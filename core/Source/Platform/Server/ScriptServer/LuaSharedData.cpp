///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LuaSharedData.h"
#include "LuaAdapters.h"
#include "LuaHelper.h"
#include "GameStateManager.h"
#ifndef PLATFORM_TEST // LocationRegistry has too much dependencies and cannot be tested at this moment
#include "common\KEPUtil\LocationRegistry.h"
#endif
using namespace std;

namespace KEP {

vector<string> ILuaSharedDataProvider::Key::ms_propertyNames;
unordered_map<string, ILuaSharedDataProvider::Key::IndexType> ILuaSharedDataProvider::Key::ms_propertyNameMap;
unsigned ILuaSharedDataProvider::Key::ms_propertyCount = ILuaSharedDataProvider::Key::staticInit();

unsigned ILuaSharedDataProvider::Key::staticInit() {
	// List of well-known properties (used across all providers / containers)
	ms_propertyNames.resize(Key::NUM_WELLKNOWN_KEYS);

	ms_propertyNames[Key::ID] = "ID";
	ms_propertyNames[Key::PID] = "PID";
	ms_propertyNames[Key::POS] = "position";
	ms_propertyNames[Key::ORI] = "orientation";
	ms_propertyNames[Key::DEST] = "destination";
	ms_propertyNames[Key::SPEED] = "speed";
	ms_propertyNames[Key::TIME2DEST] = "timeToDestination";

	// Build property name to index map (indices are 1-based contiguous integer values)
	for (unsigned index = 0; index < ms_propertyNames.size(); index++) {
		ms_propertyNameMap.insert(make_pair(ms_propertyNames[index], ILuaSharedDataProvider::Key::IndexType(index)));
	}

	return ms_propertyNames.size();
}

int LuaSharedDictionary::get(Key key, void* buffer, size_t bufSize) {
	bufSize;

	// Numeric?
	std::map<string, double>::const_iterator itNumValue = m_numericValues.find(key.str);
	if (itNumValue != m_numericValues.end()) {
		jsAssert(bufSize >= sizeof(LuaPrimValue<double>));
		new (buffer) LuaPrimValue<double>(itNumValue->second); // placement new
		return LUA_TNUMBER;
	}

	// String?
	map<string, string>::const_iterator itStrValue = m_stringValues.find(key.str);
	if (itStrValue != m_stringValues.end()) {
		jsAssert(bufSize >= sizeof(LuaPrimValue<string>));
		new (buffer) LuaPrimValue<string>(itStrValue->second); // placement new
		return LUA_TSTRING;
	}

	// Boolean?
	map<string, bool>::const_iterator itBoolValue = m_boolValues.find(key.str);
	if (itBoolValue != m_boolValues.end()) {
		jsAssert(bufSize >= sizeof(LuaPrimValue<bool>));
		new (buffer) LuaPrimValue<bool>(itBoolValue->second); // placement new
		return LUA_TBOOLEAN;
	}

	// Child dictionary?
	map<string, string>::const_iterator itDictValue = m_dictValues.find(key.str);
	if (itDictValue != m_dictValues.end()) {
		jsAssert(bufSize >= sizeof(LuaPrimValue<string>));
		new (buffer) LuaPrimValue<string>(itDictValue->second); // placement new
		return LUA_TUSERDATA;
	}

	new (buffer) LuaNil();
	return LUA_TNIL;
}

void LuaSharedDictionary::set(Key key, double val, string& childRemoved) {
	removeKey(key, childRemoved); // Q: should throw an error if user tries to change the value type?
	m_numericValues[key.str] = val;
}

void LuaSharedDictionary::set(Key key, const char* val, string& childRemoved) {
	removeKey(key, childRemoved); // Q: should throw an error if user tries to change the value type?
	m_stringValues[key.str] = val;
}

void LuaSharedDictionary::set(Key key, bool val, string& childRemoved) {
	removeKey(key, childRemoved); // Q: should throw an error if user tries to change the value type?
	m_boolValues[key.str] = val;
}

void LuaSharedDictionary::set(Key key, ChildRef val, string& childRemoved) {
	removeKey(key, childRemoved); // Q: should throw an error if user tries to change the value type?
	m_dictValues[key.str] = val.name;
}

void LuaSharedDictionary::set(Key key, lua_State* L, int index, std::string& childRemoved) {
	const char* sVal;

	// Q: should throw an error if user tries to change the value type?
	switch (lua_type(L, index)) {
		case LUA_TNUMBER:
			removeKey(key, childRemoved);
			m_numericValues[key.str] = lua_tonumber(L, index);
			break;
		case LUA_TSTRING:
			removeKey(key, childRemoved);
			sVal = lua_tostring(L, index);
			m_stringValues[key.str] = sVal ? sVal : "";
			break;
		case LUA_TBOOLEAN:
			removeKey(key, childRemoved);
			m_boolValues[key.str] = lua_toboolean(L, index) != 0;
			break;
		default:
			jsAssert(false);
			break;
	}
}

void LuaSharedDictionary::remove(Key key, string& childRemoved) {
	removeKey(key, childRemoved);
}

void LuaSharedDictionary::removeKey(Key key, string& childRemoved) {
	// Numeric?
	map<string, double>::const_iterator itNumValue = m_numericValues.find(key.str);
	if (itNumValue != m_numericValues.end()) {
		m_numericValues.erase(itNumValue);
	}

	// String?
	map<string, string>::const_iterator itStrValue = m_stringValues.find(key.str);
	if (itStrValue != m_stringValues.end()) {
		m_stringValues.erase(itStrValue);
	}

	// Boolean?
	map<string, bool>::const_iterator itBoolValue = m_boolValues.find(key.str);
	if (itBoolValue != m_boolValues.end()) {
		m_boolValues.erase(itBoolValue);
	}

	// Child dictionary?
	map<string, string>::const_iterator itDictValue = m_dictValues.find(key.str);
	if (itDictValue != m_dictValues.end()) {
		childRemoved = itDictValue->second; // Caller will deref the detached child dictionary
		m_dictValues.erase(itDictValue);
	}
}

void LuaSharedDictionary::clear(std::vector<string>& childrenToDeref) {
	m_numericValues.clear();
	m_stringValues.clear();
	m_boolValues.clear();

	// Call should deref all child dictionaries
	for (map<string, string>::const_iterator itDictVal = m_dictValues.begin(); itDictVal != m_dictValues.end(); ++itDictVal) {
		childrenToDeref.push_back(itDictVal->second);
	}

	m_dictValues.clear();
}

void LuaSharedDictionary::keys(std::set<string>& allKeys) {
	for (map<string, double>::const_iterator itNumValue = m_numericValues.begin(); itNumValue != m_numericValues.end(); ++itNumValue) {
		allKeys.insert(itNumValue->first);
	}
	for (map<string, string>::const_iterator itStrValue = m_stringValues.begin(); itStrValue != m_stringValues.end(); ++itStrValue) {
		allKeys.insert(itStrValue->first);
	}
	for (map<string, bool>::const_iterator itBoolValue = m_boolValues.begin(); itBoolValue != m_boolValues.end(); ++itBoolValue) {
		allKeys.insert(itBoolValue->first);
	}
	for (map<string, string>::const_iterator itDictValue = m_dictValues.begin(); itDictValue != m_dictValues.end(); ++itDictValue) {
		allKeys.insert(itDictValue->first);
	}
}

int LuaSharedDictionary::type(Key key) {
	return m_numericValues.find(key.str) != m_numericValues.end() ? LUA_TNUMBER :
																	m_stringValues.find(key.str) != m_stringValues.end() ? LUA_TSTRING :
																														   m_boolValues.find(key.str) != m_boolValues.end() ? LUA_TBOOLEAN :
																																											  m_dictValues.find(key.str) != m_dictValues.end() ? LUA_TUSERDATA :
																																																								 LUA_TNIL;
}

bool LuaSharedDictionary::release(std::vector<string>& childrenToDeref) {
	if (LuaSharedDataProvider::release(childrenToDeref)) {
		// Since no one is holding references to this dictionary now.
		// It should be safe to assume we can dispose it without locking it.

		// Make a copy of the child dictionaries list
		for (auto itr = m_dictValues.begin(); itr != m_dictValues.end(); ++itr) {
			childrenToDeref.push_back(itr->second);
		}

		return true;
	}

	return false;
}

// Try getting data from primary container. If not found, try secondary container.
int LuaSharedDataChain::get(Key key, void* buffer, size_t bufSize) {
	int type = m_pri ? m_pri->get(key, buffer, bufSize) : LUA_TNIL;
	if (type == LUA_TNIL)
		type = m_sec ? m_sec->get(key, buffer, bufSize) : LUA_TNIL;
	return type;
}

// If a key exists previously in primary container, update it. Otherwise put the key value pair into secondary container.
void LuaSharedDataChain::set(Key key, double val, std::string& childRemoved) {
	if (m_pri && m_pri->type(key) != LUA_TNIL)
		m_pri->set(key, val, childRemoved);
	else if (m_sec)
		m_sec->set(key, val, childRemoved);
}

// If a key exists previously in primary container, update it. Otherwise put the key value pair into secondary container.
void LuaSharedDataChain::set(Key key, const char* val, std::string& childRemoved) {
	if (m_pri && m_pri->type(key) != LUA_TNIL)
		m_pri->set(key, val, childRemoved);
	else if (m_sec)
		m_sec->set(key, val, childRemoved);
}

// If a key exists previously in primary container, update it. Otherwise put the key value pair into secondary container.
void LuaSharedDataChain::set(Key key, bool val, std::string& childRemoved) {
	if (m_pri && m_pri->type(key) != LUA_TNIL)
		m_pri->set(key, val, childRemoved);
	else if (m_sec)
		m_sec->set(key, val, childRemoved);
}

// If a key exists previously in primary container, update it. Otherwise put the key value pair into secondary container.
void LuaSharedDataChain::set(Key key, ChildRef val, std::string& childRemoved) {
	if (m_pri && m_pri->type(key) != LUA_TNIL)
		m_pri->set(key, val, childRemoved);
	else if (m_sec)
		m_sec->set(key, val, childRemoved);
}

// If a key exists previously in primary container, update it. Otherwise put the key value pair into secondary container.
void LuaSharedDataChain::set(Key key, lua_State* L, int index, std::string& childRemoved) {
	if (m_pri && m_pri->type(key) != LUA_TNIL)
		m_pri->set(key, L, index, childRemoved);
	else if (m_sec)
		m_sec->set(key, L, index, childRemoved);
}

// If a key exists previously in primary container, remove it. Otherwise try secondary container.
void LuaSharedDataChain::remove(Key key, std::string& childRemoved) {
	if (m_pri && m_pri->type(key) != LUA_TNIL)
		m_pri->remove(key, childRemoved);
	else if (m_sec)
		m_sec->remove(key, childRemoved);
}

int LuaSharedDataChain::type(Key key) {
	int type = m_pri ? m_pri->type(key) : LUA_TNIL;
	if (type == LUA_TNIL)
		type = m_sec ? m_sec->type(key) : LUA_TNIL;
	return type;
}

int LuaGameEntity::getProperty(Key key, void* buffer, size_t bufSize) {
	int type = LUA_TNIL;
	jsVerifyReturn(key.isIndex(), type);

	switch (key.idx) {
		case Key::ID:
			if (buffer == NULL || bufSize >= sizeof(LuaPrimValue<int>)) {
				if (buffer) {
					new (buffer) LuaPrimValue<int>((int)m_entityId);
				}
				type = LUA_TNUMBER;
			} else {
				jsAssert(false);
			}
	}

	return type;
}

void LuaGameEntity::allProperties(std::set<std::string>& allKeys) {
	allKeys.insert(Key::ms_propertyNames[Key::ID]);
}

int LuaGameObject::getProperty(Key key, void* buffer, size_t bufSize) {
	int type = LUA_TNIL;

	key = key.toIndex();
	jsAssert(key.isIndex());

	// Check object existence (we will check it again during evaulation to handle race condition)
	if (!m_gsm.ObjectExists(m_entityId)) {
		return LUA_TNIL;
	}

	switch (key.idx) {
		case Key::POS:
		case Key::ORI:
			if (buffer == NULL || bufSize >= sizeof(LuaStructPosRot) && bufSize >= sizeof(LuaStructVector)) {
				if (buffer) {
					double x, y, z, rx, ry, rz;
					TimeMs timeLeft;
					if (!m_gsm.EvaluateObjectPosRot(m_entityId, x, y, z, rx, ry, rz, timeLeft)) {
						return LUA_TNIL;
					}

					if (key.idx == Key::POS) {
						new (buffer) LuaStructPosRot(x, y, z, rx, ry, rz);
					} else {
						new (buffer) LuaStructVector(rx, ry, rz);
					}
				}
				type = LUA_TTABLE;
			} else {
				jsAssert(false);
			}
			break;

		case Key::DEST:
			if (buffer == NULL || bufSize >= sizeof(LuaStructPosRot)) {
				if (buffer) {
					double x, y, z, rx, ry, rz;
					TimeMs timeLeft;
					if (!m_gsm.EvaluateObjectPosRot(m_entityId, x, y, z, rx, ry, rz, timeLeft, INT_MAX)) { // Offset by INT_MAX for current destination
						return LUA_TNIL;
					}
					new (buffer) LuaStructPosRot(x, y, z, rx, ry, rz);
				}
				type = LUA_TTABLE;
			} else {
				jsAssert(false);
			}
			break;

		case Key::SPEED:
			if (buffer == NULL || bufSize >= sizeof(LuaStructVector)) {
				if (buffer) {
					double x, y, z;
					if (!m_gsm.EvaluateObjectSpeed(m_entityId, x, y, z)) {
						return LUA_TNIL;
					}
					new (buffer) LuaStructVector(x, y, z);
				}
				type = LUA_TTABLE;
			} else {
				jsAssert(false);
			}
			break;

		case Key::PID:
			type = LuaGameEntity::getProperty(Key::ID, buffer, bufSize);
			break;

		default:
			type = LuaGameEntity::getProperty(key, buffer, bufSize);
			break;
	}

	return type;
}

void LuaGameObject::allProperties(std::set<std::string>& allKeys) {
	allKeys.insert(Key::ms_propertyNames[Key::PID]);
	allKeys.insert(Key::ms_propertyNames[Key::POS]);
	allKeys.insert(Key::ms_propertyNames[Key::ORI]);
	allKeys.insert(Key::ms_propertyNames[Key::DEST]);
	allKeys.insert(Key::ms_propertyNames[Key::SPEED]);
	allKeys.insert(Key::ms_propertyNames[Key::TIME2DEST]);
	LuaGameEntity::allProperties(allKeys);
}

int LuaGamePlayer::getProperty(Key key, void* buffer, size_t bufSize) {
	int type = LUA_TNIL;

	key = key.toIndex();
	jsAssert(key.isIndex());

	switch (key.idx) {
		case Key::POS:
		case Key::ORI:
#ifndef PLATFORM_TEST // LocationRegistry has too much dependencies and cannot be tested at this moment
			if (buffer == NULL || bufSize >= sizeof(LuaStructPosRot) && bufSize >= sizeof(LuaStructVector)) {
				if (buffer) {
					pair<Vector3f, Vector3f> loc = LocationRegistry::singleton().getPlayerLocation(m_entityId);
					if (key.idx == Key::POS) {
						new (buffer) LuaStructPosRot(loc.first.x, loc.first.y, loc.first.z, loc.second.x, loc.second.y, loc.second.z);
					} else {
						new (buffer) LuaStructVector(loc.second.x, loc.second.y, loc.second.z);
					}
				}
				type = LUA_TTABLE;
			} else {
				jsAssert(false);
			}
#endif
			break;

		default:
			type = LuaGameEntity::getProperty(key, buffer, bufSize);
			break;
	}

	return type;
}

void LuaGamePlayer::allProperties(std::set<std::string>& allKeys) {
	allKeys.insert(Key::ms_propertyNames[Key::POS]);
	allKeys.insert(Key::ms_propertyNames[Key::ORI]);
	LuaGameEntity::allProperties(allKeys);
}

} // namespace KEP
