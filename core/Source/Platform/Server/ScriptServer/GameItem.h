///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

struct GameItem {
	int giId;
	int giGLID;
	int glid;
	int level;
	std::string name;
	std::string itemType;
	std::string rarity;
	std::vector<std::pair<std::string, std::string>> properties;
	bool inventoryCompatible;
};

} // namespace KEP
