///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptServerTaskObject.h"
#include "LuaAsyncFunc.h"
#include "InstanceId.h"

namespace KEP {
class IDispatcher;

class ScriptWebCallTask : public ScriptServerTaskObject {
public:
	ScriptWebCallTask(
		IDispatcher* dispatcher,
		std::string url,
		unsigned int placementId,
		ZoneIndex zoneId,
		bool usePost,
		void* pVM,
		bool async,
		const AsyncUserArg& userArg);

	virtual void ExecuteTask() override;

private:
	IDispatcher* m_dispatcher;
	std::string m_url;
	unsigned int m_placementId;
	ZoneIndex m_zoneId;
	bool m_usePost;
	void* m_vm;
	bool m_async;
	AsyncUserArg m_userArg;
};

}; // namespace KEP
