///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptServerTaskObject.h"
#include "GameStateManager.h"

namespace KEP {

class ScriptServerEngine;

class TransmitGameStateTask : public ScriptServerTaskObject {
public:
	TransmitGameStateTask(NETID netId, std::shared_ptr<GameStateManager> gsm, ZoneIndex zi) :
			ScriptServerTaskObject("TransmitGameStateTask"),
			m_clientId(netId),
			m_gameState(gsm->GetZoneGameState(zi)) {
	}

	virtual ~TransmitGameStateTask() {
	}

	virtual void ExecuteTask() override;

	NETID m_clientId;
	GameStateManager::GameStateMap m_gameState;
};

}; // namespace KEP
