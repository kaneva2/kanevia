///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Events/ScriptServerEvent.h"
#include "NetworkV2/NetworkTypes.h"
#include "InstanceId.h"

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <list>
#include <atomic>
#include <assert.h>
#include <Log4CPlus/logger.h>

namespace KEP {

class IDispatcher;
class ScriptServerTaskSystem;
class ServerScript;
enum class ScriptState;
typedef std::shared_ptr<ServerScript> ServerScriptPtr;

// A synchronized queue
class SchedulerQueue {
public:
	// Pop next script from the queue, wait if empty. Called by Scheduler only
	ServerScriptPtr pop() {
		std::unique_lock<std::mutex> lock(m_mutex);
		m_CV.wait(lock, [this]() { return !m_queue.empty(); });

		ServerScriptPtr pScript = m_queue.front();
		m_queue.pop();
		return pScript;
	}

	// Push a script into the queue
	void push(const ServerScriptPtr& pScript);

	// Abort any waiting pop() calls
	void abort() {
		ServerScriptPtr null;
		pushOne(null);
	}

private:
	__forceinline void pushOne(const ServerScriptPtr& pScript) {
		std::lock_guard<std::mutex> lock(m_mutex);
		m_queue.push(pScript);
		m_CV.notify_all();
	}

	std::mutex m_mutex;
	std::condition_variable m_CV;
	std::queue<ServerScriptPtr, std::list<ServerScriptPtr>> m_queue;
};

// Script VM scheduler
class Scheduler {
public:
	Scheduler(log4cplus::Logger& logger, ScriptServerTaskSystem& taskSystem) :
			m_pThread(nullptr), m_logger(logger), m_pDispatcher(nullptr), m_taskSystem(taskSystem) {}

	~Scheduler() {
		if (m_pThread != nullptr) {
			m_schedulerQueue.abort(); // wake up blocked worker thread
			m_pThread->join(); // wait for thread to exit
			delete m_pThread;
			m_pThread = nullptr;
		}
	}

	void start(IDispatcher* pDisp) {
		assert(m_pThread == nullptr);
		if (m_pThread == nullptr) {
			m_pThread = new std::thread(Scheduler::worker, this);
			m_pDispatcher = pDisp;
		}
	}

	// Enable script event queue and send first event (STARTING -> READY)
	void startScript(const ServerScriptPtr& pScript, NETID clientNetId, const char* arg, ULONG callerPID);

	// Add script to scheduler queue
	void queueScript(const ServerScriptPtr& pScript, const char* dbgMsg) { m_schedulerQueue.push(pScript); }

	// Schedule VM task for script (READY -> SCHEDULED)
	void scheduleScript(const ServerScriptPtr& pScript, size_t numArgs, bool unblocked, bool resumed);

	// Script running on a task thread (SCHEDULED -> RUNNING)
	void scriptCallRunning(const ServerScriptPtr& pScript);

	// Call completed (RUNNING -> IDLE | READY)
	void scriptCallCompleted(const ServerScriptPtr& pScript);

	// Script preempted (RUNNING -> YIELDED)
	void scriptCallPreempted(const ServerScriptPtr& pScript);

	// Call blocked (RUNNING -> BLOCKED)
	void scriptCallBlocked(const ServerScriptPtr& pScript, ScriptServerEvent::EventType unblockEventType, const std::string& reason);

	// Call unblocked (state already updated to BLOCKED by caller)
	void scriptCallUnblocked(const ServerScriptPtr& pScript);

	// Call erred (state already updated to ZOMBIE by caller)
	void scriptCallErred(const ServerScriptPtr& pScript);

	// Call aborted because of inconsistent state (notification only)
	void scriptCallAborted(const ServerScriptPtr& pScript);

	// Script unloaded - check for reload request (state already updated to STOPPED by caller)
	void scriptUnloaded(const ServerScriptPtr& pScript);

	void sendEventToScript(const ServerScriptPtr& pScript, ScriptServerEvent* se);

	void checkScriptBlockingTimeout(unsigned timeoutMs, bool timeoutIsFatal);

	static void worker(Scheduler* me);

private:
	std::thread* m_pThread;
	log4cplus::Logger& m_logger;
	ScriptServerTaskSystem& m_taskSystem;
	IDispatcher* m_pDispatcher;
	SchedulerQueue m_schedulerQueue;

	std::list<ServerScriptPtr> m_blockedScripts; // Used to see if a script should timeout.  Ordered by time.
	std::mutex m_blockedScriptsSync;
};

} // namespace KEP
