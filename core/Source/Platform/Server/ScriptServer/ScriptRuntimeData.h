///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

struct ScriptRuntimeData {
	std::string m_name;
	int m_placementId;
	TimeMs m_lifeTime; // total time since the script was loaded
	int m_state; // current state
	DWORD m_readyTime;
	DWORD m_idleTime;
	DWORD m_blockedTime;
};

} // namespace KEP
