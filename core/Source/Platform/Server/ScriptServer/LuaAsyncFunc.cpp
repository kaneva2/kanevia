///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LuaAsyncFunc.h"
#include "IReadableBuffer.h"
#include "IWriteableBuffer.h"

namespace KEP {

IReadableBuffer& operator>>(IReadableBuffer& ibuf, AsyncUserArg& userArg) {
	return ibuf >> userArg.m_luaType >> userArg.m_userInt >> userArg.m_userStr;
}

IWriteableBuffer& operator<<(IWriteableBuffer& obuf, const AsyncUserArg& userArg) {
	return obuf << userArg.m_luaType << userArg.m_userInt << userArg.m_userStr;
}

} // namespace KEP
