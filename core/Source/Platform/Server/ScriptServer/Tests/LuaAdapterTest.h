///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Server/ScriptServer/LuaAdapters.h"
#include "log4cplus/logger.h"

class LuaAdapterTest : public LuaTest {
};

TEST_F(LuaAdapterTest, LuaPrimValue_Push_Double) {
	ASSERT_EQ(lua_gettop(vm), 0);
	KEP::LuaPrimValue<double> nVal(2.7182818);
	nVal.push(vm);

	EXPECT_EQ(lua_gettop(vm), 1);
	EXPECT_EQ(lua_type(vm, 1), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 1), 2.7182818);

	lua_pop(vm, 1);
}

TEST_F(LuaAdapterTest, LuaPrimValue_Get_Double) {
	ASSERT_EQ(lua_gettop(vm), 0);
	lua_pushnumber(vm, 3.14159);
	KEP::LuaPrimValue<double> nVal(0);
	EXPECT_EQ(nVal.val, 0);

	nVal.get(vm, 1);
	EXPECT_EQ(nVal.val, 3.14159);

	lua_pop(vm, 1);
}

TEST_F(LuaAdapterTest, LuaPrimValue_Push_bool) {
	ASSERT_EQ(lua_gettop(vm), 0);
	KEP::LuaPrimValue<bool> bVal1(true);
	bVal1.push(vm);

	EXPECT_EQ(lua_gettop(vm), 1);
	EXPECT_EQ(lua_type(vm, 1), LUA_TBOOLEAN);
	EXPECT_EQ(lua_toboolean(vm, 1), 1);

	KEP::LuaPrimValue<bool> bVal2(false);
	bVal2.push(vm);

	EXPECT_EQ(lua_gettop(vm), 2);
	EXPECT_EQ(lua_type(vm, 2), LUA_TBOOLEAN);
	EXPECT_EQ(lua_toboolean(vm, 2), 0);

	lua_pop(vm, 2);
}

TEST_F(LuaAdapterTest, LuaPrimValue_Get_bool) {
	ASSERT_EQ(lua_gettop(vm), 0);
	lua_pushboolean(vm, 1);
	lua_pushboolean(vm, 0);
	KEP::LuaPrimValue<bool> bVal(false);
	EXPECT_EQ(bVal.val, false);

	bVal.get(vm, 1);
	EXPECT_EQ(bVal.val, true);
	bVal.get(vm, 2);
	EXPECT_EQ(bVal.val, false);

	lua_pop(vm, 2);
}

TEST_F(LuaAdapterTest, LuaPrimValue_Push_String) {
	ASSERT_EQ(lua_gettop(vm), 0);
	const char *str = "World of Kaneva";
	KEP::LuaPrimValue<const char *> sVal(str);
	sVal.push(vm);

	EXPECT_EQ(lua_gettop(vm), 1);
	EXPECT_EQ(lua_type(vm, 1), LUA_TSTRING);
	EXPECT_STREQ(lua_tostring(vm, 1), str);

	lua_pop(vm, 1);
}

TEST_F(LuaAdapterTest, LuaPrimValue_Get_String) {
	ASSERT_EQ(lua_gettop(vm), 0);
	lua_pushstring(vm, "Carpenter Dr");
	KEP::LuaPrimValue<string> sVal("");
	EXPECT_STREQ(sVal.val.c_str(), "");

	sVal.get(vm, 1);
	EXPECT_STREQ(sVal.val.c_str(), "Carpenter Dr");

	lua_pop(vm, 1);
}

TEST_F(LuaAdapterTest, LuaStructVector_Push) {
	ASSERT_EQ(lua_gettop(vm), 0);
	KEP::LuaStructVector vect(3.142, 2.718, 1.414);
	vect.push(vm);

	EXPECT_EQ(lua_gettop(vm), 1);
	EXPECT_EQ(lua_type(vm, 1), LUA_TTABLE);

	lua_getfield(vm, 1, "x");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 3.142);
	lua_pop(vm, 1);

	lua_getfield(vm, 1, "y");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 2.718);
	lua_pop(vm, 1);

	lua_getfield(vm, 1, "z");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 1.414);
	lua_pop(vm, 1);

	lua_pop(vm, 1);
}

TEST_F(LuaAdapterTest, LuaStructVector_Get) {
	ASSERT_EQ(lua_gettop(vm), 0);
	lua_newtable(vm);
	lua_pushnumber(vm, 9.314);
	lua_setfield(vm, 1, "x");
	lua_pushnumber(vm, 24);
	lua_setfield(vm, 1, "y");
	lua_pushnumber(vm, -456.13);
	lua_setfield(vm, 1, "z");

	KEP::LuaStructVector vect;
	EXPECT_EQ(vect.x, 0);
	EXPECT_EQ(vect.y, 0);
	EXPECT_EQ(vect.z, 0);

	vect.get(vm, 1);
	EXPECT_EQ(vect.x, 9.314);
	EXPECT_EQ(vect.y, 24);
	EXPECT_EQ(vect.z, -456.13);

	lua_pop(vm, 1);
}

TEST_F(LuaAdapterTest, LuaStructPosRot_Push) {
	ASSERT_EQ(lua_gettop(vm), 0);
	KEP::LuaStructPosRot vect(123, 456, 789, 3.142, 2.718, 1.414);
	vect.push(vm);

	EXPECT_EQ(lua_gettop(vm), 1);
	EXPECT_EQ(lua_type(vm, 1), LUA_TTABLE);

	lua_getfield(vm, 1, "x");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 123);
	lua_pop(vm, 1);

	lua_getfield(vm, 1, "y");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 456);
	lua_pop(vm, 1);

	lua_getfield(vm, 1, "z");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 789);
	lua_pop(vm, 1);

	lua_getfield(vm, 1, "rx");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 3.142);
	lua_pop(vm, 1);

	lua_getfield(vm, 1, "ry");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 2.718);
	lua_pop(vm, 1);

	lua_getfield(vm, 1, "rz");
	EXPECT_EQ(lua_type(vm, 2), LUA_TNUMBER);
	EXPECT_EQ(lua_tonumber(vm, 2), 1.414);
	lua_pop(vm, 1);

	lua_pop(vm, 1);
}
