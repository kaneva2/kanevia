///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Server/ScriptServer/JsonConfig.h"

class JsonConfigTest : public LuaTest {
public:
	JsonConfigTest() {
		jsonStr = R"(
{
    "global": {
        "_comment": "Global configurations and feature switches not related to A/B tests",
        "menus": {
            "Places.xml": "Places_15_5.xml"
        },
        "settings": {
            "ClientConfigs": {
                "new_users": {
                    "avatarSelection": false
                }
            }
        }
    },
    "1f05d08b-15f4-11e5-aa91-002219919c2a": { 
        "_comment": "0dfb20d2-15f4-11e5-aa91-002219919c2a: World Test / group B: script variations + ServerConfigs.survival.starving = false",
        "scripts": {
            "Pet.lua": "Pet_B.lua",
            "Controller_Game.lua": "Controller_Game_B.lua"
        },
        "settings": {
            "ServerConfigs": {
                "survival": {
                    "starving": false
                }
            }
        }
    },
    "119": {
        "_comment": "Experiment 56: New Hud vs Classics / Group B",
        "menus": {
            "Hud.xml": "Hud_Classics.xml"
        }
    }
}
		)";

		jsonStrBad = "abc";
	}

protected:
	const char* jsonStr;
	const char* jsonStrBad;
};

TEST_F(JsonConfigTest, ParsingTest) {
	JsonConfig jsonCfgBad(jsonStrBad);
	ASSERT_TRUE(jsonCfgBad.hasError());

	JsonConfig jsonCfg(jsonStr);
	ASSERT_FALSE(jsonCfg.hasError());

	auto root = jsonCfg.getJsonRoot();
	ASSERT_TRUE(root.is_object());

	const json11::Json& node1 = root["global"];
	ASSERT_TRUE(node1.is_object());
	ASSERT_TRUE(node1["menus"].is_object());
	ASSERT_TRUE(node1["menus"]["Places.xml"].is_string());
	EXPECT_EQ(node1["menus"]["Places.xml"].string_value(), std::string("Places_15_5.xml"));

	ASSERT_TRUE(node1["settings"].is_object());
	ASSERT_TRUE(node1["settings"]["ClientConfigs"].is_object());
	ASSERT_TRUE(node1["settings"]["ClientConfigs"]["new_users"].is_object());
	ASSERT_TRUE(node1["settings"]["ClientConfigs"]["new_users"]["avatarSelection"].is_bool());
	EXPECT_FALSE(node1["settings"]["ClientConfigs"]["new_users"]["avatarSelection"].bool_value());

	const json11::Json& node2 = root["1f05d08b-15f4-11e5-aa91-002219919c2a"];
	ASSERT_TRUE(node2.is_object());
	ASSERT_TRUE(node2["scripts"].is_object());
	ASSERT_TRUE(node2["scripts"]["Pet.lua"].is_string());
	EXPECT_EQ(node2["scripts"]["Pet.lua"].string_value(), std::string("Pet_B.lua"));
	ASSERT_TRUE(node2["scripts"]["Controller_Game.lua"].is_string());
	EXPECT_EQ(node2["scripts"]["Controller_Game.lua"].string_value(), std::string("Controller_Game_B.lua"));

	ASSERT_TRUE(node2["settings"].is_object());
	ASSERT_TRUE(node2["settings"]["ServerConfigs"].is_object());
	ASSERT_TRUE(node2["settings"]["ServerConfigs"]["survival"].is_object());
	ASSERT_TRUE(node2["settings"]["ServerConfigs"]["survival"]["starving"].is_bool());
	EXPECT_FALSE(node2["settings"]["ServerConfigs"]["survival"]["starving"].bool_value());

	const json11::Json& node3 = root["119"];
	ASSERT_TRUE(node3.is_object());
	ASSERT_TRUE(node3["menus"].is_object());
	ASSERT_TRUE(node3["menus"]["Hud.xml"].is_string());
	EXPECT_EQ(node3["menus"]["Hud.xml"].string_value(), std::string("Hud_Classics.xml"));
}

TEST_F(JsonConfigTest, ScriptNameMapperTest) {
	JsonConfig jsonCfg(jsonStr);
	ASSERT_FALSE(jsonCfg.hasError());

	std::vector<std::string> configGroups;
	configGroups.push_back("global");
	configGroups.push_back("119");

	auto nameMapper = jsonCfg.getScriptNameMapper(configGroups);
	EXPECT_NE(nameMapper("Pet.lua"), std::string("Pet_B.lua"));
	EXPECT_NE(nameMapper("Controller_Game.lua"), std::string("Controller_Game_B.lua"));

	configGroups.clear();
	configGroups.push_back("global");
	configGroups.push_back("1f05d08b-15f4-11e5-aa91-002219919c2a");

	auto nameMapper2 = jsonCfg.getScriptNameMapper(configGroups);
	EXPECT_EQ(nameMapper2("Pet.lua"), std::string("Pet_B.lua"));
	EXPECT_EQ(nameMapper2("Controller_Game.lua"), std::string("Controller_Game_B.lua"));
}

TEST_F(JsonConfigTest, LuaScriptConfiguratorTest) {
	JsonConfig jsonCfg(jsonStr);
	ASSERT_FALSE(jsonCfg.hasError());

	std::vector<std::string> configGroups;
	configGroups.push_back("global");
	configGroups.push_back("119");

	auto configurator = jsonCfg.getLuaScriptConfigurator(configGroups);
	configurator(vm);

	lua_getglobal(vm, "ServerConfigs");
	EXPECT_TRUE(lua_isnil(vm, -1));

	configGroups.clear();
	configGroups.push_back("global");
	configGroups.push_back("1f05d08b-15f4-11e5-aa91-002219919c2a");

	auto configurator2 = jsonCfg.getLuaScriptConfigurator(configGroups);
	configurator2(vm);

	lua_getglobal(vm, "ServerConfigs");
	ASSERT_TRUE(lua_istable(vm, -1));
	lua_getfield(vm, -1, "survival");
	ASSERT_TRUE(lua_istable(vm, -1));
	lua_getfield(vm, -1, "starving");
	ASSERT_TRUE(lua_isboolean(vm, -1));
}

TEST_F(JsonConfigTest, ConfigProviderTest) {
	JsonConfig jsonCfg(jsonStr);
	ASSERT_FALSE(jsonCfg.hasError());

	std::vector<std::string> configGroups;
	configGroups.push_back("global");
	configGroups.push_back("119");

	std::string sNotFound = "-NOT-FOUND-";

	auto serverCfg = jsonCfg.getServerConfigProvider(configGroups);
	EXPECT_EQ(serverCfg("ServerConfigs.survival.starving", &sNotFound), sNotFound);

	auto clientCfg = jsonCfg.getClientConfigProvider(configGroups);
	EXPECT_EQ(clientCfg("ClientConfigs.new_users.avatarSelection", &sNotFound), "false");

	configGroups.clear();
	configGroups.push_back("global");
	configGroups.push_back("1f05d08b-15f4-11e5-aa91-002219919c2a");

	auto serverCfg2 = jsonCfg.getServerConfigProvider(configGroups);
	EXPECT_EQ(serverCfg2("ServerConfigs.survival.starving", &sNotFound), "false");

	auto clientCfg2 = jsonCfg.getClientConfigProvider(configGroups);
	EXPECT_EQ(clientCfg2("ClientConfigs.new_users.avatarSelection", &sNotFound), "false");
}
