///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "log4cplus/logger.h"

#if (LUA_VERSION_NUM < 500 || LUA_VERSION_NUM > 502)
#pragma error("This unit test supports Lua 5.0, 5.1 and 5.2 only")
#endif

const char *linePadding = "    <STDOUT> ";

// yieldInC(debugId)
static int yieldInC(lua_State *L) {
	cout << linePadding << "yieldInC(" << lua_tonumber(L, 1) << ")" << endl;
	return lua_yield(L, 0);
}

// callbackFromC( func, arg1, arg2, ..., debugMsg, numArgs )
static int callbackFromC(lua_State *L) {
	int numArgs = (int)lua_tonumber(L, -1);
	cout << linePadding << "callbackFromC: " << lua_tostring(L, -2) << endl;
	lua_pop(L, 2);
	lua_call(L, numArgs, 0);
	return 0;
}

// print(str)
static int printMessage(lua_State *L) {
	cout << linePadding << lua_tostring(L, -1) << endl;
	return 0;
}

class LuaYieldTest : public LuaTest {
public:
	void SetUp() {
		LuaTest::SetUp();
		lua_pushcfunction(vm, luaopen_base);
		lua_call(vm, 0, 0);
#if (LUA_VERSION_NUM == 502) // In Lua 5.2 coroutine library has to be loaded separately
		lua_pushcfunction(vm, luaopen_coroutine);
		lua_call(vm, 0, 1);
		lua_setglobal(vm, "coroutine"); // In Lua 5.2 loaded library is NOT automatically a global table. Instead, luaopen_coroutine returns the table on the stack.
#endif
		lua_pushcfunction(vm, printMessage); // custom print function that takes 1 string as parameter
		lua_setglobal(vm, "print");
		lua_pushcfunction(vm, yieldInC); // yield function with debug message
		lua_setglobal(vm, "yieldInC");
		lua_pushcfunction(vm, callbackFromC); // plain callback
		lua_setglobal(vm, "callbackFromC");
	}

protected:
	int LoadString(lua_State *L, const char *script, string &errMsg) {
		errMsg.clear();
		int ret = luaL_loadstring(L, script);
		if (ret != 0) {
			const char *luaErrMsg = lua_tostring(L, -1);
			stringstream ss;
			switch (ret) {
				case LUA_ERRSYNTAX:
					ss << "loadstring: Syntax error - " << (luaErrMsg ? luaErrMsg : "N/A");
					break;
				case LUA_ERRMEM:
					ss << "loadstring: Out of memory";
					break;
			}
			errMsg = ss.str();
		}
		return ret;
	}

	lua_State *NewThread() {
		// Create coroutine with the loaded script as body
		// Coroutine will be disposed by GC once it's no longer used
		return lua_newcthread(vm, 0); // NOTE: coco provides this new API which creates a thread is capable of yielding at any location
	}

	int Resume(lua_State *L, int narg, string &errMsg) {
		errMsg.clear();
#if (LUA_VERSION_NUM == 502) // Lua 5.2 adds a new a parameter for lua_resume
		int ret = lua_resume(L, NULL, narg);
#else
		int ret = lua_resume(L, narg);
#endif
		if (ret != 0) {
			const char *luaErrMsg = lua_tostring(L, -1);
			stringstream ss;
			switch (ret) {
				case LUA_YIELD:
					ss << "resume: Yielded";
					break;
				case LUA_ERRRUN:
					ss << "resume: Runtime error - " << (luaErrMsg ? luaErrMsg : "N/A");
					break;
				case LUA_ERRSYNTAX:
					ss << "resume: Syntax error - " << (luaErrMsg ? luaErrMsg : "N/A");
					break;
				case LUA_ERRMEM:
					ss << "resume: Out of memory";
					break;
				case LUA_ERRERR:
					ss << "resume: Other error - " << (luaErrMsg ? luaErrMsg : "N/A");
					break;
			}
			errMsg = ss.str();
		}
		return ret;
	}
};

TEST_F(LuaYieldTest, pcall) {
	const char *script =
		"\
		function test()						\n\
			print(\"test()\")				\n\
			yieldInC(1)						\n\
			print(\"after yieldInC(1)\")	\n\
		end									\n\
											\n\
		print(\"before pcall\")				\n\
		local res, msg = pcall(function() test() end)	\n\
		if not res then						\n\
			print(msg)						\n\
		end									\n\
		print(\"after pcall\")				\n\
		yieldInC(2)							\n\
		print(\"after yieldInC(2)\")		\n\
	";

	string errMsg;

	// Create thread
	lua_State *thd = NewThread();
	ASSERT_NE(thd, (lua_State *)NULL) << "Error creating coroutine";

	// Load script as a chunk
	ASSERT_EQ(LoadString(thd, script, errMsg), 0) << errMsg;

	// Run chunk on the thread
	ASSERT_EQ(Resume(thd, 0, errMsg), LUA_YIELD) << errMsg; // expecting yield 1 from test()
	ASSERT_EQ(Resume(thd, 0, errMsg), LUA_YIELD) << errMsg; // expecting yield 2 from main()
	ASSERT_EQ(Resume(thd, 0, errMsg), 0) << errMsg; // expecting completion
}

TEST_F(LuaYieldTest, Iterator) {
	const char *script =
		"\
		function myIter(t)					\n\
			return							\n\
				function(t, i)				\n\
					i = i + 1				\n\
					yieldInC(i)				\n\
					if t[i]==nil then		\n\
						return nil			\n\
					end						\n\
					return i, t[i]			\n\
				end, t, 0					\n\
		end									\n\
		local t = { \"hello\", \"world\" }	\n\
		for k, v in myIter(t) do			\n\
			print(v)						\n\
		end									\n\
	";

	string errMsg;

	// Create thread
	lua_State *thd = NewThread();
	ASSERT_NE(thd, (lua_State *)NULL) << "Error creating coroutine";

	// Load script as a chunk
	ASSERT_EQ(LoadString(thd, script, errMsg), 0) << errMsg;

	// Run chunk on the thread
	ASSERT_EQ(Resume(thd, 0, errMsg), LUA_YIELD) << errMsg; // expecting yield 1 from iteration 1
	ASSERT_EQ(Resume(thd, 0, errMsg), LUA_YIELD) << errMsg; // expecting yield 2 from iteration 2
	ASSERT_EQ(Resume(thd, 0, errMsg), LUA_YIELD) << errMsg; // expecting yield 3 from iteration 3 (aborted)
	ASSERT_EQ(Resume(thd, 0, errMsg), 0) << errMsg; // expecting completion
}

TEST_F(LuaYieldTest, Callback) {
	const char *script =
		"\
		function test(str)					\n\
			print(str)						\n\
			coroutine.yield()				\n\
			print(\"done\")					\n\
		end									\n\
		callbackFromC(test, \"hello world\", \"calling test\", 1)\n\
	";

	string errMsg;

	// Create thread
	lua_State *thd = NewThread();
	ASSERT_NE(thd, (lua_State *)NULL) << "Error creating coroutine";

	// Load script as a chunk
	ASSERT_EQ(LoadString(thd, script, errMsg), 0) << errMsg;

	// Run chunk on the thread
	ASSERT_EQ(Resume(thd, 0, errMsg), LUA_YIELD) << errMsg; // expecting yield 1 from test()
	ASSERT_EQ(Resume(thd, 0, errMsg), 0) << errMsg; // expecting completion
}

TEST_F(LuaYieldTest, Metamethod) {
	const char *script =
		"\
		local x = {value = 5}								\n\
		local mt = {										\n\
			__add = function (lhs, rhs)						\n\
			coroutine.yield()								\n\
			return { value = lhs.value + rhs.value }		\n\
			end												\n\
		}													\n\
		setmetatable(x, mt)									\n\
		local y = x + x										\n\
		print(y.value)										\n\
	";

	string errMsg;

	// Create thread
	lua_State *thd = NewThread();
	ASSERT_NE(thd, (lua_State *)NULL) << "Error creating coroutine";

	// Load script as a chunk
	ASSERT_EQ(LoadString(thd, script, errMsg), 0) << errMsg;

	// Run chunk on the thread
	ASSERT_EQ(Resume(thd, 0, errMsg), LUA_YIELD) << errMsg; // expecting yield 1 from metamethod __add
	ASSERT_EQ(Resume(thd, 0, errMsg), 0) << errMsg; // expecting completion
}
