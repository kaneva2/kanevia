///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Server/ScriptServer/LuaSharedData.h"
#include "Server/ScriptServer/LuaAdapters.h"
#include "Server/ScriptServer/GameStateManager.h"
#include "Common/include/CoreStatic/InstanceId.h"
#include "log4cplus/logger.h"

class LuaSharedDataTest : public LuaTest {
protected:
	LuaSharedDataTest() {
		zi = ZoneIndex(CHANNEL_ID, INSTANCE_ID, 3); // Potential Side Effect Issue: This zone index will be recorded in ZoneIndex class and potentially affects other tests
		gsm = new GameStateManager(zi, log4cplus::Logger::getInstance(L"Test"), std::set<ULONG>());
	}

	~LuaSharedDataTest() {
		delete gsm;
	}

	bool isEmpty(ILuaSharedDataProvider &sd) {
		std::set<std::string> allKeys;
		sd.keys(allKeys);
		return allKeys.empty();
	}

	void expectLuaValue(int valIndex, double expected, double maxError = 0) {
		EXPECT_EQ(lua_type(vm, valIndex), LUA_TNUMBER);
		EXPECT_NEAR(lua_tonumber(vm, valIndex), expected, maxError);
	}

	void expectLuaValue(int valIndex, bool expected) {
		EXPECT_EQ(lua_type(vm, valIndex), LUA_TBOOLEAN);
		EXPECT_EQ(lua_toboolean(vm, valIndex), expected ? 1 : 0);
	}

	void expectLuaValue(int valIndex, const char *expected) {
		EXPECT_EQ(lua_type(vm, valIndex), LUA_TSTRING);
		EXPECT_STREQ(lua_tostring(vm, valIndex), expected);
	}

	void expectLuaValue(int valIndex, const string &expected) {
		EXPECT_EQ(lua_type(vm, valIndex), LUA_TSTRING);
		EXPECT_STREQ(lua_tostring(vm, valIndex), expected.c_str());
	}

	void expectLuaValueBetween(int valIndex, double expected1, double expected2) {
		double mid = (expected1 + expected2) / 2;
		double err = fabs(expected1 - expected2) / 2;
		expectLuaValue(valIndex, mid, err);
	}

	LuaSharedDictionary dict;
	GameStateManager *gsm;
	ZoneIndex zi;
};

TEST_F(LuaSharedDataTest, Dictionary_String_SetGetPush) {
	EXPECT_TRUE(isEmpty(dict));
	EXPECT_EQ(lua_gettop(vm), 0);

	std::string childRemoved;
	dict.set("name", "n the preceding, can be used regardless of whether the type defines a destructor. This allows ", childRemoved);

	EXPECT_FALSE(isEmpty(dict));
	EXPECT_TRUE(childRemoved.empty());
	EXPECT_EQ(dict.type("name"), LUA_TSTRING);

	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer; // First 4 bytes of the buffer is the pointer to vtable
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(dict.get("name", buffer, sizeof(buffer)), LUA_TSTRING);
	ASSERT_NE(*ppVTbl, (void *)NULL);
	auto sVal = dynamic_cast<LuaPrimValue<string> *>(value);
	ASSERT_NE(sVal, (void *)NULL);
	sVal->push(vm);

	EXPECT_EQ(lua_gettop(vm), 1);
	expectLuaValue(1, "n the preceding, can be used regardless of whether the type defines a destructor. This allows ");

	value->~ILuaAdapter();
	lua_pop(vm, 1);
}

TEST_F(LuaSharedDataTest, Dictionary_String_VMSetVerify) {
	EXPECT_TRUE(isEmpty(dict));
	EXPECT_EQ(lua_gettop(vm), 0);

	std::string childRemoved;
	lua_pushstring(vm, "cannot deallocate this memory because it is not allocated from the free store");
	dict.set("name", vm, -1, childRemoved);

	EXPECT_FALSE(isEmpty(dict));
	EXPECT_TRUE(childRemoved.empty());
	EXPECT_EQ(dict.type("name"), LUA_TSTRING);

	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(dict.get("name", buffer, sizeof(buffer)), LUA_TSTRING);
	ASSERT_NE(*ppVTbl, (void *)NULL);
	auto sVal = dynamic_cast<LuaPrimValue<string> *>(value);
	ASSERT_NE(sVal, (void *)NULL);

	EXPECT_STREQ(sVal->val.c_str(), "cannot deallocate this memory because it is not allocated from the free store");

	value->~ILuaAdapter();
	lua_pop(vm, 1);
}

TEST_F(LuaSharedDataTest, Dictionary_Double_SetGetPush) {
	EXPECT_TRUE(isEmpty(dict));
	EXPECT_EQ(lua_gettop(vm), 0);

	std::string childRemoved;
	dict.set("speed", 5.4610, childRemoved);

	EXPECT_FALSE(isEmpty(dict));
	EXPECT_TRUE(childRemoved.empty());
	EXPECT_EQ(dict.type("speed"), LUA_TNUMBER);

	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(dict.get("speed", buffer, sizeof(buffer)), LUA_TNUMBER);
	ASSERT_NE(*ppVTbl, (void *)NULL);
	auto nVal = dynamic_cast<LuaPrimValue<double> *>(value);
	ASSERT_NE(nVal, (void *)NULL);
	nVal->push(vm);

	EXPECT_EQ(lua_gettop(vm), 1);
	expectLuaValue(1, 5.4610);

	value->~ILuaAdapter();
	lua_pop(vm, 1);
}

TEST_F(LuaSharedDataTest, Dictionary_Double_VMSetVerify) {
	EXPECT_TRUE(isEmpty(dict));
	EXPECT_EQ(lua_gettop(vm), 0);

	std::string childRemoved;
	lua_pushnumber(vm, 83.14);
	lua_pushnumber(vm, 24842849);
	dict.set("balance", vm, -2, childRemoved);

	EXPECT_FALSE(isEmpty(dict));
	EXPECT_TRUE(childRemoved.empty());
	EXPECT_EQ(dict.type("balance"), LUA_TNUMBER);

	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(dict.get("balance", buffer, sizeof(buffer)), LUA_TNUMBER);
	ASSERT_NE(*ppVTbl, (void *)NULL);
	auto nVal = dynamic_cast<LuaPrimValue<double> *>(value);
	ASSERT_NE(nVal, (void *)NULL);

	EXPECT_EQ(nVal->val, 83.14);

	value->~ILuaAdapter();
	lua_pop(vm, 1);
}

TEST_F(LuaSharedDataTest, Dictionary_Bool_SetGetPush) {
	EXPECT_TRUE(isEmpty(dict));
	EXPECT_EQ(lua_gettop(vm), 0);

	std::string childRemoved;
	dict.set("moving", true, childRemoved);
	dict.set("stopped", false, childRemoved);

	EXPECT_FALSE(isEmpty(dict));
	EXPECT_TRUE(childRemoved.empty());
	EXPECT_EQ(dict.type("moving"), LUA_TBOOLEAN);
	EXPECT_EQ(dict.type("stopped"), LUA_TBOOLEAN);

	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(dict.get("moving", buffer, sizeof(buffer)), LUA_TBOOLEAN);
	ASSERT_NE(*ppVTbl, (void *)NULL);
	auto bVal = dynamic_cast<LuaPrimValue<bool> *>(value);
	ASSERT_NE(bVal, (void *)NULL);
	bVal->push(vm);

	value->~ILuaAdapter();
	*ppVTbl = NULL;

	EXPECT_EQ(dict.get("stopped", buffer, sizeof(buffer)), LUA_TBOOLEAN);
	ASSERT_NE(*ppVTbl, (void *)NULL);
	bVal = dynamic_cast<LuaPrimValue<bool> *>(value);
	ASSERT_NE(bVal, (void *)NULL);
	bVal->push(vm);

	EXPECT_EQ(lua_gettop(vm), 2);
	expectLuaValue(1, true);
	expectLuaValue(2, false);

	value->~ILuaAdapter();
	lua_pop(vm, 2);
}

TEST_F(LuaSharedDataTest, Dictionary_ChildRemoval) {
	EXPECT_TRUE(isEmpty(dict));
	EXPECT_EQ(lua_gettop(vm), 0);

	std::string childRemoved;

	dict.set("child", ILuaSharedDataProvider::ChildRef("CHILD_TEST"), childRemoved);
	EXPECT_FALSE(isEmpty(dict));
	EXPECT_TRUE(childRemoved.empty());
	EXPECT_EQ(dict.type("child"), LUA_TUSERDATA);

	dict.set("child", 5.4610, childRemoved);
	EXPECT_FALSE(isEmpty(dict));
	EXPECT_STREQ(childRemoved.c_str(), "CHILD_TEST");
	EXPECT_EQ(dict.type("child"), LUA_TNUMBER);

	std::set<std::string> keys;
	dict.keys(keys);
	EXPECT_EQ(keys.size(), 1);
}

TEST_F(LuaSharedDataTest, Object_ID) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL,
		OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID,
		POS[1].x, POS[1].y, POS[1].z, DIR[1].x, DIR[1].y, DIR[1].z,
		Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectMove event with duration
	e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2].x, POS[2].y, POS[2].z, MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// LuaGameObject
	LuaGameObject lgo(*gsm, OBJECT_ID[0]);

	// Buffer
	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(lgo.get("ID", buffer, sizeof(buffer)), LUA_TNUMBER);
	ASSERT_NE(*ppVTbl, (void *)NULL);

	auto nVal = dynamic_cast<LuaPrimValue<int> *>(value);
	ASSERT_NE(nVal, (void *)NULL);

	value->push(vm);
	EXPECT_EQ(lua_gettop(vm), 1);
	expectLuaValue(1, (double)OBJECT_ID[0]);

	value->~ILuaAdapter();
	lua_pop(vm, 1);
}

TEST_F(LuaSharedDataTest, BROKEN_Object_Position) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL,
		OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID,
		POS[1].x, POS[1].y, POS[1].z, DIR[1].x, DIR[1].y, DIR[1].z,
		Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectMove event with duration
	TimeMs startTime = fTime::TimeMs();
	e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2].x, POS[2].y, POS[2].z, MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// LuaGameObject
	LuaGameObject lgo(*gsm, OBJECT_ID[0]);

	// Buffer
	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(lgo.get("position", buffer, sizeof(buffer)), LUA_TTABLE);
	ASSERT_NE(*ppVTbl, (void *)NULL);

	auto posrot = dynamic_cast<LuaStructPosRot *>(value);
	ASSERT_NE(posrot, (void *)NULL);

	value->push(vm);
	EXPECT_EQ(lua_gettop(vm), 1);
	EXPECT_EQ(lua_type(vm, 1), LUA_TTABLE);

	lua_getfield(vm, 1, "x");
	lua_getfield(vm, 1, "y");
	lua_getfield(vm, 1, "z");
	lua_getfield(vm, 1, "rx");
	lua_getfield(vm, 1, "ry");
	lua_getfield(vm, 1, "rz");

	TimeMs elapsed = fTime::ElapsedMs(startTime);
	Vector3d posLerp = POS[1] + (POS[2] - POS[1]) * elapsed / MOVE_DURATION1;

	expectLuaValueBetween(-6, POS[1].x, posLerp.x);
	expectLuaValueBetween(-5, POS[1].y, posLerp.y);
	expectLuaValueBetween(-4, POS[1].z, posLerp.z);
	expectLuaValue(-3, DIR[1].x);
	expectLuaValue(-2, DIR[1].y);
	expectLuaValue(-1, DIR[1].z);
	lua_pop(vm, 6);

	value->~ILuaAdapter();
	lua_pop(vm, 1);
}

TEST_F(LuaSharedDataTest, Object_Destination) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL,
		OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID,
		POS[1].x, POS[1].y, POS[1].z, DIR[1].x, DIR[1].y, DIR[1].z,
		Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectMove event with duration
	e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2].x, POS[2].y, POS[2].z, MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// LuaGameObject
	LuaGameObject lgo(*gsm, OBJECT_ID[0]);

	// Buffer
	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	EXPECT_EQ(lgo.get("destination", buffer, sizeof(buffer)), LUA_TTABLE);
	ASSERT_NE(*ppVTbl, (void *)NULL);

	auto posrot = dynamic_cast<LuaStructPosRot *>(value);
	ASSERT_NE(posrot, (void *)NULL);

	value->push(vm);
	EXPECT_EQ(lua_gettop(vm), 1);
	EXPECT_EQ(lua_type(vm, 1), LUA_TTABLE);

	lua_getfield(vm, 1, "x");
	lua_getfield(vm, 1, "y");
	lua_getfield(vm, 1, "z");
	lua_getfield(vm, 1, "rx");
	lua_getfield(vm, 1, "ry");
	lua_getfield(vm, 1, "rz");

	expectLuaValue(-6, POS[2].x);
	expectLuaValue(-5, POS[2].y);
	expectLuaValue(-4, POS[2].z);
	expectLuaValue(-3, DIR[1].x);
	expectLuaValue(-2, DIR[1].y);
	expectLuaValue(-1, DIR[1].z);
	lua_pop(vm, 6);

	value->~ILuaAdapter();
	lua_pop(vm, 1);
}

TEST_F(LuaSharedDataTest, Deleted_Object) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL,
		OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID,
		POS[1].x, POS[1].y, POS[1].z, DIR[1].x, DIR[1].y, DIR[1].z,
		Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectMove event with duration
	e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2].x, POS[2].y, POS[2].z, MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// LuaGameObject
	LuaGameObject lgo(*gsm, OBJECT_ID[0]);

	// Buffer
	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void **ppVTbl = (void **)buffer;
	ILuaAdapter *value = (ILuaAdapter *)buffer;

	*ppVTbl = NULL;

	// Query object.ID
	EXPECT_EQ(lgo.get("ID", buffer, sizeof(buffer)), LUA_TNUMBER); // Check if returned type is NUMBER
	ASSERT_NE(*ppVTbl, (void *)NULL);

	// Check if the result can be casted into LuaprimValue<int>
	auto nVal = dynamic_cast<LuaPrimValue<int> *>(value);
	ASSERT_NE(nVal, (void *)NULL);

	// Push the result on Lua stack and verify it
	value->push(vm);
	EXPECT_EQ(lua_gettop(vm), 1);
	expectLuaValue(1, (double)OBJECT_ID[0]);
	lua_pop(vm, 1);

	// Call destructor on the result
	value->~ILuaAdapter();

	// Query object.position
	*ppVTbl = NULL;
	EXPECT_EQ(lgo.get("position", buffer, sizeof(buffer)), LUA_TTABLE); // Check if returned type is TABLE
	ASSERT_NE(*ppVTbl, (void *)NULL);
	value->~ILuaAdapter();

	// Query object.orientation
	*ppVTbl = NULL;
	EXPECT_EQ(lgo.get("orientation", buffer, sizeof(buffer)), LUA_TTABLE);
	ASSERT_NE(*ppVTbl, (void *)NULL);
	value->~ILuaAdapter();

	// Delete object
	gsm->RemoveGameStateObject(zi, OBJECT_ID[0]);

	// Try again (all queries should return NIL since object is deleted)

	// Query object.ID
	*ppVTbl = NULL;
	EXPECT_EQ(lgo.get("ID", buffer, sizeof(buffer)), LUA_TNIL); // Check if returned type is NIL
	ASSERT_EQ(*ppVTbl, (void *)NULL);

	// Query object.position
	*ppVTbl = NULL;
	EXPECT_EQ(lgo.get("position", buffer, sizeof(buffer)), LUA_TNIL); // Check if returned type is NIL
	ASSERT_EQ(*ppVTbl, (void *)NULL);

	// Query object.orientation
	*ppVTbl = NULL;
	EXPECT_EQ(lgo.get("orientation", buffer, sizeof(buffer)), LUA_TNIL); // Check if returned type is NIL
	ASSERT_EQ(*ppVTbl, (void *)NULL);
}
