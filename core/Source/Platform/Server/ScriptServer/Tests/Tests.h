///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "Event/EventSystem/BEREncodingFactory.h"
#include "LuaHelper.h"
#include "Core/Math/Vector.h"
#include "Core/Math/Stream.h"

class TestWithEvents : public testing::Test {
protected:
	// Setup Encoding Factory
	TestWithEvents() {
		IEvent::SetEncodingFactory(BEREncodingFactory::GetInstance());
	}

	~TestWithEvents() {
		IEvent::SetEncodingFactory(NULL);
	}

	static const NETID CLIENT_NETID = 229792458;
	static const ULONG CHANNEL_ID = 0x17;
	static const ULONG ZONE_TYPE = 3;
	static const LONG ZONE_ID = (ZONE_TYPE << 28) | CHANNEL_ID;
	static const LONG INSTANCE_ID = 142857;
};

class GameStateManagerTestBase : public TestWithEvents {
protected:
	GameStateManagerTestBase() :
			TestWithEvents() {
		POS[0] = Vector3d(0, 0, 0);
		DIR[0] = Vector3d(1, 0, 0);

		POS[1] = Vector3d(0.51, 2.23, 355.13);
		DIR[1] = Vector3d(0.766, 0, 0.643);

		POS[2] = Vector3d(100, 0, 0);
		DIR[2] = Vector3d(0.643, 0, 0.766);

		POS[3] = Vector3d(3.26, 1.1, 360.88);
		DIR[3] = Vector3d(0.643, 0, 0.766);

		UP = Vector3d(0, 1, 0);

		for (int i = 0; i < 4; i++) SLIDE[i] = UP.Cross(DIR[i]);
	}

	Vector3d POS[4], DIR[4], UP, SLIDE[4];

	static int GLOBAL_ID[4];
	static ULONG OBJECT_ID[4];
	static int GAMEITEM_ID[4];
	static UINT MOVE_DURATION1;
	static UINT ROTATE_DURATION1;
};

int GameStateManagerTestBase::GLOBAL_ID[4] = { 3141592, 3141593, 3141594, 3141595 };
ULONG GameStateManagerTestBase::OBJECT_ID[4] = { 11235813, 23581324, 58132437, 13243761 };
int GameStateManagerTestBase::GAMEITEM_ID[4] = { 618, 619, 620, 621 };
UINT GameStateManagerTestBase::MOVE_DURATION1 = 1000;
UINT GameStateManagerTestBase::ROTATE_DURATION1 = 500;

class LuaTest : public GameStateManagerTestBase {
protected:
	LuaTest() :
			vm(NULL) {}

	void SetUp() {
		vm = luaL_newstate();
	}

	void TearDown() {
		lua_close(vm);
		vm = NULL;
	}

	lua_State* vm;
};

#include "GameStateManagerTest.h"
#include "LuaAdapterTest.h"
#include "LuaSharedDataTest.h"
#include "LuaYieldTest.h"
#include "JsonConfigTest.h"