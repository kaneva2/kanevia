///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Server/ScriptServer/GameStateManager.h"
#include "Event/Events/ScriptClientEvent.h"
#include "log4cplus/logger.h"

class ScriptClientEventTest : public GameStateManagerTestBase {};

TEST_F(ScriptClientEventTest, ObjectGenerate) {
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_Sound, GAMEITEM_ID[0]);

	ULONG objId = 0, globalId = 0;
	LONG zoneId = 0, instanceId = 0;
	Vector3d pos, dir;
	int placementType = 0, gameItemId = 0;
	ScriptClientEvent::DecodeObjectGenerate(e, objId, globalId, zoneId, instanceId,
		pos[0], pos[1], pos[2], dir[0], dir[1], dir[2], placementType, gameItemId);

	EXPECT_EQ(objId, OBJECT_ID[0]);
	EXPECT_EQ(zoneId, ZONE_ID);
	EXPECT_EQ(instanceId, INSTANCE_ID);
	EXPECT_EQ(pos, POS[1]);
	EXPECT_EQ(dir, DIR[1]);
	EXPECT_EQ(placementType, Placement_Sound);
	EXPECT_EQ(gameItemId, GAMEITEM_ID[0]);
}

TEST_F(ScriptClientEventTest, ObjectMove) {
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);

	ULONG objId = 0;
	LONG zoneId = 0, instanceId = 0;
	Vector3d pos;
	double duration;
	double accDur, constVelDur, accDistFrac, constVelDistFrac;
	ScriptClientEvent::DecodeObjectMove(e, objId, zoneId, instanceId, pos[0], pos[1], pos[2], duration, accDur, constVelDur, accDistFrac, constVelDistFrac);

	EXPECT_EQ(objId, OBJECT_ID[0]);
	EXPECT_EQ(zoneId, ZONE_ID);
	EXPECT_EQ(instanceId, INSTANCE_ID);
	EXPECT_EQ(pos, POS[1]);
	EXPECT_EQ(duration, MOVE_DURATION1);
}

TEST_F(ScriptClientEventTest, ObjectRotate) {
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectRotate(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[1][0], DIR[1][1], DIR[1][2], SLIDE[1][0], SLIDE[1][1], SLIDE[1][2], MOVE_DURATION1);

	ULONG objId = 0;
	LONG zoneId = 0, instanceId = 0;
	Vector3d dir, slide;
	double duration;
	ScriptClientEvent::DecodeObjectRotate(e, objId, zoneId, instanceId, dir[0], dir[1], dir[2], slide[0], slide[1], slide[2], duration);

	EXPECT_EQ(objId, OBJECT_ID[0]);
	EXPECT_EQ(zoneId, ZONE_ID);
	EXPECT_EQ(instanceId, INSTANCE_ID);
	EXPECT_EQ(dir, DIR[1]);
	EXPECT_EQ(slide, SLIDE[1]);
	EXPECT_EQ(duration, MOVE_DURATION1);
}

class GameStateModifierTest : public GameStateManagerTestBase {};

TEST_F(GameStateModifierTest, Copy) {
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);

	GameStateModifier mod1(ScriptClientEvent::ObjectMove, 0, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	mod1.push(e, 0);

	GameStateModifier mod2 = mod1;

	ScriptClientEvent *e1 = NULL, *e2 = NULL;
	mod1.get(e1);
	mod2.get(e2);

	ULONG objId = 0;
	LONG zoneId = 0, instanceId = 0;
	Vector3d pos;
	double duration;
	double accDur, constVelDur, accDistFrac, constVelDistFrac;
	ScriptClientEvent::DecodeObjectMove(e2, objId, zoneId, instanceId, pos[0], pos[1], pos[2], duration, accDur, constVelDur, accDistFrac, constVelDistFrac);

	// e1 ref-eq e
	EXPECT_EQ(e1, e);

	// e2 eq e
	EXPECT_EQ(e2->GetEventType(), e->GetEventType());
	EXPECT_EQ(e2->From(), e->From());
	EXPECT_EQ(objId, OBJECT_ID[0]);
	EXPECT_EQ(zoneId, ZONE_ID);
	EXPECT_EQ(instanceId, INSTANCE_ID);
	EXPECT_EQ(pos, POS[1]);
	EXPECT_EQ(duration, MOVE_DURATION1);

	// timestamp
	EXPECT_EQ(mod1.timestamp(), mod2.timestamp());
}

class GameStateManagerTest : public GameStateManagerTestBase {
protected:
	// Setup GameStateManager instance
	virtual void SetUp() {
		GameStateManagerTestBase::SetUp();

		auto logger = log4cplus::Logger::getInstance(L"Test");

		zi = ZoneIndex(CHANNEL_ID, INSTANCE_ID, 3); // Potential Side Effect Issue: This zone index will be recorded in ZoneIndex class and potentially affects other tests

		gsm = new GameStateManager(zi, logger, std::set<ULONG>());
	}

	virtual void TearDown() {
		delete gsm;

		GameStateManagerTestBase::TearDown();
	}

	void ExpectObjectGenerate(GameStateModifier *mod, ULONG objId, ULONG globalId, LONG zoneId, LONG instanceId, const Vector3d &pos, const Vector3d &dir, int placementType, int gameItemId) {
		ASSERT_TRUE(mod != NULL);
		EXPECT_TRUE(mod->validate(objId, zoneId, instanceId));

		ScriptClientEvent *e = NULL;
		mod->get(e);
		ASSERT_TRUE(e != NULL);

		EXPECT_EQ(e->GetEventType(), ScriptClientEvent::ObjectGenerate);

		ULONG globalId1, objId1;
		LONG zoneId1, instanceId1;
		int placementType1, gameItemId1;
		double x1, y1, z1, dx1, dy1, dz1;

		ScriptClientEvent::DecodeObjectGenerate(e, objId1, globalId1, zoneId1, instanceId1, x1, y1, z1, dx1, dy1, dz1, placementType1, gameItemId1);

		Vector3d pos1(x1, y1, z1);
		Vector3d dir1(dx1, dy1, dz1);

		EXPECT_EQ(objId, objId1);
		EXPECT_EQ(placementType, placementType1);
		EXPECT_EQ(gameItemId, gameItemId1);
		EXPECT_EQ(globalId, globalId1);
		EXPECT_EQ(zoneId, zoneId1);
		EXPECT_EQ(instanceId, instanceId1);
		EXPECT_NEAR(pos[0], pos1[0], 0.1);
		EXPECT_NEAR(pos[1], pos1[1], 0.1);
		EXPECT_NEAR(pos[2], pos1[2], 0.1);
		EXPECT_NEAR(dir[0], dir1[0], 0.01);
		EXPECT_NEAR(dir[1], dir1[1], 0.01);
		EXPECT_NEAR(dir[2], dir1[2], 0.01);
	}

	void ExpectObjectMove(GameStateModifier *mod, ULONG objId, LONG zoneId, LONG instanceId, const Vector3d &pos, double time) {
		ASSERT_TRUE(mod != NULL);
		EXPECT_TRUE(mod->validate(objId, zoneId, instanceId));

		ScriptClientEvent *e = NULL;
		mod->get(e);
		ASSERT_TRUE(e != NULL);

		ASSERT_EQ(e->GetEventType(), ScriptClientEvent::ObjectMove);

		ULONG objId1;
		LONG zoneId1, instanceId1;
		double x1, y1, z1, time1;
		double accDur, constVelDur, accDistFrac, constVelDistFrac;
		ScriptClientEvent::DecodeObjectMove(e, objId1, zoneId1, instanceId1, x1, y1, z1, time1, accDur, constVelDur, accDistFrac, constVelDistFrac);

		Vector3d pos1(x1, y1, z1);

		EXPECT_EQ(objId, objId1);
		EXPECT_EQ(zoneId, zoneId1);
		EXPECT_EQ(instanceId, instanceId1);
		EXPECT_NEAR(pos[0], pos1[0], 0.1);
		EXPECT_NEAR(pos[1], pos1[1], 0.1);
		EXPECT_NEAR(pos[2], pos1[2], 0.1);
		EXPECT_NEAR(time, time1, 20);
	}

	void ExpectObjectRotate(GameStateModifier *mod, ULONG objId, LONG zoneId, LONG instanceId, const Vector3d &dir, const Vector3d &slide, double time) {
		ASSERT_TRUE(mod != NULL);
		EXPECT_TRUE(mod->validate(objId, zoneId, instanceId));

		ScriptClientEvent *e = NULL;
		mod->get(e);
		ASSERT_TRUE(e != NULL);

		ASSERT_EQ(e->GetEventType(), ScriptClientEvent::ObjectRotate);

		ULONG objId1;
		LONG zoneId1, instanceId1;
		double dx1, dy1, dz1, sx1, sy1, sz1, time1;
		ScriptClientEvent::DecodeObjectRotate(e, objId1, zoneId1, instanceId1, dx1, dy1, dz1, sx1, sy1, sz1, time1);

		Vector3d dir1(dx1, dy1, dz1);
		Vector3d slide1(sx1, sy1, sz1);

		EXPECT_EQ(objId, objId1);
		EXPECT_EQ(zoneId, zoneId1);
		EXPECT_EQ(instanceId, instanceId1);
		EXPECT_NEAR(dir[0], dir1[0], 0.01);
		EXPECT_NEAR(dir[1], dir1[1], 0.01);
		EXPECT_NEAR(dir[2], dir1[2], 0.01);
		EXPECT_NEAR(slide[0], slide1[0], 0.01);
		EXPECT_NEAR(slide[1], slide1[1], 0.01);
		EXPECT_NEAR(slide[2], slide1[2], 0.01);
		EXPECT_NEAR(time, time1, 20);
	}

	void ExpectSilentModifier(GameStateModifier *mod, ULONG objId, LONG zoneId, LONG instanceId) {
		ASSERT_TRUE(mod != NULL);
		EXPECT_TRUE(mod->validate(objId, zoneId, instanceId));

		ScriptClientEvent *e = NULL;
		mod->get(e);
		EXPECT_TRUE(e == NULL);
	}

	ZoneIndex zi;
	GameStateManager *gsm;
};

TEST_F(GameStateManagerTest, GeneratedObjects) {
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);
	EXPECT_EQ(gameStates.size(), 1);

	auto itrList = gameStates.begin();
	ASSERT_TRUE(itrList != gameStates.end());

	GameStateModifierMap &os = itrList->second;
	EXPECT_EQ(os.size(), 3);

	auto modGen = os.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov = os.getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot = os.getModifier(ScriptClientEvent::ObjectRotate, 0);

	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectSilentModifier(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);

	os.bake();
	EXPECT_EQ(os.size(), 3);

	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectSilentModifier(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
}

TEST_F(GameStateManagerTest, BROKEN_GenAndMove) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL,
		OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID,
		POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2],
		Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectMove event with duration
	e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2][0], POS[2][1], POS[2][2], MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// Make a copy of the current game state
	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);

	// Verify
	EXPECT_EQ(gameStates.size(), 1); // Exactly one game object inserted
	auto itrList = gameStates.begin();
	ASSERT_TRUE(itrList != gameStates.end());

	GameStateModifierMap &os = itrList->second; // ObjectState
	GameStateModifierMap os2 = os; // Make a copy of the ObjectState so that we can test in two different ways

	EXPECT_EQ(os.size(), 3); // 3 Modifiers so far: ObjectGenerate, ObjectMove and ObjectRotate
	EXPECT_EQ(os2.size(), 3);

	// Retrieve all modifiers
	auto modGen = os.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov = os.getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot = os.getModifier(ScriptClientEvent::ObjectRotate, 0);
	auto modGen2 = os2.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov2 = os2.getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot2 = os2.getModifier(ScriptClientEvent::ObjectRotate, 0);

	// Verify event parameters for ObjectGenerate and ObjectMove. ObjectRotate modifier should have no event.
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectObjectMove(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2], MOVE_DURATION1);
	ExpectSilentModifier(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);

	// ObjectState2 should be a perfect copy at this moment
	ExpectObjectGenerate(modGen2, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectObjectMove(modMov2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2], MOVE_DURATION1);
	ExpectSilentModifier(modRot2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);

	// Bake ObjectState
	os.bake();

	// Verify baked ObjectState
	EXPECT_EQ(os.size(), 3); // Still 3 modifiers

	// All modifiers should remain the same (roughly) as ObjectMove transition has very little effect with the amount of the time elapsed.
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectObjectMove(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[2], MOVE_DURATION1);
	ExpectSilentModifier(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);

	// Wait for ObjectMove transition period to elapse
	Sleep(MOVE_DURATION1 + 200);

	// Bake ObjectState2
	os2.bake();

	// Verify baked ObjectState2
	EXPECT_EQ(os2.size(), 3); // Still 3 modifiers

	// ObjectGenerate should now gets the final position inherited from ObjectMove. ObjectMove should be suppressed now.
	ExpectObjectGenerate(modGen2, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[2], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectSilentModifier(modRot2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
}

TEST_F(GameStateManagerTest, BROKEN_GenAndMove2) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectMove event with duration
	e = ScriptClientEvent::EncodeObjectMove(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[3][0], POS[3][1], POS[3][2], MOVE_DURATION1, 0, MOVE_DURATION1, 0, 1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// Make a copy of the current game state
	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);

	// Verify
	EXPECT_EQ(gameStates.size(), 1); // Exactly one game object inserted
	auto itrList = gameStates.begin();
	ASSERT_TRUE(itrList != gameStates.end());

	GameStateModifierMap &os = itrList->second; // ObjectState

	EXPECT_EQ(os.size(), 3); // 3 Modifiers so far: ObjectGenerate, ObjectMove and ObjectRotate

	// Retrieve all modifiers
	auto modGen = os.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov = os.getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot = os.getModifier(ScriptClientEvent::ObjectRotate, 0);

	// Wait for 1/4 of the ObjectMove transition period to elapse
	Sleep(MOVE_DURATION1 / 4);

	// Bake ObjectState
	os.bake();

	// Verify baked ObjectState
	EXPECT_EQ(os.size(), 3); // Still 3 modifiers

	// Roughtly 1/3 between pos1 and pos2
	Vector3d pos = POS[1] * 3 / 4 + POS[3] / 4;
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, pos, DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectObjectMove(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, POS[3], MOVE_DURATION1 * 3 / 4);
	ExpectSilentModifier(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
}

TEST_F(GameStateManagerTest, GenAndRotate) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectRotate event with duration
	Vector3d slide = UP.Cross(DIR[2]);
	e = ScriptClientEvent::EncodeObjectRotate(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2][0], DIR[2][1], DIR[2][2], slide[0], slide[1], slide[2], ROTATE_DURATION1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// Make a copy of the current game state
	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);

	// Verify
	EXPECT_EQ(gameStates.size(), 1); // Exactly one game object inserted
	auto itrList = gameStates.begin();
	ASSERT_TRUE(itrList != gameStates.end());

	GameStateModifierMap &os = itrList->second; // ObjectState
	GameStateModifierMap os2 = os; // Make a copy of the ObjectState so that we can test in two different ways

	EXPECT_EQ(os.size(), 3); // 3 Modifiers so far: ObjectGenerate, ObjectMove and ObjectRotate
	EXPECT_EQ(os2.size(), 3);

	// Retrieve all modifiers
	auto modGen = os.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov = os.getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot = os.getModifier(ScriptClientEvent::ObjectRotate, 0);
	auto modGen2 = os2.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov2 = os2.getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot2 = os2.getModifier(ScriptClientEvent::ObjectRotate, 0);

	// Verify event parameters for ObjectGenerate and ObjectRotate. ObjectMove modifier should have no event.
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectObjectRotate(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2], slide, ROTATE_DURATION1);

	// ObjectState2 should be a perfect copy at this moment
	ExpectObjectGenerate(modGen2, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectObjectRotate(modRot2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2], slide, ROTATE_DURATION1);

	// Bake ObjectState
	os.bake();

	// Verify baked ObjectState
	EXPECT_EQ(os.size(), 3); // Still 3 modifiers

	// All modifiers should remain the same (roughly) as ObjectRotate transition has very little effect with the amount of the time elapsed.
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectObjectRotate(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2], slide, ROTATE_DURATION1);

	// Wait for ObjectRotate transition period to elapse
	Sleep(ROTATE_DURATION1 + 200);

	// Bake ObjectState2
	os2.bake();

	// Verify baked ObjectState2
	EXPECT_EQ(os2.size(), 3); // Still 3 modifiers

	// ObjectGenerate should now gets the final rotation inherited from ObjectRotate. ObjectRotate should be suppressed now.
	ExpectObjectGenerate(modGen2, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[2], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectSilentModifier(modRot2, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
}

TEST_F(GameStateManagerTest, BROKEN_GenAndRotate2) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectRotate event with duration
	Vector3d slide = UP.Cross(DIR[2]);
	e = ScriptClientEvent::EncodeObjectRotate(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2][0], DIR[2][1], DIR[2][2], slide[0], slide[1], slide[2], ROTATE_DURATION1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// Make a copy of the current game state
	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);

	// Verify
	EXPECT_EQ(gameStates.size(), 1); // Exactly one game object inserted
	auto itrList = gameStates.begin();
	ASSERT_TRUE(itrList != gameStates.end());

	GameStateModifierMap &os = itrList->second; // ObjectState

	EXPECT_EQ(os.size(), 3); // 3 Modifiers so far: ObjectGenerate, ObjectMove and ObjectRotate

	// Retrieve all modifiers
	auto modGen = os.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov = os.getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot = os.getModifier(ScriptClientEvent::ObjectRotate, 0);

	// Verify event parameters for ObjectGenerate and ObjectRotate. ObjectMove modifier should have no event.
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectObjectRotate(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2], slide, ROTATE_DURATION1);

	// Wait for 1/4 of the ObjectRotate transition period to elapse
	Sleep(ROTATE_DURATION1 * 3 / 4);

	// Bake ObjectState
	os.bake();

	// Verify baked ObjectState2
	EXPECT_EQ(os.size(), 3); // Still 3 modifiers

	// ObjectGenerate should now gets the final position inherited from ObjectRotate. ObjectRotate should be suppressed now.
	Vector3d dir = DIR[1] * 1 / 4 + DIR[2] * 3 / 4;
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], dir, Placement_DynObj, INVALID_GAMEITEM);
	ExpectSilentModifier(modMov, OBJECT_ID[0], ZONE_ID, INSTANCE_ID);
	ExpectObjectRotate(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2], slide, ROTATE_DURATION1 * 1 / 4);
}

TEST_F(GameStateManagerTest, EventOrdering) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectRotate event with duration
	Vector3d slide = UP.Cross(DIR[2]);
	e = ScriptClientEvent::EncodeObjectRotate(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2][0], DIR[2][1], DIR[2][2], slide[0], slide[1], slide[2], ROTATE_DURATION1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// Make a copy of the current game state
	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);

	// Verify
	EXPECT_EQ(gameStates.size(), 1); // Exactly one game object inserted
	auto itrList = gameStates.begin();
	ASSERT_TRUE(itrList != gameStates.end());

	GameStateModifierMap &os = itrList->second; // ObjectState

	EXPECT_EQ(os.size(), 3); // 3 Modifiers so far: ObjectGenerate, ObjectMove and ObjectRotate

	// Retrieve all modifiers
	GameStateModifierMap::OrderedQueue que;
	os.getSorted(que);

	// Verify queue order

	// First one must be ObjectGenerate
	GameStateModifier *modGen;
	ASSERT_TRUE(!que.empty());

	modGen = que.top();
	ASSERT_TRUE(modGen != NULL);
	{
		ScriptClientEvent *e2 = NULL;
		modGen->get(e2);
		ASSERT_TRUE(e2 != NULL);
		EXPECT_EQ(e2->GetEventType(), ScriptClientEvent::ObjectGenerate);
	}

	que.pop();

	TimeMs lastTimestamp = modGen->timestamp();

	// The remaining must be ordered by time and not ObjectGenerate.
	while (!que.empty()) {
		GameStateModifier *mod = que.top();
		ASSERT_TRUE(mod != NULL);

		ScriptClientEvent *e2 = NULL;
		mod->get(e2);

		if (e2) {
			EXPECT_NE(e2->GetEventType(), ScriptClientEvent::ObjectGenerate);
			EXPECT_LE(lastTimestamp, mod->timestamp());

			lastTimestamp = mod->timestamp();
		}

		que.pop();
	}
}

TEST_F(GameStateManagerTest, RemoveObject) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// ObjectRotate event with duration
	Vector3d slide = UP.Cross(DIR[2]);
	e = ScriptClientEvent::EncodeObjectRotate(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2][0], DIR[2][1], DIR[2][2], slide[0], slide[1], slide[2], ROTATE_DURATION1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// Make a copy of the current game state
	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);
	EXPECT_EQ(gameStates.size(), 1);

	gsm->RemoveGameStateObject(zi, OBJECT_ID[0]);
	EXPECT_TRUE(gsm->IsEmpty());
}

// This is to make sure game state manager updates modifier timestamp properly
TEST_F(GameStateManagerTest, TimestampUpdate) {
	// ObjectGenerate event
	ScriptClientEvent *e = ScriptClientEvent::EncodeObjectGenerate(NULL, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1][0], POS[1][1], POS[1][2], DIR[1][0], DIR[1][1], DIR[1][2], Placement_DynObj, INVALID_GAMEITEM);
	gsm->AddGameStateObject(zi, e, OBJECT_ID[0]);

	// If timestamp of rotation modifier is not properly update by the second event, we will see some discrepancies on the rotation state
	Sleep(ROTATE_DURATION1 + 200);

	// ObjectRotate event with duration
	Vector3d slide = UP.Cross(DIR[2]);
	e = ScriptClientEvent::EncodeObjectRotate(NULL, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2][0], DIR[2][1], DIR[2][2], slide[0], slide[1], slide[2], ROTATE_DURATION1);
	gsm->AddGameStateObjectModifier(zi, e, OBJECT_ID[0]);

	// Make a copy of the current game state
	GameStateManager::GameStateMap gameStates = gsm->GetZoneGameState(zi);

	// Verify
	EXPECT_EQ(gameStates.size(), 1);
	auto itrList = gameStates.begin();
	ASSERT_TRUE(itrList != gameStates.end());

	GameStateModifierMap &os = itrList->second; // ObjectState

	EXPECT_EQ(os.size(), 3); // 3 Modifiers so far: ObjectGenerate, ObjectMove and ObjectRotate

	// Bake ObjectState
	os.bake();

	// Retrieve modifiers
	auto modGen = os.getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modRot = os.getModifier(ScriptClientEvent::ObjectRotate, 0);

	// We should see the original ObjectGenerate and ObjectRotate event -- in case timestamp not getting updated, the event should have elapsed and the rotation should have been merged into ObjectGenerate
	ExpectObjectGenerate(modGen, OBJECT_ID[0], GLOBAL_ID[0], ZONE_ID, INSTANCE_ID, POS[1], DIR[1], Placement_DynObj, INVALID_GAMEITEM);
	ExpectObjectRotate(modRot, OBJECT_ID[0], ZONE_ID, INSTANCE_ID, DIR[2], slide, ROTATE_DURATION1);
}
