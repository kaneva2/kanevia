///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Event\Events\ScriptServerEvent.h"
#include "ScriptWebCallTask.h"
#include "LuaAsyncFunc.h"
#include "IDispatcher.h"
#include "HttpClient\GetBrowserPage.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

ScriptWebCallTask::ScriptWebCallTask(
	IDispatcher* dispatcher,
	//	log4cplus::Logger& logger,
	std::string url,
	unsigned int placementId,
	ZoneIndex zoneId,
	bool usePost,
	void* pVM,
	bool async,
	const AsyncUserArg& userArg) :
		ScriptServerTaskObject("ScriptWebCallTask"),
		m_dispatcher(dispatcher),
		//	m_logger(logger),
		m_url(url),
		m_placementId(placementId),
		m_zoneId(zoneId),
		m_usePost(usePost),
		m_vm(pVM),
		m_async(async),
		m_userArg(userArg) {
}

void ScriptWebCallTask::ExecuteTask() {
	// make the web call
	std::string resultXML;
	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;

	if (m_usePost) {
		if (!PostBrowserPage(m_url, resultXML, httpStatusCode, httpStatusDescription)) {
			LogError("PostBrowserPage() FAILED - '" << m_url << "' objId=" << m_placementId);
		}
	} else {
		size_t resultSize = 0;
		if (!GetBrowserPage(m_url, resultXML, resultSize, httpStatusCode, httpStatusDescription)) {
			LogError("GetBrowserPage() FAILED - '" << m_url << "' objId=" << m_placementId);
		}
	}

	// send the results back to script server
	ScriptServerEvent* sse = new ScriptServerEvent(m_async ? ScriptServerEvent::ScriptMakeWebcallAsync : ScriptServerEvent::ScriptMakeWebcall, m_placementId, m_zoneId.toLong(), m_zoneId.GetInstanceId());
	*sse->OutBuffer() << m_userArg << httpStatusCode << httpStatusDescription << resultXML;
	m_dispatcher->QueueEvent(sse);
}

} // namespace KEP