///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

#include "ScriptServerTaskObject.h"

namespace KEP {

class ServerScript;
class Scheduler;
typedef std::shared_ptr<ServerScript> ServerScriptPtr;

/*!
* Represents a VM.  The this is what's scheduled
* now, not the VM by itself, so that it can be
* processed by a thread pool.
*/
class RunVMTask : public ScriptServerTaskObject {
public:
	RunVMTask(
		//		log4cplus::Logger& logger,
		Scheduler* pScheduler,
		const ServerScriptPtr& script,
		bool isNewTask,
		int numberOfArgs);

	virtual void ExecuteTask() override;

private:
	//	log4cplus::Logger& m_logger;
	Scheduler* m_pScheduler;
	ServerScriptPtr m_pScript;
	bool m_isNewTask;
	int m_numberOfArgs;
};

}; // namespace KEP
