/******************************************************************************
 resource.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AIEngine.rc
//
#define IDS_AIENGINE_NAME               3200
#define IDS_DIFFERENT_CHANNEL           3201
#define IDS_STARTSPAWNGEN_NOT_FOUND     3202
#define IDS_STOPPAWNGEN_NOT_FOUND       3203

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2001
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2000
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
