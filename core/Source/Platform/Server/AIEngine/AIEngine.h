#pragma once

#include "common/include/kepcommon.h"

#if (DRF_AI_ENGINE == 1)

#include "common/keputil/monitored.h"
#include "Engine/ClientBase/ClientBaseEngine.h"
#include "Event/Events/AIWorldsEvent.h"

#include <log4cplus/logger.h>
using namespace log4cplus;

#include "InstanceId.h"
#include "IAIBot.h"
#include "IAIEngine.h"

#include <d3d9.h>
#include <vector>

#include "MatrixArb.h"

class CAIWebObjList;
class CSpawnObjList;
class WorldAvailableObjList;
class CAIObj;
class CPersistObjList;

class AIEngine : public ClientBaseEngine, IAIEngine {
public:
	AIEngine();

	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }

	bool WebGetPassGroups(const string & /*username*/, int & /*passGroupsUser*/) { return false; }
	bool WebGetEntityCfg(const string & /*username*/, Vector3d & /*nameColor*/, int & /*passGroupsUser*/, int & /*passGroupsWorld*/) { return false; }

	bool LoadConfigValues(const IKEPConfig *cfg);
	void SaveConfigValues(IKEPConfig *cfg);
	bool Initialize(const char *baseDir, const char *configFile, const DBConfig &dbConfig);
	BOOL AppendScene(CStringA fileName, int aiMergeMode = 1, int groupMode = -1, int transitionGroupID = -1);
	void FreeAll();
	UINT RenderLoop(bool canWait);
	HRESULT doAction(const char *objectId, TiXmlElement *actionNode, int id, int depth, Logger &logger, INOUT string &result);

	void GetPossibleTargets(CMovementObj *ptrLoc, OUT AITargetVector &targets);

protected:
	//1 overrides
	bool InitEvents();
	bool ProcessScriptAttribute(CEXScriptObj *sctPtr);
	BOOL HandleEngineMessages(LPVOID lpMessage, NETID idFromLoc, int msgSize, OUT bool &bDisposeMessage);
	void DestroyRuntimeEntity(CMovementObj *runtimeEntity);
	void UpdateServerCallback();
	bool GenAndMount(CMovementObj *hostPtr, int aiIndex, int mountType = MT_RIDE, bool bLocal = false, OUT CMovementObj **outNewEntity = NULL);
	BOOL RealtimeSegmentTest(const Vector3f &lastPos, const Vector3f &curPos, Vector3f *intersect, Vector3f *normal, int *polysChecked, int *polysConsidered);
	void CollisionLoopEntityToEntity() {} // Does nothing for AI engine
	void RealtimeEntityCollisionCallback();

	bool IsRenderingSupported() const { return false; }

protected:
	bool Connect();

	//2 Implementation in AIEngine.cpp
	void SpawnGeneratorCallback(); // usually utilized by server only

	//2 virt fn called from UniversalControlUnit
	BOOL AI_Transform(CMovementObj *ptrLoc);
	BOOL AI_Loop(CMovementObj *ptrLoc);
	//3 called from AI_LOOP
	void MaintainTarget(CMovementObj *ptrLoc);
	void AI_HandleScript(CMovementObj *ptrLoc);
	void AI_TreeWebUse(CMovementObj *ptrLoc);
	BOOL AI_Attack(CMovementObj *ptrLoc, CAIObj *AIPtr, TimeMs currentTime);
	BOOL AI_Retreat(CMovementObj *ptrLoc, CAIObj *AIPtr);
	void EvaluateAIVisionToTargetDest(CMovementObj *ptrLoc, TimeMs currentTime);
	BOOL AI_targetInSight(CMovementObj *ptrLoc, float maxDistance);
	BOOL AI_TravelLookBasis(CMovementObj *ptrLoc, CAIObj *AIPtr, TimeMs currentTime);
	void AI_MoveToTarget(CMovementObj *ptrLoc, CAIObj *AIPtr);

	BOOL AI_getTarget(CMovementObj *ptrLoc, int targetRetrievalType);
	BOOL AI_VerifyTarget(CMovementObj *ptrLoc);
	BOOL RealtimeSegmentTestUseTable(Vector3f &lastPos, Vector3f &curPos);
	void AdvancedAiSteering(CMovementObj *ptrLoc, Vector3f locationLoc);
	BOOL LoadScene(const CStringA &fileName, const char *url = 0);
	void DeleteSpawnDatabase();
	bool SpawnEntityBasedOnPositionAndCfg(Vector3f position, int aiCfg, int dynamicSpawnId, long lifeMaxTime, ZoneIndex zoneIndex = ZoneIndex(-1L));
	void UpdateEntityPersist(CPersistObjList **pList);

	virtual IGetSet *GetEnvironment();
	IGetSet *Values(void) { return this; }

	DECLARE_GETSET

	EVENT_PROC_RC HandleLogonResponseEvent(const char *connectionErrorCode, HRESULT hResultCode, ULONG kanevaUserId, LOGON_RET replyCode);
	static EVENT_PROC_RC SpawnAIEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e);

	//2 Variables
	CAIWebObjList *GL_aiWebDatabase;

	CSpawnObjList *spawnGeneratorDB;

	TimeMs GL_spawnGeneratorTimeVarial;

	string m_password; // set on GL_multiplayerObject when we get it
	string m_userName; // set on GL_multiplayerObject when we get it

	bool m_connectSet;
	ULONG m_maxLoopTime;
	ULONG m_aiThrottle;
	AIWorlds m_loadedScenes;
	CWorldAvailableObjList *m_supportedWorlds;
	ChannelId m_channel;
	bool m_bAllowSafeZoneAttack; //GKF expose to editor
	bool m_hasAIBotUpdateHandler;

	friend class AIBot;
};

#endif
