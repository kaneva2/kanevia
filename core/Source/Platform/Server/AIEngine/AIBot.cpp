#include "stdafx.h"
#include "AIBot.h"

#if (DRF_AI_ENGINE == 1)

#include "CMovementObj.h"
#include "AIEngine.h"
#include "AIclass.h"
#include "CPhysicsEmuClass.h"
#include "CSkeletalClass.h"
#include "RuntimeSkeleton.h"

//  tells the server to set the AI to decay
void AIBot::SetDissipateFlag() {
	m_movementObj->m_controlType = CMovementObj::CONTROL_TYPE_DEAD_999;
}

// return dpnid, netid, name, distance, highest skill, team, race, clan, edb, ai_controlled, etc..
void AIBot::GetPossibleTargets(OUT AITargetVector &targets) {
	m_engine->GetPossibleTargets(m_movementObj, targets);
}

//  AI looks at a position
void AIBot::LookAt(IN double x, IN double y, IN double z) {
	// mainly copied from CAIObj::AI_Attack
	float actualHeight = (float)y;

	float distFromTarget = Distance(m_movementObj->m_location, Vector3f(x, y, z));

	float angle = 0.0f;
	if (distFromTarget != 0.0f) {
		angle = (float)(atan(actualHeight / distFromTarget) * 57.3); // (180/3.1415926535897932);
		if (m_movementObj && m_movementObj->m_runtimeSkeleton) {
			m_movementObj->m_runtimeSkeleton->ApplyFreeLookRotation(angle, 0.0f);
		}

		m_movementObj->m_movementDB->m_freeLookCurPitch = angle;
	}
}

//  checks the collision to see if they can see this target
bool AIBot::IsMyTargetInSight() {
	m_movementObj->m_aiObj->m_targetInSight = m_engine->AI_targetInSight(m_movementObj, min(m_movementObj->m_aiObj->m_safeDistance, m_movementObj->m_aiObj->m_maxSightRadius));
	return m_movementObj->m_aiObj->m_targetInSight != FALSE;
}

// set my target
void AIBot::SetTarget(IN IGetSet *target, bool insight) {
	CMovementObj *obj = dynamic_cast<CMovementObj *>(target);
	jsVerifyDo(obj != 0, return );
	m_movementObj->SetTarget(obj, insight);
}

// clear my target
void AIBot::ClearTarget() {
	m_movementObj->ClearTarget();
}

//  sets the AI in motion to a location
void AIBot::MoveToLocation(IN double x, IN double y, IN double z) {
	m_movementObj->m_AIdestination.x = (float)x;
	m_movementObj->m_AIdestination.y = (float)y;
	m_movementObj->m_AIdestination.z = (float)z;
}

//  sets the AI in motion to a target
void AIBot::MoveToCurrentTarget() {
	m_engine->AI_MoveToTarget(m_movementObj, m_movementObj->m_aiObj);
}

//  sets the AI in motion to retreat from a target
void AIBot::RetreatFromCurrentTarget() {
	if (!m_movementObj->m_aiObj->m_retreating) {
		m_movementObj->m_aiObj->m_retreatStamp = fTime::TimeMs();
		m_movementObj->m_aiObj->m_retreating = TRUE;
	}

	m_engine->AI_Retreat(m_movementObj, m_movementObj->m_aiObj); // ok to retreat
}

//  returns the x,y,z of a AI tree node, -1,-1,-1 if not found
void AIBot::GetTreeNodePosition(IN LONG nodeIndex, OUT double &x, OUT double &y, OUT double &z) {
	x = y = z = -1.0;

	POSITION posWeb = m_engine->GL_aiWebDatabase->FindIndex(m_movementObj->m_aiObj->m_curMap);
	if (posWeb) {
		CAIWebObj *webObj = (CAIWebObj *)m_engine->GL_aiWebDatabase->GetAt(posWeb);
		POSITION trePos = webObj->m_web->FindIndex(nodeIndex);

		if (trePos) {
			CAIWebNode *node = (CAIWebNode *)webObj->m_web->GetAt(trePos);
			x = node->m_position.x;
			y = node->m_position.y;
			z = node->m_position.z;
		}
	}
}

//  moves the AI to a desired tree location
bool AIBot::GotoTreeNode(IN LONG nodeIndex) {
	// from AI_HandleScript
	POSITION posWeb = m_engine->GL_aiWebDatabase->FindIndex(m_movementObj->m_aiObj->m_curMap);
	if (posWeb) {
		CAIWebObj *webObj = (CAIWebObj *)m_engine->GL_aiWebDatabase->GetAt(posWeb);

		int count = 0;

		int *curRoute = webObj->GetRoute(m_movementObj->m_aiObj->m_mapCurLocation, nodeIndex, &count);
		if (curRoute != 0) // nodeIndex valid
		{
			m_movementObj->m_aiObj->m_currentRoute = curRoute;
			m_movementObj->m_aiObj->m_routePointCount = count;
			m_movementObj->m_aiObj->m_routeCurTarget = nodeIndex;
			return true;
		}
	}

	return false;
}

double AIBot::CalculateDistance(IN double x1, IN double y1, IN double z1, IN double x2, IN double y2, IN double z2) {
	return Distance(Vector3f(x1, y1, z1), Vector3f(x2, y2, z2));
}

//  strafes and zig zags to a location
void AIBot::MoveToLocationDefensively(IN double x, IN double y, IN double z) {
	jsAssert(false); // TODO:
}

// added to avoid tons of script coding
void AIBot::Transform() {
	m_engine->AI_Transform(m_movementObj);
}

void AIBot::SetUseStatesToFalse() {
	m_movementObj->m_movementDB->SetUseStatesToFalse();
}
bool AIBot::VerifyTarget() {
	return m_engine->AI_VerifyTarget(m_movementObj) != FALSE;
}
bool AIBot::OnWall() {
	return m_movementObj->m_aiObj->m_feelerSys && m_movementObj->m_aiObj->m_feelerSys->m_onWallCurrently != FALSE;
}
void AIBot::Attack(TimeMs currentTime) {
	m_engine->AI_Attack(m_movementObj, m_movementObj->m_aiObj, currentTime);
}

void AIBot::Retreat() {
	m_engine->AI_Retreat(m_movementObj, m_movementObj->m_aiObj);
}

void AIBot::TravelLookBasis(TimeMs curTimeMs) {
	m_engine->AI_TravelLookBasis(m_movementObj, m_movementObj->m_aiObj, curTimeMs);
}

void AIBot::TreeWebUse() {
	m_engine->AI_TreeWebUse(m_movementObj);
}

void AIBot::HandleScript() {
	m_engine->AI_HandleScript(m_movementObj);
}

void AIBot::EvaluateAIVisionToTargetDest(TimeMs currentTime) {
	m_engine->EvaluateAIVisionToTargetDest(m_movementObj, currentTime);
}

void AIBot::ClearRoute() {
	delete[] m_movementObj->m_aiObj->m_currentRoute;
	m_movementObj->m_aiObj->m_currentRoute = 0;
}

UINT AIBot::GetNameId() {
	return m_movementObj->GetNameId();
}
Vector3f &AIBot::GetVector3f(IN ULONG index) {
	return m_movementObj->GetVector3f(index);
}
ULONG &AIBot::GetULONG(IN ULONG index) {
	return m_movementObj->GetULONG(index);
}
INT &AIBot::GetINT(IN ULONG index) {
	return m_movementObj->GetINT(index);
}
BOOL &AIBot::GetBOOL(IN ULONG index) {
	return m_movementObj->GetBOOL(index);
}
bool &AIBot::Getbool(IN ULONG index) {
	return m_movementObj->Getbool(index);
}
std::string &AIBot::Getstring(IN ULONG index) {
	return m_movementObj->Getstring(index);
}
IGetSet *AIBot::GetObjectPtr(IN ULONG index) {
	return m_movementObj->GetObjectPtr(index);
}

// return a copy of the string, if it was string or CStringA
std::string AIBot::GetString(IN ULONG index) {
	return m_movementObj->GetString(index);
}
// return a copy of the number as double
double AIBot::GetNumber(IN ULONG index) {
	return m_movementObj->GetNumber(index);
}

// sets
void AIBot::SetNumber(IN ULONG index, IN LONG number) {
	m_movementObj->SetNumber(index, number);
}
void AIBot::SetNumber(IN ULONG index, IN ULONG number) {
	m_movementObj->SetNumber(index, number);
}
void AIBot::SetNumber(IN ULONG index, IN double number) {
	m_movementObj->SetNumber(index, number);
}
void AIBot::SetNumber(IN ULONG index, IN BOOL value) {
	m_movementObj->SetNumber(index, value);
}
void AIBot::SetNumber(IN ULONG index, IN bool value) {
	m_movementObj->SetNumber(index, value);
}
void AIBot::SetString(IN ULONG index, IN const char *s) {
	m_movementObj->SetString(index, s);
}
void AIBot::SetVector3f(IN ULONG index, IN Vector3f v) {
	m_movementObj->SetVector3f(index, v);
}
void AIBot::GetColor(IN ULONG index, OUT float *r, OUT float *g, OUT float *b, OUT float *a) {
	m_movementObj->GetColor(index, r, g, b, a);
}
void AIBot::SetColor(IN ULONG index, IN float r, IN float g, IN float b, IN float a) {
	m_movementObj->SetColor(index, r, g, b, a);
}

// array methods
ULONG AIBot::GetArrayCount(IN ULONG indexOfArray) {
	return m_movementObj->GetArrayCount(indexOfArray);
}
IGetSet *AIBot::GetObjectInArray(IN ULONG indexOfArray, IN ULONG indexInArray) {
	return m_movementObj->GetObjectInArray(indexOfArray, indexInArray);
}

ULONG AIBot::AddNewIntMember(IN const char *name, IN int defaultValue, IN ULONG flags /*= amfTransient*/) {
	return m_movementObj->AddNewIntMember(name, defaultValue, flags);
}
ULONG AIBot::AddNewDoubleMember(IN const char *name, IN double defaultValue, IN ULONG flags /*= amfTransient*/) {
	return m_movementObj->AddNewDoubleMember(name, defaultValue, flags);
}
ULONG AIBot::AddNewStringMember(IN const char *name, IN const char *defaultValue, IN ULONG flags /*= amfTransient*/) {
	return m_movementObj->AddNewStringMember(name, defaultValue, flags);
}
ULONG AIBot::FindIdByName(IN const char *name, OUT EGetSetType *type) {
	return m_movementObj->FindIdByName(name, type);
}

#endif
