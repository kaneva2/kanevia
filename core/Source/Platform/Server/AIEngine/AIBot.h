#pragma once

#include "common/include/kepcommon.h"

#if (DRF_AI_ENGINE == 1)

#include "IAIBot.h"

class AIEngine;

namespace KEP {

class CMovementObj;

/***********************************************************
CLASS

	AIBot 
	
	impl of AI bot

DESCRIPTION
	This wraps some things in the CMovementObj and CAIObj
	that it contained within the CMovementObj

***********************************************************/
class AIBot : public IAIBot {
public:
	AIBot(AIEngine *engine, CMovementObj *movementObj) :
			m_movementObj(movementObj), m_engine(engine) {}

	//  tells the server to set the AI to decay
	virtual void SetDissipateFlag();

	// return dpnid, netid, name, distance, highest skill, team, race, clan, edb, ai_controlled, etc..
	virtual void GetPossibleTargets(OUT AITargetVector &targets);

	//  AI looks at a position
	virtual void LookAt(IN double x, IN double y, IN double z);

	//  checks the collision to see if they can see this target
	virtual bool IsMyTargetInSight();

	// set my target
	virtual void SetTarget(IN IGetSet *target, IN bool insight);

	// clear my target
	virtual void ClearTarget();

	//  sets the AI in motion to a location
	virtual void MoveToLocation(IN double x, IN double y, IN double z);

	//  sets the AI in motion to a target
	virtual void MoveToCurrentTarget();

	//  sets the AI in motion to retreat from a target
	virtual void RetreatFromCurrentTarget();

	//  returns the x,y,z of a AI tree node, -1,-1,-1 if not found
	virtual void GetTreeNodePosition(IN LONG nodeIndex, OUT double &x, OUT double &y, OUT double &z);

	//  moves the AI to a desired tree location
	// returns false if node not found
	virtual bool GotoTreeNode(IN LONG nodeIndex);

	//  does a magnitudesquared on input to get a distance.
	virtual double CalculateDistance(IN double x1, IN double y1, IN double z1, IN double x2, IN double y2, IN double z2);

	//  strafes and zig zags to a location
	virtual void MoveToLocationDefensively(IN double x, IN double y, IN double z);

	// added to avoid tons of script coding
	virtual void Transform();
	virtual void SetUseStatesToFalse();
	virtual bool VerifyTarget();
	virtual bool OnWall();
	virtual void Attack(TimeMs currentTime);
	virtual void Retreat();
	virtual void TravelLookBasis(TimeMs currentTime);
	virtual void TreeWebUse();
	virtual void HandleScript();
	virtual void EvaluateAIVisionToTargetDest(TimeMs currentTime);
	virtual void ClearRoute();

	CMovementObj *m_movementObj;
	AIEngine *m_engine;

	// getset overrides
	virtual UINT GetNameId();
	virtual Vector3f &GetVector3f(IN ULONG index);
	virtual ULONG &GetULONG(IN ULONG index);
	virtual INT &GetINT(IN ULONG index);
	virtual BOOL &GetBOOL(IN ULONG index);
	virtual bool &Getbool(IN ULONG index);
	virtual std::string &Getstring(IN ULONG index);
	virtual IGetSet *GetObjectPtr(IN ULONG index);

	// return a copy of the string, if it was string or CStringA
	virtual std::string GetString(IN ULONG index);
	// return a copy of the number as double
	virtual double GetNumber(IN ULONG index);
	virtual void GetColor(IN ULONG index, OUT float *r, OUT float *g, OUT float *b, OUT float *a = 0);

	// sets
	virtual void SetNumber(IN ULONG index, IN LONG number);
	virtual void SetNumber(IN ULONG index, IN ULONG number);
	virtual void SetNumber(IN ULONG index, IN double number);
	virtual void SetNumber(IN ULONG index, IN BOOL value);
	virtual void SetNumber(IN ULONG index, IN bool value);
	virtual void SetString(IN ULONG index, IN const char *s);
	virtual void SetVector3f(IN ULONG index, IN Vector3f v);
	virtual void SetColor(IN ULONG index, IN float r, IN float g, IN float b, IN float a = 1.0);

	// array methods
	virtual ULONG GetArrayCount(IN ULONG indexOfArray);
	virtual IGetSet *GetObjectInArray(IN ULONG indexOfArray, IN ULONG indexInArray);

	virtual ULONG AddNewIntMember(IN const char *name, IN int defaultValue, IN ULONG flags = amfTransient);
	virtual ULONG AddNewDoubleMember(IN const char *name, IN double defaultValue, IN ULONG flags = amfTransient);
	virtual ULONG AddNewStringMember(IN const char *name, IN const char *defaultValue, IN ULONG flags = amfTransient);
	virtual ULONG FindIdByName(IN const char *name, OUT EGetSetType *type = 0);
};

} // namespace KEP

#endif
