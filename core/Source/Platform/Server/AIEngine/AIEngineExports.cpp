#include "stdafx.h"

#include "AIEngine.h"

#if (DRF_AI_ENGINE == 1)

#include "Engine/Blades/BladeExports.h"
#include "Engine/Blades/IBlade.h"
using namespace KEP;

IMPLEMENT_CREATE(AIEngine)

BEGIN_BLADE_LIST_ENGINE_TYPE(ENGINE_SERVER)
ADD_BLADE(AIEngine)
END_BLADE_LIST

#endif
