#include "stdafx.h"
#include "AIEngine.h"

#if (DRF_AI_ENGINE == 1)

#include "KEPHelpers.h"
#include "resource.h"
#include "KEPDiag.h"
#include "KEPException.h"

#include "AIEngineStrings.h"
#include "../Base/EngineStrings.h"
#include <ServerSerialize.h>
#include <direct.h>

#include <d3d9.h>
#include <CFileFactory.h>

#include "CSpawnClass.h"
#include "CAIWebClass.h"
#include "CABFunctionLib.h"
#include "AIClass.h"
#include "CMovementObj.h"
#include "CCollisionClass.h"
#include "CEXScriptClass.h"
#include "CEnvironmentClass.h"
#include "CSafeZones.h"
#include "CHousePlacementClass.h"
#include "CHousingClass.h"
#include "CMultiplayerClass.h"
#include "CPersistClass.h"
#include "CWorldGroupClass.h"
#include "CWorldAvailableClass.h"
#include "CSkeletalClass.h"
#include "RuntimeSkeleton.h"
#include "CSphereCollisionClass.h"
#include "CReferenceLibrary.h"

// included for loadscene cleanup may remove
#include "CWaterZoneClass.h"
#include "CBackGroundClass.h"
#include "ScriptAttributes.h"
#include "Event/Events/AIWorldsEvent.h"
#include "Event/Events/SpawnAIEvents.h"
#include "Event/Events/AIBotUpdateEvent.h"
#include "Event/Events/GetAttributeEvent.h"
#include "KEPConfigurationsXML.h"
#include "DynamicObj.h"

#include <algorithm>
#include "common/keputil/VLDWrapper.h"

// Including this header file works around DLLMain initialization issue when
// Using both boost/thread and MFC in a DLL.
// MFC code has this assertion in AfxCoreInitModule():
//
// ASSERT(AfxGetModuleState() != AfxGetAppModuleState());
//
// This is caused by boost::thread's DLLMain replacement function defined in
// [boost_source]/libs/thread/src/win32/tss_pi.cpp
// Including the following header causes the boost code to ensure the MFC initialization code gets executed.
#include "boost/thread/win32/mfc_thread_init.hpp"

static const char *const AI_THROTTLE_TAG = "AIThrottle";

EVENT_PROC_RC AIEngine::SpawnAIEventHandler(IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e) {
	AIEngine *me = (AIEngine *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	StartAISpawnEvent *startEvent = dynamic_cast<StartAISpawnEvent *>(e);
	if (startEvent) {
		string spawnGenName;
		LONG channel = 0;
		LONG maxSpawn = 0;
		LONG percent = 0;
		LONG aiCfgOverride = 0;

		startEvent->ExtractInfo(spawnGenName, channel, maxSpawn, percent, aiCfgOverride);

		// see if we have it
		bool found = false;
		for (POSITION posLocal = me->spawnGeneratorDB->GetHeadPosition(); posLocal != NULL;) {
			CSpawnObj *spawnGen = (CSpawnObj *)me->spawnGeneratorDB->GetNext(posLocal);
			if (spawnGen && spawnGen->m_spawnName.CompareNoCase(spawnGenName.c_str()) == 0) {
				found = true;

				// copy it to the new channel, and start if off
				CSpawnObj *newSpawnGen = new CSpawnObj(*spawnGen);
				// override these values in the new copy
				newSpawnGen->m_channel = channel;
				if (maxSpawn > 0)
					newSpawnGen->m_maxOutPutCapacity = maxSpawn;
				if (percent > 0)
					newSpawnGen->m_possibilityOfSpawn = percent;
				if (aiCfgOverride > 0)
					newSpawnGen->m_aiCfg = aiCfgOverride;
				newSpawnGen->m_currentOutPut = 0;
				newSpawnGen->m_manualStart = FALSE; // this will start it off

				// add in empty slot, or add end
				bool added = false;
				POSITION posForReplace = 0;
				for (POSITION pos = me->spawnGeneratorDB->GetHeadPosition(); (posForReplace = pos) != NULL;) {
					if (me->spawnGeneratorDB->GetNext(pos) == 0) {
						me->spawnGeneratorDB->SetAt(posForReplace, newSpawnGen);
						added = true;
						break;
					}
				}
				if (!added)
					me->spawnGeneratorDB->AddTail(newSpawnGen);
				break;
			}
		}
		if (!found)
			LOG4CPLUS_WARN(me->m_logger, loadStr(IDS_STARTSPAWNGEN_NOT_FOUND) << spawnGenName);
	} else {
		StopAISpawnEvent *stopEvent = dynamic_cast<StopAISpawnEvent *>(e);
		if (stopEvent) {
			string spawnGenName;
			LONG channel = 0;

			stopEvent->ExtractInfo(spawnGenName, channel);
			// if name empty, kill off all the AI on this channel, otherwise, just the spawngen

			// see if we have it
			int listIndex = 0;
			POSITION posLast = 0;
			bool found = false;
			for (POSITION posLocal = me->spawnGeneratorDB->GetHeadPosition(); (posLast = posLocal) != NULL;) {
				CSpawnObj *spawnGen = (CSpawnObj *)me->spawnGeneratorDB->GetNext(posLocal);
				if (spawnGen && spawnGen->m_spawnName.CompareNoCase(spawnGenName.c_str()) == 0 &&
					spawnGen->m_channel == channel) {
					found = true;
					// remove it from the list, and delete it to prevent spawning
					me->spawnGeneratorDB->SetAt(posLast, 0);
					delete spawnGen;

					// kill off all the items spawned by this spawn gen
					for (POSITION posLoc = me->m_movObjList->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
						CMovementObj *runtimeEntity = (CMovementObj *)me->m_movObjList->GetNext(posLoc);
						if (runtimeEntity->m_SpawnGeneratorLink == listIndex) {
							// this doesn't work, need to remove from list, too me->DestroyRuntimeEntity( runtimeEntity );
							runtimeEntity->m_energy = 0; // kill them
						}
					}

					break;
				}
				listIndex++;
			}
			if (!found)
				LOG4CPLUS_WARN(me->m_logger, loadStr(IDS_STOPPAWNGEN_NOT_FOUND) << spawnGenName);
		}
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC AIEngine::HandleLogonResponseEvent(IN const char *connectionErrorCode,
	IN HRESULT hResultCode,
	IN ULONG kanevaUserId,
	IN LOGON_RET replyCode) {
	//kanevaUserId not used in ai server
	if (hResultCode == S_OK) {
		IEvent *e = new AIWorldsEvent(m_instanceName.c_str(), m_loadedScenes);

		e->AddTo(SERVER_NETID);
		m_dispatcher.QueueEvent(e);
	}

	return EVENT_PROC_RC::OK;
}

bool AIEngine::LoadConfigValues(const IKEPConfig *cfg) {
	if (!ClientBaseEngine::LoadConfigValues(cfg))
		return false;

	if (m_scriptDir.empty())
		m_scriptDir = PathAdd(PathBase(), "AIScripts");

	// times a second
	m_aiThrottle = min(1000, max(1, cfg->Get(AI_THROTTLE_TAG, 40)));
	m_maxLoopTime = 1000 / m_aiThrottle;
	return true;
}

void AIEngine::SaveConfigValues(IKEPConfig *cfg) {
	ClientBaseEngine::SaveConfigValues(cfg);

	// save the values and write out our XML
	cfg->SetValue(AI_THROTTLE_TAG, (int)m_aiThrottle);
}

bool AIEngine::Initialize(IN const char *baseDir, IN const char *configFile, IN const DBConfig &dbConfig) {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CFileFactory::Initialize(new CFileFactory()); // read non-encrypted files

	// get the channels for the loaded scenes from the server engine's supported worlds XML
	m_supportedWorlds->SerializeFromXML(PathAdd(PathGameFiles(), SUPPORTED_WORLDS_CONFIG_FILE));

	bool ret = ClientBaseEngine::Initialize(baseDir, configFile, dbConfig);
	if (ret && !GL_aiWebDatabase) // probably died with null ptr in data
		ret = false;
	else {
		// register any AI-specific events/handlers
		RegisterEvent<AIWorldsEvent>(m_dispatcher); // we send this
		RegisterEvent<StartAISpawnEvent>(m_dispatcher);
		RegisterEvent<StopAISpawnEvent>(m_dispatcher);

		m_dispatcher.RegisterHandler(AIEngine::SpawnAIEventHandler, (ULONG)this, "AIEngine::SpawnAIEventHandler", StartAISpawnEvent::ClassVersion(), StartAISpawnEvent::ClassId(), EP_NORMAL);
		m_dispatcher.RegisterHandler(AIEngine::SpawnAIEventHandler, (ULONG)this, "AIEngine::SpawnAIEventHandler", StopAISpawnEvent::ClassVersion(), StopAISpawnEvent::ClassId(), EP_NORMAL);
	}

	m_dispatcher.SetGame(this);

	return ret && Connect();
}

bool AIEngine::InitEvents() {
	RegisterEvent<AIBotUpdateEvent>(m_dispatcher);
	bool ret = ClientBaseEngine::InitEvents();
	if (ret && m_dispatcher.HasHandler(AIBotUpdateEvent::ClassId()))
		m_hasAIBotUpdateHandler = true;
	return ret;
}

static ULONG AIENGINE_VERSION = PackVersion(1, 0, 0, 0);

AIEngine::AIEngine() :
		ClientBaseEngine(ENGINE_AI, AIENGINE_VERSION, "AIEngine"),
		GL_aiWebDatabase(0),
		spawnGeneratorDB(0),
		m_connectSet(false),
		m_maxLoopTime(1000 / 40),
		m_hasAIBotUpdateHandler(false) {
	GL_aiWebDatabase = NULL;
	GL_aiWebDatabase = new CAIWebObjList;

	spawnGeneratorDB = new CSpawnObjList;
	m_supportedWorlds = new CWorldAvailableObjList();

	m_libraryReferenceDB = new CReferenceObjList; //gkf i need reference to re-calc the collision system

	GL_spawnGeneratorTimeVarial = fTime::TimeMs();

	m_bAllowSafeZoneAttack = true; //GKF read from config file later
}

bool AIEngine::Connect() {
	if (!m_connectSet) {
		// set the values we saved from config file
		m_multiplayerObj->m_passWord = m_password.c_str();
		m_multiplayerObj->m_userName = m_userName.c_str();

		if (m_password.empty() || m_userName.empty() || m_ipAddress.empty()) {
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ERR_NO_AI_CONNECTION) << " \"" << m_userName.c_str() << "\", \"" << m_password.c_str() << "\", \"" << m_ipAddress.c_str() << "\"");
		} else {
			LOG4CPLUS_INFO(m_logger, loadStr(IDS_INFO_AI_CONNECT) << " " << m_userName.c_str() << ", " << m_password.c_str() << ", " << m_ipAddress.c_str() << ":" << m_port);
			m_multiplayerObj->EnableClient(LOGON_AI);
			m_connectSet = true;
		}
	}

	return m_connectSet;
}

//
// ===============================================================================
//    Spawn Genereator Callback
// ===============================================================================
//
static ULONG reCheckTimeMS = 10000;
void AIEngine::SpawnGeneratorCallback() // usually utilized by server only
{
	TimeMs timeMs = fTime::TimeMs();

	if ((timeMs - GL_spawnGeneratorTimeVarial) < reCheckTimeMS)
		return;

	if (!ClientIsEnabled()) {
		m_connectSet = false;
		Connect();

		reCheckTimeMS = 60000;
		GL_spawnGeneratorTimeVarial = timeMs; // reset so don't retry really fast
		return;
	}
	reCheckTimeMS = 10000;

	int generatorIndexTrace = 0;

	// Spawn Generator loop
	for (POSITION posLocal = spawnGeneratorDB->GetHeadPosition(); posLocal != NULL;) {
		CSpawnObj *spawnGenPtr = (CSpawnObj *)spawnGeneratorDB->GetNext(posLocal);

		if (spawnGenPtr != 0 &&
			!spawnGenPtr->m_manualStart &&
			spawnGenPtr->m_currentOutPut < spawnGenPtr->m_maxOutPutCapacity) {
			// possibility of spawn
			if (spawnGenPtr->m_aiCfg < 0) { // error
				LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ERR_INVALID_AI_CFG_SPAWN) << spawnGenPtr->m_aiCfg << ":" << generatorIndexTrace);
				continue;
			} // end error

			CAIObj *aiPtr = m_AIOL->GetByIndex(spawnGenPtr->m_aiCfg);
			if (!aiPtr) { // Invalid ai ptr
				LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ERR_INVALID_AI_CFG_AI) << spawnGenPtr->m_aiCfg << ":" << generatorIndexTrace);
				continue;
			} // end invalid ai ptr

			// add conditions for not spawning
			Matrix44f successMatrix = spawnGenPtr->m_spawnMatrix;
			int currentTreePosition = 0;
			if ((rand() % 1000) < spawnGenPtr->m_possibilityOfSpawn) { // conditions for not spawning
				if (spawnGenPtr->m_treeUse != -1) { // tree override in use
					successMatrix.MakeIdentity();
					POSITION wPos = GL_aiWebDatabase->FindIndex(spawnGenPtr->m_treeUse);
					if (wPos) {
						CAIWebObj *wObj = (CAIWebObj *)GL_aiWebDatabase->GetAt(wPos);
						POSITION trePos = NULL;
						if (!wObj->m_web) { // error- not compiled
							LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ERR_INVALID_SPAWN_GEN) << generatorIndexTrace);
							continue;
						} // end error - not compiled

						if (spawnGenPtr->m_treePosition == -1) { // get random tree location
							int locationAmount = wObj->m_web->GetCount();
							if (locationAmount > 0) {
								int randomPosition = rand() % locationAmount;
								trePos = wObj->m_web->FindIndex(randomPosition);
								currentTreePosition = randomPosition;
							}
						} // end get random tree location
						else {
							trePos = wObj->m_web->FindIndex(spawnGenPtr->m_treePosition);
						}

						if (trePos) { // 2
							CAIWebNode *node = (CAIWebNode *)wObj->m_web->GetAt(trePos);
							successMatrix.GetTranslation() = node->m_position;
						} // end 2
					}
				} // end tree override in use

				// Create New Movement Object
				CMovementObj *refPtr = NULL;
				bool ok = ActivateMovementEntity(
					spawnGenPtr->m_aiCfg,
					successMatrix,
					aiPtr->m_entityCfgDBindexRef, // spawnGenPtr->m_spawnIndex,
					eActorControlType::CONTROL_TYPE_NPC, // hardcode ai to player-spawnGenPtr->m_spawnType,
					0,
					0,
					FALSE,
					FALSE,
					true,
					generatorIndexTrace,
					&refPtr,
					NULL,
					-1,
					spawnGenPtr->m_channel,
					false,
					"");
				if (!ok) {
					LogError("ActivateMovementEntity() FAILED");
				} else {
					if (refPtr->m_aiObj) { // if AI
						refPtr->m_aiObj->m_routeCurTarget = currentTreePosition;
						refPtr->m_aiObj->m_mapCurLocation = currentTreePosition;
						refPtr->m_aiObj->m_curMap = spawnGenPtr->m_treeUse;

						if (refPtr->m_aiObj->m_physicsModuleOverride) { // Movment ovverride valid
							if (refPtr->m_movementDB) {
								refPtr->m_movementDB->SafeDelete();
								delete refPtr->m_movementDB;
								refPtr->m_movementDB = 0;
							}

							refPtr->m_aiObj->m_physicsModuleOverride->Clone(&refPtr->m_movementDB);
							refPtr->m_movementDB->m_lastPosition = successMatrix.GetTranslation();
						} // end Movment ovverride valid

						// ArmNextWeapon(refPtr,TRUE);
					}

					spawnGenPtr->m_currentOutPut++;
				}
			}
		}

		generatorIndexTrace++;
	}
	GL_spawnGeneratorTimeVarial = timeMs;
}

BOOL AIEngine::AI_Loop(CMovementObj *ptrLoc) {
	if (!ptrLoc->m_aiObj)
		return FALSE;

	TimeMs timeMs = fTime::TimeMs();

	if (ptrLoc->m_possedBy)
		return TRUE;

	if (ptrLoc->m_aiObj->m_controlled)
		return TRUE;

	if (m_multiplayerObj == 0 || !ClientIsEnabled())
		return TRUE;

	if (m_hasAIBotUpdateHandler) {
		IEvent *e = new AIBotUpdateEvent(this, ptrLoc, ptrLoc->m_energy, ptrLoc->m_aiObj->m_lastKnownEnergy);
		if (m_dispatcher.ProcessSynchronousEvent(e) != 0) {
			e->Delete();
			return TRUE; // processed by event handler
		}
		e->Delete();
	}

	// set
	ptrLoc->m_aiObj->m_currentSpeedOverride = 0.0f;

	ptrLoc->m_movementDB->SetUseStatesToFalse();

	// get AI config
	if (ptrLoc->m_energy <= 0) { // if died dont do anything
		ptrLoc->m_filterCollision = FALSE;

		if (!ptrLoc->m_movementDB->m_floatWhenDead) {
			ptrLoc->m_movementDB->m_antiGravity = FALSE;
		}

		ptrLoc->m_deathTimeVarial = fTime::TimeMs();

		if (!ClientIsEnabled()) {
			ptrLoc->m_controlType = CMovementObj::CONTROL_TYPE_DEAD_999; // flag to dissipate
		}
	} // end if died dont do anything
	else { // do thought process
		// Brain
		// check life status--//
		if (ptrLoc->m_aiObj->m_lastKnownEnergy == UNINITIALIZED_ENERGY)
			ptrLoc->m_aiObj->m_lastKnownEnergy = ptrLoc->m_energy;

		if (ptrLoc->m_aiObj->m_lastKnownEnergy > ptrLoc->m_energy) { // body takin damage why?
			if (AI_getTarget(ptrLoc, 0) == TRUE) { // get possible attacker
				ptrLoc->m_aiObj->m_condition = 1; // under attack
				if (ptrLoc->m_energy < 0) { // hurt bad retreat?
					ptrLoc->m_aiObj->m_desiredGoal = 3; // retreat
				} else if (ptrLoc->m_aiObj->m_desiredGoal != 4) {
					delete[] ptrLoc->m_aiObj->m_currentRoute;
					ptrLoc->m_aiObj->m_currentRoute = 0;
					ptrLoc->m_aiObj->m_desiredGoal = 4; // kill
				}
			} // end get possible attacker

			ptrLoc->m_aiObj->m_lastKnownEnergy = ptrLoc->m_energy;
		} // end body takin damage why?
		else {
			ptrLoc->m_aiObj->m_lastKnownEnergy = ptrLoc->m_energy;
		}

		if (AI_VerifyTarget(ptrLoc) == FALSE) { // verify target existence
			if (ptrLoc->m_aiObj->m_desiredGoal == 4)
				ptrLoc->m_aiObj->m_desiredGoal = 0; // target not there be happy

			if (ptrLoc->m_aiObj->m_desiredGoal == 3)
				ptrLoc->m_aiObj->m_desiredGoal = 0; // safe now be happy
		} // end verify target existence
		else {
			if (ptrLoc->m_targetRef) {
				if (ptrLoc->m_targetRef->m_energy <= 0)
					MaintainTarget(ptrLoc);
			}
		}

		// test aggesive
		if (ptrLoc->m_aiObj->m_agressive != 0) { // ai is agressive
			if (ptrLoc->m_targetRef == NULL) {
				if (AI_getTarget(ptrLoc, ptrLoc->m_aiObj->m_agressive) == TRUE)
					ptrLoc->m_aiObj->m_desiredGoal = 4;
			}
		} // end ai is agressive

		EvaluateAIVisionToTargetDest(ptrLoc, timeMs); // vision module

		BOOL okToEnterAttackModule = TRUE;

		if (ptrLoc->m_aiObj->m_feelerSys) {
			if (ptrLoc->m_aiObj->m_feelerSys->m_onWallCurrently)
				okToEnterAttackModule = FALSE; // filter attack module if wall mod is enabled
		}

		if (okToEnterAttackModule) { // not currently pathfinding
			if (ptrLoc->m_aiObj->m_desiredGoal == 4 || ptrLoc->m_aiObj->m_needToDefend == TRUE) { // attack focus
				if (ptrLoc->m_targetRef != NULL) {
					AI_Attack(ptrLoc, ptrLoc->m_aiObj, timeMs);
				}
			}
		} // end not currently pathfinding

		if (ptrLoc->m_curHitCounter > 20) {
			if (!ptrLoc->m_aiObj->m_retreating) {
				ptrLoc->m_aiObj->m_retreatStamp = fTime::TimeMs();
				ptrLoc->m_aiObj->m_retreating = TRUE;
			}
		}

		if (ptrLoc->m_targetRef != NULL) {
			if (ptrLoc->m_aiObj->m_desiredGoal == 3 || ptrLoc->m_aiObj->m_retreating == TRUE) {
				if (ptrLoc->m_aiObj->m_needToDefend == FALSE || ptrLoc->m_aiObj->m_retreating == TRUE)
					AI_Retreat(ptrLoc, ptrLoc->m_aiObj); // ok to retreat
			}
		}

		AI_TravelLookBasis(ptrLoc, ptrLoc->m_aiObj, timeMs); // manages travel to a desired location

		if (ptrLoc->m_aiObj->m_radiusSysInUse == FALSE) { // radius in use no tree incrementing pls
			if (ptrLoc->m_targetRef == NULL)
				AI_TreeWebUse(ptrLoc);
		}

		if (ptrLoc->m_aiObj->m_radiusSysInUse == FALSE) { // radius in use no scripting here
			if (ptrLoc->m_aiObj->m_desiredGoal == 0)
				AI_HandleScript(ptrLoc);
		}
	} // end do thought process

	AI_Transform(ptrLoc); // Transformation Management

	if (ptrLoc->m_lastPosition.x == ptrLoc->m_location.x && ptrLoc->m_lastPosition.y == ptrLoc->m_location.y && ptrLoc->m_lastPosition.z == ptrLoc->m_location.z && ptrLoc->m_aiObj->m_desiredGoal == 0) {
		ptrLoc->m_movementDB->m_curRotY = ((float)((rand() % 100) - 50.0f)) * .015f;
		AI_Transform(ptrLoc); // Transformation Management
	}

	return TRUE;
}

//
// ===========================================================================
//    End Function
// ===========================================================================
//
void AIEngine::MaintainTarget(CMovementObj *ptrLoc) {
	ptrLoc->m_attackerTrace = -1; // target has been elliminated
	ptrLoc->m_targetRef = NULL;
	ptrLoc->m_aiObj->m_desiredGoal = 0; // safe now be happy
	ptrLoc->m_aiObj->m_needToDefend = FALSE;
	ptrLoc->m_baseFrame->GetPosition(NULL, &ptrLoc->m_aiObj->m_lookAt);
}
//
// ===========================================================================
//    End Function
// ===========================================================================
//
void AIEngine::AI_HandleScript(CMovementObj *ptrLoc) {
	// Handle scripts
	if (!ptrLoc->m_aiObj->m_currentRoute) { // no exec till route command is finished
		if (ptrLoc->m_aiObj->m_curScript) { // valid current script
			if (ptrLoc->m_aiObj->m_mapCurLocation != ptrLoc->m_aiObj->m_routeCurTarget) { // route need reinitialization
				POSITION posWeb = GL_aiWebDatabase->FindIndex(ptrLoc->m_aiObj->m_curMap);
				if (posWeb) {
					CAIWebObj *webObj = (CAIWebObj *)GL_aiWebDatabase->GetAt(posWeb);
					int count = 0;
					ptrLoc->m_aiObj->m_currentRoute = webObj->GetRoute(ptrLoc->m_aiObj->m_mapCurLocation,
						ptrLoc->m_aiObj->m_routeCurTarget,
						&count);
					ptrLoc->m_aiObj->m_routePointCount = count;
					if (!ptrLoc->m_aiObj->m_currentRoute) {
						ptrLoc->m_aiObj->m_routeCurTarget = ptrLoc->m_aiObj->m_mapCurLocation;

						// GL_systemReadout->AddLine("cows");
					}

					// GL_systemReadout->AddLine("cont route set");
					return;
				}
			} // end route need reinitialization

			KEP_ASSERT(ptrLoc->m_aiObj->m_curScript->m_delayStamp >= 0 && ptrLoc->m_aiObj->m_curScript->m_delayDuration >= 0);
			if (fTime::ElapsedMs(ptrLoc->m_aiObj->m_curScript->m_delayStamp) > ptrLoc->m_aiObj->m_curScript->m_delayDuration) { // delay factor
				int count = 0;
				CAIScriptEvent *newEventPtr = ptrLoc->m_aiObj->m_curScript->GetEvent(ptrLoc->m_aiObj->m_curScript->m_currentEvent);
				if (newEventPtr) {
					if (newEventPtr->m_event == 0) { // goTo event
						POSITION posWeb = GL_aiWebDatabase->FindIndex(ptrLoc->m_aiObj->m_curMap);
						if (posWeb) {
							CAIWebObj *webObj = (CAIWebObj *)GL_aiWebDatabase->GetAt(posWeb);
							ptrLoc->m_aiObj->m_currentRoute = webObj->GetRoute(ptrLoc->m_aiObj->m_mapCurLocation, newEventPtr->m_miscInt, &count);
							ptrLoc->m_aiObj->m_routePointCount = count;
							ptrLoc->m_aiObj->m_routeCurTarget = newEventPtr->m_miscInt;
						}
					} // end goTo event

					if (newEventPtr->m_event == 1) { // delay event
						ptrLoc->m_aiObj->m_curScript->m_delayDuration = newEventPtr->m_miscInt;
						ptrLoc->m_aiObj->m_curScript->m_delayStamp = fTime::TimeMs();
					} // end delay event

					if (newEventPtr->m_event == 2) { // set Anim by version
						if (ptrLoc->m_runtimeSkeleton) {
							ptrLoc->m_runtimeSkeleton->SetCurrentAnimationByVersion(
								newEventPtr->m_miscInt,
								RuntimeSkeleton::AnimDir_Forward,
								100.0);
						}
					} // end set Anim by version

					if (newEventPtr->m_event == 3) { // random position on tree
						POSITION posWeb = GL_aiWebDatabase->FindIndex(ptrLoc->m_aiObj->m_curMap);
						if (posWeb) {
							CAIWebObj *webObj = (CAIWebObj *)GL_aiWebDatabase->GetAt(posWeb);
							int locationAmount = webObj->m_web->GetCount();
							if (locationAmount > 0) {
								int randomPosition = rand() % locationAmount;
								ptrLoc->m_aiObj->m_routeCurTarget = randomPosition;
								ptrLoc->m_aiObj->m_currentRoute = webObj->GetRoute(ptrLoc->m_aiObj->m_mapCurLocation, randomPosition, &count);
								ptrLoc->m_aiObj->m_routePointCount = count;
							} else {
								GL_systemReadout->AddLine("Error no tree positions found.", GetD3DDevice(), m_symbolMapDB, ctSystem);
							}
						}
					} // end random position on tree

					if (newEventPtr->m_event == 4) { // loop to beggining of script
						ptrLoc->m_aiObj->m_curScript->m_currentEvent = -1;
					} // end loop to beggining of script

					ptrLoc->m_aiObj->m_curScript->m_currentEvent++; // done ..next event pls
				} else { // script done destroy
					ptrLoc->m_aiObj->m_curScript->SafeDelete();
					delete ptrLoc->m_aiObj->m_curScript;
					ptrLoc->m_aiObj->m_curScript = 0;
				} // end script done destroy
			} // end delay factor
		} // end valid current script
	}
}

void AIEngine::AI_TreeWebUse(CMovementObj *ptrLoc) {
	// DEPRECATED
}

// End Function

//Set destination to move toward target
void AIEngine::AI_MoveToTarget(CMovementObj *ptrLoc, CAIObj *AIPtr) {
	CMovementObj *targetObj = ptrLoc->m_targetRef;

	Matrix44f targetMatrix;

	if (targetObj) {
		targetObj->m_baseFrame->GetTransform(NULL, &targetMatrix);

		AIPtr->m_lookAt = targetMatrix.GetTranslation();

		ptrLoc->m_AIdestination = targetMatrix.GetTranslation();
	}
}

inline BOOL AIEngine::AI_Attack(CMovementObj *ptrLoc, CAIObj *AIPtr, TimeMs currentTime) {
	if (AIPtr->m_targetInSight) { // i can see target
		TimeMs timePassed = currentTime - AIPtr->m_attackStamp;
		if (!AIPtr->m_attackingCurrently) { // currently not attacking
			if (timePassed > AIPtr->m_attackTimeBetweenCurrent) { // done waiting set attack variables
				float percentage = ((float)(rand() % 100)) * .01f;
				AIPtr->m_attackDurationCurrent = ((long)(percentage * ((float)(AIPtr->m_attackDurationMax - AIPtr->m_attackDurationMin)))) + AIPtr->m_attackDurationMin;
				AIPtr->m_attackStamp = currentTime;
				AIPtr->m_attackingCurrently = TRUE;
			} // end done waiting set attack variables
		} // currently not attacking
		else { // currently attacking
			if (timePassed > AIPtr->m_attackDurationCurrent) { // done with attack set time between variables
				float percentage = ((float)(rand() % 100)) * .01f;
				AIPtr->m_attackTimeBetweenCurrent =
					((long)(percentage * ((float)(AIPtr->m_attackDurationMax - AIPtr->m_attackDurationMin)))) +
					AIPtr->m_attackDurationMin;
				AIPtr->m_attackStamp = currentTime;
				AIPtr->m_attackingCurrently = FALSE;
			} // end done with attack set time between variables
		} // end currently attacking
	} // end i can see target

	Vector3f targetPosition, entityWldPosition;
	Matrix44f targetMatrix;
	ptrLoc->m_baseFrame->GetPosition(NULL, &entityWldPosition);
	ptrLoc->m_targetRef->m_baseFrame->GetPosition(ptrLoc->m_baseFrame, &targetPosition);

	LOG4CPLUS_DEBUG(m_logger, "Targeting target pos " << targetPosition.x << "," << targetPosition.y << "," << targetPosition.z);

	float actualHeight = targetPosition.y;
	ptrLoc->m_targetRef->m_baseFrame->GetTransform(NULL, &targetMatrix);
	LOG4CPLUS_DEBUG(m_logger, "Targeting target matrix " << targetMatrix(3, 0) << "," << targetMatrix(3, 1) << "," << targetMatrix(3, 2));

	// find target damage location
	if (ptrLoc->m_targetRef->m_runtimeSkeleton->m_sphereLocationalDamageList) { // get vunerable point
		int desBone = 0;
		float highestMultiplier = 0.0f;
		for (POSITION posLoc = ptrLoc->m_targetRef->m_runtimeSkeleton->m_sphereLocationalDamageList->GetHeadPosition(); posLoc != NULL;) { // damage point loop
			CSphereCollisionObj *sphereObj =
				(CSphereCollisionObj *)ptrLoc->m_targetRef->m_runtimeSkeleton->m_sphereLocationalDamageList->GetNext(posLoc);
			if (sphereObj->m_damageMultiplier > highestMultiplier) {
				highestMultiplier = sphereObj->m_damageMultiplier;
				desBone = sphereObj->m_boneIndex;
			}
		} // end damage point loop

		Matrix44f boneMatrix = ptrLoc->m_targetRef->m_runtimeSkeleton->GetBoneMatrixByIndex(desBone);
		targetMatrix = boneMatrix * targetMatrix;
	} // end get vunerable point

	// end find target damage location
	// for chargeing
	Vector3f target = targetMatrix.GetTranslation();

	// lead fire adjustment
	float distFromTarget = targetPosition.Length();
	Vector3f leadVector;

	float errorVal = 1.0f;
	int rNum = rand();
	LOG4CPLUS_DEBUG(m_logger, "Randomness = " << rNum << ", " << rNum % 100 << "% accuracy = " << AIPtr->m_accuracy << " Is inaccurate = " << (bool)((rNum % 100) + 1 > AIPtr->m_accuracy));

	// 12/13/04 reversed accuracy meaning now 0 if inaccurate 100 is 100% accurate
	// now get random number 1-100 instead of 0-99
	if ((rNum % 100) + 1 > AIPtr->m_accuracy) { // inperfect shot
		float percentage = ((float)(rand() % 100)) * .01f;
		errorVal = (percentage * (AIPtr->m_accuracyErrorMax - AIPtr->m_accuracyErrorMin)) + AIPtr->m_accuracyErrorMin;
	} // end inperfect shot

	if ((AIPtr->m_currentMissileSpeed * GL_adjust) == 0.0f) {
		AIPtr->m_currentMissileSpeed = 1.0f;
	}

	float multiplier = (distFromTarget) / (AIPtr->m_currentMissileSpeed * GL_adjust);
	LOG4CPLUS_DEBUG(m_logger, "Targeting lead vector " << ptrLoc->m_targetRef->m_directionalVector.x << ","
													   << ptrLoc->m_targetRef->m_directionalVector.y << ","
													   << ptrLoc->m_targetRef->m_directionalVector.z << " multiplier " << multiplier << " error " << errorVal);
	leadVector.x = (ptrLoc->m_targetRef->m_directionalVector.x * multiplier) * errorVal;
	leadVector.y = (ptrLoc->m_targetRef->m_directionalVector.y * multiplier) * errorVal;
	leadVector.z = (ptrLoc->m_targetRef->m_directionalVector.z * multiplier) * errorVal;
	LOG4CPLUS_DEBUG(m_logger, "Targeting lead processed vector " << ptrLoc->m_targetRef->m_directionalVector.x << ","
																 << ptrLoc->m_targetRef->m_directionalVector.y << ","
																 << ptrLoc->m_targetRef->m_directionalVector.z);

	AIPtr->m_lookAt = target;

	target += leadVector;

	// lookat tag
	// advanced pathfinding-//
	ptrLoc->m_AIdestination = target;
	LOG4CPLUS_DEBUG(m_logger, "Targeting new dest" << ptrLoc->m_AIdestination.x << ","
												   << ptrLoc->m_AIdestination.y << ","
												   << ptrLoc->m_AIdestination.z);

	Matrix44f viewLoc;
	ptrLoc->m_baseFrame->GetTransform(NULL, &viewLoc);
	targetPosition = MatrixARB::GetLocalPositionFromMatrixView(viewLoc, target);

	float angle = 0.0f;
	if (distFromTarget != 0.0f) {
		angle = atan(actualHeight / distFromTarget) * 57.3; // (180/3.1415926535897932);
		if (ptrLoc->m_runtimeSkeleton) {
			ptrLoc->m_runtimeSkeleton->ApplyFreeLookRotation(angle, 0.0f);
		}

		ptrLoc->m_movementDB->m_freeLookCurPitch = angle;
	}

	return TRUE;
}

BOOL AIEngine::AI_Retreat(CMovementObj *ptrLoc, CAIObj *AIPtr) {
	if (!AIPtr->m_targetFrame || !ptrLoc->m_targetRef || !ptrLoc->m_targetRef->m_baseFrame)
		return FALSE;

	Vector3f targetPosition;
	Vector3f thisPosition;
	ptrLoc->m_baseFrame->GetPosition(NULL, &thisPosition);
	ptrLoc->m_targetRef->m_baseFrame->GetPosition(NULL, &targetPosition);

	if (fTime::ElapsedMs(ptrLoc->m_aiObj->m_retreatStamp) > 10000) {
		ptrLoc->m_attackerTrace = -1; // target has been elliminated
		ptrLoc->m_targetRef = NULL;
		ptrLoc->m_aiObj->m_needToDefend = FALSE;

		ptrLoc->m_curHitCounter = 0;
		ptrLoc->m_aiObj->m_retreating = FALSE;
		return FALSE;
	}

	AIPtr->m_lookAt.x = ((thisPosition.x - targetPosition.x) + thisPosition.x);
	AIPtr->m_lookAt.y = ((thisPosition.y - targetPosition.y) + thisPosition.y);
	AIPtr->m_lookAt.z = ((thisPosition.z - targetPosition.z) + thisPosition.z);

	return TRUE;
}

inline BOOL AIEngine::AI_targetInSight(CMovementObj *ptrLoc, float maxDistance) {
	if (ptrLoc->m_targetRef == NULL)
		return TRUE;

	int polysChecked = 0;
	int polysConsidered = 0;
	Vector3f positionWldEyeLevel;
	ptrLoc->m_baseFrame->GetPosition(NULL, &positionWldEyeLevel);
	positionWldEyeLevel.y += ptrLoc->m_aiObj->m_eyeLevel; // adjust to eyelevel

	Vector3f targetPosition;
	ptrLoc->m_targetRef->m_baseFrame->GetPosition(NULL, &targetPosition);

	float range = Distance(positionWldEyeLevel, ptrLoc->m_AIdestination);

	if (range > maxDistance)
		return FALSE;

	if (ptrLoc->m_aiObj->CheckVisionIsClear(positionWldEyeLevel, targetPosition, GL_collisionBase, &polysChecked, &polysConsidered)) {
		if (!RealtimeSegmentTestUseTable(positionWldEyeLevel, ptrLoc->m_aiObj->m_lookAt)) {
			return TRUE;
		} else {
			return FALSE;
		}
	} else {
		return FALSE;
	}
}

inline BOOL AIEngine::AI_TravelLookBasis(CMovementObj *ptrLoc, CAIObj *AIPtr, TimeMs curTimeMs) {
	// follow code
	// can be use to travel to a location
	// if walking just use yaw
	Vector3f locationLoc, wldSpcPos;
	locationLoc.x = 0.0f;
	locationLoc.y = 0.0f;
	locationLoc.z = 0.0f;

	ptrLoc->m_baseFrame->GetPosition(NULL, &wldSpcPos);

	BOOL inProcess = FALSE;
	BOOL doNormalDrive = TRUE;
	if (AIPtr->m_feelerSys) { // use feel path find method
		Matrix44f positionalMatrix;
		ptrLoc->m_baseFrame->GetTransform(NULL, &positionalMatrix);
		if ((curTimeMs - AIPtr->m_feelerSys->m_stamp) > AIPtr->m_feelerSys->m_cycleTime) {
			AIPtr->UpdateFeelerStatus(ptrLoc->m_collisionMovRefPtr,
				positionalMatrix,
				AIPtr->m_floorTolerance,
				AIPtr->m_wallMinimumHeight,
				positionalMatrix(3, 1),
				GL_systemReadout,
				ptrLoc->m_AI_reachedDestination,
				ptrLoc->m_aiObj->m_lookAt);
			AIPtr->m_feelerSys->m_stamp = curTimeMs;
		}

		if (AIPtr->m_feelerSys->m_onWallCurrently) { // calculate
			if (AIPtr->m_targetInSight == TRUE) {
				AIPtr->m_feelerSys->m_onWallCurrently = FALSE;
				if (AIPtr->m_enableJumpAbility) {
					ptrLoc->m_movementDB->JumpMovementClassObj();
				}
			}

			Vector3f currentPositionWld;
			Vector3f nUpVect, nDirVect, adjustCrossVect;

			// project normal into 2d
			nUpVect.x = 0.0f;
			nUpVect.y = 1.0f;
			nUpVect.z = 0.0f;
			nDirVect.x = AIPtr->m_feelerSys->m_mostRecentWallNormal.x;
			nDirVect.y = 0.0f;
			nDirVect.z = AIPtr->m_feelerSys->m_mostRecentWallNormal.z;

			if (AIPtr->m_feelerSys->m_wallDirection == 0) { // link progreesion direction
				adjustCrossVect = Cross(nDirVect, nUpVect); // makes a "right" cross on wall
			} else {
				adjustCrossVect = Cross(nUpVect, nDirVect); // makes a "left" cross on wall
			}

			adjustCrossVect.Normalize();

			// increment
			AIPtr->m_closeEnoughDist = 2.0f; // go to location
			inProcess = TRUE;
			adjustCrossVect = adjustCrossVect * AIPtr->m_feelerSys->m_incrementOnWall;
			ptrLoc->m_baseFrame->GetPosition(NULL, &currentPositionWld);

			// set new destination
			ptrLoc->m_aiObj->m_lookAt = currentPositionWld + adjustCrossVect;
		} // end calculate
	} // end use feel path find method

	if (doNormalDrive == TRUE) { // normal travel
		AIPtr->m_targetFrame->SetPosition2(NULL,
			ptrLoc->m_aiObj->m_lookAt.x,
			(ptrLoc->m_aiObj->m_lookAt.y + AIPtr->m_prefernceAboveTarget),
			ptrLoc->m_aiObj->m_lookAt.z);
		if (AIPtr->m_targetFrame)
			AIPtr->m_targetFrame->GetPosition(ptrLoc->m_baseFrame, &locationLoc); // get reletive location

		AdvancedAiSteering(ptrLoc, locationLoc);

		if (!inProcess) {
			if ((curTimeMs - AIPtr->m_distanceQuerryStamp) > AIPtr->m_distanceQuerryDuration) {
				// calculate range randomly
				if (!AIPtr->m_attackingCurrently) { // not attackin following
					float totalDistance = AIPtr->m_followMaxDistance - AIPtr->m_followMinDistance;
					float percentage = ((float)(rand() % 100)) * .01f;
					AIPtr->m_closeEnoughDist = AIPtr->m_followMinDistance + (percentage * totalDistance);
				} // end not attackin following
				else { // attacking set appropriate distance
					float totalDistance = AIPtr->m_attackMaxDistance - AIPtr->m_attackMinDistance;
					float percentage = ((float)(rand() % 100)) * .01f;
					AIPtr->m_closeEnoughDist = AIPtr->m_attackMinDistance + (percentage * totalDistance);
				} // end attacking set appropriate distance

				AIPtr->m_distanceQuerryStamp = curTimeMs;
			}
		}

		// check if desired distance is fullfilled
		float floatYtemp = locationLoc.y;
		if (ptrLoc->m_movementDB->m_antiGravity == FALSE)
			floatYtemp = 0.0f; // so he doesent spaz when th dis is far up and he cant fly

		int dist = (int)Vector3f(locationLoc.x, floatYtemp, locationLoc.z).Length();

		// end check if desired distance is fullfilled
		BOOL stop = FALSE;
		if (ptrLoc->m_targetRef == NULL) { // following path only
			if (ptrLoc->m_targetRef == NULL && dist <= AIPtr->m_closeEnoughDist)
				stop = TRUE;
		} // end following path only
		else { // folowing somthing
			if (dist <= AIPtr->m_closeEnoughDist && AIPtr->m_targetInSight == TRUE)
				stop = TRUE;
		} // end following somthing

		if (!AIPtr->RadiusSysAllowMovement(wldSpcPos, ptrLoc->m_AIdestination, ptrLoc->m_AI_target))
			stop = TRUE;

		if (ptrLoc->m_AI_target == -1) { // helper of radius sys (no more taregt)
			ptrLoc->m_targetRef = NULL;
			if (AIPtr->m_radiusSysInUse) {
				AIPtr->m_radiusSysInUse = FALSE;
				ptrLoc->m_aiObj->m_lookAt.x = ptrLoc->m_aiObj->m_radiusSysCenter.x;
				ptrLoc->m_aiObj->m_lookAt.y = ptrLoc->m_aiObj->m_radiusSysCenter.y;
				ptrLoc->m_aiObj->m_lookAt.z = ptrLoc->m_aiObj->m_radiusSysCenter.z;
			}
		}

		if (stop) { // close enough
			ptrLoc->m_AI_reachedDestination = TRUE;
			ptrLoc->m_movementDB->ZeroForces();
		} // end close enough
		else { // too far:=], move in
			ptrLoc->m_AI_reachedDestination = FALSE;
			ptrLoc->m_movementDB->Accellerate(AIPtr->m_currentSpeedOverride);
		} // end too far:=], move in
	} // end normal travel

	return TRUE;
}

inline void AIEngine::EvaluateAIVisionToTargetDest(CMovementObj *ptrLoc, TimeMs curTimeMs) {
	if (!ptrLoc->m_aiObj->m_enableVision)
		return;

	if ((curTimeMs - ptrLoc->m_aiObj->m_sightCheckStamp) > ptrLoc->m_aiObj->m_sightCheckDuration) {
		//GKF Use safe distance not visible distance
		ptrLoc->m_aiObj->m_targetInSight = AI_targetInSight(ptrLoc, min(ptrLoc->m_aiObj->m_safeDistance, ptrLoc->m_aiObj->m_maxSightRadius));
		ptrLoc->m_aiObj->m_sightCheckStamp = curTimeMs;
		if (ptrLoc->m_aiObj->m_targetInSight == TRUE || ptrLoc->m_targetRef == NULL)
			ptrLoc->m_aiObj->m_timeStampWhenTargetNotInSight = curTimeMs;
	}
}

BOOL AIEngine::AI_Transform(CMovementObj *ptrLoc) {
	float doYaw = (float)((ptrLoc->m_movementDB->m_curRotY * exp((fabs(ptrLoc->m_movementDB->m_curSpeed)))));
	float doPitch = (float)((ptrLoc->m_movementDB->m_curRotX * exp((fabs(ptrLoc->m_movementDB->m_curSpeed)))));
	if (ptrLoc->m_baseFrame == NULL) {
		return FALSE;
	}

	// ADDITIONAL ENTITY FORCES
	float AdditionalForceX = 0.0f;
	float AdditionalForceY = 0.0f;
	float AdditionalForceZ = 0.0f;

	// EXTERNAL FORCES
	float ExternalForceX = m_environment->m_windDirectionX + ptrLoc->m_externalVectorTemp.x;
	float ExternalForceY = m_environment->m_windDirectionY + ptrLoc->m_externalVectorTemp.y;
	float ExternalForceZ = m_environment->m_windDirectionZ + ptrLoc->m_externalVectorTemp.z;
	ptrLoc->m_externalVectorTemp.x = 0.0f;
	ptrLoc->m_externalVectorTemp.y = 0.0f;
	ptrLoc->m_externalVectorTemp.z = 0.0f;

	// end update background if required
	if (ptrLoc->m_movementDB->m_yawIsUsed == FALSE) {
		ptrLoc->m_movementDB->DecelTurn();
	}

	if (ptrLoc->m_movementDB->m_pitchIsUsed == FALSE) {
		ptrLoc->m_movementDB->DecelPitch();
	}

	if (ptrLoc->m_movementDB->m_rollIsUsed == FALSE) {
		ptrLoc->m_movementDB->DecelRoll();
	}

	if (ptrLoc->m_movementDB->m_speedControlInUse == FALSE) {
		ptrLoc->m_movementDB->DecelSpeed();
	}

	if (ptrLoc->m_movementDB->m_sidestepsInUse == FALSE) {
		ptrLoc->m_movementDB->DecelSideStep();
	}

	Vector3f ExternalTranslation(0.0f, 0.0f, 0.0f);
	if (ptrLoc->m_recentSurfaceInfo.objectType == DYNAMIC_OBJECT) {
		CDynamicPlacementObj *pPlcmt = GetObjectByPlacementId(ptrLoc->m_recentSurfaceInfo.objectId);
		if (pPlcmt && pPlcmt->IsMoving()) {
			const Matrix44f &currLocation = pPlcmt->m_skeleton->GetWorldMatrix();
			ExternalTranslation = currLocation.GetTranslation() - pPlcmt->m_lastLocation.GetTranslation();
		}
	}

	m_physics->MovPhysicsSystem(GL_adjust,
		ptrLoc->m_baseFrame,
		ptrLoc->m_movementDB,
		AdditionalForceX,
		AdditionalForceY,
		AdditionalForceZ,
		ExternalForceX,
		ExternalForceY,
		ExternalForceZ,
		doYaw,
		doPitch,
		ptrLoc->m_movementDB->m_rollIsUsed,
		ptrLoc->m_movementDB->m_pitchIsUsed,
		ptrLoc->m_movementDB->m_yawIsUsed,
		ExternalTranslation.x,
		ExternalTranslation.y,
		ExternalTranslation.z);

	if (ptrLoc->m_movementDB->m_autoLevel == TRUE) {
		ptrLoc->AutoLevelEntity(ptrLoc->m_movementDB->m_autoLevelStrength, GL_adjust);
	}

	return TRUE;
}

void AIEngine::DeleteSpawnDatabase() {
	if (!spawnGeneratorDB) {
		return;
	}

	spawnGeneratorDB->SafeDelete();

	delete spawnGeneratorDB;
	spawnGeneratorDB = 0;
}

// override so we can wait, if needed
UINT AIEngine::RenderLoop(IN bool canWait) {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	Timer timer;
	ClientBaseEngine::RenderLoop(canWait);
	m_dispatcher.ProcessEventsUntil(100); // max 1/10 sec ok?
	TimeMs elapsed = timer.ElapsedMs();
	return (elapsed < m_maxLoopTime) ? (m_maxLoopTime - elapsed) : 0;
}

void AIEngine::FreeAll() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (GL_aiWebDatabase) {
		GL_aiWebDatabase->SafeDelete();
		delete GL_aiWebDatabase;
		GL_aiWebDatabase = 0;
	}

	// end Destroy object lists
	if (m_AIOL) {
		m_AIOL->SafeDelete();
		delete m_AIOL;
		m_AIOL = 0;
	}

	DeleteSpawnDatabase();

	if (m_supportedWorlds) {
		m_supportedWorlds->SafeDelete();
		delete m_supportedWorlds;
		m_supportedWorlds = 0;
	}

	ClientBaseEngine::FreeAll();

	CFileFactory::DeInitialize();
}

bool AIEngine::ProcessScriptAttribute(CEXScriptObj *sctPtr) {
	bool ret = true;

	switch (sctPtr->m_actionAttribute) {
		case LOAD_GLOBAL_ATTACHMENTDB: // 4/08 AI needs server version
			LoadGlobalAttachmentDB((sctPtr->m_miscString + ".Server").GetString());
			break;

		case LOAD_ENTITY_DATABASE:
			::ServerSerialize = true;
			LoadEntityDatabase((sctPtr->m_miscString + ".Server").GetString());
			::ServerSerialize = false;
			break;

		case SET_USERNAME:
			m_userName = sctPtr->m_miscString;
			break;

		case SET_PASSWORD:
			m_password = sctPtr->m_miscString;
			break;

		case OPEN_SCENE:
			if (m_loadedScenes.empty()) {
				// this does LoadScene since base only supports one
				ret = ClientBaseEngine::ProcessScriptAttribute(sctPtr);
				if (ret) {
					m_loadedScenes.push_back((const char *)sctPtr->m_miscString);
					// just set to first only
					SetZoneIndex(m_supportedWorlds->GetZeroBasedIndex(sctPtr->m_miscString));
				}
			} else {
				// not first one
				if (AppendScene(sctPtr->m_miscString)) {
					m_loadedScenes.push_back((const char *)sctPtr->m_miscString);
				} else {
					ret = false;
				}
			}
			break;

		case LOAD_MISSLE_DATABASE:
			::ServerSerialize = true;
			LoadMissileDatabase((sctPtr->m_miscString + ".Server").GetString());
			::ServerSerialize = false;
			break;

		case LOAD_EXPLOSION_DATABASE:
			::ServerSerialize = true;
			LoadExplosionFile((sctPtr->m_miscString + ".Server").GetString());
			::ServerSerialize = false;
			break;

		default:
			ret = ClientBaseEngine::ProcessScriptAttribute(sctPtr);
	}

	// we always spawn, so set these
	GL_spawnGeneratorTimeVarial = fTime::TimeMs();

	return ret;
}

bool AIEngine::SpawnEntityBasedOnPositionAndCfg(Vector3f position, int aiCfg, int dynamicSpawnId, long lifeMaxTime, ZoneIndex zoneIndex) {
	if (!m_AIOL) {
		LogError("AI_List is nil");
		return false;
	}

	CAIObj *aiPtr = m_AIOL->GetByIndex(aiCfg);
	if (!aiPtr) {
		LogError("AI_List->GetByIndex() FAILD");
		return false;
	}

	// idiot protection
	if (lifeMaxTime > 2700000) // TODO: This needs to be in the general game settings.
		lifeMaxTime = 2700000;

	// this spawn cannot preexist in the world-check to see if already spawned
	if (dynamicSpawnId > -1) {
		if (!m_movObjList) {
			LogError("EntObjRuntime is nil");
			return false;
		}

		if (m_movObjList->GetObjectCountByDynaID(dynamicSpawnId) > 2)
			return false;
	}

	// add conditions for not spawning
	Matrix44f spawnMatrix;
	spawnMatrix.MakeTranslation(position);

	// Create New Movement Object
	CMovementObj *refPtr = NULL;
	bool ok = ActivateMovementEntity(
		aiCfg,
		spawnMatrix,
		aiPtr->m_entityCfgDBindexRef, // spawnGenPtr->m_spawnIndex,
		eActorControlType::CONTROL_TYPE_NPC, // hardcode ai to player-spawnGenPtr->m_spawnType,
		0,
		0,
		FALSE,
		FALSE,
		true,
		-1,
		&refPtr,
		NULL,
		-1,
#ifndef REFACTOR_INSTANCEID
		zoneIndex,
#else
		zoneIndex.toLong(),
#endif
		false,
		"");
	if (!ok) {
		LogError("ActivateMovementEntity() FAILED");
		return false;
	}

	if (refPtr->m_aiObj) { // if AI
		refPtr->m_aiObj->m_routeCurTarget = 0;
		refPtr->m_aiObj->m_mapCurLocation = 0;

		refPtr->m_aiObj->m_curMap = aiPtr->m_curMap;
		refPtr->m_dynaSpawnID = dynamicSpawnId;
		refPtr->m_lifeTimeMax = lifeMaxTime;

		if (refPtr->m_aiObj->m_physicsModuleOverride) { // Movment ovverride valid
			if (refPtr->m_movementDB) {
				refPtr->m_movementDB->SafeDelete();
				delete refPtr->m_movementDB;
				refPtr->m_movementDB = 0;
			}

			refPtr->m_aiObj->m_physicsModuleOverride->Clone(&refPtr->m_movementDB);
			refPtr->m_movementDB->m_lastPosition = spawnMatrix.GetTranslation();
		}
	}

	return true;
}

// Return TRUE if handled. Set bDisposeMessage to false if message buffer should not be disposed after call.
BOOL AIEngine::HandleEngineMessages(LPVOID lpMessage, NETID idFromLoc, int msgSize, OUT bool &bDisposeMessage) {
	if (!lpMessage || (msgSize <= 0))
		return FALSE;

	// cast into an identity
	LPCOMMON_HEADER identityMsg = (LPCOMMON_HEADER)lpMessage;

	MSG_CATEGORY category = identityMsg->Category();
	switch (category) {
		case AIDYNSPAWN_TO_CLIENT_CAT: {
			LPAIDYNSPAWN_POS AISpawnMsg = (LPAIDYNSPAWN_POS)lpMessage;
			Vector3f currentPosCnv;
			currentPosCnv.x = AISpawnMsg->m_xPos;
			currentPosCnv.y = AISpawnMsg->m_yPos;
			currentPosCnv.z = AISpawnMsg->m_zPos;
			SpawnEntityBasedOnPositionAndCfg(currentPosCnv, AISpawnMsg->m_aiCfg, AISpawnMsg->m_dynaID, AISpawnMsg->m_lifeLine, AISpawnMsg->m_zoneIndex);
			return TRUE;
		}

		default:
			return ClientBaseEngine::HandleEngineMessages(lpMessage, idFromLoc, msgSize, bDisposeMessage);
	}
}

static bool AITargetGreater(const AITarget &elem1, const AITarget &elem2) {
	return elem1.first > elem2.first;
}

void AIEngine::GetPossibleTargets(IN CMovementObj *ptrLoc, OUT AITargetVector &targets) {
	if (!m_movObjList) {
		LogError("EntObjRuntime is nil");
		return;
	}

	// mainly a copy of the old AI_getTarget, only gets all possible, not just best
	Matrix44f aiMatrix;
	ptrLoc->m_baseFrame->GetTransform(NULL, &aiMatrix);

	Vector3f curPosition;
	ptrLoc->m_baseFrame->GetPosition(NULL, &curPosition);
	CMovementObj *currentTarget = 0;

	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CMovementObj *possibleTarget = (CMovementObj *)m_movObjList->GetNext(posLoc);
		if (possibleTarget->m_energy <= 0) {
			continue;
		}

		if (possibleTarget->m_possedBy) {
			continue;
		}

		if (possibleTarget->m_traceNumber != ptrLoc->m_traceNumber) {
			// start methods for selecting a target
			// if ( targetRetrievalType == 0 )
			{
				if (possibleTarget->m_traceNumber == ptrLoc->m_attackerTrace) {
					currentTarget = possibleTarget;
				}
			}

			int polysChecked = 0;
			int polysConsidered = 0;
			// find nearest enemy
			// if ( targetRetrievalType == 1 )
			{
				ChannelId wid; //wid ?? next method was just passing in zero??
				CSafeZoneObj *possibleSafeZone = m_safeZoneDB->GetSafeZoneByPosition(curPosition, wid);
				if (m_bAllowSafeZoneAttack || !possibleSafeZone) { // disable aggro in safe zones
					if (possibleTarget->m_team != ptrLoc->m_team) { // 4
						Vector3f targetPosition;

						possibleTarget->m_baseFrame->GetPosition(NULL, &targetPosition);

						float distFromTarget = Distance(curPosition, targetPosition);
						float maxAttackRange = min(ptrLoc->m_aiObj->m_safeDistance, ptrLoc->m_aiObj->m_maxSightRadius);

						// if ( distFromTarget < bestDistance || bestDistance == -1.0f )
						{ // better choice
							if (distFromTarget < maxAttackRange) { // within sight range
								Vector3f localPosition = MatrixARB::GetLocalPositionFromMatrixView(aiMatrix, targetPosition);
								if (localPosition.z < 0) { // check to see if target is in front of you
									// end check to see if target is
									// in front
									// of you
									targetPosition.y += 5.0f;

									Vector3f positionWldEyeLevel;
									positionWldEyeLevel.x = curPosition.x;
									positionWldEyeLevel.y = curPosition.y + ptrLoc->m_aiObj->m_eyeLevel; // adjust to eyelevel
									positionWldEyeLevel.z = curPosition.z;

									if (ptrLoc->m_aiObj->CheckVisionIsClear(positionWldEyeLevel, targetPosition, GL_collisionBase, &polysChecked, &polysConsidered)) {
										targets.push_back(AITarget(distFromTarget, dynamic_cast<IGetSet *>(possibleTarget)));
									}
								} // end check to see if target is in front of you
							} // end within sight range
						} // end better choice
					} // end 4
				}
			} // end find nearest enemy

			// if ( targetRetrievalType == 2 )
			{
				// find nearest murderer
				if (possibleTarget->m_murderer) { // 4
					Vector3f targetPosition;

					possibleTarget->m_baseFrame->GetPosition(NULL, &targetPosition);

					float distFromTarget = Distance(curPosition, targetPosition);
					float maxAttackRange = min(ptrLoc->m_aiObj->m_safeDistance, ptrLoc->m_aiObj->m_maxSightRadius);

					// if ( distFromTarget < bestDistance || bestDistance == -1.0f )
					{ // better choice
						if (distFromTarget < maxAttackRange) { // within sight range
							Vector3f localPosition = MatrixARB::GetLocalPositionFromMatrixView(aiMatrix, targetPosition);
							if (localPosition.z < 0) { // check to see if target is in front of you
								// end check to see if target is in
								// front of
								// you
								targetPosition.y += 5.0f;

								Vector3f positionWldEyeLevel;
								positionWldEyeLevel.x = curPosition.x;
								positionWldEyeLevel.y = curPosition.y + ptrLoc->m_aiObj->m_eyeLevel; // adjust to eyelevel
								positionWldEyeLevel.z = curPosition.z;

								if (ptrLoc->m_aiObj->CheckVisionIsClear(positionWldEyeLevel, targetPosition, GL_collisionBase, &polysChecked, &polysConsidered)) {
									targets.push_back(AITarget(distFromTarget, dynamic_cast<IGetSet *>(possibleTarget)));
								}
							} // end check to see if target is in front of you
						} // end within sight range
					} // end better choice
				} // end 4
			} // end find nearest murderer

		} // end start methods for selecting a target
	}

	// sort by distance
	if (targets.size() > 0) {
		std::sort(targets.begin(), targets.end(), AITargetGreater);
	}

	if (currentTarget) {
		Vector3f targetPosition;

		currentTarget->m_baseFrame->GetPosition(NULL, &targetPosition);

		float distFromTarget = Distance(curPosition, targetPosition);

		targets.insert(targets.begin(), AITarget(distFromTarget, dynamic_cast<IGetSet *>(currentTarget)));
	}
}

BOOL AIEngine::AI_getTarget(CMovementObj *ptrLoc, int targetRetrievalType) {
	Matrix44f aiMatrix;
	ptrLoc->m_baseFrame->GetTransform(NULL, &aiMatrix);

	Vector3f curPosition;
	float bestDistance = -1.0f;
	ptrLoc->m_baseFrame->GetPosition(NULL, &curPosition);
	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CMovementObj *possibleTarget = (CMovementObj *)m_movObjList->GetNext(posLoc);
		if (possibleTarget->m_energy <= 0) {
			continue;
		}

		if (possibleTarget->m_possedBy) {
			continue;
		}

		TimeMs timeMs = fTime::TimeMs();
		if (possibleTarget->m_traceNumber != ptrLoc->m_traceNumber) { // start methods for selecting a target
			if (targetRetrievalType == 0) {
				if (possibleTarget->m_traceNumber == ptrLoc->m_attackerTrace) {
					if (ptrLoc->m_targetRef != NULL && ptrLoc->m_aiObj->m_targetInSight == TRUE) { // if already on a target consider switching if existing still seeable
						// use a percentage of switching future
						// variable
						int percentage = 10; // demo of variable 10 percent change of changing targets
						if (rand() % 100 < percentage) { // suceeded in switching targets
							ptrLoc->m_AI_target = possibleTarget->m_traceNumber;
							ptrLoc->m_targetRef = possibleTarget;
							ptrLoc->m_aiObj->EnableRadiusSystem(curPosition, ptrLoc->m_aiObj->m_routeCurTarget, ptrLoc->m_aiObj->m_lookAt);
							ptrLoc->m_aiObj->m_timeStampWhenTargetNotInSight = timeMs;
							ptrLoc->m_aiObj->m_targetInSight = TRUE;
						} // end suceeded in switching targets
					} // end if already on a target consider switching
					else { // if no other target/or not in sight go ahead and switch
						ptrLoc->m_AI_target = possibleTarget->m_traceNumber;
						ptrLoc->m_targetRef = possibleTarget;
						ptrLoc->m_aiObj->EnableRadiusSystem(curPosition, ptrLoc->m_aiObj->m_routeCurTarget, ptrLoc->m_aiObj->m_lookAt);
						ptrLoc->m_aiObj->m_timeStampWhenTargetNotInSight = timeMs;
						ptrLoc->m_aiObj->m_targetInSight = TRUE;
					} // end if no other target go ahead and switch

					return TRUE;
				}
			}

			int polysChecked = 0;
			int polysConsidered = 0;
			if (targetRetrievalType == 1) { // find nearest enemy
				ChannelId wid; //wid ?? next method was just passing in zero??
				CSafeZoneObj *possibleSafeZone = m_safeZoneDB->GetSafeZoneByPosition(curPosition, wid);
				if (m_bAllowSafeZoneAttack || !possibleSafeZone) { // disable aggro in safe zones
					if (possibleTarget->m_team != ptrLoc->m_team) { // 4
						Vector3f targetPosition;

						possibleTarget->m_baseFrame->GetPosition(NULL, &targetPosition);

						float distFromTarget = Distance(curPosition, targetPosition);
						float maxAttackRange = min(ptrLoc->m_aiObj->m_safeDistance, ptrLoc->m_aiObj->m_maxSightRadius);

						if (distFromTarget < bestDistance || bestDistance == -1.0f) { // better choice
							if (distFromTarget < maxAttackRange) { // within sight range
								Vector3f localPosition = MatrixARB::GetLocalPositionFromMatrixView(aiMatrix, targetPosition);
								if (localPosition.z < 0) { // check to see if target is in front of you
									// end check to see if target is
									// in front
									// of you
									targetPosition.y += 5.0f;

									Vector3f positionWldEyeLevel;
									positionWldEyeLevel.x = curPosition.x;
									positionWldEyeLevel.y = curPosition.y + ptrLoc->m_aiObj->m_eyeLevel; // adjust to eyelevel
									positionWldEyeLevel.z = curPosition.z;

									if (ptrLoc->m_aiObj->CheckVisionIsClear(positionWldEyeLevel, targetPosition, GL_collisionBase, &polysChecked, &polysConsidered)) {
										TRACE3("Mob %s considering attacking %s at %02f distance\r\n", ptrLoc->m_name, possibleTarget->m_name, distFromTarget);
										ptrLoc->m_AI_target = possibleTarget->m_traceNumber;
										ptrLoc->m_targetRef = possibleTarget;

										ptrLoc->m_aiObj->EnableRadiusSystem(curPosition,
											ptrLoc->m_aiObj->m_routeCurTarget,
											ptrLoc->m_aiObj->m_lookAt);

										ptrLoc->m_aiObj->m_timeStampWhenTargetNotInSight = timeMs;
										bestDistance = distFromTarget;
									}
								} // end check to see if target is in front of you
							} // end within sight range
						} // end better choice
					} // end 4
				}
			} // end find nearest enemy

			if (targetRetrievalType == 2) { // find nearest murderer
				if (possibleTarget->m_murderer) { // 4
					Vector3f targetPosition;

					possibleTarget->m_baseFrame->GetPosition(NULL, &targetPosition);

					float distFromTarget = Distance(curPosition, targetPosition);
					float maxAttackRange = min(ptrLoc->m_aiObj->m_safeDistance, ptrLoc->m_aiObj->m_maxSightRadius);

					if (distFromTarget < bestDistance || bestDistance == -1.0f) { // better choice
						if (distFromTarget < maxAttackRange) { // within sight range
							Vector3f localPosition = MatrixARB::GetLocalPositionFromMatrixView(aiMatrix, targetPosition);
							if (localPosition.z < 0) { // check to see if target is in front of you
								// end check to see if target is in
								// front of
								// you
								targetPosition.y += 5.0f;

								Vector3f positionWldEyeLevel;
								positionWldEyeLevel.x = curPosition.x;
								positionWldEyeLevel.y = curPosition.y + ptrLoc->m_aiObj->m_eyeLevel; // adjust to eyelevel
								positionWldEyeLevel.z = curPosition.z;

								if (ptrLoc->m_aiObj->CheckVisionIsClear(positionWldEyeLevel, targetPosition, GL_collisionBase, &polysChecked, &polysConsidered)) {
									ptrLoc->m_AI_target = possibleTarget->m_traceNumber;
									ptrLoc->m_targetRef = possibleTarget;

									ptrLoc->m_aiObj->EnableRadiusSystem(curPosition, ptrLoc->m_aiObj->m_routeCurTarget, ptrLoc->m_aiObj->m_lookAt);

									ptrLoc->m_aiObj->m_timeStampWhenTargetNotInSight = timeMs;
									bestDistance = distFromTarget;
								}
							} // end check to see if target is in front of you
						} // end within sight range
					} // end better choice
				} // end 4
			} // end find nearest murderer
		} // end start methods for selecting a target
	}

	// nothin so far
	if (ptrLoc->m_targetRef) {
		return TRUE;
	}

	return FALSE;
}

BOOL AIEngine::AI_VerifyTarget(CMovementObj *ptrLoc) {
	if (!ptrLoc->m_targetRef) {
		return FALSE;
	}

	BOOL targetExists = FALSE;
	CMovementObj *possibleTarget = NULL;
	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		possibleTarget = (CMovementObj *)m_movObjList->GetNext(posLoc);
		if (possibleTarget->m_energy > 0) {
			if (ptrLoc->m_traceNumber != possibleTarget->m_traceNumber) { // do not target self
				if (possibleTarget->m_traceNumber == ptrLoc->m_AI_target) { // found target
					ptrLoc->m_targetRef = possibleTarget;
					targetExists = TRUE;
					break;
				} // end found target
			}
		}
	}

	if (!targetExists) { // disable target
		// ptrLoc->m_aiObj->m_radiusSysInUse = FALSE;
		ptrLoc->m_targetRef = NULL;
		ptrLoc->m_AI_target = -1;
		return FALSE;
	} // end disable target

	// add distance quit
	if (ptrLoc->m_aiObj->m_targetInSight == FALSE) { // target not accessable at this moment eval persistence
		KEP_ASSERT(ptrLoc->m_aiObj->m_timeStampWhenTargetNotInSight >= 0 && ptrLoc->m_aiObj->m_durationTillTargetLost >= 0);
		if (fTime::ElapsedMs(ptrLoc->m_aiObj->m_timeStampWhenTargetNotInSight) > ptrLoc->m_aiObj->m_durationTillTargetLost) { // timed out
			// ptrLoc->m_aiObj->m_radiusSysInUse = FALSE;
			ptrLoc->m_targetRef = NULL;
			ptrLoc->m_AI_target = -1;
			return FALSE;
		} // end timed out
	} // end target not accessable at this moment eval persistence

	return TRUE;
}
//
// ===============================================================================
//    checks Collision between a world object and a entity
// ===============================================================================
//
BOOL AIEngine::RealtimeSegmentTestUseTable(Vector3f &lastPos, Vector3f &curPos) {
	if (!m_multiplayerObj->m_housingDB) {
		return FALSE;
	}

	Vector3f curPosTemp, lastPosTemp;
	CHPlacementObjList *placementList = m_multiplayerObj->m_housingDB->GetContainer(curPos);
	if (placementList) {
		for (POSITION hPos = placementList->GetHeadPosition(); hPos != NULL;) { // Housing
			CHPlacementObj *hsPtr = (CHPlacementObj *)placementList->GetNext(hPos);
			CHousingObj *houseLib = m_housingDB->GetByIndex(hsPtr->m_type);
			if (houseLib) {
				curPosTemp.x = curPos.x - hsPtr->m_wldPosition.x;
				curPosTemp.y = curPos.y - hsPtr->m_wldPosition.y;
				curPosTemp.z = curPos.z - hsPtr->m_wldPosition.z;
				lastPosTemp.x = lastPos.x - hsPtr->m_wldPosition.x;
				lastPosTemp.y = lastPos.y - hsPtr->m_wldPosition.y;
				lastPosTemp.z = lastPos.z - hsPtr->m_wldPosition.z;

				if (houseLib->m_openState) {
					if (houseLib->m_openState->CollisionSegmentIntersectBasic(lastPosTemp, curPosTemp, 20.0f)) {
						return TRUE;
					}
				}
			}
		} // end Housing
	}

	return FALSE;
}

void AIEngine::AdvancedAiSteering(CMovementObj *ptrLoc, Vector3f locationLoc) {
	if (ptrLoc->m_aiObj->m_exponentTurning == TRUE) {
		ptrLoc->m_movementDB->m_curRotY = 0.0f;
	}

	if (locationLoc.z > 0.0f) { // behind you
		if (locationLoc.x > 0) {
			if (ptrLoc->m_aiObj->m_exponentTurning == FALSE) {
				ptrLoc->m_movementDB->TurnLeft();
			} else {
				ptrLoc->m_movementDB->m_curRotY = ptrLoc->m_movementDB->m_maxYaw;
				ptrLoc->m_movementDB->m_yawIsUsed = TRUE;
			}
		} else {
			if (ptrLoc->m_aiObj->m_exponentTurning == FALSE) {
				ptrLoc->m_movementDB->TurnRight();
			} else {
				ptrLoc->m_movementDB->m_curRotY = -ptrLoc->m_movementDB->m_maxYaw;
				ptrLoc->m_movementDB->m_yawIsUsed = TRUE;
			}
		}
	} // end behind you
	else { // steer
		// yaw
		if (locationLoc.x < 0.0f) { // AIPtr->m_turnTolerance){
			if (ptrLoc->m_aiObj->m_exponentTurning == FALSE) {
				ptrLoc->m_movementDB->TurnRight();
			} else {
				if (locationLoc.z != 0.0f) {
					float turnReq = atan(fabs(locationLoc.x) / fabs(locationLoc.z)) * 57.3;

					if (turnReq > ptrLoc->m_movementDB->m_maxYaw) {
						turnReq = ptrLoc->m_movementDB->m_maxYaw;
					}

					ptrLoc->m_movementDB->m_curRotY = -turnReq;
					ptrLoc->m_movementDB->m_yawIsUsed = TRUE;
				}
			}
		} else if (locationLoc.x > 0.0f) {
			// TurnLeft(ptrLoc->m_movementDB);
			if (ptrLoc->m_aiObj->m_exponentTurning == FALSE) {
				ptrLoc->m_movementDB->TurnLeft();
			} else {
				if (locationLoc.z != 0.0f) {
					float turnReq = atan(fabs(locationLoc.x) / fabs(locationLoc.z)) * 57.3;

					if (turnReq > ptrLoc->m_movementDB->m_maxYaw) {
						turnReq = ptrLoc->m_movementDB->m_maxYaw;
					}

					ptrLoc->m_movementDB->m_curRotY = turnReq;
					ptrLoc->m_movementDB->m_yawIsUsed = TRUE;
				}
			}
		}

		// pitch
		if (locationLoc.y < 0.0f) { // pitch dive
			if (ptrLoc->m_aiObj->m_exponentTurning == FALSE) {
				ptrLoc->m_movementDB->Dive();
			} else {
				if (locationLoc.z != 0.0f) {
					float turnReq = atan(fabs(locationLoc.y) / fabs(locationLoc.z)) * 57.3;

					if (turnReq > ptrLoc->m_movementDB->m_maxPitch) {
						turnReq = ptrLoc->m_movementDB->m_maxPitch;
					}

					ptrLoc->m_movementDB->m_curRotX = turnReq;
					ptrLoc->m_movementDB->m_pitchIsUsed = TRUE;
				}
			}
		} // end pitch dive
		else if (locationLoc.y > 0.0f) {
			if (ptrLoc->m_aiObj->m_exponentTurning == FALSE) {
				ptrLoc->m_movementDB->PullUp();
			} else {
				if (locationLoc.z != 0.0f) {
					float turnReq = atan(fabs(locationLoc.y) / fabs(locationLoc.z)) * 57.3;

					if (turnReq > ptrLoc->m_movementDB->m_maxPitch) {
						turnReq = ptrLoc->m_movementDB->m_maxPitch;
					}

					ptrLoc->m_movementDB->m_curRotX = -turnReq;
					ptrLoc->m_movementDB->m_pitchIsUsed = TRUE;
				}
			}
		}
	} // end steer
}

/* //illamas: this is not really used right now
void AIEngine::ProcessActivateMovementEntity( CMovementObj *entityRuntimeObj, int aiCfg, Matrix44f &locationMatrix  )
{
	{				// AI type
		POSITION	aiPos = AI_List->FindIndex ( aiCfg );
		if ( aiPos )
		{
			CAIObj	*aiPtr = (CAIObj *) AI_List->GetAt ( aiPos );
			if ( aiPtr->m_collisionDBOverride > -1 )
			{
				CCollisionObj	*cPtr = m_collisionDatabase->GetByIndex ( aiPtr->m_collisionDBOverride );
				if ( cPtr )
				{
					entityRuntimeObj->m_collisionMovRefPtr = cPtr;
				}
			}

			if ( aiPtr->m_physicsModuleOverride )
			{		// Movment ovverride valid
				if ( entityRuntimeObj->m_movementDB )
				{
					entityRuntimeObj->m_movementDB->SafeDelete ();
					delete entityRuntimeObj->m_movementDB;
					entityRuntimeObj->m_movementDB = 0;
				}

				aiPtr->m_physicsModuleOverride->Clone ( &entityRuntimeObj->m_movementDB );
				entityRuntimeObj->m_movementDB->m_lastPosition.x = locationMatrix._41;
				entityRuntimeObj->m_movementDB->m_lastPosition.y = locationMatrix._42;
				entityRuntimeObj->m_movementDB->m_lastPosition.z = locationMatrix._43;
			}		// end Movment ovverride valid

			aiPtr->Clone ( &entityRuntimeObj->m_aiObj );
			entityRuntimeObj->m_name = aiPtr->m_nameOvveride;
			entityRuntimeObj->m_aiObj->m_lookAt.x = entityRuntimeObj->m_location.x;
			entityRuntimeObj->m_aiObj->m_lookAt.y = entityRuntimeObj->m_location.y;
			entityRuntimeObj->m_aiObj->m_lookAt.z = entityRuntimeObj->m_location.z;
			entityRuntimeObj->m_team = entityRuntimeObj->m_aiObj->m_team;
			if ( entityRuntimeObj->m_aiObj->m_spawnScript != -1 )
			{		// allocates
				entityRuntimeObj->m_aiObj->m_curScript = GL_scrList->GetScriptObject ( entityRuntimeObj->m_aiObj->m_spawnScript );
			}

			if ( entityRuntimeObj->m_aiObj->m_feelerEnable == TRUE )
			{
				entityRuntimeObj->m_aiObj->FeelerSystemContruction ( entityRuntimeObj->m_aiObj->m_feelerRange,
																	 entityRuntimeObj->m_aiObj->m_feelerDepth,
																	 entityRuntimeObj->m_aiObj->m_feelerCycleTime,
																	 entityRuntimeObj->m_aiObj->m_feelerWidth,
																	 entityRuntimeObj->m_aiObj->m_feelerHeight );
			}
		}
	}				// end AI type
}
*/

void AIEngine::DestroyRuntimeEntity(CMovementObj *runtimeEntity) {
	if (spawnGeneratorDB) { //add
		if (runtimeEntity->m_SpawnGeneratorLink != -1) { //was generated
			POSITION posLocal = spawnGeneratorDB->FindIndex(runtimeEntity->m_SpawnGeneratorLink);
			if (posLocal) {
				CSpawnObj *spwnObj = (CSpawnObj *)spawnGeneratorDB->GetAt(posLocal);
				if (spwnObj) {
					spwnObj->m_currentOutPut--;
					LOG4CPLUS_DEBUG(m_logger, "SpawnGen " << spwnObj->m_spawnName << " removed " << spwnObj->m_maxOutPutCapacity << "/" << spwnObj->m_currentOutPut);
				}
			}
		}
	}

	//Clean up targets
	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
		CMovementObj *aiEntity = (CMovementObj *)m_movObjList->GetNext(posLoc);

		if (aiEntity->m_targetRef) {
			if (aiEntity->m_targetRef->m_networkDefinedID == runtimeEntity->m_networkDefinedID) {
				aiEntity->m_targetRef = NULL;
			}
		}
	}

	ClientBaseEngine::DestroyRuntimeEntity(runtimeEntity);
}

static void DestroyPersistList(CPersistObjList *&GL_ScenePersistList) {
	if (GL_ScenePersistList) { //delete persist list
		for (POSITION posLocal = GL_ScenePersistList->GetHeadPosition(); posLocal != NULL;) {
			CPersistObj *perstPtr = (CPersistObj *)GL_ScenePersistList->GetNext(posLocal);
			delete perstPtr;
			perstPtr = 0;
		}
		delete GL_ScenePersistList;
		GL_ScenePersistList = 0;
	} //end delete persist list
}

BOOL AIEngine::LoadScene(const CStringA &fileName, const char *) {
	// particle cleanup
	if (m_curWldObjPrtList) {
		for (POSITION prtPos = m_curWldObjPrtList->GetHeadPosition(); prtPos != NULL;) { // set world particle sys to non regen to flag it
			CWldObject *wldObj = (CWldObject *)m_curWldObjPrtList->GetNext(prtPos);
			if (wldObj->m_runtimeParticleId) {
				m_particleRM->SetRuntimeEffectStatus(wldObj->m_runtimeParticleId, -1, -1, 0);
			}
		} // end set world particle sys to non regen to flag it
	}

	m_curWldObjPrtList->RemoveAll();

	// use gamefiles folder for organization
	CStringA CurDir = PathGameFiles().c_str();

	// preliminary wrapper
	CStringA reserved;

	// only used locally
	CPersistObjList *GL_ScenePersistList = NULL;

	// AI added to make serialize work, not used
	// TODO: figure out how to load scene w/o loading stuff we don't need these
	// TODO: are just thrown away
	CBackGroundObjList *BackGroundList = 0;
	int currentTrack;
	CStringA musicLoopFile;
	int m_globalWorldShader;
	BOOL W_BufferPreffered;
	CEXScriptObjList *SceneScriptExec = 0; // client only?

	// end use gamefiles folder for organization
	BEGIN_CFILE_SERIALIZE(fl, CurDir + fileName + ".Server", false)

	// destroy list that will be filled
	DeleteSpawnDatabase();
	DestroyWorldObjects();
	DestroyEnvironment();
	DestroyEntityRuntimeList();
	DeleteSpawnDatabase();

	// DestroyAllTexturesFromDatabase();
	SafeDeleteList(GL_collisionBase);
	SafeDeleteList(m_libraryReferenceDB);
	SafeDeleteList(GL_aiWebDatabase);
	SafeDeleteList(m_waterZoneDB);
	// end destroy list that will be filled

	::ServerSerialize = true;
	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >>
		m_pWOL >>
		BackGroundList >>
		m_environment >>
		GL_ScenePersistList >>
		currentTrack >>
		musicLoopFile >>
		m_globalWorldShader >>
		W_BufferPreffered >>
		SceneScriptExec >>
		reserved >>
		spawnGeneratorDB >>
		GL_collisionBase >>
		worldGroupList >>
		GL_aiWebDatabase;
	//>>m_libraryReferenceDB ; 6/15/06 ref lib is loaded in scrip instead
	// 10/05 since Water zone loaded in script, don't load
	// the one in zone.  Still save it for compatability with old editors
	//>> m_waterZoneDB;

	the_in_Archive.Close();

	END_CFILE_SERIALIZE(fl, m_logger, "LoadScene")

	if (serializeOk) {
		if (SceneScriptExec)
			DeleteScriptList(&SceneScriptExec);

		if (spawnGeneratorDB) {
#ifndef REFACTOR_INSTANCEID
			spawnGeneratorDB->UpdateChannel(m_channel);
#else
			spawnGeneratorDB->UpdateChannel(m_channel.toLong());
#endif
			spawnGeneratorDB->GetTotalAIOutput(); // spawnGeneratorDB related
		}

		InitializeOjectsIntoScene();
		RebuildTraceNumber(); // rebuild trace numbers

		if (!ClientIsEnabled() && GL_ScenePersistList) { // dont spawn stuff unless server
			UpdateEntityPersist(&GL_ScenePersistList); // fill in runtime entities
		}
		DestroyPersistList(GL_ScenePersistList); // kep

		GL_collisionBase->BuildObjectReferenceArray(m_pWOL);

		return TRUE;
	} else
		return FALSE;
}

void AIEngine::UpdateEntityPersist(CPersistObjList **pList) {
	CPersistObjList *persistList = *pList;
	for (POSITION posLoc = persistList->GetHeadPosition(); posLoc != NULL;) {
		CPersistObj *pstLoc = (CPersistObj *)persistList->GetNext(posLoc);
		POSITION entPositionLoc = m_movObjRefModelList->FindIndex(pstLoc->m_entDBIndex);
		if (entPositionLoc) {
			// Create New Movement Object
			bool ok = ActivateMovementEntity(
				0,
				pstLoc->m_entMatrix,
				pstLoc->m_entDBIndex,
				pstLoc->m_entControlType,
				0,
				0,
				FALSE,
				FALSE,
				false, // TODO: double check this parameter
				-1,
				NULL,
				NULL,
				-1,
#ifndef REFACTOR_INSTANCEID
				m_channel,
#else
				m_channel.toLong(),
#endif
				false,
				"");
			if (!ok) {
				LogError("ActivateMovementEntity() FAILED");
			}
		}
	}
}

HRESULT AIEngine::doAction(IN const char *objectId, IN TiXmlElement *actionNode, IN int id, IN int depth, IN Logger &logger, INOUT string &result) {
	NDCContextCreator ndc(Utf8ToUtf16(m_instanceName));

	HRESULT ret = Engine::doAction(objectId, actionNode, id, depth, logger, result);

	return ret;
}

#include "CBackgroundClass.h"
#include "CPersistClass.h"

/************************
Appends a scene onto an EXISTING scene, this is not tested if you have not already
loaded a scene
Comments:
	if groupmode is specified only objects in that group are imported.
	if aiMergeMode is 1 then referense, aiweb and spawngens are added to the file as well as all objects
*************************/
BOOL AIEngine::AppendScene(CStringA fileName, int aiMergeMode, int groupMode, int transitionGroupID) {
	ScopedDirectoryChanger("GameFiles");
	return FALSE;
}

void AIEngine::UpdateServerCallback() {
	TimeMs timeMs = fTime::TimeMs();

	if (m_pingInQuerry == FALSE || (timeMs - m_networkFreqStamp) > 3000) {
		if ((timeMs - m_networkFreqStamp) > m_multiplayerObj->m_aiserverCycleDuration) { // timed updates
			BOOL regeneratedOne = FALSE;

			// verify outgoing messages first before continueing
			m_networkFreqStamp = timeMs;

			POSITION posLast = NULL;
			BOOL aiIsSpawnedRequiresInfo = FALSE;
			int sendCount = 0;
			for (POSITION posLoc = m_movObjList->GetHeadPosition(); (posLast = posLoc) != NULL;) { // size loop
				CMovementObj *mObj = (CMovementObj *)m_movObjList->GetNext(posLoc);
				if (mObj->m_networkDefinedID > 0) { // has communication ID
					if (mObj->IsTypeNPC())
						sendCount++;
				} else if (mObj->m_networkDefinedID == 0) {
					if (mObj->IsTypeNPC()) {
						if ((timeMs - mObj->m_spawnTimeStamp) > 5000) {
							DestroyRuntimeEntity(mObj);
							mObj = 0;
							m_movObjList->RemoveAt(posLast);
							LOG4CPLUS_INFO(m_multiplayerObj->m_logger, "Retrying spawn");
						}
					}
				}
			} // end size loop

			AI_BLOCKPOSUPDATE_STRUCT *AIBlockMsg = NULL;
			DWORD msgSize = 0;
			if (sendCount > 0) {
				msgSize = AI_BLOCKPOSUPDATE_STRUCT::computeSize(sendCount); // Header + payload
				AIBlockMsg = (AI_BLOCKPOSUPDATE_STRUCT *)new BYTE[msgSize];
				AIBlockMsg->m_header.SetCategory(AI_SHRUNK_MOVE_TO_SERVER_CAT);
			}

			int trace = 0;
			for (POSITION posLoc = m_movObjList->GetHeadPosition(); (posLast = posLoc) != NULL;) { // runtime ai loop
				CMovementObj *mObj = (CMovementObj *)m_movObjList->GetNext(posLoc);

				if (mObj->m_networkDefinedID > 0) { // has communication ID
					if (mObj->IsTypeNPC()) { // AI cfg
						IndexEntityAnimations(mObj, TRUE);

						Vector3f upVect, dirVect, posVect, predictedPosition;
						mObj->m_baseFrame->GetOrientation(&dirVect, &upVect);
						mObj->m_baseFrame->GetPosition(NULL, &posVect);

						if (mObj->m_networkFirstTimeSendingPos) { // init prediction
							predictedPosition.x = posVect.x;
							predictedPosition.y = posVect.y;
							predictedPosition.z = posVect.z;
							mObj->m_networkFirstTimeSendingPos = FALSE;
						} // end init prediction
						else { // use prediction
							if (fabs(mObj->m_movementDB->m_curTransX) > (mObj->m_movementDB->m_maxSpeed * .2f) ||
								fabs(mObj->m_movementDB->m_curTransZ) > (mObj->m_movementDB->m_maxSpeed * .2f)) {
								predictedPosition.x = posVect.x + ((posVect.x - mObj->m_networkLastPositionSent.x) * .5f); // 1.7
								predictedPosition.y = posVect.y + ((posVect.y - mObj->m_networkLastPositionSent.y) * .5f);
								predictedPosition.z = posVect.z + ((posVect.z - mObj->m_networkLastPositionSent.z) * .5f);
							} else {
								predictedPosition.x = posVect.x;
								predictedPosition.y = posVect.y;
								predictedPosition.z = posVect.z;
							}

							// use collision on prediction model
							// build variables
							Matrix44f entMatrix;
							mObj->m_baseFrame->GetTransform(NULL, &entMatrix);

							float characterHeight = mObj->m_runtimeSkeleton->m_box.maxY;
							float characterWidth = mObj->m_runtimeSkeleton->m_box.maxX;
							if (mObj->m_runtimeSkeleton->m_box.maxY > mObj->m_runtimeSkeleton->m_box.maxX) {
								characterWidth = mObj->m_runtimeSkeleton->m_box.maxY;
							}

							VirtualCollisionAdjust(&predictedPosition,
								predictedPosition,
								posVect,
								(int)characterWidth,
								(int)characterHeight,
								entMatrix,
								(int)mObj->m_movementDB->m_antiGravity);
						} // end use prediction

						if (m_logger.isEnabledFor(TRACE_LOG_LEVEL)) {
							static const double MOVE_LIMIT = 100.0;
							if (abs(mObj->m_networkLastPositionSent.x - posVect.x) > MOVE_LIMIT ||
								abs(mObj->m_networkLastPositionSent.y - posVect.y) > MOVE_LIMIT ||
								abs(mObj->m_networkLastPositionSent.z - posVect.z) > MOVE_LIMIT)
								LogTrace("New position for " << mObj->m_name << " is " << (int)posVect.x << "," << (int)posVect.y << "," << (int)posVect.x);
						}
						mObj->m_networkLastPositionSent.x = posVect.x;
						mObj->m_networkLastPositionSent.y = posVect.y;
						mObj->m_networkLastPositionSent.z = posVect.z;

						int animInUseCnv = mObj->m_runtimeSkeleton->GetInUseAnimationGLID(ANIM_PRI);
						if (mObj->m_runtimeSkeleton->GetAnimationDirection() == TRUE) { // playing reverse?
							animInUseCnv = -animInUseCnv;
						}

						aiIsSpawnedRequiresInfo = TRUE;

						MOVE_STRUCT mvMsgBld;
						mvMsgBld.SetCategory(MOVE_CAT);
						mvMsgBld.FreeLookYaw = mObj->m_runtimeSkeleton->m_freeLookYaw;
						mvMsgBld.FreeLookPitch = mObj->m_runtimeSkeleton->m_freeLookPitch;
						mvMsgBld.Xpos = predictedPosition.x;
						mvMsgBld.Ypos = predictedPosition.y;
						mvMsgBld.Zpos = predictedPosition.z;
						ASSERT(mObj->m_networkDefinedID <= SHORT_MAX);
						mvMsgBld.idRefrence = (short)mObj->m_networkDefinedID;
						mvMsgBld.OrientateDirX = dirVect.x;
						mvMsgBld.OrientateDirY = dirVect.y;
						mvMsgBld.OrientateDirZ = dirVect.z;
						mvMsgBld.OrientateUpX = upVect.x;
						mvMsgBld.OrientateUpY = upVect.y;
						mvMsgBld.OrientateUpZ = upVect.z;
						mvMsgBld.bJumping = mObj->m_movementDB->m_currentlyJumping;

						// mvMsgBld.dbIndex = mObj->m_refModel;
						if (mObj->IsVisible()) {
							mvMsgBld.animGLID = animInUseCnv;
							mvMsgBld.animGLID2nd = mObj->m_runtimeSkeleton->GetInUseAnimationGLID(ANIM_SEC);
						} else {
							// Send both GLIDs as 0 if invisible (other clients will handle this)
							mvMsgBld.animGLID = 0;
							mvMsgBld.animGLID2nd = 0;
						}

						m_multiplayerObj->ShrinkData(&mvMsgBld, &AIBlockMsg->m_data[trace]);

						BOOL regenerate = FALSE;

						if (!regeneratedOne) {
							if (mObj->m_AI_target == -1) { // not in battle
								if ((timeMs - mObj->m_spawnTimeStamp) > 10800000)
									regenerate = TRUE;
							}
						}

						if (!regeneratedOne) {
							if (mObj->m_lifeTimeMax != 0) { // ai has a limit on life
								if ((timeMs - mObj->m_spawnTimeStamp) > mObj->m_lifeTimeMax)
									regenerate = TRUE;
							}
						}

						trace++;
						if (mObj->m_energy < 1 || posVect.y < -30000 || regenerate == TRUE) {
							bool removeFromServer = false;

							// dead entity
							if (posVect.y < -30000) { // out of bounds
								CStringA build;
								build.Format("Out of bounds: %s index: %d x: %f y: %f z: %f",
									(const char *)mObj->m_name,
									mObj->m_SpawnGeneratorLink,
									(double)posVect.x,
									(double)posVect.y,
									(double)posVect.z);

								LOG4CPLUS_INFO(m_multiplayerObj->m_logger, build);
								mObj->m_movementDB->m_doGravity = FALSE; // make it dissapear
								mObj->m_energy = 0;
								removeFromServer = true;

							} // end out of bounds

							if (regenerate) { // been in game too long for ai
								regeneratedOne = TRUE;

								LOG4CPLUS_INFO(m_multiplayerObj->m_logger, "Regenerating...:" << mObj->m_name << " index:" << mObj->m_SpawnGeneratorLink);
								mObj->m_movementDB->m_doGravity = FALSE; // make it dissapear
								mObj->m_energy = 0;
								removeFromServer = true;
							} // end been in game too long for ai

							if (removeFromServer) // 6/13/05 added this to have OOB and regenerate remove from server
							{
								SendAttribMsg(NSENDFLAG_GUARANTEED | NSENDFLAG_PRIORITY_HIGH,
									SERVER_NETID,
									GetAttributeType::REMOVE_PLAYER_BY_NETID,
									mObj->m_networkDefinedID,
									0,
									0,
									0,
									NULL);
							}

							if (mObj->m_movementDB->m_doGravity == FALSE || mObj->m_movementDB->m_inLiquid == TRUE ||
								mObj->m_movementDB->m_floatWhenDead == TRUE) { // not in air
								// remove entity
								DestroyRuntimeEntity(mObj);
								mObj = 0;
								m_movObjList->RemoveAt(posLast);
								continue;

								// end remove entity
							} // end not in air
						} // end dead entity
					} // end AI cfg
				}
			} // end runtime ai loop

			// send block data
			if (trace > 0) { // send block update
				if (AIBlockMsg) {
					m_multiplayerObj->SendAIBlockMsg(NSENDFLAG_GUARANTEED | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, AIBlockMsg, msgSize);
					delete AIBlockMsg;
					AIBlockMsg = NULL;
				}
			} // end send block update

			// ask server for update
			if (aiIsSpawnedRequiresInfo) {
				SendAttribMsg(NSENDFLAG_GUARANTEED | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, GetAttributeType::AI_REQUEST_UPDATE, 0, 0, 0, 0, NULL);

				m_pingPersistStamp = timeMs; // simulate ping here fakers
				m_pingInQuerry = TRUE;
			}

			SendGuaranteedCycleMsgToServer();

			// end ask server for update
		} // end timed updates
	} else { // just keep animating for upkeep of fire ques
		for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) { // runtime ai loop
			CMovementObj *mObj = (CMovementObj *)m_movObjList->GetNext(posLoc);
			if (mObj->m_networkDefinedID > 0) { // has communication ID
				if (mObj->IsTypeNPC()) { // AI cfg
					IndexEntityAnimations(mObj, TRUE);
				} // end AI cfg
			}
		} // end runtime ai loop
	} // end just keep animating for upkeep of fire ques
}

bool AIEngine::GenAndMount(CMovementObj *hostPtr, int aiIndex, int mountType /*= MT_RIDE*/, bool bLocal /*= false*/, OUT CMovementObj **outNewEntity /*= NULL */) {
	CMovementObj *pNewEntity = NULL;
	bool ok = ClientBaseEngine::GenAndMount(hostPtr, aiIndex, mountType, bLocal, &pNewEntity);
	if (ok && pNewEntity)
		IndexEntityAnimations(pNewEntity, TRUE);
	return ok;
}

BOOL AIEngine::RealtimeSegmentTest(const Vector3f &lastPos, const Vector3f &curPos, Vector3f *intersect, Vector3f *normal, int *polysChecked, int *polysConsidered) {
	int objectHit = 0;

	// use tabled mode
	if (!m_multiplayerObj->m_housingDB) {
		return FALSE;
	}

	CHPlacementObjList *placementList = m_multiplayerObj->m_housingDB->GetContainer(curPos);
	if (placementList) {
		for (POSITION hPos = placementList->GetHeadPosition(); hPos != NULL;) { // Housing
			CHPlacementObj *hsPtr = (CHPlacementObj *)placementList->GetNext(hPos);
			CHousingObj *houseLib = m_housingDB->GetByIndex(hsPtr->m_type);
			if (houseLib) {
				// Transform collision path into object space (translation only?)
				Vector3f pathStart = lastPos - hsPtr->m_wldPosition;
				Vector3f pathEnd = curPos - hsPtr->m_wldPosition;

				if (TRUE == houseLib->m_openState->CollisionSegmentIntersect(pathStart, pathEnd, 20.0f, true, false, intersect, normal, &objectHit, polysChecked, polysConsidered)) {
					// Transform intersection back to world space
					*intersect += hsPtr->m_wldPosition;
					return TRUE;
				}
			}
		} // end Housing
	}

	return FALSE;
}

void AIEngine::RealtimeEntityCollisionCallback() {
	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
		// Begin entity loop
		CMovementObj *entLoc = (CMovementObj *)m_movObjList->GetNext(posLoc);
		if (entLoc->m_posses || entLoc->m_networkEntity || entLoc->m_filterCollision || !entLoc->m_runtimeSkeleton) {
			continue;
		}

		if (!entLoc->IsType15() && !entLoc->IsTypeStatic()) {
			// tabled mode for ai
			if (m_multiplayerObj->m_housingDB) {
				// house table
				Vector3f wldPosition;
				entLoc->m_baseFrame->GetPosition(NULL, &wldPosition);

				CHPlacementObjList *placementList = m_multiplayerObj->m_housingDB->GetContainer(wldPosition);
				if (placementList) {
					for (POSITION hPos = placementList->GetHeadPosition(); hPos != NULL;) {
						// Housing
						CHPlacementObj *hsPtr = (CHPlacementObj *)placementList->GetNext(hPos);
						CHousingObj *houseLib = m_housingDB->GetByIndex(hsPtr->m_type);
						if (houseLib) {
							Matrix44f regMatrix, inverseMatrix;
							regMatrix.MakeTranslation(hsPtr->m_wldPosition);
							inverseMatrix.MakeTranslation(-hsPtr->m_wldPosition);

							if (hsPtr->m_open) {
								HandleMovCollisionToSystem(entLoc, houseLib->m_openState, &regMatrix, &inverseMatrix);
							} else {
								HandleMovCollisionToSystem(entLoc, houseLib->m_closedState, &regMatrix, &inverseMatrix);
							}
						}
					} // end Housing
				}
			} // end house table
		}
	}
}

IGetSet *AIEngine::GetEnvironment() {
	return m_environment;
}

// use 3200 to run getset.pl
BEGIN_GETSET_IMPL(AIEngine, IDS_AIENGINE_NAME)
GETSET(m_instanceName, gsString, GS_FLAGS_DEFAULT | GS_FLAG_INSTANCE_NAME, 0, 0, ENGINE, NAME)
GETSET_CUSTOM(0, gsString, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, ENGINE, BASEDIR, 0, 0)
GETSET(m_autoexec, gsString, GS_FLAGS_DEFAULT, 0, 0, ENGINE, AUTOEXEC)
GETSET_CUSTOM(&m_engineType, gsListOfValues, GS_FLAGS_DEFAULT | GS_FLAG_ADDONLY, 0, 0, ENGINE, TYPE, 0, 3);

GETSET(m_enabled, gsBool, GS_FLAGS_DEFAULT, 0, 0, ENGINEINFO, ENABLED)
GETSET_CUSTOM(0, gsBool, GS_FLAG_EXPOSE, 0, 0, ENGINE, RUNNING, 0, 0)
GETSET(m_ipAddress, gsString, GS_FLAGS_DEFAULT, 0, 0, AIENGINE, IPADDRESS)
GETSET_RANGE(m_port, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIENGINE, PORT, 1000, 99999)
GETSET_RANGE(m_aiThrottle, gsUlong, GS_FLAGS_DEFAULT, 0, 0, AIENGINE, FPS, 1, 1000)

GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, ADMIN, DELETE)
END_GETSET_IMPL

#endif
