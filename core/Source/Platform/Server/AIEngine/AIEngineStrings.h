///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDS_STRING101 101
#define IDS_ERR_INVALID_AI_CFG_AI 12000
#define IDS_ERR_INVALID_AI_CFG_SPAWrN 12001
#define IDS_ERR_INVALID_AI_CFG_SPAWN 12001
#define IDS_ERR_INVALID_SPAWN_GEN 12002
#define IDS_ERR_NO_AI_CONNECTION 12003
#define IDS_INFO_AI_CONNECT 12004
#define IDS_SPAWNGEN_FAILED 12005
#define IDS_ARCHIVE_ERROR 12006
#define IDS_UNARCHIVE_ERROR 12006

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE 102
#define _APS_NEXT_COMMAND_VALUE 40001
#define _APS_NEXT_CONTROL_VALUE 1001
#define _APS_NEXT_SYMED_VALUE 101
#endif
#endif
