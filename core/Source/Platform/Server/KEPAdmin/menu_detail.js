function mmLoadMenus() {
 window.mm_menu_products = new Menu("root",180,20,"Arial, Helvetica, sans-serif",11,"#00559C","#ffffff","#CECCCC","#C72C2C","left","middle",2,0,1000,-5,7,true,true,true,0,false,false);
  mm_menu_products.addMenuItem("Overview","location='../products/index.php'");
  mm_menu_products.addMenuItem("Klaus Entertainment Platform (KEP)","location='../products/kep.php'");
  mm_menu_products.fontWeight="normal";
  mm_menu_products.hideOnMouseOut=true;
  mm_menu_products.bgColor='#FFFFFF';
  mm_menu_products.menuBorder=0;
  mm_menu_products.menuLiteBgColor='#ffffff';
  mm_menu_products.menuBorderBgColor='#FFFFFF';
   
 window.mm_menu_services = new Menu("root",125,20,"Arial, Helvetica, sans-serif",11,"#00559C","#ffffff","#CECCCC","#C72C2C","left","middle",2,0,1000,-5,7,true,true,true,0,true,false);
  mm_menu_services.addMenuItem("Overview","location='../services/index.php'");
  mm_menu_services.addMenuItem("Managed Services","location='../services/managed.php'");
  mm_menu_services.addMenuItem("Kaneva Community","location='../services/kaneva.php'");
  mm_menu_services.fontWeight="normal";
  mm_menu_services.hideOnMouseOut=true;
  mm_menu_services.bgColor='#FFFFFF';
  mm_menu_services.menuBorder=0;
  mm_menu_services.menuLiteBgColor='#ffffff';
  mm_menu_services.menuBorderBgColor='#FFFFFF';
  
 window.mm_menu_studio = new Menu("root",140,20,"Arial, Helvetica, sans-serif",11,"#00559C","#FFFFFF","#CECCCC","#C72C2C","left","middle",2,0,1000,-5,7,true,true,true,0,false,false);
  mm_menu_studio.addMenuItem("Overview","location='../studio/index.php'");
  mm_menu_studio.fontWeight="normal";
  mm_menu_studio.hideOnMouseOut=true;
  mm_menu_studio.bgColor='#FFFFFF';
  mm_menu_studio.menuBorder=0;
  mm_menu_studio.menuLiteBgColor='#ffffff';
  mm_menu_studio.menuBorderBgColor='#FFFFFF';

window.mm_menu_partners = new Menu("root",124,20,"Arial, Helvetica, sans-serif",11,"#00559C","#FFFFFF","#CECCCC","#C72C2C","left","middle",2,0,1000,-5,7,true,true,true,0,false,false);
  mm_menu_partners.addMenuItem("Overview","location='../partners/index.php'");
  mm_menu_partners.addMenuItem("Studio","location='../partners/studio.php'");
  mm_menu_partners.addMenuItem("Distribution","location='../partners/distribution.php'");
  mm_menu_partners.addMenuItem("Education","location='../partners/edu.php'");
  mm_menu_partners.addMenuItem("Technology","location='../partners/technology.php'");
  mm_menu_partners.fontWeight="normal";
  mm_menu_partners.hideOnMouseOut=true;
  mm_menu_partners.bgColor='#FFFFFF';
  mm_menu_partners.menuBorder=0;
  mm_menu_partners.menuLiteBgColor='#ffffff';
  mm_menu_partners.menuBorderBgColor='#FFFFFF';
      
   
window.mm_menu_support = new Menu("root",120,20,"Arial, Helvetica, sans-serif",11,"#00559C","#FFFFFF","#CECCCC","#C72C2C","left","middle",2,0,1000,-5,7,true,true,true,0,false,false);
  mm_menu_support.addMenuItem("Support Center","location='../support/index.php'");
  mm_menu_support.fontWeight="normal";
  mm_menu_support.hideOnMouseOut=true;
  mm_menu_support.bgColor='#FFFFFF';
  mm_menu_support.menuBorder=0;
  mm_menu_support.menuLiteBgColor='#ffffff';
  mm_menu_support.menuBorderBgColor='#FFFFFF';

  window.mm_menu_company = new Menu("root",145,20,"Arial, Helvetica, sans-serif",11,"#00559C","#FFFFFF","#CECCCC","#C72C2C","left","middle",2,0,1000,-5,7,true,true,true,0,false,false);
  mm_menu_company.addMenuItem("About Klaus Entertainment","location='../company/index.php'");
  mm_menu_company.addMenuItem("Management","location='../company/management.php'");
  mm_menu_company.addMenuItem("Contact Us","location='../company/contactus.php'");
  mm_menu_company.addMenuItem("Careers","location='../company/careers.php'");
  mm_menu_company.fontWeight="normal";
  mm_menu_company.hideOnMouseOut=true;
  mm_menu_company.bgColor='#FFFFFF';
  mm_menu_company.menuBorder=0;
  mm_menu_company.menuLiteBgColor='#ffffff';
  mm_menu_company.menuBorderBgColor='#FFFFFF';

  window.mm_menu_newsevents = new Menu("root",135,20,"Arial, Helvetica, sans-serif",11,"#00559C","#FFFFFF","#CECCCC","#C72C2C","left","middle",2,0,1000,-5,7,true,true,true,0,false,false);
  mm_menu_newsevents.addMenuItem("News & Announcements","location='../news/index.php'");
  mm_menu_newsevents.fontWeight="normal";
  mm_menu_newsevents.hideOnMouseOut=true;
  mm_menu_newsevents.bgColor='#FFFFFF';
  mm_menu_newsevents.menuBorder=0;
  mm_menu_newsevents.menuLiteBgColor='#ffffff';
  mm_menu_newsevents.menuBorderBgColor='#FFFFFF';
  
  mm_menu_newsevents.writeMenus();
} // mmLoadMenus()
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_nbGroup(event, grpName) { //v6.0
  var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])? args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) {
      img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr)
      for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}