<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;">]> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="http://schemas.microsoft.com/intellisense/ie5">

<xsl:include href="str.find-last.xslt" />

<xsl:template match="/">
					<xsl:if test="ActionResult/@rc = '-2147023728'">
						<tr>
						<td width="20">&nbsp;</td>
						<td>Object was not found on server.<br>It may have been deleted.  Try refreshing the web page and try again.</br></td>
						</tr>
					</xsl:if>
					<xsl:if test="ActionResult/@rc = '157'"> <!-- logon timeout -->
							<script>alert( "Session timeout from server.  Click ok to go to logon screen." );
							window.open( "default.htm", "_top" );</script>
					</xsl:if>
					<xsl:if test="ActionResult/@rc != '0' and ActionResult/@rc != '-2147023728'">
						<tr>
						<td width="20">&nbsp;</td>
						<td><b>Error from server.</b><br/><xsl:value-of select="ActionResult" ></xsl:value-of><br/>Result code is <xsl:value-of select="ActionResult/@rc" ></xsl:value-of></td>
						</tr>
					</xsl:if>
					<xsl:if test="ActionResult/@rc = '0'">
						<xsl:if test="ActionResult/@rc = '0' and count(ActionResult/GetSetRec/GetSetRec[@type='205']) &gt; 0"> 
						<TR bgcolor="#aaaaaa">
							<TD width="20">&nbsp;</TD>
							<TD class="title1" style="color: #ffffff;" colspan="2" align="left" width="100%"><img src="images/spacer.gif" width="1" height="30"/>ACTIONS: <br></br><xsl:apply-templates select="ActionResult/GetSetRec/GetSetRec[@type='205']" />
							</TD>
						</TR>
						</xsl:if>
						<tr>
						<td colspan="3" width="100%"><img src="spacer.gif" width="1" height="2"/></td>
						</tr>
						<xsl:if test="ActionResult/@rc = '0' and count(ActionResult/GetSetRec/GetSetRec[@groupId='-1']) &gt; 0">
						<TR>
							<TD width="20" bgcolor="#B5C0A1">&nbsp;</TD>
							<TD class="title1" bgcolor="#B5C0A1" colspan="2" align="left"><img src="images/spacer.gif" width="1" height="30"/>STATUS: &nbsp;&nbsp;<input id="statusRefresh" type="submit" runat="server" value="Refresh" style="cursor: hand;">
								<xsl:attribute name="onclick">javascript:document.ActionsForm.whoclicked.value='17332|<xsl:value-of select='ActionResult/GetSetRec/@objectId' />'</xsl:attribute> <!-- 17332 is refresh -->
							</input>
							</TD>
						</TR>
						<TR>
							<TD width="20">&nbsp;</TD>
							<TD align="left" colspan="2">		
								<xsl:apply-templates select="ActionResult/GetSetRec/GetSetRec[@groupId='-1']" />
							</TD>
						</TR>
						</xsl:if>

						<tr bgcolor="#B5C0A1">					
							<TD width="20">&nbsp;</TD>
							<!-- this monster if suppresses the save button if no data, and no enabled simple data -->
							<TD colspan="2" align="left"><img src="images/spacer.gif" width="1" height="30"/><span class="title1">DATA: </span>
							<xsl:if test="count(ActionResult/GetSetRec/GetSetRec[(floor(@flags div 65536) mod 2 = 1) and (not(@groupId) or @groupId &gt;= 0)  and @type &lt; 205]) &gt; 0">
							<input id="saveData" type="submit" runat="server" style="cursor: hand;">
								<xsl:choose>
								<xsl:when test="ActionResult/GetSetRec/@objectId = '-1'">
									<xsl:attribute name="value">Save New</xsl:attribute>
									<xsl:attribute name="onclick">javascript:document.ActionsForm.whoclicked.value='17334|<xsl:value-of select='ActionResult/GetSetRec/@parentObjectId' />|<xsl:value-of select='ActionResult/GetSetRec/@arrayId' />'</xsl:attribute> <!-- 17334 is add -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="value">Save</xsl:attribute>
									<xsl:attribute name="onclick">javascript:document.ActionsForm.whoclicked.value='17338|<xsl:value-of select='ActionResult/GetSetRec/@objectId' />'</xsl:attribute> <!-- 17338 is save -->
								</xsl:otherwise>
								</xsl:choose>
							</input>
							</xsl:if>
							</TD>
						</tr>
						<tr bgcolor="#cccccc">
						<td colspan="3">&nbsp;</td>
						</tr>								
									<xsl:choose>
									<xsl:when test="count(ActionResult/GetSetRec/GetSetRec[(not(@groupId) or @groupId &gt;= 0)  and @type != '205']) &gt; 0">
											<xsl:apply-templates select="ActionResult/GetSetRec/GetSetRec[(not(@groupId) or @groupId &gt;= 0) and @type != '205' and @type != '301' and @type != '401']"/>
									</xsl:when>
									<xsl:otherwise>
										No Data
									</xsl:otherwise>
									</xsl:choose>			
						<tr>
						<td colspan="3">
						<br/><br/><br/>
						</td>
						</tr>
					</xsl:if>
</xsl:template>

<!-- default case if nothing below matches -->
<xsl:template match="GetSetRec">
<xsl:param name="indent" />
<xsl:if test="@name and @name != 'n/a' and floor(@flags div 131072) mod 2 = 1">  <!-- bit test for EXPOSED 0x20000 -->


	<tr> 
		<!-- 12/05 updated to remove redundant code for even/odd class -->
		<xsl:choose>
			<xsl:when test="position() mod 2 = 0">
					<xsl:attribute name="class">rowEven</xsl:attribute>
			</xsl:when>
			<xsl:when test="position() mod 2 = 1">
					<xsl:attribute name="class">rowOdd</xsl:attribute>
			</xsl:when>
		</xsl:choose>
			
			<td><img src="images/spacer.gif" width="1" height="23"/></td>

			<!-- array of children -->
			<xsl:if test="@type='401' or @type='301'">
			<!-- do nothing -->
			</xsl:if>		

			<!-- boolean -->
			<xsl:if test="@type='1' or @type='2'">
						
				<td valign="middle" align="right" class="title2" width="200" nowrap="yes">
				<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
					<xsl:value-of select="@name" />:&nbsp;
					
				</td><td valign="middle" width="100%">
					<asp:CheckBox runat="server">
					<xsl:attribute name="checked">
					<xsl:if test=".='1'">true</xsl:if>
					<xsl:if test=".!='1'">false</xsl:if>
					</xsl:attribute>		
					<xsl:attribute name="id">id_<xsl:call-template name="substring-after-last">
														<xsl:with-param name="input" select="../@objectId" />
														<xsl:with-param name="substr" select="string('_')"/>
													</xsl:call-template>_<xsl:value-of select="@id" />
					</xsl:attribute>
					<xsl:if test="floor(@flags div 65536) mod 2 = 0">
						<xsl:attribute name="disabled">true</xsl:attribute>
					</xsl:if>
					</asp:CheckBox>	
				</td>					
			</xsl:if>		

			<!-- string -->
			<xsl:if test="@type='15' or @type='16'"> 
				<td valign="middle" align="right" class="title2" nowrap="yes">
				<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
				<xsl:value-of select="@name" />:&nbsp;
				</td><td valign="middle" width="100%">
				<!-- editable? -->
				<xsl:if test="floor(@flags div 65536) mod 2 = 1">
					<xsl:choose>
					<xsl:when test="floor(@flags div 32768) mod 2 = 1">
						<asp:TextBox runat="server" TextMode="Password">
							<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="." /></xsl:attribute>
							<xsl:attribute name="id">id_<xsl:call-template name="substring-after-last">
															<xsl:with-param name="input" select="../@objectId" />
															<xsl:with-param name="substr" select="string('_')"/>
														</xsl:call-template>_<xsl:value-of select="@id" />
							</xsl:attribute>
						</asp:TextBox>
					</xsl:when>
					<xsl:otherwise>
						<asp:TextBox runat="server">
							<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="." /></xsl:attribute>
							<xsl:attribute name="id">id_<xsl:call-template name="substring-after-last">
															<xsl:with-param name="input" select="../@objectId" />
															<xsl:with-param name="substr" select="string('_')"/>
														</xsl:call-template>_<xsl:value-of select="@id" />
							</xsl:attribute>
						</asp:TextBox>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="floor(@flags div 65536) mod 2 = 0">
					<xsl:value-of select="." />
				</xsl:if>			
				</td>
			</xsl:if>		



			<!-- floating point numbers -->
			<xsl:if test="@type='13' or @type='14'">
				<td valign="middle" align="right" class="title2" nowrap="yes">
				<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
				<xsl:value-of select="@name" />:&nbsp;
				</td><td valign="middle" width="100%">
				<!-- editable? -->
				<xsl:if test="floor(@flags div 65536) mod 2 = 1">
					<input type="text"  runat="server">
						<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="." /></xsl:attribute>
						<xsl:attribute name="id">id_<xsl:call-template name="substring-after-last">
														<xsl:with-param name="input" select="../@objectId" />
														<xsl:with-param name="substr" select="string('_')"/>
													</xsl:call-template>_<xsl:value-of select="@id" />
						</xsl:attribute>
					</input>
				<br/>
				</xsl:if>
				<xsl:if test="floor(@flags div 65536) mod 2 = 0">
					<xsl:value-of select="." />
				</xsl:if>				
				<br/>
				</td>
			</xsl:if>


			<!-- integral numbers -->
			<xsl:if test="(@type &gt;='4' and @type &lt;= '12')">
				<td valign="middle" align="right" class="title2" nowrap="yes">
				<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
				<xsl:value-of select="@name" />:&nbsp;
				</td><td valign="middle" width="100%">
				<!-- editable? -->
				<xsl:if test="floor(@flags div 65536) mod 2 = 1">
					<input type="text" runat="server" class="inputField">
						<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="." /></xsl:attribute>
						<xsl:attribute name="id">id_<xsl:call-template name="substring-after-last">
														<xsl:with-param name="input" select="../@objectId" />
														<xsl:with-param name="substr" select="string('_')"/>
													</xsl:call-template>_<xsl:value-of select="@id" />
						</xsl:attribute>
					</input>
				</xsl:if>
				<xsl:if test="floor(@flags div 65536) mod 2 = 0">
					<xsl:value-of select="." />
				</xsl:if>
				</td>
			</xsl:if>

			<!-- 3d Vector -->
			<xsl:if test="@type='118'">
				<td valign="middle" align="right" class="title2" nowrap="yes">
				<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
				<xsl:value-of select="@name" /> (y is up):&nbsp;<br></br>
				</td><td valign="middle" width="100%">
				<!-- editable? -->
				<xsl:if test="floor(@flags div 65536) mod 2 = 1">
					&nbsp;&nbsp;<b>X:</b>&nbsp;
					<input type="text" runat="server" class="inputField">
						<xsl:attribute name="value"><xsl:value-of select="x" /></xsl:attribute>
						<xsl:attribute name="id">d3_<xsl:call-template name="substring-after-last">
														<xsl:with-param name="input" select="../@objectId" />
														<xsl:with-param name="substr" select="string('_')"/>
													</xsl:call-template>_<xsl:value-of select="@id" />_x</xsl:attribute>
					</input>
					<br></br>&nbsp;&nbsp;<b>Y:</b>&nbsp;
					<input type="text" runat="server" class="inputField">
						<xsl:attribute name="value"><xsl:value-of select="y" /></xsl:attribute>
						<xsl:attribute name="id">d3_<xsl:call-template name="substring-after-last">
														<xsl:with-param name="input" select="../@objectId" />
														<xsl:with-param name="substr" select="string('_')"/>
													</xsl:call-template>_<xsl:value-of select="@id" />_y</xsl:attribute>
					</input>
					<br></br>&nbsp;&nbsp;<b>Z:</b>&nbsp;
					<input type="text" runat="server" class="inputField">
						<xsl:attribute name="value"><xsl:value-of select="z" /></xsl:attribute>
						<xsl:attribute name="id">d3_<xsl:call-template name="substring-after-last">
														<xsl:with-param name="input" select="../@objectId" />
														<xsl:with-param name="substr" select="string('_')"/>
													</xsl:call-template>_<xsl:value-of select="@id" />_z</xsl:attribute>
					</input>
				</xsl:if>
				<xsl:if test="floor(@flags div 65536) mod 2 = 0">
					X=<xsl:value-of select="x" />, Y=<xsl:value-of select="y" />, Z=<xsl:value-of select="z" />
				</xsl:if>				
				</td>
			</xsl:if>

			<!-- list of values  -->
			<xsl:if test="@type='203'">
				<td valign="middle" align="right" class="title2">
				<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
				<xsl:value-of select="@name" />:&nbsp;
				</td><td valign="middle">
				<xsl:choose>
				<xsl:when test="../@objectId=-1 or floor(@flags div 4194304) mod 2 = 0"> <!-- show lb if add, or add only bit cleared -->
				<!-- editable? -->
				<select runat="server">
					<!-- x is separator since underscore is object id -->
					<xsl:attribute name="id">lbx<xsl:value-of select="@listKeeper" />x<xsl:value-of select="@arrayId" /></xsl:attribute>
					<xsl:attribute name="title"><xsl:value-of select="@help" /></xsl:attribute>
					<xsl:if test="floor(@flags div 65536) mod 2 = 0">
						<xsl:attribute name="disbled">1</xsl:attribute>
					</xsl:if>
					<xsl:if test="./ItemList/@size &gt; 0">
						<xsl:attribute name="size"><xsl:value-of select="./ItemList/@size" /></xsl:attribute>
						<xsl:for-each select="./ItemList/Item">
							<option><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute><xsl:value-of select="."/></option> 
						</xsl:for-each>
					</xsl:if>
				</select>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="." />
				</xsl:otherwise>
				</xsl:choose>
				<br/>
				</td>
			</xsl:if>

			<!-- object -->
			<xsl:if test="@type='400' or @type='300'">
				<!-- do nothing -->
			</xsl:if>	
			<!-- status -->
		</tr>
		<!--
			<xsl:if test="@groupId='-1'">	
			<tr>
			<td></td>
									
				<xsl:value-of select="$indent" />
				<xsl:if test="floor(@flags div 131072) mod 2 = 1">			 
					<td align="right"><b><xsl:value-of select="@name" />:</b></td>				
					<td>&nbsp;&nbsp;<xsl:value-of select="." /><br/></td>
				</xsl:if>	
			
			</tr>		
			</xsl:if>
		-->
			
</xsl:if>
</xsl:template>

			<xsl:template match="GetSetRec[@type = '205']">
			
				<xsl:if test="floor(@flags div 131072) mod 2 = 1">  <!-- bit test for EXPOSED 0x20000 -->
						&nbsp;&nbsp;					
					<input type="submit" runat="server" style="cursor: hand;">
						<xsl:attribute name="title"><xsl:value-of select='@help' /></xsl:attribute> 
						<xsl:if test="Enabled='0'"><xsl:attribute name="disabled">true</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="id">button<xsl:value-of select="position()" /></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="@name" /></xsl:attribute>
						<xsl:choose>
							<xsl:when test="@objectId">
								<xsl:attribute name="onclick">javascript:document.ActionsForm.whoclicked.value='<xsl:value-of select='@id' />|<xsl:value-of select='@objectId' />|<xsl:call-template name="substring-after-last">
																																													<xsl:with-param name="input" select="../@objectId" />
																																													<xsl:with-param name="substr" select="string('_')"/>
																																												</xsl:call-template>'</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="onclick">javascript:document.ActionsForm.whoclicked.value='<xsl:value-of select='@id' />|<xsl:value-of select='../@objectId' />'</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</input> <!-- add any parameters that are under this one --><xsl:if test="count(./GetSetRec) &gt; 0" ><table cellSpacing="0" cellPadding="0" ><td width="50"></td><td><table cellSpacing="0" cellPadding="0" >
						<xsl:apply-templates select="./GetSetRec" />
						</table></td>
						</table>
					</xsl:if>
					
														
				</xsl:if>
				
			</xsl:template>
		
</xsl:stylesheet>
 
