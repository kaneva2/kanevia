<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >


<xsl:template name="substring-before-last"> 
  <xsl:param name="input"/>
  <xsl:param name="substr"/>
  
  <xsl:if test="$substr and contains($input, $substr)">
    <xsl:variable name="temp" select="substring-after($input, $substr)" />
    <xsl:value-of select="substring-before($input, $substr)" />
    <xsl:if test="contains($temp, $substr)">
      <xsl:value-of select="$substr" />
      <xsl:call-template name="substring-before-last">
        <xsl:with-param name="input" select="$temp" />
        <xsl:with-param name="substr" select="$substr" />
      </xsl:call-template>
    </xsl:if>
  </xsl:if>
  
</xsl:template>


<xsl:template name="substring-after-last">
  <xsl:param name="input"/>
  <xsl:param name="substr"/>
  
  <!-- Extract the string which comes after the first occurence -->
  <xsl:variable name="temp" select="substring-after($input,$substr)"/>
  
  <xsl:choose>
  	<xsl:when test="$substr and contains($temp,$substr)">
  		<xsl:call-template name="substring-after-last">
  			<xsl:with-param name="input" select="$temp"/>
  			<xsl:with-param name="substr" select="$substr"/>
  		</xsl:call-template>
  	</xsl:when>
  	<xsl:otherwise>
  		<xsl:value-of select="$temp"/>
  	</xsl:otherwise>
  </xsl:choose>
</xsl:template> 
 
</xsl:stylesheet>
