///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net.Sockets;
using System.Xml;
using System.Net;

namespace KEPAdmin
{
	/// <summary>
	/// This is a wrapper over the socket-based KEPServer for admin purposes
	/// </summary>
	public class KEPServer
	{
		static private int BUFF_SIZE = 1024;

#if DEBUG
		static private int RECV_TIMEOUT = 500000;
#else
		static private int RECV_TIMEOUT = 5000;
#endif

		// copied from Controller.cpp
		const int CONTROLLER_START_SERVICE		= 2000;
		const int CONTROLLER_STOP_SERVICE		= 2001;
		const int CONTROLLER_GET_STATUS			= 2002;

		public KEPServer()
		{
			m_server = "";
			m_serverPort		= Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["KEPServer.port"]);
            m_controllerPort = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["KEPController.port"]);
		}

		/// <summary>
		/// login to the server
		/// </summary>
		/// <param name="userName">the username that must be an admin</param>
		/// <param name="password"></param>
		public void Login( string userName, string password, string server )
		{
			m_server = server;

			// save since may need to log in to server later, after it starts
			m_userName = userName;
			m_password = password; 

			m_serverRunning = true; // default to true so can call this to login just to server

			if ( m_controllerCookie.Length == 0 )
			{
				m_serverRunning = false;

				string xml = "<Action userId=\"" + userName + "\" password=\"" + password + "\" id=\"" + KEPAdminPage.GET_DATA + "\" objectId=\"0\"/>";

				// first send to the Controller since it should be up if trying to talk to it
				string resultXML = sendRequest( xml, true, m_controllerPort );

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml( resultXML );

				XmlNode root = xmlDoc.ChildNodes[0];
				int rc = Int32.Parse(root.Attributes["rc"].Value);
				if ( rc == 0 )
				{
					m_controllerCookie = root.Attributes["cookie"].Value;

					// check to see if the server is running
					XmlNode statusNode = root.SelectSingleNode( "./GetSetRec[@id=\"2003\"]" );
					if ( statusNode != null && statusNode.FirstChild != null && 
						Int32.Parse( statusNode.FirstChild.Value ) == KEPAdminPage.SERVICE_RUNNING )
					{
						m_serverRunning = true;
					}
					else
					{
						m_serverCookie = "";
					}
				}
				else
				{
					throw new Exception( "Login failed " + root.FirstChild.Value );
				}
			}
			
			if ( m_controllerCookie.Length > 0 )
			{
				// if both up, send login to main server, too

				string xml = "<Login userId=\"" + userName + "\" password=\"" + password + "\"/>";

				string resultXML = "";
				try
				{
					resultXML = sendRequest( xml, true, m_serverPort );
				}
				catch ( SocketException )
				{
					return; // probably not started
				}

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml( resultXML );

				XmlNode root = xmlDoc.ChildNodes[0];
				int rc = Int32.Parse(root.Attributes["rc"].Value);
				if ( rc == 0 )
				{
					m_serverCookie = root.Attributes["cookie"].Value;
				}
				else
				{
					throw new Exception( "Login failed " + root.FirstChild.Value );
				}
				m_serverRunning = true;
			}

		}

		/// <summary>
		/// send an action to the server to execute
		/// </summary>
		/// <param name="actionId"></param>
		/// <param name="extraXml"></param>
		/// <param name="extraAttribs"></param>
		/// <returns></returns>
		public XmlDocument DoAction( int actionId, string objectId, string extraXml, string extraAttribs )
		{
			string mergeXML = "";
			string resultXML = "";

			// send to controller if for object 0 and one of these actions
			if ( ((actionId >= CONTROLLER_START_SERVICE && actionId <= CONTROLLER_GET_STATUS) 
						|| actionId == KEPAdminPage.GET_DATA || actionId == KEPAdminPage.REFRESH_STATUS	) && 
					objectId == "0" && m_controllerCookie.Length > 0 ) 
			{
				string xml = "<Action id=\"" + actionId + "\" objectId=\"" + objectId + "\" cookie=\"" + m_controllerCookie + "\" " + extraAttribs + ">";
				if ( extraXml != null && extraXml.Length > 0 )
					xml += extraXml;
				xml += "</Action>";

				mergeXML = sendRequest( xml, actionId != KEPAdminPage.SHUTDOWN, m_controllerPort );

				if ( actionId == KEPAdminPage.GET_DATA )
				{
					// m_serverRunning = false;
					XmlDocument doc = new XmlDocument();
					doc.LoadXml( mergeXML );

					XmlNode ctrlRoot = doc.ChildNodes[0];
					if ( ctrlRoot != null )
					{
						// check to see if the server is running
						XmlNode statusNode = ctrlRoot.SelectSingleNode( "./GetSetRec[@id=\"2003\"]" );
						if ( statusNode != null && statusNode.FirstChild != null && 
							Int32.Parse( statusNode.FirstChild.Value ) == KEPAdminPage.SERVICE_RUNNING )
							m_serverRunning = true;
					}
				}
				else
				{
					// just us
					resultXML = mergeXML;
					mergeXML = "";
				}
			}

			if ( resultXML.Length == 0 && (m_serverRunning && 
				 (actionId < CONTROLLER_START_SERVICE || actionId > CONTROLLER_GET_STATUS)) ) // not for controller only?
			{
				try
				{
					if ( m_serverCookie.Length == 0 ) // need to login? this would be case where we started server from controller
						Login( m_userName, m_password, m_server );

					string xml = "<Action id=\"" + actionId + "\" objectId=\"" + objectId + "\" cookie=\"" + m_serverCookie + "\" " + extraAttribs + ">";
					if ( extraXml != null && extraXml.Length > 0 )
						xml += extraXml;
					xml += "</Action>";

					resultXML = sendRequest( xml, actionId != KEPAdminPage.SHUTDOWN, m_serverPort );
				}
				catch ( Exception e )
				{
					resultXML = "<ActionResult rc=\"0\"><GetSetRec name=\""+ e.Message +"\" type=\"400\" objectId=\"0\"/></ActionResult>";
				}
			}

			// create result if stopping or starting since none from controller
			if ( actionId == KEPAdminPage.SHUTDOWN || actionId == CONTROLLER_STOP_SERVICE )
			{
				m_serverRunning = false;
				m_serverCookie = "";
				resultXML = "<ActionResult rc=\"0\">The server shutdown message was sent.</ActionResult>";
			}
			else if ( actionId == CONTROLLER_START_SERVICE )
			{
				resultXML = "<ActionResult rc=\"0\">The server start message was sent.</ActionResult>";
			}

			// for debugging we dump out the encoded XML, just for fun
#if DEBUG
			try
			{	
				System.IO.StreamWriter file = System.IO.File.CreateText( @"C:\temp\server.xml" );
				file.Write( resultXML );
				file.Close();

				System.IO.StreamWriter file2 = System.IO.File.CreateText( @"C:\temp\merge.xml" );
				file2.Write( mergeXML );
				file2.Close();
			}
			catch ( Exception )
			{
			}
#endif

			XmlDocument xmlDoc = new XmlDocument();
			if ( resultXML.Length == 0 )
			{
				// huh? System.Diagnostics.Debug.Assert( resultXML.Length > 0 );
				// fake out XML for merging
				resultXML = "<ActionResult rc=\"0\"><GetSetRec name=\"KGP Server\" type=\"400\" objectId=\"0\"/></ActionResult>";
			}
			xmlDoc.LoadXml( resultXML );
			XmlNode root = xmlDoc.ChildNodes[0];

			if ( root != null && root.Attributes["rc"] != null && 
				 root.Attributes["rc"].Value == "0" && mergeXML.Length > 0 ) // need to merge the two
			{
				if ( root != null )
				{
					XmlNode server = root.ChildNodes[0];
					if ( server != null )
					{
						// add in the nodes from the controller
						XmlDocument mergeDoc = new XmlDocument();
						mergeDoc.LoadXml( mergeXML );
						XmlNode mergeRoot = mergeDoc.ChildNodes[0];
						if ( mergeRoot != null )
						{
							for ( int i = mergeRoot.ChildNodes.Count-1; i >= 0; i-- )
							{
								XmlNode imported = xmlDoc.ImportNode( mergeRoot.ChildNodes[i], true ); // true means deep copy (needed for text values)
								// if type 99, put at bottom since they are "hidden"
								if( imported.Attributes["type"] == null || imported.Attributes["type"].Value != "99" )
									server.PrependChild( imported );
								else
									server.AppendChild( imported );
							}
						}
					}
				}
			}

			// for debugging we dump out the encoded XML, just for fun
#if DEBUG
			try
			{	
				System.IO.StreamWriter file = System.IO.File.CreateText( @"C:\temp\merged.xml" );
				XmlTextWriter writer = new XmlTextWriter(file);
				writer.Formatting = Formatting.Indented;
				xmlDoc.WriteTo( writer );
				writer.Flush();
				file.Close();
			}
			catch ( Exception )
			{
			}
#endif
			return xmlDoc;
		}

//		private string sendRequestA( string xml )
//		{
//			// call a DLL since using the .NET calls in IIS were very slow on first time connect 
//			// for some reason.
//			KEPAdminCallerLib.IKEPAdminCaller caller = new KEPAdminCallerLib.KEPAdminCaller();
//			return caller.Call( m_server, m_port, xml );
//		}

		private string sendRequest( string xml, bool getReply, int port )
		{
//			IPAddress addr = null;
//			IPHostEntry hostInfo = Dns.GetHostByName( m_server );
//			for(int index=0; index < hostInfo.AddressList.Length; index++)
//			{
//				addr = hostInfo.AddressList[index];
//				break;
//			}
//
//			
//			IPEndPoint endPt = new IPEndPoint( addr, 8887 );
////			TcpClient conn2 = new TcpClient( m_server, m_port ); //
//			DateTime dt = DateTime.Now;
//			Socket sock = new Socket( endPt.AddressFamily, SocketType.Stream, ProtocolType.Tcp ); 
//			sock.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 2000 );
//			sock.Connect( endPt );
//			TcpClient conn = new TcpClient(); // m_server, m_port ); //
//			conn.Connect( endPt );
//			TimeSpan ds = DateTime.Now - dt;


			TcpClient conn = null;
			NetworkStream stream = null;
			string ret = "";		

			try
			{
				conn = new TcpClient( m_server, port );

				stream = conn.GetStream();

				System.Text.Encoding e = System.Text.Encoding.GetEncoding("utf-8");

				Int32 len = IPAddress.HostToNetworkOrder( xml.Length ); 

				byte [] buffer = BitConverter.GetBytes( len );
				stream.Write( buffer, 0, buffer.Length );

				buffer = e.GetBytes( xml );			
				stream.Write( buffer, 0, xml.Length );

				if ( getReply )
				{
					buffer = new byte[BUFF_SIZE];

					conn.ReceiveTimeout = RECV_TIMEOUT;
					len = stream.Read( buffer, 0, 4);
					if ( len != 4 )
						throw new Exception( "Bad length from KEPServer" );

					int expectedLen = BitConverter.ToInt32( buffer, 0 );
					expectedLen = IPAddress.NetworkToHostOrder( expectedLen );

					int total = 0;
					while ( total < expectedLen && (len = stream.Read( buffer, 0, BUFF_SIZE)) > 0 )
					{
						ret += e.GetString( buffer, 0, len);
						total += len;
					}
				}
			}
			catch ( Exception e )
			{
				throw e;
			}
			finally 
			{
				try 
				{
					if ( stream != null )
						stream.Close();
					if ( conn != null )
						conn.Close();
				}
				catch ( Exception )
				{
				}
			}
			return ret;
		}


		private		int		m_serverPort;
		private		int		m_controllerPort;
		private		string	m_server = "localhost";
		private		string  m_serverCookie = "";
		private		string  m_controllerCookie = "";
		private		string	m_userName = "";
		private		string	m_password = ""; // hash, not clear
		private		bool	m_serverRunning = false;
	}
}
