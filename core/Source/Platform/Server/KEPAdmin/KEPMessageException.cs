///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KEPAdmin
{
	/// <summary>
	/// Summary description for KEPMessageException.
	/// </summary>
	public class KEPMessageException : Exception
	{
		public KEPMessageException( string msg, Int32 errCode ) : base( msg )
		{
			m_errCode = errCode;
		}

		public Int32 ErrCode
		{
			get 
			{
				return m_errCode;
			}
		}
		private Int32  m_errCode;
		
	}
}
