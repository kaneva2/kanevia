///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Xml;

namespace KEPAdmin
{
	/// <summary>
	/// Constants used by KEPAdmin web app.  Mostly from C++ code
	/// </summary>
	public class KEPAdminPage : System.Web.UI.Page
	{
		public KEPAdminPage()
		{}

		protected const string ErrHeader = "<h2>Error!</h2><br/>";
		protected const string MsgHeader = "<h2>Action completed.</h2><br/>";
		// from C++ code

		// array bit for simple-type arrays of types below
		protected const int gsSimpleArray       = 0x10000000;

		// simple types
		protected const int gsBool 			= 1;				
		protected const int gsBOOL 			= 2;
		protected const int gsChar 			= 3;
		protected const int gsByte 			= 4;
		protected const int gsShort 		= 5;
		protected const int gsUshort		= 6;
		protected const int gsWord 			= 7;				
		protected const int gsInt 			= 8;
		protected const int gsUint 			= 9;
		protected const int gsLong 			= 10;
		protected const int gsUlong 		= 11;
		protected const int gsDword			= 12;
		protected const int gsFloat 		= 13;
		protected const int gsDouble	 	= 14;
		protected const int gsString		= 15;	// STL string
		protected const int gsCString		= 16;	// MFC CString
		protected const int gsCharArray		= gsChar | gsSimpleArray;	// fixed size array of char

		// graphic
		protected const int gsPoint 		= 115;
		protected const int gsRect 			= 116;
		protected const int gsRgbaColor		= 117;	

		// special items
		protected const int gsDate 			= 200;	// "YYYYMMDD"
		protected const int gsDatetime 		= 201;	// "YYYYMMDD HH:MM:SS.xxx"
		protected const int gsIpAddress		= 202;	// "a.b.c.d"
		protected const int gsListOfValues	= 203; // for combo boxes or list boxes
		protected const int gsCustom 		= 204;  // derived class should do something for these
		protected const int gsAction		= 205;  // for admin

		// nested GetSet object w/i this one
		protected const int gsObjectPtr 	= 300;	// 22 pointer to GetSet from CObject
		protected const int gsObListPtr 	= 301;	// CObList that has GetSet-derived classes in it

		// get sets
		protected const int gsGetSetPtr 	= 400;	// pointer to a GetSet
		protected const int gsGetSetVector	= 401;	// vector of GetSet pointers


		// from KEPServer's resource.h
		protected const int GET_STATUS		= 17346;	// TODO: these should be "wellknown"
		public    const int GET_DATA		= 17348;
		public	  const int GET_ACTIONS		= 17350;
		public	  const int REFRESH_STATUS	= 17332;
		public	  const int ADD_OBJECT		= 17334;
		public	  const int DELETE_OBJECT	= 17336;
		public	  const int SAVE_OBJECT		= 17338;
		public	  const int GET_TEMPLATE	= 17340;
		public	  const int GET_LIST		= 17342;
		public    const int SHUTDOWN		= 164;

		protected const Int32 GS_FLAG_READ_WRITE = 0x00010000;
		protected const Int32 GS_FLAG_EXPOSE     = 0x00020000;
		protected const Int32 GS_CAN_ADD         = 0x00040000;

		public    const Int32 SERVICE_RUNNING	 = 3;	// matched IDaemonManager.h

		/// <summary>
		/// send an action to the server, and get the reply
		/// </summary>
		/// <param name="server"></param>
		/// <param name="objectAndActionId"></param>
		/// <returns></returns>
		protected string doAction( ref KEPServer server, string objectAndActionId )
		{
			string redirect = Request.Url.PathAndQuery;

			// create the XML to send to the server

			// first item is action id, then object id for action, then other options depending on action
			string [] items = objectAndActionId.Split( "|".ToCharArray(), 4 );
			System.Diagnostics.Debug.Assert( items.Length >= 2 && items.Length <= 4, "Bad data", "Wrong size returned from split of action" );
		
			Int32 actionId = Int32.Parse(items[0]);

			string extraXML = "";
			string extraAttribs = "";
			string error = "";

			switch ( actionId ) 
			{
				case REFRESH_STATUS:
					return redirect;
					// break; // just refresh

				case GET_TEMPLATE:
					if ( items.Length == 3 )
					{
						return Request.Path + "?oid=" + items[1] + "&aid=" + items[2];
					}
					else
					{
						error = "Wrong number of parameters for add";
					}
					break;

				case DELETE_OBJECT:
					if ( items.Length == 4 )
					{
						extraAttribs = "arrayId=\"" + items[2] + "\" childObjectId=\"" + items[3] + "\""; 
					}
					else
					{
						error = "Wrong number of parameters for delete";
					}
					break;

				case GET_LIST:
				case ADD_OBJECT:
					if ( items.Length == 3 )
					{
						extraAttribs = "arrayId=\"" + items[2] + "\"";
					}
					else
					{
						error = "Wrong number of parameters for save new or get list";
					}
					extraXML = buildGetSetXml( items[1] );
					break;

				case SAVE_OBJECT:
					extraXML = buildGetSetXml( items[1] );
					break;

				default:
					// assume it's a custom action we added, we send in all the object's current settings
					extraXML = buildGetSetXml( items[1] );
					if ( items.Length > 2 )
						extraAttribs = "childObjectId=\"" + items[2] + "\"";
					break;
			}

			if ( error.Length > 0 )
			{
				throw new Exception( error );
			}
			else
			{
				XmlDocument xmlDoc = server.DoAction( actionId, items[1], extraXML, extraAttribs );
				XmlNode node = xmlDoc.SelectSingleNode( "/ActionResult" );
				if ( node != null )
				{
					if ( node.Attributes["rc"].Value == "0" )
					{
						// ok, but have text for non-get request
						 if ( actionId != GET_DATA && 
							  actionId != GET_TEMPLATE && 
							  actionId != GET_ACTIONS && 
							  actionId != GET_STATUS && 
							  actionId != GET_LIST && 
							  node.FirstChild != null && node.FirstChild.Value != null && node.FirstChild.Value.Length > 0 )
						{
							throw new KEPMessageException( node.FirstChild.Value, Int32.Parse(node.Attributes["rc"].Value ));
						}
					}
					else // must be an error
					{
						// error, let it go to default redirect which handles error
						redirect = Request.Path;
						if ( node.FirstChild != null )
						{
#if DEBUG
							throw new Exception( node.FirstChild.Value + " RC = " + Int32.Parse(node.Attributes["rc"].Value).ToString( "x" ) );
#else
							throw new Exception( node.FirstChild.Value );
#endif
						}
						else
						{
							throw new Exception( "RC = " + Int32.Parse(node.Attributes["rc"].Value).ToString( "x" ) );
						}
					}
				}
			}

			return redirect;
		}

		/// <summary>
		/// do an action on the server that returns a set of xml
		/// </summary>
		/// <param name="server"></param>
		/// <param name="actionId"></param>
		/// <param name="objectId"></param>
		/// <param name="arrayId"></param>
		/// <param name="xmlDoc"></param>
		/// <returns></returns>
		protected bool doActionGetXml( ref KEPServer server, int actionId, string objectId, string arrayId, ref XmlDocument xmlDoc )
		{
			xmlDoc = null;

			// create the XML to send to the server

			string extraXML = "";
			string extraAttribs = "";
			string error = "";

			switch ( actionId ) 
			{
				case GET_LIST:
					extraAttribs = "arrayId=\"" + arrayId + "\"";
					break;

				default:
					// assume it's a custom action we added, we send in all the objects current settings
					return false;
			}

			if ( error.Length > 0 )
			{
				// TODO redirect = "KEPError.aspx?err=" + HttpUtility.UrlEncode(error);
				return false;
			}
			else
			{
				xmlDoc = server.DoAction( actionId, objectId, extraXML, extraAttribs );
				// TODO: check results
			}

			return true;;
		}


		// build the GetSet XML from the page
		private string buildGetSetXml( string objectId )
		{
			string gsXml = "<GetSetRec objectId='" + objectId + "'>";

			string d3x = "";
			string d3y = "";

			// iterate over all the controls
			String[] arr1 = Request.Form.AllKeys;
			for (int i  = 0; i < arr1.Length; i++) 
			{
				if ( arr1[i].Substring( 0, 2 ) == "id" ) // one of our ids
				{
					string [] items = arr1[i].Split( "_".ToCharArray(), 3 );
					gsXml += "<GetSetRec id='" + items[2] + "'>" + Request.Form[i] + "</GetSetRec>";
				}
				else if ( arr1[i].Substring( 0, 2 ) == "lb" ) // listbox
				{
					string [] items = arr1[i].Split( "x".ToCharArray(), 3 );
					gsXml += "<GetSetRec id='" + items[2] + "'>" + Request.Form[i] + "</GetSetRec>";
				}
				else if ( arr1[i].Substring( 0, 2 ) == "d3" ) // 3d vector
				{
					// these will always be in order x,y,x
					string [] items = arr1[i].Split( "_".ToCharArray(), 4 );
					if ( items.Length == 4 )
					{
						if ( items[3] == "x" )
							d3x = Request.Form[i];
						else if ( items[3] == "y" )
							d3y = Request.Form[i];
						else if ( items[3] == "z" )
						{
							// server wants string x,y,z
							gsXml += "<GetSetRec id='" + items[2] + "'>" + d3x + "," + d3y + "," + Request.Form[i] + "</GetSetRec>";
							d3x = "";
							d3y = "";
						}
					}
				}
			}

			gsXml += "</GetSetRec>";

			return gsXml;
			
		}
	}
}
