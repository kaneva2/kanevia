///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Xml;

namespace KEPAdmin
{

	/// <summary>
	/// Summary description for KEPList.
	/// </summary>
	public partial class KEPList : KEPAdminPage // System.Web.UI.Page
	{
	
		public class ListComparer : IComparer  
		{

			// Calls CaseInsensitiveComparer.Compare with the parameters reversed.
			int IComparer.Compare( Object x, Object y )  
			{
				return( (new CaseInsensitiveComparer()).Compare( ((ListItem)x).Text, ((ListItem)y).Text ) );
			}

		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if ( IsPostBack )
			{

				//this.btnOpen_Click( sender, e );

				/*
				Debug.Assert( ((string)Session["oid"]).Length > 0 && ((string)Session["aid"]).Length > 0, "Must have no-zero length params" );
				if ( ((string)Session["oid"]).Length > 0 && ((string)Session["aid"]).Length > 0 )
					Response.Redirect( "KEPAdminItem.aspx?oid=" + (string)Session["oid"] + "&aid=" + (string)Session["aid"] );
				else
					Response.Write( "<script>alert( 'Missing parameters to web page' );</script>" );
					*/
			}
			else
			{
				
				// see if they passed in a specific object id
				if ( Request.QueryString["oid"] != null ) 
				{
					Session["oid"] = Request.QueryString["oid"];
				}
				else
				{
					Session["oid"] = "0";
				}

				if ( Request.QueryString["aid"] != null )
				{
					Session["aid"] = Request.QueryString["aid"];
				}

				if ( Request.QueryString["aid"].Length > 0 )
				{
					btnAdd.Text = "Add to " + Request.QueryString["n"];
					btnAdd.ToolTip = "Add a new object to the list";
					btnAdd.Visible = true;
					ListName.Visible = false;
					if ( Int32.Parse( Request.QueryString["aid"] ) == 15654 ) // account database
					{
						XmlDocument xmlDoc = null;

						KEPServer server = (KEPServer)Session["KEPServer"];
						if ( server != null )
						{

							if ( doActionGetXml( ref server, GET_LIST, Request.QueryString["oid"], Request.QueryString["aid"], ref xmlDoc) )
							{
								ArrayList values = new ArrayList(); 

								XmlNode node = xmlDoc.SelectSingleNode( "//ItemList" );
								if ( node != null && node.Attributes["size"] != null )
									values.Capacity = Int32.Parse(node.Attributes["size"].Value);

								// now insert all the items into the listbox
								XmlNodeList nodes = xmlDoc.SelectNodes( "//Item" );
								for ( int j = 0; j < nodes.Count; j++ )
								{
									values.Add( new ListItem(nodes[j].InnerText,nodes[j].Attributes["value"].Value) );
								}

								values.Sort( new ListComparer() );
								lbItems.DataSource = values;
								lbItems.DataValueField = "Value";
								lbItems.DataTextField = "Text";
								lbItems.DataBind();
							}
							else
							{
								throw new Exception( "Wrong number of parameters for getting listbox data" );
							}
							System.Diagnostics.Debug.WriteLine( xmlDoc.InnerXml );
						}
					}
					else // not account list
					{
						lbItems.Visible = false;
						btnOpen.Visible = false;
					}
				}
				else
				{
					btnAdd.Visible = false;
					lbItems.Visible = false;
					btnOpen.Visible = false;
				
					ListName.Visible = true;
					ListName.Text = "List of " + Request.QueryString["n"];
				}
			}
		}
		protected System.Web.UI.HtmlControls.HtmlInputButton AddButton;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnOpen_Click(object sender, System.EventArgs e)
		{
			int i = lbItems.SelectedIndex;

			string oid = Session["oid"] + "_" + lbItems.SelectedValue;

			Response.Redirect( "listDetails.aspx?oid=" + oid );
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			Debug.Assert( ((string)Session["oid"]).Length > 0 && ((string)Session["aid"]).Length > 0, "Must have no-zero length params" );
			if ( ((string)Session["oid"]).Length > 0 && ((string)Session["aid"]).Length > 0 )
				Response.Redirect( "KEPAdminItem.aspx?oid=" + (string)Session["oid"] + "&aid=" + (string)Session["aid"] );
			else
				Response.Write( "<script>alert( 'Missing parameters to web page' );</script>" );
		
		}

	}
}
