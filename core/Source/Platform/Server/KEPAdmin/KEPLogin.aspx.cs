///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KEPAdmin
{
	/// <summary>
	/// Summary description for KEPLogin.
	/// </summary>
	public partial class KEPLogin : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if ( !IsPostBack )
			{
				Session["KEPServer"] = null;
                serverName.Text = System.Configuration.ConfigurationManager.AppSettings["KEPServer.hostname"];

				string errMsg = (string)Session["ErrMsg"];
				
				if ( errMsg != null )
				{
					ResultsTextBox.Text = errMsg;
					Session["ErrMsg"] = "";
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void LoginButton_Click(object sender, System.EventArgs e)
		{
			try
			{
				KEPServer server = new KEPServer();
				server.Login( username.Text, password.Text, serverName.Text );
				Session["KEPServer"] = server;

//				Response.Redirect( "main.htm" ); Write("Loading...<script>window.open(\"tree-menu/code.aspx\",\"code\");</script><script>window.open(\"NoItem.htm\",\"main\");</script>");
				Response.Write("Loading...<script>window.open(\"main.htm\",\"_top\");</script>");

				//Response.Redirect( "NoItem.htm" );
			}
			catch ( Exception ex )
			{
				ResultsTextBox.Text = ex.Message;
			}
			
		}
	}
}
