///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Xsl;

namespace KEPAdmin
{

	using System;
	using System.IO;
	using System.Xml;
	using System.Xml.XPath;
	using System.Xml.Xsl;

	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class KEPTreeCode : KEPAdminPage
	{
		protected System.Web.UI.WebControls.Literal generatedMenu;

		protected void Page_Load(object sender, System.EventArgs e)
		{

			// Put user code to initialize the page here
			// first getall the data

			try
			{
				generatedMenu.Text = "// Main menu.\n" +
									"MTMEmulateWE=true;\n" +
									"var menu = null;\n" +
									"menu = new MTMenu();\n"; 

				KEPServer server = (KEPServer)Session["KEPServer"];
				if ( server == null )
				{
					return;
				}

				if ( !IsPostBack )
				{
					// see if they passed in a specific object id
					string oid = "0"; // default to getting the root object
					string target = "main";

					XsltArgumentList xslArg = new XsltArgumentList();
					if ( Request.QueryString["oid"] != null ) 
					{
						// right now, only accounts passed in
						oid = Request.QueryString["oid"];
						target = "accountDetails";
						generatedMenu.Text += "MTMenuText=\"User\";";
						xslArg.AddParam("expandedLevels", "", 2.ToString());
					}
					else
					{
						generatedMenu.Text += "MTMenuText=\"KGP Administration\";";
						xslArg.AddParam("expandedLevels", "", 7.ToString());
					}

					generatedMenu.Text += "MTMDefaultTarget=\"" + target + "\"\n";

					XmlDocument xmlDoc = null;

					xmlDoc = server.DoAction( GET_DATA, oid, "<Depth>8</Depth>", "filter=\"objects\"" );



					// 
					System.Xml.Xsl.XslTransform xform = new System.Xml.Xsl.XslTransform();

					xform.Load( Request.MapPath( "../KEPAdminTree.xsl" ) );

					System.IO.StringWriter writer1 = new System.IO.StringWriter();
					xform.Transform( xmlDoc, xslArg, writer1, null );
			
					string s = writer1.ToString();
					int xmlTagEnd = s.IndexOf( "?>" );
					if ( xmlTagEnd > 0 )
						generatedMenu.Text += s.Substring( xmlTagEnd+2 );
					else 
						generatedMenu.Text += s;


				}
			}
			catch ( Exception ex )
			{
				generatedMenu.Text += "alert( \"Error:" + ex.ToString().Replace("\\","\\\\").Replace("\"", "\\\"").Replace("\r\n","\\r\\n\"+\n\"") + "\");";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
