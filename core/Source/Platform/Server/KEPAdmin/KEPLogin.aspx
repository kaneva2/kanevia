<%@ Page language="c#" Codebehind="KEPLogin.aspx.cs" AutoEventWireup="True" Inherits="KEPAdmin.KEPLogin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>KEPLogin</title>
		<meta content="False" name="vs_snapToGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK title="KEI" href="KEIstylesheet.css" type="text/css" rel="stylesheet">
		<script src="mtmtrack.js" type="text/javascript">
		</script>
</HEAD>
	<BODY bgColor="#ffffff" leftMargin="0" topMargin="0" marginheight="0" marginwidth="0">
		<TABLE cellSpacing="0" cellPadding="0" align="left" border="0">
			<form id="Form1" method="post" runat="server">
				<TBODY>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="10"></td>
						<td colSpan="3"><asp:label id="Label1" runat="server" Font-Size="Medium" Font-Bold="True">Kaneva Administrative Login</asp:label></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="10"></td>
						<td align="right"><asp:label id="Label2" runat="server">Username:&nbsp;&nbsp;</asp:label></td>
						<td><asp:textbox id="username" runat="server" Width="200px"></asp:textbox></td>
					</tr>
					<tr>
						<td width="10"></td>
						<td align="right"><asp:label id="Label3" runat="server">Password:&nbsp;&nbsp;</asp:label></td>
						<td><asp:textbox id="password" runat="server" Width="200px" TextMode="Password"></asp:textbox></td>
					</tr>
					<tr>
						<td width="10"></td>
						<td></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="10"></td>
						<td>
							<P align="right"><asp:label id="Label4" runat="server">Server:&nbsp;&nbsp;</asp:label></P>
						</td>
						<td><asp:label id="serverName" runat="server"/></td>
					</tr>
					<tr>
						<td width="10"></td>
						<td></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="10"></td>
						<td></td>
						<td><asp:button id="LoginButton" runat="server" Width="125px" Text="Login" onclick="LoginButton_Click"></asp:button></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td style="HEIGHT: 24px" colSpan="2"><asp:label id="ResultsTextBox" runat="server" Font-Size="Small" Font-Bold="True" Width="399px"></asp:label></td>
					</tr>
			</form></TBODY></TABLE>
		<script>document.forms[0].username.focus();</script>
	</BODY>
</HTML>
