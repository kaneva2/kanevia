///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using System.Xml;

namespace KEPAdmin
{
	/// <summary>
	/// Summary description for KGPServer.
	/// </summary>
	public partial class KGPServer : KEPAdminPage
	{

		protected System.Web.UI.WebControls.Label Label1;

		string m_loadedXml = "";
		private static string DISABLED_ENGINES = "14895";
		
		// refresh the tree.  Now that accounts are in a tree, this will only refresh the tree
		// if not in the account tree
		private static string TREE_REFRESH_SCRIPT = "<script lang=\"javascript\">if ( parent.frames[\"accountDetails\"] == null ) parent.code.location.replace('tree-menu/code.aspx');</script>";

		private HtmlInputButton fixupDeleteButton( Control c, string did )
		{
			// see if there are any lists that need to be initialized
			for ( int i = 0; i < c.Controls.Count; i++ )
			{
				HtmlInputButton b = null;
				if ( c.Controls[i].GetType() == typeof( System.Web.UI.HtmlControls.HtmlInputButton ) )
				{
					b = (HtmlInputButton)c.Controls[i];
					if ( b.Attributes["onclick"] != null && 
						 b.Attributes["onclick"].IndexOf( DELETE_OBJECT.ToString() ) > 0 )
					{
						// if a delete request, we do special handling
						string[] items = b.Attributes["onclick"].Split( "=|'".ToCharArray() );
						if ( items.Length >= 4 ) // the =' makes items[1] empty
						{
							Int32 id = Int32.Parse(items[2]);
							if ( id == DELETE_OBJECT )
							{
								Debug.Assert( did.Length > 0 );

								// take the oid, and rip off last chunk
								int lastSplit = items[3].LastIndexOf( "_" );
								string lastOid = "", parentOid = "";
								if ( lastSplit > 0 )
								{
									lastOid = items[3].Substring( lastSplit+1 );
									parentOid = items[3].Substring(0, lastSplit);
									if ( did == DISABLED_ENGINES )
									{
										string [] x = parentOid.Split( "_".ToCharArray() );
										lastOid = x[x.Length-1];
										parentOid = "0"; // hack for this case when hiding intermediate level
									}
								}
								b.Attributes["onclick"] = items[0] + "='" + id + "|" + parentOid + "|" + did + "|" + lastOid + "'";
								if ( did == "" )
									b.Visible = false; // hide it (better solution?)
								return b;
							}
						}
					}
				}
				b = fixupDeleteButton( c.Controls[i], did );
				if ( b != null )
					return b;
			}
			return null;
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
				// Put user code to initialize the page here
				// first getall the data

			try
			{
				KEPServer server = (KEPServer)Session["KEPServer"];
				if ( server == null )
				{
					Session["ErrMsg"] = "Session Timeout";
					Response.Write( "<script>window.open(\"default.htm\", \"_top\" );</script>" );
					return;
				}

				if ( !IsPostBack )
				{
					// see if they passed in a specific object id
					string oid = "0"; // default to getting the root object
					string did = "";  // delete array id


					if ( Request.QueryString["oid"] != null ) 
					{
						// TODO: validation?  or let server do it?
						oid = Request.QueryString["oid"];
					}

					if ( Request.QueryString["did"] != null ) 
					{
						// TODO: validation?  or let server do it?
						did = Request.QueryString["did"];
					}

					XmlDocument xmlDoc = null;
					if ( Request.QueryString["aid"] != null )
					{
						xmlDoc = server.DoAction( GET_TEMPLATE, oid, "", "arrayId=\"" + Request.QueryString["aid"] + "\"" );
					}
					else
					{
						xmlDoc = server.DoAction( GET_DATA, oid, "<Depth>1</Depth>", "filter=\"long\"" );
					}


					// 
					System.Xml.Xsl.XslTransform xform = new System.Xml.Xsl.XslTransform();
					xform.Load( Request.MapPath( "KEPAdmin.xsl" ) );
					System.IO.StringWriter writer1 = new System.IO.StringWriter();
					xform.Transform( xmlDoc, null, writer1, null );
		
					m_loadedXml = writer1.ToString();

					Control c = ParseControl( m_loadedXml );
					MainTextBox.Controls.Add( c );

					ArrayList lbs = new ArrayList();
					findListBoxes( MainTextBox, ref lbs );

					// for each list box, populate it
					for ( int i = 0; i < lbs.Count; i++ )
					{
						HtmlSelect s = (HtmlSelect)lbs[i];

						string [] items = s.ID.Split( "x".ToCharArray(), 3 );
						if ( items.Length == 3 && items[0] == "lb" )
						{
							XmlDocument doc = null;

							// call the server to get the list
							if ( doActionGetXml( ref server, GET_LIST, items[1], items[2], ref doc) )
							{
								XmlNode node = doc.SelectSingleNode( "//ItemList" );
								if ( node != null && node.Attributes["size"] != null )
									s.Size = Int32.Parse(node.Attributes["size"].Value);

								// now insert all the items into the listbox
								XmlNodeList nodes = doc.SelectNodes( "//Item" );
								for ( int j = 0; j < nodes.Count; j++ )
								{
									s.Items.Add( new ListItem(nodes[j].InnerText,nodes[j].Attributes["value"].Value ) );
								}
							}
							else
							{
								throw new Exception( "Wrong number of parameters for getting listbox data" );
							}
						}
				

						//TestBox.Text = writer1.ToString();

						// System.IO.StringWriter writer = new System.IO.StringWriter();
					}
					fixupDeleteButton( MainTextBox, did );
		

				}
				else if ( Request.HttpMethod == "POST" )
				{
					//Control c = ParseControl( m_loadedXml );
					//MainTextBox.Controls.Add( c );
					string v = Request["whoclicked"];

					if ( v.Length > 0 )
					{
						Response.Redirect( doAction( ref server, v ) );
					}
				}
			}
			catch ( KEPMessageException kme )
			{
				if ( kme.ErrCode == 0 )
				{
					ResultTextBox.Text = MsgHeader + HttpUtility.HtmlEncode(kme.Message).Replace( "\n", "<br>" );
					ResultTextBox.Text += TREE_REFRESH_SCRIPT;  // force refresh on all actions
				}
				else
				{
					ResultTextBox.Text = ErrHeader + HttpUtility.HtmlEncode(kme.Message).Replace( "\n", "<br>" );
				}
			}
			catch ( Exception ex )
			{
				Session["ErrMsg"] = HttpUtility.HtmlEncode(ex.Message).Replace( "\n", "<br>" );
				Response.Write( "<script>window.open(\"default.htm\", \"_top\" );</script>" );
			}
		}

		private void findListBoxes( Control c, ref ArrayList lists )
		{
			// see if there are any lists that need to be initialized
			for ( int i = 0; i < c.Controls.Count; i++ )
			{
				if ( c.Controls[i].GetType() == typeof( System.Web.UI.HtmlControls.HtmlSelect )  )
				{
					lists.Add( c.Controls[i] );
				}
				findListBoxes( c.Controls[i], ref lists );
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
