<%@ Page language="c#" Codebehind="KEPList.aspx.cs" AutoEventWireup="True" Inherits="KEPAdmin.KEPList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>KEPList</title>
		<meta name="vs_showGrid" content="True">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" href="KEIstylesheet.css" type="text/css">
		<script type="text/javascript" src="tree-menu/mtmtrack.js">
		</script>
		<script language="javascript">

		function incrSearch()
		{
			var str = document.forms["KEPList"]["incSearchStr"].value.replace(/^\s*/,'');
			if ( str != '' )
			{
				for ( i = 0; i < document.forms["KEPList"]["lbItems"].options.length; i++ )
				{
					s = document.forms["KEPList"]["lbItems"].options[i];
					if ( s.text.substr(0,str.length).toLowerCase() == str.toLowerCase() )
					{
						document.forms["KEPList"]["lbItems"].options[i].selected = true;
						document.forms["KEPList"]["lbItems"].options[i].scrollIntoView();
						break;
					}
				}
			}
		}
		function openItem()
		{
			document.forms["KEPList"]["btnOpen"].click();
		}
		function keypress()
		{
			if ( !(window.event && window.event.keyCode == 13) )
				return true;
			else
			{
				openItem();
				return false;
			}
		}

		</script>
	</HEAD>
	<body>
		<form id="KEPList" method="post" runat="server">
			<TABLE cellSpacing="0" cellPadding="0" align="left" border="0" width="100%">
				<TR bgcolor="#aaaaaa">
					<TD width="20">&nbsp;</TD>
					<TD class="title1" style="COLOR: #ffffff" colspan="2" align="left" width="100%">
						<img src="images/spacer.gif" width="1" height="30">ACTIONS: &nbsp;&nbsp;
						<asp:Label id="ListName" runat="server" Width="152px">Label</asp:Label>
						<asp:Button id="btnOpen" runat="server" Text="Open" onclick="btnOpen_Click"></asp:Button>&nbsp;
						<asp:Button id="btnAdd" runat="server" Text="Add" onclick="btnAdd_Click"></asp:Button>&nbsp;
						<asp:Button id="btnDelete" runat="server" Text="Delete"></asp:Button>
					</TD>
					<td>&nbsp;</td>
				</TR>
				<TR>
					<TD width="20"></TD>
					<TD class="title1" style="COLOR: #ffffff" align="left" width="100%" colSpan="2">&nbsp;</TD>
				</TR>
				<TR>
					<TD width="20"></TD>
					<td valign="top" style="WIDTH: 6px">
						<P>
							Find <input type="text" id="incSearchStr" size="29" class="inputField" onKeyUp="javascript:incrSearch();"
								style="WIDTH: 208px; HEIGHT: 19px" onkeypress="javascript:return keypress();">
							<asp:ListBox id="lbItems" runat="server" Width="208px" Height="376px" ondblclick="javascript:openItem();"></asp:ListBox></P>
						<P>
							<asp:Label id="lblMessage" runat="server" Width="520px"></asp:Label></P>
					</td>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
