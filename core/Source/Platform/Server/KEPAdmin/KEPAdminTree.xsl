<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;">]> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="expandedLevels"/>
<xsl:variable name="menuNameChar" >&nbsp;</xsl:variable>
<xsl:include href="str.find-last.xslt" />

<xsl:template match="/">
	<xsl:if test="ActionResult/@rc != '0'">
		alert( "Error from server.\r\n<xsl:value-of select="ActionResult" />Result code = <xsl:value-of select="ActionResult/@rc" ></xsl:value-of>" );
		window.open( "../default.htm", "_top" );
	</xsl:if>
	<xsl:if test="ActionResult/@rc = '0'">
		<xsl:apply-templates select="ActionResult/GetSetRec[@type='300' or @type='400']">
				<xsl:with-param name="menuName">menu</xsl:with-param>
				<xsl:with-param name="hideDelete" select="1"></xsl:with-param>
				<xsl:with-param name="indent"> </xsl:with-param>
		</xsl:apply-templates>
	</xsl:if>
</xsl:template>

<!-- default case if nothing below matches -->
<xsl:template match="//GetSetRec">
	<h4>TODO:  Unknown type of <xsl:value-of select="@type"/> with a name of <xsl:value-of select="@name" /><br/></h4>
</xsl:template>

<!-- array of children -->
<xsl:template match="GetSetRec[@type='401' or @type='301']">
	<xsl:param name="menuName" />
	<xsl:param name="hideDelete"/>
	<xsl:param name="indent"/>

	<xsl:choose>
	<!-- we don't show these for compactness 
	124 = IDS_ENGINETHREAD_ENGINES        
	-->
		<xsl:when test="@id and @id = '124'">
			<!-- now dump out each kid --> 
			<xsl:apply-templates select="./GetSetRec[@type='400' or @type='300' or @type='301' or @type='401']"> 
				<xsl:with-param name="menuName" select="$menuName"/>
				<xsl:with-param name="hideDelete">
					<xsl:choose>
						<xsl:when test="floor(@flags div 262144) mod 2 = 1">
							0
						</xsl:when>
						<xsl:otherwise>
							1
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="indent" select="concat($indent,' ')"></xsl:with-param>
			</xsl:apply-templates>
		</xsl:when>
		<xsl:otherwise>
			<xsl:if test="floor(@flags div 262144) mod 2 = 1">
				<xsl:value-of select="$menuName"/>.addItem("<xsl:value-of select="translate(@name,'&quot;','')" />", "../KEPList.aspx?n=<xsl:value-of select="@name" />&amp;oid=<xsl:value-of select="../@objectId" />&amp;aid=<xsl:value-of select="@id" />", null, "<xsl:value-of select='@help'/>" );
			</xsl:if>	
			<xsl:if test="floor(@flags div 262144) mod 2 = 0">
				<xsl:value-of select="$menuName"/>.addItem("<xsl:value-of select="translate(@name,'&quot;','')" />", "../NoItem.htm", null, "<xsl:value-of select='@help'/>");
			</xsl:if>
			
			<xsl:if test="@id != '15654'"> <!-- if not accounts, make look like a subfolder -->
				var <xsl:value-of select="concat('m',@objectId)"/> = null;
				<xsl:value-of select="concat('m',@objectId)"/> = new MTMenu();
				<xsl:value-of select="$menuName"/>.makeLastSubmenu( <xsl:value-of select="concat('m',@objectId)"/><xsl:if test="string-length($indent) &lt; $expandedLevels">,true</xsl:if>  );
			</xsl:if>			
			
			<!-- now dump out each kid --> 
			<xsl:apply-templates select="./GetSetRec[@type='400' or @type='300' or @type='301' or @type='401']"> 
				<xsl:with-param name="menuName" select="concat('m',@objectId)"/>
				<xsl:with-param name="hideDelete">
					<xsl:choose>
						<xsl:when test="floor(@flags div 262144) mod 2 = 1">
							0
						</xsl:when>
						<xsl:otherwise>
							1
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="indent" select="concat($indent,' ')"></xsl:with-param>
			</xsl:apply-templates>
		</xsl:otherwise>
	</xsl:choose>		
	
</xsl:template>		

<!-- used to creat an href for a 300 or 400 object-->
<xsl:template name="objectHref">
	<xsl:param name="menuName" />
	<xsl:param name="hideDelete"/>
	<xsl:param name="indent"/>
	
	<!-- if can delete, add the delete id to the link so item page can get it -->
	<!-- suppress the Dx skills with the 14817 -->
	<!-- if ../@id not defined, assume in grandparent, and going for root object -->	
	<xsl:if test="not(../@id) or ../@id != '14817' or string-length(@name) &gt; 2"> 
		<xsl:value-of select="$menuName"/>.addItem("<xsl:value-of select="translate(@name,'&quot;','')" />", "../KEPAdminItem.aspx?oid=<xsl:value-of select="@objectId" /><xsl:if test="not(../../@id) or ../../@id != '124'">
			<xsl:choose>
				<xsl:when test="../@id"> 
					<xsl:if test="$hideDelete != 1">&amp;did=<xsl:value-of select='../@id' /></xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$hideDelete != 1">&amp;did=<xsl:value-of select='../../@id' /></xsl:if>
				</xsl:otherwise>
			</xsl:choose></xsl:if>", null, "<xsl:value-of select='@help'/>");
	</xsl:if>
			
</xsl:template>		

<!-- object -->
<xsl:template match="//GetSetRec[@type='400' or @type='300']">
	<xsl:param name="menuName" />
	<xsl:param name="hideDelete"/>
	<xsl:param name="indent"/>

	<!-- we don't show these for compactness 
	127 = IDS_KEPSERVERIMPL_ENGINETHREADS
	124 = IDS_ENGINETHREAD_ENGINES        
	14895 = IDS_KEPSERVERIMPL_DISABLEDENGINES
	-->
	<xsl:choose>
		<xsl:when test="../@id and (../@id = '127' or ../@id = '124' or ../@id = '14895')">
			<xsl:apply-templates select="./GetSetRec[@type='400' or @type='300' or @type='301' or @type='401']"> 
				<xsl:with-param name="menuName" select="$menuName"/>
				<xsl:with-param name="hideDelete" select="$hideDelete"/>
				<xsl:with-param name="indent" select="concat($indent,' ')"></xsl:with-param>
			</xsl:apply-templates>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="objectHref">
				<xsl:with-param name="menuName" select="$menuName"/>
				<xsl:with-param name="hideDelete" select="$hideDelete"/>
				<xsl:with-param name="indent" select="$indent"></xsl:with-param>
			</xsl:call-template>
			
			<xsl:if test="count(./GetSetRec) &gt; 0">
				var <xsl:value-of select="concat('m',@objectId)"/> = null;
				<xsl:value-of select="concat('m',@objectId)"/> = new MTMenu();
				<xsl:value-of select="$menuName"/>.makeLastSubmenu( <xsl:value-of select="concat('m',@objectId)"/><xsl:if test="string-length($indent) &lt; $expandedLevels">,true</xsl:if>  );
				
				<xsl:apply-templates select="./GetSetRec[@type='400' or @type='300' or @type='301' or @type='401']"> 
					<xsl:with-param name="menuName" select="concat('m',@objectId)"/>
					<xsl:with-param name="hideDelete" select="$hideDelete"/>
					<xsl:with-param name="indent" select="concat($indent,' ')"></xsl:with-param>
				</xsl:apply-templates>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>		

</xsl:stylesheet>

  