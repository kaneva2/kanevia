<%@ Page language="c#" Codebehind="KEPAdminItem.aspx.cs" AutoEventWireup="True" Inherits="KEPAdmin.KGPServer" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>KGPServer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link rel="stylesheet" href="KEIstylesheet.css" type="text/css" />
		<script type="text/javascript" src="tree-menu/mtmtrack.js"></script>
	</HEAD>
	<BODY bgColor="#ffffff" leftMargin="0" topMargin="0">
		<BR>
		<TABLE cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
			<TBODY>
				<TR>
					<TD vAlign="top" align="left">
						<form id="ActionsForm" method="post" runat="server">
							<asp:Placeholder id="MainTextBox" runat="server"></asp:Placeholder>
							<input type="hidden" id="whoclicked" name="whoclicked" runat="server">
						</form>
					</TD>
				</TR>
				<tr>
				<td><asp:Label id="ResultTextBox" runat="server"></asp:Label></td>
				</tr>
			</TBODY>
		</TABLE>
	</BODY>
</HTML>
