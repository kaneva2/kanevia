///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MySqlEventProcessor.h"
#include "KEPCommon.h"
#include "Event/Events/WorldObjectEvents.h"
#include "Event/Events/RenderTextEvent.h"
#include "Event/Events/RenderLocaleTextEvent.h"
#include "Event/Events/DynamicObjectEvents.h"
#include "Event/Events/MySQLGetResultSetsEvent.h"
#include "Event/Events/ZonePermissionEvent.h"
#include "Event/Events/ParticleEvents.h"
#include "Event/Events/SoundEvents.h"
#include "Event/Events/SpaceImportEvent.h"
#include "Event/Events/ChannelImportEvent.h"
#include "Event/Events/GotoPlaceEvent.h"
#include "Event/Events/PlacementSource.h"
#include "Event/Events/UpdateScriptZoneSpinDownDelayEvent.h"
#include "Event/Events/ZoneImportedEvent.h"
#include "CPlayerClass.h"
#include "KGPBrowser.h"
#include "CAccountClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "Param_Ids.h"
#include "Audit.h"
#include "InventoryType.h"
#include "LogTimings.h"
#include "UrlParser.h"
#include "KEPConstants.h"
#include <PassList.h>
#include "server/serverengine/serverengine.h"
#include "Common/keputil/KGenericObjList.h"
#include "Common/KEPUtil/URL.h"
#include "..\..\Tools\HttpClient\GetBrowserPage.h"
#include "event\PlayListMgr\PLMMediaData.h"
#include "LogHelper.h"
#include "LocaleStrings.h"

using namespace mysqlpp;

namespace KEP {

static LogInstance("MySQLBackground");

// flags for getting inv via URL
#define GET_INV_ARMED "1"
#define GET_INV_ARMED_FURN "3"
#define GET_INV_ALL "255"

// XMLLoader.cpp
bool fillInPlayList(TiXmlElement* table, int& assetID, std::string& assetOffsiteID, eMediaType& assetTypeID, eMediaSubType& assetSubTypeID, TimeSec& runTimeSeconds, std::string& thumbNailMediumPath, std::string& mediaPath, std::string& assetName);
bool fillInDynamicObject(TiXmlElement* table, dynamicObject* pDynamicObject);
bool fillInDynamicObjectParameters(TiXmlElement* parameter, dynamicObject* pDynamicObject);
bool fillInWorldObject(TiXmlElement* table, worldObjectPlayerSetting* pWorldObjectPlayerSetting);

static Logger alert_logger(Logger::getInstance(L"Alert"));

// cols returns for can_spawn_to_* sps
// @rc, @ii, @sid, @ssid, @cc, @zowner, @url, @zi, @x, @y, @z, @r, @spi
// @rc, @hi, @sid, @ssid, @cc, @zowner, @url, @pid
//
#define GOTO_RC_COL 0
#define GOTO_INSTANCE_COL 1
#define GOTO_HOUSING_INDEX 1
#define GOTO_SERVER_ID_COL 2
#define GOTO_SCRIPT_SERVER_ID_COL 3
#define GOTO_COVER_COL 4
#define GOTO_OWNER_COL 5
#define GOTO_URL_COL 6
#define GOTO_ZONEINDEX_COL 7
#define GOTO_PLAYERID_COL 7
#define GOTO_X_COL 8
#define GOTO_Y_COL 9
#define GOTO_Z_COL 10
#define GOTO_ROTATE_COL 11
#define GOTO_SPAWNPTINDEX_COL 12
#define GOTO_DEST_KANEVA_ID_COL 13

/* added to check for our server_id from the pinger
	since it's a bad thing to hit db without our
	valid server_id
	this is only called on server change state and
	we a user logs in.  otherwise the code is unreachable
	since log ins are blocked.
*/

bool MySqlEventProcessor::checkServerId(const char* desc, bool logMsg) {
	m_serverId = 1;
	if (m_serverId == 0) {
		if (m_serverNetwork && m_serverNetwork->pinger().GetServerId() != 0) {
			m_serverId = m_serverNetwork->pinger().GetServerId();
			LogInfo("Database got pinger id of " << m_serverId);
		} else if (logMsg) {
			LogWarn("Server id is zero for database call.  Not executing: " << desc);
		}
	}
	return m_serverId != 0;
}

const char* MySqlEventProcessor::pingerGameName() {
	//Always refresh it from pinger
	m_serverNetwork->pinger().GetGameName(m_gameName, _countof(m_gameName));
	return m_gameName;
}

sql_create_3(bg_dynamic_get, 3, 0, int, global_id, int, qty, int, inventory_sub_type);

void MySqlEventProcessor::Initialize() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// first init mysql for multi-threading
	if (mysql_thread_init() != 0) {
		throw new KEPException("Error initializing MySQL threading", E_INVALIDARG);
		return;
	}

	LogInfo("MySqlEventProcessor Database initialized using the following values:");
	LogInfo("   Server:port:           " << m_conn->server() << ":" << m_conn->port());
	LogInfo("   User:                  " << m_conn->user());
	LogInfo("   Database:              " << m_conn->db());
	LogInfo("   NeverSwitchServers:    " << m_conn->neverSwitchServers());
	LogInfo("   BackgroundThreadCount: " << m_conn->backgroundThreadCount());
	LogInfo("   GetPlayerUrl:          " << m_conn->getPlayerUrl());
	LogInfo("   getInventoryUpdaterUrl:  " << m_conn->getInventoryUpdaterUrl());

	std::string errMsg;
	try {
		m_conn->connect();
	} catch (const mysqlpp::Exception& er) {
		errMsg = er.what();
	}

	if (!m_conn) {
		throw new KEPException(loadStrPrintf(IDS_DB_CONNECT_ERROR, errMsg.c_str()), E_INVALIDARG);
	} else {
		LogInfo(loadStr(IDS_DB_CONNECT));
	}
}

MySqlEventProcessor::~MySqlEventProcessor() {
	mysql_thread_end();
	delete m_conn;
}

/// move a dynamic object in db, create event for return
void MySqlEventProcessor::updateWorldObjectTexture(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	ZoneIndex zoneIndex = zce->zoneIndex;
	std::string objId = zce->worldObjSerialNum;
	std::string playerName = zce->charName;
	NETID netId = zce->From();

	BEGIN_TRY_SQL();

	// Perform DB Query
	// ret return values - 2 not found to be consistent with other return codes
	// 0 == default
	// 2 == not found
	// 3 == update happened
	// 4 == delete happened
	// 5 == insert happened
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << "CALL update_world_obj_texture( %0q, %1, %2, %3, %4q, @ret, @textureURL ); select @ret, @textureURL;";
	query.parse();
	StoreQueryResult res = query.store(
		objId,
		(int)zce->assetId,
		(int)zoneIndex.getClearedInstanceId(),
		(int)zoneIndex.GetInstanceId(),
		zce->textureUrl);
	res = query.store_next();
	trans.commit();

	if (res.num_rows() > 0) {
		long errorCode = res.at(0).at(0);
		zce->ret = NO_ERROR;

		switch (errorCode) {
			case 2:
				LogError("FAILED to obtain world object - " << zce->ToStr());
				zce->ret = errorCode;
				break;

			case 3:
				LogError("FAILED UPDATE texture of world object - " << zce->ToStr());
				break;

			case 4:
				LogError("FAILED DELETE texture of world object - " << zce->ToStr());
				break;

			case 5:
				LogError("FAILED INSERT texture of world object - " << zce->ToStr());
				break;

			default:
				zce->ret = errorCode;
				LogError("FAILED to set world object - " << zce->ToStr());
				break;
		}

		// broadcast out the event, need to copy the event to avoid double delete
		std::string textureUrl = res.at(0).at(1);

		zce->broadcastEvent = new UpdateWorldObjectTextureEvent(
			objId,
			zce->assetId,
			textureUrl.c_str());
	} else {
		// error, don't have access
		zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
		zce->replyEvent->AddTo(netId);
	}

	END_TRY_SQL();
}

// indicator from SP not to re-add to player's inv for whatever reason
// for now the only case is in GStar
#define DONT_ADD -1

/// move a dynamic object in db, create event for return
void MySqlEventProcessor::removeDynamicObject(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	ZoneIndex zoneIndex = zce->zoneIndex;
	int objId = zce->objPlacementId;
	int playerId = zce->playerId;
	int userId = zce->kanevaUserId;
	std::string playerName = zce->charName;
	NETID netId = zce->From();

	BEGIN_TRY_SQL();

	// Perform DB Query
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << "CALL delete_dynamic_object( %0, %1, %2, %3, @global_id, @inventory_sub_type, @expired_date, @new_dyn_obj_id, @new_playlist_id ); SELECT @global_id, @inventory_sub_type, @expired_date, @new_dyn_obj_id, @new_playlist_id; ";
	query.parse();
	StoreQueryResult res = query.store(
		(int)zoneIndex.getClearedInstanceId(),
		(int)zoneIndex.GetInstanceId(),
		(int)objId,
		(int)userId);
	res = query.store_next(); // result set for the select

	if (res.num_rows() == 1) {
		// if everything ok, add the items to player
		long glid = res.at(0).at(0);
		if (glid != 0) { // did we get a valid glid?
			// If expired_date is null, add the item back into their inventory. It was
			// removed when placing it in the dynamic_objects table. If the expired_date is not null,
			// then the item was placed in the dynamic_objects table (try-on item) but never existed in
			// the user's inventory, thus, don't add it back in.

			trans.commit();
			if (glid != DONT_ADD)
				zce->globalId = glid;
			else
				zce->globalId = 0;

			// broadcast out the event, need to copy the event to avoid double delete
			zce->ret = NO_ERROR;

			zce->broadcastEvent = new RemoveDynamicObjectEvent(objId);

			// DRF - ED-1173 - Build Mode Fixes
			// Add PlayerId as filter So client can filter out based on who is performing
			// the object removal from things like adding it to the user's undo/redo stack.
			if (zce->broadcastEvent)
				zce->broadcastEvent->SetFilter(playerId);
		} else {
			LogError("Object Not Found For Return To Inventory - " << zce->ToStr());
			zce->replyEvent = new RenderLocaleTextEvent(LS_ITEM_DELETED, eChatType::System, numToString(objId).c_str());
			zce->replyEvent->AddTo(netId);
		}
	} else {
		LogError("Query Unexpected Result (rows=" << res.num_rows() << ") - " << zce->ToStr());
		trans.rollback();
		zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
		zce->replyEvent->AddTo(netId);
	}
	END_TRY_SQL();
}

void MySqlEventProcessor::rotateDynamicObject(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	ZoneIndex zoneIndex = zce->zoneIndex;
	int objId = zce->objPlacementId;
	std::string playerName = zce->charName;

	BEGIN_TRY_SQL();

	// Perform DB Query
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " CALL rotate_dynamic_object( %0, %1, %2, %3, %4, %5, %6, %7, %8 ) ";
	query.parse();
	SimpleResult res = query.execute(
		zce->dx, zce->dy, zce->dz,
		zce->sx, zce->sy, zce->sz,
		objId,
		(int)zoneIndex.getClearedInstanceId(),
		(int)zoneIndex.GetInstanceId());
	trans.commit();

	// broadcast out the event, need to copy the event to avoid double delete
	zce->ret = NO_ERROR;

	zce->broadcastEvent = new RotateDynamicObjectEvent(
		objId,
		zce->dx, zce->dy, zce->dz,
		zce->sx, zce->sy, zce->sz,
		zce->time);
	END_TRY_SQL();
}

void MySqlEventProcessor::moveDynamicObject(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	ZoneIndex zoneIndex = zce->zoneIndex;
	int objId = zce->objPlacementId;
	std::string playerName = zce->charName;

	BEGIN_TRY_SQL();

	// Perform DB Query
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " CALL move_dynamic_object( %0, %1, %2, %3, %4, %5, %6, %7, %8, %9, %10, %11) ";
	query.parse();
	SimpleResult res = query.execute(
		zce->x, zce->y, zce->z,
		zce->dx, zce->dy, zce->dz,
		zce->sx, zce->sy, zce->sz,
		objId,
		(int)zoneIndex.getClearedInstanceId(),
		(int)zoneIndex.GetInstanceId());
	trans.commit();

	// broadcast out the event, need to copy the event to avoid double delete
	zce->ret = NO_ERROR;

	zce->broadcastEvent = new MoveAndRotateDynamicObjectEvent(
		objId,
		zce->x, zce->y, zce->z,
		zce->dx, zce->dy, zce->dz,
		zce->sx, zce->sy, zce->sz,
		zce->time);
	END_TRY_SQL();
}

void MySqlEventProcessor::updateDynamicObjectFriendTexture(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	ZoneIndex zoneIndex = zce->zoneIndex;
	int objId = zce->objPlacementId;
	std::string playerName = zce->charName;
	NETID netId = zce->From();

	BEGIN_TRY_SQL();

	// Perform DB Query
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " CALL update_picture_frame_friend( %0, %1, %2, %3, @textureURL, @rc );  select @textureURL, @rc;";
	query.parse();
	StoreQueryResult res = query.store(
		zce->assetId,
		objId,
		(int)zoneIndex.getClearedInstanceId(),
		(int)zoneIndex.GetInstanceId());
	res = query.store_next();
	trans.commit();

	if (res.num_rows() == 1) {
		if ((LONG)res.at(0).at(1) == ERROR_INVALID_ACCESS) {
			LogWarn("Zone Needs Pass - " << zce->ToStr());
			zce->replyEvent = new RenderLocaleTextEvent(LS_ZONE_NEEDS_PASS, eChatType::System);
			zce->replyEvent->AddTo(netId);
		} else {
			zce->ret = NO_ERROR;
			std::string friendImageUrl = res.at(0).at(0);
			zce->broadcastEvent = new UpdatePictureFrameFriendEvent(
				objId,
				zce->assetId,
				friendImageUrl.c_str());
		}
	} else {
		LogError("Query Unexpected Result - " << zce->ToStr());
		zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
		zce->replyEvent->AddTo(netId);
	}
	END_TRY_SQL();
}

void MySqlEventProcessor::updateDynamicObjectAnimation(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	BEGIN_TRY_SQL();

	// DRF - TODO - Must Add zce->animSettings !!!

	// I only want to insert if the dynamic object has a placement id.  Otherwise, it's a generated
	// object that isn't currently visible to people who arrive anyway.
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << "CALL setDynamicObjectAnimation(%0, %1, %2, @result); SELECT @result;";
	query.parse();
	query.store(zce->objPlacementId, PARAM_ANIMATION_GLID, zce->globalId); // drf - changed assetId to globalId
	StoreQueryResult res = query.store_next();

	if (res.size() > 0) {
		Row r = res.at(0);
		int result = r.at(0);
		if (result > 0) {
			// success, else an error and roll back
			zce->ret = NO_ERROR;
			trans.commit();
		}
	}
	checkMultipleResultSets(query, "setDynamicObjectAnimation");
	END_TRY_SQL();
}

void MySqlEventProcessor::updateDynamicObjectTexture(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " CALL update_dynamic_object_texture( %0, %1, %2, %3, %4q, @rc, @textureURL ); select @rc, @textureURL;";
	query.parse();
	StoreQueryResult res = query.store(zce->assetId, zce->objPlacementId, (int)zce->zoneIndex.getClearedInstanceId(), (int)zce->zoneIndex.GetInstanceId(), zce->textureUrl);
	res = query.store_next();
	trans.commit();

	if (res.num_rows() == 1) {
		// broadcast out the event, need to copy the event to avoid double delete
		if ((int)res.at(0).at(0) == 0) { // ok
			zce->ret = NO_ERROR;
			std::string assetUrl = res.at(0).at(1);
			zce->broadcastEvent = new UpdateDynamicObjectTextureEvent(zce->objPlacementId, zce->assetId, assetUrl.c_str(), zce->playerId);
		} else if ((int)res.at(0).at(0) == ERROR_INVALID_ACCESS) {
			// error, don't have access, could be UGC
			zce->replyEvent = new RenderLocaleTextEvent(LS_ZONE_NEEDS_PASS, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		} else {
			// error, don't have access, could be UGC
			zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		}
	} else {
		// error, don't have access
		zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
		zce->replyEvent->AddTo(zce->From());
	}

	END_TRY_SQL();
}

void MySqlEventProcessor::updateDynamicObjectParticle(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " CALL update_dynamic_object_particle( %0, %1, %2, %3q, @rc ); select @rc;";
	query.parse();
	StoreQueryResult res = query.store(zce->objPlacementId, (int)zce->zoneIndex.getClearedInstanceId(), (int)zce->zoneIndex.GetInstanceId(), zce->globalId);
	res = query.store_next();
	trans.commit();

	if (res.num_rows() == 1) {
		// broadcast out the event, need to copy the event to avoid double delete
		if ((int)res.at(0).at(0) == 0) { // ok
			zce->ret = NO_ERROR;
			// Basic "use particle" scenario (as of March 2011 - YC):
			//		Add a particle at DO pivot point (offset=0,0,0), facing positive X, up is positive Y
			// We will improve this with offset and orientations once particle is open to public and build-mode particle adjustment UI available.
			// See also: ZoneCustomizationDownloadHandler::processDynamic (ClientEngine_EventHandling.cpp).
			if (zce->globalId != GLID_INVALID) {
				zce->broadcastEvent = new AddParticleEvent(zce->globalId, ObjectType::DYNAMIC, zce->objPlacementId, "", 0, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
			} else {
				zce->broadcastEvent = new RemoveParticleEvent(ObjectType::DYNAMIC, zce->objPlacementId, "", 0);
			}
		} else if ((int)res.at(0).at(0) == ERROR_INVALID_ACCESS) {
			// error, don't have access, could be UGC
			zce->replyEvent = new RenderLocaleTextEvent(LS_ZONE_NEEDS_PASS, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		} else {
			// error, don't have access, could be UGC
			zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		}
	} else {
		// error, don't have access
		zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
		zce->replyEvent->AddTo(zce->From());
	}

	END_TRY_SQL();
}

void MySqlEventProcessor::updateDynamicObjectSound(ZoneCustomizationBackgroundEvent* zce) {
	if (!zce)
		return;

	zce->ret = E_INVALIDARG;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " CALL update_dynamic_object_sound( %0, %1q, @rc ); select @rc;";
	query.parse();
	StoreQueryResult res = query.store(zce->objPlacementId, zce->globalId);
	res = query.store_next();
	trans.commit();

	if (res.num_rows() == 1) {
		if ((int)res.at(0).at(0) == 0) { // ok
			zce->ret = NO_ERROR;
			if (zce->broadcastEvent)
				zce->broadcastEvent->Delete(); //currently must do add dynamic object to prime in DB but we want to broadcast add sound
			zce->broadcastEvent = new AddSoundEvent(PLACEMENT_PLACE, zce->globalId, static_cast<int>(ObjectType::DYNAMIC), zce->objPlacementId, 0, zce->x, zce->y, zce->z);
		} else if ((int)res.at(0).at(0) == ERROR_INVALID_ACCESS) {
			// error, don't have access, could be UGC
			zce->replyEvent = new RenderLocaleTextEvent(LS_ZONE_NEEDS_PASS, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		} else {
			// error, don't have access, could be UGC
			zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		}
	} else {
		// error, don't have access
		zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
		zce->replyEvent->AddTo(zce->From());
	}

	END_TRY_SQL();
}

/// add either type of dyn object and remove the glid from inventory
void MySqlEventProcessor::addDynamicObject(ZoneCustomizationBackgroundEvent* zce) {
	bool attachable = zce->type == ZoneCustomizationBackgroundEvent::ZC_ADD_FRAME;
	const char* diagString = attachable ? "addDynamicObject attachable" : "addDynamicObject regular";

	zce->ret = ERROR_NOT_FOUND;
	zce->objPlacementId = -1;

	BEGIN_TRY_SQL_FOR(diagString);

	int AssetGroupId = 0;

	Transaction trans(*m_conn);
	Query q1 = m_conn->query();
	q1 << "CALL check_for_zone_playlist( %0, %1, %2, %3, @assetGroupId ); select ifnull(@assetGroupId,0);";
	q1.parse();
	StoreQueryResult res1 = q1.store(
		(int)zce->zoneIndex.getClearedInstanceId(), (int)(attachable ? 1 : 0), (int)zce->zoneIndex.GetInstanceId(), (int)zce->globalId);

	res1 = q1.store_next(); // result set for the select
	if (res1.num_rows() != 0)
		AssetGroupId = res1.at(0).at(0);

	// all dynamic objects are either unlimited or infinite
	int newInvSubType = (zce->inventorySubType & IT_UNLIMITED) ? IT_UNLIMITED : IT_INFINITE;

	Query query = m_conn->query();
	query << "CALL insert_dynamic_object( %0, %1, %2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14, %15, %16, %17, %18, @objSerialNum ); SELECT @objSerialNum;";
	query.parse();
	StoreQueryResult res = query.store(
		(int)zce->playerId,
		(int)zce->zoneIndex.getClearedInstanceId(),
		(int)zce->zoneIndex.GetInstanceId(),
		(int)0,
		(int)zce->globalId,
		newInvSubType,
		zce->x,
		zce->y,
		zce->z,
		zce->dx,
		zce->dy,
		zce->dz,
		zce->sx,
		zce->sy,
		zce->sz,
		(int)zce->textureId,
		(int)(attachable ? 1 : 0),
		(int)zce->expiredDuration,
		AssetGroupId);

	res = query.store_next(); // result set for the select

	if (res.num_rows() != 0) {
		zce->objPlacementId = res.at(0).at(0);
	}

	if (zce->objPlacementId > 0) {
		bool ok = false;
		bool delInvFlag = (newInvSubType & (IT_UNLIMITED | IT_INFINITE)) ? true : false;

		if (zce->tryon || delInvFlag || !localPlayers()) {
			ok = true;
		} else {
			// remove the item from the user if not try on
			query.reset();
			query << "CALL delete_inventory_item( %0, %1, 'P', %2, 1, @delRet ); select @delRet;";
			query.parse();
			LogTrace(query.str((int)zce->playerId, (int)zce->globalId, (int)zce->inventorySubType));
			res = query.store((int)zce->playerId, (int)zce->globalId, (int)zce->inventorySubType);
			res = query.store_next(); // our select
			if (res.num_rows() != 0) {
				zce->ret = res.at(0).at(0);
				if (zce->ret == 0) {
					ok = true;
				}
			}
		}
		if (ok) {
			trans.commit();

			// No animation on a newly placed object for now.  This is where a default could go.
			GLID animGlidSpecial = GLID_INVALID;

			zce->ret = 0;
			zce->broadcastEvent = new AddDynamicObjectEvent(
				PLACEMENT_PLACE,
				zce->playerId,
				zce->globalId,
				zce->x,
				zce->y,
				zce->z,
				zce->dx,
				zce->dy,
				zce->dz,
				zce->sx,
				zce->sy,
				zce->sz,
				zce->objPlacementId,
				animGlidSpecial,
				zce->textureId,
				"", // empty texture Url on add
				0, // leave friend id as 0 because it not set yet
				"", "", // empty swf and params initially
				false, false, false, false, // default values for dynamic object attributes
				-0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, // send a default boundbox for now
				zce->derivable, // UGC derivable flag
				zce->inventorySubType);

			// This reply event is added to ZCE in
			// ServerEngine::GenerateDynamicObject(), so that
			// ServerEngine::ZoneCustomizationBackgroundEventHandler()
			// has the information it needs to send a reply
			// back to the script that asked for the object
			// to be generated.  Normally, MySqlEventProcessor uses
			// the replyEvent to send messages to the client.
			// the objectId is used to store the script server Id,
			// so we have to add the placement id do the out buffer.
			if (zce->replyEvent) {
				*(zce->replyEvent)->OutBuffer() << zce->objPlacementId;
			}
		}
	} else {
		LogWarn("Failed to get objectSerialNumber - " << zce->ToStr());
	}

	END_TRY_SQL();
}

// get a list of users who are blocking this user
void MySqlEventProcessor::getCommBlockingUsers(LONG blockedKanevaId, std::vector<ULONG>& ids) {
	START_TIMING(m_timingLogger, "DB:GetCommBlockingUsers");

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "CALL get_blocked_users( %0, %1, %2 )";
	query.parse();
	StoreQueryResult res = query.store((int)blockedKanevaId, 3, 1); // 3 = zoneType, 1 = comm_blocked

	// for SPs MUST call these to avoid disconnect
	checkMultipleResultSets(query, "get_blocked_users");

	for (ULONG i = 0; i < res.num_rows(); i++)
		ids.push_back(res.at(i).at(0));

	END_TRY_SQL();
}

bool MySqlEventProcessor::populateAccountXMLFromWebCall(MySQLGetResultSetsEvent* grse) {
	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;

	std::string url(m_conn->getPlayerUrl());
	std::string::size_type p = url.find("{0}");

	if (p != std::string::npos) {
		url = url.replace(p, 3, numToString(grse->kanevaUserId, "%d"));

		p = url.find("{1}");
		if (p != std::string::npos) {
			// Full inventory access enabled? Full -> all clothing/accessories, otherwise ARMED only. GMs also get builder items in addition.
			bool bFullInvAccess = m_dbConfig.config().Get(DB_FULL_INVENTORY, false);
			url = url.replace(p, 3, bFullInvAccess ? GET_INV_ALL : GET_INV_ARMED_FURN);
		}

		p = url.find("{2}");
		if (p != std::string::npos) {
			url = url.replace(p, 3, numToString(m_serverNetwork->pinger().GetGameId(), "%d"));
		}

		size_t resultSize = 0;
		if (GetBrowserPage(url, grse->resultXml, resultSize, httpStatusCode, httpStatusDescription)) {
			grse->ret = PLAYER_FOUND;
			return true;
		} else {
			grse->ret = E_INVALIDARG;
			LogWarn("populateAccountXMLFromWebCall Error: " << httpStatusCode << " " << httpStatusDescription);
			return false;
		}
	} else {
		LogWarn("populateAccountXMLFromWebCall URL missing {0}");
		jsAssert(false); // need player id
		return false;
	}
}

static IEvent* makeBrowserReply(IDispatcher& disp, NETID client, ULONG retCode, long replyType, const char* msg) {
	IEvent* e = disp.MakeEvent(disp.GetEventId(BROWSER_REPLY_EVENT_NAME));
	jsVerifyDo(e != 0, return 0);

	e->SetFilter(retCode);
	(*e->OutBuffer()) << replyType << msg;
	e->AddTo(client);

	return e;
}

// DRF - Added
static IEvent* NewUpdateDynamicObjectMediaEvent(
	int objId,
	const MediaParams& mediaParams,
	const ZoneIndex& curZoneIndex,
	NETID netIdTo, // 0=local (PlaylistMgr only)
	NETID netIdFrom) {
	// now fire only locally to let handler process it
	IEvent* e = new UpdateDynamicObjectMediaEvent(
		objId,
		mediaParams,
#ifndef REFACTOR_INSTANCEID
		(LONG)curZoneIndex,
#else
		curZoneIndex.toLong(),
#endif
		netIdTo);
	e->SetFrom(netIdFrom);
	return e;
}

// DRF - Added
void MySqlEventProcessor::updateDynamicObjectMedia(ZoneCustomizationBackgroundEvent* zce) {
	jsVerifyReturnVoid(zce->objPlacementId != 0);

	// Assume Media Params (don't change volume or radius)
	MediaParams mediaParams("", "", -1, -1, -1);

	if (zce->playlistId != INT_MAX) {
		if (zce->playlistId != 0) {
			// Set Media Params With Playlist Url & Params
			mediaParams.SetUrl(m_playlistSwf);
			mediaParams.SetParams(m_playlistParams + std::to_string(zce->playlistId)); // plId=nnnnnnn
			mediaParams.SetPlaylistId(zce->playlistId);
		}

		assert(zce->objPlacementId > 0);
		if (zce->objPlacementId <= 0) {
			// Local or bad placement ID: should not be here
			LogWarn("Invalid placement ID " << zce->objPlacementId << " from netId " << zce->networkAssignedId);
		} else {
			// update the database
			BEGIN_TRY_SQL_FOR("updateDynObjPlaylistId");

			Transaction trans(*m_conn);
			Query query = m_conn->query();
			query << "CALL update_dyn_obj_playlist_id( %0, %1, %2, %3 )";
			query.parse();

			std::string queryStr = query.str(
				(int)zce->playlistId,
				zce->objPlacementId,
				(int)zce->zoneIndex.getClearedInstanceId(),
				(int)zce->zoneIndex.GetInstanceId());

			// now send update to all clients
			StoreQueryResult r = query.store(
				(int)zce->playlistId,
				zce->objPlacementId,
				(int)zce->zoneIndex.getClearedInstanceId(),
				(int)zce->zoneIndex.GetInstanceId());

			// for SPs MUST call these to avoid disconnect
			checkMultipleResultSets(query, "updateDynObjPlaylistId");

			if (r.size() > 0) { // get one row back if ok, nothing if can't access it
				LogInfo("OK - " << queryStr);

				// List of affected placement IDs (can be many if global media player)
				std::vector<int> affectedPlacementIds;
				affectedPlacementIds.reserve(r.size());
				for (auto i = 0u; i < r.size(); i++) {
					const mysqlpp::Row& row = r.at(i);
					affectedPlacementIds.push_back((int)row[0]);
				}

				// now fire only locally to let handler process it (PlaylistMgr)
				zce->replyEvent = new DynamicObjectMediaChangedEvent(zce->From(), zce->zoneIndex, affectedPlacementIds, mediaParams);
			} else {
				LogError("FAILED - " << queryStr);
				zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
				zce->replyEvent->AddTo(zce->From());
			}

			trans.commit();

			END_TRY_SQL_LOG_ONLY(m_conn);
		}
	} else if (!zce->itemUrl.empty() && !zce->mediaParams.GetParams().empty()) {
		BEGIN_TRY_SQL_FOR("updateDynObjMedia");

		Transaction trans(*m_conn);
		Query query = m_conn->query();
		query << "CALL update_dyn_obj_media( %0, %1q, %2q, %3 )";
		query.parse();

		int mediaType = 1; // TODO: get correct value

		std::string queryStr = query.str(
			(int)mediaType,
			zce->itemUrl,
			zce->mediaParams.GetParams(),
			zce->objPlacementId);

		// now send update to all clients
		StoreQueryResult r = query.store(
			(int)mediaType,
			zce->itemUrl,
			zce->mediaParams.GetParams(),
			zce->objPlacementId);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "updateDynObjMedia");

		if (r.size() > 0) { // get one row back if ok, nothing if can't access it
			LogWarn("DEAD CODE OK - " << queryStr);

			// Cancel Broadcast
			mediaParams.SetUrl("");
			mediaParams.SetParams(CANCEL_BROADCAST);

			zce->replyEvent = makeBrowserReply(m_dispatcher, zce->From(), NO_ERROR, REPLY_CLOSE_WINDOW, "Url set on item");

			//No swf so send cancel for swf and params, playlist manager will handle
			zce->broadcastEvent = NewUpdateDynamicObjectMediaEvent(
				zce->objPlacementId,
				mediaParams,
				zce->zoneIndex,
				zce->networkAssignedId,
				zce->From());
		} else {
			LogError("DEAD CODE FAILED - " << queryStr);
			zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		}

		trans.commit();

		END_TRY_SQL_LOG_ONLY(m_conn);

	} else if (!zce->itemUrl.empty()) {
		// update the database
		BEGIN_TRY_SQL_FOR("updateDynObjUrl");

		Transaction trans(*m_conn);
		Query query = m_conn->query();
		query << "CALL update_dyn_obj_url( %0q, %1 )";
		query.parse();

		std::string queryStr = query.str(zce->itemUrl, zce->objPlacementId);

		// now send update to all clients
		StoreQueryResult r = query.store(zce->itemUrl, zce->objPlacementId);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "updateDynObjUrl");

		if (r.size() > 0) { // get one row back if ok, nothing if can't access it
			LogWarn("DEAD CODE OK - " << queryStr);

			zce->replyEvent = makeBrowserReply(m_dispatcher, zce->From(), NO_ERROR, REPLY_CLOSE_WINDOW, "Url set on item");

			// broadcast out the event to tell them to update, send empty if to turn off
			if (!zce->itemUrl.empty()) {
				// Set Media Params With Playlist Url & Params
				mediaParams.SetUrl(m_playlistSwf);
				mediaParams.SetParams("playlistUrl=" + UrlParser::escapeString(zce->itemUrl));
			}

			// DRF - Huh?
			mediaParams.SetParams(CANCEL_BROADCAST);

			//tell playlist manager to stop broadcasting playlist
			zce->broadcastEvent = NewUpdateDynamicObjectMediaEvent(
				zce->objPlacementId,
				mediaParams,
				zce->zoneIndex,
				zce->networkAssignedId,
				zce->From());
		} else {
			LogError("DEAD CODE FAILED - " << queryStr);
			zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		}

		trans.commit();

		END_TRY_SQL_LOG_ONLY(m_conn);
	} else if (zce->assetId >= 0) { // asset id of zero means turn off media

		BEGIN_TRY_SQL_FOR("updateDynObjMediaAsset");

		Transaction trans(*m_conn);
		Query query = m_conn->query();
		query << "CALL update_dyn_obj_media_asset( %0, %1q, %2, %3q, %4, %5, %6, %7 )";
		query.parse();

		std::string queryStr = query.str(
			zce->assetId,
			zce->mediaParams.GetUrl(),
			zce->assetType,
			zce->mature,
			zce->objPlacementId,
			(int)zce->zoneIndex.getClearedInstanceId(),
			(int)zce->zoneIndex.GetInstanceId(),
			zce->stopPlay);

		// now send update to all clients
		StoreQueryResult r = query.store(
			zce->assetId,
			zce->mediaParams.GetUrl(),
			zce->assetType,
			zce->mature,
			zce->objPlacementId,
			(int)zce->zoneIndex.getClearedInstanceId(),
			(int)zce->zoneIndex.GetInstanceId(),
			zce->stopPlay);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "updateDynObjMediaAsset");

		if (r.size() > 0) { // get one row back if ok or need AP, nothing if can't access it
			trans.commit();

			if (r.at(0).size() == 1 && (LONG)r.at(0).at(0) == ERROR_INVALID_ACCESS) {
				LogError("ZONE NEEDS PASS - " << queryStr);
				zce->replyEvent = new RenderLocaleTextEvent(LS_ZONE_NEEDS_PASS, eChatType::System);
				zce->replyEvent->AddTo(zce->From());
			} else {
				LogInfo("OK - " << queryStr);

				zce->replyEvent = makeBrowserReply(m_replyDispatcher, zce->From(), NO_ERROR, REPLY_CLOSE_WINDOW, "Url set on item");

				// Set Media Params
				std::string url = r.at(0).at(0);
				mediaParams.SetUrl(url);
				mediaParams.SetParams("");

				// DRF - Huh?
				mediaParams.SetParams(CANCEL_BROADCAST);

				// broadcast out the event to tell them to update, send empty if to turn off
				zce->broadcastEvent = NewUpdateDynamicObjectMediaEvent(
					zce->objPlacementId,
					mediaParams,
					zce->zoneIndex,
					zce->networkAssignedId,
					zce->From());
			}
		} else {
			LogError("FAILED - " << queryStr);
			zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		}

		END_TRY_SQL_LOG_ONLY(m_conn);
	} else {
		LogError("FAILED");
		zce->replyEvent = new RenderLocaleTextEvent(LS_DYNOBJ_NO_PERMISSION, eChatType::System);
		zce->replyEvent->AddTo(zce->From());
	}
}

void MySqlEventProcessor::updateMediaPlayerVolumeAndRadius(ZoneCustomizationBackgroundEvent* zce) {
	std::string sqlInsert = "INSERT INTO dynamic_object_playlists(zone_index, zone_instance_id, obj_placement_id, global_id, asset_group_id";
	std::string sqlValues = " VALUES (%0, %1, %2, %3, 0";
	std::string sqlUpdate = " ON DUPLICATE KEY UPDATE ";

	// Formulate SQL conditionally based on validity of parameters
	bool appended = false;
	if (zce->mediaParams.GetRadiusVideo() != -1) {
		sqlInsert.append(", video_range");
		sqlValues.append(", %4");
		sqlUpdate.append(" video_range = VALUES(video_range)");
		appended = true;
	}

	if (zce->mediaParams.GetRadiusAudio() != -1) {
		sqlInsert.append(", audio_range");
		sqlValues.append(", %5");
		if (appended)
			sqlUpdate.append(",");
		sqlUpdate.append(" audio_range = VALUES(audio_range)");
		appended = true;
	}

	if (zce->mediaParams.GetVolume() != -1) {
		sqlInsert.append(", volume");
		sqlValues.append(", %6");
		if (appended)
			sqlUpdate.append(",");
		sqlUpdate.append(" volume = VALUES(volume)");
		appended = true;
	}

	if (!appended) {
		// nothing to do
		return;
	}

	// Query global ID
	int globalId = 0;
	{
		BEGIN_TRY_SQL_FOR("updateDynamicObjectMediaRadiiAndVolume-SELECT-globalID");

		Query query = m_conn->query();
		query << "SELECT global_id FROM dynamic_objects WHERE obj_placement_id=%0;";
		query.parse();

		auto rs = query.store(zce->objPlacementId);

		if (rs.num_rows() != 1) {
			LogError("Error getting global ID by placement ID - " << query.str(zce->objPlacementId));
			return;
		} else {
			globalId = rs.at(0).at(0);
		}

		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	jsAssert(globalId != 0);

	{
		// update the database
		BEGIN_TRY_SQL_FOR("updateDynamicObjectMediaRadiiAndVolume");

		Query query = m_conn->query();
		query << sqlInsert << ")" << sqlValues << ")" << sqlUpdate;

		query.parse();
		std::string queryStr = query.str(
			(int)zce->zoneIndex.getClearedInstanceId(),
			(int)zce->zoneIndex.GetInstanceId(),
			zce->objPlacementId,
			globalId,
			(float)zce->mediaParams.GetRadiusVideo(),
			(float)zce->mediaParams.GetRadiusAudio(),
			(float)zce->mediaParams.GetVolume());

		LogTrace("Executing - " << queryStr);

		// now send update to all clients
		query.execute(
			(int)zce->zoneIndex.getClearedInstanceId(),
			(int)zce->zoneIndex.GetInstanceId(),
			zce->objPlacementId,
			globalId,
			(float)zce->mediaParams.GetRadiusVideo(),
			(float)zce->mediaParams.GetRadiusAudio(),
			(float)zce->mediaParams.GetVolume());

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "updateDynamicObjectMediaRadiiAndVolume");

		LogInfo("OK - " << queryStr);

		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	// Tell PlaylistMgr to update radius and volume
	// now fire only locally to let handler process it
	// playlist manager recieves (py)
	zce->replyEvent = new UpdateDynamicObjectMediaVolumeAndRadiusEvent(zce->objPlacementId, zce->mediaParams, zce->zoneIndex.toLong());
	zce->ret = NO_ERROR;
}

class MemCacheRequest {
public:
	MemCacheRequest() :
			_baseURL() {}

	MemCacheRequest(const URL& baseURL) :
			_baseURL(baseURL) {}

	virtual ~MemCacheRequest() {}

	virtual URL dropCompound(const std::string& key) {
		return _baseURL.clearParams().setParam("action", "dropCompound").setParam("key", key);
	}

private:
	URL _baseURL;
};

// http://localhost/wok/kgp/memcache.aspx%3faction%3ddropCompound%26key%3dwok.zz.805306414,1053374
// http://localhost/wok/kgp/memcache.aspx?action=dropCompound&key=wok.zz.805306414,1053374
// http://localhost/wok/kgp/memcache.aspx?action=dropCompound&key=wok.zz.805306414,1053374
bool MySqlEventProcessor::dropCompoundCacheItem(const std::string& key) const {
	std::string results;
	DWORD rcode;
	std::string rdesc;

	MemCacheRequest request(URL::parse(m_conn->getMemcacheAPIUrl()));

	std::string url = request.dropCompound(key).encodedString();
	size_t resultSize = 0;
	if (GetBrowserPage(
			url, results, resultSize, rcode, rdesc)) {
		return true;
	}

	LogError("Infrastructure Error: Failed to invalidate memcache [" << url << "][" << rcode << "][" << rdesc << "]");
	return false;
}

///////////////////////////////////////////////////////////
/// handler for ChangeZoneEvents that main thd gets, preprocesses
/// then passes onto the background the for the db calls
EVENT_PROC_RC MySqlEventProcessor::ZoneCustomizationHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ZoneCustomizationBackgroundEvent* zce = dynamic_cast<ZoneCustomizationBackgroundEvent*>(e);
	jsVerifyReturn(zce != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);
	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;

	ZoneIndex zoneIndex = zce->zoneIndex;
	auto actionType = zce->type;

	LogInfo(zce->ToStr());

	try {
		switch (actionType) {
			case ZoneCustomizationBackgroundEvent::ZC_ADD:
			case ZoneCustomizationBackgroundEvent::ZC_ADD_FRAME:
				me->addDynamicObject(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_UPDATE_TEXTURE:
				me->updateDynamicObjectTexture(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_UPDATE_FRIEND:
				me->updateDynamicObjectFriendTexture(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_REMOVE:
				me->removeDynamicObject(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_MOVE:
				me->moveDynamicObject(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_ROTATE:
				me->rotateDynamicObject(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_UPDATE_WORLD_OBJ_TEXTURE:
				me->updateWorldObjectTexture(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_UPDATE_ANIMATION:
				me->updateDynamicObjectAnimation(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_UPDATE_PARTICLE:
				me->updateDynamicObjectParticle(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_ADD_SOUND:
				me->addDynamicObject(zce);
				me->updateDynamicObjectSound(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_UPDATE_ITEM_MEDIA:
				me->updateDynamicObjectMedia(zce);
				break;

			case ZoneCustomizationBackgroundEvent::ZC_UPDATE_MEDIA_VOL_RAD:
				me->updateMediaPlayerVolumeAndRadius(zce);
		};

		// invalidate memcached for each spawn point cache in the zone
		long instanceId = 1;
		if (!zoneIndex.isPermanent() && !zoneIndex.isArena())
			instanceId = zoneIndex.GetInstanceId();

		std::string key("wok.zz.");
		key += numToString(zoneIndex.getClearedInstanceId(), "%d") + numToString(instanceId, ",%d");
		me->dropCompoundCacheItem(key);
		ret = EVENT_PROC_RC::OK;
	} catch (KEPException* ex) {
		LogError("Caught Exception - " << zce->ToStr() << " ret=0x" << std::hex << zce->ret);

		if (!zce->ret)
			zce->ret = E_INVALIDARG; // make sure errors out
		ex->Delete();
	}

	if (zce->ret) {
		LogError("FAILED - " << zce->ToStr() << " ret=0x" << std::hex << zce->ret);

		if (zce->broadcastEvent) {
			zce->broadcastEvent->Delete();
			zce->broadcastEvent = NULL;
		}

		if (zce->replyEvent == NULL) { // assume if reply event it has error message for client
			zce->replyEvent = new RenderTextEvent("Failed Zone Customization", eChatType::System);
			zce->replyEvent->AddTo(zce->From());
		} else {
			jsAssert(zce->replyEvent->EventId() == RenderTextEvent::ClassId() ||
					 zce->replyEvent->EventId() == RenderLocaleTextEvent::ClassId());
		}
	}

	me->m_replyDispatcher.QueueEvent(zce);

	return ret;
}

///////////////////////////////////////////////////////////
/// handler for ChangeZoneEvents that main thd gets, preprocesses
/// then passes onto the background the for the db calls
EVENT_PROC_RC MySqlEventProcessor::ChangeZoneHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ChangeZoneBackgroundEvent* cze = dynamic_cast<ChangeZoneBackgroundEvent*>(e);
	jsVerifyReturn(cze != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;
	cze->ret = PLAYER_NOT_FOUND;

	try {
		START_TIMING(me->m_timingLogger, "DB:ChangeZoneMap");

		KGP_Connection* m_conn = me->m_conn;

		CHECK_CONNECTION

		BEGIN_TRY_SQL();

		std::vector<bg_dynamic_get> results;

		// YC - 09/29/16: Port Ben's ED-8112 fix to change_zone_map SP calls
		// Removed wrapping transaction to alleviate locking issues.

		ZoneIndex cleanZI(cze->oldZoneIndex);
		cleanZI.clearInstance(); // ok to blindly clear since is always mapped instance for apt

		// move everything out of apartment and into inventory
		// delete always from (P)ersonal inv qty of 1
		Query query = m_conn->query();
		query << "CALL change_zone_map( %0, %1, %2, %3q, %4, %5q, @czRet, @urlRet ); CALL delete_inventory_item( %6, %7, 'P', %8, 1, @delRet ); select @czRet, @delRet, @urlRet;";
		query.parse();
		StoreQueryResult res = query.store((int)cze->oldZoneIndex.getClearedInstanceId(),
			(int)cze->oldZoneIndex.GetInstanceId(),
			(int)cze->newZoneIndex.getClearedInstanceId(),
			(const char*)cze->charName.c_str(),
			(int)cze->globalId,
			me->pingerGameName(),
			(int)cze->playerId,
			(int)cze->globalId,
			cze->invType);
		query.store_next(); // second SP call
		res = query.store_next(); // our final select

		if (res.num_rows() == 1) {
			LONG temp = res.at(0).at(0);
			cze->ret = temp; // odd?? doing assignment w/o ret did nothing??
			temp = res.at(0).at(1);

			if (cze->ret != 0) { // did we update it ok?
				throw new KEPException("change_zone_map failed", cze->ret);
			} else if (temp != 0) { // delete failed
				cze->ret = temp;
				throw new KEPException("change_zone_map failed due inventory", cze->ret);
			}

			cze->url = res.at(0).at(2);
		}

		END_TRY_SQL();
	} catch (KEPException* e) {
		cze->ret = e->m_err == 0 ? E_INVALIDARG : e->m_err;
		LogError("Caught Exception - '" << e->ToString() << "'");
		e->Delete();
	}

	// always send reply
	me->m_replyDispatcher.QueueEvent(cze);

	return ret;
}

///////////////////////////////////////////////////////////
/// handler for ChangeZoneEvents that main thd gets, preprocesses
/// then passes onto the background the for the db calls
EVENT_PROC_RC MySqlEventProcessor::CanSpawnToZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	CanSpawnToZoneBackgroundEvent* csze = dynamic_cast<CanSpawnToZoneBackgroundEvent*>(e);
	jsVerifyReturn(lparam != 0 && csze != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;

	if (!me->checkServerId("CanSpawnToZoneBackgroundHandler", true))
		return EVENT_PROC_RC::NOT_PROCESSED;

	csze->ret = PLAYER_NOT_FOUND;

	Transaction* trans = 0;
	bool doTrans = !jsFileExists("skipspawntransaction", 0);

	NETID currentNetID = csze->From();
	int playerId = csze->playerId;
	int kanevaUserId = (int)csze->ObjectId();

	try {
		START_TIMING(me->m_timingLogger, "DB:CanSpawnToZoneBackgroundHandler");

		// make macros happy
		KGP_Connection* m_conn = me->m_conn;

		CHECK_CONNECTION

		csze->ret = ZONE_FULL;

		// call the SP to see if this player can spawn to the zone
		CHECK_CONNECTION

		BEGIN_TRY_SQL();

		if (doTrans)
			trans = new Transaction(*m_conn);

		/*
		call the SP to check to see if the player can spawn.
		Note that server:port can change for redirect and instanceId can also change if zone is full
		can_spawn_to_zone( playerId INT(11),
							kanevaUserId INT(11),
							zoneType INT(11),
							zoneIndex INT(11),
							instanceId INT(11),
							myServerId INT(11),
							isGm CHAR,
							country CHAR(2),
							birthDate DATE,
							showMature BINARY(1),
							gotoPlayer BOOLEAN,
							gameNameNoSpaces varchar(100),
							reasonCode INT,
							retInstanceId INT(11),
							serverId INT(11),
							scriptServerId INT(11),
							coverCharge INT(11),
							zoneOwnerKanevaId INT(11),
							url VARCHAR(300) )
		*/
		Query query = m_conn->query();
		query << "CALL can_spawn_to_zone( %0, %1, %2, %3, %4, %5, %6q, %7q, %8q, %9, %10, %11q, @rc, @ii, @sid, @ssid, @cc, @zowner, @url ); "
				 "SELECT @rc, @ii, @sid, @ssid, @cc, @zowner, @url;";
		query.parse();
		query.store(
			playerId,
			kanevaUserId,
			(int)csze->zoneIndex.GetType(),
			(int)csze->zoneIndex.getClearedInstanceId(),
			(int)csze->zoneIndex.GetInstanceId(),
			(int)me->m_serverId,
			(csze->gm ? DB_TRUE : DB_FALSE),
			csze->country,
			csze->birthDate,
			(int)(csze->showMature ? 1 : 0),
			csze->goingToPlayer,
			me->pingerGameName());
		StoreQueryResult res = query.store_next(); // get select output

		if (res.size() > 0) {
			// if server empty string, then local
			Row r = res.at(0);
			csze->ret = r.at(GOTO_RC_COL);
			if (csze->ret == PLAYER_FOUND) {
				if (trans > 0)
					trans->commit(); // only commit if zoning

				LONG instanceId = r.at(GOTO_INSTANCE_COL);
				csze->zoneIndex.setInstanceId(instanceId);
				csze->url = r.at(GOTO_URL_COL);
				csze->newServerId = r.at(GOTO_SERVER_ID_COL);
				csze->scriptServerId = r.at(GOTO_SCRIPT_SERVER_ID_COL);
				csze->coverCharge = r.at(GOTO_COVER_COL);
				csze->zoneOwnerId = r.at(GOTO_OWNER_COL);
			}
		}

		END_TRY_SQL();

		if (csze->ret == ZONE_FOUND && m_conn->neverSwitchServers() && csze->newServerId != 0) {
			LogWarn("neverSwitchServers is true. Skipping redirect to " << csze->newServerId);
			csze->newServerId = 0;
		}
	} catch (KEPException* e) {
		csze->ret = e->m_err == 0 ? E_INVALIDARG : e->m_err;
		LogWarn("CanSpawnToZoneBackgroundHandler Error: " << e->ToString());
		e->Delete();
	}

	if (csze->ret == ZONE_FOUND)
		me->getCurrentZoneInfoNoThrow(currentNetID, playerId, kanevaUserId, csze->gm, csze->zoneIndex, csze->zonePermissions, csze->parentZoneInstanceId);

	// always send reply
	me->m_replyDispatcher.QueueEvent(csze);
	if (trans > 0)
		delete trans;

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC MySqlEventProcessor::GetAccountFromWebBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	GetResultSetsEvent* grse = dynamic_cast<GetResultSetsEvent*>(e);
	jsVerifyReturn(lparam != 0 && grse != 0, EVENT_PROC_RC::NOT_PROCESSED);
	jsVerifyReturn(grse->Filter() == (LONG)GetResultSetsEvent::PLAYER || grse->Filter() == (LONG)GetResultSetsEvent::ACCOUNT, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;

	if (!me->checkServerId("GetItemBackgroundHandler", true))
		return EVENT_PROC_RC::NOT_PROCESSED;

	grse->ret = PLAYER_NOT_FOUND;

	START_TIMING(me->m_timingLogger, e->Filter() == GetResultSetsEvent::PLAYER ? "DB:GetPlayerByUrl" : "DB:GetAccountByUrl");

	if (e->Filter() == GetResultSetsEvent::PLAYER) {
		// we already loaded player when got acct (WOK-specific)
	} else if (e->Filter() == GetResultSetsEvent::ACCOUNT) {
		// get the the result sets for the account object
		GetAccountEvent* gae = dynamic_cast<GetAccountEvent*>(e);
		CAccountObject* acct = dynamic_cast<CAccountObject*>(gae->item);
		if (acct)
			me->populateAccountXMLFromWebCall(gae);
	}

	if (jsFileExists("BACKGROUND_PLAYERINFO", 0)) {
		// Queue event for handler by lower priority handlers.
		// Our foreground handler is effectively disabled using the
		// same runtime switch above.
		me->m_replyDispatcher.QueueEvent(grse);
		me->getPlayerInfo(grse);
		return EVENT_PROC_RC::CONSUMED;
	} else {
		me->m_replyDispatcher.QueueEvent(grse);
		return EVENT_PROC_RC::CONSUMED;
	}
}

class InventoryTraverser : public InventoryItemListT::Traverser {
public:
	CGlobalInventoryObjList& GIL;
	CInventoryObjList& _targetInv;
	InventoryTraverser(CGlobalInventoryObjList& gil, CInventoryObjList& targetInv) :
			GIL(gil), _targetInv(targetInv) {
	}

	/**
	 * This method fulfills the contract demaned by a Traverser.  In this implementation
	 * it is called for every element in a list of CInventoryObj instances from the head
	 * of the list to the tail or until this method returns TRUE.
	 */
	bool evaluate(CInventoryObj& GIItem) {
		// Find this user inventory item in the GIL
		CGlobalInventoryObj* globalInventoryObj = (CGlobalInventoryObj*)GIL.GetByGLID(GIItem.m_itemData->m_globalID);

		// If this item is not in the GIL...add it.
		if (globalInventoryObj == 0) {
			// If the it wasn't in the GIL, it's possible that's be cause it's a custom item that
			// has been derived from something in the GIL.  Get any inherited data
			//
			if (GIItem.m_itemData->m_baseGlid != 0) {
				CGlobalInventoryObj* baseInventoryObj = (CGlobalInventoryObj*)GIL.GetByGLID(GIItem.m_itemData->m_baseGlid);
				if (baseInventoryObj) {
					((CItemObj*)GIItem.m_itemData)->inheritFrom(*(baseInventoryObj->m_itemData));
				}
			}

			// Assure resource recovery
			globalInventoryObj = new CGlobalInventoryObj();

			// Note that this const mismatch was hidden in the prior implementation.
			// Run old code and evaluate the risk.
			globalInventoryObj->m_itemData = (CItemObj*)GIItem.m_itemData;
			GIL.AddTail(globalInventoryObj);
		} else {
			// this item was already in the global inventory.
			// Let's swap the itemdata on the invobj to use this one instead, and then clean up
			delete GIItem.m_itemData; // CItemObj::SafeDelete now merged with destructor
			GIItem.m_itemData = globalInventoryObj->m_itemData;
		}

		if (!_targetInv.AddOneInventoryItem(GIItem)) {
			// The item was not added to the inventory....so take care of release mem resources
			// With backloading, we load, instantiate and bind CItemObj instances to their corresponding
			// CInventoryObj instance all in the background (read, not on the main event thread)
			// However, one premise of the CGlobalInventoryObj/CItemObj/CInventoryObj triumvirate
			// is that both inventory object instances reference the same CItemObj.  So, it's
			// possible that there is already a bound CItemObj instance in place.  If this
			// is the case, we must release the resources for our newly loaded CItemObj, indeed
			// as this resource was allocated on a background thread, we need to release this resource
			// anyway.
			GIItem.SafeDelete();

			// Calling SafeDelete against the CInventoryObj will cause it to break
			// it's reference to the CItemObj instance.  We don't want to delete that
			// as it should already be referenced in the GIL.

			// Okay, now let's free the CInventoryObj instance.
			delete &GIItem;
		}
		return false;
	}
};

void MySqlEventProcessor::getInventory(INOUT CInventoryObjList& playerInv, CInventoryObjList& bankInv, IN InventoryItemListT& playerInventory, IN InventoryItemListT& bankInventory) {
	// invResult can be empty if no pending items existed to be
	// added to the inventory. Thus, leave inventory as is.
	// Check for global inventory as well
	CGlobalInventoryObjList* GIL = CGlobalInventoryObjList::GetInstance();

	// Guard clause.
	// If we can't get the GIL, then go no further.
	if (GIL == 0) {
		LogError("Can't get CGlobalInventoryObjList::GetInstance in addInventoryItem");
		return;
	}

	// Guard clause -- If there are no inventory records to process, then don't bother.
	if (playerInventory.size() > 0) {
		InventoryTraverser traverser(*GIL, playerInv);
		traverser.traverse(playerInventory);
		// since only updatesOnly current inv, only clear current inv
		playerInv.ClearDirty();
	}

	if (bankInventory.size() > 0) {
		InventoryTraverser(*GIL, bankInv).traverse(bankInventory);
		// since only updatesOnly current inv, only clear current inv
		bankInv.ClearDirty();
	}
}

///////////////////////////////////////////////////////////
/// handler for CanSpawnToZoneBackgroundEvent to check db
/// access then pass back to main thd
EVENT_PROC_RC MySqlEventProcessor::GetItemBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	if (!me->localPlayers())
		return GetAccountFromWebBackgroundHandler(lparam, disp, e);

	GetResultSetsEvent* grse = dynamic_cast<GetResultSetsEvent*>(e);
	jsVerifyReturn(lparam != 0 && grse != 0, EVENT_PROC_RC::NOT_PROCESSED);
	jsVerifyReturn(grse->Filter() == (LONG)GetResultSetsEvent::PLAYER || grse->Filter() == (LONG)GetResultSetsEvent::ACCOUNT, EVENT_PROC_RC::NOT_PROCESSED);

	if (!me->checkServerId("GetItemBackgroundHandler", true))
		return EVENT_PROC_RC::NOT_PROCESSED;

	grse->ret = PLAYER_NOT_FOUND;

	try {
		START_TIMING(me->m_timingLogger, e->Filter() == GetResultSetsEvent::PLAYER ? "DB:GetPlayer" : "DB:GetAccount");

		// make macros happy
		KGP_Connection* m_conn = me->m_conn;

		CHECK_CONNECTION

		BEGIN_TRY_SQL();

		BEGIN_TRANS_WITH_RETRY(*m_conn);

		// get the the result sets for the player object
		Query query = m_conn->query();

		if (e->Filter() == GetResultSetsEvent::PLAYER) {
			GetPlayerEvent* gpe = dynamic_cast<GetPlayerEvent*>(e);
			if (!gpe->reconnected && !gpe->reconnecting && !gpe->isGm) {
				// get friends to tell about login
				me->getOnlineFriendList(gpe->kanevaUserId, gpe->broadcastListArray);
			}

			query << "CALL get_player_data( %0, %1 )";
			query.parse();
			grse->results[0] = query.store((int)grse->ObjectId(), (int)grse->kanevaUserId);
		} else if (e->Filter() == GetResultSetsEvent::ACCOUNT) {
			// get the the result sets for the account object
			GetAccountEvent* gae = dynamic_cast<GetAccountEvent*>(e);
			jsAssert(gae != 0);

			query << "CALL get_account_data( %0, %1, %2q, %3q, %4q, %5q, %6 )";
			query.parse();
			gae->results[0] = query.store(
				(int)me->m_serverId,
				gae->kanevaUserId,
				gae->name,
				gae->currentIP,
				gae->gender,
				gae->birthDate,
				(int)gae->showMature);
		}

		grse->arraySize = 1;
		int unused = 0;
		while (query.more_results()) {
			if (grse->arraySize < MySQLGetResultSetsEvent::MAX_RESULT_SETS) {
				grse->results[grse->arraySize] = query.store_next();
				grse->arraySize++;
			} else {
				StoreQueryResult res = query.store_next();
				unused++;
			}
		}
		if (unused > 0) {
			LogWarn("Query had " << unused << " results sets for event of filter " << e->Filter());
		}

		if (e->Filter() == GetResultSetsEvent::PLAYER) {
			InventoryItemListT InventoryPlayer; // = new InventoryItemListT();
			InventoryItemListT BankInventory; //	= new InventoryItemListT();

			me->backloadInventory(*grse, InventoryPlayer, BankInventory);

			// At this point we have two lists contained in the grse base class GetPlayerEvent
			// that contain:
			// 1) The player inventory
			// 2) The bank inventory
			//
			// in the variables pPlayerInventory and pBankInventory respectively
			// The list mechanisim used is KGenericObjList
			GetPlayerEvent* gpe = dynamic_cast<GetPlayerEvent*>(grse);
			if (gpe->pPlayerInventory)
				((CInventoryObjList*)gpe->pPlayerInventory)->SafeDelete();
			else
				gpe->pPlayerInventory = (void*)new CInventoryObjList();

			if (gpe->pBankInventory)
				((CInventoryObjList*)gpe->pBankInventory)->SafeDelete();
			else
				gpe->pBankInventory = new CInventoryObjList;

			// This method will scan the two lists, and build GlobalInventoryItems
			// where appropriate and it will place these original CInventoryObj objects
			// into CInventoryObjLists which will later be stored in the player
			// object.
			me->getInventory(
				*((CInventoryObjList*)gpe->pPlayerInventory), *((CInventoryObjList*)gpe->pBankInventory), InventoryPlayer, BankInventory);
		}
		grse->ret = 0;

		END_TRANS_WITH_RETRY();

		END_TRY_SQL();
	} catch (KEPException* e) {
		grse->ret = e->m_err == 0 ? E_INVALIDARG : e->m_err;
		LogError("Caught Exception - '" << e->ToString() << "'");
		e->Delete();
	}

	if (jsFileExists("BACKGROUND_PLAYERINFO", 0)) {
		// The same runtime switch above is used on the high priority
		// handler for this event.  So we queue the event so that
		// lower priority handlers can still process.
		me->m_replyDispatcher.QueueEvent(grse);
		// Then do our heavy weight processing here.
		me->getPlayerInfo(grse);
		return EVENT_PROC_RC::CONSUMED;
	} else {
		me->m_replyDispatcher.QueueEvent(grse);
		return EVENT_PROC_RC::CONSUMED;
	}
}

enum RS_17_COLUMNS {
	IS_PLAYERS_INVENTORY // As opposed to being a base item from which
	// of the players items is derived
	,
	ITEM_GLOBAL_ID,
	ITEM_NAME,
	ITEM_DESCRIPTION,
	ITEM_MARKET_COST,
	ITEM_SELLING_PRICE,
	ITEM_REQUIRED_SKILL,
	ITEM_REQUIRED_SKILL_LEVEL,
	ITEM_USE_TYPE,
	ITEM_CREATOR_ID,
	ITEM_ARM_ANYWHERE,
	ITEM_DISARMABLE,
	ITEM_STACKABLE,
	ITEM_DESTROY_WHEN_USED,
	ITEM_INVENTORY_TYPE // from the item table, and called "inventory_type", this is not to
	// be confused with "inventory_type" found in the inventories table
	,
	ITEM_CUSTOM_TEXTURE,
	ITEM_BASE_GLOBAL_ID,
	ITEM_EXPIRED_DURATION,
	ITEM_PERMANENT,
	ITEM_IS_DEFAULT,
	ITEM_VISUAL_REF,
	ITEM_WEB_CONTROLLED_EDIT,
	ITEM_IS_DERIVABLE,
	ITEM_DERIVATION_LEVEL,
	ITEM_ACTOR_GROUIP,
	ITEM_SEARCH_REFRESH_NEEDED,
	ITEM_USE_VALUE,
	ITEM_XCLUSION_GROUPS,
	INV_TYPE,
	INV_SUB_TYPE,
	INV_ARMED,
	INV_QUANTITY,
	INV_CHARGE_QUANTITY
};

void popInvObjFromTuple(Row& row, CInventoryObj& invObj) {
	// N.B.: While one might think that a field in the datamodel called "inventory_type" would
	// make to a member var call "m_inventoryType" in the object model.  Nope.
	// "inventoryType" in the datamodel refers to the target player inventory.  Each player
	// can have up to three separate inventories: player, bank, housing (housing is likely
	// obsolete).  The column "sub_inventory_type" defined in the datamodel is the peer
	// to "inventory_type" in the object model.
	invObj.setInventoryType(row.at(INV_SUB_TYPE));
	invObj.setArmedRaw((row.at(30) == "T"));
	invObj.setQtyRaw(atoi(row.at(31)));
	invObj.setCharge(atoi(row.at(32)));
}

// Just implement this as a static method for now.
// If we move it to any class, this would go to some
// kind of helper class
void popItemFromTuple(Row& row, CItemObj& item) {
	/*
	    New Old Field
		0	n/a is_players_inventory
		1	n/a global_id
		2	0	name
		3	1	description
		4	2	market_cost
		5	3	selling_price
		6	4	required_skill
		7	5	required_skill_level
		8	6	use_type
		9	n/a item_creator_id
		10	n/a arm_anywhere
		11	n/a disarmable
		12	15	stackable
		13	n/a destroy_when_used
		14	8	inventory_type
		15	n/a custom_texture
		16	9	base_global_id
		17	10	expired_duration
		18	11	permanent
		19	12	is_default
		20	13	visual_ref
		21	n/a web_controlled_edit
		22	14	is_derivable
		23	n/a derivation_level
		24	17	actor_group
		25	n/a search_refresh_needed
		26	7   use_value
		27	16	exclusion_groups
	*/

	item.m_globalID = atoi(row.at(1));
	//item.m_itemName				= (const char*)row.at(2);
	//item.m_itemDescription		= (const char*)row.at(3);
	item.m_marketCost = atoi(row.at(4));
	item.m_sellingPrice = atoi(row.at(5));
	item.m_requiredSkillType = atoi(row.at(6));
	item.m_requiredSkillLevel = atoi(row.at(7));
	item.m_useType = (USE_TYPE)atoi(row.at(8));
	item.m_miscUseValue = atoi(row.at(26));
	item.m_itemType = atoi(row.at(14));
	item.m_baseGlid = atoi(row.at(16));
	item.m_expiredDuration = atoi(row.at(17));
	item.m_permanent = row.at(18);
	item.m_defaultArmedItem = row.at(19);
	item.m_visualRef = atoi(row.at(20));
	item.m_derivable = row.at(22);
	item.m_stackable = row.at(12);
	std::string sExclusionGroups = (const char*)row.at(27);
	item.ParseExclusionGroupsCSV(sExclusionGroups);
	item.m_actorGroup = atoi(row.at(24));
}

typedef KGenericObjList<CInventoryObj> InventoryItemListT;
typedef KGenericObjList<CItemObj> ItemListT;
typedef std::pair<long, long> PassT;
typedef KGenericObjList<PassT> PassListT;

class PassDeleter : public PassListT::Traverser {
public:
	bool evaluate(PassT& pass) {
		delete &pass;
		return false;
	}
};

class PassTraverser : public PassListT::Traverser {
public:
	CItemObj& _item;

	PassTraverser(CItemObj& item) :
			_item(item) {}

	bool evaluate(PassT& pass) {
		if (_item.m_globalID != pass.first)
			return false;

		if (_item.m_passList == 0) {
			// :MEMTODO:
			// This memory should be released when the GlobalInvObj it is
			// bound to is deleted.  But verify that when the CItemObj is tossed
			// in the event the item is already in the GIL that this gets
			// cleaned up.
			_item.m_passList = new PassList();
		}

		_item.m_passList->addPass(pass.second);
		return false;
	}
};

void UpdateItemPassList(CItemObj& item, PassListT& PassesList) {
	if (item.m_globalID <= 0)
		return;
	PassTraverser traverser(item);
	PassesList.traverse(traverser);
}

class BaseItemSelector : public ItemListT::Selector {
public:
	long _glid;
	BaseItemSelector(long glid = 0) :
			_glid(glid) {}
	BaseItemSelector& setGLID(long glid) {
		_glid = glid;
		return *this;
	}
	bool evaluate(CItemObj& obj) {
		return (obj.m_globalID == _glid);
	}
};

class ItemDeleter : public ItemListT::Traverser {
public:
	bool evaluate(CItemObj& row) {
		delete &row; // CItemObj::SafeDelete now merged with destructor
		return false;
	}
};

#define INV_PLAYER_ID 0
#define INV_GLID 2

MySqlEventProcessor& MySqlEventProcessor::backloadInventory(
	MySQLGetResultSetsEvent& grse, InventoryItemListT& InventoryPlayer, InventoryItemListT& BankInventory) {
	// todo: Not sure how a single record with glid zero player id zero represents a non-existent upate,
	// but in any case make sure this is replicated on the other side of the fence in MySqlEventProcessor
	// If only got updates and there are no updates,
	// one record returned with 0 values. Checking two fields here.

	// TODO: Test the outcome when this set conditions occur
	StoreQueryResult& InventoryRecords = grse.results[MySQLGetResultSetsEvent::INVENTORIES_RS];
	if (InventoryRecords.num_rows() == 1) {
		Row r = InventoryRecords.at(0);
		if ((atol(r.at(INV_PLAYER_ID)) == 0) &&
			(atol(r.at(INV_GLID)) == 0))
			return *this; // getting updates, and none, so nothing to do
	}

	// First get our passes results and load it into a list
	StoreQueryResult& PassRecords = grse.results[MySQLGetResultSetsEvent::PASSES_RS];
	unsigned int numPasses = PassRecords.num_rows();
	PassListT PassesList;
	for (UINT i2 = 0; i2 < numPasses; i2++) {
		mysqlpp::Row& rec = PassRecords.at(i2);
		// This object is freed at the bottom of this method
		PassesList.append(*(new PassT(atoi(rec.at(1)), atoi(rec.at(2)))));
	}

	ItemListT BaseItemsList;

	// Move the result sets into the ItemLists
	StoreQueryResult& ItemRecords = grse.results[MySQLGetResultSetsEvent::ITEMS_RS];

	// In this block we simply get out CItemObjects loaded into two separate collections
	// representing base and derived items.  Because we are loading the base and derived
	// items, it should not be assume that the total count represents the total discrete
	// item list, as some of the base/derived pairs actually represent a single item
	//
	unsigned int numItems = ItemRecords.num_rows();
	BaseItemSelector bis;
	LogInfo("numItems=" << numItems);
	for (UINT i1 = 0; i1 < numItems; i1++) {
		// Get the mysqlpp row object.
		Row& r = ItemRecords.at(i1);

		// Create a new CItemObj
		CItemObj* newItem = new CItemObj();

		// Populate the CItemObj from the row.
		popItemFromTuple(r, *newItem);
		UpdateItemPassList(*newItem, PassesList);
		newItem->stampTime();

		// Place the item in the appropriate list
		if (atoi(r.at(0)) == 0) {
			BaseItemsList.append(*newItem);
		} else {
			CInventoryObj* newInvItem = new CInventoryObj();
			popInvObjFromTuple(r, *newInvItem);

			// We're going to take advantage of some side behavior of the query and that is that
			// all the base items appear before the owned items in the result set....
			// so we can safely "basify" our item here if need be and remove the need to
			// perform a scan of the owned item list to basify our items.
			if (newItem->m_baseGlid != 0) {
				// basify
				bis.setGLID(newItem->m_baseGlid);
				CItemObj* baseItem = BaseItemsList.select(bis);
				if (!baseItem) {
					// if there was no base item, clean up what we've already allocated, log it,
					// ...and move on.
					LogError("Unable to find base item with id [" << newItem->m_baseGlid << "]"
																  << " for item with id [" << newItem->m_globalID << "]");

					delete newItem; // CItemObj::SafeDelete now merged with destructor
					newInvItem->SafeDelete();
					delete newInvItem;
					continue;
				}
				newItem->inheritFrom(*baseItem);
			}

			newInvItem->m_itemData = newItem;

			if (r.at(INV_TYPE) == DB_PLAYER_INVENTORY)
				InventoryPlayer.append(*newInvItem);
			else if (r.at(INV_TYPE) == DB_BANK_INVENTORY)
				BankInventory.append(*newInvItem);
		}
	}

	// Before the passes list goes off the stack, we need to recover mem resources
	// allocated to it.  The underlying CObList implementation will not do it for us.
	PassDeleter().traverse(PassesList);
	ItemDeleter().traverse(BaseItemsList);

	return *this;
}

LogoffUserBackgroundEvent::~LogoffUserBackgroundEvent() {
	if (!player)
		return;

	if (player->playerOrigRef()) {
		player->playerOrigRef()->SafeDelete();
		delete player->playerOrigRef();
	}
	player->SafeDelete();
	delete player;
}

EVENT_PROC_RC MySqlEventProcessor::BroadcastEventBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	BroadcastEventBackgroundEvent* bebe = dynamic_cast<BroadcastEventBackgroundEvent*>(e);
	jsVerifyReturn(lparam != 0 && bebe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;

	if (!me->checkServerId("BroadcastEventBackgroundHandler", true))
		return EVENT_PROC_RC::NOT_PROCESSED;

	bebe->ret = PLAYER_NOT_FOUND;

	try {
		START_TIMING(me->m_timingLogger, "DB:BroadcastEventBackgroundHandler");

		KGP_Connection* m_conn = me->m_conn;

		CHECK_CONNECTION

		std::vector<ULONG> blockedKanevaIds;

		me->getCommBlockingUsers(bebe->kanevaUserId, blockedKanevaIds);

		// now remove any in To that are in blockd list
		for (auto i = blockedKanevaIds.begin(); i != blockedKanevaIds.end(); i++) {
			NETIDVect::iterator j = bebe->event->To().begin();
			auto k = bebe->destKanevaIds->begin();
			jsAssert(bebe->event->To().size() == bebe->destKanevaIds->size());

			for (; k != bebe->destKanevaIds->end(); j++, k++) {
				if (*k == *i) { // blocked
					bebe->destKanevaIds->erase(k);
					bebe->event->To().erase(j);
					break;
				}
			}
		}

		// queue event to reply dispatcher to be sent
		if (bebe->event->To().size() >= 0) {
			bebe->ret = 0;
			// only send back reply event if successful and have something to send
			me->m_replyDispatcher.QueueEvent(bebe);
		}

	} catch (KEPException* e) {
		bebe->ret = e->m_err == 0 ? E_INVALIDARG : e->m_err;
		LogError("Caught Exception - '" << e->ToString() << "'");
		e->Delete();
	}

	return EVENT_PROC_RC::CONSUMED;
}

bool MySqlEventProcessor::getCurrentZoneInfoNoThrow(NETID currentNetID, int playerId, int kanevaUserId, bool isGm, ZoneIndex zoneIndex, BYTE& permissions, unsigned& parentZoneInstanceId) {
	permissions = ZP_UNKNOWN;
	parentZoneInstanceId = 0;
	bool res = false;

	if (!checkServerId("getCurrentZoneInfoNoThrow", true)) {
		return false;
	}

	// Obtain player permission in zone as well as parent zone instance ID
	try {
		CHECK_CONNECTION

		IEvent* replyEvent = NULL;
		permissions = getCurrentZonePermission(zoneIndex, playerId, kanevaUserId, isGm, currentNetID, &replyEvent);

		// queue event to reply dispatcher to be sent
		if (replyEvent && replyEvent->To().size() >= 0) {
			// only send back reply event if successful and have something to send
			m_replyDispatcher.QueueEvent(replyEvent);
		}

		unsigned _pzi, _pzt, _pcid;
		bool rc = getParentZoneInfo(m_conn, zoneIndex.GetInstanceId(), zoneIndex.GetType(), parentZoneInstanceId, _pzi, _pzt, _pcid);
		assert(rc);
		if (!rc) {
			LogError("Error obtaining parent zone instance ID for " << zoneIndex.ToStr());
		}

		res = true;
	} catch (KEPException* e) {
		LogError("Caught Exception - zoneIndex=" << zoneIndex << " playerId=" << playerId << " netId=" << currentNetID);
		e->Delete();
	}

	LogInfo("Player permission is " << (int)permissions
									<< ", parentZoneInstanceId=" << parentZoneInstanceId
									<< ",  NETID=" << currentNetID
									<< ", playerId=" << playerId
									<< ", zoneIndex=" << zoneIndex);

	return res;
}

/*
 *Function Name:RefreshPlayerBackgroundHandler
 *
 *Description:Refreshes the player's inventory or bank inventory (storage). Event is fired from foreground
 *  		  server main thread. Get the inventory and queue up the response to the foreground.
 *  		  Note rpbe->updateFlags is only checked for a single update, not multiples.
 *
 *Returns:	EVENT_PROC_RC::CONSUMED
 */
EVENT_PROC_RC MySqlEventProcessor::RefreshPlayerBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	RefreshPlayerBackgroundEvent* rpbe = dynamic_cast<RefreshPlayerBackgroundEvent*>(e);
	jsVerifyReturn(lparam != 0 && rpbe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;

	if (me->localPlayers()) {
		if (!me->checkServerId("RefreshPlayerBackgroundHandler", true))
			return EVENT_PROC_RC::NOT_PROCESSED;

		rpbe->ret = PLAYER_NOT_FOUND;

		try {
			START_TIMING(me->m_timingLogger, "DB:RefreshPlayerBackgroundHandler");

			KGP_Connection* m_conn = me->m_conn;

			CHECK_CONNECTION

			BEGIN_TRY_SQL();

			Query query = m_conn->query();

			if (rpbe->updateFlags & PlayerMask::CASH)
				rpbe->newAmount = me->getPlayersCash(rpbe->kanevaUserId, CurrencyType::CREDITS_ON_PLAYER);

			std::string invType = "P";
			if (rpbe->updateFlags & PlayerMask::INVENTORY)
				invType = "P";
			else if (rpbe->updateFlags & PlayerMask::BANK_INVENTORY)
				invType = "B";
			else if (rpbe->updateFlags & PlayerMask::PLAYER_HOUSE_INVENTORY)
				invType = "H";

			query.reset();
			query << "CALL get_inventory( %0, %1, %2q, %3 )";
			query.parse();

			rpbe->results[0] = query.store((int)rpbe->ObjectId(), rpbe->kanevaUserId, invType.c_str(), rpbe->firstTime);
			rpbe->arraySize = 1;

			// for SPs MUST call these to avoid disconnect
			checkMultipleResultSets(query, "RefreshPlayerBackgroundHandler");
			rpbe->ret = PLAYER_FOUND;

			END_TRY_SQL();
		} catch (KEPException* e) {
			rpbe->ret = e->m_err == 0 ? E_INVALIDARG : e->m_err;
			LogError("Caught Exception - '" << e->ToString() << "'");
			e->Delete();
		}
	} else {
		rpbe->ret = PLAYER_FOUND;
		rpbe->updateFlags &= ~PlayerMask::CASH; // clear game state cash, leave inventory empty

		// Make BrowserPage call to get account information
		if (!me->populateAccountXMLFromWebCall(rpbe)) {
			LogError("populateAccountXMLFromWebCall() FAILED");
		}
	}
	me->RefreshPlayerStage1(rpbe);
	me->m_replyDispatcher.QueueEvent(rpbe);

	return EVENT_PROC_RC::CONSUMED;
}

///////////////////////////////////////////////////////////
/// handler for LogoffUserBackgroundEvent to check db
/// access then pass back to main thd
EVENT_PROC_RC MySqlEventProcessor::LogoffUserBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	LogoffUserBackgroundEvent* lue = dynamic_cast<LogoffUserBackgroundEvent*>(e);
	jsVerifyReturn(lparam != 0 && lue != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;

	if (!me->checkServerId("LogoffUserBackgroundHandler", true))
		return EVENT_PROC_RC::NOT_PROCESSED;

	lue->ret = PLAYER_NOT_FOUND;

	LogInfo("'" << lue->name << "'");

	try {
		START_TIMING(me->m_timingLogger, "DB:Logoff");

		KGP_Connection* m_conn = me->m_conn;

		CHECK_CONNECTION

		BEGIN_TRY_SQL();

		{
			BEGIN_TRANS_WITH_RETRY(*m_conn);

			if (lue->player && lue->player->playerOrigRef() && !lue->player->playerOrigRef()->m_reconnecting) {
				// get list for telling player's friend about logout
				me->getOnlineFriendList(lue->kanevaUserId, lue->broadcastListArray);
			}

			// round to nearest min
			Query query = m_conn->query();
			query << "CALL log_off_user(%0,%1,%2,%3,%4q,%5q)";
			query.parse();

			query.execute(
				(int)lue->accountNumber,
				(int)lue->kanevaUserId,
				(int)me->m_serverId,
				(int)lue->minPlayed,
				IGameState::MapDisconnectReason(lue->reason),
				lue->currentIP.c_str());
			query.reset();

			END_TRANS_WITH_RETRY();
		}

		if (lue->player != 0) {
			if (lue->player->m_inGame) {
				BEGIN_TRANS_WITH_RETRY(*m_conn);

				lue->player->m_inGame = FALSE;
				me->updatePlayerNoTrans(lue->player, PlayerMask::PLAYER | PlayerMask::SPAWN_INFO, true);

				END_TRANS_WITH_RETRY();
			}
		}

		lue->ret = NO_ERROR;

		END_TRY_SQL();
	} catch (KEPException* e) {
		lue->ret = e->m_err == 0 ? E_INVALIDARG : e->m_err;
		LogError("Caught Exception - '" << e->ToString() << "'");
		e->Delete();
	}

	me->m_replyDispatcher.QueueEvent(lue);

	return EVENT_PROC_RC::CONSUMED;
}

//Ankit ----- functions to correct spawn location if zoning to game world through "gotoplace" event
bool MySqlEventProcessor::IsZoneAGameWorld(ZoneIndex zoneIndex) {
	bool isFrameworkEnabled = false;
	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	Query frameworkEnabledQuery = m_conn->query();
	frameworkEnabledQuery << "SELECT framework_enabled FROM script_game_framework_settings WHERE zone_instance_id=%0 AND zone_type=%1;";
	frameworkEnabledQuery.parse();
	StoreQueryResult res = frameworkEnabledQuery.store((int)(zoneIndex.GetInstanceId()), (int)(zoneIndex.GetType()));
	if (res.num_rows() == 1)
		isFrameworkEnabled = res.at(0).at(0) == "T";

	END_TRY_SQL();

	return isFrameworkEnabled;
}
void MySqlEventProcessor::MakeSpawnLocationTheWorldDefaultSpawnPoint(GotoPlaceBackgroundEvent* gpe) {
	if (gpe->spawnPtIndex != SPAWN_POINT_NOT_USED)
		return;

	CPlayerObject* destPlayer = m_server->GetPlayerObjectByServerId(gpe->destKanevaUserId);
	gpe->destKanevaUserId = -1;
	if (destPlayer == NULL)
		return;

	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	Query frameworkEnabledQuery = m_conn->query();
	frameworkEnabledQuery << "SELECT position_x, position_y, position_z, rotation FROM spawn_points WHERE zone_index=%0;";
	frameworkEnabledQuery.parse();
	StoreQueryResult res = frameworkEnabledQuery.store((int)(gpe->zoneIndex.zoneIndex()));
	if (res.num_rows() >= 1) {
		gpe->x = res.at(0).at(0);
		gpe->y = res.at(0).at(1);
		gpe->z = res.at(0).at(2);
		gpe->rotation = res.at(0).at(3);
	}
	END_TRY_SQL();
}

///////////////////////////////////////////////////////////
/// handler for GotoPlaceEvents that main thd gets, preprocesses
/// then passes onto the background the for the db calls
/// also does an initial check to see if destination is a .people url
EVENT_PROC_RC MySqlEventProcessor::GotoPlaceHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	GotoPlaceBackgroundEvent* gpbe = dynamic_cast<GotoPlaceBackgroundEvent*>(e);
	jsVerifyReturn(gpbe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);
	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;

	bool desinationIsPeopleURL = STLEndWith(gpbe->url, ".people");

	gpbe->ret = PLAYER_NOT_FOUND;

	try {
		switch (gpbe->type) {
			case GotoPlaceEvent::GOTO_PLAYER:
				ret = me->GotoPlacePlayer(gpbe);
				break;

			case GotoPlaceEvent::GOTO_PLAYERS_APT:
				ret = me->GotoPlaceApartment(gpbe);
				break;

			case GotoPlaceEvent::GOTO_ZONE:
			case GotoPlaceEvent::GOTO_ZONE_COORDS:
				ret = me->GotoPlaceZone(gpbe);
				break;

			case GotoPlaceEvent::GOTO_URL:
				ret = me->GotoPlaceUrl(gpbe);
				break;
		}
	} catch (KEPException* ex) {
		gpbe->ret = BAD_URL;
		LogError("Caught Exception - '" << ex->ToString() << "'");
		ex->Delete();
	}

	if (gpbe->ret == PLAYER_FOUND && me->m_conn->neverSwitchServers() && gpbe->serverId != 0)
		gpbe->serverId = 0;

	if (gpbe->ret == PLAYER_FOUND && (gpbe->serverId == 0 || gpbe->serverId == me->m_serverNetwork->pinger().GetServerId())) {
		me->getCurrentZoneInfoNoThrow(gpbe->From(), gpbe->playerId, gpbe->kanevaUserId, gpbe->isGm, gpbe->zoneIndex, gpbe->zonePermissions, gpbe->parentZoneInstanceId);
	}

	if (desinationIsPeopleURL && me->IsZoneAGameWorld(gpbe->zoneIndex))
		me->MakeSpawnLocationTheWorldDefaultSpawnPoint(gpbe);

	me->m_replyDispatcher.QueueEvent(gpbe);

	return ret;
}

/*
 *Function Name:getPlayLists
 *
 *Parameters:	IN/the background event send from the playlist manager blade.
 *
 *Description:Get playlist for each dynamic object within the zone
 *
 *Returns:	Returned via the passed in event.
 *
 */
void MySqlEventProcessor::getPlaylists(PlaylistBackgroundEvent* plbe, int placementId, int playListID) {
	//	if (localPlayers()) {
	// WOK
	getPlaylistsFromDB(plbe, placementId, playListID);
	//	}
	//	else {
	//		// 3DAPP
	//		getPlaylistsFromWeb(plbe, placementId, playListID);
	//	}
}

// Obtain all playlist IDs for current zone through local DB query
bool MySqlEventProcessor::getZoneMediaPlayersFromDB(ZoneIndex& zi, ZoneMediaState* zoneMediaInfo) {
	try {
		BEGIN_TRY_SQL();

		Query query = m_conn->query();

		query << "CALL get_zone_playlist(%0, %1);";
		query.parse();

		StoreQueryResult res = query.store((int)zi.getClearedInstanceId(), (int)zi.GetInstanceId());

		checkMultipleResultSets(query, "get_zone_playlist");

		if (res.num_rows() > 0) {
			int placementId = 0, playlistId = 0;
			double videoRange = 0, audioRange = 0, volumePercentage = 100;

			for (auto rowIdx = 0; rowIdx < res.num_rows(); rowIdx++) {
				auto row = res.at(rowIdx);

				// Media Players used to all share the same playlist per world.  As of the Local Media Player
				// project of 8/15 for the game worlds this is no longer the case.  Every media player now
				// has it's own playlist.
				//  (*) Each media player has its independent playlist, range and volume
				//  (*) mediaPlayers map contains all media player placement IDs and independent settings
				placementId = row.at(0);
				playlistId = row.at(1); // Use playlistId #0 if singleSwfMode
				// col#2 is global Id
				videoRange = row.at(3);
				audioRange = row.at(4);
				volumePercentage = row.at(5);
				zoneMediaInfo->addPlacementMediaPlayerCfg(placementId, MediaPlayerConfig(playlistId, videoRange, audioRange, volumePercentage));
			}
		}

		END_TRY_SQL();
	} catch (KEPException* ex) {
		LogError("Caught Exception - '" << ex->ToString() << "'");
		ex->Delete();
		return false;
	}

	return true;
}

#if 0
bool MySqlEventProcessor::getPlaylistsFromWeb(PlaylistBackgroundEvent* plbe, int placementId, int playListID) {
	auto zoneMediaState = new ZoneMediaState();
	plbe->m_retVal = E_INVALIDARG;

	bool bSingle = false;
	if (placementId == 0) {
		// For entire zone: query DB for object playlist assignments first
		if (!getZoneMediaPlayersFromDB(plbe->zoneIndex, zoneMediaState)) {
			LogError("getZoneMediaPlayersFromDB failed: " << plbe->zoneIndex.ToStr());
			return false;
		}

		// No media players in the zone?
		if (zoneMediaState->getObjectPlaylistCount() == 0) {
			LogInfo("getZoneMediaPlayersFromDB returned 0 media players: " << plbe->zoneIndex.ToStr());
			delete zoneMediaState;
			return true;
		}
	}
	else {
		// We don't care about radii and volume for single object query
		zoneMediaState->setObjectPlaylistId(placementId, playListID);
		bSingle = true;
	}

	//NOTE: 3dapp only: making multiple wokweb calls for individual playlist can be very inefficient.
	//Since the fate of 3dapp is yet not very clear, I'm leaving it along at this moment.
	jsAssert(false);

	plbe->m_retVal = NO_ERROR;

	set<int> uniquePlaylistIds;
	zoneMediaState->getAllObjectPlaylistIds(uniquePlaylistIds);

	for (auto plid : uniquePlaylistIds) {
		// Load media tracks for this playlist
		plbe->m_retVal = E_INVALIDARG;

		// Check if this playlist is already loaded
		PlaylistAssets * playlistAssets = zoneMediaState->getPlaylist(plid);

		if (!playlistAssets) {
			// No yet loaded: make the call
			std::string url(m_conn->getPlaylistUrl());
			STLStringSubstitute(url, "{0}", "getPlayList");
			STLStringSubstitute(url, "{1}", numToString(plid, "%d"));
			STLStringSubstitute(url, "{2}", numToString(plbe->m_playerID, "%d"));
			STLStringSubstitute(url, "{3}", numToString(plbe->zoneIndex.getClearedInstanceId(), "%d"));
			STLStringSubstitute(url, "{4}", numToString(plbe->zoneIndex.GetInstanceId(), "%d"));

			DWORD httpStatusCode = 0;
			std::string httpStatusDescription;
			std::string resultXml;
			size_t resultSize = 0;
			if (!GetBrowserPage(url, resultXml, resultSize, httpStatusCode, httpStatusDescription)) {
				LogError("GetBrowserPage Error: " << httpStatusCode << " " << httpStatusDescription);
				break;
			}

			// Parse the result
			if (parseWokwebPlaylistResult(plid, resultXml, *zoneMediaState)) {
				playlistAssets = zoneMediaState->getPlaylist(plid);
				plbe->m_retVal = NO_ERROR;
			}
		}
		else {
			plbe->m_retVal = NO_ERROR;
		}

		// For single object request: set current track number
		jsAssert(playlistAssets!=nullptr);
		if (playlistAssets!=nullptr && bSingle) {
			// Set the initial value of the index, so that I can
			// start playing in the middle of the playlist.
			playlistAssets->setMediaIndex(plbe->m_playListItem);
		}

		//@@ Bug: plbe->ret will be carrying result from last getPlayListSpecificFromWeb() call which is
		// not very meaningful. I guess getPlayListSpecificFromWeb() isn't supposed to fail anyway.
		jsAssert(plbe->m_retVal==NO_ERROR);
	}

	plbe->m_zoneMediaState = zoneMediaState;
	return plbe->m_retVal==NO_ERROR;
}
#endif

bool MySqlEventProcessor::parseWokwebPlaylistResult(int playListID, const std::string& resultXml, ZoneMediaState& zoneMediaInfo) {
	TiXmlDocument xmlDoc;
	bool ok = false;
	try {
		xmlDoc.Parse(resultXml.c_str());
	} catch (...) {
		// a malloc is failing inside the tinyxml parsing function
		LogError("Error loading getPlayListSpecific XML - EXCEPTION thrown");
		return false;
	}
	if (!ok || xmlDoc.Error()) {
		LogError("Error loading getPlayListSpecific XML - " << xmlDoc.ErrorDesc());
		return false;
	}

	TiXmlHandle docHandle(&xmlDoc);

	TiXmlText* text = docHandle.FirstChild("Result").FirstChild("ReturnCode").FirstChild().ToText();
	TiXmlText* desc = docHandle.FirstChild("Result").FirstChild("ReturnDescription").FirstChild().ToText();

	if (text == 0 || desc == 0) {
		LogError("Result not found in playlist XML");
		return false;
	} else if (strcmp(text->Value(), "0") != 0) {
		LogError("Error getting playlist XML: " << text->Value() << " " << desc->Value());
		return false;
	}

	TiXmlElement* playListItem = docHandle.FirstChild("Result").FirstChild("Table").ToElement();
	if (!playListItem) {
		LogError("Error getting playListItem[Table]");
		return false;
	}

	auto playlistAssets = zoneMediaInfo.getPlaylistAssets(playListID);
	if (!playlistAssets) {
		// new container for all assets
		playlistAssets = new PlaylistAssets(false); // no shuffle flag from wokweb at this moment
		playlistAssets->setPlaylistId(playListID);
		zoneMediaInfo.replacePlaylistAssets(playListID, playlistAssets);
	}

	while (playListItem) {
		int assetID = 0;
		std::string assetOffsiteID;
		eMediaType assetTypeID = eMediaType::INVALID;
		eMediaSubType assetSubTypeID = eMediaSubType::NONE;
		TimeSec runTimeSeconds = (TimeSec)0.0;
		std::string thumbNailMediumPath;
		std::string mediaPath;
		std::string assetName;

		ok = fillInPlayList(
			playListItem,
			assetID,
			assetOffsiteID,
			assetTypeID,
			assetSubTypeID,
			runTimeSeconds,
			thumbNailMediumPath,
			mediaPath,
			assetName);
		if (ok) {
			// new asset
			PlaylistAsset* pl_asset = new PlaylistAsset();
			pl_asset->SetAssetID(assetID);
			pl_asset->SetOffsiteAssetID(assetOffsiteID);
			pl_asset->SetAssetTypeID(assetTypeID);
			pl_asset->SetAssetSubTypeID(assetSubTypeID);
			pl_asset->SetRuntimeSeconds(runTimeSeconds);
			pl_asset->SetThumbNailMediumPath(thumbNailMediumPath);
			pl_asset->SetMediaPath(mediaPath);
			pl_asset->SetAssetName(assetName);

			bool dupe = playlistAssets->addAssetNoDupes(pl_asset);
			if (dupe) {
				delete pl_asset;
			}
		} else {
			LogError("Playlist result parse XML error: " << resultXml.c_str());
		}

		playListItem = TiXmlHandle(playListItem->NextSibling("Table")).ToElement();
	}

	return true;
}

bool MySqlEventProcessor::getPlaylistsFromDB(PlaylistBackgroundEvent* plbe, int placementId, int playlistId) {
	if (!plbe)
		return false;

	auto zoneMediaState = new ZoneMediaState();
	plbe->m_retVal = E_INVALIDARG;

	ZoneIndex zi = plbe->zoneIndex;

	bool bSingle = false;
	if (placementId == 0) {
		LogInfo("ALL OBJECTS " << zi.ToStr());

		// For entire zone: query DB for object playlist assignments first
		if (!getZoneMediaPlayersFromDB(plbe->zoneIndex, zoneMediaState)) {
			LogError("getZoneMediaPlayersFromDB failed: " << plbe->zoneIndex.ToStr());
			delete zoneMediaState;
			return false;
		}

		// No media players in the zone?
		if (zoneMediaState->getPlacementsCount() == 0) {
			LogInfo("getZoneMediaPlayersFromDB returned 0 media players: " << plbe->zoneIndex.ToStr());
			delete zoneMediaState;
			plbe->m_retVal = NO_ERROR;
			return true;
		}

		// No playlist filter
		playlistId = 0;

	} else {
		LogInfo("objId=" << placementId << " playlistId=" << playlistId << " " << zi.ToStr());

		// We don't care about radii and volume for single object query
		zoneMediaState->setPlacementPlaylistId(placementId, playlistId);
		bSingle = true;
	}

	Timer timer;
	size_t numAssets = 0;
	PlaylistAssets* playlistAssets = nullptr;
	int currPlaylistId = 0;
	try {
		BEGIN_TRY_SQL();

		Query query = m_conn->query();

		query << "CALL get_zone_playlist_media(%0, %1, %2);"; // %2 = playlist ID filter (single media player or single playlist)
		query.parse();

		StoreQueryResult res = query.store((int)zi.getClearedInstanceId(), (int)zi.GetInstanceId(), (int)playlistId);

		checkMultipleResultSets(query, "get_zone_playlist_media");

		// DB result parser
		auto parser = [&](mysqlpp::Row* row) {
			if (row == nullptr && playlistAssets) {
				// Last chunk
				playlistAssets->init();
				zoneMediaState->replacePlaylistAssets(currPlaylistId, playlistAssets);
				playlistAssets = nullptr;
				return; // exit lambda function
			}

			int playlistId = row->at(0);
			if (currPlaylistId != playlistId) {
				if (playlistAssets) {
					playlistAssets->init();
					zoneMediaState->replacePlaylistAssets(currPlaylistId, playlistAssets);
				}
				currPlaylistId = playlistId;
				int shuffle = row->at(9); // 0 or 1
				playlistAssets = new PlaylistAssets(shuffle != 0);
				playlistAssets->setPlaylistId(playlistId);
			}

			PlaylistAsset* asset = new PlaylistAsset();
			asset->SetAssetID((int)row->at(1));
			asset->SetOffsiteAssetID((const char*)row->at(2));
			asset->SetAssetTypeID((eMediaType)(int)row->at(3));
			asset->SetAssetSubTypeID((eMediaSubType)(int)row->at(4));
			asset->SetRuntimeSeconds((TimeSec)(int)row->at(5));
			asset->SetThumbNailMediumPath((const char*)row->at(6));
			asset->SetMediaPath((const char*)row->at(7));
			asset->SetAssetName((const char*)row->at(8));

			bool dupe = playlistAssets->addAssetNoDupes(asset);
			if (!dupe)
				++numAssets;
		};

		// Retrieve all playlists
		auto numRows = res.num_rows();
		for (auto rowIdx = 0; rowIdx < numRows; rowIdx++) {
			parser(&res.at(rowIdx));

			// For single object request: set current track number
			if (playlistAssets && bSingle) {
				// Set the initial value of the index, so that I can
				// start playing in the middle of the playlist.
				playlistAssets->setMediaIndex(plbe->m_playlistMediaIndex);
			}
		}

		// Flush Last Playlist
		if (res.num_rows() > 0)
			parser(nullptr);

		END_TRY_SQL();
	} catch (KEPException* ex) {
		LogError("Caught Exception - '" << ex->ToString() << "'");
		ex->Delete();
		delete zoneMediaState;
		return false;
	}

	TimeMs timeMs = timer.ElapsedMs();
	LogInfo("OK (" << timeMs << "ms) - playlistId=" << playlistId << " playlistAssets=" << numAssets);

	plbe->m_zoneMediaState = zoneMediaState;
	plbe->m_retVal = NO_ERROR;
	return true;
}

void MySqlEventProcessor::importCommunity(SpaceImportBackgroundEvent* sibe) {
#define OP_SUCCESS 0
#define OP_INPROGRESS 1
#define OP_FAILED 2

	//THIS function has a few suspicious queries that do not seem to be correct. I wonder if it's actually being called anywhere.
	assert(false);

	EVENT_ID responseEventID = m_replyDispatcher.GetEventId("SpaceImportResponseEvent");
	if (responseEventID == BAD_EVENT_ID)
		return;

	IEvent* response = 0;

	// Set up working variables
	ZoneIndex current_zi = ZoneIndex(sibe->currentZoneIndex);
	int newZoneIndex = (int)ZoneIndex(sibe->newZoneIndex).getClearedInstanceId();
	int newZoneinstanceId = 1;

	// Set return value assuming failure
	sibe->ret = E_FAIL;

	// Send a status heartbeat update
	response = m_replyDispatcher.MakeEvent(responseEventID);
	response->AddTo(sibe->From());
#ifndef REFACTOR_INSTANCEID
	response->SetObjectId((OBJECT_ID)current_zi);
#else
	response->SetObjectId((OBJECT_ID)current_zi.toLong());
#endif

	(*response->OutBuffer()) << OP_INPROGRESS << "Step 1/5";
	m_replyDispatcher.QueueEvent(response);

	// Call Browser
	std::string url(m_conn->getSpaceImportUrl());
	std::string::size_type p;

	// validate url
	// zoneIndex  == {0}
	// instanceId == {1}
	p = url.find("{0}");
	if (p == std::string::npos) {
		LogError("getSpaceImportUrl missing {0}");
		return;
	} else {
		url = url.replace(p, 3, numToString(sibe->importZoneIndex, "%d"));
	}

	p = url.find("{1}");
	if (p == std::string::npos) {
		LogError("getSpaceImportUrl missing {1}");
		return;
	} else {
		url = url.replace(p, 3, numToString(sibe->importZoneInstanceId, "%d"));
	}

	// Url format valid and filled
	// proceed with request
	std::string resultXml;
	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;

	// Send a status heartbeat update
	response = m_replyDispatcher.MakeEvent(responseEventID);
	response->AddTo(sibe->From());
#ifndef REFACTOR_INSTANCEID
	response->SetObjectId((OBJECT_ID)current_zi);
#else
	response->SetObjectId((OBJECT_ID)current_zi.toLong());
#endif

	(*response->OutBuffer()) << OP_INPROGRESS << "Step 2/5";
	m_replyDispatcher.QueueEvent(response);

	Timer stepTimer;
	size_t resultSize = 0;
	if (!GetBrowserPage(url, resultXml, resultSize, httpStatusCode, httpStatusDescription)) {
		LogError("Error occurred requesting URL[" << url << "] httpStatusCode[" << httpStatusCode << "] httpStatusDescription[" << httpStatusDescription << "]");
		return;
	}

	stepTimer.Pause();
	stepTimer.Reset();

	// Send a status heartbeat update
	response = m_replyDispatcher.MakeEvent(responseEventID);
	response->AddTo(sibe->From());
#ifndef REFACTOR_INSTANCEID
	response->SetObjectId((OBJECT_ID)current_zi);
#else
	response->SetObjectId((OBJECT_ID)current_zi.toLong());
#endif

	(*response->OutBuffer()) << OP_INPROGRESS << "Step 3/5";
	m_replyDispatcher.QueueEvent(response);

	TiXmlDocument xmlDoc;

	try {
		stepTimer.Start();
		xmlDoc.Parse(resultXml.c_str());
		stepTimer.Pause();
		stepTimer.Reset();
	} catch (...) {
		LogError("Caught Exception");
		return;
	}

	if (xmlDoc.Error()) {
		LogError("GetBrowserPage[" << url << "] response parsing error: " << xmlDoc.ErrorDesc());
		LogError("resultXML[" << resultXml << "]");
		return;
	}

	// Send a status heartbeat update
	response = m_replyDispatcher.MakeEvent(responseEventID);
	response->AddTo(sibe->From());
#ifndef REFACTOR_INSTANCEID
	response->SetObjectId((OBJECT_ID)current_zi);
#else
	response->SetObjectId((OBJECT_ID)current_zi.toLong());
#endif
	(*response->OutBuffer()) << OP_INPROGRESS << "Step 4/5";
	m_replyDispatcher.QueueEvent(response);

	stepTimer.Start();
	TiXmlHandle docHandle(&xmlDoc);

	TiXmlText* text = docHandle.FirstChild("Result").FirstChild("ReturnCode").FirstChild().ToText();
	TiXmlText* desc = docHandle.FirstChild("Result").FirstChild("ReturnDescription").FirstChild().ToText();

	if (text == 0 || desc == 0) {
		LogError("unrecognized response format from GetBrowserPage[" << url << "]");
		LogError("resultXML[" << resultXml << "]");
		return;
	} else if (strcmp(text->Value(), "0") != 0) {
		LogWarn("GetBrowserPage[" << url << "] response result error: " << text->Value() << " " << desc->Value());
		LogError("resultXML[" << resultXml << "]");
		return;
	}

	// PARSE XML
	// may optionally contain items and/or world object settings. Either may or may not exist.
	std::vector<dynamicObject*> dynamicObjects;
	std::vector<worldObjectPlayerSetting*> worldObjects;
	try {
		// extract all "item" in XML if any
		TiXmlElement* entry = docHandle.FirstChild("Result").FirstChild("item").ToElement();
		while (entry) {
			dynamicObject* pDynamicObject = new dynamicObject;
			if (fillInDynamicObject(entry, pDynamicObject)) {
				// Rekey the object
				pDynamicObject->zoneIndex = newZoneIndex;
				pDynamicObject->zoneInstanceID = newZoneinstanceId;

				// check and cache all parameters
				TiXmlElement* parameter = entry->FirstChildElement("parameter");
				if (parameter)
					fillInDynamicObjectParameters(parameter, pDynamicObject);

				// add new object to cache
				dynamicObjects.push_back(pDynamicObject);

				entry = TiXmlHandle(entry->NextSibling("item")).ToElement();
			} else {
				delete pDynamicObject;
				LogError("error parsing item XML");
				for (auto doItr = dynamicObjects.begin(); doItr != dynamicObjects.end(); doItr++)
					delete *doItr;
				return;
			}
		}

		// extract all "WorldObjectSetting" in XML if any
		entry = docHandle.FirstChild("Result").FirstChild("WorldObjectSetting").ToElement();
		while (entry) {
			worldObjectPlayerSetting* pWorldObjectPlayerSetting = new worldObjectPlayerSetting;
			if (fillInWorldObject(entry, pWorldObjectPlayerSetting)) {
				// Rekey the object
				pWorldObjectPlayerSetting->zoneIndex = newZoneIndex;
				pWorldObjectPlayerSetting->zoneInstanceID = newZoneinstanceId;

				// add new object to cache
				worldObjects.push_back(pWorldObjectPlayerSetting);

				entry = TiXmlHandle(entry->NextSibling("WorldObjectSetting")).ToElement();
			} else {
				delete pWorldObjectPlayerSetting;
				LogError("error parsing WorldObjectSetting XML");
				for (auto woItr = worldObjects.begin(); woItr != worldObjects.end(); woItr++)
					delete *woItr;
				return;
			}
		}
	} catch (...) {
		LogError("Caught Exception");
		for (auto doItr = dynamicObjects.begin(); doItr != dynamicObjects.end(); doItr++)
			delete *doItr;

		for (auto woItr = worldObjects.begin(); woItr != worldObjects.end(); woItr++)
			delete *woItr;
		return;
	}

	// Send a status heartbeat update
	response = m_replyDispatcher.MakeEvent(responseEventID);
	response->AddTo(sibe->From());
#ifndef REFACTOR_INSTANCEID
	response->SetObjectId((OBJECT_ID)current_zi);
#else
	response->SetObjectId((OBJECT_ID)current_zi.toLong());
#endif
	(*response->OutBuffer()) << OP_INPROGRESS << "Step 5/5";
	m_replyDispatcher.QueueEvent(response);

	// BEGIN SQL insertion
	try {
		BEGIN_TRY_SQL();

		Transaction trans(*m_conn);
		Query query = m_conn->query();

		if (sibe->maintainScripts == true) {
			// only delete non scripted objects from the newzone
			query.reset();
			query << " DELETE FROM dynamic_objects ";
			query << " WHERE (dynamic_objects.zone_index = " << newZoneIndex << ") ";
			query << " AND 0 = (select count(*) from dynamic_object_parameters where dynamic_objects.obj_placement_id = dynamic_object_parameters.obj_placement_id and dynamic_object_parameters.param_type_id = 29) ";
			query.execute();
			// _SCR_ what about dynamic_object_parameters that are not 29?  those should be deleted from dynamic_object_parameters as well otherwise they are orpahnded
		} else {
			// clear out the newzone (delete all dynamic objects inside the zone we are importing into)
			query.reset();
#ifndef REFACTOR_INSTANCEID
			query << " DELETE FROM dynamic_object_parameters WHERE obj_placement_id IN (SELECT obj_placement_id FROM dynamic_objects WHERE zone_index = " << (int)current_zi << ")";
#else
			query << " DELETE FROM dynamic_object_parameters WHERE obj_placement_id IN (SELECT obj_placement_id FROM dynamic_objects WHERE zone_index = " << (int)current_zi.toLong() << ")";
#endif
			query.execute();

			query.reset();
			query << " DELETE FROM dynamic_objects WHERE zone_index = " << newZoneIndex;
			query.execute();
		}

		sqlBulkInsertDynamicObjects(dynamicObjects);
		sqlBulkInsertDynamicObjectsParameters(dynamicObjects);
		sqlBulkInsertDynamicWorldObjects(worldObjects);

		trans.commit();

		stepTimer.Reset();

		sibe->ret = NO_ERROR;

		// Send a status heartbeat update (completion)
		response = m_replyDispatcher.MakeEvent(responseEventID);
		response->AddTo(sibe->From());
#ifndef REFACTOR_INSTANCEID
		response->SetObjectId((OBJECT_ID)current_zi);
#else
		response->SetObjectId((OBJECT_ID)current_zi.toLong());
#endif
		(*response->OutBuffer()) << OP_SUCCESS << "ImportCommunity completed successfully";
		m_replyDispatcher.QueueEvent(response);

		END_TRY_SQL();
	} catch (KEPException* ex) {
		LogWarn("Caught Exception - '" << ex->ToString() << "'");
		delete ex;
	}

	// clean up memory
	for (auto doItr = dynamicObjects.begin(); doItr != dynamicObjects.end(); doItr++)
		delete *doItr;

	for (auto woItr = worldObjects.begin(); woItr != worldObjects.end(); woItr++)
		delete *woItr;
}

EVENT_PROC_RC MySqlEventProcessor::SpaceImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	SpaceImportBackgroundEvent* sibe = dynamic_cast<SpaceImportBackgroundEvent*>(e);
	jsVerifyReturn(sibe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);
	EVENT_PROC_RC retval = EVENT_PROC_RC::NOT_PROCESSED;

	START_TIMING(me->m_timingLogger, "DB:SpaceImportHandler");

	sibe->ret = E_FAIL;
	try {
		me->importCommunity(sibe);
		retval = EVENT_PROC_RC::OK;
	} catch (KEPException* ex) {
		sibe->ret = E_FAIL;
		LogError("Caught Exception - zoneIndex=" << sibe->newZoneIndex << " zoneIndexImport=" << sibe->importZoneIndex << " " << ex->ToString());
		ex->Delete();
	}

	if (sibe->ret != NO_ERROR) {
		EVENT_ID responseEventID = me->m_replyDispatcher.GetEventId("SpaceImportResponseEvent");
		if (responseEventID == BAD_EVENT_ID) {
			jsAssert(false);
		}
		IEvent* response = me->m_replyDispatcher.MakeEvent(responseEventID);
		response->AddTo(sibe->From());
		response->SetObjectId((OBJECT_ID)sibe->newZoneIndex);
		(*response->OutBuffer()) << 2 << "Importing encountered an issue.   Please try again.";
		me->m_replyDispatcher.QueueEvent(response);
	}
	return retval;
}

void MySqlEventProcessor::importChannel(ChannelImportBackgroundEvent* cibe) {
#define OP_SUCCESS 0
#define OP_INPROGRESS 1
#define OP_FAILED 2

	EVENT_ID responseEventID = m_replyDispatcher.GetEventId("ChannelImportResponseEvent");
	if (responseEventID == BAD_EVENT_ID)
		return;

	LogInfo("playerId[" << cibe->player_id << "] "
						<< "dstZoneIndex[" << cibe->dstZoneIndex << "] "
						<< "dstInstanceId[" << cibe->dstInstanceId << "] "
						<< "baseZoneIndexPlain[" << cibe->baseZoneIndexPlain << "]"
						<< "srcZoneIndex[" << cibe->srcZoneIndex << "] "
						<< "srcInstanceId[" << cibe->srcInstanceId << "] "
						<< "maintainScripts[" << cibe->maintainScripts << "]");

	// Send a status heartbeat update
	int newZoneIndex(0);
	IEvent* response = m_replyDispatcher.MakeEvent(responseEventID);
	response->AddTo(cibe->From());
	response->SetObjectId((OBJECT_ID)cibe->dstZoneIndex);
	(*response->OutBuffer()) << OP_INPROGRESS << "Step 1/2" << newZoneIndex;
	m_replyDispatcher.QueueEvent(response);

	cibe->ret = E_FAIL;
	try {
		BEGIN_TRY_SQL();

		// wok.import_channel(__playerId, __dstZoneIndex, __dstZoneInstanceId, __baseZoneIndexPlain __srcZoneIndex, __srcZoneInstanceId, __maintainScripts __retval, __newZoneIndex)
		//    Validations & Return codes
		//    retval
		//      = -1 (failure) --
		//      =  0 (success) --
		//      =  2 (invalid destination zone type) -- this procedure only handles channel_zones of type 3(home 6(channel)
		//      =  3 (user does not own destination) --
		//      =  4 (could not find destination in database) --
		//      =  5 (rekey zone index failed) --
		// BW - 09/23/16: Removed wrapping transaction to alleviate locking issues.
		Query query = m_conn->query();
		query << "CALL wok.import_channel(%0, %1, %2, %3, %4, %5, %6, @rc, @newZoneIndex); select @rc, @newZoneIndex;";
		query.parse();

		StoreQueryResult res = query.store(
			cibe->player_id,
			(int)cibe->dstZoneIndex,
			(int)cibe->dstInstanceId,
			(int)cibe->baseZoneIndexPlain,
			(int)cibe->srcZoneIndex,
			(int)cibe->srcInstanceId,
			(int)cibe->maintainScripts);

		res = query.store_next(); // results for the select

		if (res.num_rows() == 1) {
			cibe->ret = (int)res.at(0).at(0);
			newZoneIndex = res.at(0).at(1);
			if (cibe->ret != 0)
				throw new KEPException("import_channel failed", cibe->ret);

			// clear cache
			std::string key("wok.zz.");
			key += numToString(newZoneIndex, "%d") + numToString(cibe->dstInstanceId, ",%d");
			dropCompoundCacheItem(key);

			// If we imported into a home we must update the runtime player's housing index
			ZoneIndex zi((long)newZoneIndex);
			if (zi.isHousing()) {
				if (cibe->player && cibe->player->m_housingZoneIndex.getClearedInstanceId() != zi.getClearedInstanceId())
					cibe->player->m_housingZoneIndex = (long)newZoneIndex;
			}
			cibe->ret = NO_ERROR;
		}

		// Notify script server that we want the scripts in previous zone to shutdown when last player departs
		ZoneIndex dstZoneIndex(cibe->dstZoneIndex); // Zone index plain and type (w/o instance ID)
		dstZoneIndex.setInstanceId(cibe->dstInstanceId); // Now set instance ID
		m_replyDispatcher.QueueEvent(new UpdateScriptZoneSpinDownDelayEvent(dstZoneIndex, 0, 0));

		// Notify server engine that the zone has changed
		m_replyDispatcher.QueueEvent(new ZoneImportedEvent(dstZoneIndex, cibe->playerNetId));

		// All good it worked
		cibe->ret = NO_ERROR;
		response = m_replyDispatcher.MakeEvent(responseEventID);
		response->AddTo(cibe->From());
		response->SetObjectId((OBJECT_ID)cibe->dstZoneIndex);
		(*response->OutBuffer()) << OP_SUCCESS << "ImportChannel completed successfully" << newZoneIndex;
		m_replyDispatcher.QueueEvent(response);

		END_TRY_SQL();
	} catch (KEPException* e) {
		cibe->ret = e->m_err == 0 ? E_INVALIDARG : e->m_err;
		LogError("Caught Exception - '" << e->ToString() << "'");
		e->Delete();
	}
}

EVENT_PROC_RC MySqlEventProcessor::ChannelImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ChannelImportBackgroundEvent* cibe = dynamic_cast<ChannelImportBackgroundEvent*>(e);
	jsVerifyReturn(cibe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);
	EVENT_PROC_RC retval = EVENT_PROC_RC::NOT_PROCESSED;

	START_TIMING(me->m_timingLogger, "DB:ChannelImportHandler");

	cibe->ret = E_FAIL;
	try {
		me->importChannel(cibe);
		retval = EVENT_PROC_RC::OK;
	} catch (KEPException* ex) {
		cibe->ret = E_FAIL;
		LogError("Caught Exception - playerId=" << cibe->player_id << " '" << ex->ToString() << "'");
		ex->Delete();
	}

	if (cibe->ret != NO_ERROR) {
		EVENT_ID responseEventID = me->m_replyDispatcher.GetEventId("ChannelImportResponseEvent");
		IEvent* response = me->m_replyDispatcher.MakeEvent(responseEventID);
		response->AddTo(cibe->From());
		response->SetObjectId((OBJECT_ID)cibe->dstZoneIndex);
		(*response->OutBuffer()) << 2 << "Channel Importing encountered an issue.   Please try again." << -1;
		me->m_replyDispatcher.QueueEvent(response);
	}

	return retval;
}

EVENT_PROC_RC MySqlEventProcessor::ScriptAvailableItemsBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ScriptAvailableItemsBackgroundEvent* saibe = dynamic_cast<ScriptAvailableItemsBackgroundEvent*>(e);
	jsVerifyReturn(saibe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);
	me->loadScriptServerAvailableItems();
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC MySqlEventProcessor::LoadItemsBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	LoadItemsBackgroundEvent* libe = dynamic_cast<LoadItemsBackgroundEvent*>(e);
	jsVerifyReturn(libe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	std::set<int> glidsToAdd;
	libe->getGlids(glidsToAdd);
	me->addCustomItems(glidsToAdd);
	if (libe->getResponse())
		me->m_replyDispatcher.QueueEvent(libe->getResponse());
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC MySqlEventProcessor::UpdateScriptZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	UpdateScriptZoneBackgroundEvent* uszbe = dynamic_cast<UpdateScriptZoneBackgroundEvent*>(e);
	jsVerifyReturn(uszbe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlEventProcessor* me = (MySqlEventProcessor*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	if (me->m_serverId == 0) {
		LogError("Cannot update because server ID is UNKNOWN");
		return EVENT_PROC_RC::CONSUMED;
	}

	if (uszbe->getZoneType() == CI_PERMANENT || uszbe->getZoneType() == CI_ARENA) {
		// script_zones table is not compatible with permanent zones which requires zone_index_plain as part of the primary key
		// Arena zones (if it's still used) should not be tied up with a server due to instancing.
		return EVENT_PROC_RC::CONSUMED;
	}

	if (uszbe->isSpinUp()) {
		if (me->getServerVisibilityId() == ServerVisibility::Public) {
			// Update script_zones on spin-up if server is public
			me->updateScriptZoneSpinUp(uszbe->getZoneInstanceId(), uszbe->getZoneType(), uszbe->getSpinDownDelayMinutes());
		} else {
			LogWarn("script_zones table not updated because server is *NOT* public");
		}
	} else {
		// Always update script_zones on spin-down to ensure proper cleanup if visibility changes during the session
		// updateScriptZoneSpinDown will only cleanup if existing record's server ID matches this server.
		me->updateScriptZoneSpinDown(uszbe->getZoneInstanceId(), uszbe->getZoneType());
	}

	return EVENT_PROC_RC::CONSUMED;
}

void MySqlEventProcessor::loadScriptServerAvailableItems() {
	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "SELECT global_id FROM script_available_items_plain";
	StoreQueryResult res = query.store();

	if (res.num_rows() != 0) {
		int itemId;
		std::vector<int> itemIds;
		itemIds.reserve(res.num_rows());
		for (unsigned int i = 0; i < res.num_rows(); i++) {
			Row row = res.at(i);
			itemId = row.at(0);
			itemIds.push_back(itemId);
		}

		LogInfo("loadScriptServerAvailableItems loaded[" << res.num_rows() << "] from database");
		m_server->addItemsToAvailableScriptItems(itemIds);

		std::set<int> missingItemIds;
		for (UINT i = 0; i < res.num_rows(); i++) {
			Row row = res.at(i);
			itemId = row.at(0);

			CGlobalInventoryObjList* globalDBRef = CGlobalInventoryObjList::GetInstance();
			if (globalDBRef) {
				CGlobalInventoryObj* globalInventoryObj = (CGlobalInventoryObj*)globalDBRef->GetByGLID(itemId);
				if (globalInventoryObj == 0) {
					// Item not in our global inventory, i.e. a custom item. Add it to global inventory.
					missingItemIds.insert(itemId);
				}
			}
		}

		if (missingItemIds.size() > 0) {
			LogInfo("loadScriptServerAvailableItems found[" << missingItemIds.size() << "] missing glids");
			addCustomItems(missingItemIds);
		}
	}

	END_TRY_SQL_LOG_ONLY(m_conn);
}

void MySqlEventProcessor::updateScriptZoneSpinUp(unsigned zoneInstanceId, unsigned zoneType, unsigned spinDownDelayMins) {
	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "INSERT INTO `script_zones`(`zone_instance_id`, `zone_type`, `server_id`, `spin_down_delay_minutes`) "
		  << "VALUES (%0, %1, %2, %3) "
		  << "ON DUPLICATE KEY UPDATE `server_id`=%2";
	query.parse();

	LogDebug(query.str(zoneInstanceId, zoneType, m_serverId, spinDownDelayMins));

	query.execute(zoneInstanceId, zoneType, m_serverId, spinDownDelayMins);

	END_TRY_SQL_LOG_ONLY(m_conn);
}

void MySqlEventProcessor::updateScriptZoneSpinDown(unsigned zoneInstanceId, unsigned zoneType) {
	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "UPDATE `script_zones` SET `server_id` = 0 WHERE `zone_instance_id` = %0 AND `zone_type` = %1 AND `server_id`=%2";
	query.parse();

	LogDebug(query.str(zoneInstanceId, zoneType, m_serverId));

	// Update record only if server ID matches
	query.execute(zoneInstanceId, zoneType, m_serverId);

	END_TRY_SQL_LOG_ONLY(m_conn);
}

ServerVisibility MySqlEventProcessor::getServerVisibilityId() {
	if (m_serverVisibilityId > ServerVisibility::Unknown && fTime::ElapsedMs(m_serverVisibilityIdUpdateTime) <= 180000) {
		// Cache last query for up to 3 minute
		// NOTE that a server instance can create multiple instances of MySqlEventProcessor, each will query and cache the value separately.
		return m_serverVisibilityId;
	}

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "SELECT visibility_id FROM developer.game_servers WHERE server_id=%0;";
	query.parse();

	auto rs = query.store(m_serverId);

	if (rs.num_rows() != 1) {
		LogError("Error getting server visibility by server ID - " << query.str(m_serverId));
		return m_serverVisibilityId;
	} else {
		int visibilityId = rs.at(0).at(0);
		LogInfo("Current server visibility is " << visibilityId);
		m_serverVisibilityId = static_cast<ServerVisibility>(visibilityId);
		m_serverVisibilityIdUpdateTime = fTime::TimeMs(); // If query failed with exception, timestamp will not be updated.
	}

	END_TRY_SQL_LOG_ONLY(m_conn);

	return m_serverVisibilityId;
}

EVENT_PROC_RC MySqlEventProcessor::PlaylistBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (MySqlEventProcessor*)lparam;
	auto plbe = dynamic_cast<PlaylistBackgroundEvent*>(e);
	if (!me || !plbe)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LogInfo(plbe->ToStr());

	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;
	std::string errMsg;
	try {
		switch (plbe->m_eType) {
			case PlaylistBackgroundEvent::PL_GET_PLAYLIST_ZONE:
				me->getPlaylists(plbe);
				break;

			case PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE_NEXT_TRACK:
			case PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE:
				me->getPlaylists(plbe, plbe->m_objectPlacementID, plbe->m_playlistId);
				break;
		};

		ret = EVENT_PROC_RC::OK;
	} catch (KEPException* ex) {
		StrAppend(errMsg, " " << ex->m_msg);
		LogError("Caught Exception - " << errMsg);
		if (plbe->m_retVal == 0)
			plbe->m_retVal = E_INVALIDARG; // make sure errors out
		ex->Delete();
	}

	if (plbe->m_retVal != 0) {
		StrAppend(errMsg, " ret=" << plbe->m_retVal);
		if (plbe->m_pEventBroadcast) {
			plbe->m_pEventBroadcast->Delete();
			plbe->m_pEventBroadcast = nullptr;
		}
		LogError(errMsg);

		if (!plbe->m_pEventReply) { // assume if reply event it has error message for client
			plbe->m_pEventReply = new RenderTextEvent(errMsg.c_str(), eChatType::System);
			plbe->m_pEventReply->AddTo(plbe->From());
		}
	}

	me->m_replyDispatcher.QueueEvent(plbe);

	return ret;
}

EVENT_PROC_RC MySqlEventProcessor::GotoPlacePlayer(GotoPlaceBackgroundEvent* gpe) {
	START_TIMING(m_timingLogger, "DB:GotoPlacePlayer");
	jsVerifyReturn(m_serverNetwork != 0, EVENT_PROC_RC::NOT_PROCESSED);
	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "CALL can_spawn_to_player( %0, %1, %2, %3, %4q, %5q, %6q, %7, %8q, @rc, @zi, @ii, @x, @y, @z, @sid, @ssid, @cc, @zowner, @url ); "
			 "SELECT @rc, @ii, @sid, @ssid, @cc, @zowner, @url, @zi, @x, @y, @z;";
	query.parse();

	query.store(
		(int)gpe->playerId,
		(int)gpe->kanevaUserId,
		(int)gpe->destKanevaUserId,
		(int)m_serverNetwork->pinger().GetServerId(),
		(gpe->isGm ? DB_TRUE : DB_FALSE),
		gpe->country,
		gpe->birthDate,
		(int)(gpe->showMature ? 1 : 0),
		pingerGameName());

	StoreQueryResult res = query.store_next();
	jsVerifyReturn(res.size() > 0, EVENT_PROC_RC::NOT_PROCESSED);

	if (res.size() > 0) {
		Row r = res.at(0);
		gpe->ret = r.at(GOTO_RC_COL);
		if (gpe->ret == PLAYER_FOUND) {
			gpe->spawnPtIndex = SPAWN_POINT_NOT_USED;
			gpe->serverId = r.at(GOTO_SERVER_ID_COL);
			gpe->scriptServerId = r.at(GOTO_SCRIPT_SERVER_ID_COL);
			gpe->zoneIndex = ZoneIndex((LONG)r.at(GOTO_ZONEINDEX_COL));
			gpe->zoneIndex.setInstanceId(r.at(GOTO_INSTANCE_COL));
			gpe->x = r.at(GOTO_X_COL);
			gpe->y = r.at(GOTO_Y_COL);
			gpe->z = r.at(GOTO_Z_COL);
			gpe->url = r.at(GOTO_URL_COL);

			// no cover charge for GMs
			if (!gpe->isGm) {
				gpe->coverCharge = r.at(GOTO_COVER_COL);
				gpe->zoneOwnerId = r.at(GOTO_OWNER_COL);
			}
		}
	}
	END_TRY_SQL();

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC MySqlEventProcessor::GotoPlaceApartment(GotoPlaceBackgroundEvent* gpe) {
	START_TIMING(m_timingLogger, "DB:GotoPlaceApartment");
	jsVerifyReturn(m_serverNetwork != 0, EVENT_PROC_RC::NOT_PROCESSED);

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "CALL can_spawn_to_apartment( %0, %1, %2, %3, %4q, %5q, %6q, %7, %8q, @rc, @hi, @sid, @ssid, @pid, @cc, @zowner, @url );"
			 "SELECT @rc, @hi, @sid, @ssid, @cc, @zowner, @url, @pid;";
	query.parse();
	query.store(
		(int)gpe->playerId,
		(int)gpe->kanevaUserId,
		(int)gpe->destKanevaUserId,
		(int)m_serverNetwork->pinger().GetServerId(),
		(gpe->isGm ? DB_TRUE : DB_FALSE),
		gpe->country,
		gpe->birthDate,
		(int)(gpe->showMature ? 1 : 0),
		pingerGameName());
	StoreQueryResult res = query.store_next(); // get select output
	jsVerifyReturn(res.size() > 0, EVENT_PROC_RC::NOT_PROCESSED);

	if (res.size() > 0) {
		Row r = res.at(0);
		gpe->ret = r.at(GOTO_RC_COL);

		if (gpe->ret == PLAYER_FOUND) {
			gpe->spawnPtIndex = DEFAULT_SPAWN_POINT;
			gpe->zoneIndex = ZoneIndex((LONG)r.at(GOTO_HOUSING_INDEX));
			gpe->zoneIndex.setInstanceId(r.at(GOTO_PLAYERID_COL));
			gpe->serverId = r.at(GOTO_SERVER_ID_COL);
			gpe->scriptServerId = r.at(GOTO_SCRIPT_SERVER_ID_COL);
			gpe->url = r.at(GOTO_URL_COL);

			// no cover charge for GMs
			if (!gpe->isGm) {
				gpe->coverCharge = r.at(GOTO_COVER_COL);
				gpe->zoneOwnerId = r.at(GOTO_OWNER_COL);
			}
		}
	}

	END_TRY_SQL();

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC MySqlEventProcessor::GotoPlaceZone(GotoPlaceBackgroundEvent* gpe) {
	START_TIMING(m_timingLogger, "DB:GotoPlaceZone");
	jsVerifyReturn(m_serverNetwork != 0, EVENT_PROC_RC::NOT_PROCESSED);

	// call the SP to see if this player can spawn to the zone
	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "CALL can_spawn_to_zone( %0, %1, %2, %3, %4, %5, %6q, %7q, %8q, %9, %10, %11q, @rc, @ii, @sid, @ssid, @cc, @zowner, @url ); "
			 "SELECT @rc, @ii, @sid, @ssid, @cc, @zowner, @url;";
	query.parse();
	query.store(
		(int)gpe->playerId,
		(int)gpe->kanevaUserId,
		(int)gpe->zoneIndex.GetType(),
		(int)gpe->zoneIndex.getClearedInstanceId(),
		(int)gpe->zoneIndex.GetInstanceId(),
		(int)m_serverNetwork->pinger().GetServerId(),
		(gpe->isGm ? DB_TRUE : DB_FALSE),
		gpe->country,
		gpe->birthDate,
		(int)(gpe->showMature ? 1 : 0),
		gpe->goingToPlayer,
		pingerGameName());

	StoreQueryResult res = query.store_next(); // get select output
	jsVerifyReturn(res.size() > 0, EVENT_PROC_RC::NOT_PROCESSED);

	if (res.size() > 0) {
		// if server empty string, then local
		Row r = res.at(0);
		gpe->ret = r.at(GOTO_RC_COL);
		if (gpe->ret == PLAYER_FOUND) {
			gpe->zoneIndex.setInstanceId(r.at(GOTO_INSTANCE_COL));
			gpe->serverId = r.at(GOTO_SERVER_ID_COL);
			gpe->scriptServerId = r.at(GOTO_SCRIPT_SERVER_ID_COL);
			gpe->url = r.at(GOTO_URL_COL);

			// no cover charge for GMs
			if (!gpe->isGm) {
				gpe->coverCharge = r.at(GOTO_COVER_COL);
				gpe->zoneOwnerId = r.at(GOTO_OWNER_COL);
			}
		}
	}
	END_TRY_SQL();

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC MySqlEventProcessor::GotoPlaceUrl(GotoPlaceBackgroundEvent* gpe) {
	START_TIMING(m_timingLogger, "DB:GotoPlaceUrl");
	jsVerifyReturn(m_serverNetwork != 0, EVENT_PROC_RC::NOT_PROCESSED);

	// setup output params on event that we return
	gpe->spawnPtIndex = SPAWN_POINT_NOT_USED;
	gpe->x = gpe->y = gpe->z = 0.0;
	gpe->rotation = 0;

	// call the SP to see if this player can spawn to the zone
	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "CALL can_spawn_to_url( %0, %1, %2q, %3, %4q, %5q, %6q, %7, %8q, %9, @rc, @zi, @ii, @x, @y, @z, @r, @spi, @sid, @ssid, @cc, @zowner, @url, @destKanevaId ); "
			 "SELECT @rc, @ii, @sid, IFNULL(@ssid, 0), @cc, @zowner, @url, @zi, @x, @y, @z, @r, @spi, @destKanevaId;";
	query.parse();

	// db has unescaped version
	std::string cleanUrl(StringHelpers::trim(gpe->url));
	cleanUrl = UrlParser::unescapeString(cleanUrl);

	query.store(
		(int)gpe->playerId,
		(int)gpe->kanevaUserId,
		cleanUrl.c_str(),
		(int)gpe->serverId,
		(gpe->isGm ? DB_TRUE : DB_FALSE),
		gpe->country,
		gpe->birthDate,
		(int)(gpe->showMature ? 1 : 0),
		pingerGameName(),
		(int)m_serverNetwork->pinger().GetGameId());
	StoreQueryResult res = query.store_next();

	if (res.size() > 0) {
		Row r = res.at(0);

		// if server empty string, then local
		gpe->ret = r.at(GOTO_RC_COL);
		if (gpe->ret == PLAYER_FOUND) {
			gpe->serverId = r.at(GOTO_SERVER_ID_COL);
			gpe->scriptServerId = r.at(GOTO_SCRIPT_SERVER_ID_COL);
			gpe->x = r.at(GOTO_X_COL);
			gpe->y = r.at(GOTO_Y_COL);
			gpe->z = r.at(GOTO_Z_COL);
			gpe->spawnPtIndex = r.at(GOTO_SPAWNPTINDEX_COL);
			gpe->rotation = r.at(GOTO_ROTATE_COL);
			gpe->zoneIndex = ZoneIndex((LONG)r.at(GOTO_ZONEINDEX_COL));
			gpe->zoneIndex.setInstanceId(r.at(GOTO_INSTANCE_COL));
			gpe->url = r.at(GOTO_URL_COL);
			gpe->destKanevaUserId = r.at(GOTO_DEST_KANEVA_ID_COL);

			// no cover charge for GMs
			if (!gpe->isGm) {
				gpe->coverCharge = r.at(GOTO_COVER_COL);
				gpe->zoneOwnerId = r.at(GOTO_OWNER_COL);
			}
		}
	}
	END_TRY_SQL();

	return EVENT_PROC_RC::CONSUMED;
}

BYTE MySqlEventProcessor::getPermZonePriv(LONG zoneIndex, ULONG serverId) {
	BYTE ret = ZP_NONE;
	const ULONG UPDATE_FREQ = 60000;

	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_lastPermZonePrivUpdate) > UPDATE_FREQ) {
		m_lastPermZonePrivUpdate = timeMs;

		BEGIN_TRY_SQL();

		Query query = m_conn->query();
		query << "SELECT zone_index, kaneva_user_id, role "
				 " FROM permanent_zone_roles"
				 " ORDER BY zone_index";
		StoreQueryResult res = query.store();

		m_permZonePrivs.clear();
		for (UINT i = 0; i < res.num_rows(); i++) {
			const Row& r = res.at(i);
			m_permZonePrivs[(LONG)r.at(0)].push_back(std::pair<ULONG, BYTE>(r.at(1), r.at(2)));
		}

		END_TRY_SQL();
	}

	UserPrivMap::iterator i = m_permZonePrivs.find(zoneIndex);
	if (i != m_permZonePrivs.end()) {
		for (UserPriv::iterator j = i->second.begin(); j != i->second.end(); j++) {
			if (j->first == serverId) {
				ret = j->second;
				break;
			}
		}
	}
	return ret;
}

// can the player add/remove dynamic objects in the zone
BYTE MySqlEventProcessor::getCurrentZonePermission(ZoneIndex& zi, int playerId, int kanevaUserId, bool isGm, NETID from, IEvent** replyEvent) {
	BYTE currentZonePriv = ZP_NONE;

	// channel moderators can change
	switch (zi.GetType()) {
		case CI_CHANNEL: {
			CHECK_CONNECTION

			BEGIN_TRY_SQL_FOR("getCurrentZonePermission/Channel");

			Query query = m_conn->query();
			query << "CALL get_community_account_type( %0, %1 )";
			query.parse();

			{
				UseQueryResult res = query.use(kanevaUserId, (int)zi.GetInstanceId());
				if (Row row = res.fetch_row()) {
					int type = row.at(0);
					currentZonePriv = type;
				}
			}

			// for SPs MUST call these to avoid disconnect
			checkMultipleResultSets(query, "getCurrentZonePermission");

			END_TRY_SQL();
		} break;

		case CI_HOUSING: {
			if (zi.GetInstanceId() == playerId) {
				currentZonePriv = ZP_OWNER;
			} else {
				BEGIN_TRY_SQL_FOR("getCurrentZonePermission/Housing");

				Query query = m_conn->query();
				query.reset();
				query << " SELECT account_type_id FROM kaneva.community_members cm "
					  << " RIGHT OUTER JOIN kaneva.users ku ON cm.community_id=ku.home_community_id "
					  << " WHERE cm.user_id = %0 "
					  << " AND ku.wok_player_id= %1 "
					  << " AND cm.account_type_id  in (1,2) "
					  << " AND cm.status_id = 1; ";
				query.parse();
				StoreQueryResult sqlResult = query.store((int)kanevaUserId, (int)zi.GetInstanceId());

				if (sqlResult.num_rows() != 0) {
					int account_type_id = sqlResult.at(0).at(0);
					jsAssert(account_type_id <= ZP_NONE && account_type_id >= ZP_OWNER);
					currentZonePriv = account_type_id;
				}

				END_TRY_SQL();
			}
		} break;

		case CI_PERMANENT: {
			// are they the owner?
			currentZonePriv = getPermZonePriv(zi.zoneIndex(), kanevaUserId);
		} break;
	}

	// since this is called on spawn, update the player object so we don't have to check again
	if (isGm && currentZonePriv != ZP_OWNER) // preserver owner for GM
		currentZonePriv = ZP_GM;

	//tell client if current player is owner of the zone (4/08 moved outside db call to avoid nesting locks)
	*replyEvent = new SetZonePermissionEvent(currentZonePriv);
	(*replyEvent)->AddTo(from);

	return currentZonePriv;
}

void MySqlEventProcessor::sqlBulkInsertDynamicObjects(std::vector<dynamicObject*>& dynamicObjects) {
	// query.execute will throw errors.
	Query query = m_conn->query();

	// Fetch the nextup obj_placement_id for inserts
	query.reset();
	query << " SELECT id from dynamic_objects_id ";
	StoreQueryResult sqlResult = query.store();
	int nextId = sqlResult.at(0).at(0);

	std::ostringstream sqlStatment;
	bool containsData(false);
	int recordsRead(0);

	for (auto itr = dynamicObjects.begin(); itr != dynamicObjects.end(); itr++, recordsRead++) {
		dynamicObject* pRecord = *itr;

		// Flush data every n rows to avoid buffer limitations on mysql
		if (recordsRead % 7500 == 0) {
			// execute statement if needed
			if (containsData) {
				query.reset();
				query << sqlStatment.str().c_str();
				query.execute();
				containsData = false;
			}

			// clear out and reset statement
			sqlStatment.str("");
			sqlStatment << "INSERT INTO dynamic_objects "
						<< "( "
						<< " obj_placement_id,"
						<< " player_id,"
						<< " zone_index,"
						<< " zone_instance_id,"
						<< " global_id,"
						<< " inventory_sub_type, "
						<< " position_x,"
						<< " position_y,"
						<< " position_z,"
						<< " rotation_x,"
						<< " rotation_y,"
						<< " rotation_z,"
						<< " slide_x,"
						<< " slide_y,"
						<< " slide_z,"
						<< " created_date,"
						<< " expired_date"
						<< ") "
						<< " VALUES (";
		} else {
			sqlStatment << ",(";
		}

		// Rekey the entry so we can process its children parameters later
		pRecord->objPlacementID = ++nextId;

		sqlStatment << pRecord->objPlacementID << "," // obj_placement_id
					<< pRecord->playerID << "," // player_id
					<< pRecord->zoneIndex << "," // zone_index
					<< pRecord->zoneInstanceID << "," // zone_instance_id
					<< pRecord->globalID << "," // global_id
					<< pRecord->inventorySubType << "," // inventory_sub_type
					<< pRecord->positionX << "," // position_x
					<< pRecord->positionY << "," // position_y
					<< pRecord->positionZ << "," // position_z
					<< pRecord->rotationX << "," // rotation_x
					<< pRecord->rotationY << "," // rotation_y
					<< pRecord->rotationZ << "," // rotation_z
					<< pRecord->slideX << "," // slide_x
					<< pRecord->slideY << "," // slide_y
					<< pRecord->slideZ << "," // slide_z
					<< "NOW()"
					<< "," // created_date
					<< "NULL"
					<< "" // expired_date
					<< ")";
		containsData = true;
	}

	if (containsData) {
		query.reset();
		query << sqlStatment.str().c_str();
		query.execute();
		containsData = false;
	}

	query.reset();
	query << "UPDATE dynamic_objects_id set id = " << nextId;
	query.execute();
}

void MySqlEventProcessor::sqlBulkInsertDynamicObjectsParameters(std::vector<dynamicObject*>& dynamicObjects) {
	// query.execute will throw errors.
	Query query = m_conn->query();

	std::ostringstream sqlStatment;
	bool containsData(false);
	int recordsRead(0);

	for (auto doItr = dynamicObjects.begin(); doItr != dynamicObjects.end(); doItr++) {
		dynamicObject* pRecord = *doItr;

		for (auto paramItr = pRecord->parameters.begin(); paramItr != pRecord->parameters.end(); paramItr++, recordsRead++) {
			// Flush data every n rows to avoid buffer limitations on mysql
			if (recordsRead % 5000 == 0) {
				// execute statement if needed
				if (containsData) {
					query.reset();
					query << sqlStatment.str().c_str();
					query.execute();
					containsData = false;
				}

				// clear out and reset statement
				sqlStatment.str("");
				sqlStatment << "INSERT INTO dynamic_object_parameters(obj_placement_id, param_type_id, value) VALUES (";
			} else {
				sqlStatment << ",(";
			}

			sqlStatment << pRecord->objPlacementID << "," << paramItr->first << ", \"" << paramItr->second << "\")";
			containsData = true;
		}
	}

	// execute statement if needed
	if (containsData) {
		query.reset();
		query << sqlStatment.str().c_str();
		query.execute();
		containsData = false;
	}
}

void MySqlEventProcessor::sqlBulkInsertDynamicWorldObjects(std::vector<worldObjectPlayerSetting*>& worldObjects) {
	// query.execute will throw errors.
	Query query = m_conn->query();

	// Fetch the nextup world_object_player_settings for inserts
	query.reset();
	query << " SELECT id from world_object_player_settings_id ";
	StoreQueryResult sqlResult = query.store();
	int nextId = sqlResult.at(0).at(0);

	std::ostringstream sqlStatment;
	bool containsData(false);
	int recordsRead(0);

	for (auto itr = worldObjects.begin(); itr != worldObjects.end(); itr++, recordsRead++) {
		worldObjectPlayerSetting* pRecord = *itr;

		// Flush data every n rows to avoid buffer limitations on mysql
		if (recordsRead % 5000 == 0) {
			// execute statement if needed
			if (containsData) {
				query.reset();
				query << sqlStatment.str().c_str();
				query.execute();
				containsData = false;
			}

			// clear out and reset statement
			sqlStatment.str("");
			sqlStatment << "INSERT INTO world_object_player_settings "
						<< "( "
						<< " id, "
						<< " world_object_id, "
						<< " asset_id,"
						<< " texture_url,"
						<< " zone_index,"
						<< " zone_instance_id, "
						<< " last_updated_datetime"
						<< ") "
						<< " VALUES (";
		} else {
			sqlStatment << ",(";
		}

		// Rekey the entry
		pRecord->ID = ++nextId;

		sqlStatment << pRecord->ID << "," // id
					<< "\"" << pRecord->worldObjectID << "\"," // world_object_id
					<< pRecord->assetID << "," // asset_id
					<< "\"" << pRecord->textureURL << "\"," // texture_url
					<< pRecord->zoneIndex << "," // zone_index
					<< pRecord->zoneInstanceID << "," // zone_instance_id
					<< "NOW()"
					<< "" // last_updated_datetime
					<< ")";
		containsData = true;
	}

	if (containsData) {
		query.reset();
		query << sqlStatment.str().c_str();
		query.execute();
		containsData = false;
	}

	// udpate the nextup world_object_player_settings_id
	query.reset();
	query << "UPDATE world_object_player_settings_id set id = " << nextId;
	query.execute();
}

MySQLGetResultSetsEvent::MySQLGetResultSetsEvent(EVENT_ID id, ULONG filter, OBJECT_ID objectId, EVENT_PRIORITY priority) :
		IBackgroundDBEvent(id, filter, objectId, priority), ret(0), arraySize(0), pPlayerInventory(0), pBankInventory(0) {
	results = new StoreQueryResult[MAX_RESULT_SETS];
}

MySQLGetResultSetsEvent::~MySQLGetResultSetsEvent() {
	delete[] results;
}

EVENT_PROC_RC MySqlEventProcessor::YouTubeSWFUrlBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	MySqlEventProcessor* me = reinterpret_cast<MySqlEventProcessor*>(lparam);
	YouTubeSWFUrlBackgroundEvent* pEvent = static_cast<YouTubeSWFUrlBackgroundEvent*>(e);

	assert(me != nullptr);
	assert(pEvent != nullptr);

	pEvent->setUrl(""); // Default to empty (not SET)

	std::string lastKnownGoodUrl, overrideUrl;
	try {
		switch (pEvent->getUrlSource()) {
			case YouTubeSWFUrlSource::DBOverride:
				if (me->getYouTubeSWFUrls(lastKnownGoodUrl, overrideUrl)) {
					// Only need override URL here
					pEvent->setUrl(overrideUrl);
					me->m_replyDispatcher.QueueEvent(pEvent);
				}
				break;

			case YouTubeSWFUrlSource::WebCurrent:
				if (me->parseYouTubeSWFUrlFromWeb(lastKnownGoodUrl)) {
					assert(!lastKnownGoodUrl.empty());
					// Store parsed result in DB
					me->updateYouTubeSWFLastKnownGoodUrl(lastKnownGoodUrl);
					// Reply event
					pEvent->setUrl(lastKnownGoodUrl);
					me->m_replyDispatcher.QueueEvent(pEvent);
				}
				break;
		}
	} catch (KEPException* ex) {
		LogError("Caught Exception - '" << ex->ToString() << "'");
		ex->Delete();
	} catch (...) {
		LogError("Unknown Exception");
	}

	return EVENT_PROC_RC::CONSUMED;
}

} // namespace KEP