///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "DDRGameDB.h"
#include "KEPException.h"
#include "DBConfigValues.h"
#include "DBStrings.h"
#include "InstanceId.h"
#include "Plugins/app/DDRModelFacet.h"

#include "..\..\Event\ServerEvents.h"

using namespace KEP;
using namespace mysqlpp;

#include "LogHelper.h"
static LogInstance("Database");

sql_create_4(ddr_animations,
	1, 4,
	std::string, type,
	ULONG, level_id,
	ULONG, animation_id,
	int, play_order);

sql_create_17(ddr_game_data,
	1, 17,
	ULONG, seed,
	ULONG, num_seconds_to_play,
	ULONG, num_moves,
	ULONG, num_misses,
	ULONG, num_sub_levels,
	ULONG, level_id,
	ULONG, perfect_score,
	ULONG, great_score,
	ULONG, good_score,
	ULONG, finish_bonus_score,
	ULONG, level_increase_score,
	ULONG, level_bonus_trigger_num,
	ULONG, level_bonus_score,
	ULONG, perfect_score_ceiling,
	ULONG, great_score_ceiling,
	ULONG, good_score_ceiling,
	ULONG, last_level_reduction_ms);

sql_create_6(dance_level_actions,
	1, 6,
	ULONG, dance_level,
	ULONG, skill_id,
	ULONG, action_id,
	ULONG, perfect_action_id,
	ULONG, gain_amt,
	ULONG, perfect_gain_amt);

DDRGameDB::DDRGameDB(const char *gameDir, const DBConfig &dbConfig, IDispatcher *dispatcher) :
		jsThread("DDRGameDB"),
		m_dbConfig(dbConfig),
		m_conn(new KGP_Connection(use_exceptions))
//	m_logger( Logger::getInstance(L"Database") )
{
	m_gameDir = gameDir;
	m_outDispatcher = dispatcher;
}

DDRGameDB::~DDRGameDB() {
	if (m_dispatcher) // 6/08 leak fix
	{
		m_dispatcher->StopProcessingEvents();
		delete m_dispatcher;
	}

	if (m_conn != 0)
		delete m_conn;
}

/*
 *Function Name:_doWork
 *
 * Thread spins waiting for events to be posted for background work.
 * Performs the work and then posts the results via an event.
 *
 *Parameters:	none
 *
 *Description:
 *
 *Returns:	0
 *
 */
ULONG DDRGameDB::_doWork() {
	// Use this semaphore to coordinate the initialization work with the caller.
	// This allows the caller to wait for initialization of this thread's db work
	// to complete before continuing.

	HANDLE sem_handle = ::OpenSemaphoreW(SEMAPHORE_ALL_ACCESS, FALSE, L"KDP_DBINIT");

	NDCContextCreator ndx(L"DDR");

	if (sem_handle == NULL) {
		LogError("DDRGameDB Unable to coordinate DB initialization with caller. Continuing load. Last error:" << GetLastError());
	}

	if (mysql_thread_init() != 0) {
		LogError(loadStr(IDS_DB_THREAD_INIT_ERROR));
		return 0;
	}

	try {
		Initialize(m_gameDir.c_str());
	} catch (KEPException *e) {
		if (e->m_err != S_OK) {
			if (!::ReleaseSemaphore(sem_handle, 1, NULL)) {
				LogError("DDRGameDB Unable to signal coordination semaphore. Last error:" << GetLastError());
			}

			::CloseHandle(sem_handle);
			LogError(loadStr(IDS_DB_INIT_ERROR) << e->ToString());
			e->Delete();
			return 0;
		}
	}

	if (!::ReleaseSemaphore(sem_handle, 1, NULL)) {
		LogError("DDRGameDB Unable to signal coordination semaphore. Last error:" << GetLastError());
	}

	::CloseHandle(sem_handle);

	while (!isTerminated()) {
		m_dispatcher->ProcessNextEvent(3000);
	}

	mysql_thread_end();

	LOG4CPLUS_DEBUG(log4cplus::Logger::getInstance(L"SHUTDOWN"), "DDRGameDB thread exiting cleanly");
	delete this; // 6/08 leak fix

	return 0;
}

/*
 *Function Name:RecordScoreHandler
 *
 * Process request for game data event
 *
 *Parameters:	lparam is instance of DDRGameDB
 *
 *Description:
 *
 *Returns:	0
 *
 */
EVENT_PROC_RC DDRGameDB::RecordScoreHandler(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDRDBEvent *ddre = dynamic_cast<DDRDBEvent *>(e);

	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDRGameDB *me = (DDRGameDB *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();
	try {
		DataModel *model = PluginHost::singleton().DEPRECATED_getInterface<DataModel>("AppDataModel");
		DDRModel &ddrModel = model->registry().getFacet<DDRModel>("DDRModel");

		if (!(SuppressedDDRModel(ddrModel).recordDDRScore(ddre->m_isPermanentZone, ddre->m_objectId, ddre->m_zoneIndex, ddre->m_dynObjectId, ddre->GetGameId() // Clarification.  This is actually ddr id.
				,
				ddre->m_playerId, ddre->m_playerName, ddre->m_highScore, ddre->m_score, ddre->m_numPerfects, ddre->m_numPerfectsInARow))) {
			LogError(loadStrPrintf(IDS_DB_RECORD_SCORE, ddre->GetGameId(), ddre->m_dynObjectId, ddre->GetPlayerId()));
		}
	} catch (const PluginNotFoundException &) {
		if (!(me->RecordDDRScore(ddre->m_isPermanentZone, ddre->m_objectId, ddre->m_zoneIndex, ddre->m_dynObjectId, ddre->GetGameId(), ddre->m_playerId, ddre->m_playerName, ddre->m_highScore, ddre->m_score, ddre->m_numPerfects, ddre->m_numPerfectsInARow))) {
			LogError(loadStrPrintf(IDS_DB_RECORD_SCORE, ddre->GetGameId(), ddre->m_dynObjectId, ddre->GetPlayerId()));
		}
	} catch (const FacetUnavailableException &) {
		if (!(me->RecordDDRScore(ddre->m_isPermanentZone, ddre->m_objectId, ddre->m_zoneIndex, ddre->m_dynObjectId, ddre->GetGameId(), ddre->m_playerId, ddre->m_playerName, ddre->m_highScore, ddre->m_score, ddre->m_numPerfects, ddre->m_numPerfectsInARow))) {
			LogError(loadStrPrintf(IDS_DB_RECORD_SCORE, ddre->GetGameId(), ddre->m_dynObjectId, ddre->GetPlayerId()));
		}
	}
	return ret;
}

/*
 *Function Name:LoadGameDataHandler
 *
 * Process request for game meta data event
 *
 *Parameters:	lparam is instance of DDRGameDB
 *
 *Description:
 *
 *Returns:	0
 *
 */
EVENT_PROC_RC DDRGameDB::LoadGameDataHandler(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDRDBEvent *ddre = dynamic_cast<DDRDBEvent *>(e);

	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDRGameDB *me = (DDRGameDB *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	ULONG gameId = 0;

	// Collect meta-data about the game and level from db
	try {
		DataModel *model = PluginHost::singleton().DEPRECATED_getInterface<DataModel>("AppDataModel");
		DDRModel &ddrModel = model->registry().getFacet<DDRModel>("DDRModel");
		if (!SuppressedDDRModel(ddrModel).loadDDRGameData(ddre->GetGameId(), *(ddre->m_gameLevelsMap))) {
			LogError(loadStrPrintf(IDS_DB_GAME_NOT_FOUND, ddre->GetGameId()));
		} else {
			gameId = ddre->GetGameId();
		}
	} catch (const PluginNotFoundException &) {
		if (!(me->LoadDDRGameData(ddre->GetGameId(), *(ddre->m_gameLevelsMap)))) {
			LogError(loadStrPrintf(IDS_DB_GAME_NOT_FOUND, ddre->GetGameId()));
		} else {
			gameId = ddre->GetGameId();
		}
	} catch (const FacetUnavailableException &) {
		if (!(me->LoadDDRGameData(ddre->GetGameId(), *(ddre->m_gameLevelsMap)))) {
			LogError(loadStrPrintf(IDS_DB_GAME_NOT_FOUND, ddre->GetGameId()));
		} else {
			gameId = ddre->GetGameId();
		}
	}

	// Send it to server
	IEvent *iddre_sd_server = me->GetOutDispatcher()->MakeEvent(me->GetOutDispatcher()->GetEventId("DDREvent"));
	DDREvent *ddre_sd_server = dynamic_cast<DDREvent *>(iddre_sd_server);

	ddre_sd_server->GameData(ddre->GetGameId());

	me->GetOutDispatcher()->QueueEvent(ddre_sd_server);

	return ret;
}

EVENT_PROC_RC DDRGameDB::LoadPlayerDataHandler(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	DDRDBEvent *ddre = dynamic_cast<DDRDBEvent *>(e);

	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDRGameDB *me = (DDRGameDB *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	// prepare to send it back to server's main thread
	IEvent *iddre_sd_server = me->GetOutDispatcher()->MakeEvent(me->GetOutDispatcher()->GetEventId("DDREvent"));
	DDREvent *ddre_sd_server = dynamic_cast<DDREvent *>(iddre_sd_server);

	try {
		DataModel *model = PluginHost::singleton().DEPRECATED_getInterface<DataModel>("AppDataModel");
		DDRModel &ddrModel = model->registry().getFacet<DDRModel>("DDRModel");
		if (SuppressedDDRModel(ddrModel).loadDDRPlayerData(ddre, ddre_sd_server))
			me->GetOutDispatcher()->QueueEvent(ddre_sd_server);
		else
			ddre_sd_server->Delete();

	} catch (const PluginNotFoundException &) {
		if (me->LoadDDRPlayerData(ddre, ddre_sd_server))
			me->GetOutDispatcher()->QueueEvent(ddre_sd_server);
		else
			ddre_sd_server->Delete();
	} catch (const FacetUnavailableException &) {
		if (me->LoadDDRPlayerData(ddre, ddre_sd_server))
			me->GetOutDispatcher()->QueueEvent(ddre_sd_server);
		else
			ddre_sd_server->Delete();
	}
	return ret;
}

void DDRGameDB::Initialize(const char *gameDir) {
	m_dispatcher = new Dispatcher();

	RegisterEvent<DDRDBEvent>(*m_dispatcher);

	EVENT_ID ddr_id = m_dispatcher->GetEventId("DDRDBEvent");
	m_dispatcher->RegisterFilteredHandler(DDRGameDB::RecordScoreHandler, (ULONG)this, "DDRGameDB::RecordScoredHandler", DDRDBEvent::ClassVersion(), ddr_id, DDRDBEvent::DDRDB_RECORD_SCORE);
	m_dispatcher->RegisterFilteredHandler(DDRGameDB::LoadGameDataHandler, (ULONG)this, "DDRGameDB::LoadGameDataHandler", DDRDBEvent::ClassVersion(), ddr_id, DDRDBEvent::DDRDB_LOAD_GAME_DATA);
	m_dispatcher->RegisterFilteredHandler(DDRGameDB::LoadPlayerDataHandler, (ULONG)this, "DDRGameDB::LoadGameDataHandler", DDRDBEvent::ClassVersion(), ddr_id, DDRDBEvent::DDRDB_LOAD_PLAYER_DATA);

	// read the config file for the database settings
	std::string dbServer, dbUser, dbPassword;
	std::string dbClearPassword;
	int dbPort;

	const IKEPConfig &cfg = m_dbConfig.config();
	if (cfg.Get(DB_DISABLED, 0) != 0) {
		throw new KEPException(loadStr(IDS_DATABASE_DISABLED), S_OK);
	}

	// we can default only these
	cfg.GetValue(DB_PORT, dbPort, 3306);
	cfg.GetValue(DB_SERVER, dbServer, "localhost");

	if (!cfg.GetValue(DB_NAME, m_dbName) ||
		!cfg.GetValue(DB_USER, dbUser) ||
		!cfg.GetValue(DB_PASSWORD, dbPassword)) {
		throw new KEPException(loadStrPrintf(IDS_DB_BAD_CONFIG, PathAdd(gameDir, DB_CFG_NAME).c_str()), E_INVALIDARG);
	}

	LogInfo("Database initialized using the following values:");
	LogInfo(" ... Server:port:        " << dbServer << ":" << dbPort);
	LogInfo(" ... User:               " << dbUser);
	LogInfo(" ... Database:           " << m_dbName);

	if (cfg.GetValue(DB_PASSWORD_CLEAR, dbClearPassword)) {
		dbPassword = dbClearPassword;
	} else {
		// decrypt password
		std::string szPassword;
		if (SetupCryptoClient() && DecodeAndDecryptString(dbPassword.c_str(), szPassword, DB_KEY)) {
			dbPassword = szPassword;
		}
	}

	std::string errMsg;
	try {
		m_conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
		m_conn->set_option(new ReconnectOption(true));
		m_conn->connect(m_dbName.c_str(), dbServer.c_str(), dbUser.c_str(), dbPassword.c_str(), dbPort);
		if (m_conn->connected()) {
			LogInfo("Connect successful");
		}

	} catch (const mysqlpp::Exception &er) {
		errMsg = er.what();
		DELETE_AND_ZERO(m_conn);
	}

	if (!m_conn) {
		throw new KEPException(loadStrPrintf(IDS_DB_CONNECT_ERROR, errMsg.c_str()), E_INVALIDARG);
	}

	LogInfo(loadStr(IDS_DB_CONNECT) << dbServer << ":" << dbPort << ", " << m_dbName << ", " << dbUser);
}

bool DDRGameDB::RecordDDRScore(bool isPermanentZone,
	ULONG objectId,
	ULONG zoneIndex,
	int dynObjectId,
	ULONG gameId,
	int playerId,
	std::string playerName,
	ULONG /*highScore*/,
	ULONG score,
	ULONG numPerfects,
	ULONG numPerfectsInARow) {
	bool ret = false;

	BEGIN_TRY_SQL();

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	Transaction trans(*m_conn);

	Query query = m_conn->query();

	if (isPermanentZone) {
		query << "INSERT INTO ddr_scores ( game_id, player_id, dynamic_object_id, score, num_perfects, num_perfects_inarow, player_name, created_date, object_id, zone_index ) VALUES (%0, %1, %2, %3, %4, %5, %6q, NOW(), %7, %8)";
		query.parse();

		SimpleResult res = query.execute((int)gameId, (int)playerId, (int)dynObjectId, (int)score, (int)numPerfects, (int)numPerfectsInARow, playerName.c_str(), (int)objectId, (int)zoneIndex);

		/*ULONG instanceId =*/(ULONG) res.insert_id();
	} else {
		query << "INSERT INTO ddr_scores ( game_id, player_id, dynamic_object_id, score, num_perfects, num_perfects_inarow, player_name, created_date ) VALUES (%0, %1, %2, %3, %4, %5, %6q, NOW())";
		query.parse();

		SimpleResult res = query.execute((int)gameId, (int)playerId, (int)dynObjectId, (int)score, (int)numPerfects, (int)numPerfectsInARow, playerName.c_str());

		/*ULONG instanceId =*/(ULONG) res.insert_id();
	}

	trans.commit();

	ret = true;

	END_TRY_SQL();

	return ret;
}

bool DDRGameDB::LoadDDRGameData(ULONG gameId,
	GameLevelsMap &gameData) {
	bool ret = false;

	BEGIN_TRY_SQL();

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	Query query = m_conn->query();

	query << "SELECT max_players, notify_inactive, purge_timeout"
		  << " FROM ddr_globals  "
		  << " WHERE game_id = %0";

	TRACE_QUERY(query, m_logger);
	query.parse();

	StoreQueryResult results = query.store((int)gameId);

	if (results.size() == 1) {
		ULONG max_players = results.at(0).at(0);
		ULONG notify_timeout = results.at(0).at(1);
		ULONG purge_timeout = results.at(0).at(2);

		query.reset();

		query << "SELECT seed, num_seconds_to_play, num_moves, num_misses,"
			  << " num_sub_levels, level_id, perfect_score, great_score, good_score, finish_bonus_score, level_increase_score,"
			  << " level_bonus_trigger_num, level_bonus_score, perfect_score_ceiling, great_score_ceiling, good_score_ceiling, "
			  << " last_level_reduction_ms "
			  << " FROM ddr  "
			  << " WHERE game_id = %0 ORDER BY level_id asc";

		query.parse();

		std::vector<ddr_game_data> gameDataResult;

		query.storein(gameDataResult, (int)gameId);

		if (!gameDataResult.empty()) {
			query.reset();

			query << "SELECT type, level_id, animation_id, play_order "
				  << " FROM ddr_animations  "
				  << " WHERE game_id = %0 ORDER BY type, level_id, play_order asc";

			query.parse();

			std::vector<ddr_animations> animationResult;
			animationResult.clear();

			query.storein(animationResult, (int)gameId);

			query.reset();
			query << "SELECT dance_level, skill_id, action_id, perfect_action_id, gain_amt, perfect_gain_amt FROM dance_level_actions_view";
			query.parse();

			std::vector<dance_level_actions> actionsResult;
			actionsResult.clear();
			query.storein(actionsResult);

			DDRLevels *levels = new DDRLevels();

			for (std::vector<ddr_game_data>::iterator ii = gameDataResult.begin(); ii != gameDataResult.end(); ii++) {
				DDRLevelObj *l_level = new DDRLevelObj();

				l_level->SetSeed(ii->seed);
				l_level->SetNumSecondsToPlay(ii->num_seconds_to_play);
				l_level->SetNumMoves(ii->num_moves);
				l_level->SetNumMisses(ii->num_misses);
				l_level->SetNumSubLevels(ii->num_sub_levels);
				l_level->SetLastLevelReductionMilliSeconds(ii->last_level_reduction_ms);

				l_level->SetPerfectScore(ii->perfect_score);
				l_level->SetGreatScore(ii->great_score);
				l_level->SetGoodScore(ii->good_score);
				l_level->SetFinishBonusScore(ii->finish_bonus_score);
				l_level->SetLevelIncreaseScore(ii->level_increase_score);
				l_level->SetLevelBonusTriggerNum(ii->level_bonus_trigger_num);
				l_level->SetLevelBonusScore(ii->level_bonus_score);
				l_level->SetPerfectScoreCeiling(ii->perfect_score_ceiling);
				l_level->SetGreatScoreCeiling(ii->great_score_ceiling);
				l_level->SetGoodScoreCeiling(ii->good_score_ceiling);

				for (std::vector<ddr_animations>::iterator jj = animationResult.begin(); jj != animationResult.end(); jj++) {
					if (ii->level_id == jj->level_id) {
						if (jj->type == "FLARE") {
							l_level->m_AnimationFlares.push_back(std::make_pair(jj->animation_id, jj->play_order));
						} else if (jj->type == "FINAL_FLARE") {
							l_level->m_AnimationFinalFlares.push_back(std::make_pair(jj->animation_id, jj->play_order));
						} else if (jj->type == "MISS") {
							l_level->m_AnimationMisses.push_back(std::make_pair(jj->animation_id, jj->play_order));
						} else if (jj->type == "LEVELUP") {
							l_level->m_AnimationLevelUps.push_back(std::make_pair(jj->animation_id, jj->play_order));
						} else if (jj->type == "PERFECT") {
							l_level->m_AnimationPerfects.push_back(std::make_pair(jj->animation_id, jj->play_order));
						} else if (jj->type == "IDLE") {
							l_level->m_AnimationIdles.push_back(std::make_pair(jj->animation_id, jj->play_order));
						}
					}
				}

				for (auto kk = actionsResult.begin(); kk != actionsResult.end(); kk++) {
					if (ii->level_id == kk->dance_level) {
						l_level->SetSkillId(kk->skill_id);
						l_level->SetActionId(kk->action_id);
						l_level->SetGainAmount(kk->gain_amt);
						l_level->SetPerfectActionId(kk->perfect_action_id);
						l_level->SetPerfectGainAmount(kk->perfect_gain_amt);
					}
				}

				l_level->SetAnimationFlareCount(l_level->m_AnimationFlares.size());
				l_level->SetAnimationFinalFlareCount(l_level->m_AnimationFinalFlares.size());
				l_level->SetAnimationMissCount(l_level->m_AnimationMisses.size());
				l_level->SetAnimationLevelUpCount(l_level->m_AnimationLevelUps.size());
				l_level->SetAnimationPerfectCount(l_level->m_AnimationPerfects.size());
				l_level->SetAnimationIdleCount(l_level->m_AnimationIdles.size());

				levels->m_levelsMap.insert(LevelMap::value_type(ii->level_id, l_level));
			}

			levels->SetLevelCount(levels->m_levelsMap.size());
			levels->SetDanceFloorLimit(max_players);
			levels->SetNotifyInactivePlayerTimeout(notify_timeout);
			levels->SetPurgeTimeout(purge_timeout);

			gameData.insert(GameLevelsMap::value_type(gameId, levels));

			ret = true;
		}
	}

	END_TRY_SQL();

	return ret;
}

bool DDRGameDB::LoadDDRPlayerData(DDRDBEvent *ddre, DDREvent *replyEvent) {
	bool ret = false;

	BEGIN_TRY_SQL();

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	Query query = m_conn->query();

	StoreQueryResult results;

	BOOL wantHighScore = FALSE;

	// Determine if the dynamic object is in a permanent zone. If so, score will be tracked
	// by zoneIndex and objectId so that multiple instances of a dynamic object can have the
	// same high score. Otherwise, all instances are unique.
	query << "CALL get_ddr_player_data( %0, %1, %2, %3, %4, %5 )";
	query.parse();

	results = query.store((int)ddre->m_gameId, ddre->m_playerId, ddre->m_dynObjectId, (int)ddre->m_zoneIndex, (int)ddre->m_objectId, wantHighScore);

	if (results.size() == 1) {
		Row r = results.at(0);
		long rc = r.at(0);
		if (rc == 0) // ok
		{
			replyEvent->PlayerData(ddre->From(), ddre->m_gameId, ddre->m_playerId, ddre->m_gameLevel, ddre->m_newGame, ddre->m_dynObjectId,
				r.at(1), r.at(2), r.at(3));

			if (wantHighScore) {
				results = query.store_next();
				if (results.size() == 1) {
					r = results.at(0);
					replyEvent->m_highestScore = r.at(0);
					replyEvent->m_highestScoreId = r.at(1);
					replyEvent->m_highestScoreName = r.at(2);
				}
			}
			checkMultipleResultSets(query, "get_ddr_player_data");
			ret = true;
		} else {
			LogError("Object not found for " << query.str(ddre->m_gameId, ddre->m_dynObjectId, ddre->m_playerId, ddre->m_zoneIndex, wantHighScore));
			return ret;
		}
	}

	END_TRY_SQL();

	return ret;
}

bool DDRGameDB::RecordDDRHighScore(int dynObjectId,
	int playerId,
	ULONG highScore,
	std::string playerName,
	bool isPermanentZone,
	ULONG objectId,
	ULONG zoneIndex) {
	bool ret = false;

	BEGIN_TRY_SQL();

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	StoreQueryResult results;

	Transaction trans(*m_conn);

	Query query = m_conn->query();

	if (isPermanentZone) {
		query << "SELECT high_score, player_id, object_id, zone_index"
			  << " FROM ddr_high_scores "
			  << " WHERE object_id = %0 AND zone_index = %1";

		query.parse();

		results = query.store((int)objectId, (int)zoneIndex);
	} else {
		query << "SELECT high_score, player_id, dynamic_object_id"
			  << " FROM ddr_high_scores "
			  << " WHERE dynamic_object_id = %0";

		query.parse();

		results = query.store(dynObjectId);
	}

	if (results.size() > 0) {
		if (results.size() != 1) {
			if (isPermanentZone) {
				LogError("Result != 1 for -> SELECT high_score FROM ddr_high_scores WHERE object_id = '" << objectId << "' AND zone_index = '" << zoneIndex << "'");
			} else {
				LogError("Result != 1 for -> SELECT high_score FROM ddr_high_scores WHERE dynamic_object_id = '" << dynObjectId << "'");
			}
			return ret;
		}

		ULONG dbHighScore = results.at(0).at(0);
		ULONG dbPlayerId = results.at(0).at(1);
		ULONG dbObjectId = 0;
		ULONG dbZoneIndex = 0;
		ULONG dbDynamicObjectId = 0;

		if (isPermanentZone) {
			dbObjectId = results.at(0).at(2);
			dbZoneIndex = results.at(0).at(3);
		} else {
			dbDynamicObjectId = results.at(0).at(2);
		}

		// May have been updated by another server after
		if (dbHighScore > highScore) {
			return true;
		} else if (isPermanentZone &&
				   dbPlayerId == (ULONG)playerId &&
				   dbHighScore == highScore &&
				   dbObjectId == objectId &&
				   dbZoneIndex == zoneIndex) {
			// Already has high score
			return true;
		} else if (!isPermanentZone &&
				   dbPlayerId == (ULONG)playerId &&
				   dbHighScore == highScore &&
				   dbDynamicObjectId == (ULONG)dynObjectId) // int -> ULONG ?
		{
			// Already has high score
			return true;
		}

		query.reset();

		if (isPermanentZone) {
			query << "UPDATE ddr_high_scores SET player_id = %0, high_score = %1, player_name = %2q, created_date = NOW() "
				  << " WHERE object_id = %3 AND zone_index = %4";

			query.parse();

			query.execute(playerId, (int)highScore, playerName.c_str(), (int)objectId, (int)zoneIndex);
		} else {
			query << "UPDATE ddr_high_scores SET player_id = %0, high_score = %1, player_name = %2q, created_date = NOW() "
				  << " WHERE dynamic_object_id = %3";

			query.parse();

			query.execute(playerId, (int)highScore, playerName.c_str(), dynObjectId);
		}
	} else {
		query.reset();

		if (isPermanentZone) {
			query << "INSERT INTO ddr_high_scores ( dynamic_object_id, player_id, high_score, player_name, created_date, object_id, zone_index ) VALUES ( %0, %1, %2, %3q, NOW(), %4, %5 )";

			query.parse();

			SimpleResult res = query.execute(dynObjectId, playerId, (int)highScore, playerName.c_str(), (int)objectId, (int)zoneIndex);

			/*ULONG instanceId =*/(int)res.insert_id();
		} else {
			query << "INSERT INTO ddr_high_scores ( dynamic_object_id, player_id, high_score, player_name, created_date ) VALUES ( %0, %1, %2, %3q, NOW() )";

			query.parse();

			SimpleResult res = query.execute(dynObjectId, playerId, (int)highScore, playerName.c_str());

			/*ULONG instanceId =*/(int)res.insert_id();
		}
	}

	trans.commit();

	ret = true;

	END_TRY_SQL();

	return ret;
}

bool DDRGameDB::RefreshDDRHighScore(int dynObjectId, DDRPlayers *players) {
	bool ret = false;

	BEGIN_TRY_SQL();

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	Query query = m_conn->query();

	StoreQueryResult results;

	LogDebug("Attempting refresh of players high score data for dynamic object id:" << dynObjectId);

	if (players->IsPermanentZone()) {
		query << "SELECT high_score, player_id, player_name"
			  << " FROM ddr_high_scores  "
			  << " WHERE object_id = %0 AND zone_index = %1";

		query.parse();

		results = query.store((int)players->GetDynamicObjectObjectId(), (int)players->GetDynamicObjectZoneIndex());
	} else {
		query << "SELECT high_score, player_id, player_name"
			  << " FROM ddr_high_scores  "
			  << " WHERE dynamic_object_id = %0";

		query.parse();

		results = query.store(dynObjectId);
	}

	if (results.size() == 1) {
		ULONG high_score = results.at(0).at(0);
		int player_id = results.at(0).at(1);
		std::string playerName = results.at(0).at(2);

		// If db high score greater than our in memory high score refresh
		if (players->GetHighScore() < high_score) {
			players->SetHighScore(high_score);
			players->SetHighScorePlayerId(player_id);
			players->SetHighScoreCharacterName(playerName);

			LogDebug("Refresh players high score data for player id:" << player_id << " dynamic object id:" << dynObjectId << " high score:" << high_score << ":" << playerName.c_str());
		}
	}

	END_TRY_SQL();

	return ret;
}
