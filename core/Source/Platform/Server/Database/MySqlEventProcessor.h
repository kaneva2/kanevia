///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/include/KEPCommon.h"
#include "common/keputil/KGenericObjList.h"

#include "Event/Events/GotoPlaceBackgroundEvent.h"
#include "Event/Events/ChangeZoneBackgroundEvent.h"
#include "Event/Events/ZoneCustomizationBackgroundEvent.h"
#include "Event/Events/PlayListBackgroundEvent.h"
#include "Event/Events/YouTubeSWFUrlBackgroundEvent.h"
#include "Event/Events/SpaceImportBackgroundEvent.h"
#include "Event/Events/ChannelImportBackgroundEvent.h"
#include "Event/Events/GetResultSetsEvent.h"
#include "Event/Events/CanSpawnToZoneBackgroundEvent.h"
#include "Event/Events/LogoffUserBackgroundEvent.h"
#include "Event/Events/BroadcastEventBackgroundEvent.h"
#include "Event/Events/RefreshPlayerBackgroundEvent.h"
#include "Event/Events/ScriptAvailableItemsBackgroundEvent.h"
#include "Event/Events/LoadItemsBackgroundEvent.h"
#include "Event/Events/UpdateScriptZoneBackgroundEvent.h"
#include "Event/EventSystem/Dispatcher.h"

#include "MySqlUtil.h"
#include "MySqlBase.h"

namespace KEP {

typedef KGenericObjList<CInventoryObj> InventoryItemListT;

// helper classes importing communities
typedef struct _dynamicObject {
	// methods
	_dynamicObject::_dynamicObject() :
			objPlacementID(0), playerID(0), zoneIndex(0), zoneInstanceID(0), globalID(0), inventorySubType(0), positionX(0), positionY(0), positionZ(0), rotationX(0), rotationY(0), rotationZ(0), slideX(0), slideY(0), slideZ(0), createdDate("") {}

	// members
	int objPlacementID;
	int playerID;
	int zoneIndex;
	int zoneInstanceID;
	int globalID;
	int inventorySubType;
	float positionX;
	float positionY;
	float positionZ;
	float rotationX;
	float rotationY;
	float rotationZ;
	float slideX;
	float slideY;
	float slideZ;
	std::string createdDate;
	std::vector<std::pair<int, std::string>> parameters;
} dynamicObject;

typedef struct _worldObjectPlayerSetting {
	_worldObjectPlayerSetting::_worldObjectPlayerSetting() :
			ID(0), worldObjectID(""), assetID(0), textureURL(""), zoneIndex(0), zoneInstanceID(0), lastUpdatedDateTime("") {}

	int ID;
	std::string worldObjectID;
	int assetID;
	std::string textureURL;
	int zoneIndex;
	int zoneInstanceID;
	std::string lastUpdatedDateTime;
} worldObjectPlayerSetting;

/// class to process background events, handles all the db stuff for a thread
class MySqlEventProcessor : public MySqlBase {
public:
	/**
     *
     * @param serverEngine      The engine under whose auspices this processor is running
     * @param myDispatcher      A dispatcher for receiving events forwarded by the foreground thread (MySqlGameState).  I.e.
     *                          this processor listens for events from this dispatcher
     * @param replyDispatcher   A dispatcher on which this processor queues responses to the foreground thread (MySqlGameState)
     * @param gameDir           Configuration root directory
     * @param dbConfig          Configuration for the database
     */
	// Add server engine to constructor
	MySqlEventProcessor(ServerEngine* serverEngine, IDispatcher& myDispatcher, IDispatcher& replyDispatcher, const char* gameDir, const DBConfig& dbConfig) :
			MySqlBase(serverEngine, "MySQLBackground", "Timing", dbConfig),
			m_dispatcher(myDispatcher),
			m_replyDispatcher(replyDispatcher),
			m_lastPermZonePrivUpdate(0),
			m_serverNetwork(nullptr),
			m_serverVisibilityId(ServerVisibility::Unknown),
			m_serverVisibilityIdUpdateTime(0) {
		m_gameName[0] = '\0';

		const IKEPConfig& cfg = dbConfig.config();

		m_conn->loadConfig(cfg, gameDir);

		// allow override of swf
		cfg.GetValue(DB_PLAYLISTSWF, m_playlistSwf, "KGPPlaylist.swf");
		cfg.GetValue(DB_PLAYLISTPARAM, m_playlistParams, "plId=");
	}

	void SetServerNetwork(IServerNetwork* s) { m_serverNetwork = s; }

	~MySqlEventProcessor();

	/// connect to db, throws, must be called on background thd
	virtual void Initialize();

	/// initialize any event/eventhandlers
	virtual void InitEvents() {
		m_dispatcher.RegisterHandler(MySqlEventProcessor::GotoPlaceHandler, (ULONG)this, "MySqlEventProcessor::GotoPlaceHandler", GotoPlaceBackgroundEvent::ClassVersion(), GotoPlaceBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::ChangeZoneHandler, (ULONG)this, "MySqlEventProcessor::ChangeZoneHandler", ChangeZoneBackgroundEvent::ClassVersion(), ChangeZoneBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::ZoneCustomizationHandler, (ULONG)this, "MySqlEventProcessor::ZoneCustomizationHandler", ZoneCustomizationBackgroundEvent::ClassVersion(), ZoneCustomizationBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::GetItemBackgroundHandler, (ULONG)this, "MySqlEventProcessor::GetItemBackgroundHandler", GetResultSetsEvent::ClassVersion(), GetResultSetsEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::CanSpawnToZoneBackgroundHandler, (ULONG)this, "MySqlEventProcessor::CanSpawnToZoneHandler", CanSpawnToZoneBackgroundEvent::ClassVersion(), CanSpawnToZoneBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::LogoffUserBackgroundHandler, (ULONG)this, "MySqlEventProcessor::LogOffUserHandler", LogoffUserBackgroundEvent::ClassVersion(), LogoffUserBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::BroadcastEventBackgroundHandler, (ULONG)this, "MySqlEventProcessor::BroadcastEventBackgroundHandler", BroadcastEventBackgroundEvent::ClassVersion(), BroadcastEventBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::PlaylistBackgroundEventHandler, (ULONG)this, "MySqlEventProcessor::PlaylistBackgroundEventHandler", PlaylistBackgroundEvent::ClassVersion(), PlaylistBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::YouTubeSWFUrlBackgroundEventHandler, (ULONG)this, "MySqlEventProcessor::YouTubeSWFUrlBackgroundEventHandler", YouTubeSWFUrlBackgroundEvent::ClassVersion(), YouTubeSWFUrlBackgroundEvent::ClassId());

		m_dispatcher.RegisterHandler(MySqlEventProcessor::RefreshPlayerBackgroundHandler, (ULONG)this, "MySqlEventProcessor::RefreshPlayerBackgroundHandler", RefreshPlayerBackgroundEvent::ClassVersion(), RefreshPlayerBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::SpaceImportHandler, (ULONG)this, "MySqlEventProcessor::SpaceImportHandler", SpaceImportBackgroundEvent::ClassVersion(), SpaceImportBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::ChannelImportHandler, (ULONG)this, "MySqlEventProcessor::ChannelImportHandler", ChannelImportBackgroundEvent::ClassVersion(), ChannelImportBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::ScriptAvailableItemsBackgroundHandler, (ULONG)this, "MySqlEventProcessor::ScriptAvailableItemsBackgroundHandler", ScriptAvailableItemsBackgroundEvent::ClassVersion(), ScriptAvailableItemsBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::LoadItemsBackgroundHandler, (ULONG)this, "MySqlEventProcessor::LoadItemsBackgroundHandler", LoadItemsBackgroundEvent::ClassVersion(), LoadItemsBackgroundEvent::ClassId());
		m_dispatcher.RegisterHandler(MySqlEventProcessor::UpdateScriptZoneBackgroundHandler, (ULONG)this, "MySqlEventProcessor::UpdateScriptZoneBackgroundHandler", UpdateScriptZoneBackgroundEvent::ClassVersion(), UpdateScriptZoneBackgroundEvent::ClassId());
	}

	int backgroundThreadCount() const { return m_conn->backgroundThreadCount(); }

	/// process the GotoPlaceEvent in the background
	static EVENT_PROC_RC GotoPlaceHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ChangeZoneHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ZoneCustomizationHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC GetItemBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC GetAccountFromWebBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC CanSpawnToZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC LogoffUserBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC BroadcastEventBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC PlaylistBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC YouTubeSWFUrlBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC RefreshPlayerBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SpaceImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ChannelImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ScriptAvailableItemsBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC LoadItemsBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdateScriptZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);

private:
	EVENT_PROC_RC GotoPlaceUrl(GotoPlaceBackgroundEvent* gpe);
	EVENT_PROC_RC GotoPlaceApartment(GotoPlaceBackgroundEvent* gpe);
	EVENT_PROC_RC GotoPlaceZone(GotoPlaceBackgroundEvent* gpe);
	EVENT_PROC_RC GotoPlacePlayer(GotoPlaceBackgroundEvent* gpe);

	void addDynamicObject(ZoneCustomizationBackgroundEvent* zce);
	void moveDynamicObject(ZoneCustomizationBackgroundEvent* zce);
	void rotateDynamicObject(ZoneCustomizationBackgroundEvent* zce);
	void removeDynamicObject(ZoneCustomizationBackgroundEvent* zce);
	void updateDynamicObjectTexture(ZoneCustomizationBackgroundEvent* zce);
	void updateDynamicObjectAnimation(ZoneCustomizationBackgroundEvent* zce);
	void updateDynamicObjectFriendTexture(ZoneCustomizationBackgroundEvent* zce);
	void updateDynamicObjectParticle(ZoneCustomizationBackgroundEvent* zce);
	void updateDynamicObjectSound(ZoneCustomizationBackgroundEvent* zce);
	void updateWorldObjectTexture(ZoneCustomizationBackgroundEvent* zce);

	void getCommBlockingUsers(LONG blockedKanevaId, std::vector<ULONG>& ids);

	// DRF - Added
	void updateDynamicObjectMedia(ZoneCustomizationBackgroundEvent* zce);
	void updateMediaPlayerVolumeAndRadius(ZoneCustomizationBackgroundEvent* zce);

	BYTE getPermZonePriv(LONG zoneIndex, ULONG serverId);
	BYTE getCurrentZonePermission(ZoneIndex& zi, int playerId, int kanevaUserId, bool isGm, NETID from, IEvent** replyEvent);

	// Obtain all playlist IDs for current zone through local DB query
	bool getZoneMediaPlayersFromDB(ZoneIndex& zi, ZoneMediaState* zoneMediaInfo);

	// Get tracks information for all playlists from entire zone, or for one single media players
	void getPlaylists(PlaylistBackgroundEvent* plbe, int placementID = 0, int playListID = 0);
	// Get tracks information for playlist (Web call)
	//	bool getPlaylistsFromWeb(PlaylistBackgroundEvent *plbe, int placementID, int playListID);
	// Get tracks information for playlist (Wok DB)
	bool getPlaylistsFromDB(PlaylistBackgroundEvent* plbe, int placementID, int playListID);

	// Parse wokweb playlist result
	bool parseWokwebPlaylistResult(int playListID, const std::string& resultXml, ZoneMediaState& zoneMediaInfo);

	bool populateAccountXMLFromWebCall(MySQLGetResultSetsEvent* grse);

	void importCommunity(SpaceImportBackgroundEvent* sibe);
	void importChannel(ChannelImportBackgroundEvent* cibe);

	void sqlBulkInsertDynamicObjects(std::vector<dynamicObject*>& dynamicObjects);
	void sqlBulkInsertDynamicObjectsParameters(std::vector<dynamicObject*>& dynamicObjects);
	void sqlBulkInsertDynamicWorldObjects(std::vector<worldObjectPlayerSetting*>& worldObjects);

	void loadScriptServerAvailableItems();
	void updateScriptZoneSpinUp(unsigned zoneInstanceId, unsigned zoneType, unsigned spinDownDelayMins);
	void updateScriptZoneSpinDown(unsigned zoneInstanceId, unsigned zoneType);
	ServerVisibility getServerVisibilityId();

	bool dropCacheItem(const std::string& key);
	bool getCacheItem(const std::string& key, bool& found, std::string& keys);
	const char* getLastCacheError();

	bool dropCompoundCacheItem(const std::string& key) const;
	MySqlEventProcessor& backloadInventory(MySQLGetResultSetsEvent& grse, InventoryItemListT& PlayerInventory, InventoryItemListT& BankInventory);

	void MySqlEventProcessor::getInventory(CInventoryObjList& playerInv, CInventoryObjList& bankInv, InventoryItemListT& PlayerInventory, InventoryItemListT& BankInventory);

	bool checkServerId(const char* desc, bool logMsg);
	const char* pingerGameName();

	bool getCurrentZoneInfoNoThrow(NETID currentNetID, int playerId, int kanevaUserId, bool isGm, ZoneIndex zoneIndex, BYTE& permissions, unsigned& parentZoneInstanceId);

	void DEBUGArtificialLag(int min, int max);

	//Ankit ----- functions to correct spawn location if zoning to game world through "gotoplace" event
	bool IsZoneAGameWorld(ZoneIndex zoneIndex);
	void MakeSpawnLocationTheWorldDefaultSpawnPoint(GotoPlaceBackgroundEvent* gpe);

	IDispatcher& m_dispatcher; //< mine for getting events
	IDispatcher& m_replyDispatcher; //< for replying to the main thread
	std::string m_playlistSwf;
	std::string m_playlistParams;

	typedef std::vector<std::pair<ULONG, BYTE>> UserPriv; // serverId, priv
	typedef std::map<long, UserPriv> UserPrivMap; // zoneIndex->Priv
	UserPrivMap m_permZonePrivs;
	ULONG m_lastPermZonePrivUpdate;
	IServerNetwork* m_serverNetwork;
	char m_gameName[101]; // from pinger

	ServerVisibility m_serverVisibilityId;
	TimeMs m_serverVisibilityIdUpdateTime;
};

} // namespace KEP