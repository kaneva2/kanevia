///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "MySqlUtil.h"
#include "MySqlGameState.h"
#include "MySqlEventProcessor.h"
#include "alert.h"
#include "UGCConstants.h"

// XStatic structs
#include "CAccountClass.h"
#include "CPlayerClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "PassList.h"
#include "CMultiplayerClass.h"
#include "ServerEngine.h"
#include "CCurrencyClass.h"
#include "LogHelper.h"

using namespace mysqlpp;

namespace KEP {

static LogInstance("Instance");

static const char* getValue(TiXmlElement* child, const char* elementName) {
	if (child) {
		TiXmlHandle h(child);
		TiXmlText* t = h.FirstChild(elementName).FirstChild().Text();
		if (t) {
			return t->Value();
		}
	}
	return 0;
}

template <class C>
bool getStringValue(C& value, TiXmlElement* child, const char* elementName) {
	const char* temp = getValue(child, elementName);
	// allow string values in XML to be empty
	if (temp) {
		value = temp;
	} else {
		value = "";
	}
	return true;
}

template <class C>
bool getIntValue(C& value, TiXmlElement* child, const char* elementName) {
	const char* temp = getValue(child, elementName);
	if (temp) {
		value = (C)atol(temp);
		return true;
	}
	return false;
}

template <class C>
bool getFloatValue(C& value, TiXmlElement* child, const char* elementName) {
	const char* temp = getValue(child, elementName);
	if (temp) {
		value = (C)atof(temp);
		return true;
	}
	return false;
}

static bool getBoolValue(bool& value, TiXmlElement* child, const char* elementName) {
	int temp = 0;
	if (getIntValue(temp, child, elementName)) {
		value = temp != 0;
		return true;
	}
	return false;
}

bool MySqlBase::xmlGetDefConfigs(CPlayerObject* pPO, TiXmlElement* table) {
	pPO->m_charConfig.disposeAllItems();
	pPO->m_baseMeshes = 0;

	if (table == 0) {
		return false;
	}

	const char* nodeName = table->Value();

	for (TiXmlNode* sib = table; sib != 0; sib = sib->NextSibling(nodeName)) {
		pPO->m_baseMeshes++;
	}

	if (pPO->m_baseMeshes > 0) {
		// first get counts of each type/equip
		int equipId = 0;
		if (!getIntValue(equipId, table, "equip_id")) {
			return false;
		}

		GLID_OR_BASE glidOrBase = (GLID_OR_BASE)equipId;
		BYTE slotCount = 0;

		for (TiXmlElement* sib = table; sib != 0; sib = sib->NextSiblingElement(nodeName)) {
			if (!getIntValue(equipId, sib, "equip_id")) {
				return false;
			}

			if (glidOrBase != equipId) {
				pPO->m_charConfig.addItem(glidOrBase, slotCount);
				slotCount = 0;
				glidOrBase = equipId;
			}
			slotCount++;
		}
		pPO->m_charConfig.addItem(glidOrBase, slotCount); // do last one (or first if only one)

		size_t sequence = 0;
		int prevEquip = CC_ITEM_INVALID;
		CharConfigItem* ci = 0;
		for (TiXmlElement* sib = table; sib != 0; sib = sib->NextSiblingElement(nodeName)) {
			if (!getIntValue(equipId, sib, "equip_id")) {
				return false;
			}

			if (prevEquip != equipId) {
				sequence = 0;
				prevEquip = equipId;

				ci = pPO->m_charConfig.getItemByGlidOrBase(prevEquip);
				if (ci == 0) {
					LogWarn("equip id out of range " << prevEquip << " for player " << pPO->m_charName);
					continue;
				}
			}

			if (ci != 0) {
				if (sequence < ci->getSlotCount()) {
					CharConfigItemSlot* pSlot = ci->getSlot(sequence);
					assert(pSlot != nullptr);

					if (!getIntValue(pSlot->m_meshId, sib, "deformable_config") ||
						!getIntValue(pSlot->m_matlId, sib, "custom_material") ||
						!getFloatValue(pSlot->m_r, sib, "custom_color_r") ||
						!getFloatValue(pSlot->m_g, sib, "custom_color_g") ||
						!getFloatValue(pSlot->m_b, sib, "custom_color_b")) {
						return false;
					}
				} else {
					LogWarn("def config slot out of range " << sequence << " for equip " << prevEquip << " for player " << pPO->m_charName);
				}
			}
			sequence++;
		}
	}

	return true;
}

bool MySqlBase::xmlLoadPassList(CPlayerObject* pPO, TiXmlElement*& table) {
	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);
	Query query = m_conn->query();

	query << "DELETE FROM pass_group_users WHERE kaneva_user_id = %0";
	query.parse();
	LogTrace(query.str((int)pPO->m_pAccountObject->m_serverId));
	query.execute((int)pPO->m_pAccountObject->m_serverId);

	if (pPO->m_passList) {
		DELETE_AND_ZERO(pPO->m_passList);
	}

	if (table != 0) {
		if (pPO->m_passList == 0) {
			pPO->m_passList = new PlayerPassList;
		}

		const char* nodeName = table->Value();

		for (TiXmlElement* sib = table; sib != 0; sib = sib->NextSiblingElement()) {
			if (strcmp(sib->Value(), nodeName) == 0) {
				PassId id = 0;
				if (!getIntValue(id, sib, "pass_group_id")) {
					return false;
				}

				const char* expireDate = getValue(sib, "end_date");
				if (!expireDate) {
					return false;
				}

				DateTime dt(expireDate);
				pPO->m_passList->addPass(id, dt);

				query.reset();
				query << "INSERT IGNORE INTO pass_group_users (kaneva_user_id, pass_group_id) VALUES (%0,%1)";
				query.parse();
				LogTrace(query.str((int)pPO->m_pAccountObject->m_serverId, id));
				query.execute((int)pPO->m_pAccountObject->m_serverId, id);
			} else {
				break;
			}
		}
	}

	END_TRY_SQL();
	return true;
}

#define ACCOUNT_RS "Table"
#define PLAYER_RS "Table1"
#define SCALE_RS "Table2"
#define INV_RS "Table3"
#define DEF_CONFIG_RS "Table4"
#define CASH_RS "Table5"
#define CLAN_RS "Table6"
#define PASSES_RS "Table7"
#define COLOR_RS "Table8"
#define GAME_INV_RS "Table9"

TiXmlElement* xmlParseResultHeader(Logger& logger, TiXmlDocument* doc, const char* wantedChild) {
	TiXmlHandle docHandle(doc);

	// check the result code
	TiXmlText* text = docHandle.FirstChild("Result").FirstChild("ReturnCode").FirstChild().ToText();
	TiXmlText* desc = docHandle.FirstChild("Result").FirstChild("ReturnDescription").FirstChild().ToText();
	if (text == 0 || desc == 0) {
		LogWarn("Result not found in XML");
		return 0;
	} else if (strcmp(text->Value(), "0") != 0) {
		LogWarn("Error getting account info " << text->Value() << " " << desc->Value());
		return 0;
	}

	return docHandle.FirstChild("Result").FirstChild(wantedChild).ToElement();
}

void MySqlBase::saveScriptServerAvailableItems(const std::vector<int>& glids) {
	if (glids.empty()) {
		return;
	}

	// query.execute will throw errors.
	Query query = m_conn->query();

	std::ostringstream sqlStatment;
	bool containsData(false);
	int recordsRead(0);

	for (auto itr = glids.cbegin(); itr != glids.cend(); itr++, recordsRead++) {
		long glid = *itr;

		// Flush data every n rows to avoid buffer limitations on mysql
		// 2500 * INT(11) + padding
		if (recordsRead % 2500 == 0) {
			// execute statement if needed
			if (containsData) {
				query.reset();
				query << sqlStatment.str().c_str();
				LogError(query.str());
				query.execute();
				containsData = false;
			}

			// clear out and reset statement
			sqlStatment.str("");
			sqlStatment << "INSERT IGNORE INTO script_available_items_plain(global_id) VALUES (";
		} else {
			sqlStatment << ",(";
		}
		sqlStatment << glid << ")";
		containsData = true;
	}

	if (containsData) {
		query.reset();
		query << sqlStatment.str().c_str();
		LogTrace(query.str());
		query.execute();
		containsData = false;
	}

	return;
}

char* MySqlBase::utf8ToWindows1252(const char* input) const {
	//
	// We can only display Windows-1252 in-world, but we get strings in utf-8.
	// Convert them using WideCharToMultiByte, but to do that we first have to
	// get our utf-8 string into a WCHAR array.
	//
	// 65001 the Microsoft UTF-8 code page
	//
	int wideSize = MultiByteToWideChar(65001, 0, input, -1, 0, 0);
	if (wideSize == 0) {
		return 0;
	}
	WCHAR* wideInput = new WCHAR[wideSize];
	MultiByteToWideChar(65001, 0, input, -1, wideInput, wideSize);
	//
	// Now we can convert the wchar into a multi-byte format, which doesn't have
	// to actually use multiple bytes and in the case of Windows-1252, doesn't.
	//
	LPSTR output = 0;
	int multiSize = WideCharToMultiByte(1252, 0, wideInput, wideSize, 0, 0, 0, 0);
	if (multiSize) {
		//
		// then convert the wide version to what we want, which is Windows-1252
		//
		output = new char[multiSize];
		WideCharToMultiByte(1252, 0, wideInput, wideSize, output, multiSize, 0, 0);
	}
	delete[] wideInput;
	return output;
}

bool MySqlBase::xmlFillInAccount(TiXmlDocument* doc, CAccountObject* pAO) {
	// get the account "table"
	TiXmlElement* table = xmlParseResultHeader(m_logger, doc, ACCOUNT_RS);
	if (table == 0) {
		LogWarn("Error parsing result header");
		return false;
	}

	if (!getIntValue(pAO->m_accountNumber, table, "user_id") || !getIntValue(pAO->m_age, table, "age") || !getBoolValue(pAO->m_showMature, table, "show_mature") || !getIntValue(pAO->m_timePlayedMinutes, table, "time_played")) {
		LogWarn("Error getting account elements");
		return false;
	}

	CStringA value;
	if (getStringValue(value, table, "is_gm") && value == "T") {
		pAO->m_gm = TRUE;
	}

	if (getStringValue(value, table, "can_spawn") && value == "T") {
		pAO->m_canSpawn = TRUE;
	}
	if (getStringValue(value, table, "is_admin") && value == "T") {
		pAO->m_administratorAccess = TRUE;
	}

	if (pAO->m_pPlayerObjectList == 0) {
		pAO->m_pPlayerObjectList = new CPlayerObjectList();
	}

	// get player data
	table = table->NextSiblingElement(PLAYER_RS);
	if (table == 0) {
		LogWarn("Error getting player data");
		return false;
	}

	auto pPO = new CPlayerObject();
	pPO->m_timeDbUpdate = 1.0; // DRF - Should be fTime::TimeMs() ???
	pPO->m_pAccountObject = pAO;

	if (pPO->m_cash == 0)
		pPO->m_cash = new CCurrencyObj();

	if (pPO->m_bankCash == 0)
		pPO->m_bankCash = new CCurrencyObj();

	if (pPO->m_giftCredits == 0)
		pPO->m_giftCredits = new CCurrencyObj();

	pPO->m_title = getValue(table, "title");

	if (!getIntValue(pPO->m_handle, table, "player_id") ||
		!getStringValue(pPO->m_charName, table, "name") ||
		!getIntValue(pPO->m_originalEDBCfg, table, "original_EDB")) {
		LogWarn("Error getting player elements");
		pPO->SafeDelete();
		delete pPO;
		return false;
	} else {
		char* newTitle = utf8ToWindows1252(pPO->m_title);
		if (newTitle) {
			pPO->m_title = newTitle;
			delete[] newTitle;
		}

		char* newName = utf8ToWindows1252(pPO->m_charName);
		if (newName) {
			pPO->m_charName = newName;
			delete[] newName;
		}
	}

	table = table->NextSiblingElement(SCALE_RS);
	if (table == 0) {
		LogWarn("Error getting player scaling");
		pPO->SafeDelete();
		delete pPO;
		return false;
	}

	LONG zi;
	if (!getFloatValue(pPO->m_scaleFactor.x, table, "scale_factor_x") ||
		!getFloatValue(pPO->m_scaleFactor.y, table, "scale_factor_y") ||
		!getFloatValue(pPO->m_scaleFactor.z, table, "scale_factor_z") ||
		!getIntValue(zi, table, "housing_zone_index")) {
		LogWarn("Error getting player scale");
		pPO->SafeDelete();
		delete pPO;
		return false;
	}
	pPO->m_scratchScaleFactor = pPO->m_scaleFactor;
	pPO->m_housingZoneIndex = zi;
	pPO->m_curEDBIndex = pPO->m_dbCfg = pPO->m_originalEDBCfg;
	pPO->m_clanHandle = NO_CLAN; // no longer on pPO record

	// as a safety measure set the instance id
	pPO->m_housingZoneIndex.setAsHousing(pPO->m_handle);

	if (pPO->m_respawnZoneIndex.isHousing()) {
		//10/30 need to get correct zone index (map) for respawn or else
		//we will spawn to starter home (curreently studio apt zone index == 5)
		pPO->m_respawnZoneIndex = pPO->m_housingZoneIndex;
		pPO->m_respawnZoneIndex.setInstanceId(pPO->m_handle);
	}

	// get the inventory, a bunch of this same table type
	table = table->NextSiblingElement(INV_RS);
	if (table && table->FirstChild("inventory_type")) {
		if (!xmlGetInventory(pPO->m_charName, &pPO->m_inventory, &pPO->m_bankInventory, table)) {
			LogWarn("Error getting player inventory");
			pPO->SafeDelete();
			delete pPO;
			return false;
		} else if (multiplayerObj()->m_serverNetwork->pinger().GetParentGameId() != 0) {
			// we have a gm player that needs to sync his inventory.

			// _SCR_ perf tracking
			std::clock_t _step_0_startTime = std::clock();
			// _SCR_ perf tracking

			// get worlds script server available items
			std::set<int, std::less<int>> worldGlids = m_server->getAvailableScriptItems();

			// get players available items list
			std::set<int, std::less<int>> playerGlids;
			if (pPO->m_inventory) {
				for (POSITION pos = pPO->m_inventory->GetHeadPosition(); pos != NULL;) {
					CInventoryObj* invObj = (CInventoryObj*)pPO->m_inventory->GetNext(pos);
					playerGlids.insert(invObj->m_itemData->m_globalID);
				}
			}

			// compute deltas between inventory.
			int cnt = (worldGlids.size() >= playerGlids.size()) ? worldGlids.size() : playerGlids.size();
			std::vector<int> vAddToWorld(cnt);
			std::vector<int> vAddToPlayer(cnt);

			// a) (PLayer->World) items that needed to be added to world inventory
			std::vector<int>::iterator it1 = std::set_difference(playerGlids.begin(), playerGlids.end(), worldGlids.begin(), worldGlids.end(), vAddToWorld.begin());
			vAddToWorld.resize(it1 - vAddToWorld.begin());

			// b) (World->Player) items that needed to be added to the players inventory
			std::vector<int>::iterator it2 = std::set_difference(worldGlids.begin(), worldGlids.end(), playerGlids.begin(), playerGlids.end(), vAddToPlayer.begin());
			vAddToPlayer.resize(it2 - vAddToPlayer.begin());

			// _SCR_ perf tracking
			// calculates amount of time required in memory to perform 2 distinct inventory deltas
			clock_t _step_0_endTime = (std::clock() - _step_0_startTime);
			std::clock_t _step_1_startTime = std::clock();
			// _SCR_ perf tracking

			// a) (World->Player) items that needed to be added to world inventory
			if (vAddToWorld.size() > 0) {
				LogInfo("InvTrack Updating World inventory DB with [" << vAddToWorld.size() << "] new glids from [" << pPO->m_charName << "] (Player->World)");
				saveScriptServerAvailableItems(vAddToWorld); // database
				m_server->addItemsToAvailableScriptItems(vAddToWorld); // memory
			} else {
				LogInfo("InvTrack Nothing to add to world inventory from [" << pPO->m_charName << "]");
			}

			// _SCR_ perf tracking
			// calculates amount of time for step1
			clock_t _step_1_endTime = (std::clock() - _step_1_startTime);
			std::clock_t _step_2_startTime = std::clock();
			// _SCR_ perf tracking

			// b) (World->Player) items that needed to be added to the players inventory
			if (vAddToPlayer.size() > 0) {
				LogInfo("InvTrack Updating player[" << pPO->m_charName << "] inventory with [" << vAddToPlayer.size() << "] new glids (World->Player)");
				m_server->addScriptAvailableItemsToInventory(vAddToPlayer, pPO->m_inventory);
			} else {
				LogInfo("InvTrack Nothing to add to player[" << pPO->m_charName << "] inventory from world");
			}

			// _SCR_ perf tracking
			// string is grepable by ""InvTrack_steps_"  and php explodable on ','
			clock_t _step_2_endTime = (std::clock() - _step_2_startTime);
			LogError("InvTrack_steps_ player," << pPO->m_charName << ",0," << _step_0_endTime << ",1," << _step_1_endTime << ",2," << _step_2_endTime);
			// _SCR_ perf tracking
		}
	} else {
		LogWarn("Error getting player inventory");
		pPO->SafeDelete();
		delete pPO;
		return false;
	}

	// def configs
	table = table->NextSiblingElement(DEF_CONFIG_RS);
	xmlGetDefConfigs(pPO, table);

	// get cash amts, when finished table is on next element, which can be different for each player
	// since rest are optional
	table = table->NextSiblingElement(CASH_RS);
	if (table) {
		for (; table != 0 && strcmp(table->Value(), CASH_RS) == 0; table = table->NextSiblingElement()) {
			const char* v = getValue(table, "kei_point_id");
			if (v) {
				if (strcmp(v, "GPOINT") == 0) {
					getIntValue(pPO->m_giftCredits->m_amount, table, "balance");
				} else if (strcmp(v, "KPOINT") == 0) {
					getIntValue(pPO->m_cash->m_amount, table, "balance");
				}
			}
		}
	} else {
		LogTrace("No player cash table found in xml.  Defaulting to zero.");
		pPO->m_giftCredits->m_amount = 0;
		pPO->m_cash->m_amount = 0;
	}

	// rest are optional, so need to check each table
	if (table && strcmp(table->Value(), CLAN_RS) == 0) {
		// clan name-owner
		const char* clanName = 0;
		PLAYER_HANDLE clanOwner = 0;
		clanName = getValue(table, "clan_name");
		if (clanName && getIntValue(clanOwner, table, "owner_id")) {
			// then convert to windows-1252 and pass that to setClan and use in other places for player name and title.
			char* clanNameOut = utf8ToWindows1252(clanName);
			if (clanNameOut) {
				setClan(pPO, clanNameOut, clanOwner);
				delete[] clanNameOut;
			} else {
				setClan(pPO, "", clanOwner);
			}
		}
		table = table->NextSiblingElement();
	}

	if (table && strcmp(table->Value(), PASSES_RS) == 0) {
		// table is param to move it ahead if have passlists
		if (!xmlLoadPassList(pPO, table)) {
			LogWarn("Error getting player passes");
			pPO->SafeDelete();
			delete pPO;
			return false;
		}
	}

	if (table && strcmp(table->Value(), COLOR_RS) == 0) {
		getIntValue(pPO->m_nameColor.r, table, "color_r");
		getIntValue(pPO->m_nameColor.g, table, "color_g");
		getIntValue(pPO->m_nameColor.b, table, "color_b");
		table = table->NextSiblingElement();
	}

	// add game inventory sync records to the player's inventory
	if (table && strcmp(table->Value(), GAME_INV_RS) == 0) {
		if (!xmlGetGameInventorySync(pPO->m_charName, (int)pAO->m_serverId, &pPO->m_inventory, &pPO->m_bankInventory, table)) {
			LogWarn("Error populating player's game inventory sync records");
		}
	}

	pAO->m_pPlayerObjectList->AddTail(pPO);
	return true;
}

bool fillInPlayList(TiXmlElement* table, int& assetID, std::string& assetOffsiteID, eMediaType& assetTypeID, eMediaSubType& assetSubTypeID, TimeSec& runTimeSeconds, std::string& thumbNailMediumPath, std::string& mediaPath, std::string& assetName) {
	int assetTypeIdInt = (int)eMediaType::INVALID;
	int assetSubTypeIdInt = (int)eMediaSubType::NONE;
	int runTimeSecInt = 0;
	bool ok = true && getIntValue(assetID, table, "asset_id") && getStringValue(assetOffsiteID, table, "asset_offsite_id") && getIntValue(assetTypeIdInt, table, "asset_type_id") && getIntValue(assetSubTypeIdInt, table, "asset_sub_type_id") && getIntValue(runTimeSecInt, table, "run_time_seconds") && getStringValue(thumbNailMediumPath, table, "thumbnail_medium_path") && getStringValue(mediaPath, table, "media_path") && getStringValue(assetName, table, "name");
	assetTypeID = (eMediaType)assetTypeIdInt;
	assetSubTypeID = (eMediaSubType)assetSubTypeIdInt;
	runTimeSeconds = (TimeSec)runTimeSecInt;
	return ok;
}

bool fillInDynamicObject(TiXmlElement* table, dynamicObject* pDynamicObject) {
	const char* date = 0;
	date = getValue(table, "created_date");
	if (date) {
		pDynamicObject->createdDate = date;
	}

	bool ok = true && getIntValue(pDynamicObject->objPlacementID, table, "obj_placement_id") && getIntValue(pDynamicObject->playerID, table, "player_id") && getIntValue(pDynamicObject->zoneIndex, table, "zone_index") && getIntValue(pDynamicObject->zoneInstanceID, table, "zone_instance_id") && getIntValue(pDynamicObject->globalID, table, "global_id") && getIntValue(pDynamicObject->inventorySubType, table, "inventory_sub_type") && getFloatValue(pDynamicObject->positionX, table, "position_x") && getFloatValue(pDynamicObject->positionY, table, "position_y") && getFloatValue(pDynamicObject->positionZ, table, "position_z") && getFloatValue(pDynamicObject->rotationX, table, "rotation_x") && getFloatValue(pDynamicObject->rotationY, table, "rotation_y") && getFloatValue(pDynamicObject->rotationZ, table, "rotation_z") && (!pDynamicObject->createdDate.empty()) && getFloatValue(pDynamicObject->slideX, table, "slide_x") && getFloatValue(pDynamicObject->slideY, table, "slide_y") && getFloatValue(pDynamicObject->slideZ, table, "slide_z");
	return ok;
}

bool fillInDynamicObjectParameters(TiXmlElement* parameter, dynamicObject* pDynamicObject) {
	bool retval = false;

	while (parameter) {
		retval = true;
		int paramTypeID = 0;
		std::string value;

		getIntValue(paramTypeID, parameter, "param_type_id");
		getStringValue(value, parameter, "value");

		pDynamicObject->parameters.push_back(make_pair(paramTypeID, value));

		parameter = parameter->NextSiblingElement("parameter");
	}

	return retval;
}

bool fillInWorldObject(TiXmlElement* table, worldObjectPlayerSetting* pWorldObjectPlayerSetting) {
	const char* lastUpdated = 0;
	lastUpdated = getValue(table, "last_updated_datetime");
	if (lastUpdated) {
		pWorldObjectPlayerSetting->lastUpdatedDateTime = lastUpdated;
	}

	return getIntValue(pWorldObjectPlayerSetting->ID, table, "id") &&
		   getStringValue(pWorldObjectPlayerSetting->worldObjectID, table, "world_object_id") &&
		   getIntValue(pWorldObjectPlayerSetting->assetID, table, "asset_id") &&
		   getStringValue(pWorldObjectPlayerSetting->textureURL, table, "texture_url") &&
		   (!pWorldObjectPlayerSetting->lastUpdatedDateTime.empty()) &&
		   getIntValue(pWorldObjectPlayerSetting->zoneIndex, table, "zone_index") &&
		   getIntValue(pWorldObjectPlayerSetting->zoneInstanceID, table, "zone_instance_id");
}

bool fillInItem(TiXmlElement* table, CItemObj* pItem) {
	int useTypeInt = USE_TYPE_NONE;
	bool ok = getIntValue(pItem->m_globalID, table, "global_id") &&
			  getStringValue(pItem->m_itemName, table, "name") &&
			  //getStringValue(pItem->m_itemDescription, table, "description")          &&
			  getIntValue(pItem->m_marketCost, table, "market_cost") &&
			  getIntValue(pItem->m_sellingPrice, table, "selling_price") &&
			  getIntValue(pItem->m_requiredSkillLevel, table, "required_skill_level") &&
			  getIntValue(pItem->m_requiredSkillType, table, "required_skill") &&
			  getIntValue(useTypeInt, table, "use_type") &&
			  getIntValue(pItem->m_itemType, table, "inventory_type") &&
			  getIntValue(pItem->m_baseGlid, table, "base_global_id") &&
			  getIntValue(pItem->m_expiredDuration, table, "expired_duration") &&
			  getIntValue(pItem->m_permanent, table, "permanent") &&
			  getBoolValue(pItem->m_defaultArmedItem, table, "is_default") &&
			  getIntValue(pItem->m_visualRef, table, "visual_ref") &&
			  getBoolValue(pItem->m_derivable, table, "is_derivable") &&
			  getIntValue(pItem->m_stackable, table, "stackable") &&
			  getIntValue(pItem->m_miscUseValue, table, "misc_use_value");
	pItem->m_useType = (USE_TYPE)useTypeInt;
	return ok;
}

bool xmlFillInItem(Logger& logger, TiXmlDocument* doc, CItemObj** item, CItemObj** baseItem) {
	*item = 0;
	*baseItem = 0;

	TiXmlElement* table = xmlParseResultHeader(logger, doc, "items");

	if (table) {
		*item = new CItemObj;
		if (!fillInItem(table, *item)) {
			delete *item;
			*item = 0;
			LogWarn("Error getting item elements");
			return false;
		} else {
			// Update item's pass lists
			for (TiXmlElement* pass = table->FirstChildElement("passes"); pass; pass = pass->NextSiblingElement("passes")) {
				int passGroupID = 0;
				getIntValue(passGroupID, pass, "pass_group_id");

				if ((*item)->m_passList == 0) {
					(*item)->m_passList = new PassList;
				}
				(*item)->m_passList->addPass(passGroupID);
			}

			// Update item's exclusion groups
			for (TiXmlElement* exGroup = table->FirstChildElement("exclusiongroups"); exGroup; exGroup = exGroup->NextSiblingElement("exclusiongroups")) {
				int exclusionGroupId = 0;
				getIntValue(exclusionGroupId, exGroup, "exclusion_group_id");
				(*item)->m_exclusionGroupIDs.push_back(exclusionGroupId);
			}

			if (IS_UGC_GLID((*item)->m_globalID)) {
				// If the item has associated animations, iterate over them here.
				// When the item is added to the database, m_animations can be
				// used to add the animation GLIDS too.
				TiXmlElement* animation = table->FirstChildElement("animation");
				while (animation != 0) {
					int animGLID = 0;
					getIntValue(animGLID, animation, "animation_id");
					(*item)->m_animations.push_back(animGLID);
					animation = animation->NextSiblingElement("animation");
				}

				if ((*item)->m_baseGlid > 0) {
					table = TiXmlHandle(table->NextSibling("items")).ToElement();
					if (table) {
						*baseItem = new CItemObj;
						if (!fillInItem(table, *baseItem)) {
							delete *item;
							*item = 0;
							delete *baseItem;
							*baseItem = 0;
							LogWarn("Error getting base item elements");
							return false;
						}
					} else {
						delete *item;
						*item = 0;
						LogWarn("Failed to find base item element");
						return false;
					}
				}
			}
		}
	} else {
		delete *item;
		*item = 0;
		LogWarn("Failed to find item element");
		return false;
	}
	return true;
}

} // namespace KEP