///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "IGameState.h"
#include "KEPNetwork.h"

#include <jsThread.h>
static ULONG tlsId = 0;

namespace KEP {

// methods to create "global" state for use on this thread
IGameState* IGameState::GetState() {
	jsVerifyReturn(tlsId != 0, 0);
	return (IGameState*)jsThread::getThreadData(tlsId);
}

void IGameState::Initialize(const char* /*gameDir*/) {
	if (tlsId == 0)
		tlsId = jsThread::createThreadData();
	jsThread::setThreadData(tlsId, (ULONG)this);
}

IGameState::~IGameState() {
	jsThread::setThreadData(tlsId, 0);
}

const char* IGameState::MapDisconnectReason(ULONG disconnectReason) {
	//
	// in addition to logging, these strings are used in the login_log.reason_code enum
	// in the database, so the strings here should match the db enum values
	//

	switch (disconnectReason) {
		case DISC_NORMAL:
			return "NORMAL";
		case DISC_LOST:
			return "LOST";
		case DISC_TERMINATED:
			return "TERMINATED";
		case DISC_FORCE:
			return "FORCE";
		case DISC_TIMEOUT:
			return "TIMEOUT";
		case DISC_BANNED:
			return "BANNED";
		case DISC_SECURITY_VIO:
			return "SECURITY_VIO";
		case DISC_COMM_ASSIGN:
			return "COMM_ASSIGN";
		case DISC_PLAYER_ZONED:
			return "PLAYER_ZONED";
		case DISC_SERVER_RESTART:
			return "SERVER_RESTART";
		default:
			return "UNKNOWN";
	}
}

} // namespace KEP