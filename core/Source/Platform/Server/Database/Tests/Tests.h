///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "mysql++/mysql++.h"

#include "Core/Util/Pool.h"
#include "Core/Util/ThreadPool.h"
#include "Server/Database/MySqlUtil.h"

void doUpdate(std::shared_ptr<KGP_Connection> pc, const std::string update) {
	mysqlpp::SimpleResult res;
	try {
		Query query = pc->query();
		query << update;
		query.parse();
		// need more testing
		//		This piece right here don't work 'cause zi, ii, is not staying intact after queuer exits.
		res = query.execute();
	} catch (const mysqlpp::Exception& e) {
		cout << e.what() << endl;
	}
}
void doQuery(std::shared_ptr<KGP_Connection> m_conn, const std::string select) {
	log4cplus::Logger m_logger = Logger::getInstance(L"Instance");
	for (int retry = 0; retry < 5; retry++) {
		// BEGIN_TRY_SQL
		//		mysql_thread_init();

		try {
			Query query = m_conn->query();
			query << select;
			query.parse();
			mysqlpp::StoreQueryResult res;
			res = query.store();
			break;
		} catch (const mysqlpp::Exception& e) {
			cout << e.what();
		}
		// END_TRY_SQL("TEST");
	}
}

// These tests were used to isolate connection timeout issues and are not
// appropriate for nightly testing.  Hence DISABLED.
//
// Note that these test only work when the wait_timeout value is less than the sleep
// time.
// So it either requires an aggressive wait_timeout configuration, or the sleep time will
// be prohibitive for regular execution.
// Left here as a landing zone for other tests
//
// See mysql my.ini wait_timeout documentation
//
TEST(ConnectionPoolTests, DISABLED_Test01) {
	KGPConnectionFactory* pf = new KGPConnectionFactory("localhost", "test", "test", "test", (unsigned short)0);
	std::shared_ptr<KGPConnectionFactory> spf(pf);

	KGPConnectionPool ConnPool(spf);

	// Borrow on this thread so that connection will be made on this
	// thread (Remember the pool is empty, so it will be forced
	// create a connection
	//

	// Now queue up a task to be executed on another thread that will
	// attempt to reuse the connection created on this thread.
	//
	{
		ConcreteThreadPool tp(1);
		tp.start()->wait();

		tp.enqueue([&]() -> RunResultPtr {
			std::shared_ptr<KGP_Connection> pc1 = ConnPool.borrow();
			std::shared_ptr<KGP_Connection> pc2 = ConnPool.borrow();
			std::shared_ptr<KGP_Connection> pc3 = ConnPool.borrow();
			std::shared_ptr<KGP_Connection> pc4 = ConnPool.borrow();
			std::shared_ptr<KGP_Connection> pc5 = ConnPool.borrow();
			return nullptr;
		});
		tp.shutdown()->wait();
	}

	cout << "Sleeping";
	// ::Sleep(60000);
	for (int n = 0; n < 2; n++) {
		cout << ".";
		::Sleep(60000);
	}

	cout << endl;
	ASSERT_EQ(5, ConnPool.size());
	ASSERT_EQ(5, ConnPool.available());

	ConcreteThreadPool tp(1);
	tp.start()->wait();

	tp.enqueue([&]() -> RunResultPtr {
		mysqlpp::Connection::thread_start();
		std::shared_ptr<KGP_Connection> pc = ConnPool.borrow();

		if (!pc->connected()) {
			pc->ping();
			if (!pc->connected())
				throw new KEPException(loadStr(IDS_DB_NOT_CONNECTED), pc->errnum());
		}

		//Query							query = pc->query();
		//doUpdate(pc, "drop table if exists test");
		//doUpdate(pc, "create table test.test( test varchar(10) )");
		//doUpdate(pc, "insert into test.test values( 'test')");
		//doQuery(pc, "select * from test.test");
		//doQuery(pc, "select * from sound_customizations limit 1");
		doQuery(pc, "select * from test.test");
		return nullptr;
	});
	tp.shutdown()->wait();
}

TEST(ConnectionPoolTests, DISABLED_Test02) {
	bool b = mysqlpp::Connection::thread_aware();
	if (b)
		Algorithms::Noop();
	KGPConnectionFactory* pf = new KGPConnectionFactory("localhost", "testuser", "testpass", "wok", (unsigned short)0);
	std::shared_ptr<KGPConnectionFactory> spf(pf);

	KGPConnectionPool ConnPool(spf);

	// Borrow on this thread so that connection will be made on this
	// thread (Remember the pool is empty, so it will be forced
	// create a connection
	//

	// Now queue up a task to be executed on another thread that will
	// attempt to reuse the connection created on this thread.
	//
	std::shared_ptr<KGP_Connection> pc = ConnPool.borrow();
	doQuery(pc, "select * from sound_customizations limit 1");
	cout << "Sleeping";
	for (int n = 0; n < 11; n++) {
		cout << ".";
		::Sleep(60000);
	}

	doQuery(pc, "select * from test.test");
}
