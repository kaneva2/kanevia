///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Glids.h"
#include "Event/Base/IEventHandler.h"
#include "tools\networkV2\INetwork.h"

template <class>
class KGenericObjList;

namespace mysqlpp {
class StoreQueryResult;
class Row;
class Query;

} // namespace mysqlpp

namespace KEP {

class CAccountObject;
class CGlobalInventoryObj;
class CInventoryObj;
class CInventoryObjList;
class CItemObj;
class CMultiplayerObj;
class CNoterietyObj;
class CPlayerObject;
class CQuestJournalList;
class CSkillObjectList;
class CStatObjList;
class CTitleObjectList;
class ItemWrapper;
class KGP_Connection;
class ServerEngine;
enum class CurrencyType;

/****
 * \brief base class for foreground and background MySQL processing until 
 * everything is moved
 */
class MySqlBase {
protected:
	MySqlBase(ServerEngine* serverEngine, const char* logName, const char* timingLogName, const DBConfig& dbConfig);

	CMultiplayerObj* multiplayerObj() const;

	EVENT_PROC_RC getPlayerInfo(IEvent* e);

	void fillInPlayer(CPlayerObject* player, mysqlpp::StoreQueryResult* results, int resultsCount, NETID from, void** playerInventory, void** bankInventory);

	void getGetSet(CPlayerObject* player, mysqlpp::StoreQueryResult& res);
	void loadPassList(CPlayerObject* player, mysqlpp::StoreQueryResult& res);

	void fillInPlayer(CPlayerObject* player, mysqlpp::StoreQueryResult* results, int resultsCount, NETID from);

	bool fillInAccount(CAccountObject* acct, mysqlpp::StoreQueryResult* results, int resultCount, NETID from);

	/**
	 * Given an inventories record, and collections containing, the related
	 * item mysqlpp::Row, any base item rows, and any Passes that may be associated
	 * with the owned or base items, add the owned item and optionally the base item
	 * to the CGlobalInventoryObjList instance.
	 * 
	 * @param playerName        The name of the player whose inventory is being added.
	 * @param playerInv         The players player inventory list.
	 * @param bankInv			The players bank inventory list.
	 * @param InventoryRecords	The mysqlpp::StoreQueryResult reference at index 3
	 *							of the results generated by get_player_data stored procedure.
	 * @param ItemRecords		The mysqlpp::StoreQueryResult reference at index 15 
	 *							of the results generated by get_player_data stored procedure.
	 * @param PassRecords		The mysqlpp::StoreQueryResult reference at index 16 
	 *							of the results generated by get_player_data stored procedure.
	 */
	void getInventory(const char* playerName, CInventoryObjList** playerInv, CInventoryObjList** bankInv, mysqlpp::StoreQueryResult& InventoryRecords, mysqlpp::StoreQueryResult& ItemRecords, mysqlpp::StoreQueryResult& PassRecords);

	/**
	 * Given a mysqlpp::Row reference, populate a CItemObj reference.  Note that the tuple is acquired from the 
	 * the result set at index 15, in the multi result set returned by the mysql stored procedure get_player_data()
	 * (schema rev100).
	 *
	 * @param row   mysqlpp::Row reference from which to populate.
	 * @param item  The CItemObj reference to be populated
	 */
	void popItemFromTuple(mysqlpp::Row& row, CItemObj& item);

	CGlobalInventoryObj* addCustomItem(mysqlpp::Row& InventoryTuple, KGenericObjList<ItemWrapper>& OwnedItemsList, KGenericObjList<ItemWrapper>& BaseItemsList, KGenericObjList<ItemWrapper>& PassesList);

	void updateItemPassList(CItemObj& item, KGenericObjList<ItemWrapper>& PassesList);
	bool checkServerId(const char* desc, bool logMsg);
	void playerInvUpdateCommitted(CPlayerObject* player);

	virtual bool createClan(CPlayerObject *player, const char *clanName, ULONG &clanId);
	virtual bool deleteClan(int ownerId);
	virtual bool addMemberToClan(ULONG clanId, ULONG playerId);
	virtual bool removeMemberFromClan(ULONG clanId, ULONG playerId);
	void setClan(CPlayerObject* player, const char* clanName, int ownerHandle);

	// loading items from KAPI
	bool xmlFillInAccount(TiXmlDocument* doc, CAccountObject* acct);
	bool xmlLoadPassList(CPlayerObject* player, TiXmlElement*& table);
	bool xmlGetDefConfigs(CPlayerObject* player, TiXmlElement* table);

	// Save available items for script server to db
	void saveScriptServerAvailableItems(const std::vector<int>& glids);

	char* utf8ToWindows1252(const char* input) const;

	EVENT_PROC_RC RefreshPlayerStage1(IEvent* e);

	void getInventory(const char* playerName, CInventoryObjList** playerInv, CInventoryObjList** bankInv, mysqlpp::StoreQueryResult& res);

	void addInventoryItem(const char* playerName, CInventoryObjList* invList, mysqlpp::Row& r);
	void addInventoryItem(const char* playerName, CInventoryObjList* invList, int glid, int qty, bool armed, short invSubType, int chgQty);
	CInventoryObj* addInventoryItem(const char* playerName, CInventoryObjList* invList, CGlobalInventoryObj& invItem, int qty, bool armed, short invSubType, int chgQty);

	void addCustomItems(const std::set<int>& globalIds);

	CGlobalInventoryObj* addCustomItem(GLID glid);

	bool getItemData(int glid, CItemObj* item);

	bool refreshItemData(int glid, CItemObj* item, bool loadPassList);

	bool refreshItemsData(const std::set<int>& glids, std::map<int, CItemObj>* items);

	void updateItemPassList(CItemObj* item);

	CGlobalInventoryObj* getItemFromWeb(GLID glid);

	bool xmlFillInInventory(TiXmlDocument* doc, const char* playerName, int kanevaUserId, CInventoryObjList** playerInv);

	bool xmlGetInventory(const char* playerName, CInventoryObjList** playerInv, CInventoryObjList** bankInv, TiXmlElement* table);

	bool xmlGetGameInventorySync(const char* playerName, int kanevaUserId, CInventoryObjList** playerInv, CInventoryObjList** bankInv, TiXmlElement* table);

	void updatePlayerNoTrans(CPlayerObject* player, ULONG changeMask, bool loggingOut = false);
	void updateInventory(int playerId, CInventoryObjList* inventory, const char* inventoryType);
	void updateInventory3DApp(int playerId, CInventoryObjList* inventory, const char* inventoryType);
	void updateStats(int playerId, CStatObjList* stats);
	void updateSkills(int playerId, CSkillObjectList* skills);
	bool getPlayerSkills(int playerId, CSkillObjectList*& skills);
	bool get3DAppPlayerTitles(int playerId, CTitleObjectList*& titles, CStringA& title);
	void updateTitles(int playerId, CTitleObjectList* titles);
	void updateTitles3DApp(int playerId, CTitleObjectList* titles, const CStringA& titleName);
	void updateQuests(int playerId, CQuestJournalList* skills);
	void updateGetSet(CPlayerObject* player, bool metaData = false);
	void updateNoteriety(int playerId, CNoterietyObj* noteriety);
	void updateDeformableMeshes(CPlayerObject* player);
	void getOnlineFriendList(int playerServerId, std::vector<std::string>& broadcastListArray,
		int maxListSize = 25);
	LONG getPlayersCash(int kanevaId, CurrencyType currencyType);

	void savePlayerSpawnPoint(int playerId, ULONG zoneIndexCleared, ULONG instanceId, float x, float y, float z, float rx, float ry, float rz);
	void deletePlayerSpawnPoint(int playerId, ULONG zoneIndexCleared, ULONG instanceId);
	bool getPlayerSpawnPoint(int playerId, ULONG zoneIndexCleared, ULONG instanceId, float& x, float& y, float& z, float& rx, float& ry, float& rz);

	std::string getPlayerTitle(int playerId, int titleId);

	static bool getParentZoneInfo(KGP_Connection* conn, unsigned zoneInstanceId, unsigned zoneType, unsigned& parentZoneInstanceId, unsigned& parentZoneIndex, unsigned& parentZoneType, unsigned& parentCommunityId);

	bool getYouTubeSWFUrls(std::string& lastKnownGoodUrl, std::string& overrideUrl);
	void updateYouTubeSWFLastKnownGoodUrl(const std::string& url);
	bool parseYouTubeSWFUrlFromWeb(std::string& url);

	// returns true if we're to load players and accts from local db
	bool localPlayers() const;

	int m_serverId; //< pinger id
	const DBConfig& m_dbConfig;
	KGP_Connection* m_conn;
	ServerEngine* m_server;
};

//1 stuff used by derived classes

// in db, inventory types
static const char* const DB_PLAYER_INVENTORY = "P";
static const char* const DB_BANK_INVENTORY = "B";
static const char* const DB_HOUSING_INVENTORY = "H";

// ==========================================================================
// SSQLS objects
// ==========================================================================
#define INVENTORY_COLS "`add_count`, `player_id`, `inventory_type`, `global_id`, `inventory_sub_type`, `armed`, `quantity`, `charge_quantity`"

} // namespace KEP