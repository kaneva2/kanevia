///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "KEPCommon.h"
#include "../ServerEngine/ServerEngine.h"
#include "common/keputil/KGenericObjList.h"
#include "Plugins/App/PlayerModel.h"
#include "MySqlUtil.h"
#include "MySqlGameState.h"
#include "DBConfigValues.h"
#include "DBStrings.h"
#include "LocaleStrings.h"
#include "KEPException.h"
#include "KEPConfig.h"
#include "URLParser.h"
#include "Plugins/App/ModelObjects.h"
#include "audit.h"
#include "alert.h"
#include "param_ids.h"
#include "KEPConfigurationsXML.h"
#include "GameStateDispatcher.h"
#include "kgpBrowser.h"
#include <mysqld_error.h>
#include <InstanceId.h>
#include <CPlayerClass.h>
#include "CAccountClass.h"
#include "CClanSystemClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CNoterietyClass.h"
#include "CCurrencyClass.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "CTitleObject.h"
#include <limits>
#include "SlashCommand.h"
#include <PassList.h>
#include "CWorldAvailableClass.h"
#include "Common/include/LogTimings.h"
#include "CMultiplayerClass.h"
#include "UgcConstants.h"
#include "..\..\Tools\HttpClient\GetBrowserPage.h"
#include "TransactionType.h"
#include "LogHelper.h"

#define GET_GETSET_COLS "`name`, `type`, `int_value`, `double_value`, `string_value`"
#define GET_PLAYER_COLS ""
#define OWNER_OR_MODERATOR "1,2" // 1 = owner, 2 = moderator, 3 = subscriber
#define ACTIVE_MEMBER 1

// number of result sets returned from get_player_data
#define PLAYER_RESULTS 17

// cols for get_inventory
#define INV_PLAYER_ID 0
#define INV_TYPE 1
#define INV_GLID 2
#define INV_SUB_TYPE 3
#define INV_ARMED 4
#define INV_QTY 5
#define INV_CHG_QTY 6

using namespace mysqlpp;

namespace KEP {

static LogInstance("Instance");

// first database player_id is 10,000.  Anything < that is
// local account only
static const int MIN_DATABASE_PLAYER_HANDLE = MAX_LOCAL_PLAYER_HANDLE + 1;

// gift status
static const char *const GIFT_UNACCEPTED = "U";
static const char *const GIFT_ACCEPTED = "A";

// gift type
static const char *const GIFT_3D = "3D";

// class for mapping server ids to ip/port

class ServerInfo {
public:
	ServerInfo(LONG s = 0, const char *ip = "", ULONG p = 0) :
			serverId(s), ipAddress(ip), port(p) {}
	LONG serverId;
	std::string ipAddress;
	ULONG port;
};
sql_create_8(inventories8,
	4, 8,
	int, player_id,
	std::string, inventory_type, //  type of inventory this is (Player/Bank/Housing...)
	int, global_id,
	int, inventory_sub_type,
	std::string, armed, // is item currently armed (T/F)
	int, quantity,
	int, charge_quantity,
	int, corpse_id);

//--------------------------------------
sql_create_2(placed_objects,
	1, 2,
	int, obj_placement_id,
	std::string, name);

//--------------------------------------
sql_create_6(deformable_configs_get,
	1, 6,
	int, equip_id,
	int, deformable_config,
	int, custom_material,
	float, custom_color_r,
	float, custom_color_g,
	float, custom_color_b);

//--------------------------------------
sql_create_4(player_skills,
	1, 4,
	int, player_id,
	int, skill_id,
	int, current_level,
	float, current_experience);

sql_create_2(dynamic_get_remove,
	2, 0,
	int, global_id,
	int, inventory_sub_type);

sql_create_3(user_balances,
	2, 3,
	int, user_id,
	std::string, kei_point_id,
	double, balance);

sql_create_2(server_get,
	2, 0,
	int, user_id,
	std::string, server_name);

sql_create_2(group_expansion_get,
	2, 0,
	int, player_id,
	std::string, name);

//--------------------------------------
sql_create_4(starting_inventory_get,
	1, 4,
	int, global_id,
	std::string, armed,
	int, quantity,
	int, sub_type);

static void getPlayerPosRot(CPlayerObject *pPO, float &x, float &y, float &z, int &rotation) {
	if (!pPO)
		return;

	MovementData md = pPO->InterpolatedMovementData();
	x = md.pos.x;
	y = md.pos.y;
	z = md.pos.z;
	rotation = (int)pPO->InterpolatedRotDeg();
}

IGameState::EUpdateRet MySqlGameState::PayCoverCharge(CAccountObject *pAO, CPlayerObject *pPO) {
	jsVerifyReturn(pAO != 0 && pAO->GetSpawnInfo().isSet(), UPDATE_ERROR);

	const SpawnInfo &spawnInfo = pAO->GetSpawnInfo();
	if (spawnInfo.getCoverCharge() == 0)
		return UPDATE_OK;

	EUpdateRet ret = UPDATE_ERROR;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();
	query << "CALL payCoverCharge( %0, %1, %2, %3, %4, @ret, @player1CurAmt, @player2CurAmt ); SELECT @ret, @player1CurAmt, @player2CurAmt;";
	query.parse();

	StoreQueryResult res = query.store((int)pAO->m_serverId,
		(int)spawnInfo.getZoneOwnerId(),
		(int)spawnInfo.getCoverCharge(),
		(int)ZoneIndex(spawnInfo.getZoneIndexInt()).getClearedMappedInstanceId(),
		(int)spawnInfo.getZoneInstanceId());

	res = query.store_next(); // result set for the select

	if (!res.num_rows() == 0) {
		LONG dbret = res.at(0).at(0);
		if (dbret == 0) {
			LONG player1Cash = res.at(0).at(1);
			LONG player2Cash = res.at(0).at(2);

			if (pPO != 0)
				pPO->m_cash->m_amount = player1Cash;

			// if player2 on this server?
			CPlayerObject *player2 = m_server->GetPlayerObjectByServerId(spawnInfo.getZoneOwnerId());
			if (player2 != 0)
				player2->m_cash->m_amount = player2Cash;

			ret = UPDATE_OK;
			trans.commit();
		} else if (dbret == 1) {
			ret = UPDATE_PLAYER1_INSUFFICIENT;
		}
	} else {
		// error
		LogWarn("payCoverCharge got no results " << pAO->m_accountName);
	}

	END_TRY_SQL();

	return ret;
}

EVENT_PROC_RC MySqlGameState::PlayerSpawnedEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;

	MySqlGameState *me = (MySqlGameState *)lparam;

	START_TIMING(me->m_timingLogger, "DB:PlayerSpawnedEventHandler");

	int netTraceId;
	ULONG ulPlayerNotUsed = 0; //don't use pointer for player object.  safer to get by netAssignedId.
	BOOL aiControlled = FALSE;

	CPlayerObject *pPO;

	(*e->InBuffer()) >> netTraceId >> ulPlayerNotUsed >> aiControlled;
	if (aiControlled)
		return ret;

	pPO = me->m_server->GetPlayerObjectByNetTraceId(netTraceId);

	if (pPO == 0) {
		LogInfo("GetPlayerByNetTraceId returned null, netTraceId: " << netTraceId);
		return ret;
	}

	// see if they are in a zone that has dynamic objects
	BEGIN_TRY_SQL();

	Transaction trans(*me->m_conn);

	// update them on spawn
	me->updatePlayerNoTrans(pPO, PlayerMask::SPAWN_INFO);

	// update balances from DB
	me->RefreshPlayersCashes(pPO);

	ret = EVENT_PROC_RC::OK;

	trans.commit();
	me->playerInvUpdateCommitted(pPO);

	END_TRY_SQL_LOG_ONLY(me->m_conn);
	return ret;
}

EVENT_PROC_RC MySqlGameState::TextEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	MySqlGameState *me = (MySqlGameState *)lparam;
	TextEvent *pEvent = dynamic_cast<TextEvent *>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	START_TIMING(me->m_timingLogger, "DB:TextEventHandler");

	// see if they are in a zone that has dynamic objects
	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;

	BEGIN_TRY_SQL();

	ret = EVENT_PROC_RC::OK;

	SlashCommand commandId = SlashCommand::NONE;
	int networkAssignedId = 0;
	std::string text;
	eChatType chatType;

	pEvent->ExtractInfo(commandId, networkAssignedId, text, chatType);
	if (commandId == SlashCommand::LISTOBJECTS) {
		bool isGm = false;

		// update the table, making sure the player is a gm or owner
		auto pPO = me->m_server->GetPlayerObjectByNetId(e->From(), true);
		if (pPO) {
			if (pPO->m_gm)
				isGm = true;
#ifndef _DEBUG
			if (!isGm) // not debugging, only gms can do it
				return ret;
#endif

			// hit the database for a list of items that can be placed
			ret = EVENT_PROC_RC::CONSUMED;

			// if housing, etc., get the owner
			std::set<placed_objects> result;
			Query query = me->m_conn->query();
			query << "SELECT do.obj_placement_id, i.name "
					 "  FROM dynamic_objects do INNER JOIN items i"
					 "          ON do.global_id = i.global_id"
					 " AND do.zone_index = %0:zi"
					 " AND do.zone_instance_id = %1:ii";
			query.parse();
			query.template_defaults["zi"] = pPO->m_currentZoneIndex.getClearedInstanceId();
			query.template_defaults["ii"] = pPO->m_currentZoneIndex.GetInstanceId();
			query.storein(result);

			char s[500];
			sprintf_s(s, _countof(s), "There are %d dynamic in this apartment", result.size());
			me->m_server->SendTextMessage(pPO, s, eChatType::System);

			// send down an add event for each one
			for (const auto &placedObj : result) {
				sprintf_s(s, _countof(s), "    %8d %s", placedObj.obj_placement_id, placedObj.name.c_str());
				me->m_server->SendTextMessage(pPO, s, eChatType::System);
			}
		}
	}

	END_TRY_SQL_LOG_ONLY(me->m_conn);

	return ret;
}

//This handles events before GameManager.py.  If the handler succeeds then
//the event is allowed to be processed by the python, otherwise it is consumed.
EVENT_PROC_RC MySqlGameState::UgcZoneLiteHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	UgcZoneLiteEvent *ugce = dynamic_cast<UgcZoneLiteEvent *>(e);
	jsVerifyReturn(lparam != 0 && ugce != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlGameState *me = (MySqlGameState *)lparam;

	IEvent *replyEvent = 0;

	bool isGm = false;
	bool isAdmin = false;
	bool isServer = false;
	bool success = false;

	if (e->From() == 0)
		isServer = true;

	auto pPO = me->m_server->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO && !isServer)
		return EVENT_PROC_RC::NOT_PROCESSED; //  lost player?

	if (!isServer) {
		isGm = pPO->m_gm != 0;
		isAdmin = pPO->m_admin != 0;
	} else if (isServer && e->Filter() != UgcZoneLiteEvent::REQ_LIST_ZONES) {
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// Must be GM or server for everything except:
	//     REQ_LIST_SPAWNS
	if (!isGm && !isServer && e->Filter() != UgcZoneLiteEvent::REQ_LIST_SPAWNS) {
		replyEvent = new RenderTextEvent("You must be a GM to perform that action", eChatType::System);
	} else {
		Transaction *pTrans = 0;

		try {
			// make macros happy
			KGP_Connection *m_conn = me->m_conn;

			CHECK_CONNECTION

			BEGIN_TRY_SQL();

			Query query = me->m_conn->query();

			switch (ugce->Filter()) {
				case UgcZoneLiteEvent::REQ_RE_CREATE_ZONE: {
					// extract all the parameters
					LONG zoneIndex;
					LONG baseZoneIndex;
					int maintainObjects;
					LONG copyZoneIndex;
					long copyFlag;

					jsVerifyReturn(ugce->ExtractReCreateZone(zoneIndex, baseZoneIndex, maintainObjects, copyZoneIndex, copyFlag), EVENT_PROC_RC::NOT_PROCESSED);

					if (zoneIndex < 0)
						zoneIndex = pPO->m_currentZoneIndex.zoneIndex();

					CWorldAvailableObj *waCurrent = me->multiplayerObj()->m_currentWorldsUsed->GetByIndex(zoneIndex);
					CWorldAvailableObj *waBase = me->multiplayerObj()->m_currentWorldsUsed->GetByIndex(baseZoneIndex);
					if (waCurrent != 0 && waBase != 0) {
						pTrans = new Transaction(*me->m_conn);

						CStringA baseWorldName = waBase->m_worldName;
						std::string name = waCurrent->m_displayName;
						int occupancy = waCurrent->m_maxOccupancy;
						LONG zoneType = 4;
						float x = 0, y = 0, z = 0;
						int rotation = 0;

						// DeleteZone
						Query queryDelete = m_conn->query();
						queryDelete << "CALL delete_ugc_zone_lite(%0, %1)";
						queryDelete.parse();

						LogTrace(queryDelete.str((int)zoneIndex, maintainObjects));
						StoreQueryResult resDelete = queryDelete.store((int)zoneIndex, maintainObjects);

						// for SPs MUST call these to avoid disconnect
						checkMultipleResultSets(queryDelete, "UgcZoneLiteEvent::REQ_RE_CREATE_ZONE_1");

						// CreateZone
						Query queryCreate = m_conn->query();
						queryCreate << "CALL create_ugc_zone_lite(%0, %1, %2q, %3, %4, %5, %6, %7, %8, %9, %10)";
						queryCreate.parse();

						LogTrace(queryCreate.str((int)baseZoneIndex, (int)zoneType, name, occupancy, true, x, y, z, rotation, (int)copyZoneIndex, (int)copyFlag));
						StoreQueryResult resCreate = queryCreate.store((int)baseZoneIndex, (int)zoneType, name, occupancy, true, x, y, z, rotation, (int)copyZoneIndex, (int)copyFlag);

						checkMultipleResultSets(queryCreate, "UgcZoneLiteEvent::REQ_RE_CREATE_ZONE_2");
						if (resCreate.num_rows() == 1 && (long)resCreate.at(0).at(0) != 0) {
							long newZoneIndex = (long)resCreate.at(0).at(0);

							// copy over world_object_player_settings
							if (copyZoneIndex != 0) {
								int beginZtype = ZoneIndex(copyZoneIndex).GetType();

								for (int ztype = beginZtype; ztype < 6; ztype++) {
									query.reset();
									query << "INSERT INTO world_object_player_settings(id, world_object_id, asset_id, texture_url, zone_index, zone_instance_id, last_updated_datetime) "
										  << " SELECT "
										  << "  get_world_object_player_settings_id(),"
										  << "  world_object_id,"
										  << "  asset_id,"
										  << "  texture_url,"
										  << "  make_zone_index(" << newZoneIndex << ", " << ztype << "),"
										  << "  1,"
										  << "  NOW()"
										  << " FROM "
										  << "  world_object_player_settings"
										  << " WHERE "
										  << "  zone_index = make_zone_index(" << copyZoneIndex << ", " << ztype << ")";
									query.execute();
								}
							}

							// rekey any dynamic objects
							query.reset();
							query << "UPDATE dynamic_objects set zone_index = make_zone_index(" << newZoneIndex << ", 4) WHERE zone_index = make_zone_index(" << zoneIndex << ", 4)";
							query.execute();
							query.reset();
							query << "UPDATE dynamic_objects set zone_index = make_zone_index(" << newZoneIndex << ", 5) WHERE zone_index = make_zone_index(" << zoneIndex << ", 5)";
							query.execute();

							if (pTrans) {
								pTrans->commit();
								delete pTrans;
								pTrans = 0;
							}

							// all db actions are done refresh memory
							me->multiplayerObj()->m_currentWorldsUsed->DeleteAndRemove(zoneIndex);
							me->multiplayerObj()->m_spawnPointCfgs->DeleteSpawnsPointByZoneIndex(zoneIndex);

							me->multiplayerObj()->m_currentWorldsUsed->AddNewWorld(resCreate.at(0).at(0), baseWorldName, name.c_str(), occupancy);
							me->multiplayerObj()->m_spawnPointCfgs->AddSpawnPoint(resCreate.at(0).at(1),
								resCreate.at(0).at(2), resCreate.at(0).at(3), resCreate.at(0).at(4), resCreate.at(0).at(5),
								resCreate.at(0).at(0), resCreate.at(0).at(6));

							// send back the new id in a message
							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyReCreateZone(zoneIndex, true, newZoneIndex, "Zone successfully recreated");
							replyEvent = ugReply;

							success = true;
						} else {
							if (pTrans) {
								pTrans->rollback();
								delete pTrans;
								pTrans = 0;
							}

							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyReCreateZone(zoneIndex, false, 0, "Got zero zone_index from create");
							replyEvent = ugReply;
						}
					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyReCreateZone(zoneIndex, false, 0, "Zone index not found");
						replyEvent = ugReply;
					}
				} break;

				case UgcZoneLiteEvent::REQ_CREATE_ZONE: {
					// extract all the parameters
					LONG baseZoneIndex;
					LONG zoneType;
					std::string name;
					int defaultOccupancy;
					bool usePlayerPosAsSpawnPoint;
					float x = 0, y = 0, z = 0;
					int rotation;
					LONG copyZoneIndex;
					LONG copyFlag;

					jsVerifyReturn(ugce->ExtractCreateZone(name, baseZoneIndex, zoneType, usePlayerPosAsSpawnPoint, defaultOccupancy, copyZoneIndex, copyFlag), EVENT_PROC_RC::NOT_PROCESSED);

					if (baseZoneIndex <= 0) {
						baseZoneIndex = pPO->m_currentZoneIndex.zoneIndex();
						zoneType = pPO->m_currentZoneIndex.GetType();
					}

					CWorldAvailableObj *wa = me->multiplayerObj()->m_currentWorldsUsed->GetByIndex(baseZoneIndex);
					if (wa != 0 && me->multiplayerObj()->m_currentWorldsUsed->validNewZoneName(name) && (zoneType == CI_ARENA || zoneType == CI_PERMANENT)) {
						query << "CALL create_ugc_zone_lite(%0, %1, %2q, %3, %4, %5, %6, %7, %8, %9, %10)";
						query.parse();

						getPlayerPosRot(pPO, x, y, z, rotation);

						LogTrace(query.str((int)baseZoneIndex, (int)zoneType, name, defaultOccupancy, !usePlayerPosAsSpawnPoint, x, y, z, rotation, (int)copyZoneIndex, (int)copyFlag));
						StoreQueryResult res = query.store((int)baseZoneIndex, (int)zoneType, name, defaultOccupancy, !usePlayerPosAsSpawnPoint, x, y, z, rotation, (int)copyZoneIndex, (int)copyFlag);

						// for SPs MUST call these to avoid disconnect
						checkMultipleResultSets(query, "UgcZoneLiteEvent::REQ_CREATE_ZONE");
						if (res.num_rows() == 1 && (long)res.at(0).at(0) != 0) {
							int newZoneIndex = (long)res.at(0).at(0);

							if (copyZoneIndex != 0) {
								Query queryWOPS = m_conn->query();

								int beginZtype = ZoneIndex(copyZoneIndex).GetType();

								for (int ztype = beginZtype; ztype < 6; ztype++) {
									queryWOPS.reset();
									queryWOPS << "INSERT INTO world_object_player_settings(id, world_object_id, asset_id, texture_url, zone_index, zone_instance_id, last_updated_datetime) "
											  << " SELECT "
											  << "  get_world_object_player_settings_id(),"
											  << "  world_object_id,"
											  << "  asset_id,"
											  << "  texture_url,"
											  << "  make_zone_index(" << newZoneIndex << ", " << ztype << "),"
											  << "  1,"
											  << "  NOW()"
											  << " FROM "
											  << "  world_object_player_settings"
											  << " WHERE "
											  << "  zone_index = make_zone_index(" << copyZoneIndex << ", " << ztype << ")";
									queryWOPS.execute();
								}
							}

							// add to memory
							me->multiplayerObj()->m_currentWorldsUsed->AddNewWorld(res.at(0).at(0), wa->m_worldName, name.c_str(), defaultOccupancy);
							me->multiplayerObj()->m_spawnPointCfgs->AddSpawnPoint(res.at(0).at(1),
								res.at(0).at(2), res.at(0).at(3), res.at(0).at(4), res.at(0).at(5),
								res.at(0).at(0), res.at(0).at(6));

							// send back the new id in a message
							std::string msg("New zone successfully created with index of ");
							msg += numToString(newZoneIndex, "%d");

							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyCreateZone(newZoneIndex, true, name.c_str(), msg.c_str());
							replyEvent = ugReply;

							success = true;
						} else {
							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyCreateZone(baseZoneIndex, false, name.c_str(), "Got zero zone_index from create");
							replyEvent = ugReply;
						}

					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyCreateZone(baseZoneIndex, false, name.c_str(), "Invalid new zone parameters");
						replyEvent = ugReply;
					}
				} break;

				case UgcZoneLiteEvent::REQ_DELETE_ZONE: {
					int zi = e->ObjectId();
					if (zi < 0) {
						zi = pPO->m_currentZoneIndex.zoneIndex();
					}

					int maintainObjects;
					(*e->InBuffer()) >> maintainObjects;

					CWorldAvailableObj *wa = me->multiplayerObj()->m_currentWorldsUsed->GetByIndex(zi);
					if (wa != 0) {
						query << "CALL delete_ugc_zone_lite(%0, %1)";
						query.parse();

						StoreQueryResult res = query.store(zi, maintainObjects);

						// for SPs MUST call these to avoid disconnect
						checkMultipleResultSets(query, "UgcZoneLiteEvent::REQ_DELETE_ZONE");

						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyDeleteZone(zi, true, "Zone deleted");
						replyEvent = ugReply;

						// need to delete in memory and spawn points, too
						me->multiplayerObj()->m_currentWorldsUsed->DeleteAndRemove(zi);
						me->multiplayerObj()->m_spawnPointCfgs->DeleteSpawnsPointByZoneIndex(zi);

						success = true;
					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyDeleteZone(zi, false, "Zone index not found");
						replyEvent = ugReply;
					}

				} break;

				case UgcZoneLiteEvent::REQ_RENAME_ZONE: {
					std::string newname;
					int zoneIndex;
					ugce->ExtractRenameZone(zoneIndex, newname);

					if (me->multiplayerObj()->m_currentWorldsUsed->validNewZoneName(newname)) {
						if (zoneIndex <= 0)
							zoneIndex = pPO->m_currentZoneIndex.zoneIndex();

						if ((zoneIndex > MAX_KANEVA_SUPPORTED_WORLD) || (zoneIndex == 55)) {
							query << "CALL rename_ugc_zone_lite(%0,%1q)";
							query.parse();
							LogTrace(query.str(zoneIndex, newname));
							StoreQueryResult res = query.store(zoneIndex, newname);

							// for SPs MUST call these to avoid disconnect
							checkMultipleResultSets(query, "UgcZoneLiteEvent::REQ_RENAME_ZONE");

							if (res.num_rows() == 1 && (long)res.at(0).at(0) == 1) {
								UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
								ugReply->SendReplyRenameZone(zoneIndex, true, newname, "Zone renamed");
								replyEvent = ugReply;
							} else {
								UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
								ugReply->SendReplyRenameZone(zoneIndex, false, newname, "Zone index not found");
								replyEvent = ugReply;
							}

							// update default zone url if we just renamed the default zone
							CWorldAvailableObj *wa = me->multiplayerObj()->m_currentWorldsUsed->GetByIndex(zoneIndex);
							if (wa) {
								CStringA thisZone = wa->m_displayName + ".zone";
								CStringA currentDefaultZone(me->GetDefaultPlaceUrl().c_str());

								if (thisZone.CompareNoCase(currentDefaultZone) == 0) {
									me->SetDefaultPlaceUrl(newname + ".zone");
								}
							}

							// rename in memory
							me->multiplayerObj()->m_currentWorldsUsed->UpdateDisplayName(zoneIndex, newname.c_str());
							success = true;
						} else {
							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyRenameZone(zoneIndex, false, newname, "Cannot rename Kaneva-supplied zones");
							replyEvent = ugReply;
						}
					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyRenameZone(zoneIndex, false, newname, "Invalid new zone name.");
						replyEvent = ugReply;
					}
				} break;

				case UgcZoneLiteEvent::REQ_SET_DEFAULT_ZONE: {
					int zi = e->ObjectId();
					if (zi <= 0)
						zi = pPO->m_currentZoneIndex.zoneIndex();

					// find it in zones list
					CWorldAvailableObj *wa = me->multiplayerObj()->m_currentWorldsUsed->GetByIndex(zi);
					if (wa) {
						me->SetDefaultPlaceUrl((const char *)(wa->m_displayName + ".zone"));

						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplySetDefaultZone(zi, true, "Default zone set");
						replyEvent = ugReply;
						success = true;
					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplySetDefaultZone(zi, false, "Zone index not found");
						replyEvent = ugReply;
					}
				} break;

				case UgcZoneLiteEvent::REQ_LIST_ZONES: {
					// figure out which zone is current default
					std::string defaultZone = me->GetDefaultPlaceUrl();
					STLToLower(defaultZone);
					std::string::size_type offset = defaultZone.find(".zone");
					if (offset != std::string::npos) {
						defaultZone = defaultZone.substr(0, offset);
					}

					//Get channel zones as available zones
					UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
					replyEvent = ugReply;
					ugReply->SendZones(me->multiplayerObj()->m_currentWorldsUsed->GetCount());
					for (POSITION pos = me->multiplayerObj()->m_currentWorldsUsed->GetHeadPosition(); pos != 0;) {
						CWorldAvailableObj *wa = (CWorldAvailableObj *)me->multiplayerObj()->m_currentWorldsUsed->GetNext(pos);
						(*ugReply->OutBuffer()) << wa->m_index << wa->m_displayName << wa->m_worldName << (wa->m_displayName.CompareNoCase(defaultZone.c_str()) == 0);
					}
					success = true;
				} break;

				case UgcZoneLiteEvent::REQ_LIST_SPAWNS: {
					// objectId has zoneIdex
					int zi = e->ObjectId();
					if (zi == 0)
						zi = pPO->m_currentZoneIndex.zoneIndex();

					UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
					replyEvent = ugReply;
					ugReply->SendSpawnPoints(zi, me->multiplayerObj()->m_spawnPointCfgs->SpawnPointsForZone(zi));
					for (POSITION pos = me->multiplayerObj()->m_spawnPointCfgs->GetHeadPosition(); pos != 0;) {
						CSpawnCfgObj *spawn = (CSpawnCfgObj *)me->multiplayerObj()->m_spawnPointCfgs->GetNext(pos);

						if (spawn->m_zoneIndex.zoneIndex() == zi) {
							(*ugReply->OutBuffer()) << spawn->m_id << spawn->m_spawnPointName
													<< spawn->m_position.x << spawn->m_position.y << spawn->m_position.z
													<< spawn->m_rotation;
						}
					}
					success = true;
				} break;

				case UgcZoneLiteEvent::REQ_ADD_SPAWN: {
					std::string name;
					int zoneIndex;
					float x, y, z;
					int rotation;
					bool usePlayerPos;
					ugce->ExtractAddSpawnPoint(zoneIndex, name, usePlayerPos, x, y, z, rotation);

					// name must be only alpha, no spaces
					if (me->multiplayerObj()->m_spawnPointCfgs->validNewSpawnName(name)) {
						if (usePlayerPos)
							getPlayerPosRot(pPO, x, y, z, rotation);

						if (zoneIndex <= 0)
							zoneIndex = pPO->m_currentZoneIndex.zoneIndex();

						query << "CALL add_ugc_zone_lite_spawn(%0, %1q, %2, %3, %4, %5)";
						query.parse();
						LogTrace(query.str(zoneIndex, name, x, y, z, rotation));
						StoreQueryResult res = query.store(zoneIndex, name, x, y, z, rotation);

						// for SPs MUST call these to avoid disconnect
						checkMultipleResultSets(query, "UgcZoneLiteEvent::REQ_ADD_SPAWN");

						if (res.num_rows() == 1) {
							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyAddSpawn(zoneIndex, true, res.at(0).at(0), name.c_str(), "Spawn point added");
							replyEvent = ugReply;

							// add the new spawn point to the list
							me->multiplayerObj()->m_spawnPointCfgs->AddSpawnPoint(res.at(0).at(0), x, y, z, rotation, zoneIndex, name.c_str());
							success = true;
						} else {
							// should always return if SP called w/o erros
							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyAddSpawn(zoneIndex, false, -1, name.c_str(), "Spawn point add failed");
							replyEvent = ugReply;
						}
					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyAddSpawn(zoneIndex, false, -1, name.c_str(), "Invalid spawn point name");
						replyEvent = ugReply;
					}
				} break;

				case UgcZoneLiteEvent::REQ_DELETE_SPAWN: {
					int spawnPtId;
					(*ugce->InBuffer()) >> spawnPtId;

					int zi = e->ObjectId();
					if (zi <= 0)
						zi = pPO->m_currentZoneIndex.zoneIndex();

					// make sure not the last one
					if (me->multiplayerObj()->m_spawnPointCfgs->SpawnPointsForZone(zi) > 1) {
						query << "CALL delete_ugc_zone_lite_spawn(%0, %1)";
						query.parse();

						StoreQueryResult res = query.store(zi, spawnPtId);

						// for SPs MUST call these to avoid disconnect
						checkMultipleResultSets(query, "UgcZoneLiteEvent::REQ_DELETE_SPAWN");

						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyDeleteSpawn(zi, true, spawnPtId, "Spawn point deleted");
						replyEvent = ugReply;

						me->multiplayerObj()->m_spawnPointCfgs->DeleteSpawnPointById(spawnPtId);

						success = true;
					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyDeleteSpawn(zi, false, spawnPtId, "Cannot delete last spawn point in zone.");
						replyEvent = ugReply;
					}
				} break;

				case UgcZoneLiteEvent::REQ_UPDATE_SPAWN: {
					int spawnId;
					int zoneIndex;
					std::string name;
					float x, y, z;
					int rotation;

					ugce->ExtractUpdateSpawnPoint(spawnId, zoneIndex, name, x, y, z, rotation);

					query << "CALL update_ugc_zone_lite_spawn(%0, %1, %2, %3, %4)";
					query.parse();

					StoreQueryResult res = query.store(spawnId, x, y, z, rotation);

					// for SPs MUST call these to avoid disconnect
					checkMultipleResultSets(query, "UgcZoneLiteEvent::REQ_UPDATE_SPAWN");

					if (res.num_rows() == 1) {
						CSpawnCfgObj *spawnObj = me->multiplayerObj()->m_spawnPointCfgs->FindId(spawnId);
						if (spawnObj) {
							Vector3f newPos = { x, y, z };
							spawnObj->m_position = newPos;
							spawnObj->m_rotation = rotation;

							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyUpdateSpawn(zoneIndex, true, spawnId, "Spawn point updated");
							replyEvent = ugReply;
							success = true;
						} else {
							UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
							ugReply->SendReplyUpdateSpawn(zoneIndex, false, spawnId, "Failed to update spawn point in memory.");
							replyEvent = ugReply;
						}
					} else {
						UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
						ugReply->SendReplyUpdateSpawn(zoneIndex, false, -1, "Failed to update spawn point in database.");
						replyEvent = ugReply;
					}
				} break;

				case UgcZoneLiteEvent::REQ_SET_INITIAL_ARENA: {
					int zi = e->ObjectId();
					if (zi <= 0)
						zi = pPO->m_currentZoneIndex.zoneIndex();

					me->SetInitialArenaZoneIndex(ZoneIndex(zi, 0, CI_ARENA));

					UgcZoneLiteEvent *ugReply = new UgcZoneLiteEvent();
					ugReply->SendReplySetInitialArena(zi, true, "Initial level set.");
					replyEvent = ugReply;
					success = true;
				} break;

				default:
					LogWarn("Unknown UgcZoneLite Filter " << ugce->Filter());
					return EVENT_PROC_RC::NOT_PROCESSED;
			}

			END_TRY_SQL();
		} catch (KEPException *ex) {
			if (pTrans) {
				pTrans->rollback();
				delete pTrans;
				pTrans = 0;
			}
			// log and send event to client letting them know if failed
			LogWarn("UgcLightAction of type " << e->Filter() << " failed: " << ex->ToString());
			delete ex;
			replyEvent = new RenderTextEvent("Operation failed.  Check server logs", eChatType::System);
		}
	}

	if (replyEvent != 0) {
		if (!isServer) {
			replyEvent->AddTo(e->From());
		}
		me->m_dispatcher.QueueEvent(replyEvent);
	}

	if (success) {
		return EVENT_PROC_RC::OK;
	} else {
		return EVENT_PROC_RC::CONSUMED;
	}
}

EVENT_PROC_RC MySqlGameState::GetItemBackgroundHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	jsVerifyReturn(lparam != 0, EVENT_PROC_RC::NOT_PROCESSED);
	MySqlGameState *me = (MySqlGameState *)lparam;
	if (jsFileExists("BACKGROUND_PLAYERINFO", 0))
		// work already handled on background thread.  Dummy it and return.
		return EVENT_PROC_RC::OK;
	return me->getPlayerInfo(e);
}

// static - handles updates to items from web
EVENT_PROC_RC MySqlGameState::ObjectTicklerEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	MySqlGameState *me = (MySqlGameState *)lparam;
	ObjectTicklerEvent *evt = dynamic_cast<ObjectTicklerEvent *>(e);
	if (!me || !evt)
		return EVENT_PROC_RC::NOT_PROCESSED;

	START_TIMING(me->m_timingLogger, "DB:ObjectTicklerEventHandler");

	auto ot = ObjectTicklerEvent::ObjectType::Invalid;
	auto ct = ObjectTicklerEvent::ChangeType::Invalid;

	if (!evt->ExtractInfo(ot, ct))
		return EVENT_PROC_RC::NOT_PROCESSED;

	LONG itemId = evt->ExtractNumericId();

	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;
	switch (ot) {
		case ObjectTicklerEvent::ObjectType::PlayerInventory: {
			// Refresh the bank inventory. Currently this is used when Kaneva web creates a template. All items in the template
			// are placed in storage (the bank). Refresh the player's bank items.
			CPlayerObject *pPO = me->m_server->GetPlayerObjectByServerId(itemId);
			if (!pPO)
				return EVENT_PROC_RC::NOT_PROCESSED;

			LogInfo("PlayerInventory - " << pPO->ToStr());
			try {
				me->RefreshInventory(pPO, false); //refresh both bank and active inventories
				ret = EVENT_PROC_RC::OK;

				//send an event to the player if they are online to refresh any open menus
				IDispatcher &dispatcher = *disp;
				IEvent *updateEvent = dispatcher.MakeEvent(dispatcher.GetEventId(INVENTORY_UPDATE_EVENT));
				updateEvent->AddTo(pPO->m_netId);
				dispatcher.QueueEvent(updateEvent);
			} catch (KEPException *e) {
				LogInfo("Player not found on this server for inventory refresh: " << itemId);
				e->Delete();
			}
		} break;

		case ObjectTicklerEvent::ObjectType::ItemPassList: {
			if (me->refreshItemPassList(ct, itemId))
				ret = EVENT_PROC_RC::OK;
		} break;

		case ObjectTicklerEvent::ObjectType::Item: {
			if (me->refreshItem(ct, itemId))
				ret = EVENT_PROC_RC::OK;
		} break;

		case ObjectTicklerEvent::ObjectType::PlayerPassList: {
			CPlayerObject *pPO = me->m_server->GetPlayerObjectByServerId(itemId);
			if (!pPO)
				return EVENT_PROC_RC::NOT_PROCESSED;

			LogInfo("PlayerPassList - " << pPO->ToStr());
			try {
				me->ReloadPlayer(pPO, PlayerMask::PASS_LIST);
				ret = EVENT_PROC_RC::OK;
			} catch (KEPException *e) {
				LogInfo("Player not found on this server for refresh: " << itemId);
				e->Delete();
			}
		} break;
	}

	return ret;
}

//! process refresh of item's passlist by reloading it from db
bool MySqlGameState::refreshItemPassList(ObjectTicklerEvent::ChangeType ct, LONG glid) {
	jsAssert(ct != ObjectTicklerEvent::ChangeType::Invalid);

	bool ret = false;

	// find the matching glid in global inventory and update the pass list
	// if not found, ignore it assuming it is a ugc item and will be loaded
	// later when it "faults"
	CGlobalInventoryObj *obj = m_server->GlobalInventoryDB()->GetByGLID(glid);
	if (obj == 0 || obj->m_itemData == 0)
		return true;

	// update all the pass lists on the item in global inventory from the db
	// this will cover adding, updating, or deleting a passlist on
	// the item
	// PUSHED DOWN TO MySqlBase
	MySqlBase::updateItemPassList(obj->m_itemData);

	// update all the inventory items
	m_server->UpdateItemInAllInventories(obj->m_itemData, ServerEngine::PASSLIST_UPDATE);

	return ret;
}

//! process refresh of item by reloading it from db
bool MySqlGameState::refreshItem(ObjectTicklerEvent::ChangeType ct, LONG glid) {
	jsAssert(ct != ObjectTicklerEvent::ChangeType::Invalid);

	bool ret = false;

	// find the matching glid in global inventory
	// if not found, ignore it assuming it is a ugc item and will be loaded
	// later when it "faults"
	CItemObj *obj = 0;

	if (ct == ObjectTicklerEvent::ChangeType::AddObject) {
		obj = new CItemObj;
	} else {
		CGlobalInventoryObj *inv = m_server->GlobalInventoryDB()->GetByGLID(glid);
		if (inv == 0 || inv->m_itemData == 0)
			return true;
		obj = inv->m_itemData;
	}

	// get the item from the database if not deleting it
	if (ct == ObjectTicklerEvent::ChangeType::UpdateObject) {
		// update the the item in global inventory from, db
		refreshItemData(glid, obj, false);

		// update all the inventory items
		m_server->UpdateItemInAllInventories(obj, ServerEngine::DATA_UPDATE);
	} else if (ct == ObjectTicklerEvent::ChangeType::DeleteObject) {
		// item is being deleted from global inventory

		// update all the inventory items
		m_server->UpdateItemInAllInventories(obj, ServerEngine::DELETE_ITEM);

		// remove it from the global inventory
		m_server->GlobalInventoryDB()->DeleteByGLID(glid);

		ret = true;
	}

	return ret;
}

EVENT_PROC_RC MySqlGameState::ShutdownEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	MySqlGameState *me = (MySqlGameState *)lparam;
	ShutdownEvent *evt = dynamic_cast<ShutdownEvent *>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(me->m_timingLogger, "DB: ShutdownEventHandler");

	EVENT_PROC_RC ret = me->serverStateChange(ssStopping) ? EVENT_PROC_RC::OK : EVENT_PROC_RC::NOT_PROCESSED;
	return ret;
}

static void sendBrowserReplyToClient(IDispatcher &disp, NETID client, ULONG retCode, long replyType, const char *msg) {
	IEvent *e = disp.MakeEvent(disp.GetEventId(BROWSER_REPLY_EVENT_NAME));
	jsVerifyDo(e != 0, return );

	e->SetFilter(retCode);
	(*e->OutBuffer()) << replyType << msg;
	e->AddTo(client);
	disp.QueueEvent(e);
}

EVENT_PROC_RC MySqlGameState::KgpUrlEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	MySqlGameState *me = (MySqlGameState *)lparam;

	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;

	std::string strParam;
	(*e->InBuffer()) >> strParam;

	static const char *ACCEPT_GIFT = "acceptGift";
	static const char *SEND_COD = "sendCOD";
	static const char *UPDATE_INVENTORY = "updInv";
	static const char *UPDATE_ITEM_URL = "changePlaylist";
	static const char *UPDATE_PASS_LIST = "passList";

	UrlParser url;
	url.parse(strParam.c_str(), false);
	if (strcmp(url.leftPart(), ACCEPT_GIFT) == 0) {
		me->acceptGift(e->From(), &url);
	}
#ifdef USING_COD // cash on deliver, async trade
	else if (strcmp(url.leftPart(), SEND_COD) == 0) {
		me->giftItems(e->From(), &url);
	}
#endif
	else if (strcmp(url.leftPart(), UPDATE_INVENTORY) == 0) {
		auto pPO = me->m_server->GetPlayerObjectByNetId(e->From(), true);
		if (pPO)
			me->UpdatePlayer(pPO, PlayerMask::INVENTORY);
	} else if (strcmp(url.leftPart(), UPDATE_ITEM_URL) == 0) {
		me->updateItemUrl(e->From(), &url);
	} else if (strcmp(url.leftPart(), UPDATE_PASS_LIST) == 0) {
		me->updatePassList(e->From(), &url);
	} else {
		sendBrowserReplyToClient(me->m_dispatcher, e->From(), E_INVALIDARG, REPLY_CLOSE_WINDOW, "Unhandled KGP URL");
		jsAssert(false); // don't handle this one
	}

	ret = EVENT_PROC_RC::OK;

	return ret;
}

void MySqlGameState::acceptGift(NETID netId, UrlParser *url) {
	START_TIMING(m_timingLogger, "DB:acceptGift");

	// get the giftId, add it to the player's inventory
	auto pPO = m_server->GetPlayerObjectByNetId(netId, true);
	if (pPO) {
		const char *msgId = url->hasValue("message");
		if (msgId == 0) {
			sendBrowserReplyToClient(m_dispatcher, netId, ERROR_NOT_FOUND, REPLY_CLOSE_WINDOW, "No gifts to accept!");
			jsAssert(false);
		} else if (atol(msgId) == 0) {
			sendBrowserReplyToClient(m_dispatcher, netId, ERROR_NOT_FOUND, REPLY_CLOSE_WINDOW, "No gifts to accept!");
			LogWarn("Accept gift has bad msgId " << msgId << " sent by player " << pPO->m_charName);
			jsAssert(false);
		} else {
			// figure out what item they're getting
			BEGIN_TRY_SQL();

			// first make sure they can hold the gift
			ULONG maxCharItems, maxBankItems;
			m_server->GetItemLimits(maxCharItems, maxBankItems);
			if (!pPO->m_inventory->CanTakeObjects(maxCharItems, 1)) {
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netId, eChatType::Talk, LS_QUEST_NO_INVENT_ROOM, pPO->m_charName);
				return;
			}

			Transaction trans(*m_conn);

			Query query = m_conn->query();

			query << "CALL accept3dGift( %0, %1 )";
			query.parse();

			StoreQueryResult res = query.store((int)((CAccountObject *)pPO->m_pAccountObject)->m_serverId, msgId);

			// for SPs MUST call these to avoid disconnect
			checkMultipleResultSets(query, "accept3dGift");

			if (res.num_rows() == 1) {
				// one row, two cols (globalId, fromId)
				Row r = res.at(0);
				int globalId = r.at(0);

				jsAssert(pPO->m_inventory != 0 && CGlobalInventoryObjList::GetInstance() != 0);
				if (pPO->m_inventory != 0) {
					CGlobalInventoryObj *pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(globalId);

					if (pGIO) {
						pPO->m_inventory->AddInventoryItem(1, FALSE, pGIO->m_itemData, IT_GIFT);
						// now update their inventory
						updatePlayerNoTrans(pPO, PlayerMask::INVENTORY);
						trans.commit();
						playerInvUpdateCommitted(pPO);
					} else {
						LogWarn("Bad GLID for gift item " << globalId << " for " << pPO->m_charName);
						trans.rollback();
					}
				}
			}

			sendBrowserReplyToClient(m_dispatcher, netId, NO_ERROR, REPLY_CLOSE_WINDOW, "Gift accepted to your inventory");

			END_TRY_SQL_LOG_ONLY(m_conn);
		}
	}
}

void MySqlGameState::giftItems(NETID netId, UrlParser *url) {
	jsAssert(false); // TODO: this is now cod_items
}

//! refresh the item's pass list from the database
void MySqlGameState::updateItemPassList(CItemObj *item) {
	jsVerifyReturnVoid(item && item->m_globalID > 0);
	Query query = m_conn->query();

	// get pass lists for item, if exist (added 8/08)
	query.reset();
	query << " SELECT pass_group_id from pass_group_items "
		  << " WHERE global_id = %0 ";
	query.parse();

	// wipe pass list
	if (item->m_passList) {
		DELETE_AND_ZERO(item->m_passList);
	}

	LogTrace(query.str((int)item->m_globalID));
	StoreQueryResult results = query.store((int)item->m_globalID);
	if (results.num_rows() > 0) {
		if (item->m_passList == 0)
			item->m_passList = new PassList;

		for (UINT i = 0; i < results.num_rows(); i++) {
			item->m_passList->addPass(atoi(results.at(i)[0]));
		}
	}
}

void MySqlGameState::updateItemUrl(NETID netId, UrlParser *url) {
	START_TIMING(m_timingLogger, "DB:updateItemUrl");

	jsVerifyReturnVoid(url != 0);

	// changePlaylist?objId=0&url=http://localhost/Kaneva/services/omm/request.aspx%3ftype%3d5%26plId%3d49%26fromGame%3d1
	// 5/07 now pass in playlist id
	// changePlaylist?objId=0&plId=833
	// 7/07 now can pass url, params for more generic ASX/SWF
	// changePlaylist?objId=0&url=ddd.swf&params= (this is generic, wide-open for testing)
	// changePlaylist?objId=0&assetId=ddd (get the value from assets_base)

	const char *objIdStr = url->hasValue("objId");
	const char *playlistIdStr = url->hasValue("plId");
	const char *assetIdStr = url->hasValue("assetId");
	const char *matureStr = url->hasValue("mature");
	const char *assetTypeStr = url->hasValue("asset_type");
	const char *stopPlayingStr = url->hasValue("stop");
	const char *itemUrlStr = url->hasValue("url");

	// DRF - Added - 8/15 - mediaParams
	const char *mUrl = url->hasValue("offsite_url");
	const char *mParams = url->hasValue("params");

	const char *mVolPct = url->hasValue("volPct");
	const char *mRadAud = url->hasValue("radAud");
	const char *mRadVid = url->hasValue("radVid");

	auto pPO = m_server->GetPlayerObjectByNetId(netId, true);
	if (!pPO) {
		LogError("GetPlayerByNetId() FAILED - netId=" << netId << " objId=" << NULL_CK(objIdStr) << " assetId=" << NULL_CK(assetIdStr));
		return;
	}

	int objPlacementId = 0;
	if (objIdStr)
		objPlacementId = atol(objIdStr);

	MediaParams mediaParams("", "", -1, -1, -1);
	if (mUrl)
		mediaParams.SetUrl(mUrl);
	if (mParams)
		mediaParams.SetParams(mParams);
	if (mVolPct)
		mediaParams.SetVolume(atof(mVolPct));
	if (mRadAud)
		mediaParams.SetRadiusAudio(atof(mRadAud));
	if (mRadVid)
		mediaParams.SetRadiusVideo(atof(mRadVid));
	if (playlistIdStr)
		mediaParams.SetPlaylistId(atol(playlistIdStr));

	int assetId = 0, assetType = 0, stopPlay = 0;
	if (assetIdStr)
		assetId = atol(assetIdStr);
	if (assetTypeStr)
		assetType = atol(assetTypeStr);
	if (stopPlayingStr)
		stopPlay = atol(stopPlayingStr);

	updateMediaPlayer(pPO->m_currentZoneIndex, netId, pPO, objPlacementId, mediaParams, playlistIdStr != NULL, assetId, assetType, itemUrlStr, matureStr, stopPlay);
}

void MySqlGameState::updateMediaPlayer(
	const ZoneIndex &zoneIndex, NETID netId, const CPlayerObject *player, int objPlacementId, const MediaParams &mediaParams,
	bool usePlaylistId, int assetId /*= 0*/, int assetType /*= 0*/, const char *itemUrl /*= NULL*/, const char *mature /*= NULL*/, int stopPlay /*= 0*/) {
	// Update player volume and radii settings if specified
	if (mediaParams.GetVolume() != -1 || mediaParams.GetRadiusAudio() != -1 || mediaParams.GetRadiusVideo() != -1) {
		if (objPlacementId == 0) {
			LogError("objId must be provided to update media player radii or volume");
		} else {
			updateMediaPlayerVolumeAndRadius(zoneIndex, netId, player, objPlacementId, mediaParams);
		}
	}

	// Update playlist or track
	ZoneCustomizationBackgroundEvent *zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_ITEM_MEDIA;
	zce->mediaParams = mediaParams;
	zce->SetFrom(netId);
	zce->zoneIndex = zoneIndex;
	if (player) {
		zce->playerIsGm = (player->m_gm != FALSE);
		zce->playerId = player->m_handle;
		zce->channel = player->m_currentChannel;
		zce->networkAssignedId = player->m_netTraceId;
	}
	zce->assetId = assetId;
	zce->objPlacementId = objPlacementId;
	if (usePlaylistId) {
		zce->playlistId = zce->mediaParams.GetPlaylistId();
	}
	zce->assetType = assetType;
	if (itemUrl)
		zce->itemUrl = itemUrl;
	if (mature)
		zce->mature = mature;
	zce->stopPlay = stopPlay;

	m_server->GetGameStateDispatcher()->QueueEvent(zce);
}

void MySqlGameState::updateMediaPlayerVolumeAndRadius(const ZoneIndex &zoneIndex, NETID netId, const CPlayerObject *player, int objPlacementId, const MediaParams &mediaParams) {
	ZoneCustomizationBackgroundEvent *zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_MEDIA_VOL_RAD;

	zce->SetFrom(netId);
	zce->zoneIndex = zoneIndex;
	zce->objPlacementId = objPlacementId;
	if (player) {
		zce->playerIsGm = player->m_gm != FALSE;
		zce->playerId = player->m_handle;
		zce->channel = player->m_currentChannel;
		zce->networkAssignedId = player->m_netTraceId;
	} else {
		// Otherwise this is a script server request
		// TODO: double check if some player field are actually needed
	}
	zce->mediaParams = mediaParams;

	m_server->GetGameStateDispatcher()->QueueEvent(zce);
}

void MySqlGameState::updatePassList(NETID netId, UrlParser *url) {
	START_TIMING(m_timingLogger, "DB:updatePassList");

	if (!url)
		return;

	const char *add = url->hasValue("add");
	const char *del = url->hasValue("del");

	if (add != 0 || del != 0) {
		auto pPO = m_server->GetPlayerObjectByNetId(netId, true);
		if (!pPO)
			return;

		long zoneIndex = pPO->m_currentZoneIndex.getClearedInstanceId();
		long instanceId = pPO->m_currentZoneIndex.GetInstanceId();

		// update the database
		BEGIN_TRY_SQL();

		if (pPO->m_currentZonePriv > ZP_OWNER) {
			m_server->SendTextMessage(pPO, "Must be owner/gm", eChatType::System);
			return;
		}

		long passId = 0;
		if (add) {
			passId = atol(add);
			// make sure they have the pass
			if (pPO->m_passList == 0 || !pPO->m_passList->hasActivePass(passId)) {
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS);
				return;
			}
		} else {
			// removing pass, not check needed
			passId = atol(del);
		}

		Transaction trans(*m_conn);

		Query query = m_conn->query();

		// ret is 0 for ok, else error code
		query << "CALL updatePassList( %0, %1, %2, %3, @ret ); select @ret;";
		query.parse();
		LogTrace(query.str(add != 0 ? 1 : 0, (int)zoneIndex, (int)instanceId, (int)passId));
		StoreQueryResult res = query.store(add != 0 ? 1 : 0, (int)zoneIndex, (int)instanceId, (int)passId);

		res = query.store_next();

		if (res.num_rows() > 0) {
			long errorCode = res.at(0).at(0);
			switch (errorCode) {
				case 0: {
					trans.commit();

					// update the in-memory version, too
					PassList *updatedPassList = 0;
					PassListMap::iterator i = m_passLists.find(pPO->m_currentZoneIndex.toLong());
					if (i != m_passLists.end()) {
						if (add) {
							if (i->second.first == 0)
								i->second.first = new PassList();
							updatedPassList = i->second.first;
							i->second.first->addPass(passId);
						} else if (i->second.first != 0) // deleting
						{
							i->second.first->removePass(passId);
							if (i->second.first->empty()) {
								delete i->second.first;
								m_passLists.erase(i);
							}
						}
					} else if (add) {
						updatedPassList = new PassList;
						updatedPassList->addPass(passId);

						m_passLists[pPO->m_currentZoneIndex.toLong()] = std::pair<PassList *, ULONG>(updatedPassList, fTime::TimeMs());
					}

					// tell server to kick everyone from zone, 'cept this guy
					m_server->KickEveryoneExcept(pPO->m_currentZoneIndex, pPO->m_currentChannel,
						0,
						// now always re-zone everyone since nonAP zone won't show AP items
						// old comment -- if deleting even send owner how in case wearing something not allowed
						LS_UPDATING_ZONE_PASS);

					m_dispatcher.QueueEvent(new PassListEvent(pPO->m_netId,
						pPO->m_passList ? &pPO->m_passList->ids() : 0,
						pPO->m_passList ? &pPO->m_passList->expirations() : 0,
						updatedPassList ? &updatedPassList->ids() : 0));
				} break;
				case 2:
					trans.rollback();
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_CANT_REMOVE_PASS);
					break;
				default:
					trans.rollback();
					LogWarn("Error updating zone pass for " << numToString(zoneIndex) << ":" << instanceId << " " << errorCode);
					break;
			}
		}
		END_TRY_SQL();
	}
}

// changing to ssStarting or ssStopping will mark all user records as logged off for this server
// used on startup and shutdown
bool MySqlGameState::serverStateChange(EServerState newState) {
	bool ret = false;

	if (!checkServerId("serverStateChange", false))
		return false;

	if (multiplayerObj() == 0 || multiplayerObj()->m_runtimeAccountDB == 0)
		return false; // not ready yet.

	// Init local ping (for WOK servers only)
	if (m_localPing && !m_localPingInited && newState == ssRunning && m_serverId != 0 && multiplayerObj()->m_serverNetwork->pinger().GetParentGameId() == 0) {
		BEGIN_TRY_SQL_FOR("serverStateChange.localPingInit");

		ULONG schemaVersion = 0;
		if (multiplayerObj()->m_gameStateDb) {
			schemaVersion = (ULONG)multiplayerObj()->m_gameStateDb->SchemaVersion();
		}

		std::string serverVersion = Algorithms::GetModuleVersion();

		Query query = m_conn->query();
		query << "UPDATE developer.game_servers SET ip_address=%1q, protocol_version=%2, schema_version=%3, asset_version=%4, server_engine_version=%5q WHERE server_id=%0";
		query.parse();
		LogTrace(query.str((int)m_serverId, (const char *)multiplayerObj()->getIPAddress().c_str(),
			(int)multiplayerObj()->GetVersion(), (int)schemaVersion, (int)multiplayerObj()->m_contentVersion, serverVersion.c_str()));
		query.execute((int)m_serverId, (const char *)multiplayerObj()->getIPAddress().c_str(),
			(int)multiplayerObj()->GetVersion(), (int)schemaVersion, (int)multiplayerObj()->m_contentVersion, serverVersion.c_str());
		m_localPingInited = true;

		END_TRY_SQL();
	}

	{
		BEGIN_TRY_SQL();

		Transaction trans(*m_conn);

		Query query = m_conn->query();

		query << "CALL server_changed_state_local_ping( %0, %1q, %2, %3, %4, %5 )";
		query.parse();

		query.execute((int)m_serverId, m_serverName.c_str(), (int)m_serverPort, (int)newState,
			(int)multiplayerObj()->m_runtimeAccountDB->GetCount(), (int)(m_localPing ? 1 : 0)); // also has AI, Relay servers, so may be a few higher

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "serverStateChange");
		trans.commit();
		ret = true;

		END_TRY_SQL();
	}
	return ret;
}

MySqlGameState::MySqlGameState(IDispatcher &disp, ServerEngine *se, CMultiplayerObj *cmp, const char *logName, const char *timingLogName, const DBConfig &dbConfig) :
		// Give serverengine access to MySqlEventProcessor
		MySqlBase(se, logName, timingLogName, dbConfig),
		m_dispatcher(disp),
		m_updateThd(0),
		m_maxPlayersInBackupLoop(3),
		m_backupSleepTime(3000),
		m_maxBackupTimeMs(500),
		m_serverInfo(0),
		m_localPing(false),
		m_localPingInited(false),
		m_pingInterval(3 * 60 * 1000),
		m_lastServerInfoUpdate(0),
		m_schemaVersion(0),
		m_backgroundInterval(3 * 60 * 1000),
		m_initialArenaZoneIndex(0) {
	m_serverName = m_server->ipAddress();
	m_serverPort = m_server->port();
	// just returns ptr that leaks... m_passLists.get_allocator().allocate(1000);
}

MySqlGameState::~MySqlGameState() {
	m_localPing = false; // stop pingging in
	try {
		if (m_conn != 0 && m_conn->connected())
			saveGetSetMetaDataValues();

		if (m_updateThd) {
			m_updateThd->stop(3000); // let it cleanup itself
			if (!m_updateThd->isRunning()) {
				delete m_updateThd; // else it leaks, but probably exiting anyway
			}
		}
	} catch (KEPException *e) {
		LogWarn(loadStr(IDS_ERROR_SAVING_GETSET) << e->m_msg);
		e->Delete();
	}

	for (PassListMap::iterator i = m_passLists.begin(); i != m_passLists.end(); i++) {
		if (i->second.first)
			delete i->second.first;
	}

	if (m_conn != 0)
		delete m_conn;

	if (m_serverInfo)
		delete[] m_serverInfo;

	PassLists::DeInitialize();
}

// load the supported worlds and spawn point from the database
bool MySqlGameState::loadSupportedWorlds() {
	bool ret = false;

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "SELECT zone_index, name, display_name, channel_id, default_max_occupancy FROM supported_worlds";

	TRACE_QUERY(query, m_logger);

	StoreQueryResult res = query.store();
	if (res.num_rows() > 0) {
		multiplayerObj()->m_currentWorldsUsed->SafeDelete();
		multiplayerObj()->m_spawnPointCfgs->SafeDelete();

		for (UINT i = 0; i < res.num_rows(); i++) {
			Row row = res.at(i);
			CWorldAvailableObj *wa = new CWorldAvailableObj();
			wa->m_index = row.at(0);
			wa->m_worldName = row.at(1);
			wa->m_displayName = row.at(2);
			wa->m_channel = ChannelId((long)row.at(3));
			wa->m_maxOccupancy = row.at(4);
			// visibility not used now, let have default value.

			multiplayerObj()->m_currentWorldsUsed->AddTail(wa);
		}
		query.reset();
		query << "SELECT spawn_point_id, position_x, position_y, position_z, rotation, zone_index, name FROM spawn_points ORDER BY spawn_point_id";
		TRACE_QUERY(query, m_logger);

		res = query.store();
		for (UINT i = 0; i < res.num_rows(); i++) {
			Row row = res.at(i);

			// validate that we have a zone for it,
			// spawn points can be plain, typed, or instanced
			ZoneIndex zi((long)row.at(5));

			if (multiplayerObj()->m_currentWorldsUsed->GetByIndex(zi.zoneIndex())) {
				CSpawnCfgObj *sp = new CSpawnCfgObj();
				sp->m_id = row.at(0);
				sp->m_position.x = row.at(1);
				sp->m_position.y = row.at(2);
				sp->m_position.z = row.at(3);
				sp->m_rotation = row.at(4);
				sp->m_zoneIndex = zi;
				sp->m_spawnPointName = row.at(6);
				multiplayerObj()->m_spawnPointCfgs->AddTail(sp);
			} else {
				LogWarn("Spawn point is missing a zone " << (std::string)row.at(5));
			}
		}

		bool found = false;
		// now validate that every zone has at least one spawn point
		for (POSITION pos = multiplayerObj()->m_currentWorldsUsed->GetHeadPosition(); pos != 0;) {
			found = false;

			CWorldAvailableObj *wa = (CWorldAvailableObj *)multiplayerObj()->m_currentWorldsUsed->GetNext(pos);
			POSITION pos2 = multiplayerObj()->m_spawnPointCfgs->GetHeadPosition();
			for (; pos2 != 0;) {
				CSpawnCfgObj *sp = (CSpawnCfgObj *)multiplayerObj()->m_spawnPointCfgs->GetNext(pos2);
				if (sp->m_zoneIndex.zoneIndex() == wa->m_index) {
					found = true;
					break;
				}
			}
			if (!found) {
				LogWarn("Zone missing a spawn point " << wa->m_worldName);
			}
		}

		LogInfo("Loaded "
				<< multiplayerObj()->m_currentWorldsUsed->GetCount() << " zones and "
				<< multiplayerObj()->m_spawnPointCfgs->GetCount() << " spawn points ");
	} else {
		LogError("No zones loaded from database");
	}

	END_TRY_SQL();
	return ret;
}

// load the server id mapping from the database
// this is called on init, and if can't find a server
// but only if hasn't been loaded w/i last min
// the view is game-specific, and returns only servers for this game
bool MySqlGameState::loadServerIds() {
	bool ret = false;

	const TimeMs UPDATE_FREQ = 60000;

	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_lastServerInfoUpdate) > UPDATE_FREQ) {
		BEGIN_TRY_SQL();

		m_lastServerInfoUpdate = timeMs;

		Query query = m_conn->query();
		query << "SELECT server_id, ip_address, port FROM vw_game_servers";
		TRACE_QUERY(query, m_logger);

		StoreQueryResult res = query.store();

		if (m_serverInfo) {
			delete[] m_serverInfo;
			m_serverInfo = 0;
		}
		if (res.num_rows() > 0) {
			m_serverInfo = new ServerInfo[((long)res.num_rows()) + 1];
			int j = 0;
			for (UINT i = 0; i < res.num_rows(); i++) {
				Row row = res.at(i);
				long s, p;
				s = row.at(0);
				p = row.at(2);
				std::string ip(row.at(1));
				if (s != 0 && p != 0 && ip.size() > 0) {
					m_serverInfo[j].serverId = s;
					m_serverInfo[j].ipAddress = row.at(1);
					m_serverInfo[j].port = p;
					j++;
				}
			}
			m_serverInfo[j].port = 0;
			m_serverInfo[j].serverId = 0;
		}

		ret = true;

		END_TRY_SQL();
	}

	return ret;
}

// similar in functionality to SerializeMetaDataFromXml
void MySqlGameState::loadGetSetMetaDataValues() {
	BEGIN_TRY_SQL();

	getGetSetMetaData();

	END_TRY_SQL();
}

// save the meta data part of the dynamically addded GetSet values for players
void MySqlGameState::saveGetSetMetaDataValues() {
	BEGIN_TRY_SQL();

	CPlayerObject p;
	p.m_handle = 0; // store these on one never used in DB

	Transaction trans(*m_conn);
	updateGetSet(&p, true);
	trans.commit();

	END_TRY_SQL();
}

// thread for doing background updates of players
class BackgroundUpdateThd : public jsThread {
public:
	BackgroundUpdateThd(MySqlGameState *parent, const char *gameDir) :
			jsThread("DBUpdateThd"), m_parent(parent), m_gameDir(gameDir) {}

protected:
	// background thd to update db
	ULONG _doWork() {
		DWORD ret = 0;

		// first init mysql for multi-threading
		if (mysql_thread_init() != 0) {
			LogError("Error initializing MySQL threading");
			return 0;
		}

		MySqlGameState thdState(m_parent->m_dispatcher, m_parent->m_server, m_parent->multiplayerObj(), "DBBackgroundThd", "Timing", m_parent->m_dbConfig);
		try {
			thdState.initializeState(m_gameDir.c_str());
		} catch (KEPException *e) {
			LogError("Error initializing BackgroundUpdateThd " << e->m_msg);
			e->Delete();
			return 0;
		}

		TimeMs backupTimeStamp = fTime::TimeMs();

		Timer timerPing;

		static const int MAX_FAILURES = 5;

		ULONG arraySize;
		ULONG prevArraySize = 0;
		CPlayerObject **playersToBackup = 0;

		LogInfo("Background DB thread started using the following values:");
		LogInfo("   LocalPing:              " << m_parent->m_localPing);
		LogInfo("   PingInterval:           " << FMT_TIME << m_parent->m_pingInterval);
		LogInfo("   BackgroundInterval:     " << FMT_TIME << m_parent->m_backgroundInterval);
		LogInfo("   EnableAutoBackup:       " << m_parent->multiplayerObj()->m_enableAutoBackup);
		LogInfo("   MaxBackupTimeMs:        " << FMT_TIME << thdState.m_maxBackupTimeMs);
		LogInfo("   MaxPlayersInBackupLoop: " << thdState.m_maxPlayersInBackupLoop);
		LogInfo("   BackupSleepTime:        " << FMT_TIME << thdState.m_backupSleepTime);
		LogInfo("   MaxPlayerBackupTimeMs:  " << FMT_TIME << thdState.multiplayerObj()->m_maxPlayerBackupTimeMs);

		try {
			// use this thread to send ssStarting once we ping in
			while (!isTerminated() && !thdState.serverStateChange(ssStarting))
				fTime::SleepMs(500);
		} catch (KEPException *e) {
			LogError("Error on initial serverStateChange background thread stopping: " << e->m_msg);
			e->Delete();
			return 0;
		}

		// does background thread need to continue?
		if (m_parent->m_localPing || m_parent->multiplayerObj()->m_enableAutoBackup) {
			while (!isTerminated()) {
				TimeMs sleepTime = m_parent->m_pingInterval;

				if (!m_parent->m_localPing)
					sleepTime = m_parent->m_backgroundInterval;

				// check ping first
				if (m_parent->m_localPing) {
					if (timerPing.ElapsedMs() >= m_parent->m_pingInterval) {
						try {
							timerPing.Reset();
							thdState.serverStateChange(ssRunning);
						} catch (KEPException *e) {
							e->Delete(); // call will log msg, do nothing here
						}
					}
				}

				if (m_parent->multiplayerObj()->m_enableAutoBackup) {
					// could change via KEPAdmin, so check it
					arraySize = m_parent->m_maxPlayersInBackupLoop;
					if (prevArraySize != arraySize) {
						if (playersToBackup != 0)
							delete playersToBackup;

						playersToBackup = new CPlayerObject *[arraySize];
					}
					memset(playersToBackup, 0, sizeof(CPlayerObject *) * arraySize);

					backupTimeStamp = fTime::TimeMs();

					CPlayerObjectList *players = thdState.multiplayerObj()->m_runtimePlayersOnServer;

					{ // lock scope

						// locks in constructor
						// HIGH-LEVEL CMP LOCK!, protects pos
						CSLock lock(&thdState.multiplayerObj()->m_criticalSection);

						//ULONG totalPlayers = players->GetCount();
						ULONG potentialupdates = 0;
						ULONG updatedPlayers = 0;

						// get folks who haven't been updated for longest time
						ULONG fillCount = 0;
						TimeMs thresholdTime = fTime::ElapsedMs(thdState.multiplayerObj()->m_maxPlayerBackupTimeMs);
						for (POSITION pos = players->GetHeadPosition(); pos != NULL;) {
							CPlayerObject *currentPlayer = (CPlayerObject *)players->GetNext(pos);

							// only consider those older than maxBackupTime, etc.
							if (!currentPlayer->m_aiControlled && (currentPlayer->m_handle >= MIN_DATABASE_PLAYER_HANDLE) && (currentPlayer->m_timeDbUpdate < thresholdTime) && (currentPlayer->m_dbDirtyFlags != 0)) {
								potentialupdates++;

								// put oldest first
								for (ULONG i = 0; i < arraySize; i++) {
									if (playersToBackup[i] == 0) {
										fillCount++;
										playersToBackup[i] = currentPlayer;
										break;
									} else if (currentPlayer->m_timeDbUpdate < playersToBackup[i]->m_timeDbUpdate) {
										// push out others to insertion sort
										for (ULONG j = arraySize - 1; j > i; j--) {
											playersToBackup[j] = playersToBackup[j - 1];
										}
										playersToBackup[i] = currentPlayer;
										break;
									}
								}
							}
						}

						// figured out who, now do them
						for (ULONG i = 0; i < arraySize; i++) {
							CPlayerObject *currentPlayer = playersToBackup[i];
							if (currentPlayer == 0)
								continue;

							TimeMs elapsedTime = fTime::ElapsedMs(backupTimeStamp);
							if (elapsedTime > thdState.m_maxBackupTimeMs) {
								LogWarn("TOO LONG - " << FMT_TIME << elapsedTime << "ms - Skipping " << arraySize - i << " items");
								break;
							}

							if (currentPlayer->m_pAccountObject != 0) {
								if (currentPlayer->m_pAccountObject->m_sentLogoff)
									continue;
							}

							try {
								// update using player dirty flags
								// no longer track | IGameState::CURRENT_POSITION | IGameState::GETSETS
								thdState.updatePlayer(currentPlayer, currentPlayer->m_dbDirtyFlags, false);
								updatedPlayers++;
							} catch (KEPException *e) {
								currentPlayer->m_timeDbUpdate = fTime::TimeMs() + 60.0 * 1000.0; // avoid trying this again soon
								lock.unlock(); // don't log in lock
								LogWarn(loadStrPrintf(IDS_BACKUP_EXCEPTION, currentPlayer->m_charName).c_str() << e->m_msg);
								e->Delete();
								break; // get new POS on next pass
							}
						}
						lock.unlock();
					} // lock scope

					sleepTime = m_parent->m_backupSleepTime;
				}

				fTime::SleepMs(sleepTime);
			}
		}

		if (playersToBackup != 0)
			delete playersToBackup;

		LogInfo("Backup thread exiting");

		mysql_thread_end();

		return ret;
	}

	MySqlGameState *m_parent;
	std::string m_gameDir;
};

void MySqlGameState::loadPassLists() {
	BEGIN_TRY_SQL();

	PassLists::Initialize();

	Query query = m_conn->query();

	query << "SELECT pass_group_id, flags FROM pass_groups";
	TRACE_QUERY(query, m_logger);

	StoreQueryResult res = query.store();

	for (UINT i = 0; i < res.num_rows(); i++) {
		Row row = res.at(i);
		PassLists::UpdatePassList(row.at(0), row.at(1));
	}

	END_TRY_SQL();
}

#define OPTIMIZE_CLANLOAD
void MySqlGameState::loadClanDatabase() {
	LogInfo("Start load Clan Db ");
	Timer timer;

	BEGIN_TRY_SQL();

	multiplayerObj()->m_clanDatabase->SafeDelete();

	Transaction trans(*m_conn);
	Query query = m_conn->query();

	query << "SELECT clan_id, 1 rec_type, owner_id as player_id, clan_name as name FROM clans "
		  << "union "
		  << "SELECT clan_id, 2 rec_type, player_id, name FROM vw_clan_members "
		  << "order by clan_id, rec_type";

	TRACE_QUERY(query, m_logger);

	UseQueryResult res = query.use();

	CClanSystemObj *newClan = 0;
	while (Row row = res.fetch_row()) {
		int clanId = atol(row.at(0));
		int recType = atol(row.at(1));
		int playerId = atol(row.at(2));
		const char *pcname = (const char *)row.at(3);

		if (recType == 1) {
			// Following call to CreateClan returns one of the following three values:
			//
			// 1 success
			// 2 dupe           which will never happen here as we instruct CreateClan() to not
			//                  perform a duplicate check
			// 3 invalid name   too short (<=0)or too long (>35)
			//
			if (multiplayerObj()->m_clanDatabase->CreateClan(pcname, pcname, playerId, &newClan, false) != 1)
				continue;
			newClan->m_clanId = clanId;
		} else {
			// Database constraints insure that there will be a clan record for this clan member
			// (e.g. first tuple in result set is member record such that no clan object
			// is created and we get a NPE here.
			//
			newClan->AddMemberByHandle(playerId, pcname, false);
		}
	}
	END_TRY_SQL();
	timer.Pause();
	LogInfo("Time to load Clan Db: " << timer.ElapsedMs());
}

/**
* Load the "items" from the database and overwrite the
* applicable global inventory fields for that item. The global
* inventory was initially read from the file system's GIL. This
* GIL was also exported to the database by the Editor during
* distribution, but, the web's csr tool can make changes to
* these items.
*/
void MySqlGameState::loadItemDatabase() {
	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	// Get all published items from the Editor. This excludes UGC items.
	query << "SELECT i.global_id, name, 0, market_cost, selling_price, required_skill, required_skill_level, use_type, "
			 " IFNULL(value,0) as use_value, disarmable, stackable, destroy_when_used, inventory_type, custom_texture, expired_duration, permanent, "
			 " is_default, visual_ref "
			 " FROM items i LEFT OUTER JOIN item_parameters ip ON i.global_id = ip.global_id"
			 " WHERE i.global_id < %0";

	query.parse();
	LogTrace(query.str((unsigned int)SGI_BASE_GLID));
	StoreQueryResult res = query.store((unsigned int)SGI_BASE_GLID);
	LogInfo("InvTrack loadItemDatabase loaded[" << res.num_rows() << "] items from database");

	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();

	for (UINT ii = 0; ii < res.num_rows(); ii++) {
		if (globalDBRef != 0) {
			Row row = res.at(ii);
			int glid = row.at(0);

			CGlobalInventoryObj *inventoryObj = (CGlobalInventoryObj *)globalDBRef->GetByGLID(glid);

			if (inventoryObj) {
				inventoryObj->m_itemData->m_itemName = row.at(1);
				//inventoryObj->m_itemData->m_itemDescription    = row.at(2);
				inventoryObj->m_itemData->m_marketCost = row.at(3);
				inventoryObj->m_itemData->m_sellingPrice = row.at(4);
				inventoryObj->m_itemData->m_requiredSkillType = row.at(5);
				inventoryObj->m_itemData->m_requiredSkillLevel = row.at(6);
				inventoryObj->m_itemData->m_useType = (USE_TYPE)(int)row.at(7);
				inventoryObj->m_itemData->m_miscUseValue = row.at(8);
				inventoryObj->m_itemData->m_disarmable = row.at(9);
				inventoryObj->m_itemData->m_stackable = row.at(10);
				inventoryObj->m_itemData->m_destroyOnUse = row.at(11);
				inventoryObj->m_itemData->m_itemType = row.at(12);
				inventoryObj->m_itemData->m_customTexture = row.at(13);
				inventoryObj->m_itemData->m_expiredDuration = row.at(14);
				inventoryObj->m_itemData->m_permanent = row.at(15);
				inventoryObj->m_itemData->m_defaultArmedItem = row.at(16);
				inventoryObj->m_itemData->m_visualRef = row.at(17);

				// remove passlists since db will override values from GIL
				if (inventoryObj->m_itemData->m_passList) {
					DELETE_AND_ZERO(inventoryObj->m_itemData->m_passList);
				}
			} else {
				LogError("InvTrack loadItemDatabase found glid:" << glid << " in database not represented in GIL.");
			}
		} else {
			LogError("loadItemDatabase could not access global inventory list.");
		}
	}
	// now get all passlists in one query to avoid thousands of db calls
	query.reset();

	// get pass lists for non-ugc items, ugc loaded on demand
	query << " SELECT global_id, pass_group_id from pass_group_items where global_id < " << ((unsigned int)SGI_BASE_GLID);

	TRACE_QUERY(query, m_logger);
	res = query.store();
	for (UINT i = 0; i < res.num_rows(); i++) {
		CGlobalInventoryObj *inventoryObj = (CGlobalInventoryObj *)globalDBRef->GetByGLID(res.at(i)[0]);

		if (inventoryObj && inventoryObj->m_itemData) {
			CItemObj *item = inventoryObj->m_itemData;
			if (item->m_passList == 0) {
				item->m_passList = new PassList;
			}

			if (!item->m_passList->hasPass(res.at(i)[1])) {
				item->m_passList->addPass(res.at(i)[1]);
			}
		} else {
			LogError("InvTrack loadItemDatabase pass lists found glid:" << (long)res.at(i)[0] << " in database not represented in GIL.");
		}
	}

	END_TRY_SQL();
}

void MySqlGameState::LoadScriptServerItemAvailability() {
	if (multiplayerObj()->m_serverNetwork) {
		LogInfo("InvTrack LoadScriptServerItemAvailability received request to load SSAI");
		ScriptAvailableItemsBackgroundEvent *siabe = new ScriptAvailableItemsBackgroundEvent();
		m_server->GetGameStateDispatcher()->QueueEvent(siabe);
	}
	return;
}

void MySqlGameState::loadAnimations() {
	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "SELECT p2p_animation_id, initiator_anim_index, participant_anim_index, name, runtime, looping FROM p2p_animations";
	TRACE_QUERY(query, m_logger);

	StoreQueryResult res = query.store();

	int p2pIndex = 0;
	for (UINT i = 0; i < res.num_rows(); i++) {
		Row row = res.at(i);

		P2pAnimData p2pAnimData;
		p2pIndex = row.at(0);
		p2pAnimData.animType_init = (eAnimType)(int)row.at(1);
		p2pAnimData.animType_target = (eAnimType)(int)row.at(2);
		p2pAnimData.animName = row.at(3);
		p2pAnimData.animTimeMs = (TimeMs)row.at(4);
		p2pAnimData.animLooping = (strcmp(row.at(5), DB_TRUE) == 0);

		m_server->P2pAnimAdd(p2pIndex, p2pAnimData);
	}

	END_TRY_SQL();
}

bool MySqlGameState::CheckForNewP2pAnimation(int p2pIndex) {
	bool ret = FALSE;

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "SELECT p2p_anim, initiator_anim_index, participant_anim_index, name, runtime, looping FROM p2pAnimations ";
	query << "WHERE p2p_anim = %0";

	TRACE_QUERY(query, m_logger);
	query.parse();

	StoreQueryResult res = query.store((int)p2pIndex);

	if (res.num_rows() == 1) {
		Row row = res.at(0);

		P2pAnimData p2pAnimData;
		int p2pIndex_new = row.at(0);
		p2pAnimData.animType_init = (eAnimType)(int)row.at(1);
		p2pAnimData.animType_target = (eAnimType)(int)row.at(2);
		p2pAnimData.animName = row.at(3);
		p2pAnimData.animTimeMs = (TimeMs)row.at(4);
		p2pAnimData.animLooping = (strcmp(row.at(5), DB_TRUE) == 0);

		m_server->P2pAnimAdd(p2pIndex_new, p2pAnimData);

		ret = true;
	}

	END_TRY_SQL();

	return ret;
}

// connect to db
void MySqlGameState::Initialize(const char *gameDir) {
	my_init(); // just in case not called by mysql++

	// do common state initialization
	initializeState(gameDir);
#ifndef DECOUPLE_DBCONFIG
	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << " SELECT FLOOR(IFNULL(MAX(version), 0)), NOW(),  "
		  << " SUM(if (interface_change = 'T', 1, 0) AND version > %0) AS interface_change"
		  << " FROM schema_versions";
	query.parse();
	StoreQueryResult r = query.store(MIN_DB_VERSION);
	m_schemaVersion = (long)(r.at(0).at(0));

	if (m_schemaVersion < MIN_DB_VERSION || (long)(r.at(0).at(2)) > 0) {
		throw new KEPException(loadStrPrintf(IDS_DB_VERSION, (long)(r.at(0).at(0)), MIN_DB_VERSION), E_INVALIDARG);
	}
	// log db timestamp to help with diffing log vs db time
	LogInfo("Database timestamp is " << (std::string)(r.at(0).at(1)) << " version floor is " << (long)(r.at(0).at(0)));

	END_TRY_SQL();
#else
#endif

	// load the GetSet data into players
	loadGetSetMetaDataValues();

	loadServerIds();

	loadSupportedWorlds();

	loadNetworkDefinitionsFromDB();

	// always run background thread to get initial stage change into db
	UINT tsafe = mysql_thread_safe();
	jsAssert(tsafe == 1);

	// create one time, then let it run
	if (m_updateThd == 0) {
		LogTrace("Update thread starting...");

		m_updateThd = new BackgroundUpdateThd(this, gameDir);
		if (!m_updateThd->start()) {
			std::string msg;
			LogError(loadStr(IDS_BACKGROUND_THD_FAILED));
		}
	}

	// load the clan database
	if (m_loadClanDatabase) {
		loadClanDatabase();
	}

	loadAnimations();

	loadItemDatabase();

	loadPassLists();
}

// internal init used by background thd, too
void MySqlGameState::initializeState(const char *gameDir) {
	LogInfo("gameDir='" << gameDir << "'");
	IGameState::Initialize(gameDir);

	const IKEPConfig &cfg = m_dbConfig.config();

	// Load configuration for DB connection
	m_conn->loadConfig(cfg, gameDir);

	cfg.GetValue(DB_KPOINT, m_kpointName, "KPOINT");
	cfg.GetValue(DB_GPOINT, m_giftcreditName, "GPOINT");

	cfg.GetValue("LoadClanDatabase", m_loadClanDatabase, 1);
	LogInfo((m_loadClanDatabase ? "" : "not") << " loading clan database");

	m_maxPlayersInBackupLoop = std::min(10, std::max(1, cfg.Get(DB_MAX_PLAYERS_IN_BACKUP, 3)));
	m_backupSleepTime = std::min(30000, std::max(500, cfg.Get(DB_BACKUP_SLEEP_TIME, 3000)));
	m_maxBackupTimeMs = std::min(2000, std::max(10, cfg.Get(DB_BACKUP_SLEEP_TIME, 500)));

	// allow override of swf
	cfg.GetValue(DB_PLAYLISTSWF, m_playlistSwf, "KGPPlaylist.swf");
	cfg.GetValue(DB_PLAYLISTPARAM, m_playlistParams, "plId=");

	m_localPing = cfg.Get(DB_LOCALPING, false);
	m_pingInterval = MIN(MAX(cfg.Get(DB_PINGINTERVAL, m_pingInterval), 1000), 5 * 60 * 1000);
	m_backgroundInterval = MIN(MAX(cfg.Get(DB_BACKGROUNDINTERVAL, m_backgroundInterval), 1000), 5 * 60 * 1000);

	LogInfo("MySqlGameState Database initialized using the following values:");
	LogInfo("   Server:port:             " << m_conn->server() << ":" << m_conn->port());
	LogInfo("   User:                    " << m_conn->user());
	LogInfo("   Database:                " << m_conn->db());
	LogInfo("   NeverSwitchServers:      " << m_conn->neverSwitchServers());
	LogInfo("   LocalPing:               " << m_localPing);
	LogInfo("   EnableAutoBackup:        " << multiplayerObj()->m_enableAutoBackup);
	LogInfo("   KPoint name:             " << m_kpointName);
	LogInfo("   GPoint name:             " << m_giftcreditName);
	LogInfo("   PlaylistSwf name:        " << m_playlistSwf << " params: " << m_playlistParams);
	LogInfo("   GetPlayerUrl:            " << m_conn->getPlayerUrl());
	LogInfo("   getInventoryUpdaterUrl:  " << m_conn->getInventoryUpdaterUrl());

	std::string errMsg;
	try {
		m_conn->connect();
	} catch (const mysqlpp::Exception &er) {
		errMsg = er.what();
		DELETE_AND_ZERO(m_conn);
	}

	if (!m_conn) {
		throw new KEPException(loadStrPrintf(IDS_DB_CONNECT_ERROR, errMsg.c_str()), E_INVALIDARG);
	}

	LogInfo(loadStr(IDS_DB_CONNECT));
}

// initialize the event/handler we use
void MySqlGameState::InitEvents() {
	RegisterDynamicObjectEvents(m_dispatcher);
	RegisterWorldObjectEvents(m_dispatcher);
	RegisterZonePermissionEvents(m_dispatcher);
	RegisterEvent<TextEvent>(m_dispatcher);
	RegisterEvent<ObjectTicklerEvent>(m_dispatcher);
	RegisterEvent<UgcZoneLiteEvent>(m_dispatcher);
	RegisterParticleEvents(m_dispatcher);
	RegisterSoundEvents(m_dispatcher);

	// just in case not registered yet
	m_dispatcher.RegisterEvent(BROWSER_REQUEST_EVENT_NAME, PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent(BROWSER_REPLY_EVENT_NAME, PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent(INVENTORY_UPDATE_EVENT, PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	m_dispatcher.RegisterHandler(MySqlGameState::PlayerSpawnedEventHandler, (ULONG)this, "PlayerSpawnedEvent", PackVersion(1, 0, 0, 0), m_dispatcher.GetEventId("PlayerSpawnedEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(MySqlGameState::TextEventHandler, (ULONG)this, "TextEvent", TextEvent::ClassVersion(), TextEvent::ClassId(), EP_HIGH);
	m_dispatcher.RegisterHandler(MySqlGameState::ShutdownEventHandler, (ULONG)this, "ShutdownEvent", ShutdownEvent::ClassVersion(), ShutdownEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(MySqlGameState::KgpUrlEventHandler, (ULONG)this, BROWSER_REQUEST_EVENT_NAME, PackVersion(1, 0, 0, 0), m_dispatcher.GetEventId(BROWSER_REQUEST_EVENT_NAME), EP_NORMAL);
	m_dispatcher.RegisterHandler(MySqlGameState::ObjectTicklerEventHandler, (ULONG)this, "ObjectTicklerEvent", ObjectTicklerEvent::ClassVersion(), ObjectTicklerEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(MySqlGameState::GetItemBackgroundHandler, (ULONG)this, "GetItemBackgroundHandler", GetResultSetsEvent::ClassVersion(), GetResultSetsEvent::ClassId(), EP_HIGH);
	m_dispatcher.RegisterHandler(MySqlGameState::UgcZoneLiteHandler, (ULONG)this, "UgcZoneLiteEvent", UgcZoneLiteEvent::ClassVersion(), UgcZoneLiteEvent::ClassId(), EP_NORMAL);

	UgcZoneLiteEvent *ugcEvent = new UgcZoneLiteEvent();
	ugcEvent->SetFilter(UgcZoneLiteEvent::REQ_LIST_ZONES);
	m_dispatcher.QueueEvent(ugcEvent);
}

// called when the server engine does save all
void MySqlGameState::SaveAll() {
	START_TIMING(m_timingLogger, "DB:SaveAll");

	// save all the players
	for (POSITION pos = multiplayerObj()->m_runtimePlayersOnServer->GetHeadPosition(); pos != 0;) {
		CPlayerObject *player = (CPlayerObject *)multiplayerObj()->m_runtimePlayersOnServer->GetNext(pos);
		UpdatePlayer(player, PlayerMask::USE_DIRTY_FLAGS);
	}

	// TODO: save everything else
}

void MySqlGameState::Logoff(CAccountObject *pAO, CPlayerObject *pPO, ULONG reason, IEvent *replyEvent) {
	jsVerifyReturnVoid(pAO != 0);

	if (pAO->m_serverId == 0 || pAO->m_sentLogoff || pAO->m_accountNumber == 0) {
		// do nothing for local
		LogTrace("Skipping " << pAO->m_accountName << " since local or sent logoff " << pAO->m_serverId << " " << pAO->m_sentLogoff << " " << pAO->m_accountNumber);
		return;
	}

	if (pAO->m_aiServer)
		return;

	int minPlayed = (((unsigned long)pAO->m_logonTimer.ElapsedMs()) + 3000ul) / 60000ul;
	IBackgroundDBEvent *e = new LogoffUserBackgroundEvent(pAO->m_accountName, pAO->m_accountNumber, pAO->m_serverId, pAO->m_currentIP.c_str(), minPlayed, pPO, reason, replyEvent);
	m_server->GetGameStateDispatcher()->QueueEvent(e);

	pAO->m_sentLogoff = TRUE; // 3/08 moved up here since only want to mark if they were in game
}

/// \brief get the inventoies by running the query then filling in the player
///
void MySqlGameState::getInventory(CPlayerObject *pPO) {
	if (!pPO->m_pAccountObject)
		return;

	Query query = m_conn->query();

	// player_id, kaneva_id, type of inventory, refresh inventory only flag
	query << "CALL get_inventory( %0, %1, '', 0 )";
	query.parse();

	StoreQueryResult res = query.store((int)pPO->m_handle, (int)pPO->m_pAccountObject->m_serverId);

	checkMultipleResultSets(query, "getInventory");

	// clear out the player's inventories and replace them
	MySqlBase::getInventory(pPO->m_charName, &pPO->m_inventory, &pPO->m_bankInventory, res);
}

void MySqlGameState::RefreshInventory(CPlayerObject *pPO, bool bank) {
	if (!pPO->m_pAccountObject)
		return;

	if (pPO->m_handle < MIN_DATABASE_PLAYER_HANDLE)
		return; // do nothing, if local player

	if (pPO && pPO->m_pAccountObject && pPO->m_pAccountObject->m_serverId != 0) {
		CHECK_CONNECTION

		BEGIN_TRY_SQL();

		Transaction trans(*m_conn);

		// flush any Player inventory before looking for new stuff
		LogDebug("Checking player [" << pPO->m_charName << "] dirty flag [" << pPO->m_dbDirtyFlags << "]");
		if (pPO->m_dbDirtyFlags & (PlayerMask::INVENTORY | PlayerMask::BANK_INVENTORY)) {
			LogDebug("Player [" << pPO->m_charName << "] inventory is dirty");
			updatePlayerNoTrans(pPO, 0);
		}

		Query query = m_conn->query();

		// player_id, kaneva_id, type of inventory, refresh inventory only flag
		if (bank) {
			query << "CALL get_inventory( %0, %1, 'B', 1 )";
		} else {
			query << "CALL get_inventory( %0, %1, 'P', 1 )";
		}

		query.parse();

		StoreQueryResult res = query.store((int)pPO->m_handle, (int)pPO->m_pAccountObject->m_serverId);
		checkMultipleResultSets(query, "refreshInventory");

		// clear out the player's inventories and replace them
		if (bank) {
			MySqlBase::getInventory(pPO->m_charName, 0, &pPO->m_bankInventory, res);
		} else {
			MySqlBase::getInventory(pPO->m_charName, &pPO->m_inventory, 0, res);
		}

		m_conn->select_db(m_conn->db());

		trans.commit();

		playerInvUpdateCommitted(pPO);

		END_TRY_SQL();
	}
}

// _SCR_
void MySqlGameState::updateInventory3DApp(int playerId, CInventoryObjList *inventory, ULONG changeMask) {
	if (localPlayers()) {
		return;
	}

	LogDebug("playerId: " << playerId << ", changeMask: " << std::hex << changeMask);

	std::string inventoryType;
	if (changeMask == PlayerMask::BANK_INVENTORY) {
		inventoryType = DB_BANK_INVENTORY;
	} else if (changeMask == PlayerMask::INVENTORY) {
		inventoryType = DB_PLAYER_INVENTORY;
	}

	return MySqlBase::updateInventory3DApp(playerId, inventory, inventoryType.c_str());
}

// called when the player is selected by the user after logging in
// the inbound object will have just the name, on return, the
// player will have everything
void MySqlGameState::GetPlayer(CPlayerObject *pPO) {
	if (pPO->m_handle < MIN_DATABASE_PLAYER_HANDLE)
		return; // do nothing, if local player

	START_TIMING(m_timingLogger, "DB:GetPlayer");

	StoreQueryResult results[PLAYER_RESULTS];

	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	// the the player object, and update it
	Query query = m_conn->query();

	auto pAO = pPO->m_pAccountObject;

	query << "CALL get_player_data( %0, %1 )";
	query.parse();

	LogTrace(query.str((int)pPO->m_handle, (int)pAO->m_serverId));

	results[0] = query.store((int)pPO->m_handle, (int)pAO->m_serverId);
	int arraySize = 1;
	while (query.more_results()) {
		if (arraySize < PLAYER_RESULTS) {
			results[arraySize] = query.store_next();
			arraySize++;
		} else {
			query.store_next(); // usually have one at end of every SP call
		}
	}

	MySqlBase::fillInPlayer(pPO, results, arraySize, pPO->m_netId);
	END_TRY_SQL();
}

void MySqlGameState::loadPassList(CPlayerObject *pPO) {
	Query query = m_conn->query();

	auto pAO = pPO->m_pAccountObject;

	// get the pass group and number of minutes until it times out
	query << "SELECT pass_group_id, end_date"
		  << "  FROM vw_pass_group_users u"
		  << " WHERE u.kaneva_user_id = " << pAO->m_serverId;

	StoreQueryResult res = query.store();
	MySqlBase::loadPassList(pPO, res);
}

bool MySqlGameState::IsAvatarTemplateApplied(CPlayerObject *pPO) {
	// If we don't have local db access, just return true
	// Player template application is only relevant within wok
	if (!localPlayers()) {
		return true;
	}
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << "CALL is_avatar_template_applied( %0, @ret ); SELECT @ret";
	query.parse();

	StoreQueryResult res = query.store(pPO->m_handle);

	res = query.store_next();

	// for SPs MUST call these to avoid disconnect
	checkMultipleResultSets(query, "is_avatar_template_applied");

	return ((int)res.at(0).at(0)) == 1;
	END_TRY_SQL();
	return false;
}

bool MySqlGameState::applyAvatarTemplateStartingInventory(int avatarIndex, CPlayerObject *pPO) {
	Query query = m_conn->query();

	auto pAO = pPO->m_pAccountObject;
	if (pAO) {
		std::vector<starting_inventory_get> siResults;

		query << "CALL insert_starting_inventory( %0, %1, %2, %3 );";
		query.parse();

		query.storein(siResults, pPO->m_handle, (int)pAO->m_serverId, pPO->m_originalEDBCfg, avatarIndex);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "insert_starting_inventory");

		for (auto i = siResults.begin(); i != siResults.end(); i++) {
			bool armed = i->armed == "T" ? true : false;
			m_server->AddInventoryItemToPlayer(pPO, i->quantity, armed, i->global_id, i->sub_type);
		}

		return true;
	} else {
		LogError("NULL account object on player object for " << pPO->m_charName << " rolling back avatar template selection.");
		return false;
	}
}

void MySqlGameState::getAvatarTemplateStartingInfo(int avatarIndex, CPlayerObject *pPO) {
	Query query = m_conn->query();
	query << "CALL get_starting_player_info( %0, %1, @xScale, @yScale, @zScale ); SELECT @xScale, @yScale, @zScale;";
	query.parse();

	StoreQueryResult res = query.store(avatarIndex, pPO->m_handle);

	res = query.store_next();

	// for SPs MUST call these to avoid disconnect
	checkMultipleResultSets(query, "get_starting_player_info");

	pPO->m_scaleFactor.x = (float)(res.at(0).at(0));
	pPO->m_scaleFactor.y = (float)(res.at(0).at(1));
	pPO->m_scaleFactor.z = (float)(res.at(0).at(2));
	pPO->m_scratchScaleFactor = pPO->m_scaleFactor;
}

void MySqlGameState::getAvatarTemplateDeformableConfigs(int avatarIndex, CPlayerObject *pPO) {
	std::vector<deformable_configs_get> defResult;

	auto pAO = pPO->m_pAccountObject;
	if (pAO) {
		//We need to get the base player for this account.  Not the runtime.
		auto pPO_cur = pAO->GetCurrentPlayerObject();

		//Load new template from DB
		Query query = m_conn->query();
		query << "CALL get_starting_deformable_config( %0 );";
		query.parse();

		query.storein(defResult, avatarIndex);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "get_starting_deformable_config");

		pPO_cur->m_charConfig.disposeAllItems();
		pPO_cur->m_baseMeshes = defResult.size();
		if (!defResult.empty()) {
			// first get counts of each type/equip
			GLID_OR_BASE glidOrBase = (GLID_OR_BASE)defResult[0].equip_id;
			BYTE slotCount = 0;
			for (auto i = defResult.begin(); i != defResult.end(); i++) {
				if (glidOrBase != i->equip_id) {
					pPO_cur->m_charConfig.addItem(glidOrBase, slotCount);
					slotCount = 0;
					glidOrBase = i->equip_id;
				}
				slotCount++;
			}
			pPO_cur->m_charConfig.addItem(glidOrBase, slotCount); // do last one (or first if only one)

			size_t itemSlot = 0;
			int itemEquip = CC_ITEM_INVALID;
			CharConfigItem *pConfigItem = NULL;
			for (std::vector<deformable_configs_get>::iterator i = defResult.begin(); i != defResult.end(); i++) {
				if (itemEquip != i->equip_id) {
					itemSlot = 0;
					itemEquip = i->equip_id;
					pConfigItem = pPO_cur->m_charConfig.getItemByGlidOrBase(itemEquip);
					if (!pConfigItem)
						continue;
				}

				if (pConfigItem && itemSlot < pConfigItem->getSlotCount()) {
					CharConfigItemSlot *pSlot = pConfigItem->getSlot(itemSlot);
					assert(pSlot != nullptr);
					pSlot->m_meshId = i->deformable_config;
					pSlot->m_matlId = i->custom_material;
					pSlot->m_r = i->custom_color_r;
					pSlot->m_g = i->custom_color_g;
					pSlot->m_b = i->custom_color_b;
				}

				itemSlot++;
			}
		}

		updateDeformableMeshes(pPO_cur);
	} else
		LogWarn("Account not found! For player " << pPO->m_charName);
}

void MySqlGameState::recordAvatarTemplateApplication(int avatarIndex, CPlayerObject *pPO) {
	Query query = m_conn->query();
	query << "CALL record_template_application( %0, %1 );";
	query.parse();

	StoreQueryResult res = query.store(pPO->m_handle, avatarIndex);
	query.store_next();

	// for SPs MUST call these to avoid disconnect
	checkMultipleResultSets(query, "record_template_application");
}

bool MySqlGameState::ApplyAvatarSelection(int avatarIndex, CPlayerObject *pPO) {
	std::string errMsg;
	BEGIN_TRY_SQL();

	// do this in a transaction
	Transaction trans(*m_conn);

	bool ret = IsAvatarTemplateApplied(pPO);
	if (ret) {
		trans.rollback();
		LogWarn("ApplyAvatarSelection: Avatar template already applied for player " << pPO->m_charName);
		return true; //The template was already applied.  Try switching to apartment again.
	}

	if (!applyAvatarTemplateStartingInventory(avatarIndex, pPO)) {
		trans.rollback();
		return false;
	}

	if (avatarIndex > 0) {
		getAvatarTemplateStartingInfo(avatarIndex, pPO);

		getAvatarTemplateDeformableConfigs(avatarIndex, pPO);
	}

	recordAvatarTemplateApplication(avatarIndex, pPO);

	trans.commit();

	END_TRY_SQL();

	return true;
}

// called when the user creates a new player object
void MySqlGameState::CreatePlayer(CAccountObject *pAO, CPlayerObject *pPO) {
	jsVerifyReturnVoid(pAO != 0 && localPlayers());

	if (pAO->m_serverId == 0) // do nothing for local
		return;

	START_TIMING(m_timingLogger, "DB:CreatePlayer");

	ULONG accountId = pAO->m_accountNumber;
	CHECK_CONNECTION

	std::string errMsg;
	BEGIN_TRY_SQL();

	// do this in a transaction
	Transaction trans(*m_conn);

	// hit the account table for RI check
	Query userid_query = m_conn->query();
	userid_query << "SELECT `user_id` FROM `game_users` WHERE `user_id` = " << accountId;
	StoreQueryResult userid_res = userid_query.store();

	if (userid_res.num_rows() == 1) // we know about them or
	{
		// insert the entry
		Query query = m_conn->query();
		query << "CALL insert_players( %0, %1, %2q, %3q, %4, %5, %6, %7, @ret ); SELECT @ret;";
		query.parse();

		StoreQueryResult res = query.store(
			(int)accountId,
			(int)pAO->m_serverId,
			(const char *)pPO->m_charName,
			(const char *)pPO->m_title,
			pPO->m_bornSpawnCfgIndex,
			pPO->m_originalEDBCfg,
			(int)0, //player->m_teamID,
			(int)0 //player->m_raceID
		);

		res = query.store_next();

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "insert_players");

		int player_handle = 0;
		if (res.num_rows() > 0)
			player_handle = (int)(res.at(0).at(0));

		jsVerifyThrow(player_handle >= MIN_DATABASE_PLAYER_HANDLE, new KEPException("Database not setup correctly.  Returned player id < 10000", E_INVALIDARG));

		query.reset();
		query << "CALL insert_default_clothing( %0, %1q, @ret ); SELECT @ret;";
		query.parse();

		res = query.store(player_handle, pAO->m_gender);
		res = query.store_next();

		if ((int)(res.at(0).at(0)) != 0)
			LogWarn("Unkown gender, default clothing failed to be inserted. Player " << pPO->m_charName);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "insert_default_clothing");

		pPO->m_handle = player_handle;

		// now that we have the handle, we can set the respawn instance Id, if it's our house
		if (pPO->m_respawnZoneIndex.isHousing())
			pPO->m_respawnZoneIndex.setInstanceId(pPO->m_handle);

		// insert player presences data
		query.reset();
		query << "CALL insert_player_presences ( %0, %1, %2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14, %15, %16, %17, %18, %19, %20, %21 )";
		query.parse();

		Vector3f pos = pPO->InterpolatedPos();
		Vector3f dir = pPO->InterpolatedDir();
		query.execute(
			(int)pPO->m_handle,
			dir.x,
			dir.y,
			dir.z,
			pos.x,
			pos.y,
			pos.z,
			(int)0, //player->m_energy,
			(int)0, //player->m_maxEnergy,
#ifndef REFACTOR_INSTANCEID
			(int)pPO->m_housingZoneIndex,
#else
			(int)pPO->m_respawnZoneIndex.toLong(),
#endif
			(int)pPO->m_housingZoneIndex.zoneIndex(),
#ifndef REFACTOR_INSTANCEID
			(int)pPO->m_respawnZoneIndex,
#else
			(int)pPO->m_respawnZoneIndex.toLong(),
#endif
			(int)pPO->m_respawnZoneIndex.GetInstanceId(),
			pPO->m_respawnPosition.x,
			pPO->m_respawnPosition.y,
			pPO->m_respawnPosition.z,
			pPO->m_scaleFactor.x,
			pPO->m_scaleFactor.y,
			pPO->m_scaleFactor.z,
			(int)0, //player->m_countDownToMurderPardon,
			(int)0, //player->m_arenaBasePoints,
			(int)0 //player->m_arenaTotalKills
		);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "insert_player_presences");

		updateDeformableMeshes(pPO);

		// call this to populate zone table with initial zone
		query.reset();
		query << "CALL playerZoning	( %0, %1, %2, %3, %4q )";
		query.parse();

		query.execute((int)pPO->m_handle,
			(int)m_serverId,
			(int)pPO->m_currentZoneIndex.getClearedInstanceId(),
			(int)pPO->m_currentZoneIndex.GetInstanceId(),
			(const char *)pPO->m_currentWorldMap);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "playerZoning");

		// the trigger on the players table updates the inventories now, so we need to get it
		if (!pPO->m_pAccountObject)
			pPO->m_pAccountObject = pAO;
		getInventory(pPO);

		DataModel *model = PluginHost::singleton().DEPRECATED_getInterfaceNoThrow<DataModel>("AppDataModel");
		PlayerModel *playerModel = (model == 0 ? (PlayerModel *)0 : model->registry().getFacetNoThrow<PlayerModel>("PlayerModel"));
		if (playerModel) {
			KEPModel::SkillVector tmp;
			for (POSITION invPos = pPO->m_skills->GetHeadPosition(); invPos != NULL;) {
				CSkillObject *o = (CSkillObject *)pPO->m_skills->GetNext(invPos);
				tmp.push_back(KEPModel::Skill(o->m_skillType, o->m_currentLevel, o->m_currentExperience));
			}
			try {
				playerModel->updateSkills(pPO->m_handle, tmp);
			} catch (const ChainedException &ce) {
				LogError("Exception raised while updating player skills [" << ce.str() << "]");
			}
		} else {
			updateSkills(pPO->m_handle, pPO->m_skills);
		}
		updateStats(pPO->m_handle, pPO->m_stats);
		updateQuests(pPO->m_handle, pPO->m_questJournal);

		// noteriety ok to be null

		trans.commit();
	} else {
		throw new KEPException("Account not found", E_INVALIDARG);
	}

	END_TRY_SQL();
}

void MySqlGameState::DeletePlayer(ULONG accountServerId, CPlayerObject *pPO) {
	if (accountServerId == 0) // do nothing for local
		return;

	START_TIMING(m_timingLogger, "DB:DeletePlayer");

	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	// delete the rows from the player tables
	if (pPO->m_handle >= MIN_DATABASE_PLAYER_HANDLE) {
		Transaction trans(*m_conn);

		// trigger deletes the rest
		Query query = m_conn->query();
		query << "DELETE FROM players WHERE player_id = " << pPO->m_handle;
		query.execute();

		trans.commit();
	}

	END_TRY_SQL();
}

// called to update the player in the database
void MySqlGameState::UpdatePlayer(CPlayerObject *pPO, ULONG changeMask /*= USE_PLAYER_DIRTY_FLAGS */) {
	START_TIMING(m_timingLogger, "DB:UpdatePlayer");

	if (pPO->m_handle < MIN_DATABASE_PLAYER_HANDLE)
		return; // do nothing, if local player

	updatePlayer(pPO, changeMask, false);
}

bool MySqlGameState::ReloadPlayer(CPlayerObject *pPO, ULONG changeMask) {
	// right now only support pass list reload
	if ((changeMask & PlayerMask::PASS_LIST) == PlayerMask::PASS_LIST) {
		BEGIN_TRY_SQL();
		loadPassList(pPO);
		END_TRY_SQL();

		return true;
	}
	return false;
}

void MySqlGameState::updatePlayer(CPlayerObject *pPO, ULONG changeMask, bool loggingOut) {
	CHECK_CONNECTION

	CSLock lock(&multiplayerObj()->m_criticalSection);

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	updatePlayerNoTrans(pPO, changeMask, loggingOut);

	trans.commit();
	playerInvUpdateCommitted(pPO);

	END_TRY_SQL();
}

void MySqlGameState::UpdateTradingPlayers(CPlayerObject *pPO_1, CPlayerObject *pPO_2, ULONG changeMask /*= PLAYER_INVENTORY | PLAYER*/) {
	START_TIMING(m_timingLogger, "DB:UpdateTradingPlayers");

	if (pPO_1->m_handle < MIN_DATABASE_PLAYER_HANDLE || pPO_2->m_handle < MIN_DATABASE_PLAYER_HANDLE)
		return; // do nothing, if local player

	CHECK_CONNECTION

	CSLock lock(&multiplayerObj()->m_criticalSection);

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();
	// added @traderId so triggers can get id
	query << "set @traderId = " << pPO_2->m_handle;
	query.execute();
	updatePlayerNoTrans(pPO_1, changeMask);
	query.reset();
	query << "set @traderId = " << pPO_1->m_handle;
	query.execute();
	updatePlayerNoTrans(pPO_2, changeMask);
	query.reset();
	query << "set @traderId = NULL";
	query.execute();

	trans.commit();
	playerInvUpdateCommitted(pPO_1);
	playerInvUpdateCommitted(pPO_2);

	END_TRY_SQL();
}

bool MySqlGameState::updatePlayersCashNoTrans(CPlayerObject *pPO, long player1AmtChanging, long &curAmt, TransactionType transactionType, CurrencyType currencyType, int glid, int qty) // these two only if buying or selling
{
	if (pPO->m_handle < MIN_DATABASE_PLAYER_HANDLE)
		return true; // do nothing, if local player

	if (!pPO || !pPO->m_pAccountObject || pPO->m_pAccountObject->m_serverId == 0)
		return false;

	return updatePlayersCashNoTrans((long)pPO->m_pAccountObject->m_serverId, player1AmtChanging, curAmt, transactionType, currencyType, glid, qty);
}

bool MySqlGameState::updatePlayersCashNoTrans(LONG serverId, long player1AmtChanging, long &curAmt, TransactionType transactionType, CurrencyType currencyType, int glid, int qty) // these two only if buying or selling
{
	bool ret = false;

	if (transactionType == TransactionType::HOUSE_BANKING ||
		transactionType == TransactionType::BOUGHT_HOUSE_ITEM) {
		LogWarn("Player (serverId) " << serverId << " using unsupported cash trans " << (int)transactionType << " for amount " << player1AmtChanging);
		return false; // WOK doesn't support these now
	}

	Query query = m_conn->query();

	query << "CALL apply_transaction_and_details_to_user_balance( %0, %1, %2, %3, %4, %5, @rc, @newBalance, @tranId ); SELECT @rc, @newBalance, @tranId;";

	query.parse();

	StoreQueryResult res = query.store((int)serverId,
		(int)player1AmtChanging,
		(USHORT)transactionType,
		(int)currencyType,
		(int)glid,
		(int)qty);

	res = query.store_next(); // result set for the select

	// and just as added check, call this
	checkMultipleResultSets(query, "updatePlayersCashNoTrans");

	if (!res.num_rows() == 0) {
		if ((LONG)(res.at(0).at(0)) == 0l) // 0 = ok, 1 = insufficient funds, -1 = error
		{
			ret = true;
			curAmt = res.at(0).at(1);
		}
	}

	return ret;
}

// returns 0 if ok, 1 if player1 had insufficent, 2 if player2 had insufficient
// player 2 optional, if player1AmtChanging 0 then only the cur amt is updated on the player
IGameState::EUpdateRet MySqlGameState::UpdatePlayersCash(CPlayerObject *pPO_1, long player1AmtChanging, TransactionType transactionType, CPlayerObject *pPO_2, long player2AmtChanging, CurrencyType currencyType, int glid, int qty) {
	START_TIMING(m_timingLogger, "DB:UpdatePlayersCash");

	EUpdateRet ret = UPDATE_PLAYER1_INSUFFICIENT;

	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	// holders for current db cash level
	long player1Cash = 0;
	long player1BankCash = 0;
	long player2Cash = 0;

	if (player1AmtChanging != 0 && !updatePlayersCashNoTrans(pPO_1, player1AmtChanging, player1Cash, transactionType, currencyType, glid, qty))
		ret = UPDATE_PLAYER1_INSUFFICIENT;
	else if (pPO_2 != 0 && player2AmtChanging != 0 && !updatePlayersCashNoTrans(pPO_2, player2AmtChanging, player2Cash, transactionType, currencyType, glid, qty))
		ret = UPDATE_PLAYER2_INSUFFICIENT;
	else
		ret = UPDATE_OK;

	if (ret == UPDATE_OK && transactionType == TransactionType::BANKING) {
		// only can do banking using player credits
		jsVerifyReturn(currencyType == CurrencyType::CREDITS_ON_PLAYER, ret);

		if (updatePlayersCashNoTrans(pPO_1, -player1AmtChanging, player1BankCash, transactionType, CurrencyType::CREDITS_IN_BANK, 0, 0)) {
			if (pPO_1->m_bankCash)
				pPO_1->m_bankCash->m_amount = player1BankCash;
			else if (pPO_1->playerOrigRef() && pPO_1->playerOrigRef()->m_bankCash)
				pPO_1->playerOrigRef()->m_bankCash->m_amount = player1BankCash;
		} else {
			ret = UPDATE_PLAYER1_INSUFFICIENT;
		}
	}

	if (ret == UPDATE_OK) {
		bool updatePlayer1 = (transactionType == TransactionType::SOLD_ITEM || transactionType == TransactionType::BOUGHT_ITEM || transactionType == TransactionType::BOUGHT_HOUSE_ITEM) &&
							 glid != 0 && qty != 0;
		if (updatePlayer1) {
			updatePlayerNoTrans(pPO_1, PlayerMask::INVENTORY); // also update inv in this tran if buying/selling
		}
		//Added != 0 check to player1AmtChanging because if it was 0 it would 0 out m_amount until next time cash was retrieved REK 8-7-08
		switch (currencyType) {
			case CurrencyType::GIFT_CREDITS_ON_PLAYER: {
				if (player1AmtChanging != 0) {
					pPO_1->m_giftCredits->m_amount = player1Cash;
				}

				if (pPO_2 != 0) {
					pPO_2->m_giftCredits->m_amount = player2Cash;
				}
			} break;

			case CurrencyType::CREDITS_ON_PLAYER: {
				if (player1AmtChanging != 0) {
					pPO_1->m_cash->m_amount = player1Cash;
				}

				if (pPO_2 != 0) {
					pPO_2->m_cash->m_amount = player2Cash;
				}
			} break;
		}

		trans.commit();
		if (updatePlayer1)
			playerInvUpdateCommitted(pPO_1);

	} else {
		trans.rollback();
	}
	m_conn->select_db(m_conn->db());

	END_TRY_SQL();

	return ret;
}

void MySqlGameState::RefreshPlayersCashes(CPlayerObject *pPO) {
	if (pPO->m_handle < MIN_DATABASE_PLAYER_HANDLE)
		return; // do nothing, if local player

	if (pPO && pPO->m_pAccountObject && pPO->m_pAccountObject->m_serverId != 0) {
		CHECK_CONNECTION

		BEGIN_TRY_SQL();

		Query query = m_conn->query();

		// gets two cols, point_id, amt
		query << "CALL get_user_balances( %0 )";
		query.parse();

		StoreQueryResult res = query.store((int)pPO->m_pAccountObject->m_serverId);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "get_user_balances");

		for (UINT i = 0; i < res.num_rows(); i++) {
			const Row &r = res.at(i);
			CurrencyType currencyType = static_cast<CurrencyType>((long)r.at(0));
			switch (currencyType)
			{
				case CurrencyType::CREDITS_ON_PLAYER:
					pPO->m_cash->m_amount = r.at(1);
					break;

				case CurrencyType::CREDITS_IN_BANK:
					if (pPO->playerOrigRef())
						pPO->playerOrigRef()->m_bankCash->m_amount = r.at(1);
					break;

				case CurrencyType::GIFT_CREDITS_ON_PLAYER:
					pPO->m_giftCredits->m_amount = r.at(1);
					break;

				default:
					// WOK-specific, this is all we support
					break;
			}
		}

		m_conn->select_db(m_conn->db());

		END_TRY_SQL();
	}
}

long MySqlGameState::GetPlayersCash(CPlayerObject *pPO, CurrencyType currencyType) {
	long ret = 0;

	if (pPO->m_handle < MIN_DATABASE_PLAYER_HANDLE)
		return ret; // do nothing, if local player

	if (pPO && pPO->m_pAccountObject && pPO->m_pAccountObject->m_serverId != 0) {
		if (localPlayers()) {
			CHECK_CONNECTION

			BEGIN_TRY_SQL();

			ret = getPlayersCash((int)pPO->m_pAccountObject->m_serverId, currencyType);

			END_TRY_SQL();
		} else {
			// TODO: load from web
			jsAssert(false);
		}
	}

	return ret;
}

void MySqlGameState::ExpandGroup(int groupId, std::vector<std::pair<int, std::string>> &recipientList) {
	START_TIMING(m_timingLogger, "DB:ExpandGroup");

	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	std::vector<group_expansion_get> result;

	Query query = m_conn->query();

	query << "CALL expand_group( %0 )";
	query.parse();

	query.storein(result, (int)groupId);

	// for SPs MUST call these to avoid disconnect
	checkMultipleResultSets(query, "expand_group");

	for (std::vector<group_expansion_get>::iterator i = result.begin(); i != result.end(); i++) {
		recipientList.push_back(make_pair(i->player_id, i->name));
	}

	END_TRY_SQL();
}

bool MySqlGameState::IsUserBlocked(CAccountObject *pAO, LONG destKanevaId, LONG instanceId, BlockType blockType, const char *blockingUserName) {
	START_TIMING(m_timingLogger, "DB:IsUserBlocked");

	bool ret = true;
	if (pAO->m_serverId == 0 || pAO->m_gm)
		return false; // local not in db, and gm never blocked

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	// isUserBlocked is a fn
	query << "select isUserBlocked( %0, %1, %2, %3, %4q );";

	query.parse();

	LogTrace(query.str((int)pAO->m_serverId,
		(int)destKanevaId,
		(int)instanceId,
		(int)blockType,
		blockingUserName));
	StoreQueryResult results = query.store((int)pAO->m_serverId,
		(int)destKanevaId,
		(int)instanceId,
		(int)blockType,
		blockingUserName);

	if (results.size() > 0) {
		Row r = results.at(0);

		ret = (int)r.at(0) == 1;
	} else {
		ret = false;
	}

	LogDebug("IsUserBlocked ret: " << ret << " blockType:" << (int)blockType << " sourceId:" << (int)pAO->m_serverId << " destId:" << destKanevaId << " blockUserName:" << blockingUserName);

	END_TRY_SQL();

	return ret;
}

bool MySqlGameState::DoesItemSupportAnimation(long itemGLID, long animGLID) {
	START_TIMING(m_timingLogger, "DB:isAnimSupportedByItem");
	// Validate the anim GLID can be used with this item GLID, but not in a 3D App which might not have
	// a valid item_animation table if new animations have been added since the item was imported.
	// ChooseAnimation.lua should have received only correct animations anyway, but this is an extra
	// layer of protection for WoK users.
	if (!localPlayers())
		return true;

	bool ret = false;

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	// this only works for dynamic objects
	//query << "CALL isAnimSupported( %0, %1);";
	query << "SELECT item_id FROM item_animations WHERE item_id = %0 AND animation_id = %1;";

	query.parse();

	StoreQueryResult results = query.store((int)itemGLID, (int)animGLID);

	if (results.size() > 0) {
		Row r = results.at(0);

		ret = (int)r.at(0) != 0;
	} else {
		ret = false;
	}

	LogDebug("IsAnimSupportedByItem ret: " << ret << " itemGLID:" << (int)itemGLID << " animGLID:" << (int)animGLID);
	checkMultipleResultSets(query, "IsAnimSupportedByItem");

	END_TRY_SQL();

	return ret;
}

#if DEPRECATED
bool MySqlGameState::DoesPlacementSupportAnimation(long placementId, long animGlid) {
	START_TIMING(m_timingLogger, "DB:isAnimSupported");

	// Validate the anim GLID can be used with this object, but not in a 3D App which might not have
	// a valid item_animation table if new animations have been added since the item was imported.
	// ChooseAnimation.lua should have received only correct animations anyway, but this is an extra
	// layer of protection for WoK users.
	if (!localPlayers())
		return true;

	bool ret = false;

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "CALL isAnimSupported( %0, %1);";

	query.parse();

	StoreQueryResult results = query.store((int)placementId, (int)animGlid);

	if (results.size() > 0) {
		Row r = results.at(0);

		ret = (int)r.at(0) == 1;
	} else {
		ret = false;
	}

	LogDebug("IsAnimSupported ret: " << ret << " placement Id:" << (int)placementId << " animGlid:" << (int)animGlid);
	checkMultipleResultSets(query, "IsAnimSupported");

	END_TRY_SQL();

	return ret;
}
#endif

bool MySqlGameState::SetPlayerBanStatus(const char *playerName, const char *reason, CPlayerObject *pPO, bool banned) {
	START_TIMING(m_timingLogger, "DB:SetPlayerBanStatus");

	bool ret = false;
	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();

	query << "CALL setPlayerBanStatus( %0q, %1q, %2, %3, @banned ); SELECT @banned;";

	ULONG bannerId = pPO->m_pAccountObject->m_serverId;

	query.parse();

	LogTrace(query.str(playerName, reason, (int)(banned ? 1 : 0), (int)bannerId));
	StoreQueryResult res = query.store(playerName, reason, (int)(banned ? 1 : 0), (int)bannerId);

	res = query.store_next(); // get select output
	ret = res.size() > 0 && (LONG)res.at(0).at(0) != 0;

	trans.commit();

	END_TRY_SQL();

	return ret;
}

// lookup the server:port given a serverId
bool MySqlGameState::MapServerId(LONG serverId, std::string &server, ULONG &port) {
	server = "";
	port = 0;

	if (serverId == m_serverId || serverId == 0)
		return true; // 7/08 moved this check higher -- don't return server if it's us!

	// check, if not found will try to reload and check again
	for (int i = 0; i < 2; i++) {
		if (i > 0 && !loadServerIds())
			break; // didn't refresh serverid, no sense rechecking

		if (m_serverInfo) {
			ServerInfo *p = m_serverInfo;
			while (p->serverId != 0 && p->port != 0) {
				if (p->serverId == serverId) {
					server = p->ipAddress;
					port = p->port;
					return true;
				}
				p++;
			}
		}
	}

	LogWarn("No server found in map for serverId = " << serverId);
	return false;
}

PassList *MySqlGameState::GetZonePassList(const ZoneIndex &zi) {
	PassList *ret = 0;

	static TimeMs lastCleanup = 0;
	const TimeMs CHECK_CLEANUP_INTERVAL = 60000;
	const TimeMs CLEANUP_INTERVAL = 30000;

	TimeMs timeMs = fTime::TimeMs();

	if ((timeMs - lastCleanup) > CHECK_CLEANUP_INTERVAL) {
		for (PassListMap::iterator i = m_passLists.begin(); i != m_passLists.end();) {
			if ((timeMs - i->second.second) > CLEANUP_INTERVAL) {
				delete i->second.first;
				i = m_passLists.erase(i);
			} else {
				i++;
			}
		}

		lastCleanup = timeMs;
	}

	// see if we have it in the list
	PassListMap::iterator i = m_passLists.find(zi.toLong());
	if (i == m_passLists.end()) {
		BEGIN_TRY_SQL();

		Query query = m_conn->query();
		query << "CALL getZonePasses( %0, %1 )";
		query.parse();
		StoreQueryResult res = query.store((int)zi.getClearedInstanceId(), (int)zi.GetInstanceId());
		if (res.num_rows() > 0) {
			ret = new PassList;
			for (UINT j = 0; j < res.num_rows(); j++)
				ret->addPass(res.at(j).at(0));
		}

		// ok to be null, indicates no security
		m_passLists[zi.toLong()] = std::pair<PassList *, TimeMs>(ret, timeMs);

		LogDebug("Added ZonePassList for " << numToString(zi.toLong()) << " count " << res.num_rows());

		checkMultipleResultSets(query, "GetZonePassList");

		END_TRY_SQL();
	} else {
		ret = i->second.first;
	}

	return ret;
}

// get the dynamically added GetSet values for a player
void MySqlGameState::getGetSetMetaData() {
	gsValues *values = 0;
	gsMetaData *md = 0;

	CPlayerObject player;
	player.GetValues(values, md);

	jsVerifyDo(values != 0 && md != 0, return );
	jsAssert(values->size() == md->size());

	Query query = m_conn->query();
	query << "SELECT " << GET_GETSET_COLS << "FROM `getset_player_values` "
		  << " WHERE player_id = 0";
	StoreQueryResult results = query.store();

	Row row;
	Row::size_type i;
	for (i = 0; i < results.size(); ++i) {
		row = results.at(i);

		// limit what can be set in flags, and always set some
		ULONG flags = GS_FLAG_DYNAMIC_ADD | GS_FLAG_SAVE_SPAWN;

		if (strlen(row.at(0)) > 0) {
			switch ((LONG)row.at(1)) {
				case gsInt: {
					jsVerifyDo(!row.at(2).is_null(), continue);
					player.AddNewIntMember(row.at(0), row.at(2), flags);
				} break;

				case gsDouble: {
					jsVerifyDo(!row.at(3).is_null(), continue);
					player.AddNewDoubleMember(row.at(0), row.at(3), flags);
				} break;

				case gsString: {
					jsVerifyDo(!row.at(4).is_null(), continue);
					player.AddNewStringMember(row.at(0), row.at(4), flags);
				} break;

				default: {
					jsAssert(false);
				} break;
			}
		}
	}
}

// get the dynamically added GetSet values for a player
void MySqlBase::getGetSet(CPlayerObject *pPO, StoreQueryResult &results) {
	gsValues *values = 0;
	gsMetaData *md = 0;

	pPO->GetValues(values, md);
	jsVerifyDo(values != 0 && md != 0, return );
	jsAssert(values->size() == md->size());

	EGetSetType type;
	ULONG id = FIND_ID_NOT_FOUND;

	Row row;
	Row::size_type i;
	for (i = 0; i < results.size(); ++i) {
		row = results.at(i);
		id = pPO->FindIdByName(row.at(0).c_str(), &type);
		if (id == FIND_ID_NOT_FOUND ||
			type != md->at(id).GetType() ||
			type != (int)row.at(1)) {
			jsAssert(false); // wrong type, log it?
			continue;
		}

		switch (type) {
			case gsInt:
				jsVerifyDo(!row.at(2).is_null(), continue);
				pPO->SetNumber(id, (LONG)row.at(2));
				break;

			case gsDouble:
				jsVerifyDo(!row.at(3).is_null(), continue);
				pPO->SetNumber(id, (DOUBLE)row.at(3));
				break;

			case gsString:
				jsVerifyDo(!row.at(4).is_null(), continue);
				pPO->SetString(id, (const char *)row.at(4));
				break;

			default:
				jsAssert(false);
				continue;
				break;
		}
	}
	pPO->GetSet::SetDirty(false); // just loaded, clear dirty set by these sets
}

class ItemWrapper {
public:
	ItemWrapper(mysqlpp::Row &row) :
			_row(row), _glid(0), _pass(0) {
		if (_row.size() == 0)
			return;
		_glid = atol(_row.at(1));
		_pass = atoi(_row.at(2));
	}
	mysqlpp::Row &_row;
	ULONG _glid;
	int _pass;
};

// Oh to have support for anonymous code blocks a la Java.
class ItemSelector : public KGenericObjList<ItemWrapper>::Selector {
public:
	ULONG _glid;
	ItemSelector(UINT glid = 0) :
			_glid(glid) {}
	ItemSelector &setGlid(ULONG glid) {
		_glid = glid;
		return *this;
	}

	bool evaluate(ItemWrapper &row) { return row._glid == _glid; }
};

class ItemWrapperDeleter : public KGenericObjList<ItemWrapper>::Selector {
public:
	bool evaluate(ItemWrapper &row) {
		delete &row;
		return false;
	}
};

class PassTraverser : public KGenericObjList<ItemWrapper>::Traverser {
public:
	CItemObj &_item;

	PassTraverser(CItemObj &item) :
			_item(item) {}

	bool evaluate(ItemWrapper &row) {
		if (_item.m_globalID == row._glid) {
			if (_item.m_passList == 0)
				_item.m_passList = new PassList;

			_item.m_passList->addPass(row._pass);
		}
		return false;
	}
};

//! refresh the item's pass list from the database
void MySqlBase::updateItemPassList(CItemObj &item, KGenericObjList<ItemWrapper> &PassesList) {
	jsVerifyReturnVoid(item.m_globalID > 0);
	PassTraverser traverser(item);
	PassesList.traverse(traverser);
}

void MySqlBase::popItemFromTuple(Row &row, CItemObj &item) {
	/*
    New Old Field
	0	n/a is_players_inventory 
	1	n/a global_id 
	2	0	name
	3	1	description
	4	2	market_cost
	5	3	selling_price
	6	4	required_skill
	7	5	required_skill_level
	8	6	use_type
	9	n/a item_creator_id 
	10	n/a arm_anywhere 
	11	n/a disarmable 
	12	15	stackable
	13	n/a destroy_when_used 
	14	8	inventory_type
	15	n/a custom_texture 
	16	9	base_global_id
	17	10	expired_duration
	18	11	permanent
	19	12	is_default
	20	13	visual_ref
	21	n/a web_controlled_edit 
	22	14	is_derivable
	23	n/a derivation_level 
	24	17	actor_group
	25	n/a search_refresh_needed 
	26	7   use_value
	27	16	exclusion_groups
*/

	item.m_globalID = atoi(row.at(1));
	//item.m_itemName				= (const char*)row.at(2);
	//item.m_itemDescription		= (const char*)row.at(3);
	item.m_marketCost = atoi(row.at(4));
	item.m_sellingPrice = atoi(row.at(5));
	item.m_requiredSkillType = atoi(row.at(6));
	item.m_requiredSkillLevel = atoi(row.at(7));
	item.m_useType = (USE_TYPE)atoi(row.at(8));
	item.m_miscUseValue = atoi(row.at(26));
	item.m_itemType = atoi(row.at(14));
	item.m_baseGlid = atoi(row.at(16));
	item.m_expiredDuration = atoi(row.at(17));
	item.m_permanent = row.at(18);
	item.m_defaultArmedItem = row.at(19);
	item.m_visualRef = atoi(row.at(20));
	item.m_derivable = row.at(22);
	item.m_stackable = row.at(12);
	std::string sExclusionGroups = (const char *)row.at(27);
	item.ParseExclusionGroupsCSV(sExclusionGroups);
	item.m_actorGroup = atoi(row.at(24));
}

void MySqlBase::getInventory(const char *playerName, CInventoryObjList **playerInv, CInventoryObjList **bankInv, StoreQueryResult &InventoryRecords, StoreQueryResult &ItemRecords, StoreQueryResult &PassRecords) {
	// If only got updates and there are no updates,
	// one record returned with 0 values. Checking two fields here.
	if (InventoryRecords.num_rows() == 1) {
		Row r = InventoryRecords.at(0);
		if ((atol(r.at(INV_PLAYER_ID)) == 0) &&
			(atol(r.at(INV_GLID)) == 0))
			return; // getting updates, and none, so nothing to do
	}

	// clear out the player's inventories and replace them
	if (playerInv)
		if (*playerInv)
			(*playerInv)->SafeDelete();
		else
			*playerInv = new CInventoryObjList();

	if (bankInv)
		if (*bankInv)
			(*bankInv)->SafeDelete();
		else
			*bankInv = new CInventoryObjList;

	// Guard clause -- If there are no inventory records to process, then don't bother.
	//
	if (InventoryRecords.num_rows() == 0)
		return;

	// invResult can be empty if no pending items existed to be
	// added to the inventory. Thus, leave inventory as is.
	// Check for global inventory as well
	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();

	// Guard clause.
	// If we can't get the GIL, then go no further.
	//
	if (globalDBRef == 0) {
		LogError("Can't get CGlobalInventoryObjList::GetInstance in addInventoryItem");
		return;
	}

	// Convert the items result set to two CObLists, one containing
	// items directly owned by the player in question, and the
	// other , containing item records that are base items for
	// thosed owned items.  The get_player_data() stored procedure
	// that was used acquire this data on the background thread
	// is fashioned such that the first field will have a value
	// of zero if the item is not owned by this user (i.e. is a base
	// item for an item that IS owned by this player).  Otherwise,
	// this field will have a value of 1.
	//
	KGenericObjList<ItemWrapper> OwnedItemsList;
	KGenericObjList<ItemWrapper> BaseItemsList;
	unsigned int numItems = ItemRecords.num_rows();
	for (UINT i1 = 0; i1 < numItems; i1++) {
		Row &r = ItemRecords.at(i1);
		ItemWrapper *wrapper = new ItemWrapper(r);
		if (atoi(r.at(0)) == 0)
			BaseItemsList.append(*wrapper);
		else
			OwnedItemsList.append(*wrapper);
	}

	unsigned int numPasses = PassRecords.num_rows();
	KGenericObjList<ItemWrapper> PassesList;
	for (UINT i2 = 0; i2 < numPasses; i2++)
		PassesList.append(*(new ItemWrapper(PassRecords.at(i2))));

	for (UINT i = 0; i < InventoryRecords.num_rows(); i++) {
		Row r = InventoryRecords.at(i);
		int invQty = atol(r.at(INV_QTY));
		int invGLID = atol(r.at(INV_GLID));
		if (invQty <= 0)
			continue; // deletes may remain in db
		CGlobalInventoryObj *pCustomObject = 0;
		CGlobalInventoryObj *baseGlobalInventoryObj = (CGlobalInventoryObj *)globalDBRef->GetByGLID(invGLID);

		if (baseGlobalInventoryObj == 0) {
			// Item not in our global inventory, i.e. a custom item. Add it to global inventory.
			pCustomObject = addCustomItem(r, OwnedItemsList, BaseItemsList, PassesList);
			if (pCustomObject == NULL) {
				LogWarn("Global inventory page fault on GLID. Not found in DB. Player:" << playerName << " "
																						<< "GLID:" << r.at(INV_GLID));
				continue;
			}
		}
		CInventoryObj *inv = 0;
		int chgQty = atoi(r.at(INV_CHG_QTY));
		if (r.at(INV_TYPE) == DB_PLAYER_INVENTORY && playerInv && *playerInv) {
			inv = addInventoryItem(playerName, *playerInv, pCustomObject ? *pCustomObject : *baseGlobalInventoryObj, invQty, r.at(INV_ARMED) == DB_TRUE, atoi(r.at(INV_SUB_TYPE)), chgQty);
		} else if (r.at(INV_TYPE) == DB_BANK_INVENTORY && bankInv && *bankInv) {
			inv = addInventoryItem(playerName, *bankInv, pCustomObject ? *pCustomObject : *baseGlobalInventoryObj, invQty, r.at(INV_ARMED) == DB_TRUE, atoi(r.at(INV_SUB_TYPE)), chgQty);
		}

		// The flavor of addInventoryItem called above, adds an inventory item...nothing
		// more.   We need to set the charge now.
		//
		jsVerifyThrow(inv != 0, new KEPException("Couldn't find inv item just added"));
		inv->setCharge(chgQty);
		// else we don't need it
	}
	// since only updatesOnly current inv, only clear current inv
	if (playerInv && *playerInv)
		(*playerInv)->ClearDirty();
	if (bankInv && *bankInv)
		(*bankInv)->ClearDirty();

	// clean up all those NEWS we did at the top of this method.
	//
	ItemWrapperDeleter deleter;
	BaseItemsList.select(deleter);
	OwnedItemsList.select(deleter);
	PassesList.select(deleter);
}

CGlobalInventoryObj *MySqlBase::addCustomItem(mysqlpp::Row &InventoryTuple, KGenericObjList<ItemWrapper> &OwnedItemsList, KGenericObjList<ItemWrapper> &BaseItemsList, KGenericObjList<ItemWrapper> &PassesList) {
	ULONG glid = atol(InventoryTuple.at(INV_GLID));
	ItemSelector itemSelector(glid);

	//NOTE: this function apply to all item types. Although the second getItemData call is currently only
	// apply to UGC mesh items (which have their templates come from DB, too). In the future if we allow
	// derivable UGC mesh clothings we can continue using the same logic. - Yanfeng 06/04/2009

	// Item not in our global inventory, i.e. a custom item. Add it to global inventory.
	CGlobalInventoryObj *pGIO = nullptr;
	CItemObj newItem;

	// First list find the item record in our owned item list
	//
	ItemWrapper *itemWrapper = OwnedItemsList.select(itemSelector);

	if (!itemWrapper) {
		if (!localPlayers()) { // if getting players/items remotely, see if we can get it
			// PUSHED DOWN TO MySqlBase
			pGIO = MySqlBase::getItemFromWeb((GLID)glid);
		}
		return pGIO;
	}

	mysqlpp::Row &ItemRecord = itemWrapper->_row;
	popItemFromTuple(ItemRecord, newItem);
	updateItemPassList(newItem, PassesList);

	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();

	// If we don't need to deal with a base item, then
	// let's be done with it and bug out.
	//
	if (newItem.m_baseGlid == 0) {
		//UGC item with no base item.
		pGIO = new CGlobalInventoryObj();
		newItem.CloneFromBaseGLID(&pGIO->m_itemData, &newItem, glid);
		globalDBRef->AddTail(pGIO);
		return pGIO;
	}

	CGlobalInventoryObj *baseGlobalInventoryObj = NULL;
	baseGlobalInventoryObj = globalDBRef->GetByGLID(newItem.m_baseGlid);

	//Added by Yanfeng to support UGC mesh items (Apr 2009)
	//UGC mesh items does not have a template in global inventory
	if (baseGlobalInventoryObj == NULL) //If no existing item matches base GLID
	{
		//Create a new base item from DB with base GLID
		CItemObj *baseItem = new CItemObj();
		ItemWrapper *BaseItemWrapper = BaseItemsList.select(itemSelector.setGlid(newItem.m_baseGlid));
		if (BaseItemWrapper) {
			mysqlpp::Row &BaseItemRecord = BaseItemWrapper->_row;
			popItemFromTuple(BaseItemRecord, *baseItem);
			updateItemPassList(*baseItem, PassesList);

			baseGlobalInventoryObj = new CGlobalInventoryObj();
			baseGlobalInventoryObj->m_itemData = baseItem;
			globalDBRef->AddTail(baseGlobalInventoryObj);
		} else {
			// base item not in DB?
			ASSERT(FALSE);
			LogError("Base GLID " << newItem.m_baseGlid << " not found int DB while trying to create a base global inventory obj");
			delete baseItem;
		}
	}

	if (ItemRecord && baseGlobalInventoryObj != NULL) {
		pGIO = new CGlobalInventoryObj();
		// Overwrite specifics for our obj
		baseGlobalInventoryObj->m_itemData->CloneFromBaseGLID(&pGIO->m_itemData, &newItem, glid);
		globalDBRef->AddTail(pGIO);
	}
	return pGIO;
}

bool xmlFillInItem(Logger &logger, TiXmlDocument *doc, CItemObj **item, CItemObj **baseItem);
TiXmlElement *xmlParseResultHeader(Logger &logger, TiXmlDocument *doc, const char *wantedChild);
bool fillInItem(TiXmlElement *table, CItemObj *item);

// add an item object to the database using SP that will update if already exists
void addItemToDb(Query &query, Logger &logger, CItemObj *item, bool calledDuringSync = false) {
	if (!calledDuringSync) {
#ifdef SYNC_ALL_AT_START
		return; // if syncing at start, don't add here
#endif
	}

	// add item to local database
	query.reset();
	query << "CALL add_item( %0, %1q, %2q, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14, %15, %16 )";
	query.parse();
	query.execute(item->m_globalID,
		(const char *)item->m_itemName,
		(const char *)item->m_itemDescription,
		item->m_marketCost,
		item->m_sellingPrice,
		item->m_requiredSkillType,
		item->m_requiredSkillLevel,
		(int)item->m_useType,
		item->m_itemType,
		item->m_baseGlid,
		item->m_expiredDuration,
		item->m_permanent,
		item->m_defaultArmedItem,
		item->m_visualRef,
		item->m_derivable,
		item->m_stackable,
		calledDuringSync ? 0 : 1); // for now use creator id to determine if synced or added as one-off
	LogDebug("Synced glid " << item->m_itemName << " (" << item->m_globalID << ") from Kaneva");
}

// sync all the items from the web site that we don't have
void MySqlGameState::syncItemsFromWeb() {
	START_TIMING(m_timingLogger, "DB:getItemFromWeb");

	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;
	std::string baseUrl(m_conn->getItemsUrl());
	std::string::size_type p = baseUrl.find("{0}");
	if (p != std::string::npos) {
		int insertedItems = 0;
		int insertItemsInLoop = 0;
		do {
			Query query = m_conn->query();

			// get the highest GLIDs in each range and call WOK Web to get anything new in chunks of 1000
			query << "select (select max(global_id) from items where global_id < " << UGC_BASE_GLID << "), "
																									   " (select max(global_id) from items where global_id >= "
				  << UGC_BASE_GLID << " and global_id < " << UGC_DO_BASE_GLID << "), "
																				 " (select max(global_id) from items where global_id >= "
				  << UGC_DO_BASE_GLID << ")";

			TRACE_QUERY(query, m_logger);

			StoreQueryResult res = query.store();

			jsVerifyReturnVoid(res.num_rows() > 0); // unless items is missing, gotta get something
			Null<long> max_glid = res.at(0).at(0);
			Null<long> max_ugc_glid = res.at(0).at(1);
			Null<long> max_template_glid = res.at(0).at(2);

			Transaction trans(*m_conn);

			insertItemsInLoop = 0;
			std::string url = baseUrl;
			url.replace(p, 3, numToString(max_glid.is_null ? 0 : max_glid.data, "%d"));
			url += "&ugcId=";
			url += numToString(max_ugc_glid.is_null ? UGC_BASE_GLID - 1 : max_ugc_glid.data, "%d");
			url += "&templateId=";
			url += numToString(max_template_glid.is_null ? UGC_DO_BASE_GLID - 1 : max_template_glid.data, "%d");
			url += "&count=100";
			std::string resultXml;
			size_t resultSize = 0;
			if (GetBrowserPage(url, resultXml, resultSize, httpStatusCode, httpStatusDescription)) {
				// fill in the item, and maybe base item, too
				TiXmlDocument xmlDoc;
				xmlDoc.Parse(resultXml.c_str());
				if (xmlDoc.Error()) {
					LogWarn("Error loading newItem XML " << xmlDoc.ErrorDesc());
				} else {
					// for each item, add to global inventory
					TiXmlElement *table = xmlParseResultHeader(m_logger, &xmlDoc, "items");

					while (table) {
						CItemObj *item = new CItemObj;
						if (fillInItem(table, item)) {
							// add item to local database
							query.reset();
							addItemToDb(query, m_logger, item, true);
							insertItemsInLoop++;
							insertedItems++;
						} else {
							LogWarn("Error getting item elements possible glid = " << item->m_globalID << " possible name = " << item->m_itemName);
							delete item;
							item = 0;
						}
						table = TiXmlHandle(table->NextSibling("items")).ToElement();
					}
				}
			}
			trans.commit();
		} while (insertItemsInLoop > 0);
	} else {
		LogWarn("getItemFromWeb URL missing {0}");
		jsAssert(false); // need global_id
	}
}

bool MySqlGameState::loadNetworkDefinitionsFromDB() {
	int gameId = 0;
	if (m_server) {
		gameId = m_server->GetGameId();
	}

	if (gameId == 0) {
		LogError("Unable to determine gameId of 3D App server");
		return false;
	}

	if (m_conn->dbCommon() == NULL || *m_conn->dbCommon() == '\0') {
		LogWarn("DB_Common not defined in cfg. Skip loading from network_definitions table.");
		return false;
	}

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "SELECT `name`, `value` FROM `" << m_conn->dbCommon() << "`.`network_definitions` WHERE `game_id`=%0";
	query.parse();
	LogTrace(query.str(gameId));

	StoreQueryResult res = query.store(gameId);

	for (UINT i = 0; i < res.num_rows(); i++) {
		std::string name = res[i]["name"];
		std::string value = res[i]["value"];

		if (STLCompareIgnoreCase(name, DEFAULT_PLACE_URL_TAG) == 0) {
			m_defaultPlaceUrl = value;
		} else if (STLCompareIgnoreCase(name, INITIAL_ARENA_ZONE_INDEX_TAG) == 0) {
			m_initialArenaZoneIndex.fromLong(atol(value.c_str()));
		}
	}

	END_TRY_SQL();

	return true;
}

bool MySqlGameState::saveNetworkDefinitionsToDB(const std::string &name, const std::string &value) {
	int gameId = 0;
	if (m_server) {
		gameId = m_server->GetGameId();
	}

	if (gameId == 0) {
		LogError("Unable to determine gameId of 3D App server");
		return false;
	}

	if (m_conn->dbCommon() == NULL || *m_conn->dbCommon() == '\0') {
		LogWarn("DB_Common not defined in cfg. Skip updating network_definitions table.");
		return false;
	}

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "INSERT INTO `" << m_conn->dbCommon() << "`.`network_definitions`(`game_id`, `name`, `value`) VALUES (%0, %1q, %2q) "
		  << "ON DUPLICATE KEY UPDATE `value`=%2q";
	query.parse();
	LogTrace(query.str(gameId, name, value));

	query.execute(gameId, name, value);

	END_TRY_SQL();

	return true;
}

std::string MySqlGameState::GetDefaultPlaceUrl() const {
	// Return DB value if overridden
	if (!m_defaultPlaceUrl.empty()) {
		return m_defaultPlaceUrl;
	}

	// Otherwise use network definitions
	if (multiplayerObj()) {
		return multiplayerObj()->m_defaultPlaceUrl;
	}

	jsAssert(false);
	return std::string();
}

void MySqlGameState::SetDefaultPlaceUrl(const std::string &url) {
	if (url != GetDefaultPlaceUrl()) {
		LogInfo("SetDefaultZone: [" << url << "]");

		//Override default zone
		m_defaultPlaceUrl = url;

		//Save to DB
		saveNetworkDefinitionsToDB(DEFAULT_PLACE_URL_TAG, url);
	}
}

ZoneIndex MySqlGameState::GetInitialArenaZoneIndex() const {
	// Return DB value if overridden
	if (m_initialArenaZoneIndex.toLong() != 0)
		return m_initialArenaZoneIndex;

	// Otherwise use network definitions
	if (multiplayerObj())
		return multiplayerObj()->m_initialArenaZoneIndex;

	return ZoneIndex(0);
}

void MySqlGameState::SetInitialArenaZoneIndex(ZoneIndex zi) {
	if (zi != GetInitialArenaZoneIndex()) {
#ifndef REFACTOR_INSTANCEID
		LogInfo("SetInitialArenaZoneIndex: [" << zi << "]");
#else
		LogInfo("SetInitialArenaZoneIndex: [" << zi.toLong() << "]");
#endif

		//Override initial arena zone index
		m_initialArenaZoneIndex = zi;

		std::stringstream ssZoneIndex;
		ssZoneIndex << zi.toLong();

		//Save to DB
		saveNetworkDefinitionsToDB(INITIAL_ARENA_ZONE_INDEX_TAG, ssZoneIndex.str());
	}
}

void MySqlGameState::SavePlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex, float x, float y, float z, float rx, float ry, float rz) {
	MySqlBase::savePlayerSpawnPoint(playerId, zoneIndex.getClearedInstanceId(), zoneIndex.GetInstanceId(), x, y, z, rx, ry, rz);
}

void MySqlGameState::DeletePlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex) {
	MySqlBase::deletePlayerSpawnPoint(playerId, zoneIndex.getClearedInstanceId(), zoneIndex.GetInstanceId());
}

bool MySqlGameState::GetPlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex, float &x, float &y, float &z, float &rx, float &ry, float &rz) {
	return MySqlBase::getPlayerSpawnPoint(playerId, zoneIndex.getClearedInstanceId(), zoneIndex.GetInstanceId(), x, y, z, rx, ry, rz);
}

bool MySqlGameState::GetParentZoneInfo(KGP_Connection *conn, unsigned zoneInstanceId, unsigned zoneType, unsigned &parentZoneInstanceId, unsigned &parentZoneIndex, unsigned &parentZoneType, unsigned &parentCommunityId) {
	parentZoneInstanceId = parentZoneIndex = parentZoneType = parentCommunityId = 0;

	// Homes and communities only
	if (zoneType != CI_CHANNEL && zoneType != CI_HOUSING) {
		return false;
	}

	// Check cache
	{
		std::lock_guard<std::mutex> lock(m_parentZoneInfoMutex);
		auto itr = m_parentZoneInfoMap.find(zoneInstanceId);
		if (itr != m_parentZoneInfoMap.end()) {
			// Found
			parentZoneInstanceId = itr->second.parentZoneInstanceId;
			parentZoneIndex = itr->second.parentZoneIndex;
			parentZoneType = itr->second.parentZoneType;
			parentCommunityId = itr->second.parentCommunityId;
			return true;
		}
	}

	if (conn == nullptr) {
		// Use our connection if not provided
		conn = m_conn;
	}

	// Not found - query DB
	bool res = MySqlBase::getParentZoneInfo(conn, zoneInstanceId, zoneType, parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId);
	if (res) {
		// Cache it - note that result can be 0 for a standard alone zone or a parent zone
		std::lock_guard<std::mutex> lock(m_parentZoneInfoMutex);
		m_parentZoneInfoMap.insert(std::make_pair(zoneInstanceId, ParentZoneInfo{ parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId }));
	}

	return res;
}

void MySqlGameState::UpdateGameInventorySync(int kanevaUserId, int gameId, ULONG globalId, int inventoryCount) {
	CHECK_CONNECTION;
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << "UPDATE game_inventories_sync SET quantity = %0 WHERE game_id = %1 and global_id = %2 and inventory_type = %3q and inventory_sub_type = %4 and kaneva_user_id = %5";
	query.parse();
	query.execute(inventoryCount, gameId, globalId, DB_PLAYER_INVENTORY, IT_NORMAL, kanevaUserId);
	END_TRY_SQL_LOG_ONLY(m_conn);
}

ULONG MySqlGameState::AddGameItemPlacement(const ZoneIndex &zoneIndex, ULONG doGlid, const Vector3f &pos, const Vector3f &ori, int ownerId, int gameItemId) {
	int zoneId = zoneIndex.getClearedInstanceId();
	int instanceId = zoneIndex.GetInstanceId();

	ULONG placementId = 0;

	// calc slide vector
	Vector3f up(0.0f, 1.0f, 0.0f);
	Vector3f slide = up.Cross(ori);
	slide.Normalize();

	CHECK_CONNECTION

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();
	query << "CALL insert_dynamic_object( %0, %1, %2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14, %15, %16, %17, %18, @objSerialNum ); SELECT @objSerialNum;";
	query.parse();

	StoreQueryResult res = query.store(
		(int)ownerId, (int)zoneId, (int)instanceId,
		(int)0, (int)doGlid, IT_UNLIMITED,
		pos.x, pos.y, pos.z, ori.x, ori.y, ori.z, slide.x, slide.y, slide.z,
		(int)0, // textureId
		(int)0, // attachable
		(int)0, // expiration
		(int)0); // assetGroupId

	res = query.store_next(); // result set for the select
	if (res.num_rows() != 0) {
		placementId = res.at(0).at(0);
	}
	query.reset();
	query << "INSERT INTO dynamic_object_parameters (obj_placement_id, param_type_id, value) "
		  << "VALUES (%0, %1, %2) "
		  << "ON DUPLICATE KEY UPDATE value = %2";
	query.parse();
	query.execute((int)placementId, (int)PARAM_GAME_ITEM_ID, (int)gameItemId);
	query.reset();

	trans.commit();

	END_TRY_SQL_LOG_ONLY(m_conn);

	return placementId;
}

void MySqlGameState::ClearZoneDOMemCache(ZoneIndex const &zi) {
	long tempInstanceId = 1;
	if (!zi.isPermanent() && !zi.isArena()) {
		tempInstanceId = zi.GetInstanceId();
	}
	std::string key = "wok.zz.";
	key = key + numToString(zi.getClearedInstanceId(), "%d") + "," + numToString(tempInstanceId, "%d");
	std::string results;
	DWORD rcode;
	std::string rdesc;

	URL curl = URL::parse(m_conn->getMemcacheAPIUrl());
	curl.setParam("action", "dropCompound");
	curl.setParam("key", key);
	std::string url = curl.encodedString();
	LogDebug("Calling[" << url.c_str() << "]");
	size_t resultSize = 0;
	if (!GetBrowserPage(url, results, resultSize, rcode, rdesc)) {
		LogError("Invalid response[" << rcode << "][" << rdesc << "][" << results.c_str() << "] for url [" << url << "]");
	}
}

TimeMs MySqlGameState::GetEffectDurationMs(KGP_Connection *conn, GLID globalId) {
	{
		std::lock_guard<std::mutex> lock(m_effectDurationMutex);
		auto itr = m_effectDurationMs.find(globalId);
		if (itr != m_effectDurationMs.end()) {
			return itr->second;
		}
	}

	TimeMs durationMs = 0.0;

	CHECK_CONNECTION2(conn)

	BEGIN_TRY_SQL();

	Query query = conn->query();
	query << "SELECT value+0 FROM item_parameters WHERE global_id=%0 AND param_type_id=%1";
	query.parse();

	StoreQueryResult res = query.store((int)globalId, (int)PARAM_TYPE_EFFECT_DURATION);

	if (res.num_rows() != 0) {
		durationMs = (TimeMs)res.at(0).at(0);
		LogDebug(globalId << ": " << durationMs << " ms");
		std::lock_guard<std::mutex> lock(m_effectDurationMutex);
		m_effectDurationMs.insert(std::make_pair(globalId, durationMs));
	}

	END_TRY_SQL_LOG_ONLY(conn); // No retries - recoverable at future queries

	return durationMs;
}

void MySqlGameState::AddEffectDurationMs(const std::map<GLID, TimeMs> &effectDurationMap) {
	std::lock_guard<std::mutex> lock(m_effectDurationMutex);
	m_effectDurationMs.insert(effectDurationMap.begin(), effectDurationMap.end());
}

} // namespace KEP