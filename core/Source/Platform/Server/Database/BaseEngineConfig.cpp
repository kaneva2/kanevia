///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "common/include/kepcommon.h"
#include "BaseEngineConfig.h"
#include "DBConfigValues.h"
#include "MySqlUtil.h"
#include "BaseEngine.h"
#include "GetSet.h"
#include "common/KEPUtil/ExtendedConfig.h"

#define GET_GETSET_COLS "`value_name`, `type`, `int_value`, `double_value`, `string_value`,`engine_instance_id`, `engine_type`"

using namespace KEP;
using namespace mysqlpp;
static Logger alert_logger(Logger::getInstance(L"Alert"));
static const UINT EXCLUSIVE_THREAD = 0; //Matches definition in KEPServerImpl.cpp

BaseEngineConfigContainer::BaseEngineConfigContainer(const std::vector<StringPairT>& paramSet) :
		m_logger(Logger::getInstance(L"BaseEngineConfig")), m_paramSet(paramSet)

{
}

BaseEngineConfigContainer::~BaseEngineConfigContainer() {
	for (ConfigVector::iterator i = m_configs.begin(); i != m_configs.end(); i++) {
		delete (*i);
	}
}

Traversable<IKEPConfig*>& BaseEngineConfigContainer::traverse(Traverser<IKEPConfig*>& traversed) {
	for (ConfigVector::iterator i = m_configs.begin(); i != m_configs.end(); i++) {
		traversed.evaluate((*i));
	}

	return *this;
}

// Update existing KEPConfigs with new values. Create new KEPConfig if not previously exists and incoming value is for a specific server.
void BaseEngineConfigContainer::SetConfigValue(bool commonConfig, eEngineType engineType, LONG engineInstanceId, const std::string& valueName, LONG valueType, const int* pIntValue, const double* pDoubleValue, const char* stringValue) {
	if (!commonConfig) {
		// Server-specific engine config
		ConfigVector::value_type config = NULL;

		for (ConfigVector::iterator i = m_configs.begin(); i != m_configs.end(); ++i) {
			// Look for existing KEPConfig by engine type and instance ID
			eEngineType engType = (eEngineType)(*i)->Get(ENGINE_TYPE_TAG, 0);
			LONG engInstanceId = (LONG)(*i)->Get(ENGINE_INSTANCE_ID_TAG, 0);
			if (engType == engineType && engInstanceId == engineInstanceId) {
				config = *i;
				break;
			}
		}

		// Create new KEPConfig if not found
		if (config == NULL) {
			KEPConfigAdapter* newConfig;
			config = newConfig = new KEPConfigAdapter();
			newConfig->impl().paramSet().set(m_paramSet);
			newConfig->New("EngineConfig");
			newConfig->SetValue(ENGINE_TYPE_TAG, (LONG)engineType);
			newConfig->SetValue(ENGINE_INSTANCE_ID_TAG, engineInstanceId);
			m_configs.push_back(newConfig);
		}

		// Set config value
		SetConfigGetSetValue(config, valueName, valueType, pIntValue, pDoubleValue, stringValue);
	} else {
		// Common configuration (either global or per engine type)
		// For each existing KEPConfig
		for (ConfigVector::iterator i = m_configs.begin(); i != m_configs.end(); ++i) {
			// If engine type matches (0 or equal), and the value is not previously set, store common config value.
			//ValueExists does not appear to work. Use alternative
			std::string tmp;
			eEngineType engType = (eEngineType)(*i)->Get(ENGINE_TYPE_TAG, 0);
			if ((engineType == eEngineType::None || engType == engineType) && !(*i)->GetValue(valueName.c_str(), tmp, "")) {
				SetConfigGetSetValue((*i), valueName, valueType, pIntValue, pDoubleValue, stringValue);
			}
		}
	}
}

// Set config value based on type
void BaseEngineConfigContainer::SetConfigGetSetValue(ConfigVector::value_type config, const std::string& valueName, LONG valueType, const int* pIntValue, const double* pDoubleValue, const char* stringValue) {
	switch (valueType) {
		case gsInt:
			jsVerifyDo(pIntValue != NULL, break);
			if (pIntValue)
				config->SetValue(valueName.c_str(), *pIntValue);
			break;
		case gsDouble:
			jsVerifyDo(pDoubleValue != NULL, break);
			if (pDoubleValue)
				config->SetValue(valueName.c_str(), *pDoubleValue);
			break;
		case gsString:
			jsVerifyDo(stringValue != NULL, break);
			if (stringValue)
				config->SetValue(valueName.c_str(), stringValue);
			break;
		default:
			jsAssert(false);
			break;
	}
}

void BaseEngineConfigContainer::AddConfig(IKEPConfig* newConfig, const char* configPath) {
	// see if we're replacing an existing on
	ConfigVector::iterator i = m_configs.begin();
	for (; i != m_configs.end(); i++) {
		std::string class1, class2;
		int port1 = 0, port2 = 0;
		int id1 = 0, id2 = 0;

		// all servers have uniqe class/port except dispatcher client since goes to same port, use serverId instead
		if ((*i)->GetValue(CLASSNAME_TAG, class1) && class1 == "DispatcherEngineClient") {
			if ((*i)->GetValue(SERVER_ID_TAG, id1) && newConfig->GetValue(SERVER_ID_TAG, id2) &&
				id1 == id2) {
				LOG4CPLUS_INFO(m_logger, "XML file overriding DB settings for blade engine " << class1 << " instance " << id1 << " by " << configPath);
				m_configs.erase(i);
				break;
			}
		} else if ((*i)->GetValue(CLASSNAME_TAG, class1) && newConfig->GetValue(CLASSNAME_TAG, class2) &&
				   class1 == class2 &&
				   (*i)->GetValue(IPPORT_TAG, port1) && newConfig->GetValue(IPPORT_TAG, port2) &&
				   port1 == port2) {
			LOG4CPLUS_INFO(m_logger, "XML file overriding DB settings for blade engine " << class1 << " instance " << port1 << " by " << configPath);
			m_configs.erase(i);
			break;
		}
	}

	m_configs.push_back(newConfig);
}

void BaseEngineConfigContainer::ClearAllDirtyFlags() {
	for (ConfigVector::iterator i = m_configs.begin(); i != m_configs.end(); i++) {
		(*i)->SetDirty(false); // just loaded it
	}
}

void BaseEngineConfigContainer::Dump() {
#ifdef _DEBUG
	for (ConfigVector::iterator i = m_configs.begin(); i != m_configs.end(); i++) {
		(*i)->Print();
	}
#endif
}

BaseEngineConfigLoader::BaseEngineConfigLoader(const std::string& pathXml, const DBConfig& dbConfig) :
		m_pathXml(pathXml),
		m_dbConfig(dbConfig),
		m_conn(new KGP_Connection(use_exceptions)),
		m_initialized(false),
		m_logger(Logger::getInstance(L"BaseEngineConfig")) {
}

BaseEngineConfigLoader::~BaseEngineConfigLoader() {
	if (m_conn != 0)
		delete m_conn;
}

void BaseEngineConfigLoader::initialize() {
	initializeDB(PathXml());
#ifndef DECOUPLE_DBCONFIG
	// DISABLED for now because we are well past version 49 of the schema.  In other words, in theory it's right
	// but effectively this will never fail.  Left here because it this would be a nice component of DataModel
	// e.g.
	// CHAINED( UnsupportedVersionException )
	// DataModel::assertVersion(long version) throws UnsupportedVersionException
	//
	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << " SELECT FLOOR(IFNULL(MAX(version), 0)), NOW(),  "
		  << " SUM(if (interface_change = 'T', 1, 0) AND version > %0) AS interface_change"
		  << " FROM schema_versions";
	query.parse();
	StoreQueryResult r = query.store(MIN_DB_VERSION);
	if ((long)(r.at(0).at(0)) < MIN_DB_VERSION || (long)(r.at(0).at(2)) > 0) {
		throw new KEPException(loadStrPrintf(IDS_DB_VERSION, (long)(r.at(0).at(0)), MIN_DB_VERSION), E_INVALIDARG);
	}

	// log db timestamp to help with diffing log vs db time
	LOG4CPLUS_INFO(m_logger, "Database timestamp is " << (std::string)(r.at(0).at(1)) << " version floor is " << (long)(r.at(0).at(0)));

	END_TRY_SQL();
#endif
	m_initialized = true;
}

void BaseEngineConfigLoader::initializeDB(const std::string& pathXml) {
	my_init(); // just in case not called by mysql++

	// read the config file for the database settings
	std::string dbServer, dbUser, dbPassword, dbClearPassword;
	int dbPort = 0;
	bool dontShowDlg; // we don't care about this one

	if (!m_dbConfig.Get(dbServer, dbPort, m_dbName, dbUser, dbPassword, dontShowDlg)) {
		throw new KEPException(loadStrPrintf(IDS_DB_BAD_CONFIG, PathAdd(pathXml, "db.cfg").c_str()), E_INVALIDARG);
	}

	std::string errMsg;
	try {
		m_conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
		m_conn->set_option(new ReconnectOption(true));
		m_conn->connect(m_dbName.c_str(), dbServer.c_str(), dbUser.c_str(), dbPassword.c_str(), dbPort);
	} catch (const mysqlpp::Exception& er) {
		errMsg = er.what();
		DELETE_AND_ZERO(m_conn);
	}

	if (!m_conn) {
		throw new KEPException(loadStrPrintf(IDS_DB_CONNECT_ERROR, errMsg.c_str()), E_INVALIDARG);
	}

	LOG4CPLUS_INFO(m_logger, loadStr(IDS_DB_CONNECT) << dbServer << ":" << dbPort << ", " << m_dbName << ", " << dbUser);
}

#ifdef ENABLEBLADEDBCFG
bool BaseEngineConfigLoader::LoadAllDbConfigs(int serverInstanceId, BaseEngineConfigContainer& configs) {
	bool ret = false;

	//Catch any KEPExceptions thrown by the mysql macros.
	//This will allow us to move on and attempt to load
	//configs via xml instead of stalling in KEPServerImpl.
	try {
		if (!m_initialized)
			initialize();

		char hostname[255];
		if (::gethostname(hostname, 255) == 0) {
			BEGIN_TRY_SQL();

			getAllGetSetMetaData(hostname, serverInstanceId, configs);

			END_TRY_SQL();

			ret = true;
		}
	} catch (KEPException* e) {
		LOG4CPLUS_WARN(m_logger, "BaseEngineConfigLoader::LoadAllDbConfigs failed. " << e->ToString());
		e->Delete();
	}

	return ret;
}

void BaseEngineConfigLoader::getAllGetSetMetaData(char* serverName, int serverInstanceId, BaseEngineConfigContainer& configs) {
	// Make a union query in following order:
	Query query = m_conn->query();
	//First get values for OVERRIDES for this server
	query << "SELECT " GET_GETSET_COLS ", 0 FROM `getset_server_config_values` "
		  << " WHERE server_name=%0q and server_instance_id = %1";
	//Get default values for a SPECIFIC server host and engine type (e.g. determine ServerEngine IpAddress for each server box)
	query << " UNION ALL ";
	query << "SELECT " GET_GETSET_COLS ", 1 FROM `getset_server_config_values` "
		  << " WHERE server_name=%0q AND engine_type<>0 AND server_instance_id=0";
	//Then get default values for SPECIFIC engine type
	query << " UNION ALL ";
	query << "SELECT " GET_GETSET_COLS ", 1 FROM `getset_server_config_values` "
		  << " WHERE server_name='' AND engine_type<>0";
	//At last get default values for GENERAL engines (engine_type==0)
	query << " UNION ALL ";
	query << "SELECT " GET_GETSET_COLS ", 1 FROM `getset_server_config_values` "
		  << " WHERE server_name='' AND engine_type=0 ";
	query.parse();
	LOG4CPLUS_TRACE(m_logger, query.str(serverName, serverInstanceId));
	StoreQueryResult results = query.store(serverName, (SQLTypeAdapter)serverInstanceId); // must cast second param to avoid confusion with char,size_t verson of store.

	Row row;
	Row::size_type i;

	for (i = 0; i < results.size(); ++i) {
		row = results.at(i);

		std::string valueName = row.at(0);

		if (!valueName.empty()) {
			bool isCommonConfig = ((LONG)row.at(7)) != 0;
			LONG engineType = row.at(6);
			LONG engineInstanceId = row.at(5);
			LONG valueType = row.at(1);
			int intValue = row.at(2).is_null() ? 0 : row.at(2);
			double doubleValue = row.at(3).is_null() ? 0 : row.at(3);
			std::string stringValue = row.at(4).is_null() ? "" : row.at(4);

			configs.SetConfigValue(
				isCommonConfig,
				(eEngineType)engineType,
				engineInstanceId,
				valueName,
				valueType,
				row.at(2).is_null() ? NULL : &intValue,
				row.at(3).is_null() ? NULL : &doubleValue,
				row.at(4).is_null() ? NULL : stringValue.c_str());
		}
	}

	configs.ClearAllDirtyFlags();
}
#endif