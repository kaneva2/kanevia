///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Events/IBackgroundDBEvent.h"

#include <queue>
#include <vector>
#include <jsSemaphore.h>

namespace KEP {

class MTEventQueue {
public:
	MTEventQueue(const char *instanceName
#ifdef FIX_MTQUEUE
		,
		bool optimizeForSingleThread = false
#endif
		) :
			m_instanceName(instanceName)
#ifdef FIX_MTQUEUE
			,
			m_optimizeForSingleThread(optimizeForSingleThread)
#endif
	{
	}

	/// incr semaphore a bunch to have all waiters exit
	void Shutdown() {
		m_sema.increment(99);
	}

	/// queue and event to queue and release any waiters
	/// returns the number of items now in queue
	ULONG QueueEvent(IBackgroundDBEvent *e) {
		ULONG ret = 0;
		std::lock_guard<fast_recursive_mutex> lock(getSync());
		e->incQueuedCnt();
#ifdef FIX_MTQUEUE
		if (!m_optimizeForSingleThread) {
#endif
			ret = m_queue.size() + m_holdingQueue.size() + 1;

			// is there already one in the queue for this player
			if (e->kanevaUserId > 0) {
				for (const auto dbInt : m_playersWorking) {
					if (dbInt == e->kanevaUserId) {
						// since already processing one for this player, hold it back
						m_holdingQueue.push_back(e);
						return ret;
					}
				}
			}

			// not found, queue it up
			m_playersWorking.push_back(e->kanevaUserId);
#ifdef FIX_MTQUEUE
		}
#endif
		m_queue.push(e);
#ifdef FIX_MTQUEUE
		if (m_optimizeForSingleThread) {
			ret = m_queue.size();
		}
#endif
		m_sema.increment();
		return ret;
	}

	IBackgroundDBEvent *WaitForNextEvent(ULONG waitMs = INFINITE_WAIT) {
		IBackgroundDBEvent *ret = 0;
		if (m_sema.waitAndDecrement(waitMs) == WR_OK) {
			std::lock_guard<fast_recursive_mutex> lock(getSync());
			if (!m_queue.empty()) // could be empty on shutdown
			{
				ret = m_queue.front();
				m_queue.pop();
			}
		}
		return ret;
	}

	/**
	* Determine the number of elements in the queue.
	*/
	unsigned long size() const {
		return m_queue.size();
	}

	/// called when event is finished processing, dec the event
	/// queued count and see if any pending ones for this player
	void DecQueuedOnEvent(IBackgroundDBEvent *e) {
		int eventKanevaUserId = e->kanevaUserId;
		e->Release();
		e = nullptr;

#ifdef FIX_MTQUEUE
		if (m_optimizeForSingleThread) {
			return;
		}
#endif
		// see if any other events for this player are in queue and release one
		std::lock_guard<fast_recursive_mutex> lock(getSync());
		bool found = false;
		for (auto i = m_holdingQueue.begin(); i != m_holdingQueue.end(); i++) {
			if ((*i)->kanevaUserId == eventKanevaUserId) {
				// found one, put it in workers' queueue
				m_queue.push(*i);
				m_sema.increment();
				m_holdingQueue.erase(i);
				found = true;
				break;
			}
		}

		if (!found) {
			for (auto itr = m_playersWorking.begin(); itr != m_playersWorking.end(); itr++) {
				if (*itr == eventKanevaUserId) {
					m_playersWorking.erase(itr);
					break;
				}
			}
		}
	}

	const std::string &instanceName() const {
		return m_instanceName;
	}

	~MTEventQueue() {
		std::lock_guard<fast_recursive_mutex> lock(getSync());

		while (!m_queue.empty()) {
			IBackgroundDBEvent *e = m_queue.front();
			m_queue.pop();
			e->Release();
		}

		for (const auto &pEvent : m_holdingQueue) {
			pEvent->Release();
		}
	}

private:
	typedef std::queue<IBackgroundDBEvent *> DBEventQueue;
	typedef std::vector<IBackgroundDBEvent *> DBEventVector;
	typedef std::vector<int> DBIntVector;
	jsSemaphore m_sema;
	fast_recursive_mutex m_sync; //< protect queues
	fast_recursive_mutex &getSync() {
		return m_sync;
	}
	DBEventQueue m_queue; //< events to be processes
	DBEventVector m_holdingQueue; //< events for players who already have an event in process
	DBIntVector m_playersWorking; //< players who are being processed now
	std::string m_instanceName; //< for logging

public:
#ifdef FIX_MTQUEUE
	bool m_optimizeForSingleThread;
#endif
};

} // namespace KEP