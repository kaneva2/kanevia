///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Pool.h"
#include "Core/Util/SQLConnection.h"
#include "Core/Crypto/CryptDecrypt.h"
#include "common/include/kepcommon.h"
#include "Common/Include/KEPMacros.h"
#include "common/include/config.h"

#include "Common/Include/KEPHelpers.h"
#include "Common/Include/IKEPConfig.h"
#include "Common/Include/KEPException.h"
#include "Common/Include/DBStrings.h"
#include "errmsg.h" // mysql 5
#include "mysqld_error.h" // mysql 5
#include "Common/Include/DBConfigValues.h"
#include "Common/Include/NetworkCfg.h"
#include "Common/Include/LogHelper.h"
#include "Common/KEPUtil/ExtendedConfig.h"

// mysql++
#pragma warning(push) // mysql++.h reenables warning 4244 and maybe others. We don't want mysql++ to be messing with our chosen warning level.
#include <mysql++/mysql++.h>
#pragma warning(pop)
#include <mysql++/transaction.h>
#include <mysql++/ssqls.h>

namespace mysqlpp {
	class Connection;
}

namespace KEP {

static const char* const DB_TRUE = "T";
static const char* const DB_FALSE = "F";

///////////////////////////////////////////////////////////
/// helper class mainly to track parameters of connection
/// so we can log them on exception
class KGP_Connection : public SQLConnection {
public:
	typedef std::shared_ptr<KGP_Connection> ConnectionPtr;

	// overrides this just to save off db, server, port, user for use in exceptions
	KGP_Connection(const char* db, const char* server = 0, const char* user = 0,
		const char* password = 0, unsigned int port = 3306, bool localPlayers = true) :
			SQLConnection(db, server, user, password, port),
			m_neverSwitchServers(false), m_backgroundThreadCount(1), m_localPlayers(localPlayers), _memcacheAPI() {
	}

	KGP_Connection(bool te = true) :
			SQLConnection(te), m_neverSwitchServers(false), m_backgroundThreadCount(1) {
	}

	KGP_Connection(const KGP_Connection& src) {
		operator=(src);
	}

	KGP_Connection& operator=(const KGP_Connection& src) {
		SQLConnection::operator=(src);
		if (&src != this) {
			m_dbCommon = src.m_dbCommon;
		}
		return *this;
	}

	// Used exclusively by script server.
	// I don't believe the concept of "common" is valid anymore
	bool connect(const char* _db, const char* _dbCommon, const char* _server, const char* _user, const char* _password, unsigned int _port) {
		if (_dbCommon) {
			m_dbCommon = _dbCommon;
		}

		return connect(_db, _server, _user, _password, _port);
	}

	/// pass thru to base class, saving off params
	virtual bool connect(const char* _db = 0, const char* _server = 0, const char* _user = 0, const char* _password = 0, unsigned int _port = 3306) {
		return SQLConnection::connect(_db, _server, _user, _password, _port);
	}

	/// load the configuration from the config file already opened
	void loadConfig(const IKEPConfig& cfg, const char* gameDir) {
		if (cfg.Get(DB_DISABLED, 0) != 0)
			throw new KEPException(loadStr(IDS_DATABASE_DISABLED), S_OK);

		// we can default only these
		std::string dbClearPassword;
		int port;
		std::string srvr;
		std::string db;
		std::string user;
		std::string password;

		cfg.GetValue(DB_PORT, port, 3306);
		cfg.GetValue(DB_SERVER, srvr, "localhost");

		if (!cfg.GetValue(DB_NAME, db) || !cfg.GetValue(DB_USER, user) || !cfg.GetValue(DB_PASSWORD, password)) {
			throw new KEPException(loadStrPrintf(IDS_DB_BAD_CONFIG, PathAdd(gameDir, DB_CFG_NAME).c_str()), E_INVALIDARG);
		}

		if (cfg.GetValue("DB_PasswordClear", dbClearPassword)) {
			SQLConnection::setPassword(dbClearPassword);
		} else {
			// decrypt password
			std::string szPassword;
			if (SetupCryptoClient() && DecodeAndDecryptString(password.c_str(), szPassword, DB_KEY))
				SQLConnection::setPassword(szPassword);
		}

		SQLConnection::setDb(db)
			.setServer(srvr)
			.setUser(user)
			.setPort((unsigned short)port);

		m_localPlayers = cfg.Get(DB_LOCAL_PLAYERS, true);
		m_neverSwitchServers = cfg.Get(DB_NEVERSWITCHSERVERS, false);
		m_backgroundThreadCount = MAX(MIN(cfg.Get(DB_BACKGROUNDTHREADS, 2), 100), 1);
		std::string dbCommon = cfg.Get(DB_COMMON, m_dbCommon.c_str());
		if (dbCommon.empty() || isValidDBName(dbCommon)) {
			m_dbCommon = dbCommon;
		}

		// And now for some reason, load the network configuration into this database connection
		// class.
		//
		KEPConfigAdapter netcfg;
		netcfg.Load(PathAdd(gameDir, NETCFG_CFG));
		netcfg.GetValue(GETPLAYERURL_TAG, m_getPlayerUrl, NET_DEFAULT_GET_PLAYER_URL);
		netcfg.GetValue(GETITEMSURL_TAG, m_getItemsUrl, NET_DEFAULT_GET_ITEMS_URL);
		netcfg.GetValue(GETPLAYLISTURL_TAG, m_getPlaylistUrl, NET_DEFAULT_GET_PLAYLIST_URL);
		netcfg.GetValue(SPACEIMPORTURL_TAG, m_spaceImportUrl, NET_DEFAULT_SPACEIMPORT_URL);
		netcfg.GetValue(INVENTORYUPDATER_TAG, m_inventoryUpdaterUrl, NET_DEFAULT_INVENTORYUPDATER_URL);
		netcfg.GetValue("MemCacheAPIUrl", _memcacheAPI, NET_DEFAULT_MEMCACHAPI_URL);
	}

	const char* dbCommon() const { return m_dbCommon.c_str(); }

	bool neverSwitchServers() const { return m_neverSwitchServers; }
	int backgroundThreadCount() const { return m_backgroundThreadCount; }
	const std::string& getPlayerUrl() const { return m_getPlayerUrl; }
	const std::string& getItemsUrl() const { return m_getItemsUrl; }
	const std::string& getPlaylistUrl() const { return m_getPlaylistUrl; }
	const std::string& getSpaceImportUrl() const { return m_spaceImportUrl; }
	const std::string& getInventoryUpdaterUrl() const { return m_inventoryUpdaterUrl; }
	const std::string& getMemcacheAPIUrl() const { return _memcacheAPI; }
	void setMemcacheAPIUrl(const std::string& url) { _memcacheAPI = url; }
	bool localPlayers() const { return m_localPlayers; }

private:
	std::string m_dbCommon; // Shared DB schema: e.g. kgp_common.
	bool m_neverSwitchServers;
	int m_backgroundThreadCount;
	std::string m_getPlayerUrl; //< url for getting a acct+player from Kaneva Web API instead of DB
	std::string m_getItemsUrl;
	std::string m_getPlaylistUrl;
	std::string m_spaceImportUrl;
	std::string m_inventoryUpdaterUrl;
	bool m_localPlayers; //< do we load players from local db, or web?

	// Another example of this class being subverted.
	// Simply the easiest way to pass configuration data to MySqlDb
	std::string _memcacheAPI;
};

class KGPConnectionFactory : public BasicFactory<KGP_Connection> {
public:
	KGPConnectionFactory(const std::string& host, const std::string& user, const std::string& password, const std::string& database, unsigned short port = 3306) :
			_host(host), _port(port), _user(user), _password(password), _db(database) {}

	KGP_Connection* create(void) {
		// We cannot use this constructor because it automatically connects
		// and we cannot set the reconnect option after it has connected.
		// So Create, set option, connect
		//
		// BTW, it would be my preference that we didn't call the connecting constructor
		// on the override.  Thus forcing the connection call.  However, this could
		// break something, so it will stay for now.  At some point in the db layer
		// development, this will be isolated enough to safely change this behavior.
		// Executor can also address calls to mysqlpp::Connection::thread_start()/end()
		// For now this is a caveat and must be addressed by borrowers.
		//
		KGP_Connection* pc = new KGP_Connection();
		pc->connect(_db.c_str(), _host.c_str(), _user.c_str(), _password.c_str(), _port);
		if (!pc->connected())
			throw ChainedException(ErrorSpec(pc->errnum(), pc->error()));

		return pc;
	}

	std::string _host;
	unsigned short _port;
	std::string _user;
	std::string _password;
	std::string _db;
};
typedef BasicPool<KGP_Connection> KGPConnectionPool;

#pragma region Helper Macros

#define TRACE_ALL_QUERIES
#ifdef TRACE_ALL_QUERIES
#define TRACE_QUERY(q, l) LOG4CPLUS_TRACE(l, q.str());
#else
#define TRACE_QUERY(q, l)
#endif

// These macros should be deprecated in favor of the "execSQL()" method in SQLConnection.h.
// "execSQL()" provides identical functionality to the BEGIN_TRY_SQL flow without
// depending on KEPUtil.
#define CHECK_CONNECTION CHECK_CONNECTION2(m_conn)
#define CHECK_CONNECTION2(conn)                                                    \
	if (conn == 0 || !conn->connected()) {                                         \
		if (conn)                                                                  \
			conn->ping();                                                          \
		if (conn == 0 || !conn->connected())                                       \
			throw new KEP::KEPException(loadStr(IDS_DB_NOT_CONNECTED), conn->errnum()); \
	}

// added retry logic for connection loss
#define RETRY_LIMIT 2

#define BEGIN_TRY_SQL_FOR(sqlTaskNameValue) \
	const char* sqlTaskName = (sqlTaskNameValue); \
	int __sqlTries__ = 0; \
	for (; __sqlTries__ < RETRY_LIMIT; __sqlTries__++) try {

#define BEGIN_TRY_SQL() BEGIN_TRY_SQL_FOR(__FUNCTION__)

#define SQL_TASK(sqlTaskNameValue) \
	sqlTaskName = sqlTaskNameValue;

#define SQL_TASK_REF sqlTaskName

// TODO: Note that this macro, unlike the END_TRY_SQL_LOG_ONLY, does not allow for the
// specification of the connection, but rather demands that a variable call m_conn
// be in scope.  This should modified to accept the name of the KGP_Connection instance
//
#define END_TRY_SQL()																				                                               \
	break;                                                                                                                                         \
	}                                                                                                                                              \
	catch (const mysqlpp::BadQuery& er) {                                                                                                          \
		int sqlErr = m_conn->errnum();                                                                                                             \
		if (sqlErr != CR_SERVER_LOST) {                                                                                                            \
			std::string s = formatBadQuery(sqlTaskName, er.what(), er.query_string(), m_conn->server(), m_conn->port(), m_conn->db(), m_conn->user()); \
			LogWarn(s);                                                                                                                            \
			__sqlTries__ = 100;                                                                                                                    \
			throw new KEP::KEPException(s, er.errnum());                                                                                           \
		} else {                                                                                                                                   \
			LogWarn("Lost connection in " << sqlTaskName << " retrying since got " << sqlErr << " " << er.what());                                 \
		}                                                                                                                                          \
	}                                                                                                                                              \
	catch (const mysqlpp::BadConversion& er) {                                                                                                     \
		std::string s = loadStrPrintf(IDS_DB_CONVERSION_ERROR, sqlTaskName, er.what(), er.retrieved, er.actual_size);                              \
		LogWarn(s);                                                                                                                                \
		__sqlTries__ = 100;                                                                                                                        \
		throw new KEP::KEPException(s, m_conn->errnum() != 0 ? m_conn->errnum() : E_INVALIDARG);                                                   \
	}                                                                                                                                              \
	catch (const mysqlpp::Exception& er) {                                                                                                         \
		std::string s = loadStrPrintf(IDS_DB_ERROR, sqlTaskName, er.what());                                                                       \
		LogWarn(s);                                                                                                                                \
		throw new KEP::KEPException(s, m_conn->errnum() != 0 ? m_conn->errnum() : E_INVALIDARG);                                                   \
	}                                                                                                                                              \
	if (__sqlTries__ >= RETRY_LIMIT) {                                                                                                             \
		LogWarn("Retry limit hit for " << sqlTaskName);                                                                                            \
	}

#define END_TRY_SQL_LOG_ONLY(conn)                                                                                                         \
	break;                                                                                                                                 \
	}                                                                                                                                      \
	catch (const mysqlpp::BadQuery& er) {                                                                                                  \
		int sqlErr = conn->errnum();                                                                                                       \
		if (sqlErr != CR_SERVER_LOST) {                                                                                                    \
			std::string s = formatBadQuery(sqlTaskName, er.what(), er.query_string(), conn->server(), conn->port(), conn->db(), conn->user()); \
			LogWarn(s);                                                                                                                    \
			break;                                                                                                                         \
		} else {                                                                                                                           \
			LogWarn("Lost connection in " << sqlTaskName << " retrying since got " << sqlErr << " " << er.what());                         \
		}                                                                                                                                  \
	}                                                                                                                                      \
	catch (const mysqlpp::BadConversion& er) {                                                                                             \
		LogWarn(loadStrPrintf(IDS_DB_CONVERSION_ERROR, sqlTaskName, er.what(), er.retrieved, er.actual_size).c_str());                     \
		break;                                                                                                                             \
	}                                                                                                                                      \
	catch (const mysqlpp::Exception& er) {                                                                                                 \
		LogWarn(loadStrPrintf(IDS_DB_ERROR, sqlTaskName, er.what()).c_str());                                                              \
	}                                                                                                                                      \
	if (__sqlTries__ >= RETRY_LIMIT) {                                                                                                     \
		LogWarn("Connection retry limit hit for " << sqlTaskName);                                                                         \
	}

// added retry logic for timeout or deadlock
#define BEGIN_TRANS_WITH_RETRY(conn)                           \
	std::string sqlTransactionName = __FUNCTION__;             \
	int __tranTries__ = 0;                                     \
	for (; __tranTries__ < RETRY_LIMIT; __tranTries__++) try { \
			Transaction trans(conn);

#define END_TRANS_WITH_RETRY()																	             \
	trans.commit();                                                                                          \
	break;                                                                                                   \
	}                                                                                                        \
	catch (const mysqlpp::BadQuery& er) {                                                                    \
		int sqlErr = er.errnum();                                                                            \
		if (sqlErr == ER_LOCK_DEADLOCK) {                                                                    \
			LogWarn("Deadlock in " << sqlTransactionName << " retrying since got " << sqlErr << " " << er.what());  \
		} else if (sqlErr == ER_LOCK_WAIT_TIMEOUT) {                                                         \
			LogWarn("Timeout in " << sqlTransactionName << " retrying since got " << sqlErr << " " << er.what());   \
		} else {                                                                                             \
			throw er;                                                                                        \
		}                                                                                                    \
	}                                                                                                        \
	if (__tranTries__ >= RETRY_LIMIT) {                                                                      \
		LogWarn("Transaction retry limit hit for " << sqlTransactionName);                                   \
	}

#define IF_DBNULL(x, y) ((x).is_null() ? (y) : (x))

#pragma endregion

/// autolock/unlock a CS so exception doesn't hold lock
class CSLock {
public:
	CSLock(CRITICAL_SECTION* cs) :
			m_cs(cs), m_locked(false) {
		lock();
	}

	void lock() {
		if (m_locked)
			return;
		EnterCriticalSection(m_cs);
		m_locked = true;
	}

	void unlock() {
		if (!m_locked)
			return;
		LeaveCriticalSection(m_cs);
		m_locked = false;
	}

	~CSLock() {
		if (m_locked)
			unlock();
	}

private:
	CRITICAL_SECTION* m_cs;
	bool m_locked;
};

} // namespace KEP