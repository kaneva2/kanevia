///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/keputil/KGenericObjList.h"
#include "IGameState.h"
#include "MySqlBase.h"
#include "KEPNetwork.h"
#include "Event\Base\IEventHandler.h"
#include "..\Event\ServerEvents.h"
#include "..\KEPServer\KEPServerEnums.h"
#include <map>

class jsThread;
struct inventories;

namespace mysqlpp {
class StoreQueryResult;
class Row;
class Query;
} // namespace mysqlpp

namespace KEP {

class IDispatcher;
class CMultiplayerObj;
class UrlParser;
class BackgroundUpdateThd;
class ItemWrapper;
class MediaParams;
class ServerEngine;
class ServerInfo;

class MySqlGameState : public IGameState, public MySqlBase {
public:
	MySqlGameState(IDispatcher &disp, ServerEngine *eng, CMultiplayerObj *cmp, const char *logName, const char *timingLogName, const DBConfig &config);
	~MySqlGameState();

	//1 IGameState Overrides
	virtual void Initialize(const char *gameDir);

	// initialize any event/eventhandlers
	virtual void InitEvents();

	// called when the server engine does save all
	virtual void SaveAll();

	virtual void Logoff(CAccountObject *acct, CPlayerObject *player, ULONG reason, IEvent *replyEvent);

	// called when the player is selected by the user after logging in
	// the inbound object will have just the name, on return, the
	// player will have everything
	virtual void GetPlayer(CPlayerObject *player);

	// called when the user creates a new player object
	virtual void CreatePlayer(CAccountObject *acct, CPlayerObject *player);

	virtual void DeletePlayer(ULONG accountId, CPlayerObject *player);

	bool IsAvatarTemplateApplied(CPlayerObject *player);
	bool ApplyAvatarSelection(int avatarIndex, CPlayerObject *player);

	// called to update the player in the database
	virtual void UpdatePlayer(CPlayerObject *player, ULONG changeMask = PlayerMask::USE_DIRTY_FLAGS);
	virtual bool ReloadPlayer(CPlayerObject *player, ULONG changeMask);

	// called to update both players' inventories, etc. for trade
	virtual void UpdateTradingPlayers(CPlayerObject *player1, CPlayerObject *player2, ULONG changeMask = PlayerMask::INVENTORY | PlayerMask::PLAYER);

	void RefreshInventory(CPlayerObject *player, bool bank = false);

	// _SCR_
	virtual void updateInventory3DApp(int playerId, CInventoryObjList *inventory, ULONG changeMask);

	virtual long GetPlayersCash(CPlayerObject *player, CurrencyType currencyType = CurrencyType::CREDITS_ON_PLAYER);
	void RefreshPlayersCashes(CPlayerObject *player);
	virtual EUpdateRet UpdatePlayersCash(CPlayerObject *player1, long player1AmtChanging, TransactionType transactionType, CPlayerObject *player2 = 0, long player2AmtChanging = 0, CurrencyType currencyType = CurrencyType::CREDITS_ON_PLAYER, int glid = 0, int qty = 0);
	virtual EUpdateRet PayCoverCharge(CAccountObject *acct, CPlayerObject *player);

	virtual void ExpandGroup(int groupId, std::vector<std::pair<int, std::string>> &recipientList);

	virtual PassList *GetZonePassList(const ZoneIndex &zi);

	virtual bool IsUserBlocked(CAccountObject *acctWantingAccess, LONG destKanevaId, LONG instanceId, BlockType blockType, const char *blockingUserName);
#if DEPRECATED
	virtual bool DoesPlacementSupportAnimation(long placementId, long animGlid);
#endif
	virtual bool DoesItemSupportAnimation(long itemGLID, long animGLID);

	virtual bool CreateClan(CPlayerObject *player, const char *clanName, ULONG &clanId) { return MySqlBase::createClan(player, clanName, clanId); }
	virtual bool DeleteClan(int ownerId) { return MySqlBase::deleteClan(ownerId); }
	virtual bool AddMemberToClan(ULONG clanId, ULONG playerId) { return MySqlBase::addMemberToClan(clanId, playerId); }
	virtual bool RemoveMemberFromClan(ULONG clanId, ULONG playerId) { return MySqlBase::removeMemberFromClan(clanId, playerId); }

	virtual bool SetPlayerBanStatus(const char *playerName, const char *reason, CPlayerObject *banner, bool banned);
	virtual long SchemaVersion() const { return m_schemaVersion; }
	bool CheckForNewP2pAnimation(int p2pIndex);

	/// convert a server id into server:port
	/// returns true if found, false if no server found
	virtual bool MapServerId(LONG serverId, std::string &server, ULONG &port);

	void LoadScriptServerItemAvailability();

	virtual std::string GetDefaultPlaceUrl() const;
	virtual void SetDefaultPlaceUrl(const std::string &url);
	virtual ZoneIndex GetInitialArenaZoneIndex() const;
	virtual void SetInitialArenaZoneIndex(ZoneIndex zi);

	virtual void SavePlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex, float x, float y, float z, float rx, float ry, float rz);
	virtual void DeletePlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex);
	virtual bool GetPlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex, float &x, float &y, float &z, float &rx, float &ry, float &rz);

	virtual void UpdateGameInventorySync(int kanevaUserId, int gameId, ULONG globalId, int inventoryCount) override;
	virtual ULONG AddGameItemPlacement(const ZoneIndex &zoneIndex, ULONG doGlid, const Vector3f &pos, const Vector3f &ori, int ownerId, int gameItemId) override;
	virtual void ClearZoneDOMemCache(ZoneIndex const &zi) override;
	virtual bool GetParentZoneInfo(KGP_Connection *conn, unsigned zoneInstanceId, unsigned zoneType, unsigned &parentZoneInstanceId, unsigned &parentZoneIndex, unsigned &parentZoneType, unsigned &parentCommunityId);
	virtual TimeMs GetEffectDurationMs(KGP_Connection *conn, GLID globalId) override;
	virtual void AddEffectDurationMs(const std::map<GLID, TimeMs> &effectDurationMap) override;
	virtual bool GetYouTubeSWFUrls(std::string &lastKnownGoodUrl, std::string &overrideUrl) override { return MySqlBase::getYouTubeSWFUrls(lastKnownGoodUrl, overrideUrl); }

private:
	static EVENT_PROC_RC PlayerSpawnedEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC TextEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC ShutdownEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC KgpUrlEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC ObjectTicklerEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC GetItemBackgroundHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC UgcZoneLiteHandler(ULONG lparam, IDispatcher *disp, IEvent *e);

	void initializeState(const char *gameDir);

	void updatePlayer(CPlayerObject *player, ULONG changeMask, bool loggingOut);
	bool updatePlayersCashNoTrans(CPlayerObject *player1, long player1AmtChanging, long &curAmt, TransactionType transactionType, CurrencyType currencyType, int glid, int qty); // these two only if buying or selling )
	bool updatePlayersCashNoTrans(LONG serverId, long player1AmtChanging, long &curAmt, TransactionType transactionType, CurrencyType currencyType, int glid = 0, int qty = 0); // these two only if buying or selling
	void loadPassList(CPlayerObject *player);

	void getInventory(CPlayerObject *player);
	void getGetSetMetaData();
	void loadGetSetMetaDataValues();
	void saveGetSetMetaDataValues();

	bool serverStateChange(EServerState newState);

	void acceptGift(NETID netId, UrlParser *url);
	void giftItems(NETID netId, UrlParser *url);

	virtual bool getItemData(int glid, CItemObj *item) { return MySqlBase::getItemData(glid, item); }
	virtual CGlobalInventoryObj *addCustomItem(int globalId) { return MySqlBase::addCustomItem(globalId); }

	/**
	 * Given a mysqlpp::Row reference, populate a CItemObj reference.  Note that the tuple is acquired from the 
	 * the result set at index 15, in the multi result set returned by the mysql stored procedure get_player_data()
	 * (schema rev100).
	 *
	 * @param row   mysqlpp::Row reference from which to populate.
	 * @param item  The CItemObj reference to be populated
	 */
	void popItemFromTuple(mysqlpp::Row &row, CItemObj &item);
	void updatePassList(NETID netId, UrlParser *url);
	void updateItemPassList(CItemObj *item);
	void updateItemInPlayerInventory(CPlayerObject *player, CInventoryObjList *dest, CItemObj *obj, int updateType, bool checkArmed);
	void updateItemUrl(NETID netId, UrlParser *url);
	void updateMediaPlayer(const ZoneIndex &zoneIndex, NETID netId, const CPlayerObject *player, int objPlacementId, const MediaParams &mediaParams,
		bool usePlaylistId, int assetId = 0, int assetType = 0, const char *itemUrl = NULL, const char *mature = NULL, int stopPlay = 0);
	void updateMediaPlayerVolumeAndRadius(const ZoneIndex &zoneIndex, NETID netId, const CPlayerObject *player, int objPlacementId, const MediaParams &mediaParams);

	void logoff(const char *name, int accountNumber, ULONG kanevaUserId, const char *currentIP, int minPlayed, CPlayerObject *player, ULONG reason);

	void loadClanDatabase();
	void loadItemDatabase();
	void loadPassLists();

	bool applyAvatarTemplateStartingInventory(int avatarIndex, CPlayerObject *player);
	void getAvatarTemplateStartingInfo(int avatarIndex, CPlayerObject *player);
	void getAvatarTemplateDeformableConfigs(int avatarIndex, CPlayerObject *player);
	void recordAvatarTemplateApplication(int avatarIndex, CPlayerObject *player);

	bool loadServerIds();
	bool loadSupportedWorlds();
	bool loadNetworkDefinitionsFromDB();
	bool saveNetworkDefinitionsToDB(const std::string &name, const std::string &value);

	// methods to refresh items from database, called from event handler
	bool refreshItemPassList(ObjectTicklerEvent::ChangeType ct, LONG glid);
	bool refreshItem(ObjectTicklerEvent::ChangeType ct, LONG glid);

	//P2P Animations functionality
	void loadAnimations();

	void syncItemsFromWeb();

	// background thd to update db
	TimeMs m_backupSleepTime; // ms to sleep between passed for user checks
	ULONG m_maxPlayersInBackupLoop; // number of players to backup when lock held
	TimeMs m_maxBackupTimeMs; // max time to spend in backup loop
	bool m_localPing; // should we ping in by calling an SP, too?
	bool m_localPingInited;
	TimeMs m_pingInterval; // how often background ping runs (default to 3 min)
	TimeMs m_backgroundInterval; // how often background task runs (default to 3 min)

	IDispatcher &m_dispatcher;
	jsThread *m_updateThd;
	std::string m_serverName; // cache this so don't have to look it up each time
	int m_serverPort;
	std::string m_kpointName; // in case we want to change it, name for kpoints/credits
	std::string m_giftcreditName; // in case we want to change it, name for gift credits
	bool m_loadClanDatabase; // This flag is intended for use by development to preclude
		// loading the clan database which can be quite time consuming.
		// By default, this flag is enabled.  Controlled by the
		// LoadClanDatabase item in db.cfg
	std::string m_playlistSwf;
	std::string m_playlistParams;

	typedef std::map<long, std::pair<PassList *, TimeMs>> PassListMap; // zoneIndex.toLong->(PassList,timeretrieved)
	PassListMap m_passLists;

	ServerInfo *m_serverInfo; // array of serverId, ip, port for lookup
	TimeMs m_lastServerInfoUpdate;
	LONG m_schemaVersion; // from db table

	std::string m_defaultPlaceUrl;
	ZoneIndex m_initialArenaZoneIndex;

	struct ParentZoneInfo {
		unsigned parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId;
	};
	std::map<unsigned, ParentZoneInfo> m_parentZoneInfoMap; // zone instance ID -> parent zone instance ID
	std::mutex m_parentZoneInfoMutex;

	std::map<GLID, TimeMs> m_effectDurationMs; // effect item GLID -> effect duration in ms
	std::mutex m_effectDurationMutex;

	friend class BackgroundUpdateThd;
};

} // namespace KEP
