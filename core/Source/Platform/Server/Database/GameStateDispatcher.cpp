///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common/include/kepcommon.h"
#define __GAMESTATEDISPATCHER_H_INTERNAL__
#include "server/serverengine/serverengine.h"
#include "Core/Util/Monitored.h"
#include "common/keputil/Mailer.h"
#include "GameStateDispatcher.h"
#include "MySqlEventProcessor.h"

#include "Event/EventSystem/Dispatcher.h"

#include "IGameState.h" // for PLAYER_FOUND, etc.

#include <jsThread.h>
#include <log4cplus/logger.h>
using namespace log4cplus;

//#define TESTING
#ifdef TESTING
#define _CRT_RAND_S
#include <stdlib.h>
void runtest(MTEventQueue &q);
#define RUNTEST(x) runtest(x);
#else
#define RUNTEST(x)
#endif

#include "MTEventQueue.h"

namespace KEP {

///////////////////////////////////////////////////////////
/// class for doing the work in the background for database calls
class GameStateWorkerThread : public jsThread, public Monitored {
public:
	GameStateWorkerThread(ServerEngine *serverEngine, MTEventQueue &q, IDispatcher &reply, const char *baseDir, const DBConfig &dbConfig) :
			jsThread("GSWorker"),
			m_replyDispatcher(reply),
			m_baseDir(baseDir),
			m_logger(Logger::getInstance(L"GameStateWorkerThread")),
			m_queue(q),
			m_totalTime(Timer::State::Paused),
			m_taskTime(Timer::State::Paused),
			m_gameStateDB(0),
			m_currEvent(0),
			m_currFilter(0) {
		try {
			m_gameStateDB = new MySqlEventProcessor(serverEngine, m_dispatcher, m_replyDispatcher, baseDir, dbConfig);
		} catch (KEPException *e) {
			NDCContextCreator ndc(Utf8ToUtf16(m_queue.instanceName()));
			LOG4CPLUS_ERROR(m_logger, "MySqlEventProcessor constructor failed, probably won't be able to connect to db " << e->ToString());
			e->Delete();
		}
	}

	void SetServerNetwork(IServerNetwork *s) {
		m_gameStateDB->SetServerNetwork(s);
	}
	Dispatcher &MyDispatcher() { return m_dispatcher; }

	const MySqlEventProcessor *MyGameStateDB() { return m_gameStateDB; }

	~GameStateWorkerThread() {
		dbCleanUp();
	}

	int backgroundThreadCount() {
		jsVerifyReturn(m_gameStateDB != 0, 1);
		return m_gameStateDB->backgroundThreadCount();
	}

	Monitored::DataPtr monitor() const {
		LOG4CPLUS_TRACE(Logger::getInstance(L"Monitor"), "GameStateDispatcher::GameStateWorkerThread::monitor()");
		DataPtr ret(new MonData(*this));
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Monitor"), "GameStateDispatcher::GameStateWorkerThread::Monitor(): call complete!");
		return ret;
	}

protected:
	bool dbInit(const char *baseDir) {
		try {
			m_gameStateDB->Initialize();

			// register handlers
			m_gameStateDB->InitEvents();
		} catch (KEPException *e) {
			LOG4CPLUS_ERROR(m_logger, "DB Init Error, background thread will NOT run: " << e->ToString());
			MessageBeep(0xffffffff);
			e->Delete();
			return false;
		}
		return true;
	}

	bool dbCleanUp() {
		DELETE_AND_ZERO(m_gameStateDB);
		return true;
	}

	///////////////////////////////////////////////////////////
	/// thread override worker fn
	ULONG _doWork() {
		CoInitialize(0);
		NDCContextCreator ndc(Utf8ToUtf16(m_queue.instanceName()));

		if (!dbInit(m_baseDir.c_str())) {
			dbCleanUp();
			return 2;
		}

		LOG4CPLUS_INFO(m_logger, "GameStateWorkerThread started");
		m_totalTime.Start();
		while (!isTerminated()) {
			IBackgroundDBEvent *e = m_queue.WaitForNextEvent();
			// TODO: See what happens here when an exception is thrown or npe is raised.
			// Seems to me that this should be performing some kind of exception trapping
			// to protect itself from premature exit.
			//
			if (isTerminated() || !e)
				continue;

			LOG4CPLUS_TRACE(m_logger, "Got event of id " << e->EventId() << " filter " << e->Filter());
			m_currEvent = e->EventId();
			m_currFilter = e->Filter();
			m_taskTime.Start();
			m_dispatcher.ProcessSynchronousEventNoRelease(e);
			m_taskTime.Pause();
			m_currEvent = 0;
			m_currFilter = 0;

			m_queue.DecQueuedOnEvent(e); // dec for the inc when went into our queue, delete if ref count hits zero
		}
		m_totalTime.Pause();

		dbCleanUp();
		char msg[1024];
		sprintf_s(msg, _countof(msg), "GameStateWorkerThread exiting cleanly. Processed %8d events in %8.2f secs", m_totalTime.Count(), (double)(m_totalTime.ElapsedMs() / 1000));
		LOG4CPLUS_INFO(m_logger, msg);

		getNDC().remove();

		CoUninitialize();
		return 0;
	}

private:
	IDispatcher &m_replyDispatcher; //< main thd's dispatcher for sending replies
	Dispatcher m_dispatcher; //< our dispatcher for processing events sent to us
	MySqlEventProcessor *m_gameStateDB; //< our instance of the database object
	std::string m_baseDir;
	Logger m_logger;

	// todo: break this and replace with something from the Kaneva Time family of classes.
	//
	Timer m_totalTime; // total ms spent processing
	Timer m_taskTime; // ms spent  on last or current task
	std::string m_taskSynopsis; // Some text about what is being executed.
	unsigned long m_currEvent;
	unsigned long m_currFilter;

	class MonData : public Monitored::Data {
	public:
		TimeMs _totalRuntimeMS;
		TimeMs _taskRuntimeMS;
		unsigned long _totalCount;
		bool _idle;
		std::string _synopsis;
		unsigned long _currEvent;
		unsigned long _currFilter;

		MonData(const GameStateWorkerThread &subject) :
				_totalRuntimeMS(subject.m_totalTime.ElapsedMs()), _totalCount(subject.m_taskTime.Count()), _taskRuntimeMS(subject.m_taskTime.ElapsedMs()), _idle(subject.m_taskTime.IsPaused()), _synopsis(subject.m_taskSynopsis), _currEvent(subject.m_currEvent), _currFilter(subject.m_currFilter) {}

		MonData(const MonData &toCopy) :
				// _queueSize( toCopy._queueSize )
				_idle(toCopy._idle),
				_taskRuntimeMS(toCopy._taskRuntimeMS),
				_totalRuntimeMS(toCopy._totalRuntimeMS),
				_totalCount(toCopy._totalCount),
				_synopsis(toCopy._synopsis) {}

		std::string toString() const {
			std::stringstream ss;
			ss << "<GameStateWorkerThread " //<< "queuesize="			<< _queueSize						<< ";"       // belongs in the dispatcher class
			   << "totalruntimems=\"" << (unsigned long)_totalRuntimeMS << "\" "
			   << "executioncount=\"" << (unsigned long)_totalCount << "\" "
			   << "taskruntimems=\"" << (unsigned long)_taskRuntimeMS << "\" "
			   << "state=\"" << (_idle ? "idle" : "active") << "\" "
			   << "synopsis=\"" << _synopsis << "\" "
			   << "curreventid=\"" << _currEvent << "\" "
			   << "currfilter=\"" << _currFilter << "\"/>";
			return ss.str();
		}

		unsigned long totalruntime_ms;
		unsigned long currruntime_ms;
		std::string synopsis;
	};

	MTEventQueue &m_queue;
};

/////////////////////////////////////////////////////////////////////////
/// constructor
GameStateDispatcher::GameStateDispatcher(ServerEngine *serverEngine, IDispatcher &replyDispatcher, const char *instanceName, const DBConfig &dbConfig) :
		m_server(serverEngine), m_dbConfig(dbConfig), m_peakQueueDepth(0), m_maxThreads(1), m_logger(Logger::getInstance(L"GameStateDispatcher")), m_replyDispatcher(replyDispatcher)
#ifdef FIX_MTQUEUE
		,
		m_queue(new MTEventQueue(instanceName, serverEngine->m_optimizeForSingleThread))
#else
		,
		m_queue(new MTEventQueue(instanceName))
#endif
		,
		m_instanceName(instanceName),
		m_reportQCWarnAlarm(serverEngine->getBackgroundQueueMonitorConfig().warnThrottleMS),
		m_reportQCInfoAlarm(serverEngine->getBackgroundQueueMonitorConfig().infoThrottleMS) {
}

bool GameStateDispatcher::Init(const char *baseDir) {
	if (m_maxThreads < 1)
		LOG4CPLUS_ERROR(m_logger, "Max Threads Zero, no background tasks will run");

	RegisterDBEvents(m_replyDispatcher);
	EVENT_ID id = m_replyDispatcher.GetEventId("GotoPlaceBackgroundEvent");

	int i = 0;
	m_maxThreads = 1;
	do {
		GameStateWorkerThread *gswt = new GameStateWorkerThread(m_server, *m_queue, m_replyDispatcher, baseDir, m_dbConfig);
		if (gswt && gswt->MyGameStateDB()) {
			m_workers.push_back(gswt);
		} else {
			jsAssert(false);
			LOG4CPLUS_ERROR(m_logger, "GameStateDB initialization failed!  Can the server reach the db config?");
			return false;
		}

		RegisterDBEvents(m_workers.back()->MyDispatcher());
		if (id != m_workers.back()->MyDispatcher().GetEventId("GotoPlaceBackgroundEvent")) {
			jsAssert(false);
			LOG4CPLUS_ERROR(m_logger, "Event registration mismatch between main and background threads.");
			return false;
		}
		m_workers.back()->start();
		m_maxThreads = m_workers.back()->backgroundThreadCount();
	} while (++i < m_maxThreads);

	LOG4CPLUS_INFO(m_logger, "Created " << m_maxThreads << " backgroundThreads");
	m_reportQCWarnAlarm.timer().Start();
	m_reportQCInfoAlarm.timer().Start();

	RUNTEST(*m_queue);

	return true;
}

void GameStateDispatcher::SetServerNetwork(IServerNetwork *s) {
	for (GSWorkers::iterator i = m_workers.begin(); i != m_workers.end(); i++)
		(*i)->SetServerNetwork(s);
}

/////////////////////////////////////////////////////////////////////////
/// register all the DB events for the dispatcher
void GameStateDispatcher::RegisterDBEvents(IDispatcher &disp) {
	RegisterEvent<GotoPlaceBackgroundEvent>(disp);
	RegisterEvent<ChangeZoneBackgroundEvent>(disp);
	RegisterEvent<ZoneCustomizationBackgroundEvent>(disp);
	RegisterEvent<GetResultSetsEvent>(disp);
	RegisterEvent<CanSpawnToZoneBackgroundEvent>(disp);
	RegisterEvent<LogoffUserBackgroundEvent>(disp);
	RegisterEvent<BroadcastEventBackgroundEvent>(disp);
	RegisterEvent<PlaylistBackgroundEvent>(disp);
	RegisterEvent<YouTubeSWFUrlBackgroundEvent>(disp);
	RegisterEvent<RefreshPlayerBackgroundEvent>(disp);
	RegisterEvent<SpaceImportBackgroundEvent>(disp);
	RegisterEvent<ChannelImportBackgroundEvent>(disp);
	RegisterEvent<ScriptAvailableItemsBackgroundEvent>(disp);
	RegisterEvent<LoadItemsBackgroundEvent>(disp);
	RegisterEvent<UpdateScriptZoneBackgroundEvent>(disp);
}

/////////////////////////////////////////////////////////////////////////
/// queue an event to the background
void GameStateDispatcher::QueueEvent(IBackgroundDBEvent *e) {
	jsVerifyReturnVoid(e != nullptr);

	ULONG qCnt = m_queue->QueueEvent(e);
	if (qCnt > m_peakQueueDepth)
		m_peakQueueDepth = qCnt;

	NDCContextCreator ndc(Utf8ToUtf16(m_instanceName));
	if (m_server->getBackgroundQueueMonitorConfig().infoThrottleMS && m_reportQCInfoAlarm.elapsed()) {
		m_reportQCInfoAlarm.timer().Reset();
		LOG4CPLUS_WARN(m_logger, "QueueCount: " << qCnt);

		m_server->threadPool().enqueue(LightWeightMailer::createRunnable(m_server->getBackgroundQueueMonitorConfig().emailHost, m_server->getBackgroundQueueMonitorConfig().infoEMailFrom, m_server->getBackgroundQueueMonitorConfig().infoEMailTo, "Background Queue Status", monitor()->toString()));
		return;
	}

	// If the queue depth is within configured norms, we're done.
	if (qCnt <= m_server->getBackgroundQueueMonitorConfig().warnThreshold)
		return;

	Monitored::DataPtr monitorData = monitor();
	std::string monitorString = monitorData->toString();

	// Log only when threshold is crossed.
	//
	if (qCnt == (m_server->getBackgroundQueueMonitorConfig().warnThreshold + 1))
		LOG4CPLUS_WARN(m_logger, "GameStateDispatcher::QueueEvent():BackgroundQueueAlert:("
									 << m_server->getBackgroundQueueMonitorConfig().warnThreshold
									 << ": " << qCnt << ")" << monitorString);

	// queue depth threshold is outside of configured norms.
	// If the alert is throttled, we're done.
	if (!m_reportQCWarnAlarm.elapsed())
		return;
	m_server->threadPool().enqueue(LightWeightMailer::createRunnable(m_server->getBackgroundQueueMonitorConfig().emailHost, m_server->getBackgroundQueueMonitorConfig().warnEMailFrom, m_server->getBackgroundQueueMonitorConfig().warnEMailTo, "Background Queue Alert", monitorString));

	m_reportQCWarnAlarm.timer().Reset();
}

/////////////////////////////////////////////////////////////////////////
/// stop all thread, preparing for exit
void GameStateDispatcher::Shutdown() {
	log4cplus::Logger logger = log4cplus::Logger::getInstance(L"SHUTDOWN");
	LOG4CPLUS_TRACE(logger, "GameStateDispatcher::Shutdown()");
	if (m_workers.empty()) {
		LOG4CPLUS_DEBUG(logger, "GameStateDispatcher::Shutdown(): No workers");
		return;
	}

	for (const auto &pGSWT : m_workers) {
		LOG4CPLUS_DEBUG(logger, "GameStateDispatcher::Shutdown() signal stopthread [" << pGSWT->name() << "]");
		pGSWT->stop(0, false); // sets terminated w/o killing them
	}

	m_queue->Shutdown(); // wakes them all up if waiting on sema

	Timer timerShutdown;
	for (int j = 0; j < 10; j++) {
		for (GSWorkers::iterator i = m_workers.begin(); i != m_workers.end();) {
			(*i)->join(500);
			if (!(*i)->isRunning()) {
				delete *i;
				i = m_workers.erase(i);
			} else {
				i++;
			}
		}
	}
	char msg[1024];
	sprintf_s(msg, _countof(msg), "Was running %8.2f", m_timerStart.ElapsedSec());
	LOG4CPLUS_INFO(m_logger, msg << "secs and shutdown took " << timerShutdown.ElapsedMs() << "ms and left " << m_workers.size() << " threads running ");
	m_workers.clear();
	LOG4CPLUS_TRACE(logger, "GameStateDispatcher::Shutdown() Complete");
}

class GSDMonitorData : public Monitored::Data {
public:
	GSDMonitorData(const GameStateDispatcher &gsd) :
			_gsd(gsd) {}

	std::string toString() const {
		std::stringstream ss;
		ss << "<GameStateDispatcher name=\"" << _gsd.m_instanceName << "\" "
		   << "ServerId=\"" << _gsd.m_server->GetServerId() << "\" "
		   << "QueueDepth=\"" << _gsd.m_queue->size() << "\" "
		   << "PeakQueueDepth=\"" << _gsd.peakQueueDepth() << "\">";

		for (GameStateDispatcher::GSWorkers::const_iterator i = _gsd.m_workers.begin(); i != _gsd.m_workers.end(); i++) {
			const GameStateWorkerThread *gswt = (*i);
			ss << gswt->monitor()->toString();
		}
		ss << "</GameStateDispatcher>";
		return ss.str();
	}

	const GameStateDispatcher &_gsd;
};

Monitored::DataPtr GameStateDispatcher::monitor() const {
	return Monitored::DataPtr(new GSDMonitorData(*this));
}

/////////////////////////////////////////////////////////////////////////
GameStateDispatcher::~GameStateDispatcher() {
	log4cplus::Logger logger(Logger::getInstance(L"SHUTDOWN"));
	LOG4CPLUS_TRACE(logger, "GameStateDispatcher::~GameStateDispatcher()");
	Shutdown();
	delete m_queue;
}

#ifdef TESTING

// test the MTEventQueue and processing by flooding it
void runtest(MTEventQueue &q) {
	int loopSize = 50;
	int maxUser = 5;
	ULONG loopSleep = 5;
	ULONG exitSleep = 10000;

	for (int i = 0; i < loopSize; i++) {
		UINT r = rand();

		ZoneCustomizationBackgroundEvent *zce = new ZoneCustomizationBackgroundEvent();
		zce->kanevaUserId = (r % maxUser) + 100;
		q.QueueEvent(zce);
		Sleep(loopSleep);
	}
	Sleep(exitSleep); // let threads finish
}

#endif

} // namespace KEP