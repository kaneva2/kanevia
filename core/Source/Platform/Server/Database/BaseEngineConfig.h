///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPMacros.h"
#include "EngineTypes.h"
#include <vector>
#include <Log4CPlus/Logger.h>
#include "Common\KEPUtil\traversable.h"
#include "Common\KEPUtil\Algorithms.h"

using namespace log4cplus;

namespace KEP {
class BaseEngineConfigLoader;
class IKEPConfig;
class DBConfig;
class KGP_Connection;

class BaseEngineConfigContainer : public Traversable<IKEPConfig*> {
	typedef std::vector<IKEPConfig*> ConfigVector;

public:
	BaseEngineConfigContainer(const std::vector<StringPairT>& paramSet);
	~BaseEngineConfigContainer();

	void SetConfigValue(bool commonConfig,
		eEngineType engineType,
		LONG engineInstanceId,
		const std::string& valueName,
		LONG valueType,
		const int* pIntValue,
		const double* pDoubleValue,
		const char* stringValue);

	void AddConfig(IKEPConfig* newConfig, const char* configPath);

	void ClearAllDirtyFlags();

	void Dump();

	virtual Traversable<IKEPConfig*>& traverse(Traverser<IKEPConfig*>& traversed);

private:
	void SetConfigGetSetValue(ConfigVector::value_type config,
		const std::string& valueName,
		LONG valueType,
		const int* pIntValue,
		const double* pDoubleValue,
		const char* pStringValue);

	Logger m_logger;
	ConfigVector m_configs;
	std::vector<StringPairT> m_paramSet;
};

class BaseEngineConfigLoader {
public:
	BaseEngineConfigLoader(const std::string& pathXml, const DBConfig& dbConfig);
	~BaseEngineConfigLoader();
#ifdef ENABLEBLADEDBCFG
	bool LoadAllDbConfigs(int serverInstanceId, BaseEngineConfigContainer& configs);
#endif

	// This is the path to the configuration files, specifically KGPServer.xml and db.cfg.
	// It is also set as the baseDir for all loaded engine blades.
	std::string m_pathXml;
	std::string PathXml() { return m_pathXml; }
	void SetPathXml(const std::string& path) { m_pathXml = path; }

private:
	void initialize();
	void initializeDB(const std::string& pathXml);
	void getAllGetSetMetaData(char* serverName, int serverInstanceId, BaseEngineConfigContainer& configs);

	bool m_initialized;
	KGP_Connection* m_conn;
	std::string m_dbName;
	Logger m_logger;
	const DBConfig& m_dbConfig;
};

} // namespace KEP
