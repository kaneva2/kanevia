///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "MySqlUtil.h"
#include "Plugins/App/PlayerModel.h"
#include "MySqlBase.h"
#include "DBConfigValues.h"
#include "DBStrings.h"
#include "LocaleStrings.h"
#include "KEPException.h"
#include "KEPConfig.h"
#include "audit.h"
#include "alert.h"
#include <vector>
#include <mysqld_error.h>
#include <InstanceId.h>
#include <CPlayerClass.h>
#include <CNoterietyClass.h>
#include <limits>
#include <PassList.h>
#include "UseTypes.h"
#include "CAccountClass.h"
#include "CCurrencyClass.h"
#include "CClanSystemClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CMultiplayerClass.h"
#include "CQuestJournal.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "CTitleObject.h"
#include "Common/include/LogTimings.h"
#include "param_ids.h"
#include "../ServerEngine/ServerEngine.h"
#include "PlayerMask.h"
#include "Event/Events/RefreshPlayerBackgroundEvent.h"
#include "..\..\Tools\HttpClient\GetBrowserPage.h"

//#include <log4cplus/logger.h>
#include "LogHelper.h"
static LogInstance("Instance");

#define RET_COL 0
#define USER_ID_COL 1
#define KANEVAUSER_ID_COL 2
#define IS_GM_COL 3
#define CAN_SPAWN_COL 4
#define IS_ADMIN_COL 5
#define SERVER_ID_COL 6
#define LOGGED_ON_COL 7
#define TIME_PLAYED_COL 8
#define AGE_COL 9
#define SHOW_MATURE_COL 10

#define USER_EXISTS 0L
#define USER_NEW 1L
#define USER_LOGGED_ON 2L
#define USER_MULTIPLE 3L

#define PLAYER_ID_COL 0
#define NAME_COL 1
#define EDB_COL 2

#define PLAYER_RESULTS 17
#define PLAYERS_RS 0
#define PLAYERS_PRESENCES_RS 1
#define ZONES_RS 2
#define INVENTORIES_RS 3
#define DEFCFG_RS 4
#define CASH_RS 5
#define GIFT_RS 6
#define BANK_RS 7
#define SKILL_RS 8
#define STAT_RS 9
#define GETSET_RS 10
#define NOTERIETY_RS 11
#define CLAN_RS 12
#define PASSLIST_RS 13
#define NAME_COLOR 14
#define ITEMS_RS 15
#define PASSES_RS 16

#define DM_EQUIP_ID 0
#define DM_DEFCFG 1
#define DM_MAT 2
#define DM_COLOR_R 3
#define DM_COLOR_G 4
#define DM_COLOR_B 5

using namespace mysqlpp;

namespace KEP {

sql_create_3(player_stats,
	1, 3,
	int, player_id,
	int, stat_id,
	float, current_value);

sql_create_8(deformable_configs,
	1, 8,
	int, sequence,
	int, player_id,
	int, equip_id,
	int, deformable_config,
	int, custom_material,
	float, custom_color_r,
	float, custom_color_g,
	float, custom_color_b);

sql_create_3(noterieties,
	1, 3,
	int, player_id,
	int, murder_count,
	int, minutes_since_last_murder);

sql_create_7(inventories,
	4, 7,
	int, player_id,
	std::string, inventory_type, //  type of inventory this is (Player/Bank/Housing...)
	int, global_id,
	int, inventory_sub_type,
	std::string, armed, // is item currently armed (T/F)
	int, quantity,
	int, charge_quantity);

sql_create_1(cash_get,
	1, 0,
	double, balance);

#define INS_GETSET_COLS "`player_id`, `name`, `type`, `int_value`, `double_value`, `string_value`"

class invTracker {
public:
	invTracker(CInventoryObj *o = 0, int q = 0, bool a = false) :
			obj(o), qty(q), armed(a) {}

	invTracker(const invTracker &src) { operator=(src); }
	invTracker &operator=(const invTracker &src) {
		if (this != &src) {
			obj = src.obj;
			qty = src.qty;
			armed = src.armed;
		}
		return *this;
	}
	CInventoryObj *obj;
	int qty;
	bool armed;
};

bool xmlFillInItem(Logger &logger, TiXmlDocument *doc, CItemObj **item, CItemObj **baseItem);
TiXmlElement *xmlParseResultHeader(Logger &logger, TiXmlDocument *doc, const char *wantedChild);

enum {
	INV_PLAYER_ID,
	INV_TYPE,
	INV_GLID,
	INV_SUB_TYPE,
	INV_ARMED,
	INV_QTY,
	INV_CHG_QTY
};

MySqlBase::MySqlBase(ServerEngine *serverEngine, const char *logName, const char *timingLogName, const DBConfig &dbConfig) :
		//m_logger( Logger::getInstance(Utf8ToUtf16(logName)) )
		//	m_timingLogger( Logger::getInstance(Utf8ToUtf16(timingLogName)))
		m_conn(new KGP_Connection(use_exceptions)),
		m_dbConfig(dbConfig),
		m_serverId(0),
		m_server(serverEngine) {
}

CMultiplayerObj *MySqlBase::multiplayerObj() const {
	return m_server->multiplayerObj();
}

// RENAMED TO addItemToDb1 from addItemToDb in MySqlGameState in order to avoid name collision.
// When all is said and done the version in MySqlGameState will likely be unnecessary.
void addItemToDb1(Query &query, Logger &logger, CItemObj *item, bool calledDuringSync = false) {
	if (!calledDuringSync) {
#ifdef SYNC_ALL_AT_START
		return; // if syncing at start, don't add here
#endif
	}

	// add item to local database
	query.reset();
	query << "CALL add_item( %0, %1q, %2q, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14, %15, %16 )";
	query.parse();
	query.execute(item->m_globalID,
		(const char *)item->m_itemName,
		(const char *)item->m_itemDescription,
		item->m_marketCost,
		item->m_sellingPrice,
		item->m_requiredSkillType,
		item->m_requiredSkillLevel,
		(int)item->m_useType,
		item->m_itemType,
		item->m_baseGlid,
		item->m_expiredDuration,
		item->m_permanent,
		item->m_defaultArmedItem,
		item->m_visualRef,
		item->m_derivable,
		item->m_stackable,
		calledDuringSync ? 0 : 1); // for now use creator id to determine if synced or added as one-off
	LOG4CPLUS_DEBUG(logger, "Synced glid " << item->m_globalID << " from Kaneva");
}

void addItemAnimationToDb(Query &query, Logger &logger, int itemGLID, int animGLID, bool calledDuringSync = false) {
	if (!calledDuringSync) {
#ifdef SYNC_ALL_AT_START
		return; // if syncing at start, don't add here
#endif
	}

	// add item to local database
	query.reset();
	query << "CALL add_item_animation( %0, %1 )";
	query.parse();
	query.execute(itemGLID, animGLID);
	LOG4CPLUS_DEBUG(logger, "Synced item_animation (" << itemGLID << " : " << animGLID << ") from Kaneva");
}

void addItemPassToDb(Query &query, Logger &logger, int itemGLID, int passID, bool calledDuringSync = false) {
	if (!calledDuringSync) {
#ifdef SYNC_ALL_AT_START
		return; // if syncing at start, don't add here
#endif
	}

	// add item to local database
	query.reset();
	query << "INSERT INTO pass_group_items (global_id, pass_group_id) VALUES ( %0, %1 )";
	query.parse();
	query.execute(itemGLID, passID);
	LOG4CPLUS_DEBUG(logger, "Synced pass_group_item (" << itemGLID << " : " << passID << ") from Kaneva");
}

void addItemParameterToDb(Query &query, Logger &logger, int itemGLID, int paramTypeId, int paramValue, bool calledDuringSync = false) {
	if (!calledDuringSync) {
#ifdef SYNC_ALL_AT_START
		return; // if syncing at start, don't add here
#endif
	}

	// add item parameter to local database
	query.reset();
	query << "INSERT INTO item_parameters (global_id, param_type_id, VALUE) VALUES ( %0, %1, %2 )";
	query.parse();
	query.execute(itemGLID, paramTypeId, paramValue);
	LOG4CPLUS_DEBUG(logger, "Synced item parameter (" << itemGLID << " : " << paramTypeId << " -> " << paramValue << ") from Kaneva");
}

void MySqlBase::addInventoryItem(const char *playerName, CInventoryObjList *invList, Row &r) {
	addInventoryItem(playerName, invList, atoi(r.at(INV_GLID)), atoi(r.at(INV_QTY)), r.at(INV_ARMED) == DB_TRUE, atoi(r.at(INV_SUB_TYPE)), atoi(r.at(INV_CHG_QTY)));
}

void MySqlBase::addInventoryItem(const char *playerName, CInventoryObjList *invList, int glid, int qty, bool armed, short invSubType, int chgQty) {
	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();
	if (globalDBRef == 0) {
		LogError("Can't get CGlobalInventoryObjList::GetInstance in addInventoryItem");
		return;
	}

	CGlobalInventoryObj *itemPtr = (CGlobalInventoryObj *)globalDBRef->GetByGLID(glid);
	if (itemPtr == 0) {
		LogWarn("Player: " << playerName << " losing object with glid: " << glid << " since not found in GIL");
		return;
	}

	// Note that this does not set the charge on the item...hence the use of
	// DISCRETE_ADD.
	//
	CInventoryObj *inv = addInventoryItem(playerName, invList, *itemPtr, qty, armed, invSubType, chgQty);

	// However, this method now returns the newly added item...
	// so the caller is free to set the charge.
	//
	inv->setCharge(chgQty);
}

CInventoryObj *MySqlBase::addInventoryItem(const char *playerName, CInventoryObjList *invList, CGlobalInventoryObj &invItem, int qty, bool armed, short invSubType, int chgQty) {
	CGlobalInventoryObj *itemPtr = &invItem;
	CInventoryObj *invPtr = 0;
	// if not stackable add one at a time
	if (itemPtr->m_itemData->m_stackable) {
		invPtr = invList->AddOneInventoryItem(qty,
			armed,
			itemPtr->m_itemData,
			invSubType);
	} else {
		for (LONG j = 0; j < qty; j++) {
			invPtr = invList->AddOneInventoryItem(1, // qty
				j == 0 ? armed : false, // only arm first one
				itemPtr->m_itemData,
				invSubType);
		}
	}
	return invPtr;
}

void MySqlBase::addCustomItems(const std::set<int> &globalIds) {
	std::map<int, CItemObj> items;
	if (refreshItemsData(globalIds, &items)) // get item info from db
	{
		CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();

		std::set<int> missingGLIDs; // glids that are missing from the DB
		std::set<int> missingBaseGLIDs; // base glids that have not been loaded from the DB yet
		std::multimap<int, CItemObj> itemsWithMissingBaseGLIDs; // mapping of missing base glids and corresponding glids

		for (auto position = globalIds.cbegin(); position != globalIds.cend(); position++) {
			int globalId = *position;

			std::map<int, CItemObj>::iterator it = items.find(globalId);
			if (it != items.end()) // item was found in the db
			{
				CGlobalInventoryObj *globalInvObj = NULL;
				CItemObj newItem = items[globalId];

				if (newItem.m_baseGlid != 0) // item has a base glid
				{
					CGlobalInventoryObj *baseGlobalInventoryObj = NULL;
					baseGlobalInventoryObj = globalDBRef->GetByGLID(newItem.m_baseGlid);

					if (baseGlobalInventoryObj == NULL) // no existing item matches base GLID
					{
						// add it to the missing base glid list and create both base and item later
						missingBaseGLIDs.insert(newItem.m_baseGlid);
						itemsWithMissingBaseGLIDs.insert(std::pair<int, CItemObj>(newItem.m_baseGlid, newItem));
					} else {
						globalInvObj = new CGlobalInventoryObj();
						baseGlobalInventoryObj->m_itemData->CloneFromBaseGLID(&globalInvObj->m_itemData, &newItem, globalId);
						globalDBRef->AddTail(globalInvObj);
					}
				} else {
					// UGC item with no base item.
					globalInvObj = new CGlobalInventoryObj();
					newItem.CloneFromBaseGLID(&globalInvObj->m_itemData, &newItem, globalId);
					globalDBRef->AddTail(globalInvObj);
				}
			} else // item was not found in the db
			{
				// add it to the missing items list, will make a web call later to get this item info
				missingGLIDs.insert(globalId);
			}
		}

		// fill in the base items
		std::map<int, CItemObj> baseItems;
		if (refreshItemsData(missingBaseGLIDs, &baseItems)) // get item info from db
		{
			for (auto basePosition = missingBaseGLIDs.begin(); basePosition != missingBaseGLIDs.end(); basePosition++) {
				std::map<int, CItemObj>::iterator baseIterator = baseItems.find(*basePosition);
				if (baseIterator != baseItems.end()) // base item was found in the db
				{
					CItemObj newBaseItem = (*baseIterator).second;

					// add base item
					CGlobalInventoryObj *baseGlobalInvObj = new CGlobalInventoryObj();
					newBaseItem.CloneFromBaseGLID(&baseGlobalInvObj->m_itemData, &newBaseItem, newBaseItem.m_globalID);
					globalDBRef->AddTail(baseGlobalInvObj);

					// add all the items that have this base glid
					std::pair<std::multimap<int, CItemObj>::iterator, std::multimap<int, CItemObj>::iterator> missingBaseRange = itemsWithMissingBaseGLIDs.equal_range(newBaseItem.m_globalID);
					for (std::multimap<int, CItemObj>::iterator missingBaseIterator = missingBaseRange.first; missingBaseIterator != missingBaseRange.second; missingBaseIterator++) {
						CItemObj newItem = (*missingBaseIterator).second;

						CGlobalInventoryObj *globalInvObj = new CGlobalInventoryObj();
						baseGlobalInvObj->m_itemData->CloneFromBaseGLID(&globalInvObj->m_itemData, &newItem, newItem.m_globalID);
						globalDBRef->AddTail(globalInvObj);
					}
				}
			}
		} else {
			LogError("Error refreshing groupped base item data");
		}

		if (!localPlayers()) {
			for (auto position = missingGLIDs.begin(); position != missingGLIDs.end(); position++) {
				getItemFromWeb(*position);
			}
		}
	} else {
		LogError("Error refreshing groupped item data");
	}
}

CGlobalInventoryObj *MySqlBase::addCustomItem(GLID glid) {
	//NOTE: this function apply to all item types. Although the second getItemData call is currently only
	// apply to UGC mesh items (which have their templates come from DB, too). In the future if we allow
	// derivable UGC mesh clothings we can continue using the same logic. - Yanfeng 06/04/2009

	// Item not in our global inventory, i.e. a custom item. Add it to global inventory.
	CGlobalInventoryObj *globalInvObj = NULL;
	CItemObj newItem;

	bool res;
	res = getItemData(glid, &newItem);
	if (res) {
		CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();

		if (newItem.m_baseGlid != 0) {
			CGlobalInventoryObj *baseGlobalInventoryObj = NULL;

			baseGlobalInventoryObj = globalDBRef->GetByGLID(newItem.m_baseGlid);

			//Added by Yanfeng to support UGC mesh items (Apr 2009)
			//UGC mesh items does not have a template in global inventory

			if (baseGlobalInventoryObj == NULL) //If no existing item matches base GLID
			{
				//Create a new base item from DB with base GLID
				CItemObj *baseItem = new CItemObj();
				if (getItemData(newItem.m_baseGlid, baseItem)) {
					baseGlobalInventoryObj = new CGlobalInventoryObj();
					baseGlobalInventoryObj->m_itemData = baseItem;
					globalDBRef->AddTail(baseGlobalInventoryObj);
				} else {
					// base item not in DB?
					ASSERT(FALSE);
					LogError("Base GLID " << newItem.m_baseGlid << " not found int DB while trying to create a base global inventory obj");
					delete baseItem;
					res = false;
				}
			}

			if (res && baseGlobalInventoryObj != NULL) {
				globalInvObj = new CGlobalInventoryObj();
				// Overwrite specifics for our obj
				baseGlobalInventoryObj->m_itemData->CloneFromBaseGLID(&globalInvObj->m_itemData, &newItem, glid);
				globalDBRef->AddTail(globalInvObj);
			}
		} else {
			//UGC item with no base item.
			globalInvObj = new CGlobalInventoryObj();
			newItem.CloneFromBaseGLID(&globalInvObj->m_itemData, &newItem, glid);
			globalDBRef->AddTail(globalInvObj);
		}
	} else if (!localPlayers()) // if getting players/items remotely, see if we can get it
	{
		globalInvObj = getItemFromWeb(glid);
	}

	return globalInvObj;
}

CGlobalInventoryObj *MySqlBase::getItemFromWeb(GLID glid) {
	if (!IS_VALID_GLID(glid))
		return nullptr;

	CGlobalInventoryObj *pGIO = nullptr;

	START_TIMING(m_timingLogger, "DB:getItemFromWeb");

	Query query = m_conn->query();

	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;
	std::string url(m_conn->getItemsUrl());
	std::string::size_type p = url.find("{0}");
	if (p == std::string::npos) {
		LogWarn("getItemFromWeb URL missing {0}");
		jsAssert(false); // need global_id
		return pGIO;
	}

	url = url.replace(p, 3, numToString(glid, "%d"));
	std::string resultXml;
	size_t resultSize = 0;
	if (!GetBrowserPage(url, resultXml, resultSize, httpStatusCode, httpStatusDescription)) {
		LogWarn("getItemFromWeb Error: " << httpStatusCode << " " << httpStatusDescription);
		IServerEngine::GetInstance()->setFailedLoadGlid(glid);
		return pGIO;
	}

	// fill in the item, and maybe base item, too
	CItemObj *newItem = 0;
	CItemObj *baseItem = 0;
	TiXmlDocument doc;
	doc.Parse(resultXml.c_str());
	if (doc.Error()) {
		LogError("Error loading newItem XML " << doc.ErrorDesc());
		return pGIO;
	}
	if (!xmlFillInItem(m_logger, &doc, &newItem, &baseItem)) {
		LogWarn("getItemFromWeb Error parsing XML");
		return pGIO;
	}

	bool res = true;

	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();
	CGlobalInventoryObj *baseGlobalInventoryObj = NULL;

	BEGIN_TRY_SQL();
	if (newItem->m_baseGlid > 0) {
		baseGlobalInventoryObj = globalDBRef->GetByGLID(newItem->m_baseGlid);
		if (baseGlobalInventoryObj == NULL) //If no existing item matches base GLID
		{
			if (baseItem) {
				baseGlobalInventoryObj = new CGlobalInventoryObj();
				baseGlobalInventoryObj->m_itemData = baseItem;
				globalDBRef->AddTail(baseGlobalInventoryObj);
				addItemToDb1(query, m_logger, baseGlobalInventoryObj->m_itemData);
			} else {
				// base item not in DB?
				ASSERT(FALSE);
				LogError("Base GLID " << newItem->m_baseGlid << " not found int DB while trying to create a base global inventory obj");
				IServerEngine::GetInstance()->setFailedLoadGlid(glid);
				res = false;
			}
		}

		if (res && baseGlobalInventoryObj != NULL) {
			pGIO = new CGlobalInventoryObj();
			// Overwrite specifics for our obj
			baseGlobalInventoryObj->m_itemData->CloneFromBaseGLID(&pGIO->m_itemData, newItem, glid);
			globalDBRef->AddTail(pGIO);
			addItemToDb1(query, m_logger, pGIO->m_itemData);
		}
	} else {
		//UGC item with no base item.
		pGIO = new CGlobalInventoryObj();
		newItem->CloneFromBaseGLID(&pGIO->m_itemData, newItem, glid);
		globalDBRef->AddTail(pGIO);
		addItemToDb1(query, m_logger, pGIO->m_itemData);
	}

	if (newItem->m_passList) {
		for (PassList::PassIds::iterator i = newItem->m_passList->ids().begin(); i != newItem->m_passList->ids().end(); i++) {
			addItemPassToDb(query, m_logger, glid, *i);
		}
	}

	if (!newItem->m_exclusionGroupIDs.empty()) {
		for (auto i = newItem->m_exclusionGroupIDs.begin(); i != newItem->m_exclusionGroupIDs.end(); i++) {
			addItemParameterToDb(query, m_logger, glid, PARAM_EXCLUSION_GROUPS, *i);
		}
	}

	// if the item has associated animations, add those too
	for (unsigned int i = 0; i < newItem->m_animations.size(); ++i) {
		addItemAnimationToDb(query, m_logger, glid, pGIO->m_itemData->m_animations[i]);
	}

	// Save DO use value to DB (skip zero values to save space)
	if (newItem->m_useType == USE_TYPE_ADD_DYN_OBJ && newItem->m_miscUseValue != 0) {
		addItemParameterToDb(query, m_logger, glid, (int)newItem->m_useType, newItem->m_miscUseValue);
	}

	delete newItem; // temporary, clone copied it
	END_TRY_SQL();
	return pGIO;
}

bool MySqlBase::getItemData(int glid, CItemObj *item) {
	return refreshItemData(glid, item, true);
}

bool MySqlBase::refreshItemData(int glid, CItemObj *item, bool loadPassList) {
	bool ret = true;
	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << " SELECT 0, 0, market_cost, selling_price, required_skill, required_skill_level, use_type, IFNULL(ip.value,0) as use_value, inventory_type, base_global_id, expired_duration, permanent, is_default, visual_ref, is_derivable, stackable, IFNULL(ip2.value, '') as exclusion_groups, actor_group "
			 " FROM items i LEFT OUTER JOIN item_parameters ip ON i.global_id = ip.global_id AND i.use_type = ip.param_type_id "
			 " LEFT OUTER JOIN item_parameters ip2 ON i.global_id = ip2.global_id AND ip2.param_type_id = "
		  << PARAM_EXCLUSION_GROUPS
		  << " WHERE i.global_id = %0";
	query.parse();

	StoreQueryResult results = query.store((int)glid);

	auto rows = results.num_rows();
	if (rows == 1) {
		item->m_globalID = glid;

		Row row = results.at(0);
		//item->m_itemName = (const char*)row.at(0);			// Do not store item name and description for server.
		//item->m_itemDescription = (const char*)row.at(1);		// SQL query uses placeholders to avoid index changes for now.
		item->m_marketCost = row.at(2);
		item->m_sellingPrice = row.at(3);
		item->m_requiredSkillType = row.at(4);
		item->m_requiredSkillLevel = row.at(5);
		item->m_useType = (USE_TYPE)(int)row.at(6);
		item->m_miscUseValue = row.at(7);
		item->m_itemType = row.at(8);
		item->m_baseGlid = row.at(9);
		item->m_expiredDuration = row.at(10);
		item->m_permanent = row.at(11);
		item->m_defaultArmedItem = row.at(12);
		item->m_visualRef = row.at(13);
		item->m_derivable = row.at(14);
		item->m_stackable = row.at(15);
		std::string sExclusionGroups = (const char *)row.at(16);
		item->ParseExclusionGroupsCSV(sExclusionGroups);
		item->m_actorGroup = row.at(17);

		if (loadPassList)
			updateItemPassList(item);
	} else {
		LogError("Item Query FAILED - resultRows=" << rows << " glid=" << glid);
		ret = false;
	}

	END_TRY_SQL();
	return ret;
}

bool MySqlBase::refreshItemsData(const std::set<int> &glids, std::map<int, CItemObj> *items) {
	jsVerifyReturn(items != NULL, false);

	if (glids.empty()) {
		// Nothing to do
		return true;
	}

	// build in statement
	std::stringstream strm;
	for (auto position = glids.cbegin(); position != glids.cend(); position++) {
		strm << *position << ",";
	}
	std::string inString(strm.str());
	inString = inString.substr(0, inString.size() - 1);

	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << " SELECT 0, 0, market_cost, selling_price, required_skill, required_skill_level, use_type, IFNULL(ip.value,0) as use_value, "
			 " inventory_type, base_global_id, expired_duration, permanent, is_default, visual_ref, is_derivable, stackable, IFNULL(ip2.value, '') as exclusion_groups, actor_group, i.global_id "
			 " FROM items i "
			 " LEFT OUTER JOIN item_parameters ip  ON i.global_id =  ip.global_id AND  ip.param_type_id = i.use_type "
			 " LEFT OUTER JOIN item_parameters ip2 ON i.global_id = ip2.global_id AND ip2.param_type_id = "
		  << PARAM_EXCLUSION_GROUPS
		  << " WHERE i.global_id IN (" << inString << ")";
	query.parse();

	StoreQueryResult results = query.store();

	for (unsigned int i = 0; i < results.num_rows(); i++) {
		// create the CItemObjs
		CItemObj item;
		Row row = results.at(i);

		item.m_globalID = row.at(18);
		//item.m_itemName = (const char*)row.at(0);			// Do not store item name and description for server.
		//item.m_itemDescription = (const char*)row.at(1);	// SQL query uses placeholders to avoid index changes for now.
		item.m_marketCost = row.at(2);
		item.m_sellingPrice = row.at(3);
		item.m_requiredSkillType = row.at(4);
		item.m_requiredSkillLevel = row.at(5);
		item.m_useType = (USE_TYPE)(int)row.at(6);
		item.m_miscUseValue = row.at(7);
		item.m_itemType = row.at(8);
		item.m_baseGlid = row.at(9);
		item.m_expiredDuration = row.at(10);
		item.m_permanent = row.at(11);
		item.m_defaultArmedItem = row.at(12);
		item.m_visualRef = row.at(13);
		item.m_derivable = row.at(14);
		item.m_stackable = row.at(15);

		std::string sExclusionGroups = (const char *)row.at(16);
		item.ParseExclusionGroupsCSV(sExclusionGroups);
		item.m_actorGroup = row.at(17);

		(*items)[item.m_globalID] = item;
	}

	query.reset();
	query << " SELECT global_id, pass_group_id from pass_group_items "
		  << " WHERE global_id IN (" << inString << ")";
	query.parse();

	results = query.store();

	for (unsigned int i = 0; i < results.num_rows(); i++) {
		int globalId = atoi(results.at(i)[0]);

		auto passIterator = items->find(globalId);
		if (passIterator != items->end()) {
			CItemObj item = passIterator->second;

			if (item.m_passList == 0)
				item.m_passList = new PassList;
			item.m_passList->addPass(atoi(results.at(i)[1]));
		}
	}

	END_TRY_SQL();

	return true;
}

void MySqlBase::updateItemPassList(CItemObj *item) {
	jsVerifyReturnVoid(item && item->m_globalID > 0);
	Query query = m_conn->query();

	// get pass lists for item, if exist (added 8/08)
	query.reset();
	query << " SELECT pass_group_id from pass_group_items "
		  << " WHERE global_id = %0 ";
	query.parse();

	// wipe pass list
	if (item->m_passList) {
		DELETE_AND_ZERO(item->m_passList);
	}

	LogTrace(query.str((int)item->m_globalID));
	StoreQueryResult results = query.store((int)item->m_globalID);
	if (results.num_rows() > 0) {
		if (item->m_passList == 0)
			item->m_passList = new PassList;

		for (UINT i = 0; i < results.num_rows(); i++) {
			item->m_passList->addPass(atoi(results.at(i)[0]));
		}
	}
}

void MySqlBase::getInventory(const char *playerName, CInventoryObjList **playerInv, CInventoryObjList **bankInv, StoreQueryResult &res) {
	// If only got updates and there are no updates,
	// one record returned with 0 values. Checking two fields here.
	if (res.num_rows() == 1) {
		Row r = res.at(0);
		if (((LONG)r.at(INV_PLAYER_ID) == 0) &&
			((LONG)r.at(INV_GLID) == 0)) {
			return; // getting updates, and none, so nothing to do
		}
	}

	// clear out the player's inventories and replace them
	if (playerInv) {
		if (*playerInv)
			(*playerInv)->SafeDelete();
		else
			*playerInv = new CInventoryObjList();
	}

	if (bankInv) {
		if (*bankInv)
			(*bankInv)->SafeDelete();
		else
			*bankInv = new CInventoryObjList;
	}

	// invResult can be empty if no pending items existed to be
	// added to the inventory. Thus, leave inventory as is.
	if (res.num_rows() == 0)
		return;

	// Check for global inventory as well
	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();

	if (globalDBRef == 0) {
		LogError("Can't get CGlobalInventoryObjList::GetInstance in addInventoryItem");
		return;
	}

	for (UINT i = 0; i < res.num_rows(); i++) {
		Row r = res.at(i);
		if ((LONG)r.at(INV_QTY) <= 0)
			continue; // deletes may remain in db
		CGlobalInventoryObj *baseGlobalInventoryObj = (CGlobalInventoryObj *)globalDBRef->GetByGLID(r.at(INV_GLID));

		if (baseGlobalInventoryObj == 0) {
			// Item not in our global inventory, i.e. a custom item. Add it to global inventory.
			if (addCustomItem((GLID)r.at(INV_GLID)) == NULL)
				LogWarn("Global inventory page fault on GLID. Not found in DB. Player:" << playerName << " "
																						<< "GLID:" << r.at(INV_GLID));
		}

		if (r.at(INV_TYPE) == DB_PLAYER_INVENTORY && playerInv && *playerInv) {
			addInventoryItem(playerName, *playerInv, r);
		} else if (r.at(INV_TYPE) == DB_BANK_INVENTORY && bankInv && *bankInv) {
			addInventoryItem(playerName, *bankInv, r);
		}
		// else we don't need it
	}
	// since only updatesOnly current inv, only clear current inv
	if (playerInv && *playerInv)
		(*playerInv)->ClearDirty();
	if (bankInv && *bankInv)
		(*bankInv)->ClearDirty();
}

#define ACCOUNT_RS_TAG "Table"
#define INV_RS_TAG "Table3"
#define GAME_INV_RS_TAG "Table9"

bool getValue(const char *&value, TiXmlElement *child, const char *elementName) {
	value = 0;

	if (child) {
		TiXmlHandle h(child);
		TiXmlText *t = h.FirstChild(elementName).FirstChild().Text();
		if (t)
			value = t->Value();
	}

	return (value != 0);
}

template <class C>
bool getIntValue(C &value, TiXmlElement *child, const char *elementName) {
	const char *temp;
	if (getValue(temp, child, elementName)) {
		value = (C)atol(temp);
		return true;
	}
	return false;
}

bool MySqlBase::xmlGetInventory(const char *playerName, CInventoryObjList **playerInv, CInventoryObjList **bankInv, TiXmlElement *table) {
	// clear out the player's inventories and replace them
	if (playerInv) {
		if (*playerInv)
			(*playerInv)->SafeDelete();
		else
			*playerInv = new CInventoryObjList();
	}

	if (bankInv) {
		if (*bankInv)
			(*bankInv)->SafeDelete();
		else
			*bankInv = new CInventoryObjList;
	}

	// Check for global inventory as well
	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();

	if (globalDBRef == 0) {
		LogError("Can't get CGlobalInventoryObjList::GetInstance in addInventoryItem");
		return true;
	}

	const char *tableName = table->Value();
	do {
		int qty = 0;
		int glid = 0;
		int invSubType = IT_GIFT;
		const char *invType = 0;
		const char *armed = 0;

		if (!getValue(invType, table, "inventory_type") ||
			!getValue(armed, table, "armed") ||
			!getIntValue(qty, table, "quantity") ||
			!getIntValue(glid, table, "global_id") ||
			!getIntValue(invSubType, table, "inventory_sub_type")) {
			return false;
		}

		if (qty <= 0)
			continue; // deletes may remain in db

		CGlobalInventoryObj *baseGlobalInventoryObj = (CGlobalInventoryObj *)globalDBRef->GetByGLID(glid);

		if (baseGlobalInventoryObj == 0) {
			// Item not in our global inventory, i.e. a custom item. Add it to global inventory.
			if (addCustomItem((GLID)glid) == NULL)
				LogWarn("Global inventory page fault on GLID. Not found in DB. Player:" << playerName << " "
																						<< "GLID:" << glid);
		}

		if (strcmp(invType, DB_PLAYER_INVENTORY) == 0 && playerInv && *playerInv) {
			addInventoryItem(playerName, *playerInv, glid, qty, strcmp(armed, DB_TRUE) == 0, invSubType, 0);
		} else if (strcmp(invType, DB_BANK_INVENTORY) == 0 && bankInv && *bankInv) {
			addInventoryItem(playerName, *bankInv, glid, qty, strcmp(armed, DB_TRUE) == 0, invSubType, 0);
		}
		// else we don't need it
		table = table->NextSiblingElement();
	} while (table && strcmp(table->Value(), tableName) == 0);
	// since only updatesOnly current inv, only clear current inv
	if (playerInv && *playerInv)
		(*playerInv)->ClearDirty();
	if (bankInv && *bankInv)
		(*bankInv)->ClearDirty();
	return true;
}

bool MySqlBase::xmlGetGameInventorySync(const char *playerName, int kanevaUserId, CInventoryObjList **playerInv, CInventoryObjList **bankInv, TiXmlElement *table) {
	// Call inventory sync stored procedure for each entry in the table
	// each record will be inserted or updated to have the correct quantity
	// the SP will return the updated quantity for the server to use

	CGlobalInventoryObjList *globalDBRef = CGlobalInventoryObjList::GetInstance();
	if (globalDBRef == 0) {
		LogError("Can't get CGlobalInventoryObjList::GetInstance in xmlGetGameInventorySync");
		return true;
	}
	const char *nodeName = table->Value();
	for (TiXmlElement *element = table; element != 0; element = element->NextSiblingElement()) {
		if (strcmp(element->Value(), nodeName) != 0)
			// we have moved past this entry's elements
			break;

		// Unique identifier for sync record
		int game_id, glid, inventory_sub_type;
		const char *inventory_type;

		int quantity_purchased;

		if (!getIntValue(game_id, element, "game_id"))
			return false;

		if (!getIntValue(glid, element, "global_id"))
			return false;

		if (!getValue(inventory_type, element, "inventory_type"))
			return false;

		if (!getIntValue(inventory_sub_type, element, "inventory_sub_type"))
			return false;

		if (!getIntValue(quantity_purchased, element, "quantity_purchased"))
			return false;

		BEGIN_TRY_SQL();
		Transaction trans(*m_conn);
		Query query = m_conn->query();

		query << "CALL sync_game_inventory_record(%0, %1, %2q, %3, %4, %5, @quantity); SELECT @quantity;";
		query.parse();

		LogTrace(query.str(game_id, glid, inventory_type, inventory_sub_type, kanevaUserId, quantity_purchased));

		StoreQueryResult res = query.store(game_id, glid, inventory_type, inventory_sub_type, kanevaUserId, quantity_purchased);
		res = query.store_next();

		trans.commit();

		if (res.num_rows() <= 0)
			continue;

		int quantity = res.at(0).at(0);

		if (quantity <= 0)
			continue;

		CGlobalInventoryObj *baseGlobalInventoryObj = (CGlobalInventoryObj *)globalDBRef->GetByGLID(glid);

		if (baseGlobalInventoryObj == 0) {
			// Item not in our global inventory, i.e. a custom item. Add it to global inventory.
			if (addCustomItem((GLID)glid) == NULL)
				LogWarn("Global inventory page fault on GLID in Game Inventory Sync. Not found in DB. Player:" << playerName << " "
																											   << "GLID:" << glid);
		}

		if (strcmp(inventory_type, DB_PLAYER_INVENTORY) == 0 && playerInv && *playerInv) {
			addInventoryItem(playerName, *playerInv, glid, quantity, false, inventory_sub_type, 0);
		} else if (strcmp(inventory_type, DB_BANK_INVENTORY) == 0 && bankInv && *bankInv) {
			addInventoryItem(playerName, *bankInv, glid, quantity, false, inventory_sub_type, 0);
		}
		END_TRY_SQL();
	}
	return true;
}

bool MySqlBase::xmlFillInInventory(TiXmlDocument *doc, const char *playerName, int kanevaUserId, CInventoryObjList **playerInv) {
	// get the account "table"
	TiXmlElement *table = 0;
	table = xmlParseResultHeader(m_logger, doc, ACCOUNT_RS_TAG);
	if (table == 0) {
		return false;
	}

	table = table->NextSiblingElement(INV_RS_TAG);
	if (!(table && table->FirstChild("inventory_type"))) {
		LogWarn("Error getting player inventory");
		return false;
	}

	if (!xmlGetInventory(playerName, playerInv, NULL, table)) {
		LogWarn("Error getting player inventory");
		return false;
	}

	for (;;) {
		table = table->NextSiblingElement();
		if (!table) {
			break;
		}

		// add game inventory sync records to the player's inventory
		if (strcmp(table->Value(), GAME_INV_RS_TAG) == 0) {
			if (!xmlGetGameInventorySync(playerName, kanevaUserId, playerInv, NULL, table)) {
				LogWarn("Error populating player's game inventory sync records");
			}
			break;
		}
	}
	return true;
}

EVENT_PROC_RC MySqlBase::RefreshPlayerStage1(IEvent *e) {
	RefreshPlayerBackgroundEvent *rpbe = dynamic_cast<RefreshPlayerBackgroundEvent *>(e);
	jsVerifyReturn(rpbe != 0, EVENT_PROC_RC::NOT_PROCESSED);

	if (rpbe->ret != 0) {
		return EVENT_PROC_RC::OK;
	}

	if (!rpbe->resultXml.empty()) {
		// _SCR_ perf tracking
		LogError("InvTrack Begin MySqlBase::RefreshPlayerStage1");
		clock_t startTime = clock();
		int perfTrack_GM = 0;
		// _SCR_ perf tracking

		TiXmlDocument doc;
		doc.Parse(rpbe->resultXml.c_str());

		if (doc.Error()) {
			rpbe->ret = E_INVALIDARG;
			LogInfo("Error loading XML for refresh of inventory " << doc.ErrorDesc());
			return EVENT_PROC_RC::OK;
		}

		if (!(rpbe->updateFlags & PlayerMask::INVENTORY)) {
			return EVENT_PROC_RC::OK;
		}

		if (!xmlFillInInventory(&doc, rpbe->m_playerName.c_str(), rpbe->kanevaUserId, &rpbe->inventory)) {
			rpbe->ret = E_INVALIDARG;
			LogWarn("Error loading XML for refresh of inventory ");
		}

		else if (!localPlayers() && rpbe->isGm) {
			LogError("InvTrack player[" << rpbe->m_playerName.c_str() << "] is a moderator/owner performing inventory merge");
			// _SCR_ perf tracking
			perfTrack_GM = 1;
			// _SCR_ perf tracking
			m_server->addScriptAvailableItemsToInventory(rpbe->inventory);
		}

		// _SCR_ perf tracking
		std::clock_t clockTicksTaken = (std::clock() - startTime);
		LogError("InvTrack_InvRefresh_ player," << rpbe->m_playerName.c_str() << ",GM," << perfTrack_GM << ",CT," << clockTicksTaken);
		// _SCR_ perf tracking

		return EVENT_PROC_RC::OK;
	}

	std::string invType = "P";
	if (rpbe->updateFlags & PlayerMask::INVENTORY) {
		invType = "P";
	} else if (rpbe->updateFlags & PlayerMask::BANK_INVENTORY) {
		invType = "B";
	}

	// _SCR_ WOKINV
	if (rpbe->arraySize == 1) {
		char s[50];
		_snprintf_s(s, _countof(s), _TRUNCATE, "KanevaId = %d", rpbe->kanevaUserId);
		try {
			if (invType == "B") {
				getInventory(s, 0, &rpbe->inventory, rpbe->results[0]);
			} else if (invType == "P") {
				getInventory(s, &rpbe->inventory, 0, rpbe->results[0]);
			}
		} catch (KEPException *e) {
			rpbe->ret = PLAYER_NOT_FOUND;
			LogInfo("Exception in RefreshPlayerStage1 " << e->ToString());
			e->Delete();
		}
	}
	return EVENT_PROC_RC::OK;
}

bool MySqlBase::localPlayers() const {
	return m_conn && m_conn->localPlayers();
}

// do the update, outside of context of transaction and CS
void MySqlBase::updatePlayerNoTrans(CPlayerObject *pPO, ULONG changeMask, bool loggingOut) {
	if (!localPlayers()) {
		// For 3D app dance game, i.e. ddr
		if (pPO->m_dbDirtyFlags & PlayerMask::SKILLS) {
			DataModel *model = PluginHost::singleton().DEPRECATED_getInterfaceNoThrow<DataModel>("AppDataModel");
			PlayerModel *playerModel = (model == 0 ? (PlayerModel *)0 : model->registry().getFacetNoThrow<PlayerModel>("PlayerModel"));
			if (playerModel) {
				KEPModel::SkillVector tmp;
				for (POSITION invPos = pPO->m_skills->GetHeadPosition(); invPos != NULL;) {
					CSkillObject *o = (CSkillObject *)pPO->m_skills->GetNext(invPos);
					tmp.push_back(KEPModel::Skill(o->m_skillType, o->m_currentLevel, o->m_currentExperience));
				}
				try {
					playerModel->updateSkills(pPO->m_handle, tmp);
				} catch (const ModelException &me) {
					LogError("Exception raised updating player skils [" << me.str() << "]");
				}
			} else {
				updateSkills(pPO->m_handle, pPO->m_skills);
			}
			pPO->m_dbDirtyFlags &= ~(PlayerMask::SKILLS);
		}

		if (pPO->m_dbDirtyFlags & PlayerMask::TITLES) {
			updateTitles3DApp(pPO->m_handle, pPO->m_titleList, pPO->m_title);
			pPO->m_dbDirtyFlags &= ~(PlayerMask::TITLES);
		}

		return;
	}

	char queryStr[4096];

	// figure out what to update, see IGameState.h for mask
	changeMask |= pPO->m_dbDirtyFlags;

	if (changeMask == 0) {
		LogDebug("Updated player " << pPO->m_charName << " with mask of " << numToString(changeMask));
		return;
	}

	// if the player has its preArenaInventory populated, the player is currently within an Arena and no database updates should occur
	if (pPO->playerOrigRef()) {
		if (pPO->playerOrigRef()->m_preArenaInventory) {
			if (loggingOut) {
				changeMask &= PlayerMask::PLAYER; // clear all but player so can update logout
			} else {
				LogDebug("No Update to player " << pPO->m_charName << " because player is in arena.");
				return;
			}
		}
	}

	// update the base table entry
	if (((changeMask & (PlayerMask::PLAYER | PlayerMask::PLAYER_EDB | PlayerMask::TITLES)) != 0) || loggingOut || (pPO->m_timeDbUpdate == TIME_UNDEFINED) // logging in
	) {
		Query query = m_conn->query();

		strcpy_s(queryStr, _countof(queryStr), "UPDATE `players` SET `last_update_time` = NOW()");

		if ((changeMask & PlayerMask::TITLES) != 0) {
			std::string title(pPO->m_title);
			query.escape_string(&title);
			sprintf_s(queryStr, _countof(queryStr), "%s, `title` = '%s'", queryStr, title.c_str());
		}

		if ((changeMask & PlayerMask::PLAYER_EDB) != 0) {
			// things that don't change in gameplay
			// << " `name` = " << quote << player->m_charName
			sprintf_s(queryStr, _countof(queryStr),
				"%s "
				", `original_EDB` = %d"
				", `team_id` = %d"
				", `race_id` = %d"
				", `spawn_config_index` = %d",
				queryStr,
				pPO->m_originalEDBCfg,
				(int)0, //player->m_teamID,
				(int)0, //player->m_raceID,
				pPO->m_bornSpawnCfgIndex);
		}

		if (loggingOut) {
			strcat_s(queryStr, _countof(queryStr), ", `in_game` = 'F'");
		} else if (pPO->m_timeDbUpdate == TIME_UNDEFINED) {
			sprintf_s(queryStr, _countof(queryStr),
				"%s "
				", `server_id` = %d"
				", `in_game` = 'T'",
				queryStr,
				m_serverId);
		}
		sprintf_s(queryStr, _countof(queryStr), "%s WHERE `player_id` = %d", queryStr, pPO->m_handle);
		LogTrace(queryStr);
		query.exec(queryStr);
	}

	// update the player_presences table
	if ((changeMask & (PlayerMask::POSITION | PlayerMask::RESPAWN_POINT | PlayerMask::PLAYER_EDB)) != 0) {
		Query query = m_conn->query();

		strcpy_s(queryStr, "UPDATE `player_presences` SET ");

		if ((changeMask & PlayerMask::POSITION) != 0) {
			MovementData md = pPO->InterpolatedMovementData();
			sprintf_s(queryStr, _countof(queryStr),
				"%s "
				" `position_x` = %f, `position_y` = %f, `position_z` = %f"
				", `direction_x` = %f, `direction_y` = %f, `direction_z` = %f"
				", `energy` = %d"
				", `max_energy` = %d"
				", `count_down_to_pardon` = %d"
				", `arena_base_points` = %d"
				", `arena_total_kills` = %d",
				queryStr,
				md.pos.x,
				md.pos.y,
				md.pos.z,
				md.dir.x,
				md.dir.y,
				md.dir.z,
				(int)0, //player->m_energy,
				(int)0, //player->m_maxEnergy,
				(int)0, //player->m_countDownToMurderPardon,
				(int)0, //player->m_arenaBasePoints,
				(int)0 //player->m_arenaTotalKills
			);
		}

		if ((changeMask & PlayerMask::RESPAWN_POINT) != 0) {
			CPlayerObject *origPlayer = pPO;
			// respawn, and other details are on the original only
			if (pPO->playerOrigRef() != 0) {
				origPlayer = pPO->playerOrigRef();
			}

			if ((changeMask & PlayerMask::POSITION) != 0)
				strcat_s(queryStr, _countof(queryStr), ",");

			// assert to find bug
			jsAssert(pPO->m_housingZoneIndex.zoneIndex() > 0 && pPO->m_housingZoneIndex.zoneIndex() < 255);

			sprintf_s(queryStr, _countof(queryStr),
				"%s "
				" `respawn_zone_index` = %d"
				", `respawn_zone_instance_id` = %d"
				", `respawn_position_x` = %f, `respawn_position_y` = %f, `respawn_position_z` = %f"
				", `housing_zone_index` = %d"
				", `housing_zone_index_plain` = %d",
				queryStr,
				origPlayer->m_respawnZoneIndex.getClearedInstanceId(),
				origPlayer->m_respawnZoneIndex.GetInstanceId(),
				origPlayer->m_respawnPosition.x,
				origPlayer->m_respawnPosition.y,
				origPlayer->m_respawnPosition.z,
				pPO->m_housingZoneIndex.getClearedInstanceId(),
				pPO->m_housingZoneIndex.zoneIndex());
		}

		if ((changeMask & PlayerMask::PLAYER_EDB) != 0) {
			if ((changeMask & (PlayerMask::POSITION | PlayerMask::RESPAWN_POINT)) != 0)
				strcat_s(queryStr, _countof(queryStr), ",");

			sprintf_s(queryStr, _countof(queryStr),
				"%s "
				"`scale_factor_x` = %f, `scale_factor_y` = %f, `scale_factor_z` = %f",
				queryStr,
				pPO->m_scaleFactor.x,
				pPO->m_scaleFactor.y,
				pPO->m_scaleFactor.z);
		}
		sprintf_s(queryStr, _countof(queryStr),
			"%s "
			" WHERE `player_id` = %d",
			queryStr,
			pPO->m_handle);
		LogTrace(queryStr);
		query.exec(queryStr);
	}

	// update the player_zones table
	// no need to update spawn info if logging out
	if (!loggingOut && (changeMask & PlayerMask::SPAWN_INFO) != 0) {
		Query query = m_conn->query("CALL playerZoning	( %0, %1, %2, %3, %4q )");
		query.parse();

		query.execute((int)pPO->m_handle,
			(int)m_serverId,
			(int)pPO->m_currentZoneIndex.getClearedInstanceId(),
			(int)pPO->m_currentZoneIndex.GetInstanceId(),
			(const char *)pPO->m_currentWorldMap);

		// for SPs MUST call these to avoid disconnect
		checkMultipleResultSets(query, "player_zones");
	}

	// update the inventory
	if ((changeMask & PlayerMask::INVENTORY) == PlayerMask::INVENTORY) {
		updateInventory(pPO->m_handle, pPO->m_inventory, DB_PLAYER_INVENTORY);
	}

	// update the inventory
	if ((changeMask & PlayerMask::BANK_INVENTORY) == PlayerMask::BANK_INVENTORY) {
		// bank inventory is never on runtime player
		if (pPO->playerOrigRef() != 0)
			updateInventory(pPO->m_handle, pPO->playerOrigRef()->m_bankInventory, DB_BANK_INVENTORY);
		else
			updateInventory(pPO->m_handle, pPO->m_bankInventory, DB_BANK_INVENTORY);
	}

	if ((changeMask & PlayerMask::SKILLS) == PlayerMask::SKILLS) {
		DataModel *model = PluginHost::singleton().DEPRECATED_getInterfaceNoThrow<DataModel>("AppDataModel");
		PlayerModel *playerModel = (model == 0 ? (PlayerModel *)0 : model->registry().getFacetNoThrow<PlayerModel>("PlayerModel"));
		if (playerModel) {
			KEPModel::SkillVector tmp;
			for (POSITION invPos = pPO->m_skills->GetHeadPosition(); invPos != NULL;) {
				CSkillObject *o = (CSkillObject *)pPO->m_skills->GetNext(invPos);
				tmp.push_back(KEPModel::Skill(o->m_skillType, o->m_currentLevel, o->m_currentExperience));
			}
			try {
				playerModel->updateSkills(pPO->m_handle, tmp);
			} catch (const ModelException &me) {
				LogError("Exception raised while updating player skills [" << me.str() << "]");
			}
		} else {
			updateSkills(pPO->m_handle, pPO->m_skills);
		}
	}

	// mbb 06/17/08 -
	// this was missed in the merge from fame to dev/prod
	if ((changeMask & PlayerMask::TITLES) == PlayerMask::TITLES) {
		updateTitles(pPO->m_handle, pPO->m_titleList);
	}

	if ((changeMask & PlayerMask::STATS) == PlayerMask::STATS) {
		updateStats(pPO->m_handle, pPO->m_stats);
	}

	if ((changeMask & PlayerMask::QUESTS) == PlayerMask::QUESTS) {
		// quests are never on runtime player
		if (pPO->playerOrigRef() != 0)
			updateQuests(pPO->m_handle, pPO->playerOrigRef()->m_questJournal);
		else
			updateQuests(pPO->m_handle, pPO->m_questJournal);
	}

	if ((changeMask & PlayerMask::GETSETS) == PlayerMask::GETSETS || pPO->GetSet::IsDirty()) {
		updateGetSet(pPO);
	}

	if ((changeMask & PlayerMask::NOTERIETIES) == PlayerMask::NOTERIETIES) {
		updateNoteriety(pPO->m_handle, pPO->m_noteriety);
	}

	if ((changeMask & PlayerMask::DEFCONFIG) == PlayerMask::DEFCONFIG) {
		// at this time, never changed after creation
		updateDeformableMeshes(pPO);
	}

	if ((changeMask & PlayerMask::CLAN_HOUSE_INVENTORY) == PlayerMask::CLAN_HOUSE_INVENTORY) {
		// TODO:
	}

	if ((changeMask & PlayerMask::PLAYER_HOUSE_INVENTORY) == PlayerMask::PLAYER_HOUSE_INVENTORY) {
		// TODO:
	}

	pPO->m_dbDirtyFlags &= ~changeMask;
	if (pPO->m_dbDirtyFlags == 0)
		pPO->m_timeDbUpdate = fTime::TimeMs();

	LogDebug("Updated player " << pPO->m_charName << " with mask of " << numToString(changeMask));
}

/*
 *Function Name:getPlayerSkills 
 *
 *Parameters:	playerId 
 *  			skill object list to be filled out
 *
 *Description: Called to fill out a player's skills 
 *  		   Currently used in 3D Apps to get player skills for DDR game
 * 
 *Returns:	bool true - filled out 
 *				 false - not found 
 *
 */
bool MySqlBase::getPlayerSkills(int playerId, CSkillObjectList *&skills) {
	bool ret = true;

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << " SELECT skill_id, name, type, gain_increment, basic_ability_skill, basic_ability_exp_mastered, basic_ability_function, "
			 " skill_gains_when_failed, max_multiplier, use_internal_timer, filter_eval_level, current_level, current_experience "
			 " FROM player_skills_view "
			 " WHERE player_id = %0";

	query.parse();

	LogTrace(query.str((int)playerId));
	StoreQueryResult results = query.store((int)playerId);

	if (results.num_rows() > 0) {
		skills = new CSkillObjectList();

		for (UINT i = 0; i < results.num_rows(); i++) {
			Row r = results.at(i);
			CSkillObject *o = new CSkillObject();
			o->m_skillId = r.at(0);
			o->m_skillName = r.at(1);
			o->m_skillType = r.at(2);
			o->m_gainIncrement = r.at(3);
			o->m_basicAbilitySkill = r.at(4) != DB_FALSE;
			o->m_basicAbilityExpMastered = r.at(5);
			o->m_basicAbilityFunction = r.at(6);
			o->m_skillGainsWhenFailed = r.at(7);
			o->m_maxMultiplier = r.at(8);
			o->m_useInternalTimer = r.at(9) != DB_FALSE;
			o->m_filterEvalLevel = r.at(10) != DB_FALSE;
			o->m_currentLevel = r.at(11);
			o->m_currentExperience = r.at(12);
			skills->AddTail(o);
		}
	} else {
		ret = false;
	}

	END_TRY_SQL();
	return ret;
}

/*
 *Function Name:get3DAppPlayerTitles 
 *
 *Parameters:	playerId 
 *				titles - available titles 
 *  			title be filled out
 *
 *Description: Called to fill out a player's titles 
 *  		   Currently used in 3D Apps to get player titles for DDR game
 * 
 *Returns:	bool true - filled out 
 *				 false - not found 
 *
 */
bool MySqlBase::get3DAppPlayerTitles(int playerId, CTitleObjectList *&titles, CStringA &title) {
	bool ret = true;

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << " SELECT t.title_id, t.name, pt.active "
			 " FROM player_titles pt, titles t "
			 " WHERE player_id = %0 AND t.title_id=pt.title_id ORDER BY pt.active DESC";

	query.parse();

	LogTrace(query.str((int)playerId));
	StoreQueryResult results = query.store((int)playerId);

	if (results.num_rows() > 0) {
		titles = new CTitleObjectList();

		for (UINT i = 0; i < results.num_rows(); i++) {
			Row r = results.at(i);

			CTitleObject *o = new CTitleObject();
			o->m_titleID = r.at(0);
			o->m_titleName = r.at(1);
			bool active = r.at(2);
			titles->AddTail(o);

			if (active) {
				title = o->m_titleName.c_str();
			}
		}
	} else {
		ret = false;
	}

	END_TRY_SQL();
	return ret;
}

void MySqlBase::updateSkills(int playerId, CSkillObjectList *skills) {
	Query cleanup = m_conn->query("delete from `player_skills` where player_id = %0");
	cleanup.parse();
	LogTrace(cleanup.str((int)playerId));
	cleanup.execute((int)playerId);

	if (skills != 0) {
		// re-add
		for (POSITION invPos = skills->GetHeadPosition(); invPos != NULL;) {
			// inventory loop
			CSkillObject *o = (CSkillObject *)skills->GetNext(invPos);

			Query query = m_conn->query(
				"INSERT INTO `player_skills`"
				" ( `player_id`, `skill_id`, `current_level`, `current_experience`)"
				" VALUES ( %0, %1, %2, %3 )");
			query.parse();
			query.execute(playerId, o->m_skillType, o->m_currentLevel, o->m_currentExperience);
		}
	}
}

void MySqlBase::updateTitles(int playerId, CTitleObjectList *titles) {
	// mbb 06/17/08 -
	// All titles can not be deleted then reloaded from the server because new titles
	// can be added to the player from outside the game while the player is in game
	// updateTitles will only insert new titles to the db
	/*	
	Query cleanup = m_conn->query();
	cleanup << "delete from `player_titles` where player_id = %0";
	TRACE_QUERY( cleanup, m_logger );
	cleanup.parse();
	cleanup.execute( playerId );
	*/

	if (titles != 0) {
		// re-add
		for (auto itr = titles->begin(); itr != titles->end(); ++itr) {
			CTitleObject *o = *itr;

			Query query = m_conn->query(
				"INSERT IGNORE INTO `player_titles` "
				" ( `player_id`, `title_id` )"
				" VALUES ( %0, %1 )");
			query.parse();
			query.execute(playerId, o->m_titleID);
		}
	}
}

void MySqlBase::updateTitles3DApp(int playerId, CTitleObjectList *titles, const CStringA &titleName) {
	// mbb 06/17/08 -
	// All titles can not be deleted then reloaded from the server because new titles
	// can be added to the player from outside the game while the player is in game
	// updateTitles will only insert new titles to the db
	/*	
	Query cleanup = m_conn->query();
	cleanup << "delete from `player_titles` where player_id = %0";
	TRACE_QUERY( cleanup, m_logger );
	cleanup.parse();
	cleanup.execute( playerId );
	*/

	if (titles != 0) {
		int active = 0;

		// re-add
		for (auto itr = titles->begin(); itr != titles->end(); ++itr) {
			CTitleObject *o = *itr;

			if (o->m_titleName == (const char *)titleName) {
				// Doing without a players table for now. Use active flag to denote title for 3D App
				active = 1;
			} else {
				active = 0;
			}

			Query query = m_conn->query(
				"INSERT IGNORE INTO `player_titles` "
				" ( `player_id`, `title_id`, `active` )"
				" VALUES ( %0, %1, %2 )");
			query.parse();
			query.execute(playerId, o->m_titleID, active);
		}
	}
}

// replace all stats
void MySqlBase::updateStats(int playerId, CStatObjList *stats) {
	Query cleanup = m_conn->query("delete from `player_stats` where player_id = %0");
	cleanup.parse();
	LogTrace(cleanup.str((int)playerId));
	cleanup.execute((int)playerId);

	if (stats != 0) {
		// re-add
		for (POSITION invPos = stats->GetHeadPosition(); invPos != NULL;) {
			// inventory loop
			CStatObj *o = (CStatObj *)stats->GetNext(invPos);

			Query query = m_conn->query();
			player_stats row(playerId,
				o->m_id,
				o->m_currentValue);

			query.insert(row);
			query.execute();
		}
	}
}

void MySqlBase::updateQuests(int playerId, CQuestJournalList *quests) {
	// TODO:
}

void MySqlBase::updateInventory3DApp(int playerId, CInventoryObjList *inventory, const char *inventoryType) {
	// update only what has changed, logging changes
	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;

	if (inventory != 0 && !inventory->IsEmpty()) {
		// find all items in inventory that have an updated ARMED field
		for (POSITION invPos = inventory->GetHeadPosition(); invPos != NULL;) {
			CInventoryObj *pObj = (CInventoryObj *)inventory->GetNext(invPos);

			if (pObj->dirtyFlags() & INV_ARMED_UPDATED) {
				if (pObj->m_itemData == 0) {
					LogError("inventory object null");
					continue;
				}

				std::string url(m_conn->getInventoryUpdaterUrl());
				url += "?gid=" + numToString(m_server->GetGameId(), "%d");
				url += "&action=armed";
				url += "&playerId=" + numToString(playerId, "%d");
				url += "&glid=" + numToString(pObj->m_itemData->m_globalID, "%d");
				url += "&inventoryType=" + std::string(inventoryType);
				url += "&inventorySubType=" + numToString(pObj->inventoryType(), "%d");
				url += "&val=" + std::string(pObj->isArmed() ? DB_TRUE : DB_FALSE);

				// make web call to persit equipment changes in wok
				std::string resultXml;
				int retCode = -1;
				const char *retDesc;

				LogInfo("GetBrowserPage:" << url.c_str());
				size_t resultSize = 0;
				if (!GetBrowserPage(url, resultXml, resultSize, httpStatusCode, httpStatusDescription)) {
					LogError("Error:" << httpStatusCode << " " << httpStatusDescription << " response:" << resultXml);
				} else {
					// the web call was succesfull now check response
					TiXmlDocument xmlDoc;
					xmlDoc.Parse(resultXml.c_str());
					if (xmlDoc.Error()) {
						LogInfo("Error reading web response:" << xmlDoc.ErrorDesc());
						return;
					} else {
						TiXmlHandle docHandle(&xmlDoc);
						TiXmlElement *result = docHandle.FirstChild("Result").ToElement();
						if (result) {
							if (getIntValue(retCode, result, "ReturnCode") && getValue(retDesc, result, "ResultDescription")) {
								if (retCode == 0) {
									LogError("SUCCESS : " << retDesc);
								} else {
									LogError("FAIL : " << retDesc);
								}
							}
						}
					}
				}
			}
		}
	}
}

void MySqlBase::updateInventory(int playerId, CInventoryObjList *inventory, const char *inventoryType) {
	// update only what has changed, logging changes

	if (inventory != 0) {
		// delete ones first
		if (inventory->DeletedItems() != 0 && !inventory->DeletedItems()->IsEmpty()) {
			Query query = m_conn->query();
			std::string ids;
			for (POSITION pos = inventory->DeletedItems()->GetHeadPosition(); pos != NULL;) {
				CInventoryObj *io = (CInventoryObj *)inventory->DeletedItems()->GetNext(pos);
				query << "DELETE FROM inventories "
						 " WHERE player_id = %0 "
						 "   AND global_id = %1 "
						 "   AND inventory_type = %2q "
						 "   AND inventory_sub_type = %3";

				query.parse();
				LogTrace(query.str(playerId, io->m_itemData ? io->m_itemData->m_globalID : 0, inventoryType, io->inventoryType()));
				query.execute(playerId, io->m_itemData ? io->m_itemData->m_globalID : 0, inventoryType, io->inventoryType());
				query.reset();
			}
		}

		if (!inventory->IsEmpty()) {
			// do any inserts in one shot
			std::vector<inventories> insertList;
			std::vector<invTracker> stacked;

			// stack the items for db if not stackable
			for (POSITION invPos = inventory->GetHeadPosition(); invPos != NULL;) {
				// inventory loop
				CInventoryObj *unstackedObj = (CInventoryObj *)inventory->GetNext(invPos);

				// TODO: can this be optimized better?

				// if not stackable need to combine them into one record in DB
				bool found = false;
				if (!unstackedObj->m_itemData->m_stackable) {
					// see if already have one
					for (std::vector<invTracker>::iterator stackedObj = stacked.begin(); stackedObj != stacked.end(); stackedObj++) {
						if (stackedObj->obj->m_itemData && stackedObj->obj->m_itemData->m_globalID == unstackedObj->m_itemData->m_globalID &&
							stackedObj->obj->inventoryType() == unstackedObj->inventoryType()) {
							found = true;
							if ((stackedObj->obj->dirtyFlags() & INV_NEW) == 0 &&
								(unstackedObj->dirtyFlags() & INV_NEW) != 0) {
								stackedObj->obj = unstackedObj; // switch to one w/ NEW status and...
								stackedObj->obj->incQty(0); // set dirty flag since want to update, not add
							} else if (unstackedObj->dirtyFlags() != 0) // if this one's dirty
							{
								stackedObj->obj->incQty(0); // set dirty flag on stacked one
							}

							stackedObj->armed |= unstackedObj->isArmed();
							stackedObj->qty += unstackedObj->getQty();
						}
					}
				}
				if (!found)
					stacked.push_back(invTracker(unstackedObj, unstackedObj->getQty(), unstackedObj->isArmed()));
			}

			for (std::vector<invTracker>::iterator i = stacked.begin(); i != stacked.end(); i++) {
				CInventoryObj *io = i->obj;
				if ((io->dirtyFlags() & INV_NEW) != 0) {
					insertList.push_back(inventories(
						playerId,
						inventoryType,
						io->m_itemData == 0 ? 0 : io->m_itemData->m_globalID,
						io->inventoryType(),
						i->armed != FALSE ? DB_TRUE : DB_FALSE,
						i->qty,
						io->getCharge()));
					LogTrace("Will insert " << io->m_itemData->m_globalID << " for playerId " << playerId << ":" << inventoryType << " qty " << i->qty << " armed " << i->armed);
				} else if ((io->dirtyFlags() & (INV_ARMED_UPDATED | INV_QTY_UPDATED)) != 0) {
					Query query = m_conn->query(
						"UPDATE inventories "
						"   SET quantity = %0,"
						"       armed = %1q, "
						"		  last_touch_datetime = NOW() "
						" WHERE player_id = %2 "
						"   AND global_id = %3 "
						"   AND inventory_type = %4q"
						"   AND inventory_sub_type = %5");

					query.parse();
					LogTrace(query.str(i->qty, i->armed ? DB_TRUE : DB_FALSE, playerId, io->m_itemData == 0 ? 0 : io->m_itemData->m_globalID, inventoryType, io->inventoryType()));
					query.execute(i->qty, i->armed ? DB_TRUE : DB_FALSE, playerId, io->m_itemData == 0 ? 0 : io->m_itemData->m_globalID, inventoryType, io->inventoryType());
					ulonglong rowsChanged = query.affected_rows();
					if (rowsChanged != 1) {
						LogError("Rows affected for inventory update <> 1 for playerId " << playerId << " global_id " << (io->m_itemData == 0 ? 0 : io->m_itemData->m_globalID) << " inv " << inventoryType << " subType " << io->inventoryType());
						throw new KEPException("Rows affected for inventory update <> 1", E_INVALIDARG);
					}
				}
			}

			if (!insertList.empty()) {
				Query query = m_conn->query();
				query.insert(insertList.begin(), insertList.end());
				query.execute();
			}
		}
	}
}

// save off the dynamically added GetSet values
void MySqlBase::updateGetSet(CPlayerObject *player, bool metaData) {
	gsValues *values = 0;
	gsMetaData *md = 0;

	player->GetValues(values, md);
	jsVerifyDo(values != 0 && md != 0, return );
	jsAssert(values->size() == md->size());

	Query query = m_conn->query();

	for (size_t i = md->size() - 1; i >= 0; i--) {
		ULONG flags = md->at(i).GetFlags();
		if (flags & GS_FLAG_DYNAMIC_ADD) {
			if ((flags & GS_FLAG_TRANSIENT) == 0) // persistent
			{
				query.reset();

				// do to race in MySQL delete was remove and on dup key added here
				// so you will always have a value once added
				query << "INSERT INTO `getset_player_values` (" << INS_GETSET_COLS << " )"
					  << " VALUES ("
					  << (metaData ? 0 : player->m_handle) << ","
					  << quote << md->at(i).GetDynamicName() << ","
					  << md->at(i).GetType() << ",";

				// insert into the approriate column
				switch (md->at(i).GetType()) {
					case gsInt:
						if (metaData)
							query << *(int *)*(int *)md->at(i).GetDynamicDefaultValue() << ",NULL,NULL)";
						else
							query << *(int *)values->at(i) << ",NULL,NULL)";
						query << " ON DUPLICATE KEY UPDATE int_value=" << *(int *)values->at(i);
						break;
					case gsDouble: {
						char s[100];
						sprintf_s(s, _countof(s), "%f", metaData ? *(double *)md->at(i).GetDynamicDefaultValue() : *(double *)values->at(i));
						if (metaData)
							query << "NULL," << s << ",NULL)";
						else
							query << "NULL," << s << ",NULL)";
						query << " ON DUPLICATE KEY UPDATE double_value=" << s;
					} break;
					case gsString:
						if (metaData)
							query << "NULL,NULL," << quote << ((const char *)md->at(i).GetDynamicDefaultValue()) << ")";
						else
							query << "NULL,NULL," << quote << ((std::string *)values->at(i))->c_str() << ")";
						query << " ON DUPLICATE KEY UPDATE string_value=" << quote << ((std::string *)values->at(i))->c_str();
						break;
					default:
						jsAssert(false);
						continue;
						break;
				}

				query.execute();
			}

		} else {
			// since dynamic adds always at end, stop when find non-dynamic
			break;
		}
	}
	player->GetSet::SetDirty(false);
}

void MySqlBase::updateNoteriety(int playerId, CNoterietyObj *noteriety) {
	Query query = m_conn->query();
	noterieties n(playerId, noteriety->m_currentMurders, noteriety->m_currentMinuteCountSinceLastMurder);
	query.replace(n); // will add or update
	query.execute();
}

void MySqlBase::updateDeformableMeshes(CPlayerObject *pPO) {
	if (pPO->m_handle != 0) {
		Query cleanup = m_conn->query("delete from `deformable_configs` where player_id = %0");
		cleanup.parse();
		LogTrace(cleanup.str((int)pPO->m_handle));
		cleanup.execute((int)pPO->m_handle);
	}

	Query query = m_conn->query();

	// update the deformable config
	int sequence = 1; // sequence at player level
	for (size_t i = 0; i < pPO->m_charConfig.getItemCount(); i++) {
		const CharConfigItem *pConfigItem = pPO->m_charConfig.getItemByIndex(i);
		assert(pConfigItem != nullptr);
		if (pConfigItem == nullptr) {
			continue;
		}

		for (size_t s = 0; s < pConfigItem->getSlotCount(); s++) {
			const CharConfigItemSlot *pSlot = pConfigItem->getSlot(s);
			assert(pSlot != nullptr);
			if (pSlot == nullptr) {
				continue;
			}

			deformable_configs row(
				sequence++,
				pPO->m_handle,
				(int)pConfigItem->getGlidOrBase(),
				pSlot->m_meshId,
				pSlot->m_matlId,
				pSlot->m_r,
				pSlot->m_g,
				pSlot->m_b);
			query.reset();
			query.insert(row);
			query.execute();
		}
	}
}

void MySqlBase::getOnlineFriendList(int playerServerId, std::vector<std::string> &broadcastListArray,
	int maxListSize /*= 25*/) // 3/08 added maxListItems to break up long lists
{
	if (!localPlayers())
		return; // TODO: do we need to notify local GStar user of logon/logoff

	START_TIMING(m_timingLogger, "DB:getOnlineFriendList");

	jsVerifyReturnVoid(playerServerId > 0 && maxListSize > 5);

	BEGIN_TRY_SQL();

	Query query = m_conn->query("CALL get_online_friends( %0 )");

	query.parse();

	StoreQueryResult res = query.store((int)playerServerId);

	// for SPs MUST call these to avoid disconnect
	checkMultipleResultSets(query, "get_online_friends");
	std::string broadcastList;
	for (UINT i = 0; i < res.size(); i++) {
		if (((i + 1) % maxListSize) == 0) {
			broadcastListArray.push_back(broadcastList);
			broadcastList.clear();
		}

		Row r = res[i];

		int playerId = r[0];
		std::string playerName = r[1];
		if (!broadcastList.empty())
			broadcastList += ",R";
		else
			broadcastList += "R";

		char s[24];
		_itoa_s(playerId, s, _countof(s), 10);
		broadcastList += s;
		broadcastList += ":";
		broadcastList += playerName;
	}
	if (!broadcastList.empty())
		broadcastListArray.push_back(broadcastList);

	END_TRY_SQL();
}

LONG MySqlBase::getPlayersCash(int kanevaId, CurrencyType currencyType) {
	LONG ret = 0;

	std::vector<cash_get> result;

	Query query = m_conn->query();

	query << "CALL get_user_balance( %0, %1 )";
	query.parse();

	query.storein(result, kanevaId, (int) currencyType);

	// for SPs MUST call these to avoid disconnect
	checkMultipleResultSets(query, "get_user_balance");

	if (!result.empty()) {
		ret = (LONG)std::min(LONG_MAX, (LONG)result.at(0).balance);
	}

	return ret;
}

void MySqlBase::savePlayerSpawnPoint(int playerId, ULONG zoneIndexCleared, ULONG instanceId, float x, float y, float z, float rx, float ry, float rz) {
	START_TIMING(m_timingLogger, "DB:savePlayerSpawnPoint");

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "INSERT INTO player_spawn_points(player_id, zone_index, zone_instance_id, position_x, position_y, position_z, direction_x, direction_y, direction_z ) "
			 "VALUES (%0, %1, %2, %3, %4, %5, %6, %7, %8)"
			 "ON DUPLICATE KEY UPDATE position_x=%3, position_y=%4, position_z=%5, direction_x=%6, direction_y=%7, direction_z=%8";
	query.parse();

	query.execute(playerId, (int)zoneIndexCleared, (int)instanceId, x, y, z, rx, ry, rz);

	END_TRY_SQL();
}

void MySqlBase::deletePlayerSpawnPoint(int playerId, ULONG zoneIndexCleared, ULONG instanceId) {
	START_TIMING(m_timingLogger, "DB:deletePlayerSpawnPoint");

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "DELETE FROM player_spawn_points WHERE player_id=%0 AND zone_index=%1 AND zone_instance_id=%2";
	query.parse();

	query.execute(playerId, (int)zoneIndexCleared, (int)instanceId);

	END_TRY_SQL();
}

bool MySqlBase::getPlayerSpawnPoint(int playerId, ULONG zoneIndexCleared, ULONG instanceId, float &x, float &y, float &z, float &rx, float &ry, float &rz) {
	START_TIMING(m_timingLogger, "DB:getPlayerSpawnPoint");

	bool ret = false;
	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "SELECT position_x, position_y, position_z, direction_x, direction_y, direction_z "
			 "FROM player_spawn_points WHERE player_id=%0 AND zone_index=%1 AND zone_instance_id=%2";
	query.parse();

	StoreQueryResult res = query.store(playerId, (int)zoneIndexCleared, (int)instanceId);
	if (res.num_rows() > 0) {
		x = res.at(0)[0];
		y = res.at(0)[1];
		z = res.at(0)[2];
		rx = res.at(0)[3];
		ry = res.at(0)[4];
		rz = res.at(0)[5];
		ret = true;
	}

	END_TRY_SQL();

	return ret;
}

std::string MySqlBase::getPlayerTitle(int playerId, int titleId) {
	START_TIMING(m_timingLogger, "DB:getPlayerTitle");
	std::string playerTitle = "";
	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "SELECT t.name FROM titles t, player_titles pt WHERE pt.title_id = %0 and t.title_id = %0 and pt.player_id = %1";
	query.parse();
	StoreQueryResult res = query.store(titleId, playerId);
	if (res.num_rows() > 0)
		playerTitle = (res.at(0))[0];

	END_TRY_SQL();

	return playerTitle;
}
bool MySqlBase::getParentZoneInfo(KGP_Connection *m_conn, unsigned zoneInstanceId, unsigned zoneType, unsigned &parentZoneInstanceId, unsigned &parentZoneIndex, unsigned &parentZoneType, unsigned &parentCommunityId) {
	assert(m_conn != nullptr);
	bool success = false;

	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << "SELECT uwip.zone_instance_id, uwip.zone_index, uwip.zone_type, uwip.community_id "
		  << "FROM unified_world_ids uwi "
		  << "INNER JOIN kaneva.communities c ON uwi.community_id = c.community_id "
		  << "INNER JOIN unified_world_ids uwip ON uwip.community_id = c.parent_community_id "
		  << "WHERE uwi.zone_instance_id = %0 AND uwi.zone_type = %1 AND c.status_id = 1 ";

	query.parse();
	StoreQueryResult queryResult = query.store(zoneInstanceId, zoneType);

	int num_rows = queryResult.num_rows();
	if (num_rows > 1) {
		assert(false);
		LogError("Error: Query returned more than one result");
		return false;
	}
	if (num_rows < 1) {
		parentZoneInstanceId = 0;
		parentZoneIndex = 0;
		parentZoneType = 0;
		parentCommunityId = 0;
	} else {
		parentZoneInstanceId = queryResult.at(0).at(0);
		parentZoneIndex = queryResult.at(0).at(1);
		parentZoneType = queryResult.at(0).at(2);
		parentCommunityId = queryResult.at(0).at(3);
	}

	success = true;
	END_TRY_SQL_LOG_ONLY(m_conn)

	assert(success);
	return success;
}

bool MySqlBase::getYouTubeSWFUrls(std::string &lastKnownGoodUrl, std::string &overrideUrl) {
	checkServerId("getYouTubeSWFUrls", true);
	assert(m_serverId != 0);

	// Continue anyway even if m_serverId is not assigned (will fallback to "any server")

	bool success = false;

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	// Up to three results:
	// 1) Last known good reported by any server (lowest priority),
	// 2) Last known good reported by current server,
	// 3) Administrator override (highest priority)

	query << "SELECT `option_value`, `option_key`='YouTubeSWFUrlOverride' is_override, `role`<>0 is_per_server "
			 "FROM `kaneva`.`web_options` "
			 "WHERE `option_group`='SERVER' "
			 "  AND (`option_key`='YouTubeSWFUrlOverride' OR `option_key`='YouTubeSWFUrl') "
			 "  AND (`role`=0 OR `role`=%0) "
			 "ORDER BY is_override, is_per_server";

	query.parse();

	StoreQueryResult res = query.store(m_serverId);

	assert(res.num_rows() <= 3);
	for (size_t i = 0; i < res.num_rows(); i++) {
		std::string url = res.at(i).at(0);
		bool isOverride = res.at(i).at(1);
		if (isOverride) {
			overrideUrl = url;
		} else {
			lastKnownGoodUrl = url;
		}
		success = true;
	}

	END_TRY_SQL();

	return success;
}

void MySqlBase::updateYouTubeSWFLastKnownGoodUrl(const std::string &url) {
	checkServerId("updateYouTubeSWFLastKnownGoodUrl", true);
	assert(m_serverId != 0);

	// Continue anyway even if m_serverId is not assigned (will fallback to "any server")

	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "INSERT INTO `kaneva`.`web_options`(`option_group`, `option_key`, `role`, `option_value`) "
			 "VALUES ('SERVER', 'YouTubeSWFUrl', %0, %1q), ('SERVER', 'YouTubeSWFUrl', 0, %1q) "
			 "ON DUPLICATE KEY UPDATE `option_value` = VALUES(`option_value`)";

	query.parse();
	query.execute(m_serverId, url.c_str());

	END_TRY_SQL();
}

// Always call this function from a back-end thread
bool MySqlBase::parseYouTubeSWFUrlFromWeb(std::string &url) {
	WebCallAct wca(WebCallOpt("https://www.youtube.com/embed/7FNk9zMFzW8?nohtml5=1", true)); // Kaneva a worldly vision

	// Blocking HTTP call
	if (!wca.Call()) {
		assert(false);
		LogWarn("Webcall failed: " << wca.wco.url);
		return false;
	}

	if (wca.statusCode != 200 || wca.ResponseSize() == 0) {
		assert(false);
		LogWarn("Webcall failed: " << wca.wco.url << " - HTTP " << wca.StatusCode());
		return false;
	}

	// Make a copy of the content
	const char *response = wca.ResponseChars();
	assert(response != nullptr);

	const char *SET_CONFIG_TOKEN1 = "yt.setConfig({'PLAYER_CONFIG'";
	const char *SET_CONFIG_TOKEN2 = "yt.setConfig({\"PLAYER_CONFIG\"";
	const char *URL_KEY_TOKEN1 = "\"url\":";
	const char *URL_KEY_TOKEN2 = "\'url\':";
	const char *URL_HTTP_TOKEN1 = "\"http";
	const char *URL_HTTP_TOKEN2 = "'http";
	const char *L_CURLY_TOKEN = "{";
	const char *R_CURLY_TOKEN = "});";

	const char *pStrSetConfig = strstr(response, SET_CONFIG_TOKEN1);
	if (pStrSetConfig == nullptr) {
		pStrSetConfig = strstr(response, SET_CONFIG_TOKEN2);
	}
	if (pStrSetConfig == nullptr) {
		assert(false);
		LogWarn("Error parsing YouTube page: PLAYER_CONFIG not found");
		return false;
	}

	const char *pStrLeftCurly = strstr(pStrSetConfig, L_CURLY_TOKEN);
	if (pStrLeftCurly == nullptr) {
		assert(false);
		LogWarn("Error parsing YouTube page: PLAYER_CONFIG { not found");
		return false;
	}

	const char *pStrRightCurly = strstr(pStrLeftCurly, R_CURLY_TOKEN);
	if (pStrRightCurly == nullptr) {
		assert(false);
		LogWarn("Error parsing YouTube page: PLAYER_CONFIG } not found");
		return false;
	}

	// "url":
	const char *pStrUrlKey = strstr(pStrLeftCurly, URL_KEY_TOKEN1);
	if (pStrUrlKey == nullptr || pStrUrlKey > pStrRightCurly) {
		pStrUrlKey = strstr(pStrLeftCurly, URL_KEY_TOKEN2);
	}

	if (pStrUrlKey == nullptr || pStrUrlKey > pStrRightCurly) {
		assert(false);
		LogWarn("Error parsing YouTube page: \"url:\" not found");
		return false;
	}

	// http...
	const char *pStrUrlVal = strstr(pStrUrlKey, URL_HTTP_TOKEN1);
	if (pStrUrlVal == nullptr || pStrUrlVal > pStrRightCurly) {
		pStrUrlVal = strstr(pStrUrlKey, URL_HTTP_TOKEN2);
	}
	if (pStrUrlVal == nullptr || pStrUrlVal > pStrRightCurly) {
		assert(false);
		LogWarn("Error parsing YouTube page: invalid url property");
		return false;
	}

	char urlValEndQuoteChar = *pStrUrlVal; // " or ' before http
	pStrUrlVal++; // Skip quote char

	const char *pStrUrlValEnd = strchr(pStrUrlVal, urlValEndQuoteChar);
	if (pStrUrlValEnd == nullptr || pStrUrlValEnd > pStrRightCurly) {
		assert(false);
		LogWarn("Error parsing YouTube page: unterminated url property");
		return false;
	}

	// https:\/\/s.ytimg.com\/yts\/swfbin\/player-vfl77mD0u\/watch_as3.swf
	url.resize(pStrUrlValEnd - pStrUrlVal);
	std::copy(pStrUrlVal, pStrUrlValEnd, url.begin());
	LogDebug("RAW url: [" << url << "]");

	// Unescape
	STLStringSubstitute(url, "\\/", "/");
	LogInfo("YouTube SWF player URL: [" << url << "]");
	return true;
}

void MySqlBase::playerInvUpdateCommitted(CPlayerObject *pPO) {
	// whenever a player is committed, clear out the dirty flags on inv
	if (pPO->m_inventory) {
		pPO->m_inventory->ClearDirty();
	}

	// bank inv one m_playerOrigRef, so we need to check
	// if we not the original ref
	if (pPO->playerOrigRef() != 0) {
		if (pPO->playerOrigRef()->m_bankInventory) {
			pPO->playerOrigRef()->m_bankInventory->ClearDirty();
		} else {
			LogWarn("Null bank inventory on original ref for player:  " << pPO->m_charName);
		}
	} else {
		if (pPO->m_bankInventory) {
			pPO->m_bankInventory->ClearDirty();
		} else {
			LogWarn("Null bank inventory for player:  " << pPO->m_charName);
		}
	}
}

EVENT_PROC_RC MySqlBase::getPlayerInfo(IEvent *e) {
	GetResultSetsEvent *grse = dynamic_cast<GetResultSetsEvent *>(e);
	jsVerifyReturn(grse != 0, EVENT_PROC_RC::NOT_PROCESSED);
	jsVerifyReturn(grse->Filter() == (LONG)GetResultSetsEvent::PLAYER || grse->Filter() == (LONG)GetResultSetsEvent::ACCOUNT, EVENT_PROC_RC::NOT_PROCESSED);

	jsVerifyReturn(checkServerId("GetItemForegroundHandler", true), EVENT_PROC_RC::NOT_PROCESSED);

	if (grse->ret != 0)
		return EVENT_PROC_RC::OK;

	if (!grse->resultXml.empty()) {
		if (grse->Filter() == GetResultSetsEvent::ACCOUNT) {
			GetAccountEvent *gae = gae = dynamic_cast<GetAccountEvent *>(e);
			jsAssert(gae != nullptr);
			CAccountObject *acct = dynamic_cast<CAccountObject *>(gae->item);
			if (acct == 0) {
				gae->ret = E_INVALIDARG;
				LogWarn("Bad account object in GetItemForegroundHandler " << gae->name);
			}

			acct->m_serverId = gae->kanevaUserId;

			TiXmlDocument xmlDoc;
			try {
				xmlDoc.Parse(grse->resultXml.c_str());
			} catch (...) {
				// a malloc is failing inside the tinyxml parsing function
				gae->ret = E_INVALIDARG;
				LogWarn("Error parsing XML for  " << gae->name);
			}
			if (xmlDoc.Error()) {
				gae->ret = E_INVALIDARG;
				LogWarn("Error loading XML for " << gae->name << " " << xmlDoc.ErrorDesc());
			} else if (!xmlFillInAccount(&xmlDoc, acct)) // fill in the account and player from the XML
			{
				gae->ret = E_INVALIDARG;
				LogWarn("Error loading XML for  " << gae->name);
			}
		}
	}

	if (!localPlayers()) {
		GetAccountEvent *gae = dynamic_cast<GetAccountEvent *>(e);
		jsAssert(gae != nullptr);
		CAccountObject *acct = dynamic_cast<CAccountObject *>(gae->item);
		if (acct == 0) {
			gae->ret = E_INVALIDARG;
			LogWarn("Bad account object in GetItemForegroundHandler " << gae->name);
		} else {
			// just added the player in xmlFillInAccount
			CPlayerObject *player = dynamic_cast<CPlayerObject *>(acct->m_pPlayerObjectList->GetTail());
			if (player->m_skills == NULL) {
				if (!getPlayerSkills(player->m_handle, player->m_skills)) {
					player->m_skills = new CSkillObjectList();
					CSkillObject *o = new CSkillObject();
					player->m_skills->AddTail(o);
				}
			}

			if (player->m_titleList == NULL) {
				get3DAppPlayerTitles(player->m_handle, player->m_titleList, player->m_title);
			}
		}
	}

	if (grse->results != 0 && grse->arraySize) {
		if (grse->Filter() == GetResultSetsEvent::PLAYER) {
			GetPlayerEvent *gpe = dynamic_cast<GetPlayerEvent *>(e);
			jsAssert(gpe != nullptr);
			try {
				CPlayerObject *player = new CPlayerObject();
				gpe->item = player;
				player->m_handle = gpe->ObjectId();
				fillInPlayer(player, gpe->results, gpe->arraySize, e->From(), (void **)&(gpe->pPlayerInventory), (void **)&(gpe->pBankInventory));

			} catch (KEPException *e) {
				grse->ret = E_INVALIDARG;
				// probably only can be BadIndex from MySQL++
				LogWarn("Error filling in player: " << grse->ObjectId() << " " << e->ToString());
				e->Delete();
			}
		} else if (grse->Filter() == GetResultSetsEvent::ACCOUNT) {
			try {
				GetAccountEvent *gae = dynamic_cast<GetAccountEvent *>(e);
				jsAssert(gae != nullptr);
				CAccountObject *acct = dynamic_cast<CAccountObject *>(gae->item);
				if (acct == 0) {
					gae->ret = E_INVALIDARG;
					LogWarn("Bad account object in GetItemForegroundHandler " << gae->name);
				}
				acct->m_serverId = gae->kanevaUserId;
				if (!fillInAccount(acct, gae->results, gae->arraySize, e->From()))
					gae->ret = 2;
			} catch (KEPException *e) {
				grse->ret = E_INVALIDARG;
				// probably only can be BadIndex from MySQL++
				LogWarn("Error filling in player: " << grse->ObjectId() << " " << e->ToString());
				e->Delete();
			}
		}
	}
	return EVENT_PROC_RC::OK;
}

/* added to check for our server_id from the pinger
	since it's a bad thing to hit db without our
	valid server_id

	this is only called on server change state and
	we a user logs in.  otherwise the code is unreachable
	since log ins are blocked.
*/
bool MySqlBase::checkServerId(const char *desc, bool logMsg) {
	m_serverId = 1;
	if (m_serverId == 0) {
		if (multiplayerObj()->m_serverNetwork && multiplayerObj()->m_serverNetwork->pinger().GetServerId() != 0) {
			m_serverId = multiplayerObj()->m_serverNetwork->pinger().GetServerId();
			LogInfo("Database got server id from pinger of " << m_serverId);

			// new sanity check on the dang view if we don't get our server back, view is probably incorrectly
			// configured with wrong gameId
			BEGIN_TRY_SQL();
			Query query = m_conn->query();
			query << "select * from vw_game_servers where server_id = %0";
			query.parse();
			StoreQueryResult res = query.store(m_serverId);
			if (res.num_rows() == 0) {
				LogWarn("select * from vw_game_servers where server_id = " << m_serverId << "; return no rows.  CHECK VIEW CONFIGURATION.");
				LogWarn("select * from vw_game_servers where server_id = " << m_serverId << "; return no rows.  CHECK VIEW CONFIGURATION.");
				LogWarn("select * from vw_game_servers where server_id = " << m_serverId << "; return no rows.  CHECK VIEW CONFIGURATION.");
				MessageBeep(-1);
				Sleep(100);
				MessageBeep(-1);
				Sleep(100);
				MessageBeep(-1);
			}
			END_TRY_SQL();

		} else if (logMsg)
			LogWarn("Server id is zero for database call.  Not executing: " << desc);
	}

	return m_serverId != 0;
}

// for fillInAccount
#define ACCOUNT_RS 0
#define PLAYERS_ON_ACCT_RS 1
#define NEWACCT_RS 0

bool MySqlBase::fillInAccount(CAccountObject *acct, StoreQueryResult *results, int resultCount, NETID from) {
	if (!checkServerId("GetAccount", true))
		return false;

	if (resultCount < 1 || results[ACCOUNT_RS].size() != 1) {
		LogWarn("fillInAccount got incorrect number of result sets " << resultCount);
		jsVerifyReturn(false, false);
	}

	BEGIN_TRY_SQL();

	if (results[ACCOUNT_RS].size() == 1) {
		// found the acct in the db
		Row row = results[ACCOUNT_RS].at(0);

		if ((LONG)row.at(RET_COL) == USER_LOGGED_ON) {
			LogInfo(loadStrPrintf(IDS_ALREADY_LOGGED_ON, acct->m_accountName, numToString((int)row.at(1), "%d").c_str()));
			return false;
		} else if ((LONG)row.at(RET_COL) == USER_EXISTS) {
			if (localPlayers() && resultCount <= PLAYERS_ON_ACCT_RS) {
				LogWarn("fillInAccount got incorrect number of result sets for players " << resultCount);
				jsVerifyReturn(false, false);
			}

			// update the data from the db
			acct->m_accountNumber = row.at(USER_ID_COL);

			// Select the higher of the two permissions between the developer website and game database
			acct->m_gm = acct->m_gm || strcmp(row.at(IS_GM_COL), DB_TRUE) == 0;
			acct->m_canSpawn = acct->m_canSpawn || strcmp(row.at(CAN_SPAWN_COL), DB_TRUE) == 0;
			acct->m_administratorAccess = acct->m_administratorAccess || strcmp(row.at(IS_ADMIN_COL), DB_TRUE) == 0;

			acct->m_timePlayedMinutes = row.at(TIME_PLAYED_COL);
			acct->m_age = row.at(AGE_COL);
			acct->m_showMature = ((long)row.at(SHOW_MATURE_COL)) != 0;

			if (localPlayers()) {
				int matches = 0;
				if (acct->m_pPlayerObjectList == 0)
					acct->m_pPlayerObjectList = new CPlayerObjectList();

				int curChars = acct->m_pPlayerObjectList->GetCount();
				for (size_t i = 0; i < results[PLAYERS_ON_ACCT_RS].size(); i++) {
					bool found = false;
					for (POSITION pos = acct->m_pPlayerObjectList->GetHeadPosition(); pos != 0 && !found;) {
						CPlayerObject *player = (CPlayerObject *)acct->m_pPlayerObjectList->GetNext(pos);
						if (player->m_handle == (LONG)results[PLAYERS_ON_ACCT_RS].at(i).at(PLAYER_ID_COL)) {
							matches++;
							found = true;
						}
					}
					if (!found) // add it to the list so they can login as them
					{
						CPlayerObject *newPlayer = new CPlayerObject();
						newPlayer->m_handle = results[PLAYERS_ON_ACCT_RS].at(i).at(PLAYER_ID_COL);
						newPlayer->m_charName = (const char *)results[PLAYERS_ON_ACCT_RS].at(i).at(NAME_COL);
						newPlayer->m_originalEDBCfg = results[PLAYERS_ON_ACCT_RS].at(i).at(EDB_COL);
						acct->m_pPlayerObjectList->AddTail(newPlayer);
					}
				}

				if (matches != curChars) {
					// local only players, remove them
					POSITION delPos;
					for (POSITION pos = acct->m_pPlayerObjectList->GetHeadPosition(); (delPos = pos) != 0;) {
						CPlayerObject *player = (CPlayerObject *)acct->m_pPlayerObjectList->GetNext(pos);

						bool found = false;
						for (size_t i = 0; i < results[PLAYERS_ON_ACCT_RS].size(); i++) {
							if (player->m_handle == (LONG)results[PLAYERS_ON_ACCT_RS].at(i).at(PLAYER_ID_COL)) {
								found = true;
								break;
							}
						}

						if (!found) {
							LogInfo(loadStr(IDS_REMOVING_PLAYERS) << acct->m_accountName << ":" << player->m_charName);
							player->SafeDelete();
							delete player;
							acct->m_pPlayerObjectList->RemoveAt(delPos);
						}
					}
				}
			}
		} else if ((LONG)row.at(RET_COL) == USER_NEW) {
			if (resultCount <= NEWACCT_RS) {
				LogWarn("fillInAccount got incorrect number of result sets for new account " << resultCount);
				jsVerifyReturn(false, false);
			}

			// add them, they shouldn't have any logged on users
			jsAssert(acct->m_pPlayerObjectList == 0 ||
					 acct->m_pPlayerObjectList->GetCount() == 0);

			if (acct->m_pPlayerObjectList == 0)
				acct->m_pPlayerObjectList = new CPlayerObjectList();

			if (acct->m_pPlayerObjectList->GetCount() > 0) {
				// remove them all!
				LogInfo(loadStr(IDS_REMOVING_PLAYERS) << acct->m_accountName);
				acct->m_pPlayerObjectList->SafeDelete();
			}

			acct->m_accountNumber = (int)(row.at(USER_ID_COL));
		} else {
			LogInfo("Got unknown return code from get_account_data: " << (LONG)row.at(0));
		}
	}

	END_TRY_SQL();

	return true;
}

bool MySqlBase::createClan(CPlayerObject *pPO, const char *clanName, ULONG &clanId) {
	START_TIMING(m_timingLogger, "DB:CreateClan");

	bool ret = false;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();

	query << "INSERT INTO clans ( owner_id, clan_name ) VALUES (%0, %1q)";
	query.parse();

	NoExceptions ne(query);

	SimpleResult res = query.execute((int)pPO->m_handle, clanName);

	int sqlErr = m_conn->errnum();
	if (sqlErr == ER_DUP_ENTRY) {
		ret = false;
	} else if (sqlErr == 0) {
		// ok
		clanId = (ULONG)res.insert_id();

		addMemberToClan(clanId, pPO->m_handle);

		ret = true;

		trans.commit();
	} else {
		throw BadQuery(m_conn->error());
	}

	END_TRY_SQL();

	return ret;
}

bool MySqlBase::deleteClan(int ownerId) {
	START_TIMING(m_timingLogger, "DB:DeleteClan");

	bool ret = false;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();

	query << "DELETE FROM clans where owner_id = %0";
	query.parse();

	LogTrace(query.str((int)ownerId));
	SimpleResult res = query.execute((int)ownerId);

	ret = true;

	trans.commit();

	END_TRY_SQL();

	return ret;
}

bool MySqlBase::addMemberToClan(ULONG clanId, ULONG playerId) {
	START_TIMING(m_timingLogger, "DB:AddMemberToClan");

	bool ret = false;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();

	query << "INSERT INTO clan_members (clan_id, player_id) VALUES ( %0, %1 ) ON DUPLICATE KEY UPDATE clan_id = %2";
	query.parse();

	LogTrace(query.str((int)clanId, (int)playerId, (int)clanId));
	SimpleResult res = query.execute((int)clanId, (int)playerId, (int)clanId);

	ret = true;

	trans.commit();

	END_TRY_SQL();

	return ret;
}

bool MySqlBase::removeMemberFromClan(ULONG clanId, ULONG playerId) {
	START_TIMING(m_timingLogger, "DB:RemoveMemberFromClan");

	bool ret = true;

	BEGIN_TRY_SQL();

	Transaction trans(*m_conn);

	Query query = m_conn->query();

	query << "DELETE FROM clan_members where clan_id = %0 and player_id = %1";
	query.parse();

	LogTrace(query.str((int)clanId, (int)playerId));
	SimpleResult res = query.execute((int)clanId, (int)playerId);

	ret = true;

	trans.commit();

	END_TRY_SQL();

	return ret;
}

void MySqlBase::setClan(CPlayerObject *player, const char *clanName, int ownerHandle) {
	// already have one in memory?
	PLAYER_HANDLE oldClanHandle = 0;
	CStringA oldClanName;
	CClanSystemObj *oldClan = 0;
	if (multiplayerObj()->m_clanDatabase->GetPlayersClan(player->m_handle, oldClanHandle, oldClanName))
		oldClan = multiplayerObj()->m_clanDatabase->GetClanByHandle(oldClanHandle);

	if (clanName != 0) {
		//A clan was found for me in the database
		player->m_clanName = clanName;
		player->m_clanHandle = ownerHandle;

		//also make sure I dont own clan
		if (player->m_handle != player->m_clanHandle) {
			if (oldClan == 0 || player->m_clanHandle != oldClan->m_guildMasterHandle) {
				// I dont own clan
				if (oldClan) {
					//Remove me from the clan in case it is stale
					oldClan->RemoveMemberByHandle(player->m_handle);
				}

				CClanSystemObj *newClan = multiplayerObj()->m_clanDatabase->GetClanByHandle(player->m_clanHandle);

				if (!newClan) {
					//Create the clan if its not here
					multiplayerObj()->m_clanDatabase->CreateClan(player->m_clanName, player->m_clanName, player->m_clanHandle);
					newClan = multiplayerObj()->m_clanDatabase->GetClanByHandle(player->m_clanHandle);
				}

				//Clan now on server, add me to it
				if (newClan)
					newClan->AddMemberByHandle(player->m_handle, player->m_charName);
			}

		} else {
			// I own the clan
			//Owner has clan in database
			if (oldClan) {
				//A clan for owner found on server

				if (strcmp(oldClan->m_clanName, player->m_clanName) != 0) {
					//The clan in the database and server are not the same.  Since we own and it shouldn't exist, destroy clan (and members)
					//and create the new correct clan

					multiplayerObj()->m_clanDatabase->DeleteClanByHandle(player->m_handle);
					multiplayerObj()->m_clanDatabase->CreateClan(player->m_clanName, player->m_clanName, player->m_handle);
				}
			} else {
				//No clan found on the server, so create it, with me as owner

				multiplayerObj()->m_clanDatabase->CreateClan(player->m_clanName, player->m_clanName, player->m_handle);
			}
		}
	} else {
		// not in a clan in DB, *was* I last time on this server?
		if (oldClan) {
			if (oldClanHandle == player->m_handle) // i was owner, need to disaband it
			{
				if (multiplayerObj()->m_clanDatabase->DeleteClanByHandle(player->m_handle)) {
					deleteClan(player->m_handle);
				}

			} else {
				// just remove me
				//Remove me from the clan in case it is stale
				oldClan->RemoveMemberByHandle(player->m_handle);
			}
		}
	}
}

// helper to fill in the player object
static void populatePlayer(const Row &result, const Row &presenceResult, CPlayerObject *pPO, const char*& sqlFunctionName) {
	int respawnZoneInstance = 0;

	// cash amounts loaded later from Kaneva DB, initialize here
	if (pPO->m_cash == 0)
		pPO->m_cash = new CCurrencyObj();
	if (pPO->m_bankCash == 0)
		pPO->m_bankCash = new CCurrencyObj();
	if (pPO->m_giftCredits == 0)
		pPO->m_giftCredits = new CCurrencyObj();

	pPO->m_bankCash->m_amount = 0;
	pPO->m_cash->m_amount = 0;
	pPO->m_giftCredits->m_amount = 0;

	sqlFunctionName = "GetPlayer/populatePlayer/player";

	// account Id? if acct on server, should match
	pPO->m_charName = (const char *)result.at(0);
	pPO->m_title = (const char *)result.at(1);
	pPO->m_bornSpawnCfgIndex = result.at(2);
	pPO->m_originalEDBCfg = pPO->m_curEDBIndex = pPO->m_dbCfg = result.at(3);
	pPO->m_clanHandle = NO_CLAN; // no longer on player record result.at(6);

	sqlFunctionName = "GetPlayer/populatePlayer/presence";

	Vector3f dir = Vector3f(presenceResult.at(0), presenceResult.at(1), presenceResult.at(2));
	Vector3f pos = Vector3f(presenceResult.at(3), presenceResult.at(4), presenceResult.at(5));

#if DRF_OLD_SERVER_INTERP
	pPO->m_dirVector = dir;
	pPO->m_3dPosition = pos;
#else
	MovementData md = pPO->InterpolatedMovementData();
	md.pos = pos;
	md.dir = dir;
	pPO->MovementInterpolatorInit(md);
#endif

	pPO->m_housingZoneIndex.fromLong(presenceResult.at(8));
	pPO->m_respawnZoneIndex.fromLong(presenceResult.at(9));
	respawnZoneInstance = presenceResult.at(10);
	pPO->m_respawnPosition.x = presenceResult.at(11);
	pPO->m_respawnPosition.y = presenceResult.at(12);
	pPO->m_respawnPosition.z = presenceResult.at(13);
	pPO->m_scaleFactor.x = presenceResult.at(14);
	pPO->m_scaleFactor.y = presenceResult.at(15);
	pPO->m_scaleFactor.z = presenceResult.at(16);

	pPO->m_scratchScaleFactor = pPO->m_scaleFactor; // copy to scratch

	// as a safety measure set the instance id
	pPO->m_housingZoneIndex.setAsHousing(pPO->m_handle);

	// fix up respawnZone if have > 16 bit instance
	// note that we can always figure out housing later
	// 10/07 note, if housing, set to themselves
	if (respawnZoneInstance != 0)
		pPO->m_respawnZoneIndex.setInstanceId(respawnZoneInstance);
	else if (pPO->m_respawnZoneIndex.isHousing()) {
		//10/30 need to get correct zone index (map) for respawn or else
		//we will spawn to starter home (curreently studio apt zone index == 5)
		pPO->m_respawnZoneIndex = pPO->m_housingZoneIndex;
		pPO->m_respawnZoneIndex.setInstanceId(pPO->m_handle);
	}
}

void MySqlBase::fillInPlayer(CPlayerObject *pPO, StoreQueryResult *results, int resultsCount, NETID from, void **playerInventory, void **bankInventory) {
	if (resultsCount < PLAYER_RESULTS) {
		LogWarn("Got incorrect number of result sets " << resultsCount << " need at least " << PLAYER_RESULTS);
		jsAssert(false);
		throw new KEPException("fillInPlayer got incorrect number of result sets", E_INVALIDARG);
	}

	BEGIN_TRY_SQL();

	if (results[PLAYERS_RS].num_rows() != 1) {
		std::string s = loadStr(IDS_PLAYER_NOT_FOUND_IN_DB);
		s += " players";
		LogWarn(s << pPO->m_handle);
		throw new KEPException(s, E_INVALIDARG);
	}

	SQL_TASK("GetPlayer/player_presences");

	if (results[PLAYERS_PRESENCES_RS].num_rows() != 1) {
		std::string s = loadStr(IDS_PLAYER_NOT_FOUND_IN_DB);
		s += " player_presences";
		LogWarn(s << pPO->m_handle);
		throw new KEPException(s, E_INVALIDARG);
	}

	SQL_TASK("GetPlayer/populatePlayer");

	// populate the player from the result, some fields overwritten later, e.g. m_giftCredits
	populatePlayer(results[PLAYERS_RS].at(0), results[PLAYERS_PRESENCES_RS].at(0), pPO, SQL_TASK_REF);

	// see if have a current zone row
	SQL_TASK("GetPlayer/player_zones");

	if (results[ZONES_RS].num_rows() == 1) {
		int currentZoneInstance = 0;

		// must not have logged out, set current zone
		Row row = results[ZONES_RS].at(0);
		pPO->m_currentZoneIndex.fromLong(row.at(0));
		currentZoneInstance = row.at(1);
		pPO->m_currentWorldMap = (const char *)row.at(2);

		// fix up currentZone, respawnZone if have > 16 bit instance
		// note that we can always figure out housing later
		pPO->m_currentZoneIndex.setInstanceId(currentZoneInstance);
	}

	// get the inventories and update them
	SQL_TASK("GetPlayer/inventories");

	if (pPO->m_inventory) {
		pPO->m_inventory->SafeDelete();
		delete pPO->m_inventory;
	}

	if (pPO->m_bankInventory) {
		pPO->m_bankInventory->SafeDelete();
		delete pPO->m_bankInventory;
	}

	pPO->m_inventory = (CInventoryObjList *)(*playerInventory);
	pPO->m_bankInventory = (CInventoryObjList *)(*bankInventory);

	// get deformables
	SQL_TASK("GetPlayer/deformable_configs");
	pPO->m_charConfig.disposeAllItems();
	pPO->m_baseMeshes = results[DEFCFG_RS].size();
	if (results[DEFCFG_RS].num_rows() > 0) {
		// first get counts of each type/equip
		GLID_OR_BASE glidOrBase = (GLID_OR_BASE)results[DEFCFG_RS].at(0).at(DM_EQUIP_ID);
		BYTE slotCount = 0;
		for (UINT i = 0; i < results[DEFCFG_RS].num_rows(); i++) {
			Row r = results[DEFCFG_RS].at(i);
			if (glidOrBase != (int)r.at(DM_EQUIP_ID)) {
				pPO->m_charConfig.addItem(glidOrBase, slotCount);
				slotCount = 0;
				glidOrBase = (int)r.at(DM_EQUIP_ID);
			}
			slotCount++;
		}
		pPO->m_charConfig.addItem(glidOrBase, slotCount); // do last one (or first if only one)

		size_t itemSlot = 0;
		int itemEquip = CC_ITEM_INVALID;
		CharConfigItem *pConfigItem = NULL;
		for (UINT i = 0; i < results[DEFCFG_RS].num_rows(); i++) {
			Row r = results[DEFCFG_RS].at(i);
			if (itemEquip != (int)r.at(DM_EQUIP_ID)) {
				itemSlot = 0;
				itemEquip = (int)r.at(DM_EQUIP_ID);
				pConfigItem = pPO->m_charConfig.getItemByGlidOrBase(itemEquip);
				if (!pConfigItem)
					continue;
			}

			if (pConfigItem && itemSlot < pConfigItem->getSlotCount()) {
				CharConfigItemSlot *pSlot = pConfigItem->getSlot(itemSlot);
				assert(pSlot != nullptr);
				pSlot->m_meshId = r.at(DM_DEFCFG);
				pSlot->m_matlId = r.at(DM_MAT);
				pSlot->m_r = r.at(DM_COLOR_R);
				pSlot->m_g = r.at(DM_COLOR_G);
				pSlot->m_b = r.at(DM_COLOR_B);
			}

			itemSlot++;
		}
	}

	// get the player's cash
	SQL_TASK("GetPlayer/cash");

	if (results[CASH_RS].num_rows() > 0)
		pPO->m_cash->m_amount = results[CASH_RS].at(0).at(0);
	else
		pPO->m_cash->m_amount = 0;

	if (results[GIFT_RS].num_rows() > 0)
		pPO->m_giftCredits->m_amount = results[GIFT_RS].at(0).at(0);
	else
		pPO->m_giftCredits->m_amount = 0;

	if (results[BANK_RS].num_rows() > 0)
		pPO->m_bankCash->m_amount = results[BANK_RS].at(0).at(0);
	else
		pPO->m_bankCash->m_amount = 0;

	// get the skills
	SQL_TASK("GetPlayer/skills");
	if (pPO->m_skills)
		DELETE_AND_ZERO(pPO->m_skills);
	pPO->m_skills = new CSkillObjectList();
	for (UINT i = 0; i < results[SKILL_RS].num_rows(); i++) {
		Row r = results[SKILL_RS].at(i);
		CSkillObject *o = new CSkillObject();
		o->m_skillId = r.at(0);
		o->m_skillName = r.at(1);
		o->m_skillType = r.at(2);
		o->m_gainIncrement = r.at(3);
		o->m_basicAbilitySkill = r.at(4) != DB_FALSE;
		o->m_basicAbilityExpMastered = r.at(5);
		o->m_basicAbilityFunction = r.at(6);
		o->m_skillGainsWhenFailed = r.at(7);
		o->m_maxMultiplier = r.at(8);
		o->m_useInternalTimer = r.at(9) != DB_FALSE;
		o->m_filterEvalLevel = r.at(10) != DB_FALSE;
		o->m_currentLevel = r.at(11);
		o->m_currentExperience = r.at(12);
		pPO->m_skills->AddTail(o);
	}

	// get the stats
	SQL_TASK("GetPlayer/stats");
	if (pPO->m_stats)
		DELETE_AND_ZERO(pPO->m_stats);
	pPO->m_stats = new CStatObjList();
	for (UINT i = 0; i < results[STAT_RS].num_rows(); i++) {
		Row r = results[STAT_RS].at(i);
		CStatObj *o = new CStatObj();
		o->m_id = r.at(0);
		o->m_statName = r.at(1);
		o->m_capValue = r.at(2);
		o->m_gainBasis = r.at(3);
		o->m_benefitType = r.at(4);
		o->m_bonusBasis = r.at(5);
		o->m_currentValue = r.at(6);
		pPO->m_stats->AddTail(o);
	}

	// get the additional GetSet stuff
	SQL_TASK("GetPlayer/getSet");

	getGetSet(pPO, results[GETSET_RS]);

	// get noteriety
	SQL_TASK("GetPlayer/noteriety");
	if (pPO->m_noteriety) {
		pPO->m_noteriety->SafeDelete();
		DELETE_AND_ZERO(pPO->m_noteriety);
	}
	pPO->m_noteriety = new CNoterietyObj();

	if (results[NOTERIETY_RS].size() == 1) {
		pPO->m_noteriety->m_currentMurders = results[NOTERIETY_RS].at(0).at(0);
		pPO->m_noteriety->m_currentMinuteCountSinceLastMurder = results[NOTERIETY_RS].at(0).at(1);
	}

	SQL_TASK("Clan");

	// check to see if they were removed from clan on another server, and we need
	// to update this one
	if (results[CLAN_RS].size() > 0) {
		setClan(pPO, results[CLAN_RS].at(0).at(0).c_str(), results[CLAN_RS].at(0).at(1));
	} else {
		setClan(pPO, 0, 0);
	}

	SQL_TASK("PassGroups");

	loadPassList(pPO, results[PASSLIST_RS]);

	//Load name color for player
	if (results[NAME_COLOR].size() == 1) {
		pPO->m_nameColor.r = results[NAME_COLOR].at(0).at(0);
		pPO->m_nameColor.g = results[NAME_COLOR].at(0).at(1);
		pPO->m_nameColor.b = results[NAME_COLOR].at(0).at(2);
	}

	// get the quests
	SQL_TASK("GetPlayer/quests");
	pPO->m_questJournal = new CQuestJournalList();
	// TODO: get quests

	/* mbb 06/17/08 -
		    titles that were earned before the user logged in will no longer be loaded into the server
		    only titles that are earned while in game will be stored within the player's title list and when
		    the player logs out, those new titles will be added to the db
		*/
	// reset the player's titles
	SQL_TASK("GetPlayer/titles");
	pPO->m_titleList = new CTitleObjectList();

	END_TRY_SQL();

	if (pPO) {
		LogInfo("Loaded Player - " << pPO->ToStr());
		playerInvUpdateCommitted(pPO);
	}
}

void MySqlBase::fillInPlayer(CPlayerObject *player, StoreQueryResult *results, int resultsCount, NETID from) {
	if (resultsCount < PLAYER_RESULTS) {
		LogWarn("fillInPlayer got incorrect number of result sets " << resultsCount << " need at least " << PLAYER_RESULTS);
		jsAssert(false);
		throw new KEPException("fillInPlayer got incorrect number of result sets", E_INVALIDARG);
	}

	BEGIN_TRY_SQL();

	if (results[PLAYERS_RS].num_rows() != 1) {
		std::string s = loadStr(IDS_PLAYER_NOT_FOUND_IN_DB);
		s += " players";
		LogWarn(s << player->m_handle);
		throw new KEPException(s, E_INVALIDARG);
	}

	SQL_TASK("GetPlayer/player_presences");

	if (results[PLAYERS_PRESENCES_RS].num_rows() != 1) {
		std::string s = loadStr(IDS_PLAYER_NOT_FOUND_IN_DB);
		s += " player_presences";
		LogWarn(s << player->m_handle);
		throw new KEPException(s, E_INVALIDARG);
	}

	SQL_TASK("GetPlayer/populatePlayer");

	// populate the player from the result, some fields overwritten later, e.g. m_giftCredits
	populatePlayer(results[PLAYERS_RS].at(0), results[PLAYERS_PRESENCES_RS].at(0), player, SQL_TASK_REF);

	// see if have a current zone row
	SQL_TASK("GetPlayer/player_zones");

	if (results[ZONES_RS].num_rows() == 1) {
		int currentZoneInstance = 0;

		// must not have logged out, set current zone
		Row row = results[ZONES_RS].at(0);
		player->m_currentZoneIndex.fromLong(row.at(0));
		currentZoneInstance = row.at(1);
		player->m_currentWorldMap = (const char *)row.at(2);

		// fix up currentZone, respawnZone if have > 16 bit instance
		// note that we can always figure out housing later
		player->m_currentZoneIndex.setInstanceId(currentZoneInstance);
	}

	// get the inventories and update them
	SQL_TASK("GetPlayer/inventories");

	Timer inventoryTimer;
	inventoryTimer.Start();
	getInventory(player->m_charName, &player->m_inventory, &player->m_bankInventory, results[INVENTORIES_RS], results[ITEMS_RS], results[PASSES_RS]);
	inventoryTimer.Pause();

	// log timing in format appropriate for "queue count cause" report
	LogError("Timing Inventory time : " << inventoryTimer.ElapsedMs());

	// get deformables
	SQL_TASK("GetPlayer/deformable_configs");

	player->m_charConfig.disposeAllItems();
	player->m_baseMeshes = results[DEFCFG_RS].size();
	if (results[DEFCFG_RS].num_rows() > 0) {
		// first get counts of each type/equip
		GLID_OR_BASE glidOrBase = (GLID_OR_BASE)results[DEFCFG_RS].at(0).at(DM_EQUIP_ID);
		BYTE slotCount = 0;
		for (UINT i = 0; i < results[DEFCFG_RS].num_rows(); i++) {
			Row r = results[DEFCFG_RS].at(i);
			if (glidOrBase != (int)r.at(DM_EQUIP_ID)) {
				player->m_charConfig.addItem(glidOrBase, slotCount);
				slotCount = 0;
				glidOrBase = (int)r.at(DM_EQUIP_ID);
			}
			slotCount++;
		}
		player->m_charConfig.addItem(glidOrBase, slotCount); // do last one (or first if only one)

		size_t itemSlot = 0;
		int itemEquip = CC_ITEM_INVALID;
		CharConfigItem *pConfigItem = NULL;
		for (UINT i = 0; i < results[DEFCFG_RS].num_rows(); i++) {
			Row r = results[DEFCFG_RS].at(i);
			if (itemEquip != (int)r.at(DM_EQUIP_ID)) {
				itemSlot = 0;
				itemEquip = (int)r.at(DM_EQUIP_ID);
				pConfigItem = player->m_charConfig.getItemByGlidOrBase(itemEquip);
				if (!pConfigItem)
					continue;
			}

			if (pConfigItem && itemSlot < pConfigItem->getSlotCount()) {
				CharConfigItemSlot *pSlot = pConfigItem->getSlot(itemSlot);
				assert(pSlot != nullptr);
				pSlot->m_meshId = r.at(DM_DEFCFG);
				pSlot->m_matlId = r.at(DM_MAT);
				pSlot->m_r = r.at(DM_COLOR_R);
				pSlot->m_g = r.at(DM_COLOR_G);
				pSlot->m_b = r.at(DM_COLOR_B);
			}

			itemSlot++;
		}
	}

	// get the player's cash
	SQL_TASK("GetPlayer/cash");

	if (results[CASH_RS].num_rows() > 0)
		player->m_cash->m_amount = results[CASH_RS].at(0).at(0);
	else
		player->m_cash->m_amount = 0;

	if (results[GIFT_RS].num_rows() > 0)
		player->m_giftCredits->m_amount = results[GIFT_RS].at(0).at(0);
	else
		player->m_giftCredits->m_amount = 0;

	if (results[BANK_RS].num_rows() > 0)
		player->m_bankCash->m_amount = results[BANK_RS].at(0).at(0);
	else
		player->m_bankCash->m_amount = 0;

	// get the skills
	SQL_TASK("GetPlayer/skills");
	if (player->m_skills)
		DELETE_AND_ZERO(player->m_skills);
	player->m_skills = new CSkillObjectList();
	for (UINT i = 0; i < results[SKILL_RS].num_rows(); i++) {
		Row r = results[SKILL_RS].at(i);
		CSkillObject *o = new CSkillObject();
		o->m_skillId = r.at(0);
		o->m_skillName = r.at(1);
		o->m_skillType = r.at(2);
		o->m_gainIncrement = r.at(3);
		o->m_basicAbilitySkill = r.at(4) != DB_FALSE;
		o->m_basicAbilityExpMastered = r.at(5);
		o->m_basicAbilityFunction = r.at(6);
		o->m_skillGainsWhenFailed = r.at(7);
		o->m_maxMultiplier = r.at(8);
		o->m_useInternalTimer = r.at(9) != DB_FALSE;
		o->m_filterEvalLevel = r.at(10) != DB_FALSE;
		o->m_currentLevel = r.at(11);
		o->m_currentExperience = r.at(12);
		player->m_skills->AddTail(o);
	}

	// get the stats
	SQL_TASK("GetPlayer/stats");
	if (player->m_stats)
		DELETE_AND_ZERO(player->m_stats);
	player->m_stats = new CStatObjList();
	for (UINT i = 0; i < results[STAT_RS].num_rows(); i++) {
		Row r = results[STAT_RS].at(i);
		CStatObj *o = new CStatObj();
		o->m_id = r.at(0);
		o->m_statName = r.at(1);
		o->m_capValue = r.at(2);
		o->m_gainBasis = r.at(3);
		o->m_benefitType = r.at(4);
		o->m_bonusBasis = r.at(5);
		o->m_currentValue = r.at(6);
		player->m_stats->AddTail(o);
	}

	// get the additional GetSet stuff
	SQL_TASK("GetPlayer/getSet");

	getGetSet(player, results[GETSET_RS]);

	// get noteriety
	SQL_TASK("GetPlayer/noteriety");
	if (player->m_noteriety) {
		player->m_noteriety->SafeDelete();
		DELETE_AND_ZERO(player->m_noteriety);
	}
	player->m_noteriety = new CNoterietyObj();

	if (results[NOTERIETY_RS].size() == 1) {
		player->m_noteriety->m_currentMurders = results[NOTERIETY_RS].at(0).at(0);
		player->m_noteriety->m_currentMinuteCountSinceLastMurder = results[NOTERIETY_RS].at(0).at(1);
	}

	SQL_TASK("Clan");

	// check to see if they were removed from clan on another server, and we need
	// to update this one
	if (results[CLAN_RS].size() > 0) {
		setClan(player, results[CLAN_RS].at(0).at(0).c_str(), results[CLAN_RS].at(0).at(1));
	} else {
		setClan(player, 0, 0);
	}

	SQL_TASK("PassGroups");

	loadPassList(player, results[PASSLIST_RS]);

	//Load name color for player
	if (results[NAME_COLOR].size() == 1) {
		player->m_nameColor.r = results[NAME_COLOR].at(0).at(0);
		player->m_nameColor.g = results[NAME_COLOR].at(0).at(1);
		player->m_nameColor.b = results[NAME_COLOR].at(0).at(2);
	}

	// get the quests
	SQL_TASK("GetPlayer/quests");
	player->m_questJournal = new CQuestJournalList();
	// TODO: get quests

	/* mbb 06/17/08 -
		    titles that were earned before the user logged in will no longer be loaded into the server
		    only titles that are earned while in game will be stored within the player's title list and when
		    the player logs out, those new titles will be added to the db
		*/
	// reset the player's titles
	SQL_TASK("GetPlayer/titles");
	player->m_titleList = new CTitleObjectList();

	END_TRY_SQL();

	if (player)
		playerInvUpdateCommitted(player);
}

void MySqlBase::loadPassList(CPlayerObject *pPO, StoreQueryResult &res) {
	if (pPO->m_passList)
		DELETE_AND_ZERO(pPO->m_passList);

	if (res.num_rows() > 0) {
		if (pPO->m_passList == 0)
			pPO->m_passList = new PlayerPassList;

		for (UINT i = 0; i < res.num_rows(); i++) {
			DateTime dt(res.at(i).at(1));
			pPO->m_passList->addPass(res.at(i).at(0), dt);
		}
	}
}

} // namespace KEP