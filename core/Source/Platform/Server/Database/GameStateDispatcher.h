///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IGameStateDispatcher.h"
#include "Event/Events/IBackgroundDBEvent.h"
#include "Event/Base/IDispatcher.h"
#include <log4cplus/logger.h>
#include <vector>

#include "common/include/kepcommon.h"
#include "Core/Util/Monitored.h"
#include "Core/Util/HighPrecisionTime.h"
using namespace log4cplus;

namespace KEP {

class GameStateWorkerThread;
class MTEventQueue;
class ServerEngine;

struct backgroundqueue_monitor_config {
	std::string emailHost;
	unsigned long infoThrottleMS; //	= cfg->Get( "BackgroundQCInfoThrottleMS", 0 );
	std::string infoEMailFrom; //	= string(cfg->Get( "BackgroundQCInfoEMailFrom", "" ));
	std::string infoEMailTo; //	= string(cfg->Get( "BackgroundQCInfoEMailTo", "" ));
	unsigned long warnThreshold; // default = 100
	unsigned long warnThrottleMS; //	= cfg->Get( "BackgroundQCWarnThrottleMS", 60000 );
	std::string warnEMailFrom;
	std::string warnEMailTo; //	= string(cfg->Get( "BackgroundQCWarnEMailTo", "" ));
};

///////////////////////////////////////////////////////////
/// class for queuing events to
/// background thread(s) for processing
class GameStateDispatcher : public IGameStateDispatcher, public Monitored {
public:
	GameStateDispatcher(ServerEngine *serverEngine, IDispatcher &replyDispatcher, const char *instanceName, const DBConfig &dbConfig);

	bool Init(const char *baseDir);

	/// queue an event to the background
	void QueueEvent(IBackgroundDBEvent *e);
	void SetServerNetwork(IServerNetwork *s);

	/// register all the DB events for the dispatcher
	void RegisterDBEvents(IDispatcher &disp);

	unsigned long peakQueueDepth() const { return m_peakQueueDepth; }

	/// stop all thread, preparing for exit
	void Shutdown();
	Monitored::DataPtr monitor() const;

	~GameStateDispatcher();

private:
	typedef std::vector<GameStateWorkerThread *> GSWorkers;

	GSWorkers m_workers;
	Logger m_logger;
	IDispatcher &m_replyDispatcher;
	int m_maxThreads;
	backgroundqueue_monitor_config bcqMonitorConfig;

	TimeDelay m_reportQCInfoAlarm;
	TimeDelay m_reportQCWarnAlarm;
	Timer m_timerStart;
	MTEventQueue *m_queue;
	unsigned long m_peakQueueDepth;
	std::string m_instanceName;
	ServerEngine *m_server;
	const DBConfig &m_dbConfig;
#ifdef __GAMESTATEDISPATCHER_H_INTERNAL__
	friend class GSDMonitorData;
#endif
};

} // namespace KEP