///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <js.h>
#include <jsThread.h>
#include "MySqlUtil.h"
#include "Event\EventSystem\Dispatcher.h"
#include "Event\DDR\DDR_S_GameData.h"
#include "Event\DDR\DDR_S_Player.h"

namespace KEP {
class DDREvent;
class DDRDBEvent;

// Perform all db work on a separate thread and perform interprocess
// communcation via the event framework.
class DDRGameDB : public jsThread {
public:
	DDRGameDB(const char *gameDir, const DBConfig &dbConfig, IDispatcher *dispatcher);
	~DDRGameDB();
	ULONG _doWork();

	virtual void Initialize(const char *gameDir);

	bool RecordDDRScore(bool isPermanentZone, ULONG objectId, ULONG zoneIndex,
		int dynObjectId, ULONG gameId, int playerId, std::string playerName,
		ULONG highScore, ULONG score,
		ULONG numPerfects, ULONG numPerfectsInARow);

	bool RecordDDRHighScore(int dynObjectId, int playerId, ULONG highScore, std::string playerName,
		bool isPermanentZone,
		ULONG objectId,
		ULONG zoneIndex);

	bool LoadDDRGameData(ULONG gameId, GameLevelsMap &gameData);

	bool LoadDDRPlayerData(DDRDBEvent *ddre, DDREvent *replyEvent);
	bool RefreshDDRHighScore(int dynObjectId, DDRPlayers *players);

	//static EVENT_PROC_RC GetDDRGameDataHandler( ULONG lparam, IDispatcher *disp, IEvent *e  );
	static EVENT_PROC_RC RecordScoreHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC LoadGameDataHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC LoadPlayerDataHandler(ULONG lparam, IDispatcher *disp, IEvent *e);

	virtual IDispatcher *GetDispatcher() { return m_dispatcher; }
	virtual IDispatcher *GetOutDispatcher() { return m_outDispatcher; }

private:
	Dispatcher *m_dispatcher;
	IDispatcher *m_outDispatcher;

	std::string m_gameDir;

	const DBConfig &m_dbConfig;
	KGP_Connection *m_conn;
	//	Logger					m_logger;
	std::string m_dbName;

	fast_recursive_mutex m_sync; // calls to class came come from the event system, e.g. the ddr blade, in
		// which they will be owned by the DDRGameDB thread. Syncronous calls can
		// also by invoking the methods directly on the created object. These two
		// types of calls need to coordinate their work in this class.
};

} // namespace KEP

