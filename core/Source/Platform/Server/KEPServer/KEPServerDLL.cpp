///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common/include/kepcommon.h"
#include "KEPServerImpl.h"

namespace KEP {

// DLL entry pts are here
static Logger alert_logger(Logger::getInstance(L"Alert"));

extern "C" bool KEPServer_Run(const std::string &pathXml, int instanceId) {
	Logger logger(Logger::getInstance(L"API"));
	TraceLogger tl(logger, L"Run");

	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// Run Server
	KEPServerImpl *server = KEPServerImpl::GetServer(pathXml, instanceId);
	if (server) {
		return server->Run();
	}

	LOG4CPLUS_ERROR(logger, loadStr(IDS_SERVER_NOT_STARTED));
	return false;
}

extern "C" ULONG KEPServer_GetShutdownTimeoutMs() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// get the global server object
	KEPServerImpl *server = KEPServerImpl::GetServer();

	if (server) {
		return server->ShutdownTimeoutMs();
	} else {
		return 300;
	}
}

extern "C" EServerState KEPServer_GetState() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// get the global server object
	KEPServerImpl *server = KEPServerImpl::GetServer();

	if (server) {
		EServerState ss = server->GetState();
		if ((ss == ssStopped || ss == ssFailed) && KEPServerImpl::IsShutdown()) {
			LOG4CPLUS_DEBUG(Logger::getInstance(L"KEPServer"), "KEPServer_GetState() signalling server to cleanup");
			server->Cleanup(); // shutdown and delete
		}
		return ss;
	}

	LOG4CPLUS_ERROR(Logger::getInstance(L"KEPServer"), loadStr(IDS_SERVER_NOT_STARTED));
	return ssStopped;
}

extern "C" void KEPServer_Shutdown() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	Logger logger(Logger::getInstance(L"SHUTDOWN"));
	TraceLogger tl(logger, L"KEPServer_Shutdown");

	// get the global server object
	KEPServerImpl *server = KEPServerImpl::GetServer();

	if (server)
		server->Shutdown();
	else if (!KEPServerImpl::IsShutdown()) {
		LOG4CPLUS_ERROR(logger, loadStr(IDS_SERVER_NOT_STARTED));
	}
}

extern "C" void KEPServer_Cleanup() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	Logger logger(Logger::getInstance(L"SHUTDOWN"));
	TraceLogger tl(logger, L"KEPServer_Cleanup");

	// get the global server object
	KEPServerImpl *server = KEPServerImpl::GetServer();

	if (server) {
		LOG4CPLUS_DEBUG(logger, "KEPServer_Cleanup() signalling server to cleanup");
		server->Cleanup();
	} else if (!KEPServerImpl::IsShutdown()) {
		LOG4CPLUS_ERROR(logger, loadStr(IDS_SERVER_NOT_STARTED));
	}
}

/**
 * It's not really clear to me why, but the opaque boundary between CKEPService and KEPServerImpl
 * appears to be intentional.  
 * 1) The functions in this file are the only interaction between the service and the server.
 * 2) There is no interface/wrapper for KEPServerImpl through which channel queries to the impl.
 *
 * That being said however, direct calls to the functions above from the service create a hard 
 * defacto binding.
 */
_declspec(dllexport) Monitored::DataPtr KEPServer_monitor() {
	KEPServerImpl *server = KEPServerImpl::GetServer();
	if (server)
		return server->monitor();
	return Monitored::DataPtr();
}

} // namespace KEP