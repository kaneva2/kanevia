///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common\KEPUtil\Helpers.h"

#include "jsLock.h"

#include "Common/KEPUtil/CountDownLatch.h"
#include "Core/Util/monitored.h"
#include "Core/Util/ThreadPool.h"
#include "common/keputil/httpserver.h"
#include "Common/KEPUtil/ExtendedConfig.h"
#include "Common/KEPUtil/Heartbeat.h"
#include "Common/include/DBConfigValues.h"
#include "../../Engine/Blades/BladeFactory.h"
#include "KEPConfig.h"
#include "KEPServerEnums.h"
#include <TinyXML/tinyxml.h>
#include "common/keputil/mailer.h"

// forward declarations
namespace RuntimeReporter {
struct CallbackStruct;
}

namespace KEP {

typedef std::map<std::string, time_t> CookieMap;

class EngineThread;
class EngineInfo;

class KEPServerImpl : public Monitored, public HeartbeatListener {
public:
	~KEPServerImpl(void);

	// DRF - Added
	std::string PathBladesXml();
	std::string PathBladesDll();
	std::string PathDbCfg();

	enum EServerState GetState(bool forceStop = false);
	DWORD StartAll(std::string& result);
	DWORD StopAll();
	DWORD SaveAll();
	DWORD GetServerFiles(std::vector<std::string>& files);

	static KEPServerImpl* GetServer(const std::string& pathXml, int instanceId);
	static KEPServerImpl* GetServer();

	// impl of DLL entry pts as member fns
	bool Run();

	void thump();

	// stop the server
	void Shutdown();

	// Cleanup any artifacts (e.g. cleanexit flag, pid file etc.)
	void Cleanup();

	TimeMs ShutdownTimeoutMs() const {
		return m_maxTimeoutMs;
	}

	static bool IsShutdown() {
		return m_shutdown;
	}

	// This is the base folder for the path to the application.exe (pathApp)
	std::string m_baseDir;
	std::string BaseDir() const {
		return m_baseDir;
	}
	void SetBaseDir(const std::string& path) {
		m_baseDir = path;
	}

	// This is the path to the configuration files, specifically KGPServer.xml and db.cfg.
	// It is also set as the baseDir for all loaded engine blades.
	std::string m_pathXml;
	std::string PathXml() const {
		return m_pathXml;
	}
	void SetPathXml(const std::string& path) {
		m_pathXml = path;
	}

	Monitored::DataPtr monitor() const;
	ThreadPool& threadPool() const {
		return _threadPool;
	}

	const DBConfig& GetDBConfig() const {
		return m_dbConfig;
	}

	/**
	* Acquire reference to the startup Timer.
	*/
	Timer& startupTimer() const {
		return _startupTimer;
	}

	/**
	* Acquire reference to the startup latch.
	*/
	CountDownLatch& enginesStartupLatch() const {
		return _enginesStartupLatch;
	}

private:
	KEPConfigAdapter m_dbConfigFile;
	DBConfig m_dbConfig;

	KEPServerImpl(const std::string& pathXml, int instanceId);
	KEPServerImpl& KEPServerImpl::configure(const std::string& xml);

	// check userid/pw or cookie
	UINT checkLogin(TiXmlElement* root, std::string& cookie);

	void newCookie(std::string& cookie);

	void actionResultError(std::string& resultXml, LONG errNo, const std::string& errMsg);

	bool findEngineName(const char* name);
	void freeAllDisabledEngines();

	void sortThreads();
	void createEngineBlade(const IKEPConfig* config, const std::string& bladeXmlDir);

	std::string getPidFileName(int instanceId);

	std::vector<EngineThread*> m_engineThreads;
	std::vector<EngineInfo*> m_disabledEngines;

	enum EServerState highestThreadState();
	enum EServerState m_state;

	BladeFactory m_bladeFactory;

	UINT m_adminTimeoutSec; // timeout for cookies from admin i/f

	CookieMap m_cookies;

	TimeMs m_maxTimeoutMs; // how long to wait until kill engines
	TimeMs m_shutdownStartTimestamp;

	std::string m_stateAsString;
	int m_serverCount;
	int m_threadCount;

	UINT m_adminPort; // for listening

	std::string m_debugAdminFile;

	bool m_autostart;

	std::string m_adminUser;

	std::string m_adminUserHash;

	bool m_autoSendReport; //true will auto send crash report to kaneva

	std::string m_adminEmail; //admin will be notified via email when server crashes

	jsRWSync m_adminSync; // for sync'ing admin calls

	LightWeightMailer m_mailer;

	static fast_recursive_mutex m_sync;

	static KEPServerImpl* m_theServer;

	static bool m_shutdown;

	LPVOID m_lpvState;

	int m_dumpFlags; //Holds the detail level for generating dumps. See info on DbgHelp to combine flags

	std::string m_tempDir; // Temporary directory for temporary runtime data like pid file

	std::string m_pidFileName; // Custom file name for KGPServer.pid

	int m_instanceId; // Defaults to 1 can be overridden via command line second server is getting its config from the database

	mutable ConcreteThreadPool _threadPool; // A thread pool used by web server request handlers.

	mutable Timer _startupTimer; // Tracks time elapsed from instantiation of this KEPServerImpl object to full server up (setState(running))

	mutable CountDownLatch _enginesStartupLatch; // Used to detect all engines up.

	std::string _instanceName; // Configurable name used to identify this instance.  Defaults to "<unknown>"

	friend class EngineBladesCreator;
	friend class ReportEnginesUpTask;

public:
#if (DRF_RUNTIME_REPORTER_SERVER > 0)
	// RuntimeReporter Support
	bool RR_Install();
	void RR_AppCallback(RuntimeReporter::CallbackStruct& cbs);
#endif
};

} // namespace KEP