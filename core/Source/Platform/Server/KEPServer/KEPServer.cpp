///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

class CKEPServerApp : public CWinApp {
public:
	virtual BOOL InitInstance() override { return CWinApp::InitInstance(); }

	virtual int ExitInstance() override { return CWinApp::ExitInstance(); }

	/** DRF - Overrides CWinApp::ProcessWndProcException()
	* This is where we land with some MFC exceptions. If we needed to show a message or something, we 
	* could do that here. However, in most cases we just want to cause MFC to throw the exception out 
	* to CrashRpt.
	*/
	virtual LRESULT ProcessWndProcException(CException* e, const MSG* pMsg) override { THROW_LAST(); }

	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CKEPServerApp, CWinApp)
END_MESSAGE_MAP()

CKEPServerApp theApp;

// Including this header file works around DLLMain initialization issue when
// Using both boost/thread and MFC in a DLL.
// MFC code has this assertion in AfxCoreInitModule():
//
// ASSERT(AfxGetModuleState() != AfxGetAppModuleState());
//
// This is caused by boost::thread's DLLMain replacement function defined in
// [boost_source]/libs/thread/src/win32/tss_pi.cpp
// Including the following header causes the boost code to ensure the MFC initialization code gets executed.
#include "boost/thread/win32/mfc_thread_init.hpp"
