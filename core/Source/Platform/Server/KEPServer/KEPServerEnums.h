///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

enum EServerState {
	ssStopped,
	ssRunning,
	ssStopping,
	ssStarting,
	ssLocked,
	ssFailed,
};

const char* const SERVER_LOG_PROP_FILE = "ServerLog.cfg";

} // namespace KEP
