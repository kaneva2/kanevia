///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <memory>

#include "Core/Util/ObjectRegistry.h"
#include "Core/Util/Runnable.h"
#include "Core/CorePluginLibrary/Metrics.h"
#include "Common/KEPUtil/Helpers.h"
#include "Common/KEPUtil/Application.h"
#include "Common/KEPUtil/URL.h"
#include "common/keputil/MongooseHTTPServer.h"
#include "common/KEPUtil/VLDWrapper.h"
#include "common/keputil/HTTPServer.h"
#include "common/keputil/ktime.h"
#include "common/keputil/PluginHostConfigurator.h"
#include "common/KEPUtil/ExtendedConfig.h"
#include "Common\KEPUtil\WebCall.h"
#if (DRF_RUNTIME_REPORTER_SERVER > 0)
#include "Common\KEPUtil\RuntimeReporter.h"
#endif
#include "Common\include\NetworkCfg.h"

#include "KEPServerImpl.h"
#include "resource.h"
#include "../KEPService/ServiceStrings.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "Audit.h"
#include "Alert.h"
#include "..\database\BaseEngineConfig.h"
#include "../Engine/Base/Engine.h"
#include "../Engine/Base/EngineStrings.h"
#include "Audit.h"
#include "SetThreadName.h"
#include "CGlobalInventoryClass.h"
#include "jsConnection.h"
#include "jsEnumFiles.h"
#include "LocaleStrings.h"
#include "LogHelper.h"

#pragma warning(push)
#pragma warning(disable : 4091) // 'typedef ': ignored on left of '' when no variable is declared
#include <DbgHelp.h>
#pragma warning(pop)

#define REG_APP_NAME K_SERVER_NAME
#define REG_KEY_NAME "Id"

#define IDS_BASE_STATE_STR IDS_SSSTOPPED
// how long to wait for admin reply
#ifdef _DEBUG
#define ADMIN_WAIT_MS 60000
#else
#define ADMIN_WAIT_MS 5000
#endif
#define SERVER_BUSY_RESULT "<ActionResult rc=\"-2\">The server is busy  </ActionResult>"
#define SERVER_TYPE_RESULT "<ActionResult rc=\"-3\">Wrong object type </ActionResult>"

namespace KEP {

static LogInstance("KEPServerImpl");
static const int DISABLE_DUMP = -1;

typedef std::shared_ptr<IEngine> IEnginePtr;

static const char* const XML_FILE_NAME = "KGPServer.xml";
static const char* const OLD_XML_FILE_NAME = "KEPServer.xml";
static const char* const ROOT_TAG = "KGPServer";
static const char* const DIR_TAG = "Directory";
static const char* const ENGINES_TAG = "Engines";
static const char* const ENGINE_TAG = "Engine";
static const char* const NAME_TAG = "name";
static const char* const CONFIG_TAG = "ConfigFile";
static const char* const THREAD_TAG = "Thread";
static const char* const ADMIN_PORT_TAG = "AdminPort";
static const char* const AUTO_START_TAG = "Autostart";
static const char* const ADMIN_USER_TAG = "AdminUser";
static const char* const ADMIN_HASH_TAG = "AdminHash";
static const char* const TEMP_DIR_TAG = "TempDirectory";
static const char* const PID_FNAME_TAG = "PidFileName";
static const char* const LOAD_PRIORITY = "LoadPriority";
static const char* const MAX_SHUTDOWN_TIME_TAG = "MaxShutdownTimeSecs";
static const char* const AUTO_REPORT_TAG = "AutoSendCrashReport";
static const char* const ADMIN_EMAIL_TAG = "AdminEmailTag";
static const char* const ALERT_SENDER = "AlertMailer/sender";
static const char* const ALERT_RECIPIENT = "AlertMailer/recipient";
static const char* const ALERT_HOST = "AlertMailer/host";
static const char* const ADMIN_COOKIE_TIMEOUT = "AdminCookieTimeoutSec";
static const char* const DUMP_FLAGS = "DumpFlags";

KEPServerImpl* KEPServerImpl::m_theServer = 0;
fast_recursive_mutex KEPServerImpl::m_sync; // protect m_theServer
bool KEPServerImpl::m_shutdown = false;

static const ULONG ROOT_OBJECT_ID = 0;
static const int DEFAULT_ADMIN_TIMEOUT_SEC = 5 * 60;
static const UINT DEFAULT_ADMIN_PORT = 8989;

static const LONG DEFAULT_MAX_SHUTDOWN_TIME = 120; // seconds, also the minimum value
static const int DEFAULT_DUMP_FLAGS = MiniDumpWithDataSegs | MiniDumpWithHandleData |
									  MiniDumpWithUnloadedModules;

static const UINT EXCLUSIVE_THREAD = 0;

string KEPServerImpl::getPidFileName(int instanceId) {
	if (!m_pidFileName.empty()) {
		return m_pidFileName;
	}

	stringstream ssPidFileName;
	ssPidFileName << "KGPServer";
	if (instanceId != 1) // instanceId default is 1 (unassigned)
	{
		ssPidFileName << "_" << instanceId;
	}
	ssPidFileName << ".pid";
	return ssPidFileName.str();
}

KEPServerImpl& KEPServerImpl::configure(const string& xml) {
	bool isDir = false;
	if (!(jsFileExists(xml.c_str(), &isDir) && !isDir)) {
		LogError("FILE NOT FOUND - '" << xml << "'");
		return *this;
	}

	try {
		TIXMLConfig config;

		OldCommandLine::StringParameter* p = dynamic_cast<OldCommandLine::StringParameter*>(Application::singleton().commandLine().getParameter("-D"));
		if (p != NULL) {
			config.paramSet().set(StringHelpers::parseKeyValuePairsWithOption(*p));
		}
		TIXMLConfigHelper(config).load(xml);

		// Be sure to tell the adapter to NOT manage the configurations memory...i.e. don't attempt to delete it.
		KEPConfigAdapter cfg(config, false);

		// PluginHost supports some singleton semantics, though the singularity is not
		// enforced.  I.e. the singleton class's constructor is not hidden.  You can
		// have more than one PluginHost, but only one "singleton" PluginHost.
		//
		PluginHostConfigurator().configure(PluginHost::singleton(), config);

		string newDir;
		if (cfg.GetValue(DIR_TAG, newDir)) {
			if (!newDir.empty() && jsFileExists(newDir.c_str(), &isDir) && isDir) {
				SetBaseDir(newDir);
				LogWarn("CONFIG CHANGE - baseDir='" << BaseDir() << "'");
			}
		}

		m_adminTimeoutSec = min(10 * 60, max(10, cfg.Get(ADMIN_COOKIE_TIMEOUT, DEFAULT_ADMIN_TIMEOUT_SEC)));
		m_adminPort = cfg.Get(ADMIN_PORT_TAG, (int)DEFAULT_ADMIN_PORT);
		m_debugAdminFile = cfg.Get("DebugAdminFilePrefix", "");
		m_autostart = cfg.Get(AUTO_START_TAG, false);
		m_adminUser = cfg.Get(ADMIN_USER_TAG, "Admin");
		m_adminUserHash = cfg.Get(ADMIN_HASH_TAG, "");
		m_maxTimeoutMs = 1000 * std::max((long) cfg.Get(MAX_SHUTDOWN_TIME_TAG, DEFAULT_MAX_SHUTDOWN_TIME), DEFAULT_MAX_SHUTDOWN_TIME);

		m_autoSendReport = cfg.Get(AUTO_REPORT_TAG, true);
		m_adminEmail = cfg.Get(ADMIN_EMAIL_TAG, "");
		m_dumpFlags = cfg.Get(DUMP_FLAGS, DEFAULT_DUMP_FLAGS);

		m_mailer.setMailHost(cfg.Get(ALERT_HOST, ""))
			.setSenderEmail(cfg.Get(ALERT_SENDER, ""))
			.addRecipient(cfg.Get(ALERT_RECIPIENT, ""));
		LOG4CPLUS_INFO(Logger::getInstance(L"STARTUP"), "Mailer configured: " << m_mailer.str().c_str());

		_instanceName = cfg.Get("instance_name", "unknown");

#ifdef DEPLOY_PLUGINS
		// Load the plugin configuration
		//
		TiXmlHandle pluginHostConfig = cfg.GetElement("PluginHost");
		if (pluginHostConfig) {
			// Hand off the configuration to the PluginHost configurator
			// I hate that it requires that I pass the full configuration plus
			// a handle to the configuration...I would have hoped that we
			// could simply pass a fragment around.
			//
			PluginHost::Configurator configurator(cfg, pluginHostConfig);
		}
#endif

		string fname;
		string pathLang = PathAdd(BaseDir(), "Lang");
		if (!initializeLocaleStrings(pathLang.c_str(), fname)) {
			LogError("initializeLocaleStrings() FAILED - '" << fname << "'");
		}

		// Hosted servers should define <TempDirectory> tag
		m_tempDir = cfg.Get(TEMP_DIR_TAG, "");
		if (m_tempDir.empty()) {
			// Try gameBaseDir: for self-hosted 3D Apps (launched by editor or as service)
			m_tempDir = PathXml();
		}

		// Use %TEMP%: for WOK servers
		if (m_tempDir.empty())
			m_tempDir = GetEnv("TEMP");

		// KGPServer.pid file name override
		m_pidFileName = cfg.Get(PID_FNAME_TAG, "");

		if (!m_tempDir.empty()) {
			// Look for old PID file from non-clean exit
			string pidFilePath = PathAdd(m_tempDir, getPidFileName(m_instanceId));
			if (jsFileExists(pidFilePath.c_str(), NULL)) {
				SocketSubSystem ss;
				LOG4CPLUS_WARN(m_logger, loadStr(IDS_NO_CLEAN_EXIT));
				stringstream subject;
				subject << "[WOKALERT][" << ss.hostName() << "] Non-Clean Exit alert";
				m_mailer.send(subject.str().c_str(), loadStr(IDS_NO_CLEAN_EXIT));

				if (!DeleteFileW(Utf8ToUtf16(pidFilePath).c_str())) {
					LogWarn("Error deleting old PID file [" << pidFilePath << "]: err=" << GetLastError());
				}
			}

			//Create new pid file for current process
			fstream fsPid(pidFilePath.c_str(), ios::out);
			fsPid << GetCurrentProcessId();
		} else {
			LogWarn("TEMP directory not specified in KGPServer.xml or environments");
		}
	} catch (KEPException* e) {
		LogError("Caught KEP Exception - " << e->ToString());
		e->Delete();
	} catch (const ChainedException& e) {
		LogError("Caught Chained Exception - " << e.str());
	}

	return *this;
}

KEPServerImpl::KEPServerImpl(const string& pathXml, int instanceId) :
		m_state(ssStopped), m_serverCount(0), m_threadCount(0), m_adminTimeoutSec(DEFAULT_ADMIN_TIMEOUT_SEC), m_adminPort(DEFAULT_ADMIN_PORT), m_autostart(false), m_adminSync(1, "AdminSync"), m_maxTimeoutMs(DEFAULT_MAX_SHUTDOWN_TIME), m_shutdownStartTimestamp(0), m_autoSendReport(false), m_adminEmail(""), m_dumpFlags(DEFAULT_DUMP_FLAGS), m_bladeFactory(eEngineType::Server), m_instanceId(instanceId), m_mailer(), _threadPool(2, string("KEPServer")), m_dbConfig(m_dbConfigFile) // initialized as they are declared: m_dbConfigFile before m_dbConfig
		,
		_enginesStartupLatch() {
	// DRF - Set Base Dir To Application Path
	// This is the path of the application.exe found by PathFind(), typically the current working directory.
	SetBaseDir(PathApp());

	// DRF - Set Path To Xmls
	// This is the path of the application.exe by default and is overidden by command line path.
	// This is used to load config files (*.xml, *.svr. etc) and set as the baseDir of loaded engine blades.
	SetPathXml(pathXml);

	// see if we have an XML config file
	string xml = PathAdd(PathXml(), "KGPServer.xml");

	configure(xml);
	_threadPool.start();
}

KEPServerImpl::~KEPServerImpl(void) {
	freeAllDisabledEngines();
	m_engineThreads.clear();
	freeLocaleStrings();
}

// create the snip of XML to report an error and log a message
void KEPServerImpl::actionResultError(string& resultXml, LONG errNo, const string& errMsg) {
	char s[34];
	_ltoa_s(errNo, s, _countof(s), 10);

	resultXml = "<ActionResult rc=\"";
	resultXml += s;
	resultXml += "\">";
	resultXml += errMsg;
	resultXml += "</ActionResult>";

	LogError(errMsg);
}

// static method to get *the* server
KEPServerImpl* KEPServerImpl::GetServer(const string& pathXml, int instanceId) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	if (m_theServer == 0 && !m_shutdown)
		m_theServer = new KEPServerImpl(pathXml, instanceId);
	return m_theServer;
}

KEPServerImpl* KEPServerImpl::GetServer() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return m_theServer;
}

void KEPServerImpl::newCookie(string& cookie) {
	// TODO: something better?
	char s[34];
	do {
		_ltoa_s((LONG)this * rand(), s, _countof(s), 8);
		cookie = s;
	} while (m_cookies.find(cookie) != m_cookies.end());

	m_cookies.insert(CookieMap::value_type(cookie, time(0) + m_adminTimeoutSec));
}

UINT KEPServerImpl::checkLogin(TiXmlElement* root, string& cookie) {
	// first check cookie
	if (!root) {
		LogError("root is null");
		return IDS_NO_ADMIN_XML;
	}

	cookie = NULL_CK(root->Attribute("cookie"));
	if (!cookie.empty()) {
		// see if it hasn't expired
		CookieMap::iterator f = m_cookies.find(cookie);
		if (f != m_cookies.end()) {
			if (f->second < time(0)) {
				cookie.clear();
				m_cookies.erase(f);
				LogInfo(loadStr(IDS_AUTH_EXPIRED));
				return IDS_AUTH_EXPIRED;
			} else {
				// bump up their time
				f->second = time(0) + m_adminTimeoutSec;
				return NO_ERROR;
			}
		} else
			cookie.clear(); // not found
	}

	if (cookie.empty()) {
		// better have userid/pw
		string userId = NULL_CK(root->Attribute("userId"));
		string password = NULL_CK(root->Attribute("password"));
		if (userId.empty() || password.empty()) {
			return IDS_NO_AUTH_INFO;
		} else {
			// check the credentials against the userid/pw

			// now compare against the one in the XML
			MD5_DIGEST_STR hash;
			hashString(password.c_str(), hash);

			if (::_stricmp(m_adminUser.c_str(), userId.c_str()) != 0 ||
				m_adminUserHash != hash.s) {
				LogError("AUTH_DENIED");
				return IDS_AUTH_DENIED;
			}

			// they made it, or no auth turned on
			newCookie(cookie);
		}
	}

	return NO_ERROR;
}

class WaitRequestHandler : public HTTPServer::RequestHandler {
public:
	KEPServerImpl& _server;
	WaitRequestHandler(KEPServerImpl& server) :
			_server(server), HTTPServer::RequestHandler("wait") {}
	virtual ~WaitRequestHandler() {}

	int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
		map<string, string>::const_iterator i = request._paramMap.find(string("time"));
		if (i == request._paramMap.end()) {
			endPoint << (HTTPServer::Response() << Header("Content-Type", "text/html;charset=UTF-8") << "No timeout value specified");
			return 1;
		}
		pair<string, string> p = *i;
		unsigned long l = atol(p.second.c_str());

		i = request._paramMap.find(string("id"));
		string id;
		if (i != request._paramMap.end()) {
			id = (*i).second;
		}

		Sleep(l);
		endPoint
			<< (HTTPServer::Response()
				   << Header("Content-Type", "text/html;charset=UTF-8")
				   << "OK"
				   << id);
		return 1;
	}
};
#ifdef DEPLOY_CLEANER
class GILRequestHandler : public HTTPServer::RequestHandler {
public:
	KEPServerImpl& _server;
	GILRequestHandler(KEPServerImpl& server) :
			_server(server), HTTPServer::RequestHandler("gil") {}
	virtual ~GILRequestHandler() {}

	int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
		map<string, string>::const_iterator i = request._paramMap.find(string("op"));
		if (i == request._paramMap.end()) {
			endPoint << (HTTPServer::Response() << Header("Content-Type", "text/html;charset=UTF-8") << "No operation specified");
			return 1;
		}

		string op = (*i).second;

		if (op == "dump") {
			endPoint << (HTTPServer::Response() << Header("Content-Type", "text/xml;charset=UTF-8") << CGlobalInventoryObjList::GetInstance()->toXML());
			return 1;
		}

		if (op == "summary") {
			endPoint << (HTTPServer::Response() << Header("Content-Type", "text/xml;charset=UTF-8") << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << CGlobalInventoryObjList::GetInstance()->toString());
			return 1;
		}
		return 0;
	}
};

class GILCleanerHandler : public HTTPServer::RequestHandler {
public:
	KEPServerImpl& _server;
	GILCleanerHandler(KEPServerImpl& server) :
			_server(server), HTTPServer::RequestHandler("gil") {}
	virtual ~GILCleanerHandler() {}

	int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
		map<string, string>::const_iterator i = request._paramMap.find(string("op"));
		if (i == request._paramMap.end()) {
			endPoint << (HTTPServer::Response() << Header("Content-Type", "text/html;charset=UTF-8") << "No operation specified");
			return 1;
		}

		string op = (*i).second;

		if (op == "dump") {
			endPoint << (HTTPServer::Response() << Header("Content-Type", "text/xml;charset=UTF-8") << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << CGlobalInventoryObjList::GetInstance()->cleaner()->toXML());
			return 1;
		}

		if (op == "summary") {
			endPoint << (HTTPServer::Response() << Header("Content-Type", "text/xml;charset=UTF-8") << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << CGlobalInventoryObjList::GetInstance()->cleaner()->toXMLSummary());
			return 1;
		}
		return 0;
	}
};
#endif

class LeakReporter : public HTTPServer::RequestHandler {
public:
	const KEPServerImpl& _server;
	fast_recursive_mutex _resource;
	bool _inProgress;
	wstring _originalOutputFileName;
	wstring _derivedOutputFileName;

	class LeakReportRunnable : public Runnable {
	public:
		LeakReporter* _reporter;
		LeakReportRunnable(LeakReporter& reporter) :
				_reporter(&reporter) {}

		RunResultPtr run() override {
			LogInstance("HTTPServer");
			LogTrace("run()");
#ifdef _DEBUG
#ifdef VLD_ENABLED
			VLDSetReportOptions(VLD_OPT_REPORT_TO_FILE | VLD_OPT_REPORT_TO_STDOUT, _reporter->_derivedOutputFileName.c_str());
			VLDGlobalDisable();
			LogDebug("Calling VLDReportLeaks()");
			VLDReportLeaks();
			LogDebug("VLDReportLeaks() call complete!");
			VLDGlobalEnable();

			// VLD source indicates that setting the output target to something other than VLD_OPT_REPORT_TO_FILE
			// when there is an open file will cause the file to be closed.
			VLDSetReportOptions(VLD_OPT_REPORT_TO_STDOUT, _reporter->_originalOutputFileName.c_str());
#endif
#endif
			_reporter->_inProgress = false;
			return RunResultPtr();
		};
	};

	LeakReporter(const KEPServerImpl& server) :
			HTTPServer::RequestHandler("ReportLeaks"), _server(server), _inProgress(false), _resource() {}

	wstring stampFileName(wstring baseFileName) {
		KTimeStamp tstamp;
		wstringstream temporaryOutputFileName;
		temporaryOutputFileName << baseFileName
								<< wstring(L".")
								<< tstamp.year()
								<< tstamp.month()
								<< tstamp.dayOfMonth()
								<< tstamp.hour()
								<< tstamp.second();
		return temporaryOutputFileName.str();
	}

	int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
#ifdef VLD_ENABLED
		wstringstream ss;
		if (_inProgress) {
			endPoint << (HTTPServer::ResponseW() << Header("Content-Type", "text/html;charset=UTF-16")
												 << L"Allocation dump in progress. See ["
												 << _derivedOutputFileName
												 << L"]");
			return 1;
		}

		std::lock_guard<fast_recursive_mutex> lock(_resource);
		_inProgress = true;

		wchar_t acbuf[512];
		VLDGetReportFilename(acbuf);
		_originalOutputFileName = wstring(acbuf);
		_derivedOutputFileName = stampFileName(_originalOutputFileName);

		// write the notice.
		//
		ThreadPool& tp = _server.threadPool();
		LeakReportRunnable* prunnable = new LeakReportRunnable(*this);
		tp.enqueue(RunnablePtr(prunnable));
		endPoint << (HTTPServer::ResponseW() << Header("Content-Type", "text/html;charset=UTF-16")
											 << L"Allocation dump initiated. See ["
											 << _derivedOutputFileName
											 << L"]");
#else
		endPoint << (HTTPServer::ResponseW() << Header("Content-Type", "text/html;charset=UTF-16")
											 << L"Visual Leak Detector not available!");
#endif
		return 1;
	}
};

class MonitorRequestHandler : public HTTPServer::RequestHandler {
public:
	const KEPServerImpl& _server;
	MonitorRequestHandler(const KEPServerImpl& server) :
			HTTPServer::RequestHandler("MonitorRequestHandler"), _server(server) {}

	int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) override {
		LogInstance("HTTPServer");
		LogTrace("handle()");
		endPoint << (HTTPServer::Response() << Header("Content-Type", "text/xml; charset=UTF-8")
											<< _server.monitor()->toString());
		LogDebug("Response dispatched");
		return 1;
	}
};

class ServerControlHandler : public HTTPServer::RequestHandler {
public:
	KEPServerImpl& _server;
	ServerControlHandler(KEPServerImpl& server) :
			HTTPServer::RequestHandler("ServerControlHandler"), _server(server) {}

	int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) override {
		LogTrace("");
		map<string, string>::const_iterator i = request._paramMap.find("op");
		if (i == request._paramMap.cend())
			return 1;
		if (i->second == "shutdown") {
			LOG4CPLUS_INFO(log4cplus::Logger::getInstance(L"SHUTDOWN"), "ServerControlHandler::handle(): Signalling SHUTDOWN");
			_server.Shutdown();
			endPoint << (HTTPServer::Response()
						 << Header("Content-Type", "text/xml; charset=UTF-8")
						 << "<ok/>");
		}
		return 1;
	}
};

bool KEPServerImpl::Run() {
	bool runWithoutControllerInterface = false;

	jsConnection conn;

	jsConnection::Status stat;

	stat = conn.bind(m_adminPort);
	LogInfo("adminPort=" << m_adminPort);
	if (stat != jsConnection::Ok) {
		LogError("conn.bind(adminPort) FAILED - stat=" << (int)stat);
		if (stat == jsConnection::Error) {
			// Continue to run without the controller interface. Most likely scenario is
			// a machine is running multiple servers, e.g. a 3D App developer. They are not
			// likely to use the administrative interface to manage these servers. So, continue
			// to run.

			runWithoutControllerInterface = true;
		}
	}

	if (!runWithoutControllerInterface) {
		stat = conn.listen();
		if (stat != jsConnection::Ok) {
			LogFatal("conn.listen() FAILED - stat=" << (int)stat);
			return false;
		}
	}

	ServerControlHandler serverControlHandler(*this);
	MonitorRequestHandler monitorHandler(*this);
	LeakReporter leakReporter(*this);
	WaitRequestHandler waitRequestHandler(*this);
#ifdef DEPLOY_CLEANER
	GILRequestHandler gilRequestHandler(*this);
	GILCleanerHandler gilCleanerHandler(*this);
#else
#endif
	try {
		HTTPServer::ptr_t httpServer = PluginHost::singleton().getInterface<HTTPServer>("HTTPServer");
		httpServer->registerHandler(monitorHandler, "/monitor");
		httpServer->registerHandler(serverControlHandler, "/control");
	} catch (const PluginException& pe) {
		LogError("Caught Exception - Error binding to web server! [" << pe.str() << "]");
	}

	string result;
	if (m_autostart) {
		bool ok = (StartAll(result) == S_OK);
		if (!ok) {
			LogFatal("StartAll() FAILED - " << result);
			m_shutdown = true;
		}
	} else
		LogWarn("SERVER_START_NOT_CONFIG");

#if (DRF_RUNTIME_REPORTER_SERVER > 0)
	// Install Runtime Reporter (runtime begins now, running report is sent)
	RR_Install();
	RuntimeReporter::ProfileStateRunning();
#endif

	// Loop Until Shutdown
	while (!m_shutdown) {
#if (DRF_RUNTIME_REPORTER_SERVER > 0)
		// Send Running Runtime Report Periodically
		static Timer runtimeReportTimer;
		TimeSec timeSec = runtimeReportTimer.ElapsedSec();
		if (timeSec > DRF_RUNTIME_REPORTER_SERVER) {
			runtimeReportTimer.Reset();
			RuntimeReporter::SendReport(RuntimeReporter::REPORT_TYPE_RUNNING);
		}
#endif

		// ESC To Shutdown ?
		HWND hwnd = ::GetConsoleWindow();
		bool esc = ((::GetForegroundWindow() == hwnd) && ((::GetKeyState(VK_ESCAPE) & 0x8000) != 0));
		if (esc) {
			LogFatal("ESC DETECTED - SHUTTING DOWN ...");
			m_shutdown = true;
		}

		// 'shutdown.txt' To Shutdown ?
		if (FileHelper::Exists("\\shutdown.txt")) {
			LogFatal("SHUTDOWN.TXT DETECTED - SHUTTING DOWN ...");
			m_shutdown = true;
		}

		fTime::SleepSec(1.0);
	}

#if (DRF_RUNTIME_REPORTER_SERVER > 0)
	// Stop Runtime Reporter (runtime ends now, stopped report is sent, includes web callers)
	RuntimeReporter::ProfileStateStopped();
	RuntimeReporter::WaitForReportResponseComplete();
#endif

	// Disable Web Callers (included in runtime report)
	WebCaller::DisableInstances();

	// Log App End
	LogFatal("END");
	LOG4CPLUS_DEBUG(Logger::getInstance(L"SHUTDOWN"), "KEPServerImpl::Run() exiting cleanly.");

	return true;
}

#if (DRF_RUNTIME_REPORTER_SERVER > 0)
///////////////////////////////////////////////////////////////////////////////
// RuntimeReporter Support
///////////////////////////////////////////////////////////////////////////////
static KEPServerImpl* pThis = NULL;
using namespace RuntimeReporter;
static void RR_AppCallback(RuntimeReporter::CallbackStruct& cbs) {
	if (pThis)
		pThis->RR_AppCallback(cbs);
}

bool KEPServerImpl::RR_Install() {
	// Install Runtime Reporter
	pThis = this;
	string regKey = REG_KEY_NAME;
	// TODO - Make This Server's Reg Key Unique "KEPServer/<gameId>" ?
	return RuntimeReporter::Install(REG_APP_NAME, regKey, string(NET_RUNTIME), ::KEP::RR_AppCallback);
}

static string RR_WebCallerParams(const string& wcId, const string& paramId) {
	// Build WebCaller Runtime Report Params String
	RuntimeReporterWebParams params;
	WebCaller* pWC = WebCaller::GetInstance(wcId, false);
	if (pWC) {
		params.Add(paramId + ".webCalls", pWC->CallsCompleted());
		params.Add(paramId + ".webCallsMs", pWC->CallTimeMs());
		params.Add(paramId + ".webCallsResponseMs", pWC->ResponseTimeMs());
		params.Add(paramId + ".webCallsBytes", pWC->ResponseBytes());
		params.Add(paramId + ".webCallsErrored", pWC->CallsErrored());
	}
	return std::move(params.GetString());
}

void KEPServerImpl::RR_AppCallback(RuntimeReporter::CallbackStruct& cbs) {
	string params;

	// Append Report Type Parameters
	switch (cbs.type) {
		case REPORT_TYPE_STOPPED:
		case REPORT_TYPE_CRASHED: {
			// Append WebCaller Stats
			params += RR_WebCallerParams("GetBrowserPage", "GBP");
			params += RR_WebCallerParams("KEPAuthV6", "KA6");
			params += RR_WebCallerParams("KEPPingV7", "KP7");
			params += RR_WebCallerParams("RuntimeReporter", "RR");
		} break;
	}

	// Update CallbackStruct
	cbs.params = params;
}
#endif

// helper class for calling initialize on an engine
class EngineInfo {
public:
	EngineInfo(IEnginePtr& e, const char* cf, bool isEnabled = true) :
			engine(e),
			configFile(cf),
			enabled(isEnabled) {
		instanceName = e->InstanceName();
	}

	EngineInfo(const EngineInfo& s) {
		operator=(s);
	}
	EngineInfo& operator=(const EngineInfo& s) {
		if (this != &s) {
			engine = s.engine;
			configFile = s.configFile;
			instanceName = s.instanceName;
		}
		return *this;
	}

	IEnginePtr engine;
	string configFile;
	string instanceName;
	bool enabled;
}; // engine and config file

class EngineThread : public jsThread {
public:
	EngineThread(const string& baseDir, UINT loadPriority, UINT threadID, const KEPServerImpl* server, const std::string& name) :
			jsThread(name.c_str()),
			m_logger(Logger::getInstance(L"Engine")),
			m_loadPriority(loadPriority),
			m_state(ssStopped),
			m_baseDir(baseDir),
			m_shared(false),
			m_nextThd(0),
			m_threadID(threadID),
			m_server(server) {
		m_stateAsString = loadStr(m_state + IDS_BASE_STATE_STR);
	}

	ULONG engineCount() const {
		return m_engines.size();
	}

	bool isShared() const {
		return m_shared;
	}

	UINT getThreadID() const {
		return m_threadID;
	}

	///---------------------------------------------------------
	// add an engine to be run on this thread
	//
	// [in] engine
	//
	void addEngine(IEnginePtr& engine, const char* engineDir) {
		m_engines.push_back(new EngineInfo(engine, engineDir));
		if (m_engines.size() > 1) {
			m_shared = true;
		}
	}

	bool start() {
		m_state = ssStarting;
		return jsThread::start();
	}

	void saveAll() {
		// tell each engine to save
		for (auto i = m_engines.begin(); i != m_engines.end(); i++) {
			(*i)->engine->Save();
		}
	}

	//	bool doAdminActions(const char* objectId, TiXmlNode *node, const string& resultXml);

	~EngineThread() {}

	enum EServerState state() const {
		return m_state;
	}

	UINT loadPriority() const {
		return m_loadPriority;
	}

	void setNextEngine(EngineThread* nextThd) {
		m_nextThd = nextThd;
	}

	const std::vector<EngineInfo*>& engines() const {
		return m_engines;
	}

protected:
	void setState(enum EServerState e) {
		m_state = e;
		m_stateAsString = loadStr(m_state + IDS_BASE_STATE_STR);
	}
	Logger m_logger;
	enum EServerState m_state;
	string m_stateAsString;
	EngineThread* m_nextThd;
	UINT m_loadPriority;

	//Number assigned to identify which thread an engine is on.  Thread id of 0 will always have its own thread.
	//This allows for the control of putting certain threads together to share a thread.
	//It is set in the <Thread></Thread> tag within the DefaultServer.xml or DefaultAIServer.xml file.
	//This is NOT the OS assigned thread id.
	UINT m_threadID;

	virtual ULONG _doWork() {
		AFX_MANAGE_STATE(AfxGetStaticModuleState());

		setState(ssStarting);

		::CoInitialize(0);

		for (auto i = m_engines.begin(); i != m_engines.end() && !isTerminated(); i++) {
			LogInfo(loadStrPrintf(IDS_STARTING_ENGINE, (*i)->engine->InstanceName(), threadID()));
			NDCContextCreator ndc(Utf8ToUtf16((*i)->instanceName));

			string configFile = (*i)->configFile;
			if (!(*i)->engine->Initialize(m_baseDir.c_str(), configFile.c_str(), m_server->GetDBConfig())) {
				LogError("engine->Initialize() FAILED - " << (*i)->engine->Name() << ":" << (*i)->configFile.c_str());
				for (i = m_engines.begin(); i != m_engines.end(); i++)
					(*i)->engine->FreeAll(); // free resources but leave in list so can show its state
				setState(ssFailed);
				return E_INVALIDARG;
			}
			(*i)->instanceName = (*i)->engine->InstanceName();
			SetThreadName((*i)->instanceName.c_str()); // last one wins!
		}

		bool engineCanWait = m_engines.size() == 1; // it can wait if only one on this thread

		setState(ssRunning);

		// serially start each thread
		if (m_nextThd && !isTerminated()) {
			jsAssert(m_nextThd->state() == ssStopped);
			m_nextThd->start();
		}

		// decrement the startup Latch
		// When this and all other engines are started, countDown
		// will no longer block.
		m_server->enginesStartupLatch().countDown();
		while (!isTerminated()) {
			for (auto i = m_engines.begin(); i != m_engines.end() && !isTerminated(); i++) {
				getNDC().push(Utf8ToUtf16((*i)->instanceName));
				(*i)->engine->RenderLoop(engineCanWait);
				getNDC().clear();
			}
		}

		for (auto i = m_engines.begin(); i != m_engines.end(); i++) {
			LogInfo("STOPPING ENGINE - "
					<< (*i)->engine->InstanceName()
					<< " threadId=" << threadID());
		}

		setState(ssStopping);

		FreeAll();

		::CoUninitialize();

		LOG4CPLUS_DEBUG(Logger::getInstance(L"SHUTDOWN"), "KEPServerImpl thread exiting cleanly");

		getNDC().remove();
		return 0;
	}

	void FreeAll() {
		for (auto i = m_engines.begin(); i != m_engines.end(); i++) {
			(*i)->engine->FreeAll();
			(*i)->engine = IEnginePtr();
			delete *i;
		}
		m_engines.erase(m_engines.begin(), m_engines.end());
	}

public:
	Monitored::DataPtr monitor() const {
		LOG4CPLUS_TRACE(Logger::getInstance(L"Monitor"), "KEPServerImpl::EngineThread::Monitor()");
		BasicMonitorData* p = new BasicMonitorData();
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Monitor"), "KEPServerImpl::EngineThread::Monitor(): Iterating engines");
		for (auto i = m_engines.cbegin(); i != m_engines.cend(); i++) {
			BasicMonitorDataPtr pd = (*i)->engine->monitor();
			p->ss << pd->toString();
		}
		Monitored::DataPtr ret(p);
		LOG4CPLUS_TRACE(Logger::getInstance(L"Monitor"), "KEPServerImpl::EngineThread::Monitor(): Call complete!");
		return ret;
	}

protected:
	std::vector<EngineInfo*> m_engines;
	string m_baseDir;
	bool m_shared;

	const KEPServerImpl* m_server;
};

Monitored::DataPtr KEPServerImpl::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();
	LOG4CPLUS_TRACE(Logger::getInstance(L"Monitor"), "KEPServerImpl::monitor()");
	p->ss << "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	p->ss << "<KEPService"
		  << " version=\"" << Application::singleton().version() << "\""
		  << " memory=\"" << Application::singleton().privateBytes() << "\""
		  << ">";
	p->ss << "<KEPServer>";
	p->ss << "<Blades>";
	// KEPServer doesn't keep a collection of the blades that it has loaded, per se,
	// rather it maintains a collection of threads on which those blades are running
	//
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Monitor"), "KEPServerImpl::monitor() Iterating engine threads!");
	for (auto i = m_engineThreads.cbegin(); i != m_engineThreads.cend(); i++) {
		Monitored::DataPtr pd = (*i)->monitor();
		p->ss << pd->toString();
	}
	p->ss << "</Blades>";
	p->ss << "</KEPServer>";
	p->ss << PluginHost::singleton().monitor()->toString();
	p->ss << "</KEPService>";
	LOG4CPLUS_DEBUG(Logger::getInstance(L"Monitor"), "KEPServerImpl::monitor() Call complete!");
	return Monitored::DataPtr(p);
}

#define BLADE_CONFIG_MASK "*.xml"

void KEPServerImpl::Shutdown() {
	log4cplus::Logger logger(Logger::getInstance(L"SHUTDOWN"));
	LOG4CPLUS_TRACE(logger, "KEPServerImpl::ShutDown()");

	StopAll(); // in case we're running

	m_sync.lock();
	m_shutdown = true;
	m_sync.unlock();

	// wake up the blocked accept now that we set m_shutdown
	jsConnection conn;
	conn.connect("localhost", m_adminPort, 2); // short timeout
	conn.close();
}

void KEPServerImpl::Cleanup() {
	log4cplus::Logger logger(Logger::getInstance(L"SHUTDOWN"));
	LOG4CPLUS_TRACE(logger, "KEPServerImpl::Cleanup()");
	string xml = PathAdd(PathXml(), "KGPServer.xml");

	// kill any threads, if they're still running after timeout
	if (m_state != ssStopped)
		GetState(true); // passing true, forces thread to be killed

	// delete PID file
	if (!m_tempDir.empty()) {
		LOG4CPLUS_TRACE(logger, "KEPServerImpl::Cleanup(): TempDir [" << m_tempDir << "]");
		// Look for old PID file from non-clean exit
		string pidFilePath = PathAdd(m_tempDir, getPidFileName(m_instanceId));
		if (jsFileExists(pidFilePath.c_str(), NULL)) {
			if (!DeleteFileW(Utf8ToUtf16(pidFilePath).c_str())) {
				LogError("Error deleting PID file [" << pidFilePath << "]: err=" << GetLastError());
			} else {
				LogInfo("Deleted PID file [" << pidFilePath << "]");
			}
		} else {
			LogError("Error deleting PID file [" << pidFilePath << "]: file not found");
		}
	}

	jsAssert(IsShutdown() && (GetState() == ssStopped || GetState() == ssFailed));
	DELETE_AND_ZERO(m_theServer);
}

static bool EngineGreater(EngineThread* elem1, EngineThread* elem2) {
	return elem1->loadPriority() < elem2->loadPriority();
}

void KEPServerImpl::sortThreads() {
	sort(m_engineThreads.begin(), m_engineThreads.end(), EngineGreater);

	for (auto i = m_engineThreads.begin(); i != m_engineThreads.end();) {
		auto j = i;
		i++;
		if (i == m_engineThreads.end())
			break;

		(*j)->setNextEngine(*i); // link threads together so they start serially
	}
}

class EngineBladesCreator : public Traverser<IKEPConfig*> {
public:
	EngineBladesCreator(KEPServerImpl* server, const string& pathBladesXml) :
			m_server(server), m_pathBladesXml(pathBladesXml) {}

	virtual bool evaluate(IKEPConfig*& obj) {
		m_server->createEngineBlade(obj, m_pathBladesXml);
		return true;
	}

private:
	KEPServerImpl* m_server;

	string m_pathBladesXml;
};

void KEPServerImpl::createEngineBlade(const IKEPConfig* config, const string& pathBladesXml) {
	int threadID = EXCLUSIVE_THREAD; // used to seperate engines between threads (i.e. 1 server engine on thread 0, 2 AI engines on thread 1)
	string className;
	int priority;

	className = config->Get(CLASSNAME_TAG, "");
	threadID = config->Get(THREAD_TAG, threadID); //default to exclusive thread
	priority = config->Get(LOAD_PRIORITY, className == "ServerEngine" ? 1 : 9);

	IEnginePtr pServerEngine = dynamic_pointer_cast<IEngine>(m_bladeFactory.CreateBlade(BC<BaseEngine>(), className.c_str(), ENGINE_VERSION));
	if (pServerEngine) {
		OldCommandLine& cmdLine = Application::singleton().commandLine();
		OldCommandLine::BooleanParameter* paramReadOnlyFS = dynamic_cast<OldCommandLine::BooleanParameter*>(cmdLine.getParameter("-s"));
		bool readOnlyFS = paramReadOnlyFS && (bool)*paramReadOnlyFS;

		// Support relative paths in engine blade configurations
		pServerEngine->SetPathBase(PathXml(), readOnlyFS);

		if (pServerEngine->LoadConfigValues(config)) {
			pServerEngine->SetPathBase(pServerEngine->PathBase(), readOnlyFS);

			if (pServerEngine->Enabled()) {
				// does this engine want its own thread?
				if (threadID == EXCLUSIVE_THREAD) {
					m_engineThreads.push_back(new EngineThread(BaseDir(), priority, threadID, this, className));
					(m_engineThreads.back())->addEngine(pServerEngine, pathBladesXml.c_str());
				} else {
					auto i = m_engineThreads.begin();
					for (; i != m_engineThreads.end(); i++) {
						if ((*i)->getThreadID() == threadID) {
							(*i)->addEngine(pServerEngine, pathBladesXml.c_str());
							break;
						}
					}
					// need to add a new shared thread
					if (i == m_engineThreads.end()) {
						m_engineThreads.push_back(new EngineThread(BaseDir(), priority, threadID, this, className));
						(m_engineThreads.back())->addEngine(pServerEngine, pathBladesXml.c_str());
					}
				}
			} else {
				LogInfo(loadStr(IDS_DISABLED_ENGINE) << pServerEngine->InstanceName());
				// disabled
				m_disabledEngines.push_back(new EngineInfo(pServerEngine, pathBladesXml.c_str(), false));
			}
		} else {
			LogWarn(loadStr(IDS_ENGINE_LOADCONFIG_FAILED) << pathBladesXml.c_str());
		}
	} else {
		LogError(loadStr(IDS_ERR_NO_SERVER) << " " << className);
	}
}

void KEPServerImpl::thump() {
	try {
		PluginHost::singleton().DEPRECATED_getInterface<MetricsAgent>("Metrics")->reportMetric(
			"kgp_server_instance_memory", MetricsAgent::CounterKey(SocketSubSystem().hostName(), _instanceName), MetricsAgent::CounterData(Application::singleton().privateBytes()));
	} catch (PluginException& exc) {
		LogWarn("No Metrics Agent! [" << exc.str() << "]");
	}
}

class ReportEnginesUpTask : public Runnable {
public:
	ReportEnginesUpTask(KEPServerImpl& App, CountDownLatch& latch, Timer timer) :
			_timer(timer), _latch(latch), _app(App) {}

	virtual ~ReportEnginesUpTask() {}

	RunResultPtr run() override {
		LogInstance("STARTUP");
		LogTrace("run()");
		// Wait for the the latch to open.  This will occur when each EngineThread
		// has finished starting TODO: Insure that always occurs regardless of
		// engine startup failure.
		//
		_latch.await();

		try {
			// throws PluginException if not found
			unsigned long gameid = (unsigned long)ObjectRegistry::instance().query("GameId");

			PluginHost::singleton().DEPRECATED_getInterface<MetricsAgent>("Metrics")->reportMetric(
				"world_start", MetricsAgent::CounterKey(SocketSubSystem().hostName(), StringHelpers::parseNumber<unsigned long>(gameid)), MetricsAgent::CounterData((double)_timer.ElapsedMs()));

			// Finally, acquire the HeartBeat generator and add self as a listener.  On the heartbeat,
			// record our private bytes.
			PluginHost::singleton().DEPRECATED_getInterface<HeartbeatGenerator>("Heartbeat")->addHeartbeatListener(&_app);
		} catch (const PluginException& pe) {
			LogError("Caught Exception - Failed to report all engine up [" << pe.str() << "]");
		}

		return RunResultPtr();
	}

private:
	CountDownLatch& _latch;
	Timer _timer;
	KEPServerImpl& _app;
};

// should be in <pathXml>/../Blades (Zerby Distro) -or- <pathXml>/Blades (Bill Distro)
string KEPServerImpl::PathBladesXml() {
	static string path;
	if (path.empty()) {
		static const string pathBladesXml = "Blades";
		path = PathFind(PathAdd(PathXml(), ".."), pathBladesXml);
		if (!PathExists(path, false))
			path = PathFind(PathXml(), pathBladesXml);
	}
	return path;
}

// should be in <pathApp>/../Blades or Bladesd (Zerby Distro) -or- <pathApp>/Blades or Bladesd (Bill Distro)
string KEPServerImpl::PathBladesDll() {
	static string path;
	if (path.empty()) {
#ifdef _DEBUG
		static const string pathBladesDll = "Bladesd";
#else
		static const string pathBladesDll = "Blades";
#endif
		path = PathFind(PathAdd(PathApp(), ".."), pathBladesDll);
		if (!PathExists(path, false)) {
			path = PathFind(PathApp(), pathBladesDll);
			if (!PathExists(path, false)) {
				LogFatal("PATH NOT FOUND - '" << path << "'");
			}
		}
	}
	return path;
}

// should be in <pathXml>/db.cfg
string KEPServerImpl::PathDbCfg() {
	static string path;
	if (path.empty()) {
		static const string pathDbCfg = "db.cfg";
		path = PathFind(PathXml(), pathDbCfg);
		if (!PathExists(path, false)) {
			LogFatal("PATH NOT FOUND - '" << path << "'");
		}
	}
	return path;
}

DWORD KEPServerImpl::StartAll(string& result) {
	DWORD ret = S_OK;

	if (m_state != ssStopped) {
		result = "NOT STOPPED";
		return E_INVALIDARG;
	}

	freeAllDisabledEngines();

	// Find Blade Xml Path
	string pathBladesXml = PathBladesXml();
	LogInfo("pathBladesXml='" << pathBladesXml << "'");

	// load the engine blades
	try {
		// Command line parameter for config logicals
		OldCommandLine::StringParameter* p = dynamic_cast<OldCommandLine::StringParameter*>(Application::singleton().commandLine().getParameter("-D"));
		jsAssert(p != NULL);

		// Load logicals for DB config
		vector<StringPairT> configParams = StringHelpers::parseKeyValuePairsWithOption(*p);
		m_dbConfigFile.impl().paramSet().set(configParams);

		// Find DB Config File Path
		string pathDbCfg = PathDbCfg();
		if (!PathExists(pathDbCfg, false)) {
			LogFatal("PathDbCfg() FAILED - '" << pathDbCfg << "'");
			result = "DB CONFIG NOT FOUND";
			return E_INVALIDARG;
		}
		LogInfo("pathDbCfg='" << pathDbCfg << "'");

		// Load DB config
		try {
			m_dbConfigFile.Load(pathDbCfg.c_str(), DB_CFG_ROOT_NODE);
			if (!m_dbConfigFile.IsLoaded()) {
				LogError("pathDbCfg.IsLoaded() FAILED - '" << pathDbCfg << "'");
			}
		} catch (KEPException* e) {
			LogError("pathDbCfg.IsLoaded() EXCEPTION - '" << pathDbCfg << "' " << e->ToString());
			delete e;
		} catch (KEPException& e) {
			LogError("pathDbCfg.IsLoaded() EXCEPTION - '" << pathDbCfg << "' " << e.ToString());
		}

		string pathBladesDll = PathBladesDll();
		LogInfo("pathBladesDll='" << pathBladesDll << "'");

		// enumerate the XML files from blade dir passed in

		// initialize the blade factory
		m_bladeFactory.Clear(); // in case we're calling it a second time
		m_bladeFactory.Initialize(pathBladesDll.c_str());

		// first look in the database
		BaseEngineConfigLoader configLoader(PathXml(), m_dbConfig);
		BaseEngineConfigContainer configs(configParams);
#ifdef ENABLEBLADEDBCFG
		configLoader.LoadAllDbConfigs(m_instanceId, configs);
#endif
		// load from XML file as override
		string mask = PathAdd(pathBladesXml, "*.xml");

		jsEnumFiles ef(Utf8ToUtf16(mask).c_str());

		// enumerate all the files
		while (ef.next()) {
			try {
				// Use command line parameter -D to specify logical overrides. E.g. Incubator can define \\inc-fs\Server\ServerFiles-%(GAME_ID)
				// in distributioninfo.xml for "source directory", which will be overridden by KGPServer to \\inc-fs\Server\ServerFiles-12345 if
				// -D GAME_ID=12345 is provided in KGPServer command line.

				KEPConfigAdapter* config = new KEPConfigAdapter();
				std::string currentPath = Utf16ToUtf8(ef.currentPath());
				config->Load(currentPath, ENGINE_CONFIG_TAG);
				config->impl().paramSet().set(configParams);
				configs.AddConfig(config, currentPath.c_str());
			} catch (KEPException& e) {
				LogError("Caught Excveption - Error loading blade XML " << Utf16ToUtf8(ef.currentPath()) << " " << e.ToString());
			} catch (KEPException* e) {
				LogWarn("Caught Exception - Error loading blade XML " << Utf16ToUtf8(ef.currentPath()) << " " << e->ToString());
				e->Delete();
			}
		}

		EngineBladesCreator engineBladesCreator(this, pathBladesXml);
		configs.traverse(engineBladesCreator);

		sortThreads();
	} catch (KEPException* e) {
		result = "Caught Exception - Blade Init Error";
		LogError("Caught Exception - Blade Init Error - " << e->ToString());
		e->Delete();
		return E_INVALIDARG;
	}

	if (!m_engineThreads.empty()) {
		_enginesStartupLatch = CountDownLatch(m_engineThreads.size());

		// Queue up a task that will block on the latch above and report the elapsed startup time.
		threadPool().enqueue(RunnablePtr(new ReportEnginesUpTask(
			*this, this->_enginesStartupLatch, this->_startupTimer)));

		m_state = ssStarting;
		if (!(*m_engineThreads.begin())->start()) {
			ret = E_INVALIDARG;
			result = "Thread Start Error";
			LogError("Thread Start Error");
		}
	} else {
		result = "No Servers Error";
		LogError("No Servers Error");
		ret = E_INVALIDARG;
	}

	return ret;
}

DWORD KEPServerImpl::StopAll() {
	LogTrace("");
	DWORD ret = S_OK;

	m_shutdownStartTimestamp = fTime::TimeMs();

	GetState();
	if (m_state == ssRunning || m_state == ssFailed) {
		LogDebug("Running or Failed!");
		m_state = ssStopping;

		for (auto j = m_engineThreads.begin(); j != m_engineThreads.end();) {
			if ((*j)->state() == ssFailed) {
				LogDebug("Erasing failed engine thread[" << (*j)->name() << "]");
				j = m_engineThreads.erase(j);
			} else {
				j++;
			}
		}

		// stop in reverse order, just for fun
		for (auto i = m_engineThreads.rbegin(); i != m_engineThreads.rend(); i++) {
			(*i)->name();
			LogDebug("Stopping engine thread[" << (*i)->name() << "]");
			(*i)->stop(0); // no wait
		}

	} else if (m_state == ssStarting) {
		LogDebug("Starting");
		m_state = ssStopping;

		// stop in reverse order, just for fun
		for (auto i = m_engineThreads.rbegin(); i != m_engineThreads.rend(); i++) {
			LogDebug("Stopping engine thread[" << (*i)->name() << "]");
			(*i)->stop(0); // no wait
		}
	}

	return ret;
}

void KEPServerImpl::freeAllDisabledEngines() {
	for (auto i = m_disabledEngines.begin(); i != m_disabledEngines.end(); i++) {
		if ((*i)) {
			(*i)->engine->FreeAll();
		}
		delete (*i);
	}
	m_disabledEngines.clear();
}

DWORD KEPServerImpl::GetServerFiles(vector<string>& files) {
	DWORD ret = S_OK;

	string mask = PathAdd(PathAdd(BaseDir(), "GameFiles"), "*.svr");

	WIN32_FIND_DATAW findData;
	ZeroMemory(&findData, sizeof(findData));

	HANDLE f = ::FindFirstFileW(Utf8ToUtf16(mask).c_str(), &findData);
	if (f != INVALID_HANDLE_VALUE) {
		do {
			files.push_back(Utf16ToUtf8(findData.cFileName));
		} while (::FindNextFile(f, &findData));
		::FindClose(f);
	}

	return ret;
}

enum EServerState KEPServerImpl::GetState(bool forceStop) {
	if (m_state == ssStopping || forceStop) {
		enum EServerState state = highestThreadState();
		if (state == ssStopped || fTime::ElapsedMs(m_shutdownStartTimestamp) > m_maxTimeoutMs || forceStop) {
			m_state = ssStopped;

			for (auto i = m_engineThreads.begin(); i != m_engineThreads.end(); i++) {
				if ((*i)->isRunning()) {
					LogWarn("Killing Engine - threadId=" << (*i)->threadID());
				}
				LogWarn("Killing Engine [" << (*i)->name() << "]");
				(*i)->stop(1000, true); // should be stopped
				DELETE_AND_ZERO(*i);
			}
			m_engineThreads.clear();

			if (forceStop)
				freeAllDisabledEngines();
		}
	} else if (m_state == ssStarting) {
		// serially start threads now
		if (m_engineThreads.size() > 0) {
			for (auto i = m_engineThreads.begin(); i != m_engineThreads.end(); i++) {
				if ((*i)->state() == ssStarting || (*i)->state() == ssFailed) {
					m_state = (*i)->state();
					goto updateCounters;
				}
			}
			m_state = highestThreadState();
		} else {
			ASSERT(false);
			m_state = ssStopped;
		}
	}

updateCounters:
	// update status counters
	m_threadCount = m_engineThreads.size();
	m_serverCount = 0;

	for (auto i = m_engineThreads.begin(); i != m_engineThreads.end(); i++) {
		m_serverCount += (*i)->engineCount(); // TODO: serialize?
	}

	return m_state;
}

// check all the threads running and return the highest state
enum EServerState KEPServerImpl::highestThreadState() {
	enum EServerState state = ssStopped;

	for (auto i = m_engineThreads.begin(); i != m_engineThreads.end(); i++) {
		enum EServerState thdState = (*i)->state();
		if (!(*i)->isRunning())
			thdState = ssStopped;

		if (thdState > state)
			state = (*i)->state();
	}

	return state;
}

class CIsFromString : public unary_function<char, bool> {
public:
	//Constructor specifying the separators
	CIsFromString::CIsFromString(string const& rostr) :
			m_ostr(rostr) {}
	bool operator()(char c) const;

private:
	string m_ostr;
};

static void deleteOldLogFiles(const char* baseDir) {
	// read the KEPLOG.cfg file, looking for file appenders
	FILE* f = 0;
	if (fopen_s(&f, PathAdd(baseDir, SERVER_LOG_PROP_FILE).c_str(), "r") == 0 && f) {
#define BUFSIZE 4096
		char s[BUFSIZE];
		const char* lookFor = "log4cplus.appender.";
		const char* lookFor2 = ".File=";

		while (fgets(s, BUFSIZE, f)) {
			char* p = strstr(s, lookFor);
			if (p != 0) {
				p += strlen(lookFor);
				p = strstr(p, lookFor2);
				if (p != 0) {
					p += strlen(lookFor2);
					// strip eol
					while (p[strlen(p) - 1] == '\n' || p[strlen(p) - 1] == '\r')
						p[strlen(p) - 1] = '\0';

					// now try to find any files like this with something extra
					// won't be able to delete in-use log files
					strncat_s(p, BUFSIZE - (p - s), ".?", _TRUNCATE);
					jsEnumFiles ef(Utf8ToUtf16(p).c_str());
					while (ef.next()) {
						DeleteFileW(ef.currentPath());
					}
				}
			}
		}
	}
}

bool KEPServerImpl::findEngineName(const char* name) {
	for (auto i = m_disabledEngines.cbegin(); i != m_disabledEngines.cend(); i++) {
		jsAssert((*i) != 0);
		if ((*i)->instanceName == name)
			return true;
	}

	for (auto j = m_engineThreads.begin(); j != m_engineThreads.end(); j++) {
		EngineThread* thd = dynamic_cast<EngineThread*>(*j);
		jsAssert(thd != 0);
		for (auto i = thd->engines().begin(); i != thd->engines().end(); i++) {
			jsAssert((*i) != 0);
			if ((*i)->instanceName == name)
				return true;
		}
	}
	return false;
}

} // namespace KEP