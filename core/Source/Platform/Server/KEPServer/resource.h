///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDS_PROJNAME                    100
#define IDS_STOPPING_ENGINE             101
#define IDS_USER_LOGGING_IN             102
#define IDS_DUPLICATE_ENGINE            103
#define IDS_ERR_NO_SERVER               104
#define IDS_BLADE_INIT_ERR              105
#define IDS_ENGINE_INIT_ERR             106
#define IDS_THD_START_ERR               107
#define IDS_ERR_NO_SERVER_CFG_FILE      108
#define IDS_ERR_NO_CFG_FILE_TAG         109
#define IDS_INFO_SERVER_XML             110
#define IDS_NO_SERVERS                  111
#define IDS_ENGINEINFO_NAME             112
#define IDS_ENGINEINFO_CONFIGFILE       113
#define IDS_ENGINEINFO_CONFIGFILE_HELP  114
#define IDS_ENGINEINFO_INSTANCENAME     115
#define IDS_ENGINEINFO_INSTANCENAME_HELP 116
#define IDS_ENGINEINFO_ENGINE           117
#define IDS_ENGINEINFO_ENGINE_HELP      118
#define IDS_ENGINETHREAD_NAME           119
#define IDS_ENGINETHREAD_BASEDIR        120
#define IDS_ENGINETHREAD_BASEDIR_HELP   121
#define IDS_ENGINETHREAD_SHARED         122
#define IDS_ENGINETHREAD_SHARED_HELP    123
#define IDS_ENGINETHREAD_ENGINES        124
#define IDS_ENGINETHREAD_ENGINES_HELP   125
#define IDS_KEPSERVER_NAME              126
#define IDS_KEPSERVERIMPL_ENGINETHREADS 127
#define IDS_KEPSERVERIMPL_ENGINETHREADS_HELP 128
#define IDS_KEPSERVERIMPL_BASE_DIR      129
#define IDS_KEPSERVERIMPL_BASE_DIR_HELP 130
#define IDS_KEPSERVERIMPL_STARTALL      131
#define IDS_KEPSERVERIMPL_STARTALL_HELP 132
#define IDS_KEPSERVERIMPL_STOPALL       133
#define IDS_KEPSERVERIMPL_STOPALL_HELP  134
#define IDS_KEPSERVERIMPL_SAVEALL       135
#define IDS_KEPSERVERIMPL_SAVEALL_HELP  136
#define IDS_LANG_FILE_FAILED            137
#define IDS_CONFIG_SAVE                 138
#define IDS_COULDNT_FIND_DELETE         139
#define IDS_MISSING_CHILDOBJ            140
#define IDS_BAD_ARRAY_ID                141
#define IDS_MISSING_ARRAY_ID            142
#define IDS_KEPSERVERIMPL_STATE         143
#define IDS_KEPSERVERIMPL_STATE_HELP    144
#define IDS_KEPSERVERIMPL_SERVER_COUNT  145
#define IDS_KEPSERVERIMPL_SERVER_COUNT_HELP 146
#define IDS_KEPSERVERIMPL_THD_COUNT     147
#define IDS_KEPSERVERIMPL_THD_COUNT_HELP 148
#define IDS_ENGINETHREAD_STATE          149
#define IDS_ENGINETHREAD_STATE_HELP     150
#define IDS_SERVER_NOT_STARTED          151
#define IDS_KEPSERVERIMPL_STATE_ENUM    152
#define IDS_KEPSERVERIMPL_STATE_ENUM_HELP 153
#define IDS_DISABLED_ENGINE             154
#define IDS_AUTH_ERROR                  155
#define IDS_ADMIN_SAVE_FAILED           156
#define IDS_ENGINE_LOADCONFIG_FAILED    157
#define IDS_NOT_STOPPED                 158
#define IDS_INVALID_ENGINE_FILENAME     159
#define IDS_NO_AUTH_SERVER              163
#define IDS_KEPSERVERIMPL_SHUTDOWN      164
#define IDS_KEPSERVERIMPL_SHUTDOWN_HELP 165
#define IDS_BAD_ADMIN_DATA              166
#define IDS_BAD_SERVER_XML              167
#define IDS_KEPSERVERIMPL_ADMINTIMEOUT  168
#define IDS_KEPSERVERIMPL_ADMINTIMEOUT_HELP 169
#define IDS_SERVER_STARRT_NOT_CONFIG    170
#define IDS_NO_CLEAN_EXIT               171
#define IDS_CLEAN_EXIT                  172
#define IDS_KEPSERVERIMPL_AUTOSENDREPORT 173
#define IDS_KEPSERVERIMPL_AUTOSENDREPORT_HELP 174
#define IDS_KEPSERVERIMPL_ADMINEMAIL    175
#define IDS_KEPSERVERIMPL_ADMINEMAIL_HELP 176
#define IDS_NO_ADMIN_XML                178
#define IDS_SSSTOPPED                   300
#define IDS_SSRUNNING                   301
#define IDS_SSSTOPPING                  302
#define IDS_SSSTARTING                  303
#define IDS_SSLOCKED                    304
#define IDS_SSFAILED                    305
#define IDS_KILLING_ENGINE              306
#define IDS_STARTING_ENGINE             307

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           152
#endif
#endif
