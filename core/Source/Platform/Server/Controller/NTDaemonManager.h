/******************************************************************************
 NTDaemonManager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <js.h>
#include <string>

#include "KEPHelpers.h"

#include "IDaemonManager.h"

namespace KEP {

/***********************************************************
CLASS

	NTDaemonManager
	
	impl of SCM wrapper

DESCRIPTION

***********************************************************/
class NTDaemonManager : public IDaemonManager {
public:
	virtual bool init();

	virtual bool start(const char* name);

	virtual bool stop(const char* name);

	virtual bool getStatus(const char* name, IDaemonManager::Status& status, string& statusString, ULONG& code);

	virtual bool term();

	virtual string getLastError();

private:
	NTDaemonManager(void);
	virtual ~NTDaemonManager(void);

	void setErrorString(const char* prefix, ULONG errorCode);

	SC_HANDLE m_handle;
	string m_lastError;

	friend class IDaemonManager;
};

} // namespace KEP