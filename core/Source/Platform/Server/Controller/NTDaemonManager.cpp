/******************************************************************************
 NTDaemonManager.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "NTDaemonManager.h"
#include <KEPHelpers.h>
#include "resource.h"

namespace KEP {

IDaemonManager *IDaemonManager::createDaemonManager() {
	return new NTDaemonManager;
}

void IDaemonManager::deleteDaemonManager(IDaemonManager *d) {
	delete d;
}

// map from SC state to IDeamonMgr state
static bool mapState(DWORD state, IDaemonManager::Status &status, string &statusString) {
	switch (state) {
		case SERVICE_STOPPED:
			status = IDaemonManager::stopped;
			break;

		case SERVICE_START_PENDING:
			status = IDaemonManager::starting;
			break;

		case SERVICE_STOP_PENDING:
			status = IDaemonManager::stopping;
			break;

		case SERVICE_RUNNING:
			status = IDaemonManager::running;
			break;

		case SERVICE_CONTINUE_PENDING:
			status = IDaemonManager::contining;
			break;

		case SERVICE_PAUSE_PENDING:
			status = IDaemonManager::pausing;
			break;

		case SERVICE_PAUSED:
			status = IDaemonManager::paused;
			break;

		default:
			return false;
	}

	statusString = loadStr(IDS_SERVICE_CONTINUING + (status - IDaemonManager::contining));

	return true;
}

NTDaemonManager::NTDaemonManager(void) :
		m_handle(0) {
}

NTDaemonManager::~NTDaemonManager(void) {
	term();
}

void NTDaemonManager::setErrorString(const char *prefix, ULONG errorCode) {
	m_lastError = prefix;
	m_lastError += ": ";
	string msg;
	errorNumToString(errorCode, msg);
	m_lastError += msg;
}

bool NTDaemonManager::init() {
	jsAssert(m_handle == 0); // don't call > 1 time

	m_handle = ::OpenSCManager(NULL, SERVICES_ACTIVE_DATABASE, GENERIC_READ | GENERIC_EXECUTE);
	if (m_handle == NULL) {
		setErrorString("OpenSCManager", ::GetLastError());
		return false;
	}

	return true;
}

bool NTDaemonManager::start(const char *name) {
	SC_HANDLE service = ::OpenServiceW(m_handle, Utf8ToUtf16(name).c_str(), SERVICE_START);
	if (service == NULL) {
		setErrorString("OpenService", ::GetLastError());
		return false;
	}

	if (!::StartService(service, 0, 0)) {
		DWORD err = ::GetLastError();
		if (err != ERROR_SERVICE_ALREADY_RUNNING) {
			setErrorString("StartService", err);
			return false;
		}
	}
	return true;
}

bool NTDaemonManager::stop(const char *name) {
	SC_HANDLE service = ::OpenServiceW(m_handle, Utf8ToUtf16(name).c_str(), SERVICE_STOP);
	if (service == NULL) {
		setErrorString("OpenService", ::GetLastError());
		return false;
	}

	SERVICE_STATUS status;
	if (!::ControlService(service, SERVICE_CONTROL_STOP, &status)) {
		DWORD err = ::GetLastError();
		if (err != ERROR_SERVICE_NOT_ACTIVE) {
			setErrorString("ControlService", err);
			return false;
		}
	}
	return true;
}

bool NTDaemonManager::getStatus(const char *name, IDaemonManager::Status &status, string &statusString, ULONG &code) {
	SERVICE_STATUS_PROCESS servStatus;
	DWORD size = 0;

	SC_HANDLE service = ::OpenServiceW(m_handle, Utf8ToUtf16(name).c_str(), GENERIC_READ);
	if (service == NULL) {
		setErrorString("OpenService", ::GetLastError());
		return false;
	}

	if (!::QueryServiceStatusEx(service,
			SC_STATUS_PROCESS_INFO,
			(LPBYTE)&servStatus,
			sizeof(servStatus),
			&size)) {
		setErrorString("QueryServiceStatusEx", ::GetLastError());
		return false;
	}

	if (servStatus.dwWin32ExitCode == ERROR_SERVICE_SPECIFIC_ERROR)
		code = servStatus.dwServiceSpecificExitCode;
	else
		code = servStatus.dwWin32ExitCode;

	return mapState(servStatus.dwCurrentState, status, statusString);
}

bool NTDaemonManager::term() {
	if (m_handle) {
		::CloseServiceHandle(m_handle);
		m_handle = 0;
	}
	return true;
}

string NTDaemonManager::getLastError() {
	return m_lastError;
}

} // namespace KEP
