/******************************************************************************
 IDaemonManager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

/***********************************************************
CLASS

	IDaemonManager
	
	wrapper of the SCM for NT

DESCRIPTION
	an attempt at portablility 

***********************************************************/
class IDaemonManager {
public:
	static IDaemonManager *createDaemonManager();
	static void deleteDaemonManager(IDaemonManager *d);

	enum Status {
		contining,
		pausing,
		paused,
		running,
		stopping,
		starting,
		stopped
	};

	virtual bool init() = 0;

	virtual bool start(const char *name) = 0;

	virtual bool stop(const char *name) = 0;

	virtual bool getStatus(const char *name, IDaemonManager::Status &status, string &statusString, ULONG &code) = 0;

	virtual bool term() = 0;

	virtual string getLastError() = 0;

protected:
	IDaemonManager() {
	}

	virtual ~IDaemonManager() {
	}
};

} // namespace KEP

