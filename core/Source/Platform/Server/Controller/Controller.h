/******************************************************************************
 Controller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "AdminRequestProcessor.h"

namespace KEP {

class IDaemonManager;

/***********************************************************
CLASS

	Controller
	
	class for processing requests for the controller

DESCRIPTION

***********************************************************/
class Controller : public AdminRequestProcessor {
public:
	Controller() :
			AdminRequestProcessor(), m_dmnMgr(0) {}

	virtual bool Init(Logger &logger, const char *bladeXmlDir);

	virtual ~Controller();

protected:
	virtual bool processRequest(const char *objectId, const char *cookie, TiXmlElement *node, string &resultXml);

	// called when doing something like checking password, this will refresh settings from disk
	virtual bool reloadConfig();

private:
	string m_serviceName;
	IDaemonManager *m_dmnMgr;
	string m_baseDir;
	string m_gameBaseDir; // where to get my XML file from
};

} // namespace KEP

