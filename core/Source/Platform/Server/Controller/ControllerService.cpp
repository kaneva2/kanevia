/******************************************************************************
 ControllerService.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "windows.h"
#include "tchar.h"

#include <NTService/NTService.h>
#include <tinyXML/tinyxml.h>

#include <KEPHelpers.h>

#include <js.h>
#include <jsEvent.h>

#include "Controller.h"
using namespace KEP;
#include "resource.h"
#include "../KEPService/ServiceStrings.h"
#include <log4cplus/logger.h>
using namespace log4cplus;

#include <fstream>
#include <string>
using namespace std;

#include "common/keputil/Application.h"
#include "common/keputil/Plugin.h"
#include "common/keputil/PluginHostConfigurator.h"
#include "common/keputil/CommandLine.h"

#include "common\KEPUtil\Helpers.h"
static LogInstance("Instance");

// log4cplus config parameters, use different one otherwise
// prevents log rollover
static const char* LOG_PROP_FILE = "ControllerLog.cfg";
static ULONG LOG_PROP_REFRESH_SEC = 10;

/***********************************************************
CLASS

	CControllerService
	
	the KEP version of an NT service

DESCRIPTION
	Inits, runs the service

***********************************************************/
class CControllerService : public CNTService {
public:
	CControllerService() :
			CNTService(loadStr(IDS_CONTROLLER, NULL, ::GetModuleHandle(0)).c_str(), loadStr(IDS_SERVICE_DESC, NULL, ::GetModuleHandle(0)).c_str()),
			m_controller(),
			m_configureThread(0) {
	}

	virtual void Run();
	// virtual OnInit gets passed args from caller (Start Parmeters from control panel or StartServer from another exe)
	// _tmain gets args registered on command line of service
	// we want the latter, so don't override MyInit any more
	BOOL MyInit(OldCommandLine& cl);
	virtual void OnStop();
	Logger& ControllerLogger(bool* failed = 0);

private:
	ConfigureAndWatchThread* m_configureThread;

	Controller m_controller;
	string m_gameBaseDir;

	void UpdateStatus(ULONG newState, ULONG waitHint = 500) {
		LOG4CPLUS_TRACE(ControllerLogger(), "CControllerServer::UpdateStatus()");
		LOG4CPLUS_DEBUG(ControllerLogger(), "CNTService::m_hServiceStatus=" << CNTService::GetServiceStatusHandle());

		CNTService::UpdateStatus(newState, waitHint);
	}
};

///---------------------------------------------------------
// Helper to get the ControllerLogger
//
// [Returns]
//    the logger
Logger& CControllerService::ControllerLogger(bool* failed) {
	// since apartment threaded, we're ok to not serialize
	static Logger m_logger = Logger::getInstance(L"Controller");
	string baseDirWithSlash = PathAdd(m_gameBaseDir.c_str(), "");

	if (m_configureThread == 0) {
		// for loading strings
		StringResource = ::GetModuleHandle(NULL);

		string propFile = baseDirWithSlash + LOG_PROP_FILE;

		m_configureThread = new ConfigureAndWatchThread(Utf8ToUtf16(propFile), LOG_PROP_REFRESH_SEC * 1000);
		::Sleep(10);

		if (failed != 0) {
			// if we have no appenders, assume there was an error
			*failed = m_logger.getAllAppenders().size() == 0;
		}

		LOG4CPLUS_INFO(Logger::getInstance(L"STARTUP"), "******************************************");
		LOG4CPLUS_INFO(Logger::getInstance(L"STARTUP"), "**************** BEGIN RUN ***************");
		LOG4CPLUS_INFO(Logger::getInstance(L"STARTUP"), "******************************************");

		LOG4CPLUS_INFO(m_logger, loadStr(IDS_DIR_SET) << baseDirWithSlash);
		LOG4CPLUS_INFO(m_logger, loadStr(IDS_LOG_CONFIG) << propFile); // Logs as kepserver, s/b/ controllerservice
	} else if (failed != 0) {
		*failed = false;
	}

	return m_logger;
}

void CControllerService::Run() {
	LOG4CPLUS_TRACE(ControllerLogger(), "CControllerServer::Run()");
	UpdateStatus(SERVICE_RUNNING);

	// ProcessRequests will not return until the run is complete.  So we need
	// to set up our PluginHost right here.
	//

	// start the listener thread
	if (m_controller.Init(ControllerLogger(), m_gameBaseDir.c_str())) {
		m_controller.ProcessRequests();
	}
}

BOOL CControllerService::MyInit(OldCommandLine& cl) {
	bool failed = false;
	std::ostringstream os;
	if (cl.orphans().size() > 1) {
		// Only one orphaned parameter allow
		// throw command line exception
		return failed;
	}

	OldCommandLine::BooleanParameter* RunAsExecutable = dynamic_cast<OldCommandLine::BooleanParameter*>(cl.getParameter("-r"));
	bool interactive = RunAsExecutable && (bool)*RunAsExecutable;

	std::stringstream ss;
	ss << "Runmode: "
	   << (interactive ? "Interactive" : "Service")
	   << ". ";
	os << ss.str().c_str();

	if (cl.orphans().size() == 1) {
		m_gameBaseDir = cl.orphans()[0].c_str();
		os << "Supplied base directory: " << m_gameBaseDir;

	} else {
#if 1 // TODO - Link KEPUtil
		wchar_t myDir[_MAX_PATH];
		myDir[0] = L'\0';
		::GetModuleFileNameW(NULL, myDir, _MAX_PATH);
		wstring baseDir = myDir;
		wstring::size_type len = baseDir.rfind(L'\\');
		baseDir.resize(len);
		m_gameBaseDir = Utf16ToUtf8(baseDir);
#else
		// DRF - Added - No trailing slash?
		m_gameBaseDir = PathApp();
		LogInfo("gameBaseDir='" << m_gameBaseDir << "'");
#endif
		os << "No base directory suppled.  Defaulting to " << m_gameBaseDir;
		;
	}

	ControllerLogger(&failed);

	if (failed)
		LogEvent(EVENTLOG_ERROR_TYPE, EVMSG_ERR, loadStr(IDS_CHECK_LOGS).c_str());
	else if (os.str().length() > 0)
		LOG4CPLUS_INFO(ControllerLogger(), os.str());

	return !failed;
}

void CControllerService::OnStop() {
	m_controller.StopProcessing();
	PluginHost::singleton().shutdown();
}

CControllerService service;

// Ctrl+C, etc. handler when run from command line
extern "C" BOOL CtrlHandler(DWORD fdwCtrlType) {
	jsOutputToDebugger("CtrlHandler got type of %d\n", fdwCtrlType);

	service.OnStop();

	switch (fdwCtrlType) {
		// Handle the CTRL-C signal.
		case CTRL_C_EVENT:
		case CTRL_CLOSE_EVENT:
			return TRUE;

		// Pass other signals to the next handler.
		case CTRL_SHUTDOWN_EVENT:
			fprintf(stderr, "Shutting down due to system shutdown.\n");
			return FALSE;

		case CTRL_LOGOFF_EVENT:
			fprintf(stderr, "Shutting down due to user logoff.\n");
			return FALSE;

		case CTRL_BREAK_EVENT:
			return FALSE;

		default:
			return FALSE;
	}
}

int wmain(int argc, wchar_t* argv[]) {
	// Get a shorthand reference to the application's command line parser.
	//
	OldCommandLine& cl = Application::singleton().commandLine();
	OldCommandLine::BooleanParameter& RunAsExecutable = cl.addBoolean("-r", false, "Run interactive.  Not as service."); //"run mode: application" )
	/*CommandLine::StringParameter&  ConfigurationFile	=*/cl.addString("-c", "kgpcontroller.conf", "Full path and name of configuration file");
	CommandLineUtf8 cmdLineUtf8(argc, argv);
	cl.parse(cmdLineUtf8.GetArgC(), cmdLineUtf8.GetArgV());

	// I need to be able to flex the command line...and this logic isn't very forgiving or flexible
	// Time for CommandLine
	// are we to run on command line?
	if ((bool)RunAsExecutable) {
		// add Ctrl+C handler
		jsVerify(::SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE) != FALSE);

		if (service.MyInit(cl)) // argc, argv ) )
			service.Run();

		// remove Ctrl+C handler
		jsVerify(::SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, FALSE) != FALSE);
	} else {
		// Parse for standard arguments (install, uninstall, version etc.)
		if (!service.ParseStandardArgs(cmdLineUtf8.GetArgC(), cmdLineUtf8.GetArgV())) {
			if (service.MyInit(cl)) {
				//			if ( service.MyInit( argc, argv ) )			{
				// Didn't find any standard args so start the service
				// Uncomment the DebugBreak line below to enter the debugger
				// when the service is started.
				service.StartService();
			}
		}
	}

	// When we get here, the service has been stopped
	return service.GetServiceStatus().dwWin32ExitCode;
}
