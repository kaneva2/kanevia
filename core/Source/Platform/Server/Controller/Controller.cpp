/******************************************************************************
 Controller.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "common/include/kepcommon.h"
#include <js.h>
#include <jsEnumFiles.h> // for jsFileExists
#include <KEPConfig.h>
#include <KEPHelpers.h>
#include <KEPException.h>
//#include <mdump/mdump.h>
#include "IDaemonManager.h"
using namespace KEP;

#include "common/keputil/application.h"

#include "Controller.h"
#include "resource.h"
#include "../KEPService/ServiceStrings.h"

#include "common\KEPUtil\Helpers.h"

static const char* const OLD_XML_FILE_NAME = "KEPController.xml";
static const char* const XML_FILE_NAME = "KGPController.xml";
static const char* const OLD_KEPSERVER_FILE_NAME = "KEPServer.xml";
static const char* const KEPSERVER_FILE_NAME = "KGPServer.xml";

// xml config tags
static const char* const ADMIN_USER_TAG = "AdminUser";
static const char* const ADMIN_HASH_TAG = "AdminHash";
static const char* const ROOT_TAG = "KGPController";
static const char* const KEPSERVER_ROOT_TAG = "KGPServer";
static const char* const CONTROLLER_PORT_TAG = "ControllerPort";
static const char* const KEPSERVICE_NAME_TAG = "ServiceName";
static const char* const ADMIN_TIMEOUT_TAG = "AdminCookieTimeoutSec";
static const char* const DEBUG_PREFIX_TAG = "DebugControllerFilePrefix";
static const char* const SOCKET_TIMEOUT_TAG = "SocketTimeoutSec";

// default config values
static const UINT DEFAULT_CONTROLLER_PORT = 8988;
static const char* const DEFAULT_KEPSERVICE_NAME = "KGPServer";
static const int DEFAULT_ADMIN_TIMEOUT = 600;
static const int DEFAULT_SOCKET_TIMEOUT = 3;

static const int CONTROLLER_START_SERVICE = 2000;
static const int CONTROLLER_STOP_SERVICE = 2001;
static const int CONTROLLER_STATUS_STRING = 2002;
static const int CONTROLLER_STATUS = 2003;
static const int CONTROLLER_CLEAN_EXIT = 2004;
static const int CONTROLLER_EXIT = 2005;
static const int CONTROLLER_EXIT_STRING = 2006;

static const int CONTROLLER_GET_STATUS = 17348; // GetSet's GetData Id

bool Controller::reloadConfig() {
	// see if we have an XML config file
	// should be at "c:\stage\kep_prod.pv\KGPController.xml"
	string pathXml = PathAdd(m_gameBaseDir, XML_FILE_NAME);
	LogInfoTo(logger(), "pathXml='" << pathXml << "'");
	LOG4CPLUS_INFO(logger(), "Loading '" << pathXml << "'");

	// set defaults
	m_port = (int)DEFAULT_CONTROLLER_PORT;
	m_serviceName = DEFAULT_KEPSERVICE_NAME;
	m_adminTimeoutSec = DEFAULT_ADMIN_TIMEOUT;
	m_debugAdminFile = "";
	m_socketTimeoutMs = DEFAULT_SOCKET_TIMEOUT * 1000;

	//	bool isDir				= false;
	//	if ( !(jsFileExists( xml.c_str(), &isDir ) && !isDir))				{
	if (!PathExists(pathXml)) {
		LOG4CPLUS_WARN(logger(), loadStrPrintf(IDS_NO_SVC_CONFIG, pathXml.c_str()));
		return true;
	}
	try {
		Application::singleton().loadConfiguration(pathXml.c_str(), ROOT_TAG);

		const KEPConfig& cfg = Application::singleton().configuration();

		// I gather that we used two configuration files to reduce the redundancy of
		// specifying the admin credentials.  This would be better handled via xml
		// includes as the current model couples the controller config with the game
		// server config.  E.g. admin.xml contains the credentials, and this file is
		// included in kgpserver.xml and kgpcontroller.xml
		//
		// we get these from KGPController.xml
		//
		m_port = cfg.Get(CONTROLLER_PORT_TAG, (int)DEFAULT_CONTROLLER_PORT);
		m_serviceName = cfg.Get(KEPSERVICE_NAME_TAG, DEFAULT_KEPSERVICE_NAME);
		m_adminTimeoutSec = cfg.Get(ADMIN_TIMEOUT_TAG, DEFAULT_ADMIN_TIMEOUT);
		m_debugAdminFile = cfg.Get(DEBUG_PREFIX_TAG, "");
		m_socketTimeoutMs = cfg.Get(SOCKET_TIMEOUT_TAG, DEFAULT_SOCKET_TIMEOUT) * 1000;

		// we get these from KGPServer.xml
		//
		m_adminUser = cfg.Get(ADMIN_USER_TAG, "Admin");
		m_adminUserHash = cfg.Get(ADMIN_HASH_TAG, "");
	} catch (KEPException* e) {
		e->Delete(); //
		LOG4CPLUS_WARN(logger(), loadStr(IDS_NO_SVC_CONFIG));
	}

	return true;
}

// read the config for settings
bool Controller::Init(Logger& log, const char* baseGameDir) {
	m_gameBaseDir = baseGameDir;
	AdminRequestProcessor::Init(log);
	m_dmnMgr = IDaemonManager::createDaemonManager();
	jsAssert(m_dmnMgr != 0);
	jsVerify(m_dmnMgr->init());

	// DRF - Added - Think it needs the trailing slash
	static LogInstance("Instance");
	m_baseDir = PathAdd(PathApp(), "\\");
	LogInfo("baseDir='" << m_baseDir << "'");

	// Note that reloading the configuration is only used by AdminRequestProcessor to
	// renew admin credentials when the creds cookie disappears.  However, we will not
	// reconfigure the plugin host.
	//
	reloadConfig();
	LOG4CPLUS_INFO(logger(), loadStr(IDS_CONFIG) << m_port << ", " << m_serviceName << ", " << m_adminTimeoutSec);
	return true;
}

Controller::~Controller() {
	if (!m_dmnMgr)
		return;
	m_dmnMgr->term();
	IDaemonManager::deleteDaemonManager(m_dmnMgr);
}

// avoid including GetSet, but fake it out
static void appendFakeGetSet(string& result, int type, int id, UINT nameId, UINT helpId, const char* value, bool enabled = true, bool isStatus = false) {
	result += "<GetSetRec name=\"";
	result += loadStr(nameId);
	result += "\" type=\"";
	result += numToString(type, "%d");
	result += "\" flags=\"131072\" id=\""; // 0x20000 = expose in GetSet terms
	result += numToString(id, "%d");
	result += "\" help=\"";
	result += loadStr(helpId);
	if (isStatus)
		result += "\" groupId=\"-1";
	result += "\">";
	result += value;
	if (!enabled)
		result += "<Enabled>0</Enabled>";

	result += "</GetSetRec>\n";
}

bool Controller::processRequest(const char* objectId, const char* cookie, TiXmlElement* actionNode, string& resultXml) {
	bool ret = false;
	HRESULT hr = S_FALSE;
	string result;

	jsVerifyDo(m_dmnMgr != 0 && objectId != 0 && actionNode != 0, return false);

	// for controller requests, objectId must always be 0, and the action
	if (::strcmp(objectId, "0") == 0) {
		int id;
		//int depth = 1000;

		// see what specific item the node is
		if (actionNode->QueryIntAttribute("id", &id) != TIXML_SUCCESS) {
			jsAssert(false);
			return false;
		}

		LOG4CPLUS_DEBUG(logger(), "Got controller Action of id " << id);

		// see which action it is
		switch (id) {
			case CONTROLLER_START_SERVICE:
				if (!m_dmnMgr->start(m_serviceName.c_str()))
					result = m_dmnMgr->getLastError();
				hr = S_OK;
				break;

			case CONTROLLER_STOP_SERVICE:
				if (!m_dmnMgr->stop(m_serviceName.c_str()))
					result = m_dmnMgr->getLastError();
				else
					hr = S_OK;
				break;

			case CONTROLLER_GET_STATUS: {
				ULONG code = 0;
				IDaemonManager::Status status;
				string statusString;
				if (m_dmnMgr->getStatus(m_serviceName.c_str(), status, statusString, code)) {
					string msg;

					// create a GetSetRec-type could include GetSetRec.cpp and Administered.cpp but we want to keep this light
					// first add the two actions
					result.clear();
					appendFakeGetSet(result, 205, CONTROLLER_START_SERVICE, IDS_START_SERVICE, IDS_START_SERVICE_HELP, "", status == IDaemonManager::stopped);
					appendFakeGetSet(result, 205, CONTROLLER_STOP_SERVICE, IDS_STOP_SERVICE, IDS_STOP_SERVICE_HELP, "", status != IDaemonManager::stopped);

					// now add read-only properties.
					// use bogus type of 99 for "hidden" fields since admin UI no longer honors the hidden flag
					appendFakeGetSet(result, 15, CONTROLLER_STATUS_STRING, IDS_CONTROLLER_STATUS, IDS_CONTROLLER_STATUS_HELP, statusString.c_str(), true, true);
					appendFakeGetSet(result, 15, CONTROLLER_EXIT_STRING, IDS_CONTROLLER_EXIT, IDS_CONTROLLER_EXIT_HELP, ::errorNumToString(code, msg), true, true);

					// number values for accurate comparing
					appendFakeGetSet(result, 99, CONTROLLER_STATUS, IDS_CONTROLLER_STATUS, IDS_CONTROLLER_STATUS_HELP, numToString(status, "%d").c_str());
					appendFakeGetSet(result, 99, CONTROLLER_EXIT, IDS_CONTROLLER_EXIT, IDS_CONTROLLER_EXIT_HELP, numToString(code, "%d").c_str());

					// I think exit code will get us this
					//result += "<GetSetRec name=\"Clean exit\" type=\"1\" flags=\"131072\" id=\"" + numToString( CONTROLLER_CLEAN_EXIT, "%d" ) + "\" help=\"If stopped, the 'clean exit' code of server.\">" + numToString( (int)cleanExit, "%d" ) + "</GetSetRec>\n";
					hr = S_OK;
				} else {
					result = m_dmnMgr->getLastError();
				}
			} break;

			default:
				hr = HRESULT_FROM_WIN32(ERROR_NOT_FOUND);
				break;
		}

	} else {
		hr = HRESULT_FROM_WIN32(ERROR_NOT_FOUND);
	}

	char hrAsString[32];
	resultXml = "<ActionResult rc=\"";
	_ltoa_s(hr, hrAsString, _countof(hrAsString), 10);
	resultXml += hrAsString;
	resultXml += "\" cookie=\"";
	resultXml += cookie;
	resultXml += "\">";

	if (FAILED(hr)) {
		if (result.empty()) {
			string strError;
			errorNumToString(hr, strError);
			resultXml += strError;
		} else {
			resultXml += result;
		}
	} else {
		resultXml += result;
	}
	resultXml += "</ActionResult>";

	return ret;
}
