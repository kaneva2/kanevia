/******************************************************************************
 resource.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Controller.rc
//
#define IDS_APP_TITLE                   103
#define IDS_SERVICE_DESC                108
#define IDS_BAD_DATA                    113
#define IDS_SHUTDOWN                    114
#define IDS_EXIT                        115
#define IDS_CONFIG                      116
#define IDS_NO_XML                      118
#define IDS_SERVICE_CONTINUING          122
#define IDS_SERVICE_PAUSING             123
#define IDS_SERVICE_PAUSED              124
#define IDS_SERVICE_RUNNING             125
#define IDS_SERVICE_STOPPING            126
#define IDS_SERVICE_STARTING            127
#define IDS_SERVICE_STOPPED             128
#define IDS_START_SERVICE               129
#define IDS_START_SERVICE_HELP          130
#define IDS_STOP_SERVICE                131
#define IDS_STOP_SERVICE_HELP           132
#define IDS_CONTROLLER_STATUS           133
#define IDS_CONTROLLER_STATUS_HELP      134
#define IDS_CONTROLLER_EXIT             135
#define IDS_CONTROLLER_EXIT_HELP        136

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
