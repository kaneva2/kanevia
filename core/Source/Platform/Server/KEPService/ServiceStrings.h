///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDS_CHECK_LOGS 901
#define IDS_LOG_CONFIG 902
#define IDS_DIR_SET 903
#define IDS_KEPSERVICE 904
#define IDS_KEPSERVICE_DESC 905
#define IDS_NO_SVC_CONFIG 906
#define IDS_BIND_PORT 907
#define IDS_ERR_BIND 908
#define IDS_ERR_LISTEN 909
#define IDS_XML_ERROR 910
#define IDS_AUTH_EXPIRED 911
#define IDS_NO_AUTH_INFO 912
#define IDS_AUTH_DENIED 913
#define IDS_CONTROLLER 914
#define IDS_REDIR_ERROR 915
#define IDS_LOG_REDIR 916
#define IDS_LOG_FILE_DESC_SERVER 917
#define IDS_BIND_PORT_ZERO 918
#define IDS_STRING919 919
#define IDS_LOADING_CONFIG 919

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE 101
#define _APS_NEXT_COMMAND_VALUE 40001
#define _APS_NEXT_CONTROL_VALUE 1001
#define _APS_NEXT_SYMED_VALUE 101
#endif
#endif
