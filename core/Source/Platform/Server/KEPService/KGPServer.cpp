///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "windows.h"
#include "Common/KEPUtil/Helpers.h"
#include "common/KEPUtil/Application.h"
#include "common/keputil/commandline.h"
#include "common/KEPUtil/VLDWrapper.h" // virtual leak detector
#include "Common\KEPUtil\CrashReporter.h"
#if (DRF_RUNTIME_REPORTER_SERVER > 0)
#include "Common\KEPUtil\RuntimeReporter.h"
#endif
#include "NTService/NTService.h"
#include "ServiceStrings.h"
#include "jsThread.h"
#include "resource.h"
#include "../KEPServer/resource.h" // for action ids
#include "../KEPServer/KEPServerEnums.h"
#include "SetThreadName.h"

namespace KEP {

extern "C" __declspec(dllimport) bool KEPServer_Run(const std::string& pathXml, int instanceId);
extern "C" __declspec(dllimport) EServerState KEPServer_GetState();
extern "C" __declspec(dllimport) void KEPServer_Shutdown();
extern "C" __declspec(dllimport) void KEPServer_Cleanup();
extern "C" __declspec(dllimport) ULONG KEPServer_GetShutdownTimeoutMs();
__declspec(dllimport) Monitored::DataPtr KEPServer_monitor();

static Logger logger = Logger::getInstance(L"KEPServer");

static const char* STDOUT_REDIR = "server_stdout.txt";
static const char* STDERR_REDIR = "server_stderr.txt";

extern "C" BOOL CtrlHandler(DWORD fdwCtrlType);

/***********************************************************
CLASS

	CKEPService
	
	the KEP version of an NT service

DESCRIPTION
	Inits, runs the service

***********************************************************/
class CKEPService : public CNTService
#ifdef DEPLOY_MONITORED
		,
					public Monitored
#endif
{
public:
	CKEPService() :
			CNTService(loadStr(IDS_KEPSERVICE, NULL, ::GetModuleHandle(0)).c_str(), loadStr(IDS_KEPSERVICE_DESC, NULL, ::GetModuleHandle(0)).c_str()),
			m_configureThread(0),
			m_stdOut(0),
			m_stdErr(0),
			m_isService(true),
			m_instanceId(1),
			m_logger(Logger::getInstance(L"API")) {
		//m_kepDeps = "";
		//m_dependencies = m_kepDeps.c_str();
		AddControlsAccepted(SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN);
	}

	~CKEPService() {
		Logger::shutdown();
		delete m_configureThread;
		_fcloseall();
	}

	void StartedInteractive() { m_isService = false; }
	virtual void Run();

	// virtual OnInit gets passed args from caller (Start Parmeters from control panel or StartServer from another exe)
	// _tmain gets args registered on command line of service
	// we want the latter, so don't override OnInit any more
	BOOL InitCmdLine(OldCommandLine& cmdLine);
	BOOL InitLogger();

	virtual void OnStop();
	virtual void OnShutdown();

	Monitored::DataPtr CKEPService::monitor() const;

private:
	ConfigureAndWatchThread* m_configureThread;

	//std::string 	m_kepDeps;

	void RegisterEnvironmentVariables(const std::vector<StringPairT>& envVars);

	FILE* m_stdOut;
	FILE* m_stdErr;
	bool m_isService;
	int m_instanceId;
	Logger m_logger;

public:
	// This is the path to the configuration files, specifically KGPServer.xml and db.cfg.
	// It is also set as the baseDir for all loaded engine blades.
	// By default this is set to PathApp() but may be overridden by a path on the command line.
	std::string m_pathXml;
	std::string PathXml() const { return m_pathXml; }
	bool SetPathXml(const std::string& path) {
		if (!PathExists(path, false))
			return false;
		m_pathXml = path;
		return true;
	}

	int InstanceId() { return m_instanceId; }

	// CrashReporter Support
	bool CR_Install();
	void CR_AppCallback(CrashReporter::CallbackStruct& cbs);
};

static CKEPService* pThis = NULL; // singleton stattc instance

Monitored::DataPtr CKEPService::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();
	p->ss << "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	p->ss << "<KEPService>";
	p->ss << KEPServer_monitor()->toString();
	p->ss << "</KEPService>";
	return Monitored::DataPtr(p);
}

void CKEPService::Run() {
	KEPServer_Run(PathXml(), m_instanceId);
	ULONG SERVER_WAIT_TIMEOUT = KEPServer_GetShutdownTimeoutMs();

	Timer timer;
	enum EServerState lastState;
	do {
		UpdateStatus(SERVICE_STOP_PENDING, 5000);
		fTime::SleepMs(400);
	} while ((lastState = KEPServer_GetState()) == ssStopping && timer.ElapsedMs() < SERVER_WAIT_TIMEOUT * 1000);
	// shouldn't take too long to shutdown

	UpdateStatus(SERVICE_STOP_PENDING, 1000);

	LogDebug("Exiting Run with state of " << (int)lastState);

	KEPServer_Cleanup();

	UpdateStatus(SERVICE_STOP_PENDING, 1000);

	::CoUninitialize();

	UpdateStatus(SERVICE_STOP_PENDING, 1000);
}

static std::string PathLogCfg(const std::string& pathSearch) {
	static std::string path;
	if (path.empty())
		path = PathFind(pathSearch, SERVER_LOG_PROP_FILE, false);
	return path;
}

BOOL CKEPService::InitCmdLine(OldCommandLine& cmdLine) {
	::CoInitialize(0);

	// Set environment variables with command line
	// Use -D switch and prefix variable name with $ sign. See StringHelpers::parseKeyValuePairsWithOption for more detail.
	// E.g. -D $GAME_ID=12345
	OldCommandLine::StringParameter* p = dynamic_cast<OldCommandLine::StringParameter*>(Application::singleton().commandLine().getParameter("-D"));
	jsAssert(p != NULL);
	if (p != NULL) {
		std::vector<StringPairT> envVars = StringHelpers::parseKeyValuePairsWithOption(*p, '$');
		RegisterEnvironmentVariables(envVars);
	}

	// for loading strings
	StringResource = ::GetModuleHandle(NULL);

	if (!cmdLine.orphans().empty()) {
		std::string cmdPathXml = cmdLine.orphans()[0];
		if (!SetPathXml(cmdPathXml)) {
			fprintf(stderr, __FUNCTION__ ": SetPathXml() FAILED - '%s'\n", cmdPathXml.c_str());
		}

		if (!m_isService) {
			// interactive

			if (cmdLine.orphans().size() > 1) {
				//Quick way to define a new instance for a server loading configs
				//from the database.  This would be done in the case that 2 engines
				//of the same type are running on the same box.  Mostly a dev feature.
				int instanceId = atoi(cmdLine.orphans()[1].c_str());
				if (instanceId > 0) {
					m_instanceId = instanceId;
				}
			}
		}
	}

	// Get App Path (working directory of app)
	std::string pathApp = PathApp(false);
	fprintf(stdout, __FUNCTION__ ": pathApp='%s'\n", pathApp.c_str());

	// Default Base Dir Is App Dir (can be overridden and should be added to PathFind())
	if (PathXml().empty())
		SetPathXml(pathApp);
	fprintf(stdout, __FUNCTION__ ": pathXml='%s'\n", PathXml().c_str());

	// Get Log Config File Path
	std::string pathLogCfg = PathLogCfg(PathXml());
	fprintf(stdout, __FUNCTION__ ": pathLogCfg='%s'\n", pathLogCfg.c_str());

	// Add Crash Report Attachments (${GAME_ID} -> instanceId)
	CrashReporter::AddLogFiles(pathLogCfg, "Server Log");
	CrashReporter::SetErrorData("PathApp", pathApp);
	CrashReporter::SetErrorData("PathXml", PathXml());
	CrashReporter::SetErrorData("InstanceId", m_instanceId);

	return TRUE;
}

BOOL CKEPService::InitLogger() {
	if (m_configureThread == 0) {
		// for loading strings
		StringResource = ::GetModuleHandle(NULL);

		// Reassign stderr and stdout to catch output
		if (m_stdOut == NULL && m_stdErr == NULL && m_isService) {
			// 1/09 add code to keep all old files since small, this is ok
			time_t curTime;
			struct tm locTime;
			char stampBuf[32];

			//create time stamp
			time(&curTime);
			localtime_s(&locTime, &curTime);

			strftime(stampBuf, _countof(stampBuf), ".%y%m%d%H%M.txt", &locTime);

			std::string fileStdErr = PathAdd(PathXml(), STDERR_REDIR);
			std::string fileStdErrStamp = fileStdErr + stampBuf;
			rename(fileStdErr.c_str(), fileStdErrStamp.c_str());

			std::string fileStdOut = PathAdd(PathXml(), STDOUT_REDIR);
			std::string fileStdOutStamp = fileStdOut + stampBuf;
			rename(fileStdOut.c_str(), fileStdOutStamp.c_str());

			freopen_s(&m_stdErr, fileStdErr.c_str(), "w", stderr);
			freopen_s(&m_stdOut, fileStdOut.c_str(), "w", stdout);

			if (m_stdOut == NULL || m_stdErr == NULL) {
				MessageBeep(0xffffffff);
				LogEvent(EVENTLOG_ERROR_TYPE, EVMSG_ERR, loadStrPrintf(IDS_REDIR_ERROR, fileStdErr.c_str(), fileStdOut.c_str()).c_str());
			}
		}

		// Get Log Config File Path
		std::string pathLogCfg = PathLogCfg(PathXml());

		// Start Logger Thread (10 sec refresh)
		m_configureThread = new ConfigureAndWatchThread(Utf8ToUtf16(pathLogCfg), 10 * 1000);
		::Sleep(10); // give it a chance to start

		if (!m_isService) {
			fprintf(stderr, "Press Ctrl+Break -or- hold ESC to exit\n");
		}
	}

	return TRUE;
}

void CKEPService::OnStop() {
	LogDebug("Got Stop");
	KEPServer_Shutdown();
}

void CKEPService::OnShutdown() {
	LogDebug("Got Shutdown");
	KEPServer_Shutdown();
}

void CKEPService::RegisterEnvironmentVariables(const std::vector<StringPairT>& envVars) {
	for (auto it = envVars.cbegin(); it != envVars.cend(); ++it) {
		const StringPairT& pair = *it;

		size_t requiredSize;
		getenv_s(&requiredSize, NULL, 0, pair.first.c_str());

		if (requiredSize != 0) {
			// Overriding an existing variable is now prohibited.
			// There is potential risk to override an existing environment variable (could be used by system APIs or other libraries)

			fprintf(stderr, "*** Attempt to override an environment variable: [%s]\n", pair.first.c_str());
		} else {
			// Somehow SetEnvironmentVariable API failed to pass new env variable to log4cplus. Had to go with the old fashion.
			std::string envStr = pair.first + "=" + pair.second;
			if (_putenv(envStr.c_str()) == 0) {
				printf(">>> SET [%s] = [%s]\n", pair.first.c_str(), pair.second.c_str());
			} else {
				fprintf(stderr, "*** Error setting environment variable: [%s], err=%d\n", pair.first.c_str(), GetLastError());
			}
		}
	}
}

// Ctrl+C, etc. handler when run from command line
extern "C" BOOL CtrlHandler(DWORD fdwCtrlType) {
	static int breakCnt = 0;
	jsOutputToDebugger("CtrlHandler got type of %d\n", fdwCtrlType);

	KEPServer_Shutdown();

	switch (fdwCtrlType) {
		// 2/17/05 for some reason Ctrl+C doesn't work anymore
		// so now return TRUE on Ctrl+Break so it works

		// Handle the CTRL-C signal.
		case CTRL_C_EVENT:
		case CTRL_CLOSE_EVENT:
		case CTRL_BREAK_EVENT:
			breakCnt++;
			if (breakCnt == 1)
				fprintf(stderr, "Got break.  Shutting down.\n");
			else if (breakCnt == 4)
				fprintf(stderr, "Still shutting down.\n");
			else if (breakCnt == 7)
				fprintf(stderr, "Ok, ok.  I hear ya!\n");
			else if (breakCnt == 10) {
				//fprintf( stderr, "Give it a rest! Ctrl+Shift+Esc then kill me.  Please!\n" );
				fprintf(stderr, "Exiting process\n");
				TerminateProcess(GetCurrentProcess(), -1);
			}
			return TRUE;

		// Pass other signals to the next handler.
		case CTRL_SHUTDOWN_EVENT:
			fprintf(stderr, "Shutting down due to system shutdown.\n");
			return FALSE;

		case CTRL_LOGOFF_EVENT:
			fprintf(stderr, "Shutting down due to user logoff.\n");
			return FALSE;

		default:
			return FALSE;
	}
}

class ShutdownEventMonitorThread : public jsThread {
public:
	ShutdownEventMonitorThread() :
			jsThread("ShutdownEventMonitorThread") {
		std::wstringstream ssEventName;
		ssEventName << L"KGPSERVER_PID_" << GetCurrentProcessId();

		m_hShutdownEvent = CreateEventW(NULL, FALSE, FALSE, ssEventName.str().c_str());
	}

	~ShutdownEventMonitorThread() {
		CloseHandle(m_hShutdownEvent);
	}

private:
	virtual ULONG _doWork() {
		while (m_hShutdownEvent != NULL && !isTerminated()) {
			if (WaitForSingleObject(m_hShutdownEvent, 1000) == WAIT_OBJECT_0) {
				KEPServer_Shutdown();
				break;
			}
		}
		LOG4CPLUS_DEBUG(Logger::getInstance(L"SHUTDOWN"), "KGPServer event monitor thread exiting cleanly");
		return 0;
	}

	HANDLE m_hShutdownEvent;
};

///////////////////////////////////////////////////////////////////////////////
// CrashReporter Support
///////////////////////////////////////////////////////////////////////////////
using namespace CrashReporter;
static void CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	if (pThis)
		pThis->CR_AppCallback(cbs);
}

bool CKEPService::CR_Install() {
	// Install Crash Reporter (default version, without gui)
	pThis = this; // set this crash callback instance
	return CrashReporter::Install("Kaneva Server", "", ::KEP::CR_AppCallback, CR_MINIDUMP_HIGH, false, true);
}

void CKEPService::CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	// Is Not Crash (report only) ?
	if (cbs.isNotCrash)
		return;

	// Identify As 'Server Crash' If No Known Reason
	ErrorData* pED = CrashReporter::GetErrorData();
	if (pED && pED->reason.empty()) {
		pED->reason = "Server Crash";
		pED->type = ErrorData::TYPE_CRASH;
	}

#if (DRF_RUNTIME_REPORTER_SERVER > 0)
	// Crash Runtime Reporter (runtime ends now, crashed report is sent)
	RuntimeReporter::ProfileStateCrashed();
	RuntimeReporter::WaitForReportResponseComplete();
#endif
}

} // namespace KEP

using namespace KEP;

int wmain(int argc, wchar_t* argv[]) {
	CommandLineUtf8 cmdLineUtf8(argc, argv);
#if defined(_DEBUG)
	// Disable dumping memory leaks on program exit because it just takes too long.
	EnableMFCDebugMemoryDump(false);
#endif

	Logger shutdownLogger = Logger::getInstance(L"SHUTDOWN");

	try {
		SetThreadName("main");

		OldCommandLine& cmdLine = Application::singleton().commandLine();
		OldCommandLine::BooleanParameter& paramInteractive = cmdLine.addBoolean("-r", false, "Run application interactively in a console window (run as Windows Service if not specified)");
		cmdLine.addString("-D", "", "Key value pairs for game properties. E.g. GAME_ID=1234;GAME_NAME=\"Test Game\"");
		cmdLine.addBoolean("-s", false, "Run engine with shared directory (read-only)");
		cmdLine.parse(cmdLineUtf8.GetArgC(), cmdLineUtf8.GetArgV());

		CKEPService service;

		// Install Crash Reporter (must be before creating any threads)
		service.CR_Install();

		// Are we to run on command line?
		BOOL ok = TRUE;
		if (paramInteractive) {
			jsVerify(::SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE) != FALSE);
			service.StartedInteractive();
		} else {
			ok = !service.ParseStandardArgs(cmdLineUtf8.GetArgC(), cmdLineUtf8.GetArgV());
		}

		if (ok) {
			// Initialize Service
			service.InitCmdLine(cmdLine);
			service.InitLogger();
			LogInstance("STARTUP");
			LogInfo("====================================================================");
			LogInfo("===== Starting Kaneva Server - v" << VersionApp(false));
			LogInfo("====================================================================");
			LogInfo("pathApp=" << PathApp(false));
			LogInfo("pathXml=" << service.PathXml());
			LogInfo("instanceId=" << service.InstanceId());

			// DRF - Test Crash Reporter
			//CrashReporter::ForcedCrash(3);

			// Start Shutdown Monitor Thread
			ShutdownEventMonitorThread shutdownEventMonitorThread;
			shutdownEventMonitorThread.start();

			// Run Service
			if (paramInteractive) {
				service.Run();
				LOG4CPLUS_DEBUG(shutdownLogger, "KGPServer::winMain() service run complete!");
				jsVerify(::SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, FALSE) != FALSE);
			} else {
				service.StartService();
			}

			// Stop Shutdown Monitor Thread
			shutdownEventMonitorThread.stop();
		}
		int ret = service.GetServiceStatus().dwWin32ExitCode;
		LOG4CPLUS_DEBUG(shutdownLogger, "KGPServer::winMain() exiting cleanly with result [" << ret << "] service run complete!");
		return ret;
	} catch (const ChainedException& e) {
		std::cout << e.str() << std::endl;
		LOG4CPLUS_FATAL(logger, e.str().c_str());
		return e.errorSpec().first;
	} catch (KEPException* e) {
		std::cout << e->ToString() << std::endl;
		LOG4CPLUS_FATAL(logger, e->ToString().c_str());
		delete e;
		return e->m_err;
	} catch (KEPException& e) {
		std::cout << e.ToString() << std::endl;
		LOG4CPLUS_FATAL(logger, e.ToString().c_str());
		return e.m_err;
	} catch (...) {
		ChainedException c(SOURCELOCATION, ErrorSpec(-1, "UnknownExceptionRaised!"));
		std::cout << c.str() << std::endl;
		LOG4CPLUS_FATAL(logger, c.str().c_str());
		return 1;
	}
}
