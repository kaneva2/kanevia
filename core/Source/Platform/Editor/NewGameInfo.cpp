/******************************************************************************
 NewGameInfo.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// NewGameInfo.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "KEPEditGlobals.h"
#include "NewGameInfo.h"
#include ".\newgameinfo.h"

// CNewGameInfo dialog

IMPLEMENT_DYNAMIC(CNewGameInfo, CDialog)
CNewGameInfo::CNewGameInfo(CWnd* pParent /*=NULL*/) :
		CDialog(CNewGameInfo::IDD, pParent) {
}

CNewGameInfo::~CNewGameInfo() {
}

void CNewGameInfo::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CNewGameInfo, CDialog)
ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

// CNewGameInfo message handlers

BOOL CNewGameInfo::Show() {
	BOOL ret = TRUE;

	CStringW dialogOff = AfxGetApp()->GetProfileString(szSection, szNewGameDialog);

	if (dialogOff != L"true") {
		// Show info dialog
		if (DoModal() != IDOK) {
			ret = FALSE;
		}
	}

	return ret;
}

void CNewGameInfo::OnBnClickedOk() {
	if (SendDlgItemMessage(IDC_NEW_GAME_SHOW_MSG, BM_GETSTATE, 0, 0) == BST_CHECKED) {
		// Turn off dialog
		AfxGetApp()->WriteProfileString(szSection, szNewGameDialog, _T("true"));
	}

	OnOK();
}
