; Publish goals allow the editor to provide feedback as to the user regarding the progress
; that has been made during the publishing process.  The Publish Total Goal define enables the editor
; to calculate the total progress made.
!define PUBLISH_TOTAL_GOALS 14
; Called from the Editor to construct a complete Server setup. Binaries, content, and DB.

;TODO List
; Calculate size correctly given multiple install directories. See SectionGetSize, SectionSetSize. Must content with Back button.
; Support silent installation

!ifndef STAR_DEV_ENVIRONMENT_DIR
  ; TODO, suppress this error and return something meaningful to the Editor
  !error "STAR_DEV_ENVIRONMENT_DIR is not defined. Must be passed as /DSTAR_DEV_ENVIRONMENT_DIR=STAR Development Directory"
!endif

!ifndef DISTRIBUTION_DIR
  ; TODO, suppress this error and return something meaningful to the Editor
  !error "DISTRIBUTION_DIR is not defined. Must be passed as /DDISTRIBUTION_DIR=STAR Distribution Directory"
!endif

!ifndef GAME_ID
  ; TODO, suppress this error and return something meaningful to the Editor
  !error "GAME_ID is not defined. Must be passed as /DGAME_ID=GAME_ID"
!endif

;!define WOK_WEB "http://kanevadev:8080/kgp"

!ifndef WOK_WEB
  ; TODO, suppress this error and return something meaningful to the Editor
  !error "WOK_WEB is not defined. Must be passed as /DWOK_WEB=WOK_WEB"
!endif

; HM NIS Edit Wizard helper defines
!define PRODUCT_NAME "Kaneva STAR Server"
!define SHORT_PRODUCT_NAME "Kaneva STAR Server"
!define PRODUCT_VERSION "- 1.0 Beta"
!define PRODUCT_PUBLISHER "Kanvea, Inc."
!define PRODUCT_WEB_SITE "http://www.kaneva.com"
!define PRODUCT_MYSQL_DIR "Software\MySQL AB\MySQL Server 5.1";
!define PRODUCT_ROOT_KEY "HKLM"

; This setup's registry entries
!define STAR_SERVER_DIR_REGKEY "SOFTWARE\Kaneva\STAR_SERVER"
!define STAR_SERVER_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\STAR_SERVER"
!define STAR_SERVER_UNINST_ROOT_KEY "HKLM"
!define STAR_SERVER_ROOT_KEY "HKLM"

!define BIN_DIR "${STAR_DEV_ENVIRONMENT_DIR}\bin"
!define BLADE_DIR "${STAR_DEV_ENVIRONMENT_DIR}\blades"

!define ContentInstallDir "$PROGRAMFILES\Kaneva\STAR_SERVER\Content"
!define PatchInstallDir "c:\inetpub\wwwroot\STAR\PatchFiles"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\STAR_SERVER"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

!define SERVICE_STOPPED                0x00000001
!define SERVICE_START_PENDING          0x00000002
!define SERVICE_STOP_PENDING           0x00000003
!define SERVICE_RUNNING                0x00000004
!define SERVICE_CONTINUE_PENDING       0x00000005
!define SERVICE_PAUSE_PENDING          0x00000006
!define SERVICE_PAUSED                 0x00000007

; MUI 1.67 compatible ------
!include "MUI2.nsh"
!include "FileFunc.nsh"
!insertmacro Locate


; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"
;--------------------------------
;Interface Configuration

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\STAR\Kaneva_Logo_small2.bmp"


; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
!insertmacro MUI_PAGE_LICENSE "${NSISDIR}\STAR\STAR_TOS.rtf"
;
!insertmacro MUI_PAGE_COMPONENTS
; Directory page

!define MUI_DIRECTORYPAGE_TEXT_TOP "$(MUI_DIRECTORYPAGE_TEXT_TOP_A)"
!define MUI_PAGE_CUSTOMFUNCTION_PRE nsDirectorySelect_Binaries
!insertmacro MUI_PAGE_DIRECTORY

Var Content_InstDir
Var Patch_InstDir
Var bOverwritePatchFiles

Var bSetupGameServer
Var bSetupPatchURL
var strKanevaUserName
var strKanevaPassword

; B directory is where to install content
!define MUI_DIRECTORYPAGE_TEXT_TOP "$(MUI_DIRECTORYPAGE_TEXT_TOP_B)"
!define MUI_DIRECTORYPAGE_VARIABLE $Content_InstDir
!define MUI_PAGE_CUSTOMFUNCTION_PRE nsDirectorySelect_Content
!insertmacro MUI_PAGE_DIRECTORY

; C directory is where existing content is located. Used to edit configuration for local server.
; B or C will be shown. Not both.
!define MUI_DIRECTORYPAGE_TEXT_TOP "$(MUI_DIRECTORYPAGE_TEXT_TOP_C)"
!define MUI_DIRECTORYPAGE_VARIABLE $Content_InstDir
!define MUI_PAGE_CUSTOMFUNCTION_PRE nsDirectorySelect_ContentLocation
!define MUI_PAGE_CUSTOMFUNCTION_LEAVE nsDirectorySelect_ContentLocation_Validate
!insertmacro MUI_PAGE_DIRECTORY

!define MUI_DIRECTORYPAGE_TEXT_TOP "$(MUI_DIRECTORYPAGE_TEXT_TOP_D)"
!define MUI_DIRECTORYPAGE_VARIABLE $Patch_InstDir
!define MUI_PAGE_CUSTOMFUNCTION_PRE nsDirectorySelect_PatchLocation
!define MUI_PAGE_CUSTOMFUNCTION_LEAVE nsDirectorySelect_PatchLocation_Validate
!insertmacro MUI_PAGE_DIRECTORY

; Add these 2 back once wok web's gamelogin and addGameServer aspx scripts are updated
;Page custom dlgOnGetKanevaCredentialsCreate dlgOnGetKanevaCredentialsLeave
;Page custom dlgOnGetPatchURLCreate dlgOnGetPatchURLLeave

; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
Page custom nsInstallMySQL nsInstallMySQLLeave
Page custom nsSetMySQLPassword nsSetMySQLPasswordLeave
Page custom nsMySqlConnectPage nsMySqlConnectPageLeave
Page custom nsUserPassEnter nsUserPassLeave
Page custom nsEditConfigFilesEnter
Page custom nsServiceAccountInfoEnter nsServiceAccountInfoLeave
Page custom nsServiceInstallsEnter nsServiceInstallsLeave
; Finish page
;!define MUI_FINISHPAGE_RUN "$INSTDIR\bin\KEPEdit.exe"
!define MUI_FINISHPAGE_RUN_PARAMETERS "-i"
!define MUI_PAGE_CUSTOMFUNCTION_PRE DisableBackButton
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; MUI end ------
LangString MUI_DIRECTORYPAGE_TEXT_TOP_A ${LANG_ENGLSH} "Setup will install ${SHORT_PRODUCT_NAME} ${PRODUCT_VERSION} binaries in the following folder. To install in a different \
folder, click Browse and select another folder. Click Next to continue."
LangString MUI_DIRECTORYPAGE_TEXT_TOP_B ${LANG_ENGLSH} "Setup will install ${SHORT_PRODUCT_NAME} ${PRODUCT_VERSION} content in the following folder. Content can be shared between multiple servers, e.g. a network drive.\
To install in a different folder, click Browse and select another folder."
LangString MUI_DIRECTORYPAGE_TEXT_TOP_C ${LANG_ENGLSH} "Please select the location where the ${SHORT_PRODUCT_NAME} ${PRODUCT_VERSION} content is located. This location will be used \
when editing the local configuration files for this server."
LangString MUI_DIRECTORYPAGE_TEXT_TOP_D ${LANG_ENGLSH} "Setup will install ${SHORT_PRODUCT_NAME} ${PRODUCT_VERSION} patch files in the following folder.\
To install in a different folder, click Browse and select another folder."

LangString DESC_ServerSect ${LANG_ENGLISH} "Install the server binaries."
LangString DESC_ServerSharedSect ${LANG_ENGLISH} "Install the server content. This can be shared among different servers, e.g. a network drive."
LangString DESC_AdminUISect ${LANG_ENGLISH} "Install the Admin UI to administer the server.  IIS and .NET are required."
LangString DESC_MySqlSect ${LANG_ENGLISH} "Install the MySql database server."
LangString DESC_MySqlScriptsSect ${LANG_ENGLISH} "Install the STAR database schema for the MySQL server."

; include the str function lib

!include "${NSISDIR}\STAR\StrFunc.nsh"
${StrRep}

!define VDIRNAME STARAdmin
!include "${NSISDIR}\STAR\kepiischeck.nsh"
!include "${NSISDIR}\STAR\Virtual Directory.nsh"
!include "${NSISDIR}\STAR\dotnetdetect.nsh"
!include "${NSISDIR}\STAR\GetWindowsVer.nsh"

XPStyle on

Name "${SHORT_PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "${DISTRIBUTION_DIR}\STARServerSetup.exe"
InstallDir "$PROGRAMFILES\Kaneva\STAR_SERVER"
InstallDirRegKey HKLM "${STAR_SERVER_DIR_REGKEY}" "Install_Dir"
ShowInstDetails hide
ShowUnInstDetails hide
AutoCloseWindow false

BrandingText "STAR Server"

ComponentText "This program will configure a STAR server on your computer. Below are the components that will be installed."
;InstType /NOCUSTOM
InstType "Full"                          ; 1
InstType "Server with content"           ; 2
InstType "Server services only"          ; 3
InstType "Admin UI Only (IIS required)"  ; 4
InstType "Database"                      ; 5

Function .onInit
; Each publish goal can have a text string that will be displayed in the editor's publish dialog to inform the user of how much progress has been
; made in the publishing process.
!define PUBLISH_GOAL1 "Initializing..."
;  Check to see if the KGPController service is installed.  If so,  ask the user if it is ok to delete and remove it
;  before reinstalling as it may cause problems.  Finally reboot the user's machine to guarantee that the service has been removed.
nsSCM::QueryStatus "KGPController"
pop $0
strcmp $0 success +1 MainInitialization
MessageBox MB_YESNO "Star services appear to be installed.$\r$\nThey must be uninstalled before you can continue.$\r$\nWould you like to uninstall them now?" IDYES KillServices IDNO QUIT
KillServices:
nsSCM::Stop /NOUNLOAD "KGPServer"
nsSCM::Stop /NOUNLOAD "KGPController"
nsSCM::Remove /NOUNLOAD "KGPServer"
nsSCM::Remove /NOUNLOAD "KGPController"
MessageBox MB_YESNO|MB_ICONQuestion "Your computer needs to be restarted.$\r$\nWould you like to restart now?" IDYES YES_REBOOT IDNO QUIT
QUIT:
abort
YES_REBOOT:
reboot
abort

MainInitialization:
   ; check to 9c by seeing if version is 5
   ; if exists, but not 5, ask if they want to continue
   GetDLLVersion "$SYSDIR\d3d9.dll" $R0 $R1
   IfErrors dxErr
   IntOp $R2 $R0 & 0xffff0000
   IntOp $R3 0 + 0x0050000
   StrCmp $R2 $R3 dxOk

   IntOp $R2 $R0 & 0xffff0000
   IntOp $R3 0 + 0x0060000 ; Vista
   StrCmp $R2 $R3 dxOk

   MessageBox MB_YESNO|MB_ICONQUESTION "The required D3D9.DLL does not appear to be DirectX 9c.  Do you wish to continue installing anyway?" IDYES dxOk
   Quit

   ;IfFileExists "$SYSDIR\d3d9.dll" dxOk
   dxErr:
                MessageBox MB_YESNO|MB_ICONQUESTION "The required DLLs for DirectX 9 were not found.  Installation will end now.  Would you like to go run Windows Update now to download DirectX 9?" IDNO skipDx
                        ExecShell "open" "http://windowsupdate.microsoft.com"
                skipDx:
                Quit
   dxOk:

   ; only install on 2000+
   Call GetWindowsVersion
   Pop $R0
   StrCpy $R1 $R0 2
   StrCmp $R1 "ME" BadOs
   StrCmp $R1 "95" BadOs
   StrCmp $R1 "98" BadOs
   StrCmp $R1 "NT" BadOs
   goto GoodOs
   BadOs:
      MessageBox MB_ICONEXCLAMATION \
                 "STAR servers will only work on Window 2000 or higher."
      Abort
   GoodOs:

   ReadRegStr $0 ${STAR_SERVER_ROOT_KEY} "${STAR_SERVER_UNINST_KEY}" "UninstallString"
   IfErrors SectionExit
      MessageBox MB_OK|MB_ICONINFORMATION \
                 "A previous STAR server is installed."
      goto SectionExit ; now allow them to continue
      ; Abort ; don't continue if they said no

   ; TODO Uninstall section. Not updated. Not currently entered from messagebox above.
   Uninstall:
             ExecWait "$0"
             MessageBox MB_OK|MB_ICONINFORMATION \
                 "Click OK when uninstall is complete."
             ; check again to make sure uninstalled
             ReadRegStr $0 ${STAR_SERVER_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString"
             IfErrors SectionExit
             MessageBox MB_OK|MB_ICONEXCLAMATION \
                 "The uninstall failed, or was cancelled.  Aborting install."
             Abort

  SectionExit:

  strcpy $bSetupPatchURL "false"

FunctionEnd

Var FileIn
Var FileOut

Section "-Common" CommonSect
!define PUBLISH_GOAL2 "Processing Common Data..."
        SectionIn 1 2 3
        SetOverwrite off

        SetOutPath "$SYSDIR"

        ; from MS for VC7
        File "$%windir%\system32\msvcr71.dll"   ; CRT
        File "$%windir%\system32\mfc71enu.dll"  ; MFC Lang
        File "$%windir%\system32\mfc71.dll"     ; MFC
        File "$%windir%\system32\mfc71u.dll"    ; MFC
        File "$%windir%\system32\msvcp71.dll"   ; STL
!define PUBLISH_GOAL3 "Processing Common Data..."
        SetOverwrite ifnewer

        SetOutPath "$INSTDIR"
        IfErrors createDirError

        File "${STAR_DEV_ENVIRONMENT_DIR}\Vcredist_x86.exe"
!define PUBLISH_GOAL4 "Processing Common Data..."
        ;Check to see if we need to install Vcredist_x86.exe
	ExecWait '"${BIN_DIR}\Vcredist_x86.exe" /q:a /c:$\"msiexec /i vcredist.msi /qn $\" '

        Goto end

        createDirError:
        MessageBox MB_OK|MB_ICONEXCLAMATION "Can not create output folder.  Check permissions."
        Abort
        end:
!define PUBLISH_GOAL5 "Adding Additional Logic..."
        ; If theuser has decided to destroy their old patch directory, then we should destroy it.
        ; Otherwise, leave the files in tact and add any new patch files we may have.
        strcmp $bOverwritePatchFiles "true" +1 update_newer
        IfFileExists $Patch_InstDir +1 update_patchfiles
        Rmdir /r $Patch_InstDir
        IfFileExists $Patch_InstDir error_patch_dir_still_exists update_patchfiles

        error_patch_dir_still_exists:
        MessageBox MB_OK|MB_ICONSTOP "STAR Installer encountered a problem deleting the patch files folder. \
        This installation cannot complete. Please check that your Windows account provides the correct security\
        settings for this folder and try installing again."
        abort

        ;goto update_patchfiles

        update_newer:
        setoverwrite off
!define PUBLISH_GOAL6 "Processing Patch Files..."
        update_patchfiles:
        SetOutPath $Patch_InstDir
	File /r "${DISTRIBUTION_DIR}\PatchFiles\*.*"

SectionEnd

Section "Server" ServerSect
!define PUBLISH_GOAL7 "Processing Server Files..."
  SectionIn 1 2 3
  SetOverwrite ifnewer

        SetOutPath "$INSTDIR\bin"

        File "${BIN_DIR}\dbghelp.dll" ; debug helper for mini dump

        ; extras
        File "${STAR_DEV_ENVIRONMENT_DIR}\bin\f_in_box.dll"
        File "${STAR_DEV_ENVIRONMENT_DIR}\bin\log4cplus.dll"
        File "${STAR_DEV_ENVIRONMENT_DIR}\bin\CheckTemplate.vbs"
	; nsis plugin .dll to edit defaultserver.xml file
	File "${NSISDIR}\Plugins\nsConfig.dll"
        SetOutPath "$INSTDIR\bin"

        ; database support
        File "${STAR_DEV_ENVIRONMENT_DIR}\bin\libmysql.dll"
        File "${STAR_DEV_ENVIRONMENT_DIR}\bin\mysqlpp.dll"

        SetOutPath "$INSTDIR\bin"
        File "${STAR_DEV_ENVIRONMENT_DIR}\templates\Shared\ServerLog.tmp"
        File "${STAR_DEV_ENVIRONMENT_DIR}\templates\Shared\ControllerLog.tmp"

        ; now edit the KEPLog file, if it doesn't exist
        IfFileExists "$INSTDIR\bin\ServerLog.cfg" skipLog

        FileOpen $FileIn "$INSTDIR\bin\ServerLog.tmp" "r"
        FileOpen $FileOut "$INSTDIR\bin\ServerLog.cfg" "w"

        ClearErrors

        loop:
             FileRead $FileIn $1
             IfErrors done
             ${StrRep} $0 $1 "%INSTDIR%" "$INSTDIR"
             FileWrite $FileOut $0
             Goto loop
        done:
        FileClose $FileIn
        FileClose $FileOut
        ; Delete"$INSTDIR\bin\ServerLog.tmp"
        skipLog:
        ClearErrors

        ; now edit the ControllerLog file
        IfFileExists "$INSTDIR\bin\ControllerLog.cfg" skipControllerLog

        FileOpen $FileIn "$INSTDIR\bin\ControllerLog.tmp" "r"
        FileOpen $FileOut "$INSTDIR\bin\ControllerLog.cfg" "w"
        loop2:
             FileRead $FileIn $1
             IfErrors done2
             ${StrRep} $0 $1 "%INSTDIR%" "$INSTDIR"
             FileWrite $FileOut $0
             Goto loop2
        done2:
        FileClose $FileIn
        FileClose $FileOut
        Delete "$INSTDIR\bin\ControllerLog.tmp"
        skipControllerLog:
        ClearErrors

        SetOutPath "$INSTDIR\bin"

        File "${BIN_DIR}\*.dll"
        File "${BIN_DIR}\KGPServer.exe"
        File "${BIN_DIR}\KGPController.exe"
        File "${DISTRIBUTION_DIR}\ServerFiles\KGPServer.xml"
        File "${DISTRIBUTION_DIR}\ServerFiles\KGPController.xml"

        SetOutPath "$INSTDIR\Blades"

        File "${BLADE_DIR}\*.dll"
        SetOverwrite off ; only install blade XML if not there
        File "${DISTRIBUTION_DIR}\ServerFiles\Blades\*.xml"
        SetOverwrite ifnewer

        CreateDirectory "$INSTDIR\logs" ; recursively create a directory

        SetOutPath "$INSTDIR\bin" ; set cur dir
        ClearErrors
        ;ExecWait '"$INSTDIR\bin\KGPServer" -i'
        IfErrors serviceError

        Goto end

        serviceError:
        MessageBox MB_OK|MB_ICONEXCLAMATION "Error installing KGPServer"
        Abort

        end:
SectionEnd

Section "Content" ServerSharedSect
!define PUBLISH_GOAL8 "Processing Shared Server Files..."
  SectionIn 1 2
  SetOverwrite ifnewer

        SetOutPath "$Content_InstDir"
        File /r "${DISTRIBUTION_DIR}\ServerFiles"

        ; Add PatchFiles dir to the inetpub dir.  Registry records %systemdrive% as the filesystem root dir.
        ; However NSIS doesn't like the %systemdir% var so for now manually copy it over.
	;ReadRegStr $0 HKLM "Software\Microsoft\InetStp\" "PathWWWRoot"
	;MessageBox MB_OK "Src: ${DISTRIBUTION_DIR}\PatchFiles"
	;MessageBox MB_OK "Dest: $0\PatchFiles\"
	;SetOutPath "$0\STAR\PatchFiles\"
        Goto end

        end:
SectionEnd

Section "Admin UI" AdminUISect
!define PUBLISH_GOAL9 "Processing Shared Server Files..."
  SectionIn 1 4
  SetOverwrite ifnewer

        SetOutPath "$INSTDIR\KEPAdmin"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\*.aspx.*"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\*.asax"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\*.htm*"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\*.css"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\*.res*"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\*.js"

	SetOverwrite off
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\Web.config"
	SetOverwrite ifnewer

        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\*.xsl*"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\bin"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\images"
        File /r "${STAR_DEV_ENVIRONMENT_DIR}\KEPAdmin\tree-menu"

        ; check for IIS
        Call KEPCheckIISVersion
        Pop $R3
        StrCmp $R3 0 NoIIS
        DetailPrint "Found IIS"

        ; check for IIS and .NET
        Call IsDotNETInstalled
        Pop $R3
        StrCmp $R3 0 NoDotNet
        DetailPrint "Found .NET"

        Call CreateVDir
        Goto SectionExit


    NoDotNet:
    DetailPrint "Can't find .NET!"
    Goto SectionExit

    NoIIS:
    DetailPrint "IIS not detected"

    SectionExit:
SectionEnd

Section "STAR schema" MySqlScriptsSect
!define PUBLISH_GOAL10 "Processing MySQL Scripts..."
  SectionIn 1 5
  SetOverwrite ifnewer

        SetOutPath "$INSTDIR\sqlscripts"

	File "${STAR_DEV_ENVIRONMENT_DIR}\sqlscripts\*.sql"
	File "${STAR_DEV_ENVIRONMENT_DIR}\sqlscripts\MySqlSetup.dll"
	File "${STAR_DEV_ENVIRONMENT_DIR}\bin\libmysql.dll"
	File "${STAR_DEV_ENVIRONMENT_DIR}\bin\mysqlpp.dll"

	File "${DISTRIBUTION_DIR}\ServerFiles\sqlscripts\*.sql"

SectionEnd

Section "MySql Server" MySqlSect
!define PUBLISH_GOAL11 "Including MySQL..."
  SectionIn 1 5
  SetOverwrite ifnewer

        SetOutPath "$INSTDIR\bin\mysql"

	File "${STAR_DEV_ENVIRONMENT_DIR}\bin\mysql\mysql-essential-5.1.22-rc-win32.msi"
!define PUBLISH_GOAL12 "Adding Additional Logic..."
SectionEnd

Var Dialog
Var DescLabel
Var HostLabel
Var HostText
Var PortLabel
Var PortText
Var UserLabel
Var UserText
Var PasswordLabel
Var PasswordText
Var ConfirmPasswordLabel
Var ConfirmPasswordText
Var DatabaseLabel
Var DatabaseText
Var TestConnectionButton
Var SetMySQLPasswordButton
Var StatusText
Var MySQLInstallDescText

; Windows Account Information
Var strWindowsUser
Var strWindowsPass

; Testing MySQL connection
Var theHost
Var thePort
Var theUser
Var thePass
Var theDatabase

Var DialogMySQL
Var TryInstallButton

Var bWeInstalledMySQL
var bDontSetMySQLPassword

Function nsSetMySQLPassword

         ;MessageBox MB_OK "$bDontSetMySQLPassword"
         ${If} $bDontSetMySQLPassword == 1
               goto EXIT
         ${Endif}

         CHECK_IF_WE_INSTALLED_MYSQL:
         intcmp $bWeInstalledMySQL 1 setMySQLPass

         EXIT:
         abort

         setMySQLPass:
         GetDlgItem $R0 $HWNDPARENT 1
         EnableWindow $R0 0

	nsDialogs::Create /NOUNLOAD 1018
	Pop $DialogMySQL

	${If} $DialogMySQL == error
		Abort
	${EndIf}

	${NSD_CreateLabel} 5u 12u 270u 8u "Please set your MySQL Server Instance Password for the root account."
	Pop $MySQLInstallDescText

	${NSD_CreateLabel} 5u 42u 40u 8u "Password:"
	Pop $PasswordLabel

        ${NSD_CreatePassword} 50u 40u 100u 12u ""
	Pop $PasswordText

        ${NSD_CreateLabel} 5u 62u 40u 8u "Confirm Password:"
	Pop $ConfirmPasswordLabel

        ${NSD_CreatePassword} 50u 60u 100u 12u ""
	Pop $ConfirmPasswordText

	${NSD_CreateButton} 45u 92u 80u 12u "Set Password"
	Pop $SetMySQLPasswordButton
        ${NSD_OnClick} $SetMySQLPasswordButton SetMySQLPasswordButtonClicked


	GetDlgItem $0 $HWNDPARENT 1037
	SendMessage $0 ${WM_SETTEXT} 0 "STR:MySQL Installation"

	GetDlgItem $0 $HWNDPARENT 1038
	SendMessage $0 ${WM_SETTEXT} 0 "STR:Install the MySQL database server version 5.1.22"

	showdialog:

		nsDialogs::Show
skipThis:
FunctionEnd

Function nsSetMySQLPasswordLeave

FunctionEnd


Var theMySQLPassword
Var theConfirmedMySQLPassword

Function SetMySQLPasswordButtonClicked

	${NSD_GetText} $PasswordText	        $theMySQLPassword
	${NSD_GetText} $ConfirmPasswordText	$theConfirmedMySQLPassword

        StrCmp $theMySQLPassword $theConfirmedMySQLPassword SUCCESS
	goto SetMySQLPasswordMismatchError

        SUCCESS:
                 nsSCM::Start "MySQL"
                 ReadRegStr $1 ${PRODUCT_ROOT_KEY} "${PRODUCT_MYSQL_DIR}" "Location"
                 ;ExecWait '"$1\bin\mysqladmin.exe -u root password $theMySQLPassword'
                 ExecWait '"$1\bin\mysqladmin" -u root password $theMySQLPassword' $0
                 ;mysqladmin -u root password NEWPASSWORD
                 ;IfErrors SetMySQLPasswordAdminError

		MessageBox MB_OK|MB_ICONINFORMATION "Your password has successfully been set."
	        GetDlgItem $R0 $HWNDPARENT 1
                EnableWindow $R0 1
		goto end

       SetMySQLPasswordMismatchError:
                MessageBox MB_OK|MB_ICONSTOP "The password and confirmed password do not match.  Please try again."
                goto end

       SetMySQLPasswordAdminError:
                MessageBox MB_OK|MB_ICONSTOP "Unable to set your MySQL password.  Please uninstall MySQL and try again."
                goto end
        end:


FunctionEnd

Function nsInstallMySQL

         intop $bDontSetMySQLPassword 0 + 0

	 ;MessageBox MB_OK "$PROGRAMFILES\MYSQL\data\*.*"
         IfFileExists "$PROGRAMFILES\MYSQL\data\*.*"  DONT_SET_MYSQL_PASSWORD NEXT

         DONT_SET_MYSQL_PASSWORD:
         intop $bDontSetMySQLPassword 0 + 1

        NEXT:
        ;StrCpy $bWeInstalledMySQL "false"
        SectionGetFlags ${MySqlSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipThis skipThis

	nsDialogs::Create /NOUNLOAD 1018
	Pop $DialogMySQL

	${If} $DialogMySQL == error
		Abort
	${EndIf}

	${NSD_CreateLabel} 5u 12u 250u 70u ""
	Pop $MySQLInstallDescText

	${NSD_CreateButton} 5u 92u 80u 12u "Install MySQL"
	Pop $TryInstallButton
        ${NSD_OnClick} $TryInstallButton TryInstallButtonClicked

	GetDlgItem $0 $HWNDPARENT 1037
	SendMessage $0 ${WM_SETTEXT} 0 "STR:MySQL Installation"

	GetDlgItem $0 $HWNDPARENT 1038
	SendMessage $0 ${WM_SETTEXT} 0 "STR:Install the MySQL database server version 5.1.22"

	nsSCM::QueryStatus "MySQL"
        Pop $0 ; return error/success
	Pop $1 ; return service status

	StrCmp $0 success serviceinstalled

	goto servicenotinstalled

	serviceinstalled:
		IntCmp $1 ${SERVICE_RUNNING} servicerunning servicenotrunning servicenotrunning
		goto servicenotrunning

	servicenotrunning:
		EnableWindow $TryInstallButton 0
		SendMessage $MySQLInstallDescText ${WM_SETTEXT} 0 "STR:Setup has detected that the MySQL database server is installed on this computer but is currently not running.$\r$\nPlease start the database server and retry the installation."
	        MessageBox MB_YESNO|MB_ICONQUESTION "Setup has detected that the MySQL database server is installed on this computer but is currenly not running. Would you like to start the MySQL database server?" IDYES yesStartMySQL
		goto showdialog

	servicecantstart:
		EnableWindow $TryInstallButton 0
		SendMessage $MySQLInstallDescText ${WM_SETTEXT} 0 "STR:Setup has detected that the MySQL database server is installed on this computer but is currently not running.$\r$\nPlease start the database server and retry the installation."
		GetDlgItem $0 $HWNDPARENT 1
		EnableWindow $0 0
		goto showdialog

	yesStartMySQL:
		nsSCM::Start "MySQL"
		Pop $0 ; return error/success
		StrCmp $0 success servicerunning
		goto servicecantstart

	servicerunning:
                ;StrCpy $bWeInstalledMySQL "false"
		EnableWindow $TryInstallButton 0
		SendMessage $MySQLInstallDescText ${WM_SETTEXT} 0 "STR:Setup has detected that the MySQL database server is running.$\r$\nThis database server will be used for the STAR installation.$\r$\nContinue with the setup program by pressing Next."
                intop $bWeInstalledMySQL 0 + 0
                goto showdialog

	servicenotinstalled:
        	;strcpy $bWeInstalledMySQL "true"

		SendMessage $MySQLInstallDescText ${WM_SETTEXT} 0 "STR:Setup has detected that the MySQL database server is not installed on this computer.$\r$\nPress the MySQL installation button to install the database server.$\r$\nIf the MySQL database server is installed already, continue the setup program by pressing Next."
                intop $bWeInstalledMySQL 0 + 1
                goto showdialog

	showdialog:

		nsDialogs::Show
skipThis:
FunctionEnd

Function TryInstallButtonClicked

    ; Disable Next button until done
    GetDlgItem $0 $HWNDPARENT 1
    EnableWindow $0 0

    ;MessageBox MB_OK "The STAR installer will now attempt to install the MySQL server. Please use the default settings and enter a password for your database in the MySQL Configuration wizard."
;    ExecWait '"msiexec.exe" /i $\"$INSTDIR\bin\mysql\mysql-essential-5.1.22-rc-win32.msi$\" /qn INSTALLDIR=$\"C:\PROGRAM FILES\MYSQL$\" ' $0
    ExecWait '"msiexec.exe" /i $\"$INSTDIR\bin\mysql\mysql-essential-5.1.22-rc-win32.msi$\" /qb INSTALLDIR=$\"C:\PROGRAM FILES\MYSQL$\" ' $0
    ReadRegStr $1 ${PRODUCT_ROOT_KEY} "${PRODUCT_MYSQL_DIR}" "Location"
    ExecWait '"$1\bin\mysqld" --install ' $0
    ;ReadRegStr $1 ${PRODUCT_ROOT_KEY} "${PRODUCT_MYSQL_DIR}" "Location"
    ;ExecWait "$1\bin\MySQLInstanceConfig.exe" $3

    nsSCM::Start "MySQL"
    Pop $0 ; return error/success
    StrCmp $0 success servicerunning
    MessageBox MB_OK|MB_ICONEXCLAMATION "The MySQL database server could not be started. Please retry the installation."
    Quit
    servicerunning:
	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 1
        EnableWindow $TryInstallButton 0
	SendMessage $MySQLInstallDescText ${WM_SETTEXT} 0 "STR:Setup has successfully installed the MySQL database server. Continue setup by pressing Next."

    ;StrCpy $bWeInstalledMySQL "true"
    intop $bWeInstalledMySQL 0 + 1

FunctionEnd

Function nsInstallMySQLLeave

FunctionEnd

Function nsMySqlConnectPage

	; Used to determine that these have been filled out in
	; nsEditConfigFilesEnter
	StrCpy $theUser "Empty"
	StrCpy $thePass "Empty"

        SectionGetFlags ${MySqlScriptsSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipThis skipThis

	nsDialogs::Create /NOUNLOAD 1018
	Pop $Dialog

	${If} $Dialog == error
		Abort
	${EndIf}

	Call DisableBackButton

	GetDlgItem $0 $HWNDPARENT 1037
	SendMessage $0 ${WM_SETTEXT} 0 "STR:STAR schema"

	GetDlgItem $0 $HWNDPARENT 1038
	SendMessage $0 ${WM_SETTEXT} 0 "STR:Install the MySQL database STAR schema"

	${NSD_CreateLabel} 5u 12u 80u 12u "Install MySQL Scripts"
	Pop $DescLabel

	${NSD_CreateLabel} 5u 32u 20u 15u "Host:"
	Pop $HostLabel

	${NSD_CreateText} 45u 30u 100u 12u "localhost"
	Pop $HostText

	${NSD_CreateLabel} 150u 32u 20u 15u "Port:"
	Pop $PortLabel

	${NSD_CreateText} 171u 30u 30u 12u "3306"
	Pop $PortText

	${NSD_CreateLabel} 5u 52u 20u 15u "User:"
	Pop $UserLabel

	${NSD_CreateText} 45u 50u 100u 12u "root"
	Pop $UserText

	${NSD_CreateLabel} 5u 72u 35u 15u "Password:"
	Pop $PasswordLabel

	${NSD_CreatePassword} 45u 70u 100u 12u ""
	Pop $PasswordText

	${NSD_CreateLabel} 5u 92u 35u 15u "Database:"
	Pop $DatabaseLabel

	${NSD_CreateText} 45u 90u 100u 12u "kgp"
	Pop $DatabaseText

	${NSD_CreateButton} 45u 112u 80u 12u "Test Connection"
	Pop $TestConnectionButton
        ${NSD_OnClick} $TestConnectionButton TestConnectionButtonClicked

	${NSD_CreateLabel} 45u 132u 200u 12u ""
	Pop $StatusText

	nsDialogs::Show
skipThis:
FunctionEnd

Function TestConnectionButtonClicked

	${NSD_GetText} $HostText	$theHost
	${NSD_GetText} $PortText	$thePort
	${NSD_GetText} $UserText	$theUser
	${NSD_GetText} $PasswordText	$thePass
	${NSD_GetText} $DatabaseText	$theDatabase

	; push parameters on stack right to left
	Push "$StatusText"
	Push "$theDatabase"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "notused.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll testConnection

        ;get the result
	Pop $0
	StrCmp $0 success SUCCESS
	goto end

        SUCCESS:
		MessageBox MB_OK|MB_ICONINFORMATION "Success!"

	end:

FunctionEnd

Var install_kgp_common
Var install_shard_info
Var install_kgp

Function nsMySqlConnectPageLeave

	; Disable Next button
	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 0

	${NSD_GetText} $HostText	$theHost
	${NSD_GetText} $PortText	$thePort
	${NSD_GetText} $UserText	$theUser
	${NSD_GetText} $PasswordText	$thePass
	${NSD_GetText} $DatabaseText	$theDatabase

	StrCpy $install_kgp_common "Y"
	StrCpy $install_shard_info "Y"
	StrCpy $install_kgp "Y"

	Push "$StatusText"
	Push "$theDatabase"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "notused.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll testConnection

	;MySqlSSetup.dll will present error message if needed
        ;get the result
	Pop $0
	StrCmp $0 success +2
	Abort

	; determine existence of kgp_common, shard_info, kgp(or user supplied name)
	Push "$StatusText"
	Push "kgp_common"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "notused.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll validateDB

	; success indicates database exists, error denotes db doesn't exist or a different failure occurred.
	; different failure will manifest again during script installation.
	Pop $0
	StrCmp $0 success 0 kgp_common_no_exist
	MessageBox MB_YESNO|MB_ICONQUESTION "The kgp_common database already exists. Do you wish to drop this database?" IDYES dxOk1
	StrCpy $install_kgp_common "N"
	goto kgp_common_no_exist

	dxOk1:
	 Push "$StatusText"
	 Push "kgp_common"
	 Push "$thePass"
	 Push "$theUser"
	 Push "$thePort"
	 Push "$theHost"
	 Push "notused.sql"
	 Push "$INSTDIR\sqlscripts"
	 CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll dropDB
	 Pop $0
	 StrCmp $0 success +3 0
         MessageBox MB_OK|MB_ICONEXCLAMATION "The kgp_common database could not be dropped. Aborting installation."
	 Abort

	kgp_common_no_exist:

	Push "$StatusText"
	Push "shard_info"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "notused.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll validateDB

	; success indicates database exists, error denotes db doesn't exist or a different failure occurred.
	; different failure will manifest again during script installation.
	Pop $0
	StrCmp $0 success 0 kgp_shard_no_exist
	MessageBox MB_YESNO|MB_ICONQUESTION "The shard_info database already exists. Do you wish to drop this database?" IDYES dxOk2
	StrCpy $install_shard_info "N"
	goto kgp_shard_no_exist

	dxOk2:
	 Push "$StatusText"
	 Push "shard_info"
	 Push "$thePass"
	 Push "$theUser"
	 Push "$thePort"
	 Push "$theHost"
	 Push "notused.sql"
	 Push "$INSTDIR\sqlscripts"
	 CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll dropDB
	 Pop $0
	 StrCmp $0 success +3 0
         MessageBox MB_OK|MB_ICONEXCLAMATION "The shard_info database could not be dropped. Aborting installation."
	 Abort

	kgp_shard_no_exist:

	Push "$StatusText"
	Push "$theDatabase"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "notused.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll validateDB

	; success indicates database exists, error denotes db doesn't exist or a different failure occurred.
	; different failure will manifest again during script installation.
	Pop $0
	StrCmp $0 success 0 kgp_no_exist
	MessageBox MB_YESNO|MB_ICONQUESTION "The $theDatabase already exists. Do you wish to drop this database?" IDYES dxOk3
	StrCpy $install_kgp "N"
	goto kgp_no_exist

	dxOk3:
	 Push "$StatusText"
	 Push "$theDatabase"
	 Push "$thePass"
	 Push "$theUser"
	 Push "$thePort"
	 Push "$theHost"
	 Push "notused.sql"
	 Push "$INSTDIR\sqlscripts"
	 CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll dropDB
	 Pop $0
	 StrCmp $0 success +3 0
         MessageBox MB_OK|MB_ICONEXCLAMATION "The $theDatabase could not be dropped. Aborting installation."
	 Abort

	kgp_no_exist:

	StrCmp $install_kgp "Y" 0 createusedb
	ClearErrors
        FileOpen $FileOut "$INSTDIR\sqlscripts\template_create_db.sql" "w"
	IfErrors errCreateTemplateDbSql
	FileWrite $FileOut "CREATE DATABASE $theDatabase;$\r$\n"
	IfErrors errCreateTemplateDbSql
	FileWrite $FileOut "USE $theDatabase;$\r$\n"
	IfErrors errCreateTemplateDbSql
	FileClose $FileOut
	goto createusedb

	errCreateTemplateDbSql:
         MessageBox MB_OK|MB_ICONEXCLAMATION "The template_create_db.sql file for database $theDatabase could not be created. Aborting installation."
	 Abort

	createusedb:

	ClearErrors
        FileOpen $FileOut "$INSTDIR\sqlscripts\template_use_db.sql" "w"
	IfErrors errUseDbSql
	FileWrite $FileOut "USE $theDatabase;$\r$\n"
	IfErrors errUseDbSql
	FileClose $FileOut
	goto installthescripts

	errUseDbSql:
         MessageBox MB_OK|MB_ICONEXCLAMATION "The template_use_db.sql file for database $theDatabase could not be created. Aborting installation."
	 Abort

	installthescripts:

	StrCmp $install_kgp_common "Y" 0 SCHEMA_SKIPPED
	StrCmp $install_shard_info "Y" 0 SCHEMA_SKIPPED
	SendMessage $StatusText ${WM_SETTEXT} 0 "STR:DB update begin (0 of 3)."

	; push parameters on stack right to left
	Push "$StatusText"
	Push "$theDatabase"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "star_schema_create.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll installScripts

        ;get the result
	Pop $0
	StrCmp $0 success SUCCESS_SCHEMA
	SendMessage $StatusText ${WM_SETTEXT} 0 "STR:Error creating STAR base schema. (1 of 3)"
	Abort

        SUCCESS_SCHEMA:
	   SendMessage $StatusText ${WM_SETTEXT} 0 "STR:STAR base schema created successfully. (1 of 3)"
        SCHEMA_SKIPPED:
	   SendMessage $StatusText ${WM_SETTEXT} 0 "STR:STAR base schema skipped. (1 of 3)"

	StrCmp $install_kgp "Y" 0 MASTER_SKIPPED
	Push "$StatusText"
	Push "$theDatabase"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "master.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll installScripts

        ;get the result
	Pop $0
	StrCmp $0 success SUCCESS_MASTER
	SendMessage $StatusText ${WM_SETTEXT} 0 "STR:Error creating STAR schema. (2 of 3)"
	Abort

        SUCCESS_MASTER:
	   SendMessage $StatusText ${WM_SETTEXT} 0 "STR:STAR schema created successfully. (2 of 3)"
        MASTER_SKIPPED:
	   SendMessage $StatusText ${WM_SETTEXT} 0 "STR:STAR schema skipped. (2 of 3)"

	Push "$StatusText"
	Push "$theDatabase"
	Push "$thePass"
	Push "$theUser"
	Push "$thePort"
	Push "$theHost"
	Push "distribution_export_master.sql"
	Push "$INSTDIR\sqlscripts"
	CallInstDLL $INSTDIR\sqlscripts\MySqlSetup.dll installScripts

        ;get the result
	Pop $0
	StrCmp $0 success SUCCESS_ALL
	SendMessage $StatusText ${WM_SETTEXT} 0 "STR:Error exporting distribution data. (3 of 3)"
	Abort

        SUCCESS_ALL:
	    MessageBox MB_OK|MB_ICONINFORMATION "Success! (3 of 3)"

FunctionEnd

Function nsServiceAccountInfoEnter

	nsDialogs::Create /NOUNLOAD 1018
	Pop $Dialog

	${If} $Dialog == error
		Abort
	${EndIf}

	;Call DisableBackButton

	GetDlgItem $0 $HWNDPARENT 1037
	SendMessage $0 ${WM_SETTEXT} 0 "STR:Windows Account Information"

	GetDlgItem $0 $HWNDPARENT 1038
	SendMessage $0 ${WM_SETTEXT} 0 "STR:Provide the Windows user name and password that the STAR Server will log on as."

	${NSD_CreateLabel} 5u 12u 300u 24u "Kaneva will not store your password. It will only be used to set up your STAR Server.$\r$\nYou can change the account credentials later in the Services control panel."
	Pop $DescLabel

	${NSD_CreateLabel} 5u 52u 20u 15u "User:"
	Pop $UserLabel

	;${NSD_CreateLabel} 150u 52u 150u 25u "(Ex. example@example.com)"
	;Pop $UserLabel

	${NSD_CreateText} 45u 50u 100u 12u ""
	Pop $UserText

	${NSD_CreateLabel} 5u 72u 35u 15u "Password:"
	Pop $PasswordLabel

	${NSD_CreatePassword} 45u 70u 100u 12u ""
	Pop $PasswordText

	nsDialogs::Show

skipThis:
FunctionEnd

var user
var domain

Function nsServiceAccountInfoLeave

	${NSD_GetText} $UserText	$strWindowsUser
	${NSD_GetText} $PasswordText	$strWindowsPass

	;If the user name is blank,  just pass in an empty username
	strcmp $strWindowsUser "" EXIT

	; Testing
	;MessageBox MB_OK "$strWindowsUser with password $strWindowsPass"
	ReadRegStr $domain HKCU "Volatile Environment" "USERDOMAIN"
	; If there isn't a domain, go to NO_DOMAIN
	strcmp $domain "" NO_DOMAIN
	strcpy $strWindowsUser "$domain\$strWindowsUser"
	;MessageBox MB_OK "$strWindowsUser"
	goto EXIT

	NO_DOMAIN:
	ReadRegStr $domain HKCU "Volatile Environment" "USERDNSDOMAIN"
	; If there isn't a domain DNS, go to EXIT
	strcmp $domain "" EXIT
	strcpy $strWindowsUser "$strWindowsUser@$domain"
	;MessageBox MB_OK "$strWindowsUser"

EXIT:

FunctionEnd

Function nsUserPassEnter

        SectionGetFlags ${ServerSharedSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipThis skipThis

	; if user is not installing mysql or mysqlscripts, user and pass hasn't
	; been prompted for. get it now
	StrCmp $theUser "Empty" 0 skipThis

	nsDialogs::Create /NOUNLOAD 1018
	Pop $Dialog

	${If} $Dialog == error
		Abort
	${EndIf}

	Call DisableBackButton

	GetDlgItem $0 $HWNDPARENT 1037
	SendMessage $0 ${WM_SETTEXT} 0 "STR:STAR schema"

	GetDlgItem $0 $HWNDPARENT 1038
	SendMessage $0 ${WM_SETTEXT} 0 "STR:Configuration update"

	${NSD_CreateLabel} 5u 12u 300u 24u "Please enter the user and password used to connect to the STAR database.$\r$\nThe content file $Content_InstDir\ServerFiles\db.cfg will be updated."
	Pop $DescLabel

	${NSD_CreateLabel} 5u 52u 20u 15u "User:"
	Pop $UserLabel

	${NSD_CreateText} 45u 50u 100u 12u "root"
	Pop $UserText

	${NSD_CreateLabel} 5u 72u 35u 15u "Password:"
	Pop $PasswordLabel

	${NSD_CreatePassword} 45u 70u 100u 12u ""
	Pop $PasswordText

	${NSD_CreateLabel} 5u 90u 35u 15u "Database:"
	Pop $DatabaseLabel

	${NSD_CreateText} 45u 92u 100u 12u "kgp"
	Pop $DatabaseText

	nsDialogs::Show

skipThis:
FunctionEnd

Function nsUserPassLeave

	${NSD_GetText} $UserText	$theUser
	${NSD_GetText} $PasswordText	$thePass
	${NSD_GetText} $DatabaseText	$theDatabase
FunctionEnd

Function nsEditConfigFilesEnter

        SectionGetFlags ${ServerSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipDefaultServer skipDefaultServer

	; Edit the defaultserver.xml with the install location.

	Push "$Content_InstDir"
	Push "$INSTDIR\Blades\DefaultServer.xml"
	CallInstDLL $INSTDIR\bin\nsConfig.dll updateDefaultServerXML

	Push "$Content_InstDir"
	Push "$INSTDIR\Blades\DefaultAIServer.xml"
	CallInstDLL $INSTDIR\bin\nsConfig.dll updateDefaultServerXML

	skipDefaultServer:

        SectionGetFlags ${ServerSharedSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipDBCfg skipDBCfg

	Push "$theDatabase"
	Push "$thePass"
	Push "$theUser"
	Push "$Content_InstDir"
	CallInstDLL $INSTDIR\bin\nsConfig.dll updateDbCfg

	skipDBCfg:

FunctionEnd

Var KGPServiceDesc
Var KGPServiceInstall
Var KGPServiceStartNow

Var HLine

Var KGPControllerDesc
Var KGPControllerInstall
Var KGPControllerAutoStart
Var KGPControllerStartNow

Var KGP_SERVER_RUNNING
Var KGP_CONTROLLER_RUNNING

Var RunningServerLabel
Var RunningControllerLabel
Var RunningServicesMsg

Function nsServiceInstallsEnter

        SectionGetFlags ${ServerSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipThis skipThis

	nsDialogs::Create /NOUNLOAD 1018
	Pop $Dialog

	${If} $Dialog == error
		Abort
	${EndIf}

        StrCpy $KGP_SERVER_RUNNING "N"
        StrCpy $KGP_CONTROLLER_RUNNING "N"

	; disable back button
	Call DisableBackButton

	GetDlgItem $0 $HWNDPARENT 1037
	SendMessage $0 ${WM_SETTEXT} 0 "STR:STAR services configuration"

	GetDlgItem $0 $HWNDPARENT 1038
	SendMessage $0 ${WM_SETTEXT} 0 "STR:Configure the STAR servers as Windows services"

	nsSCM::QueryStatus "KGPServer"
        Pop $0 ; return error/success
	Pop $1 ; return service status

	StrCmp $0 success kgpservice_installed
	goto kgpservice_notinstalled

	kgpservice_notinstalled:
		${NSD_CreateLabel} 5u 6u 160u 12u "STAR Server - not currently installed"
		Pop $KGPServiceDesc
		goto nextkgpcontrol

	kgpservice_installed:
		IntCmp $1 ${SERVICE_RUNNING} kgpservice_running kgpservice_notrunning kgpservice_notrunning

	kgpservice_running:
		${NSD_CreateLabel} 5u 6u 160u 12u "STAR Server - currently running"
		Pop $KGPServiceDesc
		StrCpy $KGP_SERVER_RUNNING "Y"
		goto nextkgpcontrol

	kgpservice_notrunning:
		${NSD_CreateLabel} 5u 6u 160u 12u "STAR Server - currently installed, not running"
		Pop $KGPServiceDesc
		goto nextkgpcontrol

	nextkgpcontrol:

	${NSD_CreateCheckBox} 5u 18u 160u 12u "Install the server as a Windows service"
	Pop $KGPServiceInstall
	SendMessage $KGPServiceInstall ${BM_SETCHECK} 1 0
        ${NSD_OnClick} $KGPServiceInstall KGPServiceInstallClicked

	${NSD_CreateCheckbox} 15u 33u 120u 12u "Start the service now"
	Pop $KGPServiceStartNow
	;SendMessage $KGPServiceStartNow ${BM_SETCHECK} 1 0


	${NSD_CreateHLine} 5u 51u 240u 12u "S"
	Pop $HLine

	nsSCM::QueryStatus "KGPController"
        Pop $0 ; return error/success
	Pop $1 ; return service status

	StrCmp $0 success kgpcontroller_installed
	goto kgpcontroller_notinstalled

	kgpcontroller_notinstalled:
		${NSD_CreateLabel} 5u 59u 160u 12u "STAR Controller - not currently installed"
		Pop $KGPControllerDesc
		goto nextcontrollercontrol

	kgpcontroller_installed:
		IntCmp $1 ${SERVICE_RUNNING} kgpcontroller_running kgpcontroller_notrunning kgpservice_notrunning

	kgpcontroller_running:
		${NSD_CreateLabel} 5u 59u 160u 12u "STAR Controller - currently running"
		Pop $KGPControllerDesc
		StrCpy $KGP_CONTROLLER_RUNNING "Y"
		goto nextcontrollercontrol

	kgpcontroller_notrunning:
		${NSD_CreateLabel} 5u 59u 160u 12u "STAR Controller - currently installed, not running"
		Pop $KGPControllerDesc
		goto nextcontrollercontrol

	nextcontrollercontrol:

	${NSD_CreateCheckBox} 5u 71u 160u 12u "Install the controller as a Windows service"
	Pop $KGPControllerInstall
	SendMessage $KGPControllerInstall ${BM_SETCHECK} 1 0
        ${NSD_OnClick} $KGPControllerInstall KGPControllerInstallClicked

	${NSD_CreateCheckBox} 15u 86u 120u 12u "Set service to Autostart"
	Pop $KGPControllerAutoStart
	SendMessage $KGPControllerAutoStart ${BM_SETCHECK} 1 0

	${NSD_CreateCheckbox} 15u 101u 120u 12u "Start the service now"
	Pop $KGPControllerStartNow
	;SendMessage $KGPControllerStartNow ${BM_SETCHECK} 1 0

	StrCpy $RunningServicesMsg "Currently running STAR services will be stopped before installing."

	StrCmp $KGP_SERVER_RUNNING "Y" kgp_server_running
	goto check_controller_running
	kgp_server_running:
		${NSD_CreateLabel} 5u 126u 220u 12u "$RunningServicesMsg"
		Pop $RunningServerLabel
	check_controller_running:
		StrCmp $KGP_CONTROLLER_RUNNING "Y" kgp_controller_running
		goto showthedialog
	kgp_controller_running:
		${NSD_CreateLabel} 5u 126u 220u 12u "$RunningServicesMsg"
		Pop $RunningControllerLabel
		goto showthedialog

	showthedialog:
		nsDialogs::Show

skipThis:
FunctionEnd

Var KGPServiceInstallState
Var KGPServiceStartNowState

Var KGPControllerInstallState
Var KGPControllerAutoStartState
Var KGPControllerStartNowState

Var MaxKGPServerStopTries
Var MaxKGPControllerStopTries

Function nsServiceInstallsLeave

	${NSD_GetState} $KGPServiceInstall $KGPServiceInstallState
	${NSD_GetState} $KGPServiceStartNow $KGPServiceStartNowState

	${NSD_GetState} $KGPControllerInstall $KGPControllerInstallState
	${NSD_GetState} $KGPControllerAutoStart $KGPControllerAutoStartState
	${NSD_GetState} $KGPControllerStartNow $KGPControllerStartNowState

	; disable next button
	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 0

	nsSCM::QueryStatus "KGPServer"
	Pop $0 ; return error/success
	Pop $1 ; return service status
	StrCmp $0 success 0 +6
	IntCmp $1 ${SERVICE_RUNNING} 0 +5 +5
	nsSCM::Stop /NOUNLOAD "KGPServer"
        Pop $0 ; return error/success
	StrCmp $0 success +2 0
	MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPServer service could not be stopped."

	nsSCM::QueryStatus "KGPController"
	Pop $0 ; return error/success
	Pop $1 ; return service status
	StrCmp $0 success 0 +6
	IntCmp $1 ${SERVICE_RUNNING} 0 +5 +5
	nsSCM::Stop /NOUNLOAD "KGPController"
        Pop $0 ; return error/success
	StrCmp $0 success +2 0
	MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPController service could not be stopped."

        IntCmp $KGPServiceInstallState 1 KGPServiceInstall_checked KGPServiceInstall_unchecked KGPServiceInstall_unchecked

	KGPServiceInstall_unchecked:
		nsSCM::QueryStatus "KGPServer"
		Pop $0 ; return error/success
		Pop $1 ; return service status
		StrCmp $0 success 0 controller_service
		nsSCM::Remove /NOUNLOAD "KGPServer"
		Pop $0 ; return error/success
		StrCmp $0 success controller_service
		MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPServer service could not be removed."
		Abort

	KGPServiceInstall_checked:

		nsSCM::QueryStatus "KGPServer"
		Pop $0 ; return error/success
		Pop $1 ; return service status
		StrCmp $0 success 0 +6
		;nsSCM::Start /NOUNLOAD "KGPController"
		nsSCM::Remove /NOUNLOAD "KGPServer"
		Pop $0 ; return error/success
		StrCmp $0 success +3 0
		MessageBox MB_OK|MB_ICONEXCLAMATION "The previously installed KGPServer service could not be removed."
		Abort

                IntOp $MaxKGPServerStopTries 0 + 0

		checkKGPServerStopStatus:
		nsSCM::QueryStatus "KGPServer"
		Pop $0 ; return error/success
		Pop $1 ; return service status
		StrCmp $0 success 0 +6
		IntCmp $1 ${SERVICE_STOPPED} +5 0 0
                IntOp $MaxKGPServerStopTries $MaxKGPServerStopTries + 1
		IntCmp $MaxKGPServerStopTries 20 +3 0 0 ;Wait 20 seconds for it to stop
		Sleep 1000
		goto checkKGPServerStopStatus

		nsSCM::Install /NOUNLOAD "KGPServer" "KGPServer" 16 3 \
				   "$INSTDIR\bin\KGPServer" "" "" "$strWindowsUser" "$strWindowsPass"

		Pop $0 ; return error/success
		StrCmp $0 success controller_service
		MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPServer service could not be installed."
		Abort

	controller_service:

        IntCmp $KGPControllerInstallState 1 KGPControllerInstall_checked KGPControllerInstall_unchecked KGPControllerInstall_unchecked

	KGPControllerInstall_unchecked:
		nsSCM::QueryStatus "KGPController"
		Pop $0 ; return error/success
		Pop $1 ; return service status
		StrCmp $0 success 0 kgpservice_start
		nsSCM::Remove /NOUNLOAD "KGPController"
		Pop $0 ; return error/success
		StrCmp $0 success kgpservice_start
		MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPController service could not be removed."
		Abort

	KGPControllerInstall_checked:

		nsSCM::QueryStatus "KGPController"
		Pop $0 ; return error/success
		Pop $1 ; return service status
		StrCmp $0 success 0 +6
		nsSCM::Remove /NOUNLOAD "KGPController"
		Pop $0 ; return error/success
		StrCmp $0 success +3 0
		MessageBox MB_OK|MB_ICONEXCLAMATION "The previously installed KGPController service could not be removed."
		Abort

		IntCmp $KGPControllerAutoStartState 1 KGPControllerAutoStart_checked KGPControllerAutoStart_unchecked KGPControllerAutoStart_unchecked

		KGPControllerAutoStart_checked:

			IntOp $MaxKGPControllerStopTries 0 + 0

			checkKGPControllerStopStatus1:
			nsSCM::QueryStatus "KGPController"
			Pop $0 ; return error/success
			Pop $1 ; return service status
			StrCmp $0 success 0 +6
			IntCmp $1 ${SERVICE_STOPPED} +5 0 0
			IntOp $MaxKGPControllerStopTries $MaxKGPControllerStopTries + 1
			IntCmp $MaxKGPControllerStopTries 20 +3 0 0 ;Wait 20 seconds for it to stop
			Sleep 1000
			goto checkKGPControllerStopStatus1

			nsSCM::Install /NOUNLOAD "KGPController" "KGPController" 16 2 \
					   "$INSTDIR\bin\KGPController" "" "" "$strWindowsUser" "$strWindowsPass"

			Pop $0 ; return error/success
			StrCmp $0 success kgpservice_start
			MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPController service could not be installed."
			Abort

		KGPControllerAutoStart_unchecked:

			IntOp $MaxKGPControllerStopTries 0 + 0

			checkKGPControllerStopStatus2:
			nsSCM::QueryStatus "KGPController"
			Pop $0 ; return error/success
			Pop $1 ; return service status
			StrCmp $0 success 0 +6
			IntCmp $1 ${SERVICE_STOPPED} +5 0 0
			IntOp $MaxKGPControllerStopTries $MaxKGPControllerStopTries + 1
			IntCmp $MaxKGPControllerStopTries 20 +3 0 0 ;Wait 20 seconds for it to stop
			Sleep 1000
			goto checkKGPControllerStopStatus2

			nsSCM::Install /NOUNLOAD "KGPController" "KGPController" 16 3 \
					   "$INSTDIR\bin\KGPController" "" "" "$strWindowsUser" "$strWindowsPass"

			Pop $0 ; return error/success
			StrCmp $0 success kgpservice_start
			MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPController service could not be installed."
			Abort

	kgpservice_start:

	IntCmp $KGPServiceStartNowState 1 KGPServiceStartNowState_checked kgpcontroller_start kgpcontroller_start

	KGPServiceStartNowState_checked:
		nsSCM::Start /NOUNLOAD "KGPServer"
		Pop $0 ; return error/success
		StrCmp $0 success kgpcontroller_start
		MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPServer service could not started."
		Abort

	kgpcontroller_start:

		IntCmp $KGPControllerStartNowState 1 KGPControllerStartNowState_checked end end

		KGPControllerStartNowState_checked:
			nsSCM::Start /NOUNLOAD "KGPController"
			Pop $0 ; return error/success
			StrCmp $0 success end
			MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPController service could not started."
			Abort
	end:

FunctionEnd

Function KGPServiceInstallClicked

	${NSD_GetState} $KGPServiceInstall $KGPServiceInstallState

        IntCmp $KGPServiceInstallState 1 KGPServiceInstall_checked KGPServiceInstall_unchecked KGPServiceInstall_unchecked

	KGPServiceInstall_unchecked:
		SendMessage $KGPServiceStartNow ${BM_SETCHECK} 0 0
		EnableWindow $KGPServiceStartNow 0
		goto end
	KGPServiceInstall_checked:
		EnableWindow $KGPServiceStartNow 1
		goto end
	end:
FunctionEnd

Function KGPControllerInstallClicked

	${NSD_GetState} $KGPControllerInstall $KGPControllerInstallState

        IntCmp $KGPControllerInstallState 1 KGPControllerInstall_checked KGPControllerInstall_unchecked KGPControllerInstall_unchecked

	KGPControllerInstall_unchecked:
		SendMessage $KGPControllerAutoStart ${BM_SETCHECK} 0 0
		SendMessage $KGPControllerStartNow ${BM_SETCHECK} 0 0
		EnableWindow $KGPControllerAutoStart 0
		EnableWindow $KGPControllerStartNow 0
		goto end
	KGPControllerInstall_checked:
		EnableWindow $KGPControllerAutoStart 1
		EnableWindow $KGPControllerStartNow 1
		goto end
	end:
FunctionEnd

Function nsDirectorySelect_Binaries
	; nop for now
FunctionEnd

Function nsDirectorySelect_Content

	; prompt for install location for content

	StrCpy $Content_InstDir ${ContentInstallDir}
        SectionGetFlags ${ServerSharedSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipDirectory skipDirectory
	goto showDirectory
	skipDirectory:
	  Abort
	showDirectory:
	  ; allow content directory page to be shown

FunctionEnd

; This callback method will be called right before the user enters the page where he can select
; where to place their patch files.  This callback method will set the initial patch directory
; to whatever is in the global variable PatchInstallDir.
Function nsDirectorySelect_PatchLocation

	; prompt for install location for content
        GetDlgItem $0 $HWNDPARENT 1038
	SendMessage $0 ${WM_SETTEXT} 0 "STR:"

	StrCpy $Patch_InstDir ${PatchInstallDir}
        SectionGetFlags ${ServerSharedSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 skipDirectory skipDirectory
	goto showDirectory
	skipDirectory:
	  Abort
	showDirectory:
	  ; allow content directory page to be shown

FunctionEnd

Function RelGotoPage
  IntCmp $R9 0 0 Move Move
    StrCmp $R9 "X" 0 Move
      StrCpy $R9 "120"

  Move:
  SendMessage $HWNDPARENT "0x408" "$R9" ""
FunctionEnd
; This callback method will be called when the user leaves the patch files directory page.
; This callback method will check to see if the patch file directory chosen already exists.
; If it does, the user will be asked whether they want to delete that directory ot not.
Function nsDirectorySelect_PatchLocation_Validate
         strcpy $bOverwritePatchFiles "true"
         strcpy $bSetupGameServer "false"
         strcpy $bSetupPatchURL "false"

         IfFileExists $Patch_InstDir +1 end
         MessageBox MB_YESNO "The $Patch_InstDir directory already exists.$\r$\nWould you like to delete these files?" IDYES end IDNO +1
         strcpy $bOverwritePatchFiles "false"
         end:

         ;Add these back when wok web calls gamelogin and addGameServer are on prod.
         ;MessageBox MB_YESNO "Would you like to register your STAR's patch URL? $\r$\nThis will allow players to download the STAR client software from your computer." IDYES SETUP_PATCH_URL IDNO SKIP_PATCH_URL_DIALOG

         ;SETUP_PATCH_URL:
         ;                strcpy $bSetupPatchURL "true"
         ;                goto DONE

         ;SKIP_PATCH_URL_DIALOG:
         ;                MessageBox MB_YESNO "Would you like to register this computer as a game server?" IDYES 0 IDNO DONE
         ;                strcpy $bSetupGameServer "true"

         ;DONE:

FunctionEnd

Function nsDirectorySelect_ContentLocation

	; prompt for where content was previously installed

        SectionGetFlags ${ServerSharedSect} $0
        IntOp $0 $0 & ${SF_SELECTED}
        IntCmp $0 ${SF_SELECTED} 0 showDirectory showDirectory
	Abort
	showDirectory:
	  StrCpy $Content_InstDir ${ContentInstallDir}
	  ; allow content directory page to be shown

FunctionEnd

Function nsDirectorySelect_ContentLocation_Validate

	; Validate the content directory provided. Look for versioninfo.dat.

	ClearErrors
        ${Locate} "$Content_InstDir" "/L=F /M=versioninfo.dat" "nsDirectorySelect_ContentLocation_Validate_Callback"
	IfErrors +2 0
	StrCmp $R0 StopLocate end 0
	MessageBox MB_OK|MB_ICONEXCLAMATION "Content directory $Content_InstDir does not contain the file versioninfo.dat. Please provide a valid content directory."
	Abort
	end:

FunctionEnd

Function nsDirectorySelect_ContentLocation_Validate_Callback
	StrCpy $R0 $R9
	StrCpy $R0 StopLocate
	Push $R0
FunctionEnd

Function DisableBackButton
     GetDlgItem $0 $HWNDPARENT 3
     EnableWindow $0 0
FunctionEnd

var dlgGetKanevaCredentials
var ctrlKanevaUsername
var ctrlKanevaPassword
;  If the user decides to add a game server/ patch url to the developer site from the installer,
;  we will need to get their kaneva credentials to log into the developer site.
function dlgOnGetKanevaCredentialsCreate

              strcmp $bSetupGameServer "true" START +1
              strcmp $bSetupPatchURL "true" +1 DONE

              START:
              nsDialogs::Create /NOUNLOAD 1018
	      Pop $dlgGetKanevaCredentials

	      ${If} $dlgGetKanevaCredentials == error
	            Abort
	      ${EndIf}

	      GetDlgItem $0 $HWNDPARENT 1037
	      SendMessage $0 ${WM_SETTEXT} 0 "STR:Login"

	      GetDlgItem $0 $HWNDPARENT 1038
	      SendMessage $0 ${WM_SETTEXT} 0 "STR:"

              ${NSD_CreateLabel} 10u 0u -30u 12u "Kaneva Login: "
              ${NSD_CreateText} 10u 20u 100u 12u ""
              pop $ctrlKanevaUsername

              ${NSD_CreateLabel} 10u 40u -30u 12u "Kaneva Password: "
              ${NSD_CreatePassword} 10u 60u 100u 12u ""
              pop $ctrlKanevaPassword

              nsDialogs::Show
        DONE:
functionend

;  This function is used in the addGameServer method to store output from the developer website
;  in a localfile for processing.  Afterwards the output file can be discarded.
function readAndDiscardFile
         pop $4
         strcpy $1 ""
         FileOpen $0 $4 r
         IfErrors close
         FileRead $0 $1
         FileClose $0
         DONE:
         IfFileExists $4 +1 +2
         delete $4

         close:
         push $1
functionend

function dlgOnGetKanevaCredentialsLeave
         ${NSD_GetText} $ctrlKanevaUsername $0
         strcpy $strKanevaUserName $0
         ${NSD_GetText} $ctrlKanevaPassword $0
         strcpy $strKanevaPassword $0

         ${If} $strKanevaUserName == ""
               MessageBox MB_OK|MB_ICONSTOP "Please key in your Kaneva account username."
               abort
         ${ElseIf} $strKanevaPassword == ""
               MessageBox MB_OK|MB_ICONSTOP "Please key in your Kaneva account password."
               abort
         ${Endif}

         strcpy $4 "response.txt"
         inetc::get "${WOK_WEB}/kgp/gameLogin.aspx?user=$strKanevaUserName&get=user" $4
         push $4
         call readAndDiscardFile
         pop $0
         ${If} $0 == "2"
               MessageBox MB_OK|MB_ICONSTOP "The email address or password you provided was incorrect. Please verify your login credentials and try again."
               abort
         ${Endif}
         inetc::get "${WOK_WEB}/kgp/validateuser.aspx?user=$0&pw=$strKanevaPassword" $4
         push $4
         call readAndDiscardFile
         pop $0
         ${If} $0 != "<resp>ok</resp>"
               MessageBox MB_OK|MB_ICONSTOP "The email address or password you provided was incorrect. Please verify your login credentials and try again."
               abort
         ${Endif}

         ; Add a game Server if we aren't going to add a patch url.
         ${If} $bSetupGameServer == "true"
               strcpy $0 $strKanevaUserName
               push $0
               strcpy $0 $strKanevaPassword
               push $0
               call getComputerName
               strcpy $0 ""
               push $0
               call addGameServer
               pop $0
               strcmp $0 "success" 0 error
               goto next
               error:
               MessageBox MB_OK|MB_ICONSTOP "Error registering your game server. Please verify that your Kaneva login email and password are correct."
               abort
               next:
         ${Endif}
functionend

var dlgGetPatchURL
var ctrlPatchURL
Function dlgOnGetPatchURLCreate
        ${If} $bSetupPatchURL == "true"
               	       nsDialogs::Create /NOUNLOAD 1018
	       	       Pop $dlgGetPatchURL

		       ${If} $dlgGetPatchURL == error
			       Abort
		       ${EndIf}

		       GetDlgItem $0 $HWNDPARENT 1037
		       SendMessage $0 ${WM_SETTEXT} 0 "STR:Enter your Patch URL"

		       GetDlgItem $0 $HWNDPARENT 1038
		       SendMessage $0 ${WM_SETTEXT} 0 "STR:"

        	       ${NSD_CreateLabel} 10u 0u -30u 30u "Please provide the Patch URL $\r$\n(examples: http://www.mycomputer.com/Star/PatchFiles or http://192.138.1.1/Star/PatchFiles)"
        	       ${NSD_CreateText} 10u 35u -30u 12u "http://"
        	       pop $ctrlPatchURL

        	       nsDialogs::Show
        ${Endif}
FunctionEnd

Function dlgOnGetPatchURLLeave
       strcpy $0 $strKanevaUserName
       push $0
       strcpy $0 $strKanevaPassword
       push $0
       ;call getComputerName
       strcpy $0 ""
       push $0
       ${NSD_GetText} $ctrlPatchURL $0
       push $0
       call addGameServer
       pop $0
       strcmp $0 "success" 0 ERROR_ADDING_PATCH_URL
       goto next
       ERROR_ADDING_PATCH_URL:
       MessageBox MB_OK|MB_ICONSTOP "Error registering your patch URL. Please verify that your Kaneva login email and password are correct."
       abort
       next:

       MessageBox MB_YESNO "Would you like to register this computer as a game server?" IDYES SETUP_GAME_SERVER IDNO FINISH

       SETUP_GAME_SERVER:
       strcpy $0 $strKanevaUserName
       push $0
       strcpy $0 $strKanevaPassword
       push $0
       call getComputerName
       strcpy $0 ""
       push $0
       call addGameServer
       pop $0
       strcmp $0 "success" 0 ERROR_ADDING_GAME_SERVER
       goto FINISH
       ERROR_ADDING_GAME_SERVER:
       MessageBox MB_OK|MB_ICONSTOP "Error registering your game server. Please verify that your Kaneva login email and password are correct."
       abort

       FINISH:

FunctionEnd

var strKanevaGameServer
var strKanevaGamePatchURL
var strGameID
; This function will add a GameServer and/or patch url to the developer website.
function addGameServer
         pop $3 ; Get the Patch URL if it exists
         StrCpy $strKanevaGamePatchURL $3
         pop $2 ; Get the Game Server Name if it exists
         StrCpy $strKanevaGameServer $2
         pop $1 ; Get the Password
         StrCpy $strKanevaPassword $1
         pop $0 ; Get the Username
         StrCpy $strKanevaUserName $0

         strcpy $3 "error"
         strcpy $4 "response.txt"
         ; Normally the ${GAME_ID} variable will hold th game id given from the star editor during the
         ; publishing process.  However for debugging purposes,  use the $strGameID variable.
         ;strcpy $strGameID "5301"

         strcmp $strKanevaGameServer "" ADD_PATCH_URL_ONLY +1
         strcmp $strKanevaGamePatchURL "" ADD_SERVER_ONLY ADD_SERVER_AND_PATCH_URL

         ADD_PATCH_URL_ONLY:
         strcmp $strKanevaGamePatchURL "" DONE +1
         inetc::get "${WOK_WEB}/kgp/gameAddServer.aspx?gameId=${GAME_ID}&patchUrl=$strKanevaGamePatchURL&user=$strKanevaUserName&pw=$strKanevaPassword" $4
         goto PROCESS_REQUEST

         ADD_SERVER_ONLY:
         inetc::get "${WOK_WEB}/kgp/gameAddServer.aspx?gameId=${GAME_ID}&hostName=$strKanevaGameServer&user=$strKanevaUserName&pw=$strKanevaPassword" $4
         goto PROCESS_REQUEST

         ADD_SERVER_AND_PATCH_URL:
         inetc::get "${WOK_WEB}/kgp/gameAddServer.aspx?gameId=${GAME_ID}&hostName=$strKanevaGameServer&patchUrl=$strKanevaGamePatchURL&user=$strKanevaUserName&pw=$strKanevaPassword" $4

         PROCESS_REQUEST:
         FileOpen $0 $4 r
         IfErrors close
         FileRead $0 $1
         strcmp $1 "0" +1 close
         strcpy $3 "success"
         close:
         FileClose $0
         strcpy $0 $3
         DONE:
         IfFileExists $4 +1 +2
         delete $4
         push $0
functionend

; This function is used in the addGameServer function
function getComputerName
  ReadRegStr $0 HKLM "SYSTEM\CurrentControlSet\Services\Tcpip\Parameters" "Hostname"
  push $0
functionend


!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${ServerSect} $(DESC_ServerSect)
  !insertmacro MUI_DESCRIPTION_TEXT ${ServerSharedSect} $(DESC_ServerSharedSect)
  !insertmacro MUI_DESCRIPTION_TEXT ${AdminUISect} $(DESC_AdminUISect)
  !insertmacro MUI_DESCRIPTION_TEXT ${MySqlSect} $(DESC_MySqlSect)
  !insertmacro MUI_DESCRIPTION_TEXT ${MySqlScriptsSect} $(DESC_MySqlScriptsSect)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;Section -AdditionalIcons
;  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
;  CreateShortCut "$SMPROGRAMS\Kaneva\STAR\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
;  CreateShortCut "$SMPROGRAMS\Kaneva\STAR\Uninstall.lnk" "$INSTDIR\uninst.exe"
;SectionEnd


Section -Post
!define PUBLISH_GOAL13 "Generating Uninstaller..."
  WriteUninstaller "$INSTDIR\uninst.exe"
  ;MessageBox MB_OK "$INSTDIR\uninst.exe"
;  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\bin\KGPServer.exe"
;  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  ;WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
;  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\bin\KGPServer.exe"
;  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
;  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
;  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"

;  Delete "$INSTDIR\Vcredist_x86.exe"
;  RMDir /r "$INSTDIR\bin\mysql"

SectionEnd


;Function un.onUninstSuccess
;  HideWindow
;  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
;FunctionEnd

;Function un.onInit
;  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
;  Abort
;FunctionEnd

Section Uninstall
SetAutoClose true

nsSCM::Stop /NOUNLOAD "KGPServer"
nsSCM::Stop /NOUNLOAD "KGPController"
nsSCM::Remove /NOUNLOAD "KGPServer"
nsSCM::Remove /NOUNLOAD "KGPController"
MessageBox MB_YESNO|MB_ICONQuestion "Your computer needs to restarted.  Would you like to restart your machine now?" IDYES YES_REBOOT IDNO EXIT


YES_REBOOT:
           reboot

EXIT:

;  Call un.DeleteVDir
;
;  DetailPrint "SKIPPING Stopping services..."
;  ;DetailPrint "Stopping services..."
;  ;ExecWait 'net stop KGPServer'
;  ;ExecWait 'net stop KGPController'
;
;  DetailPrint "SKIPPING Uninstalling services..."
;
;  ; DetailPrint "Uninstalling services..."
;  ; commented out for WOK uninstall
;  ; ExecWait '"$INSTDIR\bin\KGPServer.exe" -u'
;  ; ExecWait '"$INSTDIR\bin\KGPController.exe" -u'
;
;  Delete "$INSTDIR\${PRODUCT_NAME}.url"
;  Delete "$INSTDIR\uninst.exe"
;  Delete "$INSTDIR\STAR_ReadMe.htm"
;  Delete "$INSTDIR\server_readme.htm"
;  Delete "$INSTDIR\editor_readme.htm"
;
;  ; pick what to delete now
;  Delete "$INSTDIR\bin\*.dll"
;  Delete "$INSTDIR\bin\*.bat"
;  Delete "$INSTDIR\bin\*.exe"
;  Delete "$INSTDIR\bin\*.dtd"
;  Delete "$INSTDIR\bin\*.vbs"

;  Delete "$INSTDIR\blades\*.dll"  ; only delete the DLLs, preserve any XML files they may have

;  Delete "$SMPROGRAMS\Kaneva\STAR\Uninstall.lnk"
;  Delete "$SMPROGRAMS\Kaneva\STAR\Website.lnk"
;  Delete "$SMPROGRAMS\Kaneva\STAR\Start the server service.lnk"
;  Delete "$SMPROGRAMS\Kaneva\STAR\Stop the server service.lnk"
;  Delete "$SMPROGRAMS\Kaneva\STAR\Editor.lnk"
;  Delete "$SMPROGRAMS\Kaneva\STAR\KEPSDK.lnk"

;  ; /r means recursively delete
;  ; don't do /r on bin to keep XML, and config files
;  RMDir "$INSTDIR\bin"
;  RMDir /r "$INSTDIR\bin\lang"
;  RMDir /r "$INSTDIR\bin\help"
;  RMDir /r "$INSTDIR\bin\images"
;  RMDir /r "$INSTDIR\bin\python"

;  RMDir "$INSTDIR\blades"

;  RMDir /r "$INSTDIR\KEPAdmin"

;  ; keep logs RMDir /r "$INSTDIR\Logs"
;  RMDir /r "$INSTDIR\templates"

;  RMDir /r "$INSTDIR\sqlscripts"

;  ; only cleanup rest if it is empty
;  RMDir "$INSTDIR\"
;  RMDir "$SMPROGRAMS\Kaneva\STAR"


;  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
;  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
;  SetAutoClose true
!define PUBLISH_GOAL14 "Finishing Up..."
SectionEnd

