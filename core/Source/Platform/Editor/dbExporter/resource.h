//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by dbExporter.rc
//
#define IDB_TEST                        3
#define IDB_TEST2                       4
#define IDB_SAVE                        4
#define IDC_SERVER                      2000
#define IDD_DBSETTINGS                  2001
#define IDC_PORT                        2001
#define IDC_USER                        2002
#define IDC_PASSWORD                    2003
#define IDC_DBNAME                      2004
#define IDC_DONTSHOW                    2005
#define IDS_CLIENT_ENGINE_MISMATCH      3600
#define IDS_DBEXPORTER                  3601
#define IDS_EXPORT_EXCEPTION            3602
#define IDS_EXPORT_NOCONNECT            3603
#define IDS_EDIT_NOCONNECT              3604
#define IDS_EDIT_CONNECTOK              3605
#define IDS_EXPORT_QUERY_ERROR          3606
#define IDS_EXPORT_CONVERSION_ERROR     3607
#define IDS_EXPORT_ERROR                3608
#define IDS_CONFIRM_EXPORT              3609
#define IDS_CONFIG_SAVED_FAILED         3610
#define IDS_CONFIG_LOAD_FAILED          3611
// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2002
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2006
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
