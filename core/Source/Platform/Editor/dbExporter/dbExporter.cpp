#include "stdafx.h"
#include <mysql++/mysql++.h>
#include "Common/include/kepcommon.h"
#include "dbExporter.h"
#include "resource.h"
#include "DBSettingsDlg.h"
#ifndef DECOUPLE_DBCONFIG
#else
#include "Common/include/DBConfigExporter.h"
#endif
#include "DBConfigValues.h"
#include <KEPHelpers.h>
#include <KEPConfig.h>
#include <IGetSet.h>
#include <KEPException.h>
#include <sys/stat.h>
#include <DBStrings.h>
#include <jsEnumFiles.h>
//#include <log4cplus/logger.h>
#include <event/glue/KEPEditViewIds.h>

#include "common\KEPUtil\Helpers.h"

using namespace KEP;
using namespace mysqlpp;
using namespace log4cplus;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// avoid scanning for too early of version updates
#define FIRST_STAR_SCHEMA_VER 45

bool connect(Connection& con, const char* dbServer, const char* dbUser, const char* dbPassword,
	const char* dbName, ULONG dbPort);

// CdbExporterApp

BEGIN_MESSAGE_MAP(CdbExporterApp, CWinApp)
END_MESSAGE_MAP()

#include "../../../StarInstall/MySqlSetup/MySqlSetupImports.h"
#include "../../../StarInstall/MySqlSetup/MySqlSetupException.h"

// CdbExporterApp construction

CdbExporterApp::CdbExporterApp() {
}

// The one and only CdbExporterApp object

CdbExporterApp theApp;

// CdbExporterApp initialization

BOOL CdbExporterApp::InitInstance() {
	CWinApp::InitInstance();

	return TRUE;
}

bool exportToDb(HWND parent, IGetSet* engine, Connection& con, const char* exportSqlPath, FILE* fp_exporter);
#ifndef DECOUPLE_DBCONFIG
#else
enum EshowValue {
	showDialog, // always show it
	useConfigToShow,
	onlyShowIfNeeded,
	showDialogHadError,
};
#endif
INT doDialog(HWND parent, const char* gameDir, string& dbServer, string& dbUser, string& dbPassword, string& dbName,
	int& dbPort, EshowValue showValue) {
	bool loaded = false, saved = false;
	string dbConfigPath = PathAdd(gameDir, DB_CFG_NAME);

	// Load configurations from file
	KEPConfig config;
	DBConfig dbConfig(config);

	try {
		config.Load(dbConfigPath.c_str(), DB_CFG_ROOT_NODE);

		if (config.IsLoaded()) {
			bool dontShowDlg;
			if (dbConfig.Get(dbServer, dbPort, dbName, dbUser, dbPassword, dontShowDlg)) {
				if (showValue == showDialogHadError)
					dontShowDlg = false;

				loaded = true;

				// Skip dialog if not needed
				if (showValue == onlyShowIfNeeded ||
					(showValue == useConfigToShow && dontShowDlg))
					return IDOK;
			}
		}
	} catch (KEPException* e) {
		delete e;
	} catch (KEPException&) {
	}

	if (!loaded) {
		// Loading failed
		AfxMessageBox(IDS_CONFIG_LOAD_FAILED, MB_ICONEXCLAMATION);
		return IDCANCEL;
	}

	// Display configuration dialog
	CDbSettingsDlg dlg(CWnd::FromHandle(parent));

	// Send data to dialog
	dlg.m_server = dbServer.c_str();
	dlg.m_user = dbUser.c_str();
	dlg.m_password = dbPassword.c_str();
	dlg.m_name = dbName.c_str();
	dlg.m_port = dbPort;

	// open the dialog to get the database settings
	INT ret = dlg.DoModal();
	if (ret == IDOK || ret == IDB_SAVE) {
		// save off return values
		dbServer = dlg.m_server.GetString();
		dbUser = dlg.m_user.GetString();
		dbPassword = dlg.m_password.GetString();
		dbName = dlg.m_name.GetString();
		dbPort = dlg.m_port;

		try {
#ifndef DECOUPLE_DBCONFIG
			if (DBConfig::Export(config, dbServer.c_str(), dbPort, dbName.c_str(), dbUser.c_str(), dbPassword.c_str(), dlg.m_dontShowAgain) &&
#else
			if (DBConfigExporter::Export(config, dbServer.c_str(), dbPort, dbName.c_str(), dbUser.c_str(), dbPassword.c_str(), dlg.m_dontShowAgain) &&
#endif
				config.Save()) {
				saved = true;
			}
		} catch (KEPException* e) {
			delete e;
		} catch (KEPException&) {
		}

		if (!saved) {
			// Saving failed
			AfxMessageBox(IDS_CONFIG_SAVED_FAILED, MB_ICONEXCLAMATION);
		}
	}

	return ret;
}

extern "C" __declspec(dllexport) INT EditDbSettings(HWND parent, const char* gameDir, bool checkConfigToShow) {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	string dbServer, dbUser, dbPassword, dbName;
	int dbPort;

	return doDialog(parent, gameDir, dbServer, dbUser, dbPassword, dbName, dbPort, checkConfigToShow ? useConfigToShow : showDialog);
}

const char* const DB_MASTER_FILENAME = "__dbMaster.temp";

/*!
 * \brief
 * figures out what scripts to execute to update the schema and executes them in order
 * 
 * \param dbVer
 * existing version of schema
 * 
 * \param path
 * path to update scripts.
 * 
 * \param mask
 * filename mask for update files
 * 
 * \param parent
 * parent window
 * 
 * \param statusWnd
 * window for setting text on for progress msgs
 * 
 * \param dbName
 * db name
 * 
 * \param dbServer
 * db server name or ip
 * 
 * \param dbPort
 * db server's port
 * 
 * \param dbUser
 * user id for db
 * 
 * \param dbPassword
 * password for db
 * 
 * \param fp_export
 * file handle of server export/update file
 * 
 * \returns
 * string of file if error, else empty string
 * 
 * This will look in the folder for v*_update.sql where * is a number and will
 * run them in order.  Do not do USE unless you reset it since another script may not like it.
 * See twiki page for details about scripts
 * 
 */
#define CORE_MASK_STRING "template_promotepolicy_v"
#define USER_MASK_STRING "user_promotepolicy_v"

static string runUpdateScripts(Logger& logger, int dbVer, const char* path, const char* mask, HWND parent, HWND statusWnd,
	const char* dbName,
	const char* dbServer, INT dbPort,
	const char* dbUser, const char* dbPassword,
	FILE* fp_export) {
	string dir;

	string fullMask(PathAdd(path, mask) + "*.sql");
	jsEnumFiles ef(Utf8ToUtf16(fullMask).c_str());

	map<float, string> files;

	// get all the filename and put in a map so sort by
	// number instead of string (e.g. 100 is after 99 )
	while (ef.next()) {
		float ver = 0.0;
		string fname(Utf16ToUtf8(ef.currentFileName()));
		STLToLower(fname);
		const char* p = strstr(fname.c_str(), mask);
		if (p) {
			p += strlen(mask);
			ver = (float)atof(p);
			if (ver != 0.0) {
				if (dir.empty())
					dir = Utf16ToUtf8(ef.currentDirName());
				files[ver] = Utf16ToUtf8(ef.currentFileName());
			}
		}
	}

	LOG4CPLUS_INFO(logger, "Upgrading schema " << dbName << " from version " << dbVer << " in " << fullMask);
	if (!files.empty()) {
		FILE* f = 0;

		string masterFile = PathAdd(path, DB_MASTER_FILENAME);
		int upgradeCount = 0;
		if (fopen_s(&f, masterFile.c_str(), "w") == 0) {
			for (map<float, string>::iterator i = files.begin(); i != files.end(); i++) {
				if (i->first > dbVer) {
					LOG4CPLUS_INFO(logger, "Adding upgrade file to script: " << i->second);
					fprintf(f, "%s\n", i->second.c_str()); // for local update only run higher versions
					upgradeCount++;
				} else {
					LOG4CPLUS_INFO(logger, "Skipping upgrade file: " << i->second);
				}

				// write all to server's update script file since don't know its version
				if (fp_export)
					fprintf(fp_export, "\\. %s\n", i->second.c_str());
			}
			fclose(f);
		} else {
			return masterFile;
		}

		try {
			if (upgradeCount != 0 && installScriptsEx(dir.c_str(), DB_MASTER_FILENAME, dbName,
										 dbServer, dbPort,
										 dbUser, dbPassword,
										 statusWnd) != TRUE) {
				return masterFile;
			}
		} catch (MySqlSetupException* e) {
			::MessageBoxW(parent, Utf8ToUtf16(e->getMessage()).c_str(), Utf8ToUtf16(e->getTitle()).c_str(), MB_OK);
			delete e;
			return masterFile;
		}
	}

	return "";
}

/*!
 * \brief
 * callback for db upgrade.  This logs a message every time text changes to something
 * 
 */
LRESULT CALLBACK dbUpgradeWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp) {
	if (msg == WM_SETTEXT) {
		Logger* logger = (Logger*)GetWindowLongPtr(hWnd, GWL_USERDATA);
		if (lp && strlen((char*)lp) > 0)
			LOG4CPLUS_INFO((*logger), (char*)lp);
	}
	return DefWindowProc(hWnd, msg, wp, lp);
}

/*!
 * \brief
 * first step in update.  Sets up window, and calls runUpdateScripts for core and template files.
 * 
 * \param parent
 * parent window
 * 
 * \param gameDir
 * base folder for game
 * 
 * \param statusWnd
 * window for setting text on for progress msgs
 * 
 * \param dbName
 * db name
 * 
 * \param dbServer
 * db server name or ip
 * 
 * \param dbPort
 * db server's port
 * 
 * \param dbUser
 * user id for db
 * 
 * \param dbPassword
 * password for db
 * 
 * \param fp_export
 * file handle of server export/update file
 * 
 * \throws KEPException
 * if errors will throw
 * 
 */
static LogInstance("ClientEngine");
static void upgradeDb(HWND parent,
	const char* gameDir,
	const char* dbName,
	const char* dbServer, LONG dbPort,
	const char* dbUser, const char* dbPassword,
	FILE* fp_export) {
	KEPException* throwMe = 0;

	// create a window for getting status messages from db scripts.
	static const wchar_t* const DB_WINDOW_CLASS = L"KanevaDbExportWindowClass";
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));

	ATOM ant = 0;

	if (!GetClassInfo(GetModuleHandle(0), DB_WINDOW_CLASS, &wndclass)) {
		wndclass.lpfnWndProc = dbUpgradeWndProc;
		wndclass.lpszClassName = DB_WINDOW_CLASS;
		ant = RegisterClass(&wndclass);
		if (ant == 0)
			LogWarn("Failed to create window class: " << GetLastError());
	}

	HWND staticWnd = CreateWindowW(DB_WINDOW_CLASS, L"DbExportHiddenProgress",
		0, // style
		0, 0, 1, 1, // x, y, cx, cw
		parent, 0, GetModuleHandle(NULL), 0);
	if (staticWnd == NULL) {
		DWORD dwError = GetLastError();
		string msg;
		char s[1024];
		sprintf_s(s, _countof(s), "Can't create hidden window for db upgrade: %s", errorNumToString(dwError, msg));
		LogWarn(s);
	} else {
		SetWindowLongPtr(staticWnd, GWL_USERDATA, (LONG)&m_logger);
	}

	int dbVer = -1;
	try {
		dbVer = getDBVersionEx(
			dbName,
			dbServer, dbPort,
			dbUser, dbPassword,
			staticWnd);
	} catch (MySqlSetupException* e) {
		MessageBoxW(parent, Utf8ToUtf16(e->getMessage()).c_str(), Utf8ToUtf16(e->getTitle()).c_str(), MB_OK);
		delete e;
	}

	if (dbVer > FIRST_STAR_SCHEMA_VER) // only search for versions higher than first
	{
		// DRF - Added
		string pathSqlScripts = PathAdd(PathApp(), "..\\templates\\Shared\\SqlScripts");
		LogInfo("pathSqlScripts='" << pathSqlScripts << "'");

		// first do the "core" upgrades in the install folders template shared folder
		string errString = runUpdateScripts(m_logger, dbVer, pathSqlScripts.c_str(), CORE_MASK_STRING,
			parent, staticWnd,
			dbName, dbServer, dbPort,
			dbUser, dbPassword,
			fp_export);
		if (errString.empty()) {
			// repeat the process on the current game's SqlScript folder,
			// passing in a version of zero since we don't know how third-party
			// templates will upgrade
			errString = runUpdateScripts(m_logger, 0, PathAdd(gameDir, "SqlScripts").c_str(), USER_MASK_STRING,
				parent, staticWnd,
				dbName, dbServer, dbPort,
				dbUser, dbPassword,
				fp_export);
		}

		if (errString.empty()) {
#ifndef DECOUPLE_DBCONFIG
			// check to see if update got us
			dbVer = -1;
			try {
				dbVer = getDBVersionEx(
					dbName,
					dbServer, dbPort,
					dbUser, dbPassword,
					staticWnd);
			} catch (MySqlSetupException* e) {
				MessageBoxW(parent, Utf8ToUtf16(e->getMessage()).c_str(), Utf8ToUtf16(e->getTitle()).c_str(), MB_OK);
				delete e;
			}

			if (dbVer < MIN_DB_VERSION) {
				throwMe = new KEPException(loadStrPrintf(IDS_DB_VERSION, dbVer, MIN_DB_VERSION), E_INVALIDARG);
			}
#else
#endif
		} else {
			throwMe = new KEPException(loadStrPrintf(IDS_DB_UPDATE_FAILED, errString.c_str()), E_INVALIDARG);
		}

	} else {
		throwMe = new KEPException(loadStrPrintf(IDS_DB_VERSION_TOO_LOW, dbVer), E_INVALIDARG);
	}

	if (staticWnd)
		DestroyWindow(staticWnd);

	UnregisterClass(DB_WINDOW_CLASS, GetModuleHandle(0)); // issues w/ our dll getting unloaded and reusing window class, must unreg

	if (throwMe != 0)
		throw throwMe;
}

/*!
 * \brief
 * exports the object to the Write brief comment for ExportToDb here.
 * 
 * \param parent
 * parent window
 * 
 * \param gameDir
 * base folder for game
 * 
 * \param exportSqlPath
 * path for export script files for the server upgrade
 * 
 * \param engine
 * the getset object to export
 * 
 * \param checkForDbUpdate
 * if true will check for schema update files
 * 
 * \returns
 * true if succeeded
 * 
 */
extern "C" __declspec(dllexport) bool ExportToDb(HWND parent, const char* gameDir, const char* exportSqlPath,
	IGetSet* engine, bool checkForDbUpdate) {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	bool ret = false;

	FILE* fp_export = 0;

	// check on the database settings in the config file
	string dbServer, dbUser, dbPassword, dbName;
	int dbPort;

	if (doDialog(parent, gameDir, dbServer, dbUser, dbPassword, dbName, dbPort, onlyShowIfNeeded) != IDOK) {
		return ret;
	}

	try {
		unique_ptr<Connection> conn;
		do {
			//Reuse of an existing mysqlpp::Connection object will cause libmysql to crash at allocating shared memory object name if the server is 'localhost'.
			//Use a pointer to spawn a new instance every time we retry.
			unique_ptr<Connection> connTrial(new Connection(use_exceptions));

			string errMsg;
			try {
				connect(*connTrial, dbServer.c_str(), dbUser.c_str(), dbPassword.c_str(), dbName.c_str(), dbPort);
			} catch (const mysqlpp::Exception& er) {
				errMsg = er.what();
			}

			if (!*connTrial) {
				if (MessageBoxW(parent, Utf8ToUtf16(loadStrPrintf(IDS_EXPORT_NOCONNECT, connTrial->error())).c_str(), Utf8ToUtf16(loadStr(IDS_DBEXPORTER)).c_str(),
						MB_ICONEXCLAMATION | MB_YESNOCANCEL) == IDYES) {
					if (doDialog(parent, gameDir, dbServer, dbUser, dbPassword, dbName, dbPort, showDialogHadError) != IDOK) {
						return ret;
					}
				} else {
					return ret; // cancel it
				}
			} else {
				//Succeeded
				connTrial.swap(conn);
				break;
			}

		} while (true);

		jsVerifyReturn(conn != NULL, false);

		// exportPath may be empty for db export only
		if (strlen(exportSqlPath) > 0 && fopen_s(&fp_export, exportSqlPath, "a") != 0) {
			throw new KEPException(loadStrPrintf(IDS_EXPORT_SQL_FILE_ERROR, exportSqlPath));
		}

		if (checkForDbUpdate)
			upgradeDb(parent, gameDir, dbName.c_str(),
				dbServer.c_str(), dbPort,
				dbUser.c_str(), dbPassword.c_str(), fp_export);

		exportToDb(parent, engine, *conn, exportSqlPath, fp_export);

		ret = true;
	} catch (KEPException* e) {
		MessageBox(parent, Utf8ToUtf16(loadStrPrintf(IDS_EXPORT_EXCEPTION, e->m_msg.c_str())).c_str(), Utf8ToUtf16(loadStr(IDS_DBEXPORTER)).c_str(), MB_ICONEXCLAMATION);
		e->Delete();

		KEPConfig cfg;
		cfg.Load(PathAdd(gameDir, DB_CFG_NAME).c_str(), DB_CFG_ROOT_NODE);
		cfg.SetValue(DB_DONTSHOWAGAIN, 0);
	}

	if (fp_export) {
		fclose(fp_export);
	}

	if (ret == false) {
		_unlink(exportSqlPath);
	}

	return ret;
}
