#include "stdafx.h"

#include <mysql++/mysql++.h>
#include <mysql++/ssqls.h>

#include "resource.h"

#include <IGetSet.h>
#include <KEPConfig.h>
#include <KEPException.h>
#include <DBConfigValues.h>
#include <KEPConstants.h>

#include <event/glue/KEPEditViewIds.h>
#include <event/glue/GameIds.h>
#include <event/glue/SkillIds.h>
#include <event/glue/MovementIds.h>
#include <event/glue/TitleIds.h>
#include <event/glue/WorldAvailableIds.h>
#include <event/glue/StatIds.h>
#include <event/glue/StatIds.h>
#include <event/glue/ServerItemGenIds.h>
#include <event/glue/ItemIds.h>
#include <event/glue/ServerItemIds.h>
#include <event/glue/AICollectionQuestIds.h>
#include <event/glue/GlobalInventoryIds.h>
#include <event/glue/MultiplayerIds.h>
//#include <event/glue/WldIds.h>
#include <event/glue/ZoneObjectsIds.h>
#include <event/glue/InventoryIds.h>
#include <event/glue/ItemIds.h>
#include <event/glue/SpawnCfgIds.h>
#include <event/glue/CommerceIds.h>
#include <event/glue/StoreInventoryIds.h>
#include <event/glue/SkillActionIds.h>
#include <event/glue/LevelRangeIds.h>
#include <event/glue/LevelRewardIds.h>
#include <event/glue/StreamableDynamicIds.h>
#include <event/glue/StockAnimationGlidIds.h>
#include <event/glue/StockAnimationGlidsIds.h>
#include <log4cplus/logger.h>
using namespace log4cplus;
#include <DBStrings.h>

#define MIN_UCG_ITEM_ID 3000000
using namespace KEP;
using namespace mysqlpp;

static const char *exceptionTableName = "base"; // for exception msg

bool connect(Connection &con, const char *dbServer, const char *dbUser, const char *dbPassword,
	const char *dbName, ULONG dbPort) {
	con.set_option(new MultiStatementsOption(true)); // need this for calling SPs
	con.connect(dbName, dbServer, dbUser, dbPassword, dbPort);

	return con; // as bool
}

bool testConnect(const char *dbServer, const char *dbUser, const char *dbPassword,
	const char *dbName, ULONG dbPort, string &msg) {
	Connection con(use_exceptions);
	try {
		connect(con, dbServer, dbUser, dbPassword, dbName, dbPort);
	} catch (const mysqlpp::Exception &er) {
		msg = er.what();
	}

	return con; // as bool
}

inline void deleteAllFrom(HWND parent, Connection &con, const char *table, FILE *fp_export, const char *whereClause = 0) {
	bool ret = true;

	Query query = con.query();
	query << "delete from " << table;
	if (whereClause)
		query << " " << whereClause;

	string theQuery = query.str();
	theQuery += ";\n";
	if (fp_export) {
		size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
		if ((write_len != theQuery.length()) || (ferror(fp_export))) {
			ret = false;
			wstring s(L"Error writing sql export file.");
			MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
		}
	}
	query.execute();
}

// for the db
#define DB_TRUE "T"
#define DB_FALSE "F"

sql_create_10(skills,
	1, 10,
	int, skill_id,
	string, name,
	int, type,
	float, gain_increment,
	string, basic_ability_skill,
	int, basic_ability_function,
	float, skill_gains_when_failed,
	float, max_multiplier,
	string, use_internal_timer,
	string, filter_eval_level);

bool exportSkills(HWND parent, mysqlpp::Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "skills", fp_export);

	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_SKILLDATABASE);

	for (ULONG i = 0; i < count; i++) {
		IGetSet *skill = engine->GetObjectInArray(KEPEDITVIEWIDS_SKILLDATABASE, i);

		if (skill) {
			skills row(skill->GetINT(SKILLIDS_SKILLTYPE),
				skill->GetString(SKILLIDS_SKILLNAME).c_str(),
				skill->GetINT(SKILLIDS_SKILLTYPE),
				(float)skill->GetNumber(SKILLIDS_GAININCREMENT),
				skill->GetNumber(SKILLIDS_BASICABILITYFUNCTION) == 0 ? DB_FALSE : DB_TRUE,
				skill->GetINT(SKILLIDS_BASICABILITYFUNCTION),
				(float)skill->GetNumber(SKILLIDS_SKILLGAINSWHENFAILED),
				(float)skill->GetNumber(SKILLIDS_MAXMULTIPLIER),
				skill->GetNumber(SKILLIDS_USEINTERNALTIMER) == 0 ? DB_FALSE : DB_TRUE,
				skill->GetNumber(SKILLIDS_FILTEREVALLEVEL) == 0 ? DB_FALSE : DB_TRUE);
			Query query = con.query();

			query.insert(row);
			string theQuery = query.str();
			theQuery += ";\n";
			if (fp_export) {
				size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
				if ((write_len != theQuery.length()) || (ferror(fp_export))) {
					ret = false;
					wstring s(L"Error writing sql export file.");
					MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
				}
			}

			query.execute();
		}
	}

	return ret;
}

sql_create_9(skill_levels,
	1, 9,
	//	int, level_id, // autoinc, don't include
	int, skill_id,
	int, level_number,
	float, start_exp,
	float, end_exp,
	int, title_id,
	int, alt_title_id,
	string, gain_msg,
	string, alt_gain_msg,
	string, level_msg);

bool exportSkillLevels(HWND parent, mysqlpp::Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "skill_levels", fp_export);

	// number of skill objects within skillobjectlist in kepeditview getset object
	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_SKILLDATABASE);

	// for each skill object, insert each skill level object into the database
	for (ULONG i = 0; i < count; i++) {
		IGetSet *skill = engine->GetObjectInArray(KEPEDITVIEWIDS_SKILLDATABASE, i);
		if (skill) {
			// number of levels within the skill
			ULONG levelCount = skill->GetArrayCount(SKILLIDS_LEVELRANGEDB);

			for (ULONG j = 0; j < levelCount; j++) {
				IGetSet *skillLevel = skill->GetObjectInArray(SKILLIDS_LEVELRANGEDB, j);

				if (skillLevel) {
					skill_levels row(
						// skillLevel->GetINT( LEVELRANGEIDS_LEVELRANGEID ),
						skill->GetINT(SKILLIDS_SKILLTYPE),
						skillLevel->GetINT(LEVELRANGEIDS_LEVELNUMBER),
						(float)skillLevel->GetNumber(LEVELRANGEIDS_RANGESTART),
						(float)skillLevel->GetNumber(LEVELRANGEIDS_RANGEEND),
						skillLevel->GetINT(LEVELRANGEIDS_TITLEID),
						skillLevel->GetINT(LEVELRANGEIDS_ALTTITLEID),
						skillLevel->GetString(LEVELRANGEIDS_GAINMSG).c_str(),
						skillLevel->GetString(LEVELRANGEIDS_ALTGAINMSG).c_str(),
						skillLevel->GetString(LEVELRANGEIDS_LEVELMSG).c_str());
					Query query = con.query();

					query.insert(row);
					string theQuery = query.str();
					theQuery += ";\n";
					if (fp_export) {
						size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
						if ((write_len != theQuery.length()) || (ferror(fp_export))) {
							ret = false;
							wstring s(L"Error writing sql export file.");
							MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
						}
					}
					query.execute();
				}
			}
		}
	}

	return ret;
}

sql_create_10(level_rewards,
	1, 10,
	int, reward_id,
	int, skill_id,
	int, level_id,
	int, reward_glid,
	int, reward_qty,
	int, reward_point_type,
	int, reward_amt,
	int, alt_reward_glid,
	int, alt_reward_qty,
	int, alt_reward_amt);

bool exportLevelRewards(HWND parent, mysqlpp::Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "level_rewards", fp_export);

	// number of skill objects within skillobjectlist in kepeditview getset object
	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_SKILLDATABASE);

	// for each skill object, insert each skill level object into the database
	for (ULONG i = 0; i < count; i++) {
		IGetSet *skill = engine->GetObjectInArray(KEPEDITVIEWIDS_SKILLDATABASE, i);
		if (skill) {
			// number of levels within the skill
			ULONG levelCount = skill->GetArrayCount(SKILLIDS_LEVELRANGEDB);

			for (ULONG j = 0; j < levelCount; j++) {
				IGetSet *skillLevel = skill->GetObjectInArray(SKILLIDS_LEVELRANGEDB, j);

				if (skillLevel) {
					ULONG rewardCount = skillLevel->GetArrayCount(LEVELRANGEIDS_LEVELREWARDDB);

					for (ULONG k = 0; k < rewardCount; k++) {
						IGetSet *levelReward = skillLevel->GetObjectInArray(LEVELRANGEIDS_LEVELREWARDDB, k);

						if (levelReward) {
							level_rewards row(
								levelReward->GetINT(LEVELREWARDIDS_REWARDID),
								levelReward->GetINT(LEVELREWARDIDS_SKILLID),
								levelReward->GetINT(LEVELREWARDIDS_LEVELID),
								levelReward->GetINT(LEVELREWARDIDS_REWARDGLID),
								levelReward->GetINT(LEVELREWARDIDS_REWARDQTY),
								levelReward->GetINT(LEVELREWARDIDS_REWARDPOINTTYPE),
								levelReward->GetINT(LEVELREWARDIDS_REWARDAMOUNT),
								levelReward->GetINT(LEVELREWARDIDS_ALTREWARDGLID),
								levelReward->GetINT(LEVELREWARDIDS_ALTREWARDQTY),
								levelReward->GetINT(LEVELREWARDIDS_ALTREWARDAMOUNT));
							Query query = con.query();

							query.insert(row);
							string theQuery = query.str();
							theQuery += ";\n";
							if (fp_export) {
								size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
								if ((write_len != theQuery.length()) || (ferror(fp_export))) {
									ret = false;
									wstring s(L"Error writing sql export file.");
									MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
								}
							}
							query.execute();
						}
					}
				}
			}
		}
	}

	return ret;
}

sql_create_3(skill_actions,
	1, 3,
	int, action_id,
	int, skill_id,
	float, gain_amt);

bool exportSkillActions(HWND parent, mysqlpp::Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "skill_actions", fp_export);

	// number of skill objects within skillobjectlist in kepeditview getset object
	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_SKILLDATABASE);

	// for each skill object, insert each skill action object into the database
	for (ULONG i = 0; i < count; i++) {
		IGetSet *skill = engine->GetObjectInArray(KEPEDITVIEWIDS_SKILLDATABASE, i);
		if (skill) {
			// number of actions within the skill
			ULONG actionCount = skill->GetArrayCount(SKILLIDS_SKILLACTIONDB);

			for (ULONG j = 0; j < actionCount; j++) {
				IGetSet *skillAction = skill->GetObjectInArray(SKILLIDS_SKILLACTIONDB, j);

				if (skillAction) {
					skill_actions row(
						skillAction->GetINT(SKILLACTIONIDS_ACTIONID),
						skillAction->GetINT(SKILLACTIONIDS_SKILLID),
						(float)skillAction->GetNumber(SKILLACTIONIDS_GAINAMOUNT));
					Query query = con.query();

					query.insert(row);
					string theQuery = query.str();
					theQuery += ";\n";
					if (fp_export) {
						size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
						if ((write_len != theQuery.length()) || (ferror(fp_export))) {
							ret = false;
							wstring s(L"Error writing sql export file.");
							MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
						}
					}
					query.execute();
				}
			}
		}
	}
	return ret;
}

sql_create_2(titles,
	1, 2,
	int, title_id,
	string, name);

bool exportTitles(HWND parent, mysqlpp::Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "titles", fp_export);

	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_TITLE);

	for (ULONG i = 0; i < count; i++) {
		IGetSet *title = engine->GetObjectInArray(KEPEDITVIEWIDS_TITLE, i);

		if (title) {
			titles row(
				title->GetINT(TITLEIDS_TITLEID),
				title->GetString(TITLEIDS_TITLENAME).c_str());
			Query query = con.query();

			query.insert(row);
			string theQuery = query.str();
			theQuery += ";\n";
			if (fp_export) {
				size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
				if ((write_len != theQuery.length()) || (ferror(fp_export))) {
					ret = false;
					wstring s(L"Error writing sql export file.");
					MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
				}
			}
			query.execute();
		}
	}
	return ret;
}

sql_create_6(stats,
	1, 6, // explained in the user manual
	int, stat_id,
	string, name,
	float, cap_value,
	float, gain_basis,
	int, benefit_type,
	float, bonus_basis);

bool exportStats(HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "stats", fp_export);

	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_STATDATABASE);

	for (ULONG i = 0; i < count; i++) {
		IGetSet *stat = engine->GetObjectInArray(KEPEDITVIEWIDS_STATDATABASE, i);
		if (stat) {
			stats row(stat->GetINT(STATIDS_ID),
				stat->GetString(STATIDS_STATNAME).c_str(),
				(float)stat->GetNumber(STATIDS_CAPVALUE),
				(float)stat->GetNumber(STATIDS_GAINBASIS),
				stat->GetINT(STATIDS_BENEFITTYPE),
				(float)stat->GetNumber(STATIDS_BONUSBASIS));
			Query query = con.query();

			query.insert(row);
			string theQuery = query.str();
			theQuery += ";\n";
			if (fp_export) {
				size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
				if ((write_len != theQuery.length()) || (ferror(fp_export))) {
					ret = false;
					wstring s(L"Error writing sql export file.");
					MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
				}
			}
			query.execute();
		}
	}

	return ret;
}

sql_create_14(quests,
	1, 14,
	int, quest_id,
	string, title,
	string, description,
	string, completion_message,
	string, npc_name,
	int, bonus_skill_id,
	float, experience_bonus,
	float, max_exp_limit,
	string, is_autocomplete,
	string, optional_instruction,
	int, mob_id_to_hunt,
	int, drop_global_id,
	int, drop_percentage,
	string, is_undeletable);

sql_create_3(required_quest_items,
	2, 3,
	int, quest_id,
	int, global_id,
	int, quantity);

bool exportQuests(HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "`quests`", fp_export);
	deleteAllFrom(parent, con, "`required_quest_items`", fp_export);

	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_SERVERITEMGENERATOR);

	for (ULONG i = 0; i < count; i++) {
		IGetSet *pSIGO = engine->GetObjectInArray(KEPEDITVIEWIDS_SERVERITEMGENERATOR, i);
		if (pSIGO) {
			int questId = pSIGO->GetINT(SERVERITEMGENIDS_SPAWNGENID);

			IGetSet *pSIO = pSIGO->GetObjectPtr(SERVERITEMGENIDS_PSIO);
			if (pSIO) {
				string npcName = pSIO->GetString(SERVERITEMIDS_DISPLAYEDNAME);
				ULONG questCount = pSIO->GetArrayCount(SERVERITEMIDS_COLLECTIONQUESTDB);
				for (ULONG j = 0; j < questCount; j++) {
					IGetSet *quest = pSIO->GetObjectInArray(SERVERITEMIDS_COLLECTIONQUESTDB, j);
					if (quest) {
						quests row(questId,
							quest->GetString(AICOLLECTIONQUESTIDS_TITLE).c_str(),
							quest->GetString(AICOLLECTIONQUESTIDS_QUESTDESRIPTION).c_str(),
							quest->GetString(AICOLLECTIONQUESTIDS_QUESTCOMPLETIONMESSAGE).c_str(),
							npcName.c_str(),
							quest->GetINT(AICOLLECTIONQUESTIDS_SKILLTYPEBONUS),
							(float)quest->GetNumber(AICOLLECTIONQUESTIDS_EXPBONUS),
							(float)quest->GetNumber(AICOLLECTIONQUESTIDS_MAXEXPLIMIT),
							quest->GetNumber(AICOLLECTIONQUESTIDS_AUTOCOMPLETE) != 0 ? DB_TRUE : DB_FALSE,
							quest->GetString(AICOLLECTIONQUESTIDS_OPTIONALINTRUCTION),
							quest->GetINT(AICOLLECTIONQUESTIDS_MOBTOHUNTID),
							quest->GetINT(AICOLLECTIONQUESTIDS_DROPGLID),
							quest->GetINT(AICOLLECTIONQUESTIDS_DROPPERCENTAGE),
							quest->GetNumber(AICOLLECTIONQUESTIDS_UNDELETABLE) != 0 ? DB_TRUE : DB_FALSE);

						Query query = con.query();

						query.insert(row);
						string theQuery = query.str();
						theQuery += ";\n";
						if (fp_export) {
							size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
							if ((write_len != theQuery.length()) || (ferror(fp_export))) {
								ret = false;
								wstring s(L"Error writing sql export file.");
								MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
							}
						}
						query.execute();

						for (size_t k = 0; k < quest->GetArrayCount(AICOLLECTIONQUESTIDS_REQUIREDITEMS); k++) {
							// CInventoryItem
							IGetSet *requiredItem = quest->GetObjectInArray(AICOLLECTIONQUESTIDS_REQUIREDITEMS, k);
							if (requiredItem) {
								int quantity = requiredItem->GetINT(INVENTORYIDS_QUANTITY);
								IGetSet *item = requiredItem->GetObjectPtr(INVENTORYIDS_ITEMDATA);
								if (item) {
									query.reset();
									required_quest_items requiredQuestItemsRow(questId,
										item->GetINT(ITEMIDS_GLOBALID),
										quantity);
									query.insert(requiredQuestItemsRow);
									string requiredQuestItemsQuery = query.str();
									requiredQuestItemsQuery += ";\n";
									if (fp_export) {
										size_t write_len = fwrite(requiredQuestItemsQuery.c_str(), 1, requiredQuestItemsQuery.length(), fp_export);
										if ((write_len != requiredQuestItemsQuery.length()) || (ferror(fp_export))) {
											ret = false;
											wstring s(L"Error writing sql export file.");
											MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
										}
									}
									query.execute();
								}
							}
						}
					}
				}
			}
		}
	}

	return ret;
}

sql_create_5(supported_worlds,
	1, 5,
	int, zone_index,
	string, name,
	int, channel_id,
	string, display_name,
	int, default_max_occupancy);

bool exportWorlds(HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	deleteAllFrom(parent, con, "supported_worlds", fp_export, (string("where zone_index <= ") + numToString(MAX_KANEVA_SUPPORTED_WORLD, "%d")).c_str());

	IGetSet *cmp = engine->GetObjectPtr(KEPEDITVIEWIDS_MULTIPLAYEROBJ);

	if (cmp) {
		ULONG count = cmp->GetArrayCount(MULTIPLAYERIDS_CURRENTWORLDSUSED);

		std::string defaultZone = cmp->GetString(MULTIPLAYERIDS_DEFAULTPLACEURL);
		size_t pos = defaultZone.find(".");
		if (pos != 0)
			defaultZone = defaultZone.substr(0, pos);
		else
			defaultZone.clear();

		for (ULONG i = 0; i < count; i++) {
			IGetSet *world = cmp->GetObjectInArray(MULTIPLAYERIDS_CURRENTWORLDSUSED, i);
			if (world) {
				Query query = con.query();

				std::string displayName = world->GetString(WORLDAVAILABLEIDS_DISPLAYNAME);
				if (!defaultZone.empty() && (strcmp(defaultZone.c_str(), displayName.c_str()) == 0)) {
					int zoneIndexPlain = world->GetINT(WORLDAVAILABLEIDS_INDEX);
					int visibility = (int)world->GetULONG(WORLDAVAILABLEIDS_VISIBILITY);
					int zi = (4 << 28) | zoneIndexPlain; //So we dont have to pull in a bunch of other code.

					query << "SELECT name FROM channel_zones WHERE zone_index = %0 AND zone_index_plain = %1 AND name = %2q";
					query.parse();
					StoreQueryResult res = query.store(zi, zoneIndexPlain, displayName);

					if (res.size() == 0) {
						query.reset();
						query << "INSERT INTO channel_zones(kaneva_user_id, zone_index, zone_index_plain, zone_type, zone_instance_id, name, tied_to_server, created_date, raves, access_rights, access_friend_group_id, server_id, cover_charge, visibility, last_update, number_unique_visits, script_server_id) VALUES (0,%0,%1,4,1,%2q,'NOT_TIED',now(),0,0,0,0,0,%3,now(),0,0)";
						query.parse();

						if (fp_export) {
							string theQuery = query.str(zi, zoneIndexPlain, displayName, visibility) + ";\n";

							size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
							if ((write_len != theQuery.length()) || (ferror(fp_export))) {
								ret = false;
								wstring s(L"Error writing sql export file.");
								MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
							}
						}

						query.execute(zi, zoneIndexPlain, displayName, visibility);
					}
				}

				if (world->GetINT(WORLDAVAILABLEIDS_INDEX) > MAX_KANEVA_SUPPORTED_WORLD) {
					string s("Zone has index too high for exporting, skipping ");
					s += displayName;
					MessageBoxW(parent, Utf8ToUtf16(s).c_str(), L"KEPEdit", MB_ICONWARNING);
				} else {
					// zone index is order in this list!
					supported_worlds row(world->GetINT(WORLDAVAILABLEIDS_INDEX),
						world->GetString(WORLDAVAILABLEIDS_WORLDNAME).c_str(),
						world->GetINT(WORLDAVAILABLEIDS_CHANNEL),
						displayName.c_str(),
						world->GetULONG(WORLDAVAILABLEIDS_MAXOCCUPANCY));

					query.reset();

					query.insert(row);
					string theQuery = query.str();
					theQuery += ";\n";
					if (fp_export) {
						size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
						if ((write_len != theQuery.length()) || (ferror(fp_export))) {
							ret = false;
							wstring s(L"Error writing sql export file.");
							MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
						}
					}
					query.execute();
				}
			}
		}
	}

	return ret;
}

sql_create_7(spawn_points,
	1, 7,
	int, spawn_point_id,
	int, zone_index,
	float, position_x,
	float, position_y,
	float, position_z,
	int, rotation,
	string, name);

// to keep avoiding sucking in main code add this
inline int zoneIndex(int i) {
	return i & 0xfff;
}

bool exportSpawnPoints(HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	IGetSet *cmp = engine->GetObjectPtr(KEPEDITVIEWIDS_MULTIPLAYEROBJ);

	if (cmp) {
		ULONG count = cmp->GetArrayCount(MULTIPLAYERIDS_SPAWNPOINTCFGS);

		for (ULONG i = 0; i < count; i++) {
			IGetSet *spoint = cmp->GetObjectInArray(MULTIPLAYERIDS_SPAWNPOINTCFGS, i);
			if (spoint) {
				// check range, export of supported worlds will warn user
				if (zoneIndex(spoint->GetINT(SPAWNCFGIDS_ZONEINDEX)) <= MAX_KANEVA_SUPPORTED_WORLD) {
					Vector3f pt = spoint->GetVector3f(SPAWNCFGIDS_POSITION);

					Query query = con.query();
					query << "INSERT IGNORE INTO spawn_points (spawn_point_id, zone_index, position_x, position_y, position_z, rotation, name) "
						  << "VALUES (%0, %1, %2, %3, %4, %5, %6q)";

					query.parse();
					string theQuery = query.str((int)i, spoint->GetINT(SPAWNCFGIDS_ZONEINDEX),
						pt.x,
						pt.y,
						pt.z,
						spoint->GetINT(SPAWNCFGIDS_ROTATION),
						spoint->GetString(SPAWNCFGIDS_SPAWNPOINTNAME).c_str());

					theQuery += ";\n";
					if (fp_export) {
						size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
						if ((write_len != theQuery.length()) || (ferror(fp_export))) {
							ret = false;
							wstring s(L"Error writing sql export file.");
							MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
						}
					}

					query.execute((int)i, spoint->GetINT(SPAWNCFGIDS_ZONEINDEX),
						pt.x,
						pt.y,
						pt.z,
						spoint->GetINT(SPAWNCFGIDS_ROTATION),
						spoint->GetString(SPAWNCFGIDS_SPAWNPOINTNAME).c_str());
				}
			}
		}
	}

	return ret;
}

sql_create_7(items,
	1, 7,
	int, global_id,
	string, name,
	int, market_cost,
	int, selling_price,
	int, required_skill,
	int, required_skill_level,
	int, use_type);

/**
* Export the global inventory items to the database. Optionally,
* capture the sql statements to a file for later use by, for
* example, the server install program.
*/
bool exportGlobalInventory(Logger &logger, HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	// 10/07 changes to *not* wipe the items but use INSERT INTO...ON DUPLICATE KEY
	// since new columns have been added to the table not populated by this fn

	// deleteAllFrom( parent, con, "items" );

	bool ret = true;

	// 3000000 is never used anywhere in
	char whereClause[256];

	// Implementation note:
	// 		The following sql statements are captured in a file for input to the setup of
	// 		another server. Capturing of these sql statements have a few ramifications. Business logic
	// 		can not be used around these statements. For example, we can't -
	// 			get a value for a glid
	// 			perform a conditional
	// 			execute some sql.
	// 		The value for the glid would be obtained from <this> db and not the target db of the server setup.
	// 		The two dbs can have different values. Thus, statements must execute the same on both dbs.

	// only delete item passes that haven't been edited by the web
	sprintf_s(whereClause, _countof(whereClause), "where EXISTS ( select global_id from items i WHERE i.global_id < %d AND i.global_id = pass_group_items.global_id AND i.web_controlled_edit = 0)", MIN_UCG_ITEM_ID);

	deleteAllFrom(parent, con, "pass_group_items", fp_export, whereClause);

	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_GLOBALINVENTORYDB);

	LOG4CPLUS_INFO(logger, "   Exporting " << count << " items");
	for (ULONG i = 0; i < count; i++) {
		IGetSet *invItem = engine->GetObjectInArray(KEPEDITVIEWIDS_GLOBALINVENTORYDB, i);
		if (invItem) {
			IGetSet *item = invItem->GetObjectPtr(GLOBALINVENTORYIDS_ITEMDATA);
			if (item) {
				Query query = con.query();

				// only insert items that haven't been edited by the web
				query << "INSERT INTO items "
						 "  (global_id,name,market_cost,selling_price,required_skill,required_skill_level,use_type,description,inventory_type,permanent,is_default,visual_ref,actor_group) "
						 " VALUES (  %0, %1q, %2, %3, %4, %5, %6, %7q, %8, %9, %10, %11, %12) "
						 " ON DUPLICATE KEY UPDATE "
						 " global_id = IF(web_controlled_edit,global_id,%0), name = IF(web_controlled_edit,name,%1q), "
						 " market_cost = IF(web_controlled_edit,market_cost,%2), selling_price = IF(web_controlled_edit, selling_price,%3), "
						 " required_skill = IF(web_controlled_edit,required_skill,%4), "
						 " required_skill_level = IF(web_controlled_edit,required_skill_level,%5), "
						 " use_type = IF(web_controlled_edit,use_type,%6), "
						 " description = IF(web_controlled_edit,description,%7q), inventory_type = IF(web_controlled_edit,inventory_type,%8), "
						 " permanent = IF(web_controlled_edit,permanent,%9), is_default = IF(web_controlled_edit,is_default,%10), "
						 " visual_ref = IF(web_controlled_edit,visual_ref,%11), "
						 " actor_group = IF(web_controlled_edit,actor_group,%12)";

				query.parse();
				const ULONG DESC_LEN = 200; // len of column
				string desc = item->GetString(ITEMIDS_ITEMDESCRIPTION);
				if (desc.length() > DESC_LEN) {
					string msg = "Truncating CItemObj descrption that is too long:\r\n";
					msg += desc;
					MessageBoxW(parent, Utf8ToUtf16(msg).c_str(), L"KEPEdit", MB_ICONWARNING);
					desc = desc.substr(0, DESC_LEN);
				}

				{
					string s = query.str(item->GetINT(ITEMIDS_GLOBALID),
						item->GetString(ITEMIDS_ITEMNAME).c_str(),
						item->GetINT(ITEMIDS_MARKETCOST),
						item->GetINT(ITEMIDS_SELLINGPRICE),
						item->GetINT(ITEMIDS_REQUIREDSKILLTYPE),
						item->GetINT(ITEMIDS_REQUIREDSKILLLEVEL),
						item->GetINT(ITEMIDS_USETYPE),
						desc.c_str(),
						(int)item->GetNumber(ITEMIDS_ITEMTYPE),
						item->GetBOOL(ITEMIDS_PERMANENT),
						item->Getbool(ITEMIDS_DEFAULTARMEDITEM),
						item->GetINT(ITEMIDS_VISUALREF),
						item->GetINT(ITEMIDS_ACTORGROUP));

					string theQuery = s;
					theQuery += ";\n";
					if (fp_export) {
						size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
						if ((write_len != theQuery.length()) || (ferror(fp_export))) {
							ret = false;
							MessageBoxW(parent, L"Error writing sql export file.", L"KEPEdit", MB_ICONWARNING);
						}
					}

					query.execute(item->GetINT(ITEMIDS_GLOBALID),
						item->GetString(ITEMIDS_ITEMNAME).c_str(),
						item->GetINT(ITEMIDS_MARKETCOST),
						item->GetINT(ITEMIDS_SELLINGPRICE),
						item->GetINT(ITEMIDS_REQUIREDSKILLTYPE),
						item->GetINT(ITEMIDS_REQUIREDSKILLLEVEL),
						item->GetINT(ITEMIDS_USETYPE),
						desc.c_str(),
						(int)item->GetNumber(ITEMIDS_ITEMTYPE),
						item->GetBOOL(ITEMIDS_PERMANENT),
						item->Getbool(ITEMIDS_DEFAULTARMEDITEM),
						item->GetINT(ITEMIDS_VISUALREF),
						item->GetINT(ITEMIDS_ACTORGROUP));
				}

				query.reset();
				query << "INSERT INTO item_parameters ( global_id, param_type_id, value )"
						 " VALUES (%0,%1,%2q) ON DUPLICATE KEY UPDATE value = %2q";
				query.parse();

				if (fp_export) {
					std::string theQuery = query.str(item->GetINT(ITEMIDS_GLOBALID), item->GetINT(ITEMIDS_USETYPE), item->GetINT(ITEMIDS_MISCUSEVALUE)) + ";\n";

					size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
					if ((write_len != theQuery.length()) || (ferror(fp_export))) {
						ret = false;
						MessageBoxW(parent, L"Error writing sql export file.", L"KEPEdit", MB_ICONWARNING);
					}
				}

				query.execute(item->GetINT(ITEMIDS_GLOBALID), item->GetINT(ITEMIDS_USETYPE), item->GetINT(ITEMIDS_MISCUSEVALUE));

				// 8/07 add PassList to export
				query.reset();
				IGetSet *passList = item->GetObjectPtr(ITEMIDS_PASSLIST);
				if (passList) {
					// tricky, tricky, passList id 0 = count, then indexes are 1-based
					for (ULONG j = 0; j < passList->GetULONG(0); j++) {
						// only insert pass group items that haven't been edited by the web
						query << "INSERT INTO pass_group_items(global_id, pass_group_id) ( select global_id, %0 FROM items WHERE global_id = %1 AND web_controlled_edit = 0)";

						query.parse();

						string theQuery = query.str((int)passList->GetULONG(j + 1),
							item->GetINT(ITEMIDS_GLOBALID));

						theQuery += ";\n";
						if (fp_export) {
							size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
							if ((write_len != theQuery.length()) || (ferror(fp_export))) {
								ret = false;
								MessageBoxW(parent, L"Error writing sql export file.", L"KEPEdit", MB_ICONWARNING);
							}
						}
						query.execute((int)passList->GetULONG(j + 1),
							item->GetINT(ITEMIDS_GLOBALID));
					}
				}
				query.reset();

				IGetSet *dynObject = engine->GetObjectInArray(KEPEDITVIEWIDS_DYNAMICOBJECTS, item->GetINT(ITEMIDS_GLOBALID));
				if (dynObject) {
					query << "INSERT INTO item_dynamic_objects "
						  << "( select global_id, %0, %1, %2, %3, %4, %5, %6, %7, %8, %9 FROM items WHERE global_id = %10 AND web_controlled_edit = 0 )"
						  << " ON DUPLICATE KEY UPDATE "
						  << " attachable = %0, interactive = %1, playFlash = %2, collision = %3, min_x = %4, min_y = %5, min_z = %6, "
						  << " max_x = %7, max_y = %8, max_z = %9 ";
					query.parse();

					Vector3f boundBoxMin = dynObject->GetVector3f(STREAMABLEDYNAMICIDS_BOUNDBOX_MIN);
					Vector3f boundBoxMax = dynObject->GetVector3f(STREAMABLEDYNAMICIDS_BOUNDBOX_MAX);

					string theQuery = query.str(dynObject->Getbool(STREAMABLEDYNAMICIDS_ATTACHABLE),
						dynObject->Getbool(STREAMABLEDYNAMICIDS_INTERACTIVE),
						dynObject->Getbool(STREAMABLEDYNAMICIDS_CANOBJECTPLAYFLASH),
						dynObject->Getbool(STREAMABLEDYNAMICIDS_OBJECTHASCOLLISION),
						boundBoxMin.x, boundBoxMin.y, boundBoxMin.z,
						boundBoxMax.x, boundBoxMax.y, boundBoxMax.z,
						item->GetINT(ITEMIDS_GLOBALID));
					theQuery += ";\n";
					if (fp_export) {
						size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
						if ((write_len != theQuery.length()) || (ferror(fp_export))) {
							ret = false;
							wstring s(L"Error writing sql export file.");
							MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
						}
					}
					query.execute(dynObject->Getbool(STREAMABLEDYNAMICIDS_ATTACHABLE),
						dynObject->Getbool(STREAMABLEDYNAMICIDS_INTERACTIVE),
						dynObject->Getbool(STREAMABLEDYNAMICIDS_CANOBJECTPLAYFLASH),
						dynObject->Getbool(STREAMABLEDYNAMICIDS_OBJECTHASCOLLISION),
						boundBoxMin.x, boundBoxMin.y, boundBoxMin.z,
						boundBoxMax.x, boundBoxMax.y, boundBoxMax.z,
						item->GetINT(ITEMIDS_GLOBALID));
				}
			}
		}
	}

	return ret;
}

sql_create_5(world_objects,
	1, 5,
	string, world_object_id,
	int, zone_index,
	int, trace_id,
	string, name,
	string, description);

static bool exportZoneObjects(HWND parent, IGetSet *zoneObjects,
	Connection &con, FILE *fp_export) {
	bool ret = true;

	jsVerifyReturn(con, false);

	Logger logger(Logger::getInstance(L"ClientEngine"));
	exceptionTableName = "world_objects";
	LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);

	// get the zoneIndex
	INT zoneIndex = zoneObjects->GetINT(ZONEOBJECTSIDS_ZONEINDEX);

	{
		Query query = con.query();
		query << "delete from world_objects where zone_index = " << zoneIndex;
		string theQuery = query.str();
		theQuery += ";\n";
		if (fp_export) {
			size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
			if ((write_len != theQuery.length()) || (ferror(fp_export))) {
				ret = false;
				MessageBoxW(parent, L"Error writing sql export file.", L"KEPEdit", MB_ICONWARNING);
			}
		}
		query.execute();
	}

#if 0
	ULONG count = zoneObjects->GetArrayCount( ZONEOBJECTSIDS_WLDOBJLIST);

	for ( ULONG i = 0; i < count; i++ )
	{
		IGetSet *worldItem = zoneObjects->GetObjectInArray( ZONEOBJECTSIDS_WLDOBJLIST, i );
		try
		{

			if ( worldItem && worldItem->GetNumber(WLDIDS_CUSTOMIZABLETEXTURE) != 0 )
			{
				world_objects row(  worldItem->GetString(WLDIDS_UNIQUEID),
									zoneIndex,
									worldItem->GetINT(WLDIDS_IDENTITY),
									worldItem->GetString(WLDIDS_NAME).c_str(),
									worldItem->GetString(WLDIDS_DESCRIPTION).c_str()
									);

				Query query = con.query();
				
				query.insert(row);
				string theQuery = query.str();
				theQuery += ";\n";
				if ( fp_export )
				{
					size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
					if ( (write_len != theQuery.length()) || (ferror(fp_export)) )
					{
						ret = false;
						wstring s(L"Error writing sql export file.");
						MessageBoxW( parent, s.c_str(), L"KEPEdit", MB_ICONWARNING );
					}
				}
				query.execute();
			}
		}
		catch (const mysqlpp::BadQuery& er)
		{
			//1062 is the duplicate entry (for key) error in MySQL++
			if (er.errnum() == 1062) 
			{

				char error_msg[512];

				string reason = loadStrPrintf( IDS_EXPORT_QUERY_ERROR, exceptionTableName, er.what() ).c_str();

				_snprintf_s( error_msg, _countof(error_msg), _TRUNCATE, "%s\n\nUnique ID: %s\nZone Index: %d\nIdentity (TRC): %d\nName: %s\nDescription: %s\nFile Ref: %s", 
							reason.c_str(),
							worldItem->GetString(WLDIDS_UNIQUEID).c_str(),
							zoneIndex,
							worldItem->GetINT(WLDIDS_IDENTITY),
							worldItem->GetString(WLDIDS_NAME).c_str(),
							worldItem->GetString(WLDIDS_DESCRIPTION).c_str(),
							worldItem->GetString(WLDIDS_FILENAMEREF).c_str());

				MessageBoxW( parent, Utf8ToUtf16(error_msg).c_str(), L"KEPEdit", MB_ICONWARNING );

			}
			else
			{
				throw er;
			}
		}
	}
#endif

	// now cleanup any customized world objects that no longer exists
	Query query = con.query();
	query << "select count(*) from world_object_player_settings where zone_index = " << zoneIndex << " and world_object_id not in (select world_object_id from world_objects)";
	StoreQueryResult res = query.store();
	if (res.size() > 0 && (int)(res.at(0).at(0)) > 0) {
		string s("Orphaned world objects detected in database for zone ");
		s += numToString(zoneIndex);
		s += ".\r\nBad artist, bad!";
		MessageBoxW(parent, Utf8ToUtf16(s).c_str(), L"KEPEdit", MB_ICONWARNING);
	}

	LOG4CPLUS_INFO(logger, "Exporting of zone objects complete");

	return ret;
}

sql_create_4(stores,
	1, 4,
	int, store_id,
	string, name,
	int, channel_id,
	int, web_controlled_edit);

sql_create_2(store_inventories,
	2, 0,
	int, store_id,
	int, global_id);

static bool exportCommerceDb(HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	jsVerifyReturn(con, false);

	// only delete item passes that haven't been edited by the web
	deleteAllFrom(parent, con, "store_inventories", fp_export, "WHERE store_id in (SELECT store_id from stores WHERE web_controlled_edit = 0)");
	deleteAllFrom(parent, con, "stores", fp_export, "WHERE web_controlled_edit = 0");

	ULONG count = engine->GetArrayCount(KEPEDITVIEWIDS_COMMERCEDB);

	Query query = con.query();
	for (ULONG i = 0; i < count; i++) {
		// only export remaining ones
		IGetSet *cdb = engine->GetObjectInArray(KEPEDITVIEWIDS_COMMERCEDB, i);
		if (cdb) {
			exceptionTableName = "stores";
			int dbId = cdb->GetINT(COMMERCEIDS_UNIQUEID);
			stores row(dbId,
				cdb->GetString(COMMERCEIDS_ESTABLISHMENTNAME).c_str(),
				cdb->GetINT(COMMERCEIDS_CHANNEL),
				FALSE);

			query.reset();
			try {
				query.insert(row);
			} catch (const mysqlpp::BadQuery &er) {
				er.what();
				// if dupe key ignore
				continue;
			}

			{
				string theQuery = query.str();
				theQuery += ";\n";
				if (fp_export) {
					size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
					if ((write_len != theQuery.length()) || (ferror(fp_export))) {
						ret = false;
						wstring s(L"Error writing sql export file.");
						MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
					}
				}

				query.execute();
			}

			exceptionTableName = "store_inventories";

			// now do the inventories
			ULONG invCount = cdb->GetArrayCount(COMMERCEIDS_STOREINVENTORY);
			for (ULONG j = 0; j < invCount; j++) {
				IGetSet *storeItem = cdb->GetObjectInArray(COMMERCEIDS_STOREINVENTORY, j);
				if (storeItem) {
					store_inventories irow(dbId, storeItem->GetINT(STOREINVENTORYIDS_GLOBALINVENTORYID));
					query.reset();

					query.replace(irow); // got dupes?
					string theQuery = query.str();
					theQuery += ";\n";
					if (fp_export) {
						size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
						if ((write_len != theQuery.length()) || (ferror(fp_export))) {
							ret = false;
							wstring s(L"Error writing sql export file.");
							MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
						}
					}
					query.execute();
				}
			}
		}
	}
	return ret;
}

sql_create_7(stock_animation_glids,
	1, 7,
	string, owner_type,
	string, owner_name,
	int, animation_index,
	int, animation_type,
	int, animation_version,
	int, animation_glid,
	string, animation_name);

static bool exportStockAnimationGlids(HWND parent, IGetSet *animGlids, Connection &con, FILE *fp_export) {
	bool ret = true;

	jsVerifyReturn(con, false);
	Logger logger(Logger::getInstance(L"ClientEngine"));
	LOG4CPLUS_INFO(logger, "Exporting stock_animation_glids");

	deleteAllFrom(parent, con, "stock_animation_glids", fp_export);

	ULONG count = animGlids->GetArrayCount(STOCKANIMATIONGLIDSIDS_DATA);

	Query query = con.query();
	for (ULONG i = 0; i < count; i++) {
		IGetSet *glid = animGlids->GetObjectInArray(STOCKANIMATIONGLIDSIDS_DATA, i);
		if (glid) {
			exceptionTableName = "stock_animation_glids";

			stock_animation_glids row(
				glid->Getstring(STOCKANIMATIONGLIDIDS_OWNERTYPE),
				glid->Getstring(STOCKANIMATIONGLIDIDS_OWNERNAME),
				glid->GetINT(STOCKANIMATIONGLIDIDS_ANIMATIONINDEX),
				glid->GetINT(STOCKANIMATIONGLIDIDS_ANIMATIONTYPE),
				glid->GetINT(STOCKANIMATIONGLIDIDS_ANIMATIONVERSION),
				glid->GetINT(STOCKANIMATIONGLIDIDS_ANIMATIONGLID),
				glid->Getstring(STOCKANIMATIONGLIDIDS_ANIMATIONNAME));

			query.reset();
			query.insert(row);
			string theQuery = query.str();
			theQuery += ";\n";
			if (fp_export) {
				size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
				if ((write_len != theQuery.length()) || (ferror(fp_export))) {
					ret = false;
					wstring s(L"Error writing sql export file.");
					MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
				}
			}

			query.execute();
		}
	}

	LOG4CPLUS_INFO(logger, "Export of stock_animation_glids complete");
	return ret;
}

/*
 *Function Name: exportDynamicObjects
 *
 *Description: Collects data from the dynamic_objects and dynamic_object_parameters tables and 
 * 			   constructs sql statements from this data for later use by an install program.
 * 
 * 			   Only applicable for a child game, i.e. game stars.
 *
 */
bool exportDynamicObjects(HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	jsVerifyReturn(con, false);

	IGetSet *game = engine->GetObjectPtr(KEPEDITVIEWIDS_GAME);
	jsVerifyReturn(game, false);

	string parentGameIdStr = game->GetString(GAMEIDS_PARENTID);
	int parentGameId = atoi(parentGameIdStr.c_str());

	if (parentGameId != 0 && fp_export) {
		// No merge to target db, clean wipe.
		string theQuery = "delete from dynamic_object_parameters";
		theQuery += ";\n";
		theQuery += "delete from dynamic_objects";
		theQuery += ";\n";

		size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
		if ((write_len != theQuery.length()) || (ferror(fp_export))) {
			ret = false;
			wstring s(L"Error writing sql export file.");
			MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
		}

		Query query = con.query();

		query << "SELECT created_date, expired_date, global_id, inventory_sub_type, "
			  << "obj_placement_id, object_id, player_id, position_x, position_y, position_z, "
			  << "rotation_x, rotation_y, rotation_z, slide_x, slide_y, slide_z, "
			  << "zone_index, zone_instance_id FROM dynamic_objects";

		query.parse();

		StoreQueryResult resDynObj = query.store();

		for (ULONG ii = 0; ii < resDynObj.num_rows(); ii++) {
			query.reset();

			query << "INSERT INTO dynamic_objects ( created_date, expired_date, global_id, inventory_sub_type, "
				  << "obj_placement_id, object_id, player_id, position_x, position_y, position_z, "
				  << "rotation_x, rotation_y, rotation_z, slide_x, slide_y, slide_z, "
				  << "zone_index, zone_instance_id ) "
				  << "VALUES ( "
				  << (resDynObj.at(ii).at(0).is_null() ? "%0" : "%0Q") << ", " // Prevent quoting NULL values as "NULL"
				  << (resDynObj.at(ii).at(1).is_null() ? "%1" : "%1Q") << ", " // Prevent quoting NULL values as "NULL"
				  << "%2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14, %15, %16, %17 )";

			query.parse();

			theQuery = query.str(resDynObj.at(ii).at(0),
				resDynObj.at(ii).at(1),
				resDynObj.at(ii).at(2),
				resDynObj.at(ii).at(3),
				resDynObj.at(ii).at(4),
				resDynObj.at(ii).at(5),
				resDynObj.at(ii).at(6),
				resDynObj.at(ii).at(7),
				resDynObj.at(ii).at(8),
				resDynObj.at(ii).at(9),
				resDynObj.at(ii).at(10),
				resDynObj.at(ii).at(11),
				resDynObj.at(ii).at(12),
				resDynObj.at(ii).at(13),
				resDynObj.at(ii).at(14),
				resDynObj.at(ii).at(15),
				resDynObj.at(ii).at(16),
				resDynObj.at(ii).at(17));
			theQuery += ";\n";

			write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
			if ((write_len != theQuery.length()) || (ferror(fp_export))) {
				ret = false;
				wstring s(L"Error writing sql export file.");
				MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
				break;
			}

			// Target database is not current db. Sql statements captured for server install program.
			// Don't execute the sql statement.
		}

		if (ret) {
			exceptionTableName = "dynamic_object_parameters";

			query.reset();

			query << "SELECT obj_placement_id, param_type_id, value	FROM dynamic_object_parameters";

			query.parse();

			StoreQueryResult resDynObjParams = query.store();

			for (ULONG ii = 0; ii < resDynObjParams.num_rows(); ii++) {
				query.reset();

				query << "INSERT INTO dynamic_object_parameters ( obj_placement_id, param_type_id, value ) "
					  << "VALUES ( %0, %1, %2q )";

				query.parse();

				theQuery = query.str(resDynObjParams.at(ii).at(0),
					resDynObjParams.at(ii).at(1),
					resDynObjParams.at(ii).at(2));
				theQuery += ";\n";

				write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
				if ((write_len != theQuery.length()) || (ferror(fp_export))) {
					ret = false;
					wstring s(L"Error writing sql export file.");
					MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
					break;
				}

				// Target database is not current db. Sql statements captured for server install program.
				// Don't execute the sql statement.
			}
		}
	}

	return ret;
}

/*
 *Function Name: exportWorldObjectPlayerSettings
 *
 *Description: Collects data from the world_object_player_settings and 
 * 			   constructs sql statements from this data for later use by an install program.
 * 
 * 			   Only applicable for a child game, i.e. game stars.
 *
 */
bool exportWorldObjectPlayerSettings(HWND parent, Connection &con, IGetSet *engine, FILE *fp_export) {
	bool ret = true;

	jsVerifyReturn(con, false);

	IGetSet *game = engine->GetObjectPtr(KEPEDITVIEWIDS_GAME);
	jsVerifyReturn(game, false);

	string parentGameIdStr = game->GetString(GAMEIDS_PARENTID);
	int parentGameId = atoi(parentGameIdStr.c_str());

	if (parentGameId != 0 && fp_export) {
		// No merge to target db, clean wipe.
		string theQuery = "delete from world_object_player_settings";
		theQuery += ";\n";

		size_t write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
		if ((write_len != theQuery.length()) || (ferror(fp_export))) {
			ret = false;
			wstring s(L"Error writing sql export file.");
			MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
		}

		Query query = con.query();

		query << "SELECT asset_id, id, last_updated_datetime, texture_url, world_object_id, "
			  << "zone_index, zone_instance_id FROM world_object_player_settings";

		query.parse();

		StoreQueryResult resWorldObjPlayerSettings = query.store();

		for (ULONG ii = 0; ii < resWorldObjPlayerSettings.num_rows(); ii++) {
			query.reset();

			query << "INSERT INTO world_object_player_settings ( asset_id, id, last_updated_datetime, texture_url, "
				  << "world_object_id, zone_index, zone_instance_id ) "
				  << "VALUES ( %0, %1, %2q, %3q, %4q, %5, %6 )";

			query.parse();

			theQuery = query.str(resWorldObjPlayerSettings.at(ii).at(0),
				resWorldObjPlayerSettings.at(ii).at(1),
				resWorldObjPlayerSettings.at(ii).at(2),
				resWorldObjPlayerSettings.at(ii).at(3),
				resWorldObjPlayerSettings.at(ii).at(4),
				resWorldObjPlayerSettings.at(ii).at(5),
				resWorldObjPlayerSettings.at(ii).at(6));
			theQuery += ";\n";

			write_len = fwrite(theQuery.c_str(), 1, theQuery.length(), fp_export);
			if ((write_len != theQuery.length()) || (ferror(fp_export))) {
				ret = false;
				wstring s(L"Error writing sql export file.");
				MessageBoxW(parent, s.c_str(), L"KEPEdit", MB_ICONWARNING);
				break;
			}

			// Target database is not current db. Sql statements captured for server install program.
			// Don't execute the sql statement.
		}
	}

	return ret;
}

static bool exportGlobalsToDb(HWND parent, IGetSet *engine,
	Connection &con, FILE *fp_export) {
	bool ret = true;
	bool retTmp = false;

	jsVerifyReturn(con, false);
	Logger logger(Logger::getInstance(L"ClientEngine"));

	Query query = con.query();

	IGetSet *game = engine->GetObjectPtr(KEPEDITVIEWIDS_GAME);
	jsVerifyReturn(game, false);

	string gameIdStr = game->GetString(GAMEIDS_GAMEID);
	int gameId = atoi(gameIdStr.c_str());

	string parentGameIdStr = game->GetString(GAMEIDS_PARENTID);
	int parentGameId = atoi(parentGameIdStr.c_str());

	// call SP that updates the server_view to restrict it to our game
	query.reset();
	query << "CALL check_game_servers_view( %0 )";
	query.parse();
	query.store(gameId);
	while (query.more_results())
		query.store_next(); // always pull off SP's RSets

	/* things to export:
		skills
		stats
		quests
		supported worlds
		global inventory
	*/

	{
		Transaction trans(con);

		exceptionTableName = "skills";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportSkills(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "titles";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportTitles(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "skill_levels";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportSkillLevels(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "skill_actions";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportSkillActions(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "level_rewards";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportLevelRewards(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "stats";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportStats(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "quests";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportQuests(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "supported_worlds";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportWorlds(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "spawn_points";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportSpawnPoints(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "items";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportGlobalInventory(logger, parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	// Export commerce tables: store_inventories, stores for STAR only.
	// They no longer exist in 3dapps.
	if (parentGameId == 0) {
		Transaction trans(con);
		exceptionTableName = "commerce";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportCommerceDb(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "dynamic_objects";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportDynamicObjects(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	{
		Transaction trans(con);
		exceptionTableName = "world_object_player_settings";
		LOG4CPLUS_INFO(logger, "Exporting " << exceptionTableName);
		retTmp = exportWorldObjectPlayerSettings(parent, con, engine, fp_export);

		if (retTmp == false) {
			ret = false;
		} else
			trans.commit();
	}

	LOG4CPLUS_INFO(logger, "Export of globals complete");

	return ret;
}

bool exportToDb(HWND parent, IGetSet *engine, Connection &con, const char * /*exportSqlPath*/, FILE *fp_export) {
	bool ret = false;
	jsVerifyReturn(con, false);

	string msg;

	try {
		UINT objectId = engine->GetNameId();
#ifndef DECOUPLE_DBCONFIG
		Query query = con.query();
		query << " SELECT FLOOR(IFNULL(MAX(version), 0)) "
				 " FROM schema_versions";
		query.parse();
		StoreQueryResult r = query.store();
		LONG dbVer = (long)(r.at(0).at(0));
		if (dbVer < MIN_DB_VERSION) {
			throw new KEPException(loadStrPrintf(IDS_DB_VERSION, (long)(r.at(0).at(0)), MIN_DB_VERSION), E_INVALIDARG);
		}

		if (dbVer >= 48) {
			// for version 48 and higher, we have the interface_change column
			query.reset();
			query << " SELECT IFNULL(SUM(interface_change),0) AS interface_change"
					 " FROM schema_versions "
					 " WHERE interface_change = 'T' AND version > %0";
			query.parse();
			StoreQueryResult r2 = query.store(MIN_DB_VERSION);
			if ((long)(r2.at(0).at(0))) {
				throw new KEPException(loadStrPrintf(IDS_DB_VERSION, (long)(r2.at(0).at(0)), MIN_DB_VERSION), E_INVALIDARG);
			}
		}
#else
#endif
		switch (objectId) {
			case KEPEDITVIEWIDS:
				ret = exportGlobalsToDb(parent, engine, con, fp_export);
				break;
			case ZONEOBJECTSIDS:
				ret = exportZoneObjects(parent, engine, con, fp_export);
				break;
			case STOCKANIMATIONGLIDSIDS:
				ret = exportStockAnimationGlids(parent, engine, con, fp_export);
				break;
		}

	} catch (const mysqlpp::BadQuery &er) {
		msg = "Exception during dbExport. Table:";
		msg += exceptionTableName;
		msg += " Exception:";
		msg += er.what();
		if (fp_export) {
			fwrite(msg.c_str(), 1, msg.length(), fp_export);
		}
		throw new KEPException(loadStrPrintf(IDS_EXPORT_QUERY_ERROR, exceptionTableName, er.what()));
	} catch (const mysqlpp::BadConversion &er) {
		msg = "Exception during dbExport. Table:";
		msg += exceptionTableName;
		msg += " Exception:";
		msg += er.what();
		if (fp_export) {
			fwrite(msg.c_str(), 1, msg.length(), fp_export);
		}
		// Handle bad conversions
		throw new KEPException(loadStrPrintf(IDS_EXPORT_CONVERSION_ERROR, exceptionTableName, er.what(), er.retrieved, er.actual_size));
	} catch (const mysqlpp::Exception &er) {
		msg = "Exception during dbExport. Table:";
		msg += exceptionTableName;
		msg += " Exception:";
		msg += er.what();
		if (fp_export) {
			fwrite(msg.c_str(), 1, msg.length(), fp_export);
		}
		// Catch-all for any other MySQL++ exceptions
		throw new KEPException(loadStrPrintf(IDS_EXPORT_ERROR, exceptionTableName, er.what()));
	}

	return ret;
}
