#pragma once

// CDbSettingsDlg dialog

class CDbSettingsDlg : public CDialog {
	DECLARE_DYNAMIC(CDbSettingsDlg)

public:
	CDbSettingsDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CDbSettingsDlg();

	// Dialog Data
	enum { IDD = IDD_DBSETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CStringA m_server;
	DWORD m_port;
	CStringA m_password;
	CStringA m_user;
	CStringA m_name;
	afx_msg void OnBnClickedTest();
	BOOL m_dontShowAgain;
	afx_msg void OnBnClickedSave();
};
