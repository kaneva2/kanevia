// DbSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "dbExporter.h"
#include "DbSettingsDlg.h"
#include ".\dbsettingsdlg.h"
#include "UnicodeMFCHelpers.h"
using KEP::DDV_MaxChars;
using KEP::DDX_Text;

// CDbSettingsDlg dialog

IMPLEMENT_DYNAMIC(CDbSettingsDlg, CDialog)
CDbSettingsDlg::CDbSettingsDlg(CWnd* pParent /*=NULL*/) :
		CDialog(CDbSettingsDlg::IDD, pParent), m_server("localhost"), m_port(3306), m_password(""), m_user(""), m_name(""), m_dontShowAgain(FALSE) {
}

CDbSettingsDlg::~CDbSettingsDlg() {
}

void CDbSettingsDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SERVER, m_server);
	DDV_MaxChars(pDX, m_server, 255);
	DDX_Text(pDX, IDC_PORT, m_port);
	DDV_MinMaxUInt(pDX, m_port, 0, 66000);
	DDX_Text(pDX, IDC_PASSWORD, m_password);
	DDV_MaxChars(pDX, m_password, 255);
	DDX_Text(pDX, IDC_USER, m_user);
	DDV_MaxChars(pDX, m_user, 255);
	DDX_Text(pDX, IDC_DBNAME, m_name);
	DDV_MaxChars(pDX, m_name, 255);
	DDX_Check(pDX, IDC_DONTSHOW, m_dontShowAgain);
}

BEGIN_MESSAGE_MAP(CDbSettingsDlg, CDialog)
ON_BN_CLICKED(IDOK, OnBnClickedOk)
ON_BN_CLICKED(IDB_TEST, OnBnClickedTest)
ON_BN_CLICKED(IDB_SAVE, OnBnClickedSave)
END_MESSAGE_MAP()

// CDbSettingsDlg message handlers

void CDbSettingsDlg::OnBnClickedOk() {
	OnOK();
}

#include <string>
#include <KEPHelpers.h>
using namespace std;
bool testConnect(const char* dbServer, const char* dbUser, const char* dbPassword,
	const char* dbName, ULONG dbPort, string& msg);

void CDbSettingsDlg::OnBnClickedTest() {
	UpdateData();

	string msg;
	if (testConnect(m_server, m_user, m_password, m_name, m_port, msg)) {
		MessageBox(Utf8ToUtf16(loadStr(IDS_EDIT_CONNECTOK)).c_str(), Utf8ToUtf16(loadStr(IDS_DBEXPORTER)).c_str(), MB_ICONINFORMATION);
	} else {
		MessageBox(Utf8ToUtf16(loadStrPrintf(IDS_EDIT_NOCONNECT, msg.c_str())).c_str(), Utf8ToUtf16(loadStr(IDS_DBEXPORTER)).c_str(), MB_ICONEXCLAMATION);
	}
}

void CDbSettingsDlg::OnBnClickedSave() {
	UpdateData();
	EndDialog(IDB_SAVE);
}
