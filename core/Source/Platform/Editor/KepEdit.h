/******************************************************************************
 KEPEdit.h

 Copyright (c) 2014 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include <fstream>
#include <string>

#include "resource.h" // main symbols
#include "KEPEditGlobals.h" // symbols for messaging and resourceids
#include "KEPEditView.h"
#include "ClientEngine.h"

// forward declarations
namespace CrashReporter {
struct CallbackStruct;
}

class CKEPEditApp : public CWinApp {
	FILE* m_stdOut;
	FILE* m_stdErr;

	string m_appVersion; ///< application.exe version string '##.##.##.##'
	string m_appPath; ///< application.exe path string '...\Kaneva\Star\####'

	string m_starPath; ///< star path string '...\Kaneva\Star\####'
	string m_starId; ///< star id string '####'

	bool m_isProduction; ///< runtime is in production environment

public:
	CKEPEditApp();
	~CKEPEditApp() {
		if (m_stdOut)
			fclose(m_stdOut);
		if (m_stdErr)
			fclose(m_stdErr);
	}

	virtual BOOL InitInstance();
	virtual int ExitInstance();

	/** DRF
	* Return the version string of the application.exe. (eg. '##.##.##.##')
	*/
	string AppVersion() const { return m_appVersion; }

	/** DRF
	* Return the path of the application.exe. (eg. 'c:/.../kaneva/star/####')
	*/
	string AppPath() const { return m_appPath; }

	/** DRF
	* Return the star path of the runtime environment. (eg. 'c:/.../kaneva/star/####')
	*/
	string StarPath() const { return m_starPath; }

	/** DRF
	* Return the star identifier of the runtime environment. (eg. '####')
	*/
	string StarId() const { return m_starId; }

	/** DRF 
	* Return the project name of the runtime environment. (eg. 'KEPEdit (xxx)')
	*/
	string ProjectName() const;

	/** DRF 
	* Return the wok url of the runtime environment. (eg. 'http://wok.kaneva.com/kgp/')
	*/
	string WokUrl() const;

	/** DRF 
	* Return the patch url of the runtime environment. (eg. 'http://patch.kaneva.com/<version>/')
	*/
	string PatchUrl() const;

	/** DRF 
	* Return whether or not the client app is in production environment. 
	*/
	bool IsProduction() const { return m_isProduction; }

	inline void SetView(CKEPEditView* theView) { m_theView = theView; }
	CKEPEditView* GetView() { return m_theView; }
	void SetProcessingModeForeground(BOOL val) { m_processingModeForeground = val; }
	BOOL GetProcessingModeForeground() { return m_processingModeForeground; }

	// Continue message pump and update progress bar on lengthy loads
	void ProgressUpdate(CStringA position);

	void MRUFileHandler(UINT i);
	void MRURemoveFile(int index);

public:
	// Implementation
	afx_msg void OnAppAbout();
	afx_msg void OnSendFeedBack();
	afx_msg void OnMenu_Help_GamePub();

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnIdle(LONG lCount);

	// From ClientEngine to support Editor UI

	afx_msg void OnButtonQuit();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonTreetest();
	afx_msg void OnTextureFileOpen1();
	afx_msg void OnTextureFileOpen2();
	afx_msg void OnTextureFileOpen3();
	afx_msg void OnTextureFileOpen4();
	afx_msg void OnButtonObjectload();
	afx_msg void OnButtonObjectdelete();
	afx_msg void OnButtonSwitchObject();
	afx_msg void OnBUTTONBKEDITORswtch();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonMvUp();
	afx_msg void OnButtonMvDown();
	afx_msg void OnButtonMvLeft();
	afx_msg void OnButtonMvRight();
	afx_msg void OnButtonAddnewstaticBk();
	afx_msg void OnBUTTONBKSETTINGSapply();
	afx_msg void OnButtonControls();
	afx_msg void OnButtonTestEffect();
	afx_msg void OnButtonStopEffect();
	afx_msg void OnButtonMovies();
	afx_msg void OnButtonTestmovie();
	afx_msg void OnBtnEntityCreatenew();
	afx_msg void OnBtnEntityActivate();
	afx_msg void OnButtonSwEntities();
	afx_msg void OnButtonTestfullscreen();
	afx_msg void OnBtnViewwireframe();
	afx_msg void OnButtonViewsmoothshaded();
	afx_msg void OnBtnEntityMovement();
	afx_msg void OnBtnEntityDeactivate();
	afx_msg void OnBtnDeleteEntity();
	afx_msg void OnBTNENTITYdeleteAnim();
	afx_msg void OnBTNENTITYDeleteChld();
	afx_msg void OnBtnEntityAddchild();
	afx_msg void OnBtnNewall();
	afx_msg void OnBTNCameraViewportsSw();
	afx_msg void OnCAMERACreateNewCamera();
	afx_msg void OnBTNCreateNewViewport();
	afx_msg void OnBTNCameraVportActivate();
	afx_msg void OnBTNvportDeactivate();
	afx_msg void OnBtnAddRuntimeCamera();
	afx_msg void OnBtnRemoveRuntimeCamera();
	afx_msg void OnBtnAddRuntimeViewport();
	afx_msg void OnBtnRemoveRuntimeViewport();
	afx_msg void OnBTNMOUSEcreate();
	afx_msg void OnBTNUImouseSW();
	afx_msg void OnBTNMOUSEfileBrowse();
	afx_msg void OnBTNMOUSEactivate();
	afx_msg void OnBTNMOUSEdelete();
	afx_msg void OnBTNMOUSEdeactivate();
	afx_msg void OnBTN2DINTERFACEsw();
	afx_msg void OnBTN2DCreateCollection();
	afx_msg void OnBTN2DADDunit();
	afx_msg void OnBtn2dActivate();
	afx_msg void OnBtn2dDeactivate();
	afx_msg void OnBTN2DBrowseFileUp();
	afx_msg void OnBTN2DBrowseFileDown();
	afx_msg void OnBTN2DgetSelectedInfo();
	afx_msg void OnBTN2DApplyModifications();
	afx_msg void OnBTN2DDeleteCollection();
	afx_msg void OnBTN2DDeleteUnit();
	afx_msg void OnBUTTONEnviormentSw();
	afx_msg void OnBTNENVBckRgb();
	afx_msg void OnBTNENVFogRgb();
	afx_msg void OnBTNENVApplyChanges();
	afx_msg void OnBTNENVMatch();
	afx_msg void OnBTNENVBrowseSurObj();
	afx_msg void OnCHECKEnableSurrounding();
	afx_msg void OnBTNObjectMaterial();
	afx_msg void OnBTNENVemmisive();
	afx_msg void OnBTNEntityMaterial();
	afx_msg void OnBUTTONtextAddTextCollection();
	afx_msg void OnBTNtextFontSW();
	afx_msg void OnBUTTONtextAddTextObject();
	afx_msg void OnBTNtextActivate();
	afx_msg void OnBTNtextDeactivate();
	afx_msg void OnBUTTONtextTextColor();
	afx_msg void OnBTNtextApplyChanges();
	afx_msg void OnBTNtextGetCurrentInfo();
	afx_msg void OnBTNtextApplyFont();
	afx_msg void OnBUTTONtextAddFont();
	afx_msg void OnBTNtextApplySizeChange();
	afx_msg void OnBUTTONtextDeleteFont();
	afx_msg void OnBUTTONtextDeleteTextCollection();
	afx_msg void OnBUTTONtextDeleteTextObject();
	afx_msg void OnBTNtestSave();
	afx_msg void OnBTNtestLoad();
	afx_msg void OnBTNcamVportSave();
	afx_msg void OnBTNcamVportLoad();
	afx_msg void OnBTNDeleteCamera();
	afx_msg void OnBTNDeleteViewport();
	afx_msg void OnBTNUpdateViewport();
	afx_msg void OnBTNUpdateRuntimeViewport();
	afx_msg void OnBTN2DSaveConfig();
	afx_msg void OnBTN2DLoadConfig();
	afx_msg void OnBTNExplosionDBSW();
	afx_msg void OnBTNEXPAddExplosion();
	afx_msg void OnBTNEXPLoadExplosions();
	afx_msg void OnBTNEXPSaveExplosions();
	afx_msg void OnBTNEXPDeleteExplosion();
	afx_msg void OnBTNCameraAdvanced();
	afx_msg void OnBTNMovieAddMovie();
	afx_msg void OnBTNMovieTestMovie();
	afx_msg void OnBTNMovieEditMovie();
	afx_msg void OnBTNMovieDeleteMovie();
	afx_msg void OnBTNMoviesSaveDB();
	afx_msg void OnBTNMoviesLoadDB();
	afx_msg void OnBTNMovieStopMovies();
	afx_msg void OnBTNControlAddObject();
	afx_msg void OnBTNControlAddEvent();
	afx_msg void OnBTNControlCfgSave();
	afx_msg void OnBTNControlCfgLoad();
	afx_msg void OnBTNControlDeleteObject();
	afx_msg void OnBTNControlDeleteEvent();
	afx_msg void OnSelchangeLISTControlDB();
	afx_msg void OnBTNControlEditObject();
	afx_msg void OnBTNControlEditEvent();
	afx_msg void OnBTNUIBaseAdvanced();
	afx_msg void OnBTNObjectArrayAdd();
	afx_msg void OnBTNENTITYProperties();
	afx_msg void OnBTNOBJECTProperties();
	afx_msg void OnBTNOBJDeleteLight();
	afx_msg void OnBtnObjAddlight();
	afx_msg void OnBTNENTMergeData();
	afx_msg void OnBTNENTAddLight();
	afx_msg void OnBTNUIMusic();
	afx_msg void OnBTNMusicAssignTrack();
	afx_msg void OnBTNMusicAssignWav();
	afx_msg void OnBTNMusicUnassignTrack();
	afx_msg void OnBTNMusicUnassignWav();
	afx_msg void OnBTNMusicBrowseWav();
	afx_msg void OnBTNUITextureAssign();
	afx_msg void OnBTNUIMissileDatabase();
	afx_msg void OnBTNMISAddMissile();
	afx_msg void OnBTNMISBrowseFileName();
	afx_msg void OnBTNMISDeleteMissile();
	afx_msg void OnBTNMISsave();
	afx_msg void OnBTNMISLoad();
	afx_msg void OnBTNUIBulletHole();
	afx_msg void OnBTNBHAddHole();
	afx_msg void OnBTNBHBrowse();
	afx_msg void OnBTNBHAlphaTexture();
	afx_msg void OnBTNBHDeleteHole();
	afx_msg void OnBTNBHload();
	afx_msg void OnBTNBHMaterial();
	afx_msg void OnBTNBHSave();
	afx_msg void OnBTNEntityshadow();
	afx_msg void OnBTNObjectShadow();
	afx_msg void OnBTNENTAnimProperties();
	afx_msg void OnBTNEntitySound();
	afx_msg void OnBTNEntitysetSoundCenter();
	afx_msg void OnBTNOBJSound();
	afx_msg void OnBTNOBJAlphaTexture();
	afx_msg void OnBTNENTAnimSound();
	afx_msg void OnBTNTCPIPStartHost();
	afx_msg void OnBTNTCPIPStopHost();
	afx_msg void OnBTNTCPIPJoinHost();
	afx_msg void OnBTNUIMultiplayer();
	afx_msg void OnBTNEntityCombineEntityVis();
	afx_msg void OnBTNENTContentProperties();
	afx_msg void OnBTNTEXAddCollection();
	afx_msg void OnBTNTEXDeleteCollection();
	afx_msg void OnBTNUIAnimTextureDB();
	afx_msg void OnBTNTEXLoadCollection();
	afx_msg void OnBTNTEXSaveCollection();
	afx_msg void OnBTNENTLoadDB();
	afx_msg void OnBTNENTSaveDBSmoothNormals();
	afx_msg void OnBTNENTSaveDB();
	afx_msg void OnButtonSaveANM();
	afx_msg void OnButtonSaveANMU();
	afx_msg void OnBTNENTSaveDBExpanded();
	afx_msg void OnBTNENTRemoveLOD();
	afx_msg void OnBTNENTLoadDBExpanded();
	afx_msg void OnBtnUiScript();
	afx_msg void OnBTNSCRAddScript();
	afx_msg void OnBTNSCRDeleteEvent();
	afx_msg void OnBTNSCRLoadExecFile();
	afx_msg void OnBTNSCRBuild();
	afx_msg void OnBTNSCREditEvent();
	afx_msg void OnBTNOBJEditLight();
	afx_msg void OnBTNENTLoadEntity();
	afx_msg void OnBTNENTSaveEntity();
	afx_msg void OnBTNMSSaveDB();
	afx_msg void OnBTNMSLoadDB();
	afx_msg void OnBtnTxtdbAdd();
	afx_msg void OnBtnTxtdbDelete();
	afx_msg void OnBtnTxtdbEdit();
	afx_msg void OnBtnTxtdbReinitAll();
	afx_msg void OnBtnTxtdbReinitFromDisk();
	afx_msg void OnBtnTxtdbSetSubtraction();
	afx_msg void OnBTNOBJClearTexCoords();
	afx_msg void OnBTNOBJAddTexCoords();
	afx_msg void OnBTNSCRSetToScene();
	afx_msg void OnBTNSCRClearScene();
	afx_msg void OnBTNSCRLoadFromScene();
	afx_msg void OnBTNNTAdvanced();
	afx_msg void OnBTNNTBuildTxtRecord();
	afx_msg void OnBTNUIHudInterface();
	afx_msg void OnBTNHUDAddDisplayUnit();
	afx_msg void OnBTNHUDAddNewHud();
	afx_msg void OnBTNHUDSaveDB();
	afx_msg void OnBTNHUDLoadDB();
	afx_msg void OnSelchangeListHudDb();
	afx_msg void OnBTNHUDDeleteHud();
	afx_msg void OnBTNHUDDeleteDisplayUnit();
	afx_msg void OnBTNHUDReplaceUnit();
	//afx_msg void OnBTNUIMenuSystem();
	//afx_msg void OnBTNMENUAddMenu();
	//afx_msg void OnBTNMENUAddUnit();
	//afx_msg void OnBTNMENUSave();
	//afx_msg void OnBTNMENULoad();
	//afx_msg void OnBTNMENUEditMenu();
	//afx_msg void OnBTNMENUEditDisplayUnit();
	//afx_msg void OnBTNMENUDeleteMenu();
	//afx_msg void OnBTNMENUDeleteUnit();
	afx_msg void OnBTNENTUpdate();
	afx_msg void OnSelchangeLISTTXTDBDatabase();
	afx_msg void OnCHECKTEXBltOn();
	//afx_msg void OnBTNMenuClone();
	//afx_msg void OnBTNMenuMaterial3D();
	//afx_msg void OnBTNMenuAddAnimation();
	//afx_msg void OnSelchangeLISTMenuAnimations();
	//afx_msg void OnSelchangeLISTMENUUnitList();
	//afx_msg void OnBTNMenuproperties();

	afx_msg void OnBTNENVambientDayLight();

	afx_msg void OnBTNUIParticleSys();
	afx_msg void OnBTNPARTEditSystem();
	afx_msg void OnBTNPARTLoadDatabase();
	afx_msg void OnBTNUISpawnGen();
	afx_msg void OnBTNSpawnAddGen();
	afx_msg void OnBTNSpawnDeleteGen();
	afx_msg void OnBTNSpawnEditGen();
	afx_msg void OnBTNObjectBuildCollisionMap();
	//afx_msg void OnClickObjectTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangedObjectTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBTNObjectsAddGroup();
	afx_msg void OnBTNObjectsDeleteGroup();
	afx_msg void OnBTNObjectsEditGroup();
	afx_msg void OnSelchangeLISTObjectsGroupList2Box();
	afx_msg void OnDblclkLISTObjectsGroupList2Box();
	afx_msg void OnBTNGroupModifier();
	afx_msg void OnDblclkObjectTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBTNUIMerge();
	afx_msg void OnBTNTCPDisconnectPlayer();
	afx_msg void OnBTNTCPServerLoad();
	afx_msg void OnBTNTCPServerSave();
	afx_msg void OnBTNOptimizeStates();
	afx_msg void OnBTNTCPNewAccount();
	afx_msg void OnBTNTCPDeleteAccount();
	afx_msg void OnBTNOBJAddTrigger();
	afx_msg void OnBTNOBJUpdateObject();
	afx_msg void OnCHECKTCPRegistered();
	afx_msg void OnSelchangeLISTTCPIPCurrentPlayers();
	afx_msg void OnBTNTXSaveTextureBankStatic();
	afx_msg void OnBTNTXSaveTextureBankDynamic();
	afx_msg void OnBTNTXLoadTextureBank();
	//afx_msg void OnBTNMNUAnimProperties();
	//afx_msg void OnBTNMNUDeleteAnimation();
	afx_msg void OnBTNOBJGetSignificant();
	afx_msg void OnBTNOBJTextureSetMacro();
	afx_msg void OnBTNAICreateNew();
	afx_msg void OnBtnUiAi();
	afx_msg void OnBTNAIDelete();
	afx_msg void OnBtnObjLodadd();
	afx_msg void OnBTNOBJRemoveLODLevel();
	afx_msg void OnBTNOBJEditLOD();
	afx_msg void OnBTNENTITYAnimations();
	afx_msg void OnBTNENTAppendCustomDB();
	afx_msg void OnSelchangeLISTENTITYRuntimeList();
	afx_msg void OnBTNUIHaloEffects();
	afx_msg void OnBTNAddHalo();
	afx_msg void OnBTNDeleteHalo();
	afx_msg void OnBTNEditHalo();
	afx_msg void OnBTNHaloLoad();
	afx_msg void OnBTNHaloSave();
	afx_msg void OnBTNAddLensFlare();
	afx_msg void OnBTNEditLensFlare();
	afx_msg void OnBTNDeleteLensFlare();
	afx_msg void OnBTNGenerateMesh();
	afx_msg void OnBTNENTLODSystems();
	afx_msg void OnBTNENTDelLOD();
	afx_msg void OnBTNENTUpdateVisuals();
	afx_msg void OnBTNENTResourceBase();
	afx_msg void OnBTNENTViewRuntimeStats();
	afx_msg void OnBTNENTViewRuntimeResources();
	afx_msg void OnSelchangeLISTMISmissileDataBase();
	afx_msg void OnBTNMISReplaceValues();
	afx_msg void OnBTNMISEditVisual();
	afx_msg void OnBTNMISAddBillboardMissile();
	afx_msg void OnBTNMISEditDynamics();
	afx_msg void OnBTNENTEditStartInven();
	afx_msg void OnDropdownCMBOCGTEXFontTableTexture();
	afx_msg void OnBTNCGTEXAdd();
	afx_msg void OnBTNCGTEXDelete();
	afx_msg void OnBTNAIAddEvent();
	afx_msg void OnBTNAIAddScript();
	afx_msg void OnBTNAIEditScript();
	afx_msg void OnBTNAIDeleteScript();
	afx_msg void OnBTNAIEditEvent();
	afx_msg void OnBTNAIDeleteEvent();
	afx_msg void OnBTNAIAddTree();
	afx_msg void OnBTNAIDeleteTree();
	afx_msg void OnBTNAIEdit();
	afx_msg void OnBTNAISaveAiData();
	afx_msg void OnBTNAILoadAiData();
	afx_msg void OnSelchangeLISTAIScripts();
	afx_msg void OnBTNENTSaveAppendages();
	afx_msg void OnBTNENTLoadAppendages();
	afx_msg void OnBtnEntMount();
	afx_msg void OnBtnEntDemount();
	afx_msg void OnBTNEXPEditExplosionObj();
	afx_msg void OnChangeEDITTCPIPadress();
	afx_msg void OnChangeTCPuserName();
	afx_msg void OnChangeTCPpassword();
	afx_msg void OnBTNSystemGoingDown();
	afx_msg void OnBTNENTAddItem();
	afx_msg void OnBTNENTItemTest();
	afx_msg void OnBTNENTDeleteItem();
	afx_msg void OnBTNENTDisarmItem();
	afx_msg void OnBTNENTEditItem();
	afx_msg void OnBTNOBJClone();
	afx_msg void OnBTNOBJTransformation();
	afx_msg void OnBTNOBJBoundingGroupSet();
	afx_msg void OnBTNOBJLibraryAccess();
	afx_msg void OnBTNENVTimeSystem();
	afx_msg void OnBTNMISAddSkeletalMissile();
	afx_msg void OnBTNMISAddLaser();
	afx_msg void OnSelchangeEquipItem();
	afx_msg void OnSelchangeEquippedItemMeshSlot();
	afx_msg void OnSelchangeEquippedItemMesh();
	afx_msg void OnSelchangeEquippedItemMaterial();
	afx_msg void OnBTNAIEditAITree();
	afx_msg void OnBTNTCPSupportedWorlds();
	afx_msg void OnBTNENTEditDeathCfg();
	afx_msg void OnBTNTCPViewAccount();
	afx_msg void OnCHECKTXBBitmapLoadOverride();
	afx_msg void OnBTNUISkillBuilder();
	afx_msg void OnBTNOBJAddNode();
	afx_msg void OnBTNOBJReferenceLibrary();
	afx_msg void OnBTNUISoundDB();
	afx_msg void OnBTNUIGlInventoryDB();
	afx_msg void OnBtnUiCurrencysys();
	afx_msg void OnBTNUICommerceDB();
	afx_msg void OnBTNUIBankZones();
	afx_msg void OnBtnUiSafezones();
	afx_msg void OnBTNENTUnifySleletalMesh();
	afx_msg void OnBTNMISDeleteLight();
	afx_msg void OnBTNMISLightSys();
	afx_msg void OnBtnUiStatdb();
	afx_msg void OnBTNTCPItemGeneratorDB();
	afx_msg void OnBtnPatchsys();
	afx_msg void OnBTNTCPUpdateCurrentOnlinePlayers();
	afx_msg void OnBTNENTScaleEntity();
	afx_msg void OnCHECKSPNEnableSystem();
	afx_msg void OnBTNTXBSetMipLevels();
	afx_msg void OnBTNTCPUpdateAllAcounts();
	afx_msg void OnBTNENTReplaceEntityWSIE();
	afx_msg void OnBTNTCPIPBlocking();
	afx_msg void OnBtnUiWaterzones();
	afx_msg void OnBtnUiPortalzonesdb();
	afx_msg void OnBTNUIElementDB();
	afx_msg void OnBTNTCPUpgradeWorm();
	afx_msg void OnBTNEXPSaveUnit();
	afx_msg void OnBTNEXPLoadUnit();
	afx_msg void OnBTNTCPDisconnectAll();
	afx_msg void OnBTNTCPPrintReports();
	afx_msg void OnBTNAILoadAiUnit();
	afx_msg void OnBTNAISaveAiUnit();
	afx_msg void OnBTNAIImportCollisionSys();
	afx_msg void OnBTNAIDeleteSys();
	afx_msg void OnBTNAIExportCurrentCol();
	afx_msg void OnBTNAISaveColDB();
	afx_msg void OnBTNAILoadColDB();
	afx_msg void OnBTNMISSaveUnit();
	afx_msg void OnBTNMISLoadUnit();
	afx_msg void OnBTNSPNExportSys();
	afx_msg void OnBTNSPNImportSys();
	afx_msg void OnBTNAIIntegrityCheck();
	afx_msg void OnBTNTCPSearchForAccount();
	afx_msg void OnBTNTCPRestoreBackup();
	afx_msg void OnBTNTXTSymbolDB();
	afx_msg void OnCHECKEditorTxtMode();
	afx_msg void OnBTNENTMassFormatNames();
	afx_msg void OnCHKOBJEnablePlaementHotKey();
	afx_msg void OnBTNOBJReportCurRenderCache();
	afx_msg void OnBTNUIIconDBACCESS();
	afx_msg void OnBTNTCPSnapShotPacketQue();
	afx_msg void OnBTNTCPGenerateLogBasedOnTime();
	afx_msg void OnBTNUIHousingSys();
	afx_msg void OnBTNUIHszoneDB();
	afx_msg void OnBTNChkshowSpeed();
	afx_msg void OnBTNAInewTree();
	afx_msg void OnBTNTCPExportAccount();
	afx_msg void OnBTNTCPImportAccount();
	afx_msg void OnBTNTCPGenerateClanDatabase();
	afx_msg void OnBTNTCPClearInactive();
	afx_msg void OnBTNUIAIRainSys();
	afx_msg void OnBTNENTsaveItemDB();
	afx_msg void OnBTNENTLoadItemDB();
	afx_msg void OnBTNSPNAppendSys();
	afx_msg void OnButtonLoad();
	afx_msg void OnBTNTestAssign();
	afx_msg void OnBTNCommCntrlLibBtnA();
	afx_msg void OnBTNWorldRule();
	afx_msg void OnBTNArenaManager();
	afx_msg void OnObjectSelect();
	afx_msg void OnObjectMove();
	afx_msg void OnObjectRotate();
	afx_msg void OnObjectScale();
	afx_msg void OnCollapse();
	afx_msg void OnSelchangeViewports();
	afx_msg void OnSelchangeRuntimeViewports();
	afx_msg void OnBTNDeleteGuids();

private:
	CKEPEditView* m_theView;
	BOOL m_processingModeForeground;

	ULONG_PTR m_gdiplusToken;

public:
	/////////////////////////
	// Crash Report Support
	/////////////////////////
	bool CR_Install(const string& projectName);
	void CR_AppCallback(CrashReporter::CallbackStruct& cbs);
};

extern CKEPEditApp appInstance; // singleton app instance
