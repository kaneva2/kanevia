#pragma once

class CArchive;
class CDynamicObjList;
class CReferenceObjList;
class StreamableDynamicObject;
class StreamableDynamicObjectVisual;
class MaterialCompressor;
struct DYNAMIC_OBJ_SYSTEM_V2;

namespace KEP {
class DynamicObjectRM;
}

// changing the max lod count will change the file format.
#define MAX_LOD_COUNT 10

// defines for when serialization struct == runtime struct
#define DO_LIGHT DO_LIGHT_V1
#define DO_LIGHT_PLACEMENT DO_LIGHT_PLACEMENT_V1
#define DO_POPOUT DO_POPOUT_V1
#define DO_VISUAL DO_VISUAL_V2
#define DO_VISUAL_ASSIGNMENT DO_VISUAL_ASSIGNMENT_V1
#define DO_LOD DO_LOD_V1
#define DO_SYS_STRUCT DYNAMIC_OBJ_SYSTEM_V2

// Serialization helper class handles legacy DOL file format
class DynamicObjectLibrary {
public:
	// Reads a dynamic object system from an archive.  This archive contains all mesh data and meta-data for dynamic objects.
	static DynamicObjectRM* readFromDisk(CArchive& ar);

private:
	static void readLightv1(CArchive& ar, StreamableDynamicObject* ob);
	static void readLight(CArchive& ar, StreamableDynamicObject* ob);

	static StreamableDynamicObjectVisual* readVisualv1(CArchive& ar);
	static StreamableDynamicObjectVisual* readVisualv2(CArchive& ar);
	static StreamableDynamicObjectVisual* readVisual(CArchive& ar);

	static StreamableDynamicObject* readDynamicObjectv1(CArchive& ar);
	static StreamableDynamicObject* readDynamicObject(CArchive& ar);

	static DynamicObjectRM* readSystemv1(CArchive& ar);
	static DynamicObjectRM* readSystemv2(CArchive& ar, MaterialCompressor* matCompressor, int version);

	static void deCompress(DynamicObjectRM* sys, DO_SYS_STRUCT& sysStruct, const CStringA& archiveName);
	static void readDynamicObjMeshesIntoPool(CArchive& ar);
	static void deleteAndZero(DO_SYS_STRUCT& sysStruct);

private:
	static const unsigned char light_archive_code = 123;
	static const unsigned char visual_archive_code = 222;
	static const unsigned char sys_archive_code = 118;
};
