// KEPEditDoc.h : interface of the CKEPEditDoc class
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved

#pragma once

class CKEPEditDoc : public CDocument {
protected: // create from serialization only
	CKEPEditDoc();
	DECLARE_DYNCREATE(CKEPEditDoc)

	// Attributes
public:
	// Operations
public:
	// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

	// Implementation
public:
	virtual ~CKEPEditDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};
