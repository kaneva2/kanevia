/******************************************************************************
 DistributionUpdateDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "stdafx.h"
#include "KEPEdit.h"
#include "DistributionUpdateDlg.h"

#include "LogHelper.h"
static LogInstance("Exception");

extern UINT gMsgId;

CStringA CDistributionUpdateDlg::m_encryptKey;
CStringA CDistributionUpdateDlg::m_gameId;
CStringA CDistributionUpdateDlg::m_destinationPath;
CStringA CDistributionUpdateDlg::m_patchFolderLocation;
CStringA CDistributionUpdateDlg::m_licenseKey;
CKEPEditView *CDistributionUpdateDlg::m_theView;
HANDLE CDistributionUpdateDlg::m_hEvDistributionHasClosed;
HANDLE CDistributionUpdateDlg::m_hEvDialogClosing;
EDBExport CDistributionUpdateDlg::m_exportToDb = dbe_dontExportToDb;
CRCLIST *CDistributionUpdateDlg::m_oldPatchFiles;
int CDistributionUpdateDlg::m_oldFileCount;
bool CDistributionUpdateDlg::m_tristrip;
bool CDistributionUpdateDlg::m_encryptAssets;
bool CDistributionUpdateDlg::m_disableDialogs;
bool CDistributionUpdateDlg::m_menusOnly = false;

BEGIN_MESSAGE_MAP(CDistributionUpdateDlg, CDialog)
ON_REGISTERED_MESSAGE(gMsgId, OnStatusMessage)
END_MESSAGE_MAP()

// CDistributionUpdateDlg dialog

IMPLEMENT_DYNAMIC(CDistributionUpdateDlg, CDialog)
CDistributionUpdateDlg::CDistributionUpdateDlg(CWnd *pParent, CKEPEditView *theView,
	CStringA encryptKey, CStringA gameId, CStringA licenseKey, CStringA destinationPath, CStringA patchFolderLocation,
	EDBExport exportToDb, bool tristripAssets, CRCLIST *oldPatchFiles, int oldFileCount,
	bool encryptAssets, bool disableDialogs, bool menusOnly) :
		CDialog(CDistributionUpdateDlg::IDD, pParent) {
	m_theView = theView;
	m_encryptKey = encryptKey;
	m_gameId = gameId;
	m_destinationPath = destinationPath;
	m_patchFolderLocation = patchFolderLocation;
	m_licenseKey = licenseKey;
	m_exportToDb = exportToDb;
	m_tristrip = tristripAssets;
	m_oldPatchFiles = oldPatchFiles;
	m_oldFileCount = oldFileCount;
	m_encryptAssets = encryptAssets;
	m_disableDialogs = disableDialogs;
	m_menusOnly = menusOnly;
}

CDistributionUpdateDlg::~CDistributionUpdateDlg() {
	if (m_hEvDialogClosing) {
		CloseHandle(m_hEvDialogClosing);
	}

	if (m_hEvDistributionHasClosed) {
		CloseHandle(m_hEvDistributionHasClosed);
	}
}

void CDistributionUpdateDlg::DoDataExchange(CDataExchange *pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDCANCEL, m_cancelButton);
	DDX_Control(pDX, IDOK, m_okButton);
}

UINT CDistributionUpdateDlg::showIt(LPVOID pParam) {
	HCURSOR prev = SetCursor(::LoadCursor(NULL, IDC_WAIT));
	m_theView->CreateFullDistribution(((CDistributionUpdateDlg *)pParam)->m_hWnd, m_encryptKey, m_gameId, m_licenseKey,
		m_destinationPath, m_patchFolderLocation, m_hEvDialogClosing, m_hEvDistributionHasClosed, m_exportToDb, m_tristrip,
		m_encryptAssets, m_disableDialogs, m_oldPatchFiles, m_oldFileCount, m_menusOnly);
	SetCursor(prev);
	return 0;
}

BOOL CDistributionUpdateDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	CStringW step;

	m_cancelButton.EnableWindow(TRUE);
	m_okButton.EnableWindow(FALSE);

	if (m_disableDialogs) {
		m_cancelButton.EnableWindow(FALSE);
		m_cancelButton.ShowWindow(SW_HIDE);
		m_okButton.EnableWindow(FALSE);
		m_okButton.ShowWindow(SW_HIDE);
	}

	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_1);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS1, step);
	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_2);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS2, step);
	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_3);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS3, step);
	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_4);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS4, step);
	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_5);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS5, step);
	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_6);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS6, step);
	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_7);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS7, step);
	step.LoadString(IDS_CREATE_DISTRIBUTION_OP_8);
	SetDlgItemText(IDC_DISTRIBUTION_WAIT_STATUS8, step);

	// Two events to handle canceling

	// signaled when user cancels via the dialog button
	m_hEvDialogClosing = CreateEvent(NULL, TRUE, FALSE, NULL);
	// signaled when distribution received a request to close and it has processed this message
	m_hEvDistributionHasClosed = CreateEvent(NULL, TRUE, FALSE, NULL);

	VERIFY(m_hEvDialogClosing != NULL && m_hEvDistributionHasClosed != NULL);

	AfxBeginThread(showIt, (LPVOID)this, THREAD_PRIORITY_NORMAL);
	return true;
}

afx_msg LRESULT CDistributionUpdateDlg::OnStatusMessage(WPARAM wp, LPARAM lp) {
	CStringW title;
	STATUSU *stat;

	switch (wp) {
		case MSG_STATUS: {
			stat = (STATUSU *)lp;
			std::wstring lastItemCompleteTxtW = Utf8ToUtf16(stat->lastItemCompleteTxt);
			switch (stat->stepId) {
				case 1:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_1);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					break;
				case 2:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_2);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE1, lastItemCompleteTxtW.c_str());
					break;
				case 3:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_3);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE2, lastItemCompleteTxtW.c_str());
					break;
				case 4:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_4);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE3, lastItemCompleteTxtW.c_str());
					break;
				case 5:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_5);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE4, lastItemCompleteTxtW.c_str());
					break;
				case 6:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_6);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE5, lastItemCompleteTxtW.c_str());
					break;
				case 7:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_7);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE6, lastItemCompleteTxtW.c_str());
					break;
				case 8:
					title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_8);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
					SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE7, lastItemCompleteTxtW.c_str());
					break;
				default:
					break;
			}
			break;
		}
		case MSG_ERROR:
			CDialog::EndDialog(0);
			break;
		case MSG_STATUS_ENCRYPTING:
			stat = (STATUSU *)lp;
			SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE7, Utf8ToUtf16(stat->fileName).c_str());
			break;
		case MSG_STATUS_CLIENT_UPDATE:
			stat = (STATUSU *)lp;
			SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE1, Utf8ToUtf16(stat->stepDesc).c_str());
			break;
		case MSG_STATUS_SERVER_UPDATE:
			stat = (STATUSU *)lp;
			SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE2, Utf8ToUtf16(stat->stepDesc).c_str());
			break;
		case MSG_STATUS_MAKEDEPLOY_UPDATE:
			stat = (STATUSU *)lp;
			SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE8, Utf8ToUtf16(stat->fileName).c_str());
			break;
		case MSG_COMPLETE:
			stat = (STATUSU *)lp;
			m_cancelButton.EnableWindow(FALSE);
			m_cancelButton.ShowWindow(SW_HIDE);
			m_okButton.EnableWindow(TRUE);
			title.LoadString(IDS_CREATE_DISTRIBUTION_STEP_COMPLETE);
			SetDlgItemText(IDC_DISTRIBUTION_WAIT_TITLE, title);
			SetDlgItemText(IDC_DISTRIBUTION_WAIT_COMPLETE8, Utf8ToUtf16(stat->lastItemCompleteTxt).c_str());
			if (m_disableDialogs)
				OnOK();
			break;
		default:
			break;
	}
	return 0;
}

void CDistributionUpdateDlg::OnCancel() {
	UpdateData();

	SetEvent(m_hEvDialogClosing);

	// Wait till the Distribution has a chance to recognize the
	// close request and acknowledge it.
	while (WaitForSingleObject(m_hEvDistributionHasClosed, 0) != WAIT_OBJECT_0) {
		// Process Message Loop Until Empty
		FOREVER {
			// Message Waiting?
			MSG msg;
			BOOL bRet = ::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
			if (bRet == -1) {
				LogError("PeekMessage() FAILED");
				break;
			}
			if (bRet == 0)
				break;

			// Dispatch Message
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	CDialog::OnCancel();
}

// DistributionUpdateDlg message handlers
