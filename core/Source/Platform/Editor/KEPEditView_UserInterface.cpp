#include "stdafx.h"
#include "ClientStrings.h"
#include "KEPEdit.h"
#include "KEPEditDoc.h"
#include "KEPFilenames.h"
#include "KEPEditView.h"
#include "KEPException.h"
#include "KEPEditorUtil.h"
#include "IAssetsDatabase.h"
#include "AssetTree.h"
#include "ServerSerialize.h"

using namespace KEP;

#include "MainFrm.h"
#include "KEPConfigurationsXML.h"

#include "clientbladefactory.h"
#include "KEPFileNames.h"
#include "NewGameInfo.h"

#ifndef _ClientOutput
#include "AddControlEventDlg.h"
#include "AddEntityDlg.h"
#include "AddItemDlg.h"
#include "AddParticleSystemDLG.h"
#include "AddScriptEventDlg.h"
#include "AddSpawnGeneratorDlg.h"
#include "AddTextureDlg.h"
#include "AdvancedNetworkDlg.h"
#include "ArenaManagerDlg.h"
#include "AIPathDlg.h"
#include "airaiddbclass.h"
#include "AiSettingsDlg.h"
#include "AnimTextureSwpAddDlg.h"
#include "AppendageCustDlg.h"
#include "BankZonesDlg.h"
#include "CameraAdvDlg.h"
#include "CollisionResolutionDlg.h"
#include "CommCrtlEditorDlg.h"
#include "CommerceDlg.h"
#include "ControlAddObjectDlg.h"
#include "CurrencyDlg.h"
#include "CursorLimits.h"
#include "CursorScaleDlg.h"
#include "EditStartInventoryDlg.h"
#include "ElementsDlg.h"
#include "EntityItemTestDlg.h"
#include "EntityLODSystemsDlg.h"
#include "EntityPropertiesDlg.h"
#include "ExplosionBasicDlg.h"
#include "GameDlg.h"
#include "GenerateMeshDLG.h"
#include "GetSignificantOptionsDlg.h"
#include "GlobalAdvancedDlg.h"
#include "GlobalInventoryDlg.h"
#include "GroupModifierDlg.h"
#include "houseplacementzonedlg.h"
#include "housingsysdlg.h"
#include "HudAddDisplayUnitDlg.h"
#include "IconSysDlg.h"
#include "ItemConflictDlg.h"
#include "LaserDlg.h"
#include "LightInterfaceDlg.h"
#include "LodSystemDlg.h"
#include "LodSystemDlg.h"
#include "MaterialDlg.h"
#include "MaterialShaderLibDlg.h"
#include "MergeOptionsDlg.h"
#include "MissileAddBillboardDlg.h"
#include "MissileBaseMeshEdit.h"
#include "MotionSettingsDlg.h"
#include "MovieSettingsDlg.h"
#include "PlacementDlg.h"
#include "PortalsDlg.h"
#include "ReferenceLibrary.h"
#include "ResourceEditorDlg.h"
#include "SafeZoneDlg.h"
#include "ScriptAdd.h"
#include "ScriptEventAdd.h"
#include "ShaderLibDlg.h"
#include "SkeletalAnimationDlg.h"
#include "SkillSystemDlg.h"
#include "SoundDatabaseDlg.h"
#include "SoundSettingsDlg.h"
#include "SpawnGenDlg.h"
#include "statisticsdbdlg.h"
#include "StatusDlg.h"
#include "SymbolMapDlg.h"
#include "TimeSystemDlg.h"
#include "TransformationDlg.h"
#include "TriggerEditorDlg.h"
#include "UpgradeWormDlg.h"
#include "VertexAnimSettingsDlg.h"
#include "ViewportClass.h"
#include "WaterZoneDBDLG.h"
#include "WorldChannelDlg.h"
#include "WorldGroupDlg.h"
#include "WorldObjectPropertiesDlg.h"
#include "CurrentWorldsDlg.h"
#include "RuntimeEntityViewerDlg.h"
#endif

#include "CameraDBToListItemMap.h"
#include "kepdiag.h"
#include "tools/wkglib/wkgtypes.h"
#include "tools/wkglib/math/vector3.h"
#include "legacymodelproxy.h"
#include "ReMeshCreate.h"
#include "SkeletalAnimationManager.h"
#include "CDeformableMesh.h"
#include "SkeletonConfiguration.h"
#include "UnicodeMFCHelpers.h"
#include "TreeCtrlHelper.h"
#include <js.h>
#include <jsEnumFiles.h>

using namespace TreeCtrlHelper;

// Default values
#define DEFAULT_MOV_CLIPPING_RADIUS 10
#define DEFAULT_MOV_CAMERA_ID 0

class StringCompare {
public:
	StringCompare() {}
	~StringCompare() {}

	bool operator()(const string &string1, const string &string2) {
		return (_stricmp(string1.c_str(), string2.c_str()) < 0);
	}
};

#ifndef VIEWING_IN_SOURCE_INSIGHT_TO_ALLOW_PARSING
IMPLEMENT_DYNCREATE(CKEPEditView, CView)

BEGIN_MESSAGE_MAP(CKEPEditView, CView)
ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
ON_WM_SIZE()
ON_WM_DESTROY()
ON_WM_ERASEBKGND()
ON_COMMAND(ID_APP_RENDER, OnAppRender)
ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
ON_COMMAND(ID_FILE_NEW, OnFileNew)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_ALL, OnUpdateFileSaveAll)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSave)
ON_COMMAND(ID_EDITOR_ZONE_SAVE, OnEditorZoneSave)
ON_COMMAND(ID_EDITOR_ZONE_SAVEAS, OnEditorZoneSaveAs)
ON_UPDATE_COMMAND_UI(ID_EDITOR_ZONE_SAVE, OnUpdateZoneSave)
ON_UPDATE_COMMAND_UI(ID_EDITOR_ZONE_SAVEAS, OnUpdateZoneSave)
ON_WM_KILLFOCUS()
ON_WM_LBUTTONDOWN()
ON_WM_MBUTTONDOWN()
ON_WM_KEYDOWN()
ON_UPDATE_COMMAND_UI(ID_APP_RENDER, OnUpdateAppRender)
ON_COMMAND(ID_FILE_SAVE, OnFileSave)
ON_COMMAND(ID_FILE_SAVE_ALL, OnFileSaveAll)
ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
ON_COMMAND(ID_EDITOR_ZONE_NEW, OnEditorZoneNew)
ON_COMMAND(ID_EDITOR_ZONE_OPEN, OnEditorZoneOpen)
ON_UPDATE_COMMAND_UI(ID_EDITOR_ZONE_OPEN, OnUpdateEditorZoneOpen)
ON_COMMAND(ID_START_STOP_SERVER, OnEditorStartStopServer)
ON_UPDATE_COMMAND_UI(ID_EDITOR_ZONE_NEW, OnUpdateEditorZoneNew)
ON_COMMAND(ID_MANAGE_TEXTURE_DATABASE, OnEditorToolsManageTextureDB)
ON_COMMAND(ID_PLACE_FIRST_ACTOR, OnEditorPlaceFirstActor)
ON_COMMAND(ID_TOGGLE_WIREFRAME, OnEditorToggleWireframe)
ON_COMMAND(ID_TOGGLE_LOCK_GAME_WINDOW, OnEditorToggleLockGameWindow)
ON_COMMAND(ID_GAME_PUBLISH, OnGamePublish)
ON_UPDATE_COMMAND_UI(ID_GAME_PUBLISH, OnUpdateFileSave)
ON_COMMAND(ID_GAME_CREATEDISTRIBUTION, OnGameDistribution)
ON_COMMAND(ID_GAME_RUN_SERVER_CLIENT, OnGameRun)
ON_COMMAND(ID_GAME_RUN_LAST_DISTRIBUTION, OnGameRunDistribution)
ON_UPDATE_COMMAND_UI(ID_GAME_CREATEDISTRIBUTION, OnUpdateFileSave)
ON_UPDATE_COMMAND_UI(ID_DISTRIBUTION_BROWSE, OnUpdateFileSave)
ON_UPDATE_COMMAND_UI(ID_GAME_RUN_LAST_DISTRIBUTION, OnUpdateFileSave)
ON_UPDATE_COMMAND_UI(ID_GAME_RUN_SERVER_CLIENT, OnUpdateFileSave)
ON_COMMAND(ID_DISTRIBUTION_BROWSE, OnGameRunSpecificDistribution)
////{{AFX_MSG_MAP(ClientEngine)

ON_BN_CLICKED(IDC_BUTTON_QUIT, OnButtonQuit)
ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
ON_BN_CLICKED(IDC_BUTTON_MENU, OnButtonMenu)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN1, OnTextureFileOpen1)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN2, OnTextureFileOpen2)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN3, OnTextureFileOpen3)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN4, OnTextureFileOpen4)
ON_BN_CLICKED(IDC_BUTTON_TREETEST, OnButtonTreetest)
ON_BN_CLICKED(IDC_BUTTON_OBJECTLOAD, OnButtonObjectload)
ON_BN_CLICKED(IDC_BUTTON_OBJECTDELETE, OnButtonObjectdelete)
ON_BN_CLICKED(IDC_BUTTON_SWITCH_OBJECT, OnButtonSwitchObject)
ON_BN_CLICKED(IDC_BUTTON_BKEDITORSWTCH, OnBUTTONBKEDITORswtch)
ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
ON_BN_CLICKED(IDC_BUTTON_ADDNEWSTATIC_BK, OnButtonAddnewstaticBk)
ON_BN_CLICKED(IDC_BUTTON_CONTROLS, OnButtonControls)
ON_BN_CLICKED(IDC_BUTTON_TEST_EFFECT, OnButtonTestEffect)
ON_BN_CLICKED(IDC_BUTTON_STOP_EFFECT, OnButtonStopEffect)
ON_BN_CLICKED(IDC_BUTTON_MOVIES, OnButtonMovies)
ON_BN_CLICKED(IDC_BUTTON_TESTMOVIE, OnButtonTestmovie)
ON_BN_CLICKED(IDC_BTN_ENTITY_CREATENEW, OnBtnEntityCreatenew)
ON_BN_CLICKED(IDC_BTN_ENTITY_ACTIVATE, OnBtnEntityActivate)
ON_BN_CLICKED(IDC_BUTTON_SW_ENTITIES, OnButtonSwEntities)
ON_BN_CLICKED(IDC_BUTTON_TESTFULLSCREEN, OnButtonTestfullscreen)
ON_BN_CLICKED(IDC_BTN_VIEWWIREFRAME, OnBtnViewwireframe)
ON_BN_CLICKED(IDC_BUTTON_VIEWSMOOTHSHADED, OnButtonViewsmoothshaded)
ON_BN_CLICKED(IDC_BTN_ENTITY_MOVEMENT, OnBtnEntityMovement)
ON_BN_CLICKED(IDC_BTN_ENTITY_DEACTIVATE, OnBtnEntityDeactivate)
ON_BN_CLICKED(IDC_BTN_DELETE_ENTITY, OnBtnDeleteEntity)
ON_BN_CLICKED(IDC_BTN_NEWALL, OnBtnNewall)
ON_BN_CLICKED(IDB_BTN_CameraViewportsSw, OnBTNCameraViewportsSw)
ON_BN_CLICKED(IDB_CAMERA_CreateNewCamera, OnCAMERACreateNewCamera)
ON_BN_CLICKED(IDC_BTN_CREATENEWVIEWPORT, OnBTNCreateNewViewport)
ON_BN_CLICKED(IDC_BTN_CAMERAVPORTACTIVATE, OnBTNCameraVportActivate)
ON_BN_CLICKED(IDC_BTN_VPORTDEACTIVATE, OnBTNvportDeactivate)

ON_BN_CLICKED(IDC_BTN_ADD_RUNTIME_CAMERA, OnBtnAddRuntimeCamera)
ON_BN_CLICKED(IDC_BTN_REMOVE_RUNTIME_CAMERA, OnBtnRemoveRuntimeCamera)

ON_BN_CLICKED(IDC_BTN_ADD_RUNTIME_VIEWPORT, OnBtnAddRuntimeViewport)
ON_BN_CLICKED(IDC_BTN_REMOVE_RUNTIME_VIEWPORT, OnBtnRemoveRuntimeViewport)

ON_BN_CLICKED(IDC_BTN_MOUSECREATE, OnBTNMOUSEcreate)
ON_BN_CLICKED(IDC_BTN_UIMOUSESW, OnBTNUImouseSW)
ON_BN_CLICKED(IDC_BTN_MOUSEFILEBROWSE, OnBTNMOUSEfileBrowse)
ON_BN_CLICKED(IDC_BTN_MOUSEACTIVATE, OnBTNMOUSEactivate)
ON_BN_CLICKED(IDC_BTN_MOUSEDELETE, OnBTNMOUSEdelete)
ON_BN_CLICKED(IDC_BTN_MOUSEDEACTIVATE, OnBTNMOUSEdeactivate)
ON_BN_CLICKED(IDC_BTN_2DINTERFACESW, OnBTN2DINTERFACEsw)
ON_BN_CLICKED(IDC_BTN_2D_CREATECOLLECTION, OnBTN2DCreateCollection)
ON_BN_CLICKED(IDC_BTN_2D_ADDUNIT, OnBTN2DADDunit)
ON_BN_CLICKED(IDC_BTN_2D_ACTIVATE, OnBtn2dActivate)
ON_BN_CLICKED(IDC_BTN_2D_DEACTIVATE, OnBtn2dDeactivate)
ON_BN_CLICKED(IDC_BTN_2D_BROWSEFILEUP, OnBTN2DBrowseFileUp)
ON_BN_CLICKED(IDC_BTN_2D_BROWSEFILEDOWN, OnBTN2DBrowseFileDown)
ON_BN_CLICKED(IDC_BTN_2DGETSELECTEDINFO, OnBTN2DgetSelectedInfo)
ON_BN_CLICKED(IDC_BTN_2D_APPLYMODIFICATIONS, OnBTN2DApplyModifications)
ON_BN_CLICKED(IDC_BTN_2D_DELETECOLLECTION, OnBTN2DDeleteCollection)
ON_BN_CLICKED(IDC_BTN_2D_DELETEUNIT, OnBTN2DDeleteUnit)
ON_BN_CLICKED(IDC_BUTTON_ENVIORMENTSW, OnBUTTONEnviormentSw)
ON_BN_CLICKED(IDC_BTN_ENV_APPLYCHANGES, OnBTNENVApplyChanges)

ON_BN_CLICKED(IDC_BTN_OBJECT_MATERIAL, OnBTNObjectMaterial)
ON_BN_CLICKED(IDC_BUTTON_TEXT_ADDTEXTCOLLECTION, OnBUTTONtextAddTextCollection)
ON_BN_CLICKED(IDC_BTN_TEXTFONTSW, OnBTNtextFontSW)
ON_BN_CLICKED(IDC_BUTTON_TEXT_ADDTEXTOBJECT, OnBUTTONtextAddTextObject)
ON_BN_CLICKED(IDC_BTN_TEXT_ACTIVATE, OnBTNtextActivate)
ON_BN_CLICKED(IDC_BTN_TEXT_DEACTIVATE, OnBTNtextDeactivate)
ON_BN_CLICKED(IDC_BUTTON_TEXT_TEXTCOLOR, OnBUTTONtextTextColor)
ON_BN_CLICKED(IDC_BTN_TEXT_APPLYCHANGES, OnBTNtextApplyChanges)
ON_BN_CLICKED(IDC_BTN_TEXT_GETCURRENTINFO, OnBTNtextGetCurrentInfo)
ON_BN_CLICKED(IDC_BTN_TEXT_APPLYFONT, OnBTNtextApplyFont)
ON_BN_CLICKED(IDC_BUTTON_TEXT_ADDFONT, OnBUTTONtextAddFont)
ON_BN_CLICKED(IDC_BTN_TEXT_APPLYSIZECHANGE, OnBTNtextApplySizeChange)
ON_BN_CLICKED(IDC_BUTTON_TEXT_DELETEFONT, OnBUTTONtextDeleteFont)
ON_BN_CLICKED(IDC_BUTTON_TEXT_DELETETEXTCOLLECTION, OnBUTTONtextDeleteTextCollection)
ON_BN_CLICKED(IDC_BUTTON_TEXT_DELETETEXTOBJECT, OnBUTTONtextDeleteTextObject)
ON_BN_CLICKED(IDC_BTN_TEST_SAVE, OnBTNtestSave)
ON_BN_CLICKED(IDC_BTN_TEST_LOAD, OnBTNtestLoad)
ON_BN_CLICKED(IDC_BTN_CAMVPORT_SAVE, OnBTNcamVportSave)
ON_BN_CLICKED(IDC_BTN_CAMVPORT_LOAD, OnBTNcamVportLoad)
ON_BN_CLICKED(IDC_BTN_DELETECAMERA, OnBTNDeleteCamera)
ON_BN_CLICKED(IDC_BTN_DELETEVIEWPORT, OnBTNDeleteViewport)
ON_BN_CLICKED(IDC_BTN_VIEWPORTSAPPLYCHNGS, OnBTNUpdateViewport)
ON_BN_CLICKED(IDC_BTN_UPDATE_RUNTIME_VIEWPORT, OnBTNUpdateRuntimeViewport)
ON_BN_CLICKED(IDC_BTN_2D_SAVECONFIG, OnBTN2DSaveConfig)
ON_BN_CLICKED(IDC_BTN_2D_LOADCONFIG, OnBTN2DLoadConfig)
ON_BN_CLICKED(IDC_BTN_EXPLOSIONDBSW, OnBTNExplosionDBSW)
ON_BN_CLICKED(IDC_BTN_EXP_ADDEXPLOSION, OnBTNEXPAddExplosion)
ON_BN_CLICKED(IDC_BTN_EXP_LOADEXPLOSIONS, OnBTNEXPLoadExplosions)
ON_BN_CLICKED(IDC_BTN_EXP_SAVEEXPLOSIONS, OnBTNEXPSaveExplosions)
ON_BN_CLICKED(IDC_BTN_EXP_DELETEEXPLOSION, OnBTNEXPDeleteExplosion)
ON_BN_CLICKED(IDC_BTN_CAMERAADVANCED, OnBTNCameraAdvanced)
ON_BN_CLICKED(IDC_BTN_MOVIE_ADDMOVIE, OnBTNMovieAddMovie)
ON_BN_CLICKED(IDC_BTN_MOVIE_TESTMOVIE, OnBTNMovieTestMovie)
ON_BN_CLICKED(IDC_BTN_MOVIE_EDITMOVIE, OnBTNMovieEditMovie)
ON_BN_CLICKED(IDC_BTN_MOVIE_DELETEMOVIE, OnBTNMovieDeleteMovie)
ON_BN_CLICKED(IDC_BTN_MOVIESSAVEDB, OnBTNMoviesSaveDB)
ON_BN_CLICKED(IDC_BTN_MOVIESLOADDB, OnBTNMoviesLoadDB)
ON_BN_CLICKED(IDC_BTN_MOVIE_STOPMOVIES, OnBTNMovieStopMovies)
ON_BN_CLICKED(IDC_BTN_CONTROLADDOBJECT, OnBTNControlAddObject)
ON_BN_CLICKED(IDC_BTN_CONTROLADDEVENT, OnBTNControlAddEvent)
ON_BN_CLICKED(IDC_BTN_CONTROLCFGSAVE, OnBTNControlCfgSave)
ON_BN_CLICKED(IDC_BTN_CONTROLCFGLOAD, OnBTNControlCfgLoad)
ON_BN_CLICKED(IDC_BTN_CONTROLDELETEOBJECT, OnBTNControlDeleteObject)
ON_BN_CLICKED(IDC_BTN_CONTROLDELETEEVENT, OnBTNControlDeleteEvent)
ON_LBN_SELCHANGE(IDC_LIST_CONTROLDB, OnSelchangeLISTControlDB)
ON_BN_CLICKED(IDC_BTN_CONTROLEDITOBJECT, OnBTNControlEditObject)
ON_BN_CLICKED(IDC_BTN_CONTROLEDITEVENT, OnBTNControlEditEvent)
ON_BN_CLICKED(IDC_BTN_OBJECTARRAYADD, OnBTNObjectArrayAdd)
ON_BN_CLICKED(IDC_BTN_UI_BASEADVANCED, OnBTNUIBaseAdvanced)
ON_BN_CLICKED(IDC_BTN_ENTITY_PROPERTIES, OnBTNENTITYProperties)
ON_BN_CLICKED(IDC_BTN_OBJECT_PROPERTIES, OnBTNOBJECTProperties)
ON_BN_CLICKED(IDC_BTN_OBJ_DELETELIGHT, OnBTNOBJDeleteLight)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDLIGHT, OnBtnObjAddlight)
ON_BN_CLICKED(IDC_BTN_ENT_MERGEDATA, OnBTNENTMergeData)
ON_BN_CLICKED(IDC_BTN_UI_MUSIC, OnBTNUIMusic)
ON_BN_CLICKED(IDC_BTN_MUSIC_ASSIGNTRACK, OnBTNMusicAssignTrack)
ON_BN_CLICKED(IDC_BTN_MUSIC_ASSIGNWAV, OnBTNMusicAssignWav)
ON_BN_CLICKED(IDC_BTN_MUSIC_UNASSIGNTRACK, OnBTNMusicUnassignTrack)
ON_BN_CLICKED(IDC_BTN_MUSIC_UNASSIGNWAV, OnBTNMusicUnassignWav)
ON_BN_CLICKED(IDC_BTN_MUSICBROWSEWAV, OnBTNMusicBrowseWav)
ON_BN_CLICKED(IDC_BTN_UI_TEXTUREASSIGN, OnBTNUITextureAssign)
ON_BN_CLICKED(IDC_BTN_UI_MISSILEDATABASE, OnBTNUIMissileDatabase)
ON_BN_CLICKED(IDC_BTN_MIS_ADDMISSILE, OnBTNMISAddMissile)
ON_BN_CLICKED(IDC_BTN_MIS_DELETEMISSILE, OnBTNMISDeleteMissile)
ON_BN_CLICKED(IDC_BTN_MIS_SAVE, OnBTNMISsave)
ON_BN_CLICKED(IDC_BTN_MIS_LOAD, OnBTNMISLoad)
ON_BN_CLICKED(IDC_BTN_UI_BULLETHOLE, OnBTNUIBulletHole)
ON_BN_CLICKED(IDC_BTN_BH_ADDHOLE, OnBTNBHAddHole)
ON_BN_CLICKED(IDC_BTN_BH_BROWSE, OnBTNBHBrowse)
ON_BN_CLICKED(IDC_BTN_BH_ALPHATEXTURE, OnBTNBHAlphaTexture)
ON_BN_CLICKED(IDC_BTN_BH_DELETEHOLE, OnBTNBHDeleteHole)
ON_BN_CLICKED(IDC_BTN_BH_LOAD, OnBTNBHload)
ON_BN_CLICKED(IDC_BTN_BH_MATERIAL, OnBTNBHMaterial)
ON_BN_CLICKED(IDC_BTN_BH_SAVE, OnBTNBHSave)
ON_BN_CLICKED(IDC_BTN_ENTITY_SHADOW, OnBTNEntityshadow)
ON_BN_CLICKED(IDC_BTN_OBJ_SOUND, OnBTNOBJSound)
ON_BN_CLICKED(IDC_BTN_OBJ_ALPHATEXTURE, OnBTNOBJAlphaTexture)
ON_BN_CLICKED(IDC_BTN_TCPIP_JOINHOST, OnBTNTCPIPJoinHost)
ON_BN_CLICKED(IDC_BTN_UI_MULTIPLAYER, OnBTNUIMultiplayer)
ON_BN_CLICKED(IDC_BTN_TEX_ADDCOLLECTION, OnBTNTEXAddCollection)
ON_BN_CLICKED(IDC_BTN_TEX_DELETECOLLECTION, OnBTNTEXDeleteCollection)
ON_BN_CLICKED(IDC_BTN_UI_ANIMTEXTUREDB, OnBTNUIAnimTextureDB)
ON_BN_CLICKED(IDC_BTN_TEX_LOADCOLLECTION, OnBTNTEXLoadCollection)
ON_BN_CLICKED(IDC_BTN_TEX_SAVECOLLECTION, OnBTNTEXSaveCollection)
ON_BN_CLICKED(IDC_BTN_ENT_LOADDB, OnBTNENTLoadDB)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEDB, OnBTNENTSaveDB)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEDB_SMOOTHNORMALS, OnBTNENTSaveDBSmoothNormals)
ON_BN_CLICKED(IDC_BUTTON_SAVE_ANM, OnButtonSaveANM)
ON_BN_CLICKED(IDC_BUTTON_SAVE_ANMU, OnButtonSaveANMU)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEDB_EXPANDED, OnBTNENTSaveDBExpanded)
ON_BN_CLICKED(IDC_BTN_ENT_REMOVE_LOD, OnBTNENTRemoveLOD)
ON_BN_CLICKED(IDC_BTN_ENT_LOADDB_EXPANDED, OnBTNENTLoadDBExpanded)
ON_BN_CLICKED(IDC_BTN_UI_SCRIPT, OnBtnUiScript)
ON_BN_CLICKED(IDC_BTN_SCR_ADDSCRIPT, OnBTNSCRAddScript)
ON_BN_CLICKED(IDC_BTN_SCR_DELETEEVENT, OnBTNSCRDeleteEvent)
ON_BN_CLICKED(IDC_BTN_SCR_BUILD, OnBTNSCRBuild)
ON_BN_CLICKED(IDC_BTN_SCR_EDITEVENT, OnBTNSCREditEvent)
ON_BN_CLICKED(IDC_BTN_OBJ_EDITLIGHT, OnBTNOBJEditLight)
ON_BN_CLICKED(IDC_BTN_ENT_LOADENTITY, OnBTNENTLoadEntity)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEENTITY, OnBTNENTSaveEntity)
ON_BN_CLICKED(IDC_BTN_MS_SAVEDB, OnBTNMSSaveDB)
ON_BN_CLICKED(IDC_BTN_MS_LOADDB, OnBTNMSLoadDB)
ON_BN_CLICKED(IDC_BTN_TXTDB_ADD, OnBtnTxtdbAdd)
ON_BN_CLICKED(IDC_BTN_TXTDB_DELETE, OnBtnTxtdbDelete)
ON_BN_CLICKED(IDC_BTN_TXTDB_EDIT, OnBtnTxtdbEdit)
ON_BN_CLICKED(IDC_BTN_TXTDB_REINIT_ALL, OnBtnTxtdbReinitAll)
ON_BN_CLICKED(IDC_BTN_OBJ_CLEARTEXCOORDS, OnBTNOBJClearTexCoords)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDTEXCOORDS, OnBTNOBJAddTexCoords)
ON_BN_CLICKED(IDC_BTN_SCR_CLEAR_SCENE, OnBTNSCRClearScene)
ON_BN_CLICKED(IDC_BTN_NT_ADVANCED, OnBTNNTAdvanced)
ON_BN_CLICKED(IDC_BTN_NT_BUILDTXTRECORD, OnBTNNTBuildTxtRecord)
ON_BN_CLICKED(IDC_BTN_UI_HUDINTERFACE, OnBTNUIHudInterface)
ON_BN_CLICKED(IDC_BTN_HUD_ADDDISPLAYUNIT, OnBTNHUDAddDisplayUnit)
ON_BN_CLICKED(IDC_BTN_HUD_ADDNEWHUD, OnBTNHUDAddNewHud)
ON_BN_CLICKED(IDC_BTN_HUD_SAVEDB, OnBTNHUDSaveDB)
ON_BN_CLICKED(IDC_BTN_HUD_LOADDB, OnBTNHUDLoadDB)
ON_LBN_SELCHANGE(IDC_LIST_HUD_DB, OnSelchangeListHudDb)
ON_BN_CLICKED(IDC_BTN_HUD_DELETEHUD, OnBTNHUDDeleteHud)
ON_BN_CLICKED(IDC_BTN_HUD_DELETEDISPLAYUNIT, OnBTNHUDDeleteDisplayUnit)
ON_BN_CLICKED(IDC_BTN_HUD_REPLACEUNIT, OnBTNHUDReplaceUnit)
ON_BN_CLICKED(IDC_BTN_ENT_UPDATE, OnBTNENTUpdate)
ON_LBN_SELCHANGE(IDC_LIST_TXTDB_DATABASE, OnSelchangeLISTTXTDBDatabase)
ON_BN_CLICKED(IDC_BTN_UI_PARTICLESYS, OnBTNUIParticleSys)
ON_BN_CLICKED(IDC_BTN_PART_EDITSYSTEM, OnBTNPARTEditSystem)
ON_BN_CLICKED(IDC_BTN_PART_LOADDATABASE, OnBTNPARTLoadDatabase)
ON_BN_CLICKED(IDC_BTN_UI_SPAWNGEN, OnBTNUISpawnGen)
ON_BN_CLICKED(IDC_BTN_SPAWN_ADDGEN, OnBTNSpawnAddGen)
ON_BN_CLICKED(IDC_BTN_SPAWN_DELETEGEN, OnBTNSpawnDeleteGen)
ON_BN_CLICKED(IDC_BTN_SPAWN_EDITGEN, OnBTNSpawnEditGen)
ON_BN_CLICKED(IDC_BTN_OBJECT_BUILDCOLLISIONMAP, OnBTNObjectBuildCollisionMap)
ON_NOTIFY(TVN_SELCHANGED, IDC_OBJECT_TREE, OnSelchangedObjectTree)
ON_BN_CLICKED(IDC_BTN_OBJECTS_ADDGROUP, OnBTNObjectsAddGroup)
ON_BN_CLICKED(IDC_BTN_OBJECTS_DELETEGROUP, OnBTNObjectsDeleteGroup)
ON_BN_CLICKED(IDC_BTN_OBJECTSEDITGROUP, OnBTNObjectsEditGroup)
ON_LBN_SELCHANGE(IDC_LIST_OBJECTS_GROUPLIST2BOX, OnSelchangeLISTObjectsGroupList2Box)
ON_LBN_DBLCLK(IDC_LIST_OBJECTS_GROUPLIST2BOX, OnDblclkLISTObjectsGroupList2Box)
ON_BN_CLICKED(IDC_BTN_GROUP_MODIFIER, OnBTNGroupModifier)
ON_NOTIFY(NM_DBLCLK, IDC_OBJECT_TREE, OnDblclkObjectTree)
ON_BN_CLICKED(IDC_BTN_UI_MERGE, OnBTNUIMerge)
ON_BN_CLICKED(IDC_BTN_TCP_DISCONNECTPLAYER, OnBTNTCPDisconnectPlayer)
ON_BN_CLICKED(IDC_BTN_TCP_SERVERLOAD, OnBTNTCPServerLoad)
ON_BN_CLICKED(IDC_BTN_TCP_SERVERSAVE, OnBTNTCPServerSave)
ON_BN_CLICKED(IDC_BTN_OPTIMIZESTATES, OnBTNOptimizeStates)
ON_BN_CLICKED(IDC_BTN_TCP_NEWACCOUNT, OnBTNTCPNewAccount)
ON_BN_CLICKED(IDC_BTN_TCP_DELETEACCOUNT, OnBTNTCPDeleteAccount)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDTRIGGER, OnBTNOBJAddTrigger)
ON_BN_CLICKED(IDC_BTN_OBJ_UPDATEOBJECT, OnBTNOBJUpdateObject)
ON_BN_CLICKED(IDC_CHECK_TCP_REGISTERED, OnCHECKTCPRegistered)
ON_LBN_SELCHANGE(IDC_LIST_TCP_IP_CURRENTPLAYERS, OnSelchangeLISTTCPIPCurrentPlayers)
ON_BN_CLICKED(IDC_BTN_TX_SAVETEXTUREBANK_STATIC, OnBTNTXSaveTextureBankStatic)
ON_BN_CLICKED(IDC_BTN_TX_SAVETEXTUREBANK_DYNAMIC, OnBTNTXSaveTextureBankDynamic)
ON_BN_CLICKED(IDC_BTN_TX_LOADTEXTUREBANK, OnBTNTXLoadTextureBank)
ON_BN_CLICKED(IDC_BTN_OBJ_GETSIGNIFICANT, OnBTNOBJGetSignificant)
ON_BN_CLICKED(IDC_BTN_OBJ_TEXTURESETMACRO, OnBTNOBJTextureSetMacro)
ON_BN_CLICKED(IDC_BTN_AI_CREATENEW, OnBTNAICreateNew)
ON_BN_CLICKED(IDC_BTN_UI_AI, OnBtnUiAi)
ON_BN_CLICKED(IDC_BTN_AI_DELETE, OnBTNAIDelete)
ON_BN_CLICKED(IDC_BTN_OBJ_LODADD, OnBtnObjLodadd)
ON_BN_CLICKED(IDC_BTN_OBJ_REMOVELODLEVEL, OnBTNOBJRemoveLODLevel)
ON_BN_CLICKED(IDC_BTN_OBJ_EDITLOD, OnBTNOBJEditLOD)
ON_BN_CLICKED(IDC_BTN_ENTITY_ANIMATIONS, OnBTNENTITYAnimations)
ON_BN_CLICKED(IDC_BTN_ENT_APPENDCUSTOMDB, OnBTNENTAppendCustomDB)
ON_LBN_SELCHANGE(IDC_LIST_ENTITY_RUNTIMELIST, OnSelchangeLISTENTITYRuntimeList)
ON_BN_CLICKED(IDC_BTN_UI_HALOEFFECTS, OnBTNUIHaloEffects)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_ADDHALO, OnBTNAddHalo)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_DELETEHALO, OnBTNDeleteHalo)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_EDITHALO, OnBTNEditHalo)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_LOADDATABASE, OnBTNHaloLoad)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_SAVEDATABASE, OnBTNHaloSave)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_ADDLENSFLARE, OnBTNAddLensFlare)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_EDITLENSFLARE, OnBTNEditLensFlare)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_DELETELENSFLARE, OnBTNDeleteLensFlare)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_GENERATEMESH, OnBTNGenerateMesh)
ON_BN_CLICKED(IDC_BTN_ENT_LODSYSTEMS, OnBTNENTLODSystems)
ON_BN_CLICKED(IDC_BTN_ENT_DEL_LOD, OnBTNENTDelLOD)
ON_BN_CLICKED(IDC_BTN_ENT_UPDATE_VISUALS, OnBTNENTUpdateVisuals)
ON_BN_CLICKED(IDC_BTN_ENT_RESOURCEBASE, OnBTNENTResourceBase)
ON_BN_CLICKED(IDC_BTN_ENT_VIEWRUNTIMESTATS, OnBTNENTViewRuntimeStats)
ON_BN_CLICKED(IDC_BTN_ENT_VIEWRUNTIMERESOURCES, OnBTNENTViewRuntimeResources)
ON_LBN_SELCHANGE(IDC_LIST_MIS_MISSILEDATABASE, OnSelchangeLISTMISmissileDataBase)
ON_BN_CLICKED(IDC_BTN_MIS_REPLACEVALUES, OnBTNMISReplaceValues)
ON_BN_CLICKED(IDC_BTN_MIS_EDITVISUAL, OnBTNMISEditVisual)
ON_BN_CLICKED(IDC_BTN_MIS_ADDBILLBOARDMISSILE, OnBTNMISAddBillboardMissile)
ON_BN_CLICKED(IDC_BTN_MIS_EDITDYNAMICS, OnBTNMISEditDynamics)
ON_BN_CLICKED(IDC_BTN_ENT_EDITSTARTINVEN, OnBTNENTEditStartInven)
ON_CBN_DROPDOWN(IDC_CMBO_CGTEX_FONTTABLETEXTURE, OnDropdownCMBOCGTEXFontTableTexture)
ON_BN_CLICKED(IDC_BTN_CGTEX_ADD, OnBTNCGTEXAdd)
ON_BN_CLICKED(IDC_BTN_CGTEX_DELETE, OnBTNCGTEXDelete)
ON_BN_CLICKED(IDC_BTN_AI_ADDEVENT, OnBTNAIAddEvent)
ON_BN_CLICKED(IDC_BTN_AI_ADDSCRIPT, OnBTNAIAddScript)
ON_BN_CLICKED(IDC_BTN_AI_EDITSCRIPT, OnBTNAIEditScript)
ON_BN_CLICKED(IDC_BTN_AI_DELETESCRIPT, OnBTNAIDeleteScript)
ON_BN_CLICKED(IDC_BTN_AI_EDITEVENT, OnBTNAIEditEvent)
ON_BN_CLICKED(IDC_BTN_AI_DELETEEVENT, OnBTNAIDeleteEvent)
ON_BN_CLICKED(IDC_BTN_AI_ADDTREE, OnBTNAIAddTree)
ON_BN_CLICKED(IDC_BTN_AI_DELETETREE, OnBTNAIDeleteTree)
ON_BN_CLICKED(IDC_BTN_AI_EDIT, OnBTNAIEdit)
ON_BN_CLICKED(IDC_BTN_AI_SAVEAIDATA, OnBTNAISaveAiData)
ON_BN_CLICKED(IDC_BTN_AI_LOADAIDATA, OnBTNAILoadAiData)
ON_LBN_SELCHANGE(IDC_LIST_AI_SCRIPTS, OnSelchangeLISTAIScripts)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEAPPENDAGES, OnBTNENTSaveAppendages)
ON_BN_CLICKED(IDC_BTN_ENT_LOADAPPENDAGES, OnBTNENTLoadAppendages)
ON_BN_CLICKED(IDC_BTN_ENT_MOUNT, OnBtnEntMount)
ON_BN_CLICKED(IDC_BTN_ENT_DEMOUNT, OnBtnEntDemount)
ON_BN_CLICKED(IDC_BTN_EXP_EDITEXPLOSIONOBJ, OnBTNEXPEditExplosionObj)
ON_EN_CHANGE(IDC_EDIT_TCP_IPADRESS, OnChangeEDITTCPIPadress)
ON_EN_CHANGE(IDC_TCP_USERNAME, OnChangeTCPuserName)
ON_EN_CHANGE(IDC_TCP_PASSWORD, OnChangeTCPpassword)
ON_BN_CLICKED(IDC_BTN_SYSTEMGOINGDOWN, OnBTNSystemGoingDown)
ON_BN_CLICKED(IDC_BTN_ENT_ADDITEM, OnBTNENTAddItem)
ON_BN_CLICKED(IDC_BTN_ENT_ITEMTEST, OnBTNENTItemTest)
ON_BN_CLICKED(IDC_BTN_ENT_DELETEITEM, OnBTNENTDeleteItem)
ON_BN_CLICKED(IDC_BTN_ENT_DISARMITEM, OnBTNENTDisarmItem)
ON_BN_CLICKED(IDC_BTN_ENT_EDITITEM, OnBTNENTEditItem)
ON_BN_CLICKED(IDC_BTN_OBJ_CLONE, OnBTNOBJClone)
ON_BN_CLICKED(IDC_BTN_OBJ_TRANSFORMATION, OnBTNOBJTransformation)
ON_BN_CLICKED(IDC_BTN_OBJ_BOUNDINGGROUPSET, OnBTNOBJBoundingGroupSet)
ON_BN_CLICKED(IDC_BTN_OBJ_LIBRARYACCESS, OnBTNOBJLibraryAccess)
ON_BN_CLICKED(IDC_BTN_ENV_TIMESYSTEM, OnBTNENVTimeSystem)
ON_BN_CLICKED(IDC_BTN_MIS_ADDSKELETALMISSILE, OnBTNMISAddSkeletalMissile)
ON_BN_CLICKED(IDC_BTN_MIS_ADDLASER, OnBTNMISAddLaser)
ON_BN_CLICKED(IDC_BTN_AI_EDITAITREE, OnBTNAIEditAITree)
ON_BN_CLICKED(IDC_BTN_TCP_SUPPORTEDWORLDS, OnBTNTCPSupportedWorlds)
ON_BN_CLICKED(IDC_BTN_ENT_EDITDEATHCFG, OnBTNENTEditDeathCfg)
ON_BN_CLICKED(IDC_BTN_TCP_VIEWACCOUNT, OnBTNTCPViewAccount)
ON_BN_CLICKED(IDC_CHECK_TXB_BITMAPLOADOVERRIDE, OnCHECKTXBBitmapLoadOverride)
ON_BN_CLICKED(IDC_BTN_UI_SKILLBUILDER, OnBTNUISkillBuilder)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDNODE, OnBTNOBJAddNode)
ON_BN_CLICKED(IDC_BTN_OBJ_REFERENCELIBRARY, OnBTNOBJReferenceLibrary)
ON_BN_CLICKED(IDC_BTN_UI_SOUNDDB, OnBTNUISoundDB)
ON_BN_CLICKED(IDC_BTN_UI_GLINVENTORYDB, OnBTNUIGlInventoryDB)
ON_BN_CLICKED(IDC_BTN_UI_CURRENCYSYS, OnBtnUiCurrencysys)
ON_BN_CLICKED(IDC_BTN_UI_COMMERCEDB, OnBTNUICommerceDB)
ON_BN_CLICKED(IDC_BTN_UI_BANKZONES, OnBTNUIBankZones)
ON_BN_CLICKED(IDC_BTN_UI_SAFEZONES, OnBtnUiSafezones)
ON_BN_CLICKED(IDC_BTN_ENT_UNIFYSLELETALMESH, OnBTNENTUnifySleletalMesh)
ON_BN_CLICKED(IDC_BTN_MIS_DELETELIGHT, OnBTNMISDeleteLight)
ON_BN_CLICKED(IDC_BTN_MIS_LIGHTSYS, OnBTNMISLightSys)
ON_BN_CLICKED(IDC_BTN_UI_STATDB, OnBtnUiStatdb)
ON_BN_CLICKED(IDC_BTN_TCP_ITEMGENERATORDB, OnBTNTCPItemGeneratorDB)
ON_BN_CLICKED(IDC_BTN_PATCHSYS, OnBtnPatchsys)
ON_BN_CLICKED(IDC_BTN_TCP_UPDATECURRENTONLINEPLAYERS, OnBTNTCPUpdateCurrentOnlinePlayers)
ON_BN_CLICKED(IDC_BTN_ENT_SCALEENTITY, OnBTNENTScaleEntity)
ON_BN_CLICKED(IDC_BTN_ENT_SCALE_RUNTIME_ENTITY, OnBTNENTScaleRuntimeEntity)
ON_BN_CLICKED(IDC_BTN_TCP_UPDATEALLACOUNTS, OnBTNTCPUpdateAllAcounts)
ON_BN_CLICKED(IDC_BTN_ENT_REPLACEENTITYWSIE, OnBTNENTReplaceEntityWSIE)
ON_BN_CLICKED(IDC_BTN_TCP_IPBLOCKING, OnBTNTCPIPBlocking)
ON_BN_CLICKED(IDC_BTN_UI_WATERZONES, OnBtnUiWaterzones)
ON_BN_CLICKED(IDC_BTN_UI_PORTALZONESDB, OnBtnUiPortalzonesdb)
ON_BN_CLICKED(IDC_BTN_UI_ELEMENTDB, OnBTNUIElementDB)
ON_BN_CLICKED(IDC_BTN_TCP_UPGRADEWORM, OnBTNTCPUpgradeWorm)
ON_BN_CLICKED(IDC_BTN_EXP_SAVEUNIT, OnBTNEXPSaveUnit)
ON_BN_CLICKED(IDC_BTN_EXP_LOADUNIT, OnBTNEXPLoadUnit)
ON_BN_CLICKED(IDC_BTN_TCP_DISCONNECTALL, OnBTNTCPDisconnectAll)
ON_BN_CLICKED(IDC_BTN_TCP_PRINTREPORTS, OnBTNTCPPrintReports)
ON_BN_CLICKED(IDC_BTN_AI_LOADAIUNIT, OnBTNAILoadAiUnit)
ON_BN_CLICKED(IDC_BTN_AI_SAVEAIUNIT, OnBTNAISaveAiUnit)
ON_BN_CLICKED(IDC_BTN_AI_IMPORTCOLLISIONSYS, OnBTNAIImportCollisionSys)
ON_BN_CLICKED(IDC_BTN_AI_DELETESYS, OnBTNAIDeleteSys)
ON_BN_CLICKED(IDC_BTN_AI_EXPORTCURRENTCOL, OnBTNAIExportCurrentCol)
ON_BN_CLICKED(IDC_BTN_AI_SAVECOLDB, OnBTNAISaveColDB)
ON_BN_CLICKED(IDC_BTN_AI_LOADCOLDB, OnBTNAILoadColDB)
ON_BN_CLICKED(IDC_BTN_MIS_SAVEUNIT, OnBTNMISSaveUnit)
ON_BN_CLICKED(IDC_BTN_MIS_LOADUNIT, OnBTNMISLoadUnit)
ON_BN_CLICKED(IDC_BTN_SPN_EXPORTSYS, OnBTNSPNExportSys)
ON_BN_CLICKED(IDC_BTN_SPN_IMPORTSYS, OnBTNSPNImportSys)
ON_BN_CLICKED(IDC_BTN_AI_INTEGRITY_CHECK, OnBTNAIIntegrityCheck)
ON_BN_CLICKED(IDC_BTN_TCP_SEARCHFORACCOUNT, OnBTNTCPSearchForAccount)
ON_BN_CLICKED(IDC_BTN_TCP_RESTOREBACKUP, OnBTNTCPRestoreBackup)
ON_BN_CLICKED(IDC_BTN_TXT_SYMBOLDB, OnBTNTXTSymbolDB)
ON_BN_CLICKED(IDC_CHECK_EDITORTXTMODE, OnCHECKEditorTxtMode)
ON_BN_CLICKED(IDC_BTN_ENT_MASSFORMATNAMES, OnBTNENTMassFormatNames)
ON_BN_CLICKED(IDC_CHK_OBJ_ENABLEPLAEMENTHOTKEY, OnCHKOBJEnablePlaementHotKey)
ON_BN_CLICKED(IDC_BTN_OBJ_REPORTCURRENDERCACHE, OnBTNOBJReportCurRenderCache)
ON_BN_CLICKED(IDC_BTN_UI_ICONDBACCESS, OnBTNUIIconDBACCESS)
ON_BN_CLICKED(IDC_BTN_TCP_SNAPSHOTPACKETQUE, OnBTNTCPSnapShotPacketQue)
ON_BN_CLICKED(IDC_BTN_TCP_GENERATELOGBASEDONTIME, OnBTNTCPGenerateLogBasedOnTime)
ON_BN_CLICKED(IDC_BTN_UI_HOUSINGSYS, OnBTNUIHousingSys)
ON_BN_CLICKED(IDC_BTN_UI_HSZONEDB, OnBTNUIHszoneDB)
ON_BN_CLICKED(IDC_BTN_CHK_SHOWSPEED, OnBTNChkshowSpeed)
ON_WM_TIMER()
ON_BN_CLICKED(IDC_BTN_AI_NEWTREE, OnBTNAInewTree)
ON_BN_CLICKED(IDC_BTN_TCP_EXPORTACCOUNT, OnBTNTCPExportAccount)
ON_BN_CLICKED(IDC_BTN_TCP_IMPORTACCOUNT, OnBTNTCPImportAccount)
ON_BN_CLICKED(IDC_BTN_TCP_GENERATECLANDATABASE, OnBTNTCPGenerateClanDatabase)
ON_BN_CLICKED(IDC_BTN_TCP_CLEARINACTIVE, OnBTNTCPClearInactive)
ON_BN_CLICKED(IDC_BTN_UI_AIRAINSYS, OnBTNUIAIRainSys)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEITEMDB, OnBTNENTsaveItemDB)
ON_BN_CLICKED(IDC_BTN_ENT_LOADITEMDB, OnBTNENTLoadItemDB)
ON_BN_CLICKED(IDC_BTN_UI_HALOEFFECTS, OnBTNUIHaloEffects)
ON_BN_CLICKED(IDC_BTN_AI_INTEGRITY_CHECK, OnBTNAIIntegrityCheck)
ON_BN_CLICKED(IDC_BTN_SPN_APPENDSYS, OnBTNSPNAppendSys)
ON_BN_CLICKED(IDC_BTN_UI_TESTASSIGN, OnBTNTestAssign)
ON_BN_CLICKED(IDC_BTN_COMMCNTRLLIBBTNA, OnBTNCommCntrlLibBtnA)
ON_BN_CLICKED(IDD_BTN_UI_WORLDRULES, OnBTNWorldRule)
ON_BN_CLICKED(IDD_BTN_UI_ARENAMANAGER, OnBTNArenaManager)
ON_BN_CLICKED(IDC_BTN_OBJ_SELECT, OnObjectSelect)
ON_BN_CLICKED(IDC_BTN_OBJ_MOVE, OnObjectMove)
ON_BN_CLICKED(IDC_BTN_OBJ_ROTATE, OnObjectRotate)
ON_BN_CLICKED(IDC_BTN_OBJ_SCALE, OnObjectScale)
ON_LBN_SELCHANGE(IDC_LIST_VIEWPORTS, OnSelchangeViewports)
ON_LBN_SELCHANGE(IDC_LIST_RUNTIME_VIEWPORTS, OnSelchangeRuntimeViewports)
ON_BN_CLICKED(IDC_BTN_DEL_GUIDS, OnBTNDeleteGuids)
ON_LBN_SELCHANGE(IDC_LIST_EQUIPABLE_ITEMS, OnSelchangeEquipItem)
ON_CBN_SELCHANGE(IDC_COMBO_EQUIPABLE_MESH_SLOT, OnSelchangeEquippedItemMeshSlot)
ON_CBN_DROPDOWN(IDC_COMBO_EQUIPABLE_MESH_SLOT, OnSelchangeEquippedItemMeshSlot)
ON_CBN_SELCHANGE(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, OnSelchangeEquippedItemMesh)
ON_CBN_DROPDOWN(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, OnSelchangeEquippedItemMesh)
ON_CBN_SELCHANGE(IDC_COMBO_EQUIPABLE_MATERIAL, OnSelchangeEquippedItemMaterial)
ON_CBN_DROPDOWN(IDC_COMBO_EQUIPABLE_MATERIAL, OnSelchangeEquippedItemMaterial)

ON_CBN_SELCHANGE(IDC_COMBO_ENTITY_MESH_SLOT, OnSelectionChangeEntityMeshSlot)
ON_CBN_SELCHANGE(IDC_COMBO_ENTITY_MESH_CONFIG, OnSelectionChangeEntityMeshConfig)
ON_CBN_SELCHANGE(IDC_COMBO_ENTITY_MATERIAL_CONFIG, OnSelectionChangeEntityMaterialConfig)
ON_WM_COPYDATA()
////}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif

void CKEPEditView::OnButtonQuit() {
	// FreeAllThenClose();
	PostMessage(WM_CLOSE);
}

void CKEPEditView::OpenToolBar(CDialogBar *barLoc) {
	CloseAllToolDialogs();
	barLoc->ShowWindow(SW_SHOWNORMAL);
	barLoc->SetWindowPos(&wndTop, m_pCABD3DLIB->imediateRenderRECT.right, 0, 1024, 800, SWP_SHOWWINDOW);
}

void CKEPEditView::CloseAllToolDialogs() {
	m_wndDlgBar.ShowWindow(SW_HIDE);
	m_dlgBarObject.ShowWindow(SW_HIDE);
	m_backgroundDlgBar.ShowWindow(SW_HIDE);
	m_controlsDlgBar.ShowWindow(SW_HIDE);
	m_moviesDlgBar.ShowWindow(SW_HIDE);
	m_entityDlgBar.ShowWindow(SW_HIDE);
	m_camVptDlgBar.ShowWindow(SW_HIDE);
	m_2DinterfaceBar.ShowWindow(SW_HIDE);
	m_enviormentBar.ShowWindow(SW_HIDE);
	m_textFontDlgBar.ShowWindow(SW_HIDE);
	m_explosionDlgBar.ShowWindow(SW_HIDE);
	m_musicDlgBar.ShowWindow(SW_HIDE);
	m_missileDlgBar.ShowWindow(SW_HIDE);
	m_bulletHoleDlgBar.ShowWindow(SW_HIDE);
	m_multiPlayer.ShowWindow(SW_HIDE);
	m_animatedTextureDBDlgBar.ShowWindow(SW_HIDE);
	m_scriptDlgBar.ShowWindow(SW_HIDE);
	m_textureDBDlgBar.ShowWindow(SW_HIDE);
	m_hudDBDlgBar.ShowWindow(SW_HIDE);
	m_particleDBDlgBar.ShowWindow(SW_HIDE);
	m_spawnGenDBDlgBar.ShowWindow(SW_HIDE);

	m_aiDBDlgBar.ShowWindow(SW_HIDE);
	m_mouseDlgBar.ShowWindow(SW_HIDE);
}

void CKEPEditView::OnButtonSave() {
	// use gamefiles folder for organization
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();

	// end use gamefiles folder for organization
	static wchar_t BASED_CODE filter[] = L"Zone Files (*.exg)|*.EXG|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EXG", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		BOOL saved = SaveSceneObjects(fileName);
		if (saved) {
			EditorState::GetInstance()->SetZoneDirty(false);
		}
		SetZoneFileName(fileName.GetString());
		m_wndDlgBar.SetDlgItemText(IDC_EDIT_FILENAME, Utf8ToUtf16(GetZoneFileName()).c_str());
	}
}

void CKEPEditView::OnButtonLoad() {
	// use gamefiles folder for organization
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();

	// end use gamefiles folder for organization
	static wchar_t BASED_CODE filter[] = L"Zone Files (*.exg)|*.EXG|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		LoadNewWorld = TRUE;
		NewWorld = fileName;
		SetZoneFileName(fileName.GetString());
		m_wndDlgBar.SetDlgItemText(IDC_EDIT_FILENAME, Utf8ToUtf16(GetZoneFileName()).c_str());
	}
}

void CKEPEditView::OnButtonSwitchObject() {
	OpenToolBar(&m_dlgBarObject);
}

void CKEPEditView::OnButtonMenu() {
	OpenToolBar(&m_wndDlgBar);
}

void CKEPEditView::OnBUTTONBKEDITORswtch() {
	OpenToolBar(&m_backgroundDlgBar);
}

void CKEPEditView::OnButtonControls() {
	OpenToolBar(&m_controlsDlgBar);
}

void CKEPEditView::OnButtonMovies() {
	OpenToolBar(&m_moviesDlgBar);
}

void CKEPEditView::OnButtonSwEntities() {
	OpenToolBar(&m_entityDlgBar);
}

void CKEPEditView::OnBTNCameraViewportsSw() {
	OpenToolBar(&m_camVptDlgBar);
}

void CKEPEditView::OnBTN2DINTERFACEsw() {
#ifndef _ClientOutput
	CShaderLibDlg dlg(m_pxShaderSystem);
	//dlg.m_shaderLibRef = m_pxShaderSystem;
	dlg.m_pd3dDevice = g_pd3dDevice;
	dlg.m_shaderTypeMode = 1;
	dlg.DoModal();
	//m_pxShaderSystem = dlg.m_shaderLibRef;
	m_pxShaderSystem->Compile(g_pd3dDevice);
	m_renderStateObject->m_pxShderSystemRef = m_pxShaderSystem;
#endif
}

void CKEPEditView::OnBUTTONEnviormentSw() {
	OpenToolBar(&m_enviormentBar);
}

void CKEPEditView::OnBTNtextFontSW() {
	OpenToolBar(&m_textFontDlgBar);
}

void CKEPEditView::OnBTNExplosionDBSW() {
	OpenToolBar(&m_explosionDlgBar);
}

void CKEPEditView::OnBTNUIMusic() {
	OpenToolBar(&m_musicDlgBar);
}

void CKEPEditView::OnBTNUIMissileDatabase() {
	OpenToolBar(&m_missileDlgBar);
}

void CKEPEditView::OnBTNUIBulletHole() {
	OpenToolBar(&m_bulletHoleDlgBar);
}

void CKEPEditView::OnBTNUIMultiplayer() {
	OpenToolBar(&m_multiPlayer);
}

void CKEPEditView::OnBTNUIAnimTextureDB() {
	OpenToolBar(&m_animatedTextureDBDlgBar);
}

void CKEPEditView::OnBtnUiScript() {
	OpenToolBar(&m_scriptDlgBar);
}

void CKEPEditView::OnBTNUITextureAssign() {
	OpenToolBar(&m_textureDBDlgBar);
}

void CKEPEditView::OnBTNUIHudInterface() {
#ifndef _ClientOutput
	CMaterialShaderLibDlg dlg(m_cgGlobalShaderDB);
	dlg.GL_textureDataBase = m_pTextureDB;
	//dlg.m_globalShaderRef = m_cgGlobalShaderDB;
	dlg.DoModal();
	//m_cgGlobalShaderDB = dlg.m_globalShaderRef;
#endif
}

void CKEPEditView::OnBTNUIParticleSys() {
	OpenToolBar(&m_particleDBDlgBar);
}

void CKEPEditView::OnBTNUISpawnGen() {
	OpenToolBar(&m_spawnGenDBDlgBar);
}

void CKEPEditView::OnBtnUiAi() {
	OpenToolBar(&m_aiDBDlgBar);
}

void CKEPEditView::OnBTNUIHaloEffects() {
}

void CKEPEditView::OnButtonTreetest() {
}

void CKEPEditView::ProcessTextureLevelChange(int textureIndex) {
}

void CKEPEditView::OnTextureFileOpen1() {
	ProcessTextureLevelChange(0);
}

void CKEPEditView::OnTextureFileOpen2() {
	ProcessTextureLevelChange(1);
}

void CKEPEditView::OnTextureFileOpen3() {
	ProcessTextureLevelChange(2);
}

void CKEPEditView::OnTextureFileOpen4() {
	ProcessTextureLevelChange(3);
}

void CKEPEditView::OnButtonObjectload() {
	// char temp;
	wchar_t bigBuff[20000] = L""; // maximum common dialog buffer size

	// use mapsmodels folder for organization
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();

	// end use mapsmodels folder for organization
	string filter;
	IAssetImporterFactory::Instance()->GetOpenDialogFilter(KEP::KEP_STATIC_MESH, filter);
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT | OFN_FILEMUSTEXIST, Utf8ToUtf16(filter).c_str(), this);
	opendialog.m_ofn.hInstance = AfxGetInstanceHandle();
	opendialog.m_ofn.lpstrFile = bigBuff;
	opendialog.m_ofn.nMaxFile = sizeof(bigBuff);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) { // file chosen
		std::vector<CStringW> astrFileNames;
		POSITION pos = opendialog.GetStartPosition();
		while (pos) {
			CStringW strFileName = opendialog.GetNextPathName(pos);
			astrFileNames.push_back(strFileName);
		}

		for (size_t i = 0; i < astrFileNames.size(); ++i) { // begin file load loop
			CWldObject *retBasisLoc = NULL;
			LoadWorldObject(Utf16ToUtf8(astrFileNames[i]).c_str(), &retBasisLoc, TRUE);
			if (retBasisLoc) // valid
			{ // valid
				AddTextureToDatabase(retBasisLoc->m_meshObject); // apply mapping and add to DB
			}
		}

		// Added to update UI [Yanfeng 09/2008]
		VisualPropertiesInList();
		EditorState::GetInstance()->SetZoneDirty();

	} // end file chosen
}

void CKEPEditView::OnButtonObjectdelete() {
	if (AfxMessageBox(IDS_CONFIRM_DELETEVISUAL, MB_OKCANCEL, 0) != IDOK)
		return;

	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	if (!DeleteWorldObjectByTreeItem(item))
		AfxMessageBox(IDS_MSG_OBJECTNOTFOUND);
}

void CKEPEditView::ObjectTreeViewInlist(int groupNum, int traceNum) {
	map<CStringA, HTREEITEM> virtualNodes; // all virtual nodes: virtual mesh objects for multiple material-based sub-meshes

	if (!m_pWOL) {
		return;
	}

	// First lets create the list of images that will be used as icons
	// within the world object tree.
	enum {
		ICON_NONE = -1,
		ICON_WORLD,
		ICON_REFERENCE,
		ICON_MESHGROUP,
		ICON_NUMBER
	};

	CImageList treeImages;
	const int imageWidth = 16;
	const int imageHeight = 16;
	HANDLE hWorldObjIcon;
	HANDLE hReferenceObjIcon;
	HANDLE hMeshGroupIcon;

	hWorldObjIcon = LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_WORLDOBJECT), IMAGE_ICON, imageWidth, imageHeight, LR_DEFAULTCOLOR);
	hReferenceObjIcon = LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_REFERENCEOBJECT), IMAGE_ICON, imageWidth, imageHeight, LR_DEFAULTCOLOR);
	hMeshGroupIcon = LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_MESHGROUP), IMAGE_ICON, imageWidth, imageHeight, LR_DEFAULTCOLOR);
	m_worldObjectTreeImages.DeleteImageList();
	m_worldObjectTreeImages.Create(imageWidth, imageHeight, ILC_COLOR4, 0, 2);
	m_worldObjectTreeImages.Add(reinterpret_cast<HICON>(hWorldObjIcon));
	m_worldObjectTreeImages.Add(reinterpret_cast<HICON>(hReferenceObjIcon));
	m_worldObjectTreeImages.Add(reinterpret_cast<HICON>(hMeshGroupIcon));

	SetTreeImageList(IDC_OBJECT_TREE, &m_dlgBarObject, m_worldObjectTreeImages.GetSafeHandle());

	// delete all existing enries in tree
	int traceSel = -1;
	HTREEITEM track = NULL;
	DelTreeNode(IDC_OBJECT_TREE, &m_dlgBarObject, TVI_ROOT);

	// no need to traverse zone objects if no group is selected
	if (groupNum == -1)
		return;

	// in list all new entries
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		BOOL logIt = TRUE;
		int useIcon = ICON_WORLD;

		if (groupNum == -9998) {
			logIt = FALSE;

			if (pWO->m_filterGroup != -1) {
				logIt = TRUE;
			}
		}

		if (groupNum == -9999) {
			logIt = FALSE;

			if (pWO->m_particleLink != -1) {
				logIt = TRUE;
			}

			if (pWO->m_filterGroup != -1) {
				logIt = TRUE;
			}

			if (pWO->m_usedAsSpawnPoint != -1) {
				logIt = TRUE;
			}

			if (pWO->m_triggerList) {
				if (pWO->m_triggerList->GetCount() > 0) {
					logIt = TRUE;
				}
			}

			if (pWO->m_lightObject) {
				logIt = TRUE;
			}

			if (pWO->m_removeEnviorment) {
				logIt = TRUE;
			}

			if (pWO->m_meshObject) {
				if (pWO->m_meshObject->m_charona) {
					logIt = TRUE;
				}
			}
		}

		if (logIt == TRUE) {
			if (pWO->m_groupNumber == groupNum || groupNum == -9999 || groupNum == -9998) { // filter only group
				if (pWO->m_node && m_libraryReferenceDB) { // node conversion

					// In case the entire RLB or the corresponding RLB obj is missing, label this obj as "MISSING_RLB"
					pWO->m_fileNameRef = MakeZoneObjectID("MISSING_RLB", -1, pWO->m_identity);
					useIcon = ICON_REFERENCE; // ICON_REFERENCE_BROKEN? TBD. [Yanfeng 01/2009]

					if (m_libraryReferenceDB) {
						POSITION nodeLoc = m_libraryReferenceDB->FindIndex(pWO->m_node->m_libraryReference);
						if (nodeLoc) {
							CReferenceObj *refObj = (CReferenceObj *)m_libraryReferenceDB->GetAt(nodeLoc);
							if (refObj) {
								// Use global trace number for now until we have a mechanism to generate per object refcount.
								pWO->m_fileNameRef = MakeZoneObjectID(refObj->m_referenceName, -1, pWO->m_identity);
								useIcon = ICON_REFERENCE;
							}
						}
					}
				}

				HTREEITEM hTreeNode;

				CStringA meshName;
				int subID = 0;
				if (ParseSubMeshName(pWO->m_fileNameRef, meshName, subID)) {
					HTREEITEM hVirtualNode = NULL;
					if (virtualNodes.find(meshName) == virtualNodes.end()) {
						hVirtualNode = AddTreeRoot(Utf8ToUtf16(meshName).c_str(), IDC_OBJECT_TREE, &m_dlgBarObject, ICON_MESHGROUP);
						virtualNodes.insert(pair<CStringA, HTREEITEM>(meshName, hVirtualNode));
					} else
						hVirtualNode = virtualNodes[meshName];

					hTreeNode = AddTreeChild(Utf8ToUtf16(pWO->m_fileNameRef).c_str(), IDC_OBJECT_TREE, &m_dlgBarObject, hVirtualNode, useIcon, reinterpret_cast<LPARAM>(pWO));
				} else {
					hTreeNode = AddTreeRoot(Utf8ToUtf16(pWO->m_fileNameRef).c_str(), IDC_OBJECT_TREE, &m_dlgBarObject, useIcon, reinterpret_cast<LPARAM>(pWO));
				}

				if (traceNum == pWO->m_identity) {
					traceSel = traceNum;
					track = hTreeNode;
				}
			} // end filter only group
		}
	}

	if (traceSel != -1) {
		HWND treeHwnd;
		m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);
		TreeView_SelectItem(treeHwnd, track);
	}
}

BOOL CKEPEditView::SetCursorToSelectedObject(int trace) {
	if (m_editorModeEnabled == FALSE) {
		return FALSE;
	}

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);

		if (pWO->m_identity == trace) { // trace found

			CWldObject *selectedWldObj = GetSelectedWldObjectFromTreeView();
			if (selectedWldObj == pWO) // do nothing if already selected
				return TRUE;

			int groupSelIndex = 0;
			for (POSITION gPos = worldGroupList->GetHeadPosition(); gPos != NULL;) {
				CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetNext(gPos);
				if (wldPtr->m_groupInt == pWO->m_groupNumber) {
					break;
				}
				groupSelIndex++;
			}

			WorldGroupInList(groupSelIndex);
			ObjectTreeViewInlist(pWO->m_groupNumber, pWO->m_identity);

			return TRUE;
		} // trace found

	} // end Begin world object loop

	return FALSE;
} // end buuton down and select mode on

void CKEPEditView::WorldGroupInList(int sel) {
	if (m_editorModeEnabled == FALSE) {
		return;
	}

	HWND hWndGroupList = NULL;
	m_dlgBarObject.GetDlgItem(IDC_LIST_OBJECTS_GROUPLIST2BOX, &hWndGroupList);
	ASSERT(hWndGroupList != NULL);
	if (hWndGroupList == NULL)
		return; //@@Watch: to be changed to VERIFY_RETVOID

	if (sel == -1) {
		// Try use previous selection if not specified
		int prevSel = ListBox_GetCurSel(hWndGroupList);
		if (prevSel >= 0 && prevSel < worldGroupList->GetCount())
			sel = prevSel; // Previous selection must be valid
	}

	ListBox_ResetContent(hWndGroupList);

	ListItemMap groupMap = worldGroupList->GetGroups(); //groupList->GetGroups();
	ListItemMapIterator groupMapIterator;

	for (groupMapIterator = groupMap.begin(); groupMapIterator != groupMap.end(); groupMapIterator++) {
		CWldGroupObj *pWldGroupObj;
		int listBoxIndex;

		pWldGroupObj = worldGroupList->FindByID(groupMapIterator->first);
		listBoxIndex = ListBox_AddString(hWndGroupList, groupMapIterator->second.c_str());
		if (listBoxIndex >= 0)
			ListBox_SetItemData(hWndGroupList, listBoxIndex, reinterpret_cast<LPARAM>(pWldGroupObj));
	}

	if (sel != -1) {
		ListBox_SetCurSel(hWndGroupList, sel);
	}
}

void CKEPEditView::OnButtonAddnewstaticBk() {
	ASSERT(FALSE);
	//DeleteBackground ();

	// use mapsmodels folder for organization
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();

	// end use mapsmodels folder for organization
	static wchar_t BASED_CODE filter[] = L"Bitmap Files (*.bmp)|*.BMP|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		m_backgroundDlgBar.SetDlgItemText(IDC_EDIT_BACKGROUNDVSFILE, fileNameW);

		RECT usageRectLoc;
		usageRectLoc.left = 0;
		usageRectLoc.top = 0;
		usageRectLoc.right = 0;
		usageRectLoc.bottom = 0;
		CreateBackGroundObject(fileName, 0, 0, 0, 0, usageRectLoc, FALSE);
		ASSERT(FALSE);
		//InitializeBackground ( 0 );
	}
}

void CKEPEditView::OnBUTTONBKSETTINGSapply() {
}

void CKEPEditView::OnButtonTestmovie() {
}

void CKEPEditView::OnButtonTestEffect() {
	if (m_pCABD3DLIB->deviceIsForceFeedback[0] == FALSE) {
		return;
	}

	int Xdir = m_controlsDlgBar.GetForceDirectionX();
	int Ydir = m_controlsDlgBar.GetForceDirectionY();

	HRESULT hr;
	LONG rglDirection[2] = { Xdir, Ydir };

	DICONSTANTFORCE cf;
	DIEFFECT eff;
	cf.lMagnitude = 10000;

	eff.dwSize = sizeof(DIEFFECT);
	eff.dwFlags = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	eff.cAxes = 2;
	eff.rglDirection = rglDirection;
	eff.lpEnvelope = 0;
	eff.dwDuration = INFINITE;
	eff.cbTypeSpecificParams = sizeof(DICONSTANTFORCE);
	eff.lpvTypeSpecificParams = &cf;

	// now set the new parameters and start the effect immediately.
	hr = m_pCABD3DLIB->deviceEffect[0]->SetParameters(&eff, DIEP_DIRECTION | DIEP_TYPESPECIFICPARAMS | DIEP_START);
	if (hr != DD_OK) {
		m_pCABD3DLIB->deviceFound[0]->Acquire();
		hr = m_pCABD3DLIB->deviceEffect[0]->SetParameters(&eff, DIEP_DIRECTION | DIEP_TYPESPECIFICPARAMS | DIEP_START);
	}
}

void CKEPEditView::OnButtonStopEffect() {
}

void CKEPEditView::OnBtnEntityCreatenew() {
#ifndef _ClientOutput
	CAddEntityDlg dlg;
	dlg.saveCollisionInfo = TRUE;
	if (dlg.DoModal() != IDOK) {
		return;
	}

	//get the next actor trace
	int temptrace = -1;
	POSITION p = m_movObjRefModelList->GetHeadPosition();
	while (p) {
		CMovementObj *tObj = (CMovementObj *)m_movObjRefModelList->GetAt(p);
		if (tObj->m_traceNumber > temptrace)
			temptrace = tObj->m_traceNumber;

		m_movObjRefModelList->GetNext(p);
	}
	temptrace++;

	CMovementObj *mPtr = new CMovementObj(dlg.m_entityName, temptrace);

	if (mPtr && LoadActorFromDir(*mPtr, dlg.m_folderPath, dlg.m_fileNames, TRUE)) {
		if (!dlg.m_entityName.IsEmpty()) {
			// User request entity name overridden
			mPtr->m_runtimeSkeleton->m_SkeletalName = dlg.m_entityName;
		}

		// Synchronize entity name and skeleton name
		mPtr->m_name = mPtr->m_runtimeSkeleton->m_SkeletalName;

		if (mPtr->m_camera_data.empty()) {
			// no camera - assign one
			CCameraObjList *pAllCameras = IClientEngine::Instance()->GetCameraList();
			if (pAllCameras && !pAllCameras->IsEmpty()) {
				CMovementObj::Camera_Data camData = { DEFAULT_MOV_CAMERA_ID };
				mPtr->m_camera_data.push_back(camData);
			}
		}

		if (mPtr->m_unscaledBoundingRadius == 0) {
			// cliping radius not set, use default
			mPtr->m_unscaledBoundingRadius = DEFAULT_MOV_CLIPPING_RADIUS;
		}

		if (!mPtr->m_stats)
			m_statDatabase->Clone(&mPtr->m_stats);

		InitFXShadersOnSkeletal(mPtr->m_runtimeSkeleton, mPtr->m_skeletonConfiguration);
		//		ReinitAllTexturesFromDatabase ();
		m_pTextureDB->TextureMapDelAll();

		// Check all of the deformable meshes within the skeleton that was just
		// loaded. See if any materials exist within the material lists attached
		// to the deformable mesh. If no materials exist within the material
		// list then add a default material.
		if (mPtr->m_runtimeSkeleton && mPtr->m_skeletonConfiguration->m_alternateConfigurations) {
			POSITION alterConfigPos;
			CAlterUnitDBObject *pAlterUnit;

			alterConfigPos = mPtr->m_skeletonConfiguration->m_alternateConfigurations->GetHeadPosition();
			for (; alterConfigPos; mPtr->m_skeletonConfiguration->m_alternateConfigurations->GetNext(alterConfigPos)) {
				pAlterUnit = static_cast<CAlterUnitDBObject *>(mPtr->m_skeletonConfiguration->m_alternateConfigurations->GetAt(alterConfigPos));
				if (!pAlterUnit)
					continue;

				POSITION deformMeshPos;
				CDeformableMesh *pDeformMesh;

				deformMeshPos = pAlterUnit->m_configurations->GetHeadPosition();
				for (; deformMeshPos; pAlterUnit->m_configurations->GetNext(deformMeshPos)) {
					pDeformMesh = static_cast<CDeformableMesh *>(pAlterUnit->m_configurations->GetAt(deformMeshPos));
					if (!pDeformMesh)
						continue;

					bool addDefaultMaterial = false;
					if (pDeformMesh->m_supportedMaterials) {
						int numberOfMaterials;
						numberOfMaterials = pDeformMesh->m_supportedMaterials->GetSize();
						if (!numberOfMaterials) {
							addDefaultMaterial = true;
						} // if..!numberOfMaterials
					} // if..pDeformMesh->m_supportedMaterials
					else {
						pDeformMesh->m_supportedMaterials = new CObList;
						if (pDeformMesh->m_supportedMaterials)
							addDefaultMaterial = true;
					} // if..else..pDeformMesh->m_supportedMaterials

					if (addDefaultMaterial) {
						CMaterialObject *pMaterial;
						pMaterial = new CMaterialObject();
						pMaterial->m_materialName = "Default Material";
						pDeformMesh->m_supportedMaterials->AddTail(pMaterial);
						MeshRM::Instance()->CreateReMaterials(pDeformMesh);
					} // if..addDefaultMaterial
				} // for..deformMeshPos
			} // for..alterConfigPos
		} // if..mPtr->m_skeletalObject

		AddTreeRoot(Utf8ToUtf16(mPtr->m_runtimeSkeleton->m_SkeletalName).c_str(), IDC_TREE_ENTITIES, &m_entityDlgBar, -1, NULL, TRUE);

		// Pass focus back to treeview
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES)->SetFocus();

		m_movObjRefModelList->AddTail(mPtr);
		m_entityDlgBar.SetSettingsChanged(TRUE);
	} else {
		CStringW s;
		s.LoadString(IDS_ERROR_SKELETALLOADFAILED);
		s + Utf8ToUtf16(dlg.m_folderPath).c_str() + L"[";
		for (UINT i = 0; i < dlg.m_fileNames.size(); i++)
			s += Utf8ToUtf16(dlg.m_fileNames[i]).c_str();
		AfxMessageBox(s);

		if (mPtr) {
			mPtr->SafeDelete();
			delete mPtr;
		} // if..mPtr
	}
#endif
}

void CKEPEditView::OnBtnEntityActivate() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr = NULL; // DRF - potentially uninitialized variable

	// get selected index
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	bool ok = false;
	CPlacementDlg pDlg;
	Matrix44f tempMatrix = { 0 }; // DRF - potentially uninitialized variable
	Vector3f tempPosition;

	pDlg.m_posX = ((IClientEngine *)IClientEngine::Instance())->GetPlacementPositionX();
	pDlg.m_posY = ((IClientEngine *)IClientEngine::Instance())->GetPlacementPositionY();
	pDlg.m_posZ = ((IClientEngine *)IClientEngine::Instance())->GetPlacementPositionZ();

	if (pDlg.DoModal() == IDOK) {
		((IClientEngine *)IClientEngine::Instance())->SetPlacementPositionX(pDlg.m_posX);
		((IClientEngine *)IClientEngine::Instance())->SetPlacementPositionY(pDlg.m_posY);
		((IClientEngine *)IClientEngine::Instance())->SetPlacementPositionZ(pDlg.m_posZ);

		CFrameObj *tempFrame = new CFrameObj(NULL);
		if (pDlg.PlacementMethod == 0) { // user define
			Vector3f temp;
			temp.x = pDlg.m_posX;
			temp.y = pDlg.m_posY;
			temp.z = pDlg.m_posZ;
			tempFrame->SetPosition(NULL, temp);
			tempFrame->GetTransform(NULL, &tempMatrix);
		}

		if (pDlg.PlacementMethod == 1) { // based on entities position
			int runtimeIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
			if (runtimeIndex == -1) {
				AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
				return;
			}

			POSITION posLoc = m_movObjList->FindIndex(runtimeIndex);
			if (!posLoc)
				return;

			CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
			ptrLoc->m_baseFrame->GetPosition(NULL, &tempPosition);
			tempFrame->SetPosition(NULL, tempPosition);
			tempFrame->GetTransform(NULL, &tempMatrix);
		} // end based on entities position

		if (pDlg.PlacementMethod == 2) { // based on entities matrix
			int runtimeIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
			if (runtimeIndex == -1) {
				AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
				return;
			}

			POSITION posLoc = m_movObjList->FindIndex(runtimeIndex);
			if (!posLoc) {
				return;
			}

			CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
			ptrLoc->m_baseFrame->GetTransform(NULL, &tempMatrix);
		} // end based on entities matrix

		tempFrame->SafeDelete();
		delete tempFrame;
		tempFrame = 0;

		// end prepare location data
		// place entity if evrything is ok
		eActorControlType controlType = eActorControlType::CONTROL_TYPE_PC;
		if (pDlg.m_comboEntityType == "AI")
			controlType = eActorControlType::CONTROL_TYPE_NPC;
		if (pDlg.m_comboEntityType == "VEHICLE")
			controlType = eActorControlType::CONTROL_TYPE_VEHICLE;
		if (pDlg.m_comboEntityType == "STATIC")
			controlType = eActorControlType::CONTROL_TYPE_15;
		if (pDlg.m_comboEntityType == "PICKUP ITEM")
			controlType = eActorControlType::CONTROL_TYPE_STATIC;

		CMovementObj *movObjRef = NULL; // DRF - potentially uninitialized variable
		CMovementObj *movObj = NULL;
		POSITION pos = m_movObjRefModelList->FindIndex(indexLoc);
		if (pos)
			movObjRef = (CMovementObj *)m_movObjRefModelList->GetAt(pos);

		// Create New Movement Object
		if (controlType == eActorControlType::CONTROL_TYPE_PC) {
			ok = ActivateMovementEntity(
				0,
				tempMatrix,
				indexLoc,
				controlType,
				0,
				0,
				FALSE,
				TRUE,
				false,
				-1,
				&movObj,
				NULL,
				pDlg.m_animationOv,
				-1,
				false,
				"");
		} else {
			ok = ActivateMovementEntity(
				pDlg.m_aiBotIndex,
				tempMatrix,
				indexLoc,
				controlType,
				0,
				0,
				FALSE,
				FALSE,
				false,
				-1,
				&movObj,
				NULL,
				pDlg.m_animationOv,
				-1,
				false,
				"");
		}
		if (!ok) {
			LogError("ActivateMovementEntity() FAILED");
		}

		if (ok && movObj && movObjRef)
			movObj->RealizeNow(movObjRef);
	}
#endif

	// Before we reinitialize the runtime entities list. Be sure to
	// save off which item in the list box was selected. We'll do this
	// by getting the selected items text, then after the new
	// item is inserted into the list box, we'll search for the
	// selected items text.

	int runtimeEntityIndex;
	int stringLength;
	wchar_t *pSelectionString;

	stringLength = 0;
	pSelectionString = 0;
	runtimeEntityIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (LB_ERR != runtimeEntityIndex) {
		stringLength = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETTEXTLEN, static_cast<WPARAM>(runtimeEntityIndex), static_cast<LPARAM>(0));
		if (LB_ERR != stringLength) {
			pSelectionString = new wchar_t[stringLength + 1];
			m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETTEXT, static_cast<WPARAM>(runtimeEntityIndex), reinterpret_cast<LPARAM>(pSelectionString));
		}
	}

	// Reinitialize the runtime entities list box.
	InListRuntimeEntities(-1);

	// Now that the runtime entities list box has been reinitialized,
	// lets attempt to reselect the entry that was selected before
	// the list box was reinitialized.

	if (pSelectionString) {
		runtimeEntityIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_FINDSTRING, static_cast<WPARAM>(0), reinterpret_cast<LPARAM>(pSelectionString));
		m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_SETCURSEL, static_cast<WPARAM>(runtimeEntityIndex), static_cast<LPARAM>(0));

		delete[] pSelectionString;
	} else
		m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_SETCURSEL, static_cast<WPARAM>(-1), static_cast<LPARAM>(0));
}

void CKEPEditView::OnBtnEntityAddchild() {
	return;
}

void CKEPEditView::OnButtonTestfullscreen() {
	// the background doesnt seem to make it so destroy then recreate
	POSITION posBkLoc = BackGroundList->FindIndex(0);
	if (posBkLoc) { // if valid
		CBackGroundObj *bkLoc = (CBackGroundObj *)BackGroundList->GetAt(posBkLoc);
		if (bkLoc->m_mainSurface) {
			bkLoc->m_mainSurface->Release();
			bkLoc->m_mainSurface = 0;
		}
	} // end if valid

	// end the background doesnt seem to make it so destroy then
	// recreate
	CreateDisplayIM(TRUE, 0, 0, ResolutionWidth, ResolutionHeight, 16);

	ASSERT(FALSE);
	//InitializeBackground ( 0 ); // restore background
}

void CKEPEditView::OnBtnEntityMovement() {
#ifndef _LimitedEngine
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();

	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		CMovementObj *mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr)
			continue;

		if (temp == mPtr->m_name) {
			CMotionSettingsDlg dlg;
			dlg.movementDB = mPtr->m_movementDB;
			if (dlg.DoModal() == IDOK) {
				mPtr->m_movementDB = dlg.movementDB;
			}

			return;
		}
	}

	AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
	return;
#endif
#endif
}

void CKEPEditView::DeactivateEntity(int runtimeIndex) {
	if (runtimeIndex > -1) {
		POSITION posLoc = m_movObjList->FindIndex(runtimeIndex);
		if (posLoc) {
			CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
			if (ptrLoc) {
				DestroyRuntimeEntity(ptrLoc);
				ptrLoc = 0;
				m_movObjList->RemoveAt(posLoc);
				InListRuntimeEntities(runtimeIndex - 1);
			}
		}
	}
}
void CKEPEditView::InListRuntimeEntities(int sel) {
	if (m_editorModeEnabled == FALSE || !IsWindow(m_entityDlgBar)) {
		return;
	}

	m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
		CMovementObj *entLoc = (CMovementObj *)m_movObjList->GetNext(posLoc);
		stringstream strBuilder;
		strBuilder << entLoc->m_name << ">" << entLoc->m_traceNumber << " id>" << entLoc->m_networkDefinedID;

		m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_ADDSTRING,
			0, (LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
	}

	if (sel > -1) {
		m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_SETCURSEL, sel, 0);
	}
}

// End Function

void CKEPEditView::OnBtnEntityDeactivate() {
	int runtimeIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELACTOR);
		return;
	}

	DeactivateEntity(runtimeIndex);
}

int CKEPEditView::GetRuntimeEntityIndexFromEntityObject(CMovementObj *entityObj) {
	int retVal = -1;
	int index = 0;

	if (entityObj) {
		for (POSITION posLoc = m_movObjList->GetHeadPosition(); posLoc != NULL;) {
			CMovementObj *mPtr = (CMovementObj *)m_movObjList->GetNext(posLoc);

			if (mPtr->m_name == entityObj->m_name) {
				retVal = index;
				break;
			}

			index++;
		}
	}

	return retVal;
}

void CKEPEditView::OnBtnDeleteEntity() {
	HWND treeHwnd = NULL;
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringW tempW = GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item);

	CStringW msg = L"This will delete actor [" + tempW + L"], continue?";
	if (MessageBoxW(msg, L"Confirm", MB_ICONQUESTION | MB_YESNO) != IDYES)
		return;

	CStringA temp = Utf16ToUtf8(tempW).c_str();
	POSITION delPos;
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); (delPos = posLoc) != NULL;) { // visual loop
		CMovementObj *mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr)
			continue;

		if (temp == mPtr->m_name) {
			//
			// clean up any associated runtime entities
			//

			// get index of any runtime entities based on db entity
			int runtimeIndex = GetRuntimeEntityIndexFromEntityObject(mPtr);
			while (runtimeIndex > -1) {
				DeactivateEntity(runtimeIndex);
				runtimeIndex = GetRuntimeEntityIndexFromEntityObject(mPtr);
			}

			mPtr->SafeDelete();
			delete mPtr;
			mPtr = 0;
			m_movObjRefModelList->RemoveAt(delPos);
			m_entityDlgBar.SetSettingsChanged(TRUE);
			DelTreeNode(IDC_TREE_ENTITIES, &m_entityDlgBar, item);

			// Pass focus back to treeview
			m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES)->SetFocus();

			return;
		}
	}

	AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
	return;
}

void CKEPEditView::OnBTNENTITYdeleteAnim() {
}

void CKEPEditView::OnBTNENTITYDeleteChld() {
}

void CKEPEditView::OnBtnNewall() {
	OnResetAll();
}

void CKEPEditView::OnCAMERACreateNewCamera() {
	// retrieve Field of view
	float fovLoc = .8f;

	// retrieve clip plane
	float clipPlaneLoc = 60000.0f;

	// determine camera type
	CStringA cameraTypeLoc;
	int cameraTypeInt = 0;
	cameraTypeInt = 0;

	// add the camera to the database
	int index = CreateCamera("New Camera", "NOVIS", cameraTypeInt, TRUE, clipPlaneLoc, fovLoc);

	CStringW sLabel;
	POSITION posLoc = m_pCOL->FindIndex(index);
	if (posLoc) {
		CCameraObj *cam = (CCameraObj *)m_pCOL->GetAt(posLoc);
		if (cam) {
			sLabel = Utf8ToUtf16(cam->m_cameraName).c_str();
		}
	}

	m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_ADDSTRING, 0, (LPARAM) static_cast<const wchar_t *>(sLabel.GetString()));
	m_camVptDlgBar.SetSettingsChanged(TRUE);
}

void CKEPEditView::OnBTNCreateNewViewport() {
	int topPercent = m_camVptDlgBar.GetPercentTop();
	int bottomPercent = m_camVptDlgBar.GetPercentBottom();
	int leftPercent = m_camVptDlgBar.GetPercentLeft();
	int rightPercent = m_camVptDlgBar.GetPercentRight();
	CStringA name = m_camVptDlgBar.GetViewportName();

	CreateCustomViewPort(name, leftPercent, topPercent, rightPercent, bottomPercent);

	m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(name).c_str());
}

void CKEPEditView::OnBTNCameraVportActivate() {
	int cameraIndex = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_GETCURSEL, 0, 0);
	if (cameraIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCAMERA);
		return;
	}

	int vportIndex = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (vportIndex == -1) {
		AfxMessageBox(IDS_MSG_SELVIEWPORT);
		return;
	}

	int entIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
		return;
	}

	POSITION posLoc6 = m_movObjList->FindIndex(entIndex);
	if (!posLoc6) {
		AfxMessageBox(IDS_ERROR_RUNTIMEACTOR);
		return;
	}
}

void CKEPEditView::VportCameraInList() {
	// Vports
	m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_pVL->GetHeadPosition(); posLoc != NULL;) {
		CViewportObj *viewport = (CViewportObj *)m_pVL->GetNext(posLoc);

		m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(viewport->GetName()).c_str());
	}

	// Cameras
	m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_RESETCONTENT, 0, 0);

	CCameraDBToListItemMap l_cameraDBToListItemMap(m_pCOL);
	ListItemMap cameraMap = l_cameraDBToListItemMap.GetCameras();
	ListItemMapIterator cameraMapIterator;

	for (cameraMapIterator = cameraMap.begin(); cameraMapIterator != cameraMap.end(); cameraMapIterator++) {
		m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(cameraMapIterator->second).c_str());
	}
}

void CKEPEditView::OnBTNvportDeactivate() {
	/*int vportIndex =*/m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_GETCURSEL, 0, 0);
}

bool CKEPEditView::AddRuntimeCamera(CStringA camera_name, int runtime_camera_id) {
	int index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(camera_name).c_str());
	if (LB_ERR != index) {
		m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_SETITEMDATA, index, runtime_camera_id);

		// update runtime viewports property control
		m_camVptDlgBar.UpdateViewportPropertiesRuntimeCameras();

		return true;
	}

	return false;
}

bool CKEPEditView::RemoveRuntimeCamera(int runtime_camera_id) {
	if (!IsWindow(m_camVptDlgBar.m_hWnd))
		return false;

	int count = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_GETCOUNT, 0, 0);
	for (int i = 0; i < count; i++) {
		int rt_cam_id = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_GETITEMDATA, i, 0);
		if ((LB_ERR != rt_cam_id) && (rt_cam_id == runtime_camera_id)) {
			m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_DELETESTRING, i, 0);
			m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_SETCURSEL, i - 1, 0);

			// when running distro, this is on background thread and is not allowed
			if (!m_creatingDistribution) {
				// update runtime viewports property control
				m_camVptDlgBar.UpdateViewportPropertiesRuntimeCameras();
				m_camVptDlgBar.UpdatePropertyControls();
			}
			return true;
		}
	}

	return false;
}

void CKEPEditView::OnBtnAddRuntimeCamera() {
	int db_camera_index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_GETCURSEL, 0, 0);
	if (LB_ERR == db_camera_index) {
		AfxMessageBox(IDS_MSG_SELCAMERA);
		return;
	}

	POSITION posLoc = m_pCOL->FindIndex(db_camera_index);
	if (posLoc) {
		CCameraObj *camera = (CCameraObj *)m_pCOL->GetAt(posLoc);
		if (camera) {
			// script cameras do not require an actor
			if (camera->m_type == CCameraObj::Script) {
				//AfxMessageBox( IDS_MSG_ADD_RUNTIME_CAMERA );
				/*int new_camera_id = */ m_runtime_cameras->Add(db_camera_index);
			} else {
				int entIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
				if (entIndex == -1) {
					AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
					return;
				}

				POSITION posLoc6 = m_movObjList->FindIndex(entIndex);
				if (!posLoc6) {
					AfxMessageBox(IDS_ERROR_RUNTIMEACTOR);
					return;
				}

				int new_camera_id = m_runtime_cameras->Add(db_camera_index);
				CCameraObj *new_camera = m_runtime_cameras->GetCamera(new_camera_id);

				CMovementObj *actor = (CMovementObj *)m_movObjList->GetAt(posLoc6);
				// attach camera and make it removable
				AttachCameraToActor(new_camera, actor, true);
			}
		}
	}
}

void CKEPEditView::OnBtnRemoveRuntimeCamera() {
	int index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_GETCURSEL, 0, 0);
	if (LB_ERR != index) {
		int runtime_camera_id = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_CAMERAS, LB_GETITEMDATA, index, 0);
		CCameraObj *camera = m_runtime_cameras->GetCamera(runtime_camera_id);
		if (camera) {
			if (camera->m_removable)
				m_runtime_cameras->Remove(runtime_camera_id);
			else
				AfxMessageBox(IDS_MSG_CANT_REMOVE_RUNTIME_ACTOR);
		}
	}
}

void CKEPEditView::OnBtnAddRuntimeViewport() {
	int viewport_db_index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (LB_ERR == viewport_db_index) {
		AfxMessageBox(IDS_MSG_SELVIEWPORT);
		return;
	}

	int new_runtime_viewport_id = AddRuntimeViewport(viewport_db_index);
	if (new_runtime_viewport_id > 0) {
		CViewportObj *new_viewport = m_runtimeViewportDB->GetViewportById(new_runtime_viewport_id);
		if (new_viewport) {
			int index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(new_viewport->GetName()).c_str());
			if (LB_ERR != index) {
				// runtime id will be the key (data)
				m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_SETITEMDATA, index, new_viewport->GetID());

				// set default binding
				m_camVptDlgBar.SetCameraBinding(new_viewport->GetID(), 0);
			}
		}
	}
}

void CKEPEditView::OnBtnRemoveRuntimeViewport() {
	int index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (LB_ERR != index) {
		int runtime_viewport_id = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_GETITEMDATA, index, 0);
		if (m_runtimeViewportDB->RemoveViewport(runtime_viewport_id)) {
			m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_DELETESTRING, index, 0);
			m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_SETCURSEL, index - 1, 0);

			// remove camera binding
			m_camVptDlgBar.RemoveCameraBinding(runtime_viewport_id);

			OnSelchangeRuntimeViewports();
		}
	}
}

void CKEPEditView::OnBTNMOUSEcreate() {
}

void CKEPEditView::OnBTNMOUSEfileBrowse() {
	// use mapsmodels folder for organization
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();

	// end use mapsmodels folder for organization
	static wchar_t BASED_CODE filter[] = L"Bitmap Files (*.bmp)|*.bmp|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		UpdateData(TRUE);

		CStringW fileNameW = opendialog.GetFileTitle();
		m_mouseDlgBar.SetDlgItemText(IDC_EDIT_MOUSEFILE, fileNameW);
		UpdateData(FALSE);
	}
}

void CKEPEditView::OnBTNMOUSEactivate() {
}

void CKEPEditView::OnBTNMOUSEdelete() {
}

void CKEPEditView::OnBTNMOUSEdeactivate() {
}

void CKEPEditView::OnBTN2DCreateCollection() {
}

void CKEPEditView::OnBTN2DADDunit() {
}

void CKEPEditView::OnBtn2dActivate() {
}

void CKEPEditView::OnBtn2dDeactivate() {
}

void CKEPEditView::OnBTN2DBrowseFileUp() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();

	static wchar_t BASED_CODE filter[] = L"Bitmap Files (*.bmp)|*.bmp|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		UpdateData(TRUE);

		CStringW fileNameW = opendialog.GetFileName();
		m_2DinterfaceBar.SetDlgItemText(IDC_EDIT_2D_FILEUP, fileNameW);
		UpdateData(FALSE);
	}
}

void CKEPEditView::OnBTN2DBrowseFileDown() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();

	static wchar_t BASED_CODE filter[] = L"Bitmap Files (*.bmp)|*.bmp|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		UpdateData(TRUE);

		CStringW fileNameW = opendialog.GetFileName();
		m_2DinterfaceBar.SetDlgItemText(IDC_EDIT_2D_FILEDOWN, fileNameW);
		UpdateData(FALSE);
	}
}

void CKEPEditView::OnBTN2DgetSelectedInfo() {
}

void CKEPEditView::OnBTN2DApplyModifications() {
}

void CKEPEditView::OnBTN2DDeleteCollection() {
}

void CKEPEditView::OnBTN2DDeleteUnit() {
}

void CKEPEditView::OnBTNENVBckRgb() {
#ifndef _ClientOutput
	CColorDlg cdlg;
	cdlg.m_red = m_environment->m_backRed;
	cdlg.m_green = m_environment->m_backGreen;
	cdlg.m_blue = m_environment->m_backBlue;
	cdlg.DoModal();

	m_environment->m_backRed = cdlg.m_red;
	m_environment->m_backGreen = cdlg.m_green;
	m_environment->m_backBlue = cdlg.m_blue;
	InitializeEnvironment(TRUE);
#endif
}

void CKEPEditView::OnBTNENVFogRgb() {
#ifndef _ClientOutput
	CColorDlg cdlg;
	cdlg.m_red = m_environment->m_fogRed;
	cdlg.m_green = m_environment->m_fogGreen;
	cdlg.m_blue = m_environment->m_fogBlue;
	cdlg.DoModal();

	m_environment->m_fogRed = cdlg.m_red;
	m_environment->m_fogGreen = cdlg.m_green;
	m_environment->m_fogBlue = cdlg.m_blue;
	InitializeEnvironment(TRUE);
#endif
}

void CKEPEditView::OnBTNENVApplyChanges() {
	m_enviormentBar.UpdateEnvironment();

	// Invalidate light caches for world objects and dynamic placement objects (Mantis issue 6246/6248)
	InvalidateLightCaches();

	InitializeEnvironment(TRUE);
	EditorState::GetInstance()->SetZoneDirty();
}

void CKEPEditView::OnBTNENVMatch() {
	m_environment->m_backRed = m_environment->m_fogRed;
	m_environment->m_backGreen = m_environment->m_fogGreen;
	m_environment->m_backBlue = m_environment->m_fogBlue;
	InitializeEnvironment(TRUE);
}

void CKEPEditView::OnBTNENVBrowseSurObj() {
}

void CKEPEditView::OnCHECKEnableSurrounding() {
}

void CKEPEditView::OnBTNObjectMaterial() {
#ifndef _ClientOutput
	CWldObject *ptrLoc = GetSelectedWldObjectFromTreeView();

	if (ptrLoc) {
		if (!ptrLoc->m_meshObject) { // no mesh object available
			AfxMessageBox(IDS_MSG_NOMESHOBJECTAVAILABLE);
			return;
		} // end no mesh object available

		CMaterialDlg *pMaterialDlg;
		pMaterialDlg = new CMaterialDlg();
		if (!pMaterialDlg)
			return;

		pMaterialDlg->m_pxShaderSystemRef = m_pxShaderSystem;
		pMaterialDlg->m_vxShaderSystemRef = m_shaderSystem;
		pMaterialDlg->m_globalShaderRef = m_cgGlobalShaderDB;
		pMaterialDlg->GL_textureDataBase = m_pTextureDB;
		pMaterialDlg->m_materialReference = ptrLoc->m_meshObject->m_materialObject;
		pMaterialDlg->makeMirror = ptrLoc->m_meshObject->m_materialObject->m_mirrorEnabled;
		pMaterialDlg->SetWorldObject(ptrLoc);
		pMaterialDlg->SetWorldObjectList(m_pWOL);
		pMaterialDlg->m_zoneName = NewWorld;
		pMaterialDlg->m_pShaderList = m_cgGlobalShaderDB;

		pMaterialDlg->Create(IDD_DIALOG_MATERIALMODIFY, this);
		pMaterialDlg->ShowWindow(SW_SHOW);
	} else
		AfxMessageBox(IDS_MSG_SELVISUAL);
#endif
}

void CKEPEditView::OnBTNENVemmisive() {
}

void CKEPEditView::OnBTNEntityMaterial() {
}

void CKEPEditView::CreateTextCollection(CStringA CollectionName, int timeOn, int fontSelection, COLORREF color) // 1

{
	CTextCollection *cTextPtr = new CTextCollection(CollectionName, NULL, 0, timeOn, fontSelection, FALSE, color);
	cTextPtr->m_textEntryList = new CTextEntryList;
	m_pTCL->AddTail(cTextPtr);
}

BOOL CKEPEditView::AddTextEntry(int collectionIndex, CStringA text, int percX, int percY) {
	POSITION posLoc = m_pTCL->FindIndex(collectionIndex);
	if (!posLoc) {
		return FALSE;
	}

	CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);
	CTextEntry *entryPtr = new CTextEntry(text, percX, percY);
	cTextPtr->m_textEntryList->AddTail(entryPtr);
	return TRUE;
}

BOOL CKEPEditView::ActivateTextDisplay(int collectionIndex) {
	POSITION posLoc = m_pTCL->FindIndex(collectionIndex);
	if (!posLoc) {
		return FALSE;
	}

	CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);
	cTextPtr->m_displayIsOn = TRUE;
	return TRUE;
}

BOOL CKEPEditView::DeActivateTextDisplay(int collectionIndex) {
	POSITION posLoc = m_pTCL->FindIndex(collectionIndex);
	if (!posLoc) {
		return FALSE;
	}

	CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);
	cTextPtr->m_displayIsOn = FALSE;
	return TRUE;
}

void CKEPEditView::DeActivateAllTextDisplays() {
	for (POSITION posLoc = m_pTCL->GetHeadPosition(); posLoc != NULL;) { // loopB
		CTextCollection *collectionPtr = (CTextCollection *)m_pTCL->GetNext(posLoc);
		collectionPtr->m_displayIsOn = FALSE;
	}
}

void CKEPEditView::AddFont(int sizeX, int sizeY) {
	CFontDialog fdlg;
	fdlg.DoModal();

	CFontObj *fntPtr = new CFontObj(NULL, Utf16ToUtf8(fdlg.GetFaceName()).c_str(), sizeX, sizeY);
	fntPtr->m_fontObject = CreateFont((int)(sizeX),
		(int)(sizeY),
		0,
		0,
		1,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		PROOF_QUALITY,
		VARIABLE_PITCH,
		Utf8ToUtf16(fntPtr->m_fontName).c_str());
	m_pFOL->AddTail(fntPtr);
	m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_FONTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(fntPtr->m_fontName).c_str());
}

void CKEPEditView::InitAllFonts() {
	m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_FONTS, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_pFOL->GetHeadPosition(); posLoc != NULL;) { // loopB
		CFontObj *fntPtr = (CFontObj *)m_pFOL->GetNext(posLoc);
		fntPtr->m_fontObject = CreateFont((int)(fntPtr->m_sizeX),
			(int)(fntPtr->m_sizeY),
			0,
			0,
			1,
			FALSE,
			FALSE,
			FALSE,
			ANSI_CHARSET,
			OUT_DEFAULT_PRECIS,
			CLIP_DEFAULT_PRECIS,
			PROOF_QUALITY,
			VARIABLE_PITCH,
			Utf8ToUtf16(fntPtr->m_fontName).c_str());
		m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_FONTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(fntPtr->m_fontName).c_str());
	}
}

void CKEPEditView::OnBUTTONtextAddTextCollection() {
	CStringW data;
	m_textFontDlgBar.GetDlgItemText(IDC_EDIT_TEXT_DATA, data);
	if (data.GetLength() == 0) {
		AfxMessageBox(IDS_MSG_ENTERCOLLECTIONNAME);
		return;
	}

	CreateTextCollection(Utf16ToUtf8(data).c_str(), -1, 0, RGB(0, 0, 0));
	AddTreeChild(data, IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, TextDisplayTreeROOT);
}

void CKEPEditView::OnBUTTONtextAddTextObject() {
	HWND treeHwnd = NULL;

	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1 || selectionLevel != 1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int collectionIndex = GetTreeChildIndex(treeHwnd, item);

	CStringW data;
	m_textFontDlgBar.GetDlgItemText(IDC_EDIT_TEXT_DATA, data);
	if (data.GetLength() == 0) {
		AfxMessageBox(IDS_MSG_ENTERTEXT);
		return;
	}

	int tempPercX = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_PERCX, NULL, TRUE);
	int tempPercY = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_PERCY, NULL, TRUE);

	AddTextEntry(collectionIndex, Utf16ToUtf8(data).c_str(), tempPercX, tempPercY);
	AddTreeChild(data, IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, item);
}

int CKEPEditView::GetTreeSelectionLevel(UINT id, CDialogBar *barLoc) {
	HTREEITEM item = GetSelectedItem(id, barLoc);
	if (!item)
		return -1;

	return GetTreeChildDepth(id, barLoc, item);
}

void CKEPEditView::OnBTNtextActivate() {
	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1 || selectionLevel != 1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int collectionIndex = GetTreeChildIndex(treeHwnd, item);
	ActivateTextDisplay(collectionIndex);
}

void CKEPEditView::OnBTNtextDeactivate() {
	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1 || selectionLevel != 1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int collectionIndex = GetTreeChildIndex(treeHwnd, item);
	DeActivateTextDisplay(collectionIndex);
}

void CKEPEditView::OnBUTTONtextTextColor() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1 || selectionLevel != 1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int collectionIndex = GetTreeChildIndex(treeHwnd, item);

	POSITION posLoc = m_pTCL->FindIndex(collectionIndex);
	if (!posLoc) {
		return;
	}

	CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);

	CColorDlg dlg;
	dlg.m_red = (float)GetRValue(cTextPtr->m_textColor);
	dlg.m_green = (float)GetGValue(cTextPtr->m_textColor);
	dlg.m_blue = (float)GetBValue(cTextPtr->m_textColor);

	dlg.color255 = TRUE;
	if (dlg.DoModal() == TRUE) {
		cTextPtr->m_textColor = RGB(dlg.m_red, dlg.m_green, dlg.m_blue);
	}

#endif
}

void CKEPEditView::OnBTNtextApplyChanges() {
	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int universalIndex = GetTreeChildIndex(treeHwnd, item);

	if (selectionLevel == 1) { // a collection is selected
		POSITION posLoc = m_pTCL->FindIndex(universalIndex);
		if (!posLoc) {
			return;
		}

		CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);
		cTextPtr->m_timeLength = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_TIMELIMIT, NULL, TRUE);
		cTextPtr->m_timeVarial = 0;

		CStringW data;
		m_textFontDlgBar.GetDlgItemText(IDC_EDIT_TEXT_DATA, data);
		if (data.GetLength() == 0) {
			AfxMessageBox(IDS_MSG_ENTERTEXT);
			return;
		}

		cTextPtr->m_collectionName = Utf16ToUtf8(data).c_str();
		SetTreeItemText(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, item, data);
	} // end a collection is selected

	if (selectionLevel == 2) { // a text entry is selected
		HTREEITEM parent = TreeView_GetParent(treeHwnd, item);
		int parentIndex = GetTreeChildIndex(treeHwnd, parent);
		POSITION posLoc = m_pTCL->FindIndex(parentIndex);
		if (!posLoc) {
			return;
		}

		CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);

		posLoc = cTextPtr->m_textEntryList->FindIndex(universalIndex);
		if (!posLoc) {
			return;
		}

		CTextEntry *entryPtr = (CTextEntry *)cTextPtr->m_textEntryList->GetAt(posLoc);

		CStringW data;
		m_textFontDlgBar.GetDlgItemText(IDC_EDIT_TEXT_DATA, data);
		if (data.GetLength() == 0) {
			AfxMessageBox(IDS_MSG_ENTERTEXT);
			return;
		}

		entryPtr->m_text = Utf16ToUtf8(data).c_str();
		entryPtr->m_percX = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_PERCX, NULL, TRUE);
		entryPtr->m_percY = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_PERCY, NULL, TRUE);
		SetTreeItemText(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, item, data);
	} // end a text entry is selected
}

void CKEPEditView::OnBTNtextGetCurrentInfo() {
	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int universalIndex = GetTreeChildIndex(treeHwnd, item);

	if (selectionLevel == 1) { // a collection is selected
		POSITION posLoc = m_pTCL->FindIndex(universalIndex);
		if (!posLoc) {
			return;
		}

		CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);
		m_textFontDlgBar.SetDlgItemText(IDC_EDIT_TEXT_DATA, Utf8ToUtf16(cTextPtr->m_collectionName).c_str());
		m_textFontDlgBar.SetDlgItemInt(IDC_EDIT_TEXT_TIMELIMIT, cTextPtr->m_timeLength);
	} // end a collection is selected

	if (selectionLevel == 2) { // a text entry is selected
		HTREEITEM parent = TreeView_GetParent(treeHwnd, item);
		int parentIndex = GetTreeChildIndex(treeHwnd, parent);
		POSITION posLoc = m_pTCL->FindIndex(parentIndex);
		if (!posLoc) {
			return;
		}

		CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);

		posLoc = cTextPtr->m_textEntryList->FindIndex(universalIndex);
		if (!posLoc) {
			return;
		}

		CTextEntry *entryPtr = (CTextEntry *)cTextPtr->m_textEntryList->GetAt(posLoc);

		m_textFontDlgBar.SetDlgItemInt(IDC_EDIT_TEXT_PERCX, entryPtr->m_percX);
		m_textFontDlgBar.SetDlgItemInt(IDC_EDIT_TEXT_PERCY, entryPtr->m_percY);
		m_textFontDlgBar.SetDlgItemText(IDC_EDIT_TEXT_DATA, Utf8ToUtf16(entryPtr->m_text).c_str());
	} // end a text entry is selected
}

void CKEPEditView::OnBTNtextApplyFont() {
	int fontIndex = m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_FONTS, LB_GETCURSEL, 0, 0);
	if (fontIndex == -1) {
		AfxMessageBox(IDS_MSG_SELFONT);
		return;
	}

	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1 || selectionLevel != 1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int collectionIndex = GetTreeChildIndex(treeHwnd, item);

	POSITION posLoc = m_pTCL->FindIndex(collectionIndex);
	if (!posLoc) {
		return;
	}

	CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);
	cTextPtr->m_fontSel = fontIndex;
}

void CKEPEditView::OnBUTTONtextAddFont() {
	int szX = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_SIZEX, NULL, TRUE);
	int szY = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_SIZEY, NULL, TRUE);
	AddFont(szX, szY);
}

void CKEPEditView::OnBTNtextApplySizeChange() {
	int fontIndex = m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_FONTS, LB_GETCURSEL, 0, 0);
	if (fontIndex == -1) {
		AfxMessageBox(IDS_MSG_SELFONT);
		return;
	}

	POSITION position = m_pFOL->FindIndex(fontIndex);
	if (!position) {
		AfxMessageBox(IDS_MSG_SELFONTVALID);
		return;
	}

	CFontObj *fPtr = (CFontObj *)m_pFOL->GetAt(position);
	fPtr->m_sizeX = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_SIZEX, NULL, TRUE);
	fPtr->m_sizeY = m_textFontDlgBar.GetDlgItemInt(IDC_EDIT_TEXT_SIZEY, NULL, TRUE);
	DeleteObject(fPtr->m_fontObject); // standard font
	fPtr->m_fontObject = CreateFont((int)(fPtr->m_sizeX),
		(int)(fPtr->m_sizeY),
		0,
		0,
		1,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		PROOF_QUALITY,
		VARIABLE_PITCH,
		Utf8ToUtf16(fPtr->m_fontName).c_str());
}

void CKEPEditView::OnBUTTONtextDeleteFont() {
	int fontIndex = m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_FONTS, LB_GETCURSEL, 0, 0);
	if (fontIndex == -1) {
		AfxMessageBox(IDS_MSG_SELFONT);
		return;
	}

	POSITION position = m_pFOL->FindIndex(fontIndex);
	if (!position) {
		AfxMessageBox(IDS_MSG_SELFONTVALID);
		return;
	}

	CFontObj *fPtr = (CFontObj *)m_pFOL->GetAt(position);
	m_pFOL->RemoveAt(position);
	DeleteObject(fPtr->m_fontObject);
	delete fPtr;
	fPtr = 0;

	m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_FONTS, LB_DELETESTRING, fontIndex, 0);
}

void CKEPEditView::OnBUTTONtextDeleteTextCollection() {
	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1 || selectionLevel != 1) {
		AfxMessageBox(IDS_MSG_SELDISPLAYCOLLECTION);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int collectionIndex = GetTreeChildIndex(treeHwnd, item);

	POSITION posLoc = m_pTCL->FindIndex(collectionIndex);
	if (!posLoc) {
		return;
	}

	CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);
	for (POSITION posLoc2 = cTextPtr->m_textEntryList->GetHeadPosition(); posLoc2 != NULL;) {
		CTextEntry *entryPtr = (CTextEntry *)cTextPtr->m_textEntryList->GetNext(posLoc2);
		delete entryPtr;
		entryPtr = 0;
	}

	cTextPtr->m_textEntryList->RemoveAll();
	delete cTextPtr->m_textEntryList;
	cTextPtr->m_textEntryList = 0;
	m_pTCL->RemoveAt(posLoc);
	delete cTextPtr;
	cTextPtr = 0;
	DelTreeNode(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, item);
}

void CKEPEditView::OnBUTTONtextDeleteTextObject() {
	HWND treeHwnd = NULL;
	int selectionLevel = GetTreeSelectionLevel(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	if (selectionLevel == -1 || selectionLevel == 1) {
		AfxMessageBox(IDS_MSG_SELENTRY);
		return;
	}

	m_textFontDlgBar.GetDlgItem(IDC_TREE_TEXTDISPLAYS, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	int universalIndex = GetTreeChildIndex(treeHwnd, item);

	HTREEITEM parent = TreeView_GetParent(treeHwnd, item);
	int parentIndex = GetTreeChildIndex(treeHwnd, parent);
	POSITION posLoc = m_pTCL->FindIndex(parentIndex);
	if (!posLoc) {
		return;
	}

	CTextCollection *cTextPtr = (CTextCollection *)m_pTCL->GetAt(posLoc);

	posLoc = cTextPtr->m_textEntryList->FindIndex(universalIndex);
	if (!posLoc) {
		return;
	}

	CTextEntry *entryPtr = (CTextEntry *)cTextPtr->m_textEntryList->GetAt(posLoc);
	cTextPtr->m_textEntryList->RemoveAt(posLoc);
	delete entryPtr;
	entryPtr = 0;

	DelTreeNode(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, item);
}

void CKEPEditView::OnBTNtestSave() {
	BOOL newFile = m_textFontDlgBar.GetLoadedFile() == "";
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Fonts Files (*.etf)|*.ETF|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"ETF", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		if (SaveTextFontInterfaceFromFile(fileName)) {
			m_textFontDlgBar.SetLoadedFile(fileName);
			if (newFile) {
				//update script event
				CStringW sCaption, sText;
				sCaption.LoadString(IDS_CHANGE_SETTINGS);
				sText.LoadString(IDS_CONFIRM_ADDING_SCRIPT_EVENT);
				int ret = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNO);
				if (ret == IDYES) {
					m_textFontDlgBar.UpdateScript();
				}
			}
		}
	}
}

void CKEPEditView::OnBTNtestLoad() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Fonts Files (*.etf)|*.ETF|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		if (LoadTextFontInterfaceFromFile(fileName)) {
			m_textFontDlgBar.SetLoadedFile(fileName);
		} else {
			AfxMessageBox(IDS_MSG_FAILED_LOADING_GAME_FILE, MB_ICONEXCLAMATION);
		}
	}
}

BOOL CKEPEditView::SaveTextFontInterfaceFromFile(CStringA fileName) {
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, fileName, CFile::modeCreate | CFile::modeWrite, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION(exc, m_logger)
		return FALSE;
	}
	BEGIN_SERIALIZE_TRY

	CArchive the_out_Archive(&fl, CArchive::store);
	the_out_Archive << m_pTCL << m_pFOL << m_cgFontList << m_symbolMapDB;
	the_out_Archive.Close();
	fl.Close();
	END_SERIALIZE_TRY(m_logger)

	return TRUE;
}

void CKEPEditView::TextFontInterfaceInlist() {
	DelTreeNode(IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, TVI_ROOT);
	TextDisplayTreeROOT = AddTreeRoot(L"Text Display Database", IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);
	InitAllFonts();
	for (POSITION posLoc = m_pTCL->GetHeadPosition(); posLoc != NULL;) { // loopB
		CTextCollection *collectionPtr = (CTextCollection *)m_pTCL->GetNext(posLoc);
		HTREEITEM root = AddTreeChild(Utf8ToUtf16(collectionPtr->m_collectionName).c_str(), IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, TextDisplayTreeROOT);
		for (POSITION posLoc2 = collectionPtr->m_textEntryList->GetHeadPosition(); posLoc2 != NULL;) { // loopC
			CTextEntry *txtItemPtr = (CTextEntry *)collectionPtr->m_textEntryList->GetNext(posLoc2);
			AddTreeChild(Utf8ToUtf16(txtItemPtr->m_text).c_str(), IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar, root);
		} // loopC
	} // loopB
}

void CKEPEditView::OnBTNcamVportSave() {
	BOOL newFile = m_camVptDlgBar.GetLoadedFile() == "";
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Cam/View Files (*.evc)|*.EVC|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EVC", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << m_pCOL << m_pVL;
		the_out_Archive.Close();
		fl.Close();
		m_camVptDlgBar.SetLoadedFile(fileNameW);
		END_SERIALIZE_TRY(m_logger)

		if (m_camVptDlgBar.GetLoadedFile() != "" && newFile) {
			//update script event
			CStringW sCaption, sText;
			sCaption.LoadString(IDS_CHANGE_SETTINGS);
			sText.LoadString(IDS_CONFIRM_ADDING_SCRIPT_EVENT);
			int ret = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNO);
			if (ret == IDYES) {
				m_camVptDlgBar.UpdateScript();
			}
		}
	}
}

void CKEPEditView::OnBTNcamVportLoad() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Cam/View Files (*.evc)|*.EVC|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();

		CStringA oldFile = m_cameraViewFilePtr->GetFileName().c_str(); //previous loaded file
		if (LoadVportCameraConfigurations(fileName)) {
			m_camVptDlgBar.SetLoadedFile(fileName);
		}
	}
}

void CKEPEditView::OnBTNDeleteCamera() {
	int cameraIndex = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_GETCURSEL, 0, 0);
	if (cameraIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCAMERA);
		return;
	}

	POSITION posLoc = m_pCOL->FindIndex(cameraIndex);
	if (!posLoc) {
		return;
	}

	CCameraObj *camLoc = (CCameraObj *)m_pCOL->GetAt(posLoc);
	m_pCOL->RemoveAt(posLoc);
	delete camLoc;
	camLoc = 0;
	m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_DELETESTRING, cameraIndex, 0);
	m_camVptDlgBar.SetSettingsChanged(TRUE);
}

void CKEPEditView::OnBTNDeleteViewport() {
	int vportIndex = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (vportIndex == -1) {
		AfxMessageBox(IDS_MSG_SELVIEWPORT);
		return;
	}

	POSITION posLoc = m_pVL->FindIndex(vportIndex);
	if (posLoc) {
		CViewportObj *vpLoc = (CViewportObj *)m_pVL->GetAt(posLoc);
		m_pVL->RemoveAt(posLoc);
		delete vpLoc;
		vpLoc = 0;

		m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_DELETESTRING, vportIndex, 0);
		m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_SETCURSEL, vportIndex - 1, 0);

		OnSelchangeViewports();
	}
}

void CKEPEditView::OnBTNUpdateViewport() {
	int vportIndex = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (vportIndex == -1) {
		AfxMessageBox(IDS_MSG_SELVIEWPORT);
		return;
	}

	POSITION posLoc = m_pVL->FindIndex(vportIndex);
	if (posLoc) {
		CViewportObj *viewport = (CViewportObj *)m_pVL->GetAt(posLoc);
		if (viewport) {
			CStringA new_name = m_camVptDlgBar.GetViewportName();

			if (new_name != viewport->GetName()) {
				viewport->SetName(new_name);

				// replace the name of the viewport in the list
				if (LB_ERR != m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_DELETESTRING, vportIndex, 0))
					if (LB_ERR != m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_INSERTSTRING, vportIndex, (LPARAM)Utf8ToUtf16(new_name).c_str()))
						m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_SETCURSEL, vportIndex, 0);
			}

			viewport->m_Y1 = m_camVptDlgBar.GetPercentTop();
			viewport->m_X1 = m_camVptDlgBar.GetPercentLeft();
			viewport->m_Y2 = m_camVptDlgBar.GetPercentBottom();
			viewport->m_X2 = m_camVptDlgBar.GetPercentRight();
		}
	}
}

void CKEPEditView::OnBTNUpdateRuntimeViewport() {
	int runtime_viewport_index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (LB_ERR != runtime_viewport_index) {
		int runtime_camera_id = m_camVptDlgBar.GetRuntimeViewportRuntimeCameraID();
		int runtime_viewport_id = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_GETITEMDATA, runtime_viewport_index, 0);

		m_camVptDlgBar.SetCameraBinding(runtime_viewport_id, runtime_camera_id);

		BindCameraToViewportById(runtime_camera_id, runtime_viewport_id);
	}
}

void CKEPEditView::OnBTNEXPAddExplosion() {
#ifndef _ClientOutput
	CExplosionBasicDlg dlg;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.m_dirSndRef = m_pDirectSound;

	CExplosionObj *newExpObj = new CExplosionObj();
	dlg.m_explosionRef = newExpObj;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_elementDBREF = m_elementDB;
	dlg.m_skillDatabaseRef = m_skillDatabase;
	if (dlg.DoModal() == IDOK) {
		m_explosionDBList->AddTail(newExpObj);
		InListExplosionDB((m_explosionDBList->GetCount() - 1));
		m_explosionDlgBar.SetSettingsChanged(TRUE);
		if (newExpObj->m_meshObject) {
			//			ReinitTextureUsage ( newExpObj->m_meshObject );
		}
	} else {
		newExpObj->SafeDelete();
		delete newExpObj;
		newExpObj = 0;
	}

#endif
}

void CKEPEditView::InListExplosionDB(int sel) {
	if (m_editorModeEnabled == FALSE) {
		return;
	}

	int traceIt = 0;
	m_explosionDlgBar.SendDlgItemMessage(IDC_LIST_EXPLOSIONLIST, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_explosionDBList->GetHeadPosition(); posLoc != NULL;) {
		CExplosionObj *partPtr = (CExplosionObj *)m_explosionDBList->GetNext(posLoc);
		stringstream strBuilder;
		strBuilder << traceIt << " " << partPtr->m_explosionName;

		m_explosionDlgBar.SendDlgItemMessage(IDC_LIST_EXPLOSIONLIST, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
		traceIt++;
	}

	if (sel != -1) {
		m_explosionDlgBar.SendDlgItemMessage(IDC_LIST_EXPLOSIONLIST, LB_SETCURSEL, sel, 0);
	}
}

void CKEPEditView::OnResetAll() {
	DestroyEnvironment();
	DestroyWorldObjects();

	DestroyEntityRuntimeList();

	InListRuntimeEntities(0);
	if (GL_collisionBase) {
		GL_collisionBase->SafeDelete();
		delete GL_collisionBase;
		GL_collisionBase = 0;
	}

	GL_collisionBase = new CCollisionObj();
	m_pWOL = new CWldObjectList();
	m_environment = new CEnvironmentObj();
	InListTextureDB(0);
}

void CKEPEditView::InListTextureDB(const DWORD &nameHash) {
}

void CKEPEditView::OnBTNCameraAdvanced() {
#ifndef _ClientOutput
	CCameraAdvDlg dlg;
	int cameraIndex = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_GETCURSEL, 0, 0);
	if (cameraIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCAMERA);
		return;
	}

	// refrence the camera and vport
	POSITION posCamLoc = m_pCOL->FindIndex(cameraIndex);
	if (!posCamLoc) {
		AfxMessageBox(IDS_ERROR_NOCAMERAINCAMERADB);
		return;
	}

	CCameraObj *camPtr = (CCameraObj *)m_pCOL->GetAt(posCamLoc);

	dlg.m_camera_name = camPtr->m_cameraName;
	dlg.m_camera_mode = camPtr->m_camera_mode;

	dlg.m_InitialPosX = camPtr->m_InitialPosX;
	dlg.m_InitialPosY = camPtr->m_InitialPosY;
	dlg.m_InitialPosZ = camPtr->m_InitialPosZ;
	dlg.m_floatingMovePwr = camPtr->m_floatingMovePwr;
	dlg.m_clipPlane = camPtr->m_clipPlane;
	dlg.m_fov = camPtr->m_fovView;
	dlg.cameraTypeInt = camPtr->m_type;
	dlg.m_useCollision = camPtr->m_useCollision;

	dlg.m_orbitDefAzi = ToDegrees(camPtr->m_orbitAziInit);
	dlg.m_orbitDefEle = ToDegrees(camPtr->m_orbitEleInit);
	dlg.m_orbitMinDist = camPtr->m_orbitMinDist;
	dlg.m_orbitMaxDist = camPtr->m_orbitMaxDist;
	dlg.m_orbitStartDist = camPtr->m_orbitStartDist;
	dlg.m_orbitDistStep = camPtr->m_orbitDistStep;

	dlg.m_orbitFocusX = camPtr->m_orbitFocus.x;
	dlg.m_orbitFocusY = camPtr->m_orbitFocus.y;
	dlg.m_orbitFocusZ = camPtr->m_orbitFocus.z;
	dlg.m_orbitAutoFollow = camPtr->m_orbitAutoFollow;

	dlg.m_floatTargetPosX = camPtr->m_floatingCamTarget.x;
	dlg.m_floatTargetPosY = camPtr->m_floatingCamTarget.y;
	dlg.m_floatTargetPosZ = camPtr->m_floatingCamTarget.z;
	dlg.m_baseHeightAspect = camPtr->m_heightBaseAspect;
	dlg.m_pitchRotation = camPtr->m_pitchRotation;
	dlg.m_miscVal1 = camPtr->m_miscVal1;
	dlg.m_filterFirstPersonModeRef = camPtr->m_filterFirstPersonMode;
	dlg.m_script_type = camPtr->m_script_type;

	if (dlg.DoModal() == IDOK) {
		std::string newName = dlg.m_camera_name;
		// new name
		if (camPtr->m_cameraName != newName.c_str()) {
			camPtr->m_cameraName = newName.c_str();

			// update the name in the list
			//  need to remove and add back the item
			if (LB_ERR != m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_DELETESTRING, cameraIndex, 0))
				if (LB_ERR != m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_INSERTSTRING, cameraIndex, (LPARAM)Utf8ToUtf16(camPtr->m_cameraName).c_str()))
					m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_CAMERAS, LB_SETCURSEL, cameraIndex, 0);

			m_camVptDlgBar.SetSettingsChanged(TRUE);
		}

		camPtr->m_camera_mode = (CCameraObj::Camera_Mode)dlg.m_camera_mode;
		camPtr->m_filterFirstPersonMode = dlg.m_filterFirstPersonModeRef;
		camPtr->m_miscVal1 = dlg.m_miscVal1;
		camPtr->m_pitchRotation = dlg.m_pitchRotation;
		camPtr->m_heightBaseAspect = dlg.m_baseHeightAspect;

		camPtr->m_InitialPosX = dlg.m_InitialPosX;
		camPtr->m_InitialPosY = dlg.m_InitialPosY;
		camPtr->m_InitialPosZ = dlg.m_InitialPosZ;
		camPtr->m_floatingMovePwr = dlg.m_floatingMovePwr;
		camPtr->m_clipPlane = dlg.m_clipPlane;
		camPtr->m_fovView = dlg.m_fov;
		camPtr->m_type = dlg.cameraTypeInt;
		camPtr->m_useCollision = dlg.m_useCollision;

		camPtr->m_floatingCamTarget.x = dlg.m_floatTargetPosX;
		camPtr->m_floatingCamTarget.y = dlg.m_floatTargetPosY;
		camPtr->m_floatingCamTarget.z = dlg.m_floatTargetPosZ;
		camPtr->m_script_type = dlg.m_script_type;

		camPtr->m_orbitAziInit = dlg.m_orbitDefAzi * D3DX_PI / 180.0f;
		camPtr->m_orbitEleInit = dlg.m_orbitDefEle * D3DX_PI / 180.0f;
		camPtr->m_orbitMinDist = dlg.m_orbitMinDist;
		camPtr->m_orbitMaxDist = dlg.m_orbitMaxDist;
		camPtr->m_orbitStartDist = dlg.m_orbitStartDist;
		camPtr->m_orbitDistStep = dlg.m_orbitDistStep;
		camPtr->m_orbitFocus.x = dlg.m_orbitFocusX;
		camPtr->m_orbitFocus.y = dlg.m_orbitFocusY;
		camPtr->m_orbitFocus.z = dlg.m_orbitFocusZ;
		camPtr->m_orbitAutoFollow = (dlg.m_orbitAutoFollow != 0); // stupid bool perf warnings

		m_pCOL->SetDirty();
	}

#endif
}

void CKEPEditView::OnBTNMovieAddMovie() {
}

void CKEPEditView::OnBTNMovieTestMovie() {
}

void CKEPEditView::OnBTNMovieEditMovie() {
}

void CKEPEditView::OnBTNMovieDeleteMovie() {
}

void CKEPEditView::OnBTNMoviesSaveDB() {
}

void CKEPEditView::OnBTNMoviesLoadDB() {
}

void CKEPEditView::OnBTNMovieStopMovies() {
}

void CKEPEditView::AddControlConfig(CStringA configName, ControlObjType const &controlType) {
	ControlDBObj *cPtr = new ControlDBObj(controlType, configName, NULL);
	cPtr->m_controlInfoList = new ControlInfoList;
	m_controlsList->AddTail(cPtr);
}

void CKEPEditView::AddControllerEvent(ControlDBObj *controller,
	ControlObjType const &controlType,
	int keyLoc,
	Command_Action const &eventAction,
	CStringA description,
	float miscFloat1,
	float miscFloat2,
	float miscFloat3) {
	ControlInfoObj *cPtr = new ControlInfoObj(controlType, keyLoc, eventAction, description, miscFloat1, miscFloat2, miscFloat3);
	controller->m_controlInfoList->AddTail(cPtr);
}

void CKEPEditView::OnBTNControlAddObject() {
#ifndef _ClientOutput
	CControlAddObjectDlg dlg;
	dlg.controlType = CO_TYPE_KEYBOARD;
	dlg.m_name = "Default";
	if (dlg.DoModal() == IDOK) {
		AddControlConfig(dlg.m_name, dlg.controlType);
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(dlg.m_name).c_str());
	}

#endif
}

void CKEPEditView::OnBTNControlAddEvent() {
#ifndef _ClientOutput
	int objectIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_GETCURSEL, 0, 0);
	if (objectIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCONTROLOBJECT);
		return;
	}

	POSITION posLoc = m_controlsList->FindIndex(objectIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlDBObj *ptrLoc = (ControlDBObj *)m_controlsList->GetAt(posLoc);
	CAddControlEventDlg dlg;
	if (dlg.DoModal() == IDOK) {
		AddControllerEvent(ptrLoc,
			dlg.controlType,
			dlg.finalKeyNum,
			dlg.finalActionNum,
			dlg.m_comboActions,
			dlg.m_miscFloat1,
			dlg.m_miscFloat2,
			dlg.m_miscFloat3);
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(dlg.m_comboActions).c_str());
	}

#endif
}

void CKEPEditView::OnBTNControlCfgSave() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Controls Files (*.cnt)|*.CNT|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"CNT", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		SaveControlConfig(fileName);
	}
}

void CKEPEditView::OnBTNControlCfgLoad() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Controls Files (*.cnt)|*.CNT|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		LoadControlCfgList(fileName);
	}
}

void CKEPEditView::OnBTNControlDeleteObject() {
	int objectIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_GETCURSEL, 0, 0);
	if (objectIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCONTROLOBJECT);
		return;
	}

	POSITION posLoc = m_controlsList->FindIndex(objectIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlDBObj *ptrLoc = (ControlDBObj *)m_controlsList->GetAt(posLoc);
	delete ptrLoc;
	ptrLoc = 0;
	m_controlsList->RemoveAt(posLoc);
	m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_DELETESTRING, objectIndex, 0);
}

void CKEPEditView::OnBTNControlDeleteEvent() {
	int objectIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_GETCURSEL, 0, 0);
	if (objectIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCONTROLOBJECT);
		return;
	}

	int eventIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_GETCURSEL, 0, 0);
	if (eventIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCONTROLEVENT);
		return;
	}

	POSITION posLoc = m_controlsList->FindIndex(objectIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlDBObj *ptrLoc = (ControlDBObj *)m_controlsList->GetAt(posLoc);
	posLoc = ptrLoc->m_controlInfoList->FindIndex(eventIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlInfoObj *infLoc = (ControlInfoObj *)ptrLoc->m_controlInfoList->GetAt(posLoc);
	delete infLoc;
	infLoc = 0;
	ptrLoc->m_controlInfoList->RemoveAt(posLoc);
	m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_DELETESTRING, eventIndex, 0);
}

void CKEPEditView::InListControlEvents() {
	m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_RESETCONTENT, 0, 0);

	int objectIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_GETCURSEL, 0, 0);
	if (objectIndex == -1) {
		return;
	}

	POSITION posLoc = m_controlsList->FindIndex(objectIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlDBObj *conPtr = (ControlDBObj *)m_controlsList->GetAt(posLoc);
	for (POSITION posLoc2 = conPtr->m_controlInfoList->GetHeadPosition(); posLoc2 != NULL;) { // info loop
		ControlInfoObj *infoPtr = (ControlInfoObj *)conPtr->m_controlInfoList->GetNext(posLoc2);
		CStringA actionText = (infoPtr->m_description + ": " + infoPtr->GetButtonString());
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(actionText).c_str());
	}
}

void CKEPEditView::OnSelchangeLISTControlDB() {
	InListControlEvents();
}

void CKEPEditView::OnBTNControlEditObject() {
#ifndef _ClientOutput
	int objectIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_GETCURSEL, 0, 0);
	if (objectIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCONTROLOBJECT);
		return;
	}

	POSITION posLoc = m_controlsList->FindIndex(objectIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlDBObj *ptrLoc = (ControlDBObj *)m_controlsList->GetAt(posLoc);
	CControlAddObjectDlg dlg;
	dlg.GL_invertMouse = ptrLoc->m_invertMouse;
	dlg.controlType = ptrLoc->m_controlType;
	dlg.m_name = ptrLoc->m_controlConfigName;
	if (dlg.DoModal() == IDOK) {
		ptrLoc->m_controlType = dlg.controlType;
		ptrLoc->m_controlConfigName = dlg.m_name;
		ptrLoc->m_invertMouse = dlg.GL_invertMouse;
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_DELETESTRING, objectIndex, 0);
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_INSERTSTRING,
			objectIndex, (LPARAM)Utf8ToUtf16(ptrLoc->m_controlConfigName).c_str());

		m_controlsList->SetDirty();
	}

#endif
}

void CKEPEditView::OnBTNControlEditEvent() {
#ifndef _ClientOutput
	int objectIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_GETCURSEL, 0, 0);
	if (objectIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCONTROLOBJECT);
		return;
	}

	int eventIndex = m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_GETCURSEL, 0, 0);
	if (eventIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCONTROLEVENT);
		return;
	}

	POSITION posLoc = m_controlsList->FindIndex(objectIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlDBObj *ptrLoc = (ControlDBObj *)m_controlsList->GetAt(posLoc);
	posLoc = ptrLoc->m_controlInfoList->FindIndex(eventIndex);
	if (posLoc == NULL) {
		return; // didnt find index
	}

	ControlInfoObj *infLoc = (ControlInfoObj *)ptrLoc->m_controlInfoList->GetAt(posLoc);
	CAddControlEventDlg dlg;
	dlg.m_miscFloat1 = infLoc->m_miscFloat1;
	dlg.m_miscFloat2 = infLoc->m_miscFloat2;
	dlg.m_miscFloat3 = infLoc->m_miscFloat3;
	dlg.finalKeyNum = infLoc->m_buttonDown;
	dlg.controlType = infLoc->m_controlType;
	dlg.finalActionNum = infLoc->m_action;
	dlg.m_comboActions = ControlInfoList::ReturnActionByIndex(infLoc->m_action);
	dlg.m_comboKeyButton = infLoc->GetButtonString();

	if (dlg.DoModal() == IDOK) {
		infLoc->m_miscFloat1 = dlg.m_miscFloat1;
		infLoc->m_miscFloat2 = dlg.m_miscFloat2;
		infLoc->m_miscFloat3 = dlg.m_miscFloat3;
		infLoc->m_buttonDown = dlg.finalKeyNum;
		infLoc->m_controlType = dlg.controlType;
		infLoc->m_action = dlg.finalActionNum;

		infLoc->m_description = dlg.m_comboActions;
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_DELETESTRING, eventIndex, 0);
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLEVENTS, LB_INSERTSTRING, eventIndex, (LPARAM)Utf8ToUtf16(dlg.m_comboActions).c_str());

		m_controlsList->SetDirty();
	}

#endif
}

void CKEPEditView::OnBTNObjectArrayAdd() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();
	static wchar_t BASED_CODE filter[] = L"DirectX Mesh Files (*.x)|*.X|3ds Files (*.3ds)|*.3ds|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) { // file chosen
		CStringW baseNameW = opendialog.GetFileName();
		CStringA baseName = Utf16ToUtf8(baseNameW).c_str();

		// get first file then loop till done
		CStringA ext = CABFunctionLib::GetFileExt(baseName);
		ext = operator+(".", ext);
		CABFunctionLib::RemoveFileExt(&baseName);

		int curCount = 0;
		while (1) {
			CStringA fileName = baseName;
			if (curCount > 0) { // append if over 0
				stringstream strBuilder;
				strBuilder << baseName << (curCount + 1);
				fileName = strBuilder.str().c_str();
			}

			CABFunctionLib::AddNewFileExt(&fileName, ext);

			CWldObject *retBasisLoc = NULL;
			if (LoadWorldObject(fileName, &retBasisLoc) == FALSE) {
				break;
			}

			curCount++;
		}

		// ReinitAllTexturesFromDatabase();
	} // end file chosen
}

void CKEPEditView::OnBTNUIBaseAdvanced() {
#ifndef _ClientOutput
	CGlobalAdvancedDlg dlg;
	dlg.SetContactSelection(m_selectionModeOn == 1);
	dlg.SetInterpolationGlobal(m_globalShadowType);
	dlg.SetTargetFPS(m_fTargetSecondsPerFrame > 0 ? 1.0f / m_fTargetSecondsPerFrame : 1.0f);
	dlg.SetResolutionHeight(ResolutionHeight);
	dlg.SetResolutionWidth(ResolutionWidth);
	dlg.SetWBuffer(W_BufferPreffered == 1);
	dlg.SetCalcEntityCollision(m_entityCollisionOn == 1);
	dlg.m_shaderAvailable = m_shaderSystem;
	dlg.SetGlobalShaderID(m_globalWorldShader);
	if (dlg.DoModal() == IDOK) {
		W_BufferPreffered = dlg.GetWBuffer();
		m_globalWorldShader = dlg.GetGlobalShaderID();
		m_renderStateObject->SetVertexShader(g_pd3dDevice, m_shaderSystem, m_globalWorldShader);
		float fTargetFPS = (float)dlg.GetTargetFPS();
		m_fTargetSecondsPerFrame = fTargetFPS > 0 ? 1.0f / fTargetFPS : 1.0f;
		m_globalShadowType = dlg.GetInterpolationGlobal();
		ResolutionHeight = dlg.GetResolutionHeight();
		ResolutionWidth = dlg.GetResolutionWidth();
		m_selectionModeOn = dlg.GetContactSelection();
		m_entityCollisionOn = dlg.GetCalcEntityCollision();
		SetAllRenderStatesIM();
	}

#endif
}

void CKEPEditView::OnBTNENTITYProperties() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;

	// get selected index
	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);
	CEntityPropertiesDlg dlg(indexLoc);
	dlg.SetHardPointOverd(ptrLoc->m_hardPointOverdY);
	dlg.SetPopoutRadius(ptrLoc->m_popoutRadius);
	dlg.SetEnergy(ptrLoc->m_maxEnergy);
	dlg.SetModelType(ptrLoc->m_modelType);
	dlg.SetDeathCamIndex(ptrLoc->m_deathCam);
	dlg.SetHudIndex(ptrLoc->m_hudLink);
	dlg.SetMeshRadius(ptrLoc->m_unscaledBoundingRadius);
	dlg.SetMountBoneIndex(ptrLoc->m_ridable);
	dlg.SetTeam(ptrLoc->m_team);
	dlg.SetEntityName(ptrLoc->m_name);
	dlg.SetRaceID(ptrLoc->m_raceID);
	dlg.SetDisableCollision(ptrLoc->m_filterCollision == 1);
	dlg.SetSvrPhysics(ptrLoc->m_primaryPhysicsOverd);
	if (dlg.DoModal() == IDOK) {
		ptrLoc->m_primaryPhysicsOverd = dlg.GetSvrPhysics();
		ptrLoc->m_filterCollision = dlg.GetDisableCollision();
		ptrLoc->m_name = dlg.GetEntityName();
		ptrLoc->m_team = dlg.GetTeam();
		ptrLoc->m_ridable = dlg.GetMountBoneIndex();
		ptrLoc->m_unscaledBoundingRadius = dlg.GetMeshRadius();
		ptrLoc->m_boundingRadius = ptrLoc->m_unscaledBoundingRadius;
		ptrLoc->m_deathCam = dlg.GetDeathCamIndex();
		ptrLoc->m_maxEnergy = dlg.GetEnergy();
		ptrLoc->m_popoutRadius = dlg.GetPopoutRadius();
		ptrLoc->m_modelType = dlg.GetModelType();
		ptrLoc->m_hudLink = dlg.GetHudIndex();
		ptrLoc->m_raceID = dlg.GetRaceID();
		ptrLoc->m_hardPointOverdY = dlg.GetHardPointOverd();
	}

	// Pass focus back to treeview
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES)->SetFocus();

#endif
}

void CKEPEditView::OnBTNOBJECTProperties() {
	WorldObjectProperties();
}

void CKEPEditView::WorldObjectProperties() {
#ifndef _ClientOutput
	CWldObject *ptrLoc = GetSelectedWldObjectFromTreeView();

	if (ptrLoc) {
		CWorldObjectPropertiesDlg dlg;
		dlg.g_pd3dDeviceRef = g_pd3dDevice;
		dlg.m_worldGroupListRef = worldGroupList;
		dlg.GL_textureDataBase = m_pTextureDB;
		dlg.m_wldObjectRef = ptrLoc;

		dlg.m_particlelink = ptrLoc->m_particleLink;
		dlg.m_haloLink = ptrLoc->m_haloLink;
		dlg.m_lensflareLink = ptrLoc->m_lensFlareType;

		if (ptrLoc->m_meshObject) {
			dlg.m_actualBoxSizeMaxX = ptrLoc->m_meshObject->GetBoundingBox().maxX;
			dlg.m_actualBoxSizeMaxY = ptrLoc->m_meshObject->GetBoundingBox().maxY;
			dlg.m_actualBoxSizeMaxZ = ptrLoc->m_meshObject->GetBoundingBox().maxZ;
			dlg.m_actualBoxSizeMinX = ptrLoc->m_meshObject->GetBoundingBox().minX;
			dlg.m_actualBoxSizeMinY = ptrLoc->m_meshObject->GetBoundingBox().minY;
			dlg.m_actualBoxSizeMinZ = ptrLoc->m_meshObject->GetBoundingBox().minZ;
		} else if (ptrLoc->m_node) {
			POSITION nodeLoc = m_libraryReferenceDB->FindIndex(ptrLoc->m_node->m_libraryReference);
			CReferenceObj *refObj = 0;
			if (nodeLoc)
				refObj = (CReferenceObj *)m_libraryReferenceDB->GetAt(nodeLoc);

			if (refObj) {
				// modify to use remesh
				//ABBOX bbox;
				//refObj->m_meshObject->GetBoxWithMarix( ptrLoc->m_node->m_position, &bbox );
				//dlg.m_actualBoxSizeMaxX = bbox.maxX;
				//dlg.m_actualBoxSizeMaxY = bbox.maxY;
				//dlg.m_actualBoxSizeMaxZ = bbox.maxZ;
				//dlg.m_actualBoxSizeMinX = bbox.minX;
				//dlg.m_actualBoxSizeMinY = bbox.minY;
				//dlg.m_actualBoxSizeMinZ = bbox.minZ;
			}
		}

		dlg.showBoundingBox = ptrLoc->m_showBox;

		dlg.m_popoutBoxSizeMaxX = ptrLoc->m_popoutBox.max.x;
		dlg.m_popoutBoxSizeMaxY = ptrLoc->m_popoutBox.max.y;
		dlg.m_popoutBoxSizeMaxZ = ptrLoc->m_popoutBox.max.z;
		dlg.m_popoutBoxSizeMinX = ptrLoc->m_popoutBox.min.x;
		dlg.m_popoutBoxSizeMinY = ptrLoc->m_popoutBox.min.y;
		dlg.m_popoutBoxSizeMinZ = ptrLoc->m_popoutBox.min.z;

		if (ptrLoc->m_meshObject) {
			dlg.m_meshRadius = ptrLoc->m_meshObject->GetBoundingSphereRadiusSqr();
		}

		dlg.m_popoutRadius = ptrLoc->m_popoutRadius;
		dlg.attachedToScene = ptrLoc->m_environmentAttached;
		dlg.m_spawnPoint = ptrLoc->m_usedAsSpawnPoint;
		dlg.removeEnvioronment = ptrLoc->m_removeEnviorment;
		dlg.m_objectGroup = ptrLoc->m_groupNumber;

		if (!ptrLoc->m_attribute) {
			ptrLoc->m_attribute = new CAttributeObj(0, 0, 0, 0, 0, FALSE, FALSE, FALSE, 200, 0);
		}

		dlg.autoRetract = ptrLoc->m_attribute->m_autoRetract;
		dlg.vicinityBased = ptrLoc->m_attribute->m_vicinityBased;
		dlg.m_otherAttributeType = ptrLoc->m_attribute->m_otherAttributeType;
		dlg.m_otherAttributeValue = ptrLoc->m_attribute->m_otherAttributeValue;
		dlg.m_otherObjectSelect = ptrLoc->m_attribute->m_otherObjectSelect;
		dlg.m_selfAttributeType = ptrLoc->m_attribute->m_selfAttributeType;
		dlg.m_selfAttributeValue = ptrLoc->m_attribute->m_selfAttributeValue;
		if (dlg.DoModal() == IDOK) {
			if (ptrLoc->m_meshObject) { // valid mesh
				ptrLoc->m_meshObject->SetBoundingSphereRadiusSqr(dlg.m_meshRadius);
				if (ptrLoc->m_meshObject->m_charona) {
					ptrLoc->m_meshObject->m_charona->InitializeVis();
					ptrLoc->m_meshObject->m_charona->m_visual->InitBuffer(g_pd3dDevice, ptrLoc->m_meshObject->m_charona->m_visual->m_uvCount);
				}
			} // end valid mesh

			ptrLoc->m_showBox = dlg.showBoundingBox;
			ptrLoc->m_popoutBox.max.x = dlg.m_popoutBoxSizeMaxX;
			ptrLoc->m_popoutBox.max.y = dlg.m_popoutBoxSizeMaxY;
			ptrLoc->m_popoutBox.max.z = dlg.m_popoutBoxSizeMaxZ;
			ptrLoc->m_popoutBox.min.x = dlg.m_popoutBoxSizeMinX;
			ptrLoc->m_popoutBox.min.y = dlg.m_popoutBoxSizeMinY;
			ptrLoc->m_popoutBox.min.z = dlg.m_popoutBoxSizeMinZ;

			ptrLoc->m_environmentAttached = dlg.attachedToScene;
			ptrLoc->m_removeEnviorment = dlg.removeEnvioronment;

			ptrLoc->m_popoutRadius = dlg.m_popoutRadius;
			ptrLoc->m_groupNumber = dlg.m_objectGroup;

			ptrLoc->m_attribute->m_autoRetract = dlg.autoRetract;
			ptrLoc->m_attribute->m_vicinityBased = dlg.vicinityBased;
			ptrLoc->m_attribute->m_otherAttributeType = dlg.m_otherAttributeType;
			ptrLoc->m_attribute->m_otherAttributeValue = dlg.m_otherAttributeValue;
			ptrLoc->m_attribute->m_otherObjectSelect = dlg.m_otherObjectSelect;
			ptrLoc->m_attribute->m_selfAttributeType = dlg.m_selfAttributeType;
			ptrLoc->m_attribute->m_selfAttributeValue = dlg.m_selfAttributeValue;
			ptrLoc->m_usedAsSpawnPoint = dlg.m_spawnPoint;

			ptrLoc->m_particleLink = dlg.m_particlelink;
			ptrLoc->m_haloLink = dlg.m_haloLink;
			ptrLoc->m_lensFlareType = dlg.m_lensflareLink;

			// ObjectTreeViewInlist(ptrLoc->m_groupNumber);
			for (POSITION posLocal = m_pWOL->GetHeadPosition(); posLocal != NULL;) { // loop through objects
				CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLocal);
				if (pWO->m_showBox == TRUE) {
					GL_showBoundingBoxs = TRUE;
				}
			} // end loop through objects

			// Reset Particle Effects
			for (POSITION objPos = m_pWOL->GetHeadPosition(); objPos != NULL;) {
				CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(objPos);
				if (pWO->m_runtimeParticleId) {
					m_particleRM->RemoveRuntimeEffect(pWO->m_runtimeParticleId);
					pWO->m_runtimeParticleId = 0;
				}
			}

			m_curWldObjPrtList->RemoveAll();

			if (m_particleRM)
				m_particleRM->ClearRuntimeEffects();

			EditorState::GetInstance()->SetZoneDirty();
		}
	} else
		AfxMessageBox(IDS_MSG_OBJECTNOTFOUND);
#endif
}

void CKEPEditView::OnBtnObjAddlight() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = reinterpret_cast<CWldObject *>(m_pWOL->GetNext(posLoc));
		if (!pWO)
			continue;
		if (textLoc == pWO->m_fileNameRef) {
			CLightInterfaceDlg dlg;
			dlg.GL_textureDataBase = m_pTextureDB;
			dlg.m_lightREF = pWO->m_lightObject;
			if (IDOK == dlg.DoModal()) {
				pWO->m_lightObject = dlg.m_lightREF;
				EditorState::GetInstance()->SetZoneDirty();
				InvalidateLightCaches();
			} // if..IDOK == dlg.DoModal()
		} // if..textLoc == pWorldObj->m_fileNameRef
	} // end Begin world object loop

	AfxMessageBox(IDS_MSG_OBJECTNOTFOUND);
#endif
}

void CKEPEditView::OnBTNOBJDeleteLight() {
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = reinterpret_cast<CWldObject *>(m_pWOL->GetNext(posLoc));
		if (!pWO)
			continue;
		if (textLoc == pWO->m_fileNameRef) {
			if (pWO->m_lightObject) {
				pWO->m_lightObject->SafeDelete();
				delete pWO->m_lightObject;
				pWO->m_lightObject = 0;
				EditorState::GetInstance()->SetZoneDirty();
				InvalidateLightCaches();
			} // if..pWorldObj->m_lightObject

			return;
		} // textLoc == pWorldObj->m_fileNameRef
	} // end Begin world object loop

	AfxMessageBox(IDS_MSG_OBJECTNOTFOUND);
}

void CKEPEditView::OnBTNENTAddLight() {
}

void CKEPEditView::OnBTNENTMergeData() {
#ifndef _ClientOutput
	CAddEntityDlg dlg;
	dlg.saveCollisionInfo = TRUE;
	if (dlg.DoModal() != IDOK) {
		return;
	}

	HWND treeHwnd = NULL;
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
	POSITION delPos;
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); (delPos = posLoc) != NULL;) { // visual loop
		CMovementObj *mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr)
			continue;

		if (temp == mPtr->m_name) {
			if (LoadActorFromDir(*mPtr, dlg.m_folderPath, dlg.m_fileNames, FALSE)) {
				InitFXShadersOnSkeletal(mPtr->m_runtimeSkeleton, mPtr->m_skeletonConfiguration);
				//				ReinitAllTexturesFromDatabase ();
				m_pTextureDB->TextureMapReinitAll();

				m_entityDlgBar.SetSettingsChanged(TRUE);
			} else {
				CStringW s;
				s.LoadString(IDS_ERROR_SKELETALLOADFAILED);
				s += (Utf8ToUtf16(dlg.m_folderPath) + L"[").c_str();
				for (UINT i = 0; i < dlg.m_fileNames.size(); i++)
					s += Utf8ToUtf16(dlg.m_fileNames[i]).c_str();
				AfxMessageBox(s);
			}
		}
	}

#endif
}

void CKEPEditView::OnBTNMusicAssignTrack() {
}

void CKEPEditView::OnBTNMusicAssignWav() {
	GetDlgItemTextUtf8(&m_musicDlgBar, IDC_EDIT_MUSIC_WAVFILE, m_musicLoopFile);
	//InitiateMusicLoop ( musicLoopFile ); // removed
}

void CKEPEditView::OnBTNMusicUnassignTrack() {
}

void CKEPEditView::OnBTNMusicUnassignWav() {
	m_musicLoopFile = "NONE";
	//InitiateMusicLoop ( "NONE" ); // removed
	m_musicDlgBar.SetDlgItemText(IDC_EDIT_MUSIC_WAVFILE, L"NONE");
}

void CKEPEditView::OnBTNMusicBrowseWav() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"Sounds").c_str();
	static wchar_t BASED_CODE filter[] = L"Waveform Audio Files (*.wav)|*.wav|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		UpdateData(TRUE);

		CStringW fileNameW = opendialog.GetFileName();
		m_musicDlgBar.SetDlgItemText(IDC_EDIT_MUSIC_WAVFILE, fileNameW);
		UpdateData(FALSE);
	}
}
// End Function
void CKEPEditView::MissileDatabaseInList(int sel) {
	if (!m_editorModeEnabled) {
		return;
	}

	m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_RESETCONTENT, 0, 0);

	for (POSITION posLoc = MissileDB->GetHeadPosition(); posLoc != NULL;) {
		CMissileObj *misPtr = (CMissileObj *)MissileDB->GetNext(posLoc);
		m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(misPtr->m_missileName).c_str());
	}

	if (sel > -1) {
		m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_SETCURSEL, sel, 0);
		m_missileDlgBar.SetPreviousSelection(sel);
	}

	LoadSelectedMissile();
}

void CKEPEditView::LoadSelectedMissile() {
	CMissileObj *misPtr = GetSelectedMissileObj();
	if (misPtr) {
		m_missileDlgBar.SetMissileObj(misPtr);
		m_missileDlgBar.LoadMissleObj();
	}

	m_missileDlgBar.RedrawPropertiesCtrls();
}

CMissileObj *CKEPEditView::GetSelectedMissileObj() {
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	return GetMissileObj(missileIndex);
}

CMissileObj *CKEPEditView::GetMissileObj(int selection) {
	POSITION posLoc = MissileDB->FindIndex(selection);
	if (!posLoc) {
		return 0;
	}
	return (CMissileObj *)MissileDB->GetAt(posLoc);
}

void CKEPEditView::OnBTNMISBrowseFileName() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();
	static wchar_t BASED_CODE filter[] = L"DirectX Mesh Files (*.x)|*.X|3ds Files (*.3ds)|*.3ds|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		UpdateData(TRUE);

		CStringW fileNameW = opendialog.GetPathName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		CEXMeshObj::ConvertToX(&fileName);
		// NOT USED IN DLG?		m_missileDlgBar.SetDlgItemText ( IDC_EDIT_MIS_FILENAME, fileName );
		UpdateData(FALSE);
	}
}

void CKEPEditView::OnBTNMISsave() {
#ifndef _LimitedEngine
	BOOL newFile = m_missileDlgBar.GetLoadedFile() == "";
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Projectile Files (*.mis)|*.MIS|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"MIS", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}
		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << MissileDB;
		the_out_Archive.Close();
		fl.Close();
		m_missileDlgBar.SetLoadedFile(fileName);

		CFile fl2;
		if (!fl2.Open(fileNameW + L".Server", CFile::modeCreate | CFile::modeWrite, &exc)) {
			LOG_EXCEPTION(exc, m_logger)
		} else {
			::ServerSerialize = true;
			CArchive the_out_Archive(&fl2, CArchive::store);
			the_out_Archive << MissileDB;
			the_out_Archive.Close();
			fl2.Close();
		}
		END_SERIALIZE_TRY(m_logger)
		::ServerSerialize = false;

		if (m_missileDlgBar.GetLoadedFile() != "" && newFile) {
			//update script event
			CStringW sCaption, sText;
			sCaption.LoadString(IDS_CHANGE_SETTINGS);
			sText.LoadString(IDS_CONFIRM_ADDING_SCRIPT_EVENT);
			int ret = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNO);
			if (ret == IDYES) {
				m_missileDlgBar.UpdateScript();
			}
		}
	}
#endif
}

void CKEPEditView::OnBTNMISLoad() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Projectile Files (*.mis)|*.MIS|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		if (LoadMissileDatabase(fileName.GetString())) {
			m_missileDlgBar.SetLoadedFile(fileName);
		}
	}
}

void CKEPEditView::OnBTNBHAddHole() {
}

void CKEPEditView::OnBTNBHBrowse() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();
	static wchar_t BASED_CODE filter[] = L"DirectX Mesh Files (*.x)|*.X|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		UpdateData(TRUE);

		CStringW fileNameW = opendialog.GetFileName();
		m_bulletHoleDlgBar.SetDlgItemText(IDC_EDIT_BH_FILENAME, fileNameW);
		UpdateData(FALSE);
	}
}

void CKEPEditView::OnBTNBHAlphaTexture() {
}

void CKEPEditView::OnBTNBHDeleteHole() {
}

void CKEPEditView::OnBTNBHMaterial() {
}

void CKEPEditView::OnBTNBHSave() {
}

void CKEPEditView::OnBTNBHload() {
}

void CKEPEditView::OnBTNEntityshadow() {
}

void CKEPEditView::OnBTNObjectShadow() {
	// waiting on new shadow code
}

void CKEPEditView::OnBTNENTAnimProperties() {
}

void CKEPEditView::OnBTNEntitySound() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	HTREEITEM ObjectRoot = TreeView_GetParent(treeHwnd, item);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, ObjectRoot)).c_str();
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!ptrLoc)
			continue;

		if (textLoc == ptrLoc->m_name) {
			return;
		}
	} // end Begin world object loop

	AfxMessageBox(IDS_ERROR_SELOBJECTINVALID);
#endif
}

void CKEPEditView::OnBTNEntitysetSoundCenter() {
}

void CKEPEditView::OnBTNOBJSound() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);

		if (!pWO)
			continue;
		if (textLoc == pWO->m_fileNameRef) {
			CSoundSettingsDlg dlg;
			dlg.enabled = pWO->m_useSound;
			if (pWO->m_soundRange) {
				dlg.SetID(pWO->m_soundRange->m_id);
			} else {
				int snd_id = ((IClientEngine *)IClientEngine::Instance())->GetSoundManager()->GetID(pWO->m_soundFileName);
				dlg.SetID(snd_id);
			}

			dlg.m_distanceFactor = pWO->m_soundFactor;
			if (dlg.DoModal() == IDOK) {
				if (pWO->m_soundRange)
					pWO->m_soundRange->m_id = dlg.GetID();
				else
					pWO->m_soundFileName = dlg.GetFilename();

				pWO->m_useSound = dlg.enabled;
				pWO->m_soundFactor = dlg.m_distanceFactor;

				EditorState::GetInstance()->SetZoneDirty();
			}

			return;
		}
	} // end Begin world object loop

	AfxMessageBox(IDS_MSG_OBJECTNOTFOUND);
#endif
}

void CKEPEditView::OnBTNOBJAlphaTexture() {
}

void CKEPEditView::OnBTNENTAnimSound() {
}

void CKEPEditView::OnBTNEntityCombineEntityVis() {
}

void CKEPEditView::OnBTNENTContentProperties() {
}

void CKEPEditView::OnBTNTEXAddCollection() {
#ifndef _ClientOutput
	CAnimTextureSwpAddDlg dlg;
	dlg.blueSrc = 0.0f;
	dlg.redSrc = 0.0f;
	dlg.greenSrc = 0.0f;
	dlg.m_howManyTextures = 0;
	dlg.m_alphaMapSize = 64;
	if (dlg.DoModal() == IDOK) {
		if (dlg.m_howManyTextures == 0) {
			return;
		}

		AddTextureSwapCollection(dlg.m_textureName,
			dlg.m_howManyTextures,
			dlg.useAlphaTexture,
			dlg.useDecalTransparency,
			dlg.m_alphaMapSize,
			dlg.redSrc,
			dlg.greenSrc,
			dlg.blueSrc,
			dlg.m_delay,
			dlg.m_alphaChannel);
		if (m_editorModeEnabled == TRUE) {
			m_animatedTextureDBDlgBar.SendDlgItemMessage(IDC_LIST_TEX_COLLECTION,
				LB_ADDSTRING,
				0,
				(LPARAM)Utf8ToUtf16("Map Basis :: " + dlg.m_textureName).c_str());
		}
	}

#endif
}

void CKEPEditView::OnBTNTEXDeleteCollection() {
	int runtimeIndex = m_animatedTextureDBDlgBar.SendDlgItemMessage(IDC_LIST_TEX_COLLECTION, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCOLLECTION);
		return;
	}

	POSITION posLocal = TextureSwapList->FindIndex(runtimeIndex);
	if (!posLocal) {
		return;
	}

	CTextureSwapObj *txtObjPtr = (CTextureSwapObj *)TextureSwapList->GetAt(posLocal);
	DestroyTextureCollection(txtObjPtr);
	if (txtObjPtr) {
		delete txtObjPtr;
	}

	txtObjPtr = 0;
	TextureSwapList->RemoveAt(posLocal);
	if (m_editorModeEnabled == TRUE) {
		m_animatedTextureDBDlgBar.SendDlgItemMessage(IDC_LIST_TEX_COLLECTION, LB_DELETESTRING, runtimeIndex, 0);
	}
}

void CKEPEditView::OnBTNTEXLoadCollection() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Animation Texture Files (*.txa)|*.TXA|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		LoadAnimTextureSwapDB(fileName);
	}
}

void CKEPEditView::OnBTNTEXSaveCollection() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Animation Texture Files (*.txa)|*.TXA|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"TXA", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << TextureSwapList;
		the_out_Archive.Close();
		fl.Close();
	}
}

void CKEPEditView::OnBTNENTLoadDB() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Actor Database Files (*.edb)|*.EDB|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		if (LoadEntityDatabase(fileName.GetString())) {
			m_entityDlgBar.SetLoadedFile(fileName);
		} else {
			AfxMessageBox(IDS_MSG_FAILED_LOADING_GAME_FILE, MB_ICONEXCLAMATION);
		}
	}
}

void CKEPEditView::OnBTNENTSaveDBExpanded() {
	BOOL newFile = m_entityDlgBar.GetLoadedFile() == "";
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Actor Database Files (*.edb)|*.EDB|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EDB", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		CFileException exc;
		CFile fl;
		if (!fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc))
			LOG_EXCEPTION(exc, m_logger)
		else {
			::ServerSerialize = false;

			BEGIN_SERIALIZE_TRY

			CArchive the_out_Archive(&fl, CArchive::store);

			// Modified by Jonny 04/02/08 save core loose EDB.
			m_movObjRefModelList->m_saveWholeEDB = false;
			the_out_Archive << m_movObjRefModelList;
			the_out_Archive.Close();
			fl.Close();
			m_entityDlgBar.SetLoadedFile(fileName);

			CFile fl2;
			if (!fl2.Open(fileNameW + L".Server", CFile::modeCreate | CFile::modeWrite, &exc)) {
				LOG_EXCEPTION(exc, m_logger)
			} else {
				::ServerSerialize = true;
				CArchive the_out_Archive(&fl2, CArchive::store);
				the_out_Archive << m_movObjRefModelList;
				the_out_Archive.Close();
				fl2.Close();
			}
			END_SERIALIZE_TRY(m_logger)
			::ServerSerialize = false;

			if (m_entityDlgBar.GetLoadedFile() != "" && newFile) {
				//update script event
				CStringW sCaption, sText;
				sCaption.LoadString(IDS_CHANGE_SETTINGS);
				sText.LoadString(IDS_CONFIRM_ADDING_SCRIPT_EVENT);
				int ret = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNO);
				if (ret == IDYES) {
					m_entityDlgBar.UpdateScript();
				}
			}
		}
	}
}

void CKEPEditView::OnBTNENTLoadDBExpanded() {
}

void CKEPEditView::OnBTNENTSaveDBSmoothNormals() {
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) {
		CMovementObj *mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);

		if (mPtr->m_runtimeSkeleton) {
			int count = mPtr->m_runtimeSkeleton->m_deformableMeshArrayCount;
			mPtr->m_skeletonConfiguration->SmoothNormals("neck", "torso", count);
			mPtr->m_skeletonConfiguration->SmoothNormals("torso", "arms", count);
			mPtr->m_skeletonConfiguration->SmoothNormals("torso", "hips", count);
			mPtr->m_skeletonConfiguration->SmoothNormals("arms", "hands", count);
			mPtr->m_skeletonConfiguration->SmoothNormals("hips", "legs", count);
			mPtr->m_skeletonConfiguration->SmoothNormals("legs", "feet", count);

			//Two additional smoothing rules for UGC shirt per Eric's request
			mPtr->m_skeletonConfiguration->SmoothNormals("UGC_shirtN", "UGC_shirtS", count); // Smooth between tight-fit long-sleeve UGC shirt mesh and the matching neck mesh (different from standard neck)
			mPtr->m_skeletonConfiguration->SmoothNormals("UGC_shirtS", "hands", count); // Smooth between tight-fit long-sleeve UGC shirt mesh and hands mesh
		}
	}
	OnBTNENTSaveDB();
}

void CKEPEditView::OnBTNENTSaveDB() {
	// Rewritten by Jonny 04/03/08 support save of core and whole EDB
	BOOL newFile = m_entityDlgBar.GetLoadedFile() == "";
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Actor Database Files (*.edb)|*.EDB|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EDB", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		CFileException exc;
		CFile fl;
		if (!fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc))
			LOG_EXCEPTION(exc, m_logger)
		else {
			BEGIN_SERIALIZE_TRY

			::ServerSerialize = false;
			CArchive the_out_Archive(&fl, CArchive::store);

			// Modified by Jonny 04/02/08 save entire non loose EDB.
			m_movObjRefModelList->m_saveWholeEDB = true;
			the_out_Archive << m_movObjRefModelList;
			the_out_Archive.Close();
			fl.Close();
			m_entityDlgBar.SetLoadedFile(fileName);

			CFile fl2;
			if (!fl2.Open(fileNameW + L".Server", CFile::modeCreate | CFile::modeWrite, &exc)) {
				LOG_EXCEPTION(exc, m_logger)
			} else {
				::ServerSerialize = true;
				CArchive the_out_Archive(&fl2, CArchive::store);
				the_out_Archive << m_movObjRefModelList;
				the_out_Archive.Close();
				fl2.Close();
			}
			END_SERIALIZE_TRY(m_logger)
			::ServerSerialize = false;

			if (m_entityDlgBar.GetLoadedFile() != "" && newFile) {
				//update script event
				CStringW sCaption, sText;
				sCaption.LoadString(IDS_CHANGE_SETTINGS);
				sText.LoadString(IDS_CONFIRM_ADDING_SCRIPT_EVENT);
				int ret = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNO);
				if (ret == IDYES) {
					m_entityDlgBar.UpdateScript();
				}
			}
		}
	}
}

void CKEPEditView::OnButtonSaveANM() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Animation Files (*.anm)|*.ANM|All Files (*.*)|*.*||";
	CFileDialog saveDialog(FALSE, L"ANM", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	saveDialog.m_ofn.lpstrInitialDir = CurDir;
	if (saveDialog.DoModal() == IDOK) {
		CStringW filePathW = saveDialog.GetPathName();
		CStringW pathW = L"";
		int slashPos = filePathW.ReverseFind(L'\\');
		if (slashPos != -1)
			pathW = filePathW.Left(slashPos);

		// write the archive uncompressed animation file
		AnimationRM::Instance()->WriteAnimations(Utf16ToUtf8(filePathW + L"U").c_str());

		///////////////////////////////////////////////////////////////
		// !!! NO NEED TO WRITE COMPRESSED VERSION SINCE WE NO LONGER DELIVER IT !!!
		// write the compressed distribution animation file
		//SkeletalAnimationManager::Instance()->WriteAnimations( filePath, TRUE );
		///////////////////////////////////////////////////////////////

		// write the loose animations
		AnimationRM::Instance()->WriteAnimationsLoose(Utf16ToUtf8(pathW));
	}
}

void CKEPEditView::OnButtonSaveANMU() {
} // CKEPEditView::OnButtonSaveANM

void CKEPEditView::OnBTNENTRemoveLOD() {
	if (m_movObjRefModelList) {
		for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) {
			CMovementObj *mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
			if (mPtr) {
				for (POSITION posLoc2 = mPtr->m_skeletonConfiguration->m_alternateConfigurations->GetHeadPosition(); posLoc2 != NULL;) {
					CAlterUnitDBObject *aPtr = (CAlterUnitDBObject *)mPtr->m_skeletonConfiguration->m_alternateConfigurations->GetNext(posLoc2);
					if (aPtr) {
						for (POSITION posLoc3 = aPtr->m_configurations->GetHeadPosition(); posLoc3 != NULL;) {
							CDeformableMesh *dMesh = (CDeformableMesh *)aPtr->m_configurations->GetNext(posLoc3);
							if (dMesh) {
							}
						}
					}
				}
			}
		}
	}
}

void CKEPEditView::OnBTNSCRAddScript() {
#ifndef _ClientOutput

	CEXScriptObjList *pList = NULL;

	if (m_scriptDlgBar.GetType() == CScriptDialog::ZONE) {
		if (!SceneScriptExec) {
			SceneScriptExec = new CEXScriptObjList;
		}
		pList = SceneScriptExec;
		EditorState::GetInstance()->SetZoneDirty();
	} else {
		if (!GL_scriptList) {
			GL_scriptList = new CEXScriptObjList;
		}

		pList = GL_scriptList;
	}

	if (pList) {
		CAddScriptEventDlg dlg(m_scriptDlgBar.GetType());

		if (dlg.DoModal() == IDOK) {
			AddScriptEvent((ACTION_ATTR)dlg.m_eventId, dlg.m_miscText, dlg.m_miscInt, pList);

			// update Editor
			if (m_editorModeEnabled == TRUE) {
				m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(dlg.m_event).c_str());
			}
		}
	}

#endif
}

void CKEPEditView::OnBTNSCRDeleteEvent() {
	UINT dlgType = m_scriptDlgBar.GetType();

	if ((dlgType == CScriptDialog::GAME) ||
		(dlgType == CScriptDialog::AI) ||
		(dlgType == CScriptDialog::CLIENT)) {
		if (!GL_scriptList) {
			AfxMessageBox(IDS_MSG_NOEVENTDELETE);
			return;
		}

		int runtimeIndex = m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_GETCURSEL, 0, 0);
		if (runtimeIndex == -1) {
			AfxMessageBox(IDS_MSG_SELSCRIPTEVENT);
			return;
		}

		POSITION posLoc = GL_scriptList->FindIndex(runtimeIndex);
		if (!posLoc) {
			return;
		}

		CEXScriptObj *ptrLoc = (CEXScriptObj *)GL_scriptList->GetAt(posLoc);
		delete ptrLoc;
		ptrLoc = 0;
		GL_scriptList->RemoveAt(posLoc);
		m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_DELETESTRING, runtimeIndex, 0);
	} else if (dlgType == CScriptDialog::ZONE) {
		if (!SceneScriptExec) {
			AfxMessageBox(IDS_MSG_NOEVENTDELETE);
			return;
		}

		int runtimeIndex = m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_GETCURSEL, 0, 0);
		if (runtimeIndex == -1) {
			AfxMessageBox(IDS_MSG_SELSCRIPTEVENT);
			return;
		}

		POSITION posLoc = SceneScriptExec->FindIndex(runtimeIndex);
		if (!posLoc) {
			return;
		}

		CEXScriptObj *ptrLoc = (CEXScriptObj *)SceneScriptExec->GetAt(posLoc);
		delete ptrLoc;
		ptrLoc = 0;
		SceneScriptExec->RemoveAt(posLoc);
		EditorState::GetInstance()->SetZoneDirty();
		m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_DELETESTRING, runtimeIndex, 0);
	}
}

void CKEPEditView::OnBTNSCRLoadExecFile() {
	CStringA fileName;
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();

	UINT dlgType = m_scriptDlgBar.GetType();

	if ((dlgType == CScriptDialog::GAME) ||
		(dlgType == CScriptDialog::ZONE)) {
		fileName = "autoexec.xml";
		m_scriptDlgBar.SetFileName(fileName);
	} else if (dlgType == CScriptDialog::AI) {
		fileName = "AIautoexec.xml";
		m_scriptDlgBar.SetFileName(fileName);
	} else if (dlgType == CScriptDialog::CLIENT) {
		fileName = "clientautoexec.xml";
		m_scriptDlgBar.SetFileName(fileName);
	}

	if (!::jsFileExists(fileName, NULL)) {
		CStringW s;
		s.LoadString(IDS_ERROR_FILENOTFOUND);
		AfxMessageBox(s + Utf8ToUtf16(fileName).c_str());
		return;
	}

	DeleteScriptList(&GL_scriptList);

	GL_scriptList = new CEXScriptObjList();

	try {
		FileName fn(fileName.GetString());
		GL_scriptList->SerializeFromXML(fn);
	} catch (KEPException *ex) {
		ex->Delete();
		return;
	}

	ScriptInList(GL_scriptList);
}

void CKEPEditView::ScriptInList(CEXScriptObjList *ptrLoc) {
	m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_RESETCONTENT, 0, 0);
	if (!ptrLoc) {
		return;
	}

	for (POSITION posLoc = ptrLoc->GetHeadPosition(); posLoc != NULL;) {
		CEXScriptObj *scrPtr = (CEXScriptObj *)ptrLoc->GetNext(posLoc);
		m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST,
			LB_ADDSTRING,
			0,
			(LPARAM)Utf8ToUtf16(CAddScriptEventDlg::GetEventNameFromNum((int)scrPtr->m_actionAttribute)).c_str());
	}
}

void CKEPEditView::OnBTNSCRBuild() {
	UINT dlgType = m_scriptDlgBar.GetType();

	if ((dlgType == CScriptDialog::GAME) ||
		(dlgType == CScriptDialog::AI) ||
		(dlgType == CScriptDialog::CLIENT)) {
		if (GL_scriptList) {
			SaveScriptFile(m_scriptDlgBar.GetFileName(), GL_scriptList);
		}
	} else if (dlgType == CScriptDialog::ZONE) {
		// OnBTNSCRSetToScene() is not called because SceneScriptExec is already
		// update-to-date; however, in order for list to be saved, the Zone must be saved
	}
}

void CKEPEditView::OnBTNSCREditEvent() {
#ifndef _ClientOutput

	CEXScriptObjList *pList = NULL;

	if (m_scriptDlgBar.GetType() == CScriptDialog::ZONE) {
		pList = SceneScriptExec;
		EditorState::GetInstance()->SetZoneDirty();
	} else {
		pList = GL_scriptList;
	}

	if (!pList) {
		AfxMessageBox(IDS_MSG_NOEVENTTOEDIT);
		return;
	}

	int runtimeIndex = m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELSCRIPTEVENT);
		return;
	}

	POSITION posLoc = pList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CEXScriptObj *ptrLoc = (CEXScriptObj *)pList->GetAt(posLoc);
	CAddScriptEventDlg dlg(m_scriptDlgBar.GetType());

	dlg.m_eventId = (int)ptrLoc->m_actionAttribute;
	dlg.m_miscText = ptrLoc->m_miscString;
	dlg.m_miscInt = ptrLoc->m_miscInt;
	if (dlg.DoModal() == IDOK) {
		ptrLoc->m_actionAttribute = (ACTION_ATTR)dlg.m_eventId;
		ptrLoc->m_miscString = dlg.m_miscText;
		ptrLoc->m_miscInt = dlg.m_miscInt;
		m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_INSERTSTRING, runtimeIndex, (LPARAM)Utf8ToUtf16(dlg.m_event).c_str());
		m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_DELETESTRING, (runtimeIndex + 1), 0);
	}

#endif
}

void CKEPEditView::OnBTNMSLoadDB() {
}

void CKEPEditView::OnBTNENTLoadEntity() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Actor Unit Files (*.sie)|*.SIE|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();

		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		//get the next actor trace
		int temptrace = -1;
		POSITION p = m_movObjRefModelList->GetHeadPosition();
		while (p) {
			CMovementObj *tObj = (CMovementObj *)m_movObjRefModelList->GetAt(p);
			if (tObj->m_traceNumber > temptrace)
				temptrace = tObj->m_traceNumber;

			m_movObjRefModelList->GetNext(p);
		}
		temptrace++;

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		CMovementObj *newPtrLoc = NULL;
		the_in_Archive >> newPtrLoc;
		the_in_Archive.Close();
		fl.Close();

		newPtrLoc->m_traceNumber = temptrace;

		// Prep then add baby!!
		m_movObjRefModelList->AddTail(newPtrLoc);
		if (m_editorModeEnabled == TRUE) {
			EnitityInList();
		}

		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNENTSaveEntity() {
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;

	// get selected index
	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Actor Unit Files (*.sie)|*.SIE|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"SIE", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY

		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << ptrLoc;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::EnitityInList() {
	// delete all existing enries in tree
	DelTreeNode(IDC_TREE_ENTITIES, &m_entityDlgBar, TVI_ROOT);
	if (m_movObjRefModelList) {
		for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
			CMovementObj *mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
			if (!mPtr)
				continue;
			/*HTREEITEM child1 =*/AddTreeRoot(Utf8ToUtf16(mPtr->m_name).c_str(), IDC_TREE_ENTITIES, &m_entityDlgBar);
		}
	}
}

void CKEPEditView::OnBTNMSSaveDB() {
}

void CKEPEditView::OnBTNOBJEditLight() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = reinterpret_cast<CWldObject *>(m_pWOL->GetNext(posLoc));
		if (!pWO)
			continue;

		if ((textLoc == pWO->m_fileNameRef) && pWO->m_lightObject) {
			CLightInterfaceDlg dlg;
			dlg.GL_textureDataBase = m_pTextureDB;
			dlg.m_lightREF = pWO->m_lightObject;
			if (IDOK == dlg.DoModal()) {
				pWO->m_lightObject = dlg.m_lightREF;
				EditorState::GetInstance()->SetZoneDirty();
			} // if..IDOK == dlg.DoModel()

			return;
		} // if..textLoc == pWorldObj->m_fileNameRef
	} // end Begin world object loop

	AfxMessageBox(IDS_ERROR_OBJECTNOTFOUND_NOLIGHTEXISTS);
#endif
}

void CKEPEditView::OnBtnTxtdbDelete() {
}

void CKEPEditView::OnBtnTxtdbEdit() {
}

void CKEPEditView::OnBtnTxtdbReinitAll() {
}

void CKEPEditView::OnBtnTxtdbReinitFromDisk() {
}

void CKEPEditView::OnBtnTxtdbSetSubtraction() {
}

void CKEPEditView::OnBTNOBJClearTexCoords() {
}

void CKEPEditView::OnBTNOBJAddTexCoords() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();

	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = reinterpret_cast<CWldObject *>(m_pWOL->GetNext(posLoc));
		if (!pWO)
			continue;
		if (textLoc == pWO->m_fileNameRef) {
			CVertexAnimSettingsDlg dlg;

			dlg.m_animModuleRef = pWO->m_animModule;
			dlg.g_pd3dDevice = g_pd3dDevice;
			dlg.DoModal();
			pWO->m_animModule = dlg.m_animModuleRef;

			return;
		} // if..textLoc == pWorldObj->m_fileNameRef
	} // end Begin world object loop

	AfxMessageBox(IDS_ERROR_OBJECTNOTFOUND_NOLIGHTEXISTS);
#endif
}

void CKEPEditView::OnBTNSCRSetToScene() {
	if (!GL_scriptList) {
		return;
	}

	for (POSITION posLoc = GL_scriptList->GetHeadPosition(); posLoc != NULL;) {
		CEXScriptObj *scrPtr = (CEXScriptObj *)GL_scriptList->GetNext(posLoc);
		AddScriptEvent(scrPtr->m_actionAttribute, scrPtr->m_miscString, scrPtr->m_miscInt, SceneScriptExec);
	}
}

void CKEPEditView::OnBTNSCRClearScene() {
	if (m_editorModeEnabled == TRUE) {
		m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_RESETCONTENT, 0, 0);
	}

	DeleteScriptList(&SceneScriptExec);
	SceneScriptExec = new CEXScriptObjList;
}

void CKEPEditView::OnBTNSCRLoadFromScene() {
	ScriptInList(SceneScriptExec);
}

void CKEPEditView::OnBTNNTAdvanced() {
#ifndef _ClientOutput
	CAdvancedNetworkDlg dlg(m_multiplayerObj);
#endif
}

void CKEPEditView::OnBTNNTBuildTxtRecord() {
	ASSERT(FALSE);
}

void CKEPEditView::OnBTNHUDSaveDB() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Heads-up Display Files (*.hud)|*.HUD|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"HUD", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << HudList;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNHUDLoadDB() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Heads-up Display Files (*.hud)|*.HUD|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		LoadHudDatabase(fileName);
	}
}

void CKEPEditView::OnBTNHUDAddDisplayUnit() {
#ifndef _ClientOutput
	CHudAddDisplayUnitDlg dlg;
	int hudIndex = m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_DB, LB_GETCURSEL, 0, 0);
	if (hudIndex == -1) {
		AfxMessageBox(IDS_MSG_SELHUD);
		return;
	}

	POSITION posLoc = HudList->FindIndex(hudIndex);
	if (!posLoc) {
		return;
	}

	CHudObj *hudLoc = (CHudObj *)HudList->GetAt(posLoc);

	// Initialize
	dlg.redPen = 255;
	dlg.greenPen = 255;
	dlg.bluePen = 255;
	dlg.redText = 255;
	dlg.greenText = 255;
	dlg.blueText = 255;
	dlg.typeReturned = -1;

	if (dlg.DoModal() == IDOK) {
		BOOL dxfLoad = TRUE;

		CFileException exc;
		CFile fl;
		BOOL res = OpenCFile(fl, dlg.m_uniText, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			dxfLoad = FALSE;
		} else {
			fl.Close();
		}

		hudLoc->AddUnit(dlg.m_uniInt,
			dlg.typeReturned,
			dlg.m_posX,
			dlg.m_posY,
			dlg.m_fontIndex,
			dlg.m_uniFloat,
			dlg.m_uniText,
			dxfLoad,
			dlg.redPen,
			dlg.greenPen,
			dlg.bluePen,
			dlg.redText,
			dlg.greenText,
			dlg.blueText,
			255,
			255,
			255,
			dlg.m_lineThickness);

		stringstream strBuilder;
		strBuilder << "Type ->" << dlg.typeReturned;

		m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_UNITSDB,
			LB_ADDSTRING,
			0,
			(LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
	}

#endif
}

void CKEPEditView::OnBTNHUDAddNewHud() {
}

void CKEPEditView::OnBTNHUDDeleteHud() {
	int hudIndex = m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_DB, LB_GETCURSEL, 0, 0);
	if (hudIndex == -1) {
		AfxMessageBox(IDS_MSG_SELHUD);
		return;
	}

	POSITION posLoc = HudList->FindIndex(hudIndex);
	if (!posLoc) {
		return;
	}

	CHudObj *hudLoc = (CHudObj *)HudList->GetAt(posLoc);
	hudLoc->SafeDelete();
	delete hudLoc;
	hudLoc = 0;
	HudList->RemoveAt(posLoc);
	InListHud();
}

void CKEPEditView::OnBTNHUDDeleteDisplayUnit() {
	int hudIndex = m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_DB, LB_GETCURSEL, 0, 0);
	if (hudIndex == -1) {
		AfxMessageBox(IDS_MSG_SELHUD);
		return;
	}

	int DisplayIndex = m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_UNITSDB, LB_GETCURSEL, 0, 0);
	if (DisplayIndex == -1) {
		AfxMessageBox(IDS_MSG_SELHUDUNIT);
		return;
	}

	POSITION posLoc = HudList->FindIndex(hudIndex);
	if (!posLoc) {
		return;
	}

	CHudObj *hudLoc = (CHudObj *)HudList->GetAt(posLoc);
	posLoc = hudLoc->m_displayUnitList->FindIndex(DisplayIndex);
	if (!posLoc) {
		return;
	}

	CHudDisplayObj *displayPtr = (CHudDisplayObj *)hudLoc->m_displayUnitList->GetAt(posLoc);
	displayPtr->SafeDelete();
	delete displayPtr;
	displayPtr = 0;
	hudLoc->m_displayUnitList->RemoveAt(posLoc);
	InListDisplayUnits();
}

void CKEPEditView::InListHud() {
	m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_DB, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = HudList->GetHeadPosition(); posLoc != NULL;) {
		CHudObj *hudPtr = (CHudObj *)HudList->GetNext(posLoc);
		m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_DB,
			LB_ADDSTRING,
			0,
			(LPARAM)Utf8ToUtf16(">" + hudPtr->m_HudCollectionName).c_str());
	}
}

void CKEPEditView::InListDisplayUnits() {
	int hudIndex = m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_DB, LB_GETCURSEL, 0, 0);
	if (hudIndex == -1) {
		return;
	}

	POSITION posLoc = HudList->FindIndex(hudIndex);
	if (!posLoc) {
		return;
	}

	CHudObj *hudLoc = (CHudObj *)HudList->GetAt(posLoc);
	m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_UNITSDB, LB_RESETCONTENT, 0, 0);
	for (posLoc = hudLoc->m_displayUnitList->GetHeadPosition(); posLoc != NULL;) {
		CHudDisplayObj *displayPtr = (CHudDisplayObj *)hudLoc->m_displayUnitList->GetNext(posLoc);
		stringstream strBuilder;
		strBuilder << "Type ->" << displayPtr->m_unitType;

		m_hudDBDlgBar.SendDlgItemMessage(IDC_LIST_HUD_UNITSDB,
			LB_ADDSTRING,
			0,
			(LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
	}
}

void CKEPEditView::OnBTNHUDReplaceUnit() {
}

void CKEPEditView::OnSelchangeListHudDb() {
	InListDisplayUnits();
}

void CKEPEditView::OnBTNENTUpdate() {
	int runtimeIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
		return;
	}

	POSITION posLoc = m_movObjList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
	Matrix44f tempMatrix;
	ptrLoc->m_baseFrame->GetTransform(NULL, &tempMatrix);

	CMovementObj::CONTROL_TYPE controlType = ptrLoc->m_controlType;
	int refModel = ptrLoc->m_idxRefModel;
	DestroyRuntimeEntity(ptrLoc);
	ptrLoc = 0;
	m_movObjList->RemoveAt(posLoc);

	// Create New Movement Object
	bool ok = ActivateMovementEntity(
		0,
		tempMatrix,
		refModel,
		controlType,
		0,
		0,
		FALSE,
		((controlType == eActorControlType::CONTROL_TYPE_PC) ? TRUE : FALSE),
		false,
		-1,
		NULL,
		NULL,
		-1,
		-1,
		false,
		"");
	if (!ok) {
		LogError("ActivateMovementEntity() FAILED");
	}

	InListRuntimeEntities(runtimeIndex);
}

void CKEPEditView::OnSelchangeLISTTXTDBDatabase() {
}

void CKEPEditView::OnCHECKTEXBltOn() {
}

void CKEPEditView::ParticleDatabaseInList(int sel) {
	if (!m_particleRM) {
		return;
	}

	if (m_editorModeEnabled == FALSE) {
		return;
	}

	m_particleDBDlgBar.SendDlgItemMessage(IDC_LIST_PART_DATABASE, LB_RESETCONTENT, 0, 0);
	for (UINT psLoop = 0; psLoop < m_particleRM->GetNumberOfStockItems(); psLoop++) {
		EnvParticleSystem *partPtr = m_particleRM->GetStockItemByIndex(psLoop);
		m_particleDBDlgBar.SendDlgItemMessage(IDC_LIST_PART_DATABASE,
			LB_ADDSTRING,
			0,
			(LPARAM)Utf8ToUtf16(partPtr->m_systemName).c_str());
	}

	if (sel != -1) {
		m_particleDBDlgBar.SendDlgItemMessage(IDC_LIST_PART_DATABASE, LB_SETCURSEL, sel, 0);
	}
}

void CKEPEditView::OnBTNPARTLoadDatabase() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();

	string file_mask = "";

	// get the file extensions for the external particle types
	int32 num = ClientBladeFactory::Instance()->GetNumBladesOfType(typeid(IClientParticleBlade));
	for (int32 i = 0; i < num; i++) {
		if (i > 0)
			file_mask += "|";

		file_mask += ((IClientParticleBlade *)(ClientBladeFactory::Instance()->GetBladeOfType(typeid(IClientParticleBlade), i)))->GetProviderDescription();

		file_mask += "|*.";

		file_mask += ((IClientParticleBlade *)(ClientBladeFactory::Instance()->GetBladeOfType(typeid(IClientParticleBlade), i)))->GetDefinitionExtension();
	}

	file_mask += "||";

	//	static wchar_t BASED_CODE filter[] = L"Particle List Files (*.prt)|*.PRT|All Files (*.*)|*.*||";

	const char *filter = file_mask.c_str();
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, Utf8ToUtf16(filter).c_str(), this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		LoadParticleDatabase(fileName);
	}
}

void CKEPEditView::OnBTNPARTEditSystem() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();

	int particleListIndex;

	particleListIndex = m_particleDBDlgBar.SendDlgItemMessage(IDC_LIST_PART_DATABASE, LB_GETCURSEL, 0, 0);

	EnvParticleSystem *pParticleSystem = m_particleRM->GetStockItemByIndex(particleListIndex);
	if (!pParticleSystem)
		return;

	CStringA to_exec;
	UINT result;

	to_exec = "ParticleEditor.exe ";
	to_exec += pParticleSystem->storedFileName.c_str();
	result = WinExec(to_exec, SW_SHOW);
	if (ERROR_FILE_NOT_FOUND == result)
		AfxMessageBox(IDS_ERROR_PARTICLEEDITORNOTFOUND);
}

void CKEPEditView::SpawnGenDatabaseInList(int sel) {
	if (m_editorModeEnabled == FALSE) {
		return;
	}

	int traceIt = 0;
	m_spawnGenDBDlgBar.SendDlgItemMessage(IDC_LIST_SPAWN_LISTDB, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = spawnGeneratorDB->GetHeadPosition(); posLoc != NULL;) {
		CSpawnObj *partPtr = (CSpawnObj *)spawnGeneratorDB->GetNext(posLoc);
		stringstream strBuilder;
		strBuilder << traceIt << " :" << partPtr->m_spawnName;

		m_spawnGenDBDlgBar.SendDlgItemMessage(IDC_LIST_SPAWN_LISTDB,
			LB_ADDSTRING,
			0,
			(LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
		traceIt++;
	}

	if (sel != -1) {
		m_spawnGenDBDlgBar.SendDlgItemMessage(IDC_LIST_SPAWN_LISTDB, LB_SETCURSEL, sel, 0);
	}
}

void CKEPEditView::OnBTNSpawnAddGen() {
#ifndef _ClientOutput
	CAddSpawnGeneratorDlg dlg;
	dlg.m_aiCfgDatabaseRef = m_AIOL;

	CSpawnObj *spawnGenPtr = new CSpawnObj();
	dlg.m_generatorName = spawnGenPtr->m_spawnName;

	dlg.m_posX = spawnGenPtr->m_spawnMatrix[3][0];
	dlg.m_posY = spawnGenPtr->m_spawnMatrix[3][1];
	dlg.m_posZ = spawnGenPtr->m_spawnMatrix[3][2];

	dlg.m_possibilityOfSpawn = spawnGenPtr->m_possibilityOfSpawn;
	dlg.m_maxOutputCapacity = spawnGenPtr->m_maxOutPutCapacity;
	dlg.m_possibilityOfSpawn = spawnGenPtr->m_possibilityOfSpawn; // % out of 1000
	dlg.m_spawnIndex = spawnGenPtr->m_spawnIndex;
	dlg.m_spawnRadius = spawnGenPtr->m_spawnRadius;
	dlg.m_spawnType = spawnGenPtr->m_spawnType;
	dlg.treeList = m_pAIWOL;
	dlg.spawnObj = spawnGenPtr;
	dlg.useRuntimeEntityList = FALSE;
	dlg.m_manualStart = spawnGenPtr->m_manualStart;

	if (dlg.DoModal() == IDOK) {
		spawnGenPtr->m_spawnName = dlg.m_generatorName;
		if (dlg.useRuntimeEntityList == TRUE) {
			int ENIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
			if (ENIndex == -1) {
				AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
				return;
			}

			POSITION position = m_movObjList->FindIndex(ENIndex);
			if (position) {
				CMovementObj *mPtrtarget = (CMovementObj *)m_movObjList->GetAt(position);
				mPtrtarget->m_baseFrame->GetTransform(NULL, &spawnGenPtr->m_spawnMatrix);
			}
		} else {
			spawnGenPtr->m_spawnMatrix[3][0] = dlg.m_posX;
			spawnGenPtr->m_spawnMatrix[3][1] = dlg.m_posY;
			spawnGenPtr->m_spawnMatrix[3][2] = dlg.m_posZ;
		}

		spawnGenPtr->m_possibilityOfSpawn = dlg.m_possibilityOfSpawn;
		spawnGenPtr->m_maxOutPutCapacity = dlg.m_maxOutputCapacity;
		spawnGenPtr->m_spawnIndex = dlg.m_spawnIndex;
		spawnGenPtr->m_spawnRadius = dlg.m_spawnRadius;
		spawnGenPtr->m_spawnType = dlg.m_spawnType;
		spawnGeneratorDB->AddTail(spawnGenPtr);
		SpawnGenDatabaseInList(spawnGeneratorDB->GetCount() - 1);
		spawnGeneratorDB->GetTotalAIOutput();
		spawnGenPtr->m_manualStart = dlg.m_manualStart;
		EditorState::GetInstance()->SetZoneDirty();
	}

#endif
}

void CKEPEditView::OnBTNSpawnDeleteGen() {
	int runtimeIndex = m_spawnGenDBDlgBar.SendDlgItemMessage(IDC_LIST_SPAWN_LISTDB, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELSPAWNGEN);
		return;
	}

	POSITION posLoc = spawnGeneratorDB->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CSpawnObj *ptrLoc = (CSpawnObj *)spawnGeneratorDB->GetAt(posLoc);
	delete ptrLoc;
	ptrLoc = 0;
	spawnGeneratorDB->RemoveAt(posLoc);
	SpawnGenDatabaseInList(runtimeIndex - 1);
	spawnGeneratorDB->GetTotalAIOutput();
	EditorState::GetInstance()->SetZoneDirty();
}

void CKEPEditView::OnBTNSpawnEditGen() {
#ifndef _ClientOutput
	int runtimeIndex = m_spawnGenDBDlgBar.SendDlgItemMessage(IDC_LIST_SPAWN_LISTDB, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELSPAWNGEN);
		return;
	}

	POSITION posLoc = spawnGeneratorDB->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CSpawnObj *spawnGenPtr = (CSpawnObj *)spawnGeneratorDB->GetAt(posLoc);
	CAddSpawnGeneratorDlg dlg;

	dlg.m_aiCfgDatabaseRef = m_AIOL;
	dlg.m_generatorName = spawnGenPtr->m_spawnName;
	dlg.m_posX = spawnGenPtr->m_spawnMatrix[3][0];
	dlg.m_posY = spawnGenPtr->m_spawnMatrix[3][1];
	dlg.m_posZ = spawnGenPtr->m_spawnMatrix[3][2];
	dlg.m_possibilityOfSpawn = spawnGenPtr->m_possibilityOfSpawn;
	dlg.m_maxOutputCapacity = spawnGenPtr->m_maxOutPutCapacity;
	dlg.m_possibilityOfSpawn = spawnGenPtr->m_possibilityOfSpawn; // % out of 1000
	dlg.m_spawnIndex = spawnGenPtr->m_spawnIndex;
	dlg.m_spawnRadius = spawnGenPtr->m_spawnRadius;
	dlg.m_spawnType = spawnGenPtr->m_spawnType;
	dlg.treeList = m_pAIWOL;
	dlg.spawnObj = spawnGenPtr;
	dlg.useRuntimeEntityList = FALSE;
	dlg.m_manualStart = spawnGenPtr->m_manualStart;

	if (dlg.DoModal() == IDOK) {
		if (dlg.useRuntimeEntityList == TRUE) {
			int ENIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
			if (ENIndex == -1) {
				AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
				return;
			}

			POSITION position = m_movObjList->FindIndex(ENIndex);
			if (position) {
				CMovementObj *mPtrtarget = (CMovementObj *)m_movObjList->GetAt(position);
				mPtrtarget->m_baseFrame->GetTransform(NULL, &spawnGenPtr->m_spawnMatrix);
			}
		} else {
			spawnGenPtr->m_spawnMatrix[3][0] = dlg.m_posX;
			spawnGenPtr->m_spawnMatrix[3][1] = dlg.m_posY;
			spawnGenPtr->m_spawnMatrix[3][2] = dlg.m_posZ;
		}

		spawnGenPtr->m_spawnName = dlg.m_generatorName;
		spawnGenPtr->m_possibilityOfSpawn = dlg.m_possibilityOfSpawn;
		spawnGenPtr->m_maxOutPutCapacity = dlg.m_maxOutputCapacity;
		spawnGenPtr->m_spawnIndex = dlg.m_spawnIndex;
		spawnGenPtr->m_spawnRadius = dlg.m_spawnRadius;
		spawnGenPtr->m_spawnType = dlg.m_spawnType;
		SpawnGenDatabaseInList(runtimeIndex);
		spawnGenPtr->m_manualStart = dlg.m_manualStart;

		spawnGeneratorDB->SetDirty();
		EditorState::GetInstance()->SetZoneDirty();
	}

#endif
}

void CKEPEditView::OnBTNObjectBuildCollisionMap() {
#ifndef _ClientOutput
	if (m_editorModeEnabled == TRUE) {
		GL_collisionBase->m_wldObjList = m_pWOL;

		CCollisionResolutionDlg dlg;
		GL_collisionBase->GetResolution(0, dlg.m_XResolution, dlg.m_YResolution, dlg.m_ZResolution);
		dlg.m_levelsDeep = GL_collisionBase->GetNumberOfLevels();
		dlg.m_maxfaceResident = GL_collisionBase->GetMaxFacesPerBox();
		if (dlg.DoModal() == IDOK) {
			//free the memory from the last one
			//delete GL_collisionBase;
			//GL_collisionBase = 0;
			//GL_collisionBase = new CCollisionObj ();

			CDialog progressInternal;
			progressInternal.Create(IDD_DIALOG_PROGRESSMETER, this);
			progressInternal.ShowWindow(SW_SHOW);

			HWND hwndProgTotal = NULL, hwndProgTask = NULL;
			progressInternal.GetDlgItem(IDC_PROGRESS_METER, &hwndProgTotal);
			progressInternal.GetDlgItem(IDC_PROGRESS_SUBLEVEL, &hwndProgTask);

			BuildCollisionDatabase(m_pWOL, dlg.m_levelsDeep, dlg.m_maxfaceResident, dlg.m_XResolution, dlg.m_YResolution, dlg.m_ZResolution, m_libraryReferenceDB, hwndProgTotal, hwndProgTask);

			AfxMessageBox(IDS_MSG_COMPLETE);

			EditorState::GetInstance()->SetZoneDirty();
		}
	}

#endif
}

void CKEPEditView::VisualPropertiesInList() {
	m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_VISUALATTRIBUTES, LB_RESETCONTENT, 0, 0);

	CWldObject *ptrLoc = GetSelectedWldObjectFromTreeView();

	if (!ptrLoc)
		return;

	if (ptrLoc->m_usedAsSpawnPoint != -1) {
		stringstream strBuilder;
		strBuilder << "Spawn Point Index ::" << ptrLoc->m_usedAsSpawnPoint;

		m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_VISUALATTRIBUTES,
			LB_ADDSTRING,
			0,
			(LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
	}

	if (ptrLoc->m_lightObject) {
		m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_VISUALATTRIBUTES, LB_ADDSTRING, 0, (LPARAM)L"Light Exists");
	}

	if (ptrLoc->m_triggerList) {
		if (ptrLoc->m_triggerList->GetCount() > 0) {
			m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_VISUALATTRIBUTES, LB_ADDSTRING, 0, (LPARAM)L"Trigger Exists");
		}
	}

	if (ptrLoc->m_meshObject) {
		// LOD data
		m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJ_LODDATA, LB_RESETCONTENT, 0, 0);

		int tracer = 0;
		if (ptrLoc->m_lodVisualList) {
			for (POSITION posLoc = ptrLoc->m_lodVisualList->GetHeadPosition(); posLoc != NULL;) {
				CMeshLODObject *lodPtr = (CMeshLODObject *)ptrLoc->m_lodVisualList->GetNext(posLoc);
				stringstream strBuilder;
				strBuilder << tracer << " : LOD LEVEL"
						   << "  Dist : " << lodPtr->m_distance;

				m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJ_LODDATA, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
				tracer++;
			}
		}
	}
}

CWldObject *CKEPEditView::GetSelectedWldObjectFromTreeView(void) {
	int curSelectedGroupNumber = -1;
	int curSelectedGroupIdx = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	POSITION selectedGroupPos = worldGroupList->FindIndex(curSelectedGroupIdx);
	if (selectedGroupPos) {
		CWldGroupObj *wldGroupPtr = (CWldGroupObj *)worldGroupList->GetAt(selectedGroupPos);
		curSelectedGroupNumber = wldGroupPtr->m_groupInt;
	}

	HWND objectTreeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &objectTreeHwnd);
	HTREEITEM selectedObjectItem = TreeView_GetSelection(objectTreeHwnd);

	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, selectedObjectItem)).c_str();
	/*HTREEITEM itemIter = TreeView_GetRoot( objectTreeHwnd );
	HTREEITEM itemIterChild = TreeView_GetChild( objectTreeHwnd, itemIter );
	// iterate objects, and if the object is in the selected group
	//  advance the itemIter until it is equal to the selected item*/
	CWldObject *pWO = 0;
	POSITION wldObjectPos = m_pWOL->GetHeadPosition();
	while (wldObjectPos) {
		pWO = (CWldObject *)m_pWOL->GetNext(wldObjectPos);
		if (pWO && (pWO->m_groupNumber == curSelectedGroupNumber)) {
			if (textLoc == pWO->m_fileNameRef)
				break;
		}
		pWO = 0;
	}

	return pWO;
}

void CKEPEditView::OnSelchangedObjectTree(NMHDR *pNMHDR, LRESULT *pResult) {
	LPNMTREEVIEW pnmtv = (LPNMTREEVIEW)pNMHDR;
	if (pnmtv->action == TVC_UNKNOWN)
		return;

	CWldObject *wldObjectPtr = GetSelectedWldObjectFromTreeView();

	if (wldObjectPtr) {
		if (!m_selectedLmp || (m_selectedLmp->GetLegacyWorldObject()->m_identity != wldObjectPtr->m_identity)) {
			KEP::LegacyModelProxy *lmpPtr = new LegacyModelProxy(wldObjectPtr, g_pd3dDevice, m_libraryReferenceDB);
			;
			if (lmpPtr)
				SetSelected(lmpPtr);
		}
	} else
		SetSelected(0);

	VisualPropertiesInList();
	*pResult = 0;
}

void CKEPEditView::OnBTNObjectsAddGroup() {
#ifndef _ClientOutput
	CWorldGroupDlg dlg;
	dlg.m_groupID = GetNextAvailableWldGroupID();
	dlg.m_groupName.Format("NewGroup%d", dlg.m_groupID);

	if (dlg.DoModal() == IDOK) {
		CWldGroupObj *groupPtr = new CWldGroupObj(dlg.m_groupName, dlg.m_groupID);
		worldGroupList->AddTail(groupPtr);
		EditorState::GetInstance()->SetZoneDirty();
		WorldGroupInList(-1);
	}
#endif
}

void CKEPEditView::OnBTNObjectsDeleteGroup() {
	int runtimeIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELGROUP);
		return;
	}

	POSITION posLoc = worldGroupList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	if (AfxMessageBox(IDS_CONFIRM_GROUPDELETE, MB_OKCANCEL, 0) != IDOK)
		return;

	CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetAt(posLoc);
	POSITION posLast;
	for (POSITION wPos = m_pWOL->GetHeadPosition(); (posLast = wPos) != NULL;) {
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(wPos);
		if (pWO->m_groupNumber == wldPtr->m_groupInt) {
			if ((m_selectedLmp != 0) && (pWO == m_selectedLmp->GetLegacyWorldObject()))
				SetSelected(0);

			pWO->SafeDelete();
			delete pWO;
			pWO = 0;
			m_pWOL->RemoveAt(posLast);
		}
	}

	delete wldPtr;

	wldPtr = 0;
	worldGroupList->RemoveAt(posLoc);
	EditorState::GetInstance()->SetZoneDirty();
	WorldGroupInList(runtimeIndex - 1);
	int selGroupInt = -1;
	CWldGroupObj *selGroup = GetSelectedGroup();
	if (selGroup)
		selGroupInt = selGroup->m_groupInt;
	ObjectTreeViewInlist(selGroupInt, 0);
	EditorState::GetInstance()->SetZoneDirty();
}

void CKEPEditView::OnBTNObjectsEditGroup() {
#ifndef _ClientOutput
	int runtimeIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELGROUP);
		return;
	}

	POSITION posLoc = worldGroupList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetAt(posLoc);
	CWorldGroupDlg dlg;
	dlg.m_groupID = wldPtr->m_groupInt;
	dlg.m_groupName = wldPtr->m_groupName;
	if (dlg.DoModal() == IDOK) {
		wldPtr->m_groupInt = dlg.m_groupID;
		wldPtr->m_groupName = dlg.m_groupName;
		EditorState::GetInstance()->SetZoneDirty();
	}

	WorldGroupInList(runtimeIndex);
#endif
}

void CKEPEditView::OnSelchangeLISTObjectsGroupList2Box() {
	int runtimeIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		return;
	}

	POSITION posLoc = worldGroupList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetAt(posLoc);
	ObjectTreeViewInlist(wldPtr->m_groupInt, 0);
}

void CKEPEditView::OnDblclkLISTObjectsGroupList2Box() {
	ObjectsDialogGroupModifier();
}

void CKEPEditView::OnBTNGroupModifier() {
	ObjectsDialogGroupModifier();
}

void CKEPEditView::ObjectsDialogGroupModifier() {
#ifndef _ClientOutput

	bool bMovedToDiffGroup = false;

	int runtimeIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		return;
	}

	POSITION posLoc = worldGroupList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CWldGroupObj *groupPtr = (CWldGroupObj *)worldGroupList->GetAt(posLoc);

	CGroupModifierDlg dlg;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.diffuseRed = 0.0f;
	dlg.diffuseGreen = 0.0f;
	dlg.diffuseBlue = 0.0f;
	dlg.specularRed = 0.0f;
	dlg.specularGreen = 0.0f;
	dlg.specularBlue = 0.0f;
	dlg.emmisiveRed = 0.0f;
	dlg.emmisiveGreen = 0.0f;
	dlg.emmisiveBlue = 0.0f;
	if (dlg.DoModal() == IDOK) { // applicate
		if (dlg.UnifyVertsByTolerance) {
			UnionVertexesPositionalyByTolerance(dlg.m_tolerance, groupPtr->m_groupInt);
		}

		for (posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
			CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
			if (pWO->m_groupNumber == groupPtr->m_groupInt || dlg.m_applyGlobal == TRUE) { // filter only group
				if (dlg.m_diffuseEffectEnable == TRUE) {
					if (pWO->m_meshObject) {
						if (pWO->m_meshObject->m_materialObject) {
							pWO->m_meshObject->m_materialObject->m_diffuseEffect = dlg.m_diffuseVxEffect;

							if (pWO->m_lodVisualList) {
								for (POSITION lodPos = pWO->m_lodVisualList->GetHeadPosition(); lodPos != NULL;) {
									CMeshLODObject *mLod = (CMeshLODObject *)pWO->m_lodVisualList->GetNext(lodPos);
									mLod->m_material->m_diffuseEffect = dlg.m_diffuseVxEffect;
								}
							}
						}
					}
				}

				if (dlg.m_groupIdModifier == TRUE) {
					if (pWO->m_groupNumber != dlg.m_modGroupID)
						bMovedToDiffGroup = true;

					pWO->m_groupNumber = dlg.m_modGroupID;
				}

				if (dlg.particleEmmiterEnable == TRUE) { // particle assignment
					pWO->m_particleLink = dlg.m_particleDBref;
				} // end particle assignment

				if (dlg.m_enableCharonaOption == TRUE) { // charona assignment
					if (dlg.m_charonaObject) {
						if (pWO->m_meshObject->m_charona) {
							pWO->m_meshObject->m_charona->SafeDelete();
							delete pWO->m_meshObject->m_charona;
							pWO->m_meshObject->m_charona = 0;
						}

						dlg.m_charonaObject->Clone(&pWO->m_meshObject->m_charona);
						if (pWO->m_meshObject->m_charona) {
							pWO->m_meshObject->m_charona->InitializeVis();
							pWO->m_meshObject->m_charona->m_visual->InitBuffer(g_pd3dDevice,
								pWO->m_meshObject->m_charona->m_visual->m_uvCount);
						}
					} else {
						if (pWO->m_meshObject->m_charona) {
							pWO->m_meshObject->m_charona->SafeDelete();
							delete pWO->m_meshObject->m_charona;
							pWO->m_meshObject->m_charona = 0;
						}
					}
				} // end charona assignment

				if (dlg.m_enableLightOption == TRUE) { // light assignment
					if (dlg.m_lightObject) {
						if (pWO->m_lightObject) {
							pWO->m_lightObject->SafeDelete();
							delete pWO->m_lightObject;
							pWO->m_lightObject = 0;
						}

						dlg.m_lightObject->Clone(&pWO->m_lightObject);
					} else {
						if (pWO->m_lightObject) {
							pWO->m_lightObject->SafeDelete();
							delete pWO->m_lightObject;
							pWO->m_lightObject = 0;
						}
					}
				} // end light assignment

				if (dlg.materialModEnable == TRUE) { // material modification
					if (pWO->m_meshObject) //non reference object
					{
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Emissive.r = dlg.emmisiveRed;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Emissive.g = dlg.emmisiveGreen;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Emissive.b = dlg.emmisiveBlue;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Specular.r = dlg.specularRed;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Specular.g = dlg.specularGreen;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Specular.b = dlg.specularBlue;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Diffuse.r = dlg.diffuseRed;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Diffuse.g = dlg.diffuseGreen;
						pWO->m_meshObject->m_materialObject->m_baseMaterial.Diffuse.b = dlg.diffuseBlue;
						pWO->m_meshObject->m_materialObject->Reset();
					}
				}

				if (dlg.multipassModEnable == TRUE) { // material modification
					if (pWO->m_meshObject) //non reference object
					{
						pWO->m_meshObject->m_materialObject->m_texBlendMethod[0] = dlg.blendMode;
					}
				}

				if (dlg.textureDataSwapEnable == TRUE) { // material modification
					if (pWO->m_meshObject) //non reference object
					{
						pWO->m_meshObject->ExchangeTextureSets(dlg.m_texturedataSwapIndex1, dlg.m_texturedataSwapIndex2);
						pWO->m_meshObject->InitBuffer(g_pd3dDevice, NULL);
					}
				}

				if (dlg.meshOptimizer == TRUE) { // material modification
					if (pWO->m_meshObject) //non reference object
					{
						pWO->m_meshObject->m_pd3dDevice = g_pd3dDevice;
						pWO->m_meshObject->OptimizeMesh();
					}
				}

				if (dlg.boundingBoxModifier == TRUE) { // bounding box modifier
					if (pWO->m_meshObject) //non reference object
					{
						//ptrLoc->m_meshObject->GetBox ();
						pWO->m_popoutBox.max.x = pWO->m_meshObject->GetBoundingBox().maxX + ((pWO->m_meshObject->GetBoundingBox().maxX - pWO->m_meshObject->GetBoundingBox().minX) * dlg.m_groupMultiplier);
						pWO->m_popoutBox.max.y = pWO->m_meshObject->GetBoundingBox().maxY + ((pWO->m_meshObject->GetBoundingBox().maxY - pWO->m_meshObject->GetBoundingBox().minY) * dlg.m_groupMultiplier);
						pWO->m_popoutBox.max.z = pWO->m_meshObject->GetBoundingBox().maxZ + ((pWO->m_meshObject->GetBoundingBox().maxZ - pWO->m_meshObject->GetBoundingBox().minZ) * dlg.m_groupMultiplier);
						pWO->m_popoutBox.min.x = pWO->m_meshObject->GetBoundingBox().minX - ((pWO->m_meshObject->GetBoundingBox().maxX - pWO->m_meshObject->GetBoundingBox().minX) * dlg.m_groupMultiplier);
						pWO->m_popoutBox.min.y = pWO->m_meshObject->GetBoundingBox().minY - ((pWO->m_meshObject->GetBoundingBox().maxY - pWO->m_meshObject->GetBoundingBox().minY) * dlg.m_groupMultiplier);
						pWO->m_popoutBox.min.z = pWO->m_meshObject->GetBoundingBox().minZ - ((pWO->m_meshObject->GetBoundingBox().maxZ - pWO->m_meshObject->GetBoundingBox().minZ) * dlg.m_groupMultiplier);
					} else {
						CReferenceObj *refObj = m_libraryReferenceDB->GetByIndex(pWO->m_node->m_libraryReference);
						if (refObj) {
							float x = pWO->m_node->GetTranslation().x;
							float y = pWO->m_node->GetTranslation().y;
							float z = pWO->m_node->GetTranslation().z;

							pWO->m_popoutBox.max.x = x + refObj->m_boundBox.maxX + ((refObj->m_boundBox.maxX - refObj->m_boundBox.minX) * dlg.m_groupMultiplier);
							pWO->m_popoutBox.max.y = y + refObj->m_boundBox.maxY + ((refObj->m_boundBox.maxY - refObj->m_boundBox.minY) * dlg.m_groupMultiplier);
							pWO->m_popoutBox.max.z = z + refObj->m_boundBox.maxZ + ((refObj->m_boundBox.maxZ - refObj->m_boundBox.minZ) * dlg.m_groupMultiplier);
							pWO->m_popoutBox.min.x = x + refObj->m_boundBox.minX - ((refObj->m_boundBox.maxX - refObj->m_boundBox.minX) * dlg.m_groupMultiplier);
							pWO->m_popoutBox.min.y = y + refObj->m_boundBox.minY - ((refObj->m_boundBox.maxY - refObj->m_boundBox.minY) * dlg.m_groupMultiplier);
							pWO->m_popoutBox.min.z = z + refObj->m_boundBox.minZ - ((refObj->m_boundBox.maxZ - refObj->m_boundBox.minZ) * dlg.m_groupMultiplier);
						}
					}
				} // end bounding box modifier

				if (dlg.boundingBoxModifierMirror == TRUE) { // bounding box modifier

					if (dlg.boxAbsolute) {
						if (pWO->m_meshObject) //normal object
						{
							pWO->m_popoutBox.max.x = pWO->m_meshObject->GetMeshCenter().x + dlg.m_bndBoxMaxX;
							pWO->m_popoutBox.max.y = pWO->m_meshObject->GetMeshCenter().y + dlg.m_bndBoxMaxY;
							pWO->m_popoutBox.max.x = pWO->m_meshObject->GetMeshCenter().z + dlg.m_bndBoxMaxZ;
							pWO->m_popoutBox.min.x = pWO->m_meshObject->GetMeshCenter().x + dlg.m_bndBoxMinX;
							pWO->m_popoutBox.min.y = pWO->m_meshObject->GetMeshCenter().y + dlg.m_bndBoxMinY;
							pWO->m_popoutBox.min.x = pWO->m_meshObject->GetMeshCenter().z + dlg.m_bndBoxMinZ;

						} else //reference object
						{
							pWO->m_popoutBox.max.x = pWO->m_node->GetTranslation().x + dlg.m_bndBoxMaxX;
							pWO->m_popoutBox.max.y = pWO->m_node->GetTranslation().y + dlg.m_bndBoxMaxY;
							pWO->m_popoutBox.max.z = pWO->m_node->GetTranslation().z + dlg.m_bndBoxMaxZ;
							pWO->m_popoutBox.min.x = pWO->m_node->GetTranslation().x + dlg.m_bndBoxMinX;
							pWO->m_popoutBox.min.y = pWO->m_node->GetTranslation().y + dlg.m_bndBoxMinY;
							pWO->m_popoutBox.min.z = pWO->m_node->GetTranslation().z + dlg.m_bndBoxMinZ;
						}
					} else {
						pWO->m_popoutBox.max.x = dlg.m_bndBoxMaxX;
						pWO->m_popoutBox.max.y = dlg.m_bndBoxMaxY;
						pWO->m_popoutBox.max.z = dlg.m_bndBoxMaxZ;
						pWO->m_popoutBox.min.x = dlg.m_bndBoxMinX;
						pWO->m_popoutBox.min.y = dlg.m_bndBoxMinY;
						pWO->m_popoutBox.min.z = dlg.m_bndBoxMinZ;
					}
				} // end bounding box modifier

				if (dlg.wrapModifier == TRUE) {
					if (pWO->m_meshObject) //non reference object
					{
						pWO->m_meshObject->m_materialObject->m_textureTileType = dlg.wrapType;
					}
				}

				if (dlg.filterCollisionModEnable == TRUE) {
					pWO->m_collisionInfoFilter = dlg.filterCollisionOn;
				}

				if (dlg.bRenderFilterEnable == TRUE) {
					pWO->m_renderFilter = dlg.bRenderFilter;
				}
			} // end filter only group
		} // end world object loop

		// if the visuals were moved to a different group, reset the visuals tree
		if (bMovedToDiffGroup)
			DelTreeNode(IDC_OBJECT_TREE, &m_dlgBarObject, TVI_ROOT);

		EditorState::GetInstance()->SetZoneDirty();
	} // end applicate
#endif
}

void CKEPEditView::UnionVertexesPositionalyByTolerance(float tolerance, int group) {
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		if (!pWO->m_meshObject) {
			continue;
		}

		ABVERTEX *vArray = NULL;
		int vCount = 0;
		pWO->m_meshObject->m_abVertexArray.GetABVERTEXStyle(&vArray, &vCount);

		if (pWO->m_groupNumber == group) { // select by group
			for (POSITION scnPos = m_pWOL->GetHeadPosition(); scnPos != NULL;) { // begin secondary world object loop
				CWldObject *chkLoc = (CWldObject *)m_pWOL->GetNext(scnPos);
				if (!chkLoc->m_meshObject) {
					continue;
				}

				ABVERTEX *chkvArray = NULL;
				int chkvCount = 0;
				chkLoc->m_meshObject->m_abVertexArray.GetABVERTEXStyle(&chkvArray, &chkvCount);

				for (UINT pvLoop = 0; pvLoop < pWO->m_meshObject->m_abVertexArray.m_vertexCount; pvLoop++) { // primary mesh loop
					for (UINT scndLoop = 0; scndLoop < chkLoc->m_meshObject->m_abVertexArray.m_vertexCount; scndLoop++) { // secondary mesh loop
						float dist = Distance(Vector3f(vArray[pvLoop].x, vArray[pvLoop].y, vArray[pvLoop].z),
							Vector3f(chkvArray[scndLoop].x, chkvArray[scndLoop].y, chkvArray[scndLoop].z));
						if (dist < tolerance) { // colapse
							chkvArray[scndLoop].x = vArray[pvLoop].x;
							chkvArray[scndLoop].y = vArray[pvLoop].y;
							chkvArray[scndLoop].z = vArray[pvLoop].z;
						} // end colapse
					} // end secondary mesh loop
				} // end primary mesh loop

				if (chkvArray) {
					chkLoc->m_meshObject->m_abVertexArray.SetABVERTEXStyle(NULL, chkvArray, chkvCount);
					delete[] chkvArray;
					chkvArray = 0;
				}

				chkLoc->m_meshObject->InitBuffer(g_pd3dDevice, NULL);

				if (chkLoc->m_lodVisualList) {
					if (pWO->m_lodVisualList) {
						for (POSITION lPos = pWO->m_lodVisualList->GetHeadPosition(); lPos != NULL;) { // primary LOD loop
							/*CMeshLODObject	*primaryLodPtr = (CMeshLODObject *)*/ pWO->m_lodVisualList->GetNext(lPos);

							ASSERT(FALSE); // following code is obsolete, alarm if any part of system actually called this

							//@@Todo: if for some reason this is needed, alternative approach is required to make it work without m_meshObject

							/* Completely disabled as we no longer have m_meshLodLevel
							if ( !primaryLodPtr->m_meshLodLevel )
							{
								continue;
							}

							ABVERTEX	*subvArray = NULL;
							int			supvCount = 0;
							primaryLodPtr->m_meshLodLevel->m_abVertexArray.GetABVERTEXStyle ( &subvArray, &supvCount );

							POSITION	scndPos = chkLoc->m_lodVisualList->FindIndex ( tracer );
							if ( scndPos )
							{				// secondary LOD loop
								CMeshLODObject	*secondaryLodPtr = (CMeshLODObject *) chkLoc->m_lodVisualList->GetAt ( scndPos );
								if ( !secondaryLodPtr->m_meshLodLevel )
								{
									continue;
								}

								ABVERTEX	*subscndryvArray = NULL;
								int			supvscndryCount = 0;
								secondaryLodPtr->m_meshLodLevel->m_abVertexArray.GetABVERTEXStyle ( &subscndryvArray, &supvscndryCount );

								for ( UINT pvLoop = 0; pvLoop < primaryLodPtr->m_meshLodLevel->m_abVertexArray.m_vertexCount; pvLoop++ )
								{			// primary mesh loop
									for ( UINT scndLoop = 0; scndLoop < secondaryLodPtr->m_meshLodLevel->m_abVertexArray.m_vertexCount; scndLoop++ )
									{		// secondary mesh loop
										float	dist = MatrixARB::MagnitudeSquared ( subvArray[pvLoop].x,
											subvArray[pvLoop].y,
											subvArray[pvLoop].z,
											subscndryvArray[scndLoop].x,
											subscndryvArray[scndLoop].y,
											subscndryvArray[scndLoop].z );
										if ( dist < tolerance )
										{	// colapse
											subscndryvArray[scndLoop].x = subvArray[pvLoop].x;
											subscndryvArray[scndLoop].y = subvArray[pvLoop].y;
											subscndryvArray[scndLoop].z = subvArray[pvLoop].z;
										}	// end colapse
									}		// end secondary mesh loop
								}			// end primary mesh loop

								if ( subscndryvArray )
								{
									secondaryLodPtr->m_meshLodLevel->m_abVertexArray.SetABVERTEXStyle ( NULL, subscndryvArray, supvscndryCount );
									delete[] subscndryvArray;
									subscndryvArray = 0;
								}

								secondaryLodPtr->m_meshLodLevel->InitBuffer ( g_pd3dDevice, NULL );
							}				// secondary found ok

							tracer++;
							if ( subvArray )
							{
								delete[] subvArray;
								subvArray = 0;
							}
							*/
						} // end primary LOD loop
					}
				}
			} // end secondary world object loop
		} // end select by group

		if (vArray) {
			delete[] vArray;
			vArray = 0;
		}
	} // end world object loop
}

void CKEPEditView::OnDblclkObjectTree(NMHDR *pNMHDR, LRESULT *pResult) {
	WorldObjectProperties();
	*pResult = 0;
}

void CKEPEditView::OnBTNUIMerge() {
	BOOL aiMergeMode = FALSE;

	CMergeOptionsDlg dlg;
	dlg.m_aiMergeType = TRUE;
	dlg.m_groupMode = -1;
	dlg.m_transitionGroupID = 0;

	if (dlg.DoModal() == IDOK) {
		aiMergeMode = dlg.m_aiMergeType;
	}

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Zone Files (*.exg)|*.EXG|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		AppendScene(Utf16ToUtf8(fileNameW).c_str(), aiMergeMode, dlg.m_groupMode, dlg.m_transitionGroupID);
		EditorState::GetInstance()->SetZoneDirty();
	}

	AfxMessageBox(L"Merge Complete! NOTE: If you had reference objects in both zones they have been merged, dont forget to export your new reference library.", MB_OK);
	return;
}

void CKEPEditView::OnBTNOptimizeStates() {
}

void CKEPEditView::OnBTNDeleteGuids() {
	if (AfxMessageBox(IDS_MSG_CONFIRM_DEL_GUIDS, MB_OKCANCEL, 0) == IDOK)
		DeleteWorldObjectGuids();
}

void CKEPEditView::InListAccounts(int sel) {
	m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_RESETCONTENT, 0, 0);

	int trace = 0;
	for (POSITION posLoc = m_multiplayerObj->m_accountDatabase->GetHeadPosition(); posLoc != NULL;) {
		CAccountObject *accntObj = (CAccountObject *)m_multiplayerObj->m_accountDatabase->GetNext(posLoc);
		stringstream strBuilder;
		strBuilder << accntObj->m_accountName << " " << trace;

		m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
		trace++;
	}

	if (sel != -1) {
		m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_SETCURSEL, sel, 0);
	}
}

void CKEPEditView::OnBTNTCPNewAccount() {
	if (m_accountModule) {
		delete m_accountModule;
	}

	m_accountModule = 0;

	m_accountModule = new CNewAccountsModule();
	m_accountModule->Create(IDD_DIALOG_NEWACCOUNTS, this);
	m_accountModule->ShowWindow(SW_SHOW);

	if (!m_multiplayerObj->m_accountDatabase) {
		m_multiplayerObj->m_accountDatabase = new CAccountObjectList();
	}

	m_accountModule->accountDatabaseDBREF = m_multiplayerObj->m_accountDatabase;
	m_accountModule->multiplayerRef = m_multiplayerObj;
	m_accountModule->m_multiPlayerDlgRef = &m_multiPlayer;
}

void CKEPEditView::OnBTNTCPDeleteAccount() {
	CStringA pass;
	GetDlgItemTextUtf8(&m_multiPlayer, IDC_EDIT_TCP_ADMINCODE, pass);
	if (pass != "rain") {
		return;
	}

	int runtimeIndex = m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCURRENTACCT);
		return;
	}

	POSITION posLoc = m_multiplayerObj->m_accountDatabase->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CAccountObject *ptrLoc = (CAccountObject *)m_multiplayerObj->m_accountDatabase->GetAt(posLoc);
	CStringW s;
	s.LoadString(IDS_CONFIRM_DELETEACCOUNT);
	if (AfxMessageBox(s + Utf8ToUtf16(ptrLoc->m_accountName).c_str(), MB_OKCANCEL, 0) == IDOK) { // 1
		delete ptrLoc;
		ptrLoc = 0;
		m_multiplayerObj->m_accountDatabase->RemoveAt(posLoc);
		InListAccounts(runtimeIndex - 1);
	} // end 1
}

void CKEPEditView::OnBTNOBJAddTrigger() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = reinterpret_cast<CWldObject *>(m_pWOL->GetNext(posLoc));

		if (!pWO)
			continue;
		if (textLoc == pWO->m_fileNameRef) {
			CTriggerEditorDlg dlg;

			dlg.m_triggerListRef = pWO->m_triggerList;
			if (pWO->m_meshObject) {
				dlg.m_originX = pWO->m_meshObject->GetMeshCenter().x;
				dlg.m_originY = pWO->m_meshObject->GetMeshCenter().y;
				dlg.m_originZ = pWO->m_meshObject->GetMeshCenter().z;
			} // if..pWorldObj->m_meshObject
			else if (pWO->m_node) {
				dlg.m_originX = pWO->m_node->GetTranslation().x;
				dlg.m_originY = pWO->m_node->GetTranslation().y;
				dlg.m_originZ = pWO->m_node->GetTranslation().z;
			} // if..else..pWorldObj->m_node

			if (IDOK == dlg.DoModal()) {
				pWO->m_triggerList = dlg.m_triggerListRef;
				EditorState::GetInstance()->SetZoneDirty();
				if (m_triggers)
					m_triggers->RemoveAll();
			} // if..IDOK == dlg.DoModal()

			return;
		} // if..textLoc == pWorldObj->m_fileNameRef
	}

	AfxMessageBox(IDS_MSG_SELTRACEOBJECT);
#endif
}

void CKEPEditView::OnBTNOBJUpdateObject() {
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	//int			indexOfObject = GetTreeChildIndex ( treeHwnd, item );
	//HTREEITEM	ObjectRoot = TreeView_GetParent ( treeHwnd, item );
	HTREEITEM ObjectRoot = item;
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, ObjectRoot)).c_str();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		if (textLoc == pWO->m_fileNameRef) {
			CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();
			string filter;
			IAssetImporterFactory::Instance()->GetOpenDialogFilter(KEP::KEP_STATIC_MESH, filter);
			CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, Utf8ToUtf16(filter).c_str(), this);
			opendialog.m_ofn.lpstrInitialDir = CurDir;
			if (opendialog.DoModal() == IDOK) {
				CStringW fileNameW = opendialog.GetFileName();
				CStringA fileName = Utf16ToUtf8(fileNameW).c_str();

				CEXMeshObj *pMesh = IAssetsDatabase::Instance()->ImportStaticMesh((const char *)fileName);

				if (0 != pMesh) {
					pWO->SetMesh(pMesh, FALSE);

					AddTextureToDatabase(pMesh); // apply mapping and add to DB

					if (pMesh && pMesh->m_materialObject) {
						pWO->SetMaterial(pMesh->m_materialObject, NewWorld.GetString());
					} // if..ptrLoc->m_meshObject->m_materialObject

				} // if..IAssetsDatabase::Instance()->ImportStaticMesh( ... )
				EditorState::GetInstance()->SetZoneDirty();
			}

			return;
		}
	}

	AfxMessageBox(IDS_MSG_SELVISUAL);
}

void CKEPEditView::OnCHECKTCPRegistered() {
	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);
	if (m_multiplayerObj->m_serverMode == 0) {
		m_multiPlayer.SendDlgItemMessage(IDC_CHECK_TCP_REGISTERED, BM_SETCHECK, BST_CHECKED, 0);
		m_multiplayerObj->m_serverMode = 1;
	} else {
		m_multiPlayer.SendDlgItemMessage(IDC_CHECK_TCP_REGISTERED, BM_SETCHECK, BST_UNCHECKED, 0);
		m_multiplayerObj->m_serverMode = 0;
	}

	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
}

void CKEPEditView::OnSelchangeLISTTCPIPCurrentPlayers() {
	int runtimeIndex = m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		return;
	}

	POSITION posLoc = m_multiplayerObj->m_accountDatabase->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CAccountObject *ptrLoc = (CAccountObject *)m_multiplayerObj->m_accountDatabase->GetAt(posLoc);

	wstringstream strBuilder;
	strBuilder << "Minutes Played " << ptrLoc->m_timePlayedMinutes;

	m_multiPlayer.SetDlgItemText(IDC_EDIT_ACCOUNTSTATUS, strBuilder.str().c_str());

	// Reset string builder
	strBuilder.str(L"");
	strBuilder.clear();

	strBuilder << L"Account Status ";

	CStringW comments = L"";
	if (ptrLoc->m_accountStatus != 0) {
		strBuilder << L"Suspended";
		comments = Utf8ToUtf16(ptrLoc->m_suspentionNotes).c_str();
	} else {
		strBuilder << L"Ok";
	}

	m_multiPlayer.SetDlgItemText(IDC_EDIT_TCP_ACCOUNTSTATUS3, strBuilder.str().c_str());
	m_multiPlayer.SetDlgItemText(IDC_EDIT_TCP_ACCOUNTSTATUS4, comments);
}

void CKEPEditView::OnBTNTCPServerLoad() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Server State Files (*.svr)|*.SVR|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		if (m_multiplayerObj) {
			m_multiplayerObj->SafeDelete();
			delete m_multiplayerObj;
			m_multiplayerObj = 0;
		}

		CMultiplayerObj *tempObj;
		if (TryDeserializeAndLog(&fl, (CObject **)&tempObj, m_multiplayerObj->GetRuntimeClass(), &m_logger)) {
			m_multiplayerObj = tempObj;
			char cwd[_MAX_PATH];
			m_multiplayerObj->SetPathBase(_getcwd(cwd, _MAX_PATH));
			m_multiplayerObj->m_accountDatabase->ExpandFromOptimized(m_skillDatabase, m_globalInventoryDB);
			fl.Close();
			if (m_editorModeEnabled == TRUE) {
				InListAccounts(m_multiplayerObj->m_accountDatabase->GetCount() - 1);
			}
		} else {
			AfxMessageBox(IDS_MSG_FAILED_LOADING_GAME_FILE, MB_ICONEXCLAMATION);
		}
	}
}

void CKEPEditView::OnBTNTCPServerSave() {
#ifndef _LimitedEngine
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Server State Files (*.svr)|*.SVR|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"SVR", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);

		EnterCriticalSection(&m_multiplayerObj->m_criticalSection);

		m_multiplayerObj->m_accountDatabase->FlagAllForOptimizedSave();
		the_out_Archive << m_multiplayerObj;
		LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
#endif
}

void CKEPEditView::OnBTNTXSaveTextureBankStatic() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Texture Database Files (*.txb)|*.TXB|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EXG", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);

	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
#ifdef CTEXTUREEX_SERIALIZE
		if (SaveTextureBank(fileName, eTextureDatabase_Static)) {
			m_textureDBDlgBar.SetLoadedFile(fileName);
		} // if..SaveTextureBank(...)
#else
		throw new CNotSupportedException();
#endif
	} // if..opendialog.DoModal()
}

void CKEPEditView::OnBTNTXSaveTextureBankDynamic() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Texture Database Files (*.txb)|*.TXB|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EXG", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);

	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
#ifdef CTEXTUREEX_SERIALIZE
		if (SaveTextureBank(fileName, eTextureDatabase_Dynamic)) {
			m_textureDBDlgBar.SetLoadedFile(fileName);
		} // if..SaveTextureBank(...)
#else
		throw new CNotSupportedException();
#endif
	} // if..opendialog.DoModal()
}

void CKEPEditView::OnBTNTXLoadTextureBank() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Texture Database Files (*.txb)|*.TXB|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
#ifdef CTEXTUREEX_SERIALIZE
		if (LoadTextureBank(fileName)) {
			m_textureDBDlgBar.SetLoadedFile(fileName);
		}
		InListTextureDB(0);
#else
		throw new CNotSupportedException();
#endif
	}
}

void CKEPEditView::OnBtnTxtdbAdd() {
#ifndef _ClientOutput
	// attemp multiselect
	// working
	CAddTextureDlg dlg;
	dlg.m_mipLevels = 1;
	dlg.fileCounter = 0;
	if (dlg.DoModal() == IDOK) {
		D3DCOLOR colorKey;
		colorKey = D3DCOLOR_RGBA(dlg.m_colorKeyRed, dlg.m_colorKeyGreen, dlg.m_colorKeyBlue, dlg.m_colorKeyAlpha);
		for (int loop = 0; loop < dlg.fileCounter; loop++) { // load all files
			if (dlg.fileCounter != 1 && loop == 0) {
				loop++;
			}

			CStringA loadContruction(dlg.FileNames[0]);

			loadContruction += "\\";
			loadContruction += dlg.FileNames[loop];

			// Add Texture To Texture Database
			DWORD nameHash = m_pTextureDB->TextureAdd(loadContruction.GetString(), colorKey);
			if (ValidTextureNameHash(nameHash))
				InListTextureDB(nameHash);
		}
	}
#endif
}

void CKEPEditView::OnBTNOBJGetSignificant() {
#ifndef _ClientOutput
	GetSignificantOptionsDlg dlg;
	if (dlg.DoModal() == IDOK) {
		if (dlg.m_getInverseOnly) {
			ObjectTreeViewInlist(-9998, 0);
		} else {
			ObjectTreeViewInlist(-9999, 0);
		}
	}

#endif
}

void CKEPEditView::OnBTNOBJTextureSetMacro() {
	LoadRNEFile();
}

void CKEPEditView::AddNodeByData(int groupIndex, int selection, CMovementObj *entityPtr, Matrix44f *overrideMatrix) {
#ifndef _ClientOutput
	int sqCount = 1;
	int sqTrace = 0;
	while (sqCount > 0) {
		sqCount--;

		CWldObject *wldObject;
		wldObject = new CWldObject((m_globalTraceNumber++));
		wldObject->m_groupNumber = groupIndex;
		wldObject->m_triggerList = new CTriggerObjectList;
		wldObject->m_node = new CNodeObj();
		wldObject->m_node->m_libraryReference = selection + sqTrace;
		sqTrace++;

		CReferenceObj *refObj = m_libraryReferenceDB->GetByIndex(wldObject->m_node->m_libraryReference);
		if (!refObj) {
			AfxMessageBox(IDS_ERROR_REFERENCELIBRARYOBJECTCANNOTOPEN);
			return;
		}

		sqCount += refObj->m_seqLoad;
		if (!overrideMatrix) {
			Matrix44f frameTransform;
			Matrix44f scaleMatrix, translateMatrix;
			entityPtr->m_baseFrame->GetTransform(NULL, &frameTransform);
			scaleMatrix.MakeScale(Vector3f(m_nodeImporterModule.m_scaleX, m_nodeImporterModule.m_scaleY, m_nodeImporterModule.m_scaleZ));
			translateMatrix.MakeTranslation(Vector3f(m_nodeImporterModule.m_moveX, m_nodeImporterModule.m_moveY, m_nodeImporterModule.m_moveZ));
			wldObject->m_node->SetWorldTransform(translateMatrix * scaleMatrix * frameTransform);
		} else {
			wldObject->m_node->SetWorldTransform(*overrideMatrix);
		}

		// access the ReMesh of the ref object
		// get the platform-independent ReMesh data
		ReMeshData *pData;
		refObj->GetReMesh()->Access(&pData); // RENDERENGINE INTERFACE

		// get the boundbox
		for (DWORD i = 0; i < pData->NumV; i++) {
			Vector3f v = TransformPoint(wldObject->m_node->GetWorldTransform(), pData->pP[i]);

			if (i == 0) {
				wldObject->m_node->m_clipBox.maxX = wldObject->m_node->m_clipBox.minX = v.x;
				wldObject->m_node->m_clipBox.maxY = wldObject->m_node->m_clipBox.minY = v.y;
				wldObject->m_node->m_clipBox.maxZ = wldObject->m_node->m_clipBox.minZ = v.z;
			}

			if (v.x < wldObject->m_node->m_clipBox.minX) {
				wldObject->m_node->m_clipBox.minX = v.x;
			}
			if (v.y < wldObject->m_node->m_clipBox.minY) {
				wldObject->m_node->m_clipBox.minY = v.y;
			}
			if (v.z < wldObject->m_node->m_clipBox.minZ) {
				wldObject->m_node->m_clipBox.minZ = v.z;
			}

			if (v.x > wldObject->m_node->m_clipBox.maxX) {
				wldObject->m_node->m_clipBox.maxX = v.x;
			}
			if (v.y > wldObject->m_node->m_clipBox.maxY) {
				wldObject->m_node->m_clipBox.maxY = v.y;
			}
			if (v.z > wldObject->m_node->m_clipBox.maxZ) {
				wldObject->m_node->m_clipBox.maxZ = v.z;
			}
		}
		// prevent degenerates
		if (wldObject->m_node->m_clipBox.minX == wldObject->m_node->m_clipBox.maxX)
			wldObject->m_node->m_clipBox.maxX = wldObject->m_node->m_clipBox.maxX + .01f;
		if (wldObject->m_node->m_clipBox.minY == wldObject->m_node->m_clipBox.maxY)
			wldObject->m_node->m_clipBox.maxY = wldObject->m_node->m_clipBox.maxY + .01f;
		if (wldObject->m_node->m_clipBox.minZ == wldObject->m_node->m_clipBox.maxZ)
			wldObject->m_node->m_clipBox.maxZ = wldObject->m_node->m_clipBox.maxZ + .01f;

		wldObject->m_popoutBox.max.x = wldObject->m_node->GetTranslation().x + refObj->m_popoutBox.max.x;
		wldObject->m_popoutBox.max.y = wldObject->m_node->GetTranslation().y + refObj->m_popoutBox.max.y;
		wldObject->m_popoutBox.max.z = wldObject->m_node->GetTranslation().z + refObj->m_popoutBox.max.z;

		wldObject->m_popoutBox.min.x = wldObject->m_node->GetTranslation().x + refObj->m_popoutBox.min.x;
		wldObject->m_popoutBox.min.y = wldObject->m_node->GetTranslation().y + refObj->m_popoutBox.min.y;
		wldObject->m_popoutBox.min.z = wldObject->m_node->GetTranslation().z + refObj->m_popoutBox.min.z;

		// end set bounding box
		wldObject->m_poppedOut = TRUE;
		m_pWOL->AddTail(wldObject);
		m_worldListTemp->AddTail(wldObject);
	}

#endif
}

void CKEPEditView::LoadRNEFile() {
	// LB_GETSELITEMS
	int groupIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	if (groupIndex == -1) {
		groupIndex = 0;
	} else {
		POSITION wldGroupPos = worldGroupList->FindIndex(groupIndex);
		groupIndex = 0;
		if (wldGroupPos) {
			CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetAt(wldGroupPos);
			groupIndex = wldPtr->m_groupInt;
		}
	}

	CReferenceImpExpList *rList = new CReferenceImpExpList();

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Reference Export Files (*.rne)|*.RNE|RNE Files|*.RNE|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		rList->InitializeFromFile(Utf16ToUtf8(opendialog.GetPathName()).c_str());
	}

	// populate lists
	for (POSITION posLoc = rList->GetHeadPosition(); posLoc != NULL;) {
		CReferenceImpExpObj *rImp = (CReferenceImpExpObj *)rList->GetNext(posLoc);
		if (rImp->m_type == 0) { // reference
			int index = m_libraryReferenceDB->GetReferenceIndexByName(rImp->m_idName);
			if (index > -1) {
				AddNodeByData(groupIndex, index, NULL, &rImp->m_matrix);
			}
		} else if (rImp->m_type == 1) { // light
			CWldObject *ptrLoc = new CWldObject((m_globalTraceNumber++));
			ptrLoc->m_lightObject = new CRTLightObj();
			ptrLoc->m_lightObject->m_material = new CMaterialObject();
			ptrLoc->m_groupNumber = groupIndex;

			// load imediate mode mesh data
			ptrLoc->m_fileNameRef = rImp->m_idName;

			float sightRange = rImp->m_endAttenuation;
			ptrLoc->m_popoutRadius = rImp->m_endAttenuation;

			ptrLoc->m_lightObject->m_wldspacePos.x = rImp->m_matrix(3, 0);
			ptrLoc->m_lightObject->m_wldspacePos.y = rImp->m_matrix(3, 1);
			ptrLoc->m_lightObject->m_wldspacePos.z = rImp->m_matrix(3, 2);

			ptrLoc->m_lightObject->m_range = rImp->m_endAttenuation;

			ptrLoc->m_lightObject->m_material->m_baseMaterial.Diffuse.r = rImp->m_redValue;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Diffuse.g = rImp->m_greenValue;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Diffuse.b = rImp->m_blueValue;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Diffuse.a = 1.0f;

			ptrLoc->m_lightObject->m_material->m_baseMaterial.Ambient.r = 0.0f;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Ambient.g = 0.0f;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Ambient.b = 0.0f;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Ambient.a = 1.0f;

			ptrLoc->m_lightObject->m_material->m_baseMaterial.Specular.r = 0.0f;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Specular.g = 0.0f;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Specular.b = 0.0f;
			ptrLoc->m_lightObject->m_material->m_baseMaterial.Specular.a = 1.0f;

			// DPD
			// Light Type
			ptrLoc->m_lightObject->m_type = rImp->m_lightType;

			// Direction
			ptrLoc->m_lightObject->m_direction = rImp->m_direction;

			// Theta, Phi, & Falloff
			ptrLoc->m_lightObject->m_theta = rImp->m_theta;
			ptrLoc->m_lightObject->m_phi = rImp->m_phi;
			ptrLoc->m_lightObject->m_falloff = rImp->m_falloff;

			ptrLoc->m_lightObject->m_material->CopyMaterial(ptrLoc->m_lightObject->m_material->m_baseMaterial,
				&ptrLoc->m_lightObject->m_material->m_useMaterial);

			ptrLoc->m_popoutBox.max.x = ((sightRange + 1) + ptrLoc->m_lightObject->m_wldspacePos.x);
			ptrLoc->m_popoutBox.max.y = ((sightRange + 2) + ptrLoc->m_lightObject->m_wldspacePos.y);
			ptrLoc->m_popoutBox.max.z = ((sightRange + 3) + ptrLoc->m_lightObject->m_wldspacePos.z);
			ptrLoc->m_popoutBox.min.x = -(sightRange + 4) + ptrLoc->m_lightObject->m_wldspacePos.x;
			ptrLoc->m_popoutBox.min.y = -(sightRange + 5) + ptrLoc->m_lightObject->m_wldspacePos.y;
			ptrLoc->m_popoutBox.min.z = -(sightRange + 6) + ptrLoc->m_lightObject->m_wldspacePos.z;

			m_pWOL->AddTail(ptrLoc);

			/*HTREEITEM worldObjTreeItem = */ AddTreeRoot(Utf8ToUtf16(ptrLoc->m_fileNameRef).c_str(), IDC_OBJECT_TREE, &m_dlgBarObject);
		} // end light
	}

	EditorState::GetInstance()->SetZoneDirty();

	rList->SafeDelete();
	delete rList;
	rList = 0;
}

void CKEPEditView::OnBTNOBJEditLOD() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	//int			indexOfObject = GetTreeChildIndex ( treeHwnd, item );
	//HTREEITEM	ObjectRoot = TreeView_GetParent ( treeHwnd, item );
	HTREEITEM ObjectRoot = item;
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, ObjectRoot)).c_str();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;

		if (textLoc == pWO->m_fileNameRef) {
			int lodIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJ_LODDATA, LB_GETCURSEL, 0, 0);
			if (lodIndex == -1) {
				AfxMessageBox(IDS_MSG_SELLODLEVEL);
				return;
			}

			ASSERT(pWO->m_lodVisualList != NULL);
			if (pWO->m_lodVisualList == NULL)
				return;

			int lodLevel = lodIndex + 1; // Current conversion between lod level and lod index: change if we put LOD0 into the same list
			CMeshLODObject *mLod = (CMeshLODObject *)pWO->m_lodVisualList->GetLOD(lodLevel);
			ASSERT(mLod != NULL);
			if (mLod == NULL)
				return;

			// mLod->m_meshLodLevel->GetGeometryFromPlugin
			CLodSystemDlg dlg;
			dlg.g_pd3dDeviceRef = g_pd3dDevice;

			dlg.m_maxX = mLod->m_boxOption.maxX;
			dlg.m_maxY = mLod->m_boxOption.maxY;
			dlg.m_maxZ = mLod->m_boxOption.maxZ;
			dlg.m_minX = mLod->m_boxOption.minX;
			dlg.m_minY = mLod->m_boxOption.minY;
			dlg.m_minZ = mLod->m_boxOption.minZ;

			dlg.m_lodType = pWO->m_lodVisualList->m_lodType;

			dlg.m_activation = mLod->m_distance;

			if (dlg.DoModal() == IDOK) {
				if (dlg.m_fileName.GetLength() > 0) { // change mesh

					ASSERT(IAssetsDatabase::Instance() != 0);

					CEXMeshObj *pMesh = IAssetsDatabase::Instance()->ImportStaticMesh(dlg.m_fileName);
					if (pMesh) {
						// create a new ReMesh the legacy way....
						mLod->SetMesh(pMesh, pWO->GetPivot());

						//mLod->m_matrix	=	pMesh->GetWorldTransform();	// Disabled to use existing matrix. Note: MAX pivot point of new mesh must overlap with old.
						mLod->SetBoundingBox(pMesh->GetBoundingBox());

						// CEXMeshObj no longer used
						delete pMesh;
					}

				} // end change mesh

				mLod->m_boxOption.maxX = dlg.m_maxX;
				mLod->m_boxOption.maxY = dlg.m_maxY;
				mLod->m_boxOption.maxZ = dlg.m_maxZ;
				mLod->m_boxOption.minX = dlg.m_minX;
				mLod->m_boxOption.minY = dlg.m_minY;
				mLod->m_boxOption.minZ = dlg.m_minZ;

				mLod->m_distance = dlg.m_activation;

				pWO->m_lodVisualList->m_lodType = dlg.m_lodType;

				pWO->UpdateLods();

				EditorState::GetInstance()->SetZoneDirty();
				VisualPropertiesInList();
			}

			return;
		}
	} // end Begin world object loop

	AfxMessageBox(IDS_MSG_SELVISUAL);
#endif
}

void CKEPEditView::OnBTNOBJRemoveLODLevel() {
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	//int			indexOfObject = GetTreeChildIndex ( treeHwnd, item );
	//HTREEITEM	ObjectRoot = TreeView_GetParent ( treeHwnd, item );
	HTREEITEM ObjectRoot = item;
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, ObjectRoot)).c_str();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;

		if (textLoc == pWO->m_fileNameRef) {
			int lodIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJ_LODDATA, LB_GETCURSEL, 0, 0);
			if (lodIndex == -1) {
				AfxMessageBox(IDS_MSG_SELLODLEVEL);
				return;
			}

			ASSERT(pWO->m_lodVisualList != NULL);
			if (pWO->m_lodVisualList == NULL)
				return;

			int lodLevel = lodIndex + 1; // Current conversion between lod level and lod index: change if we put LOD0 into the same list
			CMeshLODObject *mLod = (CMeshLODObject *)pWO->m_lodVisualList->GetLOD(lodLevel);
			ASSERT(mLod != NULL);
			if (mLod == NULL)
				return;

			pWO->m_lodVisualList->DeleteLOD(lodLevel);
			if (pWO->m_lodVisualList->GetCount() == 0) {
				pWO->m_lodVisualList->SafeDelete();
				delete pWO->m_lodVisualList;
				pWO->m_lodVisualList = 0;
			}

			EditorState::GetInstance()->SetZoneDirty();

			VisualPropertiesInList();

			// end do it
			return;
		}
	} // end Begin world object loop

	AfxMessageBox(IDS_MSG_SELVISUAL);
}

CMeshLODObject *CKEPEditView::AddLODToWorldObject(CWldObject *wldObj, CEXMeshObj *pMesh, int lodType, float activationDistance, ABBOX *activationBox) {
	ASSERT(wldObj != NULL);
	ASSERT(pMesh != NULL);
	ASSERT(lodType == 0 || lodType == 1);
	if (lodType != 0 && lodType != 1)
		return NULL;

	ASSERT(lodType == 0 || activationBox != NULL);

	// Create LOD list if not already
	if (!wldObj->m_lodVisualList)
		wldObj->m_lodVisualList = new CMeshLODObjectList();

	// Create a new LOD object
	CMeshLODObject *pMeshLodObj = new CMeshLODObject(pMesh, // Generate ReMesh from input CEXMeshObj
		wldObj->GetPivot(),
		wldObj->GetMaterial(), // Copy material from parent world object
		wldObj->GetWorldTransform(), // Copy matrix from parent world object
		wldObj->GetBoundingBox(), // Copy bounding box from parent world object
		activationDistance, activationBox); // Set activation parameters

	// Append LOD
	wldObj->m_lodVisualList->InsertLOD(-1, pMeshLodObj);

	// Change LOD type
	wldObj->m_lodVisualList->m_lodType = lodType;

	//	ptrLoc->m_lodVisualList->Initialize ( g_pd3dDevice );

	// MessageBox("AD2");
	wldObj->UpdateLods();
	if (wldObj->m_lodVisualList->GetCount() == 0) {
		wldObj->m_lodVisualList->SafeDelete();
		delete wldObj->m_lodVisualList;
		wldObj->m_lodVisualList = 0;
		return NULL;
	}

	return pMeshLodObj;
}

void CKEPEditView::OnBtnObjLodadd() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	//int			indexOfObject = GetTreeChildIndex ( treeHwnd, item );
	//HTREEITEM	ObjectRoot = TreeView_GetParent ( treeHwnd, item );
	HTREEITEM ObjectRoot = item;
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, ObjectRoot)).c_str();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;

		if (textLoc == pWO->m_fileNameRef) {
			CLodSystemDlg dlg;
			dlg.g_pd3dDeviceRef = g_pd3dDevice;

			dlg.m_lodType = 0; //ptrLoc->m_lodVisualList->m_lodType;
			if (dlg.DoModal() == IDOK) {
				if (IAssetsDatabase::Instance() != NULL) {
					CEXMeshObj *tmpMeshObj = IAssetsDatabase::Instance()->ImportStaticMesh(dlg.m_fileName);
					if (tmpMeshObj) {
						//
						// ptrContent->m_imMesh->AddLODLevel(dlg.m_fileName,dlg.m_activation);
						//
						ABBOX trnsferBx;
						trnsferBx.maxX = dlg.m_maxX;
						trnsferBx.maxY = dlg.m_maxY;
						trnsferBx.maxZ = dlg.m_maxZ;
						trnsferBx.minX = dlg.m_minX;
						trnsferBx.minY = dlg.m_minY;
						trnsferBx.minZ = dlg.m_minZ;

						AddLODToWorldObject(pWO, tmpMeshObj, dlg.m_lodType, dlg.m_activation, &trnsferBx);

						VisualPropertiesInList();

						EditorState::GetInstance()->SetZoneDirty();
					} else
						AfxMessageBox(L"Loading mesh failed\n");
				}
			}

			// end do it
			return;
		}
	} // end Begin world object loop

	AfxMessageBox(IDS_MSG_SELVISUAL);
#endif
}

void CKEPEditView::OnBTNENTITYAnimations() {
#ifndef _LimitedEngine
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();

	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!ptrLoc)
			continue;

		if (textLoc == ptrLoc->m_name) {
			CSkeletalAnimationDlg dlg;
			dlg.m_renderStateObjectRef = m_renderStateObject;
			dlg.m_globalShaderRef = m_cgGlobalShaderDB;
			dlg.GL_textureDataBase = m_pTextureDB;
			dlg.m_skeletonReferencePtr = new CSkeletonObject(ptrLoc->m_runtimeSkeleton, ptrLoc->m_skeletonConfiguration);
			dlg.directSoundRef = m_pDirectSound;
			dlg.m_elementDBREF = m_elementDB;
			dlg.m_skillDatabaseREF = m_skillDatabase;
			dlg.g_pd3dDevice = g_pd3dDevice;

			//Check just incase we dont have this from a previous version of the editor.
			if (!ptrLoc->m_stats)
				m_statDatabase->Clone(&ptrLoc->m_stats);

			dlg.DoModal();
			return;
		}
	} // end Begin world object loop

	AfxMessageBox(IDS_MSG_SELSKELETON);
#endif
#endif
}

void CKEPEditView::OnBTNAIDelete() {
	if (!m_AIOL) {
		return;
	}

	if (m_editorModeEnabled == FALSE) {
		return;
	}

	int runtimeIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_DB, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAIBOT);
		return;
	}

	POSITION posLoc = m_AIOL->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CAIObj *partPtr = (CAIObj *)m_AIOL->GetAt(posLoc);
	partPtr->SafeDelete();
	delete partPtr;
	partPtr = 0;
	m_AIOL->RemoveAt(posLoc);
	AI_DBInList(runtimeIndex - 1);
}

void CKEPEditView::OnBTNENTAppendCustomDB() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;

	// get selected index
	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);
	CAppendageCustDlg dlg;

	dlg.m_renderStateObjectRef = m_renderStateObject;
	dlg.m_cgGlobalShaderDB = m_cgGlobalShaderDB;
	dlg.m_skillDatabaseREF = m_skillDatabase;
	dlg.m_elementDBREF = m_elementDB;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_focusMovPtr = ptrLoc;
	dlg.m_dirSndRef = m_pDirectSound;

	dlg.DoModal();

	HCURSOR wait_cursor = LoadCursor(NULL, IDC_WAIT);
	HCURSOR old_cursor = SetCursor(wait_cursor);

	SetCursor(old_cursor);
	DestroyCursor(wait_cursor);

#endif
}

void CKEPEditView::OnSelchangeLISTENTITYRuntimeList() {
	InitializeEntityConfigurationsCombo(0);
	InListEntityDefcfgs(0);
}

void CKEPEditView::OnBTNENTDelLOD() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;
	CEntityLODSystemsDlg dlg;
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	POSITION posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);
	dlg.entityReference = ptrLoc;
	dlg.m_dirSndRef = m_pDirectSound;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.delOnlyMode();
	if (dlg.DoModal() == IDOK) {
		if (ptrLoc->m_skeletonLODList) {
			if (ptrLoc->m_skeletonLODList->GetCount() == 0) {
				ptrLoc->m_skeletonLODList->SafeDelete();
				delete ptrLoc->m_skeletonLODList;
				ptrLoc->m_skeletonLODList = NULL;
				ptrLoc->m_currentLOD = 0;
			}
		}
	}

#endif
}

void CKEPEditView::OnBTNENTLODSystems() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;
	CEntityLODSystemsDlg dlg;
	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);
	dlg.entityReference = ptrLoc;
	dlg.m_dirSndRef = m_pDirectSound;
	dlg.GL_textureDataBase = m_pTextureDB;
	if (dlg.DoModal() == IDOK) {
		if (ptrLoc->m_skeletonLODList) {
			if (ptrLoc->m_skeletonLODList->GetCount() == 0) {
				ptrLoc->m_skeletonLODList->SafeDelete();
				delete ptrLoc->m_skeletonLODList;
				ptrLoc->m_skeletonLODList = NULL;
				ptrLoc->m_currentLOD = 0;
			}
		}
	}

#endif
}

void CKEPEditView::OnBTNENTResourceBase() {
#ifndef _ClientOutput
	CResourceEditorDlg dlg;
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;
	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);
	dlg.resourceListRef = ptrLoc->m_resources;
	dlg.DoModal();
	ptrLoc->m_resources = dlg.resourceListRef;
#endif
}

void CKEPEditView::OnBTNAddHalo() {
}

void CKEPEditView::OnBTNDeleteHalo() {
}

void CKEPEditView::OnBTNEditHalo() {
}

void CKEPEditView::OnBTNAddLensFlare() {
}

void CKEPEditView::OnBTNEditLensFlare() {
}

void CKEPEditView::OnBTNDeleteLensFlare() {
}

void CKEPEditView::OnBTNHaloLoad() {
}

void CKEPEditView::OnBTNHaloSave() {
}

void CKEPEditView::OnBTNGenerateMesh() {
#ifndef _ClientOutput
	GenerateMeshDLG gm;
	gm.DoModal();
#endif
}

void CKEPEditView::OnBTNMISAddMissile() {
	// load type and mesh data
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"MapsModels").c_str();
	string filter;
	IAssetImporterFactory::Instance()->GetOpenDialogFilter(KEP::KEP_STATIC_MESH, filter);
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, Utf8ToUtf16(filter).c_str(), this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;

	CEXMeshObj *tmpMeshObj = 0;

	if (opendialog.DoModal() == IDOK) {
		CStringW filenamePathandAllW = opendialog.GetPathName();
		CStringA filenamePathandAll = Utf16ToUtf8(filenamePathandAllW).c_str();

		// load imediate mode mesh data
		if (0 != (tmpMeshObj = IAssetsDatabase::Instance()->ImportStaticMesh((const char *)filenamePathandAll))) {
			tmpMeshObj->InitAfterLoad(g_pd3dDevice);
			AddTextureToDatabase(tmpMeshObj); // apply mapping and add to DB
			GL_systemReadout->AddLine(operator+("Mesh Initialization Success:", filenamePathandAll), g_pd3dDevice, m_symbolMapDB, ctSystem);
		} else {
			GL_systemReadout->AddLine(operator+("Mesh Initialization Failed:", filenamePathandAll), g_pd3dDevice, m_symbolMapDB, ctSystem);
		}
	}

	if (!tmpMeshObj)
		return;

	CMissileObj *newMissilePtr = new CMissileObj();
	newMissilePtr->m_basicMesh = tmpMeshObj;
	newMissilePtr->m_typeOfProjectile = 0; // custom mesh

	CMissileObj *currentMissilePtr = this->GetSelectedMissileObj();
	if (currentMissilePtr) {
		//copy current missile obj to new obj
		newMissilePtr->m_missileName = currentMissilePtr->m_missileName;
		newMissilePtr->m_lifeTime = currentMissilePtr->m_lifeTime;
		newMissilePtr->m_damageValue = currentMissilePtr->m_damageValue;
		newMissilePtr->m_particleLink = currentMissilePtr->m_particleLink;
		newMissilePtr->m_explosionLink = currentMissilePtr->m_explosionLink;
		newMissilePtr->m_abilityType = currentMissilePtr->m_abilityType;
		newMissilePtr->m_skillType = currentMissilePtr->m_skillType;
		newMissilePtr->m_damageSpecific = currentMissilePtr->m_damageSpecific;
		newMissilePtr->m_baseKickPower = currentMissilePtr->m_baseKickPower;
		newMissilePtr->m_collisionRadius = currentMissilePtr->m_collisionRadius;
		newMissilePtr->m_heatSeekingPower = currentMissilePtr->m_heatSeekingPower;
		newMissilePtr->m_pushPower = currentMissilePtr->m_pushPower;

		newMissilePtr->m_damageChannel = currentMissilePtr->m_damageChannel;
		newMissilePtr->m_criteriaForSkillUse = currentMissilePtr->m_criteriaForSkillUse;

		newMissilePtr->m_forceExplodeAtEnd = currentMissilePtr->m_forceExplodeAtEnd;
		newMissilePtr->m_orientateToCamera = currentMissilePtr->m_orientateToCamera;
		newMissilePtr->m_useElastics = currentMissilePtr->m_useElastics;
		newMissilePtr->m_useFullPhysics = currentMissilePtr->m_useFullPhysics;
		newMissilePtr->m_burstForceType = currentMissilePtr->m_burstForceType;
		newMissilePtr->m_filterCollision = currentMissilePtr->m_filterCollision;
	}
	newMissilePtr->m_physicDynamics = new CMovementCaps();

	MissileDB->AddTail(newMissilePtr);
	m_missileDlgBar.SetSettingsChanged(TRUE);
	MissileDatabaseInList(MissileDB->GetCount() - 1);
}

void CKEPEditView::OnBTNMISDeleteMissile() {
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	if (missileIndex == -1) {
		AfxMessageBox(IDS_MSG_SELPROJECTILE);
		return;
	}

	POSITION posLoc = MissileDB->FindIndex(missileIndex);
	if (!posLoc) {
		return;
	}

	CMissileObj *misPtr = (CMissileObj *)MissileDB->GetAt(posLoc);
	misPtr->SafeDelete();
	delete misPtr;
	misPtr = 0;
	MissileDB->RemoveAt(posLoc);
	m_missileDlgBar.SetSettingsChanged(TRUE);
	MissileDatabaseInList(missileIndex == 0 ? 0 : missileIndex - 1);
}

void CKEPEditView::OnSelchangeLISTMISmissileDataBase() {
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	int previouslySelectedMissle = m_missileDlgBar.GetPreviousSelection();
	if (previouslySelectedMissle != -1) {
		//commit any changes made to previous selection
		POSITION posLoc = MissileDB->FindIndex(previouslySelectedMissle);
		if (!posLoc) {
			return;
		}

		CMissileObj *misPtr = this->GetMissileObj(previouslySelectedMissle);
		m_missileDlgBar.SetMissileObj(misPtr);
		m_missileDlgBar.SaveMissleObj();

		if (!m_missileDlgBar.IsSettingsApplied()) {
			MissileDB->SetDirty();
		}
		m_missileDlgBar.SettingsApplied();

		MissileDatabaseInList(previouslySelectedMissle);
	}
	if (missileIndex == -1) {
		return;
	}
	//put back user selection
	m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_SETCURSEL, missileIndex, 0);
	m_missileDlgBar.SetPreviousSelection(missileIndex);

	if (GetSelectedMissileObj()) {
		m_missileDlgBar.UpdateData(TRUE);
		LoadSelectedMissile();
		m_missileDlgBar.UpdateData(FALSE);
	}
}

void CKEPEditView::OnBTNMISReplaceValues() {
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	if (missileIndex == -1) {
		AfxMessageBox(IDS_MSG_SELPROJECTILE);
		return;
	}

	POSITION posLoc = MissileDB->FindIndex(missileIndex);
	if (!posLoc) {
		return;
	}

	CMissileObj *misPtr = this->GetSelectedMissileObj();

	m_missileDlgBar.SetMissileObj(misPtr);
	m_missileDlgBar.SaveMissleObj();

	MissileDatabaseInList(missileIndex);
}

void CKEPEditView::OnBTNMISEditVisual() {
#ifndef _ClientOutput
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	if (missileIndex == -1) {
		AfxMessageBox(IDS_MSG_SELPROJECTILE);
		return;
	}

	POSITION posLoc = MissileDB->FindIndex(missileIndex);
	if (!posLoc) {
		return;
	}

	CMissileObj *misPtr = (CMissileObj *)MissileDB->GetAt(posLoc);

	if (misPtr->m_typeOfProjectile == 0) { // basic missile
		CMissileBaseMeshEdit dlg;
		dlg.g_pd3dDeviceRef = g_pd3dDevice;
		dlg.GL_textureDataBase = m_pTextureDB;
		dlg.m_missileRef = misPtr;
		dlg.DoModal();
		if (misPtr->m_basicMesh) {
			AddTextureToDatabase(misPtr->m_basicMesh);
		}
	} // end basic missile

	if (misPtr->m_typeOfProjectile == 1) { // billboard
		CMissileAddBillboardDlg dlg;
		dlg.textureDBref = m_pTextureDB;
		dlg.m_missileRef = misPtr;
		if (dlg.DoModal() == IDOK) {
			float newBillboardSize = dlg.m_billboardSize * .5f;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[0].x = newBillboardSize;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[0].y = newBillboardSize;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[1].x = newBillboardSize;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[1].y = -newBillboardSize;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[2].x = -newBillboardSize;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[2].y = -newBillboardSize;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[3].x = -newBillboardSize;
			misPtr->m_basicMesh->m_abVertexArray.m_vertexArray1[3].y = newBillboardSize;
			misPtr->m_basicMesh->InitBuffer(g_pd3dDevice, NULL);
			if (misPtr->m_basicMesh) {
				AddTextureToDatabase(misPtr->m_basicMesh);
			}
		}
	} // end basic missile

	if (misPtr->m_typeOfProjectile == 2) { // skeletal
		CSkeletalAnimationDlg dlg;
		dlg.m_renderStateObjectRef = m_renderStateObject;
		dlg.m_globalShaderRef = m_cgGlobalShaderDB;
		dlg.m_elementDBREF = NULL;
		dlg.m_skillDatabaseREF = NULL;
		dlg.GL_textureDataBase = m_pTextureDB;
		dlg.m_skeletonReferencePtr = misPtr->m_skeletalVisual;
		dlg.directSoundRef = m_pDirectSound;
		dlg.g_pd3dDevice = g_pd3dDevice;
		dlg.DoModal();
		SkeletalTextureReassignment(misPtr->m_skeletalVisual->m_skeletonConfiguration);
	} // end //skeletal

	if (misPtr->m_typeOfProjectile == 3) { // laser object
		CLaserDlg dlg;
		dlg.GL_textureDataBase = m_pTextureDB;
		dlg.m_laserObjectRef = misPtr->m_laserObject;
		dlg.DoModal();
		misPtr->m_laserObject->InitializeMesh(g_pd3dDevice);
	} // end laser object

	MissileDB->SetDirty();
#endif
}

void CKEPEditView::OnBTNMISAddBillboardMissile() {
}

void CKEPEditView::OnBTNMISEditDynamics() {
#ifndef _ClientOutput
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	if (missileIndex == -1) {
		AfxMessageBox(IDS_MSG_SELPROJECTILE);
		return;
	}

	POSITION posLoc = MissileDB->FindIndex(missileIndex);
	if (!posLoc) {
		return;
	}

	CMissileObj *misPtr = (CMissileObj *)MissileDB->GetAt(posLoc);
	CMotionSettingsDlg dlg;
	dlg.movementDB = misPtr->m_physicDynamics;
	dlg.DoModal();
#endif
}

void CKEPEditView::OnBTNENTEditStartInven() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;

	// get selected index
	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);
	CEditStartInventoryDlg dlg;
	dlg.m_globalInventoryRef = m_globalInventoryDB;
	dlg.m_autoCfgRef = ptrLoc->m_autoCfg;
	dlg.DoModal();
	ptrLoc->m_autoCfg = dlg.m_autoCfgRef;
#endif
}

void CKEPEditView::OnDropdownCMBOCGTEXFontTableTexture() {
}

void CKEPEditView::OnBTNCGTEXAdd() {
}

void CKEPEditView::OnBTNCGTEXDelete() {
	int CGtextIndex = m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_CGTEX_LIST, CB_GETCURSEL, 0, 0);
	if (CGtextIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCGTEXT);
		return;
	}

	POSITION texPositionLoc = m_cgFontList->FindIndex(CGtextIndex);
	CGFontObj *fntObj = (CGFontObj *)m_cgFontList->GetAt(texPositionLoc);
	delete fntObj;
	fntObj = 0;
	m_cgFontList->RemoveAt(texPositionLoc);
	m_textFontDlgBar.SetSettingsChanged(TRUE);
	InListCGFonts(m_cgFontList->GetCount() - 1);
}

void CKEPEditView::InListCGFonts(int sel) {
	m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_CGTEX_LIST, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_cgFontList->GetHeadPosition(); posLoc != NULL;) {
		CGFontObj *fntObj = (CGFontObj *)m_cgFontList->GetNext(posLoc);
		m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_CGTEX_LIST, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16("CG:" + fntObj->m_fontName).c_str());
	}

	if (sel > -1) {
		m_textFontDlgBar.SendDlgItemMessage(IDC_LIST_CGTEX_LIST, LB_SETCURSEL, sel, 0);
	}
}

void CKEPEditView::OnBTNAIAddEvent() {
#ifndef _ClientOutput
	int entIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAISCRIPT);
		return;
	}

	CScriptEventAdd dlg;
	dlg.m_finalEventInt = 0;
	if (dlg.DoModal() == IDOK) {
		POSITION posIndex = m_pAISOL->FindIndex(entIndex);
		if (posIndex) {
			CAIScriptObj *script = (CAIScriptObj *)m_pAISOL->GetAt(posIndex);
			if (!script->m_events) {
				script->m_events = new CAIScriptEventList();
			}

			script->m_events->AddEvent(dlg.m_finalEventInt, dlg.m_miscInt);
			ScriptEventsInList(script->m_events->GetCount() - 1);
		}
	}

#endif
}

void CKEPEditView::ScriptInList(int sel) {
	m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_pAISOL->GetHeadPosition(); posLoc != NULL;) {
		CAIScriptObj *newScript = (CAIScriptObj *)m_pAISOL->GetNext(posLoc);
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(newScript->scriptName).c_str());
	}

	if (sel != -1) {
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_SETCURSEL, sel, 0);
	}
}

void CKEPEditView::OnBTNAIAddScript() {
#ifndef _ClientOutput
	CScriptAdd dlg;
	if (dlg.DoModal() == IDOK) {
		CAIScriptObj *newScript = new CAIScriptObj();
		newScript->scriptName = dlg.m_scriptName;
		newScript->scriptDescription = dlg.m_scriptDescription;

		m_pAISOL->AddTail(newScript);
		ScriptInList(m_pAISOL->GetCount() - 1);
	}

#endif
}

void CKEPEditView::OnBTNAIEditScript() {
#ifndef _ClientOutput
	CScriptAdd dlg;
	int scrIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_GETCURSEL, 0, 0);
	if (scrIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAISCRIPT);
		return;
	}

	POSITION posIndex = m_pAISOL->FindIndex(scrIndex);
	if (posIndex) {
		CAIScriptObj *script = (CAIScriptObj *)m_pAISOL->GetAt(posIndex);
		dlg.m_scriptName = script->scriptName;
		dlg.m_scriptDescription = script->scriptDescription;
		if (dlg.DoModal() == IDOK) {
			script->scriptName = dlg.m_scriptName;
			script->scriptDescription = dlg.m_scriptDescription;
			m_pAISOL->SetDirty();
			ScriptInList(scrIndex);
		}
	}

#endif
}

void CKEPEditView::OnBTNAIDeleteScript() {
	int scrIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_GETCURSEL, 0, 0);
	if (scrIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAISCRIPT);
		return;
	}

	POSITION posIndex = m_pAISOL->FindIndex(scrIndex);
	if (posIndex) {
		CAIScriptObj *script = (CAIScriptObj *)m_pAISOL->GetAt(posIndex);
		script->SafeDelete();
		delete script;
		script = 0;
		m_pAISOL->RemoveAt(posIndex);
		ScriptInList(scrIndex - 1);
	}
}

void CKEPEditView::OnBTNAIEditEvent() {
#ifndef _ClientOutput
	int entIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAISCRIPT);
		return;
	}

	int eventIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS, LB_GETCURSEL, 0, 0);
	if (eventIndex == -1) {
		AfxMessageBox(IDS_MSG_SELSCRIPTEVENT);
		return;
	}

	CScriptEventAdd dlg;
	POSITION posIndex = m_pAISOL->FindIndex(entIndex);
	if (posIndex) {
		CAIScriptObj *script = (CAIScriptObj *)m_pAISOL->GetAt(posIndex);
		if (!script->m_events) {
			return;
		}

		POSITION eventPos = script->m_events->FindIndex(eventIndex);
		if (eventPos) {
			CAIScriptEvent *scEvent = (CAIScriptEvent *)script->m_events->GetAt(eventPos);
			CScriptEventAdd dlg;
			dlg.m_finalEventInt = scEvent->m_event;
			dlg.m_miscInt = scEvent->m_miscInt;
			if (dlg.DoModal() == IDOK) { // 1
				scEvent->m_event = dlg.m_finalEventInt;
				scEvent->m_miscInt = dlg.m_miscInt;
				ScriptEventsInList(eventIndex);
			} // end 1
		}
	}

#endif
}

void CKEPEditView::OnBTNAIDeleteEvent() {
#ifndef _ClientOutput
	int entIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAISCRIPT);
		return;
	}

	int eventIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS, LB_GETCURSEL, 0, 0);
	if (eventIndex == -1) {
		AfxMessageBox(IDS_MSG_SELSCRIPTEVENT);
		return;
	}

	CScriptEventAdd dlg;
	POSITION posIndex = m_pAISOL->FindIndex(entIndex);
	if (posIndex) {
		CAIScriptObj *script = (CAIScriptObj *)m_pAISOL->GetAt(posIndex);
		if (!script->m_events) {
			return;
		}

		POSITION eventPos = script->m_events->FindIndex(eventIndex);
		if (eventPos) {
			CAIScriptEvent *scEvent = (CAIScriptEvent *)script->m_events->GetAt(eventPos);
			delete scEvent;
			scEvent = 0;
			script->m_events->RemoveAt(eventPos);
			ScriptEventsInList(eventIndex - 1);
		}
	}

#endif
}

void CKEPEditView::OnBTNAICreateNew() {
#ifndef _ClientOutput
	CAiSettingsDlg dlg;
	dlg.m_globalInventoryList = m_globalInventoryDB;
	dlg.m_edbRef = m_movObjRefModelList;
	dlg.m_refList = m_pAISOL;
	dlg.m_finalAiScriptAssign = -1;

	CAIObj *ai_ptr = new CAIObj();
	dlg.m_aiPtrRef = ai_ptr;
	if (dlg.DoModal() == IDOK) {
		ai_ptr = dlg.m_aiPtrRef;
		ai_ptr->m_spawnScript = dlg.m_finalAiScriptAssign;
		m_AIOL->AddTail(ai_ptr);
		m_AIOL->ResetIndexReferences();
		ai_ptr->m_targetFrame = new CFrameObj(NULL);
		AI_DBInList(m_AIOL->GetCount() - 1);
	}

#endif
}

void CKEPEditView::ScriptEventsInList(int sel) {
	int entIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		return;
	}

	POSITION posIndex = m_pAISOL->FindIndex(entIndex);
	if (posIndex) {
		CAIScriptObj *script = (CAIScriptObj *)m_pAISOL->GetAt(posIndex);
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS, LB_RESETCONTENT, 0, 0);
		if (script->m_events) {
			for (POSITION posLoc = script->m_events->GetHeadPosition(); posLoc != NULL;) {
				CAIScriptEvent *scEvent = (CAIScriptEvent *)script->m_events->GetNext(posLoc);
				if (scEvent->m_event == 0) {
					stringstream strBuilder;
					strBuilder << "GoTo :" << scEvent->m_miscInt;
					m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS,
						LB_ADDSTRING,
						0,
						(LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
				}

				if (scEvent->m_event == 1) {
					stringstream strBuilder;
					strBuilder << "Delay :" << scEvent->m_miscInt;
					m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS,
						LB_ADDSTRING,
						0,
						(LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
				}

				if (scEvent->m_event == 2) {
					stringstream strBuilder;
					strBuilder << "Play Animation :" << scEvent->m_miscInt;

					m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS,
						LB_ADDSTRING,
						0,
						(LPARAM)Utf8ToUtf16(strBuilder.str()).c_str());
				}

				if (scEvent->m_event == 3) {
					m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS, LB_ADDSTRING, 0, (LPARAM)L"GoTo Random :");
				}

				if (scEvent->m_event == 4) {
					m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS, LB_ADDSTRING, 0, (LPARAM)L"Reset Script :");
				}
			}
		}

		if (sel != -1) {
			m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS, LB_SETCURSEL, sel, 0);
		}
	}
}

void CKEPEditView::AITreeInList(int sel) {
	m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_WEBTREES, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_pAIWOL->GetHeadPosition(); posLoc != NULL;) {
		CAIWebObj *webObj = (CAIWebObj *)m_pAIWOL->GetNext(posLoc);
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_WEBTREES, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(webObj->m_name).c_str());
	}

	if (sel != -1) {
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_WEBTREES, LB_SETCURSEL, sel, 0);
	}
}

void CKEPEditView::OnBTNAIAddTree() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();

	CStringW fileNameW = L"<none>";

	// end use mapsmodels folder for organization
	static wchar_t BASED_CODE filter[] = L"TRE Files|*.tre";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		fileNameW = opendialog.GetPathName();
	} else {
		return;
	}

	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
	if (!res) {
		LOG_EXCEPTION(exc, m_logger)
		return;
	}

	BEGIN_SERIALIZE_TRY
	CArchive the_in_Archive(&fl, CArchive::load);
	CAIWebObj *webObj = NULL;
	the_in_Archive >> webObj;
	the_in_Archive.Close();
	fl.Close();
	m_pAIWOL->AddTail(webObj);
	EditorState::GetInstance()->SetZoneDirty();
	AITreeInList(m_pAIWOL->GetCount() - 1);
	END_SERIALIZE_TRY(m_logger)
}

void CKEPEditView::OnBTNAIDeleteTree() {
	int treeIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_WEBTREES, LB_GETCURSEL, 0, 0);
	if (treeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAITREE);
		return;
	}

	POSITION posIndex = m_pAIWOL->FindIndex(treeIndex);
	if (posIndex) {
		CAIWebObj *webObj = (CAIWebObj *)m_pAIWOL->GetAt(posIndex);
		webObj->SafeDelete();
		delete webObj;
		webObj = 0;
		m_pAIWOL->RemoveAt(posIndex);
		m_aiDBDlgBar.SetSettingsChanged(TRUE);
		EditorState::GetInstance()->SetZoneDirty();
		AITreeInList(m_pAIWOL->GetCount() - 1);
	}
}

void CKEPEditView::OnBTNAIEdit() {
#ifndef _ClientOutput
	int aiIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_DB, LB_GETCURSEL, 0, 0);
	if (aiIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAIBOT);
		return;
	}

	POSITION posLoc = m_AIOL->FindIndex(aiIndex);
	if (!posLoc) {
		return;
	}

	CAIObj *aiPtr = (CAIObj *)m_AIOL->GetAt(posLoc);

	CAiSettingsDlg dlg;
	dlg.m_globalInventoryList = m_globalInventoryDB;
	dlg.m_edbRef = m_movObjRefModelList;
	dlg.m_refList = m_pAISOL;
	dlg.m_finalAiScriptAssign = aiPtr->m_spawnScript;
	dlg.m_aiPtrRef = aiPtr;
	if (dlg.DoModal() == IDOK) {
		aiPtr = dlg.m_aiPtrRef;
		aiPtr->m_spawnScript = dlg.m_finalAiScriptAssign;
		m_AIOL->SetDirty();
		AI_DBInList(aiIndex);
	}

#endif
}

void CKEPEditView::OnBTNAISaveAiData() {
	BOOL newFile = m_aiDBDlgBar.GetLoadedFile() == "";
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"AI Data Files (*.aii)|*.AII|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"AII", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << m_AIOL << m_pAISOL;
		the_out_Archive.Close();
		fl.Close();
		m_aiDBDlgBar.SetLoadedFile(fileNameW);
		END_SERIALIZE_TRY(m_logger)

		if (m_aiDBDlgBar.GetLoadedFile() != "" && newFile) {
			//update script event
			CStringW sCaption, sText;
			sCaption.LoadString(IDS_CHANGE_SETTINGS);
			sText.LoadString(IDS_CONFIRM_ADDING_SCRIPT_EVENT);
			int ret = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNO);
			if (ret == IDYES) {
				m_aiDBDlgBar.UpdateScript();
			}
		}
	}
}

void CKEPEditView::OnBTNAILoadAiData() {
	//TODO reset pointers when deseriliazation fails
	//singleton aiclass
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"AI Data Files (*.aii)|*.AII|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();

		CStringA oldFile = m_aiFilePtr->GetFileName().c_str();
		if (LoadAiData(fileName.GetString())) {
			m_aiDBDlgBar.SetLoadedFile(fileName);
		} else {
			AfxMessageBox(IDS_MSG_FAILED_LOADING_GAME_FILE, MB_ICONEXCLAMATION);
			//try reloading previous file
			if (oldFile == "" || !LoadAiData(oldFile.GetString())) {
				//reset pointers
				m_AIOL = new CAIObjList;
				m_pAISOL = new CAIScriptObjList;
			}
		}
	}
}

void CKEPEditView::OnSelchangeLISTAIScripts() {
	m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_EVENTS, LB_RESETCONTENT, 0, 0);

	int entIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_SCRIPTS, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		return;
	}

	POSITION posIndex = m_pAISOL->FindIndex(entIndex);
	if (posIndex) {
		CAIScriptObj *script = (CAIScriptObj *)m_pAISOL->GetAt(posIndex);
		if (script->m_events) {
			ScriptEventsInList(script->m_events->GetCount() - 1);
		}
	}
}

void CKEPEditView::OnBTNENTSaveAppendages() {
	EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
	if (!equippableRM)
		return;

	if (equippableRM->GetResourceCount() == 0) {
		AfxMessageBox(IDS_MSG_NOEQUIPPABLEITEMS);
		return;
	}

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Equippable Items Files (*.apd)|*.APD|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"APD", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
		if (!equippableRM)
			return;

		CArmedInventoryList *tempList = equippableRM->GetLegacyAPD();
		BEGIN_SERIALIZE_TRY
		::ServerSerialize = false;
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << tempList;
		the_out_Archive.Close();
		fl.Close();

		CFile fl2;
		if (!fl2.Open(fileNameW + L".Server", CFile::modeCreate | CFile::modeWrite, &exc)) {
			LOG_EXCEPTION(exc, m_logger)
		} else {
			::ServerSerialize = true;
			CArchive the_out_Archive(&fl2, CArchive::store);
			the_out_Archive << tempList;
			the_out_Archive.Close();
			fl2.Close();
		}

		END_SERIALIZE_TRY(m_logger)
		tempList->RemoveAll();
		tempList->SafeDelete();
		delete tempList;
		::ServerSerialize = false;
	}

	string myPath = ((IClientEngine *)IClientEngine::Instance())->PathBase();
	myPath = myPath + "\\GameFiles\\EquippableItems\\";
	EquippableRM::Instance()->SaveLooseEquippableItems(myPath, CGlobalInventoryObjList::GetInstance());
}

void CKEPEditView::OnBTNENTLoadAppendages() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Equippable Items Files (*.apd)|*.APD|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() != IDOK) {
	}

	CStringW fileNameW = opendialog.GetFileName();
	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION(exc, m_logger)
		return;
	}

	CArmedInventoryList *tempList = nullptr;
	BEGIN_SERIALIZE_TRY
	CArchive the_in_Archive(&fl, CArchive::load);
	the_in_Archive >> tempList;
	the_in_Archive.Close();
	fl.Close();
	END_SERIALIZE_TRY(m_logger)

	if (serializeOk) {
		EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
		if (equippableRM) {
			equippableRM->LoadFromAPD(tempList);
		}
	}

	if (tempList) {
		tempList->SafeDelete();
		delete tempList;
	}
}

void CKEPEditView::OnBtnEntMount() {
	POSITION posLoc = m_movObjList->FindIndex(0);
	if (!posLoc) {
		AfxMessageBox(IDS_MSG_NOUSERCREATED);
		return;
	}

	CMovementObj *hostPtr = (CMovementObj *)m_movObjList->GetAt(posLoc);
	int aiselect = m_entityDlgBar.GetDlgItemInt(IDC_EDIT_ENT_MOUNTAIINDEX, NULL, TRUE);

	GenAndMount(hostPtr, aiselect);
}

void CKEPEditView::OnBtnEntDemount() {
	POSITION posLoc = m_movObjList->FindIndex(0);
	if (!posLoc) {
		AfxMessageBox(IDS_MSG_NOUSERCREATED);
		return;
	}

	CMovementObj *hostPtr = (CMovementObj *)m_movObjList->GetAt(posLoc);

	DeMount(hostPtr);
}

void CKEPEditView::OnBTNEXPEditExplosionObj() {
#ifndef _ClientOutput
	int runtimeIndex = m_explosionDlgBar.SendDlgItemMessage(IDC_LIST_EXPLOSIONLIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELEXPLOSION);
		return;
	}

	POSITION posLoc = m_explosionDBList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CExplosionObj *expObj = (CExplosionObj *)m_explosionDBList->GetAt(posLoc);
	CExplosionBasicDlg dlg;
	dlg.m_dirSndRef = m_pDirectSound;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.m_explosionRef = expObj;
	dlg.m_elementDBREF = m_elementDB;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_skillDatabaseRef = m_skillDatabase;
	if (dlg.DoModal() == IDOK) {
		m_explosionDBList->SetDirty();
	}

	InListExplosionDB(runtimeIndex);
#endif
}

void CKEPEditView::OnBTNEXPDeleteExplosion() {
	int runtimeIndex = m_explosionDlgBar.SendDlgItemMessage(IDC_LIST_EXPLOSIONLIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELEXPLOSION);
		return;
	}

	POSITION posLoc = m_explosionDBList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CExplosionObj *expObj = (CExplosionObj *)m_explosionDBList->GetAt(posLoc);
	expObj->SafeDelete();
	delete expObj;
	expObj = 0;
	m_explosionDBList->RemoveAt(posLoc);
	m_explosionDlgBar.SetSettingsChanged(TRUE);

	InListExplosionDB(runtimeIndex - 1);
}

void CKEPEditView::OnBTNEXPLoadExplosions() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Explosions Database Files (*.exp)|*.EXP|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		if (LoadExplosionFile(fileName.GetString())) {
			m_explosionDlgBar.SetLoadedFile(fileName);
		}
	}
}

void CKEPEditView::OnBTNEXPSaveExplosions() {
#ifndef _LimitedEngine
	BOOL newFile = m_explosionDlgBar.GetLoadedFile() == "";
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Explosions Database Files (*.exp)|*.EXP|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EXP", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << m_explosionDBList;
		the_out_Archive.Close();
		fl.Close();
		m_explosionDlgBar.SetLoadedFile(fileNameW);
		CFile fl2;
		if (!fl2.Open(fileNameW + L".Server", CFile::modeCreate | CFile::modeWrite, &exc)) {
			LOG_EXCEPTION(exc, m_logger)
		} else {
			::ServerSerialize = true;
			CArchive the_out_Archive(&fl2, CArchive::store);
			the_out_Archive << m_explosionDBList;
			the_out_Archive.Close();
			fl2.Close();
		}
		END_SERIALIZE_TRY(m_logger)
		::ServerSerialize = false;

		if (m_explosionDlgBar.GetLoadedFile() != "" && newFile) {
			//update script event
			CStringW sCaption, sText;
			sCaption.LoadString(IDS_CHANGE_SETTINGS);
			sText.LoadString(IDS_CONFIRM_ADDING_SCRIPT_EVENT);
			int ret = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNO);
			if (ret == IDYES) {
				m_explosionDlgBar.UpdateScript();
			}
		}
	}

#endif
}

void CKEPEditView::OnSelectionChangeEntityMeshSlot() {
	int entitySelectionIndex;

	entitySelectionIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, CB_GETCURSEL, 0, 0);
	if (-1 == entitySelectionIndex)
		return;

	int meshSlotSelectionIndex;
	meshSlotSelectionIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_SLOT, CB_GETCURSEL, 0, 0);
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_CONFIG, CB_RESETCONTENT, 0, 0);
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MATERIAL_CONFIG, CB_RESETCONTENT, 0, 0);

	POSITION entityPosition;
	entityPosition = m_movObjList->FindIndex(entitySelectionIndex);
	if (!entityPosition)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPosition));
	if (!pMovementObj || !pMovementObj->m_runtimeSkeleton)
		return;

	// Update the deformable mesh list box with the new movement objects
	// list of alternate configurations.
	POSITION entityReferencePosition;
	entityReferencePosition = m_movObjRefModelList->FindIndex(pMovementObj->m_idxRefModel);
	if (!entityReferencePosition)
		return;

	CMovementObj *pMovementObjReference;
	CDeformableMeshList *pSelectionDeformableMeshList;

	pSelectionDeformableMeshList = 0;
	pMovementObjReference = static_cast<CMovementObj *>(m_movObjRefModelList->GetAt(entityReferencePosition));
	if (!pMovementObjReference || !pMovementObjReference->m_runtimeSkeleton || !pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations)
		return;

	if (pMovementObjReference->m_runtimeSkeleton->m_deformableMeshArrayCount) {
		POSITION alternativeConfigPosition;
		alternativeConfigPosition = pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations->FindIndex(meshSlotSelectionIndex);
		if (alternativeConfigPosition) {
			CAlterUnitDBObject *pAlternativeUnitObj;
			pAlternativeUnitObj = static_cast<CAlterUnitDBObject *>(pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations->GetAt(alternativeConfigPosition));
			if (pAlternativeUnitObj) {
				pSelectionDeformableMeshList = pAlternativeUnitObj->m_configurations;
			} // if..pAlternativeUnitObj
		} // if..alPos
	} // if..pMovementObjReference->m_skeletalObject->m_deformableeMeshArrayCount

	int meshConfigurationIndex = -1;
	int materialConfigurationIndex = -1;
	if (pMovementObj->m_skeletonConfiguration->m_newConfig.ItemCount()) {
		ConfigInfo *pConfigInfo;
		pConfigInfo = pMovementObj->m_skeletonConfiguration->m_newConfig.at(0);
		if (pConfigInfo) {
			if (meshSlotSelectionIndex < (int)pConfigInfo->slotCount) {
				meshConfigurationIndex = pConfigInfo->slotConfigs[meshSlotSelectionIndex].meshIndex;
				materialConfigurationIndex = pConfigInfo->slotConfigs[meshSlotSelectionIndex].matIndex;
			}
		}
	}

	InitializeEntityMeshConfigurationsCombo(meshConfigurationIndex, materialConfigurationIndex, pSelectionDeformableMeshList);
}

void CKEPEditView::OnSelectionChangeEntityMeshConfig() {
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MATERIAL_CONFIG, CB_RESETCONTENT, 0, 0);

	int entitySelectionIndex;
	entitySelectionIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, CB_GETCURSEL, 0, 0);
	if (-1 == entitySelectionIndex)
		return;

	int meshSlotSelectionIndex;
	meshSlotSelectionIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_SLOT, CB_GETCURSEL, 0, 0);
	if (-1 == meshSlotSelectionIndex)
		return;

	int meshConfigurationIndex;
	meshConfigurationIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_CONFIG, CB_GETCURSEL, 0, 0);
	if (-1 == meshConfigurationIndex)
		return;

	POSITION entityPosition;
	entityPosition = m_movObjList->FindIndex(entitySelectionIndex);
	if (!entityPosition)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPosition));
	if (!pMovementObj || !pMovementObj->m_runtimeSkeleton)
		return;

	// Update the deformable mesh list box with the new movement objects
	// list of alternate configurations.
	POSITION entityReferencePosition;
	entityReferencePosition = m_movObjRefModelList->FindIndex(pMovementObj->m_idxRefModel);
	if (!entityReferencePosition)
		return;

	CMovementObj *pMovementObjReference;
	CObList *pSelectionMaterialList = NULL; // DRF - potentially uninitialized variable
	CDeformableMeshList *pSelectionDeformableMeshList;

	pSelectionDeformableMeshList = 0;
	pMovementObjReference = static_cast<CMovementObj *>(m_movObjRefModelList->GetAt(entityReferencePosition));
	if (!pMovementObjReference || !pMovementObjReference->m_runtimeSkeleton || !pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations)
		return;

	BOOL result;
	result = pMovementObj->m_skeletonConfiguration->HotSwapDim(meshSlotSelectionIndex, meshConfigurationIndex, pMovementObjReference->m_runtimeSkeleton->m_deformableMeshArrayCount, TRUE);
	if (!result)
		return;

	if (pMovementObjReference->m_runtimeSkeleton->m_deformableMeshArrayCount) {
		POSITION alternativeConfigPosition;
		alternativeConfigPosition = pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations->FindIndex(meshSlotSelectionIndex);
		if (alternativeConfigPosition) {
			CAlterUnitDBObject *pAlternativeUnitObj;
			pAlternativeUnitObj = static_cast<CAlterUnitDBObject *>(pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations->GetAt(alternativeConfigPosition));
			if (pAlternativeUnitObj && pAlternativeUnitObj->m_configurations) {
				pSelectionDeformableMeshList = pAlternativeUnitObj->m_configurations;
			} // if..pAlternativeUnitObj
		} // if..alternativeConfigPosition
	} // if..pMovementObjReference->m_skeletalObject->m_deformableeMeshArrayCount

	if (pSelectionDeformableMeshList) {
		CDeformableMesh *pDeformableMesh = 0;
		POSITION deformableMeshPosition;

		deformableMeshPosition = pSelectionDeformableMeshList->FindIndex(meshConfigurationIndex);
		if (deformableMeshPosition) {
			pDeformableMesh = dynamic_cast<CDeformableMesh *>(pSelectionDeformableMeshList->GetAt(deformableMeshPosition));
			if (pDeformableMesh) {
				pSelectionMaterialList = pDeformableMesh->m_supportedMaterials;
			} // if..pDeformableMesh
		} // if..deformableMeshPosition
	} // if..pSelectionDeformableMeshList

	int materialConfigurationIndex = -1;
	if (pMovementObj->m_skeletonConfiguration->m_newConfig.ItemCount()) {
		ConfigInfo *pConfigInfo;
		pConfigInfo = pMovementObj->m_skeletonConfiguration->m_newConfig.at(0);
		if (pConfigInfo) {
			if (meshSlotSelectionIndex < (int)pConfigInfo->slotCount)
				materialConfigurationIndex = pConfigInfo->slotConfigs[meshSlotSelectionIndex].matIndex;
		}
	}

	InitializeEntityMaterialConfigurationsCombo(materialConfigurationIndex, pSelectionMaterialList);

	pMovementObj->RealizeNow(pMovementObjReference);
}

void CKEPEditView::OnSelectionChangeEntityMaterialConfig() {
	int entitySelectionIndex;
	entitySelectionIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, CB_GETCURSEL, 0, 0);
	if (-1 == entitySelectionIndex)
		return;

	int meshSlotSelectionIndex;
	meshSlotSelectionIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_SLOT, CB_GETCURSEL, 0, 0);
	if (-1 == meshSlotSelectionIndex)
		return;

	int meshConfigurationIndex;
	meshConfigurationIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_CONFIG, CB_GETCURSEL, 0, 0);
	if (-1 == meshConfigurationIndex)
		return;

	int materialConfigurationIndex;
	materialConfigurationIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MATERIAL_CONFIG, CB_GETCURSEL, 0, 0);
	if (-1 == materialConfigurationIndex)
		return;

	POSITION entityPosition;
	entityPosition = m_movObjList->FindIndex(entitySelectionIndex);
	if (!entityPosition)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPosition));
	if (!pMovementObj || !pMovementObj->m_runtimeSkeleton)
		return;

	// Update the deformable mesh list box with the new movement objects
	// list of alternate configurations.
	POSITION entityReferencePosition;
	entityReferencePosition = m_movObjRefModelList->FindIndex(pMovementObj->m_idxRefModel);
	if (!entityReferencePosition)
		return;

	CMovementObj *pMovementObjReference;
	pMovementObjReference = static_cast<CMovementObj *>(m_movObjRefModelList->GetAt(entityReferencePosition));
	if (!pMovementObjReference || !pMovementObjReference->m_runtimeSkeleton || !pMovementObjReference->m_skeletonConfiguration || !pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations)
		return;

	BOOL result;
	result = pMovementObj->m_skeletonConfiguration->HotSwapMat(meshSlotSelectionIndex, meshConfigurationIndex, materialConfigurationIndex, TRUE);
	if (!result)
		return;

	pMovementObj->RealizeNow(pMovementObjReference);
}

void CKEPEditView::InitializeEntityConfigurationsCombo(int selection) {
	int entitySelectionIndex;

	entitySelectionIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, CB_GETCURSEL, 0, 0);
	if (-1 == entitySelectionIndex)
		return;

	POSITION entityPosition;
	entityPosition = m_movObjList->FindIndex(entitySelectionIndex);
	if (!entityPosition)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPosition));
	if (!pMovementObj)
		return;

	// Update the deformable mesh list box with the new movement objects
	// list of alternate configurations.
	POSITION entityReferencePosition;
	entityReferencePosition = m_movObjRefModelList->FindIndex(pMovementObj->m_idxRefModel);
	if (!entityReferencePosition)
		return;

	CMovementObj *pMovementObjReference;
	CDeformableMeshList *pSelectionDeformableMeshList;

	pSelectionDeformableMeshList = 0;
	pMovementObjReference = static_cast<CMovementObj *>(m_movObjRefModelList->GetAt(entityReferencePosition));
	if (!pMovementObjReference)
		return;

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_SLOT, CB_RESETCONTENT, 0, 0);
	if (pMovementObjReference->m_runtimeSkeleton->m_deformableMeshArrayCount) {
		for (int deformableMeshIndex = 0; deformableMeshIndex < pMovementObjReference->m_runtimeSkeleton->m_deformableMeshArrayCount; deformableMeshIndex++) {
			CStringA label = "No Name";
			POSITION alternativeConfigPosition;
			alternativeConfigPosition = pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations->FindIndex(deformableMeshIndex);
			if (alternativeConfigPosition) {
				CAlterUnitDBObject *pAlternativeUnitObj;
				pAlternativeUnitObj = static_cast<CAlterUnitDBObject *>(pMovementObjReference->m_skeletonConfiguration->m_alternateConfigurations->GetNext(alternativeConfigPosition));
				if (pAlternativeUnitObj) {
					if (pAlternativeUnitObj->m_alterUnitName != "")
						label = pAlternativeUnitObj->m_alterUnitName;

					if (deformableMeshIndex == selection) {
						pSelectionDeformableMeshList = pAlternativeUnitObj->m_configurations;
					} // if..deformableMeshIndex == selection
				} // if..pAlternativeUnitObj
			} // if..alPos

			m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_SLOT, CB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(label).c_str());
		} // for..deformableMeshIndex < pMovementObjReference->m_skeletalObject->m_deformableMeshArrayCount
	} // if..pMovementObjReference->m_skeletalObject->m_deformableeMeshArrayCount

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_SLOT, CB_SETCURSEL, selection, 0);

	int meshConfigurationIndex = -1;
	int materialConfigurationIndex = -1;
	if (pMovementObj->m_skeletonConfiguration->m_newConfig.ItemCount()) {
		ConfigInfo *pConfigInfo;
		pConfigInfo = pMovementObj->m_skeletonConfiguration->m_newConfig.at(0);
		if (pConfigInfo && pConfigInfo->slotCount) {
			meshConfigurationIndex = pConfigInfo->slotConfigs[0].meshIndex;
			materialConfigurationIndex = pConfigInfo->slotConfigs[0].matIndex;
		}
	}

	InitializeEntityMeshConfigurationsCombo(meshConfigurationIndex, materialConfigurationIndex, pSelectionDeformableMeshList);
}

void CKEPEditView::InitializeEntityMeshConfigurationsCombo(int meshConfigurationIndex, int materialConfirguationIndex, CDeformableMeshList *pDeformableMeshList) {
	if (!pDeformableMeshList)
		return;

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_CONFIG, CB_RESETCONTENT, 0, 0);

	CObList *pMaterialList = 0;
	int meshIndex = 0;
	POSITION deformableMeshPosition;

	for (deformableMeshPosition = pDeformableMeshList->GetHeadPosition(); deformableMeshPosition; meshIndex++) {
		CStringA label = "No Name";
		CDeformableMesh *pDeformableMesh = 0;

		pDeformableMesh = dynamic_cast<CDeformableMesh *>(pDeformableMeshList->GetNext(deformableMeshPosition));
		if (pDeformableMesh) {
			label = pDeformableMesh->m_dimName;
			if (meshIndex == meshConfigurationIndex) {
				pMaterialList = pDeformableMesh->m_supportedMaterials;
			} // if..meshIndex == meshConfigurationIndex
		} // if..alPos

		m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_CONFIG, CB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(label).c_str());
	} // for..deformableMeshPosition

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MESH_CONFIG, CB_SETCURSEL, meshConfigurationIndex, 0);
	InitializeEntityMaterialConfigurationsCombo(materialConfirguationIndex, pMaterialList);
} // CKEPEditView::InitializeEntityMeshConfigurationsCombo

void CKEPEditView::InitializeEntityMaterialConfigurationsCombo(int materialConfigurationIndex, CObList *pMaterialList) {
	if (!pMaterialList)
		return;

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MATERIAL_CONFIG, CB_RESETCONTENT, 0, 0);

	POSITION materialPosition;
	for (materialPosition = pMaterialList->GetHeadPosition(); materialPosition;) {
		CStringA label = "No Name";
		CMaterialObject *pMaterial = 0;

		pMaterial = dynamic_cast<CMaterialObject *>(pMaterialList->GetNext(materialPosition));
		if (pMaterial) {
			label = pMaterial->m_materialName;
		} // if..alPos

		m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MATERIAL_CONFIG, CB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(label).c_str());
	} // for..materialPosition

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_ENTITY_MATERIAL_CONFIG, CB_SETCURSEL, materialConfigurationIndex, 0);
} // CKEPEditView::InitializeEntityMaterialConfigurationsCombo

void CKEPEditView::InListEntityDefcfgs(int sel) {
	int entIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entIndex == -1)
		return;

	POSITION posloc = m_movObjList->FindIndex(entIndex);
	if (!posloc)
		return;

	m_entityDlgBar.SendDlgItemMessage(IDC_LIST_DEFSWAPOPTIONS, LB_RESETCONTENT, 0, 0);
	m_entityDlgBar.SendDlgItemMessage(IDC_LIST_EQUIPABLE_ITEMS, CB_RESETCONTENT, 0, 0);
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_SLOT, CB_RESETCONTENT, 0, 0);
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, CB_RESETCONTENT, 0, 0);
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MATERIAL, CB_RESETCONTENT, 0, 0);

	CMovementObj *movObj = static_cast<CMovementObj *>(m_movObjList->GetAt(posloc));
	if (movObj) {
		// Update the deformable mesh list box with the new movement objects
		// list of alternate configurations.
		POSITION posRef = m_movObjRefModelList->FindIndex(movObj->m_idxRefModel);
		if (posRef) {
			CMovementObj *dbPtr = static_cast<CMovementObj *>(m_movObjRefModelList->GetAt(posRef));
			if (dbPtr && dbPtr->m_runtimeSkeleton && dbPtr->m_skeletonConfiguration->m_alternateConfigurations) {
				if (dbPtr->m_runtimeSkeleton->m_deformableMeshArrayCount) {
					for (int loop = 0; loop < dbPtr->m_runtimeSkeleton->m_deformableMeshArrayCount; loop++) {
						CStringA label = "Deform Cfg";
						POSITION alPos = dbPtr->m_skeletonConfiguration->m_alternateConfigurations->FindIndex(loop);
						if (alPos) {
							CAlterUnitDBObject *alPtr = static_cast<CAlterUnitDBObject *>(dbPtr->m_skeletonConfiguration->m_alternateConfigurations->GetNext(alPos));
							if (alPtr->m_alterUnitName != "")
								label = alPtr->m_alterUnitName;
						} // if..alPos

						m_entityDlgBar.SendDlgItemMessage(IDC_LIST_DEFSWAPOPTIONS, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(label).c_str());
					} // for..loop < dbPtr->m_skeletalObject->m_deformableMeshArrayCount
				} // if..dbPtr->m_skeletalObject->m_deformableeMeshArrayCount
			} // if..dbPtr->m_skeletonConfiguration->m_alternateConfigurations
		} // if..posRef

		// Update the new movement objects list of equipable items, along with
		// the currently selected equipable items list of available materials.
		if (movObj->m_runtimeSkeleton->m_armedInventoryDB) {
			POSITION posInv;
			posInv = movObj->m_runtimeSkeleton->m_armedInventoryDB->GetHeadPosition();
			for (; posInv; movObj->m_runtimeSkeleton->m_armedInventoryDB->GetNext(posInv)) {
				CArmedInventoryObj *pArmedInv;
				pArmedInv = static_cast<CArmedInventoryObj *>(movObj->m_runtimeSkeleton->m_armedInventoryDB->GetAt(posInv));
				if (pArmedInv) {
					const char *pEquipItemName;
					pEquipItemName = static_cast<const char *>(pArmedInv->m_name);
					m_entityDlgBar.SendDlgItemMessage(IDC_LIST_EQUIPABLE_ITEMS, LB_ADDSTRING, 0, reinterpret_cast<LPARAM>(Utf8ToUtf16(pEquipItemName).c_str()));
				} // if..pArmedInv
			} // for..!posInv

			m_entityDlgBar.SendDlgItemMessage(IDC_LIST_EQUIPABLE_ITEMS, LB_SETCURSEL, sel, 0);
		} // if..movObj->m_armedInventoryDB
	} // if..movObj

	InitializeEquippedItemMeshSlot(0);
}

void CKEPEditView::InitializeEquippedItemMeshSlot(int meshSlotIndex) {
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_SLOT, CB_RESETCONTENT, 0, 0);

	int entityIndex;
	entityIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entityIndex == -1)
		return;

	POSITION entityPos;
	entityPos = m_movObjList->FindIndex(entityIndex);
	if (!entityPos)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPos));
	if (!pMovementObj || !pMovementObj->m_runtimeSkeleton || !pMovementObj->m_runtimeSkeleton->m_armedInventoryDB)
		return;

	POSITION equipItemPosition;
	equipItemPosition = pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->GetHeadPosition();

	int equippedItemIndex;
	equippedItemIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_EQUIPABLE_ITEMS, LB_GETCURSEL, 0, 0);
	if (equippedItemIndex == -1 || !pMovementObj->m_runtimeSkeleton->m_armedInventoryDB)
		return;

	POSITION equippedItemPos;
	equippedItemPos = pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->FindIndex(equippedItemIndex);
	if (!equippedItemPos)
		return;

	CArmedInventoryObj *pRuntimeArmedInv;
	pRuntimeArmedInv = static_cast<CArmedInventoryObj *>(pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->GetAt(equippedItemPos));
	if (!pRuntimeArmedInv)
		return;

	EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
	if (!equippableRM)
		return;

	CArmedInventoryObj *pGlobalArmedInv = equippableRM->GetProxy(pRuntimeArmedInv->m_type, true)->GetData();
	if (!pGlobalArmedInv)
		return;

	SkeletonConfiguration *skeletalCfg = pGlobalArmedInv->m_thirdPersonSystem->m_skeletonConfiguration;
	if (skeletalCfg) {
		if (!skeletalCfg->m_alternateConfigurations)
			return;

		POSITION configPos;
		CAlterUnitDBObject *pAlterUnitObj;
		CAlterUnitDBObject *pSelectedAlterUnitObj;

		pSelectedAlterUnitObj = 0;
		configPos = skeletalCfg->m_alternateConfigurations->GetHeadPosition();
		for (size_t configIndex = 0; configPos; ++configIndex) {
			pAlterUnitObj = reinterpret_cast<CAlterUnitDBObject *>(skeletalCfg->m_alternateConfigurations->GetNext(configPos));
			if (configIndex == meshSlotIndex)
				pSelectedAlterUnitObj = pAlterUnitObj;
			m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_SLOT, CB_ADDSTRING, static_cast<WPARAM>(-1), reinterpret_cast<LPARAM>(Utf8ToUtf16(pAlterUnitObj->m_alterUnitName).c_str()));
		}

		if ((meshSlotIndex >= 0) && (meshSlotIndex < (int)skeletalCfg->m_charConfig.at(BASE_CONFIG)->slotCount)) {
			int meshIndex;
			int materialIndex;

			meshIndex = skeletalCfg->m_charConfig.at(BASE_CONFIG)->slotConfigs[meshSlotIndex].meshIndex;
			materialIndex = skeletalCfg->m_charConfig.at(BASE_CONFIG)->slotConfigs[meshSlotIndex].matIndex;
			m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_SLOT, CB_SETCURSEL, meshSlotIndex, 0);
			InitializeEquippedItemMesh(pSelectedAlterUnitObj, meshIndex, materialIndex);
			return;
		}
	}

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, CB_RESETCONTENT, 0, 0);
} // CKEPEditView::InitializeEquippedItemMeshSlot

void CKEPEditView::InitializeEquippedItemMesh(CAlterUnitDBObject *pAlterUnitObj, int selectedMeshIndex, int selectedMaterialIndex) {
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, CB_RESETCONTENT, 0, 0);
	if (!pAlterUnitObj || !pAlterUnitObj->m_configurations)
		return;

	POSITION meshPosition;
	CDeformableMesh *pDeformMesh;
	CDeformableMesh *pSelectedDeformMesh;

	pSelectedDeformMesh = 0;
	meshPosition = pAlterUnitObj->m_configurations->GetHeadPosition();
	for (int meshIndex = 0; meshPosition; ++meshIndex) {
		pDeformMesh = reinterpret_cast<CDeformableMesh *>(pAlterUnitObj->m_configurations->GetNext(meshPosition));

		if (meshIndex == selectedMeshIndex)
			pSelectedDeformMesh = pDeformMesh;

		m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, CB_ADDSTRING, static_cast<WPARAM>(-1), reinterpret_cast<LPARAM>(Utf8ToUtf16(pDeformMesh->m_dimName).c_str()));
	} // for..meshPosition

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, CB_SETCURSEL, 0, static_cast<WPARAM>(selectedMeshIndex));

	if (pSelectedDeformMesh)
		InitializeEquippedItemMaterial(pSelectedDeformMesh->m_supportedMaterials, selectedMaterialIndex);
	else
		m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MATERIAL, CB_RESETCONTENT, 0, 0);
} // CKEPEditView::InitializeEquippedItemMesh

void CKEPEditView::InitializeEquippedItemMaterial(CObList *pMaterialList, int selectedMaterialIndex) {
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MATERIAL, CB_RESETCONTENT, 0, 0);
	if (!pMaterialList)
		return;

	POSITION materialPosition;
	CMaterialObject *pMaterialObj;
	CMaterialObject *pSelectedMaterialObj;

	pSelectedMaterialObj = 0;
	materialPosition = pMaterialList->GetHeadPosition();
	for (int materialIndex = 0; materialPosition; ++materialIndex) {
		pMaterialObj = reinterpret_cast<CMaterialObject *>(pMaterialList->GetNext(materialPosition));

		if (materialIndex == selectedMaterialIndex)
			pSelectedMaterialObj = pMaterialObj;

		m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MATERIAL, CB_ADDSTRING, static_cast<WPARAM>(-1), reinterpret_cast<LPARAM>(Utf8ToUtf16(pMaterialObj->m_materialName).c_str()));
	} // for..materialPosition

	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MATERIAL, CB_SETCURSEL, 0, static_cast<WPARAM>(selectedMaterialIndex));
} // CKEPEditView::InitializeEquippedItemMaterial

void CKEPEditView::OnSelchangeEquipItem() {
	InitializeEquippedItemMeshSlot(0);
} // CKEPEditView..OnSelchangeEquipItem

void CKEPEditView::OnSelchangeEquippedItemMeshSlot() {
	int entityIndex;
	entityIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entityIndex == -1)
		return;

	POSITION entityPos;
	entityPos = m_movObjList->FindIndex(entityIndex);
	if (!entityPos)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPos));
	if (!pMovementObj)
		return;

	int equippedItemIndex;
	equippedItemIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_EQUIPABLE_ITEMS, LB_GETCURSEL, 0, 0);
	if (equippedItemIndex == -1 || !pMovementObj->m_runtimeSkeleton->m_armedInventoryDB)
		return;

	POSITION equippedItemPos;
	equippedItemPos = pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->FindIndex(equippedItemIndex);
	if (!equippedItemPos)
		return;

	CArmedInventoryObj *pRuntimeArmedInv;
	pRuntimeArmedInv = static_cast<CArmedInventoryObj *>(pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->GetAt(equippedItemPos));
	if (!pRuntimeArmedInv)
		return;

	EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
	if (!equippableRM)
		return;

	CArmedInventoryObj *pArmedInv = equippableRM->GetProxy(pRuntimeArmedInv->m_type, true)->GetData();
	if (!pArmedInv)
		return;

	SkeletonConfiguration *skeletonCfg = pArmedInv->m_thirdPersonSystem->m_skeletonConfiguration;
	if (skeletonCfg && skeletonCfg->m_alternateConfigurations && pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration) {
		int meshSlot;
		meshSlot = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_SLOT, CB_GETCURSEL, 0, 0);
		if (meshSlot == -1)
			return;

		POSITION configPos;
		configPos = skeletonCfg->m_alternateConfigurations->FindIndex(meshSlot);
		if (!configPos)
			return;

		CAlterUnitDBObject *pAlterUnit;
		pAlterUnit = reinterpret_cast<CAlterUnitDBObject *>(skeletonCfg->m_alternateConfigurations->GetAt(configPos));
		if (!pAlterUnit || !pAlterUnit->m_configurations)
			return;

		if ((pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.ItemCount() == 0) || !(pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)) || (meshSlot >= (int)pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)->slotCount))
			return;

		int meshIndex;
		int materialIndex;

		meshIndex = pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)->slotConfigs[meshSlot].meshIndex;
		materialIndex = pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)->slotConfigs[meshSlot].matIndex;
		InitializeEquippedItemMesh(pAlterUnit, meshIndex, materialIndex);
	} // if..pSkeleton
} // CKEPEditView::OnSelchangeEquippedItemMeshSlot

void CKEPEditView::OnSelchangeEquippedItemMesh() {
	m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MATERIAL, CB_RESETCONTENT, 0, 0);

	int entityIndex;
	entityIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entityIndex == -1)
		return;

	POSITION entityPos;
	entityPos = m_movObjList->FindIndex(entityIndex);
	if (!entityPos)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPos));
	if (!pMovementObj)
		return;

	int equippedItemIndex;
	equippedItemIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_EQUIPABLE_ITEMS, LB_GETCURSEL, 0, 0);
	if ((equippedItemIndex == -1) || !pMovementObj->m_runtimeSkeleton->m_armedInventoryDB)
		return;

	POSITION equippedItemPos;
	equippedItemPos = pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->FindIndex(equippedItemIndex);
	if (!equippedItemPos)
		return;

	CArmedInventoryObj *pRuntimeArmedInv;
	pRuntimeArmedInv = static_cast<CArmedInventoryObj *>(pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->GetAt(equippedItemPos));
	if (!pRuntimeArmedInv)
		return;

	EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
	if (!equippableRM)
		return;

	CArmedInventoryObj *pArmedInv = equippableRM->GetProxy(pRuntimeArmedInv->m_type, true)->GetData();
	if (!pArmedInv)
		return;

	SkeletonConfiguration *skeletonCfg = pArmedInv->m_thirdPersonSystem->m_skeletonConfiguration;
	if (skeletonCfg && skeletonCfg->m_alternateConfigurations && pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration) {
		int meshSlot;
		meshSlot = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_SLOT, CB_GETCURSEL, 0, 0);
		if (meshSlot == -1)
			return;

		POSITION configPos;
		configPos = skeletonCfg->m_alternateConfigurations->FindIndex(meshSlot);
		if (!configPos)
			return;

		CAlterUnitDBObject *pAlterUnit;
		pAlterUnit = reinterpret_cast<CAlterUnitDBObject *>(skeletonCfg->m_alternateConfigurations->GetAt(configPos));
		if (!pAlterUnit || !pAlterUnit->m_configurations)
			return;

		int meshConfigIndex;
		meshConfigIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, CB_GETCURSEL, 0, 0);
		if (meshConfigIndex == -1)
			return;

		POSITION meshConfigPos;
		meshConfigPos = pAlterUnit->m_configurations->FindIndex(meshConfigIndex);
		if (!meshConfigPos)
			return;

		CDeformableMesh *pDeformMesh;
		pDeformMesh = reinterpret_cast<CDeformableMesh *>(pAlterUnit->m_configurations->GetAt(meshConfigPos));
		if (!pDeformMesh || !pDeformMesh->m_supportedMaterials)
			return;

		if ((pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.ItemCount() == 0) || !(pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)) || (meshSlot >= (int)pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)->slotCount))
			return;

		int materialIndex;
		materialIndex = pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)->slotConfigs[meshSlot].matIndex;
		pDeformMesh->m_supportedMaterials;
		InitializeEquippedItemMaterial(pDeformMesh->m_supportedMaterials, materialIndex);
		SetEquippableAlternateConfiguration(pRuntimeArmedInv->m_thirdPersonSystem, meshSlot, meshConfigIndex, materialIndex, pRuntimeArmedInv->m_type);
	}
}

void CKEPEditView::OnSelchangeEquippedItemMaterial() {
	int entityIndex;
	entityIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entityIndex == -1)
		return;

	POSITION entityPos;
	entityPos = m_movObjList->FindIndex(entityIndex);
	if (!entityPos)
		return;

	CMovementObj *pMovementObj;
	pMovementObj = static_cast<CMovementObj *>(m_movObjList->GetAt(entityPos));
	if (!pMovementObj)
		return;

	int equippedItemIndex;
	equippedItemIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_EQUIPABLE_ITEMS, LB_GETCURSEL, 0, 0);
	if (equippedItemIndex == -1)
		return;

	POSITION equippedItemPos;
	equippedItemPos = pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->FindIndex(equippedItemIndex);
	if (!equippedItemPos)
		return;

	CArmedInventoryObj *pRuntimeArmedInv;
	pRuntimeArmedInv = static_cast<CArmedInventoryObj *>(pMovementObj->m_runtimeSkeleton->m_armedInventoryDB->GetAt(equippedItemPos));
	if (!pRuntimeArmedInv)
		return;

	EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
	if (!equippableRM)
		return;

	CArmedInventoryObj *pArmedInv = equippableRM->GetProxy(pRuntimeArmedInv->m_type, true)->GetData();
	if (!pArmedInv)
		return;

	SkeletonConfiguration *skeletonCfg = pArmedInv->m_thirdPersonSystem->m_skeletonConfiguration;
	if (skeletonCfg && skeletonCfg->m_alternateConfigurations && pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration) {
		int meshSlot;
		meshSlot = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_SLOT, CB_GETCURSEL, 0, 0);
		if (meshSlot == -1)
			return;

		int meshConfigIndex;
		meshConfigIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, CB_GETCURSEL, 0, 0);
		if (meshConfigIndex == -1)
			return;

		int materialIndex;
		materialIndex = m_entityDlgBar.SendDlgItemMessage(IDC_COMBO_EQUIPABLE_MATERIAL, CB_GETCURSEL, 0, 0);
		if (materialIndex == -1)
			return;

		if ((pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.ItemCount() == 0) || !(pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)) || (meshSlot >= (int)pRuntimeArmedInv->m_thirdPersonSystem->m_skeletonConfiguration->m_charConfig.at(BASE_CONFIG)->slotCount))
			return;

		SetEquippableAlternateConfiguration(pRuntimeArmedInv->m_thirdPersonSystem, meshSlot, meshConfigIndex, materialIndex, pRuntimeArmedInv->m_type);
	}
}

void CKEPEditView::OnBTNAIEditAITree() {
#ifndef _ClientOutput
	int treeIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_WEBTREES, LB_GETCURSEL, 0, 0);
	if (treeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAITREE);
		return;
	}

	POSITION posIndex = m_pAIWOL->FindIndex(treeIndex);
	if (posIndex) {
		CAIWebObj *webObj = (CAIWebObj *)m_pAIWOL->GetAt(posIndex);
		CAIPathDlg dlg;
		POSITION ePos = m_movObjList->FindIndex(0);
		if (ePos) {
			CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(ePos);
			ptrLoc->m_baseFrame->GetPosition(NULL, &dlg.m_charCurrentPosition);
		} else {
			dlg.m_charCurrentPosition.x = 0.0f;
			dlg.m_charCurrentPosition.y = 0.0f;
			dlg.m_charCurrentPosition.z = 0.0f;
		}

		dlg.m_aiObject = webObj;
		if (dlg.DoModal() == IDOK) {
			m_pAIWOL->SetDirty();
			EditorState::GetInstance()->SetZoneDirty();
		}
		webObj->m_name = dlg.m_webName;
		AITreeInList(treeIndex);
	}

#endif
}

void CKEPEditView::OnBTNSystemGoingDown() {
	ASSERT(false); // OLD
}

void CKEPEditView::OnBTNENTAddItem() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mainPtr = NULL;

	// get selected index
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mainPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mainPtr) {
			indexLoc++;
			continue;
		} // if..!mainPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mainPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	CItemObj *itemObj = new CItemObj();
	itemObj->m_attachmentRefList = new CAttachmentRefList();

	CAddItemDlg dlg;
	dlg.m_skillDatabaseRef = m_skillDatabase;
	dlg.m_statDatabaseRef = m_statDatabase;
	dlg.m_itemRef = itemObj;
	dlg.m_currentElements = m_elementDB;
	dlg.m_enitityDB = m_movObjRefModelList;
	dlg.m_skeletonReferencePtr = new CSkeletonObject(mainPtr->m_runtimeSkeleton, mainPtr->m_skeletonConfiguration);
	if (dlg.DoModal() == IDOK) {
		CGlobalInventoryObj *glPtr = new CGlobalInventoryObj();
		glPtr->m_itemData = itemObj;
		if (itemObj->m_attachmentRefList) {
			for (POSITION posLoc = itemObj->m_attachmentRefList->GetHeadPosition(); posLoc != NULL;) {
				CAttachmentRefObj *attachmentPtr = (CAttachmentRefObj *)itemObj->m_attachmentRefList->GetNext(posLoc);

				EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
				if (!equippableRM)
					return;

				CArmedInventoryObj *visObj = equippableRM->GetProxy(attachmentPtr->m_attachmentType, true)->GetData();
				if (visObj) {
					if (visObj->m_functionalityAttribute == 3) {
						glPtr->m_itemData->m_yJetforce = visObj->m_miscPosition.y;
						glPtr->m_itemData->m_zJetforce = visObj->m_miscPosition.z;
					}
				}
			}
		}
		glPtr->m_itemData->m_raceID = mainPtr->m_raceID;
		glPtr->m_itemData->m_dbSrcIndex = indexLoc;
		CGlobalInventoryObjList::GetInstance()->AddTail(glPtr);
	}

#endif
}

void CKEPEditView::OnBTNENTItemTest() {
#ifndef _ClientOutput
	int entIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
		return;
	}

	POSITION posloc = m_movObjList->FindIndex(entIndex);
	if (!posloc) {
		return;
	}

	CMovementObj *mainPtr = (CMovementObj *)m_movObjList->GetAt(posloc);

	posloc = m_movObjRefModelList->FindIndex(mainPtr->m_idxRefModel);
	if (!posloc) {
		return;
	}

	CMovementObj *DBPtr = (CMovementObj *)m_movObjRefModelList->GetAt(posloc);

	CEntityItemTestDlg dlg;
	dlg.m_entities = m_movObjRefModelList;
	dlg.m_actor = DBPtr;
	if (dlg.DoModal() == IDOK) {
		CItemObj *itm = CGlobalInventoryObjList::GetInstance()->GetByGLID(dlg.m_selGLID)->m_itemData;
		ArmInventoryItem(itm->m_globalID, mainPtr);
		if (mainPtr && mainPtr->m_runtimeSkeleton->m_armedInventoryDB) {
			for (POSITION appendagePos = mainPtr->m_runtimeSkeleton->m_armedInventoryDB->GetHeadPosition(); appendagePos != NULL;) {
				CArmedInventoryObj *appendPtr = (CArmedInventoryObj *)mainPtr->m_runtimeSkeleton->m_armedInventoryDB->GetNext(appendagePos);
				for (POSITION itmAppendPos = itm->m_attachmentRefList->GetHeadPosition(); itmAppendPos != NULL;) {
					CAttachmentRefObj *attachmentPtr = (CAttachmentRefObj *)itm->m_attachmentRefList->GetNext(itmAppendPos);

					EquippableRM *equippableRM = IClientEngine::Instance()->GetEquippableRM();
					if (!equippableRM)
						return;

					CArmedInventoryObj *armedObj = equippableRM->GetProxy(attachmentPtr->m_attachmentType, true)->GetData();
					if (appendPtr->m_type == armedObj->m_type) {
						if (appendPtr->m_firstPersonSystem && appendPtr->m_firstPersonSystem->m_skeletonConfiguration) {
							appendPtr->m_firstPersonSystem->m_skeletonConfiguration->RealizeDimConfig(appendPtr->m_firstPersonSystem->m_runtimeSkeleton, armedObj->m_firstPersonSystem->m_skeletonConfiguration->m_alternateConfigurations);
						}
						if (appendPtr->m_thirdPersonSystem && appendPtr->m_thirdPersonSystem->m_skeletonConfiguration) {
							appendPtr->m_thirdPersonSystem->m_skeletonConfiguration->RealizeDimConfig(appendPtr->m_thirdPersonSystem->m_runtimeSkeleton, armedObj->m_thirdPersonSystem->m_skeletonConfiguration->m_alternateConfigurations);
						}
					}
				}
			}
		}
	}

#endif
}

void CKEPEditView::OnBTNENTDeleteItem() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mainPtr = NULL;

	// get selected index
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mainPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mainPtr) {
			indexLoc++;
			continue;
		} // if..!mainPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mainPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	CEntityItemTestDlg dlg(IDS_DELETE);
	dlg.m_selGLID = 0;

	dlg.m_actor = mainPtr;
	dlg.m_entities = m_movObjRefModelList;
	if (dlg.DoModal() == IDOK) {
		CGlobalInventoryObj *invObj = CGlobalInventoryObjList::GetInstance()->GetByGLID(dlg.m_selGLID);
		if (invObj) {
			CGlobalInventoryObjList::GetInstance()->removeItemWithGLID(invObj->m_itemData->m_globalID);
		}
	} // if..dlg.DoModal() == IDOK
#endif
}

void CKEPEditView::OnBTNENTDisarmItem() {
#ifndef _ClientOutput
	int entIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (entIndex == -1) {
		AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
		return;
	}

	POSITION posloc = m_movObjList->FindIndex(entIndex);
	if (!posloc) {
		return;
	}

	CMovementObj *mainPtr = (CMovementObj *)m_movObjList->GetAt(posloc);

	if (!mainPtr->m_currentlyArmedItems) {
		AfxMessageBox(IDS_ERROR_ITEMLISTNOTFOUND);
		return;
	}

	CEntityItemTestDlg dlg(IDS_DISARM);

	dlg.m_selGLID = 0;
	dlg.m_currentlyArmedListRef = mainPtr->m_currentlyArmedItems;
	dlg.m_entities = NULL; // Only can disarm off my own actor
	dlg.m_actor = mainPtr;
	if (dlg.DoModal() == IDOK) {
		auto pCAI = mainPtr->m_currentlyArmedItems;
		if (pCAI) {
			auto pIO = pCAI->GetByGLID(dlg.m_selGLID);
			if (pIO)
				DisarmInventoryItem(pIO->m_globalID, mainPtr);
		}
	}

#endif
}

void CKEPEditView::OnBTNENTEditItem() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mainPtr = NULL;

	// get selected index
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mainPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mainPtr) {
			indexLoc++;
			continue;
		} // if..!mainPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mainPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	int selGLID = 0;
	while (1) {
		CEntityItemTestDlg dlg(IDS_EDIT, IDS_CLOSE);
		dlg.m_selGLID = selGLID;
		dlg.m_entities = m_movObjRefModelList; // Can only edit the items on this actor.
		dlg.m_actor = mainPtr;
		if (dlg.DoModal() == IDOK) {
			selGLID = dlg.m_selGLID;

			CItemObj *pItemObj;
			pItemObj = (CGlobalInventoryObjList::GetInstance()->GetByGLID(dlg.m_selGLID))->m_itemData;
			if (pItemObj) {
				if (!pItemObj->m_attachmentRefList) {
					pItemObj->m_attachmentRefList = new CAttachmentRefList();
				} // if..!pItemObj->m_attachmentRefList

				CAddItemDlg dlg;
				dlg.m_skillDatabaseRef = m_skillDatabase;
				dlg.m_statDatabaseRef = m_statDatabase;
				dlg.m_itemRef = pItemObj;
				dlg.m_enitityDB = m_movObjRefModelList;
				dlg.m_currentElements = m_elementDB;
				dlg.m_skeletonReferencePtr = new CSkeletonObject(mainPtr->m_runtimeSkeleton, mainPtr->m_skeletonConfiguration);
				if (dlg.DoModal() == IDOK) {
				} // if..dlg.DoModal() == IDOK
			} // if..pItemObj
		} else {
			break;
		}
	}

#endif
}

void CKEPEditView::OnBTNOBJClone() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;

		if (textLoc == pWO->m_fileNameRef) {
			CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
			static wchar_t BASED_CODE filter[] = L"Zone Objects DB Files (*.wle)|*.WLE|All Files (*.*)|*.*||";
			CFileDialog opendialog(FALSE, L"WLE", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
			opendialog.m_ofn.lpstrInitialDir = CurDir;
			if (opendialog.DoModal() == IDOK) {
				CStringW fileNameW = opendialog.GetFileName();
				CFileException exc;
				CFile fl;
				BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
				if (res == FALSE) {
					LOG_EXCEPTION(exc, m_logger)
					return;
				}

				BEGIN_SERIALIZE_TRY
				CArchive the_out_Archive(&fl, CArchive::store);
				the_out_Archive << pWO;
				the_out_Archive.Close();
				fl.Close();
				END_SERIALIZE_TRY(m_logger)
			}

			return;
		} // end Begin world object loop
	}

#endif
}

void CKEPEditView::OnBTNOBJTransformation() {
#ifndef _ClientOutput
	int baseObjectIndex = -1;
	CWldObject *pTemp = NULL;
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();
	int trace = 0;
	for (POSITION sPos = m_pWOL->GetHeadPosition(); sPos != NULL;) { // Begin world object loop
		pTemp = (CWldObject *)m_pWOL->GetNext(sPos);

		if (!pTemp)
			continue;

		if (textLoc == pTemp->m_fileNameRef) {
			baseObjectIndex = trace;
			break;
		}

		trace++;
	}

	if (baseObjectIndex == -1) {
		AfxMessageBox(IDS_MSG_SELZONEOBJECT);
		return;
	}

	CTransformationDlg dlg;

	dlg.m_rotX = pTemp->GetRotation().x;
	dlg.m_rotY = pTemp->GetRotation().y;
	dlg.m_rotZ = pTemp->GetRotation().z;

	dlg.m_transX = pTemp->GetTranslation().x - pTemp->GetPivot().x;
	dlg.m_transY = pTemp->GetTranslation().y - pTemp->GetPivot().y;
	dlg.m_transZ = pTemp->GetTranslation().z - pTemp->GetPivot().z;

	dlg.m_scaleX = pTemp->GetScale().x;
	dlg.m_scaleY = pTemp->GetScale().y;
	dlg.m_scaleZ = pTemp->GetScale().z;

	if (dlg.DoModal() == IDOK) {
		// get dialog data
		Vector3f rotation, scale, offset, pivot;

		rotation.x = dlg.m_rotX;
		rotation.y = dlg.m_rotY;
		rotation.z = dlg.m_rotZ;

		scale.x = dlg.m_scaleX;
		scale.y = dlg.m_scaleY;
		scale.z = dlg.m_scaleZ;

		offset.x = dlg.m_transX + pTemp->GetPivot().x;
		offset.y = dlg.m_transY + pTemp->GetPivot().y;
		offset.z = dlg.m_transZ + pTemp->GetPivot().z;

		// get pivot from dialog
		Vector3f *pivotPtr = 0;
		if (dlg.m_pivotSelection != CTransformationDlg::PIVOT_LOCAL) {
			pivotPtr = &pivot;
			if (dlg.m_pivotSelection == CTransformationDlg::PIVOT_ACTOR) {
				// TODO: grab active actor's position
				pivot.x = 0;
				pivot.y = 0;
				pivot.z = 0;
			} else // USER_DEFINED
			{
				pivot.x = dlg.m_pivotX;
				pivot.y = dlg.m_pivotY;
				pivot.z = dlg.m_pivotZ;
			}
		}

		// perform the transform
		if (dlg.m_applyToSelection == CTransformationDlg::APPLYTO_GROUP || dlg.m_applyToSelection == CTransformationDlg::APPLYTO_ALL) {
			for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
				CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
				if (dlg.m_applyToSelection == CTransformationDlg::APPLYTO_ALL ||
					pWO->m_groupNumber == pTemp->m_groupNumber) {
					pWO->Transform(scale, rotation, offset, pivotPtr);
					if (m_selectedLmp && (m_selectedLmp->GetLegacyWorldObject() == pWO))
						m_selectedLmp->Move(0, 0, 0); // just to force a bounding box update
					pWO->UpdateAllTriggerLocations();
					if (pWO->m_lightObject) {
						InvalidateLightCaches();
					}
				}
			}
		} else {
			pTemp->Transform(scale, rotation, offset, pivotPtr);
			if (m_selectedLmp && (m_selectedLmp->GetLegacyWorldObject() == pTemp))
				m_selectedLmp->Move(0, 0, 0); // just to force a bounding box update
			pTemp->UpdateAllTriggerLocations();
			if (pTemp->m_lightObject) {
				InvalidateLightCaches();
			}
		}

		AfxMessageBox(IDS_MSG_COMPLETE);
	} else
		AfxMessageBox(IDS_ERROR_NOMESHFOUND);
#endif
}

BNDBOX CKEPEditView::BoundingBoxGroupSet(int group, float maxBumpX, float maxBumpY, float maxBumpZ, float minBumpX, float minBumpY, float minBumpZ) {
	BNDBOX bndingBx;
	bndingBx.max.x = 0.0f;
	bndingBx.max.y = 0.0f;
	bndingBx.max.z = 0.0f;
	bndingBx.min.x = 0.0f;
	bndingBx.min.y = 0.0f;
	bndingBx.min.z = 0.0f;

	BOOL initialized = FALSE;
	for (POSITION sPos = m_pWOL->GetHeadPosition(); sPos != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(sPos);
		if (pWO->m_groupNumber == group) { // found group
			ABVERTEX *vArray = NULL;
			int vCount = 0;
			pWO->m_meshObject->m_abVertexArray.GetABVERTEXStyle(&vArray, &vCount);

			for (UINT vLoop = 0; vLoop < pWO->m_meshObject->m_abVertexArray.m_vertexCount; vLoop++) { // 1
				if (!initialized) { // set
					bndingBx.max.x = vArray[vLoop].x;
					bndingBx.max.y = vArray[vLoop].y;
					bndingBx.max.z = vArray[vLoop].z;
					bndingBx.min.x = vArray[vLoop].x;
					bndingBx.min.y = vArray[vLoop].y;
					bndingBx.min.z = vArray[vLoop].z;
				} // end set

				// check for new max
				if (bndingBx.max.x < vArray[vLoop].x) {
					bndingBx.max.x = vArray[vLoop].x;
				}

				if (bndingBx.max.y < vArray[vLoop].y) {
					bndingBx.max.y = vArray[vLoop].y;
				}

				if (bndingBx.max.z < vArray[vLoop].z) {
					bndingBx.max.z = vArray[vLoop].z;
				}

				// check for new min
				if (bndingBx.min.x > vArray[vLoop].x) {
					bndingBx.min.x = vArray[vLoop].x;
				}

				if (bndingBx.min.y > vArray[vLoop].y) {
					bndingBx.min.y = vArray[vLoop].y;
				}

				if (bndingBx.min.z > vArray[vLoop].z) {
					bndingBx.min.z = vArray[vLoop].z;
				}

				initialized = TRUE;
			} // end 1

			if (vArray) {
				delete[] vArray;
				vArray = 0;
			}
		} // end found group
	} // end world object loop

	bndingBx.max.x += maxBumpX;
	bndingBx.max.y += maxBumpY;
	bndingBx.max.z += maxBumpZ;
	bndingBx.min.x += minBumpX;
	bndingBx.min.y += minBumpY;
	bndingBx.min.z += minBumpZ;

	// set to group objects
	for (POSITION sPos = m_pWOL->GetHeadPosition(); sPos != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(sPos);
		if (pWO->m_groupNumber == group) {
			CopyMemory(&pWO->m_popoutBox, &bndingBx, sizeof(BNDBOX));
		}
	} // end world object loop

	return bndingBx;
}

void CKEPEditView::OnBTNOBJBoundingGroupSet() {
#ifndef _ClientOutput
	HWND treeHwnd = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA textLoc = Utf16ToUtf8(GetTreeItemText(IDC_OBJECT_TREE, &m_dlgBarObject, item)).c_str();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		if (!pWO)
			continue;

		if (textLoc == pWO->m_fileNameRef) {
			pWO->m_meshObject->m_abVertexArray.GenerateTangentsPerfect(g_pd3dDevice);

			return;
		}
	} // end Begin world object loop

	AfxMessageBox(IDS_ERROR_ZONEOBJECTNOTFOUND);

#endif
}

void CKEPEditView::OnBTNOBJLibraryAccess() {
#ifndef _ClientOutput
	int groupIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	if (groupIndex == -1) {
		groupIndex = 0;
	} else {
		POSITION posLoc = worldGroupList->FindIndex(groupIndex);
		groupIndex = 0;
		if (posLoc) {
			CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetAt(posLoc);
			groupIndex = wldPtr->m_groupInt;
		}
	}

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Zone Objects DB Files (*.wle)|*.WLE|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetPathName();

		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		CWldObject *wleNewPtr = NULL;
		the_in_Archive >> wleNewPtr;
		the_in_Archive.Close();
		fl.Close();
		wleNewPtr->m_groupNumber = groupIndex;
		wleNewPtr->m_identity = m_globalTraceNumber++;
		m_pWOL->AddTail(wleNewPtr);
		END_SERIALIZE_TRY(m_logger)

		EditorState::GetInstance()->SetZoneDirty();
	}

#endif
}

void CKEPEditView::OnBTNENVTimeSystem() {
#ifndef _ClientOutput
	CTimeSystemDlg dlg;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.m_enviormentObjRef = m_environment;
	dlg.m_textureDataBaseRef = m_pTextureDB;
	dlg.m_directSoundRef = m_pDirectSound;

	if (!m_environment->m_environmentTimeSystem) {
		m_environment->m_environmentTimeSystem = new CTimeSystemObj();
	}

	if (dlg.DoModal() == IDOK) {
		if (m_environment->m_environmentTimeSystem->m_currentState) {
			m_environment->m_environmentTimeSystem->m_currentState->SafeDelete();
			delete m_environment->m_environmentTimeSystem->m_currentState;
			m_environment->m_environmentTimeSystem->m_currentState = 0;
		}

		if (m_environment->m_environmentTimeSystem->m_stormPtr) {
			m_environment->m_environmentTimeSystem->m_stormPtr->SafeDelete();
			delete m_environment->m_environmentTimeSystem->m_stormPtr;
			m_environment->m_environmentTimeSystem->m_stormPtr = 0;
		}

		if (m_environment->m_environmentTimeSystem)
			m_environment->m_environmentTimeSystem->StartSystem(fTime::TimeMs(), 0);

		EditorState::GetInstance()->SetZoneDirty();
	}

#endif
}

void CKEPEditView::OnBTNMISAddSkeletalMissile() {
#ifndef _ClientOutput
	vector<string> fileNames;
	string folderName, filter;
	IAssetImporterFactory::Instance()->GetOpenDialogFilter(KEP::KEP_SKA_SKELETAL_OBJECT, filter);

	CSkeletonObject *newSkeletonObject = 0;
	if (!KEP::GetMultipleFileNamesToOpen(fileNames, folderName, filter, this, true) || (0 == (newSkeletonObject = IAssetsDatabase::Instance()->ImportSkeletalObject(fileNames[0]))))
		return;

	CMissileObj *newMissilePtr = new CMissileObj();
	newMissilePtr->m_typeOfProjectile = 2; // skeletal missile
	newMissilePtr->m_skeletalVisual = newSkeletonObject;

	CSkeletalAnimationDlg dlg;
	dlg.m_renderStateObjectRef = m_renderStateObject;
	dlg.m_globalShaderRef = m_cgGlobalShaderDB;
	dlg.m_elementDBREF = NULL;
	dlg.m_skillDatabaseREF = NULL;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_skeletonReferencePtr = newMissilePtr->m_skeletalVisual;
	dlg.directSoundRef = m_pDirectSound;
	dlg.g_pd3dDevice = g_pd3dDevice;
	dlg.DoModal();
	SkeletalTextureReassignment(newMissilePtr->m_skeletalVisual->m_skeletonConfiguration);

	// end
	CStringA conversion;
	newMissilePtr->m_collisionRadius = (float)atof(conversion.GetString());
	newMissilePtr->m_heatSeekingPower = (float)atof(conversion.GetString());
	newMissilePtr->m_forceExplodeAtEnd = FALSE;
	newMissilePtr->m_filterCollision = FALSE;

	newMissilePtr->m_physicDynamics = new CMovementCaps();

	MissileDB->AddTail(newMissilePtr);
	m_missileDlgBar.SetSettingsChanged(TRUE);
	MissileDatabaseInList(MissileDB->GetCount());
#endif
}

void CKEPEditView::OnBTNMISAddLaser() {
#ifndef _ClientOutput
	CMissileObj *newMissilePtr = new CMissileObj();
	newMissilePtr->m_typeOfProjectile = 3; // laser segment

	newMissilePtr->m_laserObject = new CLaserObj();
	newMissilePtr->m_laserObject->InitializeMesh(g_pd3dDevice);

	CLaserDlg dlg;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_laserObjectRef = newMissilePtr->m_laserObject;
	dlg.DoModal();

	// end
	CStringA conversion;
	newMissilePtr->m_collisionRadius = (float)atof(conversion.GetString());

	newMissilePtr->m_heatSeekingPower = (float)atof(conversion.GetString());
	newMissilePtr->m_forceExplodeAtEnd = TRUE;
	newMissilePtr->m_forceExplodeAtEnd = FALSE;
	newMissilePtr->m_filterCollision = TRUE;
	newMissilePtr->m_filterCollision = FALSE;

	newMissilePtr->m_physicDynamics = new CMovementCaps();

	MissileDB->AddTail(newMissilePtr);
	m_missileDlgBar.SetSettingsChanged(TRUE);
	MissileDatabaseInList(MissileDB->GetCount() - 1);
#endif
}

void CKEPEditView::OnBTNTCPSupportedWorlds() {
	if (m_currentWldsSpawnPointModule) {
		delete m_currentWldsSpawnPointModule;
	}

	m_currentWldsSpawnPointModule = 0;

	m_currentWldsSpawnPointModule = new CCurrentWorldsDlg();
	m_currentWldsSpawnPointModule->GL_multiplayerObject = m_multiplayerObj;
	m_currentWldsSpawnPointModule->Create(IDD_DIALOG_CURRENTWORLDS, this);
	m_currentWldsSpawnPointModule->ShowWindow(SW_SHOW);
}

void CKEPEditView::OnBTNENTEditDeathCfg() {
#ifndef _ClientOutput
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;

	// get selected index
	POSITION posLoc;
	for (posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	// get database object
	posLoc = m_movObjRefModelList->FindIndex(indexLoc);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjRefModelList->GetAt(posLoc);
	CEditStartInventoryDlg dlg;
	dlg.m_globalInventoryRef = m_globalInventoryDB;
	dlg.m_autoCfgRef = ptrLoc->m_deathCfg;
	dlg.DoModal();
	ptrLoc->m_deathCfg = dlg.m_autoCfgRef;
#endif
}

void CKEPEditView::OnBTNTCPViewAccount() {
	int runtimeIndex = m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCURRENTACCT);
		return;
	}

	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);

	POSITION posLoc = m_multiplayerObj->m_accountDatabase->FindIndex(runtimeIndex);
	if (!posLoc) {
		LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
		return;
	}

	CAccountObject *ptrLoc = (CAccountObject *)m_multiplayerObj->m_accountDatabase->GetAt(posLoc);

	CStringW pass;
	m_multiPlayer.GetDlgItemText(IDC_EDIT_TCP_ADMINCODE, pass);

	if (pass != L"rain") {
		LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
		return;
	}

	if (m_accountViewerModule) {
		delete m_accountViewerModule;
	}

	m_accountViewerModule = 0;

	m_accountViewerModule = new CAccountViewerModule();
	m_accountViewerModule->m_multiplayerRef = m_multiplayerObj;
	m_accountViewerModule->accountPtr = ptrLoc;
	m_accountViewerModule->m_globalDBRef = m_globalInventoryDB;
	m_accountViewerModule->m_entityDatabase = m_movObjRefModelList;
	m_accountViewerModule->m_skillDatabaseREF = m_skillDatabase;

	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);

	m_accountViewerModule->Create(IDD_DIALOG_ACCOUNTVIEWERDLG, this);

	m_accountViewerModule->ShowWindow(SW_SHOW);
}

void CKEPEditView::OnCHECKTXBBitmapLoadOverride() {
	if (m_txbBitmapLoadOverride) {
		m_txbBitmapLoadOverride = FALSE;
	} else {
		m_txbBitmapLoadOverride = TRUE;
	}
}

void CKEPEditView::OnBTNUISkillBuilder() {
#ifndef _ClientOutput
	CSkillSystemDlg dlg(m_skillDatabase);
	//dlg.m_skillDatabaseRef = m_skillDatabase;
	dlg.m_edbDatabaseRef = m_movObjRefModelList;
	dlg.m_statDatabaseRef = m_statDatabase;
	dlg.m_globalInventoryDBREF = m_globalInventoryDB;

	if (dlg.DoModal() == IDOK) {
		//m_skillDatabase = dlg.m_skillDatabaseRef;
		InListMissileSkillCriteria(0);
	}

#endif
}

void CKEPEditView::OnBTNENTUpdateVisuals() {
#ifndef _ClientOutput
	CAddEntityDlg dlg;
	dlg.saveCollisionInfo = TRUE;
	if (dlg.DoModal() != IDOK) {
		return;
	}

	HWND treeHwnd = NULL;
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	HTREEITEM item = TreeView_GetSelection(treeHwnd);
	CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
	POSITION delPos;
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); (delPos = posLoc) != NULL;) {
		CMovementObj *mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr)
			continue;

		if (temp == mPtr->m_name) {
			if (LoadActorFromDir(*mPtr, dlg.m_folderPath, dlg.m_fileNames, TRUE)) {
				//init the stats, if we dont these characters are invulnerable
				if (!mPtr->m_stats)
					m_statDatabase->Clone(&mPtr->m_stats);

				InitFXShadersOnSkeletal(mPtr->m_runtimeSkeleton, mPtr->m_skeletonConfiguration);
				//				ReinitAllTexturesFromDatabase();
				m_pTextureDB->TextureMapReinitAll();

				m_entityDlgBar.SetSettingsChanged(TRUE);
			} else {
				CStringW s;
				s.LoadString(IDS_ERROR_SKELETALLOADFAILED);
				s + Utf8ToUtf16(dlg.m_folderPath).c_str() + L"[";
				for (UINT i = 0; i < dlg.m_fileNames.size(); i++)
					s += Utf8ToUtf16(dlg.m_fileNames[i]).c_str();
				AfxMessageBox(s);
			}
		}
	}

#endif
}

void CKEPEditView::OnBTNOBJAddNode() {
#ifndef _ClientOutput
	// test
	m_nodeImporterModule.m_referenceDBREF = m_libraryReferenceDB;
	if (m_nodeImporterModule.DoModal() != IDOK)
		return;

	POSITION entPosition = m_movObjList->FindIndex(0);
	if (!entPosition) {
		AfxMessageBox(IDS_MSG_NORUNTIMESPAWNED);
		return;
	}

	CMovementObj *entityPtr = dynamic_cast<CMovementObj *>(m_movObjList->GetAt(entPosition));
	int listBoxIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	int selectedGroupID = -1;

	if (listBoxIndex >= 0) {
		CWldGroupObj *pWldGroupObj;
		LRESULT returnValue;
		returnValue = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETITEMDATA, listBoxIndex, 0);
		if ((returnValue != LB_ERR) && returnValue) {
			pWldGroupObj = reinterpret_cast<CWldGroupObj *>(returnValue);
			selectedGroupID = pWldGroupObj->m_groupInt;
		} // if..returnValue != LB_ERR
	} // if..listBoxIndex >= 0

	for (int loop = 0; loop < m_nodeImporterModule.m_selectionCount; loop++) {
		int selectionIndex;
		selectionIndex = m_libraryReferenceDB->GetReferenceIndexByName(Utf16ToUtf8(m_nodeImporterModule.m_selectionArray[loop]).c_str());
		AddNodeByData(selectedGroupID, selectionIndex, entityPtr, NULL);
	}

	// Update tree view
	ObjectTreeViewInlist(selectedGroupID, -1);
#endif
}

void CKEPEditView::OnBTNOBJReferenceLibrary() {
#ifndef _LimitedEngine
#ifndef _ClientOutput
	CReferenceLibrary dlg;
	dlg.m_worldObjListRef = m_pWOL;
	dlg.m_sceneScriptExec = SceneScriptExec;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_referenceDBREF = m_libraryReferenceDB;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.m_renderStateObjectRef = m_renderStateObject;
	dlg.m_textureSearchPathsRef = m_textureSearchPath;
	dlg.m_texturePathCountRef = m_searchPathCount;
	dlg.m_editor = this;
	dlg.DoModal();
	m_searchPathCount = dlg.m_texturePathCountRef;
	m_libraryReferenceDB = dlg.m_referenceDBREF;

	if (AfxMessageBox(IDS_MSG_REINIT_TEXTURES, MB_OKCANCEL, 0) == IDOK) {
		HCURSOR prev = SetCursor(::LoadCursor(NULL, IDC_WAIT));
		//		ReinitAllTexturesFromDatabase();
		m_pTextureDB->TextureMapReinitAll();
		SetCursor(prev);
	}
#endif
#endif
}

void CKEPEditView::OnBTNUISoundDB() {
#ifndef _ClientOutput
	CSoundDatabaseDlg dlg(m_soundManager);
	//dlg.m_soundManagerRef = m_soundManager;
	dlg.directSoundRef = m_pDirectSound;
	dlg.DoModal();
	//m_soundManager = dlg.m_soundManagerRef;
#endif
}

void CKEPEditView::OnBTNUIGlInventoryDB() {
#ifndef _ClientOutput
	m_globalInventoryDB;
	m_movObjRefModelList;

	CGlobalInventoryDlg dlg(m_globalInventoryDB);
	dlg.m_entityDBRef = m_movObjRefModelList;

	if (dlg.DoModal() == IDOK) {
		// TODO: check if this leaves a memory leak
		//m_globalInventoryDB = dlg.m_globalInventoryDBREF;
	}

#endif
}

void CKEPEditView::OnBtnUiCurrencysys() {
#ifndef _ClientOutput
	CCurrencyDlg dlg;
	dlg.m_currencyName = m_currencyObject->m_currencyName;
	if (dlg.DoModal() == IDOK) {
		m_currencyObject->m_currencyName = dlg.m_currencyName;
	}

#endif
}

void CKEPEditView::OnBTNUICommerceDB() {
#ifndef _ClientOutput
	CCommerceDlg dlg(m_commerceDB);
	//dlg.m_commerceObjDBRef = m_commerceDB;
	dlg.DoModal();
	//m_commerceDB = dlg.m_commerceObjDBRef;
#endif
}

void CKEPEditView::OnBTNUIBankZones() {
#ifndef _ClientOutput
	CBankZonesDlg dlg(m_bankZoneList);
	//dlg.m_bankDBRef = m_bankZoneList;
	dlg.DoModal();
	//m_bankZoneList = dlg.m_bankDBRef;
#endif
}

void CKEPEditView::OnBtnUiSafezones() {
#ifndef _ClientOutput
	CSafeZoneDlg dlg(m_safeZoneDB);
	//dlg.m_safeZoneDBREF = m_safeZoneDB;
	dlg.DoModal();
	//m_safeZoneDB = dlg.m_safeZoneDBREF;
#endif
}

void CKEPEditView::OnBTNENTViewRuntimeStats() {
	int runtimeIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELACTOR);
		return;
	}

	POSITION posLoc = m_movObjList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);

	if (m_entityViewerModule) {
		delete m_entityViewerModule;
	}

	m_entityViewerModule = 0;

	m_entityViewerModule = new CRuntimeEntityViewerDlg();
	m_entityViewerModule->m_runtimeEntityRef = ptrLoc;
	m_entityViewerModule->Create(IDD_DIALOG_RUNTIMEENTITYINFO, this);
	m_entityViewerModule->ShowWindow(SW_SHOW);
}

void CKEPEditView::OnBTNENTViewRuntimeResources() {
#ifndef _ClientOutput
	int runtimeIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELACTORRUNTIME);
		return;
	}

	POSITION posLoc = m_movObjList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
	CSkeletalAnimationDlg dlg;
	dlg.m_renderStateObjectRef = m_renderStateObject;
	dlg.m_globalShaderRef = m_cgGlobalShaderDB;
	dlg.m_elementDBREF = NULL;
	dlg.m_skillDatabaseREF = NULL;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_skeletonReferencePtr = new CSkeletonObject(ptrLoc->m_runtimeSkeleton, ptrLoc->m_skeletonConfiguration);
	dlg.directSoundRef = m_pDirectSound;
	dlg.g_pd3dDevice = g_pd3dDevice;
	dlg.DoModal();
#endif
}

void CKEPEditView::OnBTNENTUnifySleletalMesh() {
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr;

	// get selected index
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}
}

void CKEPEditView::OnBTNMISDeleteLight() {
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	if (missileIndex == -1) {
		AfxMessageBox(IDS_MSG_SELPROJECTILE);
		return;
	}

	POSITION posLoc = MissileDB->FindIndex(missileIndex);
	if (!posLoc) {
		return;
	}

	CMissileObj *misPtr = (CMissileObj *)MissileDB->GetAt(posLoc);
	if (misPtr->m_light) {
		misPtr->m_light->SafeDelete();
		delete misPtr->m_light;
		misPtr->m_light = 0;
		MissileDB->SetDirty();

		AfxMessageBox(IDS_MSG_COMPLETE);
	} else {
		AfxMessageBox(IDS_ERROR_NOLIGHTEXISTS);
	}
}

void CKEPEditView::OnBTNMISLightSys() {
#ifndef _ClientOutput
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	if (missileIndex == -1) {
		AfxMessageBox(IDS_MSG_SELPROJECTILE);
		return;
	}

	POSITION posLoc = MissileDB->FindIndex(missileIndex);
	if (!posLoc) {
		return;
	}

	CMissileObj *misPtr = (CMissileObj *)MissileDB->GetAt(posLoc);
	CLightInterfaceDlg dlg;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.m_lightREF = misPtr->m_light;
	if (dlg.DoModal() == IDOK) {
		misPtr->m_light = dlg.m_lightREF;
		MissileDB->SetDirty();
	}

#endif
}

void CKEPEditView::OnBTNTCPItemGeneratorDB() {
#ifndef _ClientOutput
	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);

	CSpawnGenDlg dlg(m_serverItemGenerator);
	dlg.m_aiListRef = m_AIOL;
	dlg.m_skillDatabaseRef = m_skillDatabase;

	if (!m_multiplayerObj->m_currentWorldsUsed) {
		AfxMessageBox(IDS_ERROR_NOWORLDLISTFOUND);
		LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
		return;
	}

	if (m_multiplayerObj->m_runtimeItemsOnServer) {
		m_multiplayerObj->m_runtimeItemsOnServer->SafeDelete();
		delete m_multiplayerObj->m_runtimeItemsOnServer;
		m_multiplayerObj->m_runtimeItemsOnServer = 0;
	}

	if (m_serverItemGenerator) {
		for (POSITION posLoc = m_serverItemGenerator->GetHeadPosition(); posLoc != NULL;) {
			CServerItemGenObj *spnGenPtr = (CServerItemGenObj *)m_serverItemGenerator->GetNext(posLoc);
			spnGenPtr->m_existsOnServer = FALSE;
		}
	}

	//dlg.m_serverItemGenRef = m_serverItemGenerator;
	dlg.m_globalInventoryList = m_globalInventoryDB;
	dlg.m_worldsDB = m_multiplayerObj->m_currentWorldsUsed;
	dlg.m_edbRef = m_movObjRefModelList;
	dlg.m_focusEntity = NULL;
	dlg.m_focusEntityExists = FALSE;

	POSITION posLoc = m_movObjList->FindIndex(0);
	if (posLoc) {
		CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
		dlg.m_focusEntity = ptrLoc;
		dlg.m_focusEntityExists = TRUE;
	}

	dlg.DoModal();
	//m_serverItemGenerator = dlg.m_serverItemGenRef;

	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
#endif
}

void CKEPEditView::OnBtnPatchsys() {
	AfxMessageBox(IDS_MSG_SYSTEMREMOVED);
}

void CKEPEditView::OnBTNTCPUpdateCurrentOnlinePlayers() {
}

void CKEPEditView::OnBTNENTScaleRuntimeEntity() {
	CMovementObj *ptrLoc = 0;

	int runtimeIndex = m_entityDlgBar.SendDlgItemMessage(IDC_LIST_ENTITY_RUNTIMELIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex != -1) {
		POSITION posLoc = m_movObjList->FindIndex(runtimeIndex);
		if (posLoc)
			ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
	}

	if (!ptrLoc) {
		AfxMessageBox(IDS_MSG_SELACTOR);
		return;
	}

	CStringW conversionX, conversionY, conversionZ;
	m_entityDlgBar.GetDlgItemText(IDC_EDIT_ENT_SCALE_RUNTIME_PERCENTAGE_X, conversionX);
	m_entityDlgBar.GetDlgItemText(IDC_EDIT_ENT_SCALE_RUNTIME_PERCENTAGE_Y, conversionY);
	m_entityDlgBar.GetDlgItemText(IDC_EDIT_ENT_SCALE_RUNTIME_PERCENTAGE_Z, conversionZ);

	float absoluteScaleX = (float)_wtof(conversionX.GetString());
	float absoluteScaleY = (float)_wtof(conversionY.GetString());
	float absoluteScaleZ = (float)_wtof(conversionZ.GetString());

	//get the max scale factor around 3 axes
	float maxScale = absoluteScaleX > absoluteScaleY ? absoluteScaleX : absoluteScaleY;
	maxScale = maxScale > absoluteScaleZ ? maxScale : absoluteScaleZ;

	//radius could have been previously scaled, so calculate relative scale factor
	ptrLoc->m_boundingRadius = ptrLoc->m_unscaledBoundingRadius * maxScale;
	ptrLoc->m_runtimeSkeleton->ScaleSystem(
		absoluteScaleX,
		absoluteScaleY,
		absoluteScaleZ);

	//AfxMessageBox ( IDS_MSG_COMPLETE );
}

void CKEPEditView::OnBTNENTScaleEntity() {
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr = NULL; // DRF - potentially uninitialized pointer

	// get selected index
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); posLoc != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			continue;
		} // if..!mPtr

		HWND treeHwnd = NULL;
		m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

		HTREEITEM item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	CStringW conversionX, conversionY, conversionZ;
	m_entityDlgBar.GetDlgItemText(IDC_EDIT_ENT_SCALEPERCENTAGE_X, conversionX);
	m_entityDlgBar.GetDlgItemText(IDC_EDIT_ENT_SCALEPERCENTAGE_Y, conversionY);
	m_entityDlgBar.GetDlgItemText(IDC_EDIT_ENT_SCALEPERCENTAGE_Z, conversionZ);

	const float relativeScaleX = (float)_wtof(conversionX.GetString());
	const float relativeScaleY = (float)_wtof(conversionY.GetString());
	const float relativeScaleZ = (float)_wtof(conversionZ.GetString());

	//get the max scale factor around 3 axes
	float maxScale = relativeScaleX > relativeScaleY ? relativeScaleX : relativeScaleY;
	maxScale = maxScale > relativeScaleZ ? maxScale : relativeScaleZ;

	if (mPtr == NULL) {
		assert(false); // DRF - potentially uninitialized variable
	}
	mPtr->m_boundingRadius = mPtr->m_unscaledBoundingRadius * maxScale;

	mPtr->m_runtimeSkeleton->ScaleSystem(relativeScaleX, relativeScaleY, relativeScaleZ);

	AfxMessageBox(IDS_MSG_COMPLETE);
}

void CKEPEditView::OnCHECKSPNEnableSystem() {
	// AI SERVER ONLY
}

void CKEPEditView::OnBTNTXBSetMipLevels() {
}

void CKEPEditView::OnBTNENTReplaceEntityWSIE() {
	int indexLoc = 0;
	BOOL foundIt = FALSE;
	CMovementObj *mPtr = nullptr;

	HTREEITEM item = NULL;
	HWND treeHwnd = NULL;

	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	// get selected index
	POSITION posLast = NULL;
	int tracer = 0;
	for (POSITION posLoc = m_movObjRefModelList->GetHeadPosition(); (posLast = posLoc) != NULL;) { // visual loop
		mPtr = (CMovementObj *)m_movObjRefModelList->GetNext(posLoc);
		if (!mPtr) {
			indexLoc++;
			tracer++;
			continue;
		} // if..!mPtr

		item = TreeView_GetSelection(treeHwnd);
		CStringA temp = Utf16ToUtf8(GetTreeItemText(IDC_TREE_ENTITIES, &m_entityDlgBar, item)).c_str();
		if (temp == mPtr->m_name) {
			foundIt = TRUE;
			break;
		}

		indexLoc++;
		tracer++;
	}

	if (foundIt == FALSE) {
		AfxMessageBox(IDS_MSG_SELACTORBASEOBJECT);
		return;
	}

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Actor Unit Files (*.sie)|*.SIE|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();

		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		CMovementObj *newPtrLoc = NULL;
		the_in_Archive >> newPtrLoc;
		the_in_Archive.Close();
		fl.Close();

		// set the trace of the new item to be the same as the old one
		newPtrLoc->m_traceNumber = mPtr->m_traceNumber;

		// add the new item after the original
		POSITION hubLoc = m_movObjRefModelList->FindIndex(tracer);
		if (hubLoc) {
			m_movObjRefModelList->InsertAfter(hubLoc, newPtrLoc);

			// select the old item so it can be removed
			TreeView_SelectItem(treeHwnd, item);
			OnBtnDeleteEntity();
		}

		if (m_editorModeEnabled == TRUE) {
			EnitityInList();
		}
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNTCPIPBlocking() {
}

void CKEPEditView::OnBtnUiWaterzones() {
#ifndef _ClientOutput
	CWaterZoneDBDLG dlg(m_waterZoneDB);

	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	//dlg.m_waterZnDBREF = m_waterZoneDB;
	dlg.m_waterZnDBREF_memloc = &m_waterZoneDB;

	if (dlg.DoModal() == IDOK) {
	}
#endif
}

void CKEPEditView::OnBtnUiPortalzonesdb() {
#ifndef _ClientOutput
	CPortalsDlg dlg(m_portalDatabase);
	//dlg.m_portalsDBRef = m_portalDatabase;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	if (dlg.DoModal() == IDOK) {
		//m_portalDatabase = dlg.m_portalsDBRef;
	}

#endif
}

void CKEPEditView::OnBTNUIElementDB() {
#ifndef _ClientOutput
	CElementsDlg dlg(m_elementDB);
	//dlg.m_elementDBREF = m_elementDB;
	if (dlg.DoModal() == IDOK) {
		//m_elementDB = dlg.m_elementDBREF;
	}

#endif
}

void CKEPEditView::OnBTNTCPUpgradeWorm() {
#ifndef _ClientOutput
	CUpgradeWormDlg dlg;
	dlg.m_disarmAll = FALSE;
	dlg.m_deleteAllUnknownItems = FALSE;
	dlg.m_useDeathCfg = FALSE;
	dlg.m_rebuildGroupDatabases = FALSE;
	dlg.m_clearAll = FALSE;
	if (dlg.DoModal() == IDOK) {
		ASSERT(FALSE);
	}

#endif
}

void CKEPEditView::OnBTNEXPSaveUnit() {
#ifndef _LimitedEngine
	int runtimeIndex = m_explosionDlgBar.SendDlgItemMessage(IDC_LIST_EXPLOSIONLIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELEXPLOSION);
		return;
	}

	POSITION posLoc = m_explosionDBList->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CExplosionObj *expObj = (CExplosionObj *)m_explosionDBList->GetAt(posLoc);

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Explosion Unit Files (*.exu)|*.EXU|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"EXU", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << expObj;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}

#endif
}

void CKEPEditView::OnBTNEXPLoadUnit() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Explosion Unit Files (*.exu)|*.EXU|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CExplosionObj *expObj = NULL;
		CArchive the_in_Archive(&fl, CArchive::load);
		the_in_Archive >> expObj;
		the_in_Archive.Close();
		fl.Close();
		m_explosionDBList->AddTail(expObj);
		END_SERIALIZE_TRY(m_logger)
	}

	InListExplosionDB(m_explosionDBList->GetCount() - 1);
}

void CKEPEditView::OnBTNTCPDisconnectAll() {
	// old server UI
	ASSERT(FALSE);
}

DWORD WINAPI SqlUpdateToServer(LPVOID lpParameter) {
	return 1;
}

void CKEPEditView::QuestDataSQLOutput() {
}

void CKEPEditView::SpawnGenDataSQLOutput() {
}

DWORD CKEPEditView::SqlItemUpdateToServer() {
	return 1;
}

void CKEPEditView::OnBTNTCPPrintReports() {
	SpawnGenDataSQLOutput();
	QuestDataSQLOutput();
	SqlItemUpdateToServer();

	AfxMessageBox(IDS_MSG_COMPLETE);
}

void CKEPEditView::OnBTNAILoadAiUnit() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"AI Bot Data Files (*.aie)|*.AIE|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetPathName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}
		CAIObj *partPtr = NULL;
		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		the_in_Archive >> partPtr;
		the_in_Archive.Close();
		fl.Close();
		m_AIOL->AddTail(partPtr);
		if (m_editorModeEnabled == TRUE) { // if editor is available
			AI_DBInList(m_AIOL->GetCount() - 1);
			ScriptInList(m_pAISOL->GetCount() - 1);
		}
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNAISaveAiUnit() {
	if (!m_AIOL) {
		return;
	}

	if (m_editorModeEnabled == FALSE) {
		return;
	}

	int runtimeIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_DB, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELAIBOT);
		return;
	}

	POSITION posLoc = m_AIOL->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CAIObj *partPtr = (CAIObj *)m_AIOL->GetAt(posLoc);

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"AI Bot Data Files (*.aie)|*.AIE|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"AIE", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << partPtr;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNAIImportCollisionSys() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Collision Unit Files (*.cui)|*.CUI|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		CCollisionObj *cObj = NULL;
		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		the_in_Archive >> cObj;
		the_in_Archive.Close();
		fl.Close();
		m_collisionDatabase->AddTail(cObj);
		END_SERIALIZE_TRY(m_logger)
	}

	InListCollisionDB(m_collisionDatabase->GetCount() - 1);
}

void CKEPEditView::OnBTNAIDeleteSys() {
	int runtimeIndex = m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_COLLISIONDATABASES, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCOLLISION);
		return;
	}

	POSITION posLoc = m_collisionDatabase->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CCollisionObj *cObj = (CCollisionObj *)m_collisionDatabase->GetAt(posLoc);

	cObj->SafeDelete();
	delete cObj;
	cObj = 0;
	m_collisionDatabase->RemoveAt(posLoc);

	InListCollisionDB(runtimeIndex - 1);
}

void CKEPEditView::OnBTNAIExportCurrentCol() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Collision Unit Files (*.cui)|*.CUI|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"CUI", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << GL_collisionBase;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNAISaveColDB() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Collision AI Files (*.crd)|*.CRD|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"CRD", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << m_collisionDatabase;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNAILoadColDB() {
#if (DRF_AI_ENGINE == 1)
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Collision AI Files (*.crd)|*.CRD|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		LoadAICollisionDB(fileName.GetString());
	}
#endif
}

void CKEPEditView::OnBTNMISSaveUnit() {
#ifndef _LimitedEngine
	int missileIndex = m_missileDlgBar.SendDlgItemMessage(IDC_LIST_MIS_MISSILEDATABASE, LB_GETCURSEL, 0, 0);
	if (missileIndex == -1) {
		AfxMessageBox(IDS_MSG_SELPROJECTILE);
		return;
	}

	POSITION posLoc = MissileDB->FindIndex(missileIndex);
	if (!posLoc) {
		return;
	}

	CMissileObj *misPtr = (CMissileObj *)MissileDB->GetAt(posLoc);

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Projectile Unit Files (*.mie)|*.MIE|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"MUI", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << misPtr;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
#endif
}

void CKEPEditView::OnBTNMISLoadUnit() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Projectile Unit Files (*.mie)|*.MIE|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();

		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		CMissileObj *misPtr = NULL;
		the_in_Archive >> misPtr;
		the_in_Archive.Close();
		fl.Close();

		// Prep then add baby!!
		MissileDB->AddTail(misPtr);
		if (m_editorModeEnabled == TRUE) {
			MissileDatabaseInList(MissileDB->GetCount() - 1);
		}
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNSPNExportSys() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Spawn Generator DB Files (*.sdb)|*.SDB|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"SDB", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << spawnGeneratorDB << m_pAIWOL;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNSPNImportSys() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Spawn Generator DB Files (*.sdb)|*.SDB|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();

		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		if (spawnGeneratorDB) {
			spawnGeneratorDB->SafeDelete();
			delete spawnGeneratorDB;
			spawnGeneratorDB = 0;
		}

		if (m_pAIWOL) {
			m_pAIWOL->SafeDelete();
			delete m_pAIWOL;
			m_pAIWOL = 0;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		the_in_Archive >> spawnGeneratorDB >> m_pAIWOL;
		the_in_Archive.Close();
		fl.Close();

		// Prep then add baby!!
		if (m_editorModeEnabled == TRUE) {
			SpawnGenDatabaseInList(0);
			AITreeInList(-1);
		}
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNTCPSearchForAccount() {
	if (!m_multiplayerObj->m_accountDatabase) {
		AfxMessageBox(IDS_ERROR_NOACCOUNTDB);
		return;
	}

	CStringW dataW;
	m_multiPlayer.GetDlgItemText(IDC_EDIT_TCP_SEARCHACCOUNTSTRING, dataW);
	CStringA data = Utf16ToUtf8(dataW).c_str();

	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);

	int trace = 0;
	for (POSITION posLoc = m_multiplayerObj->m_accountDatabase->GetHeadPosition(); posLoc != NULL;) {
		CAccountObject *ptrLoc = (CAccountObject *)m_multiplayerObj->m_accountDatabase->GetNext(posLoc);
		if (ptrLoc->m_accountName == data) {
			break;
		}

		trace++;
	}

	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);

	m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_SETCURSEL, trace, 0);
}

void CKEPEditView::OnBTNTCPRestoreBackup() {
	CStringW pass;
	m_multiPlayer.GetDlgItemText(IDC_EDIT_TCP_ADMINCODE, pass);
	if (pass != L"rain") {
		return;
	}

	if (AfxMessageBox(IDS_CONFIRM_RESTOREACCOUNTS, MB_OKCANCEL, 0) != IDOK) {
		return;
	}

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Backup Files (*.bkk)|*.BKK|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		m_multiplayerObj->RestoreAccountsBackup(fileName, m_skillDatabase, m_globalInventoryDB);

		if (m_editorModeEnabled == TRUE) {
			InListAccounts(m_multiplayerObj->m_accountDatabase->GetCount() - 1);
		}
	}
}

void CKEPEditView::OnBTNTXTSymbolDB() {
#ifndef _ClientOutput
	CSymbolMapDlg dlg;
	dlg.m_symbolMapDBREF = m_symbolMapDB;
	dlg.DoModal();
	m_symbolMapDB = dlg.m_symbolMapDBREF;
#endif
}

void CKEPEditView::OnCHECKEditorTxtMode() {
	if (m_editorTextModeOn) {
		m_editorTextModeOn = FALSE;
	} else {
		m_editorTextModeOn = TRUE;
	}
}

class NameFormatter : public CGlobalInventoryObjList::Traverser {
public:
	virtual bool evaluate(CGlobalInventoryObj &obj) {
		CItemObj *itemPtr = obj.m_itemData;
		int length = itemPtr->m_itemName.GetLength();
		CStringA newName;
		BOOL firstChar = TRUE;
		for (int loop = 0; loop < length; loop++) {
			CStringA temp(itemPtr->m_itemName.GetAt(loop));
			if (firstChar)
				temp.MakeUpper();
			else
				temp.MakeLower();
			firstChar = FALSE;

			if (temp == " ")
				firstChar = TRUE;
			if (temp == "-")
				firstChar = TRUE;

			if (temp == ")")
				firstChar = TRUE;
			newName = operator+(newName, temp);
		}
		itemPtr->m_itemName = newName;
		return false;
	}
};

void CKEPEditView::FormatAllNames() {
	CGlobalInventoryObjList::GetInstance()->traverse(NameFormatter());
	AfxMessageBox(IDS_MSG_COMPLETE);
}

void CKEPEditView::OnBTNENTMassFormatNames() {
	if (AfxMessageBox(IDS_CONFIRM_MASSFORMAT, MB_OKCANCEL, 0) != IDOK) {
		return;
	}

	FormatAllNames();
}

void CKEPEditView::OnCHKOBJEnablePlaementHotKey() {
	if (m_placementHotKeyEnabled) {
		m_placementHotKeyEnabled = FALSE;
	} else {
		m_placementHotKeyEnabled = TRUE;
	}
}

void CKEPEditView::OnBTNOBJReportCurRenderCache() {
	OnBTNUIMerge();
	//SnapShotRenderSection ();
}

void CKEPEditView::OnBTNUIIconDBACCESS() {
#ifndef _ClientOutput
	CIconSysDlg dlg(m_iconDatabase);
	dlg.m_texturedatabaseREF = m_pTextureDB;
	//dlg.m_iconDatabaseREF = m_iconDatabase;
	dlg.DoModal();
	m_iconDatabase = dlg.m_iconDatabaseREF;
#endif
}

void CKEPEditView::OnBTNTCPSnapShotPacketQue() {
	m_snapshotPacketQue = TRUE;
}

BOOL CKEPEditView::CreateBackGroundObject(CStringA bitmapName, int xMax, int yMax, int startX, int startY, RECT usageRect, BOOL movingMap) { // BEGIN

	return TRUE;
} // END

void CKEPEditView::BackGroundScrollRight(int pixelIndex, int listIndex) { // BEGIN
}

void CKEPEditView::BackGroundScrollLeft(int pixelIndex, int listIndex) { // BEGIN
}

void CKEPEditView::BackGroundScrollDown(int pixelIndex, int listIndex) { // BEGIN
}

void CKEPEditView::BackGroundScrollUp(int pixelIndex, int listIndex) { // BEGIN
}

void CKEPEditView::OnButtonDelete() {
	ASSERT(FALSE);
	//DeleteBackground ();
}

void CKEPEditView::OnButtonMvUp() {
	BackGroundScrollUp(5, 0);
}

void CKEPEditView::OnButtonMvDown() {
	BackGroundScrollDown(5, 0);
}

void CKEPEditView::OnButtonMvLeft() {
	BackGroundScrollLeft(5, 0);
}

void CKEPEditView::OnButtonMvRight() {
	BackGroundScrollRight(5, 0);
}

void CKEPEditView::OnBTN2DSaveConfig() {
}

void CKEPEditView::OnBTN2DLoadConfig() {
}

void CKEPEditView::OnBTNTCPIPJoinHost() {
	m_multiplayerObj->EnableClient();
}

void CKEPEditView::OnBTNTCPDisconnectPlayer() {
}

void CKEPEditView::OnChangeEDITTCPIPadress() {
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the RMWin::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	// TODO: Add your control notification handler code here
	CStringA cs(m_multiplayerObj->getIPAddress().c_str());
	GetDlgItemTextUtf8(&m_multiPlayer, IDC_EDIT_TCP_IPADRESS, cs);
	m_multiplayerObj->setIPAddress((const char *)cs);
}

void CKEPEditView::OnChangeTCPuserName() {
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the RMWin::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	// TODO: Add your control notification handler code here
	GetDlgItemTextUtf8(&m_multiPlayer, IDC_TCP_USERNAME, m_multiplayerObj->m_userName);
}

void CKEPEditView::OnChangeTCPpassword() {
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the RMWin::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	// TODO: Add your control notification handler code here
	GetDlgItemTextUtf8(&m_multiPlayer, IDC_TCP_PASSWORD, m_multiplayerObj->m_passWord);
}

void CKEPEditView::OnBtnUiStatdb() {
#ifndef _ClientOutput
	CStatisticsDBDlg dlg(m_statDatabase);
	//dlg.statisticDBREF = m_statDatabase;
	dlg.DoModal();
	//m_statDatabase = dlg.statisticDBREF;
#endif
}

void CKEPEditView::OnBTNTCPUpdateAllAcounts() {
	//CheckAccountDBSyncronization ();
	// NOOP
	AfxMessageBox(IDS_MSG_COMPLETE);
}

void CKEPEditView::OnBTNTCPGenerateLogBasedOnTime() {
	if (!m_multiplayerObj->m_accountDatabase) {
		return;
	}

	int years = m_multiPlayer.GetDlgItemInt(IDC_EDIT_TCP_GENLOGYEARS, NULL, TRUE);
	int months = m_multiPlayer.GetDlgItemInt(IDC_EDIT_TCP_GENLOGMONTHS, NULL, TRUE);
	int days = m_multiPlayer.GetDlgItemInt(IDC_EDIT_TCP_GENLOGDAYS, NULL, TRUE);
	int hours = m_multiPlayer.GetDlgItemInt(IDC_EDIT_TCP_GENLOGHOURS, NULL, TRUE);
	int minutes = m_multiPlayer.GetDlgItemInt(IDC_EDIT_TCP_GENLOGMINUTES, NULL, TRUE);

	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);
	m_multiplayerObj->m_accountDatabase->GenerateLogonRecordBasedOnTime(minutes, hours, days, months, years);
	OutputFormat();
	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
}

void CKEPEditView::OutputFormat() {
}

void CKEPEditView::OnBTNUIHousingSys() {
#ifndef _ClientOutput
	CHousingSysDlg dlg(m_housingDB);
	dlg.GL_textureDataBase = m_pTextureDB;
	//dlg.m_houseDBREF = m_housingDB;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.DoModal();
	m_housingDB = dlg.m_houseDBREF;
	if (dlg.GetSpawnHouseOnClose()) {
		POSITION entPosition = m_movObjList->FindIndex(0);
		if (!entPosition) {
			AfxMessageBox(IDS_MSG_NORUNTIMESPAWNED);
			return;
		}

		CMovementObj *entityPtr = (CMovementObj *)m_movObjList->GetAt(entPosition);
		Vector3f position;
		entityPtr->m_baseFrame->GetPosition(NULL, &position);
		m_housingCache->PlaceHouse(position, dlg.m_houseSelection, 22);
	}

#endif
}

void CKEPEditView::OnBTNUIHszoneDB() {
#ifndef _ClientOutput
	CHousePlacementZoneDlg dlg(m_multiplayerObj->m_housingDB);
	dlg.g_pd3dDeviceRef = g_pd3dDevice;

	if (!m_multiplayerObj->m_housingDB) {
		m_multiplayerObj->m_housingDB = new CHousingZoneDB();
	}

	//dlg.m_housingZonesDB = GL_multiplayerObject->m_housingDB;
	if (dlg.DoModal() == IDOK) {
		//GL_multiplayerObject->m_housingDB = dlg.m_housingZonesDB;
	}

#endif
}

void CKEPEditView::OnBTNChkshowSpeed() {
}

void CKEPEditView::OnBTNAIIntegrityCheck() {
#ifndef _ClientOutput
	// check web boundries
	if (m_pAIWOL) {
		for (POSITION posLoc = m_pAIWOL->GetHeadPosition(); posLoc != NULL;) {
			CAIWebObj *web = (CAIWebObj *)m_pAIWOL->GetNext(posLoc);
			for (POSITION wPos = web->m_web->GetHeadPosition(); wPos != NULL;) {
				CAIWebNode *nodePtr = (CAIWebNode *)web->m_web->GetNext(wPos);

				if (fabs(nodePtr->m_position.x) > 29000 || fabs(nodePtr->m_position.y) > 29000 || fabs(nodePtr->m_position.z) > 29000) {
					CStringW s;
					s.LoadString(IDS_ERROR_OUTOFRANGE);
					AfxMessageBox(s + Utf8ToUtf16(web->m_name).c_str());
				}
			}
		}
	}

	AfxMessageBox(IDS_MSG_BEGINDIMCFGCHECK);
	AIdimIntegrityCheck();
	AfxMessageBox(IDS_MSG_COMPLETEBOUNDSTREECHECK);
	AfxMessageBox(IDS_MSG_BEGINITEMINTEGRITYCHECK);

	// check inventory validity
	int lastSelectedPersist = 0;

	if (m_AIOL) {
		m_AIOL->RefreshData(m_globalInventoryDB);
	}

	if (m_AIOL) {
		for (POSITION posLoc = m_AIOL->GetHeadPosition(); posLoc != NULL;) {
			CAIObj *partPtr = (CAIObj *)m_AIOL->GetNext(posLoc);

			if (!partPtr->m_inventoryCfg) {
				continue;
			}

			POSITION rPosLast;
			for (POSITION rPos = partPtr->m_inventoryCfg->GetHeadPosition(); (rPosLast = rPos) != NULL;) { // 1
				CInventoryObj *invPtr = (CInventoryObj *)partPtr->m_inventoryCfg->GetNext(rPos);
				CGlobalInventoryObj *glidPtr = m_globalInventoryDB->GetByGLID(invPtr->m_itemData->m_globalID);

				BOOL callRepairMan = FALSE;
				if (!glidPtr) {
					callRepairMan = TRUE;
				} else if (glidPtr->m_itemData->m_itemName != invPtr->m_itemData->m_itemName) {
					callRepairMan = TRUE;
				}

				if (callRepairMan) { // r
					CItemConflictDlg cnDlg;
					cnDlg.m_lastSelected = lastSelectedPersist;
					cnDlg.m_currentInventory = partPtr->m_inventoryCfg;
					cnDlg.m_currentInvItem = invPtr;
					cnDlg.m_currentItemGlid = invPtr->m_itemData->m_globalID;
					cnDlg.m_globalInventoryList = m_globalInventoryDB;
					cnDlg.m_currentName = invPtr->m_itemData->m_itemName;
					cnDlg.m_reference = partPtr->m_nameOvveride;

					/*int result = */ cnDlg.DoModal();
					lastSelectedPersist = cnDlg.m_lastSelected;
					if (cnDlg.m_returnCommand == 5) {
						return;
					}

					if (cnDlg.m_returnCommand == 0) { // delete current
						invPtr->SafeDelete();
						delete invPtr;
						invPtr = 0;
						partPtr->m_inventoryCfg->RemoveAt(rPosLast);
					} // end delete current

					if (cnDlg.m_returnCommand == 10) {
						// SetAllItemData(invPtr->m_itemData,glidOld);
					}
				} // end r
			} // end 1
		}
	}

	Sleep(4000);
	AfxMessageBox(IDS_MSG_BEGINARMEDITEMCONFLICTCHECK);
	if (m_AIOL) {
		for (POSITION posLoc = m_AIOL->GetHeadPosition(); posLoc != NULL;) {
			CAIObj *partPtr = (CAIObj *)m_AIOL->GetNext(posLoc);

			if (!partPtr->m_inventoryCfg) {
				continue;
			}

			POSITION rPosLast;
			for (POSITION rPos = partPtr->m_inventoryCfg->GetHeadPosition(); (rPosLast = rPos) != NULL;) { // 1
				CInventoryObj *invPtr = (CInventoryObj *)partPtr->m_inventoryCfg->GetNext(rPos);
				if (!invPtr->isArmed()) {
					continue;
				}

				for (POSITION rPos2 = partPtr->m_inventoryCfg->GetHeadPosition(); rPos2 != NULL;) { // 2
					CInventoryObj *invPtr2 = (CInventoryObj *)partPtr->m_inventoryCfg->GetNext(rPos2);
					if (!invPtr2->isArmed()) {
						continue;
					}

					if (invPtr == invPtr2) {
						continue;
					}

					bool conflictFound = false;
					for (vector<int>::const_iterator invLocID2 = invPtr2->m_itemData->m_exclusionGroupIDs.begin(); invLocID2 != invPtr2->m_itemData->m_exclusionGroupIDs.end(); invLocID2++) {
						for (vector<int>::const_iterator invLocID1 = invPtr->m_itemData->m_exclusionGroupIDs.begin(); invLocID1 != invPtr->m_itemData->m_exclusionGroupIDs.end(); invLocID1++) {
							if (*invLocID2 == *invLocID1) { // conflict exists
								CStringA build = "armed location ID conflict:";
								build += invPtr2->m_itemData->m_itemName;
								build += "  ";
								build += invPtr->m_itemData->m_itemName;
								AfxMessageBoxUtf8(build);
								conflictFound = true;
								break;
							} // end conflict exists
						}
						if (conflictFound)
							break;
					}
				} // end 2
			} // end 1
		}
	}

	Sleep(4000);
	AfxMessageBox(IDS_MSG_BEGINWEAPONARMEDCHECK);

	int tracerItTemp = 0;
	if (m_AIOL) {
		for (POSITION posLoc = m_AIOL->GetHeadPosition(); posLoc != NULL;) {
			CAIObj *partPtr = (CAIObj *)m_AIOL->GetNext(posLoc);
			tracerItTemp++;
			if (!partPtr->m_inventoryCfg) {
				continue;
			}

			BOOL isArmed = FALSE;
			POSITION rPosLast;
			for (POSITION rPos = partPtr->m_inventoryCfg->GetHeadPosition(); (rPosLast = rPos) != NULL;) { // 1
				CInventoryObj *invPtr = (CInventoryObj *)partPtr->m_inventoryCfg->GetNext(rPos);
				if (!invPtr->isArmed()) {
					continue;
				}

				for (vector<int>::const_iterator exID = invPtr->m_itemData->m_exclusionGroupIDs.begin(); exID != invPtr->m_itemData->m_exclusionGroupIDs.end(); exID++) {
					if (*exID == 12) {
						isArmed = TRUE;
						break;
					}
				}
			} // end 1

			if (!isArmed) {
				CStringW s;
				s.LoadString(IDS_ERROR_AINOARMEDITEMONID12);
				wstringstream strBuilder;
				strBuilder << s << (tracerItTemp - 1);
				AfxMessageBox(strBuilder.str().c_str());
			}
		}
	}

	Sleep(4000);
	AfxMessageBox(IDS_MSG_COMPLETE);
#endif
}

void CKEPEditView::OnBTNAInewTree() {
	CAIWebObj *webObj = new CAIWebObj();
	webObj->m_web = new CAIWebNodeList();
	m_pAIWOL->AddTail(webObj);
	m_aiDBDlgBar.SetSettingsChanged(TRUE);
	AITreeInList(m_pAIWOL->GetCount() - 1);
}

void CKEPEditView::OnBTNTCPExportAccount() {
	CStringW pass;
	m_multiPlayer.GetDlgItemText(IDC_EDIT_TCP_ADMINCODE, pass);
	if (pass != L"rain") {
		return;
	}

	int runtimeIndex = m_multiPlayer.SendDlgItemMessage(IDC_LIST_TCP_IP_CURRENTPLAYERS, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELCURRENTACCT);
		return;
	}

	POSITION posLoc = m_multiplayerObj->m_accountDatabase->FindIndex(runtimeIndex);
	if (!posLoc) {
		return;
	}

	CAccountObject *ptrLoc = (CAccountObject *)m_multiplayerObj->m_accountDatabase->GetAt(posLoc);

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Accounts Files (*.acc)|*.ACC|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"ACC", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			MessageBox(fileNameW);
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		m_multiplayerObj->m_accountDatabase->FlagAllForOptimizedSave();
		ptrLoc->SetOptimizedSave(TRUE);

		the_out_Archive << ptrLoc;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNTCPImportAccount() {
	CStringW pass;
	m_multiPlayer.GetDlgItemText(IDC_EDIT_TCP_ADMINCODE, pass);
	if (pass != L"rain") {
		return;
	}

	CAccountObject *ptrLoc = NULL;

	if (!m_multiplayerObj->m_accountDatabase) {
		AfxMessageBox(IDS_ERROR_NOACCOUNTDATABASE);
		return;
	}

	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Accounts Files (*.acc)|*.ACC|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		the_in_Archive >> ptrLoc;
		the_in_Archive.Close();
		fl.Close();

		if (!m_multiplayerObj->m_accountDatabase)
			m_multiplayerObj->m_accountDatabase = new CAccountObjectList();

		ptrLoc->RestoreAccountFromOptimized(m_skillDatabase, m_globalInventoryDB);

		// resyncronize
		ptrLoc->m_accountNumber = m_multiplayerObj->m_accountDatabase->GetUntakenAccountNumber();

		if (ptrLoc->m_charactersOnAccount) {
			for (POSITION lPos = ptrLoc->m_charactersOnAccount->GetHeadPosition(); lPos != NULL;) {
				CPlayerObject *newPobj = (CPlayerObject *)ptrLoc->m_charactersOnAccount->GetNext(lPos);
				newPobj->m_handle = m_multiplayerObj->m_accountDatabase->GetNewLocalPlayerHandleID();
			}
		}

		m_multiplayerObj->m_accountDatabase->AddTail(ptrLoc);

		if (ptrLoc->m_charactersOnAccount) { // call a secopnd time
			for (POSITION lPos = ptrLoc->m_charactersOnAccount->GetHeadPosition(); lPos != NULL;) {
				CPlayerObject *newPobj = (CPlayerObject *)ptrLoc->m_charactersOnAccount->GetNext(lPos);
				newPobj->m_handle = m_multiplayerObj->m_accountDatabase->GetNewLocalPlayerHandleID();
			}
		}

		if (m_editorModeEnabled == TRUE) {
			InListAccounts(m_multiplayerObj->m_accountDatabase->GetCount() - 1);
		}
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnBTNTCPGenerateClanDatabase() {
}

void CKEPEditView::OnBTNTCPClearInactive() {
	if (AfxMessageBox(IDS_CONFIRM_AREYOUSURE, MB_OKCANCEL, 0) != IDOK) {
		return;
	}

	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);

	ASSERT(FALSE);
	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
	AfxMessageBox(IDS_MSG_COMPLETE);
}

void CKEPEditView::OnBTNUIAIRainSys() {
#ifndef _ClientOutput
	Vector3f optionalInitializer;
	optionalInitializer.x = 0.0f;
	optionalInitializer.y = 0.0f;
	optionalInitializer.z = 0.0f;

	POSITION entPosition = m_movObjList->FindIndex(0);
	if (entPosition) { // valid entity
		CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(entPosition);
		ptrLoc->m_baseFrame->GetPosition(NULL, &optionalInitializer);
	}

	CAIRaidDbClass dlg(m_aiRaidDatabase);
	dlg.SetPositionX(optionalInitializer.x);
	dlg.SetPositionY(optionalInitializer.y);
	dlg.SetPositionZ(optionalInitializer.z);
	//dlg.m_aiRaidRefList = m_aiRaidDatabase;
	dlg.DoModal();
	//m_aiRaidDatabase = dlg.m_aiRaidRefList;
#endif
}

class CSVWriter : public CGlobalInventoryObjList::Traverser {
public:
	CSVWriter(CFile &vfl) :
			fl(vfl), build() {}

	virtual bool evaluate(CGlobalInventoryObj &obj) {
		CGlobalInventoryObj *glPtr = &obj;

		CStringA tempName(glPtr->m_itemData->m_itemName);
		tempName.Replace(",", " ");

		CStringW locationIDs;
		for (vector<int>::const_iterator locID = glPtr->m_itemData->m_exclusionGroupIDs.begin(); locID != glPtr->m_itemData->m_exclusionGroupIDs.end(); locID++) {
			CStringW locationID;
			locationID.Format(L"%d ", *locID);
			locationIDs += locationID;
		}

		build.Format("%d,%s,%d,%d,%d,%d,%d,%d,%d,%d,%s,%d,%d,%d,%d,%d,%d",
			glPtr->m_itemData->m_globalID,
			tempName,
			glPtr->m_itemData->m_marketCost,
			glPtr->m_itemData->m_sellingPrice,
			glPtr->m_itemData->m_animAccessValue,
			glPtr->m_itemData->m_chargeCurrent,
			glPtr->m_itemData->m_chargeType,
			glPtr->m_itemData->m_destroyOnUse,
			glPtr->m_itemData->m_disarmable,
			glPtr->m_itemData->m_keyID,
			Utf16ToUtf8(locationIDs).c_str(),
			glPtr->m_itemData->m_maxCharges,
			glPtr->m_itemData->m_nonDrop,
			glPtr->m_itemData->m_nonTransferable,
			glPtr->m_itemData->m_permanent,
			glPtr->m_itemData->m_stackable,
			glPtr->m_itemData->m_itemType);

		fl.Write(build, build.GetLength());
		fl.Write("\r\n", 2);
		return false;
	}
	CFile &fl;
	CStringA build;
};

void CKEPEditView::OnBTNENTsaveItemDB() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Inventory List Files (*.gil)|*.GIL|CSV File (*.csv)|*.CSV|All Files (*.*)|*.*||";
	CFileDialog opendialog(FALSE, L"GIL", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION_NO_LOGGER(exc)
		}
		CStringW ext = opendialog.GetFileExt();
		if (ext.CompareNoCase(L"csv") == 0) {
			UpdateData(TRUE);
			CStringW build = L"GLID,NAME,COST,SELLING PRICE,ANIMVER,CHARGESCUR,CHARGETYPE,DESTROYONUSE,DISARMABLE,KEYID,LOCATION,MAXCHARGES,NODROP,NOTRANSFER,PERMANENT,STACKABLE,INV_TYPE";
			fl.Write(build, build.GetLength());
			fl.Write("\r\n", 2);

			CGlobalInventoryObjList::GetInstance()->traverse(CSVWriter(fl));
			fl.Close();
		} else {
			BEGIN_SERIALIZE_TRY
			CArchive the_out_Archive(&fl, CArchive::store);
			GILSerializer serializer(*(CGlobalInventoryObjList::GetInstance()));
			the_out_Archive << (&serializer);
			the_out_Archive.Close();
			fl.Close();
			END_SERIALIZE_TRY_NO_LOGGER
		}
	}
}

void CKEPEditView::OnBTNENTLoadItemDB() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Inventory List Files (*.gil)|*.GIL|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetPathName();
		CStringA fileName = Utf16ToUtf8(fileNameW).c_str();
		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION_NO_LOGGER(exc)
			return;
		}

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		CGlobalInventoryObjList::GetInstance()->SafeDelete();
		CGlobalInventoryObjList *temp = CGlobalInventoryObjList::GetInstance();
		GILSerializer *serializer;
		the_in_Archive >> serializer;
		the_in_Archive.Close();
		temp = serializer->retrieveList();
		delete serializer;
		fl.Close();
		END_SERIALIZE_TRY_NO_LOGGER
	}
}

void CKEPEditView::OnBTNSPNAppendSys() {
	CStringW CurDir = GetSubDirectoryOfCurrentW(L"GameFiles").c_str();
	static wchar_t BASED_CODE filter[] = L"Spawn Generator DB Files (*.sdb)|*.SDB|All Files (*.*)|*.*||";
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, filter, this);
	opendialog.m_ofn.lpstrInitialDir = CurDir;
	if (opendialog.DoModal() == IDOK) {
		CStringW fileNameW = opendialog.GetFileName();

		CFileException exc;
		CFile fl;
		BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			return;
		}

		if (!spawnGeneratorDB) {
			spawnGeneratorDB = new CSpawnObjList();
		}

		if (!m_pAIWOL) {
			m_pAIWOL = new CAIWebObjList();
		}

		CAIWebObjList *aiWebHolder = NULL;
		CSpawnObjList *spawnGenHolder = NULL;

		BEGIN_SERIALIZE_TRY
		CArchive the_in_Archive(&fl, CArchive::load);
		the_in_Archive >> spawnGenHolder >> aiWebHolder;
		the_in_Archive.Close();
		fl.Close();

		int offset = m_pAIWOL->GetCount();

		if (aiWebHolder) {
			for (POSITION posBuild = aiWebHolder->GetHeadPosition(); posBuild != NULL;) {
				CAIWebNode *webNode = (CAIWebNode *)aiWebHolder->GetNext(posBuild);
				m_pAIWOL->AddTail(webNode);
			}
		}

		if (spawnGenHolder) {
			for (POSITION posBuild = spawnGenHolder->GetHeadPosition(); posBuild != NULL;) {
				CSpawnObj *spawnNode = (CSpawnObj *)spawnGenHolder->GetNext(posBuild);
				spawnNode->m_treeUse += offset;
				spawnGeneratorDB->AddTail(spawnNode);
			}
		}

		// Prep then add baby!!
		if (m_editorModeEnabled == TRUE) {
			SpawnGenDatabaseInList(0);
			AITreeInList(-1);
		}
		END_SERIALIZE_TRY(m_logger)
	}
}

void CKEPEditView::OnButtonViewsmoothshaded() {
#if EXPLOSION
	int runtimeIndex = m_explosionDlgBar.SendDlgItemMessage(IDC_LIST_EXPLOSIONLIST, LB_GETCURSEL, 0, 0);
	if (runtimeIndex == -1) {
		AfxMessageBox(IDS_MSG_SELEXPLOSION);
		return;
	}
	DoExpEffect(runtimeIndex, 0);
#endif
}

void CKEPEditView::OnBtnViewwireframe() {
}

void CKEPEditView::OnBTNCommCntrlLibBtnA() {
#ifndef _ClientOutput
	CCommCrtlEditorDlg dlg;
	dlg.GL_textureDataBase = m_pTextureDB;
	dlg.g_pd3dDeviceRef = g_pd3dDevice;
	dlg.g_directSoundRef = m_pDirectSound;
	dlg.m_controlDatabase = m_commCtrlDB;
	dlg.DoModal();
	m_commCtrlDB = dlg.m_controlDatabase;
#endif
}

void CKEPEditView::OnBTNWorldRule() {
#ifndef _ClientOutput
	CWorldChannelDlg dlg(m_worldChannelRuleDB);

	dlg.DoModal();

#endif
}

void CKEPEditView::OnBTNArenaManager() {
#ifndef _ClientOutput
	CArenaManagerDlg dlg(m_battleSchedulerSys);

	dlg.DoModal();

#endif
}

void CKEPEditView::OnBTNTestAssign() {
	int *test = new int[5];
	ZeroMemory(test, sizeof(int) * 5);

	if (test[7] > 0) {
	}

	AfxMessageBox(L"Test1");
}

void CKEPEditView::OnBTNUImouseSW() {
#ifndef _ClientOutput
	CShaderLibDlg dlg(m_shaderSystem);
	//dlg.m_shaderLibRef = m_shaderSystem;
	dlg.m_pd3dDevice = g_pd3dDevice;
	dlg.m_shaderTypeMode = 0;
	dlg.DoModal();
	//m_shaderSystem = dlg.m_shaderLibRef;
	m_shaderSystem->Compile(g_pd3dDevice);
	m_renderStateObject->m_shderSystemRef = m_shaderSystem;

#endif
}

#include "wkgmesh/selectwidget.h"
void CKEPEditView::OnObjectSelect() {
	// get the selection blade and tell it to accept input
	IClientBlade *b = ClientBladeFactory::Instance()->GetBladeOfExactlyTypeByName(typeid(SelectWidget).name());
	if (b) {
		SelectWidget *sw = (SelectWidget *)b;
		if (sw->IsInputEnabled() /*|| sw->GetSelected()*/) {
			sw->EnableInput(false);
			// disable the move, rotate, and scale buttons

			//sw->SetSelected(0);

			// turn off all the buttons
			m_dlgBarObject.EnableSelectButton(false);
			m_dlgBarObject.EnableMoveButton(false);
			m_dlgBarObject.EnableRotateButton(false);
			m_dlgBarObject.EnableScaleButton(false);

			sw->OnKey(3);
		} else {
			sw->EnableInput(true);
			// enable the move, rotate, and scale buttons
			// turn off all the buttons

			//sw->SetSelected(0);

			// turn off all buttons except the select button
			m_dlgBarObject.EnableSelectButton(true);
			m_dlgBarObject.EnableMoveButton(false);
			m_dlgBarObject.EnableRotateButton(false);
			m_dlgBarObject.EnableScaleButton(false);

			// enable moving by default
			OnObjectMove();
		}
	}
}

void CKEPEditView::OnObjectMove() {
	IClientBlade *b = ClientBladeFactory::Instance()->GetBladeOfExactlyTypeByName(typeid(SelectWidget).name());
	if (b) {
		SelectWidget *sw = (SelectWidget *)b;
		//if(sw->GetSelected())
		if (sw->IsInputEnabled()) {
			sw->OnKey(0);

			m_dlgBarObject.EnableSelectButton(true);
			m_dlgBarObject.EnableMoveButton(true);
			m_dlgBarObject.EnableRotateButton(false);
			m_dlgBarObject.EnableScaleButton(false);
		}
		/*
		else
		{
			// user must select an object first
			m_dlgBarObject.EnableSelectButton(true);
			m_dlgBarObject.EnableMoveButton(false);
			m_dlgBarObject.EnableRotateButton(false);
			m_dlgBarObject.EnableScaleButton(false);
		}
		*/
	}
}

void CKEPEditView::OnObjectRotate() {
	// get the selection blade and tell it to accept input
	IClientBlade *b = ClientBladeFactory::Instance()->GetBladeOfExactlyTypeByName(typeid(SelectWidget).name());
	if (b) {
		SelectWidget *sw = (SelectWidget *)b;
		//if(sw->GetSelected())
		if (sw->IsInputEnabled()) {
			sw->OnKey(1);

			m_dlgBarObject.EnableSelectButton(true);
			m_dlgBarObject.EnableMoveButton(false);
			m_dlgBarObject.EnableRotateButton(true);
			m_dlgBarObject.EnableScaleButton(false);
		}
		/*
		else
		{
			// user must select an object first
			m_dlgBarObject.EnableSelectButton(true);
			m_dlgBarObject.EnableMoveButton(false);
			m_dlgBarObject.EnableRotateButton(false);
			m_dlgBarObject.EnableScaleButton(false);
		}
		*/
	}
}

void CKEPEditView::OnObjectScale() {
	// get the selection blade and tell it to accept input
	IClientBlade *b = ClientBladeFactory::Instance()->GetBladeOfExactlyTypeByName(typeid(SelectWidget).name());
	if (b) {
		SelectWidget *sw = (SelectWidget *)b;
		//if(sw->GetSelected())
		if (sw->IsInputEnabled()) {
			sw->OnKey(2);

			m_dlgBarObject.EnableSelectButton(true);
			m_dlgBarObject.EnableMoveButton(false);
			m_dlgBarObject.EnableRotateButton(false);
			m_dlgBarObject.EnableScaleButton(true);
		}
		/*
		else
		{
			// user must select an object first
			m_dlgBarObject.EnableSelectButton(true);
			m_dlgBarObject.EnableMoveButton(false);
			m_dlgBarObject.EnableRotateButton(false);
			m_dlgBarObject.EnableScaleButton(false);
		}
		*/
	}
}

void CKEPEditView::OnChangeDialog() {
	// get the selection blade and tell it to accept input
	IClientBlade *b = ClientBladeFactory::Instance()->GetBladeOfExactlyTypeByName(typeid(SelectWidget).name());
	if (b) {
		SelectWidget *sw = (SelectWidget *)b;
		sw->OnKey(3);

		m_dlgBarObject.EnableSelectButton(false);
		m_dlgBarObject.EnableMoveButton(false);
		m_dlgBarObject.EnableRotateButton(false);
		m_dlgBarObject.EnableScaleButton(false);
	}
}

void CKEPEditView::OnSelchangeViewports() {
	int viewport_index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (LB_ERR != viewport_index) {
		// change property control
		POSITION pos = m_pVL->FindIndex(viewport_index);
		if (pos) {
			CViewportObj *viewport = (CViewportObj *)m_pVL->GetAt(pos);
			if (viewport) {
				m_camVptDlgBar.SetViewportName(viewport->GetName());
				m_camVptDlgBar.SetPercentTop(viewport->m_Y1);
				m_camVptDlgBar.SetPercentLeft(viewport->m_X1);
				m_camVptDlgBar.SetPercentBottom(viewport->m_Y2);
				m_camVptDlgBar.SetPercentRight(viewport->m_X2);

				m_camVptDlgBar.UpdatePropertyControls();
			}
		}
	}
	// no viewport was selected
	else {
		m_camVptDlgBar.ResetViewportPropertyControl();
		m_camVptDlgBar.UpdatePropertyControls();
	}
}

void CKEPEditView::OnSelchangeRuntimeViewports() {
	int runtime_viewport_index = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_GETCURSEL, 0, 0);
	if (LB_ERR != runtime_viewport_index) {
		// change property control
		int runtime_viewport_id = m_camVptDlgBar.SendDlgItemMessage(IDC_LIST_RUNTIME_VIEWPORTS, LB_GETITEMDATA, runtime_viewport_index, 0);
		m_camVptDlgBar.UpdateCameraBinding(runtime_viewport_id);
	}
	// no viewport was selected
	else {
		m_camVptDlgBar.ResetRuntimeViewportPropertyControl();
	}

	m_camVptDlgBar.UpdatePropertyControls();
}

void CKEPEditView::InitializeControlDB() {
	for (POSITION posLoc = m_controlsList->GetHeadPosition(); posLoc != NULL;) { // control loop
		ControlDBObj *conPtr = (ControlDBObj *)m_controlsList->GetNext(posLoc);
		if (m_editorModeEnabled == TRUE)
			m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(conPtr->m_controlConfigName).c_str());

		/*
		for ( POSITION posLoc2 = conPtr->m_controlInfoList->GetHeadPosition (); posLoc2 != NULL; )
			ControlInfoObj	*infoPtr = (ControlInfoObj *) conPtr->m_controlInfoList->GetNext ( posLoc2 );
		*/
	}
}

void CKEPEditView::InListMisChannels(int sel) {
	if (m_editorModeEnabled == FALSE) {
		return;
	}
	/*
	for ( POSITION posLoc = m_elementDB->GetHeadPosition (); posLoc != NULL; )
	{
		CElementObj *entLoc = (CElementObj *) m_elementDB->GetNext ( posLoc );
	}
	*/
}

void CKEPEditView::InListMissileSkillCriteria(int sel) {
	if (m_editorModeEnabled == FALSE) {
		return;
	}

	/*
	for ( POSITION posLoc = m_skillDatabase->GetHeadPosition (); posLoc != NULL; )
	{
		CSkillObject	*skLoc = (CSkillObject *) m_skillDatabase->GetNext ( posLoc );
	}
	*/
}

void CKEPEditView::InListCollisionDB(int sel) {
	m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_COLLISIONDATABASES, LB_RESETCONTENT, 0, 0);
	for (POSITION posLoc = m_collisionDatabase->GetHeadPosition(); posLoc != NULL;) {
		/*CCollisionObj	*cObj = (CCollisionObj *)*/ m_collisionDatabase->GetNext(posLoc);
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_COLLISIONDATABASES, LB_ADDSTRING, 0, (LPARAM)L"Collision Object");
	}

	if (sel > -1) {
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_COLLISIONDATABASES, LB_SETCURSEL, sel, 0);
	}
}

BOOL CKEPEditView::SaveSceneObjects(CStringA fileName) {
	//this is a hack, when a CKEPObList is serialized, the "lastSavePath" will be
	//overwritten, currently waterzone db is saved to both zone file and wtz file
	//but only loaded from wtz file
	//here we remember the waterzone db state info and set it
	//back after saving the zone file so when waterzone db is auto-saved, it will be
	//saved to wtz file not zone file
	bool isWaterZoneDirty = false;
	CStringA waterZoneLoadedFrom;
	if (m_waterZoneDB != 0) {
		isWaterZoneDirty = m_waterZoneDB->IsDirty();
		waterZoneLoadedFrom = m_waterZoneDB->GetLastSavedPath();
	}

	BOOL retVal = SaveScene(fileName);

	if (m_waterZoneDB != 0) {
		m_waterZoneDB->SetDirty(isWaterZoneDirty);
		m_waterZoneDB->SetLastSavedPath(waterZoneLoadedFrom);
	}
	return retVal;
}

//! \brief Delete world object from WldObjectList and item from tree.
//! \param hItem Tree item selected for deletion.
//! \return FALSE if item not found in WldObjectList and not a group. TRUE otherwise.
//!
//! Delete world object from WldObjectList and item from tree. Delete the entire group if the selected
//! item is a mesh group. In addition, delete mesh group from tree if all its children have been removed.
BOOL CKEPEditView::DeleteWorldObjectByTreeItem(HTREEITEM hItem) {
	HWND hWndTreeCtrl = NULL;
	m_dlgBarObject.GetDlgItem(IDC_OBJECT_TREE, &hWndTreeCtrl);
	ASSERT(hWndTreeCtrl != NULL);
	if (!hWndTreeCtrl)
		return FALSE;

	CWldObject *pWldObj = reinterpret_cast<CWldObject *>(GetTreeItemData(hWndTreeCtrl, hItem));
	if (pWldObj == NULL) // mesh group
	{
		// loop through all children and delete them
		for (HTREEITEM hChildItem = TreeView_GetChild(hWndTreeCtrl, hItem); hChildItem != NULL; hChildItem = TreeView_GetChild(hWndTreeCtrl, hItem))
			DeleteWorldObjectByTreeItem(hChildItem); // note that the tree item will be deleted by this func, so we only need to reget the first child as next item

		// Tree item for mesh group will be automatically deleted when last child is deleted
		return TRUE;
	}

	POSITION posLoc = m_pWOL->Find(pWldObj);
	if (posLoc == NULL)
		return FALSE; // not found
	ASSERT(pWldObj->m_fileNameRef == Utf16ToUtf8(GetTreeItemText(hWndTreeCtrl, hItem)).c_str());

	DeleteObjectByTrace(pWldObj->m_identity);
	EditorState::GetInstance()->SetZoneDirty();

	// check for parent
	HTREEITEM hParent = TreeView_GetParent(hWndTreeCtrl, hItem);
	TreeView_DeleteItem(hWndTreeCtrl, hItem);

	// Parent must be a group
	ASSERT(GetTreeItemData(hWndTreeCtrl, hParent) == NULL);

	if (TreeView_GetChild(hWndTreeCtrl, hParent) == NULL) {
		// Parent has no more child, delete it from the tree
		TreeView_DeleteItem(hWndTreeCtrl, hParent);
	}

	return TRUE;
}

void CKEPEditView::OnRuntimeMovieListChanged() {
}

void CKEPEditView::OnTextureSwapListChanged() {
	if (m_editorModeEnabled == TRUE) {
		m_animatedTextureDBDlgBar.SendDlgItemMessage(IDC_LIST_TEX_COLLECTION, LB_RESETCONTENT, 0, 0);
		for (POSITION posLoc = TextureSwapList->GetHeadPosition(); posLoc != NULL;) {
			CTextureSwapObj *txtObjPtr = (CTextureSwapObj *)TextureSwapList->GetNext(posLoc);

			m_animatedTextureDBDlgBar.SendDlgItemMessage(IDC_LIST_TEX_COLLECTION,
				LB_ADDSTRING,
				0,
				(LPARAM)Utf8ToUtf16("Map Basis :: " + txtObjPtr->m_collectionName).c_str());
		}
	}
}

void CKEPEditView::OnEnvironmentChanged() {
	if (m_editorModeEnabled == TRUE) {
		m_enviormentBar.SetEnvironmentObj(m_environment);
		m_enviormentBar.InitializeEnvironment();
	}
}

void CKEPEditView::OnControlDBListChanged() {
	if (m_editorModeEnabled == TRUE) {
		m_controlsDlgBar.SendDlgItemMessage(IDC_LIST_CONTROLDB, LB_RESETCONTENT, 0, 0);
	}
}

BOOL CKEPEditView::LoadScene(CStringA fileName, const char *url /* = 0 */) {
	LoadScenePre();
	if (LoadSceneMain(fileName, url)) {
		OnSize(m_windowStateGL, m_szXMonitor, m_szYMonitor);
		return TRUE;
	}

	return FALSE;
}
