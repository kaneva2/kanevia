// PropTreeItemStatic.h
//
// Copyright (C) 2004 Kaneva, Inc.
// Portions adapted from Scott Ramsay (sramsay@gonavi.com)

#pragma once

#include "PropTreeItem.h"

class PROPTREE_API CPropTreeItemStatic : public CPropTreeItem {
public:
	CPropTreeItemStatic();
	virtual ~CPropTreeItemStatic();

public:
	// The attribute area needs drawing
	virtual void DrawAttribute(CDC* pDC, const RECT& rc);

	// Retrieve the item's attribute value (in this case the CString)
	virtual LPARAM GetItemValue();

	// Set the item's attribute value
	virtual void SetItemValue(LPARAM lParam);

protected:
	CStringW m_sAttribute;
};

