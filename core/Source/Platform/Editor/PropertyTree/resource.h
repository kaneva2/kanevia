/******************************************************************************
 resource.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PropTree.rc
//
#define IDC_SPLITTER                    1000
#define IDC_FPOINT                      1001
#define IDS_TRUE                        10000
#define IDS_FALSE                       10001
#define IDS_NOITEMSEL                   10002
#define IDS_SELFORINFO                  10003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2000
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
