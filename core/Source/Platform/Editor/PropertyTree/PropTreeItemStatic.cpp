// PropTreeItemStatic.cpp
//
// Copyright (C) 2004 Kaneva, Inc.
// Portions adapted from Scott Ramsay (sramsay@gonavi.com)

#include "stdafx.h"
#include "PropTree.h"

#include "PropTreeItemStatic.h"

CPropTreeItemStatic::CPropTreeItemStatic() :
		m_sAttribute(_T("")) {
}

CPropTreeItemStatic::~CPropTreeItemStatic() {
}

void CPropTreeItemStatic::DrawAttribute(CDC* pDC, const RECT& rc) {
	ASSERT(m_pProp != NULL);

	pDC->SelectObject(m_pProp->GetNormalFont());
	pDC->SetTextColor(RGB(0, 0, 0));
	pDC->SetBkMode(TRANSPARENT);

	CRect r = rc;
	pDC->DrawText(m_sAttribute, r, DT_SINGLELINE | DT_VCENTER);
}

LPARAM CPropTreeItemStatic::GetItemValue() {
	return (LPARAM)(const wchar_t*)m_sAttribute;
}

void CPropTreeItemStatic::SetItemValue(LPARAM lParam) {
	if (lParam == 0L) {
		TRACE0("CPropTreeItemStatic::SetItemValue() - Invalid lParam value\n");
		return;
	}

	m_sAttribute = (const wchar_t*)lParam;
}
