#if !defined(AFX_PROPTREELIST_H__2E09E831_09F5_44AA_B41D_9C4BF495873C__INCLUDED_)
#define AFX_PROPTREELIST_H__2E09E831_09F5_44AA_B41D_9C4BF495873C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropTreeList.h : header file
//
// Copyright (C) 2004 Kaneva, Inc.
// Portions adapted from Scott Ramsay (sramsay@gonavi.com)

class CPropTree;

/////////////////////////////////////////////////////////////////////////////
// CPropTreeList window

class PROPTREE_API CPropTreeList : public CWnd {
	// Construction
public:
	CPropTreeList();
	virtual ~CPropTreeList();

	BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);

	// Attributes
public:
	void SetPropOwner(CPropTree* pProp);

protected:
	// CPropTree class that this class belongs
	CPropTree* m_pProp;

	// bitmap back buffer for flicker free drawing
	CBitmap m_BackBuffer;

	// current diminsions of the back buffer
	CSize m_BackBufferSize;

	// splitter pevious position
	LONG m_nPrevCol;

	// TRUE if we are dragging the splitter
	BOOL m_bColDrag;

	// Operations
public:
	void UpdateResize();

protected:
	void RecreateBackBuffer(int cx, int cy);
	void CheckVisibleFocus();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropTreeList)
	//}}AFX_VIRTUAL

	// Implementation
public:
	// Generated message map functions
protected:
	//{{AFX_MSG(CPropTreeList)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg UINT OnGetDlgCode();
	//}}AFX_MSG
public:
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPTREELIST_H__2E09E831_09F5_44AA_B41D_9C4BF495873C__INCLUDED_)
