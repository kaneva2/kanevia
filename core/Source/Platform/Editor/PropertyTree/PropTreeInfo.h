#if !defined(AFX_PROPTREEINFO_H__22BD9C18_A68C_4BB8_B7FC_C4A7DA0E1EBF__INCLUDED_)
#define AFX_PROPTREEINFO_H__22BD9C18_A68C_4BB8_B7FC_C4A7DA0E1EBF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropTreeInfo.h : header file
//
//
// Copyright (C) 2004 Kaneva, Inc.
// Portions adapted from Scott Ramsay (sramsay@gonavi.com)

class CPropTree;

/////////////////////////////////////////////////////////////////////////////
// CPropTreeInfo window

class PROPTREE_API CPropTreeInfo : public CStatic {
	// Construction
public:
	CPropTreeInfo();

	// Attributes
public:
	// CPropTree class that this class belongs
	void SetPropOwner(CPropTree* pProp);

protected:
	CPropTree* m_pProp;

	// Operations
public:
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropTreeInfo)
	//}}AFX_VIRTUAL

	// Implementation
public:
	virtual ~CPropTreeInfo();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPropTreeInfo)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPTREEINFO_H__22BD9C18_A68C_4BB8_B7FC_C4A7DA0E1EBF__INCLUDED_)
