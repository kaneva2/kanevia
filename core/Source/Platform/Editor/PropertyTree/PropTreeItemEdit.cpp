// PropTreeItemEdit.cpp : implementation file
//
// Copyright (C) 2004 Kaneva, Inc.
// Portions adapted from Scott Ramsay (sramsay@gonavi.com)

#error Dead code?

#include "stdafx.h"
#include "proptree.h"
#include "PropTreeItemEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropTreeItemEdit

CPropTreeItemEdit::CPropTreeItemEdit() :
		m_sEdit(_T("")),
		m_nFormat(ValueFormatText),
		m_bPassword(FALSE),
		m_fValue(0.0f) {
}

CPropTreeItemEdit::~CPropTreeItemEdit() {
}

BEGIN_MESSAGE_MAP(CPropTreeItemEdit, CEdit)
//{{AFX_MSG_MAP(CPropTreeItemEdit)
ON_WM_GETDLGCODE()
ON_WM_KEYDOWN()
ON_CONTROL_REFLECT(EN_KILLFOCUS, OnKillfocus)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropTreeItemEdit message handlers

void CPropTreeItemEdit::DrawAttribute(CDC* pDC, const RECT& rc) {
	ASSERT(m_pProp != NULL);

	pDC->SelectObject(IsReadOnly() ? m_pProp->GetNormalFont() : m_pProp->GetBoldFont());
	pDC->SetTextColor(RGB(0, 0, 0));
	pDC->SetBkMode(TRANSPARENT);

	CRect r = rc;

	TCHAR ch;

	// can't use GetPasswordChar(), because window may not be created yet
	ch = (m_bPassword) ? '*' : '\0';

	if (ch) {
		CString s;

		s = m_sEdit;
		for (LONG i = 0; i < s.GetLength(); i++)
			s.SetAt(i, ch);

		pDC->DrawText(s, r, DT_SINGLELINE | DT_VCENTER);
	} else {
		pDC->DrawText(m_sEdit, r, DT_SINGLELINE | DT_VCENTER);
	}
}

void CPropTreeItemEdit::SetAsPassword(BOOL bPassword) {
	m_bPassword = bPassword;
}

void CPropTreeItemEdit::SetValueFormat(ValueFormat nFormat) {
	m_nFormat = nFormat;
}

LPARAM CPropTreeItemEdit::GetItemValue() {
#if 0
	switch (m_nFormat)
	{
		case ValueFormatNumber:
			return _ttoi(m_sEdit);

		case ValueFormatFloatPointer:
			_stscanf(m_sEdit, _T("%f"), &m_fValue);
			return (LPARAM)&m_fValue;
	}

	return (LPARAM)(const wchar_t*)m_sEdit;
#endif

	return (LPARAM)(const wchar_t*)m_sEdit;
}

void CPropTreeItemEdit::SetItemValue(LPARAM lParam) {
	switch (m_nFormat) {
		case ValueFormatNumber:
			m_sEdit.Format(_T("%d"), lParam);
			return;

		case ValueFormatFloatPointer: {
			TCHAR tmp[MAX_PATH];
			m_fValue = *(float*)lParam;
			_stprintf(tmp, _T("%f"), m_fValue);
			m_sEdit = tmp;
		}
			return;
	}

	if (lParam == 0L) {
		TRACE0("CPropTreeItemEdit::SetItemValue - Invalid lParam value\n");
		return;
	}

	m_sEdit = (const wchar_t*)lParam;
}

void CPropTreeItemEdit::OnMove() {
	if (IsWindow(m_hWnd))
		SetWindowPos(NULL, m_rc.left, m_rc.top, m_rc.Width(), m_rc.Height(), SWP_NOZORDER | SWP_NOACTIVATE);
}

void CPropTreeItemEdit::OnRefresh() {
	if (IsWindow(m_hWnd))
		SetWindowText(m_sEdit);
}

void CPropTreeItemEdit::OnCommit() {
	// hide edit control
	ShowWindow(SW_HIDE);

	// store edit text for GetItemValue
	GetWindowText(m_sEdit);
}

void CPropTreeItemEdit::OnActivate() {
	// Check if the edit control needs creation
	if (!IsWindow(m_hWnd)) {
		DWORD dwStyle;

		dwStyle = WS_CHILD | ES_AUTOHSCROLL;
		Create(dwStyle, m_rc, m_pProp->GetCtrlParent(), GetCtrlID());
		SendMessage(WM_SETFONT, (WPARAM)m_pProp->GetNormalFont()->m_hObject);
	}

	SetPasswordChar((TCHAR)(m_bPassword ? '*' : 0));
	SetWindowText(m_sEdit);
	SetSel(0, -1);

	SetWindowPos(NULL, m_rc.left, m_rc.top, m_rc.Width(), m_rc.Height(), SWP_NOZORDER | SWP_SHOWWINDOW);
	SetFocus();
}

UINT CPropTreeItemEdit::OnGetDlgCode() {
	return CEdit::OnGetDlgCode() | DLGC_WANTALLKEYS;
}

void CPropTreeItemEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) {
	if (nChar == VK_RETURN)
		CommitChanges();

	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CPropTreeItemEdit::OnKillfocus() {
	CommitChanges();
}
