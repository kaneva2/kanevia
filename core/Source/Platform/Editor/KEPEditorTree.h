/******************************************************************************
 KEPEditorTree.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
// CKEPEditorTree

enum KEPEDITTREEID {
	KEP_GAMETREE = 0,
	KEP_ZONETREE,
	KEP_AIMISSIONSTREE,
	KEP_OBJECTSTREE
};

typedef struct _KEPTREEDATA {
	std::wstring wstrObjectLink;
	//wchar_t wszObjectLink[255];
	UINT dialogId;
	UINT dialogType;
	BOOL disabled;

} KEPTREEDATA, *LPKEPTREEDATA;

class CKEPEditorTree : public CTreeCtrl {
	DECLARE_DYNAMIC(CKEPEditorTree)

public:
	CKEPEditorTree();
	virtual ~CKEPEditorTree();

	BOOL LoadTab(KEPEDITTREEID tabName);
	void InsertNode(const wchar_t* nodeName, WORD indentLevel, UINT dlgId, UINT dlgType = 0, const BOOL enable = FALSE);
	BOOL Init(HWND hWndParent);

	void SetGameStatus(const BOOL gameOpen, const BOOL zoneOpen);
	BOOL GameOpen() { return m_gameOpen; }
	BOOL ZoneOpen() { return m_zoneOpen; }

protected:
	virtual BOOL DeleteAllItems();

	void InitGameTab();
	void InitZoneTab();
	void InitAIMissionsTab();
	void InitObjectsTab();

	BOOL FreeChildTree(HTREEITEM hItem);
	HWND m_hWndParent;

	BOOL NotifyTreeChange() { return m_notifyTreeChange; }
	void NotifyTreeChange(BOOL notify) {
		m_notifyTreeChange = notify;
		m_lastSelectedItem = NULL;
	}

	DECLARE_MESSAGE_MAP()
private:
	HTREEITEM m_lastSelectedItem;
	BOOL m_notifyTreeChange;

	BOOL m_gameOpen;
	BOOL m_zoneOpen;

	KEPEDITTREEID m_currentTabName;

public:
	afx_msg void OnTvnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnRClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint ptMousePos);

	afx_msg void OnPaint();
};
