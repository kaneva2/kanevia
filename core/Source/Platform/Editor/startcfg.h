#pragma once

#include <getset.h>

class CKEPEditView;

// helper class for editing startcfg.xml w/o exposing it to scripting
class StartCfg : public GetSet {
public:
	static void Init(CKEPEditView *parent) {
		if (startCfg == 0)
			startCfg = new StartCfg(parent);
	}
	static void Free() {
		if (startCfg != 0)
			DELETE_AND_ZERO(startCfg);
	}
	static StartCfg *Instance() { return startCfg; }

	void Save();

protected:
	StartCfg(CKEPEditView *parent);

	DECLARE_GETSET

	CKEPEditView *m_parent;
	static StartCfg *startCfg;
};
