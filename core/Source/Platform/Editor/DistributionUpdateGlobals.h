#pragma once

/******************************************************************************
 DistributionUpdateGlobals.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#define MSG_STATUS WM_USER + 1
#define MSG_COMPLETE WM_USER + 2
#define MSG_ERROR WM_USER + 3
#define MSG_STATUS_ENCRYPTING WM_USER + 4
#define MSG_STATUS_CLIENT_UPDATE WM_USER + 5
#define MSG_STATUS_SERVER_UPDATE WM_USER + 6
#define MSG_STATUS_MAKEDEPLOY_UPDATE WM_USER + 7

typedef struct _STATUSU {
	char stepTitle[255];
	char stepDesc[255];
	char fileName[255];
	LONG stepId;
	char lastItemCompleteTxt[255];
} STATUSU;

// values for creating a distribution
enum EDBExport {
	dbe_dontExportToDb,
	dbe_exportToDb,
	dbe_onlyExportToDb
};

