/******************************************************************************
 WaitDialog.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// WaitDialog.cpp : implementation file
//

#include "stdafx.h"
#include "KEPEdit.h"
#include "WaitDialog.h"
#include ".\waitdialog.h"

// CWaitDialog dialog

IMPLEMENT_DYNAMIC(CWaitDialog, CDialog)
CWaitDialog::CWaitDialog(CWnd* pParent /*=NULL*/) :
		CDialog(CWaitDialog::IDD, pParent),
		m_id(0),
		m_theCaller(NULL) {
}

CWaitDialog::~CWaitDialog() {
}

void CWaitDialog::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDR_WAIT_AVI, m_wndAnim);
}

BEGIN_MESSAGE_MAP(CWaitDialog, CDialog)
//	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

UINT CWaitDialog::showIt(LPVOID pParam) {
	CWaitDialog dlg(CWnd::FromHandle(HWND_DESKTOP));
	dlg.m_theCaller = pParam;
	dlg.DoModal();
	return 0;
}

// CWaitDialog message handlers
void CWaitDialog::ShowIt() {
#ifdef _DEBUG
	AfxBeginThread(showIt, (LPVOID)this);
#else
	AfxBeginThread(showIt, (LPVOID)this, THREAD_PRIORITY_NORMAL);
#endif
}

void CWaitDialog::KillIt() {
	ASSERT(m_theHwnd);
	::SendMessage(m_theHwnd, WM_CLOSE, 0, 0);
	//::EndDialog(m_theHwnd, 0);
}

BOOL CWaitDialog::OnInitDialog() {
	CRect r;

	CWnd* pAnimControl = GetDlgItem(IDR_WAIT_AVI);
	pAnimControl->GetClientRect(&r);
	m_wndAnim.Create(WS_CHILD | ACS_CENTER | WS_VISIBLE | ACS_TRANSPARENT, r, pAnimControl, IDR_WAIT_AVI);
	m_wndAnim.Open(IDC_WAIT_AVI);

	m_wndAnim.Play(0, -1, -1); // loop forever

	((CWaitDialog*)(m_theCaller))->m_theHwnd = this->m_hWnd;

	CDialog::OnInitDialog();

	return TRUE; // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CWaitDialog::UpdateText(CStringA s) {
	if (m_theHwnd) {
		::SetDlgItemText(m_theHwnd, IDC_STATIC, Utf8ToUtf16(s).c_str());
	}
}
