/******************************************************************************
 GetSetContextHandler.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// GetSetContextHandler.cpp : implementation file
//

#include "stdafx.h"
#include "GetSetContextHandler.h"
#include ".\getsetcontexthandler.h"

// GetSetContextHandler

IMPLEMENT_DYNAMIC(GetSetContextHandler, CWnd)
GetSetContextHandler::GetSetContextHandler() {
}

GetSetContextHandler::~GetSetContextHandler() {
}

BEGIN_MESSAGE_MAP(GetSetContextHandler, CWnd)
END_MESSAGE_MAP()

// GetSetContextHandler message handlers

BOOL GetSetContextHandler::OnCommand(WPARAM wParam, LPARAM lParam) {
	// TODO: Add your specialized code here and/or call the base class

	return CWnd::OnCommand(wParam, lParam);
}
