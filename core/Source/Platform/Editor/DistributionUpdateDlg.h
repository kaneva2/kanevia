/******************************************************************************
 DistributionUpdateDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxwin.h"
#include "DistributionUpdateGlobals.h"
#include "DistributionTargetDlg.h"

// DistributionUpdateDlg dialog

class CDistributionUpdateDlg : public CDialog {
	DECLARE_DYNAMIC(CDistributionUpdateDlg)

public:
	CDistributionUpdateDlg(CWnd* pParent, CKEPEditView* theView, CStringA encryptKey,
		CStringA gameId, CStringA licenseKey, CStringA destinationPath, CStringA patchFolderLocation,
		EDBExport exportToDb, bool tristripAssets, CRCLIST* oldPatchFiles, int oldFileCount,
		bool encryptAssets, bool disableDialogs, bool menusOnly);
	virtual ~CDistributionUpdateDlg();

	// Dialog Data
	enum { IDD = IDD_DIALOG_DISTRIBUTION_UPDATE };
	virtual BOOL OnInitDialog();

private:
	static CKEPEditView* m_theView;
	static CStringA m_encryptKey;
	static CStringA m_gameId;
	static CStringA m_destinationPath;
	static CStringA m_patchFolderLocation;
	static CStringA m_licenseKey;
	static HANDLE m_hEvDialogClosing;
	static HANDLE m_hEvDistributionHasClosed;
	static EDBExport m_exportToDb;
	static CRCLIST* m_oldPatchFiles;
	static int m_oldFileCount;
	static bool m_tristrip;
	static bool m_encryptAssets;
	static bool m_disableDialogs;
	static bool m_menusOnly;
	CButton m_cancelButton;
	CButton m_okButton;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	afx_msg LRESULT OnStatusMessage(WPARAM wp, LPARAM lp);
	static UINT showIt(LPVOID pParam);
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()
};
