/******************************************************************************
 OpenZoneDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxwin.h"

// OpenZoneDlg dialog

class OpenZoneDlg : public CDialog {
	DECLARE_DYNAMIC(OpenZoneDlg)

public:
	OpenZoneDlg(const char* gameDir, CWnd* pParent = NULL); // standard constructor
	virtual ~OpenZoneDlg();

	// Dialog Data
	enum { IDD = IDD_OPEN_ZONE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	CStringA m_gameDir;

	DECLARE_MESSAGE_MAP()
	CStringA m_fname;
	CListBox m_zoneList;

public:
	virtual BOOL OnInitDialog();
	const char* ZoneName() const { return m_fname; }

	afx_msg void OnBnClickedOk();

	afx_msg void OnLbnDblclkZoneList();
};
