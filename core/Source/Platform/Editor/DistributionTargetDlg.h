#pragma once
#include "afxwin.h"

#include "Resource.h"
#include "DistributionUpdateGlobals.h"

typedef struct _CRCLIST {
	std::string filename;
	std::string directory;
	std::string serverfile;
	std::string crc;
	BOOL bRegister;
	LONG nFilesize;
	BOOL bUpToDate;
	BOOL bRequired; // added by Jonny 03/27/07 to specify optional additional assets for progressive dl
	std::string command; // additional command to run on this file ie "delete" functionality
	struct _CRCLIST* next;
} CRCLIST;

class CDistributionTargetDlg : public CDialog {
	DECLARE_DYNAMIC(CDistributionTargetDlg)

public:
	CDistributionTargetDlg(CWnd* pParent = NULL, bool showDbExport = false);
	virtual ~CDistributionTargetDlg();
	bool ProcessVersionInfo(const CStringA& fileName);
	static bool ReadCRCTable2(int count, CRCLIST* cList, const char* szServerInfoTemp);

	enum { IDD = IDD_DIALOG_PUBLISHING_LOCATION };

	enum publishlocation { COMPLETE_DISTRO,
		ASSET_ONLY,
		DB_ONLY };

	void LoadVersionInfo();

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	BOOL OnInitDialog();
	void OnOK();

	DECLARE_MESSAGE_MAP()
public:
	int pubSelection;
	CRCLIST* m_oldCRCList;
	int m_oldFileCount;
	EDBExport exportToDb;
	BOOL tristripAssets;
	BOOL disableDialogs;
	BOOL encryptAssets;

private:
	bool canDbExport;

public:
	afx_msg void OnCbnSelchangeComboDistro();
};
