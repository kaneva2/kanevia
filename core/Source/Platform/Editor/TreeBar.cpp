// TreeBar.cpp : implementation file
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved

#include "stdafx.h"
#include "KEPEditGlobals.h"
#include "TreeBar.h"

// CTreeBar

CTreeBar::CTreeBar(int minW, int minH, int prefW, int prefH) :
		CKEPControlBar(minW, minH, prefW, prefH), m_hWndParent(0) {
}

CTreeBar::~CTreeBar() {
}

BEGIN_MESSAGE_MAP(CTreeBar, CKEPControlBar)
//{{AFX_MSG_MAP(CTreeBar)
ON_WM_CREATE()
ON_WM_SIZE()
//}}AFX_MSG_MAP
//	ON_WM_DESTROY()
END_MESSAGE_MAP()

// CTreeBar message handlers
int CTreeBar::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	if (CKEPControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_TabControl.Create(WS_CHILD | WS_VISIBLE, CRect(0, 0, 0, 0), this, 100))
		return -1;

	if (!m_TreeList.Create(WS_CHILD | WS_VISIBLE | TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT, CRect(0, 0, 0, 0), this, 123))
		return -1;

	// older versions of Windows* (NT 3.51 for instance)
	// fail with DEFAULT_GUI_FONT
	if (!m_font.CreateStockObject(DEFAULT_GUI_FONT))
		if (!m_font.CreatePointFont(80, L"MS Sans Serif"))
			return -1;

	m_TreeList.SetFont(&m_font);
	m_TabControl.SetFont(&m_font);

	m_TabControl.Init(m_hWnd);
	m_TreeList.Init(m_hWnd);
	m_TreeList.SetParent(&m_TabControl);

	m_hWndParent = lpCreateStruct->hwndParent;

	return 0;
}

void CTreeBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler) {
	CKEPControlBar::OnUpdateCmdUI(pTarget, bDisableIfNoHndler);

	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}

void CTreeBar::OnSize(UINT nType, int cx, int cy) {
	CKEPControlBar::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	CRect rc;
	GetClientRect(rc);

	m_TabControl.MoveWindow(rc);

	rc.bottom -= 5;
	rc.top += 25;
	rc.left += 5;
	rc.right -= 5;
	m_TreeList.MoveWindow(rc);
}

///---------------------------------------------------------
// CTreeBar::WindowProc
//
// [in] message
// [in] wParam
// [in] lParam
//
// [Returns]
//    LRESULT
LRESULT CTreeBar::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) {
	if (message == KEP_TABCHANGE) {
		m_TreeList.LoadTab((KEPEDITTREEID)lParam);
		return 0;
	}

	if (message == KEP_TREECHANGE) {
		//tell the parameter list about the new object

		::SendMessage(m_hWndParent, message, wParam, lParam);
	}

	return CWnd::WindowProc(message, wParam, lParam);
}

void CTreeBar::SetGameStatus(const BOOL gameOpen, const BOOL zoneOpen) {
	m_TreeList.SetGameStatus(gameOpen, zoneOpen);
}