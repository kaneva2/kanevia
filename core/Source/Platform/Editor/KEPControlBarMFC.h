#pragma once

class CControlBarInfo;

class CKEPControlBarMFC : public CControlBar {
private:
	int m_minWidth, m_minHeight;
	int m_prefWidth, m_prefHeight;
	bool bDraggingFlagFixed;

public:
	CKEPControlBarMFC(int minW, int minH, int prefW, int prefH) :
			m_minWidth(minW), m_minHeight(minH), m_prefWidth(prefW), m_prefHeight(prefH), bDraggingFlagFixed(false) {}

	BOOL Create(CWnd* pParentWnd, const char* szCaption, UINT nID, UINT dwStyle);
	BOOL Create(CWnd* pParentWnd, UINT nCaptionID, UINT nID, UINT dwStyle);

	virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler); // overload
	virtual CSize CalcFixedLayout(BOOL bStretch, BOOL bHorz); // overload

	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	afx_msg void OnNcPaint();
	afx_msg void OnClose();
	afx_msg void OnMove(int x, int y);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnNcLButtonDblClk(UINT nHitTest, CPoint point);
	afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
	afx_msg void OnNcLButtonUp(UINT nHitTest, CPoint point);

	static void InitAll();
	static void CleanupAll();

	DECLARE_MESSAGE_MAP()
};
