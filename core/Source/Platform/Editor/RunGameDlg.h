#pragma once

// CRunGameDlg dialog

class CRunGameDlg : public CDialog {
	DECLARE_DYNAMIC(CRunGameDlg)

public:
	static bool ShouldRun(const char* path, UINT idStringMsg, CWnd* pParent, bool& client, bool& server);

private:
	CRunGameDlg(const char* path, UINT idStringMsg, CWnd* pParent = NULL); // standard constructor
	virtual ~CRunGameDlg();
	BOOL OnInitDialog();

	// Dialog Data
	enum { IDD = IDD_RUN_CLIENT_AND_SERVER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	bool m_runServer;
	afx_msg void OnBnClickedOk();
	UINT m_idStringMsg;
	CStringA m_path;
	afx_msg void OnStnClickedRunHelp();
	bool m_runClient;
};
