/******************************************************************************
 EditorState.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "StdAfx.h"
#include ".\editorstate.h"

EditorState* EditorState::inst = 0;

EditorState::EditorState(void) {
	m_zoneDirty = false;
}

EditorState::~EditorState(void) {
}

EditorState* EditorState::GetInstance() {
	if (inst == 0) {
		inst = new EditorState;
	}
	return inst;
}

void EditorState::SaveAll() {
	IKEPGameFileSet listDirty;
	GetDirtyGameFiles(listDirty);

	IKEPGameFileIterator itList;
	for (itList = listDirty.begin(); itList != listDirty.end(); itList++) {
		IKEPGameFile* gameFilePtr = **(itList);
		if (gameFilePtr->GetFileName() != "") {
			gameFilePtr->SaveFile();
		}
	}
}

bool EditorState::IsDirty() {
	bool retVal = false;
	IKEPGameFileSet listDirty;
	GetDirtyGameFiles(listDirty);
	retVal = listDirty.size() > 0;

	return retVal;
}

void EditorState::GetDirtyGameFiles(IKEPGameFileSet& dirtyFiles) {
	IKEPGameFileIterator itFile;
	for (itFile = m_gameFiles.begin(); itFile != m_gameFiles.end(); itFile++) {
		IKEPGameFile* filePtr = **(itFile);
		if (filePtr && filePtr->IsDirty() && filePtr->GetFileName() != "") {
			dirtyFiles.insert(*(itFile));
		}
	}
}

void EditorState::RegisterGameFile(IKEPGameFile** gameFile) {
	if (gameFile && m_gameFiles.find(gameFile) == m_gameFiles.end())
		m_gameFiles.insert(gameFile);
}

void EditorState::UnRegisterGameFile(IKEPGameFile** gameFile) {
	IKEPGameFileIterator itFile = m_gameFiles.find(gameFile);
	if (itFile != m_gameFiles.end()) {
		m_gameFiles.erase(itFile);
	}
}

void EditorState::UnRegisterAllGameFiles() {
	m_gameFiles.clear();
}