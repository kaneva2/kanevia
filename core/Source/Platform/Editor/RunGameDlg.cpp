// RunGame.cpp : implementation file
//

#include "stdafx.h"
#include "KepEdit.h"
#include "RunGameDlg.h"

// CRunGameDlg dialog

IMPLEMENT_DYNAMIC(CRunGameDlg, CDialog)

CRunGameDlg::CRunGameDlg(const char* path, UINT idStringMsg, CWnd* pParent /*=NULL*/) :
		CDialog(CRunGameDlg::IDD, pParent), m_runServer(false), m_idStringMsg(idStringMsg), m_path(path), m_runClient(false) {
}

CRunGameDlg::~CRunGameDlg() {
}

void CRunGameDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
}

bool CRunGameDlg::ShouldRun(const char* path, UINT idStringMsg, CWnd* pParent, bool& client, bool& server) {
	CRunGameDlg dlg(path, idStringMsg, pParent);
	if (dlg.DoModal() == IDOK && (dlg.m_runClient || dlg.m_runServer)) {
		server = dlg.m_runServer;
		client = dlg.m_runClient;
		return true;
	} else
		return false;
}

BEGIN_MESSAGE_MAP(CRunGameDlg, CDialog)
ON_BN_CLICKED(IDOK, &CRunGameDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// CRunGameDlg message handlers

void CRunGameDlg::OnBnClickedOk() {
	CDialog::OnOK();
	m_runServer = IsDlgButtonChecked(IDC_SERVER) != BST_UNCHECKED;
	m_runClient = IsDlgButtonChecked(IDC_CLIENT) != BST_UNCHECKED;
}

BOOL CRunGameDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	UpdateData(TRUE);

	CheckDlgButton(IDC_SERVER, BST_CHECKED);
	CheckDlgButton(IDC_CLIENT, BST_CHECKED);
	CStringW strMsg;
	strMsg.LoadStringW(m_idStringMsg);
	GetDlgItem(IDC_RUN_HELP)->SetWindowText(strMsg + L"\r\n" + Utf8ToUtf16(m_path).c_str());

	return TRUE;
}
