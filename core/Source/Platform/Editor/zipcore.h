#pragma once
/******************************************************************************
 zipcore.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//zipcore.h

#define WRITEBUFFERSIZE (16384)
#define MAXFILENAME (256)
#define CASESENSITIVITY (0)

uLong filetime(char* f, tm_zip tmzip, uLong* dt);
//int do_extract_currentfile(unzFile uf,const int* popt_extract_without_path,
//						   int* popt_overwrite, char *destfile);
int do_extract_currentfile(unzFile uf, const int* popt_extract_without_path, int* popt_overwrite);
int do_extract_onefile(unzFile uf, const char* filename, int opt_extract_without_path, int opt_overwrite);
int do_extract(unzFile uf, int opt_extract_without_path, int opt_overwrite);
void change_file_date(const char* filename, uLong dosdate, tm_unz tmu_date);
int makedir(const char* newdir);
int mymkdir(const char* dirname);

void ZipUpFile(char* filename, char* dest);