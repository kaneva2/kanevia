//
// CKEPEditCommandInfo: Specialization of CCommandLineInfo that
// adds support for application specific command-line arguments.
// It parses the arguments and holds state.  It's easy to re-generate
// this state (i.e. parse the command line arguments again) by calling
// AfxGetApp()->ParseCommandLine(cmdInfo) again from anywhere that can
// use AfxGetApp.
//
// Copyright (C) 2004 Kaneva, Inc.
// All Rights Reserved
//
#pragma once
#include "Core/Crypto/CryptDecrypt.h"
#include "Common/KEPUtil/SolMan.h"

class CKEPEditCommandInfo : public CCommandLineInfo {
public:
	//
	// I could define a bitmask for the various arguments.
	//
	CKEPEditCommandInfo() :
			m_createAvatar(false), m_autoCreateStar(false), m_commandline_override_ip_port(false), m_gotoplayeronstartup(false), m_gotoapartmentonstartup(false), m_gotochannelonstartup(false), m_gotozoneonstartup(false), m_gotourlonstartup(false), m_instanceId(0), m_zoneIndex(0), m_autoLogon(false), m_playerServerId(-1), m_tryon(false), m_tryonGlid(0), m_tryonUseType(USE_TYPE_NONE), m_hwndEventListener((HWND)INVALID_HANDLE_VALUE), m_clientRunning(false), m_startSATFramework(false) {}

	virtual ~CKEPEditCommandInfo() {}

	//
	// Although it's not very good, I have maintained the current situation
	// of using flags for most things and stripping out the first characters
	// to get the argument.  The only real argument is used to override the
	// server ip and port information.
	//
	virtual void ParseParam(const wchar_t *pszParam, BOOL bFlag, BOOL bLast) {
		CStringA resToken = Utf16ToUtf8(pszParam).c_str();

		if (bFlag == FALSE) {
			// trim it
			CStringA ss(resToken);
			ss.Trim();
			int start = 0;
			m_ip = ss.Tokenize(":", start);
			m_port = ss.Tokenize(":", start);
			m_commandline_override_ip_port = true;
		} else {
			//
			// All these are really flags with some non-standard handling to get the
			// arguments out, if necessary.
			//
			if (resToken.GetLength() > 3 && resToken.Left(2).CompareNoCase("gp") == 0) {
				// do they want to jump to a player on startup?
				m_playerServerId = atoi(resToken.Mid(2));
				m_gotoplayeronstartup = true;
			} else if (resToken.CompareNoCase("cs") == 0) {
				// Commence the editor with the Create New Star Wizard.
				m_autoCreateStar = true;
			} else if (resToken.GetLength() > 3 && resToken.Left(2).CompareNoCase("ga") == 0) {
				// do they want to jump to a player's apartment on startup?
				m_playerServerId = atoi(resToken.Mid(2));
				m_gotoapartmentonstartup = true;
			} else if (resToken.GetLength() > 3 && resToken.Left(2).CompareNoCase("gz") == 0) {
				// do they want to jump to an instance zone on startup?
				// format of param is -gz<zoneIndex>:<instanceId>
				if (sscanf_s(resToken, "gz%d:%d", &m_zoneIndex, &m_instanceId) == 2) {
					m_gotozoneonstartup = true;
				}
			} else if (resToken.GetLength() > 2 && resToken.Left(2).CompareNoCase("gc") == 0) {
				// do they want to jump to a channel on startup?
				// format of param is -gc<instanceId>
				m_instanceId = atol(resToken.Mid(2));
				m_gotochannelonstartup = true;
			} else if (resToken.GetLength() > 2 && resToken.Left(2).CompareNoCase("gu") == 0) {
				// do they want to goto a URL on startup?
				// format of param is -gu<url>
				m_url = resToken.Mid(2);
				m_gotourlonstartup = true;

				// Optionally, the username and password can be contained in the url (albeit in
				// encrypted form).  If this info is present let's decrypte and make it look like
				// it came through as plaintext on the command line.
				//
				URL url = URL::parse((const char *)m_url);
				std::map<std::string, std::string>::const_iterator iuserid = url.paramMap().find("User");
				std::map<std::string, std::string>::const_iterator ipassword = url.paramMap().find("Id");

				if (ipassword != url.paramMap().cend() && iuserid != url.paramMap().cend()) {
					string encryptedUserName(iuserid->second);
					string encryptedPassword(ipassword->second);

					m_userName = decryptAES((unsigned char *)encryptedUserName.c_str(), encryptedUserName.length());
					m_origPassword = decryptAES((unsigned char *)encryptedPassword.c_str(), encryptedPassword.length());
					m_autoLogon = true;
				}
			} else if (resToken.GetLength() > 3 && resToken.Left(3).CompareNoCase("try") == 0) {
				int useTypeInt = USE_TYPE_NONE;
				if (sscanf_s(resToken, "try%d:%d", &m_tryonGlid, &useTypeInt) == 2) {
					m_tryonUseType = (USE_TYPE)useTypeInt;
					m_tryon = true;
				}
			} else if (resToken.GetLength() > 1 && resToken.Left(1).CompareNoCase("u") == 0) {
				m_userName = resToken.Mid(1);
				if (!m_origPassword.IsEmpty() && !m_userName.IsEmpty())
					m_autoLogon = true;
			} else if (resToken.GetLength() > 1 && resToken.Left(1).CompareNoCase("c") == 0) {
				string password;
				DecodeAndDecryptString(resToken.Mid(1), password, ENCRYPT_KEY);
				m_origPassword = password.c_str();
				if (!m_origPassword.IsEmpty() && !m_userName.IsEmpty())
					m_autoLogon = true;
			} else if (resToken.GetLength() > 1 && resToken.Left(1).CompareNoCase("p") == 0) {
				m_origPassword = resToken.Mid(1);
				if (!m_origPassword.IsEmpty() && !m_userName.IsEmpty())
					m_autoLogon = true;
			} else if (resToken.CompareNoCase("av") == 0) {
				m_createAvatar = true;
			} else if (resToken.GetLength() > 1 && resToken.Left(1).CompareNoCase("k") == 0) {
				m_patchUrl = resToken.Mid(1);
				CStringA rChar = m_patchUrl.Right(1);
				if (rChar != "/" && rChar != "\\")
					m_patchUrl.Append("/");
			} else if (resToken.GetLength() > 1 && resToken.Left(1).CompareNoCase("w") == 0) {
				// load the wok file automagically
				// Be sure to use a fully qualified file name, or the editor will get
				// confused as to which directories to use, unfortunately.  That's
				// probably a bug.
				m_wokFile = resToken.Mid(1);
			} else if (resToken.GetLength() > 1 && resToken.Left(1).CompareNoCase("d") == 0) {
				m_distributionConfig = resToken.Mid(1);
			} else if (resToken.GetLength() > 2 && resToken.Left(2).CompareNoCase("ew") == 0) {
				m_hwndEventListener = (HWND)atoi(resToken.Mid(2));
			} else if (resToken.GetLength() > 2 && resToken.Left(2).CompareNoCase("an") == 0) {
				// 3D App Name
				m_appName = resToken.Mid(2);
			} else if (resToken.GetLength() > 2 && resToken.Left(2).CompareNoCase("tn") == 0) {
				// Template Name
				m_templateName = resToken.Mid(2);
			} else if (resToken.GetLength() > 2 && resToken.Left(2).CompareNoCase("id") == 0) {
				// Install Directory
				m_installDirectory = resToken.Mid(2);
			} else if (resToken.Left(1).CompareNoCase("r") == 0) {
				m_clientRunning = true;
			} else if (resToken.GetLength() == 3 && resToken.Left(3).CompareNoCase("SAT") == 0) {
				//Ankit ----- check for if the parameter passed to start SATFramework
				m_startSATFramework = true;
			}
		}
	}

	//
	// Some of these flags could be implied, e.g. by looking at the size of the URL,
	// but I want to make it explicit in most cases, which should be safer when dealing
	// with mall-formed input.
	//
	CStringA m_distributionConfig;
	CStringA m_wokFile;
	CStringA m_origPassword;
	bool m_createAvatar;
	CStringA m_patchUrl;
	CStringA m_ip;
	CStringA m_port;
	int m_playerServerId;
	bool m_commandline_override_ip_port;
	bool m_gotoplayeronstartup;
	bool m_gotoapartmentonstartup;
	bool m_gotochannelonstartup;
	bool m_gotozoneonstartup;
	bool m_gotourlonstartup;
	bool m_tryon;
	int m_tryonGlid;

	USE_TYPE m_tryonUseType;

	long m_zoneIndex;
	long m_instanceId;
	CStringA m_url;
	CStringA m_userName;
	bool m_autoLogon;
	bool m_autoCreateStar;
	HWND m_hwndEventListener;
	CStringA m_appName;
	CStringA m_templateName;
	CStringA m_installDirectory;
	bool m_clientRunning;

	//Ankit ----- bool to check whether SATFramework is to be started, value supplied from cmdLine
	bool m_startSATFramework;
};
