/******************************************************************************
 PublishUpdateDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// DistributionUpdateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PublishUpdateDlg.h"
#include "Publish.h"

extern UINT gPubUpdateMsgId;

CStringA CPublishUpdateDlg::m_user;
CStringA CPublishUpdateDlg::m_pass;
CStringA CPublishUpdateDlg::m_pubFolder;
CStringA CPublishUpdateDlg::m_gameId;
HANDLE CPublishUpdateDlg::m_hEvPublishHasClosed;
HANDLE CPublishUpdateDlg::m_hEvDialogClosing;

BEGIN_MESSAGE_MAP(CPublishUpdateDlg, CDialog)
ON_REGISTERED_MESSAGE(gPubUpdateMsgId, OnStatusMessage)
END_MESSAGE_MAP()

// CPublishUpdateDlg dialog

IMPLEMENT_DYNAMIC(CPublishUpdateDlg, CDialog)
CPublishUpdateDlg::CPublishUpdateDlg(CWnd *pParent, CStringA user, CStringA pass, CStringA pubFolder, CStringA gameId, Logger &logger) :
		CDialog(CPublishUpdateDlg::IDD, pParent), m_logger(logger) {
	m_user = user;
	m_pass = pass;
	m_pubFolder = pubFolder;
	m_gameId = gameId;
}

CPublishUpdateDlg::~CPublishUpdateDlg() {
	if (m_hEvDialogClosing) {
		CloseHandle(m_hEvDialogClosing);
	}

	if (m_hEvPublishHasClosed) {
		CloseHandle(m_hEvPublishHasClosed);
	}
}

void CPublishUpdateDlg::DoDataExchange(CDataExchange *pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDCANCEL, m_cancelButton);
	DDX_Control(pDX, IDOK, m_okButton);
}

UINT CPublishUpdateDlg::showIt(LPVOID pParam) {
	Publish::instance()->Transfer(((CPublishUpdateDlg *)pParam)->m_hWnd, m_user, m_pass, m_pubFolder, m_gameId, m_hEvDialogClosing, m_hEvPublishHasClosed, ((CPublishUpdateDlg *)pParam)->m_logger);
	return 0;
}

BOOL CPublishUpdateDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	m_cancelButton.EnableWindow(TRUE);
	m_okButton.EnableWindow(FALSE);

	// Two events to handle canceling

	// signaled when user cancels via the dialog button
	m_hEvDialogClosing = CreateEvent(NULL, TRUE, FALSE, NULL);
	// signaled when distribution received a request to close and it has processed this message
	m_hEvPublishHasClosed = CreateEvent(NULL, TRUE, FALSE, NULL);

	VERIFY(m_hEvDialogClosing != NULL && m_hEvPublishHasClosed != NULL);

	AfxBeginThread(showIt, (LPVOID)this, THREAD_PRIORITY_NORMAL);
	return true;
}

afx_msg LRESULT CPublishUpdateDlg::OnStatusMessage(WPARAM wp, LPARAM lp) {
	CStringW title;
	CStringW bytesSent;
	CStringW bytesToSend;
	CStringW totalbytes;
	CStringW totalsent;
	CStringW byteStr;

	STATUSUPDATEPUB *stat;

	switch (wp) {
		case MSG_STATUS:
			stat = (STATUSUPDATEPUB *)lp;
			title.Format(IDS_PUBLISHING_FILE_TITLE, stat->numFilesXferCompleted, stat->numFilesToXfer);
			SetDlgItemText(IDC_PUBLISH_WAIT_TITLE, title);
			SetDlgItemText(IDC_PUBLISHING_WAIT_FILENAME, Utf8ToUtf16(stat->destPath).c_str());

			bytesToSend.LoadString(IDS_PUBLISHING_BYTES_TOSEND);

			if (stat->lastByteSent != -1) {
				if (stat->lastByteSent > 1048576) {
					byteStr.Format(L"%01.1f M ", stat->lastByteSent / 1048576.0);
					bytesToSend += byteStr;
				} else if (stat->lastByteSent > 1024) {
					byteStr.Format(L"%01.1f K ", stat->lastByteSent / 1024.0);
					bytesToSend += byteStr;
				} else {
					byteStr.Format(L"%ld bytes ", stat->lastByteSent);
					bytesToSend += byteStr;
				}
			}

			if (stat->lastByteToSend > 1048576) {
				byteStr.Format(L"(%01.1f M)", stat->lastByteToSend / 1048576.0);
				bytesToSend += byteStr;
			} else if (stat->lastByteToSend > 1024) {
				byteStr.Format(L"(%01.1f K)", stat->lastByteToSend / 1024.0);
				bytesToSend += byteStr;
			} else {
				byteStr.Format(L"(%ld bytes)", stat->lastByteToSend);
				bytesToSend += byteStr;
			}

			if (strlen(stat->msg) != 0) {
				bytesToSend += L" ";
				bytesToSend += Utf8ToUtf16(stat->msg).c_str();
			}

			SetDlgItemText(IDC_PUBLISHING_WAIT_LASTBYTETOSEND, bytesToSend);

			totalsent.LoadString(IDS_PUBLISHING_BYTES_TOTAL_SENT);
			totalbytes.LoadString(IDS_PUBLISHING_BYTES_TOTAL_TOSEND);

			if (stat->totalByteToSend > 1048576) {
				byteStr.Format(L"%01.1f M", stat->totalByteSent / 1048576.0);
				totalsent += byteStr;
				byteStr.Format(L"%01.1f M", stat->totalByteToSend / 1048576.0);
				totalbytes += byteStr;
			} else if (stat->totalByteToSend > 1024) {
				byteStr.Format(L"%01.1f K", stat->totalByteSent / 1024.0);
				totalsent += byteStr;
				byteStr.Format(L"%01.1f K", stat->totalByteToSend / 1024.0);
				totalbytes += byteStr;
			} else {
				byteStr.Format(L"%ld bytes", stat->totalByteSent);
				totalsent += byteStr;
				byteStr.Format(L"%ld bytes", stat->totalByteToSend);
				totalbytes += byteStr;
			}

			SetDlgItemText(IDC_PUBLISHING_WAIT_TOTALBYTES, totalsent);
			SetDlgItemText(IDC_PUBLISHING_WAIT_TOTALBYTESTOSEND, totalbytes);

			break;
		case MSG_ERROR:
			CDialog::EndDialog(0);
			break;
		case MSG_COMPLETE:
			m_cancelButton.EnableWindow(FALSE);
			m_cancelButton.ShowWindow(SW_HIDE);
			m_okButton.EnableWindow(TRUE);
			title.LoadString(IDS_PUBLISHING_FILE_TITLE_COMPLETE);
			SetDlgItemText(IDC_PUBLISH_WAIT_TITLE, title);
			SetDlgItemText(IDC_PUBLISHING_WAIT_FILENAME, L"");
			//missingres		SetDlgItemText(IDC_PUBLISHING_WAIT_LASTBYTESENT, "");
			SetDlgItemText(IDC_PUBLISHING_WAIT_LASTBYTETOSEND, L"");
			SetDlgItemText(IDC_PUBLISHING_WAIT_TOTALBYTES, L"");
			SetDlgItemText(IDC_PUBLISHING_WAIT_TOTALBYTESTOSEND, L"");
			break;
		default:
			break;
	}

	return 0;
}

void CPublishUpdateDlg::OnCancel() {
	UpdateData();

	SetEvent(m_hEvDialogClosing);

	// Wait till the Publish has a chance to recognize the
	// close request and acknowledge it.
	MSG pMsg;
	while (WaitForSingleObject(m_hEvPublishHasClosed, 0) != WAIT_OBJECT_0) {
		while (PeekMessage(&pMsg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&pMsg);
			DispatchMessage(&pMsg);
		}
	}

	CDialog::OnCancel();
}

// PublishUpdateDlg message handlers
