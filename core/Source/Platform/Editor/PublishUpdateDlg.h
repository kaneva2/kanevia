/******************************************************************************
 PublishUpdateDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxwin.h"
#include "PublishUpdateGlobals.h"

// PublishUpdateDlg dialog

class CPublishUpdateDlg : public CDialog {
	DECLARE_DYNAMIC(CPublishUpdateDlg)

public:
	CPublishUpdateDlg(CWnd* pParent, CStringA user, CStringA pass, CStringA pubFolder, CStringA gameId, Logger& logger);
	virtual ~CPublishUpdateDlg();

	// Dialog Data
	enum { IDD = IDD_DIALOG_PUBLISH_UPDATE };

	virtual BOOL OnInitDialog();

private:
	static CStringA m_user;
	static CStringA m_pass;
	static CStringA m_pubFolder;
	static CStringA m_gameId;
	static HANDLE m_hEvDialogClosing;
	static HANDLE m_hEvPublishHasClosed;

	CButton m_cancelButton;
	CButton m_okButton;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	afx_msg LRESULT OnStatusMessage(WPARAM wp, LPARAM lp);
	static UINT showIt(LPVOID pParam);
	virtual void OnCancel();

	Logger m_logger;
	DECLARE_MESSAGE_MAP()
};
