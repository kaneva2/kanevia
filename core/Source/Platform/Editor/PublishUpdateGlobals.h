/******************************************************************************
 PublishUpdateGlobals.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#define MSG_STATUS WM_USER + 1
#define MSG_COMPLETE WM_USER + 2
#define MSG_ERROR WM_USER + 3

typedef struct _STATUSUPDATEPUB {
	ULONG numFilesToXfer;
	ULONG numFilesXferCompleted;
	char fileName[255];
	char destPath[255];
	char msg[255];
	ULONG lastByteSent;
	ULONG lastByteToSend;
	ULONG totalByteSent;
	ULONG totalByteToSend;
} STATUSUPDATEPUB;
