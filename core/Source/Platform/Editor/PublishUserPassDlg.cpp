/******************************************************************************
 PublishUserPassDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// PublishUserPassDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PublishUserPassDlg.h"
#include "UnicodeMFCHelpers.h"

// CPublishUserPassDlg dialog

IMPLEMENT_DYNAMIC(CPublishUserPassDlg, CDialog)
CPublishUserPassDlg::CPublishUserPassDlg(CWnd* pParent /*=NULL*/) :
		CDialog(CPublishUserPassDlg::IDD, pParent) {
}

CPublishUserPassDlg::~CPublishUserPassDlg() {
}

void CPublishUserPassDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PUBLISH_USER_INPUT, user);
	DDX_Text(pDX, IDC_PUBLISH_PASS_INPUT, pass);
}

BOOL CPublishUserPassDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	GetDlgItem(IDC_PUBLISH_USER_INPUT)->SetFocus();

	return false;
}

BEGIN_MESSAGE_MAP(CPublishUserPassDlg, CDialog)
END_MESSAGE_MAP()

// PublishUserPassDlg message handlers
