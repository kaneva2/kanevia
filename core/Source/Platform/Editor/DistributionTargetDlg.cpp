/******************************************************************************
 DistributionTargetDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// DistributionTargetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DistributionUpdateGlobals.h"
#include "DistributionTargetDlg.h"
#include "KEPFileNames.h"
#include <sys/stat.h>
#include <direct.h>

// CDistributionTargetDlg dialog

IMPLEMENT_DYNAMIC(CDistributionTargetDlg, CDialog)
CDistributionTargetDlg::CDistributionTargetDlg(CWnd* pParent /*=NULL*/, bool _showDbExport /*= false*/) :
		CDialog(CDistributionTargetDlg::IDD, pParent), pubSelection(COMPLETE_DISTRO), canDbExport(_showDbExport), exportToDb(dbe_dontExportToDb), tristripAssets(TRUE) // always true now, used to be selection
		,
		encryptAssets(TRUE) // always true now, used to be selection
		,
		disableDialogs(FALSE) {
	m_oldCRCList = NULL;
	m_oldFileCount = 0;
}

CDistributionTargetDlg::~CDistributionTargetDlg() {
	delete[] m_oldCRCList;
}

void CDistributionTargetDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_CBIndex(pDX, IDC_COMBO_DISTRO, pubSelection);

	if (pDX->m_bSaveAndValidate) {
		AfxGetApp()->WriteProfileInt(L"Distribution", L"LastPublish", pubSelection);

		if (pubSelection == (int)DB_ONLY) {
			pubSelection = (int)COMPLETE_DISTRO;
			exportToDb = dbe_onlyExportToDb;
		} else if (pubSelection == (int)ASSET_ONLY) {
			pubSelection = (int)COMPLETE_DISTRO;
			exportToDb = dbe_dontExportToDb;
		} else // COMPLETE
		{
			exportToDb = dbe_exportToDb;
		}
	}
}

BOOL CDistributionTargetDlg::OnInitDialog() {
	if (canDbExport)
		exportToDb = dbe_exportToDb;
	else
		exportToDb = dbe_dontExportToDb;

	pubSelection = AfxGetApp()->GetProfileInt(L"Distribution", L"LastPublish", 0);

	CDialog::OnInitDialog();
	OnCbnSelchangeComboDistro();

	LoadVersionInfo();

	return true;
}

void CDistributionTargetDlg::OnOK() {
	CDialog::OnOK();
}

void CDistributionTargetDlg::LoadVersionInfo() {
	// load default versioninfo.dat
	char buffer[_MAX_PATH];
	_getcwd(buffer, _MAX_PATH);

	struct _stat stat;
	std::string fileName = PathAdd(buffer, VERSIONINFO_DAT).c_str();
	CStringW fileNameW(Utf8ToUtf16(fileName).c_str());
	if (_wstat(fileNameW, &stat) == 0) {
		ProcessVersionInfo(fileName.c_str());
	} else {
		if (m_oldCRCList) {
			m_oldCRCList = NULL;
		}
	}
}

bool CDistributionTargetDlg::ProcessVersionInfo(const CStringA& fileName) {
	int nTotalFiles = GetPrivateProfileIntW(L"CURRENT", L"TOTALFILES", 0, Utf8ToUtf16(fileName).c_str());
	m_oldFileCount = nTotalFiles;

	if (nTotalFiles > 0) {
		delete m_oldCRCList; // just in case.
		m_oldCRCList = new CRCLIST[nTotalFiles];
		if (ReadCRCTable2(nTotalFiles, m_oldCRCList, fileName)) {
			return true;
		} else {
			delete m_oldCRCList;
			m_oldCRCList = NULL;
			m_oldFileCount = 0;
		}
	}
	return false;
}

// Copy pasted from patcher
// Note Event/CppEventHandler/utils.cpp::ReadCRCTable2 is obsolete and should be updated i guess...
// crc table reading into a struct -- Jonny 3/31/07
bool CDistributionTargetDlg::ReadCRCTable2(int count, CRCLIST* cList, const char* szServerInfoTemp) {
	char szFormattedSection[] = "[FILES]";
	int i, listcount = 0;
	char line[2048];
	BOOL bInSection = FALSE;
	char *filename, *md5, *directory, *serverfile, *bregister, *filesize, *brequired, *command;

	FILE* data = 0;
	_wfopen_s(&data, Utf8ToUtf16(szServerInfoTemp).c_str(), L"r");
	if (data) {
		while (fgets(&line[0], 500, data)) {
			if (bInSection) {
				if (strlen(line) > 0) {
					CStringA csLine = line;
					csLine.Replace(",,", ", ,");
					strcpy_s(line, _countof(line), csLine);
					char* tokTemp;
					filename = strtok_s(line, "=", &tokTemp);
					md5 = strtok_s(NULL, ",", &tokTemp);
					directory = strtok_s(NULL, ",", &tokTemp);
					serverfile = strtok_s(NULL, ",", &tokTemp);
					bregister = strtok_s(NULL, ",", &tokTemp);
					filesize = strtok_s(NULL, ",", &tokTemp);
					brequired = strtok_s(NULL, ",", &tokTemp);
					command = strtok_s(NULL, "\n", &tokTemp);

					CStringA csDirectory = directory;
					csDirectory.TrimRight();

					if (md5) {
						cList[listcount].filename = filename;
						cList[listcount].directory = csDirectory;
						i = strlen(serverfile);
						do {
							if (i > 0) {
								i--;
								if (serverfile[i] == '\\') {
									serverfile = serverfile + i + 1;
									break;
								}
							}
						} while (i);
						cList[listcount].serverfile = serverfile;
						cList[listcount].crc = md5;
						if (strncmp(bregister, "N", 1) == 0)
							cList[listcount].bRegister = FALSE;
						else
							cList[listcount].bRegister = TRUE;

						if (filesize)
							cList[listcount].nFilesize = atol(filesize);
						if (brequired) {
							if (strncmp(brequired, "N", 1) == 0)
								cList[listcount].bRequired = FALSE;
							else
								cList[listcount].bRequired = TRUE;
						} else {
							cList[listcount].bRequired = TRUE; // old versioninfo format assume asset required.
						}
						if (command) {
							cList[listcount].command = command;
						} else {
							cList[listcount].command = "None";
						}
					}

					cList[listcount].bUpToDate = FALSE;

					//Total files
					listcount++;
					if (listcount > count) {
						ASSERT(FALSE);
						fclose(data);
						return false;
					}
				}
				memset(&line[0], 0, 255);
			}

			if (line[0] == '[' && strncmp(line, szFormattedSection, strlen(szFormattedSection)) == 0)
				bInSection = TRUE;
			else if (line[0] == '[')
				bInSection = FALSE;
		}
	}
	fclose(data);
	return TRUE;
}

BEGIN_MESSAGE_MAP(CDistributionTargetDlg, CDialog)
ON_CBN_SELCHANGE(IDC_COMBO_DISTRO, &CDistributionTargetDlg::OnCbnSelchangeComboDistro)
END_MESSAGE_MAP()

// DistributionTargetDlg message handlers

void CDistributionTargetDlg::OnCbnSelchangeComboDistro() {
	CStringW s;
	s.LoadString(IDS_DISTRO_DESC + ((CComboBox*)GetDlgItem(IDC_COMBO_DISTRO))->GetCurSel());
	SetDlgItemText(IDC_STATIC_DESC, s);
}
