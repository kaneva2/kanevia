/******************************************************************************
 DistributionGameSelectionDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// DistributionGameSelectionDlg : implementation file
//

#include "stdafx.h"
#include "KEPEdit.h"
#include "DistributionGameSelectionDlg.h"
#include <tinyxml.h>
#include ".\distributiongameselectiondlg.h"

// CDistributionGameSelectionDlg dialog

IMPLEMENT_DYNAMIC(CDistributionGameSelectionDlg, CDialog)
CDistributionGameSelectionDlg::CDistributionGameSelectionDlg(CWnd* pParent, CStringA accountXML) :
		CDialog(CDistributionGameSelectionDlg::IDD, pParent) {
	m_accountXML = accountXML;
	m_sel_encryptKey.Empty();
	m_sel_comLicense.Empty();
	m_sel_gameId.Empty();
}

CDistributionGameSelectionDlg::CDistributionGameSelectionDlg(CStringA accountXML, CStringA gameId) {
	m_accountXML = accountXML;
	m_sel_gameId = gameId;
	m_sel_encryptKey.Empty();
	m_sel_comLicense.Empty();
}

bool CDistributionGameSelectionDlg::parseAccountInfo(const char* overrideXML) {
	bool ret = false;
	if (overrideXML)
		ret = ::ParseAccountInfo(overrideXML, m_games_data);
	else
		ret = ::ParseAccountInfo(m_accountXML, m_games_data);

	if (ret) {
		if (m_games_data.IsEmpty()) {
			CStringW errMsg;
			errMsg.Format(IDS_PUBLISH_NO_GAMES_FOUND);
			if (AfxMessageBox(errMsg, MB_YESNO | MB_ICONEXCLAMATION) == IDYES) {
				CStringA pubHelp;
				pubHelp = "file://";
				pubHelp += ((CKEPEditApp*)AfxGetApp())->GetView()->EditorBaseDir().c_str();
				pubHelp += "help\\DistributionHelp.html";

				ShellExecute(NULL, L"open", Utf8ToUtf16(pubHelp).c_str(), NULL, NULL, SW_SHOW);
			}
			ret = false;
		} else if (!m_sel_gameId.IsEmpty()) {
			for (POSITION posLoc = m_games_data.GetHeadPosition(); posLoc != NULL;) {
				CGameData* ptr = (CGameData*)m_games_data.GetNext(posLoc);
				if (ptr->GetId() == m_sel_gameId) {
					m_sel_encryptKey = ptr->GetEncryptionKey();
					m_sel_distributionLicense = ptr->GetServerKey();
					m_sel_parentId = ptr->GetParentId();
				}
			}
		}
	}

	return ret;
}

bool ParseAccountInfo(const char* accountXML, CObList& games_data) {
	/* Sample XML
	<kaneva>
		<result>0</result>
		<ticket>
			9DADF3C221664D0D9A80F2CCDFF3CDFB4E9A256D4168D087B38EEA000B79DFC83FA0F6454142A421594610E715F4F3AB64DCB4D96C9366DD2ABE5A8FBC8B35665DF89EF93F9967C35439BBE6DEE2339D4AA5FE9978C049D5507940B6A9A92340AE37D74705A09919E1D28D70F79E7C1AB84332A8542A67A64E96B77DF1C90EE72CBA74FF069BB20F909E4BC7752B89F4
		</ticket>
		<games>
			<game name="Cardboard" id="226" encryptionKey="8748375839475934" parentId="0" serverKey="36U14962Kk2pNPrpMD8Qg6L4" licType="2"/>
			<game name="World of Kaneva" id="3296" encryptionKey="rwk3gH2nXok7QqLk" parentId="0" serverKey="36U7663KaMgfmaCj8ftkyJN" licType="3"/>
			<game name="World of Kaneva-Jim" id="5281" encryptionKey="NafeyK2dX2jmf2be" parentId="3296" serverKey="10238U5281Ge7FbQprNyCkSg2Q" licType="3"/>
		</games>
	</kaneva>
	*/

	static const char* ROOT_TAG = "kaneva";
	static const char* RESULT_TAG = "result";
	static const char* TICKET_TAG = "ticket";
	static const char* GAMES_TAG = "games";
	static const char* GAME_TAG = "game";
	static const char* GAME_ATTRIBUTE_NAME = "name";
	static const char* GAME_ATTRIBUTE_ID = "id";
	static const char* GAME_ATTRIBUTE_ENCRYPTION_KEY = "encryptionKey";
	static const char* GAME_ATTRIBUTE_SERVER_KEY = "serverKey";
	static const char* GAME_ATTRIBUTE_PARENT_ID = "parentId";

	bool ret = true;

	TiXmlElement* root;
	TiXmlDocument dom;

	dom.Parse(accountXML);

	if (!dom.Error()) {
		root = dom.FirstChildElement(ROOT_TAG);

		CStringA result;
		CStringA ticket;

		if (root) {
			TiXmlText* result_entry = TiXmlHandle(root->FirstChild(RESULT_TAG)).FirstChild().Text();
			TiXmlText* ticket_entry = TiXmlHandle(root->FirstChild(TICKET_TAG)).FirstChild().Text();
			result = (result_entry) ? NULL_CK(result_entry->Value()) : "";
			ticket = (ticket_entry) ? NULL_CK(ticket_entry->Value()) : "";

			TiXmlElement* games = root->FirstChildElement(GAMES_TAG);

			TiXmlNode* node;
			TiXmlElement* child = 0;

			FreeGameData(games_data); // clear out any old entries

			if (games) {
				while ((node = games->IterateChildren(GAME_TAG, child)) != 0 && (child = node->ToElement()) != 0) {
					CGameData* gameDat = new CGameData();

					CStringA game_name = NULL_CK(child->Attribute(GAME_ATTRIBUTE_NAME));
					gameDat->SetName(game_name);

					CStringA game_id = NULL_CK(child->Attribute(GAME_ATTRIBUTE_ID));
					gameDat->SetId(game_id);

					CStringA parent_game_id = NULL_CK(child->Attribute(GAME_ATTRIBUTE_PARENT_ID));
					gameDat->SetParentId(parent_game_id);

					CStringA game_encryption_key = NULL_CK(child->Attribute(GAME_ATTRIBUTE_ENCRYPTION_KEY));
					gameDat->SetEncryptionKey(game_encryption_key);

					CStringA game_server_key = NULL_CK(child->Attribute(GAME_ATTRIBUTE_SERVER_KEY));
					gameDat->SetServerKey(game_server_key);

					games_data.AddTail(gameDat);
				}
			}
		}
	} else {
		CStringW errMsg;
		errMsg.Format(IDS_PUBDIST_ACCOUNT_INFO_PARSE_ERROR, Utf8ToUtf16(dom.ErrorDesc()).c_str(), dom.ErrorId());
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
		ret = false;
	}

	return ret;
}

void FreeGameData(CObList& games_data) {
	for (POSITION posLoc = games_data.GetHeadPosition(); posLoc != NULL;) {
		CGameData* ptr = (CGameData*)games_data.GetNext(posLoc);
		delete ptr;
		ptr = 0;
	}
	games_data.RemoveAll();
}

void CDistributionGameSelectionDlg::FreeGameData() {
	::FreeGameData(m_games_data);
}

CDistributionGameSelectionDlg::~CDistributionGameSelectionDlg() {
	FreeGameData();
}

void CDistributionGameSelectionDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GAME_LIST, m_gameList);
}

BEGIN_MESSAGE_MAP(CDistributionGameSelectionDlg, CDialog)
ON_NOTIFY(NM_DBLCLK, IDC_GAME_LIST, OnNMDblclkGameList)
END_MESSAGE_MAP()

BOOL CDistributionGameSelectionDlg::OnInitDialog() {
	return true;
}
// DistributionGameSelectionDlg message handlers

void CDistributionGameSelectionDlg::OnOK() {
}

void CDistributionGameSelectionDlg::OnNMDblclkGameList(NMHDR* pNMHDR, LRESULT* pResult) {
	// TODO: Add your control notification handler code here
	*pResult = 0;
	OnOK();
}

/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx

BOOL CListCtrlEx::AddColumn(const char* strItem, int nItem, int nSubItem, int nMask, int nFmt) {
	LV_COLUMN lvc;
	lvc.mask = nMask;
	lvc.fmt = nFmt;
	lvc.pszText = const_cast<wchar_t*>(Utf8ToUtf16(strItem).c_str());
	lvc.cx = GetStringWidth(lvc.pszText) + 15;
	if (nMask & LVCF_SUBITEM) {
		if (nSubItem != -1)
			lvc.iSubItem = nSubItem;
		else
			lvc.iSubItem = nItem;
	}
	return InsertColumn(nItem, &lvc);
}

int CListCtrlEx::AddItem(int nItem, int nSubItem, const char* strItem, int nImageIndex) {
	LV_ITEM lvItem;
	lvItem.mask = LVIF_TEXT;
	lvItem.iItem = nItem;
	lvItem.iSubItem = nSubItem;
	lvItem.pszText = const_cast<wchar_t*>(Utf8ToUtf16(strItem).c_str());
	lvItem.iImage = 0;
	if (nImageIndex != -1) {
		lvItem.mask |= LVIF_IMAGE;
		lvItem.iImage |= LVIF_IMAGE;
	}

	if (nSubItem == 0)
		return InsertItem(&lvItem);
	return SetItemText(nItem, nSubItem, Utf8ToUtf16(strItem).c_str());
}

/////////////////////////////////////////////////////////////////////////////
