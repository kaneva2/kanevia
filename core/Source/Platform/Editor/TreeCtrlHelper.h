/******************************************************************************
TreeCtrlHelper.h

Copyright (c) 2004-2008 Kaneva, Inc. All Rights Reserved Worldwide
Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace TreeCtrlHelper {
//! ********************************************************
//! \brief Adds a item as a root object
//! \param name Name of the new tree item.
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM AddTreeRoot(CStringW name, UINT id, CWnd* pParentWnd, int iconIndex = -1, LPARAM param = NULL, BOOL bSelect = FALSE);

//! ********************************************************
//! \brief Adds a item as a root object
//! \param name Name of the new tree item.
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM AddTreeRoot(CStringW name, HWND hWndTreeCtrl, int iconIndex = -1, LPARAM param = NULL, BOOL bSelect = FALSE);

//! ********************************************************
//! \brief Adds a child item to a tree control
//! \param name Name of the new tree item.
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem The parent item where the new item is about to be added under
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM AddTreeChild(CStringW name, UINT id, CWnd* pParentWnd, HTREEITEM treeItem, int iconIndex = -1, LPARAM param = NULL, BOOL bSelect = FALSE);

//! ********************************************************
//! \brief Adds a child item to a tree control
//! \param name Name of the new tree item.
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param treeItem The parent item where the new item is about to be added under
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM AddTreeChild(CStringW name, HWND hWndTreeCtrl, HTREEITEM treeItem, int iconIndex = -1, LPARAM param = NULL, BOOL bSelect = FALSE);

//! ********************************************************
//! \brief Delete a node from a tree control
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem The parent item where the new item is about to be added under
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL DelTreeNode(UINT id, CWnd* pParentWnd, HTREEITEM treeItem);

//! ********************************************************
//! \brief Delete a node from a tree control
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param treeItem The parent item where the new item is about to be added under
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL DelTreeNode(HWND hWndTreeCtrl, HTREEITEM treeItem);

//! ********************************************************
//! \brief Delete all nodes from a tree control
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL DelAllTreeNodes(UINT id, CWnd* pParentWnd);

//! ********************************************************
//! \brief Delete all nodes from a tree control
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL DelAllTreeNodes(HWND hWndTreeCtrl);

//! ********************************************************
//! \brief Retrieve item text from a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item where text is to be retrieved.
//! \return item text
CStringW GetTreeItemText(UINT id, CWnd* pParentWnd, HTREEITEM treeItem);

//! ********************************************************
//! \brief Retrieve item text from a tree item
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item where text is to be retrieved.
//! \return item text
CStringW GetTreeItemText(HWND hWndTreeCtrl, HTREEITEM treeItem);

//! ********************************************************
//! \brief Set item text for a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item
//! \param newText The new item text to be stored.
//! \return TRUE if succeeded. FALSE otherwise
BOOL SetTreeItemText(UINT id, CWnd* pParentWnd, HTREEITEM treeItem, CStringW newText);

//! ********************************************************
//! \brief Set item text for a tree item
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item
//! \param newText The new item text to be stored.
//! \return TRUE if succeeded. FALSE otherwise
BOOL SetTreeItemText(HWND hWndTreeCtrl, HTREEITEM treeItem, CStringW newText);

//! ********************************************************
//! \brief Retrieve data associated with a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item where the data is stored
//! \return data (NULL if not set or not found).
LPARAM GetTreeItemData(UINT id, CWnd* pParentWnd, HTREEITEM treeItem);

//! ********************************************************
//! \brief Retrieve data associated with a tree item
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item where the data is stored
//! \return data (NULL if not set or not found).
LPARAM GetTreeItemData(HWND hWndTreeCtrl, HTREEITEM treeItem);

//! ********************************************************
//! \brief Associate data with a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item to which the data will be associated.
//! \param data Data to be associated to the tree item.
//! \return TRUE if successed. FALSE otherwise.
BOOL SetTreeItemData(UINT id, CWnd* pParentWnd, HTREEITEM treeItem, LPARAM data);

//! ********************************************************
//! \brief Associate data with a tree item
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item to which the data will be associated.
//! \param data Data to be associated to the tree item.
//! \return TRUE if successed. FALSE otherwise.
BOOL SetTreeItemData(HWND hWndTreeCtrl, HTREEITEM treeItem, LPARAM data);

//! ********************************************************
//! \brief Obtain the index of specified child item relative to its parent.
int GetTreeChildIndex(HWND hWndTreeCtrl, HTREEITEM item);

//! ********************************************************
//! \brief Obtain the index of specified child item relative to its parent.
int GetTreeChildIndex(UINT id, CWnd* pParentWnd, HTREEITEM item);

//! ********************************************************
//! \brief Obtain the depth of specified child item from root
int GetTreeChildDepth(HWND hWndTreeCtrl, HTREEITEM item);

//! ********************************************************
//! \brief Obtain the depth of specified child item from root
int GetTreeChildDepth(UINT id, CWnd* pParentWnd, HTREEITEM item);

//! ********************************************************
//! \brief Obtain selected tree item
HTREEITEM GetSelectedItem(UINT id, CWnd* pParentWnd);

//! ********************************************************
//! \brief Obtain selected tree item
HTREEITEM GetSelectedItem(HWND hWndTreeCtrl);

//! ********************************************************
//! \brief Set tree image list.
//! \return Previous image list if succeeded. NULL otherwise.
HIMAGELIST SetTreeImageList(UINT id, CWnd* pParentWnd, HIMAGELIST hImageList, int Type = TVSIL_NORMAL);

//! ********************************************************
//! \brief Set tree image list.
//! \return Previous image list if succeeded. NULL otherwise.
HIMAGELIST SetTreeImageList(HWND hWndTreeCtrl, HIMAGELIST hImageList, int Type = TVSIL_NORMAL);
}; // namespace TreeCtrlHelper
