/******************************************************************************
 PublishUserPassDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxwin.h"

// PublishUserPassDlg dialog

class CPublishUserPassDlg : public CDialog {
	DECLARE_DYNAMIC(CPublishUserPassDlg)

public:
	CPublishUserPassDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CPublishUserPassDlg();

	CStringA GetUser() { return user; }
	CStringA GetPass() { return pass; }

	BOOL OnInitDialog();

	// Dialog Data
	enum { IDD = IDD_DIALOG_PUBLISHING_USER_PASS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CStringA user;
	CStringA pass;
};
