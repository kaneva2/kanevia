/******************************************************************************
 DistributionGameSelectionDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxwin.h"
#include "afxcmn.h"

// DistributionGameSelectionDlg dialog

class CGameData : public CObject {
public:
	CStringA GetName() { return m_name; }
	void SetName(CStringA name) { m_name = name; }

	CStringA GetId() { return m_id; }
	void SetId(CStringA id) { m_id = id; }

	CStringA GetParentId() { return m_parentId; }
	void SetParentId(CStringA id) { m_parentId = id; }

	CStringA GetEncryptionKey() { return m_encryptionKey; }
	void SetEncryptionKey(CStringA encryptionKey) { m_encryptionKey = encryptionKey; }

	CStringA GetServerKey() { return m_serverKey; }
	void SetServerKey(CStringA ServerKey) { m_serverKey = ServerKey; }

	CStringA GetConsumerKey() { return m_consumerKey; }
	void SetConsumerKey(CStringA ConsumerKey) { m_consumerKey = ConsumerKey; }

	CStringA GetConsumerSecret() { return m_consumerSecret; }
	void SetConsumerSecret(CStringA ConsumerSecret) { m_consumerSecret = ConsumerSecret; }

private:
	CStringA m_name;
	CStringA m_id;
	CStringA m_encryptionKey;
	CStringA m_serverKey;
	CStringA m_parentId;
	CStringA m_consumerKey;
	CStringA m_consumerSecret;
};

// helper fns
bool ParseAccountInfo(IN const char* accountXML, INOUT CObList& games_data);
void FreeGameData(CObList& games_data);

class CListCtrlEx : public CListCtrl {
	// Attributes
protected:
	// Operation
public:
	CListCtrlEx() :
			CListCtrl() {}
	~CListCtrlEx() {}
	CImageList* SetImageList(CImageList* pImageList, int nImageListType = TVSIL_NORMAL) { return CListCtrl::SetImageList(pImageList, nImageListType); }

	BOOL AddColumn(
		const char* strItem, int nItem, int nSubItem = -1,
		int nMask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM,
		int nFmt = LVCFMT_LEFT);
	int AddItem(int nItem, int nSubItem, const char* strItem, int nImageIndex = -1);
};

class CDistributionGameSelectionDlg : public CDialog {
	DECLARE_DYNAMIC(CDistributionGameSelectionDlg)

public:
	CDistributionGameSelectionDlg(CWnd* pParent, CStringA accountXML);
	CDistributionGameSelectionDlg(CStringA accountXML, CStringA gameId);

	virtual ~CDistributionGameSelectionDlg();
	void OnOK();
	BOOL OnInitDialog();
	CStringA clientEncryptKey() { return m_sel_encryptKey; }
	CStringA gameId() { return m_sel_gameId; }
	CStringA distributionLicense() { return m_sel_distributionLicense; } // aka server license
	CStringA parentId() { return m_sel_parentId; }
	bool parseAccountInfo(const char* overrideXML = 0);

	// Dialog Data
	enum { IDD = IDD_DIALOG_PUBLISHING_GAME_SELECTION };

	const CObList& gameData() const { return m_games_data; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	void FreeGameData();

	DECLARE_MESSAGE_MAP()
private:
	CListCtrlEx m_gameList;
	CStringA m_accountXML;

	CObList m_games_data;

	CStringA m_sel_encryptKey;
	CStringA m_sel_gameId;
	CStringA m_sel_parentId;

	CStringA m_sel_distributionLicense;

	CStringA m_sel_comLicense;

public:
	afx_msg void OnNMDblclkGameList(NMHDR* pNMHDR, LRESULT* pResult);
};
