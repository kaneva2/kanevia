/******************************************************************************
 KEPEditorTab.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// KEPEditorTab.cpp : implementation file
//

#include "stdafx.h"
#include "kepeditglobals.h"
#include "GetSetStrings.h"
#include "KEPEditorTab.h"

// CKEPEditorTab
IMPLEMENT_DYNAMIC(CKEPEditorTab, CTabCtrl)
CKEPEditorTab::CKEPEditorTab() {
	m_currentTab = 0;
	m_hWndParent = 0;
}

CKEPEditorTab::~CKEPEditorTab() {
}

BEGIN_MESSAGE_MAP(CKEPEditorTab, CTabCtrl)
ON_NOTIFY_REFLECT(TCN_SELCHANGE, OnTcnSelchange)
END_MESSAGE_MAP()

// CKEPEditorTab message handlers
BOOL CKEPEditorTab::Init(HWND hWndParent) {
	CStringW title;
	m_hWndParent = hWndParent;

	m_csArray.RemoveAll();

	title.LoadString(IDS_TAB_1);
	m_csArray.Add(title);
	title.LoadString(IDS_TAB_2);
	m_csArray.Add(title);
	title.LoadString(IDS_TAB_3);
	m_csArray.Add(title);
	title.LoadString(IDS_TAB_4);
	m_csArray.Add(title);

	//Loop through the entries and build our tabs

	int i = 0;
	for (int ii = 0; ii < m_csArray.GetSize(); ii++) {
		i = InsertItem(TCIF_TEXT | TCIF_PARAM, ii, m_csArray.GetAt(ii), -1, (LPARAM)ii);
		ASSERT((i != -1));
	}

	this->SetCurSel(0);

	return TRUE;
}

void CKEPEditorTab::OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult) {
	*pResult = 0;
	TCITEM item;

	memset(&item, 0, sizeof(item));
	item.mask = TCIF_PARAM;

	if (m_currentTab == GetCurSel())
		return;

	m_currentTab = GetCurSel();

	GetItem(m_currentTab, &item);
	if (m_hWndParent)
		::SendMessage(m_hWndParent, KEP_TABCHANGE, 0, (LPARAM)item.lParam);

	*pResult = 0;
}
