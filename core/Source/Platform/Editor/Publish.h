/******************************************************************************
 Publish.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <time.h>
#include <vector>
#include <utility>

extern "C" {
// avoid benign warning due to Python/log4cplus conflict
#undef HAVE_FTIME
#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif

// Forward reference of call back functions
PyObject *ItemStatusUpdateEvent(PyObject *self, PyObject *args);
PyObject *GameStatusUpdateEvent(PyObject *self, PyObject *args);
PyObject *PythonExceptionHandler(PyObject *self, PyObject *args);
}
typedef vector<pair<string, string>> strPairVect;

class Publish {
public:
	static Publish *instance() { return &theInstance; }
	void Transfer(HWND dialogHwnd, CStringA user, CStringA pass, CStringA inputPath, CStringA gameId, HANDLE closeEvent, HANDLE pubCloseEvent, Logger &logger);
	bool GetAccountInfo(CStringA user, CStringA pass, CStringA gameDir, CStringA &accountXML, Logger &logger);
	bool Publish::InitPSM(IN CStringA gameDir);
	void Publish::DeinitPSM();
	void Publish::LockPSM();
	void Publish::UnlockPSM();

protected:
	bool Publish::Initialize(IN CStringA gameDir);
	bool Publish::DeInitialize();

	PyObject *CreateUploadItem(CStringA fileToTransfer, CStringA destinationPath);
	PyObject *Login(CStringA user, CStringA pass);
	PyObject *GetUploadItemManager();
	PyObject *GetGlobalHandler();
	PyObject *GetGameUpload(CStringA gameId);
	bool AddToUpload(PyObject *pUploadItemManager, PyObject *pUploadItem);
	bool RegisterItemStatusHandler(PyObject *pUploadItemManager);
	bool RegisterGameStatusHandler(PyObject *pUploadItemManager);
	bool RegisterExceptionHandler(PyObject *pGlobal);
	bool StartUpload(PyObject *pUploadItemManager);
	bool AddBatchToUploadManager(PyObject *pUploadItemManager, PyObject *pGameUpload);
	void _Transfer(CStringA user, CStringA pass, CStringA inputPath, CStringA gameId);

	void GetFolderFiles(INOUT strPairVect &clientFiles, CStringA baseDir, CStringA sourceDir);
	void TransferFolderFiles(PyObject *pUploadItemManager, PyObject *pGameUpload, IN strPairVect &filesToTransfer);
	bool WriteManifestFile(CStringA manifestFileName, IN strPairVect &serverFiles, IN strPairVect &patchFiles);
	bool ExcludeFile(CStringA loc);

	void InitProgressDlgParams(HWND dialogHwnd, HANDLE closeEvent, HANDLE pubCloseEvent);

	bool GetInitialized() { return m_initialized; }
	void SetInitialized(bool isInit) { m_initialized = isInit; }

	Logger &theLogger() { return *m_callbackLogger; }

	Logger *CallbackLogger() { return m_callbackLogger; }

	void SetupPythonGlobalLogger(PyObject *pGlobal);

private:
	static Publish theInstance;
	Publish() :
			m_pModuleUploadItemManager(NULL), m_pModuleSession(NULL), m_pModuleUploadItem(NULL) {}

	PyObject *m_pGlobal;
	PyObject *m_pModuleSession;
	PyObject *m_pModuleUploadItemManager;
	PyObject *m_pModuleUploadItem;

	bool m_initialized;

public:
	static PyObject *m_pUploadItemManager;
	static HWND m_progressHwnd;
	static HANDLE m_closeEvent;
	static HANDLE m_pubCloseEvent;
	static ULONG m_numFilesToXfer;
	static ULONG m_numFilesXferCompleted;
	static ULONG m_totalBytesXfer;
	static ULONG m_totalBytesToXfer;
	static bool m_XferComplete;
	static Logger *m_callbackLogger;
	static PyObject *m_pGameUpload;
	static time_t m_timeoutStart;
	static bool m_XferCanceled;
	static bool bPSM_Lock;
};
