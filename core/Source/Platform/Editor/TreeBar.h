#pragma once
// Treebar.h
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved
#pragma once

#include "KEPEditorTree.h"
#include "KEPEditorTab.h"
#include "KEPControlBar.h"

// CTreeBar

class CTreeBar : public CKEPControlBar {
public:
	CTreeBar(int minW, int minH, int prefW, int prefH);
	virtual ~CTreeBar();
	virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);
	void SetGameStatus(const BOOL gameOpen, const BOOL zoneOpen);

protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	CKEPEditorTree m_TreeList;
	CKEPEditorTab m_TabControl;
	CFont m_font;

	HWND m_hWndParent;

protected:
	//{{AFX_MSG(CTreeBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};
