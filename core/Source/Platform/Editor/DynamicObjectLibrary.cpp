#include "stdafx.h"

#include "DynamicObjectLibrary.h"

#include "StreamableDynamicObj.h"
#include "DynamicObj.h"
#include "referencelibrary.h"
#include "ReMesh.h"
#include "CMaterialClass.h"
#include "CVertexAnimationModule.h"
#include "CCollisionClass.h"
#include "CTriggerClass.h"
#include "MaterialPersistence.h"
#include "ReMeshCreate.h"
#include "IClientEngine.h"
#include "ServerSerialize.h"
#include "DynamicObjectManager.h"

#pragma pack(push, 1)

struct ExternalMeshDOStruct {
	long objectId;
	IExternalEXMesh* mesh;
};

struct DO_LIGHT_V1 {
	RGBColor color;
	float range;
	float atten1;
	float atten2;
	float atten3;
};

struct DO_LIGHT_PLACEMENT_V1 {
	int lightIndex;
	int positionIndex;
};

struct DO_POPOUT_V1 {
	float popoutMaxX;
	float popoutMaxY;
	float popoutMaxZ;
	float popoutMinX;
	float popoutMinY;
	float popoutMinZ;
};

struct DO_POSITION {
	float positionX;
	float positionY;
	float positionZ;
};

struct DO_ORIENTATION {
	float orientationX;
	float orientationY;
	float orientationZ;
};

struct DO_LOD_V1 {
	unsigned char lodCount;
	UINT64 hashes[MAX_LOD_COUNT];
	float distances[MAX_LOD_COUNT];
};

struct DO_VISUAL_V1 {
	int lodIndex;
	int nameIndex;
	int matCompressIndex;
	int popoutIndex;
	int positionIndex;
	int orientationIndex;
	int vertexAnimIndex;
};

struct DO_VISUAL_V2 {
	int lodIndex;
	int nameIndex;
	int matCompressIndex;
	int popoutIndex;
	int positionIndex;
	int orientationIndex;
	int vertexAnimIndex;
	bool canCollide;
	DO_VISUAL_V2& operator=(const DO_VISUAL_V1 a) {
		lodIndex = a.lodIndex;
		nameIndex = a.nameIndex;
		matCompressIndex = a.matCompressIndex;
		popoutIndex = a.popoutIndex;
		positionIndex = a.positionIndex;
		orientationIndex = a.orientationIndex;
		vertexAnimIndex = a.vertexAnimIndex;
		canCollide = true;
		return *this;
	}
};

struct DO_VISUAL_ASSIGNMENT_V1 {
	int visualIndex;
	bool mediaSupported;
	bool customizable;
};

// This struct for storage
struct DYNAMIC_OBJ {
	long long id;
	int nameIndex;
	int collisionIndex;
	int lightPlacementIndex;
	int particleEffectId;
	int particleOffsetIndex;
	int triggersIndex;
	bool attachable;
	bool interactive;
	std::vector<unsigned int> visualAssignments;
};

// This struct for serialization
struct DYNAMIC_OBJ_V1 {
	long long id;
	int nameIndex;
	int collisionIndex;
	int lightPlacementIndex;
	int particleEffectId;
	int particleOffsetIndex;
	int triggersIndex;
	bool attachable;
	bool interactive;
};

struct DYNAMIC_OBJ_SYSTEM_V2 {
	std::vector<CStringA> names;
	std::vector<DO_LIGHT> lights;
	std::vector<DO_LIGHT_PLACEMENT> lightPlacements;
	std::vector<DO_POPOUT> popouts;
	std::vector<DO_POSITION> positions;
	std::vector<CCollisionObj*> collisions; // is this even a word?
	std::vector<CTriggerObjectList*> triggers;
	std::vector<CVertexAnimationModule*> vertexAnims;
	std::vector<DO_ORIENTATION> orientations;
	std::vector<DO_LOD> lods;
	std::vector<DO_VISUAL> visuals;
	std::vector<DO_VISUAL_ASSIGNMENT> visualAssignments;
	std::vector<DYNAMIC_OBJ> dynamicObjects;
};

#pragma pack(pop)

////////////////////////////////////////////////////////////////////////////////////////
////							persistence layer									////
////////////////////////////////////////////////////////////////////////////////////////

// version 1 light read
void DynamicObjectLibrary::readLightv1(CArchive& ar, StreamableDynamicObject* ob) {
	bool containsLight = false;
	ar >> containsLight;
	if (!containsLight) {
		return;
	} else {
		unsigned char r, g, b;
		float att1, att2, att3, lx, ly, lz;
		StreamableDynamicObjectLight* lightfromarchive = new StreamableDynamicObjectLight();
		ar >> lx;
		ar >> ly;
		ar >> lz;
		ar >> r;
		ar >> g;
		ar >> b;
		ar >> att1;
		ar >> att2;
		ar >> att3;
		lightfromarchive->setLightColor(r, g, b);
		lightfromarchive->setAttenuation1(att1);
		lightfromarchive->setAttenuation2(att2);
		lightfromarchive->setAttenuation3(att3);
		ob->setLightPosition(lx, ly, lz);
		ob->setLight(lightfromarchive);
	}
}

void DynamicObjectLibrary::readLight(CArchive& ar, StreamableDynamicObject* ob) {
	unsigned char code;
	ar >> code;
	if (code != light_archive_code) {
		AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj light");
	} else {
		unsigned char lightversion;
		ar >> lightversion;
		if (lightversion == 1) {
			return readLightv1(ar, ob);
		} else {
			// unknown version
			AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj visual");
		}
	}
}

// version 1 visual read
StreamableDynamicObjectVisual* DynamicObjectLibrary::readVisualv1(CArchive& ar) {
	StreamableDynamicObjectVisual* vis = new StreamableDynamicObjectVisual();
	int lodCount;
	CStringA inname;
	ar >> lodCount;
	ar >> inname;
	vis->setName(inname);
	for (int i = 0; i < lodCount; i++) {
		long long hash;
		ar >> hash;
		vis->addLOD(hash);
	}
	CMaterialObject* mat;
	float px, py, pz, ox, oy, oz;
	bool vxanim;
	ar >> mat;
	if (mat) {
		vis->setMaterial(mat);
	} else {
		// This DOL was saved with an unmatching RLB just make a blank material to stop crash.
		mat = new CMaterialObject();
		vis->setMaterial(mat);
	}
	delete mat; // it's cloned in setMaterial
	ar >> px;
	ar >> py;
	ar >> pz;
	ar >> ox;
	ar >> oy;
	ar >> oz;
	vis->setRelativePosition(px, py, pz);
	vis->setRelativeOrientation(ox, oy, oz);
	float oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> vxanim;
	if (vxanim) {
		CVertexAnimationModule* inAnim;
		ar >> inAnim;
		vis->setVertexAnim(inAnim);
	} else {
		vis->setVertexAnim(NULL);
	}
	return vis;
}

// version 2 visual read
StreamableDynamicObjectVisual* DynamicObjectLibrary::readVisualv2(CArchive& ar) {
	StreamableDynamicObjectVisual* vis = new StreamableDynamicObjectVisual();
	int lodCount;
	CStringA inname;
	ar >> lodCount;
	ar >> inname;
	vis->setName(inname);
	for (int i = 0; i < lodCount; i++) {
		long long hash;
		float distance;
		ar >> hash;
		ar >> distance;
		vis->addLOD(hash, distance);
	}
	CMaterialObject* mat;
	float px, py, pz, ox, oy, oz;
	bool vxanim;
	ar >> mat;
	if (mat) {
		vis->setMaterial(mat);
	} else {
		// This DOL was saved with an unmatching RLB just make a blank material to stop crash.
		mat = new CMaterialObject();
		vis->setMaterial(mat);
	}
	delete mat; // it's cloned in setMaterial
	ar >> px;
	ar >> py;
	ar >> pz;
	ar >> ox;
	ar >> oy;
	ar >> oz;
	vis->setRelativePosition(px, py, pz);
	vis->setRelativeOrientation(ox, oy, oz);
	float oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> oldBox;
	ar >> vxanim;
	if (vxanim) {
		CVertexAnimationModule* inAnim;
		ar >> inAnim;
		vis->setVertexAnim(inAnim);
	} else {
		vis->setVertexAnim(NULL);
	}
	return vis;
}

StreamableDynamicObjectVisual* DynamicObjectLibrary::readVisual(CArchive& ar) {
	unsigned char code;
	ar >> code;
	if (code != visual_archive_code) {
		AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj visual");
	} else {
		unsigned char dovversion;
		ar >> dovversion;
		if (dovversion == 1) {
			return readVisualv1(ar);
		} else if (dovversion == 2) {
			return readVisualv2(ar);
		} else {
			// unknown version
			AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj visual");
		}
	}
}

// version 1 dyn obj read
StreamableDynamicObject* DynamicObjectLibrary::readDynamicObjectv1(CArchive& ar) {
	CStringA nm;
	long long id;
	CCollisionObj* col;
	int visCount, effectId;
	float px, py, pz;
	bool inter, att;

	ASSERT(false); // TODO: fix GLID
	StreamableDynamicObject* dynO = new StreamableDynamicObject(GLID_INVALID);
	CTriggerObjectList* trg = NULL; //Fixed memory leak: no need to allocate memory, will be done by serialization [Yanfeng 10/2008]
	ar >> id;
	ar >> nm;
	ar >> col;
	ar >> effectId;
	ar >> px;
	ar >> py;
	ar >> pz;
	ar >> trg;
	ar >> att;
	ar >> inter;
	dynO->setName(nm.GetString());
	// Legacy collision disabled.
	//dynO->setCollision(col);
	dynO->setParticleEffectId(effectId);
	dynO->setParticleOffset(px, py, pz);
	readLight(ar, dynO);
	dynO->setTriggers(trg);
	dynO->setAttachable(att);
	dynO->setInteractive(inter);
	ar >> visCount;
	for (int i = 0; i < visCount; i++) {
		bool customizable, mediaSupported;
		StreamableDynamicObjectVisual* vis = readVisual(ar);
		ar >> mediaSupported;
		ar >> customizable;
		dynO->AddVisual(vis, customizable, mediaSupported);
	}
	return dynO;
}

StreamableDynamicObject* DynamicObjectLibrary::readDynamicObject(CArchive& ar) {
	unsigned char code;
	ar >> code;
	if (code != StreamableDynamicObject::dyn_obj_archive_code) {
		AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj");
	} else {
		unsigned char doversion;
		ar >> doversion;
		if (doversion == 1) {
			return readDynamicObjectv1(ar);
		} else {
			// unknown version
			AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj");
		}
	}
}

void DynamicObjectLibrary::deCompress(DynamicObjectRM* sys, DO_SYS_STRUCT& sysStruct, const CStringA& archiveName) {
	for (unsigned int i = 0; i < sysStruct.dynamicObjects.size(); i++) {
		ASSERT(false); // TODO: fix GLID
		StreamableDynamicObject* sdo = new StreamableDynamicObject(GLID_INVALID);
		sdo->setName(sysStruct.names[sysStruct.dynamicObjects[i].nameIndex].GetString());
		sdo->setAttachable(sysStruct.dynamicObjects[i].attachable);
		sdo->setInteractive(sysStruct.dynamicObjects[i].interactive);

		if (sysStruct.dynamicObjects[i].lightPlacementIndex != -1) {
			DO_LIGHT_PLACEMENT_V1 lp = sysStruct.lightPlacements[sysStruct.dynamicObjects[i].lightPlacementIndex];
			DO_LIGHT_V1 ls = sysStruct.lights[lp.lightIndex];
			DO_POSITION pos = sysStruct.positions[lp.positionIndex];
			StreamableDynamicObjectLight* light = new StreamableDynamicObjectLight(ls.color.r, ls.color.g, ls.color.b, ls.range, ls.atten1, ls.atten2, ls.atten3);
			sdo->setLight(light);
			sdo->setLightPosition(pos.positionX, pos.positionY, pos.positionZ);
		} else {
			sdo->setLight((StreamableDynamicObjectLight*)NULL);
			sdo->setLightPosition(0.0f, 0.0f, 0.0f);
		}
		{
			DO_POSITION offset = sysStruct.positions[sysStruct.dynamicObjects[i].particleOffsetIndex];
			sdo->setParticleEffectId(sysStruct.dynamicObjects[i].particleEffectId);
			sdo->setParticleOffset(offset.positionX, offset.positionY, offset.positionZ);
		}
		if (sysStruct.dynamicObjects[i].triggersIndex != -1) {
			CTriggerObjectList* triggers;
			sysStruct.triggers[sysStruct.dynamicObjects[i].triggersIndex]->Clone(&triggers);
			sdo->setTriggers(triggers);
		} else {
			sdo->setTriggers(NULL);
		}
		for (unsigned int j = 0; j < sysStruct.dynamicObjects[i].visualAssignments.size(); j++) {
			StreamableDynamicObjectVisual* vis = new StreamableDynamicObjectVisual();
			DO_VISUAL_ASSIGNMENT va = sysStruct.visualAssignments[sysStruct.dynamicObjects[i].visualAssignments[j]];
			DO_VISUAL v = sysStruct.visuals[va.visualIndex];
			DO_POSITION p = sysStruct.positions[v.positionIndex];
			DO_ORIENTATION o = sysStruct.orientations[v.orientationIndex];
			DO_LOD lod = sysStruct.lods[v.lodIndex];
			vis->setRelativePosition(p.positionX, p.positionY, p.positionZ);
			vis->setRelativeOrientation(o.orientationX, o.orientationY, o.orientationZ);
			if (v.vertexAnimIndex >= 0) {
				CVertexAnimationModule* vAnim;
				sysStruct.vertexAnims[v.vertexAnimIndex]->Clone(&vAnim);
				vis->setVertexAnim(vAnim);
			} else {
				vis->setVertexAnim(NULL);
			}
			MaterialCompressor* mc = MaterialCompressor::getMaterialCompressor(archiveName.GetString());
			CMaterialObject* mat = NULL;
			if (mc)
				mat = mc->getMaterial(v.matCompressIndex);
			vis->setMaterial(mat);
			delete mat;

			//@@Note: this decompress function is not currently used for Client because we already move to DOB streaming. (Yanfeng 05/2009)
			// Since we already moved material serialization to material compressor, it's much harder to find a right
			// opportunity to save m_originalTextureSelect because m_textureSelects are available only after CreateReMaterial,
			// which is not done in a centralized manner. Even worse, you can't put in inside CreateReMaterial because
			// there are other cases when CreateReMaterial is called. We have to do it only when we get a CreateReMaterial
			// call after MaterialCompressor::getMaterial.
			//
			// At the mean time, I'm going to do individually -- dynamic object first because we need it in UGC.
			{
				// Now material is set and m_textureSelect's are computed - a right time point to save m_textureSelect
				// copy m_textureSelect to m_originalTextureSelect
				CMaterialObject* pVisMat = vis->getMaterial();
				if (pVisMat) {
					for (int texLoop = 0; texLoop < NUM_CMATERIAL_TEXTURES; texLoop++)
						pVisMat->m_texNameHashOriginal[texLoop] = pVisMat->m_texNameHash[texLoop];
				}
			}

			vis->setName(sysStruct.names[v.nameIndex]);
			for (unsigned int k = 0; k < lod.lodCount; k++) {
				vis->addLOD(lod.hashes[k], lod.distances[k]);
			}
			vis->setCanCollide(v.canCollide);
			sdo->AddVisual(vis, va.customizable, va.mediaSupported);
		}

		sdo->setBoundBox(sdo->computeBoundingBox());
		sys->addDynamicObject(sdo);
	}
}

void DynamicObjectLibrary::deleteAndZero(DO_SYS_STRUCT& sysStruct) {
	for (unsigned int i = 0; i < sysStruct.collisions.size(); i++) {
		sysStruct.collisions[i]->SafeDelete();
		delete sysStruct.collisions[i];
	}
	sysStruct.collisions.clear();
	for (unsigned int i = 0; i < sysStruct.triggers.size(); i++) {
		sysStruct.triggers[i]->SafeDelete();
		delete sysStruct.triggers[i];
	}
	sysStruct.triggers.clear();
	for (unsigned int i = 0; i < sysStruct.vertexAnims.size(); i++) {
		sysStruct.vertexAnims[i]->SafeDelete();
		delete sysStruct.vertexAnims[i];
	}
	sysStruct.vertexAnims.clear();

	sysStruct.dynamicObjects.clear();
	sysStruct.lightPlacements.clear();
	sysStruct.lights.clear();
	sysStruct.lods.clear();
	sysStruct.names.clear();
	sysStruct.orientations.clear();
	sysStruct.popouts.clear();
	sysStruct.positions.clear();
	sysStruct.visualAssignments.clear();
	sysStruct.visuals.clear();
}

// file versioning version 1
DynamicObjectRM* DynamicObjectLibrary::readSystemv1(CArchive& ar) {
	unsigned char code;
	ar >> code;
	if (code != sys_archive_code) {
		// handle not a streamable dynamic obj system in ClientBaseEngine::LoadDynamicObjects
		//AfxThrowArchiveException(CArchiveException::badClass, "dynamic obj system");
		return NULL;
	} else {
		DynamicObjectRM* sys = new DynamicObjectRM("sysDynamicObjectRM");
		int count;
		ar >> count;
		for (int i = 0; i < count; i++) {
			StreamableDynamicObject* ob = readDynamicObject(ar);
			ob->setBoundBox(ob->computeBoundingBox());
			sys->addDynamicObject(ob);
		}
		return sys;
	}
}

DynamicObjectRM* DynamicObjectLibrary::readSystemv2(CArchive& ar, MaterialCompressor* matCompressor, int version) {
	unsigned char code;
	ar >> code;
	if (code != sys_archive_code) {
		return NULL;
	} else {
		DynamicObjectRM* sys = new DynamicObjectRM("sysDynamicObjectRM");
		DYNAMIC_OBJ_SYSTEM_V2 sysStruct;

		unsigned int count;
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			CStringA name;
			ar >> name;
			sysStruct.names.push_back(name);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DO_LIGHT_V1 light;
			ar.Read(&light, sizeof(DO_LIGHT_V1));
			sysStruct.lights.push_back(light);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DO_LIGHT_PLACEMENT_V1 lightPlacement;
			ar.Read(&lightPlacement, sizeof(DO_LIGHT_PLACEMENT_V1));
			sysStruct.lightPlacements.push_back(lightPlacement);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DO_POPOUT_V1 pop;
			ar.Read(&pop, sizeof(DO_POPOUT_V1));
			sysStruct.popouts.push_back(pop);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DO_POSITION pos;
			ar.Read(&pos, sizeof(DO_POSITION));
			sysStruct.positions.push_back(pos);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DO_ORIENTATION orien;
			ar.Read(&orien, sizeof(DO_ORIENTATION));
			sysStruct.orientations.push_back(orien);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			CCollisionObj* col;
			ar >> col;
			sysStruct.collisions.push_back(col);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			CTriggerObjectList* trgs;
			ar >> trgs;
			sysStruct.triggers.push_back(trgs);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			CVertexAnimationModule* vAnim;
			ar >> vAnim;
			sysStruct.vertexAnims.push_back(vAnim);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DO_LOD_V1 lod;
			ar.Read(&lod, sizeof(DO_LOD));
			sysStruct.lods.push_back(lod);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			if (version == 2) {
				DO_VISUAL_V1 visTemp;
				DO_VISUAL_V2 vis;
				ar.Read(&visTemp, sizeof(DO_VISUAL_V1));
				vis = visTemp;
				sysStruct.visuals.push_back(vis);
			} else {
				DO_VISUAL_V2 vis;
				ar.Read(&vis, sizeof(DO_VISUAL_V2));
				sysStruct.visuals.push_back(vis);
			}
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DO_VISUAL_ASSIGNMENT_V1 visa;
			ar.Read(&visa, sizeof(DO_VISUAL_ASSIGNMENT_V1));
			sysStruct.visualAssignments.push_back(visa);
		}
		ar >> count;
		for (unsigned int i = 0; i < count; i++) {
			DYNAMIC_OBJ_V1 dOb1;
			DYNAMIC_OBJ dOb;
			unsigned int vCount;
			ar.Read(&dOb1, sizeof(DYNAMIC_OBJ_V1));
			dOb.attachable = dOb1.attachable;
			dOb.collisionIndex = dOb1.collisionIndex;
			dOb.id = dOb1.id;
			dOb.interactive = dOb1.interactive;
			dOb.lightPlacementIndex = dOb1.lightPlacementIndex;
			dOb.nameIndex = dOb1.nameIndex;
			dOb.particleEffectId = dOb1.particleEffectId;
			dOb.particleOffsetIndex = dOb1.particleOffsetIndex;
			dOb.triggersIndex = dOb1.triggersIndex;
			sysStruct.dynamicObjects.push_back(dOb);
			ar >> vCount;
			for (unsigned int j = 0; j < vCount; j++) {
				unsigned int visIndex;
				ar >> visIndex;
				sysStruct.dynamicObjects[i].visualAssignments.push_back(visIndex);
			}
		}
		deCompress(sys, sysStruct, Utf16ToUtf8(ar.m_strFileName).c_str());
		deleteAndZero(sysStruct);
		return sys;
	}
}

// Reads a dynamic object system from an archive.  This archive contains all mesh data and meta-data for
// dynamic objects.
DynamicObjectRM* DynamicObjectLibrary::readFromDisk(CArchive& ar) {
	unsigned char version;
	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	DynamicObjectRM* res = NULL;
	ar >> version;
	if (!::ServerSerialize) {
		if (version >= 1) {
			readDynamicObjMeshesIntoPool(ar);
		}
		if (version >= 2) {
			matCompressor->readFromArchive(ar);
		}
	}
	if (version == 1) {
		res = readSystemv1(ar);
	} else if (version >= 2) {
		res = readSystemv2(ar, matCompressor, version);
	}

	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	return res;
}

/*!
 * \brief
 * Reads the dynamic object from the given archive into the new DYNAMIC mesh pool
 *
 * \param ar
 * the CFileArchive containing the data
 *
 * \throws <CArchiveException>
 * Isn't this everyone's favorite
 *
 * Reads the dynamic object meshes into the pool using Charles's loadremeshes functions
 *
 * \remarks
 * Write remarks for readDynamicObjMeshesIntoPool here.
 *
 * \see
 * Separate items with the '|' character.
 */
void DynamicObjectLibrary::readDynamicObjMeshesIntoPool(CArchive& ar) {
	// read all meshes into the dynamic pool
	MeshRM::Instance()->LoadReMeshes(ar, IClientEngine::Instance()->GetReDevice(), MeshRM::RMP_ID_DYNAMIC);
}
