/******************************************************************************
 DistributionProgressiveDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// DistributionProgressiveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DistributionUpdateGlobals.h"
#include "DistributionTargetDlg.h"
#include "DistributionProgressiveDlg.h"

// CDistributionProgressiveDlg dialog

IMPLEMENT_DYNAMIC(DistributionProgressiveDlg, CDialog)
DistributionProgressiveDlg::DistributionProgressiveDlg(CWnd *pParent /*=NULL*/, CRCLIST *inputCRCLIST, int crcFileCount) :
		CDialog(DistributionProgressiveDlg::IDD, pParent) {
	m_CRCList = inputCRCLIST;
	m_FileCount = crcFileCount;
}

DistributionProgressiveDlg::~DistributionProgressiveDlg() {
}

void DistributionProgressiveDlg::refreshLists(bool skipDialog) {
	if (!skipDialog) {
		m_listRequired.ResetContent();
		m_listProgressive.ResetContent();
	}
	long progressiveFileSize = 0;
	long requiredFileSize = 0;
	long totalFileSize = 0;
	int progressiveCount = 0;
	int requiredCount = 0;

	for (int i = 0; i < m_FileCount; i++) {
		if (m_CRCList[i].bRequired) {
			if (!skipDialog)
				m_listRequired.AddString(Utf8ToUtf16(m_CRCList[i].filename).c_str());
			requiredCount = requiredCount + 1;
			requiredFileSize = requiredFileSize + m_CRCList[i].nFilesize;
		} else {
			if (!skipDialog)
				m_listProgressive.AddString(Utf8ToUtf16(m_CRCList[i].filename).c_str());
			progressiveCount = progressiveCount + 1;
			progressiveFileSize = progressiveFileSize + m_CRCList[i].nFilesize;
		}
		totalFileSize = totalFileSize + m_CRCList[i].nFilesize;
	}
	requiredFileSize = requiredFileSize / 1024; // Conversion to KB because bytes is somewhat pointless
	progressiveFileSize = progressiveFileSize / 1024;
	totalFileSize = totalFileSize / 1024;

	if (!skipDialog) {
		CStringW temp;
		// Set file size for progressive
		temp.Format(L"%d", progressiveFileSize);
		for (int j = temp.GetLength(); j > 0; j = j - 3) {
			if (j != temp.GetLength()) // No comma in the 1's place
			{
				temp.Insert(j, ',');
			}
		}
		temp = L"Size: " + temp;
		temp = temp + L" KB";
		m_staticProgressiveSize.SetWindowText(temp);
		// set file size for required
		temp.Format(L"%d", requiredFileSize);
		for (int j = temp.GetLength(); j > 0; j = j - 3) {
			if (j != temp.GetLength()) // No comma in the 1's place
			{
				temp.Insert(j, ',');
			}
		}
		temp = L"Size: " + temp;
		temp = temp + L" KB";
		m_staticRequiredSize.SetWindowText(temp);
		// set total file size
		temp.Format(L"%d", totalFileSize);
		for (int j = temp.GetLength(); j > 0; j = j - 3) {
			if (j != temp.GetLength()) // No comma in the 1's place
			{
				temp.Insert(j, ',');
			}
		}
		temp = L"Size: " + temp;
		temp = temp + L" KB";
		m_staticTotalSize.SetWindowText(temp);
		temp.Format(L"Count: %d", progressiveCount);
		m_staticProgressiveCount.SetWindowText(temp);
		temp.Format(L"Count: %d", requiredCount);
		m_staticRequiredCount.SetWindowText(temp);
		temp.Format(L"Count: %d", m_FileCount);
		m_staticTotalCount.SetWindowText(temp);
		// Clear up the selected info data
		m_staticSelectedName.SetWindowText(L"None Selected");
		m_staticSelectedSize.SetWindowText(L"Size: ");
		m_comboFileCommand.SelectString(-1, L"None");
		m_comboFileCommand.EnableWindow(false);
	}
}

BOOL DistributionProgressiveDlg::OnInitDialog() {
	CDialog::OnInitDialog();
	m_comboFileCommand.AddString(L"None");
	m_comboFileCommand.AddString(L"Delete");
	m_comboFileCommand.AddString(L"Register");
	refreshLists();
	//GetDlgItem(IDC_PUBLISH_LOCATION_GROUP)->EnableWindow();
	//GetDlgItem(IDC_EXPORT_TO_DB)->EnableWindow(canDbExport);
	//GetDlgItem(IDC_DB_EXPORT_ONLY)->EnableWindow(canDbExport);
	return true;
}

void DistributionProgressiveDlg::DoDataExchange(CDataExchange *pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_REQUIRED, m_listRequired);
	DDX_Control(pDX, IDC_LIST_PROGRESSIVE, m_listProgressive);
	DDX_Control(pDX, IDC_STATIC_PROGRESSIVE_SIZE, m_staticProgressiveSize);
	DDX_Control(pDX, IDC_STATIC_REQUIRED_SIZE, m_staticRequiredSize);
	DDX_Control(pDX, IDC_STATIC_TOTAL_SIZE, m_staticTotalSize);
	DDX_Control(pDX, IDC_STATIC_PROGRESSIVE_COUNT, m_staticProgressiveCount);
	DDX_Control(pDX, IDC_STATIC_REQUIRED_COUNT, m_staticRequiredCount);
	DDX_Control(pDX, IDC_STATIC_TOTAL_COUNT, m_staticTotalCount);
	DDX_Control(pDX, IDC_STATIC_SELECTED_NAME, m_staticSelectedName);
	DDX_Control(pDX, IDC_STATIC_SELECTED_SIZE, m_staticSelectedSize);
	DDX_Control(pDX, IDC_COMBO_FILE_COMMAND, m_comboFileCommand);
	DDX_Control(pDX, IDC_EXTENSION, m_requiredextension);
	DDX_Control(pDX, IDC_BUTTON_MOVESPECIFICS, m_requiredmoveall);
}

inline int getCRCIndex(CRCLIST *crclst, int crcCount, CStringW filename) {
	for (int i = 0; i < crcCount; i++) {
		if (strcmp(Utf16ToUtf8(filename.GetString()).c_str(), crclst[i].filename.c_str()) == 0) {
			return i;
		}
	}
	return -1; // This should never ever ever happen.
}

void DistributionProgressiveDlg::OnBTNMakeProgressive() {
	int scrollPos = m_listRequired.GetTopIndex();
	int count = m_listRequired.GetCount();
	m_listRequired.SetCurSel(-1); // clear all
	for (int index = 0; index < count; index++) {
		if (m_listRequired.GetSel(index)) {
			CStringW seltextW;
			m_listRequired.GetText(index, seltextW);
			int crcindex = getCRCIndex(m_CRCList, m_FileCount, seltextW);
			m_CRCList[crcindex].bRequired = false;
		}
	}
	refreshLists();
	m_listRequired.SetTopIndex(scrollPos);
}

//******************************************************************************
//Move all the files of the specified extension to the Progressive side.
//This function also take cares of lower and upper case of the extension as well.
//Added by Jerome.
//******************************************************************************
void DistributionProgressiveDlg::OnBnClickedButtonMovespecifics() {
	CStringW ctext;
	CStringW selectedstring;
	m_requiredextension.GetWindowText(ctext);
	if (!ctext.IsEmpty()) {
		ctext = ctext.MakeLower();

		int count = m_listRequired.GetCount();
		int firstSel = -1;
		m_listRequired.SetCurSel(-1); // clear all

		for (int index = 0; index < count; index++) {
			m_listRequired.GetText(index, selectedstring);
			selectedstring = selectedstring.MakeLower();

			if (selectedstring.Find(ctext) >= 0) {
				if (firstSel == -1)
					firstSel = index;
				m_listRequired.SetSel(index, TRUE);
			}
		}
		if (firstSel >= 0)
			m_listRequired.SetTopIndex(firstSel);

		count = m_listProgressive.GetCount();
		firstSel = -1;
		m_listProgressive.SetCurSel(-1); // clear all
		for (int index = 0; index < count; index++) {
			m_listProgressive.GetText(index, selectedstring);
			selectedstring = selectedstring.MakeLower();

			if (selectedstring.Find(ctext) >= 0) {
				if (firstSel == -1)
					firstSel = index;
				m_listProgressive.SetSel(index, TRUE);
			}
		}
		if (firstSel >= 0)
			m_listProgressive.SetTopIndex(firstSel);
	}
}

void DistributionProgressiveDlg::OnBTNMakeRequired() {
	int scrollPos = m_listRequired.GetTopIndex();
	int count = m_listProgressive.GetCount();
	m_listProgressive.SetCurSel(-1); // clear all
	for (int index = 0; index < count; index++) {
		if (m_listProgressive.GetSel(index)) {
			CStringW seltextW;
			m_listProgressive.GetText(index, seltextW);
			int crcindex = getCRCIndex(m_CRCList, m_FileCount, seltextW);
			m_CRCList[crcindex].bRequired = true;
		}
	}

	refreshLists();
	m_listProgressive.SetTopIndex(scrollPos);
}

void DistributionProgressiveDlg::OnSelchangeListRequired() {
	int index = m_listRequired.GetCaretIndex();
	CStringW seltextW;
	m_listRequired.GetText(index, seltextW);
	int crcindex = getCRCIndex(m_CRCList, m_FileCount, seltextW);
	// Init Selected Box info
	m_staticSelectedName.SetWindowText(Utf8ToUtf16(m_CRCList[crcindex].filename).c_str());
	CStringW temp;
	int size = m_CRCList[crcindex].nFilesize / 1024;
	temp.Format(L"%d", size);
	for (int j = temp.GetLength(); j > 0; j = j - 3) {
		if (j != temp.GetLength()) // No comma in the 1's place
		{
			temp.Insert(j, ',');
		}
	}
	temp = L"Size: " + temp;
	temp = temp + L" KB";
	m_staticSelectedSize.SetWindowText(temp);
	m_comboFileCommand.SelectString(-1, Utf8ToUtf16(m_CRCList[crcindex].command).c_str());
	m_comboFileCommand.EnableWindow(true);
	return;
}

void DistributionProgressiveDlg::OnSelchangeListProgressive() {
	int index = m_listProgressive.GetCaretIndex();

	CStringW seltext;
	m_listProgressive.GetText(index, seltext);
	int crcindex = getCRCIndex(m_CRCList, m_FileCount, seltext);
	// Init Selected Box info
	m_staticSelectedName.SetWindowText(Utf8ToUtf16(m_CRCList[crcindex].filename).c_str());
	CStringA temp;
	int size = m_CRCList[crcindex].nFilesize / 1024;
	temp.Format("%d", size);
	for (int j = temp.GetLength(); j > 0; j = j - 3) {
		if (j != temp.GetLength()) // No comma in the 1's place
		{
			temp.Insert(j, ',');
		}
	}
	temp = "Size: " + temp;
	temp = temp + " KB";
	m_staticSelectedSize.SetWindowText(Utf8ToUtf16(temp).c_str());
	m_comboFileCommand.SelectString(-1, Utf8ToUtf16(m_CRCList[crcindex].command).c_str());
	m_comboFileCommand.EnableWindow(true);
	return;
}

void DistributionProgressiveDlg::OnSelchangeComboCommand() {
	int crcindex = -1;
	int requiredIndex = m_listRequired.GetCurSel();
	int progressiveIndex = m_listProgressive.GetCurSel();
	if (requiredIndex >= 0) {
		CStringW seltext;
		m_listRequired.GetText(requiredIndex, seltext);
		crcindex = getCRCIndex(m_CRCList, m_FileCount, seltext);
	} else if (progressiveIndex >= 0) {
		CStringW seltext;
		m_listProgressive.GetText(progressiveIndex, seltext);
		crcindex = getCRCIndex(m_CRCList, m_FileCount, seltext);
	}
	int cmdIndex = m_comboFileCommand.GetCurSel();
	if (cmdIndex >= 0) {
		char *cmd;
		switch (cmdIndex) {
			case 0:
				cmd = "None";
				break;
			case 1:
				cmd = "Delete";
				break;
			case 2:
				cmd = "Register";
				break;
			default:
				cmd = "None";
				break;
		}
		m_CRCList[crcindex].command = cmd;
	}
}

BEGIN_MESSAGE_MAP(DistributionProgressiveDlg, CDialog)
ON_BN_CLICKED(IDC_BUTTON_MAKE_PROGRESSIVE, OnBTNMakeProgressive)
ON_BN_CLICKED(IDC_BUTTON_MAKE_REQUIRED, OnBTNMakeRequired)
ON_LBN_SELCHANGE(IDC_LIST_REQUIRED, OnSelchangeListRequired)
ON_LBN_SELCHANGE(IDC_LIST_PROGRESSIVE, OnSelchangeListProgressive)
ON_CBN_SELCHANGE(IDC_COMBO_FILE_COMMAND, OnSelchangeComboCommand)
ON_BN_CLICKED(IDC_BUTTON_MOVESPECIFICS, OnBnClickedButtonMovespecifics)
END_MESSAGE_MAP()
