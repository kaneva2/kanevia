// KEPEditView.h : interface of the CKEPEditView class
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved
#pragma once

#define D3D_OVERLOADS
//#include <AfxWin.h>
//#include <AfxExt.h>
#include "d3d9.h"
#include "common/include/kepcommon.h"
#include "ClientEngine.h"
#include "KEPEditDoc.h"

// DIALOGS
#include "WaitDialog.h"
#include "ObjectsDlg.h"
#include "BackgroundDlg.h"
#include "MenusDlg.h" // old menus
#include "ActorsDlg.h"
#include "CamerasViewportsDlg.h"
#include "TextFontDlg.h"
#include "ExplosionsDlg.h"
#include "ProjectileSystemsDlg.h"
#include "ScriptDlg.h"
#include "TextureDlg.h"
#include "ParticleSystemDlg.h"
#include "SpawnGeneratorDlg.h"
#include "DynamicObjectsDlg.h"
#include "DistributionUpdateGlobals.h"
#include "KEPEditCommandInfo.h"
#ifndef _ClientOutput
#include "NodeImportDlg.h"
#endif
#include "ControlsDlg.h"
#include "EnvironmentDlg.h"
#include "AIDlg.h"
#include "AnimatedTexturesDlg.h"
#include "MusicDlg.h"
#include "MoviesDlg.h"

#include "IAssetsDatabase.h"
#include <SerializeHelper.h>
#include "EditorState.h"
#include "KEPGameFile.h"
#include "DistributionTargetDlg.h"
#include "Game.h"
#include "PublishGameStatusDlg.h"

#include "ReferencePointer.h"

// Forward Declarations
class CMissileObj;
class CEXScriptObjList;
class CWldGroupObj;
class CCurrentWorldsDlg;
class CRuntimeEntityViewerDlg;

#define CLIENT_FILE_LOCATION "ClientFiles"
#define SERVER_FILE_LOCATION "ServerFiles"
#define PATCH_FILE_LOCATION "PatchFiles"
#define GAME_FILE_LOCATION "GameFiles"
#define KEPADMIN_LOCATION "KEPAdmin"
#define KEPADMIN_LOGIN_LOCATION "KEPLogin.aspx"
#define PARENT_DIRECTORY ".."
#define SERVER_FILES_LOCATION "ServerFiles"
#define SQL_SCRIPTS_LOCATION "SqlScripts"
#define SQL_DISTRIBUTION_EXPORT_LOCATION "distribution_export.sql"

typedef vector<pair<string, bool>> strVect;
typedef vector<string> dirVect;
typedef vector<pair<string, int>> zoneFileVect; // name, type
typedef vector<zoneFileVect> dependVect;

class CKEPEditView : public ClientEngine, public IKEPDialogProcessor //, public IGameDirProvider
{
protected:
	CKEPEditView();
	DECLARE_DYNCREATE(CKEPEditView)

public:
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }

	CKEPEditDoc* GetDocument() const;
	CStringA GetCurrentSceneFile() const { return NewWorld; }

	void AddToStrVect(strVect& sv, const string& str);

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual void RenderLoopInfoUpdate();

	void SetEditorAcceptText(BOOL val);

protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

public:
	// === Implementation of IKEPDialogProcessor ===
	CWnd* Preprocess(CWnd* parent, UINT dlgId, UINT dlgType = 0);
	BOOL PostProcess(CWnd* wnd, UINT dlgId);

protected:
	bool GetInitialized() { return m_initialized; }
	virtual void SetInitialized(bool flag);

	void SetTitle();

	CWldGroupObj* GetSelectedGroup();

	//!**************************************************************************
	//! /brief Compose a string for identifying and displaying zone objects in the tree.
	//! /param objectName Name of the world object or reference object.
	//! /param subID ID of sub-mesh for objects with material-based multiple sub-meshes. Supply -1 if single material.
	//! /param instanceNo Instance number for reference objects. Supply -1 if world object.
	//! /return Composed ID string to identify a zone object.
	CStringA MakeZoneObjectID(IN const CStringA& objectName, IN int subID = -1, IN int instanceNo = -1);

	//!**************************************************************************
	//! /brief Parse the identification string created by MakeZoneWorldObjectID()
	//! /param objectName Name of the world object or reference object.
	//! /param subID ID of sub-mesh for objects with material-based multiple sub-meshes. Return -1 if single material.
	//! /param instanceNo Instance number for reference objects. Return -1 if world object.
	void ParseZoneObjectID(IN const CStringA& IDStr, OUT CStringA& objectName, OUT int& subID, OUT int& instanceNo);

	//!**************************************************************************
	//! /brief Parse the identification string created by MakeZoneWorldObjectID() - subID is not parsed in this function. It's treated as part of the objectName.
	//! /param objectName Name of the world object or reference object.
	//! /param instanceNo Instance number for reference objects. Return -1 if world object.
	void ParseZoneObjectID(IN const CStringA& IDStr, OUT CStringA& objectName, OUT int& instanceNo);

	//!********************************************************************
	//! \brief Return pointer to default collision group.
	//! \return Pointer to default collision group (CWldGroupObj*).
	//!
	//! This function will iterate thru all existing world object groups and find the default
	//! collision group (where all collision meshes specified with LOD "C" will be automatically
	//! imported). If default collision group does not already exist, create and return it.
	CWldGroupObj* GetDefaultCollisionGroup();

	//!********************************************************************
	//! \brief Return the next available world object group ID.
	//! \return available world object group ID to be assigned to a new group.
	int GetNextAvailableWldGroupID();

	//!********************************************************************
	//! \brief Add a new LOD to an existing world object.
	//! \param pWldObj World object where LOD is to be added
	//! \param pMesh CEXMeshObj used to construct LOD
	//! \param lodType 0 = by distance, 1 = box
	//! \param activationDistance Activate LOD if camera is moving further than argument from object. (if lodType==0)
	//! \param activationBox Activate LOD if camera moves outside the box originated at the object. (if lodType==1)
	//! \return LOD added
	CMeshLODObject* AddLODToWorldObject(CWldObject* pWldObj, CEXMeshObj* pMesh, int lodType, float activationDistance, ABBOX* activationBox);

public:
	// Continue message pump and update progress bar on lengthy loads
	void ProgressUpdate(CStringA position, BOOL init = FALSE, BOOL deinit = FALSE);
	void DistributionProgressUpdate(BOOL init, BOOL deinit, int stepId);

public:
	bool RenderLoop(bool canWait);

	void RenderLoopMessageHandling();
	void ControlsInLoop();

	void OnDraw(CDC* /*pDC*/);
	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void FreeAll();
	void ReInit();

	void SetGamePubUserPass(CStringA user, CStringA pass) {
		m_gamePubUser = user;
		m_gamePubPass = pass;
	}
	void GetGamePubUserPass(CStringA& user, CStringA& pass) {
		user = m_gamePubUser;
		pass = m_gamePubPass;
	}

	Logger& GetLogger() { return m_engineLogger; }

	virtual bool AddRuntimeCamera(CStringA camera_name, int runtime_camera_id);
	virtual bool RemoveRuntimeCamera(int runtime_camera_id);
	Game* GetGame() { return &m_game; }
	ULONG CKEPEditView::GetArrayCount(ULONG indexOfArray);
	IGetSet* CKEPEditView::GetObjectInArray(IN ULONG indexOfArray, IN ULONG indexInArray);

	bool CreateDistroShortCuts(CStringA destinationDirectory);

	void SetParamUsername(CStringA user) { m_paramUsername = user; }
	void GetParamUsername(CStringA& user) { user = m_paramUsername; }

	void SetParamPassword(CStringA pass) { m_paramPassword = pass; }
	void GetParamPassword(CStringA& pass) { pass = m_paramPassword; }

	void SetParamAppName(CStringA appName) { m_paramAppName = appName; }
	void GetParamAppName(CStringA& appName) { appName = m_paramAppName; }

	void SetParamTemplateName(CStringA templateName) { m_paramTemplateName = templateName; }
	void GetParamTemplateName(CStringA& templateName) { templateName = m_paramTemplateName; }

	void SetParamInstallDirectory(CStringA dir) { m_paramInstallDirectory = dir; }
	void GetParamInstallDirectory(CStringA& dir) { dir = m_paramInstallDirectory; }

	void SetParamClientRunning(bool running) { m_paramClientRunning = running; }
	bool GetParamClientRunning() { return m_paramClientRunning; }

protected:
	bool CleanDistributionDirectories(CStringA destDir, CStringA patchFolderLocation);
	bool GetPublishFolder(CStringA& pubFolder, CStringA accountXML, CStringA& folderGameId);
	bool GetDistributionFolder(CStringA& distFolder, CStringA platformInstallLocation);
	bool UpdateGameFile(CStringA gameId, CStringA clientDir);
	CStringA GetGameId(CStringA destDir);

	void CreateLocalCondatVer(CStringA clientDir);

	// Implementation
private:
	bool m_initialized;
	bool m_updateGameFiles;
	bool m_makeDistributionAutomatically;

	//CStringA m_baseDir;
	CWaitDialog* m_WaitDialog;
	CStringA m_title;
	CStringA m_MRUgameFile; //Set by the App when processing MRU menu
	int m_MRUgameFileIndex;
	CStringA m_zoneFile; //Set by the View when getting a request from ClientEngine
		// that a scene should auto-load

	CStringA m_gamePubUser;
	CStringA m_gamePubPass;
	CStringA m_delayLoadHousingZone;

	CStringA m_paramUsername;
	CStringA m_paramPassword;
	CStringA m_paramAppName;
	CStringA m_paramTemplateName;
	CStringA m_paramInstallDirectory;
	bool m_paramClientRunning;

public:
	virtual ~CKEPEditView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	enum SERVERACTIONS { START = 0,
		STOP };
	BOOL ServerAction(SERVERACTIONS action);
	BOOL ServerIsRunning();
	void CreateDistribution(const char* scriptName);
	void RunClientAndServer(const char* clientFolder, const char* serverFolder);

	virtual BOOL EditorLoadScene(CStringA filename);

	// overloaded load functions
	BOOL LoadElementDatabase(const string& fileName);
	BOOL LoadSkillsDatabase(const string& fileName);
	BOOL LoadAnimationDatabase(const string& fileName);
	BOOL LoadEntityDatabase(const string& fileName);
	BOOL LoadMissileDatabase(const string& fileName);
	BOOL LoadExplosionFile(const string& fileName);
	BOOL LoadAiData(const string& fileName);
	BOOL LoadDynamicObjects(const string& fileName);
	void LoadItemGeneratedData(const string& fileName);

	BOOL LoadActorFromDir(CMovementObj& actor, const CStringA& directory, const std::vector<string>& fileNames, BOOL resetBaseSkeleton);
	BOOL LoadScene(CStringA fileName, const char* url = 0);
	bool BuildCollisionDatabase(CWldObjectList* WldObjList, size_t maxLevels, size_t maxFacesPerBox, int nResX, int nResY, int nResZ, CReferenceObjList* libraryReferenceDB, HWND hwndProgTotal = NULL, HWND hwndProgTask = NULL);

	// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	//afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnAppRender();
	virtual void OnInitialUpdate();
	afx_msg void OnFileOpen();
	afx_msg void OnEditorZoneNew();
	afx_msg void OnEditorZoneOpen();
	afx_msg void OnEditorZoneSave();
	afx_msg void OnEditorZoneSaveAs();
	afx_msg LRESULT OnNewZone(WPARAM wParam, LPARAM lParam);
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAll(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoneSave(CCmdUI* pCmdUI);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnUpdateAppRender(CCmdUI* pCmdUI);
	afx_msg void OnFileSave();
	afx_msg void OnFileSaveAll();
	afx_msg void OnFileSaveAs();

	void SetMRUFileToOpen(CStringA gameFile, int index) {
		m_MRUgameFile = gameFile;
		m_MRUgameFileIndex = index;
	}
	CStringA GetMRUFileToOpen(int* index);
	bool DoGameDistribution(CStringA& destinationDirectory, const char* cfgFileName = 0, const char* destDirOverride = 0, bool childGame = false, bool promptForType = true);
	//
	// In order to help automate the distribution process,
	// the reply to 'do you want to update the game files'
	// is assumed to be yes, but can be overridden by a
	// command line flag.
	//
	void updateGameFiles(bool val = true) { m_updateGameFiles = val; }
	void makeDistributionAutomatically(bool val = true) { m_makeDistributionAutomatically = val; }

	CStringA GetZoneFileToOpen() { return m_zoneFile; }
	void ClearZoneFileToOpen() { m_zoneFile.Empty(); }

	void loadAutoExec(CStringA baseDir);
	BOOL loadEditorSVR(CStringA baseDir, CStringA fname);
	void loadSVRParts(CStringA baseDir);
	BOOL SaveEditorSVR(CStringA baseDir, CStringA fname);
	BOOL makeEmptySVR(CStringA baseDir, CStringA fname);
	BOOL EditorZoneSaveAs(BOOL saveAs);
	void ProcessTextureLevelChange(int textureIndex);
	void CommencePublishProcess(); //HWND hWndParent);

	afx_msg void OnUpdateEditorZoneOpen(CCmdUI* pCmdUI);
	afx_msg void OnEditorStartStopServer();
	afx_msg void OnUpdateEditorZoneNew(CCmdUI* pCmdUI);
	afx_msg void OnEditorToolsManageTextureDB();
	afx_msg void OnEditorPlaceFirstActor();
	afx_msg void OnEditorToggleWireframe();
	afx_msg void OnEditorToggleLockGameWindow();
	afx_msg void OnGamePublish();
	afx_msg void OnGameDistribution();
	afx_msg void OnGameRun();
	afx_msg void OnGameRunSpecificDistribution();
	afx_msg void RunSpecificDistribution(CStringA strDistroPath);
	afx_msg void OnGameRunDistribution();
	void OnGameDistribution(const char* destDirOverride, bool childGame, bool promptForType = true);

	//{{AFX_MSG(ClientEngine)
public:
	afx_msg void OnButtonQuit();
	afx_msg void OnTextureFileOpen1();
	afx_msg void OnTextureFileOpen2();
	afx_msg void OnTextureFileOpen3();
	afx_msg void OnTextureFileOpen4();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnButtonSave();
	afx_msg void OnButtonMenu();
	afx_msg void OnButtonTreetest();
	afx_msg void OnButtonObjectload();
	afx_msg void OnButtonObjectdelete();
	afx_msg void OnButtonSwitchObject();
	afx_msg void OnBUTTONBKEDITORswtch();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonMvUp();
	afx_msg void OnButtonMvDown();
	afx_msg void OnButtonMvLeft();
	afx_msg void OnButtonMvRight();
	afx_msg void OnButtonAddnewstaticBk();
	afx_msg void OnBUTTONBKSETTINGSapply();
	afx_msg void OnButtonControls();
	afx_msg void OnButtonTestEffect();
	afx_msg void OnButtonStopEffect();
	afx_msg void OnButtonMovies();
	afx_msg void OnButtonTestmovie();
	afx_msg void OnBtnEntityCreatenew();
	afx_msg void OnBtnEntityActivate();
	afx_msg void OnButtonSwEntities();
	afx_msg void OnButtonTestfullscreen();
	afx_msg void OnBtnViewwireframe();
	afx_msg void OnButtonViewsmoothshaded();
	afx_msg void OnBtnEntityMovement();
	afx_msg void OnBtnEntityDeactivate();
	afx_msg void OnBtnDeleteEntity();
	afx_msg void OnBTNENTITYdeleteAnim();
	afx_msg void OnBTNENTITYDeleteChld();
	afx_msg void OnBtnEntityAddchild();
	afx_msg void OnBtnNewall();
	afx_msg void OnBTNCameraViewportsSw();
	afx_msg void OnCAMERACreateNewCamera();
	afx_msg void OnBTNCreateNewViewport();
	afx_msg void OnBTNCameraVportActivate();
	afx_msg void OnBTNvportDeactivate();
	afx_msg void OnBtnAddRuntimeCamera();
	afx_msg void OnBtnRemoveRuntimeCamera();
	afx_msg void OnBtnAddRuntimeViewport();
	afx_msg void OnBtnRemoveRuntimeViewport();
	afx_msg void OnBTNMOUSEcreate();
	afx_msg void OnBTNUImouseSW();
	afx_msg void OnBTNMOUSEfileBrowse();
	afx_msg void OnBTNMOUSEactivate();
	afx_msg void OnBTNMOUSEdelete();
	afx_msg void OnBTNMOUSEdeactivate();
	afx_msg void OnBTN2DINTERFACEsw();
	afx_msg void OnBTN2DCreateCollection();
	afx_msg void OnBTN2DADDunit();
	afx_msg void OnBtn2dActivate();
	afx_msg void OnBtn2dDeactivate();
	afx_msg void OnBTN2DBrowseFileUp();
	afx_msg void OnBTN2DBrowseFileDown();
	afx_msg void OnBTN2DgetSelectedInfo();
	afx_msg void OnBTN2DApplyModifications();
	afx_msg void OnBTN2DDeleteCollection();
	afx_msg void OnBTN2DDeleteUnit();
	afx_msg void OnBUTTONEnviormentSw();
	afx_msg void OnBTNENVBckRgb();
	afx_msg void OnBTNENVFogRgb();
	afx_msg void OnBTNENVApplyChanges();
	afx_msg void OnBTNENVMatch();
	afx_msg void OnCHECKENVFogOn();
	afx_msg void OnBTNENVBrowseSurObj();
	afx_msg void OnCHECKEnableSurrounding();
	afx_msg void OnBTNObjectMaterial();
	afx_msg void OnBTNENVemmisive();
	afx_msg void OnCHECKENVenableSun();
	afx_msg void OnBTNENVsunColor();
	afx_msg void OnBTNEntityMaterial();
	afx_msg void OnBUTTONtextAddTextCollection();
	afx_msg void OnBTNtextFontSW();
	afx_msg void OnBUTTONtextAddTextObject();
	afx_msg void OnBTNtextActivate();
	afx_msg void OnBTNtextDeactivate();
	afx_msg void OnBUTTONtextTextColor();
	afx_msg void OnBTNtextApplyChanges();
	afx_msg void OnBTNtextGetCurrentInfo();
	afx_msg void OnBTNtextApplyFont();
	afx_msg void OnBUTTONtextAddFont();
	afx_msg void OnBTNtextApplySizeChange();
	afx_msg void OnBUTTONtextDeleteFont();
	afx_msg void OnBUTTONtextDeleteTextCollection();
	afx_msg void OnBUTTONtextDeleteTextObject();
	afx_msg void OnBTNtestSave();
	afx_msg void OnBTNtestLoad();
	afx_msg void OnBTNcamVportSave();
	afx_msg void OnBTNcamVportLoad();
	afx_msg void OnBTNDeleteCamera();
	afx_msg void OnBTNDeleteViewport();
	afx_msg void OnBTNUpdateViewport();
	afx_msg void OnBTNUpdateRuntimeViewport();
	afx_msg void OnBTN2DSaveConfig();
	afx_msg void OnBTN2DLoadConfig();
	afx_msg void OnBTNExplosionDBSW();
	afx_msg void OnBTNEXPAddExplosion();
	afx_msg void OnBTNEXPLoadExplosions();
	afx_msg void OnBTNEXPSaveExplosions();
	afx_msg void OnBTNEXPDeleteExplosion();
	afx_msg void OnBTNCameraAdvanced();
	afx_msg void OnBTNMovieAddMovie();
	afx_msg void OnBTNMovieTestMovie();
	afx_msg void OnBTNMovieEditMovie();
	afx_msg void OnBTNMovieDeleteMovie();
	afx_msg void OnBTNMoviesSaveDB();
	afx_msg void OnBTNMoviesLoadDB();
	afx_msg void OnBTNMovieStopMovies();
	afx_msg void OnBTNControlAddObject();
	afx_msg void OnBTNControlAddEvent();
	afx_msg void OnBTNControlCfgSave();
	afx_msg void OnBTNControlCfgLoad();
	afx_msg void OnBTNControlDeleteObject();
	afx_msg void OnBTNControlDeleteEvent();
	afx_msg void OnSelchangeLISTControlDB();
	afx_msg void OnBTNControlEditObject();
	afx_msg void OnBTNControlEditEvent();
	afx_msg void OnBTNUIBaseAdvanced();
	afx_msg void OnBTNObjectArrayAdd();
	afx_msg void OnBTNENTITYProperties();
	afx_msg void OnBTNOBJECTProperties();
	afx_msg void OnBTNOBJDeleteLight();
	afx_msg void OnBtnObjAddlight();
	afx_msg void OnBTNENTMergeData();
	afx_msg void OnBTNENTAddLight();
	afx_msg void OnBTNUIMusic();
	afx_msg void OnBTNMusicAssignTrack();
	afx_msg void OnBTNMusicAssignWav();
	afx_msg void OnBTNMusicUnassignTrack();
	afx_msg void OnBTNMusicUnassignWav();
	afx_msg void OnBTNMusicBrowseWav();
	afx_msg void OnBTNUITextureAssign();
	afx_msg void OnBTNUIMissileDatabase();
	afx_msg void OnBTNMISAddMissile();
	afx_msg void OnBTNMISBrowseFileName();
	afx_msg void OnBTNMISDeleteMissile();
	afx_msg void OnBTNMISsave();
	afx_msg void OnBTNMISLoad();
	afx_msg void OnBTNUIBulletHole();
	afx_msg void OnBTNBHAddHole();
	afx_msg void OnBTNBHBrowse();
	afx_msg void OnBTNBHAlphaTexture();
	afx_msg void OnBTNBHDeleteHole();
	afx_msg void OnBTNBHload();
	afx_msg void OnBTNBHMaterial();
	afx_msg void OnBTNBHSave();
	afx_msg void OnBTNEntityshadow();
	afx_msg void OnBTNObjectShadow();
	afx_msg void OnBTNENTAnimProperties();
	afx_msg void OnBTNEntitySound();
	afx_msg void OnBTNEntitysetSoundCenter();
	afx_msg void OnBTNOBJSound();
	afx_msg void OnBTNOBJAlphaTexture();
	afx_msg void OnBTNENTAnimSound();
	afx_msg void OnBTNTCPIPJoinHost();
	afx_msg void OnBTNUIMultiplayer();
	afx_msg void OnBTNEntityCombineEntityVis();
	afx_msg void OnBTNENTContentProperties();
	afx_msg void OnBTNTEXAddCollection();
	afx_msg void OnBTNTEXDeleteCollection();
	afx_msg void OnBTNUIAnimTextureDB();
	afx_msg void OnBTNTEXLoadCollection();
	afx_msg void OnBTNTEXSaveCollection();
	afx_msg void OnBTNENTLoadDB();
	afx_msg void OnBTNENTSaveDBSmoothNormals();
	afx_msg void OnBTNENTSaveDB();
	afx_msg void OnButtonSaveANM();
	afx_msg void OnButtonSaveANMU();
	afx_msg void OnBTNENTSaveDBExpanded();
	afx_msg void OnBTNENTRemoveLOD();
	afx_msg void OnBTNENTLoadDBExpanded();
	afx_msg void OnBtnUiScript();
	afx_msg void OnBTNSCRAddScript();
	afx_msg void OnBTNSCRDeleteEvent();
	afx_msg void OnBTNSCRLoadExecFile();
	afx_msg void OnBTNSCRBuild();
	afx_msg void OnBTNSCREditEvent();
	afx_msg void OnBTNOBJEditLight();
	afx_msg void OnBTNENTLoadEntity();
	afx_msg void OnBTNENTSaveEntity();
	afx_msg void OnBTNMSSaveDB();
	afx_msg void OnBTNMSLoadDB();
	afx_msg void OnBtnTxtdbAdd();
	afx_msg void OnBtnTxtdbDelete();
	afx_msg void OnBtnTxtdbEdit();
	afx_msg void OnBtnTxtdbReinitAll();
	afx_msg void OnBtnTxtdbReinitFromDisk();
	afx_msg void OnBtnTxtdbSetSubtraction();
	afx_msg void OnBTNOBJClearTexCoords();
	afx_msg void OnBTNOBJAddTexCoords();
	afx_msg void OnBTNSCRSetToScene();
	afx_msg void OnBTNSCRClearScene();
	afx_msg void OnBTNSCRLoadFromScene();
	afx_msg void OnBTNNTAdvanced();
	afx_msg void OnBTNNTBuildTxtRecord();
	afx_msg void OnBTNUIHudInterface();
	afx_msg void OnBTNHUDAddDisplayUnit();
	afx_msg void OnBTNHUDAddNewHud();
	afx_msg void OnBTNHUDSaveDB();
	afx_msg void OnBTNHUDLoadDB();
	afx_msg void OnSelchangeListHudDb();
	afx_msg void OnBTNHUDDeleteHud();
	afx_msg void OnBTNHUDDeleteDisplayUnit();
	afx_msg void OnBTNHUDReplaceUnit();
	afx_msg void OnBTNENTUpdate();
	afx_msg void OnSelchangeLISTTXTDBDatabase();
	afx_msg void OnCHECKTEXBltOn();
	afx_msg void OnBTNENVFogDay();
	afx_msg void OnCHECKENVAmbientLightOn();
	afx_msg void OnBTNENVambientDayLight();
	afx_msg void OnCHECKENVTimeSystem();
	afx_msg void OnCHECKENVWindRandomizer();
	afx_msg void OnBTNUIParticleSys();
	afx_msg void OnBTNPARTEditSystem();
	afx_msg void OnBTNPARTLoadDatabase();
	afx_msg void OnBTNUISpawnGen();
	afx_msg void OnBTNSpawnAddGen();
	afx_msg void OnBTNSpawnDeleteGen();
	afx_msg void OnBTNSpawnEditGen();
	afx_msg void OnBTNObjectBuildCollisionMap();
	afx_msg void OnSelchangedObjectTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBTNObjectsAddGroup();
	afx_msg void OnBTNObjectsDeleteGroup();
	afx_msg void OnBTNObjectsEditGroup();
	afx_msg void OnSelchangeLISTObjectsGroupList2Box();
	afx_msg void OnDblclkLISTObjectsGroupList2Box();
	afx_msg void OnBTNGroupModifier();
	afx_msg void OnDblclkObjectTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBTNUIMerge();
	afx_msg void OnBTNTCPDisconnectPlayer();
	afx_msg void OnBTNTCPServerLoad();
	afx_msg void OnBTNTCPServerSave();
	afx_msg void OnBTNOptimizeStates();
	afx_msg void OnBTNTCPNewAccount();
	afx_msg void OnBTNTCPDeleteAccount();
	afx_msg void OnBTNOBJAddTrigger();
	afx_msg void OnBTNOBJUpdateObject();
	afx_msg void OnCHECKTCPRegistered();
	afx_msg void OnSelchangeLISTTCPIPCurrentPlayers();
	afx_msg void OnBTNTXSaveTextureBankStatic();
	afx_msg void OnBTNTXSaveTextureBankDynamic();
	afx_msg void OnBTNTXLoadTextureBank();
	afx_msg void OnBTNOBJGetSignificant();
	afx_msg void OnBTNOBJTextureSetMacro();
	afx_msg void OnBTNAICreateNew();
	afx_msg void OnBtnUiAi();
	afx_msg void OnBTNAIDelete();
	afx_msg void OnBtnObjLodadd();
	afx_msg void OnBTNOBJRemoveLODLevel();
	afx_msg void OnBTNOBJEditLOD();
	afx_msg void OnBTNENTITYAnimations();
	afx_msg void OnBTNENTAppendCustomDB();
	afx_msg void OnSelchangeLISTENTITYRuntimeList();
	afx_msg void OnBTNUIHaloEffects();
	afx_msg void OnBTNAddHalo();
	afx_msg void OnBTNDeleteHalo();
	afx_msg void OnBTNEditHalo();
	afx_msg void OnBTNHaloLoad();
	afx_msg void OnBTNHaloSave();
	afx_msg void OnBTNAddLensFlare();
	afx_msg void OnBTNEditLensFlare();
	afx_msg void OnBTNDeleteLensFlare();
	afx_msg void OnBTNGenerateMesh();
	afx_msg void OnBTNENTLODSystems();
	afx_msg void OnBTNENTDelLOD();
	afx_msg void OnBTNENTUpdateVisuals();
	afx_msg void OnBTNENTResourceBase();
	afx_msg void OnBTNENTViewRuntimeStats();
	afx_msg void OnBTNENTViewRuntimeResources();
	afx_msg void OnSelchangeLISTMISmissileDataBase();
	afx_msg void OnBTNMISReplaceValues();
	afx_msg void OnBTNMISEditVisual();
	afx_msg void OnBTNMISAddBillboardMissile();
	afx_msg void OnBTNMISEditDynamics();
	afx_msg void OnBTNENTEditStartInven();
	afx_msg void OnDropdownCMBOCGTEXFontTableTexture();
	afx_msg void OnBTNCGTEXAdd();
	afx_msg void OnBTNCGTEXDelete();
	afx_msg void OnBTNAIAddEvent();
	afx_msg void OnBTNAIAddScript();
	afx_msg void OnBTNAIEditScript();
	afx_msg void OnBTNAIDeleteScript();
	afx_msg void OnBTNAIEditEvent();
	afx_msg void OnBTNAIDeleteEvent();
	afx_msg void OnBTNAIAddTree();
	afx_msg void OnBTNAIDeleteTree();
	afx_msg void OnBTNAIEdit();
	afx_msg void OnBTNAISaveAiData();
	afx_msg void OnBTNAILoadAiData();
	afx_msg void OnSelchangeLISTAIScripts();
	afx_msg void OnBTNENTSaveAppendages();
	afx_msg void OnBTNENTLoadAppendages();
	afx_msg void OnBtnEntMount();
	afx_msg void OnBtnEntDemount();
	afx_msg void OnBTNEXPEditExplosionObj();
	afx_msg void OnChangeEDITTCPIPadress();
	afx_msg void OnChangeTCPuserName();
	afx_msg void OnChangeTCPpassword();
	afx_msg void OnBTNSystemGoingDown();
	afx_msg void OnBTNENTAddItem();
	afx_msg void OnBTNENTItemTest();
	afx_msg void OnBTNENTDeleteItem();
	afx_msg void OnBTNENTDisarmItem();
	afx_msg void OnBTNENTEditItem();
	afx_msg void OnBTNOBJClone();
	afx_msg void OnBTNOBJTransformation();
	afx_msg void OnBTNOBJBoundingGroupSet();
	afx_msg void OnBTNOBJLibraryAccess();
	afx_msg void OnBTNENVTimeSystem();
	afx_msg void OnBTNMISAddSkeletalMissile();
	afx_msg void OnBTNMISAddLaser();
	afx_msg void OnSelchangeEquipItem();
	afx_msg void OnSelchangeEquippedItemMeshSlot();
	afx_msg void OnSelchangeEquippedItemMesh();
	afx_msg void OnSelchangeEquippedItemMaterial();
	afx_msg void OnBTNAIEditAITree();
	afx_msg void OnBTNTCPSupportedWorlds();
	afx_msg void OnBTNENTEditDeathCfg();
	afx_msg void OnBTNTCPViewAccount();
	afx_msg void OnCHECKTXBBitmapLoadOverride();
	afx_msg void OnBTNUISkillBuilder();
	afx_msg void OnBTNOBJAddNode();
	afx_msg void OnBTNOBJReferenceLibrary();
	afx_msg void OnBTNUISoundDB();
	afx_msg void OnBTNUIGlInventoryDB();
	afx_msg void OnBtnUiCurrencysys();
	afx_msg void OnBTNUICommerceDB();
	afx_msg void OnBTNUIBankZones();
	afx_msg void OnBtnUiSafezones();
	afx_msg void OnBTNENTUnifySleletalMesh();
	afx_msg void OnBTNMISDeleteLight();
	afx_msg void OnBTNMISLightSys();
	afx_msg void OnBtnUiStatdb();
	afx_msg void OnBTNTCPItemGeneratorDB();
	afx_msg void OnBtnPatchsys();
	afx_msg void OnBTNTCPUpdateCurrentOnlinePlayers();
	afx_msg void OnBTNENTScaleEntity();
	afx_msg void OnBTNENTScaleRuntimeEntity();
	afx_msg void OnCHECKSPNEnableSystem();
	afx_msg void OnBTNTXBSetMipLevels();
	afx_msg void OnBTNTCPUpdateAllAcounts();
	afx_msg void OnBTNENTReplaceEntityWSIE();
	afx_msg void OnBTNTCPIPBlocking();
	afx_msg void OnBtnUiWaterzones();
	afx_msg void OnBtnUiPortalzonesdb();
	afx_msg void OnBTNUIElementDB();
	afx_msg void OnBTNTCPUpgradeWorm();
	afx_msg void OnBTNEXPSaveUnit();
	afx_msg void OnBTNEXPLoadUnit();
	afx_msg void OnBTNTCPDisconnectAll();
	afx_msg void OnBTNTCPPrintReports();
	afx_msg void OnBTNAILoadAiUnit();
	afx_msg void OnBTNAISaveAiUnit();
	afx_msg void OnBTNAIImportCollisionSys();
	afx_msg void OnBTNAIDeleteSys();
	afx_msg void OnBTNAIExportCurrentCol();
	afx_msg void OnBTNAISaveColDB();
	afx_msg void OnBTNAILoadColDB();
	afx_msg void OnBTNMISSaveUnit();
	afx_msg void OnBTNMISLoadUnit();
	afx_msg void OnBTNSPNExportSys();
	afx_msg void OnBTNSPNImportSys();
	afx_msg void OnBTNAIIntegrityCheck();
	afx_msg void OnBTNTCPSearchForAccount();
	afx_msg void OnBTNTCPRestoreBackup();
	afx_msg void OnBTNTXTSymbolDB();
	afx_msg void OnCHECKEditorTxtMode();
	afx_msg void OnBTNENTMassFormatNames();
	afx_msg void OnCHKOBJEnablePlaementHotKey();
	afx_msg void OnBTNOBJReportCurRenderCache();
	afx_msg void OnBTNUIIconDBACCESS();
	afx_msg void OnBTNTCPSnapShotPacketQue();
	afx_msg void OnBTNTCPGenerateLogBasedOnTime();
	afx_msg void OnBTNUIHousingSys();
	afx_msg void OnBTNUIHszoneDB();
	afx_msg void OnBTNChkshowSpeed();
	afx_msg void OnBTNAInewTree();
	afx_msg void OnBTNTCPExportAccount();
	afx_msg void OnBTNTCPImportAccount();
	afx_msg void OnBTNTCPGenerateClanDatabase();
	afx_msg void OnBTNTCPClearInactive();
	afx_msg void OnBTNUIAIRainSys();
	afx_msg void OnBTNENTsaveItemDB();
	afx_msg void OnBTNENTLoadItemDB();
	afx_msg void OnBTNSPNAppendSys();
	afx_msg void OnButtonLoad();
	afx_msg void OnBTNTestAssign();
	afx_msg void OnBTNCommCntrlLibBtnA();
	afx_msg void OnBTNWorldRule();
	afx_msg void OnBTNArenaManager();
	afx_msg void OnObjectSelect();
	afx_msg void OnObjectMove();
	afx_msg void OnObjectRotate();
	afx_msg void OnObjectScale();
	afx_msg void OnSelchangeViewports();
	afx_msg void OnSelchangeRuntimeViewports();
	afx_msg void OnBTNDeleteGuids();
	afx_msg void OnSelectionChangeEntityMeshSlot();
	afx_msg void OnSelectionChangeEntityMeshConfig();
	afx_msg void OnSelectionChangeEntityMaterialConfig();
	//}}AFX_MSG
protected:
	void OnResetAll();
	void OnChangeDialog();

protected:
	typedef KEP::ReferencePointer<CDialog> CDialogPointer;
	typedef std::set<CDialogPointer> CDialogContainer;

	// === DIALOGS Instances ===
	CCurrentWorldsDlg* m_currentWldsSpawnPointModule;
	CAccountViewerModule* m_accountViewerModule;
	CRuntimeEntityViewerDlg* m_entityViewerModule;
	CNewAccountsModule* m_accountModule;
#ifndef _ClientOutput
	CNodeImportDlg m_nodeImporterModule;
#endif
	CObjectsDialog m_dlgBarObject;
	CDialogBar m_wndDlgBar; // Is this really used right now?
	CBackgroundDialog m_backgroundDlgBar; // Does not seem of much use, especially if we remove the background stuff
	CActorsDialog m_entityDlgBar;
	CCamerasViewportsDialog m_camVptDlgBar;
	CDialogBar m_mouseDlgBar;
	CDialogBar m_2DinterfaceBar; // this one is also hardly used I think
	CTextFontDialog m_textFontDlgBar;
	CExplosionsDialog m_explosionDlgBar;
	CProjectileSystemsDialog m_missileDlgBar;
	CDialogBar m_bulletHoleDlgBar;
	CScriptDialog m_scriptDlgBar;
	CTextureDialog m_textureDBDlgBar;
	CDialogBar m_hudDBDlgBar;
	CParticleSystemDialog m_particleDBDlgBar;
	CSpawnGeneratorDialog m_spawnGenDBDlgBar;
	CAIDialog m_aiDBDlgBar;
	CMusicDialog m_musicDlgBar;
	// ___ END DIALOGS ___

	CStringA* m_textureSearchPath;
	int m_searchPathCount;
	bool m_creatingDistribution; // for later determining if in distro

	CImageList m_worldObjectTreeImages;
	Game m_game;

private:
	CKEPGameFile* m_fontsTextFilePtr;
	CKEPGameFile* m_aiFilePtr;
	CKEPGameFile* m_entityFilePtr;
	CKEPGameFile* m_explosionFilePtr;
	CKEPGameFile* m_cameraViewFilePtr;
	CKEPGameFile* m_musicFilePtr;
	//CKEPGameFile * m_textureFilePtr;
	CKEPGameFile* m_missileFilePtr;

	CKEPGameFile* m_arenaFilePtr;
	CKEPGameFile* m_elementFilePtr;
	CKEPGameFile* m_iconsFilePtr;
	CKEPGameFile* m_skillsFilePtr;
	CKEPGameFile* m_worldRulesFilePtr;
	CKEPGameFile* m_aiTriggeredSpawnFilePtr;
	CKEPGameFile* m_npcFilePtr;
	CKEPGameFile* m_bankFilePtr;
	CKEPGameFile* m_commereceAreaFilePtr;
	CKEPGameFile* m_vertexShadersFilePtr;
	CKEPGameFile* m_pixelShadersFilePtr;
	IKEPGameFile* m_globalInventoryFilePtr;
	CKEPGameFile* m_housingSystemFilePtr;
	CKEPGameFile* m_housingAreaFilePtr;
	CKEPGameFile* m_portalAreaFilePtr;
	CKEPGameFile* m_safeAreaFilePtr;
	CKEPGameFile* m_soundsFilePtr;
	CKEPGameFile* m_waterAreaFilePtr;
	CKEPGameFile* m_statsFilePtr;
	CKEPGameFile* m_dynamicObjectsFilePtr;
	CObList* m_altGlobalInventory;
	CWldObjectList* m_worldListTemp; //temp object list
	CServerItemGenObjList* m_serverItemGenerator;

	// === MISC ===
	void QuestDataSQLOutput();
	void SpawnGenDataSQLOutput();
	DWORD SqlItemUpdateToServer();
	bool updateBinaries(CWnd* parent, IN const char* gameDir, IN const char* binDir);

	void WorldNormalModifier(int process, int group);
	BOOL LoadWorldObject(CStringA fileName, CWldObject** retLoc, BOOL fromUI = FALSE);
	BOOL ReplaceObjectByName(CStringA objectNameBasis, CStringA path);

	CStringA ParseData(unsigned int& msg);
	void OutputFormat();
	void FormatAllNames();
	void PlaceFirstActor();
	void LoadRNEFile();
	// --- MISC InList stuff ---
	//void InListClickZones(int sel);
	void LoadSelectedMissile();
	CMissileObj* GetSelectedMissileObj();
	CMissileObj* GetMissileObj(int selection);
	void MissileDatabaseInList(int sel);

	void InitializeEntityConfigurationsCombo(int selection);
	void InitializeEntityMeshConfigurationsCombo(int meshSelectionIndex, int materialSelectionIndex, CDeformableMeshList* pDeformableMeshList);
	void InitializeEntityMaterialConfigurationsCombo(int materialSelectionIndex, CObList* pMaterialList);

	void InListEntityDefcfgs(int sel);
	void InitializeEquippedItemMeshSlot(int selectedMeshSlotIndex);
	void InitializeEquippedItemMesh(CAlterUnitDBObject* pAlterUnitObj, int selectedMeshIndex, int selectedMaterialIndex);
	void InitializeEquippedItemMaterial(CObList* pMaterialList, int selectedMaterialIndex);

	void InListRuntimeEntities(int sel);
	void EnitityInList();
	void VisualPropertiesInList();
	void WorldGroupInList(int sel);
	void VportCameraInList();
	void InListExplosionDB(int sel);
	void InitializeControlDB();
	void ScriptInList(CEXScriptObjList* ptrLoc);
	void ScriptInList(int sel);
	void InListTextureDB(const DWORD& nameHash);
	void InListDisplayUnits();
	void InListHud();
	void ParticleDatabaseInList(int sel);
	void SpawnGenDatabaseInList(int sel);
	void InListCollisionDB(int sel);
	void InListMissileSkillCriteria(int sel);
	void InListMisChannels(int sel);
	void AITreeInList(int sel);
	void AI_DBInList(int sel);
	void InListAccounts(int sel);

	// === TOOLBAR Functions ===
	void CloseAllToolDialogs();
	void OpenToolBar(CDialogBar* barLoc);

	// === TREE Helper Functions ===
	int GetTreeSelectionLevel(UINT id, CDialogBar* barLoc);
	void ObjectTreeViewInlist(int groupNum, int traceNum); //belongs to base objects
	void DeactivateEntity(int runtimeIndex);
	int GetRuntimeEntityIndexFromEntityObject(CMovementObj* entityObj);
	BOOL SetCursorToSelectedObject(int trace);
	CWldObject* GetSelectedWldObjectFromTreeView(void);

	// === TEXT Display Stuff ===
	void CreateTextCollection(CStringA CollectionName, int timeOn, int fontSelection, COLORREF color);
	BOOL AddTextEntry(int collectionIndex, CStringA text, int percX, int percY);
	BOOL ActivateTextDisplay(int collectionIndex);
	BOOL DeActivateTextDisplay(int collectionIndex);
	void DeActivateAllTextDisplays();
	// --- FONT ---
	void AddFont(int sizeX, int sizeY);
	void InitAllFonts();
	BOOL SaveTextFontInterfaceFromFile(CStringA fileName);
	void TextFontInterfaceInlist();
	void InListCGFonts(int sel);

	// === CONTROLS ===
	void AddControlConfig(CStringA configName, ControlObjType const& controlType);

	void AddControllerEvent(ControlDBObj* controller, ControlObjType const& controlType,
		int keyLoc, Command_Action const& eventAction, CStringA description,
		float miscFloat1, float miscFloat2, float miscFloat3);

	void InListControlEvents();
	void UpdateControlList();

	// === SCRIPT ===
	void ScriptEventsInList(int sel);
	bool ProcessScriptAttribute(CEXScriptObj* sctPtr);
	virtual bool InitEvents();

	// === OBJECT and GROUP Operations ===
	void WorldObjectProperties();

	//! \brief Delete world object from WldObjectList and item from tree.
	//! \param hItem Tree item selected for deletion.
	//! \return FALSE if item not found in WldObjectList. TRUE otherwise.
	//!
	//! Delete world object from WldObjectList and item from tree. Delete the entire group if the selected
	//! item is a mesh group. In addition, delete mesh group from tree if all its children have been removed.
	BOOL DeleteWorldObjectByTreeItem(HTREEITEM hItem);

	void UnionVertexesPositionalyByTolerance(float tolerance, int group);
	// --- Bounding Box ---
	BNDBOX BoundingBoxGroupSet(int group, float maxBumpX, float maxBumpY,
		float maxBumpZ, float minBumpX, float minBumpY, float minBumpZ);
	// === NODE ===
	void AddNodeByData(int groupIndex, int selection, CMovementObj* entityPtr, Matrix44f* overrideMatrix);

	// === BACKGROUND ===
	BOOL CreateBackGroundObject(CStringA bitmapName, int xMax, int yMax, int startX, int startY, RECT usageRect, BOOL movingMap);
	void BackGroundScrollRight(int pixelIndex, int listIndex);
	void BackGroundScrollLeft(int pixelIndex, int listIndex);
	void BackGroundScrollDown(int pixelIndex, int listIndex);
	void BackGroundScrollUp(int pixelIndex, int listIndex);

	void RegisterGameFiles();

	// Save
	BOOL SaveScene(CStringA fileName);
	BOOL SaveSceneObjects(CStringA fileName);

	BOOL AppendScene(CStringA fileName, int aiMergeMode = 1,
		int groupMode = -1, int transitionGroupID = 0);

	virtual void RenderTempListPreviews(Matrix44f camGeneratedMatrix, Matrix44f invCamGeneratedMatrix, Vector3f pointLocation, CCameraObj* cameraPtr, Matrix44f camMatrix);
	void DeleteObjectByTrace(int trace);

	// == BoundingBox drawing == // not used now
protected:
	void BoundingBoxesRender();
	void DrawBox(LPDIRECT3DDEVICE9 pd3dDevice, Vector3f max, Vector3f min);

protected:
	BOOL GL_showBoundingBoxs;

private:
	void ObjectsDialogGroupModifier();
	bool GetClientDistributionFiles(HANDLE distCloseEvent, HANDLE closeEvent, INOUT strVect& files,
		INOUT dirVect& dirs, IN const char* baseDir, IN const char* exportSqlPath,
		IN EDBExport exportToDb, IN bool tristripAssets,
		OUT ULONG& coreClientFileCount, OUT dependVect& zoneFiles,
		IN bool menusOnly);
	bool GetServerDistributionFiles(HANDLE distCloseEvent, HANDLE closeEvent, INOUT strVect& files,
		INOUT dirVect& serverDirs, IN const char* baseDir, IN const char* exportSqlPath,
		IN EDBExport exportToDb);

public:
	void GetPlatformInstallLocation(CStringA& platformInstallLocation);
	void GetDistributionDirectory(CStringA& destinationPath, CStringA patchFolderLocation, bool useSuppliedPath = false);
	void CreateFullDistribution(HWND dialogHwnd, const char* encryptKey, CStringA gameId, CStringA licenseKey,
		CStringA destinationPath, CStringA patchFolderLocation, HANDLE closeEvent, HANDLE distCloseEvent, EDBExport exportToDb,
		bool tristripAssets, bool encryptassets, bool skipdialog, CRCLIST* oldpatchData, int oldFileCount,
		bool menusOnly);

	DECLARE_GETSET

	//======================================
	// EDITOR: Dialogs
	//======================================
protected:
	// moved from ClientEngine.h
	CControlsDialog m_controlsDlgBar;
	CMoviesDialog m_moviesDlgBar;
	CEnvironmentDialog m_enviormentBar;
	CDialogBar m_multiPlayer;
	CAnimatedTexturesDialog m_animatedTextureDBDlgBar;

	virtual void OnRuntimeMovieListChanged();
	virtual void OnTextureSwapListChanged();
	virtual void OnEnvironmentChanged();
	virtual void OnControlDBListChanged();

public:
	static EVENT_PROC_RC ClickEventHandler(IN ULONG lparam, IN IDispatcher* disp, IN IEvent* e);
	static EVENT_PROC_RC SelectEventHandler(IN ULONG lparam, IN IDispatcher* disp, IN IEvent* e);
	static EVENT_PROC_RC WorldObjectSelectedHandler(IN ULONG lparam, IN IDispatcher* disp, IN IEvent* e);
};

#ifndef _DEBUG // debug version in KEPEditView.cpp
inline CKEPEditDoc* CKEPEditView::GetDocument() const {
	return reinterpret_cast<CKEPEditDoc*>(m_pDocument);
}
#endif

template <class T>
///---------------------------------------------------------
// Helper template to load serializations that have been
// segmented from the main SVR file.
//
// [in] fileName
// [out] obj
//
// [Returns]
//    TRUE if worked
BOOL LoadSVRPart(IN const char* fileName, OUT T& obj) {
	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(fileName, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	obj->SafeDelete();

	BEGIN_SERIALIZE_TRY
	// end destroy list that will be filled
	CArchive the_in_Archive(&fl, CArchive::load);
	the_in_Archive >> obj;

	the_in_Archive.Close();
	fl.Close();
	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

template <class T, class V>
///---------------------------------------------------------
// Helper template to save serializations that have been
// segmented from the main SVR file.
//
// [in] fileName
// [in] obj to serialize
// [in] obj the GL_multiplayerObject
//
// [Returns]
//    TRUE if worked
BOOL SaveSVRPart(IN const char* fileName, IN T& obj, IN V& mutiplayerObject) {
	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(fileName, CFile::modeCreate | CFile::modeWrite, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	CArchive the_in_Archive(&fl, CArchive::store);

	EnterCriticalSection(&mutiplayerObject->m_recieveCriticalSection);
	mutiplayerObject->m_accountDatabase->FlagAllForOptimizedSave();

	BEGIN_SERIALIZE_TRY
	the_in_Archive << obj;
	END_SERIALIZE_TRY_NO_LOGGER

	LeaveCriticalSection(&mutiplayerObject->m_recieveCriticalSection);
	the_in_Archive.Close();
	fl.Close();

	return TRUE;
}
