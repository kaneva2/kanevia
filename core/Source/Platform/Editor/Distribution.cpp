///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "KEPEdit.h"

#include "CWorldAvailableClass.h"
#include <direct.h>
#include <shlobj.h>
#include <jsEnumFiles.h>
#include "DistributionUpdateGlobals.h"
#include "makedeploy.h"
#include "Core/Crypto/Crypto_MD5.h"

#include "dbExportHelper.h"
#include "TextureManager.h"
#include "DynamicObjectManager.h"
#include "CArmedInventoryClass.h"
#include "CBoneClass.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "CParticleEmitRange.h"
#include "CBoneAnimationNaming.h"
#include "SkeletonConfiguration.h"
#include "UnicodeMFCHelpers.h"
#include "CShadowClass.h"

#include "Common\KepUtil\kpack.h"

#include "Common\KEPUtil\DistributionInfo.h"

#include "TinyXML/tinyxml.h"

static LogInstance("Instance");

typedef vector<pair<string, bool>> strVect;
typedef vector<string> dirVect;
typedef vector<string> subDirToUse;
typedef vector<string> directoryList;
typedef vector<pair<string, string>> finalTexturePacketList;
typedef vector<CItemObj*> CustomTextureItemList;

CustomTextureItemList gListOfCustomTexuredItems;
directoryList gListOfTextureDirs;
finalTexturePacketList gFinalTexurePackage;

string gCurrentSubDirectory = "";

#define GAMEFILES "GameFiles"
#define SOUNDS "sounds"
#define MAPSMODELS "MapsModels"

extern int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData);

UINT gMsgId = ::RegisterWindowMessageW(L"KEI_DISTRIBUTION_STATUS_MSG");
HWND gProgressHwnd;

// Methods to traverse an object that ultimately contains some sort of reference
// to a CEXMesh object. The name of the texture file from this CEXMesh object
// will be used to determine if the mapsmodels file needs to be copied. It will
// be copied if:
//
//	it is contained in a one of the global objects that is parsed
//  it is contained in an object loaded by one of the game's zone scripts
//  it is not in the texture database
//
// The hieharchy of these objects and their members were determined by manaully
// walking the code base. The processscriptattribute call of the engine, the client base,
// and the client were all parsed. Comments have been added to the processscriptattribute call
// in these locations to warn the user that additional code placed in these calls should
// be accounted for in the following methods if a mesh is introduced.

static void Mesh_GlobalObjectDump(strVect& meshes);
static void Mesh_LoadSceneScriptLocal(strVect& meshes, int action, const char* fileName, Logger& logger);
static void Mesh_EnvironmentObjDump(strVect& meshes, CEnvironmentObj* environmentPtr);
static void Mesh_MeshObjDump(strVect& meshes, CEXMeshObj* meshObj);
static void Mesh_MaterialObjDump(strVect& meshes, CDeformableMesh* pMesh);
static void Mesh_SkeletalTextureDump(strVect& meshes, RuntimeSkeleton* skeletonObj, SkeletonConfiguration* skeletonCfg);
static void Mesh_SkeletalTextureDump(strVect& meshes, RuntimeSkeleton* skeletonObj, SkeletonConfiguration* skeletonCfg, strVect& core);
static void Mesh_CollisionObjDump(strVect& meshes, CCollisionObj* collisionObj);
static void Mesh_EquippableSystemDump(strVect& meshes, EquippableRM* eqSystem);
static void Mesh_MovementObjDump(strVect& meshes, CMovementObj* movementObj);
static void Mesh_MovementObjDump(strVect& meshes, CMovementObj* movementObj, strVect& core);
static void Mesh_MeshObjListDump(strVect& meshes, CEXMeshObjList* meshObjList);
static void Mesh_HousingDBDump(strVect& meshes, CHousingObjList* housingObjList);
static void Mesh_ExplosionDBDump(strVect& meshes, CExplosionObjList* explosionObjList);
static void Mesh_MissileObjDump(strVect& meshes, CMissileObj* missileObj);
static void Mesh_MissileDBDump(strVect& meshes, CMissileObjList* missileObjList);
static void Mesh_GlobalInventoryDump(strVect& meshes, CGlobalInventoryObjList* globalEntityObjList);
static void Mesh_EntityObjectDump(strVect& meshes, CMovementObjList* entityObjList, strVect& core);
static void processCharacterTexture(string texName, strVect& core, const char* baseDir);
static void processCoreCharacterTextures(strVect& core, const char* baseDir);
static void Texture_MovementObjDump();

///---------------------------------------------------------
// mkdirReportErr
//
// Wraps the mkdir function and reports the error if one occurs
//
// [Returns]
//  false - error
//  true - success
static bool mkdirReportErr(const wchar_t* dir) {
	bool isDir = true;
	if (jsFileExists(Utf16ToUtf8(dir).c_str(), &isDir) && isDir)
		return true;

	bool ret = true;
	int mkret = _wmkdir(dir);
	if (mkret != 0) {
		ret = false;
		CStringW msg;
		msg.Format(IDS_CREATE_DIST_DIR_FAILED, dir, errno);
		AfxMessageBox(msg, MB_ICONEXCLAMATION);
	}

	return ret;
}

static bool mkdirReportErr(const char* dir) {
	return mkdirReportErr(Utf8ToUtf16(dir).c_str());
}

static void addStringNoDupes(strVect& files, const string& newString, bool encrypted) {
	string newStringToAdd;

	if (gCurrentSubDirectory != "") {
		newStringToAdd = newString + '$' + gCurrentSubDirectory;

		for (auto i = files.begin(); i != files.end(); i++) {
			if (STLCompareIgnoreCase(i->first.c_str(), newStringToAdd.c_str()) == 0)
				return;
		}

		files.push_back(make_pair(newStringToAdd, encrypted));
	} else {
		for (auto i = files.begin(); i != files.end(); i++) {
			if (STLCompareIgnoreCase(i->first.c_str(), newString.c_str()) == 0)
				return;
		}

		files.push_back(make_pair(newString, encrypted)); // not found
	}
}

static void addStringNoDupes(dirVect& files, const string& newString) {
	for (auto i = files.begin(); i != files.end(); i++) {
		if (STLCompareIgnoreCase(i->c_str(), newString.c_str()) == 0)
			return;
	}
	files.push_back(newString); // not found
}

static void getExtraFilesFromXml(const char* baseDir, const string& fname, strVect& files, dirVect& clientDirs) {
	TiXmlDocument dom;
	if (dom.LoadFile(fname)) {
		TiXmlElement* root = dom.FirstChildElement("Files");
		if (root) {
			TiXmlNode* node;
			TiXmlElement* child = 0;
			while ((node = root->IterateChildren("File", child)) != 0 && (child = node->ToElement()) != 0) {
				TiXmlText* miscStr = TiXmlHandle(child).FirstChild().Text();
				string s;
				s = miscStr->Value();
				addStringNoDupes(files, s, false);
			}
			child = NULL;
			while ((node = root->IterateChildren("Dir", child)) != 0 && (child = node->ToElement()) != 0) {
				TiXmlText* miscStr = TiXmlHandle(child).FirstChild().Text();
				string s;
				s = miscStr->Value();
				addStringNoDupes(clientDirs, s);
			}
			//child = NULL;
			//while ( (node = root->IterateChildren( "Pak", child )) != 0 && (child = node->ToElement()) != 0 )
			//{
			//    TiXmlText *miscStr = TiXmlHandle(child).FirstChild().Text();
			//    kstring s;
			//    s = miscStr->Value();
			//	addStringNoDupes( files, s, false);
			//	addStringNoDupes( paks, s);
			//}
			return;
		}
	}
	throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
}

static void getFilesFromXml(const char* gameDir, const string& fname, strVect& files) {
	CEXScriptObjList list;
	FileName fileName(fname);
	list.SerializeFromXML(fileName);

	for (POSITION pos = list.GetHeadPosition(); pos != NULL;) {
		CEXScriptObj* so = (CEXScriptObj*)list.GetNext(pos);

		bool isDir = false;
		if (jsFileExists(PathAdd(gameDir, so->m_miscString).c_str(), &isDir) && !isDir) {
			// This removes the DOL from the distro and replaces it with the dobs from extraclientfiles.
			// Also now removed APD and replaces it with DNG files.
			if (so->m_actionAttribute != LOAD_DYNAMIC_OBJECTS && so->m_actionAttribute != LOAD_GLOBAL_ATTACHMENTDB) {
				addStringNoDupes(files, PathAdd(GAMEFILES, so->m_miscString), (so->m_actionAttribute != LOAD_PARTICLE_SYSTEMS));
			}
		}
	}
}

static void getFilesFromSupportedWorlds(const char* gameDir, CWorldAvailableObjList* currentWorldsUsed, strVect& files, dependVect& worldFiles) {
	if (!currentWorldsUsed)
		return;

	for (POSITION pos = currentWorldsUsed->GetHeadPosition(); pos != NULL;) {
		CWorldAvailableObj* wa = (CWorldAvailableObj*)currentWorldsUsed->GetNext(pos);
		addStringNoDupes(files, PathAdd(GAMEFILES, wa->m_worldName), true);
		zoneFileVect newFile;
		newFile.push_back(make_pair(wa->m_worldName.GetString(), OPEN_SCENE));
		worldFiles.push_back(newFile);
	}
}

static void getFilesWithExt(strVect& inFiles, const char* ext, strVect& files) {
	files.clear();
	size_t extLen = ::strlen(ext);

	for (auto i = inFiles.begin(); i != inFiles.end(); i++) {
		size_t iLen = i->first.length();

		const char* first = i->first.c_str();

		int index = i->first.find_last_of('.');

		const char* firstExt = first + index;

		if (iLen > extLen && ::_strnicmp(firstExt, ext, 4) == 0)
			addStringNoDupes(files, first, false);
	}
}

// Processes the given zone script to determine references to CEXMesh objects in order
// to obtain the list of textures needed to be copied for a distribution.
static void LoadSceneScriptLocal(strVect& meshes, int action, const char* fileNameStr, Logger& logger) {
	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return;

	string fileName = fileNameStr ? fileNameStr : "";

	switch (action) {
		case LOAD_GLOBAL_INVENTORY:
		case LOAD_PARTICLE_SYSTEMS:
		case LOAD_AI_COLLISION_DATABASE:
			// DEPRECATED
			break;

		case LOAD_ENTITY_DATABASE: { // 14
			CMovementObjList* entObjList;
			entObjList = new CMovementObjList;

			if (!SerializeFromFile(fileName, entObjList, logger))
				return;

			EquippableRM* equippableRM = pICE->GetEquippableRM();
			Mesh_EquippableSystemDump(meshes, equippableRM);
			Mesh_EntityObjectDump(meshes, entObjList, meshes); // Is this even used?????

			POSITION posLoc;
			if (entObjList) {
				for (posLoc = entObjList->GetHeadPosition(); posLoc != NULL;) {
					// visual loop
					CMovementObj* mPtr = (CMovementObj*)entObjList->GetNext(posLoc);
					mPtr->SafeDelete();
					delete mPtr;
					mPtr = 0;
				}
			}

			entObjList->RemoveAll();
			delete entObjList;
			entObjList = 0;
		}

		break;

		case LOAD_MISSLE_DATABASE: { // 2
			CMissileObjList* missileDB;
			missileDB = new CMissileObjList;

			if (!SerializeFromFile(fileName, missileDB, logger))
				return;

			Mesh_MissileDBDump(meshes, missileDB);

			if (missileDB) {
				for (POSITION posLoc = missileDB->GetHeadPosition(); posLoc != NULL;) {
					CMissileObj* mPtr = (CMissileObj*)missileDB->GetNext(posLoc);
					SafeDeleteList(mPtr);
				}

				missileDB->RemoveAll();
				delete missileDB;
			}
		} break;

		case LOAD_EXPLOSION_DATABASE: { // 3
			CExplosionObjList* explosionDBList;
			explosionDBList = new CExplosionObjList;

			if (!SerializeFromFile(fileName, explosionDBList, logger))
				return;

			Mesh_ExplosionDBDump(meshes, explosionDBList);

			if (explosionDBList)
				SafeDeleteList(explosionDBList);
		} break;

		case LOAD_HOUSING: { // 42
			CHousingObjList* housingDB;
			housingDB = new CHousingObjList();

			if (!SerializeFromFile(fileName, housingDB, logger))
				return;

			Mesh_HousingDBDump(meshes, housingDB);

			if (housingDB)
				SafeDeleteList(housingDB);
		} break;

		case LOAD_GLOBAL_ATTACHMENTDB: { // 54
			CArmedInventoryList* globalArmedInvenList;
			globalArmedInvenList = new CArmedInventoryList();

			if (!SerializeFromFile(fileName, globalArmedInvenList, logger))
				return;

			EquippableRM* equippableRM = pICE->GetEquippableRM();
			Mesh_EquippableSystemDump(meshes, equippableRM);

			if (globalArmedInvenList)
				SafeDeleteList(globalArmedInvenList);
		} break;

		case LOAD_COMM_CONTROLS: { // 44
			CCommonCtrlList* commCtrlDB;
			CFile fl;

			CArchive the_in_Archive(&fl, CArchive::load);

			the_in_Archive >> commCtrlDB;

			if (commCtrlDB) {
				commCtrlDB->SafeDelete();
				delete commCtrlDB;
				commCtrlDB = 0;
			}
		} break;
	}
}

class ZoneObjects : public GetSet {
public:
	CWldObjectList* WldObjList;
	LONG zoneIndex;

	DECLARE_GETSET
};

static void getFilesFromExg(const char* gameDir, const char* destinationPath, const char* exgName, LONG zoneIndex, strVect& files,
	Logger& logger, strVect& meshes, CKEPEditView* view, EDBExport exportToDb, zoneFileVect& currentWorld) {
	// load the EXG, so we can get its scene

	// end use gamefiles folder for organization
	CFileException exc;
	CFile fl;
	CStringA fileName = PathAdd(gameDir, exgName).c_str();
	BOOL res = OpenCFile(fl, fileName, CFile::modeRead | CFile::shareDenyWrite, &exc);
	if (res == FALSE)
		throw new CFileException(exc.m_cause, exc.m_lOsError, exc.m_strFileName);

	CWldObjectList* WldObjList = 0;
	CBackGroundObjList* BackGroundList = 0;
	CEnvironmentObj* EnvironmentPtr = 0;
	CPersistObjList* GL_ScenePersistList = 0;
	int currentTrack;
	CStringA musicLoopFile;
	int m_globalWorldShader;
	BOOL W_BufferPreffered;
	CEXScriptObjList* SceneScriptExec = 0;

	// end preliminary wrapper
	CArchive the_in_Archive(&fl, CArchive::load);

	the_in_Archive >>
		WldObjList >>
		BackGroundList >>
		EnvironmentPtr >>
		GL_ScenePersistList >>
		currentTrack >>
		musicLoopFile >>
		m_globalWorldShader >>
		W_BufferPreffered >>
		SceneScriptExec;

	the_in_Archive.Close();
	fl.Close();

	if (WldObjList) {
		CStringA fileShort = fileName.Right(fileName.GetLength() - fileName.ReverseFind('\\') - 1);

		int fileShortExt = fileShort.Find('.');

		CStringA fileHandle = fileShort.Left(fileShortExt);

		const char* charbuff = fileHandle.GetString();

		gCurrentSubDirectory = charbuff;

		gListOfTextureDirs.push_back(gCurrentSubDirectory);

		for (POSITION posLoc = WldObjList->GetHeadPosition(); posLoc != NULL;) {
			CWldObject* p = (CWldObject*)WldObjList->GetNext(posLoc);
			Mesh_MeshObjDump(meshes, p->m_meshObject);
		}
		gCurrentSubDirectory = "";
	}

	if (WldObjList) {
		if (exportToDb != dbe_dontExportToDb) {
			ZoneObjects objs;
			objs.WldObjList = WldObjList;
			objs.zoneIndex = zoneIndex;

			ExportGetSetToDb(view->m_hWnd, view->EditorBaseDir().c_str(), gameDir, destinationPath, &objs, false);
		}
		delete WldObjList;
	}

	if (exportToDb != dbe_onlyExportToDb) {
		if (EnvironmentPtr) {
			Mesh_EnvironmentObjDump(meshes, EnvironmentPtr);
			EnvironmentPtr->SafeDelete();
		}

		if (SceneScriptExec) {
			POSITION pos;

			// For each script loaded by the zone, determine the list of
			// CEXMesh objects (textures) that will be needed.
			for (pos = SceneScriptExec->GetHeadPosition(); pos != NULL;) {
				CEXScriptObj* so = (CEXScriptObj*)SceneScriptExec->GetNext(pos);

				LoadSceneScriptLocal(meshes, (int)so->m_actionAttribute, PathAdd(PathAdd(gameDir, GAMEFILES).c_str(), so->m_miscString).c_str(), logger);
			}

			for (pos = SceneScriptExec->GetHeadPosition(); pos != NULL;) {
				CEXScriptObj* so = (CEXScriptObj*)SceneScriptExec->GetNext(pos);

				bool isDir = false;
				if (jsFileExists(PathAdd(PathAdd(gameDir, GAMEFILES).c_str(), so->m_miscString).c_str(), &isDir) && !isDir) {
					addStringNoDupes(files, PathAdd(GAMEFILES, so->m_miscString), (so->m_actionAttribute != LOAD_PARTICLE_SYSTEMS));
					currentWorld.push_back(make_pair(so->m_miscString.GetString(), (int)so->m_actionAttribute));
				}
			}
		}
	}

	if (GL_ScenePersistList)
		delete GL_ScenePersistList;

	if (SceneScriptExec)
		delete SceneScriptExec;

	if (EnvironmentPtr) {
		EnvironmentPtr->SafeDelete();
		delete EnvironmentPtr;
	}

	if (BackGroundList)
		delete BackGroundList;
}

static void getFilesFromSbk(const char* gameDir, const char* exgName, strVect& files) {
	CFileException exc;
	CFile fl;
	CStringA fileName = PathAdd(gameDir, exgName).c_str();
	BOOL res = OpenCFile(fl, fileName, CFile::modeRead | CFile::shareDenyWrite, &exc);
	if (res == FALSE)
		throw new CFileException(exc.m_cause, exc.m_lOsError, exc.m_strFileName);

	CSoundBankList* sounds = 0;

	// end preliminary wrapper
	CArchive the_in_Archive(&fl, CArchive::load);
	the_in_Archive >> sounds;

	the_in_Archive.Close();
	fl.Close();

	if (sounds) {
		string soundDir = PathAdd(gameDir, SOUNDS);

		for (POSITION pos = sounds->GetHeadPosition(); pos != NULL;) {
			CSoundBankObj* sb = (CSoundBankObj*)sounds->GetNext(pos);

			bool isDir = false;
			if (sb->m_primary &&
				jsFileExists(PathAdd(soundDir.c_str(), sb->m_primary->m_fileName).c_str(), &isDir) && !isDir) {
				addStringNoDupes(files, PathAdd(SOUNDS, sb->m_primary->m_fileName), false);
			}
		}
		sounds->SafeDelete();
		delete sounds;
	}
}

void RemoveSpaces(char* s) {
	if (!s)
		return;

	int len = strlen(s);

	for (int i = 0; i < len; i++) {
		if (s[i] == ' ')
			s[i] = '_';
	}
}

//Search a directory for a specific file extension.
int SearchDirectory(
	vector<wstring>& refvecFiles,
	wstring& refcstrRootDirectory,
	wstring& refcstrExtension,
	bool bSearchSubdirectories) {
	wstring strFilePath; // Filepath
	wstring strPattern; // Pattern
	wstring strExtension; // Extension
	HANDLE hFile; // Handle to file
	WIN32_FIND_DATA FileInformation; // File information
	strPattern = refcstrRootDirectory + L"\\*.*";
	hFile = ::FindFirstFileW(strPattern.c_str(), &FileInformation);
	if (hFile != INVALID_HANDLE_VALUE) {
		do {
			if (FileInformation.cFileName[0] != L'.') {
				strFilePath.erase();
				strFilePath = refcstrRootDirectory + L"\\" + FileInformation.cFileName;

				if (FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					if (bSearchSubdirectories) {
						// Search subdirectory
						int iRC = SearchDirectory(refvecFiles,
							strFilePath,
							refcstrExtension,
							bSearchSubdirectories);
						if (iRC)
							return iRC;
					}
				} else {
					// Check extension
					strExtension = FileInformation.cFileName;
					strExtension = strExtension.substr(strExtension.rfind(L".") + 1);

					wchar_t extension[0xff];
					wcscpy_s(extension, _countof(extension), strExtension.c_str());
					for (size_t i = 0; i < wcslen(extension); i++)
						extension[i] = (char)tolower((int)extension[i]);

					strExtension = extension;
					wcscpy_s(extension, _countof(extension), refcstrExtension.c_str());
					for (size_t i = 0; i < wcslen(extension); i++)
						extension[i] = (char)tolower((int)extension[i]);

					refcstrExtension = extension;
					if ((strExtension == refcstrExtension) || (refcstrExtension == L"*")) {
						// Save filename
						refvecFiles.push_back(strFilePath);
					}
				}
			}
		} while (::FindNextFileW(hFile, &FileInformation) == TRUE);

		// Close handle
		::FindClose(hFile);

		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
			return dwError;
	}

	return 0;
}

//Glid encryption table.
void ApplyEncryption(int GLID, CStringA& glid) {
	char encryptable[10] = { 'h', 'v', 'g', '_', 'j', 'i', 'r', 'o', 'm', '3' };
	char buffer[MAX_PATH];
	memset(buffer, 0, sizeof(buffer));
	_itoa_s(GLID, buffer, _countof(buffer), 10);
	char* num = buffer;
	glid = "";
	for (size_t i = 0; i < strlen(num); i++) {
		char temp = num[i];
		int value = atol(&temp);
		glid += encryptable[value];
	}
}

static bool makeLooseFileInfo(const wchar_t* sourceDirName, const wchar_t* baseDir, const wchar_t* infoFileName) {
	WIN32_FIND_DATAW FindData;
	wchar_t oldDir[_MAX_PATH];
	_wgetcwd(oldDir, _MAX_PATH);
	SetCurrentDirectoryW(sourceDirName);
	CStringW RelativePath;
	CStringW SrcDir(baseDir);

	_wgetcwd(RelativePath.GetBufferSetLength(MAX_PATH), MAX_PATH);
	RelativePath.ReleaseBuffer();
	if (RelativePath == baseDir)
		RelativePath = L"";
	else
		RelativePath = RelativePath.Mid(SrcDir.GetLength() + 1, RelativePath.GetLength() - SrcDir.GetLength());
	RelativePath.Replace(L"\\", L"_");

	HANDLE fp = FindFirstFileW(L"*.*", &FindData);
	FILE* outFile = 0;

	if (_wfopen_s(&outFile, L"textureinfo.dat", L"w") != 0 || !outFile) {
		SetCurrentDirectoryW(oldDir);
		return false;
	}

	if (fp) {
		FOREVER {
			// loop through all files in folder.
			int size = FindData.nFileSizeLow; // assumes texture filesize won't overflow a dword.  we'll be
			// ok until we start using megatextures.
			if (size > 0) {
				if (wcscmp(FindData.cFileName, L"textureinfo.dat") != 0) {
					std::string sMD5 = Md5File(Utf16ToUtf8(FindData.cFileName));
					fprintf(outFile, "%s, %s, %s, %d\n", Utf16ToUtf8(RelativePath.GetString()).c_str(),
						Utf16ToUtf8(FindData.cFileName).c_str(), sMD5.c_str(), size);
				} else {
					TRACE("Skipping %s", FindData.cFileName);
				}
			}
			if (!FindNextFileW(fp, &FindData)) {
				break;
			}
		}
	}
	fclose(outFile);
	SetCurrentDirectoryW(oldDir);
	return true;
}

static bool copyDistFiles(HWND hwnd, strVect& files, dirVect& dirs, const char* baseDir, const char* destDir, Logger& logger, finalTexturePacketList* gFinalTexurePackage, strVect& looseTextureDirs, bool menusOnly) {
	size_t totLen = 2; // extra nulls on end
	size_t totDestLen = 2;

	size_t baseDirLen = ::strlen(baseDir) + 1; // add one for possible slash
	size_t destDirLen = ::strlen(destDir) + 1;

	// create a destination string that is double null terminated
	for (auto i = files.begin(); i != files.end(); i++) {
		totLen += baseDirLen + i->first.length() + 2;
		totDestLen += destDirLen + i->first.length() + 2;
	}

	//	Add texture package size to dests
	if (gFinalTexurePackage) {
		for (auto ti = gFinalTexurePackage->begin(); ti != gFinalTexurePackage->end(); ti++) {
			totLen += baseDirLen + ti->first.length() + 2;
			totDestLen += destDirLen + ti->second.length() + 2;
		}
	}

	char* fromString = new char[totLen];
	char* toString = new char[totDestLen];

	SHFILEOPSTRUCTW fo;

	memset(&fo, 0, sizeof(fo));
	fo.hwnd = hwnd;
	fo.wFunc = FO_COPY;
	fo.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_NORECURSION | FOF_FILESONLY;

	LOG4CPLUS_DEBUG(logger, "Begin copying distribution files.  Will copy the following files:");

	bool ret = true;

	char* buff = fromString;
	*buff = '\0';
	char* destBuff = toString;
	*destBuff = '\0';
	for (auto i = files.begin(); i != files.end(); i++) {
		// if wildcards, must to them separately
		size_t star = i->first.find("*");
		size_t lastSlash = i->first.rfind("\\");

		if (star != string::npos && lastSlash != string::npos) {
			wstring from = Utf8ToUtf16(PathAdd(baseDir, i->first.c_str()));
			wstring to = Utf8ToUtf16(PathAdd(destDir, i->first.substr(0, lastSlash)));
			from += L'\0';
			to += L'\0';

			wchar_t path_buffer[_MAX_PATH];
			wchar_t drive[_MAX_DRIVE];
			wchar_t dir[_MAX_DIR];

			_wsplitpath_s(Utf8ToUtf16(PathAdd(baseDir, i->first.c_str())).c_str(), drive, _countof(drive), dir, _countof(dir), 0, 0, 0, 0);
			_wmakepath_s(path_buffer, _countof(path_buffer), drive, dir, 0, 0);

			bool isDir = true;
			if (jsFileExists(Utf16ToUtf8(path_buffer).c_str(), &isDir) && isDir) {
				LOG4CPLUS_DEBUG(logger, "    " << Utf16ToUtf8(from) << "->" << Utf16ToUtf8(to));

				fo.pFrom = from.c_str();
				fo.pTo = to.c_str();
				fo.fFlags = fo.fFlags | FOF_SILENT;

				if (::SHFileOperation(&fo) != 0) {
					if (!fo.fAnyOperationsAborted) {
						CStringW s;
						s.LoadString(IDS_DIST_COPY_FAILED);
						s += L"\r\n";
						s += from.c_str();
						s += L" ->\r\n";
						s += to.c_str();

						AfxMessageBox(s, MB_ICONEXCLAMATION);
					}
					ret = false;
					break;
				}
			} else {
				LOG4CPLUS_WARN(logger, "   Create distribution skipping missing folder \"" << path_buffer << "\"");
			}
		} else {
			bool isDir;
			if (jsFileExists(PathAdd(baseDir, i->first.c_str()).c_str(), &isDir) && !isDir) {
				string to(PathAdd(destDir, i->first.c_str()));
				string from(PathAdd(baseDir, i->first.c_str()));

				LOG4CPLUS_DEBUG(logger, "    " << from << "-->" << to);

				::strncpy_s(buff, totLen - (buff - fromString), from.c_str(), _TRUNCATE);
				buff += from.length() + 1;

				::strncpy_s(destBuff, totDestLen - (destBuff - toString), to.c_str(), _TRUNCATE);
				destBuff += to.length() + 1;

				CStringA t;
				t.Format("\"%s\" -> \"%s\"\r\n", from.c_str(), to.c_str());
				OutputDebugStringA(t);
			} else {
				LOG4CPLUS_WARN(logger, "   Create distribution skipping missing file \"" << PathAdd(baseDir, i->first.c_str()) << "\"");
			}
		}
	}

	fo.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION;
	for (auto i = dirs.begin(); i != dirs.end(); i++) {
		size_t lastSlash = i->rfind("\\");

		wstring from = Utf8ToUtf16(PathAdd(baseDir, i->c_str()));
		from += L'\0';
		wstring to = Utf8ToUtf16(PathAdd(destDir, i->substr(0, lastSlash)));
		to += L'\0';

		wchar_t path_buffer[_MAX_PATH];
		wchar_t drive[_MAX_DRIVE];
		wchar_t dir[_MAX_DIR];

		_wsplitpath_s(Utf8ToUtf16(PathAdd(baseDir, i->c_str())).c_str(), drive, _countof(drive), dir, _countof(dir), 0, 0, 0, 0);
		_wmakepath_s(path_buffer, _countof(path_buffer), drive, dir, 0, 0);

		bool isDir = true;
		if (jsFileExists(Utf16ToUtf8(path_buffer).c_str(), &isDir) && isDir) {
			LOG4CPLUS_DEBUG(logger, "    " << Utf16ToUtf8(from) << "->" << Utf16ToUtf8(to));
			fo.pFrom = from.c_str();
			fo.pTo = to.c_str();
			fo.fFlags = fo.fFlags | FOF_SILENT;
			_wmkdir(to.c_str());
			if (::SHFileOperation(&fo) != 0) {
				if (!fo.fAnyOperationsAborted) {
					CStringW s;
					s.LoadString(IDS_DIST_COPY_FAILED);
					s += L"\r\n";
					s += from.c_str();
					s += L" ->\r\n";
					s += to.c_str();

					AfxMessageBox(s, MB_ICONEXCLAMATION);
				}
				ret = false;
				break;
			}
		} else {
			LOG4CPLUS_WARN(logger, "   Create distribution skipping missing folder \"" << path_buffer << "\"");
		}
	}

	if (!menusOnly) {
		if (gFinalTexurePackage) {
			for (auto ti = gFinalTexurePackage->begin(); ti != gFinalTexurePackage->end(); ti++) {
				bool isDir;
				string from(PathAdd(baseDir, ti->first.c_str()));

				if (jsFileExists(from.c_str(), &isDir) && !isDir) {
					string to(PathAdd(destDir, ti->second.c_str()));

					::strcpy_s(buff, totLen - (buff - fromString), from.c_str());
					buff += from.length() + 1;

					::strcpy_s(destBuff, totDestLen - (destBuff - toString), to.c_str());
					destBuff += to.length() + 1;
				} else {
					LogWarn("Distribution could not find texture source in MapsModels: " << ti->first.c_str());
				}
			}
		}
	}

	if (ret && strlen(fromString) > 0) {
		fo.fFlags = FOF_NOCONFIRMMKDIR | FOF_MULTIDESTFILES | FOF_NOCONFIRMATION;
		wstring fromStringW = Utf8ToUtf16(fromString);
		fo.pFrom = fromStringW.c_str();
		if ((size_t)(buff - fromString) < totLen)
			*buff = '\0'; // append null since in debug strncpy fills memory after string
		wstring toStringW = Utf8ToUtf16(toString);
		fo.pTo = toStringW.c_str();
		if ((size_t)(destBuff - toString) < totDestLen)
			*destBuff = '\0'; // append null since in debug strncpy fills memory after string

		fo.fFlags = fo.fFlags | FOF_SILENT;

		if (::SHFileOperation(&fo) != 0) {
			if (!fo.fAnyOperationsAborted) {
				CStringW s;
				s.LoadString(IDS_DIST_COPY_FAILED);
				s += L"\r\n";
				s += fromStringW.c_str();
				s += L" ->\r\n";
				s += toStringW.c_str();

				AfxMessageBox(s, MB_ICONEXCLAMATION);
			}
			ret = false;
		}
	}

	if (!menusOnly && gFinalTexurePackage) {
		CStringA mdest = CStringA(destDir) + "\\" + MAPSMODELS + "\\" + "Icons";

		for (auto si = looseTextureDirs.begin(); si != looseTextureDirs.end(); si++) {
			CStringW tempstr = Utf8ToUtf16(CStringA(destDir) + "\\" + si->first.c_str()).c_str();
			if (strncmp(si->first.c_str(), "MapsModels", 10) == 0) {
				makeLooseFileInfo(tempstr, Utf8ToUtf16(destDir).c_str(), L"textureinfo.dat");
			} else {
				makeLooseFileInfo(tempstr, Utf8ToUtf16(destDir).c_str(), L"meshinfo.dat");
			}
		}
	}

	delete fromString;
	delete toString;

	LOG4CPLUS_DEBUG(logger, "End copying distribution files.  Ret = " << (ret ? "true" : "false"));

	return ret;
}

static void displayPackErrorDialog(Logger& logger, int returnCode, const string& fileName) {
	CStringW errStr = returnCode == -1 ? L"Unable to create pack file (file write, " : L"Unable to create pack file (file read, ";
	wchar_t buff[256];
	_wcserror_s(buff, _countof(buff));
	errStr.Append(buff);
	errStr.Append(L") ");
	errStr.Append(Utf8ToUtf16(fileName).c_str());
	LOG4CPLUS_WARN(logger, errStr);
	AfxMessageBox(errStr, MB_ICONEXCLAMATION, 0);
}

static void pakDistFiles(const char* destDir, strVect& looseTextureDirs, Logger& logger, bool menusOnly, int gameId) {
	string relativeSrcDir; // where, under the root, we should look for files to pack
	string relativePackFile; // where, on the disk, we should write the resulting pack file
	string extensionToPack; // which files to pack
	string rootDir(destDir); // the root directory for the distribution
	rootDir += '\\';
	int err = 0;

	if (!menusOnly) {
		//	Add any textures in the MapsModels Dir itself first, so we dont add packfiles to it
		relativeSrcDir = string(MAPSMODELS) + '\\';
		extensionToPack = "*";
		relativePackFile = relativeSrcDir + MAPSMODELS + ".pak";
		if ((err = KPack::PackFiles(rootDir, relativeSrcDir, extensionToPack, relativePackFile)) < 0) {
			displayPackErrorDialog(logger, err, rootDir + relativePackFile);
		}

		//	Add all the subdirectory packs
		for (auto ti = gListOfTextureDirs.begin(); ti != gListOfTextureDirs.end(); ti++) {
			relativeSrcDir = string(MAPSMODELS) + '\\' + (*ti).c_str() + '\\';
			relativePackFile = relativeSrcDir + (*ti).c_str() + ".pak";

			bool looseDir = false;
			for (auto si = looseTextureDirs.begin(); si != looseTextureDirs.end(); si++) {
				CStringA tempstr;
				tempstr.Format("MapsModels\\%s", (*ti).c_str());
				if (strcmp(tempstr.GetString(), si->first.c_str()) == 0) {
					looseDir = true;
					tempstr = CStringA(destDir) + "\\" + tempstr;
					// check this... why don't we just exit from the loop here?
				}
			}
			if (!looseDir) {
				if ((err = KPack::PackFiles(rootDir, relativeSrcDir, extensionToPack, relativePackFile)) < 0) {
					displayPackErrorDialog(logger, err, rootDir + relativePackFile);
				}
			}
		}
	}

	string additionalPakFiles[][3] = {
		// [source folder], [extension to pack], [destination folder and file name]
		{ "\\", "swf", "swf.pak" },
		{ "\\", "dll", "dll.pak" },
		{ "\\", "xml", "xml.pak" },
		{ "ClientBlades\\", "dll", "ClientBlades\\dll.pak" },
		{ "ClientBlades\\effects\\", "fx", "ClientBlades\\effects\\fx.pak" },
		{ "ClientBlades\\meshes\\", "wbm", "ClientBlades\\meshes\\wbm.pak" },
		{ "GameFiles\\", "trg", "GameFiles\\trg.pak" },
		{ "GameFiles\\BladeScripts\\", "dll", "GameFiles\\BladeScripts\\dll.pak" },
		{ "GameFiles\\ClientScripts\\", "lua", "GameFiles\\ClientScripts\\lua.pak" },
		{ "GameFiles\\ClientScripts\\", "py", "GameFiles\\ClientScripts\\py.pak" },
		{ "GameFiles\\ScriptData\\", "swf", "GameFiles\\ScriptData\\swf.pak" },
		{ "GameFiles\\ScriptData\\", "lua", "GameFiles\\ScriptData\\lua.pak" },
		{ "GameFiles\\Menus\\", "xml", "GameFiles\\Menus\\xml.pak" },
		{ "GameFiles\\Menus\\", "dds", "GameFiles\\Menus\\dds.pak" },
		{ "GameFiles\\Menus\\", "tga", "GameFiles\\Menus\\tga.pak" },
		{ "GameFiles\\MenuScripts\\", "lua", "GameFiles\\MenuScripts\\lua.pak" },
		{ "GameFiles\\Scripts\\", "lua", "GameFiles\\Scripts\\lua.pak" },
		{ "GameFiles\\Scripts\\", "py", "GameFiles\\Scripts\\py.pak" },
		{ "GameFiles\\textures\\particles\\", "tga", "GameFiles\\textures\\particles\\tga.pak" },
		{ "MapsModels\\CharacterTextures\\Core\\", "dds", "MapsModels\\CharacterTextures\\dds.pak" },
		{ "sounds\\", "wav", "sounds\\wav.pak" },
		{ "GameFiles\\CharacterAnimations\\Core\\", "ranm", "GameFiles\\CharacterAnimations\\CoreAnimations.pak" },
		{ "GameFiles\\CharacterMeshes\\Core\\", "rmsh", "GameFiles\\CharacterMeshes\\CoreMeshes.pak" },
		{ "ca-certs\\", "pem", "ca-certs\\pem.pak" },
		{ "gtk\\bin\\", "exe", "gtk\\bin\\exe.pak" },
		{ "gtk\\bin\\", "dll", "gtk\\bin\\dll.pak" },
		{ "gtk\\lib\\gtk-2.0\\2.10.0\\engines\\", "dll", "gtk\\lib\\gtk-2.0\\2.10.0\\engines\\dll.pak" },
		{ "gtk\\manifest\\", "mft", "gtk\\manifest\\mft.pak" },
		{ "plugins\\", "dll", "plugins\\dll.pak" },
		{ "sasl2\\", "dll", "sasl2\\dll.pak" },
		{ "sounds\\purple\\", "wav", "sounds\\purple\\wav.pak" },
		{ "pixmaps\\", "xpm", "pixmaps\\xpm.pak" },
		{ "pixmaps\\animations\\", "png", "pixmaps\\animations\\png.pak" },
		{ "pixmaps\\buttons\\", "png", "pixmaps\\buttons\\png.pak" },
		{ "pixmaps\\dialogs\\", "png", "pixmaps\\dialogs\\png.pak" },
		{ "pixmaps\\emblems\\", "png", "pixmaps\\emblems\\png.pak" },
		{ "pixmaps\\emotes\\default\\", "png", "pixmaps\\emotes\\default\\png.pak" },
		{ "pixmaps\\emotes\\small\\", "png", "pixmaps\\emotes\\small\\png.pak" },
		{ "pixmaps\\kaneva\\", "png", "pixmaps\\kaneva\\png.pak" },
		{ "pixmaps\\protocols\\", "png", "pixmaps\\protocols\\png.pak" },
		{ "pixmaps\\status\\", "png", "pixmaps\\status\\png.pak" },
		{ "pixmaps\\toolbar\\", "png", "pixmaps\\toolbar\\png.pak" },
		{ "pixmaps\\tray\\", "ico", "pixmaps\\tray\\ico.pak" },
		{ "pixmaps\\tray\\hicolor\\status\\", "png", "pixmaps\\tray\\hicolor\\status\\png.pak" },
		{ "", "", "" } // Don't forget this!
	};

	//
	// iterate over the array until the terminating row is reached,
	// which is identified by an empty pak file name.
	//
	for (int i = 0; additionalPakFiles[i][2] != ""; i++) {
		if (menusOnly) {
			if (additionalPakFiles[i][0].find("\\Menu") == string::npos)
				continue; // skip if only doing menus

			// put gameId in menu folder
			string::size_type p = additionalPakFiles[i][0].find("\\Menus");
			if (p != string::npos)
				additionalPakFiles[i][0].insert(p + 6, numToString(gameId, "-%d"));
			else if ((p = additionalPakFiles[i][0].find("\\MenuScripts")) != string::npos)
				additionalPakFiles[i][0].insert(p + 12, numToString(gameId, "-%d"));

			if ((p = additionalPakFiles[i][2].find("\\Menus")) != string::npos)
				additionalPakFiles[i][2].insert(p + 6, numToString(gameId, "-%d"));
			else if ((p = additionalPakFiles[i][2].find("\\MenuScripts")) != string::npos)
				additionalPakFiles[i][2].insert(p + 12, numToString(gameId, "-%d"));
		}

		if ((err = KPack::PackFiles(rootDir, additionalPakFiles[i][0], additionalPakFiles[i][1], additionalPakFiles[i][2])) < 0) {
			displayPackErrorDialog(logger, err, rootDir + relativePackFile);
		}
		string tmp = additionalPakFiles[i + 1][2];
	}
}

bool postDistStep(const char* scriptName, HWND hwnd, const char* baseDir, const char* destDir, const char* fileName, const char* licenseKey, const char* extraParam, bool skipdialog, const char* gameId) {
	bool isDir;
	bool ret = true;

	if (jsFileExists(PathAdd(baseDir, scriptName).c_str(), &isDir) && !isDir) {
		char fname[_MAX_PATH];
		_splitpath_s(fileName, 0, 0, 0, 0, fname, _countof(fname), 0, 0);

		CStringA parms;
		parms.Format("%s %d \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" %s", scriptName, (int)hwnd, baseDir, destDir, fname, licenseKey, extraParam, gameId);
		CStringW parmsW(Utf8ToUtf16(parms).c_str());

		wstring baseDirW = Utf8ToUtf16(baseDir);
		SHELLEXECUTEINFOW shi;
		memset(&shi, 0, sizeof(shi));
		shi.cbSize = sizeof(shi);
		shi.fMask = SEE_MASK_NOCLOSEPROCESS;
		shi.hwnd = hwnd;
		shi.lpVerb = L"open";
		shi.lpFile = L"wscript.exe";
		shi.lpParameters = parmsW;
		shi.nShow = SW_SHOW;
		shi.lpDirectory = baseDirW.c_str();
		if (ShellExecuteExW(&shi) && (int)shi.hInstApp > 32) {
			while (WaitForSingleObject(shi.hProcess, 1) != WAIT_OBJECT_0)
				::Sleep(10);

			DWORD exitCode = 0;
			GetExitCodeProcess(shi.hProcess, &exitCode);
			if (exitCode == 0)
				ret = true;

			CloseHandle(shi.hProcess);
		} else {
			CStringW s;
			string ss;
			s.FormatMessage(IDS_ERR_LAUNCH_DIST, errorNumToString(::GetLastError(), ss));
			AfxMessageBox(s, MB_ICONEXCLAMATION, 0);
		}
	}
	return ret;
}

///---------------------------------------------------------
// targetDistributionDirEmpty
//
// Checks that the ClientFiles, ServerFiles, and PatchFiles
// directories do not already exist. PatchFiles are contained within the
// platform install directoy, e.g. C:\Program Files (x86)\Kaneva\Kaneva Platform\PatchFiles\Patch<GAMEID>
//
// [Returns]
//  destEmpty
//  patchEmpty
void targetDistributionDirEmpty(const char* dir, bool& destEmpty, const char* patchDir, bool& patchEmpty) {
	bool isDir;

	destEmpty = true;
	patchEmpty = true;

	string testDir;
	testDir = PathAdd(dir, CLIENT_FILE_LOCATION);
	if (jsFileExists(testDir.c_str(), &isDir) && isDir)
		destEmpty = false;

	testDir = PathAdd(dir, SERVER_FILE_LOCATION);
	if (jsFileExists(testDir.c_str(), &isDir) && isDir)
		destEmpty = false;

	testDir = patchDir;
	if (jsFileExists(testDir.c_str(), &isDir) && isDir)
		patchEmpty = false;
}

///---------------------------------------------------------
// makeDeploy
//
// Create a patch from the clientDir into the given patchDir.
//
// [Returns]
//  false - error
//  true - success
bool makeDeploy(CStringA clientDir, CStringA patchDir, HANDLE closeEvent, HANDLE distCloseEvent, Logger& logger, CRCLIST* oldpatchData, int oldFileCount, const char* version, strVect& looseTextureDirs, bool skipdialog) {
	bool ret = false;

	CMakepatch patch(gProgressHwnd, distCloseEvent, closeEvent, gMsgId, skipdialog);

	int retCode = patch.Make(clientDir, patchDir, version, oldpatchData, oldFileCount, looseTextureDirs);

	switch (retCode) {
		case patch.general_error:
			break;
		case patch.invalid_dest:
			break;
		case patch.invalid_source:
			break;
		case patch.invalid_version:
			break;
		case patch.cancel:
			break;
		case patch.success:
			ret = true;
			break;
		default:
			break;
	}

	return ret;
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_EnvironmentObjDump(strVect& meshes, CEnvironmentObj* environmentPtr) {
	if (!environmentPtr)
		return;

	if (environmentPtr->m_environmentTimeSystem)
		Mesh_MeshObjDump(meshes, environmentPtr->m_environmentTimeSystem->m_visual);
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_MeshObjDump(strVect& meshes, CEXMeshObj* meshObj) {
	if (!meshObj)
		return;

	for (int jj = 0; jj < NUM_CMATERIAL_TEXTURES; jj++) {
		string texFileName = meshObj->m_materialObject->m_texFileName[jj];
		if (ValidTextureFileName(texFileName))
			addStringNoDupes(meshes, texFileName, false);

		if (!meshObj->m_shadowTemp)
			continue;

		for (int ii = 0; ii < NUM_CMATERIAL_TEXTURES; ii++) {
			texFileName = meshObj->m_shadowTemp->m_materialObject->m_texFileName[ii];
			if (ValidTextureFileName(texFileName))
				addStringNoDupes(meshes, texFileName, false);
		}
	}
}

static void Mesh_MaterialObjDump(strVect& meshes, CDeformableMesh* pMesh) {
	if (!pMesh || !pMesh->m_supportedMaterials)
		return;

	for (POSITION posLocal = pMesh->m_supportedMaterials->GetHeadPosition(); posLocal != NULL;) {
		CMaterialObject* pMat = (CMaterialObject*)pMesh->m_supportedMaterials->GetNext(posLocal);
		if (!pMat)
			continue;

		for (int jj = 0; jj < NUM_CMATERIAL_TEXTURES; jj++) {
			string texFileName = pMat->m_texFileName[jj];
			if (ValidTextureFileName(texFileName))
				addStringNoDupes(meshes, texFileName, false);
		}
	}
}

// overloaded by Jonny 02/13/08 to separate core textures from extended textures
static void Mesh_SkeletalTextureDump(strVect& meshes, RuntimeSkeleton* skeletonObj, SkeletonConfiguration* skeletonCfg, strVect& core) {
	if (!skeletonObj)
		return;

	// hiarch bone objects
	for (POSITION posLocal = skeletonObj->m_pBoneList->GetHeadPosition(); posLocal != NULL;) {
		CBoneObject* bonePtr = (CBoneObject*)skeletonObj->m_pBoneList->GetNext(posLocal);
		for (auto itMesh = bonePtr->m_rigidMeshes.begin(); itMesh != bonePtr->m_rigidMeshes.end(); ++itMesh) {
			for (POSITION posOni = (*itMesh)->m_lodChain->GetHeadPosition(); posOni != NULL;) {
				CHiarcleVisObj* hObj = (CHiarcleVisObj*)(*itMesh)->m_lodChain->GetNext(posOni);

				Mesh_MeshObjDump(meshes, hObj->m_mesh);
			}
		}
	}

	// spawn deformable cfg
	if (skeletonCfg->m_alternateConfigurations) {
		for (POSITION posLoc = skeletonCfg->m_alternateConfigurations->GetHeadPosition(); posLoc != NULL;) {
			CAlterUnitDBObject* aPtr = (CAlterUnitDBObject*)skeletonCfg->m_alternateConfigurations->GetNext(posLoc);
			for (POSITION cfgPos = aPtr->m_configurations->GetHeadPosition(); cfgPos != NULL;) {
				CDeformableMesh* dMesh = (CDeformableMesh*)aPtr->m_configurations->GetNext(cfgPos);

				//	Dump all texture which belong to the DeformableMesh
				if (!dMesh->m_coreContent) {
					Mesh_MaterialObjDump(meshes, dMesh);
				} else {
					Mesh_MaterialObjDump(core, dMesh); // This is core content
					Mesh_MaterialObjDump(meshes, dMesh); // And it should also be streamable
				}
			}
		}
	}

	if (skeletonObj->m_shadowInterface)
		Mesh_MeshObjDump(meshes, skeletonObj->m_shadowInterface->m_shadowMesh);
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_SkeletalTextureDump(strVect& meshes, RuntimeSkeleton* skeletonObj, SkeletonConfiguration* skeletonCfg) {
	if (!skeletonObj)
		return;

	// hiarch bone objects
	for (POSITION posLocal = skeletonObj->m_pBoneList->GetHeadPosition(); posLocal != NULL;) {
		CBoneObject* bonePtr = (CBoneObject*)skeletonObj->m_pBoneList->GetNext(posLocal);
		for (auto itMesh = bonePtr->m_rigidMeshes.begin(); itMesh != bonePtr->m_rigidMeshes.end(); ++itMesh) {
			for (POSITION posOni = (*itMesh)->m_lodChain->GetHeadPosition(); posOni != NULL;) {
				CHiarcleVisObj* hObj = (CHiarcleVisObj*)(*itMesh)->m_lodChain->GetNext(posOni);

				Mesh_MeshObjDump(meshes, hObj->m_mesh);
			}
		}
	}

	// spawn deformable cfg
	if (skeletonCfg->m_alternateConfigurations) {
		for (POSITION posLoc = skeletonCfg->m_alternateConfigurations->GetHeadPosition(); posLoc != NULL;) {
			CAlterUnitDBObject* aPtr = (CAlterUnitDBObject*)skeletonCfg->m_alternateConfigurations->GetNext(posLoc);
			for (POSITION cfgPos = aPtr->m_configurations->GetHeadPosition(); cfgPos != NULL;) {
				CDeformableMesh* dMesh = (CDeformableMesh*)aPtr->m_configurations->GetNext(cfgPos);

				//	Dump all texture which belong to the DeformableMesh
				Mesh_MaterialObjDump(meshes, dMesh);
			}
		}
	}

	if (skeletonObj->m_shadowInterface)
		Mesh_MeshObjDump(meshes, skeletonObj->m_shadowInterface->m_shadowMesh);
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_CollisionObjDump(strVect& meshes, CCollisionObj* collisionObj) {
	if (!collisionObj)
		return;

	if (!collisionObj->m_wldObjList)
		return;

	for (POSITION worldLoc = collisionObj->m_wldObjList->GetHeadPosition(); worldLoc != NULL;) {
		CWldObject* worldPtr = (CWldObject*)collisionObj->m_wldObjList->GetNext(worldLoc);

		Mesh_MeshObjDump(meshes, worldPtr->m_meshObject);

		if (worldPtr->m_lodVisualList) {
			for (POSITION lPos = worldPtr->m_lodVisualList->GetHeadPosition(); lPos != NULL;) {
				CMeshLODObject* lodPtr = (CMeshLODObject*)worldPtr->m_lodVisualList->GetNext(lPos);

				for (int jj = 0; jj < NUM_CMATERIAL_TEXTURES; jj++) {
					string texFileName = lodPtr->m_material->m_texFileName[jj];
					if (ValidTextureFileName(texFileName))
						addStringNoDupes(meshes, texFileName, false);
				}
			}
		}
	}

	jsAssert(false);
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_EquippableSystemDump(strVect& meshes, EquippableRM* eqSystem) {
	if (!eqSystem)
		return;

	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return;

	vector<UINT64> allKeys;
	eqSystem->GetAllResourceKeys(allKeys);

	EquippableRM* equippableRM = pICE->GetEquippableRM();
	if (!equippableRM)
		return;

	// appendage Loop
	for (auto it = allKeys.begin(); it != allKeys.end(); ++it) {
		CArmedInventoryProxy* proxy = dynamic_cast<CArmedInventoryProxy*>(equippableRM->GetResource(*it));
		if (proxy && proxy->GetInventoryObj()) {
			CArmedInventoryObj* armedObj = proxy->GetInventoryObj();
			// First person system can be null.  There's a check in the function for a null runtimeSkeleton,
			// but I need to check the first/third person systems out here.
			if (armedObj->m_thirdPersonSystem) {
				Mesh_SkeletalTextureDump(meshes, armedObj->m_thirdPersonSystem->getRuntimeSkeleton(), armedObj->m_thirdPersonSystem->getSkeletonConfiguration());
			}
			if (armedObj->m_firstPersonSystem) {
				Mesh_SkeletalTextureDump(meshes, armedObj->m_firstPersonSystem->getRuntimeSkeleton(), armedObj->m_firstPersonSystem->getSkeletonConfiguration());
			}
		}
	}
}

static void Mesh_MovementObjDump(strVect& meshes, CMovementObj* movementObj, strVect& core) {
	if (!movementObj)
		return;

	if (movementObj->getSkeleton())
		Mesh_SkeletalTextureDump(meshes, movementObj->getSkeleton(), movementObj->m_skeletonConfiguration, core);

	if (movementObj->getPosses())
		Mesh_MovementObjDump(meshes, movementObj->getPosses());

	if (movementObj->m_pMO_possedBy)
		Mesh_MovementObjDump(meshes, movementObj->m_pMO_possedBy);

	Mesh_CollisionObjDump(meshes, movementObj->getCollisionMovRefPtr());
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_MovementObjDump(strVect& meshes, CMovementObj* movementObj) {
	if (!movementObj)
		return;

	if (movementObj->getSkeleton())
		Mesh_SkeletalTextureDump(meshes, movementObj->getSkeleton(), movementObj->m_skeletonConfiguration);

	if (movementObj->m_targetRef)
		Mesh_MovementObjDump(meshes, movementObj->m_targetRef);

	if (movementObj->getPosses())
		Mesh_MovementObjDump(meshes, movementObj->getPosses());

	if (movementObj->m_pMO_possedBy)
		Mesh_MovementObjDump(meshes, movementObj->m_pMO_possedBy);

	Mesh_CollisionObjDump(meshes, movementObj->getCollisionMovRefPtr());
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_MeshObjListDump(strVect& meshes, CEXMeshObjList* meshObjList) {
	if (!meshObjList)
		return;

	for (POSITION posLoc = meshObjList->GetHeadPosition(); posLoc != NULL;) {
		CEXMeshObj* ptrLoc = (CEXMeshObj*)meshObjList->GetNext(posLoc);
		Mesh_MeshObjDump(meshes, ptrLoc);
	}
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_HousingDBDump(strVect& meshes, CHousingObjList* housingObjList) {
	if (!housingObjList)
		return;

	for (POSITION posLoc = housingObjList->GetHeadPosition(); posLoc != NULL;) {
		CHousingObj* ptrLoc = (CHousingObj*)housingObjList->GetNext(posLoc);

		Mesh_CollisionObjDump(meshes, ptrLoc->m_closedState);
		Mesh_CollisionObjDump(meshes, ptrLoc->m_openState);

		Mesh_MeshObjListDump(meshes, ptrLoc->m_externalVisuals);
		Mesh_MeshObjListDump(meshes, ptrLoc->m_internalVisuals);
		Mesh_MeshObjListDump(meshes, ptrLoc->m_lod2Set);
		Mesh_MeshObjListDump(meshes, ptrLoc->m_lod3Set);

		Mesh_MeshObjDump(meshes, ptrLoc->m_doorVisual);
	}
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_ExplosionDBDump(strVect& meshes, CExplosionObjList* explosionObjList) {
	if (!explosionObjList)
		return;

	for (POSITION posLoc = explosionObjList->GetHeadPosition(); posLoc != NULL;) {
		CExplosionObj* ptrLoc = (CExplosionObj*)explosionObjList->GetNext(posLoc);
		Mesh_MeshObjDump(meshes, ptrLoc->m_meshObject);
		if (ptrLoc->getRuntimeSkeleton())
			Mesh_SkeletalTextureDump(meshes, ptrLoc->getRuntimeSkeleton(), nullptr);
	}
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_MissileObjDump(strVect& meshes, CMissileObj* missileObj) {
	if (!missileObj)
		return;

	if (missileObj->getRuntimeSkeleton())
		Mesh_SkeletalTextureDump(meshes, missileObj->getRuntimeSkeleton(), nullptr);

	Mesh_MeshObjDump(meshes, missileObj->m_basicMesh);

	if (missileObj->m_laserObject)
		Mesh_MeshObjDump(meshes, missileObj->m_laserObject->m_meshObject);
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_MissileDBDump(strVect& meshes, CMissileObjList* missileObjList) {
	if (!missileObjList)
		return;

	for (POSITION posLoc = missileObjList->GetHeadPosition(); posLoc != NULL;) {
		CMissileObj* ptrLoc = (CMissileObj*)missileObjList->GetNext(posLoc);
		Mesh_MissileObjDump(meshes, ptrLoc);
	}
}

class MeshDumpTraverser : public CGlobalInventoryObjList::Traverser {
public:
	MeshDumpTraverser(strVect& vmeshes) :
			meshes(vmeshes) {}
	virtual bool evaluate(CGlobalInventoryObj& obj) {
		Mesh_MeshObjDump(meshes, obj.m_visual);
		return false;
	}
	strVect& meshes;
};

static void Mesh_GlobalInventoryDump(strVect& meshes, CGlobalInventoryObjList* globalEntityObjList) {
	if (globalEntityObjList)
		globalEntityObjList->traverse(MeshDumpTraverser(meshes));
}

class MovementDumpTraverser : public CGlobalInventoryObjList::Traverser {
public:
	MovementDumpTraverser(strVect& vmeshes) :
			meshes(vmeshes) {}

	virtual bool evaluate(CGlobalInventoryObj& obj) {
		CItemObj* pItem = obj.m_itemData;
		if (!pItem)
			return false;

		string texFileName = (const char*)pItem->m_customTexture;
		if (!ValidTextureFileName(texFileName)) {
			// Add texture item to list. We need to add this to the list of texture processed and distributed
			string temp = gCurrentSubDirectory;
			gCurrentSubDirectory = "TextureTemplates";
			addStringNoDupes(meshes, texFileName, false);
			gCurrentSubDirectory = temp;
		}
		return false;
	}

	strVect& meshes;
};

// What is this even used for?  -- Jonny
static void Texture_MovementObjDump(strVect& meshes) {
	CGlobalInventoryObjList::GetInstance()->traverse(MovementDumpTraverser(meshes));
}

// Predetermined walk path for the given object to determine if a CEXMesh object is used.
static void Mesh_EntityObjectDump(strVect& meshes, CMovementObjList* entityObjList, strVect& core) {
	if (entityObjList) {
		for (POSITION posLoc = entityObjList->GetHeadPosition(); posLoc != NULL;) {
			CMovementObj* ptrLoc = (CMovementObj*)entityObjList->GetNext(posLoc);

			//	First dump all the meshes associated with this movementObj
			Mesh_MovementObjDump(meshes, ptrLoc, core);
		}
		//	Now dump custom attributes associated with each item
		Texture_MovementObjDump(meshes);
	}
}

class StockAnimationGlid : public CObject, public GetSet {
public:
	string m_ownerType;
	string m_ownerName;
	int m_animationIndex;
	int m_animationType;
	int m_animationVersion;
	int m_animationGlid;
	string m_animationName;

	DECLARE_GETSET
};

class StockAnimationGlids : public GetSet {
public:
	CObList* m_data;

	StockAnimationGlids() {
		m_data = new CObList;
	}

	~StockAnimationGlids() {
		for (POSITION pos = m_data->GetHeadPosition(); pos != NULL;) {
			StockAnimationGlid* rec = (StockAnimationGlid*)m_data->GetNext(pos);
			delete rec;
		}

		m_data->RemoveAll();
		delete m_data;
		m_data = NULL;
	}

	DECLARE_GETSET
};

static void DumpAnimationGlidsForSkeleton(const string& ownerType, const string& ownerName, const RuntimeSkeleton* pSkeleton, StockAnimationGlids& allGlids) {
	for (UINT animLoop = 0; animLoop < pSkeleton->getSkeletonAnimHeaderCount(); animLoop++) {
		auto pSAH = pSkeleton->getSkeletonAnimHeaderByIndex(animLoop);
		if (!pSAH)
			continue;
		StockAnimationGlid* rec = new StockAnimationGlid;
		rec->m_ownerType = ownerType;
		rec->m_ownerName = ownerName;
		rec->m_animationIndex = animLoop;
		rec->m_animationType = pSAH->m_animType;
		rec->m_animationVersion = pSAH->m_animVer;
		rec->m_animationGlid = pSAH->m_animGlid;
		rec->m_animationName = CBoneAnimationNaming::AnimToName(pSAH->m_animType);
		allGlids.m_data->AddTail(rec);
	}
}

static void DumpAnimationGlids(CMovementObjList* entities, StockAnimationGlids& allGlids) {
	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return;

	for (POSITION posEnt = entities->GetHeadPosition(); posEnt != NULL;) {
		CMovementObj* pEnt = dynamic_cast<CMovementObj*>(entities->GetNext(posEnt));
		RuntimeSkeleton* pSkeleton = pEnt->getSkeleton();
		DumpAnimationGlidsForSkeleton("ACTOR", (const char*)pEnt->m_name, pSkeleton, allGlids);
	}

	EquippableRM* equippableRM = pICE->GetEquippableRM();
	if (!equippableRM)
		return;

	vector<UINT64> allKeys;
	equippableRM->GetAllResourceKeys(allKeys);
	for (auto it = allKeys.begin(); it != allKeys.end(); ++it) {
		CArmedInventoryProxy* proxy = dynamic_cast<CArmedInventoryProxy*>(equippableRM->GetResource(*it));
		if (proxy && proxy->GetInventoryObj()) {
			CArmedInventoryObj* pEquip = proxy->GetInventoryObj();
			if (pEquip->m_thirdPersonSystem)
				DumpAnimationGlidsForSkeleton("EQUIP", pEquip->getName(), pEquip->m_thirdPersonSystem->getRuntimeSkeleton(), allGlids);

			if (pEquip->m_firstPersonSystem) // Tag equipment name with special postfix if this is a first person system
				DumpAnimationGlidsForSkeleton("EQUIP", (pEquip->getName() + "$FP").c_str(), pEquip->m_firstPersonSystem->getRuntimeSkeleton(), allGlids);
		}
	}
}

class MeshVecBuilder : public CGlobalInventoryObjList::Traverser {
public:
	MeshVecBuilder(strVect& vmeshes) :
			meshes(vmeshes) {}
	virtual bool evaluate(CGlobalInventoryObj& obj) {
		CItemObj* Itm = obj.m_itemData;
		CStringA formatter;
		string s;
		if (Itm) {
			formatter.Format("%d.dds", Itm->m_globalID);
			s = formatter;
			addStringNoDupes(meshes, s, false);
		}
		return false;
	}
	strVect& meshes;
};

void CKEPEditView::AddToStrVect(strVect& sv, const string& str) {
	if (!str.empty())
		addStringNoDupes(sv, str, false);
}

// CKEPEditView::GetClientDistributionFiles
//
// Obtain the list of client distribution files and whether they
// should be encrypted or not.
//
// [Returns]
//  false - error
//  true - success
//  clientFiles as output if success
bool CKEPEditView::GetClientDistributionFiles(
	HANDLE distCloseEvent,
	HANDLE closeEvent,
	strVect& clientFiles,
	dirVect& clientDirs,
	const char* baseDir,
	const char* exportSqlPath,
	EDBExport exportToDb,
	bool tristripAssets,
	ULONG& coreClientFileCount,
	dependVect& worldFiles,
	bool menusOnly) {
	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return false;

	bool ret = false;
	string fileToOpen;
	fileToOpen.clear();

	try {
		strVect meshes;
		strVect core;
		CStringW titleString;
		CStringW statusString;
		STATUSU pStatus;

		titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_1);
		pStatus.stepId = 1;
		strcpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(titleString.GetString()).c_str());
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

		if (exportToDb != dbe_onlyExportToDb) {
			//	For entities, set curDirectory to Entity Sub Dir
			gCurrentSubDirectory = "CharacterTextures";

			EquippableRM* equippableRM = pICE->GetEquippableRM();
			if (!equippableRM)
				return false;

			Mesh_EntityObjectDump(meshes, m_movObjRefModelList, core);
			Mesh_EquippableSystemDump(meshes, equippableRM);
			gCurrentSubDirectory = "";

			titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_2);
			statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_2);
			string statusStringUtf8 = Utf16ToUtf8(statusString.GetString());
			pStatus.stepId = 2;
			strcpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), statusStringUtf8.c_str());
			strcpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), statusStringUtf8.c_str());
			strcpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete");
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

			Mesh_GlobalInventoryDump(meshes, m_globalInventoryDB);
			Mesh_MissileDBDump(meshes, m_missileDB);
			Mesh_ExplosionDBDump(meshes, m_explosionDBList);
			Mesh_HousingDBDump(meshes, m_housingDB);

			gCurrentSubDirectory = "Common";
			gListOfTextureDirs.push_back(gCurrentSubDirectory);

			// Add Files To Mesh String Vector
			AddToStrVect(meshes, GetLoadingFileName());
			AddToStrVect(meshes, GetShadowFileName());
			for (size_t i = 0; i < (size_t)CURSOR_TYPES; ++i)
				AddToStrVect(meshes, GetCursorFileName((CURSOR_TYPE)i));
			AddToStrVect(meshes, "512font.dds");

			gCurrentSubDirectory = "";
		}

		if (exportToDb != dbe_dontExportToDb) {
			// Export animation information into database for legacy animation index remapping
			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "Database export", _TRUNCATE);
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_SERVER_UPDATE, (LPARAM)&pStatus);

			StockAnimationGlids stockAnimationGlids;
			DumpAnimationGlids(m_movObjRefModelList, stockAnimationGlids);
			ExportGetSetToDb(m_hWnd, EditorBaseDir().c_str(), baseDir, exportSqlPath, &stockAnimationGlids, true);

			if (exportToDb == dbe_onlyExportToDb)
				return true;
		}

		string gameDir = PathAdd(baseDir, GAMEFILES);

		// read the three autoexec XML files and get a list of files

		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "extra client files", _TRUNCATE);
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_CLIENT_UPDATE, (LPARAM)&pStatus);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetEvent(distCloseEvent);
			return false;
		}

		bool isDir = false;
		if (jsFileExists(PathAdd(baseDir, "extraClientFiles.xml").c_str(), &isDir) && !isDir)
			getExtraFilesFromXml(baseDir, PathAdd(baseDir, "extraClientFiles.xml"), clientFiles, clientDirs);

		if (menusOnly)
			return true;

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetEvent(distCloseEvent);
			return false;
		}

		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "client autoexec.xml", _TRUNCATE);
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_CLIENT_UPDATE, (LPARAM)&pStatus);

		fileToOpen = PathAdd(gameDir.c_str(), "clientautoexec.xml");
		getFilesFromXml(gameDir.c_str(), fileToOpen, clientFiles);
		fileToOpen.clear();

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetEvent(distCloseEvent);
			return false;
		}

		// default core files loaded by clientautoexec.xml
		coreClientFileCount = clientFiles.size();

		strVect xtraAnms;

		// if edb in list, manually add the anm file, too
		// REK 5-15 pushing to vector that is being iterated on breaks.
		// added second vector to store anms then add to clientFiles.
		for (auto jj = clientFiles.begin(); jj != clientFiles.end(); jj++) {
			string l(jj->first);
			STLToLower(l);
			string::size_type pos = l.find(".edb");
			if (pos != string::npos) {
				string anm = jj->first.substr(0, pos);
				anm += ".anm";
				//addStringNoDupes(clientFiles, anm, false );
				xtraAnms.push_back(make_pair(anm, false));
			}
		}

		for (auto kk = xtraAnms.begin(); kk != xtraAnms.end(); kk++) {
			addStringNoDupes(clientFiles, kk->first, kk->second);
		}

		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "supported worlds", _TRUNCATE);
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_CLIENT_UPDATE, (LPARAM)&pStatus);

		// add supported worlds exgs to client list, not to server since AI explicitly loads its scenes
		getFilesFromSupportedWorlds(gameDir.c_str(), m_multiplayerObj->m_currentWorldsUsed, clientFiles, worldFiles);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetEvent(distCloseEvent);
			return false;
		}

		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), ".exg files", _TRUNCATE);
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_CLIENT_UPDATE, (LPARAM)&pStatus);

		strVect files;
		getFilesWithExt(clientFiles, ".exg", files);

		// load each of the exg files to see what they load.  these
		// will be required for client and AI (server)

		for (auto i = files.begin(); i != files.end(); i++) {
			// get the zone index since we can get it in the context of the view

			if (m_multiplayerObj && m_multiplayerObj->m_currentWorldsUsed) {
				// get the filename off the path
				CStringA zoneName(i->first.c_str());
				zoneName = zoneName.Right(zoneName.GetLength() - zoneName.ReverseFind('\\') - 1);
				ZoneIndex zi = m_multiplayerObj->m_currentWorldsUsed->GetZeroBasedIndex(zoneName);

				unsigned int j;
				bool worldFound = false;
				for (j = 0; j < worldFiles.size(); j++) {
					zoneFileVect& currentWorld = worldFiles.at(j);

					for (auto k = currentWorld.begin(); k != currentWorld.end(); k++) {
						if (::strcmp(k->first.c_str(), zoneName) == 0) {
							worldFound = true;
							break;
						}
					}

					if (worldFound)
						break;
				}

				if (worldFound) {
					getFilesFromExg(baseDir, exportSqlPath, i->first.c_str(), zi.toLong(), clientFiles, m_logger, meshes, this, zi != INVALID_ZONE_INDEX ? exportToDb : dbe_dontExportToDb, worldFiles.at(j));

					// For every exg, tristrip it and save it to the output directory
					if (tristripAssets) {
						std::string fileName;

						const char* start = strrchr((const char*)i->first.c_str(), '\\');
						fileName = start + 1;

						m_loadScene = true;
						std::string origWorkingFile = GetZoneFileName().c_str();
						m_loadSceneExgFileName = fileName;
						SetZoneFileName(fileName);

						SetCurrentDirectoryW(Utf8ToUtf16(gameDir).c_str());

						if (ClientEngine::RenderLoop()) {
							SetPathConfigs(gameDir.c_str());
							m_game.SetLoadedZone(gameDir.c_str());

							EditorState::GetInstance()->SetZoneDirty(false);

							SaveSceneObjects(m_loadSceneExgFileName);
						} else {
							m_loadSceneExgFileName = origWorkingFile;
							SetZoneFileName(origWorkingFile);
						}
					}
				} else {
					AfxMessageBox(Utf8ToUtf16("Couldn't find zone " + zoneName).c_str(), MB_ICONEXCLAMATION);
				}
			}
		}

		// run through the clientFiles and tri-strip RLBs
		if (tristripAssets) {
			CStringW CurDir, MainDir;
			wchar_t buffer[_MAX_PATH];
			_wgetcwd(buffer, _MAX_PATH);
			MainDir = CurDir = buffer;
			CurDir = Utf8ToUtf16(PathAdd(Utf16ToUtf8(CurDir.GetString()).c_str(), "GameFiles")).c_str();
			SetCurrentDirectory(CurDir);

			getFilesWithExt(clientFiles, ".rlb", files);
			for (auto i = files.begin(); i != files.end(); i++) {
				CStringA fileName = i->first.c_str();
				fileName = fileName.Right(fileName.GetLength() - fileName.ReverseFind('\\') - 1);

				if (LoadReferenceLibrary(fileName.GetString())) {
					CFileException exc;
					CFile fl;
					BOOL res = OpenCFile(fl, fileName, CFile::modeCreate | CFile::modeWrite, &exc);
					if (res == FALSE) {
						LOG_EXCEPTION(exc, m_logger)
					} else {
						BEGIN_SERIALIZE_TRY
						CArchive the_out_Archive(&fl, CArchive::store);
						the_out_Archive << m_libraryReferenceDB;
						the_out_Archive.Close();
						fl.Close();
						END_SERIALIZE_TRY_NO_LOGGER
					}
				}
			}

			SetCurrentDirectory(MainDir);
		}

		CStringW CurDir, MainDir;
		wchar_t buffer[_MAX_PATH] = L"";
		_wgetcwd(buffer, _MAX_PATH);
		MainDir = CurDir = buffer;
		CurDir = Utf8ToUtf16(PathAdd(Utf16ToUtf8(CurDir.GetString()), "GameFiles")).c_str();
		SetCurrentDirectoryW(CurDir);

		getFilesWithExt(clientFiles, ".rlb", files);
		//		swprintf_s( buffer, _countof(buffer), L"RLB count: %d", files.size() );
		//		OutputDebugStringW( buffer );

		for (auto i = files.begin(); i != files.end(); i++) {
			CStringA fileName = i->first.c_str();
			fileName = fileName.Right(fileName.GetLength() - fileName.ReverseFind('\\') - 1);

			CStringA fileHandle = fileName.Left(fileName.ReverseFind('.'));

			gCurrentSubDirectory = (char*)fileHandle.GetString();
			gCurrentSubDirectory += "RLB";

			gListOfTextureDirs.push_back(gCurrentSubDirectory);

			if (LoadReferenceLibrary(fileName.GetString())) {
				for (POSITION posLocal = m_libraryReferenceDB->GetHeadPosition(); posLocal != NULL;) {
					CReferenceObj* pRef = (CReferenceObj*)m_libraryReferenceDB->GetNext(posLocal);

					if (pRef) {
						for (int jj = 0; jj < NUM_CMATERIAL_TEXTURES; jj++) {
							string texFileName = pRef->m_material->m_texFileName[jj];
							if (ValidTextureFileName(texFileName))
								addStringNoDupes(meshes, texFileName, false);
						}
					}
				}
			}
			gCurrentSubDirectory = "";
		}

		// Dance Party textures
		// This is WOK hardcoding.  Need to move this code to the Dance Party DLL.  Right now it will not affect
		// STAR distro will report missing textures.  For now better than hardcoding textures in extraclientfiles.
		gCurrentSubDirectory = "DanceParty";
		gListOfTextureDirs.push_back(gCurrentSubDirectory);
		addStringNoDupes(meshes, "1.dds", false);
		addStringNoDupes(meshes, "2.dds", false);
		addStringNoDupes(meshes, "3.dds", false);
		addStringNoDupes(meshes, "DanceArrows.dds", false);
		addStringNoDupes(meshes, "DDRLives.dds", false);
		addStringNoDupes(meshes, "DDRNumbers.dds", false);
		addStringNoDupes(meshes, "Go.dds", false);
		addStringNoDupes(meshes, "awesome.dds", false);
		addStringNoDupes(meshes, "excellent.dds", false);
		addStringNoDupes(meshes, "fantastic.dds", false);
		addStringNoDupes(meshes, "great_job.dds", false);
		addStringNoDupes(meshes, "rock_on.dds", false);
		addStringNoDupes(meshes, "perfect_bonus.dds", false);
		addStringNoDupes(meshes, "paused.dds", false);
		addStringNoDupes(meshes, "asterisk.dds", false);
		addStringNoDupes(meshes, "dance_fame_plus.dds", false);
		addStringNoDupes(meshes, "dance_fame_level_up.dds", false);
		addStringNoDupes(meshes, "dance_fame_0.dds", false);
		addStringNoDupes(meshes, "dance_fame_1.dds", false);
		addStringNoDupes(meshes, "dance_fame_2.dds", false);
		addStringNoDupes(meshes, "dance_fame_3.dds", false);
		addStringNoDupes(meshes, "dance_fame_4.dds", false);
		addStringNoDupes(meshes, "dance_fame_5.dds", false);
		addStringNoDupes(meshes, "dance_fame_6.dds", false);
		addStringNoDupes(meshes, "dance_fame_7.dds", false);
		addStringNoDupes(meshes, "dance_fame_8.dds", false);
		addStringNoDupes(meshes, "dance_fame_9.dds", false);
		gCurrentSubDirectory = "";

		// Icon System
		gCurrentSubDirectory = "Icons";
		gListOfTextureDirs.push_back(gCurrentSubDirectory);
		m_globalInventoryDB->traverse(MeshVecBuilder(meshes));
		gCurrentSubDirectory = "";

		// added by Jonny 10/25/08 puts dynamic obj textures in loose folder MapsModels\DynamicObject
		if (m_dynamicObjectRM) {
			gCurrentSubDirectory = "DynamicObject";
			gListOfTextureDirs.push_back(gCurrentSubDirectory);

			ASSERT(false); // move dynamic objects somewhere else?
			vector<GLID> allGLIDs;
			m_dynamicObjectRM->getAllDynamicObjectGLIDs(allGLIDs);
			for (auto glid : allGLIDs) {
				StreamableDynamicObject* dob = m_dynamicObjectRM->getDynamicObjectByGLID(glid);
				for (size_t j = 0; j < dob->getVisualCount(); j++) {
					StreamableDynamicObjectVisual* dov = dob->Visual((unsigned int)j);
					for (int k = 0; k < NUM_CMATERIAL_TEXTURES; k++) {
						CMaterialObject* mat = dov->getMaterial();
						if (mat) {
							string texFileName = mat->m_texFileName[k];
							if (ValidTextureFileName(texFileName))
								addStringNoDupes(meshes, texFileName, false);
						}
					}
				}
			}
			gCurrentSubDirectory = "";
		}

		if (exportToDb == dbe_onlyExportToDb)
			return true;

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetEvent(distCloseEvent);
			return false;
		}

		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), ".sbk files", _TRUNCATE);
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_CLIENT_UPDATE, (LPARAM)&pStatus);

		// get the sound files from each
		getFilesWithExt(clientFiles, ".sbk", files);
		for (auto i = files.begin(); i != files.end(); i++)
			getFilesFromSbk(baseDir, i->first.c_str(), clientFiles);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetEvent(distCloseEvent);
			return false;
		}

		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), ".stk files", _TRUNCATE);
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_CLIENT_UPDATE, (LPARAM)&pStatus);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetEvent(distCloseEvent);
			return false;
		}
		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "maps and models", _TRUNCATE);
		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_CLIENT_UPDATE, (LPARAM)&pStatus);

		// Add the meshes we found that are in the global engine objects and in
		// each zone.
		for (auto i = meshes.begin(); i != meshes.end(); i++) {
			CStringA toMove(i->first.c_str());

			int index = toMove.Find('$');
			if (index != -1) {
				CStringA strippedSubDir = toMove.Right(toMove.GetLength() - index - 1);
				CStringA strippedMainName = toMove.Left(index);

				string combinedPath = PathAdd(MAPSMODELS, strippedSubDir);
				string finalDestPath = PathAdd(combinedPath.c_str(), strippedMainName.GetString());

				if (strippedSubDir.Left(17).CompareNoCase("CharacterTextures") == 0) {
					// Special code for character textures
					processCharacterTexture(i->first, core, baseDir);
				} else {
					// Everything else.
					string subdirfinal = PathAdd(baseDir, finalDestPath.c_str());
					bool isDir;
					if (jsFileExists(subdirfinal.c_str(), &isDir) && !isDir) {
						gFinalTexurePackage.push_back(
							make_pair(finalDestPath.c_str(), finalDestPath.c_str()));
						OutputDebugStringA(finalDestPath.c_str());
					} else {
						subdirfinal = PathAdd(baseDir, MAPSMODELS);
						subdirfinal = PathAdd(subdirfinal.c_str(), strippedMainName.GetString());

						if (jsFileExists(subdirfinal.c_str(), &isDir) && !isDir) {
							gFinalTexurePackage.push_back(
								make_pair(PathAdd(MAPSMODELS, strippedMainName.GetString()), finalDestPath.c_str()));
						} else {
							subdirfinal = PathAdd("MapsModels\\CharacterTextures\\Extended", i->first.c_str());
							subdirfinal = PathAdd(baseDir, subdirfinal.c_str());

							if (jsFileExists(subdirfinal.c_str(), &isDir) && !isDir) {
								//	File exists in Extended
								LogWarn("Tried to add a texture to MapsModels that exists in Extended: " << subdirfinal.c_str());
								continue;
							}

							subdirfinal = PathAdd("MapsModels\\CharacterTextures\\Core", i->first.c_str());
							subdirfinal = PathAdd(baseDir, subdirfinal.c_str());

							if (jsFileExists(subdirfinal.c_str(), &isDir) && !isDir) {
								//	File exists in Extended
								LogWarn("Tried to add a texture to MapsModels that exists in Core: " << subdirfinal.c_str());
								continue;
							}

							//	Here we may need to process the case where we looked for a file everywhere except
							//	in core. Need to figure this out
							LogWarn("Tried to add a texture to the distribution that did not exist anywhere: " << strippedMainName.GetString());
						}
					}
				}
			} else {
				string subdirfinal = PathAdd(baseDir, MAPSMODELS);
				subdirfinal = PathAdd(subdirfinal.c_str(), i->first.c_str());

				if (jsFileExists(subdirfinal.c_str(), &isDir) && !isDir) {
					gFinalTexurePackage.push_back(
						make_pair(PathAdd(MAPSMODELS, i->first.c_str()), PathAdd(MAPSMODELS, i->first.c_str())));
				} else {
					subdirfinal = PathAdd("MapsModels\\CharacterTextures\\Extended", i->first.c_str());
					subdirfinal = PathAdd(baseDir, subdirfinal.c_str());

					if (jsFileExists(subdirfinal.c_str(), &isDir) && !isDir) {
						//	File exists in Extended
						LogWarn("Tried to add a texture to MapsModels that exists in Extended: " << subdirfinal.c_str());
						continue;
					}

					subdirfinal = PathAdd("MapsModels\\CharacterTextures\\Core", i->first.c_str());
					subdirfinal = PathAdd(baseDir, subdirfinal.c_str());

					if (jsFileExists(subdirfinal.c_str(), &isDir) && !isDir) {
						//	File exists in Extended
						LogWarn("Tried to add a texture to MapsModels that exists in Core: " << subdirfinal.c_str());
						continue;
					}

					//	Didn't find the texture at all
					LogWarn("Tried to add a texture to the list that did not exist: " << subdirfinal.c_str());
				}
			}
		}

		processCoreCharacterTextures(core, baseDir);

		ret = true;
	} catch (KEPException* e) {
		CStringA msg;
		msg.Format(IDS_CREATE_DIST_FAILED, Utf8ToUtf16(e->m_msg).c_str());
		if (!fileToOpen.empty()) {
			msg += " ";
			msg += fileToOpen.c_str();
		}
		AfxMessageBoxUtf8(msg, MB_ICONEXCLAMATION);
		e->Delete();
	} catch (CException* ce) {
		CStringW msg;
		wchar_t buf[64];
		ZeroMemory(buf, sizeof(buf));
		ce->GetErrorMessage(buf, _countof(buf));

		msg.Format(IDS_CREATE_DIST_FAILED, buf);
		AfxMessageBox(msg, MB_ICONEXCLAMATION);
		ce->Delete();
	}

	return ret;
}

// Added by Jonny 02/13/08 to handle final texture processing of character textures.
static void processCharacterTexture(string texName, strVect& core, const char* baseDir) {
	CStringA toMove(texName.c_str());

	int index = toMove.Find('$');
	if (index != -1) {
		CStringA strippedSubDir = toMove.Right(toMove.GetLength() - index - 1);
		CStringA strippedMainName = toMove.Left(index);
		bool isDir;
		string patchlocation = PathAdd("MapsModels\\CharacterTextures", strippedMainName.GetString());
		string location = PathAdd("MapsModels\\CharacterTextures\\Extended", strippedMainName.GetString());
		string testLocal = PathAdd(baseDir, location.c_str());
		if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
			gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
		} else {
			location = PathAdd("MapsModels\\CharacterTextures", strippedMainName.GetString());
			testLocal = PathAdd(baseDir, location.c_str());
			if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
				gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
			} else {
				location = PathAdd("MapsModels", strippedMainName.GetString());
				testLocal = PathAdd(baseDir, location.c_str());

				if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
					gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
				} else {
					location = PathAdd("MapsModels\\CharacterTextures\\Core", strippedMainName.GetString());
					testLocal = PathAdd(baseDir, location.c_str());

					//	Check to see if this file exists in core. If so, let them know
					if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
						//	File was found in core
						LogWarn("WARN: Added a texture to Extended that exists in Core: " << testLocal.c_str());
						gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
					} else {
						//	File was not found anywhere.
						LogWarn("Tried to add a texture to the distribution that did not exist anywhere: " << strippedMainName.GetString());
					}
				}
			}
		}
	} else {
		// No sub Dir.
		gFinalTexurePackage.push_back(
			make_pair(PathAdd(MAPSMODELS, texName.c_str()), PathAdd(MAPSMODELS, texName.c_str())));
	}
}

static void processCoreCharacterTextures(strVect& core, const char* baseDir) {
	for (auto i = core.begin(); i != core.end(); i++) {
		CStringA toMove(i->first.c_str());

		int index = toMove.Find('$');
		if (index != -1) {
			CStringA strippedSubDir = toMove.Right(toMove.GetLength() - index - 1);
			CStringA strippedMainName = toMove.Left(index);
			bool isDir;
			string patchlocation = PathAdd("MapsModels\\CharacterTextures\\Core", strippedMainName.GetString());
			string location = PathAdd("MapsModels\\CharacterTextures\\Core", strippedMainName.GetString());
			string testLocal = PathAdd(baseDir, location.c_str());
			if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
				gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
			} else {
				location = PathAdd("MapsModels\\CharacterTextures", strippedMainName.GetString());
				testLocal = PathAdd(baseDir, location.c_str());
				if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
					gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
				} else {
					location = PathAdd("MapsModels", strippedMainName.GetString());
					testLocal = PathAdd(baseDir, location.c_str());

					if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
						gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
					} else {
						location = PathAdd("MapsModels\\CharacterTextures\\Extended", strippedMainName.GetString());
						testLocal = PathAdd(baseDir, location.c_str());

						//	Check to see if this file exists in extended. If so, let them know
						if (jsFileExists(testLocal.c_str(), &isDir) && !isDir) {
							//	File was found in extended
							LogWarn("WARN: Added a texture to Core that exists in Extended: " << testLocal.c_str());
							gFinalTexurePackage.push_back(make_pair(location.c_str(), patchlocation.c_str()));
						} else {
							//	Here we may need to process the case where we looked for a file everywhere except
							//	in core. Need to figure this out
							LogWarn("Tried to add a texture to the distribution that did not exist anywhere: " << strippedMainName.GetString());
						}
					}
				}
			}
		} else {
			// No sub Dir.
			gFinalTexurePackage.push_back(
				make_pair(PathAdd(MAPSMODELS, i->first.c_str()), PathAdd(MAPSMODELS, i->first.c_str())));
		}
	}
}

static const char* timeStamp() {
	time_t t;
	time(&t);
	struct tm now;
	localtime_s(&now, &t);

	const int BUF_SIZE = 100;
	static char s[BUF_SIZE];

	strftime(s, BUF_SIZE, "%x %X", &now);

	return s;
}

static bool isFileRequired(CRCLIST* patchData, int oldFileCount, const char* filename) {
	for (int i = 0; i < oldFileCount; i++) {
		if (_stricmp(filename, patchData[i].filename.c_str()) == 0) {
			// Might have to also check dir in case same filename in mult folders
			return patchData[i].bRequired != FALSE;
		}
	}
	ASSERT(FALSE); // shouldn't ever happen
	return false;
}

static void WriteZonesLua(Logger& m_logger, CRCLIST* filesList, int fileCount, const char* baseDir) {
	FILE* fp;
	string outfile(PathAdd(baseDir, "\\GameFiles\\ScriptData\\Zones.lua"));
	if (fopen_s(&fp, outfile.c_str(), "w") != 0) {
		return;
	}

	if (fp != NULL) {
		fprintf(fp, "-- Kaneva Progressive Zone downloader\n");
		fprintf(fp, "-- This file is generated during create distribution.\n");
		fprintf(fp, "-- Generated by WriteZonesLua at %s\n", timeStamp());
		fprintf(fp, "Zones = {}\n");
		for (int i = 0; i < fileCount; i++) {
			CStringA fname(filesList[i].filename.c_str());
			if (fname.Right(4).CompareNoCase(".exg") == 0 || fname.Right(4).CompareNoCase(".txb") == 0 ||
				fname.Right(4).CompareNoCase(".trg") == 0 || fname.Right(4).CompareNoCase(".rlb") == 0 ||
				fname.Right(4).CompareNoCase(".pak") == 0) {
				fname = fname.Right(fname.GetLength() - fname.Find("---", 0) - 3);
				fprintf(fp, "Zones[\"%s\"] = %d\n", fname.GetString(), filesList[i].nFilesize);
			}
		}
	}
	fclose(fp);
}

static void WriteZoneInfoDat(Logger& m_logger, CRCLIST* crcList, int fileCount, dependVect& worldFiles, const char* baseDir) {
	string filename = PathAdd(baseDir, ZONEINFO_DAT);

	FILE* outFile = 0;
	if (fopen_s(&outFile, filename.c_str(), "w") == 0) {
		fprintf(outFile, "-- Kaneva Progressive Zone files\n");
		fprintf(outFile, "-- This file is generated during create distribution.\n");
		fprintf(outFile, "-- Generated by WriteZoneInfoDat at %s\n", timeStamp());

		// for each zone, write out exg, rlb, trg, and txb
		int zoneIndex = 0;
		for (auto j = worldFiles.begin(); j != worldFiles.end(); j++) {
			string zoneName;
			bool skipZone = false;
			for (auto k = j->begin(); k != j->end(); k++) {
				if (k->second == OPEN_SCENE) {
					skipZone = isFileRequired(crcList, fileCount, (string("GameFiles---" + k->first).c_str()));
					if (!skipZone) {
						zoneName = k->first;
						string::size_type lastDot = zoneName.rfind('.');
						if (lastDot > 0)
							zoneName = zoneName.substr(0, lastDot);
						fprintf(outFile, "z\t%d\t%s\n", zoneIndex, zoneName.c_str());
						fprintf(outFile, "f\tGameFiles---%s\n", k->first.c_str());
					}
				} else if (k->second == LOAD_REFERENCE_LIBRARY && !skipZone) {
					fprintf(outFile, "f\tGameFiles---%s\n", k->first.c_str());
				} else if (k->second == LOAD_TEXTURE_BANK && !skipZone) {
					fprintf(outFile, "f\tGameFiles---%s\n", k->first.c_str());
				}
			}
			zoneIndex++;
		}
		fclose(outFile);
	} else {
		LOG4CPLUS_WARN(m_logger, "Failed to open " << filename);
	}
}

static void WriteZoneFilesLua(Logger& m_logger, CRCLIST* crcList, int fileCount, dependVect& worldFiles, const char* baseDir) {
	string filename = PathAdd(baseDir, "GameFiles\\ScriptData\\ZoneFiles.lua");

	FILE* outFile = 0;
	if (fopen_s(&outFile, filename.c_str(), "w") == 0) {
		fprintf(outFile, "-- Kaneva Progressive Zone files\n");
		fprintf(outFile, "-- This file is generated during create distribution.\n");
		fprintf(outFile, "-- Generated by WriteZoneFilesLua at %s\n", timeStamp());

		fputs("ZoneFiles = {\n", outFile);
		string zoneName;
		char tempString[1024];

		for (auto j = worldFiles.begin(); j != worldFiles.end(); j++) {
			fputs("\t{", outFile);
			tempString[0] = '\0';
			bool sceneFound = false;

			for (auto k = j->begin(); k != j->end(); k++) {
				switch (k->second) {
					case OPEN_SCENE: {
						zoneName = k->first;
						string::size_type lastDot = zoneName.rfind('.');
						if (lastDot > 0)
							zoneName = zoneName.substr(0, lastDot);

						sprintf_s(tempString, _countof(tempString),
							"{\"%s environment\", \"%s\"}, {\"%s textures\", \"%s\\\\%s.pak\"}",
							zoneName.c_str(),
							k->first.c_str(),
							zoneName.c_str(),
							zoneName.c_str(),
							zoneName.c_str());
						sceneFound = true;
					} break;
					case LOAD_REFERENCE_LIBRARY: {
						if (sceneFound) {
							string rlbName = k->first;
							string::size_type lastDot = rlbName.rfind('.');
							if (lastDot > 0)
								rlbName = rlbName.substr(0, lastDot);

							sprintf_s(tempString, _countof(tempString),
								"%s%s{\"%s textures(RLB)\", \"%sRLB\\\\%sRLB.pak\"}, {\"%s objects\", \"%s\"}",
								tempString,
								(strlen(tempString) > 0 ? ", " : ""),
								zoneName.c_str(),
								rlbName.c_str(),
								rlbName.c_str(),
								zoneName.c_str(),
								k->first.c_str());
						}
					} break;
					case LOAD_PARTICLE_SYSTEMS: {
						if (sceneFound) {
							sprintf_s(tempString, _countof(tempString),
								"%s%s{\"%s particles\", \"%s\"}",
								tempString,
								(strlen(tempString) > 0 ? ", " : ""),
								zoneName.c_str(),
								k->first.c_str());
						}
					} break;
				}
			}

			if (strlen(tempString) > 0)
				fputs(tempString, outFile);

			if ((j + 1) == worldFiles.end()) {
				fputs("}\n", outFile);
			} else {
				fputs("},\n", outFile);
			}
		}

		fputs("}", outFile);
		fclose(outFile);
	} else {
		LOG4CPLUS_WARN(m_logger, "Failed to open " << filename);
	}
}

static void WriteProgressiveFiles(Logger& m_logger, dependVect& worldFiles, const char* baseDir) {
	string filename = PathAdd(baseDir, VERSIONINFO_DAT);

	// read the CRCList from versioninfo.dat
	int nTotalFiles = GetPrivateProfileIntW(TEXT("CURRENT"), TEXT("TOTALFILES"), 0, Utf8ToUtf16(filename).c_str());

	CRCLIST* crcList(new CRCLIST[nTotalFiles]);

	if (CDistributionTargetDlg::ReadCRCTable2(nTotalFiles, crcList, filename.c_str())) {
		WriteZonesLua(m_logger, crcList, nTotalFiles, baseDir);
		WriteZoneFilesLua(m_logger, crcList, nTotalFiles, worldFiles, baseDir);
		WriteZoneInfoDat(m_logger, crcList, nTotalFiles, worldFiles, baseDir);
	} else {
		LOG4CPLUS_WARN(m_logger, "Failed to open " << filename);
	}
	delete crcList;
}

bool CKEPEditView::GetServerDistributionFiles(HANDLE distCloseEvent, HANDLE closeEvent,
	strVect& serverFiles, dirVect& serverDirs, const char* baseDir, const char* exportSqlPath, EDBExport exportToDb) {
	bool ret = false;

	STATUSU pStatus;

	dependVect worldFiles;

	try {
		string gameDir = PathAdd(baseDir, GAMEFILES);

		if (exportToDb != dbe_onlyExportToDb) {
			// read the three autoexec XML files and get a list of files

			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "extra server files", _TRUNCATE);
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_SERVER_UPDATE, (LPARAM)&pStatus);

			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetEvent(distCloseEvent);
				return false;
			}

			bool isDir = false;
			if (jsFileExists(PathAdd(baseDir, "extraServerFiles.xml").c_str(), &isDir) && !isDir)
				getExtraFilesFromXml(baseDir, PathAdd(baseDir, "extraServerFiles.xml"), serverFiles, serverDirs);

			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetEvent(distCloseEvent);
				return false;
			}

			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "autoexec.xml", _TRUNCATE);
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_SERVER_UPDATE, (LPARAM)&pStatus);

			getFilesFromXml(gameDir.c_str(), PathAdd(gameDir.c_str(), "autoexec.xml"), serverFiles);

			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetEvent(distCloseEvent);
				return false;
			}

			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "aiautoexec.xml", _TRUNCATE);
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_SERVER_UPDATE, (LPARAM)&pStatus);

			getFilesFromXml(gameDir.c_str(), PathAdd(gameDir.c_str(), "aiautoexec.xml"), serverFiles);

			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetEvent(distCloseEvent);
				return false;
			}

			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "supported worlds", _TRUNCATE);
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_SERVER_UPDATE, (LPARAM)&pStatus);

			// add supported worlds exgs to client list, not to server since AI explicitly loads its scenes
			getFilesFromSupportedWorlds(gameDir.c_str(), m_multiplayerObj->m_currentWorldsUsed, serverFiles, worldFiles);

			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetEvent(distCloseEvent);
				return false;
			}

			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), ".exg files", _TRUNCATE);
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_SERVER_UPDATE, (LPARAM)&pStatus);

			strVect exgs;
			getFilesWithExt(serverFiles, ".exg", exgs);
		}

		// load each of the exg files to see what they load.  these
		// will be required for client and AI (server)
		if (exportToDb != dbe_dontExportToDb) {
			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), "Database export", _TRUNCATE);
			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS_SERVER_UPDATE, (LPARAM)&pStatus);
			ExportGetSetToDb(m_hWnd, EditorBaseDir().c_str(), baseDir, exportSqlPath, this, false);
			if (exportToDb == dbe_onlyExportToDb)
				return true;
		}

		ret = true;
	} catch (KEPException* e) {
		CStringW msg;
		msg.Format(IDS_CREATE_DIST_FAILED, Utf8ToUtf16(e->m_msg).c_str());
		AfxMessageBox(msg, MB_ICONEXCLAMATION);
		e->Delete();
	} catch (CException* ce) {
		CStringW msg;
		wchar_t buf[64];
		ZeroMemory(buf, sizeof(buf));
		ce->GetErrorMessage(buf, _countof(buf));

		msg.Format(IDS_CREATE_DIST_FAILED, buf);
		AfxMessageBox(msg, MB_ICONEXCLAMATION);
		ce->Delete();
	}

	return ret;
}

bool CKEPEditView::CleanDistributionDirectories(CStringA destDir, CStringA patchFolderLocation) {
	bool ret = true;

	bool isDir;

	SHFILEOPSTRUCTW fo;

	memset(&fo, 0, sizeof(fo));
	fo.hwnd = m_hWnd;
	fo.wFunc = FO_COPY;
	fo.fFlags = FOF_NOCONFIRMATION;

	wstring from, to;

	//Since the app developer runs the server from the distribution location to develop server scripts
	//we need to ensure the server scripts are preserved and not deleted.  Need to copy any scripts from
	//the distribution ScriptServerScripts directory to the app GameFiles base directory.
	bool serverFilesExists = jsFileExists(PathAdd(destDir, SERVER_FILE_LOCATION).c_str(), &isDir) && isDir;

	CStringW toDir = Utf8ToUtf16(PathAdd(PathBase(), GAME_FILE_LOCATION)).c_str();

	string fromDirUtf8 = PathAdd(destDir, SERVER_FILE_LOCATION);
	fromDirUtf8 = PathAdd(fromDirUtf8, GAME_FILE_LOCATION).c_str();
	fromDirUtf8 = PathAdd(fromDirUtf8, SCRIPTSERVERDIR).c_str();
	CStringW fromDir = Utf8ToUtf16(fromDirUtf8).c_str();
	if (serverFilesExists && jsFileExists(fromDirUtf8.c_str(), &isDir) && isDir) {
		from = fromDir;
		from += L'\0';
		fo.pFrom = from.c_str();

		to = toDir;
		to += L'\0';
		fo.pTo = to.c_str();

		fo.fFlags = fo.fFlags | FOF_SILENT;

		if (::SHFileOperation(&fo) != 0) {
			ret = false;
		}

		fo.pTo = NULL;
	}

	fo.wFunc = FO_DELETE;

	if (jsFileExists(PathAdd(destDir, CLIENT_FILE_LOCATION).c_str(), &isDir) && isDir) {
		from = Utf8ToUtf16(PathAdd(destDir, CLIENT_FILE_LOCATION));
		from += L'\0';
		fo.pFrom = from.c_str();

		fo.fFlags = fo.fFlags | FOF_SILENT;
		if (::SHFileOperationW(&fo) != 0) {
			ret = false;
		}
	}

	if (serverFilesExists) {
		from = Utf8ToUtf16(PathAdd(destDir, SERVER_FILE_LOCATION));
		from += L'\0';
		fo.pFrom = from.c_str();

		fo.fFlags = fo.fFlags | FOF_SILENT;
		if (::SHFileOperationW(&fo) != 0) {
			ret = false;
		}
	}

	// This is the old patch file location prior to moving the location under the platform
	// install location. Wipe it as well just to support the move.
	if (jsFileExists(PathAdd(destDir, PATCH_FILE_LOCATION).c_str(), &isDir) && isDir) {
		from = Utf8ToUtf16(PathAdd(destDir, PATCH_FILE_LOCATION));
		from += L'\0';
		fo.pFrom = from.c_str();

		fo.fFlags = fo.fFlags | FOF_SILENT;
		if (::SHFileOperationW(&fo) != 0) {
			ret = false;
		}
	}

	if (jsFileExists(patchFolderLocation, &isDir) && isDir) {
		from = Utf8ToUtf16(patchFolderLocation);
		from += L'\0';
		fo.pFrom = from.c_str();

		fo.fFlags = fo.fFlags | FOF_SILENT;
		if (::SHFileOperation(&fo) != 0) {
			ret = false;
		}
	}

	return ret;
}

void CKEPEditView::GetDistributionDirectory(CStringA& destinationPath, CStringA patchFolderLocation, bool useSuppliedPath) {
	bool openInProgFilesPath = true;
	wchar_t progFilesPath[_MAX_PATH];

	if (!destinationPath.IsEmpty()) {
		wcscpy_s(progFilesPath, _countof(progFilesPath), Utf8ToUtf16(destinationPath).c_str());
	} else if (FAILED(SHGetFolderPath(AfxGetMainWnd()->m_hWnd, CSIDL_DRIVES | CSIDL_FLAG_CREATE, NULL, 0, progFilesPath))) {
		openInProgFilesPath = false;
	}

	CStringW title;
	title.LoadString(IDS_CREATE_FULL_DIST);

	BROWSEINFOW bi;
	memset(&bi, 0, sizeof(bi));
	wchar_t destDisplay[_MAX_PATH];
	bi.hwndOwner = m_hWnd;
	bi.lpszTitle = title;
	bi.ulFlags = BIF_NEWDIALOGSTYLE;
	bi.pszDisplayName = destDisplay;

	if (openInProgFilesPath) {
		bi.lpfn = BrowseCallbackProc;
		bi.lParam = (long)progFilesPath;
	}

	wchar_t destDir[_MAX_PATH];

	do {
		if (!useSuppliedPath) {
			LPITEMIDLIST idlist = ::SHBrowseForFolder(&bi);

			if (idlist == NULL || !SHGetPathFromIDList(idlist, destDir)) {
				destinationPath.Empty(); // signify user canceled to caller
				return; // canceled
			}
		} else {
			// Set the internal, working buffer to the supplied value
			// so that I don't have to modify the code too much.
			wcscpy_s(destDir, _MAX_PATH, Utf8ToUtf16(destinationPath).c_str());
		}

		bool destEmpty = true;
		bool patchEmpty = true;

		targetDistributionDirEmpty(Utf16ToUtf8(destDir).c_str(), destEmpty, patchFolderLocation, patchEmpty);

		if ((!destEmpty) ||
			(!patchEmpty)) {
			// 5/1 deleted override key from dest folder

			// turn off this msg box if I supplied the path,
			// because I'm not interacting with the user unless
			// I have an error
			CStringW msg;

			if ((!destEmpty) &&
				(!patchEmpty)) {
				msg.Format(IDS_DIST_PATCH_DIR_NOTEMPTY, destDir, Utf8ToUtf16(patchFolderLocation).c_str());
			} else if (!destEmpty) {
				msg.Format(IDS_DISTRO_DIR_NOTEMPTY, destDir);
			} else if (!patchEmpty) {
				msg.Format(IDS_PATCH_DIR_NOTEMPTY, Utf8ToUtf16(patchFolderLocation).c_str());
			}

			if (!useSuppliedPath && AfxMessageBox(msg, MB_YESNO | MB_ICONEXCLAMATION) == IDNO) {
				continue;
			} else {
				if (!CleanDistributionDirectories(Utf16ToUtf8(destDir).c_str(), patchFolderLocation)) {
					msg.Format(IDS_DISTRIBUTION_FOLDER_CLEAN_ERROR, destDir);
					AfxMessageBox(msg, MB_ICONEXCLAMATION);
					return;
				}
				destinationPath = Utf16ToUtf8(destDir).c_str();
				break;
			}
		} else {
			destinationPath = Utf16ToUtf8(destDir).c_str();
			break;
		}

	} while (true);
}

///---------------------------------------------------------
// CKEPEditView::UpdateGameFile
//
// Update the .kep game file with the gameid that is being published. This game file
// is located in the distribution directory. The game id will later be used to ensure this
// distribution can be published to Kaneva by obtaining the encryption key from Kaneva that
// corressponds to this game id. If the encryption key retrieved from Kaneva is not the same
// key used to encrypt the distribution, publishing will not be allowed.
//
// [Returns]
//  bool - true, success
//         false, error
bool CKEPEditView::UpdateGameFile(CStringA gameId, CStringA clientDir) {
	wchar_t drive[_MAX_DRIVE];
	wchar_t dir[_MAX_DIR];
	wchar_t fname[_MAX_FNAME];
	wchar_t ext[_MAX_EXT];

	CStringW gameFile;
	Game distributionGameFile;

	gameFile = Utf8ToUtf16(GetGame()->FileNameCStr().GetString()).c_str();

	_wsplitpath_s(gameFile, drive, _countof(drive), dir, _countof(dir), fname, _countof(fname), ext, _countof(ext));

	if (clientDir.GetAt(clientDir.GetLength()) != '\\') {
		clientDir += "\\";
	}

	gameFile = Utf8ToUtf16(clientDir).c_str();
	gameFile += fname;
	gameFile += ext;

	return true;
}

///---------------------------------------------------------
// CKEPEditView::CreateFullDistribution
//
// Obtains the client and server distribution files. Encrypts any client
// files that are necessary.
//
// [Returns]
//  None
void CKEPEditView::CreateFullDistribution(HWND dialogHwnd, const char* encryptKey, CStringA gameId, CStringA licenseKey,
	CStringA destinationPath, CStringA patchFolderLocation, HANDLE closeEvent, HANDLE distCloseEvent, EDBExport exportToDb, bool tristripAssets,
	bool encryptAssets, bool skipdialog, CRCLIST* oldpatchData, int oldFileCount, bool menusOnly) {
	HCURSOR prev = SetCursor(::LoadCursor(NULL, IDC_WAIT));

	try {
		gProgressHwnd = dialogHwnd;

		CStringA baseDir = PathBase().c_str();

		bool isDir;

		string serverDir;
		string clientDir;
		string patchDir;

		wchar_t destDir[_MAX_PATH];

		CStringW destinationPathW(Utf8ToUtf16(destinationPath).c_str());
		wcsncpy_s(destDir, _countof(destDir), destinationPathW, _TRUNCATE);

		CStringW exportSqlPath(destinationPathW);
		if (!destinationPath.IsEmpty()) {
			// for db only destinationPath is empty
			if (exportSqlPath.GetAt(exportSqlPath.GetLength()) != L'\\') {
				exportSqlPath += L"\\";
			}

			exportSqlPath += L"ServerFiles";

			if (!mkdirReportErr(exportSqlPath)) {
				SetCursor(prev);
				::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
				return;
			}

			exportSqlPath += L"\\SqlScripts";

			if (!mkdirReportErr(exportSqlPath)) {
				SetCursor(prev);
				::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
				return;
			}

			exportSqlPath += L"\\distribution_export.sql";

			_wunlink(exportSqlPath);
		}

		CStringW titleString;
		CStringW statusString;

		STATUSU pStatus;

		strVect looseTextureDirs;
		strVect clientFiles;
		dirVect clientDirs;
		dependVect zoneFiles;
		clientDir = PathAdd(Utf16ToUtf8(destDir).c_str(), CLIENT_FILE_LOCATION);

		if (!menusOnly) {
			titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_1);
			statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_1);

			pStatus.stepId = 1;

			string statusStringUtf8 = Utf16ToUtf8(statusString.GetString());
			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), statusStringUtf8.c_str(), _TRUNCATE);
			strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), statusStringUtf8.c_str(), _TRUNCATE);

			looseTextureDirs.push_back(make_pair("MapsModels\\CharacterTextures", false));
			looseTextureDirs.push_back(make_pair("MapsModels\\DynamicObject", false));
			looseTextureDirs.push_back(make_pair("MapsModels\\Icons", false));
			looseTextureDirs.push_back(make_pair("GameFiles\\CharacterMeshes", false));
			looseTextureDirs.push_back(make_pair("GameFiles\\CharacterAnimations", false));
			looseTextureDirs.push_back(make_pair("GameFiles\\DynamicObjects", false));
			looseTextureDirs.push_back(make_pair("GameFiles\\EquippableItems", false));

			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetCursor(prev);
				SetEvent(distCloseEvent);
				return;
			}

			// Generate Loose Dynamic Object Files
			string dobDir;
			StrBuild(dobDir, baseDir << "\\GameFiles\\DynamicObjects\\");
			m_dynamicObjectRM->saveLooseFiles(dobDir, m_globalInventoryDB);

			// Populate GetSet can play flash and collsion variable for DB export
			if (m_dynamicObjectRM) {
				ASSERT(false); // move dynamic objects somewhere else?
				vector<GLID> allGLIDs;
				m_dynamicObjectRM->getAllDynamicObjectGLIDs(allGLIDs);
				for (auto glid : allGLIDs) {
					StreamableDynamicObject* dob = m_dynamicObjectRM->getDynamicObjectByGLID(glid);
					dob->populateMediaSupported();
					dob->populateObjectHasCollision();
				}
			}

			// Handle Loose Equippable Items
			string eqpDir;
			StrBuild(eqpDir, baseDir << "\\GameFiles\\EquippableItems\\");
			EquippableRM::Instance()->SaveLooseEquippableItems(eqpDir, m_globalInventoryDB);
		}

		ULONG coreClientFiles = 0;
		if (!GetClientDistributionFiles(distCloseEvent, closeEvent, clientFiles, clientDirs, baseDir, Utf16ToUtf8(exportSqlPath.GetString()).c_str(), exportToDb, tristripAssets, coreClientFiles, zoneFiles, menusOnly)) {
			// Error message displayed by GetClientDistributionFiles method
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		auto_ptr<CRCLIST> newOldPatchData; // so will free it since we "return" on many condistions
		if (oldpatchData == 0 && !clientFiles.empty()) {
			// create a default one
			oldpatchData = new CRCLIST[clientFiles.size()];
			newOldPatchData = auto_ptr<CRCLIST>(oldpatchData);
			memset(oldpatchData, 0, sizeof(CRCLIST) * clientFiles.size());
			oldFileCount = clientFiles.size();
			for (ULONG i = 0; i < clientFiles.size(); i++) {
				// replace last \ with "---" so will match name in MakeDeploy
				string temp(clientFiles[i].first);
				size_t lastSlash = temp.rfind("\\");
				if (lastSlash != string::npos)
					temp.replace(lastSlash, 1, "---");
				oldpatchData[i].filename = temp.c_str();
				if (i < coreClientFiles)
					oldpatchData[i].bRequired = TRUE;
			}
		}

		strVect serverFiles;
		dirVect serverDirs;

		titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_3);
		statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_3);
		pStatus.stepId = 3;
		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetCursor(prev);
			SetEvent(distCloseEvent);
			return;
		}

		if (!GetServerDistributionFiles(distCloseEvent, closeEvent, serverFiles, serverDirs, baseDir, Utf16ToUtf8(exportSqlPath.GetString()).c_str(), exportToDb)) {
			// Error message displayed by GetServerDistributionFiles method
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		// short circuit of only exporting to db
		if (exportToDb == dbe_onlyExportToDb) {
			::SendMessage(gProgressHwnd, gMsgId, MSG_COMPLETE, (LPARAM)&pStatus);
			return;
		}

		serverDir = PathAdd(Utf16ToUtf8(destDir), SERVER_FILE_LOCATION);
		isDir = false;
		if (!mkdirReportErr(serverDir.c_str())) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_4);
		statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_4);
		pStatus.stepId = 4;
		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetCursor(prev);
			SetEvent(distCloseEvent);
			return;
		}

		strVect nada; // do nothing
		// Copy server files first since none are encrypted
		if (!copyDistFiles(gProgressHwnd, serverFiles, serverDirs, baseDir, serverDir.c_str(), m_logger, NULL, nada, menusOnly)) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_5);
		statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_5);
		pStatus.stepId = 5;
		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetCursor(prev);
			SetEvent(distCloseEvent);
			return;
		}
		//
		// if skipdialog, then this doesn't update the default blade XML files.
		// I might need more control over this in the future, in which case I
		// need to modify the script to actually do something with the currently
		// ignored skipdialog parameter (it's not passed to the script).
		// NOTE: JMW REMOVED !skipDialog from calls to postDistStep statements since they need to run
		if (!postDistStep("CustomServerDist.wsf", gProgressHwnd, baseDir, serverDir.c_str(), m_game.FileNameCStr(), licenseKey, "", skipdialog, gameId)) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		if (!mkdirReportErr(clientDir.c_str())) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		// for some reason since textures is empty, but has subdirs, it won't create textures
		string temp(PathAdd(clientDir.c_str(), GAMEFILES));
		if (!mkdirReportErr(temp.c_str())) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		if (!mkdirReportErr(PathAdd(temp.c_str(), "textures").c_str())) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		// Create an FX directory under textures for use by the particle editor. This directory
		// may be empty but is needed by the particle editor.
		if (!mkdirReportErr(PathAdd(PathAdd(temp.c_str(), "textures").c_str(), "fx").c_str())) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_6);
		statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_6);
		pStatus.stepId = 6;
		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetCursor(prev);
			SetEvent(distCloseEvent);
			return;
		}

		if (menusOnly) {
			for (auto i = clientFiles.begin(); i != clientFiles.end();) {
				// if menus only get only menu files
				string::size_type menuPos = i->first.find("\\Menus\\");
				string::size_type menuScriptPos = i->first.find("\\MenuScripts\\");
				if (menuPos != string::npos) {
					i->first.insert(menuPos + 6, CStringA("-") + gameId);
					i++;
				} else if (menuScriptPos != string::npos) {
					i->first.insert(menuScriptPos + 12, CStringA("-") + gameId);
					i++;
				} else
					i = clientFiles.erase(i);
			}
		}

		if (!copyDistFiles(gProgressHwnd, clientFiles, clientDirs, baseDir, clientDir.c_str(), m_logger, &gFinalTexurePackage, looseTextureDirs, menusOnly)) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}

		titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_7);
		statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_7);
		pStatus.stepId = 7;
		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

		if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
			SetCursor(prev);
			SetEvent(distCloseEvent);
			return;
		}

		if (!postDistStep("CustomClientDist.wsf", gProgressHwnd, baseDir, clientDir.c_str(), "", "", "", skipdialog, gameId)) {
			SetCursor(prev);
			::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
			return;
		}
		if (!menusOnly) {
			// pack only if not doing 3D app distro
			pakDistFiles(clientDir.c_str(), looseTextureDirs, m_logger, menusOnly, atol(gameId));
		}
		titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_8);
		statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_8);
		pStatus.stepId = 8;
		strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
		strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

		::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

		bool encryptSkipped = true;

		if (menusOnly) {
			// Hacked by Jonny for dev tools 3D apps so that the game server will know the patch and source asset directores
			string distroInfo = PathAdd(serverDir.c_str(), DISTRO_INFO_FILENAME);
			if (!WriteDistributionInfo(distroInfo, serverDir, (const char*)baseDir, (const char*)patchFolderLocation, "", "", "", "")) {
				::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
				return;
			}
		}
		if (encryptAssets) {
			encryptSkipped = false;
			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetCursor(prev);
				SetEvent(distCloseEvent);
				return;
			}

			if (!menusOnly) {
				string newKey;
				int ret = WriteEncryptionKeyFile(clientDir.c_str(), encryptKey, &newKey);
				if (ret != 0) {
					CStringW s;
					string msg;
					errorNumToString(ret, msg);
					s.Format(IDS_WRITE_ENCRYPT_FAILED, Utf8ToUtf16(msg).c_str());
					AfxMessageBox(s);
					SetCursor(prev);
					::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
					return;
				}

				// Write the gameId to the .kep file in the distribution directory. It will be used
				// to obtain the encryption key from kaneva during publishing to compare against the encryption key
				// that was used to create the distribution.

				if (!UpdateGameFile(gameId, clientDir.c_str())) {
					SetCursor(prev);
					::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
					return;
				}

				if (!encryptClientFiles(gProgressHwnd, closeEvent, distCloseEvent, clientFiles, baseDir, clientDir.c_str(), newKey.c_str(), m_logger)) {
					SetCursor(prev);
					::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
					return;
				}
			}

			patchDir = patchFolderLocation;

			// Create parent PatchFiles folder for specific game.
			CStringA patchParentFolder = patchDir.substr(0, patchDir.find_last_of("\\")).c_str();
			if (!mkdirReportErr(patchParentFolder)) {
				SetCursor(prev);
				::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
				return;
			}

			// Create patch folder for specific game.
			if (!mkdirReportErr(patchDir.c_str())) {
				SetCursor(prev);
				::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
				return;
			}

			titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_8);
			statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_8);
			pStatus.stepId = 8;
			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
			strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
			strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

			if (WaitForSingleObject(closeEvent, 0) == WAIT_OBJECT_0) {
				SetCursor(prev);
				SetEvent(distCloseEvent);
				return;
			}

			if (!makeDeploy(clientDir.c_str(), patchDir.c_str(), closeEvent, distCloseEvent, m_logger, oldpatchData, oldFileCount, GetGame() ? GetGame()->Version() : "1.1", looseTextureDirs, skipdialog)) {
				SetCursor(prev);
				if (::IsWindow(gProgressHwnd)) {
					::SendMessage(gProgressHwnd, gMsgId, MSG_ERROR, (LPARAM)0);
				}
				return;
			}

			// Copy the versioninfo.dat to the ServerFiles directory. The server will use
			// this file to perform a lazy runtime tampering check.
			CopyFileW(Utf8ToUtf16(PathAdd(clientDir.c_str(), VERSIONINFO_DAT)).c_str(), Utf8ToUtf16(PathAdd(serverDir.c_str(), VERSIONINFO_DAT)).c_str(), FALSE);

			// copy versioninfo.dat to game folder so we can re-use it next time
			CopyFileW(Utf8ToUtf16(PathAdd(clientDir.c_str(), VERSIONINFO_DAT)).c_str(), Utf8ToUtf16(PathAdd(baseDir, VERSIONINFO_DAT)).c_str(), FALSE);

			if (!menusOnly)
				WriteProgressiveFiles(m_logger, zoneFiles, baseDir);

			// Status of step 8
			strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);
		}

		if (encryptSkipped) {
			titleString.LoadString(IDS_CREATE_DISTRIBUTION_STEP_8);
			statusString.LoadString(IDS_CREATE_DISTRIBUTION_OP_8);
			pStatus.stepId = 8;
			strncpy_s(pStatus.stepDesc, _countof(pStatus.stepDesc), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
			strncpy_s(pStatus.stepTitle, _countof(pStatus.stepTitle), Utf16ToUtf8(statusString.GetString()).c_str(), _TRUNCATE);
			strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Complete", _TRUNCATE);

			::SendMessage(gProgressHwnd, gMsgId, MSG_STATUS, (LPARAM)&pStatus);

			// Status of step 8
			strncpy_s(pStatus.lastItemCompleteTxt, _countof(pStatus.lastItemCompleteTxt), "Skipped", _TRUNCATE);
		}

		/*Added by jerome.
		This will convert the Encryption.dat to version info in the destination directory.
		It will also copy version Zoneinfo to the PatchFile destination folder*/
		CStringA encrypfilename = baseDir + "Encryption.dat";
		CStringA encryptdest = destinationPath + "\\" + VERSIONINFO_DAT;
		CStringA zoneinfosrc = baseDir + ZONEINFO_DAT;
		CStringA zoneinfodst = patchFolderLocation + "\\" + ZONEINFO_DAT;
		FILE* file = 0;
		if (fopen_s(&file, encrypfilename, "r") == 0 && file) {
			fclose(file);
			CopyFileW(Utf8ToUtf16(encrypfilename).c_str(), Utf8ToUtf16(encryptdest).c_str(), false);
		}

		if (fopen_s(&file, zoneinfosrc, "r") == 0 && file) {
			fclose(file);
			CopyFileW(Utf8ToUtf16(zoneinfosrc).c_str(), Utf8ToUtf16(zoneinfodst).c_str(), false);
		}

		//Unpack all the pak files in the current directory including
		//sub directories.
		CStringA dest = CStringA(destinationPath) + "\\" + "ClientFiles" + "\\";
		string s((const char*)dest);
		KPack::UnpackDirectoryExtension((char*)s.c_str());

		CreateLocalCondatVer(clientDir.c_str());

		::SendMessage(gProgressHwnd, gMsgId, MSG_COMPLETE, (LPARAM)&pStatus);
	} catch (KEPException* e) {
		AfxMessageBox(Utf8ToUtf16(e->m_msg.c_str()).c_str(), MB_ICONEXCLAMATION);
		e->Delete();
	} catch (CException* ce) {
		CStringW msg;
		wchar_t buf[64];
		ZeroMemory(buf, sizeof(buf));
		ce->GetErrorMessage(buf, _countof(buf));

		msg.Format(IDS_CREATE_DIST_FAILED, buf);
		AfxMessageBox(msg, MB_ICONEXCLAMATION);
		ce->Delete();
	}
	SetCursor(prev);
}

void CKEPEditView::CreateLocalCondatVer(CStringA clientDir) {
	bool isDir = false;

	if ((!jsFileExists(PathAdd(clientDir, "condat.ver").c_str(), &isDir)) && (!isDir)) {
		CStringA machineName;
		machineName = "localhost";

		FILE* fp = 0;
		if (fopen_s(&fp, PathAdd(clientDir, "condat.ver").c_str(), "w") == 0 && fp != NULL) {
			fwrite((const char*)machineName, machineName.GetLength(), 1, fp);
			fclose(fp);
		}
	}
}

BEGIN_GETSET_IMPL(ZoneObjects, IDS_ZONE_OBJECTS)
GETSET_OBLIST_PTR(WldObjList, GS_FLAGS_DEFAULT, 0, 0, ZONEOBJECTS, WORLDOBJECTS)
GETSET(zoneIndex, gsLong, GS_FLAGS_DEFAULT, 0, 0, ZONEOBJECTS, ZONEINDEX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(StockAnimationGlids, IDS_STOCKANIMATIONGLIDS_NAME)
GETSET2_OBLIST_PTR(m_data, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLIDS_DATA, IDS_STOCKANIMATIONGLIDS_DATA)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(StockAnimationGlid, IDS_STOCKANIMATIONGLID_NAME)
GETSET2(m_ownerType, gsString, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLID_OWNERTYPE, IDS_STOCKANIMATIONGLID_OWNERTYPE)
GETSET2(m_ownerName, gsString, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLID_OWNERNAME, IDS_STOCKANIMATIONGLID_OWNERNAME)
GETSET2(m_animationIndex, gsInt, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLID_ANIMATIONINDEX, IDS_STOCKANIMATIONGLID_ANIMATIONINDEX)
GETSET2(m_animationType, gsInt, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLID_ANIMATIONTYPE, IDS_STOCKANIMATIONGLID_ANIMATIONTYPE)
GETSET2(m_animationVersion, gsInt, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLID_ANIMATIONVERSION, IDS_STOCKANIMATIONGLID_ANIMATIONVERSION)
GETSET2(m_animationGlid, gsInt, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLID_ANIMATIONGLID, IDS_STOCKANIMATIONGLID_ANIMATIONGLID)
GETSET2(m_animationName, gsString, GS_FLAG_EXPOSE, 0, 0, IDS_STOCKANIMATIONGLID_ANIMATIONNAME, IDS_STOCKANIMATIONGLID_ANIMATIONNAME)
END_GETSET_IMPL
