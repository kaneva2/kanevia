/******************************************************************************
 OpenZoneDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// OpenZoneDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "OpenZoneDlg.h"
#include <jsEnumFiles.h>
#include ".\openzonedlg.h"

// OpenZoneDlg dialog

IMPLEMENT_DYNAMIC(OpenZoneDlg, CDialog)
OpenZoneDlg::OpenZoneDlg(const char* gameDir, CWnd* pParent /*=NULL*/) :
		CDialog(OpenZoneDlg::IDD, pParent), m_gameDir(gameDir) {
}

OpenZoneDlg::~OpenZoneDlg() {
}

void OpenZoneDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ZONE_LIST, m_zoneList);
}

BOOL OpenZoneDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	// find all the exg files and put them in a list
	jsEnumFiles ef(Utf8ToUtf16(PathAdd(m_gameDir, "*.exg")).c_str());
	while (ef.next()) {
		m_zoneList.AddString(ef.currentFileName());
	}
	if (m_zoneList.GetCount() == 0)
		GetDlgItem(IDOK)->EnableWindow(FALSE);

	return TRUE;
}

BEGIN_MESSAGE_MAP(OpenZoneDlg, CDialog)
ON_BN_CLICKED(IDOK, OnBnClickedOk)
ON_LBN_DBLCLK(IDC_ZONE_LIST, OnLbnDblclkZoneList)
END_MESSAGE_MAP()

// OpenZoneDlg message handlers
void OpenZoneDlg::OnBnClickedOk() {
	if (m_zoneList.GetCurSel() == -1) {
		AfxMessageBox(IDS_SELECT_A_ZONE, MB_ICONEXCLAMATION);
		return;
	}
	CStringW fnameW;
	m_zoneList.GetText(m_zoneList.GetCurSel(), fnameW);
	m_fname = Utf16ToUtf8(fnameW).c_str();
	OnOK();
}

void OpenZoneDlg::OnLbnDblclkZoneList() {
	OnBnClickedOk();
}
