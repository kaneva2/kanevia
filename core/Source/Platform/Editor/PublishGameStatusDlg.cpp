/******************************************************************************
 PublishGameStatusDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// PublishGameStatusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PublishGameStatusDlg.h"

CProgressCtrl CPublishGameStatusDlg::m_CrtlProgressBar;
float CPublishGameStatusDlg::m_tCurrentTime;
float CPublishGameStatusDlg::m_tLastTime;
float CPublishGameStatusDlg::m_tElapsedTime;
HANDLE CPublishGameStatusDlg::m_hEvPublishHasClosed;

PROCESS_INFORMATION *CPublishGameStatusDlg::m_pi;
CStringA CPublishGameStatusDlg::m_logFilePath;
DWORD CPublishGameStatusDlg::m_dwRead;
DWORD CPublishGameStatusDlg::m_dwWritten;
CHAR *CPublishGameStatusDlg::m_chBuf;
HANDLE CPublishGameStatusDlg::m_hchildStdoutRead;
CStringW CPublishGameStatusDlg::m_errMsg;
CStringA CPublishGameStatusDlg::m_distFolder;
CStringW CPublishGameStatusDlg::m_successMsg;
CStringA CPublishGameStatusDlg::m_strStatusMsg;
float CPublishGameStatusDlg::m_iPublishTotalGoals;
float CPublishGameStatusDlg::m_iGoalsCompleted;

// CPublishGameStatusDlg dialog
UINT gMsgPubId = ::RegisterWindowMessageW(L"KEI_PUBLISH_STATUS_MSG");

IMPLEMENT_DYNAMIC(CPublishGameStatusDlg, CDialog)

CPublishGameStatusDlg::CPublishGameStatusDlg(CWnd *pParent /*=NULL*/, PROCESS_INFORMATION *pi, CStringA logFilePath,
	DWORD dwRead, DWORD dwWritten, CHAR *chBuf, HANDLE hchildStdoutRead, CStringA errMsg, CStringA distFolder, CStringA successMsg) :
		CDialog(CPublishGameStatusDlg::IDD, pParent) {
	m_iMsgPubId = gMsgPubId;
	m_pi = pi;
	m_logFilePath = logFilePath;
	m_dwRead = dwRead;
	m_dwWritten = dwWritten;
	m_chBuf = chBuf;
	m_hchildStdoutRead = hchildStdoutRead;
	m_errMsg = Utf8ToUtf16(errMsg).c_str();
	m_distFolder = distFolder;
	m_successMsg = Utf8ToUtf16(successMsg).c_str();

	m_iGoalsCompleted = 0;
	m_iPublishTotalGoals = 0;
	m_strStatusMsg = "";
}

CPublishGameStatusDlg::~CPublishGameStatusDlg() {
}

void CPublishGameStatusDlg::DoDataExchange(CDataExchange *pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS1, m_CrtlProgressBar);
	DDX_Control(pDX, IDC_STATUS_UPDATE, m_CrtStatusUpdate);
}

BEGIN_MESSAGE_MAP(CPublishGameStatusDlg, CDialog)
ON_REGISTERED_MESSAGE(gMsgPubId, onStatusMessage)
END_MESSAGE_MAP()

// This method will handle any gui events that need to take place such as
// updating the progress bar.  This method will be updated by the BuildSetup method.
afx_msg LRESULT CPublishGameStatusDlg::onStatusMessage(WPARAM wp, LPARAM lp) {
	switch (wp) {
		case START_PUBLISH_PROCESS:
			//AfxMessageBox("Starting...");
			break;
		case UPDATE_PUBLISH_PROGRESS:
			m_CrtlProgressBar.SetPos((m_iGoalsCompleted / m_iPublishTotalGoals) * 100.0f);
			m_CrtlProgressBar.UpdateWindow();
			m_CrtlProgressBar.Invalidate();
			break;
		case UPDATE_PUBLISH_STATUS:
			m_CrtStatusUpdate.SetWindowText(Utf8ToUtf16(m_strStatusMsg).c_str());
			m_CrtStatusUpdate.UpdateWindow();
			break;
		case PUBLISH_PROGRESS_COMPLETE:
			m_iGoalsCompleted = 0;
			m_iPublishTotalGoals = 0;
			m_strStatusMsg = "";
			OnOK();
			break;
	}
	return 0;
}

// This method basically parses the NSIS pipe for
// publish goal defines.  Those defines allow us to now where we
// are in the publishing process as well as which section NSIS is currently
// compiling.  Once this information is retrieved, we send a windows messge
// to the publish gui to update the progress bar and status label.
UINT CPublishGameStatusDlg::BuildSetup(LPVOID pParam) {
	m_iGoalsCompleted = 0;
	m_iPublishTotalGoals = -1;
	HANDLE outputFile;
	outputFile = CreateFileW(Utf8ToUtf16(m_logFilePath).c_str(),
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (outputFile == INVALID_HANDLE_VALUE) {
		m_errMsg.Format(IDS_PUBLISH_STAR_MAKE_SERVER_OUTUT_LOG_ERROR, Utf8ToUtf16(m_logFilePath).c_str());
		AfxMessageBox(m_errMsg, MB_ICONEXCLAMATION);
		return 1;
	}

	bool outputFileErr = false;

	for (;;) {
		if (!ReadFile(m_hchildStdoutRead, m_chBuf, 4096, &m_dwRead, NULL) || m_dwRead == 0) {
			break;
		}

		// If we don't know how many goals there are, then first parse out this information.
		if (m_iPublishTotalGoals == -1) {
			CStringA str = m_chBuf;
			int iStartingIndex = str.Find("!define: \"PUBLISH_TOTAL_GOALS\"", 0);
			if (iStartingIndex != -1) {
				int iEndingIndex = str.Find("\n", iStartingIndex);
				if (iEndingIndex != -1) {
					char chCmdBuf[50];
					ZeroMemory(chCmdBuf, sizeof(chCmdBuf));
					memcpy_s(chCmdBuf, sizeof(chCmdBuf) - 1, m_chBuf + iStartingIndex, iEndingIndex - iStartingIndex);
					//str.CopyChars(chCmdBuf,sizeof(chCmdBuf)-1,m_chBuf + iStartingIndex,iEndingIndex - iStartingIndex);
					str = chCmdBuf;
					str.Remove('\"');
					iEndingIndex = str.Find("=");
					if (iEndingIndex != -1) {
						str.Delete(0, ++iEndingIndex);
						m_iPublishTotalGoals = atoi(str);
					} else {
						CStringW strError;
						strError.LoadString(IDS_ERROR_PARSING_TOTAL_PUBLISH_GOALS);
						AfxMessageBox(strError);
						return 1;
					}
				} else {
					CStringW strError;
					strError.LoadString(IDS_ERROR_PARSING_TOTAL_PUBLISH_GOALS);
					AfxMessageBox(strError);
					return 1;
				}
			}
		}

		// A Goal may be in the current buffer we have available
		if (m_iPublishTotalGoals > -1) {
			CStringA str = m_chBuf;
			int iStartingIndex = -1;
			int iEndingIndex = 0;
			char chCmdBuf[100];
			do {
				// Search for the Goals and parse out the description text for the gui's status label.
				int iStartingIndex = str.Find("!define: \"PUBLISH_GOAL", iEndingIndex);
				if (iStartingIndex != -1) {
					iEndingIndex = str.Find("\n", iStartingIndex);
					if (iEndingIndex != -1) {
						ZeroMemory(chCmdBuf, sizeof(chCmdBuf));
						memcpy_s(chCmdBuf, sizeof(chCmdBuf) - 1, m_chBuf + iStartingIndex, iEndingIndex - iStartingIndex);
						//str.CopyChars(chCmdBuf,sizeof(chCmdBuf)-1,m_chBuf + iStartingIndex,iEndingIndex - iStartingIndex);
						str = chCmdBuf;
						iEndingIndex = str.Find("=");
						if (iEndingIndex != -1) {
							str.Delete(0, ++iEndingIndex);
							str.Remove('\"');
							m_strStatusMsg = str;
							m_iGoalsCompleted++;
							::SendMessage(((CPublishGameStatusDlg *)pParam)->m_hWnd, gMsgPubId, UPDATE_PUBLISH_STATUS, 0);
						}
						::SendMessage(((CPublishGameStatusDlg *)pParam)->m_hWnd, gMsgPubId, UPDATE_PUBLISH_PROGRESS, 0);
					} else {
						CStringW strError;
						strError.LoadString(IDS_ERROR_PARSING_PUBLISH_GOAL);
						AfxMessageBox(strError);
					}
				}
			} while (iEndingIndex < sizeof(m_chBuf) && iStartingIndex != -1);
		}

		if (!WriteFile(outputFile, m_chBuf, m_dwRead, &m_dwWritten, NULL)) {
			outputFileErr = true;
			break;
		}
	}

	CloseHandle(outputFile);

	DWORD processRet = 0;

	if (!GetExitCodeProcess(m_pi->hProcess, &processRet) || processRet || outputFileErr) {
		CStringW logLocation;
		logLocation = Utf8ToUtf16(m_distFolder).c_str();
		logLocation += L"\\STARServerSetup.log";
		m_errMsg.Format(IDS_PUBLISH_STAR_MAKE_SERVER_INSTALL_FAIL2, processRet, logLocation);

		AfxMessageBox(m_errMsg, MB_ICONEXCLAMATION);
	} else {
		CStringW installLocation;
		installLocation = Utf8ToUtf16(m_distFolder).c_str();
		installLocation += L"\\STARServerSetup.exe";
		m_successMsg.Format(IDS_PUBLISH_STAR_MAKE_SERVER_INSTALL_SUCCESS, installLocation);
		AfxMessageBox(m_successMsg, MB_ICONINFORMATION);
	}

	::SendMessage(((CPublishGameStatusDlg *)pParam)->m_hWnd, gMsgPubId, PUBLISH_PROGRESS_COMPLETE, 0);

	return 0;
}

// CPublishGameStatusDlg message handlers
BOOL CPublishGameStatusDlg::OnInitDialog() {
	CDialog::OnInitDialog();
	//Initialize the publish gui properly.
	m_CrtlProgressBar.SetRange(0, 100);
	m_CrtlProgressBar.SetPos(0);
	m_CrtStatusUpdate.HideCaret();
	AfxBeginThread(BuildSetup, (LPVOID)this, THREAD_PRIORITY_NORMAL);

	return true;
}
