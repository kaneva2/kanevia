/******************************************************************************
 EXScriptHelper.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

class CEXScriptHelper {
public:
	CEXScriptHelper(void);
	~CEXScriptHelper(void);

	enum SCRIPT_FILE_TYPES { GAME = 1,
		ZONE,
		AI,
		CLIENT };

	//add a new event to the script file and save
	//if updateIfExists is set to be true, existing event with same key will be replaced instead
	static BOOL AddEvent(int scriptType, CEXScriptObj* eventObjPtr, BOOL updateIfExists = TRUE);

	//get the script file name
	static CStringA GetScriptFileName(int scriptType);
};
