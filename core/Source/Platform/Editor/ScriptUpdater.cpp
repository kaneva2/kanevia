/******************************************************************************
 ScriptUpdater.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "ScriptUpdater.h"

#include "../KEPEdit/GetSetContextHandler.h" // todo include in here since this is only place used

namespace KEP {

ScriptUpdater::ScriptUpdater(GetSet *baseItem) :
		GetSetUpdateData(baseItem) {
	//todo
	//	m_textures.clear();
	//	ULONG trace = 0;
	//
	//	m_textures.push_back( gsListItem("NONE", 0 ) );
	//	for(POSITION posLoc = textureDB->GetHeadPosition();posLoc!=NULL;)
	//	{
	//		CTextureDBObj * texture = (CTextureDBObj *)textureDB->GetNext(posLoc);
	//		char s[255];
	//		_snprintf( s, 255, "%d %s", trace, texture->m_textureName);
	//		string num(s);
	//
	//		m_textures.push_back( gsListItem( num, trace ) );
	//		trace++;
	//	}
	//	m_textures.SetCurSel( item->m_textureIndex+1 );
}

enum GetSetUpdateData::EContextRet ScriptUpdater::ContextMenu(GetSet *item, int screenX, int screenY, CWnd *parent) {
	GetSetContextHandler ch;
	RECT rect;
	ZeroMemory(&rect, sizeof(rect));
	ch.Create(NULL, "GetSetContextHandler", 0, rect, parent, 1);
	ch.ShowWindow(SW_SHOW); // or else don't get WM_COMMAND ???

	CMenu menu;

	menu.CreatePopupMenu();

	// TESTTESTTEST
	bool x = item == m_item; // if equal, at list level, otherwise, must be an item
	if (x) {
		menu.AppendMenu(MF_STRING, 1, "Add event");
		menu.AppendMenu(MF_STRING, 2, "Load");
		menu.AppendMenu(MF_STRING, 2, "Save");
	} else {
		menu.AppendMenu(MF_STRING, 1, "Delete");
		menu.AppendMenu(MF_STRING, 2, "Move up");
		menu.AppendMenu(MF_STRING, 3, "Move down");
	}

	menu.TrackPopupMenu(TPM_LEFTALIGN, screenX, screenY, &ch);

	return crNoAction;
}

} // namespace KEP
