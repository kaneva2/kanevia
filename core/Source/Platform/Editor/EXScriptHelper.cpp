/******************************************************************************
 EXScriptHelper.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "Direct.h"
#include "CEXScriptClass.h"
#include "exscripthelper.h"
#include "KEPException.h"

CEXScriptHelper::CEXScriptHelper(void) {
}

CEXScriptHelper::~CEXScriptHelper(void) {
}

BOOL CEXScriptHelper::AddEvent(int scriptType, CEXScriptObj *eventObjPtr,
	BOOL updateIfExists) {
	//load script list
	ScopedDirectoryChanger sdc("\\GameFiles\\");

	CEXScriptObjList *scriptListPtr = new CEXScriptObjList();
	CStringA fileName = GetScriptFileName(scriptType);
	try {
		FileName fn(fileName.GetString());
		scriptListPtr->SerializeFromXML(fn);
	} catch (KEPException *ex) {
		ex->Delete();
		return FALSE;
	}

	CEXScriptObj *existingEvtPtr = 0;
	POSITION posLoc;
	for (posLoc = scriptListPtr->GetHeadPosition(); posLoc != NULL;) {
		CEXScriptObj *evtPtr = (CEXScriptObj *)scriptListPtr->GetNext(posLoc);
		if (evtPtr->m_actionAttribute == eventObjPtr->m_actionAttribute) {
			existingEvtPtr = evtPtr;
			break;
		}
	}
	if (existingEvtPtr && updateIfExists) {
		//replace value
		existingEvtPtr->m_miscString = eventObjPtr->m_miscString;
	} else {
		//add event to the top
		scriptListPtr->AddHead(eventObjPtr);
	}

	//save it
	scriptListPtr->SerializeToXML(scriptListPtr->GetLastSavedPath());

	return TRUE;
}

CStringA CEXScriptHelper::GetScriptFileName(int scriptType) {
	CStringA retVal = "";
	switch (scriptType) {
		case GAME:
			retVal = "autoexec.xml";
			break;
		case ZONE:
			retVal = "autoexec.xml";
			break;
		case AI:
			retVal = "AIautoexec.xml";
			break;
		case CLIENT:
			retVal = "clientautoexec.xml";
			break;
		default:
			break;
	}
	return retVal;
}
