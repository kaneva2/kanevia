/******************************************************************************
 EditorState.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <string>
#include <SerializeHelper.h>
#include "KEPObject.h"
#include "IXmlSerializableObj.h"
#include "IKEPGameFile.h"
#include <set>

class EditorState {
public:
	typedef std::set<IKEPGameFile**> IKEPGameFileSet;
	typedef IKEPGameFileSet::iterator IKEPGameFileIterator;

	static EditorState* GetInstance();
	virtual ~EditorState();

	/**
		*Save all dirty objects
		**/
	void SaveAll();

	/**
		*Return all dirty files
		**/
	void GetDirtyGameFiles(IKEPGameFileSet& dirtyFiles);

	/**
		*Register an file to monitor
		**/
	void RegisterGameFile(IKEPGameFile** gameFile);

	/**
		*UnRegister an file to monitor
		*
		**/
	void UnRegisterGameFile(IKEPGameFile** gameFile);
	void UnRegisterAllGameFiles();
	/**
		*returns true if there are dirty objects
		**/
	bool IsDirty();

	bool IsZoneDirty() const { return m_zoneDirty; }
	void SetZoneDirty(bool dirty = true) { m_zoneDirty = dirty; }

protected:
	EditorState();
	EditorState(const EditorState&);
	EditorState& operator=(const EditorState&);

private:
	static EditorState* inst;
	IKEPGameFileSet m_gameFiles;

	bool m_zoneDirty; //whether zone file needs to be updated
};