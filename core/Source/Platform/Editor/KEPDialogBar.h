#pragma once
// KEPDialogbar.h
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved#pragma once

#include "KEPControlBar.h"

class IKEPDialogProcessor;

class CKEPDialogBar : public CKEPControlBar {
public:
	CKEPDialogBar(int minW, int minH, int prefW, int prefH, bool vScrollBar = true);
	virtual ~CKEPDialogBar();
	virtual void OnUpdateCmdUI(CFrameWnd *pTarget, BOOL bDisableIfNoHndler);
	void SetDialogBar(IKEPDialogProcessor *processor, UINT dlgId, UINT dlgType = 0);
	void SetDialogBar(CWnd *dlg, UINT dlgId);
	CWnd *Dialog() { return m_dlg; }

	bool cleanUp();

protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	void updateScrollInfo(IN DWORD size);

	bool m_useVScroll;
	CFont m_font;
	CWnd *m_dlg;
	CScrollBar m_vScroll;
	DWORD m_vScrollWidth;
	DWORD m_origDialogHeight;
	int m_barScroll;
	bool m_isDialog; // is it a CDialog vs a dialog bar

protected:
	//{{AFX_MSG(CKEPDialogBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar *pScrollBar);
};
