/******************************************************************************
 KEPDialog.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "resource.h"
/**
*Many CDialog type first level dialogs derive from this type
*It has additional functions to track changes made in the dialog
**/
class CKEPDialog : public CDialog {
	DECLARE_DYNAMIC(CKEPDialog)

public:
	CKEPDialog(UINT id, CWnd* pParent = NULL); // standard constructor
	virtual ~CKEPDialog();

protected:
	/**
		*whether changes user made have been applied. TRUE if no changes have been made
		**/
	bool m_settingsApplied;

	/**
		*whether user has made any changes to the dialog at all
		**/
	bool m_settingsChanged;

	/**
		*user has made changes to the dialog settings
		**/
	void SettingsChanged();

	/**
		*user has applied the changes made to the dialog settings
		**/
	void SettingsApplied();

	/**
		*user has applied and saved the changes made to the dialog settings
		**/
	void SettingsSaved();

public:
	bool IsSettingsApplied() const { return m_settingsApplied; }

	bool IsSettingsChanged() const { return m_settingsChanged; }
	void SetSettingsChanged(bool changed) { m_settingsChanged = changed; }

	virtual bool ApplyChanges();

	DECLARE_MESSAGE_MAP()
	/**
		*changes have been made in property controls
		**/
	virtual afx_msg LRESULT OnPropertyCtrlChanged(WPARAM wp, LPARAM lp);
};
