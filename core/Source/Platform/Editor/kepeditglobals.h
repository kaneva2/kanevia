/******************************************************************************
 kepeditglobals.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

//IDCs for the manually created controls
#define KEP_PROJECTTREE 9000
#define KEP_PROPERTYTREE 9001
#define KEP_STATUSBAR 9002
#define KEP_DIALOGBAR 9003

#define KEP_PROP_PROJECT 0

#define KEP_TABCHANGE WM_USER + 9000
#define KEP_TREECHANGE KEP_TABCHANGE + 1
#define KEP_DUMMY KEP_TREECHANGE + 1

typedef struct _KEPSELECT {
	std::wstring wstrObjectLink;
} KEPSELECT, *LPKEPSELECT;

static wchar_t BASED_CODE szSection[] = L"Settings";
static wchar_t BASED_CODE szWindowPos[] = L"WindowPos";
static wchar_t BASED_CODE szNewGameDialog[] = L"NewGameDialog";
static wchar_t szFormat[] = L"%u,%u,%d,%d,%d,%d,%d,%d,%d,%d";
