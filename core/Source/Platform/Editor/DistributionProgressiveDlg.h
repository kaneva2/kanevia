/******************************************************************************
 DistributionProgressiveDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxwin.h"

class DistributionProgressiveDlg : public CDialog {
	DECLARE_DYNAMIC(DistributionProgressiveDlg)

public:
	DistributionProgressiveDlg(CWnd* pParent = NULL, CRCLIST* inputCRCLIST = NULL, int crcFileCount = 0);
	virtual ~DistributionProgressiveDlg();
	UINT ProcessList(bool skipDialog) {
		if (skipDialog) {
			refreshLists(skipDialog);
			return IDOK;
		} else
			return DoModal();
	}
	// Dialog Data
	enum { IDD = IDD_DIALOG_PROGRESSIVE_DISTRIBUTION };
	CListBox m_listRequired;
	CListBox m_listProgressive;
	CStatic m_staticProgressiveSize;
	CStatic m_staticRequiredSize;
	CStatic m_staticProgressiveCount;
	CStatic m_staticRequiredCount;
	CStatic m_staticTotalCount;
	CStatic m_staticTotalSize;
	CStatic m_staticSelectedName;
	CStatic m_staticSelectedSize;
	CComboBox m_comboFileCommand;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	BOOL OnInitDialog();
	void OnBTNMakeProgressive();
	void OnBTNMakeRequired();
	void refreshLists(bool skipDialog = false);
	void OnSelchangeListRequired();
	void OnSelchangeListProgressive();
	void OnSelchangeComboCommand();
	DECLARE_MESSAGE_MAP()
public:
	CRCLIST* m_CRCList;
	int m_FileCount;
	afx_msg void OnBnClickedButtonMove();
	afx_msg void OnBnClickedButtonMovespecifics();
	CEdit m_requiredextension; //Added by jerome.
	CButton m_requiredmoveall; //Added by jerome.
};
