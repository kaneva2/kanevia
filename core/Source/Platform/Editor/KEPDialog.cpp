/******************************************************************************
 KEPDialog.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "StdAfx.h"
#include ".\kepdialog.h"
#include "KEPPropertiesCtrl.h"
#include "EditorState.h"

static EditorState* editorState = EditorState::GetInstance();

IMPLEMENT_DYNAMIC(CKEPDialog, CDialog)
CKEPDialog::CKEPDialog(UINT id, CWnd* pParent /*=NULL*/) :
		CDialog(id, pParent) {
	m_settingsApplied = TRUE;
	m_settingsChanged = FALSE;
}

CKEPDialog::~CKEPDialog() {
}

BEGIN_MESSAGE_MAP(CKEPDialog, CDialog)
ON_REGISTERED_MESSAGE(WM_KEP_PROPERTY_CTRL_CHANGED, OnPropertyCtrlChanged)
END_MESSAGE_MAP()

void CKEPDialog::SettingsChanged() {
	m_settingsChanged = TRUE;
	m_settingsApplied = FALSE;
}

void CKEPDialog::SettingsApplied() {
	m_settingsApplied = TRUE;
}

bool CKEPDialog::ApplyChanges() {
	m_settingsApplied = TRUE;
	return true;
}

afx_msg LRESULT CKEPDialog::OnPropertyCtrlChanged(WPARAM wp, LPARAM lp) {
	SettingsChanged();
	return 0;
}