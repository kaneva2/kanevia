// KEPEditDoc.cpp : implementation of the CKEPEditDoc class
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved

#include "stdafx.h"
#include "KEPEdit.h"

#include "KEPEditDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CKEPEditDoc

IMPLEMENT_DYNCREATE(CKEPEditDoc, CDocument)

BEGIN_MESSAGE_MAP(CKEPEditDoc, CDocument)
END_MESSAGE_MAP()

// CKEPEditDoc construction/destruction

CKEPEditDoc::CKEPEditDoc() {
	// TODO: add one-time construction code here
}

CKEPEditDoc::~CKEPEditDoc() {
}

BOOL CKEPEditDoc::OnNewDocument() {
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

// CKEPEditDoc serialization

void CKEPEditDoc::Serialize(CArchive& ar) {
	if (ar.IsStoring()) {
		// TODO: add storing code here
	} else {
		// TODO: add loading code here
	}
}

// CKEPEditDoc diagnostics

#ifdef _DEBUG
void CKEPEditDoc::AssertValid() const {
	CDocument::AssertValid();
}

void CKEPEditDoc::Dump(CDumpContext& dc) const {
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CKEPEditDoc commands
