// DialogBar.cpp : implementation file
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved

#include "stdafx.h"
#include "resource.h"
#include "KEPEditDialogBar.h"
#include "KEPDialogBar.h"
#include "IKEPDialogProcessor.h"

// CKEPDialogBar
#define ID_VSCROLL 15001

// copied from MSJ
class CTempDlg : public CDialog {
public:
	CRect m_rcDlg;

protected:
	virtual BOOL OnInitDialog();
};

BOOL CTempDlg::OnInitDialog() {
	GetWindowRect(&m_rcDlg);
	m_rcDlg -= m_rcDlg.TopLeft();
	EndDialog(0);
	return TRUE;
}

CSize GetDialogSize(const wchar_t* pszTemplName,
	CWnd* pParent) {
	CTempDlg dlg;
	dlg.Create(pszTemplName, pParent);
	return dlg.m_rcDlg.Size();
}
// end MSJ copy

CKEPDialogBar::CKEPDialogBar(int minW, int minH, int prefW, int prefH, bool useVScroll) :
		CKEPControlBar(minW, minH, prefW, prefH), m_vScrollWidth(0), m_origDialogHeight(0), m_barScroll(0), m_dlg(0), m_useVScroll(useVScroll), m_isDialog(false) {
}

CKEPDialogBar::~CKEPDialogBar() {
	if (m_dlg && m_isDialog) {
		// if dlg, get rid of it
		m_dlg->DestroyWindow();
		delete m_dlg;
		m_dlg = 0;
	}
}

BEGIN_MESSAGE_MAP(CKEPDialogBar, CKEPControlBar)
//{{AFX_MSG_MAP(CKEPDialogBar)
ON_WM_CREATE()
ON_WM_SIZE()
//}}AFX_MSG_MAP
ON_WM_VSCROLL()
END_MESSAGE_MAP()

//
extern "C" BOOL CALLBACK hideMainMenuProc(HWND hwnd,
	LPARAM lParam) {
	if (::GetWindowLong(hwnd, GWL_ID) == IDOK) // Make OKs "Apply"
	{
		CStringW s;
		s.LoadString(IDS_APPLY);
		::SetWindowText(hwnd, s);
	} else if (::GetWindowLong(hwnd, GWL_ID) == IDCANCEL || // hide Cancel and "Main Menu" buttons
			   ::GetWindowLong(hwnd, GWL_ID) == IDC_BUTTON_MENU) {
		::ShowWindow(hwnd, SW_HIDE);
		return FALSE;
	}
	return TRUE;
}

bool CKEPDialogBar::cleanUp() {
	m_vScrollWidth = 0;
	m_origDialogHeight = 0;
	int origBarScroll = m_barScroll;
	m_barScroll = 0;

	if (m_dlg && ::IsWindow(m_dlg->m_hWnd)) {
		// if dlg, get rid of it
		if (m_isDialog) {
			if (m_dlg->DestroyWindow()) {
				delete m_dlg;
				m_dlg = 0;
			} else {
				return false;
			}
		} else // if dlgbar, just hide it
		{
			if (m_dlg->IsKindOf(RUNTIME_CLASS(CKEPEditDialogBar))) {
				if (((CKEPEditDialogBar*)m_dlg)->CheckChanges()) {
					// restore the scroll to the original position such that
					// reentry and showing of this dialog again will not have to
					// position our scroll bar in the correct position
					m_dlg->ScrollWindow(0, (origBarScroll * -1));

					m_dlg->ShowWindow(SW_HIDE);
					m_dlg = 0;
				} else {
					return false;
				}
			} else {
				// restore the scroll to the original position such that
				// reentry and showing of this dialog again will not have to
				// position our scroll bar in the correct position
				m_dlg->ScrollWindow(0, (origBarScroll * -1));

				m_dlg->ShowWindow(SW_HIDE);
				m_dlg = 0;
			}
		}
	}
	if (::IsWindow(m_vScroll.m_hWnd))
		m_vScroll.DestroyWindow();
	return true;
}

void CKEPDialogBar::SetDialogBar(IKEPDialogProcessor* processor, UINT dlgId, UINT dlgType) {
	if (!cleanUp())
		return;

	CWnd* dlg = processor->Preprocess(this, dlgId, dlgType);

	if (dlg) {
		SetDialogBar(dlg, dlgId);
	}
}

void CKEPDialogBar::SetDialogBar(CWnd* dlg, UINT dlgId) {
	if (!cleanUp())
		return;

	CRect r;
	m_dlg = dlg;

	m_isDialog = m_dlg->IsKindOf(RUNTIME_CLASS(CDialog)) != FALSE;

	if (m_dlg == 0) {
		ASSERT(FALSE);
		return;
	}

	// must for dialogs, shouldn't hurt for dialog bar
	m_dlg->ShowWindow(SW_SHOW);
	m_dlg->SetParent(this);

	CSize sz = GetDialogSize(MAKEINTRESOURCE(dlgId), this);
	m_origDialogHeight = sz.cy;

	::EnumChildWindows(m_dlg->m_hWnd, hideMainMenuProc, 0);

	GetClientRect(&r);
	if (m_useVScroll) {
		m_vScrollWidth = GetSystemMetrics(SM_CXVSCROLL);
		r.left = r.right - m_vScrollWidth;
		m_vScroll.Create(SBS_RIGHTALIGN | SBS_VERT | WS_VISIBLE | WS_CHILD, r, this, ID_VSCROLL);

		// set the scroll bar info by using the size of the bar
		GetClientRect(&r);
		updateScrollInfo(r.Height());
	}

	m_dlg->MoveWindow(0, 0, (r.Width()) - m_vScrollWidth, r.Height());
}

void CKEPDialogBar::updateScrollInfo(DWORD curHeight) {
	SCROLLINFO si;
	ZeroMemory(&si, sizeof(si));
	m_vScroll.GetScrollInfo(&si);
	int prevPageSize = si.nPage;

	if (curHeight - prevPageSize != 0) {
		int enable = ESB_DISABLE_BOTH;

		ZeroMemory(&si, sizeof(si));
		si.cbSize = sizeof(si);
		si.fMask = SIF_PAGE | SIF_RANGE;
		si.nMax = m_origDialogHeight;
		si.nMin = 0;
		si.nPage = curHeight;

		if (curHeight < m_origDialogHeight) {
			enable = ESB_ENABLE_BOTH;
		} else {
			si.nPos = 0;
			si.fMask |= SIF_POS;
		}

		if (prevPageSize != 0 && m_barScroll < 0) {
			// recalc dlg position based on new page size
			int pageChange = curHeight - prevPageSize; // delta
			if (pageChange > 0 && (prevPageSize + pageChange) - m_barScroll > (int)m_origDialogHeight) {
				int extraScroll = ((prevPageSize + pageChange) - m_barScroll) - m_origDialogHeight;
				si.nPos -= extraScroll;
				m_dlg->ScrollWindow(0, extraScroll);
				m_barScroll += extraScroll;
			}
		}
		m_vScroll.SetScrollInfo(&si);
		m_vScroll.EnableScrollBar(enable);
	}
}

// CKEPDialogBar message handlers
int CKEPDialogBar::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	if (CKEPControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CKEPDialogBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler) {
	CKEPControlBar::OnUpdateCmdUI(pTarget, bDisableIfNoHndler);

	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}

void CKEPDialogBar::OnSize(UINT nType, int cx, int cy) {
	CKEPControlBar::OnSize(nType, cx, cy);

	CRect r;
	GetClientRect(r);

	if (m_dlg && ::IsWindow(m_dlg->m_hWnd)) {
		m_dlg->MoveWindow(0, 0, (r.Width()) - m_vScrollWidth, r.Height());
	}

	if (::IsWindow(m_vScroll.m_hWnd)) {
		m_vScroll.MoveWindow(r.right - m_vScrollWidth, 0, m_vScrollWidth, r.Height());

		updateScrollInfo(r.Height());
	}
}

LRESULT CKEPDialogBar::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) {
	return CKEPControlBar::WindowProc(message, wParam, lParam);
}

void CKEPDialogBar::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) {
	CRect r;
	GetClientRect(r);
	int scrollAmt = 0;
	int lineSize = 10; // TODO:

	SCROLLINFO si;
	ZeroMemory(&si, sizeof(si));
	m_vScroll.GetScrollInfo(&si);

	switch (nSBCode) {
		case SB_TOP:
			scrollAmt = -si.nPos;
			break;
		case SB_BOTTOM:
			scrollAmt = si.nPos - (m_origDialogHeight - r.Height());
			break;
		case SB_LINEDOWN:
			scrollAmt = lineSize;
			break;
		case SB_PAGEDOWN:
			scrollAmt = si.nPage;
			break;
		case SB_LINEUP:
			scrollAmt = -lineSize;
			break;
		case SB_PAGEUP:
			scrollAmt = -(int)si.nPage;
			break;
		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:
			scrollAmt = nPos + m_barScroll;
			break;

		default:
			TRACE("Got code of %d %d %d\n", nSBCode, nPos, si.nPos);
			break; // ignore
	};

	if (scrollAmt != 0) {
		if (scrollAmt < 0 && scrollAmt + si.nPos < 0)
			scrollAmt = -si.nPos;
		else if (scrollAmt > 0 && scrollAmt + si.nPos > (int)m_origDialogHeight - r.Height())
			scrollAmt = (m_origDialogHeight - r.Height()) - si.nPos;

		m_vScroll.SetScrollPos(si.nPos + scrollAmt);

		m_dlg->ScrollWindow(0, -scrollAmt);
		m_barScroll -= scrollAmt;

		CRect rect;
		m_dlg->GetClientRect(&rect);
		rect.top = rect.bottom - 5 + -scrollAmt; // 5 is fudge factor
		m_dlg->RedrawWindow(&rect, 0, RDW_INVALIDATE | RDW_ALLCHILDREN);

		m_dlg->GetClientRect(&rect);
		rect.bottom = rect.top + 5 - scrollAmt;
		m_dlg->RedrawWindow(&rect, 0, RDW_INVALIDATE | RDW_ALLCHILDREN);
	}
}
