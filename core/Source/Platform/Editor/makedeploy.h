/******************************************************************************
 makedeploy.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// makepatch.h : header file
//
#pragma once

#include "DistributionTargetDlg.h"
/////////////////////////////////////////////////////////////////////////////
// CMakepatch

typedef vector<pair<string, bool>> strVect;

class CMakepatch {
	// Construction
public:
	CMakepatch(HWND progressDlg, HANDLE distCloseEvent, HANDLE closeEvent, UINT msgId, bool skipdialog);

	CStringA m_SrcDir;
	CStringA m_Version;
	BOOL m_Traverse;
	BOOL m_ZipUp;
	CStringA m_ZipDest;
	CStringA m_ProgramDir;
	CStringA m_WindowsDir;

	HWND m_progressDlg;
	HANDLE m_distCloseEvent;
	HANDLE m_closeEvent;
	UINT m_msgId;
	strVect m_looseTextureDirs;

	bool m_skipDialog;

protected:
	int m_nRecursionLevel;
	int Generate(CRCLIST *oldpatchData, int oldFileCount);
	void createProgressiveInfo(CRCLIST *finalfiles, int finalfilecount, char *outputfile);

public:
	enum MakeReturnCodes { success = 0,
		invalid_source = 1,
		invalid_dest = 2,
		invalid_version = 3,
		general_error = 4,
		cancel = 5 };

	bool TraverseDirectory(const char *cFileName, char *outfile, BOOL bZipped, long *totalSize, int *totalFiles, CRCLIST *oldpatchData, int oldFileCount);
	void CleanDirectory(const char *dir);

	int Make(CStringA sourceDirectory, CStringA destinationDirectory, CStringA version, CRCLIST *oldpatchData, int oldFileCount, IN strVect &looseTextureDirs);
};
