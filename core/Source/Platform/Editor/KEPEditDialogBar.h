/******************************************************************************
 KEPEditDialogBar.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxext.h"
#include "resource.h"

/**
*Many CDialogBar type first level dialogs derive from this type
*It has additional functions to track changes made in the dialog
**/
class CKEPEditDialogBar : public CDialogBar {
	DECLARE_DYNAMIC(CKEPEditDialogBar)

public:
	CKEPEditDialogBar(void);
	~CKEPEditDialogBar(void);

protected:
	/**
		*whether user has made any changes to the dialog at all
		**/
	bool m_settingsChanged;

	/**
		*whether changes user made have been applied. TRUE if no changes have been made
		**/
	bool m_settingsApplied;

	/**
		*associated file
		**/
	CStringW m_loadedFile;

	/**
		*user has made changes to the dialog settings
		**/
	void SettingsChanged();

public:
	CStringA GetLoadedFile() const { return Utf16ToUtf8(m_loadedFile).c_str(); }

	//m_settingsChanged will be reset to false when settingsCommitted is true
	void SetLoadedFile(CStringW loadedFile, bool settingsCommitted = true);
	void SetLoadedFile(CStringA loadedFile, bool settingsCommitted = true);
	void SetSettingsChanged(bool changed) { m_settingsChanged = changed; }

	/**
		*user has applied the changes made to the dialog settings, public because that's the way
		*CDialogBar dialogs work in this app
		**/
	void SettingsApplied();
	bool IsSettingsApplied() const { return m_settingsApplied; }

	bool IsSettingsChanged() const { return m_settingsChanged; }

	virtual bool ApplyChanges();
	virtual bool CheckChanges() = 0;

	DECLARE_MESSAGE_MAP()
	/**
		*changes have been made in property controls
		**/
	virtual afx_msg LRESULT OnPropertyCtrlChanged(WPARAM wp, LPARAM lp);
};
