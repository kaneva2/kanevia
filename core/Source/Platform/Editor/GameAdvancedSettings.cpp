/******************************************************************************
 GameAdvancedSettings.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "StdAfx.h"
#include ".\gameadvancedsettings.h"

#include "CShaderLibClass.h"

GameAdvancedSettings::GameAdvancedSettings(void) {
}

GameAdvancedSettings::~GameAdvancedSettings(void) {
}

#ifdef USED_FOR_SERIALIZE_PL
void GameAdvancedSettings::Serialize() {
	<< m_fps;
	<< m_globalShadowType;
	<< m_W_BufferPreffered;
	<< m_EntityCollisionOn;
	<< m_SelectionModeOn;
	<< m_shaderSystem;
	<< m_globalWorldShader; // selection in above
	<< m_ResolutionHeight;
	<< m_ResolutionWidth;
}
#endif

// begin serialize.pl generated code
BEGIN_GETSET_IMPL(GameAdvancedSettings, IDS_GAMEADVANCEDSETTINGS_NAME)
GETSET_RANGE(m_fps, gsInt, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, FPS, INT_MIN, INT_MAX)
GETSET_RANGE(m_globalShadowType, gsInt, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, GLOBALSHADOWTYPE, INT_MIN, INT_MAX)
GETSET(m_W_BufferPreffered, gsBool, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, W_BUFFERPREFFERED)
GETSET(m_EntityCollisionOn, gsBool, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, ENTITYCOLLISIONON)
GETSET(m_SelectionModeOn, gsBool, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, SELECTIONMODEON)
GETSET_OBLIST_PTR(m_shaderSystem, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, SHADERSYSTEM)
GETSET_RANGE(m_globalWorldShader, gsInt, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, GLOBALWORLDSHADER, INT_MIN, INT_MAX)
GETSET_RANGE(m_ResolutionHeight, gsInt, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, RESOLUTIONHEIGHT, INT_MIN, INT_MAX)
GETSET_RANGE(m_ResolutionWidth, gsInt, GS_FLAGS_DEFAULT, 0, 0, GAMEADVANCEDSETTINGS, RESOLUTIONWIDTH, INT_MIN, INT_MAX)
END_GETSET_IMPL
// end of generated code
