/******************************************************************************
 NewZoneDlg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// NewZoneDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "NewZoneDlg.h"
#include <sys/stat.h>
#include "UnicodeMFCHelpers.h"

// NewZoneDlg dialog

IMPLEMENT_DYNAMIC(NewZoneDlg, CDialog)
NewZoneDlg::NewZoneDlg(const char* gameDir, BOOL saveAs, CWnd* pParent /*=NULL*/) :
		CDialog(NewZoneDlg::IDD, pParent), m_gameDir(gameDir), m_saveAs(saveAs) {
}

NewZoneDlg::~NewZoneDlg() {
}

void NewZoneDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(NewZoneDlg, CDialog)
ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

BOOL NewZoneDlg::OnInitDialog() {
	CDialog::OnInitDialog();
	if (m_saveAs) {
		CStringW s;
		s.LoadString(IDS_SAVEAS_ZONE_TITLE);
		SetWindowText(s);
	}
	((CEdit*)GetDlgItem(IDC_ZONE_NAME))->SetLimitText(120);
	return TRUE;
}

// NewZoneDlg message handlers

void NewZoneDlg::OnBnClickedOk() {
	struct _stat st;

	GetDlgItemTextUtf8(this, IDC_ZONE_NAME, m_fname);
	if (m_fname.Trim().IsEmpty() ||
		m_fname.Find("\\") >= 0 ||
		m_fname.Find("..") >= 0 ||
		m_fname.Find("/") >= 0) {
		AfxMessageBox(IDS_INVALID_ZONE_NAME, MB_ICONEXCLAMATION);
		return;
	}

	if (m_fname.Right(4).CompareNoCase(".exg") != 0)
		m_fname += ".exg";

	if (_stat(PathAdd(m_gameDir, m_fname).c_str(), &st) != -1) {
		if (AfxMessageBox(IDS_EXISTING_ZONE, MB_ICONQUESTION | MB_YESNOCANCEL) != IDYES)
			return;
	}
	OnOK();
}
