///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "CEXMeshClass.h"
#include "ColladaAssert.h"
#include "ColladaClasses.h"
#include "IAssetsDatabase.h"
#include "ColladaUtil.h"
//#include "AssetConverter.h"

namespace KEP {

int ColladaUtil::EnumAllAnimations(domLibrary_animations* pDomLibAnims, AnimationHandler& animHandler) {
	const domAnimation_Array& DomAnimArr = pDomLibAnims->getAnimation_array();
	if (DomAnimArr.getCount() == 0)
		return COLLADA_NOTFOUND;

	for (UINT animLoop = 0; animLoop < DomAnimArr.getCount(); animLoop++) {
		domAnimation* pAnim = DomAnimArr[animLoop];
		animHandler(pAnim);
	}

	return COLLADA_OK;
}

int ColladaUtil::ReadDomLibrary_Animations(domLibrary_animations* pDomLibAnims, ColladaSkeletalAnimation& animation,
	std::map<std::string, ColladaAnimMatrixSampler*>& allSamplers,
	std::map<std::string, SimpleArray<ImpFloatMatrix4>*>& allMatrixSources) {
	const domAnimation_Array& DomAnimArr = pDomLibAnims->getAnimation_array();
	if (DomAnimArr.getCount() == 0)
		return COLLADA_NOTFOUND;

	BOOL errorFound = FALSE;

	for (UINT animLoop = 0; animLoop < DomAnimArr.getCount(); animLoop++) {
		domAnimation* pAnim = DomAnimArr[animLoop];

		if (ReadDomAnimation_Skeletal(pAnim, animation, allSamplers, allMatrixSources) < 0)
			errorFound = TRUE;
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

int ColladaUtil::ReadDomAnimation_Skeletal(domAnimation* pDomAnim, ColladaSkeletalAnimation& animation,
	std::map<std::string, ColladaAnimMatrixSampler*>& allSamplers,
	std::map<std::string, SimpleArray<ImpFloatMatrix4>*>& allMatrixSources) {
	domChannel_Array& chanArr = pDomAnim->getChannel_array();
	if (chanArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT;

	BOOL errorFound = FALSE;

	for (UINT chanLoop = 0; chanLoop < chanArr.getCount(); chanLoop++) {
		domChannel* pChan = chanArr[chanLoop];

		ColladaAnimMatrixSampler* pSampler = NULL;
		const domURIFragmentType& uriSource = pChan->getSource();

		if (allSamplers.find(uriSource.getURI()) != allSamplers.end())
			pSampler = allSamplers[uriSource.getURI()];
		else {
			domSampler* pDomSampler = dynamic_cast<domSampler*>(uriSource.getElement().cast());
			if (pDomSampler == NULL) {
				errorFound = TRUE;
				continue;
			}

			pSampler = new ColladaAnimMatrixSampler();
			pSampler->outputValues = NULL;

			if (ReadDomAnimationSampler(pDomSampler, allMatrixSources, *pSampler) <= 0) {
				errorFound = TRUE;
				delete pSampler;
				continue;
			}

			allSamplers.insert(std::pair<std::string, ColladaAnimMatrixSampler*>(uriSource.getURI(), pSampler));
		}

		xsToken target = pChan->getTarget();
		std::string attrName, sidPath;
		int attrIndex;
		daeElement* pTarget = ResolveDomTarget(pChan->getDAE()->getDatabase(), target, sidPath, attrName, attrIndex);
		if (pTarget == NULL) {
			errorFound = TRUE;
			continue;
		}

		if (!attrName.empty() || attrIndex != -1) {
			//@@Todo: support advanced animation
			ASSERT(FALSE);
		}

		// Assign animation sampler data to target

		//@@Limitation: only apply animation data to <node><matrix> element [Yanfeng 10/2008]
		domMatrix* pDomMatrix = dynamic_cast<domMatrix*>(pTarget);
		if (!pDomMatrix)
			continue;

		domNode* pDomNode = dynamic_cast<domNode*>(pDomMatrix->getParent());
		if (!pDomNode)
			continue;

		std::string assetName = GetAssetNameFromElement(pDomNode);
		if (assetName.empty()) // no name?
			return COLLADA_ERR_BAD_FORMAT;

		animation.AddAnimationData(assetName, pSampler);
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

int ColladaUtil::ReadDomAnimation_Vertex(domAnimation* pDomAnim, ColladaVertexAnimation& animation,
	std::map<std::string, ColladaAnimFloatSampler*>& allSamplers,
	std::map<std::string, SimpleArray<float>*>& allSources) {
	domChannel_Array& chanArr = pDomAnim->getChannel_array();
	if (chanArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT;

	BOOL errorFound = FALSE;

	for (UINT chanLoop = 0; chanLoop < chanArr.getCount(); chanLoop++) {
		domChannel* pChan = chanArr[chanLoop];

		ColladaAnimFloatSampler* pSampler = NULL;
		const domURIFragmentType& uriSource = pChan->getSource();

		if (allSamplers.find(uriSource.getURI()) != allSamplers.end())
			pSampler = allSamplers[uriSource.getURI()];
		else {
			domSampler* pDomSampler = dynamic_cast<domSampler*>(uriSource.getElement().cast());
			if (pDomSampler == NULL) {
				errorFound = TRUE;
				continue;
			}

			pSampler = new ColladaAnimFloatSampler();
			pSampler->outputValues = NULL;

			if (ReadDomAnimationSampler(pDomSampler, allSources, *pSampler) <= 0) {
				errorFound = TRUE;
				delete pSampler;
				continue;
			}

			allSamplers.insert(std::pair<std::string, ColladaAnimFloatSampler*>(uriSource.getURI(), pSampler));
		}

		xsToken target = pChan->getTarget();
		std::string attrName, sidPath;
		int attrIndex;
		daeElement* pTarget = ResolveDomTarget(pChan->getDAE()->getDatabase(), target, sidPath, attrName, attrIndex);
		if (pTarget == NULL) {
			errorFound = TRUE;
			continue;
		}

		// Assign animation sampler data to target

		//@@Limitation: only apply animation data to <morph><source> [Yanfeng 11/2008]
		domSource* pDomSource = dynamic_cast<domSource*>(pTarget);
		if (!pDomSource)
			continue;

		domMorph* pDomMorph = dynamic_cast<domMorph*>(pDomSource->getParent());
		if (!pDomMorph)
			continue;

		///////////////////////////////////////////////////////
		if (!attrName.empty())
			animation.AddAnimationData(attrName, pSampler);
		else if (attrIndex == -1)
			animation.AddAnimationData("", pSampler);
		else {
			std::string attrIndexStr;
			std::stringstream ss(attrIndexStr);
			ss << "(" << attrIndex << ")";
			animation.AddAnimationData(attrIndexStr, pSampler);
		}
		///////////////////////////////////////////////////////
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

template <typename SamplerDataType>
int ColladaUtil::ReadDomAnimationSampler(domSampler* pDomSampler, std::map<std::string, SimpleArray<SamplerDataType>*> allMatrixSources,
	ColladaAnimSampler<SamplerDataType>& sampler) {
	const domInputLocal_Array& DomInputLA = pDomSampler->getInput_array();
	if (DomInputLA.getCount() == 0)
		return COLLADA_NOTFOUND;

	BOOL errorFound = FALSE;

	for (UINT inpLoop = 0; inpLoop < DomInputLA.getCount(); inpLoop++) {
		domInputLocal* pInput = DomInputLA[inpLoop];
		xsNMTOKEN sem = pInput->getSemantic();
		domURIFragmentType uriSource = pInput->getSource();
		domSource* pDomSource = dynamic_cast<domSource*>(uriSource.getElement().cast());
		if (pDomSource == NULL) {
			errorFound = TRUE;
			continue;
		}

		if (MatchxsNMToken(sem, "INPUT")) {
			//@@Limitation: must be time
			SimpleArray<float> timelineSource;
			if (ReadDomSourceAllData<domFloat_array, float, float>(sampler.timeline, pDomSource) <= 0)
				errorFound = TRUE;
		} else if (MatchxsNMToken(sem, "OUTPUT")) {
			//@@Limitation: must be matrices
			SimpleArray<SamplerDataType>* pMatrixSource = NULL;
			if (allMatrixSources.find(uriSource.getURI()) != allMatrixSources.end()) {
				// already read
				pMatrixSource = allMatrixSources[uriSource.getURI()];
			} else {
				pMatrixSource = new SimpleArray<SamplerDataType>();
			}

			if (ReadDomSourceAllData<domFloat_array, float, SamplerDataType>(*pMatrixSource, pDomSource) <= 0) {
				errorFound = TRUE;
				delete pMatrixSource;
			} else {
				allMatrixSources.insert(std::pair<std::string, SimpleArray<SamplerDataType>*>(uriSource.getURI(), pMatrixSource));
				sampler.outputValues = pMatrixSource;
			}
		} else if (MatchxsNMToken(sem, "INTERPOLATION")) {
			//@@Limitation: interpolation not supported
		} else if (MatchxsNMToken(sem, "IN_TANGENT")) {
			//@@Limitation: interpolation not supported
		} else if (MatchxsNMToken(sem, "OUT_TANGENT")) {
			//@@Limitation: interpolation not supported
		}
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

} // namespace KEP