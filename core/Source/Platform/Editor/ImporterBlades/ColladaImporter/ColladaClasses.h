///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AssetImporterMath.h"

#include <memory>

class daeElement;
class domSource;

namespace KEP {

typedef ABVERTEX Vertex;

struct ColladaColor {
	union {
		float val[4];
		struct {
			float r, g, b, a;
		};
	};

	ColladaColor() { clear(); }
	ColladaColor(D3DCOLORVALUE clr) {
		r = clr.r;
		g = clr.g;
		b = clr.b;
		a = clr.a;
	}
	ColladaColor(float R, float G, float B, float A) {
		r = R;
		g = G;
		b = B;
		a = A;
	}
	ColladaColor(float* floatArr, UINT size) {
		clear();
		for (UINT i = 0; i < std::max(4U, size); i++)
			val[i] = floatArr[i];
	}

	void clear() { r = g = b = a = 0.0f; }

	operator D3DCOLORVALUE() {
		D3DCOLORVALUE ret;
		ret.r = r;
		ret.g = g;
		ret.b = b;
		ret.a = a;
		return ret;
	}
};

// Enumerates for collada surface types
enum enumCST {
	CST_UNTYPED = 0,
	CST_1D,
	CST_2D,
	CST_3D,
	CST_CUBE,
	CST_DEPTH,
	CST_RECT,
};

// Enumerates for collada surface format hints
enum enumCSF {
	CSF_INVALID = -1,
	CSF_CHN_RGB,
	CSF_CHN_RGBA,
	CSF_CHN_L,
	CSF_CHN_LA,
	CSF_CHN_D,
	CSF_CHN_XYZ,
	CSF_CHN_XYZW, // channels
	CSF_RNG_SNORM,
	CSF_RNG_UNORM,
	CSF_RNG_SINT,
	CSF_RNG_UINT,
	CSF_RNG_FLOAT, // range
	CSF_PRC_LOW,
	CSF_PRC_MID,
	CSF_PRC_HIGH, // precision
	CSF_OPT_GAMMA,
	CSF_OPT_NORMALIZED3,
	CSF_OPT_NORMALIZED4,
	CSF_OPT_COMPRESSABLE, // option
};

struct ColladaFxSurface {
	enumCST type;
	std::string imageURI;
	std::string format;
	struct {
		enumCSF channels;
		enumCSF range;
		enumCSF precision;
		enumCSF option;
	} formatHint;
	int sizeX, sizeY, sizeZ; //0 0 0
	float ratioX, ratioY; //1 1
	int mipLevels; //0
	BOOL mipMapGenerate; //FALSE

	ColladaFxSurface() { clear(); }

	void clear() {
		type = CST_UNTYPED;
		imageURI.clear();
		format.clear();
		formatHint.channels = formatHint.range = formatHint.precision = formatHint.option = CSF_INVALID;
		sizeX = sizeY = sizeZ = 0;
		ratioX = ratioY = 1;
		mipLevels = 0;
		mipMapGenerate = FALSE;
	}
};

// Enumerates for collada sampler
enum enumCS { CS_NONE = 0,
	CS_WRAP,
	CS_MIRROR,
	CS_CLAMP,
	CS_BORDER, //wrap
	CS_NEAREST,
	CS_LINEAR,
	CS_NEAREST_MIPMAP_NEAREST,
	CS_LINEAR_MIPMAP_NEAREST,
	CS_NEAREST_MIPMAP_LINEAR,
	CS_LINEAR_MIPMAP_LINEAR, // filter
};

struct ColladaFxSampler {
	std::string surfaceSource; // <source> to <surface>
	ColladaFxSurface const* surface; // actual <surface>

	enumCS wrapU, wrapV;
	enumCS minFilter, magFilter, mipFilter;
	ColladaColor borderColor;
	unsigned char mipMapMaxLevel;
	float mipMapBias;

	ColladaFxSampler() { clear(); }

	void clear() {
		surfaceSource.clear();
		surface = NULL;
		wrapU = wrapV = CS_NONE;
		minFilter = magFilter = mipFilter = CS_NONE;
		borderColor.clear();
		mipMapMaxLevel = 0;
		mipMapBias = 0;
	}

	BOOL resolveSurfaceSource(std::map<std::string, ColladaFxSurface>& allSurfaces);
};

class ColladaTexture {
private:
	std::string textureParam; // reference to <newparam>
	std::string uvSetSymbol; // to be resolved by <bind_vertex_input> or <bind>

	std::string imageURI; // actual image used for texture

	int uvIndex;
	std::string uvSetTarget;

public:
	ColladaTexture() { clear(); }

	void clear() {
		textureParam.clear();
		uvSetSymbol.clear();
		imageURI.clear();
		uvSetTarget.clear();
		uvIndex = -1;
	}

	void setTextureParam(const std::string& param) { textureParam = param; }
	void setUVSetSymbol(const std::string& symbol) { uvSetSymbol = symbol; }

	BOOL isUnresolved() { return hasUnresolvedMapSymbol() || hasUnresolvedParam(); }
	BOOL hasUnresolvedParam() { return !textureParam.empty() && imageURI.empty(); }
	BOOL hasUnresolvedMapSymbol() { return !uvSetSymbol.empty() && uvSetTarget.empty() && uvIndex == -1; }

	BOOL resolveParamToImageURI(std::map<std::string, ColladaFxSampler>& allSamplers);
	BOOL resolveMapSymbolToTarget(const std::string& symbol, const std::string& target);
	BOOL resolveMapSymbolToUVIndex(const std::string& symbol, const int idx);

	const std::string& getImageURI() const { return imageURI; }

	BOOL hasMap() const { return !uvSetSymbol.empty(); }
	int getMapIndex() const { return uvIndex; }
	const std::string& getMapTarget() const { return uvSetTarget; }
};

enum enumCT { CT_NONE,
	CT_COLOR = 1,
	CT_TEXTURE = 2,
	CT_FLOAT = 4 };
enum enumCM {
	CM_DIFFUSE = 0,
	CM_SPECULAR = 1,
	CM_SPECLEVEL = 2,
	CM_SHININESS = 3, // must follow the same order used in MAX
	CM_AMBIENT = 4,
	CM_EMISSIVE = 5,
	CM_REFLECTIVE = 6,
	CM_TRANSPARENT = 7,
	CM_MAP_CHANNELS = 8, // first 8 are of color or texture
	CM_REFRACTION = 8,
	CM_REFLECTIVITY = 9,
	CM_TRANSPARENCY = 10, // float type only
	CM_TOTAL_CHANNELS = 11
};

class ColladaMaterial {
protected:
	class ColladaMaterialChannel {
	private:
		UINT typeMask;
		ColladaColor color;
		ColladaTexture texture;
		float fvalue;

	public:
		ColladaMaterialChannel() { clear(); }

		void clear() {
			typeMask = CT_NONE;
			memset(&color, 0, sizeof(color));
			texture.clear();
			fvalue = 0;
		}

		BOOL hasValue(enumCT valueType) const { return (typeMask & valueType) != 0; }
		void clearValue(enumCT valueType) { typeMask &= ~valueType; }

		void setValue(const ColladaColor& clr) {
			clearValue(CT_TEXTURE); // Color and texture is mutually exclusive
			typeMask |= CT_COLOR;
			color = clr;
		}

		void setValue(const ColladaTexture& tex) {
			clearValue(CT_COLOR); // Color and texture is mutually exclusive
			typeMask |= CT_TEXTURE;
			texture = tex;
		}

		void setValue(float f) {
			typeMask |= CT_FLOAT;
			fvalue = f;
		}

		BOOL getValue(ColladaColor& clr) const {
			if (!hasValue(CT_COLOR))
				return FALSE;
			clr = color;
			return TRUE;
		}

		BOOL getValue(ColladaTexture& tex) const {
			if (!hasValue(CT_TEXTURE))
				return FALSE;
			tex = texture;
			return TRUE;
		}

		BOOL getValue(float& f) const {
			if (!hasValue(CT_FLOAT))
				return FALSE;
			f = fvalue;
			return TRUE;
		}

		ColladaTexture& getTexture() {
			ASSERT(hasValue(CT_TEXTURE) == TRUE);
			return texture;
		}

		const ColladaTexture& getTexture() const {
			ASSERT(hasValue(CT_TEXTURE) == TRUE);
			return texture;
		}
	};

private:
	// Basic shading
	ColladaMaterialChannel channels[CM_TOTAL_CHANNELS];
	//float shininess;

	// lists of <newparam>s
	std::map<std::string, ColladaFxSurface> allSurfaces;
	std::map<std::string, ColladaFxSampler> allSamplers;

	// <extra><technique profile="GOOGLEEARTH">
	bool googleEarthDoubleSided;

public:
	ColladaMaterial() { clear(); }

	BOOL hasUnresolvedTextureParams();
	void resolveTextureParamToImageURI();
	BOOL hasUnresolvedTextureMapSymbols();
	void resolveTextureMapSymbolToTarget(const std::string& symbol, const std::string& target);
	void resolveTextureMapSymbolToUVIndex(const std::string& symbol, int idx);

	int getNumberOfBindableMaps();

	//// Check if map channels are resolvable with binding arguments supplied
	//BOOL resolveTextureMapChannels(const ColladaMaterialBindingArguments& bindingArgs);

	void clear() {
		for (int i = 0; i < CM_TOTAL_CHANNELS; i++)
			channels[i].clear();

		// default diffuse color
		ColladaColor defaultDiffuse(1.0f, 1.0f, 1.0f, 1.0f);
		channels[CM_DIFFUSE].setValue(defaultDiffuse);

		// default shininess
		channels[CM_SHININESS].setValue(0.0f);

		allSurfaces.clear();
		allSamplers.clear();

		googleEarthDoubleSided = false;
	}

	BOOL channelHasValue(UINT id, enumCT valueType) const {
		VERIFY_RETURN(id < CM_TOTAL_CHANNELS, FALSE);
		return channels[id].hasValue(valueType);
	}

	ColladaColor getColorByChannel(UINT id) const {
		ASSERT(id < CM_MAP_CHANNELS);
		ColladaColor clr;
		VERIFY(channels[id].getValue(clr) == TRUE);
		return clr;
	}

	const ColladaTexture& getTextureByChannel(UINT id) const {
		ASSERT(id < CM_MAP_CHANNELS);
		return channels[id].getTexture();
	}

	float getFloatByChannel(UINT id) const {
		VERIFY_RETURN(id < CM_TOTAL_CHANNELS, 0);
		float f = 0.0f;
		VERIFY(channels[id].getValue(f) == TRUE);
		return f;
	}

	void setColorChannel(UINT id, const ColladaColor& clr) {
		VERIFY_RETVOID(id < CM_MAP_CHANNELS);
		channels[id].setValue(clr);
	}

	void setTextureChannel(UINT id, const ColladaTexture& texture) {
		VERIFY_RETVOID(id < CM_MAP_CHANNELS);
		channels[id].setValue(texture);
	}

	void setFloatChannel(UINT id, float value) {
		VERIFY_RETVOID(id < CM_TOTAL_CHANNELS);
		channels[id].setValue(value);
	}

	ColladaFxSurface& addSurface(const std::string& param) {
		std::map<std::string, ColladaFxSurface>::iterator it = allSurfaces.find(param);
		if (it != allSurfaces.end())
			return allSurfaces[param]; // use bracket operator to obtain a reference

		return allSurfaces[param] = ColladaFxSurface();
	}

	ColladaFxSampler& addSampler(const std::string& param) {
		std::map<std::string, ColladaFxSampler>::iterator it = allSamplers.find(param);
		if (it != allSamplers.end())
			return allSamplers[param]; // use bracket operator to obtain a reference

		return allSamplers[param] = ColladaFxSampler();
	}

	void resolveAllSurfaceSources() {
		for (std::map<std::string, ColladaFxSampler>::iterator it = allSamplers.begin(), itEnd = allSamplers.end(); it != itEnd; ++it)
			allSamplers[it->first].resolveSurfaceSource(allSurfaces); // use bracket operator to obtain a reference
	}

	void setGoogleEarthDoubleSided(bool value) { googleEarthDoubleSided = value; }
	bool getGoogleEarthDoubleSided() const { return googleEarthDoubleSided; }
};

typedef std::shared_ptr<ColladaMaterial> ColladaMaterialPtr;

enum enumCIS { CIS_UNKNOWN,
	CIS_POSITION,
	CIS_NORMAL,
	CIS_TANGENT,
	CIS_TEXCOORD,
	CIS_VERTEX,
	CIS_JOINT,
	CIS_WEIGHT };

//! Use two C structs to expedite lookup of <input> records so we can pre-process C++ data at per-mesh level instead of per-vertex.

//@@Assumption: this is a pure C struct. No virtual members otherwise VFT will be destroyed.
struct ColladaInputOffset {
	enumCIS semantic; // input semantic
	UINT offset; // offset into the data array

	// note the difference between set and uvIndex:
	//		* "set" can be any arbitrary number while uvIndex is zero-based continuous values.
	//		* "set" will be mapped into uvIndex when material is bound.
	// Look up method:
	//		* <input> from uvIndex		: TexCoord-Input = allInputs[uvIndexToInputId[uvIndex]];
	//		* set from <input>			: set = TexCoord-Input.set
	//		* uvIndex from <input>	: uvIndex = TexCoord-Input.uvIndex
	//		* uvIndex from set		: uvIndex = FindUVIndexBySet(set)

	UINT set; // input set as defined in COLLADA document
	UINT uvIndex; // Map channel used for rendering, see ColladaInputOffsetArray::uvIndexToInputId
	daeElement* source;
};

class ColladaInputOffsetArray {
private:
	UINT numOfInputs; // Array size: number of inputs
	UINT stride; // Helper data 1: stride of data array (after summing length of all inputs together)
	UINT maxUVIndex; // Helper data 2: number of Map channels
	UINT uvIndexToInputId[CM_MAP_CHANNELS]; // Where to find input of TexCoords per map channel
	ColladaInputOffset* allInputs; // Real data: flexible array

public:
	ColladaInputOffsetArray(UINT inputCount) :
			numOfInputs(inputCount), allInputs(NULL), stride(0), maxUVIndex(0) {
		allInputs = new ColladaInputOffset[numOfInputs];
		memset(uvIndexToInputId, 0, sizeof(uvIndexToInputId));
		memset(allInputs, 0, sizeof(ColladaInputOffset) * numOfInputs);
	}

	~ColladaInputOffsetArray() {
		if (allInputs)
			delete allInputs;
		allInputs = NULL;
	}

	UINT GetInputCount() const { return numOfInputs; }

	UINT GetStride() const { return stride; }
	void SetStride(UINT s) { stride = s; }

	UINT GetMaxUVIndex() const { return maxUVIndex; }

	const ColladaInputOffset* GetInputConst(UINT index) const {
		jsAssert(index < numOfInputs);
		if (index >= numOfInputs)
			return NULL;

		return &allInputs[index];
	}

	ColladaInputOffset* GetInput(UINT index) {
		return const_cast<ColladaInputOffset*>(GetInputConst(index));
	}

	int AddUVIndexByInputId(UINT inputId) {
		ColladaInputOffset* pCldInputOfs = GetInput(inputId);
		if (!pCldInputOfs)
			return -1;

		if (maxUVIndex + 1 < CM_MAP_CHANNELS) {
			pCldInputOfs->uvIndex = maxUVIndex++;
			uvIndexToInputId[pCldInputOfs->uvIndex] = inputId;
			return pCldInputOfs->uvIndex;
		}

		return -1;
	}

	int FindUVIndexBySet(UINT set) const {
		for (UINT i = 0; i < maxUVIndex; i++) {
			const ColladaInputOffset* pCldInputOfs = GetInputConst(uvIndexToInputId[i]);
			if (pCldInputOfs && pCldInputOfs->set == set)
				return i;
		}

		return -1;
	}

	int FindSetByUVIndex(UINT chan) const {
		UINT inputId = uvIndexToInputId[chan];
		return FindSetByInputId(inputId);
	}

	int FindUVIndexByInputId(UINT id) const {
		const ColladaInputOffset* pCldInputOfs = GetInputConst(id);
		if (!pCldInputOfs)
			return -1;
		return pCldInputOfs->uvIndex;
	}

	int FindSetByInputId(UINT id) const {
		const ColladaInputOffset* pCldInputOfs = GetInputConst(id);
		if (!pCldInputOfs)
			return -1;
		return pCldInputOfs->set;
	}
};

typedef std::shared_ptr<ColladaInputOffsetArray> ColladaInputOffsetArrayPtr;

//!Helper function to map texture channel to pointer of corresponding UV data
//!Can be removed after we optimize CAdvancedVertexObj
inline TXTUV* GetKEPTexCoordPtrByChannelID(Vertex* vtx, int chan) {
	//@@Limitation: Vertex must has 8 adjacent TXTUV fields, starting from tx0
	//@@Watch: cast structure fields as array, will fail if structure changes.
	if (chan >= 0 && chan < 8)
		return (&vtx->tx0) + chan;
	return NULL;
}

enum enumUPAXIS { CSM_UNK_UP,
	CSM_X_UP,
	CSM_Y_UP,
	CSM_Z_UP };

struct ColladaVertex {
	UINT positionIndex,
		normalIndex,
		texelIndex[CM_MAP_CHANNELS];

	ColladaVertex() :
			positionIndex(-1), normalIndex(-1) {
		for (int i = 0; i < CM_MAP_CHANNELS; i++) texelIndex[i] = -1;
	}
};

class ColladaSubmesh {
protected:
	UINT numVtx;
	ColladaVertex* vertices;

	UINT numPositions;
	ImpFloatVector3* positions;
	UINT numNormals;
	ImpFloatVector3* normals;
	UINT numTexels[CM_MAP_CHANNELS];
	TXTUV* texels[CM_MAP_CHANNELS];

	bool normalBufferValid;

	//Vertex* vertices;						// Exploded vertices list (not indexed)
	//std::vector<UINT>* uniqueVertexIds;		// Unique vertex ID per exploded vertex for matching vertices to other data (e.g. weights)
	std::string materialName;
	ColladaMaterialPtr material;
	enumUPAXIS upAxis;
	ColladaInputOffsetArrayPtr pInputOffsetArr;

protected:
	virtual void deletePositionBuffer() {
		if (positions)
			delete[] positions;
		positions = NULL;
		numPositions = 0;
	}
	virtual void deleteNormalBuffer() {
		if (normals)
			delete[] normals;
		normals = NULL;
		numNormals = 0;
		normalBufferValid = false; // normal buffer not set
	}
	virtual void deleteTexelBuffer(int chan) {
		if (texels[chan])
			delete[] texels[chan];
		texels[chan] = NULL;
		numTexels[chan] = 0;
	}
	virtual void deleteVertexBuffer() {
		if (vertices)
			delete[] vertices;
		vertices = NULL;
		numVtx = 0;
	}

public:
	ColladaSubmesh() //: vertices(NULL), uniqueVertexIds(NULL), numVtx(0), materialName(""), upAxis(CSM_UNK_UP) {}
			:
			positions(NULL),
			numPositions(0), normals(NULL), numNormals(0), vertices(NULL), numVtx(0), materialName(""), upAxis(CSM_UNK_UP), normalBufferValid(false) {
		for (int i = 0; i < CM_MAP_CHANNELS; i++) {
			texels[i] = NULL;
			numTexels[i] = 0;
		}
	}
	virtual ~ColladaSubmesh() {
		jsAssert(this != NULL);
		deletePositionBuffer();
		deleteNormalBuffer();
		for (int i = 0; i < CM_MAP_CHANNELS; i++) deleteTexelBuffer(i);
		deleteVertexBuffer();
	}

	virtual UINT getPositionCount() const { return numPositions; }
	virtual void setPositionBuffer(int posCnt, ImpFloatVector3* buffer = NULL) {
		deletePositionBuffer();
		numPositions = posCnt;
		if (buffer == NULL && numPositions > 0)
			positions = new ImpFloatVector3[numPositions];
		else
			positions = buffer;
	}
	virtual const ImpFloatVector3& getPositionVector(UINT idx) const {
		static ImpFloatVector3 dummy(0, 0, 0);
		jsVerifyReturn(idx < getPositionCount() && positions != NULL, dummy);
		return positions[idx];
	}
	virtual void setPositionVector(UINT idx, const ImpFloatVector3& pos) {
		jsAssert(idx < getPositionCount() && positions != NULL);
		positions[idx] = pos;
	}

	virtual UINT getNormalCount() const { return numNormals; }
	virtual void setNormalBuffer(int nmlCnt, ImpFloatVector3* buffer = NULL) {
		deleteNormalBuffer();
		numNormals = nmlCnt;
		normalBufferValid = buffer != nullptr;
		if (buffer == NULL && numNormals > 0)
			normals = new ImpFloatVector3[numNormals];
		else
			normals = buffer;
	}
	virtual const ImpFloatVector3& getNormalVector(UINT idx) const {
		static ImpFloatVector3 dummy(1, 0, 0);
		if (!normalBufferValid && idx == (UINT)-1) {
			// No normal vectors available, return silently
			return dummy;
		}

		jsVerifyReturn(idx < getNormalCount() && normals != NULL, dummy);
		return normals[idx];
	}
	virtual void setNormalVector(UINT idx, const ImpFloatVector3& nml) {
		jsAssert(idx < getNormalCount() && normals != NULL);
		normals[idx] = nml;
	}

	virtual UINT getTexelCount(int chan) const { return numTexels[chan]; }
	virtual void setTexelBuffer(int chan, int texCnt, TXTUV* buffer = NULL) {
		deleteTexelBuffer(chan);
		numTexels[chan] = texCnt;
		if (buffer == NULL && texCnt > 0) {
			texels[chan] = new TXTUV[texCnt];
			memset(texels[chan], 0, texCnt * sizeof(TXTUV));
		} else
			texels[chan] = buffer;
	}
	virtual void unlinkTexelBuffer(int chan) {
		texels[chan] = NULL;
		numTexels[chan] = 0;
	}
	virtual LPVOID getTexelBuffer(int chan) { return texels[chan]; }
	virtual const TXTUV* getTexelBuffer(int chan) const { return texels[chan]; }
	virtual const TXTUV& getTexel(int chan, UINT idx) const {
		jsAssert(idx < getTexelCount(chan) && texels[chan] != NULL);
		return texels[chan][idx];
	}
	virtual void setTexel(int chan, UINT idx, const TXTUV& tex) {
		jsAssert(idx < getTexelCount(chan) && texels[chan] != NULL);
		texels[chan][idx] = tex;
	}

	virtual UINT getVertexCount() const { return numVtx; }
	virtual void setVertexBuffer(int vcnt, ColladaVertex* buffer = NULL) {
		deleteVertexBuffer();
		numVtx = vcnt;
		if (buffer == NULL && numVtx > 0)
			vertices = new ColladaVertex[numVtx];
		else
			vertices = buffer;
	}
	virtual const ColladaVertex& getVertex(UINT id) const {
		jsAssert(id < getVertexCount());
		return vertices[id];
	}
	virtual void setVertex(UINT id, const ColladaVertex& v) {
		jsAssert(id < getVertexCount());
		vertices[id] = v;
	}

	virtual void setMaterialName(const std::string& matName) { materialName = matName; }
	virtual const std::string& getMaterialName() const { return materialName; }

	//! Set material link: incoming material must not carry unbound parameter
	virtual void setMaterial(ColladaMaterialPtr mat) { material = mat; }
	//! Return material link
	virtual ColladaMaterialPtr getMaterial() const { return material; }
	//! Resolve material by name: will try to resolve any unbound parameter if possible. Do nothing if material has unresolvable parameter.
	virtual BOOL resolveMaterial(const std::string& matName, ColladaMaterialPtr mat);

	virtual BOOL hasUnresolvedMaterial() { return !material; }

	ColladaInputOffsetArrayPtr getInputOffsetArray() const { return pInputOffsetArr; }
	void setInputOffsetArray(ColladaInputOffsetArrayPtr pArray) { pInputOffsetArr = pArray; }

	void setUpAxis(enumUPAXIS up) { upAxis = up; }
	enumUPAXIS getUpAxis() const { return upAxis; }

	bool isNormalBufferValid() const { return normalBufferValid; }

	//! Collapse the mesh to static version. Return NULL if failed.
	//! Return self as it's already static. (override if inherited from ColladaSubmesh)
	//! Note: Caller is responsible for disposing returned pointer if it's not the same as original mesh.
	virtual const ColladaSubmesh* collapseToStaticMesh() const { return this; }
};

typedef std::shared_ptr<ColladaSubmesh> ColladaSubmeshPtr;

class ColladaSkeleton;

class ColladaJoint {
private:
	std::string name, parent, uri, sid;
	ImpFloatMatrix4 transformFromParent, parentTransform;
	ColladaSkeleton* skeleton;
	enumUPAXIS upAxis;

public:
	ColladaJoint(ColladaSkeleton* ske = NULL) :
			skeleton(ske), upAxis(CSM_UNK_UP) {}
	ColladaJoint(const std::string& n, const std::string& p, const std::string& u, const std::string& id, ColladaSkeleton* ske = NULL) :
			name(n), parent(p), uri(u), sid(id), skeleton(ske), upAxis(CSM_UNK_UP) { ASSERT(!name.empty()); }
	ColladaJoint(const std::string& n, const std::string& p, const std::string& u, const std::string& id, const ImpFloatMatrix4& m, ColladaSkeleton* ske = NULL) :
			name(n), parent(p), uri(u), sid(id), transformFromParent(m), skeleton(ske), upAxis(CSM_UNK_UP) { ASSERT(!name.empty()); }

	const std::string& getName() const { return name; }
	const std::string& getParentName() const { return parent; }
	const std::string& getUri() const { return uri; }
	const std::string& getSid() const { return sid; }
	const ImpFloatMatrix4& getRelativeMatrix() const { return transformFromParent; }
	ImpFloatMatrix4 getParentMatrix() const { return parentTransform; }
	ColladaSkeleton* getSkeleton() const { return skeleton; }

	void setName(const std::string& n) { name = n; }
	void setParentName(const std::string& n) { parent = n; }
	void setUri(const std::string u) { uri = u; }
	void setSid(const std::string id) { sid = id; }
	void setRelativeMatrix(const ImpFloatMatrix4& m) { transformFromParent = m; }
	void setParentMatrix(const ImpFloatMatrix4& pm) { parentTransform = pm; }
	void setSkeleton(ColladaSkeleton* ske) { skeleton = ske; }

	void setUpAxis(enumUPAXIS up) { upAxis = up; }
	enumUPAXIS getUpAxis() const { return upAxis; }

	friend bool operator==(const ColladaJoint& a, const ColladaJoint& b) { return a.name == b.name; }
	friend bool operator<(const ColladaJoint& a, const ColladaJoint& b) { return a.name < b.name; }
};

class ColladaSkeleton {
private:
	std::map<std::string, ColladaJoint> allBones;
	std::map<std::string, std::string> mapSIDToName;

	struct jointHasNoParent {
		bool operator()(std::pair<std::string, ColladaJoint> pr) { return pr.second.getParentName().empty(); }
	};

	struct jointHasParent {
		std::string parentName;
		jointHasParent(const std::string& pname) :
				parentName(pname) {}
		bool operator()(std::pair<std::string, ColladaJoint> pr) { return pr.second.getParentName() == parentName; }
	};

	template <typename jointPredicate>
	ColladaJoint* findJoint(jointPredicate& pred) {
		std::map<std::string, ColladaJoint>::iterator findIt = find_if(allBones.begin(), allBones.end(), pred);
		if (findIt == allBones.end())
			return NULL;
		return &findIt->second;
	}

	template <typename jointPredicate>
	ColladaJoint* findJoint(jointPredicate& pred, std::map<std::string, ColladaJoint>::iterator itStart, std::map<std::string, ColladaJoint>::iterator itEnd) {
		std::map<std::string, ColladaJoint>::iterator findIt = find_if(itStart, itEnd, pred);
		if (findIt == allBones.end())
			return NULL;
		return &findIt->second;
	}

	struct setSkeleton {
		ColladaSkeleton* skeleton;
		setSkeleton(ColladaSkeleton* s) :
				skeleton(s) {}
		void operator()(std::pair<std::string, ColladaJoint> pr) { pr.second.setSkeleton(skeleton); }
	};

public:
	ColladaSkeleton() {}

	const ColladaJoint& addBone(const ColladaJoint& b) {
		std::map<std::string, ColladaJoint>::iterator it = allBones.find(b.getName());
		if (it == allBones.end())
			allBones.insert(std::pair<std::string, ColladaJoint>(b.getName(), b));

		mapSIDToName[b.getSid()] = b.getName();

		return allBones[b.getName()];
	}

	void clearBones() {
		allBones.clear();
		mapSIDToName.clear();
	}

	const ColladaJoint* findBoneConst(const std::string& name) const {
		std::map<std::string, ColladaJoint>::const_iterator it = allBones.find(name);
		if (it != allBones.end())
			return &it->second;
		return NULL;
	}

	ColladaJoint* findBone(const std::string& name) {
		std::map<std::string, ColladaJoint>::iterator it = allBones.find(name);
		if (it != allBones.end())
			return &it->second;
		return NULL;
	}

	const ColladaJoint* findBoneBySIDConst(const std::string& sid) const {
		std::map<std::string, std::string>::const_iterator it = mapSIDToName.find(sid);
		if (it == mapSIDToName.end())
			return NULL;

		return findBoneConst(it->second);
	}

	ColladaJoint* findBoneBySID(const std::string& sid) {
		std::map<std::string, std::string>::iterator it = mapSIDToName.find(sid);
		if (it == mapSIDToName.end())
			return NULL;

		return findBone(it->second);
	}

	UINT getBoneCount() const { return allBones.size(); }

	//! Get first root bone (usually one one:)
	ColladaJoint* getFirstRoot() {
		jointHasNoParent predicate_NoParent;
		return findJoint(predicate_NoParent);
	}

	//! Get first child of a bone specified by name
	ColladaJoint* getFirstChild(std::string name) {
		jointHasParent predicate_HasParent(name);
		return findJoint(predicate_HasParent);
	}

	//! Get next sibling of a bone specified by name
	ColladaJoint* getNextSibling(std::string name) {
		std::map<std::string, ColladaJoint>::iterator it = allBones.find(name);
		if (it == allBones.end())
			return NULL;

		std::string parent = it->second.getParentName();
		++it;

		jointHasParent predicate_HasParent(parent);
		return findJoint(predicate_HasParent, it, allBones.end());
		/*
		for (; it!=allBones.end(); ++it)
		{
			ColladaJoint& joint = it->second;
			if (joint.getParentName()==parent) return &joint;
		}

		return NULL;*/
	}

	//! Refresh skeleton linkage for all child bones
	//! Call this function when a skeleton is copied (newSkeleton = thisSkeleton; newSkeleton.updateBoneSkeletonLinkage())
	void updateBoneSkeletonLinkage() {
		for_each(allBones.begin(), allBones.end(), setSkeleton(this));
		/*		for (std::map<kstring, ColladaJoint>::iterator it = allBones.begin(); it!=allBones.end(); ++it)
			it->second.setSkeleton(this);*/
	}
};

class ColladaRigidMesh : public ColladaSubmesh {
private:
	// associated joint
	const ColladaJoint* joint;

public:
	ColladaRigidMesh() :
			joint(NULL) {}
	virtual ~ColladaRigidMesh() {}

	const ColladaJoint* getJoint() { return joint; }
	void setJoint(const ColladaJoint* j) { joint = j; }

	//! Collapse the mesh to static version. Return NULL if failed.
	//! Note: Caller is responsible for disposing returned pointer if it's not the same as original mesh.
	virtual const ColladaSubmesh* collapseToStaticMesh() const;
};

class ColladaSkinnedMesh : public ColladaSubmesh {
protected:
	// associated skeleton
	const ColladaSkeleton* skeleton;
	UINT weightsArrSize;
	std::map<const ColladaJoint*, float>* weightsArr; // weights of influencing joints
	std::map<const ColladaJoint*, ImpFloatMatrix4> ibms; // Invert Bind Matrices of influencing joints
	ImpFloatMatrix4 bsm;

public:
	ColladaSkinnedMesh() :
			skeleton(NULL), weightsArr(NULL), weightsArrSize(0) {}
	virtual ~ColladaSkinnedMesh() { deleteWeightsArray(); }

	const ColladaSkeleton* getSkeleton() { return skeleton; }
	void setSkeleton(const ColladaSkeleton* s) { skeleton = s; }

	void deleteWeightsArray() {
		if (weightsArr)
			delete[] weightsArr;
		weightsArr = NULL;
		weightsArrSize = 0;
	}
	void createWeightsArray(UINT vcnt) {
		deleteWeightsArray();
		if (vcnt > 0)
			weightsArr = new std::map<const ColladaJoint*, float>[vcnt];
		weightsArrSize = vcnt;
	}
	UINT getWeightsArraySize() const { return weightsArrSize; }

	void clearWeights(UINT vtxId) {
		jsAssert(weightsArr != NULL);
		jsAssert(vtxId < weightsArrSize);
		weightsArr[vtxId].clear();
	}

	void addWeight(UINT vtxId, const ColladaJoint* joint, float weight) {
		jsAssert(vtxId < weightsArrSize);
		weightsArr[vtxId].insert(std::pair<const ColladaJoint*, float>(joint, weight));
	}

	const std::map<const ColladaJoint*, float>& getWeights(UINT vtxId) const {
		jsAssert(vtxId < weightsArrSize);
		return weightsArr[vtxId];
	}

	float getTotalWeight(UINT vtxId) {
		jsAssert(vtxId < weightsArrSize);
		float total = 0;
		for (std::map<const ColladaJoint*, float>::iterator it = weightsArr[vtxId].begin(); it != weightsArr[vtxId].end(); ++it)
			total += it->second;
		return total;
	}

	void clearInvertedBindMatrices() { ibms.clear(); }
	void addInvertedBindMatrix(const ColladaJoint* joint, const ImpFloatMatrix4& mat) { ibms.insert(std::pair<const ColladaJoint*, ImpFloatMatrix4>(joint, mat)); }
	ImpFloatMatrix4 getInvertedBindMatrix(const ColladaJoint* joint) const {
		std::map<const ColladaJoint*, ImpFloatMatrix4>::const_iterator it = ibms.find(joint);
		if (it != ibms.end())
			return it->second;
		return ImpFloatMatrix4();
	}

	void setBSM(const ImpFloatMatrix4& matrix) { bsm = matrix; }
	const ImpFloatMatrix4& getBSMConst() const { return bsm; }
	ImpFloatMatrix4& getBSM() { return bsm; }

	//! Collapse the mesh to static version. Return NULL if failed.
	//! Note: Caller is responsible for disposing returned pointer if it's not the same as original mesh.
	virtual const ColladaSubmesh* collapseToStaticMesh() const;
};

struct deleteFunc {
	template <typename anyClass>
	void operator()(anyClass* ptr) {
		if (ptr != NULL)
			delete ptr;
	}
};

class ColladaSubmeshWithMorphing : public ColladaSubmesh {
private:
	std::set<ColladaSubmesh*> targetPool; // distinctive pointers to avoid double-deletion
	std::vector<ColladaSubmesh*> targets;
	std::vector<float> weights;
	const domSource* pWeightSource; // source element holding weights data: <source id="$weightsInputElementID">

public:
	ColladaSubmeshWithMorphing() {}
	virtual ~ColladaSubmeshWithMorphing() {
		targets.clear();
		weights.clear();
		for_each(targetPool.begin(), targetPool.end(), deleteFunc());
	}

	void addTarget(ColladaSubmesh* target) {
		targetPool.insert(target);
		targets.push_back(target);
	}

	void addWeight(float weight) {
		weights.push_back(weight);
	}

	void addTargetWeight(ColladaSubmesh* target, float weight) {
		ASSERT(targets.size() == weights.size());
		addTarget(target);
		addWeight(weight);
	}

	UINT getTargetCount() const {
		ASSERT(targets.size() == weights.size());
		return targets.size();
	}

	BOOL getTargetAndWeightById(UINT idx, const ColladaSubmesh*(&target), float& weight) const {
		if (idx >= targets.size() || idx >= weights.size())
			return FALSE;

		target = targets.at(idx);
		weight = weights.at(idx);
		return TRUE;
	}

	void setWeightSource(const domSource* pSource) {
		pWeightSource = pSource;
	}

	const domSource* getWeightSource() {
		return pWeightSource;
	}
};

template <typename ValueType>
struct SimpleArray {
	ValueType* NullRef;
	ValueType* allValues;
	UINT count;

	SimpleArray(int num = 0) :
			count(0), allValues(NULL), NullRef(NULL) {
		resize(num);
	}

	~SimpleArray() {
		if (allValues)
			delete allValues;
	}

	void resize(int cnt) {
		if (allValues != NULL)
			delete allValues;

		count = cnt;
		if (cnt != 0)
			allValues = new ValueType[cnt];
	}

	//! \brief Array member access: non-const version
	ValueType& operator[](UINT id) {
		if (id < count)
			return allValues[id];

		ASSERT(FALSE);
		return *NullRef;
	}

	//! \brief Array member access: const version
	const ValueType& operator[](UINT id) const {
		if (id < count)
			return allValues[id];

		ASSERT(FALSE);
		return *NullRef;
	}

	void replaceBuffer(ValueType* newArray, UINT newCount) {
		if (allValues != NULL)
			delete allValues;

		allValues = newArray;
		if (newArray != NULL)
			count = newCount;
		else
			count = 0;
	}

	ValueType* detachBuffer(UINT& valueCount) {
		ValueType* ret = allValues;
		valueCount = count;

		allValues = NULL;
		count = 0;

		return ret;
	}
};

template <typename OutputType>
struct ColladaAnimSampler {
	SimpleArray<float> timeline;
	SimpleArray<OutputType>* outputValues;

	const OutputType& getOutputValueByIndex(UINT idx) {
		if (outputValues == NULL || outputValues->count <= idx) {
			static OutputType defaultValue = 0;
			return defaultValue;
		}

		return outputValues[idx];
	}

	const OutputType& getOutputValueByTime(TimeMs millis) {
		if (outputValues == NULL) {
			static OutputType defaultValue = 0;
			return defaultValue;
		}

		int from = 0, to = std::min(timeline.count, outputValues->count);

		// boundary check
		if (to == 1 || millis <= (timeline.allValues[from] * 1000.0))
			return outputValues->allValues[from];

		// clamp animation time - don't wrap around
		TimeMs endingTimeMillis = (timeline.allValues[to - 1] * 1000.0);
		if (millis > endingTimeMillis)
			millis = endingTimeMillis;

		// perform a binary search
		while (from < to - 2) {
			// [from, to-1]
			int mid = (from + to) / 2;
			TimeMs midVal = (timeline.allValues[mid] * 1000.0);

			if (millis < midVal)
				to = mid + 1; // "sticky": [from, mid]
			else if (millis > midVal)
				from = mid; // "sticky": [mid, to-1]
			else
				return outputValues->allValues[mid];
		}

		// now only two candidates, perform a final check
		TimeMs fromVal = (timeline.allValues[from] * 1000.0);
		TimeMs toVal = (timeline.allValues[to - 1] * 1000.0);

		// the closer one, no interpolation -- assuming granularity is fine enough
		if (millis - fromVal < toVal - millis)
			return outputValues->allValues[from];
		else
			return outputValues->allValues[to - 1];
	}
};

typedef ColladaAnimSampler<ImpFloatMatrix4> ColladaAnimMatrixSampler;
typedef ColladaAnimSampler<float> ColladaAnimFloatSampler;

template <typename TargetType, typename OutputType>
class ColladaAnimation {
private:
	std::map<TargetType, ColladaAnimSampler<OutputType>*> allAnimData;
	std::string name;

public:
	ColladaAnimation(const std::string& n) :
			name(n) {}
	virtual ~ColladaAnimation() {}

	const std::string& getName() const { return name; }

	void AddAnimationData(const TargetType& target, ColladaAnimSampler<OutputType>* pSampler) {
		allAnimData.insert(std::pair<TargetType, ColladaAnimSampler<OutputType>*>(target, pSampler));
	}

	const std::map<TargetType, ColladaAnimSampler<OutputType>*>& getAllAnimationData() const { return allAnimData; }
};

typedef ColladaAnimation<std::string, ImpFloatMatrix4> ColladaSkeletalAnimation;
typedef ColladaAnimation<std::string, float> ColladaVertexAnimation;

} // namespace KEP
