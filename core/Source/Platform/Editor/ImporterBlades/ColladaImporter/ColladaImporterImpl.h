///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IAssetsDatabase.h"
#include "IAssetImporterImpl.h"
#include "Core/Util/fast_mutex.h"

class DAE;
class daeDatabase;
class daeElement;
class domCOLLADA;
class domNode;
class domAnimation;
class Vertex;

#define VirtualSkeletonRoot "*VROOT*"

namespace KEP {

struct ColladaAnimCacheEntry;
class ColladaSubmesh;
class ColladaSkeleton;
class ImpFloatMatrix4;
enum enumUPAXIS;

class ColladaImporterImpl : public IAssetImporterImpl {
private:
	std::string docURI;
	int globalError; // record global error: file not found, bad file, etc.
	DAE* dae;
	domCOLLADA* root;
	daeDatabase* db;

	std::map<std::string, domNode*> mapUriToDomNode;
	std::map<std::string, std::string> mapUriToJointName;
	std::map<std::string, ImpFloatMatrix4> mapUriToMatrix;
	std::map<std::string, enumUPAXIS> mapUriToUpAxis;
	ColladaSkeleton* skeleton;

	std::map<std::string, int>* importOptions;
	IAssetImporterImpl* pBaseImpl;

	std::map<std::string, ColladaAnimCacheEntry*> animCache;
	fast_recursive_mutex animCacheSync;

protected:
	enumUPAXIS GetMostRecentUpAxis(daeElement* pElem);

	virtual int DoEnumAssets(std::map<KEP_ASSET_TYPE, std::vector<std::string>>& allAssets, BOOL bSkeletal, IClassifyAssetNode* classifyFunc);

	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	int DoLoadMeshByName(const std::string& meshUri, KEP_ASSET_TYPE type, ColladaSubmesh** ppMesh, ImpFloatMatrix4& matrix);

	// Collada DOM utility functions
	bool ReadVertexDataBySemanticByIndex(daeElement* pElement, char* semantic, DWORD index, DWORD set, Vertex& vtx, DWORD& uvCount);

	domNode* GetDomNodeByUri(const std::string& nodeName);
	std::string GetJointNameByUri(const std::string& meshUri);

	int EnumSkeletalAnimationsFromDefinitionFile(std::vector<std::string>& allAnimationURIs, const std::string& filename);

	bool IsValid();

public:
	ColladaImporterImpl();
	ColladaImporterImpl(IAssetImporterImpl* base, const std::string& aRootURI, int aFlags);

	virtual ~ColladaImporterImpl();

	virtual void Register() { IAssetImporterFactory::Instance()->RegisterImporterImpl(this, "ColladaImporter"); }
	virtual void Unregister() { IAssetImporterFactory::Instance()->UnregisterImporterImpl(this); }
	virtual IAssetImporterImpl* spawn(const std::string& aRootURI, int aFlags) { return new ColladaImporterImpl(this, aRootURI, aFlags); }

	virtual int EnumAssetMeshes(std::map<KEP_ASSET_TYPE, std::vector<std::string>>& allMeshURIs, IClassifyAssetNode* classifyFunc);
	virtual int LoadAssetMeshByName(const std::string& meshUri, CEXMeshObj& mesh, int aFlags);
	virtual void PrepareMeshForKEP(CEXMeshObj& mesh);

	virtual int EnumAssetMeshesWithSkeletons(std::map<KEP_ASSET_TYPE, std::vector<std::string>>& allMeshURIs, ColladaSkeleton** allSkeletons, IClassifyAssetNode* classifyFunc);
	virtual int LoadAssetSkeleton(const std::string& rootBone, CSkeletonObject& skeletonObject, int aFlags);
	virtual int LoadAssetRigidMeshByName(const std::string& meshUri, CHierarchyVisObj& hvis, std::string& joint, int aFlags);
	virtual int LoadAssetSkinnedMeshByName(const std::string& meshUri, CDeformableVisObj& dvis, const CSkeletonObject& aSkeletalObject, int aFlags);
	virtual void PrepareSkeletonForKEP(CSkeletonObject& skeleton);
	virtual void PrepareRigidMeshForKEP(CHierarchyVisObj& hvis);
	virtual void PrepareSkinnedMeshForKEP(CDeformableVisObj& dvis);

	virtual int EnumSkeletalAnimations(std::vector<std::string>& allAnimationURIs, IClassifyAssetNode* classifyFunc);
	virtual int LoadSkeletalAnimation(const std::string& animUri, CSkeletonObject* apRefSkeleton, CSkeletonAnimation& aNewAnimation, int aFlags, const AnimImportOptions* apOptions = NULL);
	virtual void PrepareSkeletalAnimationForKEP(CSkeletonAnimation& aNewAnimation); // if aAnimationIndex==-1, prepare all animations

	virtual int EnumVertexAnimations(std::vector<std::string>& allAnimationURIs, IClassifyAssetNode* classifyFunc);
	virtual int LoadVertexAnimation(const std::string& animUri, CEXMeshObj& baseMesh, CVertexAnimationModule& targets);
	virtual void PrepareVertexAnimationForKEP(CEXMeshObj& baseMesh, CVertexAnimationModule& targets);

	virtual int LoadMaterialByName(const std::string& name, CMaterialObject* mat);
	virtual void PrepareMaterialForKEP(CMaterialObject* mat);

	// Asset specs with multi-revisions (KZM v2/v3, Collada 1.3/1.4, etc)
	virtual BOOL SupportsFormat(const std::string& extension, float ver);
	virtual BOOL SupportsURI(const std::string& aURI);
	virtual float GetDocumentVersion(const std::string& aURI);

	virtual ImpFloatMatrix4 GetMatrixByURI(const std::string& meshUri) const;
	virtual enumUPAXIS GetUpAxisByURI(const std::string& meshUri) const;

	virtual void GetSupportedOptions(std::set<std::string>& optionNames);
	virtual bool SetOption(const std::string& optionName, int intValue);
	virtual bool GetOption(const std::string& optionName, int& intValue);

	friend class AddNodeToAssetTree;
};

} // namespace KEP
