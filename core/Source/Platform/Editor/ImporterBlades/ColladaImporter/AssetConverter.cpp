///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CHierarchyMesh.h"
#include "CEXMeshClass.h"
#include "CDeformableMesh.h"
#include "CMaterialClass.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CBoneClass.h"
#include "CBoneAnimationNaming.h"
#include "CVertexAnimationModule.h"
#include "ColladaAssert.h"
#include "IAssetsDatabase.h"
#include "ColladaClasses.h"
#include "AssetConverter.h"
#include "TextureDef.h"
#include "CSkeletonAnimation.h"
#include "SkeletalAnimationManager.h"
#include "CBoneAnimationClass.h"
#include "Core/Math/Vector.h"

using namespace KEP;

//! \brief Convert COLLADA shininess value into KEP unit.
//!
//! Our legacy plugin use MaxMaterial->getShininess() * 100
//! ColladaMAX converted max shininess value into a so-called "micro-facet" factor
//! Now we need to convert it back to max shininess then multiply it by 100.
//!
//! Updated by YC April 2011
//! According to COLLADA 1.4/1.5 SPEC:
//!		"To maximize application compatibility, it is suggested that developers use the Blinn-Torrance-Sparrow for
//!		<shininess> range of 0 to 1. For <shininess> greater than 1.0, the COLLADA author was probably
//!		using an application that followed the Blinn-Phong lighting model, so it is recommended to support both
//!		Blinn equations according to whichever range the shininess resides in."
//!
//! As required by spec, for <blinn> effects, we are supposed to treat shininess value as Blinn's c3 coefficient
//! (see http://www.siggraph.org/education/materials/HyperGraph/illumin/specular_highlights/blinn_model_for_specular_reflect_1.htm).
//! While for <phong> effects, shininess are supposed to be the exponential index. I checked exported DAE files
//! from various software/exporter and found their behaviors vary a lot. Most of the case they use phong semantic
//! for <blinn><shininess> element. So I guess the safest way is to use the method suggested by COLLADA:
//!
//!		If it's greater than 1, treat it as phong exponential index. Otherwise, blinn.
//!		In addition, just to make most people's life easier, I make two special cases: if the shininess value is
//!		exactly 0 or 1, treat it as exponential index, too. This is because most exporter use phong semantic regardless
//!		of actual shader type used. For the ones supporting blinn semantic (e.g. ColladaMAX/Maya 3.04+), they have to
//!		use a complex conversion algorithm to generate blinn shininess value from the software-specific specularity hardness
//!		parameter. The chance the generated value being exactly 0 or 1 is remote.

float ConvertShininess(float fColladaShininess) {
	if (fColladaShininess > 1E-6 && fColladaShininess < 1 - 1E-6) {
		// Use conversion algorithm from ColladaMAX
		const float a = 0.36f, b = 0.45f;

		float kepShininess1 = expf((fColladaShininess - 0.0226f) / -0.157f) * 100.0f;
		float kepShininess2 = logf(fColladaShininess / 0.64893f) / -3.3611f * 100.0f;

		if (fColladaShininess > b)
			return kepShininess1; // Calc with first formula if < a
		if (fColladaShininess < a)
			return kepShininess2; // Calc with second formula if > b

		float coeff = (fColladaShininess - a) / (b - a); // Interpolate if within [a,b]
		float kepShininess = kepShininess1 * coeff + kepShininess2 * (1.0f - coeff);
		kepShininess = floor(kepShininess + 0.5f);
		return kepShininess;
	} else {
		// Otherwise, treat it as exponential index as used in D3D. Plus, ignore negative values just in case.
		return fColladaShininess < 0 ? 0 : fColladaShininess;
	}
}

void ConvertVertexByUpAxis(Vertex& v, enumUPAXIS upAxis) {
	float tmp;
	switch (upAxis) {
		case CSM_X_UP:
			tmp = v.x;
			v.x = -v.y;
			v.y = tmp;
			tmp = v.nx;
			v.nx = -v.ny;
			v.ny = tmp;
			break;
		case CSM_Y_UP:
			// Do nothing
			break;
		case CSM_Z_UP:
			tmp = v.y;
			v.y = v.z;
			v.z = -tmp;
			tmp = v.ny;
			v.ny = v.nz;
			v.nz = -tmp;
			break;
	}

	// Flip Z-axis to convert RH system to LH
	v.z = -v.z;
	v.nz = -v.nz;
}

void ConvertVectorByUpAxis(Vector3f& v, enumUPAXIS upAxis) {
	float tmp;
	switch (upAxis) {
		case CSM_X_UP:
			tmp = v.x;
			v.x = -v.y;
			v.y = tmp;
			break;
		case CSM_Y_UP:
			// Do nothing
			break;
		case CSM_Z_UP:
			tmp = v.y;
			v.y = v.z;
			v.z = -tmp;
			break;
	}

	// Flip Z-axis to convert RH system to LH
	v.z = -v.z;
}

void MatrixConverter::ConvertFrom(const ImpFloatMatrix4& impMatrix, enumUPAXIS upAxis, int flags /*= 0*/) {
	for (int row = 0; row < 4; row++)
		for (int col = 0; col < 4; col++)
			matrix[row][col] = impMatrix.valueAt(col, row);

	// Convert transformation matrix to use the new coordinate system
	for (int col = 0; col < 4; col++) {
		float tmp;
		switch (upAxis) {
			case CSM_X_UP:
				tmp = matrix[0][col];
				matrix[0][col] = -matrix[1][col];
				matrix[1][col] = tmp;
				break;
			case CSM_Y_UP:
				// Do nothing
				break;
			case CSM_Z_UP:
				tmp = matrix[1][col];
				matrix[1][col] = matrix[2][col];
				matrix[2][col] = -tmp;
				break;
		}
	}

	for (int row = 0; row < 4; row++) {
		float tmp;
		switch (upAxis) {
			case CSM_X_UP:
				tmp = matrix[row][0];
				matrix[row][0] = -matrix[row][1];
				matrix[row][1] = tmp;
				break;
			case CSM_Y_UP:
				// Do nothing
				break;
			case CSM_Z_UP:
				tmp = matrix[row][1];
				matrix[row][1] = matrix[row][2];
				matrix[row][2] = -tmp;
				break;
		}
	}

	for (int i = 0; i < 4; i++) {
		matrix[i][2] = -matrix[i][2];
		matrix[2][i] = -matrix[2][i];
	}
	/*
	// change from LH to RH system
	D3DXMatrixTranspose((D3DXMATRIX *)&matrix, (D3DXMATRIX *)&matrix);
	// but keep the translation intact
	matrix._41 = matrix._14;
	matrix._42 = matrix._24;
	matrix._43 = matrix._34;
	matrix._14 = matrix._24 = matrix._34 = 0;*/
}

void KEP::TransformConverter::ConvertFrom(const ImpFloatMatrix4& impMatrix, enumUPAXIS upAxis, int flags /*= 0*/) {
	MatrixConverter::ConvertFrom(impMatrix, upAxis, flags);

	// decompose matrix into four vectors
	position = tmpMatrix.GetTranslation();
	tmpMatrix.GetTranslation() = Vector3f(0, 0, 0);

	Vector4f tmpUp(0.0f, 1.0f, 0.0f, 0.0f), tmpDir(0.0f, 0.0f, 1.0f, 0.0f);

	tmpUp = TransformVector(tmpMatrix, tmpUp);
	tmpDir = TransformVector(tmpMatrix, tmpDir);
	jsAssert(fabs(tmpUp.w) < 1E-10); // Make sure matrix is correct
	jsAssert(fabs(tmpDir.w) < 1E-10);

	tmpUp.Normalize();
	tmpDir.Normalize();

	upVector = tmpUp.SubVector<3>();
	dirVector = tmpDir.SubVector<3>();
	scales.Fill(1.0f); //@@Watch: scaling not computed
}

void StaticMeshConverter::ConstructVertex(const ColladaSubmesh* pCldMesh, const ColladaVertex& cv, Vertex& vtx) {
	UINT posIdx = cv.positionIndex;
	const ImpFloatVector3& pos = pCldMesh->getPositionVector(posIdx);
	UINT nmlIdx = cv.normalIndex;
	ImpFloatVector3 nml = pCldMesh->getNormalVector(nmlIdx);
	nml.normalize();

	vtx.x = pos.x;
	vtx.y = pos.y;
	vtx.z = pos.z;
	vtx.nx = nml.x;
	vtx.ny = nml.y;
	vtx.nz = nml.z;

	UINT texID = 0;
	for (int i = 0; i < CM_MAP_CHANNELS; i++) {
		if (pCldMesh->getTexelBuffer(i) == NULL)
			continue; // skip if no texel buffer for current channel

		TXTUV* pTx = GetKEPTexCoordPtrByChannelID(&vtx, texID);
		pTx->tu = pTx->tv = 0;

		UINT texIdx = cv.texelIndex[i];
		if (texIdx == (UINT)-1)
			continue;

		const TXTUV& tuv = pCldMesh->getTexel(i, texIdx);
		*pTx = tuv;

		texID++;
	}
}

bool StaticMeshConverter::AddTriangles(const ColladaSubmesh* pCldMesh) {
	UINT vcnt = pCldMesh->getVertexCount();
	enumUPAXIS upAxis = pCldMesh->getUpAxis();

	UINT remainder = vcnt % 3;
	if (remainder != 0) {
		//@@Todo: bad data warning
		vcnt -= remainder; // Drop odd vertices
	}

	// Convert coordinate system
	std::vector<Vertex> convertedVertices;
	convertedVertices.reserve(vcnt); // Reserve memory to avoid reallocation
	for (UINT i = 0; i < vcnt; i++) {
		Vertex v;
		ConstructVertex(pCldMesh, pCldMesh->getVertex(i), v);
		ConvertVertexByUpAxis(v, upAxis);
		convertedVertices.push_back(v);
	}

	if (!pCldMesh->isNormalBufferValid()) {
		// Vertex normals not provided, calculate as face normals
		jsAssert(convertedVertices.size() % 3 == 0);
		for (UINT faceIdx = 0; faceIdx < convertedVertices.size() / 3; ++faceIdx) {
			Vertex* v = &convertedVertices[faceIdx * 3];

			Vector3f a(v[1].x - v[0].x, v[1].y - v[0].y, v[1].z - v[0].z);
			Vector3f b(v[2].x - v[1].x, v[2].y - v[1].y, v[2].z - v[1].z);
			Vector3f n = a.Cross(b);
			n.Normalize();

			v[0].nx = v[1].nx = v[2].nx = n.x;
			v[0].ny = v[1].ny = v[2].ny = n.y;
			v[0].nz = v[1].nz = v[2].nz = n.z;
		}
	}

	// Append new vertices
	CAdvancedVertexObj& advancedVtxObj = pExMesh->GetVertices();
	UINT oldVertexCount = advancedVtxObj.GetVertexCount();
	advancedVtxObj.AppendVertexArray(convertedVertices);

	// Append new indices
	std::vector<UINT> newIndices;
	newIndices.reserve(vcnt); // Reserve memory to avoid reallocation
	UINT nextIndex = oldVertexCount;
	for (UINT i = 0; i < vcnt; i++)
		newIndices.push_back(nextIndex++);
	advancedVtxObj.AppendVertexIndexArray(newIndices);

	return TRUE;
}

BOOL StaticMeshConverter::CopyTexCoords(int oldChan, int newChan) {
	if (oldChan == newChan || oldChan < 0 || oldChan >= NUM_CMATERIAL_TEXTURES || newChan < 0 || newChan >= NUM_CMATERIAL_TEXTURES)
		return FALSE;

	CAdvancedVertexObj& vtxObj = pExMesh->GetVertices();

	std::vector<Vertex>* tempVtxArr = vtxObj.GetVertexArray();
	if (!tempVtxArr)
		return FALSE;

	for (std::vector<Vertex>::iterator it = tempVtxArr->begin(); it != tempVtxArr->end(); ++it) {
		Vertex* v = &(*it);
		TXTUV* oldUV = GetKEPTexCoordPtrByChannelID(v, oldChan);
		TXTUV* newUV = GetKEPTexCoordPtrByChannelID(v, newChan);
		*newUV = *oldUV;
	}

	// Increase UV count if necessary
	if (newChan + 1 > vtxObj.GetUVCount())
		vtxObj.SetUVCount(newChan + 1);

	vtxObj.SetVertexArray(*tempVtxArr);

	delete tempVtxArr;

	return TRUE;
}

void StaticMeshConverter::ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, int flags /*= 0*/) {
	VERIFY_RETVOID(pCldMesh != NULL && pExMesh != NULL);

	//if (pCldMesh->getInputOffsetArray())
	//	exmesh.GetVertices().SetUVCount(pCldMesh->getInputOffsetArray()->GetMaxUVIndex());
	UINT uvCount = 0;
	for (UINT i = 0; i < CM_MAP_CHANNELS; i++)
		if (pCldMesh->getTexelBuffer(i) != NULL)
			uvCount++;
	pExMesh->GetVertices().SetUVCount(uvCount);

	// Convert matrix
	Matrix44f worldTransform(pExMesh->GetWorldTransform());
	MatrixConverter mtxCvt(worldTransform);
	mtxCvt.ConvertFrom(matrix, pCldMesh->getUpAxis(), flags);
	pExMesh->SetWorldTransform(mtxCvt.GetMatrix());

	// Convert mesh
	AddTriangles(pCldMesh);

	// Convert material
	if (pExMesh->m_materialObject != NULL) {
		delete pExMesh->m_materialObject;
		pExMesh->m_materialObject = NULL;
	}

	CMaterialObject* newMat = new CMaterialObject();
	if (pCldMesh->getMaterial()) {
		MaterialConverter matCon(newMat);
		matCon.ConvertFrom(&*pCldMesh->getMaterial(), flags);
	}
	pExMesh->m_materialObject = newMat;
}

void RigidMeshConverter::ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, int flags /*= 0*/) {
	hVis.m_mesh = pExMesh = new CEXMeshObj();
	StaticMeshConverter::ConvertFrom(pCldMesh, matrix, flags);
}

void SkinnedMeshConverter::ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, int flags /*= 0*/) {
	ColladaSkinnedMesh* pCldSkinnedMesh = const_cast<ColladaSkinnedMesh*>(dynamic_cast<const ColladaSkinnedMesh*>(pCldMesh));
	VERIFY_RETVOID(pCldSkinnedMesh != NULL && pDVis != NULL && pSkeleton != NULL);

	// Combine node transform with BSM
	// Note: left multiply: the one closer to the position vector should be on the left.
	ImpFloatMatrix4 combinedMatrix = pCldSkinnedMesh->getBSMConst();
	combinedMatrix *= matrix;

	// Transform all positions and normals in buffers
	for (UINT posLoop = 0; posLoop < pCldSkinnedMesh->getPositionCount(); posLoop++) {
		ImpFloatVector4 pos = pCldSkinnedMesh->getPositionVector(posLoop);
		pos.w = 1;
		pCldSkinnedMesh->setPositionVector(posLoop, combinedMatrix.Transform(pos));
	}

	ImpFloatMatrix4 transformForNormal = combinedMatrix;
	transformForNormal.Inverse();
	transformForNormal.Transpose();
	transformForNormal[3][0] = transformForNormal[3][1] = transformForNormal[3][2] = 0;
	transformForNormal[0][3] = transformForNormal[1][3] = transformForNormal[2][3] = 0;
	transformForNormal[3][3] = 1.0f;

	for (UINT nmlLoop = 0; nmlLoop < pCldSkinnedMesh->getNormalCount(); nmlLoop++) {
		ImpFloatVector4 nml = pCldSkinnedMesh->getNormalVector(nmlLoop);
		nml.w = 0;
		nml = transformForNormal.Transform(nml);
		nml.normalize();
		pCldSkinnedMesh->setNormalVector(nmlLoop, nml);
	}

	pDVis->m_blendedType = SkinVertexBlendType::Position_Normal; // always 1
	pDVis->m_mesh = pExMesh = new CEXMeshObj();
	StaticMeshConverter::ConvertFrom(pCldMesh, ImpFloatMatrix4(), flags); // use identity matrix as mesh data has been transformed

	ImpFloatMatrix4 bsm = pCldSkinnedMesh->getBSMConst();
	ImpFloatMatrix4 bsmInv = bsm;
	bsmInv.Inverse();

	for (UINT vtxLoop = 0; vtxLoop < pCldSkinnedMesh->getVertexCount(); vtxLoop++) {
		const ColladaVertex& cv = pCldSkinnedMesh->getVertex(vtxLoop);

		ImpFloatVector4 pos = pCldSkinnedMesh->getPositionVector(cv.positionIndex);
		ImpFloatVector4 nml = pCldSkinnedMesh->getNormalVector(cv.normalIndex);
		pos.w = 1;
		nml.w = 0;

		const std::map<const ColladaJoint*, float>& weightsMap = pCldSkinnedMesh->getWeights(cv.positionIndex);
		if (weightsMap.size() == 0)
			continue; // skip vertices without controlling bones

		std::vector<VertexBoneWeight> vertexBoneWeight;
		vertexBoneWeight.resize(weightsMap.size());

		int traceNo = 0;
		for (const auto& pr : weightsMap) {
			const ColladaJoint* pJoint = pr.first;
			int boneIndex = pSkeleton->getBoneIndexByName(pJoint->getName());

			assert(boneIndex >= 0 && boneIndex <= 255);
			vertexBoneWeight[traceNo].boneIndex = static_cast<byte>(boneIndex);
			vertexBoneWeight[traceNo].weight = pr.second;

			ImpFloatMatrix4 ibm = pCldSkinnedMesh->getInvertedBindMatrix(pJoint);
			//ibm = bsm * ibm;

			ImpFloatMatrix4 tbm = ibm;
			tbm.Inverse();
			tbm.Transpose();
			tbm._m41 = tbm._m42 = tbm._m43 = tbm._m14 = tbm._m24 = tbm._m34 = 0.0f;
			tbm._m44 = 1.0f;

			ImpFloatVector3 newpos = ibm.Transform(pos);
			ImpFloatVector3 newnml = tbm.Transform(nml);
			newnml.normalize();

			vertexBoneWeight[traceNo].positionInBoneSpace.Set(newpos.x, newpos.y, newpos.z);
			ConvertVectorByUpAxis(vertexBoneWeight[traceNo].positionInBoneSpace, pJoint->getUpAxis());

			vertexBoneWeight[traceNo].normalInBoneSpace.Set(newnml.x, newnml.y, newnml.z);
			ConvertVectorByUpAxis(vertexBoneWeight[traceNo].normalInBoneSpace, pJoint->getUpAxis());

			traceNo++;
		}

		pDVis->AddVertexAssignmentBlended(std::move(vertexBoneWeight), vtxLoop);
	}
}

//! Perform a DFS to append everything under pRootJoint into skeletonObject
void SkeletonConverter::ConvertFrom(ColladaSkeleton* pCldSkeleton, ColladaJoint* pRootJoint, RuntimeSkeleton* pRefSkeleton /*= NULL*/, int flags /*= 0*/, const std::map<std::string, ImpFloatVector3>* pRootOffsets /*= NULL*/) {
	jsAssert(pCldSkeleton != NULL && pRootJoint != NULL);

	//A non-recursive algorithm is used here.
	const ColladaJoint* pCurrParent = pRootJoint;
	ColladaJoint* pCurrJoint = pCldSkeleton->getFirstChild(pCurrParent->getName());
	ImpFloatMatrix4 parentMatrix;
	while (pCurrParent) // ending condition: parent is NULL
	{
		if (!pCurrJoint) // end of current level
		{
			// Back-track to upper level
			pCurrJoint = pCldSkeleton->getNextSibling(pCurrParent->getName()); // next node in upper level
			pCurrParent = pCldSkeleton->findBoneConst(pCurrParent->getParentName());
			if (pCurrParent)
				parentMatrix = pCurrParent->getRelativeMatrix() * pCurrParent->getParentMatrix();
			else
				parentMatrix.Identity();
		} else {
			// Store parent transform
			pCurrJoint->setParentMatrix(parentMatrix);
			ImpFloatMatrix4 relMatrix = pCurrJoint->getRelativeMatrix();

			// normalize relative matrix to match bones in reference skeleton ( pRefSkeleton )
			if (pRefSkeleton) {
				CBoneObject *pBoneObj = NULL, *pParentBoneObj = NULL;
				pRefSkeleton->getBoneByName(pCurrJoint->getName(), &pBoneObj, true);
				if (pBoneObj && pCurrParent)
					pRefSkeleton->getBoneByName(pBoneObj->m_parentName, &pParentBoneObj, false);

				if (!pBoneObj) {
					// Unrecognized bone (no matches in reference skeleton)
				} else if (!pParentBoneObj) {
					// Skeleton root
					if (pRootOffsets) {
						std::map<std::string, ImpFloatVector3>::const_iterator itRootOffset = pRootOffsets->find(pCurrJoint->getName());
						if (itRootOffset != pRootOffsets->end()) {
							// Skeleton root: subtract root offset from reference skeleton
							// Reference skeleton must provide offsets to be subtracted in its all root bones.
							// At first frame: caller should set root offsets to 0 in order to obtain initial root offsets from animation
							// For all frames, root bone translations will be adjusted by the difference between initial animation root offsets and desired root offset
							ImpFloatVector3 offset = itRootOffset->second;
							float tmp;
							switch (pCurrJoint->getUpAxis()) {
								case CSM_X_UP:
									tmp = offset.x;
									offset.x = offset.y;
									offset.y = tmp;
									break;
								case CSM_Z_UP:
									tmp = offset.z;
									offset.z = offset.y;
									offset.y = tmp;
									break;
							}

							relMatrix._m41 -= offset.x;
							relMatrix._m42 -= offset.y;
							relMatrix._m43 -= offset.z;
						}
					}
				} else {
					ImpFloatVector3 boneOfsTemplate(pBoneObj->m_startPosition(3, 0), pBoneObj->m_startPosition(3, 1), pBoneObj->m_startPosition(3, 2));
					if (pParentBoneObj) {
						// Take offset from parent (if exists)
						boneOfsTemplate.x -= pParentBoneObj->m_startPosition(3, 0);
						boneOfsTemplate.y -= pParentBoneObj->m_startPosition(3, 1);
						boneOfsTemplate.z -= pParentBoneObj->m_startPosition(3, 2);

						float scaleTemplate = boneOfsTemplate.getNormSquare();

						ImpFloatVector3 boneOfsImp(relMatrix._m41, relMatrix._m42, relMatrix._m43);
						float scaleImport = boneOfsImp.getNormSquare();

						if (scaleImport <= 1E-12) {
							if (abs(scaleImport - scaleTemplate) > 1E-6) // fix only if bone length has changed significantly
							{
								// Imported length is 0 -- copy bone translation from template
								// Todo: take rotation into account
								relMatrix._m41 = boneOfsTemplate.x;
								relMatrix._m42 = boneOfsTemplate.y;
								relMatrix._m43 = boneOfsTemplate.z;
							}
						} else {
							// Scale imported bone
							float ratio = sqrt(scaleTemplate / scaleImport);
							if (abs(ratio - 1.0) > 1E-3) // scale only if bone length has changed significantly
							{
								relMatrix._m41 *= ratio;
								relMatrix._m42 *= ratio;
								relMatrix._m43 *= ratio;
							}
						}

						pCurrJoint->setRelativeMatrix(relMatrix);
					}
				}
			}

			ImpFloatMatrix4 matrix = relMatrix * parentMatrix;

			// process current node
			CBoneObject* pBone;

			// Break down matrix into dirVector, upVector, position and scale
			Vector3f dirVector, upVector, position, scales;
			TransformConverter tc(dirVector, upVector, position, scales);
			tc.ConvertFrom(matrix, pCurrJoint->getUpAxis(), flags);

			skeleton.addBone(pCurrJoint->getName().c_str(), pCurrJoint->getParentName().c_str(), dirVector, upVector, position, scales, &pBone);
			pBone->m_lastToTargetTimeStamp[0] = 0;
			pBone->m_lastToTargetTimeStamp[1] = 0;
			pBone->m_lastPositionsAreInitialized[0] = FALSE;
			pBone->m_lastPositionsAreInitialized[1] = FALSE;
			pBone->m_procAnimmat = -1;
			//pBone->m_referencedAnimList       = TRUE;

			// Try all children if available
			pCurrParent = pCurrJoint;
			pCurrJoint = pCldSkeleton->getFirstChild(pCurrJoint->getName());
			parentMatrix = matrix;
		}
	}

	// Initialize bone hierarchy
	skeleton.buildHierarchy();

	// Call initBones to prepare inverted matrices and etc.
	skeleton.initBones();
}

void MaterialConverter::initializeKEPMaterial(CMaterialObject* pMat) {
	if (pMat == NULL)
		return;

	for (int i = 0; i < NUM_CMATERIAL_TEXTURES; i++)
		pMat->m_texNameHash[i] = -1; // -1: not used @@Watch: for some reason constructor initialize it to zero (plugin uses a different version of constructor)
}

void MaterialConverter::ConvertFrom(const ColladaMaterial* pCldMat, int flags /*= 0*/) {
	VERIFY_RETVOID(pCldMat != NULL && pMaterial != NULL);

	initializeKEPMaterial(pMaterial);

	//Load basic material
	if (pCldMat->channelHasValue(CM_AMBIENT, CT_COLOR))
		pMaterial->m_baseMaterial.Ambient = pCldMat->getColorByChannel(CM_AMBIENT);
	if (pCldMat->channelHasValue(CM_DIFFUSE, CT_COLOR))
		pMaterial->m_baseMaterial.Diffuse = pCldMat->getColorByChannel(CM_DIFFUSE);
	if (pCldMat->channelHasValue(CM_SPECULAR, CT_COLOR))
		pMaterial->m_baseMaterial.Specular = pCldMat->getColorByChannel(CM_SPECULAR);
	if (pCldMat->channelHasValue(CM_EMISSIVE, CT_COLOR))
		pMaterial->m_baseMaterial.Emissive = pCldMat->getColorByChannel(CM_EMISSIVE);
	pMaterial->m_baseMaterial.Power = ConvertShininess(pCldMat->getFloatByChannel(CM_SHININESS));

	//Load textures
	//Retrieve all texture file names from ColladaMaterial

	UINT texID = 0;
	for (int i = 0; i < CM_MAP_CHANNELS; i++) {
		if (pCldMat->channelHasValue(i, CT_TEXTURE)) // safe to type only: unresolved texture channels has been cleaned up in ColladaUtil (turned into color channels)
		{
			std::string textureUri = pCldMat->getTextureByChannel(i).getImageURI();

			//check transparency
			if (i == CM_TRANSPARENT && pCldMat->channelHasValue(CM_DIFFUSE, CT_TEXTURE)) {
				const std::string& diffuseMap = pCldMat->getTextureByChannel(CM_DIFFUSE).getImageURI();
				if (STLCompareIgnoreCase(textureUri, diffuseMap) == 0) {
					//Same map used in transparency as in diffuse, enable alpha blending
					//@@Todo: check UV?
					//@@Todo: Determine if using one-bit alpha or alpha channel
					pMaterial->m_baseBlendMode = pMaterial->m_blendMode = 1; // alpha channel for now
					continue; // Do not add to multi-texturing list if this is used for indicating transparency with the same map as diffuse
				}
			}

			std::string convertedTexturePath;
			TextureDef texDef = IAssetsDatabase::Instance()->ImportTexture(textureUri, flags);
			if (texDef.valid) {
				convertedTexturePath = texDef.fileName;
			}

			pMaterial->m_texFileName[texID] = convertedTexturePath;
			if (texID > 0) // note: operator of texture 0 is set in m_BlendMethod[9], texture 1 in m_BlendMethod[0], etc.
				pMaterial->m_texBlendMethod[texID - 1] = 0; // mark operation against previous texture as modulated (default)
			pMaterial->m_texNameHash[texID] = 0; // mark as used (-1=not used)
			texID++;
		}
	}
}

bool AnimationConverter::ConvertFrom(const ColladaSkeleton* pCldSkeleton, const ColladaSkeletalAnimation* pCldAnim, int firstFrame, int lastFrame, double fps, RuntimeSkeleton* pRefSkeleton /*= NULL*/, int flags /*= 0*/, const AnimImportOptions* pOptions /*= NULL*/) {
	ColladaSkeleton tmpCldSkeleton = *pCldSkeleton;
	tmpCldSkeleton.updateBoneSkeletonLinkage();

	//Compute frame count
	UINT frameCount = INT_MAX;
	const std::map<std::string, ColladaAnimMatrixSampler*>& allBoneAnim = pCldAnim->getAllAnimationData();
	for (const auto& it : allBoneAnim) {
		ColladaAnimMatrixSampler* pSampler = it.second;
		if (!pSampler || !pSampler->outputValues)
			continue;

		frameCount = std::min(frameCount, std::min(pSampler->timeline.count, pSampler->outputValues->count));
	}

	if (frameCount == INT_MAX)
		return false;

	if (lastFrame == -1)
		lastFrame = frameCount - 1; // compute last frame# if -1

	newAnimation.m_name = pCldAnim->getName();
	newAnimation.m_animType = CBoneAnimationNaming::AnimNameToType(pCldAnim->getName().c_str());
	int numKeyFrames = lastFrame - firstFrame + 1;
	newAnimation.setAnimDurationMs(1000.0 * (numKeyFrames - 1) / fps);
	newAnimation.setAnimFrameTimeMs(1000.0 / fps);
	newAnimation.setAnimLooping(TRUE);
	newAnimation.m_animMinTimeMs = newAnimation.m_animDurationMs;

	bool bBoneKeyFramesInitialized = false;

	//Convert individual frames
	TimeMs initialMillis = 1000.0 * firstFrame / fps;
	TimeMs frameLength = 1000.0 / fps;
	for (int frameNo = firstFrame; frameNo <= lastFrame; frameNo++) {
		TimeMs millis = 1000.0 * frameNo / fps;

		//Compile a temporary skeleton with matrices of current frame
		for (const auto& it : allBoneAnim) {
			const std::string& boneName = it.first;
			ColladaJoint* pJoint = tmpCldSkeleton.findBone(boneName);
			if (!pJoint)
				continue;

			ColladaAnimMatrixSampler* pSampler = it.second;
			if (!pSampler)
				continue;

			const ImpFloatMatrix4& matrix = pSampler->getOutputValueByTime(millis);
			pJoint->setRelativeMatrix(matrix);
		}

		std::shared_ptr<RuntimeSkeleton> pTmpSkeletonObj = RuntimeSkeleton::New();
		SkeletonConverter sc(*pTmpSkeletonObj);
		sc.ConvertFrom(&tmpCldSkeleton, tmpCldSkeleton.getFirstRoot(), pRefSkeleton, flags, pOptions ? &pOptions->rootOffsets : NULL);

		if (!bBoneKeyFramesInitialized) {
			// Set # of bones
			newAnimation.setNumBones(pTmpSkeletonObj->getBoneCount());

			for (UINT i = 0; i < newAnimation.m_numBones; i++) {
				CBoneAnimationObject* pBoneAnimObj = new CBoneAnimationObject;
				pBoneAnimObj->m_pAnimKeyFrames = NULL;
				newAnimation.m_ppBoneAnimationObjs[i] = pBoneAnimObj;
			}

			bBoneKeyFramesInitialized = true;
		}

		if (newAnimation.m_skeletonDef == NULL) {
			newAnimation.m_skeletonDef = new SkeletonDef;
			ASSERT(pTmpSkeletonObj->m_pBoneList != NULL);
			for (POSITION p = pTmpSkeletonObj->getBoneList()->GetHeadPosition(); p != NULL;) {
				auto pBone = static_cast<const CBoneObject*>(pTmpSkeletonObj->getBoneList()->GetNext(p));
				if (pBone == NULL)
					continue;

				newAnimation.m_skeletonDef->AddBone(pBone->m_boneName, pTmpSkeletonObj->getInitialBoneLength(pBone->m_boneName), pBone->m_parentName);
			}
		}

		//Copy pose of temporary skeleton to original skeleton as animation data
		ASSERT(pTmpSkeletonObj->m_pBoneList != NULL);
		int boneIndex = 0;
		for (POSITION p = pTmpSkeletonObj->getBoneList()->GetHeadPosition(); p; ++boneIndex) {
			auto pBone = static_cast<const CBoneObject*>(pTmpSkeletonObj->getBoneList()->GetNext(p));
			if (pBone == NULL)
				continue;

			const Matrix44f& matrix = pBone->m_startPosition;
			Vector3f pos = matrix.GetTranslation();
			Vector3f dir = matrix.GetRowZ();
			Vector3f up = matrix.GetRowY();

			CBoneAnimationObject* pBAO = newAnimation.GetBoneAnimationObj(boneIndex);

			if (pBAO && !pBAO->m_pAnimKeyFrames) {
				pBAO->m_animDurationMs = newAnimation.m_animDurationMs;
				pBAO->m_animKeyFrames = numKeyFrames;
				pBAO->m_animFrameTimeMs = 1000.0 / fps;
				pBAO->m_animMinTimeMs = newAnimation.m_animMinTimeMs;
				pBAO->m_pAnimKeyFrames = new AnimKeyFrame[numKeyFrames];
			}

			if (pBAO) {
				pBAO->m_pAnimKeyFrames[frameNo - firstFrame].at = dir;
				pBAO->m_pAnimKeyFrames[frameNo - firstFrame].pos = pos;
				pBAO->m_pAnimKeyFrames[frameNo - firstFrame].up = up;
				pBAO->m_pAnimKeyFrames[frameNo - firstFrame].m_timeMs = (millis - initialMillis + frameLength);
				// timestamps specify ending time of a frame, not starting time
			}
		}
	}

	newAnimation.Hash();

	AnimationRM::Instance()->AddSkeletonAnimation(&newAnimation);
	return true;
}

void KEP::VertexAnimationConverter::ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, const ColladaVertexAnimation& animation, int firstFrame, int lastFrame, double fps, int flags /*= 0*/) {
	const ColladaSubmeshWithMorphing* pCldMorphedMesh = dynamic_cast<const ColladaSubmeshWithMorphing*>(pCldMesh);
	VERIFY_RETVOID(pCldMorphedMesh != NULL && pExMesh != NULL && pVtxAnim != NULL);

	// Convert base mesh
	StaticMeshConverter::ConvertFrom(pCldMorphedMesh, matrix, flags);
	CEXMeshObj* pBaseMesh = StaticMeshConverter::pExMesh;

	//@@Limitation: current implementation requires animation fragments start from first frame to ensure correct morphing
	//@@Todo: arbitrary animation fragments
	ASSERT(firstFrame == 0);

	// Convert targets
	UINT numTargets = pCldMorphedMesh->getTargetCount();
	float* pTgtWeights = new float[numTargets];
	CEXMeshObj* pTgtMeshes = new CEXMeshObj[numTargets];

	for (UINT tgtLoop = 0; tgtLoop < numTargets; tgtLoop++) {
		const ColladaSubmesh* pTarget = NULL;
		float weight = 0.0f;
		pCldMorphedMesh->getTargetAndWeightById(tgtLoop, pTarget, weight);
		ASSERT(pTarget != NULL);

		StaticMeshConverter smcTgt(&pTgtMeshes[tgtLoop]);
		smcTgt.ConvertFrom(pTarget, matrix, flags);
		pTgtWeights[tgtLoop] = 0.0f;
	}

	//Compute frame count
	UINT frameCount = INT_MAX;
	const std::map<std::string, ColladaAnimFloatSampler*>& allVertAnim = animation.getAllAnimationData();
	for (std::map<std::string, ColladaAnimFloatSampler*>::const_iterator it = allVertAnim.begin(); it != allVertAnim.end(); ++it) {
		ColladaAnimFloatSampler* pSampler = it->second;
		if (pSampler->outputValues == NULL)
			continue;

		frameCount = std::min(frameCount, std::min(pSampler->timeline.count, pSampler->outputValues->count));
	}

	if (lastFrame == -1)
		lastFrame = frameCount - 1; // compute last frame# if -1

	//Initialize CVertexAnimationModule
	pVtxAnim->m_curFrame = 0;
	pVtxAnim->m_targetFrame = 0;
	pVtxAnim->m_timeBetweenFrames = 1000.0 / fps;
	pVtxAnim->m_looping = TRUE;
	pVtxAnim->m_lastUpdatedFrameChngTime = 0;
	pVtxAnim->m_currentFrameTime = 0;

	//Convert individual frames
	for (int frameNo = firstFrame; frameNo <= lastFrame; frameNo++) {
		TimeMs millis = 1000.0 * frameNo / fps;

		//Compile a temporary weights array
		for (const auto& it : allVertAnim) {
			//Obtain weight data
			ColladaAnimFloatSampler* pSampler = it.second;
			if (!pSampler)
				continue;

			float weight = pSampler->getOutputValueByTime(millis);

			//Update weight

			//Parse Target ID
			UINT targetID = (UINT)-1; // initialize with invalid value
			std::string strTargetID = it.first;
			if (strTargetID.empty() || strTargetID[0] != '(' || strTargetID[strTargetID.length() - 1] != ')')
				continue; // not an target ID

			std::stringstream ss(strTargetID.substr(1, strTargetID.length() - 2));
			ss >> targetID;
			if (targetID >= numTargets)
				continue; // invalid ID value

			pTgtWeights[targetID] = weight;
		}

		pVtxAnim->AddKeyFrameByMorphing(*pBaseMesh, numTargets, pTgtMeshes, pTgtWeights);
	}

	delete[] pTgtWeights;

	for (UINT tgtLoop = 0; tgtLoop < numTargets; tgtLoop++)
		pTgtMeshes[tgtLoop].SafeDelete();
	delete[] pTgtMeshes;

	pBaseMesh->SafeDelete();
	delete pBaseMesh;
}
