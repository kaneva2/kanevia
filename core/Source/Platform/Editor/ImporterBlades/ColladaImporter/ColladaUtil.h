///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

#include "ColladaClasses.h"

#define COLLADA_OK 1
#define COLLADA_NOTFOUND 0
#define COLLADA_ERR_BAD_FORMAT -1 // ill-formatted COLLADA document (missing required element, bad url target, etc.)
#define COLLADA_ERR_NOT_SUPPORTED -2 // unsupported COLLADA element (see user's manual for supported scene composition)
#define COLLADA_ERR_COLLADADOM -3 // COLLADA DOM internal error
#define COLLADA_ERR_MEMORY -4 // Memory allocation error during import
#define COLLADA_ERR_INTERNAL -5 // Other internal error
#define COLLADA_ERR_FILESIZE -6 // File size over limit

namespace KEP {

#define VERTICES_POSITION 1
#define VERTICES_NORMAL 2
#define VERTICES_TEXCOORD 4

typedef unsigned int VertexSemantics; // A bit field describing the parts of a "VERTEX" semantic in the <triangle> element, as defined in the <vertices> element.

class NodeHandler {
public:
	virtual BOOL operator()(KEP_ASSET_TYPE assetType, const std::string& nodeName, int numSubmeshes, domNode* pDomNode, const ImpFloatMatrix4& matrix, const ImpFloatMatrix4& parentMatrix, const std::string& jointName, enumUPAXIS upAxis, enumUPAXIS parentUpAxis) = 0;
};

class JointHandler {
public:
	virtual BOOL operator()(const std::string& jointName, const std::string& parentJoint, const std::string& sid, const ImpFloatMatrix4& matrix, enumUPAXIS upAxis) = 0;
};

class AnimationHandler {
public:
	virtual void operator()(domAnimation* pDomAnimation) = 0;
	virtual void operator()(domAnimation_clip* pDomAnimClip) = 0;
};

class ColladaUtil {
public:
	///////////////////////////////////////////////////////////////////////////////////////
	// Assets Preview
	static int PreviewDomNode(enumUPAXIS parentUpAxis, const std::string& parentName, domNode* pDomNode, const ImpFloatMatrix4& parentMatrix, const std::string& parentJoint, NodeHandler* handleNode, JointHandler* handleJoint, IClassifyAssetNode* classifyFunc);
	static UINT ReadDomSubMeshCount(domInstance_geometry* pDomInstGeom);
	static UINT ReadDomSubMeshCount(domInstance_controller* pDomInstCntlr, BOOL& isMorph);
	static UINT ReadDomSubMeshCount(domMesh* pDomMesh);

	///////////////////////////////////////////////////////////////////////////////////////
	// Static Mesh

	//! Read data from <node> with nested <instance_geometry>
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomNode_Geoms(enumUPAXIS parentUpAxis, domNode* pDomNode, int subID, ColladaSubmesh& submesh, ImpFloatMatrix4& matrix);

	//! Read data from <instance_geometry>
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomInstance_Geometry(enumUPAXIS parentUpAxis, domInstance_geometry* pDomInstGeom, UINT submeshID, ColladaSubmesh& submesh);

	//! Read mesh data from <geometry>
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomGeometry(enumUPAXIS parentUpAxis, domGeometry* pDomGeom, UINT submeshID, ColladaSubmesh& submesh);

	//! Load vertices and faces from a <triangles> element.
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomTriangles(enumUPAXIS parentUpAxis, domTriangles* pDomTris, VertexSemantics vs, ColladaSubmesh& submesh);

	//! Load vertices and faces from a <trifans> element.
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomTrifans(enumUPAXIS parentUpAxis, domTrifans* pDomTfans, VertexSemantics vs, ColladaSubmesh& submesh);

	//! Load vertices and faces from a <tristrips> element.
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomTristrips(enumUPAXIS parentUpAxis, domTristrips* pDomTstrips, VertexSemantics vs, ColladaSubmesh& submesh);

	//! Load vertices and faces from a <polygons> element.
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomPolygons(enumUPAXIS parentUpAxis, domPolygons* pDomPolys, VertexSemantics vs, ColladaSubmesh& submesh);

	//! Load vertices and faces from a <polylist> element.
	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomPolyList(enumUPAXIS parentUpAxis, domPolylist* pDomPolyList, VertexSemantics vs, ColladaSubmesh& submesh);

	static ImpFloatMatrix4 ReadDomNodeTransformation(domNode* pDomNode);
	static bool ReadDomLookat(domLookat* pDomMatrix, ImpFloatMatrix4& matrix);
	static bool ReadDomMatrix(domListOfFloats& floatArr, ImpFloatMatrix4& matrix, UINT offset = 0);
	static bool ReadDomRotate(domRotate* pDomRotate, ImpFloatMatrix4& matrix);
	static bool ReadDomScale(domScale* pDomScale, ImpFloatMatrix4& matrix);
	static bool ReadDomSkew(domSkew* pDomSkew, ImpFloatMatrix4& matrix);
	static bool ReadDomTranslate(domTranslate* pDomTranslate, ImpFloatMatrix4& matrix);

	//! Read data from <any_element><assets><up_axis>
	static enumUPAXIS ReadDomUpAxis(daeElement* pElem);
	//! Read data from <assets><up_axis>
	static enumUPAXIS ReadDomAssetUpAxis(domAsset* pDomAsset);

	///////////////////////////////////////////////////////////////////////////////////////
	// Material, Texture and Effects

	static void ReadDomBindMaterial(domBind_material* pDomBindMat, ColladaSubmesh& submesh);

	static bool ReadDomMaterial(domMaterial* pDomMat, ColladaMaterial& mat);
	static bool ReadDomEffect(domEffect* pDomEffect, ColladaMaterial& mat);
	static bool ReadDomFxProfileCommon(domProfile_COMMON* pDomProfile, ColladaMaterial& mat);
	static bool ReadDomFxProfileCommonTechExtra(domExtra* pDomExtra, ColladaMaterial& mat);
	static bool ReadDomFxProfileCommonExtra(domExtra* pDomExtra, ColladaMaterial& mat);
	static bool ReadDomFxProfileFCollada(domTechnique* pDomTech, ColladaMaterial& mat);
	static bool ReadDomFxProfileGoogleEarth(domTechnique* pDomTech, ColladaMaterial& mat);
	static bool ReadDomFxShading(daeElement* pShading, ColladaMaterial& mat);
	static bool ReadDomColorOrTexture(domCommon_color_or_texture_type_complexType* pColor, UINT chan, ColladaMaterial& mat);
	static bool ReadDomColorOrTexture(daeElement* pElement, UINT chan, ColladaMaterial& mat);
	static bool ReadDomTexture(xsNCName textureParam, xsNCName uvSetSymbol, ColladaTexture& tex);
	static bool ReadDomBindVertexInput(domInstance_material::domBind_vertex_input* pBindVtx, const ColladaSubmesh& submesh, ColladaMaterial& mat);
	static bool ReadDomBind(domInstance_material::domBind* pBindVtx, ColladaMaterial& mat);
	static bool ReadDomNewParamSampler(domFx_sampler2D_common* pDomSampler2D, ColladaFxSampler& sampler);
	static bool ReadDomNewParamSurface(domFx_surface_common* pDomSurface, ColladaFxSurface& surface);
	static bool ReadDomSurfaceInitFrom(domFx_surface_init_from_common* pSurfaceInitFrom, ColladaFxSurface& surface);

	///////////////////////////////////////////////////////////////////////////////////////
	// Skinning and Morphing

	//! \brief Read skinned mesh and matrix from <node>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomNode_Skinning(enumUPAXIS parentUpAxis, domNode* pDomNode, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh, ImpFloatMatrix4& matrix);

	//! \brief Read skinned mesh from <instance_controller>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomInstance_Controller_Skinning(enumUPAXIS parentUpAxis, domInstance_controller* pDomInstCntlr, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh);

	//! \brief Read skinned mesh from <controller>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomController_Skinning(enumUPAXIS parentUpAxis, domController* pDomCntlr, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh);

	//! \brief Read skinned mesh from <skin>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomSkin(enumUPAXIS parentUpAxis, domSkin* pDomSkin, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh);

	//! Return 1 if succeeded, 0 if not found, <0 for error codes
	//static int ReadDomNode_Joints(domNode* pDomNode, const kstring& parentName, const ImpFloatMatrix4& parentTransform, ColladaSkeleton& ske);

	static int ReadDomSkin_IBMs(domSkin::domJoints* pDomJoints, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh);
	static int ReadDomSkin_VertexWeights(domSkin::domVertex_weights* pDomVtxWts, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh);

	//! \brief Read morph animation and matrix from <node>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomNode_Morphing(enumUPAXIS parentUpAxis, domNode* pDomNode, int subID, ColladaSubmeshWithMorphing& morphedMesh, ImpFloatMatrix4& matrix);

	//! \brief Read morph animation from <instance_controller>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomInstance_Controller_Morphing(enumUPAXIS parentUpAxis, domInstance_controller* pDomInstCntlr, int subID, ColladaSubmeshWithMorphing& morphedMesh);

	//! \brief Read morph animation from <controller>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomController_Morphing(enumUPAXIS parentUpAxis, domController* pDomCntlr, int subID, ColladaSubmeshWithMorphing& morphedMesh);

	//! \brief Read skinned mesh from <skin>
	//! \return 1 if succeeded, 0 if not found, <0 for error codes
	static int ReadDomMorph(enumUPAXIS parentUpAxis, domMorph* pDomSkin, int subID, ColladaSubmeshWithMorphing& morphedMesh);

	static int EnumAllAnimations(domLibrary_animations* pDomLibAnims, AnimationHandler& animHandler);
	static int ReadDomLibrary_Animations(domLibrary_animations* pDomLibAnims, ColladaSkeletalAnimation& animation,
		std::map<std::string, ColladaAnimMatrixSampler*>& allSamplers, std::map<std::string, SimpleArray<ImpFloatMatrix4>*>& allMatrixSources);

	static int ReadDomAnimation_Skeletal(domAnimation* pDomAnim, ColladaSkeletalAnimation& animation,
		std::map<std::string, ColladaAnimMatrixSampler*>& allSamplers, std::map<std::string, SimpleArray<ImpFloatMatrix4>*>& allSources);
	static int ReadDomAnimation_Vertex(domAnimation* pDomAnim, ColladaVertexAnimation& animation,
		std::map<std::string, ColladaAnimFloatSampler*>& allSamplers, std::map<std::string, SimpleArray<float>*>& allSources);

	template <typename SamplerDataType>
	static int ReadDomAnimationSampler(domSampler* pDomSampler, std::map<std::string, SimpleArray<SamplerDataType>*> allMatrixSources,
		ColladaAnimSampler<SamplerDataType>& sampler);

	static daeElement* ResolveDomTarget(daeDatabase* db, const std::string& target, std::string& sidPath, std::string& attribName, int& attribIndex);

private:
	static bool MatchStringIgnoreCase(const char* input, const char* value);
	static bool MatchxsNMToken(xsNMTOKEN token, const char* value);
	static bool MatchxsNCName(xsNCName name, const char* value);

	static bool ReadDomColor(domFx_color_common& color, ColladaColor& cldColor);
	static bool ReadDomColor(const std::string& content, ColladaColor& cldColor);
	static float ReadDomFloat(domCommon_float_or_param_type* pFloat);
	static float ReadDomFloat(daeElement* pElement);

	template <typename DomArrayType, typename ValueType>
	static BOOL ReadDomSourceByIndex(domSource* pDomSource, DWORD index, ValueType* buffer, DWORD buflen);

	template <typename DomArrayType, typename DataType, typename OutType>
	static int ReadDomSourceAllData(SimpleArray<OutType>& result, domSource* pDomSource, UINT outOffsetInBytes = 0, UINT maxReadPerStride = 0);

	static int ReadAllFloats(SimpleArray<float>& result, domSource* pDomSource);
	static int ReadAllFloatVectors(SimpleArray<ImpFloatVector3>& result, domSource* pDomSource);
	static int ReadAllTexCoords(SimpleArray<TXTUV>& result, domSource* pDomSource);
	static int ReadAllNames(SimpleArray<domString>& result, domSource* pDomSource);
	static int ReadAllIDRefs(SimpleArray<daeIDRef>& result, domSource* pDomSource);

	static BOOL ReadFloatVectorByIndex(domSource* pDomSource, DWORD index, ImpFloatVector3* pV);
	static BOOL ReadTexCoordsByIndex(domSource* pDomSource, DWORD index, TXTUV* pTx);

	static ImpFloatVector3* ReadVertexPositions(domVertices* pDomVertices, UINT& vtxCount);
	static ImpFloatVector3* ReadNormalPositions(domVertices* pDomVertices, UINT& vtxCount);
	static ImpFloatVector3* ReadPositions(char const* const type, domVertices* pDomVertices, UINT& vtxCount);

	static BOOL ReadVertexIndices(domListOfUInts& indices, int vtxId, const ColladaInputOffsetArray& cldInputOfsArr, ColladaVertex& vtx, VertexSemantics vs);
	static BOOL ReadVertexDataBySemanticByIndex(daeElement* pElement, enumCIS semantic, DWORD index, int mapChannel, const ImpFloatVector3* positions, UINT posCount, Vertex& vtx);

	static ColladaInputOffsetArrayPtr ParseDomInputArray(const domInputLocalOffset_Array& DomInputLOArr, ColladaSubmesh& submesh);

	static std::string GetAssetNameFromElement(domNode* pDomNode);

	// Check <node> element if there is any geometry attached.
	static bool hasGeometry(domNode* pDomNode, IClassifyAssetNode* classifyFunc);
};

template <typename DomArrayType, typename ValueType>
inline BOOL ColladaUtil::ReadDomSourceByIndex(domSource* pDomSource, DWORD index, ValueType* buffer, DWORD buflen) {
	// <source> ==> <tecnique_common>
	domSource::domTechnique_common* pDomTech = pDomSource->getTechnique_common();
	if (!pDomTech)
		return FALSE;

	// <tecnique_common><accessor>
	domAccessor* pDomAccessor = pDomTech->getAccessor();
	if (!pDomAccessor)
		return FALSE;

	// <accessor:source> ==> <tecnique_common><float_array>
	xsAnyURI& uriArraySource = pDomAccessor->getSource();
	//if (uriArraySource.getState()!=daeURI::uri_success) uriArraySource.resolveElement();	// Resolve URL if not already done
	DomArrayType* pDomValueArr = dynamic_cast<DomArrayType*>(uriArraySource.getElement().cast());
	if (pDomValueArr == NULL)
		return FALSE;

	DWORD stride = (DWORD)pDomAccessor->getStride(); // forced type conversion to avoid warning. loss of data OK

	// <accessor><param>
	domParam_Array& DomParamArr = pDomAccessor->getParam_array();
	if (DomParamArr.getCount() > stride)
		return FALSE; // Must have three or less accessors for a position

	DWORD valIdx = 0;
	for (DWORD arrIdx = 0; arrIdx < DomParamArr.getCount(); arrIdx++) {
		xsNCName paramName = DomParamArr[arrIdx]->getName();
		if (MatchxsNCName(paramName, NULL))
			continue; // Skip unnamed params (see Collada Spec <accessor>)

		buffer[valIdx++] = (float)pDomValueArr->getValue()[index * stride + arrIdx]; // forced type conversion to avoid warning. loss of data OK
		if (valIdx >= buflen)
			break;
	}

	return TRUE;
}

template <typename DomArrayType, typename DataType, typename OutType>
inline int ColladaUtil::ReadDomSourceAllData(SimpleArray<OutType>& result, domSource* pDomSource, UINT outOffsetInBytes /*=0*/, UINT maxReadPerStride /*=0*/) {
	ASSERT(sizeof(DataType) <= sizeof(OutType));

	// <source> ==> <tecnique_common>
	domSource::domTechnique_common* pDomTech = pDomSource->getTechnique_common();
	if (!pDomTech)
		return COLLADA_ERR_BAD_FORMAT;

	// <tecnique_common><accessor>
	domAccessor* pDomAccessor = pDomTech->getAccessor();
	if (!pDomAccessor)
		return COLLADA_ERR_BAD_FORMAT;

	// <accessor:source> ==> <tecnique_common><float_array>
	xsAnyURI& uriArraySource = pDomAccessor->getSource();
	//if (uriArraySource.getState()!=daeURI::uri_success) uriArraySource.resolveElement();	// Resolve URL if not already done
	DomArrayType* pDomValueArr = dynamic_cast<DomArrayType*>(uriArraySource.getElement().cast());
	if (pDomValueArr == NULL)
		return COLLADA_ERR_BAD_FORMAT;

	DWORD valueStride = (DWORD)pDomAccessor->getStride(); // forced type conversion to avoid warning. loss of data OK
	ASSERT(valueStride < 1024); // Error prevention with arbitrary limit

	// <accessor><param>
	domParam_Array& DomParamArr = pDomAccessor->getParam_array();
	if (DomParamArr.getCount() > valueStride)
		return COLLADA_ERR_BAD_FORMAT; // Must have three or less accessors for a position

	// Pre-Parse param list to save time as string comparisons are slow.
	BOOL skipParam[1024]; //@@Limitation: use the same arbitrary limit as above (currently protected by ASSERT)
	UINT paramSize[1024];

	memset(skipParam, 0, sizeof(skipParam));
	memset(paramSize, 0, sizeof(paramSize));

	UINT paramCount = DomParamArr.getCount();
	for (DWORD prmLoop = 0; prmLoop < paramCount; prmLoop++) {
		xsNCName paramName = DomParamArr[prmLoop]->getName();
		skipParam[prmLoop] = MatchxsNCName(paramName, NULL); // Skip unnamed params (see Collada Spec <accessor>)

		xsNMTOKEN paramType = DomParamArr[prmLoop]->getType();
		if (MatchxsNMToken(paramType, "float4x4"))
			paramSize[prmLoop] = 16;
		else
			paramSize[prmLoop] = 1;
	}

	UINT valueCount = (UINT)pDomValueArr->getCount();
	UINT numData = valueCount / valueStride;

	// Use a generic buffer for all data types -- dataStride is measured in bytes
	//unsigned char* buffer = new unsigned char[numData*outStrideInBytes];
	result.resize(numData);

	DWORD valOffset = 0, dataIdx = 0;
	while (dataIdx < numData) {
		DWORD ofs = outOffsetInBytes;
		for (DWORD prmLoop = 0; prmLoop < paramCount && (maxReadPerStride == 0 || prmLoop < maxReadPerStride); prmLoop++) {
			for (UINT unitLoop = 0; unitLoop < paramSize[prmLoop]; unitLoop++) {
				if (skipParam[prmLoop])
					continue; // Skip unnamed params (see Collada Spec <accessor>)

				*(DataType*)&((unsigned char*)&result[dataIdx])[ofs] = (DataType)pDomValueArr->getValue()[valOffset + prmLoop * paramSize[prmLoop] + unitLoop];

				ofs += sizeof(DataType);
				if (ofs >= sizeof(OutType))
					break; // read offset not to exceed stride either
			}

			if (ofs >= sizeof(OutType))
				break; // read offset not to exceed stride either
		}

		dataIdx++;
		valOffset += valueStride;
	}

	return COLLADA_OK;
}

inline bool ColladaUtil::MatchStringIgnoreCase(const char* input, const char* value) {
	if (value == NULL)
		return input == NULL || input[0] == 0;
	if (input == NULL)
		return value == NULL || value[0] == 0;
	return !_stricmp(input, value);
}

inline bool ColladaUtil::MatchxsNMToken(xsNMTOKEN token, const char* value) {
	return MatchStringIgnoreCase(token, value);
}

inline bool ColladaUtil::MatchxsNCName(xsNCName name, const char* value) {
	return MatchStringIgnoreCase(name, value);
}

inline bool ColladaUtil::ReadDomColor(domFx_color_common& color, ColladaColor& cldColor) {
	for (int i = 0; i < 4; i++) cldColor.val[i] = (float)color.get(i);
	return TRUE;
}

inline bool ColladaUtil::ReadDomColor(const std::string& content, ColladaColor& cldColor) {
	std::stringstream ss(content);
	for (int i = 0; i < 4; i++) ss >> cldColor.val[i];
	return TRUE;
}

inline float ColladaUtil::ReadDomFloat(domCommon_float_or_param_type* pFloat) {
	return (float)pFloat->getFloat()->getValue();
}

inline float ColladaUtil::ReadDomFloat(daeElement* pElement) {
	domCommon_float_or_param_type* pFloat = dynamic_cast<domCommon_float_or_param_type*>(pElement);
	if (!pFloat)
		return 0;

	return ReadDomFloat(pFloat);
}

inline int ColladaUtil::ReadAllFloats(SimpleArray<float>& result, domSource* pDomSource) {
	return ReadDomSourceAllData<domFloat_array, float, float>(result, pDomSource);
}

inline int ColladaUtil::ReadAllFloatVectors(SimpleArray<ImpFloatVector3>& result, domSource* pDomSource) {
	return ReadDomSourceAllData<domFloat_array, float, ImpFloatVector3>(result, pDomSource);
}

inline int ColladaUtil::ReadAllTexCoords(SimpleArray<TXTUV>& result, domSource* pDomSource) {
	return ReadDomSourceAllData<domFloat_array, float, TXTUV>(result, pDomSource);
}

inline int ColladaUtil::ReadAllNames(SimpleArray<domString>& result, domSource* pDomSource) {
	return ReadDomSourceAllData<domName_array, domString, domString>(result, pDomSource);
}

inline int ColladaUtil::ReadAllIDRefs(SimpleArray<daeIDRef>& result, domSource* pDomSource) {
	return ReadDomSourceAllData<domIDREF_array, daeIDRef, daeIDRef>(result, pDomSource);
}

inline BOOL ColladaUtil::ReadFloatVectorByIndex(domSource* pDomSource, DWORD index, ImpFloatVector3* pV) {
	pV->clear();
	return ReadDomSourceByIndex<domFloat_array, float>(pDomSource, index, pV->val, 3);
}

inline BOOL ColladaUtil::ReadTexCoordsByIndex(domSource* pDomSource, DWORD index, TXTUV* pTx) {
	if (!pTx)
		return FALSE;
	pTx->tu = pTx->tv = 0;
	return ReadDomSourceByIndex<domFloat_array, float>(pDomSource, index, (float*)pTx, 2);
}

inline std::string ColladaUtil::GetAssetNameFromElement(domNode* pDomNode) {
	std::string assetName;
	if (pDomNode->getName())
		assetName = pDomNode->getName();
	else if (pDomNode->getId())
		assetName = pDomNode->getId(); //@@Watch: joint name retrieval: ID or name?
	return assetName;
}

} // namespace KEP
