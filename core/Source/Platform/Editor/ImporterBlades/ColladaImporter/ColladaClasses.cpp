///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CStructuresMisl.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "CMaterialClass.h"
#include "ColladaAssert.h"
#include "ColladaClasses.h"
#include "IAssetsDatabase.h"
#include "ColladaUtil.h"

using namespace std;

namespace KEP {
BOOL ColladaFxSampler::resolveSurfaceSource(map<string, ColladaFxSurface>& allSurfaces) {
	if (allSurfaces.find(surfaceSource) != allSurfaces.end()) {
		surface = &allSurfaces[surfaceSource];
		return TRUE;
	}

	return FALSE;
}

BOOL ColladaTexture::resolveParamToImageURI(map<string, ColladaFxSampler>& allSamplers) {
	if (allSamplers.find(textureParam) != allSamplers.end()) {
		ColladaFxSampler& sampler = allSamplers[textureParam];
		if (sampler.surface) {
			imageURI = sampler.surface->imageURI;
			return TRUE;
		}
	}

	return FALSE;
}

BOOL ColladaTexture::resolveMapSymbolToTarget(const string& symbol, const string& target) {
	if (uvSetSymbol == symbol) {
		uvSetTarget = target;
		return TRUE;
	}
	return FALSE;
}

BOOL ColladaTexture::resolveMapSymbolToUVIndex(const string& symbol, const int idx) {
	if (uvSetSymbol == symbol) {
		uvIndex = idx;
		return TRUE;
	}
	return FALSE;
}

BOOL ColladaMaterial::hasUnresolvedTextureParams() {
	for (UINT clrLoop = 0; clrLoop < CM_MAP_CHANNELS; clrLoop++) {
		ColladaTexture texture;
		if (!channels[clrLoop].getValue(texture))
			continue;

		if (texture.hasUnresolvedParam())
			return TRUE;
	}

	return FALSE;
}

BOOL ColladaMaterial::hasUnresolvedTextureMapSymbols() {
	for (UINT clrLoop = 0; clrLoop < CM_MAP_CHANNELS; clrLoop++) {
		ColladaTexture texture;
		if (!channels[clrLoop].getValue(texture))
			continue;

		if (texture.hasUnresolvedMapSymbol())
			return TRUE;
	}

	return FALSE;
}

void ColladaMaterial::resolveTextureMapSymbolToTarget(const string& symbol, const string& target) {
	for (UINT clrLoop = 0; clrLoop < CM_MAP_CHANNELS; clrLoop++) {
		if (channels[clrLoop].hasValue(CT_TEXTURE))
			channels[clrLoop].getTexture().resolveMapSymbolToTarget(symbol, target);
	}
}

void ColladaMaterial::resolveTextureMapSymbolToUVIndex(const string& symbol, int idx) {
	for (UINT clrLoop = 0; clrLoop < CM_MAP_CHANNELS; clrLoop++) {
		if (channels[clrLoop].hasValue(CT_TEXTURE))
			channels[clrLoop].getTexture().resolveMapSymbolToUVIndex(symbol, idx);
	}
}

int ColladaMaterial::getNumberOfBindableMaps() {
	int count = 0;
	for (UINT clrLoop = 0; clrLoop < CM_MAP_CHANNELS; clrLoop++) {
		if (channels[clrLoop].hasValue(CT_TEXTURE) && channels[clrLoop].getTexture().hasMap() && !channels[clrLoop].getTexture().getMapTarget().empty())
			count++;
	}

	return count;
}

void ColladaMaterial::resolveTextureParamToImageURI() {
	for (int i = 0; i < CM_MAP_CHANNELS; i++)
		if (channels[i].hasValue(CT_TEXTURE)) // Has texture
			channels[i].getTexture().resolveParamToImageURI(allSamplers);
}
/*
void ColladaSubmesh::setMapChannelData(UINT chan, UINT vertexID, const TXTUV* uv)
{
	jsAssert(chan<MAX_UV_CHANNELS && vertexID<vertices.size() && uv!=NULL);
	if (chan>=MAX_UV_CHANNELS || vertexID>=vertices.size() || uv==NULL) return;

	Vertex& v = vertices[vertexID];
	TXTUV* pTx = GetKEPTexCoordPtrByChannelID(&v, chan);
	jsAssert(pTx!=NULL);

	if (pTx) 
	{
		pTx->tu = uv->tu;
		pTx->tv = uv->tv;
	}
}
*/
BOOL ColladaSubmesh::resolveMaterial(const string& matName, ColladaMaterialPtr mat) {
	if (materialName != matName)
		return FALSE; // no match

	// Check if material itself still has unbound parameter
	BOOL bUnresolved = FALSE;

	if (mat) {
		// check for unbound parameter
		if (mat->hasUnresolvedTextureParams())
			bUnresolved = TRUE;
		else if (mat->hasUnresolvedTextureMapSymbols())
			bUnresolved = TRUE;
		/*		else {
			// map channels are resolvable, check if we have enough free map channels available in current submesh
			int mapChannelsNeeded = mat->getNumberOfBindableMaps();

			if (mapChannelsNeeded>0)
			{
				bUnresolved = TRUE;

				int currMapChannels = 0;
				if (pInputOffsetArr) currMapChannels = pInputOffsetArr->GetMaxUVIndex();

				if (currMapChannels+mapChannelsNeeded<=CM_TOTAL_CHANNELS)
				{
					// Yes we do have enough free channels
					bUnresolved = FALSE;	// resolvable
				}
			}
		}*/
	}

	if (bUnresolved) {
		//Todo: warning: unresolved material
		//return FALSE;		//Set material even if it's not fully resolved as it's the legacy behavior of exporter plugin [Yanfeng 10/2008]
	}

	setMaterial(mat);
	return !bUnresolved;
}

//! Collapse the mesh to static version. Return NULL if failed.
const ColladaSubmesh* ColladaRigidMesh::collapseToStaticMesh() const {
	//@@Todo
	return new ColladaSubmesh(*this);
}

//! Collapse the mesh to static version. Return NULL if failed.
const ColladaSubmesh* ColladaSkinnedMesh::collapseToStaticMesh() const {
	//@@Todo
	return new ColladaSubmesh(*this);
}

} // namespace KEP
