///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IAssetsDatabase.h"

using namespace tcl; // Tree Container Library

namespace KEP {

class IAssetImporterImpl;
class ColladaJoint;
class ColladaSkeleton;
class CSkeletonAnimation;

class ColladaImporter : public IAssetImporter {
private:
	IAssetImporterImpl* pBaseImpl;
	std::map<std::string, IAssetImporterImpl*> allSessions;

protected:
	virtual BOOL SupportsColladaVersion(IAssetImporterImpl* pImpl, float ver);
	virtual float GetColladaDocumentVersion(IAssetImporterImpl* pImpl, const std::string& aURI);
	IAssetImporterImpl* GetBaseImpl(const std::string& aURI);
	IAssetImporterImpl* GetSessionImpl(const std::string& aURI);
	BOOL BuildAssetTree(IAssetImporterImpl* pImpl, const std::vector<std::string>& meshNames, const std::string& rootUri, KEP_ASSET_TYPE assetType, AssetTree& assetTree);
	BOOL BuildBoneTree(IAssetImporterImpl* pImpl, ColladaSkeleton& ske, const ColladaJoint* pJoint, const std::string& rootUri, AssetTree& assetTree);

	bool DoImportRigidMeshAsset(CSkeletonObject& aSkeletalObject, AssetData& asset, int aFlags);
	bool DoImportSkinnedMeshAsset(CSkeletonObject& aSkeletalObject, const AssetTree* node, AssetData& asset, CAlterUnitDBObject*& currSlot, CDeformableMesh*& currConfig, CDeformableVisList*& currLodList, int aFlags);

public:
	ColladaImporter(IAssetImporterImpl* impl = NULL) :
			IAssetImporter("ColladaImporter", "COLLADA Universal Importer (*.DAE)"), pBaseImpl(impl) {}

	//! Registers the importer with the aAssetDatabase and does any initialization required
	virtual void Register();

	//! Unregisters the importer, doing any necessary cleanup
	virtual void Unregister();

	AssetTree* PreviewAssets(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags, IClassifyAssetNode* classifyFunc) override;
	BOOL SupportAssetURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType) override;
	BOOL IsURICompatible() override { return true; }
	BOOL VerticesAlwaysBaked(KEP_ASSET_TYPE aAssetType) override { return FALSE; }

	/** @name Import by object type.
	  * These functions try to import the specified file into the object passed
	  *   returning true upon success and false otherwise.
	  */
	//@{
	bool Import(CEXMeshObj& aStaticMesh, const std::string& URI, AssetData* aDef = NULL, int aFlags = IMP_STATIC_LEGACY_FLAGS) override;
	bool Import(CSkeletonObject& aSkeletalObject, const std::string& URI, AssetTree* aTree = NULL, int aFlags = IMP_SKELETAL_LEGACY_FLAGS) override;
	bool Import(CSkeletonObject& aSkeletalObject, const kstringVector& aURIVector, AssetTree* aTree = NULL, int aFlags = IMP_SKELETAL_LEGACY_FLAGS) override;
	//@}

	/** @name Additional skeletal animation import functions.
	  * These functions try to import skeletal animation data of different types
	  *  to an existing skeletal animation object which is already initialized
	  * See IAssetsDatabase documentation for more details.
	  */
	//@{
	bool ImportSkinnedMesh(CSkeletonObject& aSkeletalObject, const std::string& aURI, int aSectionIndex, int aConfigIndex, int aLodIndex, int aFlags, AssetData* aDef = NULL) override;
	bool ImportRigidMesh(RuntimeSkeleton& aSkeletalObject, const std::string& aURI, int aBoneIndex, int aLodIndex, int aFlags, AssetData* aDef = NULL) override;
	bool ImportAnimation(CSkeletonObject& aSkeletalObject, const std::string& aURI, int& aAnimGLID, int aFlags, AssetData* aDef = NULL, const AnimImportOptions* apOptions = NULL) override;
	//@}

	//! Newly added members (to be merged to IAssetImporter interface)
	bool ImportSkeleton(CSkeletonObject& aSkeletalObject, AssetTree* aTree, int aFlags);
	bool ImportSkeletalMeshes(CSkeletonObject& aSkeletalObject, AssetTree* aTree, int aFlags);
	bool ImportSkeletalAnimations(CSkeletonObject& aSkeletalObject, std::vector<int> allAnimGLIDs, AssetTree* aTree, int aFlags);

	bool ImportVertexAnimation(CEXMeshObj& aMesh, CVertexAnimationModule& aAnim, const std::string& aURI, AssetData* aDef = NULL) override;

	bool BeginSession(const std::string& aURI, int aFlags) override;
	bool EndSession(const std::string& aURI) override;

	void GetSupportedOptions(std::set<std::string>& optionNames) override;
	bool SetOption(const std::string& optionName, int intValue) override;
	bool GetOption(const std::string& optionName, int& intValue) override;

}; // ColladaImporter

}; // namespace KEP
