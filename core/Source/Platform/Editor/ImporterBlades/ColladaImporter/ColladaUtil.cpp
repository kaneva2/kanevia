///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ColladaAssert.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "CEXMeshClass.h"
#include "ColladaClasses.h"
#include "IAssetsDatabase.h"
#include "ColladaUtil.h"
#include "AssetConverter.h"
#include "AssetTree.h"

using namespace std;

namespace KEP {

//@@Note: double precision is not necessary in Editor as Direct3D set all math instructions to single precision only.

int ColladaUtil::PreviewDomNode(enumUPAXIS parentUpAxis, const string& parentName, domNode* pDomNode,
	const ImpFloatMatrix4& parentMatrix, const string& parentJoint,
	NodeHandler* handleNode, JointHandler* handleJoint, IClassifyAssetNode* classifyFunc) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomNode);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	string assetName = GetAssetNameFromElement(pDomNode);

	// If current node is filtered, treat everything inside as child of its parent
	bool isDummy = false;
	if (classifyFunc && classifyFunc->isDummy(assetName)) {
		assetName = parentName;
		isDummy = true;
	}

	//const ColladaJoint* pCurrJoint = NULL;

	UINT numSubmeshes = 0;
	KEP_ASSET_TYPE meshType = (KEP_ASSET_TYPE)-1;
	string currJoint = parentJoint;

	//1. Read all transformation and build currMatrix.
	//Left multiply: currMatrix *= parentMatrix. Parent matrix is further from the vector so goes to the right.
	ImpFloatMatrix4 currMatrix = ReadDomNodeTransformation(pDomNode);
	currMatrix *= parentMatrix;

	bool hasGeom = hasGeometry(pDomNode, classifyFunc);
	int typeHint = KEP_UNKNOWN_TYPE;
	if (!isDummy) {
		if (pDomNode->getType() == NODETYPE_JOINT)
			typeHint = KEP_SKA_SKELETAL_OBJECT;
		else if (hasGeom) {
			if (!parentJoint.empty())
				typeHint = KEP_SKA_RIGID_MESH;
			else
				typeHint = KEP_STATIC_MESH;
		}
	}

	if (!isDummy && (classifyFunc && (*classifyFunc)(assetName, typeHint, hasGeom) == KEP_SKA_SKELETAL_OBJECT || !classifyFunc && typeHint == KEP_SKA_SKELETAL_OBJECT)) {
		//2. Joint found
		string sid;
		if (pDomNode->getSid())
			sid = pDomNode->getSid(); // Some JOINT nodes do not have SID (but they are not referenced by others either).
		if (handleJoint)
			(*handleJoint)(assetName, parentJoint, sid, currMatrix, upAxis);
		//2.3.	currMatrix = identity.
		currMatrix.Identity();
		currJoint = assetName;
	} else {
		//3.1. Either dummy or non-joint node
	}

	//4. For all children elements
	daeTArray<daeSmartRef<daeElement>> allChildren = pDomNode->getChildren();
	for (UINT i = 0; i < allChildren.getCount(); i++) {
		daeElement* pElem = allChildren[i];
		domNode* pDomChildNode = NULL;

		daeString elementType = pElem->getTypeName();

		//4.1.	If <instance_controller>:
		if (numSubmeshes == 0 && MatchStringIgnoreCase(elementType, "instance_controller")) {
			//@@Limitation: only support one instance_controller under each node
			//@@Limitation: mix of instance_controller and instance_geometry is not supported
			ASSERT(meshType == (KEP_ASSET_TYPE)-1 || meshType == KEP_VERTEX_ANIMATION || meshType == KEP_SKA_SKINNED_MESH);

			//submeshCount = CountSubMeshes(<instance_controller>).
			//submeshType = SKINNED

			domInstance_controller* pDomInstCntlr = dynamic_cast<domInstance_controller*>(pElem);
			if (pDomInstCntlr) {
				BOOL isMorph = FALSE;
				numSubmeshes = ReadDomSubMeshCount(pDomInstCntlr, isMorph);
				if (numSubmeshes > 0)
					meshType = isMorph ? KEP_VERTEX_ANIMATION : KEP_SKA_SKINNED_MESH;
			}

			//Next.
			continue;
		}
		//4.2.	If <instance_geometry>:
		else if (MatchStringIgnoreCase(elementType, "instance_geometry")) {
			//@@Limitation: mix of instance_controller and instance_geometry is not supported
			ASSERT(meshType == (KEP_ASSET_TYPE)-1 || meshType == KEP_STATIC_MESH || meshType == KEP_SKA_RIGID_MESH);

			//submeshCount = CountSubMeshes(<instance_geometry>)
			//subMeshType = RIGID or STATIC [depending on currJoint]
			domInstance_geometry* pDomInstGeom = dynamic_cast<domInstance_geometry*>(pElem);
			if (pDomInstGeom) {
				numSubmeshes += ReadDomSubMeshCount(pDomInstGeom);
				if (numSubmeshes > 0)
					meshType = currJoint.empty() ? KEP_STATIC_MESH : KEP_SKA_RIGID_MESH;
			}

			//Next.
			continue;
		}
		//4.3.	If <instance_node>:
		else if (MatchStringIgnoreCase(elementType, "instance_node")) {
			//resolve <node> => child-<node>,
			domInstance_node* pDomInstNode = dynamic_cast<domInstance_node*>(pElem);
			if (pDomInstNode) {
				const xsAnyURI& uri = pDomInstNode->getUrl();
				pDomChildNode = dynamic_cast<domNode*>(uri.getElement().cast());
			}
		}
		//4.4.	If <node>:
		else if (MatchStringIgnoreCase(elementType, "node")) {
			// 	<node> => child-<node>
			pDomChildNode = dynamic_cast<domNode*>(pElem);
		}

		//4.5.	If nodeFound
		if (pDomChildNode != NULL) {
			int ret = PreviewDomNode(upAxis, assetName, pDomChildNode, currMatrix, currJoint, handleNode, handleJoint, classifyFunc);
			if (ret < 0)
				return ret;
		}
	}

	//5. If geomFound:
	if (numSubmeshes > 0 && meshType != (KEP_ASSET_TYPE)-1) {
		if (handleNode)
			(*handleNode)(meshType, assetName, numSubmeshes, pDomNode, currMatrix, parentMatrix, currJoint, upAxis, parentUpAxis);
		return COLLADA_OK;
	}

	return COLLADA_OK;
}

UINT ColladaUtil::ReadDomSubMeshCount(domMesh* pDomMesh) {
	// Continue to parse sub-meshes
	//@@Limitation: <polygons> is not supported [Yanfeng 09/2008]
	//@@Todo: <polygons> support
	UINT totalSubmeshes = 0;
	totalSubmeshes += (UINT)pDomMesh->getTriangles_array().getCount();
	totalSubmeshes += (UINT)pDomMesh->getTrifans_array().getCount();
	totalSubmeshes += (UINT)pDomMesh->getTristrips_array().getCount();
	totalSubmeshes += (UINT)pDomMesh->getPolylist_array().getCount();

	return totalSubmeshes;
}

UINT ColladaUtil::ReadDomSubMeshCount(domInstance_geometry* pDomInstGeom) {
	// Validate current node
	xsAnyURI& uri = pDomInstGeom->getUrl();
	domGeometry* pDomGeom = dynamic_cast<domGeometry*>(uri.getElement().cast());
	if (pDomGeom == NULL)
		return 0; // skip this node if no geometry available

	domMesh* pDomMesh = dynamic_cast<domMesh*>(pDomGeom->getMesh().cast());
	if (pDomMesh == NULL)
		return 0; // skip this node if no mesh available

	return ReadDomSubMeshCount(pDomMesh);
}

UINT ColladaUtil::ReadDomSubMeshCount(domInstance_controller* pDomInstCntlr, BOOL& isMorph) {
	xsAnyURI uriCntlr = pDomInstCntlr->getUrl();
	domController* pDomCntlr = dynamic_cast<domController*>(uriCntlr.getElement().cast());
	if (pDomCntlr == NULL) {
		//@@Todo: warning ill-formed collada document
		return 0;
	}

	xsAnyURI* pUriSource = NULL;
	if (pDomCntlr->getSkin() != NULL) {
		pUriSource = &pDomCntlr->getSkin()->getSource();
		isMorph = FALSE;
	} else if (pDomCntlr->getMorph() != NULL) {
		pUriSource = &pDomCntlr->getMorph()->getSource();
		isMorph = TRUE;
	}

	if (pUriSource == NULL) {
		//@@Todo: warning ill-formed collada document
		return 0;
	}

	domGeometry* pDomGeom = dynamic_cast<domGeometry*>(pUriSource->getElement().cast());
	if (pDomGeom == NULL) {
		//@@Todo: warning ill-formed collada document or unsupported data
		return 0;
	}

	domMesh* pDomMesh = pDomGeom->getMesh();
	if (pDomMesh == NULL) {
		//@@Todo: warning ill-formed collada document
		return 0;
	}

	return ReadDomSubMeshCount(pDomMesh);
}

//! Read data from <node> with nested <instance_geometry>
//! Return 1 if succeeded, 0 if not found, <0 for error codes
int ColladaUtil::ReadDomNode_Geoms(enumUPAXIS parentUpAxis, domNode* pDomNode, int subID, ColladaSubmesh& submesh, ImpFloatMatrix4& matrix) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomNode);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	// <instance_geometry>
	domInstance_geometry_Array& pDomInstGeomArr = pDomNode->getInstance_geometry_array();
	if (pDomInstGeomArr.getCount() == 0) { // No geometries at current node

		BOOL meshFound = FALSE;

		// Check child nodes
		domNode_Array& childNodes = pDomNode->getNode_array();
		for (DWORD i = 0; i < childNodes.getCount(); i++) {
			//@@Limitation: only retrieve first <instance_geometry> from children: possibly it's mean to be.
			int ret = ReadDomNode_Geoms(upAxis, childNodes[i], subID, submesh, matrix);
			if (ret < 0)
				return ret; // failed: pass error code to caller
			if (ret == 0)
				continue; // not found: continue searching until a geometry is found

			// succeeded: stop searching
			meshFound = TRUE;
			break; // stop if geometry found
		}

		if (!meshFound) {
			// Check <instance_node>'s
			domInstance_node_Array& childInstNodes = pDomNode->getInstance_node_array();
			for (DWORD i = 0; i < childInstNodes.getCount(); i++) {
				domInstance_node* pDomInstNode = childInstNodes[i];
				if (!pDomInstNode)
					continue;

				const xsAnyURI& uri = pDomInstNode->getUrl();
				domNode* pChildNode = dynamic_cast<domNode*>(uri.getElement().cast());
				if (!pChildNode)
					continue;

				int ret = ReadDomNode_Geoms(upAxis, pChildNode, subID, submesh, matrix);
				if (ret < 0)
					return ret; // failed: pass error code to caller
				if (ret == 0)
					continue; // not found: continue searching until a geometry is found

				// succeeded: stop searching
				meshFound = TRUE;
				break; // stop if geometry found
			}
		}

		if (!meshFound)
			return 0;
	} else {
		bool found = false;
		UINT relSubmeshId = subID;

		for (UINT i = 0; i < pDomInstGeomArr.getCount(); i++) {
			domInstance_geometry* pDomInstGeom = pDomInstGeomArr[i];

			// Count meshes inside current <instance_geometry>
			UINT currGeomSubmeshCount = ReadDomSubMeshCount(pDomInstGeom);

			// Check if submesh requested belong to current <instance_geometry>
			if (relSubmeshId < currGeomSubmeshCount) {
				int ret = ReadDomInstance_Geometry(upAxis, pDomInstGeom, relSubmeshId, submesh);
				if (ret <= 0)
					return ret; // failed, pass error code to caller

				// Loaded submesh successfully - break loop
				found = true;
				break;
			}

			// Check next <instance_geometry>
			relSubmeshId -= currGeomSubmeshCount;
		}

		if (!found) {
			// Invalid submesh ID
			return COLLADA_NOTFOUND;
		}
	}

	ImpFloatMatrix4 currMatrix = ReadDomNodeTransformation(pDomNode);
	matrix *= currMatrix;
	return 1;
}

//! <lookat> | <matrix> | <rotate> | <scale> | <skew> | <translate>
ImpFloatMatrix4 ColladaUtil::ReadDomNodeTransformation(domNode* pDomNode) {
	ImpFloatMatrix4 matrix; // default is identity matrix

	// <lookat> | <matrix> | <rotate> | <scale> | <skew> | <translate>
	//@@Limitation: TBD
	daeTArray<daeSmartRef<daeElement>> children = pDomNode->getChildren();

	for (DWORD childLoop = 0; childLoop < children.getCount(); childLoop++) {
		daeElement* pChild = children[childLoop];
		if (!pChild)
			continue;

		ImpFloatMatrix4 currMatrix;

		daeString elementType = pChild->getTypeName();
		if (MatchStringIgnoreCase(elementType, "lookat")) {
			domLookat* pDomLookat = dynamic_cast<domLookat*>(pChild);
			if (!ReadDomLookat(pDomLookat, currMatrix))
				continue;
		} else if (MatchStringIgnoreCase(elementType, "matrix")) {
			domMatrix* pDomMatrix = dynamic_cast<domMatrix*>(pChild);
			ASSERT(pDomMatrix != NULL);

			domFloat4x4& floatArr = pDomMatrix->getValue();
			if (!ReadDomMatrix(floatArr, currMatrix))
				continue;
		} else if (MatchStringIgnoreCase(elementType, "rotate")) {
			domRotate* pDomRotate = dynamic_cast<domRotate*>(pChild);
			if (!ReadDomRotate(pDomRotate, currMatrix))
				continue;
		} else if (MatchStringIgnoreCase(elementType, "scale")) {
			domScale* pDomScale = dynamic_cast<domScale*>(pChild);
			if (!ReadDomScale(pDomScale, currMatrix))
				continue;
		} else if (MatchStringIgnoreCase(elementType, "skew")) {
			domSkew* pDomSkew = dynamic_cast<domSkew*>(pChild);
			if (!ReadDomSkew(pDomSkew, currMatrix))
				continue;
		} else if (MatchStringIgnoreCase(elementType, "translate")) {
			domTranslate* pDomTranslate = dynamic_cast<domTranslate*>(pChild);
			if (!ReadDomTranslate(pDomTranslate, currMatrix))
				continue;
		}

		matrix = currMatrix * matrix; // Lefty-multiplication: Vector = Vector * Matrix, Matrix-Composed = Matrix-Closest-To-Geometry * ... * Matrix-Furthest-From-Geometry
	}

	return matrix;
}

//! Read data from <instance_geometry>
//! Return 1 if succeeded, 0 if not found, <0 for error codes
int ColladaUtil::ReadDomInstance_Geometry(enumUPAXIS parentUpAxis, domInstance_geometry* pDomInstGeom, UINT submeshID, ColladaSubmesh& submesh) {
	// Update UpAxis from current <instance_geometry>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomInstGeom);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	// <instance_geometry url="...">
	daeURI& UrlGeom = pDomInstGeom->getUrl();
	//if (UrlGeom.getState()!=daeURI::uri_success) UrlGeom.resolveElement();	// Resolve URL if not already done

	// <instance_geometry url="..."> ==> <library_geometries><geometry>
	domGeometry* pDomGeom = dynamic_cast<domGeometry*>(UrlGeom.getElement().cast());
	if (!pDomGeom)
		return COLLADA_ERR_BAD_FORMAT; // Bad format

	// Read <geometry> element
	int ret = ReadDomGeometry(upAxis, pDomGeom, submeshID, submesh);
	if (ret <= 0)
		return ret;

	// Read <bind_material> element
	domBind_material* pDomBindMat = pDomInstGeom->getBind_material();
	if (pDomBindMat)
		ReadDomBindMaterial(pDomBindMat, submesh); // @@Watch: Ignore error

	return COLLADA_OK;
}

//! Read mesh data from <geometry>
//! Return 1 if succeeded, 0 if not found, <0 for error codes
int ColladaUtil::ReadDomGeometry(enumUPAXIS parentUpAxis, domGeometry* pDomGeom, UINT submeshID, ColladaSubmesh& submesh) {
	// <library_geometries><geometry><mesh>
	domMesh* pDomMesh = pDomGeom->getMesh();
	if (!pDomMesh)
		return COLLADA_ERR_BAD_FORMAT; // Bad format

	// Update UpAxis from current <mesh> and then <geometry>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomMesh);
	if (upAxis == CSM_UNK_UP)
		upAxis = ReadDomUpAxis(pDomGeom);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	// <library_geometries><geometry><mesh><vertices>
	domVertices* pDomVertices = pDomMesh->getVertices();
	if (!pDomVertices)
		return COLLADA_ERR_BAD_FORMAT; // Bad format

	// 1. Read vertices positions
	UINT posCount = 0;
	ImpFloatVector3* positions = ReadVertexPositions(pDomVertices, posCount);
	submesh.setPositionBuffer(posCount, positions);
	ImpFloatVector3* normals = ReadNormalPositions(pDomVertices, posCount);
	submesh.setNormalBuffer(posCount, normals);

	// 2. Read mesh: support triangles/trifans/tristrips/polylists for now
	// @@Limitation: polygons must be coplanar and convex. [Yanfeng 09/2008]
	// @@Limitation: <polygons> not supported [Yanfeng 09/2008]
	// @@Todo: <polygons> support [Yanfeng 09/2008]
	// @@Todo: concave polygons [Yanfeng 09/2008]
	// @@Todo: non-coplanar polygons: possibly won't support it anyway

	// <library_geometries><geometry><mesh>

	// At this point I have a position and/or normal array.  These are part of
	// a vertex.  When I process a triangle and process CIS_VERTEX, these are
	// the indices that need to be set.
	VertexSemantics vs = 0;
	if (positions != 0) {
		vs |= VERTICES_POSITION;
	}
	if (normals != 0) {
		vs |= VERTICES_NORMAL;
	}
	// TEXCOORD not used yet

	// 2.a <triangles> (0 or more)
	domTriangles_Array& allTrianglesArr = pDomMesh->getTriangles_array();
	if (submeshID < allTrianglesArr.getCount())
		return ReadDomTriangles(upAxis, allTrianglesArr[submeshID], vs, submesh);
	else
		submeshID -= allTrianglesArr.getCount();

	// 2.b <trifans> (0 or more)
	domTrifans_Array& allTrifansArr = pDomMesh->getTrifans_array();
	if (submeshID < allTrifansArr.getCount())
		return ReadDomTrifans(upAxis, allTrifansArr[submeshID], vs, submesh);
	else
		submeshID -= allTrifansArr.getCount();

	// 2.c <tristrips> (0 or more)
	domTristrips_Array& allTristripsArr = pDomMesh->getTristrips_array();
	if (submeshID < allTristripsArr.getCount())
		return ReadDomTristrips(upAxis, allTristripsArr[submeshID], vs, submesh);
	else
		submeshID -= allTristripsArr.getCount();

	// 2.d <polylists> (0 or more)
	domPolylist_Array& allPolylistsArr = pDomMesh->getPolylist_array();
	if (submeshID < allPolylistsArr.getCount())
		return ReadDomPolyList(upAxis, allPolylistsArr[submeshID], vs, submesh);
	else
		submeshID -= allPolylistsArr.getCount();

	// 2.e <polygons> (0 or more)
	/*domPolygons_Array& allPolygonsArr = pDomMesh->getPolygons_array();
	if (submeshID < allPolygonsArr.getCount())
		return ReadDomPolygons(upAxis, allPolygonsArr[submeshID], positions, posCount, submesh);
	else
		submeshID -= allPolygonsArr.getCount();*/

	return FALSE;
}

//! Load vertices and faces from a <triangles> element.
//! Return 1 if succeeded, 0 if not found, <0 for error codes
int ColladaUtil::ReadDomTriangles(enumUPAXIS parentUpAxis, domTriangles* pDomTris, VertexSemantics vs, ColladaSubmesh& submesh) {
	// Update UpAxis from current <triangles>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomTris);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;
	submesh.setUpAxis(upAxis);

	xsNCName matName = pDomTris->getMaterial();
	if (matName) {
		submesh.setMaterialName(string(matName));
	}

	DWORD triCount = (DWORD)pDomTris->getCount();
	if (triCount == 0)
		return COLLADA_OK; // no vertices, just return as succeeded

	domInputLocalOffset_Array& DomInputLOArr = pDomTris->getInput_array();
	if (DomInputLOArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: at least one <input> is required

	// Parse input array for once so we don't have to do it at per-vertex level
	auto pInputOffsetArr = ParseDomInputArray(DomInputLOArr, submesh);
	submesh.setInputOffsetArray(pInputOffsetArr);
	if (!pInputOffsetArr || pInputOffsetArr->GetStride() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: stride must be >0

	// <p>
	domP* pDomP = pDomTris->getP();
	if (!pDomP)
		return COLLADA_ERR_BAD_FORMAT; // bad format: <p> is required

	domListOfUInts& indices = pDomP->getValue();

	BOOL errorFound = FALSE;

	//submesh.createVerticesBuffer(3*triCount);
	submesh.setVertexBuffer(3 * triCount);

	UINT globalVtxId = 0;
	for (UINT triIdx = 0; triIdx < triCount; triIdx++) // For each triangle
	{
		for (int vtxIdx = 0; vtxIdx < 3; vtxIdx++, globalVtxId++) // Vertex 0/1/2 for current triangle
		{
			ColladaVertex vtx;
			if (!ReadVertexIndices(indices, globalVtxId, *pInputOffsetArr, vtx, vs))
				errorFound = TRUE;

			submesh.setVertex(triIdx * 3 + vtxIdx, vtx);
		}
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

//! Load vertices and faces from a <trifans> element.
//! Return 1 if succeeded, 0 if not found, <0 for error codes
int ColladaUtil::ReadDomTrifans(enumUPAXIS parentUpAxis, domTrifans* pDomTfans, VertexSemantics vs, ColladaSubmesh& submesh) {
	// Update UpAxis from current <trifans>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomTfans);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;
	submesh.setUpAxis(upAxis);

	xsNCName matName = pDomTfans->getMaterial();
	if (matName) {
		submesh.setMaterialName(string(matName));
	}

	DWORD fansCount = (DWORD)pDomTfans->getCount();
	if (fansCount == 0)
		return COLLADA_OK; // no vertices - return as succeeded

	domInputLocalOffset_Array& DomInputLOArr = pDomTfans->getInput_array();
	if (DomInputLOArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: at least on <input> is required

	// Parse input array for once so we don't have to do it at per-vertex level
	auto pInputOffsetArr = ParseDomInputArray(DomInputLOArr, submesh);
	submesh.setInputOffsetArray(pInputOffsetArr);
	if (!pInputOffsetArr || pInputOffsetArr->GetStride() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: stride must be >0

	// <p> (multiple)
	domP_Array& DomPArr = pDomTfans->getP_array();
	if (DomPArr.getCount() == 0)
		return COLLADA_OK; // no indices, no faces

	ASSERT(DomPArr.getCount() == fansCount); // number of <p>'s must match trifan count

	BOOL errorFound = FALSE;

	// Counting triangles
	DWORD triCount = 0; //total triangles
	for (UINT fanLoop = 0; fanLoop < fansCount; fanLoop++)
		triCount += std::max((int)DomPArr[fanLoop]->getValue().getCount() - 2, 0);
	submesh.setVertexBuffer(triCount * 3);

	UINT globalTriId = 0;
	for (UINT fanLoop = 0; fanLoop < fansCount; fanLoop++) {
		// <p>
		domP* pDomP = DomPArr[fanLoop];
		domListOfUInts& indices = pDomP->getValue();

		int numTrianglesInFan = indices.getCount() - 2;
		ASSERT(numTrianglesInFan > 0); //@@Temporarily
		if (numTrianglesInFan < 0)
			continue; // Skip if invalid tri-fan list

		ColladaVertex v0, v1;

		for (DWORD pIdx = 0; pIdx < indices.getCount(); pIdx++) // For each index
		{
			ColladaVertex vtx;
			if (!ReadVertexIndices(indices, pIdx, *pInputOffsetArr, vtx, vs))
				errorFound = TRUE;

			if (pIdx == 0)
				v0 = vtx;
			else if (pIdx == 1)
				v1 = vtx;
			else {
				// Add triangle
				ASSERT(globalTriId + 1 <= triCount);
				submesh.setVertex(globalTriId * 3, v0);
				submesh.setVertex(globalTriId * 3 + 1, v1);
				submesh.setVertex(globalTriId * 3 + 2, vtx);
				globalTriId++;

				v1 = vtx; // Shift vertex
			}
		}
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

//! Load vertices and faces from a <tristrips> element.
//! Return 1 if succeeded, 0 if not found, <0 for error codes
int ColladaUtil::ReadDomTristrips(enumUPAXIS parentUpAxis, domTristrips* pDomTstrips, VertexSemantics vs, ColladaSubmesh& submesh) {
	// Update UpAxis from current <tristrips>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomTstrips);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;
	submesh.setUpAxis(upAxis);

	xsNCName matName = pDomTstrips->getMaterial();
	if (matName) {
		submesh.setMaterialName(string(matName));
	}

	DWORD stripsCount = (DWORD)pDomTstrips->getCount();
	if (stripsCount == 0)
		return COLLADA_OK; // no vertices - return as succeeded

	domInputLocalOffset_Array& DomInputLOArr = pDomTstrips->getInput_array();
	if (DomInputLOArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: at least one <input> is required

	// Parse input array for once so we don't have to do it at per-vertex level
	auto pInputOffsetArr = ParseDomInputArray(DomInputLOArr, submesh);
	submesh.setInputOffsetArray(pInputOffsetArr);
	if (!pInputOffsetArr || pInputOffsetArr->GetStride() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: stride must be >0

	// <p> (multiple)
	domP_Array& DomPArr = pDomTstrips->getP_array();
	if (DomPArr.getCount() == 0)
		return COLLADA_OK; // no indices, no faces

	ASSERT(DomPArr.getCount() == stripsCount); // number of <p>'s must match trifan count

	BOOL errorFound = FALSE;

	// Counting triangles
	DWORD triCount = 0; //total triangles
	for (UINT strLoop = 0; strLoop < stripsCount; strLoop++)
		triCount += std::max((int)DomPArr[strLoop]->getValue().getCount() - 2, 0);
	submesh.setVertexBuffer(triCount * 3);

	UINT globalTriId = 0;
	for (UINT strLoop = 0; strLoop < DomPArr.getCount(); strLoop++) {
		domP* pDomP = DomPArr[strLoop];
		domListOfUInts& indices = pDomP->getValue();

		int numTrianglesInFan = indices.getCount() - 2;
		ASSERT(numTrianglesInFan > 0); //@@Temporarily
		if (numTrianglesInFan < 0)
			continue; // Skip if invalid tri-fan list

		ColladaVertex v0, v1;

		for (DWORD pIdx = 0; pIdx < indices.getCount(); pIdx++) // For each index
		{
			ColladaVertex vtx;
			if (!ReadVertexIndices(indices, pIdx, *pInputOffsetArr, vtx, vs))
				errorFound = TRUE;

			if (pIdx == 0)
				v0 = vtx;
			else if (pIdx == 1)
				v1 = vtx;
			else {
				// Add triangle
				ASSERT(globalTriId + 1 <= triCount);
				if (pIdx % 2) // flip vertex order for odd ones (1, 3, 5...)
				{
					submesh.setVertex(globalTriId * 3, vtx);
					submesh.setVertex(globalTriId * 3 + 1, v1);
					submesh.setVertex(globalTriId * 3 + 2, v0);
				} else { // keep vertex order for even ones (0, 2, 4, ...)
					submesh.setVertex(globalTriId * 3, v0);
					submesh.setVertex(globalTriId * 3 + 1, v1);
					submesh.setVertex(globalTriId * 3 + 2, vtx);
				}
				globalTriId++;

				v0 = v1;
				v1 = vtx; // Shift vertex
			}
		}
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

//! Load vertices and faces from a <polygons> element.
//! Return 1 if succeeded, 0 if not found, <0 for error codes
//! @@Limitation: polygons must be coplanar and convex. [Yanfeng 09/2008]
//! @@Todo: concave polygons [Yanfeng 09/2008]
//! @@Todo: non-coplanar polygons: possibly won't support it anyway
int ColladaUtil::ReadDomPolyList(enumUPAXIS parentUpAxis, domPolylist* pDomPolyList, VertexSemantics vs, ColladaSubmesh& submesh) {
	// Update UpAxis from current <tristrips>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomPolyList);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;
	submesh.setUpAxis(upAxis);

	xsNCName matName = pDomPolyList->getMaterial();
	if (matName) {
		submesh.setMaterialName(string(matName));
	}

	DWORD polyCount = (DWORD)pDomPolyList->getCount();
	if (polyCount == 0)
		return COLLADA_OK; // no vertices: return as succeeded

	domInputLocalOffset_Array& DomInputLOArr = pDomPolyList->getInput_array();
	if (DomInputLOArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: at least one <input> is required

	// Parse input array for once so we don't have to do it at per-vertex level
	auto pInputOffsetArr = ParseDomInputArray(DomInputLOArr, submesh);
	submesh.setInputOffsetArray(pInputOffsetArr);
	if (!pInputOffsetArr || pInputOffsetArr->GetStride() == 0)
		return COLLADA_ERR_BAD_FORMAT; // bad format: stride must be >0

	// <p>
	domP* pDomP = pDomPolyList->getP();
	if (!pDomP)
		return COLLADA_ERR_BAD_FORMAT; // bad format: <p> is required

	domPolylist::domVcount* pDomVCount = pDomPolyList->getVcount();
	if (!pDomVCount)
		return COLLADA_ERR_BAD_FORMAT; // bad format: <vcount> is required
	domListOfUInts& DomVCountIntArr = pDomVCount->getValue();

	if (polyCount != DomVCountIntArr.getCount()) {
		//@@Todo: log warning

		//Use DomVCountIntArr.getCount() if there is a discrepancy.
		polyCount = DomVCountIntArr.getCount();
	}

	domListOfUInts& indices = pDomP->getValue();

	BOOL errorFound = FALSE;

	// Counting triangles
	DWORD triCount = 0; //total triangles
	for (UINT polyLoop = 0; polyLoop < polyCount; polyLoop++)
		triCount += std::max((int)DomVCountIntArr[polyLoop] - 2, 0);
	submesh.setVertexBuffer(triCount * 3);

	UINT globalTriId = 0;
	UINT globalVtxId = 0;
	for (DWORD polyLoop = 0; polyLoop < polyCount; polyLoop++) // For each polygon
	{
		vector<ColladaVertex> pol;
		pol.clear();

		for (int vtxIdx = 0; vtxIdx < DomVCountIntArr[polyLoop]; vtxIdx++, globalVtxId++) {
			ColladaVertex vtx;
			if (!ReadVertexIndices(indices, globalVtxId, *pInputOffsetArr, vtx, vs))
				errorFound = TRUE;
			pol.push_back(vtx); // add vertex even if error found
		}

		//triangulate this polygon
		//! @@Limitation: polygons must be coplanar and convex. [Yanfeng 09/2008]
		//! @@Todo: concave polygons [Yanfeng 09/2008]
		//! @@Todo: non-coplanar polygons: possibly won't support it anyway

		if (pol.size() < 3)
			continue; // less than three vertices

		vector<ColladaVertex>::iterator itA = pol.begin();
		vector<ColladaVertex>::iterator itB = itA + 1;

		int triCnt = 0;
		while (itA != itB + 1 && itB + 1 != pol.end()) {
			if (triCnt % 2) // odd triangles
			{
				vector<ColladaVertex>::iterator itC;
				if (itA == pol.begin())
					itC = pol.end() - 1;
				else
					itC = itA - 1;

				// Add triangle
				ASSERT(globalTriId + 1 <= triCount);
				if (globalTriId + 1 > triCount) {
					errorFound = TRUE;
					break;
				}

				submesh.setVertex(globalTriId * 3, *itA);
				submesh.setVertex(globalTriId * 3 + 1, *itB);
				submesh.setVertex(globalTriId * 3 + 2, *itC);

				itA = itC;
			} else // even triangles
			{
				vector<ColladaVertex>::iterator itC = itB + 1;
				if (itC == pol.end())
					break;

				// Add triangle
				ASSERT(globalTriId + 1 <= triCount);
				if (globalTriId + 1 > triCount) {
					errorFound = TRUE;
					break;
				}

				submesh.setVertex(globalTriId * 3, *itA);
				submesh.setVertex(globalTriId * 3 + 1, *itB);
				submesh.setVertex(globalTriId * 3 + 2, *itC);

				itB = itC;
			}

			globalTriId++;
			triCnt++;
		}
	}

	return errorFound ? COLLADA_ERR_BAD_FORMAT : COLLADA_OK;
}

bool ColladaUtil::ReadDomLookat(domLookat* pDomMatrix, ImpFloatMatrix4& matrix) {
	//@@Todo: support <lookat> element
	return FALSE;
}

bool ColladaUtil::ReadDomMatrix(domListOfFloats& floatArr, ImpFloatMatrix4& matrix, UINT offset /*=0*/) {
	if (floatArr.getCount() < offset + 16)
		return FALSE;

	for (int col = 0; col < 4; col++)
		for (int row = 0; row < 4; row++)
			matrix[col][row] = (float)floatArr[offset + col * 4 + row];

	return TRUE;
}

bool ColladaUtil::ReadDomRotate(domRotate* pDomRotate, ImpFloatMatrix4& matrix) {
	domFloat4& floatArr = pDomRotate->getValue();

	float deg = floatArr[3];
	if (deg == 0)
		return FALSE;

	//@@Bug: sin/cos problem - math function in rotate? returned bad values when angle is negative. [Yanfeng 10/2008]
	//@@Bug: sin(-3.1415927) ~ 0.098 cos(-3.1415927) ~ 0.95. Causing two pyramids in ver2.dae misaligned.
	//@@Bug: such problem only happens in Editor, collada previewer works fine.
	//@@Fix: temporary fix to make angles positive - need to figure out why sin/cos failed. [Yanfeng 10/2008]
	while (deg < 0) deg += 360; // Weird problem: very bad precision in negative float point number, why?
	float rad = deg * (float)M_PI / 180.0f;

	if (floatArr[0] == 1 && floatArr[1] == 0 && floatArr[2] == 0)
		matrix.RotateX(rad);
	else if (floatArr[0] == 0 && floatArr[1] == 1 && floatArr[2] == 0)
		matrix.RotateY(rad);
	else if (floatArr[0] == 0 && floatArr[1] == 0 && floatArr[2] == 1)
		matrix.RotateZ(rad);
	else
		return FALSE; //@@Todo: rotation along any arbitrary axis

	return TRUE;
}

bool ColladaUtil::ReadDomScale(domScale* pDomScale, ImpFloatMatrix4& matrix) {
	domFloat3& floatArr = pDomScale->getValue();

	matrix.Scale((float)floatArr[0], (float)floatArr[1], (float)floatArr[2]);
	return TRUE;
}

bool ColladaUtil::ReadDomSkew(domSkew* pDomSkew, ImpFloatMatrix4& matrix) {
	//@@Todo: support <skew> element
	return FALSE;
}

bool ColladaUtil::ReadDomTranslate(domTranslate* pDomTranslate, ImpFloatMatrix4& matrix) {
	domFloat3& floatArr = pDomTranslate->getValue();

	matrix.Translate((float)floatArr[0], (float)floatArr[1], (float)floatArr[2]);
	return TRUE;
}

//! Read data from <any_element><assets><up_axis>
enumUPAXIS ColladaUtil::ReadDomUpAxis(daeElement* pElem) {
	daeElement* pChild = pElem->getChild("asset");
	domAsset* pDomAsset = dynamic_cast<domAsset*>(pChild);
	if (pDomAsset != NULL)
		return ReadDomAssetUpAxis(pDomAsset);
	return CSM_UNK_UP;
}

//! Read data from <assets><up_axis>
enumUPAXIS ColladaUtil::ReadDomAssetUpAxis(domAsset* pDomAsset) {
	const domAsset::domUp_axis* pDomUp = pDomAsset->getUp_axis();
	if (pDomUp) {
		switch (pDomUp->getValue()) {
			case UPAXISTYPE_X_UP: return CSM_X_UP;
			case UPAXISTYPE_Y_UP: return CSM_Y_UP;
			case UPAXISTYPE_Z_UP: return CSM_Z_UP;
		}
	}

	return CSM_UNK_UP;
}

void ColladaUtil::ReadDomBindMaterial(domBind_material* pDomBindMat, ColladaSubmesh& submesh) {
	domBind_material::domTechnique_common* pTechComm = pDomBindMat->getTechnique_common();
	if (pTechComm == NULL)
		return;

	domInstance_material_Array& DomInstMatArr = pTechComm->getInstance_material_array();

	for (DWORD i = 0; i < DomInstMatArr.getCount(); i++) {
		domInstance_material* pDomInstMat = DomInstMatArr[i];

		xsAnyURI& uriTarget = pDomInstMat->getTarget();
		xsNCName symbol = pDomInstMat->getSymbol();

		//Only bind material to submesh if material symbol name matches
		//This will fix multimaterial mesh binding error. [Yanfeng 05/2009]
		if (symbol != submesh.getMaterialName())
			continue;

		domMaterial* pDomMat = dynamic_cast<domMaterial*>(uriTarget.getElement().cast());
		if (pDomMat == NULL) {
			//@@Todo: log warnings
			continue;
		}

		ColladaMaterialPtr pMat(new ColladaMaterial());

		if (ReadDomMaterial(pDomMat, *pMat)) {
			// <instance_material><bind_vertex_input>
			domInstance_material::domBind_vertex_input_Array& DomBindVtxArr = pDomInstMat->getBind_vertex_input_array();
			for (DWORD bindVtxLoop = 0; bindVtxLoop < DomBindVtxArr.getCount(); bindVtxLoop++)
				ReadDomBindVertexInput(DomBindVtxArr[bindVtxLoop], submesh, *pMat);

			// <instance_material><bind>
			domInstance_material::domBind_Array& DomBindArr = pDomInstMat->getBind_array();
			for (DWORD bindLoop = 0; bindLoop < DomBindArr.getCount(); bindLoop++)
				ReadDomBind(DomBindArr[bindLoop], *pMat);

			BOOL bMaterialResolved = submesh.resolveMaterial(symbol, pMat);
			if (!bMaterialResolved) {
				//@@Todo: warning about missing texture/UVs
			}

			// Rebuild texcoord sets for all vertices based on map channel

			LPVOID savedChannels[CM_MAP_CHANNELS];
			UINT savedCounts[CM_MAP_CHANNELS];
			UINT useCount[CM_MAP_CHANNELS];
			memset(savedChannels, 0, sizeof(savedChannels));
			memset(savedCounts, 0, sizeof(savedCounts));
			memset(useCount, 0, sizeof(useCount));

			// Save pointers to texel buffers
			for (UINT chanLoop = 0; chanLoop < CM_MAP_CHANNELS; chanLoop++) {
				if (!pMat->channelHasValue(chanLoop, CT_TEXTURE))
					continue;

				const ColladaTexture& texture = pMat->getTextureByChannel(chanLoop);
				if (!texture.hasMap())
					continue; // skip if no map

				int uvIndex = texture.getMapIndex();
				if (uvIndex == -1)
					continue; // skip if texture index not resolved

				ASSERT(submesh.getTexelBuffer(uvIndex) != NULL);

				if (useCount[uvIndex]) {
					//already referenced, make a copy
					UINT texCount = submesh.getTexelCount(uvIndex);
					savedChannels[chanLoop] = new TXTUV[texCount];
					savedCounts[chanLoop] = texCount;
					memcpy(savedChannels[chanLoop], submesh.getTexelBuffer(uvIndex), texCount * sizeof(TXTUV));
				} else {
					//first reference, copy pointer
					savedChannels[chanLoop] = submesh.getTexelBuffer(uvIndex);
					savedCounts[chanLoop] = submesh.getTexelCount(uvIndex);
				}

				useCount[uvIndex]++;
			}

			// Now savedChannels stores tex buffers by channel ID (instead of by uv index)
			// Previously-shared tex buffers have been replicated--one for each channel.

			// Clean up texel buffers from submesh
			for (UINT uvLoop = 0; uvLoop < CM_MAP_CHANNELS; uvLoop++) {
				if (useCount[uvLoop])
					submesh.unlinkTexelBuffer(uvLoop); //unlink referenced UV channels to avoid memory disposal
				else
					submesh.setTexelBuffer(uvLoop, 0); //dispose unreferenced UV channels
			}

			// Save indices in vertex
			ColladaVertex* savedVertices = new ColladaVertex[submesh.getVertexCount()];
			for (UINT vtxLoop = 0; vtxLoop < submesh.getVertexCount(); vtxLoop++) {
				ColladaVertex v = submesh.getVertex(vtxLoop);
				savedVertices[vtxLoop] = v;
				memset(v.texelIndex, 0xFF, sizeof(v.texelIndex));
				submesh.setVertex(vtxLoop, v);
			}

			// Now reconstruct data
			for (UINT chanLoop = 0; chanLoop < CM_MAP_CHANNELS; chanLoop++) {
				if (!pMat->channelHasValue(chanLoop, CT_TEXTURE))
					continue; // skip if no texture

				const ColladaTexture& texture = pMat->getTextureByChannel(chanLoop);
				if (!texture.hasMap())
					continue; // skip if no map

				int uvIndex = texture.getMapIndex();
				const string& target = texture.getMapTarget();

				// 1) copy back pointers of texel buffers
				if (uvIndex != -1) {
					// copy pointer if previously saved
					ASSERT(savedChannels[chanLoop] != NULL);
					submesh.setTexelBuffer(chanLoop, savedCounts[chanLoop], (TXTUV*)savedChannels[chanLoop]);
					savedChannels[chanLoop] = NULL;
					savedCounts[chanLoop] = 0;
				} else if (!target.empty()) // otherwise, check to see if we have non-indexed(bindable) UV channels
				{
					// Bindable map: read TexCoords from <source> specified by target

					// Resolve target
					DAE* dae = pDomBindMat->getDAE();
					if (!dae)
						continue;
					daeDatabase* db = dae->getDatabase();
					if (!db)
						continue;

					string attrName, sidPath;
					int attrIndex;
					daeElement* pTarget = ResolveDomTarget(db, target, sidPath, attrName, attrIndex);
					if (!pTarget)
						continue;

					//@@Limitation: only deal with <source> - It's unclear if there would be any other elements. [Yanfeng 09/2008]
					domSource* pDomSource = dynamic_cast<domSource*>(pTarget);
					if (!pDomSource)
						continue;

					// Reading TexCoords into vertices
					SimpleArray<TXTUV> allTexCoords;
					if (ColladaUtil::ReadAllTexCoords(allTexCoords, pDomSource) > 0) {
						UINT texCnt = 0;
						TXTUV* texBuffer = allTexCoords.detachBuffer(texCnt);
						submesh.setTexelBuffer(chanLoop, texCnt, texBuffer);
					}
				} else if (uvIndex == -1) {
					//@@Todo: log warning - texture dropped because of no matching UVs [Yanfeng 10/2008]
					pMat->setColorChannel(chanLoop, ColladaColor(1.0f, 1.0f, 1.0f, 1.0f));
				}

				// 2) rebuild per-vertex indices
				for (UINT vtxLoop = 0; vtxLoop < submesh.getVertexCount(); vtxLoop++) {
					ColladaVertex vtx = submesh.getVertex(vtxLoop);
					if (uvIndex != -1)
						vtx.texelIndex[chanLoop] = savedVertices[vtxLoop].texelIndex[uvIndex];
					else if (!target.empty())
						vtx.texelIndex[chanLoop] = vtxLoop; //<== @@Watch
					else
						vtx.texelIndex[chanLoop] = -1;
					submesh.setVertex(vtxLoop, vtx);

				} // vtxLoop

			} // chanLoop

			// dispose saved data
			// vertices (dispose all as this is a full clone)
			delete[] savedVertices;
		}
	}
}

bool ColladaUtil::ReadDomMaterial(domMaterial* pDomMat, ColladaMaterial& mat) {
	// <material><instance_effect> ==> <library_effects><effect>
	domInstance_effect* pDomInstEff = pDomMat->getInstance_effect();
	if (pDomInstEff == NULL)
		return FALSE;
	xsAnyURI uri = pDomInstEff->getUrl();

	// <library_effects><effect>
	domEffect* pDomEffect = dynamic_cast<domEffect*>(uri.getElement().cast());
	if (pDomEffect == NULL)
		return FALSE;

	return ReadDomEffect(pDomEffect, mat);
}

bool ColladaUtil::ReadDomEffect(domEffect* pDomEffect, ColladaMaterial& mat) {
	domFx_profile_abstract_Array& DomProfiles = pDomEffect->getFx_profile_abstract_array();
	if (DomProfiles.getCount() == 0)
		return FALSE;

	for (DWORD profIdx = 0; profIdx < DomProfiles.getCount(); profIdx++) {
		// Loop until we have a profile_COMMON
		domProfile_COMMON* pDomProfile = dynamic_cast<domProfile_COMMON*>(DomProfiles.get(profIdx).cast());
		if (pDomProfile == NULL)
			continue;

		return ReadDomFxProfileCommon(pDomProfile, mat);
	}

	// profile_COMMON not found
	// @@Todo: BAD FORMAT warning
	return FALSE;
}

bool ColladaUtil::ReadDomFxProfileCommon(domProfile_COMMON* pDomProfile, ColladaMaterial& mat) {
	domProfile_COMMON::domTechnique* pDomTech = pDomProfile->getTechnique();
	if (pDomTech == NULL)
		return FALSE;

	// Must include one of the following: blinn, lambert, phong or constant

	bool ret = FALSE;
	if (pDomTech->getChild("blinn") != NULL)
		ret = ReadDomFxShading(pDomTech->getChild("blinn"), mat);
	if (pDomTech->getChild("phong") != NULL)
		ret = ReadDomFxShading(pDomTech->getChild("phong"), mat);
	if (pDomTech->getChild("lambert") != NULL)
		ret = ReadDomFxShading(pDomTech->getChild("lambert"), mat);
	if (pDomTech->getChild("constant") != NULL)
		ret = ReadDomFxShading(pDomTech->getChild("constant"), mat);

	// <profile_COMMON><technique><extra>
	domExtra_Array& techExtraArr = pDomTech->getExtra_array();
	for (size_t extLoop = 0; extLoop < techExtraArr.getCount(); extLoop++) {
		ReadDomFxProfileCommonTechExtra(techExtraArr[extLoop], mat);
	}

	// <profile_COMMON><extra>
	domExtra_Array& extraArr = pDomProfile->getExtra_array();
	for (size_t extLoop = 0; extLoop < extraArr.getCount(); extLoop++) {
		ReadDomFxProfileCommonExtra(extraArr[extLoop], mat);
	}

	// Read all <newparam> elements and try to resolve texture images definitions in ColladaMaterial

	// Read all samplers and all surfaces
	domCommon_newparam_type_Array& DomNewParamArr = pDomProfile->getNewparam_array();
	for (UINT paramLoop = 0; paramLoop < DomNewParamArr.getCount(); paramLoop++) {
		domCommon_newparam_type* pNewParam = DomNewParamArr[paramLoop];
		if (!pNewParam || !pNewParam->getSid())
			continue;

		string paramName = pNewParam->getSid();

		if (domFx_surface_common* pDomSurface = pNewParam->getSurface()) {
			// <newparam><surface>
			ColladaFxSurface& surface = mat.addSurface(paramName);
			if (!ReadDomNewParamSurface(pDomSurface, surface))
				continue;
		} else if (domFx_sampler2D_common* pDomSampler2D = pNewParam->getSampler2D()) {
			// <newparam><sampler2D>
			ColladaFxSampler& sampler = mat.addSampler(paramName);
			if (!ReadDomNewParamSampler(pDomSampler2D, sampler))
				continue;
		}
	}

	// Resolve all samplers with surface link
	mat.resolveAllSurfaceSources();

	// Resolve all textures with samplers links
	mat.resolveTextureParamToImageURI();

	return ret;
}

bool ColladaUtil::ReadDomFxProfileCommonTechExtra(domExtra* pDomExtra, ColladaMaterial& mat) {
	bool ret = true;
	domTechnique_Array& techArr = pDomExtra->getTechnique_array();
	if (techArr.getCount() > 0) {
		for (UINT techLoop = 0; techLoop < techArr.getCount(); techLoop++) {
			domTechnique* pDomTech = techArr[techLoop];
			if (MatchxsNMToken(pDomTech->getProfile(), "FCOLLADA")) {
				if (!ReadDomFxProfileFCollada(pDomTech, mat))
					ret = false;
			}
		}
	}

	return ret;
}

bool ColladaUtil::ReadDomFxProfileCommonExtra(domExtra* pDomExtra, ColladaMaterial& mat) {
	bool ret = true;
	domTechnique_Array& techArr = pDomExtra->getTechnique_array();
	if (techArr.getCount() > 0) {
		for (UINT techLoop = 0; techLoop < techArr.getCount(); techLoop++) {
			domTechnique* pDomTech = techArr[techLoop];
			if (MatchxsNMToken(pDomTech->getProfile(), "GOOGLEEARTH")) {
				ReadDomFxProfileGoogleEarth(pDomTech, mat);
			}
		}
	}

	return ret;
}

bool ColladaUtil::ReadDomFxProfileFCollada(domTechnique* pDomTech, ColladaMaterial& mat) {
	if (pDomTech->getChild("spec_level"))
		ReadDomColorOrTexture(pDomTech->getChild("spec_level"), CM_SPECLEVEL, mat);

	if (pDomTech->getChild("shininess"))
		ReadDomColorOrTexture(pDomTech->getChild("shininess"), CM_SHININESS, mat);

	if (pDomTech->getChild("emission"))
		ReadDomColorOrTexture(pDomTech->getChild("emission"), CM_EMISSIVE, mat);

	//Following extensions are not supported for now
	//if (pDomTech->getChild("filter_color")) ReadDomColorOrTexture(pDomTech->getChild("filter_color"), CM_FILTERCLR, mat);
	//if (pDomTech->getChild("bump")) ReadDomColorOrTexture(pDomTech->getChild("bump"), CM_BUMP, mat);
	//if (pDomTech->getChild("index_of_refraction")) ReadDomColorOrTexture(pDomTech->getChild("index_of_refraction"), CM_REFRACTION, mat);
	//if (pDomTech->getChild("displacement")) ReadDomColorOrTexture(pDomTech->getChild("displacement"), CM_DISPLACEMENT, mat);

	return TRUE;
}

bool KEP::ColladaUtil::ReadDomFxProfileGoogleEarth(domTechnique* pDomTech, ColladaMaterial& mat) {
	domElement* pDomDoubleSided = pDomTech->getChild("double_sided");
	if (pDomDoubleSided) {
		mat.setGoogleEarthDoubleSided(pDomDoubleSided->getCharData() == "1");
	}

	return true;
}

bool ColladaUtil::ReadDomFxShading(daeElement* pShading, ColladaMaterial& mat) {
	mat.clear(); // Move this to caller?

	if (pShading->getChild("ambient"))
		ReadDomColorOrTexture(pShading->getChild("ambient"), CM_AMBIENT, mat);
	if (pShading->getChild("diffuse"))
		ReadDomColorOrTexture(pShading->getChild("diffuse"), CM_DIFFUSE, mat);
	if (pShading->getChild("specular"))
		ReadDomColorOrTexture(pShading->getChild("specular"), CM_SPECULAR, mat);
	if (pShading->getChild("emission"))
		ReadDomColorOrTexture(pShading->getChild("emission"), CM_EMISSIVE, mat);
	if (pShading->getChild("reflective"))
		ReadDomColorOrTexture(pShading->getChild("reflective"), CM_REFLECTIVE, mat);
	if (pShading->getChild("transparent"))
		ReadDomColorOrTexture(pShading->getChild("transparent"), CM_TRANSPARENT, mat);
	if (pShading->getChild("index_of_refraction"))
		mat.setFloatChannel(CM_REFRACTION, ReadDomFloat(pShading->getChild("index_of_refraction")));
	if (pShading->getChild("reflectivity"))
		mat.setFloatChannel(CM_REFLECTIVITY, ReadDomFloat(pShading->getChild("reflectivity")));
	if (pShading->getChild("shininess"))
		mat.setFloatChannel(CM_SHININESS, ReadDomFloat(pShading->getChild("shininess")));
	if (pShading->getChild("transparency"))
		mat.setFloatChannel(CM_TRANSPARENCY, ReadDomFloat(pShading->getChild("transparency")));

	return TRUE;
}

// One out of following: <color>, <texture> or <param>
bool ColladaUtil::ReadDomColorOrTexture(domCommon_color_or_texture_type_complexType* pColorOrTexture, UINT chan, ColladaMaterial& mat) {
	domCommon_color_or_texture_type_complexType::domColor* pColor = pColorOrTexture->getColor();
	if (pColor) {
		ColladaColor clr;
		ReadDomColor(pColor->getValue(), clr);
		mat.setColorChannel(chan, clr);
		return TRUE;
	}

	domCommon_color_or_texture_type_complexType::domTexture* pTexture = pColorOrTexture->getTexture();
	if (pTexture) {
		ColladaTexture texture;
		ReadDomTexture(pTexture->getTexture(), pTexture->getTexcoord(), texture);
		mat.setTextureChannel(chan, texture);
		return TRUE;
	}

	domCommon_color_or_texture_type_complexType::domParam* pParam = pColorOrTexture->getParam();
	if (pParam) {
		//@@Todo: <param>
		//ReadDomMaterialParam(pParam, mat);
		//return TRUE;
	}

	return FALSE;
}

bool ColladaUtil::ReadDomColorOrTexture(daeElement* pElement, UINT chan, ColladaMaterial& mat) {
	domCommon_color_or_texture_type_complexType* pColorOrTexture = dynamic_cast<domCommon_color_or_texture_type_complexType*>(pElement);
	if (pColorOrTexture)
		return ReadDomColorOrTexture(pColorOrTexture, chan, mat);

	// type conversion failed - pElement is not a color_or_texture_type
	// In order to support FCOLLADA extended format we need to deal with a color_or_texture element in non-standard type (domAny)
	daeElement* pColor = pElement->getChild("color");
	if (pColor) {
		ColladaColor clr;
		ReadDomColor(pColor->getCharData(), clr);
		mat.setColorChannel(chan, clr);
		return TRUE;
	}

	daeElement* pTexture = pElement->getChild("texture");
	if (pTexture) {
		ColladaTexture texture;
		ReadDomTexture(pTexture->getAttribute("texture").c_str(), pTexture->getAttribute("texcoord").c_str(), texture);
		mat.setTextureChannel(chan, texture);
		return TRUE;
	}

	daeElement* pParam = pElement->getChild("param");
	if (pParam) {
		//@@Todo: <param>
		//ReadDomMaterialParam(pParam, mat);
		//return TRUE;
	}

	return FALSE;
}

bool ColladaUtil::ReadDomTexture(xsNCName textureParam, xsNCName uvSetSymbol, ColladaTexture& tex) {
	tex.clear();

	if (textureParam)
		tex.setTextureParam(textureParam); // To be resolved
	if (uvSetSymbol)
		tex.setUVSetSymbol(uvSetSymbol); // To be resolved

	return TRUE;
}

bool ColladaUtil::ReadDomBindVertexInput(domInstance_material::domBind_vertex_input* pBindVtx, const ColladaSubmesh& submesh, ColladaMaterial& mat) {
	xsNCName sem = pBindVtx->getSemantic();
	xsNCName inputSem = pBindVtx->getInput_semantic();

	UINT inputSet = 0; //@@Watch: default input set is 0 or 1? checked collada spec but not found
	if (pBindVtx->hasAttribute("input_set"))
		inputSet = (UINT)pBindVtx->getInput_set();

	ColladaInputOffsetArrayPtr pInputOffsetArr = submesh.getInputOffsetArray();
	VERIFY_RETURN(pInputOffsetArr, FALSE);

	int uvIndex = pInputOffsetArr->FindUVIndexBySet(inputSet);

	//@@Limitation: only binds TEXCOORD semantic
	if (inputSem != NULL && sem != NULL && _stricmp(inputSem, "TEXCOORD") == 0) {
		// Resolve texture coordinates
		mat.resolveTextureMapSymbolToUVIndex(sem, uvIndex);
		return TRUE;
	}

	return FALSE;
}

bool ColladaUtil::ReadDomBind(domInstance_material::domBind* pBind, ColladaMaterial& mat) {
	xsNCName sem = pBind->getSemantic();
	xsToken target = pBind->getTarget();

	if (sem != NULL && target != NULL) {
		// Resolve texture coordinates
		mat.resolveTextureMapSymbolToTarget(sem, target);
		return TRUE;
	}

	return FALSE;
}

enumCS GetSamplerWrap(domFx_sampler_wrap_common wrap) {
	switch (wrap) {
		case FX_SAMPLER_WRAP_COMMON_NONE: return CS_NONE;
		case FX_SAMPLER_WRAP_COMMON_WRAP: return CS_WRAP;
		case FX_SAMPLER_WRAP_COMMON_MIRROR: return CS_MIRROR;
		case FX_SAMPLER_WRAP_COMMON_CLAMP: return CS_CLAMP;
		case FX_SAMPLER_WRAP_COMMON_BORDER: return CS_BORDER;
	}

	return CS_NONE;
}

enumCS GetSamplerFilter(domFx_sampler_filter_common filter) {
	switch (filter) {
		case FX_SAMPLER_WRAP_COMMON_NONE: return CS_NONE;
		case FX_SAMPLER_FILTER_COMMON_NEAREST: return CS_NEAREST;
		case FX_SAMPLER_FILTER_COMMON_LINEAR: return CS_LINEAR;
		case FX_SAMPLER_FILTER_COMMON_NEAREST_MIPMAP_NEAREST: return CS_NEAREST_MIPMAP_NEAREST;
		case FX_SAMPLER_FILTER_COMMON_LINEAR_MIPMAP_NEAREST: return CS_LINEAR_MIPMAP_NEAREST;
		case FX_SAMPLER_FILTER_COMMON_NEAREST_MIPMAP_LINEAR: return CS_NEAREST_MIPMAP_LINEAR;
		case FX_SAMPLER_FILTER_COMMON_LINEAR_MIPMAP_LINEAR: return CS_LINEAR_MIPMAP_LINEAR;
	}

	return CS_NONE;
}

bool ColladaUtil::ReadDomNewParamSampler(domFx_sampler2D_common* pDomSampler2D, ColladaFxSampler& sampler) {
	domFx_sampler2D_common_complexType::domSource* pDomSource = pDomSampler2D->getSource();
	if (!pDomSource || !pDomSource->getValue())
		return FALSE; // required

	sampler.surfaceSource = pDomSource->getValue();

	domFx_sampler2D_common_complexType::domWrap_s* ws = pDomSampler2D->getWrap_s();
	if (ws)
		sampler.wrapU = GetSamplerWrap(ws->getValue());
	domFx_sampler2D_common_complexType::domWrap_t* wt = pDomSampler2D->getWrap_t();
	if (wt)
		sampler.wrapV = GetSamplerWrap(wt->getValue());

	domFx_sampler2D_common_complexType::domMinfilter* minF = pDomSampler2D->getMinfilter();
	if (minF)
		sampler.minFilter = GetSamplerFilter(minF->getValue());
	domFx_sampler2D_common_complexType::domMagfilter* magF = pDomSampler2D->getMagfilter();
	if (magF)
		sampler.magFilter = GetSamplerFilter(magF->getValue());
	domFx_sampler2D_common_complexType::domMipfilter* mipF = pDomSampler2D->getMipfilter();
	if (mipF)
		sampler.mipFilter = GetSamplerFilter(mipF->getValue());

	domFx_sampler2D_common_complexType::domBorder_color* border = pDomSampler2D->getBorder_color();
	if (border)
		ReadDomColor(border->getValue(), sampler.borderColor);

	domFx_sampler2D_common_complexType::domMipmap_maxlevel* maxMipLevel = pDomSampler2D->getMipmap_maxlevel();
	if (maxMipLevel)
		sampler.mipMapMaxLevel = maxMipLevel->getValue();

	domFx_sampler2D_common_complexType::domMipmap_bias* mipBias = pDomSampler2D->getMipmap_bias();
	if (mipBias)
		sampler.mipMapBias = (float)mipBias->getValue();

	return TRUE;
}

enumCST GetSurfaceType(domFx_surface_type_enum type) {
	switch (type) {
		case FX_SURFACE_TYPE_ENUM_UNTYPED: return CST_UNTYPED;
		case FX_SURFACE_TYPE_ENUM_1D: return CST_1D;
		case FX_SURFACE_TYPE_ENUM_2D: return CST_2D;
		case FX_SURFACE_TYPE_ENUM_3D: return CST_3D;
		case FX_SURFACE_TYPE_ENUM_CUBE: return CST_CUBE;
		case FX_SURFACE_TYPE_ENUM_DEPTH: return CST_DEPTH;
		case FX_SURFACE_TYPE_ENUM_RECT: return CST_RECT;
	}

	return CST_UNTYPED;
}

bool ColladaUtil::ReadDomNewParamSurface(domFx_surface_common* pDomSurface, ColladaFxSurface& surface) {
	surface.type = GetSurfaceType(pDomSurface->getType());

	//@@Limitation: requires a init_from [Yanfeng 09/2008]
	domFx_surface_init_common* pDomSurfaceInit = pDomSurface->getFx_surface_init_common();
	if (!pDomSurfaceInit)
		return FALSE;

	domFx_surface_init_from_common_Array& DomSurfaceInitFromArr = pDomSurfaceInit->getInit_from_array();
	if (DomSurfaceInitFromArr.getCount() == 0)
		return FALSE; //@@Limitation: only parse init_from [Yanfeng 09/2008]

	//@@Todo: support advanced initialization methods [Yanfeng 09/2008]

	for (UINT i = 0; i < DomSurfaceInitFromArr.getCount(); i++) {
		domFx_surface_init_from_common* pSurfaceInitFrom = DomSurfaceInitFromArr[i];
		if (!pSurfaceInitFrom)
			continue;

		ReadDomSurfaceInitFrom(pSurfaceInitFrom, surface);
	}

	return TRUE;
}

bool ColladaUtil::ReadDomSurfaceInitFrom(domFx_surface_init_from_common* pSurfaceInitFrom, ColladaFxSurface& surface) {
	//@@Limitation: only support file path [Yanfeng 09/2008]
	//@@Todo: advanced surface: mip, face, slice [Yanfeng 09/2008]

	xsIDREF& idRef = pSurfaceInitFrom->getValue();
	domImage* pDomImage = dynamic_cast<domImage*>(idRef.getElement());
	if (!pDomImage)
		return FALSE; // conversion failed

	domImage::domInit_from* pDomImageInitFrom = pDomImage->getInit_from();
	if (!pDomImageInitFrom)
		return FALSE;

	xsAnyURI& uri = pDomImageInitFrom->getValue();
	surface.imageURI = uri.getURI(); //@@Watch: uri might be in somewhere else

	return TRUE;
}

ImpFloatVector3* ColladaUtil::ReadNormalPositions(domVertices* pDomVertices, UINT& vtxCount) {
	return ReadPositions("NORMAL", pDomVertices, vtxCount);
}

ImpFloatVector3* ColladaUtil::ReadVertexPositions(domVertices* pDomVertices, UINT& vtxCount) {
	return ReadPositions("POSITION", pDomVertices, vtxCount);
}

ImpFloatVector3* ColladaUtil::ReadPositions(char const* const type, domVertices* pDomVertices, UINT& vtxCount) {
	domInputLocal_Array& DomInputLocArr = pDomVertices->getInput_array();

	if (DomInputLocArr.getCount() == 0)
		return NULL;

	TIMING_START(ReadPositions)

	ImpFloatVector3* ret = NULL;

	// There might be multiple inputs, need to determine which one contains the desired vertex position
	for (DWORD arrIdx = 0; arrIdx < DomInputLocArr.getCount(); arrIdx++) {
		// <input:semantic>
		xsNMTOKEN sem = DomInputLocArr[arrIdx]->getSemantic();

		if (MatchxsNMToken(sem, type)) {
			// <input:source> ==> <source>
			domURIFragmentType& uriSource = DomInputLocArr[arrIdx]->getSource();
			//if (uriSource.getState()!=daeURI::uri_success) uriSource.resolveElement();

			//@@Limitation: only <source> is supported [Yanfeng 09/2008]
			domSource* pDomSource = dynamic_cast<domSource*>(uriSource.getElement().cast());
			if (!pDomSource)
				break;

			// Read all vertex position data from data source through pipeline
			SimpleArray<ImpFloatVector3> allVectors;
			if (ReadAllFloatVectors(allVectors, pDomSource) > 0)
				ret = allVectors.detachBuffer(vtxCount);
			break;
		}
	}

	TIMING_END(ReadPositions)
	return ret;
}

BOOL ColladaUtil::ReadVertexIndices(domListOfUInts& indices, int vtxId, const ColladaInputOffsetArray& cldInputOfsArr, ColladaVertex& vtx, VertexSemantics vs) {
	BOOL errorFound = FALSE;

	DWORD baseOffset = vtxId * cldInputOfsArr.GetStride();

	for (DWORD inpIdx = 0; inpIdx < cldInputOfsArr.GetInputCount(); inpIdx++) {
		const ColladaInputOffset* pCldInputOfs = cldInputOfsArr.GetInputConst(inpIdx);

		// Index into the data source
		DWORD index = (DWORD)indices[baseOffset + pCldInputOfs->offset]; // forced type conversion to avoid warning. loss of data OK

		switch (pCldInputOfs->semantic) {
			case CIS_POSITION:
				vtx.positionIndex = index;
				break;
			case CIS_VERTEX: {
				if (vs & VERTICES_POSITION) {
					vtx.positionIndex = index;
				}
				if (vs & VERTICES_NORMAL) {
					vtx.normalIndex = index;
				}
				// TEXCOORD not implemented yet
			} break;
			case CIS_NORMAL:
				vtx.normalIndex = index;
				break;
			case CIS_TEXCOORD:
				// MapChannel ID if this is a TexCoord <input>
				int chan = cldInputOfsArr.FindUVIndexByInputId(inpIdx);
				vtx.texelIndex[chan] = index;
				break;
		}

		//// Read data from data source through pipeline (by index based on semantics)
		//if (!ReadVertexDataBySemanticByIndex(elem, pCldInputOfs->semantic, index, chan, positions, posCount, vtx)) { errorFound = TRUE; continue; }
	}

	return !errorFound;
}

/*
BOOL ColladaUtil::ReadVertexData( domListOfUInts& indices, int vtxId, const ColladaInputOffsetArray& cldInputOfsArr, const ImpFloatVector3* positions, UINT posCount, Vertex& vtx, UINT& uniqueId)
{
	BOOL errorFound = FALSE;

	DWORD baseOffset = vtxId * cldInputOfsArr.GetStride();

	for (DWORD inpIdx=0; inpIdx<cldInputOfsArr.GetInputCount(); inpIdx++)
	{
		const ColladaInputOffset * pCldInputOfs = cldInputOfsArr.GetInputConst(inpIdx);

		// Index into the data source
		DWORD index = (DWORD)indices[baseOffset+pCldInputOfs->offset];	// forced type conversion to avoid warning. loss of data OK
		if (pCldInputOfs->semantic==CIS_VERTEX) 
			uniqueId = index;

		daeElement* elem = pCldInputOfs->source;
		if (!elem) { errorFound = TRUE; continue; }

		// Two possibilities here:
		// a) <source> element: 
		// b) any other elements with an embedded <input>

		// MapChannel ID if this is a TexCoord <input>
		int chan = cldInputOfsArr.FindUVIndexByInputId(inpIdx);

		// Read data from data source through pipeline (by index based on semantics)
		if (!ReadVertexDataBySemanticByIndex(elem, pCldInputOfs->semantic, index, chan, positions, posCount, vtx)) { errorFound = TRUE; continue; }
	}

	return !errorFound;
}
*/
//! Read data from <source> by index based on semantics.
BOOL ColladaUtil::ReadVertexDataBySemanticByIndex(daeElement* pElement, enumCIS semantic, DWORD index, int mapChannel, const ImpFloatVector3* positions, UINT posCount, Vertex& vtx) {
	////////////////////////////////////////////////////////////////
	//! Float array for POSITION, NORMAL, TEXCOORD
	//! Vertex array for VERTEX (indirect referencing)
	////////////////////////////////////////////////////////////////

	TIMING_START(ReadVertexDataBySemanticByIndex);

	domSource* pDomSource = NULL;

	switch (semantic) {
		case CIS_POSITION:
			// Must be <source>
			pDomSource = dynamic_cast<domSource*>(pElement);
			if (pDomSource == NULL)
				return FALSE;
			if (!ReadFloatVectorByIndex(pDomSource, index, (ImpFloatVector3*)&vtx))
				return FALSE;
			break;

		case CIS_NORMAL:
			// Must be <source>
			pDomSource = dynamic_cast<domSource*>(pElement);
			if (pDomSource == NULL)
				return FALSE;
			if (!ReadFloatVectorByIndex(pDomSource, index, (ImpFloatVector3*)&vtx.nx))
				return FALSE;
			break;

		case CIS_TEXCOORD:
			VERIFY_RETURN(mapChannel >= 0, FALSE); // -1 is an invalid value here

			// Must be <source>
			pDomSource = dynamic_cast<domSource*>(pElement);
			if (pDomSource == NULL)
				return FALSE;
			else {
				TXTUV* pTx = GetKEPTexCoordPtrByChannelID(&vtx, mapChannel);
				if (!ReadTexCoordsByIndex(pDomSource, index, pTx))
					return FALSE;
			}
			break;

		case CIS_VERTEX:
			// Performance fix: original implementation is too slow. [Yanfeng 09/2008]
			if (posCount <= index && index < 0)
				return FALSE;
			*(ImpFloatVector3*)&vtx = positions[index];
			// could index normals too?
			break;
	}

	TIMING_END(ReadVertexDataBySemanticByIndex)
	return TRUE;
}

//! \brief Parse a target string in SID path format.
//! \param db Root daeDatabase
//! \param target Target SID path. ID/SID/.../SID.attribName or ID/SID/.../SID(attribIndex)
//! \param attribName Name of the attribute
//! \param attribIndex Index of the attribute
//! \return Target dae element.
daeElement* ColladaUtil::ResolveDomTarget(daeDatabase* db, const string& target, string& sidPath, string& attribName, int& attribIndex) {
	vector<string> allIDs;

	// Default results
	sidPath = target;
	attribName.clear();
	attribIndex = -1;

	size_t posDot = target.find('.');
	if (posDot != string::npos) {
		sidPath = target.substr(0, posDot);
		attribName = target.substr(posDot + 1);
	} else if (!target.empty()) {
		size_t posLP = target.rfind('(');
		size_t posRP = target.rfind(')');
		if (posRP == target.length() - 1 && posLP != string::npos) {
			sidPath = target.substr(0, posLP);
			string attribIndexStr = target.substr(posLP + 1, posRP - posLP - 1);
			if (!attribIndexStr.empty()) {
				stringstream ss(attribIndexStr);
				ss >> attribIndex;
			}
		}
	}

	string segment = sidPath;

	UINT slashPos = 0;
	while (slashPos != string::npos) {
		slashPos = segment.find('/');
		if (slashPos == 0) {
			//bad path found
			//@@Todo: warning message
			return NULL;
		}

		if (slashPos == string::npos)
			allIDs.push_back(segment);
		else {
			allIDs.push_back(segment.substr(0, slashPos));
			segment = segment.substr(slashPos + 1);
		}
	}

	daeElement* pElem = db->idLookup(allIDs[0], db->getDocument((daeUInt)0));
	UINT currLevel = 1;

	while (pElem && currLevel < allIDs.size()) {
		daeElementRefArray arr = pElem->getChildren();
		pElem = NULL; // clear pElem, do not recurse unless it's set in the loop

		for (UINT i = 0; i < arr.getCount(); i++) {
			if (arr[i]->hasAttribute("sid")) {
				if (arr[i]->getAttribute("sid") == allIDs[currLevel])
					pElem = arr[i];
				break;
			}
		}

		if (!pElem)
			break;
		currLevel++;
	}

	return pElem;
}

ColladaInputOffsetArrayPtr ColladaUtil::ParseDomInputArray(const domInputLocalOffset_Array& DomInputLOArr, ColladaSubmesh& submesh) {
	UINT inputCount = DomInputLOArr.getCount();
	if (inputCount == 0)
		return nullptr; // Can't do it without any <input> element

	// Allocate ColladaInputOffsetArray
	auto pCldInputOfsArr = std::make_shared<ColladaInputOffsetArray>(inputCount);
	if (!pCldInputOfsArr)
		return nullptr;

	UINT maxOffset = -1; // maximum offset value for calculating stride width

	for (UINT inputLoop = 0; inputLoop < inputCount; inputLoop++) {
		domInputLocalOffset* pDomInputLO = DomInputLOArr[inputLoop];
		ColladaInputOffset* pCldInputOfs = pCldInputOfsArr->GetInput(inputLoop);

		pCldInputOfs->set = (UINT)pDomInputLO->getSet(); // default set is 0
		pCldInputOfs->offset = (UINT)pDomInputLO->getOffset();
		if (pCldInputOfs->offset > maxOffset || maxOffset == -1)
			maxOffset = pCldInputOfs->offset;

		domURIFragmentType& uri = pDomInputLO->getSource();
		pCldInputOfs->source = uri.getElement();
		pCldInputOfs->semantic = CIS_UNKNOWN; // initialized as unknown

		if (MatchxsNMToken(pDomInputLO->getSemantic(), "VERTEX")) {
			pCldInputOfs->semantic = CIS_VERTEX;
			// A 'vertex' can be defined to contain more than just position,
			// so I need to check <vertices></vertices> to find out what's in it.
			// It could be position, normal and/or texcoord.
		} else if (MatchxsNMToken(pDomInputLO->getSemantic(), "POSITION"))
			pCldInputOfs->semantic = CIS_POSITION;
		else if (MatchxsNMToken(pDomInputLO->getSemantic(), "TEXCOORD")) {
			pCldInputOfs->semantic = CIS_TEXCOORD;
			int uvIndex = pCldInputOfsArr->AddUVIndexByInputId(inputLoop);
			if (uvIndex != -1) {
				domSource* pDomSource = dynamic_cast<domSource*>(pCldInputOfs->source);
				SimpleArray<TXTUV> allTexCoords;
				if (ReadAllTexCoords(allTexCoords, pDomSource) > 0) {
					UINT texCnt = 0;
					TXTUV* uvBuffer = allTexCoords.detachBuffer(texCnt);
					submesh.setTexelBuffer(uvIndex, texCnt, uvBuffer);
				}
			}
		} else if (MatchxsNMToken(pDomInputLO->getSemantic(), "NORMAL")) {
			pCldInputOfs->semantic = CIS_NORMAL;
			domSource* pDomSource = dynamic_cast<domSource*>(pCldInputOfs->source);

			SimpleArray<ImpFloatVector3> allNormals;
			if (ReadAllFloatVectors(allNormals, pDomSource) > 0) {
				UINT normalCount = 0;
				ImpFloatVector3* normalBuffer = allNormals.detachBuffer(normalCount);
				submesh.setNormalBuffer(normalCount, normalBuffer);
			}
		} else if (MatchxsNMToken(pDomInputLO->getSemantic(), "TANGENT"))
			pCldInputOfs->semantic = CIS_TANGENT;
		else if (MatchxsNMToken(pDomInputLO->getSemantic(), "JOINT"))
			pCldInputOfs->semantic = CIS_JOINT;
		else if (MatchxsNMToken(pDomInputLO->getSemantic(), "WEIGHT"))
			pCldInputOfs->semantic = CIS_WEIGHT;
	}

	//! Determine stride width for a <p> element.
	//! It is not well documented in Collada spec how to tell the stride width of the indices defined in <p> element.
	//! Two attributes can be used to determined stride:
	//!   a) the "count" attribute telling how many sets of data there are in the <p> element. However, "count" attributes
	//!      are not available to all parent elements.
	//!   b) the "offset" attributes defined in sibling <input> elements. Although not perfect, we could walk through
	//!      all sibling <input> elements for maximum "offset" value and let [stride width] = [max offset] + 1.
	//! Current implementation uses (b).
	pCldInputOfsArr->SetStride(maxOffset + 1);

	return pCldInputOfsArr;
}

bool KEP::ColladaUtil::hasGeometry(domNode* pDomNode, IClassifyAssetNode* classifyFunc) {
	//check if there is static or skinned meshes attached to current node
	if (pDomNode->getInstance_geometry_array().getCount() > 0 || pDomNode->getInstance_controller_array().getCount() > 0)
		return true;

	//Additional check: if all children of current node are filtered, check geometry at child level as well
	for (UINT i = 0; i < pDomNode->getNode_array().getCount(); i++) {
		domNode* pChild = pDomNode->getNode_array()[i];
		string childName = GetAssetNameFromElement(pChild);
		if (!classifyFunc || classifyFunc->isDummy(childName))
			continue; // Skip if not dummy

		// Now we have a dummy child, see if it has any geometry
		if (hasGeometry(pChild, classifyFunc))
			return true;
	}

	return false;
}

} // namespace KEP