///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ColladaImporter.h"
#include "ColladaImporterImpl.h"

namespace KEP {

// ColladaImporter Module Initialization
static KEP::ColladaImporterImpl instanceImpl;
static KEP::ColladaImporter instanceImporter(&instanceImpl);
KEP::IAssetImporter* g_pColladaImporter = &instanceImporter;

} // namespace KEP
