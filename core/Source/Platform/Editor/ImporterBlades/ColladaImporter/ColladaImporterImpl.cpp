///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CStructuresMisl.h"
#include "CMaterialClass.h"
#include "CHierarchyMesh.h"
#include "CDeformableMesh.h"
#include "CEXMeshClass.h"
#include "TextureDatabase.h"
#include "ColladaAssert.h"
#include "ColladaClasses.h"
#include "ColladaImporterImpl.h"
#include "AssetConverter.h"
#include "ColladaUtil.h"
#include "AssetTree.h"
#include "Common\AssetsDatabase\AssetURI.h"
#include "CSkeletonObject.h"

#define ANIM_URI_ALLFRAMES "*ALLFRAMES*"

// See README.TXT for planned COLLADA schema support

namespace KEP {

class ClassifyColladaAssetNode : public IClassifyAssetNode {
private:
	bool bSkeletal;

public:
	ClassifyColladaAssetNode(bool ske) :
			bSkeletal(ske) {}

	virtual KEP_ASSET_TYPE operator()(const std::string& nodeName, int typeHint, bool hasGeom) {
		if (bSkeletal && typeHint == KEP_SKA_RIGID_MESH && !hasGeom)
			return KEP_SKA_SKELETAL_OBJECT;

		return (KEP_ASSET_TYPE)typeHint;
	}

	virtual bool isDummy(const std::string& nodeName) {
		return STLEndWithIgnoreCase(nodeName, "_PIVOT");
	}
};

static ClassifyColladaAssetNode defClassifyNode(false);
static ClassifyColladaAssetNode defClassifyNodeSkeletal(true);

struct ColladaAnimCacheEntry {
	ColladaSkeletalAnimation animation;
	std::map<std::string, ColladaAnimMatrixSampler*> allSamplers;
	std::map<std::string, SimpleArray<ImpFloatMatrix4>*> allMatrixSources;

	ColladaAnimCacheEntry(const std::string& name) :
			animation(name) {}
	~ColladaAnimCacheEntry() {
		// dispose memory
		for (std::map<std::string, ColladaAnimMatrixSampler*>::iterator it = allSamplers.begin(); it != allSamplers.end(); ++it)
			delete it->second;
		allSamplers.clear();

		for (std::map<std::string, SimpleArray<ImpFloatMatrix4>*>::iterator it = allMatrixSources.begin(); it != allMatrixSources.end(); ++it)
			delete it->second;
		allMatrixSources.clear();
	}
};

ColladaImporterImpl::ColladaImporterImpl() :
		dae(NULL), root(NULL), db(NULL), globalError(COLLADA_OK), importOptions(NULL), pBaseImpl(NULL) {
	skeleton = new ColladaSkeleton();
	importOptions = new std::map<std::string, int>(); // only initialize import options if this is a base impl
}

ColladaImporterImpl::ColladaImporterImpl(IAssetImporterImpl* base, const std::string& aRootURI, int aFlags) :
		IAssetImporterImpl(aRootURI), root(NULL), db(NULL), globalError(COLLADA_OK), importOptions(NULL), pBaseImpl(base) {
	skeleton = new ColladaSkeleton();

	// Parse URI String
	docURI = AssetURI::GetDocumentURIFromURIString(aRootURI);

	dae = new DAE();
	root = dae->open(docURI);
	if (root)
		db = dae->getDatabase();

	if (!root || !db) {
		if ((aFlags & IMP_GENERAL_UIMASK) == IMP_GENERAL_WITHUI) {
			MessageBoxW(0, L"DAE open failed", L"TRACE", MB_OK);
		}
		globalError = COLLADA_ERR_COLLADADOM;
	}
}

ColladaImporterImpl::~ColladaImporterImpl() {
	if (skeleton)
		delete skeleton;
	skeleton = NULL;

	mapUriToDomNode.clear();
	mapUriToJointName.clear();
	mapUriToMatrix.clear();
	mapUriToUpAxis.clear();

	if (dae) {
		if (root)
			dae->close(docURI);
		root = NULL;
		delete dae;
	}

	dae = NULL;

	if (importOptions)
		delete importOptions;
	importOptions = NULL;

	for (std::map<std::string, ColladaAnimCacheEntry*>::iterator it = animCache.begin(); it != animCache.end(); ++it)
		delete it->second;
	animCache.clear();
}

class AddNodeToAssetTree : public NodeHandler {
private:
	std::map<KEP_ASSET_TYPE, std::vector<std::string>>& allAssets;
	ColladaImporterImpl* pImpl;

public:
	AddNodeToAssetTree(ColladaImporterImpl* aImpl, std::map<KEP_ASSET_TYPE, std::vector<std::string>>& aAssetTree) :
			pImpl(aImpl), allAssets(aAssetTree) {}

	BOOL operator()(KEP_ASSET_TYPE assetType, const std::string& origNodeName, int numSubmeshes, domNode* pDomNode, const ImpFloatMatrix4& matrix, const ImpFloatMatrix4& parentMatrix, const std::string& jointName, enumUPAXIS upAxis, enumUPAXIS parentUpAxis) override {
		//5.1.	Add submesh into allAssets[type].
		if (allAssets.find(assetType) != allAssets.end()) {
			std::string nodeName(origNodeName);
			// Check if duplicated name exists
			if (pImpl->mapUriToDomNode.find(nodeName) != pImpl->mapUriToDomNode.end()) {
				// Resolve the conflict
				for (int i = 1;; i++) {
					nodeName = origNodeName + "_" + std::to_string(i);
					if (pImpl->mapUriToDomNode.find(nodeName) == pImpl->mapUriToDomNode.end()) {
						break;
					}
				}
			}

			std::vector<std::string>& assets = allAssets[assetType];
			AssetURI uri;
			uri.node = nodeName;
			uri.subscript = numSubmeshes;
			assets.push_back(uri.ToURIString());

			pImpl->mapUriToDomNode.insert(std::pair<std::string, domNode*>(nodeName, pDomNode));
			pImpl->mapUriToMatrix.insert(std::pair<std::string, ImpFloatMatrix4>(nodeName, parentMatrix));
			pImpl->mapUriToUpAxis.insert(std::pair<std::string, enumUPAXIS>(nodeName, parentUpAxis));
			pImpl->mapUriToJointName.insert(std::pair<std::string, std::string>(nodeName, jointName));
			return TRUE;
		}

		return FALSE;
	}
};

class AddJointToSkeleton : public JointHandler {
private:
	ColladaSkeleton* skeleton;
	//vector<ColladaSkeleton*>* pSkeletonSet;
	//map<kstring, ColladaSkeleton*> mapJointToSkeleton;
	ColladaImporterImpl* pImpl;

public:
	//AddJointToSkeleton(ColladaImporterImpl* aImpl, vector<ColladaSkeleton*>* aSkeletonSet): pImpl(aImpl), pSkeletonSet(aSkeletonSet) { }
	AddJointToSkeleton(ColladaImporterImpl* aImpl, ColladaSkeleton* aSkeleton) :
			pImpl(aImpl), skeleton(aSkeleton) {}

	BOOL operator()(const std::string& jointName, const std::string& parentJoint, const std::string& sid, const ImpFloatMatrix4& matrix, enumUPAXIS upAxis) override {
		/*if (!pSkeletonSet) return FALSE;
		
		//2.1.	If parentJoint==NULL: create new skeleton. 
		ColladaSkeleton* skeleton;
		if (parentJoint.empty())
		{
			skeleton = new ColladaSkeleton();
			pSkeletonSet->push_back(skeleton);
		}
		else skeleton = mapJointToSkeleton[parentJoint];*/

		ASSERT(skeleton != NULL);

		//2.2.	Create a ColladaJoint from curr<node>, currMatrix => currJoint.

		// Record JOINT node
		std::string newParentJoint = parentJoint;
		if (parentJoint.empty())
			newParentJoint = VirtualSkeletonRoot;
		ColladaJoint joint(jointName, newParentJoint, jointName, sid, matrix, skeleton);
		joint.setUpAxis(upAxis);

		//matrix.debug();

		//2.4.	Insert currJoint into skeleton.
		skeleton->addBone(joint);
		//mapJointToSkeleton.insert(pair<kstring, ColladaSkeleton*>(jointName, skeleton));

		return TRUE;
	}
};

int ColladaImporterImpl::DoEnumAssets(std::map<KEP_ASSET_TYPE, std::vector<std::string>>& allAssets, BOOL bSkeletal, IClassifyAssetNode* classifyFunc) {
	domCOLLADA::domScene* pDomScene = root->getScene();
	if (pDomScene) {
		domInstanceWithExtra* pDomInstVisScene = pDomScene->getInstance_visual_scene();
		if (pDomInstVisScene) {
			xsAnyURI& uri = pDomInstVisScene->getUrl();
			domVisual_scene* pDomVisScene = dynamic_cast<domVisual_scene*>(uri.getElement().cast());
			if (pDomVisScene) {
				// Check all parent nodes to get the most recent UP_AXIS
				enumUPAXIS upAxis = GetMostRecentUpAxis(pDomVisScene); // First try <visual_scene> -> <library_visual_scene> -> <collada>
				if (upAxis == CSM_UNK_UP)
					upAxis = GetMostRecentUpAxis(pDomInstVisScene); // If not found, try <instance_visual_scene> -> <scene> -> <collada>

				domNode_Array& DomNodeArr = pDomVisScene->getNode_array();
				AddNodeToAssetTree handleNode(this, allAssets);
				AddJointToSkeleton handleJoint(this, skeleton);

				if (classifyFunc == NULL) {
					//Use default classification function
					classifyFunc = bSkeletal ? &defClassifyNodeSkeletal : &defClassifyNode;
				}

				for (UINT i = 0; i < DomNodeArr.getCount(); i++) {
					ColladaUtil::PreviewDomNode(upAxis, std::string(), DomNodeArr[i], ImpFloatMatrix4(), "", &handleNode, bSkeletal ? &handleJoint : NULL, classifyFunc);
					//@@Todo: check return value and report error if found
				}

				return DomNodeArr.getCount() > 0;
			}
		}
	}

	return COLLADA_ERR_BAD_FORMAT;
}

int ColladaImporterImpl::EnumAssetMeshes(std::map<KEP_ASSET_TYPE, std::vector<std::string>>& allMeshURIs, IClassifyAssetNode* classifyFunc) {
	if (!IsValid())
		return globalError;

	return DoEnumAssets(allMeshURIs, FALSE, classifyFunc);
}

int ColladaImporterImpl::EnumAssetMeshesWithSkeletons(std::map<KEP_ASSET_TYPE, std::vector<std::string>>& allMeshURIs, ColladaSkeleton** allSkeletons, IClassifyAssetNode* classifyFunc) {
	if (!IsValid())
		return globalError;

	skeleton->addBone(ColladaJoint(VirtualSkeletonRoot, "", "#" VirtualSkeletonRoot, "#" VirtualSkeletonRoot, skeleton));
	int ret = DoEnumAssets(allMeshURIs, TRUE, classifyFunc);
	if (skeleton->getBoneCount() == 1) {
		// no bones added during enumeration, clean up skeleton
		skeleton->clearBones();
	}
	*allSkeletons = skeleton; // Return result from state variable of ColladaImporterImpl
	return ret;
}

int ColladaImporterImpl::EnumSkeletalAnimations(std::vector<std::string>& allAnimationURIs, IClassifyAssetNode* classifyFunc) {
	if (!IsValid())
		return globalError;

	allAnimationURIs.clear();

	const domLibrary_animations_Array& DomLibAnimArr = root->getLibrary_animations_array();
	if (DomLibAnimArr.getCount() == 0)
		return COLLADA_OK;

	// Add Animation URIs by "animation.txt"
	(void)EnumSkeletalAnimationsFromDefinitionFile(allAnimationURIs, "animation.txt");

	if (allAnimationURIs.empty()) // if "animation.txt" not available, add entire animation as a whole
	{
		// only one animation (all <library_animations> apply to entire sence)
		// Collada SPEC has animation_clips but it's not defined to be used in a Collada document (yet).
		allAnimationURIs.push_back("#" ANIM_URI_PREFIX ANIM_URI_ALLFRAMES);
	}

	return COLLADA_OK;
}

//! Construct bones for CSkeletonObject
//! Bone data will be extracted from state variable "skeleton", capped at rootBone.
int ColladaImporterImpl::LoadAssetSkeleton(const std::string& rootUri, CSkeletonObject& skeletonObject, int aFlags) {
	if (!IsValid())
		return globalError;

	std::string rootBone = AssetURI::GetNodeNameFromURIString(rootUri);

	ColladaJoint* pRootJoint = skeleton->findBone(rootBone);
	if (pRootJoint == NULL)
		return 0; // root bone not found in entire skeleton tree

	SkeletonConverter sc(*skeletonObject.getRuntimeSkeleton());
	sc.ConvertFrom(skeleton, pRootJoint, NULL, aFlags);
	return 1;
}

int ColladaImporterImpl::DoLoadMeshByName(const std::string& meshUri, KEP_ASSET_TYPE type, ColladaSubmesh** ppMesh, ImpFloatMatrix4& matrix) {
	*ppMesh = NULL;

	int subID = AssetURI::GetSubscriptFromURIString(meshUri);
	domNode* pDomNode = GetDomNodeByUri(meshUri);
	if (pDomNode == NULL)
		return COLLADA_NOTFOUND; // not found

	ColladaSubmesh* pSubmesh;
	if (type == KEP_SKA_SKINNED_MESH)
		pSubmesh = new ColladaSkinnedMesh();
	//	else if (type==KEP_SKA_RIGID_MESH) pSubmesh = new ColladaRigidMesh();
	else
		pSubmesh = new ColladaSubmesh();

	// Check all parent nodes to get the most recent UP_AXIS: <visual_scene> -> <library_visual_scene> -> <Collada>
	//@@Limitation: doesnot walk up another path to <instance_visual_scene> -> <scene> -> <collada>
	enumUPAXIS upAxis = GetMostRecentUpAxis(pDomNode);

	int ret;
	if (type == KEP_SKA_SKINNED_MESH)
		ret = ColladaUtil::ReadDomNode_Skinning(upAxis, pDomNode, subID, *skeleton, *(ColladaSkinnedMesh*)pSubmesh, matrix);
	else
		ret = ColladaUtil::ReadDomNode_Geoms(upAxis, pDomNode, subID, *pSubmesh, matrix);

	if (ret <= 0) {
		delete pSubmesh;
		return ret;
	}

	// If not described in the document, use Y_UP as default
	if (pSubmesh->getUpAxis() == CSM_UNK_UP)
		pSubmesh->setUpAxis(CSM_Y_UP);

	// swap vertices orders
	for (UINT faceLoop = 0; faceLoop < pSubmesh->getVertexCount() / 3; faceLoop++) {
		ColladaVertex v0 = pSubmesh->getVertex(faceLoop * 3);
		ColladaVertex v2 = pSubmesh->getVertex(faceLoop * 3 + 2);
		pSubmesh->setVertex(faceLoop * 3, v2);
		pSubmesh->setVertex(faceLoop * 3 + 2, v0);
	}
	*ppMesh = pSubmesh;
	return COLLADA_OK;
}

//! Load named asset mesh from Collada file.
//! @Param meshUri: name of the mesh as specified in DCC software. (In COLLADA, it's the name of the <node> element.)
//! @Param mesh: returns loaded mesh data.
//! Algorithm:
//!  - Start from <instance_geometry>, follow URL to <geometry> / <mesh> / <vertices> and <triangles>
//!  - @@Limitation: <polygons> not supported. [Yanfeng 09/2008]
//!  - @@Limitation: <polylist> must uses coplanar and convex polygons [Yanfeng 09/2008]
//!  - @@Todo: <polygons> [Yanfeng 09/2008]
//!  - @@Todo: non-coplanar polygons [Yanfeng 09/2008]
//!  - @@Todo: concave polygons [Yanfeng 09/2008]
int ColladaImporterImpl::LoadAssetMeshByName(const std::string& meshUri, CEXMeshObj& exmesh, int aFlags) {
	if (!IsValid())
		return globalError;

	ImpFloatMatrix4 matrix; // initialized as identity matrix
	ColladaSubmesh* tmpMesh;
	int ret = DoLoadMeshByName(meshUri, KEP_STATIC_MESH, &tmpMesh, matrix);
	if (ret <= 0)
		return ret; // not found or error

	// Construct CEXMeshObj
	StaticMeshConverter mc(&exmesh);

	// We want static mesh only. Collapse mesh of any type before construction.
	const ColladaSubmesh* pStaticMesh = tmpMesh->collapseToStaticMesh();
	if (pStaticMesh)
		mc.ConvertFrom(pStaticMesh, matrix, aFlags);
	else {
		//@@Todo: log error
		ASSERT(FALSE); //TBD
	}

	if (pStaticMesh && pStaticMesh != tmpMesh)
		delete pStaticMesh; // delete static mesh if newly created
	delete tmpMesh;
	return 1; // success
}

int ColladaImporterImpl::LoadAssetRigidMeshByName(const std::string& meshUri, CHierarchyVisObj& hVis, std::string& joint, int aFlags) {
	if (!IsValid())
		return globalError;

	ImpFloatMatrix4 matrix; // initialized as identity matrix
	ColladaSubmesh* tmpMesh;
	int ret = DoLoadMeshByName(meshUri, KEP_STATIC_MESH, &tmpMesh, matrix);
	if (ret <= 0)
		return ret; // not found or error

	// Construct CEXMeshObj
	RigidMeshConverter mc(hVis);
	mc.ConvertFrom(tmpMesh, matrix, aFlags);

	// Return joint name
	joint = GetJointNameByUri(meshUri);

	delete tmpMesh;
	return 1; // success
}

int ColladaImporterImpl::LoadAssetSkinnedMeshByName(const std::string& meshUri, CDeformableVisObj& dVis, const CSkeletonObject& aSkeletalObject, int aFlags) {
	if (!IsValid())
		return globalError;

	VERIFY_RETURN(skeleton != NULL, COLLADA_ERR_INTERNAL); // skeleton must already been loaded

	ImpFloatMatrix4 matrix; // initialized as identity matrix
	ColladaSubmesh* tmpMesh;
	int ret = DoLoadMeshByName(meshUri, KEP_SKA_SKINNED_MESH, &tmpMesh, matrix);
	if (ret <= 0)
		return ret; // not found or error

	// Construct CDeformableVisObj
	SkinnedMeshConverter sc(&dVis, aSkeletalObject.getRuntimeSkeleton());
	sc.ConvertFrom(tmpMesh, matrix, aFlags);

	delete tmpMesh;
	return COLLADA_OK;
}

int ColladaImporterImpl::LoadSkeletalAnimation(const std::string& animUri, CSkeletonObject* apRefSkeleton, CSkeletonAnimation& aNewAnimation, int aFlags, const AnimImportOptions* apOptions /*= NULL*/) {
	if (!IsValid())
		return globalError;

	std::string nodeName = AssetURI::GetNodeNameFromURIString(animUri);

	std::string animName;
	int firstFrame, lastFrame, fps;
	IAssetsDatabase::Instance()->ParseAnimationName(nodeName, animName, firstFrame, lastFrame, fps);
	if (fps == -1)
		fps = 30;

	//if (animNode!= ANIM_URI_PREFIX ANIM_URI_ALLFRAMES)	// only support one animation for entire scene
	//	return COLLADA_NOTFOUND;

	BOOL bAnimFound = FALSE;
	bool bErrorFound = false;

	std::lock_guard<fast_recursive_mutex> lock(animCacheSync);

	try {
		std::map<std::string, ColladaAnimCacheEntry*>::iterator itCache = animCache.find(animUri);

		if (itCache == animCache.end()) {
			const domLibrary_animations_Array& DomLibAnimArr = root->getLibrary_animations_array();
			for (UINT libLoop = 0; libLoop < DomLibAnimArr.getCount(); libLoop++) {
				const domAnimation_Array& DomAnimArr = DomLibAnimArr[libLoop]->getAnimation_array();
				for (UINT aniLoop = 0; aniLoop < DomAnimArr.getCount(); aniLoop++) {
					if (!bAnimFound) {
						bAnimFound = TRUE;
						itCache = animCache.insert(make_pair(animUri, new ColladaAnimCacheEntry(animName))).first;
					}

					ColladaUtil::ReadDomAnimation_Skeletal(DomAnimArr[aniLoop], itCache->second->animation, itCache->second->allSamplers, itCache->second->allMatrixSources);
				}
			}
		} else {
			bAnimFound = TRUE;
		}

		if (bAnimFound) {
			AnimationConverter ac(aNewAnimation);
			if (!ac.ConvertFrom(skeleton, &itCache->second->animation, firstFrame, lastFrame, fps, apRefSkeleton->getRuntimeSkeleton(), aFlags, apOptions)) // use defClassifyNodeSkeletal for now (should eventually be passed in from caller)
				bErrorFound = true; // conversion failed
		}
	} catch (...) {
		bErrorFound = true;
	}

	if (!bAnimFound)
		return COLLADA_NOTFOUND;

	if (bErrorFound)
		return COLLADA_ERR_BAD_FORMAT;

	return COLLADA_OK;
}

//! Load named material from COLLADA file.
//! @Param name: name of the material.
//! @Param material: return loaded material.
//! Algorithm:
//!  - Obtain <material> element by name
//!  - Trace through <material> / <instance_effect url="..."> ==> <library_effects> / <effect>
//!  - @@Limitation: ignored <instance_effect> / <technique_hint> and <setparam> [Yanfeng 09/2008]
//!  - @@Todo: read <technique_hint> and <setparam> [Yanfeng 09/2008]
int ColladaImporterImpl::LoadMaterialByName(const std::string& name, CMaterialObject* mat) {
	if (!IsValid())
		return globalError;

	int count = db->getElementCount(name.c_str(), "material");
	if (count == 0)
		return COLLADA_NOTFOUND;

	daeElement* pElement = NULL;
	daeInt ret = db->getElement(&pElement, 0);
	if (ret == DAE_ERR_QUERY_NO_MATCH)
		return 0; // no match
	if (ret != DAE_OK) {
		//@@Todo: log unexpected error
		ASSERT(FALSE); //TBD
		return -1;
	}

	// <material>
	domMaterial* pDomMat = dynamic_cast<domMaterial*>(pElement);
	if (pDomMat == NULL)
		return 0; // element found but not a material: considered as not found

	ASSERT(name == pDomMat->getName());

	ColladaMaterial colmat;
	ColladaUtil::ReadDomMaterial(pDomMat, colmat);

	mat->m_baseMaterial.Ambient = colmat.getColorByChannel(CM_AMBIENT);
	mat->m_baseMaterial.Diffuse = colmat.getColorByChannel(CM_DIFFUSE);
	mat->m_baseMaterial.Specular = colmat.getColorByChannel(CM_SPECULAR);
	mat->m_baseMaterial.Emissive = colmat.getColorByChannel(CM_EMISSIVE);
	mat->m_baseMaterial.Power = colmat.getFloatByChannel(CM_SHININESS) * 100; //@@Watch: semantic of shininess?
	return 1;
}

void ColladaImporterImpl::PrepareMeshForKEP(CEXMeshObj& mesh) {
	// Prepare CEXMeshObj
	UINT vCount = mesh.m_abVertexArray.GetVertexCount();
	int uvCount = mesh.m_abVertexArray.GetUVCount();

	for (UINT vLoop = 0; vLoop < vCount; vLoop++) {
		ABVERTEX v = mesh.m_abVertexArray.GetVertex(vLoop);

		// Flip TXTUV.tv (as in MAX Exporter)
		for (int uvChan = 0; uvChan < uvCount; uvChan++) {
			TXTUV* pTx = GetKEPTexCoordPtrByChannelID(&v, uvChan);
			if (pTx)
				pTx->tv = 1 - pTx->tv;
		}
		mesh.m_abVertexArray.SetVertex(vLoop, v);
	}

	// Use default value for POPOUT box and Smooth Angle Tolerance for now
	// @@Todo: add to user interface
	mesh.m_sightBasis = 5000;
	mesh.m_smoothAngle = 45;

	// Prepare associated material if exists
	if (mesh.m_materialObject)
		PrepareMaterialForKEP(mesh.m_materialObject);
}

void ColladaImporterImpl::PrepareSkeletonForKEP(CSkeletonObject& skeleton_) {
	// do nothing for now
}

void ColladaImporterImpl::PrepareRigidMeshForKEP(CHierarchyVisObj& hVis) {
	ASSERT(hVis.m_mesh != NULL);
	PrepareMeshForKEP(*hVis.m_mesh);
}

void ColladaImporterImpl::PrepareSkinnedMeshForKEP(CDeformableVisObj& dVis) {
	ASSERT(dVis.m_mesh != NULL);
	PrepareMeshForKEP(*dVis.m_mesh);
}

void ColladaImporterImpl::PrepareSkeletalAnimationForKEP(CSkeletonAnimation& aNewAnimation) // if aAnimationIndex==-1, prepare all animations
{
	// do nothing for now
}

// forward declaration of GetShortNames function (in XStatic)
void GetShortNames_CorrectVersion(const std::string* pathSource, const char* subDirSource, std::string* pathDest, std::string* subDirDest);

void ColladaImporterImpl::PrepareMaterialForKEP(CMaterialObject* mat) {
	// Assign "NONE" to unused texture slots
	for (int i = 0; i < 10; i++) {
		if (mat->m_texNameHash[i] == INVALID_TEXTURE_NAMEHASH) // not used
			mat->m_texFileName[i] = "";
		else {
			// Strip off path and only use file name
			// According to CMaterialDlg::ProcessTextureLevelChange, we should only specify file name.
			// The guess is engine will automatically find the appropriate path: either under MapsModels or inside per-zone folder.
			std::string orig = mat->m_texFileName[i];
			std::string tmp;
			GetShortNames_CorrectVersion(&orig, NULL, &tmp, NULL); // Once GetShortNames is fixed we will switch to the standard function
			mat->m_texFileName[i] = tmp;
		}
	}

	mat->CopyMaterial(mat->m_baseMaterial, &mat->m_useMaterial);
}

// Asset specs with multi-revisions (KZM v2/v3, Collada 1.3/1.4, etc)
BOOL ColladaImporterImpl::SupportsFormat(const std::string& extension, float ver) {
	return TRUE;
}

BOOL ColladaImporterImpl::SupportsURI(const std::string& aURI) {
	return TRUE;
}

float ColladaImporterImpl::GetDocumentVersion(const std::string& aURI) {
	return 0;
}

ImpFloatMatrix4 ColladaImporterImpl::GetMatrixByURI(const std::string& meshUri) const {
	std::string nodeName = AssetURI::GetNodeNameFromURIString(meshUri);

	std::map<std::string, ImpFloatMatrix4>::const_iterator it = mapUriToMatrix.find(nodeName);
	if (it != mapUriToMatrix.end())
		return it->second;

	return ImpFloatMatrix4();
}

enumUPAXIS ColladaImporterImpl::GetUpAxisByURI(const std::string& meshUri) const {
	std::string nodeName = AssetURI::GetNodeNameFromURIString(meshUri);

	std::map<std::string, enumUPAXIS>::const_iterator it = mapUriToUpAxis.find(nodeName);
	if (it != mapUriToUpAxis.end())
		return it->second;

	return CSM_UNK_UP;
}

enumUPAXIS ColladaImporterImpl::GetMostRecentUpAxis(daeElement* pElem) {
	// Check all parent nodes to get the most recent UP_AXIS
	enumUPAXIS upAxis = CSM_UNK_UP;
	while (upAxis == CSM_UNK_UP && pElem) {
		upAxis = ColladaUtil::ReadDomUpAxis(pElem);
		pElem = pElem->getParent();
	}

	return upAxis;
}

domNode* ColladaImporterImpl::GetDomNodeByUri(const std::string& meshUri) {
	std::string nodeName = AssetURI::GetNodeNameFromURIString(meshUri);

	domNode* pDomNode = NULL;
	std::map<std::string, domNode*>::iterator it = mapUriToDomNode.find(nodeName);
	if (it != mapUriToDomNode.end())
		pDomNode = it->second;
	ASSERT(pDomNode != NULL);

	return pDomNode;
}

std::string ColladaImporterImpl::GetJointNameByUri(const std::string& meshUri) {
	std::string nodeName = AssetURI::GetNodeNameFromURIString(meshUri);

	std::map<std::string, std::string>::iterator it = mapUriToJointName.find(nodeName);
	if (it != mapUriToJointName.end())
		return mapUriToJointName[nodeName];

	return std::string();
}

int ColladaImporterImpl::EnumSkeletalAnimationsFromDefinitionFile(std::vector<std::string>& allAnimationURIs, const std::string& filename) {
	std::ifstream ifs(filename.c_str());

	std::string animDesc, animName;
	int firstFrame = 0, lastFrame = -1;

	if (!ifs.bad() && !ifs.fail()) {
		while (!ifs.eof()) {
			ifs >> animDesc >> animName >> firstFrame >> lastFrame;
			if (!animName.empty()) {
				AssetURI uri;
				uri.node = IAssetsDatabase::Instance()->MakeAnimationName(animName, firstFrame, lastFrame);
				allAnimationURIs.push_back(uri.ToURIString());
			}
		}

		return COLLADA_OK;
	}

	return COLLADA_NOTFOUND;
}

int ColladaImporterImpl::EnumVertexAnimations(std::vector<std::string>& allAnimationURIs, IClassifyAssetNode* classifyFunc) {
	if (!IsValid())
		return globalError;

	std::map<KEP_ASSET_TYPE, std::vector<std::string>> allAssetURIs;
	allAssetURIs.insert(std::pair<KEP_ASSET_TYPE, std::vector<std::string>>(KEP_VERTEX_ANIMATION, std::vector<std::string>()));
	int ret = DoEnumAssets(allAssetURIs, FALSE, classifyFunc);

	if (ret == COLLADA_OK)
		allAnimationURIs = allAssetURIs[KEP_VERTEX_ANIMATION];
	return ret;
}

int ColladaImporterImpl::LoadVertexAnimation(const std::string& animUri, CEXMeshObj& baseMesh, CVertexAnimationModule& targets) {
	if (!IsValid())
		return globalError;

	domNode* pDomNode = GetDomNodeByUri(animUri);
	if (pDomNode == NULL)
		return COLLADA_NOTFOUND;

	enumUPAXIS upAxis = GetMostRecentUpAxis(pDomNode);

	// Read <morph> element
	ImpFloatMatrix4 matrix;
	ColladaSubmeshWithMorphing* pMorphedMesh = new ColladaSubmeshWithMorphing();
	int ret = ColladaUtil::ReadDomNode_Morphing(upAxis, pDomNode, 0, *pMorphedMesh, matrix);

	if (ret <= 0) {
		delete pMorphedMesh;
		return ret;
	}

	// Look for related <animation> element
	BOOL bAnimFound = FALSE;
	ColladaVertexAnimation animation("");

	std::map<std::string, ColladaAnimFloatSampler*> allSamplers;
	std::map<std::string, SimpleArray<float>*> allWeightSources;

	const domLibrary_animations_Array& DomLibAnimArr = root->getLibrary_animations_array();
	for (UINT libLoop = 0; libLoop < DomLibAnimArr.getCount(); ++libLoop) {
		const domAnimation_Array& DomAnimArr = DomLibAnimArr[libLoop]->getAnimation_array();
		for (UINT aniLoop = 0; aniLoop < DomAnimArr.getCount(); ++aniLoop) {
			const domChannel_Array& DomChanArr = DomAnimArr[aniLoop]->getChannel_array();
			for (UINT chanLoop = 0; chanLoop < DomChanArr.getCount(); ++chanLoop) {
				domChannel* pDomChan = const_cast<domChannel*>(DomChanArr[chanLoop].cast());
				std::string target = pDomChan->getTarget();

				std::string attrName, sidPath;
				int attrIndex = -1;
				daeElement* pTarget = ColladaUtil::ResolveDomTarget(pDomChan->getDAE()->getDatabase(), target, sidPath, attrName, attrIndex);

				if (pTarget && pTarget == pMorphedMesh->getWeightSource()) {
					bAnimFound = TRUE;
					ColladaUtil::ReadDomAnimation_Vertex(DomAnimArr[aniLoop], animation, allSamplers, allWeightSources);
					break;
				}
			}
		}
	}

	if (!bAnimFound) {
		delete pMorphedMesh;
		return COLLADA_ERR_BAD_FORMAT;
	}

	VertexAnimationConverter vac(&baseMesh, &targets);
	vac.ConvertFrom(pMorphedMesh, matrix, animation);
	return COLLADA_OK;
}

void ColladaImporterImpl::PrepareVertexAnimationForKEP(CEXMeshObj& baseMesh, CVertexAnimationModule& targets) {
	// do nothing for now
}

bool ColladaImporterImpl::IsValid() {
	return globalError == COLLADA_OK;
}

void ColladaImporterImpl::GetSupportedOptions(std::set<std::string>& optionNames) {
	optionNames.clear();
	optionNames.insert(IMP_OPT_MAXFILESIZE);
}

bool ColladaImporterImpl::SetOption(const std::string& optionName, int intValue) {
	VERIFY_RETURN(pBaseImpl == NULL, false); // only call set option on base impl
	VERIFY_RETURN(importOptions != NULL, false);

	if (optionName == IMP_OPT_MAXFILESIZE) {
		importOptions->insert(make_pair(optionName, intValue));
		return true;
	}

	return false;
}

bool ColladaImporterImpl::GetOption(const std::string& optionName, int& intValue) {
	if (!pBaseImpl)
		return pBaseImpl->GetOption(optionName, intValue);

	VERIFY_RETURN(importOptions != NULL, false);

	std::map<std::string, int>::iterator findIt = importOptions->find(optionName);
	if (findIt == importOptions->end())
		return false;

	intValue = findIt->second;
	return true;
}

} // namespace KEP