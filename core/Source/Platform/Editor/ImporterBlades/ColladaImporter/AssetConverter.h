///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

struct AnimImportOptions;
class CSkeletonAnimation;

class MatrixConverter {
protected:
	Matrix44f& matrix;

public:
	MatrixConverter(Matrix44f& m) :
			matrix(m) {}
	virtual ~MatrixConverter() {}

	virtual void ConvertFrom(const ImpFloatMatrix4& impMatrix, enumUPAXIS upAxis, int flags = 0);
	const Matrix44f& GetMatrix() const { return matrix; }
};

class TransformConverter : public MatrixConverter {
protected:
	Vector3f &dirVector, &upVector, &position, &scales;
	Matrix44f tmpMatrix;

public:
	TransformConverter(Vector3f& dir, Vector3f& up, Vector3f& pos, Vector3f& s) :
			MatrixConverter(tmpMatrix), dirVector(dir), upVector(up), position(pos), scales(s) {}

	virtual void ConvertFrom(const ImpFloatMatrix4& impMatrix, enumUPAXIS upAxis, int flags = 0);
};

class MaterialConverter {
protected:
	CMaterialObject* pMaterial;
	static void initializeKEPMaterial(CMaterialObject* pMat);

public:
	MaterialConverter(CMaterialObject* pMat) :
			pMaterial(pMat) {}
	virtual void ConvertFrom(const ColladaMaterial* pCldMat, int flags = 0);
};

class MeshConverter {
public:
	MeshConverter() {}
	virtual ~MeshConverter() {}
	virtual void ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, int flags = 0) = 0;
};

class StaticMeshConverter : public MeshConverter {
protected:
	CEXMeshObj* pExMesh;

protected:
	// Copy texture coordinates (a hack for shared map channels -- need to be got rid of in the future)
	BOOL CopyTexCoords(int oldChan, int newChan);

	//! Add a number of triangles to the mesh
	//! Vertex count must be multiple of 3: 1st = (V0, V1, V2), 2nd = (V3, V4, V5), ...
	//! Return true if added
	bool AddTriangles(const ColladaSubmesh* pCldMesh);

	void ConstructVertex(const ColladaSubmesh* pCldMesh, const ColladaVertex& cv, Vertex& vtx);

public:
	StaticMeshConverter(CEXMeshObj* m = NULL) :
			pExMesh(m) {}
	virtual ~StaticMeshConverter() {}
	virtual void ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, int flags = 0);
};

class RigidMeshConverter : public StaticMeshConverter {
protected:
	CHierarchyVisObj& hVis;

public:
	RigidMeshConverter(CHierarchyVisObj& v) :
			hVis(v) {}
	virtual ~RigidMeshConverter() {}
	virtual void ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, int flags = 0);
};

class SkinnedMeshConverter : public StaticMeshConverter {
protected:
	CDeformableVisObj* pDVis;
	const RuntimeSkeleton* pSkeleton;

public:
	SkinnedMeshConverter(CDeformableVisObj* v, const RuntimeSkeleton* s) :
			pDVis(v), pSkeleton(s) {}
	virtual ~SkinnedMeshConverter() {}
	virtual void ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, int flags = 0);
};

class SkeletonConverter {
protected:
	RuntimeSkeleton& skeleton;

public:
	SkeletonConverter(RuntimeSkeleton& aSkeleton) :
			skeleton(aSkeleton) {}
	virtual ~SkeletonConverter() {}
	virtual void ConvertFrom(ColladaSkeleton* pCldSkeleton, ColladaJoint* pRootJoint, RuntimeSkeleton* pRefSkeleton = NULL, int flags = 0, const std::map<std::string, ImpFloatVector3>* pRootOffsets = NULL);
};

class AnimationConverter {
protected:
	CSkeletonAnimation& newAnimation;

public:
	AnimationConverter(CSkeletonAnimation& aNewAnimation) :
			newAnimation(aNewAnimation) {}
	virtual ~AnimationConverter() {}

	virtual bool ConvertFrom(const ColladaSkeleton* pCldSkeleton, const ColladaSkeletalAnimation* pCldAnim, int firstFrame = 0, int lastFrame = -1, double fps = 30.0, RuntimeSkeleton* pRefSkeleton = NULL, int flags = 0, const AnimImportOptions* pOptions = NULL);
};

class VertexAnimationConverter : public StaticMeshConverter {
protected:
	CVertexAnimationModule* pVtxAnim;

public:
	VertexAnimationConverter(CEXMeshObj* m, CVertexAnimationModule* va) :
			StaticMeshConverter(m), pVtxAnim(va) {}
	virtual ~VertexAnimationConverter() {}

	virtual void ConvertFrom(const ColladaSubmesh* pCldMesh, const ImpFloatMatrix4& matrix, const ColladaVertexAnimation& animation, int firstFrame = 0, int lastFrame = -1, double fps = 30.0, int flags = 0);
};

} // namespace KEP
