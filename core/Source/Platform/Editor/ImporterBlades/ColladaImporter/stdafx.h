// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#define _AFXDLL
#include "afxwin.h"

#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <algorithm>
#include <sstream>
#include <fstream>

#include <KEPDiag.h>
#include <js.h>

#include "log4cplus/logger.h"
using namespace log4cplus;

#include "tcl/tree.h"
using namespace tcl;

#include "dae.h"
#include "1.4\dom\domConstants.h"
#include "1.4\dom\domCOLLADA.h"
#include "1.4\dom\domVisual_scene.h"
#include "1.4\dom\domInstance_geometry.h"
#include "1.4\dom\domGeometry.h"
#include "1.4\dom\domSource.h"
#include "1.4\dom\domEffect.h"
#include "1.4\dom\domProfile_COMMON.h"
#include "1.4\dom\domInstance_controller.h"

#if defined(_DEBUG) && defined(_TIMING)

#define TIMING_START(name) \
	DWORD __start_time_##name = TickCount();	\
	const char* __timing_name_##name = #name;

#define TIMING_END(name) \
{							\
	CStringA __timing_msg;	\
	__timing_msg.Format("%s: %d ms\n", __timing_name_##name, TickCount()-__start_time_##name);	\
	TRACE(__timing_msg);	\
}

#else

#define TIMING_START(x)
#define TIMING_END(x)

#endif

#define INVALID_POINTER ((void*)-1)
