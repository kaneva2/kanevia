///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ColladaAssert.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "CEXMeshClass.h"
#include "ColladaClasses.h"
#include "IAssetsDatabase.h"
#include "ColladaUtil.h"
#include "AssetConverter.h"

using namespace std;

namespace KEP {

//! \brief Read skinned mesh and matrix from <node>
//! \return 1 if succeeded, 0 if not found, <0 for error codes
int ColladaUtil::ReadDomNode_Skinning(enumUPAXIS parentUpAxis, domNode* pDomNode, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh, ImpFloatMatrix4& matrix) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomNode);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	//@@Limitation: only support one <instance_controller> inside <node>
	domInstance_controller_Array& DomInstCntlrArr = pDomNode->getInstance_controller_array();
	if (DomInstCntlrArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT; // return error - as checked earlier there should be at least one instance_controller

	int ret = ReadDomInstance_Controller_Skinning(upAxis, DomInstCntlrArr[0], subID, skeleton, skinnedMesh);
	if (ret <= 0)
		return ret;

	ImpFloatMatrix4 currMatrix = ReadDomNodeTransformation(pDomNode);
	matrix *= currMatrix;
	return COLLADA_OK;
}

int ColladaUtil::ReadDomInstance_Controller_Skinning(enumUPAXIS parentUpAxis, domInstance_controller* pDomInstCntlr, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomInstCntlr);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	xsAnyURI uri = pDomInstCntlr->getUrl();
	domController* pDomCntlr = dynamic_cast<domController*>(uri.getElement().cast());
	if (pDomCntlr == NULL)
		return COLLADA_ERR_BAD_FORMAT; // bad format: <controller> is required

	int ret = ReadDomController_Skinning(upAxis, pDomCntlr, subID, skeleton, skinnedMesh);
	if (ret <= 0)
		return ret;

	// Read <bind_material> element
	domBind_material* pDomBindMat = pDomInstCntlr->getBind_material();
	if (pDomBindMat)
		ReadDomBindMaterial(pDomBindMat, skinnedMesh); // @@Watch: Ignore error

	return COLLADA_OK;
}

int ColladaUtil::ReadDomController_Skinning(enumUPAXIS parentUpAxis, domController* pDomCntlr, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomCntlr);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	domSkin* pDomSkin = pDomCntlr->getSkin();
	if (pDomSkin == NULL)
		return COLLADA_ERR_BAD_FORMAT; // bad format: <skin> is required

	return ReadDomSkin(upAxis, pDomSkin, subID, skeleton, skinnedMesh);
}

int ColladaUtil::ReadDomSkin(enumUPAXIS parentUpAxis, domSkin* pDomSkin, int subID, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomSkin);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	xsAnyURI uri = pDomSkin->getSource();
	domGeometry* pDomGeom = dynamic_cast<domGeometry*>(uri.getElement().cast());
	if (pDomGeom == NULL)
		return COLLADA_ERR_NOT_SUPPORTED; // bad format or unsupported source--source must be <geometry> for now

	// Read mesh data
	int ret = ReadDomGeometry(upAxis, pDomGeom, subID, skinnedMesh);
	if (ret <= 0)
		return ret;

	// Read bind shape matrix
	ImpFloatMatrix4 bsm;
	domSkin::domBind_shape_matrix* pDomBSM = pDomSkin->getBind_shape_matrix();
	domFloat4x4& floatArr = pDomBSM->getValue();
	ReadDomMatrix(floatArr, bsm);
	skinnedMesh.setBSM(bsm);

	// Clear data
	skinnedMesh.clearInvertedBindMatrices();
	skinnedMesh.deleteWeightsArray();

	// Read Inverted bind matrices
	domSkin::domJoints* pDomJoints = pDomSkin->getJoints();
	ret = ReadDomSkin_IBMs(pDomJoints, skeleton, skinnedMesh);
	if (ret <= 0)
		return ret;

	// Read vertex weights
	domSkin::domVertex_weights* pDomVtxWts = pDomSkin->getVertex_weights();
	ret = ReadDomSkin_VertexWeights(pDomVtxWts, skeleton, skinnedMesh);
	if (ret <= 0)
		return ret;

	return COLLADA_OK;
}

int ColladaUtil::ReadDomSkin_IBMs(domSkin::domJoints* pDomJoints, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh) {
	domInputLocal_Array& DomInputArr = pDomJoints->getInput_array();
	if (DomInputArr.getCount() < 2)
		return COLLADA_ERR_BAD_FORMAT; // at least two inputs

	int jointInputId = -1, ibmInputId = -1;

	//@@Todo: use previously parsed input array

	for (UINT i = 0; i < DomInputArr.getCount(); i++) {
		xsNMTOKEN sem = DomInputArr[i]->getSemantic();

		if (MatchxsNMToken(sem, "JOINT")) {
			ASSERT(jointInputId == -1);
			jointInputId = i;
		} else if (MatchxsNMToken(sem, "INV_BIND_MATRIX")) {
			ASSERT(ibmInputId == -1);
			ibmInputId = i;
		}
	}

	if (jointInputId == -1 || ibmInputId == -1)
		return COLLADA_ERR_BAD_FORMAT;

	domURIFragmentType uriSourceJoint = DomInputArr[jointInputId]->getSource();
	domSource* pDomSourceJoint = dynamic_cast<domSource*>(uriSourceJoint.getElement().cast());
	if (pDomSourceJoint == NULL)
		return COLLADA_ERR_BAD_FORMAT;
	domName_array* pDomNames = pDomSourceJoint->getName_array();
	if (pDomNames == NULL)
		return COLLADA_ERR_BAD_FORMAT;

	domURIFragmentType uriSourceIBM = DomInputArr[ibmInputId]->getSource();
	domSource* pDomSourceIBM = dynamic_cast<domSource*>(uriSourceIBM.getElement().cast());
	if (pDomSourceIBM == NULL)
		return COLLADA_ERR_BAD_FORMAT;
	domFloat_array* pDomFloats = pDomSourceIBM->getFloat_array();
	if (pDomFloats == NULL)
		return COLLADA_ERR_BAD_FORMAT;

	for (UINT jointLoop = 0; jointLoop < pDomNames->getCount(); jointLoop++) {
		//@@Todo: read data by technique/accessor

		const ColladaJoint* pJoint = skeleton.findBoneBySIDConst(pDomNames->getValue()[jointLoop]);
		if (pJoint == NULL)
			return COLLADA_ERR_BAD_FORMAT;

		ImpFloatMatrix4 ibm;
		if (!ReadDomMatrix(pDomFloats->getValue(), ibm, jointLoop * 16)) {
			skinnedMesh.clearInvertedBindMatrices();
			return COLLADA_ERR_BAD_FORMAT;
		}

		skinnedMesh.addInvertedBindMatrix(pJoint, ibm);
	}

	return COLLADA_OK;
}

int ColladaUtil::ReadDomSkin_VertexWeights(domSkin::domVertex_weights* pDomVtxWts, const ColladaSkeleton& skeleton, ColladaSkinnedMesh& skinnedMesh) {
	domInputLocalOffset_Array& DomInputOfsArr = pDomVtxWts->getInput_array();
	if (DomInputOfsArr.getCount() < 2)
		return COLLADA_ERR_BAD_FORMAT; // at least two inputs

	ColladaInputOffset *pJointInputOfs = NULL, *pWeightInputOfs = NULL;

	auto pInputArr = ParseDomInputArray(DomInputOfsArr, skinnedMesh);
	if (!pInputArr)
		return COLLADA_ERR_BAD_FORMAT;

	//@@Limitation: only handle WEIGHT semantic--might have others but not explicitly defined in SPEC
	for (UINT i = 0; i < pInputArr->GetInputCount(); i++) {
		if (pInputArr->GetInput(i)->semantic == CIS_JOINT) {
			ASSERT(pJointInputOfs == NULL);
			pJointInputOfs = pInputArr->GetInput(i);
		} else if (pInputArr->GetInput(i)->semantic == CIS_WEIGHT) {
			ASSERT(pWeightInputOfs == NULL);
			pWeightInputOfs = pInputArr->GetInput(i);
		}
	}

	if (pJointInputOfs == NULL || pWeightInputOfs == NULL)
		return COLLADA_ERR_BAD_FORMAT;

	domSource* pDomSourceJoint = dynamic_cast<domSource*>(pJointInputOfs->source);
	if (pDomSourceJoint == NULL)
		return COLLADA_ERR_BAD_FORMAT;
	domName_array* pDomNames = pDomSourceJoint->getName_array();
	if (pDomNames == NULL)
		return COLLADA_ERR_BAD_FORMAT;

	domSource* pDomSourceWeight = dynamic_cast<domSource*>(pWeightInputOfs->source);
	if (pDomSourceWeight == NULL)
		return COLLADA_ERR_BAD_FORMAT;
	domFloat_array* pDomFloats = pDomSourceWeight->getFloat_array();
	if (pDomFloats == NULL)
		return COLLADA_ERR_BAD_FORMAT;

	domSkin::domVertex_weights::domVcount* pDomVCount = pDomVtxWts->getVcount();
	domSkin::domVertex_weights::domV* pDomV = pDomVtxWts->getV();
	if (pDomVCount == NULL || pDomV == NULL)
		return COLLADA_ERR_BAD_FORMAT;

	UINT vtxCount = pDomVtxWts->getCount();
	ASSERT(pDomVCount->getValue().getCount() <= vtxCount);

	skinnedMesh.createWeightsArray(vtxCount);

	UINT baseOfs = 0, globalTrace = 0;
	for (UINT vLoop = 0; vLoop < vtxCount; vLoop++) {
		UINT jointCnt = pDomVCount->getValue()[vLoop];
		if (globalTrace + jointCnt > pDomV->getValue().getCount())
			return COLLADA_ERR_BAD_FORMAT; // not enough data

		for (UINT jointLoop = 0; jointLoop < jointCnt; jointLoop++, globalTrace++, baseOfs += pInputArr->GetStride()) {
			UINT jointIdx = pDomV->getValue()[baseOfs + pJointInputOfs->offset];
			UINT weightIdx = pDomV->getValue()[baseOfs + pWeightInputOfs->offset];

			if (jointIdx == -1) {
				ASSERT(FALSE);
				//@@Todo: support special weights
			} else {
				if (pDomNames->getValue().getCount() <= jointIdx || pDomFloats->getValue().getCount() <= weightIdx)
					return COLLADA_ERR_BAD_FORMAT;

				const ColladaJoint* pJoint = skeleton.findBoneBySIDConst(pDomNames->getValue()[jointIdx]);
				if (pJoint == NULL)
					return COLLADA_ERR_BAD_FORMAT;

				float weight = pDomFloats->getValue()[weightIdx];
				skinnedMesh.addWeight(vLoop, pJoint, weight);
			}
		}
	}

	return COLLADA_OK;
}

int ColladaUtil::ReadDomNode_Morphing(enumUPAXIS parentUpAxis, domNode* pDomNode, int subID, ColladaSubmeshWithMorphing& morphedMesh, ImpFloatMatrix4& matrix) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomNode);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	//@@Limitation: only support one <instance_controller> inside <node>
	domInstance_controller_Array& DomInstCntlrArr = pDomNode->getInstance_controller_array();
	if (DomInstCntlrArr.getCount() == 0)
		return COLLADA_ERR_BAD_FORMAT; // return error - as checked earlier there should be at least one instance_controller

	int ret = ReadDomInstance_Controller_Morphing(upAxis, DomInstCntlrArr[0], subID, morphedMesh);
	if (ret <= 0)
		return ret;

	ImpFloatMatrix4 currMatrix = ReadDomNodeTransformation(pDomNode);
	matrix *= currMatrix;
	return COLLADA_OK;
}

int ColladaUtil::ReadDomInstance_Controller_Morphing(enumUPAXIS parentUpAxis, domInstance_controller* pDomInstCntlr, int subID, ColladaSubmeshWithMorphing& morphedMesh) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomInstCntlr);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	xsAnyURI uri = pDomInstCntlr->getUrl();
	domController* pDomCntlr = dynamic_cast<domController*>(uri.getElement().cast());
	if (pDomCntlr == NULL)
		return COLLADA_ERR_BAD_FORMAT; // bad format: <controller> is required

	int ret = ReadDomController_Morphing(upAxis, pDomCntlr, subID, morphedMesh);
	if (ret <= 0)
		return ret;

	// Read <bind_material> element
	domBind_material* pDomBindMat = pDomInstCntlr->getBind_material();
	if (pDomBindMat)
		ReadDomBindMaterial(pDomBindMat, morphedMesh); // @@Watch: Ignore error

	return COLLADA_OK;
}

int ColladaUtil::ReadDomController_Morphing(enumUPAXIS parentUpAxis, domController* pDomCntlr, int subID, ColladaSubmeshWithMorphing& morphedMesh) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomCntlr);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	domMorph* pDomMorph = pDomCntlr->getMorph();
	if (pDomMorph == NULL)
		return COLLADA_ERR_BAD_FORMAT; // bad format: <morph> is required

	return ReadDomMorph(upAxis, pDomMorph, subID, morphedMesh);
}

int ColladaUtil::ReadDomMorph(enumUPAXIS parentUpAxis, domMorph* pDomMorph, int subID, ColladaSubmeshWithMorphing& morphedMesh) {
	// Update UpAxis from current <node>, use parent value if not defined
	enumUPAXIS upAxis = ReadDomUpAxis(pDomMorph);
	if (upAxis == CSM_UNK_UP)
		upAxis = parentUpAxis;

	xsAnyURI uri = pDomMorph->getSource();
	domGeometry* pDomGeom = dynamic_cast<domGeometry*>(uri.getElement().cast());
	if (pDomGeom == NULL)
		return COLLADA_ERR_NOT_SUPPORTED; // bad format or unsupported source--source must be <geometry> for now

	// Read mesh data
	int ret = ReadDomGeometry(upAxis, pDomGeom, subID, morphedMesh);
	if (ret <= 0)
		return ret;

	// Read targets
	domMorph::domTargets* pDomMorphTargets = pDomMorph->getTargets();
	domInputLocal_Array& DomInputLocalArr = pDomMorphTargets->getInput_array();

	daeIDRef* idRefs = NULL;
	float* weights = NULL;
	UINT idRefCount = 0;
	UINT weightCount = 0;

	// Get all <input>'s
	for (DWORD arrIdx = 0; arrIdx < DomInputLocalArr.getCount(); arrIdx++) {
		domInputLocal* pDomInput = DomInputLocalArr[arrIdx];
		xsNMTOKEN sem = pDomInput->getSemantic();

		domURIFragmentType& uriSource = pDomInput->getSource();
		domSource* pDomSource = dynamic_cast<domSource*>(uriSource.getElement().cast());
		if (pDomSource == NULL)
			return COLLADA_ERR_BAD_FORMAT;

		//@@Limitation: only support one MORPH_TARGET and one MORPH_WEIGHT
		if (MatchStringIgnoreCase(sem, "MORPH_TARGET") && idRefs == NULL) {
			SimpleArray<daeIDRef> allIDRefs;
			if (ReadAllIDRefs(allIDRefs, pDomSource) > 0)
				idRefs = allIDRefs.detachBuffer(idRefCount);
		} else if (MatchStringIgnoreCase(sem, "MORPH_WEIGHT") && weights == NULL) {
			morphedMesh.setWeightSource(pDomSource);
			SimpleArray<float> allWeights;
			if (ReadAllFloats(allWeights, pDomSource) > 0)
				weights = allWeights.detachBuffer(weightCount);
		}
	}

	BOOL errorFound = FALSE;

	if (idRefCount != 0 && idRefCount == weightCount) {
		map<string, ColladaSubmesh*> loadedTargets;
		for (UINT i = 0; i < idRefCount; i++) {
			ColladaSubmesh* pTargetMesh = NULL;
			map<string, ColladaSubmesh*>::iterator findTarget = loadedTargets.find(idRefs[i].getID());
			if (findTarget == loadedTargets.end()) {
				domGeometry* pSubDomGeom = dynamic_cast<domGeometry*>(idRefs[i].getElement());
				if (!pSubDomGeom) {
					errorFound = TRUE;
					continue;
				}

				//@@Limitation: only first submesh is handled (not compatible with multi-material mesh)
				pTargetMesh = new ColladaSubmesh();
				ReadDomGeometry(parentUpAxis, pSubDomGeom, 0, *pTargetMesh);
			} else // use existing
			{
				pTargetMesh = findTarget->second;
			}

			morphedMesh.addTargetWeight(pTargetMesh, weights[i]);
		}
		return COLLADA_OK;
	}

	if (idRefs != NULL)
		delete[] idRefs;
	if (weights != NULL)
		delete[] weights;
	return COLLADA_ERR_BAD_FORMAT;
}

} // namespace KEP