///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "CEXMeshClass.h"
#include "CBoneClass.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "CMaterialClass.h"
#include "IClientEngine.h"
#include "ReMeshCreate.h"
#include "ColladaAssert.h"
#include "ColladaClasses.h"
#include "colladaimporter.h"
#include "IAssetImporterImpl.h"
#include "AssetTree.h"
#include "ColladaAssert.h"
#include "Common/AssetsDatabase/AssetURI.h"
#include "ColladaUtil.h"
#include "CSkeletonAnimation.h"
#include "SkeletalAnimationManager.h"
#include "AssetConverter.h"
#include "SkeletonConfiguration.h"
#include "KEPFileNames.h"
#include "UnicodeWindowsHelpers.h"
#include "AnimationProxy.h"

#include "LogHelper.h"
static LogInstance("Instance");

using namespace KEP;

static bool HashDeformableVisObj(CDeformableVisObj* pDVis, int lod);
static bool HashDeformableMesh(CDeformableMesh* pDMesh, CDeformableVisList* lodChain);

//! Registers the importer with the aAssetDatabase and does any initialization required
void ColladaImporter::Register() {
	IAssetImporterFactory::Instance()->RegisterImporter(this, KEP_STATIC_MESH, "dae");
	IAssetImporterFactory::Instance()->RegisterImporter(this, KEP_SKA_SKELETAL_OBJECT, "dae");
	IAssetImporterFactory::Instance()->RegisterImporter(this, KEP_SKA_ANIMATION, "dae");
	IAssetImporterFactory::Instance()->RegisterImporter(this, KEP_VERTEX_ANIMATION, "dae");
	if (pBaseImpl)
		pBaseImpl->Register();
}

//! Unregisters the importer, doing any necessary cleanup
void ColladaImporter::Unregister() {
	if (pBaseImpl)
		pBaseImpl->Unregister();
	IAssetImporterFactory::Instance()->UnregisterImporter(this);
}

BOOL ColladaImporter::SupportsColladaVersion(IAssetImporterImpl* pImpl, float ver) {
	return pImpl->SupportsFormat(NULL, ver);
}

float ColladaImporter::GetColladaDocumentVersion(IAssetImporterImpl* pImpl, const std::string& aURI) {
	return pImpl->GetDocumentVersion(aURI);
}

IAssetImporterImpl* ColladaImporter::GetBaseImpl(const std::string& aURI) {
	std::set<IAssetImporterImpl*> allImpls = IAssetImporterFactory::Instance()->GetImporterImpl(this);
	for (std::set<IAssetImporterImpl*>::iterator it = allImpls.begin(); it != allImpls.end(); ++it) {
		if ((*it)->SupportsURI(aURI))
			return (*it);
	}

	return NULL;
}

IAssetImporterImpl* ColladaImporter::GetSessionImpl(const std::string& aURI) {
	// Check if already spawned
	std::map<std::string, IAssetImporterImpl*>::iterator findIt = allSessions.find(aURI);
	if (findIt != allSessions.end())
		return findIt->second;

	return NULL;
}

BOOL isdigits(const std::string& str) {
	for (std::string::const_iterator it = str.begin(); it != str.end(); ++it)
		if (!isdigit(*it))
			return FALSE;

	return TRUE;
}

BOOL isenclosed(const std::string& str) {
	return str.length() >= 2 && str[0] == _T('{') && str[str.length() - 1] == _T('}');
}

BOOL ismesh(const char& ch) {
	return toupper(ch) != _T('M') && toupper(ch) != _T('F'); // World object / Static mesh
}

BOOL isLOD(const char& ch) {
	return isdigit(ch) || toupper(ch) == _T('C'); // lod level or collision mesh
}

static char SEPCH = '_';
static char* SEPSTR = "_";

BOOL isSEP(const char& ch) {
	return ch == SEPCH;
}

int parseLOD(const char& ch) {
	if (isdigit(ch))
		return ch - _T('0'); // LOD 0 to 9
	if (toupper(ch) == _T('C'))
		return 10; // LOD 10: collision mesh
	return -1; // unknown LOD
}

BOOL ColladaImporter::BuildAssetTree(IAssetImporterImpl* pImpl, const std::vector<std::string>& meshURIs, const std::string& rootUri, KEP_ASSET_TYPE assetType, AssetTree& assetTree) {
	assetTree.get()->SetRootURI(rootUri);

	for (std::vector<std::string>::const_iterator it = meshURIs.begin(); it != meshURIs.end(); ++it) {
		const std::string& sURI = *it;
		AssetURI uri(sURI);
		int submeshCount = uri.subscript;

		if (uri.node.empty())
			continue;

		std::string stem, section, dimVar, part;
		int lod;
		char type;

		BOOL bParsingOK = IAssetsDatabase::Instance()->ParseAssetName(assetType, uri.node, type, stem, section, dimVar, lod);

		if (type != 0)
			stem = type + ("_" + stem); //@@Watch: Prefix type for now
		//if (ismesh(type) && !dimVar.empty()) {
		if (assetType == KEP_STATIC_MESH && !dimVar.empty()) {
			stem = stem + "_" + dimVar; //@@Watch: postfix variation to static mesh
			dimVar.clear();
		}

		std::string stemTag, sectionTag, varTag, lodTag;

		// find top-level node
		stemTag = stem;
		AssetData anObject(assetType, AssetData::LVL_OBJECT, stemTag, stem);
		anObject.SetRootURI(rootUri);
		AssetTree::iterator stemIt = assetTree.find(anObject);

		if (stemIt == assetTree.end()) { // Not yet exists
			//insert as top-level
			stemIt = assetTree.insert(anObject);
		}

		//find next level
		//for world object: part (if exists) then LOD
		//for skeletal object: section, variation, then LOD
		AssetTree::iterator sectionIt;
		sectionTag = stemTag;
		if (!section.empty())
			sectionTag += "_" + section;

		AssetData anSection = AssetData(assetType, AssetData::LVL_SECTION, sectionTag, sectionTag); // note: unlike other nodes, section name is prefixed with object name
		anSection.SetRootURI(rootUri);
		sectionIt = stemIt.node()->find(anSection);
		if (sectionIt == stemIt.node()->end()) // not found
			sectionIt = stemIt.node()->insert(anSection);

		AssetTree::iterator varIt;
		varTag = sectionTag;
		if (!dimVar.empty())
			varTag += "_" + dimVar;

		AssetData anVar(assetType, AssetData::LVL_VARIATION, varTag, dimVar);
		anVar.SetRootURI(rootUri);
		varIt = sectionIt.node()->find(anVar);
		if (varIt == sectionIt.node()->end()) // not found
			varIt = sectionIt.node()->insert(anVar);

		if (lod < 0)
			lod = 0;
		if (lod > 10)
			lod = 10; // Valid LODs: 0 to 9
		AssetData anLOD(assetType, AssetData::LVL_LOD, uri.node, uri.node);
		if (varIt.node()->find(anLOD) == varIt.node()->end()) // not found
		{
			anLOD.SetLODLevel(lod);
			anLOD.SetURI(uri.GetNodeURI());
			anLOD.SetRootURI(rootUri);
			anLOD.SetNumSubmeshes(submeshCount);
			anLOD.SetMatrix(pImpl->GetMatrixByURI(anLOD.GetURI()));
			anLOD.SetUpAxis(pImpl->GetUpAxisByURI(anLOD.GetURI()));
			if (!bParsingOK)
				anLOD.SetFlag(AssetData::WRN_NAMING); // Mark for naming convention mismatch

			// All members must be assigned before insertion (AssetData will be copied during insertion)
			varIt.node()->insert(anLOD);
		}
	}
	return true;
}

BOOL ColladaImporter::BuildBoneTree(IAssetImporterImpl* pImpl, ColladaSkeleton& ske, const ColladaJoint* pJoint, const std::string& rootUri, AssetTree& assetTree) {
	VERIFY_RETURN(pJoint != NULL, FALSE);

	assetTree.get()->SetRootURI(rootUri);

	while (pJoint) {
		AssetData asset = AssetData(KEP_SKA_SKELETAL_OBJECT, AssetData::LVL_FLAT, pJoint->getName(), pJoint->getName(), pJoint->getUri());
		asset.SetRootURI(rootUri);
		AssetTree::iterator it = assetTree.insert(asset);

		ColladaJoint* pChild = ske.getFirstChild(pJoint->getName());
		if (pChild)
			BuildBoneTree(pImpl, ske, pChild, rootUri, *it.node());

		pJoint = ske.getNextSibling(pJoint->getName());
	}

	return TRUE;
}

AssetTree* ColladaImporter::PreviewAssets(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags, IClassifyAssetNode* classifyFunc) {
	std::map<KEP_ASSET_TYPE, std::vector<std::string>> allMeshURIs;
	ColladaSkeleton* pAllSkeletons = NULL;
	std::vector<std::string> allAnimationURIs;

	AssetTree* assetTree = NULL;
	int rc = 0;

	// 1. Find an appropriate impl class
	IAssetImporterImpl* pImpl = GetSessionImpl(aURI);
	VERIFY_RETURN(pImpl != NULL, NULL);

	// 2. Enumerate assets in aURI
	switch (aAssetType) {
		case KEP_STATIC_MESH:
			allMeshURIs[KEP_STATIC_MESH] = std::vector<std::string>(); // Loaded with STATIC mesh only
			rc = pImpl->EnumAssetMeshes(allMeshURIs, classifyFunc);
			break;

		case KEP_SKA_SKELETAL_OBJECT:
			allMeshURIs[KEP_STATIC_MESH] = std::vector<std::string>();
			allMeshURIs[KEP_SKA_SKINNED_MESH] = std::vector<std::string>();
			allMeshURIs[KEP_SKA_RIGID_MESH] = std::vector<std::string>();
			rc = pImpl->EnumAssetMeshesWithSkeletons(allMeshURIs, &pAllSkeletons, classifyFunc);
			if (rc == COLLADA_OK || rc == COLLADA_NOTFOUND)
				rc = pImpl->EnumSkeletalAnimations(allAnimationURIs, classifyFunc);
			break;

		case KEP_SKA_SECTION_MESHES:
			//@@Watch: section mesh has broken for while. Disable it for now.
			ASSERT(FALSE);
			break;

		case KEP_SKA_SKINNED_MESH:
			allMeshURIs[KEP_SKA_SKINNED_MESH] = std::vector<std::string>();
			rc = pImpl->EnumAssetMeshesWithSkeletons(allMeshURIs, &pAllSkeletons, classifyFunc);
			break;

		case KEP_SKA_RIGID_MESH:
			//@@Todo: import rigid mesh only (not used so far?)
			break;

		case KEP_SKA_ANIMATION:
			// We don't need mesh, only skeleton and animation. Leave allMeshURIs empty.
			rc = pImpl->EnumAssetMeshesWithSkeletons(allMeshURIs, &pAllSkeletons, classifyFunc);
			if (rc == COLLADA_OK || rc == COLLADA_NOTFOUND)
				rc = pImpl->EnumSkeletalAnimations(allAnimationURIs, classifyFunc);
			break;

		case KEP_VERTEX_ANIMATION:
			rc = pImpl->EnumVertexAnimations(allAnimationURIs, classifyFunc);
			break;

		default:
			break;
	}

	// 3. Build asset map
	assetTree = new AssetTree(AssetData(aAssetType, AssetData::LVL_ALL, "*", "", aURI));
	assetTree->get()->SetRootURI(aURI);

	// 3.a Check return codes
	if (rc != COLLADA_OK && rc != COLLADA_NOTFOUND) {
		// error returned, create a dummy tree with an error node
		AssetData errorNode(KEP_ERROR, AssetData::LVL_ALL); // a global error
		switch (rc) {
			case COLLADA_ERR_BAD_FORMAT:
				errorNode.SetFlag(AssetData::ERR_BADDATA);
				break;

			case COLLADA_ERR_NOT_SUPPORTED:
				errorNode.SetFlag(AssetData::ERR_NOTSUPP);
				break;

			case COLLADA_ERR_FILESIZE:
				errorNode.SetFlag(AssetData::ERR_FILESIZE);
				break;

			case COLLADA_ERR_COLLADADOM:
			case COLLADA_ERR_MEMORY:
			default:
				errorNode.SetFlag(AssetData::ERR_INTERNAL);
				break;
		}

		assetTree->insert(errorNode);
	} else {
		// 3.b Pack all meshes into asset tree
		for (std::map<KEP_ASSET_TYPE, std::vector<std::string>>::iterator it = allMeshURIs.begin(); it != allMeshURIs.end(); ++it) {
			std::vector<std::string>& meshURIs = allMeshURIs[it->first];
			if (meshURIs.size() > 0) {
				std::string typeName, typeDesc;
				AssetData::GetAssetGroupNameFromType(it->first, typeName, typeDesc);

				AssetTree::iterator it2 = assetTree->insert(AssetData(aAssetType, AssetData::LVL_GROUP, typeName, typeDesc));
				BuildAssetTree(pImpl, meshURIs, aURI, aAssetType, *it2.node());
			}
		}

		// 4. Pack all skeletons into asset tree
		if (pAllSkeletons && pAllSkeletons->getBoneCount() > 0) {
			std::string typeName, typeDesc;
			AssetData::GetAssetGroupNameFromType(KEP_SKA_SKELETAL_OBJECT, typeName, typeDesc);
			AssetTree::iterator it = assetTree->insert(AssetData(KEP_SKA_SKELETAL_OBJECT, AssetData::LVL_GROUP, typeName, typeDesc));
			for (const ColladaJoint* root = pAllSkeletons->getFirstRoot(); root != NULL; root = pAllSkeletons->getNextSibling(root->getName()))
				BuildBoneTree(pImpl, *pAllSkeletons, root, aURI, *it.node());
		}

		// 5. Pack all animations into asset tree
		if (!allAnimationURIs.empty()) {
			KEP_ASSET_TYPE animationType = aAssetType == KEP_VERTEX_ANIMATION ? KEP_VERTEX_ANIMATION : KEP_SKA_ANIMATION;
			std::string typeName, typeDesc;
			AssetData::GetAssetGroupNameFromType(animationType, typeName, typeDesc);
			AssetTree::iterator itGrp = assetTree->insert(AssetData(animationType, AssetData::LVL_GROUP, typeName, typeDesc));

			for (std::vector<std::string>::iterator it = allAnimationURIs.begin(); it != allAnimationURIs.end(); ++it) {
				std::string sURI = *it;
				AssetURI uri(sURI);

				// Parse animation name for animation information: first/last frame number, fps
				std::string animName;
				int firstFrame, lastFrame, fps;
				IAssetsDatabase::Instance()->ParseAnimationName(uri.node, animName, firstFrame, lastFrame, fps);

				AssetData asset(animationType, AssetData::LVL_FLAT, uri.node, animName, uri.GetNodeURI());
				asset.SetRootURI(aURI);
				asset.SetNumSubmeshes(uri.subscript); // sub mesh count for vertex animations
				itGrp.node()->insert(asset);
			}
		}
	}

	// 6. Return
	return assetTree;
}

BOOL ColladaImporter::SupportAssetURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType) {
	// Fragment asset files are no longer supported in new importers. Use KEP_SKA_SKELETAL_OBJECT instead.
	if (aAssetType == KEP_SKA_SKINNED_MESH || aAssetType == KEP_SKA_RIGID_MESH || aAssetType == KEP_SKA_SECTION_MESHES)
		return FALSE;

	return GetBaseImpl(aURI) != NULL;
}

bool ColladaImporter::Import(CEXMeshObj& aStaticMesh, const std::string& aURI, AssetData* aDef, int aFlags /*= IMP_STATIC_LEGACY_FLAGS*/) {
	IAssetImporterImpl* pImpl;
	pImpl = GetSessionImpl(aDef->GetRootURI());
	VERIFY_RETURN(pImpl != NULL, false);

	int ret = pImpl->LoadAssetMeshByName(aURI, aStaticMesh, aFlags);
	if (ret <= 0)
		return false;

	pImpl->PrepareMeshForKEP(aStaticMesh);
	return true;
}

bool ColladaImporter::Import(CSkeletonObject& aSkeletalObject, const std::string& aURI, AssetTree* aTree, int aFlags /*= IMP_SKELETAL_LEGACY_FLAGS*/) {
	ASSERT(aTree != NULL); // asset tree is required for Skeletal Object import in ColladaImporter
		// aURI is actually not used because caller might want to import multiple meshes at a time.

	// clear name of the skeletal object
	aSkeletalObject.getRuntimeSkeleton()->setSkeletonName("");

	// incoming AssetTree must contains exactly one SKELETON subtree.
	if (!ImportSkeleton(aSkeletalObject, aTree, aFlags))
		return false;

	if ((aFlags & IMP_SKELETAL_MESHTYPEMASK) != IMP_SKELETAL_NOMESH) {
		// Import skeletal meshes
		if (!ImportSkeletalMeshes(aSkeletalObject, aTree, aFlags)) {
			LogError("ImportSkeletalMeshes() FAILED - '" << aSkeletalObject.getRuntimeSkeleton()->getSkeletonName() << "'");
			return false;
		}
	}

	if ((aFlags & IMP_SKELETAL_ANIMMASK) != IMP_SKELETAL_NOANIM) {
		std::vector<int> allAnimGLIDs;
		if (!ImportSkeletalAnimations(aSkeletalObject, allAnimGLIDs, aTree, aFlags)) {
			// no animation found
			// do nothing now
		}

		for (std::vector<int>::iterator it = allAnimGLIDs.begin(); it != allAnimGLIDs.end(); ++it) {
			int animGLID = *it;
			auto pAP = AnimationRM::Instance()->GetAnimationProxy(animGLID, true);
			if (!pAP)
				continue;

			aSkeletalObject.getRuntimeSkeleton()->queueAnimForLoad(animGLID);
			return true;
		}
	}

	return true;
}

bool ColladaImporter::Import(CSkeletonObject& aSkeletalObject, const kstringVector& aURIVector, AssetTree* aTree, int aFlags /*= IMP_SKELETAL_LEGACY_FLAGS*/) {
	if (aURIVector.size() > 1) {
		// One URI per Skeletal Object only -- multiple URIs means multiple skeletal objects for COLLADA
		ASSERT(FALSE);
	}

	Import(aSkeletalObject, aURIVector[0], aTree, aFlags);

	return false;
}

bool ColladaImporter::ImportSkinnedMesh(CSkeletonObject& aSkeletalObject, const std::string& aURI, int aSectionIndex, int aConfigIndex, int aLodIndex, int aFlags, AssetData* aDef) {
	ASSERT(FALSE);
	VERIFY_RETURN(aDef != NULL, false); // Asset definition must be provided for ColladaImporters

	SkeletonConfiguration* skeletalCfg = aSkeletalObject.getSkeletonConfiguration();

	POSITION sectionPos = skeletalCfg->m_meshSlots->FindIndex(aSectionIndex);
	if (sectionPos == NULL)
		return false;

	CAlterUnitDBObject* currSection = dynamic_cast<CAlterUnitDBObject*>(skeletalCfg->m_meshSlots->GetAt(sectionPos));
	VERIFY_RETURN(currSection != NULL, false);
	VERIFY_RETURN(currSection->m_configurations != NULL, false);

	POSITION configPos = currSection->m_configurations->FindIndex(aConfigIndex);
	if (configPos == NULL)
		return FALSE;

	CDeformableMesh* currConfig = dynamic_cast<CDeformableMesh*>(currSection->m_configurations->GetAt(configPos));
	VERIFY_RETURN(currConfig != NULL, false);

	CDeformableVisList* currLodList = currConfig->GetDeformableVisList();
	VERIFY_RETURN(currLodList != NULL, false);

	AssetData& asset = *aDef;

	if (!DoImportSkinnedMeshAsset(aSkeletalObject, NULL, asset, currSection, currConfig, currLodList, aFlags))
		return FALSE;

	asset.SetFlag(AssetData::STA_IMPORTED);
	HashDeformableMesh(currConfig, currLodList);

	//@@Temporary: to be moved to somewhere else
	skeletalCfg->m_charConfig.initBaseItem(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount());

	return TRUE;
}

bool ColladaImporter::ImportRigidMesh(RuntimeSkeleton& aSkeletalObject, const std::string& aURI, int aBoneIndex, int aLodIndex, int aFlags, AssetData* aDef) {
	//@@Todo: import rigid mesh only (not used so far?)
	ASSERT(FALSE);
	return false;
}

bool ColladaImporter::ImportAnimation(CSkeletonObject& aSkeletalObject, const std::string& aURI, int& aAnimGLID, int aFlags, AssetData* aDef /*= NULL*/, const AnimImportOptions* apOptions /*= NULL*/) {
	IAssetImporterImpl* pImpl = GetSessionImpl(aDef->GetRootURI());
	VERIFY_RETURN(pImpl != NULL, false);

	CSkeletonAnimation* pNewAnimation = new CSkeletonAnimation;
	pNewAnimation->m_animGlid = aAnimGLID;

	int ret = pImpl->LoadSkeletalAnimation(aURI, &aSkeletalObject, *pNewAnimation, aFlags, apOptions);
	if (ret == COLLADA_OK) {
		pImpl->PrepareSkeletalAnimationForKEP(*pNewAnimation);
		aAnimGLID = pNewAnimation->m_animGlid;

		auto pAP = AnimationRM::Instance()->GetAnimationProxy(aAnimGLID, true, true);
		if (pAP != NULL) {
			aSkeletalObject.getRuntimeSkeleton()->queueAnimForLoad(aAnimGLID); // Ready immediately
			assert(pAP->GetAnimDataPtr() != nullptr && pAP->GetAnimDataPtr()->m_ppBoneAnimationObjs != nullptr);
			return true;
		}
	}

	delete pNewAnimation;
	return false;
}

//! Traverse asset tree to determine which skeleton to import
//! Problem: selected assets might has relationships to bones of more than one skeleton.
//! Three solutions:
//!    1) Report an error
//!    2) Use virtual root to link skeletons into a big one
//!    3) Merge skeleton by bone name (by trimming root name from bone names)
//! Although solution 3 might be want we need. However, two skeletons might not be easily merged. There would be naming conflicts, hierarchy conflicts and etc.
//! Currently we only allow one Collada skeleton to be selected for one KEP Skeletal Object.
bool ColladaImporter::ImportSkeleton(CSkeletonObject& aSkeletalObject, AssetTree* aTree, int aFlags) {
	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return false;

	AssetTree* pSkeletonGroup = AssetData::GetAssetGroup(aTree, KEP_SKA_SKELETAL_OBJECT);
	if (pSkeletonGroup == NULL && (aFlags & IMP_SKELETAL_USEDUMMYBONE) != 0) {
		ASSERT(aSkeletalObject.getRuntimeSkeleton()->m_pBoneList == NULL || aSkeletalObject.getRuntimeSkeleton()->m_pBoneList->IsEmpty());

		// insert a dummy bone to skeleton
		Vector3f dirVector, upVector, position, scales;
		CBoneObject* pBone = NULL;
		dirVector.x = 1.0f;
		dirVector.y = dirVector.z = 0.0f;
		upVector.y = 1.0f;
		upVector.x = upVector.z = .0f;
		position.x = position.y = position.z = 0.0f;
		scales.x = scales.y = scales.z = 1.0f;

		aSkeletalObject.getRuntimeSkeleton()->addBone("__ROOT__", "", dirVector, upVector, position, scales, &pBone);

		// Initialize bone hierarchy
		aSkeletalObject.getRuntimeSkeleton()->buildHierarchy();

		// Call initBones to prepare inverted matrices and etc.
		aSkeletalObject.getRuntimeSkeleton()->initBones();
		return true;
	}

	//ASSERT(pSkeletonGroup!=NULL);
	if (pSkeletonGroup == NULL) {
		return false;
	}

	// Must have exactly one selected SKELETON in the group
	for (AssetTree::iterator itSkeleton = pSkeletonGroup->begin(); itSkeleton != pSkeletonGroup->end(); ++itSkeleton) {
		AssetData& skeletonRoot = *itSkeleton;

		if (!skeletonRoot.CheckFlag(AssetData::STA_SELECTED))
			continue; // Skip not-selected nodes

		IAssetImporterImpl* pImpl = GetSessionImpl(skeletonRoot.GetRootURI());
		ASSERT(pImpl != NULL);

		if (pImpl) {
			if (pImpl->LoadAssetSkeleton(skeletonRoot.GetURI(), aSkeletalObject, aFlags) <= 0)
				return FALSE;

			pImpl->PrepareSkeletonForKEP(aSkeletalObject);
			MeshRM::Instance()->SetCurrentBoneList(aSkeletalObject.getRuntimeSkeleton()->getBoneList());
			return TRUE;
		}
	}

	return FALSE;
}

bool ColladaImporter::DoImportRigidMeshAsset(CSkeletonObject& aSkeletalObject, AssetData& asset, int aFlags) {
	if (asset.GetLevel() == AssetData::LVL_LOD) {
		if (!asset.CheckFlag(AssetData::STA_SELECTED))
			return false; // Skip not-selected nodes

		std::string sURI = asset.GetURI();
		AssetURI uri(sURI);
		ASSERT(uri.isValid == TRUE);

		IAssetImporterImpl* pImpl = GetSessionImpl(asset.GetRootURI());
		VERIFY_RETURN(pImpl != NULL, false); // Skip if no session

		int numSubmeshes = asset.GetNumSubmeshes();

		// Get parent matrix of current node
		Matrix44f parentMatrix;
		parentMatrix.MakeIdentity();
		MatrixConverter mc(parentMatrix);
		mc.ConvertFrom(asset.GetMatrix(), asset.GetUpAxis());

		// Additional argument to be passed to IAssetImporterImpl (for stateful processing)
		for (int subID = 0; subID < numSubmeshes || numSubmeshes == -1; subID++) // If numSubmeshes==-1, keep looping until no more results
		{
			int ret = 0;

			CHierarchyVisObj* pHVis = new CHierarchyVisObj();
			std::string joint;
			uri.subscript = subID;
			ret = pImpl->LoadAssetRigidMeshByName(uri.ToURIString(), *pHVis, joint, aFlags);

			if (joint.empty()) {
				// orphan mesh: use root bone
				CBoneObject* pRootBone = aSkeletalObject.getRuntimeSkeleton()->getBoneByIndex(aSkeletalObject.getRuntimeSkeleton()->getRootBoneIndex());
				ASSERT(pRootBone != NULL);
				if (pRootBone)
					joint = pRootBone->m_boneName;
			}

			if (ret > 0) {
				pImpl->PrepareRigidMeshForKEP(*pHVis);

				// Concatenate parent matrix (combined matrices since bone node) to local matrix
				// asset->GetMatrix should actually returns relative matrix between current node and nearest parent bone (see ColladaUtil::PreviewDomNode)
				Matrix44f localMatrix = pHVis->m_mesh->GetWorldTransform();
				Matrix44f matrix = localMatrix * parentMatrix;
				pHVis->m_mesh->SetWorldTransform(matrix);

				CBoneObject* pBone;
				if (aSkeletalObject.getRuntimeSkeleton()->getBoneByName(joint, &pBone)) {
					// Bone found
					ASSERT(pBone != NULL);
					CStringA meshName = uri.GetAssetName().c_str();
					if (numSubmeshes > 1)
						meshName.Format("%s#%d", uri.GetAssetName().c_str(), subID);

					CHierarchyMesh* pHMesh = pBone->GetRigidMeshByName(meshName);

					if (pHMesh == NULL) {
						pHMesh = new CHierarchyMesh();
						pHMesh->m_name = meshName;
						pBone->m_rigidMeshes.push_back(pHMesh);
					}

					CHierarchyVisList* pLodChain = pHMesh->m_lodChain;
					if (pLodChain == NULL)
						pHMesh->m_lodChain = pLodChain = new CHierarchyVisList();

					while (pLodChain->GetCount() <= asset.GetLODLevel())
						pLodChain->AddTail((CObject*)NULL); // Leave spaces for lower LODs if necessary

					POSITION posLOD = pLodChain->FindIndex(asset.GetLODLevel());
					CObject* pOld = pLodChain->GetAt(posLOD);
					if (pOld != NULL) {
						//@@Todo: warning lod already exists
						// dispose old LOD
						CHierarchyVisObj* pOldMesh = dynamic_cast<CHierarchyVisObj*>(pOld);
						ASSERT(pOldMesh != NULL); // must be of the right type
						pOldMesh->SafeDelete();
						delete pOldMesh;
					}
					pLodChain->SetAt(posLOD, pHVis);
				} else {
					//@@Todo: log error
					delete pHVis;
				}
			}

			if (numSubmeshes == -1 && ret == 0)
				break; // Exit loop if submesh count is unknown and pImpl found no more submeshes

			if (ret > 0) {
				asset.SetFlag(AssetData::STA_IMPORTED); // import successfully
			} else {
				//@@Todo: show warning/confirmation message if possible

				//@@Debug {{
				CStringA msg;
				msg.Format("Unable to import: %s, returned: %d", asset.GetURI().c_str(), ret);
				MessageBoxUtf8(0, msg, "TRACE", MB_OK);
				//@@}}

				continue;
			}
		}
	}

	return true;
}

bool HashDeformableMesh(CDeformableMesh* pDMesh, CDeformableVisList* lodChain) {
	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return false;

	if (!pDMesh)
		return false;

	MeshRM::Instance()->CreateReMaterials(pDMesh, IMPORTED_TEXTURE);
	pDMesh->m_lodCount = lodChain->GetCount();
	if (pDMesh->m_lodCount <= 0) {
		LogError("lodChain->GetCount() FAILED");
		return false;
	}

	pDMesh->m_lodHashes = new UINT64[pDMesh->m_lodCount];
	int lcnt = 0;
	;
	for (POSITION posLoc = lodChain->GetHeadPosition(); posLoc != NULL;) {
		CDeformableVisObj* dVis = (CDeformableVisObj*)lodChain->GetNext(posLoc);
		const ReMesh* reMesh = dVis->GetReMesh();
		if (!reMesh) {
			LogError("FAILED GetReMesh()");
			return false;
		}

		pDMesh->m_lodHashes[lcnt] = reMesh ? reMesh->GetHash() : 0;
		lcnt++;
	}
	return true;
}

bool HashDeformableVisObj(CDeformableVisObj* pDVis, int lod) {
	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return false;

	if (!pDVis)
		return false;

	// optimize the vertex buffers
	pDVis->Optimize();

	// set LOD number so that ReMeshCreate will use proper Wpv
	MeshRM::Instance()->SetCurrentLOD(lod);

	// create a new ReMesh the legacy way....
	// Add it to the dynamic mesh pool.  This is only for skinned
	// dynamic objects.
	pDVis->SetReMesh(MeshRM::Instance()->CreateReSkinMeshPersistent(pDVis, false, MeshRM::RMP_ID_DYNAMIC));

	return true;
}

bool ColladaImporter::DoImportSkinnedMeshAsset(CSkeletonObject& aSkeletalObject, const AssetTree* node, AssetData& asset, CAlterUnitDBObject*& currSection, CDeformableMesh*& currConfig, CDeformableVisList*& currLodList, int aFlags) {
	switch (asset.GetLevel()) {
		case AssetData::LVL_OBJECT:
			//Note: if user selected meshes of one skeletal object from asset list, this check point will only be hit one once.
			// As we allow selection of assets across original 3D skeletal objects(which will be merged into once final object),
			// we need to make sure previous m_meshSlots will be reused for subsequent TYPE_OBJECT's.
			if (!aSkeletalObject.getSkeletonConfiguration()->m_meshSlots) {
				aSkeletalObject.getSkeletonConfiguration()->m_meshSlots = new CAlterUnitDBObjectList();
				currSection = NULL;
			}

			//Assigning name to skeletal object (use name of the first selected character if merging multiple characters)
			if (aSkeletalObject.getRuntimeSkeleton()->getSkeletonName().empty())
				aSkeletalObject.getRuntimeSkeleton()->setSkeletonName(asset.GetName());
			break;

		case AssetData::LVL_SECTION: {
			CAlterUnitDBObjectList* pAllSections = aSkeletalObject.getSkeletonConfiguration()->m_meshSlots;
			ASSERT(pAllSections != NULL);
#ifdef _DEBUG
			// Double-check to make sure current section hasn't been created (DEBUG only)
			POSITION pos = pAllSections->GetHeadPosition();
			while (pos != NULL) {
				CAlterUnitDBObject* pSection = dynamic_cast<CAlterUnitDBObject*>(pAllSections->GetNext(pos));
				ASSERT(pSection != NULL);
				ASSERT(!pSection->m_name.IsEmpty());

				if (pSection->m_name == asset.GetName().c_str())
					break;
			}

			ASSERT(pos == NULL); // should be not found
#endif
			currSection = new CAlterUnitDBObject();
			currSection->m_name = asset.GetName().c_str();
			currSection->m_configurations = new CDeformableMeshList();
			currSection->m_meshNames = new CStringA[node->size()];
			//@@Todo: more attributes?

			pAllSections->AddTail(currSection);
			aSkeletalObject.getRuntimeSkeleton()->setSkinnedMeshCount(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount() + 1);
			break;
		}

		case AssetData::LVL_VARIATION: {
			ASSERT(currSection != NULL);

			CDeformableMeshList* pAllConfigs = currSection->m_configurations;
			ASSERT(pAllConfigs != NULL);

			if (currConfig != NULL) {
				ASSERT(currLodList != NULL);
				HashDeformableMesh(currConfig, currLodList);
				currConfig = NULL;
				currLodList = NULL;
			}

#ifdef _DEBUG
			// Double-check to make sure current config hasn't been created (DEBUG only)
			POSITION pos = pAllConfigs->GetHeadPosition();
			while (pos != NULL) {
				CDeformableMesh* pDMesh = dynamic_cast<CDeformableMesh*>(pAllConfigs->GetNext(pos));
				ASSERT(pDMesh != NULL);

				if (pDMesh->m_dimName == asset.GetName().c_str())
					break;
			}

			ASSERT(pos == NULL); // should be not found
#endif
			currConfig = new CDeformableMesh();
			currConfig->m_dimName = asset.GetName().c_str();
			currLodList = new CDeformableVisList();
			currConfig->SetDeformableVisList(currLodList);
			//@@Todo: more attributes?

			pAllConfigs->AddTail(currConfig);
			if (asset.GetName().empty())
				currSection->m_meshNames[currSection->m_meshCount] = "Unnamed";
			else
				currSection->m_meshNames[currSection->m_meshCount] = asset.GetName().c_str();
			currSection->m_meshCount++;
			break;
		}

		case AssetData::LVL_LOD: {
			if (!asset.CheckFlag(AssetData::STA_SELECTED))
				return false; // Skip not-selected nodes

			std::string sURI = asset.GetURI();
			AssetURI uri(sURI);

			IAssetImporterImpl* pImpl = GetSessionImpl(asset.GetRootURI());
			VERIFY_RETURN(pImpl != NULL, false); // Skip if no session

			int numSubmeshes = asset.GetNumSubmeshes();

			// Additional argument to be passed to IAssetImporterImpl (for stateful processing)

			ASSERT(currConfig != NULL);
			ASSERT(currLodList != NULL);

			auto meshName = asset.GetName();
			ASSERT(currConfig->m_dimName.IsEmpty() || meshName == (const char*)currConfig->m_dimName);
			if (currConfig->m_dimName.IsEmpty()) {
				currConfig->m_dimName = meshName.c_str();
			}

			while (currLodList->GetCount() <= asset.GetLODLevel())
				currLodList->AddTail((CObject*)NULL); // Leave spaces for lower LODs if necessary

			for (int subID = 0; subID < numSubmeshes || numSubmeshes == -1; subID++) // If numSubmeshes==-1, keep looping until no more results
			{
				int ret = 0;
				CDeformableVisObj* pDVis = new CDeformableVisObj();

				uri.subscript = subID;
				ret = pImpl->LoadAssetSkinnedMeshByName(uri.ToURIString(), *pDVis, aSkeletalObject, aFlags);
				if (ret > 0) {
					pImpl->PrepareSkinnedMeshForKEP(*pDVis);

					//Copy material from exmesh to dmesh::m_supportedMaterials
					if (currConfig->m_supportedMaterials == NULL)
						currConfig->m_supportedMaterials = new CObList();
					if (pDVis->m_mesh->m_materialObject != NULL) {
						CMaterialObject* newMat = NULL;
						pDVis->m_mesh->m_materialObject->Clone(&newMat);
						currConfig->m_supportedMaterials->AddTail(newMat);
					}

					//Copy material to dmesh::m_material
					if (currConfig->m_material == NULL && pDVis->m_mesh->m_materialObject != NULL)
						pDVis->m_mesh->m_materialObject->Clone(&currConfig->m_material);

					HashDeformableVisObj(pDVis, asset.GetLODLevel());

					POSITION posLOD = currLodList->FindIndex(asset.GetLODLevel());
					CObject* pOld = currLodList->GetAt(posLOD);
					if (pOld != NULL) {
						//@@Todo: warning lod already exists
						// dispose old LOD
						CDeformableVisObj* pOldMesh = dynamic_cast<CDeformableVisObj*>(pOld);
						ASSERT(pOldMesh != NULL); // must be of the right type
						delete pOldMesh;
					}
					currLodList->SetAt(posLOD, pDVis);
				} else {
					delete pDVis;
					pDVis = NULL;
				}

				if (numSubmeshes == -1 && ret == 0)
					break; // Exit loop if submesh count is unknown and pImpl found no more submeshes

				if (ret > 0) {
					asset.SetFlag(AssetData::STA_IMPORTED); // import successfully
				} else {
					//@@Todo: show warning/confirmation message if possible

					//@@Debug {{
					CStringA msg;
					msg.Format("Unable to import: %s, returned: %d", asset.GetURI().c_str(), ret);
					MessageBoxUtf8(0, msg, "TRACE", MB_OK);
					//@@}}

					continue;
				}
			}

			break;
		} // case
	} //switch(asset.GetType()&ANS_TYPE_MASK)

	return TRUE;
}

bool ColladaImporter::ImportSkeletalMeshes(CSkeletonObject& aSkeletalObject, AssetTree* aTree, int aFlags) {
	for (AssetTree::iterator itGroup = aTree->begin(); itGroup != aTree->end(); ++itGroup) // check top-level nodes for asset groups
	{
		AssetData& group = *itGroup;
		AssetTree* pGroupNode = itGroup.node();

		// Skip skeleton groups
		KEP_ASSET_TYPE meshType = AssetData::GetAssetTypeByGroupName(group.GetKey());
		if (meshType == KEP_SKA_SKELETAL_OBJECT)
			continue;

		CAlterUnitDBObject* currSection = NULL;
		CDeformableMesh* currConfig = NULL;
		CDeformableVisList* currLodList = NULL;

		for (AssetTree::pre_order_iterator it = pGroupNode->pre_order_begin(); it != pGroupNode->pre_order_end(); ++it) {
			AssetData& asset = *it;
			if (!asset.CheckFlag(AssetData::STA_SELECTED))
				continue; // Skip not-selected nodes

			switch (meshType) {
				case KEP_SKA_RIGID_MESH:
					if ((aFlags & IMP_SKELETAL_RIGIDMESHES) == IMP_SKELETAL_RIGIDMESHES) {
						if (DoImportRigidMeshAsset(aSkeletalObject, asset, aFlags))
							asset.SetFlag(AssetData::STA_IMPORTED);
					}
					break;

				case KEP_SKA_SKINNED_MESH:
					if ((aFlags & IMP_SKELETAL_SKINNEDMESHES) == IMP_SKELETAL_SKINNEDMESHES) {
						if (DoImportSkinnedMeshAsset(aSkeletalObject, it.node(), asset, currSection, currConfig, currLodList, aFlags))
							asset.SetFlag(AssetData::STA_IMPORTED);
					}
					break;
				case KEP_STATIC_MESH:
					if ((aFlags & IMP_SKELETAL_ORPHANMESHES) != 0) {
						if (DoImportRigidMeshAsset(aSkeletalObject, asset, aFlags))
							asset.SetFlag(AssetData::STA_IMPORTED);
					}
					break;
			}
		}

		if (currConfig != NULL) {
			if (!currLodList) {
				LogError("currLodList is null");
				return FALSE;
			}

			if (!HashDeformableMesh(currConfig, currLodList)) {
				LogError("HashDeformableMesh() FAILED");
				return FALSE;
			}
		}
	}

	aSkeletalObject.getSkeletonConfiguration()->m_charConfig.initBaseItem(aSkeletalObject.getRuntimeSkeleton()->getSkinnedMeshCount());

	return TRUE;
}

bool ColladaImporter::ImportSkeletalAnimations(CSkeletonObject& aSkeletalObject, std::vector<int> allAnimGLIDs, AssetTree* aTree, int aFlags) {
	AssetTree* pAnimGroup = AssetData::GetAssetGroup(aTree, KEP_SKA_ANIMATION);
	if (pAnimGroup == NULL)
		return false;

	for (AssetTree::iterator it = pAnimGroup->begin(); it != pAnimGroup->end(); ++it) {
		AssetData* pAnimData = &*it;
		int animGLID = GLID_INVALID;
		if (ImportAnimation(aSkeletalObject, pAnimData->GetURI(), animGLID, aFlags, pAnimData))
			allAnimGLIDs.push_back(animGLID);
	}

	return true;
}

bool ColladaImporter::ImportVertexAnimation(CEXMeshObj& aMesh, CVertexAnimationModule& aAnim, const std::string& aURI, AssetData* aDef /*= NULL*/) {
	VERIFY_RETURN(aDef != NULL, false);

	IAssetImporterImpl* pImpl = GetSessionImpl(aDef->GetRootURI());
	VERIFY_RETURN(pImpl != NULL, false);

	int ret = pImpl->LoadVertexAnimation(aURI, aMesh, aAnim);
	if (ret <= 0)
		return false;

	pImpl->PrepareVertexAnimationForKEP(aMesh, aAnim);
	return true;
}

bool ColladaImporter::BeginSession(const std::string& aURI, int aFlags) {
	// Check if already spawned
	if (GetSessionImpl(aURI) != NULL)
		return true;

	IAssetImporterImpl* pImplBase = GetBaseImpl(aURI);
	if (pImplBase) {
		IAssetImporterImpl* pImpl = pImplBase->spawn(aURI, aFlags);
		allSessions[aURI] = pImpl;
		return true;
	} else {
		return false;
	}
}

bool ColladaImporter::EndSession(const std::string& aURI) {
	// Check if already spawned
	std::map<std::string, IAssetImporterImpl*>::iterator findIt = allSessions.find(aURI);
	VERIFY_RETURN(findIt != allSessions.end(), false);

	delete findIt->second;
	allSessions.erase(findIt);
	return true;
}

void ColladaImporter::GetSupportedOptions(std::set<std::string>& optionNames) {
	// delegate to impl class
	if (pBaseImpl)
		pBaseImpl->GetSupportedOptions(optionNames);
	else
		optionNames.clear();
}

bool ColladaImporter::SetOption(const std::string& optionName, int intValue) {
	if (!pBaseImpl)
		return false;

	return pBaseImpl->SetOption(optionName, intValue);
}

bool ColladaImporter::GetOption(const std::string& optionName, int& intValue) {
	if (!pBaseImpl)
		return false;

	return pBaseImpl->GetOption(optionName, intValue);
}
