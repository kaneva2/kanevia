/******************************************************************************
 OgreXMLmesh_parse_handler.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "BaseHandlers.h"
#include "OgreXMLMesh.h"

using namespace KEP::OgreXML;
using namespace KEP::OgreXML::mesh;

///////////////////////////////////////////////////////////////////////////////
/// ParseOgreXMLMeshHandler: A Xerces content handler
///		that parses an Ogre3D mesh XML file
///////////////////////////////////////////////////////////////////////////////
class ParseOgreXMLMeshHandler : public ProgressiveScanHandler {
public:
	//! Constuctor / destructor
	//@{
	ParseOgreXMLMeshHandler() :
			m_success(false), m_mesh(0), m_currGeom(0), m_currVBuff(0), m_currVBuffSize(0), m_curr_tc_idx(0), m_curr_boneassignments(0), m_curr_faces(0) {
		ResetDone();
		InitHandlersTable();
		InitAttrStrings();
	}

	virtual ~ParseOgreXMLMeshHandler() {
		ReleaseAttrStrings();
		if (m_mesh)
			delete m_mesh;
	}
	//@}

	//! Loaded successfully?
	bool Success(void) { return m_success; }

	//! Get the pointer to the mesh, transfering ownership. The caller must free the object.
	meshType* Get_mesh() {
		meshType* ret = m_mesh;
		m_mesh = 0;
		return ret;
	}

	//! Implementation of the ContentHandler API
	//@{
	void startDocument();

	void endDocument();

	void startElement(const XMLCh* const uri, const XMLCh* const localname,
		const XMLCh* const qname, const Attributes& attrs);

	void endElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

	//@}

protected:
//! OgreXML mesh element/node handlers
//@{
#undef declareHandlers
#define declareHandlers(str)                                                                                                         \
	void handleStart_##str(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const Attributes& attrs); \
	void handleEnd_##str(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

	declareHandlers(mesh)
		declareHandlers(sharedgeometry)
			declareHandlers(vertexbuffer)
				declareHandlers(vertex)
					declareHandlers(position)
						declareHandlers(normal)
							declareHandlers(colour_diffuse)
								declareHandlers(colour_specular)
									declareHandlers(texcoord)
										declareHandlers(submeshes)
											declareHandlers(submesh)
												declareHandlers(faces)
													declareHandlers(face)
														declareHandlers(geometry)
															declareHandlers(skeletonlink)
																declareHandlers(boneassignments)
																	declareHandlers(vertexboneassignment)
																		declareHandlers(levelofdetail)
																			declareHandlers(lodmanual)
																				declareHandlers(lodgenerated)
																					declareHandlers(lodfacelist)
																						declareHandlers(submeshnames)
																							declareHandlers(submeshname)
		//@}

		//! OgreXML lookup tables for handlers
		//@{
		typedef void (ParseOgreXMLMeshHandler::*startElementPtr)(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const Attributes& attrs);
	typedef void (ParseOgreXMLMeshHandler::*endElementPtr)(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);
	struct elementHandlers {
		elementHandlers() :
				start(&ParseOgreXMLMeshHandler::handleStart_INVALID), end(&ParseOgreXMLMeshHandler::handleEnd_INVALID) {}
		elementHandlers(startElementPtr aStart, endElementPtr aEnd) :
				start(aStart), end(aEnd) {}
		startElementPtr start;
		endElementPtr end;
	};
	typedef map<kstring, elementHandlers> stringToHandlers;
	stringToHandlers m_stringToHandlers;

	void InitHandlersTable(void);
//@}

//! OgreXML strings for element attributes. Held here because they are needed as XMLCh* during parsing
//@{
#undef DOXSTRING
#define DOXSTRINGS                      \
	DOXSTR(vertexcount);                \
	DOXSTR(material);                   \
	DOXSTR(usesharedvertices);          \
	DOXSTR(use32bitindexes);            \
	DOXSTR(operationtype);              \
	DOXSTR(true);                       \
	DOXSTR(false);                      \
	DOXSTR(triangle_list);              \
	DOXSTR(triangle_strip);             \
	DOXSTR(triangle_fan);               \
	DOXSTR(count);                      \
	DOXSTR(v1);                         \
	DOXSTR(v2);                         \
	DOXSTR(v3);                         \
	DOXSTR(name);                       \
	DOXSTR(vertexindex);                \
	DOXSTR(boneindex);                  \
	DOXSTR(weight);                     \
	DOXSTR(numlevels);                  \
	DOXSTR(manual);                     \
	DOXSTR(fromdepthsquared);           \
	DOXSTR(meshname);                   \
	DOXSTR(submeshindex);               \
	DOXSTR(numfaces);                   \
	DOXSTR(positions);                  \
	DOXSTR(normals);                    \
	DOXSTR(colours_diffuse);            \
	DOXSTR(colours_specular);           \
	DOXSTR(texture_coords);             \
	DOXSTR(texture_coord_dimensions_0); \
	DOXSTR(texture_coord_dimensions_1); \
	DOXSTR(texture_coord_dimensions_2); \
	DOXSTR(texture_coord_dimensions_3); \
	DOXSTR(texture_coord_dimensions_4); \
	DOXSTR(texture_coord_dimensions_5); \
	DOXSTR(texture_coord_dimensions_6); \
	DOXSTR(texture_coord_dimensions_7); \
	DOXSTR(x);                          \
	DOXSTR(y);                          \
	DOXSTR(z);                          \
	DOXSTR(value);                      \
	DOXSTR(u);                          \
	DOXSTR(v);                          \
	DOXSTR(w);                          \
	DOXSTR(index);

#undef DOXSTR
#define DOXSTR(str) XMLCh* m_xstr_##str
	DOXSTRINGS
#undef DOXSTR

	void InitAttrStrings();
	void ReleaseAttrStrings();
	//@}

	//! OgreXML Parser internal state variables
	//@{
	bool m_success; //!< loaded successfully?

	meshType* m_mesh; //!< Holds data read (may hold everything if needed)
	geometryData* m_currGeom;
	vertexBuffer* m_currVBuff;
	uint m_currVBuffSize;
	uint m_curr_tc_idx;
	typedef stack<vBoneAssgmtVector*> vBoneAssgmtVectorPtrStack;
	vBoneAssgmtVectorPtrStack m_curr_boneassignmentsStack;
	vBoneAssgmtVector* m_curr_boneassignments;
	uintTriVector* m_curr_faces;
	//@}

	///////////////////////////////////////////////////////////////////////////////
	/// Utility Functions to deal with XML elements
	///////////////////////////////////////////////////////////////////////////////
	//@{
	void XMLStrToCOLOR4F(const XMLCh* const xstr, color4f& color) {
		if (xstr) {
			char* cstr = XMLString::transcode(xstr);

			color.r = color.g = color.b = color.a = 1.0;
			float r, g, b, a;
			int numread = sscanf_s(cstr, "%f %f %f %f", &r, &g, &b, &a);
			if (numread >= 3) {
				color.r = r;
				color.g = g;
				color.b = b;
				if (numread == 4)
					color.a = a;
			}

			XMLString::release(&cstr);
		}
	}
	//@}
};

