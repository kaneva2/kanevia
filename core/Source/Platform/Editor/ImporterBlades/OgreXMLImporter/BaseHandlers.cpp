/******************************************************************************
 BaseHandlers.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "resource.h"

#include "BaseHandlers.h"
#include "KEPHelpers.h"

XERCES_CPP_NAMESPACE_USE

using namespace KEP;

void DefaultErrorHandler::error(const SAXParseException& exc) {
}

void DefaultErrorHandler::warning(const SAXParseException& exc) {
}

void DefaultErrorHandler::fatalError(const SAXParseException& exception) {
	char* message = XMLString::transcode(exception.getMessage());
	LOG4CPLUS_FATAL(m_logger, loadStr(IDS_FATAL_ERROR) << message << " at line: "
													   << exception.getLineNumber());
	XMLString::release(&message);
}

void DefaultErrorHandler::resetErrors() {
}

///

void ProgressiveScanHandler::handleStart_INVALID(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const Attributes& attrs) {
	// log error message
	char* message = XMLString::transcode(uri);
	LOG4CPLUS_ERROR(m_logger, loadStr(IDS_INVALID_START_ELEM) << message);
	XMLString::release(&message);

	//interrupt parsing
	m_done = true;
}

void ProgressiveScanHandler::handleEnd_INVALID(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname) {
	// log error message
	char* message = XMLString::transcode(uri);
	LOG4CPLUS_ERROR(m_logger, loadStr(IDS_INVALID_END_ELEM) << message);
	XMLString::release(&message);

	//interrupt parsing
	m_done = true;
}

///////////////////////////////////////////////////////////////////////////////
/// Utility Functions to deal with XML elements
///////////////////////////////////////////////////////////////////////////////
//@{
int KEP::XMLStrToINT(const XMLCh* const xstr) {
	if (xstr)
		return XMLString::parseInt(xstr);
	return 0;
}

unsigned int KEP::XMLStrToUNSIGNED_INT(const XMLCh* const xstr) {
	if (xstr) {
		int ret = XMLString::parseInt(xstr);
		if (ret < 0) // avoid negative values wrapping to a large positive one
			ret = 0;
		return ret;
	}
	return 0;
}

float KEP::XMLStrToFLOAT(const XMLCh* const xstr) {
	if (xstr) {
		char* cstr = XMLString::transcode(xstr);
		float ret = (float)atof(cstr);
		XMLString::release(&cstr);
		return ret;
	}
	return 0;
}

bool KEP::XMLStrToBOOL(const XMLCh* const xstr) {
	//! Static since it's only needed once
	static XMLCh* _xstr_true = 0;
	if (_xstr_true == 0)
		_xstr_true = XMLString::transcode("true");

	if (xstr) {
		return XMLString::equals(xstr, _xstr_true);
	}
	return false;
}

void KEP::XMLStrToKSTRING(const XMLCh* const xstr, kstring& kstr) {
	if (xstr) {
		char* cstr = XMLString::transcode(xstr);
		kstr = cstr;
		XMLString::release(&cstr);
	}
}

const XMLCh* KEP::attrsGetValue(const Attributes& attrs, const XMLCh* const uri, const char* const cstr) {
	XMLCh* xstr = XMLString::transcode(cstr);
	const XMLCh* retstr = attrs.getValue(uri, xstr);
	XMLString::release(&xstr);
	return retstr;
}
//@}
