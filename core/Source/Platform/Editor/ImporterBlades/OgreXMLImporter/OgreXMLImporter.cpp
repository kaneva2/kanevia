/******************************************************************************
 OgreXMLImporter.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "resource.h"

#include "CEXMeshClass.h"
#include "OgreXmlImporter.h"
#include "BaseHandlers.h"

#include "IAssetsDatabase.h"
#include "CWldObject.h"
#include "CMovementObj.h"

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/util/XMLString.hpp>

#if defined(XERCES_NEW_IOSTREAMS)
#include <iostream>
#else
#include <iostream.h>
#endif

XERCES_CPP_NAMESPACE_USE

OgreXMLImporter ogreImpInstance;
IAssetImporter* ogreImporter = &ogreImpInstance;

OgreXMLImporter::OgreXMLImporter(void) :
		IAssetImporter("OgreXMLImporter", "Ogre3D XML Files Importer (*.mesh.xml/*.skeleton.xml)"),
		m_logger(Logger::getInstance("OgreXMLImporter")) {
}

OgreXMLImporter::~OgreXMLImporter(void) {
	m_logger.shutdown();
}

///
void OgreXMLImporter::Register() {
	// Register with importer factory
	IAssetImporterFactory* factory = IAssetImporterFactory::Instance();
	factory->RegisterImporter(this, KEP_STATIC_MESH, kstring("mesh.xml"));
	factory->RegisterImporter(this, KEP_SKA_SKELETAL_OBJECT, kstring("mesh.xml"));
	factory->RegisterImporter(this, KEP_SKA_SECTION_MESHES, kstring("mesh.xml"));
	factory->RegisterImporter(this, KEP_SKA_SKINNED_MESH, kstring("mesh.xml"));
	factory->RegisterImporter(this, KEP_SKA_RIGID_MESH, kstring("mesh.xml"));
	factory->RegisterImporter(this, KEP_SKA_ANIMATION, kstring("skeleton.xml"));
	factory->RegisterImporter(this, KEP_MATERIAL, kstring("material"));

	// Initialize Xerces XML parser
	try {
		XMLPlatformUtils::Initialize();
	} catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_XERCES_INIT_FAILED) << message);
		XMLString::release(&message);

#ifdef USE_CView
		AfxMessageBox("Unable to initialize Xerces XML parser");
#endif
		return;
	}
}

void OgreXMLImporter::Unregister() {
	// Cleanup Xerces XML parser
	XMLPlatformUtils::Terminate();

	// Unregister with importer factory
	IAssetImporterFactory* factory = IAssetImporterFactory::Instance();
	factory->UnregisterImporter(this);
}

///
bool OgreXMLImporter::Import(const kstring& aFileName) {
	// By default we'll try to make our best guess about the type of XML

	// Guess first based on file extension
	kstring lowerCaseFileName = aFileName;
	STLToLower(lowerCaseFileName);
	if (lowerCaseFileName.rfind(".xml") != kstring::npos) {
		ImportXML(aFileName);
	} else if (lowerCaseFileName.rfind(".material") != kstring::npos) {
		// Right now we don't support loading materials separately from a mesh.
		// In the future we may do, and then we'll create a new material object,
		//  call LoadMaterial( aFileName, newMaterial ) and then add the material
		// to the now non-existent material library
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
/// DetectOgreXMLHandler: A simpler content handler that determines the type
///	  of Ogre XML by examining some file contents
///////////////////////////////////////////////////////////////////////////////
class DetectOgreXMLTypeHandler : public ProgressiveScanHandler {
public:
	void startDocument() {
		m_done = false;
		m_foundMeshElement = false;
		m_foundSkeletonElement = false;
		m_foundSkeletonLink = false;
		m_foundSceneElement = false;
	}

	void startElement(const XMLCh* const uri, const XMLCh* const localname,
		const XMLCh* const qname, const Attributes& attrs) {
		XMLCh* tmpStr;

		// We found a mesh root node before, now we look for a "skeletonlink"
		// NOTE of warning: a mesh file will be parsed until a "skeletonlink"
		//  is found or the end is reached
		if (m_foundMeshElement) {
			tmpStr = XMLString::transcode("skeletonlink");
			if (XMLString::equals(localname, tmpStr)) {
				m_foundSkeletonLink = true;
				m_done = true;
			}
			XMLString::release(&tmpStr);

			return;
		}

		tmpStr = XMLString::transcode("skeleton");
		if (XMLString::equals(localname, tmpStr)) {
			m_foundSkeletonElement = true;
			m_done = true;
		}
		XMLString::release(&tmpStr);

		if (!m_done) {
			tmpStr = XMLString::transcode("scene");
			if (XMLString::equals(localname, tmpStr)) {
				m_foundSceneElement = true;
				m_done = true;
			}
			XMLString::release(&tmpStr);
		}

		if (!m_done) {
			tmpStr = XMLString::transcode("mesh");
			if (XMLString::equals(localname, tmpStr))
				m_foundMeshElement = true;
			XMLString::release(&tmpStr);
		}
	}

	bool FoundMeshElement() { return m_foundMeshElement; }
	bool FoundSkeletonElement() { return m_foundSkeletonElement; }
	bool FoundSkeletonLink() { return m_foundSkeletonLink; }
	bool FoundSceneElement() { return m_foundSceneElement; }

private:
	bool m_foundMeshElement;
	bool m_foundSkeletonElement;
	bool m_foundSkeletonLink;
	bool m_foundSceneElement;
};

///
bool OgreXMLImporter::ImportXML(const kstring& aFileName) {
	// Start parsing the XML file, look for a root "mesh" or "skeleton" node
	SAX2XMLReader* parser = XMLReaderFactory::createXMLReader();
	parser->setFeature(XMLUni::fgSAX2CoreValidation, true); // optional
	parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true); // optional

	DetectOgreXMLTypeHandler* detectTypeHandler = new DetectOgreXMLTypeHandler();
	DefaultErrorHandler* defaultErrorHandler = new DefaultErrorHandler();
	parser->setContentHandler(detectTypeHandler);
	parser->setErrorHandler(defaultErrorHandler);

	bool retval = ExecXMLParse(*parser, *detectTypeHandler, aFileName);

	if (retval) {
		if (detectTypeHandler->FoundMeshElement()) {
			/*
			if( detectTypeHandler->FoundSkeletonLink() )
			{	// import as a KEP_ANIMATED_MESH
				CSkeletonObject *newSkeletonObject = new CSkeletonObject();
				// TODO: check if anything needs initialization here (or moved to constructor)

				if( retval = LoadAnimatedMesh( aFileName, *newSkeletonObject) )
					m_assetsDatabase->AddAsset(newSkeletonObject);
				else
					delete newSkeletonObject;
			}
			else
			{	// import as KEP_STATIC_MESH
				CEXMeshObj *newMeshObject = new CEXMeshObj();
				// TODO: why isn't this done in the constructor?
				newMeshObject->List_materialObject = new CMaterialObject ();

				if( retval = LoadStaticMesh( aFileName, *newMeshObject ) )
					m_assetsDatabase->AddAsset(newMeshObject);
				else
					delete newMeshObject;
			}
		*/
		} else if (detectTypeHandler->FoundSkeletonElement()) {
			// FutureTODO:
			// If a "skeleton" root node is found, import as a KEP_SKELETON
			//   In the future we may support importing these individually into
			//	 a skeletal animation library.
		}
	}

	delete defaultErrorHandler;
	delete detectTypeHandler;
	delete parser;

	return retval;
}

///
#define CATCH_EXCEPTIONS(RETURNaction)                                   \
	catch (const XMLException& toCatch) {                                \
		char* message = XMLString::transcode(toCatch.getMessage());      \
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_XMLEXCEPTION) << message); \
		XMLString::release(&message);                                    \
		RETURNaction;                                                    \
	}                                                                    \
	catch (const SAXException& toCatch) {                                \
		char* message = XMLString::transcode(toCatch.getMessage());      \
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_SAXEXCEPTION) << message); \
		XMLString::release(&message);                                    \
		RETURNaction;                                                    \
	}                                                                    \
	catch (...) {                                                        \
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_UNKNOWN_EXCEPTION));       \
		RETURNaction;                                                    \
	}

///
int OgreXMLImporter::PreloadSchemas(INOUT XERCES_CPP_NAMESPACE_QUALIFIER SAX2XMLReader& aParser, IN const vector<kstring>& aSchemaFileNames) {
	int validGrammars = 0;

	vector<kstring>::const_iterator xsdIt(aSchemaFileNames.begin()), xsdEnd(aSchemaFileNames.end());
	Grammar* retGrammar;
	for (; xsdIt != xsdEnd; ++xsdIt) {
		try {
			retGrammar = aParser.loadGrammar(xsdIt->c_str(), Grammar::SchemaGrammarType, true);
			validGrammars++;
		}
		CATCH_EXCEPTIONS(continue)
	}

	return validGrammars;
}

///
int OgreXMLImporter::PreloadDTDs(INOUT XERCES_CPP_NAMESPACE_QUALIFIER SAX2XMLReader& aParser, IN const vector<kstring>& aDTDFileNames) {
	int validGrammars = 0;

	vector<kstring>::const_iterator dtdIt(aDTDFileNames.begin()), dtdEnd(aDTDFileNames.end());
	Grammar* retGrammar;
	for (; dtdIt != dtdEnd; ++dtdIt) {
		try {
			retGrammar = aParser.loadGrammar(dtdIt->c_str(), Grammar::DTDGrammarType, true);
			validGrammars++;
		}
		CATCH_EXCEPTIONS(continue)
	}
	return validGrammars;
}

///
bool OgreXMLImporter::ExecXMLParse(INOUT XERCES_CPP_NAMESPACE_QUALIFIER SAX2XMLReader& aParser,
	INOUT ProgressiveScanHandler& aHandler, IN const kstring& aFileName) {
	try {
		aParser.setContentHandler(&aHandler);

		XMLPScanToken scanToken;
		aParser.parseFirst(aFileName.c_str(), scanToken);

		bool gotMore = true;
		while (gotMore && !aHandler.IsDone())
			gotMore = aParser.parseNext(scanToken);

		aParser.parseReset(scanToken);

		if (!gotMore && !aHandler.IsDone())
			return false;
	}
	CATCH_EXCEPTIONS(return false)

	return true;
}

///
bool OgreXMLImporter::LoadMaterial(OUT CMaterialObject& aMaterial, OUT CEXMeshObj& aMesh, IN const kstring& aFileName) {
	// TODO
	return false;
}
