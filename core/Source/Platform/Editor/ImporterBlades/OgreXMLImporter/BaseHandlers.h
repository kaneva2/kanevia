/******************************************************************************
 BaseHandlers.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <log4cplus/logger.h>
using namespace log4cplus;

#include "kepstring.h"

#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>

XERCES_CPP_NAMESPACE_USE

namespace KEP {

///////////////////////////////////////////////////////////////////////////////
/// LoggedHandler: A base class for handlers which need access to a logger
///////////////////////////////////////////////////////////////////////////////
class LoggedHandler {
public:
	LoggedHandler() :
			m_logger(Logger::getInstance("OgreXMLImporter")) {}
	virtual ~LoggedHandler() { /*m_logger.shutdown();*/ /* needed? */
	}

protected:
	Logger m_logger;
};

///////////////////////////////////////////////////////////////////////////////
/// DefaultErrorHandler: A simpler error handler
///////////////////////////////////////////////////////////////////////////////
class DefaultErrorHandler : public XERCES_CPP_NAMESPACE_QUALIFIER ErrorHandler, public LoggedHandler {
public:
	void error(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
	void warning(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exc);
	void fatalError(const XERCES_CPP_NAMESPACE_QUALIFIER SAXParseException& exception);
	void resetErrors();
};

///////////////////////////////////////////////////////////////////////////////
/// A base class for handlers used during progressive scanning of XML files
///////////////////////////////////////////////////////////////////////////////
class ProgressiveScanHandler : public DefaultHandler, public LoggedHandler {
public:
	ProgressiveScanHandler() :
			m_done(false) {}
	bool IsDone() { return m_done; }
	void ResetDone() { m_done = false; }

protected:
	void handleStart_INVALID(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const Attributes& attrs);
	void handleEnd_INVALID(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);
	bool m_done;
};

///////////////////////////////////////////////////////////////////////////////
/// Utility Functions to deal with XML elements
///////////////////////////////////////////////////////////////////////////////
//@{

int XMLStrToINT(const XMLCh* const xstr);

unsigned int XMLStrToUNSIGNED_INT(const XMLCh* const xstr);

float XMLStrToFLOAT(const XMLCh* const xstr);

bool XMLStrToBOOL(const XMLCh* const xstr);

void XMLStrToKSTRING(const XMLCh* const xstr, kstring& kstr);

inline void XMLStrToINT(const XMLCh* const xstr, int& _int) {
	_int = XMLStrToINT(xstr);
}
inline void XMLStrToUNSIGNED_INT(const XMLCh* const xstr, unsigned int& _uint) {
	_uint = XMLStrToUNSIGNED_INT(xstr);
}
inline void XMLStrToFLOAT(const XMLCh* const xstr, float& _float) {
	_float = XMLStrToFLOAT(xstr);
}
inline void XMLStrToBOOL(const XMLCh* const xstr, bool& _bool) {
	_bool = XMLStrToBOOL(xstr);
}

const XMLCh* attrsGetValue(const Attributes& attrs, const XMLCh* const uri, const char* const cstr);
//@}

} // end namespace KEP
