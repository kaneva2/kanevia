<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet [
  <!ENTITY lcub "&#x7b;">
  <!ENTITY rcub "&#x7d;">
  <!ENTITY nl "&#xa;">
  <!ENTITY tab "&#x9;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsd2cpp="http://www.kaneva.com">
	
	<xsl:include href="SchemaToCppCommon.xslt" />
	
	<xsl:variable name="headerDefine" select="translate($headerFileName, 'abcdefghijklmnopqrstuvwxyz.', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ_')" />
	
	<xsl:template match="/">
		<xsl:call-template name="header-begin"/>
		
		<xsl:variable name="indent">
			<xsl:text>&nl;</xsl:text>
		</xsl:variable>
		<xsl:value-of select="$indent"/>
		<xsl:if test="$parentNamespace">
			<xsl:text>namespace </xsl:text>
			<xsl:value-of select="$parentNamespace" />
			<xsl:value-of select="$indent"/>
			<xsl:text>{</xsl:text>
		</xsl:if>
		
		<xsl:variable name="indent2">
			<xsl:value-of select="$indent"/>
			<xsl:text>&tab;</xsl:text>
		</xsl:variable>

		<xsl:value-of select="$indent2"/>
		<xsl:if test="$mainElementName">
			<xsl:text>namespace </xsl:text>
			<xsl:value-of select="$mainElementName" />
			<xsl:value-of select="$indent2"/>
			<xsl:text>{</xsl:text>
		</xsl:if>

		<xsl:value-of select="$indent2"/>
		<xsl:apply-templates select="xsd:schema/xsd:complexType" mode="declare">
			<xsl:with-param name="indent" select="$indent2" />
		</xsl:apply-templates>
		
		<xsl:value-of select="$indent2"/>
		<xsl:if test="$mainElementName">
			<xsl:text>} /* namespace </xsl:text>
			<xsl:value-of select="$mainElementName" />
			<xsl:text> */</xsl:text>
			<xsl:value-of select="$indent2"/>
		</xsl:if>

		<xsl:value-of select="$indent"/>
		<xsl:if test="$parentNamespace">
			<xsl:text>} /* namespace </xsl:text>
			<xsl:value-of select="$parentNamespace" />
			<xsl:text> */</xsl:text>
			<xsl:value-of select="$indent"/>
		</xsl:if>
		
		<xsl:call-template name="header-end"/>
	</xsl:template>

	<xsl:template name="header-begin">#ifndef <xsl:value-of select="$headerDefine" />
#define <xsl:value-of select="$headerDefine" />

#include &lt;vector&gt;
#include &lt;jsRefCountPtr.h&gt;

namespace KEP
{
	using namespace std;
	</xsl:template>

	<xsl:template name="header-end">
} /* namespace KEP */
#endif /* <xsl:value-of select="$headerDefine" /> */
</xsl:template>
	
	
	<xsl:template match="xsd:complexType" mode="forwardDeclare">
		<xsl:text>class </xsl:text>
		<xsl:value-of select="@name"/>
		<xsl:text>;</xsl:text>
	</xsl:template>

	<xsl:template match="xsd:complexType" mode="declare">
		<xsl:param name="indent"/>
		
		<xsl:choose>
			<xsl:when test="*/xsd:restriction[@base]">
				<xsl:value-of select="$indent" />
				<xsl:text>typedef </xsl:text>
				<xsl:value-of select="*/xsd:restriction[@base][1]/@base" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="@name"/>
				<xsl:text>;</xsl:text>
			</xsl:when>
			<xsl:otherwise>

				<xsl:for-each select=".//xsd:element[@type and (@minOccurs= '0' or @maxOccurs= 'unbounded')]">
					<xsl:value-of select="$indent"/>
					<xsl:apply-templates select="." mode="declarePtr"/>
				</xsl:for-each>
				<xsl:value-of select="$indent"/>
				<xsl:for-each select=".//xsd:element[@type and @maxOccurs= 'unbounded']" >
					<xsl:value-of select="$indent"/>
					<xsl:apply-templates select="." mode="declareSequence"/>
				</xsl:for-each>
				<xsl:value-of select="$indent"/>
			
				<xsl:text>struct </xsl:text>
				<xsl:value-of select="@name"/>
				<xsl:if test="*/xsd:extension[@base]">
					<xsl:text> : public </xsl:text>
					<xsl:value-of select="*/xsd:extension[@base][1]/@base"/>
				</xsl:if>
				<xsl:value-of select="$indent" />
				<xsl:text>{</xsl:text>

				<xsl:variable name="indent2">
					<xsl:value-of select="$indent"/><xsl:text>&tab;</xsl:text>
				</xsl:variable>

				<xsl:for-each select=".//xsd:element">
					<xsl:value-of select="$indent2" />
					<xsl:apply-templates select="." />
				</xsl:for-each>
				<xsl:for-each select=".//xsd:attribute">
					<xsl:value-of select="$indent2" />
					<xsl:apply-templates select="." />
				</xsl:for-each>
				
				<xsl:value-of select="$indent" />
				<xsl:text>};</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$indent" />
	</xsl:template>
	
	<xsl:template match="xsd:element" mode="declarePtr">
		<xsl:text>typedef jsRefCountPtr&lt;</xsl:text>
		<xsl:value-of select="@type"/>
		<xsl:text>&gt; </xsl:text>
		<xsl:value-of select="concat(substring-before(@type, 'Type'),'Ptr')" />
		<xsl:text>;</xsl:text>
	</xsl:template>

	<xsl:template match="xsd:element" mode="declareSequence">
		<xsl:text>typedef vector&lt;</xsl:text>
		<xsl:value-of select="concat(substring-before(@type, 'Type'),'Ptr')" />
		<xsl:text>&gt; </xsl:text>
		<xsl:value-of select="concat(substring-before(@type, 'Type'),'Sequence')" />
		<xsl:text>;</xsl:text>
	</xsl:template>

	<xsl:template match="xsd:element">
		<xsl:apply-templates select="@type"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="@name" />
		<xsl:text>;</xsl:text>
	</xsl:template>

	<xsl:template match="xsd:attribute">
		<xsl:apply-templates select="@type"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="@name" />
		<xsl:text>;</xsl:text>
	</xsl:template>
	
	<xsl:template match="@type">
		<xsl:choose>
			<xsl:when test="parent::*/@maxOccurs = 'unbounded'">
				<xsl:value-of select="concat(substring-before(.,'Type'), 'Sequence')"/>
			</xsl:when>
			<xsl:when test="parent::*/@minOccurs = 0">
				<xsl:value-of select="concat(substring-before(.,'Type'), 'Ptr')"/>
			</xsl:when>
			<xsl:when test="substring-before(.,':') = 'xs'">
				<xsl:value-of select="document('SchemaToCppCommon.xslt')/*/xsd2cpp:type[@xsdt=current()]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
