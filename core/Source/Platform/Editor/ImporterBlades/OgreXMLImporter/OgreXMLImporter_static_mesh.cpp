/******************************************************************************
 OgreXMLImporter_static_mesh.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"

#include "IClientEngine.h"

#include "OgreXMLmesh_parse_handler.h"

#include "OgreXMLImporter.h"
#include "CExMeshClass.h"

void KEP::OgreXML::mesh::FlipGeometryZAxis(INOUT geometryData& aGeometry) {
	vertexBufferVector::iterator vbIt(aGeometry.vertexbuffers.begin()), vbEnd(aGeometry.vertexbuffers.end());
	for (; vbIt != vbEnd; ++vbIt) {
		vec3fVector::iterator normIt(vbIt->vNormal.begin()), normEnd(vbIt->vNormal.end());
		for (; normIt != normEnd; ++normIt)
			normIt->z = -normIt->z;

		vec3fVector::iterator posIt(vbIt->vPos.begin()), posEnd(vbIt->vPos.end());
		for (; posIt != posEnd; ++posIt)
			posIt->z = -posIt->z;
	}
}

void KEP::OgreXML::mesh::FlipMeshZAxis(INOUT meshType& aMesh) {
	FlipGeometryZAxis(aMesh.sharedgeometry);

	submeshVector::iterator smIt(aMesh.submeshes.begin()), smEnd(aMesh.submeshes.end());
	for (; smIt != smEnd; ++smIt) {
		FlipGeometryZAxis(smIt->geometry);

		// swap face indices 1 and 2 to change clockwiseness
		uintTriVector::iterator fIt(smIt->faces.begin()), fEnd(smIt->faces.end());
		for (; fIt != fEnd; ++fIt)
			swap(fIt->v1, fIt->v2);
	}
}

///////////////////////////////////////////////////////////////////////////////
/// OgreXMLImporter - static mesh parsing and conversion
///////////////////////////////////////////////////////////////////////////////
//@{

void OgreXMLImporter::InitEngineMesh(OUT CEXMeshObj& aExMesh) {
	// set some variables to reasonable defaults
	aExMesh.m_sightBasis = 1.0f; // TODO: figure out a reasonable value for this
	aExMesh.m_smoothAngle = -1; // TODO: figure out a reasonable value for this

	// get textures and material properties
	// TODO: get textures and material data from material file
	aExMesh.m_materialObject->m_storedTextureFromFile[0] = "Test.tga";

	// 7 here is the maximum number of textures that we want to support...
	// this should be fixed to use some enum or static const int from the class declaration
	// but legacy code that this is based on was doing just this...
	for (int idx = 1; idx < 7; idx++)
		aExMesh.m_materialObject->m_storedTextureFromFile[idx] = "NONE";

	if (!aExMesh.m_materialObject)
		aExMesh.m_materialObject = new CMaterialObject();
}

bool OgreXMLImporter::ConvertSubmeshToEngineMesh(OUT CEXMeshObj& aExMesh, IN submesh& aSubMesh, IN meshType& aMesh) {
	if (aSubMesh.operationtype != submesh::TRI_LIST) {
		LOG4CPLUS_ERROR(m_logger, "Cannot handle meshes in a format other than 'triangle_list'");
		return false;
	}

	InitEngineMesh(aExMesh);

	// get pointer to geometry
	OgreXML::mesh::geometryData* geometry = &(aSubMesh.geometry);
	if (aSubMesh.usesharedvertices)
		geometry = &(aMesh.sharedgeometry);

	UINT* indexArray = new UINT[aSubMesh.facecount * 3];
	UINT* dstIdxPtr = indexArray;
	uint srcTriIdx;
	for (srcTriIdx = 0; srcTriIdx < aSubMesh.facecount; srcTriIdx++) {
		*dstIdxPtr = aSubMesh.faces[srcTriIdx].v1;
		dstIdxPtr++;
		*dstIdxPtr = aSubMesh.faces[srcTriIdx].v2;
		dstIdxPtr++;
		*dstIdxPtr = aSubMesh.faces[srcTriIdx].v3;
		dstIdxPtr++;
	}

	// get geometry counts and triangle index data
	aExMesh.m_abVertexArray.SetVertexIndexArray(indexArray, aSubMesh.facecount * 3, geometry->vertexcount);

	delete[] indexArray;
	indexArray = 0;

	// get count of texture coordinates channels/layers
	int uvCount = 0;
	OgreXML::mesh::vertexBufferVector::iterator vbIt(geometry->vertexbuffers.begin()),
		vbEnd(geometry->vertexbuffers.end());
	for (; vbIt != vbEnd; ++vbIt)
		uvCount += vbIt->texCrdCount;
	if (uvCount > 7)
		uvCount = 7;
	aExMesh.m_validUvCount = aExMesh.m_abVertexArray.m_uvCount = uvCount;

	// get vertex data from "geometry"
	if (geometry->vertexcount > 0) {
		// allocate memory to copy data
		ABVERTEX *abVertex(0), *abVertexPtr(0);
		abVertex = new ABVERTEX[geometry->vertexcount];

		vbIt = geometry->vertexbuffers.begin();
		vbEnd = geometry->vertexbuffers.end();
		for (; vbIt != vbEnd; ++vbIt) {
			// positions
			if (vbIt->vPos.size() != 0) {
				abVertexPtr = abVertex;
				vec3fVector::iterator posIt(vbIt->vPos.begin()), posEnd(vbIt->vPos.end());
				for (; posIt != posEnd; ++posIt, ++abVertexPtr) {
					abVertexPtr->x = posIt->x;
					abVertexPtr->y = posIt->y;
					abVertexPtr->z = posIt->z;
				}
			}

			// normals
			if (vbIt->vNormal.size() != 0) {
				abVertexPtr = abVertex;
				vec3fVector::iterator normalIt(vbIt->vNormal.begin()), normalEnd(vbIt->vNormal.end());
				for (; normalIt != normalEnd; ++normalIt, ++abVertexPtr) {
					abVertexPtr->nx = normalIt->x;
					abVertexPtr->ny = normalIt->y;
					abVertexPtr->nz = normalIt->z;
				}
			}

			// color0
			// TODO: not supported by our vertex class right now
			// color1
			// TODO: not supported by our vertex class right now

			// Texture coordinates
			for (uint texCrdSetIdx = 0; texCrdSetIdx < vbIt->texCrdCount; ++texCrdSetIdx) {
				if (vbIt->vTexCrd[texCrdSetIdx].size() != 0) {
					abVertexPtr = abVertex;
					vec3fVector::iterator texCrdIt(vbIt->vTexCrd[texCrdSetIdx].begin()),
						texCrdEnd(vbIt->vTexCrd[texCrdSetIdx].end());
					for (; texCrdIt != texCrdEnd; ++texCrdIt, ++abVertexPtr) {
						TXTUV* tx = &(abVertexPtr->tx0);
						tx += texCrdSetIdx;
						tx->tu = texCrdIt->x;
						tx->tv = texCrdIt->y;
						//tx->tw = texCrdIt->z; // TODO: not supported
					}
				}
			}
		}

		aExMesh.m_abVertexArray.SetABVERTEXStyleData(abVertex, aExMesh.m_abVertexArray.m_vertexCount);
	}

	return true;
}

bool EqualsSize(IN const vertexBuffer& vb, IN uint size) {
	if (vb.positions && vb.vPos.size() != size)
		return false;
	if (vb.normals && vb.vNormal.size() != size)
		return false;
	if (vb.col0 && vb.vCol0.size() != size)
		return false;
	if (vb.col1 && vb.vCol1.size() != size)
		return false;
	for (uint tcIdx = 0; tcIdx < vb.texCrdCount; tcIdx++)
		if (vb.vTexCrd[tcIdx].size() != size)
			return false;

	return true;
}

bool OgreXMLImporter::AppendJointGeometry(OUT geometryData& dstGeom, IN const geometryData& srcGeom) {
	// first check the data source is fine
	const vertexBufferVector& srcVector = srcGeom.vertexbuffers;
	if (srcVector.size() == 0) {
		LOG4CPLUS_ERROR(m_logger, "Source geometry contains no vertexbuffers");
		return false;
	}

	uint srcSize = srcGeom.vertexcount;
	if (srcSize == 0)
		return true; // for all purposes we were successful...

	// verify that the source srcVector contains equal sized vbs
	vertexBufferVector::const_iterator vbIt(srcVector.begin()), vbEnd(srcVector.end());
	for (; vbIt != vbEnd; ++vbIt)
		if (!EqualsSize(*vbIt, srcSize)) {
			LOG4CPLUS_ERROR(m_logger, "Source geometry contains a vertexbuffer of length different from "
										  << srcSize);
			return false;
		}

	// check the destination is fine
	vertexBufferVector& dstVector = dstGeom.vertexbuffers;
	if (dstVector.size() == 0) {
		LOG4CPLUS_ERROR(m_logger, "Destination geometry contains no vertexbuffers. One is required");
		return false;
	}

	// increment destination vertexcount
	//dstGeom.vertexcount += srcGeom.vertexcount;
	uint dstSize = dstGeom.vertexcount;

	// pick the last vb in the destination geometry to append to
	vertexBuffer& dstVb = dstVector.back();

	// add all the data in each vertexbuffer to this single vertexbuffer
	for (vbIt = srcVector.begin(), vbEnd = srcVector.end(); vbIt != vbEnd; ++vbIt) {
// If the geometry of the source submesh has an element that is not available
//  in the destination we must make sure we fill in that element's vector
//  up to the vertexcount of the destination's geometry, so that it's on par with
//  existing elements' vectors.
#define APPEND_ELEMENT(element, defaultValue)                                                          \
	if (vbIt->##element.size() != 0) {                                                                 \
		if ((dstVb.##element.size() == 0) && (dstSize > 0))                                            \
			dstVb.##element.insert(dstVb.##element.end(), dstSize, defaultValue);                      \
		dstVb.##element.insert(dstVb.##element.end(), vbIt->##element.begin(), vbIt->##element.end()); \
	}

		vec3f p;
		p.x = p.y = p.z = 0.0f;
		APPEND_ELEMENT(vPos, p)
		APPEND_ELEMENT(vNormal, p)

		color4f c;
		c.r = c.g = c.b = c.a = 0.0f;
		APPEND_ELEMENT(vCol0, c)
		APPEND_ELEMENT(vCol1, c)

		for (uint tcIdx = 0; tcIdx < vertexBuffer::MAX_TEXCRD; tcIdx++) {
			APPEND_ELEMENT(vTexCrd[tcIdx], p);
		}
		// TODO: figure out how texCrdCount shows up when texCrd sets are split over several vbs
	}

	// After appending all the vertexbuffers in the source geometry
	//  check if an exisiting element's buffer has not been appended to and add default values
	//  to level it up with the rest if needed
	uint appendedSize = dstSize + srcSize;
	dstGeom.vertexcount = appendedSize;
#undef APPEND_ELEMENT
#define APPEND_ELEMENT(element, defaultValue)                                    \
	if ((dstVb.##element.size() > 0) && (dstVb.##element.size() < appendedSize)) \
		dstVb.##element.insert(dstVb.##element.end(), srcSize, defaultValue);
	vec3f p;
	p.x = p.y = p.z = 0.0f;
	APPEND_ELEMENT(vPos, p)
	APPEND_ELEMENT(vNormal, p)

	color4f c;
	c.r = c.g = c.b = c.a = 0.0f;
	APPEND_ELEMENT(vCol0, c)
	APPEND_ELEMENT(vCol1, c)

	for (uint tcIdx = 0; tcIdx < vertexBuffer::MAX_TEXCRD; tcIdx++) {
		APPEND_ELEMENT(vTexCrd[tcIdx], p);
	}

	// set each element availability flag if its vector is not empty
	if (dstVb.vPos.size() != 0)
		dstVb.positions = true;
	if (dstVb.vNormal.size() != 0)
		dstVb.normals = true;
	if (dstVb.vCol0.size() != 0)
		dstVb.col0 = true;
	if (dstVb.vCol1.size() != 0)
		dstVb.col1 = true;
	for (uint tcIdx = 0; tcIdx < vertexBuffer::MAX_TEXCRD; tcIdx++)
		if (dstVb.vTexCrd[tcIdx].size() != 0)
			dstVb.texCrdCount = tcIdx + 1;

	return true;
}

void OgreXMLImporter::MergeSubmeshes(OUT OgreXML::mesh::submesh& aSubmesh, IN OgreXML::mesh::meshType& aMesh) {
	// init submesh
	aSubmesh.material = aMesh.submeshes[0].material; // use the material of the first mesh
	aSubmesh.usesharedvertices = false; // will not use shared vertices
	aSubmesh.use32bitindexes = false;
	aSubmesh.operationtype = submesh::TRI_LIST;
	aSubmesh.facecount = 0;
	aSubmesh.geometry.vertexcount = 0;
	aSubmesh.geometry.vertexbuffers.push_back(vertexBuffer());

	submeshVector::iterator smIt(aMesh.submeshes.begin()), smEnd(aMesh.submeshes.end());
	for (; smIt != smEnd; ++smIt) {
		if (smIt->usesharedvertices && smIt->operationtype == submesh::TRI_LIST) {
			AppendJointGeometry(aSubmesh.geometry, aMesh.sharedgeometry);
			aSubmesh.boneassignments.insert(aSubmesh.boneassignments.end(),
				aMesh.boneassignments.begin(), aMesh.boneassignments.end());
			break; // do this if needed, but only once
		}
	}

	for (smIt = aMesh.submeshes.begin(), smEnd = aMesh.submeshes.end(); smIt != smEnd; ++smIt) {
		if (smIt->operationtype != submesh::TRI_LIST)
			continue; // we only handle triangle lists for now

		aSubmesh.facecount += smIt->facecount;

		if (smIt->usesharedvertices) {
			// only append the faces as they are (don't need an offset and don't need to copy any vertices)
			aSubmesh.faces.reserve(aSubmesh.facecount);
			aSubmesh.faces.insert(aSubmesh.faces.end(), smIt->faces.begin(), smIt->faces.end());
		} else {
			// append faces with the appropriate offset
			uint voffset = aSubmesh.geometry.vertexcount;
			aSubmesh.faces.reserve(aSubmesh.facecount);
			uintTriVector::iterator fIt(smIt->faces.begin()), fEnd(smIt->faces.end());
			for (; fIt != fEnd; ++fIt) {
				uintTri face = *fIt;
				face.v1 += voffset;
				face.v2 += voffset;
				face.v3 += voffset;
				aSubmesh.faces.push_back(face);
			}

			// append vertices
			AppendJointGeometry(aSubmesh.geometry, smIt->geometry);

			// append bone assignments
			aSubmesh.boneassignments.reserve(smIt->boneassignments.size());
			vBoneAssgmtVector::iterator baIt(smIt->boneassignments.begin()), baEnd(smIt->boneassignments.end());
			for (; baIt != baEnd; ++baIt) {
				vBoneAssgmt tmpBa = *baIt;
				tmpBa.vertexindex += voffset;
				aSubmesh.boneassignments.push_back(tmpBa);
			}
		}
	}
}

bool OgreXMLImporter::ConvertMergedMeshToEngineMesh(OUT CEXMeshObj& aExMesh, IN meshType& aMesh) {
	bool retval(false);

	if (aMesh.submeshes.size() == 0)
		return retval;

	if (aMesh.submeshes.size() == 1) {
		submesh& srcSubmesh = aMesh.submeshes.back();
		retval = ConvertSubmeshToEngineMesh(aExMesh, srcSubmesh, aMesh);
	} else {
		// Otherwise merge all the meshes into a single submesh
		submesh* mySubmesh = new submesh();

		MergeSubmeshes(*mySubmesh, aMesh);

		retval = ConvertSubmeshToEngineMesh(aExMesh, *mySubmesh, aMesh);

		delete mySubmesh;
	}

	return retval;
}

///
bool OgreXMLImporter::Import(OUT CEXMeshObj& aExMesh, IN const kstring& aFileName, IN AssetData* aDef, IN int aFlags /*= IMP_STATIC_LEGACY_FLAGS*/) {
	const bool flipZAxis = true;

	SAX2XMLReader* parser = XMLReaderFactory::createXMLReader();
	parser->setFeature(XMLUni::fgSAX2CoreValidation, true);
	parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);

	DefaultErrorHandler* defaultErrorHandler = new DefaultErrorHandler();
	parser->setErrorHandler(defaultErrorHandler);

	ParseOgreXMLMeshHandler* ogreXmlHandler = new ParseOgreXMLMeshHandler();

	// load DTD grammar so that validation works even if XML file does not include it
	// TODO illamas: load open the file from some editor subdirectory
	kstring editorDirectory = IClientEngine::Instance()->EditorBaseDir();
	vector<kstring> dtdFileNames;
	dtdFileNames.push_back(editorDirectory + "ogremeshxml.dtd");
	if (PreloadDTDs(*parser, dtdFileNames) > 0)
		parser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse, true);

	// do the actual parsing
	bool retval = ExecXMLParse(*parser, *ogreXmlHandler, aFileName);

	// verify we actually reached the end successfully
	if (retval)
		retval = ogreXmlHandler->Success();

	if (retval) {
		meshType* loadedMesh = ogreXmlHandler->Get_mesh();

		if (flipZAxis)
			FlipMeshZAxis(*loadedMesh);
		// now we are done parsing, convert the data we loaded into
		//  the Engine internal data structures
		// TODO: allow importing submeshes into separate destination meshes (with a different Import call)
		retval = ConvertMergedMeshToEngineMesh(aExMesh, *loadedMesh);
		delete loadedMesh;
		loadedMesh = 0;
	}

	delete defaultErrorHandler;
	delete ogreXmlHandler;
	delete parser;

	return retval;
}

//@}
