<?xml version="1.0" encoding="UTF-8" ?>

<!DOCTYPE xsl:stylesheet [
  <!ENTITY lcub "&#x7b;">
  <!ENTITY rcub "&#x7d;">
  <!ENTITY nl "&#xa;">
  <!ENTITY tab "&#x9;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsd2cpp="http://www.kaneva.com">

	<xsl:include href="SchemaToCppCommon.xslt" />
	
	<xsl:param name="className" select="concat('Parse_',$baseName,'_Handler')"/>
	
	<xsl:template match="/">
		<xsl:call-template name="parser-heading"/>
		<xsl:apply-templates select="/xsd:schema[1]"/>
	</xsl:template>
	

<xsl:template name="parser-heading">
#include "stdafx.h"
#include "BaseHandlers.h"
#include "<xsl:value-of select="$headerFileName"/>"

#include &lt;stack&gt;

using namespace std;
using namespace KEP::<xsl:value-of select="$parentNamespace"/>;
using namespace KEP::<xsl:value-of select="$fullNamespace"/>;

</xsl:template>


<xsl:template match="xsd:schema">
	<xsl:variable name="uniqueElements" select=".//xsd:element[not(@name=preceding::xsd:element/@name)]"/>
	<xsl:variable name="uniqueAttributes" select=".//xsd:attribute[not(@name=preceding::xsd:attribute/@name)]"/>	
	
///////////////////////////////////////////////////////////////////////////////
/// <xsl:value-of select="$className"/>: A Xerces content handler 
///		that parses an <xsl:value-of select="$baseName"/> XML file
///////////////////////////////////////////////////////////////////////////////
class <xsl:value-of select="$className"/> : public ProgressiveScanHandler
{
public:

	//! Constuctor / destructor
	//@{
	<xsl:value-of select="$className"/>();
	
	virtual ~<xsl:value-of select="$className"/>();
	//@}

	//! Loaded successfully?
	bool Success( void ) { return m_success; }
	
	//! Get the pointer to the <xsl:value-of select="$mainElementName"/>, transfering ownership. The caller must free the object.
	<xsl:value-of select="$mainElementType"/>* Get_<xsl:value-of select="$mainElementName"/>( void )
	{ 
		<xsl:value-of select="$mainElementType"/>* ret = m_<xsl:value-of select="$mainElementName"/>_ptr;
		m_<xsl:value-of select="$mainElementName"/>_ptr = 0;
		return ret;
	}


	//! Implementation of the ContentHandler API
	//@{
	void startDocument();

	void endDocument();

	void startElement(  const   XMLCh*const      uri, const   XMLCh*const    localname,
						const   XMLCh*const    qname, const   Attributes&amp;    attrs );
	
	void endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname);
	//@}

protected:
	//! <xsl:value-of select="$baseName"/> handlers
	//@{
	//! per-element handlers
	#undef declareHandlers
	#define declareHandlers( str ) \
		void handleStart_##str( const XMLCh*const uri, const XMLCh*const localname, const XMLCh*const qname, const Attributes&amp; attrs ); \
		void handleEnd_##str(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname );
	<xsl:for-each select="$uniqueElements">declareHandlers( <xsl:value-of select="@name"/> )
	</xsl:for-each>
	//! per-type handlers
	#undef declareHandlers
	#define declareHandlers( str ) \
		void handleStartT_##str( str* ptr, const XMLCh*const uri, const XMLCh*const localname, const XMLCh*const qname, const Attributes&amp; attrs ); \
		void handleEndT_##str( const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname );
	<xsl:for-each select="xsd:complexType">declareHandlers( <xsl:value-of select="@name"/> )
	</xsl:for-each>
	//@}
	
	//! <xsl:value-of select="$baseName"/> lookup table for handlers
	//@{
	typedef void (<xsl:value-of select="$className"/>::*startElementPtr) ( const XMLCh*const uri, const XMLCh*const localname, const XMLCh*const qname, const Attributes&amp; attrs );
	typedef void (<xsl:value-of select="$className"/>::*endElementPtr) (const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname);
	struct elementHandlers
	{
		elementHandlers() : start(&amp;<xsl:value-of select="$className"/>::handleStart_INVALID), end(&amp;<xsl:value-of select="$className"/>::handleEnd_INVALID) {}
		elementHandlers( startElementPtr aStart, endElementPtr aEnd ) : start(aStart), end(aEnd) {}
		startElementPtr		start;
		endElementPtr		end;
	};
	typedef map&lt;kstring, elementHandlers&gt; stringToHandlers;
	stringToHandlers m_stringToHandlers;

	void InitHandlersTable(void);
	//@}
	
	//! <xsl:value-of select="$baseName"/> strings for element attributes. Held here because they are needed as XMLCh* during parsing
	//@{
	<xsl:for-each select="$uniqueAttributes">XMLCh* m_xstr_<xsl:value-of select="@name"/>;
	</xsl:for-each>
	void InitAttrStrings();
	void ReleaseAttrStrings();
	//@}
	
	//! <xsl:value-of select="$baseName"/> parser internal state variables
	//@{
	bool m_success;					//!&lt; loaded successfully?
	
	<xsl:apply-templates select="$uniqueElements" mode="process-pointers">
		<xsl:with-param name="basecontext" select="."/>
		<xsl:with-param name="action" select="'declare'"/>
	</xsl:apply-templates>
	void InitPointers();
	//@}
};

<!-- Implementation begins -->
//! Initialization and cleanup
//@{
<xsl:value-of select="$className"/>::<xsl:value-of select="$className"/>() : m_success(false)
{
	ResetDone();
	InitHandlersTable();
	InitAttrStrings();
	InitPointers();
}

<xsl:value-of select="$className"/>::~<xsl:value-of select="$className"/>()
{
	ReleaseAttrStrings();
	if( m_<xsl:value-of select="$mainElementName"/>_ptr == 0 )
		delete m_<xsl:value-of select="$mainElementName"/>_ptr;
}

void <xsl:value-of select="$className"/>::InitHandlersTable(void)
{
	<xsl:for-each select="$uniqueElements">m_stringToHandlers["<xsl:value-of select="@name"/>"] =
			elementHandlers(	&amp;<xsl:value-of select="concat($className,'::','handleStart_',@name)"/>, 
								&amp;<xsl:value-of select="concat($className,'::','handleEnd_',@name)"/> );
	</xsl:for-each>
}

void <xsl:value-of select="$className"/>::InitAttrStrings()
{
	<xsl:for-each select="$uniqueAttributes">m_xstr_<xsl:value-of select="@name"/> = XMLString::transcode( "<xsl:value-of select="@name"/>" );
	</xsl:for-each>
}

void <xsl:value-of select="$className"/>::ReleaseAttrStrings()
{
	<xsl:for-each select="$uniqueAttributes">XMLString::release( &amp;m_xstr_<xsl:value-of select="@name"/> );
	</xsl:for-each>
}

template&lt;typename StackType&gt; void clear( StackType&amp; _stack )
{
	while( !_stack.empty() )
		_stack.pop();
}

void <xsl:value-of select="$className"/>::InitPointers()
{
	<xsl:apply-templates select="$uniqueElements" mode="process-pointers">
		<xsl:with-param name="basecontext" select="."/>
		<xsl:with-param name="action" select="'init'"/>
	</xsl:apply-templates>
}
//@}

//! Implementation of ContentHandler API
//@{
void <xsl:value-of select="$className"/>::startDocument()
{
	m_done = false;
	m_success = false;

	if( m_<xsl:value-of select="$mainElementName"/>_ptr )
		delete m_<xsl:value-of select="$mainElementName"/>_ptr;
		
	InitPointers();
	
	m_<xsl:value-of select="$mainElementName"/>_ptr = new <xsl:value-of select="$mainElementType"/>();
	
}

void <xsl:value-of select="$className"/>::endDocument()
{
	m_done = true;
	m_success = true; // reaching the end without exceptions seems like a good sign of success for now
}


void <xsl:value-of select="$className"/>::startElement( const XMLCh*const uri, const XMLCh*const localname,
										const XMLCh*const qname, const Attributes&amp; attrs )
{
	if( m_done )
		return;

	char* elementName = XMLString::transcode( localname );
	stringToHandlers::iterator sthIt = m_stringToHandlers.find(elementName);
	if( sthIt != m_stringToHandlers.end() )
		(this->*(sthIt->second.start))( uri, localname, qname, attrs );
	else
		handleStart_INVALID( uri, localname, qname, attrs );
	
	XMLString::release( &amp;elementName );
}

void <xsl:value-of select="$className"/>::endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname)
{
	if( m_done )
		return;

	char* elementName = XMLString::transcode( localname );
	stringToHandlers::iterator sthIt = m_stringToHandlers.find(elementName);
	if( sthIt != m_stringToHandlers.end() )
		(this->*(sthIt->second.end))( uri, localname, qname );
	else
		handleEnd_INVALID( uri, localname, qname );
	
	XMLString::release( &amp;elementName );
}
//@}


<xsl:variable name="basecontext" select="."/>
//! Implementation of per-element custom handlers
//@{
<xsl:for-each select="$uniqueElements">
void <xsl:value-of select="$className"/>::handleStart_<xsl:value-of select="@name"/>( const XMLCh*const uri, const XMLCh*const localname, const XMLCh*const qname, const Attributes&amp; attrs )
{
	<xsl:value-of select="@type"/>* ptr = 0;
	<xsl:apply-templates select="." mode="process-pointers">
		<xsl:with-param name="basecontext" select="$basecontext"/>
		<xsl:with-param name="action" select="'get'"/>
	</xsl:apply-templates>
	handleStartT_<xsl:value-of select="@type"/>( ptr, uri, localname, qname, attrs );
}

void <xsl:value-of select="$className"/>::handleEnd_<xsl:value-of select="@name"/>(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname )
{
	handleEndT_<xsl:value-of select="@type"/>( uri, localname, qname );
}
</xsl:for-each>
//@}

//! Implementation of per-type custom handlers
//@{
<xsl:for-each select="xsd:complexType">
void <xsl:value-of select="$className"/>::handleStartT_<xsl:value-of select="@name"/>( <xsl:value-of select="@name"/>* ptr, const XMLCh*const uri, const XMLCh*const localname, const XMLCh*const qname, const Attributes&amp; attrs )
{
	<xsl:apply-templates select=".//xsd:element" mode="process-pointers">
		<xsl:with-param name="basecontext" select="$basecontext"/>
		<xsl:with-param name="action" select="'push'"/>
	</xsl:apply-templates>
<xsl:apply-templates select=".//xsd:attribute" mode="get"/>
<!-- handle inheritance-->
<xsl:choose>
	<xsl:when test="*/xsd:restriction[@base]">
	handleStartT_<xsl:value-of select="*/xsd:restriction[@base][1]/@base" />( ptr, uri, localname, qname, attrs );
	</xsl:when>
	<xsl:when test="*/xsd:extension[@base]">
	handleStartT_<xsl:value-of select="*/xsd:extension[@base][1]/@base" />( ptr, uri, localname, qname, attrs );
	</xsl:when>
</xsl:choose>
}

void <xsl:value-of select="$className"/>::handleEndT_<xsl:value-of select="@name"/>( const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname )
{
	<xsl:apply-templates select=".//xsd:element" mode="process-pointers">
		<xsl:with-param name="basecontext" select="$basecontext"/>
		<xsl:with-param name="action" select="'pop'"/>
	</xsl:apply-templates>
<!-- handle inheritance-->
<xsl:choose>
	<xsl:when test="*/xsd:restriction[@base]">
	handleEndT_<xsl:value-of select="*/xsd:restriction[@base][1]/@base" />( uri, localname, qname );
	</xsl:when>
	<xsl:when test="*/xsd:extension[@base]">
	handleEndT_<xsl:value-of select="*/xsd:extension[@base][1]/@base" />( uri, localname, qname );
	</xsl:when>
</xsl:choose>	
}
</xsl:for-each>
//@}

</xsl:template>

<xsl:template match="xsd:attribute" mode="get">
	<xsl:variable name="xsdType" select="@type"/>
	<xsl:variable name="cppType" select="document('SchemaToCppCommon.xslt')/*/xsd2cpp:type[@xsdt=$xsdType]"/>
	XMLStrTo<xsl:value-of select="translate($cppType,'abcdefghijklmnopqrstuvwxyz .','ABCDEFGHIJKLMNOPQRSTUVWXYZ__')"/>(attrs.getValue( uri, m_xstr_<xsl:value-of select="@name"/>), ptr-&gt;<xsl:value-of select="@name"/>);
</xsl:template>


<xsl:template match="xsd:element" mode="process-pointers">
	<xsl:param name="basecontext"/>
	<xsl:param name="action" select="'declare'"/>
	
	<xsl:variable name="thisname" select="@name"/>
	
	<xsl:variable name="typeExists" select="$basecontext//xsd:element[@name=$thisname and (not(@minOccurs) or @minOccurs='1') and (not(@maxOccurs) or @maxOccurs='1')]"/>
	<xsl:variable name="ptrExists" select="$basecontext//xsd:element[@name=$thisname and @minOccurs='0' and (not(@maxOccurs) or @maxOccurs='1')]"/>
	<xsl:variable name="sequenceExists" select="$basecontext//xsd:element[@name=$thisname and @maxOccurs and not(@maxOccurs='1')]" />
	
	<xsl:variable name="ptrType" select="concat(substring-before(@type, 'Type'),'Ptr')"/>
	

	<xsl:choose>

		<xsl:when test="$action='declare'">
			<xsl:if test="$typeExists">
				<xsl:value-of select="@type"/>* m_<xsl:value-of select="@name"/>_ptr;
	stack&lt;<xsl:value-of select="@type"/>*&gt; m_<xsl:value-of select="@name"/>_ptr_stack;
			</xsl:if>
			<xsl:if test="$ptrExists">
				<xsl:value-of select="$ptrType"/>* m_<xsl:value-of select="@name"/>_Ptr_ptr;
	stack&lt;<xsl:value-of select="$ptrType"/>*&gt; m_<xsl:value-of select="@name"/>_Ptr_ptr_stack;
			</xsl:if>
			<xsl:if test="$sequenceExists">
				<xsl:variable name="sequenceType" select="concat(substring-before(@type, 'Type'),'Sequence')"/>
				<xsl:value-of select="$sequenceType"/>* m_<xsl:value-of select="@name"/>_Sequence_ptr;
	stack&lt;<xsl:value-of select="$sequenceType"/>*&gt; m_<xsl:value-of select="@name"/>_Sequence_ptr_stack;
			</xsl:if>
		</xsl:when>
	

		<xsl:when test="$action='init'">
			<xsl:if test="$typeExists">clear(m_<xsl:value-of select="@name"/>_ptr_stack);
	m_<xsl:value-of select="@name"/>_ptr_stack.push(0);
	m_<xsl:value-of select="@name"/>_ptr = 0;
			</xsl:if>
			<xsl:if test="$ptrExists">clear(m_<xsl:value-of select="@name"/>_Ptr_ptr_stack);
	m_<xsl:value-of select="@name"/>_Ptr_ptr_stack.push(0);
	m_<xsl:value-of select="@name"/>_Ptr_ptr = 0;
			</xsl:if>
			<xsl:if test="$sequenceExists">clear(m_<xsl:value-of select="@name"/>_Sequence_ptr_stack);
	m_<xsl:value-of select="@name"/>_Sequence_ptr_stack.push(0);
	m_<xsl:value-of select="@name"/>_Sequence_ptr = 0;
			</xsl:if>
		</xsl:when>
		
		<xsl:when test="$action='get'">
			<xsl:if test="$typeExists">
	if( m_<xsl:value-of select="@name"/>_ptr )
		ptr = m_<xsl:value-of select="@name"/>_ptr;
			</xsl:if>
			<xsl:if test="$ptrExists">
	if( m_<xsl:value-of select="@name"/>_Ptr_ptr )
	{
		ptr = new <xsl:value-of select="@type"/>();
		*m_<xsl:value-of select="@name"/>_Ptr_ptr = <xsl:value-of select="$ptrType"/>(ptr);
	}
			</xsl:if>
			<xsl:if test="$sequenceExists">
	if( m_<xsl:value-of select="@name"/>_Sequence_ptr )
	{
		ptr = new <xsl:value-of select="@type"/>();
		m_<xsl:value-of select="@name"/>_Sequence_ptr->push_back(<xsl:value-of select="$ptrType"/>(ptr));
	}
			</xsl:if>
		</xsl:when>
		

		<xsl:when test="$action='push'">
			<xsl:choose>
				<xsl:when test="(not(@minOccurs) or @minOccurs='1') and (not(@maxOccurs) or @maxOccurs='1')">
	m_<xsl:value-of select="@name"/>_ptr_stack.push(&amp;(ptr-><xsl:value-of select="@name"/>));
	m_<xsl:value-of select="@name"/>_ptr = m_<xsl:value-of select="@name"/>_ptr_stack.top();
					<xsl:if test="$ptrExists">
	m_<xsl:value-of select="@name"/>_Ptr_ptr = 0;
					</xsl:if>
					<xsl:if test="$sequenceExists">
	m_<xsl:value-of select="@name"/>_Sequence_ptr = 0;
					</xsl:if>
				</xsl:when>
				<xsl:when test="@minOccurs='0' and (not(@maxOccurs) or @maxOccurs='1')">
	m_<xsl:value-of select="@name"/>_Ptr_ptr_stack.push(&amp;(ptr-><xsl:value-of select="@name"/>));
	m_<xsl:value-of select="@name"/>_Ptr_ptr = m_<xsl:value-of select="@name"/>_Ptr_ptr_stack.top();
					<xsl:if test="typeExists">
	m_<xsl:value-of select="@name"/>_ptr = 0;
					</xsl:if>
					<xsl:if test="$sequenceExists">
	m_<xsl:value-of select="@name"/>_Sequence_ptr = 0;
					</xsl:if>
				</xsl:when>
				<xsl:when test="@maxOccurs and not(@maxOccurs='1')">
	m_<xsl:value-of select="@name"/>_Sequence_ptr_stack.push(&amp;(ptr-><xsl:value-of select="@name"/>));
	m_<xsl:value-of select="@name"/>_Sequence_ptr = m_<xsl:value-of select="@name"/>_Sequence_ptr_stack.top();
					<xsl:if test="$typeExists">
	m_<xsl:value-of select="@name"/>_ptr = 0;
					</xsl:if>
					<xsl:if test="$ptrExists">
	m_<xsl:value-of select="@name"/>_Ptr_ptr = 0;
					</xsl:if>
				</xsl:when>
			</xsl:choose>
		</xsl:when>
		

		<xsl:when test="$action='pop'">
			<xsl:choose>
				<xsl:when test="(not(@minOccurs) or @minOccurs='1') and (not(@maxOccurs) or @maxOccurs='1')">
	m_<xsl:value-of select="@name"/>_ptr_stack.pop();
	m_<xsl:value-of select="@name"/>_ptr = m_<xsl:value-of select="@name"/>_ptr_stack.top();
					<xsl:if test="$ptrExists">
	m_<xsl:value-of select="@name"/>_Ptr_ptr = m_<xsl:value-of select="@name"/>_Ptr_ptr_stack.top();;
					</xsl:if>
					<xsl:if test="$sequenceExists">
	m_<xsl:value-of select="@name"/>_Sequence_ptr = m_<xsl:value-of select="@name"/>_Sequence_ptr_stack.top();;
					</xsl:if>
				</xsl:when>
				<xsl:when test="@minOccurs='0' and (not(@maxOccurs) or @maxOccurs='1')">
	m_<xsl:value-of select="@name"/>_Ptr_ptr_stack.pop();
	m_<xsl:value-of select="@name"/>_Ptr_ptr = m_<xsl:value-of select="@name"/>_Ptr_ptr_stack.top();
					<xsl:if test="typeExists">
	m_<xsl:value-of select="@name"/>_ptr = m_<xsl:value-of select="@name"/>_ptr_stack.top();
					</xsl:if>
					<xsl:if test="$sequenceExists">
	m_<xsl:value-of select="@name"/>_Sequence_ptr = m_<xsl:value-of select="@name"/>_Sequence_ptr_stack.top();
					</xsl:if>
				</xsl:when>
				<xsl:when test="@maxOccurs and not(@maxOccurs='1')">
	m_<xsl:value-of select="@name"/>_Sequence_ptr_stack.pop();
	m_<xsl:value-of select="@name"/>_Sequence_ptr = m_<xsl:value-of select="@name"/>_Sequence_ptr_stack.top();
					<xsl:if test="$typeExists">
	m_<xsl:value-of select="@name"/>_ptr = m_<xsl:value-of select="@name"/>_ptr_stack.top();
					</xsl:if>
					<xsl:if test="$ptrExists">
	m_<xsl:value-of select="@name"/>_Ptr_ptr = m_<xsl:value-of select="@name"/>_Ptr_ptr_stack.top();
					</xsl:if>
				</xsl:when>
			</xsl:choose>
		</xsl:when>

	</xsl:choose>
	<xsl:text>
	</xsl:text>
</xsl:template>

	
</xsl:stylesheet>
