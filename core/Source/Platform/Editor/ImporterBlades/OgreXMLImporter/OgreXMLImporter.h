/******************************************************************************
 OgreXMLImporter.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEPHelpers.h"
#include "IAssetsDatabase.h"

#include <jsRefCountPtr.h>

#include <map>

#include <log4cplus/logger.h>
using namespace log4cplus;

#include <xercesc/sax2/SAX2XMLReader.hpp>

namespace KEP {

// forward declarations
class ProgressiveScanHandler;
class kstringToBone_map_t;
struct bone_t;
class CDeformableVisObj;
class CBoneObject;
class CBoneList;

namespace OgreXML {
namespace mesh {
struct submesh;
struct meshType;
struct geometryData;
typedef jsRefCountPtr<meshType> meshPtr;
} // namespace mesh
namespace skeleton {
struct skeletonType;
typedef jsRefCountPtr<skeletonType> skeletonPtr;
} // namespace skeleton
} // namespace OgreXML

///////////////////////////////////////////////////////////////////////////////
/// OgreXMLImporter imports files in the OgreXML and related formats.
///
///	More specifically we will support Ogre mesh and skeleton XML files,
///	  Ogre .material files (initially a subset of its contents), and
///	  Ogre dotScene XML files.
///
///////////////////////////////////////////////////////////////////////////////
class OgreXMLImporter : public IAssetImporter {
public:
	OgreXMLImporter(void);
	~OgreXMLImporter(void);

	void Register();

	void Unregister();

	bool Import(IN const kstring& aFileName);

	bool Import(OUT CEXMeshObj& aStaticMesh, IN const kstring& aFileName, IN AssetData* aDef = NULL, IN int aFlags = IMP_STATIC_LEGACY_FLAGS);

	bool Import(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, IN AssetTree* aTree = NULL, IN int aFlags = IMP_SKELETAL_LEGACY_FLAGS);

	bool Import(OUT CSkeletonObject& aSkeletalObject, IN const kstringVector& aFileNameVector, IN AssetTree* aTree = NULL, IN int aFlags = IMP_SKELETAL_LEGACY_FLAGS);

	/** @name Additional skeletal animation import functions.
	  * These functions try to import skeletal animation data of different types
	  *  to an existing skeletal animation object which is already initialized
	  * See IAssetsDatabase documentation for more details.
	  */
	//@{
	//@@Broken - Disabled [Yanfeng 09/2008]
	//	bool ImportSectionMeshes( OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, int aConfigIndex
	//						, IN int aLodIndex, int flags =
	//						( SKELETAL_REPLACE_CONFIG | SKELETAL_REPLACE_LOD ));

	bool ImportSkinnedMesh(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, IN int aSectionIndex, IN int aConfigIndex, IN int aLodIndex, int flags = (SKELETAL_REPLACE_SECTION | SKELETAL_REPLACE_CONFIG | SKELETAL_REPLACE_LOD), IN AssetData* aDef = NULL);

	bool ImportRigidMesh(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, IN int aBoneIndex, IN int aLodIndex, int flags = SKELETAL_REPLACE_LOD, IN AssetData* aDef = NULL);

	bool ImportAnimation(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, INOUT int& aAnimationIndex, IN int flags = SKELETAL_REPLACE_ANIMATION, IN BOOL bLooping = TRUE, IN int type = 0, IN int version = 0, IN int minPlayDuration = 0, IN AssetData* aDef = NULL);
	//@}

protected:
	//! Examines the XML file and tries to import it as the most appropriate type
	//   based on the nodes found.
	//! \return true if succeeded
	bool ImportXML(IN const kstring& aFileName);

	//! Do the parsing and error reporting for a mesh and its skeleton
	bool LoadMeshAndSkeleton(OUT OgreXML::mesh::meshPtr& aMeshPtr, OUT OgreXML::skeleton::skeletonPtr& aSkeletonPtr, IN const kstring& aMeshFileName);

	//! Load a material from the file. It requires also the mesh because the mesh stores texture names
	//! \return true if succeeded
	bool LoadMaterial(OUT CMaterialObject& aMaterial, OUT CEXMeshObj& aMesh, IN const kstring& aFileName);

	//! Performs a progressive scan of the XML specified file
	//! \param aParser the SAX2 parser to be used
	//!	\param aHandler used to check whether parsing should terminate
	//! \param aFileName the name of the file to parse
	//! \return true if succeeded
	bool ExecXMLParse(INOUT XERCES_CPP_NAMESPACE_QUALIFIER SAX2XMLReader& aParser, INOUT ProgressiveScanHandler& aHandler, IN const kstring& aFileName);

	//! Try to preload the given DTDs into the given parser
	int PreloadDTDs(INOUT XERCES_CPP_NAMESPACE_QUALIFIER SAX2XMLReader& aParser, IN const std::vector<kstring>& aDTDFileNames);

	//! Try to preload the given Schemas into the given parser
	int PreloadSchemas(INOUT XERCES_CPP_NAMESPACE_QUALIFIER SAX2XMLReader& aParser, IN const std::vector<kstring>& aSchemaFileNames);

	///////////////////////////////////////////////////////////////////////////////
	/// Functions to convert to Engine internal mesh data structure
	///////////////////////////////////////////////////////////////////////////////
	//@{

	//! Convert a single submesh to an engine mesh
	bool ConvertSubmeshToEngineMesh(OUT CEXMeshObj& aExMesh,
		IN OgreXML::mesh::submesh& aSubMesh, IN OgreXML::mesh::meshType& aMesh);

	//! Merge all submeshes and convert the merged object to an engine mesh
	bool ConvertMergedMeshToEngineMesh(OUT CEXMeshObj& aExMesh, IN OgreXML::mesh::meshType& aMesh);

	//! Helper function. Init some variables in an engine mesh to reasonable defaults
	void InitEngineMesh(OUT CEXMeshObj& aExMesh);

	//! Helper function. Merge all submeshes in a mesh into a single submesh
	//! Uses the material of the first submesh
	void MergeSubmeshes(OUT OgreXML::mesh::submesh& aSubmesh, IN OgreXML::mesh::meshType& aMesh);

	//! Helper function. appends the data in a vector of vertexbuffers in srcGeom to a single
	//!  vertexbuffer in dstGeom. For proper use the vertexbuffers in srcGeom should have the same size
	//!  and not contain data for the same stream type (pos, normal, col0, col1, or texCrd*)
	bool AppendJointGeometry(OUT OgreXML::mesh::geometryData& dstGeom, IN const OgreXML::mesh::geometryData& srcGeom);

	//@}

	///////////////////////////////////////////////////////////////////////////////
	/// Functions to convert to Engine internal skeletal animation object
	///////////////////////////////////////////////////////////////////////////////
	//@{
	void ConvertSkeletonToBones(OUT kstringToBone_map_t& aBones, IN OgreXML::skeleton::skeletonType& aSkeleton, IN float aFramesPerSecond, IN bool aFlipZAxis);

	void ConvertBonesToCBoneList(OUT CBoneList& aBoneList, IN kstringToBone_map_t& aBones, IN float aFramesPerSecond);

	void ConvertBoneAnimations(INOUT CBoneObject& aDstBone, IN bone_t& aSrcBone, IN float aFramesPerSecond, IN BOOL bLooping, IN int type, IN int version, IN int minPlayDuration);

	bool AppendMeshToSkeletalObjectAsSections(OUT CSkeletonObject& aSkeletalObject, IN OgreXML::mesh::meshType& aMesh, IN OgreXML::skeleton::skeletonType& aSkeleton);

	bool ConvertMeshToLODMesh(OUT CDeformableVisObj& aLodMesh, IN OgreXML::mesh::meshType& aMesh, IN OgreXML::skeleton::skeletonType& aSkeleton, IN CSkeletonObject& aSkeletalObject);

	//! map by bone id to (id, CBoneObject*) to access quickly when converting vertex-bone assignments
	typedef std::pair<unsigned int, CBoneObject*> idCBonePair;
	typedef std::map<unsigned int, idCBonePair> uintToCBone_map_t;

	//! Build a mapping between the bone ids defined locally in this aSubmes's aSkeleton
	//!  and the merged bones (aBones) generated after merging all the different skeleton files
	void BuildLocalBoneToSkeletonBoneMap(OUT uintToCBone_map_t& aBtoBmap, IN OgreXML::skeleton::skeletonType& aSkeleton, IN CBoneList& aCBoneList);

	void ConvertBoneAssignments(OUT CDeformableVisObj& aLodMesh, IN OgreXML::mesh::submesh& aSubmesh, IN OgreXML::mesh::meshType& aMesh, IN OgreXML::skeleton::skeletonType& aSkeleton, IN uintToCBone_map_t& aBones);
	//@}

	///////////////////////////////////////////////////////////////////////////////
	/// Helper Functions
	///////////////////////////////////////////////////////////////////////////////
	//@{
	int AnimationNameToType(IN kstring& aName);

	void GetSectionName(OUT kstring& aSectionName, IN OgreXML::mesh::meshType& aMesh, unsigned int aIndex, unsigned int aExistingSectionCount);
	//@}

protected:
	Logger m_logger;
};

} // end namespace KEP
