/******************************************************************************
 resource.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OgreXMLImporter.rc
//
#define IDS_INVALID_START_ELEM          3400
#define IDS_INVALID_END_ELEM            3401
#define IDS_XERCES_INIT_FAILED          3402
#define IDS_XMLEXCEPTION                3403
#define IDS_SAXEXCEPTION                3404
#define IDS_UNKNOWN_EXCEPTION           3405
#define IDS_BONE_EXISTS                 3406
#define IDS_BONE_NOTFOUND               3407
#define IDS_BONE_PARENT_NOTFOUND        3408
#define IDS_ANIM_FOR_UNKNOWN_BONE       3409
#define IDS_BONE_NOTFOUND_IN_MERGEDSKEL 3410
#define IDS_BONEID_NOTFOUND             3411
#define IDS_ERROR_READING_FILE          3412
#define IDS_MESHFILE_HAS_NO_SKELETON    3413
#define IDS_FATAL_ERROR                 3414

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
