/******************************************************************************
 OgreXMLmesh_parse_handler.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"

#include "OgreXMLmesh_parse_handler.h"

void ParseOgreXMLMeshHandler::startDocument() {
	m_done = false;
	m_success = false;

	if (m_mesh)
		delete m_mesh;

	m_mesh = 0;
	m_currGeom = 0;
	m_currVBuff = 0;
	m_currVBuffSize = 0;
	m_curr_tc_idx = 0;
	m_curr_boneassignments = 0;
	m_curr_faces = 0;

	m_mesh = new meshType();
}

void ParseOgreXMLMeshHandler::endDocument() {
	m_done = true;
	m_success = true; // reaching the end without exceptions seems like a good sign of success for now
}

void ParseOgreXMLMeshHandler::InitHandlersTable(void) {
#define addSTHEntry(str) m_stringToHandlers[#str] =                                       \
							 elementHandlers(&ParseOgreXMLMeshHandler::handleStart_##str, \
								 &ParseOgreXMLMeshHandler::handleEnd_##str)

	addSTHEntry(INVALID);
	addSTHEntry(mesh);
	addSTHEntry(sharedgeometry);
	addSTHEntry(vertexbuffer);
	addSTHEntry(vertex);
	addSTHEntry(position);
	addSTHEntry(normal);
	addSTHEntry(colour_diffuse);
	addSTHEntry(colour_specular);
	addSTHEntry(texcoord);
	addSTHEntry(submeshes);
	addSTHEntry(submesh);
	addSTHEntry(faces);
	addSTHEntry(face);
	addSTHEntry(geometry);
	addSTHEntry(skeletonlink);
	addSTHEntry(boneassignments);
	addSTHEntry(vertexboneassignment);
	addSTHEntry(levelofdetail);
	addSTHEntry(lodmanual);
	addSTHEntry(lodgenerated);
	addSTHEntry(lodfacelist);
	addSTHEntry(submeshnames);
	addSTHEntry(submeshname);
}

void ParseOgreXMLMeshHandler::InitAttrStrings() {
#define DOXSTR(str) m_xstr_##str = XMLString::transcode(#str)
	DOXSTRINGS
#undef DOXSTR
}

void ParseOgreXMLMeshHandler::ReleaseAttrStrings() {
#define DOXSTR(str) XMLString::release(&m_xstr_##str)
	DOXSTRINGS
#undef DOXSTR
}

void ParseOgreXMLMeshHandler::startElement(const XMLCh *const uri, const XMLCh *const localname,
	const XMLCh *const qname, const Attributes &attrs) {
	if (m_done)
		return;

	char *elementName = XMLString::transcode(localname);
	stringToHandlers::iterator sthIt = m_stringToHandlers.find(elementName);
	if (sthIt != m_stringToHandlers.end())
		(this->*(sthIt->second.start))(uri, localname, qname, attrs);
	else
		handleStart_INVALID(uri, localname, qname, attrs);

	XMLString::release(&elementName);
}

void ParseOgreXMLMeshHandler::endElement(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	if (m_done)
		return;

	char *elementName = XMLString::transcode(localname);
	stringToHandlers::iterator sthIt = m_stringToHandlers.find(elementName);
	if (sthIt != m_stringToHandlers.end())
		(this->*(sthIt->second.end))(uri, localname, qname);
	else
		handleEnd_INVALID(uri, localname, qname);

	XMLString::release(&elementName);
}

///////////////////////////////////////////////////////////////////////////////
/// OgreXML element/node handlers
///////////////////////////////////////////////////////////////////////////////
//@{

void ParseOgreXMLMeshHandler::handleStart_mesh(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);
	m_curr_boneassignmentsStack.push(&(m_mesh->boneassignments));
}

void ParseOgreXMLMeshHandler::handleEnd_mesh(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	m_curr_boneassignmentsStack.pop();
}

///

void ParseOgreXMLMeshHandler::handleStart_sharedgeometry(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);
	// get vertexcount attribute

	const XMLCh *vcountXStr = attrs.getValue(uri, m_xstr_vertexcount);
	m_currGeom = &(m_mesh->sharedgeometry);
	m_currGeom->vertexcount = XMLStrToUNSIGNED_INT(vcountXStr);
}

void ParseOgreXMLMeshHandler::handleEnd_sharedgeometry(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	m_currGeom = 0;
}

///

void ParseOgreXMLMeshHandler::handleStart_vertexbuffer(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_currGeom);

	// add a vertex buffer
	m_currGeom->vertexbuffers.push_back(vertexBuffer());
	m_currVBuff = &(m_currGeom->vertexbuffers.back());
	m_currVBuffSize = 0;

	// get vertexbuffer attributes
	if (XMLStrToBOOL(attrs.getValue(uri, m_xstr_positions))) {
		//m_currVBuff->vPos = new vec3fVector();
		m_currVBuff->positions = true;
		m_currVBuff->vPos.reserve(m_currGeom->vertexcount);
	}

	if (XMLStrToBOOL(attrs.getValue(uri, m_xstr_normals))) {
		//m_currVBuff->vNormal= new vec3fVector();
		m_currVBuff->normals = true;
		m_currVBuff->vNormal.reserve(m_currGeom->vertexcount);
	}

	if (XMLStrToBOOL(attrs.getValue(uri, m_xstr_colours_diffuse))) {
		//m_currVBuff->vCol0 = new color4fVector();
		m_currVBuff->col0 = true;
		m_currVBuff->vCol0.reserve(m_currGeom->vertexcount);
	}

	if (XMLStrToBOOL(attrs.getValue(uri, m_xstr_colours_specular))) {
		//m_currVBuff->vCol1 = new color4fVector();
		m_currVBuff->col1 = true;
		m_currVBuff->vCol1.reserve(m_currGeom->vertexcount);
	}

	uint numTexCoords = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_texture_coords));
	m_currVBuff->texCrdCount = numTexCoords;
	ASSERT(numTexCoords <= vertexBuffer::MAX_TEXCRD);
	for (uint i = 0; i < numTexCoords; i++) {
		//m_currVBuff->vTexCrd[i] = new vec3fVector();
		m_currVBuff->vTexCrd[i].reserve(m_currGeom->vertexcount);
		// WARNING: this assumes contiguous allocation of these variables in the class
		m_currVBuff->texCrdDim[i] = 2; // default fromt DTD
		m_currVBuff->texCrdDim[i] = XMLStrToUNSIGNED_INT(attrs.getValue(uri, *(&m_xstr_texture_coord_dimensions_0 + i)));
	}
}

#define ASSERT_VCOUNT3(elname, element, vcount) ASSERT((m_currVBuff->##elname == false) || \
													   (vcount == m_currVBuff->##element.size()))

#define ASSERT_VCOUNT2(element, vcount) ASSERT(vcount == m_currVBuff->##element.size())

void ParseOgreXMLMeshHandler::handleEnd_vertexbuffer(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	// verify that we actually read as many vertices as we said we would
	// NOTE: I'm assuming each vertexbuffer has vertexcount vertices...
	//	otherwise this should be in handleEnd_*geometry

	// this one should be enough if we do a per vertex check
	ASSERT(m_currVBuffSize == m_currGeom->vertexcount);

	// but we check just in case for now
	ASSERT_VCOUNT3(positions, vPos, m_currGeom->vertexcount);
	ASSERT_VCOUNT3(normals, vNormal, m_currGeom->vertexcount);
	ASSERT_VCOUNT3(col0, vCol0, m_currGeom->vertexcount);
	ASSERT_VCOUNT3(col1, vCol1, m_currGeom->vertexcount);
	for (uint i = 0; i < m_currVBuff->texCrdCount; i++)
		ASSERT_VCOUNT2(vTexCrd[i], m_currGeom->vertexcount);

	m_currVBuff = 0;
}

///

void ParseOgreXMLMeshHandler::handleStart_vertex(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	m_curr_tc_idx = 0;
}

void ParseOgreXMLMeshHandler::handleEnd_vertex(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	m_currVBuffSize++;

	// we want to check that all the vertex arrays have the same size
	//	i.e.: we have read one of each one that we expected
	ASSERT(m_currVBuff);
	ASSERT_VCOUNT3(positions, vPos, m_currVBuffSize);
	ASSERT_VCOUNT3(normals, vNormal, m_currVBuffSize);
	ASSERT_VCOUNT3(col0, vCol0, m_currVBuffSize);
	ASSERT_VCOUNT3(col1, vCol1, m_currVBuffSize);
	for (uint i = 0; i < m_currVBuff->texCrdCount; i++)
		ASSERT_VCOUNT2(vTexCrd[i], m_currVBuffSize);
}

///

void ParseOgreXMLMeshHandler::handleStart_position(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_currVBuff);
	//ASSERT(m_currVBuff->vPos);

	vec3f pos;
	pos.x = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_x));
	pos.y = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_y));
	pos.z = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_z));
	m_currVBuff->vPos.push_back(pos);
}

void ParseOgreXMLMeshHandler::handleEnd_position(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_normal(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_currVBuff);
	//ASSERT(m_currVBuff->vNormal);

	vec3f normal;
	normal.x = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_x));
	normal.y = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_y));
	normal.z = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_z));
	m_currVBuff->vNormal.push_back(normal);
}

void ParseOgreXMLMeshHandler::handleEnd_normal(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_colour_diffuse(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_currVBuff);
	//ASSERT(m_currVBuff->vCol0);

	color4f color;
	XMLStrToCOLOR4F(attrs.getValue(uri, m_xstr_value), color);
	m_currVBuff->vCol0.push_back(color);
}

void ParseOgreXMLMeshHandler::handleEnd_colour_diffuse(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_colour_specular(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_currVBuff);
	//ASSERT(m_currVBuff->vCol1);

	color4f color;
	XMLStrToCOLOR4F(attrs.getValue(uri, m_xstr_value), color);
	m_currVBuff->vCol1.push_back(color);
}

void ParseOgreXMLMeshHandler::handleEnd_colour_specular(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_texcoord(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_currVBuff);
	//ASSERT(m_currVBuff->vTexCrd[m_curr_tc_idx]);

	vec3f texCrd;
	texCrd.x = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_u));
	texCrd.y = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_v));
	texCrd.z = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_w));
	m_currVBuff->vTexCrd[m_curr_tc_idx].push_back(texCrd);

	m_curr_tc_idx++;
}

void ParseOgreXMLMeshHandler::handleEnd_texcoord(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_submeshes(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
}

void ParseOgreXMLMeshHandler::handleEnd_submeshes(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	// NOOP
}

///

void ParseOgreXMLMeshHandler::handleStart_submesh(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);

	// create new submesh
	m_mesh->submeshes.push_back(submesh());
	submesh &sm = m_mesh->submeshes.back();

	m_curr_boneassignmentsStack.push(&(sm.boneassignments));

	// get attributes for this submesh
	XMLStrToKSTRING(attrs.getValue(uri, m_xstr_material), sm.material);
	sm.usesharedvertices = XMLStrToBOOL(attrs.getValue(uri, m_xstr_usesharedvertices));
	sm.use32bitindexes = XMLStrToBOOL(attrs.getValue(uri, m_xstr_use32bitindexes));
	sm.operationtype = submesh::TRI_LIST; // default
	//if( XMLString::equals( attrs.getValue( uri, m_xstr_operationtype ), m_xstr_triangle_list ) )
	//	sm.operationtype = submesh::TRI_LIST;
	//else
	if (XMLString::equals(attrs.getValue(uri, m_xstr_operationtype), m_xstr_triangle_strip))
		sm.operationtype = submesh::TRI_STRIP;
	else if (XMLString::equals(attrs.getValue(uri, m_xstr_operationtype), m_xstr_triangle_fan))
		sm.operationtype = submesh::TRI_FAN;
}

void ParseOgreXMLMeshHandler::handleEnd_submesh(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	m_curr_boneassignmentsStack.pop();
}

///

void ParseOgreXMLMeshHandler::handleStart_faces(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);

	m_mesh->submeshes.back().facecount = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_count));
	m_curr_faces = &(m_mesh->submeshes.back().faces);
	m_curr_faces->reserve(m_mesh->submeshes.back().facecount);
}

void ParseOgreXMLMeshHandler::handleEnd_faces(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	// check the number of faces read is as expected
	ASSERT(m_curr_faces);
	ASSERT(m_curr_faces->size() == m_mesh->submeshes.back().facecount);
	m_curr_faces = 0;
}

///

void ParseOgreXMLMeshHandler::handleStart_face(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_curr_faces);

	// read face attributes
	uintTri face;
	face.v1 = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_v1));
	face.v2 = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_v2));
	face.v3 = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_v3));
	m_curr_faces->push_back(face);
}

void ParseOgreXMLMeshHandler::handleEnd_face(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_geometry(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);

	m_currGeom = &(m_mesh->submeshes.back().geometry);
	m_currGeom->vertexcount = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_vertexcount));
}

void ParseOgreXMLMeshHandler::handleEnd_geometry(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	m_currGeom = 0;
}

///

void ParseOgreXMLMeshHandler::handleStart_skeletonlink(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);
	XMLStrToKSTRING(attrs.getValue(uri, m_xstr_name), m_mesh->skeletonlink);
}

void ParseOgreXMLMeshHandler::handleEnd_skeletonlink(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_boneassignments(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);
	m_curr_boneassignments = m_curr_boneassignmentsStack.top();
	//	if( m_state == OXP_mesh )
	//		m_curr_boneassignments = &(m_mesh->boneassignments);
	//	else if( m_state == OXP_submesh )
	//		m_curr_boneassignments = &(m_mesh->submeshes.back().boneassignments );
}

void ParseOgreXMLMeshHandler::handleEnd_boneassignments(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	//m_curr_boneassignments = 0;
}

///

void ParseOgreXMLMeshHandler::handleStart_vertexboneassignment(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_curr_boneassignments);

	vBoneAssgmt ba;
	ba.vertexindex = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_vertexindex));
	ba.boneindex = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_boneindex));
	ba.weight = XMLStrToFLOAT(attrs.getValue(uri, m_xstr_weight));
	m_curr_boneassignments->push_back(ba);
}

void ParseOgreXMLMeshHandler::handleEnd_vertexboneassignment(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_levelofdetail(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);

	m_mesh->levelofdetail.numlevels = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_numlevels));
	m_mesh->levelofdetail.manual = XMLStrToBOOL(attrs.getValue(uri, m_xstr_manual));
	if (m_mesh->levelofdetail.manual)
		m_mesh->levelofdetail.lodmanual.reserve(m_mesh->levelofdetail.numlevels);
	else
		m_mesh->levelofdetail.lodgenerated.reserve(m_mesh->levelofdetail.numlevels);
}

void ParseOgreXMLMeshHandler::handleEnd_levelofdetail(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_lodmanual(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);

	m_mesh->levelofdetail.lodmanual.push_back(lodmanualData());
	m_mesh->levelofdetail.lodmanual.back().fromdepthsquared =
		XMLStrToFLOAT(attrs.getValue(uri, m_xstr_fromdepthsquared));
	XMLStrToKSTRING(attrs.getValue(uri, m_xstr_meshname),
		m_mesh->levelofdetail.lodmanual.back().meshname);
}

void ParseOgreXMLMeshHandler::handleEnd_lodmanual(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_lodgenerated(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);

	m_mesh->levelofdetail.lodgenerated.push_back(lodgeneratedData());

	m_mesh->levelofdetail.lodgenerated.back().fromdepthsquared =
		XMLStrToFLOAT(attrs.getValue(uri, m_xstr_fromdepthsquared));
	XMLStrToKSTRING(attrs.getValue(uri, m_xstr_meshname),
		m_mesh->levelofdetail.lodgenerated.back().meshname);
}

void ParseOgreXMLMeshHandler::handleEnd_lodgenerated(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

///

void ParseOgreXMLMeshHandler::handleStart_lodfacelist(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);

	m_mesh->levelofdetail.lodgenerated.back().lodfacelists.push_back(lodfacelist());
	m_curr_faces = &(m_mesh->levelofdetail.lodgenerated.back().lodfacelists.back().faces);
	m_mesh->levelofdetail.lodgenerated.back().lodfacelists.back().submeshindex = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_submeshindex));
	uint numfaces = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_numfaces));
	m_mesh->levelofdetail.lodgenerated.back().lodfacelists.back().numfaces = numfaces;
	m_curr_faces->reserve(numfaces);
}

void ParseOgreXMLMeshHandler::handleEnd_lodfacelist(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	// veryfy that we actually read as many faces as we expected
	ASSERT(m_mesh);
	ASSERT(m_curr_faces->size() == m_mesh->levelofdetail.lodgenerated.back().lodfacelists.back().numfaces);

	m_curr_faces = 0;
}

///

void ParseOgreXMLMeshHandler::handleStart_submeshnames(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
}

void ParseOgreXMLMeshHandler::handleEnd_submeshnames(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
	// NOOP
}

///

void ParseOgreXMLMeshHandler::handleStart_submeshname(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname, const Attributes &attrs) {
	ASSERT(m_mesh);
	kstring name;
	XMLStrToKSTRING(attrs.getValue(uri, m_xstr_name), name);
	uint index = XMLStrToUNSIGNED_INT(attrs.getValue(uri, m_xstr_index));
	m_mesh->submeshnames[index] = name;
}

void ParseOgreXMLMeshHandler::handleEnd_submeshname(const XMLCh *const uri, const XMLCh *const localname, const XMLCh *const qname) {
}

//@}
///////////////////////////////////////////////////////////////////////////////
