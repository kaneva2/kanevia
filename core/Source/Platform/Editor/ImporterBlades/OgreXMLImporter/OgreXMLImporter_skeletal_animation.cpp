/******************************************************************************
 OgreXMLImporter_skeletal_animation.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "resource.h"
#include "KEPDiag.h"
#include "IClientEngine.h"

#include "OgreXMLImporter.h"

// parsing handlers
#include "OgreXMLskeleton_parse_handler.h"
#include "OgreXMLmesh_parse_handler.h"

// engine classes that we import into
#include "CBoneClass.h"
#include "CDeformableMeshClass.h"
#include "CExMeshClass.h"
#include "CSkeletalClass.h"
#include "CBoneAnimationNaming.h"

// helper template for keyframe interpolation
#include "KeyFramedTypeT.hh"

using namespace KEP::OgreXML::skeleton;

#include <sstream>
#include <algorithm>

// TODO: ImportSectionMeshes should allow taking multiple files as input
// TODO: need an Add button for Rigid Meshes
// TODO: UI needed for ImportAnimation: should let users choose which animation/s to import and replace individual ones
// TODO: UI needed for ImportSkinnedMesh: should let user choose which submesh/es to import
// TODO: UI needed for ImportSectionMeshes: should allow assigning submeshes to sections
// TODO: MergeData dialog is not useful with OgreXML files

///////////////////////////////////////////////////////////////////////////////
///
/// Helper data structures for data conversion
//
///////////////////////////////////////////////////////////////////////////////
//@{
namespace KEP {

typedef basic_stringstream<kstring::value_type> kstringStream;

struct bone_anim_t {
	bone_anim_t() :
			length(0) {}

	kstring name; // name of animation
	float length; // total length of this animation

	// keyframes defined at float time values
	//   each keyframe is a delta from the bind pose
	Vec3KFfloat translate;
	QuatKFfloat rotate;
	Vec3KFfloat scale;

	// baked animations in absolute (world/object) coordinates
	vector<Vec3> derivedPosition;
	vector<Quat> derivedRotation;
	vector<Mat4> derivedTransform;
};
typedef vector<bone_anim_t> bone_anim_vector_t;

struct bone_t {
	bone_t() :
			parent(0), id(0) {}

	uint id;
	kstring name;

	// pointer to parent
	bone_t* parent;

	// bind pose position and rotation relative to parent
	Vec3 position;
	Quat rotation;

	// bind pose position and rotation as derived from parent
	// (this is in world space, before any additional transform to the root node)
	Vec3 derivedPosition;
	Quat derivedRotation;
	Mat4 derivedTransform;
	Mat4 derivedTransformInv; // inverse of derivedTransform
	Mat4 derivedTransformTranspose; // for normal transformation: tranpose of derivedTransform = transpose(inverse(inverse(derivedTransformInv)))

	// animation data for this bone for all of the existing animations
	bone_anim_vector_t animations;

	bool IsRoot() { return parent == 0; }

	void ComputeDerivedKeyFrame(uint aAnimIdx, float aFrameTime, bool aFlipZAxis) {
		if (aAnimIdx < 0 || aAnimIdx > animations.size()) {
			/*
			// need m_logger
				LOG4CPLUS_ERROR( m_logger, "ComputeDerivedKeyFrame needs unexistent animation (bone, animation#)"
					<< "( " << name << ", " << aAnimIdx << " )" );
			*/
			return;
		}

		KEP_ASSERT(aAnimIdx >= 0 && aAnimIdx <= animations.size());
		if (aAnimIdx == animations.size()) {
			// assume there was no track for this bone in this animation, so insert a track with 0 deltas at the beginning
			animations.push_back(bone_anim_t());
			animations.back().translate[0] = Vec3(0, 0, 0);
			animations.back().rotate[0] = Quat(1, 0, 0, 0);
			animations.back().scale[0] = Vec3(1, 1, 1);
		}

		Vec3 pos;
		Quat rot;
		if (IsRoot()) {
			pos = position + animations[aAnimIdx].translate.atTime(aFrameTime);
			rot = rotation * animations[aAnimIdx].rotate.atTime(aFrameTime);
			animations[aAnimIdx].derivedPosition.push_back(pos);
			animations[aAnimIdx].derivedRotation.push_back(rot);
		} else {
			//pos = parent->animations[aAnimIdx].derivedTransform.back() * ( position + animations[aAnimIdx].translat e.atTime(aFrameTime) );
			pos = (position + animations[aAnimIdx].translate.atTime(aFrameTime));
			pos = parent->animations[aAnimIdx].derivedPosition.back() + (parent->animations[aAnimIdx].derivedRotation.back() * Quat(1.0, pos[0], pos[1], pos[2]) * !parent->animations[aAnimIdx].derivedRotation.back()).vec3();
			rot = parent->animations[aAnimIdx].derivedRotation.back() * rotation * animations[aAnimIdx].rotate.atTime(aFrameTime);
			animations[aAnimIdx].derivedPosition.push_back(pos);
			animations[aAnimIdx].derivedRotation.push_back(rot);
		}

		Mat4 xform(rot);
		xform.columnV3(3, pos);
		if (aFlipZAxis) {
			xform[2][0] = -xform[2][0];
			xform[2][1] = -xform[2][1];
			xform[0][2] = -xform[0][2];
			xform[1][2] = -xform[1][2];
			xform[2][2] = xform[2][2];
			xform[2][3] = -xform[2][3];
		}
		animations[aAnimIdx].derivedTransform.push_back(xform);

		bone_ptr_set_t::iterator childIt(children.begin()), childEnd(children.end());
		for (; childIt != childEnd; ++childIt)
			if (*childIt)
				(*childIt)->ComputeDerivedKeyFrame(aAnimIdx, aFrameTime, aFlipZAxis);
	}

	void ComputeDerivedTransform(bool aFlipZAxis) {
		if (IsRoot()) {
			derivedPosition = position;
			derivedRotation = rotation;
		} else {
			//derivedPosition = parent->derivedTransform * position;
			// NB: the quaternation math to transform a point by a rotation represented by a quaternion QR is:
			//		QR * Quat(1, x, y, z) * QR^-1 (QR^-1 == QR* <--> |QR| == 1)
			derivedPosition = parent->derivedPosition + (parent->derivedRotation * Quat(1.0, position[0], position[1], position[2]) * !(parent->derivedRotation)).vec3() /*position in parent's space */;
			derivedRotation = parent->derivedRotation * rotation;
		}

		derivedTransform = Mat4(derivedRotation);
		derivedTransform.columnV3(3, derivedPosition);
		if (aFlipZAxis) {
			derivedTransform[2][0] = -derivedTransform[2][0];
			derivedTransform[2][1] = -derivedTransform[2][1];
			derivedTransform[0][2] = -derivedTransform[0][2];
			derivedTransform[1][2] = -derivedTransform[1][2];
			derivedTransform[2][2] = derivedTransform[2][2];
			derivedTransform[2][3] = -derivedTransform[2][3];
		}
		derivedTransformInv = !derivedTransform; // (operator! means inverse)
		derivedTransformTranspose = ~derivedTransform;

		bone_ptr_set_t::iterator childIt(children.begin()), childEnd(children.end());
		for (; childIt != childEnd; ++childIt)
			if (*childIt)
				(*childIt)->ComputeDerivedTransform(aFlipZAxis);
	}

	void SetParent(bone_t* aNewParent) {
		parent = aNewParent;
		parent->AddChild(this);
	}

	void AddChild(bone_t* aNewChild) {
		children.insert(aNewChild);
	}

	void RemoveChild(bone_t* aOldChild) {
		children.erase(aOldChild);
	}

protected:
	// pointers to children
	typedef set<bone_t*> bone_ptr_set_t;
	bone_ptr_set_t children;
};

// map by name to allow quick access based on the bone name
class kstringToBone_map_t : public map<kstring, bone_t> {
};

//! map by bone id to bone_t* to access quickly when converting vertex-bone assignments
typedef map<uint, bone_t*> uintToBonePtr_map_t;
} // namespace KEP

//@}

///////////////////////////////////////////////////////////////////////////////
///
/// Importer functions
///
///////////////////////////////////////////////////////////////////////////////

//@{
bool OgreXMLImporter::Import(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, IN AssetTree* aTree, IN int aFlags /*= IMP_SKELETAL_LEGACY_FLAGS*/) {
	kstringVector ksv;
	ksv.push_back(aFileName);
	return Import(aSkeletalObject, ksv);
}

bool OgreXMLImporter::LoadMeshAndSkeleton(OUT meshPtr& aMeshPtr, OUT skeletonPtr& aSkeletonPtr, IN const kstring& aMeshFileName) {
	SAX2XMLReader* parser = XMLReaderFactory::createXMLReader();
	parser->setFeature(XMLUni::fgSAX2CoreValidation, true);
	parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);

	DefaultErrorHandler* defaultErrorHandler = new DefaultErrorHandler();
	parser->setErrorHandler(defaultErrorHandler);

	ParseOgreXMLMeshHandler* ogreXMLmeshHandler = new ParseOgreXMLMeshHandler();
	Parse_OgreXML_skeleton_Handler* ogreXMLskeletonHandler = new Parse_OgreXML_skeleton_Handler();

	// load DTD grammar so that validation works even if XML file does not include it
	// TODO illamas: load open the file from some editor subdirectory
	kstring editorDirectory = IClientEngine::Instance()->EditorBaseDir();
	vector<kstring> dtdFileNames;
	dtdFileNames.push_back(editorDirectory + "ogremeshxml.dtd");
	dtdFileNames.push_back(editorDirectory + "ogreskeletonxml.dtd");
	if (PreloadDTDs(*parser, dtdFileNames) > 0)
		parser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse, true);

	// do the actual parsing
	bool retval = ExecXMLParse(*parser, *ogreXMLmeshHandler, aMeshFileName);

	// if we finished check if we did it successfully
	if (retval && ogreXMLmeshHandler->Success()) {
		meshPtr loadedMesh(ogreXMLmeshHandler->Get_mesh());
		loadedMesh->name = aMeshFileName;
		aMeshPtr = loadedMesh;

		if (loadedMesh->skeletonlink != "") {
			kstring skeletonFile = loadedMesh->skeletonlink;
			if (skeletonFile.rfind(".xml") == kstring::npos)
				skeletonFile += ".xml";

			kstring baseDir = aMeshFileName.substr(0, aMeshFileName.rfind("\\") + 1);
			skeletonFile = baseDir + skeletonFile;

			retval = ExecXMLParse(*parser, *ogreXMLskeletonHandler, skeletonFile);

			if (retval && ogreXMLskeletonHandler->Success()) {
				aSkeletonPtr = skeletonPtr(ogreXMLskeletonHandler->Get_skeleton());
			} else
				LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ERROR_READING_FILE) << skeletonFile << " .");
		} else {
			retval = false;
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_MESHFILE_HAS_NO_SKELETON) << aMeshFileName);
		}
	} else {
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ERROR_READING_FILE) << aMeshFileName);
	}

	delete defaultErrorHandler;
	delete ogreXMLmeshHandler;
	delete ogreXMLskeletonHandler;
	delete parser;

	return retval;
}

bool OgreXMLImporter::Import(OUT CSkeletonObject& aSkeletalObject, IN const kstringVector& aFileNameVector, IN AssetTree* aTree, IN int aFlags /*= IMP_SKELETAL_LEGACY_FLAGS*/) {
	// TODO: make these import options
	const bool flipZAxis = true;
	const float fps = 15.0f;

	if (aFileNameVector.size() < 1)
		return false;

	skeletonPtr masterSkeleton;

	uint successfulMeshConversions = 0;

	kstringVector::const_iterator fnIt(aFileNameVector.begin()), fnEnd(aFileNameVector.end());
	for (; fnIt != fnEnd; ++fnIt) {
		meshPtr tmpMesh;
		skeletonPtr tmpSkeleton;
		LoadMeshAndSkeleton(tmpMesh, tmpSkeleton, *fnIt);

		// get the skeleton data from the first mesh's skeleton
		if (fnIt == aFileNameVector.begin()) {
			if ((tmpMesh != (meshType*)0) && (tmpSkeleton != (skeletonType*)0)) {
				masterSkeleton = tmpSkeleton;

				// cleanup the destination first just in case
				aSkeletalObject.SafeDelete();

				// this will probably be overwritten by a .cfg file, but let's put something for now
				aSkeletalObject.m_SkeletalName = tmpMesh->skeletonlink.c_str();

				// convert aSkeleton to an intermediate structure (bones) used to transform to the Engine's representation
				kstringToBone_map_t bones;
				ConvertSkeletonToBones(bones, *tmpSkeleton, fps, flipZAxis);

				// convert bones to Engine's representation (CBoneList)
				aSkeletalObject.m_bonesList = new CBoneList();
				ConvertBonesToCBoneList(*(aSkeletalObject.m_bonesList), bones, fps);
				aSkeletalObject.BuildHierarchy(); // now set the parent-child relationships using the names

				// build bounding box (note that right now it only depends on the bone hierarchy)
				// TODO: fix to use other information
				aSkeletalObject.ComputeBoundingBox();
			} else {
				LOG4CPLUS_ERROR(m_logger, "The first mesh file or its skeleton"
											  << " were not read successfully. Cannot continue without a master skeleton.");
				return false;
			}
		}

		if ((tmpMesh != (meshType*)0) && (tmpSkeleton != (skeletonType*)0)) {
			if (flipZAxis)
				FlipMeshZAxis(*tmpMesh);

			// now we got both the mesh and skeleton data, convert it into the Engine internal data structures
			if (AppendMeshToSkeletalObjectAsSections(aSkeletalObject, *tmpMesh, *tmpSkeleton))
				successfulMeshConversions++;
		}
	}

	if (successfulMeshConversions == 0)
		return false;

	return true;
}

//@@Broken - Disabled to avoid confusion [Yanfeng 09/2008]
/*bool OgreXMLImporter::ImportSectionMeshes( OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, int aConfigIndex
					, IN int aLodIndex, int aFlags, IN const AssetDef* aDef )
{	
	const bool flipZAxis = true;

	// 1. load the file and it's skeleton assuming it's a mesh
	meshPtr loadedMeshPtr;
	skeletonPtr loadedSkeletonPtr;
	LoadMeshAndSkeleton( loadedMeshPtr, loadedSkeletonPtr, aFileName );
	if( (loadedMeshPtr == (meshType*)0) || (loadedSkeletonPtr == (skeletonType*)0) )
		return false;

	if( flipZAxis )
		FlipMeshZAxis( *loadedMeshPtr );

	// 2. try to build map from local bone IDs in loadedSkeletonPtr to bone ids in aSkeletalObject
	uintToCBone_map_t bone2boneMap;
	BuildLocalBoneToSkeletonBoneMap( bone2boneMap, *loadedSkeletonPtr, *(aSkeletalObject.m_bonesList) );
	if( bone2boneMap.size() <= 0 )
	{
		LOG4CPLUS_ERROR( m_logger, "The source mesh skeleton does not match any bones in the destination skeleton."
			<< " Source skeleton file = " << loadedMeshPtr->skeletonlink );
		return false;
	}

    
	// 3. copy the data loaded, replacing or appending as needed
	ImportFlags flags;
	flags.intValue = aFlags;
	uint sectionIndex=0;
	
	// loop through sections
	for(POSITION sectionPos = aSkeletalObject.m_alternateConfigurations->GetHeadPosition(); (sectionPos != NULL); sectionIndex++ )
	{
		// get pointer to section
		CAlterUnitDBObject * sectionPtr = (CAlterUnitDBObject *) aSkeletalObject.m_alternateConfigurations->GetNext(sectionPos);
		if( sectionPtr == 0 )
			continue;

		// find a matching source submesh to import data from
		submesh* matchingSubmesh = 0;
		kstring sectionName;
		if( loadedMeshPtr->submeshnames.size() > 0 )
		{	// try to find a submesh mesh in the input .mesh.xml file that matches its name
			submeshVector::iterator smIt( loadedMeshPtr->submeshes.begin() ), smEnd( loadedMeshPtr->submeshes.end() );
			uintToKstringMap::iterator smNameIt;
			for( uint smIndex = 0; smIt != smEnd; ++smIt, smIndex++ )
			{
				GetSectionName( sectionName, *loadedMeshPtr, smIndex, 0 );
				if( CString(sectionName.c_str()) == sectionPtr->m_alterUnitName )
				{
					matchingSubmesh = &(*smIt);
					break;
				}
			}
		}
		else
		{	// try to find a submesh assuming they match the sections in the same order they appear (trying our best to get something in) 
			if( sectionIndex >= loadedMeshPtr->submeshes.size() )
				break;
			else
				matchingSubmesh = &(loadedMeshPtr->submeshes[sectionIndex]);
		}

		// if there is no match skip this section
		if( matchingSubmesh == 0 )
			continue;

		// convert submesh to a skinned mesh
		CDeformableVisObj* newLodMesh = new CDeformableVisObj();
		newLodMesh->m_mesh = new CEXMeshObj();
		if(	!ConvertSubmeshToEngineMesh( *(newLodMesh->m_mesh), *matchingSubmesh, *loadedMeshPtr ) )
		{
			LOG4CPLUS_ERROR( m_logger, "An error occured while converting a submesh to an engine skinned mesh. Submesh name, index: "  
				<< sectionName << ", " << sectionIndex );

			// note: newLodMesh->m_mesh is deleted in SafeDelete()
			newLodMesh->SafeDelete();
			delete newLodMesh;
			continue;
		}
		ConvertBoneAssignments( *newLodMesh, *matchingSubmesh, *loadedMeshPtr, *loadedSkeletonPtr, bone2boneMap );

		// get pointer to config
		CDeformableMesh* configPtr = GetSectionConfigPtr( *sectionPtr, aConfigIndex, flags );

		// ensure there is LOD list
// removed by Jonny 03/04/08 must be replicated using reMesh
//		if( configPtr->m_lodChain == 0 )
//			configPtr->m_lodChain = new CDeformableVisList();
//
//		// insert the new skinned mesh into the right LOD
//		SetConfigLODMeshPtr( *configPtr->m_lodChain, newLodMesh, aLodIndex, flags );
	}

	// TODO: load textures

	return true;
}
*/
bool OgreXMLImporter::ImportSkinnedMesh(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, IN int aSectionIndex, IN int aConfigIndex, IN int aLodIndex, int aFlags, IN AssetData* aDef) {
	const bool flipZAxis = true;

	// try to load the files first, do nothing if it fails
	meshPtr loadedMeshPtr;
	skeletonPtr loadedSkeletonPtr;
	LoadMeshAndSkeleton(loadedMeshPtr, loadedSkeletonPtr, aFileName);
	if ((loadedMeshPtr == (meshType*)0) || (loadedSkeletonPtr == (skeletonType*)0))
		return false;

	if (flipZAxis)
		FlipMeshZAxis(*loadedMeshPtr);

	// try to do the conversion first, do nothing if it failes
	CDeformableVisObj* newLodMesh = new CDeformableVisObj();
	if (!ConvertMeshToLODMesh(*newLodMesh, *loadedMeshPtr, *loadedSkeletonPtr, aSkeletalObject)) {
		newLodMesh->SafeDelete();
		delete newLodMesh;
		return false;
	}

	ImportFlags flags;
	flags.intValue = aFlags;

	// get pointer to config
	CDeformableMesh* configPtr = GetSectionConfigPtr(aSkeletalObject, aSectionIndex, aConfigIndex, flags, "", "");
	if (configPtr == 0)
		return false;

	if (configPtr->m_dimName == CString("New_Config"))
		configPtr->m_dimName = loadedMeshPtr->name.substr(loadedMeshPtr->name.rfind("\\") + 1).c_str();

	// make sure there is an m_lodChain
	// removed by Jonny 03/04/08 must be replicated using reMesh
	//	if( configPtr->m_lodChain == 0 )
	//		configPtr->m_lodChain = new CDeformableVisList();
	//
	//	// insert the new skinned mesh into the right LOD
	//	SetConfigLODMeshPtr( *configPtr->m_lodChain, newLodMesh, aLodIndex, flags );

	// TODO: load textures

	return true;
}

bool OgreXMLImporter::ImportRigidMesh(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, IN int aBoneIndex, IN int aLodIndex, int aFlags, IN AssetData* aDef) {
	if (aSkeletalObject.m_bonesList == 0)
		return false;

	// get a valid bone pointer
	CBoneObject* bonePtr = aSkeletalObject.m_bonesList->GetBoneByIndex(aBoneIndex);
	if (bonePtr == 0)
		return false;

	// Read the input mesh first (ignoring the skeleton link)
	CEXMeshObj* newMesh = new CEXMeshObj();
	if (!Import(*newMesh, aFileName)) {
		newMesh->SafeDelete();
		delete newMesh;
		return false;
	}

	// make sure there is a destination rigid mesh
	if (bonePtr->m_hiarchyMesh == 0)
		bonePtr->m_hiarchyMesh = new CHiarchyMesh();

	// make sure there is an m_lodChain
	if (bonePtr->m_hiarchyMesh->m_lodChain == 0)
		bonePtr->m_hiarchyMesh->m_lodChain = new CHiarcleVisList();

	// create a new rigid mesh with the imported mesh
	CHiarcleVisObj* newLodMesh = new CHiarcleVisObj();
	newLodMesh->m_mesh = newMesh;

	// insert the new rigid mesh into the right LOD
	ImportFlags flags;
	flags.intValue = aFlags;
	SetConfigLODMeshPtr(*bonePtr->m_hiarchyMesh->m_lodChain, newLodMesh, aLodIndex, flags);

	// TODO: load textures

	return true;
}

bool OgreXMLImporter::ImportAnimation(OUT CSkeletonObject& aSkeletalObject, IN const kstring& aFileName, INOUT int& aAnimationIndex, IN int aFlags, IN BOOL bLooping, IN int type, IN int version, IN int minPlayDuration, IN AssetData* aDef) {
	// TODO: use aAnimationIndex and aFlags, now it's importing all the animations

	// TODO: make these import options
	const bool flipZAxis = true;
	const float fps = 15.0f;

	SAX2XMLReader* parser = XMLReaderFactory::createXMLReader();
	parser->setFeature(XMLUni::fgSAX2CoreValidation, true);
	parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);

	// load DTD grammar so that validation works even if XML file does not include it
	kstring editorDirectory = IClientEngine::Instance()->EditorBaseDir();
	vector<kstring> dtdFileNames;
	dtdFileNames.push_back(editorDirectory + "ogreskeletonxml.dtd");
	if (PreloadDTDs(*parser, dtdFileNames) > 0)
		parser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse, true);

	DefaultErrorHandler* defaultErrorHandler = new DefaultErrorHandler();
	parser->setErrorHandler(defaultErrorHandler);

	Parse_OgreXML_skeleton_Handler* ogreXMLskeletonHandler = new Parse_OgreXML_skeleton_Handler();

	// do the actual parsing
	bool retval = ExecXMLParse(*parser, *ogreXMLskeletonHandler, aFileName);

	// if we finished check if we did it successfully
	if (retval && ogreXMLskeletonHandler->Success()) {
		skeletonType* loadedSkeleton = ogreXMLskeletonHandler->Get_skeleton();

		kstringToBone_map_t bones;
		ConvertSkeletonToBones(bones, *loadedSkeleton, fps, flipZAxis);

		kstringToBone_map_t::iterator bIt(bones.begin()), bEnd(bones.end());
		for (; bIt != bEnd; ++bIt) {
			CBoneObject* dstBone = 0;
			int dstIndex;
			if (0 != (dstBone = aSkeletalObject.m_bonesList->GetBoneObjectByName(bIt->first.c_str(), &dstIndex))) {
				ConvertBoneAnimations(*dstBone, bIt->second, fps, bLooping, type, version, minPlayDuration);
			} else {
				LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BONE_NOTFOUND) << bIt->first);
			}
		}

		delete loadedSkeleton;
	} else {
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ERROR_READING_FILE) << aFileName);
	}

	delete defaultErrorHandler;
	delete ogreXMLskeletonHandler;
	delete parser;

	return retval;
}

//@}

///////////////////////////////////////////////////////////////////////////////
///
/// Helper Functions
///
///////////////////////////////////////////////////////////////////////////////

//@{
int OgreXMLImporter::AnimationNameToType(IN kstring& aName) {
	return CBoneAnimationNaming::AnimToNumber(aName.c_str());
}

void OgreXMLImporter::GetSectionName(OUT kstring& aSectionName, IN OgreXML::mesh::meshType& aMesh, unsigned int aIndex, unsigned int aExistingSectionCount) {
	uintToKstringMap::iterator utkIt = aMesh.submeshnames.find(aIndex);
	if (utkIt != aMesh.submeshnames.end())
		aSectionName = utkIt->second;
	else {
		kstringStream sectionNameStream(aSectionName);
		sectionNameStream << "Submesh_" << (aIndex + aExistingSectionCount);
		aSectionName = sectionNameStream.str();
	}
}
//@}

///////////////////////////////////////////////////////////////////////////////
///
/// Data Conversion Functions
///
///////////////////////////////////////////////////////////////////////////////

//@{
void OgreXMLImporter::ConvertSkeletonToBones(OUT kstringToBone_map_t& aBones, IN OgreXML::skeleton::skeletonType& aSkeleton, IN float aFramesPerSecond, IN bool aFlipZAxis) {
	KEP_ASSERT(aFramesPerSecond > 0);

	// get bone data, transforms are assumed to be for the binding pose
	// bone ids are recomputed from 0 to total number of bones (since this is required later in the engine for direct bone access)
	boneSequence::iterator bIt(aSkeleton.bones.bone.begin()), bEnd(aSkeleton.bones.bone.end());
	for (int newBoneId = 0; bIt != bEnd; ++bIt, newBoneId++) {
		if (aBones.find((*bIt)->name) != aBones.end()) {
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BONE_EXISTS) << (*bIt)->name);
			continue;
		}

		aBones[(*bIt)->name] = bone_t();
		kstringToBone_map_t::iterator boneIt = aBones.find((*bIt)->name);
		boneIt->second.id = newBoneId;
		boneIt->second.name = (*bIt)->name;
		boneIt->second.position = Vec3((*bIt)->position.x, (*bIt)->position.y, (*bIt)->position.z);
		boneIt->second.rotation.rotation((*bIt)->rotation.angle, (*bIt)->rotation.axis.x, (*bIt)->rotation.axis.y, (*bIt)->rotation.axis.z, true);
	}

	// get bone hierarchy information (bone-parent relationships)
	boneparentSequence::iterator bpIt(aSkeleton.bonehierarchy.boneparent.begin()), bpEnd(aSkeleton.bonehierarchy.boneparent.end());
	for (; bpIt != bpEnd; ++bpIt) {
		kstringToBone_map_t::iterator boneIt, boneParent;
		boneIt = aBones.find((*bpIt)->bone);
		boneParent = aBones.find((*bpIt)->parent);
		if (boneIt == aBones.end()) {
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BONE_NOTFOUND) << (*bpIt)->bone);
			continue;
		}
		if (boneParent == aBones.end()) {
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BONE_PARENT_NOTFOUND)
										  << "(" << (*bpIt)->bone << ", " << (*bpIt)->parent << ")");
			continue;
		}
		boneIt->second.SetParent(&(boneParent->second));
	}

	// compute derived bone position, orientation and full transform for binding pose
	kstringToBone_map_t::iterator boneIt(aBones.begin()), boneEnd(aBones.end());
	for (; boneIt != boneEnd; ++boneIt) {
		if (boneIt->second.IsRoot())
			boneIt->second.ComputeDerivedTransform(aFlipZAxis);
	}

	// for each animation copy each track (corresponding to a bone) to its respective bone
	if (aSkeleton.animations != (animationsType*)0) {
		animationSequence::iterator animIt, animEnd(aSkeleton.animations->animation.end());
		uint animIdx = 0;
		for (animIt = aSkeleton.animations->animation.begin(); animIt != animEnd; ++animIt, animIdx++) {
			if ((*animIt)->length <= 0) {
				LOG4CPLUS_ERROR(m_logger, "Animation has negative or zero length. Skipping animation. (animation, length) = ("
											  << (*animIt)->name << ", " << (*animIt)->length << ")");
				continue;
			}

			trackSequence::iterator trackIt, trackEnd((*animIt)->tracks.track.end());
			for (trackIt = (*animIt)->tracks.track.begin(); trackIt != trackEnd; ++trackIt) {
				kstringToBone_map_t::iterator trackBone;
				trackBone = aBones.find((*trackIt)->bone);
				if (trackBone == aBones.end()) {
					LOG4CPLUS_ERROR(m_logger, loadStr(IDS_ANIM_FOR_UNKNOWN_BONE)
												  << "(" << (*animIt)->name << ", " << (*trackIt)->bone << ")");
					continue;
				}

				trackBone->second.animations.push_back(bone_anim_t());
				bone_anim_t& trackBoneAnim = trackBone->second.animations.back();
				trackBoneAnim.length = (*animIt)->length;
				trackBoneAnim.name = (*animIt)->name;
				keyframeSequence::iterator kfIt, kfEnd((*trackIt)->keyframes.keyframe.end());
				for (kfIt = (*trackIt)->keyframes.keyframe.begin(); kfIt != kfEnd; ++kfIt) {
					if ((*kfIt)->translate != (translateType*)0)
						trackBoneAnim.translate[(*kfIt)->time] = Vec3((*kfIt)->translate->x, (*kfIt)->translate->y, (*kfIt)->translate->z);

					if ((*kfIt)->scale != (scaleType*)0)
						trackBoneAnim.scale[(*kfIt)->time] = Vec3((*kfIt)->scale->x, (*kfIt)->scale->y, (*kfIt)->scale->z);

					if ((*kfIt)->rotate != (rotateType*)0) {
						Quat rotate;
						rotate.rotation((*kfIt)->rotate->angle, (*kfIt)->rotate->axis.x, (*kfIt)->rotate->axis.y, (*kfIt)->rotate->axis.z, true);
						trackBoneAnim.rotate[(*kfIt)->time] = rotate;
					}
				}
			}

			// sample animation into frames
			float frameInc(1.0f / aFramesPerSecond), frameTime;
			for (boneIt = aBones.begin(); boneIt != boneEnd; ++boneIt) {
				if (boneIt->second.IsRoot()) {
					frameTime = 0;
					//frameTime = frameInc; // DEBUG
					//if( boneIt->second.animations.size() > animIdx )
					{
						//uint totalFrames = (uint) floor( boneIt->second.animations[animIdx].length * aFramesPerSecond );
						uint totalFrames = (uint)floor((*animIt)->length * aFramesPerSecond);
						for (uint frameIdx = 0; frameIdx < totalFrames; frameIdx++, frameTime += frameInc)
							boneIt->second.ComputeDerivedKeyFrame(animIdx, frameTime, aFlipZAxis);
					}
					/*
					else
					{
						LOG4CPLUS_ERROR( m_logger, "A skeleton root bone does not contain any animations. "
							<< " The resulting imported model might be missing animation data. (bone name = "
							<<  boneIt->first << " )" );

					}
					*/
				}
			}
		}
	}
}

void OgreXMLImporter::ConvertBoneAnimations(INOUT CBoneObject& aDstBone, IN bone_t& aSrcBone, IN float aFramesPerSecond, IN BOOL bLooping, IN int type, IN int version, IN int minPlayDuration) {
	if (!aDstBone.m_animationList)
		aDstBone.m_animationList = new CBoneAnimationList();

	bone_anim_vector_t::iterator boneAnimIt, boneAnimEnd(aSrcBone.animations.end());
	for (boneAnimIt = aSrcBone.animations.begin(); boneAnimIt != boneAnimEnd; ++boneAnimIt) {
		CBoneAnimationObject* newCBoneAnim = new CBoneAnimationObject();

		newCBoneAnim->m_totalFrames = (int)floor(boneAnimIt->length * aFramesPerSecond);
		newCBoneAnim->m_millisecondsPerFrame = int(1000.0f / aFramesPerSecond);
		newCBoneAnim->m_looping = bLooping;
		newCBoneAnim->m_animationName = boneAnimIt->name.c_str();
		newCBoneAnim->m_type = type; //AnimationNameToType( boneAnimIt->name );
		newCBoneAnim->m_minPlayDuration = minPlayDuration;
		newCBoneAnim->m_version = version;
		/*
		// leave these to defaults for now
		newCBoneAnim->m_timingCapMax
		newCBoneAnim->m_timingCapMin
		newCBoneAnim->m_timingBased
		newCBoneAnim->m_soundRanges
		*/
		if (newCBoneAnim->m_totalFrames > 0) {
			if ((boneAnimIt->derivedTransform.size() < (UINT)newCBoneAnim->m_totalFrames)) {
				LOG4CPLUS_ERROR(m_logger, "A bone required more animation frames than the number available. (bone, animation, required#, available#)"
											  << "( " << aSrcBone.name << ", " << boneAnimIt->name << ", "
											  << newCBoneAnim->m_totalFrames << ", " << boneAnimIt->derivedTransform.size() << " )");
				continue;
			}

			newCBoneAnim->m_animationData = new AnimationContent[newCBoneAnim->m_totalFrames];

			// time in the input skeleton.xml file is in seconds, so compute frame duration in seconds
			//float frameTime(0), timeInc(1.0f/aFramesPerSecond);
			for (int frameNumber(0); frameNumber < newCBoneAnim->m_totalFrames; frameNumber++ /*, frameTime+=timeInc*/) {
				// get linearly interpolated xform from slerped quaternion + lerped position
				// TODO illamas: use scale information from bones (needs some work in the Engine to support it)
				KEP_ASSERT((uint)frameNumber < boneAnimIt->derivedTransform.size());
				Mat4 frameMat = boneAnimIt->derivedTransform[frameNumber];
				// TODO illamas: use spline (cardinal or whatever) interpolation
				//KEP_ASSERT( (uint)frameNumber < boneAnimIt->derivedPosition.size() );
				//Vec3 framePos = boneAnimIt->derivedPosition[frameNumber];
				Vec3 framePos = frameMat.columnV3(3);
				Vec3 upVector = frameMat.columnV3(1);
				Vec3 dirVector = frameMat.columnV3(2);

				newCBoneAnim->m_animationData[frameNumber].pos.x = framePos[0];
				newCBoneAnim->m_animationData[frameNumber].pos.y = framePos[1];
				newCBoneAnim->m_animationData[frameNumber].pos.z = framePos[2];

				newCBoneAnim->m_animationData[frameNumber].up.x = upVector[0];
				newCBoneAnim->m_animationData[frameNumber].up.y = upVector[1];
				newCBoneAnim->m_animationData[frameNumber].up.z = upVector[2];

				newCBoneAnim->m_animationData[frameNumber].at.x = dirVector[0];
				newCBoneAnim->m_animationData[frameNumber].at.y = dirVector[1];
				newCBoneAnim->m_animationData[frameNumber].at.z = dirVector[2];
			}
		}

		// add animation to bone animation list
		aDstBone.m_animationList->AddTail(newCBoneAnim);
	}
}

void OgreXMLImporter::ConvertBonesToCBoneList(OUT CBoneList& aBoneList, IN kstringToBone_map_t& aBones, IN float aFramesPerSecond) {
	uintToBonePtr_map_t bonesSortedById;

	// use this to add the bones in ID order instead of alphabetically (as they would come from aBones.
	//  this typically will be the order in which they appear
	//  in the skin modifier and also in the skeleton.xml file,
	kstringToBone_map_t::iterator boneIt, boneEnd(aBones.end());
	for (boneIt = aBones.begin(); boneIt != boneEnd; ++boneIt)
		bonesSortedById[boneIt->second.id] = &boneIt->second;

	uint typeCounter = 1;
	uintToBonePtr_map_t::iterator bonePtrIt(bonesSortedById.begin()), bonePtrEnd(bonesSortedById.end());
	for (; bonePtrIt != bonePtrEnd; ++bonePtrIt) {
		bone_t& bone = *(bonePtrIt->second);
		// copy basic bone data
		CBoneObject* newCBone = new CBoneObject();
		newCBone->m_boneName = bone.name.c_str();
		if (bone.parent)
			newCBone->m_parentName = bone.parent->name.c_str();

		newCBone->m_secondaryOverridable = FALSE; // TODO (verify this default is ok)

// transpose as we copy to adapt to DIRECTX row-vector convention
#define MATRIXT_TO_D3DMATRIX(dst, src) \
	for (int i = 0; i < 4; i++)        \
		for (int j = 0; j < 4; j++)    \
			dst[i][j] = src[j][i];

		MATRIXT_TO_D3DMATRIX(newCBone->m_startPosition.m, bone.derivedTransform);
		MATRIXT_TO_D3DMATRIX(newCBone->m_startPositionInv.m, bone.derivedTransformInv);
		MATRIXT_TO_D3DMATRIX(newCBone->m_startPositionTranspose.m, bone.derivedTransformTranspose);
		newCBone->m_boneFrame = new CFrameObj(NULL);
		newCBone->m_boneFrame->AddTransformReplace(newCBone->m_startPosition);

		// copy animations
		ConvertBoneAnimations(*newCBone, bone, aFramesPerSecond, TRUE, 0, 0, 0); // Use default animation settings because they are not available during character import

		// add to bone list
		aBoneList.AddTail(newCBone);
	}
}

bool OgreXMLImporter::ConvertMeshToLODMesh(OUT CDeformableVisObj& aLodMesh, IN meshType& aMesh, IN skeletonType& aSkeleton, IN CSkeletonObject& aSkeletalObject) {
	if (0 == aSkeletalObject.m_bonesList) {
		LOG4CPLUS_ERROR(m_logger, "The destination skeleton does not contain any bones");
		return false;
	}

	// Build map from local bone IDs in aSkeleton to bone ids in aSkeletalObject
	uintToCBone_map_t bone2boneMap;
	BuildLocalBoneToSkeletonBoneMap(bone2boneMap, aSkeleton, *(aSkeletalObject.m_bonesList));
	if (bone2boneMap.size() <= 0) {
		LOG4CPLUS_ERROR(m_logger, "The source mesh skeleton does not match any bones in the destination skeleton."
									  << " Source skeleton file = " << aMesh.skeletonlink);
		return false;
	}

	submesh* submeshPtr = 0;

	if (aMesh.submeshes.size() == 0)
		return false;

	//if( aMesh.submeshes.size() == 1 )
	//	submeshPtr = &aMesh.submeshes.back(); // don't do this, submeshPtr gets deleted below, merge anyhow

	// Otherwise merge all the meshes into a single submesh
	submeshPtr = new submesh();

	MergeSubmeshes(*submeshPtr, aMesh);

	if (aLodMesh.m_mesh == 0)
		aLodMesh.m_mesh = new CEXMeshObj();

	if (!ConvertSubmeshToEngineMesh(*aLodMesh.m_mesh, *submeshPtr, aMesh)) {
		aLodMesh.m_mesh->SafeDelete();
		delete aLodMesh.m_mesh;
		aLodMesh.m_mesh = 0;
		delete submeshPtr;
		return false;
	}

	// set the boneassignments for this submesh
	ConvertBoneAssignments(aLodMesh, *submeshPtr, aMesh, aSkeleton, bone2boneMap);

	delete submeshPtr;

	return true;
}

bool OgreXMLImporter::AppendMeshToSkeletalObjectAsSections(OUT CSkeletonObject& aSkeletalObject,
	IN OgreXML::mesh::meshType& aMesh, IN OgreXML::skeleton::skeletonType& aSkeleton) {
	if (0 == aSkeletalObject.m_bonesList) {
		LOG4CPLUS_ERROR(m_logger, "The destination skeleton does not contain any bones");
		return false;
	}

	// Build map from local bone IDs in aSkeleton to bone ids in aSkeletalObject
	uintToCBone_map_t bone2boneMap;
	BuildLocalBoneToSkeletonBoneMap(bone2boneMap, aSkeleton, *(aSkeletalObject.m_bonesList));
	if (bone2boneMap.size() <= 0) {
		LOG4CPLUS_ERROR(m_logger, "The source mesh skeleton does not match any bones in the destination skeleton."
									  << " Source skeleton file = " << aMesh.skeletonlink);
		return false;
	}

	// increase the count
	int prevMeshCount = aSkeletalObject.m_deformableMeshArrayCount;
	aSkeletalObject.m_deformableMeshArrayCount += aMesh.submeshes.size();

	// create a list of sections if it does not exist already
	if (0 == aSkeletalObject.m_alternateConfigurations)
		aSkeletalObject.m_alternateConfigurations = new CAlterUnitDBObjectList();

	if (aSkeletalObject.m_deformableMeshArrayCount > 0) {
		// allocate or expand aSkeletalObject.m_spawnCfg
		aSkeletalObject.m_charConfig.reSizeConfig(0, aSkeletalObject.m_deformableMeshArrayCount);

		// for each submesh
		submeshVector::iterator smIt(aMesh.submeshes.begin()), smEnd(aMesh.submeshes.end());
		for (uint smIndex = 0; smIt != smEnd; ++smIt, smIndex++) {
			// first try to convert the submesh to a skinned mesh
			CEXMeshObj* newMesh = new CEXMeshObj();
			if (!ConvertSubmeshToEngineMesh(*newMesh, *smIt, aMesh)) {
				newMesh->SafeDelete();
				delete newMesh;
				continue;
			}
			// convert the boneassignments for the new skinned mesh
			CDeformableVisObj* newLodMesh = new CDeformableVisObj();
			newLodMesh->m_mesh = newMesh;
			ConvertBoneAssignments(*newLodMesh, *smIt, aMesh, aSkeleton, bone2boneMap);

			// create a new section
			CAlterUnitDBObject* newSection = new CAlterUnitDBObject();

			// set the name for this section (the submesh name or a default Submesh_# name)
			kstring sectionName;
			GetSectionName(sectionName, aMesh, smIndex, prevMeshCount);
			newSection->m_alterUnitName = sectionName.c_str();

			// create a list of configurations for this section
			newSection->m_configurations = new CDeformableMeshList();
			CDeformableMesh* newConfig = new CDeformableMesh;

			// initialize this (default) configuration
			newConfig->m_dimName = aMesh.name.substr(aMesh.name.rfind("\\") + 1).c_str();
			newConfig->m_publicDim = TRUE;
			// removed by Jonny 03/04/08 must be replicated using reMesh
			//			newConfig->m_lodChain = new CDeformableVisList();
			//
			//			// add to list of LODs
			//			newConfig->m_lodChain->AddTail(newLodMesh);
			// add to list of configurations
			newSection->m_configurations->AddTail(newConfig);
			// add to list of sections
			aSkeletalObject.m_alternateConfigurations->AddTail(newSection);
		}
	}
	return true;
}

void OgreXMLImporter::BuildLocalBoneToSkeletonBoneMap(OUT uintToCBone_map_t& aBtoBmap, IN skeletonType& aSkeleton, IN CBoneList& aCBoneList) {
	boneSequence::iterator bIt, bEnd(aSkeleton.bones.bone.end());
	for (bIt = aSkeleton.bones.bone.begin(); bIt != bEnd; ++bIt) {
		CBoneObject* matchingBonePtr = 0;
		uint matchingBoneId = 0;
		;
		POSITION bonePos = aCBoneList.GetHeadPosition();
		while (bonePos != 0) {
			CBoneObject* tmpBone = (CBoneObject*)aCBoneList.GetNext(bonePos);
			if (tmpBone->m_boneName == CString((*bIt)->name.c_str())) {
				matchingBonePtr = tmpBone;
				break;
			}
			matchingBoneId++;
		}

		if (matchingBonePtr)
			aBtoBmap[(*bIt)->id] = idCBonePair(matchingBoneId, matchingBonePtr);
		else
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BONE_NOTFOUND_IN_MERGEDSKEL) << (*bIt)->name);
	}
}

void OgreXMLImporter::ConvertBoneAssignments(OUT CDeformableVisObj& aLodMesh, IN OgreXML::mesh::submesh& aSubmesh, IN OgreXML::mesh::meshType& aMesh, IN OgreXML::skeleton::skeletonType& aSkeleton, IN uintToCBone_map_t& localBoneIdToCBone) {
	// use the right geometry and boneassignments based on whether the submesh uses shared geometry or not
	mesh::vBoneAssgmtVector& boneassignments = aSubmesh.usesharedvertices ? aMesh.boneassignments : aSubmesh.boneassignments;
	mesh::geometryData& geometry = aSubmesh.usesharedvertices ? aMesh.sharedgeometry : aSubmesh.geometry;

	// figure out where the vertices and normals are
	vec3fVector *verticesVectorPtr(0), *normalsVectorPtr(0);
	vertexBufferVector::iterator vbIt, vbEnd(geometry.vertexbuffers.end());
	for (vbIt = geometry.vertexbuffers.begin(); vbIt != vbEnd; ++vbIt) {
		if (vbIt->positions)
			verticesVectorPtr = &(vbIt->vPos);
		if (vbIt->normals)
			normalsVectorPtr = &(vbIt->vNormal);
	}

	aLodMesh.m_blendedType = 1;
	aLodMesh.m_assignmentCount = 0;

	// classify boneassignments by vertex by placing them in their respective position within a vector of size vertexcount
	typedef map<uint, float> boneIdToWeightMap; // use a map to allow only one entry per bone (may be inefficient for small sizes though)
	typedef vector<boneIdToWeightMap> vertexToBoneWeightsVector;
	vertexToBoneWeightsVector vertex2boneweights(geometry.vertexcount);
	vBoneAssgmtVector::iterator baIt(boneassignments.begin()), baEnd(boneassignments.end());
	for (; baIt != baEnd; ++baIt) {
		if (baIt->vertexindex >= geometry.vertexcount)
			continue;
		//if( vertex2boneweights[baIt->vertexindex].find(baIt->boneindex) == vertex2boneweights[baIt->vertexindex].end() )
		vertex2boneweights[baIt->vertexindex][baIt->boneindex] = baIt->weight;
	}

	aLodMesh.m_assignmentCount = geometry.vertexcount;

	// allocate the vector of vertex-bone assignments in the destination LOD mesh
	aLodMesh.m_blendedVertexAssignments = new VertexAssignmentBlended[aLodMesh.m_assignmentCount];
	memset(aLodMesh.m_blendedVertexAssignments, 0, sizeof(VertexAssignmentBlended) * aLodMesh.m_assignmentCount);
	VertexAssignmentBlended* vbaPtr = aLodMesh.m_blendedVertexAssignments;
	// iterate to fill the vertex-bone assignment array
	vertexToBoneWeightsVector::iterator v2bIt(vertex2boneweights.begin()), v2bEnd = vertex2boneweights.end();
	for (int vindex = 0; v2bIt != v2bEnd; ++v2bIt, vindex++) {
		if (v2bIt->size() > 0) {
			// allocate		the vectors of weights, bones and bound (offset) vectors as needed
			vbaPtr->carriedCount = 0;
			vbaPtr->vertexesCarried = 0;
			vbaPtr->vertexIndex = vindex;
			vbaPtr->boneIndexesCount = v2bIt->size();
			vbaPtr->boneIndexes = new byte[vbaPtr->boneIndexesCount];
			vbaPtr->weightsToBone = new float[vbaPtr->boneIndexesCount];
			vbaPtr->offsetVects = new ABVERTEX0L[vbaPtr->boneIndexesCount];
			// fill the these last three arrays with the bone assignments for this vertex
			boneIdToWeightMap::iterator btwIt(v2bIt->begin()), btwEnd(v2bIt->end());
			for (int bindex = 0; btwIt != btwEnd; ++btwIt, bindex++) {
				// get a pointer to the merged bone from the mapping created before
				uintToCBone_map_t::iterator boundBone(localBoneIdToCBone.find(btwIt->first));
				if (boundBone == localBoneIdToCBone.end()) {
					LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BONEID_NOTFOUND) << btwIt->first);
					vbaPtr->boneIndexes[bindex] = 0;
					vbaPtr->weightsToBone[bindex] = 0;
					continue;
				}

				// Transform the vertex position and normal to the coordinate frame of this bone
				//   for the binding pose, expressed in world coordinates (not relative to its parent)
				vec3f v3fposition = verticesVectorPtr->at(vindex);
				Vec4 position = Vec4(v3fposition.x, v3fposition.y, v3fposition.z, 1);
				Mat4 boneMatInv(boundBone->second.second->m_startPositionInv.m);
				boneMatInv.transpose();
				position = boneMatInv * position;

				vec3f v3fnormal = normalsVectorPtr->at(vindex);
				Vec4 normal = Vec4(v3fnormal.x, v3fnormal.y, v3fnormal.z, 0 /* avoid transform by position */);
				Mat4 boneMatTranspose(boundBone->second.second->m_startPosition.m /* it's already transposed */);
				normal = boneMatTranspose * normal;

				// copy all the data to the VertexAssignmentBlended
				vbaPtr->boneIndexes[bindex] = boundBone->second.first; // TODO illamas: see if boneIndexes can be made unsigned without causing problems
				vbaPtr->weightsToBone[bindex] = btwIt->second;
				vbaPtr->offsetVects[bindex].x = position[0];
				vbaPtr->offsetVects[bindex].y = position[1];
				vbaPtr->offsetVects[bindex].z = position[2];
				vbaPtr->offsetVects[bindex].nx = normal[0];
				vbaPtr->offsetVects[bindex].ny = normal[1];
				vbaPtr->offsetVects[bindex].nz = normal[2];
			}
		}
		vbaPtr++;
	}

	// TODO illamas: fill also the aLodMesh.m_originalArrayVector (just in case)
	// TODO illamas: figure out if we need to fill the aLodMesh.m_blendedVertexAssignmentsWt
	//   or if that's automatically filled when needed later on
}

//@}
