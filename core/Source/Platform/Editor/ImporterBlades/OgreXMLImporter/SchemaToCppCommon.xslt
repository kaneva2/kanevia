<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsd2cpp="http://www.kaneva.com">
	
	<xsl:output method="text" />
	<xsl:strip-space elements="*" />
	
	<xsd2cpp:type xsdt="xs:float">float</xsd2cpp:type>
	<xsd2cpp:type xsdt="xs:int">int</xsd2cpp:type>
	<xsd2cpp:type xsdt="xs:unsignedInt">unsigned int</xsd2cpp:type>
	<xsd2cpp:type xsdt="xs:string">kstring</xsd2cpp:type>
	
	<xsl:param name="parentNamespace" select="/xsd:schema[1]/@targetNamespace" />
	<xsl:param name="mainElementName" select="/xsd:schema[1]/xsd:element[1]/@name" />
	<xsl:param name="mainElementType" select="/xsd:schema[1]/xsd:element[1]/@type" />
	<xsl:param name="fullNamespace" select="concat($parentNamespace,'::',$mainElementName )" />	
	<xsl:param name="baseName" select="concat($parentNamespace,'_',$mainElementName )" />	
	<xsl:param name="headerFileName" select="concat($parentNamespace,$mainElementName,'.h' )" />
	<xsl:param name="implFileName" select="concat($parentNamespace,$mainElementName,'_parse_handler.inl' )" />
		

</xsl:stylesheet>

  