/******************************************************************************
 OgreXMLmesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <vector>
#include <jsRefCountPtr.h>

namespace KEP {

//! Internal Data structures to store data in OgreXML formats
//@{
namespace OgreXML {
//! manual translation from mesh.dtd
//@{
namespace mesh {

//! Common data types
//@{
// triangle definition
typedef unsigned int uint;
struct uintTri {
	uint v1, v2, v3;
};
typedef vector<uintTri> uintTriVector;

// 3D/4D vector definitions
struct vec3f {
	float x, y, z;
};
struct color4f {
	float r, g, b, a;
};
typedef vector<vec3f> vec3fVector;
typedef vector<color4f> color4fVector;
//@}

// vertexbufer definition
struct vertexBuffer {
	static const int MAX_TEXCRD = 8;

	vertexBuffer() :
			positions(false), normals(0), col0(false), col1(false), texCrdCount(0) {
		memset(texCrdDim, 0, sizeof(texCrdDim));
	}

	bool positions;
	bool normals;
	bool col0;
	bool col1;
	uint texCrdCount;
	uint texCrdDim[MAX_TEXCRD];

	vec3fVector vPos;
	vec3fVector vNormal;
	color4fVector vCol0;
	color4fVector vCol1;
	vec3fVector vTexCrd[MAX_TEXCRD];
};

typedef vector<vertexBuffer> vertexBufferVector;

// geometry definition
struct geometryData {
	geometryData() :
			vertexcount(0) {}
	uint vertexcount;
	vertexBufferVector vertexbuffers;
};

// boneassignments definition
struct vBoneAssgmt {
	uint vertexindex;
	uint boneindex;
	float weight;
};
typedef vector<vBoneAssgmt> vBoneAssgmtVector;

// submesh definition
struct submesh {
	enum MESH_OPTYPE { TRI_LIST,
		TRI_STRIP,
		TRI_FAN };
	submesh() :
			usesharedvertices(true), use32bitindexes(false), operationtype(TRI_LIST), facecount(0) {}

	//! attributes
	kstring material;
	bool usesharedvertices;
	bool use32bitindexes;
	MESH_OPTYPE operationtype;

	//! child elements
	//@{
	//! faces
	uint facecount;
	uintTriVector faces;

	//! geometry
	geometryData geometry;

	//! boneassignment data for this mesh's geometry
	vBoneAssgmtVector boneassignments;
	//@}
};
typedef vector<submesh> submeshVector;

// levelofdetail definitions
struct lodmanualData {
	float fromdepthsquared;
	kstring meshname;
};
typedef vector<lodmanualData> lodmanualDataVector;

struct lodfacelist {
	uint submeshindex;
	uint numfaces;
	uintTriVector faces;
};
typedef vector<lodfacelist> lodfacelistVector;

struct lodgeneratedData {
	float fromdepthsquared;
	kstring meshname;
	lodfacelistVector lodfacelists;
};
typedef vector<lodgeneratedData> lodgeneratedDataVector;

struct levelofdetailData {
	uint numlevels;
	bool manual;
	lodmanualDataVector lodmanual;
	lodgeneratedDataVector lodgenerated;
};

// mesh data
typedef map<uint, kstring> uintToKstringMap;
struct meshType {
	// the name of the mesh (this is not in the DTD, but it's useful to store the filename
	kstring name;

	// sharedgeometry data
	geometryData sharedgeometry;

	// submeshes data
	submeshVector submeshes;

	// skeletonlink data
	kstring skeletonlink;

	// boneassignment data for sharedgeometry
	vBoneAssgmtVector boneassignments;

	// levelofdetail definition
	levelofdetailData levelofdetail;

	// submeshnames data
	uintToKstringMap submeshnames;
};
//@}

//! Helper functions
//@{
void FlipGeometryZAxis(INOUT geometryData& aGeometry);

void FlipMeshZAxis(INOUT meshType& aMesh);
//@}

} // end namespace mesh

} // end namespace OgreXML
} // end namespace KEP
