/******************************************************************************
 ARBImporter.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "IAssetsDatabase.h"

namespace KEP {

class ARBImporter : public IAssetImporter {
public:
	ARBImporter(void);
	~ARBImporter(void);

	void Register();

	void Unregister();

	bool Import(IN const string& aFileName);

	bool Import(OUT CSkeletonObject& aSkeletalObject, IN const string& aFileName, IN AssetTree* aTree = NULL, IN int aFlags = IMP_SKELETAL_LEGACY_FLAGS);

	bool Import(OUT CSkeletonObject& aSkeletalObject, IN const kstringVector& aFileNameVector, IN AssetTree* aTree = NULL, IN int aFlags = IMP_SKELETAL_LEGACY_FLAGS);

	bool ImportSkinnedMesh(OUT CSkeletonObject& aSkeletalObject, IN const string& aFileName, IN int aSectionIndex, IN int aConfigIndex, IN int aLodIndex, int flags = (SKELETAL_REPLACE_SECTION | SKELETAL_REPLACE_CONFIG | SKELETAL_REPLACE_LOD), IN AssetData* aDef = NULL);

	bool ImportRigidMesh(OUT RuntimeSkeleton& aSkeletalObject, IN const string& aFileName, IN int aBoneIndex, IN int aLodIndex, int flags = SKELETAL_REPLACE_LOD, IN AssetData* aDef = NULL);

	bool ImportAnimation(INOUT CSkeletonObject& aSkeletalObject, IN const string& aFileName, INOUT int& aAnimGLID, IN int flags, IN AssetData* aDef = NULL, IN const AnimImportOptions* apOptions = NULL);

private:
	//Import-specific function moved from CSkeletonObject class
	BOOL InitBoneSystemFromPlugin(CSkeletonObject& skeleton, CStringA fileName);
	BOOL LoadAnimation(INOUT CSkeletonObject& skeleton, INOUT int& animGLID, IN const CStringA& fileName);
	void InitSkeletalAnimations(CSkeletonObject& skeleton, int animGLID);
	BOOL CheckLegacyBoneAnimationFrameCount(const CSkeletonObject& skeleton, UINT animID, int& numFrames);
};

} // namespace KEP
