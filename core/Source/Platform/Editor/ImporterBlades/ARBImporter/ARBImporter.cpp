/******************************************************************************
 ARBImporter.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "StdAfx.h"
#include "KEPDiag.h"
#include "ARBImporter.h"
#include "CWldObject.h"
#include "ReMeshCreate.h" // RENDERENGINE INTERFACE
#include "CSkeletalClass.h"

#include "CDeformableMeshClass.h"
#include "CBoneClass.h"
#include "SerializeHelper.h"
#include "CSkeletonAnimation.h"
#include "CGlobalInventoryClass.h"
#include "SkeletalAnimationManager.h"
#include "SkeletonConfiguration.h"
#include "RuntimeSkeleton.h"
#include "Common/include/UnicodeMFCHelpers.h"

ARBImporter arbImpInstance;
IAssetImporter* g_pArbImporter = &arbImpInstance;

ARBImporter::ARBImporter(void) :
		IAssetImporter("KanevaSkeletonImporter", "Kaneva Skeletal Object and Animation Importer (*.ARB/*.KAO)") {
}

ARBImporter::~ARBImporter(void) {
}

///
void ARBImporter::Register() {
	// Register with importer factory
	IAssetImporterFactory* factory = IAssetImporterFactory::Instance();
	factory->RegisterImporter(this, KEP_SKA_SKELETAL_OBJECT, string("arb"));
	factory->RegisterImporter(this, KEP_SKA_SECTION_MESHES, string("arb"));
	factory->RegisterImporter(this, KEP_SKA_SECTION_MESHES, string("alt"));
	factory->RegisterImporter(this, KEP_SKA_SKINNED_MESH, string("dim"));
	factory->RegisterImporter(this, KEP_SKA_RIGID_MESH, string("him"));
	factory->RegisterImporter(this, KEP_SKA_ANIMATION, string("arb"));

	factory->RegisterImporter(this, KEP_SKA_SKELETAL_OBJECT, string("kao"));
	factory->RegisterImporter(this, KEP_SKA_SECTION_MESHES, string("kao"));
	factory->RegisterImporter(this, KEP_SKA_SKINNED_MESH, string("kdm"));
	factory->RegisterImporter(this, KEP_SKA_RIGID_MESH, string("ksm"));
	factory->RegisterImporter(this, KEP_SKA_ANIMATION, string("kao"));
}

void ARBImporter::Unregister() {
	// Unregister with importer factory
	IAssetImporterFactory* factory = IAssetImporterFactory::Instance();
	factory->UnregisterImporter(this);
}

///
bool ARBImporter::Import(const string& aFileName) {
	// Guess first based on file extension
	string lowerCaseFileName = aFileName;
	STLToLower(lowerCaseFileName);
	if (lowerCaseFileName.rfind(".arb") != string::npos) {
		// create a CSkeletonObject and import an ARB
	}

	return false;
}

BOOL ARBImporter::InitBoneSystemFromPlugin(CSkeletonObject& skeleton, CStringA fileName) {
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, fileName, CFile::modeRead, &exc);
	SkeletonConfiguration* skeletalCfg = skeleton.m_skeletonConfiguration;
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	int physiqueModCount = 0;
	int firstFrame = 0;
	int lastFrame = 0;
	int frameCount = 0;

	// default fileVersion == 0 for .arb files
	int fileVersion = 0;

	// default (minimum fileVersion == 1 for .kao files, we'll determine exact number from file
	string lcFileName = fileName;
	STLToLower(lcFileName);
	if (StrSameFileExt(lcFileName, "kao"))
		fileVersion = 1;

	BEGIN_SERIALIZE_TRY

	CArchive the_in_Archive(&fl, CArchive::load);

	if (fileVersion > 0)
		the_in_Archive >> fileVersion;

	the_in_Archive >> skeleton.m_runtimeSkeleton->m_SkeletalName >> skeleton.m_runtimeSkeleton->m_frameSpeed >> skeleton.m_runtimeSkeleton->m_ticksPerFrame >> skeleton.m_runtimeSkeleton->m_bonesList >> skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount >> physiqueModCount >> firstFrame >> lastFrame >> frameCount;

	skeleton.InitBones();
	// set these prior to streaming in CDeformableVisObj's
	MeshRM::Instance()->SetCurrentBoneList(skeleton.m_runtimeSkeleton->m_bonesList);
	MeshRM::Instance()->SetProgressiveLoadFlag(skeleton.m_runtimeSkeleton->m_loadProgressively);
	// init the mesh cache
	for (DWORD i = 0; i < CSKELETALLODOBJECT_MAXLODS; i++) {
		skeleton.m_runtimeSkeleton->m_clonedReMeshes[i].resize(skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount);
		skeleton.m_runtimeSkeleton->m_MeshCache[i].resize(skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount);
		for (DWORD j = 0; j < (DWORD)skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount; j++) {
			skeleton.m_runtimeSkeleton->m_clonedReMeshes[i][j] = NULL; // RENDERENGINE INTERFACE
			skeleton.m_runtimeSkeleton->m_MeshCache[i][j] = NULL; // RENDERENGINE INTERFACE
		}
	}

	if (skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount > 0) {
		ASSERT(skeleton.m_runtimeSkeleton->m_deformableMeshArrayV2 == 0);
		skeleton.m_runtimeSkeleton->m_deformableMeshArrayV2 = new DeformableMeshStructV2[skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount];
		for (int loop = 0; loop < skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount; loop++) {
			// start new lod chain
			MeshRM::Instance()->SetCurrentLOD(0);
			the_in_Archive >> skeleton.m_runtimeSkeleton->m_deformableMeshArrayV2[loop].m_defMeshVisualUnit;
		}
	}

	skeletalCfg->m_alternateConfigurations = NULL;
	//port configurations
	skeletalCfg->m_alternateConfigurations = new CAlterUnitDBObjectList();
	if (skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount > 0) { //valid deformables
		skeletalCfg->m_charConfig.InitBase(skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount);
		for (int loop = 0; loop < skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount; loop++) {
			CAlterUnitDBObject* newCfg = new CAlterUnitDBObject();
			newCfg->m_configurations = new CDeformableMeshList();
			CDeformableMesh* dMesh = NULL;

			// Memory leak: see disposal of m_deformableMeshArrayV2 at end of loop--note only array is disposed, not pointers to CDeformableMesh
			// We can safely reuse this pointer directly instead of cloning as it will stay and not be referred by anyone else
			// Alternatively we can clone it and dispose the original but it's practically not necessary.
			dMesh = skeleton.m_runtimeSkeleton->m_deformableMeshArrayV2[loop].m_defMeshVisualUnit;
			//m_deformableMeshArrayV2[loop].m_defMeshVisualUnit->Clone(&dMesh,g_pd3dDevice, m_bonesList);

			newCfg->m_alterUnitName = skeleton.m_runtimeSkeleton->m_deformableMeshArrayV2[loop].m_defMeshVisualUnit->m_dimName;
			// 04/07/08 Jonny equippable items import without default material bug fix.
			if (dMesh->m_supportedMaterials == NULL) {
				CMaterialObject* mat = NULL;
				dMesh->m_material->Clone(&mat);
				dMesh->m_supportedMaterials = new CObList();
				dMesh->m_supportedMaterials->AddTail(mat);
			}

			newCfg->m_configurations->AddTail(dMesh);
			skeletalCfg->m_alternateConfigurations->AddTail(newCfg);
		}
		delete[] skeleton.m_runtimeSkeleton->m_deformableMeshArrayV2;
		skeleton.m_runtimeSkeleton->m_deformableMeshArrayV2 = 0;
	} //end valid deformables

	InitSkeletalAnimations(skeleton, GLID_INVALID);

	the_in_Archive.Close();
	fl.Close();

	skeleton.BuildHierarchy();

	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

//------------------------------------------------------------------//
//------------------------------------------------------------------//
//--------------------load animation--------------------------------//
BOOL ARBImporter::LoadAnimation(INOUT CSkeletonObject& skeleton, INOUT int& animGLID, IN const CStringA& fileName) {
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, fileName, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	CSkeletonObject* tmpSkeletal = new CSkeletonObject();
	RuntimeSkeleton* ghostSkeletalPtr = tmpSkeletal->m_runtimeSkeleton;
	int physiqueModCount = 0;
	int firstFrame = 0;
	int lastFrame = 0;
	int frameCount = 0;

	// default fileVersion == 0 for .arb files
	int fileVersion = 0;

	// default (minimum fileVersion == 1 for .kao files, we'll determine exact number from file
	string lcFileName = fileName;
	STLToLower(lcFileName);
	if (StrSameFileExt(lcFileName, "kao"))
		fileVersion = 1;

	BEGIN_SERIALIZE_TRY;

	CArchive the_in_Archive(&fl, CArchive::load);

	if (fileVersion > 0)
		the_in_Archive >> fileVersion;

	the_in_Archive >> ghostSkeletalPtr->m_SkeletalName >> ghostSkeletalPtr->m_frameSpeed >> ghostSkeletalPtr->m_ticksPerFrame >> ghostSkeletalPtr->m_bonesList >> ghostSkeletalPtr->m_deformableMeshArrayCount >> physiqueModCount >> firstFrame >> lastFrame >> frameCount;

	if (ghostSkeletalPtr->m_deformableMeshArrayCount > 0) {
		ASSERT(ghostSkeletalPtr->m_deformableMeshArrayV2 == 0);
		ghostSkeletalPtr->m_deformableMeshArrayV2 = new DeformableMeshStructV2[skeleton.m_runtimeSkeleton->m_deformableMeshArrayCount];
		for (int loop = 0; loop < ghostSkeletalPtr->m_deformableMeshArrayCount; loop++)
			the_in_Archive >> ghostSkeletalPtr->m_deformableMeshArrayV2[loop].m_defMeshVisualUnit;
	}

	the_in_Archive.Close();
	fl.Close();

	int numFrames = -1;
	if (!CheckLegacyBoneAnimationFrameCount(*tmpSkeletal, 0, numFrames)) {
		ASSERT(FALSE);

		ghostSkeletalPtr->SafeDelete();
		return FALSE;
	}

	CSkeletonAnimation* newAnim = new CSkeletonAnimation(ghostSkeletalPtr->GetBoneCount(), 0);

	int replaceIndex = ANIM_INVALID;
	if (IS_VALID_GLID(animGLID))
		replaceIndex = skeleton.GetAnimationIndexByGLID(animGLID);

	//apply animation data
	for (POSITION posLoc = ghostSkeletalPtr->m_bonesList->GetHeadPosition(); posLoc != NULL;) {
		CBoneObject* pGhostBone = (CBoneObject*)ghostSkeletalPtr->m_bonesList->GetNext(posLoc);
		int boneIndex = skeleton.GetBoneIndexByName(pGhostBone->m_boneName);
		if (boneIndex != -1) { //found bone
			CBoneObject* pBone = (CBoneObject*)skeleton.GetBoneByIndex(boneIndex);
			POSITION animPos = pGhostBone->m_animationList->FindIndex(0); //@@Watch: Only import first animation from specified KAO file (?) [Yanfeng 01/2009]
			if (animPos) {
				CBoneAnimationObject* animationObject = (CBoneAnimationObject*)pGhostBone->m_animationList->GetAt(animPos);
				CBoneAnimationObject* animObjectClone = NULL;
				animationObject->Clone(&animObjectClone);

				animObjectClone->m_msPerFrame = animationObject->m_msPerFrame;
				animObjectClone->m_totalFrames = lastFrame;
				animationObject->m_msPerFrame = animationObject->m_msPerFrame;
				animationObject->m_totalFrames = lastFrame;

				newAnim->m_boneKeyframes[boneIndex] = animationObject;

				if (!IS_VALID_GLID(animGLID)) {
					if (pBone->m_animationList)
						pBone->m_animationList->AddTail(animObjectClone);
				} else {
					if (pBone->m_animationList) {
						//replacing
						POSITION animToReplacePos = pBone->m_animationList->FindIndex(replaceIndex);
						if (animToReplacePos) {
							CBoneAnimationObject* animationObjectExist = (CBoneAnimationObject*)pBone->m_animationList->GetAt(animToReplacePos);
							animationObjectExist->SafeDelete();
							delete animationObjectExist;
							animationObjectExist = 0;

							pBone->m_animationList->InsertAfter(animToReplacePos, animObjectClone);
							pBone->m_animationList->RemoveAt(animToReplacePos);
						}
					}
				}
			}
		}
	}

	InitSkeletalAnimations(skeleton, animGLID);

	ghostSkeletalPtr->SafeDelete();
	ghostSkeletalPtr = 0;

	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

void ARBImporter::InitSkeletalAnimations(CSkeletonObject& skeleton, int animGLID /*=ANIM_GLID_NONE*/) {
	// put animations into animation table
	UINT numAnimations = skeleton.m_runtimeSkeleton->m_bonesList->GetSize();

	for (UINT i = 0; i < numAnimations; i++) {
		CSkeletonAnimation* pNewAnim = new CSkeletonAnimation(skeleton.m_runtimeSkeleton->m_bonesList, i);

		if (!IS_VALID_GLID(animGLID)) {
			pNewAnim->Hash();
			if (!AnimationRM::Instance()->AddAnimation(pNewAnim)) {
				delete pNewAnim;
			} else {
				animGLID = pNewAnim->m_glid;
				skeleton.QueueNewAnimation(animGLID, true);
			}
		} else {
			if (!AnimationRM::Instance()->ReplaceAnimation(pNewAnim, animGLID)) {
				delete pNewAnim;
			}

			skeleton.DeleteAnimationByGLID(animGLID);
			skeleton.QueueNewAnimation(animGLID, true);
		}
	}
}

BOOL ARBImporter::CheckLegacyBoneAnimationFrameCount(const CSkeletonObject& skeleton, UINT animID, int& numFrames) {
	ASSERT(animID < skeleton.m_runtimeSkeleton->m_animHeaders.size());
	if (animID >= skeleton.m_runtimeSkeleton->m_animHeaders.size())
		return FALSE;

	BOOL errorFound = FALSE;
	int tmpFrameCnt = -1;

	for (POSITION posBone = skeleton.m_runtimeSkeleton->m_bonesList->GetHeadPosition(); posBone != NULL;) {
		CBoneObject* pBone = dynamic_cast<CBoneObject*>(skeleton.m_runtimeSkeleton->m_bonesList->GetAt(posBone));
		ASSERT(pBone != NULL && pBone->m_animationList != NULL); // Must have a animation list because we are inquiring about an existing animation

		if (pBone != NULL && pBone->m_animationList != NULL) {
			ASSERT(pBone->m_animationList->GetCount() == skeleton.m_runtimeSkeleton->m_animHeaders.size());
			if (pBone->m_animationList->GetCount() != skeleton.m_runtimeSkeleton->m_animHeaders.size())
				errorFound = TRUE;

			POSITION posAnim = pBone->m_animationList->FindIndex(animID);
			ASSERT(posAnim != NULL);

			if (posAnim != NULL) {
				CBoneAnimationObject* pAnim = dynamic_cast<CBoneAnimationObject*>(pBone->m_animationList->GetAt(posAnim));
				ASSERT(pAnim != NULL);

				if (pAnim) {
					if (tmpFrameCnt == -1)
						tmpFrameCnt = pAnim->m_totalFrames;

					if (tmpFrameCnt == pAnim->m_totalFrames)
						continue;
				}
			}
		}

		errorFound = TRUE;
	}

	if (!errorFound)
		numFrames = tmpFrameCnt;

	return !errorFound;
}

bool ARBImporter::Import(OUT CSkeletonObject& aSkeletalObject, IN const string& aFileName, IN AssetTree* aTree, IN int aFlags /*= IMP_SKELETAL_LEGACY_FLAGS*/) {
	if (InitBoneSystemFromPlugin(aSkeletalObject, CStringA(aFileName.c_str())) == TRUE) {
		aSkeletalObject.ComputeBoundingBox();
		return true;
	}
	return false;
}

bool ARBImporter::Import(OUT CSkeletonObject& aSkeletalObject, IN const kstringVector& aFileNameVector, IN AssetTree* aTree, IN int aFlags /*= IMP_SKELETAL_LEGACY_FLAGS*/) {
	if (aFileNameVector.size() > 0)
		return Import(aSkeletalObject, aFileNameVector[0]); //@@Watch: incomplete impl -- Yanfeng 09/2008

	return false;
}

bool ARBImporter::ImportSkinnedMesh(OUT CSkeletonObject& aSkeletalObject, IN const string& aFileName, IN int aSectionIndex, IN int aConfigIndex, IN int aLodIndex, int aFlags, IN AssetData* aDef) {
	ImportFlags flags;
	flags.intValue = aFlags;

	// get pointer to config
	CDeformableMesh* configPtr = GetSectionConfigPtr(aSkeletalObject, aSectionIndex, aConfigIndex, flags, "", "");

	if (configPtr == 0)
		return false;

	// set this prior to streaming in CDeformableVisObj's											// RENDERENGINE INTERFACE
	MeshRM::Instance()->SetCurrentBoneList(aSkeletalObject.m_runtimeSkeleton->m_bonesList);
	MeshRM::Instance()->SetCurrentLOD(aLodIndex == -1 ? 0 : aLodIndex);

	// rely on the implementation already available for this
	// TODO: take into account the flags.lod, it replaces by default
	configPtr->SetLevel(aLodIndex, aFileName.c_str());
	return true;
}

bool ARBImporter::ImportRigidMesh(OUT RuntimeSkeleton& aSkeletalObject, IN const string& aFileName, IN int aBoneIndex, IN int aLodIndex, int aFlags, IN AssetData* aDef) {
	if (aSkeletalObject.m_bonesList == 0)
		return false;

	CBoneObject* bonePtr = aSkeletalObject.m_bonesList->GetBoneByIndex(aBoneIndex);

	if (bonePtr == 0)
		return false;

	//@@Note: one hierarchy mesh per bone only here to retain backward compatibility with legacy exporter
	CHiarchyMesh* pHMesh = NULL;
	if (bonePtr->m_rigidMeshes.empty()) {
		pHMesh = new CHiarchyMesh();
		bonePtr->m_rigidMeshes.push_back(pHMesh);
	} else
		pHMesh = *bonePtr->m_rigidMeshes.begin();

	// make sure there is an m_lodChain
	if (pHMesh->m_lodChain == 0)
		pHMesh->m_lodChain = new CHiarcleVisList();

	// rely on the implementation already available for this
	// TODO: take into account the flags.lod, it replaces by default
	pHMesh->SetLevel(aLodIndex, aFileName.c_str());

	return false;
}

bool ARBImporter::ImportAnimation(INOUT CSkeletonObject& aSkeletalObject, IN const string& aFileName, INOUT int& aAnimGLID, IN int aFlags, IN AssetData* aDef /*= NULL*/, IN const AnimImportOptions* apOptions /*= NULL*/) {
	// TODO: take flags into account (now it replaces by default)
	return (LoadAnimation(aSkeletalObject, aAnimGLID, aFileName.c_str()) == TRUE);
}
