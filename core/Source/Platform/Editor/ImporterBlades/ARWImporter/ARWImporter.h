/******************************************************************************
 ARWImporter.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "IAssetsDatabase.h"

class ARWImporter : public IAssetImporter {
public:
	ARWImporter(void);
	~ARWImporter(void);

	void Register();

	void Unregister();

	bool Import(IN const string& aFileName);

	bool Import(OUT CEXMeshObj& aStaticMesh, IN const string& aFileName, IN AssetData* aDef = NULL, IN int aFlags = IMP_STATIC_LEGACY_FLAGS);
};
