/******************************************************************************
 ARWImporter.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "StdAfx.h"

#include "ARWImporter.h"

#include "CWldObject.h"

ARWImporter arwImpInstance;
IAssetImporter* g_pArwImporter = &arwImpInstance;

ARWImporter::ARWImporter(void) :
		IAssetImporter("KanevaMeshImporter", "Kaneva Static Mesh Importer (*.ARW/*.KZM)") {
}

ARWImporter::~ARWImporter(void) {
}

///
void ARWImporter::Register() {
	// Register with importer factory
	IAssetImporterFactory* factory = IAssetImporterFactory::Instance();
	factory->RegisterImporter(this, KEP_STATIC_MESH, string("arw"));
	factory->RegisterImporter(this, KEP_STATIC_MESH, string("kzm"));
}

void ARWImporter::Unregister() {
	// Unregister with importer factory
	IAssetImporterFactory* factory = IAssetImporterFactory::Instance();
	factory->UnregisterImporter(this);
}

///
bool ARWImporter::Import(const string& aFileName) {
	// Guess first based on file extension
	string lowerCaseFileName = aFileName;
	STLToLower(lowerCaseFileName);
	if (lowerCaseFileName.rfind(".arw") != string::npos) {
		// create a CEXMeshObj and import an ARW
	}

	return false;
}

bool ARWImporter::Import(OUT CEXMeshObj& aStaticMesh, IN const string& aFileName, IN AssetData* aDef, IN int aFlags /*= IMP_STATIC_LEGACY_FLAGS*/) {
	return (aStaticMesh.LoadGeometryFromPlugin(aFileName.c_str()) == TRUE ? true : false);
}
