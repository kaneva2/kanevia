/******************************************************************************
TreeCtrlHelper.h

Copyright (c) 2004-2008 Kaneva, Inc. All Rights Reserved Worldwide
Kaneva Proprietary and Confidential
******************************************************************************/

#include <stdafx.h>
#include "TreeCtrlHelper.h"

#define VERIFY_RETURN(x, retVal) \
	{                            \
		VERIFY(x);               \
		if (!x)                  \
			return retVal;       \
	}
#define VERIFY_RETVOID(x) \
	{                     \
		VERIFY(x);        \
		if (!x)           \
			return;       \
	}

//! ********************************************************
//! \brief Adds a item as a root object
//! \param name Name of the new tree item.
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM TreeCtrlHelper::AddTreeRoot(CStringW name, UINT id, CWnd* pParentWnd, int iconIndex /*=-1*/, LPARAM param /*= NULL*/, BOOL bSelect /*= FALSE*/) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, NULL);

	return AddTreeRoot(name, hWndTreeCtrl, iconIndex, param, bSelect);
}

//! ********************************************************
//! \brief Adds a item as a root object
//! \param name Name of the new tree item.
//! \param hWndTreeCtrl HWND to the tree control being accessed.
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM TreeCtrlHelper::AddTreeRoot(CStringW name, HWND hWndTreeCtrl, int iconIndex /*=-1*/, LPARAM param /*= NULL*/, BOOL bSelect /*= FALSE*/) {
	const wchar_t* nameLoc = name;
	TV_INSERTSTRUCT treeCtrlItem;

	treeCtrlItem.hParent = TVI_ROOT;
	treeCtrlItem.hInsertAfter = TVI_LAST;
	treeCtrlItem.item.mask = TVIF_TEXT | TVIF_PARAM;
	treeCtrlItem.item.pszText = const_cast<wchar_t*>(nameLoc);
	treeCtrlItem.item.lParam = param; // Initialize item data to NULL

	if (-1 != iconIndex)
		treeCtrlItem.item.mask |= TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	treeCtrlItem.item.iImage = iconIndex;
	treeCtrlItem.item.iSelectedImage = iconIndex;

	HTREEITEM hItem = TreeView_InsertItem(hWndTreeCtrl, &treeCtrlItem);
	if (bSelect && hItem)
		TreeView_Select(hWndTreeCtrl, hItem, TVGN_CARET);

	return hItem;
}

//! ********************************************************
//! \brief Adds a child item to a tree control
//! \param name Name of the new tree item.
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem The parent item where the new item is about to be added under
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM TreeCtrlHelper::AddTreeChild(CStringW name, UINT id, CWnd* pParentWnd, HTREEITEM treeItem, int iconIndex /*=-1*/, LPARAM param /*= NULL*/, BOOL bSelect /*= FALSE*/) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, NULL);

	return AddTreeChild(name, hWndTreeCtrl, treeItem, iconIndex, param, bSelect);
}

//! ********************************************************
//! \brief Adds a child item to a tree control
//! \param name Name of the new tree item.
//! \param hWndTreeCtrl HWND to the tree control being accessed.
//! \param treeItem The parent item where the new item is about to be added under
//! \param iconIndex Index of the icon (in IMAGELIST) for display.
//! \param param The optional LPARAM to be associated to this tree item.
HTREEITEM TreeCtrlHelper::AddTreeChild(CStringW name, HWND hWndTreeCtrl, HTREEITEM treeItem, int iconIndex /*=-1*/, LPARAM param /*= NULL*/, BOOL bSelect /*= FALSE*/) {
	const wchar_t* nameLoc = name;
	TV_INSERTSTRUCT treeCtrlItem;

	treeCtrlItem.hInsertAfter = TVI_LAST;
	treeCtrlItem.item.mask = TVIF_TEXT | TVIF_PARAM;
	treeCtrlItem.hParent = treeItem;
	treeCtrlItem.item.pszText = const_cast<wchar_t*>(nameLoc);
	treeCtrlItem.item.lParam = param;

	if (-1 != iconIndex)
		treeCtrlItem.item.mask |= TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_STATE;
	treeCtrlItem.item.iImage = iconIndex;
	treeCtrlItem.item.iSelectedImage = iconIndex;
	treeCtrlItem.item.state = TVIS_BOLD;

	HTREEITEM hNewItem = TreeView_InsertItem(hWndTreeCtrl, &treeCtrlItem);
	if (hNewItem) {
		if (bSelect)
			TreeView_Select(hWndTreeCtrl, hNewItem, TVGN_CARET);
		else
			TreeView_EnsureVisible(hWndTreeCtrl, hNewItem);
	}
	return hNewItem;
}

//! ********************************************************
//! \brief Delete a node from a tree control
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem The parent item where the new item is about to be added under
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL TreeCtrlHelper::DelTreeNode(UINT id, CWnd* pParentWnd, HTREEITEM treeItem) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, FALSE);

	return DelTreeNode(hWndTreeCtrl, treeItem);
}

//! ********************************************************
//! \brief Delete a node from a tree control
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \param treeItem The parent item where the new item is about to be added under
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL TreeCtrlHelper::DelTreeNode(HWND hWndTreeCtrl, HTREEITEM treeItem) {
	return TreeView_DeleteItem(hWndTreeCtrl, treeItem);
}

//! ********************************************************
//! \brief Delete all nodes from a tree control
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL TreeCtrlHelper::DelAllTreeNodes(UINT id, CWnd* pParentWnd) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, FALSE);

	return DelAllTreeNodes(hWndTreeCtrl);
}

//! ********************************************************
//! \brief Delete all nodes from a tree control
//! \param hWndTreeCtrl HWND of the tree control being accessed.
//! \return TRUE if succeeded. FALSE otherwise (e.g. not found).
BOOL TreeCtrlHelper::DelAllTreeNodes(HWND hWndTreeCtrl) {
	return TreeView_DeleteAllItems(hWndTreeCtrl);
}

//! ********************************************************
//! \brief Retrieve item text from a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item where text is to be retrieved.
//! \return item text
CStringW TreeCtrlHelper::GetTreeItemText(UINT id, CWnd* pParentWnd, HTREEITEM treeItem) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, L"NONE");

	return GetTreeItemText(hWndTreeCtrl, treeItem);
}

//! ********************************************************
//! \brief Retrieve item text from a tree item
//! \param hWndTreeCtrl HWND to the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item where text is to be retrieved.
//! \return item text
CStringW TreeCtrlHelper::GetTreeItemText(HWND hWndTreeCtrl, HTREEITEM treeItem) {
	const int c_bufferLength = 200;
	wchar_t buffer[c_bufferLength];
	TVITEM tvItem;

	tvItem.mask = TVIF_TEXT;
	tvItem.hItem = treeItem;
	tvItem.cchTextMax = c_bufferLength;
	tvItem.pszText = buffer;

	BOOL res = TreeView_GetItem(hWndTreeCtrl, &tvItem);
	if (res == FALSE) {
		return L"NONE";
	} // if..res == FALSE

	return tvItem.pszText;
}

//! ********************************************************
//! \brief Set item text for a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item
//! \param newText The new item text to be stored.
//! \return TRUE if succeeded. FALSE otherwise
BOOL TreeCtrlHelper::SetTreeItemText(UINT id, CWnd* pParentWnd, HTREEITEM treeItem, CStringW newText) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, FALSE);

	return SetTreeItemText(hWndTreeCtrl, treeItem, newText);
}

//! ********************************************************
//! \brief Set item text for a tree item
//! \param hWndTreeCtrl HWND to the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item
//! \param newText The new item text to be stored.
//! \return TRUE if succeeded. FALSE otherwise
BOOL TreeCtrlHelper::SetTreeItemText(HWND hWndTreeCtrl, HTREEITEM treeItem, CStringW newText) {
	TVITEM tvItem;

	tvItem.mask = TVIF_TEXT;
	tvItem.hItem = treeItem;
	const wchar_t* nameLoc = newText;
	tvItem.pszText = const_cast<wchar_t*>(nameLoc);
	return TreeView_SetItem(hWndTreeCtrl, &tvItem);
}

//! ********************************************************
//! \brief Retrieve data associated with a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item where the data is stored
//! \return data (NULL if not set or not found).
LPARAM TreeCtrlHelper::GetTreeItemData(UINT id, CWnd* pParentWnd, HTREEITEM treeItem) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, NULL);

	return GetTreeItemData(hWndTreeCtrl, treeItem);
}

//! ********************************************************
//! \brief Retrieve data associated with a tree item
//! \param hWndTreeCtrl HWND to the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item where the data is stored
//! \return data (NULL if not set or not found).
LPARAM TreeCtrlHelper::GetTreeItemData(HWND hWndTreeCtrl, HTREEITEM treeItem) {
	TVITEM tvItem;
	tvItem.mask = TVIF_PARAM;
	tvItem.hItem = treeItem;
	tvItem.lParam = NULL;

	BOOL res = TreeView_GetItem(hWndTreeCtrl, &tvItem);
	if (res == FALSE)
		return NULL;

	return tvItem.lParam;
}

//! ********************************************************
//! \brief Associate data with a tree item
//! \param id Dialog item ID for the tree control.
//! \param pParentWnd Parent dialog where tree control resides.
//! \param treeItem HTREEITEM to the tree item to which the data will be associated.
//! \param data Data to be associated to the tree item.
//! \return TRUE if successed. FALSE otherwise.
BOOL TreeCtrlHelper::SetTreeItemData(UINT id, CWnd* pParentWnd, HTREEITEM treeItem, LPARAM data) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, FALSE);

	return SetTreeItemData(hWndTreeCtrl, treeItem, data);
}

//! ********************************************************
//! \brief Associate data with a tree item
//! \param hWndTreeCtrl HWND to the tree control being accessed.
//! \param treeItem HTREEITEM to the tree item to which the data will be associated.
//! \param data Data to be associated to the tree item.
//! \return TRUE if successed. FALSE otherwise.
BOOL TreeCtrlHelper::SetTreeItemData(HWND hWndTreeCtrl, HTREEITEM treeItem, LPARAM data) {
	TVITEM tvItem;
	tvItem.mask = TVIF_PARAM;
	tvItem.hItem = treeItem;
	tvItem.lParam = data;

	return TreeView_SetItem(hWndTreeCtrl, &tvItem);
}

//! ********************************************************
//! \brief Obtain the index of specified child item relative to its parent.
int TreeCtrlHelper::GetTreeChildIndex(HWND hWndTreeCtrl, HTREEITEM item) {
	int indexLoc = 0;
	HTREEITEM itemLoc = item;
	while (itemLoc != NULL) {
		itemLoc = TreeView_GetPrevSibling(hWndTreeCtrl, itemLoc);
		if (itemLoc) {
			indexLoc++;
		}
	}

	return indexLoc;
}

//! ********************************************************
//! \brief Obtain the index of specified child item relative to its parent.
int TreeCtrlHelper::GetTreeChildIndex(UINT id, CWnd* pParentWnd, HTREEITEM item) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, -1);

	return GetTreeChildIndex(hWndTreeCtrl, item);
}

//! ********************************************************
//! \brief Obtain the depth of specified child item from root
int TreeCtrlHelper::GetTreeChildDepth(HWND hWndTreeCtrl, HTREEITEM item) {
	int counterA = 0;
	HTREEITEM parent = item;
	while (1) {
		parent = TreeView_GetParent(hWndTreeCtrl, parent);
		if (!parent) {
			break;
		}

		counterA++;
	}

	return counterA;
}

//! ********************************************************
//! \brief Obtain the depth of specified child item from root
int TreeCtrlHelper::GetTreeChildDepth(UINT id, CWnd* pParentWnd, HTREEITEM item) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, -1);

	return GetTreeChildDepth(hWndTreeCtrl, item);
}

//! ********************************************************
//! \brief Obtain selected tree item
HTREEITEM TreeCtrlHelper::GetSelectedItem(UINT id, CWnd* pParentWnd) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, NULL);

	return GetSelectedItem(hWndTreeCtrl);
}

//! ********************************************************
//! \brief Obtain selected tree item
HTREEITEM TreeCtrlHelper::GetSelectedItem(HWND hWndTreeCtrl) {
	return TreeView_GetSelection(hWndTreeCtrl);
}

//! ********************************************************
//! \brief Set tree image list.
//! \return Previous image list if succeeded. NULL otherwise.
HIMAGELIST TreeCtrlHelper::SetTreeImageList(UINT id, CWnd* pParentWnd, HIMAGELIST hImageList, int type /* = TVSIL_NORMAL*/) {
	HWND hWndTreeCtrl = NULL;
	pParentWnd->GetDlgItem(id, &hWndTreeCtrl);
	VERIFY_RETURN(hWndTreeCtrl != NULL, NULL);

	return SetTreeImageList(hWndTreeCtrl, hImageList);
}

//! ********************************************************
//! \brief Set tree image list.
//! \return Previous image list if succeeded. NULL otherwise.
HIMAGELIST TreeCtrlHelper::SetTreeImageList(HWND hWndTreeCtrl, HIMAGELIST hImageList, int type /* = TVSIL_NORMAL*/) {
	return TreeView_SetImageList(hWndTreeCtrl, hImageList, type);
}
