// NewGameInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "KEPEdit.h"
#include "NewGameInfoDlg.h"
#include "UnicodeMFCHelpers.h"

// CNewGameInfoDlg dialog

IMPLEMENT_DYNAMIC(CNewGameInfoDlg, CDialog)

CNewGameInfoDlg::CNewGameInfoDlg(bool allowStandAloneCreation, CWnd* pParent /*=NULL*/) :
		CDialog(CNewGameInfoDlg::IDD, pParent), m_name(""), m_desc(""), m_standAloneApp(FALSE), m_allowStandAloneCreation(allowStandAloneCreation) {
}

CNewGameInfoDlg::~CNewGameInfoDlg() {
}

BOOL CNewGameInfoDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	if (m_allowStandAloneCreation) {
		GetDlgItem(IDC_3D_APP)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_STANDALONE)->ShowWindow(SW_SHOW);
	}
	return true;
}

void CNewGameInfoDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_GAME_NAME, m_name);
	DDX_Text(pDX, IDC_GAME_DESC, m_desc);
	DDX_Check(pDX, IDC_3D_APP, m_standAloneApp);
}

BEGIN_MESSAGE_MAP(CNewGameInfoDlg, CDialog)
ON_EN_CHANGE(IDC_GAME_NAME, &CNewGameInfoDlg::OnEnChangeGameName)
END_MESSAGE_MAP()

// CNewGameInfoDlg message handlers
void CNewGameInfoDlg::OnOK() {
	UpdateData(TRUE);
	if (m_name.IsEmpty()) // must be at least http://x
	{
		AfxMessageBox(L"Name must not be empty", MB_ICONINFORMATION);
		return;
	}
	CDialog::OnOK();
}

void CNewGameInfoDlg::OnEnChangeGameName() {
	CStringW s;
	GetDlgItemText(IDC_GAME_NAME, s);
	int badChar = s.FindOneOf(L"'-\\/:*<>?|\"");
	if (badChar >= 0) {
		s.Delete(badChar);
		SetDlgItemText(IDC_GAME_NAME, s);
		AfxMessageBox(L"Name must not contain the following characters: '-\\/:*<>?|\"");
	}
}
