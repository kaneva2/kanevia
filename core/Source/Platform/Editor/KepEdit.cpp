/******************************************************************************
 KEPEdit.cpp

 Copyright (c) 2014 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "KEPEdit.h"
#include "MainFrm.h"
#include "ClientStrings.h"
#include "KEPEditDoc.h"
#include "KEPEditView.h"
#include "shlwapi.h"
#include "IClientEngine.h"
#include "KEPEditCommandInfo.h"
#include "common/keputil/VLDWrapper.h"

#include "KEPCommon.h"
#include "KEPHelpers.h"
#include "Common\KEPUtil\Helpers.h"
#include "Common\KEPUtil\CrashReporter.h"
#include "UnicodeMFCHelpers.h"
#include "KEPStar.h"

#include "LogHelper.h"
static LogInstance("Exception");

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define PROJECT_NAME "KEPEdit"

#pragma data_seg("SHARED") // notify linker of shared
// section in options
LONG g_lUsageCount = -1;
#pragma data_seg()

BEGIN_MESSAGE_MAP(CKEPEditApp, CWinApp)
ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
ON_COMMAND(ID_HELP_SENDFEEDBACK, OnSendFeedBack)
ON_COMMAND(ID_Menu_Help_GamePub, OnMenu_Help_GamePub)
// Standard file based document commands
// Standard print setup command
ON_COMMAND_RANGE(ID_FILE_MRU_FILE1, ID_FILE_MRU_FILE10, MRUFileHandler)

// From ClientEngine to support Editor UI

ON_BN_CLICKED(IDC_BUTTON_QUIT, OnButtonQuit)
ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
ON_BN_CLICKED(IDC_BUTTON_TREETEST, OnButtonTreetest)
ON_BN_CLICKED(IDC_BUTTON_OBJECTLOAD, OnButtonObjectload)
ON_BN_CLICKED(IDC_BUTTON_OBJECTDELETE, OnButtonObjectdelete)
ON_BN_CLICKED(IDC_BUTTON_SWITCH_OBJECT, OnButtonSwitchObject)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN1, OnTextureFileOpen1)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN2, OnTextureFileOpen2)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN3, OnTextureFileOpen3)
ON_BN_CLICKED(IDC_BUTTON_FILEOPEN4, OnTextureFileOpen4)
ON_BN_CLICKED(IDC_BUTTON_BKEDITORSWTCH, OnBUTTONBKEDITORswtch)
ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
ON_BN_CLICKED(IDC_BUTTON_ADDNEWSTATIC_BK, OnButtonAddnewstaticBk)
ON_BN_CLICKED(IDC_BUTTON_CONTROLS, OnButtonControls)
ON_BN_CLICKED(IDC_BUTTON_TEST_EFFECT, OnButtonTestEffect)
ON_BN_CLICKED(IDC_BUTTON_STOP_EFFECT, OnButtonStopEffect)
ON_BN_CLICKED(IDC_BUTTON_MOVIES, OnButtonMovies)
ON_BN_CLICKED(IDC_BUTTON_TESTMOVIE, OnButtonTestmovie)
ON_BN_CLICKED(IDC_BTN_ENTITY_CREATENEW, OnBtnEntityCreatenew)
ON_BN_CLICKED(IDC_BTN_ENTITY_ACTIVATE, OnBtnEntityActivate)
ON_BN_CLICKED(IDC_BUTTON_SW_ENTITIES, OnButtonSwEntities)
ON_BN_CLICKED(IDC_BUTTON_TESTFULLSCREEN, OnButtonTestfullscreen)
ON_BN_CLICKED(IDC_BTN_VIEWWIREFRAME, OnBtnViewwireframe)
ON_BN_CLICKED(IDC_BUTTON_VIEWSMOOTHSHADED, OnButtonViewsmoothshaded)
ON_BN_CLICKED(IDC_BTN_ENTITY_MOVEMENT, OnBtnEntityMovement)
ON_BN_CLICKED(IDC_BTN_ENTITY_DEACTIVATE, OnBtnEntityDeactivate)
ON_BN_CLICKED(IDC_BTN_DELETE_ENTITY, OnBtnDeleteEntity)
//missingres	ON_BN_CLICKED( IDC_BTN_ENTITY_DELETEANIM, OnBTNENTITYdeleteAnim )
//missingres	ON_BN_CLICKED( IDC_BTN_ENTITY_DELETECHLD, OnBTNENTITYDeleteChld )
//missingres	ON_BN_CLICKED( IDC_BTN_ENTITY_ADDCHILD, OnBtnEntityAddchild )
ON_BN_CLICKED(IDC_BTN_NEWALL, OnBtnNewall)
ON_BN_CLICKED(IDB_BTN_CameraViewportsSw, OnBTNCameraViewportsSw)
ON_BN_CLICKED(IDB_CAMERA_CreateNewCamera, OnCAMERACreateNewCamera)
ON_BN_CLICKED(IDC_BTN_CREATENEWVIEWPORT, OnBTNCreateNewViewport)
ON_BN_CLICKED(IDC_BTN_CAMERAVPORTACTIVATE, OnBTNCameraVportActivate)
ON_BN_CLICKED(IDC_BTN_VPORTDEACTIVATE, OnBTNvportDeactivate)

ON_BN_CLICKED(IDC_BTN_ADD_RUNTIME_CAMERA, OnBtnAddRuntimeCamera)
ON_BN_CLICKED(IDC_BTN_REMOVE_RUNTIME_CAMERA, OnBtnRemoveRuntimeCamera)

ON_BN_CLICKED(IDC_BTN_ADD_RUNTIME_VIEWPORT, OnBtnAddRuntimeViewport)
ON_BN_CLICKED(IDC_BTN_REMOVE_RUNTIME_VIEWPORT, OnBtnRemoveRuntimeViewport)

ON_BN_CLICKED(IDC_BTN_MOUSECREATE, OnBTNMOUSEcreate)
ON_BN_CLICKED(IDC_BTN_UIMOUSESW, OnBTNUImouseSW)
ON_BN_CLICKED(IDC_BTN_MOUSEFILEBROWSE, OnBTNMOUSEfileBrowse)
ON_BN_CLICKED(IDC_BTN_MOUSEACTIVATE, OnBTNMOUSEactivate)
ON_BN_CLICKED(IDC_BTN_MOUSEDELETE, OnBTNMOUSEdelete)
ON_BN_CLICKED(IDC_BTN_MOUSEDEACTIVATE, OnBTNMOUSEdeactivate)
ON_BN_CLICKED(IDC_BTN_2DINTERFACESW, OnBTN2DINTERFACEsw)
ON_BN_CLICKED(IDC_BTN_2D_CREATECOLLECTION, OnBTN2DCreateCollection)
ON_BN_CLICKED(IDC_BTN_2D_ADDUNIT, OnBTN2DADDunit)
ON_BN_CLICKED(IDC_BTN_2D_ACTIVATE, OnBtn2dActivate)
ON_BN_CLICKED(IDC_BTN_2D_DEACTIVATE, OnBtn2dDeactivate)
ON_BN_CLICKED(IDC_BTN_2D_BROWSEFILEUP, OnBTN2DBrowseFileUp)
ON_BN_CLICKED(IDC_BTN_2D_BROWSEFILEDOWN, OnBTN2DBrowseFileDown)
ON_BN_CLICKED(IDC_BTN_2DGETSELECTEDINFO, OnBTN2DgetSelectedInfo)
ON_BN_CLICKED(IDC_BTN_2D_APPLYMODIFICATIONS, OnBTN2DApplyModifications)
ON_BN_CLICKED(IDC_BTN_2D_DELETECOLLECTION, OnBTN2DDeleteCollection)
ON_BN_CLICKED(IDC_BTN_2D_DELETEUNIT, OnBTN2DDeleteUnit)
ON_BN_CLICKED(IDC_BUTTON_ENVIORMENTSW, OnBUTTONEnviormentSw)
//missingres	ON_BN_CLICKED( IDC_BTN_ENV_BCKRGB, OnBTNENVBckRgb )
ON_BN_CLICKED(IDC_BTN_ENV_APPLYCHANGES, OnBTNENVApplyChanges)
//missingres	ON_BN_CLICKED( IDC_BTN_ENV_MATCH, OnBTNENVMatch )
ON_BN_CLICKED(IDC_BTN_OBJECT_MATERIAL, OnBTNObjectMaterial)
//missingres	ON_BN_CLICKED( IDC_BTN_ENTITY_MATERIAL, OnBTNEntityMaterial )
ON_BN_CLICKED(IDC_BUTTON_TEXT_ADDTEXTCOLLECTION, OnBUTTONtextAddTextCollection)
ON_BN_CLICKED(IDC_BTN_TEXTFONTSW, OnBTNtextFontSW)
ON_BN_CLICKED(IDC_BUTTON_TEXT_ADDTEXTOBJECT, OnBUTTONtextAddTextObject)
ON_BN_CLICKED(IDC_BTN_TEXT_ACTIVATE, OnBTNtextActivate)
ON_BN_CLICKED(IDC_BTN_TEXT_DEACTIVATE, OnBTNtextDeactivate)
ON_BN_CLICKED(IDC_BUTTON_TEXT_TEXTCOLOR, OnBUTTONtextTextColor)
ON_BN_CLICKED(IDC_BTN_TEXT_APPLYCHANGES, OnBTNtextApplyChanges)
ON_BN_CLICKED(IDC_BTN_TEXT_GETCURRENTINFO, OnBTNtextGetCurrentInfo)
ON_BN_CLICKED(IDC_BTN_TEXT_APPLYFONT, OnBTNtextApplyFont)
ON_BN_CLICKED(IDC_BUTTON_TEXT_ADDFONT, OnBUTTONtextAddFont)
ON_BN_CLICKED(IDC_BTN_TEXT_APPLYSIZECHANGE, OnBTNtextApplySizeChange)
ON_BN_CLICKED(IDC_BUTTON_TEXT_DELETEFONT, OnBUTTONtextDeleteFont)
ON_BN_CLICKED(IDC_BUTTON_TEXT_DELETETEXTCOLLECTION, OnBUTTONtextDeleteTextCollection)
ON_BN_CLICKED(IDC_BUTTON_TEXT_DELETETEXTOBJECT, OnBUTTONtextDeleteTextObject)
ON_BN_CLICKED(IDC_BTN_TEST_SAVE, OnBTNtestSave)
ON_BN_CLICKED(IDC_BTN_TEST_LOAD, OnBTNtestLoad)
ON_BN_CLICKED(IDC_BTN_CAMVPORT_SAVE, OnBTNcamVportSave)
ON_BN_CLICKED(IDC_BTN_CAMVPORT_LOAD, OnBTNcamVportLoad)
ON_BN_CLICKED(IDC_BTN_DELETECAMERA, OnBTNDeleteCamera)
ON_BN_CLICKED(IDC_BTN_DELETEVIEWPORT, OnBTNDeleteViewport)
ON_BN_CLICKED(IDC_BTN_VIEWPORTSAPPLYCHNGS, OnBTNUpdateViewport)
ON_BN_CLICKED(IDC_BTN_UPDATE_RUNTIME_VIEWPORT, OnBTNUpdateRuntimeViewport)
ON_BN_CLICKED(IDC_BTN_2D_SAVECONFIG, OnBTN2DSaveConfig)
ON_BN_CLICKED(IDC_BTN_2D_LOADCONFIG, OnBTN2DLoadConfig)
ON_BN_CLICKED(IDC_BTN_EXPLOSIONDBSW, OnBTNExplosionDBSW)
ON_BN_CLICKED(IDC_BTN_EXP_ADDEXPLOSION, OnBTNEXPAddExplosion)
ON_BN_CLICKED(IDC_BTN_EXP_LOADEXPLOSIONS, OnBTNEXPLoadExplosions)
ON_BN_CLICKED(IDC_BTN_EXP_SAVEEXPLOSIONS, OnBTNEXPSaveExplosions)
ON_BN_CLICKED(IDC_BTN_EXP_DELETEEXPLOSION, OnBTNEXPDeleteExplosion)
ON_BN_CLICKED(IDC_BTN_CAMERAADVANCED, OnBTNCameraAdvanced)
ON_BN_CLICKED(IDC_BTN_MOVIE_ADDMOVIE, OnBTNMovieAddMovie)
ON_BN_CLICKED(IDC_BTN_MOVIE_TESTMOVIE, OnBTNMovieTestMovie)
ON_BN_CLICKED(IDC_BTN_MOVIE_EDITMOVIE, OnBTNMovieEditMovie)
ON_BN_CLICKED(IDC_BTN_MOVIE_DELETEMOVIE, OnBTNMovieDeleteMovie)
ON_BN_CLICKED(IDC_BTN_MOVIESSAVEDB, OnBTNMoviesSaveDB)
ON_BN_CLICKED(IDC_BTN_MOVIESLOADDB, OnBTNMoviesLoadDB)
ON_BN_CLICKED(IDC_BTN_MOVIE_STOPMOVIES, OnBTNMovieStopMovies)
ON_BN_CLICKED(IDC_BTN_CONTROLADDOBJECT, OnBTNControlAddObject)
ON_BN_CLICKED(IDC_BTN_CONTROLADDEVENT, OnBTNControlAddEvent)
ON_BN_CLICKED(IDC_BTN_CONTROLCFGSAVE, OnBTNControlCfgSave)
ON_BN_CLICKED(IDC_BTN_CONTROLCFGLOAD, OnBTNControlCfgLoad)
ON_BN_CLICKED(IDC_BTN_CONTROLDELETEOBJECT, OnBTNControlDeleteObject)
ON_BN_CLICKED(IDC_BTN_CONTROLDELETEEVENT, OnBTNControlDeleteEvent)
ON_LBN_SELCHANGE(IDC_LIST_CONTROLDB, OnSelchangeLISTControlDB)
ON_BN_CLICKED(IDC_BTN_CONTROLEDITOBJECT, OnBTNControlEditObject)
ON_BN_CLICKED(IDC_BTN_CONTROLEDITEVENT, OnBTNControlEditEvent)
ON_BN_CLICKED(IDC_BTN_OBJECTARRAYADD, OnBTNObjectArrayAdd)
ON_BN_CLICKED(IDC_BTN_UI_BASEADVANCED, OnBTNUIBaseAdvanced)
ON_BN_CLICKED(IDC_BTN_ENTITY_PROPERTIES, OnBTNENTITYProperties)
ON_BN_CLICKED(IDC_BTN_OBJECT_PROPERTIES, OnBTNOBJECTProperties)
ON_BN_CLICKED(IDC_BTN_OBJ_DELETELIGHT, OnBTNOBJDeleteLight)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDLIGHT, OnBtnObjAddlight)
ON_BN_CLICKED(IDC_BTN_ENT_MERGEDATA, OnBTNENTMergeData)
//missingres	ON_BN_CLICKED( IDC_BTN_ENT_ADDLIGHT, OnBTNENTAddLight )
ON_BN_CLICKED(IDC_BTN_UI_MUSIC, OnBTNUIMusic)
ON_BN_CLICKED(IDC_BTN_MUSIC_ASSIGNTRACK, OnBTNMusicAssignTrack)
ON_BN_CLICKED(IDC_BTN_MUSIC_ASSIGNWAV, OnBTNMusicAssignWav)
ON_BN_CLICKED(IDC_BTN_MUSIC_UNASSIGNTRACK, OnBTNMusicUnassignTrack)
ON_BN_CLICKED(IDC_BTN_MUSIC_UNASSIGNWAV, OnBTNMusicUnassignWav)
ON_BN_CLICKED(IDC_BTN_MUSICBROWSEWAV, OnBTNMusicBrowseWav)
ON_BN_CLICKED(IDC_BTN_UI_TEXTUREASSIGN, OnBTNUITextureAssign)
ON_BN_CLICKED(IDC_BTN_UI_MISSILEDATABASE, OnBTNUIMissileDatabase)
ON_BN_CLICKED(IDC_BTN_MIS_ADDMISSILE, OnBTNMISAddMissile)
ON_BN_CLICKED(IDC_BTN_MIS_DELETEMISSILE, OnBTNMISDeleteMissile)
ON_BN_CLICKED(IDC_BTN_MIS_SAVE, OnBTNMISsave)
ON_BN_CLICKED(IDC_BTN_MIS_LOAD, OnBTNMISLoad)
ON_BN_CLICKED(IDC_BTN_UI_BULLETHOLE, OnBTNUIBulletHole)
ON_BN_CLICKED(IDC_BTN_BH_ADDHOLE, OnBTNBHAddHole)
ON_BN_CLICKED(IDC_BTN_BH_BROWSE, OnBTNBHBrowse)
ON_BN_CLICKED(IDC_BTN_BH_ALPHATEXTURE, OnBTNBHAlphaTexture)
ON_BN_CLICKED(IDC_BTN_BH_DELETEHOLE, OnBTNBHDeleteHole)
ON_BN_CLICKED(IDC_BTN_BH_LOAD, OnBTNBHload)
ON_BN_CLICKED(IDC_BTN_BH_MATERIAL, OnBTNBHMaterial)
ON_BN_CLICKED(IDC_BTN_BH_SAVE, OnBTNBHSave)
ON_BN_CLICKED(IDC_BTN_ENTITY_SHADOW, OnBTNEntityshadow)
//missingres	ON_BN_CLICKED( IDC_BTN_ENT_ANIMPROPERTIES, OnBTNENTAnimProperties )
//missingres	ON_BN_CLICKED( IDC_BTN_ENTITY_SOUND, OnBTNEntitySound )
//missingres	ON_BN_CLICKED( IDC_BTN_ENTITY_SETSOUNDCENTER, OnBTNEntitysetSoundCenter )
ON_BN_CLICKED(IDC_BTN_OBJ_SOUND, OnBTNOBJSound)
ON_BN_CLICKED(IDC_BTN_OBJ_ALPHATEXTURE, OnBTNOBJAlphaTexture)
//missingres	ON_BN_CLICKED( IDC_BTN_ENT_ANIMSOUND, OnBTNENTAnimSound )
ON_BN_CLICKED(IDC_BTN_TCPIP_STARTHOST, OnBTNTCPIPStartHost)
ON_BN_CLICKED(IDC_BTN_TCPIP_STOPHOST, OnBTNTCPIPStopHost)
ON_BN_CLICKED(IDC_BTN_TCPIP_JOINHOST, OnBTNTCPIPJoinHost)
ON_BN_CLICKED(IDC_BTN_UI_MULTIPLAYER, OnBTNUIMultiplayer)
//missingres	ON_BN_CLICKED( IDC_BTN_ENTITY_COMBINEENTITYVIS, OnBTNEntityCombineEntityVis )
//missingres	ON_BN_CLICKED( IDC_BTN_ENT_CONTENTPROPERTIES, OnBTNENTContentProperties )
ON_BN_CLICKED(IDC_BTN_TEX_ADDCOLLECTION, OnBTNTEXAddCollection)
ON_BN_CLICKED(IDC_BTN_TEX_DELETECOLLECTION, OnBTNTEXDeleteCollection)
ON_BN_CLICKED(IDC_BTN_UI_ANIMTEXTUREDB, OnBTNUIAnimTextureDB)
ON_BN_CLICKED(IDC_BTN_TEX_LOADCOLLECTION, OnBTNTEXLoadCollection)
ON_BN_CLICKED(IDC_BTN_TEX_SAVECOLLECTION, OnBTNTEXSaveCollection)
ON_BN_CLICKED(IDC_BTN_ENT_LOADDB, OnBTNENTLoadDB)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEDB, OnBTNENTSaveDB)
ON_BN_CLICKED(IDC_BTN_ENT_REMOVE_LOD, OnBTNENTRemoveLOD)
ON_BN_CLICKED(IDC_BTN_UI_SCRIPT, OnBtnUiScript)
ON_BN_CLICKED(IDC_BTN_SCR_ADDSCRIPT, OnBTNSCRAddScript)
ON_BN_CLICKED(IDC_BTN_SCR_DELETEEVENT, OnBTNSCRDeleteEvent)
//missingres	ON_BN_CLICKED( IDC_BTN_SCR_LOADEXECFILE, OnBTNSCRLoadExecFile )
ON_BN_CLICKED(IDC_BTN_SCR_BUILD, OnBTNSCRBuild)
ON_BN_CLICKED(IDC_BTN_SCR_EDITEVENT, OnBTNSCREditEvent)
ON_BN_CLICKED(IDC_BTN_OBJ_EDITLIGHT, OnBTNOBJEditLight)
ON_BN_CLICKED(IDC_BTN_ENT_LOADENTITY, OnBTNENTLoadEntity)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEENTITY, OnBTNENTSaveEntity)
ON_BN_CLICKED(IDC_BTN_MS_SAVEDB, OnBTNMSSaveDB)
ON_BN_CLICKED(IDC_BTN_MS_LOADDB, OnBTNMSLoadDB)
ON_BN_CLICKED(IDC_BTN_TXTDB_ADD, OnBtnTxtdbAdd)
ON_BN_CLICKED(IDC_BTN_TXTDB_DELETE, OnBtnTxtdbDelete)
ON_BN_CLICKED(IDC_BTN_TXTDB_EDIT, OnBtnTxtdbEdit)
ON_BN_CLICKED(IDC_BTN_TXTDB_REINIT_ALL, OnBtnTxtdbReinitAll)
//missingres	ON_BN_CLICKED( IDC_BTN_TXTDB_REINIT_FROM_DISK, OnBtnTxtdbReinitFromDisk )
ON_BN_CLICKED(IDC_BTN_TXTDB_SET_SUBTRACTION, OnBtnTxtdbSetSubtraction)
ON_BN_CLICKED(IDC_BTN_OBJ_CLEARTEXCOORDS, OnBTNOBJClearTexCoords)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDTEXCOORDS, OnBTNOBJAddTexCoords)
//missingres	ON_BN_CLICKED( IDC_BTN_SCR_SETTOSCENE, OnBTNSCRSetToScene )
ON_BN_CLICKED(IDC_BTN_SCR_CLEAR_SCENE, OnBTNSCRClearScene)
//missingres	ON_BN_CLICKED( IDC_BTN_SCR_LOADFROMSCENE, OnBTNSCRLoadFromScene )
ON_BN_CLICKED(IDC_BTN_NT_ADVANCED, OnBTNNTAdvanced)
ON_BN_CLICKED(IDC_BTN_NT_BUILDTXTRECORD, OnBTNNTBuildTxtRecord)
ON_BN_CLICKED(IDC_BTN_UI_HUDINTERFACE, OnBTNUIHudInterface)
ON_BN_CLICKED(IDC_BTN_HUD_ADDDISPLAYUNIT, OnBTNHUDAddDisplayUnit)
ON_BN_CLICKED(IDC_BTN_HUD_ADDNEWHUD, OnBTNHUDAddNewHud)
ON_BN_CLICKED(IDC_BTN_HUD_SAVEDB, OnBTNHUDSaveDB)
ON_BN_CLICKED(IDC_BTN_HUD_LOADDB, OnBTNHUDLoadDB)
ON_LBN_SELCHANGE(IDC_LIST_HUD_DB, OnSelchangeListHudDb)
ON_BN_CLICKED(IDC_BTN_HUD_DELETEHUD, OnBTNHUDDeleteHud)
ON_BN_CLICKED(IDC_BTN_HUD_DELETEDISPLAYUNIT, OnBTNHUDDeleteDisplayUnit)
ON_BN_CLICKED(IDC_BTN_HUD_REPLACEUNIT, OnBTNHUDReplaceUnit)
ON_BN_CLICKED(IDC_BTN_ENT_UPDATE, OnBTNENTUpdate)
ON_LBN_SELCHANGE(IDC_LIST_TXTDB_DATABASE, OnSelchangeLISTTXTDBDatabase)
//missingres	ON_BN_CLICKED( IDC_CHECK_TEX_BLTON, OnCHECKTEXBltOn )
ON_BN_CLICKED(IDC_BTN_UI_PARTICLESYS, OnBTNUIParticleSys)
ON_BN_CLICKED(IDC_BTN_PART_EDITSYSTEM, OnBTNPARTEditSystem)
ON_BN_CLICKED(IDC_BTN_PART_LOADDATABASE, OnBTNPARTLoadDatabase)
ON_BN_CLICKED(IDC_BTN_UI_SPAWNGEN, OnBTNUISpawnGen)
ON_BN_CLICKED(IDC_BTN_SPAWN_ADDGEN, OnBTNSpawnAddGen)
ON_BN_CLICKED(IDC_BTN_SPAWN_DELETEGEN, OnBTNSpawnDeleteGen)
ON_BN_CLICKED(IDC_BTN_SPAWN_EDITGEN, OnBTNSpawnEditGen)
ON_BN_CLICKED(IDC_BTN_OBJECT_BUILDCOLLISIONMAP, OnBTNObjectBuildCollisionMap)
ON_NOTIFY(TVN_SELCHANGED, IDC_OBJECT_TREE, OnSelchangedObjectTree)
ON_BN_CLICKED(IDC_BTN_OBJECTS_ADDGROUP, OnBTNObjectsAddGroup)
ON_BN_CLICKED(IDC_BTN_OBJECTS_DELETEGROUP, OnBTNObjectsDeleteGroup)
ON_BN_CLICKED(IDC_BTN_OBJECTSEDITGROUP, OnBTNObjectsEditGroup)
ON_LBN_SELCHANGE(IDC_LIST_OBJECTS_GROUPLIST2BOX, OnSelchangeLISTObjectsGroupList2Box)
ON_LBN_DBLCLK(IDC_LIST_OBJECTS_GROUPLIST2BOX, OnDblclkLISTObjectsGroupList2Box)
ON_BN_CLICKED(IDC_BTN_GROUP_MODIFIER, OnBTNGroupModifier)
ON_NOTIFY(NM_DBLCLK, IDC_OBJECT_TREE, OnDblclkObjectTree)
ON_BN_CLICKED(IDC_BTN_UI_MERGE, OnBTNUIMerge)
ON_BN_CLICKED(IDC_BTN_TCP_DISCONNECTPLAYER, OnBTNTCPDisconnectPlayer)
ON_BN_CLICKED(IDC_BTN_TCP_SERVERLOAD, OnBTNTCPServerLoad)
ON_BN_CLICKED(IDC_BTN_TCP_SERVERSAVE, OnBTNTCPServerSave)
ON_BN_CLICKED(IDC_BTN_OPTIMIZESTATES, OnBTNOptimizeStates)
ON_BN_CLICKED(IDC_BTN_TCP_NEWACCOUNT, OnBTNTCPNewAccount)
ON_BN_CLICKED(IDC_BTN_TCP_DELETEACCOUNT, OnBTNTCPDeleteAccount)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDTRIGGER, OnBTNOBJAddTrigger)
ON_BN_CLICKED(IDC_BTN_OBJ_UPDATEOBJECT, OnBTNOBJUpdateObject)
ON_BN_CLICKED(IDC_CHECK_TCP_REGISTERED, OnCHECKTCPRegistered)
ON_LBN_SELCHANGE(IDC_LIST_TCP_IP_CURRENTPLAYERS, OnSelchangeLISTTCPIPCurrentPlayers)
ON_BN_CLICKED(IDC_BTN_TX_SAVETEXTUREBANK_STATIC, OnBTNTXSaveTextureBankStatic)
ON_BN_CLICKED(IDC_BTN_TX_SAVETEXTUREBANK_DYNAMIC, OnBTNTXSaveTextureBankDynamic)
ON_BN_CLICKED(IDC_BTN_TX_LOADTEXTUREBANK, OnBTNTXLoadTextureBank)
ON_BN_CLICKED(IDC_BTN_OBJ_GETSIGNIFICANT, OnBTNOBJGetSignificant)
ON_BN_CLICKED(IDC_BTN_OBJ_TEXTURESETMACRO, OnBTNOBJTextureSetMacro)
ON_BN_CLICKED(IDC_BTN_AI_CREATENEW, OnBTNAICreateNew)
ON_BN_CLICKED(IDC_BTN_UI_AI, OnBtnUiAi)
ON_BN_CLICKED(IDC_BTN_AI_DELETE, OnBTNAIDelete)
ON_BN_CLICKED(IDC_BTN_OBJ_LODADD, OnBtnObjLodadd)
ON_BN_CLICKED(IDC_BTN_OBJ_REMOVELODLEVEL, OnBTNOBJRemoveLODLevel)
ON_BN_CLICKED(IDC_BTN_OBJ_EDITLOD, OnBTNOBJEditLOD)
ON_BN_CLICKED(IDC_BTN_ENTITY_ANIMATIONS, OnBTNENTITYAnimations)
ON_BN_CLICKED(IDC_BTN_ENT_APPENDCUSTOMDB, OnBTNENTAppendCustomDB)
ON_LBN_SELCHANGE(IDC_LIST_ENTITY_RUNTIMELIST, OnSelchangeLISTENTITYRuntimeList)
ON_BN_CLICKED(IDC_BTN_UI_HALOEFFECTS, OnBTNUIHaloEffects)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_ADDHALO, OnBTNAddHalo)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_DELETEHALO, OnBTNDeleteHalo)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_EDITHALO, OnBTNEditHalo)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_LOADDATABASE, OnBTNHaloLoad)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_SAVEDATABASE, OnBTNHaloSave)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_ADDLENSFLARE, OnBTNAddLensFlare)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_EDITLENSFLARE, OnBTNEditLensFlare)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_DELETELENSFLARE, OnBTNDeleteLensFlare)
ON_BN_CLICKED(IDC_BTN_HALOEFFECTS_GENERATEMESH, OnBTNGenerateMesh)
ON_BN_CLICKED(IDC_BTN_ENT_LODSYSTEMS, OnBTNENTLODSystems)
ON_BN_CLICKED(IDC_BTN_ENT_DEL_LOD, OnBTNENTDelLOD)
ON_BN_CLICKED(IDC_BTN_ENT_UPDATE_VISUALS, OnBTNENTUpdateVisuals)
ON_BN_CLICKED(IDC_BTN_ENT_RESOURCEBASE, OnBTNENTResourceBase)
ON_BN_CLICKED(IDC_BTN_ENT_VIEWRUNTIMESTATS, OnBTNENTViewRuntimeStats)
ON_BN_CLICKED(IDC_BTN_ENT_VIEWRUNTIMERESOURCES, OnBTNENTViewRuntimeResources)
ON_LBN_SELCHANGE(IDC_LIST_MIS_MISSILEDATABASE, OnSelchangeLISTMISmissileDataBase)
ON_BN_CLICKED(IDC_BTN_MIS_REPLACEVALUES, OnBTNMISReplaceValues)
ON_BN_CLICKED(IDC_BTN_MIS_EDITVISUAL, OnBTNMISEditVisual)
ON_BN_CLICKED(IDC_BTN_MIS_ADDBILLBOARDMISSILE, OnBTNMISAddBillboardMissile)
ON_BN_CLICKED(IDC_BTN_MIS_EDITDYNAMICS, OnBTNMISEditDynamics)
ON_BN_CLICKED(IDC_BTN_ENT_EDITSTARTINVEN, OnBTNENTEditStartInven)
ON_CBN_DROPDOWN(IDC_CMBO_CGTEX_FONTTABLETEXTURE, OnDropdownCMBOCGTEXFontTableTexture)
ON_BN_CLICKED(IDC_BTN_CGTEX_ADD, OnBTNCGTEXAdd)
ON_BN_CLICKED(IDC_BTN_CGTEX_DELETE, OnBTNCGTEXDelete)
ON_BN_CLICKED(IDC_BTN_AI_ADDEVENT, OnBTNAIAddEvent)
ON_BN_CLICKED(IDC_BTN_AI_ADDSCRIPT, OnBTNAIAddScript)
ON_BN_CLICKED(IDC_BTN_AI_EDITSCRIPT, OnBTNAIEditScript)
ON_BN_CLICKED(IDC_BTN_AI_DELETESCRIPT, OnBTNAIDeleteScript)
ON_BN_CLICKED(IDC_BTN_AI_EDITEVENT, OnBTNAIEditEvent)
ON_BN_CLICKED(IDC_BTN_AI_DELETEEVENT, OnBTNAIDeleteEvent)
ON_BN_CLICKED(IDC_BTN_AI_ADDTREE, OnBTNAIAddTree)
ON_BN_CLICKED(IDC_BTN_AI_DELETETREE, OnBTNAIDeleteTree)
ON_BN_CLICKED(IDC_BTN_AI_EDIT, OnBTNAIEdit)
ON_BN_CLICKED(IDC_BTN_AI_SAVEAIDATA, OnBTNAISaveAiData)
ON_BN_CLICKED(IDC_BTN_AI_LOADAIDATA, OnBTNAILoadAiData)
ON_LBN_SELCHANGE(IDC_LIST_AI_SCRIPTS, OnSelchangeLISTAIScripts)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEAPPENDAGES, OnBTNENTSaveAppendages)
ON_BN_CLICKED(IDC_BTN_ENT_LOADAPPENDAGES, OnBTNENTLoadAppendages)
ON_BN_CLICKED(IDC_BTN_ENT_MOUNT, OnBtnEntMount)
ON_BN_CLICKED(IDC_BTN_ENT_DEMOUNT, OnBtnEntDemount)
ON_BN_CLICKED(IDC_BTN_EXP_EDITEXPLOSIONOBJ, OnBTNEXPEditExplosionObj)
ON_EN_CHANGE(IDC_EDIT_TCP_IPADRESS, OnChangeEDITTCPIPadress)
ON_EN_CHANGE(IDC_TCP_USERNAME, OnChangeTCPuserName)
ON_EN_CHANGE(IDC_TCP_PASSWORD, OnChangeTCPpassword)
ON_BN_CLICKED(IDC_BTN_SYSTEMGOINGDOWN, OnBTNSystemGoingDown)
ON_BN_CLICKED(IDC_BTN_ENT_ADDITEM, OnBTNENTAddItem)
ON_BN_CLICKED(IDC_BTN_ENT_ITEMTEST, OnBTNENTItemTest)
ON_BN_CLICKED(IDC_BTN_ENT_DELETEITEM, OnBTNENTDeleteItem)
ON_BN_CLICKED(IDC_BTN_ENT_DISARMITEM, OnBTNENTDisarmItem)
ON_BN_CLICKED(IDC_BTN_ENT_EDITITEM, OnBTNENTEditItem)
ON_BN_CLICKED(IDC_BTN_OBJ_CLONE, OnBTNOBJClone)
ON_BN_CLICKED(IDC_BTN_OBJ_TRANSFORMATION, OnBTNOBJTransformation)
ON_BN_CLICKED(IDC_BTN_OBJ_BOUNDINGGROUPSET, OnBTNOBJBoundingGroupSet)
ON_BN_CLICKED(IDC_BTN_OBJ_LIBRARYACCESS, OnBTNOBJLibraryAccess)
ON_BN_CLICKED(IDC_BTN_ENV_TIMESYSTEM, OnBTNENVTimeSystem)
ON_BN_CLICKED(IDC_BTN_MIS_ADDSKELETALMISSILE, OnBTNMISAddSkeletalMissile)
ON_BN_CLICKED(IDC_BTN_MIS_ADDLASER, OnBTNMISAddLaser)
ON_BN_CLICKED(IDC_BTN_AI_EDITAITREE, OnBTNAIEditAITree)
ON_BN_CLICKED(IDC_BTN_TCP_SUPPORTEDWORLDS, OnBTNTCPSupportedWorlds)
ON_BN_CLICKED(IDC_BTN_ENT_EDITDEATHCFG, OnBTNENTEditDeathCfg)
ON_BN_CLICKED(IDC_BTN_TCP_VIEWACCOUNT, OnBTNTCPViewAccount)
ON_BN_CLICKED(IDC_CHECK_TXB_BITMAPLOADOVERRIDE, OnCHECKTXBBitmapLoadOverride)
ON_BN_CLICKED(IDC_BTN_UI_SKILLBUILDER, OnBTNUISkillBuilder)
ON_BN_CLICKED(IDC_BTN_OBJ_ADDNODE, OnBTNOBJAddNode)
ON_BN_CLICKED(IDC_BTN_OBJ_REFERENCELIBRARY, OnBTNOBJReferenceLibrary)
ON_BN_CLICKED(IDC_BTN_UI_SOUNDDB, OnBTNUISoundDB)
ON_BN_CLICKED(IDC_BTN_UI_GLINVENTORYDB, OnBTNUIGlInventoryDB)
ON_BN_CLICKED(IDC_BTN_UI_CURRENCYSYS, OnBtnUiCurrencysys)
ON_BN_CLICKED(IDC_BTN_UI_COMMERCEDB, OnBTNUICommerceDB)
ON_BN_CLICKED(IDC_BTN_UI_BANKZONES, OnBTNUIBankZones)
ON_BN_CLICKED(IDC_BTN_UI_SAFEZONES, OnBtnUiSafezones)
ON_BN_CLICKED(IDC_BTN_ENT_UNIFYSLELETALMESH, OnBTNENTUnifySleletalMesh)
ON_BN_CLICKED(IDC_BTN_MIS_DELETELIGHT, OnBTNMISDeleteLight)
ON_BN_CLICKED(IDC_BTN_MIS_LIGHTSYS, OnBTNMISLightSys)
ON_BN_CLICKED(IDC_BTN_UI_STATDB, OnBtnUiStatdb)
ON_BN_CLICKED(IDC_BTN_TCP_ITEMGENERATORDB, OnBTNTCPItemGeneratorDB)
ON_BN_CLICKED(IDC_BTN_PATCHSYS, OnBtnPatchsys)
ON_BN_CLICKED(IDC_BTN_TCP_UPDATECURRENTONLINEPLAYERS, OnBTNTCPUpdateCurrentOnlinePlayers)
ON_BN_CLICKED(IDC_BTN_ENT_SCALEENTITY, OnBTNENTScaleEntity)
ON_BN_CLICKED(IDC_BTN_TCP_UPDATEALLACOUNTS, OnBTNTCPUpdateAllAcounts)
ON_BN_CLICKED(IDC_BTN_ENT_REPLACEENTITYWSIE, OnBTNENTReplaceEntityWSIE)
ON_BN_CLICKED(IDC_BTN_TCP_IPBLOCKING, OnBTNTCPIPBlocking)
ON_BN_CLICKED(IDC_BTN_UI_WATERZONES, OnBtnUiWaterzones)
ON_BN_CLICKED(IDC_BTN_UI_PORTALZONESDB, OnBtnUiPortalzonesdb)
ON_BN_CLICKED(IDC_BTN_UI_ELEMENTDB, OnBTNUIElementDB)
ON_BN_CLICKED(IDC_BTN_TCP_UPGRADEWORM, OnBTNTCPUpgradeWorm)
ON_BN_CLICKED(IDC_BTN_EXP_SAVEUNIT, OnBTNEXPSaveUnit)
ON_BN_CLICKED(IDC_BTN_EXP_LOADUNIT, OnBTNEXPLoadUnit)
ON_BN_CLICKED(IDC_BTN_TCP_DISCONNECTALL, OnBTNTCPDisconnectAll)
ON_BN_CLICKED(IDC_BTN_TCP_PRINTREPORTS, OnBTNTCPPrintReports)
ON_BN_CLICKED(IDC_BTN_AI_LOADAIUNIT, OnBTNAILoadAiUnit)
ON_BN_CLICKED(IDC_BTN_AI_SAVEAIUNIT, OnBTNAISaveAiUnit)
ON_BN_CLICKED(IDC_BTN_AI_IMPORTCOLLISIONSYS, OnBTNAIImportCollisionSys)
ON_BN_CLICKED(IDC_BTN_AI_DELETESYS, OnBTNAIDeleteSys)
ON_BN_CLICKED(IDC_BTN_AI_EXPORTCURRENTCOL, OnBTNAIExportCurrentCol)
ON_BN_CLICKED(IDC_BTN_AI_SAVECOLDB, OnBTNAISaveColDB)
ON_BN_CLICKED(IDC_BTN_AI_LOADCOLDB, OnBTNAILoadColDB)
ON_BN_CLICKED(IDC_BTN_MIS_SAVEUNIT, OnBTNMISSaveUnit)
ON_BN_CLICKED(IDC_BTN_MIS_LOADUNIT, OnBTNMISLoadUnit)
ON_BN_CLICKED(IDC_BTN_SPN_EXPORTSYS, OnBTNSPNExportSys)
ON_BN_CLICKED(IDC_BTN_SPN_IMPORTSYS, OnBTNSPNImportSys)
ON_BN_CLICKED(IDC_BTN_AI_INTEGRITY_CHECK, OnBTNAIIntegrityCheck)
ON_BN_CLICKED(IDC_BTN_TCP_SEARCHFORACCOUNT, OnBTNTCPSearchForAccount)
ON_BN_CLICKED(IDC_BTN_TCP_RESTOREBACKUP, OnBTNTCPRestoreBackup)
ON_BN_CLICKED(IDC_BTN_TXT_SYMBOLDB, OnBTNTXTSymbolDB)
ON_BN_CLICKED(IDC_CHECK_EDITORTXTMODE, OnCHECKEditorTxtMode)
ON_BN_CLICKED(IDC_BTN_ENT_MASSFORMATNAMES, OnBTNENTMassFormatNames)
ON_BN_CLICKED(IDC_CHK_OBJ_ENABLEPLAEMENTHOTKEY, OnCHKOBJEnablePlaementHotKey)
ON_BN_CLICKED(IDC_BTN_OBJ_REPORTCURRENDERCACHE, OnBTNOBJReportCurRenderCache)
ON_BN_CLICKED(IDC_BTN_UI_ICONDBACCESS, OnBTNUIIconDBACCESS)
ON_BN_CLICKED(IDC_BTN_TCP_SNAPSHOTPACKETQUE, OnBTNTCPSnapShotPacketQue)
ON_BN_CLICKED(IDC_BTN_TCP_GENERATELOGBASEDONTIME, OnBTNTCPGenerateLogBasedOnTime)
ON_BN_CLICKED(IDC_BTN_UI_HOUSINGSYS, OnBTNUIHousingSys)
ON_BN_CLICKED(IDC_BTN_UI_HSZONEDB, OnBTNUIHszoneDB)
ON_BN_CLICKED(IDC_BTN_CHK_SHOWSPEED, OnBTNChkshowSpeed)
ON_BN_CLICKED(IDC_BTN_AI_NEWTREE, OnBTNAInewTree)
ON_BN_CLICKED(IDC_BTN_TCP_EXPORTACCOUNT, OnBTNTCPExportAccount)
ON_BN_CLICKED(IDC_BTN_TCP_IMPORTACCOUNT, OnBTNTCPImportAccount)
ON_BN_CLICKED(IDC_BTN_TCP_GENERATECLANDATABASE, OnBTNTCPGenerateClanDatabase)
ON_BN_CLICKED(IDC_BTN_TCP_CLEARINACTIVE, OnBTNTCPClearInactive)
ON_BN_CLICKED(IDC_BTN_UI_AIRAINSYS, OnBTNUIAIRainSys)
ON_BN_CLICKED(IDC_BTN_ENT_SAVEITEMDB, OnBTNENTsaveItemDB)
ON_BN_CLICKED(IDC_BTN_ENT_LOADITEMDB, OnBTNENTLoadItemDB)
ON_BN_CLICKED(IDC_BTN_UI_HALOEFFECTS, OnBTNUIHaloEffects)
ON_BN_CLICKED(IDC_BTN_AI_INTEGRITY_CHECK, OnBTNAIIntegrityCheck)
ON_BN_CLICKED(IDC_BTN_SPN_APPENDSYS, OnBTNSPNAppendSys)
ON_BN_CLICKED(IDC_BTN_UI_TESTASSIGN, OnBTNTestAssign)
ON_BN_CLICKED(IDC_BTN_COMMCNTRLLIBBTNA, OnBTNCommCntrlLibBtnA)
ON_BN_CLICKED(IDD_BTN_UI_WORLDRULES, OnBTNWorldRule)
ON_BN_CLICKED(IDD_BTN_UI_ARENAMANAGER, OnBTNArenaManager)
ON_BN_CLICKED(IDC_BTN_OBJ_SELECT, OnObjectSelect)
ON_BN_CLICKED(IDC_BTN_OBJ_MOVE, OnObjectMove)
ON_BN_CLICKED(IDC_BTN_OBJ_ROTATE, OnObjectRotate)
ON_BN_CLICKED(IDC_BTN_OBJ_SCALE, OnObjectScale)
ON_LBN_SELCHANGE(IDC_LIST_VIEWPORTS, OnSelchangeViewports)
ON_LBN_SELCHANGE(IDC_LIST_RUNTIME_VIEWPORTS, OnSelchangeRuntimeViewports)

// collapsible groups
ON_BN_CLICKED(IDC_CHECK_AVAILABLE_JOYSTICKS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_EFFECT_SETTINGS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_CONTROL_OBJECTS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_CONTROL_EVENTS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_TEXT_FONT, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_SCRIPT, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_MENU_MENU, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_MENU_ICON, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_BACKGROUND, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_FOG, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_TIME_STORM, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_ENVIRONMENT_LIGHTING, OnCollapse)

ON_BN_CLICKED(IDC_CHECK_OBJECT_GROUPS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_VISUAL_IN_GROUP, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_ATTRIB, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_LOD_DATA, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_REFERENCE, OnCollapse)

ON_BN_CLICKED(IDC_CHECK_WORLD_MAP_SEARCH_TREES, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_COLLISION_SYSTEMS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_AI_BOTS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_AI_SCRIPTS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_SCRIPT_EVENTS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_SPAWN_GENERATOR, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_ANIMATED_TEXTURE, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_CAMERAS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_RUNTIME_CAMERAS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_VIEWPORTS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_RUNTIME_VIEWPORTS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_TEXTURE, OnCollapse)

ON_BN_CLICKED(IDC_CHECK_ACTORS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_RUNTIME, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_INVENTORY, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_APPENDAGES, OnCollapse)

ON_BN_CLICKED(IDC_CHECK_WEAPONS, OnCollapse)

ON_BN_CLICKED(IDC_CHECK_CD_AUDIO, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_WAV_AUDIO, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_SOUNDTRACK_LIST, OnCollapse)

ON_BN_CLICKED(IDC_CHECK_EXPLOSIONS, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_MOVIES, OnCollapse)
ON_BN_CLICKED(IDC_CHECK_PARTICLE_SYSTEMS, OnCollapse)

ON_BN_CLICKED(IDC_BTN_DEL_GUIDS, OnBTNDeleteGuids)

// Equipable Items
ON_LBN_SELCHANGE(IDC_LIST_EQUIPABLE_ITEMS, OnSelchangeEquipItem)
ON_CBN_SELCHANGE(IDC_COMBO_EQUIPABLE_MESH_SLOT, OnSelchangeEquippedItemMeshSlot)
ON_CBN_SELCHANGE(IDC_COMBO_EQUIPABLE_MESH_CONFIGS, OnSelchangeEquippedItemMesh)
ON_CBN_SELCHANGE(IDC_COMBO_EQUIPABLE_MATERIAL, OnSelchangeEquippedItemMaterial)
END_MESSAGE_MAP()

CKEPEditApp appInstance; // singleton app instance

CKEPEditApp::CKEPEditApp() :
		m_stdOut(0),
		m_stdErr(0) {
	m_processingModeForeground = FALSE;
}

string CKEPEditApp::ProjectName() const {
	return string(PROJECT_NAME) + StarLabel(m_starId);
}
string CKEPEditApp::WokUrl() const {
	return StarWokUrl(m_starId);
}
string CKEPEditApp::PatchUrl() const {
	return StarPatchUrl(m_starId);
}

BOOL CKEPEditApp::InitInstance() {
	// Reassign stderr and stdout to catch output
	freopen_s(&m_stdErr, "editor_stderr.txt", "w", stderr);
	freopen_s(&m_stdOut, "editor_stdout.txt", "w", stdout);

	// Determine App Version String 'xx.xx.xx.xx'
	m_appVersion = VersionApp();

	// Determine App Path 	'c:/.../kaneva/star/xxxx'
	m_appPath = PathApp();

	// Determine Star Path 	'c:/.../kaneva/star/xxxx'
	m_starPath = m_appPath;

	// Determine Star Id 'xxxx'
	m_starId = StrStripLeftLast(m_starPath, "/\\");

	// Runtime In Production Environment?
	m_isProduction = StarIsProduction(m_starId);

	AfxInitRichEdit();
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	HANDLE mutexhandle = CreateMutexW(NULL, FALSE, L"KANEVA_EDITOR");
	if (mutexhandle != 0 && GetLastError() == ERROR_ALREADY_EXISTS) {
		AfxMessageBox(L"The Editor is already running\r\nSecond instance is exiting.", MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Install Crash Reporter
	CR_Install(ProjectName());

	if (!AfxSocketInit()) {
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit()) {
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();

	Gdiplus::GdiplusStartupInput gdiplusStartupInput;

	// Initialize GDI+.
	Gdiplus::GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);

	BOOL fFirstInstance = (InterlockedIncrement(&g_lUsageCount) == 0);
	if (!fFirstInstance) {
		AfxMessageBox(IDS_APP_LOAD_DUPLICATE, MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	SetRegistryKey(_T("Kaneva"));
	LoadStdProfileSettings(4); // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CKEPEditDoc),
		RUNTIME_CLASS(CMainFrame), // main SDI frame window
		RUNTIME_CLASS(CKEPEditView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);
	// Parse command line
	// Some of the flags and arguments won't be used until later.
	CKEPEditCommandInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	m_nCmdShow = SW_HIDE; // always hide by default, show only if need to later
	if (!ProcessShellCommand(cmdInfo)) {
		return FALSE;
	}

	//
	// If the Editor is invoked with the -cs argument,
	// then allow the editor to automatically prompt
	// the user with the "Create a New Star" wizard.
	// The installer will utilize this command as
	// a finalization option.
	//
	if (cmdInfo.m_autoCreateStar == true) {
		if (cmdInfo.m_autoLogon == true) {
			appInstance.GetView()->SetParamUsername(cmdInfo.m_userName);
			appInstance.GetView()->SetParamPassword(cmdInfo.m_origPassword);
		}

		if (!cmdInfo.m_appName.IsEmpty()) {
			appInstance.GetView()->SetParamAppName(cmdInfo.m_appName);
		}

		if (!cmdInfo.m_templateName.IsEmpty()) {
			appInstance.GetView()->SetParamTemplateName(cmdInfo.m_templateName);
		}

		if (!cmdInfo.m_installDirectory.IsEmpty()) {
			appInstance.GetView()->SetParamInstallDirectory(cmdInfo.m_installDirectory);
		}

		if (cmdInfo.m_clientRunning) {
			appInstance.GetView()->SetParamClientRunning(true);
		}

		appInstance.GetView()->OnFileNew();
		m_pMainWnd->PostMessage(WM_CLOSE);
		return TRUE;
	} else {
		// The one and only window has been initialized, so show and update it
		m_pMainWnd->ShowWindow(SW_SHOW);
		m_pMainWnd->UpdateWindow();
		// call DragAcceptFiles only if there's a suffix
		//  In an SDI app, this should occur after ProcessShellCommand
		// Enable drag/drop open
		m_pMainWnd->DragAcceptFiles();
	}

	//
	// Process command line arguments.  Other arguments are processed
	// in ClientEngine::InitDirectInputAutoExecTextures(), after the
	// WoK.kep file has been loaded.
	//
	// Allow double-click launch
	if ((CCommandLineInfo::FileOpen == cmdInfo.m_nShellCommand) &&
		(!cmdInfo.m_strFileName.IsEmpty())) {
		GetView()->SetMRUFileToOpen(Utf16ToUtf8(cmdInfo.m_strFileName).c_str(), -1);
		// Reuse same file open code
		GetView()->OnFileOpen();
	} else if (cmdInfo.m_wokFile.GetLength() > 0) {
		//
		// Try to perform the steps to make a distribution
		// automatically.  Popups are still used to report
		// errors though.
		//
		GetView()->SetMRUFileToOpen(cmdInfo.m_wokFile, -1);
		if (cmdInfo.m_distributionConfig.GetLength()) {
			GetView()->updateGameFiles();
			GetView()->makeDistributionAutomatically();
		}
		GetView()->OnFileOpen();
		//
		// Is the a sensible thing to do?
		// Try it and see.
		//
		if (cmdInfo.m_distributionConfig.GetLength()) {
			CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
			pMainFrame->m_wndInformation.ShowWindow(SW_HIDE);
			pMainFrame->m_wndGameToolBar.ShowWindow(SW_HIDE);
			pMainFrame->m_wndKEPDialogBar.ShowWindow(SW_HIDE);
			pMainFrame->m_wndKEPStatusBar.ShowWindow(SW_HIDE);
			pMainFrame->m_wndToolBar.ShowWindow(SW_HIDE);
			pMainFrame->m_wndTreeBar.ShowWindow(SW_HIDE);
			pMainFrame->m_wndStatusBar.ShowWindow(SW_HIDE);

			CStringA destinationDirectory;

			if (GetView()->DoGameDistribution(destinationDirectory, cmdInfo.m_distributionConfig) && GetView()->GetGame()->IsSubGame()) {
				GetView()->CreateDistroShortCuts(destinationDirectory);
			}

			//
			// Once the distribution has been done, exit.
			//
			m_pMainWnd->PostMessage(WM_CLOSE);
		}
	} else if (cmdInfo.m_distributionConfig.GetLength()) {
		AfxMessageBox(IDS_CREATE_DISTRIBUTION_BAD_COMMANDLINE, MB_ICONEXCLAMATION);
	}

	//SendAutoFeedbackReport();

	return TRUE;
}

int CKEPEditApp::ExitInstance() {
	InterlockedDecrement(&g_lUsageCount);

	Gdiplus::GdiplusShutdown(m_gdiplusToken);

	return CWinApp::ExitInstance();
}

///---------------------------------------------------------
// CKEPEditApp::MRUFileHandler
//
// Catches the call for a MRU and passes it to the view for
// processing.
//
// [in] i - index of the file to open in the recent file list
//
void CKEPEditApp::MRUFileHandler(UINT i) {
	CStringW strName;

	ASSERT_VALID(this);
	ASSERT(m_pRecentFileList != NULL);

	ASSERT(i >= ID_FILE_MRU_FILE1);
	ASSERT(i < ID_FILE_MRU_FILE1 + (UINT)m_pRecentFileList->GetSize());

	int nIndex = i - ID_FILE_MRU_FILE1;

	ASSERT((*m_pRecentFileList)[nIndex].GetLength() != 0);

	GetView()->SetMRUFileToOpen(Utf16ToUtf8((*m_pRecentFileList)[nIndex]).c_str(), nIndex);
	// Reuse same file open code
	GetView()->OnFileOpen();
}

void CKEPEditApp::MRURemoveFile(int index) {
	ASSERT(index >= 0);
	m_pRecentFileList->Remove(index);
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog {
public:
	CAboutDlg();

	// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	CStringA m_version;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
};

CAboutDlg::CAboutDlg() :
		CDialog(CAboutDlg::IDD) {
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_BUILD_NUM, m_version);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CKEPEditApp::OnAppAbout() {
	CAboutDlg aboutDlg;

	CStringA aboutDescription;

	struct LANGANDCODEPAGE {
		WORD wLanguage;
		WORD wCodePage;
	} * lpTranslate;

	VS_FIXEDFILEINFO pvsf = { 0 }; // DRF - potentially uninitialized variable
	DWORD dwHandle;
	wchar_t appName[_MAX_PATH];
	appName[0] = '\0';
	::GetModuleFileName(AfxGetApp()->m_hInstance, appName, _MAX_PATH);
	DWORD cchver = GetFileVersionInfoSize(appName, &dwHandle);
	if (cchver != 0) {
		char* pver = new char[cchver];
		CStringA SpecialBuild;
		BOOL bret = GetFileVersionInfo(appName, dwHandle, cchver, pver);
		if (bret) {
			UINT uLen;
			void* pbuf;
			bret = VerQueryValue(pver, L"\\\\", &pbuf, &uLen);
			if (bret) {
				memcpy(&pvsf, pbuf, sizeof(VS_FIXEDFILEINFO));
				aboutDescription.Format("Build # %d.%d.%d.%d",
					HIWORD(pvsf.dwFileVersionMS),
					LOWORD(pvsf.dwFileVersionMS),
					HIWORD(pvsf.dwFileVersionLS),
					LOWORD(pvsf.dwFileVersionLS));
			} else {
				assert(false); // DRF - uninitialized pvsf
			}

			if (pvsf.dwFileFlags & VS_FF_SPECIALBUILD) {
				bret = VerQueryValue(pver, L"\\\\VarFileInfo\\\\Translation", (LPVOID*)&lpTranslate, &uLen);

				if (bret) {
					SpecialBuild.Format("%s%04x%04x%s", "\\\\StringFileInfo\\\\", lpTranslate[0].wLanguage, lpTranslate[0].wCodePage, "\\\\SpecialBuild");

					bret = VerQueryValueW(pver, Utf8ToUtf16(SpecialBuild).c_str(), &pbuf, &uLen);

					if (bret) {
						CStringW specialDescription;
						specialDescription.Format(L" - %s", pbuf);
						aboutDescription += Utf16ToUtf8(specialDescription).c_str();
					}
				}
			}

			if (!aboutDescription.IsEmpty()) {
				aboutDlg.m_version = aboutDescription;
			}
		}
		delete[] pver;
	}

	aboutDlg.DoModal();
}

void CKEPEditApp::OnSendFeedBack() {
	CrashReporter::ReportError(ErrorData("KEPEdit OnSendFeedback"), true);
}

// CKEPEditApp message handlers

BOOL CKEPEditApp::OnIdle(LONG lCount) {
	static BOOL isRendering = FALSE;

	CKEPEditView* theView = appInstance.GetView();

	if ((NULL != theView) &&
		(!isRendering)) {
		isRendering = TRUE; // disallow recursive call
		theView->RenderLoop();
		isRendering = FALSE;
	}

	CWinApp::OnIdle(lCount);

	return TRUE; // Keep calling renderloop
}

void CKEPEditApp::ProgressUpdate(CStringA position) {
	// Process Message Loop Until Empty
	FOREVER {
		// Message Waiting ?
		MSG msg;
		BOOL bRet = ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
		if (bRet == -1) {
			LogError("PeekMessage() FAILED");
			break;
		}
		if (bRet == 0)
			break;

		// Pump Message
		PumpMessage();
	}
}

void CKEPEditApp::OnButtonQuit() {
	try {
		GetView()->OnButtonQuit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonSave() {
	try {
		GetView()->OnButtonSave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonTreetest() {
	try {
		GetView()->OnButtonTreetest();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonObjectload() {
	try {
		GetView()->OnButtonObjectload();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonObjectdelete() {
	try {
		GetView()->OnButtonObjectdelete();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonSwitchObject() {
	try {
		GetView()->OnButtonSwitchObject();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONBKEDITORswtch() {
	try {
		GetView()->OnBUTTONBKEDITORswtch();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonDelete() {
	try {
		GetView()->OnButtonDelete();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonMvUp() {
	try {
		GetView()->OnButtonMvUp();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonMvDown() {
	try {
		GetView()->OnButtonMvDown();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonMvLeft() {
	try {
		GetView()->OnButtonMvLeft();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonMvRight() {
	try {
		GetView()->OnButtonMvRight();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnTextureFileOpen1() {
	try {
		GetView()->OnTextureFileOpen1();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnTextureFileOpen2() {
	try {
		GetView()->OnTextureFileOpen2();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnTextureFileOpen3() {
	try {
		GetView()->OnTextureFileOpen3();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnTextureFileOpen4() {
	try {
		GetView()->OnTextureFileOpen4();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnButtonAddnewstaticBk() {
	try {
		GetView()->OnButtonAddnewstaticBk();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONBKSETTINGSapply() {
	try {
		GetView()->OnBUTTONBKSETTINGSapply();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonControls() {
	try {
		GetView()->OnButtonControls();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonTestEffect() {
	try {
		GetView()->OnButtonTestEffect();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonStopEffect() {
	try {
		GetView()->OnButtonStopEffect();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonMovies() {
	try {
		GetView()->OnButtonMovies();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonTestmovie() {
	try {
		GetView()->OnButtonTestmovie();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnEntityCreatenew() {
	try {
		GetView()->OnBtnEntityCreatenew();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnEntityActivate() {
	try {
		GetView()->OnBtnEntityActivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonSwEntities() {
	try {
		GetView()->OnButtonSwEntities();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonTestfullscreen() {
	try {
		GetView()->OnButtonTestfullscreen();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnViewwireframe() {
	try {
		GetView()->OnBtnViewwireframe();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonViewsmoothshaded() {
	try {
		GetView()->OnButtonViewsmoothshaded();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnEntityMovement() {
	try {
		GetView()->OnBtnEntityMovement();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnEntityDeactivate() {
	try {
		GetView()->OnBtnEntityDeactivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnDeleteEntity() {
	try {
		GetView()->OnBtnDeleteEntity();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTITYdeleteAnim() {
	try {
		GetView()->OnBTNENTITYdeleteAnim();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTITYDeleteChld() {
	try {
		GetView()->OnBTNENTITYDeleteChld();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnEntityAddchild() {
	try {
		GetView()->OnBtnEntityAddchild();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnNewall() {
	try {
		GetView()->OnBtnNewall();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNCameraViewportsSw() {
	try {
		GetView()->OnBTNCameraViewportsSw();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnCAMERACreateNewCamera() {
	try {
		GetView()->OnCAMERACreateNewCamera();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNCreateNewViewport() {
	try {
		GetView()->OnBTNCreateNewViewport();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNCameraVportActivate() {
	try {
		GetView()->OnBTNCameraVportActivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNvportDeactivate() {
	try {
		GetView()->OnBTNvportDeactivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnAddRuntimeCamera() {
	try {
		GetView()->OnBtnAddRuntimeCamera();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnRemoveRuntimeCamera() {
	try {
		GetView()->OnBtnRemoveRuntimeCamera();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnAddRuntimeViewport() {
	try {
		GetView()->OnBtnAddRuntimeViewport();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnRemoveRuntimeViewport() {
	try {
		GetView()->OnBtnRemoveRuntimeViewport();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMOUSEcreate() {
	try {
		GetView()->OnBTNMOUSEcreate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUImouseSW() {
	try {
		GetView()->OnBTNUImouseSW();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMOUSEfileBrowse() {
	try {
		GetView()->OnBTNMOUSEfileBrowse();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMOUSEactivate() {
	try {
		GetView()->OnBTNMOUSEactivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMOUSEdelete() {
	try {
		GetView()->OnBTNMOUSEdelete();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMOUSEdeactivate() {
	try {
		GetView()->OnBTNMOUSEdeactivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DINTERFACEsw() {
	try {
		GetView()->OnBTN2DINTERFACEsw();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DCreateCollection() {
	try {
		GetView()->OnBTN2DCreateCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DADDunit() {
	try {
		GetView()->OnBTN2DADDunit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtn2dActivate() {
	try {
		GetView()->OnBtn2dActivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtn2dDeactivate() {
	try {
		GetView()->OnBtn2dDeactivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DBrowseFileUp() {
	try {
		GetView()->OnBTN2DBrowseFileUp();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DBrowseFileDown() {
	try {
		GetView()->OnBTN2DBrowseFileDown();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DgetSelectedInfo() {
	try {
		GetView()->OnBTN2DgetSelectedInfo();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DApplyModifications() {
	try {
		GetView()->OnBTN2DApplyModifications();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DDeleteCollection() {
	try {
		GetView()->OnBTN2DDeleteCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DDeleteUnit() {
	try {
		GetView()->OnBTN2DDeleteUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONEnviormentSw() {
	try {
		GetView()->OnBUTTONEnviormentSw();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENVBckRgb() {
	try {
		GetView()->OnBTNENVBckRgb();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENVFogRgb() {
	try {
		GetView()->OnBTNENVFogRgb();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENVApplyChanges() {
	try {
		GetView()->OnBTNENVApplyChanges();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENVMatch() {
	try {
		GetView()->OnBTNENVMatch();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENVBrowseSurObj() {
	try {
		GetView()->OnBTNENVBrowseSurObj();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnCHECKEnableSurrounding() {
	try {
		GetView()->OnCHECKEnableSurrounding();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNObjectMaterial() {
	try {
		GetView()->OnBTNObjectMaterial();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENVemmisive() {
	try {
		GetView()->OnBTNENVemmisive();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEntityMaterial() {
	try {
		GetView()->OnBTNEntityMaterial();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONtextAddTextCollection() {
	try {
		GetView()->OnBUTTONtextAddTextCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtextFontSW() {
	try {
		GetView()->OnBTNtextFontSW();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONtextAddTextObject() {
	try {
		GetView()->OnBUTTONtextAddTextObject();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtextActivate() {
	try {
		GetView()->OnBTNtextActivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtextDeactivate() {
	try {
		GetView()->OnBTNtextDeactivate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONtextTextColor() {
	try {
		GetView()->OnBUTTONtextTextColor();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtextApplyChanges() {
	try {
		GetView()->OnBTNtextApplyChanges();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtextGetCurrentInfo() {
	try {
		GetView()->OnBTNtextGetCurrentInfo();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtextApplyFont() {
	try {
		GetView()->OnBTNtextApplyFont();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONtextAddFont() {
	try {
		GetView()->OnBUTTONtextAddFont();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtextApplySizeChange() {
	try {
		GetView()->OnBTNtextApplySizeChange();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONtextDeleteFont() {
	try {
		GetView()->OnBUTTONtextDeleteFont();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONtextDeleteTextCollection() {
	try {
		GetView()->OnBUTTONtextDeleteTextCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBUTTONtextDeleteTextObject() {
	try {
		GetView()->OnBUTTONtextDeleteTextObject();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtestSave() {
	try {
		GetView()->OnBTNtestSave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNtestLoad() {
	try {
		GetView()->OnBTNtestLoad();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNcamVportSave() {
	try {
		GetView()->OnBTNcamVportSave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNcamVportLoad() {
	try {
		GetView()->OnBTNcamVportLoad();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNDeleteCamera() {
	try {
		GetView()->OnBTNDeleteCamera();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNDeleteViewport() {
	try {
		GetView()->OnBTNDeleteViewport();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUpdateViewport() {
	try {
		GetView()->OnBTNUpdateViewport();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUpdateRuntimeViewport() {
	try {
		GetView()->OnBTNUpdateRuntimeViewport();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTN2DSaveConfig() {
	try {
		GetView()->OnBTN2DSaveConfig();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTN2DLoadConfig() {
	try {
		GetView()->OnBTN2DLoadConfig();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNExplosionDBSW() {
	try {
		GetView()->OnBTNExplosionDBSW();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEXPAddExplosion() {
	try {
		GetView()->OnBTNEXPAddExplosion();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEXPLoadExplosions() {
	try {
		GetView()->OnBTNEXPLoadExplosions();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEXPSaveExplosions() {
	try {
		GetView()->OnBTNEXPSaveExplosions();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEXPDeleteExplosion() {
	try {
		GetView()->OnBTNEXPDeleteExplosion();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNCameraAdvanced() {
	try {
		GetView()->OnBTNCameraAdvanced();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMovieAddMovie() {
	try {
		GetView()->OnBTNMovieAddMovie();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMovieTestMovie() {
	try {
		GetView()->OnBTNMovieTestMovie();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMovieEditMovie() {
	try {
		GetView()->OnBTNMovieEditMovie();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMovieDeleteMovie() {
	try {
		GetView()->OnBTNMovieDeleteMovie();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMoviesSaveDB() {
	try {
		GetView()->OnBTNMoviesSaveDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMoviesLoadDB() {
	try {
		GetView()->OnBTNMoviesLoadDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMovieStopMovies() {
	try {
		GetView()->OnBTNMovieStopMovies();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlAddObject() {
	try {
		GetView()->OnBTNControlAddObject();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlAddEvent() {
	try {
		GetView()->OnBTNControlAddEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlCfgSave() {
	try {
		GetView()->OnBTNControlCfgSave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlCfgLoad() {
	try {
		GetView()->OnBTNControlCfgLoad();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlDeleteObject() {
	try {
		GetView()->OnBTNControlDeleteObject();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlDeleteEvent() {
	try {
		GetView()->OnBTNControlDeleteEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeLISTControlDB() {
	try {
		GetView()->OnSelchangeLISTControlDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlEditObject() {
	try {
		GetView()->OnBTNControlEditObject();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNControlEditEvent() {
	try {
		GetView()->OnBTNControlEditEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIBaseAdvanced() {
	try {
		GetView()->OnBTNUIBaseAdvanced();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNObjectArrayAdd() {
	try {
		GetView()->OnBTNObjectArrayAdd();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTITYProperties() {
	try {
		GetView()->OnBTNENTITYProperties();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJECTProperties() {
	try {
		GetView()->OnBTNOBJECTProperties();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJDeleteLight() {
	try {
		GetView()->OnBTNOBJDeleteLight();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnObjAddlight() {
	try {
		GetView()->OnBtnObjAddlight();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTMergeData() {
	try {
		GetView()->OnBTNENTMergeData();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTAddLight() {
	try {
		GetView()->OnBTNENTAddLight();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIMusic() {
	try {
		GetView()->OnBTNUIMusic();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMusicAssignTrack() {
	try {
		GetView()->OnBTNMusicAssignTrack();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMusicAssignWav() {
	try {
		GetView()->OnBTNMusicAssignWav();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMusicUnassignTrack() {
	try {
		GetView()->OnBTNMusicUnassignTrack();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMusicUnassignWav() {
	try {
		GetView()->OnBTNMusicUnassignWav();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMusicBrowseWav() {
	try {
		GetView()->OnBTNMusicBrowseWav();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUITextureAssign() {
	try {
		GetView()->OnBTNUITextureAssign();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIMissileDatabase() {
	try {
		GetView()->OnBTNUIMissileDatabase();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISAddMissile() {
	try {
		GetView()->OnBTNMISAddMissile();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISBrowseFileName() {
	try {
		GetView()->OnBTNMISBrowseFileName();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISDeleteMissile() {
	try {
		GetView()->OnBTNMISDeleteMissile();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISsave() {
	try {
		GetView()->OnBTNMISsave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISLoad() {
	try {
		GetView()->OnBTNMISLoad();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIBulletHole() {
	try {
		GetView()->OnBTNUIBulletHole();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNBHAddHole() {
	try {
		GetView()->OnBTNBHAddHole();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNBHBrowse() {
	try {
		GetView()->OnBTNBHBrowse();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNBHAlphaTexture() {
	try {
		GetView()->OnBTNBHAlphaTexture();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNBHDeleteHole() {
	try {
		GetView()->OnBTNBHDeleteHole();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNBHload() {
	try {
		GetView()->OnBTNBHload();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNBHMaterial() {
	try {
		GetView()->OnBTNBHMaterial();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNBHSave() {
	try {
		GetView()->OnBTNBHSave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEntityshadow() {
	try {
		GetView()->OnBTNEntityshadow();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNObjectShadow() {
	try {
		GetView()->OnBTNObjectShadow();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTAnimProperties() {
	try {
		GetView()->OnBTNENTAnimProperties();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEntitySound() {
	try {
		GetView()->OnBTNEntitySound();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEntitysetSoundCenter() {
	try {
		GetView()->OnBTNEntitysetSoundCenter();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJSound() {
	try {
		GetView()->OnBTNOBJSound();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJAlphaTexture() {
	try {
		GetView()->OnBTNOBJAlphaTexture();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTAnimSound() {
	try {
		GetView()->OnBTNENTAnimSound();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTCPIPStartHost() {
#if 1
	ASSERT(FALSE); // not used the called fn was commented out
#else
	try {
		GetView()->OnBTNTCPIPStartHost();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
#endif
}
void CKEPEditApp::OnBTNTCPIPStopHost() {
#if 1
	ASSERT(FALSE); // not used the called fn was commented out
#else
	try {
		GetView()->OnBTNTCPIPStopHost();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
#endif
}
void CKEPEditApp::OnBTNTCPIPJoinHost() {
	try {
		GetView()->OnBTNTCPIPJoinHost();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIMultiplayer() {
	try {
		GetView()->OnBTNUIMultiplayer();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEntityCombineEntityVis() {
	try {
		GetView()->OnBTNEntityCombineEntityVis();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTContentProperties() {
	try {
		GetView()->OnBTNENTContentProperties();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTEXAddCollection() {
	try {
		GetView()->OnBTNTEXAddCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTEXDeleteCollection() {
	try {
		GetView()->OnBTNTEXDeleteCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIAnimTextureDB() {
	try {
		GetView()->OnBTNUIAnimTextureDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTEXLoadCollection() {
	try {
		GetView()->OnBTNTEXLoadCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTEXSaveCollection() {
	try {
		GetView()->OnBTNTEXSaveCollection();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTLoadDB() {
	try {
		GetView()->OnBTNENTLoadDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTSaveDBSmoothNormals() {
	try {
		GetView()->OnBTNENTSaveDBSmoothNormals();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTSaveDB() {
	try {
		GetView()->OnBTNENTSaveDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonSaveANM() {
	try {
		GetView()->OnButtonSaveANM();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnButtonSaveANMU() {
	try {
		GetView()->OnButtonSaveANMU();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTSaveDBExpanded() {
	try {
		GetView()->OnBTNENTSaveDBExpanded();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTLoadDBExpanded() {
	try {
		GetView()->OnBTNENTLoadDBExpanded();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTRemoveLOD() {
	try {
		GetView()->OnBTNENTRemoveLOD();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnUiScript() {
	try {
		GetView()->OnBtnUiScript();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCRAddScript() {
	try {
		GetView()->OnBTNSCRAddScript();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCRDeleteEvent() {
	try {
		GetView()->OnBTNSCRDeleteEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCRLoadExecFile() {
	try {
		GetView()->OnBTNSCRLoadExecFile();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCRBuild() {
	try {
		GetView()->OnBTNSCRBuild();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCREditEvent() {
	try {
		GetView()->OnBTNSCREditEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJEditLight() {
	try {
		GetView()->OnBTNOBJEditLight();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTLoadEntity() {
	try {
		GetView()->OnBTNENTLoadEntity();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTSaveEntity() {
	try {
		GetView()->OnBTNENTSaveEntity();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMSSaveDB() {
	try {
		GetView()->OnBTNMSSaveDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMSLoadDB() {
	try {
		GetView()->OnBTNMSLoadDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnTxtdbAdd() {
	try {
		GetView()->OnBtnTxtdbAdd();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnTxtdbDelete() {
	try {
		GetView()->OnBtnTxtdbDelete();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnTxtdbEdit() {
	try {
		GetView()->OnBtnTxtdbEdit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnTxtdbReinitAll() {
	try {
		GetView()->OnBtnTxtdbReinitAll();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnTxtdbReinitFromDisk() {
	try {
		GetView()->OnBtnTxtdbReinitFromDisk();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnTxtdbSetSubtraction() {
	try {
		GetView()->OnBtnTxtdbSetSubtraction();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJClearTexCoords() {
	try {
		GetView()->OnBTNOBJClearTexCoords();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJAddTexCoords() {
	try {
		GetView()->OnBTNOBJAddTexCoords();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCRSetToScene() {
	try {
		GetView()->OnBTNSCRSetToScene();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCRClearScene() {
	try {
		GetView()->OnBTNSCRClearScene();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSCRLoadFromScene() {
	try {
		GetView()->OnBTNSCRLoadFromScene();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNNTAdvanced() {
	try {
		GetView()->OnBTNNTAdvanced();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNNTBuildTxtRecord() {
	try {
		GetView()->OnBTNNTBuildTxtRecord();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIHudInterface() {
	try {
		GetView()->OnBTNUIHudInterface();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHUDAddDisplayUnit() {
	try {
		GetView()->OnBTNHUDAddDisplayUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHUDAddNewHud() {
	try {
		GetView()->OnBTNHUDAddNewHud();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHUDSaveDB() {
	try {
		GetView()->OnBTNHUDSaveDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHUDLoadDB() {
	try {
		GetView()->OnBTNHUDLoadDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeListHudDb() {
	try {
		GetView()->OnSelchangeListHudDb();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHUDDeleteHud() {
	try {
		GetView()->OnBTNHUDDeleteHud();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHUDDeleteDisplayUnit() {
	try {
		GetView()->OnBTNHUDDeleteDisplayUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHUDReplaceUnit() {
	try {
		GetView()->OnBTNHUDReplaceUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTUpdate() {
	try {
		GetView()->OnBTNENTUpdate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeLISTTXTDBDatabase() {
	try {
		GetView()->OnSelchangeLISTTXTDBDatabase();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnCHECKTEXBltOn() {
	try {
		GetView()->OnCHECKTEXBltOn();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIParticleSys() {
	try {
		GetView()->OnBTNUIParticleSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNPARTEditSystem() {
	try {
		GetView()->OnBTNPARTEditSystem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNPARTLoadDatabase() {
	try {
		GetView()->OnBTNPARTLoadDatabase();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUISpawnGen() {
	try {
		GetView()->OnBTNUISpawnGen();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSpawnAddGen() {
	try {
		GetView()->OnBTNSpawnAddGen();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSpawnDeleteGen() {
	try {
		GetView()->OnBTNSpawnDeleteGen();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNSpawnEditGen() {
	try {
		GetView()->OnBTNSpawnEditGen();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNObjectBuildCollisionMap() {
	try {
		GetView()->OnBTNObjectBuildCollisionMap();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
/*
void CKEPEditApp::OnClickObjectTree(NMHDR* pNMHDR, LRESULT* pResult)
{
	try
	{
		GetView()->OnClickObjectTree(pNMHDR, pResult);
	}
	catch(...)
	{
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
*/

void CKEPEditApp::OnSelchangedObjectTree(NMHDR* pNMHDR, LRESULT* pResult) {
	try {
		GetView()->OnSelchangedObjectTree(pNMHDR, pResult);
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNObjectsAddGroup() {
	try {
		GetView()->OnBTNObjectsAddGroup();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNObjectsDeleteGroup() {
	try {
		GetView()->OnBTNObjectsDeleteGroup();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNObjectsEditGroup() {
	try {
		GetView()->OnBTNObjectsEditGroup();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeLISTObjectsGroupList2Box() {
	try {
		GetView()->OnSelchangeLISTObjectsGroupList2Box();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnDblclkLISTObjectsGroupList2Box() {
	try {
		GetView()->OnDblclkLISTObjectsGroupList2Box();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNGroupModifier() {
	try {
		GetView()->OnBTNGroupModifier();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnDblclkObjectTree(NMHDR* pNMHDR, LRESULT* pResult) {
	try {
		GetView()->OnDblclkObjectTree(pNMHDR, pResult);
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNUIMerge() {
	try {
		GetView()->OnBTNUIMerge();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTCPDisconnectPlayer() {
	try {
		GetView()->OnBTNTCPDisconnectPlayer();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTCPServerLoad() {
	try {
		GetView()->OnBTNTCPServerLoad();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTCPServerSave() {
	try {
		GetView()->OnBTNTCPServerSave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOptimizeStates() {
	try {
		GetView()->OnBTNOptimizeStates();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNDeleteGuids() {
	try {
		GetView()->OnBTNDeleteGuids();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTCPNewAccount() {
	try {
		GetView()->OnBTNTCPNewAccount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTCPDeleteAccount() {
	try {
		GetView()->OnBTNTCPDeleteAccount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJAddTrigger() {
	try {
		GetView()->OnBTNOBJAddTrigger();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJUpdateObject() {
	try {
		GetView()->OnBTNOBJUpdateObject();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnCHECKTCPRegistered() {
	try {
		GetView()->OnCHECKTCPRegistered();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeLISTTCPIPCurrentPlayers() {
	try {
		GetView()->OnSelchangeLISTTCPIPCurrentPlayers();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTXSaveTextureBankStatic() {
	try {
		GetView()->OnBTNTXSaveTextureBankStatic();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTXSaveTextureBankDynamic() {
	try {
		GetView()->OnBTNTXSaveTextureBankDynamic();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNTXLoadTextureBank() {
	try {
		GetView()->OnBTNTXLoadTextureBank();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJGetSignificant() {
	try {
		GetView()->OnBTNOBJGetSignificant();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJTextureSetMacro() {
	try {
		GetView()->OnBTNOBJTextureSetMacro();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAICreateNew() {
	try {
		GetView()->OnBTNAICreateNew();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnUiAi() {
	try {
		GetView()->OnBtnUiAi();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIDelete() {
	try {
		GetView()->OnBTNAIDelete();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnObjLodadd() {
	try {
		GetView()->OnBtnObjLodadd();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJRemoveLODLevel() {
	try {
		GetView()->OnBTNOBJRemoveLODLevel();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNOBJEditLOD() {
	try {
		GetView()->OnBTNOBJEditLOD();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTITYAnimations() {
	try {
		GetView()->OnBTNENTITYAnimations();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTAppendCustomDB() {
	try {
		GetView()->OnBTNENTAppendCustomDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeLISTENTITYRuntimeList() {
	try {
		GetView()->OnSelchangeLISTENTITYRuntimeList();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIHaloEffects() {
	try {
		GetView()->OnBTNUIHaloEffects();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAddHalo() {
	try {
		GetView()->OnBTNAddHalo();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNDeleteHalo() {
	try {
		GetView()->OnBTNDeleteHalo();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEditHalo() {
	try {
		GetView()->OnBTNEditHalo();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHaloLoad() {
	try {
		GetView()->OnBTNHaloLoad();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNHaloSave() {
	try {
		GetView()->OnBTNHaloSave();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAddLensFlare() {
	try {
		GetView()->OnBTNAddLensFlare();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEditLensFlare() {
	try {
		GetView()->OnBTNEditLensFlare();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNDeleteLensFlare() {
	try {
		GetView()->OnBTNDeleteLensFlare();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNGenerateMesh() {
	try {
		GetView()->OnBTNGenerateMesh();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTLODSystems() {
	try {
		GetView()->OnBTNENTLODSystems();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTDelLOD() {
	try {
		GetView()->OnBTNENTDelLOD();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTUpdateVisuals() {
	try {
		GetView()->OnBTNENTUpdateVisuals();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTResourceBase() {
	try {
		GetView()->OnBTNENTResourceBase();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTViewRuntimeStats() {
	try {
		GetView()->OnBTNENTViewRuntimeStats();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTViewRuntimeResources() {
	try {
		GetView()->OnBTNENTViewRuntimeResources();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeLISTMISmissileDataBase() {
	try {
		GetView()->OnSelchangeLISTMISmissileDataBase();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISReplaceValues() {
	try {
		GetView()->OnBTNMISReplaceValues();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISEditVisual() {
	try {
		GetView()->OnBTNMISEditVisual();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISAddBillboardMissile() {
	try {
		GetView()->OnBTNMISAddBillboardMissile();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNMISEditDynamics() {
	try {
		GetView()->OnBTNMISEditDynamics();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTEditStartInven() {
	try {
		GetView()->OnBTNENTEditStartInven();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnDropdownCMBOCGTEXFontTableTexture() {
	try {
		GetView()->OnDropdownCMBOCGTEXFontTableTexture();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNCGTEXAdd() {
	try {
		GetView()->OnBTNCGTEXAdd();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNCGTEXDelete() {
	try {
		GetView()->OnBTNCGTEXDelete();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIAddEvent() {
	try {
		GetView()->OnBTNAIAddEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIAddScript() {
	try {
		GetView()->OnBTNAIAddScript();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIEditScript() {
	try {
		GetView()->OnBTNAIEditScript();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIDeleteScript() {
	try {
		GetView()->OnBTNAIDeleteScript();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIEditEvent() {
	try {
		GetView()->OnBTNAIEditEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIDeleteEvent() {
	try {
		GetView()->OnBTNAIDeleteEvent();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIAddTree() {
	try {
		GetView()->OnBTNAIAddTree();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIDeleteTree() {
	try {
		GetView()->OnBTNAIDeleteTree();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAIEdit() {
	try {
		GetView()->OnBTNAIEdit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAISaveAiData() {
	try {
		GetView()->OnBTNAISaveAiData();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNAILoadAiData() {
	try {
		GetView()->OnBTNAILoadAiData();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnSelchangeLISTAIScripts() {
	try {
		GetView()->OnSelchangeLISTAIScripts();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTSaveAppendages() {
	try {
		GetView()->OnBTNENTSaveAppendages();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNENTLoadAppendages() {
	try {
		GetView()->OnBTNENTLoadAppendages();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnEntMount() {
	try {
		GetView()->OnBtnEntMount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBtnEntDemount() {
	try {
		GetView()->OnBtnEntDemount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}
void CKEPEditApp::OnBTNEXPEditExplosionObj() {
	try {
		GetView()->OnBTNEXPEditExplosionObj();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnChangeEDITTCPIPadress() {
	try {
		GetView()->OnChangeEDITTCPIPadress();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnChangeTCPuserName() {
	try {
		GetView()->OnChangeTCPuserName();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnChangeTCPpassword() {
	try {
		GetView()->OnChangeTCPpassword();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNSystemGoingDown() {
	try {
		GetView()->OnBTNSystemGoingDown();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTAddItem() {
	try {
		GetView()->OnBTNENTAddItem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTItemTest() {
	try {
		GetView()->OnBTNENTItemTest();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTDeleteItem() {
	try {
		GetView()->OnBTNENTDeleteItem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTDisarmItem() {
	try {
		GetView()->OnBTNENTDisarmItem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTEditItem() {
	try {
		GetView()->OnBTNENTEditItem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJClone() {
	try {
		GetView()->OnBTNOBJClone();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJTransformation() {
	try {
		GetView()->OnBTNOBJTransformation();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJBoundingGroupSet() {
	try {
		GetView()->OnBTNOBJBoundingGroupSet();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJLibraryAccess() {
	try {
		GetView()->OnBTNOBJLibraryAccess();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENVTimeSystem() {
	try {
		GetView()->OnBTNENVTimeSystem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNMISAddSkeletalMissile() {
	try {
		GetView()->OnBTNMISAddSkeletalMissile();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNMISAddLaser() {
	try {
		GetView()->OnBTNMISAddLaser();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnSelchangeEquipItem() {
	try {
		GetView()->OnSelchangeEquipItem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnSelchangeEquippedItemMeshSlot() {
	try {
		GetView()->OnSelchangeEquippedItemMeshSlot();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnSelchangeEquippedItemMesh() {
	try {
		GetView()->OnSelchangeEquippedItemMesh();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnSelchangeEquippedItemMaterial() {
	try {
		GetView()->OnSelchangeEquippedItemMaterial();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAIEditAITree() {
	try {
		GetView()->OnBTNAIEditAITree();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPSupportedWorlds() {
	try {
		GetView()->OnBTNTCPSupportedWorlds();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTEditDeathCfg() {
	try {
		GetView()->OnBTNENTEditDeathCfg();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPViewAccount() {
	try {
		GetView()->OnBTNTCPViewAccount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnCHECKTXBBitmapLoadOverride() {
	try {
		GetView()->OnCHECKTXBBitmapLoadOverride();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUISkillBuilder() {
	try {
		GetView()->OnBTNUISkillBuilder();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJAddNode() {
	try {
		GetView()->OnBTNOBJAddNode();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJReferenceLibrary() {
	try {
		GetView()->OnBTNOBJReferenceLibrary();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUISoundDB() {
	try {
		GetView()->OnBTNUISoundDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIGlInventoryDB() {
	try {
		GetView()->OnBTNUIGlInventoryDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnUiCurrencysys() {
	try {
		GetView()->OnBtnUiCurrencysys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUICommerceDB() {
	try {
		GetView()->OnBTNUICommerceDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIBankZones() {
	try {
		GetView()->OnBTNUIBankZones();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnUiSafezones() {
	try {
		GetView()->OnBtnUiSafezones();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTUnifySleletalMesh() {
	try {
		GetView()->OnBTNENTUnifySleletalMesh();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNMISDeleteLight() {
	try {
		GetView()->OnBTNMISDeleteLight();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNMISLightSys() {
	try {
		GetView()->OnBTNMISLightSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnUiStatdb() {
	try {
		GetView()->OnBtnUiStatdb();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPItemGeneratorDB() {
	try {
		GetView()->OnBTNTCPItemGeneratorDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnPatchsys() {
	try {
		GetView()->OnBtnPatchsys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPUpdateCurrentOnlinePlayers() {
	try {
		GetView()->OnBTNTCPUpdateCurrentOnlinePlayers();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTScaleEntity() {
	try {
		GetView()->OnBTNENTScaleEntity();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnCHECKSPNEnableSystem() {
	try {
		GetView()->OnCHECKSPNEnableSystem();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTXBSetMipLevels() {
	try {
		GetView()->OnBTNTXBSetMipLevels();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPUpdateAllAcounts() {
	try {
		GetView()->OnBTNTCPUpdateAllAcounts();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTReplaceEntityWSIE() {
	try {
		GetView()->OnBTNENTReplaceEntityWSIE();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPIPBlocking() {
	try {
		GetView()->OnBTNTCPIPBlocking();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnUiWaterzones() {
	try {
		GetView()->OnBtnUiWaterzones();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBtnUiPortalzonesdb() {
	try {
		GetView()->OnBtnUiPortalzonesdb();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIElementDB() {
	try {
		GetView()->OnBTNUIElementDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPUpgradeWorm() {
	try {
		GetView()->OnBTNTCPUpgradeWorm();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNEXPSaveUnit() {
	try {
		GetView()->OnBTNEXPSaveUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNEXPLoadUnit() {
	try {
		GetView()->OnBTNEXPLoadUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPDisconnectAll() {
	try {
		GetView()->OnBTNTCPDisconnectAll();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPPrintReports() {
	try {
		GetView()->OnBTNTCPPrintReports();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAILoadAiUnit() {
	try {
		GetView()->OnBTNAILoadAiUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAISaveAiUnit() {
	try {
		GetView()->OnBTNAISaveAiUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAIImportCollisionSys() {
	try {
		GetView()->OnBTNAIImportCollisionSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAIDeleteSys() {
	try {
		GetView()->OnBTNAIDeleteSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAIExportCurrentCol() {
	try {
		GetView()->OnBTNAIExportCurrentCol();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAISaveColDB() {
	try {
		GetView()->OnBTNAISaveColDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAILoadColDB() {
	try {
		GetView()->OnBTNAILoadColDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNMISSaveUnit() {
	try {
		GetView()->OnBTNMISSaveUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNMISLoadUnit() {
	try {
		GetView()->OnBTNMISLoadUnit();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNSPNExportSys() {
	try {
		GetView()->OnBTNSPNExportSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNSPNImportSys() {
	try {
		GetView()->OnBTNSPNImportSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAIIntegrityCheck() {
	try {
		GetView()->OnBTNAIIntegrityCheck();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPSearchForAccount() {
	try {
		GetView()->OnBTNTCPSearchForAccount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPRestoreBackup() {
	try {
		GetView()->OnBTNTCPRestoreBackup();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTXTSymbolDB() {
	try {
		GetView()->OnBTNTXTSymbolDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnCHECKEditorTxtMode() {
	try {
		GetView()->OnCHECKEditorTxtMode();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTMassFormatNames() {
	try {
		GetView()->OnBTNENTMassFormatNames();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnCHKOBJEnablePlaementHotKey() {
	try {
		GetView()->OnCHKOBJEnablePlaementHotKey();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNOBJReportCurRenderCache() {
	try {
		GetView()->OnBTNOBJReportCurRenderCache();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIIconDBACCESS() {
	try {
		GetView()->OnBTNUIIconDBACCESS();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPSnapShotPacketQue() {
	try {
		GetView()->OnBTNTCPSnapShotPacketQue();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPGenerateLogBasedOnTime() {
	try {
		GetView()->OnBTNTCPGenerateLogBasedOnTime();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIHousingSys() {
	try {
		GetView()->OnBTNUIHousingSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIHszoneDB() {
	try {
		GetView()->OnBTNUIHszoneDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNChkshowSpeed() {
	try {
		GetView()->OnBTNChkshowSpeed();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNAInewTree() {
	try {
		GetView()->OnBTNAInewTree();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPExportAccount() {
	try {
		GetView()->OnBTNTCPExportAccount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPImportAccount() {
	try {
		GetView()->OnBTNTCPImportAccount();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPGenerateClanDatabase() {
	try {
		GetView()->OnBTNTCPGenerateClanDatabase();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTCPClearInactive() {
	try {
		GetView()->OnBTNTCPClearInactive();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNUIAIRainSys() {
	try {
		GetView()->OnBTNUIAIRainSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTsaveItemDB() {
	try {
		GetView()->OnBTNENTsaveItemDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNENTLoadItemDB() {
	try {
		GetView()->OnBTNENTLoadItemDB();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNSPNAppendSys() {
	try {
		GetView()->OnBTNSPNAppendSys();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnButtonLoad() {
	try {
		GetView()->OnButtonLoad();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNTestAssign() {
	try {
		GetView()->OnBTNTestAssign();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNCommCntrlLibBtnA() {
	try {
		GetView()->OnBTNCommCntrlLibBtnA();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNWorldRule() {
	try {
		GetView()->OnBTNWorldRule();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnBTNArenaManager() {
	try {
		GetView()->OnBTNArenaManager();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnObjectSelect() {
	try {
		GetView()->OnObjectSelect();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnObjectMove() {
	try {
		GetView()->OnObjectMove();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnObjectRotate() {
	try {
		GetView()->OnObjectRotate();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnObjectScale() {
	try {
		GetView()->OnObjectScale();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnSelchangeViewports() {
	try {
		GetView()->OnSelchangeViewports();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnSelchangeRuntimeViewports() {
	try {
		GetView()->OnSelchangeRuntimeViewports();
	} catch (...) {
		CStringW msg;
		msg.LoadString(IDS_FATAL_CATCH_ERROR);
		AfxMessageBox(msg, MB_ICONSTOP);
		exit(1);
	}
}

void CKEPEditApp::OnCollapse() {
	// do nothing -- this callback is only in place to ensure
	// the control is not disabled
}

void CKEPEditApp::OnMenu_Help_GamePub() {
	CStringW pubHelp;
	pubHelp = L"file://";
	pubHelp += Utf8ToUtf16(((CKEPEditApp*)AfxGetApp())->GetView()->EditorBaseDir()).c_str();
	pubHelp += L"help\\GamePublishingHelp.html";

	ShellExecute(NULL, L"open", pubHelp, NULL, NULL, SW_SHOW);
}

///////////////////////////////////////////////////////////////////////////////
// Crash Report Support
///////////////////////////////////////////////////////////////////////////////
using namespace CrashReporter;
static void CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	appInstance.CR_AppCallback(cbs);
}

bool CKEPEditApp::CR_Install(const string& projectName) {
	// Install Crash Reporter (default version, without gui)
	return CrashReporter::Install(projectName, "", ::CR_AppCallback);
}

void CKEPEditApp::CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	// Add Files To Crash Report
	string logCfg = KEP::IClientEngine::Instance()->EditorBaseDir();
	logCfg.append("\\KEPLog.cfg");
	CrashReporter::AddLogFiles(logCfg, "KEPEdit Logs");
}
