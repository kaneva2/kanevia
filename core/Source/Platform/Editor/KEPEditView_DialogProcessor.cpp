/******************************************************************************
 KEPEditView_DialogProcessor.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "d3d9.h"
#include "KePEditView.h"

#include "KEPConfigurationsXML.h"

// dialog headers
#include "advancednetworkdlg.h"
#include "airaiddbclass.h"
#include "ArenaManagerDlg.h"
#include "bankzonesdlg.h"
#include "MenuDialog.h" // new menus
#include "commercedlg.h"
#include "currencydlg.h"
#include "currentworldsdlg.h"
#include "ElementsDlg.h"
#include "GameDlg.h"
#include "GlobalAdvancedDlg.h"
#include "globalinventorydlg.h"
#include "houseplacementzonedlg.h"
#include "housingsysdlg.h"
#include "iconsysdlg.h"
#include "MaterialShaderLibDlg.h"
#include "portalsdlg.h"
#include "safezonedlg.h"
#include "scriptdlg.h"
#include "ShaderLibDlg.h"
#include "skillsystemdlg.h"
#include "sounddatabasedlg.h"
#include "spawngendlg.h"
#include "statisticsdbdlg.h"
#include "WaterZoneDBDLG.h"
#include "WorldChannelDlg.h"

#include "CIconSystemClass.h"

#include "StartCfg.h"

// override to do our dialogs
CWnd *CKEPEditView::Preprocess(CWnd *parent, UINT dlgId, UINT dlgType /*=0*/) {
	CWnd *ret = 0;

	switch (dlgId) {
		case CGameDlg::IDD: {
			CGameDlg *dlg = new CGameDlg(StartCfg::Instance(), &m_game);

			dlg->Create(CGameDlg::IDD, parent);
			ret = dlg;
		} break;

		case CGlobalAdvancedDlg::IDD: {
			CGlobalAdvancedDlg *dlg = new CGlobalAdvancedDlg();

			dlg->SetContactSelection(m_selectionModeOn == 1);
			dlg->SetInterpolationGlobal(m_globalShadowType);
			dlg->SetTargetFPS((int)(1.0f / m_fTargetSecondsPerFrame));
			dlg->SetResolutionHeight(ResolutionHeight);
			dlg->SetResolutionWidth(ResolutionWidth);
			dlg->SetWBuffer(W_BufferPreffered == 1);
			dlg->SetCalcEntityCollision(m_entityCollisionOn == 1);
			dlg->m_shaderAvailable = m_shaderSystem;

			dlg->Create(CGlobalAdvancedDlg::IDD, parent);

			// set this after the dialog has been created because
			// we want CGlobalAdvancedDlg::OnInitDialog() to be called
			// (creating the items for the global shader list)
			// before we set the value here, because setting this
			// value also defaults the list
			dlg->SetGlobalShaderID(m_globalWorldShader);

			dlg->RegisterProcessor(this);

			ret = dlg;
		} break;

		case CSkillSystemDlg::IDD: {
			CSkillSystemDlg *dlg = new CSkillSystemDlg(m_skillDatabase);
			dlg->m_edbDatabaseRef = m_movObjRefModelList;
			dlg->m_statDatabaseRef = m_statDatabase;
			dlg->m_globalInventoryDBREF = m_globalInventoryDB;
			dlg->Create(CSkillSystemDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CCurrencyDlg::IDD: {
			CCurrencyDlg *dlg = new CCurrencyDlg();
			dlg->m_currencyName = m_currencyObject->m_currencyName;
			dlg->Create(CCurrencyDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CStatisticsDBDlg::IDD: {
			CStatisticsDBDlg *dlg = new CStatisticsDBDlg(m_statDatabase);
			dlg->Create(CStatisticsDBDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CIconSysDlg::IDD: {
			CIconSysDlg *dlg = new CIconSysDlg(m_iconDatabase);
			dlg->m_texturedatabaseREF = m_pTextureDB;
			dlg->Create(CIconSysDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CElementsDlg::IDD: {
			CElementsDlg *dlg = new CElementsDlg(m_elementDB);
			dlg->Create(CElementsDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CWorldChannelDlg::IDD: {
			CWorldChannelDlg *dlg = new CWorldChannelDlg(m_worldChannelRuleDB);
			dlg->Create(CWorldChannelDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CArenaManagerDlg::IDD: {
			CArenaManagerDlg *dlg = new CArenaManagerDlg(m_battleSchedulerSys);
			dlg->Create(CArenaManagerDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CAIRaidDbClass::IDD: {
			CAIRaidDbClass *dlg = new CAIRaidDbClass(m_aiRaidDatabase);

			Vector3f optionalInitializer;
			optionalInitializer.x = 0.0f;
			optionalInitializer.y = 0.0f;
			optionalInitializer.z = 0.0f;

			POSITION entPosition = m_movObjList->FindIndex(0);
			if (entPosition) { // valid entity
				CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(entPosition);
				ptrLoc->m_baseFrame->GetPosition(NULL, &optionalInitializer);
			}

			dlg->SetPositionX(optionalInitializer.x);
			dlg->SetPositionY(optionalInitializer.y);
			dlg->SetPositionZ(optionalInitializer.z);
			dlg->Create(CAIRaidDbClass::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CSoundDatabaseDlg::IDD: {
			CSoundDatabaseDlg *dlg = new CSoundDatabaseDlg(m_soundManager);
			dlg->directSoundRef = m_pDirectSound;
			dlg->m_ConfigPath = PathConfigs().c_str();
			dlg->Create(CSoundDatabaseDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CGlobalInventoryDlg::IDD: {
			CGlobalInventoryDlg *dlg = new CGlobalInventoryDlg(m_globalInventoryDB);
			m_globalInventoryDB;
			m_movObjRefModelList;
			dlg->m_entityDBRef = m_movObjRefModelList;
			dlg->Create(CGlobalInventoryDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CCommerceDlg::IDD: {
			CCommerceDlg *dlg = new CCommerceDlg(m_commerceDB);
			dlg->Create(CCommerceDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CBankZonesDlg::IDD: {
			CBankZonesDlg *dlg = new CBankZonesDlg(m_bankZoneList);
			dlg->Create(CBankZonesDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CSafeZoneDlg::IDD: {
			CSafeZoneDlg *dlg = new CSafeZoneDlg(m_safeZoneDB);
			dlg->Create(CSafeZoneDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CWaterZoneDBDLG::IDD: {
			CWaterZoneDBDLG *dlg = new CWaterZoneDBDLG(m_waterZoneDB);
			dlg->g_pd3dDeviceRef = g_pd3dDevice;
			dlg->m_waterZnDBREF_memloc = &m_waterZoneDB;
			dlg->Create(CWaterZoneDBDLG::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CPortalsDlg::IDD: {
			CPortalsDlg *dlg = new CPortalsDlg(m_portalDatabase);
			dlg->g_pd3dDeviceRef = g_pd3dDevice;
			dlg->Create(CPortalsDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CHousingSysDlg::IDD: {
			CHousingSysDlg *dlg = new CHousingSysDlg(m_housingDB);
			dlg->GL_textureDataBase = m_pTextureDB;
			dlg->g_pd3dDeviceRef = g_pd3dDevice;
			dlg->Create(CHousingSysDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CHousePlacementZoneDlg::IDD: {
			CHousePlacementZoneDlg *dlg = new CHousePlacementZoneDlg(m_multiplayerObj->m_housingDB);
			dlg->g_pd3dDeviceRef = g_pd3dDevice;
			if (!m_multiplayerObj->m_housingDB)
				m_multiplayerObj->m_housingDB = new CHousingZoneDB();
			dlg->Create(CHousePlacementZoneDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CShaderLibDlg::IDD: {
			CShaderLibDlg *dlg = 0;
			if (dlgType == CShaderLibDlg::VERTEX) {
				dlg = new CShaderLibDlg(m_shaderSystem);
				dlg->m_pd3dDevice = g_pd3dDevice;
				dlg->m_shaderTypeMode = 0;
			} else if (dlgType == CShaderLibDlg::PIXEL) {
				dlg = new CShaderLibDlg(m_pxShaderSystem);
				dlg->m_pd3dDevice = g_pd3dDevice;
				dlg->m_shaderTypeMode = 1;
			}
			if (dlg) {
				dlg->Create(CShaderLibDlg::IDD, parent);
				dlg->RegisterProcessor(this);
				ret = dlg;
			} else {
				ret = NULL;
			}
		} break;

		case CMaterialShaderLibDlg::IDD: {
			CMaterialShaderLibDlg *dlg = new CMaterialShaderLibDlg(m_cgGlobalShaderDB);
			dlg->GL_textureDataBase = m_pTextureDB;
			dlg->Create(CMaterialShaderLibDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CAdvancedNetworkDlg::IDD: {
			CAdvancedNetworkDlg *dlg = new CAdvancedNetworkDlg(m_multiplayerObj);
			m_multiplayerObj->m_tutorialZoneIndex.clearInstanceAndType();
			dlg->Create(CAdvancedNetworkDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case CSpawnGenDlg::IDD: {
			CSpawnGenDlg *dlg = new CSpawnGenDlg(m_serverItemGenerator);
			EnterCriticalSection(&m_multiplayerObj->m_criticalSection);
			dlg->m_aiListRef = m_AIOL;
			dlg->m_skillDatabaseRef = m_skillDatabase;

			if (!m_multiplayerObj->m_currentWorldsUsed) {
				CStringW msg;
				msg.LoadString(IDS_NO_WORLD_LIST_ERROR);
				AfxMessageBox(msg, MB_ICONEXCLAMATION);
				LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
				return ret;
			}

			if (m_multiplayerObj->m_runtimeItemsOnServer) {
				m_multiplayerObj->m_runtimeItemsOnServer->SafeDelete();
				delete m_multiplayerObj->m_runtimeItemsOnServer;
				m_multiplayerObj->m_runtimeItemsOnServer = 0;
			}

			if (m_serverItemGenerator) {
				for (POSITION posLoc = m_serverItemGenerator->GetHeadPosition(); posLoc != NULL;) {
					CServerItemGenObj *spnGenPtr = (CServerItemGenObj *)m_serverItemGenerator->GetNext(posLoc);
					spnGenPtr->m_existsOnServer = FALSE;
				}
			}

			dlg->m_globalInventoryList = m_globalInventoryDB;
			dlg->m_worldsDB = m_multiplayerObj->m_currentWorldsUsed;
			dlg->m_edbRef = m_movObjRefModelList;
			dlg->m_focusEntity = NULL;
			dlg->m_focusEntityExists = FALSE;

			POSITION posLoc = m_movObjList->FindIndex(0);
			if (posLoc) {
				CMovementObj *ptrLoc = (CMovementObj *)m_movObjList->GetAt(posLoc);
				dlg->m_focusEntity = ptrLoc;
				dlg->m_focusEntityExists = TRUE;
			}

			dlg->Create(CSpawnGenDlg::IDD, parent);

			dlg->RegisterProcessor(this);

			ret = dlg;
		} break;

		case CCurrentWorldsDlg::IDD: {
			m_currentWldsSpawnPointModule = 0;
			m_currentWldsSpawnPointModule = new CCurrentWorldsDlg();
			m_currentWldsSpawnPointModule->GL_multiplayerObject = m_multiplayerObj;
			m_currentWldsSpawnPointModule->Create(CCurrentWorldsDlg::IDD, parent);
			m_currentWldsSpawnPointModule->RegisterProcessor(this);
			ret = m_currentWldsSpawnPointModule;
		} break;

		case CDynamicObjectsDlg::IDD: {
			CDynamicObjectsDlg *dlg = new CDynamicObjectsDlg(m_dynamicObjectRM);
			dlg->m_textureDataBase = m_pTextureDB;
			dlg->Create(CDynamicObjectsDlg::IDD, parent);
			dlg->RegisterProcessor(this);
			ret = dlg;
		} break;

		case IDD_DIALOG_TCP_IP: {
			ret = &m_multiPlayer;
		} break;

		case IDD_DIALOG_TEXTINTERFACE: {
			if (m_pTCL) {
				m_textFontDlgBar.SetLoadedFile(m_pTCL->GetLastSavedPath());
			}
			ret = &m_textFontDlgBar;
		} break;

		case IDD_DIALOG_CONTROLERS: {
			ret = &m_controlsDlgBar;
		} break;

		case IDD_DIALOG_SCRIPTING: {
			if (!m_scriptDlgBar.SetType(dlgType)) {
				ret = NULL;
			} else {
				DeleteScriptList(&GL_scriptList);
				m_scriptDlgBar.SendDlgItemMessage(IDC_LIST_SCR_AUTOEXECEVENTLIST, LB_RESETCONTENT, 0, 0);

				if ((dlgType == CScriptDialog::GAME) || (dlgType == CScriptDialog::AI) || (dlgType == CScriptDialog::CLIENT)) {
					// preload
					OnBTNSCRLoadExecFile();
				} else if (dlgType == CScriptDialog::ZONE) {
					OnBTNSCRLoadFromScene();
				}

				ret = &m_scriptDlgBar;
			}
		} break;

		case IDD_MENUS: {
			CMenuDialog *dlg = new CMenuDialog();
			CStringA sConfigsPath = PathConfigs().c_str();
			dlg->SetBaseDir(sConfigsPath.GetString());
			dlg->Create(CMenuDialog::IDD, parent);

			dlg->RegisterProcessor(this);

			ret = dlg;

		} break;

		case IDD_DIALOG_ENVIORMENT: {
			ret = &m_enviormentBar;
		} break;

		case IDD_DIALOG_BACKGROUND: {
			ret = &m_backgroundDlgBar;
		} break;

		case IDD_DIALOG_OBJECTADD: {
			ret = &m_dlgBarObject;
		} break;

		case IDD_DIALOG_AI_BOTS: {
			if (m_AIOL) {
				m_aiDBDlgBar.SetLoadedFile(m_AIOL->GetLastSavedPath());
			}
			ret = &m_aiDBDlgBar;
		} break;

		case IDD_DIALOG_SPAWNGENERATORUI: {
			ret = &m_spawnGenDBDlgBar;
		} break;

		case IDD_DIALOG_ANIMTEXTURES: {
			ret = &m_animatedTextureDBDlgBar;
		} break;

		case IDD_DIALOG_TEXTUREDATA:
			break;

		case IDD_DIALOG_MISSILE_INTERFACE: {
			if (MissileDB) {
				m_missileDlgBar.SetLoadedFile(MissileDB->GetLastSavedPath());
			}
			ret = &m_missileDlgBar;
			m_missileDlgBar.SetSkillDatabase(m_skillDatabase);
			m_missileDlgBar.SetElementDatabase(m_elementDB);
			m_missileDlgBar.LoadOptions();
		} break;

		case IDD_DIALOG_MUSIC: {
			LogError("Soundtrack Dialog Music is deprecated");
		} break;

		case IDD_DIALOG_EXPLOSIONS: {
			if (m_explosionDBList) {
				m_explosionDlgBar.SetLoadedFile(m_explosionDBList->GetLastSavedPath());
			}
			ret = &m_explosionDlgBar;
		} break;

		case IDD_DIALOG_CAMERAVIEWPORTS: {
			if (m_pCOL) {
				m_camVptDlgBar.SetLoadedFile(m_pCOL->GetLastSavedPath());
			}
			ret = &m_camVptDlgBar;
		} break;

		case IDD_DIALOG_ENTITY: {
			//so that the dialog knows a file has been loaded
			if (m_movObjRefModelList) {
				m_entityDlgBar.SetLoadedFile(m_movObjRefModelList->GetLastSavedPath());
			}
			ret = &m_entityDlgBar;
		} break;

		case IDD_DIALOG_MOVIES: {
			ret = &m_moviesDlgBar;
		} break;

		case IDD_DIALOG_PARTICLESYSTEM: {
			ret = &m_particleDBDlgBar;
		} break;
		default:

			ASSERT(0);
			break;
	};

	OnChangeDialog();

	return ret;
}

BOOL CKEPEditView::PostProcess(CWnd *wnd, UINT dlgId) {
	BOOL ret = TRUE;

	switch (dlgId) {
		case CGlobalAdvancedDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CGlobalAdvancedDlg)));
			CGlobalAdvancedDlg *dlg = (CGlobalAdvancedDlg *)wnd;

			W_BufferPreffered = dlg->GetWBuffer();
			m_globalWorldShader = dlg->GetGlobalShaderID();
			m_renderStateObject->SetVertexShader(g_pd3dDevice, m_shaderSystem, m_globalWorldShader);
			float fTargetFPS = (float)dlg->GetTargetFPS();
			m_fTargetSecondsPerFrame = fTargetFPS <= 0 ? 1.0f : 1.0f / fTargetFPS;
			m_globalShadowType = dlg->GetInterpolationGlobal();
			ResolutionHeight = dlg->GetResolutionHeight();
			ResolutionWidth = dlg->GetResolutionWidth();
			m_selectionModeOn = dlg->GetContactSelection();
			m_entityCollisionOn = dlg->GetCalcEntityCollision();
			SetAllRenderStatesIM();

		} break;

		case CSkillSystemDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CSkillSystemDlg)));
			InListMissileSkillCriteria(0);
		} break;

		case CCurrencyDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CCurrencyDlg)));
			CCurrencyDlg *dlg = (CCurrencyDlg *)wnd;

			m_currencyObject->m_currencyName = dlg->m_currencyName;
		} break;

		case CStatisticsDBDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CStatisticsDBDlg)));
		} break;

		case CIconSysDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CIconSysDlg)));
		} break;

		case CElementsDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CElementsDlg)));
		} break;

		case CWorldChannelDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CWorldChannelDlg)));
		} break;

		case CArenaManagerDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CArenaManagerDlg)));
		} break;

		case CAIRaidDbClass::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CAIRaidDbClass)));
		} break;

		case CSoundDatabaseDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CSoundDatabaseDlg)));
		} break;

		case CGlobalInventoryDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CGlobalInventoryDlg)));
		} break;

		case CCommerceDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CCommerceDlg)));
		} break;

		case CBankZonesDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CBankZonesDlg)));
		} break;

		case CSafeZoneDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CSafeZoneDlg)));
		} break;

		case CWaterZoneDBDLG::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CWaterZoneDBDLG)));
		} break;

		case CPortalsDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CPortalsDlg)));
		} break;

		case CHousingSysDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CHousingSysDlg)));
			CHousingSysDlg *dlg = (CHousingSysDlg *)wnd;

			//m_housingDB = dlg->m_houseDBREF;
			if (dlg->GetSpawnHouseOnClose()) {
				POSITION entPosition = m_movObjList->FindIndex(0);
				if (!entPosition) {
					AfxMessageBox(IDS_MSG_NORUNTIMESPAWNED);
					return ret;
				}

				CMovementObj *entityPtr = (CMovementObj *)m_movObjList->GetAt(entPosition);
				Vector3f position;
				entityPtr->m_baseFrame->GetPosition(NULL, &position);
				m_housingCache->PlaceHouse(position, dlg->m_houseSelection, 22);
			}

		} break;

		case CShaderLibDlg::IDD: {
			// This code is always called regardless if the user
			// hit ok or not.

			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CShaderLibDlg)));
			CShaderLibDlg *dlg = (CShaderLibDlg *)wnd;

			if (dlg->GetType() == CShaderLibDlg::VERTEX) {
				// Vertex shader
				//m_shaderSystem = dlg->m_shaderLibRef;
				m_shaderSystem->Compile(g_pd3dDevice);
				m_renderStateObject->m_shderSystemRef = m_shaderSystem;
			} else if (dlg->GetType() == CShaderLibDlg::PIXEL) {
				// Pixel shader
				//m_pxShaderSystem = dlg->m_shaderLibRef;
				m_pxShaderSystem->Compile(g_pd3dDevice);
				m_renderStateObject->m_pxShderSystemRef = m_pxShaderSystem;
			}
		} break;

		case CMaterialShaderLibDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CMaterialShaderLibDlg)));
		} break;

		case CAdvancedNetworkDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CAdvancedNetworkDlg)));
			//CAdvancedNetworkDlg *dlg = (CAdvancedNetworkDlg*)wnd;

			// Save a part of the master SVR file. This facilitates the Editor editing and saving some
			// parts of the SVR file while allowing the SVR file to standalone. These parts are merged back
			// in when the SVR file is loaded. Thus, the Editor can modify these parts, an Admin can make changes
			// to the SVR file, and they can stand independent from one another.

			m_multiplayerObj->SerializeToXML(PathAdd(PathConfigs(), NETWORK_DEFINITION_ZONE_DB_FILE));
		} break;

		case CSpawnGenDlg::IDD: {
			ASSERT(wnd->IsKindOf(RUNTIME_CLASS(CSpawnGenDlg)));
			LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
		} break;

		case CCurrentWorldsDlg::IDD: {
			m_multiplayerObj->m_currentWorldsUsed->SerializeToXML(PathAdd(PathConfigs(), SUPPORTED_WORLDS_CONFIG_FILE));
			m_multiplayerObj->m_spawnPointCfgs->SerializeToXML(PathAdd(PathConfigs(), SPAWN_POINTS_CONFIG_FILE));
		} break;

		case CDynamicObjectsDlg::IDD:
			break;

		default:
			break; // must be a dialog bar we don't care about
	};

	return ret;
}
