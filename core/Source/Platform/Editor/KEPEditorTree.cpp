/******************************************************************************
 KEPEditorTree.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// KEPEditorTree.cpp : implementation file
//

#include "stdafx.h"
#include "KEPEditGlobals.h"
#include "ScriptDlg.h"
#include "KEPEditorTree.h"
#include "CoronaUpdater.h"
#include "ShaderLibDlg.h"

#include "resource.h"

// CKEPEditorTree

IMPLEMENT_DYNAMIC(CKEPEditorTree, CTreeCtrl)
CKEPEditorTree::CKEPEditorTree() {
	m_hWndParent = 0;
	m_notifyTreeChange = true;
	m_gameOpen = FALSE;
	m_zoneOpen = FALSE;
	m_currentTabName = KEP_GAMETREE;
}

CKEPEditorTree::~CKEPEditorTree() {
}

BEGIN_MESSAGE_MAP(CKEPEditorTree, CTreeCtrl)
ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnTvnSelchanged)
ON_WM_DESTROY()
ON_NOTIFY_REFLECT(NM_RCLICK, OnRClick)
ON_WM_CONTEXTMENU()
ON_WM_PAINT()
END_MESSAGE_MAP()

BOOL CKEPEditorTree::Init(HWND hWndParent) {
	m_hWndParent = hWndParent;

	return LoadTab(KEP_GAMETREE);
}

BOOL CKEPEditorTree::LoadTab(KEPEDITTREEID tabName) {
	NotifyTreeChange(FALSE);

	DeleteAllItems();

	switch (tabName) {
		case KEP_GAMETREE:
			InitGameTab();
			m_currentTabName = KEP_GAMETREE;
			break;

		case KEP_ZONETREE:
			InitZoneTab();
			m_currentTabName = KEP_ZONETREE;
			break;

		case KEP_AIMISSIONSTREE:
			InitAIMissionsTab();
			m_currentTabName = KEP_AIMISSIONSTREE;
			break;

		case KEP_OBJECTSTREE:
			InitObjectsTab();
			m_currentTabName = KEP_OBJECTSTREE;
			break;

		default:
			break;
	}

	NotifyTreeChange(TRUE);

	return TRUE;
}

void CKEPEditorTree::InitGameTab() {
	CStringW title;

	//title.LoadString(IDS_NODE_ADVANCED);
	//InsertNode( title, 1, IDD_DIALOG_GLOBAL_ADVANCED, 0, GameOpen() );

	title.LoadString(IDS_NODE_ARENA_MANAGER);
	InsertNode(title, 1, IDD_DIALOG_ARENAMANAGER, 0, GameOpen());

	title.LoadString(IDS_NODE_CONTROLS);
	InsertNode(title, 1, IDD_DIALOG_CONTROLERS, 0, GameOpen());

	//title.LoadString(IDS_NODE_CURRENCY);
	//InsertNode( title, 1, IDD_DIALOG_CURRENCYOBJECT, 0, GameOpen());

	title.LoadString(IDS_NODE_ELEMENTS);
	InsertNode(title, 1, IDD_DIALOG_ELEMENTS, 0, GameOpen());

	title.LoadString(IDS_NODE_GENERAL);
	InsertNode(title, 1, IDD_DIALOG_GAME_GENERAL, 0, GameOpen());

	title.LoadString(IDS_NODE_ICONS);
	InsertNode(title, 1, IDD_DIALOG_ICONSYS, 0, GameOpen());

	title.LoadString(IDS_NODE_MENUS);
	InsertNode(title, 1, IDD_MENUS, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_SERVER_EDIT);
	InsertNode(title, 1, IDD_DIALOG_ADVANCEDNETWORK, 0, GameOpen());

	title.LoadString(IDS_NODE_SCRIPT_AUT);
	InsertNode(title, 1, IDD_DIALOG_SCRIPTING, CScriptDialog::GAME, GameOpen());

	title.LoadString(IDS_NODE_CLIENT_SCRIPT);
	InsertNode(title, 1, IDD_DIALOG_SCRIPTING, CScriptDialog::CLIENT, GameOpen());

	title.LoadString(IDS_NODE_SKILLS);
	InsertNode(title, 1, IDD_DIALOG_SKILLSYSTEMDLG, 0, GameOpen());

	title.LoadString(IDS_NODE_SUPPORTED_WORLDS);
	InsertNode(title, 1, IDD_DIALOG_CURRENTWORLDS, 0, GameOpen());

	title.LoadString(IDS_NODE_STATS);
	InsertNode(title, 1, IDD_DIALOG_STATISTICSDB, 0, GameOpen());

	title.LoadString(IDS_NODE_TEXT_FONTS);
	InsertNode(title, 1, IDD_DIALOG_TEXTINTERFACE, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_WORLD_RULES);
	InsertNode(title, 1, IDD_COMBO_RULECHANNEL, 0, GameOpen());
}

void CKEPEditorTree::InitZoneTab() {
	CStringW title;

	title.LoadString(IDS_NODE_ENVIRONMENT);
	InsertNode(title, 1, IDD_DIALOG_ENVIORMENT, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_SCRIPT_EXG);
	InsertNode(title, 1, IDD_DIALOG_SCRIPTING, CScriptDialog::ZONE, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_ZONE_OBJECTS);
	InsertNode(title, 1, IDD_DIALOG_OBJECTADD, 0, GameOpen() && ZoneOpen());
}

void CKEPEditorTree::InitAIMissionsTab() {
	CStringW title;

	title.LoadString(IDS_NODE_A_I);
	InsertNode(title, 1, IDD_DIALOG_AI_BOTS, 0, GameOpen());

	title.LoadString(IDS_NODE_A_I_TRIGGERED_SPAWN);
	InsertNode(title, 1, IDD_DIALOG_AIRAIDDLG, 0, GameOpen());

	title.LoadString(IDS_NODE_NPC_CONFIGURATION);
	InsertNode(title, 1, IDD_DIALOG_SPAWNGENDLG, 0, GameOpen());

	title.LoadString(IDS_NODE_A_I_SCRIPT);
	InsertNode(title, 1, IDD_DIALOG_SCRIPTING, CScriptDialog::AI, GameOpen());

	title.LoadString(IDS_NODE_SPAWN_GENERATORS);
	InsertNode(title, 1, IDD_DIALOG_SPAWNGENERATORUI, 0, GameOpen() && ZoneOpen());
}

void CKEPEditorTree::InitObjectsTab() {
	CStringW title;

	title.LoadString(IDS_NODE_ENTITIES);
	InsertNode(title, 1, IDD_DIALOG_ENTITY, 0, GameOpen());

	title.LoadString(IDS_NODE_BANK_ZONE);
	InsertNode(title, 1, IDD_DIALOG_BANKZONES, 0, GameOpen());

	title.LoadString(IDS_NODE_CAM_VIEW);
	InsertNode(title, 1, IDD_DIALOG_CAMERAVIEWPORTS, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_COMMERCE_DB);
	InsertNode(title, 1, IDD_DIALOG_COMMERCE, 0, GameOpen());

	title.LoadString(IDS_NODE_DYNAMIC_OBJECTS);
	InsertNode(title, 1, IDD_DIALOG_DYNAMIC_OBJECTS, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_EXPLOSION_DB);
	InsertNode(title, 1, IDD_DIALOG_EXPLOSIONS, 0, GameOpen());

	title.LoadString(IDS_NODE_PIXEL_SHADERS);
	InsertNode(title, 1, IDD_DIALOG_SHADERLIBEDITOR, CShaderLibDlg::PIXEL, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_GLOBAL_INVENTORY);
	InsertNode(title, 1, IDD_DIALOG_GLOBALINVENTORYSYS, 0, GameOpen());

	title.LoadString(IDS_NODE_HOUSING_SYSTEM);
	InsertNode(title, 1, IDD_DIALOG_HOUSESYSTEM, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_HOUSING_ZONE);
	InsertNode(title, 1, IDD_DIALOG_PLACEMENTZONEDLG, 0, GameOpen() && ZoneOpen());

	//title.LoadString(IDS_NODE_MATERIAL_SHADERS);
	//InsertNode( title, 1, IDD_DIALOG_MATERIALSHADERLIB, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_MUSIC);
	InsertNode(title, 1, IDD_DIALOG_MUSIC, 0, GameOpen());

	title.LoadString(IDS_NODE_PARTICLE_SYSTEM);
	InsertNode(title, 1, IDD_DIALOG_PARTICLESYSTEM, 0, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_PORTAL_ZONE);
	InsertNode(title, 1, IDD_DIALOG_PORTALSDLG, 0, GameOpen());

	title.LoadString(IDS_NODE_SAFE_ZONE);
	InsertNode(title, 1, IDD_DIALOG_SAFEZONEDLG, 0, GameOpen());

	title.LoadString(IDS_NODE_SOUND_DB);
	InsertNode(title, 1, IDD_DIALOG_SOUNDDATABASE, 0, GameOpen());

	title.LoadString(IDS_NODE_TEXTURE_DB);
	InsertNode(title, 1, IDD_DIALOG_TEXTUREDATA, 0, GameOpen());

	//title.LoadString(IDS_NODE_VERTEX_SHADERS);
	//InsertNode( title, 1, IDD_DIALOG_SHADERLIBEDITOR, CShaderLibDlg::VERTEX, GameOpen() && ZoneOpen());

	title.LoadString(IDS_NODE_WATER_ZONE);
	InsertNode(title, 1, IDD_DIALOG_WATERZONEDB, 0, GameOpen());

	title.LoadString(IDS_NODE_WEAPON_SYSTEMS);
	InsertNode(title, 1, IDD_DIALOG_MISSILE_INTERFACE, 0, GameOpen());
}

void CKEPEditorTree::InsertNode(const wchar_t* nodeName, WORD indentLevel, UINT dialogId, UINT dialogType, const BOOL enableFlag) {
	static CPtrArray nodePtrs;
	HTREEITEM item;
	LPKEPTREEDATA ktd;

	if (nodePtrs.GetSize() == 0) {
		nodePtrs.Add(TVI_ROOT);
	}

	if (nodePtrs.GetUpperBound() >= indentLevel) {
		nodePtrs.RemoveAt(indentLevel, (nodePtrs.GetUpperBound() - indentLevel));
	}

	item = InsertItem(nodeName, static_cast<HTREEITEM>(nodePtrs.GetAt(indentLevel - 1)));

	ktd = (LPKEPTREEDATA)malloc(sizeof(KEPTREEDATA));

	ktd->wstrObjectLink = nodeName;

	ktd->dialogId = dialogId;
	ktd->dialogType = dialogType;
	ktd->disabled = enableFlag;

	SetItemData(item, (DWORD_PTR)ktd);

	nodePtrs.InsertAt(indentLevel, item);
}

BOOL CKEPEditorTree::FreeChildTree(HTREEITEM hItem) {
	HTREEITEM hTemp = hItem;
	HTREEITEM child;

	TVITEM item;
	item.mask = TVIF_HANDLE | TVIF_PARAM;

	while (hTemp) {
		item.hItem = hTemp;
		if (GetItem(&item)) {
			if (item.lParam)
				free((LPKEPTREEDATA)item.lParam);

			if (ItemHasChildren(item.hItem)) {
				child = GetChildItem(item.hItem);
				FreeChildTree(child);
			}
		}

		// Try to get the next item
		hTemp = GetNextItem(hTemp, TVGN_NEXT);
	}

	return TRUE;
}

BOOL CKEPEditorTree::DeleteAllItems() {
	//call this recursively to remove all the additional data.
	if (GetCount() != 0)
		FreeChildTree(GetRootItem());

	return CTreeCtrl::DeleteAllItems();
}

void CKEPEditorTree::OnTvnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) {
	//LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	HTREEITEM hti = GetSelectedItem();

	if ((hti) &&
		(hti != m_lastSelectedItem) &&
		(NotifyTreeChange())) {
		KEPSELECT ks;
		LPKEPTREEDATA pks = 0;
		TVITEM item;
		item.mask = TVIF_HANDLE | TVIF_PARAM;
		item.hItem = hti;
		if (GetItem(&item))
			pks = (LPKEPTREEDATA)item.lParam;
		if (pks && (pks->disabled)) {
			//notify the parent window that we just changed selections
			ks.wstrObjectLink = pks->wstrObjectLink;
			::SendMessage(m_hWndParent, KEP_TREECHANGE, 0, (LPARAM)pks);
			m_lastSelectedItem = hti;
		} else {
			SetItemState(hti, 0, TVIS_SELECTED);
		}
	}

	*pResult = 0;
}

void CKEPEditorTree::OnDestroy() {
	NotifyTreeChange(FALSE);

	//free all memory and remove items
	DeleteAllItems();

	CTreeCtrl::OnDestroy();
}

// added for Context menus
void CKEPEditorTree::OnRClick(NMHDR* pNMHDR, LRESULT* pResult) {
	TRACE0("CKEPEditorTree::OnRClick()\n");
	// Send WM_CONTEXTMENU to self
	SendMessage(WM_CONTEXTMENU, (WPARAM)m_hWnd, GetMessagePos());
	// Mark message as handled and suppress default handling
	*pResult = 1;
}

void CKEPEditorTree::OnContextMenu(CWnd* pWnd, CPoint ptMousePos) {
	// if Shift-F10
	if (ptMousePos.x == -1 && ptMousePos.y == -1)
		ptMousePos = (CPoint)GetMessagePos();

	ScreenToClient(&ptMousePos);

	UINT uFlags;
	HTREEITEM htItem;

	htItem = HitTest(ptMousePos, &uFlags);

	if (htItem == NULL)
		return;

		//LPKEPTREEDATA ktd = (LPKEPTREEDATA)GetItemData( htItem );

#ifndef USE_CView
	if (ktd && ktd->getSetObj && ktd->getSetObj->GetUpdater()) {
		ClientToScreen(&ptMousePos);
		GetSetUpdateData::EContextRet ret = ktd->getSetObj->GetUpdater()->ContextMenu(ktd->getSetObj, ptMousePos.x, ptMousePos.y, this);
	}
#endif
}

///---------------------------------------------------------
// CKEPEditorTree::OnPaint
//
// Draw tree items that are out of context as gray when they have been
// disabled.
//
void CKEPEditorTree::OnPaint() {
	CPaintDC dc(this);

	// Create a memory DC compatible with the paint DC
	CDC memDC;
	memDC.CreateCompatibleDC(&dc);

	CRect rcClip, rcClient;
	dc.GetClipBox(&rcClip);
	GetClientRect(&rcClient);

	// Select a compatible bitmap into the memory DC
	CBitmap bitmap;
	bitmap.CreateCompatibleBitmap(&dc, rcClient.Width(), rcClient.Height());
	memDC.SelectObject(&bitmap);

	// Set clip region to be same as that in paint DC
	CRgn rgn;
	rgn.CreateRectRgnIndirect(&rcClip);
	memDC.SelectClipRgn(&rgn);
	rgn.DeleteObject();

	// First let the control do its default drawing.
	CWnd::DefWindowProc(WM_PAINT, (WPARAM)memDC.m_hDC, 0);

	HTREEITEM hItem = GetFirstVisibleItem();

	int n = GetVisibleCount() + 1;
	while (hItem && n--) {
		CRect rect;

		// Do not meddle with selected items or drop highlighted items
		UINT selflag = TVIS_DROPHILITED | TVIS_SELECTED;

		if (!(GetItemState(hItem, selflag) & selflag)) {
			CFont* pFontDC;
			CFont fontDC;
			LOGFONT logfont;

			CFont* pFont = GetFont();
			pFont->GetLogFont(&logfont);

			fontDC.CreateFontIndirect(&logfont);
			pFontDC = memDC.SelectObject(&fontDC);

			LPKEPTREEDATA ktd = (LPKEPTREEDATA)GetItemData(hItem);

			if (ktd->disabled) {
				memDC.SetTextColor(RGB(0, 0, 0)); //black
			} else {
				memDC.SetTextColor(RGB(140, 140, 140)); //gray
			}

			CStringW sItem = GetItemText(hItem);

			GetItemRect(hItem, &rect, TRUE);
			memDC.SetBkColor(GetSysColor(COLOR_WINDOW));
			memDC.TextOut(rect.left + 2, rect.top + 1, sItem);

			memDC.SelectObject(pFontDC);
		}
		hItem = GetNextVisibleItem(hItem);
	}

	dc.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &memDC,
		rcClip.left, rcClip.top, SRCCOPY);
}

///---------------------------------------------------------
// SetGameStatus
//
// [in] gameOpen
// [in] zoneOpen
//
void CKEPEditorTree::SetGameStatus(const BOOL gameOpen, const BOOL zoneOpen) {
	m_gameOpen = gameOpen;
	m_zoneOpen = zoneOpen;

	// Refresh current tab
	LoadTab(m_currentTabName);
}