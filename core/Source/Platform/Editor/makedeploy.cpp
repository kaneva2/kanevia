#include "stdafx.h"

#include "makedeploy.h"

#include <io.h>
#include "resource.h"
#include "DistributionUpdateGlobals.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "direct.h"
#include "DistributionProgressiveDlg.h"
#include "KEPFileNames.h"

#include "common\include\CompressHelper.h"

char g_szINIFile[25] = { "makedeploy.ini\0" };

static long B2Compress(const string &filePathIn, const string &filePathOut, const string &fileNameOut) {
	// Create Destination Folder (deep)
	string filePathDest = ::CompressTypeExtAdd(PathAdd(filePathOut, fileNameOut));
	string destPath = StrStripFileNameExt(filePathDest);
	if (!FileHelper::CreateFolderDeep(destPath)) {
		//		LogError("CreateFolderDeep() FAILED - '" << destPath << "'");
	} else {
		// Compress File To Destination
		if (!::CompressFile(filePathIn, filePathDest)) {
			//			LogError("CompressFile() FAILED - '" << sourcePath << "'");
		}
	}

	return FileHelper::Size(filePathDest);
}

CMakepatch::CMakepatch(HWND progressDlg, HANDLE distCloseEvent, HANDLE closeEvent, UINT msgId, bool skipdialog) :
		m_ProgramDir(""), m_WindowsDir(""), m_skipDialog(skipdialog) {
	m_progressDlg = progressDlg;
	m_distCloseEvent = distCloseEvent;
	m_closeEvent = closeEvent;
	m_msgId = msgId;
	m_ZipUp = true;
}

// Find this file in the last distribution if it exists.
inline int findIndex(CRCLIST *patchData, int oldFileCount, CStringA path, CStringA filename) {
	for (int i = 0; i < oldFileCount; i++) {
		if (strcmp(filename, patchData[i].filename.c_str()) == 0) {
			// Might have to also check dir incase same filename in mult folders
			return i;
		}
	}
	return -1;
}

bool CMakepatch::TraverseDirectory(const char *cFileName, char *outfile, BOOL bZipped, long *totalSize, int *totalFiles, CRCLIST *oldpatchData, int oldFileCount) {
	STATUSU pStatus;

	char szCurrentData[1024];
	char *szExtention;
	char *pCurrent;
	char szFileToZipWithPath[MAX_PATH];
	memset(szFileToZipWithPath, 0, sizeof(szFileToZipWithPath));
	CStringW RelativePathW;

	//start old
	if (strcmp(cFileName, "..") == 0)
		return 0;
	if (strcmp(cFileName, ".") == 0)
		return 0;

	WIN32_FIND_DATA FindData;
	char szTemp[_MAX_PATH + 1];
	char szTempFilename[64];
	char *szMD5 = NULL;

	if (strcmp(cFileName, "") != 0) {
		SetCurrentDirectory(Utf8ToUtf16(cFileName).c_str());
	}

	if (strcmp(cFileName, m_SrcDir) == 0)
		cFileName = "";

	//determine the relative path for the file name entry
	_wgetcwd(RelativePathW.GetBufferSetLength(MAX_PATH), MAX_PATH);
	RelativePathW.ReleaseBuffer();
	CStringA RelativePath = Utf16ToUtf8(RelativePathW).c_str();
	if (RelativePath == m_SrcDir)
		RelativePath = "";
	else
		RelativePath = RelativePath.Mid(m_SrcDir.GetLength() + 1, RelativePath.GetLength() - m_SrcDir.GetLength());

	HANDLE fp = FindFirstFileW(L"*.*", &FindData);
	if (fp) {
		while (TRUE) {
			std::string strFileName = Utf16ToUtf8(FindData.cFileName);
			if ((FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				TraverseDirectory(strFileName.c_str(), outfile, bZipped, totalSize, totalFiles, oldpatchData, oldFileCount);
			} else {
				bool looseDir = false;
				//
				// Don't include these files, they are special cases at the moment.
				//
				if (strFileName != "CoreAnimations.pak" &&
					strFileName != "CoreMeshes.pak" &&
					strFileName != "textureinfo.pak") {
					// Check to see if this is a loose file folder
					for (strVect::iterator i = m_looseTextureDirs.begin(); i != m_looseTextureDirs.end(); i++) {
						CStringW toMove = Utf8ToUtf16(i->first.c_str()).c_str();
						if (toMove.CompareNoCase(RelativePathW) == 0) {
							looseDir = true;
						}
					}
				}
				// Now we also patch pak files in loose folders.
				if ((strFileName != VERSIONINFO_DAT && !looseDir) || (looseDir && strFileName == "dds.pak")) {
					int oldindex = -1;
					memset(&szCurrentData[0], 0, sizeof(szCurrentData));
					CStringA csTemp, csRTemp;
					for (int recur = 0; recur < m_nRecursionLevel; recur++)
						csRTemp += "_";

					if (!RelativePath.IsEmpty())
						csTemp.Format("%s---%s", RelativePath, Utf16ToUtf8(FindData.cFileName).c_str());
					else
						csTemp = Utf16ToUtf8(FindData.cFileName).c_str();

					GetPrivateProfileStringA("FILES", csTemp, "", szCurrentData, 1024, outfile);

					//hash it
					szMD5 = MDFile(strFileName.c_str());
					strcpy_s(szTempFilename, _countof(szTempFilename), strFileName.c_str());

					char *tokTemp;
					szExtention = strtok_s(szTempFilename, ".", &tokTemp);
					szExtention = strtok_s(NULL, ".", &tokTemp);

					if (strlen(szCurrentData) > 0) {
						pCurrent = strtok_s(szCurrentData, ",", &tokTemp);
						pCurrent = strtok_s(NULL, "|", &tokTemp);

						_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s,%s", szMD5, pCurrent);
					} else {
						csTemp.Replace("\\", "_");
						oldindex = findIndex(oldpatchData, oldFileCount, RelativePath, csTemp);
						if (strlen(cFileName) == 0)
							_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s,,%s.lzma,No", szMD5, csTemp);
						else
							_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s,%s,%s.lzma,No", szMD5, RelativePath, csTemp);
					}

					int file_size = 0;

					if (m_ZipUp) {
						memset(szFileToZipWithPath, 0, sizeof(szFileToZipWithPath));
						if (m_SrcDir.GetAt(m_SrcDir.GetLength() - 1) != '\\')
							_snprintf_s(szFileToZipWithPath, _countof(szFileToZipWithPath), _TRUNCATE, "%s\\%s\\%s", m_SrcDir, cFileName, FindData.cFileName);
						else
							_snprintf_s(szFileToZipWithPath, _countof(szFileToZipWithPath), _TRUNCATE, "%s%s\\%s", m_SrcDir, cFileName, FindData.cFileName);

						char zipDest[_MAX_PATH + 1];
						memset(zipDest, 0, sizeof(zipDest));
						_snprintf_s(zipDest, _countof(zipDest), _TRUNCATE, "%s", m_ZipDest);
						file_size = B2Compress(strFileName, zipDest, csTemp.GetBuffer(0));
					}

					_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s,%d", szTemp, file_size);
					// Modifications for progressive distribution
					if (oldindex >= 0) {
						char *required;
						if (oldpatchData[oldindex].bRequired) {
							required = "Yes";
						} else {
							required = "No";
						}
						_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s,%s,%s", szTemp, required, oldpatchData[oldindex].command);
						CStringA *temptemp = new CStringA(szTemp); // hack to get rid of trailing newline
						temptemp->Replace("\n", "");
						_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s", temptemp->GetString());
						delete temptemp;

					} else {
						// defaults
						_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s,%s,%s", szTemp, "Yes", "None");
					}
					// end progressive addition.

					WritePrivateProfileStringA("FILES", csTemp, szTemp, outfile);

					*totalSize += file_size;
					(*totalFiles)++;

					memset(pStatus.fileName, 0, sizeof(pStatus.fileName));
					strcpy_s(pStatus.fileName, _countof(pStatus.fileName), strFileName.c_str());
					::SendMessage(m_progressDlg, m_msgId, MSG_STATUS_MAKEDEPLOY_UPDATE, (LPARAM)&pStatus);

					if (WaitForSingleObject(m_closeEvent, 0) == WAIT_OBJECT_0) {
						SetEvent(m_distCloseEvent);
						FindClose(fp);
						return false;
					}

				} //not versioninfo.dat
				else {
					if (strcmp(strFileName.c_str(), VERSIONINFO_DAT) != 0) {
						// Handle loose file folder
						CStringA folderTarget = RelativePath;
						folderTarget.Replace("\\", "_");
						char zipDest[255];
						memset(zipDest, 0, sizeof(zipDest));
						sprintf_s(zipDest, _countof(zipDest), "%s\\%s", m_ZipDest, folderTarget);
						B2Compress(strFileName, zipDest, strFileName);
					}
				}
			} //find data
			if (!FindNextFile(fp, &FindData))
				break;
		} // while true
	} //if fp

	FindClose(fp);

	SetCurrentDirectory(L"..");

	return true;
}

void CMakepatch::CleanDirectory(const char *dir) {
	WIN32_FIND_DATA FindData;

	if (!SetCurrentDirectoryW(Utf8ToUtf16(dir).c_str())) {
		//not there it will be created later
		return;
	}

	HANDLE fp = FindFirstFileW(L"*.*", &FindData);
	if (fp) {
		while (TRUE) {
			if (wcscmp(FindData.cFileName, L"..") != 0 && wcscmp(FindData.cFileName, L".") != 0)
				DeleteFile(FindData.cFileName);

			if (!FindNextFile(fp, &FindData))
				break;
		}

		FindClose(fp);
	}
}

int CMakepatch::Make(CStringA sourceDirectory, CStringA destinationDirectory, CStringA version, CRCLIST *oldpatchData, int oldFileCount, IN strVect &looseTextureDirs) {
	std::wstring origDir = Utf8ToUtf16(FileHelper::GetCurrentDirectory());

	m_SrcDir = sourceDirectory;
	m_ZipDest = destinationDirectory;
	m_Version = version;

	m_looseTextureDirs = looseTextureDirs;

	int ret = Generate(oldpatchData, oldFileCount);

	SetCurrentDirectoryW(origDir.c_str());

	return ret;
}

//char				filename[_MAX_PATH+1];
//char				directory[_MAX_PATH+1];
//char				serverfile[_MAX_PATH+1];
//char				crc[32];
//BOOL				bRegister;
//LONG				nFilesize;
//BOOL				bUpToDate;
//BOOL				bRequired;
//char				command[10];

// Added by Jonny to convert data from progressive distro menu back into the versioninfo.
void CMakepatch::createProgressiveInfo(CRCLIST *finalfiles, int finalfilecount, char *outputfile) {
	for (int i = 0; i < finalfilecount; i++) {
		char szTemp[_MAX_PATH + 1];
		CStringA srequired, sregister;
		if (finalfiles[i].bRegister) {
			sregister = "Yes";
		} else {
			sregister = "No";
		}
		if (finalfiles[i].bRequired) {
			srequired = "Yes";
		} else {
			srequired = "No";
		}

		_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%s,%s,%s,%s,%d,%s,%s", finalfiles[i].crc, finalfiles[i].directory, finalfiles[i].serverfile, sregister, finalfiles[i].nFilesize, srequired, finalfiles[i].command);
		WritePrivateProfileStringA("FILES", finalfiles[i].filename.c_str(), szTemp, outputfile);
	}
}

int CMakepatch::Generate(CRCLIST *oldpatchData, int oldFileCount) {
	char szTemp[_MAX_PATH + 1];
	int totalfiles = 0;
	long totalSize = 0;
	char outfile[_MAX_PATH + 1]; // Leaving this as a "ANSI" characters since we don't want to Read/Write the profile strings using wide characters.

	char szProgramDir[_MAX_PATH + 1];
	char szWindowsDir[_MAX_PATH + 1];

	m_nRecursionLevel = -1;

	char szFileToZipWithPath[2048];

	memset(szFileToZipWithPath, 0, sizeof(szFileToZipWithPath));

	WritePrivateProfileStringA("General", "WindowsDir", m_WindowsDir, g_szINIFile);
	WritePrivateProfileStringA("General", "Version", m_Version, g_szINIFile);
	WritePrivateProfileStringA("General", "ProgramDir", m_ProgramDir, g_szINIFile);
	WritePrivateProfileStringA("General", "SourceDir", m_SrcDir, g_szINIFile);
	WritePrivateProfileStringA("General", "ZipDest", m_ZipDest, g_szINIFile);

	_snprintf_s(szProgramDir, _countof(szProgramDir), _TRUNCATE, m_ProgramDir);
	_snprintf_s(szWindowsDir, _countof(szWindowsDir), _TRUNCATE, m_WindowsDir);

	if (m_SrcDir.GetAt(m_SrcDir.GetLength() - 1) != '\\')
		_snprintf_s(outfile, _countof(outfile), _TRUNCATE, "%s\\%s", m_SrcDir, VERSIONINFO_DAT);
	else
		_snprintf_s(outfile, _countof(outfile), _TRUNCATE, "%s%s", m_SrcDir, VERSIONINFO_DAT);

	//Clean the old file (dont do this if you allow editing)
	FileHelper::Delete(outfile);

	//Clean the patch directory
	CleanDirectory(m_ZipDest);

	if ((_access(m_ZipDest, 0) == -1))
		return CMakepatch::invalid_dest;

	if (!FileHelper::SetCurrentDirectory(m_SrcDir.GetString()))
		return CMakepatch::invalid_source;

	//Create the file and write the version info to the file
	WritePrivateProfileStringA("CURRENT", "VERSION", m_Version, outfile);

	//Set total files to 0 to start
	WritePrivateProfileStringA("CURRENT", "TOTALFILES", "0", outfile);

	if (!TraverseDirectory(m_SrcDir, &outfile[0], m_ZipUp, &totalSize, &totalfiles, oldpatchData, oldFileCount)) {
		return CMakepatch::cancel;
	}

	//Set total files
	_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%d", totalfiles);
	WritePrivateProfileStringA("CURRENT", "TOTALFILES", szTemp, outfile);

	//Set total size
	_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%d", totalSize);
	WritePrivateProfileStringA("CURRENT", "TOTALSIZE", szTemp, outfile);

	CRCLIST *curfiles = new CRCLIST[totalfiles];
	CDistributionTargetDlg::ReadCRCTable2(totalfiles, curfiles, outfile);

	DistributionProgressiveDlg distProg(AfxGetApp()->m_pMainWnd, curfiles, totalfiles);
	//
	// Zerby normally hits 'ok' here, but doesn't modify the default empty set of
	// distribution files.  So curfiles should be empty.  I just need to bypass
	// the distProg.DoModel and run the following code with the empty curfiles?
	// Not 100% sure, but try it and correct if necessary.
	//
	if (distProg.ProcessList(m_skipDialog) != IDCANCEL) {
		// Basically start over and do from modified curfiles.
		DeleteFileA(outfile);
		WritePrivateProfileStringA("CURRENT", "VERSION", m_Version, outfile);
		WritePrivateProfileStringA("CURRENT", "TOTALFILES", "0", outfile);
		createProgressiveInfo(curfiles, totalfiles, outfile);
		_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%d", totalfiles);
		WritePrivateProfileStringA("CURRENT", "TOTALFILES", szTemp, outfile);
		_snprintf_s(szTemp, _countof(szTemp), _TRUNCATE, "%d", totalSize);
		WritePrivateProfileStringA("CURRENT", "TOTALSIZE", szTemp, outfile);
	}

	//copy the versioninfo file to the dest
	char newoutfile[_MAX_PATH + 1];

	if (m_ZipUp) {
		if (m_ZipDest.GetAt(m_ZipDest.GetLength()) != '\\')
			_snprintf_s(newoutfile, _countof(newoutfile), _TRUNCATE, "%s\\%s", m_ZipDest, VERSIONINFO_DAT);
		else
			_snprintf_s(newoutfile, _countof(newoutfile), _TRUNCATE, "%s", m_ZipDest, VERSIONINFO_DAT);

		CopyFileA(outfile, newoutfile, FALSE);
	}

	delete curfiles;

	return CMakepatch::success;
}
