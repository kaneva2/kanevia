/******************************************************************************
 WaitDialog.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <afxcmn.h>
#include "afxcmn.h"

// CWaitDialog dialog

class CWaitDialog : public CDialog {
	DECLARE_DYNAMIC(CWaitDialog)

public:
	CWaitDialog(CWnd* pParent = NULL); // standard constructor
	virtual ~CWaitDialog();

	void ShowIt();
	void KillIt();
	void UpdateText(CStringA s);

	// Dialog Data
	enum { IDD = IDD_WAIT };

protected:
	UINT m_id;

	static UINT showIt(LPVOID pParam);

	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	CAnimateCtrl m_wndAnim;

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

private:
	HWND m_theHwnd;
	LPVOID m_theCaller;
};
