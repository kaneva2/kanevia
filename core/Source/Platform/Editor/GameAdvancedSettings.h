/******************************************************************************
 GameAdvancedSettings.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
// added by serialize.pl
#include <GetSet.h>
#include "afx.h"

class CShaderObjList;

class GameAdvancedSettings : public CObject GETSET_BASE {
public:
	GameAdvancedSettings(void);
	~GameAdvancedSettings(void);

	// names from ClientEngine
	int m_fps;
	int m_globalShadowType;
	bool m_W_BufferPreffered;
	bool m_EntityCollisionOn;
	bool m_SelectionModeOn;
	CShaderObjList *m_shaderSystem;
	int m_globalWorldShader; // selection in above
	int m_ResolutionHeight;
	int m_ResolutionWidth;

	DECLARE_GETSET
};
