///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <direct.h>
#include <stdlib.h>
#include <stdio.h>
#include "ClientStrings.h"
#include "EngineStrings.h"
#include <windows.h>
#include <tlhelp32.h>
#include <shlobj.h>
#include "config.h"

#include "NewGameLoginDlg.h"
#include "NewUserRegisterDlg.h"
#include "NewGameSelectDlg.h"
#include "NewGameTemplateSelDlg.h"
#include "NewGameDbDlg.h"
#include "NewGameProgressDlg.h"
#include "NewGameIntroDlg.h"
#ifndef DECOUPLE_DBCONFIG
#include "DBConfigValues.h"
#else
#include "Common/include/DBConfigExporter.h"
#endif
#include "RunGameDlg.h"

#include "KEPEdit.h"
#include "KEPEditDoc.h"
#include "KEPFilenames.h"
#include "KEPEditView.h"
#include "KEPException.h"
#include "NetworkCfg.h"

using namespace KEP;

#include "StatusDlg.h"
#include "GameDlg.h"
#include "MainFrm.h"
#include "KEPConfigurationsXML.h"
#include "Publish.h"
#include <DecryptingFileFactory.h>

#include "clientbladefactory.h"
#include "KEPFileNames.h"
#include "NewGameInfo.h"
#include ".\kepeditview.h"
#include "PublishUserPassDlg.h"
#include "DistributionTargetDlg.h"
#include "DistributionUpdateDlg.h"
#include "DistributionGameSelectionDlg.h"
#include "PublishUpdateDlg.h"
#include "OpenZoneDlg.h"
#include "NewZoneDlg.h"

#include "CurrentWorldsDlg.h"
#include "RuntimeEntityViewerDlg.h"

#include "CPluginTransfer.h"
#include "ScriptAttributes.h"
#include "IXmlSerializableObj.h"
#include "../../StarInstall/MySqlSetup/MySqlSetupImports.h"
#include "../../StarInstall/MySqlSetup/MySqlSetupException.h"

#include "Event/Base/ISendHandler.h"
#include "Event/Glue/KEPEditViewIds.h"
#include "common/include/StpUrl.h"
#include "PythonStateManager.h"

#include "dbExportHelper.h"

#include "ReMeshCreate.h" // RENDERENGINE INTERFACE
#include "SkeletonConfiguration.h"

#include "StartCfg.h"

#include "AssetsDatabase.h"
#include "AssetTree.h"
#include "UGC/UGCManager.h"

#include "TreeCtrlHelper.h"
using namespace TreeCtrlHelper;

#include "SkeletalAnimationManager.h"
#include "DynamicObjectManager.h"
#include "CBoneAnimationNaming.h"
#include "CBoneClass.h"
#include "CDeformableMesh.h"
#include "ServerSerialize.h"
#include "TriggerFile.h"
#include "legacymodelproxy.h"
#include "DynamicObjectLibrary.h"

#include "UnicodeMFCHelpers.h"
#include "UnicodeWindowsHelpers.h"

#include "HttpClient\GetBrowserPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define ADD_ACHIEVEMENT_URL "kgp/gameAddAchievement.aspx?name={0}&description={1}&points={2}&assetId={3}&gameId={4}&user={5}&pw={6}"
#define ADD_PREMIUMITEM_URL "kgp/gameAddPremiumItem.aspx?name={0}&description={1}&price={2}&assetId={3}&gameId={4}&user={5}&pw={6}"
#define SETUP_LEADERBOARD_URL "kgp/gameSetupLeaderboard.aspx?name={0}&label={1}&type={2}&sort={3}&gameId={4}&user={5}&pw={6}"

extern const char *LANG_DIR;
const char *CONFIG_NAME_EXT = ".kep";
const char *SHARED_TEMPLATE_DIR = "Shared";
const char *SQLSCRIPT_DIR = "SqlScripts";
const char *SAVED_SCRIPTS_DIR = "GameFiles\\ScriptServerScripts\\SavedScripts";
const char *WEB_IDS_SCRIPT = "WebIDs.lua";
const char *DEFAULT_NEW_GAME_URL = "http://developer.kaneva.com/games/gameLibrary.aspx";

// AIMPTEMP: AssetImporters harcoded here for now (for testing), later they'll become blades
//extern IAssetImporter* ogreImporter;
extern IAssetImporter *g_pArwImporter;
extern IAssetImporter *g_pArbImporter;
extern IAssetImporter *g_pColladaImporter;

inline string EXG_FILTER() {
	bool loaded;
	string ret = loadStr(IDS_EXG_FILTER, &loaded);
	jsAssert(loaded);
	return ret;
}
inline string KEP_FILTER() {
	bool loaded;
	string ret = loadStr(IDS_KEP_FILTER, &loaded);
	jsAssert(loaded);
	return ret;
}

HWND kaneva_ClientFindWindow() {
	HWND hwnd = FindWindowW(K_NAME_CLIENT_WINDOW, NULL);
	return (hwnd == NULL) ? (HWND)INVALID_HANDLE_VALUE : hwnd;
}

CKEPEditView::CKEPEditView() :
		ClientEngine("Editor"), m_creatingDistribution(false), m_updateGameFiles(true), m_makeDistributionAutomatically(false), m_dynamicObjectsFilePtr(0), m_altGlobalInventory(new CObList()), m_paramClientRunning(false), GL_showBoundingBoxs(FALSE), m_worldListTemp(NULL) {
	m_engineType = ENGINE_EDITOR;

	m_MRUgameFile.Empty();
	m_MRUgameFileIndex = -1;
	m_zoneFile.Empty();

	m_WaitDialog = NULL;
	// Modules
	m_currentWldsSpawnPointModule = NULL;
	m_accountViewerModule = NULL;
	m_entityViewerModule = NULL;
	m_accountModule = NULL;

	m_textureSearchPath = NULL;
	m_searchPathCount = 0;
#if (DRF_OLD_VENDORS == 1)
	m_serverItemGenerator = new CServerItemGenObjList();
#endif

	g_pArwImporter->Register();
	g_pArbImporter->Register();
	g_pColladaImporter->Register();
	m_ugcMgr = InitUGCManager(this);

	RegisterGameFiles();

	StartCfg::Init(this);

	m_activated = true; // set activated state to enable inputs
}

void CKEPEditView::RegisterGameFiles() {
	EditorState *editorStatePtr = EditorState::GetInstance();

	//register game files to monitor,
	//*NOTE THAT THE DB OBJECTS HAVE TO BE PUT THE RIGHT ORDER FOR THE SERIALIZATION TO WORK*
	//

	m_fontsTextFilePtr = new CKEPGameFile;
	CKEPGameFile::CKEPObListVector *fontsTextObjs = new CKEPGameFile::CKEPObListVector;
	fontsTextObjs->push_back((CKEPObList **)&m_pTCL);
	fontsTextObjs->push_back((CKEPObList **)&m_pFOL);
	fontsTextObjs->push_back((CKEPObList **)&m_cgFontList);
	fontsTextObjs->push_back((CKEPObList **)&m_symbolMapDB);
	m_fontsTextFilePtr->SetObjects(fontsTextObjs);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_fontsTextFilePtr);

	//ai file
	m_aiFilePtr = new CKEPGameFile;
	CKEPGameFile::CKEPObListVector *aiObjs = new CKEPGameFile::CKEPObListVector;
	aiObjs->push_back((CKEPObList **)&m_AIOL);
	aiObjs->push_back((CKEPObList **)&m_pAISOL);
	m_aiFilePtr->SetObjects(aiObjs);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_aiFilePtr);

	//actor file
	m_entityFilePtr = new CKEPGameFile((CKEPObList **)&m_movObjRefModelList);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_entityFilePtr);

	//camera/view
	m_cameraViewFilePtr = new CKEPGameFile;
	CKEPGameFile::CKEPObListVector *cameraViewObjs = new CKEPGameFile::CKEPObListVector;
	cameraViewObjs->push_back((CKEPObList **)&m_pCameraList);
	cameraViewObjs->push_back((CKEPObList **)&m_pViewportList);
	m_cameraViewFilePtr->SetObjects(cameraViewObjs);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_cameraViewFilePtr);

	//explosion
	m_explosionFilePtr = new CKEPGameFile((CKEPObList **)&m_explosionDBList);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_explosionFilePtr);

	//missile
	m_missileFilePtr = new CKEPGameFile((CKEPObList **)&m_missileDB);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_missileFilePtr);

	//arena manager
	m_arenaFilePtr = new CKEPGameFile((CKEPObList **)&m_battleSchedulerSys);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_arenaFilePtr);

	//element
	m_elementFilePtr = new CKEPGameFile((CKEPObList **)&m_elementDB);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_elementFilePtr);

	//icons
	m_iconsFilePtr = new CKEPGameFile((CKEPObList **)&m_iconDatabase);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_iconsFilePtr);

	//skills
	m_skillsFilePtr = new CKEPGameFile((CKEPObList **)&m_skillDatabase);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_skillsFilePtr);

	m_statsFilePtr = new CKEPGameFile((CKEPObList **)&m_statDatabase);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_statsFilePtr);

	//world rules
	m_worldRulesFilePtr = new CKEPGameFile((CKEPObList **)&m_worldChannelRuleDB);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_worldRulesFilePtr);

	//ai triggered spawn point
	m_aiTriggeredSpawnFilePtr = new CKEPGameFile((CKEPObList **)&m_aiRaidDatabase);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_aiTriggeredSpawnFilePtr);

	//npc config
	m_npcFilePtr = new CKEPGameFile((CKEPObList **)&m_serverItemGenerator);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_npcFilePtr);

	//bank
	m_bankFilePtr = new CKEPGameFile((CKEPObList **)&m_bankZoneList);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_bankFilePtr);

	//commerece area
	m_commereceAreaFilePtr = new CKEPGameFile((CKEPObList **)&m_commerceDB);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_commereceAreaFilePtr);

	//global inventory
	m_globalInventoryFilePtr = new KEPGameFileReference(m_globalInventoryDB->gameFileAdapter());

	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_globalInventoryFilePtr);

	//housing system
	m_housingSystemFilePtr = new CKEPGameFile((CKEPObList **)&m_housingDB);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_housingSystemFilePtr);

	//housing placement
	m_housingAreaFilePtr = new CKEPGameFile((CKEPObList **)&m_multiplayerObj->m_housingDB);
	//editorStatePtr->RegisterGameFile((IKEPGameFile**)&m_housingAreaFilePtr);

	//portal area
	m_portalAreaFilePtr = new CKEPGameFile((CKEPObList **)&m_portalDatabase);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_portalAreaFilePtr);

	//safe area
	m_safeAreaFilePtr = new CKEPGameFile((CKEPObList **)&m_safeZoneDB);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_safeAreaFilePtr);

	//sounds
	m_soundsFilePtr = new CKEPGameFile((CKEPObList **)&m_soundManager);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_soundsFilePtr);

	//water area
	m_waterAreaFilePtr = new CKEPGameFile((CKEPObList **)&m_waterZoneDB);
	editorStatePtr->RegisterGameFile((IKEPGameFile **)&m_waterAreaFilePtr);
}

CKEPEditView::~CKEPEditView() {
	EditorState::GetInstance()->UnRegisterAllGameFiles();

	delete m_fontsTextFilePtr;
	delete m_aiFilePtr;
	delete m_entityFilePtr;
	delete m_cameraViewFilePtr;
	delete m_explosionFilePtr;
	delete m_musicFilePtr;
	delete m_missileFilePtr;
	delete m_arenaFilePtr;
	delete m_elementFilePtr;
	delete m_iconsFilePtr;
	delete m_skillsFilePtr;
	delete m_statsFilePtr;
	delete m_worldRulesFilePtr;
	delete m_aiTriggeredSpawnFilePtr;
	delete m_npcFilePtr;
	delete m_bankFilePtr;
	delete m_commereceAreaFilePtr;
	delete m_globalInventoryFilePtr;
	delete m_housingSystemFilePtr;
	delete m_housingAreaFilePtr;
	delete m_portalAreaFilePtr;
	delete m_safeAreaFilePtr;
	delete m_soundsFilePtr;
	delete m_waterAreaFilePtr;

	StartCfg::Free();
	// 4/08 not needed PSM_Uninitialize();

	g_pArbImporter->Unregister();
	g_pArwImporter->Unregister();
	g_pColladaImporter->Unregister();

	freeLocaleStrings(); // Cleanup locale strings
	delete m_altGlobalInventory;
}

void CKEPEditView::FreeAll() {
	if (m_textureSearchPath) {
		delete[] m_textureSearchPath;
		m_textureSearchPath = 0;
	}

	// DIALOGS Instances
	if (m_currentWldsSpawnPointModule && ::IsWindow(m_currentWldsSpawnPointModule->m_hWnd)) {
		delete m_currentWldsSpawnPointModule;
		m_currentWldsSpawnPointModule = 0;
	}
	if (m_entityViewerModule) {
		delete m_entityViewerModule;
		m_entityViewerModule = 0;
	}
	if (m_accountViewerModule) {
		delete m_accountViewerModule;
		m_accountViewerModule = 0;
	}

	if (m_accountModule) {
		delete m_accountModule;
		m_accountModule = 0;
	}

	if (m_worldListTemp) {
		// dont delete contents its referenced
		m_worldListTemp->RemoveAll();
		delete m_worldListTemp;
		m_worldListTemp = NULL;
	}

	if (m_serverItemGenerator) {
		m_serverItemGenerator->SafeDelete();
		delete m_serverItemGenerator;
		m_serverItemGenerator = 0;
	}

	m_controlsDlgBar.DestroyWindow();
	m_moviesDlgBar.DestroyWindow();
	m_enviormentBar.DestroyWindow();
	m_musicDlgBar.DestroyWindow();
	m_multiPlayer.DestroyWindow();
	m_animatedTextureDBDlgBar.DestroyWindow();
	m_aiDBDlgBar.DestroyWindow();

	m_dlgBarObject.DestroyWindow();
	m_wndDlgBar.DestroyWindow();
	m_backgroundDlgBar.DestroyWindow();
	m_entityDlgBar.DestroyWindow();
	m_camVptDlgBar.DestroyWindow();
	m_mouseDlgBar.DestroyWindow();
	m_2DinterfaceBar.DestroyWindow();
	m_textFontDlgBar.DestroyWindow();
	m_explosionDlgBar.DestroyWindow();
	m_missileDlgBar.DestroyWindow();
	m_bulletHoleDlgBar.DestroyWindow();
	m_scriptDlgBar.DestroyWindow();
	m_textureDBDlgBar.DestroyWindow();
	m_hudDBDlgBar.DestroyWindow();
	m_particleDBDlgBar.DestroyWindow();
	m_spawnGenDBDlgBar.DestroyWindow();

	ClientEngine::FreeAll();
}

BOOL CKEPEditView::PreCreateWindow(CREATESTRUCT &cs) {
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CKEPEditView printing

BOOL CKEPEditView::OnPreparePrinting(CPrintInfo *pInfo) {
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CKEPEditView::OnBeginPrinting(CDC * /*pDC*/, CPrintInfo * /*pInfo*/) {
	// TODO: add extra initialization before printing
}

bool CKEPEditView::RenderLoop(cool canWait) {
	if (GetInitialized() && !m_bStopRendering) {
		bool ret = false;
		try {
			ret = ClientEngine::RenderLoop(canWait);
		} catch (...) {
			CStringW msg;
			msg.LoadString(IDS_FATAL_CATCH_ERROR);
			AfxMessageBox(msg, MB_ICONSTOP);
			exit(1);
		}

		return ret;
	} else {
		if (m_bStopRendering)
			fTime::SleepMs(500.0); // give some cycles to everyone else
		else
			fTime::SleepMs(10.0);
	}

	return true;
}

///---------------------------------------------------------
// CKEPEditView::OnEndPrinting
//
// [in] *pDC
// [in] *pInfo

void CKEPEditView::OnEndPrinting(CDC * /*pDC*/, CPrintInfo * /*pInfo*/) {
	// TODO: add cleanup after printing
}

// CKEPEditView diagnostics

#ifdef _DEBUG
void CKEPEditView::AssertValid() const {
	CView::AssertValid();
}

void CKEPEditView::Dump(CDumpContext &dc) const {
	CView::Dump(dc);
}

CKEPEditDoc *CKEPEditView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CKEPEditDoc)));
	return (CKEPEditDoc *)m_pDocument;
}
#endif //_DEBUG

void CKEPEditView::OnInitialUpdate() {
	CStringW titleW;
	titleW.LoadString(IDS_MAIN_TITLE);
	m_title = Utf16ToUtf8(titleW).c_str();

	g_ceApp.SetView(this);

	GetParent()->SetWindowTextW(titleW);

	m_editorModeEnabled = TRUE;
	m_editorTextModeOn = TRUE;

	m_editorBaseDir = PathApp();

	SetMouseKeyboardUpdate(TRUE);

	this->Create(m_title, IDR_MAINFRAME, IDR_MAINMENU);

	OnCreate(0);
}

void CKEPEditView::ReInit() {
	ClientEngine_CFrameWnd::OnDestroy();

	ClientEngine::ReInit();

	m_editorModeEnabled = TRUE;
	m_editorTextModeOn = TRUE;
	GL_showBoundingBoxs = FALSE;

	SetMouseKeyboardUpdate(TRUE);

	m_pCABD3DLIB->currentVideoMode = 3;

	if (m_worldListTemp) {
		// dont delete contents its referenced
		m_worldListTemp->RemoveAll();
		delete m_worldListTemp;
		m_worldListTemp = NULL;
	}

	m_worldListTemp = new CWldObjectList();

	OnCreate(0);
}

int CKEPEditView::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	CRect rect;

	GetClientRect(&rect);

	m_curModeRect = rect;

	ClientEngine::OnCreate(lpCreateStruct);

	{ // TextureSearchPath init
		char bufferX[_MAX_PATH];
		_getcwd(bufferX, _MAX_PATH);

		CStringA CurDirX = bufferX;

		CFileException excAuto2;
		CFile flAuto2;
		CArchive the_ArchiveAuto2(&flAuto2, CArchive::load);
		BOOL resAuto = flAuto2.Open(L"TextureSearchPaths.aut", CFile::modeRead, &excAuto2);
		if (resAuto) {
			CStringA tempString;
			const size_t MAX_SEARCH_PATHS = 50;
			m_textureSearchPath = new CStringA[MAX_SEARCH_PATHS];

			int trace = 0;
			while (trace < MAX_SEARCH_PATHS && ReadString(the_ArchiveAuto2, tempString)) {
				m_searchPathCount++;
				m_textureSearchPath[trace++] = CurDirX + tempString;
			}

			the_ArchiveAuto2.Close();
			flAuto2.Close();
		}
	}

	// TOOL BARS FOR EDITOR
#ifndef _ClientOutput
	if (m_editorModeEnabled == TRUE) { // if editing mode is on create interfaces
		UINT styleDock = CBRS_RIGHT;
		if (!m_wndDlgBar.Create(this, IDD_DIALOG_DLGBARUI, styleDock, IDD_DIALOG_DLGBARUI)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_wndDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		m_wndDlgBar.SendDlgItemMessage(IDC_PROGRESS_UI, PBM_SETBARCOLOR, 0, RGB(0, 255, 0));
		m_wndDlgBar.SendDlgItemMessage(IDC_PROGRESS_UI, PBM_SETBKCOLOR, 0, RGB(0, 0, 0));

		// object interface
		if (!m_dlgBarObject.Create(this, IDD_DIALOG_OBJECTADD, styleDock, IDD_DIALOG_OBJECTADD)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_dlgBarObject.EnableDocking(CBRS_ALIGN_ANY);
#endif
		m_dlgBarObject.SendDlgItemMessage(IDC_PROGRESS_OBJ, PBM_SETBARCOLOR, 0, RGB(0, 255, 0));
		m_dlgBarObject.SendDlgItemMessage(IDC_PROGRESS_OBJ, PBM_SETBKCOLOR, 0, RGB(0, 0, 0));

		// background interface
		if (!m_backgroundDlgBar.Create(this, IDD_DIALOG_BACKGROUND, styleDock, IDD_DIALOG_BACKGROUND)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_backgroundDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// controls interface
		if (!m_controlsDlgBar.Create(this, IDD_DIALOG_CONTROLERS, styleDock, IDD_DIALOG_CONTROLERS)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_controlsDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		UpdateControlList();

		// movies interface
		if (!m_moviesDlgBar.Create(this, IDD_DIALOG_MOVIES, styleDock, IDD_DIALOG_MOVIES)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_moviesDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// entity movement interface
		if (!m_entityDlgBar.Create(this, IDD_DIALOG_ENTITY, styleDock, IDD_DIALOG_ENTITY)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_entityDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// cameras and viwports interface
		if (!m_camVptDlgBar.Create(this, IDD_DIALOG_CAMERAVIEWPORTS, styleDock, IDD_DIALOG_CAMERAVIEWPORTS)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_camVptDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		m_camVptDlgBar.SetPercentTop(0);
		m_camVptDlgBar.SetPercentBottom(100);
		m_camVptDlgBar.SetPercentLeft(0);
		m_camVptDlgBar.SetPercentRight(100);
		m_camVptDlgBar.SetDlgItemTextW(IDC_EDIT_FOV, L"0.7");
		m_camVptDlgBar.SetDlgItemTextW(IDC_EDIT_CLIPPLANE, L"1000.0");
		m_camVptDlgBar.SetDlgItemTextW(IDC_COMBO_CAMERATYPE, L"Fixed");

		// mouse interface
		if (!m_mouseDlgBar.Create(this, IDD_DIALOG_MOUSESETTINGS, styleDock, IDD_DIALOG_MOUSESETTINGS)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_mouseDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		m_mouseDlgBar.SetDlgItemInt(IDC_EDIT_MOUSESRCRED, 0);
		m_mouseDlgBar.SetDlgItemInt(IDC_EDIT_MOUSESRCGREEN, 0);
		m_mouseDlgBar.SetDlgItemInt(IDC_EDIT_MOUSESRCBLUE, 0);
		m_mouseDlgBar.SetDlgItemInt(IDC_EDIT_MOUSESIZEX, 5);
		m_mouseDlgBar.SetDlgItemInt(IDC_EDIT_MOUSESIZEY, 5);
		m_mouseDlgBar.SetDlgItemInt(IDC_EDIT_MOUSEFRAMECOUNT, 1);
		m_mouseDlgBar.SetDlgItemTextW(IDC_EDIT_MOUSEFILE, L"Mouse Filename");

		// 2Dinterface
		if (!m_2DinterfaceBar.Create(this, IDD_DIALOG_2DINTERFACE, styleDock, IDD_DIALOG_2DINTERFACE)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_2DinterfaceBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_REDSRC, L"-1");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_GREENSRC, L"0");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_BLUESRC, L"0");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_ATTRIBUTE, L"0");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_TOP, L"0");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_BOTTOM, L"10");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_LEFT, L"0");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_RIGHT, L"10");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_TRANSLUCENCY, L".5");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2DCOLLECTIONNAME, L"Unique ID");
		m_2DinterfaceBar.SetDlgItemTextW(IDC_EDIT_2D_FADEINSPEED, L".02");

		// Enviorment
		if (!m_enviormentBar.Create(this, IDD_DIALOG_ENVIORMENT, styleDock, IDD_DIALOG_ENVIORMENT)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_enviormentBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		//missingres?       m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_STARTRANGE, L"10.0" );
		//missingres?       m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_ENDRANGE, L"1000.0" );
		//missingres?       m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_DENSITY, L"1.0" );
		// NOT USED DLG?     m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_FOGRED, L"0.0" );
		// NOT USED DLG?     m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_FOGGREEN, L"0.0" );
		// NOT USED DLG?     m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_FOGBLUE, L"0.0" );
		// NOT USED DLG?     m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_BCKRED, L"0.0" );
		// NOT USED DLG?     m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_BCKGREEN, L"0.0" );
		// NOT USED DLG?     m_enviormentBar.SetDlgItemTextW ( IDC_EDIT_ENV_BCKBLUE, L"0.0" );

		//missingres?       m_enviormentBar.SendDlgItemMessage ( IDC_CHECK_ENV_ENABLESUN, BM_SETCHECK, BST_CHECKED, 0 );

		// text and font dlg
		if (!m_textFontDlgBar.Create(this, IDD_DIALOG_TEXTINTERFACE, styleDock, IDD_DIALOG_TEXTINTERFACE)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_textFontDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		TextDisplayTreeROOT = AddTreeRoot(L"Text Display Database", IDC_TREE_TEXTDISPLAYS, &m_textFontDlgBar);

		//
		// m_textFontDlgBar.SendDlgItemMessage(IDC_CMBO_CGTEX_TARGETTEX,LB_ADDSTRING,0,(LPARAM)L"HELLO");
		//
		m_textFontDlgBar.SetDlgItemTextW(IDC_EDIT_TEXT_PERCX, L"0");
		m_textFontDlgBar.SetDlgItemTextW(IDC_EDIT_TEXT_PERCY, L"0");
		m_textFontDlgBar.SetDlgItemTextW(IDC_EDIT_TEXT_SIZEX, L"2");
		m_textFontDlgBar.SetDlgItemTextW(IDC_EDIT_TEXT_SIZEY, L"2");
		m_textFontDlgBar.SetDlgItemTextW(IDC_EDIT_TEXT_TIMELIMIT, L"-1");

		// explosion dlg
		if (!m_explosionDlgBar.Create(this, IDD_DIALOG_EXPLOSIONS, styleDock, IDD_DIALOG_EXPLOSIONS)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_explosionDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		// NOT USED DLG?     m_explosionDlgBar.SetDlgItemTextW ( IDC_EDIT_EXP_VISCOUNT, L"10" );
		// NOT USED DLG?     m_explosionDlgBar.SetDlgItemTextW ( IDC_EDIT_EXP_FADESPEED, L".05" );
		// NOT USED DLG?     m_explosionDlgBar.SetDlgItemTextW ( IDC_EDIT_EXP_FADESTART, L"0" );
		// NOT USED DLG?     m_explosionDlgBar.SetDlgItemTextW ( IDC_EDIT_EXP_RADIUSEXPLOSION, L"0.0" );

		// music dlg UI
		if (!m_musicDlgBar.Create(this, IDD_DIALOG_MUSIC, styleDock, IDD_DIALOG_MUSIC)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_musicDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// missile interface
		if (!m_missileDlgBar.Create(this, IDD_DIALOG_MISSILE_INTERFACE, styleDock, IDD_DIALOG_MISSILE_INTERFACE)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_missileDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif
		//missingres?       m_missileDlgBar.SetDlgItemTextW ( IDC_EDIT_MIS_MISSILENAME, L"Name" );
		//missingres?       m_missileDlgBar.SetDlgItemInt ( IDC_EDIT_MIS_LIFETIME, 2000, TRUE );
		//missingres?       m_missileDlgBar.SetDlgItemInt ( IDC_EDIT_MIS_DAMAGE, 10, TRUE );
		//missingres?       m_missileDlgBar.SetDlgItemInt ( IDC_EDIT_MIS_PARTICLEDBLINK, -1, TRUE );
		//missingres?       m_missileDlgBar.SetDlgItemInt ( IDC_EDIT_MIS_EXPLOSIONATTRIBUTE, 0, TRUE );
		//missingres?       m_missileDlgBar.SetDlgItemInt ( IDC_EDIT_MIS_ABILITYUSETYPE, -1, TRUE );
		//missingres?       m_missileDlgBar.SetDlgItemInt ( IDC_EDIT_MIS_SKILLUSETYPE, -1, TRUE );
		//missingres?       m_missileDlgBar.SetDlgItemTextW ( IDC_EDIT_MIS_COLLISIONRADIUS, CABLIB->FloatToStingCnv ( 5.0f ) );
		//missingres?       m_missileDlgBar.SetDlgItemTextW ( IDC_EDIT_MIS_HEATSEEKPOWER, CABLIB->FloatToStingCnv ( 0.0f ) );
		//missingres?       m_missileDlgBar.SendDlgItemMessage ( IDC_COMBO_MIS_CRITERIAFSKILL, CB_SETCURSEL, 0, 0 );

		// bullet hole interface
		if (!m_bulletHoleDlgBar.Create(this, IDD_DIALOG_BULLETHOLEINTERFACE, styleDock, IDD_DIALOG_BULLETHOLEINTERFACE)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_bulletHoleDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// Animated Texture Interface
		if (!m_animatedTextureDBDlgBar.Create(this, IDD_DIALOG_ANIMTEXTURES, styleDock, IDD_DIALOG_ANIMTEXTURES)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_animatedTextureDBDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// Script Interface
		if (!m_scriptDlgBar.Create(this, IDD_DIALOG_SCRIPTING, styleDock, IDD_DIALOG_SCRIPTING)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_scriptDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// texture database
		if (!m_textureDBDlgBar.Create(this, IDD_DIALOG_TEXTUREDATA, styleDock, IDD_DIALOG_TEXTUREDATA)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_textureDBDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// hud interface
		if (!m_hudDBDlgBar.Create(this, IDD_DIALOG_HUDINTERFACE, styleDock, IDD_DIALOG_HUDINTERFACE)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_hudDBDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		if (!m_particleDBDlgBar.Create(this, IDD_DIALOG_PARTICLESYSTEM, styleDock, IDD_DIALOG_PARTICLESYSTEM)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_particleDBDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		if (!m_spawnGenDBDlgBar.Create(this, IDD_DIALOG_SPAWNGENERATORUI, styleDock, IDD_DIALOG_SPAWNGENERATORUI)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_spawnGenDBDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		if (!m_aiDBDlgBar.Create(this, IDD_DIALOG_AI_BOTS, styleDock, IDD_DIALOG_AI_BOTS)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_aiDBDlgBar.EnableDocking(CBRS_ALIGN_ANY);
#endif

		// multiplayer
		if (!m_multiPlayer.Create(this, IDD_DIALOG_TCP_IP, styleDock, IDD_DIALOG_TCP_IP)) {
			TRACE0("Failed to create DlgBar\n");
			return -1;
		}

#ifndef USE_CView
		m_multiPlayer.EnableDocking(CBRS_ALIGN_ANY);
#endif
		// NOT USED DLG?     m_multiPlayer.SetDlgItemInt ( IDC_EDIT_TCP_CAMERAINDEX, 0, TRUE );

#ifndef USE_CView
		// editor prep
		if (m_editorModeEnabled == TRUE) { // editor open
			CloseAllToolDialogs();
			OpenToolBar(&m_wndDlgBar);
		} // end editor open
#endif
	} // end if editing mode is on create interfaces
#endif

#ifdef USE_CView
	ProgressUpdater::m_thisProgressUpdate = (ProgressUpdater *)this;
#endif

	// limited client stuffs
#ifdef _LimitedEngine
	CurDir = operator+(CurDir, "\\GameFiles\\");
	SetCurrentDirectory(CurDir);

	// end use gamefiles folder for organization
	CStringW fileName = L"testtemp.svr";
	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(fileName, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION(exc, m_logger)
		SetCurrentDirectory(MainDir);
		return 1;
	}

	if (m_multiplayerObj) {
		m_multiplayerObj->SafeDelete();
		delete m_multiplayerObj;
		m_multiplayerObj = 0;
	}

	BEGIN_SERIALIZE_TRY

	CArchive the_in_Archive(&fl, CArchive::load);
	the_in_Archive >> m_multiplayerObj;
	m_multiplayerObj->m_accountDatabase->ExpandFromOptimized(m_skillDatabase, m_globalInventoryDB);
	the_in_Archive.Close();
	fl.Close();

	/*$F if(EditorModeOn == TRUE){
	    GL_multiplayerObject->m_multiplayerDlgLink = &m_multiPlayer;
	    InListAccounts(GL_multiplayerObject->m_accountDatabase->GetCount()-1);
	    }*/
	CSpawnGenDlg dlg;
	SetCurrentDirectory(MainDir);
	dlg.m_serverItemGenRef = m_serverItemGenerator;
	dlg.m_globalInventoryList = m_globalInventoryDB;
	dlg.m_pGlobalArmedInvenList = m_globalArmedInvenList;
	dlg.m_worldsDB = m_multiplayerObj->m_currentWorldsUsed;
	dlg.m_edbRef = m_movObjRefModelList;
	dlg.m_focusEntity = NULL;
	dlg.m_focusEntityExists = FALSE;
	dlg.m_skillDatabaseRef = m_skillDatabase;
	dlg.m_aiListRef = m_AIOL;

	dlg.DoModal();
	m_serverItemGenerator = dlg.m_serverItemGenRef;
	PostMessage(WM_CLOSE);
	END_SERIALIZE_TRY(m_logger)
#endif

	return 0;
}

//
// ===============================================================================
//    updates controller stuff
// ===============================================================================
//
void CKEPEditView::UpdateControlList() {
	for (int loop = 0; loop < m_pCABD3DLIB->HowManyJoysticks; loop++) { // loop A
		if (m_pCABD3DLIB->deviceIsForceFeedback[loop] == TRUE) { // if it is a forcefeedback controller
			m_controlsDlgBar.SendDlgItemMessageW(IDC_LIST_CONTROLS, LB_INSERTSTRING, 0, (LPARAM)L"Force Controller");
		} else { // normal controller
			m_controlsDlgBar.SendDlgItemMessageW(IDC_LIST_CONTROLS, LB_INSERTSTRING, 0, (LPARAM)L"Normal Controller");
		}
	} // end loopA
}

void CKEPEditView::OnDraw(CDC * /*pDC*/) {
	return;
}

void CKEPEditView::OnDestroy() {
	m_shutdown = true;

	ClientEngine_CFrameWnd::OnDestroy();
	CView::OnDestroy();
}

BOOL CKEPEditView::OnEraseBkgnd(CDC *pDC) {
	return ClientEngine_CFrameWnd::OnEraseBkgnd(pDC);
}

void CKEPEditView::OnAppRender() {
	ToggleLockGameWindow();
}

bool CKEPEditView::updateBinaries(CWnd *parent, const char *gameDir, const char *binDir) {
	bool ret = false;

	bool isDir;

	char CurDir[_MAX_PATH];
	_getcwd(CurDir, _MAX_PATH);

	const char *SCRIPTNAME = "CheckTemplate.vbs";
	if (jsFileExists(PathAdd(binDir, SCRIPTNAME).c_str(), &isDir) && !isDir) {
		CStringA parms;

#ifdef _DEBUG
		const char *isDebug = "debug";
#else
		const char *isDebug = "";
#endif
		if (m_makeDistributionAutomatically)
			parms.Format("%s \"%s\" \"%s\" \"%s\" \"%s\" %s", SCRIPTNAME, m_game.GameId(), gameDir, binDir, isDebug, m_updateGameFiles ? "copy" : "");
		else
			parms.Format("%s \"%s\" \"%s\" \"%s\" \"%s\"", SCRIPTNAME, m_game.GameId(), gameDir, binDir, isDebug);
		CStringW parmsW = Utf8ToUtf16(parms).c_str();
		std::wstring binDirW = Utf8ToUtf16(binDir);

		parent->EnableWindow(FALSE);
		SHELLEXECUTEINFOW shi;
		memset(&shi, 0, sizeof(shi));
		shi.cbSize = sizeof(shi);
		shi.fMask = SEE_MASK_NOCLOSEPROCESS;
		shi.hwnd = parent->m_hWnd;
		shi.lpVerb = L"open";
		shi.lpFile = L"wscript.exe";
		shi.lpParameters = parmsW;
		shi.nShow = SW_SHOW;
		shi.lpDirectory = binDirW.c_str();
		if (ShellExecuteEx(&shi) && (int)shi.hInstApp > 32) {
			UINT id = parent->SetTimer(1, 500, 0); // to break out of msg pump if nothing else does
			while (WaitForSingleObject(shi.hProcess, 1) != WAIT_OBJECT_0) {
				AfxGetApp()->PumpMessage();
			}
			DWORD exitCode = -1;
			GetExitCodeProcess(shi.hProcess, &exitCode);
			ret = exitCode == 0;

			parent->KillTimer(id);
			CloseHandle(shi.hProcess);
		} else {
			CStringW s;
			wstring ss;
			s.FormatMessageW(IDS_ERR_LAUNCH_CHECK, errorNumToString(::GetLastError(), ss));
			AfxMessageBox(s, MB_ICONEXCLAMATION, 0);
		}
		parent->EnableWindow(TRUE);
	} else {
		AfxMessageBox(IDS_TEMPLATE_UPDATE_SCRIPT_MISSING, MB_ICONEXCLAMATION, 0);
	}

	_chdir(CurDir);
	return ret;
}

void CKEPEditView::OnFileOpen() {
	int index = -1;
	bool isDir = false;

	CStringW gameFileW;

	// If called by the MRU menu, get the game set by the app
	CStringA MRUgameFile = GetMRUFileToOpen(&index);

	if (MRUgameFile.IsEmpty() ||
		(!(jsFileExists(MRUgameFile, &isDir) && !isDir))) {
		if ((!(jsFileExists(MRUgameFile, &isDir) && !isDir)) &&
			(index != -1)) {
			g_ceApp.MRURemoveFile(index);
		}

		CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT, Utf8ToUtf16(KEP_FILTER()).c_str(), this);
		if (opendialog.DoModal() == IDOK) {
			gameFileW = opendialog.GetPathName();
		} else {
			gameFileW.Empty();
		}
	} else {
		gameFileW = Utf8ToUtf16(MRUgameFile).c_str();
		MRUgameFile.Empty();
		SetMRUFileToOpen(MRUgameFile, -1);
	}

	if (!gameFileW.IsEmpty()) {
		wchar_t orig_path_buffer[_MAX_PATH];
		wchar_t path_buffer[_MAX_PATH];
		wchar_t drive[_MAX_DRIVE];
		wchar_t dir[_MAX_DIR];

		// Restore if game load fails
		GetCurrentDirectoryW(_MAX_PATH, orig_path_buffer);

		_wsplitpath_s(gameFileW, drive, _countof(drive), dir, _countof(dir), 0, 0, 0, 0);
		_wmakepath_s(path_buffer, _countof(path_buffer), drive, dir, 0, 0);
		SetCurrentDirectoryW(path_buffer);
		SetPathBase(Utf16ToUtf8(path_buffer), false);

		// set the script dir
		m_scriptDir = PathAdd(PathGameFiles(), "ClientScripts");

		try {
			// see if the binaries need to be updated
			CStringA bootStrapDir;
			bootStrapDir = PathApp().c_str();

			// load the game 8/09 moved ahead of updateBinaries since we need to know gameId
			if (!m_game.Load(PathBase().c_str(), Utf16ToUtf8(gameFileW.GetString()).c_str())) {
				if (index != -1)
					g_ceApp.MRURemoveFile(index);

				SetCurrentDirectory(orig_path_buffer);

				AfxMessageBox(IDS_GAME_LOAD_FAILED, MB_ICONEXCLAMATION);
				return;
			}
			if (!m_game.IsSubGame()) {
				// Load the full menu.
				CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
				pMainFrame->SetNewMenu(IDR_MAINMENU);
				pMainFrame->InitControlBars();
			}

			if (!updateBinaries(AfxGetMainWnd(), PathBase().c_str(), bootStrapDir)) {
				AfxMessageBox(IDS_LOAD_STOPPED, MB_ICONEXCLAMATION, 0);
				return;
			}
			SetFocus();
			SetActiveWindow();

			string fname;
			if (!initializeLocaleStrings(PathAdd(PathApp(), LANG_DIR).c_str(), fname)) {
				AfxMessageBox(IDS_LOAD_LOCALE_FAILED);
			}

			//	Set resource paths
			IResource::SetBaseTexturePath(eTexturePath::MapsModels, PathMapsModels());
			IResource::SetBaseTexturePath(eTexturePath::CustomTexture, PathCustomTexture());
			IResource::SetBaseAnimPath(eAnimPath::CharacterAnimations, PathCharacterAnimations());
			IResource::SetBaseMeshPath(eMeshPath::CharacterMeshes, PathCharacterMeshes());

			try {
				ProgressUpdate("Game File Open START", TRUE, FALSE);

				CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
				if (pMainFrame->IsGameOpen()) {
					ReInit();
					CStringA u, p;
					if (!m_game.GetGameUserPass(u, p))
						AfxMessageBox(IDS_DECRYPTION_FAILED, MB_ICONEXCLAMATION);
					SetGamePubUserPass(u, p);
					m_loadSceneExgFileName.Empty();
				}

				// The autoexec.xml may load a scene and thus update
				// the title to indicate the zone is loaded. Place the
				// game open update before this load so it is not reset.
				((CMainFrame *)AfxGetMainWnd())->SetGameStatus(TRUE, FALSE);
				SetTitle();

				ReadStartCfgFile();

				CStringA baseDir(PathBase().c_str());

				loadAutoExec(baseDir);

				if (loadEditorSVR(baseDir, m_game.SVR())) {
					loadSVRParts(baseDir);
				}

				SetPathConfigs(PathAdd(baseDir.GetString(), "GAMEFILES"));

				ProgressUpdate("Game File Open STOP", FALSE, TRUE);

				// switch to the general tab

				KEPTREEDATA td;
				td.dialogId = CGameDlg::IDD;
				AfxGetMainWnd()->SendMessage(KEP_TREECHANGE, 0, (LPARAM)&td);

				InvalidateRect(NULL, TRUE);

				// unload and reload the client blades
				ClientBladeFactory::Instance()->UnloadBlades();

				ClientBladeFactory::Instance()->FindBlades(baseDir.GetString());
				size_t blades = ClientBladeFactory::Instance()->GetNumBlades();
				for (size_t i = 0; i < blades; i++)
					ClientBladeFactory::Instance()->GetBlade(i)->SetEngineInterface(this);

				ClientBladeFactory::Instance()->InitClientBlades();

				// load the menu list
				m_menuList->Load(0, false, true);

				g_ceApp.AddToRecentFileList(gameFileW);

				//put a pointer in MainFrame
				pMainFrame->SetGame(&m_game);

				//monitor m_housingDB after it's loaded
				//EditorState::GetInstance()->RegisterList((CKEPObList **)&GL_multiplayerObject->m_housingDB);
			} catch (KEPException *e) // maybe we need to catch other exceptions?
			{
				AfxMessageBox(Utf8ToUtf16(e->ToString()).c_str(), MB_ICONEXCLAMATION);
				e->Delete();

				ReInit();
				SetGamePubUserPass(NULL, NULL);
				m_loadSceneExgFileName.Empty();
				((CMainFrame *)AfxGetMainWnd())->SetGameStatus(FALSE, FALSE);
				SetTitle();
			}

		} catch (KEPException *e) {
			AfxMessageBox(Utf8ToUtf16(e->ToString()).c_str(), MB_ICONEXCLAMATION);
			e->Delete();
		}
	}
}

void CKEPEditView::OnEditorZoneSave() {
	// just like OnButtonSave, but w/o the dialog
	int msgReturn;
	CStringW msgBuildCollisionStr;
	CStringW msgCollisionDataStr;
	msgBuildCollisionStr.LoadString(ID_STRING_BUILDCOLLISION);
	msgCollisionDataStr.LoadString(ID_STRING_COLLISIONDATA);
	msgReturn = MessageBox(msgBuildCollisionStr, msgCollisionDataStr, MB_YESNO);
	if (IDYES == msgReturn) {
		OnBTNObjectBuildCollisionMap();
	} // if..IDYES == msgReturn

	if (SaveSceneObjects(m_loadSceneExgFileName)) {
		EditorState::GetInstance()->SetZoneDirty(false);
	}
}

void CKEPEditView::OnEditorZoneSaveAs() {
	EditorZoneSaveAs(TRUE);
}

BOOL CKEPEditView::EditorZoneSaveAs(BOOL saveAs) {
	BOOL ret = FALSE;

	wchar_t buffer[_MAX_PATH];
	_wgetcwd(buffer, _MAX_PATH);
	CStringW MainDir = buffer;
	std::string CurDirUtf8 = PathAdd(Utf16ToUtf8(MainDir.GetString()), "GameFiles");
	CStringW CurDirW = Utf8ToUtf16(CurDirUtf8).c_str();
	SetCurrentDirectoryW(CurDirW);

	int msgReturn;
	CStringW msgBuildCollisionStr;
	CStringW msgCollisionDataStr;

	msgBuildCollisionStr.LoadString(ID_STRING_BUILDCOLLISION);
	msgCollisionDataStr.LoadString(ID_STRING_COLLISIONDATA);
	msgReturn = MessageBoxW(msgBuildCollisionStr, msgCollisionDataStr, MB_YESNO);
	if (IDYES == msgReturn) {
		OnBTNObjectBuildCollisionMap();
	} // if..IDYES == msgReturn

	NewZoneDlg dlg(CurDirUtf8.c_str(), saveAs != FALSE, this);
	if (dlg.DoModal() == IDOK) {
		CStringA fileName = dlg.ZoneName();
		if (SaveSceneObjects(fileName)) {
			EditorState::GetInstance()->SetZoneDirty(false);
		} // if..SaveSceneObjects

		SetZoneFileName(fileName.GetString());
		m_loadSceneExgFileName = fileName;
		m_game.SetLoadedZone(CurDirUtf8.c_str() + fileName);
		SetTitle();
		ret = TRUE;
	}

	SetCurrentDirectoryW(MainDir);
	return ret;
}

void CKEPEditView::OnEditorZoneNew() {
	CStringA baseDir(PathBase().c_str());

	//remove previous registered pointers from EditorState, because they may get changed
	//in the new zone
	//UnRegisterSceneObjects();

	if (EditorZoneSaveAs(FALSE)) {
		if (((CMainFrame *)AfxGetMainWnd())->IsZoneOpen()) {
			// Save these before reset
			CStringA origCurrentWorkingFile = GetZoneFileName().c_str();
			CStringA origNewWorld = m_loadSceneExgFileName;

			ProgressUpdate("START", TRUE, FALSE);

			ReInit();

			ReadStartCfgFile();

			loadAutoExec(baseDir);

			if (loadEditorSVR(baseDir, m_game.SVR())) {
				loadSVRParts(baseDir);
			}

			SetPathConfigs(PathAdd(baseDir.GetString(), "GAMEFILES"));

			((CMainFrame *)AfxGetMainWnd())->SetGameStatus(TRUE, TRUE);

			SetZoneFileName(origCurrentWorkingFile.GetString());
			m_loadSceneExgFileName = origNewWorld;

			ProgressUpdate("STOP", FALSE, TRUE);

			KEPTREEDATA td;
			td.dialogId = CGameDlg::IDD;
			AfxGetMainWnd()->SendMessage(KEP_TREECHANGE, 0, (LPARAM)&td);

			InvalidateRect(NULL, TRUE);

			// DWD: unload and reload the client blades
			ClientBladeFactory::Instance()->UnloadBlades();

			ClientBladeFactory::Instance()->FindBlades(baseDir.GetString());
			size_t blades = ClientBladeFactory::Instance()->GetNumBlades();
			for (size_t i = 0; i < blades; i++)
				ClientBladeFactory::Instance()->GetBlade(i)->SetEngineInterface(this);

			ClientBladeFactory::Instance()->InitClientBlades();
		} else {
			SetPathConfigs(PathAdd(baseDir.GetString(), "GAMEFILES"));
			((CMainFrame *)AfxGetMainWnd())->SetGameStatus(TRUE, TRUE);
			InvalidateRect(NULL, TRUE);
		}
	}
}

int CALLBACK BrowseCallbackProc(HWND hwnd,
	UINT uMsg,
	LPARAM lParam,
	LPARAM lpData) {
	if (uMsg == BFFM_INITIALIZED) {
		SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData); // lpData = folder name
	}
	return 0;
}

// Added
void CKEPEditView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) {
	switch (nChar) {
		case VK_F5:
			// Run from local project directory
			if (!m_game.PathBase().IsEmpty()) {
				OnGameRun();
			}
			break;
		case VK_F6:
			// Run Last Distribution
			if (!m_game.PathBase().IsEmpty()) {
				OnGameRunDistribution();
			}
			break;
		case VK_F7:
			// Browse for a Distribution to run
			OnGameRunSpecificDistribution();
			break;
	};
}

// Move this traverser to CGlobalInventoryClass.
//Then either configure some kind of "mimic" function where if a CObList is present it is populated as well when ever the list is serialized.
//or...
class GILCopyAgent : public CGlobalInventoryObjList::Traverser {
public:
	GILCopyAgent(CObList &target) :
			_target(target) {}
	virtual ~GILCopyAgent() {}

	bool evaluate(CGlobalInventoryObj &invObj) {
		_target.AddTail(&invObj);
		return false;
	}
	CObList &_target;
};

void CKEPEditView::OnFileNew() {
	bool bOkToCreateNewStar = true;
	//CStringW newGameUrlW( AfxGetApp()->GetProfileStringW(L"NewGame", L"NewGameUrl", DEFAULT_NEW_GAME_URL ) );

	if (m_game.PathBase().GetLength() > 0) {
		if (AfxMessageBox(L"Would you like to save your changes?", MB_YESNO) == IDYES) {
			this->GetGame()->Save();
		}

		if (AfxMessageBox(L"Your current STAR will be closed.  Would you like to continue?", MB_YESNO) != IDYES) {
			bOkToCreateNewStar = false;
		}
	}

	if (bOkToCreateNewStar) // && infoDialog.Show()  )
	{
		// to simulate modal, all disabled except status to which the
		// file copying is attached.
		((CMainFrame *)AfxGetMainWnd())->m_wndKEPDialogBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndInformation.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndTreeBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndToolBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndGameToolBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->EnableWindow(FALSE);
		this->EnableWindow(FALSE);

		// should be at <appPath>\Templates
		CStringA templateBaseDir = PathAdd(PathApp(), "Templates").c_str();

		int startPage = 0;

		bool defaultDbSettingsOk = false;

		bool skipExtraEditorDlgs = ::GA("Login%5FCreate%5FAccount", m_editorBaseDir.c_str(), true);

		// use shared folder for load game (network.cfg)
		CNewGameLoginDlg loginDlg(m_gamePubUser, m_gamePubPass, PathAdd(templateBaseDir, SHARED_TEMPLATE_DIR).c_str(), m_editorBaseDir.c_str(), skipExtraEditorDlgs, m_logger, m_paramClientRunning);
		CNewUserRegisterDlg registerDlg(loginDlg, PathAdd(templateBaseDir, SHARED_TEMPLATE_DIR).c_str(), m_editorBaseDir.c_str(), skipExtraEditorDlgs, m_logger);
		CNewGameSelectDlg gameSelDlg(loginDlg, PathAdd(templateBaseDir, SHARED_TEMPLATE_DIR).c_str(), m_editorBaseDir.c_str(), skipExtraEditorDlgs, defaultDbSettingsOk, m_logger);
		CNewGameTemplateSelDlg templateSelDlg(gameSelDlg.m_selectedGame, m_editorBaseDir.c_str(), m_logger);
		CNewGameDbDlg dbConfigDlg(gameSelDlg.m_selectedGame, m_editorBaseDir.c_str(), skipExtraEditorDlgs, m_logger);

		if (skipExtraEditorDlgs) {
			if (!m_paramClientRunning)
				registerDlg.m_pageLoc = CKEPPropertyPage::firstPage;
			else
				loginDlg.m_pageLoc = CKEPPropertyPage::firstPage;

			defaultDbSettingsOk = dbConfigDlg.TestDbConnection();
		} else {
			loginDlg.m_pageLoc = CKEPPropertyPage::firstPage;
		}

		if (defaultDbSettingsOk && skipExtraEditorDlgs) {
			gameSelDlg.m_pageLoc = CKEPPropertyPage::lastPage;
			if (!dbConfigDlg.CheckDB()) {
				return;
			}

			// If not showing DB dialog, record GA
			::GA("Setup%5F3DApp%5FDB", m_editorBaseDir.c_str());
		} else {
			dbConfigDlg.m_pageLoc = CKEPPropertyPage::lastPage;
		}

		if (!templateSelDlg.LoadTemplates(templateBaseDir)) {
			AfxMessageBox(IDS_NO_TEMPLATES, MB_ICONEXCLAMATION);
			return; // must have at least one
		}

		CPropertySheet dlg(L"", this, startPage);

		// Show the register dialog first for new users with an option of getting to the login screen.
		if (skipExtraEditorDlgs) {
			if (!m_paramClientRunning)
				dlg.AddPage(&registerDlg);
			dlg.AddPage(&loginDlg);
		} else {
			dlg.AddPage(&loginDlg);
			if (!m_paramClientRunning)
				dlg.AddPage(&registerDlg);
		}

		dlg.AddPage(&gameSelDlg);
		if (!skipExtraEditorDlgs) {
			dlg.AddPage(&templateSelDlg);
		} else {
			// If not showing Template dialog, record GA
			::GA("Choose%5FTemplate", m_editorBaseDir.c_str());
		}

		if (!defaultDbSettingsOk) {
			dlg.AddPage(&dbConfigDlg);
		}

		dlg.SetWizardMode();

		if (!m_paramUsername.IsEmpty() && !m_paramPassword.IsEmpty()) {
			// All parameters required for no dialogs to appear have been specified
			loginDlg.SetEmail(m_paramUsername);
			loginDlg.SetPassword(m_paramPassword);
			if (loginDlg.LoadGames()) {
				// loginDlg
				AfxGetApp()->WriteProfileStringW(L"NewGame", L"LastEmail", Utf8ToUtf16(m_paramUsername).c_str());
				if (!m_paramClientRunning) {
					dlg.SetActivePage(dlg.GetActiveIndex() + 2); // skip login email/pass screen
				} else {
					dlg.SetActivePage(dlg.GetActiveIndex() + 1); // skip login email/pass screen
				}
			} else {
				LogError("Login Dialog failed to load all games");
			}
		}

		int dlgRet = 0;
		if (!m_paramAppName.IsEmpty() && !m_paramTemplateName.IsEmpty() && !m_paramInstallDirectory.IsEmpty()) {
			// gameSelDlg
			string consumerKey;
			string consumerSecret;

			CStringA gameName = m_paramAppName;
			CStringA gameDescription = "";

			CStringA error;

			if (gameSelDlg.CreateNewGame(loginDlg.m_email, loginDlg.m_password, gameName, gameDescription, false, consumerKey, consumerSecret, error)) {
				// reload login dialog's game list
				if (loginDlg.LoadGames()) {
					if (loginDlg.m_gameData.GetCount() != 0) {
						gameSelDlg.m_selectedGame = loginDlg.GetGameDataByGameName(gameName);
						if (gameSelDlg.m_selectedGame) {
							int ret = _mkdir(m_paramInstallDirectory);
							if (ret == 0 || errno == EEXIST) {
								gameSelDlg.m_destDir = m_paramInstallDirectory;
								if (templateSelDlg.SelectTemplateByName(m_paramTemplateName)) {
									// template, game and login all setup; ready to copy files
									dlgRet = ID_WIZFINISH;
								} else {
									LogError("Template Select Dialog contained no template named " << m_paramTemplateName);
								}
							} else {
								LogError("Failed to create the install directory");
							}
						}
					} else {
						LogError("Login Dialog contained no game data after create and reload");
					}
				} else {
					LogError("Login Dialog failed to reload available games from the web");
				}
			} else {
				LogError("Game Select Dialog failed to create a new game");
			}
		} else {
			dlgRet = dlg.DoModal();
		}

		if ((dlgRet == ID_WIZFINISH || dlgRet == ID_WIZNEXT || dlgRet == 0) && gameSelDlg.m_selectedGame && templateSelDlg.m_selectedTemplate) {
			Publish::instance()->UnlockPSM();
			Publish::instance()->DeinitPSM();

			CNewGameProgressDlg progress;
			progress.Create(AfxGetMainWnd());

			// wipe all the files
			{
				::GA("Wipe%5F3DApp%5FFolder", m_editorBaseDir.c_str());

				SHFILEOPSTRUCT fo;

				memset(&fo, 0, sizeof(fo));
				fo.hwnd = m_hWnd;
				fo.wFunc = FO_DELETE;
				fo.fFlags = FOF_NOCONFIRMATION;

				std::wstring from = Utf8ToUtf16(PathAdd(gameSelDlg.m_destDir, "*.*"));
				from += L'\0'; // Must be doubly null terminated
				fo.pFrom = from.c_str();
				fo.fFlags = fo.fFlags | FOF_SILENT;

				if (::SHFileOperation(&fo) != 0) {
					AfxMessageBox(IDS_NEW_GAME_CLEAN_DEST_FAILED, MB_ICONEXCLAMATION);
					return;
				}
			}

			progress.SetNextToRunning();

			// create a gameFile name
			CStringA gameFile(PathAdd(gameSelDlg.m_destDir, gameSelDlg.m_selectedGame->GetName() + CONFIG_NAME_EXT).c_str());

			// copy the files
			{
				SHFILEOPSTRUCT FileOp;
				ZeroMemory(&FileOp, sizeof(FileOp));

				// Attach to status window, disable all others to avoid user
				// actions. Simulates modal
				FileOp.hwnd = ((CMainFrame *)AfxGetMainWnd())->m_wndKEPStatusBar.m_hWnd;
				FileOp.wFunc = FO_COPY;
				std::wstring tempTo = Utf8ToUtf16(gameSelDlg.m_destDir);
				tempTo += L'\0'; // must be double null terminated
				FileOp.pTo = tempTo.c_str();
				FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR;

				// first copy shared template
				std::wstring fromStr = Utf8ToUtf16(templateBaseDir + "\\" + SHARED_TEMPLATE_DIR + "\\*.*");
				fromStr += L'\0'; // must be double null terminated
				FileOp.pFrom = fromStr.c_str();

				InvalidateRect(NULL, true);

				FileOp.fFlags = FileOp.fFlags | FOF_SILENT;

				::GA("Copy%5F3DApp%5FFiles", m_editorBaseDir.c_str());

				int ret = SHFileOperation(&FileOp);

				if (ret == NO_ERROR && !FileOp.fAnyOperationsAborted) {
					progress.SetNextToRunning();

					// copy the template they picked
					CStringA templateDir(templateSelDlg.m_selectedTemplate->m_folder.c_str());

					wstring fromStr = Utf8ToUtf16(templateDir + "\\*.*");
					fromStr += L'\0'; // Must be doubly null terminated
					FileOp.pFrom = fromStr.c_str();

					// Copy the bootstrap directory to the new location to get them started

					if (templateDir == gameSelDlg.m_destDir) {
						CStringW msg;
						msg.Format(IDS_FILESAVEAS_BASETARGET_ERROR);
						AfxMessageBox(msg, MB_ICONEXCLAMATION);
						return;
					}
					InvalidateRect(NULL, true);

					FileOp.fFlags = FileOp.fFlags | FOF_SILENT;

					::GA("Copy%5F3DApp%5FTemplates", m_editorBaseDir.c_str());

					int ret = SHFileOperation(&FileOp);

					if (ret == NO_ERROR && !FileOp.fAnyOperationsAborted) {
						progress.SetNextToRunning();

						SetCurrentDirectoryW(Utf8ToUtf16(gameSelDlg.m_destDir).c_str());

						STARTUPINFO si;
						memset(&si, 0, sizeof(si));
						si.cb = sizeof(si);
						PROCESS_INFORMATION pi;
						memset(&pi, 0, sizeof(pi));
						char cmdLine[] = "attrib.exe -r *.* /s";
						if (KEP::CreateProcess(
								NULL,
								static_cast<LPCSTR>(cmdLine),
								NULL,
								NULL,
								FALSE,
								CREATE_NO_WINDOW,
								NULL,
								NULL,
								&si,
								&pi)) {
							::CloseHandle(pi.hThread);
							while (WaitForSingleObject(pi.hProcess, 10) == WAIT_TIMEOUT) {
								MSG msg;
								if (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
									AfxGetApp()->PumpMessage();
							}
							::CloseHandle(pi.hProcess);
						}
						progress.SetNextToRunning();

						std::map<std::string, int> achievements;
						std::map<std::string, int> premiumItems;

						string gameId(gameSelDlg.m_selectedGame->GetId());
						string email(loginDlg.m_email);
						string pw(loginDlg.m_password);
						string urlPrefix;

						try {
							KEPConfig cfg;

							string path(PathAdd(PathAdd(templateBaseDir, SHARED_TEMPLATE_DIR).c_str(), NETCFG_CFG));
							cfg.Load(path.c_str(), NETCFG_ROOT);
							urlPrefix = cfg.Get(SECUREWOKURL_TAG, DEFAULT_SECURE_WOK_URL);
						} catch (KEPException *e) {
							string msg = loadStr(IDS_NETWORK_CFG_FAILED);
							LOG4CPLUS_WARN(m_logger, msg << ": " << e->m_msg);
							e->Delete();
						}

						if (urlPrefix != "") {
							// create achievements, premium items and setup leaderboard
							if (templateSelDlg.m_selectedTemplate->m_achievements.size() > 0) {
								::GA("Create%5F3DApp%5FAchievements", m_editorBaseDir.c_str());

								for (auto i = templateSelDlg.m_selectedTemplate->m_achievements.begin(); i != templateSelDlg.m_selectedTemplate->m_achievements.end(); i++) {
									string achievementUrl(urlPrefix);
									PathAddInPlace(achievementUrl, ADD_ACHIEVEMENT_URL);
									STLStringSubstitute(achievementUrl, "{0}", (*i)->m_name);
									STLStringSubstitute(achievementUrl, "{1}", (*i)->m_description);
									STLStringSubstitute(achievementUrl, "{2}", (*i)->m_points);
									STLStringSubstitute(achievementUrl, "{3}", (*i)->m_assetId);
									STLStringSubstitute(achievementUrl, "{4}", gameId);
									STLStringSubstitute(achievementUrl, "{5}", email);
									STLStringSubstitute(achievementUrl, "{6}", pw);

									DWORD httpStatusCode;
									string httpStatusDescription;
									string resultText;
									size_t resultSize = 0;
									if (::GetBrowserPage(achievementUrl, resultText, resultSize, httpStatusCode, httpStatusDescription)) {
										KEPConfig cfg;
										if (!cfg.Parse(resultText.c_str(), "Result")) {
											LOG4CPLUS_ERROR(m_logger, "Call to achievement url failed ");
										} else if (cfg.Get("ReturnCode", 1) != 0) {
											LOG4CPLUS_ERROR(m_logger, "Call to achievement url got bad result " << resultText);
										} else {
											// add achievement to the list
											int achievementId = cfg.Get("AchievementId", -1);
											if (achievementId != -1) {
												achievements.insert(std::map<std::string, int>::value_type((*i)->m_name, achievementId));
											}
										}
									}
								}
							}

							if (templateSelDlg.m_selectedTemplate->m_premiumItems.size() > 0) {
								::GA("Create%5F3DApp%5FPremiumItems", m_editorBaseDir.c_str());

								for (auto i = templateSelDlg.m_selectedTemplate->m_premiumItems.begin(); i != templateSelDlg.m_selectedTemplate->m_premiumItems.end(); i++) {
									string itemUrl(urlPrefix);
									PathAddInPlace(itemUrl, ADD_PREMIUMITEM_URL);
									STLStringSubstitute(itemUrl, "{0}", (*i)->m_name);
									STLStringSubstitute(itemUrl, "{1}", (*i)->m_description);
									STLStringSubstitute(itemUrl, "{2}", (*i)->m_price);
									STLStringSubstitute(itemUrl, "{3}", (*i)->m_assetId);
									STLStringSubstitute(itemUrl, "{4}", gameId);
									STLStringSubstitute(itemUrl, "{5}", email);
									STLStringSubstitute(itemUrl, "{6}", pw);

									DWORD httpStatusCode;
									string httpStatusDescription;
									string resultText;
									size_t resultSize = 0;
									if (::GetBrowserPage(itemUrl, resultText, resultSize, httpStatusCode, httpStatusDescription)) {
										KEPConfig cfg;
										if (!cfg.Parse(resultText.c_str(), "Result")) {
											LOG4CPLUS_ERROR(m_logger, "Call to premium item url failed ");
										} else if (cfg.Get("ReturnCode", 1) != 0) {
											LOG4CPLUS_ERROR(m_logger, "Call to premium item url got bad result " << resultText);
										} else {
											// add item to the list
											int itemId = cfg.Get("GlobalId", -1);
											if (itemId != -1) 
												premiumItems.insert(std::map<std::string, int>::value_type((*i)->m_name, itemId));
										}
									}
								}
							}

							if (templateSelDlg.m_selectedTemplate->m_leaderBoard != NULL) {
								string leaderUrl(urlPrefix);
								PathAddInPlace(leaderUrl, SETUP_LEADERBOARD_URL);
								STLStringSubstitute(leaderUrl, "{0}", templateSelDlg.m_selectedTemplate->m_leaderBoard->m_name);
								STLStringSubstitute(leaderUrl, "{1}", templateSelDlg.m_selectedTemplate->m_leaderBoard->m_label);
								STLStringSubstitute(leaderUrl, "{2}", templateSelDlg.m_selectedTemplate->m_leaderBoard->m_type);
								STLStringSubstitute(leaderUrl, "{3}", templateSelDlg.m_selectedTemplate->m_leaderBoard->m_sort);
								STLStringSubstitute(leaderUrl, "{4}", gameId);
								STLStringSubstitute(leaderUrl, "{5}", email);
								STLStringSubstitute(leaderUrl, "{6}", pw);

								::GA("Create%5F3DApp%5FLeaderBoard", m_editorBaseDir.c_str());

								DWORD httpStatusCode;
								string httpStatusDescription;
								string resultText;
								size_t resultSize = 0;
								if (::GetBrowserPage(leaderUrl, resultText, resultSize, httpStatusCode, httpStatusDescription)) {
									KEPConfig cfg;
									if (!cfg.Parse(resultText.c_str(), "Result")) {
										LOG4CPLUS_ERROR(m_logger, "Call to setup leaderboard url failed ");
									} else if (cfg.Get("ReturnCode", 1) != 0) {
										LOG4CPLUS_ERROR(m_logger, "Call to setup leaderboard url got bad result " << resultText);
									}
								}
							}
						}

						// write out web ids file
						CStringA scriptDir(PathAdd(gameSelDlg.m_destDir, SAVED_SCRIPTS_DIR).c_str());

						TRY {
							CFile f;
							CFileException cfe;
							if (f.Open(Utf8ToUtf16(PathAdd(scriptDir, WEB_IDS_SCRIPT)).c_str(), CFile::modeCreate | CFile::modeWrite, &cfe)) {
								CStringA webIds;
								webIds.Format("GAME_INFO = {%s, \"%s\", \"%s\"}\n", gameId.c_str(), gameSelDlg.m_selectedGame->GetConsumerKey(), gameSelDlg.m_selectedGame->GetConsumerSecret());

								// badgeNames = {"name 1", "name 2", "etc..."}
								webIds += "badgeNames = {";
								CStringA badgeNameString;
								for (std::map<std::string, int>::iterator achievement = achievements.begin(); achievement != achievements.end(); achievement++) {
									CStringA tempString;
									tempString.Format("\"%s\", ", achievement->first.c_str());
									badgeNameString += tempString;
								}
								if (badgeNameString.GetLength() > 0) {
									badgeNameString = badgeNameString.Left(badgeNameString.GetLength() - 2);
								}
								webIds += badgeNameString + "}\n";

								// BADGES = {}
								webIds += "BADGES = {}\n";

								// BADGES[badgeNames[1]] = badgeGLID1
								int achievementCount = 1;
								for (std::map<std::string, int>::iterator achievement = achievements.begin(); achievement != achievements.end(); achievement++) {
									CStringA tempString;
									tempString.Format("BADGES[badgeNames[%d]] = %d", achievementCount, achievement->second);
									webIds += tempString + "\n";
									achievementCount++;
								}

								// itemNames = {"name 1", "name 2", "etc..."}
								webIds += "itemNames = {";
								CStringA itemNameString;
								for (std::map<std::string, int>::iterator item = premiumItems.begin(); item != premiumItems.end(); item++) {
									CStringA tempString;
									tempString.Format("\"%s\", ", item->first.c_str());
									itemNameString += tempString;
								}
								if (itemNameString.GetLength() > 0) {
									itemNameString = itemNameString.Left(itemNameString.GetLength() - 2);
								}
								webIds += itemNameString + "}\n";

								// PREMIUM_ITEMS = {}
								webIds += "PREMIUM_ITEMS = {}\n";

								// PREMIUM_ITEMS[itemNames[1]] = itemGLID1
								int premiumItemCount = 1;
								for (std::map<std::string, int>::iterator item = premiumItems.begin(); item != premiumItems.end(); item++) {
									CStringA tempString;
									tempString.Format("PREMIUM_ITEMS[itemNames[%d]] = %d", premiumItemCount, item->second);
									webIds += tempString + "\n";
									premiumItemCount++;
								}

								f.Write((const char *)webIds, webIds.GetLength());
								f.Close();
							}
						}
						CATCH(CFileException, pEx) {
							wchar_t s[255];
							pEx->GetErrorMessage(s, 255);
							LOG4CPLUS_ERROR(m_logger, "Error writing web ids file: " << Utf16ToUtf8(s) << " " << pEx->m_cause);
							pEx->Delete();
						}
						END_CATCH

						::GA("Configure%5F3DApp%5FDB", m_editorBaseDir.c_str());

						// setup the database
						bool dbConfigSaved = false;

						try {
							KEPConfig cfg;
							cfg.Load(PathAdd(gameSelDlg.m_destDir, DB_CFG_NAME).c_str(), DB_CFG_ROOT_NODE);
#ifndef DECOUPLE_DBCONFIG
							if (DBConfig::Export(cfg, dbConfigDlg.GetDBServerUtf8(), dbConfigDlg.GetDBPort(), dbConfigDlg.GetDBNameUtf8(), dbConfigDlg.GetDBUserUtf8(), dbConfigDlg.GetDBPasswordUtf8(), true,
#else
							if (DBConfigExporter::Export(cfg, dbConfigDlg.m_dbServer, dbConfigDlg.m_dbPort, dbConfigDlg.m_dbName, dbConfigDlg.m_dbUser, dbConfigDlg.m_dbPassword, true,
#endif
									atol(gameSelDlg.m_selectedGame->GetParentId()) != 0)) {
								dbConfigSaved = cfg.SaveAs(PathAdd(gameSelDlg.m_destDir, DB_CFG_NAME).c_str());
							}
						} catch (KEPException *e) {
							delete e;
						} catch (KEPException &) {
						}

						if (dbConfigSaved) {
							// write out file to bootstrap db
							CStringA scriptDir(PathAdd(gameSelDlg.m_destDir, SQLSCRIPT_DIR).c_str());

							::GA("Install%5F3DApp%5FDB%5FScripts", m_editorBaseDir.c_str());

							HWND statusWindowHandle = progress.GetDlgItem(IDC_STATUS5)->m_hWnd;

							int ret = 0;
							try {
								// Create DB
								if (!createDBEx(dbConfigDlg.GetDBNameUtf8(),
										dbConfigDlg.GetDBServerUtf8(), dbConfigDlg.GetDBPort(),
										dbConfigDlg.GetDBUserUtf8(), dbConfigDlg.GetDBPasswordUtf8(),
										statusWindowHandle)) {
									MessageBoxW(Utf8ToUtf16("Error creating database: [" + dbConfigDlg.GetDBNameUtf8() + "]").c_str(), L"Error", MB_OK);
								}
								// Run master script
								else if (!installScriptsEx(scriptDir, "master.sql", dbConfigDlg.GetDBNameUtf8(),
											 dbConfigDlg.GetDBServerUtf8(), dbConfigDlg.GetDBPort(),
											 dbConfigDlg.GetDBUserUtf8(), dbConfigDlg.GetDBPasswordUtf8(),
											 statusWindowHandle)) {
									MessageBoxW(Utf8ToUtf16("Error running DB scripts on database: [" + dbConfigDlg.GetDBNameUtf8() + "]").c_str(), L"Error", MB_OK);
								} else {
									// Succeeded
									ret = 1;
								}
							} catch (MySqlSetupException *e) {
								MessageBoxW(Utf8ToUtf16(e->getMessage().c_str()).c_str(), Utf8ToUtf16(e->getTitle()).c_str(), MB_OK);
								delete e;
							}

							if (ret == 1) {
								progress.SetNextToRunning();

								// update the game object
								m_game.New(gameFile, gameSelDlg.m_selectedGame->GetName());
								m_game.SetAuthor(loginDlg.m_email);

								if (m_game.SetGameUserPass(loginDlg.m_email, loginDlg.m_password) &&
									m_game.SetEncryptionKey(gameSelDlg.m_selectedGame->GetEncryptionKey()) &&
									m_game.SetServerKey(gameSelDlg.m_selectedGame->GetServerKey())) {
									m_game.SetGameId(gameSelDlg.m_selectedGame->GetId());
									m_game.SetParentId(gameSelDlg.m_selectedGame->GetParentId());
								} else {
									AfxMessageBox(IDS_ENCRYPTION_FAILED, MB_ICONEXCLAMATION);
									SetGamePubUserPass(NULL, NULL);
								}

								if (m_game.Save(gameFile)) {
									int fileXMLPort = 0;
									const char *const BLADES_FOLDER = "Blades";

									SetPathBase((const char *)m_game.PathBase(), false);

									// if subgame, and template has folders for it, rename them
									bool isDir = false;
									string menuFolder = PathAdd(PathGameFiles(), "Menus-");
									if (jsFileExists(menuFolder.c_str(), &isDir) && isDir)
										rename(menuFolder.c_str(), (menuFolder + (const char *)m_game.GameId()).c_str());
									menuFolder = PathAdd(PathGameFiles(), "MenuScripts-");
									if (jsFileExists(menuFolder.c_str(), &isDir) && isDir)
										rename(menuFolder.c_str(), (menuFolder + (const char *)m_game.GameId()).c_str());

									// update the default XML file, if it exists
									try {
										// the CustomServerDist.wsf that is called in create distro
										// does this same thing
										char hostname[128];
										CStringA key;
										if (gethostname(hostname, _countof(hostname)) == 0 &&
											m_game.GetServerKey(key)) {
											KEPConfig cfg;
											cfg.Load((PathAdd(gameSelDlg.m_destDir, BLADES_FOLDER) + "\\DefaultServer.xml").c_str(), "EngineConfig");
											cfg.SetValue("Directory", gameSelDlg.m_destDir);
											cfg.SetValue("ScriptDir", PathAdd(gameSelDlg.m_destDir, "GameFiles\\ServerScripts").c_str());
											cfg.SetValue("Enabled", 1);
											cfg.SetValue("EngineName", m_game.Name() + " Server");
											cfg.SetValue("IpAddress", "AUTO");
											cfg.SetValue("RegistrationKey", key);
											fileXMLPort = cfg.GetValue(IPPORT_TAG, DEFAULT_SERVER_PORT);
											cfg.Save();

											cfg.Load((PathAdd(gameSelDlg.m_destDir, BLADES_FOLDER) + "\\DefaultAIServer.xml").c_str(), "EngineConfig");
											cfg.SetValue("Directory", gameSelDlg.m_destDir);
											cfg.SetValue("ScriptDir", PathAdd(gameSelDlg.m_destDir, "GameFiles\\ServerScripts").c_str());
											// cfg.SetValue("Enabled", 0 );  //8/25 leave enabled state the same for now since doesn't work
											cfg.SetValue("EngineName", m_game.Name() + " AI Server");
											cfg.Save();

											cfg.Load((PathAdd(gameSelDlg.m_destDir, BLADES_FOLDER) + "\\DefaultDispatcherClient.xml").c_str(), "EngineConfig");
											cfg.SetValue("Directory", gameSelDlg.m_destDir);
											cfg.SetValue("Enabled", 1);
											cfg.SetValue("EngineName", m_game.Name() + " Dispatcher Client");
											cfg.Save();
										}

									} catch (KEPException *e) {
										e->Delete();
									}

									ProgressUpdate("Game File Open New", TRUE, FALSE);

									if (((CMainFrame *)AfxGetMainWnd())->IsGameOpen()) {
										ReInit();
										SetGamePubUserPass(NULL, NULL);
										m_loadSceneExgFileName.Empty();
									}

									ReadStartCfgFile();

									CStringA baseDir(PathBase().c_str());

									//	Set resource paths
									IResource::SetBaseTexturePath(eTexturePath::MapsModels, PathMapsModels());
									IResource::SetBaseTexturePath(eTexturePath::CustomTexture, PathCustomTexture());
									IResource::SetBaseAnimPath(eAnimPath::CharacterAnimations, PathCharacterAnimations());
									IResource::SetBaseMeshPath(eMeshPath::CharacterMeshes, PathCharacterMeshes());

									loadAutoExec(baseDir);

									// Okay, now copy the GIL into a new CObList that can be accessed via SetGet.
									// NOTE, This work around is predicated on the assumption that the GIL is
									// static.
									m_globalInventoryDB->traverse(GILCopyAgent(*m_altGlobalInventory));

									if (makeEmptySVR(baseDir, m_game.SVR()) && loadEditorSVR(baseDir, m_game.SVR())) {
										loadSVRParts(baseDir);
									}

									SetPathConfigs(PathAdd(baseDir.GetString(), "GAMEFILES"));
									m_scriptDir = PathAdd(PathGameFiles(), "ClientScripts");

									((CMainFrame *)AfxGetMainWnd())->SetGameStatus(TRUE, FALSE);

									SetTitle();

									ProgressUpdate("Game File New STOP", FALSE, TRUE);

									// switch to the general tab

									KEPTREEDATA td;
									td.dialogId = CGameDlg::IDD;
									AfxGetMainWnd()->SendMessage(KEP_TREECHANGE, 0, (LPARAM)&td);

									// DWD: unload and reload the client blades
									ClientBladeFactory::Instance()->UnloadBlades();

									ClientBladeFactory::Instance()->FindBlades(baseDir.GetString());
									size_t blades = ClientBladeFactory::Instance()->GetNumBlades();
									for (size_t i = 0; i < blades; i++)
										ClientBladeFactory::Instance()->GetBlade(i)->SetEngineInterface(this);

									ClientBladeFactory::Instance()->InitClientBlades();

									// re-load the menu list
									m_menuList->Load(0, false, true);

									progress.Destroy();

									CStringA platformInstallLocation;

									GetPlatformInstallLocation(platformInstallLocation);

									if (platformInstallLocation.IsEmpty()) {
										return; // Already informed of error
									}

									// register this server with the game
									int webAssignedPort = 0;
									string distroFolder;
									::GA("Add%5F3DApp%5FGame%5FServer", m_editorBaseDir.c_str());

									if (!gameSelDlg.AddGameServer(gameSelDlg.m_destDir, distroFolder, platformInstallLocation, webAssignedPort)) {
										::GA("Add%5F3DApp%5FGame%5FServerFailed", m_editorBaseDir.c_str());
										AfxMessageBox(IDS_NEW_SERVER_FAILED, MB_ICONINFORMATION);
									} else if (webAssignedPort != 0 && (fileXMLPort != webAssignedPort)) {
										// The call to add the gameserver on the web returned a different port than the one
										// specified in our defaultserver.xml file. Use the web's port. The web assigned a different
										// port due to the server name and port already having a game associated to this port.
										KEPConfig cfg;
										cfg.Load((PathAdd(gameSelDlg.m_destDir, BLADES_FOLDER) + "\\DefaultServer.xml").c_str(), "EngineConfig");
										cfg.SetValue(IPPORT_TAG, (int)webAssignedPort);
										cfg.Save();
									}

									if (atol(gameSelDlg.m_selectedGame->GetParentId()) != 0) {
										// for child games, create distro always, creating default folder if needed
										_mkdir(distroFolder.c_str());
										m_game.SetLastDistroFolder(distroFolder.c_str());
										m_game.Save();

										::GA("Call%5F3DApp%5FDistribution", m_editorBaseDir.c_str());
										OnGameDistribution(distroFolder.c_str(), true, IsWindowVisible() != FALSE);
									} else
										AfxMessageBox(IDS_NEW_GAME_COMPLETE, MB_ICONINFORMATION);
								} else {
									CStringW s;
									s.Format(IDS_NEW_GAME_SAVE_FAILED, Utf8ToUtf16(gameFile).c_str());
									AfxMessageBox(s, MB_ICONEXCLAMATION);
									ret = E_INVALIDARG;
								}
							} else {
								::GA("Install%5F3DApp%5FDB%5FScriptsFailed", m_editorBaseDir.c_str());
							}
						} else {
							CStringW s;
							s.Format(IDS_DB_CONFIG_SAVE_FAILED);
							AfxMessageBox(s, MB_ICONEXCLAMATION);
							ret = E_INVALIDARG;
						}
					}
				}

				if (ret != NO_ERROR || FileOp.fAnyOperationsAborted) {
					// DWD: unload the client blades
					ClientBladeFactory::Instance()->UnloadBlades();

					// clean up;
					FileOp.wFunc = FO_DELETE;
					FileOp.fFlags = FOF_SILENT | FOF_NOERRORUI;
					std::wstring tempFrom = Utf8ToUtf16(gameSelDlg.m_destDir + "*.*");
					tempFrom += L'\0';
					FileOp.pFrom = tempFrom.c_str();
					FileOp.pTo = NULL;
					FileOp.fFlags = FileOp.fFlags | FOF_SILENT;
					SHFileOperation(&FileOp);
				}
			}
		}

		// always save off to make easier to re-File->New
		SetGamePubUserPass(loginDlg.m_email, loginDlg.m_password);

		((CMainFrame *)AfxGetMainWnd())->m_wndKEPDialogBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndInformation.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndTreeBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndToolBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndGameToolBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->EnableWindow(TRUE);
		this->EnableWindow(TRUE);
	}
}

void CKEPEditView::OnUpdateFileSave(CCmdUI *pCmdUI) {
	pCmdUI->Enable(((CMainFrame *)AfxGetMainWnd())->IsGameOpen());
}

void CKEPEditView::OnUpdateFileSaveAll(CCmdUI *pCmdUI) {
	pCmdUI->Enable(((CMainFrame *)AfxGetMainWnd())->IsGameOpen());
}

///---------------------------------------------------------
// CKEPEditView::OnUpdateZoneSave
//
void CKEPEditView::OnUpdateZoneSave(CCmdUI *pCmdUI) {
	pCmdUI->Enable(!m_loadSceneExgFileName.IsEmpty() && m_loadSceneExgFileName != "NONE");
}

// TODO these should be in a config file somewhere
// limits on when to change color of status text
static long fpsRedThresh = 29;
static long fpsYellowThresh = 40;
static ULONG textMemRedThresh = 400000000;
static ULONG textMemYellowThresh = 500000000;

// color indexes in the rtf file
#define RED_INDEX 4
#define YELLOW_INDEX 3
#define GREEN_INDEX 2

void CKEPEditView::RenderLoopInfoUpdate() {
	// copied mostly from from ClientEngine
	CStringW msg;
	VERIFY(msg.LoadString(IDS_STATUS_ZONE));

	// Update Once Per Second
	if (m_timerRenderLoopInfo.ElapsedSec() > 1.0) {
		m_timerRenderLoopInfo.Reset();

		if (m_editorModeEnabled == TRUE) {
			wstringstream strBuilder;
			strBuilder << m_numObjectsInGoodSubBox << "/V:" << GL_objectsRendered;

			int fpsColor, textureColor;

			long m_exactFPSHiRes = 0; // No longer a class member. Replacement not yet available.
			if (m_exactFPSHiRes <= fpsRedThresh)
				fpsColor = RED_INDEX;
			else if (m_exactFPSHiRes <= fpsYellowThresh)
				fpsColor = YELLOW_INDEX;
			else
				fpsColor = GREEN_INDEX;

			if (g_pD3dDevice->GetAvailableTextureMem() >= textMemRedThresh)
				textureColor = RED_INDEX;
			else if (g_pD3dDevice->GetAvailableTextureMem() >= textMemYellowThresh)
				textureColor = YELLOW_INDEX;
			else
				textureColor = GREEN_INDEX;

			CStringW status;
			status.Format(msg,
				strBuilder.str().c_str(), // objects rendered
				GL_polysRenderedPerFrame, // polys rendered
				m_pRenderStateMgr->m_stateChangesPerPass, // TS/Pass
				m_pRenderStateMgr->m_maxActiveLights,
				m_viewportRenderCount,
				fpsColor,
				m_exactFPSHiRes,
				m_particleRM->GetNumberOfRuntimeEffects(),
				textureColor,
				(g_pD3dDevice->GetAvailableTextureMem() / 1000000));
		} // end 1
	} // end update every second
}

///---------------------------------------------------------
// CKEPEditView::OnKillFocus
//
// [in] pNewWnd
//
void CKEPEditView::OnKillFocus(CWnd *pNewWnd) {
	SetMouseKeyboardUpdate(FALSE);
	SetLockGameWindow(true);
	CView::OnKillFocus(pNewWnd);
}

///---------------------------------------------------------
// CKEPEditView::OnLButtonDown
//
// Allow navigation keyboard/mouse in the game when clicking in the view.
// Loss of focus of the view will disable keyboard/mouse in the game
// so the Editor user will not be navigating in the game while using the
// Editor dialogs, setting values, etc.
//
// [in] nFlags
// [in] point
//
void CKEPEditView::OnLButtonDown(UINT nFlags, CPoint point) {
	CRect rect;

	GetClientRect(&rect);

	if (rect.PtInRect(point)) {
		SetMouseKeyboardUpdate(TRUE);
		SetLockGameWindow(false);
	}

	CView::OnLButtonDown(nFlags, point);
}

void CKEPEditView::OnMButtonDown(UINT nFlags, CPoint point) {
	ToggleLockGameWindow();

	CView::OnMButtonDown(nFlags, point);
}

///---------------------------------------------------------
// CKEPEditView::SetEditorAcceptText
//
// Allows the editor to capture text as well as the game.
//
// [in] val
//
void CKEPEditView::SetEditorAcceptText(BOOL val) {
	m_editorTextModeOn = val;
}

///---------------------------------------------------------
// CKEPEditView::OnUpdateAppRender
//
// [in] pCmdUI
//
void CKEPEditView::OnUpdateAppRender(CCmdUI *pCmdUI) {
	pCmdUI->Enable(!m_loadSceneExgFileName.IsEmpty() && m_loadSceneExgFileName != "NONE");
}

void CKEPEditView::OnFileSave() {
	m_game.Save();
	SetTitle();
}

void CKEPEditView::OnFileSaveAll() {
	//save game
	OnFileSave();

	//save game files before saving zone
	EditorState::GetInstance()->SaveAll();

	//save zone if it's loaded
	if (GetCurrentSceneFile() != "" && GetCurrentSceneFile() != "NONE")
		OnEditorZoneSave();
}

void CKEPEditView::OnFileSaveAs() {
	CFileDialog opendialog(FALSE, L"kep", 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT, Utf8ToUtf16(KEP_FILTER()).c_str(), this);
	if (opendialog.DoModal() == IDOK) {
		CStringA origGameName;
		const char *start = strrchr(m_game.FileNameCStr(), '\\');
		origGameName = start + 1;

		CStringW copyGameName;
		copyGameName.LoadString(IDS_COPY_OF);
		copyGameName += Utf8ToUtf16(m_game.Name()).c_str(); // current game name

		CStringW gameFile = opendialog.GetPathName();
		std::string origBaseDirUtf8 = PathBase();
		CStringW origBaseDirW = Utf8ToUtf16(origBaseDirUtf8).c_str();

		CStringW destDir = gameFile;
		int fileNameOffset = destDir.GetLength() - destDir.ReverseFind('\\');
		destDir.GetBufferSetLength(destDir.GetLength() - (fileNameOffset - 1));

		SHFILEOPSTRUCT FileOp;
		ZeroMemory(&FileOp, sizeof(FileOp));
		// Attach to status window, disable all others to avoid user
		// actions. Simulates modal
		FileOp.hwnd = ((CMainFrame *)AfxGetMainWnd())->m_wndKEPStatusBar.m_hWnd;
		FileOp.wFunc = FO_COPY;

		std::wstring tempFrom;
		std::wstring tempTo;
		std::wstring tempFromDeleteFiles;

		tempFrom = origBaseDirW;
		tempFrom += L"startcfg.xml";
		tempFrom += L'\0';
		tempFrom += L'\0';

		// Save if we need to delete
		tempFromDeleteFiles = destDir;
		tempFromDeleteFiles += L"startcfg.xml";
		tempFromDeleteFiles += L'\0';
		tempFromDeleteFiles += L'\0';

		tempTo = destDir;

		FileOp.pFrom = tempFrom.c_str();
		FileOp.pTo = tempTo.c_str();
		FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR | FOF_NORECURSION | FOF_SILENT | FOF_NOERRORUI;

		if (origBaseDirW == destDir) {
			CStringW msg;
			msg.Format(IDS_FILESAVEAS_BASETARGET_ERROR);
			AfxMessageBox(msg, MB_ICONEXCLAMATION);
			return;
		}

		// to simulate modal, all disabled except status to which the
		// file copying is attached.
		((CMainFrame *)AfxGetMainWnd())->m_wndKEPDialogBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndInformation.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndTreeBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndToolBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->m_wndGameToolBar.EnableWindow(FALSE);
		((CMainFrame *)AfxGetMainWnd())->EnableWindow(FALSE);
		this->EnableWindow(FALSE);

		int ret = SHFileOperation(&FileOp);

		if (ret == NO_ERROR) {
			ZeroMemory(&FileOp, sizeof(FileOp));
			FileOp.hwnd = AfxGetMainWnd()->m_hWnd;
			FileOp.wFunc = FO_COPY;

			std::string tempFromUtf8;

			tempFromUtf8 = origBaseDirUtf8;
			tempFromUtf8 += GAMEFILES;
			tempFromUtf8 += '\0';
			tempFromUtf8 += origBaseDirUtf8;
			tempFromUtf8 += MAPSMODELS;
			tempFromUtf8 += '\0';
			tempFromUtf8 += origBaseDirUtf8;
			tempFromUtf8 += SOUNDS;
			tempFromUtf8 += '\0';
			tempFromUtf8 += origBaseDirUtf8;
			tempFromUtf8 += CLIENTBLADES;
			tempFrom = Utf8ToUtf16(tempFromUtf8);
			tempFrom += L'\0';

			tempTo = destDir;
			tempTo += L'\0';

			FileOp.pFrom = tempFrom.c_str();
			FileOp.pTo = tempTo.c_str();
			FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR | FOF_NOERRORUI;

			ret = SHFileOperation(&FileOp);

			if (ret == NO_ERROR && !FileOp.fAnyOperationsAborted) {
				ProgressUpdate("Game File Save As START", TRUE, FALSE);

				m_game.SetName(Utf16ToUtf8(copyGameName.GetString()).c_str());
				m_game.Save(Utf16ToUtf8(gameFile.GetString()).c_str());
				m_game.Copy(m_game.Name());

				SetPathBase(m_game.PathBase().GetString(), false);
				SetCurrentDirectoryW(Utf8ToUtf16(PathBase()).c_str());

				if (((CMainFrame *)AfxGetMainWnd())->IsGameOpen()) {
					ReInit();
					m_loadSceneExgFileName.Empty();
				}

				// Reload to pick up any dependencies with directory change
				CStringA baseDir(PathBase().c_str());

				loadAutoExec(baseDir);

				if (loadEditorSVR(baseDir, m_game.SVR())) {
					loadSVRParts(baseDir);
				}

				SetPathConfigs(PathGameFiles().c_str());

				SetTitle();

				((CMainFrame *)AfxGetMainWnd())->SetGameStatus(TRUE, FALSE);

				ProgressUpdate("Game File Save As END", FALSE, TRUE);

				// switch to the general tab

				KEPTREEDATA td;
				td.dialogId = CGameDlg::IDD;
				AfxGetMainWnd()->SendMessage(KEP_TREECHANGE, 0, (LPARAM)&td);

				// DWD: unload and reload the client blades
				ClientBladeFactory::Instance()->UnloadBlades();

				ClientBladeFactory::Instance()->FindBlades(baseDir.GetString());
				size_t blades = ClientBladeFactory::Instance()->GetNumBlades();
				for (size_t i = 0; i < blades; i++)
					ClientBladeFactory::Instance()->GetBlade(i)->SetEngineInterface(this);

				ClientBladeFactory::Instance()->InitClientBlades();
			} else {
				if (!FileOp.fAnyOperationsAborted) {
					CStringW msg;
					msg.Format(IDS_FILESAVEAS_COPYDIR_ERROR);
					AfxMessageBox(msg, MB_ICONEXCLAMATION);
				}

				ClientBladeFactory::Instance()->UnloadBlades();

				FileOp.wFunc = FO_DELETE;
				FileOp.fFlags = FOF_SILENT | FOF_NOERRORUI | FOF_NOCONFIRMATION;
				FileOp.pTo = NULL;

				CStringW tempDeleteDirs;

				tempDeleteDirs = destDir;
				tempDeleteDirs += Utf8ToUtf16(GAMEFILES).c_str();
				tempDeleteDirs += L'\0';
				tempDeleteDirs += L'\0';

				FileOp.pFrom = tempDeleteDirs.GetString();
				SHFileOperation(&FileOp);

				tempDeleteDirs.Empty();
				tempDeleteDirs = destDir;
				tempDeleteDirs += Utf8ToUtf16(MAPSMODELS).c_str();
				tempDeleteDirs += L'\0';
				tempDeleteDirs += L'\0';

				FileOp.pFrom = tempDeleteDirs.GetString();
				SHFileOperation(&FileOp);

				tempDeleteDirs.Empty();
				tempDeleteDirs = destDir;
				tempDeleteDirs += Utf8ToUtf16(SOUNDS).c_str();
				tempDeleteDirs += L'\0';
				tempDeleteDirs += L'\0';

				FileOp.pFrom = tempDeleteDirs.GetString();
				SHFileOperation(&FileOp);

				tempDeleteDirs.Empty();
				tempDeleteDirs = destDir;
				tempDeleteDirs += Utf8ToUtf16(CLIENTBLADES).c_str();
				tempDeleteDirs += L'\0';
				tempDeleteDirs += L'\0';

				FileOp.pFrom = tempDeleteDirs.GetString();
				SHFileOperation(&FileOp);

				// clean up files too;

				FileOp.pFrom = tempFromDeleteFiles.c_str();
				SHFileOperation(&FileOp);
			}
		} else {
			CStringW msg;
			msg.Format(IDS_FILESAVEAS_COPYFILE_ERROR);
			AfxMessageBox(msg, MB_ICONEXCLAMATION);

			ClientBladeFactory::Instance()->UnloadBlades();

			// clean up;
			FileOp.wFunc = FO_DELETE;
			FileOp.fFlags = FOF_SILENT | FOF_NOERRORUI | FOF_NOCONFIRMATION;
			FileOp.pFrom = tempFromDeleteFiles.c_str();
			FileOp.pTo = NULL;
			SHFileOperation(&FileOp);
		}

		((CMainFrame *)AfxGetMainWnd())->m_wndKEPDialogBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndInformation.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndTreeBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndToolBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->m_wndGameToolBar.EnableWindow(TRUE);
		((CMainFrame *)AfxGetMainWnd())->EnableWindow(TRUE);
		this->EnableWindow(TRUE);
	}
}

void CKEPEditView::loadSVRParts(CStringA baseDir) {
	string baseConfigPath = PathAdd(baseDir.GetString(), GAMEFILES);
	SetPathConfigs(baseConfigPath);

	string supportedWorldsFileName = PathAdd(baseConfigPath, SUPPORTED_WORLDS_CONFIG_FILE);
	m_multiplayerObj->m_currentWorldsUsed->SerializeFromXML(supportedWorldsFileName);

	string spawnPointsFileName = PathAdd(baseConfigPath, SPAWN_POINTS_CONFIG_FILE);
	m_multiplayerObj->m_spawnPointCfgs->SerializeFromXML(spawnPointsFileName);

	string networkDefinitionsFileName = PathAdd(baseConfigPath, NETWORK_DEFINITION_ZONE_DB_FILE);
	m_multiplayerObj->SerializeFromXML(networkDefinitionsFileName);

	if (!m_delayLoadHousingZone.IsEmpty()) {
		string housingDB = PathAdd(baseConfigPath, m_delayLoadHousingZone.GetString());
		LoadHouseZoneDB(housingDB);
	}
}

void CKEPEditView::loadAutoExec(CStringA baseDir) {
	// game load sets dir
	m_pRenderStateMgr->m_baseDir = baseDir;

	CRect rect;
	GetClientRect(&rect);

	m_curModeRect = rect;

	LoadScenePerformSplashScreen();

	InitEvents();

	AutoExecInitAndParseCommandLine();

	RECT sizeRect = RECT({ 0, 0, RectWidth(m_curModeRect), RectHeight(m_curModeRect) });
	m_pCABD3DLIB->m_curModeRect = GuiRect = sizeRect;
}

typedef struct
{
	const char *name;
	const char *pw;
	bool isGM;
	bool isAI;
} AcctInfo;

AcctInfo svrBootStrapAccts[] = {
	{ "gm", "gm", true, false },
	{ "ai", "ai", false, true },
	{ "gamer", "gamer", false, false },
	{ 0, 0, false, false }
};

// make an empty svr file when they create a new game
BOOL CKEPEditView::makeEmptySVR(CStringA baseDir, CStringA fname) {
	CFileException exc;
	CFile fl;

	CStringA editorfname = (PathAdd(baseDir, GAMEFILES) + "\\").c_str(); // must have trailing slash
	editorfname += fname;
	CStringW editorfnameW = Utf8ToUtf16(editorfname).c_str();

	// first create an empty CMultiplayer object
	CMultiplayerObj *cmp = new CMultiplayerObj();
	if (cmp) {
		CFile fl;
		cmp->m_accountDatabase = new CAccountObjectList();

		// allocate empty lists so not null when serialized.
		cmp->m_currentWorldsUsed = new CWorldAvailableObjList();
		cmp->m_spawnPointCfgs = new CSpawnCfgObjList();
		cmp->m_clanDatabase = new CClanSystemObjList();
		cmp->m_housingDB = new CHousingZoneDB();

		// add the default accounts
		CAccountObject *acct = 0;
		for (int i = 0; svrBootStrapAccts[i].name != 0; i++) {
			MD5_DIGEST_STR pw;

			hashString(CStringA(svrBootStrapAccts[i].pw) + svrBootStrapAccts[i].name, pw);
			acct = new CAccountObject(svrBootStrapAccts[i].name, pw.s);
			if (svrBootStrapAccts[i].isGM) {
				acct->m_gm = acct->m_administratorAccess = acct->m_canSpawn = TRUE;
			}
			if (svrBootStrapAccts[i].isAI) {
				acct->m_aiServer = TRUE;
			}
			acct->m_accountNumber = cmp->m_accountDatabase->GetUntakenAccountNumber();
			acct->m_pPlayerObjectList = new CPlayerObjectList();
			acct->m_registeredVersion = TRUE;
			cmp->m_accountDatabase->AddTail(acct);
		}

		// clear read-only bit if set
		DWORD fileAttributes;
		fileAttributes = GetFileAttributesW(editorfnameW);
		if (INVALID_FILE_ATTRIBUTES != fileAttributes) {
			SetFileAttributes(editorfnameW, fileAttributes & (~FILE_ATTRIBUTE_READONLY));
		} // INVALID_FILE_ATTRIBUTES != fileAttributes

		// write out the file
		BOOL res = fl.Open(editorfnameW, CFile::modeCreate | CFile::modeWrite, &exc);

		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			CStringW msg;
			wstring err;
			msg.Format(IDS_EDITOR_SVR_CREATE_FAILED, editorfnameW, ::errorNumToString(exc.m_cause, err));
			AfxMessageBox(msg, MB_ICONEXCLAMATION);
			return FALSE;
		}
		BEGIN_SERIALIZE_TRY

		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << cmp;
		cmp->SafeDelete();
		delete cmp;
		the_out_Archive.Close();
		fl.Close();

		END_SERIALIZE_TRY(m_logger)
		if (!serializeOk) {
			CStringW msg;
			msg.Format(IDS_EDITOR_SVR_CREATE_FAILED_SEE_LOG, editorfnameW.GetString());
			AfxMessageBox(msg, MB_ICONEXCLAMATION);
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CKEPEditView::loadEditorSVR(CStringA baseDir, CStringA fname) {
	BOOL ret = FALSE;

	CFileException exc;
	CFile fl;

	CStringA editorfname = (PathAdd(baseDir, GAMEFILES) + "\\").c_str(); // must have trailing slash
	editorfname += fname;
	CStringW editorfnameW = Utf8ToUtf16(editorfname).c_str();

	if (!fname.IsEmpty()) {
		BOOL res = fl.Open(editorfnameW, CFile::modeReadWrite | CFile::shareDenyWrite, &exc);

		if (res == FALSE) {
			LOG_EXCEPTION(exc, m_logger)
			CStringW msg;
			msg.Format(IDS_EDITOR_SVR_LOAD_FAILED, editorfnameW, exc.m_cause);
			AfxMessageBox(msg, MB_ICONEXCLAMATION);
			return ret;
		}

		if (m_multiplayerObj) {
			m_multiplayerObj->SafeDelete();
			delete m_multiplayerObj;
			m_multiplayerObj = 0;
		}
		BEGIN_SERIALIZE_TRY

		CArchive the_in_Archive(&fl, CArchive::load);
		the_in_Archive >> m_multiplayerObj;
		char cwd[_MAX_PATH];
		m_multiplayerObj->SetPathBase(_getcwd(cwd, _MAX_PATH));
		m_multiplayerObj->m_accountDatabase->ExpandFromOptimized(m_skillDatabase, m_globalInventoryDB);
		the_in_Archive.Close();
		fl.Close();
		if (m_editorModeEnabled == TRUE) {
			InListAccounts(m_multiplayerObj->m_accountDatabase->GetCount() - 1);
		}
		END_SERIALIZE_TRY(m_logger)

		ret = TRUE;
	}

	return ret;
}

BOOL CKEPEditView::SaveEditorSVR(CStringA baseDir, CStringA fname) {
	CStringA editorfname = (PathAdd(baseDir, GAMEFILES) + "\\").c_str(); // must have trailing slash
	editorfname += fname;
	CStringW editorfnameW = Utf8ToUtf16(editorfname).c_str();

	CFileException exc;
	CFile fl;

	BOOL res = fl.Open(editorfnameW, CFile::modeCreate | CFile::modeWrite, &exc);

	if (res == FALSE) {
		LOG_EXCEPTION(exc, m_logger)
		CStringW msg;
		msg.Format(IDS_EDITOR_SVR_SAVE_FAILED, editorfnameW, exc.m_cause);
		AfxMessageBox(msg, MB_ICONEXCLAMATION);
		return res;
	}

	CArchive the_out_Archive(&fl, CArchive::store);

	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);

	BEGIN_SERIALIZE_TRY
	m_multiplayerObj->m_accountDatabase->FlagAllForOptimizedSave();
	the_out_Archive << m_multiplayerObj;
	END_SERIALIZE_TRY(m_logger)

	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
	the_out_Archive.Close();
	fl.Close();

	return serializeOk ? TRUE : FALSE;
}

void CKEPEditView::ProgressUpdate(CStringA position, BOOL init, BOOL deinit) {
	//temp addition of the wait cursor
	static CWaitCursor *wait = NULL;

	if (init) {
		wait = new CWaitCursor();
		return;
	} else if (deinit) {
		if (wait) {
			delete wait;
			wait = NULL;
		}
		return;
	}
}

void CKEPEditView::OnEditorZoneOpen() {
	BOOL autoLoad = FALSE;

	CStringA CurDir;
	CStringA MainDir;
	MainDir = PathBase().c_str();
	CurDir = PathAdd(MainDir, "GameFiles").c_str();
	SetCurrentDirectory(Utf8ToUtf16(CurDir.GetString()).c_str());

	CStringA engineAutoZone = GetZoneFileToOpen();
	CStringA pathName;

	pathName.Empty();

	if (!engineAutoZone.IsEmpty()) {
		// ClientEngine autoexec.aut scene load
		pathName = CurDir + engineAutoZone;
		autoLoad = TRUE;
		ClearZoneFileToOpen();
	} else {
		OpenZoneDlg dlg(CurDir, this);
		if (dlg.DoModal() == IDOK) {
			pathName = PathAdd(CurDir, dlg.ZoneName()).c_str();
		}
	}

	if (!pathName.IsEmpty()) {
		if (!autoLoad) {
			ProgressUpdate("START", TRUE, FALSE); // render loop will post final message
		}

		CStringA fileName;
		CStringA origMainDir = MainDir;

		MainDir = pathName.Left(pathName.ReverseFind('\\'));
		MainDir = MainDir.Left(MainDir.ReverseFind('\\'));

		const char *start = strrchr((const char *)pathName, '\\');
		fileName = start + 1;

		m_loadScene = true;

		CStringA origWorkingFile = GetZoneFileName().c_str();
		m_loadSceneExgFileName = fileName;
		SetZoneFileName(fileName.GetString());

		SetCurrentDirectoryW(Utf8ToUtf16(MainDir.GetString()).c_str());

		// Don't wait for the idle processing to kick off RenderLoop.
		// Determine if the zone is good or not right away so as not
		// to setup and init our working environment with a bogus
		// zone and then attempt to roll-back.

		if (ClientEngine::RenderLoop()) {
			((CMainFrame *)AfxGetMainWnd())->SetGameStatus(TRUE, TRUE);
			SetPathConfigs(PathAdd(MainDir.GetString(), "GAMEFILES"));
			m_game.SetLoadedZone(pathName);
			SetTitle();
		} else {
			SetCurrentDirectoryW(Utf8ToUtf16(origMainDir).c_str());
			m_loadSceneExgFileName = origWorkingFile;
			SetZoneFileName(origWorkingFile.GetString());
		}

		EditorState::GetInstance()->SetZoneDirty(false);
	}
}

void CKEPEditView::OnUpdateEditorZoneOpen(CCmdUI *pCmdUI) {
	pCmdUI->Enable(((CMainFrame *)AfxGetMainWnd())->IsGameOpen());
}

bool postDistStep(const char *scriptName, HWND hwnd, const char *baseDir, const char *destDir, const char *fileName, const char *licenseKey, const char *extra = "", bool skipDialogs = false, const char *gameId = "");

void CKEPEditView::OnEditorStartStopServer() {
	BOOL ret = TRUE;

	CWaitCursor wait;

	if (ServerIsRunning()) {
		ret = ServerAction(STOP);
	}

	if (ret) {
		// run the custom server dist with slightly modified params
		postDistStep("CustomServerDist.wsf", m_hWnd, m_game.PathBase(), m_game.PathBase(), m_game.FileNameCStr(), "", "Prompt");
		ServerAction(START);
	}
}

BOOL CKEPEditView::ServerAction(SERVERACTIONS action) {
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	CStringW cmdLine;
	CStringW serviceName;
	CStringW serviceNameNoExtension;

	serviceName.LoadString(IDS_SERVER_SERVICENAME);
	serviceNameNoExtension.LoadString(IDS_SERVER_SERVICENAME_NOEXT);

	if (action == START) {
		cmdLine = L"NET START ";
	} else if (action == STOP) {
		cmdLine = L"NET STOP ";
	} else {
		return FALSE;
	}

	cmdLine += serviceNameNoExtension;

	BOOL ret = FALSE;

	CStringW errMsg;

	// Start the child process.
	if (!KEP::CreateProcess(
			NULL, // No module name (use command line).
			static_cast<LPCWSTR>(cmdLine.GetString()),
			NULL, // Process handle not inheritable.
			NULL, // Thread handle not inheritable.
			FALSE, // Set handle inheritance to FALSE.
			CREATE_NO_WINDOW, // No creation flags.
			NULL, // Use parent's environment block.
			NULL, // Use parent's starting directory.
			&si, // Pointer to STARTUPINFO structure.
			&pi)) // Pointer to PROCESS_INFORMATION structure.
	{
		DWORD cause = GetLastError();
		if (action == START)
			errMsg.Format(IDS_SERVER_SERVICE_START_FAILED, serviceName.GetString(), cause);
		else
			errMsg.Format(IDS_SERVER_SERVICE_STOP_FAILED, serviceName.GetString(), cause);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Wait until child process exits.
	::WaitForSingleObject(pi.hProcess, INFINITE);

	DWORD processRet = 0;
	if (!GetExitCodeProcess(pi.hProcess, &processRet) || processRet) {
		if (action == START)
			errMsg.Format(IDS_SERVER_SERVICE_START_FAILED, serviceName.GetString(), processRet);
		else
			errMsg.Format(IDS_SERVER_SERVICE_STOP_FAILED, serviceName.GetString(), processRet);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
		ret = FALSE;
	} else
		ret = TRUE;

	// Close process and thread handles.
	::CloseHandle(pi.hProcess);
	::CloseHandle(pi.hThread);

	return ret;
}

BOOL CKEPEditView::ServerIsRunning() {
	HANDLE hProcessSnap = NULL;
	BOOL bRet = FALSE;
	PROCESSENTRY32W pe32 = { 0 };

	//  Take a snapshot of all processes in the system.

	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (hProcessSnap == INVALID_HANDLE_VALUE)
		return (FALSE);

	//  Fill in the size of the structure before using it.

	pe32.dwSize = sizeof(pe32);

	//  Walk the snapshot of the processes

	if (Process32First(hProcessSnap, &pe32)) {
		do {
			CStringW module = pe32.szExeFile;
			CStringW serviceName;
			serviceName.LoadStringW(IDS_SERVER_SERVICENAME);
			if (module == serviceName) {
				bRet = TRUE;
			}
		} while (Process32Next(hProcessSnap, &pe32));
	} else
		bRet = FALSE; // could not walk the list of processes

	// Do not forget to clean up the snapshot object.

	CloseHandle(hProcessSnap);
	return (bRet);
}
void CKEPEditView::OnUpdateEditorZoneNew(CCmdUI *pCmdUI) {
	pCmdUI->Enable(((CMainFrame *)AfxGetMainWnd())->IsGameOpen());
}

CStringA CKEPEditView::GetMRUFileToOpen(int *index) {
	if (m_MRUgameFile.IsEmpty()) {
		*index = -1;
	} else {
		*index = m_MRUgameFileIndex;
	}

	return m_MRUgameFile;
}

///---------------------------------------------------------
// CKEPEditView::SetTitle
//
// Set the title bar of the application. Format is:
// KEPEdit - Game Name [Physical path of game file] - [Physical path of zone file]
//
// Game Name, game file, zone file are optional based on the operations that have been
// performed by the user.
//
//
void CKEPEditView::SetTitle() {
	std::string title;
	if (((CMainFrame *)AfxGetMainWnd())->IsZoneOpen())
		title = m_title + " - " + m_game.Name() + " [" + m_game.FileNameCStr() + "] - [" + m_game.Zone() + "]";
	else if (((CMainFrame *)AfxGetMainWnd())->IsGameOpen())
		title = m_title + " - " + m_game.Name() + " [" + m_game.FileNameCStr() + "] - []";
	else
		title = m_title;

	GetDocument()->SetTitle(Utf8ToUtf16(title).c_str());

	GetParent()->SetWindowText(GetDocument()->GetTitle());
}

///---------------------------------------------------------
// CKEPEditView::EditorLoadScene
//
// Called by ClientEngine when the autoexec.aut specifies to
// automatically load a scene. The Editor needs to make sure
// that it knows about this auto scene load.
//
// [in] fileName
//
// [Returns]
//    BOOL
BOOL CKEPEditView::EditorLoadScene(CStringA fileName) {
	if (!fileName.IsEmpty()) {
		m_zoneFile = fileName;
		OnEditorZoneOpen();
	}

	return TRUE;
}

BOOL CKEPEditView::LoadElementDatabase(const string &fileName) {
	if (ClientEngine::LoadElementDatabase(fileName)) {
		InListMisChannels(0);
		return TRUE;
	}
	return FALSE;
}

BOOL CKEPEditView::LoadSkillsDatabase(const string &fileName) {
	if (ClientEngine::LoadSkillsDatabase(fileName)) {
		InListMissileSkillCriteria(0);
		return TRUE;
	}

	return FALSE;
}

BOOL CKEPEditView::LoadAnimationDatabase(const string &fileName) {
	return AnimationRM::Instance()->ReadSkeletonAnimations(PathAdd(PathGameFiles(), fileName).c_str());
}

bool CKEPEditView::LoadEntityDatabase(const string &fileName) {
	if (ClientEngine::LoadEntityDatabase(fileName)) {
		ProgressUpdate("EnitityInList");
		EnitityInList();
		return true;
	}

	return false;
}

BOOL CKEPEditView::LoadMissileDatabase(const string &fileName) {
	if (ClientEngine::LoadMissileDatabase(fileName)) {
		MissileDatabaseInList(-1);
		return TRUE;
	}

	return FALSE;
}

BOOL CKEPEditView::LoadExplosionFile(const string &fileName) {
	if (ClientEngine::LoadExplosionFile(fileName)) {
		InListExplosionDB(0);
		return TRUE;
	}

	return FALSE;
}

BOOL CKEPEditView::LoadAiData(const string &fileName) {
	if (ClientEngine::LoadAiData(fileName)) {
		AI_DBInList(m_AIOL->GetCount() - 1);
		ScriptInList(m_pAISOL->GetCount() - 1);
		return TRUE;
	}

	return FALSE;
}

//
// ===============================================================================
//    End Function
// ===============================================================================
//
void CKEPEditView::AI_DBInList(int sel) {
	if (!m_AIOL) {
		return;
	}

	if (m_editorModeEnabled == FALSE) {
		return;
	}

	m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_DB, LB_RESETCONTENT, 0, 0);

	ListItemMap botMap = m_AIOL->GetBots();
	ListItemMapIterator botMapIterator;

	for (botMapIterator = botMap.begin(); botMapIterator != botMap.end(); botMapIterator++) {
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_DB, LB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(botMapIterator->second).c_str());
	}

	if (sel > -1) {
		m_aiDBDlgBar.SendDlgItemMessage(IDC_LIST_AI_DB, LB_SETCURSEL, sel, 0);
	}
}

// End Function

bool CKEPEditView::ProcessScriptAttribute(CEXScriptObj *sctPtr) {
	switch (sctPtr->m_actionAttribute) {
		case LOAD_HOUSE_ZONES:
			// delay loading housing until after SVR is loaded
			m_delayLoadHousingZone = sctPtr->m_miscString;
			break;

		case LOAD_ARENA_SYS:
			LoadArenaSys(sctPtr->m_miscString.GetString());
			break;

		case OPEN_SCENE:
			// the superclass process it
			if (ClientEngine::ProcessScriptAttribute(sctPtr))
				// if it succeeded we want to break the script execution...
				sctPtr->m_miscInt = MISCINT_BREAK_FROM_SCRIPT;
			break;

		case LOAD_ENTITY_DATABASE:
			CKEPEditView::LoadEntityDatabase(sctPtr->m_miscString.GetString());
			break;

		case LOAD_ANIMATION_DATABASE:
			CKEPEditView::LoadAnimationDatabase(sctPtr->m_miscString.GetString());
			break;

		case LOAD_DYNAMIC_OBJECTS:
			if (LoadDynamicObjects(sctPtr->m_miscString.GetString()) == FALSE)
				m_dynamicObjectRM = new DynamicObjectRM("dynamicObjectRM");
			LogFatal("LOAD_DYNAMIC_OBJECTS DEPRECATED - '" << (LPCTSTR)sctPtr->m_miscString.GetString());
			break;

		case LOAD_ITEM_GENERATOR:
			LoadItemGeneratedData(sctPtr->m_miscString.GetString());
			break;

		default:
			return ClientEngine::ProcessScriptAttribute(sctPtr);
	}

	return true;
}

void CKEPEditView::OnEditorToolsManageTextureDB() {
}

void CKEPEditView::PlaceFirstActor() {
	HWND treeHwnd = NULL;
	m_entityDlgBar.GetDlgItem(IDC_TREE_ENTITIES, &treeHwnd);

	// get the first actor if there is one
	HTREEITEM firstItem = TreeView_GetRoot(treeHwnd);
	if (firstItem) {
		// select the item in the tree
		if (TreeView_SelectItem(treeHwnd, firstItem)) {
			// show the dialog
			OnBtnEntityActivate();
		}
	}
}

void CKEPEditView::OnEditorPlaceFirstActor() {
	PlaceFirstActor();
}

void CKEPEditView::OnEditorToggleWireframe() {
	ToggleWireframe();
}

void CKEPEditView::OnEditorToggleLockGameWindow() {
	ToggleLockGameWindow();
}

void CKEPEditView::GetPlatformInstallLocation(CStringA &platformInstallLocation) {
	CStringW errMsg;
	CStringW errMsgRegKey;
	CStringW successMsg;

	CStringW starRegKey = L"SOFTWARE\\Kaneva\\STAR";

	HKEY hKey = NULL;
	DWORD type;
	DWORD bufsize;
	wchar_t buf[_MAX_PATH];

	platformInstallLocation.Empty();

	if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, starRegKey, 0, KEY_READ | KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS) {
		bufsize = sizeof(buf);
		if (::RegQueryValueExW(hKey, L"InstallDir", 0, &type, reinterpret_cast<LPBYTE>(buf), &bufsize) == ERROR_SUCCESS && type == REG_SZ) {
			platformInstallLocation = Utf16ToUtf8(buf).c_str();
		} else {
			errMsgRegKey = L"HKLM\\";
			errMsgRegKey += starRegKey;
			errMsg.Format(IDS_PUBLISH_STAR_INSTALL_REGKEY_NOT_FOUND, L"InstallDir", errMsgRegKey);
			AfxMessageBox(errMsg, MB_ICONSTOP);
			return;
		}
	} else {
		errMsgRegKey = L"HKLM\\";
		errMsgRegKey += starRegKey;
		errMsg.Format(IDS_PUBLISH_STAR_INSTALL_REG_NOT_FOUND, errMsgRegKey);
		AfxMessageBox(errMsg, MB_ICONSTOP);
		return;
	}
}

///---------------------------------------------------------
// CKEPEditView::PublishGame
//
// Invoked from the file menu to construct a server setup installation file
// from an NSIS script.
//
// [Returns]
//      None
///---------------------------------------------------------
// CKEPEditView::PublishGame
//
// [Returns]
//      None

void CKEPEditView::OnGamePublish() {
	// CommencePublishProcess();
}

bool CKEPEditView::GetPublishFolder(CStringA &pubFolder, CStringA accountXML, CStringA &folderGameId) {
	bool ret = false;
	bool openInProgFilesPath = true;

	bool isDir;

	wchar_t progFilesPath[_MAX_PATH];

	if (!SHGetSpecialFolderPathW(AfxGetMainWnd()->m_hWnd, progFilesPath, 0x0026, FALSE)) {
		openInProgFilesPath = false;
	}

	CStringW title;
	title.LoadString(IDS_SELECT_FULL_DIST);

	BROWSEINFOW bi;
	memset(&bi, 0, sizeof(bi));
	wchar_t destDisplay[_MAX_PATH] = { 0 };
	bi.hwndOwner = m_hWnd;
	bi.lpszTitle = title;
	bi.ulFlags = BIF_NEWDIALOGSTYLE;
	bi.pszDisplayName = destDisplay;

	if (openInProgFilesPath) {
		bi.lpfn = BrowseCallbackProc;
		bi.lParam = (long)progFilesPath;
	}

	wchar_t destDirW[_MAX_PATH];

	do {
		LPITEMIDLIST idlist = ::SHBrowseForFolder(&bi);

		if (idlist == NULL || !SHGetPathFromIDListW(idlist, destDirW))
			return false; // canceled

		std::string destDir = Utf16ToUtf8(destDirW);
		char key[ENCRYPT_KEY_LEN + 1];

		if ((jsFileExists(PathAdd(destDir, CLIENT_FILE_LOCATION).c_str(), &isDir) && isDir) &&
			(jsFileExists(PathAdd(destDir, SERVER_FILE_LOCATION).c_str(), &isDir) && isDir) &&
			(jsFileExists(PathAdd(destDir, PATCH_FILE_LOCATION).c_str(), &isDir) && isDir)) {
			CStringA gameId;

			gameId = GetGameId(PathAdd(destDir, CLIENT_FILE_LOCATION).c_str());

			if (gameId.IsEmpty()) {
				CStringW errMsg;
				errMsg.Format(IDS_PUBLISH_FOLDER_SELECTION_ERROR_NOKEY, Utf8ToUtf16(destDir).c_str());
				AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
				continue;
			}

			CDistributionGameSelectionDlg gameSelDlg(accountXML, gameId);
			if (!gameSelDlg.parseAccountInfo()) {
				continue;
			}
			/* no longer have commercial licenses
			else if ( gameSelDlg.comLicense().IsEmpty() )
			{
				// No commercial license for the game
	            CStringW errMsg;
		        errMsg.LoadString(IDS_PUBLISH_NO_COMMERCIAL_LICENSE);
			    AfxMessageBox(errMsg, MB_ICONEXCLAMATION );
				continue;
			}
			*/
		} else {
			CStringW errMsg;
			errMsg.Format(IDS_PUBLISH_FOLDER_SELECTION_ERROR, Utf8ToUtf16(destDir).c_str());
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
		}

	} while (true);

	return ret;
}

CStringA CKEPEditView::GetGameId(CStringA destDir) {
	jsEnumFiles ef(Utf8ToUtf16(PathAdd(destDir, "*.kep")).c_str(), false);
	if (ef.next()) // Should be only one
	{
		Game gameFile;

		CStringW curDir = ef.currentDirName();

		if (curDir.GetAt(curDir.GetLength()) != L'\\') // Shouldn't this be GetLength()-1?
		{
			curDir += L"\\";
		}

		if (gameFile.Load(Utf16ToUtf8(curDir.GetString()).c_str(), Utf16ToUtf8(ef.currentPath()).c_str(), false)) {
			return gameFile.GameId();
		}
	}

	return NULL;
}

void CKEPEditView::OnGameDistribution() {
	OnGameDistribution(0, atol(m_game.ParentId()) != 0);
}

void CKEPEditView::OnGameDistribution(const char *destDirOverride, bool childGame, bool promptForType) {
	CStringA destinationDirectory;
	bool success = DoGameDistribution(destinationDirectory, 0, destDirOverride, childGame, promptForType);
	if (success && m_game.IsSubGame()) {
		// create short cut to create and run last distro
		CreateDistroShortCuts(destinationDirectory);
	}
}

bool CKEPEditView::DoGameDistribution(CStringA &destinationDirectory, const char *cfgFileName, const char *destDirOverride, bool childGame, bool promptForType) {
	//
	// If we're given a cfgFile, try to load it first.
	// If there's a problem, we can fall back to asking
	// the user for information via the UI.
	//
	bool ret = false;

	KEPConfig cfg;

	if (cfgFileName) {
		try {
			cfg.Load(cfgFileName, "Distribution");
		} catch (KEPException *e) {
			LOG4CPLUS_ERROR(m_logger, "Unable to load the automatic distribution configuration file: " << cfgFileName);
			cfgFileName = 0;
			e->Delete();
		}
	}

	CStringA platformInstallLocation;

	GetPlatformInstallLocation(platformInstallLocation);

	if (platformInstallLocation.IsEmpty()) {
		// Already informed of error
		return ret;
	}

	CStringA patchFolderLocation;

	patchFolderLocation = PathAdd(platformInstallLocation, PATCH_FILE_LOCATION).c_str();
	patchFolderLocation = PathAdd(patchFolderLocation, "KanevaPatch").c_str();
	patchFolderLocation += m_game.GameId();

	bool skipExtraEditorDlgs = false;

	CStringA destDir;
	CStringA accountXML;
	CDistributionTargetDlg distTarget(AfxGetApp()->m_pMainWnd, CanExportToDb(m_editorBaseDir.c_str(), m_logger));
	//
	// Either collect the data from the user interactively or from the CFG file
	// and then create the distribution.
	//
	if (cfgFileName == 0) {
		skipExtraEditorDlgs = ::GA("Begin%5F3DApp%5FDistribution", m_editorBaseDir.c_str());

		// collect options from the user if this distribution isn't being controlled from
		// the configuration file via command line option
		if (promptForType) {
			if (distTarget.DoModal() == IDCANCEL) {
				return ret;
			}
		} else {
			distTarget.exportToDb = dbe_exportToDb;
			distTarget.pubSelection = CDistributionTargetDlg::COMPLETE_DISTRO;
			distTarget.LoadVersionInfo();
		}

		// show the database edit
		if (distTarget.exportToDb != dbe_dontExportToDb) {
			if (IDOK != EditDbSettings(AfxGetApp()->m_pMainWnd->m_hWnd, m_editorBaseDir.c_str(), PathBase().c_str(), true)) // true = if config ok, don't show it
				return ret; // they canceled.
		}

		CStringA encryptionKey;
		CStringA gameId;
		CStringA distributionLicense;

		// now get from game
		if (!m_game.GetEncryptionKey(encryptionKey) ||
			!m_game.GetServerKey(distributionLicense)) {
			AfxMessageBox(IDS_KEYDECRYPTION_FAILED, MB_ICONEXCLAMATION);
			return ret;
		}
		gameId = m_game.GameId();

		if (gameId.IsEmpty() || encryptionKey.IsEmpty() || distributionLicense.IsEmpty()) {
			CStringW s;
			s.Format(IDS_BAD_KEP_FILE, Utf8ToUtf16(m_game.FileNameCStr().GetString()).c_str());
			AfxMessageBox(s, MB_ICONEXCLAMATION);
			return ret;
		}

		if (encryptionKey.GetLength() != ENCRYPT_KEY_LEN) {
			AfxMessageBox(IDS_BAD_ENCRYPT_KEY_LEN, MB_ICONEXCLAMATION);
			return ret;
		}

		// get dest folder
		if (distTarget.pubSelection == CDistributionTargetDlg::COMPLETE_DISTRO) {
			if (distTarget.exportToDb != dbe_onlyExportToDb) {
				if (destDirOverride != 0 && FileHelper::Exists(destDirOverride)) {
					destDir = destDirOverride;
				} else {
					destDir = m_game.GetLastDistroFolder();
					GetDistributionDirectory(destDir, patchFolderLocation);
					if (destDir.IsEmpty())
						return ret;
				}
			}
		}

		bool distroOk = false;

		if (distTarget.pubSelection == CDistributionTargetDlg::COMPLETE_DISTRO) {
			m_creatingDistribution = true;
			if (!destDir.IsEmpty() || distTarget.exportToDb == dbe_onlyExportToDb) {
				CDistributionUpdateDlg distDlg(AfxGetApp()->m_pMainWnd, this, encryptionKey,
					gameId, distributionLicense,
					destDir,
					patchFolderLocation,
					distTarget.exportToDb,
					distTarget.tristripAssets != FALSE,
					distTarget.m_oldCRCList,
					distTarget.m_oldFileCount,
					distTarget.encryptAssets != FALSE,
					childGame || distTarget.disableDialogs != FALSE,
					m_game.IsSubGame());

				skipExtraEditorDlgs = ::GA("Performing%5F3DApp%5FDistribution", m_editorBaseDir.c_str());

				distroOk = distDlg.DoModal() == IDOK;
			}

			if (distroOk) {
				m_game.SetLastDistroFolder(destDir);
				// set the folder
				m_game.Save();
				destinationDirectory = destDir;
				ret = true;
				skipExtraEditorDlgs = ::GA("Complete%5F3DApp%5FDistribution", m_editorBaseDir.c_str());
			}
			m_creatingDistribution = false;

			skipExtraEditorDlgs = ::GA("Run%5F3DApp%5FServer%5FExplorer", m_editorBaseDir.c_str());

			bool runClient = false, runServer = false;

			if (!distTarget.disableDialogs &&
				distroOk) {
				bool okToRun = false;
				if (skipExtraEditorDlgs) {
					runClient = true;
					runServer = true;
					okToRun = true;
				} else {
					okToRun = CRunGameDlg::ShouldRun(destDir, IDS_DISTRO_LAUNCH_PROMPT, this, runClient, runServer);
				}

				if (okToRun) {
					RunClientAndServer(runClient ? PathAdd(destDir, CLIENT_FILE_LOCATION).c_str() : 0,
						runServer ? PathAdd(destDir, SERVER_FILE_LOCATION).c_str() : 0);
				}
			}
		}
	} else {
		//
		// The distribution is being done using a configuration file
		// and a command line option that tells us to which config file
		// to use.
		//

		std::string ddir;
		if (!cfg.GetValue("Destination", ddir))
			LOG4CPLUS_INFO(m_logger, "Destination undefined. This means a local distribution can't be done.");

		CStringA destDir(ddir.c_str());

		EDBExport exportToDb = dbe_dontExportToDb;
		std::string retu;
		cfg.GetValue("ExportToDB", retu, "No");
		CStringA val(retu.c_str());
		if (val.CompareNoCase("Only") == 0) {
			exportToDb = dbe_onlyExportToDb;
		} else if (val.CompareNoCase("Also") == 0) {
			exportToDb = dbe_exportToDb;
		} else if (val.CompareNoCase("No") == 0) {
			exportToDb = dbe_dontExportToDb;
		} else {
			LOG4CPLUS_INFO(m_logger, "ExportToDB undefined, assuming \"No\"");
		}

		std::string vi;
		if (!cfg.GetValue("VersionInfo", vi))
			LOG4CPLUS_INFO(m_logger, "VersionInfo undefined.");

		CStringA fileName(vi.c_str());
		//
		// Use the existing code in CDistributionTargetDlg to set m_oldCRCList
		// and m_oldFileCount.
		//
		distTarget.ProcessVersionInfo(fileName);
		//
		// In the automatic distribution mode, we don't give the user
		// the chance to modify the values that are already stored in
		// db.cfg, so there is no need to call EditDbSettings().
		//
		LOG4CPLUS_INFO(m_logger, "The database settings in db.cfg will be used without modification, when running in automatic distribution mode.");
		//
		// Currently, local distributions are the only ones available.  If that changes in the future,
		// I'll need to add code into here to take the information (user names, passwords or whatever)
		// that are needed for a remote distribution.
		//
		//if ( distTarget.pubSelection != CDistributionTargetDlg::LOCAL ) // Kaneva
		if (false) {
			//
			// Can't get here at all at the moment.
			//
			ASSERT(!"Not Implemented!");
			LOG4CPLUS_ERROR(m_logger, "The non-local case hasn't been coded for; do it manually and see Steve about coding the automated version.");
		} else {
			CStringA overrideKey;
			if (exportToDb != dbe_onlyExportToDb) {
				GetDistributionDirectory(destDir, patchFolderLocation, true); // This has some UI, so need to see if it's only used on error
			}
			if (!destDir.IsEmpty() || exportToDb == dbe_onlyExportToDb) {
				bool triStripAssets;
				bool encryptAssets;
				bool disableDialogs;
				//
				// Collect some general information before creating the distribution.
				//
				log4cplus::tstring str = m_logger.getName();

				if (!cfg.GetValue("TriStrip", triStripAssets, false))
					LOG4CPLUS_INFO(m_logger, "TriStrip undefined, assuming \"false\"");

				if (!cfg.GetValue("EncryptAssets", encryptAssets, false))
					LOG4CPLUS_INFO(m_logger, "EncryptAssets undefined, assuming \"false\"");

				if (!cfg.GetValue("DisableDialogs", disableDialogs, false))
					LOG4CPLUS_INFO(m_logger, "DisableDialogs undefined, assuming \"false\"");

				m_creatingDistribution = true;
				CDistributionUpdateDlg distDlg(AfxGetApp()->m_pMainWnd,
					this,
					overrideKey,
					m_game.GameId(),
					0,
					destDir,
					patchFolderLocation,
					exportToDb,
					triStripAssets,
					distTarget.m_oldCRCList,
					distTarget.m_oldFileCount,
					encryptAssets,
					disableDialogs,
					m_game.IsSubGame());
				distDlg.DoModal();
				m_creatingDistribution = false;
				destinationDirectory = destDir;
				ret = true;
			}
		}
	}

	return ret;
}

// run both the client and server from the base folders specified
void CKEPEditView::RunClientAndServer(const char *clientFolder, const char *serverFolder) {
	STARTUPINFOW si;
	PROCESS_INFORMATION pi;
	CStringA s;

	if (serverFolder && strlen(serverFolder) > 0) {
		// server first
		CStringA s;
		s.LoadString(IDS_SERVER_SERVICENAME);
		CStringA serverExe = "\"";
		serverExe += PathAdd(EditorBaseDir().c_str(), s + "\" -r ").c_str();

		// We need to put quotes around the serverFolder incase their are spaces in the path.
		CStringA strTemp = serverFolder;
		// Add a " at the beginning of the serverFolder path.
		strTemp.Insert(0, '\"');
		// We need to remove the final \ in the path if there is one as this will crash the
		// server instance. Finally, add a " at the end of the serverFolder path.
		if (strTemp.GetAt(strTemp.GetLength() - 1) == '\\') {
			strTemp.SetAt(strTemp.GetLength() - 1, '\"');
		} else {
			strTemp.Insert(strTemp.GetLength(), '\"');
		}
		// Now augment the serverExe so that it uses the serverFolder as the working directory.
		serverExe = serverExe + strTemp;

		memset(&si, 0, sizeof(si));
		si.cb = sizeof(si);
		memset(&pi, 0, sizeof(pi));

		string title = loadStr(IDS_SERVER_EXE_TITLE);
		std::wstring titleW = Utf8ToUtf16(title).c_str();
		si.lpTitle = const_cast<wchar_t *>(titleW.c_str()); // API requires non-const pointer, but should not modify it.

		if (KEP::CreateProcess(
				NULL,
				const_cast<LPSTR>(serverExe.GetString()), // win32 modifies this!
				NULL,
				NULL,
				FALSE,
				0,
				NULL,
				static_cast<LPCSTR>(EditorBaseDir().c_str()),
				&si,
				&pi)) {
			CloseHandle(pi.hThread);
			CloseHandle(pi.hProcess);
		} else {
			LogError("CreateProcess() FAILED - Server Launch");
		}
	}

	if (clientFolder != 0 && strlen(clientFolder) > 0) {
		if (serverFolder && strlen(serverFolder) > 0) // if launched server, wait for it before launching client
		{
			Timer timer;
			DWORD httpStatus;
			string httpStatusDesc, resultText, url;

			try {
				KEPConfig cfg;
				string path = PathAdd(PathBase(), NETCFG_CFG);
				cfg.Load(path, NETCFG_ROOT);
				url = cfg.Get(CLIENTDEFAULTLOGINURL_TAG, DEFAULT_LOGIN_URL);
			} catch (KEPException *e) {
				string msg = loadStr(IDS_NETWORK_CFG_FAILED);
				AfxMessageBox(Utf8ToUtf16(msg).c_str());
				LOG4CPLUS_WARN(m_logger, msg << ": " << e->m_msg);
				e->Delete();
				return;
			}

			STLStringSubstitute(url, "{0}", "testuser@kaneva.com");
			url += "&goto=u&id=http://";
			url += m_game.GameId();
			HCURSOR prev = SetCursor(::LoadCursor(NULL, IDC_WAIT));
			size_t resultSize = 0;
			while (::GetBrowserPage(url, resultText, resultSize, httpStatus, httpStatusDesc) &&
				   (resultText.empty() || (resultText.at(0) != '0' && resultText.at(0) != '3')) && // 0 = ok, 3 = allow access
				   timer.ElapsedMs() < 360000) // try for 6 minutes
			{
				MSG msg;
				while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
					DispatchMessage(&msg);
				Sleep(100);
				resultText = ""; // GetBrowserPage always appends to it.
			}
			SetCursor(prev);

			if (resultText.empty() || (resultText.at(0) != '0' && resultText.at(0) != '3')) {
				AfxMessageBox(L"Server hasn't completed starting.  Check its output.", MB_ICONINFORMATION);
				LOG4CPLUS_WARN(m_logger, "Bad return from " << url << " is " << resultText);
				return;
			}
		}

		string clientDir(clientFolder);
		string clientCmdLine;

		if (m_game.IsSubGame()) {
			// figure out the folder of the installed parent game, which is in the registry, copied
			// mostly from Patcher's RegisterEdit.cpp
			wchar_t data[_MAX_PATH + 1];
			ULONG datasize = _countof(data);
			HKEY hkey;
			DWORD type;
			if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"Software\\Kaneva", 0, KEY_QUERY_VALUE, &hkey) == ERROR_SUCCESS &&
				RegQueryValueEx(hkey, L"InstallPathSub", NULL, &type, (LPBYTE)data, &datasize) == ERROR_SUCCESS &&
				type == REG_SZ) {
				clientCmdLine = "\"";
				clientCmdLine += Utf16ToUtf8(data).c_str();

				PathAddInPlace(clientCmdLine, "KanevaLauncher.exe\" ");
				clientCmdLine += StpUrl::MakeUrlPrefix(m_game.GameId());
				clientCmdLine += " -p" + m_game.ParentId();
				clientCmdLine += " -dev";
			} else {
				AfxMessageBox(IDS_GET_CLIENT_REG_FAILED, MB_ICONEXCLAMATION);
				return;
			}
		} else {
			clientCmdLine = "\"";
			clientCmdLine = clientDir;

			s.LoadString(IDS_CLIENT_EXE_NAME);
			PathAddInPlace(clientCmdLine, s.GetString());
			clientCmdLine += "\" localhost"; // assume they're using default port for localhost server
		}

		memset(&si, 0, sizeof(si));
		si.cb = sizeof(si);
		memset(&pi, 0, sizeof(pi));

		cout << endl
			 << clientCmdLine.c_str() << endl;

		// client must be run w/cur dir set
		if (KEP::CreateProcess(
				NULL,
				static_cast<LPCSTR>(clientCmdLine.c_str()),
				NULL,
				NULL,
				FALSE,
				0,
				NULL,
				static_cast<LPCSTR>(clientDir.c_str()),
				&si,
				&pi)) {
			::CloseHandle(pi.hThread);
			::CloseHandle(pi.hProcess);
		} else {
			LogError("CreateProcess() FAILED - Launch Client");
		}
	}
}

void CKEPEditView::OnGameRun() {
	bool runClient = false, runServer = false;
	if (CRunGameDlg::ShouldRun(PathBase().c_str(), IDS_CONFIRM_RUN, this, runClient, runServer))
		RunClientAndServer(runClient ? PathBase().c_str() : 0, runServer ? PathBase().c_str() : 0);
}

void CKEPEditView::OnGameRunDistribution() {
	RunSpecificDistribution(m_game.GetLastDistroFolder());
}

void CKEPEditView::OnGameRunSpecificDistribution() {
	bool openInProgFilesPath = true;
	wchar_t progFilesPath[_MAX_PATH];

	if (!SHGetSpecialFolderPathW(AfxGetMainWnd()->m_hWnd, progFilesPath, CSIDL_DRIVES, FALSE)) {
		openInProgFilesPath = false;
	}
	// First Choose a directory.
	BROWSEINFOW bi;
	ZeroMemory(&bi, sizeof(bi));
	//memset( &bi, 0, sizeof( bi ) );
	wchar_t destDisplay[_MAX_PATH];
	bi.hwndOwner = m_hWnd;
	bi.lpszTitle = L"Select Disitribution";
	bi.ulFlags = BIF_NEWDIALOGSTYLE;
	bi.pszDisplayName = destDisplay;

	if (openInProgFilesPath) {
		bi.lpfn = BrowseCallbackProc;
		bi.lParam = (long)progFilesPath;
	}

	wchar_t destDir[_MAX_PATH];

	LPITEMIDLIST idlist = ::SHBrowseForFolderW(&bi);
	if (idlist == NULL || !SHGetPathFromIDList(idlist, destDir)) {
		return;
	}

	RunSpecificDistribution(Utf16ToUtf8(destDir).c_str());
}

void CKEPEditView::RunSpecificDistribution(CStringA strDistroPath) {
	if (strDistroPath.IsEmpty()) {
		AfxMessageBox(IDS_NO_LAST_DISTRO, MB_ICONINFORMATION);
	} else {
		bool isDir;
		CStringA svr(PathAdd(strDistroPath, SERVER_FILE_LOCATION).c_str());
		CStringA clnt(PathAdd(strDistroPath, CLIENT_FILE_LOCATION).c_str());
		if (jsFileExists(svr, &isDir) && isDir &&
			jsFileExists(clnt, &isDir) && isDir) {
			bool runClient = false, runServer = false;
			if (CRunGameDlg::ShouldRun(strDistroPath, IDS_CONFIRM_RUN, this, runClient, runServer))
				RunClientAndServer(runClient ? clnt : 0, runServer ? svr : 0);
		} else {
			CStringW s;
			s.Format(IDS_MISSING_FOLDER, Utf8ToUtf16(clnt).c_str(), Utf8ToUtf16(svr).c_str());
			AfxMessageBox(s, MB_ICONEXCLAMATION);
		}
	}
}

/*
===============================================================================
===============================================================================
*/
CStringA CKEPEditView::ParseData(unsigned int &msg) {
	SHORT shiftState = GetKeyState(VK_SHIFT);
	BOOL capitalOption = FALSE;
	BOOL shiftOn = FALSE;
	if (shiftState != 0 && shiftState != 1) {
		capitalOption = TRUE;
		shiftOn = TRUE;
	} else if (GetKeyState(VK_CAPITAL)) {
		capitalOption = TRUE;
	}

	if ((msg >= 'A') && (msg <= 'Z')) {
		char c;
		if (!capitalOption)
			c = tolower(msg);
		else
			c = msg;
		return CStringA(c);
	}

	switch (msg) {
		case '1':
			if (shiftOn) {
				return "!";
			} else {
				return "1";
			}

		case '2':
			if (shiftOn) {
				return "@";
			} else {
				return "2";
			}

		case '3':
			if (shiftOn) {
				return "#";
			} else {
				return "3";
			}

		case '4':
			if (shiftOn) {
				return "$";
			} else {
				return "4";
			}

		case '5':
			if (shiftOn) {
				return "%";
			} else {
				return "5";
			}

		case '6':
			if (shiftOn) {
				return "^";
			} else {
				return "6";
			}

		case '7':
			if (shiftOn) {
				return "&";
			} else {
				return "7";
			}

		case '8':
			if (shiftOn) {
				return "*";
			} else {
				return "8";
			}

		case '9':
			if (shiftOn) {
				return "(";
			} else {
				return "9";
			}

		case '0':
			if (shiftOn) {
				return ")";
			} else {
				return "0";
			}

		case 189:
			if (shiftOn) {
				return "_";
			} else {
				return "-";
			}

		case 187:
			if (shiftOn) {
				return "+";
			} else {
				return "=";
			}

		case VK_ADD:
			return "+";

		case VK_SUBTRACT:
			return "_";

		case VK_DIVIDE:
			return "/";

		case VK_MULTIPLY:
			return "*";

		case 219:
			if (shiftOn) {
				return "{";
			} else {
				return "[";
			}

		case 221:
			if (shiftOn) {
				return "}";
			} else {
				return "]";
			}

		case 220:
			if (shiftOn) {
				return "|";
			} else {
				return "\\";
			}

		case 186:
			if (shiftOn) {
				return ":";
			} else {
				return ";";
			}

		case 222:
			if (shiftOn) {
				return "\"";
			} else {
				return "'";
			}

		case 188:
			if (shiftOn) {
				return "<";
			} else {
				return ",";
			}

		case 190:
			if (shiftOn) {
				return ">";
			} else {
				return ".";
			}

		case 191:
			if (shiftOn) {
				return "?";
			} else {
				return "/";
			}

		case 192:
			if (shiftOn) {
				return "~";
			} else {
				return "`";
			}

		case 32:
			return " ";
	}

	return "";
}

BOOL CKEPEditView::ReplaceObjectByName(CStringA objectNameBasis, CStringA path) {
	CStringA cmp1 = objectNameBasis;
	cmp1.MakeUpper();
	for (POSITION posLoc = m_pWOL->GetHeadPosition(); posLoc != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLoc);
		CStringA cmp2 = pWO->m_fileNameRef;
		cmp2.MakeUpper();
		if (cmp1 == cmp2) { // compare names
			CEXMeshObj *newMesh;
			if (0 != (newMesh = IAssetsDatabase::Instance()->ImportStaticMesh((const char *)path))) {
				newMesh->InitAfterLoad(g_pD3dDevice);

				if (pWO->m_meshObject) {
					pWO->m_meshObject->SafeDelete();
					delete pWO->m_meshObject;
					pWO->m_meshObject = 0;
				}

				pWO->m_meshObject = newMesh;
				AddTextureToDatabase(pWO->m_meshObject);
				return TRUE;
			}
		} // end compare names
	} // End world object loop

	return FALSE;
}

void CKEPEditView::RenderLoopMessageHandling() {
	if (!GL_dedicatedRenderLoop) {
		MSG Msg;
		if (PeekMessage(&Msg, NULL, 0, 0, PM_NOREMOVE)) {
			CPluginTransfer transfer;
			CStringA formatPathToName, objName;

			if (Msg.message == transfer.m_linkMSG) { // file target system
				BYTE *dataRaw = NULL;
				DWORD sizeRaw = 0;
				transfer.ReceiveMemory(&dataRaw, &sizeRaw);

				DWORD trace = 0;
				while (trace < sizeRaw) { // illiterate
					DWORD strinLength = 0;
					short specialLoadCommand = 0;
					CopyMemory(&strinLength, &dataRaw[trace], sizeof(DWORD));
					trace += sizeof(DWORD);

					char *newPath = new char[strinLength + 1];
					ZeroMemory(newPath, strinLength + 1);
					CopyMemory(newPath, &dataRaw[trace], strinLength - (sizeof(DWORD) + sizeof(short)));
					CopyMemory(&specialLoadCommand, &dataRaw[trace + (strinLength - (sizeof(short) + sizeof(DWORD)))], sizeof(short));

					CWldObject *retBasisLoc = NULL;
					formatPathToName = newPath;
					if (formatPathToName.GetLength() > 0) { // valid path
						CStringA fType = CABFunctionLib::GetFileExt(formatPathToName);
						if (fType.MakeUpper() == "ARW") { // world obj type
							// parse name
							CABFunctionLib::RemoveFileExt(&formatPathToName);

							int trace = formatPathToName.GetLength() - 1;
							objName = "";
							while (trace > 0) {
								char xAdd = formatPathToName.GetAt(trace);
								if (xAdd == '\\') {
									break;
								}

								objName = xAdd + objName;
								trace--;
							}

							if (specialLoadCommand == 0) {
								LoadWorldObject(newPath, &retBasisLoc);
								retBasisLoc->m_fileNameRef = objName;
							} else if (specialLoadCommand == 1) {
								if (!ReplaceObjectByName(objName, newPath)) {
									LoadWorldObject(newPath, &retBasisLoc);
									retBasisLoc->m_fileNameRef = objName;
								}
							}
						} // end world obj type
						else if (fType.MakeUpper() == "ARB") { // Entity obj type
							CMovementObj *mPtr = new CMovementObj("NEW ENT", m_globalTraceNumber++);

							int trace = formatPathToName.GetLength() - 1;
							objName = "";
							while (trace > 0) {
								char xAdd = formatPathToName.GetAt(trace);
								if (xAdd == '\\') {
									break;
								}

								objName = xAdd + objName;
								trace--;
							}

							CStringA folderPath = formatPathToName;
							folderPath.Replace(objName, "");
							kstringVector filenames;
							filenames.push_back(string(objName));
							if (LoadActorFromDir(*mPtr, folderPath, filenames, TRUE)) {
								if (!mPtr->m_stats)
									m_statDatabase->Clone(&mPtr->m_stats);

								InitFXShadersOnSkeletal(mPtr->getSkeleton(), mPtr->m_skeletonConfiguration);

								// Unload All Textures From Memory
								m_pTextureDB->TextureMapUnloadAll();

								AddTreeRoot(Utf8ToUtf16(mPtr->getSkeleton()->m_skeletonName).c_str(), IDC_TREE_ENTITIES, &m_entityDlgBar);

								m_movObjRefModelList->AddTail(mPtr);
								m_entityDlgBar.SetSettingsChanged(TRUE);
							} else {
								CStringW s;
								s.LoadString(IDS_ERROR_SKELETALLOADFAILED);
								AfxMessageBox(s + Utf8ToUtf16(formatPathToName).c_str());
								mPtr->SafeDelete();
								delete mPtr;
							}
						} // end Entity obj type
					} // end valid path

					trace += strinLength - (sizeof(DWORD));
				} // end illiterate

				if (dataRaw) {
					delete[] dataRaw;
				}

				dataRaw = 0;
			} // end file target system
		}
	}

	// get all keystroke messages if requested
	if (!m_editorTextModeOn) { // game text accept on
		// build consol temp txt segments
		MSG Msg;
		MSG MsgMimmic;
		BOOL result = 1;

		BOOL foundOneGoodMsg = FALSE;

		result = PeekMessage(&Msg, NULL, WM_KEYFIRST, WM_KEYFIRST, PM_NOREMOVE);
		if (result != 0) // filter
		{
			CopyMemory(&MsgMimmic, &Msg, sizeof(MSG));
			foundOneGoodMsg = TRUE;
		}
	} // game text accept on
}

void CKEPEditView::ControlsInLoop() {
	if (m_keyboardState.KeyDown(DIK_F4)) {
#ifndef _ClientOutput
		if (!m_placementHotKeyEnabled) {
			return;
		}

		POSITION entPosition = m_movObjList->FindIndex(0);
		if (!entPosition) {
			AfxMessageBox(IDS_MSG_NORUNTIMESPAWNED);
			return;
		}

		CMovementObj *entityPtr = (CMovementObj *)m_movObjList->GetAt(entPosition);

		// LB_GETSELITEMS
		int groupIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
		if (groupIndex == -1) {
			groupIndex = 0;
		} else {
			POSITION wldGroupPos = worldGroupList->FindIndex(groupIndex);
			groupIndex = 0;
			if (wldGroupPos) {
				CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetAt(wldGroupPos);
				groupIndex = wldPtr->m_groupInt;
			}
		}

		for (int loop = 0; loop < m_nodeImporterModule.m_selectionCount; loop++) {
			int selectionIndex;
			selectionIndex = m_libraryReferenceDB->GetReferenceIndexByName(Utf16ToUtf8(m_nodeImporterModule.m_selectionArray[loop]).c_str());
			AddNodeByData(groupIndex, selectionIndex, entityPtr, NULL);
		}

		Sleep(200);
#endif
	}

	ClientEngine::ControlsInLoop();
}

//
// ===============================================================================
//    Fully loads an object
// ===============================================================================
//

#include "AssetFactory.h"
#include "IClientAssetBlade.h"
#include "IAsset.h"
#include "IMesh.h"
//#include "Engine/ClientBlades/wkgmesh/SceneGraphBlade.h"

BOOL CKEPEditView::LoadWorldObject(CStringA fileName, CWldObject **retLoc, BOOL fromUI /*= FALSE*/) {
	// try loading the mesh object
	auto_ptr<AssetTree> assets(IAssetsDatabase::Instance()->ImportStaticMeshes((const char *)fileName, (fromUI ? IMP_GENERAL_WITHUI : IMP_GENERAL_NOUI) | IMP_GENERAL_MULTISUBMESH | IMP_GENERAL_MULTIPART));
	if (!assets.get()) {
		AfxMessageBox(IDS_ERROR_CANNOTLOADMESHTYPE);
		return FALSE;
	}

	BOOL errorFound = FALSE;
	map<CStringA, CWldObject *> loadedWldObjects;
	const CStringA collisionPrefix = "COL$";

	// Load all meshes with associated LODs
	for (AssetTree::pre_order_node_iterator it = assets->pre_order_node_begin(); it != assets->pre_order_node_end(); ++it) {
		// Get root of current sub tree
		AssetTree &node = (*it);

		// Get asset data from current node
		AssetData *pAsset = node.get();

		if (!pAsset->IsMeshLevel())
			continue;

		// Get pointer to parent node
		AssetTree *parentNode = node.parent();
		AssetData *pParent = NULL;
		if (parentNode)
			pParent = parentNode->get();

		//@@Assumption: Always has LOD0 plus always get LOD0 first [Yanfeng 09/2008]
		int LODLevel = pAsset->GetLODLevel();
		if (LODLevel == -1)
			LODLevel = 0;

		for (UINT subLoop = 0; subLoop < pAsset->GetActualSubmeshCount(); subLoop++) {
			CEXMeshObj *pMesh = pAsset->ExtractExMesh(subLoop);
			if (pMesh == NULL) {
				CStringA msg;
				msg.Format("Loading mesh/submesh failed: %s", pAsset->GetURI().c_str());
				AfxMessageBox(Utf8ToUtf16(msg).c_str());

				// Skip this submesh
				continue;
			}

			// Key for LOD matching
			// 1) for rendering meshes: using parent->GetKey postfixed with submesh ID
			//	@@Limitation: LOD meshes must have same number of submeshes as base mesh (LOD0).
			// 2) for collision meshes: using parent->GetKey prefixed with "COL$" and postfixed with submesh ID
			CStringA key = "", prefix = "";
			if (LODLevel == AssetData::LOD_COL)
				prefix = collisionPrefix;
			if (pParent)
				key.Format("%s%s[%d]", prefix, pParent->GetKey().c_str(), subLoop);
			else
				key.Format("%s[%d]", prefix, subLoop);

			if (LODLevel == AssetData::LOD0 || LODLevel == AssetData::LOD_COL) {
				// Basic mesh or Collision mesh

				// Create a new world object and insert it into loadedWldObjects
				CWldObject *wldObj = new CWldObject(m_globalTraceNumber++);
				wldObj->SetMesh(pMesh);

				if (pAsset->GetActualSubmeshCount() > 1)
					wldObj->m_fileNameRef.Format("%s[%d]", pAsset->GetName().c_str(), subLoop);
				else
					wldObj->m_fileNameRef = pAsset->GetName().c_str();

				loadedWldObjects.insert(pair<CStringA, CWldObject *>(key, wldObj));
			} else if (LODLevel <= AssetData::LOD_MAX) {
				// Other LODs

				// LOD0 must already be read
				map<CStringA, CWldObject *>::iterator it = loadedWldObjects.find(key);
				if (it != loadedWldObjects.end()) {
					// Grab the previously constructed CWldObject
					CWldObject *wldObj = it->second;

					// Inject LOD into CWldObject
					AddLODToWorldObject(wldObj, pMesh, 0, LODLevel * 25, NULL); //@@Watch: hard-coded default value
				} else {
					// otherwise, we have to discard this mesh, this isn't supposed to happen.
					//@@Todo: create a solution to deal with missing LOD0
					jsAssert(FALSE);
					delete pMesh;
				}
			}
		}
	}

	CWldGroupObj *selGroup = GetSelectedGroup();
	CWldGroupObj *colGroup = NULL;
	int firstLoadedIdentity = -1;

	for (map<CStringA, CWldObject *>::iterator it = loadedWldObjects.begin(); it != loadedWldObjects.end(); ++it) {
		CWldObject *pWO = it->second;

		// Check if this is a collision mesh
		BOOL bIsCollision = FALSE;
		if (it->first.Left(collisionPrefix.GetLength()) == collisionPrefix)
			bIsCollision = TRUE;

		// More initializations
		pWO->m_triggerList = new CTriggerObjectList;

		if (InitializeWorldObject(pWO)) {
			m_pWOL->AddTail(pWO);

			pWO->m_popoutRadius = pWO->m_meshObject->GetBoundingSphereRadiusSqr();

			float sightRange = pWO->m_meshObject->m_sightBasis;
			pWO->m_popoutBox.max.x = ((sightRange + 1) + pWO->m_meshObject->GetMeshCenter().x);
			pWO->m_popoutBox.max.y = ((sightRange + 2) + pWO->m_meshObject->GetMeshCenter().y);
			pWO->m_popoutBox.max.z = ((sightRange + 3) + pWO->m_meshObject->GetMeshCenter().z);
			pWO->m_popoutBox.min.x = -(sightRange + 4) + pWO->m_meshObject->GetMeshCenter().x;
			pWO->m_popoutBox.min.y = -(sightRange + 5) + pWO->m_meshObject->GetMeshCenter().y;
			pWO->m_popoutBox.min.z = -(sightRange + 6) + pWO->m_meshObject->GetMeshCenter().z;

			if (pWO->m_meshObject->m_materialObject->m_paramCount > 0) {
				pWO->m_meshObject->m_materialObject->m_pixelShader =
					m_pxShaderSystem->LoadShaderByStringData(pWO->m_meshObject->m_materialObject->m_fxBindString,
						g_pD3dDevice, pWO->m_meshObject->m_materialObject->m_fxBindString);
			}

			*retLoc = pWO;

			firstLoadedIdentity = pWO->m_identity;

			if (!bIsCollision) // is this a collision mesh?
			{
				// NO: Insert into selected mesh group.
				pWO->m_groupNumber = selGroup ? selGroup->m_groupInt : 0;
			} else {
				// YES: Insert into default collision group

				// Obtain default collision group, create if not already exists
				if (colGroup == NULL)
					colGroup = GetDefaultCollisionGroup();
				ASSERT(colGroup != NULL);

				// Use selected render group if for any reason collision group cannot be created
				if (colGroup == NULL)
					colGroup = selGroup;

				// Set appropriate group number
				pWO->m_groupNumber = colGroup ? colGroup->m_groupInt : 0;

				// treat newly inserted world object as collision object
				pWO->m_renderFilter = TRUE;
				pWO->m_collisionInfoFilter = FALSE;
				//wldObj->m_collisionType = ??? // What is m_collisionType, seems it's not needed
			}
		} else {
			pWO->SafeDelete();
			delete pWO;
			errorFound = TRUE;
		}
	}

	// Update tree view to reflect the newly added item(s)
	if (selGroup && firstLoadedIdentity != -1)
		ObjectTreeViewInlist(selGroup->m_groupInt, firstLoadedIdentity);

	return !errorFound;
}

// End Function

//! GetNameOnly: Get the local file name without the rest of the path
void GetNameOnly(const string &aFileName, string &aNameOnly) {
	int nameBegin = 0;
	if ((nameBegin = aFileName.rfind("\\")) == 0)
		nameBegin = aFileName.rfind("//");

	aNameOnly = aFileName.substr(nameBegin);
}

//! GetSelectedObjectGroup: Get the group currently selected in the zone objects dialog
CWldGroupObj *CKEPEditView::GetSelectedGroup() {
	int groupIndex = m_dlgBarObject.SendDlgItemMessage(IDC_LIST_OBJECTS_GROUPLIST2BOX, LB_GETCURSEL, 0, 0);
	if (groupIndex == -1) {
		groupIndex = 0;
	} else {
		POSITION posLoc = worldGroupList->FindIndex(groupIndex);
		groupIndex = 0;
		if (posLoc) {
			CWldGroupObj *wldPtr = (CWldGroupObj *)worldGroupList->GetAt(posLoc);
			return wldPtr;
		}
	}
	return 0;
}

//!**************************************************************************
//! /brief Compose a string for identifying and displaying zone objects in the tree.
//! /param objectName Name of the world object or reference object.
//! /param subID ID of sub-mesh for objects with material-based multiple sub-meshes. Supply -1 if single material.
//! /param instanceNo Instance number for reference objects. Supply -1 if world object.
//! /return Composed ID string to identify a zone object.
//!
//! Current implementation uses following naming convention:
//! ZoneObjectID = [instanceNo]objectName[subID]
CStringA CKEPEditView::MakeZoneObjectID(const CStringA &objectName, int subID /*= -1*/, int instanceNo /*= -1*/) {
	CStringA IDStr, formatStr = "%s";
	if (subID != -1)
		formatStr += "[%d]";

	if (instanceNo != -1) {
		formatStr = "[%d]" + formatStr; // [instanceNo]objectName[optional subID]
		IDStr.Format(formatStr, instanceNo, objectName, subID);
	} else {
		IDStr.Format(formatStr, objectName, subID); // objectName[optional subID]
	}

	return IDStr;
}

//!**************************************************************************
//! /brief Parse the identification string created by MakeZoneWorldObjectID()
//! /param objectName Name of the world object or reference object.
//! /param subID ID of sub-mesh for objects with material-based multiple sub-meshes. Return -1 if single material.
//! /param instanceNo Instance number for reference objects. Return -1 if world object.
//!
//! Current implementation uses following naming convention:
//! ZoneObjectID = [instanceNo]objectName[subID]
void CKEPEditView::ParseZoneObjectID(const CStringA &IDStr, CStringA &objectName, int &subID, int &instanceNo) {
	// Initialize with default values
	subID = -1;

	// Parse instance number first
	ParseZoneObjectID(IDStr, objectName, instanceNo);

	// Boundary check
	if (objectName.IsEmpty())
		return;

	if (objectName.Right(1) == ']') {
		// Sub-mesh ID found
		int posLB = objectName.ReverseFind('[');
		if (posLB != -1) {
			// Parse sub-mesh ID
			CStringA subIDStr = objectName.Mid(posLB + 1, objectName.GetLength() - posLB - 2);
			subID = atoi(subIDStr);

			// Strip sub-mesh ID from object name
			objectName = objectName.Left(posLB);
		}
	}
}

//!**************************************************************************
//! /brief Parse the identification string created by MakeZoneWorldObjectID() - subID is not parsed in this function. It's returned as part of the objectName.
//! /param objectName Name of the world object or reference object.
//! /param instanceNo Instance number for reference objects. Return -1 if world object.
void CKEPEditView::ParseZoneObjectID(const CStringA &IDStr, CStringA &objectName, int &instanceNo) {
	// Initialize with default values
	objectName = IDStr;
	objectName.Trim();
	instanceNo = -1;

	// Boundary check
	if (objectName.IsEmpty())
		return;

	if (objectName[0] == '[') {
		// Instance number found
		int posRB = objectName.Find(']'); // locate right bracket
		if (posRB != -1) {
			// Parse instance number
			CStringA instNoStr = objectName.Mid(1, posRB - 1);
			instanceNo = atoi(instNoStr);

			// Strip instance number from object name
			objectName = objectName.Mid(posRB + 1, objectName.GetLength() - posRB - 2);
		}
	}
}

//!********************************************************************
//! \brief Return pointer to default collision group.
//! \return Pointer to default collision group (CWldGroupObj*).
//!
//! This function will iterate thru all existing world object groups and find the default
//! collision group (where all collision meshes specified with LOD "C" will be automatically
//! imported). If default collision group does not already exist, create and return it.
CWldGroupObj *CKEPEditView::GetDefaultCollisionGroup() {
	static char *defaultCollisionGroupName = "AltCOL Auto-Import";

	CWldGroupObj *pDefaultCollisionGroup = NULL;

	for (POSITION pos = worldGroupList->GetHeadPosition(); pos != NULL;) {
		CWldGroupObj *pTmpGroup = dynamic_cast<CWldGroupObj *>(worldGroupList->GetNext(pos));
		if (pTmpGroup->m_groupName.Compare(defaultCollisionGroupName) == 0) {
			pDefaultCollisionGroup = pTmpGroup;
			break;
		}
	}

	if (pDefaultCollisionGroup == NULL) {
		pDefaultCollisionGroup = new CWldGroupObj(defaultCollisionGroupName, GetNextAvailableWldGroupID());
		worldGroupList->AddTail(pDefaultCollisionGroup);
		WorldGroupInList(-1);
	}

	return pDefaultCollisionGroup;
}

//!********************************************************************
//! \brief Return the next available world object group ID.
//! \return available world object group ID to be assigned to a new group.
int CKEPEditView::GetNextAvailableWldGroupID() {
	int maxGroupInt = -1;
	for (POSITION wPos = worldGroupList->GetHeadPosition(); wPos != NULL;) {
		CWldGroupObj *groupPtr = (CWldGroupObj *)worldGroupList->GetNext(wPos);
		if (groupPtr->m_groupInt > maxGroupInt)
			maxGroupInt = groupPtr->m_groupInt;
	}

	return maxGroupInt + 1;
}

/*
    TODO illamas: handle this
    CStringA ext = CABLIB->GetFileExt ( fileName );
    ext.MakeLower ();

    bool found = false;
    float sightRange = 0.0f;
    if(ext == "arw")
    {
        found = true;
        ptrLoc->m_meshObject = IAssetsDatabase::Instance()->ImportStaticMesh ( fileName, &sightRange, g_pd3dDevice );   // proprietory file format
    }
    else if(ext == "ksg")
    {
        IClientBlade *b = ClientBladeFactory::Instance()->GetBladeByName("SceneGraphBlade");
        SceneGraphBlade *sgb = dynamic_cast<SceneGraphBlade *>(b);
        if(sgb)
        {
            sgb->Load((const char *)fileName);
        }

        return FALSE;
    }
    else
    {
        // see if one of our asset creators knows how to create this mesh
        int32 count = ClientBladeFactory::Instance()->GetNumBladesOfType(typeid(KEP::IClientAssetBlade));
        for(int32 i = 0; i < count; i++)
        {
            KEP::IClientBlade *blade = ClientBladeFactory::Instance()->GetBladeOfType(typeid(KEP::IAsset), i);
            KEP::IClientAssetBlade *asset_creator = (KEP::IClientAssetBlade *)blade;

            for(int32 e = 0; e < asset_creator->GetNumExtensions(); e++)
            {
                if(!stricmp(ext, asset_creator->GetExtension(e)))
                {
                    // found it
                    found = true;

                    // allow the asset factory to get it for us
                    KEP::IAsset *asset = asset_creator->operator ()((std::string((const char *)fileName)));
                    KEP::IMesh *kep_mesh = dynamic_cast<KEP::IMesh *>(asset);
                    ASSERT(kep_mesh);

                    ptrLoc->m_meshObject->blade_mesh = kep_mesh;

                    IClientBlade *b = ClientBladeFactory::Instance()->GetBladeByName("SceneGraphBlade");
                    SceneGraphBlade *sgb = dynamic_cast<SceneGraphBlade *>(b);
                    if(sgb)
                    {
                        sgb->AddMesh(kep_mesh);
                    }

                    break;
                }
            }
        }
    }

*/

class EditorDoNothingSendHandler : public ISendHandler {
public:
	EditorDoNothingSendHandler() {}

protected:
	virtual EVENT_PROC_RC SendEventTo(IEvent *e) { return EPR_OK; }
};

bool CKEPEditView::InitEvents() {
	bool ret = ClientEngine::InitEvents();
	m_dispatcher.SetSendHandler(new EditorDoNothingSendHandler()); // override the handler with our own
	m_dispatcher.RegisterHandler(CKEPEditView::ClickEventHandler, (ULONG)this, "ClickEvent", PackVersion(1, 0, 0, 0), m_clickEventId, EP_NORMAL);
	m_dispatcher.RegisterHandler(CKEPEditView::SelectEventHandler, (ULONG)this, "SelectEvent", PackVersion(1, 0, 0, 0), m_selectEventId, EP_NORMAL);
	m_dispatcher.RegisterHandler(CKEPEditView::WorldObjectSelectedHandler, (ULONG)this, "WorldObjectSelectedEvent", PackVersion(1, 0, 0, 0), m_worldObjectSelectedEventId, EP_NORMAL);

	return ret;
}

ULONG CKEPEditView::GetArrayCount(ULONG indexOfArray) {
	initGetSet();

	if (indexOfArray == KEPEDITVIEWIDS_TITLE) {
		if (m_skillDatabase) {
			if (m_skillDatabase->m_titles) {
				return m_skillDatabase->m_titles->GetCount();
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	} else {
		return GetSet::GetArrayCount(indexOfArray);
	}
}

IGetSet *CKEPEditView::GetObjectInArray(ULONG indexOfArray, ULONG indexInArray) {
	initGetSet();

	if (indexOfArray == KEPEDITVIEWIDS_TITLE) {
		if (m_skillDatabase) {
			if (m_skillDatabase->m_titles) {
				return m_skillDatabase->m_titles->GetObjByIndex(indexInArray);
			} else {
				return NULL;
			}
		} else {
			return NULL;
		}
	} else if (indexOfArray == KEPEDITVIEWIDS_DYNAMICOBJECTS) {
		if (m_dynamicObjectRM) {
			return m_dynamicObjectRM->getDynamicObjectByGLID(indexInArray);
		} else {
			return NULL;
		}
	} else {
		return GetSet::GetObjectInArray(indexOfArray, indexInArray);
	}
}

BEGIN_GETSET_IMPL(CKEPEditView, IDS_ENGINE_NAME)
GETSET_OBLIST_PTR(m_statDatabase, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, STATS)
GETSET_OBLIST_PTR(m_skillDatabase, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, SKILLS)
GETSET_CUSTOM(NULL, gsObjectPtr, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, TITLE, 0, 0)
GETSET2_OBJ_PTR(m_multiplayerObj, GS_FLAGS_DEFAULT, 0, 0, IDS_SERVERENGINE_MULTIPLAYER, IDS_SERVERENGINE_MULTIPLAYER_HELP, CMultiplayerObj)
GETSET_OBLIST_PTR((m_altGlobalInventory), GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, INVENTORY)
GETSET_OBLIST_PTR(m_globalInventoryDB, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, INVENTORY)
GETSET_OBLIST_PTR(m_serverItemGenerator, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMGENOBJ, GENNAME)
GETSET2_OBLIST_PTR(m_commerceDB, GS_FLAGS_DEFAULT, 0, 0, IDS_ENGINE_COMMERCEDB, IDS_ENGINE_COMMERCEDB)
GETSET_CUSTOM(NULL, gsObjectPtr, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, DYNAMICOBJECTS, 0, 0)
GETSET2_PTR(&m_game, GS_FLAGS_DEFAULT, 0, 0, IDS_GAMECLASS_NAME, IDS_GAMECLASS_NAME)
END_GETSET_IMPL

CStringA GetFileExt(CStringA currentName) {
	CStringA ext;
	int length = currentName.GetLength();
	if (length == 0)
		return "";

	BOOL done = FALSE;
	while (done == FALSE) {
		CStringA chr = (CStringA)currentName.GetAt(length - 1);

		if (chr == ".")
			break;

		ext = operator+(chr, ext);
		length = length - 1;

		if (length == 0)
			break;
	}
	return ext;
}

BOOL CKEPEditView::LoadActorFromDir(CMovementObj &actor, const CStringA &directory, const std::vector<string> &fileNames, BOOL resetBaseSkeleton) {
	WIN32_FIND_DATA FindData;
	HANDLE FileHandle;
	DWORD Attrib;
	CStringA name;
	if (resetBaseSkeleton) {
		if (fileNames.empty())
			return FALSE; // Nothing to import

		vector<string> fullFileNames;
		for (UINT i = 0; i < fileNames.size(); i++)
			fullFileNames.push_back(string(directory) + fileNames[i]);

		CSkeletonObject *tmpSkeleton = 0;
		if (0 == (tmpSkeleton = IAssetsDatabase::Instance()->ImportSkeletalObject(fullFileNames)))
			return FALSE;

		if (!actor.getMovementCaps())
			actor.setMovementCaps(new CMovementCaps());

		if (actor.getSkeleton()) {
			actor.getSkeleton()->SafeDelete();
			delete actor.m_skeletonConfiguration;
		}
		actor.setSkeleton(tmpSkeleton->getRuntimeSkeleton());
		actor.m_skeletonConfiguration = tmpSkeleton->getSkeletonConfiguration();
	}
	CStringA cfgFileToApply = "";
	//first initialize skeleton m_skeletalObject

	// illamas: this is to avoid loading ARBs and ALT files from a directory when the
	//  data has been loaded from some other format, such as Ogre XML
	// We may want to rethink a bit how the whole character loading thing should work,
	//  define some new file formats perhaps to contain lists of files to make define an
	//  entire character (files for meshes, files for animations, LODs, sections, etc)
	//  instead of this hardcoded way of looking for all the file in a directory...
	bool loadingARB = false;
	if (fileNames.size() > 0) {
		string firstFile = fileNames[0];
		STLToUpper(firstFile);
		if (firstFile.rfind(".ARB") != string::npos)
			loadingARB = true;
		if (firstFile.rfind(".KAO") != string::npos)
			loadingARB = true;
	}

	FileHandle = FindFirstFileW(Utf8ToUtf16(directory + "*.*").c_str(), &FindData);
	do {
		Attrib = FindData.dwFileAttributes;
		if (Attrib != 16) { //if valid file
			name = Utf16ToUtf8(FindData.cFileName).c_str();
			CStringA ext = GetFileExt(name);
			name.MakeUpper();

			if (ext.MakeUpper() == "CFG") { //Config file
				cfgFileToApply = (directory + name);
			} //end config file
			else if (ext.MakeUpper() == "MDB") {
				CFileException exc;
				CFile fl;
				CArchive the_Archive(&fl, CArchive::load);
				BOOL res = fl.Open(Utf8ToUtf16(directory + name).c_str(), CFile::modeRead, &exc);
				if (res) {
					BEGIN_SERIALIZE_TRY

					actor.deleteMovementCaps();

					CMovementCaps *pMovementDB = nullptr;
					the_Archive >> pMovementDB;

					actor.setMovementCaps(pMovementDB);

					the_Archive.Close();
					fl.Close();
					END_SERIALIZE_TRY_NO_LOGGER

					if (!serializeOk) {
						//								AfxMessageBox( IDS_ERROR_FAILLOAD_MDB+" "+name );
					}
				} else
					LOG_EXCEPTION_NO_LOGGER(exc)
			} else if (ext.MakeUpper() == "ALT" && loadingARB) {
				CSkeletonObject *pTmpSkeleton = NULL;
				IAssetsDatabase::Instance()->ImportSkeletalObject(directory + name, IMP_SKELETAL_LEGACY_FLAGS, pTmpSkeleton);
				if (pTmpSkeleton) {
					actor.m_skeletonConfiguration->MergeAsAltCfg(pTmpSkeleton->getSkeletonConfiguration());
					pTmpSkeleton->SafeDelete();
				} else {
					CStringW s;
					//					s.LoadString(IDS_ERROR_FAILLOADALTCFGFILE);
					AfxMessageBox(s + Utf8ToUtf16(name).c_str());
				}
			} else if ((ext.MakeUpper() == "ARB" || ext.MakeUpper() == "KAO") && loadingARB) {
				eAnimType animType = CBoneAnimationNaming::AnimNameToType(name);
				if (IS_VALID_ANIM_TYPE(animType)) {
					int animGLID = GLID_INVALID;
					CSkeletonObject *pTmpSkeleton = new CSkeletonObject();
					if (!IAssetsDatabase::Instance()->ImportAnimation(*pTmpSkeleton, (const char *)(directory + name), animGLID, SKELETAL_REPLACE_ANIMATION)) {
						CStringW s;
						AfxMessageBox(s + Utf8ToUtf16(name).c_str());
						break;
					}
					actor.setSkeleton(pTmpSkeleton->getRuntimeSkeleton());
					actor.m_skeletonConfiguration = pTmpSkeleton->getSkeletonConfiguration();
					pTmpSkeleton->getRuntimeSkeleton() = 0;
					pTmpSkeleton->getSkeletonConfiguration() = 0;
					pTmpSkeleton->SafeDelete();
				}
			}
		} //end if valid file
	} while (FindNextFile(FileHandle, &FindData));

	//if cfg file needed load and exec now post build
	if (cfgFileToApply.GetLength() > 0) { //DO CFG
		CFileException exc;
		CFile fl;
		CArchive the_Archive(&fl, CArchive::load);
		BOOL res = fl.Open(Utf8ToUtf16(cfgFileToApply).c_str(), CFile::modeRead, &exc);
		if (res) {
			BEGIN_SERIALIZE_TRY

			CStringA tempBuffer, secondaryBuffer;
			while (ReadString(the_Archive, tempBuffer)) {
				ReadString(the_Archive, secondaryBuffer);

				if (tempBuffer == "First Person Camera") {
					actor.m_camFirstPerson = -1;
					CMovementObj::CameraData cdata = { atoi(secondaryBuffer), 0, -1 };
					actor.m_cameraData.push_back(cdata);
				}

				if (tempBuffer == "Third Person Camera") {
					actor.m_camThirdPerson = -1;

					CMovementObj::CameraData cdata = { atoi(secondaryBuffer), 0, -1 };
					actor.m_cameraData.push_back(cdata);
				}

				if (tempBuffer == "Radius")
					actor.setBoundingRadius((float)atof(secondaryBuffer));

				if (tempBuffer == "Base Name") {
					actor.getSkeleton()->m_skeletonName = secondaryBuffer;
					actor.m_name = secondaryBuffer;
				}

				if (tempBuffer == "Bone Look Lock")
					actor.getSkeleton()->m_freeLookBone = atoi(secondaryBuffer);

				if (tempBuffer == "Bone Look Lock2")
					actor.getSkeleton()->m_freeLookBone2 = atoi(secondaryBuffer);

				if (tempBuffer == "Bone Waist Lock")
					actor.getSkeleton()->m_waistBone = atoi(secondaryBuffer);

				if (tempBuffer == "Unlock Secondary Index") {
					POSITION posLoc = actor.getSkeleton()->m_pBoneList->FindIndex(atoi(secondaryBuffer));
					if (posLoc) {
						CBoneObject *bn = (CBoneObject *)actor.getSkeleton()->m_pBoneList->GetAt(posLoc);
						bn->m_secondaryOverridable = FALSE;
					}
				}

				if (tempBuffer == "Center Of Mass Link")
					actor.getSkeleton()->m_secondaryCOMInheritBone = atoi(secondaryBuffer);

				if (tempBuffer == "Make Public Cfg") {
					if (actor.getSkeleton()->m_baseMeshes > 0)
						if (actor.m_skeletonConfiguration->m_alternateConfigurations) { //alternate exists - this is based off of DB gen
							for (int countTemp = 0; countTemp < actor.getSkeleton()->m_baseMeshes; countTemp++) { //base loop
								POSITION alPos = actor.m_skeletonConfiguration->m_alternateConfigurations->FindIndex(countTemp);
								if (alPos) { //valid configuration
									int traceTemp = 0;
									CAlterUnitDBObject *altPtr = (CAlterUnitDBObject *)actor.m_skeletonConfiguration->m_alternateConfigurations->GetAt(alPos);
									for (POSITION curCfgPos = altPtr->m_configurations->GetHeadPosition(); curCfgPos != NULL;) { //deformable unit same array ie head0 head1
										CDeformableMesh *dMesh = (CDeformableMesh *)altPtr->m_configurations->GetNext(curCfgPos);

										if (atoi(secondaryBuffer) == traceTemp)
											dMesh->m_publicDim = TRUE;

										traceTemp++;
									}
								}
							} //end base loop
						}
				}

				//DPD LOD
				if (tempBuffer == "LOD Distance") {
					if (!actor.m_skeletonLODList)
						actor.m_skeletonLODList = new CSkeletalLODObjectList();

					CSkeletalLODObject *newLODEntity = new CSkeletalLODObject();
					newLODEntity->m_distance = (float)atof(secondaryBuffer);
					actor.m_skeletonLODList->AddTail(newLODEntity);
				}
			}
			the_Archive.Close();
			fl.Close();

			END_SERIALIZE_TRY_NO_LOGGER

			if (!serializeOk)
				return FALSE;
		} else {
			LOG_EXCEPTION_NO_LOGGER(exc)
			return FALSE;
		}
	}

	return TRUE;
}

// where to put our shortcuts, if installer changes, so must this
static const char *PLATFORM_SHORTCUTS = "\\Programs\\Kaneva\\";

static DWORD CreateShortCut(const char *appName,
	const char *name, const char *description,
	const char *exePath, const char *parameters, const char *workingDir) {
	jsVerifyReturn(appName != 0 && name != 0 && description != 0 && exePath != 0 && parameters != 0 && workingDir != 0, E_INVALIDARG);

	HRESULT hr;
	IShellLink *psl = NULL;
	IPersistFile *pPf = NULL;
	std::wstring wstr;
	LPITEMIDLIST pidl = 0;

	hr = CoCreateInstance(CLSID_ShellLink,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IShellLink,
		(LPVOID *)&psl);
	if (FAILED(hr))
		goto cleanup;

	hr = psl->QueryInterface(IID_IPersistFile, (LPVOID *)&pPf);
	if (FAILED(hr))
		goto cleanup;

	hr = psl->SetPath(Utf8ToUtf16(exePath).c_str());
	if (FAILED(hr))
		goto cleanup;

	hr = psl->SetDescription(Utf8ToUtf16(description).c_str());
	if (FAILED(hr))
		goto cleanup;

	hr = psl->SetArguments(Utf8ToUtf16(parameters).c_str());
	if (FAILED(hr))
		goto cleanup;

	hr = psl->SetWorkingDirectory(Utf8ToUtf16(workingDir).c_str());
	if (FAILED(hr))
		goto cleanup;

	//place the shortcut on the desktop
	SHGetSpecialFolderLocation(HWND_DESKTOP, CSIDL_STARTMENU, &pidl);
	wchar_t wbuf[256];
	SHGetPathFromIDListW(pidl, wbuf);
	wstr = wbuf;
	wstr += Utf8ToUtf16(PLATFORM_SHORTCUTS);
	wstr += Utf8ToUtf16(appName);

	SHCreateDirectoryExW(NULL, wstr.c_str(), 0);
	wstr += L"\\";
	wstr += Utf8ToUtf16(name);
	wstr += L".lnk";
	printf("Path is %S\n", wstr.c_str());

	hr = pPf->Save(wstr.c_str(), TRUE);

	if (FAILED(hr)) {
		goto cleanup;
	}

cleanup:
	if (pPf)
		pPf->Release();

	if (psl)
		psl->Release();

	if (pidl)
		CoTaskMemFree(pidl);

	return hr;
}

bool CKEPEditView::CreateDistroShortCuts(CStringA destinationDirectory) {
	wchar_t editor[_MAX_PATH];
	jsVerifyReturn(GetModuleFileNameW(NULL, editor, _countof(editor)) != 0, false);

	wchar_t server[_MAX_PATH];
	wchar_t workingDir[_MAX_PATH];
	wchar_t drive[_MAX_DRIVE];
	wchar_t dir[_MAX_DIR];

	_wsplitpath_s(editor, drive, _countof(drive), dir, _countof(dir), 0, 0, 0, 0);
	_wmakepath_s(server, _countof(server), drive, dir, L"kgpserver", L".exe");
	_wmakepath_s(workingDir, _countof(workingDir), drive, dir, 0, 0);

	string cfgFile(PathAdd(m_game.PathBase(), "Distribution.xml"));

	// create a distro config file
	try {
		/*
		<Distribution>
			<ExportToDB>Also</ExportToDB>
			<TriStrip>1</TriStrip>
			<EncryptAssets>1</EncryptAssets>
			<DisableDialogs>1</DisableDialogs>
			<Destination></Destination>               
		</Distribution>
		*/
		KEPConfig cfg;
		cfg.New("Distribution");
		cfg.SetValue("ExportToDB", "Also");
		cfg.SetValue("TriStrip", 1);
		cfg.SetValue("EncryptAssets", 1);
		cfg.SetValue("DisableDialogs", 1);
		cfg.SetValue("Destination", destinationDirectory);
		cfg.SaveAs(cfgFile.c_str());
	} catch (KEPException *e) {
		AfxMessageBox(Utf8ToUtf16(loadStr(IDS_FAILED_CREATE_DIST_FILE) + e->ToString()).c_str(), MB_ICONEXCLAMATION);
		e->Delete();
	}
	CStringA distroParams;
	distroParams.Format("\"-w%s\" \"-d%s\"", m_game.FileNameCStr(), cfgFile.c_str());

	CStringA serverParams, serviceParams;
	CStringA baseDir(PathAdd(destinationDirectory, "ServerFiles").c_str());
	baseDir.TrimRight("\\/"); // shortcut doesn't like trailing slash on basedir

	serverParams.Format("-r \"%s\"", baseDir);
	serviceParams.Format("-p \"%s\" -au \"3dApp-%s\"", baseDir, m_game.Name());

	// create or update the service for the server
	bool serviceCreated = false;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;

	wchar_t kgpserverexe[_MAX_PATH * 2];
	swprintf_s(kgpserverexe, _countof(kgpserverexe), L"\"%s\" %s", server, Utf8ToUtf16(serviceParams).c_str());
	LOG4CPLUS_DEBUG(m_logger, "Launching service install of " << Utf16ToUtf8(kgpserverexe));
	if (KEP::CreateProcess(
			NULL,
			static_cast<LPCWSTR>(kgpserverexe),
			NULL,
			NULL, // security
			FALSE, // inherit handles,
			CREATE_NO_WINDOW,
			NULL, // env
			static_cast<LPCWSTR>(workingDir),
			&si,
			&pi)) {
		::CloseHandle(pi.hThread);
		if (WaitForSingleObject(pi.hProcess, 100000) == WAIT_OBJECT_0) {
			DWORD exitCode = NO_ERROR;
			if (!GetExitCodeProcess(pi.hProcess, &exitCode))
				exitCode = GetLastError();

			if (exitCode != NO_ERROR) {
				CStringW msg;
				wstring m;
				msg.Format(IDS_REG_SVR_FAIL, errorNumToString(GetLastError(), m));
				AfxMessageBox(msg, MB_ICONEXCLAMATION);
			} else {
				serviceCreated = true;
				LOG4CPLUS_DEBUG(m_logger, "Service created ok");
			}
			::CloseHandle(pi.hProcess);
		} else {
			LogError("WaitForSingleObject() FAILED - Server CreateProcess Hang");
		}
	} else {
		LogError("CreateProcess() FAILED - Start Server");
	}

	if (FAILED(CreateShortCut(m_game.Name(),
			loadStr(IDS_CREATE_DISTRO).c_str(),
			loadStr(IDS_CREATE_DISTRO_DESC).c_str(),
			Utf16ToUtf8(editor).c_str(),
			distroParams,
			Utf16ToUtf8(workingDir).c_str())) ||
		FAILED(CreateShortCut(m_game.Name(),
			loadStr(IDS_RUN_SERVER).c_str(),
			loadStr(IDS_RUN_SERVER_DESC).c_str(),
			Utf16ToUtf8(server).c_str(),
			serverParams,
			Utf16ToUtf8(workingDir).c_str()))) {
		AfxMessageBox(IDS_CREATE_SHORTCUT_FAILED, MB_ICONEXCLAMATION);
		return false;
	} else {
		if (serviceCreated) {
			// C:\Windows\System32\net.exe start KGPServer
			wchar_t system32[_MAX_PATH];
			if (SHGetSpecialFolderPathW(m_hWnd,
					system32,
					CSIDL_SYSTEM,
					FALSE)) {
				CStringW exePath = system32;
				exePath += L"\\net.exe";

				LOG4CPLUS_DEBUG(m_logger, "Creating service shortcuts");

				if (FAILED(CreateShortCut(m_game.Name(),
						loadStr(IDS_START_SERVICE).c_str(),
						loadStr(IDS_START_SERVICE_DESC).c_str(),
						Utf16ToUtf8(exePath.GetString()).c_str(),
						"start \"3dApp-" + m_game.Name() + "\"",
						Utf16ToUtf8(workingDir).c_str())) ||
					FAILED(CreateShortCut(m_game.Name(),
						loadStr(IDS_STOP_SERVICE).c_str(),
						loadStr(IDS_STOP_SERVICE_DESC).c_str(),
						Utf16ToUtf8(exePath.GetString()).c_str(),
						"stop \"3dApp-" + m_game.Name() + "\"",
						Utf16ToUtf8(workingDir).c_str()))) {
					AfxMessageBox(IDS_CREATE_SVC_SHORTCUT_FAILED, MB_ICONEXCLAMATION);
					return false;
				}
			} else
				AfxMessageBox(L"Failed get system32 folder", MB_ICONEXCLAMATION);
		}
		return true;
	}
}

EVENT_PROC_RC CKEPEditView::ClickEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	CKEPEditView *me = (CKEPEditView *)lparam;

	int button;
	int objType;
	int objId;

	try {
		(*e->InBuffer()) >> button >> objType >> objId;
	} catch (CException *e) {
		LOG4CPLUS_WARN(me->m_logger, "Decoding click event failed: " << e);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	if (button == 1) //right click
	{
		me->ObjectReplaceSelection((ObjType)objType, objId);
	} else {
		me->ObjectClearSelection();
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC CKEPEditView::SelectEventHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	CKEPEditView *me = (CKEPEditView *)lparam;
	int count = 0;

	try {
		(*e->InBuffer()) >> count;
	} catch (CException *e) {
		LOG4CPLUS_WARN(me->m_logger, "Decoding select event failed: " << e);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	for (int i = 0; i < count; i++) {
		int objType;
		int objId;
		int selected;
		try {
			(*e->InBuffer()) >> objType >> objId >> selected;
		} catch (CException *e) {
			LOG4CPLUS_WARN(me->m_logger, "Decoding select event failed: " << e);
			break;
		}

		float r, g, b;
		switch ((ObjType)objType) {
			case ObjectType::DYNAMIC:
			case ObjectType::WORLD:
				r = 0.05f;
				g = 0.25f;
				b = 1.0f;
				break;
			case ObjectType::MOVEMENT:
				r = 1.0f;
				g = 0.25f;
				b = 0.05f;
				break;
			default:
				r = 0.25f;
				g = 1.0f;
				b = 0.25f;
				break;
		}

		me->ObjectSetOutlineState((ObjType)objType, objId, selected != 0);
		me->ObjectSetOutlineColor((ObjType)objType, objId, D3DXCOLOR(r, g, b, 1.0f));
	}

	if (me->ObjectsSelected() > 0) {
		ASSERT(me->ObjectsSelected() == 1);
		ObjectType objType;
		int objId;
		me->ObjectGetSelection(&objType, &objId, 0);
		if (objType == ObjectType::WORLD)
			me->SetCursorToSelectedObject(objId);
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC CKEPEditView::WorldObjectSelectedHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	CKEPEditView *me = (CKEPEditView *)lparam;
	int objectId = -1;
	try {
		(*e->InBuffer()) >> objectId;
	} catch (CException *e) {
		LOG4CPLUS_WARN(me->m_logger, "Decoding worldobjectselected event failed: " << e);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	me->SetCursorToSelectedObject(objectId);
	return EVENT_PROC_RC::OK;
}

/************************
Appends a scene onto an EXISTING scene, this is not tested if you have not already
loaded a scene
Comments:
	if groupmode is specified only objects in that group are imported.
	if aiMergeMode is 1 then referense, aiweb and spawngens are added to the file as well as all objects
*************************/
BOOL CKEPEditView::AppendScene(CStringA fileName, int aiMergeMode, int groupMode, int transitionGroupID) {
	CStringA CurDir;
	CStringA MainDir;

	MainDir = PathBase().c_str();

	CurDir = MainDir + "\\GameFiles\\";
	SetCurrentDirectoryW(Utf8ToUtf16(CurDir).c_str());

	size_t nLevels = DEF_COL_LEVELSDEEP, nMaxFaces = DEF_COL_FACESPERBOX;
	int nResX = DEF_COL_RES_X, nResY = DEF_COL_RES_Y, nResZ = DEF_COL_RES_Z;
	if (m_pCollisionObj) {
		m_pCollisionObj->GetResolution(0, nResX, nResY, nResZ);
		nLevels = m_pCollisionObj->GetNumberOfLevels();
		nMaxFaces = m_pCollisionObj->GetMaxFacesPerBox();
	}

	BOOL bReturn = FALSE;

	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(Utf8ToUtf16(fileName).c_str(), CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION(exc, m_logger)
		SetCurrentDirectoryW(Utf8ToUtf16(MainDir).c_str());
		return FALSE;
	}

	CBackGroundObjList *dumpGround;
	CEnvironmentObj *envDump;
	CPersistObjList *persistDump;
	CEXScriptObjList *zoneStartScript;
	//CEXScriptObjList	*scriptDump;
	CSpawnObjList *spawnDump;
	CCollisionObj *collisionDump;
	CWldGroupObjList *wldListDump;
	CAIWebObjList *aiWebDatabaseDump;
	CReferenceObjList *referenceDatabase = NULL;
	int intDump;
	CStringA stringDump;

	// end preliminary wrapper
	CWldObjectList *patchList = NULL;
	CArchive the_in_Archive(&fl, CArchive::load);
	the_in_Archive >>
		patchList >>
		dumpGround >>
		envDump >>
		persistDump >>
		intDump >>
		stringDump >>
		intDump >>
		intDump >>
		zoneStartScript >>
		stringDump >>
		spawnDump >>
		collisionDump >>
		wldListDump >>
		aiWebDatabaseDump;

	//save of our index of the currently loaded reference database
	int offsetRefLib = m_libraryReferenceDB->GetCount();

	//see if we load a reference library
	for (POSITION posLoc = zoneStartScript->GetHeadPosition(); posLoc != NULL;) {
		CEXScriptObj *sob = (CEXScriptObj *)zoneStartScript->GetNext(posLoc);
		// script events
		if (sob->m_actionAttribute == LOAD_REFERENCE_LIBRARY) {
			CFileException exc;
			CFile fl;
			BOOL res = fl.Open(Utf8ToUtf16(sob->m_miscString).c_str(), CFile::modeRead, &exc);
			if (res == FALSE) {
				LOG_EXCEPTION(exc, m_logger)
			} else {
				//open and merge this file
				CArchive sob_archive(&fl, CArchive::load);
				sob_archive >> referenceDatabase;
				fl.Close();
				for (POSITION refLoc = referenceDatabase->GetHeadPosition(); refLoc != NULL;) {
					CReferenceObj *ref = (CReferenceObj *)referenceDatabase->GetNext(refLoc);
					m_libraryReferenceDB->AddTail(ref);
				}
				//fl.Close();
			}
		}
	}

	if (groupMode > -1) {
		// world objects
		for (POSITION posLoc = patchList->GetHeadPosition(); posLoc != NULL;) {
			CWldObject *patchPtr = (CWldObject *)patchList->GetNext(posLoc);
			if (groupMode != patchPtr->m_groupNumber) {
				continue;
			}

			patchPtr->m_groupNumber = transitionGroupID;
			patchPtr->m_identity = m_globalTraceNumber++;

			m_pWOL->AddTail(patchPtr);
		}

		BuildCollisionDatabase(m_pWOL, nLevels, nMaxFaces, nResX, nResY, nResZ, m_libraryReferenceDB);
		return TRUE;
	}

	int webPreCount = m_pAIWOL->GetCount();

	if (aiMergeMode) {
		if (aiWebDatabaseDump) {
			for (POSITION posLoc = aiWebDatabaseDump->GetHeadPosition(); posLoc != NULL;) {
				CAIWebObj *webObj = (CAIWebObj *)aiWebDatabaseDump->GetNext(posLoc);
				m_pAIWOL->AddTail(webObj);
			}
		}
	}

	// spawnDump
	if (aiMergeMode) {
		if (spawnDump) {
			for (POSITION rPos = spawnDump->GetHeadPosition(); rPos != NULL;) {
				CSpawnObj *ptrLoc = (CSpawnObj *)spawnDump->GetNext(rPos);
				ptrLoc->m_treeUse = ptrLoc->m_treeUse + webPreCount;
				m_spawnGeneratorDB->AddTail(ptrLoc);
			}
		}
	}

	// get newest group number
	int newGroup = 0;
	for (POSITION wPos = m_pWOL->GetHeadPosition(); wPos != NULL;) {
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(wPos);
		if (pWO->m_groupNumber >= newGroup) {
			newGroup = pWO->m_groupNumber + 1;
		}
	}

	for (POSITION wPos = patchList->GetHeadPosition(); wPos != NULL;) {
		CWldObject *pWO = (CWldObject *)patchList->GetNext(wPos);
		if (pWO->m_groupNumber >= newGroup) {
			newGroup = pWO->m_groupNumber + 1;
		}
	}

	// end get newest group number
	// groups
	for (POSITION gPos = wldListDump->GetHeadPosition(); gPos != NULL;) {
		CWldGroupObj *pWGO = (CWldGroupObj *)wldListDump->GetNext(gPos);
		BOOL addIt = TRUE;
		for (POSITION wPos = worldGroupList->GetHeadPosition(); wPos != NULL;) {
			CWldGroupObj *wPtr = (CWldGroupObj *)worldGroupList->GetNext(wPos);

			// if(wPtr->m_groupInt == gPtr->m_groupInt)
			// addIt = FALSE;
			if (wPtr->m_groupName == pWGO->m_groupName) {
				addIt = FALSE;
			}
		}

		if (addIt) {
			// add unknow group
			CWldGroupObj *newPtr = new CWldGroupObj();
			int originalGroup = pWGO->m_groupInt;
			newPtr->m_groupInt = newGroup;
			newPtr->m_groupName = pWGO->m_groupName;

			// update Patch List
			for (POSITION patchPos = patchList->GetHeadPosition(); patchPos != NULL;) {
				CWldObject *patchPtr = (CWldObject *)patchList->GetNext(patchPos);
				if (patchPtr->m_groupNumber == originalGroup) {
					patchPtr->m_groupNumber = newGroup;
				}
			}

			// end update Patch List
			newGroup++;
			worldGroupList->AddTail(newPtr);
		} // end add unknown group
	}

	// world objects
	for (POSITION posLoc = patchList->GetHeadPosition(); posLoc != NULL;) {
		CWldObject *patchPtr = (CWldObject *)patchList->GetNext(posLoc);
		patchPtr->m_identity = m_globalTraceNumber++;

		if (patchPtr->m_node) { //set the references object pointers
			patchPtr->m_node->m_libraryReference += offsetRefLib;
		}
		m_pWOL->AddTail(patchPtr);
	}

	RebuildTraceNumber(); // rebuild trace numbers
	//ReinitAllTexturesFromDatabase ();
	WorldGroupInList(0);
	SpawnGenDatabaseInList(0);
	AITreeInList(m_pAIWOL->GetCount() - 1);
	the_in_Archive.Close();
	fl.Close();

	SetCurrentDirectoryW(Utf8ToUtf16(MainDir).c_str());

	BuildCollisionDatabase(m_pWOL, nLevels, nMaxFaces, nResX, nResY, nResZ, m_libraryReferenceDB);

	return bReturn;
}

BOOL CKEPEditView::SaveScene(CStringA fileName) {
	// preliminary wrapper
	CPersistObjList *persistList = NULL;
#if KEP_EDIT
	GeneratePersistInfo(&persistList);
#endif

	// end preliminary wrapper
	CFileException exc;
	CFile fl;
	CStringA reserved = "HI";
	if (fl.Open(Utf8ToUtf16(fileName).c_str(), CFile::modeCreate | CFile::modeWrite, &exc)) {
		BEGIN_SERIALIZE_TRY
		ServerSerialize = false;

		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive
			<< m_pWOL
			<< m_backgroundList
			<< m_environment
			<< persistList
			<< -1 //m_currentTrack
			<< CStringA("") //m_musicLoopFile
			<< m_globalWorldShader
			<< (BOOL)FALSE //m_bufferPreffered
			<< m_sceneScriptExec
			<< reserved
			<< m_spawnGeneratorDB
			<< m_pCollisionObj
			<< worldGroupList
			<< m_pAIWOL;

		the_out_Archive.Close();
		fl.Close();

		// write out the triggers to a separate file
		CStringA trgFileName;
		int p = fileName.ReverseFind(_T('.'));
		if (p > 0)
			trgFileName = fileName.Left(p);
		trgFileName += ".trg";

		TriggerFile::SaveTriggers(m_logger, trgFileName, fileName, m_pWOL);

#if KEP_EDIT
		// delete temp wrappers
		DestroyPersistInfo(&persistList);
#endif

		END_SERIALIZE_TRY(m_logger)

		if (serializeOk) {
			CFileException exc;
			CFile fl;
			CStringA svrFileName(fileName + ".Server");
			ServerSerialize = true;

			if (OpenCFile(fl, svrFileName, CFile::modeCreate | CFile::modeWrite, &exc)) {
				BEGIN_SERIALIZE_TRY
				CArchive the_out_Archive(&fl, CArchive::store);
				the_out_Archive
					<< m_pWOL << m_backgroundList << m_environment << persistList << -1 << CStringA("") << m_globalWorldShader << (BOOL)FALSE //m_bufferPreffered <<
																																	  m_sceneScriptExec
					<< reserved << m_spawnGeneratorDB << m_pCollisionObj << worldGroupList << m_pAIWOL;

				the_out_Archive.Close();
				fl.Close();
				END_SERIALIZE_TRY(m_logger)
			}

			ServerSerialize = false;
		}
		return serializeOk ? TRUE : FALSE;
	} else {
		LOG_EXCEPTION(exc, m_logger)
		return FALSE;
	}
}

void CKEPEditView::DrawBox(LPDIRECT3DDEVICE9 pd3dDevice, Vector3f max, Vector3f min) {
}

void CKEPEditView::BoundingBoxesRender() {
	if (GL_showBoundingBoxs == TRUE) {
		for (POSITION posLocal = m_pWOL->GetHeadPosition(); posLocal != NULL;) {
			CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(posLocal);
			if (pWO->m_showBox == TRUE) {
				DrawBox(g_pD3dDevice, pWO->m_popoutBox.max, pWO->m_popoutBox.min);
			}
		}
	}
}

bool CKEPEditView::BuildCollisionDatabase(CWldObjectList *WldObjList, size_t maxLevels, size_t maxFacesPerBox, int nResX, int nResY, int nResZ, CReferenceObjList *libraryReferenceDB, HWND hwndProgTotal /*= NULL*/, HWND hwndProgTask /*= NULL*/) {
	bool res = ClientEngine::BuildCollisionDatabase(WldObjList, maxLevels, maxFacesPerBox, nResX, nResY, nResZ, libraryReferenceDB, hwndProgTotal, hwndProgTask);
	if (res) {
		AfxMessageBox(IDS_ERROR_STOPPED);
	}

	m_worldListTemp->RemoveAll();
	return res;
}

void CKEPEditView::DeleteObjectByTrace(int trace) { // BEGIN
	POSITION worldPos1, worldPos2;

	// clean temp list
	POSITION posLast;
	for (POSITION posLoc = m_worldListTemp->GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CWldObject *pWO = (CWldObject *)m_worldListTemp->GetNext(posLoc);
		if (pWO->m_identity == trace) {
			m_worldListTemp->RemoveAt(posLast);
			break;
		}
	}

	// clean normal
	for (worldPos1 = m_pWOL->GetHeadPosition(); (worldPos2 = worldPos1) != NULL;) { // Begin world object loop
		CWldObject *pWO = (CWldObject *)m_pWOL->GetNext(worldPos1);
		if (pWO->m_identity == trace) {
			if ((m_selectedLmp != 0) && (pWO == m_selectedLmp->GetLegacyWorldObject()))
				SetSelected(0);

			m_pWOL->RemoveAt(worldPos2);

			// clear any triggers before deleting the object
			for (POSITION pos = pWO->m_triggerList->GetHeadPosition(); pos != NULL;) {
				CTriggerObject *trg2 = (CTriggerObject *)pWO->m_triggerList->GetNext(pos);

				POSITION posLoc2;
				for (POSITION posLoc = m_pTOL->GetHeadPosition(); (posLoc2 = posLoc) != NULL;) {
					CTriggerObject *trg = (CTriggerObject *)m_pTOL->GetNext(posLoc);
					if (trg2 == trg) {
						m_pTOL->RemoveAt(posLoc2);
						break;
					}
				}
			}

			pWO->SafeDelete();
			delete pWO;
			pWO = 0;

			return;
		} // end found its ass
	} // end Begin world object loop
} // END

BOOL CKEPEditView::LoadDynamicObjects(const std::string &fileName) {
	if (m_dynamicObjectRM) {
		delete m_dynamicObjectRM;
		m_dynamicObjectRM = NULL;
	}

	CFileException exc;
	CFile *fl = CFileFactory::GetFile();
	BOOL res;
	CStringW buffer;

	res = fl->Open(Utf8ToUtf16(PathAdd(PathGameFiles(), fileName)).c_str(), CFile::modeRead, &exc);

	if (!res) {
		delete fl;
		LogError(loadStr(IDS_ERROR_LOADING) << fileName << ":" << exc.m_cause);
		return FALSE;
	}
	CArchive ar(fl, CArchive::load);
	m_dynamicObjectRM = DynamicObjectLibrary::readFromDisk(ar);
	ar.Close();
	fl->Close();
	DELETE_AND_ZERO(fl);

	if (m_dynamicObjectRM) {
		return TRUE;
	} else {
		return FALSE;
	}
}

void KEPEditView::LoadItemGeneratedData(const string &fileName) {
	ScopedDirectoryChanger sdc("GameFiles\\");

	BEGIN_CFILE_SERIALIZE(fl, fileName, false)

	m_serverItemGenerator->SafeDelete();
	delete m_serverItemGenerator;
	m_serverItemGenerator = 0;

	CArchive the_in_Archive(fl, CArchive::load);
	the_in_Archive >> m_serverItemGenerator;
	the_in_Archive.Close();

	END_CFILE_SERIALIZE(fl, m_logger, "LoadItemGeneratedData")
}
