#include "stdafx.h"
#include "KEPControlBarMFC.h"
#include "afxpriv.h"

BEGIN_MESSAGE_MAP(CKEPControlBarMFC, CControlBar)
ON_WM_NCCALCSIZE()
ON_WM_NCPAINT()
ON_WM_MOVE()
ON_WM_NCLBUTTONDOWN()
ON_WM_NCLBUTTONUP()
ON_WM_NCLBUTTONDBLCLK()
ON_WM_NCHITTEST()
END_MESSAGE_MAP()

BOOL CKEPControlBarMFC::Create(CWnd* pParentWnd, const char* szCaption, UINT nID, UINT dwStyle) {
	m_dwStyle = dwStyle & CBRS_ALL;

	CStringW sClassName;
	try {
		sClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 0, GetSysColorBrush(COLOR_BTNFACE), 0);

		if (CControlBar::Create(sClassName, Utf8ToUtf16(szCaption).c_str(), dwStyle, CRect(0, 0, 0, 0), pParentWnd, nID))
			return TRUE;
	} catch (CResourceException* e) {
		e->Delete();
	}

	return FALSE;
}

BOOL CKEPControlBarMFC::Create(CWnd* pParentWnd, UINT nCaptionID, UINT nID, UINT dwStyle) {
	CStringW s;
	s.LoadString(nCaptionID);
	return Create(pParentWnd, Utf16ToUtf8(s).c_str(), nID, dwStyle);
}

void CKEPControlBarMFC::InitAll() {
	// TBD, do nothing for now
}

void CKEPControlBarMFC::CleanupAll() {
	// TBD, do nothing for now
}

void CKEPControlBarMFC::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler) {
}

CSize CKEPControlBarMFC::CalcFixedLayout(BOOL bStretch, BOOL bHorz) {
	if (bStretch) {
		if (bHorz)
			return CSize(32767, m_minHeight);
		else
			return CSize(m_minWidth, 32767);
	} else {
		CRect fullRect;
		if (IsFloating())
			GetWindowRect(fullRect);
		else
			m_pDockSite->GetClientRect(fullRect);

		int w = m_prefWidth, h = m_prefHeight;
		if (w == -1)
			w = fullRect.Width();
		if (h == -1)
			h = fullRect.Height();
		if (w < m_minWidth)
			w = m_minWidth;
		if (h < m_minHeight)
			h = m_minHeight;

		return CSize(w, h);
	}
}

void CKEPControlBarMFC::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp) {
	CControlBar::OnNcCalcSize(bCalcValidRects, lpncsp);
	CRect* pRect = (CRect*)(&lpncsp->rgrc[0]);
	CalcInsideRect(*pRect, m_dwStyle & CBRS_ORIENT_HORZ);
}

void CKEPControlBarMFC::OnNcPaint() {
	CControlBar::OnNcPaint();

	if (!IsVisible())
		return;

	if (!IsFloating()) {
		CRect wndRect, clientRect;
		GetWindowRect(wndRect);
		GetClientRect(clientRect);
		ClientToScreen(clientRect);

		CSize wndSize = wndRect.Size();
		int leftBorderWidth, rightBorderWidth;
		int topBorderHeight, bottomBorderHeight;

		leftBorderWidth = clientRect.left - wndRect.left;
		rightBorderWidth = wndRect.right - clientRect.right;
		topBorderHeight = clientRect.top - wndRect.top;
		bottomBorderHeight = wndRect.bottom - clientRect.bottom;

		CWindowDC dc(this);
		dc.FillSolidRect(0, 0, wndSize.cx, topBorderHeight, GetSysColor(COLOR_3DDKSHADOW));
		dc.FillSolidRect(0, wndSize.cy - bottomBorderHeight, wndSize.cx, bottomBorderHeight, GetSysColor(COLOR_BTNFACE));
		dc.FillSolidRect(0, 0, leftBorderWidth, wndSize.cy, GetSysColor(COLOR_BTNFACE));
		dc.FillSolidRect(wndSize.cx - rightBorderWidth, 0, rightBorderWidth, wndSize.cy, GetSysColor(COLOR_BTNFACE));
	}
}

void CKEPControlBarMFC::OnClose() {
	ShowWindow(SW_HIDE);
}

void CKEPControlBarMFC::OnMove(int x, int y) {
	CControlBar::OnMove(x, y);
	if (m_pDockContext && !bDraggingFlagFixed) {
		m_pDockContext->m_bDragging = FALSE; // MFC bug? initial value is random for this var
		bDraggingFlagFixed = true;
	}

	// Always resize border (some class changes border size by itself)
	SetBorders(16, 0, 0, 0);
}

LRESULT CKEPControlBarMFC::OnNcHitTest(CPoint point) {
	CRect wndRect, clientRect;
	GetWindowRect(wndRect);
	GetClientRect(clientRect);
	ClientToScreen(clientRect);

	if (wndRect.PtInRect(point) && !clientRect.PtInRect(point))
		return HTCAPTION;

	return HTCLIENT;
}

void CKEPControlBarMFC::OnNcLButtonDblClk(UINT nHitTest, CPoint point) {
	m_pDockContext->ToggleDocking();
}

void CKEPControlBarMFC::OnNcLButtonDown(UINT nHitTest, CPoint point) {
	if (m_pDockContext)
		m_pDockContext->StartDrag(point);
}

void CKEPControlBarMFC::OnNcLButtonUp(UINT nHitTest, CPoint point) {
	if (m_pDockContext && m_pDockContext->m_bDragging)
		m_pDockContext->EndDrag();
}
