/******************************************************************************
 NewZoneDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// NewZoneDlg dialog

class NewZoneDlg : public CDialog {
	DECLARE_DYNAMIC(NewZoneDlg)

public:
	NewZoneDlg(const char* gameDir, BOOL saveAs, CWnd* pParent = NULL); // standard constructor
	virtual ~NewZoneDlg();

	// Dialog Data
	enum { IDD = IDD_NEW_ZONE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	CStringA m_gameDir;
	BOOL m_saveAs;
	CStringA m_fname; // will have value if clicked ok

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	const char* ZoneName() const { return m_fname; }
};
