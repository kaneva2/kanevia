/******************************************************************************
 PublishGameStatusDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#define START_PUBLISH_PROCESS 1
#define UPDATE_PUBLISH_PROGRESS 2
#define UPDATE_PUBLISH_STATUS 3
#define PUBLISH_PROGRESS_COMPLETE 4

// CPublishGameStatusDlg dialog

class CPublishGameStatusDlg : public CDialog {
	DECLARE_DYNAMIC(CPublishGameStatusDlg)

public:
	UINT m_iMsgPubId;
	CPublishGameStatusDlg(CWnd *pParent = NULL, PROCESS_INFORMATION *pi = NULL, CStringA logFilePath = NULL,
		DWORD dwRead = NULL, DWORD dwWritten = NULL, CHAR *chBuf = NULL, HANDLE hchildStdoutRead = NULL, CStringA errMsg = NULL, CStringA distFolder = NULL, CStringA successMSG = NULL); // standard constructor
	virtual ~CPublishGameStatusDlg();

	// Dialog Data
	enum { IDD = IDD_DIALOG_PUBLISH_GAME };
	virtual BOOL OnInitDialog();

private:
	static HANDLE m_hEvPublishHasClosed;

protected:
	virtual void DoDataExchange(CDataExchange *pDX); // DDX/DDV support
	afx_msg LRESULT onStatusMessage(WPARAM wp, LPARAM lp);
	static UINT BuildSetup(LPVOID pParam);
	static CProgressCtrl m_CrtlProgressBar;
	static float m_tCurrentTime;
	static float m_tLastTime;
	static float m_tElapsedTime;
	static CStringA m_strStatusMsg;
	static float m_iPublishTotalGoals;
	static float m_iGoalsCompleted;

	static PROCESS_INFORMATION *m_pi;
	static CStringA m_logFilePath;
	static DWORD m_dwRead;
	static DWORD m_dwWritten;
	static CHAR *m_chBuf;
	static HANDLE m_hchildStdoutRead;
	static CStringW m_errMsg;
	static CStringA m_distFolder;
	static CStringW m_successMsg;
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_CrtStatusUpdate;
};
