/******************************************************************************
 Publish.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "StdAfx.h"
#include ".\Publish.h"
#include "Engine.h"
#include <direct.h>
#include "KEPEdit.h"
#include <jsLibraryFunction.h>
#include "PublishUpdateDlg.h"
#include "LogHelper.h"
#include <js.h>
#include <jsEnumFiles.h>

#define GAMEFILES "GameFiles"
#define SOUNDS "Sounds"
#define MAPSMODELS "MapsModels"

#define XFER_TIMEOUT_SECONDS 60
#define CHECK_TIMEOUT 52
// A structure of call back methods that will later be
// registered with Python.
static struct PyMethodDef publish_item_callback_methods[] = {
	{ "ItemStatusUpdateEventCallback", (PyCFunction)ItemStatusUpdateEvent, METH_VARARGS },
	{ NULL, NULL }
};

static struct PyMethodDef publish_game_callback_methods[] = {
	{ "GameStatusUpdateEventCallback", (PyCFunction)GameStatusUpdateEvent, METH_VARARGS },
	{ NULL, NULL }
};

static struct PyMethodDef exception_callback_methods[] = {
	{ "PythonExceptionCallback", (PyCFunction)PythonExceptionHandler, METH_VARARGS },
	{ NULL, NULL }
};

// Python modules and functions that are called.
#define PYTHON_MODULE_GLOBAL "Global"
#define PYTHON_MODULE_GLOBAL_METHOD_GLOBAL "Global"
#define PYTHON_MODULE_GLOBAL_METHOD_REGISTER "register"
#define PYTHON_MODULE_GLOBAL_METHOD_LOGGER "setupLogger"
#define PYTHON_MODULE_GLOBAL_METHOD_UNHANDLED_EXCEPTION "unhandled exception"

#define PYTHON_MODULE_ITEM "UploadItem"
#define PYTHON_MODULE_ITEM_METHOD_CREATE "create"
#define PYTHON_MODULE_ITEM_GAMEUPLOAD "GameUpload"
#define PYTHON_MODULE_ITEM_GAMEUPLOAD_METHOD_ADD "addUploadItem"

#define PYTHON_MODULE_SESSION "Session"
#define PYTHON_MODULE_SESSION_METHOD_USERSESSION "UserSession"
#define PYTHON_MODULE_SESSION_METHOD_LOGIN "login"
#define PYTHON_MODULE_SESSION_METHOD_GETACCOUNTDATA "getGames"
#define PYTHON_MODULE_SESSION_METHOD_INITIALIZE "initialize"

#define PYTHON_MODULE_ITEM_MANAGER "uploadItemManager"
#define PYTHON_MODULE_ITEM_MANAGER_METHOD_MANAGER "UploadItemManager"
#define PYTHON_MODULE_ITEM_MANAGER_METHOD_ADD "add"
#define PYTHON_MODULE_ITEM_MANAGER_METHOD_ADD_GAME_UPLOAD "addGameUpload"
#define PYTHON_MODULE_ITEM_MANAGER_METHOD_START_UPLOAD "startUpload"
#define PYTHON_MODULE_ITEM_MANAGER_METHOD_STOP_UPLOAD "stopUpload"

#define PYTHON_MODULE_ITEM_MANAGER_METHOD_REGISTER "register"
#define PYTHON_MODULE_ITEM_MANAGER_METHOD_ITEMSTATUSUPDATE "event_item_status_update"
#define PYTHON_MODULE_ITEM_MANAGER_METHOD_GAMESTATUSUPDATE "event_game_status_update"

#ifdef _DEBUG
#define PYTHON_DLL "GameFiles\\BladeScriptsd\\PythonEventhandler.dll"
#else
#define PYTHON_DLL "GameFiles\\BladeScripts\\PythonEventhandler.dll"
#endif

// Message to give feedback to the progress dialog
UINT gPubUpdateMsgId = ::RegisterWindowMessageW(L"KEI_PUBLISH_STATUS_MSG");

#define PSM
// 4/08 now load from DLL
typedef bool (*PSM_BeginFn)(IN bool reInit);
typedef bool (*PSM_InitializeFn)(bool useGlobalState);
typedef void (*PSM_EndFn)();
typedef void (*PSM_UninitializeFn)();
typedef void (*logPythonErrFn)(log4cplus::Logger &logger, const char *msg, bool warn);
PSM_BeginFn PSM_Begin;
PSM_InitializeFn PSM_Initialize;
PSM_EndFn PSM_End;
PSM_UninitializeFn PSM_Uninitialize;
logPythonErrFn logPythonErrFunc;

inline void logPythonErrA(log4cplus::Logger &logger, const char *msg, bool warn) {
	return logPythonErrFunc(logger, msg, warn);
}
inline void logPythonErrW(log4cplus::Logger &logger, const wchar_t *msg, bool warn) {
	return logPythonErrFunc(logger, Utf16ToUtf8(msg).c_str(), warn);
}

// Variables used by the python callback function
Publish Publish::theInstance;
PyObject *Publish::m_pUploadItemManager;
HWND Publish::m_progressHwnd;
HANDLE Publish::m_closeEvent;
HANDLE Publish::m_pubCloseEvent;
ULONG Publish::m_numFilesToXfer;
ULONG Publish::m_numFilesXferCompleted;
ULONG Publish::m_totalBytesXfer;
ULONG Publish::m_totalBytesToXfer;
bool Publish::m_XferComplete;
Logger *Publish::m_callbackLogger;
PyObject *Publish::m_pGameUpload;
time_t Publish::m_timeoutStart;
bool Publish::m_XferCanceled;
bool Publish::bPSM_Lock;

///---------------------------------------------------------
// StopUpload
//
// Request python to stop its current upload. Signal is trapped
// in the callback.
//
// [Returns]
//  None.

static bool StopUpload(PyObject *pUploadItemManager) {
	bool ret = true;

	PyObject *pStopResult = NULL;

	try {
		pStopResult = PyObject_CallMethod(pUploadItemManager, PYTHON_MODULE_ITEM_MANAGER_METHOD_STOP_UPLOAD, NULL);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_STOP_UPLOAD_EXCEPTION);
		logPythonErrW(*Publish::m_callbackLogger, errMsg, false);
		ret = false;
	}

	if (pStopResult) {
		Py_DECREF(pStopResult);
		Publish::m_XferComplete = true;
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_STOP_UPLOAD_ERROR);
		logPythonErrW(*Publish::m_callbackLogger, errMsg, false);

		ret = false;
	}

	if (!ret) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_STOP_PROBLEM);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
	}

	return ret;
}

///---------------------------------------------------------
// ItemStatusUpdateEvent
//
// Called by python modules during the transfer process to provide
// feedback for specific files.
//
// [Returns]
//  NULL python object as per the interface

extern "C" PyObject *ItemStatusUpdateEvent(PyObject *self, PyObject *args) {
	char *event_name = nullptr;
	char *event_message = nullptr;
	char *file_name = nullptr;
	char *file_path = nullptr;
	char *destination_path = nullptr;
	char *status = nullptr;
	char *lastErrorMessage = nullptr;

	long lastByteSent = 0;
	long lastByteToSend = 0;

	// update the timeout timer. If messages aren't flowing via
	// these callbacks, issue a timeout message
	time(&Publish::m_timeoutStart);

	if (WaitForSingleObject(Publish::m_closeEvent, 0) == WAIT_OBJECT_0) {
		// A close requested from the progress dialog
		// Stop the active upload
		Publish::m_XferCanceled = true;
		StopUpload(Publish::m_pUploadItemManager);
		SetEvent(Publish::m_pubCloseEvent);
		Py_INCREF(Py_None);
		return Py_None;
	}

	if (Publish::m_XferCanceled) // Stop post cancel messages
	{
		return Py_None;
	}

	PyObject *pObj;
	PyObject *pWorkObj;

	STATUSUPDATEPUB pStatus;
	memset(&pStatus, 0, sizeof(pStatus));

	pStatus.numFilesToXfer = Publish::m_numFilesToXfer;

	try {
		//int size = PyTuple_GET_SIZE(args);

		/*int ii = */ PyArg_ParseTuple(args, "O", &pObj);

		if (PyObject_HasAttrString(pObj, "__dict__")) {
			PyObject *pDict = PyObject_GetAttrString(pObj, "__dict__");

			pWorkObj = PyDict_GetItemString(pDict, "name");

			if (PyString_Check(pWorkObj)) {
				event_name = PyString_AsString(pWorkObj);
			}

			pWorkObj = PyDict_GetItemString(pDict, "message");

			if (PyString_Check(pWorkObj)) {
				event_message = PyString_AsString(pWorkObj);
			}

			PyObject *uploadItemObj = PyDict_GetItemString(pDict, "uploadItem");

			if (uploadItemObj) {
				pDict = PyObject_GetAttrString(uploadItemObj, "__dict__");

				pWorkObj = PyDict_GetItemString(pDict, "name");

				if (PyString_Check(pWorkObj)) {
					file_name = PyString_AsString(pWorkObj);
					strcpy_s(pStatus.fileName, _countof(pStatus.fileName), file_name);
				}

				pWorkObj = PyDict_GetItemString(pDict, "filePath");

				if (PyString_Check(pWorkObj)) {
					file_path = PyString_AsString(pWorkObj);
				}
				pWorkObj = PyDict_GetItemString(pDict, "destinationPath");

				if (PyString_Check(pWorkObj)) {
					destination_path = PyString_AsString(pWorkObj);

					char fileLoc[_MAX_PATH];
					strcpy_s(fileLoc, _countof(fileLoc), destination_path);
					strcat_s(fileLoc, _countof(fileLoc), "\\");
					strcat_s(fileLoc, _countof(fileLoc), file_name);

					size_t fileLocLen = strlen(fileLoc);
					size_t statPathLen = sizeof(pStatus.destPath);
					if (statPathLen <= fileLocLen) {
						strcpy_s(pStatus.destPath, _countof(pStatus.destPath), fileLoc + (fileLocLen - statPathLen + 1));
					} else {
						strcpy_s(pStatus.destPath, _countof(pStatus.destPath), fileLoc);
					}
				}

				pWorkObj = PyDict_GetItemString(pDict, "lastByteSent");

				if (PyInt_Check(pWorkObj)) {
					lastByteSent = PyInt_AsLong(pWorkObj);
					pStatus.lastByteSent = lastByteSent;
				}

				pWorkObj = PyDict_GetItemString(pDict, "lastByteToSend");

				if (PyLong_Check(pWorkObj)) {
					lastByteToSend = PyLong_AsLong(pWorkObj);
					pStatus.lastByteToSend = lastByteToSend;
				}

				pWorkObj = PyDict_GetItemString(pDict, "lastErrorMessage");

				if (PyString_Check(pWorkObj)) {
					lastErrorMessage = PyString_AsString(pWorkObj);
				}

				pWorkObj = PyDict_GetItemString(pDict, "status");

				if (PyString_Check(pWorkObj)) {
					status = PyString_AsString(pWorkObj);

					if ((_stricmp(status, "Completed") == 0) ||
						((_stricmp(lastErrorMessage, "Same file already exists on the server") == 0) && (_stricmp(status, "Error") == 0)) ||
						((_stricmp(lastErrorMessage, "File not changed") == 0) && (_stricmp(status, "Error") == 0))) {
						Publish::m_numFilesXferCompleted++;
						Publish::m_totalBytesXfer += lastByteToSend;

						pStatus.numFilesXferCompleted = Publish::m_numFilesXferCompleted;
						pStatus.totalByteToSend = Publish::m_totalBytesToXfer;
						pStatus.totalByteSent = Publish::m_totalBytesXfer;

						if ((_stricmp(lastErrorMessage, "Same file already exists on the server") == 0) ||
							(_stricmp(lastErrorMessage, "File not changed") == 0)) {
							strcpy_s(pStatus.msg, _countof(pStatus.msg), lastErrorMessage);
						}

						::SendMessage(Publish::m_progressHwnd, gPubUpdateMsgId, MSG_STATUS, (LPARAM)&pStatus);
					} else if ((_stricmp(status, "Preparing") == 0) ||
							   (_stricmp(status, "Uploading") == 0)) {
						pStatus.numFilesXferCompleted = Publish::m_numFilesXferCompleted;
						pStatus.totalByteToSend = Publish::m_totalBytesToXfer;
						pStatus.totalByteSent = Publish::m_totalBytesXfer;

						::SendMessage(Publish::m_progressHwnd, gPubUpdateMsgId, MSG_STATUS, (LPARAM)&pStatus);
					} else {
						pStatus.numFilesXferCompleted = Publish::m_numFilesXferCompleted;
						pStatus.totalByteToSend = Publish::m_totalBytesToXfer;
						pStatus.totalByteSent = Publish::m_totalBytesXfer;

						::KillTimer(Publish::m_progressHwnd, CHECK_TIMEOUT);

						CStringW errMsg;

						if (_stricmp(lastErrorMessage, "Previous upload is not yet processed") == 0) {
							errMsg.LoadString(IDS_PUB_TRANSFER_UPLOAD_NOT_PROCESSED_FAILURE);
							AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
						} else {
							errMsg.Format(IDS_PUB_TRANSFER_FAILURE, Utf8ToUtf16(lastErrorMessage).c_str());
							AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
						}

						// Show error message
						::SendMessage(Publish::m_progressHwnd, gPubUpdateMsgId, MSG_ERROR, (LPARAM)0);
						Publish::m_XferComplete = true;
					}
				}
			}

			CStringA t;
			t.Format("Fired %s File=<%s> LastByteSent<%ld> LastByteToSend<%ld> LastErrorMessage<%s> Status<%s>\n", NULL_CK(event_name), NULL_CK(file_name), lastByteSent, lastByteToSend, NULL_CK(lastErrorMessage), NULL_CK(status));
			logPythonErrA(*Publish::m_callbackLogger, t, true);
			OutputDebugStringA(t);
		}
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_ITEM_CALLBACK_EXCEPTION);
		logPythonErrW(*Publish::m_callbackLogger, errMsg, false);
	}

	Py_INCREF(Py_None);
	return Py_None;
}

///---------------------------------------------------------
// GameStatusUpdateEvent
//
// Called by python modules during the transfer process to provide
// feedback for the entire game batch.
//
// [Returns]
//  NULL python object as per the interface

extern "C" PyObject *GameStatusUpdateEvent(PyObject *self, PyObject *args) {
	char *status;
	char *lastErrorMessage;
	ULONG totalsize = 0; // DRF - potentially uninitialized variable

	// update the timeout timer. If messages aren't flowing via
	// these callbacks, issue a timeout message
	time(&Publish::m_timeoutStart);

	if (WaitForSingleObject(Publish::m_closeEvent, 0) == WAIT_OBJECT_0) {
		// A close requested from the progress dialog
		// Stop the active upload
		Publish::m_XferCanceled = true;
		StopUpload(Publish::m_pUploadItemManager);
		SetEvent(Publish::m_pubCloseEvent);
		Py_INCREF(Py_None);
		return Py_None;
	}

	if (Publish::m_XferCanceled) // Stop post cancel messages
	{
		return Py_None;
	}

	PyObject *pObj;
	PyObject *pWorkObj;

	STATUSUPDATEPUB pStatus;
	memset(&pStatus, 0, sizeof(pStatus));

	pStatus.numFilesToXfer = Publish::m_numFilesToXfer;

	try {
		//int size = PyTuple_GET_SIZE(args);

		/*int ii = */ PyArg_ParseTuple(args, "O", &pObj);

		if (PyObject_HasAttrString(pObj, "__dict__")) {
			PyObject *pDict = PyObject_GetAttrString(pObj, "__dict__");

			PyObject *gameUploadObj = PyDict_GetItemString(pDict, "gameUpload");

			if (gameUploadObj) {
				status = NULL;
				lastErrorMessage = NULL;

				pDict = PyObject_GetAttrString(gameUploadObj, "__dict__");

				pWorkObj = PyDict_GetItemString(pDict, "status");

				if (PyString_Check(pWorkObj)) {
					status = PyString_AsString(pWorkObj);
				}

				pWorkObj = PyDict_GetItemString(pDict, "lastErrorMessage");

				if (PyString_Check(pWorkObj)) {
					lastErrorMessage = PyString_AsString(pWorkObj);
				}

				pWorkObj = PyDict_GetItemString(pDict, "size");

				if (PyLong_Check(pWorkObj)) {
					totalsize = PyLong_AsLong(pWorkObj);
				}

				if (_stricmp(status, "Preparing") == 0) {
					Publish::m_totalBytesToXfer = totalsize;
				} else if (_stricmp(status, "Completed") == 0) {
					Publish::m_XferComplete = true;

					// Send complete message
					::SendMessage(Publish::m_progressHwnd, gPubUpdateMsgId, MSG_COMPLETE, (LPARAM)0);
				} else if ((_stricmp(status, "Error") == 0) ||
						   (_stricmp(status, "Paused") == 0)) {
					::KillTimer(Publish::m_progressHwnd, CHECK_TIMEOUT);

					CStringW errMsg;
					errMsg.Format(IDS_PUB_TRANSFER_FAILURE, Utf8ToUtf16(lastErrorMessage).c_str());
					AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

					// Show error message
					::SendMessage(Publish::m_progressHwnd, gPubUpdateMsgId, MSG_ERROR, (LPARAM)0);
					Publish::m_XferComplete = true;
				}

				CStringA t;
				t.Format("Game status =%s - Error Msg =%s\n", NULL_CK(status), NULL_CK(lastErrorMessage));
				logPythonErrA(*Publish::m_callbackLogger, t, true);
				OutputDebugStringA(t);
			}
		}
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_GAME_CALLBACK_EXCEPTION);
		logPythonErrW(*Publish::m_callbackLogger, errMsg, false);
	}

	Py_INCREF(Py_None);
	return Py_None;
}

///---------------------------------------------------------
// PythonExceptionHandle
//
// Called by python modules when they catch an exception that is
// on their own thread. This happens when we give control to python
// during the transfer process. Status is given via callbacks, although, if
// an exception occurs, python must trap it and give it to this module
// via a callback. If it doesn't trap the exception, this module will
// not detect the exception.
//
// [Returns]
//  NULL python object as per the interface

extern "C" PyObject *PythonExceptionHandler(PyObject *self, PyObject *args) {
	char *error_message = 0; // DRF - potentially uninitialized variable

	PyObject *pObj;
	PyObject *pWorkObj;

	try {
		//int size = PyTuple_GET_SIZE(args);

		/*int ii = */ PyArg_ParseTuple(args, "O", &pObj);

		if (PyObject_HasAttrString(pObj, "__dict__")) {
			PyObject *pDict = PyObject_GetAttrString(pObj, "__dict__");

			pWorkObj = PyDict_GetItemString(pDict, "errStr");

			if (PyString_Check(pWorkObj)) {
				error_message = PyString_AsString(pWorkObj);
			}

			::KillTimer(Publish::m_progressHwnd, CHECK_TIMEOUT);

			CStringW errMsg;
			errMsg.Format(IDS_PYTHON_EXCEPTION_HANDLER, ((error_message) ? Utf8ToUtf16(error_message).c_str() : L""));
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

			::SendMessage(Publish::m_progressHwnd, gPubUpdateMsgId, MSG_ERROR, (LPARAM)0);
			Publish::m_XferComplete = true;
		}
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_EXCEPTION_CALLBACK_EXCEPTION);
		logPythonErrW(*Publish::m_callbackLogger, errMsg, false);
	}

	Py_INCREF(Py_None);
	return Py_None;
}

///---------------------------------------------------------
// CheckTimeout
//
// Determines that a message has not been received by the python
// transfer modules in XFER_TIMEOUT_SECONDS. Request the xfer to stop and
// present an error to the user.
//
// [Returns]
//  None.

void CALLBACK EXPORT CheckTimeout(HWND hWnd, UINT nMsg, UINT nIDEvent, DWORD dwTime) {
	time_t finish;

	time(&finish);

	double elapsed_time = difftime(finish, Publish::m_timeoutStart);

	if (elapsed_time > XFER_TIMEOUT_SECONDS) {
		::KillTimer(Publish::m_progressHwnd, CHECK_TIMEOUT);

		CStringW errMsg;

		errMsg.Format(IDS_PUB_TRANSFER_FAILURE, L"Timeout");
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
		::SendMessage(Publish::m_progressHwnd, gPubUpdateMsgId, MSG_ERROR, (LPARAM)0);
		Publish::m_XferComplete = true;
	}
}

///---------------------------------------------------------
// addStringNoDupes
//
// Add to vector and ensure it doesn't already exist.
//
// [Returns]
//  None
static void addStringNoDupes(INOUT OUT strPairVect &files, IN const string &firstString, IN const string &secondString) {
	for (strPairVect::iterator i = files.begin(); i != files.end(); i++) {
		if (STLCompareIgnoreCase(i->first.c_str(), firstString.c_str()) == 0)
			return;
	}
	files.push_back(make_pair(firstString, secondString)); // not found
}

///---------------------------------------------------------
// Publish::Initialize
//
// Initialize the python envrionment and all needed modules for the
// publishing process.
//
// [Returns]
//  bool    true - success
//          false - error

PyThreadState *m_threadState = 0;
PyGILState_STATE m_pythonGIL;

static jsLibraryFunction *lib = 0;

void Publish::LockPSM() {
	bPSM_Lock = false;
}

void Publish::UnlockPSM() {
	bPSM_Lock = false;
	DeinitPSM();
}

bool Publish::InitPSM(IN CStringA gameDir) {
#ifdef PSM
	if (bPSM_Lock == false) {
		bPSM_Lock = true;
		if (lib)
			delete lib;
		lib = new jsLibraryFunction(PathAdd(gameDir, PYTHON_DLL).c_str());
		PSM_Initialize = (PSM_InitializeFn)lib->getFunction("PSM_Initialize");
		PSM_Uninitialize = (PSM_UninitializeFn)lib->getFunction("PSM_Uninitialize");
		PSM_Begin = (PSM_BeginFn)lib->getFunction("PSM_Begin");
		PSM_End = (PSM_EndFn)lib->getFunction("PSM_End");
		logPythonErrFunc = (logPythonErrFn)lib->getFunction("logPythonErr");
		jsVerifyReturn(PSM_Begin && PSM_Initialize && PSM_End && PSM_Uninitialize && logPythonErrFunc, false);

		return PSM_Initialize(false) && PSM_Begin(false);
	}
#else
	if (!::Py_IsInitialized()) {
		Py_Initialize();
		PyEval_InitThreads();
	} else {
		//Engine blades are using Python as well and may
		//have already initialized Python.

		m_pythonGIL = PyGILState_Ensure();
		m_threadState = PyThreadState_Get();
	}
#endif

	return true;
}

void Publish::DeinitPSM() {
#ifdef PSM
	if (PSM_End && lib) {
		PSM_End();
		if (PSM_Uninitialize) {
			PSM_Uninitialize();
		}
	}
#else
	PyGILState_Release(m_pythonGIL);
#endif

	DELETE_AND_ZERO(lib); // 3/08 make DLL
}

bool Publish::Initialize(IN CStringA gameDir) {
	InitPSM(gameDir);

	string baseDir = PathApp(false);

	const char *existingPath = ::Py_GetPath();
	if (existingPath) {
		string ep(existingPath);

		vector<string> tokens;
		STLTokenize(ep, tokens, ";");

		vector<string>::iterator i;
		for (i = tokens.begin(); i != tokens.end(); i++) {
			// Windows case insensitive
			if (STLCompareIgnoreCase(*i, baseDir) == 0)
				break;
		}
		if (i == tokens.end()) {
			string newPath = baseDir;
			PathAddInPlace(newPath, "python;");
			newPath += existingPath;
			LOG4CPLUS_WARN(theLogger(), "Existing python path " << existingPath);

			::PySys_SetPath((char *)newPath.c_str());
			LOG4CPLUS_WARN(theLogger(), "New python path " << newPath);
		}
	} else {
		string newPath = baseDir;
		PathAddInPlace(newPath, "python;");
		::PySys_SetPath((char *)newPath.c_str());
		LOG4CPLUS_WARN(theLogger(), "No existing path. New python path " << newPath);
	}

	PyObject *pName;

	m_pModuleSession = NULL;
	m_pModuleUploadItemManager = NULL;
	m_pModuleUploadItem = NULL;
	m_pGlobal = NULL;

	pName = PyString_FromString(PYTHON_MODULE_GLOBAL);

	m_pGlobal = PyImport_Import(pName);

	Py_DECREF(pName);

	if (!m_pGlobal) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_GLOBAL_ERROR);
		logPythonErrW(theLogger(), errMsg, false);
		return false;
	}

	PyObject *pGlobal = GetGlobalHandler();

	if (pGlobal) {
		Py_DECREF(pGlobal);
	} else {
		return false;
	}

	pName = PyString_FromString(PYTHON_MODULE_SESSION);
	m_pModuleSession = PyImport_Import(pName);

	Py_DECREF(pName);

	if (!m_pModuleSession) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_SESSION_ERROR);
		logPythonErrW(theLogger(), errMsg, false);
		return false;
	}

	pName = PyString_FromString(PYTHON_MODULE_ITEM_MANAGER);
	m_pModuleUploadItemManager = PyImport_Import(pName);

	Py_DECREF(pName);

	if (!m_pModuleUploadItemManager) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_UPLOADITEMMGR_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		Py_DECREF(m_pModuleSession);
		return false;
	}

	pName = PyString_FromString(PYTHON_MODULE_ITEM);
	m_pModuleUploadItem = PyImport_Import(pName);

	Py_DECREF(pName);

	if (!m_pModuleUploadItem) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_UPLOADITEM_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		Py_DECREF(m_pModuleSession);
		Py_DECREF(m_pModuleUploadItemManager);
		return false;
	}

	{ // call init fn to set the path for loading settings
		PyObject *pArgs, *pFunc, *pDict;
		PyObject *pSession = NULL;
		PyObject *pLoginResult = NULL;

		pDict = PyModule_GetDict(m_pModuleSession);

		pFunc = PyDict_GetItemString(pDict, PYTHON_MODULE_SESSION_METHOD_USERSESSION);

		if (pFunc && PyCallable_Check(pFunc)) {
			pArgs = PyTuple_New(0);

			try {
				pSession = PyObject_CallObject(pFunc, pArgs);
			} catch (...) {
				CStringW errMsg;
				errMsg.Format(IDS_PYTHON_INIT_SESSION_EXCEPTION, Utf8ToUtf16(gameDir).c_str());
				logPythonErrW(theLogger(), errMsg, false);
			}

			Py_DECREF(pArgs);

			if (!pSession) {
				CStringW errMsg;
				errMsg.Format(IDS_PYTHON_INIT_SESSION_EXCEPTION, Utf8ToUtf16(gameDir).c_str());
				AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
				return NULL;
			}

			try {
				pLoginResult = PyObject_CallMethod(pSession, PYTHON_MODULE_SESSION_METHOD_INITIALIZE, "s", (const char *)gameDir);
			} catch (...) {
				CStringW errMsg;
				errMsg.Format(IDS_PYTHON_INIT_SESSION_EXCEPTION, Utf8ToUtf16(gameDir).c_str());
				AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

				Py_DECREF(pSession);

				return false;
			}
		}
	}

	SetInitialized(true);

	return true;
}

///---------------------------------------------------------
// Publish::DeInitialize
//
// Tear down the python envrionment.
//
// [Returns]
//  bool    true - success
//          false - error

bool Publish::DeInitialize(void) {
	if (m_pGlobal) {
		Py_DECREF(m_pGlobal);
		m_pGlobal = NULL;
	}

	if (m_pModuleSession) {
		Py_DECREF(m_pModuleSession);
		m_pModuleSession = NULL;
	}

	if (m_pModuleUploadItemManager) {
		Py_DECREF(m_pModuleUploadItemManager);
		m_pModuleUploadItemManager = NULL;
	}

	if (m_pModuleUploadItem) {
		Py_DECREF(m_pModuleUploadItem);
		m_pModuleUploadItem = NULL;
	}

	//#ifdef PSM
	//PSM_End();
	//PSM_Uninitialize();
	//
	//#else
	//    PyGILState_Release(m_pythonGIL);
	//#endif

	//	DELETE_AND_ZERO( lib ); // 3/08 make DLL

	SetInitialized(false);

	return true;
}

///---------------------------------------------------------
// Publish::CreateUploadItem
//
// Construct an individual uploadItem. This item will then
// be added to the uploadItemManager for transfer to the UploadServer.
//
// [Returns]
//  PyObject * - success - the newly created uploadItem
//  NULL - error

PyObject *Publish::CreateUploadItem(CStringA fileToTransfer, CStringA destinationPath) {
	PyObject *pArgs, *pFunc, *pDict;
	PyObject *pUploadItem = NULL;

	pDict = PyModule_GetDict(m_pModuleUploadItem);

	pFunc = PyDict_GetItemString(pDict, PYTHON_MODULE_ITEM_METHOD_CREATE);

	if (pFunc && PyCallable_Check(pFunc)) {
		// Three parameters to the method,
		//  the file to transfer
		//  the type of asset, asset or game
		//  the destination folder
		pArgs = PyTuple_New(3);

		PyObject *arg = PyString_FromString((const char *)fileToTransfer);
		if (!arg) {
			Py_DECREF(pArgs);
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_ERROR);
			logPythonErrW(theLogger(), errMsg, false);
			return NULL;
		}
		PyTuple_SetItem(pArgs, 0, arg);

		arg = PyString_FromString("1"); // game
		if (!arg) {
			Py_DECREF(pArgs);
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_ERROR);
			logPythonErrW(theLogger(), errMsg, false);
			return NULL;
		}
		PyTuple_SetItem(pArgs, 1, arg);

		arg = PyString_FromString((const char *)destinationPath);
		if (!arg) {
			Py_DECREF(pArgs);
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_ERROR);
			logPythonErrW(theLogger(), errMsg, false);
			return NULL;
		}
		PyTuple_SetItem(pArgs, 2, arg);

		try {
			pUploadItem = PyObject_CallObject(pFunc, pArgs);
		} catch (...) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_EXCEPTION);
			logPythonErrW(theLogger(), errMsg, false);
			Py_DECREF(pArgs);
			return NULL;
		}

		Py_DECREF(pArgs);

		if (pUploadItem) {
			return pUploadItem;
		} else {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_ERROR);
			logPythonErrW(theLogger(), errMsg, false);
		}
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_ERROR);
		logPythonErrW(theLogger(), errMsg, false);
	}

	return NULL;
}

///---------------------------------------------------------
// Publish::Login
//
// Login the user. This is performed via the python
// modules in coordination with the UploadServer.
//
// [Returns]
//  pSession - success - a session object
//  NULL - error

PyObject *Publish::Login(CStringA user, CStringA pass) {
	PyObject *pArgs, *pFunc, *pDict;
	PyObject *pSession = NULL;
	PyObject *pLoginResult = NULL;

	pDict = PyModule_GetDict(m_pModuleSession);

	pFunc = PyDict_GetItemString(pDict, PYTHON_MODULE_SESSION_METHOD_USERSESSION);

	if (pFunc && PyCallable_Check(pFunc)) {
		pArgs = PyTuple_New(0);

		try {
			pSession = PyObject_CallObject(pFunc, pArgs);
		} catch (...) {
			CStringW errMsg;
			errMsg.Format(IDS_PYTHON_XFER_SESSION_EXCEPTION, Utf8ToUtf16(user).c_str());
			logPythonErrW(theLogger(), errMsg, false);
		}

		Py_DECREF(pArgs);

		if (!pSession) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PUBDIST_LOGIN_FAILURE);
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
			return NULL;
		}

		try {
			pLoginResult = PyObject_CallMethod(pSession, PYTHON_MODULE_SESSION_METHOD_LOGIN, "ss", (const char *)user, (const char *)pass);
		} catch (...) {
			CStringW errMsg;
			errMsg.Format(IDS_PYTHON_XFER_LOGIN_EXCEPTION, Utf8ToUtf16(user).c_str());
			logPythonErrW(theLogger(), errMsg, false);

			errMsg.LoadString(IDS_PUBDIST_LOGIN_FAILURE);
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

			Py_DECREF(pSession);

			return NULL;
		}

		if (pLoginResult && PyBool_Check(pLoginResult)) {
			if (pLoginResult == Py_True) {
				Py_DECREF(pLoginResult);
				return pSession;
			} else {
				CStringW errMsg;
				errMsg.LoadString(IDS_PUBDIST_LOGIN_FAILURE);
				AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

				Py_DECREF(pLoginResult);
				Py_DECREF(pSession);
				return NULL;
			}
		} else {
			CStringW errMsg;
			errMsg.Format(IDS_PYTHON_XFER_LOGIN_FAILED, Utf8ToUtf16(user).c_str());
			logPythonErrW(theLogger(), errMsg, false);

			Py_DECREF(pSession);
		}
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_LOGIN_FAILED_TO_OBTAIN_SESSION);
		logPythonErrW(theLogger(), errMsg, false);
	}

	CStringW errMsg;
	errMsg.LoadString(IDS_PUBDIST_LOGIN_FAILURE);
	AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

	return NULL;
}

///---------------------------------------------------------
// Publish::SetupPythonGlobalLogger
//
// Setup the python transfer modules own logger.
//
// [Returns]
//  NONE, logs to own log if error occurs.

void Publish::SetupPythonGlobalLogger(PyObject *pGlobal) {
	PyObject *pResult = NULL;

	try {
		pResult = PyObject_CallMethod(pGlobal, PYTHON_MODULE_GLOBAL_METHOD_LOGGER, NULL);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_LOGGER_SETUP_FAILED_EXCEPTION);
		logPythonErrW(theLogger(), errMsg, false);
	}

	if (pResult) {
		Py_DECREF(pResult);
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_LOGGER_SETUP_FAILED);
		logPythonErrW(theLogger(), errMsg, false);
	}
}

///---------------------------------------------------------
// Publish::GetGlobalHandler
//
// Return the python GetGlobalHandler
//
// [Returns]
//  PyObject * - success - the globalhandler object
//  NULL - error

PyObject *Publish::GetGlobalHandler() {
	PyObject *pArgs;
	PyObject *pDict = NULL;
	PyObject *pGlobal = NULL;
	PyObject *pFunc = NULL;

	pDict = PyModule_GetDict(m_pGlobal);

	pFunc = PyDict_GetItemString(pDict, PYTHON_MODULE_GLOBAL_METHOD_GLOBAL);

	if (pFunc && PyCallable_Check(pFunc)) {
		pArgs = PyTuple_New(0);

		try {
			pGlobal = PyObject_CallObject(pFunc, pArgs);
		} catch (...) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_GLOBAL_EXCEPTION);
			logPythonErrW(theLogger(), errMsg, false);
		}

		Py_DECREF(pArgs);

		if (!pGlobal) {
			CStringW errMsg;

			errMsg.LoadString(IDS_PYTHON_XFER_GLOBAL_ERROR);
			logPythonErrW(theLogger(), errMsg, false);

			errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
			return NULL;
		}

		if (RegisterExceptionHandler(pGlobal)) {
			SetupPythonGlobalLogger(pGlobal);
			return pGlobal;
		} else {
			Py_DECREF(pGlobal);
		}
	}

	return NULL;
}

///---------------------------------------------------------
// Publish::GetUploadItemManager
//
// Return the python uploadItemManager.
//
// [Returns]
//  PyObject * - success - the uploaditemmanager
//  NULL - error
PyObject *Publish::GetUploadItemManager() {
	bool errReported = false;

	PyObject *pArgs;
	PyObject *pDict = NULL;
	PyObject *pUploadItemManager = NULL;
	PyObject *pFunc = NULL;

	pDict = PyModule_GetDict(m_pModuleUploadItemManager);

	pFunc = PyDict_GetItemString(pDict, PYTHON_MODULE_ITEM_MANAGER_METHOD_MANAGER);

	if (pFunc && PyCallable_Check(pFunc)) {
		pArgs = PyTuple_New(1);

		PyObject *arg = PyString_FromString("1");
		if (!arg) {
			Py_DECREF(pArgs);
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_UPLOAD_MGR_FAILED);
			logPythonErrW(theLogger(), errMsg, false);
			return NULL;
		}
		PyTuple_SetItem(pArgs, 0, arg);

		try {
			pUploadItemManager = PyObject_CallObject(pFunc, pArgs);
		} catch (...) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_UPLOAD_MGR_EXCEPTION);
			logPythonErrW(theLogger(), errMsg, false);
			errReported = true;
		}

		Py_DECREF(pArgs);

		if (pUploadItemManager) {
			return pUploadItemManager;
		} else if (!errReported) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_UPLOAD_MGR_FAILED);
			logPythonErrW(theLogger(), errMsg, false);
		}
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_UPLOAD_MGR_FAILED);
		logPythonErrW(theLogger(), errMsg, false);
	}

	CStringW errMsg;
	errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
	AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

	return NULL;
}

///---------------------------------------------------------
// Publish::GetGameUpload
//
// Return the python GameUpload object.
//
// [Returns]
//  PyObject * - success - the GameUpload object
//  NULL - error

PyObject *Publish::GetGameUpload(CStringA gameId) {
	bool errReported = false;

	PyObject *pArgs;
	PyObject *pDict = NULL;
	PyObject *pGameUpload = NULL;
	PyObject *pFunc = NULL;

	pDict = PyModule_GetDict(m_pModuleUploadItem);

	pFunc = PyDict_GetItemString(pDict, PYTHON_MODULE_ITEM_GAMEUPLOAD);

	if (pFunc && PyCallable_Check(pFunc)) {
		pArgs = PyTuple_New(1);

		PyObject *arg = PyString_FromString((const char *)gameId);
		if (!arg) {
			Py_DECREF(pArgs);
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_GAME_UPLOAD_ERROR);
			logPythonErrW(theLogger(), errMsg, false);
			return NULL;
		}
		PyTuple_SetItem(pArgs, 0, arg);

		try {
			pGameUpload = PyObject_CallObject(pFunc, pArgs);
		} catch (...) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_GAME_UPLOAD_EXCEPTION);
			logPythonErrW(theLogger(), errMsg, false);
			errReported = true;
		}

		Py_DECREF(pArgs);

		if (pGameUpload) {
			return pGameUpload;
		} else if (!errReported) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_GAME_UPLOAD_FAILED);
			logPythonErrW(theLogger(), errMsg, false);
		}
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_GAME_UPLOAD_FAILED);
		logPythonErrW(theLogger(), errMsg, false);
	}

	CStringW errMsg;
	errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
	AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

	return NULL;
}

///---------------------------------------------------------
// Publish::AddToUpload
//
// Add an individual uploaditem to the GameUpload batch. Once
// all items are included in the batch, the uploadItemManager will be given
// the GameUpload batch and a startupload message will
// be given that kicks off the upload procedure.
//
// [Returns]
//  bool  - true - success
//          false - error

bool Publish::AddToUpload(PyObject *pGameUpload, PyObject *pUploadItem) {
	bool ret = true;

	PyObject *pAddResult = NULL;

	try {
		pAddResult = PyObject_CallMethod(pGameUpload, PYTHON_MODULE_ITEM_GAMEUPLOAD_METHOD_ADD, "O", pUploadItem);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_ADD_UPLOAD_EXCEPTION);
		logPythonErrW(theLogger(), errMsg, false);
		ret = false;
	}

	if (pAddResult) {
		Py_DECREF(pAddResult);
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_ADD_UPLOAD_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		ret = false;
	}

	if (!ret) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
	}

	return ret;
}

///---------------------------------------------------------
// Publish::RegisterItemStatusHandler
//
// Push down call back method to python. This will be invoked
// during the python transfer to feed status.
//
// [Returns]
//  bool - true - success
//         false - error
bool Publish::RegisterItemStatusHandler(PyObject *pUploadItemManager) {
	bool ret = true;

	PyObject *pRegisterResult = NULL;

	PyObject *callbackModule = Py_InitModule("kepedit", publish_item_callback_methods);
	PyObject *pDict = PyModule_GetDict(callbackModule);
	PyObject *pFunc = PyDict_GetItemString(pDict, "ItemStatusUpdateEventCallback");

	try {
		pRegisterResult = PyObject_CallMethod(pUploadItemManager, PYTHON_MODULE_ITEM_MANAGER_METHOD_REGISTER, "sO", PYTHON_MODULE_ITEM_MANAGER_METHOD_ITEMSTATUSUPDATE, pFunc);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_REGISTER_ITEM_CALLBACK_EXCEPTION);
		logPythonErrW(theLogger(), errMsg, false);
		ret = false;
	}

	if (pRegisterResult) {
		Py_DECREF(pRegisterResult);
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_REGISTER_ITEM_CALLBACK_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		ret = false;
	}

	if (!ret) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
	}

	return ret;
}

///---------------------------------------------------------
// Publish::RegisterGameStatusHandler
//
// Push down call back method to python. This will be invoked
// during the python transfer to feed status.
//
// [Returns]
//  bool - true - success
//         false - error
bool Publish::RegisterGameStatusHandler(PyObject *pUploadItemManager) {
	bool ret = true;

	PyObject *pRegisterResult = NULL;

	PyObject *callbackModule = Py_InitModule("kepedit", publish_game_callback_methods);
	PyObject *pDict = PyModule_GetDict(callbackModule);
	PyObject *pFunc = PyDict_GetItemString(pDict, "GameStatusUpdateEventCallback");

	try {
		pRegisterResult = PyObject_CallMethod(pUploadItemManager, PYTHON_MODULE_ITEM_MANAGER_METHOD_REGISTER, "sO", PYTHON_MODULE_ITEM_MANAGER_METHOD_GAMESTATUSUPDATE, pFunc);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_REGISTER_GAME_CALLBACK_EXCEPTION);
		logPythonErrW(theLogger(), errMsg, false);
		ret = false;
	}

	if (pRegisterResult) {
		Py_DECREF(pRegisterResult);
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_REGISTER_GAME_CALLBACK_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		ret = false;
	}

	if (!ret) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
	}

	return ret;
}

///---------------------------------------------------------
// Publish::RegisterExceptionHandler
//
// Push down call back method to python. This will be invoked
// if the python transfer modules catch exceptions when they
// are processing on their own threads.
//
// [Returns]
//  bool - true - success
//         false - error
bool Publish::RegisterExceptionHandler(PyObject *pGlobal) {
	bool ret = true;

	PyObject *pRegisterResult = NULL;

	PyObject *callbackModule = Py_InitModule("kepedit", exception_callback_methods);
	PyObject *pDict = PyModule_GetDict(callbackModule);
	PyObject *pFunc = PyDict_GetItemString(pDict, "PythonExceptionCallback");

	try {
		pRegisterResult = PyObject_CallMethod(pGlobal, PYTHON_MODULE_GLOBAL_METHOD_REGISTER, "sO", PYTHON_MODULE_GLOBAL_METHOD_UNHANDLED_EXCEPTION, pFunc);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_REGISTER_EXCEPTION_CALLBACK_EXCEPTION);
		logPythonErrW(theLogger(), errMsg, false);
		ret = false;
	}

	if (pRegisterResult) {
		Py_DECREF(pRegisterResult);
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_REGISTER_EXCEPTION_CALLBACK_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		ret = false;
	}

	if (!ret) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
	}

	return ret;
}

///---------------------------------------------------------
// Publish::AddBatchToUploadManager
//
// Adds the file batch represented by pGameUpload to the uploadItemManager
// in preparation for kicking off the upload.
//
// [Returns]
//  bool - true - success
//         false - error
bool Publish::AddBatchToUploadManager(PyObject *pUploadItemManager, PyObject *pGameUpload) {
	bool ret = true;

	PyObject *pAddBatchResult = NULL;

	try {
		pAddBatchResult = PyObject_CallMethod(pUploadItemManager, PYTHON_MODULE_ITEM_MANAGER_METHOD_ADD_GAME_UPLOAD, "O", pGameUpload);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_ADD_BATCH_EXCEPTION);
		logPythonErrW(theLogger(), errMsg, false);
		ret = false;
	}

	if (pAddBatchResult) {
		Py_DECREF(pAddBatchResult);
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_ADD_BATCH_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		ret = false;
	}

	if (!ret) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
	}

	return ret;
}

///---------------------------------------------------------
// Publish::StartUpload
//
// Kicks off the python module upload to the upload server. Prior to calling,
// file are added to the uploadmgr.
//
// [Returns]
//  bool - true - success
//         false - error
bool Publish::StartUpload(PyObject *pUploadItemManager) {
	bool ret = true;

	PyObject *pStartResult = NULL;

	try {
		pStartResult = PyObject_CallMethod(pUploadItemManager, PYTHON_MODULE_ITEM_MANAGER_METHOD_START_UPLOAD, NULL);
	} catch (...) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_START_UPLOAD_EXCEPTION);
		logPythonErrW(theLogger(), errMsg, false);
		ret = false;
	}

	if (pStartResult) {
		Py_DECREF(pStartResult);
	} else {
		CStringW errMsg;
		errMsg.LoadString(IDS_PYTHON_XFER_START_UPLOAD_ERROR);
		logPythonErrW(theLogger(), errMsg, false);

		ret = false;
	}

	if (!ret) {
		CStringW errMsg;
		errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
		AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
	}

	return ret;
}

///---------------------------------------------------------
// Publish::_Transfer
//
// Transfers given folder after all the python modules have
// been initialized.
//
// [Returns]
//  None.

void Publish::_Transfer(CStringA user, CStringA pass, CStringA inputPath, CStringA gameId) {
	PyObject *pSession = Login(user, pass);

	if (pSession) {
		PyObject *pGameUpload = GetGameUpload(gameId);

		if (pGameUpload) {
			Publish::m_pGameUpload = pGameUpload;

			PyObject *pUploadItemManager = GetUploadItemManager();

			if (pUploadItemManager) {
				Publish::m_pUploadItemManager = pUploadItemManager;

				if (RegisterItemStatusHandler(pUploadItemManager) &&
					RegisterGameStatusHandler(pUploadItemManager)) {
#ifdef PSM
					PSM_End();
#else
					PyEval_ReleaseThread(m_threadState);
#endif

					strPairVect serverFilesToTransfer, patchFilesToTransfer;

					GetFolderFiles(serverFilesToTransfer, inputPath, PathAdd(PathAdd(inputPath, SERVER_FILE_LOCATION).c_str(), "*.*").c_str());
					GetFolderFiles(patchFilesToTransfer, inputPath, PathAdd(PathAdd(inputPath, PATCH_FILE_LOCATION).c_str(), "*.*").c_str());

					strPairVect allFilesToTransfer;

					for (strPairVect::iterator i = serverFilesToTransfer.begin(); i != serverFilesToTransfer.end(); i++) {
						allFilesToTransfer.push_back(make_pair(i->first, i->second));
					}

					for (strPairVect::iterator i = patchFilesToTransfer.begin(); i != patchFilesToTransfer.end(); i++) {
						allFilesToTransfer.push_back(make_pair(i->first, i->second));
					}

					Publish::m_numFilesToXfer = allFilesToTransfer.size();
					Publish::m_numFilesXferCompleted = 0;
					Publish::m_totalBytesXfer = 0;
					Publish::m_totalBytesToXfer = 0;

					TransferFolderFiles(pUploadItemManager, pGameUpload, allFilesToTransfer);

#ifdef PSM
					PSM_Begin(false);
#else
					PyEval_AcquireThread(m_threadState);
#endif

					Py_DECREF(pUploadItemManager);
				} else {
#ifdef PSM
					PSM_End();
#else
					PyEval_ReleaseThread(m_threadState);
#endif
				}
			}

			Py_DECREF(pGameUpload);
		}

		Py_DECREF(pSession);
	}
}

///---------------------------------------------------------
// Publish::TransferFolderFiles
//
// Submits a set of files for transfer to the python modules.
// Initiates the transfer. Wait for completion.
//
// [Returns]
//  None.

void Publish::TransferFolderFiles(PyObject *pUploadItemManager, PyObject *pGameUpload, IN strPairVect &filesToTransfer) {
	bool filesComplete = true;

#ifdef PSM
	PSM_Begin(false);
#else
	PyEval_AcquireThread(m_threadState);
#endif

	for (strPairVect::iterator i = filesToTransfer.begin(); i != filesToTransfer.end(); i++) {
		PyObject *pUploadItem = CreateUploadItem(i->first.c_str(), i->second.c_str());

		if (pUploadItem) {
			if (!AddToUpload(pGameUpload, pUploadItem)) {
				CStringW errMsg;
				errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_ERROR);
				AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

				Py_DECREF(pUploadItem);

				break;
			} else {
				LOG4CPLUS_DEBUG(theLogger(), "File added to xfer module game upload list = " << i->first.c_str() << endl);
				Py_DECREF(pUploadItem);
			}
		} else {
			CStringW errMsg;
			errMsg.LoadString(IDS_PYTHON_XFER_CREATE_UPLOAD_ITEM_ERROR);
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);

			filesComplete = false;
			break;
		}
	}

	if (filesComplete && AddBatchToUploadManager(pUploadItemManager, pGameUpload) && StartUpload(pUploadItemManager)) {
		//DWORD tid = GetCurrentThreadId();

#ifdef PSM
		PSM_End();
#else
		PyEval_ReleaseThread(m_threadState);
#endif

		time(&Publish::m_timeoutStart);

		/*int timerId = */ ::SetTimer(m_progressHwnd, CHECK_TIMEOUT, XFER_TIMEOUT_SECONDS * 1000, (TIMERPROC)CheckTimeout);

		// Wait for completion of the download
		// if no errors above

		do {
			Sleep(1000);

		} while (!m_XferComplete);

		if (IsWindow(m_progressHwnd)) // Maybe already destroyed
		{
			::KillTimer(m_progressHwnd, CHECK_TIMEOUT);
		}
	} else {
#ifdef PSM
		PSM_End();
#else
		PyEval_ReleaseThread(m_threadState);
#endif
	}
}

///---------------------------------------------------------
// Publish::GetFolderFiles
//
// Enumerates over a give directory adding all files to the fileSet.
// Recursive in nature.
//
// [Returns]
//  None.

void Publish::GetFolderFiles(INOUT strPairVect &fileSet, CStringA baseDir, CStringA sourceDir) {
	jsEnumFiles ef(Utf8ToUtf16(sourceDir).c_str(), true);
	while (ef.next()) {
		if (ef.isDirectory()) {
			CStringW path = ef.currentPath();
			GetFolderFiles(fileSet, baseDir, PathAdd(Utf16ToUtf8(ef.currentPath()).c_str(), "*.*").c_str());
		} else {
			CStringW path = ef.currentPath();
			CStringW loc = path.Right(path.GetLength() - baseDir.GetLength() - 1);
			int lastSlash = loc.ReverseFind('\\');
			std::string locOnly = Utf16ToUtf8(loc.Left(lastSlash)).c_str();

			if (!ExcludeFile(locOnly.c_str())) {
				addStringNoDupes(fileSet, Utf16ToUtf8(ef.currentPath()).c_str(), locOnly);
			}
		}
	}
}

///---------------------------------------------------------
// Publish::ExcludeFile
//
// Calls out specific files that should be excluded from the
// publishing process.
//
// [Returns]
//  bool    true - exclude the file
//          false - ok to include

bool Publish::ExcludeFile(CStringA loc) {
	if (loc == PathAdd(PathAdd(SERVER_FILE_LOCATION, GAMEFILES).c_str(), ((CKEPEditApp *)AfxGetApp())->GetView()->GetGame()->SVR()).c_str()) {
		return true;
	}

	return false;
}

///---------------------------------------------------------
// Publish::InitProgressDlgParams
//
// Initialize a set of static variables used by the callback function
// invoked from the python modules.
//
// [Returns]
//  None

void Publish::InitProgressDlgParams(HWND dialogHwnd, HANDLE closeEvent, HANDLE pubCloseEvent) {
	m_pUploadItemManager = NULL;
	m_progressHwnd = dialogHwnd;
	m_closeEvent = closeEvent;
	m_pubCloseEvent = pubCloseEvent;
	m_numFilesToXfer = 0;
	m_numFilesXferCompleted = 0;
	m_XferComplete = false;
	m_XferCanceled = false;
	m_callbackLogger = CallbackLogger();
}

///---------------------------------------------------------
// Publish::Transfer
//
// High level entry point for transferring a folder to the Upload server via
// python modules.
//
// [Returns]
//  None

void Publish::Transfer(HWND dialogHwnd, CStringA user, CStringA pass, CStringA inputPath, CStringA gameId, HANDLE closeEvent, HANDLE pubCloseEvent, Logger &logger) {
	m_callbackLogger = &logger;

	if (!GetInitialized()) {
		if (!Initialize(inputPath)) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
			return;
		}
	}

	InitProgressDlgParams(dialogHwnd, closeEvent, pubCloseEvent);

	_Transfer(user, pass, inputPath, gameId);

	DeInitialize();
}

///---------------------------------------------------------
// Publish::GetAccountInfo
//
// Login the user and obtain their account information. This is performed
// via the python modules in coordination with the UploadServer.
//
// [Returns]
//  bool    true - success
//          false - error

bool Publish::GetAccountInfo(CStringA user, CStringA pass, CStringA gameDir, CStringA &accountXML, Logger &logger) {
	m_callbackLogger = &logger;

	bool ret = true;

	PyObject *pSession = NULL;
	PyObject *pAccountData = NULL;

	char *account_data;
	accountXML.Empty();

	HCURSOR prev = SetCursor(::LoadCursor(NULL, IDC_WAIT));

	if (!GetInitialized()) {
		if (!Initialize(gameDir)) {
			CStringW errMsg;
			errMsg.LoadString(IDS_PUBDIST_XFER_MODULE_INITIALIZATION_PROBLEM);
			SetCursor(prev);
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
			return false;
		}
	}

	pSession = Login(user, pass);

	if (pSession) {
		try {
			pAccountData = PyObject_CallMethod(pSession, PYTHON_MODULE_SESSION_METHOD_GETACCOUNTDATA, NULL);
		} catch (...) {
			CStringW errMsg;
			errMsg.Format(IDS_PYTHON_XFER_ACCOUNT_EXCEPTION, Utf8ToUtf16(user).c_str());
			logPythonErrW(theLogger(), errMsg, false);
			ret = false;
		}

		if (pAccountData) {
			if (PyString_Check(pAccountData)) {
				account_data = PyString_AsString(pAccountData);
				accountXML = account_data;
			} else {
				ret = false;
			}

			Py_DECREF(pAccountData);
		}

		if (accountXML.IsEmpty()) {
			CStringW errMsg;
			errMsg.Format(IDS_PYTHON_XFER_ACCOUNT_DATA_ERR, Utf8ToUtf16(user).c_str());
			logPythonErrW(theLogger(), errMsg, true);

			errMsg.LoadString(IDS_PUBDIST_LOGIN_FAILURE);
			AfxMessageBox(errMsg, MB_ICONEXCLAMATION);
			ret = false;
		}
	} else {
		ret = false;
	}

	DeInitialize();

	SetCursor(prev);

	return ret;
}
