/******************************************************************************
 NewGameInfo.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// CNewGameInfo dialog

class CNewGameInfo : public CDialog {
	DECLARE_DYNAMIC(CNewGameInfo)

public:
	CNewGameInfo(CWnd* pParent = NULL); // standard constructor
	virtual ~CNewGameInfo();

	BOOL Show();
	// Dialog Data
	enum { IDD = IDD_NEW_GAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
