#pragma once
#include "afxwin.h"

// CNewGameInfoDlg dialog

class CNewGameInfoDlg : public CDialog {
	DECLARE_DYNAMIC(CNewGameInfoDlg)

public:
	CNewGameInfoDlg(bool allowStandAloneCreation, CWnd* pParent = NULL); // standard constructor
	virtual ~CNewGameInfoDlg();

	// Dialog Data
	enum { IDD = IDD_NEW_GAME_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	void OnEnChangeGameName();

	DECLARE_MESSAGE_MAP()
public:
	// name to send to web service
	CStringA m_name;
	// description to send to web service
	CStringA m_desc;
	BOOL m_standAloneApp;
	bool m_allowStandAloneCreation;
	afx_msg void OnBnClicked3dApp();
};
