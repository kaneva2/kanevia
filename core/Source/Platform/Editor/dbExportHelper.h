#pragma once

#include <jsLibraryFunction.h>
#include <DBStrings.h>
#include "KEPException.h"

#define EXPORTER_DLL_NAME "dbExporter.dll"
#define EXPORTTODB "ExportToDb"
#define EDITDBSETTINGS "EditDbSettings"

typedef bool (*ExportToDbFn)(HWND parent, const char* gameDir, const char* exportSqlPath, IGetSet* engine, bool checkForDbUpdate);
typedef INT (*EditDbSettingsFn)(HWND parent, const char* gameDir, bool checkConfigToShow);

inline bool CanExportToDb(IN const char* baseDir, IN Logger& logger) {
	jsLibraryFunction dbLib(PathAdd(baseDir, EXPORTER_DLL_NAME).c_str());

	ExportToDbFn ExportToDb = (ExportToDbFn)dbLib.getFunction(EXPORTTODB);
	if (ExportToDb == 0) {
		string msg;
		LOG4CPLUS_DEBUG(logger, "Export to db unavailble.  Error code: " << errorNumToString(dbLib.lastError(), msg));
	}
	return ExportToDb != 0;
}

inline INT EditDbSettings(HWND parent, const char* baseDir, const char* gameDir, bool checkConfigToShow) {
	jsLibraryFunction dbLib(PathAdd(baseDir, EXPORTER_DLL_NAME).c_str());

	EditDbSettingsFn EditDbSettings = (EditDbSettingsFn)dbLib.getFunction(EDITDBSETTINGS);
	if (EditDbSettings != 0) {
		return EditDbSettings(parent, gameDir, checkConfigToShow);
	} else {
		return IDOK; // let them continue
	}
}

inline void ExportGetSetToDb(IN HWND parent, IN const char* baseDir, const char* gameDir, IN const char* exportSqlPath, IN IGetSet* engine, IN bool checkForDbUpdate) {
	jsLibraryFunction dbLib(PathAdd(baseDir, EXPORTER_DLL_NAME).c_str());

	ExportToDbFn ExportToDb = (ExportToDbFn)dbLib.getFunction(EXPORTTODB);
	if (ExportToDb) {
		if (!ExportToDb(parent, gameDir, exportSqlPath, engine, checkForDbUpdate)) {
			// this causes some major leaking and will make MFC bark in debug mode, but
			// bubbling up error very painful
			throw new KEP::KEPException(loadStr(IDS_DB_EXPORT_FAILED), E_INVALIDARG);
		}
	}
}

