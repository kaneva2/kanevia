// MainFrm.cpp : implementation of the CMainFrame class
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved

#include "stdafx.h"
#include "kepcommon.h"
#include "KEPEdit.h"
#include "KEPEditGlobals.h"

#include "InformationDlg.h"
#include "StatusDlg.h"

#include "MainFrm.h"
#include ".\mainfrm.h"
#include "Game.h"
#include <set>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
ON_WM_CREATE()
ON_WM_CLOSE()
ON_COMMAND(ID_EDIT_EDITORACCEPTTEXT, OnEditEditoraccepttext)
ON_COMMAND(ID_VIEW_GAMESTATUS, OnViewGamestatus)
ON_COMMAND(ID_VIEW_GAMETREE, OnViewGametree)
ON_COMMAND(ID_VIEW_GAMEHELP, OnViewGamehelp)
ON_COMMAND(ID_VIEW_GAMESETTINGS, OnViewGameSettings)
ON_WM_MENUSELECT()
ON_COMMAND(ID_VIEW_EDITORTOOLS, OnViewEditortools)
ON_UPDATE_COMMAND_UI(ID_MANAGE_TEXTURE_DATABASE, OnUpdateViewTextureDBTool)
ON_UPDATE_COMMAND_UI(ID_PLACE_FIRST_ACTOR, OnUpdatePlaceFirstActor)
ON_UPDATE_COMMAND_UI(ID_TOGGLE_WIREFRAME, OnUpdateToggleWireframe)
ON_UPDATE_COMMAND_UI(ID_TOGGLE_LOCK_GAME_WINDOW, OnUpdateToggleLockGameWindow)
ON_UPDATE_COMMAND_UI(ID_VIEW_EDITORTOOLS, OnViewEditorToolsMenu)
ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()

static UINT indicators[] = {
	ID_SEPARATOR, // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// Helpers for saving/restoring window state

static BOOL PASCAL NEAR ReadWindowPlacement(LPWINDOWPLACEMENT pwp) {
	CStringW strBuffer = AfxGetApp()->GetProfileString(szSection, szWindowPos);
	if (strBuffer.IsEmpty())
		return FALSE;

	WINDOWPLACEMENT wp;
	int nRead = _stscanf_s(strBuffer, szFormat,
		&wp.flags, &wp.showCmd,
		&wp.ptMinPosition.x, &wp.ptMinPosition.y,
		&wp.ptMaxPosition.x, &wp.ptMaxPosition.y,
		&wp.rcNormalPosition.left, &wp.rcNormalPosition.top,
		&wp.rcNormalPosition.right, &wp.rcNormalPosition.bottom);

	if (nRead != 10)
		return FALSE;

	wp.length = sizeof wp;
	*pwp = wp;

	return TRUE;
}

static void PASCAL NEAR WriteWindowPlacement(LPWINDOWPLACEMENT pwp)
// write a window placement to settings section of app's ini file
{
	CStringW strBuffer;
	CRect rect;

	strBuffer.Format(szFormat,
		pwp->flags, pwp->showCmd,
		pwp->ptMinPosition.x, pwp->ptMinPosition.y,
		pwp->ptMaxPosition.x, pwp->ptMaxPosition.y,
		pwp->rcNormalPosition.left, pwp->rcNormalPosition.top,
		pwp->rcNormalPosition.right, pwp->rcNormalPosition.bottom);

	AfxGetApp()->WriteProfileString(szSection, szWindowPos, strBuffer);
}

// CMainFrame construction/destruction

CMainFrame::CMainFrame() :
		m_wndTreeBar(100, 200, 220, -1),
		m_wndInformation(100, 200, 200, 400, false),
		m_wndKEPDialogBar(100, 200, 250, -1),
		m_wndKEPStatusBar(100, 200, 200, 400),
		m_lastMenuId(0),
		m_controlBarInited(false) {
	m_gameOpen = FALSE;
	m_zoneOpen = FALSE;
	m_game = NULL;
}

CMainFrame::~CMainFrame() {
}

CSize GetDialogSize(const wchar_t* pszTemplName, CWnd* pParent); // in KEPDialogBar

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	WINDOWPLACEMENT wp;
	if (ReadWindowPlacement(&wp))
		SetWindowPlacement(&wp);

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME)) {
		TRACE0("Failed to create toolbar\n");
		return -1; // fail to create
	}

	CRect rect(0, 0, 0, 0);
	// Accepting the default ID for the additional toolbar will cause the above toolbar
	// to conflict when saving and loading its state information, e.g. position, float, etc.

	if (!m_wndGameToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, rect, IDR_GAME_TOOLS) ||
		!m_wndGameToolBar.LoadToolBar(IDR_GAME_TOOLS)) {
		TRACE0("Failed to create game toolbar\n");
		return -1; // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
			sizeof(indicators) / sizeof(UINT))) {
		TRACE0("Failed to create windows status bar\n");
		return -1; // fail to create
	}

	UINT dwCtrlBarStyle = WS_CHILD | WS_VISIBLE | CBRS_TOP;

	if (!m_wndTreeBar.Create(this, IDS_PROJECT_TREE, KEP_PROJECTTREE, dwCtrlBarStyle)) {
		TRACE0("Failed to create Treebar\n");
		return -1; // fail to create
	}

	if (!m_wndInformation.Create(this, IDS_INFORMATION, KEP_PROPERTYTREE, dwCtrlBarStyle))
		return -1;

	if (!m_wndKEPStatusBar.Create(this, IDS_STATUS, KEP_STATUSBAR, dwCtrlBarStyle))
		return -1;

	// use this as to get the width
	CSize cs = GetDialogSize(MAKEINTRESOURCE(IDD_DIALOG_ANIMTEXTURES), this);
	/*DWORD scrollWidth = */ GetSystemMetrics(SM_CXVSCROLL);

	//		if(!m_wndKEPDialogBar.Create(s, this, CSize(cs.cx + scrollWidth + 5, 250), TRUE, KEP_DIALOGBAR ) )
	if (!m_wndKEPDialogBar.Create(this, IDS_SETTINGS, KEP_DIALOGBAR, dwCtrlBarStyle))
		return -1;

	m_wndTreeBar.SetBarStyle(m_wndTreeBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC | CBRS_ORIENT_VERT);
	m_wndInformation.SetBarStyle(m_wndTreeBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndKEPStatusBar.SetBarStyle(m_wndTreeBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndKEPDialogBar.SetBarStyle(m_wndTreeBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	m_wndKEPDialogBar.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
	m_wndKEPStatusBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndInformation.EnableDocking(CBRS_ALIGN_ANY);
	m_wndTreeBar.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndGameToolBar.EnableDocking(CBRS_ALIGN_ANY);

	EnableDocking(CBRS_ALIGN_ANY);

#ifdef _SCB_REPLACE_MINIFRAME
	m_pFloatingFrameClass = RUNTIME_CLASS(CSCBMiniDockFrameWnd);
#endif //_SCB_REPLACE_MINIFRAME

	DockControlBar(&m_wndToolBar);
	DockControlBar(&m_wndGameToolBar);
	DockControlBar(&m_wndTreeBar, AFX_IDW_DOCKBAR_LEFT);
	DockControlBarUnder(&m_wndInformation, &m_wndTreeBar);
	DockControlBarUnder(&m_wndKEPStatusBar, &m_wndInformation);
	DockControlBar(&m_wndKEPDialogBar, AFX_IDW_DOCKBAR_RIGHT);

	// initialize the help
	CInformationDlg* dlg = new CInformationDlg();
	dlg->Create(CInformationDlg::IDD, &m_wndInformation);
	m_wndInformation.SetDialogBar(dlg, CInformationDlg::IDD);

	CStatusDlg* statDlg = new CStatusDlg();
	statDlg->Create(CStatusDlg::IDD, &m_wndKEPStatusBar);
	m_wndKEPStatusBar.SetDialogBar(statDlg, CStatusDlg::IDD);

	// hide all dlgs initially
	m_wndInformation.ShowWindow(SW_HIDE);
	m_wndGameToolBar.ShowWindow(SW_HIDE);
	m_wndKEPDialogBar.ShowWindow(SW_HIDE);
	m_wndKEPStatusBar.ShowWindow(SW_HIDE);
	m_wndToolBar.ShowWindow(SW_HIDE);
	m_wndTreeBar.ShowWindow(SW_HIDE);
	m_wndStatusBar.ShowWindow(SW_HIDE);
	return 0;
}

void CMainFrame::DockControlBarUnder(CKEPControlBar* Bar, CKEPControlBar* Under) {
	CRect rect;
	DWORD dw;
	UINT n;

	// get MFC to adjust the dimensions of all docked ToolBars
	// so that GetWindowRect will be accurate
	RecalcLayout();
	Under->GetWindowRect(&rect);
	rect.OffsetRect(0, (rect.bottom + 1));
	dw = Under->GetBarStyle();
	n = 0;
	n = (dw & CBRS_ALIGN_TOP) ? AFX_IDW_DOCKBAR_TOP : n;
	n = (dw & CBRS_ALIGN_BOTTOM && n == 0) ? AFX_IDW_DOCKBAR_BOTTOM : n;
	n = (dw & CBRS_ALIGN_LEFT && n == 0) ? AFX_IDW_DOCKBAR_LEFT : n;
	n = (dw & CBRS_ALIGN_RIGHT && n == 0) ? AFX_IDW_DOCKBAR_RIGHT : n;

	// When we take the default parameters on rect, DockControlBar will dock
	// each Toolbar on a seperate line.  By calculating a rectangle, we in effect
	// are simulating a Toolbar being dragged to that location and docked.
	DockControlBar(Bar, n, &rect);
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs) {
	if (!CFrameWnd::PreCreateWindow(cs))
		return FALSE;

	cs.style &= ~(LONG)FWS_ADDTOTITLE;

	return TRUE;
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const {
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const {
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case KEP_TREECHANGE: {
			m_wndKEPDialogBar.SetDialogBar(static_cast<IKEPDialogProcessor*>(appInstance.GetView()), ((LPKEPTREEDATA)lParam)->dialogId, ((LPKEPTREEDATA)lParam)->dialogType);
			AlterGameedit(ID_VIEW_GAMESETTINGS, TRUE);
			((CInformationDlg*)m_wndInformation.Dialog())->LoadHelp(((LPKEPTREEDATA)lParam)->dialogId);
			return 0;
		} break;
		default:
			return CFrameWnd::WindowProc(message, wParam, lParam);
	}
}

void CMainFrame::OnClose() {
	//check if changes need to be saved
	if (m_wndKEPDialogBar.cleanUp() && CheckGameFilesChanges()) {
		CFrameWnd::OnClose();
	}
}

void CMainFrame::OnEditEditoraccepttext() {
	CMenu* menu = GetMenu();

	UINT state = menu->GetMenuState(ID_EDIT_EDITORACCEPTTEXT, MF_BYCOMMAND);

	ASSERT(state != 0xFFFFFFFF);

	if (state & MF_CHECKED) {
		menu->CheckMenuItem(ID_EDIT_EDITORACCEPTTEXT, MF_UNCHECKED | MF_BYCOMMAND);
		appInstance.GetView()->SetEditorAcceptText(FALSE);
	} else {
		menu->CheckMenuItem(ID_EDIT_EDITORACCEPTTEXT, MF_CHECKED | MF_BYCOMMAND);
		appInstance.GetView()->SetEditorAcceptText(TRUE);
	}
}
void CMainFrame::OnViewGamestatus() {
	CMenu* menu = GetMenu();

	UINT state = menu->GetMenuState(ID_VIEW_GAMESTATUS, MF_BYCOMMAND);

	ASSERT(state != 0xFFFFFFFF);

	if (state & MF_CHECKED) {
		AlterGameedit(ID_VIEW_GAMESTATUS, FALSE);
	} else {
		AlterGameedit(ID_VIEW_GAMESTATUS, TRUE);
	}
}

void CMainFrame::OnViewGametree() {
	CMenu* menu = GetMenu();

	UINT state = menu->GetMenuState(ID_VIEW_GAMETREE, MF_BYCOMMAND);

	ASSERT(state != 0xFFFFFFFF);

	if (state & MF_CHECKED) {
		AlterGameedit(ID_VIEW_GAMETREE, FALSE);
	} else {
		AlterGameedit(ID_VIEW_GAMETREE, TRUE);
	}
}

void CMainFrame::OnViewGamehelp() {
	CMenu* menu = GetMenu();

	UINT state = menu->GetMenuState(ID_VIEW_GAMEHELP, MF_BYCOMMAND);

	ASSERT(state != 0xFFFFFFFF);

	if (state & MF_CHECKED) {
		AlterGameedit(ID_VIEW_GAMEHELP, FALSE);
	} else {
		AlterGameedit(ID_VIEW_GAMEHELP, TRUE);
	}
}

void CMainFrame::OnViewGameSettings() {
	CMenu* menu = GetMenu();

	UINT state = menu->GetMenuState(ID_VIEW_GAMESETTINGS, MF_BYCOMMAND);

	ASSERT(state != 0xFFFFFFFF);

	if (state & MF_CHECKED) {
		AlterGameedit(ID_VIEW_GAMESETTINGS, FALSE);
	} else {
		AlterGameedit(ID_VIEW_GAMESETTINGS, TRUE);
	}
}

void CMainFrame::AlterGameedit(UINT menuId, BOOL flipOn) {
	CMenu* menu = GetMenu();
	jsVerifyReturnVoid(menu != NULL);

	if (flipOn) {
		menu->CheckMenuItem(menuId, MF_CHECKED | MF_BYCOMMAND);
	} else {
		menu->CheckMenuItem(menuId, MF_UNCHECKED | MF_BYCOMMAND);
	}

	switch (menuId) {
		case ID_VIEW_GAMESTATUS:
			ShowControlBar(&m_wndKEPStatusBar, flipOn, FALSE);
			break;
		case ID_VIEW_GAMESETTINGS:
			ShowControlBar(&m_wndKEPDialogBar, flipOn, FALSE);
			break;
		case ID_VIEW_GAMEHELP:
			ShowControlBar(&m_wndInformation, flipOn, FALSE);
			break;
		case ID_VIEW_GAMETREE:
			ShowControlBar(&m_wndTreeBar, flipOn, FALSE);
			break;
		case ID_VIEW_EDITORTOOLS:
			ShowControlBar(&m_wndGameToolBar, flipOn, FALSE);
			break;
		default:
			break;
	}

	RecalcLayout(TRUE);
}

///---------------------------------------------------------
// CMainFrame::OnMenuSelect
//
// [in] nItemID
// [in] nFlags
// [in] hSysMenu
//
// Catch a drop down of the menu and disable any edit windows that have
// been closed by clicking the close(x) button.
//

void CMainFrame::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu) {
	if (IsMenu(hSysMenu)) {
		if (!m_wndKEPStatusBar.IsVisible()) {
			AlterGameedit(ID_VIEW_GAMESTATUS, FALSE);
		} else {
			AlterGameedit(ID_VIEW_GAMESTATUS, TRUE);
		}

		if (!m_wndKEPDialogBar.IsVisible()) {
			AlterGameedit(ID_VIEW_GAMESETTINGS, FALSE);
		} else {
			AlterGameedit(ID_VIEW_GAMESETTINGS, TRUE);
		}

		if (!m_wndInformation.IsVisible()) {
			AlterGameedit(ID_VIEW_GAMEHELP, FALSE);
		} else {
			AlterGameedit(ID_VIEW_GAMEHELP, TRUE);
		}

		if (!m_wndTreeBar.IsVisible()) {
			AlterGameedit(ID_VIEW_GAMETREE, FALSE);
		} else {
			AlterGameedit(ID_VIEW_GAMETREE, TRUE);
		}

		if (!m_wndGameToolBar.IsVisible()) {
			AlterGameedit(ID_VIEW_EDITORTOOLS, FALSE);
		} else if (IsGameOpen()) {
			AlterGameedit(ID_VIEW_EDITORTOOLS, TRUE);
		}
	}

	CFrameWnd::OnMenuSelect(nItemID, nFlags, hSysMenu);
}

BOOL CMainFrame::DestroyWindow() {
	if (m_lastMenuId == IDR_MAINMENU) {
		CKEPControlBar::CleanupAll();
		SaveBarState(_T("KEPEditDockState"));
	}
	return CFrameWnd::DestroyWindow();
}

BOOL CMainFrame::VerifyBarState(const wchar_t* lpszProfileName) {
	CDockState state;
	state.LoadState(lpszProfileName);

	for (int i = 0; i < state.m_arrBarInfo.GetSize(); i++) {
		CControlBarInfo* pInfo = (CControlBarInfo*)state.m_arrBarInfo[i];
		ASSERT(pInfo != NULL);
		int nDockedCount = pInfo->m_arrBarID.GetSize();
		if (nDockedCount > 0) {
			// dockbar
			for (int j = 0; j < nDockedCount; j++) {
				UINT nID = (UINT)pInfo->m_arrBarID[j];
				if (nID == 0)
					continue; // row separator
				if (nID > 0xFFFF)
					nID &= 0xFFFF; // placeholder - get the ID
				if (GetControlBar(nID) == NULL)
					return FALSE;
			}
		}

		if (!pInfo->m_bFloating) // floating dockbars can be created later
			if (GetControlBar(pInfo->m_nBarID) == NULL)
				return FALSE; // invalid bar ID
	}

	return TRUE;
}

///---------------------------------------------------------
// CMainFrame::SetGameStatus
//
// Refresh the items that are valid selections in the tree control
// based on whether the game and/or zone has been opened.
//
// [in] gameOpen - denotes a game has been opened
// [in] zoneOpen - denotes a zone has been opened
//
void CMainFrame::SetGameStatus(const BOOL gameOpen, const BOOL zoneOpen) {
	m_gameOpen = gameOpen;
	m_zoneOpen = zoneOpen;

	m_wndTreeBar.SetGameStatus(gameOpen, zoneOpen);
}

void CMainFrame::OnViewEditortools() {
	CMenu* menu = GetMenu();

	UINT state = menu->GetMenuState(ID_VIEW_EDITORTOOLS, MF_BYCOMMAND);

	ASSERT(state != 0xFFFFFFFF);

	if (IsGameOpen()) {
		if (state & MF_CHECKED) {
			AlterGameedit(ID_VIEW_EDITORTOOLS, FALSE);
		} else if (this->IsGameOpen()) {
			AlterGameedit(ID_VIEW_EDITORTOOLS, TRUE);
		}
	}
}

void CMainFrame::OnUpdateViewTextureDBTool(CCmdUI* pCmdUI) {
	pCmdUI->Enable(IsGameOpen());
}

void CMainFrame::OnUpdatePlaceFirstActor(CCmdUI* pCmdUI) {
	pCmdUI->Enable(IsZoneOpen());
}

void CMainFrame::OnUpdateToggleWireframe(CCmdUI* pCmdUI) {
	pCmdUI->Enable(IsZoneOpen());
	pCmdUI->SetCheck(((IClientEngine*)IClientEngine::Instance())->GetShowWireframe());
}

void CMainFrame::OnUpdateToggleLockGameWindow(CCmdUI* pCmdUI) {
	pCmdUI->Enable(IsZoneOpen());
	pCmdUI->SetCheck(((IClientEngine*)IClientEngine::Instance())->GetLockGameWindow());
}

void CMainFrame::OnViewEditorToolsMenu(CCmdUI* pCmdUI) {
	pCmdUI->Enable(IsGameOpen());
}

void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam) {
	switch (nID) {
		case SC_MINIMIZE:
			((IClientEngine*)IClientEngine::Instance())->SetStopRendering(true);
			break;

		case SC_RESTORE:
		case SC_MAXIMIZE:
			((IClientEngine*)IClientEngine::Instance())->SetStopRendering(false);
			break;
	}

	CFrameWnd::OnSysCommand(nID, lParam);
}
/**
*return true if
*1. no changes have been made to any game objects
*2. user has applied the changes
*3. user chose to discard the changes
**/
BOOL CMainFrame::CheckGameFilesChanges() {
	BOOL retVal = TRUE;
	EditorState* editorStatePtr = EditorState::GetInstance();
	EditorState::IKEPGameFileSet dirtyFiles;
	editorStatePtr->GetDirtyGameFiles(dirtyFiles);

	BOOL zoneDirty = appInstance.GetView()->GetCurrentSceneFile() != "" &&
					 appInstance.GetView()->GetCurrentSceneFile() != "NONE" &&
					 EditorState::GetInstance()->IsZoneDirty();
	//only game i snot null
	BOOL gameDirty = m_game && m_game->IsDirty();
	BOOL gameFilesDirty = dirtyFiles.size() > 0;

	if (gameDirty || zoneDirty || gameFilesDirty) {
		//build change file names
		std::set<CStringA> filesChanged;
		if (gameDirty) {
			filesChanged.insert(m_game->FileNameCStr());
		}
		if (zoneDirty) {
			filesChanged.insert(appInstance.GetView()->GetCurrentSceneFile());
		}
		if (gameFilesDirty) {
			EditorState::IKEPGameFileIterator itList;
			for (itList = dirtyFiles.begin();
				 itList != dirtyFiles.end(); itList++) {
				IKEPGameFile* filePtr = **(itList);
				if (filePtr->GetFileName() != "") {
					filesChanged.insert(filePtr->GetFileName().c_str());
				}
			}
		}

		CStringW sCaption, sText;
		sText.LoadString(filesChanged.size() > 1 ? IDS_CONFIRM_GAME_CHANGES : IDS_CONFIRM_GAME_CHANGE);
		sCaption.LoadString(filesChanged.size() > 1 ? IDS_GAME_CHANGES : IDS_GAME_CHANGE);

		std::set<CStringA>::iterator itStr;
		for (itStr = filesChanged.begin();
			 itStr != filesChanged.end(); itStr++) {
			sText += L"\n";
			sText += Utf8ToUtf16(*itStr).c_str();
		}

		int result = MessageBox(sText, sCaption, MB_ICONEXCLAMATION | MB_YESNOCANCEL);
		switch (result) {
			case IDCANCEL:
				retVal = FALSE;
				break;

			case IDYES:
				//apply changes
				SaveGameFiles(dirtyFiles);
				break;

			case IDNO:
				retVal = TRUE;
				break;
		}
	}
	return retVal;
}

void CMainFrame::SaveGameFiles(EditorState::IKEPGameFileSet& listDirty) {
	BOOL zoneDirty = appInstance.GetView()->GetCurrentSceneFile() != "" &&
					 appInstance.GetView()->GetCurrentSceneFile() != "NONE" &&
					 EditorState::GetInstance()->IsZoneDirty();
	//only game i snot null
	BOOL gameDirty = m_game && m_game->IsDirty();
	if (gameDirty) {
		m_game->Save();
	}
	if (zoneDirty) {
		appInstance.GetView()->OnEditorZoneSave();
	}
	EditorState::IKEPGameFileIterator itList;
	for (itList = listDirty.begin(); itList != listDirty.end(); itList++) {
		IKEPGameFile* gameFilePtr = **(itList);
		if (gameFilePtr->GetFileName() != "") {
			gameFilePtr->SaveFile();
		}
	}
}
/**
*return true if
*1. no changes have been made to the game
*2. user has applied the changes
*3. user chose to discard the changes
**/
BOOL CMainFrame::CheckGameChanges() {
	BOOL retVal = TRUE;
	if (m_game && m_game->IsDirty()) {
		CStringW sCaption, sText;
		sCaption.LoadString(IDS_CONFIRM_GAME_CHANGES);
		sText.LoadString(IDS_GAME_CHANGES);
		int result = MessageBox(sCaption, sText, MB_ICONEXCLAMATION | MB_YESNOCANCEL);
		switch (result) {
			case IDCANCEL:
				retVal = FALSE;
				break;

			case IDYES:
				//apply changes
				return m_game->Save();
				break;

			case IDNO:
				retVal = TRUE;
				break;
		}
	}
	return retVal;
}

void CMainFrame::SetNewMenu(UINT id) {
	if (m_lastMenuId == id) // Do nothing if not changed
		return;

	m_newMenu.LoadMenu(id);
	ASSERT(m_newMenu);

	CMenu* oldMenu = GetMenu();
	if (oldMenu)
		oldMenu->DestroyMenu();

	// Add the new menu
	SetMenu(&m_newMenu);
	m_hMenuDefault = m_newMenu.GetSafeHmenu();
	m_lastMenuId = id;
}

void CMainFrame::InitControlBars() {
	if (!m_controlBarInited && VerifyBarState(L"KEPEditDockState")) {
		CKEPControlBar::InitAll();
		LoadBarState(L"KEPEditDockState");
		m_controlBarInited = true;
	}
}