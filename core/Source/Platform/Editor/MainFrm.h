// MainFrm.h : interface of the CMainFrame class
//
//Copyright (C) 2004 Kaneva, Inc.
//All Rights Reserved

#pragma once

#include "TreeBar.h" //main project tabed tree
#include "KEPDialogBar.h"
#include "resource.h"
#include "EditorState.h"
#include "IXmlSerializableObj.h"
#include "KEPObList.h"
#include "IKEPGameFile.h"

class CMainFrame : public CFrameWnd {
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

	// Attributes
public:
	// Operations
public:
	// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CStatusBar m_wndStatusBar; //Status Bar
	CToolBar m_wndToolBar; //Windows Tool Bar
	CToolBar m_wndGameToolBar; // Tool Bar for frequently used Editor items
	CTreeBar m_wndTreeBar; //Tree Control top left
	CKEPDialogBar m_wndInformation; //info/help in lower left

	void SetNewMenu(UINT id);
	CMenu m_newMenu;
	UINT m_lastMenuId;
	bool m_controlBarInited;

protected:
	BOOL VerifyBarState(const wchar_t* lpszProfileName);
	BOOL CheckGameChanges();
	BOOL CheckGameFilesChanges();
	void SaveGameFiles(EditorState::IKEPGameFileSet& dirtyFiles);

public: // for adding
	CKEPDialogBar m_wndKEPDialogBar;
	CKEPDialogBar m_wndKEPStatusBar; //Bottom status bar
	virtual BOOL DestroyWindow();
	void SetGameStatus(const BOOL gameOpen, const BOOL zoneOpen);
	BOOL IsGameOpen() { return m_gameOpen; }
	BOOL IsZoneOpen() { return m_zoneOpen; }
	KEP::Game* GetGame() const { return m_game; }
	void SetGame(Game* game) { m_game = game; }
	void InitControlBars();

	// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void EnableGameedit();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	void DockControlBarUnder(CKEPControlBar* Bar, CKEPControlBar* Under);
	void AlterGameedit(UINT menuId, BOOL flipOn);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnEditEditoraccepttext();
	afx_msg void OnViewGamestatus();
	afx_msg void OnViewGametree();
	afx_msg void OnViewGamehelp();
	afx_msg void OnViewGameSettings();
	afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
	afx_msg void OnUpdateViewTextureDBTool(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlaceFirstActor(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToggleWireframe(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToggleLockGameWindow(CCmdUI* pCmdUI);
	afx_msg void OnViewEditorToolsMenu(CCmdUI* pCmdUI);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);

private:
	BOOL m_gameOpen;
	BOOL m_zoneOpen;
	//current game object, assigned when a game is loaded. (not the best way...will be better if
	//there is a centralized place holder where current game can be retrieved)
	KEP::Game* m_game;

public:
	afx_msg void OnViewEditortools();
};
