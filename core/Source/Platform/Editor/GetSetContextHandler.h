/******************************************************************************
 GetSetContextHandler.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// GetSetContextHandler

class GetSetContextHandler : public CWnd {
	DECLARE_DYNAMIC(GetSetContextHandler)

public:
	GetSetContextHandler();
	virtual ~GetSetContextHandler();

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
};
