/******************************************************************************
 KEPEditorTab.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// CKEPEditorTab
#define KEP_PROPERYTAB 100

//future update, if we need more data per tab
typedef struct _KEPTABDATA {
	int rootid;
} TABDATA, *LPTABDATA;

class CKEPEditorTab : public CTabCtrl {
	DECLARE_DYNAMIC(CKEPEditorTab)

public:
	CKEPEditorTab();
	virtual ~CKEPEditorTab();

	BOOL Init(HWND hWndParent);

protected:
	DECLARE_MESSAGE_MAP()

	int m_currentTab;
	HWND m_hWndParent;

private:
	CStringArray m_csArray;

public:
	afx_msg void OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult);
};
