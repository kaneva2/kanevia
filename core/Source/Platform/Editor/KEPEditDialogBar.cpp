/******************************************************************************
 KEPEditDialogBar.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "StdAfx.h"
#include ".\kepeditdialogbar.h"
#include "KEPPropertiesCtrl.h"

IMPLEMENT_DYNAMIC(CKEPEditDialogBar, CDialogBar)

CKEPEditDialogBar::CKEPEditDialogBar(void) :
		CDialogBar() {
	m_settingsApplied = TRUE;
	m_settingsChanged = FALSE;
	m_loadedFile = L"";
}

CKEPEditDialogBar::~CKEPEditDialogBar(void) {
}

BEGIN_MESSAGE_MAP(CKEPEditDialogBar, CDialogBar)
ON_REGISTERED_MESSAGE(WM_KEP_PROPERTY_CTRL_CHANGED, OnPropertyCtrlChanged)
END_MESSAGE_MAP()

void CKEPEditDialogBar::SettingsChanged() {
	m_settingsChanged = TRUE;
	m_settingsApplied = FALSE;
}

void CKEPEditDialogBar::SettingsApplied() {
	m_settingsApplied = TRUE;
}

bool CKEPEditDialogBar::ApplyChanges() {
	m_settingsApplied = TRUE;
	return true;
}

afx_msg LRESULT CKEPEditDialogBar::OnPropertyCtrlChanged(WPARAM wp, LPARAM lp) {
	SettingsChanged();
	return 0;
}

void CKEPEditDialogBar::SetLoadedFile(CStringW loadedFile, bool settingsCommitted) {
	m_loadedFile = loadedFile;
	if (settingsCommitted) {
		m_settingsChanged = false;
	}
}

void CKEPEditDialogBar::SetLoadedFile(CStringA loadedFile, bool settingsCommitted) {
	SetLoadedFile(Utf8ToUtf16(loadedFile).c_str(), settingsCommitted);
}
