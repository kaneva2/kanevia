#pragma once
#if DEPRECATED

#include "js.h"

#include <atomic>

template <class T, class InternalImpl> class jsRefCountPtr;

template <class T> class jsInternalRefCntPtrBase {
public:

	jsInternalRefCntPtrBase(T * ptr) : 
		m_refCnt(1), 
		m_ptr(ptr)
	{}

	virtual ~jsInternalRefCntPtrBase() { }

	void incCnt() {
		m_refCnt++;
	}

	void decCnt() {
		auto prevCount = m_refCnt.fetch_sub(1, std::memory_order_relaxed);
		if (prevCount == 1) {
			if (m_ptr) {
				delete m_ptr;
				m_ptr = nullptr;
			}
			delete this;
		}
	}

	T *ptr() {
		return m_ptr;
	}

	int refCnt() {
		return m_refCnt;
	}

protected:

	T *m_ptr;
	std::atomic<int> m_refCnt;
};

template <class T> class jsInternalRefCntPtr : public jsInternalRefCntPtrBase<T> {
public:
	jsInternalRefCntPtr(T * ptr) : 
		jsInternalRefCntPtrBase<T>(ptr) 
	{}
};

template <class T, class InternalImpl = jsInternalRefCntPtr<T> >
class jsRefCountPtr {
public:

	jsRefCountPtr(T *ptr = 0) : 
		m_ref(new InternalImpl(ptr)) 
	{}

	jsRefCountPtr(InternalImpl *ptr) : m_ref(ptr) {
		m_ref->incCnt();
	}

	jsRefCountPtr(const jsRefCountPtr &s) : m_ref(s.m_ref) {
		m_ref->incCnt();
	}

	virtual ~jsRefCountPtr() {
		m_ref->decCnt();
	}

	jsRefCountPtr& operator=(T* pT) = delete;

	jsRefCountPtr& operator=(const jsRefCountPtr &rhs) {
		if (this != &rhs) {
			m_ref->decCnt();
			m_ref = rhs.m_ref;
			m_ref->incCnt();
		}
		return *this;
	}

	T* operator->() {
		return m_ref->ptr();
	}

	const T* operator->() const {
		return m_ref->ptr();
	}

	T &operator*() {
		return *(m_ref->ptr());
	}

	const T &operator*() const {
		return *(m_ref->ptr());
	}

	operator bool() const {
		return m_ref->ptr() != 0;
	}

	int count() const {
		return m_ref->refCnt();
	}

	bool operator==(const jsRefCountPtr &p) const {
		return m_ref->ptr() == p.m_ref->ptr();
	}

	bool operator!=(const jsRefCountPtr &p) const {
		return !operator==(p);
	}

	bool operator==(const T *p) const {
		return m_ref->ptr() == p;
	}

	bool operator!=(const T *p) const {
		return !operator==(p);
	}

private:

	InternalImpl *m_ref;
};

#endif
