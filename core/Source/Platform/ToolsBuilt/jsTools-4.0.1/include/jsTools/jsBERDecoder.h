#ifndef __JSBERDECODER_H__
#define __JSBERDECODER_H__
/******************************************************************************
 Name: jsBEREncoder.cpp

 Description: definition of the jsBERDecoder class and its exception

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 2/23/99		jmw created

 Notes: 

******************************************************************************/

#ifndef _js_h_
	#error Include js.h before jsBERDecoder.h
#endif

typedef ULONG			jsBERTag;

const BYTE HIGH_TAG_TYPE = 0x1f;


///---------------------------------------------------
// tag used for default value when getting data out
// that indicates that we don't care what the tag is
const jsBERTag DONT_CARE_TAG						= ULONG_MAX;

/******************************************************************************
CLASS     
    jsBERDecoder

    Class for decoding BER data from a buffer

DESCRIPTION
    This class is for extracting BER data from a buffer.  It is constructed
	with a const pointer to a buffer.  No copy is made.  

******************************************************************************/
class JSEXPORT jsBERDecoder 
{
public:
	///---------------------------------------------------
	// enum for help in shifting items.
	//
    enum nextCode 
	{ 
        nextEnum,   // next shifted integer is an enum
        nextBOOL,   // next shifted integer is a bool
        nextAny     // next shifted item not any integer
	};

	//===================================================================
	// GROUP: Constructor and initiailizers
	//===================================================================

	///---------------------------------------------------
	// Constructor from buffer.
	// 
	// [in] buffer the buffer where to start decoding
	// [in] size size if BYTEs of buffer
	//
	// [throws] jsBERDecodeException
	//
	jsBERDecoder( PCBYTE buffer, ULONG size ) { setPtr( size, buffer ); }

	///---------------------------------------------------
	// Copy constructor.
	//
	// [in] src jsBERDecoder to copy.  The new object is a rewound clone.
	// 
	// [throws] jsBERDecodeException
	//
	// Note that if the src is not in read mode, neither will this one!
	jsBERDecoder( const jsBERDecoder &src ) { operator=(src); }

	///---------------------------------------------------
	// Copy operator.
	//
	// [in] src jsBERDecoder to copy.  Sets this object's pointers to same values
	// as the others, including the current pointer.
	//
	// [throws] jsBERDecodeException
	// 
	// Note that if the src is not in read mode, neither will this one!
	//
	// [Returns] 
	//		reference to this
	jsBERDecoder &operator=( const jsBERDecoder &src );

	///---------------------------------------------------
	// Set the internal pointer to a new buffer
	//
	// [in] size size if BYTEs of buffer
	// [in] buffer the buffer where to start decoding
	//
	// [throws] jsBERDecodeException
	//
	void setPtr( ULONG size, PCBYTE buffer );

	//===================================================================
	// GROUP: Extraction functions
	//===================================================================

	///---------------------------------------------------
	// Extract a bool value
	//
	// [out] val destination for value from buffer
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		number of bytes the pointer moved
	ULONG decodeBoolean ( bool &val, jsBERTag expectedTag = DONT_CARE_TAG );

	///---------------------------------------------------
	// Decoder a tag
	//
	// [out] val destination for tag from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		At least 1 for number or bytes extracted
	inline ULONG decodeTag( jsBERTag &val );

	///---------------------------------------------------
	// See what the tag is at the current pointer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		the tag
	inline jsBERTag peekTag() const;

	///---------------------------------------------------
	// Extract an enumerated value
	//
	// [out] val destination for value from buffer
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		number of bytes the pointer moved
	ULONG	decodeEnumerated ( int &val, jsBERTag expectedTag = DONT_CARE_TAG );

	///---------------------------------------------------
	// Extract a four-byte long value
	//
	// [out] val destination for value from buffer
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		Number of bytes decoded
	ULONG decodeInteger( LONG &val, jsBERTag expectedTag = DONT_CARE_TAG );

	///---------------------------------------------------
	// Extract an eight-byte long value
	//
	// [out] val destination for value from buffer
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		Number of bytes decoded
	ULONG decodeLongInteger( LONGLONG &val, jsBERTag expectedTag = DONT_CARE_TAG );

	///---------------------------------------------------
	// Extract a double real value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns] 
	//		bytes read
	ULONG decodeReal( double &val, jsBERTag expectedTag = DONT_CARE_TAG );

	///---------------------------------------------------
	// Extract a length only.  This moves the pointer!
	//
	// [out] length referenc to ULONG for length
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		number of bytes taken up by the length
	ULONG decodeLength( ULONG &length ); 

	///---------------------------------------------------
	// Extract an character string.  Since usually extracting a null
	// terminated string, this function will always null terminate the
	// array
	//
	// [out] val destination for value from buffer
	// [in] maxLen max length for this string, including the null terminator
	// [out] len length of this extracted string, <b>not</b> including the null terminator
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	ULONG decodeOctetString( PBYTE s, ULONG maxLen, ULONG &length, jsBERTag expectedTag = DONT_CARE_TAG );

#ifdef JS_OCTETSTRING
	///---------------------------------------------------
	// Extract a string. This moves the pointer!
	//
	// [out] s destination string 
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	ULONG decodeOctetString( jsOctetString &s, jsBERTag expectedTag = DONT_CARE_TAG ); 
#endif

	///---------------------------------------------------
	// Extract an octet string
	//
	// [out] val destination for value from buffer
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	ULONG decodeOctetString( std::string &s, jsBERTag expectedTag = DONT_CARE_TAG);

	///---------------------------------------------------
	// Extract a ASCII string
	//
	// [out] val destination for value from buffer
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	inline ULONG decodeString( std::string &s, jsBERTag expectedTag = DONT_CARE_TAG) { return decodeOctetString( s, expectedTag ); }

	///---------------------------------------------------
	// Extract a two-byte short value
	//
	// [out] val destination for value from buffer
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		Number of bytes decoded
	ULONG decodeShortInteger( SHORT &val, jsBERTag expectedTag = DONT_CARE_TAG );


	///---------------------------------------------------------
	// decode a constructed tag and the following length.
	// 
	// [out] tag the tag found.  Will be expectedTag, unless DONT_CARE_TAG
	// passed in (the default)
	// [out] len length of the consturcted item
	// [in] expectedTag optional expected tag 
	// [in] movePointer if true moves the pointer (default) if false this is
	// a peek-type operation
	//
	// [throws] jsBERDecodeException if wrong tag found, or other error
	//
	// [Returns]
	//    number of bytes decoded to get tag and length
	ULONG decodeConstructedTagAndLength( OUT jsBERTag &tag, OUT ULONG &len, IN jsBERTag expectedTag = DONT_CARE_TAG, bool movePointer = true );


	///---------------------------------------------------------
	// decode a null item
	// 
	// [in] expectedTag tag you expect this value to have.  It 
	//		will throw an exception if it doesn't match what you
	//		pass in, unless you pass in DONT_CARE_TAG (the default)
	//		in which case it does not check
	//
	// [Returns]
	//    number of bytes decoded to get tag and length
	ULONG decodeNull( jsBERTag expectedTag = DONT_CARE_TAG );

	//===================================================================
	// GROUP: Extraction operators
	//===================================================================

	///---------------------------------------------------
	// Extract an int type.  This <i>must</i> abe preceded with the 
	// nextBOOL or nextEnum nextCode enum or it will throw an exception.
	// This reset the nextCode value so if you're doing multiple
	// bools or enums, you must shift a nextCode for each one
	//
	// [out] val an enum or bool destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
    jsBERDecoder& operator>>( int &i);

#ifdef JS_OCTETSTRING
	///---------------------------------------------------
	// Extract a jsOctetString.
	//
	// [out] s destination string 
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		len of string + length of length
	inline jsBERDecoder& operator>>(jsOctetString &s) { decodeOctetString( s ); return *this; }
#endif

	///---------------------------------------------------
	// Extract a string
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBERDecoder& operator>>(std::string &s) { decodeString( s ); return *this; }


	///---------------------------------------------------
	// Extract a four-byte long value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBERDecoder& operator>>( LONG &val ) { decodeInteger( (LONG&)val ); return *this; }

	///---------------------------------------------------
	// Extract a double real value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns] 
	//		bytes read
	inline jsBERDecoder& operator>>( DOUBLE &val ) { decodeReal( val ); return *this; }


	///---------------------------------------------------
	// Set the next extraction type.  Pass in nextBOOL, or nextEnum
	// to be able to make the next >>(int) unambiguous
	//
	// [in] nd the value of the next Integer to decode
	//
	// [Returns]
	//		reference to this for chaining operators
    jsBERDecoder& operator>>( const nextCode &nd );

	///---------------------------------------------------
	// Extract a two-byte short value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBERDecoder& operator>>( SHORT  &val ) { decodeShortInteger( (SHORT&)val ); return *this; }

	///---------------------------------------------------
	// Extract a four-byte unsigned long value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBERDecoder& operator>>( ULONG &val ) { decodeInteger( (LONG&)val ); return *this; }

	///---------------------------------------------------
	// Extract an eight-byte unsigned long value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBERDecoder& operator>>( ULONGLONG &val ) { decodeLongInteger( (LONGLONG&)val ); return *this; }

	///---------------------------------------------------
	// Extract an eight-byte unsigned long value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBERDecoder& operator>>( LONGLONG &val ) { decodeLongInteger( val ); return *this; }

	///---------------------------------------------------
	// Extract a two-byte unsigned short value
	//
	// [out] val destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBERDecoder& operator>>( USHORT  &val ) { decodeShortInteger( (SHORT&)val ); return *this; }

	///---------------------------------------------------
	// Extract a bool type.  This <i>must</i> be preceded with the 
	// nextBOOL Action enum or it will throw an exception
	//
	// [out] val a bool destination for value from buffer
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	jsBERDecoder& operator>>( bool &b ) { decodeBoolean( b ); return *this; }

	//===================================================================
	// GROUP: Accessor functions
	//===================================================================

	///---------------------------------------------------
	// [Returns]
	//		true if this buffer has been set to read mode, 
	//		false if in write mode and shouldn't be decoding
	bool canRead() const { return m_getOffset != WRITE_MODE; }

	///---------------------------------------------------
	// Get a pointer into the base buffer.
	//
	// [Returns]
	//		pointer in buffer.
	PCBYTE buffer() const { return m_buffer; }

	///---------------------------------------------------
	// Get a pointer into the base buffer.
	//
	// [Returns]
	//		the current pointer for getting data out
	PCBYTE	curPtr() const { jsAssert( canRead() ); return m_buffer + m_getOffset; }

	///---------------------------------------------------
	// Get the current get offset
	//
	// [Returns]
	//		the offset for the next extraction
	ULONG curOffset() const { return m_getOffset; }

	///---------------------------------------------------
	// Get a pointer into the current position in the buffer.
	//
	// [in] offset bytes from current position to jump ahead. Without moving current
	//			position
	//
	// [Returns]
	//		pointer in buffer, null if past end
	PCBYTE peekAhead( ULONG offset ) const { jsVerifyReturn( canRead() && offset < sizeLeft(), 0 ); return curPtr() + offset; }

	///---------------------------------------------------
	// Get the size of the buffer
	//
	// [Returns]
	//		total length in bytes of this buffer
	ULONG size() const { return m_size; }

	///---------------------------------------------------
	// Get the size left in the buffer.  
	//
	// [Returns]
	//		length in bytes left in this buffer
	ULONG sizeLeft() const { return m_size - m_getOffset; }

	//===================================================================
	// GROUP: Pointer positioning functions
	//===================================================================

	///---------------------------------------------------
	// Rewind the current pointer to start of buffer.
	//
	void rewind() { m_getOffset = 0; m_nextCode = nextAny; }

	///---------------------------------------------------
	// move current pointer past the tagged item we just got out.  Use 
	// this function if you come to an item you don't recognize and 
	// want to skip over it.
	//
	// [in] tag tag that just extracted which we want to skip.
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		the number of bytes it moved
	ULONG	skipLastTaggedItem( jsBERTag tag );

	///---------------------------------------------------
	// move current pointer past this tagged item.
	//
	// [throws] jsBERDecodeException
	//
	// [Returns]
	//		the number of bytes it moved
	ULONG	skipTaggedItem();

	~jsBERDecoder() {}	

protected:
	///---------------------------------------------------
	// Moves the get pointer an arbitrary amount.  We assume
	// you know what you're doing when you call this
	//
	// [in] len number of bytes to move the pointer
	//
	// [Returns]
	//		true if pointer was moved, false if would be passed end
	bool advancePointer( ULONG len ) { if ( len + m_getOffset > m_size ) return false; m_getOffset+=len; return true; }

	///---------------------------------------------------
	// Sets this coder to read or write mode.  In write mode, no extraction is
	// allowed.  When setting to read mode, a rewind is performed.
	//
	// [in] on true to set to Write mode, false for Read mode
	//
	// [Returns]
	//		previous mode
	inline bool setWriteMode( bool on ); 

private:
	ULONG decodeTagNoMove( jsBERTag &tag ) const;

	// helpers to check validity of request and throw exceptions
	// is errors
	inline void			throwIfCantRead( const char* str );
	inline void			throwIfLessThan( const char* str, ULONG len );
	inline ULONG		decodeAndCheckTag( const char* str, jsBERTag expectedTag );

	static const ULONG WRITE_MODE;	// = ULONG_MAX

	ULONG			m_size;			// total size of buffer

	PCBYTE			m_buffer;		// const ptr to original buffer

	nextCode		m_nextCode;	// what the next decode value should be
									// set to nextAny most of the time

	ULONG			m_getOffset;	// current offset to get next value
										// set to WRITE_MODE if can't get
	ULONG			m_lastTagOffset;// offset of last tag extracted with decodeTag
};

/***********************************************************
CLASS
	jsBERException
	
	base class for exception

DESCRIPTION

***********************************************************/
class JSEXPORT jsBERException
{
public:
	///---------------------------------------------------
	// construct the exception
	//
	// [in] text text about the exception
	jsBERException( const char* text) : m_text( text ) {}

	///---------------------------------------------------
	// construct the exception
	//
	// [in] text text string id about the exception
//	jsBERException( UINT text) { jsVerify( m_text.loadString( text ) ); }

    ///---------------------------------------------------
    // Text accessor function
    // 
    // [Returns] 
	// the text object inside this object
	const std::string &text() const { return m_text; }

private:
	std::string	m_text;
};

/******************************************************************************
CLASS     
    jsBERDecodeException

    Class used for throwing decoding exceptions

DESCRIPTION
    Catch this class when using functions in the jsBERDecoder class.  The reason
	holds the numeric reason why the exeption was thrown and the text will contain
	some human-readable reason
    
******************************************************************************/
class JSEXPORT jsBERDecodeException : public jsBERException
{
public:
    ///---------------------------------------------------
    // Reasons why this exception can be thrown
    enum Reason { 
        nullPointer,        // Null pointer passed in
        badParam,           // Some other parm was bad
        pointerPastEnd,     // the current pointer has moved past the end 
                            // of the buffer.  This object is now corrupted.
        incorrectTag,       // when decoding a type, an incorrect tag was encountered
        tagNotFound,        // the tag passed in was not found
        unexpectedData,     // unexpected data was decoded
        incorrectOperation, // tried to shift out enum without shifing valid nextCode
        bufferTooSmall,     // buffer passed in too, small, current pointer not changed
                            // the length needed is in the text in ASCII
        writeMode,          // attempt to extract when not in read mode
        badLength,          // encode length is bad (usu too small to extracting expected type)
                            // i.e. get short, but size is > 2
		tagTooLong			// an encode tag is more than four bytes.
    };

	///---------------------------------------------------
	// construct the exception
	//
	// [in] text text about the exception
	// [in] why reason why the exception was thrown
	jsBERDecodeException( const char* text, Reason why ) : jsBERException( text ), m_why( why ) {}

	///---------------------------------------------------
	// construct the exception
	//
	// [in] text text string id about the exception
	// [in] why reason why the exception was thrown
//	jsBERDecodeException( UINT text, Reason why ) : jsBERException( text ), m_why( why ) {}

	///---------------------------------------------------
	// get the reason
	//
	// [Returns] 
	//		reason why it was thrown
	Reason why() const { return m_why; }

private:
	Reason		m_why;
};

// macros to help build tags 

///
// create a tag in a 4-byte number
//
// [in] tag the tag number 0-0x1fffff
// 
// [Returns]
//	 jsBERTag
//
#define MAKE_TAG( tag)\
		( (tag < 31) ?  (TAG_1BYTE (tag)):\
		  ((tag < 128) ?  (TAG_2BYTES (tag)):\
		  ((tag < 16384) ?  (TAG_3BYTES (tag)):\
		  (TAG_4BYTES (tag)))))

// supporting macros for BER_TAG
#define TAG_1BYTE( tag ) ((ULONG)tag << 24)

#define TAG_2BYTES( tag ) ((HIGH_TAG_TYPE << 24) | \
						  (tag << 16))

#define TAG_3BYTES( tag ) ((HIGH_TAG_TYPE << 24) | ((tag & 0x3f80) << 9) |\
						   ( 0x0080 << 16 ) | ((tag & 0x007F) << 8))

#define TAG_4BYTES( tag ) ((HIGH_TAG_TYPE << 24)    | ((tag & 0x1fc000) << 2) | \
						 ( 0x0080 << 16) | ((tag & 0x3f80)   << 1) |\
						 ( 0x0080 << 8)  | ((tag & 0x007F)))

///---------------------------------------------------
// BER application tag
const jsBERTag APPLICATION_TAG = 0x40000000;

///---------------------------------------------------
// BER application tag
const jsBERTag PRIVATE_TAG = 0xC0000000;

///---------------------------------------------------
// BER constructed tag
const jsBERTag CONSTRUCTED_TAG = 0x20000000;

///---------------------------------------------------
// BER context tag
const jsBERTag CONTEXT_TAG = 0x80000000;

///---------------------------------------------------
// tag used for encoding a bool 
const jsBERTag BOOLEAN_TAG						= MAKE_TAG( 0x01 );

///---------------------------------------------------
// tag used for encoding a 32 bit integer
const jsBERTag INTEGER_TAG						= MAKE_TAG( 0x02 );

///---------------------------------------------------
// tag used for encoding a bit string
const jsBERTag BIT_STRING_TAG	   				= MAKE_TAG( 0x03 );

///---------------------------------------------------
// tag used for encoding an octet string
const jsBERTag OCTET_STRING_TAG	   				= MAKE_TAG( 0x04 );

///---------------------------------------------------
// tag used for encoding an octet string
const jsBERTag NULL_TAG	   						= MAKE_TAG( 0x05 );

///---------------------------------------------------
// tag used for encoding a 32 bit integer
const jsBERTag REAL_TAG							= MAKE_TAG( 0x09 );

///---------------------------------------------------
// tag used for encoding an enumerated integer
const jsBERTag ENUMERATED_TAG	   				= MAKE_TAG( 0x0a );

///---------------------------------------------------
// tag used for encoding a SEQUENCE, or SEQUENCE OF 
// may be or'd with constructed, etc
const jsBERTag SEQUENCE_TAG						= MAKE_TAG( 0x10 ) | CONSTRUCTED_TAG;

///---------------------------------------------------
// tag used for encoding a SET, or SET OF 
// may be or'd with constructed, etc
const jsBERTag SET_TAG							= MAKE_TAG( 0x11 ) | CONSTRUCTED_TAG;

///---------------------------------------------------
// tag used for encoding an ASCII string
const jsBERTag IA5_STRING_TAG	   				= MAKE_TAG( 0x16 );

///---------------------------------------------------
// tag used to indicate the length is indefinite 
// (this is not used in LDAP)
const BYTE INDEFINITE_LENGTH				= 0x80;

///---------------------------------------------------
// for long form of length, indicates one byte follows
const BYTE ONE_BYTE_LENGTH					= 0x81;

///---------------------------------------------------
// for long form of length, indicates two bytes follows
const BYTE TWO_BYTE_LENGTH					= 0x82;

///---------------------------------------------------
// for long form of length, indicates three bytes follows
const BYTE THREE_BYTE_LENGTH				= 0x83;

///---------------------------------------------------
// for long form of length, indicates four bytes follows
const BYTE FOUR_BYTE_LENGTH					= 0x84;

///---------------------------------------------------
// mask used for getting number of bytes of the length in long for 
// of length
const BYTE LENGTH_MASK						= 0x7f;

///---------------------------------------------------
// used for or'ing with a length
const BYTE LONG_FORM_MASK					= 0x80;

///---------------------------------------------------
// two of these are used as an indicator for end of indefinite
// length	
const BYTE END_OF_CONTENTS					= 0x00;

///---------------------------------------------------
// constructed context tags
const jsBERTag CONSTRUCTED_TAG_ZERO				= MAKE_TAG( 0 ) | CONSTRUCTED_TAG | CONTEXT_TAG;
const jsBERTag CONSTRUCTED_TAG_ONE				= MAKE_TAG( 1 ) | CONSTRUCTED_TAG | CONTEXT_TAG;
const jsBERTag CONSTRUCTED_TAG_TWO				= MAKE_TAG( 2 ) | CONSTRUCTED_TAG | CONTEXT_TAG;
const jsBERTag CONSTRUCTED_TAG_THREE			= MAKE_TAG( 3 ) | CONSTRUCTED_TAG | CONTEXT_TAG;
const jsBERTag CONSTRUCTED_TAG_FOUR				= MAKE_TAG( 4 ) | CONSTRUCTED_TAG | CONTEXT_TAG;

//---------------------------------------------------
// Real tag for + infinity, used internally 
const BYTE REAL_PLUS_INFINITY = 0x40;

//---------------------------------------------------
// Real tag for - infinity, used internally 
const BYTE REAL_MINUS_INFINITY = 0x41;

//---------------------------------------------------
// base of encoded real.  Currently we only support binary
// used internally 
const BYTE REAL_BINARY = 0x80;

//---------------------------------------------------
// sign bit for real tag used internally 
const BYTE REAL_SIGN = 0x40;

//---------------------------------------------------
// used internally 
const BYTE REAL_EXPLEN_1 = 0x00;

//---------------------------------------------------
// used internally 
const BYTE REAL_EXPLEN_2 = 0x01;

// used internally 
const BYTE REAL_EXPLEN_3 = 0x02;

///---------------------------------------------------
// return the class bits
inline BYTE tagClass( jsBERTag tag ) 
	{ return (BYTE)((tag & PRIVATE_TAG) >> 24); } // private has all bits set -- use as mask

///---------------------------------------------------
// return true if the tag is constructed 
inline bool tagIsConstructed( jsBERTag tag ) { return (tag & CONSTRUCTED_TAG) != 0; }

///---------------------------------------------------
// set to write mode
inline bool jsBERDecoder::setWriteMode( bool on )
{ 
	bool bRet = !canRead(); 

	if ( on )
		m_getOffset = WRITE_MODE;
	else
		m_getOffset = 0;

	return bRet;
}

///---------------------------------------------------
// private fn to throw an exception if not in read mode
inline void jsBERDecoder::throwIfCantRead( const char* str )
{
	if ( !canRead() )
	{
		jsAssert( false );
		throw new jsBERDecodeException( str, jsBERDecodeException::writeMode );
	}
}

///---------------------------------------------------
// Decoder a tag
//
// [out] val destination for tag from buffer
//
// [throws] jsBERDecodeException
//
// [Returns]
//		At least 1 for number or bytes extracted
inline ULONG jsBERDecoder::decodeTag( jsBERTag &val )
{
	ULONG ret = decodeTagNoMove( val );
	m_lastTagOffset = m_getOffset;
	m_getOffset += ret;
	return ret;
}

///---------------------------------------------------
// See what the tag is at the current pointer
//
// [throws] jsBERDecodeException
//
// [Returns]
//		the tag
inline jsBERTag jsBERDecoder::peekTag() const
{
	jsBERTag val;
	decodeTagNoMove( val );
	return val;
}

///---------------------------------------------------
// private fn to throw an exception if not enough bytes are available 
// to read
// ---- ? Should this be debug only ?
inline void jsBERDecoder::throwIfLessThan( const char* str, ULONG len )
{
	if ( curOffset() + len > size() )
	{
		jsAssert( false );
		throw new jsBERDecodeException( str, jsBERDecodeException::pointerPastEnd );
	}
}

///---------------------------------------------------
// private fn to throw an exception if not at the expectedTag
inline ULONG jsBERDecoder::decodeAndCheckTag( const char* str, jsBERTag expectedTag )
{
	jsBERTag val;
	ULONG ret = decodeTag( val );

	if ( expectedTag != DONT_CARE_TAG &&
		 val   != expectedTag )
	{
		jsAssert( false );
		throw new jsBERDecodeException( str, jsBERDecodeException::incorrectTag );
	}

	return ret;
}


inline ULONG jsBERDecoder::decodeNull( jsBERTag expectedTag /*= DONT_CARE_TAG*/ )
{

	ULONG bytesDecoded = decodeAndCheckTag( "decodeNull", expectedTag );

	ULONG len = 0;
	bytesDecoded += decodeLength( len );

	// length should always be zero
	if ( len != 0 )
	{
		jsAssert( false );
		throw new jsBERDecodeException( "decodeNull", jsBERDecodeException::unexpectedData );
	}

	return bytesDecoded;
}

#endif //__JSBERDECODER_H__
