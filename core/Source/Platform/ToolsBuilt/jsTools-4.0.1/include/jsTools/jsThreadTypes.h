/******************************************************************************
 Name: jsThreadTypes.h

 Description: types for use by jsThread and other classes

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 3/17/99	jmw created

 Notes: 

******************************************************************************/
#ifndef __JSTHREADTYPES_H__
#define __JSTHREADTYPES_H__

#ifndef _js_h_
	#error Include js.h before jsThreadTypes.h
#endif

///
// priority enum for threads and processes.
enum jsThdPriority 
{ 
    PR_LOWEST,  
    PR_LOW, 
    PR_NORMAL, 
    PR_HIGH, 
    PR_HIGHEST 
};

///
// NORMAL schedules at the OS level, COOPERATIVE is a fiber in NT
// or ? for Solaris and Unixware
enum jsThreadType  { TT_NORMAL, TT_COOPERATIVE };

typedef ULONG jsThreadID;

#endif //__JSTHREADTYPES_H__
