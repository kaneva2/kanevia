/******************************************************************************
 Name: jsSemaphore.h

 Description: definition of the portable counting semaphore class

 Copyright (C) 1996 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsSemaphore_h_
#define _jsSemaphore_h_

#ifndef _js_h_
	#error Include js.h before jsSemaphore.h
#endif

// forward decl of Implementation classes
class jsSemaphoreImpl;

/***********************************************************
CLASS
	jsSemaphore 
	
	counting semaphore

DESCRIPTION

***********************************************************/
class JSEXPORT jsSemaphore : public jsBase
{
public:

	///---------------------------------------------------------
	// constructor
	// 
	// [in]  initCount initial count
	//
	jsSemaphore( ULONG initCount = 0, ULONG maxCount = LONG_MAX );


	///---------------------------------------------------------
	// increment the counter.  This releases waiters
	// 
	// [in] count number to increment
	//
	// [Returns]
	//    try if ok
	bool increment(UINT count = 1);

	///---------------------------------------------------------
	// wait for the item and decrement the count.  Note that
	// UNIX does support timeout, so this method uses a bit of
	// polling under the covers for UNIX
	// 
	// [in] timeoutMS number of milleseconds to wait before timing out
	// [in] unixPollMS for implementations that don't support timeout
	// on a semaphore, how often should this poll, in milliseconds.
	// 
	// [Returns]
	//    WR_OK if got item<br>
	//	  WR_ERROR if error,
	//	  WR_TIMEROUT if a timeout
	jsWaitRet waitAndDecrement( ULONG timeoutMS = INFINITE_WAIT, ULONG unixPollMS = 100 );


	///---------------------------------------------------------
	// try to decrement the counter.  Returns immediately
	// 
	//
	// [Returns]
	//    WR_OK if got item<br>
	//	  WR_ERROR if error<br>
	//	  WR_TIMEOUT if got it<br>
	jsWaitRet tryWaitAndDecrement();

	~jsSemaphore();

private:
	jsSemaphoreImpl	*m_impl;
};

#endif
 
 
 
 
 
