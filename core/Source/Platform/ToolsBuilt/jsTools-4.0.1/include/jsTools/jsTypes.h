/******************************************************************************
 Name: jsTypes.h

 Description: header of common types, used by js.h and in CORBA idl files

 Copyright (C) 1996 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsTypes_h_
#define _jsTypes_h_

#ifndef _js_h_
	#error Include js.h before jsTypes.h
#endif

///
// 8 bit unsigned number
typedef unsigned char octet;

///
// 8 bit unsigned number
typedef octet			BYTE;   

///
// 16 bit signed number
typedef short			SHORT;

///
// 16 bit unsigned number
typedef unsigned short	USHORT;

///
// 32 bit signed number
typedef long			LONG;

///
// 32 bit unsigned number
typedef unsigned long	ULONG;

///
// 64 bit signed number
#ifdef _NT
	typedef __int64		LONGLONG;
	#define _LL(x)		x##i64
#elif defined( _UNIX )
	typedef long long	LONGLONG;
	#define _LL(x)		x##LL
#elif defined( _MAC )
	typedef long long	LONGLONG;
	#define _LL(x)		x##LL
#else
#	error "_MAC, _UNIX or _NT must be defined"
#endif

///
// 64 bit unsigned number
#ifdef _NT
	typedef unsigned __int64	ULONGLONG;
	#define _ULL(x)		x##Ui64
#elif defined( _UNIX )
	typedef unsigned long long	ULONGLONG;
	#define _ULL(x)		x##ULL
#elif defined( _MAC )
	typedef unsigned long long	ULONGLONG;
	#define _ULL(x)		x##ULL
#else
#	error "_MAC, _UNIX or _NT must be defined"
#endif

#endif
 
 
 
 
 
