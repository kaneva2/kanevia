/******************************************************************************
 Name: jsTime.h

 Description: wrapper for the C time_t struct

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/26/97	jmw created

 Notes: 

******************************************************************************/
#ifndef _jsTime_h_
#define _jsTime_h_

#ifndef _js_h_
	#error Include js.h before jsTime.h
#endif

#include <time.h>

/******************************************************************************
CLASS
	jsTime

	time class

DESCRIPTION
	This class wraps the C time_t construct

******************************************************************************/
class JSEXPORT jsTime	
{
public:
	///----------------------------------------------------
	// constructor, set to current time
	inline jsTime();

	///----------------------------------------------------
	// copy constructor
	jsTime( const jsTime &t ) { operator=( t ); }

	///----------------------------------------------------
	// construct from a time_t
	jsTime( time_t t )	{ operator=( t ); }

	///----------------------------------------------------
	// construct from components of time
	jsTime( UINT year, UINT mon, UINT day, UINT hr = 0, UINT min = 0, UINT sec = 0 );

	///----------------------------------------------------
	// assignment operator
	//
	// [in] t jsTime to set this one to
	//
	// [Returns]
	//	reference to this
	inline jsTime & operator=( const jsTime &t ) { m_time = t.m_time; return *this; }

	///----------------------------------------------------
	// assignment operator
	//
	// [in] t time_t to set this one to
	//
	// [Returns]
	//	reference to this
	inline jsTime & operator=( time_t t ) { m_time = t; return *this; }

	///----------------------------------------------------
	// static function to get current time
	//
	// [Returns]
	//	a jsTime with current time set
	inline static jsTime now() { return jsTime(); }

	///----------------------------------------------------
	// [Returns]
	//	the internal time_t value
	inline operator time_t() const { return m_time; }
#if 0
	///----------------------------------------------------
	// format this time into a string
	//
	// [in] formatStr see strftime C function for help on this
	// [out] result string to put the result into
	// [in] maxAlloc length of string to allocate to hold the result
	//
	// [Returns]
	//	true if formatted ok and result has something
	inline bool format( const char* formatStr, std::string &result, UINT maxAlloc = 255 ) const;

	///----------------------------------------------------
	// format this time into a string
	//
	// [in] formatStr see strftime C function for help on this
	// [out] result pointer to put the result into
	// [in] strAlloced number of characters result can hold
	//
	// [Returns]
	//	true if formatted ok and result has something
	static inline bool format( const jsTime &t, const char* formatStr, char* result, UINT strAlloced );
#endif
private:
	time_t		m_time;
};

//========== inlines
inline jsTime::jsTime()
{
	time( &m_time );
}


inline jsTime::jsTime( UINT year, UINT mon, UINT day, UINT hr, UINT min, UINT sec )
{
	jsVerifyReturnVoid( mon > 0 && mon < 13 && day > 0 
		&& day < 32 && hr < 25 
		&& min < 61 && sec < 61 );

	struct tm timeStruct;

	timeStruct.tm_year = year - 1900;     
	timeStruct.tm_mon = mon - 1;        
	timeStruct.tm_mday = day;

	timeStruct.tm_hour = hr;
	timeStruct.tm_min = min;
	timeStruct.tm_sec = sec;

	timeStruct.tm_isdst = -1;

	m_time = ::mktime(&timeStruct);

	jsAssert(m_time != -1);       
}
#if 0
inline bool jsTime::format( const jsTime &t, const char* formatStr, char* result, UINT maxAlloc )
{
	struct tm myTime;
	::localtime_s( &myTime, &t.m_time );
	struct tm *timeptr = &myTime;

	bool ret = false;

	if ( timeptr && _tcsftime( result, maxAlloc, formatStr, timeptr ) > 0 )
	{
		ret =  true;
	}
	else
	{
		result[0] = '\0';
	}

	return ret;
}

inline bool jsTime::format( const char* formatStr, std::string &result, UINT maxAlloc ) const
{
	bool ret = false;
	char *p = new char[maxAlloc];

	if ( p )
	{
		ret = jsTime::format( *this, formatStr, p, maxAlloc );
		result = p;
		delete p;
	}

	return ret;
}
#endif
#endif

 
 
 
 
 
