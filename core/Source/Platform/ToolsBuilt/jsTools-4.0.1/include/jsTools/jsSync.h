/******************************************************************************
 Name: jsSync.h

 Description: definition of the portable synchronization 
 classes

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsSync_h_
#define _jsSync_h_

#ifndef _js_h_
	#error Include js.h before jsSync.h
#endif

#include "jsThreadTypes.h" // for threadid
#include "jsEvent.h"

// enums and consts
enum jsSyncType { ST_THREAD, ST_PROCESS };
enum jsLockReason { LR_NOLOCK, LR_READONLY, LR_READWRITE, LR_READPROMOTE };

const UINT FASTSYNC_LEVEL = (UINT)32;

// forward decl of Implementation classes
class jsRWSyncImpl;
class jsFastSyncImpl;
class jsSyncImpl;
class jsRWSync;

// !!!!**** FOR NOW ****!!! this is for threads only, need to find out
// more about inter-process events for POSIX

class jsFastSync;


/******************************************************************************

 Description: base class, holds level and thread id

******************************************************************************/
class JSEXPORT jsSyncBase : public jsBase
{
public:
	BYTE		level() const { return m_level; }
	jsThreadID	threadID() const { return m_TID; }

protected:
	jsSyncBase( BYTE level, const char* name, bool useLockMgr = true );

	BYTE		m_level;	// level for deadlock detection
	jsThreadID	m_TID;		// last thread to get lock
};


/***********************************************************
CLASS
	jsSync 
	
	sync object.  

DESCRIPTION
	Can be called recursively from the thread
	that holds the lock.

***********************************************************/
class JSEXPORT jsSync : public jsSyncBase
{
public:

	///---------------------------------------------------------
	// constructor
	// 
	// [in] level the level of the lock for the lock manager's use
	// [in] name name for debugging
	// [in] lockNow if true then locks during the constructor
	//
	jsSync( BYTE level, const char* name, bool lockNow = false );


	///---------------------------------------------------------
	// lock the sync object
	// 
	// [Returns]
	//    WR_OK if locked
	jsWaitRet lock();

	///---------------------------------------------------------
	// try to lock the sync object
	// 
	// [Returns]
	//    WR_OK if locked<br>
	//	  WR_TIMEOUT if couldn't get the lock
	jsWaitRet tryLock();


	///---------------------------------------------------------
	// see if the item is locked
	//
	// [Returns]
	//    WR_OK if locked<br>
	//	  WR_TIMEOUT if not locked
	jsWaitRet peekLock();


	///---------------------------------------------------------
	// unlock the item.  You must hold the lock
	//
	void unlock();

	~jsSync();

protected:
	jsSync( BYTE level, const char* name, bool lockNow, bool UseLockMgr );
	jsSyncImpl	*m_impl;

friend class jsRWSync; // so can construct with other constuctor
};

/******************************************************************************
CLASS
	jsFastSync

	a quick locking mechanism.  On Win32 this is a Critical Section.  It is 
	recursive

DESCRIPTION
	Fast sync and Fast lock are for non-recursive
	very Fast locks that shouldn't get any other locks while locked

******************************************************************************/
class JSEXPORT jsFastSync : public jsSyncBase
{
public:
	///
	// default constructor 
	//
	// [in] lockNow if true, locks the sync during construction
	//
	jsFastSync( bool lockNow = false );

	///
	// lock the sync
	void lock();

	///
	// unlock the sync
	void unlock();

	///
	// destructor will unlock the sync if it is locked
	~jsFastSync();

	///
	// a global Fast sync for use of SyncExchange (see below)
	//
	// [Returns]
	//		<b>the</b> global sync
	static jsFastSync &Global();

	static jsFastSync m_global;

private:
	jsFastSyncImpl	*m_impl;
};

 
/******************************************************************************
CLASS
	jsRWSync 
	
	read-write sync object.  

DESCRIPTION
	Can be called recursively from the thread
 that holds the lock.short desc

******************************************************************************/
class JSEXPORT jsRWSync : public jsSyncBase
{
public:

	///---------------------------------------------------------
	// constructor
	// 
	// [in] level the level of the lock for the lock manager's use
	// [in] name name for debugging
	// [in] lockNow if true then locks during the constructor
	// [in] lr how to lock it
	//
	jsRWSync( BYTE level, const char* name, bool lockNow = false, jsLockReason lr = LR_READONLY );

	///---------------------------------------------------------
	// lock the sync object
	// 
	// [in] lr how to lock it
	//
	// [Returns]
	//    WR_OK if locked
	jsWaitRet lock( jsLockReason lr );

	///---------------------------------------------------------
	// try to lock the sync object
	// 
	// [in] lr how to lock it
	//
	// [Returns]
	//    WR_OK if locked<br>
	//	  WR_TIMEOUT if couldn't get the lock
	jsWaitRet tryLock(jsLockReason lr);


	///---------------------------------------------------------
	// see if the item is locked
	//
	// [in] lr how to lock it
	//
	// [Returns]
	//    WR_OK if locked<br>
	//	  WR_TIMEOUT if not locked
	jsWaitRet peekLock(jsLockReason lr);

	///---------------------------------------------------------
	// unlock the item.  You must hold the lock
	//
	// [in] lr how it was locked.  Must match
	//
	void unlock( jsLockReason lr );

	~jsRWSync();

protected:
	jsEvent		m_readDoneEvent;	// lets write know reader done
	UINT		m_readerCnt;		// number of readers now
	jsSync		m_exclusive,		// locks for exclusive access
				m_reader;			// serialize m_readerCnt
private:
	jsWaitRet _lock( jsLockReason lr, bool bTrying );
};


#endif

 
 
 
 
 
