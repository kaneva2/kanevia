/******************************************************************************
 Name: jsEnumFiles.h

 Description: enumerate files in a directory

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/98		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsEnumFiles_h_
#define _jsEnumFiles_h_

#ifndef _js_h_
	#error Include js.h before jsEnumFiles.h
#endif

#include "jsTime.h"

class jsEnumFilesImpl;

///---------------------------------------------------------
// does a file exists
// 
// [in] fname the file or directory name to check
// [out] isDir true if it is a directory.  If null, not set.
//
// [Returns]
//    true if the file or directory exists
JSEXPORT bool jsFileExists( const char* fname, bool *isDir );

/***********************************************************
CLASS
	jsEnumFiles
	
	class to enumerate files in a directory

DESCRIPTION
	enumerator

***********************************************************/
class JSEXPORT jsEnumFiles : public jsBase
{
public:

	///---------------------------------------------------------
	// read only attribute
	static const ULONG FA_readOnly;		

	///---------------------------------------------------------
	// directory attribute
	static const ULONG FA_directory;

	///---------------------------------------------------------
	// hidden attribute
	static const ULONG FA_hidden;	
	
	///---------------------------------------------------------
	// hidden attribute
	static const ULONG FA_system;	

	///---------------------------------------------------------
	// hidden attribute
	static const ULONG FA_normal;


	///---------------------------------------------------------
	// constructor
	// 
	// [in] fileMask the mask of files to look for
	// [in] includeDirs if true returns matching directories, too
	//
	jsEnumFiles( const wchar_t* fileMask, bool includeDirs = false );

	///---------------------------------------------------------
	// get the next file.  Only if this returns true will
	// the current file name, etc. be valid
	//
	// [Returns]
	//    true if got another file.  
	bool next();

	///---------------------------------------------------------
	// [Returns]
	//    the current file name without its path
	const wchar_t* currentFileName() const;

	///---------------------------------------------------------
	// [Returns]
	//    the current directory name without the file name, will
	// not have trailing slash
	const wchar_t* currentDirName() const;

	///---------------------------------------------------------
	// [Returns]
	//    the current path name
	const wchar_t* currentPath() const;

	///---------------------------------------------------------
	// [Returns]
	//    the current file's create time
	jsTime currentLastWriteTime() const;

	///---------------------------------------------------------
	// [Returns]
	//    the current attributes of the current path
	ULONG currentAttrs() const;

	///---------------------------------------------------------
	// [Returns]
	//    true if directory bit set
	inline 	bool isDirectory() const { return (currentAttrs() & FA_directory) != 0; }

	///---------------------------------------------------------
	// [Returns]
	//    true if hidden bit set
	inline 	bool isHidden() const { return (currentAttrs() & FA_hidden) != 0; }

	///---------------------------------------------------------
	// [Returns]
	//    true if read only bit set
	inline bool isReadOnly() const { return (currentAttrs() & FA_readOnly) != 0; }

	///---------------------------------------------------------
	// [Returns]
	//    true if system bit set
	inline bool isSystem() const { return (currentAttrs() & FA_system) != 0; }

	~jsEnumFiles();

private:
	jsEnumFilesImpl		*m_impl;
};
#endif
