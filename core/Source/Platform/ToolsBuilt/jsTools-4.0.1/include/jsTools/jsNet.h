/*
 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/
#ifndef _jsNet_h_
#define _jsNet_h_

#ifndef bad_addr 
const int bad_addr = -1;
#endif

#ifdef _NT

#ifndef MAXHOSTNAMELEN
	#define MAXHOSTNAMELEN 64                     
#endif

	#define DESIRED_WINSOCK_VERSION 0x0101  
	#define MINIMUM_WINSOCK_VERSION 0x0101 

#ifndef EWOULDBLOCK
	#define EWOULDBLOCK     WSAEWOULDBLOCK
	#define EINPROGRESS     WSAEINPROGRESS
	#define ECONNREFUSED    WSAECONNREFUSED
	#define ETIMEDOUT       WSAETIMEDOUT
	#define ENETUNREACH     WSAENETUNREACH
	#define EHOSTUNREACH    WSAEHOSTUNREACH
	#define EHOSTDOWN       WSAEHOSTDOWN
	#define EISCONN         WSAEISCONN
#endif
#ifndef EINTR	        
	#define EINTR	        WSAEINTR
#endif

	#define socketerrno WSAGetLastError()

	#define SOCKFD SOCKET                   
	#define INVSOC INVALID_SOCKET

#pragma warning (push)
#pragma warning (disable:4201)

	typedef UINT SOCKFD; // avoid including windows
	#include <winsock.h>

#pragma warning (pop)
    

#elif defined(_SOLARIS) || defined(_UNIXWARE) 

	#include <sys/types.h>
	#include <unistd.h>
	#include <limits.h>
	#include <string.h>
	#include <errno.h>          /* independent */
	#include <sys/time.h>       /* independent */
	#include <sys/stat.h>
	#include <sys/param.h>
	#include <sys/file.h>       /* For open() etc */
	#include <sys/filio.h>
	#include <sys/ioctl.h>
	#include <signal.h>
	#include <pwd.h>
	#include <grp.h>
	#include <stdio.h>
	#include <dirent.h>

	#define INCLUDES_DONE


#ifndef TCP_INCLUDES_DONE
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>                 /* Must be after netinet/in.h */
	#include <netdb.h>
	#define TCP_INCLUDES_DONE
#endif

	#ifndef ERRNO_DONE
	extern int errno;
	#define socketerrno errno
	#define ERRNO_DONE
	#define INADDR_NONE -1   
	#define NO_ERROR 0
	#endif

	#ifndef SOCKFD
	#define SOCKFD int              /* Unix like socket descriptor */
	#define INVSOC -1               /* Unix invalid socket */
	#endif


	#if	defined(_SOLARIS) || defined(_UNIXWARE)
	extern "C" 
	{ 
		int gethostname( char* n, int size ); 
	}
	#endif

	#ifdef _UNIXWARE
	extern "C" 
	{ 
		long getdtablesize(void);
	}
	#endif


//used for gethostbyname calls
	#ifndef MAXGETHOSTSTRUCT
	#define MAXGETHOSTSTRUCT        2048
	#endif

	#ifdef _UNIX
	#define RESVPORT(x) ( ::ntohs((x)) < IPPORT_RESERVED && ::ntohs((x)) >= IPPORT_RESERVED/2)
	#endif

	#define PORTDECR(x) ((x) = ::htons(ntohs((x)-1)))


	#ifdef _UNIX
	#define closesocket close
	#define ioctlsocket ioctl

	/*
	inline int closesocket( SOCKFD fd );
	inline int closesocket( SOCKFD fd ) { return ::close( fd ); }

	inline int ioctlsocket( SOCKFD s, long cmd, unsigned long * argp );
	inline int ioctlsocket( SOCKFD s, long cmd, unsigned long  *argp) { return ::ioctl( s, cmd, argp ); }
	*/
	#endif

#elif defined(_MAC) 

	// on the Mac, we try to keep the implementation as hidden as possible
	#define INVSOC -1
	typedef UINT SOCKFD;
	
#endif

#endif
 
 
 
 
 
