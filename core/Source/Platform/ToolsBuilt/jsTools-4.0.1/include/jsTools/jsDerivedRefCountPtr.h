#pragma once
#if DEPRECATED

#include "jsRefCountPtr.h"

template <class DERIVEDCLASS, class PARENTCLASS, class InternalImpl = jsInternalRefCntPtr<PARENTCLASS> > 
class jsDerivedRefCountPtr : public jsRefCountPtr<PARENTCLASS, InternalImpl>
{
public:
	typedef jsRefCountPtr<PARENTCLASS, InternalImpl> parentClassPtr;

	jsDerivedRefCountPtr( parentClassPtr &ptr ) : 
		jsRefCountPtr<PARENTCLASS, InternalImpl>( (PARENTCLASS*)0 )
	{
		if ( ptr && checkPointer( ptr.operator->() ) )
			(*this) = ptr;
	}

	jsDerivedRefCountPtr( const jsDerivedRefCountPtr &ptr ) : 
		jsRefCountPtr<PARENTCLASS, InternalImpl>( ptr )
	{}

	jsDerivedRefCountPtr( DERIVEDCLASS *ptr = NULL) : 
		jsRefCountPtr<PARENTCLASS, InternalImpl>( ptr )
	{}

	jsDerivedRefCountPtr( PARENTCLASS *ptr ) : 
		jsRefCountPtr<PARENTCLASS, InternalImpl>( (PARENTCLASS*)0 )
	{
		if ( ptr && checkPointer( ptr ) )
			operator=( jsRefCountPtr<PARENTCLASS, InternalImpl>( ptr ) );
	}

	jsDerivedRefCountPtr &operator=( const jsDerivedRefCountPtr &src ) 
	{ 
		if ( this != &src ) 
		{
			jsRefCountPtr<PARENTCLASS, InternalImpl>::operator=(src); 
		} 
		return *this; 
	}

	jsDerivedRefCountPtr &operator=( const parentClassPtr &src ) 	
	{ 
		if ( this != &src ) 
		{
			if ( checkPointer( src.operator->() ) )
				jsRefCountPtr<PARENTCLASS, InternalImpl>::operator=(src); 
			else
				(*this) = jsDerivedRefCountPtr((PARENTCLASS*)0);
		} 
		return *this; 
	}

#ifdef _CPPRTTI
	inline DERIVEDCLASS* operator->() { return dynamic_cast<DERIVEDCLASS*>(jsRefCountPtr<PARENTCLASS, InternalImpl>::operator->()); }
#else
	inline DERIVEDCLASS* operator->() { return (DERIVEDCLASS*)(jsRefCountPtr<PARENTCLASS, InternalImpl>::operator->()); }
#endif

#ifdef _CPPRTTI
	inline const DERIVEDCLASS* operator->() const { return dynamic_cast<DERIVEDCLASS*>(jsRefCountPtr<PARENTCLASS, InternalImpl>::operator->()); }
#else
	inline const DERIVEDCLASS* operator->() const { return (DERIVEDCLASS*)(jsRefCountPtr<PARENTCLASS, InternalImpl>::operator->()); }
#endif

	inline DERIVEDCLASS &operator*() { return *operator->(); }

	inline const DERIVEDCLASS &operator*() const { return *operator->(); }

	inline operator bool() const { return jsRefCountPtr<PARENTCLASS, InternalImpl>::operator bool(); }

private:
	bool checkPointer( const PARENTCLASS *ptr )
	{
		bool ret = true;
#		ifdef _CPPRTTI
			if ( ptr )
			{
				const DERIVEDCLASS *p = dynamic_cast<const DERIVEDCLASS*>( ptr );
				ret = p != 0;
			}
#		endif
		return ret;
	}
};
#endif
