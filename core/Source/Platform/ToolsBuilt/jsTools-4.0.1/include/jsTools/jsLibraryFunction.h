/******************************************************************************
 Name: jsLibraryFunction.h

 Description: gets a function from a library

 Copyright (C) 1999, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/99		jmw created

 Notes: 

******************************************************************************/
#ifndef __JSLIBRARYFUNCTION_H__
#define __JSLIBRARYFUNCTION_H__

#ifndef _js_h_
	#error Include js.h before jsLibraryFunction.h
#endif


class jsLibraryFunctionImpl;

#ifdef _NT
	//#define JSLIBFUNCTION FARPROC
	typedef int (*JSLIBFUNCTION)();
#else
	typedef void (*JSLIBFUNCTION)();
#endif


/**********************************************************
CLASS
	jsLibraryFunction
	
	get a function from a lib

DESCRIPTION

***********************************************************/
class JSEXPORT jsLibraryFunction : public jsBase
{
public:
	///---------------------------------------------------------
	// constructor
	// 
	// [in] lib name of lib including extension, without dir
	// [in] freeOnDelete set to false to not free/close the lib on 
	//      delete of the object.  Sometimes this can be useful in debugging
	//		etc.
	//
	jsLibraryFunction( const char* lib, bool freeOnDelete = true);

	///---------------------------------------------------------
	// getFunction
	// 
	// [in] procName name of procedure in lib. always single byte
	//
	// [Returns]
	//    the fn ptr or null if error.  In that case lastError() is valid
	JSLIBFUNCTION getFunction( const char* procName ); 

	~jsLibraryFunction();

	ULONG lastError() const;

	const char* libraryName() const;

private:
	jsLibraryFunctionImpl	*m_impl;
};

#endif //__JSLIBRARYFUNCTION_H__
