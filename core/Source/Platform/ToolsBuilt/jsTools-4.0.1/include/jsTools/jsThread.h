/******************************************************************************
 Name: jsThread.h

 Description: portable thread class definition file
 See the html help for information and example on usage of this class

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96	jmw created

 Notes: 

******************************************************************************/
#ifndef _jsThread_h_
#define _jsThread_h_

#ifndef _js_h_
	#error Include js.h before jsThread.h
#endif

#include "jsThreadTypes.h"

class jsThreadImpl;

/*****************************************************************************
CLASS
    jsThread 

    OS thread object

DESCRIPTION
    This abstract class is an encapsulation of a platform-independant thread class. 
    

    This is a 'least common denominator' between 
    NT threads and POSIX threads. For UNIX all the threads are bound to LWP 
    and detached. What this means is that we implemented our own waitForExit 
    since you may not know at creation if you will always wait for exit, 
    and each thread gets a LWP. This greatly improved concurrency on Unixware, 
    and may be changed if it is too much overhead on other platforms. 

CREATION NOTES
    To create a thread, simply derive from jsThread, add any thread-specific
    data and override the _doWork function. Then instantiate your class
    and call start. 

TERMINATION NOTES
    Call the terminate function to cleanly stop a thread. You pass
    in how long you wish to wait before killing it if the thread is not gracefully
    exiting. Call kill only if you don't need a graceful shutdown. 

    One thing to be careful about is killing or terminating a thread.
    It will *not* free any held resources. Threfore I highly recommend that
    all _doWork functions check the isTerminated value to see
    if it should cleanly exit. If you thread doesn't hold any resources, don't
    worry about this.

SLEEPING NOTES
    jsThread supports a wake-able sleep. sleep is a protected
    function for a thread to stop executing. Simply call wakeUp to make
    the thread resume. Since POSIX doesn't have Suspend and Resume, they are
    not implemented.

WAITING ON THREADS
    You may wait for a thread to exit using the join() method.
    This is implemented as an jsEvent in Unix. This may be timed. 

THREAD-SPECIFIC DATA
    There are times that you want a variable to have a thread-specific instance.
    Basically this gives you a global variable, that has separate storage for
    each thread. (For NT this is called Thread Local Storage.) createThreadData
    creates a new four-byte piece of data that is available to all threads.
    To get or set the variable, call getThreadData, or setThreadData
    using the id returned from createThreadData.

    Why, you ask? This is useful for function that are not members of a
    thread class to get to instance data for a thread. For example, dbtest
    uses a function to read a string from a file. This function is global,
    but each thread will have its own thread handle. The code was such that
    we couldn't easily pass the file handle, so instead, I used createThreadData
    in main and set a global variable to its return code. Each thread opened
    a file, and setThreadData to the handle. Then the function called
    getThreadData to get the correct file handle. 

******************************************************************************/
class JSEXPORT jsThread : public jsBase
{
public:
	enum StopRet
	{
		srStoppedOk,
		srKilled,
		srKillFailed,
		srTimeout
	};

    ///
    // constructor.  Just creates the thread.  Unless you call start() in 
    // your derived class, this does not start() the thread.
    //
    // [in] name name of the thread used in debugging messages
    // [in] prio thread priority
	jsThread( const char* name, jsThdPriority prio = PR_NORMAL,
				jsThreadType ttype = TT_NORMAL );

    ///
    // start the thread running
    //
    // [Returns]
    //  True if the thread is now running
	bool start();

    ///
    // wake the thread up if it is sleeping internally
	void wakeUp();

    ///
    // reset the thread so it may be re start()'ed
    //
    // [Returns]
    //  true if the reset is ok
	bool reset();

    // GROUP: status

    ///
    // [Returns]
    //  true if the thread is sleeping
	bool isSleeping() const;

    ///
    // [Returns]
    //  true if the thread is running
	bool isRunning() const;

    ///
    // [Returns]
    //  true if the thread is created.  
    bool isCreated() const;

    ///
    // [Returns]
    //  true if the thread is terminated
	bool isTerminated() const;

    // GROUP: Accessors

    ///
    // [Returns]
    //  The current priority of the thread
	jsThdPriority priority() const;

    ///
    // Set the priority of the thread
    //
    // [in]
    //  the new priority of the thread
	void priority( jsThdPriority mp );

    ///
    // [Returns]
    //  the exit code of the thread
	ULONG exitCode();

    ///
    // [Returns]
    //  the name of the thread
	const std::string &name() const { return m_name; }

    // GROUP: Ending the thread

    ///
    // maliciously kill the thread.  This is to be only a last
    // resort as it may leave resources open, and locks locks.  It
    // will most likely leak memory also
	void kill();

    ///
    // stop the thread gracefully.  This sets the internal flag
    // so the thread can check isTerminated(), which is should
    // do often.  Override this function is you need to do something
    // to release the thread, such as set a special event, etc.
    //
    // [in] waitTimeMS how long to wait for the thread to exit
    // [in] killIfTimeout if true, the kill() function will be called
    //      if the thread does not exit in time
    //
    // [Returns]
    //   true if it exited.
	virtual StopRet stop( ULONG waitTimeMS = 60000, 
									bool killIfTimeout = false );
    ///
    // wait for the thread to exit
    //
    // [in] waitTimeMS how long to wait for the thread to exit
    //
    // [Returns]
    //   true if joined ok
	bool join( ULONG WaitMS = INFINITE_WAIT ) const;
 
    // GROUP: thread id functions

    ///
    // static fn to get the current thread id
    //
    // [Returns]
    //   the current thread id
	static jsThreadID currentThreadID();

    ///
    // [Returns]
    //   the thread id
	jsThreadID threadID() const;

    ///
    // Check for thread equality
    //
    // [Returns]
    //   true if the thread ids are equal
	bool operator==( jsThreadID id );

    ///
    // Check for thread inequality
    //
    // [Returns]
    //   true if the thread ids are not equal
	bool operator!=(jsThreadID id ) { return !operator==(id); }

    // GROUP: thread-specific data

    ///
	// create an id for some thread-specific data.  Call this once in 
    // the application and the id is then used by all threads.
    // Call freeThreadData() when this id is no longer needed, when 
    // the app is closing is a good time.
    //
    // [Returns]
    //  an id used in  getThreadData(), setThreadData() and freeThreadData()
	static int		createThreadData();

    ///
	// get a piece of thread-specific data that was set with setThreadData()
    //
    // [in] id id returned by createThreadData()
    //
    // [Returns]
    //  the value set earlier
	static ULONG	getThreadData( int id );

    ///
	// set a piece of thread-specific data 
    //
    // [in] id id returned by createThreadData()
    // [in] value the value to set for this thread
    //
    // [Returns]
    //  true if the value was set 
	static bool		setThreadData( int id, ULONG value );

    ///
	// free any os resources used by createThreadData().  Call this when
    // no threads will use this id any more
    //
    // [in] id id returned by createThreadData()
    //
	static void 	freeThreadData( int id );

	virtual ~jsThread();

protected:
	jsThread( const jsThread & ) { jsAssert(false); }
	jsThread &operator=( const jsThread &) { jsAssert(false); return *this; }

    ///
    // sleep this thread for a bit
    //
    // [in] ms milliseconds to sleep
    //
    // [Returns]
    //  true if sleep returned ok
	bool _sleep( ULONG ms ); 

    ///
    // some os's use this to help improve concurrency.  On others
    //  it is a no op.
	void _yield();

    ///
    // simply set the terminated flag. This is useful if you override
    // the stop() function and need to set the variable first, before
    // you set and event or something
	void _setTerminated();

    /// 
	// override to do real work
    //
    // [Returns]
    //  the thread exit code
	virtual	ULONG _doWork() = 0;

private:
	jsThreadImpl		*m_thd;		// the OS-specific implemenation of the thread
	std::string			m_name;		// the name of the thread
	jsThdPriority		m_priority;	// the given priority of the thread

friend class jsThreadImpl;
};

#endif
 
 
 
 
 
