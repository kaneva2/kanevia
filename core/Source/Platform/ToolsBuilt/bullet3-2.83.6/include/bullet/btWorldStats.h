#ifndef BT_WORLD_STATS_H
#define BT_WORLD_STATS_H

#include "BulletCollision\Collisiondispatch\btCollisionWorld.h"

#ifndef BT_NO_PROFILE
	struct btWorldStats
	{
		btWorldStats(btCollisionWorld* world) 
		{
			m_world = world;
		}

		~btWorldStats()
		{
			m_world = nullptr;
		}

		btWorldStats(const btWorldStats&) = delete;
		btWorldStats& operator=(const btWorldStats&) = delete;

		size_t m_activationStates[ACTIVATION_STATES_COUNT] = { 0,0,0,0,0,0 };
		size_t m_numberOfCollisionObjectsInWorld = { 0 };

		void resetStats();
		void trackCollisionObjectsActivationState(const btCollisionObject* co);
		void trackCollisonObjectsInWorld();

		btCollisionWorld* m_world;
	};

	inline void resetWorldStats(btCollisionWorld* colWorld)
	{
		colWorld->getWorldStats()->resetStats();
		colWorld->getWorldStats()->trackCollisonObjectsInWorld();
	}
	inline void trackWorldStats(btCollisionWorld* colWorld, btCollisionObject* collisionObject)
	{
		colWorld->getWorldStats()->trackCollisionObjectsActivationState(collisionObject);
	}
#else
	inline void resetWorldStats(btCollisionWorld* colWorld) {}
	inline void trackWorldStats(btCollisionWorld* colWorld, btCollisionObject* collisionObject) {}

#endif // BT_NO_PROFILE
#endif // BT_WORLD_STATS_H

