#pragma once

#include "LinearMath/btAlignedObjectArray.h"

namespace KEP {

	class IMemSizeGadget;

	template<typename T>
	void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const btAlignedObjectArray<T>& rAlignedObjectArray)
	{
		if (!rAlignedObjectArray.size()) {

			if (rAlignedObjectArray.capacity() > 0) {
				pMemSizeGadget->AddMemSize(reinterpret_cast<size_t>(&rAlignedObjectArray), rAlignedObjectArray.capacity() * sizeof(T));
			}

		} else {

			// Add each node and any dependent child allocations
			for (int i=0; i < rAlignedObjectArray.size();++i) {
				pMemSizeGadget->AddObject(rAlignedObjectArray.at(i));
			}

			// Add any capacity that is not used
			if (rAlignedObjectArray.size() < rAlignedObjectArray.capacity()) {
				pMemSizeGadget->AddMemSize(reinterpret_cast<size_t>(&rAlignedObjectArray) + rAlignedObjectArray.size() + 1, (rAlignedObjectArray.capacity() - rAlignedObjectArray.size()) * sizeof(T));
			}

		}
	}
}
