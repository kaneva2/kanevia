///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class ReD3DX9StaticMesh;
struct ReMeshData;

class ReShader_Prelighting : public ReShader {
public:
	ReShader_Prelighting() {
		m_Mesh = NULL;
		m_PreLit = FALSE;
	}
	ReShader_Prelighting(ReD3DX9StaticMesh* mesh) {
		m_Mesh = mesh;
		m_PreLit = FALSE;
	}
	ReShader_Prelighting(const ReShader_Prelighting& shader) {
		m_Mesh = shader.m_Mesh;
		m_PreLit = shader.m_PreLit;
	}
	virtual ~ReShader_Prelighting() { ; }

	virtual void PreRender() { ; }
	virtual void Render() { ; }
	virtual void PostRender() { ; }
	virtual void SetTechnique() { ; }
	virtual BOOL Update(const ReShaderData* /*desc*/) { return !m_PreLit ? Restore() : TRUE; }
	virtual void Invalidate() { m_PreLit = FALSE; }
	virtual BOOL EqualTo(const ReShader* /*shader*/) const { return TRUE; }
	virtual INT Compare(const ReShader* /*shader*/) const { return 0; }
	//virtual UINT64			Hash() const							{return ReD3DX9DEVICEINVALID;}
	virtual BOOL Invalid() const { return FALSE; }
	virtual ReShader* Clone(ReMesh* mesh = NULL) { return new ReShader_Prelighting((ReD3DX9StaticMesh*)mesh); }
	virtual ReShaderType ShaderType() const { return ReShaderType::PRELIGHTING; }
	virtual void Begin(UINT& passes) { passes = 1; }
	virtual void BeginPass(UINT /*pass*/) const { ; }
	virtual void EndPass() const { ; }
	virtual void End() const { ; }

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	virtual BOOL Restore();
	virtual void LightVertex(const ReMatProps* mat, const Vector3f* amb, UINT i, DWORD* diff, DWORD* spec) const;

private:
	ReD3DX9StaticMesh* m_Mesh;
	BOOL m_PreLit;
};

} // namespace KEP
