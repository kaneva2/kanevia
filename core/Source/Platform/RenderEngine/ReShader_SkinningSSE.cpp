///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "../Common/include/config.h"
#include "../Common/include/IMemSizeGadget.h"

#include "ReSkinMesh.h"
#include "ReShader_SkinningSSE.h"
#include "ReD3DX9SkinMesh.h"

namespace KEP {

ReShader_SkinningSSE::ReShader_SkinningSSE() {
	m_batchedVertices = 0;
	m_remainingVertices = 0;
	m_SSEVD.pX = NULL;
	m_SSEVD.pY = NULL;
	m_SSEVD.pZ = NULL;
	m_SSEVD.pNX = NULL;
	m_SSEVD.pNY = NULL;
	m_SSEVD.pNZ = NULL;
	m_pSSEMD = NULL;
	m_SSEDataSize = 0;
	m_mesh = NULL;
}

ReShader_SkinningSSE::ReShader_SkinningSSE(ReD3DX9SkinMesh* mesh) {
	m_batchedVertices = 0;
	m_remainingVertices = 0;
	m_SSEVD.pX = NULL;
	m_SSEVD.pY = NULL;
	m_SSEVD.pZ = NULL;
	m_SSEVD.pNX = NULL;
	m_SSEVD.pNY = NULL;
	m_SSEVD.pNZ = NULL;
	m_pSSEMD = NULL;
	m_SSEDataSize = 0;
	m_mesh = mesh;
	InitInternalData();
}

ReShader_SkinningSSE::ReShader_SkinningSSE(const ReShader_SkinningSSE& shader, ReD3DX9SkinMesh* mesh) {
	m_batchedVertices = shader.m_batchedVertices;
	m_remainingVertices = shader.m_remainingVertices;
	m_SSEVD.pX = NULL;
	m_SSEVD.pY = NULL;
	m_SSEVD.pZ = NULL;
	m_SSEVD.pNX = NULL;
	m_SSEVD.pNY = NULL;
	m_SSEVD.pNZ = NULL;
	m_pSSEMD = NULL;
	m_SSEDataSize = 0;
	m_mesh = mesh;
	InitInternalData();
}

ReShader_SkinningSSE::~ReShader_SkinningSSE() {
}

BOOL ReShader_SkinningSSE::Update(const ReShaderData* /*desc*/) {
	if (m_mesh->GetBones()) {
		UpdateMatrices();

		if (m_mesh->m_SkinDesc.Wpv == 1) {
			UpdateSkin1Weight();
		} else if (m_mesh->m_SkinDesc.Wpv == 2) {
			UpdateSkin2Weight();
		} else if (m_mesh->m_SkinDesc.Wpv == 3) {
			UpdateSkin3Weight();
		} else {
			UpdateSkin4Weight();
		}
	}
	return TRUE;
}

void ReShader_SkinningSSE::UpdateSkin1Weight() {
	switch (m_mesh->m_Desc.NumUV) {
		case 0:
		case 1:
			UpdateSkin1WeightSingleTex();
			break;

		case 2:
			UpdateSkin1WeightMultiTex0();
			break;

		case 3:
			UpdateSkin1WeightMultiTex1();
			break;

		case 4:
			UpdateSkin1WeightMultiTex2();
			break;
	}
}

void ReShader_SkinningSSE::UpdateSkin2Weight() {
	switch (m_mesh->m_Desc.NumUV) {
		case 0:
		case 1:
			UpdateSkin2WeightSingleTex();
			break;

		case 2:
			UpdateSkin2WeightMultiTex0();
			break;

		case 3:
			UpdateSkin2WeightMultiTex1();
			break;

		case 4:
			UpdateSkin2WeightMultiTex2();
			break;
	}
}

void ReShader_SkinningSSE::UpdateSkin3Weight() {
	switch (m_mesh->m_Desc.NumUV) {
		case 0:
		case 1:
			UpdateSkin3WeightSingleTex();
			break;

		case 2:
			UpdateSkin3WeightMultiTex0();
			break;

		case 3:
			UpdateSkin3WeightMultiTex1();
			break;

		case 4:
			UpdateSkin3WeightMultiTex2();
			break;
	}
}

void ReShader_SkinningSSE::UpdateSkin4Weight() {
	switch (m_mesh->m_Desc.NumUV) {
		case 0:
		case 1:
			UpdateSkin4WeightSingleTex();
			break;

		case 2:
			UpdateSkin4WeightMultiTex0();
			break;

		case 3:
			UpdateSkin4WeightMultiTex1();
			break;

		case 4:
			UpdateSkin4WeightMultiTex2();
			break;
	}
}

void ReShader_SkinningSSE::InitInternalData() {
	DWORD BatchedVertices = m_mesh->m_Desc.NumV / 4;
	m_batchedVertices = BatchedVertices * 4;
	m_remainingVertices = m_mesh->m_Desc.NumV - m_batchedVertices;
	DWORD ExtraBatch = (m_remainingVertices > 0) ? (1) : (0);

	size_t nSSEVerts = BatchedVertices + ExtraBatch;
	size_t nSSEMatrixData = (sizeof(SSEMatrixData) / 16) * m_mesh->m_SkinDesc.NumB;
	size_t nSSEElements = 6 * nSSEVerts + nSSEMatrixData;
	m_SSEDataSize = nSSEElements * sizeof(__m128);
	m_pSSEData.reset(static_cast<__m128*>(_aligned_malloc(m_SSEDataSize, 16)));
	m_SSEVD.pX = m_pSSEData.get() + 0 * nSSEVerts;
	m_SSEVD.pY = m_pSSEData.get() + 1 * nSSEVerts;
	m_SSEVD.pZ = m_pSSEData.get() + 2 * nSSEVerts;
	m_SSEVD.pNX = m_pSSEData.get() + 3 * nSSEVerts;
	m_SSEVD.pNY = m_pSSEData.get() + 4 * nSSEVerts;
	m_SSEVD.pNZ = m_pSSEData.get() + 5 * nSSEVerts;
	m_pSSEMD = reinterpret_cast<SSEMatrixData*>(m_pSSEData.get() + 6 * nSSEVerts);

	int i, j = 0;
	for (i = 0; i < m_batchedVertices; i += 4) {
		LoadFourFloats(
			m_mesh->m_Desc.pP[i].x,
			m_mesh->m_Desc.pP[i + 1].x,
			m_mesh->m_Desc.pP[i + 2].x,
			m_mesh->m_Desc.pP[i + 3].x,
			&m_SSEVD.pX[j]);
		LoadFourFloats(
			m_mesh->m_Desc.pP[i].y,
			m_mesh->m_Desc.pP[i + 1].y,
			m_mesh->m_Desc.pP[i + 2].y,
			m_mesh->m_Desc.pP[i + 3].y,
			&m_SSEVD.pY[j]);
		LoadFourFloats(
			m_mesh->m_Desc.pP[i].z,
			m_mesh->m_Desc.pP[i + 1].z,
			m_mesh->m_Desc.pP[i + 2].z,
			m_mesh->m_Desc.pP[i + 3].z,
			&m_SSEVD.pZ[j]);

		LoadFourFloats(
			m_mesh->m_Desc.pN[i].x,
			m_mesh->m_Desc.pN[i + 1].x,
			m_mesh->m_Desc.pN[i + 2].x,
			m_mesh->m_Desc.pN[i + 3].x,
			&m_SSEVD.pNX[j]);
		LoadFourFloats(
			m_mesh->m_Desc.pN[i].y,
			m_mesh->m_Desc.pN[i + 1].y,
			m_mesh->m_Desc.pN[i + 2].y,
			m_mesh->m_Desc.pN[i + 3].y,
			&m_SSEVD.pNY[j]);
		LoadFourFloats(
			m_mesh->m_Desc.pN[i].z,
			m_mesh->m_Desc.pN[i + 1].z,
			m_mesh->m_Desc.pN[i + 2].z,
			m_mesh->m_Desc.pN[i + 3].z,
			&m_SSEVD.pNZ[j]);
		j++;
	}

	if (ExtraBatch) {
		float TempX[4], TempY[4], TempZ[4], TempNX[4], TempNY[4], TempNZ[4];

		j = 0;
		for (i = m_batchedVertices; i < (int)m_mesh->m_Desc.NumV; i++) {
			TempX[j] = m_mesh->m_Desc.pP[i].x;
			TempY[j] = m_mesh->m_Desc.pP[i].y;
			TempZ[j] = m_mesh->m_Desc.pP[i].z;
			TempNX[j] = m_mesh->m_Desc.pN[i].x;
			TempNY[j] = m_mesh->m_Desc.pN[i].y;
			TempNZ[j] = m_mesh->m_Desc.pN[i].z;
			j++;
		}
		for (; j < 4; j++) {
			TempX[j] = TempX[0];
			TempY[j] = TempY[0];
			TempZ[j] = TempZ[0];
			TempNX[j] = TempNX[0];
			TempNY[j] = TempNY[0];
			TempNZ[j] = TempNZ[0];
		}

		LoadFourFloats(TempX[0], TempX[1], TempX[2], TempX[3], &m_SSEVD.pX[BatchedVertices]);
		LoadFourFloats(TempY[0], TempY[1], TempY[2], TempY[3], &m_SSEVD.pY[BatchedVertices]);
		LoadFourFloats(TempZ[0], TempZ[1], TempZ[2], TempZ[3], &m_SSEVD.pZ[BatchedVertices]);
		LoadFourFloats(TempNX[0], TempNX[1], TempNX[2], TempNX[3], &m_SSEVD.pNX[BatchedVertices]);
		LoadFourFloats(TempNY[0], TempNY[1], TempNY[2], TempNY[3], &m_SSEVD.pNY[BatchedVertices]);
		LoadFourFloats(TempNZ[0], TempNZ[1], TempNZ[2], TempNZ[3], &m_SSEVD.pNZ[BatchedVertices]);
	}
}

void ReShader_SkinningSSE::UpdateMatrices() {
	// DRF - Crash Fix
	if (!m_mesh || !m_pSSEMD)
		return;

	// DRF - Crash Fix
	const Matrix44f* pBones = m_mesh->GetBones();
	if (!pBones)
		return;

	// DRF - Performance Gain
	DWORD numBones = m_mesh->m_SkinDesc.NumB;
	for (DWORD i = 0; i < numBones; i++) {
		const Matrix44f& B = pBones[i];
		LoadFourFloats(B(0, 0), B(1, 0), B(2, 0), B(3, 0), &m_pSSEMD[i].M[0]);
		LoadFourFloats(B(0, 1), B(1, 1), B(2, 1), B(3, 1), &m_pSSEMD[i].M[1]);
		LoadFourFloats(B(0, 2), B(1, 2), B(2, 2), B(3, 2), &m_pSSEMD[i].M[2]);
	}
}

__forceinline void ReShader_SkinningSSE::LoadFourFloats(const float& a0, const float& a1, const float& a2, const float& a3, __m128* dev) {
	register __m128 xmm0 = _mm_load_ss(&a0); // 0 0 0 a0
	register __m128 xmm1 = _mm_load_ss(&a1); // 0 0 0 a1
	register __m128 xmm2 = _mm_load_ss(&a2); // 0 0 0 a2
	register __m128 xmm3 = _mm_load_ss(&a3); // 0 0 0 a3
	xmm0 = _mm_movelh_ps(xmm0, xmm2); // 0 a2 0 a0
	xmm1 = _mm_shuffle_ps(xmm1, xmm3, _MM_SHUFFLE(0, 1, 0, 1)); // a3 0 a1 0
	*dev = _mm_or_ps(xmm0, xmm1); // a3 a2 a1 a0
}

__forceinline void ReShader_SkinningSSE::StoreFourFloats(float* a0, float* a1, float* a2, float* a3, __m128 src) {
	register __m128 xmm0 = _mm_unpackhi_ps(src, src); //
	register __m128 xmm1 = _mm_unpacklo_ps(src, src); //
	register __m128 xmm2 = _mm_movehl_ps(xmm0, xmm0); //
	register __m128 xmm3 = _mm_movehl_ps(xmm1, xmm1); //
	_mm_store_ss(a0, xmm1); //
	_mm_store_ss(a1, xmm3); //
	_mm_store_ss(a2, xmm0); //
	_mm_store_ss(a3, xmm2); //
}

__forceinline void ReShader_SkinningSSE::Reshuffle(__m128* pM1, __m128* pM2, __m128* pM3, __m128* pM4,
	__m128* pR1, __m128* pR2, __m128* pR3, __m128* pR4) {
	register __m128 xmm0, xmm1;
	xmm0 = _mm_shuffle_ps(*pM1, *pM2, _MM_SHUFFLE(3, 2, 3, 2)); //
	xmm1 = _mm_shuffle_ps(*pM3, *pM4, _MM_SHUFFLE(3, 2, 3, 2)); //
	*pR4 = _mm_shuffle_ps(xmm0, xmm1, _MM_SHUFFLE(3, 1, 3, 1)); //
	*pR3 = _mm_shuffle_ps(xmm0, xmm1, _MM_SHUFFLE(2, 0, 2, 0)); //
	xmm0 = _mm_shuffle_ps(*pM1, *pM2, _MM_SHUFFLE(1, 0, 1, 0)); //
	xmm1 = _mm_shuffle_ps(*pM3, *pM4, _MM_SHUFFLE(1, 0, 1, 0)); //
	*pR2 = _mm_shuffle_ps(xmm0, xmm1, _MM_SHUFFLE(3, 1, 3, 1)); //
	*pR1 = _mm_shuffle_ps(xmm0, xmm1, _MM_SHUFFLE(2, 0, 2, 0)); //
}

__forceinline void ReShader_SkinningSSE::Collapse2Mat(BYTE MI1, BYTE MI2, float W1, float W2,
	__m128* pR) {
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6;

	// Load and propagate the matrix weight 1.
	xmm0 = _mm_load_ss(&W1);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	if (W2 == 0.f) {
		// Multiply matrix 1 by weight 1 and return
		pR[0] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[0]);
		pR[1] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[1]);
		pR[2] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[2]);
		return;
	}

	// Multiply matrix 1 by weight 1.
	xmm1 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[0]);
	xmm2 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[1]);
	xmm3 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[2]);

	// Load weight 2.
	xmm0 = _mm_load_ss(&W2);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	// Multiply matrix 2 by weight 2.
	xmm4 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[0]);
	xmm5 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[1]);
	xmm6 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[2]);

	// Add matrix 1 to matrix 2.
	pR[0] = _mm_add_ps(xmm1, xmm4);
	pR[1] = _mm_add_ps(xmm2, xmm5);
	pR[2] = _mm_add_ps(xmm3, xmm6);
}

__forceinline void ReShader_SkinningSSE::Collapse3Mat(BYTE MI1, BYTE MI2, BYTE MI3, float W1,
	float W2, float W3, __m128* pR) {
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6;

	// Load and propagate the matrix weight 1.
	xmm0 = _mm_load_ss(&W1);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	if (W2 == 0.f) {
		// Multiply matrix 1 by weight 1 and return
		pR[0] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[0]);
		pR[1] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[1]);
		pR[2] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[2]);
		return;
	}

	// Multiply matrix 1 by weight 1.
	xmm1 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[0]);
	xmm2 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[1]);
	xmm3 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[2]);

	// Load weight 2.
	xmm0 = _mm_load_ss(&W2);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	// Multiply matrix 2 by weight 2.
	xmm4 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[0]);
	xmm5 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[1]);
	xmm6 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[2]);

	if (W3 == 0.f) {
		// Add matrix 1 to matrix 2.
		pR[0] = _mm_add_ps(xmm1, xmm4);
		pR[1] = _mm_add_ps(xmm2, xmm5);
		pR[2] = _mm_add_ps(xmm3, xmm6);
		return;
	}

	// Add matrix 1 to matrix 2.
	xmm1 = _mm_add_ps(xmm1, xmm4);
	xmm2 = _mm_add_ps(xmm2, xmm5);
	xmm3 = _mm_add_ps(xmm3, xmm6);

	// Load weight 3.
	xmm0 = _mm_load_ss(&W3);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	// Multiply matrix 3 by weight 3.
	xmm4 = _mm_mul_ps(xmm0, m_pSSEMD[MI3].M[0]);
	xmm5 = _mm_mul_ps(xmm0, m_pSSEMD[MI3].M[1]);
	xmm6 = _mm_mul_ps(xmm0, m_pSSEMD[MI3].M[2]);

	// Add matrix 1 and 2 to matrix 3.
	pR[0] = _mm_add_ps(xmm1, xmm4);
	pR[1] = _mm_add_ps(xmm2, xmm5);
	pR[2] = _mm_add_ps(xmm3, xmm6);
}

__forceinline void ReShader_SkinningSSE::Collapse4Mat(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4,
	float W1, float W2, float W3, float W4, __m128* pR) {
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6;

	// Load and propagate the matrix weight 1.
	xmm0 = _mm_load_ss(&W1);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	if (W2 == 0.f) {
		// Multiply matrix 1 by weight 1 and return
		pR[0] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[0]);
		pR[1] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[1]);
		pR[2] = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[2]);
		return;
	}

	// Multiply matrix 1 by weight 1.
	xmm1 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[0]);
	xmm2 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[1]);
	xmm3 = _mm_mul_ps(xmm0, m_pSSEMD[MI1].M[2]);

	// Load weight 2.
	xmm0 = _mm_load_ss(&W2);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	// Multiply matrix 2 by weight 2.
	xmm4 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[0]);
	xmm5 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[1]);
	xmm6 = _mm_mul_ps(xmm0, m_pSSEMD[MI2].M[2]);

	if (W3 == 0.f) {
		// Add matrix 1 to matrix 2.
		pR[0] = _mm_add_ps(xmm1, xmm4);
		pR[1] = _mm_add_ps(xmm2, xmm5);
		pR[2] = _mm_add_ps(xmm3, xmm6);
		return;
	}

	// Add matrix 1 to matrix 2.
	xmm1 = _mm_add_ps(xmm1, xmm4);
	xmm2 = _mm_add_ps(xmm2, xmm5);
	xmm3 = _mm_add_ps(xmm3, xmm6);

	// Load weight 3.
	xmm0 = _mm_load_ss(&W3);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	// Multiply matrix 3 by weight 3.
	xmm4 = _mm_mul_ps(xmm0, m_pSSEMD[MI3].M[0]);
	xmm5 = _mm_mul_ps(xmm0, m_pSSEMD[MI3].M[1]);
	xmm6 = _mm_mul_ps(xmm0, m_pSSEMD[MI3].M[2]);

	if (W4 == 0.f) {
		// Add matrix 1 and 2 to matrix 3.
		pR[0] = _mm_add_ps(xmm1, xmm4);
		pR[1] = _mm_add_ps(xmm2, xmm5);
		pR[2] = _mm_add_ps(xmm3, xmm6);
		return;
	}

	// Add matrix 1 and 2 to matrix 3.
	xmm1 = _mm_add_ps(xmm1, xmm4);
	xmm2 = _mm_add_ps(xmm2, xmm5);
	xmm3 = _mm_add_ps(xmm3, xmm6);

	// Load weight 4.
	xmm0 = _mm_load_ss(&W4);
	xmm0 = _mm_shuffle_ps(xmm0, xmm0, 0);

	// Multiply matrix 4 by weight 4.
	xmm4 = _mm_mul_ps(xmm0, m_pSSEMD[MI4].M[0]);
	xmm5 = _mm_mul_ps(xmm0, m_pSSEMD[MI4].M[1]);
	xmm6 = _mm_mul_ps(xmm0, m_pSSEMD[MI4].M[2]);

	// Add matrix 1, 2 and 3 to matrix 4.
	pR[0] = _mm_add_ps(xmm1, xmm4);
	pR[1] = _mm_add_ps(xmm2, xmm5);
	pR[2] = _mm_add_ps(xmm3, xmm6);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4,
	__m128* pX, __m128* pY, __m128* pZ, __m128* pNX, __m128* pNY,
	__m128* pNZ, ReOutputVertex0_D3DX9* pOV) {
	// 7 xmm registers :-).
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&m_pSSEMD[MI1].M[0], &m_pSSEMD[MI2].M[0], &m_pSSEMD[MI3].M[0], &m_pSSEMD[MI4].M[0], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm4);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[1], &m_pSSEMD[MI2].M[1], &m_pSSEMD[MI3].M[1], &m_pSSEMD[MI4].M[1], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm4);

	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[2], &m_pSSEMD[MI2].M[2], &m_pSSEMD[MI3].M[2], &m_pSSEMD[MI4].M[2], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm4);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4,
	__m128* pX, __m128* pY, __m128* pZ, __m128* pNX, __m128* pNY,
	__m128* pNZ, ReOutputVertex3_D3DX9* pOV) {
	// 7 xmm registers :-).
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&m_pSSEMD[MI1].M[0], &m_pSSEMD[MI2].M[0], &m_pSSEMD[MI3].M[0], &m_pSSEMD[MI4].M[0], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm4);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[1], &m_pSSEMD[MI2].M[1], &m_pSSEMD[MI3].M[1], &m_pSSEMD[MI4].M[1], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm4);

	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[2], &m_pSSEMD[MI2].M[2], &m_pSSEMD[MI3].M[2], &m_pSSEMD[MI4].M[2], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm4);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4,
	__m128* pX, __m128* pY, __m128* pZ, __m128* pNX, __m128* pNY,
	__m128* pNZ, ReOutputVertex4_D3DX9* pOV) {
	// 7 xmm registers :-).
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&m_pSSEMD[MI1].M[0], &m_pSSEMD[MI2].M[0], &m_pSSEMD[MI3].M[0], &m_pSSEMD[MI4].M[0], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm4);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[1], &m_pSSEMD[MI2].M[1], &m_pSSEMD[MI3].M[1], &m_pSSEMD[MI4].M[1], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm4);

	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[2], &m_pSSEMD[MI2].M[2], &m_pSSEMD[MI3].M[2], &m_pSSEMD[MI4].M[2], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm4);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4,
	__m128* pX, __m128* pY, __m128* pZ, __m128* pNX, __m128* pNY,
	__m128* pNZ, ReOutputVertex5_D3DX9* pOV) {
	// 7 xmm registers :-).
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&m_pSSEMD[MI1].M[0], &m_pSSEMD[MI2].M[0], &m_pSSEMD[MI3].M[0], &m_pSSEMD[MI4].M[0], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm4);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[1], &m_pSSEMD[MI2].M[1], &m_pSSEMD[MI3].M[1], &m_pSSEMD[MI4].M[1], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm4);

	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&m_pSSEMD[MI1].M[2], &m_pSSEMD[MI2].M[2], &m_pSSEMD[MI3].M[2], &m_pSSEMD[MI4].M[2], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm4);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY,
	__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex0_D3DX9* pOV) {
	// 9 xmm registers :-(.
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7, xmm8;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&pM[0], &pM[3], &pM[6], &pM[9], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm7 = _mm_add_ps(xmm4, xmm6);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&pM[1], &pM[4], &pM[7], &pM[10], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm8 = _mm_add_ps(xmm4, xmm6);

	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&pM[2], &pM[5], &pM[8], &pM[11], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Normalise the normals.
	xmm0 = _mm_mul_ps(xmm7, xmm7);
	xmm1 = _mm_mul_ps(xmm8, xmm8);
	xmm2 = _mm_mul_ps(xmm4, xmm4);
	xmm0 = _mm_add_ps(xmm0, xmm1);
	xmm0 = _mm_add_ps(xmm0, xmm2);
	xmm0 = _mm_rsqrt_ps(xmm0);
	xmm1 = _mm_mul_ps(xmm7, xmm0);
	xmm2 = _mm_mul_ps(xmm8, xmm0);
	xmm3 = _mm_mul_ps(xmm4, xmm0);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm1);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm2);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm3);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY,
	__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex3_D3DX9* pOV) {
	// 9 xmm registers :-(.
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7, xmm8;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&pM[0], &pM[3], &pM[6], &pM[9], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm7 = _mm_add_ps(xmm4, xmm6);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&pM[1], &pM[4], &pM[7], &pM[10], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm8 = _mm_add_ps(xmm4, xmm6);

	///////////////////////////////////////////////////////////////////////////
	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&pM[2], &pM[5], &pM[8], &pM[11], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Normalise the normals.
	xmm0 = _mm_mul_ps(xmm7, xmm7);
	xmm1 = _mm_mul_ps(xmm8, xmm8);
	xmm2 = _mm_mul_ps(xmm4, xmm4);
	xmm0 = _mm_add_ps(xmm0, xmm1);
	xmm0 = _mm_add_ps(xmm0, xmm2);
	xmm0 = _mm_rsqrt_ps(xmm0);
	xmm1 = _mm_mul_ps(xmm7, xmm0);
	xmm2 = _mm_mul_ps(xmm8, xmm0);
	xmm3 = _mm_mul_ps(xmm4, xmm0);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm1);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm2);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm3);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY,
	__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex4_D3DX9* pOV) {
	// 9 xmm registers :-(.
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7, xmm8;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&pM[0], &pM[3], &pM[6], &pM[9], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm7 = _mm_add_ps(xmm4, xmm6);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&pM[1], &pM[4], &pM[7], &pM[10], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm8 = _mm_add_ps(xmm4, xmm6);

	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&pM[2], &pM[5], &pM[8], &pM[11], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Normalise the normals.
	xmm0 = _mm_mul_ps(xmm7, xmm7);
	xmm1 = _mm_mul_ps(xmm8, xmm8);
	xmm2 = _mm_mul_ps(xmm4, xmm4);
	xmm0 = _mm_add_ps(xmm0, xmm1);
	xmm0 = _mm_add_ps(xmm0, xmm2);
	xmm0 = _mm_rsqrt_ps(xmm0);
	xmm1 = _mm_mul_ps(xmm7, xmm0);
	xmm2 = _mm_mul_ps(xmm8, xmm0);
	xmm3 = _mm_mul_ps(xmm4, xmm0);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm1);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm2);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm3);
}

__forceinline void ReShader_SkinningSSE::Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY,
	__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex5_D3DX9* pOV) {
	// 9 xmm registers :-(.
	register __m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7, xmm8;

	///////////////////////////////////////////////////////////////////////////
	// We process the X,Y and Z components of the input position by the four
	// bone matrices separately. So we do all the X's first etc. Here is the
	// maths for the X's to give you an idea of what is happening.

	// Firstly the rotational part of the transform.
	// pR1->x=pM1->00*pP1->x+pM1->10*pP1->y+pM1->20*pP1->z;
	// pR2->x=pM2->00*pP2->x+pM2->10*pP2->y+pM2->20*pP2->z;
	// pR3->x=pM3->00*pP3->x+pM3->10*pP3->y+pM3->20*pP3->z;
	// pR4->x=pM4->00*pP4->x+pM4->10*pP4->y+pM4->20*pP4->z;

	// The translational component.
	// pR1->x += pM1->30;
	// pR2->x += pM2->30;
	// pR3->x += pM3->30;
	// pR4->x += pM4->30;

	// Do the X's.

	// Load 1st column of each bone matrix.
	Reshuffle(&pM[0], &pM[3], &pM[6], &pM[9], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed x components [POSITION].
	StoreFourFloats(&pOV[0].X, &pOV[1].X, &pOV[2].X, &pOV[3].X, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm7 = _mm_add_ps(xmm4, xmm6);

	// Do the Y's.

	// Reorder the data so that we use the second column of each matrix.
	Reshuffle(&pM[1], &pM[4], &pM[7], &pM[10], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed y components [POSITION].
	StoreFourFloats(&pOV[0].Y, &pOV[1].Y, &pOV[2].Y, &pOV[3].Y, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm8 = _mm_add_ps(xmm4, xmm6);

	// Do the Z's.

	// Reorder the data so that we use the third column of each matrix.
	Reshuffle(&pM[2], &pM[5], &pM[8], &pM[11], &xmm0, &xmm1, &xmm2, &xmm3);

	// Do the multiplies for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_mul_ps(*pX, xmm0);
	xmm5 = _mm_mul_ps(*pY, xmm1);
	xmm6 = _mm_mul_ps(*pZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Now add the translational part [POSITION].
	xmm4 = _mm_add_ps(xmm4, xmm3);

	// Store transformed z components [POSITION].
	StoreFourFloats(&pOV[0].Z, &pOV[1].Z, &pOV[2].Z, &pOV[3].Z, xmm4);

	// Do the multiplies for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_mul_ps(*pNX, xmm0);
	xmm5 = _mm_mul_ps(*pNY, xmm1);
	xmm6 = _mm_mul_ps(*pNZ, xmm2);

	// Do the adds for the rotational part of the bone matrices [NORMAL].
	xmm4 = _mm_add_ps(xmm4, xmm5);
	xmm4 = _mm_add_ps(xmm4, xmm6);

	// Normalise the normals.
	xmm0 = _mm_mul_ps(xmm7, xmm7);
	xmm1 = _mm_mul_ps(xmm8, xmm8);
	xmm2 = _mm_mul_ps(xmm4, xmm4);
	xmm0 = _mm_add_ps(xmm0, xmm1);
	xmm0 = _mm_add_ps(xmm0, xmm2);
	xmm0 = _mm_rsqrt_ps(xmm0);
	xmm1 = _mm_mul_ps(xmm7, xmm0);
	xmm2 = _mm_mul_ps(xmm8, xmm0);
	xmm3 = _mm_mul_ps(xmm4, xmm0);

	// Store transformed x components [NORMAL].
	StoreFourFloats(&pOV[0].NX, &pOV[1].NX, &pOV[2].NX, &pOV[3].NX, xmm1);

	// Store transformed y components [NORMAL].
	StoreFourFloats(&pOV[0].NY, &pOV[1].NY, &pOV[2].NY, &pOV[3].NY, xmm2);

	// Store transformed z components [NORMAL].
	StoreFourFloats(&pOV[0].NZ, &pOV[1].NZ, &pOV[2].NZ, &pOV[3].NZ, xmm3);
}

void ReShader_SkinningSSE::UpdateSkin1WeightSingleTex() {
	int i, j, k;

	ReOutputVertex0_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Vert4Mat4(m_mesh->m_SkinDesc.pBI[i], m_mesh->m_SkinDesc.pBI[i + 1], m_mesh->m_SkinDesc.pBI[i + 2],
				m_mesh->m_SkinDesc.pBI[i + 3], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0; k < 4; k++) {
				pVB[k].U = m_mesh->m_Desc.pUv[i + k].x;
				pVB[k].V = m_mesh->m_Desc.pUv[i + k].y;
			}
			pVB += 4;

			j++;
		}

		BYTE BoneIndices[4] = { 0, 0, 0, 0 };
		for (i = 0; i < m_remainingVertices; i++) {
			BoneIndices[i] = m_mesh->m_SkinDesc.pBI[m_batchedVertices + i];
		}

		ReOutputVertex0_D3DX9 OV[4];
		Vert4Mat4(BoneIndices[0], BoneIndices[1], BoneIndices[2], BoneIndices[3], &m_SSEVD.pX[j],
			&m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0; i < m_remainingVertices; i++) {
			OV[i].U = m_mesh->m_Desc.pUv[m_batchedVertices + i].x;
			OV[i].V = m_mesh->m_Desc.pUv[m_batchedVertices + i].y;
			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin2WeightSingleTex() {
	int i, j, k;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex0_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 2;
			Offset2 = (i + 1) * 2;
			Offset3 = (i + 2) * 2;
			Offset4 = (i + 3) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[0]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2],
				m_mesh->m_SkinDesc.pW[Offset2 + 1], &CollapsedMatrices[3]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3],
				m_mesh->m_SkinDesc.pW[Offset3 + 1], &CollapsedMatrices[6]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4],
				m_mesh->m_SkinDesc.pW[Offset4 + 1], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
				&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0; k < 4; k++) {
				pVB[k].U = m_mesh->m_Desc.pUv[i + k].x;
				pVB[k].V = m_mesh->m_Desc.pUv[i + k].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex0_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
			&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0; i < m_remainingVertices; i++) {
			OV[i].U = m_mesh->m_Desc.pUv[m_batchedVertices + i].x;
			OV[i].V = m_mesh->m_Desc.pUv[m_batchedVertices + i].y;
			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin3WeightSingleTex() {
	int i, j, k;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex0_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 3;
			Offset2 = (i + 1) * 3;
			Offset3 = (i + 2) * 3;
			Offset4 = (i + 3) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[0]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2], &CollapsedMatrices[3]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2], &CollapsedMatrices[6]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0; k < 4; k++) {
				pVB[k].U = m_mesh->m_Desc.pUv[i + k].x;
				pVB[k].V = m_mesh->m_Desc.pUv[i + k].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex0_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0; i < m_remainingVertices; i++) {
			OV[i].U = m_mesh->m_Desc.pUv[m_batchedVertices + i].x;
			OV[i].V = m_mesh->m_Desc.pUv[m_batchedVertices + i].y;
			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin4WeightSingleTex() {
	int i, j, k;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex0_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 4;
			Offset2 = (i + 1) * 4;
			Offset3 = (i + 2) * 4;
			Offset4 = (i + 3) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[0]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pBI[Offset2 + 3], m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2 + 3], &CollapsedMatrices[3]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pBI[Offset3 + 3], m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3 + 3], &CollapsedMatrices[6]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pBI[Offset4 + 3], m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4 + 3], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0; k < 4; k++) {
				pVB[k].U = m_mesh->m_Desc.pUv[i + k].x;
				pVB[k].V = m_mesh->m_Desc.pUv[i + k].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex0_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0; i < m_remainingVertices; i++) {
			OV[i].U = m_mesh->m_Desc.pUv[m_batchedVertices + i].x;
			OV[i].V = m_mesh->m_Desc.pUv[m_batchedVertices + i].y;
			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin1WeightMultiTex0() {
	int i, j, k, l;

	ReOutputVertex3_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex3_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Vert4Mat4(m_mesh->m_SkinDesc.pBI[i], m_mesh->m_SkinDesc.pBI[i + 1], m_mesh->m_SkinDesc.pBI[i + 2],
				m_mesh->m_SkinDesc.pBI[i + 3], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 2) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].y;
			}
			pVB += 4;

			j++;
		}

		BYTE BoneIndices[4] = { 0, 0, 0, 0 };
		for (i = 0; i < m_remainingVertices; i++) {
			BoneIndices[i] = m_mesh->m_SkinDesc.pBI[m_batchedVertices + i];
		}

		ReOutputVertex3_D3DX9 OV[4];
		Vert4Mat4(BoneIndices[0], BoneIndices[1], BoneIndices[2], BoneIndices[3], &m_SSEVD.pX[j],
			&m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 2) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin2WeightMultiTex0() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex3_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex3_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 2;
			Offset2 = (i + 1) * 2;
			Offset3 = (i + 2) * 2;
			Offset4 = (i + 3) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[0]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2],
				m_mesh->m_SkinDesc.pW[Offset2 + 1], &CollapsedMatrices[3]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3],
				m_mesh->m_SkinDesc.pW[Offset3 + 1], &CollapsedMatrices[6]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4],
				m_mesh->m_SkinDesc.pW[Offset4 + 1], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
				&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 2) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex3_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
			&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 2) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin3WeightMultiTex0() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex3_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex3_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 3;
			Offset2 = (i + 1) * 3;
			Offset3 = (i + 2) * 3;
			Offset4 = (i + 3) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[0]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2], &CollapsedMatrices[3]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2], &CollapsedMatrices[6]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 2) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex3_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 2) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin4WeightMultiTex0() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex3_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex3_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 4;
			Offset2 = (i + 1) * 4;
			Offset3 = (i + 2) * 4;
			Offset4 = (i + 3) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[0]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pBI[Offset2 + 3], m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2 + 3], &CollapsedMatrices[3]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pBI[Offset3 + 3], m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3 + 3], &CollapsedMatrices[6]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pBI[Offset4 + 3], m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4 + 3], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 2) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 2 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 2 + l + 1].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex3_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 2) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 2 + j + 1].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin1WeightMultiTex1() {
	int i, j, k, l;

	ReOutputVertex4_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex4_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Vert4Mat4(m_mesh->m_SkinDesc.pBI[i], m_mesh->m_SkinDesc.pBI[i + 1], m_mesh->m_SkinDesc.pBI[i + 2],
				m_mesh->m_SkinDesc.pBI[i + 3], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 3) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].y;
			}
			pVB += 4;

			j++;
		}

		BYTE BoneIndices[4] = { 0, 0, 0, 0 };
		for (i = 0; i < m_remainingVertices; i++) {
			BoneIndices[i] = m_mesh->m_SkinDesc.pBI[m_batchedVertices + i];
		}

		ReOutputVertex4_D3DX9 OV[4];
		Vert4Mat4(BoneIndices[0], BoneIndices[1], BoneIndices[2], BoneIndices[3], &m_SSEVD.pX[j],
			&m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 3) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin2WeightMultiTex1() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex4_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex4_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 2;
			Offset2 = (i + 1) * 2;
			Offset3 = (i + 2) * 2;
			Offset4 = (i + 3) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[0]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2],
				m_mesh->m_SkinDesc.pW[Offset2 + 1], &CollapsedMatrices[3]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3],
				m_mesh->m_SkinDesc.pW[Offset3 + 1], &CollapsedMatrices[6]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4],
				m_mesh->m_SkinDesc.pW[Offset4 + 1], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
				&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 3) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex4_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
			&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 3) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin3WeightMultiTex1() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex4_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex4_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 3;
			Offset2 = (i + 1) * 3;
			Offset3 = (i + 2) * 3;
			Offset4 = (i + 3) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[0]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2], &CollapsedMatrices[3]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2], &CollapsedMatrices[6]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 3) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex4_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 3) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin4WeightMultiTex1() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex4_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex4_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 4;
			Offset2 = (i + 1) * 4;
			Offset3 = (i + 2) * 4;
			Offset4 = (i + 3) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[0]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pBI[Offset2 + 3], m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2 + 3], &CollapsedMatrices[3]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pBI[Offset3 + 3], m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3 + 3], &CollapsedMatrices[6]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pBI[Offset4 + 3], m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4 + 3], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 3) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 3 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 3 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 3 + l + 2].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex4_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 3) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 3 + j + 2].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin1WeightMultiTex2() {
	int i, j, k, l;

	ReOutputVertex5_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex5_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Vert4Mat4(m_mesh->m_SkinDesc.pBI[i], m_mesh->m_SkinDesc.pBI[i + 1], m_mesh->m_SkinDesc.pBI[i + 2],
				m_mesh->m_SkinDesc.pBI[i + 3], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 4) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].y;
				pVB[k].U3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].x;
				pVB[k].V3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].y;
			}
			pVB += 4;

			j++;
		}

		BYTE BoneIndices[4] = { 0, 0, 0, 0 };
		for (i = 0; i < m_remainingVertices; i++) {
			BoneIndices[i] = m_mesh->m_SkinDesc.pBI[m_batchedVertices + i];
		}

		ReOutputVertex5_D3DX9 OV[4];
		Vert4Mat4(BoneIndices[0], BoneIndices[1], BoneIndices[2], BoneIndices[3], &m_SSEVD.pX[j],
			&m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 4) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].y;
			OV[i].U3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].x;
			OV[i].V3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin2WeightMultiTex2() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex5_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex5_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 2;
			Offset2 = (i + 1) * 2;
			Offset3 = (i + 2) * 2;
			Offset4 = (i + 3) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[0]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2],
				m_mesh->m_SkinDesc.pW[Offset2 + 1], &CollapsedMatrices[3]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3],
				m_mesh->m_SkinDesc.pW[Offset3 + 1], &CollapsedMatrices[6]);
			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4],
				m_mesh->m_SkinDesc.pW[Offset4 + 1], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
				&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 4) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].y;
				pVB[k].U3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].x;
				pVB[k].V3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 2;

			Collapse2Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1],
				m_mesh->m_SkinDesc.pW[Offset1 + 1], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex5_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j],
			&m_SSEVD.pNX[j], &m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 4) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].y;
			OV[i].U3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].x;
			OV[i].V3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin3WeightMultiTex2() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex5_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex5_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 3;
			Offset2 = (i + 1) * 3;
			Offset3 = (i + 2) * 3;
			Offset4 = (i + 3) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[0]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2], &CollapsedMatrices[3]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2], &CollapsedMatrices[6]);
			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 4) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].y;
				pVB[k].U3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].x;
				pVB[k].V3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 3;

			Collapse3Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex5_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 4) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].y;
			OV[i].U3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].x;
			OV[i].V3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::UpdateSkin4WeightMultiTex2() {
	int i, j, k, l;
	__m128 CollapsedMatrices[12];
	int Offset1, Offset2, Offset3, Offset4;

	ReOutputVertex5_D3DX9* pVB = NULL;

	// Access the vertex buffer... use "DISCARD" to double buffer the VB and void blocking "Lock()" call
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex5_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) == D3D_OK && pVB) {
		j = 0;
		for (i = 0; i < m_batchedVertices; i += 4) {
			Offset1 = i * 4;
			Offset2 = (i + 1) * 4;
			Offset3 = (i + 2) * 4;
			Offset4 = (i + 3) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[0]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset2], m_mesh->m_SkinDesc.pBI[Offset2 + 1], m_mesh->m_SkinDesc.pBI[Offset2 + 2],
				m_mesh->m_SkinDesc.pBI[Offset2 + 3], m_mesh->m_SkinDesc.pW[Offset2], m_mesh->m_SkinDesc.pW[Offset2 + 1], m_mesh->m_SkinDesc.pW[Offset2 + 2],
				m_mesh->m_SkinDesc.pW[Offset2 + 3], &CollapsedMatrices[3]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset3], m_mesh->m_SkinDesc.pBI[Offset3 + 1], m_mesh->m_SkinDesc.pBI[Offset3 + 2],
				m_mesh->m_SkinDesc.pBI[Offset3 + 3], m_mesh->m_SkinDesc.pW[Offset3], m_mesh->m_SkinDesc.pW[Offset3 + 1], m_mesh->m_SkinDesc.pW[Offset3 + 2],
				m_mesh->m_SkinDesc.pW[Offset3 + 3], &CollapsedMatrices[6]);
			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset4], m_mesh->m_SkinDesc.pBI[Offset4 + 1], m_mesh->m_SkinDesc.pBI[Offset4 + 2],
				m_mesh->m_SkinDesc.pBI[Offset4 + 3], m_mesh->m_SkinDesc.pW[Offset4], m_mesh->m_SkinDesc.pW[Offset4 + 1], m_mesh->m_SkinDesc.pW[Offset4 + 2],
				m_mesh->m_SkinDesc.pW[Offset4 + 3], &CollapsedMatrices[9]);
			Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
				&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], pVB);

			for (k = 0, l = 0; k < 4; k++, l += 4) {
				pVB[k].U0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].x;
				pVB[k].V0 = m_mesh->m_Desc.pUv[i * 4 + l + 0].y;
				pVB[k].U1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].x;
				pVB[k].V1 = m_mesh->m_Desc.pUv[i * 4 + l + 1].y;
				pVB[k].U2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].x;
				pVB[k].V2 = m_mesh->m_Desc.pUv[i * 4 + l + 2].y;
				pVB[k].U3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].x;
				pVB[k].V3 = m_mesh->m_Desc.pUv[i * 4 + l + 3].y;
			}
			pVB += 4;

			j++;
		}

		for (i = 0; i < m_remainingVertices; i++) {
			Offset1 = (m_batchedVertices + i) * 4;

			Collapse4Mat(m_mesh->m_SkinDesc.pBI[Offset1], m_mesh->m_SkinDesc.pBI[Offset1 + 1], m_mesh->m_SkinDesc.pBI[Offset1 + 2],
				m_mesh->m_SkinDesc.pBI[Offset1 + 3], m_mesh->m_SkinDesc.pW[Offset1], m_mesh->m_SkinDesc.pW[Offset1 + 1], m_mesh->m_SkinDesc.pW[Offset1 + 2],
				m_mesh->m_SkinDesc.pW[Offset1 + 3], &CollapsedMatrices[i * 3]);
		}

		ReOutputVertex5_D3DX9 OV[4];
		Vert4Mat4Normalise(&CollapsedMatrices[0], &m_SSEVD.pX[j], &m_SSEVD.pY[j], &m_SSEVD.pZ[j], &m_SSEVD.pNX[j],
			&m_SSEVD.pNY[j], &m_SSEVD.pNZ[j], OV);

		for (i = 0, j = 0; i < m_remainingVertices; i++, j += 4) {
			OV[i].U0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].x;
			OV[i].V0 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 0].y;
			OV[i].U1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].x;
			OV[i].V1 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 1].y;
			OV[i].U2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].x;
			OV[i].V2 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 2].y;
			OV[i].U3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].x;
			OV[i].V3 = m_mesh->m_Desc.pUv[m_batchedVertices * 4 + j + 3].y;

			*pVB++ = OV[i];
		}

		m_mesh->m_pVB->Unlock();
	}
}

void ReShader_SkinningSSE::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObjectSizeInBytes(&m_pSSEData, m_SSEDataSize);
	}
}

} // namespace KEP
