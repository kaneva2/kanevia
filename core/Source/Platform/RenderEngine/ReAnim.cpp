///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#ifndef WINVER
#define WINVER 0x0501
#endif

#include <afx.h>
#include "ReAnim.h"
#include "Utils/ReUtils.h"
#include "Core/Math/Interpolate.h"

namespace KEP {

ReAnimAnimationD3D::ReAnimAnimationD3D() {
	m_flags = CANIMANIMATIONFLAGS_VERSION;
	m_numNodes = 0;
	m_animLength = 0;
	m_hashCode = 0;
	m_fps = 0;
	m_numKeys = NULL;
	m_nodeMask = NULL;
	m_keyFrames = NULL;
}

ReAnimAnimationD3D::ReAnimAnimationD3D(FLOAT** keys, UINT* numKeys, UINT numNodes, UINT fps, UINT length, UINT flags) {
	m_flags = flags | CANIMANIMATIONFLAGS_VERSION;
	m_numNodes = numNodes;
	m_fps = fps;
	m_animLength = length;
	m_fps = fps;
	m_numKeys = NULL;
	m_nodeMask = NULL;
	m_keyFrames = NULL;

	if (m_numNodes == 0 || length == 0) {
		return;
	}

	m_numKeys = new UINT[m_numNodes];
	m_nodeMask = new BYTE[m_numNodes];

	memset((void*)m_nodeMask, 0xff, m_numNodes);

	memcpy((void*)m_numKeys, (void*)numKeys, numNodes * sizeof(UINT));

	// consolidate input keys for hash
	UINT totalNumfloats = 0;
	for (UINT i = 0; i < m_numNodes; i++) {
		totalNumfloats += m_numKeys[i] * 13;
	}
	FLOAT* hashBlock = new FLOAT[totalNumfloats];
	FLOAT* blockPtr = hashBlock;
	for (UINT i = 0; i < m_numNodes; i++) {
		FLOAT* key = keys[i];

		memcpy(blockPtr, key, m_numKeys[i] * 13 * sizeof(FLOAT));
		blockPtr += m_numKeys[i] * 13;
	}
	// get the hash code
	m_hashCode = ReHash((BYTE*)hashBlock, totalNumfloats * sizeof(FLOAT), 0);

	delete[] hashBlock;

	if (flags & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
		ReAnimKeyFrameMatrixD3D** keyFrames = new ReAnimKeyFrameMatrixD3D*[m_numNodes];

		for (UINT i = 0; i < m_numNodes; i++) {
			keyFrames[i] = new ReAnimKeyFrameMatrixD3D[m_numKeys[i]];
		}

		for (UINT i = 0; i < m_numNodes; i++) {
			FLOAT* key = keys[i];
			UINT numFloats = 0;

			for (UINT j = 0; j < m_numKeys[i]; j++) {
				Vector3f rt;
				Vector3f up;
				Vector3f at;
				Vector3f ps;
				FLOAT time;

				rt.x = key[numFloats++];
				rt.y = key[numFloats++];
				rt.z = key[numFloats++];
				up.x = key[numFloats++];
				up.y = key[numFloats++];
				up.z = key[numFloats++];
				at.x = key[numFloats++];
				at.y = key[numFloats++];
				at.z = key[numFloats++];
				ps.x = key[numFloats++];
				ps.y = key[numFloats++];
				ps.z = key[numFloats++];

				if (j == m_numKeys[i] - 1) {
					time = (FLOAT)length;
					numFloats++;
				} else {
					time = key[numFloats++];
				}

				keyFrames[i][j].m_matrix.MakeIdentity();

				keyFrames[i][j].m_matrix.GetRowX() = rt;
				keyFrames[i][j].m_matrix.GetRowY() = up;
				keyFrames[i][j].m_matrix.GetRowZ() = at;
				keyFrames[i][j].m_matrix.GetRowW() = ps;
				keyFrames[i][j].m_time = time;
			}
		}

		m_keyFrames = (ReAnimKeyFrame**)keyFrames;
	} else if (flags & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
		;
	}
}

ReAnimAnimationD3D::~ReAnimAnimationD3D() {
	if (m_keyFrames) {
		for (UINT i = 0; i < m_numNodes; i++) {
			if (m_keyFrames[i]) {
				delete[] m_keyFrames[i];
				m_keyFrames[i] = NULL;
			}
		}

		delete[] m_keyFrames;

		m_keyFrames = NULL;
	}

	if (m_numKeys) {
		delete m_numKeys;
		m_numKeys = NULL;
	}

	if (m_nodeMask) {
		delete m_nodeMask;
		m_nodeMask = NULL;
	}

	m_numNodes = 0;
}

void ReAnimAnimationD3D::Write(CArchive& ar, BOOL /*compress*/) const {
	ar.Write((void*)&m_flags, ReAnimAnimationD3D::m_headerSize0);

	ar.Write((void*)m_numKeys, m_numNodes * sizeof(UINT));

	ar.Write((void*)m_nodeMask, m_numNodes);

	if (m_flags & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
		for (UINT i = 0; i < m_numNodes; i++) {
			ar.Write((void*)m_keyFrames[i], m_numKeys[i] * sizeof(ReAnimKeyFrameMatrixD3D));
		}
	} else if (m_flags & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
		for (UINT i = 0; i < m_numNodes; i++) {
			ar.Write((void*)m_keyFrames[i], m_numKeys[i] * sizeof(ReAnimKeyFrameQuatD3D));
		}
	}
}

void ReAnimAnimationD3D::Read(CArchive& ar) {
	ar.Read((void*)&m_flags, ReAnimAnimationD3D::m_headerSize0);

	if (m_numNodes == 0) {
		return;
	}

	m_numKeys = new UINT[m_numNodes];

	ar.Read((void*)m_numKeys, m_numNodes * sizeof(UINT));

	m_nodeMask = new BYTE[m_numNodes];

	ar.Read((void*)m_nodeMask, m_numNodes);

	if (m_flags & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
		ReAnimKeyFrameMatrixD3D** keyFrames = new ReAnimKeyFrameMatrixD3D*[m_numNodes];

		for (UINT i = 0; i < m_numNodes; i++) {
			keyFrames[i] = new ReAnimKeyFrameMatrixD3D[m_numKeys[i]];
			ar.Read((void*)keyFrames[i], m_numKeys[i] * sizeof(ReAnimKeyFrameMatrixD3D));
		}
		m_keyFrames = (ReAnimKeyFrame**)keyFrames;
	} else if (m_flags & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
		ReAnimKeyFrameQuatD3D** keyFrames = new ReAnimKeyFrameQuatD3D*[m_numNodes];

		for (UINT i = 0; i < m_numNodes; i++) {
			keyFrames[i] = new ReAnimKeyFrameQuatD3D[m_numKeys[i]];
			ar.Read((void*)keyFrames[i], m_numKeys[i] * sizeof(ReAnimKeyFrameQuatD3D));
		}
		m_keyFrames = (ReAnimKeyFrame**)keyFrames;
	}
}

ReAnimInterpolatorD3D::ReAnimInterpolatorD3D() {
	m_flags = CANIMINTERPOLATORFLAGS_VERSION;
	m_numNodes = 0;
	m_anim = NULL;
	m_time = 0;
	m_animCallback = NULL;
	m_callbackTime = 0.f;
	m_callbackdata = NULL;
	m_loopCallback = NULL;
	m_loopCallbackdata = NULL;
	m_interpMats = NULL;
	m_currFrame = NULL;
	m_parent = NULL;
}

ReAnimInterpolatorD3D::ReAnimInterpolatorD3D(UINT numNodes, UINT flags) {
	m_flags = flags | CANIMINTERPOLATORFLAGS_VERSION;
	m_numNodes = numNodes;
	m_anim = NULL;
	m_time = 0;
	m_animCallback = NULL;
	m_callbackTime = 0.f;
	m_callbackdata = NULL;
	m_loopCallback = NULL;
	m_loopCallbackdata = NULL;
	m_parent = NULL;
	m_interpMats = new Matrix44fA16[numNodes];
	m_currFrame = new UINT[numNodes];
}

ReAnimInterpolatorD3D::ReAnimInterpolatorD3D(ReAnimInterpolator* parent, UINT flags) {
	m_flags = flags | CANIMINTERPOLATORFLAGS_VERSION;
	m_anim = NULL;
	m_time = 0;
	m_animCallback = NULL;
	m_callbackTime = 0.f;
	m_callbackdata = NULL;
	m_loopCallback = NULL;
	m_loopCallbackdata = NULL;
	m_interpMats = NULL;
	m_currFrame = NULL;
	m_parent = NULL;

	if (parent) {
		m_numNodes = ((ReAnimInterpolatorD3D*)parent)->m_numNodes;
		m_interpMats = new Matrix44fA16[m_numNodes];
		m_currFrame = new UINT[m_numNodes];
		m_parent = parent;
	}
}

ReAnimInterpolatorD3D::~ReAnimInterpolatorD3D() {
	if (m_currFrame) {
		delete[] m_currFrame;
		m_currFrame = NULL;
	}

	if (m_interpMats) {
		delete[] m_interpMats;
		m_interpMats = NULL;
	}
}

BOOL ReAnimInterpolatorD3D::SetAnimation(const ReAnimAnimationNew* anim, UINT flags) {
	if (anim->GetNumNodes() != m_numNodes) {
		return FALSE;
	}

	m_anim = (ReAnimAnimationD3D*)anim;

	m_flags |= flags;

	m_time = 0;

	for (UINT i = 0; i < m_numNodes; i++) {
		m_currFrame[i] = 0;
	}

	return TRUE;
}

void ReAnimInterpolatorD3D::AddTime(UINT time) {
	if (m_parent) {
		AddTimeSubinterpolator(time);
		return;
	}

	if (!m_anim) {
		return;
	}

	// increment animation time
	m_time += time;

	// ran past the length of the current animation
	if (m_time >= m_anim->GetLength()) {
		// if not looping, just set animation to end keyframe
		if (!(m_flags & CANIMINTERPOLATORFLAGS_LOOPING)) {
			if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
				ReAnimKeyFrameMatrixD3D** keyFrames = (ReAnimKeyFrameMatrixD3D**)m_anim->GetKeyFrames();
				for (UINT i = 0; i < m_anim->GetNumNodes(); i++) {
					if (m_anim->GetNodeMask(i)) {
						UINT lastFrame = m_anim->GetNumKeys()[i] - 1;
						m_interpMats[i] = ((ReAnimKeyFrameMatrixD3D)keyFrames[i][lastFrame]).m_matrix;
					}
				}
			} else if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
				;
			}
			return;
		}

		// renormalize the elapsed time to within the animation duration
		m_time %= m_anim->GetLength();

		// reset animation frame search
		for (UINT i = 0; i < m_numNodes; i++) {
			m_currFrame[i] = 0;
		}
	}

	// search for next animation frame if ran past it
	if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
		ReAnimKeyFrameMatrixD3D** keyFrames = (ReAnimKeyFrameMatrixD3D**)m_anim->GetKeyFrames();
		for (UINT i = 0; i < m_numNodes; i++) {
			while (m_time >= keyFrames[i][m_currFrame[i]].m_time) {
				m_currFrame[i]++;
			}
		}
	} else if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
		;
	}

	// update the target matrices
	if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
		ReAnimKeyFrameMatrixD3D** keyFrames = (ReAnimKeyFrameMatrixD3D**)m_anim->GetKeyFrames();

		for (UINT i = 0; i < m_numNodes; i++) {
			if (m_anim->GetNodeMask(i)) {
				UINT currFrametime = (UINT)keyFrames[i][m_currFrame[i]].m_time;
				UINT elapsedFrametime = abs((long)(currFrametime - m_time));
				FLOAT interpFactor = 1.f - (FLOAT)elapsedFrametime / currFrametime;
				int currFrame = m_currFrame[i];
				int nextFrame;

				if (m_flags & CANIMINTERPOLATORFLAGS_REVERSE) {
					// next frame, wrap to start if at end
					nextFrame = currFrame - 1 < 0 ? m_anim->GetNumKeys()[i] - 1 : currFrame - 1;
				} else {
					// next frame, wrap to start if at end
					nextFrame = currFrame + 1 >= (int)m_anim->GetNumKeys()[i] ? 0 : currFrame + 1;

					const Matrix44f& matCurr = keyFrames[i][currFrame].m_matrix;
					const Matrix44f& matNext = keyFrames[i][nextFrame].m_matrix;
					Matrix44f& matInterp = m_interpMats[i];
					for (size_t j = 0; j < 4; ++j)
						matInterp[j] = matCurr[j] + interpFactor * (matNext[j] - matCurr[j]);

					if (m_flags & CANIMINTERPOLATORFLAGS_RENORM) {
						matInterp.GetRowX().Normalize();
						matInterp.GetRowY().Normalize();
						matInterp.GetRowZ().Normalize();
					}
				}
			}
		}
	} else if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
		;
	}
}

void ReAnimInterpolatorD3D::AddTimeSubinterpolator(UINT time) {
	if (!m_anim) {
		return;
	}

	// increment animation time
	m_time += time;

	// ran past the length of the current animation
	if (m_time >= m_anim->GetLength()) {
		// if not looping, just set animation to end keyframe
		if (!(m_flags & CANIMINTERPOLATORFLAGS_LOOPING)) {
			if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
				ReAnimKeyFrameMatrixD3D** keyFrames = (ReAnimKeyFrameMatrixD3D**)m_anim->GetKeyFrames();
				for (UINT i = 0; i < m_anim->GetNumNodes(); i++) {
					// use sub-anim node
					if (m_anim->GetNodeMask(i)) {
						UINT lastFrame = m_anim->GetNumKeys()[i] - 1;
						m_interpMats[i] = ((ReAnimKeyFrameMatrixD3D)keyFrames[i][lastFrame]).m_matrix;
					}
					// else use parent anim node
					else {
						m_interpMats[i] = ((ReAnimInterpolatorD3D*)m_parent)->m_interpMats[i];
					}
				}
			} else if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
				;
			}
			return;
		}

		// renormalize the elapsed time to within the animation duration
		m_time %= m_anim->GetLength();

		// reset animation frame search
		for (UINT i = 0; i < m_numNodes; i++) {
			m_currFrame[i] = 0;
		}
	}

	// search for next animation frame if ran past it
	if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
		ReAnimKeyFrameMatrixD3D** keyFrames = (ReAnimKeyFrameMatrixD3D**)m_anim->GetKeyFrames();
		for (UINT i = 0; i < m_numNodes; i++) {
			while (m_time >= keyFrames[i][m_currFrame[i]].m_time) {
				if (m_currFrame[i] == m_anim->GetNumKeys()[i] - 1) {
					break;
				}
				m_currFrame[i]++;
			}
		}
	} else if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
		;
	}

	// update the target matrices
	if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEMATRIX) {
		ReAnimKeyFrameMatrixD3D** keyFrames = (ReAnimKeyFrameMatrixD3D**)m_anim->GetKeyFrames();

		for (UINT i = 0; i < m_numNodes; i++) {
			// use sub-anim node
			if (m_anim->GetNodeMask(i)) {
				UINT currFrametime = (UINT)keyFrames[i][m_currFrame[i]].m_time;
				UINT elapsedFrametime = abs((long)(currFrametime - m_time));
				FLOAT interpFactor = 1.f - (FLOAT)elapsedFrametime / currFrametime;
				int currFrame = m_currFrame[i];
				int nextFrame;

				if (m_flags & CANIMINTERPOLATORFLAGS_REVERSE) {
					// next frame, wrap to start if at end
					nextFrame = currFrame - 1 < 0 ? m_anim->GetNumKeys()[i] - 1 : currFrame - 1;
				} else {
					// next frame, wrap to start if at end
					nextFrame = currFrame + 1 >= (int)m_anim->GetNumKeys()[i] ? 0 : currFrame + 1;

					const Matrix44f& matCurr = keyFrames[i][currFrame].m_matrix;
					const Matrix44f& matNext = keyFrames[i][nextFrame].m_matrix;
					Matrix44f& matInterp = m_interpMats[i];
					for (size_t j = 0; j < 4; ++j)
						matInterp[j] = matCurr[j] + interpFactor * (matNext[j] - matCurr[j]);

					if (m_flags & CANIMINTERPOLATORFLAGS_RENORM) {
						matInterp.GetRowX().Normalize();
						matInterp.GetRowY().Normalize();
						matInterp.GetRowZ().Normalize();
					}
				}
			}
			// else use parent anim node
			else {
				m_interpMats[i] = ((ReAnimInterpolatorD3D*)m_parent)->m_interpMats[i];
			}
		}
	} else if (m_anim->GetFlags() & CANIMANIMATIONFLAGS_KEYFRAMEQUAT) {
		;
	}
}

} // namespace KEP