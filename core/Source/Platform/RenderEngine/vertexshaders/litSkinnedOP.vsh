const int iNumLights		:	register(i0);

/***********	Mat Props	**************
00 Mesh material ambient
01 Mesh material diffuse
02 Mesh material specular
03 Mesh material emissive
******************************************/
float4	AmbientMat			:	register(c0);
float4	DiffuseMat			:	register(c1);
float4	SpecularMat			:	register(c2);
float4	EmissiveMat			:	register(c3);

/********** Transform *********************
04 Object world matrix
05
06
07
*******************************************/
float4x4 world				:	register(c4);

/*******	Light data block **************/
//	1 register for global ambient
//	1 set of 3 constants for directional light
//	4 sets of 4 constants for point lights
/******************************************
08 GLOBAL AMBIENT

09 DIRECTIONAL LIGHT DIRECTION
10 DIRECTIONAL LIGHT DIFF
11 DIRECTIONAL LIGHT SPEC

12 POINTLIGHT0 POSITION
13 POINTLIGHT0 DIFF
14 POINTLIGHT0 SPEC
15 POINTLIGHT0 ATTEN

16 POINTLIGHT1 POSITION
17 POINTLIGHT1 DIFF
18 POINTLIGHT1 SPEC
19 POINTLIGHT1 ATTEN

20 POINTLIGHT2 POSITION
21 POINTLIGHT2 DIFF
22 POINTLIGHT2 SPEC
23 POINTLIGHT2 ATTEN

24 POINTLIGHT3 POSITION
25 POINTLIGHT3 DIFF
26 POINTLIGHT3 SPEC
27 POINTLIGHT3 ATTEN

28 PROJVIEW MATRIX
29
30
31
32 CAM POSITION
33 MISC CONSTANTS

34 57 Bones Mats
********************************************/
float4		GlobalAmbient		:	register(c8);
float4		DirLightDirection	:	register(c9);
float4		DirLightDiffuse		:	register(c10);
float4		DirLightSpecular	:	register(c11);

float4		PointLights[16]		:	register(c12);

float4x4	ViewProj			:	register(c28);
float4		CameraPosition		:	register(c32);
float4		MiscConstants		:	register(c33);

float4		fogRange			:	register(c34);

float4x3	mBoneArray[57]		:	register(c35);

struct vertexInputSkinned
{
    float3 position			: POSITION;
    float3 normal			: NORMAL;
    float2 texCoordDiffuse	: TEXCOORD0;
    
    float4  BlendIndices    : BLENDINDICES;
    float4  BlendWeights    : BLENDWEIGHT;
};

struct vertexOutput 
{
    float4 hPosition		: POSITION;
    float2 BaseTexCoords	: TEXCOORD0;
    float4 Diffuse			: COLOR0;
    float4 Specular			: COLOR1;
    float  Fog				: FOG;
};

float3 Skin( float3 inPos, float4 BlendWeights, float4 BlendIndices )
{	
	float3      result = 0.0f;
	
	//	We always have at least 1 bone
	result += mul(float4(inPos, 1.0f), mBoneArray[BlendIndices.x]) * BlendWeights.x;
	
	//	Skin by up to 3 more bones, if we have a weight for each
	result += mul(float4(inPos, 1.0f), mBoneArray[BlendIndices.y]) * BlendWeights.y;
	result += mul(float4(inPos, 1.0f), mBoneArray[BlendIndices.z]) * BlendWeights.z;
	result += mul(float4(inPos, 1.0f), mBoneArray[BlendIndices.w]) * BlendWeights.w;
		
	return result;
}

float3 SkinNormal( float3 inNorm, float4 BlendWeights, float4 BlendIndices )
{	
	float3      result = 0.0f;
	
	//	We always have at least 1 bone
	result += mul(inNorm, mBoneArray[BlendIndices.x]) * BlendWeights.x;
	
	//	Skin by up to 3 more bones, if we have a weight for each
	result += mul(inNorm, mBoneArray[BlendIndices.y]) * BlendWeights.y;
	result += mul(inNorm, mBoneArray[BlendIndices.z]) * BlendWeights.z;
	result += mul(inNorm, mBoneArray[BlendIndices.w]) * BlendWeights.w;
	
	return result;
}

float4 LightVert( float3 WorldPos, float3 WorldNormal, out float3 specular, out float fog )
{
	float4	finalColor;
	const float3 specularMaterial = float3(1, 1, 1);
	
	specular = float3( 0.0f, 0.0f, 0.0f );

	//	Dir lighting first
	finalColor.xyz = DirLightDiffuse.xyz * max( 0, dot( WorldNormal, normalize( -DirLightDirection.xyz ) ) );
	finalColor.w = DiffuseMat.w;
	
	float	dist;
	int		index	= 0;
	float3	eyeVec	= CameraPosition.xyz - WorldPos;
	
	fog = length(eyeVec);
	
	//	Calculate fog
	fog = ( ( fogRange.y - fog ) / ( fogRange.y - fogRange.x ) );
	
#define POS		PointLights[index]
#define DIFF	PointLights[index + 1]
#define SPEC	PointLights[index + 2]
#define ATTN	PointLights[index + 3]	
	
	for( int i = 0; i < iNumLights; i++ )
	{
		float3	vecToLight	= ( POS.xyz - WorldPos );
	
		float3	halfVec		=	normalize( vecToLight + eyeVec ); 
		
		float3	dir			=	normalize( vecToLight );
		float4	lightVec	=	lit( dot( WorldNormal, dir ), dot( WorldNormal, halfVec ), 1);
	
		//	Add diffuse
		finalColor.xyz		+=  DiffuseMat * DIFF.xyz * lightVec.y * ATTN.w;
	
		//	Add Specular
		specular			+=	SpecularMat * SPEC.xyz * pow( lightVec.z, 16 ) * ATTN.w;
		
		index += 4;
	}
	
	return ( finalColor + ( ( GlobalAmbient + EmissiveMat + AmbientMat ) * DiffuseMat ) );
}

vertexOutput VS_LitSkinned( vertexInputSkinned IN )
{
	vertexOutput OUT;
	float3 specular;
	
	float4      Pos		= float4( 0.0f, 0.0f, 0.0f, 1.0f );
	float3      Normal;
	float		fog;

	Pos.xyz				= Skin( IN.position, IN.BlendWeights, IN.BlendIndices );
	Normal				= SkinNormal( IN.normal, IN.BlendWeights, IN.BlendIndices );
	
	Normal				= mul( Normal, (float3x3)world );
	Pos					= mul( Pos, world );
	
	OUT.Diffuse			= LightVert( Pos, Normal, specular, fog );
	//OUT.Diffuse			= float4( 1.0f, 1.0f, 1.0f, 1.0f );
	OUT.Specular		= float4( specular, 1.0f);
	OUT.Fog				= fog;
	OUT.hPosition		= mul( Pos, ViewProj );
    OUT.BaseTexCoords	= IN.texCoordDiffuse;
    
    return OUT;
}

