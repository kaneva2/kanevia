; VERSION.
vs.2.0

;------------------------------------------------------------------------------
; v0 = position
; v1 = bone indices
; v2 = bone weights
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; r0.w = Last blend weight
; r1 = Blend indices
; r2 = Temp position
; r3 = Temp normal
; r4 = Blended position in camera space
; r5 = Blended normal in camera space
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Constants specified by the app;
;
; c0-c3   = view-projection matrix
; c4	  = {1.0f,4.0f,0.0f,0.5f}
; c5	  = light direction vector
; c6-c125 = world-view matrix palette
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; oPos	  = Output position
; oD0	  = Diffuse
;------------------------------------------------------------------------------

dcl_position v0;
dcl_blendindices v1;
dcl_blendweight v2;

; 1 and only bone.
mova a0.x, v1.x
m4x3 r0.xyz, v0, c[a0.x+6]

; view-projection.
mov r0.w, c4.x
m4x4 oPos,r0,c0

mov oD0, c4.w
