const int iNumLights		:	register(i0);

/***********	Mat Props	**************
00 Mesh material ambient
01 Mesh material diffuse
02 Mesh material specular
03 Mesh material emissive
******************************************/
float4	AmbientMat			:	register(c0);
float4	DiffuseMat			:	register(c1);
float4	SpecularMat			:	register(c2);
float4	EmissiveMat			:	register(c3);

/********** Transform *********************
04 Object world matrix
05
06
07
*******************************************/
float4x4 world				:	register(c4);

/*******	Light data block **************/
//	1 register for global ambient
//	1 set of 3 constants for directional light
//	4 sets of 4 constants for point lights
/******************************************
08 GLOBAL AMBIENT

09 DIRECTIONAL LIGHT DIRECTION
10 DIRECTIONAL LIGHT DIFF
11 DIRECTIONAL LIGHT SPEC

12 POINTLIGHT0 POSITION
13 POINTLIGHT0 DIFF
14 POINTLIGHT0 SPEC
15 POINTLIGHT0 ATTEN

16 POINTLIGHT1 POSITION
17 POINTLIGHT1 DIFF
18 POINTLIGHT1 SPEC
19 POINTLIGHT1 ATTEN

20 POINTLIGHT2 POSITION
21 POINTLIGHT2 DIFF
22 POINTLIGHT2 SPEC
23 POINTLIGHT2 ATTEN

24 POINTLIGHT3 POSITION
25 POINTLIGHT3 DIFF
26 POINTLIGHT3 SPEC
27 POINTLIGHT3 ATTEN

28 PROJVIEW MATRIX
29
30
31
32 CAM POSITION
33 MISC CONSTANTS
********************************************/
float4		GlobalAmbient		:	register(c8);
float4		DirLightDirection	:	register(c9);
float4		DirLightDiffuse		:	register(c10);
float4		DirLightSpecular	:	register(c11);

float4		PointLights[16]		:	register(c12);

float4x4	ViewProj			:	register(c28);
float4		CameraPosition		:	register(c32);
float4		MiscConstants		:	register(c33);

float4		fogRange			:	register(c34);

struct vertexInput 
{
    float3 position			: POSITION;
    float3 normal			: NORMAL;
    float2 texCoordDiffuse	: TEXCOORD0;
};

struct vertexOutput 
{
    float4 hPosition		: POSITION;
    float2 BaseTexCoords	: TEXCOORD0;
    float4 Diffuse			: COLOR0;
    float4 Specular			: COLOR1;
    float  Fog				: FOG;
};

float4 LightVert( float3 WorldPos, float3 WorldNormal, out float3 specular, out float fog )
{
	float4	finalColor;
	const float3 specularMaterial = float3(1, 1, 1);
	
	specular	= float3( 0.0f, 0.0f, 0.0f );

	//	Dir lighting first
	//finalColor.xyz = DirLightDiffuse.xyz * max( 0, dot( WorldNormal, normalize( -DirLightDirection.xyz ) ) );
	finalColor.xyz = DiffuseMat * DirLightDiffuse.xyz * max( 0, dot( WorldNormal, normalize( -DirLightDirection.xyz ) ) );
	finalColor.w = DiffuseMat.w;
	
	float	atten;
	float	dist;
	float	rangeMod;
	int		index = 0;
	float3	eyeVec		=	CameraPosition.xyz - WorldPos;
	
	fog = length(eyeVec);
	
	//	Calculate fog
	fog = ( ( fogRange.y - fog ) / ( fogRange.y - fogRange.x ) );

#define POS		PointLights[index]
#define DIFF	PointLights[index + 1]
#define SPEC	PointLights[index + 2]
#define ATTN	PointLights[index + 3]	
	
	for(int i = 0; i < iNumLights; i++)
	{
		float3	vecToLight	= ( POS.xyz - WorldPos );
	
		float3	halfVec		=	normalize( vecToLight + eyeVec ); 
		
		dist				=	length( vecToLight );
		
		//	 0 if out of range, 1 if in range
		rangeMod			=	max( 0, sign( POS.w - dist ) );
		
		//	1 / ( a + ( b * d ) + ( c * ( d ^ 2) ) )
		//	Where abc are atten coefficients and d = dist to light from vert
		//	Where abcw  are xyzw of attn constant of the light
		atten				=	1.0f / ( 1.0f + ( dist * ATTN.y ) + ( pow( dist, 2) * ATTN.z ) );
		
		float3	dir			=	normalize( vecToLight );
		
		float4	lightVec	=	lit( dot( WorldNormal, dir ), dot( WorldNormal, halfVec ), 1);
	
		//	Add diffuse
		finalColor.xyz		+=	DiffuseMat * DIFF.xyz * lightVec.y * atten * rangeMod;
	
		//	Add Specular
		specular			+=	SpecularMat * SPEC.xyz * pow( lightVec.z, 16 ) * atten * rangeMod;
		
		index += 4;
	}
	
	return ( finalColor + ( ( GlobalAmbient + EmissiveMat + AmbientMat ) * DiffuseMat ) );
}

vertexOutput VS_Lit( vertexInput IN )
{
	vertexOutput OUT;
	float3 specular;
	float  fog;

	float4      Pos		= mul( float4( IN.position, 1.0f), world );
	float3		Normal	= mul( IN.normal, (float3x3)world );
	
	OUT.Diffuse			= LightVert( Pos, Normal, specular, fog );
	OUT.Specular		= float4( specular, 0.0f);
	OUT.Fog				= fog; 
	OUT.hPosition		= mul( Pos , ViewProj );
    OUT.BaseTexCoords	= IN.texCoordDiffuse;
    
    return OUT;
}