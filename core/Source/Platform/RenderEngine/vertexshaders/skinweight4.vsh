; VERSION.
vs.2.0

;------------------------------------------------------------------------------
; v0 = position
; v1 = normal
; v2 = texcoord
; v3 = bone indices
; v4 = bone weights
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; r0.w = Last blend weight
; r1 = Blend indices
; r2 = Temp position
; r3 = Temp normal
; r4 = Blended position in camera space
; r5 = Blended normal in camera space
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Constants specified by the app;
;
; c9-c95 = world-view matrix palette
; c8	  = diffuse * light.diffuse
; c7	  = ambient color
; c2-c5   = projection matrix
; c1	  = light direction
; c0	  = {1, power, 0.5, 1020.01};
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; oPos	  = Output position
; oD0	  = Diffuse
; oD1	  = Specular
; oT0	  = Texture coord 0
;------------------------------------------------------------------------------

// vertex decl
dcl_position v0;
dcl_normal v1;
dcl_texcoord0 v2;
dcl_blendindices v3;
dcl_blendweight v4;


// 1st bone.
mova a0.x, v3.x
m4x3 r0.xyz, v0, c[a0.x+9]
m3x3 r1.xyz, v1, c[a0.x+9]
mul r0.xyz, r0.xyz, v4.x
mul r1.xyz, r1.xyz, v4.x

// 2nd bone.
mova a0.x, v3.y
m4x3 r2.xyz, v0, c[a0.x+9]
m3x3 r3.xyz, v1, c[a0.x+9]
mad r0.xyz, r2.xyz, v4.y, r0.xyz 
mad r1.xyz, r3.xyz, v4.y, r1.xyz

// 3rd bone.
mova a0.x, v3.z
m4x3 r2.xyz, v0, c[a0.x+9]
m3x3 r3.xyz, v1, c[a0.x+9]
mad r0.xyz, r2.xyz, v4.z, r0.xyz 
mad r1.xyz, r3.xyz, v4.z, r1.xyz

// 4th bone.
mova a0.x, v3.w
m4x3 r2.xyz, v0, c[a0.x+9]
m3x3 r3.xyz, v1, c[a0.x+9]
mad r0.xyz, r2.xyz, v4.w, r0.xyz 
mad r1.xyz, r3.xyz, v4.w, r1.xyz

// view-projection.
mov r0.w, c0.x
m4x4 oPos,r0,c2

// normalise the normal.
dp3 r1.w, r1, r1
rsq r1.w, r1.w
mul r1, r1, r1.w

// Do the lighting calculation
dp3 r5.x, r1, c1	; normal dot light
lit r5, r5
mul r0, r5.y, c8	; Multiply with diffuse
add r0, r0, c7		; Add in ambient
min oD0, r0, c0.x	; clamp if > 1
mov oD1, c0.zzzz	; output specular

// copy texcoords
mov oT0, v2
