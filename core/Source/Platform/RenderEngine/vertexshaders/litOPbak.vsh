const int iNumLights		:	register(i0);

/***********	Mat Props	**************
00 Mesh material ambient
01 Mesh material diffuse
02 Mesh material specular
******************************************/
float4	AmbientMat			:	register(c0);
float4	DiffuseMat			:	register(c1);
float4	SpecularMat			:	register(c2);

/********** Transform *********************
03 Object world matrix
04
05
06
*******************************************/
float4x4 world				:	register(c3);

/*******	Light data block **************/
//	1 register for global ambient
//	1 set of 3 constants for directional light
//	4 sets of 4 constants for point lights
/******************************************
07 GLOBAL AMBIENT

08 DIRECTIONAL LIGHT DIRECTION
09 DIRECTIONAL LIGHT DIFF
10 DIRECTIONAL LIGHT SPEC

11 POINTLIGHT0 POSITION
12 POINTLIGHT0 DIFF
13 POINTLIGHT0 SPEC
14 POINTLIGHT0 ATTEN

15 POINTLIGHT1 POSITION
16 POINTLIGHT1 DIFF
17 POINTLIGHT1 SPEC
18 POINTLIGHT1 ATTEN

19 POINTLIGHT2 POSITION
20 POINTLIGHT2 DIFF
21 POINTLIGHT2 SPEC
22 POINTLIGHT2 ATTEN

23 POINTLIGHT3 POSITION
24 POINTLIGHT3 DIFF
25 POINTLIGHT3 SPEC
26 POINTLIGHT3 ATTEN

27 PROJVIEW MATRIX
28
29
30
31 CAM POSITION
32 MISC CONSTANTS
********************************************/
float4	GlobalAmbient		:	register(c7);
float4	DirLightDirection	:	register(c8);
float4	DirLightDiffuse		:	register(c9);
float4	DirLightSpecular	:	register(c10);

float4	PointLights[16]		:	register(c11);

float4x4 worldViewProj		:	register(c27);
float4	CameraPosition		:	register(c31);
float4	MiscConstants		:	register(c32);

struct vertexInput 
{
    float3 position			: POSITION;
    float3 normal			: NORMAL;
    float2 texCoordDiffuse	: TEXCOORD0;
};

struct vertexOutput 
{
    float4 hPosition		: POSITION;
    float2 BaseTexCoords	: TEXCOORD0;
    float4 Diffuse			: COLOR0;
    float4 Specular			: COLOR1;
};

float4 LightVert( float3 WorldPos, float3 WorldNormal )
{
	float4	finalColor;

	//	Dir lighting first
	//finalColor = DiffuseMat * float4( ( DirLightDiffuse.xyz * max( 0, dot( WorldNormal, normalize( -DirLight0.xyz ) ) ) ), 1.0f );
	finalColor.xyz = DiffuseMat * DirLightDiffuse.xyz * max( 0, dot( WorldNormal, normalize( -DirLightDirection.xyz ) ) );
	finalColor.w = DiffuseMat.w;
	
	float	atten;
	float	dist;
	float	rangeMod;
	int		index = 0;
	
#define POS		PointLights[index]
#define DIFF	PointLights[index + 1]
#define SPEC	PointLights[index + 2]
#define ATTN	PointLights[index + 3]	
	
	for(int i = 0; i < iNumLights; i++)
	{
		float3	vecToLight	= ( POS.xyz - WorldPos );
	
		float3	eyeVec		= normalize( CameraPosition.xyz - WorldPos );
		float3	halfVec		= normalize( vecToLight + eyeVec ); 
		
		dist				= length( vecToLight );
		
		//	 0 if out of range, 1 if in range
		rangeMod			= max(0, sign(ATTN.w - dist) );
		
		//	1 / ( a + ( b * d ) + ( c * ( d ^ 2) ) )
		//	Where abc are atten coefficients and d = dist to light from vert
		//	Where abcw  are xyzw of attn constant of the light
		atten				= 1.0f / ( ATTN.x + ( dist * ATTN.y ) + ( pow( dist, 2) * ATTN.z ) );
		
		float3	dir			= normalize( vecToLight );
		
		float4	lightVec = lit( dot( WorldNormal, dir ), dot( WorldNormal, halfVec ), 2 );
	
		//	Add diffuse
		finalColor.xyz += DiffuseMat * DIFF.xyz * lightVec.y * atten * rangeMod;
	
		//	Add Specular
		finalColor.xyz += SpecularMat * SPEC.xyz * lightVec.z * atten * rangeMod;
		
		index++;
	}
	
	return ( finalColor + AmbientMat );
}

vertexOutput VS_Lit( vertexInput IN )
{
	vertexOutput OUT;

	float4      Pos		= mul( float4( IN.position, 1.0f), world );
	float3		Normal	= mul( IN.normal, (float3x3)world );
	
	//OUT.Diffuse			= LightVert( Pos, Normal );
	
	OUT.Diffuse			= float4( 1.0f, 1.0f, 1.0f, 1.0f);
	OUT.Specular		= float4( 0.0f, 0.0f, 0.0f, 0.0f);
	OUT.hPosition		= mul( Pos , worldViewProj );
    OUT.BaseTexCoords	= IN.texCoordDiffuse;
    
    return OUT;
}

