///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReD3DX9SkinMesh.h"
#include "ReShader_SkinningC.h"
#include "Common\include\IMemSizeGadget.h"

namespace KEP {

BOOL ReShader_SkinningC::Update(const ReShaderData* /*desc*/) {
	if (m_mesh->GetBones()) {
		UpdateSkinGeneric();
	}
	return TRUE;
}
__forceinline void ReShader_SkinningC::UpdateSkinGeneric() {
	switch (m_mesh->m_Desc.NumUV) {
		case 0:
		case 1:
			UpdateSkinGenericSingleTex();
			break;

		case 2:
			UpdateSkinGenericMultiTex0();
			break;

		case 3:
			UpdateSkinGenericMultiTex1();
			break;

		case 4:
			UpdateSkinGenericMultiTex2();
			break;
	}
}

__forceinline void ReShader_SkinningC::UpdateSkinGenericSingleTex() {
	Vector3f Position, Normal, Temp1, Temp2;

	ReOutputVertex0_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0; i < m_mesh->m_Desc.NumV; i++) {
		Position.x = Position.y = Position.z = 0.0f;
		Normal.x = Normal.y = Normal.z = 0.0f;

		for (UINT j = 0; j < m_mesh->m_SkinDesc.Wpv; j++) {
			float Weight = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + j];
			BYTE BoneIndex = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + j];

			Vec3TransformCoord(&Temp1, &m_mesh->m_Desc.pP[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Position, &Position, &Temp2);

			Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Temp1, &Normal, &Temp2);
			Vec3Normalize(&Normal, &Temp1);
		}

		pVB[i].X = Position.x;
		pVB[i].Y = Position.y;
		pVB[i].Z = Position.z;

		pVB[i].NX = Normal.x;
		pVB[i].NY = Normal.y;
		pVB[i].NZ = Normal.z;

		pVB[i].U = m_mesh->m_Desc.pUv[i].x;
		pVB[i].V = m_mesh->m_Desc.pUv[i].y;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::UpdateSkinGenericMultiTex0() {
	Vector3f Position, Normal, Temp1, Temp2;

	ReOutputVertex3_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex3_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex3_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0, j = 0; i < m_mesh->m_Desc.NumV; i++, j += 2) {
		Position.x = Position.y = Position.z = 0.0f;
		Normal.x = Normal.y = Normal.z = 0.0f;

		for (UINT k = 0; k < m_mesh->m_SkinDesc.Wpv; k++) {
			float Weight = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + k];
			BYTE BoneIndex = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + k];

			Vec3TransformCoord(&Temp1, &m_mesh->m_Desc.pP[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Position, &Position, &Temp2);

			Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Temp1, &Normal, &Temp2);
			Vec3Normalize(&Normal, &Temp1);
		}

		pVB[i].X = Position.x;
		pVB[i].Y = Position.y;
		pVB[i].Z = Position.z;

		pVB[i].NX = Normal.x;
		pVB[i].NY = Normal.y;
		pVB[i].NZ = Normal.z;

		pVB[i].U0 = m_mesh->m_Desc.pUv[j].x;
		pVB[i].V0 = m_mesh->m_Desc.pUv[j].y;
		pVB[i].U1 = m_mesh->m_Desc.pUv[j + 1].x;
		pVB[i].V1 = m_mesh->m_Desc.pUv[j + 1].y;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::UpdateSkinGenericMultiTex1() {
	Vector3f Position, Normal, Temp1, Temp2;

	ReOutputVertex4_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex4_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex4_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0, j = 0; i < m_mesh->m_Desc.NumV; i++, j += 3) {
		Position.x = Position.y = Position.z = 0.0f;
		Normal.x = Normal.y = Normal.z = 0.0f;

		for (UINT k = 0; k < m_mesh->m_SkinDesc.Wpv; k++) {
			float Weight = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + k];
			BYTE BoneIndex = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + k];

			Vec3TransformCoord(&Temp1, &m_mesh->m_Desc.pP[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Position, &Position, &Temp2);

			Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Temp1, &Normal, &Temp2);
			Vec3Normalize(&Normal, &Temp1);
		}

		pVB[i].X = Position.x;
		pVB[i].Y = Position.y;
		pVB[i].Z = Position.z;

		pVB[i].NX = Normal.x;
		pVB[i].NY = Normal.y;
		pVB[i].NZ = Normal.z;

		pVB[i].U0 = m_mesh->m_Desc.pUv[j].x;
		pVB[i].V0 = m_mesh->m_Desc.pUv[j].y;
		pVB[i].U1 = m_mesh->m_Desc.pUv[j + 1].x;
		pVB[i].V1 = m_mesh->m_Desc.pUv[j + 1].y;
		pVB[i].U2 = m_mesh->m_Desc.pUv[j + 2].x;
		pVB[i].V2 = m_mesh->m_Desc.pUv[j + 2].y;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::UpdateSkinGenericMultiTex2() {
	Vector3f Position, Normal, Temp1, Temp2;

	ReOutputVertex5_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex5_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex5_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0, j = 0; i < m_mesh->m_Desc.NumV; i++, j += 4) {
		Position.x = Position.y = Position.z = 0.0f;
		Normal.x = Normal.y = Normal.z = 0.0f;

		for (UINT k = 0; k < m_mesh->m_SkinDesc.Wpv; k++) {
			float Weight = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + k];
			BYTE BoneIndex = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + k];

			Vec3TransformCoord(&Temp1, &m_mesh->m_Desc.pP[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Position, &Position, &Temp2);

			Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &m_mesh->GetBones()[BoneIndex]);
			Vec3Scale(&Temp2, &Temp1, Weight);
			Vec3Add(&Temp1, &Normal, &Temp2);
			Vec3Normalize(&Normal, &Temp1);
		}

		pVB[i].X = Position.x;
		pVB[i].Y = Position.y;
		pVB[i].Z = Position.z;

		pVB[i].NX = Normal.x;
		pVB[i].NY = Normal.y;
		pVB[i].NZ = Normal.z;

		pVB[i].U0 = m_mesh->m_Desc.pUv[j].x;
		pVB[i].V0 = m_mesh->m_Desc.pUv[j].y;
		pVB[i].U1 = m_mesh->m_Desc.pUv[j + 1].x;
		pVB[i].V1 = m_mesh->m_Desc.pUv[j + 1].y;
		pVB[i].U2 = m_mesh->m_Desc.pUv[j + 2].x;
		pVB[i].V2 = m_mesh->m_Desc.pUv[j + 2].y;
		pVB[i].U3 = m_mesh->m_Desc.pUv[j + 3].x;
		pVB[i].V3 = m_mesh->m_Desc.pUv[j + 3].y;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::UpdateSkin1Weight() {
	Vector3f Position, Normal, Temp1;

	ReOutputVertex0_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0; i < m_mesh->m_Desc.NumV; i++) {
		float Weight = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv];
		BYTE BoneIndex = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv];

		Vec3TransformCoord(&Temp1, &m_mesh->m_Desc.pP[i], &m_mesh->GetBones()[BoneIndex]);
		Vec3Scale(&Position, &Temp1, Weight);

		Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &m_mesh->GetBones()[BoneIndex]);
		Vec3Scale(&Normal, &Temp1, Weight);

		pVB->X = Position.x;
		pVB->Y = Position.y;
		pVB->Z = Position.z;

		pVB->NX = Normal.x;
		pVB->NY = Normal.y;
		pVB->NZ = Normal.z;

		pVB++;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::UpdateSkin2Weight() {
	Matrix44f Mat[2];
	float Weight[2];
	BYTE BoneIndex[2];
	Vector3f Position, Normal, Temp1;

	ReOutputVertex0_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0; i < m_mesh->m_Desc.NumV; i++) {
		Weight[0] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv];
		BoneIndex[0] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv];
		Weight[1] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + 1];
		BoneIndex[1] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + 1];

		Mat4Scale(&Mat[0], &m_mesh->GetBones()[BoneIndex[0]], Weight[0]);
		Mat4Scale(&Mat[1], &m_mesh->GetBones()[BoneIndex[1]], Weight[1]);

		Mat4Add(&Mat[0], &Mat[0], &Mat[1]);

		Vec3TransformCoord(&Position, &m_mesh->m_Desc.pP[i], &Mat[0]);
		Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &Mat[0]);
		Vec3Normalize(&Normal, &Temp1);

		pVB->X = Position.x;
		pVB->Y = Position.y;
		pVB->Z = Position.z;

		pVB->NX = Normal.x;
		pVB->NY = Normal.y;
		pVB->NZ = Normal.z;

		pVB++;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::UpdateSkin3Weight() {
	Matrix44f Mat[3];
	float Weight[3];
	BYTE BoneIndex[3];
	Vector3f Position, Normal, Temp1;

	ReOutputVertex0_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0; i < m_mesh->m_Desc.NumV; i++) {
		Weight[0] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv];
		BoneIndex[0] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv];
		Weight[1] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + 1];
		BoneIndex[1] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + 1];
		Weight[2] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + 2];
		BoneIndex[2] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + 2];

		Mat4Scale(&Mat[0], &m_mesh->GetBones()[BoneIndex[0]], Weight[0]);
		Mat4Scale(&Mat[1], &m_mesh->GetBones()[BoneIndex[1]], Weight[1]);
		Mat4Scale(&Mat[2], &m_mesh->GetBones()[BoneIndex[2]], Weight[2]);

		Mat4Add(&Mat[0], &Mat[0], &Mat[1]);
		Mat4Add(&Mat[0], &Mat[0], &Mat[2]);

		Vec3TransformCoord(&Position, &m_mesh->m_Desc.pP[i], &Mat[0]);
		Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &Mat[0]);
		Vec3Normalize(&Normal, &Temp1);

		pVB->X = Position.x;
		pVB->Y = Position.y;
		pVB->Z = Position.z;

		pVB->NX = Normal.x;
		pVB->NY = Normal.y;
		pVB->NZ = Normal.z;

		pVB++;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::UpdateSkin4Weight() {
	Matrix44f Mat[4];
	float Weight[4];
	BYTE BoneIndex[4];
	Vector3f Position, Normal, Temp1;

	ReOutputVertex0_D3DX9* pVB;
	if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, D3DLOCK_DISCARD) != D3D_OK) {
		if (m_mesh->m_pVB->Lock(0, m_mesh->m_Desc.NumV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, 0) != D3D_OK) {
			return;
		}
	}

	for (UINT i = 0; i < m_mesh->m_Desc.NumV; i++) {
		Weight[0] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv];
		BoneIndex[0] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv];
		Weight[1] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + 1];
		BoneIndex[1] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + 1];
		Weight[2] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + 2];
		BoneIndex[2] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + 2];
		Weight[3] = m_mesh->m_SkinDesc.pW[i * m_mesh->m_SkinDesc.Wpv + 3];
		BoneIndex[3] = m_mesh->m_SkinDesc.pBI[i * m_mesh->m_SkinDesc.Wpv + 3];

		Mat4Scale(&Mat[0], &m_mesh->GetBones()[BoneIndex[0]], Weight[0]);
		Mat4Scale(&Mat[1], &m_mesh->GetBones()[BoneIndex[1]], Weight[1]);
		Mat4Scale(&Mat[2], &m_mesh->GetBones()[BoneIndex[2]], Weight[2]);
		Mat4Scale(&Mat[3], &m_mesh->GetBones()[BoneIndex[3]], Weight[3]);

		Mat4Add(&Mat[0], &Mat[0], &Mat[1]);
		Mat4Add(&Mat[0], &Mat[0], &Mat[2]);
		Mat4Add(&Mat[0], &Mat[0], &Mat[3]);

		Vec3TransformCoord(&Position, &m_mesh->m_Desc.pP[i], &Mat[0]);
		Vec3TransformNormal(&Temp1, &m_mesh->m_Desc.pN[i], &Mat[0]);
		Vec3Normalize(&Normal, &Temp1);

		pVB->X = Position.x;
		pVB->Y = Position.y;
		pVB->Z = Position.z;

		pVB->NX = Normal.x;
		pVB->NY = Normal.y;
		pVB->NZ = Normal.z;

		pVB++;
	}

	m_mesh->m_pVB->Unlock();
}

__forceinline void ReShader_SkinningC::Mat4Scale(Matrix44f* pOut, const Matrix44f* pIn, float Scale) {
	*pOut = Scale * *pIn;
}

__forceinline void ReShader_SkinningC::Mat4Add(Matrix44f* pOut, const Matrix44f* pInA, const Matrix44f* pInB) {
	*pOut = *pInA + *pInB;
}

__forceinline void ReShader_SkinningC::Vec3TransformCoord(Vector3f* pOut, const Vector3f* pIn, const Matrix44f* pMat) {
	*pOut = TransformPoint_NonPerspective(*pMat, *pIn);
}

__forceinline void ReShader_SkinningC::Vec3Add(Vector3f* pOut, const Vector3f* pInA, const Vector3f* pInB) {
	*pOut = *pInA + *pInB;
}

__forceinline void ReShader_SkinningC::Vec3TransformNormal(Vector3f* pOut, const Vector3f* pIn, const Matrix44f* pMat) {
	*pOut = TransformVector(*pMat, *pIn);
}

__forceinline void ReShader_SkinningC::Vec3Scale(Vector3f* pOut, const Vector3f* pIn, float Scale) {
	*pOut = *pIn * Scale;
}

__forceinline void ReShader_SkinningC::Vec3Normalize(Vector3f* pOut, const Vector3f* pIn) {
	*pOut = pIn->GetNormalized();
}

void ReShader_SkinningC::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_mesh);
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	-------------------------------------------------------------------------------*/
}

} // namespace KEP