///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "d3dx9math.h"
#include "Core/Math/KEPMath.h"

class CArchive;

namespace KEP {

/*!
 * \brief
 * Overloadable pure virtual base class for keyframe data
 * 
 * The classes derived from the pure virtual base class are 
 * NOT intended as base classes for the sake of compactness.  
 * Any keyframe data configuration can be used and it will 
 * always be compatible with the CanimAnimation and ReAnimInterpolator 
 * presented below as long as it is derived from the CanimKeyframe.
 */
class ReAnimKeyFrame {
public:
	ReAnimKeyFrame() { ; }
	virtual ~ReAnimKeyFrame() = 0 { ; }
};

/*!
 * \brief
 * Platform-independent keyframe structure.
 *
 * Keyframe structure used for streaming keyframes
 * due to its compact size.  The keyframe short
 * data will typically expanded to float after
 * streaming.
 */
class ReAnimKeyFrameShort : public ReAnimKeyFrame {
public:
	SHORT m_up[3];
	SHORT m_at[3];
	SHORT m_pos[3];
	USHORT m_time;
	ReAnimKeyFrameShort() { ; }
	~ReAnimKeyFrameShort() { ; }
};

/*!
 * \brief
 * Platform-independent keyframe structure.
 * 
 * Keyframe structure used for streaming and
 * runtime.  The third orientation vector is
 * obtained by cross product of the "up" and "at".
 */
class ReAnimKeyFrameFloat : public ReAnimKeyFrame {
public:
	FLOAT m_up[3];
	FLOAT m_at[3];
	FLOAT m_pos[3];
	FLOAT m_time;

	ReAnimKeyFrameFloat() { ; }
	~ReAnimKeyFrameFloat() { ; }
};

/*!
 * \brief
 * D3D-specific keyframe structure.
 * 
 * D3D-specific keyframe structure used for runtime.
 * The matrix data will typically be linearly
 * interpolated and renormalized.
 * The keyframe matrix data is 16-byte aligned.
 */
class ReAnimKeyFrameMatrixD3D : public ReAnimKeyFrame {
public:
	static void* operator new[](size_t size) {
		void* p = _aligned_malloc(size, 16);
		if (p)
			return p;
		else
			throw std::bad_alloc();
	}

	static void operator delete[](void* p) {
		_aligned_free(p);
	}

	Matrix44fA16 m_matrix;
	FLOAT m_time;

	ReAnimKeyFrameMatrixD3D() { ; }
	~ReAnimKeyFrameMatrixD3D() { ; }
};

/*!
 * \brief
 * D3D-specific keyframe structure.
 * 
 * D3D-specific keyframe structure used for runtime.
 * The keyframe matrix data is a D3D quaternion
 * which will typically be SLERPed for interpolation,
 * then converted to matrix form.
 */
class ReAnimKeyFrameQuatD3D : public ReAnimKeyFrame {
public:
	D3DXQUATERNION m_quat;
	Vector3f m_pos;
	FLOAT m_time;

	ReAnimKeyFrameQuatD3D() { ; }
	~ReAnimKeyFrameQuatD3D() { ; }
};

#define CANIMANIMATIONFLAGS_VERSION 0x00000000
#define CANIMANIMATIONFLAGS_KEYFRAMEMATRIX 0x00000010
#define CANIMANIMATIONFLAGS_KEYFRAMEQUAT 0x00000020
#define CANIMANIMATIONFLAGS_KEYFRAMESHORT 0x00000040

/*!
 * \brief
 * Pure virtual animation class.
 *
 * ReAnimAnimation serves to contain an array of nodes (i.e. bones), 
 * where each node contains an array of key frames.  The nodes 
 * correspond to the bones of the animated skeleton, and the keyframes 
 * for each bone are interpolated to provide smooth animation.  
 * The pure virtual CanimAnimation contains an array of CanimKeyFrame.  
 * Quick comparison of animation data is provided by hashing the keyframe 
 * data and storing it in m_hashCode (UINT64).
 *
 * Sub-hierarchical animation support is provided through the SetNodeMask() 
 * function.  This accepts an array of bytes equal in number to the number of 
 * nodes, where 1 indicates the node is used in animating a bone, and 0 indicates 
 * this node should be ignored.  Thus the arm bones of a skeleton can be animated 
 * while every other bone is ignored.
 *
 * Standard access functions include GetNumKeys() which returns an array of 
 * integers describing the number of keyframes per animation node; GetHashCode() 
 * to access the 64-bit hash code; GetKeyFrames() which returns the array of CanimKeyFrame; 
 * GetFlags() to return the creation and version flags.
 *
 * A standard generic constructor is provided that uses C-language references only, i.e. 
 * ReAnimAnimationD3D(FLOAT** keys, UINT numNodes, UINT* numKeys, UINT flags).
 * Here "numKeys" is an array of integers of size "numNodes" describing the number of keyframes
 * per node.
 * Here "mat" is an array of size "numNodes', where each entry of this array points to 
 * an array of keyframe data whose number is given by the "numNodes" array.  Here each keyframe
 * in this array consists of 13 floats with the following format:
 * Right vector x, y, z;
 * Up vector x, y , z;
 * At vector x, y, z;
 * Position vector x, y, z;
 * Time.
 * The "flags" argument consists of one of the following:
 * CANIMANIMATIONFLAGS_KEYFRAMEMATRIX;
 * CANIMANIMATIONFLAGS_KEYFRAMEQUAT;
 * CANIMANIMATIONFLAGS_KEYFRAMESHORT.
 * These describe the underlying type of keyframe data which determines how the
 * keyframe data will be interpolated.
 */
class ReAnimAnimationNew {
public:
	/*!
	 * \brief
	 * Default constructor.
	 */
	ReAnimAnimationNew();
	/*!
	 * \brief
	 * Constructor.
	 *
	 * \param keys
	 * "keys" is an array of size "numNodes', where each entry of this array points to 
	 * an array of keyframe data whose number is given by the "numNodes" array.  Here each keyframe
	 * in this array consists of 13 floats with the following format:
	 * Right vector x, y, z;
	 * Up vector x, y , z;
	 * At vector x, y, z;
	 * Position vector x, y, z;
	 * Time.
	 * 
	 * \param numNodes
	 * "numNodes" is the number of animation nodes (or "bones).
	 *
	 * \param numKeys
	 * "numKeys" is an array of integers of size "numNodes" describing the number of keyframes
	 * per node.
	 *
	 * \flags
	 * "flags" argument consists of one of the following:
	 * CANIMANIMATIONFLAGS_KEYFRAMEMATRIX;
	 * CANIMANIMATIONFLAGS_KEYFRAMEQUAT;
	 * CANIMANIMATIONFLAGS_KEYFRAMESHORT.
	 * These describe the underlying type of keyframe data which determines how the
	 * keyframe data will be interpolated.
	 */
	ReAnimAnimationNew(FLOAT** keys, UINT numNodes, UINT* numKeys, UINT flags);
	virtual ~ReAnimAnimationNew();
	/*!
	 * \brief
	 * Streaming function
	 *
	 * \param ar
	 * "ar" is the CArchive to which the animation is streamed.
	 * 
	 * \param compress
	 * "compress" is TRUE when keyframe data compression and keyframe reduction are applied
	 */
	virtual void Write(CArchive& ar, BOOL compress = FALSE) const = 0;
	/*!
	 * \brief
	 * Streaming function
	 *
	 * \param ar
	 * "ar" is the CArchive from which the animation is streamed.
	 */
	virtual void Read(CArchive& ar) = 0;
	virtual UINT const GetFlags() const = 0;
	virtual UINT const GetNumNodes() const = 0;
	virtual UINT const GetLength() const = 0;
	virtual UINT const GetFPS() const = 0;
	virtual UINT64 const GetHashCode() const = 0;
	virtual UINT* const GetNumKeys() const = 0;
	virtual ReAnimKeyFrame** const GetKeyFrames() const = 0;
	/*!
	 * \brief
	 * Node mask access function
	 *
	 * \param i
	 * "i" is the index of the node whose mask boolean value is obtained.
	 */
	virtual BYTE const GetNodeMask(const UINT i) const = 0;
	/*!
	 * \brief
	 * Node mask set function
	 *
	 * \param nodeMask
	 * "nodeMask" accepts an array of bytes equal in number to the number of 
 	 * nodes, where 1 indicates the node is used in animating a bone, and 0 indicates 
	 * this node should be ignored.  
	 */
	virtual void SetNodeMask(const BYTE* nodeMask) = 0;
};

/*!
 * \brief
 * D3D-specific ReAnimAnimation.
 * 
 * A D3D implementation of ReAnimAnimatino which uses the CanimKeyFrameMatrixD3D keyframe 
 * type by default.  The CanimAnimationD3D class can also handle the CanimKeyFrameQuatD3D 
 * by setting a flag (CANIMANIMATIONFLAGS_KEYFRAMEQUAT) at creation time.  
 * The user can derive new keyframe types and extend the class appropriately.
 */
class ReAnimAnimationD3D : public ReAnimAnimationNew {
public:
	ReAnimAnimationD3D();
	ReAnimAnimationD3D(FLOAT** keys, UINT* numKeys, UINT numNodes, UINT fps, UINT length, UINT flags);
	virtual ~ReAnimAnimationD3D();

	virtual void Write(CArchive& ar, BOOL compress = FALSE) const;
	virtual void Read(CArchive& ar);
	virtual UINT const GetFlags() const { return m_flags; }
	virtual UINT const GetNumNodes() const { return m_numNodes; }
	virtual UINT const GetLength() const { return m_animLength; }
	virtual UINT const GetFPS() const { return m_fps; }
	virtual UINT64 const GetHashCode() const { return m_hashCode; }
	virtual UINT* const GetNumKeys() const { return m_numKeys; }
	virtual ReAnimKeyFrame** const GetKeyFrames() const { return m_keyFrames; }
	virtual BYTE const GetNodeMask(const UINT i) const { return i < m_numNodes ? m_nodeMask[i] : 0; }
	virtual void SetNodeMask(const BYTE* nodeMask) { memcpy(m_nodeMask, nodeMask, m_numNodes); }

private:
	UINT m_flags;
	UINT m_numNodes;
	UINT m_animLength;
	UINT m_fps;
	UINT64 m_hashCode;
	UINT* m_numKeys;
	BYTE* m_nodeMask;
	ReAnimKeyFrame** m_keyFrames;

	const static UINT m_headerSize0 = sizeof(UINT) * 4 + sizeof(UINT64);
};

class ReAnimInterpolator;

typedef ReAnimInterpolator* (*ReAnimInterpolatorCallback)(ReAnimInterpolator* interpolator, void* data);

/*!
 * \brief
 * Pure virtual keyframe interpolation class.
 *
 * The ReAnimInterpolator class is provided to do actual keyframe interpolation 
 * for an animation specified using the SetAnimation function.  
 * The CanimInterpolator is constructed using the number of animation nodes and a 
 * flag describing global interpolation behaviour, such as CANIMINTERPOLATORFLAGS_RENORM, 
 * which specifies that the interpolated quantity (i.e. matrix, quaternion) should be 
 * renormalized, or  CANIMINTERPOLATORFLAGS_LOOPING and CANIMINTERPOLATORFLAGS_REVERSE which
 * specify that the interpolated animation loops or is playing in reverse.
 * One can access the interpolated keyframe matrices by the GetMatrices() function.  
 * To interpolate keyframes by a time increment dt, call the AddTime(dt) function.  
 * One can interpolate backwards by calling SubTime(dt), and one can set a particular 
 * interpolation time by SetTime(time).
 *
 * Subhierarchical animation support is provided by constructing  a subinterpolator via 
 * the constructor ReAnimInterpolator(ReAnimInterpolator*, flags).  The ReAnimInterpolator argument 
 * here is the parent interpolator which is used to advance the base animation, while the 
 * constructed subinterpolator advances the secondary animation which typically will only include 
 * a subset of the parent nodes.  Here it is assumed that the secondary CanimAnimation which is 
 * advanced by the subinterpolator will have the SetNodeMask() function described above to specify 
 * the node subset interpolated by the subinterpolator.  The parent animation itself can have a node 
 * mask opposite to that used by the subinterpolator so that it does not update nodes processed by 
 * the subinterpolator.
 *
 * Hierarchical interpolation (i.e. two or more animations blended together) is provided by 
 * the Blend(CanimInterpolator*, ReAnimInterpolator*) function.   These calls can be chained 
 * hierarchically to provide extremely complicated animations (i.e. IK simulations).
 *
 * There is callback function support which enables one to specify functions that are called 
 * at a particular time in the interpolation process.  These are SetAnimCallBack and SetAnimLoopCallBack.
 * The former takes a time argument that invokes the provided callback function when the given time is 
 * reached, while the later invokes the callback function when the animation loops.
 */
class ReAnimInterpolator {
public:
	ReAnimInterpolator();
	/*!
	 * \brief
	 * Constructor to allocate internal data structures
	 *
	 * \param numNodes
	 * "numNodes" number of nodes ("bones") to interpolate
	 * NOTE: this much match the number of bones in the interpolated
	 * animation and in the target skeleton
	 * 
	 * \param flags
	 * "flags" is a global interpolation behaviour flag, such as
	 * CANIMINTERPOLATORFLAGS_RENORM to renormalize the interpolated matrix or quaternion
	 */
	ReAnimInterpolator(UINT numNodes, UINT flags = 0);
	/*!
	 * \brief
	 * Constructor to allocate a subinterpolator
	 *
	 * \param parent
	 * "parent" is the interpolator running the base animation
	 * 
	 * \param flags
	 * "flags" is a global interpolation behaviour flag, such as
	 * CANIMINTERPOLATORFLAGS_RENORM to renormalize the interpolated matrix or quaternion
	 */
	ReAnimInterpolator(ReAnimInterpolator* parent, UINT flags = 0);
	virtual ~ReAnimInterpolator();

private:
	/*!
	 * \brief
	 * Subinterpolator add time function
	 *
	 * \param time
	 * "time" by which to advance the subinterpolator animation
	 */
	virtual void AddTimeSubinterpolator(UINT time) = 0;

public:
	/*!
	 * \brief
	 * Set flags function
	 *
	 * \param flags
	 * "flags" is OR'ed into the current flags.  It can be one of the following values:
	 * CANIMINTERPOLATORFLAGS_LOOPING flags a looping animation
	 * CANIMINTERPOLATORFLAGS_REVERSE to play an animation in reverse
	 */
	virtual void SetFlags(UINT flags) = 0;
	virtual UINT const GetFlags() const = 0;
	/*!
	 * \brief
	 * Set animation function
	 *
	 * \param anim
	 * "anim" is the animation to interpolate
	 */
	virtual BOOL SetAnimation(const ReAnimAnimationNew* anim) = 0;
	virtual FLOAT* const GetMatrices() const = 0;
	virtual ReAnimAnimationNew* const GetAnimation() const = 0;
	/*!
	 * \brief
	 * Forward-Interpolate keyframes by a time increment
	 *
	 * \param time
	 * "time" is the interpolation time increment
	 */
	virtual void AddTime(FLOAT time) = 0;
	/*!
	 * \brief
	 * Reverse-Interpolate keyframes by a time increment
	 *
	 * \param time
	 * "time" is the interpolation time increment
	 */
	virtual void SubTime(FLOAT time) = 0;
	/*!
	 * \brief
	 * Static-interpolate keyframes by a time increment
	 *
	 * \param time
	 * "time" is the interpolation time
	 */
	virtual void SetTime(FLOAT time) = 0;
	/*
	 * \brief
	 * Set a callback function which is called at the given time
	 *
	 * \param callback
	 * "callback" is the callback function to use
	 *
	 * \param time
	 * "time" in the interpolation at which to call the callback function
	 *
	 * \param data
	 * "data" is a user-specified data structure passed to the callback function
	 */
	virtual void SetAnimCallBack(ReAnimInterpolatorCallback callback, FLOAT time, void* data) = 0;
	/*
	 * \brief
	 * Set a callback function which is called when the animation loops
	 *
	 * \param callback
	 * "callback" is the callback function to use
	 *
	 * \param data
	 * "data" is a user-specified data structure passed to the callback function
	 */
	virtual void SetAnimLoopCallBack(ReAnimInterpolatorCallback loopcallback, void* data) = 0;
	/*
	 * \brief
	 * Blends between two ReAnimInterpolator objects to provide multi-level blending
	 *
	 * \param out
	 * "out" holds the blend product
	 *
	 * \param in0, in1
	 * "in0" and "in1" are the interpolators to blend with the result stored in this interpolator
	 */
	virtual void Blend(ReAnimInterpolator* in0, ReAnimInterpolator* in1) = 0;
};

#define CANIMINTERPOLATORFLAGS_VERSION 0x00000000
#define CANIMINTERPOLATORFLAGS_LOOPING 0x00000010
#define CANIMINTERPOLATORFLAGS_REVERSE 0x00000020
#define CANIMINTERPOLATORFLAGS_RENORM 0x00000040

/*!
 * \brief
 * D3D-specific ReAnimInterpolator.
 *
 * A D3D implementation of ReAnimInterpolator which uses the ReAnimAnimationD3D 
 * animation class. The matrices holding the interpolated keyframes comprise
 * 16-byte aligned matrices.
 */
class ReAnimInterpolatorD3D : public ReAnimInterpolator {
private:
	UINT m_flags;
	UINT m_numNodes;
	ReAnimAnimationNew* m_anim;
	UINT m_time;
	ReAnimInterpolatorCallback m_animCallback;
	FLOAT m_callbackTime;
	void* m_callbackdata;
	ReAnimInterpolatorCallback m_loopCallback;
	void* m_loopCallbackdata;
	Matrix44fA16* m_interpMats;
	UINT* m_currFrame;
	ReAnimInterpolator* m_parent;

private:
	virtual void AddTimeSubinterpolator(UINT time);

public:
	ReAnimInterpolatorD3D();
	ReAnimInterpolatorD3D(UINT numNodes, UINT flags = 0);
	ReAnimInterpolatorD3D(ReAnimInterpolator* parent, UINT flags = 0);
	virtual ~ReAnimInterpolatorD3D();

	virtual void SetFlags(UINT flags) { m_flags |= flags; }
	virtual UINT const GetFlags() const { return m_flags; }
	virtual FLOAT* const GetMatrices() const { return (FLOAT*)m_interpMats; }
	virtual ReAnimAnimationNew* const GetAnimation() const { return m_anim; }
	virtual BOOL SetAnimation(const ReAnimAnimationNew* anim, UINT flags = 0);
	virtual void AddTime(UINT time);
	virtual void SubTime(UINT /*time*/) { ; }
	virtual void SetTime(UINT /*time*/) { ; }
	virtual void SetAnimCallBack(ReAnimInterpolatorCallback /*callback*/, FLOAT /*time*/, void* /*data*/) { ; }
	virtual void SetAnimLoopCallBack(ReAnimInterpolatorCallback /*loopcallback*/, void* /*data*/) { ; }
	virtual void Blend(ReAnimInterpolator* /*out*/, ReAnimInterpolator* /*in*/) { ; }
};

} // namespace KEP
