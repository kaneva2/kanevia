///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReMeshConvertOptions.h"

#ifdef ED7119_DYNOBJ_ASSET_FIX

bool ReMeshConvertOptions::ms_bRecalcSkinHashBeforeWrite = false;
bool ReMeshConvertOptions::ms_bFixBoneWeightSortOrder = false;
bool ReMeshConvertOptions::ms_bFixReducedFirstWeight = false;
bool ReMeshConvertOptions::ms_bRecalcDecompressionConstant = false;
bool ReMeshConvertOptions::ms_bRoundSkinMeshNormalAndUVScale = false;

#endif
