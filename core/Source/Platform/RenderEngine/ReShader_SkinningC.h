///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ReMaterial.h"
#include "ReD3DX9.h"

namespace KEP {

class ReD3DX9SkinMesh;
struct ReMeshData;
struct ReSkinMeshData;
class IMemSizeGadget;

class ReShader_SkinningC : public ReShader {
public:
	ReShader_SkinningC() { m_mesh = NULL; }
	ReShader_SkinningC(ReD3DX9SkinMesh* mesh) { m_mesh = mesh; }
	ReShader_SkinningC(const ReShader_SkinningC& shader) {
		m_mesh = shader.m_mesh;
		;
	}
	virtual ~ReShader_SkinningC() { ; }

	virtual void PreRender() { ; }
	virtual void Render() { ; }
	virtual void PostRender() { ; }
	virtual void SetTechnique() { ; }
	virtual BOOL Update(const ReShaderData* desc = NULL);
	virtual void Invalidate() { ; }
	virtual BOOL EqualTo(const ReShader* /*shader*/) const { return TRUE; }
	virtual INT Compare(const ReShader* /*shader*/) const { return 0; }
	//virtual UINT64				Hash() const							{return ReD3DX9DEVICEINVALID;}
	virtual BOOL Invalid() const { return FALSE; }
	virtual ReShader* Clone(ReMesh* /*mesh*/ = NULL) { return NULL; }
	virtual ReShaderType ShaderType() const { return ReShaderType::CPU_GENERIC; }
	virtual void Begin(UINT& passes) { passes = 1; }
	virtual void BeginPass(UINT /*pass*/) const { ; }
	virtual void EndPass() const { ; }
	virtual void End() const { ; }

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	virtual BOOL Restore() { return TRUE; }

private:
	ReD3DX9SkinMesh* m_mesh;

	void UpdateSkinGeneric();
	void UpdateSkinGenericSingleTex();
	void UpdateSkinGenericMultiTex0();
	void UpdateSkinGenericMultiTex1();
	void UpdateSkinGenericMultiTex2();
	void UpdateSkin1Weight();
	void UpdateSkin2Weight();
	void UpdateSkin3Weight();
	void UpdateSkin4Weight();

	void Mat4Scale(Matrix44f* pOut, const Matrix44f* pIn, float Scale);
	void Mat4Add(Matrix44f* pOut, const Matrix44f* pInA, const Matrix44f* pInB);
	void Vec3TransformCoord(Vector3f* pOut, const Vector3f* pIn, const Matrix44f* pMat);
	void Vec3Add(Vector3f* pOut, const Vector3f* pInA, const Vector3f* pInB);
	void Vec3TransformNormal(Vector3f* pOut, const Vector3f* pIn, const Matrix44f* pMat);
	void Vec3Scale(Vector3f* pOut, const Vector3f* pIn, float Scale);
	void Vec3Normalize(Vector3f* pOut, const Vector3f* pIn);
};

} // namespace KEP