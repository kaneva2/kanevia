///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReD3DX9SkinMesh.h"
#include "ReD3DX9Shader.h"
#include "Utils/ReUtils.h"
#include "..\Common\include\ITextureProxy.h"
#include "..\common\include\IMemSizeGadget.h"

namespace KEP {

ReD3DX9Shader::ReD3DX9Shader() {
	m_fX = NULL;
	m_technq = NULL;
	m_dev = NULL;
	m_resFX = ReD3DX9DEVICEINVALID;
	//m_hashFX		=	ReD3DX9DEVICEINVALID;
	m_codeFX = NULL;
	m_numHandles = 0;
}

ReD3DX9Shader::ReD3DX9Shader(const ReD3DX9Shader& shader) :
		ReShader(shader) {
	m_fX = shader.m_fX;
	m_technq = shader.m_technq;
	m_dev = shader.m_dev;
	m_resFX = shader.m_resFX;
	//m_hashFX		=	shader.m_hashFX;
	m_codeFX = shader.m_codeFX;
	m_numHandles = shader.m_numHandles;
	memcpy(&m_handles, &shader.m_handles, sizeof(ReD3DX9EffectHandle) * m_numHandles);
}

ReD3DX9Shader::ReD3DX9Shader(ReD3DX9DeviceState* dev, const char* shader) {
	m_fX = NULL;
	m_technq = NULL;
	m_codeFX = NULL;
	m_dev = dev;
	m_resFX = ReD3DX9DEVICEINVALID;
	m_numHandles = 0;

	//TODO: allocate the m_handles and set this pointer to cloned shaders!!!! (add clone flag also).
	if (shader) {
		//m_hashFX		=	ReHash((BYTE*)shader, (DWORD)strlen(shader), 0);
		this->SetHash(ReHash((BYTE*)shader, (DWORD)strlen(shader), 0));
		m_codeFX = shader;
	}
}

ReD3DX9Shader::~ReD3DX9Shader() {
	m_dev->InvalidateResource(&m_resFX);
}

void ReD3DX9Shader::GetHandles() {
	D3DXEFFECT_DESC fxDesc;

	m_fX->GetDesc(&fxDesc);

	m_numHandles = 0;

	for (UINT i = 0; i < fxDesc.Parameters; i++) {
		D3DXPARAMETER_DESC desc;
		D3DXHANDLE hParam = m_fX->GetParameter(NULL, i);

		m_fX->GetParameterDesc(hParam, &desc);

		if (desc.Semantic != NULL && (desc.Class == D3DXPC_MATRIX_ROWS || desc.Class == D3DXPC_MATRIX_COLUMNS)) {
			if (_strcmpi(desc.Semantic, "worldmatrixarray") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_WORLDMATRIXARRAY_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "world") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATWORLD_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "view") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATVIEW_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "projection") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATPROJ_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "viewprojection") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATVIEWPROJ_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "worldviewprojection") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATWORLDVIEWPROJ_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "worldinversetranspose") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATWORLDINVERSETRANSPOSE_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "viewinverse") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATVIEWINVERSE_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "texturematrix0") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATTEXTURE0_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "texturematrix1") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATTEXTURE1_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "texturematrix2") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATTEXTURE2_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "texturematrix3") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATTEXTURE3_EFFECTHANDLE;
			}

			if (m_numHandles < MAXEFFECTHANDLES) {
				m_numHandles++;
			}
		} else if (desc.Semantic != NULL && (desc.Class == D3DXPC_VECTOR)) {
			if (_strcmpi(desc.Semantic, "materialambient") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATERIALAMBIENT_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "materialdiffuse") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATERIALDIFFUSE_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "materialspecular") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATERIALSPECULAR_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "materialemissive") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATERIALEMISSIVE_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "cameraposition") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_CAMERAPOS_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "globalambient") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_GLOBALAMBIENT_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightambient") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTAMBIENT_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightdiffuse") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTDIFFUSE_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightdirection") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTDIRECTION_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightposition") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTPOSITION_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightspecular") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTSPECULAR_EFFECTHANDLE;
			}

			if (m_numHandles < MAXEFFECTHANDLES) {
				m_numHandles++;
			}
		} else if (desc.Semantic != NULL && (desc.Class == D3DXPC_SCALAR)) {
			if (_strcmpi(desc.Semantic, "materialpower") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MATERIALSPECULARPOWER_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "time") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_TIME_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "meshradius") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_MESHRADIUS_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "numbones") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_NUMBONES_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "numlights") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_NUMLIGHTS_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightattenuation0") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTATTENUATION0_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightattenuation1") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTATTENUATION1_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightattenuation2") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTATTENUATION2_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "lightrange") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_LIGHTRANGE_EFFECTHANDLE;
			}

			if (m_numHandles < MAXEFFECTHANDLES) {
				m_numHandles++;
			}
		} else if ((desc.Class == D3DXPC_OBJECT) && (desc.Type == D3DXPT_TEXTURE)) {
			if (_strcmpi(desc.Semantic, "texture0") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_TEXTURE0_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "texture1") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_TEXTURE1_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "texture2") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_TEXTURE2_EFFECTHANDLE;
			} else if (_strcmpi(desc.Semantic, "texture3") == 0) {
				m_handles[m_numHandles].handle = hParam;
				m_handles[m_numHandles].type = ReD3DX9_TEXTURE3_EFFECTHANDLE;
			}

			if (m_numHandles < MAXEFFECTHANDLES) {
				m_numHandles++;
			}
		}
	}
}

BOOL ReD3DX9Shader::Restore() {
	if (Invalid() && m_codeFX) {
		uint64_t hash;
		if (!m_dev->CreateSystemResource(ReD3DX9ResourceType::EFFECT, ReD3DX9VertexType::INVALID, &hash, (void**)&m_fX, &m_resFX, m_codeFX)) {
			return FALSE;
		}
		//m_hashFX = hash;
		this->SetHash(hash);

		m_technq = m_fX->GetCurrentTechnique();

		if (!m_technq) {
			if (!SUCCEEDED(m_fX->FindNextValidTechnique(NULL, &m_technq))) {
				return FALSE;
			}
		}

		GetHandles();
	}
	return TRUE;
}

BOOL ReD3DX9Shader::Update(const ReShaderData* data) {
	if (!Restore()) {
		return FALSE;
	}

	if (data->pWorld == NULL) {
		return FALSE;
	}

	ReD3DX9DeviceRenderState* state = m_dev->GetRenderState();

	for (UINT i = 0; i < m_numHandles; i++) {
		switch (m_handles[i].type) {
			case ReD3DX9_WORLDMATRIXARRAY_EFFECTHANDLE: {
				if (m_dev->WorldMatArrayRender()) {
					m_fX->SetMatrixArray(m_handles[i].handle, data->pBones, data->NumBones);
				}
				break;
			}

			case ReD3DX9_NUMBONES_EFFECTHANDLE: {
				m_fX->SetInt(m_handles[i].handle, data->NumWeightsPerVertex);
				break;
			}

			case ReD3DX9_MATWORLD_EFFECTHANDLE: {
				if (m_dev->WorldMatRender()) {
					m_fX->SetMatrix(m_handles[i].handle, data->pWorld);
				}
				break;
			}

			case ReD3DX9_MATVIEW_EFFECTHANDLE: {
				if (m_dev->ViewMatRender()) {
					m_fX->SetMatrix(m_handles[i].handle, state->GetViewMatrix());
				}
				break;
			}

			case ReD3DX9_MATPROJ_EFFECTHANDLE: {
				if (m_dev->ProjMatRender()) {
					m_fX->SetMatrix(m_handles[i].handle, state->GetProjMatrix());
				}
				break;
			}

			case ReD3DX9_MATVIEWPROJ_EFFECTHANDLE: {
				if (m_dev->ProjViewMatRender()) {
					m_fX->SetMatrix(m_handles[i].handle, state->GetProjView());
				}
				break;
			}

			case ReD3DX9_MATWORLDVIEWPROJ_EFFECTHANDLE: {
				m_fX->SetMatrix(m_handles[i].handle, data->pWorldViewProj);
				break;
			}

			case ReD3DX9_MATWORLDINVERSETRANSPOSE_EFFECTHANDLE: {
				m_fX->SetMatrix(m_handles[i].handle, data->pWorldInvTranspose);
				break;
			}

			case ReD3DX9_MATVIEWINVERSE_EFFECTHANDLE: {
				break;
			}

			case ReD3DX9_MATTEXTURE0_EFFECTHANDLE: {
				if (m_dev->TextMatRender((UINT64)data->State->TexMat[0], 0)) {
					m_fX->SetMatrix(m_handles[i].handle, data->State->TexMat[0]);
				}
				break;
			}

			case ReD3DX9_MATTEXTURE1_EFFECTHANDLE: {
				if (m_dev->TextMatRender((UINT64)data->State->TexMat[1], 1)) {
					m_fX->SetMatrix(m_handles[i].handle, data->State->TexMat[1]);
				}
				break;
			}

			case ReD3DX9_MATTEXTURE2_EFFECTHANDLE: {
				if (m_dev->TextMatRender((UINT64)data->State->TexMat[2], 2)) {
					m_fX->SetMatrix(m_handles[i].handle, data->State->TexMat[2]);
				}
				break;
			}

			case ReD3DX9_MATTEXTURE3_EFFECTHANDLE: {
				if (m_dev->TextMatRender((UINT64)data->State->TexMat[3], 3)) {
					m_fX->SetMatrix(m_handles[i].handle, data->State->TexMat[3]);
				}
				break;
			}

			case ReD3DX9_CAMERAPOS_EFFECTHANDLE: {
				if (m_dev->CamPosRender()) {
					m_fX->SetVector(m_handles[i].handle, state->GetCamPosition());
				}
				break;
			}

			case ReD3DX9_MATERIALAMBIENT_EFFECTHANDLE: {
				m_fX->SetVector(m_handles[i].handle, reinterpret_cast<const D3DXVECTOR4*>(&data->State->Mat.Ambient));
				break;
			}

			case ReD3DX9_MATERIALDIFFUSE_EFFECTHANDLE: {
				m_fX->SetVector(m_handles[i].handle, reinterpret_cast<const D3DXVECTOR4*>(&data->State->Mat.Diffuse));
				break;
			}

			case ReD3DX9_MATERIALSPECULAR_EFFECTHANDLE: {
				m_fX->SetVector(m_handles[i].handle, reinterpret_cast<const D3DXVECTOR4*>(&data->State->Mat.Specular));
				break;
			}

			case ReD3DX9_MATERIALEMISSIVE_EFFECTHANDLE: {
				m_fX->SetVector(m_handles[i].handle, reinterpret_cast<const D3DXVECTOR4*>(&data->State->Mat.Emissive));
				break;
			}

			case ReD3DX9_MATERIALSPECULARPOWER_EFFECTHANDLE: {
				m_fX->SetFloat(m_handles[i].handle, data->State->Mat.Power);
				break;
			}

			case ReD3DX9_TIME_EFFECTHANDLE: {
				m_fX->SetFloat(m_handles[i].handle, 0.f);
				break;
			}

			case ReD3DX9_MESHRADIUS_EFFECTHANDLE: {
				m_fX->SetFloat(m_handles[i].handle, data->Radius);
				break;
			}

			case ReD3DX9_TEXTURE0_EFFECTHANDLE:
			case ReD3DX9_TEXTURE1_EFFECTHANDLE:
			case ReD3DX9_TEXTURE2_EFFECTHANDLE:
			case ReD3DX9_TEXTURE3_EFFECTHANDLE: {
				size_t idx = m_handles[i].type - ReD3DX9_TEXTURE0_EFFECTHANDLE;
				if (data->State->ResPtr[0] != NULL) {
					auto pTex = data->State->ResPtr[idx]->getTexturePtr();
					if (pTex) {
						pTex->AddRef();
						data->State->TexPtr[idx].reset(pTex, [](void* p) { static_cast<IDirect3DTexture9*>(p)->Release(); });
					} else {
						data->State->TexPtr[idx].reset();
					}
				}

				if (m_dev->TexRender((UINT64)data->State->TexPtr[idx].get(), idx))
					m_fX->SetTexture(m_handles[i].handle, (LPDIRECT3DTEXTURE9)data->State->TexPtr[idx].get());
			} break;

			case ReD3DX9_GLOBALAMBIENT_EFFECTHANDLE: {
				if (m_dev->GlobalAmbnRender()) {
					m_fX->SetVector(m_handles[i].handle, state->GetGlobalAmbient());
				}
			} break;

			case ReD3DX9_LIGHTAMBIENT_EFFECTHANDLE: {
				if (m_dev->LightAmbnRender()) {
					m_fX->SetVectorArray(m_handles[i].handle, state->GetLightAmbient(), 4);
				}
			} break;

			case ReD3DX9_LIGHTDIFFUSE_EFFECTHANDLE: {
				if (m_dev->LightDiffRender()) {
					m_fX->SetVectorArray(m_handles[i].handle, state->GetLightDiffuse(), 4);
				}
			} break;

			case ReD3DX9_LIGHTDIRECTION_EFFECTHANDLE: {
				if (m_dev->LightDirnRender()) {
					m_fX->SetVectorArray(m_handles[i].handle, state->GetLightDirection(), 4);
				}
			} break;

			case ReD3DX9_LIGHTPOSITION_EFFECTHANDLE: {
				if (m_dev->LightPosnRender()) {
					m_fX->SetVectorArray(m_handles[i].handle, state->GetLightPosition(), 4);
				}
			} break;

			case ReD3DX9_LIGHTSPECULAR_EFFECTHANDLE: {
				if (m_dev->LightSpecRender()) {
					m_fX->SetVectorArray(m_handles[i].handle, state->GetLightSpecular(), 4);
				}
			} break;

			case ReD3DX9_LIGHTATTENUATION0_EFFECTHANDLE: {
				if (m_dev->LightAtt0Render()) {
					m_fX->SetFloatArray(m_handles[i].handle, state->GetLightAttenuation0(), 4);
				}
			} break;

			case ReD3DX9_LIGHTATTENUATION1_EFFECTHANDLE: {
				if (m_dev->LightAtt1Render()) {
					m_fX->SetFloatArray(m_handles[i].handle, state->GetLightAttenuation1(), 4);
				}
			} break;

			case ReD3DX9_LIGHTATTENUATION2_EFFECTHANDLE: {
				if (m_dev->LightAtt2Render()) {
					m_fX->SetFloatArray(m_handles[i].handle, state->GetLightAttenuation2(), 4);
				}
			} break;

			case ReD3DX9_LIGHTRANGE_EFFECTHANDLE: {
				m_fX->SetFloatArray(m_handles[i].handle, state->GetLightRange(), 4);
			} break;

			case ReD3DX9_NUMLIGHTS_EFFECTHANDLE: {
				m_fX->SetInt(m_handles[i].handle, state->GetNumActiveLights());
			} break;

			default:;
		}
	}

	return TRUE;
}

void ReD3DX9Shader::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObjectSizeInBytes(&MAXEFFECTHANDLES, sizeof(ReD3DX9EffectHandle) * m_numHandles);
	}
}

} // namespace KEP
