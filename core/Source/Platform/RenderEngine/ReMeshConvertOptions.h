///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Helpers for asset converter
#define ED7119_DYNOBJ_ASSET_FIX

class ReMeshConvertOptions {
public:
#ifdef ED7119_DYNOBJ_ASSET_FIX
public:
	static void EnableED7119AssetFix() {
		ms_bRecalcSkinHashBeforeWrite = true;
		ms_bFixBoneWeightSortOrder = true;
		ms_bFixReducedFirstWeight = true;
		ms_bRecalcDecompressionConstant = true;
		ms_bRoundSkinMeshNormalAndUVScale = true;
	}

	static bool getRecalcSkinHashBeforeWrite() { return ms_bRecalcSkinHashBeforeWrite; }
	static bool getFixBoneWeightSortOrder() { return ms_bFixBoneWeightSortOrder; }
	static bool getFixReducedFirstWeight() { return ms_bFixReducedFirstWeight; }
	static bool getRecalcDecompressionConstant() { return ms_bRecalcDecompressionConstant; }
	static bool getRoundSkinMeshNormalAndUVScale() { return ms_bRoundSkinMeshNormalAndUVScale; }

private:
	static bool ms_bRecalcSkinHashBeforeWrite; // Recalculate hash for ReSkinMesh before serialization
	static bool ms_bFixBoneWeightSortOrder; // Sort bone weights by weight (descending) first, then by bone index (ascending)
	static bool ms_bFixReducedFirstWeight; // pW[0] = pW[1] if (pW[0] < pW[1] && pW[0] >= pW[1] - 2)
	static bool ms_bRecalcDecompressionConstant; // ScaleConst[i].z = 1.0f / ScaleConst[i].y
	static bool ms_bRoundSkinMeshNormalAndUVScale; // ScaleConst[0/1].x/y -> round to nearest 2^-20 (~1E-6) in significant
#endif
};
