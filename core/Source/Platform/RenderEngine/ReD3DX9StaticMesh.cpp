///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "ReD3DX9StaticMesh.h"

#include "ReShader_Prelighting.h"
#include "ReD3DX9Shader.h"
#include "emmintrin.h"
#include "Common/include/IMemSizeGadget.h"

namespace KEP {

ReD3DX9StaticMesh::ReD3DX9StaticMesh() :
		ReMesh(), ReD3DX9Mesh() {
	;
}

ReD3DX9StaticMesh::ReD3DX9StaticMesh(void *dev) :
		ReMesh(), ReD3DX9Mesh((ReD3DX9DeviceState *)dev) {
	m_Shader = new ReD3DX9ShaderNULL;
}

ReD3DX9StaticMesh::~ReD3DX9StaticMesh() {
	;
}

void ReD3DX9StaticMesh::SetMaterial(ReMaterialPtr material) {
	if (m_Material == material) {
		return;
	}

	m_Material = material;

	if (material->PreLit()) {
		ReShader_Prelighting *shader = new ReShader_Prelighting(this);
		SetShader(shader);
	} else {
		// use the material's shader if using fixedfunc
		if (m_Shader->ShaderType() == ReShaderType::FIXEDFUNC) {
			if (material->GetRenderState()->GetData()->ShaderCode) {
				CreateShader(material->GetRenderState()->GetData()->ShaderCode);
			}
		}
	}
}

BOOL ReD3DX9StaticMesh::CreateShader(const char *shader) {
	if (!m_pDev) {
		return FALSE;
	}

	if (m_Shader) {
		delete m_Shader;
	}

	// invalidate current D3DX9 resources to rebuild them
	ReD3DX9Mesh::Invalidate();

	// get hardware caps
	auto HWCaps = m_pDev->GetRenderState()->GetHardwareCaps();
	// no GPU shader...use default CPU skinning shader
	if (shader == NULL) {
		m_Shader = new ReD3DX9ShaderNULL;
	}
	// use GPU effect shader if HW allows
	else if (HWCaps >= ReD3DX9MINVERTEXSHADERSUPPORT) {
		m_Shader = new ReD3DX9Shader(m_pDev, shader);
		if (!m_Shader) {
			return FALSE;
		}
	} else {
		m_Shader = new ReD3DX9ShaderNULL;
	}

	switch (m_Desc.NumUV) {
		case 1:
			m_VertexType = ReD3DX9VertexType::POS_NORM_UV1;
			break;

		case 2:
			m_VertexType = ReD3DX9VertexType::POS_NORM_UV2;
			break;

		case 3:
			m_VertexType = ReD3DX9VertexType::POS_NORM_UV3;
			break;

		case 4:
			m_VertexType = ReD3DX9VertexType::POS_NORM_UV4;
			break;

		default:
			m_VertexType = ReD3DX9VertexType::INVALID;
			break;
	}

	return TRUE;
}

BOOL ReD3DX9StaticMesh::SetShader(ReShader *shader) {
	if (m_Shader == shader) {
		return TRUE;
	}

	if (shader == nullptr && typeid(*m_Shader) == typeid(ReD3DX9ShaderNULL))
		return TRUE;

	// delete current shader
	delete m_Shader;
	m_Shader = NULL;

	// invalidate current D3DX9 resources to rebuild them
	ReD3DX9Mesh::Invalidate();

	if (shader == NULL) {
		m_Shader = new ReD3DX9ShaderNULL;
	} else {
		m_Shader = shader;
	}

	switch (m_Shader->ShaderType()) {
		case ReShaderType::PRELIGHTING: {
			switch (m_Desc.NumUV) {
				case 1:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV1_DS;
					break;

				case 2:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV2_DS;
					break;

				case 3:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV3_DS;
					break;

				case 4:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV4_DS;
					break;

				default:
					m_VertexType = ReD3DX9VertexType::INVALID;
					break;
			}
		} break;

		default: {
			switch (m_Desc.NumUV) {
				case 1:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV1;
					break;

				case 2:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV2;
					break;

				case 3:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV3;
					break;

				case 4:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV4;
					break;

				default:
					m_VertexType = ReD3DX9VertexType::INVALID;
					break;
			}
		} break;
	}
	return TRUE;
}

ReD3DX9StaticMesh::ReD3DX9StaticMesh(ReD3DX9StaticMesh &mesh) :
		ReMesh(), ReD3DX9Mesh(mesh.m_pDev) {
	// copy over the ReMesh properties
	m_Desc = mesh.m_Desc;

	// copy over the state
	m_State = mesh.m_State;

	// copy over the ReD3DX9Mesh properties
	m_pDev = mesh.m_pDev;
	m_VertexType = mesh.m_VertexType;
	m_HashVB = mesh.m_HashVB;
	m_HashIB = mesh.m_HashIB;
	m_HashVD = mesh.m_HashVD;
	m_VSize = mesh.m_VSize;

	// create new vertex buffer and index buffer
	m_pVB = mesh.m_pVB;
	m_ResVB = ReD3DX9DEVICEINVALID;
	m_pIB = mesh.m_pIB;
	m_ResIB = ReD3DX9DEVICEINVALID;

	// this flag invokes a "CreateSystemResource" call to manage the vertex decl
	m_pVD = NULL;
	m_ResVD = ReD3DX9DEVICEINVALID;

	m_Material = mesh.m_Material;

	if (mesh.m_Shader) {
		// clone the shader
		ReShader *shader = mesh.m_Shader->Clone(this);
		// set the cloned shader
		SetShader(shader);
	}

	// mark the mesh as cloned
	SetCloned(TRUE);
}

void ReD3DX9StaticMesh::Clone(ReMesh **mesh, bool forceClone) {
	if (!Restore() && forceClone) {
		*mesh = new ReD3DX9StaticMesh(this->m_pDev);
		// copy over the ReMesh properties
		((ReD3DX9StaticMesh *)*mesh)->m_Desc = m_Desc;
		// copy over the state
		((ReD3DX9StaticMesh *)*mesh)->m_State = m_State;
		// copy over the ReD3DX9Mesh properties
		((ReD3DX9StaticMesh *)*mesh)->m_pDev = m_pDev;
		((ReD3DX9StaticMesh *)*mesh)->m_VertexType = m_VertexType;
		((ReD3DX9StaticMesh *)*mesh)->m_HashVB = m_HashVB;
		((ReD3DX9StaticMesh *)*mesh)->m_HashIB = m_HashIB;
		((ReD3DX9StaticMesh *)*mesh)->m_HashVD = m_HashVD;
		((ReD3DX9StaticMesh *)*mesh)->m_VSize = m_VSize;
		// create new vertex buffer and index buffer
		((ReD3DX9StaticMesh *)*mesh)->m_pVB = NULL; //m_pVB;
		((ReD3DX9StaticMesh *)*mesh)->m_ResVB = ReD3DX9DEVICEINVALID;
		((ReD3DX9StaticMesh *)*mesh)->m_pIB = NULL; //m_pIB;
		((ReD3DX9StaticMesh *)*mesh)->m_ResIB = ReD3DX9DEVICEINVALID;
		// this flag invokes a "CreateSystemResource" call to manage the vertex decl
		((ReD3DX9StaticMesh *)*mesh)->m_pVD = NULL;
		((ReD3DX9StaticMesh *)*mesh)->m_ResVD = ReD3DX9DEVICEINVALID;
		((ReD3DX9StaticMesh *)*mesh)->m_Material = m_Material;
		if (m_Shader) {
			// clone the shader
			ReShader *shader = m_Shader->Clone(this);
			// set the cloned shader
			(*mesh)->SetShader(shader);
		}
		// mark the mesh as cloned
		(*mesh)->SetCloned(TRUE);
	} else if (!Restore()) {
		*mesh = NULL;
	} else {
		*mesh = new ReD3DX9StaticMesh(*this);
	}
}

BOOL ReD3DX9StaticMesh::Update() {
	if (!Restore()) {
		return FALSE;
	}

	// DRF - Update Moving Textures (sky clouds, 'in-motion' clothing, dance floor, etc)
	m_Material->Update();

	m_Material->Render();

	if (m_Shader->ShaderType() == ReShaderType::GPU) {
		ReShaderData data;

		data.pWorld = (D3DXMATRIX *)GetWorldMatrix();
		data.pWorldViewProj = (D3DXMATRIX *)GetWorldViewProjMatrix();
		data.pWorldInvTranspose = (D3DXMATRIX *)GetWorldInvTransposeMatrix();
		data.LightCache = (ReLightCache *)GetLightCache();
		data.State = GetMaterialMutable()->GetRenderStateDataMutable();
		data.Radius = GetRadius();

		if (!m_Shader->Update(&data)) {
			return FALSE;
		}
	} else {
		m_Shader->Update(NULL);
	}

	return TRUE;
}

void ReD3DX9StaticMesh::Render() {
	if (ReD3DX9Mesh::Invalid()) {
		return;
	}

	m_Shader->Render();

	m_pDev->FlushVertexBufferCache();

	BOOL indexed = (m_Desc.pI != NULL);
	ReD3DX9Mesh::Render(m_Desc.NumV, m_Desc.NumI, m_Desc.PrimType, indexed);
}

void ReD3DX9StaticMesh::GetMemSize(IMemSizeGadget *pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		ReMesh::GetMemSize(pMemSizeGadget);
	}
}

} // namespace KEP
