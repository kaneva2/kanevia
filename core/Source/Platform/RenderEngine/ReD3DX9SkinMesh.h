///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ReSkinMesh.h"

#include "../Common/include/config.h"
#include "ReD3DX9.h"

namespace KEP {

class IMemSizeGadget;

class ReD3DX9SkinMesh : public ReSkinMesh, public ReD3DX9Mesh {
	friend class ReD3DX9Shader;
	friend class ReShader_SkinningC;
	friend class ReShader_SkinningSSE;

public:
	ReD3DX9SkinMesh();
	ReD3DX9SkinMesh(void* dev);
	ReD3DX9SkinMesh(ReD3DX9SkinMesh& mesh);
	virtual ~ReD3DX9SkinMesh();
	virtual ReSkinMesh* New() const { return new ReD3DX9SkinMesh(m_pDev); }

	virtual BOOL Lock(Vector3f* pos, DWORD numV) override {
		if (!Restore())
			return FALSE;
		return ReD3DX9Mesh::Lock(reinterpret_cast<D3DXVECTOR3*>(pos), numV);
	}
	virtual void Build(bool strip = true) override {
		ReSkinMesh::Build(strip);
		if (m_Desc.Status == ReMESHSTATUS_INIT)
			ReD3DX9Mesh::Hash(m_Desc.Hash);
	}
	virtual void PreRender() override {
		if (m_Material)
			m_Material->PreRender();
		ReD3DX9Mesh::PreRender();
	}
	virtual void Render() override;
	virtual void PostRender() override {
		if (m_Material)
			m_Material->PostRender();
	}
	virtual BOOL IsInvalid() const override { return m_Shader ? (m_Shader->Invalid() || ReD3DX9Mesh::Invalid()) : ReD3DX9Mesh::Invalid(); }
	virtual void SetMaterial(ReMaterialPtr material) override;
	virtual BOOL Update() override;
	virtual BOOL CreateShader(const char* shader) override;
	virtual BOOL SetShader(ReShader* shader) override;
	virtual void Clone(ReMesh** mesh) override { *mesh = new ReD3DX9SkinMesh(*this); }
	virtual void Clone(ReMesh** mesh, bool /*forceClone*/) override { Clone(mesh); }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	virtual BOOL Restore() override;
};

} // namespace KEP