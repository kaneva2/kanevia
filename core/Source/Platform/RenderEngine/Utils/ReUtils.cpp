///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "ReUtils.h"

namespace KEP {

// "Hash Functions for Hash Table Lookup"
// Robert J. Jenkins Jr., 1995-1997
// http://burtleburtle.net/bob/hash/evahash.html

/* The whole new hash function */
DWORD ReHash(BYTE *k, DWORD length, DWORD initval) {
	register DWORD a, b, c; /* the internal state */
	DWORD len; /* how many key bytes still need mixing */

	/* Set up the internal state */
	len = length;
	a = b = 0x9e3779b9; /* the golden ratio; an arbitrary value */
	c = initval; /* variable initialization of internal state */

	/*---------------------------------------- handle most of the key */
	while (len >= 12) {
		a = a + (k[0] + ((DWORD)k[1] << 8) + ((DWORD)k[2] << 16) + ((DWORD)k[3] << 24));
		b = b + (k[4] + ((DWORD)k[5] << 8) + ((DWORD)k[6] << 16) + ((DWORD)k[7] << 24));
		c = c + (k[8] + ((DWORD)k[9] << 8) + ((DWORD)k[10] << 16) + ((DWORD)k[11] << 24));
		ReHashMix(a, b, c);
		k = k + 12;
		len = len - 12;
	}

	/*------------------------------------- handle the last 11 bytes */
	c = c + length;
	switch (len) /* all the case statements fall through */
	{
		case 11: c = c + ((DWORD)k[10] << 24);
		case 10: c = c + ((DWORD)k[9] << 16);
		case 9:
			c = c + ((DWORD)k[8] << 8);
			/* the first byte of c is reserved for the length */
		case 8: b = b + ((DWORD)k[7] << 24);
		case 7: b = b + ((DWORD)k[6] << 16);
		case 6: b = b + ((DWORD)k[5] << 8);
		case 5: b = b + k[4];
		case 4: a = a + ((DWORD)k[3] << 24);
		case 3: a = a + ((DWORD)k[2] << 16);
		case 2: a = a + ((DWORD)k[1] << 8);
		case 1:
			a = a + k[0];
			/* case 0: nothing left to add */
	}
	ReHashMix(a, b, c);
	/*-------------------------------------------- report the result */
	return c;
}

// CPUID from Microsoft VisualC++ sample code

// These are the bit flags that get set on calling cpuid
// with register eax set to 1
#define _MMX_FEATURE_BIT 0x00800000
#define _SSE_FEATURE_BIT 0x02000000
#define _SSE2_FEATURE_BIT 0x04000000

// This bit is set when cpuid is called with
// register set to 80000001h (only applicable to AMD)
#define _3DNOW_FEATURE_BIT 0x80000000

int IsCPUID() {
	__try {
		_asm {
            xor eax, eax
            cpuid
		}
	} __except (EXCEPTION_EXECUTE_HANDLER) {
		return 0;
	}
	return 1;
}

/***
* int _os_support(int feature)
*   - Checks if OS Supports the capablity or not
*
* Entry:
*   feature: the feature we want to check if OS supports it.
*
* Exit:
*   Returns 1 if OS support exist and 0 when OS doesn't support it.
*
****************************************************************/

int _os_support(int feature) {
	__try {
		switch (feature) {
			case _CPU_FEATURE_SSE:
				__asm
				{
					xorps xmm0, xmm0 // executing SSE instruction
				}
				break;
			case _CPU_FEATURE_SSE2:
				__asm
				{
					xorpd xmm0, xmm0 // executing SSE2 instruction
				}
				break;
			case _CPU_FEATURE_3DNOW:
				__asm
				{
					pfrcp mm0, mm0 // executing 3DNow! instruction
					emms
				}
				break;
			case _CPU_FEATURE_MMX:
				__asm
				{
					pxor mm0, mm0 // executing MMX instruction
					emms
				}
				break;
		}
	} __except (EXCEPTION_EXECUTE_HANDLER) {
		if (_exception_code() == STATUS_ILLEGAL_INSTRUCTION) {
			return 0;
		}
		return 0;
	}
	return 1;
}

/***
*
* void map_mname(int, int, const char *, char *)
*   - Maps family and model to processor name
*
****************************************************/

void map_mname(int family, int model, const char *v_name, char *m_name) {
	// Default to name not known
	m_name[0] = '\0';

	if (!strncmp("AuthenticAMD", v_name, 12)) {
		switch (family) { // extract family code
			case 4: // Am486/AM5x86
				strcpy_s(m_name, _MAX_MNAME_LEN, "AMD Am486");
				break;

			case 5: // K6
				switch (model) { // extract model code
					case 0:
					case 1:
					case 2:
					case 3:
						strcpy_s(m_name, _MAX_MNAME_LEN, "AMD K5");
						break;
					case 6:
					case 7:
						strcpy_s(m_name, _MAX_MNAME_LEN, "AMD K6");
						break;
					case 8:
						strcpy_s(m_name, _MAX_MNAME_LEN, "AMD K6-2");
						break;
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
					case 15:
						strcpy_s(m_name, _MAX_MNAME_LEN, "AMD K6-3");
						break;
				}
				break;

			case 6: // Athlon
				// No model numbers are currently defined
				strcpy_s(m_name, _MAX_MNAME_LEN, "AMD ATHLON");
				break;
		}
	} else if (!strncmp("GenuineIntel", v_name, 12)) {
		switch (family) { // extract family code
			case 4:
				switch (model) { // extract model code
					case 0:
					case 1:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL 486DX");
						break;
					case 2:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL 486SX");
						break;
					case 3:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL 486DX2");
						break;
					case 4:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL 486SL");
						break;
					case 5:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL 486SX2");
						break;
					case 7:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL 486DX2E");
						break;
					case 8:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL 486DX4");
						break;
				}
				break;

			case 5:
				switch (model) { // extract model code
					case 1:
					case 2:
					case 3:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL Pentium");
						break;
					case 4:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL Pentium-MMX");
						break;
				}
				break;

			case 6:
				switch (model) { // extract model code
					case 1:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL Pentium-Pro");
						break;
					case 3:
					case 5:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL Pentium-II");
						break; // actual differentiation depends on cache settings
					case 6:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL Celeron");
						break;
					case 7:
					case 8:
					case 10:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL Pentium-III");
						break; // actual differentiation depends on cache settings
				}
				break;

			case 15 | (0x00 << 4): // family 15, extended family 0x00
				switch (model) {
					case 0:
						strcpy_s(m_name, _MAX_MNAME_LEN, "INTEL Pentium-4");
						break;
				}
				break;
		}
	} else if (!strncmp("CyrixInstead", v_name, 12)) {
		strcpy_s(m_name, _MAX_MNAME_LEN, "Cyrix");
	} else if (!strncmp("CentaurHauls", v_name, 12)) {
		strcpy_s(m_name, _MAX_MNAME_LEN, "Centaur");
	}

	if (!m_name[0]) {
		strcpy_s(m_name, _MAX_MNAME_LEN, "Unknown");
	}
}

/***
*
* int _cpuid (_p_info *pinfo)
*
* Entry:
*
*   pinfo: pointer to _p_info.
*
* Exit:
*
*   Returns int with capablity bit set even if pinfo = NULL
*
****************************************************/

int _cpuid(_p_info *pinfo) {
	DWORD dwStandard = 0;
	DWORD dwFeature = 0;
	DWORD dwMax = 0;
	DWORD dwExt = 0;
	int feature = 0;
	int os_support = 0;
	union {
		char cBuf[12 + 1];
		struct {
			DWORD dw0;
			DWORD dw1;
			DWORD dw2;
		} s;
	} Ident;

	if (!IsCPUID()) {
		return 0;
	}

	_asm {
        push ebx
        push ecx
        push edx

			// get the vendor string
        xor eax, eax
        cpuid
        mov dwMax, eax
        mov Ident.s.dw0, ebx
        mov Ident.s.dw1, edx
        mov Ident.s.dw2, ecx

			// get the Standard bits
        mov eax, 1
        cpuid
        mov dwStandard, eax
        mov dwFeature, edx

			// get AMD-specials
        mov eax, 80000000h
        cpuid
        cmp eax, 80000000h
        jc notamd
        mov eax, 80000001h
        cpuid
        mov dwExt, edx

notamd:
        pop ecx
        pop ebx
        pop edx
	}

	if (dwFeature & _MMX_FEATURE_BIT) {
		feature |= _CPU_FEATURE_MMX;
		if (_os_support(_CPU_FEATURE_MMX))
			os_support |= _CPU_FEATURE_MMX;
	}
	if (dwExt & _3DNOW_FEATURE_BIT) {
		feature |= _CPU_FEATURE_3DNOW;
		if (_os_support(_CPU_FEATURE_3DNOW))
			os_support |= _CPU_FEATURE_3DNOW;
	}
	if (dwFeature & _SSE_FEATURE_BIT) {
		feature |= _CPU_FEATURE_SSE;
		if (_os_support(_CPU_FEATURE_SSE))
			os_support |= _CPU_FEATURE_SSE;
	}
	if (dwFeature & _SSE2_FEATURE_BIT) {
		feature |= _CPU_FEATURE_SSE2;
		if (_os_support(_CPU_FEATURE_SSE2))
			os_support |= _CPU_FEATURE_SSE2;
	}

	if (pinfo) {
		memset(pinfo, 0, sizeof(_p_info));

		pinfo->os_support = os_support;
		pinfo->feature = feature;
		pinfo->family = (dwStandard >> 8) & 0xF; // retrieve family
		if (pinfo->family == 15) { // retrieve extended family
			pinfo->family |= (dwStandard >> 16) & 0xFF0;
		}
		pinfo->model = (dwStandard >> 4) & 0xF; // retrieve model
		if (pinfo->model == 15) { // retrieve extended model
			pinfo->model |= (dwStandard >> 12) & 0xF;
		}
		pinfo->stepping = (dwStandard)&0xF; // retrieve stepping

		Ident.cBuf[12] = 0;
		strcpy_s(pinfo->v_name, _MAX_VNAME_LEN, Ident.cBuf);

		map_mname(pinfo->family,
			pinfo->model,
			pinfo->v_name,
			pinfo->model_name);

		pinfo->checks = _CPU_FEATURE_MMX |
						_CPU_FEATURE_SSE |
						_CPU_FEATURE_SSE2 |
						_CPU_FEATURE_3DNOW;
	}

	return feature;
}

} // namespace KEP
