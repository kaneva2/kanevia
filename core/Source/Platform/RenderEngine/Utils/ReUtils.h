///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <dxerr9.h>
#include "assert.h"

#include "common\include\LogHelper.h"

namespace KEP {

inline void ReportD3DError(HRESULT hr, int line, char* func) {
	static LogInstance("Instance");
	const char* desc = DXGetErrorDescription9A(hr);
	std::string errMsg = desc ? desc : "<null>";
	LogError("D3DERROR - " << func << ":" << line << " '" << errMsg << "'");
}

#if defined(_DEBUG)
#define D3DCheck(hr) _D3DCheck(hr, __LINE__, __FUNCTION__)
#else
#define D3DCheck(hr) hr
#endif
inline HRESULT _D3DCheck(HRESULT hr, int line, char* func) {
	if (FAILED(hr))
		ReportD3DError(hr, line, func);
	return hr;
}

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

// HASHING SUPPORT
#define ReHashMix(a, b, c) \
	{                      \
		a = a - b;         \
		a = a - c;         \
		a = a ^ (c >> 13); \
		b = b - c;         \
		b = b - a;         \
		b = b ^ (a << 8);  \
		c = c - a;         \
		c = c - b;         \
		c = c ^ (b >> 13); \
		a = a - b;         \
		a = a - c;         \
		a = a ^ (c >> 12); \
		b = b - c;         \
		b = b - a;         \
		b = b ^ (a << 16); \
		c = c - a;         \
		c = c - b;         \
		c = c ^ (b >> 5);  \
		a = a - b;         \
		a = a - c;         \
		a = a ^ (c >> 3);  \
		b = b - c;         \
		b = b - a;         \
		b = b ^ (a << 10); \
		c = c - a;         \
		c = c - b;         \
		c = c ^ (b >> 15); \
	}

DWORD ReHash(BYTE* k, DWORD length, DWORD initval);

// CPU FEATURE EXTRACTION SUPPORT
#define _CPU_FEATURE_MMX 0x0001
#define _CPU_FEATURE_SSE 0x0002
#define _CPU_FEATURE_SSE2 0x0004
#define _CPU_FEATURE_3DNOW 0x0008

#define _MAX_VNAME_LEN 13
#define _MAX_MNAME_LEN 30

typedef struct _processor_info {
	char v_name[_MAX_VNAME_LEN]; // vendor name
	char model_name[_MAX_MNAME_LEN]; // name of model
		// e.g. Intel Pentium-Pro
	int family; // family of the processor
		// e.g. 6 = Pentium-Pro architecture
	int model; // model of processor
		// e.g. 1 = Pentium-Pro for family = 6
	int stepping; // processor revision number
	int feature; // processor feature
		// (same as return value from _cpuid)
	int os_support; // does OS Support the feature?
	int checks; // mask of checked bits in feature
		// and os_support fields
} _p_info;

int _cpuid(_p_info*);

#ifdef __cplusplus
}
#endif /* __cplusplus */

} // namespace KEP
