///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <js.h>

#include "d3dx9math.h"
#include "d3d9types.h"
#include "dxerr9.h"
#include "stdio.h"
#include "ReMaterial.h"
#include "ReD3DX9Shader.h"
#include "Core/Util/fast_mutex.h"
#include "Core/Util/Memory.h"
#include "ReMeshPrimType.h"

namespace KEP {
class ITextureProxy;
class IMemSizeGadget;

enum class ReD3DX9HardwareCaps {
	CPU_FEATURE_NONE,
	CPU_FEATURE_3DNOW,
	CPU_FEATURE_MMX,
	CPU_FEATURE_SSE,
	CPU_FEATURE_SSE2,
	GPU_FEATURE_NONE,
	GPU_FEATURE_1_1,
	GPU_FEATURE_2_0,
	GPU_FEATURE_2_1,
	GPU_FEATURE_3_1,
};

#define ReD3DX9MINVERTEXSHADERSUPPORT ReD3DX9HardwareCaps::GPU_FEATURE_2_0 // minimum vertex shader support required
#define ReD3DX9MINVERTEXSHADERCONSTANTS 256 // minimum vertex shader constants required

enum class ReD3DX9VertexType {
	POS_WTX,
	POS_NORM_UV1,
	POS_NORM_UV2,
	POS_NORM_UV3,
	POS_NORM_UV4,
	POS_NORM_UV1D,
	POS_NORM_UV2D,
	POS_NORM_UV3D,
	POS_NORM_UV4D,
	POS_NORM_WTXUV1,
	POS_NORM_WTXUV2,
	POS_NORM_WTXUV3,
	POS_NORM_WTXUV4,
	POS_NORM_UV1_DS,
	POS_NORM_UV2_DS,
	POS_NORM_UV3_DS,
	POS_NORM_UV4_DS,
	INVALID,
};

enum class ReD3DX9ResourceType {
	VERTEX_BUFFER,
	INDEX_BUFFER,
	STATE_BLOCK,
	INDEX_BUFFER_TRIS,
	VERTEX_BUFFER_DISCARD,
	VERTEX_DECLARATION,
	EFFECT,
	INVALID,
};

#define RED3DX9RESOURCE_STATIC 0x01
#define RED3DX9RESOURCE_DYNAMIC 0x02
#define RED3DX9RESOURCE_SYSTEM 0x04
#define RED3DX9RESOURCE_ALL 0x07

struct ReD3DX9Resource {
	void* resource;
	UINT* owner;
};

struct ReD3DX9ResourceSystem {
	void* resource;
	ReD3DX9ResourceType resType;
	UINT64 resHash;
	UINT** resOwners;
	DWORD resNumowners;
};

// Fixedfunction pipe light cache
struct _ReD3DX9Light {
	D3DXVECTOR4 Position;
	D3DXVECTOR4 Diffuse;
	D3DXVECTOR4 Specular;
	D3DXVECTOR4 Attenuation;
	D3DXVECTOR4 Ambient;
	D3DLIGHT9 Light;
};

// Fixedfunction pipe light cache
typedef __declspec(align(32)) _ReD3DX9Light ReD3DX9Light;
typedef __declspec(align(32)) D3DXVECTOR4 D3DXVECTORA4;

struct ReOutputVertex0_D3DX9 // CPU skinned or GPU static
{
	FLOAT X, Y, Z;
	FLOAT NX, NY, NZ;
	FLOAT U, V; // 32 bytes
};

struct ReOutputVertex1_D3DX9 // GPU skinned & lit
{
	FLOAT X, Y, Z;
	FLOAT NX, NY, NZ;
	FLOAT U, V;
	DWORD Bx;
	FLOAT Wt[4];
};

struct ReOutputVertex2_D3DX9 // GPU skinned & unlit
{
	FLOAT X, Y, Z;
	DWORD BoneIndices;
	FLOAT W[4]; // 32 bytes
};

struct ReOutputVertex3_D3DX9 // CPU skinned or GPU static multitextured
{
	FLOAT X, Y, Z;
	FLOAT NX, NY, NZ;
	FLOAT U0, V0;
	FLOAT U1, V1;
};

struct ReOutputVertex4_D3DX9 // CPU skinned or GPU static multitextured
{
	FLOAT X, Y, Z;
	FLOAT NX, NY, NZ;
	FLOAT U0, V0;
	FLOAT U1, V1;
	FLOAT U2, V2;
};

struct ReOutputVertex5_D3DX9 // CPU skinned or GPU static multitextured
{
	FLOAT X, Y, Z;
	FLOAT NX, NY, NZ;
	FLOAT U0, V0;
	FLOAT U1, V1;
	FLOAT U2, V2;
	FLOAT U3, V3;
};

struct ReOutputVertex6_D3DX9 // CPU static prelit
{
	FLOAT X, Y, Z;
	DWORD Diff;
	FLOAT U, V;
	DWORD Pad[2];
};

struct ReOutputVertex7_D3DX9 // CPU static prelit multitex
{
	FLOAT X, Y, Z;
	DWORD Diff;
	FLOAT U0, V0;
	FLOAT U1, V1;
};

struct ReOutputVertex8_D3DX9 // CPU static prelit multitex
{
	FLOAT X, Y, Z;
	DWORD Diff;
	FLOAT U0, V0;
	FLOAT U1, V1;
	FLOAT U2, V2;
};

struct ReOutputVertex9_D3DX9 // CPU static prelit multitex
{
	FLOAT X, Y, Z;
	DWORD Diff;
	FLOAT U0, V0;
	FLOAT U1, V1;
	FLOAT U2, V2;
	FLOAT U3, V3;
};

class ReD3DX9DeviceRenderState {
public:
	ReD3DX9DeviceRenderState();

	virtual ~ReD3DX9DeviceRenderState() {}

	void SetDevice(LPDIRECT3DDEVICE9 pd3dDevice) {
		if (m_pD3DDevice != pd3dDevice) {
			m_pD3DDevice = pd3dDevice;
			SetHardwareCaps();
		}
		Update();
	}

	void Update() {
		if (m_pD3DDevice) {
			m_pD3DDevice->GetTransform(D3DTS_VIEW, &m_ViewMatrix);
			m_pD3DDevice->GetTransform(D3DTS_PROJECTION, &m_ProjMatrix);
			D3DXMatrixMultiply(&m_ProjViewMatrix, &m_ViewMatrix, &m_ProjMatrix);
		}
	}

	void SetViewMatrix(const D3DMATRIX* pView) {
		memcpy((void*)&m_ViewMatrix, (void*)pView, sizeof(D3DMATRIX));
		D3DXMatrixMultiply(&m_ProjViewMatrix, &m_ViewMatrix, &m_ProjMatrix);
	}

	void SetProjMatrix(const D3DMATRIX* pProj) {
		memcpy((void*)&m_ProjMatrix, (void*)pProj, sizeof(D3DMATRIX));
		D3DXMatrixMultiply(&m_ProjViewMatrix, &m_ViewMatrix, &m_ProjMatrix);
	}

	void SetProjViewMatrix(const D3DMATRIX* pView, const D3DMATRIX* pProj) {
		memcpy((void*)&m_ViewMatrix, (void*)pView, sizeof(D3DMATRIX));
		memcpy((void*)&m_ProjMatrix, (void*)pProj, sizeof(D3DMATRIX));
		D3DXMatrixMultiply(&m_ProjViewMatrix, &m_ViewMatrix, &m_ProjMatrix);
	}

	D3DXMATRIX* GetWorldMatrix() {
		m_pD3DDevice->GetTransform(D3DTS_WORLD, &m_WorldMatrix);
		return &m_WorldMatrix;
	}

	void SetWorldMatrix(D3DXMATRIX* pMat) {
		m_pD3DDevice->SetTransform(D3DTS_WORLD, pMat);
	}

	void SetCamMatrix(D3DMATRIX* pMat) {
		memcpy((void*)&m_CamMatrix, (void*)pMat, sizeof(D3DMATRIX));
	}

	/************** RUNTIME LIGHT CACHE API ************************
	*
	*		for (each frame)
	*			FlushLightCache()
	*
	*			for (all lights in level)
	*				LoadLight(light)....NOTE: vertex shader light upload
	*									format assumes 1 directional sunlight
	*									is loaded first, followed by 3 point
	*									lights.
	*
	*			for (each lit object at "pos")
	*				SetMaxLights(max)
	*				SortLights(pos) or SetLights(light cache)
	*				RenderLights(Shader)
	*				(..render object geometry)
	*
	*************** RUNTIME LIGHT CACHE API ***********************/
	void FlushLightCache();
	void LoadLight(const D3DLIGHT9* light);
	void SortLights(const D3DXVECTOR3* pos, FLOAT radius, ReLightCache* lightCache = NULL);
	void SetLights(const ReLightCache* lightCache);
	void RenderLights(BOOL shader);

	void PrintSortedLights() {
		char buf[256];
		for (DWORD i = 0; i < m_NumSortedLights; i++) {
			_snprintf_s(&buf[0], _countof(buf), _TRUNCATE, " %f ", m_LightSort[i]->Attenuation.w);
			OutputDebugStringA(&buf[0]);
		}
		_snprintf_s(&buf[0], _countof(buf), _TRUNCATE, " \n ");
		OutputDebugStringA(&buf[0]);
	}

	void PrintCachedLights() {
		char buf[256];
		for (DWORD i = 0; i < m_LightCache.size(); i++) {
			_snprintf_s(&buf[0], _countof(buf), _TRUNCATE, " %d ", (DWORD)D3DXVec4Length(&m_LightCache[i].Position));
			OutputDebugStringA(&buf[0]);
		}
		_snprintf_s(&buf[0], _countof(buf), _TRUNCATE, " \n ");
		OutputDebugStringA(&buf[0]);
	}

private:
	static const UINT FIXEDFUNCLIGHTINDEX = 0;
	static const UINT SHADERLIGHTINDEX = 1;

public:
	const LPDIRECT3DDEVICE9 GetDevice() const {
		return m_pD3DDevice;
	}

	const D3DXMATRIXA16* GetViewMatrix() const {
		return &m_ViewMatrix;
	}

	const D3DXMATRIXA16* GetProjMatrix() const {
		return &m_ProjMatrix;
	}

	const D3DXMATRIXA16* GetProjView() const {
		return &m_ProjViewMatrix;
	}

	const D3DXMATRIXA16* GetWorldMatrix() const {
		return &m_WorldMatrix;
	}

	const D3DXMATRIXA16* GetCamMatrix() const {
		return &m_CamMatrix;
	}

	const ReD3DX9Light* GetLight(DWORD ix) const {
		return m_LightSort[ix];
	}

	const D3DXVECTOR4* GetGlobalAmbient() const {
		return &m_GlobalAmbient;
	}

	const D3DXVECTOR4* GetFogColor() const {
		return &m_FogColor;
	}

	const D3DXVECTOR4* GetFogRange() const {
		return &m_FogRange;
	}

	const D3DXVECTOR4* GetCamPosition() const {
		return (D3DXVECTOR4*)&m_CamMatrix.m[3][0];
	}

	const D3DXVECTOR4* GetLightAmbient() const {
		return (D3DXVECTOR4*)m_LightAmbient;
	}

	const D3DXVECTOR4* GetLightDiffuse() const {
		return (D3DXVECTOR4*)m_LightDiffuse;
	}

	const D3DXVECTOR4* GetLightDirection() const {
		return (D3DXVECTOR4*)m_LightDirection;
	}

	const D3DXVECTOR4* GetLightPosition() const {
		return (D3DXVECTOR4*)m_LightPosition;
	}

	const D3DXVECTOR4* GetLightSpecular() const {
		return (D3DXVECTOR4*)m_LightSpecular;
	}

	const FLOAT* GetLightAttenuation0() const {
		return m_LightAttenuation0;
	}

	const FLOAT* GetLightAttenuation1() const {
		return m_LightAttenuation1;
	}

	const FLOAT* GetLightAttenuation2() const {
		return m_LightAttenuation2;
	}

	const FLOAT* GetLightRange() const {
		return m_LightRange;
	}

	const DWORD GetMaxConstants() const {
		return m_MaxConstants;
	}

	const ReD3DX9HardwareCaps GetHardwareCaps() const {
		return m_HWCaps;
	}

	const DWORD GetMaxLights() const {
		return m_MaxLights;
	}

	const ReRenderMode GetRenderMode() const {
		return m_RenderMode;
	}

	const DWORD GetNumActiveLights() const {
		return m_LightSort.size();
	}

	const DWORD GetHardwareRating() const {
		return m_HWRating;
	}

	const BOOL GetPreLighting() const {
		return m_PreLighting;
	};

	const void SetMaxLights(DWORD max) {
		m_MaxUploadedLights = max > ReD3DX9DEVICEMAXUPLOADLIGHTS ? ReD3DX9DEVICEMAXUPLOADLIGHTS : max;
	}

	const void SetGlobalAmbient(D3DXVECTOR4* amb) {
		m_GlobalAmbient = *amb;
	}

	const void SetFogColor(D3DXVECTOR4* color) {
		m_FogColor = *color;
	}

	const void SetFogRange(D3DXVECTOR4* range) {
		m_FogRange = *range;
	}

	const void SetRenderMode(ReRenderMode mode) {
		m_RenderMode = mode;
	}

	const void SetPreLighting(const BOOL val) {
		m_PreLighting = val;
	}

	const void SetHardwareCaps();

private:
	LPDIRECT3DDEVICE9 m_pD3DDevice;
	D3DXMATRIXA16 m_WorldMatrix;
	D3DXMATRIXA16 m_ViewMatrix;
	D3DXMATRIXA16 m_ProjMatrix;
	D3DXMATRIXA16 m_ProjViewMatrix;
	D3DXMATRIXA16 m_CamMatrix;
	std::vector<ReD3DX9Light> m_LightCache;
	std::vector<ReD3DX9Light*> m_LightSort;
	D3DXVECTORA4 m_GlobalAmbient;
	D3DXVECTORA4 m_FogColor;
	D3DXVECTORA4 m_FogRange;
	D3DXVECTORA4 m_LightAmbient[ReD3DX9DEVICEMAXGPULIGHTS];
	D3DXVECTORA4 m_LightDiffuse[ReD3DX9DEVICEMAXGPULIGHTS];
	D3DXVECTORA4 m_LightDirection[ReD3DX9DEVICEMAXGPULIGHTS];
	D3DXVECTORA4 m_LightPosition[ReD3DX9DEVICEMAXGPULIGHTS];
	D3DXVECTORA4 m_LightSpecular[ReD3DX9DEVICEMAXGPULIGHTS];
	FLOAT m_LightAttenuation0[ReD3DX9DEVICEMAXGPULIGHTS];
	FLOAT m_LightAttenuation1[ReD3DX9DEVICEMAXGPULIGHTS];
	FLOAT m_LightAttenuation2[ReD3DX9DEVICEMAXGPULIGHTS];
	FLOAT m_LightRange[ReD3DX9DEVICEMAXGPULIGHTS];
	DWORD m_MaxConstants;
	DWORD m_MaxLights;
	DWORD m_MaxUploadedLights;
	DWORD m_MaxPreLitLights;
	DWORD m_NumSortedLights;
	ReD3DX9HardwareCaps m_HWCaps;
	ReRenderMode m_RenderMode;
	DWORD m_HWRating;
	BOOL m_PreLighting;
};

class ReD3DX9DeviceState : public AlignedOperatorNewBase<16> {
	friend class ReD3DX9Mesh;
	friend class ReD3DX9Shader;

public:
	ReD3DX9DeviceState();

	virtual ~ReD3DX9DeviceState() {
		Destroy();
	}

	static ReD3DX9DeviceState* Instance() {
		return ms_instance;
	}

	BOOL Initialize(UINT maxDynamic = ReD3DX9DEFAULTNUMRESDYNAMIC, UINT maxStatic = ReD3DX9DEFAULTNUMRESSTATIC, UINT maxSystem = ReD3DX9DEFAULTNUMRESSYSTEM);

	void Destroy();

	ReD3DX9DeviceRenderState* GetRenderState() {
		return &m_DeviceRenderState;
	}

	LPDIRECT3DDEVICE9 GetDevice() {
		return m_DeviceRenderState.GetDevice();
	}

	void ResetShaderState() {
		m_ConstantBlockCurr = ReD3DX9DEVICEINVALID;
	}

	void SetD3dDevice(LPDIRECT3DDEVICE9 pD3dDevice) {
		if (GetDevice() != pD3dDevice)
			InvalidateResources();
		m_DeviceRenderState.SetDevice(pD3dDevice);
	}

	BOOL VertexDeclRender(UINT64 val) {
		if (m_VertexDeclCurr != val) {
			m_VertexDeclCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL IndexBufferRender(DWORD val) {
		if (m_IndexBufferCurr != val) {
			m_IndexBufferCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL VertexBufferRender(UINT64 val) {
		if (m_VertexBufferCurr != val) {
			m_VertexBufferCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL VertexShaderRender(DWORD val) {
		if (m_VertexShaderCurr != val) {
			m_VertexShaderCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL StateBlockRender(DWORD val) {
		if (m_StateBlockCurr != val) {
			m_StateBlockCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL ConstantBlockRender(DWORD val) {
		if (m_ConstantBlockCurr != val) {
			m_ConstantBlockCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL MatStateRender(DWORD val) {
		if (m_MatStateCurr != val) {
			m_MatStateCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL BlendStateRender(DWORD val) {
		if (m_BlendStateCurr != val) {
			m_BlendStateCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL ModeStateRender(DWORD val) {
		if (m_ModeStateCurr != val) {
			m_ModeStateCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL ColorSourceRender(DWORD val) {
		if (m_ColorSourceCurr != val) {
			m_ColorSourceCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL TexStateRender(DWORD val, DWORD i) {
		if (m_TexStateCurr[i] != val) {
			m_TexStateCurr[i] = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL TexRender(UINT64 val, DWORD i) {
		if (m_TexCurr[i] != val) {
			m_TexCurr[i] = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL TextMatRender(UINT64 val, DWORD i) {
		if (m_TexMatCurr[i] != val) {
			m_TexMatCurr[i] = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL WorldMatrixRender(const D3DXMATRIX* val) {
		if (m_WorldMatrixCurr != val) {
			m_WorldMatrixCurr = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL WorldMatArrayRender() {
		if (m_WorldMatArrayCurr) {
			m_WorldMatArrayCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL WorldMatRender() {
		if (m_WorldMatCurr) {
			m_WorldMatCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL ViewMatRender() {
		if (m_ViewMatCurr) {
			m_ViewMatCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL ProjMatRender() {
		if (m_ProjMatCurr) {
			m_ProjMatCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL ProjViewMatRender() {
		if (m_ProjViewMatCurr) {
			m_ProjViewMatCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL CamPosRender() {
		if (m_CamPosCurr) {
			m_CamPosCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL GlobalAmbnRender() {
		if (m_GlobalAmbnCurr) {
			m_GlobalAmbnCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightAmbnRender() {
		if (m_LightAmbnCurr) {
			m_LightAmbnCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightDiffRender() {
		if (m_LightDiffCurr) {
			m_LightDiffCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightSpecRender() {
		if (m_LightSpecCurr) {
			m_LightSpecCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightPosnRender() {
		if (m_LightPosnCurr) {
			m_LightPosnCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightDirnRender() {
		if (m_LightDirnCurr) {
			m_LightDirnCurr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightAtt0Render() {
		if (m_LightAtt0Curr) {
			m_LightAtt0Curr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightAtt1Render() {
		if (m_LightAtt1Curr) {
			m_LightAtt1Curr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL LightAtt2Render() {
		if (m_LightAtt2Curr) {
			m_LightAtt2Curr = 0;
			return TRUE;
		}
		return FALSE;
	}

	BOOL TextureConstantChanged(DWORD val) {
		if (m_lastTextureConstant != val) {
			m_lastTextureConstant = val;
			return TRUE;
		}
		return FALSE;
	}

	BOOL TextureFactorChanged(DWORD val) {
		if (m_lastTextureFactor != val) {
			m_lastTextureFactor = val;
			return TRUE;
		}
		return FALSE;
	}

	void FlushVertexBufferCache() {
		m_VertexBufferCurr = 0;
	}

	void FlushIndexBufferCache() {
		m_IndexBufferCurr = 0;
	}

	void FlushVertexShaderCache() {
		m_VertexShaderCurr = 0;
	}

	void ResetRenderState();
	void SetNumTextureStages(UINT numStages);
	void ResetTextureState();
	void ResetEffectState();
	void ResetEffectObjectState();
	void InvalidateResources(UINT type = RED3DX9RESOURCE_ALL, bool resetArray = true);
	bool GrowResourceArray(DWORD type);

	// Wrapper for selected D3D functions
	HRESULT SetRenderState(D3DRENDERSTATETYPE state, DWORD value);

private:
	BOOL CreateStaticResource(ReD3DX9ResourceType resType, ReD3DX9VertexType vertexType, UINT size, void** res, UINT* id);
	BOOL CreateDynamicResource(ReD3DX9ResourceType resType, ReD3DX9VertexType vertexType, UINT size, void** res, UINT* id);
	BOOL CreateSystemResource(ReD3DX9ResourceType resType, ReD3DX9VertexType vertexType, UINT64* resHash, void** out, UINT* id, const char* code = NULL);
	BOOL GetDuplicateSystemResource(ReD3DX9ResourceType resType, UINT64* resHash, void** out, UINT* id);
	void InvalidateResource(UINT id);
	void InvalidateResource(UINT* id);

private:
	fast_recursive_mutex m_sync; /// Added to synchronize manipulation of m_Resource (e.g. in CreateDynamicResource()).
	ReD3DX9Resource* m_Resource;
	ReD3DX9ResourceSystem* m_ResourceSystem;

	ReD3DX9DeviceRenderState m_DeviceRenderState;
	DWORD m_NumStaticResources;
	DWORD m_NumDynamicResources;
	DWORD m_NumSystemResources;
	DWORD m_NumShaderStates;
	UINT64 m_VertexBufferCurr;
	DWORD m_IndexBufferCurr;
	DWORD m_VertexShaderCurr;
	UINT64 m_VertexDeclCurr;
	DWORD m_StateBlockCurr;
	DWORD m_ConstantBlockCurr;
	DWORD m_ModeStateCurr;
	DWORD m_ColorSourceCurr;
	DWORD m_MatStateCurr;
	DWORD m_BlendStateCurr;
	DWORD m_TexStateCurr[ReDEVICEMAXTEXTURESTAGES];
	UINT64 m_TexCurr[ReDEVICEMAXTEXTURESTAGES];
	UINT64 m_TexMatCurr[ReDEVICEMAXTEXTURESTAGES];
	unsigned m_NumTexStages;
	const D3DXMATRIX* m_WorldMatrixCurr;
	BOOL m_WorldMatArrayCurr;
	BOOL m_WorldMatCurr;
	BOOL m_ViewMatCurr;
	BOOL m_ProjMatCurr;
	BOOL m_ProjViewMatCurr;
	BOOL m_CamPosCurr;
	BOOL m_GlobalAmbnCurr;
	BOOL m_LightAmbnCurr;
	BOOL m_LightDiffCurr;
	BOOL m_LightSpecCurr;
	BOOL m_LightPosnCurr;
	BOOL m_LightDirnCurr;
	BOOL m_LightAtt0Curr;
	BOOL m_LightAtt1Curr;
	BOOL m_LightAtt2Curr;

	UINT m_maxResourcesdynamicInit; // Initial array sizes (static and dynamic)
	UINT m_maxResourcesstaticInit;
	UINT m_maxResourcesdynamic; // Current array sizes (to be reverted to initial value if entire array is invalidated)
	UINT m_maxResourcesstatic;
	UINT m_maxResourcessystem;

	DWORD m_lastTextureConstant; // D3DTSS_CONSTANT
	DWORD m_lastTextureFactor; // D3DRS_TEXTUREFACTOR

	static ReD3DX9DeviceState* ms_instance;
};

class ReD3DX9RenderState : public ReRenderState {
public:
	ReD3DX9RenderState();

	ReD3DX9RenderState(const void* dev, const ReRenderStateData* state);

	ReD3DX9RenderState(const ReD3DX9RenderState& state);

	virtual ~ReD3DX9RenderState() {}

	virtual void PreRender() override final;

	virtual void Render() override final;

	virtual void PostRender() override final;

	virtual void Update(ReRenderStateData* data) override final {
		m_RenderStateData = *data;
	}

	virtual BOOL EqualTo(const ReRenderState* s) const override final {
		return m_RenderStateData.SortOrder == ((ReD3DX9RenderState*)s)->m_RenderStateData.SortOrder;
	}

	virtual INT Compare(const ReRenderState* s) const override final {
		if (EqualTo(s))
			return 0;
		return (m_RenderStateData.SortOrder < ((ReD3DX9RenderState*)s)->m_RenderStateData.SortOrder ? -1 : 1);
	}

	virtual BOOL Invalid() const override final {
		return m_RenderStateData.BlendHash == 0;
	}

	virtual ReD3DX9RenderState* Clone() const override final {
		return new ReD3DX9RenderState(*this);
	}

	virtual void Update(std::shared_ptr<void> tex, UINT stage = 0) override final {
		m_RenderStateData.TexPtr[stage] = std::move(tex);
	}

	void SetTextureMatrix(UINT stage, D3DXMATRIX* pMatrix);

	ReRenderStateData* GetRenderStateDataNonVirtual() {
		return &m_RenderStateData;
	}

protected:
	ReD3DX9DeviceState* m_pDev;
	ReRenderStateData m_RenderStateData;
};

class ReD3DX9Material : public ReMaterial {
public:
	ReD3DX9Material(const void* dev, const ReRenderStateData* data);

	ReD3DX9Material(const ReD3DX9Material& mat);

	virtual ~ReD3DX9Material();

	virtual void PreRender() override {
		m_State.PreRender();
	}

	virtual void Render() override {
		m_State.Render();
	}

	virtual void PostRender() override {
		m_State.PostRender();
	}

	virtual void Update(ReRenderStateData* data) override {
		m_State.Update(data);
	}

	virtual void Update() override {
	}

	virtual void Update(std::shared_ptr<void> texture, DWORD stage) override {
		m_State.Update(std::move(texture), stage);
	}

	virtual void Update(ReShaderData* /*data*/) override {
	}

	virtual const ReRenderState* GetRenderState() const override {
		return &m_State;
	}

	virtual BOOL EqualTo(ReMaterialPtr mat) const override {
		return m_State.EqualTo(mat->GetRenderState());
	}

	virtual BOOL Invalid() const override {
		return m_State.Invalid();
	}

	virtual ReMaterialPtr Clone() const override {
		return ReMaterialPtr(new ReD3DX9Material(*this));
	}

	virtual void PreLight(BOOL val) override {
		ReRenderStateData data = *m_State.GetData();
		data.ColorSrc = val ? ReColorSource::VERTEX : ReColorSource::MATERIAL;
		Update(&data);
	}

	virtual const BOOL PreLit() const override {
		return m_State.GetData()->ColorSrc == ReColorSource::VERTEX;
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override;

private:
	ReD3DX9DeviceState* m_pDev;
	ReD3DX9RenderState m_State;
};

class ReD3DX9Material_TexMat : public AlignedOperatorNewBase<16>, public ReMaterial_TexMat {
public:
	ReD3DX9Material_TexMat();

	ReD3DX9Material_TexMat(const void* dev, const ReRenderStateData* data, const FLOAT* uvIncr);

	ReD3DX9Material_TexMat(const ReD3DX9Material_TexMat& mat);

	virtual ~ReD3DX9Material_TexMat();

	virtual void PreRender() override;

	virtual void Render() override {
		m_State.Render();
	}

	virtual void PostRender() override;

	virtual void Update(ReRenderStateData* data) override {
		m_State.Update(data);
	}

	virtual void Update() override;

	virtual void Update(std::shared_ptr<void> tex, DWORD stage) override {
		m_State.Update(std::move(tex), stage);
	}

	virtual void Update(ReShaderData* /*data*/) override {
	}

	virtual const ReRenderState* GetRenderState() const override {
		return &m_State;
	}

	virtual BOOL EqualTo(ReMaterialPtr mat) const override {
		return m_State.EqualTo(mat->GetRenderState());
	}

	virtual BOOL Invalid() const override {
		return m_State.Invalid();
	}

	virtual ReMaterialPtr Clone() const override {
		return ReMaterialPtr(new ReD3DX9Material_TexMat(*this));
	}

	virtual void PreLight(BOOL val) override {
		ReRenderStateData data = *m_State.GetData();
		data.ColorSrc = val ? ReColorSource::VERTEX : ReColorSource::MATERIAL;
		Update(&data);
	}

	virtual const BOOL PreLit() const override {
		return m_State.GetData()->ColorSrc == ReColorSource::VERTEX;
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override;

private:
	ReD3DX9DeviceState* m_pDev;
	ReD3DX9RenderState m_State;
	D3DXMATRIXA16 m_TexMat[ReDEVICEMAXTEXTURESTAGES];

	D3DXVECTOR2 m_UVIncr[2]; // texture movement vector (per frame)
};

class ReD3DX9Mesh {
protected:
	ReD3DX9Mesh();

	ReD3DX9Mesh(ReD3DX9DeviceState* dev);

	virtual ~ReD3DX9Mesh() {
		Invalidate();
	}

	void Render(DWORD numV, DWORD numI, ReMeshPrimType primType, BOOL indexed = TRUE);

	void Invalidate();

	BOOL Lock(D3DXVECTOR3* pos, DWORD numV);

	void Reload(D3DXVECTOR3* pPos, D3DXVECTOR3* pNorm, DWORD numV);

	BOOL Restore(D3DXVECTOR3* pPos, D3DXVECTOR3* pNorm, D3DXVECTOR2* pUv, WORD* pI, DWORD numV, DWORD numI, ReMeshPrimType primType);

	BOOL Restore(D3DXVECTOR3* pPos, D3DXVECTOR3* pNorm, D3DXVECTOR2* pUv, WORD* pI, BYTE* pBoneIx, float* pWt, DWORD wpv, DWORD numV, DWORD numI, ReMeshPrimType primType);

	BOOL Invalid() const {
		return m_ResVD == ReD3DX9DEVICEINVALID || m_ResVB == ReD3DX9DEVICEINVALID;
	}

	void Hash(UINT64 hash) {
		m_HashIB = (DWORD)(hash >> 32);
		m_HashVB = hash;
	}

	void PreRender() {
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	CRITICAL_SECTION m_cs;
	ReD3DX9DeviceState* m_pDev;
	LPDIRECT3DVERTEXBUFFER9 m_pVB;
	LPDIRECT3DINDEXBUFFER9 m_pIB;
	LPDIRECT3DVERTEXDECLARATION9 m_pVD;
	ReD3DX9VertexType m_VertexType;
	UINT64 m_HashVB;
	UINT m_HashIB;
	UINT64 m_HashVD;
	UINT m_ResVB;
	UINT m_ResIB;
	UINT m_ResVD;
	UINT m_VSize;
};

} // namespace KEP
