///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReMaterial.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

void ReLightCache::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (int i = 0; i < MAXRESHADERLIGHTS; ++i) {
			pMemSizeGadget->AddObjectSizeof(Lights[i]);
		}
	}
}

} // namespace KEP
