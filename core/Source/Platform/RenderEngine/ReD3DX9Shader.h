///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ReMaterial.h"
#include "d3dx9math.h"
#include "d3d9types.h"
#include "ReD3DX9Constants.h"

namespace KEP {
class ReD3DX9DeviceState;

enum ReD3DX9EffectHandleType {
	ReD3DX9_WORLDMATRIXARRAY_EFFECTHANDLE,
	ReD3DX9_MATWORLD_EFFECTHANDLE,
	ReD3DX9_MATVIEW_EFFECTHANDLE,
	ReD3DX9_MATPROJ_EFFECTHANDLE,
	ReD3DX9_MATWORLDVIEW_EFFECTHANDLE,
	ReD3DX9_MATVIEWPROJ_EFFECTHANDLE,
	ReD3DX9_MATWORLDVIEWPROJ_EFFECTHANDLE,
	ReD3DX9_MATWORLDINVERSETRANSPOSE_EFFECTHANDLE,
	ReD3DX9_MATVIEWINVERSE_EFFECTHANDLE,
	ReD3DX9_MATTEXTURE0_EFFECTHANDLE,
	ReD3DX9_MATTEXTURE1_EFFECTHANDLE,
	ReD3DX9_MATTEXTURE2_EFFECTHANDLE,
	ReD3DX9_MATTEXTURE3_EFFECTHANDLE,
	ReD3DX9_CAMERAPOS_EFFECTHANDLE,
	ReD3DX9_MATERIALAMBIENT_EFFECTHANDLE,
	ReD3DX9_MATERIALDIFFUSE_EFFECTHANDLE,
	ReD3DX9_MATERIALSPECULAR_EFFECTHANDLE,
	ReD3DX9_MATERIALEMISSIVE_EFFECTHANDLE,
	ReD3DX9_NUMBONES_EFFECTHANDLE,
	ReD3DX9_MATERIALSPECULARPOWER_EFFECTHANDLE,
	ReD3DX9_TIME_EFFECTHANDLE,
	ReD3DX9_MESHRADIUS_EFFECTHANDLE,
	ReD3DX9_TEXTURE0_EFFECTHANDLE,
	ReD3DX9_TEXTURE1_EFFECTHANDLE,
	ReD3DX9_TEXTURE2_EFFECTHANDLE,
	ReD3DX9_TEXTURE3_EFFECTHANDLE,
	ReD3DX9_GLOBALAMBIENT_EFFECTHANDLE,
	ReD3DX9_NUMLIGHTS_EFFECTHANDLE,
	ReD3DX9_LIGHTAMBIENT_EFFECTHANDLE,
	ReD3DX9_LIGHTATTENUATION0_EFFECTHANDLE,
	ReD3DX9_LIGHTATTENUATION1_EFFECTHANDLE,
	ReD3DX9_LIGHTATTENUATION2_EFFECTHANDLE,
	ReD3DX9_LIGHTDIFFUSE_EFFECTHANDLE,
	ReD3DX9_LIGHTDIRECTION_EFFECTHANDLE,
	ReD3DX9_LIGHTPOSITION_EFFECTHANDLE,
	ReD3DX9_LIGHTRANGE_EFFECTHANDLE,
	ReD3DX9_LIGHTSPECULAR_EFFECTHANDLE,
	NA_ReD3DX9EFFECTHANDLE
};

struct ReD3DX9EffectHandle {
	D3DXHANDLE handle;
	ReD3DX9EffectHandleType type;

	ReD3DX9EffectHandle() {
		handle = NULL;
		type = NA_ReD3DX9EFFECTHANDLE;
	}
};

class ReD3DX9ShaderNULL : public ReShader {
public:
	ReD3DX9ShaderNULL() {}
	ReD3DX9ShaderNULL(const ReD3DX9ShaderNULL& /*shader*/) { ; }
	virtual ~ReD3DX9ShaderNULL() { ; }

	virtual void PreRender() { ; }
	virtual void Render() { ; }
	virtual void PostRender() { ; }
	virtual void SetTechnique() { ; }
	virtual BOOL Update(const ReShaderData* /*desc*/) { return TRUE; }
	virtual void Invalidate() { ; }
	virtual BOOL EqualTo(const ReShader* /*shader*/) const { return TRUE; }
	virtual INT Compare(const ReShader* /*shader*/) const { return 0; }
	//virtual UINT64				Hash() const							{return ReD3DX9DEVICEINVALID;}
	virtual BOOL Invalid() const { return FALSE; }
	virtual ReShader* Clone(ReMesh* /*mesh*/ = NULL) { return new ReD3DX9ShaderNULL(*this); }
	virtual ReShaderType ShaderType() const { return ReShaderType::FIXEDFUNC; }
	virtual void Begin(UINT& passes) { passes = 1; }
	virtual void BeginPass(UINT /*pass*/) const { ; }
	virtual void EndPass() const { ; }
	virtual void End() const { ; }

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override {}

private:
	virtual BOOL Restore() { return TRUE; }
};

class ReD3DX9SkinMesh;

class ReD3DX9Shader : public ReShader {
public:
	ReD3DX9Shader();
	ReD3DX9Shader(ReD3DX9DeviceState* dev, const char* shader);
	ReD3DX9Shader(const ReD3DX9Shader& shader);
	virtual ~ReD3DX9Shader();

	virtual void PreRender() { ; }
	virtual void Render() {
		if (m_fX)
			m_fX->CommitChanges();
	}
	virtual void PostRender() { ; }
	virtual void SetTechnique() {
		if (m_fX)
			m_fX->SetTechnique(m_technq);
	}
	virtual BOOL Update(const ReShaderData* data);
	virtual void Invalidate() { ; }
	virtual BOOL EqualTo(const ReShader* shader) const { return Hash() == ((ReD3DX9Shader*)shader)->Hash(); }
	virtual BOOL Compare(const ReShader* shader) const {
		if (EqualTo(shader))
			return 0;
		return (Hash() < ((ReD3DX9Shader*)shader)->Hash() ? -1 : 1);
	}
	virtual BOOL Invalid() const { return m_resFX == ReD3DX9DEVICEINVALID; }
	//virtual UINT64					Hash() const									{return m_hashFX;}
	virtual ReShader* Clone(ReMesh* /*mesh*/ = NULL) { return new ReD3DX9Shader(m_dev, m_codeFX); }
	virtual ReShaderType ShaderType() const { return ReShaderType::GPU; }
	virtual void Begin(UINT& passes) {
		if (Restore()) {
			m_fX->SetTechnique(m_technq);
			m_fX->Begin(&passes, D3DXFX_DONOTSAVESTATE);
		}
	}
	virtual void BeginPass(UINT pass) const {
		if (m_fX)
			m_fX->BeginPass(pass);
	}
	virtual void EndPass() const {
		if (m_fX)
			m_fX->EndPass();
	}
	virtual void End() const {
		if (m_fX)
			m_fX->End();
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override;

private:
	static const UINT MAXEFFECTHANDLES = 32;
	void GetHandles();
	virtual BOOL Restore();

public:
	LPD3DXEFFECT m_fX;
	D3DXHANDLE m_technq;
	ReD3DX9DeviceState* m_dev;
	UINT m_resFX;
	//UINT64							m_hashFX;
	const char* m_codeFX;
	ReD3DX9EffectHandle m_handles[MAXEFFECTHANDLES];
	DWORD m_numHandles;
};

} // namespace KEP