///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include "ReSkinMesh.h"
#include "ReMaterial.h"
#include "ReMeshCache.h"
#include "ReD3DX9.h"

namespace KEP {

ReMaterialPool::ReMaterialPool(DWORD size) {
	m_MaxIDs = size;
	m_NumIDs = 0;

	m_MaterialPool = new ReMaterialPtr[size];

	if (!m_MaterialPool)
		m_MaxIDs = 0;
}

void ReMaterialPool::Invalidate() {
	if (m_MaterialPool) {
		delete[] m_MaterialPool;
		m_MaterialPool = NULL;
	}
	m_MaxIDs = 0;
	m_NumIDs = 0;
}

void ReMaterialPool::Reset() {
	for (DWORD i = 0; i < m_NumIDs; i++) {
		if (m_MaterialPool[i])
			m_MaterialPool[i] = ReMaterialPtr();
	}
	m_NumIDs = 0;
}

ReMaterialPtr ReMaterialPool::FindMaterial(ReMaterialPtr mat) {
	for (DWORD i = 0; i < m_NumIDs; i++) {
		if (m_MaterialPool[i]->EqualTo(mat))
			return m_MaterialPool[i];
	}
	return ReMaterialPtr();
}

ReMaterialPtr ReMaterialPool::FindMaterial(ReRenderState* state) {
	for (DWORD i = 0; i < m_NumIDs; i++) {
		if (m_MaterialPool[i]->GetRenderState()->EqualTo(state)) {
			return m_MaterialPool[i];
		}
	}
	return ReMaterialPtr();
}

ReMaterialPtr ReMaterialPool::AddMaterial(ReMaterialPtr mat) {
	ReMaterialPtr foundMat = FindMaterial(mat);
	if (foundMat) {
		return foundMat;
	}
	if (m_NumIDs < m_MaxIDs) {
		m_MaterialPool[m_NumIDs++] = mat;
	}
	return ReMaterialPtr();
}

ReMaterialPtr ReMaterialPool::GetMaterial(DWORD id) {
	if (id < m_MaxIDs) {
		return m_MaterialPool[id];
	}
	return ReMaterialPtr();
}

/**********************************************************************************
 ****************************** ReMeshPool ****************************************
 **********************************************************************************/
ReMeshPool::ReMeshPool(DWORD /*size*/) {
}

void ReMeshPool::Destroy() {
	Reset();
}

void ReMeshPool::Reset() {
	for (meshes_t::const_iterator iter = _meshes.begin(); iter != _meshes.end(); iter++) {
		if (iter->second)
			delete iter->second;
	}

	_meshes.clear();
}

ReMesh* ReMeshPool::FindMesh(ReMesh* mesh) {
	return FindMesh(mesh->GetHash());
}

ReMesh* ReMeshPool::FindMesh(UINT64 hash) {
	meshes_t::const_iterator iter = _meshes.find(hash);

	if (iter != _meshes.end())
		return iter->second;

	return NULL;
}

ReMesh* ReMeshPool::AddMesh(ReMesh* mesh) {
	meshes_t::iterator iter = _meshes.find(mesh->GetHash());

	if (iter != _meshes.end())
		return iter->second;

	mesh->CalculateDiameter();

	_meshes.insert(std::pair<unsigned __int64, ReMesh*>(mesh->GetHash(), mesh));
	return NULL;
}

/**********************************************************************************
 ****************************** ReMeshCache ***************************************
 **********************************************************************************/
ReMeshCache::ReMeshCache(DWORD size) {
	m_MaxIDs = size;
	m_NumCacheFrames = 0;
	m_NumIDs = 0;

	m_MeshCache = new ReMeshCachePair[size];
	m_CacheFrame = new ReMeshCacheFrame*[size];

	if (m_MeshCache && m_CacheFrame) {
		for (DWORD i = 0; i < size; i++)
			m_CacheFrame[i] = new ReMeshCacheFrame;
	} else {
		m_MaxIDs = 0;
	}
}

void ReMeshCache::Invalidate() {
	if (m_MeshCache) {
		delete[] m_MeshCache;
		m_MeshCache = NULL;
	}

	if (m_CacheFrame) {
		for (DWORD i = 0; i < m_MaxIDs; i++)
			delete m_CacheFrame[i];
		delete[] m_CacheFrame;
	}

	m_NumCacheFrames = 0;
	m_NumIDs = 0;
	m_MaxIDs = 0;
}

// TODO: Shell sort instead of Insertion sort
void ReMeshCache::SortFrames(ReMESHCACHESORTKEY key) {
	switch (key) {
		case ReMESHCACHESORTVERTEXSHADER: {
			for (DWORD i = 1; i < m_NumCacheFrames; i++) {
				ReMeshCacheFrame* temp = m_CacheFrame[i];
				DWORD j = i;
				while (j > 0 && m_CacheFrame[j - 1]->Mesh->GetShader()->Compare(temp->Mesh->GetShader()) > 0) {
					m_CacheFrame[j] = m_CacheFrame[j - 1];
					j = j - 1;
				}
				m_CacheFrame[j] = temp;
			}
			break;
		}

		case ReMESHCACHESORTCAMERAZ: {
			const Vector3f& camPos = *reinterpret_cast<const Vector3f*>(m_Device->GetRenderState()->GetCamPosition());
			for (DWORD i = 0; i < m_NumCacheFrames; i++) {
				const Vector3f& objPos = FirstMesh(i)->GetWorldMatrix()->GetTranslation();
				Vector3f camVec = camPos - objPos;
				m_CacheFrame[i]->CamDist = camVec.LengthSquared();
			}
			for (DWORD i = 1; i < m_NumCacheFrames; i++) {
				ReMeshCacheFrame* temp = m_CacheFrame[i];
				DWORD j = i;
				while (j > 0 && m_CacheFrame[j - 1]->CamDist < temp->CamDist) {
					m_CacheFrame[j] = m_CacheFrame[j - 1];
					j = j - 1;
				}
				m_CacheFrame[j] = temp;
			}
			break;
		}

		case ReMESHCACHESORTORDER: {
			for (DWORD i = 1; i < m_NumCacheFrames; i++) {
				ReMeshCacheFrame* temp = m_CacheFrame[i];

				if (m_MeshCache[temp->Start].pMat) {
					DWORD j = i;
					const ReRenderStateData* data = m_MeshCache[temp->Start].pMat->GetRenderState()->GetData();
					DWORD frame = m_CacheFrame[j - 1]->Start;
					while (m_MeshCache[frame].pMat && m_MeshCache[frame].pMat->GetRenderState()->GetData()->SortOrder > data->SortOrder) {
						m_CacheFrame[j] = m_CacheFrame[j - 1];
						if (--j == 0) {
							break;
						}
						frame = m_CacheFrame[j - 1]->Start;
					}
					m_CacheFrame[j] = temp;
				}
			}
			break;
		}

		default:;
	}
}

void ReMeshCache::RenderCacheFrame(const ReMeshCacheFrame* cacheFrame, bool lighting) const {
	const ReMeshCachePair& cacheEntry = m_MeshCache[cacheFrame->Start];
	ReMesh* pFirstMesh = cacheEntry.pMesh;
	const ReMaterial* mat = cacheEntry.pMat;

	// set world matrix
	if (m_Device->WorldMatrixRender((D3DXMATRIX*)pFirstMesh->GetWorldMatrix()))
		m_Device->GetRenderState()->SetWorldMatrix((D3DXMATRIX*)pFirstMesh->GetWorldMatrix());

	if (lighting) {
		// mesh light cache is ready to use
		const ReLightCache* pLightCache = pFirstMesh->GetLightCache();
		if (pLightCache && !pLightCache->Empty()) {
			if (!mat->PreLit()) {
				// load the global light cache directly from the mesh's light cache
				m_Device->GetRenderState()->SetLights(pLightCache);
			}
		} else {
			// load the mesh light cache
			m_Device->GetRenderState()->SortLights(reinterpret_cast<const D3DXVECTOR3*>(&pFirstMesh->GetWorldMatrix()->GetTranslation()), pFirstMesh->GetRadius(), const_cast<ReLightCache*>(pLightCache));
		}

		if (!mat->PreLit()) {
			m_Device->GetRenderState()->RenderLights(cacheFrame->Mesh->GetShader()->ShaderType() == ReShaderType::GPU);
		}
	}

	// reset the effect light parameter cache for each cache frame
	m_Device->ResetEffectObjectState();

	for (DWORD i = cacheFrame->Start; i < cacheFrame->End; i++) {
		ReMesh* mesh = m_MeshCache[i].pMesh;

		if (mesh->Update()) {
			mesh->PreRender();
			mesh->Render();
			mesh->PostRender();
		}
	}
}

void ReMeshCache::Render(void* device, ReMESHCACHESORTKEY key, bool lighting) {
	// Saw a crash where the cache was attempting to render with a null
	// device.  Didn't find the root cause, so just trying to stop the
	// crash and hope the client can recover the device.
	if (!device)
		return;

	m_Device = (ReD3DX9DeviceState*)device;

	SortFrames(key);

	// reset the render state for each mesh cache
	m_Device->ResetRenderState();

	for (DWORD i = 0; i < m_NumCacheFrames;) {
		ReShader* shader = (ReShader*)m_CacheFrame[i]->Mesh->GetShader();
		uint64_t iShaderHash = shader->Hash();

		UINT passes;
		shader->Begin(passes);

		UINT j = 0; // DRF - potentially uninitialized variable

		// reset the effect parameter cache for each effect
		m_Device->ResetEffectState();

		for (UINT pass = 0; pass < passes; pass++) {
			shader->BeginPass(pass);

			j = i;

			// render all cache frame meshes sharing "shader"
			while (true) {
				RenderCacheFrame(m_CacheFrame[j], lighting);
				++j;
				if (j >= m_NumCacheFrames)
					break;

				uint64_t iNextShaderHash = m_CacheFrame[j]->Mesh->GetShader()->Hash();
				if (iNextShaderHash != iShaderHash)
					break;
			}

			shader->EndPass();
		}

		shader->End();

		// move to the next shader
		i = j;
	}
	m_Device->ResetRenderState();
}

} // namespace KEP
