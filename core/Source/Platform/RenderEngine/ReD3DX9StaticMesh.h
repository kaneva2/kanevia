///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ReMesh.h"

#include "ReMeshStream.h"
#include "ReD3DX9.h"

namespace KEP {

class IMemSizeGadget;

class ReD3DX9StaticMesh : public ReMesh, public ReD3DX9Mesh {
	friend class ReShader_Prelighting;

public:
	ReD3DX9StaticMesh();
	ReD3DX9StaticMesh(void* dev);
	ReD3DX9StaticMesh(ReD3DX9StaticMesh& mesh);
	virtual ~ReD3DX9StaticMesh();

	virtual void Build(bool strip = true) override {
		ReMesh::Build(strip);
		if (m_Desc.Status == ReMESHSTATUS_INIT)
			ReD3DX9Mesh::Hash(m_Desc.Hash);
	}
	virtual void PreRender() override {
		if (m_Material)
			m_Material->PreRender();
		ReD3DX9Mesh::PreRender();
	}
	virtual void Render() override;
	virtual void PostRender() override {
		if (m_Material)
			m_Material->PostRender();
	}
	virtual void SetMaterial(ReMaterialPtr material) override;
	virtual BOOL Update() override;
	virtual BOOL IsInvalid() const override { return ReD3DX9Mesh::Invalid(); }
	virtual void Reload() override { ReD3DX9Mesh::Reload(reinterpret_cast<D3DXVECTOR3*>(m_Desc.pP), reinterpret_cast<D3DXVECTOR3*>(m_Desc.pN), m_Desc.NumV); }
	virtual void Clone(ReMesh** mesh) override { Clone(mesh, false); }
	virtual void Clone(ReMesh** mesh, bool forceClone) override;
	virtual BOOL CreateShader(const char* shader) override;
	virtual BOOL SetShader(ReShader* shader) override;
	virtual BOOL Lock(Vector3f* pos, DWORD numV) override {
		if (!Restore())
			return FALSE;
		return ReD3DX9Mesh::Lock(reinterpret_cast<D3DXVECTOR3*>(pos), numV);
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	virtual BOOL Restore() override { return Invalid() ? ReD3DX9Mesh::Restore(reinterpret_cast<D3DXVECTOR3*>(m_Desc.pP), reinterpret_cast<D3DXVECTOR3*>(m_Desc.pN), reinterpret_cast<D3DXVECTOR2*>(m_Desc.pUv), m_Desc.pI, m_Desc.NumV, m_Desc.NumI, m_Desc.PrimType) : TRUE; }
};

} // namespace KEP
