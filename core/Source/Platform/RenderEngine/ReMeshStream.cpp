///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#ifndef WINVER
#define WINVER 0x0501
#endif
#include "afx.h"

#include "ReMesh.h"
#include "ReD3DX9StaticMesh.h"
#include "ReSkinMesh.h"
#include "ReD3DX9SkinMesh.h"
#include "ReMeshStream.h"
#include "ReMeshConvertOptions.h"

#include "LogHelper.h"
#include <iomanip>

static LogInstance("Instance");

namespace KEP {

#ifdef ED7119_DYNOBJ_ASSET_FIX
// Binary rounding. I.e. if numOfZeroBits = 3, round significant bits to the nearest 2^-20.
void RoundFloatBySignificantBits(float& input, size_t numOfZeroBits) {
	if (numOfZeroBits > 0) {
		unsigned& data = *(unsigned*)&input;

		unsigned offset = 1 << (numOfZeroBits - 1);
		unsigned zeroMask = ~((1 << numOfZeroBits) - 1);
		unsigned sigMask = (1 << 23) - 1;
		if ((data & sigMask) + offset <= sigMask) { // Overflow test
			data = (data + offset) & zeroMask;
		}
	}
}
#endif

void ReMeshStream::WriteSkin(ReSkinMesh* mesh, CArchive& ar) {
	// access the ReMesh data
	ReMeshData* pData;
	ReSkinMeshData* pSkinData;
	mesh->Access(&pData, &pSkinData);

	// bump the version level for mesh compression
	pData->Version = 1;
	pSkinData->Version = 1;
	pSkinData->Reserved0 = 0;
	pSkinData->Reserved1 = 0;

	// initialize internal compression vars
	m_MinPos = pData->ScaleConst[0].x;
	m_MinNrm = pData->ScaleConst[1].x;
	m_MinUvs = pData->ScaleConst[2].x;
	m_MinWgt = pSkinData->ScaleConstW.x;

	m_PosConstC = pData->ScaleConst[0].y;
	m_NrmConstC = pData->ScaleConst[1].y;
	m_UvsConstC = pData->ScaleConst[2].y;
	m_WgtConstC = pSkinData->ScaleConstW.y;
	m_PosConstD = pData->ScaleConst[0].z;
	m_NrmConstD = pData->ScaleConst[1].z;
	m_UvsConstD = pData->ScaleConst[2].z;
	m_WgtConstD = pSkinData->ScaleConstW.z;

	// indicate the status as streamed
	pData->Status = ReMESHSTATUS_STREAMED;

	// write the mesh stream header data
	ar.Write((void*)pData, pData->HeaderSize0 + pData->HeaderSize1);
	ar.Write((void*)pSkinData, pSkinData->HeaderSize0 + pSkinData->HeaderSize1);

	// allocate the stream array
	BYTE* pMm = new BYTE[pData->StrmSize];
	BYTE* pM = pMm;

	// compress the stream array
	for (DWORD i = 0; i < pData->NumV; i++) {
		switch (pData->ScaleSize[0]) {
			case 1:
				CompressPosByte(pM, (FLOAT*)&pData->pP[i]);
				break;
			case 2:
				CompressPosWord(pM, (FLOAT*)&pData->pP[i]);
				break;
			case 4:
				AssignPosFloat(pM, (FLOAT*)&pData->pP[i]);
				break;
		}
	}
	for (DWORD i = 0; i < pData->NumV; i++) {
		switch (pData->ScaleSize[1]) {
			case 1:
				CompressNrmByte(pM, (FLOAT*)&pData->pN[i]);
				break;
			case 2:
				CompressNrmWord(pM, (FLOAT*)&pData->pN[i]);
				break;
			case 4:
				AssignNrmFloat(pM, (FLOAT*)&pData->pN[i]);
				break;
		}
	}

	switch (pData->NumUV) {
		case 1: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 1) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						break;
				}
			}
		} break;

		case 2: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 2) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
						break;
				}
			}
		} break;

		case 3: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 3) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
						break;
				}
			}
		} break;

		default: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 3]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 3]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 3]);
						break;
				}
			}
		} break;
	}

	for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += pSkinData->Wpv) {
		switch (pSkinData->ScaleSizeW) {
			case 1:
				CompressWgtByte(pM, (FLOAT*)&pSkinData->pW[j], pSkinData->Wpv);
				break;
			case 2:
				CompressWgtWord(pM, (FLOAT*)&pSkinData->pW[j], pSkinData->Wpv);
				break;
			case 4:
				AssignWgtFloat(pM, (FLOAT*)&pSkinData->pW[j], pSkinData->Wpv);
				break;
		}
	}

	for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += pSkinData->Wpv) {
		AssignBixByte(pM, (BYTE*)&pSkinData->pBI[j], pSkinData->Wpv);
	}

	// transfer the index array
	for (DWORD i = 0; i < pData->NumI; i++) {
		AssignIdxWord(pM, (WORD*)&pData->pI[i]);
	}

	// write the stream data
	ar.Write((void*)pMm, pData->StrmSize);

	delete[] pMm;
}

void ReMeshStream::ReadSkin(ReSkinMesh** mesh, CArchive& ar, ReD3DX9DeviceState* dev) {
	// the new mesh
	ReSkinMesh* newMesh = NULL;

	newMesh = new ReD3DX9SkinMesh(dev);

	// access mesh data
	ReMeshData* pData = NULL;
	ReSkinMeshData* pSkinData = NULL;
	newMesh->Access(&pData, &pSkinData);

	// read the mesh stream header data
	ar.Read((void*)pData, pData->HeaderSize0);

	// version 0 is uncompressed mesh data
	//
	// We could check this to make sure that
	// it's not corrupt, but I don't think
	// we have any version 0 data, so it's
	// dead code.
	if (pData->Version == 0) {
		// read the skinning data streaming header
		ar.Read((void*)pSkinData, pSkinData->HeaderSize0);

		// allocate the mesh data array
		newMesh->Allocate();

		ar.Read((void*)pData->pMem, pData->DataSize);

		// build internal data
		newMesh->Build();

		// export the mesh
		*mesh = newMesh;
	}
	// mesh compression
	else if (pData->Version == 1) {
		// read the version 1 stream header extension
		ar.Read((void*)pData->ScaleConst, pData->HeaderSize1);

#ifdef ED7119_DYNOBJ_ASSET_FIX
		if (ReMeshConvertOptions::getRoundSkinMeshNormalAndUVScale()) {
			RoundFloatBySignificantBits(pData->ScaleConst[1].x, 3); // Normal value min
			RoundFloatBySignificantBits(pData->ScaleConst[1].y, 3); // Normal value compression constant
			RoundFloatBySignificantBits(pData->ScaleConst[2].x, 3); // UV value min
			RoundFloatBySignificantBits(pData->ScaleConst[2].y, 3); // UV value compression constant
		}

		if (ReMeshConvertOptions::getRecalcDecompressionConstant()) {
			pData->ScaleConst[0].z = 1.0f / pData->ScaleConst[0].y; // Position decompression constant
			pData->ScaleConst[1].z = 1.0f / pData->ScaleConst[1].y; // Normal decompression constant
			pData->ScaleConst[2].z = 1.0f / pData->ScaleConst[2].y; // UV decompression constant
		}
#endif

		// read the skinning data streaming headers
		ar.Read((void*)pSkinData, pSkinData->HeaderSize0 + pSkinData->HeaderSize1);

		// initialize internal compression vars
		m_MinPos = pData->ScaleConst[0].x;
		m_MinNrm = pData->ScaleConst[1].x;
		m_MinUvs = pData->ScaleConst[2].x;
		m_MinWgt = pSkinData->ScaleConstW.x;

		m_PosConstC = pData->ScaleConst[0].y;
		m_NrmConstC = pData->ScaleConst[1].y;
		m_UvsConstC = pData->ScaleConst[2].y;
		m_WgtConstC = pSkinData->ScaleConstW.y;
		m_PosConstD = pData->ScaleConst[0].z;
		m_NrmConstD = pData->ScaleConst[1].z;
		m_UvsConstD = pData->ScaleConst[2].z;
		m_WgtConstD = pSkinData->ScaleConstW.z;

		// if the mesh is corrupt, but the version is still 1 (possible)
		// then check some other relationships to see if it's valid.
		// We can't check the hash until the data is decompressed in
		// version 1, but it'll crash during decompression if the data
		// is corrupt (doh!).  In future, assets should have the hash
		// in a header or otherwise uncompressed part of the file!
		if (pData->StrmSize < pData->NumI * 2 +
								  pData->NumV * (pData->ScaleSize[0] +
													pData->ScaleSize[1] +
													pData->NumUV * pData->ScaleSize[2] +
													pSkinData->ScaleSizeW + 1)) {
			delete newMesh;
			*mesh = NULL;
			return;
		}

		// allocate the mesh data array
		newMesh->Allocate();

		// allocate the stream array
		BYTE* pMm = new BYTE[pData->StrmSize];
		BYTE* pM = pMm;

		// load the stream data into stream array
		ar.Read((void*)pM, pData->StrmSize);

		// decompress the stream array
		for (DWORD i = 0; i < pData->NumV; i++) {
			switch (pData->ScaleSize[0]) {
				case 1:
					DeCompressPosByte(pM, (FLOAT*)&pData->pP[i]);
					break;
				case 2:
					DeCompressPosWord(pM, (FLOAT*)&pData->pP[i]);
					break;
				case 4:
					DeAssignPosFloat(pM, (FLOAT*)&pData->pP[i]);
					break;
			}
		}
		for (DWORD i = 0; i < pData->NumV; i++) {
			switch (pData->ScaleSize[1]) {
				case 1:
					DeCompressNrmByte(pM, (FLOAT*)&pData->pN[i]);
					break;
				case 2:
					DeCompressNrmWord(pM, (FLOAT*)&pData->pN[i]);
					break;
				case 4:
					DeAssignNrmFloat(pM, (FLOAT*)&pData->pN[i]);
					break;
			}
		}

		switch (pData->NumUV) {
			case 1: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 1) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							break;
					}
				}
			} break;

			case 2: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 2) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
							break;
					}
				}
			} break;

			case 3: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 3) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
							break;
					}
				}
			} break;

			default: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 3]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 3]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 3]);
							break;
					}
				}
			} break;
		}

		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += pSkinData->Wpv) {
#ifdef ED7119_DYNOBJ_ASSET_FIX
			assert(pSkinData->ScaleSizeW == 1);
			if (pSkinData->ScaleSizeW == 1 && pSkinData->Wpv > 1 && ReMeshConvertOptions::getFixReducedFirstWeight()) {
				// NOTE: Existing asset already have bone-weights sorted (by weights only, not by bone index for equal weights)

				//
				// ****************************************************************
				// Workaround for original asset storing equal weights with different compressed value due to the total weight
				// correction logic found in ReMeshStreamer::CompressWgtByte().
				//
				// ****************************************************************
				// E.g. There is a vertex with three float weights: {0.45, 0.45, 0.1} which sum up to 1. When compressed,
				// the corresponding byte values would be {115, 115, 26} which would sum up to 256 (> 255). The correction
				// logic would subtract the first compressed weight by 1 (256 - 255) and turn the weight array into into
				// {114, 115, 26}.
				// This causes two issues: 1) the weights no longer appear sorted, 2) the first two bones no longer carry
				// equal weights which will be sorted differently once BuildBoneWeightsForVertex() is called, than they
				// initially were. It had caused troubles in generating deterministic output from same input.
				//
				// With maximum 4 weights per vertex, the maximum error of first weight would be <= 2. The weight correction
				// logic could also be applied to the smallest weight when the total compressed weight is less than 255.
				//
				// ****************************************************************
				// Since existing assets are supposed to have weights sorted and the first weight is supposed to be greater
				// than or equal to the second one. If we find the first compressed weight slightly (1 or 2) less than the
				// second weight, it can be determined that the correction logic had been applied to the weights. Consider
				// it unlikely for the difference between two independent weights to be as small as 1 (or 0.004 uncompressed),
				// we can safely assume that if first compressed weight appears 1 or 2 less than the second, it should have
				// been equal to the second weight before compression.
				//
				assert(pM[0] >= pM[1] - 2);
				if (pM[1] >= 1 && pM[0] == pM[1] - 1 || pM[1] >= 2 && pM[0] == pM[1] - 2) {
					pM[0] = pM[1];
				}
			}
#endif
			switch (pSkinData->ScaleSizeW) {
				case 1:
					DeCompressWgtByte(pM, (FLOAT*)&pSkinData->pW[j], pSkinData->Wpv);
					break;
				case 2:
					DeCompressWgtWord(pM, (FLOAT*)&pSkinData->pW[j], pSkinData->Wpv);
					break;
				case 4:
					DeAssignWgtFloat(pM, (FLOAT*)&pSkinData->pW[j], pSkinData->Wpv);
					break;
			}
		}

		int iLargestBoneIndex = -1;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += pSkinData->Wpv) {
			DeAssignBixByte(pM, (BYTE*)&pSkinData->pBI[j], pSkinData->Wpv);
			for (DWORD k = 0; k < pSkinData->Wpv; ++k) {
				BYTE iBoneIndex = pSkinData->pBI[j + k];
				if (iBoneIndex > iLargestBoneIndex)
					iLargestBoneIndex = iBoneIndex;
			}
		}
		DWORD nUsedBones = iLargestBoneIndex + 1;
		if (pSkinData->NumB < nUsedBones)
			LogError("Invalid mesh data: bone index is beyond range specified by NumB. Correcting.");
		pSkinData->NumB = nUsedBones;

		// transfer the index array
		for (DWORD i = 0; i < pData->NumI; i++) {
			DeAssignIdxWord(pM, (WORD*)&pData->pI[i]);
		}

		delete[] pMm;

		// build internal data
		newMesh->Build();

		// export new mesh
		*mesh = newMesh;
	} else {
		// neither version 0 or 1, this is an error and
		// should catch most corrupt files
		*mesh = NULL;
	}
}

void ReMeshStream::Write(ReMesh* mesh, CArchive& ar) {
	// access the ReMesh data
	ReMeshData* pData;
	mesh->Access(&pData);

	// bump the version level for compression
	pData->Version = 1;

	// initialize internal compression vars
	m_MinPos = pData->ScaleConst[0].x;
	m_MinNrm = pData->ScaleConst[1].x;
	m_MinUvs = pData->ScaleConst[2].x;

	m_PosConstC = pData->ScaleConst[0].y;
	m_NrmConstC = pData->ScaleConst[1].y;
	m_UvsConstC = pData->ScaleConst[2].y;
	m_PosConstD = pData->ScaleConst[0].z;
	m_NrmConstD = pData->ScaleConst[1].z;
	m_UvsConstD = pData->ScaleConst[2].z;

	// indicate the status as streamed
	pData->Status = ReMESHSTATUS_STREAMED;

	// write the mesh stream header data
	ar.Write((void*)pData, pData->HeaderSize0 + pData->HeaderSize1);

	// allocate the stream array
	BYTE* pMm = new BYTE[pData->StrmSize];
	BYTE* pM = pMm;

	// compress the stream array
	for (DWORD i = 0; i < pData->NumV; i++) {
		switch (pData->ScaleSize[0]) {
			case 1:
				CompressPosByte(pM, (FLOAT*)&pData->pP[i]);
				break;
			case 2:
				CompressPosWord(pM, (FLOAT*)&pData->pP[i]);
				break;
			case 4:
				AssignPosFloat(pM, (FLOAT*)&pData->pP[i]);
				break;
		}
	}
	for (DWORD i = 0; i < pData->NumV; i++) {
		switch (pData->ScaleSize[1]) {
			case 1:
				CompressNrmByte(pM, (FLOAT*)&pData->pN[i]);
				break;
			case 2:
				CompressNrmWord(pM, (FLOAT*)&pData->pN[i]);
				break;
			case 4:
				AssignNrmFloat(pM, (FLOAT*)&pData->pN[i]);
				break;
		}
	}

	switch (pData->NumUV) {
		case 0:
			break;

		case 1: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 1) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						break;
				}
			}
		} break;

		case 2: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 2) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
						break;
				}
			}
		} break;

		case 3: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 3) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
						break;
				}
			}
		} break;

		default: {
			for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
				switch (pData->ScaleSize[2]) {
					case 1:
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
						CompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 3]);
						break;
					case 2:
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
						CompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 3]);
						break;
					case 4:
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
						AssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 3]);
						break;
				}
			}
		} break;
	}

	// transfer the index array
	for (DWORD i = 0; i < pData->NumI; i++) {
		AssignIdxWord(pM, (WORD*)&pData->pI[i]);
	}

	// write the stream data
	ar.Write((void*)pMm, pData->StrmSize);

	delete[] pMm;
}

void ReMeshStream::Read(ReMesh** mesh, CArchive& ar, ReD3DX9DeviceState* dev) {
	// create the mesh
	ReMesh* newMesh = new ReD3DX9StaticMesh(dev);

	// access mesh data
	ReMeshData* pData = NULL;
	newMesh->Access(&pData);

	// read the mesh stream header data
	ar.Read((void*)pData, pData->HeaderSize0);

	// version 0 is uncompressed mesh data
	if (pData->Version == 0) {
		// allocate the mesh data array
		newMesh->Allocate();

		ar.Read((void*)pData->pMem, pData->DataSize);

		// build internal data
		newMesh->Build();

		// export new mesh
		*mesh = newMesh;
	}
	// version 1 is compressed mesh data
	else if (pData->Version == 1) {
		// read the version 1 stream header extension
		ar.Read((void*)pData->ScaleConst, pData->HeaderSize1);

		// initialize internal compression vars
		m_MinPos = pData->ScaleConst[0].x;
		m_MinNrm = pData->ScaleConst[1].x;
		m_MinUvs = pData->ScaleConst[2].x;

		m_PosConstC = pData->ScaleConst[0].y;
		m_NrmConstC = pData->ScaleConst[1].y;
		m_UvsConstC = pData->ScaleConst[2].y;
		m_PosConstD = pData->ScaleConst[0].z;
		m_NrmConstD = pData->ScaleConst[1].z;
		m_UvsConstD = pData->ScaleConst[2].z;

		if (pData->StrmSize < pData->NumI * 2 +
								  pData->NumV * (pData->ScaleSize[0] +
													pData->ScaleSize[1] +
													pData->NumUV * pData->ScaleSize[2])) {
			delete newMesh;
			*mesh = NULL;
			return;
		}

		// allocate the mesh data array
		if (newMesh->Allocate() == FALSE) {
			// Failed to allocate space for
			// positions, normals, UV.
			// Will probably result in a failure elsewhere.
			delete newMesh;
			*mesh = NULL;
			return;
		}

		// allocate the stream array
		BYTE* pMm = new BYTE[pData->StrmSize];
		BYTE* pM = pMm;

		// load the stream data into stream array
		ar.Read((void*)pM, pData->StrmSize);

		// decompress the stream array
		for (DWORD i = 0; i < pData->NumV; i++) {
			switch (pData->ScaleSize[0]) {
				case 1:
					DeCompressPosByte(pM, (FLOAT*)&pData->pP[i]);
					break;
				case 2:
					DeCompressPosWord(pM, (FLOAT*)&pData->pP[i]);
					break;
				case 4:
					DeAssignPosFloat(pM, (FLOAT*)&pData->pP[i]);
					break;
			}
		}
		for (DWORD i = 0; i < pData->NumV; i++) {
			switch (pData->ScaleSize[1]) {
				case 1:
					DeCompressNrmByte(pM, (FLOAT*)&pData->pN[i]);
					break;
				case 2:
					DeCompressNrmWord(pM, (FLOAT*)&pData->pN[i]);
					break;
				case 4:
					DeAssignNrmFloat(pM, (FLOAT*)&pData->pN[i]);
					break;
			}
		}

		switch (pData->NumUV) {
			case 1: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 1) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							break;
					}
				}
			} break;

			case 2: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 2) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
							break;
					}
				}
			} break;

			case 3: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 3) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
							break;
					}
				}
			} break;

			default: {
				for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
					switch (pData->ScaleSize[2]) {
						case 1:
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 2]);
							DeCompressUvsByte(pM, (FLOAT*)&pData->pUv[j + 3]);
							break;
						case 2:
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 2]);
							DeCompressUvsWord(pM, (FLOAT*)&pData->pUv[j + 3]);
							break;
						case 4:
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 1]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 2]);
							DeAssignUvsFloat(pM, (FLOAT*)&pData->pUv[j + 3]);
							break;
					}
				}
			} break;
		}

		// transfer the index array
		for (DWORD i = 0; i < pData->NumI; i++) {
			DeAssignIdxWord(pM, (WORD*)&pData->pI[i]);
		}

		delete[] pMm;

		// build internal data
		newMesh->Build();

		newMesh->SetShader(NULL);

		// export new mesh
		*mesh = newMesh;
	} else {
		*mesh = NULL;
	}
}

} // namespace KEP
