///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReD3DX9.h"

#include "Utils/ReUtils.h"
#include "ReMaterial.h"
#include ".\vertexshaders\vertexshaders.h"
#include "..\common\include\ITextureProxy.h"
#include "..\common\include\IMemSizeGadget.h"

#include <jsLock.h>

#include "common\include\glAdjust.h"

#include "common\include\LogHelper.h"
static LogInstance("Rendering");

namespace KEP {

ReD3DX9RenderState::ReD3DX9RenderState() :
		ReRenderState(&m_RenderStateData) {
	m_pDev = NULL;
	m_RenderStateData.clear();
}

ReD3DX9RenderState::ReD3DX9RenderState(const void* dev, const ReRenderStateData* data) :
		ReRenderState(&m_RenderStateData) {
	m_pDev = (ReD3DX9DeviceState*)dev;

	if (data)
		m_RenderStateData = *data;
	else
		m_RenderStateData.clear();
}

ReD3DX9RenderState::ReD3DX9RenderState(const ReD3DX9RenderState& state) :
		ReRenderState(&m_RenderStateData) {
	m_pDev = state.m_pDev;
	m_RenderStateData = state.m_RenderStateData;
}

void ReD3DX9RenderState::Render() {
	if (!m_pDev->GetDevice())
		return;

	// material
	if (m_pDev->MatStateRender(m_RenderStateData.MatHash)) {
		D3DCheck(m_pDev->GetDevice()->SetMaterial((D3DMATERIAL9*)&m_RenderStateData.Mat));
	}

	// mode
	if (m_pDev->ModeStateRender(m_RenderStateData.ModeHash)) {
		D3DCheck(m_pDev->SetRenderState(D3DRS_ZWRITEENABLE, m_RenderStateData.Mode.Zwriteenable));
		D3DCheck(m_pDev->SetRenderState(D3DRS_ALPHABLENDENABLE, m_RenderStateData.Mode.Alphaenable));
		D3DCheck(m_pDev->SetRenderState(D3DRS_ALPHATESTENABLE, m_RenderStateData.Mode.Alphatestenable));
		D3DCheck(m_pDev->SetRenderState(D3DRS_CULLMODE, m_RenderStateData.Mode.CullMode));

		if (m_RenderStateData.Mode.FogFilter) {
			D3DCheck(m_pDev->SetRenderState(D3DRS_FOGENABLE, FALSE));
		}
	}
	// color source
	if (m_pDev->ColorSourceRender(static_cast<int>(m_RenderStateData.ColorSrc))) {
		switch (m_RenderStateData.ColorSrc) {
			case ReColorSource::MATERIAL:
				m_pDev->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_MATERIAL);
				m_pDev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
				m_pDev->SetRenderState(D3DRS_LIGHTING, TRUE);
				break;

			case ReColorSource::VERTEX:
				m_pDev->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
				m_pDev->SetRenderState(D3DRS_COLORVERTEX, TRUE);
				m_pDev->SetRenderState(D3DRS_LIGHTING, FALSE);
				break;

			default:
				m_pDev->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_MATERIAL);
				m_pDev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
				m_pDev->SetRenderState(D3DRS_LIGHTING, TRUE);
				break;
		}
	}

	// blend
	if (m_pDev->BlendStateRender(m_RenderStateData.BlendHash)) {
		D3DCheck(m_pDev->SetRenderState(D3DRS_SRCBLEND, m_RenderStateData.Blend.Srcblend));
		D3DCheck(m_pDev->SetRenderState(D3DRS_DESTBLEND, m_RenderStateData.Blend.Dstblend));
	}

	m_pDev->SetNumTextureStages(m_RenderStateData.NumTexStages);

	if (m_pDev->TextureConstantChanged(m_RenderStateData.Constant)) {
		// One constant per material for now
		unsigned NumTexStages = m_RenderStateData.NumTexStages;
		if (m_RenderStateData.Highlight == TRUE && NumTexStages < 4) {
			NumTexStages++;
		}
		for (DWORD i = 0; i < NumTexStages; i++) {
			D3DCheck(m_pDev->GetDevice()->SetTextureStageState(i, D3DTSS_CONSTANT, m_RenderStateData.Constant));
		}
	}

	if (m_pDev->TextureFactorChanged(m_RenderStateData.TextureFactor)) {
		D3DCheck(m_pDev->SetRenderState(D3DRS_TEXTUREFACTOR, m_RenderStateData.TextureFactor));
	}

	for (DWORD i = 0; i < m_RenderStateData.NumTexStages; i++) {
		IDirect3DTexture9* pTex = nullptr;

		// check if texture has been streamed off disc
		if (m_RenderStateData.ResPtr[i]) {
			pTex = m_RenderStateData.ResPtr[i]->getTexturePtr();
			if (pTex) {
				if (pTex != m_RenderStateData.TexPtr[i].get()) {
					pTex->AddRef();
					m_RenderStateData.TexPtr[i].reset(pTex, [](void* p) { static_cast<IDirect3DTexture9*>(p)->Release(); });
				}
			} else {
				m_RenderStateData.TexPtr[i].reset();
			}
		}

		pTex = static_cast<IDirect3DTexture9*>(m_RenderStateData.TexPtr[i].get());
		if (m_pDev->TexRender((UINT64)pTex, i)) {
			D3DCheck(m_pDev->GetDevice()->SetTexture(i, pTex));
		}

		if (m_pDev->TexStateRender(m_RenderStateData.TexHash[i], i)) {
			D3DCheck(m_pDev->GetDevice()->SetTextureStageState(i, D3DTSS_COLOROP, m_RenderStateData.Tex[i].ColorOp));
			D3DCheck(m_pDev->GetDevice()->SetTextureStageState(i, D3DTSS_ALPHAOP, m_RenderStateData.Tex[i].AlphaOp));
			D3DCheck(m_pDev->GetDevice()->SetTextureStageState(i, D3DTSS_COLORARG1, m_RenderStateData.Tex[i].ColorArg1));
			D3DCheck(m_pDev->GetDevice()->SetTextureStageState(i, D3DTSS_COLORARG2, m_RenderStateData.Tex[i].ColorArg2));
		}
	}

	if (m_RenderStateData.Highlight == TRUE && m_RenderStateData.NumTexStages < 4) {
		// Additional stage for texture highlight
		D3DCheck(m_pDev->GetDevice()->SetTextureStageState(m_RenderStateData.NumTexStages, D3DTSS_COLOROP, D3DTOP_BLENDFACTORALPHA));
		D3DCheck(m_pDev->GetDevice()->SetTextureStageState(m_RenderStateData.NumTexStages, D3DTSS_ALPHAOP, D3DTOP_DISABLE));
		D3DCheck(m_pDev->GetDevice()->SetTextureStageState(m_RenderStateData.NumTexStages, D3DTSS_COLORARG1, D3DTA_CONSTANT));
		D3DCheck(m_pDev->GetDevice()->SetTextureStageState(m_RenderStateData.NumTexStages, D3DTSS_COLORARG2, D3DTA_CURRENT));

		m_pDev->SetNumTextureStages(m_RenderStateData.NumTexStages + 1);
	}
}

void ReD3DX9RenderState::PostRender() {
	if (!m_pDev->GetDevice())
		return;

	if (m_RenderStateData.Mode.DrawWireFrame) {
		// reset fill mode back to solid
		m_pDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	}

	if (m_RenderStateData.Mode.Zenable == 0) {
		// restore z sorting for other objects
		m_pDev->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	}

	if (m_RenderStateData.Mode.NormalizeNormals) {
		// restore NormalizeNormals render state
		m_pDev->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
	}

	// If fog filtering was turned on and global fog is turned on, then
	// be sure to turn fog back on.
	if (m_RenderStateData.Mode.FogFilter) {
		const D3DXVECTOR4* pFogRange = m_pDev->GetRenderState()->GetFogRange();
		if (pFogRange->x != pFogRange->y) {
			m_pDev->SetRenderState(D3DRS_FOGENABLE, TRUE);
		}
	}
}

void ReD3DX9RenderState::SetTextureMatrix(UINT stage, D3DXMATRIX* pMatrix) {
	assert(stage < ReDEVICEMAXTEXTURESTAGES);
	if (stage < ReDEVICEMAXTEXTURESTAGES) {
		m_RenderStateData.TexMat[stage] = pMatrix;
	}
}

void ReD3DX9RenderState::PreRender() {
	if (!m_pDev->GetDevice())
		return;

	if (m_RenderStateData.Mode.DrawWireFrame) {
		// change fill mode to wireframe
		m_pDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	}

	if (m_RenderStateData.Mode.Zenable == 0) {
		// turn off z sorting on request
		m_pDev->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	}

	if (m_RenderStateData.Mode.NormalizeNormals) {
		// turn on normalization on normal vectors
		m_pDev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	}
}

ReD3DX9Material::ReD3DX9Material(const void* dev, const ReRenderStateData* data) :
		m_pDev((ReD3DX9DeviceState*)dev), m_State((void*)dev, data), ReMaterial(m_State.GetRenderStateDataNonVirtual()) {}

ReD3DX9Material::ReD3DX9Material(const ReD3DX9Material& mat) :
		m_pDev(mat.m_pDev), m_State(mat.m_State), ReMaterial(m_State.GetRenderStateDataNonVirtual()) {}

ReD3DX9Material::~ReD3DX9Material() {
	m_pDev = NULL;
}

void ReD3DX9Material::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	m_pDev 					// ReD3DX9DeviceState*
	m_State                 // ReD3DX9RenderState              
	-------------------------------------------------------------------------------*/
}

ReD3DX9Material_TexMat::ReD3DX9Material_TexMat() :
		ReMaterial_TexMat(m_State.GetRenderStateDataNonVirtual()) {
	m_pDev = NULL;

	m_UVIncr[0].x = 0.f;
	m_UVIncr[0].y = 0.f;
	m_UVIncr[1].x = 0.f;
	m_UVIncr[1].y = 0.f;
}

ReD3DX9Material_TexMat::ReD3DX9Material_TexMat(const void* dev, const ReRenderStateData* data, const FLOAT* uvIncr) :
		ReMaterial_TexMat(m_State.GetRenderStateDataNonVirtual()),
		m_State((void*)dev, data) {
	m_pDev = (ReD3DX9DeviceState*)dev;

	D3DXVECTOR2* uvI = (D3DXVECTOR2*)uvIncr;

	for (size_t i = 0; i < 2; ++i) {
		m_State.SetTextureMatrix(i, &m_TexMat[i]);
		D3DXMatrixIdentity(&m_TexMat[i]);
	}

	m_UVIncr[0].y = -uvI[0].x;
	m_UVIncr[0].x = -uvI[0].y;
	m_UVIncr[1].y = -uvI[1].x;
	m_UVIncr[1].x = -uvI[1].y;
}

ReD3DX9Material_TexMat::ReD3DX9Material_TexMat(const ReD3DX9Material_TexMat& mat) :
		ReMaterial_TexMat(m_State.GetRenderStateDataNonVirtual()),
		m_State((void*)mat.m_pDev, mat.m_State.GetData()) {
	m_pDev = mat.m_pDev;

	for (size_t i = 0; i < 2; ++i) {
		m_TexMat[i] = mat.m_TexMat[i];
		m_State.SetTextureMatrix(i, &m_TexMat[i]);
	}

	memcpy(m_UVIncr, mat.m_UVIncr, sizeof(m_UVIncr));
}

ReD3DX9Material_TexMat::~ReD3DX9Material_TexMat() {
	m_pDev = NULL;
}

// DRF - This function increments moving textures (sky clouds, 'in-motion' clothing, dance floor, etc)
void ReD3DX9Material_TexMat::Update() {
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	for (size_t i = 0; i < 2; ++i) {
		if (m_TexMat[i] && m_pDev->TextMatRender((UINT64)&m_TexMat[i], i)) {
			m_TexMat[i]._31 += m_UVIncr[i].x * deltaT;
			m_TexMat[i]._32 += m_UVIncr[i].y * deltaT;
		}
	}
}

void ReD3DX9Material_TexMat::PreRender() {
	if (!m_pDev->GetDevice())
		return;

	m_State.PreRender();

	for (size_t i = 0; i < 2; ++i) {
		if (m_TexMat[i]) {
			D3DCheck(m_pDev->GetDevice()->SetTextureStageState(i, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2));
			D3DCheck(m_pDev->GetDevice()->SetTransform(static_cast<D3DTRANSFORMSTATETYPE>(D3DTS_TEXTURE0 + i), &m_TexMat[i]));
		}
	}
}

void ReD3DX9Material_TexMat::PostRender() {
	if (!m_pDev->GetDevice())
		return;

	m_State.PostRender();

	for (size_t i = 0; i < 2; ++i) {
		if (m_TexMat[i])
			m_pDev->GetDevice()->SetTextureStageState(i, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);
	}
}

void ReD3DX9Material_TexMat::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	-------------------------------------------------------------------------------*/
}

ReD3DX9Mesh::ReD3DX9Mesh() {
	m_pDev = NULL;
	m_VertexType = ReD3DX9VertexType::INVALID;
	m_pVB = NULL;
	m_pIB = NULL;
	m_pVD = NULL;
	m_HashVB = ReD3DX9DEVICEINVALID;
	m_HashIB = ReD3DX9DEVICEINVALID;
	m_HashVD = ReD3DX9DEVICEINVALID;
	m_ResVB = ReD3DX9DEVICEINVALID;
	m_ResIB = ReD3DX9DEVICEINVALID;
	m_ResVD = ReD3DX9DEVICEINVALID;
};

ReD3DX9Mesh::ReD3DX9Mesh(ReD3DX9DeviceState* dev) {
	m_pDev = NULL;
	m_VertexType = ReD3DX9VertexType::INVALID;
	m_pVB = NULL;
	m_pIB = NULL;
	m_pVD = NULL;
	m_HashVB = ReD3DX9DEVICEINVALID;
	m_HashIB = ReD3DX9DEVICEINVALID;
	m_HashVD = ReD3DX9DEVICEINVALID;
	m_ResVB = ReD3DX9DEVICEINVALID;
	m_ResIB = ReD3DX9DEVICEINVALID;
	m_ResVD = ReD3DX9DEVICEINVALID;
	m_pDev = dev;
};

void ReD3DX9Mesh::Invalidate() {
	m_pDev->InvalidateResource(m_ResVB);
	m_pDev->InvalidateResource(m_ResIB);
	m_pDev->InvalidateResource(&m_ResVD);
}

// reloads vertex buffer pos & normal for static & CPU-skinned geometry (i.e. vertex without indexes & weights)
void ReD3DX9Mesh::Reload(D3DXVECTOR3* pPos, D3DXVECTOR3* pNorm, DWORD numV) {
	switch (m_VertexType) {
		case ReD3DX9VertexType::POS_NORM_UV1:
		case ReD3DX9VertexType::POS_NORM_UV1D: {
			m_VSize = sizeof(ReOutputVertex0_D3DX9);
			// restore the vertex buffer
			ReOutputVertex0_D3DX9* pVB = 0;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
				for (DWORD i = 0; i < numV; i++) {
					pVB->X = pPos[i].x;
					pVB->Y = pPos[i].y;
					pVB->Z = pPos[i].z;
					pVB->NX = pNorm[i].x;
					pVB->NY = pNorm[i].y;
					pVB->NZ = pNorm[i].z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
			}
		} break;

		case ReD3DX9VertexType::POS_NORM_UV2:
		case ReD3DX9VertexType::POS_NORM_UV2D: {
			m_VSize = sizeof(ReOutputVertex3_D3DX9);
			// restore the vertex buffer
			ReOutputVertex3_D3DX9* pVB = 0;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
				for (DWORD i = 0, j = 0; i < numV; i++, j += 2) {
					pVB->X = pPos[i].x;
					pVB->Y = pPos[i].y;
					pVB->Z = pPos[i].z;
					pVB->NX = pNorm[i].x;
					pVB->NY = pNorm[i].y;
					pVB->NZ = pNorm[i].z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
			}
		} break;

		case ReD3DX9VertexType::POS_NORM_UV3:
		case ReD3DX9VertexType::POS_NORM_UV3D: {
			m_VSize = sizeof(ReOutputVertex4_D3DX9);
			// restore the vertex buffer
			ReOutputVertex4_D3DX9* pVB = 0;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
				for (DWORD i = 0, j = 0; i < numV; i++, j += 3) {
					pVB->X = pPos[i].x;
					pVB->Y = pPos[i].y;
					pVB->Z = pPos[i].z;
					pVB->NX = pNorm[i].x;
					pVB->NY = pNorm[i].y;
					pVB->NZ = pNorm[i].z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
			}
		} break;

		case ReD3DX9VertexType::POS_NORM_UV4:
		case ReD3DX9VertexType::POS_NORM_UV4D: {
			m_VSize = sizeof(ReOutputVertex5_D3DX9);
			// restore the vertex buffer
			ReOutputVertex5_D3DX9* pVB = 0;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
				for (DWORD i = 0, j = 0; i < numV; i++, j += 4) {
					pVB->X = pPos[i].x;
					pVB->Y = pPos[i].y;
					pVB->Z = pPos[i].z;
					pVB->NX = pNorm[i].x;
					pVB->NY = pNorm[i].y;
					pVB->NZ = pNorm[i].z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
			}
		} break;
	}
}

// restores index & vertex buffer for static & CPU-skinned geometry (i.e. vertex without indexes & weights)
BOOL ReD3DX9Mesh::Restore(D3DXVECTOR3* pPos, D3DXVECTOR3* pNorm, D3DXVECTOR2* pUv, WORD* pI, DWORD numV, DWORD numI, ReMeshPrimType primType) {
	if (!m_pDev)
		return FALSE;

	// vertex buffer not created yet
	if (m_ResVB == ReD3DX9DEVICEINVALID) {
		// CPU-skinned vertex...."DISCARD" enables driver to double-buffer the vertex buffers so the "Lock" call does not block
		if (m_VertexType >= ReD3DX9VertexType::POS_NORM_UV1D && m_VertexType <= ReD3DX9VertexType::POS_NORM_UV4D) {
			if (!m_pDev->CreateDynamicResource(ReD3DX9ResourceType::VERTEX_BUFFER_DISCARD, m_VertexType, numV, (void**)&m_pVB, &m_ResVB))
				return FALSE;
		} else {
			if (!m_pDev->CreateStaticResource(ReD3DX9ResourceType::VERTEX_BUFFER, m_VertexType, numV, (void**)&m_pVB, &m_ResVB))
				return FALSE;
		}

		switch (m_VertexType) {
			case ReD3DX9VertexType::POS_NORM_UV1:
			case ReD3DX9VertexType::POS_NORM_UV1D: {
				m_VSize = sizeof(ReOutputVertex0_D3DX9);
				// restore the vertex buffer
				ReOutputVertex0_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0; i < numV; i++) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->NX = pNorm[i].x;
						pVB->NY = pNorm[i].y;
						pVB->NZ = pNorm[i].z;
						pVB->U = pUv[i].x;
						pVB->V = pUv[i].y;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;

			case ReD3DX9VertexType::POS_NORM_UV2:
			case ReD3DX9VertexType::POS_NORM_UV2D: {
				m_VSize = sizeof(ReOutputVertex3_D3DX9);
				// restore the vertex buffer
				ReOutputVertex3_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0, j = 0; i < numV; i++, j += 2) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->NX = pNorm[i].x;
						pVB->NY = pNorm[i].y;
						pVB->NZ = pNorm[i].z;
						pVB->U0 = pUv[j].x;
						pVB->V0 = pUv[j].y;
						pVB->U1 = pUv[j + 1].x;
						pVB->V1 = pUv[j + 1].y;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;

			case ReD3DX9VertexType::POS_NORM_UV3:
			case ReD3DX9VertexType::POS_NORM_UV3D: {
				m_VSize = sizeof(ReOutputVertex4_D3DX9);
				// restore the vertex buffer
				ReOutputVertex4_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0, j = 0; i < numV; i++, j += 3) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->NX = pNorm[i].x;
						pVB->NY = pNorm[i].y;
						pVB->NZ = pNorm[i].z;
						pVB->U0 = pUv[j].x;
						pVB->V0 = pUv[j].y;
						pVB->U1 = pUv[j + 1].x;
						pVB->V1 = pUv[j + 1].y;
						pVB->U2 = pUv[j + 2].x;
						pVB->V2 = pUv[j + 2].y;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;

			case ReD3DX9VertexType::POS_NORM_UV4:
			case ReD3DX9VertexType::POS_NORM_UV4D: {
				m_VSize = sizeof(ReOutputVertex5_D3DX9);
				// restore the vertex buffer
				ReOutputVertex5_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0, j = 0; i < numV; i++, j += 4) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->NX = pNorm[i].x;
						pVB->NY = pNorm[i].y;
						pVB->NZ = pNorm[i].z;
						pVB->U0 = pUv[j].x;
						pVB->V0 = pUv[j].y;
						pVB->U1 = pUv[j + 1].x;
						pVB->V1 = pUv[j + 1].y;
						pVB->U2 = pUv[j + 2].x;
						pVB->V2 = pUv[j + 2].y;
						pVB->U3 = pUv[j + 3].x;
						pVB->V3 = pUv[j + 3].y;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;

			case ReD3DX9VertexType::POS_NORM_UV1_DS: {
				m_VSize = sizeof(ReOutputVertex6_D3DX9);
				// restore the vertex buffer
				ReOutputVertex6_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0; i < numV; i++) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->U = pUv[i].x;
						pVB->V = pUv[i].y;
						pVB->Diff = 0x00aaaaaa;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;

			case ReD3DX9VertexType::POS_NORM_UV2_DS: {
				m_VSize = sizeof(ReOutputVertex7_D3DX9);
				// restore the vertex buffer
				ReOutputVertex7_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0, j = 0; i < numV; i++, j += 2) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->U0 = pUv[j].x;
						pVB->V0 = pUv[j].y;
						pVB->U1 = pUv[j + 1].x;
						pVB->V1 = pUv[j + 1].y;
						pVB->Diff = 0x00aaaaaa;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;

			case ReD3DX9VertexType::POS_NORM_UV3_DS: {
				m_VSize = sizeof(ReOutputVertex8_D3DX9);
				// restore the vertex buffer
				ReOutputVertex8_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0, j = 0; i < numV; i++, j += 3) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->U0 = pUv[j].x;
						pVB->V0 = pUv[j].y;
						pVB->U1 = pUv[j + 1].x;
						pVB->V1 = pUv[j + 1].y;
						pVB->U2 = pUv[j + 2].x;
						pVB->V2 = pUv[j + 2].y;
						pVB->Diff = 0x00aaaaaa;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;

			case ReD3DX9VertexType::POS_NORM_UV4_DS: {
				m_VSize = sizeof(ReOutputVertex9_D3DX9);
				// restore the vertex buffer
				ReOutputVertex9_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0, j = 0; i < numV; i++, j += 4) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->U0 = pUv[j].x;
						pVB->V0 = pUv[j].y;
						pVB->U1 = pUv[j + 1].x;
						pVB->V1 = pUv[j + 1].y;
						pVB->U2 = pUv[j + 2].x;
						pVB->V2 = pUv[j + 2].y;
						pVB->U3 = pUv[j + 3].x;
						pVB->V3 = pUv[j + 3].y;
						pVB->Diff = 0x00aaaaaa;
						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;
		}
	}

	// index buffer not created yet
	if (m_ResIB == ReD3DX9DEVICEINVALID && numI > 0) {
		// tristripped index buffer
		if (primType == ReMeshPrimType::TRISTRIP) {
			if (!m_pDev->CreateStaticResource(ReD3DX9ResourceType::INDEX_BUFFER_TRIS, m_VertexType, numI, (void**)&m_pIB, &m_ResIB))
				return FALSE;
		}
		// All other PrimTypes uses INDEX_BUFFER type for now. Not sure why we need two different resource types. They are handled the same AFAICS.
		else {
			if (!m_pDev->CreateStaticResource(ReD3DX9ResourceType::INDEX_BUFFER, m_VertexType, numI, (void**)&m_pIB, &m_ResIB))
				return FALSE;
		}

		// restore the index buffer
		WORD* pIB;
		if (m_pIB && SUCCEEDED(D3DCheck(m_pIB->Lock(0, numI * sizeof(WORD), (void**)&pIB, 0)))) {
			memcpy((void*)pIB, (void*)pI, sizeof(WORD) * numI);
			m_pIB->Unlock();
		}
	}

	// vertex declaration not created yet
	if (m_ResVD == ReD3DX9DEVICEINVALID) {
		switch (m_VertexType) {
			case ReD3DX9VertexType::POS_NORM_UV1:
			case ReD3DX9VertexType::POS_NORM_UV1D:
				m_HashVD = 0x00000017;
				break;

			case ReD3DX9VertexType::POS_NORM_UV2:
			case ReD3DX9VertexType::POS_NORM_UV2D:
				m_HashVD = 0x00000018;
				break;

			case ReD3DX9VertexType::POS_NORM_UV3:
			case ReD3DX9VertexType::POS_NORM_UV3D:
				m_HashVD = 0x00000019;
				break;

			case ReD3DX9VertexType::POS_NORM_UV4:
			case ReD3DX9VertexType::POS_NORM_UV4D:
				m_HashVD = 0x0000001a;
				break;

			case ReD3DX9VertexType::POS_NORM_UV1_DS:
				m_HashVD = 0x0000001b;
				break;

			case ReD3DX9VertexType::POS_NORM_UV2_DS:
				m_HashVD = 0x0000001c;
				break;

			case ReD3DX9VertexType::POS_NORM_UV3_DS:
				m_HashVD = 0x0000001d;
				break;

			case ReD3DX9VertexType::POS_NORM_UV4_DS:
				m_HashVD = 0x0000001e;
				break;

			default:
				m_HashVD = 0xffffffff;
				break;
		}
		// vertex declaration
		if (m_ResVD == ReD3DX9DEVICEINVALID) {
			if (!m_pDev->CreateSystemResource(ReD3DX9ResourceType::VERTEX_DECLARATION, (ReD3DX9VertexType)m_VertexType, &m_HashVD, (void**)&m_pVD, &m_ResVD))
				return FALSE;
		}
	}
	return TRUE;
}

// restore device-dependent geometry of GPU-skinned meshes
BOOL ReD3DX9Mesh::Restore(D3DXVECTOR3* pPos, D3DXVECTOR3* pNorm, D3DXVECTOR2* pUv, WORD* pI, BYTE* pBoneIx, float* pWt, DWORD wpv, DWORD numV, DWORD numI, ReMeshPrimType primType) {
	if (!m_pDev)
		return FALSE;

	BOOL restoreVB = FALSE;

	// vertex buffer not created yet
	if (m_ResVB == ReD3DX9DEVICEINVALID) {
		if (!m_pDev->CreateStaticResource(ReD3DX9ResourceType::VERTEX_BUFFER, (ReD3DX9VertexType)m_VertexType, numV, (void**)&m_pVB, &m_ResVB))
			return FALSE;
		restoreVB = TRUE;
	}

	// index buffer not created yet
	if (m_ResIB == ReD3DX9DEVICEINVALID && numI > 0) {
		// tristripped index buffer
		if (primType == ReMeshPrimType::TRISTRIP) {
			if (!m_pDev->CreateStaticResource(ReD3DX9ResourceType::INDEX_BUFFER_TRIS, (ReD3DX9VertexType)m_VertexType, numI, (void**)&m_pIB, &m_ResIB))
				return FALSE;
		}
		// All other PrimTypes uses INDEX_BUFFER type for now. Not sure why we need two different resource types. They are handled the same AFAICS.
		else {
			if (!m_pDev->CreateStaticResource(ReD3DX9ResourceType::INDEX_BUFFER, (ReD3DX9VertexType)m_VertexType, numI, (void**)&m_pIB, &m_ResIB))
				return FALSE;
		}

		// restore the index buffer
		WORD* pIB;
		if (m_pIB && SUCCEEDED(D3DCheck(m_pIB->Lock(0, numI * sizeof(WORD), (void**)&pIB, 0)))) {
			memcpy((void*)pIB, (void*)pI, sizeof(WORD) * numI);
			m_pIB->Unlock();
		}
	}

	if (restoreVB) {
		// restore the vertex buffer
		switch (m_VertexType) {
			// 1.0 and above GPU skinned lit & textured vertex (i.e. with indexes & weights)
			case ReD3DX9VertexType::POS_NORM_WTXUV1:
			case ReD3DX9VertexType::POS_NORM_WTXUV2:
			case ReD3DX9VertexType::POS_NORM_WTXUV3:
			case ReD3DX9VertexType::POS_NORM_WTXUV4: {
				m_VSize = sizeof(ReOutputVertex1_D3DX9);
				ReOutputVertex1_D3DX9* pVB = 0;
				if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * m_VSize, (void**)&pVB, 0)))) {
					for (DWORD i = 0; i < numV; i++) {
						pVB->X = pPos[i].x;
						pVB->Y = pPos[i].y;
						pVB->Z = pPos[i].z;
						pVB->NX = pNorm[i].x;
						pVB->NY = pNorm[i].y;
						pVB->NZ = pNorm[i].z;
						pVB->U = pUv[i].x;
						pVB->V = pUv[i].y;
						pVB->Bx = 0;

						for (unsigned int j = 0; j < wpv; j++) {
							pVB->Wt[j] = pWt[i * wpv + j];
							DWORD bx = pBoneIx[i * wpv + j];
							pVB->Bx |= bx << j * 8;
						}

						pVB++;
					}
					((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				}
			} break;
		}
	}

	// vertex declaration not created yet
	if (m_ResVD == ReD3DX9DEVICEINVALID) {
		switch (m_VertexType) {
			case ReD3DX9VertexType::POS_WTX:
				m_HashVD = 0x0000001f;
				break;

			case ReD3DX9VertexType::POS_NORM_WTXUV1:
				m_HashVD = 0x00000020;
				break;

			case ReD3DX9VertexType::POS_NORM_WTXUV2:
				m_HashVD = 0x00000021;
				break;

			case ReD3DX9VertexType::POS_NORM_WTXUV3:
				m_HashVD = 0x00000022;
				break;

			case ReD3DX9VertexType::POS_NORM_WTXUV4:
				m_HashVD = 0x00000023;
				break;

			default:
				m_HashVD = 0xffffffff;
				break;
		}

		// vertex declaration
		if (!m_pDev->CreateSystemResource(ReD3DX9ResourceType::VERTEX_DECLARATION, (ReD3DX9VertexType)m_VertexType, &m_HashVD, (void**)&m_pVD, &m_ResVD))
			return FALSE;
	}

	return TRUE;
}

void ReD3DX9Mesh::Render(DWORD numV, DWORD numI, ReMeshPrimType primType, BOOL indexed) {
	if (Invalid())
		return;

	IDirect3DDevice9* pDevice = m_pDev->GetDevice();
	if (!pDevice)
		return;

	if (m_pDev->VertexDeclRender(m_HashVD))
		D3DCheck(pDevice->SetVertexDeclaration(m_pVD));

	if (m_pDev->IndexBufferRender(m_HashIB))
		D3DCheck(pDevice->SetIndices(m_pIB));

	if (m_pDev->VertexBufferRender(m_HashVB))
		D3DCheck(pDevice->SetStreamSource(0, m_pVB, 0, m_VSize));

	auto renderMode = m_pDev->GetRenderState()->GetRenderMode();
	assert(renderMode == ReRenderMode::SOLID);
	renderMode;
	if (indexed) {
		switch (primType) {
			case ReMeshPrimType::TRISTRIP:
				D3DCheck(pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, numV, 0, numI - 2));
				break;

			case ReMeshPrimType::TRILIST:
				D3DCheck(pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, numV, 0, numI / 3));
				break;

			case ReMeshPrimType::LINESTRIP:
				D3DCheck(pDevice->DrawIndexedPrimitive(D3DPT_LINESTRIP, 0, 0, numV, 0, numI - 1));
				break;

			case ReMeshPrimType::LINELIST:
				D3DCheck(pDevice->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, numV, 0, numI / 2));
				break;

			default:
				assert(false);
				break;
		}
	} else {
		switch (primType) {
			case ReMeshPrimType::TRISTRIP:
				D3DCheck(pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, numV - 2));
				break;

			case ReMeshPrimType::TRILIST:
				D3DCheck(pDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, numV / 3));
				break;

			case ReMeshPrimType::LINESTRIP:
				D3DCheck(pDevice->DrawPrimitive(D3DPT_LINESTRIP, 0, numV - 1));
				break;

			case ReMeshPrimType::LINELIST:
				D3DCheck(pDevice->DrawPrimitive(D3DPT_LINELIST, 0, numV / 2));
				break;

			default:
				assert(false);
				break;
		}
	}
}

BOOL ReD3DX9Mesh::Lock(D3DXVECTOR3* pos, DWORD numV) {
	switch (m_VertexType) {
		case ReD3DX9VertexType::POS_NORM_UV1:
		case ReD3DX9VertexType::POS_NORM_UV1D: {
			// copy the vertex buffer positions into "pos"
			ReOutputVertex0_D3DX9* pVB = NULL;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * sizeof(ReOutputVertex0_D3DX9), (void**)&pVB, 0)))) {
				for (DWORD i = 0; i < numV; i++) {
					pos[i].x = pVB->X;
					pos[i].y = pVB->Y;
					pos[i].z = pVB->Z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				return true;
			}
			return FALSE;
		}

		case ReD3DX9VertexType::POS_NORM_UV2:
		case ReD3DX9VertexType::POS_NORM_UV2D: {
			// copy the vertex buffer positions into "pos"
			ReOutputVertex3_D3DX9* pVB = NULL;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * sizeof(ReOutputVertex3_D3DX9), (void**)&pVB, 0)))) {
				for (DWORD i = 0; i < numV; i++) {
					pos[i].x = pVB->X;
					pos[i].y = pVB->Y;
					pos[i].z = pVB->Z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				return true;
			}
			return FALSE;
		}

		case ReD3DX9VertexType::POS_NORM_UV3:
		case ReD3DX9VertexType::POS_NORM_UV3D: {
			// copy the vertex buffer positions into "pos"
			ReOutputVertex4_D3DX9* pVB = NULL;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * sizeof(ReOutputVertex4_D3DX9), (void**)&pVB, 0)))) {
				for (DWORD i = 0; i < numV; i++) {
					pos[i].x = pVB->X;
					pos[i].y = pVB->Y;
					pos[i].z = pVB->Z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				return true;
			}
			return FALSE;
		}

		case ReD3DX9VertexType::POS_NORM_UV4:
		case ReD3DX9VertexType::POS_NORM_UV4D: {
			// copy the vertex buffer positions into "pos"
			ReOutputVertex5_D3DX9* pVB = NULL;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * sizeof(ReOutputVertex5_D3DX9), (void**)&pVB, 0)))) {
				for (DWORD i = 0; i < numV; i++) {
					pos[i].x = pVB->X;
					pos[i].y = pVB->Y;
					pos[i].z = pVB->Z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				return true;
			}
			return FALSE;
		}

		case ReD3DX9VertexType::POS_NORM_WTXUV1: {
			// copy the vertex buffer positions into "pos"
			ReOutputVertex1_D3DX9* pVB = NULL;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * sizeof(ReOutputVertex1_D3DX9), (void**)&pVB, 0)))) {
				for (DWORD i = 0; i < numV; i++) {
					pos[i].x = pVB->X;
					pos[i].y = pVB->Y;
					pos[i].z = pVB->Z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				return true;
			}
			return FALSE;
		}

		case ReD3DX9VertexType::POS_WTX: {
			// copy the vertex buffer positions into "pos"
			ReOutputVertex2_D3DX9* pVB = NULL;
			if (m_pVB && SUCCEEDED(D3DCheck(((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Lock(0, numV * sizeof(ReOutputVertex2_D3DX9), (void**)&pVB, 0)))) {
				for (DWORD i = 0; i < numV; i++) {
					pos[i].x = pVB->X;
					pos[i].y = pVB->Y;
					pos[i].z = pVB->Z;
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_pVB)->Unlock();
				return true;
			}
			return FALSE;
		}

		default:;
	}

	return FALSE;
}

void ReD3DX9Mesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	m_pVB;                  // LPDIRECT3DVERTEXBUFFER9
	m_pIB;                  // LPDIRECT3DINDEXBUFFER9                      
	m_pVD;                  // LPDIRECT3DVERTEXDECLARATION9                
	-------------------------------------------------------------------------------*/
}

ReLightCache::ReLightCache() {
	memset(&Lights, 0, sizeof(ReLight*) * MAXRESHADERLIGHTS);
	NumLights = 0;
}

ReLightCache::~ReLightCache() {
	for (UINT i = 0; i < MAXRESHADERLIGHTS; i++) {
		if (Lights[i]) {
			delete Lights[i];
			Lights[i] = NULL;
		}
	}
}

void ReLightCache::LoadLights(const ReLight** lights, const UINT numLights) {
	if (NumLights != 0 || numLights > MAXRESHADERLIGHTS) {
		return;
	}

	ReLight* newLight = NULL;
	for (DWORD i = 0; i < numLights; i++) {
		// create the light if necessary
		if (Lights[i] == NULL) {
			newLight = new ReLight;
			Lights[i] = newLight;
		}
		// else just load it
		else {
			newLight = Lights[i];
		}
		memcpy(newLight, lights[i], sizeof(ReLight));

		NumLights++;
	}
}

ReD3DX9DeviceRenderState::ReD3DX9DeviceRenderState() {
	m_pD3DDevice = NULL;
	m_MaxLights = 0;
	m_NumSortedLights = 0;
	m_MaxConstants = 96;
	m_HWCaps = ReD3DX9HardwareCaps::CPU_FEATURE_NONE;
	m_RenderMode = ReRenderMode::SOLID;
	m_MaxUploadedLights = ReD3DX9DEVICEMAXUPLOADLIGHTS;
	m_GlobalAmbient = D3DXVECTORA4(0.f, 0.f, 0.f, 0.f);
	m_FogColor = D3DXVECTORA4(0.f, 0.f, 0.f, 0.f);
	m_FogRange = D3DXVECTORA4(0.f, 0.f, 0.f, 0.f);
	m_HWRating = 0;
	m_PreLighting = TRUE;

	D3DXMatrixIdentity(&m_ViewMatrix);
	D3DXMatrixIdentity(&m_ProjMatrix);
	D3DXMatrixIdentity(&m_ProjViewMatrix);
}

const void ReD3DX9DeviceRenderState::SetHardwareCaps() {
	if (GetDevice() == NULL) {
		return;
	}

	D3DCAPS9 caps;
	GetDevice()->GetDeviceCaps(&caps);

	// get machine's vector processor caps
	_p_info info;
	_cpuid(&info);
	int avail = info.feature;
	int mask = info.checks;

	// store the max vertex shader constants & max fixed func lights
	m_MaxConstants = caps.MaxVertexShaderConst;
	m_MaxLights = (int)caps.MaxActiveLights < 0 ? ReD3DX9DEVICEMAXUPLOADLIGHTS : caps.MaxActiveLights;
	m_HWCaps = ReD3DX9HardwareCaps::CPU_FEATURE_NONE;

	// Vertex shaders.....
	if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) != 0) {
		// try and get vertex shader support
		if (caps.VertexShaderVersion >= D3DVS_VERSION(3, 0)) {
			m_HWCaps = ReD3DX9HardwareCaps::GPU_FEATURE_3_1;
		} else if (caps.VertexShaderVersion >= D3DVS_VERSION(2, 1)) {
			m_HWCaps = ReD3DX9HardwareCaps::GPU_FEATURE_2_1;
		} else if (caps.VertexShaderVersion >= D3DVS_VERSION(2, 0)) {
			m_HWCaps = ReD3DX9HardwareCaps::GPU_FEATURE_2_0;
		} else if (caps.VertexShaderVersion >= D3DVS_VERSION(1, 1)) {
			m_HWCaps = ReD3DX9HardwareCaps::GPU_FEATURE_1_1;
		}
	}

	m_HWCaps = ReD3DX9HardwareCaps::GPU_FEATURE_NONE; // DISABLE GPU !!!!!!!!!!!!!!!!!!!!!!!!!!

	if (m_HWCaps < ReD3DX9MINVERTEXSHADERSUPPORT) {
		// Intel MMX and SSE support
		if (!strncmp("GenuineIntel", info.v_name, 12)) {
			if ((mask & _CPU_FEATURE_MMX) && (avail & _CPU_FEATURE_MMX)) {
				m_HWCaps = ReD3DX9HardwareCaps::CPU_FEATURE_MMX;
			}
			if ((mask & _CPU_FEATURE_SSE) && (avail & _CPU_FEATURE_SSE)) {
				m_HWCaps = ReD3DX9HardwareCaps::CPU_FEATURE_SSE;
			}
			if ((mask & _CPU_FEATURE_SSE2) && (avail & _CPU_FEATURE_SSE2)) {
				m_HWCaps = ReD3DX9HardwareCaps::CPU_FEATURE_SSE2;
			}
		} else {
			m_HWCaps = ReD3DX9HardwareCaps::CPU_FEATURE_NONE;
		}
	}

#if defined(LOG_HWCAPS)
	char cpubuf[256];
	char vsbuf[256];
	FILE* pFile = NULL;

	// init the file read buffer with null termination chars
	char printbuf[4096];
	for (DWORD i = 0; i < 4096; i++)
		printbuf[i] = '\0';

	char hwbuf[256];
	sprintf_s(&hwbuf[0], _countof(hwbuf), "NA_D3DX9HARDWARECAPS");

	switch (m_HWCaps) {
		case ReD3DX9HardwareCaps::CPU_FEATURE_3DNOW:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "CPU_FEATURE_3DNOW");
			break;

		case ReD3DX9HardwareCaps::CPU_FEATURE_MMX:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "CPU_FEATURE_MMX");
			break;

		case ReD3DX9HardwareCaps::CPU_FEATURE_SSE:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "CPU_FEATURE_SSE");
			break;

		case ReD3DX9HardwareCaps::CPU_FEATURE_SSE2:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "CPU_FEATURE_SSE2");
			break;

		case ReD3DX9HardwareCaps::GPU_FEATURE_1_1:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "GPU_FEATURE_1_1");
			break;

		case ReD3DX9HardwareCaps::GPU_FEATURE_2_0:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "GPU_FEATURE_2_0");
			break;

		case ReD3DX9HardwareCaps::GPU_FEATURE_2_1:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "GPU_FEATURE_2_1");
			break;

		case ReD3DX9HardwareCaps::GPU_FEATURE_3_1:
			sprintf_s(&hwbuf[0], _countof(hwbuf), "GPU_FEATURE_3_1");
			break;
	}

	sprintf_s(&vsbuf[0], _countof(vsbuf), "NO HARDWARE T&L");
	if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) != 0) {
		// print out the dev caps
		if (caps.VertexShaderVersion >= D3DVS_VERSION(3, 0)) {
			sprintf_s(&vsbuf[0], _countof(vsbuf), "GPU_FEATURE_3_1");
		} else if (caps.VertexShaderVersion >= D3DVS_VERSION(2, 1)) {
			sprintf_s(&vsbuf[0], _countof(vsbuf), "GPU_FEATURE_2_1");
		} else if (caps.VertexShaderVersion >= D3DVS_VERSION(2, 0)) {
			sprintf_s(&vsbuf[0], _countof(vsbuf), "GPU_FEATURE_2_0");
		} else if (caps.VertexShaderVersion >= D3DVS_VERSION(1, 1)) {
			sprintf_s(&vsbuf[0], _countof(vsbuf), "GPU_FEATURE_1_1");
		}
	}

	sprintf_s(&cpubuf[0], _countof(cpubuf), "NO VECTOR PROCESSOR");
	if ((mask & _CPU_FEATURE_3DNOW) && (avail & _CPU_FEATURE_3DNOW)) // AMD vector processor
	{
		sprintf_s(&cpubuf[0], _countof(cpubuf), "CPU_FEATURE_3DNOW");
	}
	if ((mask & _CPU_FEATURE_MMX) && (avail & _CPU_FEATURE_MMX)) {
		sprintf_s(&cpubuf[0], _countof(cpubuf), "CPU_FEATURE_MMX");
	}
	if ((mask & _CPU_FEATURE_SSE) && (avail & _CPU_FEATURE_SSE)) {
		sprintf_s(&cpubuf[0], _countof(cpubuf), "CPU_FEATURE_SSE");
	}
	if ((mask & _CPU_FEATURE_SSE2) && (avail & _CPU_FEATURE_SSE2)) {
		sprintf_s(&cpubuf[0], _countof(cpubuf), "CPU_FEATURE_SSE2");
	}

	// get device creation parameters
	D3DDEVICE_CREATION_PARAMETERS pParameters;
	GetDevice()->GetCreationParameters(&pParameters);

	// print out SSE & GPU support
	sprintf_s(&printbuf[0], _countof(printbuf), "current hw setting                    %s \n", hwbuf);
	size_t sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "sse caps                              %s \n", cpubuf);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "gpu caps                              %s \n", vsbuf);
	// print out device creation flags
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "device type                           %d \n", pParameters.DeviceType);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_HARDWARE_VERTEXPROCESSING   %d \n", (pParameters.BehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_MIXED_VERTEXPROCESSING      %d \n", (pParameters.BehaviorFlags & D3DCREATE_MIXED_VERTEXPROCESSING) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_SOFTWARE_VERTEXPROCESSING   %d \n", (pParameters.BehaviorFlags & D3DCREATE_SOFTWARE_VERTEXPROCESSING) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_ADAPTERGROUP_DEVICE         %d \n", (pParameters.BehaviorFlags & D3DCREATE_ADAPTERGROUP_DEVICE) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_DISABLE_DRIVER_MANAGEMENT   %d \n", (pParameters.BehaviorFlags & D3DCREATE_DISABLE_DRIVER_MANAGEMENT) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_DISABLE_DRIVER_MANAGEMENT_EX%d \n", (pParameters.BehaviorFlags & D3DCREATE_DISABLE_DRIVER_MANAGEMENT_EX) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_FPU_PRESERVE                %d \n", (pParameters.BehaviorFlags & D3DCREATE_FPU_PRESERVE) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_MULTITHREADED               %d \n", (pParameters.BehaviorFlags & D3DCREATE_MULTITHREADED) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_NOWINDOWCHANGES             %d \n", (pParameters.BehaviorFlags & D3DCREATE_NOWINDOWCHANGES) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCREATE_PUREDEVICE                  %d \n", (pParameters.BehaviorFlags & D3DCREATE_PUREDEVICE) != 0);
	// print out device caps
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "texture video memory                  %d \n", GetDevice()->GetAvailableTextureMem());
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCAPS3_COPY_TO_VIDMEM               %d \n", (caps.Caps3 & 0x00000100L) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DCAPS3_COPY_TO_SYSTEMMEM            %d \n", (caps.Caps3 & 0x00000200L) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_CANBLTSYSTONONLOCAL        %d \n", (caps.DevCaps & D3DDEVCAPS_CANBLTSYSTONONLOCAL) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_CANRENDERAFTERFLIP         %d \n", (caps.DevCaps & D3DDEVCAPS_CANRENDERAFTERFLIP) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "DirectX 5.0 compliant driver          %d \n", (caps.DevCaps & D3DDEVCAPS_DRAWPRIMITIVES2) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "DirectX 7.0 compliant driver          %d \n", (caps.DevCaps & D3DDEVCAPS_DRAWPRIMITIVES2EX) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_DRAWPRIMTLVERTEX           %d \n", (caps.DevCaps & D3DDEVCAPS_DRAWPRIMTLVERTEX) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_EXECUTESYSTEMMEMORY        %d \n", (caps.DevCaps & D3DDEVCAPS_EXECUTESYSTEMMEMORY) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_EXECUTEVIDEOMEMORY         %d \n", (caps.DevCaps & D3DDEVCAPS_EXECUTEVIDEOMEMORY) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_HWRASTERIZATION            %d \n", (caps.DevCaps & D3DDEVCAPS_HWRASTERIZATION) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_HWTRANSFORMANDLIGHT        %d \n", (caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_PUREDEVICE                 %d \n", (caps.DevCaps & D3DDEVCAPS_PUREDEVICE) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_SEPARATETEXTUREMEMORIES    %d \n", (caps.DevCaps & D3DDEVCAPS_SEPARATETEXTUREMEMORIES) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_TEXTURENONLOCALVIDMEM      %d \n", (caps.DevCaps & D3DDEVCAPS_TEXTURENONLOCALVIDMEM) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_TEXTURESYSTEMMEMORY        %d \n", (caps.DevCaps & D3DDEVCAPS_TEXTURESYSTEMMEMORY) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_TEXTUREVIDEOMEMORY         %d \n", (caps.DevCaps & D3DDEVCAPS_TEXTUREVIDEOMEMORY) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_TLVERTEXSYSTEMMEMORY       %d \n", (caps.DevCaps & D3DDEVCAPS_TLVERTEXSYSTEMMEMORY) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DDEVCAPS_TLVERTEXVIDEOMEMORY        %d \n", (caps.DevCaps & D3DDEVCAPS_TLVERTEXVIDEOMEMORY) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DPTEXTURECAPS_MIPMAP                %d \n", (caps.TextureCaps & D3DPTEXTURECAPS_MIPMAP) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxTextureWidth                       %d \n", caps.MaxTextureWidth);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxTextureHeight                      %d \n", caps.MaxTextureHeight);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DFVFCAPS_DONOTSTRIPELEMENTS         %d \n", (caps.FVFCaps & D3DFVFCAPS_DONOTSTRIPELEMENTS) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxTextureBlendStages in FF pipe      %d \n", caps.MaxTextureBlendStages);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxSimultaneousTextures in FF pipe    %d \n", caps.MaxSimultaneousTextures);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxActiveLights                       %d \n", caps.MaxActiveLights);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxStreams.. if 0, NOT DX9.0 driver   %d \n", caps.MaxStreams);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxVertexShaderConst                  %d \n", caps.MaxVertexShaderConst);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "NumSimultaneousRTs                    %d \n", caps.NumSimultaneousRTs);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxVShaderInstructionsExecuted        %d \n", caps.MaxVShaderInstructionsExecuted);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxPrimitiveCount                     %d \n", caps.MaxPrimitiveCount);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "MaxVertexIndex                        %d \n", caps.MaxVertexIndex);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_DIRECTIONALLIGHTS         %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_DIRECTIONALLIGHTS) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_LOCALVIEWER               %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_LOCALVIEWER) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_MATERIALSOURCE7           %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_LOCALVIEWER) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_NO_TEXGEN_NONLOCALVIEWER  %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_NO_TEXGEN_NONLOCALVIEWER) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_POSITIONALLIGHTS          %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_POSITIONALLIGHTS) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_TEXGEN                    %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_TEXGEN) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_TEXGEN_SPHEREMAP          %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_TEXGEN_SPHEREMAP) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DVTXPCAPS_TWEENING                  %d \n", (caps.VertexProcessingCaps & D3DVTXPCAPS_TWEENING) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DPRASTERCAPS_DEPTHBIAS              %d \n", (caps.RasterCaps & D3DPRASTERCAPS_DEPTHBIAS) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DPRASTERCAPS_SLOPESCALEDEPTHBIAS    %d \n", (caps.RasterCaps & D3DPRASTERCAPS_SLOPESCALEDEPTHBIAS) != 0);
	sl = strlen(&printbuf[0]);
	sprintf_s(&printbuf[sl], _countof(printbuf) - sl, "D3DPRASTERCAPS_MIPMAPLODBIAS          %d \n", (caps.RasterCaps & D3DPRASTERCAPS_MIPMAPLODBIAS) != 0);

	// printout the caps to "HWCAPS.txt"
	pFile = 0;
	if (fopen_s(&pFile, "HWCAPS.txt", "wt") == 0 && pFile) {
		fwrite(&printbuf[0], 1, strlen(&printbuf[0]), pFile);
		fclose(pFile);
	}
#endif
}

void ReD3DX9DeviceRenderState::FlushLightCache() {
	// disable all hardware lights
	for (DWORD i = 0; i < m_MaxLights; i++) {
		m_pD3DDevice->LightEnable(i, FALSE);
	}
	m_LightCache.clear();
	// init the sorted light array
	m_LightSort.clear();
	for (DWORD i = 0; i < std::min<size_t>(m_LightCache.size(), ReD3DX9DEVICERENDERSTATELIGHTS); i++)
		m_LightSort.push_back(&m_LightCache[i]);
}

void ReD3DX9DeviceRenderState::LoadLight(const D3DLIGHT9* light) {
	size_t m_NumLights = m_LightCache.size();
	m_LightCache.resize(m_NumLights + 1);

	// copy over light properties
	m_LightCache[m_NumLights].Ambient = D3DXVECTOR4(light->Ambient.r, light->Ambient.g, light->Ambient.b, 0.f);
	m_LightCache[m_NumLights].Diffuse = D3DXVECTOR4(light->Diffuse.r, light->Diffuse.g, light->Diffuse.b, 0.f);
	m_LightCache[m_NumLights].Specular = D3DXVECTOR4(light->Specular.r, light->Specular.g, light->Specular.b, 0.f);

	// copy over the light data for fixed func pipeline
	m_LightCache[m_NumLights].Light = *light;

	// get the directional light
	if (m_LightCache[m_NumLights].Light.Type == D3DLIGHT_DIRECTIONAL) {
		// store directional light direction in "Position"
		m_LightCache[m_NumLights].Position = D3DXVECTOR4(light->Direction.x, light->Direction.y, light->Direction.z, 0.f);
		// store "infinite radius" factor in w
		m_LightCache[m_NumLights].Attenuation = D3DXVECTOR4(0.f, 0.f, 0.f, ReRealMAXVAL);
	} else if (m_LightCache[m_NumLights].Light.Type == D3DLIGHT_POINT) {
		m_LightCache[m_NumLights].Position = D3DXVECTOR4(light->Position.x, light->Position.y, light->Position.z, 0.f);
		m_LightCache[m_NumLights].Attenuation = D3DXVECTOR4(light->Attenuation0, light->Attenuation1, light->Attenuation2, 0.f);
	}
}

void ReD3DX9DeviceRenderState::SortLights(const D3DXVECTOR3* pos, FLOAT radius, ReLightCache* lightCache) {
	m_LightSort.clear();

	// check all cached lights
	for (DWORD i = 0; i < m_LightCache.size(); i++) {
		// add rangeless directional lights
		if (m_LightCache[i].Light.Type == D3DLIGHT_DIRECTIONAL) {
			// add the light to the sorted light array
			m_LightCache[i].Attenuation.w = FLT_MAX;
			m_LightSort.push_back(&m_LightCache[i]);
			continue;
		}

		// get vector from "pos" to light
		D3DXVECTOR3 lightVec = *pos - (D3DXVECTOR3)m_LightCache[i].Light.Position;
		FLOAT dsq = D3DXVec3LengthSq(&lightVec);
		FLOAT r = (m_LightCache[i].Light.Range + radius) * ReD3DX9DEVICERENDERSTATERADIUSADJ;
		// skip light if "pos" outside its range
		if (dsq >= r * r)
			continue;

		FLOAT d = D3DXVec3Length(&lightVec);

		// calculate the light attenuation & store in "w"
		D3DXVECTOR4& attenV = m_LightCache[i].Attenuation;
		FLOAT atten = attenV.x + attenV.y * d + attenV.z * d * d;

		// Igore extremely dim lights.
		if (atten > 256)
			continue;

		m_LightCache[i].Attenuation.w = atten != 0 ? 1 / atten : FLT_MAX;

		//	Stick light range in w component of position for shader
		m_LightCache[i].Position.w = m_LightCache[i].Light.Range;

		// add the light to the sorted light array
		m_LightSort.push_back(&m_LightCache[i]);
	}

	// Get closest lights if too many.
	if (m_LightSort.size() > ReLightCache::MAXRESHADERLIGHTS) {
		std::nth_element(
			m_LightSort.begin(),
			m_LightSort.begin() + ReLightCache::MAXRESHADERLIGHTS,
			m_LightSort.end(),
			[](ReD3DX9Light* left, ReD3DX9Light* right) { return left->Attenuation.w > right->Attenuation.w; });
		m_LightSort.resize(ReLightCache::MAXRESHADERLIGHTS);
	}

	if (lightCache)
		lightCache->LoadLights((const ReLight**)m_LightSort.data(), m_LightSort.size());
}

// assign a light list to current light state
void ReD3DX9DeviceRenderState::SetLights(const ReLightCache* lightCache) {
	DWORD num = lightCache->NumLights > ReD3DX9DEVICEMAXUPLOADLIGHTS ? ReD3DX9DEVICEMAXUPLOADLIGHTS : lightCache->NumLights;

	m_LightSort.resize(num);
	for (DWORD i = 0; i < num; i++) {
		m_LightSort[i] = (ReD3DX9Light*)lightCache->Lights[i];
	}
}

// load lights into fixed function device: call SortLights() or SetLights()
void ReD3DX9DeviceRenderState::RenderLights(BOOL shader) {
	// load light data arrays for upload to vertex constant registers
	if (shader) {
		for (UINT i = 0; i < m_LightSort.size(); i++) {
			m_LightAmbient[i] = m_LightSort[i]->Ambient;
			m_LightDiffuse[i] = m_LightSort[i]->Diffuse;
			m_LightDirection[i] = m_LightSort[i]->Position;
			m_LightPosition[i] = m_LightSort[i]->Position;
			m_LightSpecular[i] = m_LightSort[i]->Specular;

			m_LightAttenuation0[i] = m_LightSort[i]->Attenuation.x;
			m_LightAttenuation1[i] = m_LightSort[i]->Attenuation.y;
			m_LightAttenuation2[i] = m_LightSort[i]->Attenuation.z;

			m_LightRange[i] = m_LightSort[i]->Light.Range;
		}
	}
	// enable fixed function lights
	else {
		// enable the hardware lights from sorted light list
		for (size_t i = 0; i < m_MaxLights; ++i) {
			if (i < m_LightSort.size()) {
				m_pD3DDevice->SetLight(i, &m_LightSort[i]->Light);
				m_pD3DDevice->LightEnable(i, TRUE);
			} else {
				m_pD3DDevice->LightEnable(i, FALSE);
			}
		}

		// set global ambient
		D3DCOLOR amb = D3DCOLOR_COLORVALUE(GetGlobalAmbient()->x, GetGlobalAmbient()->y, GetGlobalAmbient()->z, 0.f);
		m_pD3DDevice->SetRenderState(D3DRS_AMBIENT, amb);
	}
}

ReD3DX9DeviceState* ReD3DX9DeviceState::ms_instance = NULL;

ReD3DX9DeviceState::ReD3DX9DeviceState() :
		m_lastTextureConstant(0),
		m_lastTextureFactor(0) {
	// reset the resource counters
	m_NumStaticResources = 0;
	m_NumDynamicResources = 0;
	m_NumSystemResources = 0;
	m_maxResourcesdynamic = ReD3DX9DEFAULTNUMRESDYNAMIC;
	m_maxResourcesstatic = ReD3DX9DEFAULTNUMRESSTATIC;
	m_maxResourcessystem = ReD3DX9DEFAULTNUMRESSYSTEM;
	m_maxResourcesdynamicInit = m_maxResourcesdynamic;
	m_maxResourcesstaticInit = m_maxResourcesstatic;
	m_Resource = NULL;
	m_ResourceSystem = NULL;
	m_NumShaderStates = 0;
	m_VertexBufferCurr = 0;
	m_IndexBufferCurr = 0;
	m_VertexShaderCurr = 0;
	m_VertexDeclCurr = 0;
	m_StateBlockCurr = 0;
	m_ConstantBlockCurr = 0;
	m_ModeStateCurr = 0;
	m_ColorSourceCurr = 0;
	m_MatStateCurr = 0;
	m_BlendStateCurr = 0;
	m_WorldMatArrayCurr = 0;
	m_WorldMatCurr = 0;
	m_ViewMatCurr = 0;
	m_ProjMatCurr = 0;
	m_ProjViewMatCurr = 0;
	m_CamPosCurr = 0;
	m_GlobalAmbnCurr = 0;
	m_LightAmbnCurr = 0;
	m_LightDiffCurr = 0;
	m_LightSpecCurr = 0;
	m_LightPosnCurr = 0;
	m_LightDirnCurr = 0;
	m_LightAtt0Curr = 0;
	m_LightAtt1Curr = 0;
	m_LightAtt2Curr = 0;

	// Texture stage data
	m_NumTexStages = 0;
	for (size_t i = 0; i < ReDEVICEMAXTEXTURESTAGES; i++) {
		m_TexStateCurr[i] = ReD3DX9DEVICEINVALID;
		m_TexCurr[i] = (UINT64)0xffffffffffffffff;
		m_TexMatCurr[i] = (UINT64)0xffffffffffffffff;
	}

	assert(ms_instance == NULL);
	ms_instance = this;
}

BOOL ReD3DX9DeviceState::Initialize(UINT maxDynamic, UINT maxStatic, UINT maxSystem) {
	m_maxResourcesdynamic = std::min(ReD3DX9DEVICEMAXRESOURCES, maxDynamic);
	m_maxResourcesstatic = std::min(ReD3DX9DEVICEMAXRESOURCES, maxStatic);
	m_maxResourcessystem = std::min(ReD3DX9DEVICEMAXRESOURCES, maxSystem);
	m_maxResourcesdynamicInit = m_maxResourcesdynamic;
	m_maxResourcesstaticInit = m_maxResourcesstatic;
	m_Resource = new ReD3DX9Resource[maxStatic + maxDynamic];
	if (m_Resource) {
		m_ResourceSystem = new ReD3DX9ResourceSystem[maxSystem];
		if (m_ResourceSystem) {
			// reset the static & dynamic resources
			for (UINT i = 0; i < m_maxResourcesstatic + m_maxResourcesdynamic; i++) {
				m_Resource[i].resource = NULL;
				m_Resource[i].owner = NULL;
			}
			// reset the system resources
			for (UINT i = 0; i < m_maxResourcessystem; i++) {
				m_ResourceSystem[i].resource = NULL;
				m_ResourceSystem[i].resType = ReD3DX9ResourceType::INVALID;
				m_ResourceSystem[i].resHash = ReD3DX9DEVICEINVALID;
				m_ResourceSystem[i].resOwners = NULL;
				m_ResourceSystem[i].resNumowners = 0;
			}
			ResetRenderState();
			return TRUE;
		}
	}
	Destroy();
	return FALSE;
}

void ReD3DX9DeviceState::Destroy() {
	InvalidateResources(RED3DX9RESOURCE_ALL, false);

	if (m_Resource) {
		delete[] m_Resource;
		m_Resource = NULL;
	}
	if (m_ResourceSystem) {
		delete[] m_ResourceSystem;
		m_ResourceSystem = NULL;
	}

	ms_instance = NULL;
}

void ReD3DX9DeviceState::ResetRenderState() {
	m_VertexBufferCurr = ReD3DX9DEVICEINVALID;
	m_IndexBufferCurr = ReD3DX9DEVICEINVALID;
	m_VertexShaderCurr = ReD3DX9DEVICEINVALID;
	m_VertexDeclCurr = ReD3DX9DEVICEINVALID;
	m_StateBlockCurr = ReD3DX9DEVICEINVALID;
	m_ConstantBlockCurr = ReD3DX9DEVICEINVALID;
	m_ModeStateCurr = ReD3DX9DEVICEINVALID;
	m_ColorSourceCurr = ReD3DX9DEVICEINVALID;
	m_MatStateCurr = ReD3DX9DEVICEINVALID;
	m_BlendStateCurr = ReD3DX9DEVICEINVALID;
	m_WorldMatrixCurr = NULL;
	SetNumTextureStages(0);
	for (DWORD i = 0; i < ReDEVICEMAXTEXTURESTAGES; i++) {
		m_TexStateCurr[i] = ReD3DX9DEVICEINVALID;
		m_TexCurr[i] = (UINT64)0xffffffffffffffff;
		m_TexMatCurr[i] = (UINT64)0xffffffffffffffff;
	}

	if (GetDevice()) {
		GetDevice()->SetVertexShader(NULL);
		GetDevice()->SetPixelShader(NULL);
	}
}

void ReD3DX9DeviceState::ResetEffectState() {
	m_ViewMatCurr = TRUE;
	m_ProjMatCurr = TRUE;
	m_ProjViewMatCurr = TRUE;
	m_CamPosCurr = TRUE;
	m_GlobalAmbnCurr = TRUE;
}

void ReD3DX9DeviceState::ResetEffectObjectState() {
	m_WorldMatArrayCurr = TRUE;
	m_WorldMatCurr = TRUE;
	m_LightAmbnCurr = TRUE;
	m_LightDiffCurr = TRUE;
	m_LightSpecCurr = TRUE;
	m_LightPosnCurr = TRUE;
	m_LightDirnCurr = TRUE;
	m_LightAtt0Curr = TRUE;
	m_LightAtt1Curr = TRUE;
	m_LightAtt2Curr = TRUE;
}

void ReD3DX9DeviceState::ResetTextureState() {
	m_NumTexStages = 0;
	for (DWORD i = 0; i < ReDEVICEMAXTEXTURESTAGES; i++) {
		m_TexStateCurr[i] = ReD3DX9DEVICEINVALID;
		m_TexCurr[i] = (UINT64)0xffffffffffffffff;
	}

	if (GetDevice()) {
		for (DWORD i = 0; i < 8; i++) {
			GetDevice()->SetTexture(i, NULL);
			GetDevice()->SetTextureStageState(i, D3DTSS_TEXCOORDINDEX, i);
			GetDevice()->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			GetDevice()->SetTextureStageState(i, D3DTSS_COLORARG2, D3DTA_CURRENT);
			GetDevice()->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
			GetDevice()->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
			GetDevice()->SetTextureStageState(i, D3DTSS_ALPHAARG2, D3DTA_CURRENT);
			GetDevice()->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
		}
		GetDevice()->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
		GetDevice()->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		GetDevice()->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		GetDevice()->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	}
}

// release the static and (or) dynamic resource list
void ReD3DX9DeviceState::InvalidateResources(UINT type, bool resetArray) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	UINT newStaticSize = m_maxResourcesstatic;
	UINT newDynamicSize = m_maxResourcesdynamic;

	// delete all static resources
	if (type & RED3DX9RESOURCE_STATIC) {
		for (UINT i = 0; i < m_NumStaticResources; i++) {
			if (m_Resource[i].resource) {
				// release the resource
				((IUnknown*)m_Resource[i].resource)->Release();
				// reset the resource entry
				m_Resource[i].resource = NULL;
			}
			// inform the owner that its resource is now invalid
			if (m_Resource[i].owner) {
				*m_Resource[i].owner = ReD3DX9DEVICEINVALID;
				// clear out the owner entry
				m_Resource[i].owner = NULL;
			}
		}
		m_NumStaticResources = 0;
		newStaticSize = m_maxResourcesstaticInit;
	}
	// delete all dynamic resources
	if (type & RED3DX9RESOURCE_DYNAMIC) {
		for (UINT i = m_maxResourcesstatic; i < m_maxResourcesstatic + m_NumDynamicResources; i++) {
			if (m_Resource[i].resource) {
				// release the resource
				((IUnknown*)m_Resource[i].resource)->Release();
				// reset the resource entry
				m_Resource[i].resource = NULL;
			}
			// inform the owner that its resource is now invalid
			if (m_Resource[i].owner) {
				*m_Resource[i].owner = ReD3DX9DEVICEINVALID;
				// clear out the owner entry
				m_Resource[i].owner = NULL;
			}
		}
		m_NumDynamicResources = 0;
		newDynamicSize = m_maxResourcesdynamicInit;
	}

	// Reset the array size (if previously grown)
	if (resetArray && (m_maxResourcesstatic > newStaticSize || m_maxResourcesdynamic > newDynamicSize)) {
		// Reallocate and copy data
		ReD3DX9Resource* newArray = new ReD3DX9Resource[newStaticSize + newDynamicSize];
		ZeroMemory(newArray, sizeof(ReD3DX9Resource) * (newStaticSize + newDynamicSize));

		for (DWORD i = 0; i < m_NumStaticResources; i++) {
			newArray[i] = m_Resource[i];
		}

		for (DWORD i = 0; i < m_NumDynamicResources; i++) {
			newArray[newStaticSize + i] = m_Resource[m_maxResourcesstatic + i];
			*newArray[newStaticSize + i].owner = newStaticSize + i;
		}

		// Dispose old array
		delete[] m_Resource;

		// Realize
		m_Resource = newArray;
		m_maxResourcesstatic = newStaticSize;
		m_maxResourcesdynamic = newDynamicSize;
	}

	// delete all system resources
	if (type & RED3DX9RESOURCE_SYSTEM) {
		for (UINT i = 0; i < m_NumSystemResources; i++) {
			if (m_ResourceSystem[i].resource) {
				// release the resource
				((IUnknown*)m_ResourceSystem[i].resource)->Release();
				// reset the resource entry
				m_ResourceSystem[i].resource = NULL;
				// clear out the resource type
				m_ResourceSystem[i].resType = ReD3DX9ResourceType::INVALID;
				// clear out the hash code
				m_ResourceSystem[i].resHash = ReD3DX9DEVICEINVALID;
				// invalidate the owners of this resource
				if (m_ResourceSystem[i].resOwners) {
					for (UINT j = 0; j < m_ResourceSystem[i].resNumowners; j++) {
						if (m_ResourceSystem[i].resOwners[j]) {
							*m_ResourceSystem[i].resOwners[j] = ReD3DX9DEVICEINVALID;
						}
					}
					delete[] m_ResourceSystem[i].resOwners;
					m_ResourceSystem[i].resOwners = NULL;
					m_ResourceSystem[i].resNumowners = 0;
				}
			}
		}
		m_NumSystemResources = 0;
	}

	// reset the cached render state
	ResetRenderState();
}

// invalidate a static or dynamic resource....called when the owner is destroyed
void ReD3DX9DeviceState::InvalidateResource(UINT id) {
	// resource not created yet, or has been cloned
	if (id == ReD3DX9DEVICEINVALID || id == ReD3DX9DEVICECLONED)
		return;

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	if (m_Resource && m_Resource[id].resource) {
		// release the resource
		((IUnknown*)m_Resource[id].resource)->Release();
		m_Resource[id].resource = NULL;

		// inform the owner that its resource is now invalid
		if (m_Resource[id].owner)
			*m_Resource[id].owner = ReD3DX9DEVICEINVALID;

		m_Resource[id].owner = NULL;

		UINT lastEntryId = id < m_maxResourcesstatic ? (m_NumStaticResources - 1) : (m_maxResourcesstatic + m_NumDynamicResources - 1);
		if (id != lastEntryId) {
			jsAssert(m_Resource[lastEntryId].owner != NULL);

			// Overwrite this entry with last valid entry
			m_Resource[id] = m_Resource[lastEntryId];
			if (m_Resource[id].owner) {
				*m_Resource[id].owner = id; // inform the owner of the swapped entry of index change
			}

			// reset the last valid entry
			m_Resource[lastEntryId].resource = NULL;
			m_Resource[lastEntryId].owner = NULL;
		}

		// decrement resource count
		if (id < m_maxResourcesstatic) {
			m_NumStaticResources--;
		} else {
			m_NumDynamicResources--;
		}
	}
}

// invalidate the owner of a system resource....called when the owner is destroyed
// TODO: System resource owner list cleanup & compaction!!!!!
void ReD3DX9DeviceState::InvalidateResource(UINT* id) {
	// resource not created yet, or has been cloned
	if (id == NULL || *id == ReD3DX9DEVICEINVALID || *id == ReD3DX9DEVICECLONED)
		return;

	UINT index = *id >> ReD3DX9DEVICESYSTEMRESOURCESHIFT;
	UINT owner = *id & ReD3DX9DEVICESYSTEMRESOURCEMASK;

	if (index >= m_NumSystemResources)
		return;

	ReD3DX9ResourceSystem* res = &m_ResourceSystem[index];

	if (res->resource) {
		// invalidate the owner of this resource
		if (res->resOwners) {
			if (owner < res->resNumowners) {
				if (res->resOwners[owner] == id) {
					*res->resOwners[owner] = ReD3DX9DEVICEINVALID;
					res->resOwners[owner] = NULL;
				}
			}
		}
	}
}

bool ReD3DX9DeviceState::GrowResourceArray(DWORD type) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	UINT newStaticSize = m_maxResourcesstatic;
	UINT newDynamicSize = m_maxResourcesdynamic;

	if (type & RED3DX9RESOURCE_STATIC) {
		// Double the capacity
		newStaticSize = std::min(ReD3DX9DEVICEMAXRESOURCES, m_maxResourcesstatic << 1);
	}

	if (type & RED3DX9RESOURCE_DYNAMIC) {
		// Double the capacity
		newDynamicSize = std::min(ReD3DX9DEVICEMAXRESOURCES, m_maxResourcesdynamic << 1);
	}

	if (newStaticSize == m_maxResourcesstatic && newDynamicSize == m_maxResourcesdynamic) {
		// No change
		return false;
	}

	// Reallocate and copy data
	ReD3DX9Resource* newArray = new ReD3DX9Resource[newStaticSize + newDynamicSize];
	ZeroMemory(newArray, sizeof(ReD3DX9Resource) * (newStaticSize + newDynamicSize));

	for (DWORD i = 0; i < m_NumStaticResources; i++)
		newArray[i] = m_Resource[i];

	for (DWORD i = 0; i < m_NumDynamicResources; i++) {
		newArray[newStaticSize + i] = m_Resource[m_maxResourcesstatic + i];
		*newArray[newStaticSize + i].owner = newStaticSize + i;
	}

	// Dispose old array
	delete[] m_Resource;

	// Realize
	m_Resource = newArray;
	m_maxResourcesstatic = newStaticSize;
	m_maxResourcesdynamic = newDynamicSize;
	return true;
}

BOOL ReD3DX9DeviceState::CreateStaticResource(ReD3DX9ResourceType resType, ReD3DX9VertexType vertexType, UINT size, void** out, UINT* id) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	if (!GetDevice())
		return FALSE;

	// out of resources....remove invalid entries & compact array to make room
	if (m_NumStaticResources >= m_maxResourcesstatic) {
		if (!GrowResourceArray(RED3DX9RESOURCE_STATIC))
			return FALSE;
		jsAssert(m_NumStaticResources < m_maxResourcesstatic);
	}

	if (GetDevice()->TestCooperativeLevel() != D3D_OK)
		return FALSE;

	// get next static resource pointer to create new resource
	ReD3DX9Resource* res = &m_Resource[m_NumStaticResources];
	HRESULT hr = E_FAIL;
	switch (resType) {
		case ReD3DX9ResourceType::VERTEX_BUFFER: {
			switch (vertexType) {
				case ReD3DX9VertexType::POS_NORM_UV1: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex0_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_WTXUV1: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex1_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 | D3DFVF_LASTBETA_UBYTE4 | D3DFVF_XYZRHW,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_WTX: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex2_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_LASTBETA_UBYTE4 | D3DFVF_XYZRHW | D3DFVF_TEX0,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV2: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex3_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX2,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV3: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex4_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX3,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV4: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex5_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX4,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV1_DS: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex6_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_DIFFUSE,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV2_DS: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex7_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_TEX2 | D3DFVF_DIFFUSE,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV3_DS: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex8_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_TEX3 | D3DFVF_DIFFUSE,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV4_DS: {
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex9_D3DX9),
						D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_TEX4 | D3DFVF_DIFFUSE,
						D3DPOOL_MANAGED, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				default:;
					return FALSE;
			}
		} break;

		case ReD3DX9ResourceType::INDEX_BUFFER:
		case ReD3DX9ResourceType::INDEX_BUFFER_TRIS: {
			hr = GetDevice()->CreateIndexBuffer(size * sizeof(WORD), D3DUSAGE_WRITEONLY,
				D3DFMT_INDEX16, D3DPOOL_MANAGED, (IDirect3DIndexBuffer9**)&res->resource, NULL);
		} break;

		default:
			return FALSE;
	}

	if (FAILED(hr)) {
		D3DCheck(hr);
		return FALSE;
	}

	// store the owner
	res->owner = id;
	// resource created OK.....
	*out = (void*)res->resource;
	// export the resource index and increment resource count
	*id = m_NumStaticResources++;

	return TRUE;
}

BOOL ReD3DX9DeviceState::CreateDynamicResource(ReD3DX9ResourceType resType, ReD3DX9VertexType vertexType, UINT size, void** out, UINT* id) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	if (!GetDevice())
		return FALSE;

	// out of resources....remove invalid entries & compact array to make room
	if (m_NumDynamicResources >= m_maxResourcesdynamic) {
		if (!GrowResourceArray(RED3DX9RESOURCE_DYNAMIC))
			return FALSE;
		jsAssert(m_NumDynamicResources < m_maxResourcesdynamic);
	}

	if (GetDevice()->TestCooperativeLevel() != D3D_OK)
		return FALSE;

	// get next dynamic resource pointer to create new resource
	ReD3DX9Resource* res = &m_Resource[m_maxResourcesstatic + m_NumDynamicResources];
	HRESULT hr = E_FAIL;

	switch (resType) {
		case ReD3DX9ResourceType::VERTEX_BUFFER_DISCARD: {
			switch (vertexType) {
				case ReD3DX9VertexType::POS_NORM_UV1D: {
					// create the dynamic vertex buffer
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex0_D3DX9),
						D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX0,
						D3DPOOL_DEFAULT, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV2D: {
					// create the dynamic vertex buffer
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex3_D3DX9),
						D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX2,
						D3DPOOL_DEFAULT, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV3D: {
					// create the dynamic vertex buffer
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex4_D3DX9),
						D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX3,
						D3DPOOL_DEFAULT, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				case ReD3DX9VertexType::POS_NORM_UV4D: {
					// create the dynamic vertex buffer
					hr = GetDevice()->CreateVertexBuffer(size * sizeof(ReOutputVertex5_D3DX9),
						D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX4,
						D3DPOOL_DEFAULT, (IDirect3DVertexBuffer9**)&res->resource, NULL);
				} break;

				default:
					return FALSE;
			}
		} break;

		default:
			return FALSE;
	}

	if (FAILED(hr)) {
		D3DCheck(hr);
		return FALSE;
	}

	// store the owner
	res->owner = id;
	// resource created OK.....
	assert(res->resource);
	*out = (void*)res->resource;
	// export the resource index and increase resource count
	*id = m_maxResourcesstatic + m_NumDynamicResources++;
	return TRUE;
}

BOOL ReD3DX9DeviceState::CreateSystemResource(ReD3DX9ResourceType resType, ReD3DX9VertexType vertexType, UINT64* resHash, void** out, UINT* id, const char* code) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	if (!GetDevice())
		return FALSE;

	if (GetDevice()->TestCooperativeLevel() != D3D_OK)
		return FALSE;

	// check for existing identical resource
	if (GetDuplicateSystemResource(resType, resHash, out, id))
		return TRUE;

	// out of system resources....
	if (m_NumSystemResources >= m_maxResourcessystem)
		return FALSE;

	// get next static resource pointer to create new resource
	ReD3DX9ResourceSystem* res = &m_ResourceSystem[m_NumSystemResources];

	// allocate the owner array
	if (!res->resOwners)
		res->resOwners = new UINT*[ReD3DX9DEVICEMAXRESOURCEOWNERS];

	switch (resType) {
		case ReD3DX9ResourceType::EFFECT: {
			LPD3DXBUFFER errorBuf = NULL;
			UINT flags = 0;
			if (!code || FAILED(D3DCheck(D3DXCreateEffect(GetDevice(), code, (UINT)strlen(code), NULL, NULL, flags, NULL, (LPD3DXEFFECT*)&res->resource, &errorBuf))))
				return FALSE;
		} break;

		case ReD3DX9ResourceType::VERTEX_DECLARATION: {
			switch (vertexType) {
				case ReD3DX9VertexType::POS_NORM_UV1:
				case ReD3DX9VertexType::POS_NORM_UV1D: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_WTXUV1: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						{ 0, 32, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },
						{ 0, 36, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_WTX: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },
						{ 0, 16, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (LPDIRECT3DVERTEXDECLARATION9*)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_UV2:
				case ReD3DX9VertexType::POS_NORM_UV2D: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_UV3:
				case ReD3DX9VertexType::POS_NORM_UV3D: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
						{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_UV4:
				case ReD3DX9VertexType::POS_NORM_UV4D: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
						{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
						{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_UV1_DS: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
						{ 0, 16, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_UV2_DS: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
						{ 0, 16, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_UV3_DS: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
						{ 0, 16, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
						{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				case ReD3DX9VertexType::POS_NORM_UV4_DS: {
					D3DVERTEXELEMENT9 decl[] = {
						{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
						{ 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
						{ 0, 16, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
						{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
						{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
						{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
						D3DDECL_END()
					};
					if (FAILED(D3DCheck(GetDevice()->CreateVertexDeclaration(decl, (IDirect3DVertexDeclaration9**)&res->resource))))
						return FALSE;
				} break;

				default:
					return FALSE;
			}
		} break;

		case ReD3DX9ResourceType::STATE_BLOCK: {
			D3DCheck(GetDevice()->CreateStateBlock(D3DSBT_VERTEXSTATE, (LPDIRECT3DSTATEBLOCK9*)&res->resource));
		} break;

		default:
			return FALSE;
	}

	// store the INPUT (generic) hash code
	res->resHash = *resHash;
	// export the OUTPUT (resource) hash code ....several resources possible per INPUT hashcode given the ReD3DX9DEVICEMAXRESOURCEOWNERS limit
	*resHash |= (UINT64)res->resource << 32;
	// store the resource type
	res->resType = resType;
	// add the owner
	res->resOwners[res->resNumowners] = id;
	// resource created OK.....
	*out = (void*)res->resource;
	// export the resource index and owner index
	*id = (m_NumSystemResources++ << ReD3DX9DEVICESYSTEMRESOURCESHIFT) + res->resNumowners++;

	return TRUE;
}

// checks system resource list for duplicate entries
BOOL ReD3DX9DeviceState::GetDuplicateSystemResource(ReD3DX9ResourceType resType, UINT64* resHash, void** out, UINT* id) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// check the entire list of existing resources
	for (UINT i = 0; i < m_NumSystemResources; i++) {
		ReD3DX9ResourceSystem* res = &m_ResourceSystem[i];

		// owner list is full
		if (res->resNumowners >= ReD3DX9DEVICEMAXRESOURCEOWNERS)
			continue;

		// match the type and INPUT (generic) hash code
		if (res->resType == resType && res->resHash == *resHash) {
			if (res->resOwners) {
				// export the OUTPUT (resource) hash code ....several resources possible per INPUT hashcode given the ReD3DX9DEVICEMAXRESOURCEOWNERS limit
				*resHash |= (UINT64)res->resource << 32;
				// add the owner
				res->resOwners[res->resNumowners] = id;
				// export the found resource
				*out = (void*)res->resource;
				// export the resource index and owner index
				*id = (i << ReD3DX9DEVICESYSTEMRESOURCESHIFT) + res->resNumowners++;
				// show that a dup was found
				return TRUE;
			}
		}
	}
	// no dup found...must create new resource
	return FALSE;
}

HRESULT ReD3DX9DeviceState::SetRenderState(D3DRENDERSTATETYPE eState, DWORD value) {
	// Direct call
	return GetDevice()->SetRenderState(eState, value);
}

void ReD3DX9DeviceState::SetNumTextureStages(UINT numStages) {
	// Selectively reset higher texture stages that is no longer needed for next draw call.
	if (numStages < m_NumTexStages) {
		if (GetDevice()) {
			for (DWORD i = numStages; i < m_NumTexStages; i++) {
				m_TexStateCurr[i] = ReD3DX9DEVICEINVALID;
				m_TexCurr[i] = (UINT64)0xffffffffffffffff;
				GetDevice()->SetTexture(i, NULL);
				GetDevice()->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
				GetDevice()->SetTextureStageState(i, D3DTSS_COLORARG2, D3DTA_CURRENT);
				GetDevice()->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
				GetDevice()->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
				GetDevice()->SetTextureStageState(i, D3DTSS_ALPHAARG2, D3DTA_CURRENT);
				GetDevice()->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
			}
		}
	}

	m_NumTexStages = numStages;
}

} // namespace KEP
