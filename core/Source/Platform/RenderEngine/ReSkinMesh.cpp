///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReSkinMesh.h"
#include "Utils/ReUtils.h"
#include "Utils/NvTriStrip.h"
#include "Common/include/IMemSizeGadget.h"
#include "Common/include/MemSizeSpecializations.h"
#include "SkinVertex.h"

#include <algorithm>
#include <numeric>
#include <malloc.h>

namespace KEP {

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////
ReSkinMesh::~ReSkinMesh() {
	if (IsCloned()) {
		return;
	}
	if (m_Desc.pMem) {
		_aligned_free(m_Desc.pMem);
		m_Desc.pMem = NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Return TRUE if succeeded, FALSE otherwise
///////////////////////////////////////////////////////////////////////////////
BOOL ReSkinMesh::Allocate() {
	// mesh doesn't own the data
	if (IsCloned()) {
		return TRUE;
	}

	// mesh is not yet allocated, but initialized
	if (m_Desc.Status == ReMESHSTATUS_STREAMED) {
		// delete existing data array
		if (m_Desc.pMem) {
			_aligned_free(m_Desc.pMem);
			m_Desc.pMem = NULL;
		}

		DWORD DataSize = m_Desc.NumV * (sizeof(Vector3f) * 2 + m_Desc.NumUV * sizeof(Vector2f)) +
						 m_Desc.NumI * sizeof(WORD) + m_Desc.NumV * m_SkinDesc.Wpv * (sizeof(float) + sizeof(BYTE));

		// allocate the data array and get offset pointers
		m_Desc.pMem = (BYTE*)_aligned_malloc(DataSize, 32);
		if (!m_Desc.pMem) {
#ifdef _DEBUG
			std::stringstream ss;
			ss << "ReSkinMesh::Allocate - memory allocate failed: vcnt=" << m_Desc.NumV << ", icnt=" << m_Desc.NumI << ", uvcnt=" << m_Desc.NumUV << "\n";
			OutputDebugStringA(ss.str().c_str());
#endif
			return FALSE;
		}

		m_Desc.DataSize = DataSize;
		m_Desc.pP = (Vector3f*)m_Desc.pMem;
		m_Desc.pN = (Vector3f*)((BYTE*)m_Desc.pP + m_Desc.NumV * sizeof(Vector3f));
		m_Desc.pUv = (Vector2f*)((BYTE*)m_Desc.pN + m_Desc.NumV * sizeof(Vector3f));
		m_SkinDesc.pBI = (BYTE*)((BYTE*)m_Desc.pUv + m_Desc.NumUV * m_Desc.NumV * sizeof(Vector2f));
		m_SkinDesc.pW = (FLOAT*)((BYTE*)m_SkinDesc.pBI + m_Desc.NumV * m_SkinDesc.Wpv * sizeof(BYTE));
		m_Desc.pI = (WORD*)((BYTE*)m_SkinDesc.pW + m_Desc.NumV * m_SkinDesc.Wpv * sizeof(float));
		m_Desc.Status = ReMESHSTATUS_INIT;
		return TRUE;
	}

	// mesh is not yet allocated
	if (m_Desc.Status == ReMESHSTATUS_UNALLOC) {
		// delete existing data array
		if (m_Desc.pMem) {
			_aligned_free(m_Desc.pMem);
			m_Desc.pMem = NULL;
		}

		DWORD DataSize = m_Desc.NumV * (sizeof(Vector3f) * 2 + m_Desc.NumUV * sizeof(Vector2f)) +
						 m_Desc.NumI * sizeof(WORD) + m_Desc.NumV * m_SkinDesc.Wpv * (sizeof(float) + sizeof(BYTE));

		// allocate the data array and get offset pointers
		m_Desc.pMem = (BYTE*)_aligned_malloc(DataSize, 32);
		if (!m_Desc.pMem) {
#ifdef _DEBUG
			std::stringstream ss;
			ss << "ReSkinMesh::Allocate - memory allocate failed: vcnt=" << m_Desc.NumV << ", icnt=" << m_Desc.NumI << ", uvcnt=" << m_Desc.NumUV << "\n";
			OutputDebugStringA(ss.str().c_str());
#endif
			return FALSE;
		}

		m_Desc.DataSize = DataSize;
		m_Desc.pP = (Vector3f*)m_Desc.pMem;
		m_Desc.pN = (Vector3f*)((BYTE*)m_Desc.pP + m_Desc.NumV * sizeof(Vector3f));
		m_Desc.pUv = (Vector2f*)((BYTE*)m_Desc.pN + m_Desc.NumV * sizeof(Vector3f));
		m_SkinDesc.pBI = (BYTE*)((BYTE*)m_Desc.pUv + m_Desc.NumUV * m_Desc.NumV * sizeof(Vector2f));
		m_SkinDesc.pW = (FLOAT*)((BYTE*)m_SkinDesc.pBI + m_Desc.NumV * m_SkinDesc.Wpv * sizeof(BYTE));
		m_Desc.pI = (WORD*)((BYTE*)m_SkinDesc.pW + m_Desc.NumV * m_SkinDesc.Wpv * sizeof(float));
		m_Desc.Status = ReMESHSTATUS_ALLOC;
		return TRUE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////
void ReSkinMesh::Build(bool strip) {
	// mesh is not yet allocated or already initialized, or doesn't own the data
	if (m_Desc.Status != ReMESHSTATUS_ALLOC || IsCloned()) {
		return;
	}

	// update the status to initialized
	m_Desc.Status = ReMESHSTATUS_INIT;

	// tristrip the mesh
	if (strip && m_Desc.PrimType == ReMeshPrimType::TRILIST) {
		TriStrip();
	}

	DWORD hashN = ReHash((BYTE*)m_Desc.pN, m_Desc.NumV * sizeof(Vector3f), 0);
	DWORD hashV = ReHash((BYTE*)m_Desc.pP, m_Desc.NumV * sizeof(Vector3f), hashN);
	DWORD hashUV = ReHash((BYTE*)m_Desc.pUv, m_Desc.NumV * sizeof(Vector2f) * m_Desc.NumUV, hashV);
	DWORD hashBI = ReHash((BYTE*)m_SkinDesc.pBI, m_Desc.NumV * sizeof(BYTE) * m_SkinDesc.Wpv, hashUV);
	DWORD hashW = ReHash((BYTE*)m_SkinDesc.pW, m_Desc.NumV * sizeof(float) * m_SkinDesc.Wpv, hashBI);

	m_Desc.Hash = ((unsigned __int64)hashV << 32) | ((unsigned __int64)hashW);

	DWORD compress = TRUE;

	if (compress) {
		// get the ranges for the position vectors
		FLOAT Min = FLT_MAX;
		FLOAT Max = -FLT_MAX;
		FLOAT min[3] = { Min, Min, Min };
		FLOAT max[3] = { Max, Max, Max };
		for (DWORD i = 0; i < m_Desc.NumV; i++) {
			if (m_Desc.pP[i].x < min[0]) {
				min[0] = m_Desc.pP[i].x;
				if (min[0] < Min) {
					Min = min[0];
				}
			}
			if (m_Desc.pP[i].y < min[1]) {
				min[1] = m_Desc.pP[i].y;
				if (min[1] < Min) {
					Min = min[1];
				}
			}
			if (m_Desc.pP[i].z < min[2]) {
				min[2] = m_Desc.pP[i].z;
				if (min[2] < Min) {
					Min = min[2];
				}
			}

			if (m_Desc.pP[i].x > max[0]) {
				max[0] = m_Desc.pP[i].x;
				if (max[0] > Max) {
					Max = max[0];
				}
			}
			if (m_Desc.pP[i].y > max[1]) {
				max[1] = m_Desc.pP[i].y;
				if (max[1] > Max) {
					Max = max[1];
				}
			}
			if (m_Desc.pP[i].z > max[2]) {
				max[2] = m_Desc.pP[i].z;
				if (max[2] > Max) {
					Max = max[2];
				}
			}
		}

		if (Max > Min) {
			// get the min & max of the position vectors
			m_Desc.ScaleConst[0].x = Min;

			// get the stream size & compression/decompression scales
			if (Max - Min <= .1f) {
				m_Desc.ScaleSize[0] = 1;
				m_Desc.ScaleConst[0].y = 255.f / (Max - Min);
				m_Desc.ScaleConst[0].z = (Max - Min) / 255.f;

			} else if (Max - Min <= 128.f) {
				m_Desc.ScaleSize[0] = 2;
				m_Desc.ScaleConst[0].y = 65535.f / (Max - Min);
				m_Desc.ScaleConst[0].z = (Max - Min) / 65535.f;
			} else {
				m_Desc.ScaleSize[0] = 4;
				m_Desc.ScaleConst[0].y = 1.f;
				m_Desc.ScaleConst[0].z = 1.f;
			}
		} else {
			m_Desc.ScaleSize[0] = 4;
			m_Desc.ScaleConst[0].x = 0.f;
			m_Desc.ScaleConst[0].y = 1.f;
			m_Desc.ScaleConst[0].z = 1.f;
		}
	}

	if (compress) {
		// get the ranges for the normal vectors
		FLOAT Min = FLT_MAX;
		FLOAT Max = -FLT_MAX;
		FLOAT min[3] = { Min, Min, Min };
		FLOAT max[3] = { Max, Max, Max };
		for (DWORD i = 0; i < m_Desc.NumV; i++) {
			if (m_Desc.pN[i].x < min[0]) {
				min[0] = m_Desc.pN[i].x;
				if (min[0] < Min) {
					Min = min[0];
				}
			}
			if (m_Desc.pN[i].y < min[1]) {
				min[1] = m_Desc.pN[i].y;
				if (min[1] < Min) {
					Min = min[1];
				}
			}
			if (m_Desc.pN[i].z < min[2]) {
				min[2] = m_Desc.pN[i].z;
				if (min[2] < Min) {
					Min = min[2];
				}
			}

			if (m_Desc.pN[i].x > max[0]) {
				max[0] = m_Desc.pN[i].x;
				if (max[0] > Max) {
					Max = max[0];
				}
			}
			if (m_Desc.pN[i].y > max[1]) {
				max[1] = m_Desc.pN[i].y;
				if (max[1] > Max) {
					Max = max[1];
				}
			}
			if (m_Desc.pN[i].z > max[2]) {
				max[2] = m_Desc.pN[i].z;
				if (max[2] > Max) {
					Max = max[2];
				}
			}
		}

		if (Max > Min) {
			// get the min & max of the position vectors
			m_Desc.ScaleConst[1].x = Min;

			// get the stream size & compression/decompression scales
			if (Max - Min <= 2.f) {
				m_Desc.ScaleSize[1] = 1;
				m_Desc.ScaleConst[1].y = 255.f / (Max - Min);
				m_Desc.ScaleConst[1].z = (Max - Min) / 255.f;

			} else if (Max - Min <= 128.f) {
				m_Desc.ScaleSize[1] = 2;
				m_Desc.ScaleConst[1].y = 65535.f / (Max - Min);
				m_Desc.ScaleConst[1].z = (Max - Min) / 65535.f;
			} else {
				m_Desc.ScaleSize[1] = 4;
				m_Desc.ScaleConst[1].y = 1.f;
				m_Desc.ScaleConst[1].z = 1.f;
			}
		} else {
			m_Desc.ScaleSize[1] = 4;
			m_Desc.ScaleConst[1].x = 0.f;
			m_Desc.ScaleConst[1].y = 1.f;
			m_Desc.ScaleConst[1].z = 1.f;
		}
	}

	if (compress) {
		// get the ranges for the UVs
		FLOAT Min = FLT_MAX;
		FLOAT Max = -FLT_MAX;
		FLOAT min[3] = { Min, Min, Min };
		FLOAT max[3] = { Max, Max, Max };

		switch (m_Desc.NumUV) {
			case 1: {
				for (DWORD i = 0; i < m_Desc.NumV; i++) {
					// 1st UV set
					if (m_Desc.pUv[i].x < min[0]) {
						min[0] = m_Desc.pUv[i].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[i].y < min[1]) {
						min[1] = m_Desc.pUv[i].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[i].x > max[0]) {
						max[0] = m_Desc.pUv[i].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[i].y > max[1]) {
						max[1] = m_Desc.pUv[i].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			case 2: {
				for (DWORD i = 0, j = 0; i < m_Desc.NumV; i++, j += 2) {
					// 1st UV set
					if (m_Desc.pUv[j].x < min[0]) {
						min[0] = m_Desc.pUv[j].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j].y < min[1]) {
						min[1] = m_Desc.pUv[j].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}
					if (m_Desc.pUv[j].x > max[0]) {
						max[0] = m_Desc.pUv[j].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j].y > max[1]) {
						max[1] = m_Desc.pUv[j].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 2nd UV set
					if (m_Desc.pUv[j + 1].x < min[0]) {
						min[0] = m_Desc.pUv[j + 1].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 1].y < min[1]) {
						min[1] = m_Desc.pUv[j + 1].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 1].x > max[0]) {
						max[0] = m_Desc.pUv[j + 1].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 1].y > max[1]) {
						max[1] = m_Desc.pUv[j + 1].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			case 3: {
				for (DWORD i = 0, j = 0; i < m_Desc.NumV; i++, j += 3) {
					// 1st UV set
					if (m_Desc.pUv[j].x < min[0]) {
						min[0] = m_Desc.pUv[j].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j].y < min[1]) {
						min[1] = m_Desc.pUv[j].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}
					if (m_Desc.pUv[j].x > max[0]) {
						max[0] = m_Desc.pUv[j].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j].y > max[1]) {
						max[1] = m_Desc.pUv[j].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 2nd UV set
					if (m_Desc.pUv[j + 1].x < min[0]) {
						min[0] = m_Desc.pUv[j + 1].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 1].y < min[1]) {
						min[1] = m_Desc.pUv[j + 1].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 1].x > max[0]) {
						max[0] = m_Desc.pUv[j + 1].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 1].y > max[1]) {
						max[1] = m_Desc.pUv[j + 1].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 3rd UV set
					if (m_Desc.pUv[j + 2].x < min[0]) {
						min[0] = m_Desc.pUv[j + 2].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 2].y < min[1]) {
						min[1] = m_Desc.pUv[j + 2].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 2].x > max[0]) {
						max[0] = m_Desc.pUv[j + 2].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 2].y > max[1]) {
						max[1] = m_Desc.pUv[j + 2].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			case 4: {
				for (DWORD i = 0, j = 0; i < m_Desc.NumV; i++, j += 4) {
					// 1st UV set
					if (m_Desc.pUv[j].x < min[0]) {
						min[0] = m_Desc.pUv[j].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j].y < min[1]) {
						min[1] = m_Desc.pUv[j].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}
					if (m_Desc.pUv[j].x > max[0]) {
						max[0] = m_Desc.pUv[j].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j].y > max[1]) {
						max[1] = m_Desc.pUv[j].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 2nd UV set
					if (m_Desc.pUv[j + 1].x < min[0]) {
						min[0] = m_Desc.pUv[j + 1].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 1].y < min[1]) {
						min[1] = m_Desc.pUv[j + 1].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 1].x > max[0]) {
						max[0] = m_Desc.pUv[j + 1].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 1].y > max[1]) {
						max[1] = m_Desc.pUv[j + 1].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 3rd UV set
					if (m_Desc.pUv[j + 2].x < min[0]) {
						min[0] = m_Desc.pUv[j + 2].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 2].y < min[1]) {
						min[1] = m_Desc.pUv[j + 2].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 2].x > max[0]) {
						max[0] = m_Desc.pUv[j + 2].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 2].y > max[1]) {
						max[1] = m_Desc.pUv[j + 2].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 4th UV set
					if (m_Desc.pUv[j + 3].x < min[0]) {
						min[0] = m_Desc.pUv[j + 3].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 3].y < min[1]) {
						min[1] = m_Desc.pUv[j + 3].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 3].x > max[0]) {
						max[0] = m_Desc.pUv[j + 3].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 3].y > max[1]) {
						max[1] = m_Desc.pUv[j + 3].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			default: {
				Max = 2.f;
				Min = 0.f;

				min[0] = 0.f;
				max[0] = 0.f;

				min[1] = 0.f;
				max[1] = 0.f;
			}
		}

		if (Max > Min) {
			// get the min & max of the position vectors
			m_Desc.ScaleConst[2].x = Min;

			// get the stream size & compression/decompression scales
			if (Max - Min <= 2.f) {
				m_Desc.ScaleSize[2] = 1;
				m_Desc.ScaleConst[2].y = 255.f / (Max - Min);
				m_Desc.ScaleConst[2].z = (Max - Min) / 255.f;

			} else if (Max - Min <= 128.f) {
				m_Desc.ScaleSize[2] = 2;
				m_Desc.ScaleConst[2].y = 65535.f / (Max - Min);
				m_Desc.ScaleConst[2].z = (Max - Min) / 65535.f;
			} else {
				m_Desc.ScaleSize[2] = 4;
				m_Desc.ScaleConst[2].y = 1.f;
				m_Desc.ScaleConst[2].z = 1.f;
			}
		} else {
			m_Desc.ScaleSize[2] = 4;
			m_Desc.ScaleConst[2].x = 0.f;
			m_Desc.ScaleConst[2].y = 1.f;
			m_Desc.ScaleConst[2].z = 1.f;
		}
	}

	if (compress) {
		// Weights are between 0 and 1. Compress floats into bytes (precision at 1/255 ~ 0.004)
		m_SkinDesc.ScaleConstW.x = 0.0f;
		m_SkinDesc.ScaleSizeW = 1;
		m_SkinDesc.ScaleConstW.y = 255.f;
		m_SkinDesc.ScaleConstW.z = 1.f / 255.f;
	}

	// calculate the streamed data array size
	m_Desc.StrmSize = m_Desc.NumV * (m_Desc.ScaleSize[0] + m_Desc.ScaleSize[1]) * 3;
	m_Desc.StrmSize += m_Desc.NumV * m_Desc.NumUV * m_Desc.ScaleSize[2] * 2;
	m_Desc.StrmSize += m_Desc.NumV * m_SkinDesc.Wpv * (sizeof(BYTE) + m_SkinDesc.ScaleSizeW);
	m_Desc.StrmSize += m_Desc.NumI * sizeof(WORD);
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////
void ReSkinMesh::TriStrip() {
	PrimitiveGroup* primGroups = NULL;
	unsigned short numGroups = 0;

	if (m_Desc.NumI <= 0xffff) {
		if (GenerateStrips((const unsigned short*)m_Desc.pI, (const unsigned int)m_Desc.NumI,
				(PrimitiveGroup**)&primGroups, (unsigned short*)&numGroups)) {
			if (primGroups[0].numIndices < m_Desc.NumI) {
				// get new number of indexes
				m_Desc.NumI = primGroups[0].numIndices;

				// allocate the new mem block sizes for the new index count
				m_Desc.DataSize = m_Desc.NumV * (sizeof(Vector3f) * 2 + m_Desc.NumUV * sizeof(Vector2f)) +
								  m_Desc.NumI * sizeof(WORD) + m_Desc.NumV * m_SkinDesc.Wpv * (sizeof(float) + sizeof(BYTE));

				// get temp pointer to existing data arrays for copy
				void* pMem = m_Desc.pMem;
				// allocate new mem block with data array offset pointers
				m_Desc.pMem = (BYTE*)_aligned_malloc(m_Desc.DataSize, 32);
				m_Desc.pP = (Vector3f*)m_Desc.pMem;
				m_Desc.pN = (Vector3f*)((BYTE*)m_Desc.pP + m_Desc.NumV * sizeof(Vector3f));
				m_Desc.pUv = (Vector2f*)((BYTE*)m_Desc.pN + m_Desc.NumV * sizeof(Vector3f));
				m_SkinDesc.pBI = (BYTE*)((BYTE*)m_Desc.pUv + m_Desc.NumUV * m_Desc.NumV * sizeof(Vector2f));
				m_SkinDesc.pW = (FLOAT*)((BYTE*)m_SkinDesc.pBI + m_Desc.NumV * m_SkinDesc.Wpv * sizeof(BYTE));
				m_Desc.pI = (WORD*)((BYTE*)m_SkinDesc.pW + m_Desc.NumV * m_SkinDesc.Wpv * sizeof(float));

				// copy over existing data arrays
				memcpy((void*)m_Desc.pMem, (void*)pMem, m_Desc.DataSize);
				// copy over new indexes
				memcpy((void*)m_Desc.pI, (void*)primGroups[0].indices, m_Desc.NumI * sizeof(WORD));

				// delete the old mem block
				_aligned_free(pMem);
				m_Desc.PrimType = ReMeshPrimType::TRISTRIP;
			}
			// delete internal tristrip data
			if (primGroups) {
				delete[] primGroups;
			}
		}
	}
}

void ReSkinMeshState::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(pB);
	}
}

void ReSkinMesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		GetMemSizeofInternals(pMemSizeGadget);
	}
}

void ReSkinMesh::GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const {
	ReMesh::GetMemSizeofInternals(pMemSizeGadget);
	pMemSizeGadget->AddObject(m_SkinState);
}

// Enable precise FP operation to guarantee deterministic output for serialization
#pragma float_control(precise, on, push)

void ReSkinMesh::BuildBoneWeights(const std::vector<VertexAssignmentBlended>& assignments) // Extracted from CreateReSkinMeshPersistent()
{
	assert(m_Desc.NumV == assignments.size());

	for (DWORD i = 0; i < m_Desc.NumV; i++) {
		std::vector<BoneWeightForSorting> validBoneWeights;
		validBoneWeights.reserve(assignments[i].vertexBoneWeights.size());

		// Get the non-zero weights
		for (const VertexBoneWeight& boneWeight : assignments[i].vertexBoneWeights) {
			if (boneWeight.weight > 0.f) {
				assert(boneWeight.weight <= 1.f);
				byte iWeight = boneWeight.weight * 255; // Capped at 255
				validBoneWeights.push_back(BoneWeightForSorting{ boneWeight.boneIndex, boneWeight.weight, iWeight });
			}
		}

		BuildBoneWeightsForVertex(i, validBoneWeights);
	}
}

void ReSkinMesh::BuildBoneWeightsForVertex(size_t vertexIndex, std::vector<BoneWeightForSorting>& boneWeights) {
	BYTE* bixPtr = &m_SkinDesc.pBI[vertexIndex * m_SkinDesc.Wpv];
	float* wgtPtr = &m_SkinDesc.pW[vertexIndex * m_SkinDesc.Wpv];

	// Sort bone weights by weight (descending order), then by bone index (ascending order)
	std::sort(boneWeights.begin(), boneWeights.end(), [](const BoneWeightForSorting& lhs, const BoneWeightForSorting& rhs) {
		return lhs.iWeight > rhs.iWeight || lhs.iWeight == rhs.iWeight && lhs.boneIndex < rhs.boneIndex;
	});

	// Truncate weights by Wpv
	size_t validWeightCount = (std::min)((size_t)m_SkinDesc.Wpv, boneWeights.size());

	// Sum selected weights
	float sum = std::accumulate(boneWeights.begin(), boneWeights.begin() + validWeightCount, 0.f, [](float sum, const BoneWeightForSorting& bw) {
		return sum + bw.fWeight;
	});

	assert(sum >= 0.f); // Assuming total weight is non-negative - consistent with the old logic
	float weightFactor = sum > 0.f ? (1.f / sum) : 0.0f;

	// Assign the new weights & re-weight them to sum to 1
	for (DWORD j = 0; j < m_SkinDesc.Wpv; j++) {
		if (j < validWeightCount) {
			// NOTE: this is a port from the old logic in CreateReSkinMeshPersistent. It forces total weight to 1.
			// It's useful if the input has more weights per vertex than supported and we have to drop some weights
			// and normalize the rest. However, sometimes non 1 total-weight can be legitimate.
			wgtPtr[j] = boneWeights[j].fWeight * weightFactor;
			bixPtr[j] = boneWeights[j].boneIndex;
		} else {
			wgtPtr[j] = 0.f;
			bixPtr[j] = 0;
		}
	}
}

#ifdef ED7119_DYNOBJ_ASSET_FIX
ReSkinMesh* ReSkinMesh::CloneMeshWeightsSorted() const {
	ReSkinMesh* pNewMesh = New();
	pNewMesh->m_Desc.NumV = m_Desc.NumV;
	pNewMesh->m_Desc.NumI = m_Desc.NumI;
	pNewMesh->m_Desc.NumUV = m_Desc.NumUV;
	pNewMesh->m_Desc.PrimType = m_Desc.PrimType;
	pNewMesh->m_SkinDesc.NumB = m_SkinDesc.NumB;
	pNewMesh->m_SkinDesc.Wpv = m_SkinDesc.Wpv;

	if (!pNewMesh->Allocate()) {
		assert(false);
		delete pNewMesh;
		return nullptr;
	}

	// Make a full copy of the data
	assert(pNewMesh->m_Desc.DataSize == m_Desc.DataSize);
	memcpy(pNewMesh->m_Desc.pMem, m_Desc.pMem, m_Desc.DataSize);

	if (ReMeshConvertOptions::getFixBoneWeightSortOrder()) {
		// Sort weights
		for (unsigned vi = 0; vi < m_Desc.NumV; vi++) {
			std::vector<BoneWeightForSorting> validBoneWeights;
			validBoneWeights.reserve(m_SkinDesc.Wpv);

			size_t ofs = vi * m_SkinDesc.Wpv;
			auto pB = m_SkinDesc.pBI + ofs;
			auto pW = m_SkinDesc.pW + ofs;

			// Get the non-zero weights
			for (size_t wi = 0; wi < m_SkinDesc.Wpv; wi++) {
				assert(pW[wi] <= 1.f);
				if (pW[wi] > 0.f) {
					byte iWeight = pW[wi] * 255; // Capped at 255 - weights are alway compressed in bytes
					validBoneWeights.push_back(BoneWeightForSorting{ pB[wi], pW[wi], iWeight });
				}
			}

			pNewMesh->BuildBoneWeightsForVertex(vi, validBoneWeights);
		}
	}

	// Rebuild internal data (hashes, stream size, etc)
	pNewMesh->Build(false);
	assert(pNewMesh->m_Desc.StrmSize == m_Desc.StrmSize);
	return pNewMesh;
}
#endif

#pragma float_control(pop)

} // namespace KEP
