///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ReMesh.h"
#include "ReMeshConvertOptions.h"

namespace KEP {

class IMemSizeGadget;
struct VertexAssignmentBlended;

struct ReSkinMeshData {
	// version 0 streaming header
	DWORD Version;
	DWORD NumB;
	DWORD Wpv;
	DWORD Reserved0;
	// version 1 streaming header extension
	Vector3f ScaleConstW;
	DWORD ScaleSizeW;
	DWORD Reserved1;
	// mesh creation data
	FLOAT* pW;
	BYTE* pBI;

	ReSkinMeshData() :
			Version(1), Reserved0(0), Reserved1(0) {
	}

	static const UINT HeaderSize0 = sizeof(DWORD) * 4;
	static const UINT HeaderSize1 = sizeof(DWORD) * 2 + sizeof(Vector3f);
};

struct ReSkinMeshState {
	const Matrix44f* pB;

	ReSkinMeshState() {
		pB = NULL;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class ReSkinMesh : public ReMesh {
public:
	ReSkinMesh() { ; }
	ReSkinMesh(void* /*dev*/) { ; }
	virtual ~ReSkinMesh();
	virtual ReSkinMesh* New() const = 0;

	virtual void Access(ReMeshData** pData, ReSkinMeshData** pSkinData) {
		*pData = &m_Desc;
		*pSkinData = &m_SkinDesc;
	}
	virtual BOOL Allocate() override;
	virtual void Build(bool strip = true) override;
	virtual void Render() override { ; }
	virtual void SetBones(const Matrix44f* bones) override { m_SkinState.pB = bones; }
	virtual const Matrix44f* GetBones() const override { return m_SkinState.pB; }
	virtual const DWORD GetNumBones() const override { return m_SkinDesc.NumB; }

#ifdef ED7119_DYNOBJ_ASSET_FIX
	virtual ReSkinMesh* CloneMeshWeightsSorted() const;
#endif

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	void BuildBoneWeights(const std::vector<VertexAssignmentBlended>& assignments);

protected:
	void GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const;

	struct BoneWeightForSorting {
		byte boneIndex; // Bone index
		float fWeight; // fWeight
		byte iWeight; // fWeight * 255
	};

	void BuildBoneWeightsForVertex(size_t vertexIndex, std::vector<BoneWeightForSorting>& boneWeights);

private:
	virtual void TriStrip() override;

protected:
	ReSkinMeshData m_SkinDesc;
	ReSkinMeshState m_SkinState;
};

} // namespace KEP