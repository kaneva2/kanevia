///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

enum class ReMeshPrimType : DWORD {
	TRILIST, // 0 - previously FALSE
	TRISTRIP, // 1 - previously TRUE
	LINELIST, // 2 - added
	LINESTRIP, // 3 - added
};

}
