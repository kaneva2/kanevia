///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

#include "d3dx9math.h"
#include "d3d9types.h"
#include "Material.h"

namespace KEP {
class ITextureProxy;
class IMemSizeGadget;

#define ReMATERIALINVALID 0xffffffff
#define ReDEVICEMAXTEXTURESTAGES 4 // max multitexturing texture stage

enum class ReRenderMode {
	WIREFRAME,
	SOLID,
};

enum class ReColorSource {
	MATERIAL,
	VERTEX,
};

enum class ReShaderType {
	CPU_GENERIC,
	CPU_SSE,
	FIXEDFUNC,
	PRELIGHTING,
	GPU,
};

typedef TMaterial<float> ReMatProps;

struct ReBlendProps {
	DWORD Srcblend;
	DWORD Dstblend;
};

struct ReModeProps {
	DWORD Alphaenable;
	DWORD Zenable;
	DWORD Shademode;
	DWORD Zwriteenable;
	DWORD Alphatestenable;
	DWORD CullMode;
	DWORD FogFilter;
	LONG ZBias;
	BOOL DrawWireFrame;
	BOOL NormalizeNormals;
};

struct ReTexProps {
	CHAR ColorOp;
	CHAR AlphaOp;
	CHAR ColorArg1;
	CHAR ColorArg2;

	void clear() {
		ColorOp = 0;
		AlphaOp = 0;
		ColorArg1 = 0;
		ColorArg2 = 0;
	}
};

struct ReRenderStateData {
	ReMatProps Mat;
	ReBlendProps Blend;
	ReModeProps Mode;
	ReColorSource ColorSrc;
	std::shared_ptr<void> TexPtr[ReDEVICEMAXTEXTURESTAGES];
	ITextureProxy* ResPtr[ReDEVICEMAXTEXTURESTAGES];
	ReTexProps Tex[ReDEVICEMAXTEXTURESTAGES];
	DWORD TexHash[ReDEVICEMAXTEXTURESTAGES];
	D3DXMATRIX* TexMat[ReDEVICEMAXTEXTURESTAGES];
	const char* ShaderCode;
	DWORD MatHash;
	DWORD BlendHash;
	DWORD ModeHash;
	DWORD NumTexStages;
	DWORD SortOrder;
	BOOL Highlight; // Highlight ON/OFF
	DWORD Constant; // D3DTSS_CONSTANT
	DWORD TextureFactor; // D3DRS_TEXTUREFACTOR

	ReRenderStateData() {
		clear();
		Mode.Zenable = 1; // This flag is now activated. As most callers didn't set it previously, default to 1 for backward compatibility.
	}

	void clear() {
		memset(&Mat, 0, sizeof(Mat));
		memset(&Blend, 0, sizeof(Blend));
		memset(&Mode, 0, sizeof(Mode));
		memset(&ColorSrc, 0, sizeof(ColorSrc));
		for (size_t i = 0; i < ReDEVICEMAXTEXTURESTAGES; ++i) {
			TexPtr[i].reset();
			ResPtr[i] = 0;
			Tex[i].clear();
			TexHash[i] = 0;
			TexMat[i] = nullptr;
		}
		ShaderCode = nullptr;
		MatHash = 0;
		BlendHash = 0;
		ModeHash = 0;
		NumTexStages = 0;
		SortOrder = 0;
		Highlight = FALSE;
		Constant = 0;
		TextureFactor = 0;
	}

	~ReRenderStateData() {
		;
	}
};

// Only some of these attributes are used.
// They are all duplicated in the D3DLIGHT9 type, but
// not all of those are used either.
// And just incase you didn't think that was bad enough,
// this structure is initialized in ReLightCache::LoadLights()
// by memcpy of an entry in the ReD3DX9DeviceRenderState::m_LightSort
// array, which is of type ReD3DX9Light and has the same layout as a
// ReLight.  It's the m_LightSort version that's actually updated
// when a light is moved, but the memcpy is required to copy the
// data into an object-specific (DO or World) m_lightCache that's
// the data used by ReShader_Prelighting::LightVertex().
struct ReLight {
	D3DXVECTOR4 Position;
	D3DXVECTOR4 Diffuse;
	D3DXVECTOR4 Specular;
	D3DXVECTOR4 Attenuation;
	D3DXVECTOR4 Ambient;
	D3DLIGHT9 Light;
};

class ReMaterial;
typedef std::shared_ptr<ReMaterial> ReMaterialPtr; // Declairs a typedef for "ReMaterialPtr".
class ReMesh;

struct ReLightCache {
	static const UINT MAXRESHADERLIGHTS = 8;

	ReLight* Lights[MAXRESHADERLIGHTS];
	UINT NumLights;

	ReLightCache();
	~ReLightCache();
	void Flush() { NumLights = 0; }
	const BOOL Empty() const { return NumLights == 0; }
	const ReLight* Light(const UINT i) const {
		if (i < NumLights)
			return Lights[i];
		return NULL;
	}
	const UINT NumLight() const { return NumLights; }
	void LoadLights(const ReLight** lights, const UINT numLights);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

struct ReShaderData {
	D3DXMATRIX* pBones;
	D3DXMATRIX* pWorld;
	UINT NumBones;
	FLOAT Radius;
	D3DXMATRIX* pWorldCam;
	D3DXMATRIX* pWorldViewProj;
	D3DXMATRIX* pWorldInvTranspose;
	ReLightCache* LightCache;
	ReRenderStateData* State;
	FLOAT CamDist;
	UINT NumWeightsPerVertex;
	BOOL PreLit;

	ReShaderData() {
		memset(&pBones, 0, sizeof(ReShaderData));
	}
};

struct ReMaterialConstBlock {
	D3DXVECTOR4 MatDiffuse;
	D3DXVECTOR4 MatSpecular;
	D3DXVECTOR4 MatAmbient;
	D3DXVECTOR4 MiscConstants;

	ReMaterialConstBlock() { ; }
	~ReMaterialConstBlock() { ; }
};

class ReRenderState {
protected:
	ReRenderState(ReRenderStateData* pRenderStateData) :
			m_pRenderStateData(pRenderStateData) {}

public:
	virtual ~ReRenderState() { ; }

	virtual void PreRender() = 0;
	virtual void Render() = 0;
	virtual void PostRender() = 0;
	virtual void Update(ReRenderStateData* data) = 0;
	virtual void Update(std::shared_ptr<void> tex, UINT stage) = 0;
	virtual BOOL EqualTo(const ReRenderState* s) const = 0;
	virtual INT Compare(const ReRenderState* s) const = 0;
	virtual BOOL Invalid() const = 0;
	//virtual	const ReRenderStateData*	GetData() const							=	0;
	//virtual	ReRenderStateData*			GetDataMutable()						=	0;
	const ReRenderStateData* GetData() const { return m_pRenderStateData; }
	ReRenderStateData* GetDataMutable() { return m_pRenderStateData; }
	virtual ReRenderState* Clone() const = 0;

private:
	ReRenderStateData* m_pRenderStateData;
};

class ReRenderState_VS : public ReRenderState {
protected:
	ReRenderState_VS(ReRenderStateData* pRenderStateData) :
			ReRenderState(pRenderStateData) {}

public:
	virtual ~ReRenderState_VS() { ; }

	virtual void PreRender() = 0;
	virtual void Render() = 0;
	virtual void PostRender() = 0;
	virtual void Update(ReRenderStateData* data) = 0;
	virtual ReRenderState_VS* Clone() const = 0;
};

class ReShader {
public:
	ReShader() { ; }
	ReShader(const ReShader& /*shader*/) { ; }
	virtual ~ReShader() { ; }

public:
	virtual void PreRender() = 0;
	virtual void Render() = 0;
	virtual void PostRender() = 0;
	virtual void SetTechnique() = 0;
	virtual BOOL Update(const ReShaderData* data) = 0;
	virtual void Invalidate() = 0;
	virtual BOOL EqualTo(const ReShader* shader) const = 0;
	virtual INT Compare(const ReShader* shader) const = 0;
	//virtual	UINT64			Hash() const										=	0;
	virtual UINT64 Hash() const final { return m_Hash; } // final for performance
	virtual BOOL Invalid() const = 0;
	virtual ReShader* Clone(ReMesh* mesh = NULL) = 0;
	virtual ReShaderType ShaderType() const = 0;
	virtual void Begin(UINT& passes) = 0;
	virtual void BeginPass(UINT pass) const = 0;
	virtual void EndPass() const = 0;
	virtual void End() const = 0;

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;

private:
	virtual BOOL Restore() = 0;

protected:
	void SetHash(uint64_t hash) { m_Hash = hash; }

private:
	uint64_t m_Hash = 0xffffffff; //ReD3DX9DEVICEINVALID
};

class ReMaterial {
	ReMaterial(const ReMaterial&) = delete;
	void operator=(const ReMaterial&) = delete;

protected:
	ReMaterial(ReRenderStateData* pRenderStateData) :
			m_pRenderStateData(pRenderStateData) {}

public:
	virtual ~ReMaterial() { ; }

public:
	virtual void PreRender() = 0;
	virtual void Render() = 0;
	virtual void PostRender() = 0;
	virtual void Update(ReRenderStateData* data) = 0;
	virtual void Update() = 0;
	virtual void Update(std::shared_ptr<void> texture, DWORD stage) = 0;
	virtual void Update(ReShaderData* data) = 0;
	virtual void PreLight(BOOL val) = 0;
	virtual const BOOL PreLit() const = 0;
	virtual const ReRenderState* GetRenderState() const = 0;
	const ReRenderStateData* GetRenderStateData() const { return m_pRenderStateData; }
	ReRenderStateData* GetRenderStateDataMutable() { return m_pRenderStateData; }
	virtual BOOL EqualTo(ReMaterialPtr mat) const = 0;
	virtual BOOL Invalid() const = 0;
	virtual ReMaterialPtr Clone() const = 0;

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;

private:
	ReRenderStateData* m_pRenderStateData; // Cached for efficient access
};

class ReMaterial_TexMat : public ReMaterial {
protected:
	ReMaterial_TexMat(ReRenderStateData* pRenderStateData) :
			ReMaterial(pRenderStateData) {}

public:
	virtual ~ReMaterial_TexMat() { ; }

public:
	virtual void Update() = 0;
	virtual ReMaterialPtr Clone() const = 0;
};

} // namespace KEP