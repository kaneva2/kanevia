///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReD3DX9StaticMesh.h"
#include "ReShader_Prelighting.h"
#include "ReMaterial.h"
#include "ReD3DX9.h"
#include "..\common\include\IMemSizeGadget.h"

namespace KEP {

void ReShader_Prelighting::LightVertex(const ReMatProps* mat, const Vector3f* ambient, UINT iVtx, DWORD* diff, DWORD* spec) const {
	Vector3f ambientSum(0.f, 0.f, 0.f);
	Vector3f diffuseSum(0.f, 0.f, 0.f);
	Vector3f speculrSum(0.f, 0.f, 0.f);

	// transform vertex to world space
	Vector3f worldPos = TransformPoint_NonPerspective(*m_Mesh->GetWorldMatrix(), m_Mesh->m_Desc.pP[iVtx]);
	Vector3f worldNorm = TransformVector(*m_Mesh->GetWorldMatrix(), m_Mesh->m_Desc.pN[iVtx]);

	// directional lights
	for (DWORD i = 0; i < m_Mesh->GetLightCache()->NumLight(); i++) {
		const ReLight* light = m_Mesh->GetLightCache()->Light(i);

		if (light->Light.Type == D3DLIGHT_DIRECTIONAL) {
			float dotProd = worldNorm.x * -light->Position.x + worldNorm.y * -light->Position.y + worldNorm.z * -light->Position.z;
			dotProd = dotProd < 0.f ? 0.f : dotProd;

			Vector3f amb(light->Ambient.x, light->Ambient.y, light->Ambient.z);
			ambientSum += amb;

			Vector3f diffuse(dotProd * light->Diffuse.x, dotProd * light->Diffuse.y, dotProd * light->Diffuse.z);
			diffuseSum += diffuse;
		}
	}

	// point lights
	for (DWORD i = 0; i < m_Mesh->GetLightCache()->NumLight(); i++) {
		const ReLight* light = m_Mesh->GetLightCache()->Light(i);

		if (light->Light.Type == D3DLIGHT_POINT) {
			Vector3f vecToLight = *((Vector3f*)&light->Position) - *((Vector3f*)&worldPos);
			float dist = vecToLight.Length();

			if (light->Light.Range > dist) {
				if (dist > 0.f) {
					vecToLight /= dist;
				}

				float lightVecdot = worldNorm.Dot(vecToLight);

				float atten = (light->Attenuation.x + dist * light->Attenuation.y + dist * dist * light->Attenuation.z);

				if (atten != 0.f) {
					atten = 1.f / atten;
				} else {
					atten = 1.f;
				}

				ambientSum += Vector3f(light->Ambient.x * atten, light->Ambient.y * atten, light->Ambient.z * atten);

				float diffFac = lightVecdot < 0.f ? 0.f : lightVecdot;
				diffFac *= atten;

				diffuseSum += Vector3f(light->Diffuse.x * diffFac, light->Diffuse.y * diffFac, light->Diffuse.z * diffFac);

				if (spec) {
					const Vector3f* camPos = (const Vector3f*)m_Mesh->m_pDev->GetRenderState()->GetCamPosition();
					Vector3f eyeVec = *camPos - *((Vector3f*)&worldPos);
					Vector3f halfVec = vecToLight + eyeVec;
					halfVec.Normalize();

					float halfVecdot = worldNorm.Dot(halfVec);
					float specFac = (lightVecdot < 0.f || halfVecdot < 0.f) ? 0.f : halfVecdot;
					specFac = specFac * specFac * specFac * specFac * specFac * specFac;
					specFac *= atten;

					Vector3f specular(light->Specular.x * mat->Specular.x * specFac, light->Specular.y * mat->Specular.y * specFac, light->Specular.z * mat->Specular.z * specFac);
					speculrSum += specular;
				}
			}
		}
	}

	Vector3f ambientOut((ambient->x + ambientSum.x) * mat->Ambient.x, (ambient->y + ambientSum.y) * mat->Ambient.y, (ambient->z + ambientSum.z) * mat->Ambient.z);
	Vector3f diffuseOut(diffuseSum.x * mat->Diffuse.x, diffuseSum.y * mat->Diffuse.y, diffuseSum.z * mat->Diffuse.z);

	Vector3f output = ambientOut + diffuseOut + speculrSum;

	if (output.x < 0.f) {
		output.x = 0.f;
	}
	if (output.y < 0.f) {
		output.y = 0.f;
	}
	if (output.z < 0.f) {
		output.z = 0.f;
	}

	output += *((Vector3f*)&mat->Emissive);

	output.x = output.x > 1.f ? 1.f : output.x;
	output.y = output.y > 1.f ? 1.f : output.y;
	output.z = output.z > 1.f ? 1.f : output.z;

	*diff = (DWORD)D3DCOLOR_RGBA((UINT)(output.x * 255.f), (UINT)(output.y * 255.f), (UINT)(output.z * 255.f), 0);
}

BOOL ReShader_Prelighting::Restore() {
	if (m_PreLit) {
		return TRUE;
	}

	if (!m_Mesh || !m_Mesh->GetLightCache() || !m_Mesh->GetWorldMatrix() || !(m_Mesh->GetMaterial())) {
		return FALSE;
	}

	const Vector3f* ambient = (Vector3f*)m_Mesh->m_pDev->GetRenderState()->GetGlobalAmbient();
	const ReMatProps* mat = &m_Mesh->GetMaterial()->GetRenderState()->GetData()->Mat;

	switch (m_Mesh->m_VertexType) {
		case ReD3DX9VertexType::POS_NORM_UV1_DS: {
			m_Mesh->m_VSize = sizeof(ReOutputVertex6_D3DX9);
			// restore the vertex buffer
			ReOutputVertex6_D3DX9* pVB = 0;
			if (m_Mesh->m_pVB && SUCCEEDED(((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Lock(0, m_Mesh->m_Desc.NumV * m_Mesh->m_VSize, (void**)&pVB, 0))) {
				for (DWORD i = 0; i < m_Mesh->m_Desc.NumV; i++) {
					LightVertex(mat, ambient, i, &pVB->Diff, NULL);
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Unlock();
			} else {
				return FALSE;
			}
		} break;

		case ReD3DX9VertexType::POS_NORM_UV2_DS: {
			m_Mesh->m_VSize = sizeof(ReOutputVertex7_D3DX9);
			// restore the vertex buffer
			ReOutputVertex7_D3DX9* pVB = 0;
			if (m_Mesh->m_pVB && SUCCEEDED(((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Lock(0, m_Mesh->m_Desc.NumV * m_Mesh->m_VSize, (void**)&pVB, 0))) {
				for (DWORD i = 0; i < m_Mesh->m_Desc.NumV; i++) {
					LightVertex(mat, ambient, i, &pVB->Diff, NULL);
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Unlock();
			} else {
				return FALSE;
			}
		} break;

		case ReD3DX9VertexType::POS_NORM_UV3_DS: {
			m_Mesh->m_VSize = sizeof(ReOutputVertex8_D3DX9);
			// restore the vertex buffer
			ReOutputVertex8_D3DX9* pVB = 0;
			if (m_Mesh->m_pVB && SUCCEEDED(((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Lock(0, m_Mesh->m_Desc.NumV * m_Mesh->m_VSize, (void**)&pVB, 0))) {
				for (DWORD i = 0; i < m_Mesh->m_Desc.NumV; i++) {
					LightVertex(mat, ambient, i, &pVB->Diff, NULL);
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Unlock();
			} else {
				return FALSE;
			}
		} break;

		case ReD3DX9VertexType::POS_NORM_UV4_DS: {
			m_Mesh->m_VSize = sizeof(ReOutputVertex9_D3DX9);
			// restore the vertex buffer
			ReOutputVertex9_D3DX9* pVB = 0;
			if (m_Mesh->m_pVB && SUCCEEDED(((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Lock(0, m_Mesh->m_Desc.NumV * m_Mesh->m_VSize, (void**)&pVB, 0))) {
				for (DWORD i = 0; i < m_Mesh->m_Desc.NumV; i++) {
					LightVertex(mat, ambient, i, &pVB->Diff, NULL);
					pVB++;
				}
				((LPDIRECT3DVERTEXBUFFER9)m_Mesh->m_pVB)->Unlock();
			} else {
				return FALSE;
			}
		} break;

		default:;
	}

	m_PreLit = TRUE;

	return TRUE;
}

void ReShader_Prelighting::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_Mesh);
	}
}

} // namespace KEP
