///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReMesh.h"

#include "Utils/ReUtils.h"
#include "Utils/NvTriStrip.h"
#include "Core/Math/Distance.h"
#include "Core/Math/Intersect.h"
#include "KEPPhysics/RigidBodyStatic.h"
#include "KEPPhysics/CollisionShape.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "common/include/IMemSizeGadget.h"
#include "common/include/MemSizeSpecializations.h"

#include <malloc.h>
#include <limits.h>
#include <crtdbg.h>
#include <cmath>

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

fast_mutex ReMesh::m_mutex;

ReMesh::~ReMesh() {
	// DRF - Added
	std::lock_guard<fast_mutex> lock(m_mutex);

	if (m_Shader) {
		delete m_Shader;
		m_Shader = NULL;
	}

	if (IsCloned())
		return;

	if (m_Desc.pMem) {
		_aligned_free(m_Desc.pMem);
		m_Desc.pMem = NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Return TRUE if succeeded, FALSE otherwise
///////////////////////////////////////////////////////////////////////////////
BOOL ReMesh::Allocate() {
	// mesh doesn't own the data
	if (IsCloned()) {
		return TRUE;
	}

	// filter the data
	if (m_Desc.NumV == 0 || m_Desc.NumV > MAX_VERTICES_PER_MESH || m_Desc.NumI > MAX_INDICIES_PER_MESH || m_Desc.NumUV > 4) {
		LogError("ReMesh Invalid - numV=" << m_Desc.NumV << " numI=" << m_Desc.NumI << " numUV=" << m_Desc.NumUV);
		return FALSE;
	}

	// mesh is not yet allocated, but initialized
	if (m_Desc.Status == ReMESHSTATUS_STREAMED) {
		// delete existing data array
		if (m_Desc.pMem) {
			_aligned_free(m_Desc.pMem);
			m_Desc.pMem = NULL;
		}

		// allocate the (uncompressed) data array and get offset pointers
		DWORD DataSize = m_Desc.getTotalDataSize();

		m_Desc.pMem = (BYTE*)_aligned_malloc(DataSize, 32);
		if (!m_Desc.pMem) {
			LogError("Memory Allocation FAILURE (2)");
			return FALSE;
		}

		m_Desc.DataSize = DataSize;
		m_Desc.pP = (Vector3f*)m_Desc.pMem;
		m_Desc.pN = (Vector3f*)((BYTE*)m_Desc.pP + m_Desc.NumV * sizeof(Vector3f));
		m_Desc.pUv = (Vector2f*)((BYTE*)m_Desc.pN + m_Desc.NumV * sizeof(Vector3f));
		m_Desc.pI = (WORD*)((BYTE*)m_Desc.pUv + m_Desc.NumUV * m_Desc.NumV * sizeof(Vector2f));
		m_Desc.Status = ReMESHSTATUS_INIT;
		return TRUE;
	}

	// mesh is not yet allocated
	if (m_Desc.Status == ReMESHSTATUS_UNALLOC) {
		// delete existing data array
		if (m_Desc.pMem) {
			_aligned_free(m_Desc.pMem);
			m_Desc.pMem = NULL;
		}

		// allocate the (uncompressed) data array and get offset pointers
		DWORD DataSize = m_Desc.NumV * (sizeof(Vector3f) * 2 +
										   m_Desc.NumUV * sizeof(Vector2f)) +
						 m_Desc.NumI * sizeof(WORD);

		m_Desc.pMem = (BYTE*)_aligned_malloc(DataSize, 32);
		if (!m_Desc.pMem) {
			LogError("Memory Allocation FAILURE (3)");
			return FALSE;
		}

		m_Desc.DataSize = DataSize;
		m_Desc.pP = (Vector3f*)m_Desc.pMem;
		m_Desc.pN = (Vector3f*)((BYTE*)m_Desc.pP + m_Desc.NumV * sizeof(Vector3f));
		m_Desc.pUv = (Vector2f*)((BYTE*)m_Desc.pN + m_Desc.NumV * sizeof(Vector3f));

		m_Desc.pI = (m_Desc.NumI == 0) ? NULL : ((WORD*)((BYTE*)m_Desc.pUv + m_Desc.NumUV * m_Desc.NumV * sizeof(Vector2f)));

		m_Desc.Status = ReMESHSTATUS_ALLOC;
		return TRUE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////
void ReMesh::Build(bool strip) {
	// mesh is not yet allocated or already initialized, or doesn't own the data
	if (m_Desc.Status != ReMESHSTATUS_ALLOC || IsCloned()) {
		return;
	}

	// update the status to initialized
	m_Desc.Status = ReMESHSTATUS_INIT;

	// tristrip the mesh
	if (strip && m_Desc.PrimType == ReMeshPrimType::TRILIST) {
		TriStrip();
	}

	// hash the normals to prime the following hashes
	DWORD hashN = ReHash((BYTE*)m_Desc.pN, m_Desc.NumV * sizeof(Vector3f), 0);
	// hash the vertex & uv data for fast duplicate checking
	DWORD hashV = ReHash((BYTE*)m_Desc.pP, m_Desc.NumV * sizeof(Vector3f), hashN);
	DWORD hashUV = ReHash((BYTE*)m_Desc.pUv, m_Desc.NumV * m_Desc.NumUV * sizeof(Vector2f), hashV);
	m_Desc.Hash = ((unsigned __int64)hashV << 32) | ((unsigned __int64)hashUV);

	BOOL compress = TRUE;

	if (compress) {
		// get the ranges for the position vectors
		FLOAT Min = FLT_MAX;
		FLOAT Max = -FLT_MAX;
		FLOAT min[3] = { Min, Min, Min };
		FLOAT max[3] = { Max, Max, Max };
		for (DWORD i = 0; i < m_Desc.NumV; i++) {
			if (m_Desc.pP[i].x < min[0]) {
				min[0] = m_Desc.pP[i].x;
				if (min[0] < Min) {
					Min = min[0];
				}
			}
			if (m_Desc.pP[i].y < min[1]) {
				min[1] = m_Desc.pP[i].y;
				if (min[1] < Min) {
					Min = min[1];
				}
			}
			if (m_Desc.pP[i].z < min[2]) {
				min[2] = m_Desc.pP[i].z;
				if (min[2] < Min) {
					Min = min[2];
				}
			}

			if (m_Desc.pP[i].x > max[0]) {
				max[0] = m_Desc.pP[i].x;
				if (max[0] > Max) {
					Max = max[0];
				}
			}
			if (m_Desc.pP[i].y > max[1]) {
				max[1] = m_Desc.pP[i].y;
				if (max[1] > Max) {
					Max = max[1];
				}
			}
			if (m_Desc.pP[i].z > max[2]) {
				max[2] = m_Desc.pP[i].z;
				if (max[2] > Max) {
					Max = max[2];
				}
			}
		}

		if (Max > Min) {
			// get the min & max of the position vectors
			m_Desc.ScaleConst[0].x = Min;

			// get the stream size & compression/decompression scales
			if (Max - Min <= 2.f) {
				m_Desc.ScaleSize[0] = 1;
				m_Desc.ScaleConst[0].y = 255.f / (Max - Min);
				m_Desc.ScaleConst[0].z = (Max - Min) / 255.f;

			} else if (Max - Min <= 128.f) {
				m_Desc.ScaleSize[0] = 2;
				m_Desc.ScaleConst[0].y = 65535.f / (Max - Min);
				m_Desc.ScaleConst[0].z = (Max - Min) / 65535.f;
			} else {
				m_Desc.ScaleSize[0] = 4;
				m_Desc.ScaleConst[0].y = 1.f;
				m_Desc.ScaleConst[0].z = 1.f;
			}
		} else {
			m_Desc.ScaleSize[0] = 4;
			m_Desc.ScaleConst[0].x = 0.f;
			m_Desc.ScaleConst[0].y = 1.f;
			m_Desc.ScaleConst[0].z = 1.f;
		}
	}

	if (compress) {
		// get the ranges for the normal vectors
		FLOAT Min = FLT_MAX;
		FLOAT Max = -FLT_MAX;
		FLOAT min[3] = { Min, Min, Min };
		FLOAT max[3] = { Max, Max, Max };
		for (DWORD i = 0; i < m_Desc.NumV; i++) {
			if (m_Desc.pN[i].x < min[0]) {
				min[0] = m_Desc.pN[i].x;
				if (min[0] < Min) {
					Min = min[0];
				}
			}
			if (m_Desc.pN[i].y < min[1]) {
				min[1] = m_Desc.pN[i].y;
				if (min[1] < Min) {
					Min = min[1];
				}
			}
			if (m_Desc.pN[i].z < min[2]) {
				min[2] = m_Desc.pN[i].z;
				if (min[2] < Min) {
					Min = min[2];
				}
			}

			if (m_Desc.pN[i].x > max[0]) {
				max[0] = m_Desc.pN[i].x;
				if (max[0] > Max) {
					Max = max[0];
				}
			}
			if (m_Desc.pN[i].y > max[1]) {
				max[1] = m_Desc.pN[i].y;
				if (max[1] > Max) {
					Max = max[1];
				}
			}
			if (m_Desc.pN[i].z > max[2]) {
				max[2] = m_Desc.pN[i].z;
				if (max[2] > Max) {
					Max = max[2];
				}
			}
		}

		if (Max > Min) {
			// get the min & max of the position vectors
			m_Desc.ScaleConst[1].x = Min;

			// get the stream size & compression/decompression scales
			if (Max - Min <= 2.f) {
				m_Desc.ScaleSize[1] = 1;
				m_Desc.ScaleConst[1].y = 255.f / (Max - Min);
				m_Desc.ScaleConst[1].z = (Max - Min) / 255.f;

			} else if (Max - Min <= 128.f) {
				m_Desc.ScaleSize[1] = 2;
				m_Desc.ScaleConst[1].y = 65535.f / (Max - Min);
				m_Desc.ScaleConst[1].z = (Max - Min) / 65535.f;
			} else {
				m_Desc.ScaleSize[1] = 4;
				m_Desc.ScaleConst[1].y = 1.f;
				m_Desc.ScaleConst[1].z = 1.f;
			}
		} else {
			m_Desc.ScaleSize[1] = 4;
			m_Desc.ScaleConst[1].x = 0.f;
			m_Desc.ScaleConst[1].y = 1.f;
			m_Desc.ScaleConst[1].z = 1.f;
		}
	}

	if (compress) {
		// get the ranges for the UVs
		FLOAT Min = FLT_MAX;
		FLOAT Max = -FLT_MAX;
		FLOAT min[3] = { Min, Min, Min };
		FLOAT max[3] = { Max, Max, Max };

		switch (m_Desc.NumUV) {
			case 1: {
				for (DWORD i = 0; i < m_Desc.NumV; i++) {
					// 1st UV set
					if (m_Desc.pUv[i].x < min[0]) {
						min[0] = m_Desc.pUv[i].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[i].y < min[1]) {
						min[1] = m_Desc.pUv[i].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[i].x > max[0]) {
						max[0] = m_Desc.pUv[i].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[i].y > max[1]) {
						max[1] = m_Desc.pUv[i].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			case 2: {
				for (DWORD i = 0, j = 0; i < m_Desc.NumV; i++, j += 2) {
					// 1st UV set
					if (m_Desc.pUv[j].x < min[0]) {
						min[0] = m_Desc.pUv[j].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j].y < min[1]) {
						min[1] = m_Desc.pUv[j].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}
					if (m_Desc.pUv[j].x > max[0]) {
						max[0] = m_Desc.pUv[j].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j].y > max[1]) {
						max[1] = m_Desc.pUv[j].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 2nd UV set
					if (m_Desc.pUv[j + 1].x < min[0]) {
						min[0] = m_Desc.pUv[j + 1].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 1].y < min[1]) {
						min[1] = m_Desc.pUv[j + 1].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 1].x > max[0]) {
						max[0] = m_Desc.pUv[j + 1].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 1].y > max[1]) {
						max[1] = m_Desc.pUv[j + 1].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			case 3: {
				for (DWORD i = 0, j = 0; i < m_Desc.NumV; i++, j += 3) {
					// 1st UV set
					if (m_Desc.pUv[j].x < min[0]) {
						min[0] = m_Desc.pUv[j].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j].y < min[1]) {
						min[1] = m_Desc.pUv[j].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}
					if (m_Desc.pUv[j].x > max[0]) {
						max[0] = m_Desc.pUv[j].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j].y > max[1]) {
						max[1] = m_Desc.pUv[j].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 2nd UV set
					if (m_Desc.pUv[j + 1].x < min[0]) {
						min[0] = m_Desc.pUv[j + 1].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 1].y < min[1]) {
						min[1] = m_Desc.pUv[j + 1].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 1].x > max[0]) {
						max[0] = m_Desc.pUv[j + 1].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 1].y > max[1]) {
						max[1] = m_Desc.pUv[j + 1].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 3rd UV set
					if (m_Desc.pUv[j + 2].x < min[0]) {
						min[0] = m_Desc.pUv[j + 2].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 2].y < min[1]) {
						min[1] = m_Desc.pUv[j + 2].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 2].x > max[0]) {
						max[0] = m_Desc.pUv[j + 2].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 2].y > max[1]) {
						max[1] = m_Desc.pUv[j + 2].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			case 4: {
				for (DWORD i = 0, j = 0; i < m_Desc.NumV; i++, j += 4) {
					// 1st UV set
					if (m_Desc.pUv[j].x < min[0]) {
						min[0] = m_Desc.pUv[j].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j].y < min[1]) {
						min[1] = m_Desc.pUv[j].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}
					if (m_Desc.pUv[j].x > max[0]) {
						max[0] = m_Desc.pUv[j].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j].y > max[1]) {
						max[1] = m_Desc.pUv[j].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 2nd UV set
					if (m_Desc.pUv[j + 1].x < min[0]) {
						min[0] = m_Desc.pUv[j + 1].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 1].y < min[1]) {
						min[1] = m_Desc.pUv[j + 1].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 1].x > max[0]) {
						max[0] = m_Desc.pUv[j + 1].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 1].y > max[1]) {
						max[1] = m_Desc.pUv[j + 1].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 3rd UV set
					if (m_Desc.pUv[j + 2].x < min[0]) {
						min[0] = m_Desc.pUv[j + 2].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 2].y < min[1]) {
						min[1] = m_Desc.pUv[j + 2].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 2].x > max[0]) {
						max[0] = m_Desc.pUv[j + 2].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 2].y > max[1]) {
						max[1] = m_Desc.pUv[j + 2].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
					// 4th UV set
					if (m_Desc.pUv[j + 3].x < min[0]) {
						min[0] = m_Desc.pUv[j + 3].x;
						if (min[0] < Min) {
							Min = min[0];
						}
					}
					if (m_Desc.pUv[j + 3].y < min[1]) {
						min[1] = m_Desc.pUv[j + 3].y;
						if (min[1] < Min) {
							Min = min[1];
						}
					}

					if (m_Desc.pUv[j + 3].x > max[0]) {
						max[0] = m_Desc.pUv[j + 3].x;
						if (max[0] > Max) {
							Max = max[0];
						}
					}
					if (m_Desc.pUv[j + 3].y > max[1]) {
						max[1] = m_Desc.pUv[j + 3].y;
						if (max[1] > Max) {
							Max = max[1];
						}
					}
				}
			} break;

			default: {
				Max = 2.f;
				Min = 0.f;

				min[0] = 0.f;
				max[0] = 0.f;

				min[1] = 0.f;
				max[1] = 0.f;
			}
		}

		if (Max > Min) {
			// get the min & max of the position vectors
			m_Desc.ScaleConst[2].x = Min;

			// get the stream size & compression/decompression scales
			if (Max - Min <= 2.f) {
				m_Desc.ScaleSize[2] = 1;
				m_Desc.ScaleConst[2].y = 255.f / (Max - Min);
				m_Desc.ScaleConst[2].z = (Max - Min) / 255.f;

			} else if (Max - Min <= 128.f) {
				m_Desc.ScaleSize[2] = 2;
				m_Desc.ScaleConst[2].y = 65535.f / (Max - Min);
				m_Desc.ScaleConst[2].z = (Max - Min) / 65535.f;
			} else {
				m_Desc.ScaleSize[2] = 4;
				m_Desc.ScaleConst[2].y = 1.f;
				m_Desc.ScaleConst[2].z = 1.f;
			}
		} else {
			m_Desc.ScaleSize[2] = 4;
			m_Desc.ScaleConst[2].x = 0.f;
			m_Desc.ScaleConst[2].y = 1.f;
			m_Desc.ScaleConst[2].z = 1.f;
		}
	}

	// calculate the streamed data array size
	m_Desc.StrmSize = m_Desc.NumV * (m_Desc.ScaleSize[0] + m_Desc.ScaleSize[1]) * 3;
	m_Desc.StrmSize += m_Desc.NumV * m_Desc.NumUV * m_Desc.ScaleSize[2] * 2;
	m_Desc.StrmSize += m_Desc.NumI * sizeof(WORD);
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////
void ReMesh::TriStrip() {
	PrimitiveGroup* primGroups = NULL;
	unsigned short numGroups = 0;

	if (m_Desc.NumI <= 0x7fff) {
		if (GenerateStrips((const unsigned short*)m_Desc.pI, (const unsigned int)m_Desc.NumI,
				(PrimitiveGroup**)&primGroups, (unsigned short*)&numGroups)) {
			if (primGroups[0].numIndices < m_Desc.NumI) {
				m_Desc.NumI = primGroups[0].numIndices;

				m_Desc.DataSize = m_Desc.NumV * (sizeof(Vector3f) * 2 +
													m_Desc.NumUV * sizeof(Vector2f)) +
								  m_Desc.NumI * sizeof(WORD);

				// get temp pointers to existing data arrays for copy
				void* pMem = (void*)m_Desc.pMem;
				void* pP = (void*)m_Desc.pP;
				void* pN = (void*)m_Desc.pN;
				void* pUv = (void*)m_Desc.pUv;
				void* pI = (void*)primGroups[0].indices;

				// allocate the (uncompressed) data array and get offset pointers
				m_Desc.pMem = (BYTE*)_aligned_malloc(m_Desc.DataSize, 32);
				m_Desc.pP = (Vector3f*)m_Desc.pMem;
				m_Desc.pN = (Vector3f*)((BYTE*)m_Desc.pP + m_Desc.NumV * sizeof(Vector3f));
				m_Desc.pUv = (Vector2f*)((BYTE*)m_Desc.pN + m_Desc.NumV * sizeof(Vector3f));
				m_Desc.pI = (WORD*)((BYTE*)m_Desc.pUv + m_Desc.NumV * m_Desc.NumUV * sizeof(Vector2f));

				// copy over existing data arrays
				memcpy(m_Desc.pP, pP, m_Desc.NumV * sizeof(Vector3f));
				memcpy(m_Desc.pN, pN, m_Desc.NumV * sizeof(Vector3f));
				memcpy(m_Desc.pUv, pUv, m_Desc.NumV * m_Desc.NumUV * sizeof(Vector2f));
				memcpy(m_Desc.pI, pI, m_Desc.NumI * sizeof(WORD));

				// delete the old mem block
				_aligned_free(pMem);
				m_Desc.PrimType = ReMeshPrimType::TRISTRIP;
			}
		}
		// delete internal tristrip data
		if (primGroups) {
			delete[] primGroups;
		}
	}
}

bool ReMesh::IntersectRay(const Vector3f& origin, const Vector3f& direction, float* distance, Vector3f* location, Vector3f* normal) {
	bool intersect = false; /* whether or not any intersections have been found */
	Vector3f n;

	_ASSERTE(this);

	if (this) {
		DWORD cullMode = D3DCULL_NONE; /* Objects with no material use 2-sided test */

		if (GetMaterial() && GetMaterial()->GetRenderState() && GetMaterial()->GetRenderState()->GetData())
			cullMode = GetMaterial()->GetRenderState()->GetData()->Mode.CullMode;

		float closest = FLT_MAX; /* distance to current closest intersection point */

		/* access mesh data */
		ReMeshData* pData = NULL;
		Access(&pData);

		if (pData && pData->pP && pData->NumV > 0 && (pData->PrimType == ReMeshPrimType::TRISTRIP || pData->PrimType == ReMeshPrimType::TRILIST)) {
			/* sanity check on num verts, anything larger than this is
			   almost surely a mistake i.e. reading garbage memory */
			if (pData->NumV < 500000) {
				Vector3f* verts = pData->pP;

				if (verts) {
					bool indexed = pData->NumI > 0 && pData->pI != NULL;

					int triangles;

					if (indexed)
						triangles = pData->PrimType == ReMeshPrimType::TRISTRIP ? (pData->NumI - 2) : (pData->NumI / 3);
					else
						triangles = pData->PrimType == ReMeshPrimType::TRISTRIP ? (pData->NumV - 2) : (pData->NumV / 3);

					int multiplier = pData->PrimType == ReMeshPrimType::TRISTRIP ? 1 : 3;

					/* this handles both triangle strips and triangle lists */
					for (int tri = 0; tri < triangles; tri++) {
						/* get triangle verts */
						WORD i0, i1, i2;

						int ix = multiplier * tri;

						if (indexed) {
							i0 = pData->pI[ix];
							i1 = pData->pI[ix + 1];
							i2 = pData->pI[ix + 2];
						} else {
							assert(ix >= 0 && ix <= (USHRT_MAX - 2)); // DRF - typecast int->WORD issue
							i0 = (WORD)ix;
							i1 = (WORD)ix + 1;
							i2 = (WORD)ix + 2;
						}

						/* ignore degenerate triangles */
						if (i0 != i1 && i0 != i2 && i1 != i2) {
							const Vector3f *p0 = &verts[i0], *p1 = &verts[i1], *p2 = &verts[i2];

							bool culled = false;

							if (cullMode != D3DCULL_NONE) {
								Vector3f e0(*p1 - *p0), e1(*p2 - *p0), tN;

								if (pData->PrimType != ReMeshPrimType::TRISTRIP || ((tri % 2) == 0))
									tN = e1.Cross(e0);
								else
									tN = e0.Cross(e1); /* Culling is reversed on odd-number triangles in a strip */

								float DdotN = direction.Dot(tN);
								culled = (cullMode == D3DCULL_CCW) ? (DdotN <= 0.0f) : (DdotN > 0.0f);
							}

							if (!culled) {
								/* do intersection test */
								float u, v, t;
								bool hit = IntersectLineTriangle(origin, direction, *p0, *p1, *p2, &t, &u, &v);
								//FLOAT u = 0, v = 0, dist = 0;
								//BOOL hit = D3DXIntersectTri(p0, p1, p2, &origin, &direction, &u, &v, &dist);

								if (hit && t > 0.0f) {
									intersect = true;

									/* if the caller isn't interested in exact intersection
									   information, then it's good enough to know any triangle
									   was hit */
									if (!distance && !location && !normal)
										return true;

									closest = std::min(closest, t);

									if (normal) {
										Vector3f e0, e1;
										e0 = *p1 - *p0;
										e1 = *p2 - *p0;
										n = e1.Cross(e0);
									}
								}
							}
						}
					}
				}
			}
		}

		if (intersect) {
			if (distance)
				*distance = closest;

			if (location)
				*location = origin + closest * direction;

			if (normal)
				*normal = n.GetNormalized();
		}
	}

	return intersect;
}

bool ReMesh::IntersectSphere(const Vector3f& origin, float radius, const Vector3f& orientation, float maxAngle, float* distance /* = NULL */, Vector3f* location /* = NULL */, Vector3f* normal /* = NULL */) {
	bool intersect = false; /* whether or not any intersections have been found */
	Vector3f n;

	_ASSERTE(this);

	if (this) {
		DWORD cullMode = D3DCULL_NONE; /* Objects with no material use 2-sided test */

		if (GetMaterial() && GetMaterial()->GetRenderState() && GetMaterial()->GetRenderState()->GetData())
			cullMode = GetMaterial()->GetRenderState()->GetData()->Mode.CullMode;

		float closest = FLT_MAX; /* distance to current closest intersection point */

		Vector3f closestHit;

		/* access mesh data */
		ReMeshData* pData = NULL;
		Access(&pData);

		if (pData && pData->pP && pData->NumV > 0 && (pData->PrimType == ReMeshPrimType::TRISTRIP || pData->PrimType == ReMeshPrimType::TRILIST)) {
			/* sanity check on num verts, anything larger than this is
			   almost surely a mistake i.e. reading garbage memory */
			if (pData->NumV < 500000) {
				Vector3f* verts = pData->pP;

				if (verts) {
					bool indexed = pData->NumI > 0 && pData->pI != NULL;

					int triangles;

					if (indexed)
						triangles = pData->PrimType == ReMeshPrimType::TRISTRIP ? (pData->NumI - 2) : (pData->NumI / 3);
					else
						triangles = pData->PrimType == ReMeshPrimType::TRISTRIP ? (pData->NumV - 2) : (pData->NumV / 3);

					int multiplier = pData->PrimType == ReMeshPrimType::TRISTRIP ? 1 : 3;

					/* this handles both triangle strips and triangle lists */
					for (int tri = 0; tri < triangles; tri++) {
						/* get triangle verts */
						WORD i0, i1, i2;

						int ix = multiplier * tri;

						if (indexed) {
							i0 = pData->pI[ix];
							i1 = pData->pI[ix + 1];
							i2 = pData->pI[ix + 2];
						} else {
							assert(ix >= 0 && ix <= (USHRT_MAX - 2)); // DRF - typecast int->WORD issue
							i0 = (WORD)ix;
							i1 = (WORD)ix + 1;
							i2 = (WORD)ix + 2;
						}

						/* ignore degenerate triangles */
						if (i0 != i1 && i0 != i2 && i1 != i2) {
							const Vector3f *p0 = &verts[i0], *p1 = &verts[i1], *p2 = &verts[i2];

							bool culled = false;

							if (cullMode != D3DCULL_NONE) {
								Vector3f e0(*p1 - *p0), e1(*p2 - *p0), tN;

								if (pData->PrimType != ReMeshPrimType::TRISTRIP || ((tri % 2) == 0))
									tN = e1.Cross(e0);
								else
									tN = e0.Cross(e1); /* Culling is reversed on odd-number triangles in a strip */

								Vector3f direction = origin - *p0;
								float DdotN = direction.Dot(tN);
								culled = (cullMode == D3DCULL_CCW) ? (DdotN <= 0.0f) : (DdotN > 0.0f);
							}

							if (!culled) {
								// do intersection test
								Vector3f hitLoc;
								float distSq = DistanceSquaredPointTriangle(origin, *p0, *p1, *p2, &hitLoc);

								// Test against radius
								if (distSq <= radius * radius) {
									// Test angle against the facing and max angle.
									// First, get the vector from the hit location to the origin of the sphere.
									Vector3f vecToPoint = hitLoc - origin;
									// Set the Y difference to 0, making it lie in the same plane.
									vecToPoint.y = 0.f;
									// Normalize the vector.
									vecToPoint.Normalize();

									float angle = vecToPoint.Dot(orientation);
									angle = acos(angle);

									if (angle <= maxAngle) {
										intersect = true;

										/* if the caller isn't interested in exact intersection
										   information, then it's good enough to know any triangle
										   was hit */
										if (!distance && !location && !normal)
											return true;

										if (distSq < closest) {
											closest = distSq;

											closestHit = hitLoc;

											if (normal) {
												Vector3f e0, e1;
												e0 = *p1 - *p0;
												e1 = *p2 - *p0;
												n = Cross(e1, e0);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (intersect) {
			if (distance)
				*distance = std::sqrt(closest);

			if (location)
				*location = closestHit;

			if (normal)
				*normal = n.GetNormalized();
		}
	}

	return intersect;
}

Physics::CollisionShapeMeshProperties ReMesh::GetPhysicsMeshProps() {
	ReMeshData* pMeshData;
	this->Access(&pMeshData);
	const Vector3f* pPoints = pMeshData->pP;
	size_t nVerts = pMeshData->NumV;
	size_t nIndexes = pMeshData->NumI;
	unsigned short* pIndexes = pMeshData->pI;

	assert(pMeshData->PrimType == ReMeshPrimType::TRISTRIP || pMeshData->PrimType == ReMeshPrimType::TRILIST);
	bool bIsStrip = pMeshData->PrimType == ReMeshPrimType::TRISTRIP;

	Physics::CollisionShapeMeshProperties meshProps(pPoints, nVerts, pIndexes, nIndexes, bIsStrip);
	return meshProps;
}

void ReMeshData::applyMatrix(const Matrix44f* pMatrix, const Matrix44f* pMatrixForNormal /*= NULL */) {
	BYTE* pTmpMem = (BYTE*)_aligned_malloc(sizeof(Vector3f) * NumV * 2, 32);
	if (!pTmpMem) {
		assert(false);
		return;
	}

	Vector3f* pTmpP = (Vector3f*)pTmpMem;
	Vector3f* pTmpN = (Vector3f*)((BYTE*)pTmpP + this->NumV * sizeof(Vector3f));

	TransformPoints(*pMatrix, pP, sizeof(*pP), pTmpP, sizeof(*pTmpP), NumV);
	memcpy(pP, pTmpP, this->NumV * sizeof(Vector3f));

	Matrix44f invTrans;
	if (pMatrixForNormal == NULL) {
		invTrans.MakeInverseOf(*pMatrix);
		invTrans.Transpose();
		pMatrixForNormal = &invTrans;
	}

	TransformVectors(*pMatrixForNormal, pN, sizeof(*pN), pTmpN, sizeof(*pTmpN), NumV);

	// Renormalize normal vectors -- note: will affect with normal vectors intentionally altered for special effect
	for (DWORD i = 0; i < NumV; i++)
		pN[i] = pTmpN[i].GetNormalized();

	_aligned_free(pTmpMem);
}

void ReMesh::DumpTrianglesToLog() const {
	const ReMeshData* pMeshData = &m_Desc;

	assert(pMeshData->PrimType == ReMeshPrimType::TRISTRIP || pMeshData->PrimType == ReMeshPrimType::TRILIST);

	size_t iStep = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? 1 : 3;
	size_t nIndices = pMeshData->NumI;
	size_t iEnd = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? (std::max<size_t>(2, nIndices) - 2) : (nIndices - nIndices % 3);
	Vector3f* pFirstPoint = pMeshData->pP;
	uint16_t* pFirstIndex = pMeshData->pI;
	for (size_t i = 0; i < iEnd; i += iStep) {
		size_t idx0 = pFirstIndex[i + 0];
		size_t idx1 = pFirstIndex[i + 1];
		size_t idx2 = pFirstIndex[i + 2];
		if (idx0 == idx1 || idx1 == idx2 || idx2 == idx0)
			continue;

		Vector3f apts[3] = { pFirstPoint[idx0], pFirstPoint[idx1], pFirstPoint[idx2] };
		std::sort(apts + 0, apts + 3, [](const Vector3f& left, const Vector3f& right) -> bool {
			for (int i = 0; i < 3; ++i) {
				if (left[i] != right[i])
					return left[i] < right[i];
			}
			return false;
		});

		std::ostringstream strm;
		for (size_t j = 0; j < 3; ++j) {
			const Vector3f& pt = apts[j];
			strm << "(" << pt[0] << "," << pt[1] << "," << pt[2] << ") ";
		}
		LogInfo(strm.str());
	}
}

void ReMeshData::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObjectSizeof(pMem, DataSize);
	}
}

void ReMeshState::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(LightCache);
		pMemSizeGadget->AddObject(WorldMatrix);
		pMemSizeGadget->AddObject(WorldViewProjMatrix);
		pMemSizeGadget->AddObject(WorldInvTransposeMatrix);
	}
}

void ReMesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		GetMemSizeofInternals(pMemSizeGadget);
	}
}

void ReMesh::GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const {
	pMemSizeGadget->AddObject(m_Desc);
	pMemSizeGadget->AddObject(m_State);
	pMemSizeGadget->AddObject(m_Shader);
	pMemSizeGadget->AddObject(m_Material);
}

} // namespace KEP
