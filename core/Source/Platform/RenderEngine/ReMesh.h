///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "d3dx9math.h"
#include "d3d9types.h"
#include "ReMaterial.h"
#include "ReMeshPrimType.h"
#include "Core/Math/KEPMath.h"
#include "KEPPhysics/Declarations.h"
#include "Core/Util/fast_mutex.h"
#include <js.h>

#undef min
#undef max

#define ReMESHINVALID 0xffffffff
#define MAX_VERTICES_PER_MESH 0xffff
#define MAX_INDICIES_PER_MESH 0xffff

namespace KEP {

class IMemSizeGadget;
struct ABBOX;

enum ReMeshStatus {
	ReMESHSTATUS_UNALLOC,
	ReMESHSTATUS_ALLOC,
	ReMESHSTATUS_STREAMED,
	ReMESHSTATUS_INIT,
	NA_ReMESHSTATUS
};

struct ReMeshData {
	// version 0 streaming header
	DWORD Version;
	DWORD NumV;
	DWORD NumI;
	DWORD NumUV;
	DWORD StrmSize;
	DWORD DataSize;
	UINT64 Hash;
	ReMeshPrimType PrimType;
	DWORD Status;
	DWORD Reserved0;
	// version 1 streaming header extension
	Vector3f ScaleConst[3];
	DWORD ScaleSize[3];
	DWORD Reserved1;
	// version 2 streaming header extension
	Vector3f ScaleConstTB[2];
	DWORD ScaleSizeTB[2];
	DWORD Reserved2;
	// mesh creation data
	BYTE* pMem;
	Vector3f* pP;
	Vector3f* pN;
	Vector2f* pUv;
	WORD* pI;

	// state for current mesh concatenation process
	unsigned NextVertexOffset;
	unsigned NextIndexOffset;
	unsigned NextBaseIndex;
	bool PadDegeneratedTriangles;

	float diameter; // temp: merge with the existing Radius value if possible

	// compute the stream header sizes
	static const UINT HeaderSize0 = sizeof(DWORD) * 9 + sizeof(long long);
	static const UINT HeaderSize1 = sizeof(DWORD) * 4 + sizeof(Vector3f) * 3;
	static const UINT HeaderSize2 = sizeof(DWORD) * 3 + sizeof(Vector3f) * 2;

	ReMeshData() :
			Reserved0(0), Reserved1(0), Reserved2(0) // For serialization consistency
	{
		// init version
		Version = 2;
		// init streamed data
		NumV = 0;
		// "Status" is a platform-INdependent data valid/invalid flag
		Status = ReMESHSTATUS_UNALLOC;
		Hash = ReMESHINVALID;
		// reset flags
		PrimType = ReMeshPrimType::TRILIST;
		// init mesh creation data
		pMem = NULL;
		// mesh concatenation state
		NextVertexOffset = NextIndexOffset = NextBaseIndex = 0;
		PadDegeneratedTriangles = false;

		diameter = 0;
	}

	void CalculateDiameter() {
		if (NumV > 0) {
			Vector3f mi(FLT_MAX, FLT_MAX, FLT_MAX);
			Vector3f mx(-mi);
			for (size_t i = 0; i < NumV; i++) {
				mi.x = std::min(mi.x, pP[i].x);
				mi.y = std::min(mi.y, pP[i].y);
				mi.z = std::min(mi.z, pP[i].z);
				mx.x = std::max(mx.x, pP[i].x);
				mx.y = std::max(mx.y, pP[i].y);
				mx.z = std::max(mx.z, pP[i].z);
			}
			diameter = Distance(mi, mx);
		}
	}

	float Diameter() { return diameter; }

	void applyMatrix(const Matrix44f* pMatrix, const Matrix44f* pMatrixForNormal = NULL);

	unsigned getPosBufSize() const { return NumV * sizeof(Vector3f); }
	unsigned getNmlBufSize() const { return NumV * sizeof(Vector3f); }
	unsigned getUVsBufSize() const { return NumV * NumUV * sizeof(Vector2f); }
	unsigned getIdxBufSize() const { return NumI * sizeof(WORD); }
	unsigned getTotalDataSize() { return getPosBufSize() + getNmlBufSize() + getUVsBufSize() + getIdxBufSize(); }

	unsigned getPosStride() const { return sizeof(Vector3f); }
	unsigned getNmlStride() const { return sizeof(Vector3f); }
	unsigned getUVsStride() const { return sizeof(Vector2f) * NumUV; }
	unsigned getIdxStride() const { return sizeof(WORD); }

	BYTE* getPosPtr(unsigned idx) const { return pMem + idx * sizeof(Vector3f); }
	BYTE* getNmlPtr(unsigned idx) const { return pMem + getPosBufSize() + idx * sizeof(Vector3f); }
	BYTE* getUVsPtr(unsigned idx) const { return pMem + getPosBufSize() + getNmlBufSize() + idx * NumUV * sizeof(Vector2f); }
	BYTE* getIdxPtr(unsigned idx) const { return pMem + getPosBufSize() + getNmlBufSize() + getUVsBufSize() + idx * sizeof(WORD); }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

struct ReMeshState {
	const Matrix44f* WorldMatrix;
	const Matrix44f* WorldViewProjMatrix;
	const Matrix44f* WorldInvTransposeMatrix;
	ReLightCache* LightCache;
	BOOL Cloned;
	BOOL Sorted;
	FLOAT Radius;

	ReMeshState() {
		WorldMatrix = NULL;
		WorldViewProjMatrix = NULL;
		WorldInvTransposeMatrix = NULL;
		LightCache = NULL;
		Cloned = FALSE;
		Sorted = FALSE;
		Radius = 0.f;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class ReMesh {
public:
	static fast_mutex m_mutex; // drf - added - crash fix

	ReMesh() :
			m_Shader(NULL) {}

	virtual ~ReMesh();

	void CalculateDiameter() { m_Desc.CalculateDiameter(); }

	ReMeshData* GetMeshData() { return &m_Desc; }
	virtual void Access(ReMeshData** desc) { *desc = &m_Desc; }
	virtual BOOL Allocate();
	virtual void Build(bool strip = true);
	void DumpTrianglesToLog() const;
	virtual BOOL Lock(Vector3f* /*pos*/, DWORD /*numV*/) { return false; }
	virtual void PreRender() = 0;
	virtual void Render() = 0;
	virtual void PostRender() = 0;
	virtual void Clone(ReMesh** /*mesh*/) = 0;
	virtual void Clone(ReMesh** /*mesh*/, bool /*forceClone*/) = 0;
	virtual void Reload() { ; }
	virtual BOOL Update() { return TRUE; }
	virtual BOOL EqualTo(ReMesh* mesh) const { return m_Desc.Hash == mesh->m_Desc.Hash; }
	virtual BOOL EqualTo(UINT64 hash) const { return m_Desc.Hash == hash; }
	virtual BOOL IsInvalid() const { return FALSE; }
	virtual void SetSorted(BOOL val) { m_State.Sorted = val; }
	virtual void SetCloned(BOOL val) { m_State.Cloned = val; }
	virtual void SetRadius(FLOAT val) { m_State.Radius = val; }
	virtual void SetBones(const Matrix44f* /*bones*/) { ; }
	virtual BOOL SetShader(ReShader* /*shader*/) { return FALSE; }
	virtual void SetMaterial(ReMaterialPtr material) { m_Material = material; }
	virtual BOOL CreateShader(const char* /*shader*/) { return FALSE; }
	virtual void DeleteShader() {
		delete m_Shader;
		m_Shader = NULL;
	}
	virtual BOOL CreateMaterial(const ReRenderStateData* /*state*/) { return FALSE; }
	virtual void DeleteMaterial() { m_Material = ReMaterialPtr(); }
	virtual void SetWorldMatrix(const Matrix44f* mat) { m_State.WorldMatrix = mat; }
	virtual void SetWorldViewProjMatrix(const Matrix44f* mat) { m_State.WorldViewProjMatrix = mat; }
	virtual void SetWorldInvTransposeMatrix(const Matrix44f* mat) { m_State.WorldInvTransposeMatrix = mat; }
	virtual void SetLightCache(ReLightCache* cache) { m_State.LightCache = cache; }
	virtual void InvalidateShader() { m_Shader->Invalidate(); }
	virtual const BOOL IsCloned() const { return m_State.Cloned; }
	virtual const BOOL IsSorted() const { return m_State.Sorted; }
	virtual const FLOAT GetRadius() const { return m_State.Radius; }
	virtual const DWORD GetNumUV() const final { return m_Desc.NumUV; }
	virtual const DWORD GetNumTris() const { return m_Desc.PrimType == ReMeshPrimType::TRISTRIP ? (m_Desc.NumI - 2) : m_Desc.PrimType == ReMeshPrimType::TRILIST ? (m_Desc.NumI / 3) : 0; }
	virtual const UINT64 GetHash() const { return m_Desc.Hash; }
	virtual const ReShader* GetShader() const final { return m_Shader; }
	virtual const ReMaterial* GetMaterial() const final { return m_Material.get(); }
	virtual ReMaterial* GetMaterialMutable() const final { return m_Material.get(); }
	virtual std::shared_ptr<ReMaterial> GetSharedMaterial() const final { return m_Material; }
	virtual const Matrix44f* GetBones() const { return NULL; }
	virtual const DWORD GetNumBones() const { return 0; }
	virtual const DWORD GetNumWeightsPerVertex() const { return 0; }
	virtual const Matrix44f* GetWorldMatrix() const { return m_State.WorldMatrix; }
	virtual const Matrix44f* GetWorldViewProjMatrix() const { return m_State.WorldViewProjMatrix; }
	virtual const Matrix44f* GetWorldInvTransposeMatrix() const { return m_State.WorldInvTransposeMatrix; }
	virtual const ReLightCache* GetLightCache() const final { return m_State.LightCache; }

	// ReMesh::IntersectRay
	//
	//   Performs an intersection test with the specified ray against the triangles of this ReMesh.
	//   Ignores any world matrix that might be set on this ReMesh -- the ray needs to be
	//   specified in object space.  Direction vector needs to be unit length.  Stores distance
	//   to intersection in 'distance' and point of intersection in 'location'.  Does not modify
	//   'distance' or 'location' if no intersection found.
	bool IntersectRay(const Vector3f& origin, const Vector3f& direction, float* distance = NULL, Vector3f* location = NULL, Vector3f* normal = NULL);

	bool IntersectSphere(const Vector3f& origin, float radius, const Vector3f& orientation, float maxAngle, float* distance = NULL, Vector3f* location = NULL, Vector3f* normal = NULL);

	Physics::CollisionShapeMeshProperties GetPhysicsMeshProps();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	void GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const;

private:
	virtual void TriStrip();
	virtual BOOL Restore() { return TRUE; }

protected:
	ReMeshData m_Desc;
	ReMeshState m_State;
	ReShader* m_Shader;
	std::shared_ptr<ReMaterial> m_Material;
};

} // namespace KEP