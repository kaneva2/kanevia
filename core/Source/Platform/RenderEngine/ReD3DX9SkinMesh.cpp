///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ReD3DX9SkinMesh.h"

#include "ReShader_SkinningSSE.h"
#include "ReD3DX9Shader.h"
#include "Common/include/IMemSizeGadget.h"

namespace KEP {

ReD3DX9SkinMesh::ReD3DX9SkinMesh() :
		ReSkinMesh(),
		ReD3DX9Mesh() {}

ReD3DX9SkinMesh::ReD3DX9SkinMesh(void* dev) :
		ReSkinMesh(),
		ReD3DX9Mesh((ReD3DX9DeviceState*)dev) {}

ReD3DX9SkinMesh::ReD3DX9SkinMesh(ReD3DX9SkinMesh& mesh) :
		ReSkinMesh(),
		ReD3DX9Mesh((ReD3DX9DeviceState*)mesh.m_pDev) {
	// copy over the ReMesh & ReSkinMesh properties
	m_Desc = mesh.m_Desc;
	m_SkinDesc = mesh.m_SkinDesc;

	// copy over the states
	m_State = mesh.m_State;
	m_SkinState = mesh.m_SkinState;

	// copy over the ReD3DX9Mesh properties
	m_pDev = mesh.m_pDev;
	m_pIB = mesh.m_pIB;
	m_VertexType = mesh.m_VertexType;
	m_HashVB = mesh.m_HashVB;
	m_HashIB = mesh.m_HashIB;
	m_HashVD = mesh.m_HashVD;
	m_VSize = mesh.m_VSize;

	// share vertex buffers when GPU skinning
	if (0) //mesh.m_Shader->ShaderType() == ReShaderType::GPU)
	{
		m_pVB = mesh.m_pVB;
		m_ResVB = ReD3DX9DEVICECLONED;
	}
	// create new managed vertex buffer with CPU skinning
	else {
		m_pVB = NULL;
		m_ResVB = ReD3DX9DEVICEINVALID;
	}

	// create new managed index buffer
	m_pIB = NULL;
	m_ResIB = ReD3DX9DEVICEINVALID;

	// this flag invokes a "CreateSystemResource" call to manage the vertex decl
	m_pVD = NULL;
	m_ResVD = ReD3DX9DEVICEINVALID;

	if (mesh.GetShader()) {
		// clone the shader
		ReShader* shader = mesh.m_Shader->Clone(this);

		// set the cloned shader
		SetShader(shader);
	}

	if (mesh.GetMaterial()) {
		SetMaterial(mesh.GetSharedMaterial());
	}

	// mark the mesh as cloned
	SetCloned(TRUE);
}

ReD3DX9SkinMesh::~ReD3DX9SkinMesh() {
	;
}

void ReD3DX9SkinMesh::SetMaterial(ReMaterialPtr material) {
	m_Material = material;

	// use the material's shader if using fixedfunc
	if (m_Shader) {
		if (m_Shader->ShaderType() == ReShaderType::FIXEDFUNC && material->GetRenderState()->GetData()->ShaderCode) {
			CreateShader(material->GetRenderState()->GetData()->ShaderCode);
		}
	} else {
		// Is this really the correct time to create the shader for a skinned dynamic object?
		// Probably not; I need a proper material from somewhere.  Ask Jonny about it.  Should
		// this be left in as a fail-safe?  Probably.
		CreateShader(NULL);
	}
}

BOOL ReD3DX9SkinMesh::Update() {
	if (!Restore())
		return FALSE;

	ReMaterial* pMaterial = GetMaterialMutable();

	// DRF - Update Moving Textures (sky clouds, 'in-motion' clothing, dance floor, etc)
	m_Material->Update();

	pMaterial->Render();

	ReShaderData data;
	if (m_Shader->ShaderType() == ReShaderType::GPU) {
		data.pBones = (D3DXMATRIX*)GetBones();
		data.pWorld = (D3DXMATRIX*)GetWorldMatrix();
		data.pWorldViewProj = (D3DXMATRIX*)GetWorldViewProjMatrix();
		data.pWorldInvTranspose = (D3DXMATRIX*)GetWorldInvTransposeMatrix();
		data.LightCache = (ReLightCache*)GetLightCache();
		data.State = pMaterial->GetRenderStateDataMutable();
		data.NumBones = GetNumBones();
		data.Radius = GetRadius();
		data.NumWeightsPerVertex = GetNumWeightsPerVertex();
	}

	if (!m_Shader->Update(&data)) {
		return FALSE;
	}

	return TRUE;
}

BOOL ReD3DX9SkinMesh::Restore() {
	if (!m_Shader)
		return FALSE;

	if (ReD3DX9Mesh::Invalid()) {
		switch (m_VertexType) {
			case ReD3DX9VertexType::POS_NORM_UV1:
			case ReD3DX9VertexType::POS_NORM_UV2:
			case ReD3DX9VertexType::POS_NORM_UV3:
			case ReD3DX9VertexType::POS_NORM_UV4: {
				if (!ReD3DX9Mesh::Restore(reinterpret_cast<D3DXVECTOR3*>(m_Desc.pP),
						reinterpret_cast<D3DXVECTOR3*>(m_Desc.pN), reinterpret_cast<D3DXVECTOR2*>(m_Desc.pUv),
						m_Desc.pI, m_Desc.NumV, m_Desc.NumI, m_Desc.PrimType)) {
					return FALSE;
				}
				break;
			}

			case ReD3DX9VertexType::POS_NORM_UV1D:
			case ReD3DX9VertexType::POS_NORM_UV2D:
			case ReD3DX9VertexType::POS_NORM_UV3D:
			case ReD3DX9VertexType::POS_NORM_UV4D: {
				if (!ReD3DX9Mesh::Restore(reinterpret_cast<D3DXVECTOR3*>(m_Desc.pP), reinterpret_cast<D3DXVECTOR3*>(m_Desc.pN),
						reinterpret_cast<D3DXVECTOR2*>(m_Desc.pUv), m_Desc.pI, m_Desc.NumV, m_Desc.NumI, m_Desc.PrimType)) {
					return FALSE;
				}
				break;
			}

			case ReD3DX9VertexType::POS_WTX:
			case ReD3DX9VertexType::POS_NORM_WTXUV1:
			case ReD3DX9VertexType::POS_NORM_WTXUV2:
			case ReD3DX9VertexType::POS_NORM_WTXUV3:
			case ReD3DX9VertexType::POS_NORM_WTXUV4: {
				if (!ReD3DX9Mesh::Restore(reinterpret_cast<D3DXVECTOR3*>(m_Desc.pP), reinterpret_cast<D3DXVECTOR3*>(m_Desc.pN),
						reinterpret_cast<D3DXVECTOR2*>(m_Desc.pUv), m_Desc.pI, m_SkinDesc.pBI, m_SkinDesc.pW, m_SkinDesc.Wpv, m_Desc.NumV, m_Desc.NumI, m_Desc.PrimType)) {
					return FALSE;
				}
				break;
			}

			default:
				return FALSE;
		}
	}
	return TRUE;
}

BOOL ReD3DX9SkinMesh::CreateShader(const char* shaderCode) {
	if (!m_pDev)
		return FALSE;

	if (m_Shader == NULL) {
		// invalidate current D3DX9 resources to rebuild them
		ReD3DX9Mesh::Invalidate();

		// get hardware caps
		auto HWCaps = m_pDev->GetRenderState()->GetHardwareCaps();
		// no GPU shader...use default CPU skinning shader
		if (shaderCode == NULL) {
			m_Shader = new ReShader_SkinningSSE(this);
		}
		// use GPU effect shader if HW allows
		else if (HWCaps >= ReD3DX9MINVERTEXSHADERSUPPORT) {
			m_Shader = new ReD3DX9Shader(m_pDev, shaderCode);
		}
		// use C skinning by default
		else {
			m_Shader = new ReShader_SkinningSSE(this);
		}
		if (!m_Shader)
			return FALSE;

		switch (m_Desc.NumUV) {
			case 1: {
				switch (m_Shader->ShaderType()) {
					case ReShaderType::CPU_GENERIC:
					case ReShaderType::CPU_SSE:
						m_VertexType = ReD3DX9VertexType::POS_NORM_UV1D;
						break;

					case ReShaderType::GPU:
						m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV1;
						break;

					default:
						return FALSE;
				}
			} break;

			case 2: {
				switch (m_Shader->ShaderType()) {
					case ReShaderType::CPU_GENERIC:
					case ReShaderType::CPU_SSE:
						m_VertexType = ReD3DX9VertexType::POS_NORM_UV2D;
						break;

					case ReShaderType::GPU:
						m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV2;
						break;

					default:
						return FALSE;
				}
			} break;

			case 3: {
				switch (m_Shader->ShaderType()) {
					case ReShaderType::CPU_GENERIC:
					case ReShaderType::CPU_SSE:
						m_VertexType = ReD3DX9VertexType::POS_NORM_UV3D;
						break;

					case ReShaderType::GPU:
						m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV3;
						break;

					default:
						return FALSE;
				}
			} break;

			case 4: {
				switch (m_Shader->ShaderType()) {
					case ReShaderType::CPU_GENERIC:
					case ReShaderType::CPU_SSE:
						m_VertexType = ReD3DX9VertexType::POS_NORM_UV4D;
						break;

					case ReShaderType::GPU:
						m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV4;
						break;

					default:
						return FALSE;
				}
			} break;

			default:
				return FALSE;
		}
	}
	return TRUE;
}

BOOL ReD3DX9SkinMesh::SetShader(ReShader* shader) {
	if (!m_pDev)
		return FALSE;

	// delete current shader
	delete m_Shader;
	m_Shader = NULL;

	// invalidate current D3DX9 resources to rebuild them
	ReD3DX9Mesh::Invalidate();

	// get hardware caps
	auto HWCaps = m_pDev->GetRenderState()->GetHardwareCaps();

	// use a default CPU skinning shader
	if (shader == NULL || shader->ShaderType() == ReShaderType::FIXEDFUNC) {
		m_Shader = new ReShader_SkinningSSE(this);
		if (!m_Shader)
			return FALSE;
	} else {
		m_Shader = shader;
	}

	switch (m_Shader->ShaderType()) {
		case ReShaderType::CPU_GENERIC:
		case ReShaderType::CPU_SSE: {
			switch (m_Desc.NumUV) {
				case 1:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV1D;
					break;

				case 2:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV2D;
					break;

				case 3:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV3D;
					break;

				case 4:
					m_VertexType = ReD3DX9VertexType::POS_NORM_UV4D;
					break;

				default:
					return FALSE;
			}
		} break;

		case ReShaderType::GPU: {
			if (HWCaps < ReD3DX9MINVERTEXSHADERSUPPORT) {
				return FALSE;
			}

			switch (m_Desc.NumUV) {
				case 1:
					m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV1;
					break;

				case 2:
					m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV2;
					break;

				case 3:
					m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV3;
					break;

				case 4:
					m_VertexType = ReD3DX9VertexType::POS_NORM_WTXUV4;
					break;

				default:
					return FALSE;
			}
		} break;

		default:
			return FALSE;
	}

	return TRUE;
}

void ReD3DX9SkinMesh::Render() {
	if (ReD3DX9Mesh::Invalid())
		return;

	m_Shader->Render();
	m_pDev->FlushVertexBufferCache();
	ReD3DX9Mesh::Render(m_Desc.NumV, m_Desc.NumI, m_Desc.PrimType);
}

void ReD3DX9SkinMesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		ReSkinMesh::GetMemSizeofInternals(pMemSizeGadget);
	}
}

} // namespace KEP
