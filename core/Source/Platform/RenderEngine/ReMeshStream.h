///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

class CArchive;

namespace KEP {

class ReMesh;
class ReSkinMesh;
class ReD3DX9DeviceState;

class ReMeshStream {
public:
	ReMeshStream() { ; }
	~ReMeshStream() { ; }

public:
	void WriteSkin(ReSkinMesh* mesh, CArchive& ar);
	void ReadSkin(ReSkinMesh** mesh, CArchive& ar, ReD3DX9DeviceState* dev);
	void Write(ReMesh* mesh, CArchive& ar);
	void Read(ReMesh** mesh, CArchive& ar, ReD3DX9DeviceState* dev);

private:
	float m_MinPos;
	float m_MinUvs;
	float m_MinNrm;
	float m_MinWgt;

	float m_PosConstC;
	float m_NrmConstC;
	float m_UvsConstC;
	float m_WgtConstC;
	float m_PosConstD;
	float m_NrmConstD;
	float m_UvsConstD;
	float m_WgtConstD;

private:
	// compress/decompress position vector
	void CompressPosByte(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			*((BYTE*)pM) = (BYTE)((pos[i] - m_MinPos) * m_PosConstC + .5f);
			pM += 1;
		}
	}

	void DeCompressPosByte(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			pos[i] = *((BYTE*)pM) * m_PosConstD + m_MinPos;
			pM += 1;
		}
	}

	void CompressPosWord(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			*((WORD*)pM) = (WORD)((pos[i] - m_MinPos) * m_PosConstC + .5f);
			pM += 2;
		}
	}

	void DeCompressPosWord(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			pos[i] = *((WORD*)pM) * m_PosConstD + m_MinPos;
			pM += 2;
		}
	}

	void CompressPosLong(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			*((DWORD*)pM) = (DWORD)((pos[i] - m_MinPos) * m_PosConstC + .5f);
			pM += 4;
		}
	}

	void DeCompressPosLong(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			pos[i] = *((LONG*)pM) * m_PosConstD + m_MinPos;
			pM += 4;
		}
	}

	void AssignPosFloat(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			*((FLOAT*)pM) = pos[i];
			pM += 4;
		}
	}

	void DeAssignPosFloat(BYTE*& pM, FLOAT* pos) {
		for (DWORD i = 0; i < 3; i++) {
			pos[i] = *((FLOAT*)pM);
			pM += 4;
		}
	}

	// compress/decompress normal vector
	void CompressNrmByte(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			*((BYTE*)pM) = (BYTE)((nrm[i] - m_MinNrm) * m_NrmConstC + .5f);
			pM += 1;
		}
	}

	void DeCompressNrmByte(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			nrm[i] = *((BYTE*)pM) * m_NrmConstD + m_MinNrm;
			pM += 1;
		}
	}

	void CompressNrmWord(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			*((WORD*)pM) = (WORD)((nrm[i] - m_MinNrm) * m_NrmConstC + .5f);
			pM += 2;
		}
	}

	void DeCompressNrmWord(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			nrm[i] = *((WORD*)pM) * m_NrmConstD + m_MinNrm;
			pM += 2;
		}
	}

	void CompressNrmLong(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			*((DWORD*)pM) = (DWORD)((nrm[i] - m_MinNrm) * m_NrmConstC + .5f);
			pM += 4;
		}
	}

	void DeCompressNrmLong(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			nrm[i] = *((LONG*)pM) * m_NrmConstD + m_MinNrm;
			pM += 4;
		}
	}

	void AssignNrmFloat(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			*((FLOAT*)pM) = nrm[i];
			pM += 4;
		}
	}

	void DeAssignNrmFloat(BYTE*& pM, FLOAT* nrm) {
		for (DWORD i = 0; i < 3; i++) {
			nrm[i] = *((FLOAT*)pM);
			pM += 4;
		}
	}

	// compress/decompress uv vector
	void CompressUvsByte(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			*((BYTE*)pM) = (BYTE)((uvs[i] - m_MinUvs) * m_UvsConstC + .5f);
			pM += 1;
		}
	}

	void DeCompressUvsByte(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			uvs[i] = *((BYTE*)pM) * m_UvsConstD + m_MinUvs;
			pM += 1;
		}
	}

	void CompressUvsWord(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			*((WORD*)pM) = (WORD)((uvs[i] - m_MinUvs) * m_UvsConstC + .5f);
			pM += 2;
		}
	}

	void DeCompressUvsWord(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			uvs[i] = *((WORD*)pM) * m_UvsConstD + m_MinUvs;
			pM += 2;
		}
	}

	void CompressUvsLong(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			*((DWORD*)pM) = (DWORD)((uvs[i] - m_MinUvs) * m_UvsConstC + .5f);
			pM += 4;
		}
	}

	void DeCompressUvsLong(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			uvs[i] = *((LONG*)pM) * m_UvsConstD + m_MinUvs;
			pM += 4;
		}
	}

	void AssignUvsFloat(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			*((FLOAT*)pM) = uvs[i];
			pM += 4;
		}
	}

	void DeAssignUvsFloat(BYTE*& pM, FLOAT* uvs) {
		for (DWORD i = 0; i < 2; i++) {
			uvs[i] = *((FLOAT*)pM);
			pM += 4;
		}
	}

	// compress/decompress weight
	void CompressWgtByte(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		DWORD total = 0;
		DWORD weight[4];

		for (DWORD i = 0; i < wpv; i++) {
			weight[i] = (DWORD)((wgt[i] - m_MinWgt) * m_WgtConstC + .5f);
			total += weight[i];
		}

		// weights sum to > 1 (255)...subtract diff from highest weight
		if (total > 255) {
			weight[0] -= total - 255;
		}
		// weights sum to < 1 (255)...add to lowest weight
		else if (total < 255) {
			weight[wpv - 1] += 255 - total;
		}

		for (DWORD i = 0; i < wpv; i++) {
			*((BYTE*)pM) = (BYTE)weight[i];
			pM += 1;
		}
	}

	void DeCompressWgtByte(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			wgt[i] = *((BYTE*)pM) * m_WgtConstD + m_MinWgt;
			pM += 1;
		}
	}

	void CompressWgtWord(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			*((WORD*)pM) = (WORD)((wgt[i] - m_MinWgt) * m_WgtConstC + .5f);
			pM += 2;
		}
	}

	void DeCompressWgtWord(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			wgt[i] = *((WORD*)pM) * m_WgtConstD + m_MinWgt;
			pM += 2;
		}
	}

	void CompressWgtLong(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			*((DWORD*)pM) = (DWORD)((wgt[i] - m_MinWgt) * m_WgtConstC + .5f);
			pM += 4;
		}
	}

	void DeCompressWgtLong(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			wgt[i] = *((LONG*)pM) * m_WgtConstD + m_MinWgt;
			pM += 4;
		}
	}

	void AssignWgtFloat(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			*((FLOAT*)pM) = wgt[i];
			pM += 4;
		}
	}

	void DeAssignWgtFloat(BYTE*& pM, FLOAT* wgt, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			wgt[i] = *((FLOAT*)pM);
			pM += 4;
		}
	}

	// bone index assignment/deassignment
	void AssignBixByte(BYTE*& pM, BYTE* bix, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			*((BYTE*)pM) = bix[i];
			pM += 1;
		}
	}

	void DeAssignBixByte(BYTE*& pM, BYTE* bix, DWORD wpv) {
		for (DWORD i = 0; i < wpv; i++) {
			bix[i] = *((BYTE*)pM);
			pM += 1;
		}
	}

	// index assignment/deassignment
	void AssignIdxWord(BYTE*& pM, WORD* idx) {
		*((WORD*)pM) = *idx;
		pM += 2;
	}

	void DeAssignIdxWord(BYTE*& pM, WORD* idx) {
		*idx = *((WORD*)pM);
		pM += 2;
	}
};

} // namespace KEP
