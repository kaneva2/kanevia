///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ReMesh.h"

#include <map>

namespace KEP {

class ReSkinMesh;
class ReMeshCache;
class ReRenderState;
class ReD3DX9DeviceState;

#define ReMESHCACHEMAXNUMMESHES 4096
#define ReMESHCACHEMAXNUMMATERIALS 4096
#define ReMESHCACHEMAXCACHESIZE 4096
#define ReMESHCACHEINVALID 0xffffffff

enum ReMESHCACHESORTKEY {
	ReMESHCACHESORTRENDERSTATE,
	ReMESHCACHESORTVERTEXSHADER,
	ReMESHCACHESORTBONEUPLOAD,
	ReMESHCACHESORTCAMERAZ,
	ReMESHCACHESORTORDER,
	NA_ReMESHCACHESORTKEY,
};

struct _ReMeshCacheFrame {
	// first mesh index of mesh cache
	WORD Start;
	// last mesh index of mesh cache
	WORD End;
	// the first mesh of the array of cache frame meshes
	const ReMesh* Mesh;
	// distance of cache frame meshes from camera
	FLOAT CamDist;

	_ReMeshCacheFrame() {
		Start = 0;
		End = 0;
		Mesh = NULL;
		CamDist = 0;
	}

	~_ReMeshCacheFrame() {
		;
	}
};

typedef __declspec(align(32)) _ReMeshCacheFrame ReMeshCacheFrame;

struct ReMeshCachePair {
	const ReMaterial* pMat;
	ReMesh* pMesh;

	ReMeshCachePair() {
		pMesh = NULL;
	};
	~ReMeshCachePair() {
		;
	}
};

/**********************************************************************************
 **************************** ReMaterialPool **************************************
 **********************************************************************************/
class ReMaterialPool {
	friend ReMeshCache;

public:
	ReMaterialPool(DWORD size = ReMESHCACHEMAXNUMMATERIALS);
	~ReMaterialPool() { Invalidate(); }

	ReMaterialPtr FindMaterial(ReMaterialPtr mat);
	ReMaterialPtr FindMaterial(ReRenderState* state);
	ReMaterialPtr AddMaterial(ReMaterialPtr mat);
	ReMaterialPtr GetMaterial(DWORD id);
	DWORD GetID(ReMaterialPtr mat);
	void Reset();

private:
	void Invalidate();

private:
	ReMaterialPtr* m_MaterialPool;
	DWORD m_MaxIDs;
	DWORD m_NumIDs;
};

/**********************************************************************************
 ****************************** ReMeshPool ****************************************
 **********************************************************************************/
class ReMeshPool {
public:
	ReMeshPool(DWORD size = ReMESHCACHEMAXNUMMESHES);
	~ReMeshPool() { Destroy(); }

	ReMesh* FindMesh(ReMesh* mesh);
	ReMesh* FindMesh(unsigned __int64 hash);
	ReMesh* AddMesh(ReMesh* mesh);
	void Reset();
	void Destroy();

private:
	typedef std::map<unsigned __int64, ReMesh*> meshes_t;
	meshes_t _meshes;
};

/**********************************************************************************
 ****************************** ReMeshCache ***************************************
 **********************************************************************************/
class ReMeshCache {
public:
	ReMeshCache(DWORD size = ReMESHCACHEMAXCACHESIZE);
	~ReMeshCache() { Invalidate(); }

	void BeginCache() {
		if (m_NumCacheFrames < m_MaxIDs) {
			m_CacheFrame[m_NumCacheFrames]->Start = (WORD)m_NumIDs;
		}
	}

	void EndCache(ReMESHCACHESORTKEY /*key*/ = NA_ReMESHCACHESORTKEY) {
		if (m_NumCacheFrames < m_MaxIDs) {
			// NOTE: All meshes in cacheframe assumed to have same shader!
			m_CacheFrame[m_NumCacheFrames]->Mesh = (ReMesh*)FirstMesh(m_NumCacheFrames);
			m_CacheFrame[m_NumCacheFrames++]->End = (WORD)m_NumIDs;
		}
	}
	void AddMesh(ReMesh* mesh) {
		if (m_NumIDs < m_MaxIDs) {
			m_MeshCache[m_NumIDs].pMesh = mesh;
			m_MeshCache[m_NumIDs++].pMat = mesh->GetMaterial();
		}
	}
	void AddMesh(ReMesh* mesh, const ReMaterial* mat) {
		if (m_NumIDs < m_MaxIDs) {
			m_MeshCache[m_NumIDs].pMesh = mesh;
			m_MeshCache[m_NumIDs++].pMat = mat;
		}
	}
	void AddMesh(ReMeshCachePair* pair) {
		if (m_NumIDs < m_MaxIDs) {
			m_MeshCache[m_NumIDs++] = *pair;
		}
	}
	void AddMeshes(ReMeshCachePair** pairs, DWORD num) {
		for (DWORD i = 0; i < num; i++) {
			if (m_NumIDs < m_MaxIDs) {
				if (pairs[i]->pMesh == NULL) {
					break;
				}
				m_MeshCache[m_NumIDs++] = *pairs[i];
			}
		}
	}
	void AddMeshes(ReMesh** meshes, DWORD num) {
		for (DWORD i = 0; i < num; i++) {
			if (m_NumIDs < m_MaxIDs) {
				if (meshes[i] == NULL) {
					break;
				}
				m_MeshCache[m_NumIDs].pMesh = meshes[i];
				m_MeshCache[m_NumIDs++].pMat = meshes[i]->GetMaterial();
			}
		}
	}
	void Render(void* dev, ReMESHCACHESORTKEY key = NA_ReMESHCACHESORTKEY, bool lighting = true);
	void Reset() {
		m_NumCacheFrames = 0;
		m_NumIDs = 0;
	}

private:
	void Invalidate();
	void SortFrames(ReMESHCACHESORTKEY key = ReMESHCACHESORTVERTEXSHADER);
	void RenderCacheFrame(const ReMeshCacheFrame* cacheFrame, bool lighting = true) const;
	ReMesh* const FirstMesh(UINT i) const { return m_MeshCache[m_CacheFrame[i]->Start].pMesh; }
	const ReMaterial* FirstMat(UINT i) const { return m_MeshCache[m_CacheFrame[i]->Start].pMat; }

private:
	ReD3DX9DeviceState* m_Device;
	ReMeshCachePair* m_MeshCache;
	ReMeshCacheFrame** m_CacheFrame;
	DWORD m_NumCacheFrames;
	DWORD m_MaxIDs;
	DWORD m_NumIDs;
};

} // namespace KEP
