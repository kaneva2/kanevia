///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define ReD3DX9DEVICERENDERSTATELIGHTS 256 // max lights per level
#define ReD3DX9DEVICERENDERSTATELIGHTATT0 1.f // default point light attenuation0
#define ReD3DX9DEVICERENDERSTATELIGHTATT1 .1f // default point light attenuation1
#define ReD3DX9DEVICERENDERSTATELIGHTATT2 0.01f // default point light attenuation2
#define ReD3DX9DEVICERENDERSTATEMAXCON 32 // max number of vertex shader register constants
#define ReD3DX9DEVICEMAXUPLOADLIGHTS 8 // max lights for fixed function
#define ReD3DX9DEVICEMAXGPULIGHTS 8 // max lights uploaded to vertex shader constant registers
#define ReD3DX9DEVICERENDERSTATERADIUSADJ 1.5f // light radius overlap test adjustment
#define ReD3DX9DEVICEMAXRESOURCEOWNERS 256 // max owners per system resource
#define ReD3DX9DEVICESYSTEMRESOURCESHIFT 16 // system resource index shift
#define ReD3DX9DEVICESYSTEMRESOURCEMASK 0xffff // system resource owner mask
#define ReD3DX9DEVICEMAXRESOURCES 65536u * 8u // max resources per type
#define ReD3DX9DEFAULTNUMRESDYNAMIC 32768u
#define ReD3DX9DEFAULTNUMRESSTATIC 65536u
#define ReD3DX9DEFAULTNUMRESSYSTEM 16384u
#define ReD3DX9DEVICEINVALID 0xffffffff
#define ReD3DX9DEVICECLONED 0xfffffffe
#define ReRealMAXVAL (FLT_MAX)
#define ReRealMINVAL (FLT_MIN)
