///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#ifndef WINVER
#define WINVER 0x0501
#endif

#include <AFX.h> // for ASSERT macro

#include "common\include\Rect.h"

#include "ReDynamicGeom.h"
#include "ReUtils.h"
#include "Core/Util/Unicode.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

#define MAX_BUFFERSIZE 500
#define Z_POS 2.0f
#define FVF D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1

//	for every float, there will be a pointer to a const char * containing the name
//	of the float, and a float to display, intermixed.
//	For example, for displaying of 2 floats, you would call this function
//	like so:
//	SHOW_FLOATS( 2, "FirstFloat", 3.4, "SecondFloat", 7.8 );
//	For 4 floats:
//	SHOW_FLOATS( 4, "FirstFloat", 3.4, "SecondFloat", 7.8, "thirdFloat", 8.2, "FourthFloat", 10.87 );
//	WARNING THIS FUNCTION IS VERY DANGEROUS IF YOU DONT KNOW WHAT YOUR DOING DON'T USE IT
void SHOW_FLOATS(int numFloats, ...) {
	int i = 0;
	va_list argList;
	std::string finalStringToShow = "";

	double valToUse = 0;

	const char *stringToUse;
	static char tempBuffer[100];

	//	Initialize variable arguments
	va_start(argList, numFloats);

	while (i < numFloats) {
		//	Grab vals
		stringToUse = va_arg(argList, char *);
		valToUse = va_arg(argList, double);

		finalStringToShow += stringToUse;
		sprintf_s(tempBuffer, _countof(tempBuffer), ": %.3f ", valToUse);

		finalStringToShow += tempBuffer;

		i++;
	}

	//	Reset variable arguments
	va_end(argList);

	finalStringToShow += '\n';

	OutputDebugStringA(finalStringToShow.c_str());
}

void ReDynamicGeom::ReSize(DWORD viewPortWidth, DWORD viewPortHeight) {
	if (((mViewPortWidth == viewPortWidth) && (mViewPortHeight == viewPortHeight)) ||
		((viewPortWidth == 0) && (viewPortHeight == 0))) {
		return;
	}

	mViewPortWidth = viewPortWidth;
	mViewPortHeight = viewPortHeight;

	D3DXMatrixOrthoOffCenterLH(&mOrthoMatrix, 0, (float)viewPortWidth,
		0, (float)viewPortHeight, 0.0f, 200.0f);
}

void ReDynamicGeom::OnLostDevice() {
	if (mpFont)
		mpFont->OnLostDevice();
}

void ReDynamicGeom::OnReset() {
	if (mpFont)
		mpFont->OnResetDevice();
}

void ReDynamicGeom::Init(RECT viewportRect, LPDIRECT3DDEVICE9 dev, const std::string &appDir) {
	// Copy Application Base Directory
	if (!appDir.empty())
		mAppDir = appDir;

	mViewPortWidth = RectWidth(viewportRect);
	mViewPortHeight = RectHeight(viewportRect);

	mpDevice = dev;

	HRESULT err;

	if (S_OK != (err = D3DXCreateFontW(dev, // D3D device
					 0, // Height
					 0, // Width
					 FW_BOLD, // Weight
					 1, // MipLevels, 0 = autogen mipmaps
					 FALSE, // Italic
					 DEFAULT_CHARSET, // CharSet
					 OUT_DEFAULT_PRECIS, // OutputPrecision
					 DEFAULT_QUALITY, // Quality
					 DEFAULT_PITCH | FF_DONTCARE, // PitchAndFamily
					 L"Arial", // pFaceName
					 &mpFont)) // ppFont
	) {
		ASSERT(false);
		mpFont = NULL;
	}

	//	Set up ortho matrix
	D3DXMatrixIdentity(&mIdent);

	D3DXMatrixOrthoOffCenterLH(&mOrthoMatrix, 0, (float)mViewPortWidth,
		0, (float)mViewPortHeight, 0.0f, 200.0f);

	//	Setup vertex buffer
	//	TODO: Double buffer this when we switch to multithreaded renderer
	mpDevice->CreateVertexBuffer(MAX_BUFFERSIZE * sizeof(QUAD_VERTEX),
		D3DUSAGE_WRITEONLY, FVF, D3DPOOL_MANAGED, &mVertexBuffer[0], NULL);

	mpDevice->CreateVertexBuffer(MAX_BUFFERSIZE * sizeof(QUAD_VERTEX),
		D3DUSAGE_WRITEONLY, FVF, D3DPOOL_MANAGED, &mVertexBuffer[1], NULL);
}

TexData *ReDynamicGeom::GetSetTexture(DWORD nameHash, std::string *textureName) {
	HashTexMap::iterator i;

	i = mTextureMap.find(nameHash);

	if (i == mTextureMap.end()) {
		if (!textureName) {
			//	Texture not found, and no texture name given. Error!
			return NULL;
		}

		//	Texture not found, add
		IDirect3DTexture9 *pTex = NULL;
		WIN32_FIND_DATA FindFileData;
		TexData *p = NULL;

		std::string mapsModelsDir;

		mapsModelsDir.assign(mAppDir.c_str());
		mapsModelsDir.append("\\MapsModels\\");
		mapsModelsDir += (*textureName);
		mapsModelsDir += ".dds";

		std::string correctDir;

		correctDir.assign(mAppDir.c_str());
		correctDir.append("\\MapsModels\\DanceParty\\");
		correctDir += (*textureName);
		correctDir += ".dds";

		std::wstring correctDirW = Utf8ToUtf16(correctDir).c_str();
		std::wstring mapsModelsDirW = Utf8ToUtf16(mapsModelsDir).c_str();
		if (FindFirstFileW(correctDirW.c_str(), &FindFileData) != INVALID_HANDLE_VALUE) {
			// first try DanceParty dir.
			D3DXCreateTextureFromFileW(mpDevice, correctDirW.c_str(), &pTex);
		} else {
			//	Look in the MapsModels Dir
			if (FindFirstFileW(mapsModelsDirW.c_str(), &FindFileData) != INVALID_HANDLE_VALUE) {
				D3DXCreateTextureFromFileW(mpDevice, mapsModelsDirW.c_str(), &pTex);
			}
		}

		if (pTex) {
			p = new TexData;
			D3DSURFACE_DESC desc;
			IDirect3DSurface9 *pSurface = NULL;

			p->pTex = pTex;

			//	Keep name of texture around, for debugging
			strcpy_s(p->texName, _countof(p->texName), textureName->c_str());

			//	Dimensions
			pTex->GetSurfaceLevel(0, &pSurface);
			if (pSurface) {
				// get the description of the surface
				if (D3D_OK == pSurface->GetDesc(&desc)) {
					p->height = desc.Height;
					p->width = desc.Width;
				}

				pSurface->Release();
			}

			mTextureMap.insert(HashTexPair(nameHash, p));
		} else {
			//	Error
			return NULL;
		}

		return p;
	} else {
		//	Found texture.
		return i->second;
	}
}

void ReDynamicGeom::DrawQuadEx(QUAD_POINT BottomLeft, QUAD_POINT TopRight, std::string &textureName, DWORD color,
	float sizeToScaleBy, float scaleCur, float scaleMax, D3DTEXTUREFILTERTYPE filterType,
	QUAD_UV BottomLeftUV, QUAD_UV TopRightUV) {
	float scaledPosMaxX = BottomLeft.x - sizeToScaleBy;
	float scaledPosMaxY = BottomLeft.y - sizeToScaleBy;

	BottomLeft.x = LERP(scaledPosMaxX, BottomLeft.x, scaleMax, scaleCur);
	BottomLeft.y = LERP(scaledPosMaxY, BottomLeft.y, scaleMax, scaleCur);

	scaledPosMaxX = TopRight.x + sizeToScaleBy;
	scaledPosMaxY = TopRight.y + sizeToScaleBy;

	TopRight.x = NLERP(TopRight.x, scaledPosMaxX, scaleMax, scaleCur);
	TopRight.y = NLERP(TopRight.y, scaledPosMaxY, scaleMax, scaleCur);

	//	Check texture. If the texture hasn't changed, then don't render
	//	if it has changed, we need to render the current quad batch
	DWORD nameHash = ReHash((BYTE *)textureName.c_str(), (DWORD)textureName.length(), 0);

	if (mBlockIndex < 0 || nameHash != mRenderBlocks[mBlockIndex].textureHash) {
		//	Start a new render block - Increase block count
		mBlockIndex++;

		mRenderBlocks[mBlockIndex].numPrimatives = 0;
		mRenderBlocks[mBlockIndex].sizeOfRenderBlock = 0;

		mRenderBlocks[mBlockIndex].pTexture = GetSetTexture(nameHash, &textureName);
		mRenderBlocks[mBlockIndex].textureHash = nameHash;

		mRenderBlocks[mBlockIndex].fType = filterType;

		//	If this is the first block, then the offset into the buffer is 0
		//	else, use the previous blocks offset + it's size
		if (mBlockIndex == 0)
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = 0;
		else {
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = (mRenderBlocks[mBlockIndex - 1].offsetIntoBuffer +
														   mRenderBlocks[mBlockIndex - 1].sizeOfRenderBlock);
		}
	}

	//	Lock Buffer
	QUAD_VERTEX *pVert = NULL;

	//	Size of this draw
	const int drawSize = 6 * sizeof(QUAD_VERTEX);

	//	6 verts per quad, since D3DX has yet to support QUAD prim types
	mVertexBuffer[0]->Lock(mRenderBlocks[mBlockIndex].offsetIntoBuffer + mRenderBlocks[mBlockIndex].sizeOfRenderBlock,
		drawSize, (void **)&pVert, 0);

	if (!pVert)
		return;

	// Tri 1 vertex 1 lower left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	pVert++;

	// Tri 1 vertex 2 bottom right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	pVert++;

	// Tri 1 vertex 3 Upper left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 1 upper right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 2 upper left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 3 lower right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	mVertexBuffer[0]->Unlock();

	//	Increase the size of this block
	mRenderBlocks[mBlockIndex].sizeOfRenderBlock += drawSize;
	mRenderBlocks[mBlockIndex].numPrimatives += 2;
}

void ReDynamicGeom::DrawQuad(QUAD_POINT BottomLeft, QUAD_POINT TopRight, std::string &textureName, D3DTEXTUREFILTERTYPE filterType,
	QUAD_UV BottomLeftUV, QUAD_UV TopRightUV) {
	//	Check texture. If the texture hasn't changed, then don't render
	//	if it has changed, we need to render the current quad batch
	DWORD nameHash = ReHash((BYTE *)textureName.c_str(), (DWORD)textureName.length(), 0);

	if (mBlockIndex < 0 || nameHash != mRenderBlocks[mBlockIndex].textureHash) {
		//	Start a new render block - Increase block count
		mBlockIndex++;

		mRenderBlocks[mBlockIndex].numPrimatives = 0;
		mRenderBlocks[mBlockIndex].sizeOfRenderBlock = 0;

		mRenderBlocks[mBlockIndex].pTexture = GetSetTexture(nameHash, &textureName);
		mRenderBlocks[mBlockIndex].textureHash = nameHash;

		mRenderBlocks[mBlockIndex].fType = filterType;

		//	If this is the first block, then the offset into the buffer is 0
		//	else, use the previous blocks offset + it's size
		if (mBlockIndex == 0)
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = 0;
		else {
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = (mRenderBlocks[mBlockIndex - 1].offsetIntoBuffer +
														   mRenderBlocks[mBlockIndex - 1].sizeOfRenderBlock);
		}
	}

	//	Lock Buffer
	QUAD_VERTEX *pVert = NULL;

	//	No color
	DWORD color = D3DCOLOR_RGBA(255, 255, 255, 255);

	//	Size of this draw
	const int drawSize = 6 * sizeof(QUAD_VERTEX);

	//	6 verts per quad, since D3DX has yet to support QUAD prim types
	mVertexBuffer[0]->Lock(mRenderBlocks[mBlockIndex].offsetIntoBuffer + mRenderBlocks[mBlockIndex].sizeOfRenderBlock,
		drawSize, (void **)&pVert, 0);

	if (!pVert)
		return;

	// Tri 1 vertex 1 lower left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	pVert++;

	// Tri 1 vertex 2 bottom right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	pVert++;

	// Tri 1 vertex 3 Upper left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 1 upper right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 2 upper left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 3 lower right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	mVertexBuffer[0]->Unlock();

	//	Increase the size of this block
	mRenderBlocks[mBlockIndex].sizeOfRenderBlock += drawSize;
	mRenderBlocks[mBlockIndex].numPrimatives += 2;
}

void ReDynamicGeom::DrawQuad(QUAD_POINT BottomLeft, QUAD_POINT TopRight, DWORD color) {
	//	Lock Buffer
	QUAD_VERTEX *pVert = NULL;

	//	No texture
	DWORD nameHash = 0;

	if (mBlockIndex < 0 || nameHash != mRenderBlocks[mBlockIndex].textureHash) {
		//	Start a new render block - Increase block count
		mBlockIndex++;
		mRenderBlocks[mBlockIndex].numPrimatives = 0;
		mRenderBlocks[mBlockIndex].sizeOfRenderBlock = 0;

		mRenderBlocks[mBlockIndex].pTexture = NULL;
		mRenderBlocks[mBlockIndex].textureHash = nameHash;

		mRenderBlocks[mBlockIndex].fType = D3DTEXF_LINEAR;

		//	If this is the first block, then the offset into the buffer is 0
		//	else, use the previous blocks offset + it's size
		if (mBlockIndex == 0)
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = 0;
		else {
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = (mRenderBlocks[mBlockIndex - 1].offsetIntoBuffer +
														   mRenderBlocks[mBlockIndex - 1].sizeOfRenderBlock);
		}
	}

	//	Size of this draw
	const int drawSize = 6 * sizeof(QUAD_VERTEX);

	//	6 verts per quad, since D3DX has yet to support QUAD prim types
	mVertexBuffer[0]->Lock(mRenderBlocks[mBlockIndex].offsetIntoBuffer + mRenderBlocks[mBlockIndex].sizeOfRenderBlock,
		drawSize, (void **)&pVert, 0);

	if (!pVert)
		return;

	// Tri 1 vertex 1 upper left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = 0.0f;
	pVert->mColor = color;

	pVert++;

	// Tri 1 vertex 2 bottom left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = 0.0f;
	pVert->mColor = color;

	pVert++;

	// Tri 1 vertex 3 Upper right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = 0.0f;
	pVert->mColor = color;

	pVert++;

	// Tri 2 vertex 1 lower left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = 0.0f;
	pVert->mColor = color;

	pVert++;

	// Tri 2 vertex 2 bottom right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = 0.0f;
	pVert->mColor = color;

	pVert++;

	// Tri 2 vertex 3 upper right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = 0.0f;
	pVert->mColor = color;

	mVertexBuffer[0]->Unlock();

	//	Increase the size of this block
	mRenderBlocks[mBlockIndex].sizeOfRenderBlock += drawSize;
	mRenderBlocks[mBlockIndex].numPrimatives += 2;
}

void ReDynamicGeom::DrawQuad(QUAD_POINT BottomLeft, QUAD_POINT TopRight, DWORD color, std::string &textureName,
	D3DTEXTUREFILTERTYPE filterType, QUAD_UV BottomLeftUV, QUAD_UV TopRightUV) {
	//	Check texture. If the texture hasn't changed, then don't render
	//	if it has changed, we need to render the current quad batch
	DWORD nameHash = ReHash((BYTE *)textureName.c_str(), (DWORD)textureName.length(), 0);

	if (mBlockIndex < 0 || nameHash != mRenderBlocks[mBlockIndex].textureHash) {
		//	Start a new render block - Increase block count
		mBlockIndex++;

		mRenderBlocks[mBlockIndex].numPrimatives = 0;
		mRenderBlocks[mBlockIndex].sizeOfRenderBlock = 0;

		mRenderBlocks[mBlockIndex].pTexture = GetSetTexture(nameHash, &textureName);
		mRenderBlocks[mBlockIndex].textureHash = nameHash;

		mRenderBlocks[mBlockIndex].fType = filterType;

		//	If this is the first block, then the offset into the buffer is 0
		//	else, use the previous blocks offset + it's size
		if (mBlockIndex == 0)
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = 0;
		else {
			mRenderBlocks[mBlockIndex].offsetIntoBuffer = (mRenderBlocks[mBlockIndex - 1].offsetIntoBuffer +
														   mRenderBlocks[mBlockIndex - 1].sizeOfRenderBlock);
		}
	}

	//	Lock Buffer
	QUAD_VERTEX *pVert = NULL;

	//	Size of this draw
	const int drawSize = 6 * sizeof(QUAD_VERTEX);

	//	6 verts per quad, since D3DX has yet to support QUAD prim types
	mVertexBuffer[0]->Lock(mRenderBlocks[mBlockIndex].offsetIntoBuffer + mRenderBlocks[mBlockIndex].sizeOfRenderBlock,
		drawSize, (void **)&pVert, 0);

	if (!pVert)
		return;

	// Tri 1 vertex 1 lower left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	pVert++;

	// Tri 1 vertex 2 bottom right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	pVert++;

	// Tri 1 vertex 3 Upper left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 1 upper right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 2 upper left
	pVert->mPos.x = BottomLeft.x;
	pVert->mPos.y = TopRight.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = BottomLeftUV.u;
	pVert->mUV.y = TopRightUV.v;

	pVert++;

	// Tri 2 vertex 3 lower right
	pVert->mPos.x = TopRight.x;
	pVert->mPos.y = BottomLeft.y;
	pVert->mPos.z = Z_POS;
	pVert->mColor = color;
	pVert->mUV.x = TopRightUV.u;
	pVert->mUV.y = BottomLeftUV.v;

	mVertexBuffer[0]->Unlock();

	//	Increase the size of this block
	mRenderBlocks[mBlockIndex].sizeOfRenderBlock += drawSize;
	mRenderBlocks[mBlockIndex].numPrimatives += 2;
}

void ReDynamicGeom::DrawText(const char *textToDraw, int xPos, int yPos, D3DXCOLOR color) {
	mTextIndex++;

	SetRect(&mTextBlocks[mTextIndex].rc, xPos, mViewPortHeight - yPos, 0, 0);

	mTextBlocks[mTextIndex].textToDrawW = Utf8ToUtf16(textToDraw);
	mTextBlocks[mTextIndex].color = color;
}

void ReDynamicGeom::DrawDynamicGeom() {
	if (mBlockIndex < 0)
		return;

	//	TODO: This should be part of the block render state
	mpDevice->SetRenderState(D3DRS_ZWRITEENABLE, false);
	mpDevice->SetRenderState(D3DRS_ALPHATESTENABLE, false);

	//	Set alpha
	mpDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	mpDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	mpDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);

	mpDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	mpDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	mpDevice->SetTransform(D3DTS_VIEW, &mIdent);
	mpDevice->SetTransform(D3DTS_WORLD, &mIdent);
	mpDevice->SetTransform(D3DTS_PROJECTION, &mOrthoMatrix);

	mpDevice->SetFVF(FVF);

	TexData *pCurrentTex;

	//	Go through all the render blocks and render them
	int i = 0;
	for (; i <= mBlockIndex; i++) {
		pCurrentTex = mRenderBlocks[i].pTexture;

		mpDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, mRenderBlocks[i].fType);
		mpDevice->SetSamplerState(0, D3DSAMP_MINFILTER, mRenderBlocks[i].fType);
		mpDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

		mpDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		mpDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

		//	Set the texture
		if (pCurrentTex)
			mpDevice->SetTexture(0, pCurrentTex->pTex);
		else
			mpDevice->SetTexture(0, NULL);

		mpDevice->SetStreamSource(0, mVertexBuffer[0], mRenderBlocks[i].offsetIntoBuffer, sizeof(QUAD_VERTEX));

		mpDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, mRenderBlocks[i].numPrimatives);
	}

	//	Reset block Index
	mBlockIndex = -1;

	//	Draw Text
	for (i = 0; i <= mTextIndex; i++) {
		if (mpFont)
			mpFont->DrawText(NULL, mTextBlocks[i].textToDrawW.c_str(), -1, &mTextBlocks[i].rc, DT_NOCLIP, mTextBlocks[i].color);
	}

	//	reset text index
	mTextIndex = -1;
}

void ReDynamicGeom::FreeAll() {
	HashTexMap::iterator i;

	for (i = mTextureMap.begin(); i != mTextureMap.end(); i++) {
		//	Release saved Dx Textures
		if (i->second) {
			if (i->second->pTex)
				i->second->pTex->Release();

			delete i->second;
			i->second = NULL;
		}
	}

	mTextureMap.clear();

	if (mVertexBuffer[0])
		mVertexBuffer[0]->Release();
	if (mVertexBuffer[1])
		mVertexBuffer[1]->Release();

	mVertexBuffer[0] = NULL;
	mVertexBuffer[1] = NULL;

	if (mpFont)
		mpFont->Release();
}

} // namespace KEP
