///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdio.h"
#include "d3dx9.h"
#include "d3dx9math.h"
#include "d3d9types.h"

#include <string>
#include <xstring>
#include <map>

#include "Core/Math/KEPMath.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

static __inline int IntFromFloat(float x) {
	short savemode;

	short workmode;
	int res;
	_asm
	{
        fnstcw    savemode      ; get fpu mode
        fld dword ptr[x]        ; load rwreal x
        mov       ax,savemode   ; put fpu mode in register
        or        ah,0ch        ; or-in truncate mode
        mov       workmode,ax   ; make ready to set fpu mode
        fldcw     workmode      ; set fpu to truncate mode
        fistp     dword ptr[res]; store the integer result
        fldcw     savemode      ; restore fpu mode
	}
	return res;
}

inline float LERP(float b, float e, float T, float dT) {
	if (T == dT)
		return e;

	//	Obtain fractional component
	float f = (dT / T);
	int w = ((int)f);
	f = f - w;

	return b + (f * (e - b));
}

inline float NLERP(float b, float e, float T, float dT) {
	if (T == dT)
		return e;

	//	Obtain fractional component
	float f = (dT / T);
	int w = ((int)f);
	f = f - w;

	return e - (f * (e - b));
}

inline void SHOW_INT(std::string debugStr, int Value) {
	char showIntBuffer[100];

	debugStr += ":%d\n";

	_snprintf_s(showIntBuffer, _countof(showIntBuffer), _TRUNCATE, debugStr.c_str(), Value);
	OutputDebugStringA(showIntBuffer);
}

void SHOW_FLOATS(int numFloats, ...);

inline float OSSCILATING_LERP(float b, float e, float T, float dT) {
	//	Obtain fractional component
	float f = (dT / T);
	int w = (IntFromFloat(f));
	f = f - w;

	if ((w % 2) == 0)
		return b + (f * (e - b));
	else
		return e - (f * (e - b));
}

struct QUAD_VERTEX {
	Vector3f mPos;
	DWORD mColor;
	D3DXVECTOR2 mUV;
};

struct QUAD_POINT {
	float x;
	float y;

	QUAD_POINT(float ix, float iy) {
		x = ix;
		y = iy;
	}
	QUAD_POINT() {
		x = 0;
		y = 0;
	}
};

struct QUAD_UV {
	float u;
	float v;

	QUAD_UV(float iu, float iv) {
		u = iu;
		v = iv;
	}
	QUAD_UV() {
		u = 0.0f;
		v = 0.0f;
	}
};

struct TexData {
	IDirect3DTexture9 *pTex;
	char texName[20];
	int width;
	int height;

	TexData() { pTex = NULL; }
};

struct RenderBlock {
	int offsetIntoBuffer;
	int sizeOfRenderBlock;
	int numPrimatives;

	D3DTEXTUREFILTERTYPE fType;

	DWORD textureHash;
	TexData *pTexture;
};

struct TextBlock {
	//std::string textToDraw;
	std::wstring textToDrawW;
	RECT rc;
	D3DXCOLOR color;
};

typedef std::map<DWORD, TexData *> HashTexMap;
typedef std::pair<DWORD, TexData *> HashTexPair;

class ReDynamicGeom {
	IDirect3DVertexBuffer9 *mVertexBuffer[2];
	bool mWhichBuffer;
	D3DXMATRIX mOrthoMatrix;

	TimeSec m_frameTimeSec;

	D3DXMATRIX mIdent;
	//static ReDynamicGeom *		mInstance;

	//	This map will hold all textures accessed through this interface
	HashTexMap mTextureMap;

	//	Fonts
	ID3DXFont *mpFont;

	std::string mAppDir;
	LPDIRECT3DDEVICE9 mpDevice;

	//	RenderBlock Collection
	RenderBlock mRenderBlocks[512];
	TextBlock mTextBlocks[512];
	int mBlockIndex;
	int mTextIndex;

	DWORD mViewPortWidth;
	DWORD mViewPortHeight;

public:
	ReDynamicGeom() {
		mWhichBuffer = false;
		mBlockIndex = -1;
		mViewPortWidth = 0;
		mViewPortHeight = 0;
		mTextIndex = -1;
		mpDevice = 0;
		memset(&mVertexBuffer, 0, sizeof(mVertexBuffer));
	}

	void Init(RECT viewportRect, LPDIRECT3DDEVICE9 m_pD3DDevice, const std::string &appDir);
	void ReSize(DWORD viewPortWidth, DWORD viewPortHeight);

	void DrawDynamicGeom();

	void OnReset();
	void OnLostDevice();
	void DrawText(const char *textToDraw, int xPos, int yPos, D3DXCOLOR color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	TexData *ReDynamicGeom::GetSetTexture(DWORD nameHash, std::string *textureName = NULL);
	void GetViewportSize(DWORD &viewPortWidth, DWORD &viewPortHeight) {
		viewPortWidth = mViewPortWidth;
		viewPortHeight = mViewPortHeight;
	}

	void FreeAll();

	//////////////////////////////////////////////////////////////////////////////////
	//	DrawQuad
	//
	//	This function takes in two points (bottom left, top right)
	//	and a color, drawing a 2D quad with 0,0 starting at the bottom left of the screen
	//////////////////////////////////////////////////////////////////////////////////
	void DrawQuad(QUAD_POINT BottomLeft, QUAD_POINT TopRight, DWORD color);

	//////////////////////////////////////////////////////////////////////////////////
	//	DrawQuad
	//
	//	This function takes in two points (bottom left, top right) plus a texture string.
	//	Texture notes:
	//		- The texture MUST be inside /MapsModels
	//		- The texture MUST be a DDS
	//		- Do not give the full path or file extension, just the name of the texture
	//		- Try to batch DrawQuad calls together which use the same texture - if the same
	//		  texture is used in a consecutive DrawQuad call, rendering is not done until
	//		  the texture changes or until the end of the frame
	//
	//	Two additional parms are the top left and bottom right UV coords. The syntax is
	//	given in DX style, where
	//
	//			0,0 *------------* 1,0
	//				|			 |
	//				|			 |
	//				|			 |
	//				|			 |
	//				|			 |
	//			0,1 *------------* 1,1
	//
	//	The top left coord defaults to 0,0, and the bottom right defaults to 1,1.
	//
	//	The positions are given in a system where 0,0 is bottom left of the screen
	//////////////////////////////////////////////////////////////////////////////////
	void DrawQuad(QUAD_POINT BottomLeft, QUAD_POINT TopRight, std::string &texture, D3DTEXTUREFILTERTYPE filterType = D3DTEXF_LINEAR,
		QUAD_UV BottomLeftUV = QUAD_UV(0.0f, 1.0f), QUAD_UV TopRightUV = QUAD_UV(1.0f, 0.0f));

	//////////////////////////////////////////////////////////////////////////////////
	//	DrawQuad
	//
	//	This function takes in two points (bottom left, top right) plus a texture string.
	//	Texture notes:
	//		- The texture MUST be inside /MapsModels
	//		- The texture MUST be a DDS
	//		- Do not give the full path or file extension, just the name of the texture
	//		- Try to batch DrawQuad calls together which use the same texture - if the same
	//		  texture is used in a consecutive DrawQuad call, rendering is not done until
	//		  the texture changes or until the end of the frame
	//
	//	Two additional parms are the top left and bottom right UV coords. The syntax is
	//	given in DX style, where
	//
	//			0,0 *------------* 1,0
	//				|			 |
	//				|			 |
	//				|			 |
	//				|			 |
	//				|			 |
	//			0,1 *------------* 1,1
	//
	//	The top left coord defaults to 0,0, and the bottom right defaults to 1,1.
	//
	//	The positions are given in a system where 0,0 is bottom left of the screen
	//////////////////////////////////////////////////////////////////////////////////
	void ReDynamicGeom::DrawQuadEx(QUAD_POINT BottomLeft, QUAD_POINT TopRight, std::string &textureName, DWORD color,
		float scalePct, float scaleCur, float scaleMax, D3DTEXTUREFILTERTYPE filterType = D3DTEXF_LINEAR,
		QUAD_UV BottomLeftUV = QUAD_UV(0.0f, 1.0f), QUAD_UV TopRightUV = QUAD_UV(1.0f, 0.0f));

	//////////////////////////////////////////////////////////////////////////////////
	//	DrawQuad
	//
	//	This function takes in two points (bottom left, top right) plus a texture string,
	//	Plus a diffuse color
	//
	//	Texture notes:
	//		- The texture MUST be inside /MapsModels
	//		- The texture MUST be a DDS
	//		- Do not give the full path or file extension, just the name of the texture
	//		- Try to batch DrawQuad calls together which use the same texture - if the same
	//		  texture is used in a consecutive DrawQuad call, rendering is not done until
	//		  the texture changes or until the end of the frame
	//
	//	Two additional parms are the top left and bottom right UV coords. The syntax is
	//	given in DX style, where
	//
	//			0,0 *------------* 1,0
	//				|			 |
	//				|			 |
	//				|			 |
	//				|			 |
	//				|			 |
	//			0,1 *------------* 1,1
	//
	//	The top left coord defaults to 0,0, and the bottom right defaults to 1,1.
	//
	//	The positions are given in a system where 0,0 is bottom left of the screen
	//////////////////////////////////////////////////////////////////////////////////
	void DrawQuad(QUAD_POINT BottomLeft, QUAD_POINT TopRight, DWORD color, std::string &texture, D3DTEXTUREFILTERTYPE filterType = D3DTEXF_LINEAR,
		QUAD_UV BottomLeftUV = QUAD_UV(0.0f, 1.0f), QUAD_UV TopRightUV = QUAD_UV(1.0f, 0.0f));

	~ReDynamicGeom() {}
};

} // namespace KEP
