///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Common/include/config.h"
#include "Core/Util/Memory.h"

#include "ReMaterial.h"
#include "ReD3DX9.h"
#include "emmintrin.h"

namespace KEP {

class ReD3DX9SkinMesh;
struct ReMeshData;
struct ReSkinMeshData;

typedef struct _SSEVertexData {
	__m128* pX;
	__m128* pY;
	__m128* pZ;
	__m128* pNX;
	__m128* pNY;
	__m128* pNZ;
} SSEVertexData;

typedef struct _SSEMatrixData {
	__m128 M[3];
} SSEMatrixData;

class ReShader_SkinningSSE : public ReShader {
public:
	ReShader_SkinningSSE();
	ReShader_SkinningSSE(ReD3DX9SkinMesh* mesh);
	ReShader_SkinningSSE(const ReShader_SkinningSSE& shader, ReD3DX9SkinMesh* mesh);
	virtual ~ReShader_SkinningSSE();

	virtual void PreRender() { ; }
	virtual void Render() { ; }
	virtual void PostRender() { ; }
	virtual void SetTechnique() { ; }
	virtual BOOL Update(const ReShaderData* desc = NULL);
	virtual void Invalidate() { ; }
	virtual BOOL EqualTo(const ReShader* /*shader*/) const { return TRUE; }
	virtual INT Compare(const ReShader* /*shader*/) const { return 0; }
	//virtual UINT64				Hash() const							{return ReD3DX9DEVICEINVALID;}
	virtual BOOL Invalid() const { return FALSE; }
	virtual ReShader* Clone(ReMesh* mesh = NULL) {
		Restore();
		return new ReShader_SkinningSSE(*this, (ReD3DX9SkinMesh*)mesh);
	}
	virtual ReShaderType ShaderType() const { return ReShaderType::CPU_SSE; }
	virtual void Begin(UINT& passes) { passes = 1; }
	virtual void BeginPass(UINT /*pass*/) const { ; }
	virtual void EndPass() const { ; }
	virtual void End() const { ; }

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	virtual void InitInternalData();
	virtual BOOL Restore() {
		if (m_pSSEMD == NULL)
			InitInternalData();
		return TRUE;
	}

private:
	int m_batchedVertices;
	int m_remainingVertices;
	SSEVertexData m_SSEVD;
	SSEMatrixData* m_pSSEMD;
	ReD3DX9SkinMesh* m_mesh;
	std::unique_ptr<__m128[], AlignedDeleter<__m128>> m_pSSEData;
	size_t m_SSEDataSize;

	void UpdateMatrices();
	void UpdateSkin1Weight();
	void UpdateSkin2Weight();
	void UpdateSkin3Weight();
	void UpdateSkin4Weight();

	void UpdateSkin1WeightSingleTex();
	void UpdateSkin2WeightSingleTex();
	void UpdateSkin3WeightSingleTex();
	void UpdateSkin4WeightSingleTex();

	void UpdateSkin1WeightMultiTex0();
	void UpdateSkin2WeightMultiTex0();
	void UpdateSkin3WeightMultiTex0();
	void UpdateSkin4WeightMultiTex0();

	void UpdateSkin1WeightMultiTex1();
	void UpdateSkin2WeightMultiTex1();
	void UpdateSkin3WeightMultiTex1();
	void UpdateSkin4WeightMultiTex1();

	void UpdateSkin1WeightMultiTex2();
	void UpdateSkin2WeightMultiTex2();
	void UpdateSkin3WeightMultiTex2();
	void UpdateSkin4WeightMultiTex2();

	void LoadFourFloats(const float& a0, const float& a1, const float& a2, const float& a3, __m128* dev);

	void StoreFourFloats(float* a0, float* a1, float* a2, float* a3, __m128 src);

	void Reshuffle(__m128* pM1, __m128* pM2, __m128* pM3, __m128* pM4, __m128* pR1,
		__m128* pR2, __m128* pR3, __m128* pR4);

	void Load1Mat(float* pM1, __m128* pR);

	void Collapse1Mat(float* pM1, float W1, __m128* pR);
	void Collapse2Mat(BYTE MI1, BYTE MI2, float W1, float W2, __m128* pR);
	void Collapse3Mat(BYTE MI1, BYTE MI2, BYTE MI3, float W1, float W2, float W3,
		__m128* pR);
	void Collapse4Mat(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4,
		float W1, float W2, float W3, float W4, __m128* pR);

	void Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4, __m128* pX, __m128* pY,
		__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex0_D3DX9* pOV);
	void Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4, __m128* pX, __m128* pY,
		__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex3_D3DX9* pOV);
	void Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4, __m128* pX, __m128* pY,
		__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex4_D3DX9* pOV);
	void Vert4Mat4(BYTE MI1, BYTE MI2, BYTE MI3, BYTE MI4, __m128* pX, __m128* pY,
		__m128* pZ, __m128* pNX, __m128* pNY, __m128* pNZ, ReOutputVertex5_D3DX9* pOV);
	void Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY, __m128* pZ, __m128* pNX,
		__m128* pNY, __m128* pNZ, ReOutputVertex0_D3DX9* pOV);
	void Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY, __m128* pZ, __m128* pNX,
		__m128* pNY, __m128* pNZ, ReOutputVertex3_D3DX9* pOV);
	void Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY, __m128* pZ, __m128* pNX,
		__m128* pNY, __m128* pNZ, ReOutputVertex4_D3DX9* pOV);
	void Vert4Mat4Normalise(__m128* pM, __m128* pX, __m128* pY, __m128* pZ, __m128* pNX,
		__m128* pNY, __m128* pNZ, ReOutputVertex5_D3DX9* pOV);
};

} // namespace KEP
