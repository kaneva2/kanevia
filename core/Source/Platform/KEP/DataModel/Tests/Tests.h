///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Util/SQLStatementBuilder.h"
#include "KEP/DataModel/DataModel.h"

class SQLInsert : public SQLStatement {
	std::string _into;
	std::vector<std::string> _columns;
	std::vector<std::string> _values;

public:
	SQLInsert() :
			_into("")

	{}

	SQLInsert(const std::string& fullyQualifiedTableName) :
			_into(fullyQualifiedTableName) {}

	SQLInsert& into(const std::string& fullyQualifiedTableName) {
		_into = fullyQualifiedTableName;
		return *this;
	}

	SQLInsert& value(const std::string& name, const std::string& value) {
		_values.push_back(value);
		_columns.push_back(name);
		return *this;
	}

	SQLInsert& values(const std::vector<std::vector<std::string>>& keyvalpairs) {
		for (auto v : keyvalpairs) {
			value(v[0], v[1]);
		}
		return *this;
	}

	std::string str() const {
		if (_into.length() == 0)
			throw ChainedException(SOURCELOCATION, ErrorSpec(-1) << "No target table defined!");
		std::stringstream ss;
		ss << "insert into " << _into << "("
		   << KEP::StringHelpers::implode(_columns)
		   << ") values ("
		   << KEP::StringHelpers::implode(_values)
		   << ")";
		return ss.str();
	}
};

// The tests implemented here exercise the datamodel, which requires access to a
// database.
//
// 1) Local running instance of MySqld.
// 2) The existence of a user named test identified by test with all privs on test.*.
//    Note that the "test" schema is created by these tests and if successful, are
//    dropped by these these tests.  To insure easy reentry, this schema will be dropped
//    as needed, e.g. at the start a test.
// 3) Not all tests can be written against the test database as their original queries
//    may explicitly reference the table's schema.  Need to take special precaution to insure
//    the test executable cannot be executed against a live database.  Consider that the
//    text executable
//    may be present on the server (hopefully not with client as it reveals way too much
//    about our architecture...it's not)
//    For right now write the tests and address this issue Ben and Zerby, perhaps even
//    other platform developers.
//
// In any case in the process of tearing down test data, these tests will heavily exercise
// our SQLConnectionPool/SQLConnection mods.
//

// Also, there is is the issue statements like this...for the purpose of schema set up.
// However, it simply will not do to keep this definition here.  For best practices,
// this should reside one place that reflects the current state of that environments
// schema definition.  To this end, I recommend Ben, Zerby and I define a method whereby,
// after a schema promotion, a reference image is created.  This reference is readonly as
// it only exists for the purpose of acquiring statements like this.   Tables in these
// reference schema, may or may not contain data.  For example, we would definitely want
// control/codelist tables to contain data.
//
// A reference would exist for each environment.  This environment would be communicated to the test
// executable via a parameter.  Note that absence of this parameter would be interpreted to
// mean "do not run environment based tests (like these).
//
// For the sake of having something demonstrable for Monday, simply put it here for now.
//
// Okay, so here is the plan.  Configuration still needs some help to pick the environment and
// hence the reference instance, but the plan is to simply get the create statements from the
// master db for each environment.
//
class SchemaReference {
	//	std::map<	std::string
	//		, std::string> tableDefs;
public:
	std::string getSPDef(const std::string fullyQualifiedRoutineName) {
		std::string ret;
		// So this gets rewritten
		//
		// return tableDefs[fullyQualifiedTableName];
		std::shared_ptr<SQLExecutor> executor = _connection.getSQLExecutor();
		executor->execSQLThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << "show create procedure %0";
			query.parse();
			mysqlpp::StoreQueryResult res = query.store(fullyQualifiedRoutineName);
			ASSERT_EQ(1, res.num_rows());
			mysqlpp::Row r = res[0];
			ret = std::string(r[2]);
		});

		std::vector<std::string> parts;
		StringHelpers::explode(parts, ret, " ");
		parts[1] = "";
		ret = StringHelpers::implode(parts, " ");
		return ret;

		//// Got get rid of the freakin definer "CREATE DEFINER=`admin`@`%` PROCEDURE `get_player_server_ids`( IN __player_list text )\nBEGIN\n\t-- called from dispatcher server only to get a list of servers \n\t-- based on player_ids\n\t-- this used to hi...
		//std::stringstream ss;
		//ss << "create " << ret.find("PROCEDURE ")
		//ret.replace()
		//return ret;
	}

	std::string getTableDef(const std::string& fullyQualifiedTableName) {
		std::string ret;
		// So this gets rewritten
		//
		// return tableDefs[fullyQualifiedTableName];
		std::shared_ptr<SQLExecutor> executor = _connection.getSQLExecutor();
		executor->execSQLThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << "show create table %0";
			query.parse();
			mysqlpp::StoreQueryResult res = query.store(fullyQualifiedTableName);
			ASSERT_EQ(1, res.num_rows());

			mysqlpp::Row r = res[0];
			ret = std::string(r[1]);
		});
		return ret;
	}

	// As does this as it needs to connect to our reference
	SchemaReference(const std::string& user, const std::string& password, const std::string& host, unsigned short port = 3306, const std::string& schema = "") :
			_connection(schema, host, user, password, port) {}

private:
	SQLConnection _connection;
};

class DataModelTest : public ::testing::Test {
public:
	DataModelTest() :
			allowed(true), _reference("testuser", "testpass", "localhost"), conn() {}

	// Verifies that the subject schema is not defined.  If it is
	// flag is set to disable all further operations and an exception
	// is thrown.
	//
	void assertSchemaUndefined(const std::string& subjectSchema) {
		if (!allowed)
			throw ChainedException(SOURCELOCATION, ErrorSpec(-1) << "This operation has been disallowed[assertSchemaUndefined]");

		std::stringstream ss;
		ss << "SELECT *"
			  "FROM information_schema.schemata "
			  "WHERE schema_name = '%0'";
		mysqlpp::StoreQueryResult res = _executor->execSelectThrow(ss.str(), subjectSchema);
		allowed = res.num_rows() == 0;

		if (!allowed)
			throw ChainedException(SOURCELOCATION, ErrorSpec(-1) << "Schema testing has been disabled due to presence of schema [" << subjectSchema << "] Drop manually!");
	}

	void assertSchemaDefined(const std::string& subjectSchema) {
		if (!allowed)
			throw ChainedException(SOURCELOCATION, ErrorSpec(-1) << "This operation has been disallowed[assertSchemaDefined]");

		std::stringstream ss;
		ss << "SELECT *"
			  "FROM information_schema.schemata "
			  "WHERE schema_name = '%0'";

		mysqlpp::StoreQueryResult res = _executor->execSelectThrow(ss.str(), subjectSchema);
		allowed = res.num_rows() == 1;

		if (!allowed)
			throw ChainedException(SOURCELOCATION, ErrorSpec(-1) << "Schema testing has been disabled due to presence of schema [" << subjectSchema << "]");
	}

	void createVolatileSchema(const std::string& schema) {
		_executor->execUpdateThrow("create schema %0", schema);
		_tearDowns.push_back(schema);
	}

	void createTable(const std::string schema, const std::string& tableName) {
		std::string key = schema + "." + tableName;
		_executor->execUpdateThrow("use %0", schema);
		_executor->execUpdateThrow(_reference.getTableDef(key));
	}

	void createSP(const std::string schema, const std::string& tableName) {
		std::string key = schema + "." + tableName;
		_executor->execUpdateThrow("use %0", schema);
		_executor->execUpdateThrow(_reference.getSPDef(key));
	}

	void SetUp() {
		try {
			if (!conn.connect("", "localhost", "test", "test"))
				FAIL() << "Failed to connection to database test on local host using test/test";

			_executor = conn.getSQLExecutor();

			// Okay, the pattern is this (and this will likely get pushed down the hierarchy
			// simply assert
			//
			// First off assert that this is a testing mysql instance by checking for the
			// existence of the "test_control" schema.
			//
			assertSchemaDefined("test_control");

			// Insure that we are clear to create the wok schema
			// If it's already defined in the database, then
			// it's a no go.  This code WILL NOT drop schemas that
			// it did not created.  And if it created them, it would have been
			// dropped by the cleanup.  Insure that a bio-unit be the
			// one to assess and drop.
			//
			assertSchemaUndefined("wok");
			assertSchemaUndefined("kaneva");
			assertSchemaUndefined("developer");

			if (!allowed)
				FAIL() << "Testing not allowed on this mysql instance!";

		} catch (const std::exception& e) {
			const char* pc = e.what();
			FAIL() << "Exception raised while preparing test [" << pc << "]";
		}
	}

	void TearDown() {
		// drop database wok?  Some discussion on this would be a good thing.
		// We only teardown if flag is set that allows  teardown?  Then, instead of "dropping"
		// schema in setup, we only create the schema if it doesn't already exist, in which
		// case we'll set the flag
		for (std::string& schema : _tearDowns) {
			_executor->execUpdateThrow("drop schema %0", schema);
		}
	}

	SchemaReference _reference;
	bool allowed = false;
	SQLConnection conn;
	std::shared_ptr<SQLExecutor> _executor;
	std::vector<std::string> _tearDowns;
};

TEST_F(DataModelTest, BROKEN_SoundCustomization01) {
	createVolatileSchema("wok");
	createTable("wok", "sound_customizations");

	try {
		// For now let's just test the setup and teardown.
		DataModel dm("localhost", 3306, "test", "test");
		Sound s;
		s.ObjectPlacementId = 1;
		s.Pitch = 2;
		s.Gain = 3;
		s.MaxDistance = 4;
		s.RollOff = 5;
		s.loop = 1;
		s.loopDelay = 6;
		s.DirX = 7;
		s.DirY = 8;
		s.DirZ = 9;
		s.ConeOuterGain = 10;
		s.ConeInnerAngle = 11;
		s.ConeOuterAngle = 12;
		dm.updateSound(1, 1, s);
		std::vector<std::shared_ptr<Sound>> sounds = dm.getSoundsFor(1, 1);
		ASSERT_EQ(1, sounds.size());
		ASSERT_TRUE(s == *(sounds[0].get()));
	} catch (const std::exception& e) {
		FAIL() << e.what();
	}
}

TEST_F(DataModelTest, BROKEN_Dispatcher01) {
	try {
		_reference.getTableDef("wok.players");
		// We're about to call a stored procedure.
		// So we need to create the tables the procedure
		// references...and the stored procedure
		//
		createVolatileSchema("wok");
		createTable("wok", "players");
		createVolatileSchema("developer");
		createTable("developer", "active_logins");
		createSP("wok", "get_player_server_ids");
		// xecutor& executor = conn.getSQLExecutor();
		_executor->execUpdateThrow(SQLInsert().into("developer.active_logins").value("user_id", "5295053").value("server_id", "8820").value("game_id", "5316").value("created_date", "now()").value("ip_address", "'10.1.201.79'"))
			.execUpdateThrow(SQLInsert().into("developer.active_logins").value("user_id", "5143195").value("server_id", "517700").value("game_id", "3296").value("created_date", "now()").value("ip_address", "'10.1.201.69'"))
			.execUpdateThrow(SQLInsert().into("wok.players").values({ { "player_id", "1094703" }, { "user_id", "1074370" }, { "name", "'groovyd'" }, { "title", "'Legendary Builder'" }, { "spawn_config_index", "109" }, { "original_EDB", "15" }, { "team_id", "0" }, { "race_id", "0" }, { "last_update_time", "now()" }, { "in_game", "'T'" }, { "server_id", "8820" }, { "kaneva_user_id", "5295053" }, { "created_date", "now()" }, { "is_active", "'Y'" } }))
			.execUpdateThrow(SQLInsert().into("wok.players").values({ { "player_id", "1053374" }, { "user_id", "1033038" }, { "name", "'BillyA'" }, { "title", "'Famous'" }, { "spawn_config_index", "109" }, { "original_EDB", "14" }, { "team_id", "2" }, { "race_id", "0" }, { "last_update_time", "now()" }, { "in_game", "'T'" }, { "server_id", "517700" }, { "kaneva_user_id", "5143195" }, { "created_date", "now()" }, { "is_active", "'Y'" } }));

		// For now let's just test the setup and teardown.
		DataModel dm("localhost", 3306, "test", "test");
		//		std::vector< unsigned long > serverIds;
		//		std::map<unsigned long, unsigned long> v = { { 1053374, 0 }
		//													 ,{ 1094703, 0 }
		//		                                             ,{ 1,0}};

		//		dm.GetPlayerServers(v);
		PlayersByServerT map;
		dm.GetPlayerServers(std::vector<unsigned long>({ 1053374, 1094703, 1 }), map);
		ASSERT_EQ(2, map.size());
		ASSERT_EQ(1, map[8820].size());
		ASSERT_EQ(1094703, map[8820][0]);
		ASSERT_EQ(1, map[517700].size());
		ASSERT_EQ(1053374, map[517700][0]);

		//ASSERT_NE(0, v[1053374]);
		//ASSERT_NE(0, v[1094703]);
		//ASSERT_EQ(0, v[1]);
	} catch (const std::exception& e) {
		FAIL() << e.what();
	}
}
