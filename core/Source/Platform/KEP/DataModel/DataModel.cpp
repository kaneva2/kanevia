///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// DataModel.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <map>
#include "KEP/DataModel/DataModel.h"
#include "Core/Util/LoggingTimer.h"

using namespace std;
using namespace mysqlpp;

namespace KEP {

DataModel::DataModel(const std::string& host, unsigned int port, const std::string& user, const std::string& pwd, const std::string& schema) :
		//_factory(new SQLConnectionFactory(host, port, user, pwd, schema))
		_connectionPool(new SQLConnectionPool(std::make_shared<SQLConnectionFactory>(host, port, user, pwd, schema))) {}

std::vector<std::shared_ptr<Sound>>
DataModel::getSoundsFor(unsigned long zoneIndex, unsigned long instanceId) {
	std::vector<std::shared_ptr<Sound>> ret;
	LoggingTimer lgt(log4cplus::Logger::getInstance(L"Timing"), "DB:getSoundsXMLAsString");
	std::stringstream ss;
	ss << "SELECT "
		  "obj_placement_id, pitch, gain, max_distance"
		  ", roll_off, `loop`, loop_delay"
		  ", dir_x, dir_y, dir_z"
		  ", cone_outer_gain, cone_inner_angle, cone_outer_angle "
		  "FROM "
		  "wok.sound_customizations "
		  "WHERE zone_index=%0 AND instance_id=%1";
	std::shared_ptr<SQLConnection> m_conn = _connectionPool->borrow();
	mysqlpp::StoreQueryResult res = m_conn->getSQLExecutor(__FUNCTION__, "SQLTests")
										->execSelectThrow(ss.str(), (int)zoneIndex, (int)instanceId);

	// Which leaves us with doing in the model, exactly what the model was inteded to do...
	// serve up generic objects to the caller,  removing data access logic from the client
	// logic.
	//
	for (int i = 0; i < res.size(); i++) {
		mysqlpp::Row r = res[i];
		std::shared_ptr<Sound> sound = std::make_shared<Sound>();
		sound->ObjectPlacementId = atol(r[0]);
		sound->Pitch = atol(r[1]);
		sound->Gain = atol(r[2]);
		sound->MaxDistance = atol(r[3]);
		sound->RollOff = atol(r[4]);
		sound->loop = r[5];
		sound->loopDelay = atol(r[6]);
		sound->DirX = atol(r[7]);
		sound->DirY = atol(r[8]);
		sound->DirZ = atol(r[9]);
		sound->ConeOuterGain = atol(r[10]);
		sound->ConeInnerAngle = atol(r[11]);
		sound->ConeOuterAngle = atol(r[12]);
		ret.push_back(sound);
	}
	return ret;
}

DataModel&
DataModel::updateSound(unsigned long zoneIndex, unsigned long instanceId, const Sound& sound) {
	std::vector<std::shared_ptr<Sound>> ret;
	LoggingTimer lgt(log4cplus::Logger::getInstance(L"Timing"), "DB:getSoundsXMLAsString");
	std::shared_ptr<SQLConnection> m_conn = _connectionPool->borrow();
	std::shared_ptr<SQLExecutor> executor = m_conn->getSQLExecutor(__FUNCTION__, "SQLTests");
	executor->execUpdateThrow(
		"INSERT INTO wok.sound_customizations(obj_placement_id, zone_index, instance_id, pitch, gain, max_distance, roll_off, "
		"`loop`, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle)"
		"VALUES (%0, %1, %2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14)"
		"ON DUPLICATE KEY UPDATE obj_placement_id=%0, zone_index=%1, instance_id=%2, pitch=%3, gain=%4, max_distance=%5, roll_off=%6, "
		"`loop`=%7, loop_delay=%8, dir_x=%9, dir_y=%10, dir_z=%11, cone_outer_gain=%12, cone_inner_angle=%13, cone_outer_angle=%14",
		(int)sound.ObjectPlacementId // And a sound object has linkage.  Is this appropriate?
		,
		(int)zoneIndex // to be resolved.  closer to being external key.
		,
		(int)instanceId, (int)sound.Pitch, (int)sound.Gain, (int)sound.MaxDistance, (int)sound.RollOff, (int)sound.loop, (int)sound.loopDelay, (int)sound.DirX, (int)sound.DirY, (int)sound.DirZ, (int)sound.ConeOuterGain, (int)sound.ConeInnerAngle, (int)sound.ConeOuterAngle);

	return *this;
}

typedef std::map<unsigned long, unsigned long> mapT;

PlayersByServerT&
DataModel::GetPlayerServers(const vec_player_ids_t& playerIds, PlayersByServerT& playerServerMap) {
	std::shared_ptr<SQLConnection> m_conn = _connectionPool->borrow();

	std::stringstream inclause;
	string delim("");
	for (auto i : playerIds) {
		inclause << delim << i;
		delim = ",";
	}
	std::shared_ptr<SQLExecutor> executor = m_conn->getSQLExecutor();
	//executor.execSQLThrow( "SELECT p.player_id, al.server_id "
	//	 "FROM wok.players p "
	//		"inner join developer.active_logins al on p.kaneva_user_id = al.user_id "
	//	"WHERE p.player_id IN(%0) "
	//	 "order by 2" );

	executor->execSQLThrow([&](SQLConnection& conn) {
		// client code is expecting this to be in server order so that it can
		// effectively bundle
		//
		Query query = m_conn->query();
		query << "SELECT p.player_id, al.server_id "
			  << "FROM wok.players p "
			  << "inner join developer.active_logins al on p.kaneva_user_id = al.user_id "
			  << "WHERE p.player_id IN(%0) "
			  << "order by 2";
		query.parse();
		StoreQueryResult res = query.store(inclause.str());

		unsigned long csid = 0;
		vector<unsigned long>* pPlayerVec = 0;
		for (ULONG i = 0; i < res.num_rows(); i++) {
			unsigned long sid = res.at(i).at(1);
			if (sid != csid) {
				playerServerMap.insert_or_assign(sid, vector<unsigned long>());
				pPlayerVec = &(playerServerMap[sid]);
				csid = sid;
			}
			pPlayerVec->push_back(res.at(i).at(0));
		}
	});
	return playerServerMap;
}

} // namespace KEP