///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

_dowork
#include "stdafx.h"
#include <log4cplus/logger.h>
#include "KEP/Plugins/LocationRegistryReporter.h"
#include "Common/KEPUtil/LocationRegistry.h"

using namespace log4cplus;

namespace KEP {

/**
* An implementation of LocationRegistryReporter that reports to a log file
*/
class __declspec(dllexport) LoggingLocationRegistryReporter : public LocationRegistryReporter {
public:
	/**
	* Construct with the name of a log4cplus logger
	*
	* @param	logName			The name of the logger to be used when reporting to the log
	*/
	LoggingLocationRegistryReporter(const std::string& logName) :
			_logger(Logger::getInstance(Utf8ToUtf16(logName))) // I'm a logging reporter.  instantiate my logger.
			,
			LocationRegistryReporter() {}

	/**
	* Create an LocationRegistryReporter from an existing heartbeat generator
	* Creates completely new generator with the same interval as the other
	* LocationRegistryReporter.
	*/
	LoggingLocationRegistryReporter(const LoggingLocationRegistryReporter& other) :
			_logger(other._logger) {}

	/**
	* dtor
	*/
	virtual ~LoggingLocationRegistryReporter() {}

	/**
	* The business end of this plugin, queries the LocationRegistry and reports.
	* In this case to a log file.
	*/
	LoggingLocationRegistryReporter& report() {
		// todo: implement me.
		LocationRegistry& reg = LocationRegistry::singleton();
		std::vector<LocationRegistry::Entry> entries;
		reg.entries(entries);
		std::stringstream ss;
		LOG4CPLUS_DEBUG(_logger, "BeginSample");
		for (auto iter = entries.cbegin(); iter != entries.cend(); iter++) {
			auto netId = iter->netId();
			auto coords = iter->coords();
			//			auto zone = iter->zoneIndex();
			ss << netId
			   << "," << iter->zoneInstanceId()
			   << "," << iter->zoneChannelId()
			   << "," << iter->zoneType()
			   << "," << coords.first.X()
			   << "," << coords.first.Y()
			   << "," << coords.first.Z()
			   << "," << coords.second.X()
			   << "," << coords.second.Y()
			   << "," << coords.second.Z();
			LOG4CPLUS_INFO(_logger, ss.str());
			ss.str(std::string());
		}
		LOG4CPLUS_DEBUG(_logger, "EndSample");

		return *this;
	}

private:
	/**
	* Assigment operator is disabled
	*/
	LoggingLocationRegistryReporter& operator=(const LoggingLocationRegistryReporter& other) {
		return *this;
	}

	log4cplus::Logger _logger;
};

/**
* LocationRegistry which, while still supporting LocationRegisryReporter:report()
* method, also starts a background thread to report on the registry on an arbitrary
* interval.
*/
class ConcreteActiveLocationRegistryReporter
		: public ActiveLocationRegistryReporter,
		  public jsThread {
public:
	ConcreteActiveLocationRegistryReporter::ConcreteActiveLocationRegistryReporter(LocationRegistryReporter* impl, unsigned long idleIntervalSecs = 180) :
			_impl(impl), jsThread("LocationRegistryReporter", PR_LOWEST), _stopped(false), _pollingLatch(1), _intervalMillis(idleIntervalSecs * 1000), _startupACT(new ACT()), _stopACT(new ACT()) {}

	virtual ~ConcreteActiveLocationRegistryReporter() {}

	/**
	* @see jsThread
	*/
	unsigned long _doWork() {
		_startupACT->complete();
		while (!_stopped) {
			report();
			_pollingLatch.await(_intervalMillis);
		}
		_stopACT->complete();
		LOG4CPLUS_DEBUG(Logger::getInstance(L"SHUTDOWN"), "LocationRegistryReporter loop exited cleanly");
		return 0;
	}

	/**
	* @see LocationRegistryReporter
	* @return reference to self
	*/
	ConcreteActiveLocationRegistryReporter& report() {
		_impl->report();
		return *this;
	}

	/**
	* Stops the Active reporter's thread
	* @return ACTPtr   start is an asynchronous.  This ACTPtr allows the caller to synchronize should they desire
	*/
	ACTPtr start() {
		jsThread::start();
		return _startupACT;
	};

	/**
	* Stops the Active reporter's thread
	* @return ACTPtr   start is an asynchronous.  This ACTPtr allows the caller to synchronize should they desire
	*/
	ACTPtr stop() {
		_stopped = true; // Flip the flag that will allow the polling loop to exit
		_pollingLatch.countDown(); // And release the latch, thus interrupting the wait and
		return _stopACT;
	}

private:
	LocationRegistryReporter* _impl; // The implementation used to report the player locations
	// at patch poll interval

	bool _stopped; // Flag exit polling loop

	CountDownLatch _pollingLatch; // Countdown used to block for configured time interval
	// Note that this allows to block without using Sleep()
	// which cannot be interupted

	unsigned long _intervalMillis; // The amount of time in millis to block on the pollingLatch

	ACTPtr _startupACT;
	ACTPtr _stopACT;
};

// Plugin Factory Implementation
//
LocationRegistryReporterPluginFactory::LocationRegistryReporterPluginFactory() :
		AbstractPluginFactory("LocationRegistryReporter"), _config(0) {}

LocationRegistryReporterPluginFactory::~LocationRegistryReporterPluginFactory() {
	// cout << "LocationRegistryReporterPlugin::LocationRegistryReporterPlugin()" << endl;
}

LocationRegistryReporter&
LocationRegistryReporter::report(void) {
	return *this;
}

const std::string& LocationRegistryReporterPluginFactory::name() const {
	// cout << "LocationRegistryReporterPlugin::name()" << endl;
	return AbstractPluginFactory::name();
}

PluginPtr LocationRegistryReporterPluginFactory::createPlugin(const TIXMLConfig::Item& configItem) const {
	TiXmlElement* config = configItem._internal;
	LOG4CPLUS_TRACE(Logger::getInstance(L"LocationRegistry"), "LocationRegistryReporterPlugin::createPlugin()");

	// Guard clause.  If no configuration provided, we can go with
	// default settings.  We're done here.
	//
	if (config == 0)
		throw IllegalArgumentException(SOURCELOCATION);

	std::string loggerName = "LocationRegistryReporter"; // todo: s/b const var
	const TiXmlElement* pe = config->FirstChildElement("logger_name");
	if (pe != 0)
		loggerName = pe->GetText();

	std::string idleIntervalSecs = "60";
	pe = config->FirstChildElement("idle_interval_secs");
	if (pe != 0)
		idleIntervalSecs = pe->GetText();

	int intervalSecs = atoi(idleIntervalSecs.c_str());

	LoggingLocationRegistryReporter* rbase = new LoggingLocationRegistryReporter(loggerName);
	ConcreteActiveLocationRegistryReporter* ractive = new ConcreteActiveLocationRegistryReporter(rbase, intervalSecs);
	return PluginPtr(new LocationRegistryReporterPlugin(ractive));
}

// Plugin implementation
LocationRegistryReporterPlugin::LocationRegistryReporterPlugin(ActiveLocationRegistryReporter* impl) :
		_impl(impl), AbstractPlugin("LocationRegistryReporter") {}

LocationRegistryReporterPlugin::LocationRegistryReporterPlugin(const LocationRegistryReporterPlugin* other) :
		_impl(other->_impl), AbstractPlugin("LocationRegistryReporter"), LocationRegistryReporter() {}

LocationRegistryReporterPlugin&
LocationRegistryReporterPlugin::operator=(const LocationRegistryReporterPlugin& /* rhs */) {
	return *this;
}

LocationRegistryReporterPlugin::~LocationRegistryReporterPlugin() {
	delete (_impl);
}

const std::string&
LocationRegistryReporterPlugin::name() {
	return AbstractPlugin::name();
}

unsigned int
LocationRegistryReporterPlugin::version() {
	return AbstractPlugin::version();
}

Plugin&
LocationRegistryReporterPlugin::startPlugin() {
	_impl->start();
	return AbstractPlugin::startPlugin();
}

ACTPtr
LocationRegistryReporterPlugin::stopPlugin() {
	_impl->stop();
	return AbstractPlugin::stopPlugin();
}

LocationRegistryReporterPlugin&
LocationRegistryReporterPlugin::report(void) {
	_impl->report();
	return *this;
}

} // namespace KEP