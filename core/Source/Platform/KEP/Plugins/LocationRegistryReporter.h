///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <set>
#include "Core/Util/Runnable.h"
#include "Core/Util/Executor.h"
#include "Common/KEPUtil/Plugin.h"
#include "Common/include/kepconfig.h"
#include "Common/KEPUtil/CountDownLatch.h"

class TiXmlElement;

namespace KEP {

/** 
 * An interface that addresses a pattern emerging from the plugin host/asynchronous 
 * programming.  That is "asynchronous component".  Perhaps "threaded" or "daemon" 
 * is what we
 */
class Active {
public:
	Active() {}
	virtual ~Active() {}
	virtual ACTPtr start() = 0;
	virtual ACTPtr stop() = 0;
};

/**
 * Interface of a Location Registry Reporter.  
 *
 * A Location Registry Reporter is and  active component (read threaded) which,
 * on a configured interval, taks a snapshot of LocationRegistry and reports
 * that data.  Initial implementation will be to log out to a dedicated log
 * but other strategies (e.g. ship to a central aggregator on MQ, or web call).
 *
 * This abstraction exists to facilitate these different output strategies.
 */
class __declspec(dllexport) LocationRegistryReporter {
public:
	LocationRegistryReporter() {}
	virtual LocationRegistryReporter& report() = 0;
};

class _declspec(dllexport) ActiveLocationRegistryReporter : public Active, public LocationRegistryReporter {
public:
	virtual ~ActiveLocationRegistryReporter() {}
};

/**
 * The following class adapts the reporter to the Plugin API allowing us to arbitrarily load an active reporter
 * into a plugin host.
 */
class __declspec(dllexport) LocationRegistryReporterPlugin
		: public LocationRegistryReporter
		,
		  public AbstractPlugin {
public:
	/**
	 * Construct with a pointer to the ActiveLocationRegistry.
	 *
	 * @param impl		Pointer to an ConcreteActiveLocationRegistryReporter allocated on the heap.
	 *					N.B. this plugin will delete it during destruction.  Future iterations
	 *					may take a more elegant approach and employ ref pointers
	 */
	LocationRegistryReporterPlugin(ActiveLocationRegistryReporter* impl);

	/**
	 * dtor.  Note this destructor deletes any resources associated with it's
	 * internal implementation (read the implementation supplied with the constructor)
	 */
	virtual ~LocationRegistryReporterPlugin();

	/**
	 * Copy constructor
	 */
	LocationRegistryReporterPlugin(const LocationRegistryReporterPlugin* other);

	/**
	 * Assignment operator
	 */
	LocationRegistryReporterPlugin& operator=(const LocationRegistryReporterPlugin& /* rhs */);

	/**
	 * @see Plugin::name()
	 */
	const std::string& name();

	/**
	 * @see Plugin::version()
	 */
	unsigned int version();

	/**
	 * @see Plugin::startPlugin()
	 */
	Plugin& startPlugin();

	/**
	 * @see Plugin::stopPlugin()
	 */
	ACTPtr stopPlugin();

	/**
	 * @see LocationRegistryReporter::report()
	 */
	LocationRegistryReporterPlugin& report(void);

private:
	ActiveLocationRegistryReporter* _impl;
};

/**
 * Factory implementation to create our plugin. 
 * @see PluginFactory
 */
class __declspec(dllexport) LocationRegistryReporterPluginFactory : public AbstractPluginFactory {
public:
	LocationRegistryReporterPluginFactory();
	virtual ~LocationRegistryReporterPluginFactory();

	/**
	 * @see PluginFactory::name()
	 */
	const std::string& name() const;

	/**
	 * @see PluginFactory::createPlugin()
	 */
	PluginPtr createPlugin(const TIXMLConfig::Item& config) const;

private:
	KEPConfig* _config;
};

} // namespace KEP