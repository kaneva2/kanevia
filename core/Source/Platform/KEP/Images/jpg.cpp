///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KEP/Images/KEPImages.h"
#include <libjpeg-turbo/jpeglib.h>
#include <string>
#include <assert.h>
#include "d3d9types.h"

// Link matching build to guarantee only one type of CRT DLLs are used otherwise CRT might
// complain (e.g. exception while free memory allocated by a different CRT DLL)
#ifdef _DEBUG
#pragma comment(lib, "jpeg-staticd.lib")
#else
#pragma comment(lib, "jpeg-static.lib")
#endif

namespace KEP {

// Inherit std jpeg_error_mgr to support try/catch
struct my_error_mgr : public jpeg_error_mgr {
	jmp_buf setjmp_buffer; // for return to caller
};

METHODDEF(void)
my_error_exit(j_common_ptr cinfo) {
	my_error_mgr *myerr = (my_error_mgr *)cinfo->err;
	(*cinfo->err->output_message)(cinfo); // Display an error message?

	// Return control to the setjmp point
	longjmp(myerr->setjmp_buffer, 1);
}

#pragma warning(push)
#pragma warning(disable : 4611) // warning C4611: interaction between '_setjmp' and C++ object destruction is non-portable

unsigned char *decodeJPG(unsigned char *rawBuffer, unsigned rawBufSize, ImageInfo &info, unsigned &pixBufSize) {
	int outColorComponents = 0;
	J_COLOR_SPACE outColorSpace = JCS_UNKNOWN;
	switch (info.pixelFormat) {
		case D3DFMT_A8R8G8B8:
		case D3DFMT_X8R8G8B8:
			outColorComponents = 4;
			outColorSpace = JCS_EXT_BGRA; // LSB - B - G - R - A - HSB
			break;
		default:
			assert(false);
			return nullptr;
	}

	// JPEG decompression object
	jpeg_decompress_struct dinfo;
	memset(&dinfo, 0, sizeof(dinfo));

	// Set up the normal JPEG error routines, then override error_exit.
	my_error_mgr jerr;
	dinfo.err = jpeg_std_error(&jerr);
	jerr.error_exit = my_error_exit;

	// TRY: establish the setjmp return context for my_error_exit to use.
	if (setjmp(jerr.setjmp_buffer)) {
		// CATCH (jpeg err)
		jpeg_destroy_decompress(&dinfo);
		return NULL;
	}

	// Initialize for decompression
	jpeg_create_decompress(&dinfo);
	jpeg_mem_src(&dinfo, rawBuffer, (long)rawBufSize);

	// Read JPEG header
	(void)jpeg_read_header(&dinfo, TRUE);

	// Setup output transform
	dinfo.output_components = outColorComponents;
	dinfo.out_color_space = outColorSpace;

	// Step 5: Start decompressor
	(void)jpeg_start_decompress(&dinfo);

	// Read image info
	info.width = dinfo.output_width;
	info.height = dinfo.output_height;
	info.opacity = ImageOpacity::FULLY_OPAQUE;

	// JSAMPLEs per row in output buffer
	int row_stride = dinfo.output_width * dinfo.output_components;

	// Allocate buffer to hold entire image
	pixBufSize = dinfo.output_height * dinfo.output_width * 4;
	auto pixBuffer(new unsigned char[pixBufSize]);

	// Make a one-row-high sample array that will go away when done with image
	JSAMPARRAY buffer = (*dinfo.mem->alloc_sarray)((j_common_ptr)&dinfo, JPOOL_IMAGE, row_stride, 1);

	assert(dinfo.out_color_space == outColorSpace);

	// Decompress scanlines
	// Here we use the library's state variable cinfo.output_scanline as the loop counter, so that we don't have to keep track ourselves.
	while (dinfo.output_scanline < dinfo.output_height) {
		// jpeg_read_scanlines expects an array of pointers to scanlines. Here the array is only one element long,
		// but you could ask for more than one scanline at a time if that's more convenient.
		(void)jpeg_read_scanlines(&dinfo, buffer, 1);

		// Assume put_scanline_someplace wants a pointer and sample count.
		memcpy(pixBuffer + row_stride * (dinfo.output_scanline - 1), buffer[0], row_stride);
	}

	// Step 7: Finish decompression
	(void)jpeg_finish_decompress(&dinfo);

	// Step 8: Release JPEG decompression object: this is an important step since it will release a good deal of memory.
	jpeg_destroy_decompress(&dinfo);

	return pixBuffer;
}

unsigned char *encodeJPG(unsigned char *pixels, unsigned pixBufSize, const ImageInfo &info, unsigned &rawBufSize, int outQuality) {
	assert(info.width != 0 && info.height != 0);

	int inColorComponents = 0;
	J_COLOR_SPACE inColorSpace = JCS_UNKNOWN;
	switch (info.pixelFormat) {
		case D3DFMT_A8R8G8B8:
		case D3DFMT_X8R8G8B8:
			inColorComponents = 4;
			inColorSpace = JCS_EXT_BGRA; // LSB - B - G - R - A - HSB
			break;
		default:
			assert(false);
			return nullptr;
	}

	// JPEG compression object
	jpeg_compress_struct cinfo;
	memset(&cinfo, 0, sizeof(cinfo));

	// Set up the normal JPEG error routines, then override error_exit.
	my_error_mgr jerr;
	cinfo.err = jpeg_std_error(&jerr);
	jerr.error_exit = my_error_exit;

	// TRY: establish the setjmp return context for my_error_exit to use.
	if (setjmp(jerr.setjmp_buffer)) {
		// CATCH (jpeg err)
		jpeg_destroy_compress(&cinfo);
		return NULL;
	}

	// Determine output buffer size
	unsigned long outSize = 0;
	unsigned char *outBuf = nullptr;

	// Initialize for compression
	jpeg_create_compress(&cinfo);
	jpeg_mem_dest(&cinfo, &outBuf, &outSize);

	cinfo.image_width = info.width;
	cinfo.image_height = info.height;
	cinfo.input_components = inColorComponents;
	cinfo.in_color_space = inColorSpace;

	jpeg_set_defaults(&cinfo);
	cinfo.dct_method = JDCT_FLOAT;
	jpeg_set_quality(&cinfo, outQuality, TRUE); // TRUE: force baseline. According to libjpeg, this is useful when quality is below 25. It does generate better image on quality 0 when used.

	// Start compressor
	jpeg_start_compress(&cinfo, TRUE); // TRUE: "a complete JPEG interchange datastream will be written"

	// JSAMPLEs per row in output buffer
	int inStride = cinfo.image_width * cinfo.input_components;

	// Compress scanlines
	// Here we use the library's state variable cinfo.output_scanline as the loop counter, so that we don't have to keep track ourselves.
	while (cinfo.next_scanline < cinfo.image_height) {
		JSAMPROW row_pointer[4] = { nullptr };
		int numScanLines = 0;
		for (size_t i = 0; i < 4 && cinfo.next_scanline + i < cinfo.image_height; i++) {
			row_pointer[i] = pixels + (cinfo.next_scanline + i) * inStride;
			numScanLines++;
		}
		(void)jpeg_write_scanlines(&cinfo, row_pointer, numScanLines);
	}

	// Finish compression
	jpeg_finish_compress(&cinfo);

	// Final cleanup
	jpeg_destroy_compress(&cinfo);

	assert(outBuf != nullptr && outSize != 0);
	rawBufSize = outSize;

	// Reallocate the memory with new - this costs extra but would allow the pointer to be passed
	// all the way to caller without special flag regarding how to free this buffer in the end.
	auto mallocedOutBuf = outBuf;
	outBuf = new unsigned char[outSize];
	memcpy(outBuf, mallocedOutBuf, outSize);
	free(mallocedOutBuf);
	return outBuf;
}

#pragma warning(pop)

} // namespace KEP
