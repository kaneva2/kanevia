///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <assert.h>
#include <algorithm>

namespace KEP {

unsigned getAverageColor(unsigned char* pixBuf, unsigned pixBufSize, unsigned width, unsigned height) {
	assert(pixBufSize == width * height * 4);
	pixBufSize;
	unsigned numPixels = height * width;

#ifdef SSE2
	assert(numPixels < (1 << 24)); // Maximum size: 4096x4096 due to use of 32-bit sums
	__m128i zeros = _mm_setzero_si128();
	__m128i ones16 = _mm_set1_epi16(1);
	__m128i sum = zeros;
	__m128i itrlvd;
	__m128i sumOf2;

	__m128i* p128i = (__m128i*)pixBuf;

	for (size_t i = 0; i < (numPixels >> 3); i++) {
		// Load 8 pixels into two registers
		__m128i pix0to3 = _mm_loadu_si128(p128i++); // unaligned load
		__m128i pix4to7 = _mm_loadu_si128(p128i++); // unaligned load

		// Pixels #0, #1, #4, #5
		itrlvd = _mm_unpacklo_epi8(pix0to3, pix4to7);

		// Pixels #0, #1
		sumOf2 = _mm_unpacklo_epi8(itrlvd, zeros);
		sumOf2 = _mm_madd_epi16(sumOf2, ones16);
		sum = _mm_add_epi32(sum, sumOf2);

		// Pixels #4, #5
		sumOf2 = _mm_unpackhi_epi8(itrlvd, zeros);
		sumOf2 = _mm_madd_epi16(sumOf2, ones16);
		sum = _mm_add_epi32(sum, sumOf2);

		// Pixels #2, #3, #6, #7
		itrlvd = _mm_unpackhi_epi8(pix0to3, pix4to7);

		// Pixels #2, #3
		sumOf2 = _mm_unpacklo_epi8(itrlvd, zeros);
		sumOf2 = _mm_madd_epi16(sumOf2, ones16);
		sum = _mm_add_epi32(sum, sumOf2);

		// Pixels #6, #7
		sumOf2 = _mm_unpackhi_epi8(itrlvd, zeros);
		sumOf2 = _mm_madd_epi16(sumOf2, ones16);
		sum = _mm_add_epi32(sum, sumOf2);
	}

	// Color sums so far
	unsigned sumA = sum.m128i_u32[3];
	unsigned sumR = sum.m128i_u32[2];
	unsigned sumG = sum.m128i_u32[1];
	unsigned sumB = sum.m128i_u32[0];

	// Remaining <8 pixels: use x86 code
	unsigned* p = (unsigned*)p128i;
	for (size_t i = numPixels >> 3 << 3; i < numPixels; i++, p++) {
		sumA += (*p >> 24) & 0xff;
		sumR += (*p >> 16) & 0xff;
		sumG += (*p >> 8) & 0xff;
		sumB += *p & 0xff;
	}

	sumA /= numPixels;
	sumR /= numPixels;
	sumG /= numPixels;
	sumB /= numPixels;

	assert(sumA <= 0xff && sumR <= 0xff && sumG <= 0xff && sumB <= 0xff);
	sumA = std::min<unsigned>(0xff, sumA);
	sumR = std::min<unsigned>(0xff, sumR);
	sumG = std::min<unsigned>(0xff, sumG);
	sumB = std::min<unsigned>(0xff, sumB);

	return (sumA << 24) | (sumR << 16) | (sumG << 8) | sumB;
#else
	// Use 64-bit sums to handle images larger than 4096x4096
	unsigned long long sumA = 0, sumR = 0, sumG = 0, sumB = 0;
	auto p = (unsigned*)pixBuf;
	for (unsigned i = 0; i < numPixels; i++, p++) {
		sumA += (*p >> 24) & 0xff;
		sumR += (*p >> 16) & 0xff;
		sumG += (*p >> 8) & 0xff;
		sumB += *p & 0xff;
	}
	sumA /= numPixels;
	sumR /= numPixels;
	sumG /= numPixels;
	sumB /= numPixels;

	assert(sumA <= 0xff && sumR <= 0xff && sumG <= 0xff && sumB <= 0xff);
	sumA = std::min<unsigned long long>(0xff, sumA);
	sumR = std::min<unsigned long long>(0xff, sumR);
	sumG = std::min<unsigned long long>(0xff, sumG);
	sumB = std::min<unsigned long long>(0xff, sumB);

	return (unsigned)((sumA << 24) | (sumR << 16) | (sumG << 8) | sumB);
#endif
}

} // namespace KEP