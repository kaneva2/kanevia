///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KEP/Images/KEPImages.h"
#include "dds.h"
#include "color.h"

#include <nvtt/nvtt.h>
#include <string>
#include <memory>
#include <algorithm>
#include <sstream>
#include <assert.h>
#include <d3d9types.h>

#define BC3_OPACITY_THRESHOLD 0xFC
#define GENERATE_MIPMAPS false
#define DDS_ERROR(x)                            \
	{                                           \
		std::stringstream ss;                   \
		ss << x;                                \
		throw std::exception(ss.str().c_str()); \
	}

const static unsigned A8R8G8B8_R_MASK = 0x00ff0000, A8R8G8B8_G_MASK = 0x0000ff00, A8R8G8B8_B_MASK = 0x000000ff, A8R8G8B8_A_MASK = 0xff000000;

namespace KEP {

unsigned FCC_DDS = FOURCC("DDS "), FCC_DXT1 = FOURCC("DXT1"), FCC_DXT3 = FOURCC("DXT3"), FCC_DXT5 = FOURCC("DXT5");
unsigned FCC_DXT2 = FOURCC("DXT2"), FCC_DXT4 = FOURCC("DXT4"), FCC_BC4U = FOURCC("BC4U"), FCC_BC4S = FOURCC("BC4S"), FCC_BC5U = FOURCC("ATI2"), FCC_BC5S = FOURCC("BC5S");
unsigned FCC_ZERO = FOURCC((DWORD)0); // Pseudo FOURCC with all zero to indicate that it's uncompressed RGB
static auto g_requiredFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;
static auto g_requiredCaps = DDSCAPS_TEXTURE;

bool isValidFourCC(unsigned fcc) {
	return fcc == FCC_DXT1 ||
		   fcc == FCC_DXT2 || fcc == FCC_DXT3 ||
		   fcc == FCC_DXT4 || fcc == FCC_DXT5 ||
		   fcc == FCC_BC4U || fcc == FCC_BC4S ||
		   fcc == FCC_BC5U || fcc == FCC_BC5S;
}

//================================================================
// Decoding function
static size_t decodeDXTBlock(unsigned fourCC, const unsigned char *pIn, unsigned char *pOut, unsigned width, unsigned height, unsigned x0, unsigned y0, unsigned outPitch, bool &opaque, bool &translucent, bool &allZeroAlpha);

// Both in and out masks must contain up to one continuous chunk of 1-bits otherwise the results are undefined.
// makeShifts(0x000001F00, 0x0x00FF0000, true) -> (downShift=8, upShift=19)
// makeShifts(0x000001F00, 0x0x00FF0000, false) -> (downShift=8, upShift=16)
inline void makeShifts(unsigned inMask, unsigned outMask, bool leftAlign, size_t &downShifts, size_t &upShifts) {
	downShifts = upShifts = 0;
	if (inMask == 0) {
		// No shift if inMask is 0
		return;
	}
	// Calculate down shifts
	while (outMask != 0 && (inMask & 1) == 0 || outMask == 0 && inMask != 0) {
		inMask >>= 1;
		downShifts++;
	}
	// Calculate up shifts
	if (leftAlign) {
		while (outMask != 0 && (outMask | inMask) != inMask) {
			outMask >>= 1;
			upShifts++;
		}
	} else {
		while (outMask != 0 && (outMask & 1) == 0) {
			outMask >>= 1;
			upShifts++;
		}
	}
}

unsigned char *decodeDDS(const unsigned char *rawBuffer, unsigned rawBufSize, ImageInfo &info, unsigned &pixBufSize) {
	auto pDDS = (DDS_FILE *)rawBuffer;
	auto pDDSHeader = &pDDS->header;

	/////////////////////////////////////////////////////////////////////////////////////
	// 1) Pre-screening

	if (rawBufSize < sizeof(DDS_HEADER) + sizeof(unsigned)) {
		DDS_ERROR("DDS texture is invalid: containing too few bytes");
	}

	// Validate signature
	if (pDDS->sig != FCC_DDS) {
		auto possibleType = detectImageFormat(rawBuffer, rawBufSize);
		if (possibleType.empty()) {
			DDS_ERROR("Input is not a valid DDS texture.");
		} else {
			DDS_ERROR("Input is not a valid DDS texture. It appears to be in " << possibleType << " format.");
		}
	}

	// Validate header size
	if (pDDSHeader->dwSize != sizeof(DDS_HEADER)) {
		DDS_ERROR("DDS texture has unsupported header size: got " << pDDSHeader->dwSize << ", expect " << sizeof(DDS_HEADER));
	}

	// Validate required flags
	if ((pDDSHeader->dwFlags & g_requiredFlags) != g_requiredFlags) {
		// Correct malformed headers if possible
#ifndef STRICT_DDS_VALIDATION
		assert(false);
		if (pDDSHeader->dwWidth != 0)
			pDDSHeader->dwFlags |= DDSD_WIDTH;
		if (pDDSHeader->dwHeight != 0)
			pDDSHeader->dwFlags |= DDSD_HEIGHT;
		if (pDDSHeader->dwCaps != 0)
			pDDSHeader->dwFlags |= DDSD_CAPS;
		if (pDDSHeader->ddspf.dwFlags != 0)
			pDDSHeader->dwFlags |= DDSD_PIXELFORMAT;
#endif
		// Validate again
		if ((pDDSHeader->dwFlags & g_requiredFlags) != g_requiredFlags) {
			DDS_ERROR("DDS texture is missing one or more required flags");
		}
	}

	// Validate required caps
	if ((pDDSHeader->dwCaps & g_requiredCaps) != g_requiredCaps) {
		DDS_ERROR("DDS texture is missing one or more required caps");
	}

	// Handle simple texture and mipmap only. Only the full res texture will be processed if mipmap.
	if ((pDDSHeader->dwCaps & DDSCAPS_COMPLEX) && // Is complex
		(!(pDDSHeader->dwCaps & DDSCAPS_MIPMAP) || pDDSHeader->dwCaps2 != 0)) { // Not mipmap or has other complex types
		DDS_ERROR("DDS complex texture other than mipmap is not supported");
	}

	assert(pDDSHeader->dwCaps2 == 0);

	// Validate pixel formats
	if (pDDSHeader->ddspf.dwFlags & DDPF_YUV) {
		DDS_ERROR("Legacy YUV DDS texture is not supported");
	}
	if (pDDSHeader->ddspf.dwFlags & DDPF_LUMINANCE) {
		DDS_ERROR("Legacy single channel DDS texture (luminance only) is not supported");
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// 2) Determine format variations

	info.height = pDDSHeader->dwHeight;
	info.width = pDDSHeader->dwWidth;
	info.pixelFormat = D3DFMT_A8R8G8B8;

	// Mipmap
	bool hasMipmap = (pDDSHeader->dwCaps & DDSCAPS_MIPMAP) != 0;
#ifndef STRICT_DDS_VALIDATION
	// Correct malformed headers if possible
	if (!hasMipmap && pDDSHeader->dwMipMapCount > 1) {
		assert(false);
		pDDSHeader->dwCaps |= DDSCAPS_MIPMAP;
		hasMipmap = true;
	}
#endif

	// POT
	bool isPowerOf2 = (info.width & (info.width - 1)) == 0 && (info.height & (info.height - 1)) == 0;
	isPowerOf2;

	// Uncompressed vs FOURCC
	bool isUncompressed = (pDDSHeader->ddspf.dwFlags & DDPF_RGB) != 0;
	bool hasFOURCC = (pDDSHeader->ddspf.dwFlags & DDPF_FOURCC) != 0;
#ifndef STRICT_DDS_VALIDATION
	// Correct malformed headers if possible
	if (!hasFOURCC && isValidFourCC(pDDSHeader->ddspf.dwFourCC)) {
		assert(false);
		hasFOURCC = true;
	}
#endif

	assert(isUncompressed && !hasFOURCC || !isUncompressed && hasFOURCC); // Either it's uncompressed or has FOURCC, but not both.
	if (!isUncompressed && !hasFOURCC) {
		DDS_ERROR("Compressed DDS texture without valid FOURCC is not supported"); // Compressed texture must have valid FOURCC
	}

	auto fourCC = pDDSHeader->ddspf.dwFourCC;
	info.compression = fourCC;
	if (hasFOURCC && fourCC != FCC_DXT1 && fourCC != FCC_DXT3 && fourCC != FCC_DXT5) {
		DDS_ERROR("DDS texture uses unsupported FOURCC [" << FOURCC(fourCC).getName() << "]");
	}

	// Validate uncompressed pixel format
	bool transformPixels = false;
	if (isUncompressed) {
		if (pDDSHeader->ddspf.dwRGBBitCount == 32) {
			// Transform non-ARGB pixels
			if (pDDSHeader->ddspf.dwRBitMask != A8R8G8B8_R_MASK ||
				pDDSHeader->ddspf.dwGBitMask != A8R8G8B8_G_MASK ||
				pDDSHeader->ddspf.dwBBitMask != A8R8G8B8_B_MASK ||
				pDDSHeader->ddspf.dwABitMask != A8R8G8B8_A_MASK) {
				transformPixels = true;
			}
		} else if (pDDSHeader->ddspf.dwRGBBitCount == 16 || pDDSHeader->ddspf.dwRGBBitCount == 24) {
			// Transform individual pixels to 32-bit
			transformPixels = true;
		} else {
			DDS_ERROR("Uncompressed DDS texture uses unsupported RGB bit count: got " << pDDSHeader->ddspf.dwRGBBitCount << ", expect 32, 24 or 16");
		}
		info.depth = 32;
	} else {
		// DXT1~5 has color depth of 16
		info.depth = 16;
	}

	// Calculate expected sizes - main image
	unsigned DXTBlockSize = 0, expectedInPitch = 0, expectedMainTextureBytes = 0, expectedTotalTextureBytes = 0;
	if (isUncompressed) {
		assert(pDDSHeader->ddspf.dwRGBBitCount == 32 || pDDSHeader->ddspf.dwRGBBitCount == 24 || pDDSHeader->ddspf.dwRGBBitCount == 16);
		expectedInPitch = info.width * (pDDSHeader->ddspf.dwRGBBitCount >> 3);
		expectedTotalTextureBytes = expectedMainTextureBytes = info.height * expectedInPitch;
	} else {
		DXTBlockSize = fourCC == FCC_DXT1 ? 8 : 16;
		expectedInPitch = ((info.width + 3) / 4) * DXTBlockSize;
		expectedTotalTextureBytes = expectedMainTextureBytes = ((info.height + 3) / 4) * expectedInPitch;
	}

#ifndef STRICT_DDS_VALIDATION
	// Some DDS encoder does not set mipmap flag or count even though mipmaps are used
	if (!hasMipmap) {
		// Correct malformed headers if possible
		if (expectedMainTextureBytes >= 64) { // Arbitrary limit. Only when main texture is large enough can we detect significant discrepancy in buffer size.
			auto mipmapDetectThreshold = sizeof(pDDS->sig) + pDDSHeader->dwSize + expectedMainTextureBytes + expectedMainTextureBytes / 4;
			if (rawBufSize >= mipmapDetectThreshold) {
				// Most likely mipmaps are used
				assert(false);
				hasMipmap = true;
			}
		}
	}
#endif

	// Sizes of mipmaps
	unsigned expectedMipmipCount = 1; // 1 for main image
#ifndef STRICT_DDS_VALIDATION
	unsigned expectedTotalTextureBytes2 = expectedTotalTextureBytes; // total size if we ignore mipmap count defined in the header
#endif
	if (hasMipmap) {
		// Additional bytes for low-res textures
		bool hasMipmapCount = (pDDSHeader->dwFlags & DDSD_MIPMAPCOUNT) != 0;
		unsigned mipmapH = info.height, mipmapW = info.width;
		while (mipmapH > 1 || mipmapW > 1) // Bigger than 1 x 1
		{
			// Next mipmap level
			mipmapH = std::max(1u, mipmapH / 2); // half
			mipmapW = std::max(1u, mipmapW / 2); // and half
			unsigned mipmapSize = 0;
			if (isUncompressed) {
				mipmapSize = mipmapH * mipmapW * (pDDSHeader->ddspf.dwRGBBitCount >> 3);
			} else {
				mipmapSize = ((mipmapH + 3) / 4) * ((mipmapW + 3) / 4) * DXTBlockSize;
			}

			if (!hasMipmapCount || expectedMipmipCount < pDDSHeader->dwMipMapCount) { // Mipmap count hasn't exceed what's specified in the header, if any
				expectedTotalTextureBytes += mipmapSize;
				expectedMipmipCount++;
			}

#ifndef STRICT_DDS_VALIDATION
			// Sometimes mipmap count from header does not match the actual number of mipmaps stored
			expectedTotalTextureBytes2 += mipmapSize; // Use a different variable to measure the "would-be" total size if the header is ignored
#endif
		}

		// Validate mipmap count
		if (hasMipmapCount && expectedMipmipCount != pDDSHeader->dwMipMapCount) {
			// Shouldn't be here
			assert(false);
			DDS_ERROR("DDS texture has incorrect mipmap count: got " << pDDSHeader->dwMipMapCount << ", expect " << expectedMipmipCount);
		}

		info.numSubImages = expectedMipmipCount;
	}

	// Validate pitch or linear size
	if (isUncompressed) {
		// Uncompressed
		if (pDDSHeader->dwFlags & DDSD_PITCH) {
			assert(expectedInPitch == pDDSHeader->dwPitchOrLinearSize);
			if (expectedInPitch != pDDSHeader->dwPitchOrLinearSize) {
				DDS_ERROR("DDS texture has invalid pitch value: " << pDDSHeader->dwPitchOrLinearSize << ", expect " << expectedInPitch);
			}
		}
	} else {
		// Compressed
		assert(hasFOURCC);
		if (pDDSHeader->dwFlags & DDSD_LINEARSIZE) {
			if (expectedMainTextureBytes != pDDSHeader->dwPitchOrLinearSize) {
				// Errs out only if strict mode: too many ways of getting a mismatched linear size.
				assert(false);
#ifdef STRICT_DDS_VALIDATION
				DDS_ERROR("DDS texture has invalid top-level texture size: " << pDDSHeader->dwPitchOrLinearSize << ", expect " << expectedMainTextureBytes);
#endif
			}
		}
	}

	// Validate file size
	unsigned expectedFileSize = sizeof(pDDS->sig) + pDDSHeader->dwSize + expectedTotalTextureBytes;
	unsigned expectedFileSizePadded = (expectedFileSize + 15) / 16 * 16; // 16-byte alignment sometimes used in encryption
	if (expectedFileSize != rawBufSize && expectedFileSizePadded != rawBufSize) {
#ifndef STRICT_DDS_VALIDATION
		// See comments for expectedTotalTextureBytes2
		unsigned expectedFileSize2 = sizeof(pDDS->sig) + pDDSHeader->dwSize + expectedTotalTextureBytes2;
		unsigned expectedFileSizePadded2 = (expectedFileSize2 + 15) / 16 * 16;
		if (expectedFileSize2 != rawBufSize && expectedFileSizePadded2 != rawBufSize) {
			DDS_ERROR("DDS texture has incorrect size: got " << rawBufSize << ", expect " << expectedFileSize);
		}
#else
		DDS_ERROR("DDS texture has incorrect size: got " << rawBufSize << ", expect " << expectedFileSize);
#endif
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// 3) Decode and measure opacity
	unsigned outPitch = info.width * 4;
	pixBufSize = outPitch * info.height;
	auto pixBuffer = std::make_unique<unsigned char[]>(pixBufSize);
	bool opaque = true, translucent = false, allZeroAlpha = true;
	if (isUncompressed) {
		if (transformPixels) {
			// Transform raw pixels into 32-bit ARGB
			assert(pDDSHeader->ddspf.dwRGBBitCount == 32 || pDDSHeader->ddspf.dwRGBBitCount == 24 || pDDSHeader->ddspf.dwRGBBitCount == 16);
			unsigned bytesPerPixel = pDDSHeader->ddspf.dwRGBBitCount / 8;
			unsigned pixelMask = pDDSHeader->ddspf.dwRGBBitCount < 32 ? (1u << pDDSHeader->ddspf.dwRGBBitCount) - 1 : 0xFFFFFFFF;

			// Unoptimized implementation as it's a rare case
			// Bit shift from input to output (first shift right, then left)
			size_t downShiftR, downShiftG, downShiftB, downShiftA, upShiftR, upShiftG, upShiftB, upShiftA;
			makeShifts(pDDSHeader->ddspf.dwRBitMask, A8R8G8B8_R_MASK, true, downShiftR, upShiftR);
			makeShifts(pDDSHeader->ddspf.dwGBitMask, A8R8G8B8_G_MASK, true, downShiftG, upShiftG);
			makeShifts(pDDSHeader->ddspf.dwBBitMask, A8R8G8B8_B_MASK, true, downShiftB, upShiftB);
			makeShifts(pDDSHeader->ddspf.dwABitMask, A8R8G8B8_A_MASK, true, downShiftA, upShiftA);

			unsigned char *pIn = &pDDS->data[0];
			unsigned *pOut = (unsigned *)pixBuffer.get();
			for (size_t y = 0; y < info.height; y++) {
				for (size_t x = 0; x < info.width; x++, pOut++) {
					unsigned inPix = (*(unsigned *)pIn) & pixelMask;
					*pOut = ((inPix & pDDSHeader->ddspf.dwRBitMask) >> downShiftR << upShiftR) |
							((inPix & pDDSHeader->ddspf.dwGBitMask) >> downShiftG << upShiftG) |
							((inPix & pDDSHeader->ddspf.dwBBitMask) >> downShiftB << upShiftB) |
							((inPix & pDDSHeader->ddspf.dwABitMask) >> downShiftA << upShiftA);

					// Check opacity if alpha mask is not zero (otherwise opaque)
					if (pDDSHeader->ddspf.dwABitMask != 0) {
						auto inAlpha = inPix & pDDSHeader->ddspf.dwABitMask;
						allZeroAlpha = allZeroAlpha && (inAlpha == 0);

						if (inAlpha < pDDSHeader->ddspf.dwABitMask) {
							// Not fully opaque
							opaque = false;
							translucent = translucent || (inAlpha > 0);
						} else {
							// Force pixel alpha to be fully opaque - otherwise it could be <100% opaque due to up-scaling
							*pOut |= A8R8G8B8_A_MASK;
						}
					} else {
						// Input pixel does not carry alpha information. Treat as opaque.
						*pOut |= A8R8G8B8_A_MASK;
						allZeroAlpha = false;
					}

					pIn += bytesPerPixel;
				}
			}
		} else {
			// Straight copy 32-bit ARGB pixels
			assert(pDDSHeader->ddspf.dwRGBBitCount == 32);
			assert(pixBufSize <= expectedTotalTextureBytes);
			memcpy(pixBuffer.get(), &pDDS->data[0], pixBufSize);

			unsigned *pOut = (unsigned *)pixBuffer.get();
			for (size_t i = 0; i < pixBufSize / sizeof(unsigned); i++, pOut++) {
				auto alpha = *pOut & A8R8G8B8_A_MASK;
				allZeroAlpha = allZeroAlpha && (alpha == 0);

				if (alpha < A8R8G8B8_A_MASK) {
					// Not fully opaque
					opaque = false;
					translucent = translucent || (alpha > 0);
				}
			}
		}
	} else {
		// Decode BC1/2/3 blocks
		for (size_t v = 0; v < (info.height + 3) / 4; v++) {
			unsigned char *pIn = &pDDS->data[v * expectedInPitch];
			unsigned char *pOut = pixBuffer.get() + (v << 2) * outPitch;
			for (size_t u = 0; u < (info.width + 3) / 4; u++) {
				auto bytesDecoded = decodeDXTBlock(fourCC, pIn, pOut, info.width, info.height, u * 4, v * 4, outPitch, opaque, translucent, allZeroAlpha);
				pIn += bytesDecoded;
				pOut += 4 * 4; // Move forward by 4 pixels
			}
		}
	}

	info.opacity = opaque ? ImageOpacity::FULLY_OPAQUE : translucent ? ImageOpacity::TRANSLUCENT : ImageOpacity::ONE_BIT_ALPHA;
	info.allZeroAlpha = allZeroAlpha;
	return pixBuffer.release();
}

inline void interpolateBC1Colors(const unsigned char *pIn, unsigned c[4], unsigned alpha, bool force4Color) {
	BC1Pix *pBC1 = (BC1Pix *)pIn;
	c[0] = alpha | to888(pBC1->c0, 0);
	c[1] = alpha | to888(pBC1->c1, 0);
	if (force4Color || pBC1->c0 > pBC1->c1) {
		// 4-color model
		c[2] = alpha | colorLerpIntCoeff(c[0], c[1], 171, 8); // 171 / (1<<8) ~= 2/3
		c[3] = alpha | colorLerpIntCoeff(c[0], c[1], 85, 8); //  85 / (1<<8) ~= 1/3
	} else {
		// 3-color + transparent
		c[2] = alpha | colorLerpIntCoeff(c[0], c[1], 128, 8); // 128 / (1<<8) ~= 1/2
		c[3] = 0; // transparent black
	}
}

inline void interpolateBC3Alphas(const unsigned char *pIn, unsigned a[8], bool &opaque, bool &translucent) {
	BC3Pix *pBC3 = (BC3Pix *)pIn;
	// Image opacity check
	if (pBC3->a0 < BC3_OPACITY_THRESHOLD) {
		opaque = false;
		translucent = translucent || (pBC3->a0 > 0);
	}
	if (pBC3->a1 < BC3_OPACITY_THRESHOLD) {
		opaque = false;
		translucent = translucent || (pBC3->a1 > 0);
	}

	a[0] = pBC3->a0 << 24;
	a[1] = pBC3->a1 << 24;
	if (pBC3->a0 > pBC3->a1) {
		// { alpha x 8 }
		a[2] = (pBC3->a0 * 219 + pBC3->a1 * 37) >> 8 << 24; // 219 / (1<<8) ~= 6/7
		a[3] = (pBC3->a0 * 183 + pBC3->a1 * 73) >> 8 << 24; // 183 / (1<<8) ~= 5/7
		a[4] = (pBC3->a0 * 146 + pBC3->a1 * 110) >> 8 << 24; // 146 / (1<<8) ~= 4/7
		a[5] = (pBC3->a0 * 110 + pBC3->a1 * 146) >> 8 << 24; // 110 / (1<<8) ~= 3/7
		a[6] = (pBC3->a0 * 73 + pBC3->a1 * 183) >> 8 << 24; //  73 / (1<<8) ~= 2/7
		a[7] = (pBC3->a0 * 37 + pBC3->a1 * 219) >> 8 << 24; //  37 / (1<<8) ~= 1/7
	} else {
		// { alpha x 6, transparent, opaque }
		a[2] = (pBC3->a0 * 205 + pBC3->a1 * 51) >> 8 << 24; // 205 / (1<<8) ~= 4/5
		a[3] = (pBC3->a0 * 154 + pBC3->a1 * 102) >> 8 << 24; // 154 / (1<<8) ~= 3/5
		a[4] = (pBC3->a0 * 102 + pBC3->a1 * 154) >> 8 << 24; // 102 / (1<<8) ~= 2/5
		a[5] = (pBC3->a0 * 51 + pBC3->a1 * 205) >> 8 << 24; //  51 / (1<<8) ~= 1/5
		a[6] = 0;
		a[7] = 0xff000000;
	}
}

inline size_t decodeBC1(const unsigned char *pIn, unsigned char *pOut, unsigned width, unsigned height, unsigned x0, unsigned y0, unsigned outPitch, bool &opaque, bool &translucent, bool &allZeroAlpha) {
	translucent = false; // DXT1 can be either fully opaque or using one-bit-alpha.

	BC1Pix *pBC1 = (BC1Pix *)pIn;
	unsigned c[4];
	interpolateBC1Colors((unsigned char *)&pBC1->c0, c, 0xff000000, false);

	auto clookup = pBC1->clu;
	for (size_t y = y0; y < y0 + 4 && y < height; y++, pOut += outPitch) {
		auto pOutPix = (unsigned *)pOut;
		for (size_t x = x0; x < x0 + 4 && x < width; x++, pOutPix++) {
			*pOutPix = c[clookup & 0x03];
			clookup >>= 2;
			opaque = opaque && (*pOutPix != 0); // Check for transparent pixels
			allZeroAlpha = allZeroAlpha && (*pOutPix == 0); // A BC1 transparent pixel is also black
		}
	}

	return sizeof(BC1Pix);
}

inline size_t decodeBC2(const unsigned char *pIn, unsigned char *pOut, unsigned width, unsigned height, unsigned x0, unsigned y0, unsigned outPitch, bool &opaque, bool &translucent, bool &allZeroAlpha) {
	BC2Pix *pBC2 = (BC2Pix *)pIn;
	assert(pBC2->c0 >= pBC2->c1); // It appears BC2/BC3 use 4-color interpolation only. The question is whether c0 can be less than c1 with 4-color model
	unsigned c[4];
	interpolateBC1Colors((unsigned char *)&pBC2->c0, c, 0, true); // force 4-color model for BC2/BC3

	auto alphaPacked = pBC2->alpha;
	auto clookup = pBC2->clu;
	for (size_t y = y0; y < y0 + 4 && y < height; y++, pOut += outPitch) {
		auto pOutPix = (unsigned *)pOut;
		for (size_t x = x0; x < x0 + 4 && x < width; x++, pOutPix++) {
			auto alpha = alphaPacked & 0x0f;
			allZeroAlpha = allZeroAlpha && (alpha == 0);

			*pOutPix = c[clookup & 0x03] | (alpha << 28);
			clookup >>= 2;
			alphaPacked >>= 4;

			if (alpha < 0x0f) {
				opaque = false;
				translucent = translucent || (alpha > 0);
			} else {
				// Force pixel alpha to be fully opaque - otherwise it could be <100% opaque due to up-scaling
				*pOutPix |= A8R8G8B8_A_MASK;
			}
		}
	}

	return sizeof(BC2Pix);
}

inline size_t decodeBC3(const unsigned char *pIn, unsigned char *pOut, unsigned width, unsigned height, unsigned x0, unsigned y0, unsigned outPitch, bool &opaque, bool &translucent, bool &allZeroAlpha) {
	BC3Pix *pBC3 = (BC3Pix *)pIn;
	assert(pBC3->c0 >= pBC3->c1); // It appears BC2/BC3 use 4-color interpolation only. The question is whether c0 can be less than c1 with 4-color model
	unsigned a[8], c[4];
	interpolateBC3Alphas((unsigned char *)&pBC3->a0, a, opaque, translucent);
	interpolateBC1Colors((unsigned char *)&pBC3->c0, c, 0, true); // force 4-color model for BC2/BC3

	auto alookup = pBC3->alu;
	auto clookup = pBC3->clu;
	for (size_t y = y0; y < y0 + 4 && y < height; y++, pOut += outPitch) {
		auto pOutPix = (unsigned *)pOut;
		for (size_t x = x0; x < x0 + 4 && x < width; x++, pOutPix++) {
			auto alpha = a[alookup & 0x07];
			allZeroAlpha = allZeroAlpha && (alpha == 0);

			opaque = opaque && (alpha != 0); // Catch an edge case in 6-alpha model
			*pOutPix = c[clookup & 0x03] | alpha;
			clookup >>= 2;
			alookup >>= 3;
		}
	}

	return sizeof(BC3Pix);
}

static size_t decodeDXTBlock(unsigned fourCC, const unsigned char *pIn, unsigned char *pOut, unsigned width, unsigned height, unsigned x0, unsigned y0, unsigned outPitch, bool &opaque, bool &translucent, bool &allZeroAlpha) {
	if (fourCC == FCC_DXT1) {
		return decodeBC1(pIn, pOut, width, height, x0, y0, outPitch, opaque, translucent, allZeroAlpha);
	} else if (fourCC == FCC_DXT3) {
		return decodeBC2(pIn, pOut, width, height, x0, y0, outPitch, opaque, translucent, allZeroAlpha);
	} else if (fourCC == FCC_DXT5) {
		return decodeBC3(pIn, pOut, width, height, x0, y0, outPitch, opaque, translucent, allZeroAlpha);
	}

	// Shouldn't be here
	assert(false);
	DDS_ERROR("Unsupported FOURCC [" << FOURCC(fourCC).getName() << "]");
}

//================================================================
// Encoding function
unsigned char *encodeDDS_Fast(const unsigned char *rawBuffer, unsigned rawBufSize, ImageInfo &info, unsigned &DDSBufSize) {
	assert(info.opacity != ImageOpacity::TRANSLUCENT);
	if (info.opacity == ImageOpacity::TRANSLUCENT) {
		DDS_ERROR("encodeDDS_Fast does NOT support alpha blending");
	}

	size_t dsW = (info.width + 3) >> 2;
	size_t dsH = (info.height + 3) >> 2;

	// Assumption: the input pixel array is previously BC1-compressed and opaque
	struct alignedDeleter {
		void operator()(void *ptr) { _aligned_free(ptr); }
	};
	typedef std::unique_ptr<unsigned[][12], alignedDeleter> PalArrayPtr;

	// Palette array - aligned for SSE
	auto c = (PalArrayPtr::pointer)_aligned_malloc(sizeof(PalArrayPtr::element_type) * dsW * dsH, 16);
	PalArrayPtr cPtr(c);
	memset(c, 0, sizeof(PalArrayPtr::element_type) * dsW * dsH);
	for (size_t i = 0; i < dsW * dsH; i++) {
		c[i][1] = 0xffffffff;
	}

	// BC1 encoding buffer
	auto blockSize = sizeof(BC1Pix);
	DDSBufSize = dsW * dsH * blockSize + sizeof(DDS_HEADER) + sizeof(unsigned);
	auto DDSBuf = std::make_unique<unsigned char[]>(DDSBufSize);
	memset(DDSBuf.get(), 0, DDSBufSize);

	auto pDDS = (DDS_FILE *)DDSBuf.get();
	auto pHeader = &pDDS->header;
	auto pixsBC1 = (BC1Pix *)(pDDS->data);

	pDDS->sig = FCC_DDS;
	pHeader->dwSize = sizeof(DDS_HEADER);
	pHeader->dwFlags = g_requiredFlags;
	pHeader->dwHeight = info.height;
	pHeader->dwWidth = info.width;
	pHeader->dwCaps = g_requiredCaps;
	pHeader->ddspf.dwSize = sizeof(DDS_PIXELFORMAT);
	pHeader->ddspf.dwFlags = DDPF_FOURCC;
	pHeader->ddspf.dwFourCC = FCC_DXT1;

	////////////////////////////////////////////////////////////
	// NOTES regarding this simplified BC1 encoder
	//
	// The alternative method cheated by knowing that the pixels of the images have previously
	// been processed by an BC1 encoder, which means colors from any of the DXT 4x4 blocks
	// must be somewhat conform to the BC1 color interpolation model. It means that from each
	// 4x4 block, the minimum and maximum values out of the 16 pixels can be directly used for
	// BC1 encoding *WITHOUT* additional analysis by methods like least square approximation.
	// It also means any of the 16 colors must already be very close to one of the colors
	// interpolated from the min/max values.
	//
	// With the above assumption in mind, it's possible to implement a simple BC1 encoder
	// that is 40+ times faster than the standard one while the result remains in reasonable
	// quality. Some comparison below:
	//
	// +--------------------+--------------+--------------+--------------+
	// | Texture            | Texture Size | This Encoder | NVTT encoder |
	// +--------------------+--------------+--------------+--------------+
	// | Tractor (gimp 50%) |  512 x 512   |     4 ms     |    240 ms    |
	// | Tractor            | 2048 x 2048  |    60 ms     |   2900 ms    |
	// +--------------------+--------------+--------------+--------------+
	//
	// The speed improvement makes it possible for us to do real-time BC1 encoding. This can
	// be very useful once we switch to JPG for asset downloads. Because JPG would have to be
	// fully decompressed before it can be used by DirectX. if we don't re-encode JPGs after
	// download and use them directly, in theory the required convention memory and graphics
	// memory could be 3x to 6x as it would be for DXT.
	//
	// One little problem is that for BC1 there could be two possible interpolation models:
	// the four-color one: {0, 0.33, 0.66, 1} or the three-color one {0, 0.5, 1}. The model
	// used vary among different 4x4 blocks. The data indicating which model is used on which
	// 4x4 block is only available in the original DDS file and hence lost with JPG. (See wiki
	// page: https://en.wikipedia.org/wiki/S3_Texture_Compression).
	//
	// I tried to add logic to detect the 3-color blocks but it does not seem to give significant
	// improvement to the image quality. I have it ifdef-ed out to save some processing time
	// (<10% overhead).

	// Pass 1: find c0 and c1
	auto pixels = (unsigned *)rawBuffer;
	size_t pixIdx = 0;
	for (auto y = 0u; y < info.height; y++) {
		for (auto x = 0u; x < info.width; x++, pixIdx++) {
			size_t dsIdx = (y >> 2) * dsW + (x >> 2);
			auto pixel = pixels[pixIdx] & 0x00f8fcf8;
			c[dsIdx][0] = std::max(c[dsIdx][0], pixel);
			c[dsIdx][1] = std::min(c[dsIdx][1], pixel);
		}
	}

	// Interpolate
	size_t dsIdx = 0;
	for (auto y = 0u; y < dsH; y++) {
		for (auto x = 0u; x < dsW; x++, dsIdx++) {
			assert(c[dsIdx][0] >= c[dsIdx][1]);
			if (c[dsIdx][0] > c[dsIdx][1]) {
				c[dsIdx][2] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 171, 8); // 171/256 ~= 2/3
				c[dsIdx][3] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 85, 8); //  85/256 ~= 1/3
				// Interval helpers
				c[dsIdx][4] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 213, 8); // 213/256 ~= 5/6
				c[dsIdx][5] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 128, 8); // 128/256  = 1/2
				c[dsIdx][6] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 43, 8); //  43/256 ~= 1/6
				// Center helper
				c[dsIdx][7] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 120, 8); // 120/256  = 15/32
				c[dsIdx][8] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 136, 8); // 136/256  = 17/32
			} else {
				c[dsIdx][2] = colorLerpIntCoeff(c[dsIdx][0], c[dsIdx][1], 128, 8); // 128/256 = 1/2
				c[dsIdx][3] = 0;
			}
			pixsBC1[dsIdx].c0 = to565(c[dsIdx][0]);
			pixsBC1[dsIdx].c1 = to565(c[dsIdx][1]);
		}
	}

	auto useCompress3 = std::make_unique<bool[]>(dsW * dsH);
	memset(useCompress3.get(), 0, sizeof(bool) * dsW * dsH);

	// Pass 2: reconstruct
	pixIdx = 0;
	for (auto y = 0u; y < info.height; y++) {
		for (auto x = 0u; x < info.width; x++, pixIdx++) {
			dsIdx = (y >> 2) * dsW + (x >> 2);
			unsigned temp = pixels[pixIdx] & 0x00f8fcf8;

			int colorIndex = 0; // Use any color if c0 == c1

			assert(c[dsIdx][0] >= c[dsIdx][1]);

#ifdef DETECT_COMPRESS_3
			if (useCompress3[dsIdx]) {
				unsigned minDiff = std::numeric_limits<unsigned>::max();
				colorIndex = getBestColorIndex(temp, c[dsIdx], 3, minDiff);
			} else
#endif
				if (c[dsIdx][0] > c[dsIdx][1]) {
				assert(useCompress3[dsIdx] || pixsBC1[dsIdx].c0 > pixsBC1[dsIdx].c1);
				unsigned minDiff = std::numeric_limits<unsigned>::max();
				colorIndex = getBestColorIndex(temp, c[dsIdx], 4, minDiff);
#ifdef DETECT_COMPRESS_3
				if (!useCompress3[dsIdx] && colorDiff(temp, c[dsIdx][5]) < minDiff) {
					std::swap(pixsBC1[dsIdx].c0, pixsBC1[dsIdx].c1);
					size_t xx, yy;
					for (yy = 0u; yy < y % 4; yy++) {
						pixsBC1[dsIdx].tbl[yy] |= 0xaa; // XY -> 1Y
						pixsBC1[dsIdx].tbl[yy] ^= 0xff; // 10 -> 01, 11 -> 00
					}

					for (xx = 0u; xx < x % 4; xx++) {
						auto shift = xx << 1;
						pixsBC1[dsIdx].tbl[yy] |= (0x02 << shift);
						pixsBC1[dsIdx].tbl[yy] ^= (0x03 << shift);
					}
					useCompress3[dsIdx] = true;
					std::swap(c[dsIdx][0], c[dsIdx][1]);
					c[dsIdx][2] = c[dsIdx][5];
					c[dsIdx][3] = 0;
					colorIndex = 2;
				}
#endif
			}

			assert(colorIndex >= 0 && colorIndex < 4);
			size_t shift = (x % 4) << 1;
			auto pLookup = (unsigned char *)&pixsBC1[dsIdx].clu;
			pLookup[y % 4] |= (colorIndex << shift);
		}
	}

	return DDSBuf.release();
}

#pragma comment(lib, "nvtt.lib")

unsigned char *encodeDDS_NVTT(const unsigned char *rawBuffer, unsigned rawBufSize, ImageInfo &info, unsigned &DDSBufSize) {
	class DXTOutputHandler : public nvtt::OutputHandler {
	public:
		DXTOutputHandler(unsigned char *buffer, unsigned bufSize, size_t width, size_t height) :
				m_buf(buffer), m_bufSize(bufSize), m_cur(buffer), m_w(width), m_h(height), m_rcvd(0) {}

		virtual void beginImage(int size, int width, int height, int depth, int face, int miplevel) {
			assert(m_w == (size_t)width);
			assert(m_h == (size_t)height);
			assert(m_bufSize == size + sizeof(DDS_HEADER) + sizeof(unsigned)); // size: encoded size exclude headers
			assert(miplevel == 0);
		}

		virtual bool writeData(const void *data, int size) {
			assert(m_cur != NULL);
			assert(m_cur + size <= m_buf + m_bufSize);

			m_rcvd += size;
			//std::cout << "NVTT bytes received: " << m_rcvd << std::endl;

			if (m_cur + size <= m_buf + m_bufSize) {
				memcpy(m_cur, data, size);
				m_cur += size;
				return true;
			}

			return false;
		}

		bool done() { return m_cur == m_buf + m_bufSize; }

	private:
		unsigned m_bufSize;
		unsigned char *m_buf;
		unsigned char *m_cur;
		size_t m_w;
		size_t m_h;
		size_t m_rcvd;
	};

	size_t dsW = (info.width + 3) >> 2;
	size_t dsH = (info.height + 3) >> 2;

	nvtt::Format format;
	switch (info.opacity) {
		case ImageOpacity::TRANSLUCENT:
			format = nvtt::Format_DXT5;
			break;
		case ImageOpacity::ONE_BIT_ALPHA:
			format = nvtt::Format_DXT1a;
			break;
		default:
			format = nvtt::Format_DXT1;
			break;
	}

	// Allocate out buffer
	auto blockSize = (format == nvtt::Format_DXT1 || format == nvtt::Format_DXT1a) ? sizeof(BC1Pix) : sizeof(BC3Pix); // BC2/BC3 are of same size
	DDSBufSize = dsW * dsH * blockSize + sizeof(DDS_HEADER) + sizeof(unsigned);
	auto DDSBuf = std::make_unique<unsigned char[]>(DDSBufSize);
	memset(DDSBuf.get(), 0, DDSBufSize);

	// Input options
	nvtt::InputOptions inputOptions;
	inputOptions.setTextureLayout(nvtt::TextureType_2D, (int)info.width, (int)info.height);
	inputOptions.setMipmapData(rawBuffer, (int)info.width, (int)info.height);
	inputOptions.setMipmapGeneration(GENERATE_MIPMAPS);

	// Output options
	DXTOutputHandler outputHandler(DDSBuf.get(), DDSBufSize, info.width, info.height);
	nvtt::OutputOptions outputOptions;
	outputOptions.setOutputHandler(&outputHandler);
	outputOptions.setOutputHeader(true);

	// Compression options
	nvtt::CompressionOptions compressionOptions;
	compressionOptions.setFormat(format);

	nvtt::Compressor compressor;
	compressor.process(inputOptions, compressionOptions, outputOptions);

	assert(outputHandler.done());
	if (outputHandler.done()) {
		return DDSBuf.release();
	}

	return nullptr;
}

} // namespace KEP
