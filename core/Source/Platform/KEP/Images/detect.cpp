///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <string>
#include <vector>

namespace KEP {

static unsigned char sigDDS[] = { 'D', 'D', 'S', ' ' }; // DDS
static unsigned char sigJPG1[] = { 'J', 'F', 'I', 'F', 0x00, 0x01 }; // JFIF
static unsigned char sigJPG2[] = { 'E', 'x', 'i', 'f', 0x00, 0x00 }; // Exif JPEG
static unsigned char sigJPG3[] = { 0xFF, 0xD8, 0xFF }; // Possible other JPEG variations
static unsigned char sigPNG[] = { 0x89, 'P', 'N', 'G' }; // PNG
static unsigned char sigGIF1[] = { 'G', 'I', 'F', '8', '7', 'a' }; // GIF87a
static unsigned char sigGIF2[] = { 'G', 'I', 'F', '8', '9', 'a' }; // GIF89a
static unsigned char sigTGA[] = { 'T', 'R', 'U', 'E', 'V', 'I', 'S', 'I', 'O', 'N', '-', 'F', 'I', 'L', 'E', '.', 0x00 }; // TGA

struct FormatInfo {
	std::string type;
	unsigned char* magic;
	size_t length;
	int offset;
};

static std::vector<FormatInfo> knownFormats{
	{ "DDS", sigDDS, _countof(sigDDS), 0 },
	{ "JPG", sigJPG1, _countof(sigJPG1), 6 },
	{ "JPG", sigJPG2, _countof(sigJPG2), 6 },
	{ "JPG?", sigJPG3, _countof(sigJPG3), 0 },
	{ "PNG", sigPNG, _countof(sigPNG), 0 },
	{ "GIF", sigGIF1, _countof(sigGIF1), 0 },
	{ "GIF", sigGIF2, _countof(sigGIF2), 0 },
	{ "TGA", sigTGA, _countof(sigTGA), -(int)_countof(sigTGA) },
};

// Quick and dirty format detection, assuming input is a valid image of certain file format
std::string detectImageFormat(const unsigned char* rawBuffer, unsigned rawBufSize) {
	for (const auto& fmt : knownFormats) {
		if (fmt.offset >= 0 && rawBufSize >= fmt.offset + fmt.length && memcmp(rawBuffer + fmt.offset, fmt.magic, fmt.length) == 0 ||
			fmt.offset < 0 && rawBufSize > -fmt.offset && memcmp(rawBuffer + rawBufSize + fmt.offset, fmt.magic, fmt.length) == 0) {
			return fmt.type;
		}
	}

	return std::string();
}

} // namespace KEP
