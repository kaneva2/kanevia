///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

//Use Microsoft D3D header instead?

//https://msdn.microsoft.com/en-us/library/windows/desktop/bb943982%28v=vs.85%29.aspx
struct DDS_PIXELFORMAT {
	DWORD dwSize;
	DWORD dwFlags;
	DWORD dwFourCC;
	DWORD dwRGBBitCount;
	DWORD dwRBitMask;
	DWORD dwGBitMask;
	DWORD dwBBitMask;
	DWORD dwABitMask;
};

struct DDS_HEADER {
	DWORD dwSize;
	DWORD dwFlags;
	DWORD dwHeight;
	DWORD dwWidth;
	DWORD dwPitchOrLinearSize;
	DWORD dwDepth;
	DWORD dwMipMapCount;
	DWORD dwReserved1[11];
	DDS_PIXELFORMAT ddspf;
	DWORD dwCaps;
	DWORD dwCaps2;
	DWORD dwCaps3;
	DWORD dwCaps4;
	DWORD dwReserved2;
};

struct DDS_FILE {
	unsigned sig;
	DDS_HEADER header;
	unsigned char data[1];
};

enum {
	DDSD_CAPS = 0x1,
	DDSD_HEIGHT = 0x2,
	DDSD_WIDTH = 0x4,
	DDSD_PITCH = 0x8,
	DDSD_PIXELFORMAT = 0x1000,
	DDSD_MIPMAPCOUNT = 0x20000,
	DDSD_LINEARSIZE = 0x80000,
	DDSD_DEPTH = 0x800000,
};

enum {
	DDSCAPS_ALPHA = 0x02,
	DDSCAPS_COMPLEX = 0x08,
	DDSCAPS_MIPMAP = 0x400000,
	DDSCAPS_TEXTURE = 0x1000,
};

enum {
	DDPF_ALPHAPIXELS = 0x1,
	DDPF_ALPHA = 0x2,
	DDPF_FOURCC = 0x4,
	DDPF_RGB = 0x40,
	DDPF_YUV = 0x200,
	DDPF_LUMINANCE = 0x20000,
};

class FOURCC {
public:
	FOURCC(DWORD dw) :
			m_dw(dw) {}
	FOURCC(const char str[4]) { memcpy(&m_dw, str, sizeof(DWORD)); }
	operator unsigned() { return m_dw; }
	std::string getName() {
		std::string res;
		res.resize(4);
		memcpy(&res[0], &m_dw, sizeof(DWORD));
		return res;
	}

private:
	DWORD m_dw;
};

struct BC1Pix {
	unsigned short c0;
	unsigned short c1;
	unsigned clu;
};

struct BC2Pix {
	unsigned long long alpha;
	unsigned short c0;
	unsigned short c1;
	unsigned clu;
};

struct BC3Pix {
	union {
		struct {
			unsigned char a0;
			unsigned char a1;
		};
		struct {
			unsigned long long a01 : 16; // two lower bytes are a0 and a1
			unsigned long long alu : 48; // 48-bit alpha lookup table
		};
	};
	unsigned short c0;
	unsigned short c1;
	unsigned clu;
};
