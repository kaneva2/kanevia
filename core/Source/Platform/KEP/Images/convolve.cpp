///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// Image convolution: integer version
#include "stdafx.h"
#include "KEP/Images/KEPImages_Convolve.h"
#include "assert.h"
#include <set>
#include <memory>
#include <algorithm>
#include <iterator>

#define USE_SSE2 // ~5% faster if enabled
#define FAST_ESTIMATED_MEDIAN // Save 60% median sampling time if enabled

namespace KEP {

#ifdef USE_SSE2
static const __m128i zeroX = _mm_setzero_si128(); // 0, 0, 0, 0
static const __m128i oneX = _mm_set1_epi32(1); // 1, 1, 1, 1
static const __m128i lumFactor = _mm_set_epi16(0, 0, 0, 0, 0, 3, 6, 1); // R*0.3 + G*0.6 + B*0.1
#endif

union Color32Bit {
	struct {
		unsigned b : 8;
		unsigned g : 8;
		unsigned r : 8;
		unsigned a : 8;
	} chan;
	unsigned dw;
};

// Retrieve pixel value at (x, y). x/y can be outside image boundary. Nearest border pixel will be used if out-of-bound.
inline unsigned getRawPixelAt(unsigned* pixels, size_t pitch, size_t width, size_t height, int x, int y) {
	assert(width != 0 && height != 0);
	assert(x >= 0 && x < width && y >= 0 && y < height);

#ifdef CONV_ALLOW_CUSTOM_PITCH
	auto pScanline = reinterpret_cast<unsigned*>(reinterpret_cast<unsigned char*>(pixels) + pitch * y);
	return pScanline[x];
#else
	return pixels[y * width + x];
#endif
}

bool convolve(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelFlt& kernel, PixelSampler* sampler, void** pOutPixels) {
	assert(kernel.w > 0 && kernel.h > 0 && kernel.w * kernel.h < MAX_KERNEL_SIZE);
	return convolve(pixels, pitch, width, height, (KernelInt)kernel, sampler, pOutPixels);
}

bool convolve(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelInt& kernel, PixelSampler* sampler, void** pOutPixels) {
#ifdef CONV_ALLOW_CUSTOM_PITCH
#else
	assert(pitch == width * 4);
#endif
	assert(pOutPixels != nullptr);
	assert(pixels != nullptr && width != 0 && height != 0 && pitch >= width * 4);
	assert(kernel.w != 0 && kernel.h != 0);

	if (pOutPixels == nullptr || pixels == nullptr || width == 0 || height == 0 || kernel.w == 0 || kernel.h == 0) {
		return false;
	}

	auto outPixels = std::make_unique<unsigned[]>(width * height);
	if (!outPixels) {
		assert(false);
		return false;
	}

	int edgeWidth = (int)std::max(kernel.w / 2, kernel.h / 2);
	if (sampler != nullptr) {
		edgeWidth = (int)std::max(kernel.w / 2 + sampler->getBlockWidth() / 2, kernel.h / 2 + sampler->getBlockHeight() / 2);
	}

	// For all pixels from the image
	size_t pixCount = 0;
	for (int y = 0; y < (int)height; y++) {
		bool isBorderY = y < (int)edgeWidth || y >= (int)height - (int)edgeWidth;

		for (int x = 0; x < (int)width; x++, pixCount++) {
			if (isBorderY || x < (int)edgeWidth || x >= (int)width - (int)edgeWidth) {
				outPixels[pixCount] = getRawPixelAt(pixels, pitch, width, height, x, y) | 0xff000000;
				continue;
			}

			// For all weights in kernel matrix
			size_t weightIndex = 0;

			// Process all pixels, with boundary check
#ifdef USE_SSE2
			__m128i sumX = _mm_setzero_si128();
#else
			int out[4] = {};
#endif
			for (int dy = -(int)kernel.h / 2; dy <= (int)(kernel.h - 1) / 2; dy++) {
				for (int dx = -(int)kernel.w / 2; dx <= (int)(kernel.w - 1) / 2; dx++, weightIndex++) {
					int weight = kernel.m[weightIndex];
					if (weight == 0) {
						// Shortcut
						continue;
					}

					unsigned pix;
					if (sampler != nullptr) {
						pix = sampler->get(pixels, pitch, width, height, x + dx, y + dy);
					} else {
						// single pixel at (x+dx, y+dy)
						pix = getRawPixelAt(pixels, pitch, width, height, x + dx, y + dy);
					}

#ifdef USE_SSE2
					__m128i wgtX = _mm_set1_epi32(weight);
					__m128i pixX = _mm_cvtsi32_si128(pix);
					pixX = _mm_unpacklo_epi8(pixX, zeroX); // b16, g16, r16, x16,   0,   0,   0,   0
					pixX = _mm_unpacklo_epi16(pixX, zeroX); // b32,      g32,        r32,      x32
					sumX = _mm_add_epi32(sumX, _mm_mullo_epi32(pixX, wgtX)); // b32*wgt,  g32*wgt,    r32*wgt,  x32 * wgt
#else
					if (weight == 0x100) {
						out[0] += (pix & 0xff) << 8;
						out[1] += (pix & 0xff00);
						pix >>= 8;
						out[2] += (pix & 0xff00);
					} else {
						out[0] += (int)(pix & 0xff) * weight;
						pix >>= 8;
						out[1] += (int)(pix & 0xff) * weight;
						pix >>= 8;
						out[2] += (int)(pix & 0xff) * weight;
					}
#endif
				}
			}

#ifdef USE_SSE2
			// Get absolute value
			__m128i signExtX, signBitX;
			signExtX = _mm_srai_epi32(sumX, 31); // 0xffffffff or 0x00000000
			signBitX = _mm_and_si128(signExtX, oneX); // 1 or 0
			sumX = _mm_xor_si128(sumX, signExtX);
			sumX = _mm_add_epi32(sumX, signBitX); // Make positive

			sumX = _mm_srli_epi32(sumX, 8); // sum >> 8  (either logical or arithmatic as the values should be positive)

			sumX = _mm_packus_epi32(sumX, zeroX);
			sumX = _mm_packus_epi16(sumX, zeroX);
			outPixels[pixCount] = sumX.m128i_i32[0] & 0x00ffffff | 0xff000000;
#else
			if (out[0] < 0)
				out[0] = -out[0];
			if (out[1] < 0)
				out[1] = -out[1];
			if (out[2] < 0)
				out[2] = -out[2];

			Color32Bit pix;
			pix.chan.a = 0xff;
			pix.chan.r = out[2] >> 8;
			pix.chan.g = out[1] >> 8;
			pix.chan.b = out[0] >> 8;
			outPixels[pixCount] = pix.dw;
#endif
		}
	}

	*pOutPixels = outPixels.release();
	return true;
}

bool convolve2(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelFlt& kernel, PixelSampler* sampler, unsigned brightness, bool grayScale, unsigned lumThres, unsigned lumMax, void** pOutPixels) {
	assert(kernel.w > 0 && kernel.h > 0 && kernel.w * kernel.h < MAX_KERNEL_SIZE);
	return convolve2(pixels, pitch, width, height, (KernelInt)kernel, sampler, brightness, grayScale, lumThres, lumMax, pOutPixels);
}

bool convolve2(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelInt& kernel, PixelSampler* sampler, unsigned brightness, bool grayScale, unsigned lumThres, unsigned lumMax, void** pOutPixels) {
#ifdef CONV_ALLOW_CUSTOM_PITCH
#else
	assert(pitch == width * 4);
#endif
	assert(pOutPixels != nullptr);
	assert(pixels != nullptr && width != 0 && height != 0 && pitch >= width * 4);
	assert(kernel.w != 0 && kernel.h != 0);

	if (pOutPixels == nullptr || pixels == nullptr || width == 0 || height == 0 || kernel.w == 0 || kernel.h == 0) {
		return false;
	}

	// Allocate new bitmap
	auto outPixels = std::make_unique<unsigned[]>(width * height);
	if (!outPixels) {
		assert(false);
		return false;
	}

	std::unique_ptr<unsigned[]> resampledPixelsPtr;
	if (sampler != nullptr) {
		// Re-sample input image
		unsigned* resampledPixels = nullptr;
		KernelInt dummy{ 1, 1, { 0x100 } };
		if (!convolve(pixels, pitch, width, height, dummy, sampler, (void**)&resampledPixels)) {
			return false;
		}

		assert(resampledPixels != nullptr);
		resampledPixelsPtr = std::unique_ptr<unsigned[]>(resampledPixels);
		pixels = resampledPixels;
	}

	int edgeWidth = (int)std::max(kernel.w / 2, kernel.h / 2);

	// For all pixels from the image
	size_t pixCount = 0;
	for (int y = 0; y < (int)height; y++) {
		bool isBorderY = y < (int)edgeWidth || y >= (int)height - (int)edgeWidth;

		for (int x = 0; x < (int)width; x++, pixCount++) {
			if (isBorderY || x < (int)edgeWidth || x >= (int)width - (int)edgeWidth) {
				outPixels[pixCount] = getRawPixelAt(pixels, pitch, width, height, x, y) | 0xff000000;
				continue;
			}

			// For all weights in kernel matrix
			size_t weightIndex = 0;

			// Process all pixels, with boundary check
#ifdef USE_SSE2
			__m128i sum1X = _mm_setzero_si128();
			__m128i sum2X = _mm_setzero_si128();
#else
			int sum1[4] = {};
			int sum2[4] = {};
#endif
			for (int dy = -(int)kernel.h / 2; dy <= (int)(kernel.h - 1) / 2; dy++) {
				for (int dx = -(int)kernel.w / 2; dx <= (int)(kernel.w - 1) / 2; dx++, weightIndex++) {
					int weight = kernel.m[weightIndex];
					if (weight == 0) {
						// Shortcut
						continue;
					}

					unsigned pix1 = getRawPixelAt(pixels, pitch, width, height, x + dx, y + dy);
					unsigned pix2 = getRawPixelAt(pixels, pitch, width, height, x + dy, y + dx);

#ifdef USE_SSE2
					__m128i wgtX = _mm_set1_epi32(weight);
					__m128i pix1X = _mm_cvtsi32_si128(pix1);
					pix1X = _mm_unpacklo_epi8(pix1X, zeroX); // b16, g16, r16, x16,   0,   0,   0,   0
					pix1X = _mm_unpacklo_epi16(pix1X, zeroX); // b32,      g32,        r32,      x32
					sum1X = _mm_add_epi32(sum1X, _mm_mullo_epi32(pix1X, wgtX)); // b32*wgt,  g32*wgt,    r32*wgt,  x32 * wgt

					__m128i pix2X = _mm_cvtsi32_si128(pix2);
					pix2X = _mm_unpacklo_epi8(pix2X, zeroX); // b16, g16, r16, x16,   0,   0,   0,   0
					pix2X = _mm_unpacklo_epi16(pix2X, zeroX); // b32,      g32,        r32,      x32
					sum2X = _mm_add_epi32(sum2X, _mm_mullo_epi32(pix2X, wgtX)); // b32*wgt,  g32*wgt,    r32*wgt,  x32 * wgt
#else
					if (weight == 0x100) {
						sum1[0] += (pix1 & 0xff) << 8;
						sum1[1] += (pix1 & 0xff00);
						pix1 >>= 8;
						sum1[2] += (pix1 & 0xff00);
					} else {
						sum1[0] += (int)(pix1 & 0xff) * weight;
						pix1 >>= 8;
						sum1[1] += (int)(pix1 & 0xff) * weight;
						pix1 >>= 8;
						sum1[2] += (int)(pix1 & 0xff) * weight;
					}

					if (weight == 0x100) {
						sum2[0] += (pix2 & 0xff) << 8;
						sum2[1] += (pix2 & 0xff00);
						pix2 >>= 8;
						sum2[2] += (pix2 & 0xff00);
					} else {
						sum2[0] += (int)(pix2 & 0xff) * weight;
						pix2 >>= 8;
						sum2[1] += (int)(pix2 & 0xff) * weight;
						pix2 >>= 8;
						sum2[2] += (int)(pix2 & 0xff) * weight;
					}
#endif
				}
			}

#ifdef USE_SSE2
			// Get absolute values
			__m128i signExtX, signBitX;
			signExtX = _mm_srai_epi32(sum1X, 31); // 0xffffffff or 0x00000000
			signBitX = _mm_and_si128(signExtX, oneX); // 1 or 0
			sum1X = _mm_xor_si128(sum1X, signExtX);
			sum1X = _mm_add_epi32(sum1X, signBitX); // Make positive

			signExtX = _mm_srai_epi32(sum2X, 31); // 0xffffffff or 0x00000000
			signBitX = _mm_and_si128(signExtX, oneX); // 1 or 0
			sum2X = _mm_xor_si128(sum2X, signExtX);
			sum2X = _mm_add_epi32(sum2X, signBitX); // Make positive

			// Sum up and down scale pixel values as weights are up-scaled by 256
			sum1X = _mm_add_epi32(sum1X, sum2X); // sum = abs(sum1) + abs(sum2)
			sum1X = _mm_srli_epi32(sum1X, 8); // sum = sum >> 8  (either logical or arithmatic as the values should be positive)

			// Brightness
			__m128i bgtX = _mm_set1_epi32(brightness);
			sum1X = _mm_mullo_epi32(sum1X, bgtX); // sum *= scale

			sum1X = _mm_packus_epi32(sum1X, zeroX); // 32-bit -> 16-bit
			__m128i lum10X = _mm_madd_epi16(sum1X, lumFactor); // B*1 + G*6, R*3, 0, 0

			unsigned lum = (lum10X.m128i_u32[0] + lum10X.m128i_u32[1]) / 10; // lum = (B*1 + G*6 + R*3) / 10
			if (lum > 255) {
				lum = 255;
			}

			if (lum < lumThres) {
				// Clip dark pixels
				outPixels[pixCount] = 0xff000000;
			} else if (grayScale) {
				// Gray scale
				if (lumMax < lum) {
					lum = lumMax;
				}
				outPixels[pixCount] = lum << 16 | lum << 8 | lum | 0xff000000;
			} else {
				// Color
				sum1X = _mm_packus_epi16(sum1X, zeroX); // 16-bit -> 8-bit
				outPixels[pixCount] = sum1X.m128i_i32[0] & 0x00ffffff | 0xff000000;
			}
#else
			if (sum1[0] < 0)
				sum1[0] = -sum1[0];
			if (sum1[1] < 0)
				sum1[1] = -sum1[1];
			if (sum1[2] < 0)
				sum1[2] = -sum1[2];
			if (sum2[0] < 0)
				sum2[0] = -sum2[0];
			if (sum2[1] < 0)
				sum2[1] = -sum2[1];
			if (sum2[2] < 0)
				sum2[2] = -sum2[2];

			sum1[0] += sum2[0];
			sum1[1] += sum2[1];
			sum1[2] += sum2[2];

			sum1[0] >>= 8;
			sum1[1] >>= 8;
			sum1[2] >>= 8;

			sum1[0] *= brightness;
			sum1[1] *= brightness;
			sum1[2] *= brightness;

			unsigned lum10 = sum1[0] + sum1[1] * 6 + sum1[2] * 3;
			if (lum10 < lumThres * 10) {
				sum1[0] = sum1[1] = sum1[2] = 0;
			} else if (grayScale) {
				sum1[0] = sum1[1] = sum1[2] = lum10 / 10;
			}

			Color32Bit pix;
			pix.chan.a = 0xff;
			pix.chan.r = sum1[2];
			pix.chan.g = sum1[1];
			pix.chan.b = sum1[0];
			outPixels[pixCount] = pix.dw;
#endif
		}
	}

	*pOutPixels = outPixels.release();
	return true;
}

#ifdef FAST_ESTIMATED_MEDIAN
// Sample pixel R/G/B value at (x, y). x/y can be outside image boundary.
unsigned PixelSampler_Median::get(unsigned* pixels, size_t pitch, size_t width, size_t height, int x, int y) {
	// Scan all pixels in the neighborhood and place per-channel values into one of the 16 buckets
	unsigned bucketsR[16] = {};
	unsigned bucketsG[16] = {};
	unsigned bucketsB[16] = {};

	size_t pixCount = 0;
	for (int i = getMinY(); i <= getMaxY(); i++) {
		for (int j = getMinX(); j <= getMaxX(); j++, pixCount++) {
			unsigned pix = getRawPixelAt(pixels, pitch, width, height, x + j, y + i);
			pix >>= 4;
			bucketsB[(pix & 0xf)]++;
			pix >>= 8;
			bucketsG[(pix & 0xf)]++;
			pix >>= 8;
			bucketsR[(pix & 0xf)]++;
		}
	}

	unsigned halfCount = pixCount >> 1;
	unsigned bucketWidth = 1 << 4;

	Color32Bit res = {};

	unsigned rCount = 0;
	for (size_t i = 0; i < 16; i++) {
		if (rCount + bucketsR[i] >= halfCount) {
			res.chan.r = bucketWidth * i + 16 * (halfCount - rCount) / bucketsR[i];
			break;
		}
		rCount += bucketsR[i];
	}

	unsigned gCount = 0;
	for (size_t i = 0; i < 16; i++) {
		if (gCount + bucketsG[i] >= halfCount) {
			res.chan.g = bucketWidth * i + 16 * (halfCount - gCount) / bucketsG[i];
			break;
		}
		gCount += bucketsG[i];
	}

	unsigned bCount = 0;
	for (size_t i = 0; i < 16; i++) {
		if (bCount + bucketsB[i] >= halfCount) {
			res.chan.b = bucketWidth * i + 16 * (halfCount - bCount) / bucketsB[i];
			break;
		}
		bCount += bucketsB[i];
	}

	return res.dw;
}
#else
// Sample pixel R/G/B value at (x, y). x/y can be outside image boundary.
unsigned PixelSampler_Median::get(unsigned* pixels, size_t pitch, size_t width, size_t height, int x, int y) {
	// Scan all pixels in the neighborhood
	unsigned* allR = (unsigned*)alloca(sizeof(unsigned) * getBlockWidth() * getBlockHeight());
	unsigned* allG = (unsigned*)alloca(sizeof(unsigned) * getBlockWidth() * getBlockHeight());
	unsigned* allB = (unsigned*)alloca(sizeof(unsigned) * getBlockWidth() * getBlockHeight());

	size_t pixCount = 0;
	for (int i = -(int)getBlockHeight() / 2; i <= (int)(getBlockHeight() - 1) / 2; i++) {
		for (int j = -(int)getBlockWidth() / 2; j <= (int)(getBlockWidth() - 1) / 2; j++, pixCount++) {
			unsigned pix = getRawPixelAt(pixels, pitch, width, height, x + j, y + i);
			allB[pixCount] = pix & 0xff;
			pix >>= 8;
			allG[pixCount] = pix & 0xff;
			pix >>= 8;
			allR[pixCount] = pix & 0xff;
		}
	}

	// Take median value from each of three channels
	unsigned pix = getMedian(allR, pixCount);
	pix <<= 8;
	pix |= getMedian(allG, pixCount);
	pix <<= 8;
	pix |= getMedian(allB, pixCount);
	return pix;
}

unsigned PixelSampler_Median::getMedian(unsigned* array, size_t size) {
	assert(array != nullptr && size != 0);
	if (size == 1) {
		return array[0];
	}

	size_t halfCount = size / 2;
	std::nth_element(array, array + halfCount, array + size);

	unsigned candidate = array[halfCount];
	if (size % 2 == 1) {
		// Odd number of elements
		return candidate;
	}

	// Even number of elements - get the max value to the left of the candidate
	auto itr = std::max_element(array, array + halfCount);
	// Take the average
	return (candidate + *itr) / 2;
}
#endif

PixelSampler_Weighted::PixelSampler_Weighted(size_t blockW, size_t blockH, int weights[]) :
		PixelSampler(blockW, blockH) {
	size_t numWeights = blockW * blockH;
	std::copy(weights, weights + numWeights, std::back_inserter(m_weights));
}

PixelSampler_Weighted::PixelSampler_Weighted(size_t blockW, size_t blockH, float weights[]) :
		PixelSampler(blockW, blockH) {
	size_t numWeights = blockW * blockH;
	m_weights.reserve(numWeights);
	for (size_t i = 0; i < numWeights; i++) {
		m_weights.push_back((int)(weights[i] * 256.0f + 0.5f));
	}
}

// Sample pixel R/G/B value at (x, y). x/y can be outside image boundary.
unsigned PixelSampler_Weighted::get(unsigned* pixels, size_t pitch, size_t width, size_t height, int x, int y) {
#ifdef USE_SSE2
	__m128i sumX = _mm_setzero_si128();
#else
	int sum[3] = {};
#endif

	size_t pixCount = 0;
	for (int i = getMinY(); i <= getMaxY(); i++) {
		for (int j = getMinX(); j <= getMaxX(); j++, pixCount++) {
			assert(pixCount < m_weights.size());
			int weight = m_weights[pixCount];
			if (weight == 0) {
				continue;
			}
			unsigned pix = getRawPixelAt(pixels, pitch, width, height, x + j, y + i);
#ifdef USE_SSE2
			__m128i wgtX = _mm_set1_epi32(weight);
			__m128i pixX = _mm_cvtsi32_si128(pix);
			pixX = _mm_unpacklo_epi8(pixX, zeroX); // b16, g16, r16, x16,   0,   0,   0,   0
			pixX = _mm_unpacklo_epi16(pixX, zeroX); // b32,      g32,        r32,      x32
			sumX = _mm_add_epi32(sumX, _mm_mullo_epi32(pixX, wgtX)); // b32*wgt,  g32*wgt,    r32*wgt,  x32 * wgt
#else
			sum[0] += (int)(pix & 0xff) * weight;
			pix >>= 8;
			sum[1] += (int)(pix & 0xff) * weight;
			pix >>= 8;
			sum[2] += (int)(pix & 0xff) * weight;
#endif
		}
	}

#ifdef USE_SSE2
	// Get absolute value
	sumX = _mm_srli_epi32(sumX, 8); // sum >> 8  (either logical or arithmatic as the values should be positive)
	sumX = _mm_packus_epi32(sumX, zeroX);
	sumX = _mm_packus_epi16(sumX, zeroX);
	return sumX.m128i_i32[0] & 0x00ffffff;
#else
	Color32Bit pix;
	pix.chan.r = sum[2] >> 8;
	pix.chan.g = sum[1] >> 8;
	pix.chan.b = sum[0] >> 8;
	return pix.dw;
#endif
}

} // namespace KEP
