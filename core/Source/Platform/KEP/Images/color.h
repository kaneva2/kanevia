///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifndef SSE2
#pragma message("Warning: SSE2 not defined for " __FILE__)
#endif

union R5G6B5 {
	struct {
		unsigned short r : 5;
		unsigned short g : 6;
		unsigned short b : 5;
	};
	unsigned short color;
};

union R8G8B8X8 {
	struct {
		unsigned char r;
		unsigned char g;
		unsigned char b;
		unsigned char x;
	};
	struct {
		unsigned char c[4];
	};
	unsigned color;
};

// Convert 32-bit color to 16-bit
inline unsigned short to565(unsigned c888) {
	R8G8B8X8 *p888 = (R8G8B8X8 *)&c888;
	R5G6B5 c565;
	c565.r = p888->r >> 3;
	c565.g = p888->g >> 2;
	c565.b = p888->b >> 3;
	return c565.color;
}

// Convert 16-bit color to 32-bit
inline unsigned to888(unsigned short c565, unsigned char x) {
	R5G6B5 *p565 = (R5G6B5 *)&c565;
	R8G8B8X8 c888;
	c888.r = p565->r << 3;
	c888.g = p565->g << 2;
	c888.b = p565->b << 3;
	c888.x = x;
	return c888.color;
}

inline unsigned colorDiff(unsigned clr1, unsigned clr2) {
#ifdef SSE2
	__m128i m1 = _mm_cvtsi32_si128(clr1);
	__m128i m2 = _mm_cvtsi32_si128(clr2);
	__m128i pdiff = _mm_sad_epu8(m1, m2);
	return pdiff.m128i_u32[0];
#else
	auto p1 = (unsigned char *)&clr1;
	auto p2 = (unsigned char *)&clr2;
	unsigned diff = 0;
	for (size_t i = 0; i < 4; i++) {
		diff += abs((int)p1[i] - (int)p2[i]);
	}

	return diff;
#endif
}

inline unsigned colorLerpIntCoeff(unsigned clr1, unsigned clr2, unsigned char w1, unsigned s) {
	unsigned char w2 = (unsigned char)((1 << s) - w1);
#ifdef SSE2
	__m128i zero, clr1X, wgt1X, mul1X, clr2X, wgt2X, mul2X, sumX, shfX;
	zero = _mm_setzero_si128(); // 0
	clr1X = _mm_cvtsi32_si128(clr1);
	clr1X = _mm_unpacklo_epi8(clr1X, zero); // clr1.r16, clr1.g16, clr1.b16, clr1.x16,       0,        0,        0,        0
	wgt1X = _mm_set1_epi16(w1); //  wgt1_16,  wgt1_16,  wgt1_16,  wgt1_16, wgt1_16,  wgt1_16,  wgt1_16,  wgt1_16
	mul1X = _mm_mullo_epi16(clr1X, wgt1X); // clr1.r*wgt1 (16-bit), clr1.g*wgt1 (16-bit), clr1.b*wgt1 (16-bit), ...
	clr2X = _mm_cvtsi32_si128(clr2);
	clr2X = _mm_unpacklo_epi8(clr2X, zero); // clr2.r16, clr2.g16, clr2.b16, clr2.x16,       0,        0,        0,        0
	wgt2X = _mm_set1_epi16(w2); //  wgt2_16,  wgt2_16,  wgt2_16,  wgt2_16,  wgt2_16,  wgt2_16,  wgt2_16,  wgt2_16
	mul2X = _mm_mullo_epi16(clr2X, wgt2X); // clr2.r*wgt2 (16-bit), clr2.g*wgt2 (16-bit), clr2.b*wgt2 (16-bit), ...
	sumX = _mm_add_epi32(mul1X, mul2X); // clr1.r*wgt1 (16-bit), clr2.r*wgt2 (16-bit), ...
	shfX = _mm_srli_epi16(sumX, s); // (clr1.r*wgt1+clr2.r*wgt2)>>8 (8-bit), 0, (clr2.g*wgt1+clr2.g*wgt2)>>8, 0...
	__m128i resX = _mm_packus_epi16(shfX, zero); // (clr1.r*wgt1+clr2.r*wgt2)>>8 (8-bit), (clr2.g*wgt1+clr2.g*wgt2)>>8, ...
	return resX.m128i_u32[0];
#else
	auto p1 = (unsigned char *)&clr1;
	auto p2 = (unsigned char *)&clr2;
	unsigned out = 0;
	auto pOut = (unsigned char *)&out;
	for (size_t i = 0; i < 4; i++) {
		pOut[i] = (unsigned char)(((p1[i] * w1) + (p2[i] * w2)) >> s);
	}
	return out;
#endif
}

inline unsigned getBestColorIndex(unsigned pixel, unsigned palette[4], size_t palSize, unsigned &minDiff) {
	minDiff = std::numeric_limits<unsigned>::max();
	size_t minIndex = 0;

#ifdef SSE2
	__m128i pal128, pix128, dif128, dif128b;
	unsigned diff[4];
	pal128 = _mm_set_epi32(0, palette[2], 0, palette[0]); // LSB - B0  G0  R0  A0  00  00  00  00  B2  G2  R2  A2  00  00  00  00 - MSB
	pix128 = _mm_set_epi32(0, pixel, 0, pixel); // LSB - Bp  Gp  Rp  Ap  00  00  00  00  Bp  Gp  Rp  Ap  00  00  00  00 - MSB
	dif128 = _mm_sad_epu8(pal128, pix128); // LSB - |Bp-B0| + |Gp-G0| + |Rp-R0| + |Ap-A0|, 00000000, |Bp-B2| + |Gp-G2| + |Rb-R2| + |Ab-A2|, 00000000 - MSB
	pal128 = _mm_set_epi32(0, palette[3], 0, palette[1]); // LSB - B2  G2  R2  A2  00  00  00  00  B3  G3  R3  A3  00  00  00  00 - MSB
	dif128b = _mm_sad_epu8(pal128, pix128); // LSB - |Bp-B1| + |Gp-G1| + |Rp-R1| + |Ap-A1|, 00000000, |Bp-B3| + |Gp-G3| + |Rb-R3| + |Ab-A3|, 00000000 - MSB
	dif128b = _mm_shuffle_epi32(dif128b, _MM_SHUFFLE(2, 3, 0, 1)); // LSB - 00000000, DIF(pix,pal1), 00000000, DIF(pix,pal3) - MSB
	dif128 = _mm_or_si128(dif128, dif128b); // LSB - DIF(pix, pal0), DIF(pix, pal1), DIF(pix, pal2), DIF(pix, pal3) - MSB
	_mm_storeu_si128((__m128i *)&diff, dif128);

	for (size_t i = 0; i < palSize; i++) {
		if (minDiff > diff[i]) {
			minDiff = diff[i];
			minIndex = i;
		}
	}
#else
	for (int i = 0; i < 4; i++) {
		unsigned cand = palette[i];
		unsigned diff = colorDiff(pixel, cand);
		if (diff < minDiff) {
			minDiff = diff;
			minIndex = i;
		}
	}
#endif

	return minIndex;
}
