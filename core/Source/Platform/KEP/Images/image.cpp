///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "dds.h"
#include "KEP/Images/KEPImages.h"
#include "FreeImage.h"
#include "d3d9types.h"
#include <memory>
#include <assert.h>

namespace KEP {

static unsigned getCompressionCode(FREE_IMAGE_FORMAT format, FIBITMAP *dib);
static ImageOpacity getOpacity(FREE_IMAGE_FORMAT format, FIBITMAP *dib);

unsigned char *decodeImage(unsigned char *rawBuffer, unsigned rawBufSize, ImageInfo &info, unsigned &pixBufSize) {
	// Out buffer
	std::unique_ptr<unsigned char> outBuffer;
	pixBufSize = 0;

	// Create a memory stream
	FIMEMORY *stream = FreeImage_OpenMemory(rawBuffer, rawBufSize);

	// Identify the file format
	FREE_IMAGE_FORMAT format = FreeImage_GetFileTypeFromMemory(stream, 0);

	assert(format == FIF_JPEG || format == FIF_PNG);

	// Load image from the memory stream
	FIBITMAP *bitmap = FreeImage_LoadFromMemory(format, stream, 0);
	assert(bitmap != nullptr);

	if (bitmap != nullptr) {
		info.width = FreeImage_GetWidth(bitmap);
		info.height = FreeImage_GetHeight(bitmap);
		info.depth = FreeImage_GetBPP(bitmap);
		info.compression = getCompressionCode(format, bitmap);
		info.numSubImages = 0;
		info.opacity = getOpacity(format, bitmap);
		info.pixelFormat = info.opacity == ImageOpacity::FULLY_OPAQUE ? D3DFMT_X8R8G8B8 : D3DFMT_A8R8G8B8;

		// Allocate memory and copy data
		unsigned pitch = info.width * 4;

		pixBufSize = pitch * info.height;
		std::unique_ptr<unsigned char> tmp(new unsigned char[pixBufSize]);
		outBuffer.swap(tmp);
		assert(outBuffer);

		if (outBuffer) {
			// Convert to 32-bit pixels with top-down scanline order
			FreeImage_ConvertToRawBits(outBuffer.get(), bitmap, pitch, 32, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);
		}

		// Cleanup
		FreeImage_Unload(bitmap); // Dispose bitmap
	}

	// Cleanup
	FreeImage_CloseMemory(stream); // Always close the memory stream

	// Return buffer (if allocated) or null
	return outBuffer.release();
}

unsigned getCompressionCode(FREE_IMAGE_FORMAT format, FIBITMAP *dib) {
	// For informational and statistics purposes
	switch (format) {
		case FIF_JPEG:
			return FOURCC("JPEG");
		case FIF_PNG:
			return FOURCC("PNG\0");
		case FIF_DDS:
			assert(false);
			return FOURCC("DDS ");
		case FIF_GIF:
			return FOURCC("GIF\0");
		case FIF_JP2:
			return FOURCC("JP2K");
		case FIF_TIFF:
			return FOURCC("TIFF");
		case FIF_TARGA:
			return FOURCC("TGA\0");
		case FIF_UNKNOWN:
			return FCC_ZERO;
		default:
			// Other FreeImage supported trivial format.
			return 0xFFFF0000 + format;
	}
}

ImageOpacity getOpacity(FREE_IMAGE_FORMAT format, FIBITMAP *dib) {
	switch (format) {
		case FIF_JPEG:
			return ImageOpacity::FULLY_OPAQUE;
		default:
			return FreeImage_IsTransparent(dib) ? ImageOpacity::TRANSLUCENT : ImageOpacity::FULLY_OPAQUE;
	}
}

} // namespace KEP
