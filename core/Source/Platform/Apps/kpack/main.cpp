///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/kpack.h"
#include "Core/Util/Unicode.h"
#include "wildcard.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace KEP;

void printUsage() {
	std::cout << "Pack   : kpack -p <pak-file> <file1> [file2 [...]]" << std::endl;
	std::cout << "Unpack : kpack -u <pak-file> [<out-dir>]" << std::endl;
}

int pack(std::string pakFilePath, std::vector<std::string> filePatterns) {
	std::ofstream out(pakFilePath, std::ios::binary | std::ios::out | std::ios::trunc);
	if (!out.good()) {
		std::cerr << "Error creating pak file '" << pakFilePath << "' for write - error: " << errno << std::endl;
		return 2;
	}

	std::vector<std::string> filesToPack;
	for (std::string filePattern : filePatterns) {
		Wildcard::IterateFile(filePattern, [&filesToPack](const filesystem::path& path) {
			filesToPack.push_back(path.u8string());
			return true;
		});
	}

	try {
		size_t count = 0;
		KPack::PackFiles(pakFilePath, filesToPack, [&count](const std::string& filePath) {
			std::cout << "[" << filePath << "]" << std::endl;
			count++;
		});

		std::cout << "=== " << count << " file(s) packed ===" << std::endl;
	}
	catch (const std::exception& exception) {
		std::cerr << "Error packing files into '" << pakFilePath << "' - " << exception.what() << std::endl;
		return 1;
	}

	return 0;
}

int unpack(std::string pakFilePath, std::string outDir) {
	std::ofstream in(pakFilePath, std::ios::binary | std::ios::in);
	if (!in.good()) {
		std::cerr << "Error opening pak file '" << pakFilePath << "' for read - error: " << errno << std::endl;
		return 2;
	}

	try {
		size_t numFilesUnpacked;
		KPack::UnpackFiles(pakFilePath, outDir, numFilesUnpacked);
		std::cout << "Succeeded: " << numFilesUnpacked << " file(s) unpacked" << std::endl;
	}
	catch (const std::exception& exception) {
		std::cerr << "Error un-packing '" << pakFilePath << "' - " << exception.what() << std::endl;
		return 1;
	}

	return 0;
}

int wmain(int argc, wchar_t** argv)
{
	std::string commandSwitch;
	std::string pakFilePath;

	if (argc >= 3) {
		commandSwitch = KEP::Utf16ToUtf8(argv[1]);
		pakFilePath = KEP::Utf16ToUtf8(argv[2]);
	}

	if (commandSwitch == "-p" && argc >= 4) {
		std::vector<std::string> filePatterns;
		for (int i = 3; i < argc; i++) {
			filePatterns.push_back(KEP::Utf16ToUtf8(argv[i]));
		}
		return pack(pakFilePath, filePatterns);
	} 

	if (commandSwitch == "-u" && argc <= 4) {
		std::string outDir;
		if (argc == 4) {
			outDir = KEP::Utf16ToUtf8(argv[3]);
		}
		return unpack(pakFilePath, outDir);
	}

	printUsage();
	return 1;
}
