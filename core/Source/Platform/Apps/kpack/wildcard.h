#pragma once

#include <string>
#include <regex>
#include <boost/algorithm/string.hpp>
#include <filesystem>

namespace filesystem = std::experimental::filesystem::v1;

namespace KEP {

class Wildcard {
public:
	static bool IsWildcard(const std::string& pattern) {
		return pattern.find_first_of("*?") != std::string::npos;
	}

	// Wildcard match with * and ? support
	static bool Match(const std::string& text, const std::string& pattern) {
		return std::regex_match(text, ConvertPatternToRegex(pattern));
	}

	// Wildcard directory search with **, * and ? support
	static void IterateFile(const std::string& pattern, const std::function<void(const filesystem::path&)>& callback) {
		filesystem::path pathPattern = filesystem::u8path(pattern);
		filesystem::path rootPath = pathPattern.root_path();
		if (rootPath.u8string().empty() && pattern.find_first_of("/\\") == std::string::npos) {
			// Workaround for directory_iterator not handling empty path issue
			rootPath = filesystem::path(".");
		}
		IterateFileIn(rootPath, pathPattern.relative_path().u8string(), callback);
	}

private:
	static std::regex ConvertPatternToRegex(const std::string& wildcardPattern) {
		std::string regexPattern = wildcardPattern;

		// Escape (\ -> \\, . -> \.)
		boost::replace_all(regexPattern, "\\", "\\\\");
		boost::replace_all(regexPattern, ".", "\\.");

		// Convert (* -> [^\/]* , ? -> [^\/])
		boost::replace_all(regexPattern, "*", "[^\\/]*");
		boost::replace_all(regexPattern, "?", "[^\\/]");

		return std::regex(regexPattern);
	}

	static void IterateFileIn(const filesystem::path& parentPath, const std::string& pattern, const std::function<void(const filesystem::path&)> & callback) {
		int ofs = (int) pattern.find_first_of("*?");
		if (ofs == std::string::npos) {
			// concrete path
			callback(parentPath / pattern);
			return;
		}

		for (; ofs >= 0; ofs--) {
			if (pattern[ofs] == '/' || pattern[ofs] == '\\') {
				break;
			}
		}

		filesystem::path currentDirectory(parentPath / pattern.substr(0, ofs + 1));
	
		std::string remainingPattern;
		std::string currentPattern = pattern.substr(ofs + 1);;
		bool searchFile = true;

		size_t ofsNext = currentPattern.find_first_of("/\\");
		if (ofsNext != std::string::npos) {
			remainingPattern = currentPattern.substr(ofsNext + 1);
			currentPattern = currentPattern.substr(0, ofsNext);
			searchFile = false;
		}

		if (currentPattern == "**") {
			// Current directory
			IterateFileIn(currentDirectory, remainingPattern, callback);

			// Recursive sub-directory search
			for (auto entry : filesystem::recursive_directory_iterator(currentDirectory)) {
				bool isDirectory = filesystem::is_directory(entry);
				if (isDirectory) {
					IterateFileIn(entry.path(), remainingPattern, callback);
				}
			}
			return;
		}

		// Wildcard search
		std::regex regex(ConvertPatternToRegex(currentPattern));

		for (auto entry : filesystem::directory_iterator(currentDirectory)) {
			bool isDirectory = filesystem::is_directory(entry);
			if (searchFile && isDirectory || !searchFile && !isDirectory) {
				// Skip
				continue;
			}

			if (!std::regex_match(entry.path().filename().u8string(), regex)) {
				continue;
			}

			if (searchFile) {
				callback(entry.path());
			}
			else {
				IterateFileIn(entry.path(), remainingPattern, callback);
			}
		}
	}
};

}
