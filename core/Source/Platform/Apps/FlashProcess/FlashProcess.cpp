///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "FlashProcess.h"
#include "Common/include/CoreStatic/FlashControlServer.h"
#include "Common/include/CoreStatic/WindowsMixer.h"
#include "Common/include/LogHelper.h"
#include "Common/KEPUtil/WebCall.h"
#include "Common/KEPUtil/CrashReporter.h"
#include "Common/KEPUtil/RuntimeReporter.h"
#include "Common/include/KEPStar.h"
#include "Common/include/LogHelper.h"

#include <Shellapi.h>
#include <vector>
#include <regex>

namespace KEP {

static LogInstance("Instance");
static const char* kpszWindowsMixerLabel = K_CLIENT_MEDIA_NAME;

static std::wstring GetExecutablePathName() {
	wchar_t achFileName[2 * MAX_PATH + 1] = { 0 };
	size_t iBufferSize = std::extent<decltype(achFileName)>::value;
	DWORD nChar = GetModuleFileNameW(NULL, achFileName, iBufferSize);
	(void)nChar;
	achFileName[iBufferSize - 1] = 0;
	return achFileName;
}

// 3296, 3298, 5316
static std::string GetStarString() {
	std::regex regex("(3296|3298|5316)[/\\\\]+[^/\\\\]*$");
	std::string strExe = Utf16ToUtf8(GetExecutablePathName());
	std::string strCurDir = FileHelper::GetCurrentDirectory();
	strCurDir += '/'; // Make sure directory ends in a path separator.
	for (const std::string& strTest : { strExe, strCurDir }) {
		std::smatch mr;
		if (std::regex_search(strTest, mr, regex)) {
			std::string strSubMatch = mr[1].str(); // Index 1 is the first parenthesized subexpression.
			return strSubMatch;
		}
	}
	return std::string();
}

// Standard says that argv[argc] is a null pointer, so we add nullptr to argv vector. End result is that argc is argv.size()-1.
static void GetArgV(wchar_t* pCmdLine, Out<std::vector<wchar_t*>> argv, Out<std::vector<wchar_t>> storage) {
	std::wstring wstrExePath = GetExecutablePathName();
	int nArgs = 0;
	wchar_t** args = nullptr;
	if (pCmdLine && *pCmdLine != 0)
		args = CommandLineToArgvW(pCmdLine, &nArgs);

	std::vector<size_t> aOffsets; // Offsets into storage for each argument.

	// Executable name
	aOffsets.push_back(0);
	storage.get().assign(wstrExePath.c_str(), wstrExePath.c_str() + wstrExePath.size() + 1);

	// Arguments
	std::vector<wchar_t*> achArgv;
	for (int i = 0; i < nArgs; ++i) {
		aOffsets.push_back(storage.get().size());
		int j = 0;
		while (true) {
			char ch = args[i][j++];
			storage.get().push_back(ch);
			if (ch == 0)
				break;
		}
	}
	LocalFree(args);

	// Construct argv pointers from offsets.
	for (size_t offset : aOffsets)
		argv.get().push_back(storage.get().data() + offset);
	argv.get().push_back(nullptr);
}

///////////////////////////////////////////////////////////////////////////////
// RuntimeReporter Support
///////////////////////////////////////////////////////////////////////////////

static void RR_AppCallback(RuntimeReporter::CallbackStruct& cbs) {
	// Nothing To Add
}

static void RR_Install(const std::string& strStar) {
	if (strStar.empty()) {
		LogError("No star path. Not enabling runtime reporting.");
		return;
	}
	LogInfo("Using star " << strStar);
	RuntimeReporter::Install(K_CLIENT_MEDIA_NAME, "Status", StarWokUrl(strStar) + "runtimeReport.aspx?", RR_AppCallback);
}

///////////////////////////////////////////////////////////////////////////////
// CrashReporter Support
///////////////////////////////////////////////////////////////////////////////

static void CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	// Identify As 'Flash Crash' If No Known Reason
	ErrorData* pED = CrashReporter::GetErrorData();
	bool noReason = (pED && pED->reason.empty());
	if (noReason) {
		pED->reason = "Flash Crash";
		pED->type = ErrorData::TYPE_CRASH;
	}

	// Set Runtime Reporter State
	if (pED && pED->type != ErrorData::TYPE_CRASH)
		RuntimeReporter::ProfileStateStopped();
	else
		RuntimeReporter::ProfileStateCrashed();

	// Wait For Runtime Report Response Complete
	RuntimeReporter::WaitForReportResponseComplete();
}

static bool CR_Install() {
	// Install Crash Reporter (default version)
	bool ok = CrashReporter::Install(K_CLIENT_MEDIA_NAME, "", CR_AppCallback);

	// Add Crash Report Attachments
	CrashReporter::AddFile(PathAdd(PathApp(), "*.log*"), "Log");
	CrashReporter::AddFile(PathAdd(PathApp(), "..\\..\\*.log*"), "Launcher Log");
	CrashReporter::AddRegistryKey("HKEY_CURRENT_USER\\Software\\" K_NAME);

	return ok;
}

} // namespace KEP

///////////////////////////////////////////////////////////////////////////////
// This is the entry point if compiled as a Windows application
///////////////////////////////////////////////////////////////////////////////

using namespace KEP;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR lpCmdLine,
	_In_ int nCmdShow) {
	// Start Crash Reporter (must be before creating any threads)
	CR_Install();

	// Start Logger (creates a thread)
	LogInit(K_CLIENT_MEDIA_LOG_CFG);
	LogInfo("====================================================================");
	LogInfo("===== " << K_CLIENT_MEDIA_EXE);
	LogInfo("====================================================================");

	// Start Runtime Reporter
	RR_Install(GetStarString());

	// Send Runtime Report (running)
	RuntimeReporter::ProfileStateRunning();

	// Build argv
	std::vector<wchar_t> aArgvStorage;
	std::vector<wchar_t*> aArgv;
	GetArgV(lpCmdLine, out(aArgv), out(aArgvStorage));

	// Configure volume control
	// Volume control won't work until we initialize COM.
	HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
	if (hr != S_OK && hr != S_FALSE && hr != RPC_E_CHANGED_MODE) // COM is initialized with any of these codes.
		LogError("Could not initialize COM. Return code: " << hr);

		// If we don't set icon and label, the mixer gets it from the executable's icon resource
		// and the file description stored in the version resource.
#if 0
	std::shared_ptr<IWindowsMixerProxy> pWindowsMixerProxy = IWindowsMixerProxy::New();
	if( pWindowsMixerProxy ) {
		pWindowsMixerProxy->SetLabel(kpszWindowsMixerLabel);
		if( !aArgv.empty() ) {
			std::string strIconPath = Utf16ToUtf8(aArgv[0]);
			pWindowsMixerProxy->SetIconPath(strIconPath.c_str());
		}
	} else {
		LogWarn("Could not create interface to Windows mixer.");
	}
#endif

	// Start flash process.
	int ret = FlashControl::FlashProcessMain(aArgv.size() - 1, aArgv.data());

	// Send Runtime Report (stopped)
	RuntimeReporter::ProfileStateStopped();
	RuntimeReporter::WaitForReportResponseComplete();

	// Wait Until All Web Calls Complete
	WebCaller::DisableInstances();

	return ret;
}
