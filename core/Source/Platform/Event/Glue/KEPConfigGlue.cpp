///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"
#include "KEPHelpers.h"
#include "IKEPConfig.h"

using namespace KEP;

SG_BEGIN_FN(GetString, TWO_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IKEPConfig, f, popObj);
const char* valueName;
const char* defaultValue;
SG_POPARGS("Oss", &popObj, &valueName, &defaultValue);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
std::string value;
bool ret = f->GetValue(valueName, value, defaultValue);
SG_PUSHARGS("ds", (ret ? 1.0 : 0.0), value.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetNumber, TWO_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IKEPConfig, f, popObj);
const char* valueName;
double defaultValue;
SG_POPARGS("Osd", &popObj, &valueName, &defaultValue);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
std::string strValue;
char s[128] = "0";
_snprintf_s(s, _countof(s), _TRUNCATE, "%f", defaultValue);
bool ret = f->GetValue(valueName, strValue, s);
double value = atof(strValue.c_str());
SG_PUSHARGS("dd", (ret ? 1.0 : 0.0), (double)value);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(SetString, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IKEPConfig, f, popObj);
const char* value;
const char* valueName;
SG_POPARGS("Oss", &popObj, &valueName, &value);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
f->SetValue(valueName, value);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(SetNumber, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IKEPConfig, f, popObj);
double value;
const char* valueName;
SG_POPARGS("Osd", &popObj, &valueName, &value);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
std::string errMsg;
char s[128] = "0";
_snprintf_s(s, _countof(s), _TRUNCATE, "%f", value);
f->SetValue(valueName, s);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(ValueExists, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IKEPConfig, f, popObj);
const char* valueName;
SG_POPARGS("Os", &popObj, &valueName);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
std::string errMsg;
bool ret = f->ValueExists(valueName);
SG_PUSHARGS("d", (ret ? 1.0 : 0.0));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Save, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IKEPConfig, f, popObj);
SG_POPARGS("O", &popObj);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
f->Save();
// KEPConfig is cached now and deleted when ClientEngine is destroyed.
SG_RETURN;
SG_END_FN

SG_VM_REGISTER(KEPConfigVMRegister)
SG_BEGIN_SG_REGISTER(KEPConfig)
SG_REGISTER(GetString, GetString)
SG_REGISTER(GetNumber, GetNumber)
SG_REGISTER(SetString, SetString)
SG_REGISTER(SetNumber, SetNumber)
SG_REGISTER(Save, Save)
SG_REGISTER(ValueExists, ValueExists)
SG_END_REGISTER
SG_END_VM_REGISTER
