use strict;

# wrapper for generating all the getset code
my (@files,$xStaticPrefix,$coreStaticPrefix,$platformPrefix);

$xStaticPrefix  = "..\\..\\Common\\XStatic\\";
$coreStaticPrefix  = "..\\..\\Common\\include\\CoreStatic\\";
$platformPrefix = "..\\..\\";

@files = ( 
	$xStaticPrefix."CAIICollectionQuest.cpp",
	$xStaticPrefix."CAIclass.cpp",
	$xStaticPrefix."CCameraClass.cpp",
	$xStaticPrefix."CCommerceClass.cpp",
	$xStaticPrefix."CDayStateClass.cpp",
	$xStaticPrefix."CEnvironmentClass.cpp",
	$xStaticPrefix."CFrameObj.cpp", 
	$xStaticPrefix."CGlobalInventoryClass.cpp",
	$xStaticPrefix."CInventoryClass.cpp",
	$xStaticPrefix."CItemClass.cpp",
	$xStaticPrefix."CMovementObj.cpp",
	$xStaticPrefix."CMultiplayerClass.cpp",
	$xStaticPrefix."CPhysicsEmuClass.cpp",
	$xStaticPrefix."CPlayerClass.cpp",
	$xStaticPrefix."CQuestJournal.cpp",
	$xStaticPrefix."CServerItemGen.cpp",
	$xStaticPrefix."CServerItemClass.cpp",
	$xStaticPrefix."RuntimeSkeleton.cpp",
	$xStaticPrefix."CSkillObject.cpp",
	$xStaticPrefix."CSpawnClass.cpp",
	$xStaticPrefix."CStatClass.cpp",
	$xStaticPrefix."CStormClass.cpp",
	$xStaticPrefix."CTimeSystem.cpp",
	$coreStaticPrefix."CTitleObject.h",
	$xStaticPrefix."CWldObject.cpp",
	$xStaticPrefix."CWorldAvailableClass.cpp",
	$xStaticPrefix."StreamableDynamicObj.cpp",
	$xStaticPrefix."SoundPlacement.cpp",
	$xStaticPrefix."LightweightGeom.cpp",
	$xStaticPrefix."EnvParticleSystem.cpp",
	$platformPrefix."common\\src\\Game.cpp",
	$platformPrefix."Editor\\KepEditView.cpp",
	$platformPrefix."Editor\\Distribution.cpp",
	$platformPrefix."Server\\ServerEngine\\ServerEngine.cpp",
	$platformPrefix."Client\\ClientEngine.cpp",
	$platformPrefix."UGC\\DynamicObjectImport\\DynamicObjectImport.cpp",
	$platformPrefix."UGC\\TextureImport\\UGCTextureImport.cpp",
	$platformPrefix."UGC\\AnimationImport\\UGCCharAnimImport.cpp",
	$platformPrefix."UGC\\EquippableObjectImport\\UGCEquippableObjectImport.cpp",
	$platformPrefix."UGC\\UGCPublish\\AppEditing.cpp"
);

foreach (@files)
{
	`getsetscript.pl $_`;
}
