///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Event/Base/IArena.h"
#include "IGetSet.h"
#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"

using namespace KEP;

SG_BEGIN_FN(Arena_GetSpawnPointCount, ONE_RETURN_VALUE)

SG_DECLARE_POP_OBJ(IArena, arena, sgArena);

if (SG_POPARGS("O", &sgArena)) {
	if (SG_CAST_TO_DERIVED_OBJECT(arena, IArena, IGetSet, sgArena)) {
		SG_PUSH((double)arena->GetSpawnPointCount());
		SG_RETURN;
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_OBJECT));
	}
}
SG_RETURN_ERR;

SG_END_FN

// get spanwn pt given an index
SG_BEGIN_FN(Arena_GetSpawnPoint, THREE_RETURN_VALUES)

SG_DECLARE_POP_OBJ(IArena, arena, sgArena);
double index = -1;

if (SG_POPARGS("Od", &sgArena, &index)) {
	if (SG_CAST_TO_DERIVED_OBJECT(arena, IArena, IGetSet, sgArena)) {
		Vector3f v = arena->GetSpawnPoint((int)index);
		SG_PUSHARGS("ddd", (double)v.x, (double)v.y, (double)v.z);
		if (__retArgs)
			SG_RETURN;
		else
			SG_RETURN_ERR;
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_OBJECT));
	}
}
SG_RETURN_ERR;
SG_END_FN

SG_VM_REGISTER(ArenaVMRegister)
SG_BEGIN_SG_REGISTER(Arena)
SG_REGISTER(GetSpawnPointCount, Arena_GetSpawnPointCount)
SG_REGISTER(GetSpawnPoint, Arena_GetSpawnPoint)
SG_END_REGISTER
SG_END_VM_REGISTER
