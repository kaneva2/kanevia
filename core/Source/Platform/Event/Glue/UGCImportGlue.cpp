///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "config.h"

#include <set>
#include <js.h>
#include "KEPHelpers.h"
#include "IGetSet.h"
#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"
#include "Common/include/IClientEngine.h"
#include "UGC/DynamicObjectImport/DynamicObjectImport.h"
#include "UGC/DynamicObjectImport/DynamicObjectCreate.h"
#include "UGC/DynamicObjectImport/DynamicObjectProperties.h"
#include "UGC/UGCImportStateObserver.h"
#include "UGC/UGCImportStateMachine.h"
#include "UGC/UGCImport.h"
#include "UGC/UGCManager.h"

using namespace KEP;

static unsigned int s_sessionId = 0;
static std::map<unsigned int, IUGCImportStateObj*> s_activeImportStates;

static unsigned int insertActiveImportState(IUGCImportStateObj* pImpStateObj) {
	s_sessionId++;
	s_activeImportStates.insert(std::make_pair(s_sessionId, pImpStateObj));
	return s_sessionId;
}

static IUGCImportStateObj* getActiveImportState(unsigned int sessionId) {
	auto itr = s_activeImportStates.find(sessionId);
	return (itr == s_activeImportStates.end()) ? nullptr : itr->second;
}

static IUGCImportStateObj* eraseActiveImportState(unsigned int sessionId) {
	auto itr = s_activeImportStates.find(sessionId);
	IUGCImportStateObj* pImpStateObj = (itr == s_activeImportStates.end()) ? nullptr : itr->second;
	if (pImpStateObj)
		s_activeImportStates.erase(itr);
	return pImpStateObj;
}

SG_BEGIN_FN(UGC_CreateImportSession, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double importType;
SG_POPARGS("d", &importType);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
UGCImporter* pImp = pMgr->GetImporterByType((UGC_TYPE)(int)importType);
IUGCImportStateObj* pImpStateObj = pImp->beginSession();
double sessionId = insertActiveImportState(pImpStateObj);
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(pImpStateObj->Values(), IGetSet, sgImpStateObj);
SG_PUSHARGS("dO", sessionId, sgImpStateObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_GetImportSession, ONE_RETURN_VALUE)
double sessionId;
SG_POPARGS("d", &sessionId);
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(pImpStateObj->Values(), IGetSet, sgImpStateObj);
SG_PUSHARGS("O", sgImpStateObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_GetErrorCount, ONE_RETURN_VALUE)
double sessionId;
SG_POPARGS("d", &sessionId);
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
SG_PUSH((double)pImpStateObj->GetErrorCount());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_GetErrorInfo, FOUR_RETURN_VALUES)
double sessionId;
double violationId;
SG_POPARGS("dd", &sessionId, &violationId);
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
if ((size_t)violationId < 0 || (size_t)violationId >= pImpStateObj->GetErrorCount())
	SG_RETURN_ERR;
IUGCImportStateObj::ErrorInfo info;
if (!pImpStateObj->GetErrorInfo((size_t)violationId, info))
	SG_RETURN_ERR;
SG_PUSHARGS("sddd", info.message.c_str(), (double)info.recoveryAction, (double)info.errorId, (double)info.dupCount);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ClearErrors, NO_RETURN_VALUE)
double sessionId;
SG_POPARGS("d", &sessionId);
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
pImpStateObj->ClearErrors();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ReportError, NO_RETURN_VALUE)
double sessionId = 0;
double errorId = 0;
char* errorMessage = NULL;
double recoveryAction = 0;
double suppressDup = 0;
SG_POPARGS("ddsdd", &sessionId, &errorId, &errorMessage, &recoveryAction, &suppressDup);
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
pImpStateObj->ReportError((int)errorId, errorMessage, (int)recoveryAction, suppressDup != 0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_GetMissingTextureCount, ONE_RETURN_VALUE)
double sessionId;
SG_POPARGS("d", &sessionId);
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
ISOMaterial* pMaterialImpState = dynamic_cast<ISOMaterial*>(pImpStateObj);
if (!pMaterialImpState)
	SG_RETURN_ERR;
SG_PUSH((double)pMaterialImpState->m_missingTextures.size());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_GetMissingTexture, ONE_RETURN_VALUE)
double sessionId;
double missingTextureId;
SG_POPARGS("dd", &sessionId, &missingTextureId);
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
ISOMaterial* pMaterialImpState = dynamic_cast<ISOMaterial*>(pImpStateObj);
if (!pMaterialImpState)
	SG_RETURN_ERR;
if ((size_t)missingTextureId < 0 || (size_t)missingTextureId >= pMaterialImpState->m_missingTextures.size())
	SG_RETURN_ERR;
const char* desc = pMaterialImpState->m_missingTextures[(size_t)missingTextureId].c_str();
SG_PUSHARGS("s", desc);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_StartImportAsync, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double sessionId;
SG_POPARGS("d", &sessionId);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
pMgr->TickleImportState(pImpStateObj, EV_START);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ContinueImportAsync, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double sessionId;
SG_POPARGS("d", &sessionId);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
pMgr->TickleImportState(pImpStateObj, EV_CONT);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_CancelImportAsync, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double sessionId;
SG_POPARGS("d", &sessionId);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
pMgr->TickleImportState(pImpStateObj, EV_CANCEL);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ReconvertAsync, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double sessionId;
SG_POPARGS("d", &sessionId);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
auto pImpStateObj = getActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
pMgr->TickleImportState(pImpStateObj, EV_CONVERT);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_CleanupImportSessionAsync, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double sessionId;
SG_POPARGS("d", &sessionId);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
auto pImpStateObj = eraseActiveImportState(sessionId);
if (!pImpStateObj)
	SG_RETURN_ERR;
pMgr->TickleImportState(pImpStateObj, EV_DESTROY);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ClassifyImportType, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double dropType;
char* importUri;
double targetType;
double targetId;
double targetPosX;
double targetPosY;
double targetPosZ;
double targetNmlX;
double targetNmlY;
double targetNmlZ;
SG_POPARGS("dsdddddddd", &dropType, &importUri, &targetType, &targetId, &targetPosX, &targetPosY, &targetPosZ, &targetNmlX, &targetNmlY, &targetNmlZ);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;

UGC_TYPE importType;
importType = pMgr->ClassifyImportType((int)dropType, importUri, (IModelType)(int)targetType, (int)targetId, Vector3f(targetPosX, targetPosY, targetPosZ), Vector3f(targetNmlX, targetNmlY, targetNmlZ));

// Return category here as it's difficult to do bit-op in Lua
UGC_TYPE importTypeCat = (UGC_TYPE)(importType & UGC_TYPE_CATEMASK);
SG_PUSH((double)importTypeCat);
SG_PUSH((double)importType);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_BrowseFileByImportType, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double ugcType = 0;
SG_DECLARE_POP_OBJ(IEvent, ev, sgEvent);
SG_POPARGS("dO", &ugcType, &sgEvent);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
if (!SG_CAST_TO_DERIVED_OBJECT(ev, IEvent, IEvent, sgEvent))
	SG_RETURN_ERR;
if (!pMgr->BrowseFileByImportType((UGC_TYPE)(int)ugcType, ev))
	SG_RETURN_ERR;
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_AddImageToUGCObj, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double ugcType = 0;
double ugcObjId = 0;
double texType = 0;
char* texPath = 0;
double subId = 0;
SG_POPARGS("dddsd", &ugcType, &ugcObjId, &texType, &texPath, &subId);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr || !texPath)
	SG_RETURN_ERR;
pMgr->AddImageToUGCObj((int)ugcType, (long long)ugcObjId, (int)texType, texPath, (long long)subId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ClearUGCObjImagesByType, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double ugcType = 0;
double ugcObjId = 0;
double texType = 0;
SG_POPARGS("ddd", &ugcType, &ugcObjId, &texType);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
pMgr->ClearUGCObjImagesByType((int)ugcType, (long long)ugcObjId, (int)texType);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_SubmitUGCObj, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double ugcType = 0;
double ugcObjId = 0;
SG_DECLARE_POP_OBJ(IEvent, ev, sgEvent);
SG_POPARGS("ddO", &ugcType, &ugcObjId, &sgEvent);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
if (!SG_CAST_TO_DERIVED_OBJECT(ev, IEvent, IEvent, sgEvent))
	SG_RETURN_ERR;
bool ret = pMgr->SubmitUGCObj((int)ugcType, (long long)ugcObjId, ev);
SG_PUSH(ret ? 1 : 0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_UploadUGCObj, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
char* uploadId = NULL;
double ugcType = 0;
double ugcObjId = 0;
SG_DECLARE_POP_OBJ(IEvent, evProgress, sgEventProgress);
SG_DECLARE_POP_OBJ(IEvent, evCompletion, sgEventCompletion);
SG_POPARGS("sddOO", &uploadId, &ugcType, &ugcObjId, &sgEventProgress, &sgEventCompletion);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
if (!(SG_CAST_TO_DERIVED_OBJECT(evProgress, IEvent, IEvent, sgEventProgress)) || !(SG_CAST_TO_DERIVED_OBJECT(evCompletion, IEvent, IEvent, sgEventCompletion)))
	SG_RETURN_ERR;
if (!uploadId)
	SG_RETURN_ERR;
bool ret = pMgr->Upload(uploadId, (int)ugcType, (long long)ugcObjId, evProgress, evCompletion);
SG_PUSH(ret ? 1 : 0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_DeployUGCUpload, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
char* uploadId = NULL;
double ugcType = 0;
SG_DECLARE_POP_OBJ(IEvent, ev, sgEvent);
SG_POPARGS("sdO", &uploadId, &ugcType, &sgEvent);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
if (!SG_CAST_TO_DERIVED_OBJECT(ev, IEvent, IEvent, sgEvent))
	SG_RETURN_ERR;
bool ret = pMgr->DeployUGCUpload(uploadId, (int)ugcType, ev);
SG_PUSH(ret ? 1 : 0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ImportSoundFile, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
char* fileName = NULL;
SG_POPARGS("s", &fileName);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
int ret = pMgr->ImportSoundFile(fileName, true);
SG_PUSH((double)ret);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(UGC_ImportActionItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
char *fileName = NULL, *bundleGlids;
double defaultGlid, parentGlid;
SG_POPARGS("sdsd", &fileName, &defaultGlid, &bundleGlids, &parentGlid);
IUGCManager* pMgr = pICE->GetUGCManager();
if (!pMgr)
	SG_RETURN_ERR;
int ret = pMgr->ImportActionItem(fileName, (int)defaultGlid, bundleGlids, (int)parentGlid);
SG_PUSH((double)ret);
SG_RETURN;
SG_END_FN

SG_VM_REGISTER(UGCImportVMRegister)
SG_BEGIN_SG_REGISTER(UGC)
// Import specific
SG_REGISTER(ClassifyImportType, UGC_ClassifyImportType)
SG_REGISTER(BrowseFileByImportType, UGC_BrowseFileByImportType)
SG_REGISTER(CreateImportSession, UGC_CreateImportSession)
SG_REGISTER(GetImportSession, UGC_GetImportSession)
SG_REGISTER(StartImportAsync, UGC_StartImportAsync)
SG_REGISTER(ContinueImportAsync, UGC_ContinueImportAsync)
SG_REGISTER(CancelImportAsync, UGC_CancelImportAsync)
SG_REGISTER(ReconvertAsync, UGC_ReconvertAsync)
SG_REGISTER(CleanupImportSessionAsync, UGC_CleanupImportSessionAsync)
SG_REGISTER(AddImageToUGCObj, UGC_AddImageToUGCObj)
SG_REGISTER(ClearUGCObjImagesByType, UGC_ClearUGCObjImagesByType)
SG_REGISTER(SubmitUGCObj, UGC_SubmitUGCObj)
SG_REGISTER(UploadUGCObj, UGC_UploadUGCObj)
SG_REGISTER(DeployUGCUpload, UGC_DeployUGCUpload)
SG_REGISTER(ImportSoundFile, UGC_ImportSoundFile)
SG_REGISTER(ImportActionItem, UGC_ImportActionItem)
SG_REGISTER(GetMissingTextureCount, UGC_GetMissingTextureCount)
SG_REGISTER(GetMissingTexture, UGC_GetMissingTexture)
SG_REGISTER(GetErrorCount, UGC_GetErrorCount)
SG_REGISTER(GetErrorInfo, UGC_GetErrorInfo)
SG_REGISTER(ClearErrors, UGC_ClearErrors)
SG_REGISTER(ReportError, UGC_ReportError)
SG_END_REGISTER
SG_END_VM_REGISTER
