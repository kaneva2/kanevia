///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "common/include/KEPCommon.h"

#include "IGetSet.h"
#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"
#include "LogHelper.h"

static LogInstance("Instance");

using namespace KEP;

SG_BEGIN_FN(GetSet_FindIdByName, TWO_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
const char* name;
SG_POPARGS("Os", &popObj, &name);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (!name)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (name)");
EGetSetType type = 0;
double index = (double)igs->FindIdByName(name, &type);
SG_PUSHARGS("dd", index, type);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetArrayCount, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
SG_POPARGS("Od", &popObj, &index);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
double count = (double)igs->GetArrayCount((ULONG)index);
SG_PUSHARGS("d", count);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_AddNewNumber, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
const char* name;
double valDefault;
double flags;
SG_POPARGS("Osdd", &popObj, &name, &valDefault, &flags);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (!name)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (name)");
if (flags < 0)
	SG_RETURN_ERR_STR("Invalid 4th Parameter (flags < 0)");
double index = (double)igs->AddNewDoubleMember(name, valDefault, (ULONG)flags);
SG_PUSHARGS("d", index);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetNumber, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
SG_POPARGS("Od", &popObj, &index);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
double value = igs->GetNumber((ULONG)index);
SG_PUSHARGS("d", value);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_SetNumber, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
double val;
SG_POPARGS("Odd", &popObj, &index, &val);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index != -1 && index < 0) // -1 = clear array special case
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
igs->SetNumber((ULONG)index, val);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetNumberByName, TWO_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
const char* name;
double val;
SG_POPARGS("Osd", &popObj, &name, &val);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (!name)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (name)");
double value = val;
EGetSetType type = 0;
ULONG index = igs->FindIdByName(name, &type);
bool ok = GS_OK(index);
if (ok)
	value = igs->GetNumber(index);
else {
	LogError("GetNumber() FAILED - name=" << name);
}
SG_PUSHARGS("dd", value, ok ? 1.0 : 0.0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_SetNumberByName, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
const char* name;
double val;
SG_POPARGS("Osd", &popObj, &name, &val);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (!name)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (name)");
EGetSetType type = 0;
ULONG index = igs->FindIdByName(name, &type);
bool ok = GS_OK(index);
if (!ok) {
	index = igs->AddNewDoubleMember(name, 0, GS_FLAG_TRANSIENT);
	ok = GS_OK(index);
}
if (ok)
	igs->SetNumber(index, val);
else {
	LogError("SetNumber() FAILED - name=" << name);
}
SG_PUSHARGS("d", ok ? 1.0 : 0.0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetNumberInArray, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double indexOfArray, indexInArray;
SG_POPARGS("Odd", &popObj, &indexOfArray, &indexInArray);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (indexOfArray < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (indexOfArray < 0)");
if (indexInArray < 0)
	SG_RETURN_ERR_STR("Invalid 3rd Parameter (indexInArray < 0)");
void* ret = igs->GetObjectInArray((ULONG)indexOfArray, (ULONG)indexInArray);
double numRet = *(double*)&ret;
SG_PUSHARGS("d", numRet);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_AddNewString, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
const char* name;
const char* valDefault;
double flags;
SG_POPARGS("Ossd", &popObj, &name, &valDefault, &flags);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (!name)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (name)");
if (!valDefault)
	SG_RETURN_ERR_STR("Invalid 3rd Parameter (valDefault)");
if (flags < 0)
	SG_RETURN_ERR_STR("Invalid 4th Parameter (flags < 0)");
double index = (double)igs->AddNewStringMember(name, valDefault, (ULONG)flags);
SG_PUSHARGS("d", index);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetString, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
SG_POPARGS("Od", &popObj, &index);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
std::string value = igs->GetString((ULONG)index);
SG_PUSHARGS("s", value.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_SetString, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
const char* str;
SG_POPARGS("Ods", &popObj, &index, &str);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
if (!str)
	SG_RETURN_ERR_STR("Invalid 3rd Parameter (str)");
igs->SetString((ULONG)index, str);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetVector, THREE_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
SG_POPARGS("Od", &popObj, &index);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
Vector3f& v = igs->GetVector3f((ULONG)index);
SG_PUSHARGS("ddd", (double)v.x, (double)v.y, (double)v.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_SetVector, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
double x, y, z;
SG_POPARGS("Odddd", &popObj, &index, &x, &y, &z);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
Vector3f v = { (float)x, (float)y, (float)z };
igs->SetVector3f((ULONG)index, v);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetColor, FOUR_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
SG_POPARGS("Od", &popObj, &index);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
float r, g, b, a;
r = g = b = a = 0.0;
igs->GetColor((ULONG)index, &r, &g, &b, &a);
SG_PUSHARGS("dddd", (double)r, (double)g, (double)b, (double)a);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_SetColor, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
double r, g, b, a;
SG_POPARGS("Oddddd", &popObj, &index, &r, &g, &b, &a);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
igs->SetColor((ULONG)index, (float)r, (float)g, (float)b, (float)a);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetObject, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double index;
SG_POPARGS("Od", &popObj, &index);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (index < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (index < 0)");
igs = igs->GetObjectPtr((ULONG)index);
if (!igs) {
	SG_PUSHARGS("d", 0.0);
	SG_RETURN;
}
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(GetSet_GetObjectInArray, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double indexOfArray, indexInArray;
SG_POPARGS("Odd", &popObj, &indexOfArray, &indexInArray);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
if (indexOfArray < 0)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (indexOfArray < 0)");
if (indexInArray < 0)
	SG_RETURN_ERR_STR("Invalid 3rd Parameter (indexInArray < 0)");
igs = igs->GetObjectInArray((ULONG)indexOfArray, (ULONG)indexInArray);
if (!igs) {
	SG_PUSHARGS("d", (double)0.0);
	SG_RETURN;
}
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_VM_REGISTER(GetSetVMRegister)
SG_BEGIN_SG_REGISTER(GetSet)

SG_REGISTER(FindIdByName, GetSet_FindIdByName)
SG_REGISTER(GetArrayCount, GetSet_GetArrayCount)

SG_REGISTER(AddNewNumber, GetSet_AddNewNumber)
SG_REGISTER(GetNumber, GetSet_GetNumber)
SG_REGISTER(SetNumber, GetSet_SetNumber)
SG_REGISTER(GetNumberByName, GetSet_GetNumberByName)
SG_REGISTER(SetNumberByName, GetSet_SetNumberByName)
SG_REGISTER(GetNumberInArray, GetSet_GetNumberInArray)

SG_REGISTER(AddNewString, GetSet_AddNewString)
SG_REGISTER(GetString, GetSet_GetString)
SG_REGISTER(SetString, GetSet_SetString)

SG_REGISTER(GetVector, GetSet_GetVector)
SG_REGISTER(SetVector, GetSet_SetVector)

SG_REGISTER(GetColor, GetSet_GetColor)
SG_REGISTER(SetColor, GetSet_SetColor)

SG_REGISTER(GetObject, GetSet_GetObject)
SG_REGISTER(GetObjectInArray, GetSet_GetObjectInArray)

SG_END_REGISTER
SG_END_VM_REGISTER
