///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "Event/Base/IEncodingFactory.h"
#include "Event/Base/IEvent.h"
#include "Event/Base/IDispatcher.h"
#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"
#include "Event/Events/GenericEvent.h"
#include "Event/Events/GetFunctionEvent.h"
#include "Event/EventSystem/Dispatcher.h"
#include "IGetSet.h"
#include "KEPConstants.h"
#include "LogHelper.h"
#include <Core/Util/Stream.h>

static LogInstance("Instance");

using namespace KEP;

#define SG_NULL_DISPATCHER(pD) if (!pD) SG_RETURN_ERR_STR("NULL DISPATCHER");
#define SG_NULL_EVENT(pE) if (!pE) SG_RETURN_ERR_STR("NULL EVENT");
#define SG_NULL_DISPATCHER_EVENT(pD, pE) SG_NULL_DISPATCHER(pD) SG_NULL_EVENT(pE)

SG_BEGIN_FN(Dispatcher_MakeEvent, ONE_RETURN_VALUE)
const char* eventName;
SG_POP_OBJECT(IDispatcher, pD)
SG_POP(eventName);
SG_NULL_DISPATCHER(pD)
auto id = pD->GetEventId(eventName ? eventName : "");
if (id == BAD_EVENT_ID) {
	//	SG_RETURN_ERR_STR((std::string("Bad event name '") + NULL_CK(eventName) + "'"));
	LogError("BAD_EVENT_ID - '" << (eventName ? eventName : "") << "'");
	SG_PUSH_OBJECT(nullptr);
	SG_RETURN;
}
auto pE = pD->MakeEvent(id);
if (!pE) {
	//	SG_RETURN_ERR_STR((std::string("FAILED MakeEvent() '") + NULL_CK(eventName) + "'"));
	LogError("FAILED MakeEvent() - '" << (eventName ? eventName : "") << "'");
	SG_PUSH_OBJECT(nullptr);
	SG_RETURN;
}
SG_PUSH_OBJECT(pE);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_RegisterEvent, ONE_RETURN_VALUE)
const char* eventName;
double temp;
double v1, v2, v3, v4;
SG_DECLARE_POP_OBJ(IDispatcher, pD, gsDisp)
SG_POPARGS("Oddddsd", &gsDisp, &v1, &v2, &v3, &v4, &eventName, &temp);
SG_CAST_TO_OBJECT(pD, gsDisp);
SG_NULL_DISPATCHER(pD)
EVENT_PRIORITY priority = std::max(EP_LOW, std::min(EP_HIGH, (EVENT_PRIORITY)temp));
auto id = pD->RegisterEvent(NULL_CK(eventName), PackVersion((BYTE)v1, (BYTE)v2, (BYTE)v3, (BYTE)v4), GenericEvent::CreateEvent, 0, priority);
SG_PUSH(id == BAD_EVENT_ID ? 0.0 : 1.0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_RegisterSecureEvent, ONE_RETURN_VALUE)
const char* eventName;
double temp;
double v1, v2, v3, v4, src, dest;
SG_DECLARE_POP_OBJ(IDispatcher, pD, gsDisp)
SG_POPARGS("Oddddsddd", &gsDisp, &v1, &v2, &v3, &v4, &eventName, &temp, &src, &dest);
SG_CAST_TO_OBJECT(pD, gsDisp);
SG_NULL_DISPATCHER(pD)
EVENT_PRIORITY priority = std::max(EP_LOW, std::min(EP_HIGH, (EVENT_PRIORITY)temp));
auto id = pD->RegisterEvent(NULL_CK(eventName), PackVersion((BYTE)v1, (BYTE)v2, (BYTE)v3, (BYTE)v4), GenericEvent::CreateEvent, 0, priority);
SG_PUSH(id == BAD_EVENT_ID ? 0.0 : 1.0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_GetEventId, ONE_RETURN_VALUE)
const char* eventName;
SG_POP_OBJECT(IDispatcher, pD)
SG_POP(eventName);
SG_NULL_DISPATCHER(pD)
EVENT_ID id = pD->GetEventId(eventName ? eventName : "");
SG_PUSH((double)id);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_QueueEvent, NO_RETURN_VALUE)
SG_POP_OBJECT(IDispatcher, pD)
SG_POP_OBJECT(IEvent, pE)
SG_NULL_DISPATCHER_EVENT(pD, pE)
pD->QueueEvent(pE);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_QueueEventInFuture, NO_RETURN_VALUE)
double secsInFuture = 0;
SG_POP_OBJECT(IDispatcher, pD)
SG_POP_OBJECT(IEvent, pE)
SG_POP(secsInFuture)
SG_NULL_DISPATCHER_EVENT(pD, pE)
pD->QueueEventInFutureMs(pE, secsInFuture * MS_PER_SEC);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_QueueEventInFutureMS, NO_RETURN_VALUE)
double msecsInFuture = 0;
SG_POP_OBJECT(IDispatcher, pD)
SG_POP_OBJECT(IEvent, pE)
SG_POP(msecsInFuture)
SG_NULL_DISPATCHER_EVENT(pD, pE)
pD->QueueEventInFutureMs(pE, msecsInFuture);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_RescheduleEvent, ONE_RETURN_VALUE)
double secsInFuture = 0;
SG_POP_OBJECT(IDispatcher, pD)
SG_POP_OBJECT(IEvent, pE)
SG_POP(secsInFuture)
SG_NULL_DISPATCHER_EVENT(pD, pE)
bool ret = pD->RescheduleEventInFutureMs(pE, secsInFuture * MS_PER_SEC);
SG_PUSH(ret ? 1.0 : 0.0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_CancelEvent, ONE_RETURN_VALUE)
SG_POP_OBJECT(IDispatcher, pD)
SG_POP_OBJECT(IEvent, pE)
SG_NULL_DISPATCHER_EVENT(pD, pE)
bool ret = pD->CancelEvent(pE);
SG_PUSH(ret ? 1.0 : 0.0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_CurrentTime, ONE_RETURN_VALUE)
double ret = fTime::TimeMs();
if ((SG_PUSHARGS("d", ret)) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Dispatcher_GetCurrentEpochTime, ONE_RETURN_VALUE)
double secondsEpoch = GetCurrentEpochTime();
if ((SG_PUSHARGS("d", secondsEpoch)) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Dispatcher_GetCurrentLocalEpochTime, ONE_RETURN_VALUE)
double secondsEpoch = GetCurrentLocalEpochTime();
if ((SG_PUSHARGS("d", secondsEpoch)) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Dispatcher_GetCurrentTimeStamp, ONE_RETURN_VALUE)
std::string date = GetCurrentTimeStamp();
if ((SG_PUSHARGS("s", date.c_str())) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Dispatcher_EpochTimeToTimeStamp, ONE_RETURN_VALUE)
double epochTime;
SG_POPARGS("d", &epochTime);
std::string date = EpochTimeToTimeStamp(epochTime);
if ((SG_PUSHARGS("s", date.c_str())) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Dispatcher_DateToEpochTime, ONE_RETURN_VALUE)
double year, month, day, hour, minute, second, millisecond, epochTime;
SG_POPARGS("ddddddd", &year, &month, &day, &hour, &minute, &second, &millisecond);
epochTime = DateToEpochTime((WORD)year, (WORD)month, (WORD)day, (WORD)hour, (WORD)minute, (WORD)second, (WORD)millisecond);
if ((SG_PUSHARGS("d", epochTime)) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

// Dispatcher_GetFunctions( dispatcher, functionNames )
SG_BEGIN_FN(Dispatcher_GetFunctions, ONE_RETURN_VALUE)
const char* functionNames;
SG_DECLARE_POP_OBJ(IDispatcher, pD, gsDisp)
SG_POPARGS("Os", &gsDisp, &functionNames);
SG_CAST_TO_OBJECT(pD, gsDisp);
EVENT_ID eventId = BAD_EVENT_ID;
if (pD && (eventId = pD->GetEventId(functionNames ? functionNames : "")) != BAD_EVENT_ID) {
	// cast since we don't want IDispatcher to expose ProcessOneEvent
	Dispatcher* d = dynamic_cast<Dispatcher*>(pD);
	if (d) {
		IScriptVM* vm = SG_NATIVE_VM;
		IEvent* e = new GetFunctionEvent(functionNames, SG_VM_NAME, (void*)vm, eventId);
		int count = 0; // anyone process this?
		d->ProcessOneEvent(e, &count);
		SG_PUSH((double)count);

		if (vm)
			delete vm;
		SG_RETURN;
	}
}
SG_RESET_RETURN_COUNT(0);
SG_PUSH((double)0);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_GetGame, ONE_RETURN_VALUE)
SG_POP_OBJECT(IDispatcher, pD)
SG_NULL_DISPATCHER(pD)
auto pG = pD->Game();
if (pG) {
	SG_DYNAMIC_CAST_TO_PUSH_OBJ(pG, IGame, pushPtr);
	if ((SG_PUSHARGS("O", pushPtr)) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
} else if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Dispatcher_GetGameAsGetSet, ONE_RETURN_VALUE)
SG_POP_OBJECT(IDispatcher, pD)
SG_NULL_DISPATCHER(pD)
IGame* game = pD->Game();
IGetSet* gs = dynamic_cast<IGetSet*>(game);
if (gs) {
	SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(gs, IGetSet, pushPtr);
	if ((SG_PUSHARGS("O", pushPtr)) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
} else if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;
SG_END_FN

void Dispatcher_LogMessage(IScriptArgManager* pScriptArgManager) {
	ScriptPointer<void, false> pvDispatcher = nullptr;
	int logType = 0;
	if (!GetScriptValueAt(pScriptArgManager, 0, &pvDispatcher) || !pvDispatcher.get()) {
		LogError("Invalid dispatcher argument 0 in " << pScriptArgManager->GetCallInfo());
		return;
	}
	if (!GetScriptValueAt(pScriptArgManager, 1, &logType)) {
		LogError("Invalid logType argument 1 in " << pScriptArgManager->GetCallInfo());
		return;
	}
	struct Local {
		static void Output(std::ostream& strm, const ScriptValue& scriptValue) {
			switch (scriptValue.GetType()) {
				default:
					strm << "<UNKNOWN>";
					return;
				case IScriptArgManager::eValueType::None:
					strm << "nil";
					return;
				case IScriptArgManager::eValueType::Double:
					strm << scriptValue.GetValueRef<double>();
					return;
				case IScriptArgManager::eValueType::Int:
					strm << scriptValue.GetValueRef<int>();
					return;
				case IScriptArgManager::eValueType::Bool:
					strm << (scriptValue.GetValueRef<bool>() ? "true" : "false");
					return;
				case IScriptArgManager::eValueType::String:
					strm << scriptValue.GetValueRef<std::string>();
					return;
				case IScriptArgManager::eValueType::Table: {
					strm << "{";
					const std::map<std::string, ScriptValue>& m = scriptValue.GetValueRef<std::map<std::string, ScriptValue>>();
					for (auto itr = m.begin(); itr != m.end(); ++itr) {
						if (itr != m.begin())
							strm << ",";
						strm << " " << itr->first << ": ";
						Output(strm, itr->second);
					}
					strm << " }";
					return;
				}
				case IScriptArgManager::eValueType::Array: {
					strm << "[ ";
					const std::vector<ScriptValue>& v = scriptValue.GetValueRef<std::vector<ScriptValue>>();
					for (auto itr = v.begin(); itr != v.end(); ++itr) {
						if (itr != v.begin())
							strm << ", ";
						strm << (1 + (itr - v.begin())) << ": ";
						Output(strm, *itr);
					}
					strm << " ]";
					return;
				}
				case IScriptArgManager::eValueType::Pointer:
					strm << "<";
					strm << scriptValue.GetValueRef<void*>();
					strm << ">";
					break;
			}
		}
	};

	IDispatcher* pD = static_cast<IDispatcher*>(pvDispatcher.get());

	std::ostringstream strm;
	size_t iArg = 2;
	for (;; ++iArg) {
		if (!pScriptArgManager->PushArgAtIndex(iArg))
			break; // no more args
		ScriptValue arg;
		bool bGotValue = pScriptArgManager->GetValue(&arg);
		pScriptArgManager->Pop();
		if (!bGotValue) {
			LogError("Invalid log argument " << iArg << " in " << pScriptArgManager->GetCallInfo());
			return;
		}
		Local::Output(strm, arg);
	}
	pD->LogMessage(logType, strm.str());
}

SG_BEGIN_FN(Dispatcher_LogAlertMessage, 0)
const char* s;
SG_POP_OBJECT(IDispatcher, pD);
SG_POP(s);
SG_NULL_DISPATCHER(pD)
if (s)
	pD->LogAlertMessage(s);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Dispatcher_GetCurrentThreadId, ONE_RETURN_VALUE)
int tid = GetCurrentThreadId();
SG_PUSH((double)tid);
SG_RETURN;
SG_END_FN

SG_VM_REGISTER(DispatcherVMRegister)
SG_BEGIN_SG_REGISTER(Dispatcher)
SG_REGISTER(CancelEvent, Dispatcher_CancelEvent)
SG_REGISTER(CurrentTime, Dispatcher_CurrentTime)
SG_REGISTER(GetCurrentEpochTime, Dispatcher_GetCurrentEpochTime)
SG_REGISTER(GetCurrentLocalEpochTime, Dispatcher_GetCurrentLocalEpochTime)
SG_REGISTER(GetCurrentTimeStamp, Dispatcher_GetCurrentTimeStamp)
SG_REGISTER(EpochTimeToTimeStamp, Dispatcher_EpochTimeToTimeStamp)
SG_REGISTER(DateToEpochTime, Dispatcher_DateToEpochTime)
SG_REGISTER(GetCurrentThreadId, Dispatcher_GetCurrentThreadId)
SG_REGISTER(GetEventId, Dispatcher_GetEventId)
SG_REGISTER(GetFunctions, Dispatcher_GetFunctions)
SG_REGISTER(GetGame, Dispatcher_GetGame)
SG_REGISTER(GetGameAsGetSet, Dispatcher_GetGameAsGetSet)
REGISTER_SCRIPT_FUNCTION("Dispatcher_LogMessage", &Dispatcher_LogMessage)
SG_REGISTER(LogAlertMessage, Dispatcher_LogAlertMessage)
SG_REGISTER(MakeEvent, Dispatcher_MakeEvent)
SG_REGISTER(QueueEvent, Dispatcher_QueueEvent)
SG_REGISTER(QueueEventInFuture, Dispatcher_QueueEventInFuture)
SG_REGISTER(QueueEventInFutureMS, Dispatcher_QueueEventInFutureMS)
SG_REGISTER(RegisterEvent, Dispatcher_RegisterEvent)
SG_REGISTER(RegisterSecureEvent, Dispatcher_RegisterSecureEvent)
SG_REGISTER(RescheduleEvent, Dispatcher_RescheduleEvent)
SG_END_REGISTER
SG_END_VM_REGISTER
