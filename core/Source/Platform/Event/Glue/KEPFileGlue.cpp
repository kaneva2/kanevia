///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "IKEPFile.h"
#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"
#include "KEPHelpers.h"

using namespace KEP;

SG_BEGIN_FN(ReadLine, THREE_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IKEPFile, f, popObj);
SG_POPARGS("O", &popObj);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
std::string line, errMsg;
bool ret = f->ReadLine(line, errMsg);
SG_PUSHARGS("dss", (ret ? 1.0 : 0.0), line.c_str(), errMsg.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(WriteLine, TWO_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IKEPFile, f, popObj);
const char* line;
SG_POPARGS("Os", &popObj, &line);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
std::string errMsg;
bool ret = f->WriteLine(line, errMsg);
SG_PUSHARGS("ds", (ret ? 1.0 : 0.0), errMsg.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(CloseAndDelete, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IKEPFile, f, popObj);
SG_POPARGS("O", &popObj);
SG_VALID_CAST_FROM_POP_OBJ(f, popObj);
f->CloseAndDelete(); // delete KEPFile
SG_RETURN;
SG_END_FN

SG_VM_REGISTER(KEPFileVMRegister)
SG_BEGIN_SG_REGISTER(KEPFile)
SG_REGISTER(ReadLine, ReadLine)
SG_REGISTER(WriteLine, WriteLine)
SG_REGISTER(CloseAndDelete, CloseAndDelete)
SG_END_REGISTER
SG_END_VM_REGISTER
