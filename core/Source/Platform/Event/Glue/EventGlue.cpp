///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "Event/Base/IEncodingFactory.h"
#include "Event/Base/IEvent.h"
#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"

#include "IGetSet.h"

#include "common\KEPUtil\Helpers.h"

using namespace KEP;

SG_BEGIN_FN(Event_DecodeNumber, ONE_RETURN_VALUE)

SG_POP_OBJECT(IEvent, e)
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

IReadableBuffer *reader = e->InBuffer();
if (!reader)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_READER));

PCBYTE buf = 0;
ULONG bufLen = 0;
reader->GetBufferLeft(buf, bufLen);
if (!bufLen)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_EMPTY_BUFFER));

double d;
(*reader) >> d;
SG_PUSH(d);
SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_DecodeObject, ONE_RETURN_VALUE)

SG_POP_OBJECT(IEvent, e)
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

IReadableBuffer *reader = e->InBuffer();
if (!reader)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_READER));

PCBYTE buf = 0;
ULONG bufLen = 0;
reader->GetBufferLeft(buf, bufLen);
if (!bufLen)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_EMPTY_BUFFER));

LONG temp;
(*reader) >> temp;

// must be GetSetObject
IGetSet *ret = (IGetSet *)temp;

SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(ret, IGetSet, getSetPtr);
if ((SG_PUSHARGS("O", getSetPtr)) != NULL)
	SG_RETURN;
else
	SG_RETURN_ERR;

SG_END_FN

SG_BEGIN_FN(Event_DecodeString, ONE_RETURN_VALUE)

SG_POP_OBJECT(IEvent, e)
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

IReadableBuffer *reader = e->InBuffer();
if (!reader)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_READER));

PCBYTE buf = 0;
ULONG bufLen = 0;
reader->GetBufferLeft(buf, bufLen);
if (!bufLen)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_EMPTY_BUFFER));

std::string s;
try {
	reader->ReadString(s);
} catch (...) {
	SG_RETURN_ERR_STR("ReadString() Caught Exception");
}
SG_PUSH(s.c_str());

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_EncodeString, NO_RETURN_VALUE)

const char *s = NULL;
SG_POP_OBJECT(IEvent, e)
SG_POP(s);
if (!e || !s)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

IWriteableBuffer *writer = e->OutBuffer();
if (!writer)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_READER));

(*writer) << s;

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_EncodeNumber, NO_RETURN_VALUE)

double d;
SG_POP_OBJECT(IEvent, e)
SG_POP(d);
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

IWriteableBuffer *writer = e->OutBuffer();
if (!writer)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_READER));

(*writer) << d;

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_AddTo, NO_RETURN_VALUE)

double d;
SG_POP_OBJECT(IEvent, e)
SG_POP(d);
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

e->AddTo((NETID)d);

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_ResetForOutput, NO_RETURN_VALUE)

SG_POP_OBJECT(IEvent, e)
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

e->ResetForOutput();

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_SetFilter, NO_RETURN_VALUE)

double d;
SG_POP_OBJECT(IEvent, e)
SG_POP(d);
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

e->SetFilter((ULONG)d);

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_SetObjectId, NO_RETURN_VALUE)

double d;
SG_POP_OBJECT(IEvent, e)
SG_POP(d);
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

e->SetObjectId((OBJECT_ID)d);

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_SetFlags, NO_RETURN_VALUE)

double d;
SG_POP_OBJECT(IEvent, e)
SG_POP(d);
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

e->SetFlags((ULONG)d);

SG_RETURN;

SG_END_FN

SG_BEGIN_FN(Event_MoreToDecode, ONE_RETURN_VALUE)

SG_POP_OBJECT(IEvent, e)
if (!e)
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_EVENT));

IReadableBuffer *reader = e->InBuffer();
if (!reader) {
	SG_PUSH(0.0);
	SG_RETURN;
}

PCBYTE buf = 0;
ULONG bufLen = 0;
reader->GetBufferLeft(buf, bufLen);
SG_PUSH((bufLen > 0) ? 1.0 : 0.0);

SG_RETURN;

SG_END_FN

SG_VM_REGISTER(EventVMRegister)
SG_BEGIN_SG_REGISTER(Event)
SG_REGISTER(DecodeNumber, Event_DecodeNumber)
SG_REGISTER(DecodeString, Event_DecodeString)
SG_REGISTER(EncodeNumber, Event_EncodeNumber)
SG_REGISTER(EncodeString, Event_EncodeString)
SG_REGISTER(ResetForOutput, Event_ResetForOutput)
SG_REGISTER(AddTo, Event_AddTo)
SG_REGISTER(DecodeObject, Event_DecodeObject)
SG_REGISTER(SetFilter, Event_SetFilter)
SG_REGISTER(SetObjectId, Event_SetObjectId)
SG_REGISTER(SetFlags, Event_SetFlags)
SG_REGISTER(MoreToDecode, Event_MoreToDecode)
SG_END_REGISTER
SG_END_VM_REGISTER
