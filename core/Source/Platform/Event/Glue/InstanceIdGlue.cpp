///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "KEPHelpers.h"

#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"

#include "InstanceId.h"

using namespace KEP;

//	virtual LONG setInstanceAndTypeFrom( currentChannel ) = 0;
SG_BEGIN_FN(InstanceId_SetInstanceAndTypeFrom, ONE_RETURN_VALUE)

double myInstance = 0;
;
double currentChannel = 0;

if (SG_POPARGS("dd", &myInstance, &currentChannel)) {
	ZoneIndex z((LONG)myInstance);
	z.setInstanceAndTypeFrom((LONG)currentChannel);

	if ((SG_PUSHARGS("d", (double)z.toLong())) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

SG_BEGIN_FN(InstanceId_SetInstanceFrom, ONE_RETURN_VALUE)

double myInstance = 0;
;
double currentChannel = 0;

if (SG_POPARGS("dd", &myInstance, &currentChannel)) {
	ZoneIndex z((LONG)myInstance);
	z.setInstanceId((LONG)currentChannel);

	if ((SG_PUSHARGS("d", (double)(z.toLong()))) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

//	virtual bool isInstanced() const = 0;
SG_BEGIN_FN(InstanceId_IsInstanced, ONE_RETURN_VALUE)

double myInstance = 0;
;

if (SG_POPARGS("d", &myInstance)) {
	ZoneIndex z((LONG)myInstance);

	if ((SG_PUSHARGS("d", (double)z.isInstanced())) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

//	virtual LONG instancedId() const = 0;
SG_BEGIN_FN(InstanceId_GetInstanceId, ONE_RETURN_VALUE)

double myInstance = 0;
;

if (SG_POPARGS("d", &myInstance)) {
	ZoneIndex z((LONG)myInstance);

	if ((SG_PUSHARGS("d", (double)z.GetInstanceId())) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

//	virtual ULONG type() const = 0;
SG_BEGIN_FN(InstanceId_GetType, ONE_RETURN_VALUE)

double myInstance = 0;
;

if (SG_POPARGS("d", &myInstance)) {
	ZoneIndex z((LONG)myInstance);

	if ((SG_PUSHARGS("d", (double)z.GetType())) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

//	virtual LONG zoneIndex() const = 0;
SG_BEGIN_FN(InstanceId_GetId, ONE_RETURN_VALUE)

double myInstance = 0;
;

if (SG_POPARGS("d", &myInstance)) {
	ZoneIndex z((LONG)myInstance);

	if ((SG_PUSHARGS("d", (double)z.zoneIndex())) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

// ChannelId( channel.channelId(), m_nextInstanceId++, CI_ARENA );
SG_BEGIN_FN(InstanceId_MakeId, ONE_RETURN_VALUE)

double id = 0;
double instanceId = 0;
double type;

if (SG_POPARGS("ddd", &id, &instanceId, &type)) {
	ZoneIndex z((LONG)id, (LONG)instanceId, (LONG)type);

	if ((SG_PUSHARGS("d", (double)z.toLong())) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

//Return the zone index without the instance id
SG_BEGIN_FN(InstanceId_GetClearedInstanceId, ONE_RETURN_VALUE)

double myInstance = 0;
;

if (SG_POPARGS("d", &myInstance)) {
	ZoneIndex z((LONG)myInstance);

	double zoneIndex = (double)z.getClearedInstanceId();

	if ((SG_PUSHARGS("d", zoneIndex)) != NULL)
		SG_RETURN;
	else
		SG_RETURN_ERR;
}
SG_RETURN_ERR;

SG_END_FN

SG_VM_REGISTER(InstanceIdVMRegister)
SG_BEGIN_SG_REGISTER(InstanceId)
SG_REGISTER(GetInstanceId, InstanceId_GetInstanceId)
SG_REGISTER(GetId, InstanceId_GetId)
SG_REGISTER(GetType, InstanceId_GetType)
SG_REGISTER(IsInstanced, InstanceId_IsInstanced)
SG_REGISTER(MakeId, InstanceId_MakeId)
SG_REGISTER(SetInstanceAndTypeFrom, InstanceId_SetInstanceAndTypeFrom)
SG_REGISTER(SetInstanceFrom, InstanceId_SetInstanceFrom)
SG_REGISTER(GetClearedInstanceId, InstanceId_GetClearedInstanceId)
SG_END_REGISTER
SG_END_VM_REGISTER
