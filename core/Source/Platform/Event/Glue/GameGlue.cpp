///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "common\include\KEPCommon.h"

#include "common\KEPUtil\Helpers.h"

#include "IServerEngine.h"
#include "IPlayer.h"
#include "IKEPObject.h"
#include "IGetSet.h"

#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"

using namespace KEP;

// DEPRECATED
SG_BEGIN_FN(Game_StartAISpawn, ONE_RETURN_VALUE)
SG_RETURN_ERR;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(Game_StopAISpawn, ONE_RETURN_VALUE)
SG_RETURN_ERR;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(Game_AdjustArenaPoints, NO_RETURN_VALUE)
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(Game_ActivateTempInvulnerability, NO_RETURN_VALUE)
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(Game_DeMount, NO_RETURN_VALUE)
SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Game_GetPlayerByNetId, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double id = 0;
if (SG_POPARGS("Od", &sgGame, &id)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IPlayer *p = g->GetPlayerByNetId((ULONG)id, true);
		if (p != 0) {
			SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(p, IGetSet, scriptPtr);
			if ((SG_PUSHARGS("O", scriptPtr)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		} else {
			if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		}
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
}
SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Game_GetPlayerByNetTraceId, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double id = 0;
double pconly = 0;
double aliveonly = 0;
if (SG_POPARGS("Oddd", &sgGame, &id, &pconly, &aliveonly)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IPlayer *p = g->GetPlayerByNetTraceId((int)id);
		if (p) {
			SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(p, IGetSet, scriptPtr);
			if ((SG_PUSHARGS("O", scriptPtr)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		} else {
			if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		}
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
}
SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Game_GetPlayerByName, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
const char *name = 0;
double PCOnly = FALSE;
double aliveOnly = FALSE;
if (SG_POPARGS("Osdd", &sgGame, &name, &PCOnly, &aliveOnly)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IPlayer *p = g->GetPlayerByName(name);
		if (p != 0) {
			SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(p, IGetSet, scriptPtr);
			if ((SG_PUSHARGS("O", scriptPtr)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		} else {
			if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		}
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
}
SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Game_GetPlayerOffAccountByNetId, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double id = 0;
double index = 0;
if (SG_POPARGS("Odd", &sgGame, &id, &index)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IPlayer *p = g->GetPlayerOffAccountByNetId((ULONG)id, (int)index);
		if (p != 0) {
			SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(p, IGetSet, scriptPtr);
			if ((SG_PUSHARGS("O", scriptPtr)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		} else {
			if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		}
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
}
SG_RETURN_ERR;
SG_END_FN

SG_BEGIN_FN(Game_BroadcastMessage, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double radius;
const char *msg;
double x;
double y;
double z;
double id;
double netid;
double allInstances;
double chatType;
if (SG_POPARGS("Odsddddddd", &sgGame, &radius, &msg, &x, &y, &z, &id, &netid, &chatType, &allInstances)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		g->BroadcastMessage((short)radius, msg, x, y, z, (LONG)id, (int)netid, (eChatType)(int)chatType, allInstances != 0.0);

		SG_RETURN;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_BroadcastMessageToDistribution, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
const char *msg;
double netid;
double chatType;
const char *toDistribution;
const char *fromRecipient;
double fromServer = 0.0;
if (SG_POPARGS("Osdssdd", &sgGame, &msg, &netid, &toDistribution, &fromRecipient, &chatType, &fromServer)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		g->BroadcastMessageToDistribution(msg, (int)netid, toDistribution, fromRecipient, (eChatType)(int)chatType, (fromServer != 0.0));

		SG_RETURN;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_UpdatePlayer, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
if (SG_POPARGS("OO", &sgGame, &sgPlayer)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		if (SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
			g->UpdatePlayer(p);

			SG_RETURN;
		} else {
			SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_PLAYER));
		}
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_SpawnToPoint, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
double x, y, z, rotation;
double zoneIndex;
if (SG_POPARGS("OOddddd", &sgGame, &sgPlayer, &x, &y, &z, &zoneIndex, &rotation)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		if (SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
			g->SpawnToPoint(p, x, y, z, (LONG)zoneIndex, DT_Zone, (int)rotation);
			SG_RETURN;
		} else {
			SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_PLAYER));
		}
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_SpawnToArenaPoint, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
double x, y, z;
double zoneIndex;
if (SG_POPARGS("OOdddd", &sgGame, &sgPlayer, &x, &y, &z, &zoneIndex)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		if (SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
			g->SpawnToPoint(p, x, y, z, (LONG)zoneIndex, DT_Arena);
			SG_RETURN;
		} else {
			SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_PLAYER));
		}
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_SpawnToArenaPointWithRot, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
double x, y, z;
double zoneIndex;
double rot;
if (SG_POPARGS("OOddddd", &sgGame, &sgPlayer, &x, &y, &z, &rot, &zoneIndex)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		if (SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
			g->SpawnToPoint(p, x, y, z, (LONG)zoneIndex, DT_Arena, (int)rot);
			SG_RETURN;
		} else {
			SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_PLAYER));
		}
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_TextHandler, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double filter;
const char *msg;
double idFromLoc;
if (SG_POPARGS("Odsd", &sgGame, &filter, &msg, &idFromLoc)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		g->TextHandler(static_cast<SlashCommand>((int)filter), msg, (NETID)idFromLoc);
		SG_RETURN;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_SendTextMessage, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
double chatType;
const char *msg;
if (SG_POPARGS("OOsd", &sgGame, &sgPlayer, &msg, &chatType)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame) &&
		SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
		jsOutputToDebugger("Sending text message %s", msg);

		bool b = g->SendTextMessage(p, msg, (eChatType)(int)chatType) != FALSE;
		SG_PUSH(b ? 1.0 : 0.0);
		SG_RETURN;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_GetItemObjectByNetTraceId, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double id;
if (SG_POPARGS("Od", &sgGame, &id)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IGetSet *p = g->GetItemObjectByNetTraceId((int)id);
		if (p != 0) {
			SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(p, IGetSet, scriptPtr);
			if ((SG_PUSHARGS("O", scriptPtr)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		} else if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
			SG_RETURN;
		else
			SG_RETURN_ERR;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_UpdatePlayerDataToClient, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double netID;
if (SG_POPARGS("Od", &sgGame, &netID)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		g->SendInventoryUpdateToClient((NETID)netID);
		g->SendHealthUpdateToClient((NETID)netID);
		SG_RETURN;
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_UpdatePlayerHealth, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double netID;
if (SG_POPARGS("Od", &sgGame, &netID)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		g->SendHealthUpdateToClient((NETID)netID);
		SG_RETURN;
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_EquipInventoryItem, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double netID, glID;
if (SG_POPARGS("Odd", &sgGame, &netID, &glID)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IPlayer *p = g->GetPlayerByNetId((NETID)netID, true);
		if (!p) {
			if ((SG_PUSHARGS("d", (double)0.0)) != NULL) // return null
				SG_RETURN;
			else
				SG_RETURN_ERR;
		}
		g->EquipInventoryItem(p, (int)glID, (NETID)netID);
		SG_PUSH((double)0);
		SG_RETURN;
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_GetInventoryItemQty, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double netID, glID;
if (SG_POPARGS("Odd", &sgGame, &netID, &glID)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		int itemQuantity = 0;
		IPlayer *p = g->GetPlayerByNetId((NETID)netID, true);
		if (!p) {
			if ((SG_PUSHARGS("d", (double)0.0)) != NULL) // return null
				SG_RETURN;
			else
				SG_RETURN_ERR;
		}
		g->GetInventoryItemQty(p, (int)glID, itemQuantity);
		SG_PUSH((double)itemQuantity);
		SG_RETURN;
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_ReloadInventoryItem, NO_RETURN_VALUE)
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Game_DeSpawnStaticAI, NO_RETURN_VALUE)
#if (DRF_OLD_VENDORS == 1)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double netAssignedId;
if (SG_POPARGS("Od", &sgGame, &netAssignedId)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		g->DeSpawnStaticAIByNetTraceId((int)netAssignedId);
		SG_RETURN;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
#else
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(Game_DeSpawnStaticAIByNumber, NO_RETURN_VALUE)
#if (DRF_OLD_VENDORS == 1)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double spawnID;
double channel;
if (SG_POPARGS("Odd", &sgGame, &spawnID, &channel)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		g->DeSpawnStaticAIBySpawnId((int)spawnID, (LONG)channel);
		SG_RETURN;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
#else
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(Game_SpawnStaticAIByNumber, ONE_RETURN_VALUE)
#if (DRF_OLD_VENDORS == 1)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double spawnid;
double x, y, z;
double channel;
if (SG_POPARGS("Oddddd", &sgGame, &spawnid, &x, &y, &z, &channel)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IGetSet *p = g->SpawnStaticAIBySpawnId((int)spawnid, x, y, z, (LONG)channel);
		if (p != 0) {
			SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(p, IGetSet, scriptPtr);
			if ((SG_PUSHARGS("O", scriptPtr)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		} else
			SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_OBJECT));
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
#else
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(Game_TellClientToOpenMenu, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
double menuId, playerNetId, closeAllOthers, networkAssignIdOfTradee;
if (SG_POPARGS("Odddd", &sgGame, &menuId, &playerNetId, &closeAllOthers, &networkAssignIdOfTradee)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		bool ok = g->TellClientToOpenMenu((LONG)menuId, (NETID)playerNetId, closeAllOthers != 0.0 ? TRUE : FALSE, (int)networkAssignIdOfTradee);
		SG_PUSH(ok ? 1.0 : 0.0);
		SG_RETURN;
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_AddInventoryItemToPlayer, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
double GLID, qty, armed;
if (SG_POPARGS("OOddd", &sgGame, &sgPlayer, &GLID, &qty, &armed)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		if (SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
			g->AddInventoryItemToPlayer(p, (int)qty, armed != 0.0, (int)GLID);
			SG_RETURN;
		}
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Game_RemoveInventoryItemFromPlayer, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
double GLID, qty, armed, invType;
if (SG_POPARGS("OOdddd", &sgGame, &sgPlayer, &GLID, &qty, &armed, &invType)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		if (SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
			g->RemoveInventoryItemFromPlayer(p, (int)qty, armed != 0.0, (int)GLID, (short)invType);
			SG_RETURN;
		}
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(Game_RequestArenaList, NO_RETURN_VALUE)
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(Game_GetDBConfig, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
if (SG_POPARGS("O", &sgGame)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		IKEPConfig *cfg = const_cast<IKEPConfig *>(g->GetDBKEPConfig());
		if (cfg != NULL) {
			SG_CAST_TO_PUSH_OBJ(cfg, scriptPtr);
			if ((SG_PUSHARGS("O", scriptPtr)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		} else {
			if ((SG_PUSHARGS("d", (double)0.0)) != NULL)
				SG_RETURN;
			else
				SG_RETURN_ERR;
		}
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_BroadcastEvent, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IEvent, e, sgEvent)
double channel, netId, radius, x, y, z, chatType, allInstances;
if (SG_POPARGS("OOdddddddd", &sgGame, &sgEvent, &channel, &netId, &radius, &x, &y, &z, &chatType, &allInstances)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame) && SG_CAST_TO_OBJECT(e, sgEvent)) {
		g->BroadcastEvent(e, (LONG)channel, (NETID)netId, (short)radius, x, y, z, (eChatType)(int)chatType, (allInstances != 0.0));
		SG_RETURN;
	} else {
		SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
	}
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_BEGIN_FN(Game_GetPlayerPosition, THREE_RETURN_VALUES)
SG_DECLARE_POP_OBJ(IServerEngine, g, sgGame);
SG_DECLARE_POP_OBJ(IPlayer, p, sgPlayer);
if (SG_POPARGS("OO", &sgGame, &sgPlayer)) {
	if (SG_CAST_TO_DERIVED_OBJECT(g, IServerEngine, IGame, sgGame)) {
		if (SG_CAST_TO_DERIVED_OBJECT(p, IPlayer, IGetSet, sgPlayer)) {
			Vector3f pos = p->getLocation();
			SG_PUSHARGS("ddd", (double)pos.x, (double)pos.y, (double)pos.z);
			SG_RETURN;
		} else {
			SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_PLAYER));
		}
	}
	SG_RETURN_ERR_STR(loadStr(IDS_ES_NULL_GAME));
} else {
	SG_RETURN_ERR;
}
SG_END_FN

SG_VM_REGISTER(GameVMRegister)
SG_BEGIN_SG_REGISTER(Game)

// Lua Only (Game_*)
SG_REGISTER(DeMount, Game_DeMount)
SG_REGISTER(TextHandler, Game_TextHandler)
SG_REGISTER(BroadcastMessageToDistribution, Game_BroadcastMessageToDistribution)
SG_REGISTER(GetPlayerPosition, Game_GetPlayerPosition)

// Lua (Game_*) & Python (Game.*)
SG_REGISTER(GetPlayerByNetId, Game_GetPlayerByNetId)
SG_REGISTER(GetPlayerByNetTraceId, Game_GetPlayerByNetTraceId)
SG_REGISTER(GetItemObjectByNetTraceId, Game_GetItemObjectByNetTraceId)
SG_REGISTER(BroadcastMessage, Game_BroadcastMessage)

// Python Only (Game.*)
SG_REGISTER(ActivateTempInvulnerability, Game_ActivateTempInvulnerability)
SG_REGISTER(GetPlayerByName, Game_GetPlayerByName)
SG_REGISTER(GetPlayerOffAccountByNetId, Game_GetPlayerOffAccountByNetId)
SG_REGISTER(GetInventoryItemQty, Game_GetInventoryItemQty)
SG_REGISTER(AddInventoryItemToPlayer, Game_AddInventoryItemToPlayer)
SG_REGISTER(RemoveInventoryItemFromPlayer, Game_RemoveInventoryItemFromPlayer)
SG_REGISTER(ReloadInventoryItem, Game_ReloadInventoryItem)
SG_REGISTER(EquipInventoryItem, Game_EquipInventoryItem)
SG_REGISTER(RequestArenaList, Game_RequestArenaList)
SG_REGISTER(SpawnToPoint, Game_SpawnToPoint)
SG_REGISTER(SpawnToArenaPoint, Game_SpawnToArenaPoint)
SG_REGISTER(SpawnToArenaPointWithRot, Game_SpawnToArenaPointWithRot)
SG_REGISTER(SpawnStaticAIByNumber, Game_SpawnStaticAIByNumber)
SG_REGISTER(DeSpawnStaticAIByNumber, Game_DeSpawnStaticAIByNumber)
SG_REGISTER(DeSpawnStaticAI, Game_DeSpawnStaticAI)
SG_REGISTER(StartAISpawn, Game_StartAISpawn)
SG_REGISTER(UpdatePlayer, Game_UpdatePlayer)
SG_REGISTER(UpdatePlayerData, Game_UpdatePlayerDataToClient)
SG_REGISTER(GetDBConfig, Game_GetDBConfig)
SG_REGISTER(BroadcastEvent, Game_BroadcastEvent)
SG_REGISTER(SendTextMessage, Game_SendTextMessage)
SG_REGISTER(TellClientToOpenMenu, Game_TellClientToOpenMenu)
SG_REGISTER(UpdatePlayerHealth, Game_UpdatePlayerHealth)
SG_REGISTER(AdjustArenaPoints, Game_AdjustArenaPoints)
SG_REGISTER(StopAISpawn, Game_StopAISpawn)

SG_END_REGISTER
SG_END_VM_REGISTER
