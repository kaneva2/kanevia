///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <d3d9.h>
#include <d3dx9math.h>
#include <Ikepconfig.h>

#include "KEPCommon.h"

#include "IClientEngine.h"
#include "IPlayer.h"
#include "IGetSet.h"
#include "Event/Base/ScriptGlue.h"
#include "Event/Base/EventStrings.h"
#include "Event/Base/idispatcher.h"
#include "Event/Events/ScriptServerEvent.h"
#include "LightweightGeom.h"
#include "MediaParams.h"
#include "ScriptGluePrimitiveGeomArgs.h"
#include "Common\include\UgcConstants.h"
#include "Common\KEPUtil\WebCall.h"
#include "Common\KEPUtil\CrashReporter.h"
#include "Common\KEPUtil\Helpers.h"
#include "Core/Math/Units.h"

using namespace KEP;

static LogInstance("Instance");

IGetSet* g_pIGS = NULL; // drf - global game getset singleton
IDispatcher* g_pID = NULL; // drf - global dispatcher singleton

template <typename T>
T* GetGlueGlobal();
template <>
IClientEngine* GetGlueGlobal<IClientEngine>() {
	return IClientEngine::Instance();
}
template <>
IGetSet* GetGlueGlobal<IGetSet>() {
	return g_pIGS;
}
template <>
IDispatcher* GetGlueGlobal<IDispatcher>() {
	return g_pID;
}

#define ToString(s) (s ? s : "")

SG_BEGIN_FN(KEP_SetInstance, NO_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IClientEngine, pICE, _pCE);
SG_DECLARE_POP_OBJ(IGetSet, pIGS, _pGS);
SG_DECLARE_POP_OBJ(IDispatcher, pID, _pD);
SG_POPARGS("OOO", &_pCE, &_pGS, &_pD);
if (!IClientEngine::Instance()) {
	if (!SG_CAST_TO_DERIVED_OBJECT(pICE, IClientEngine, IGame, _pCE))
		SG_RETURN_ERR_STR("Failed Casting 1st Parameter (pClientEngine)");
	IClientEngine::Instance(pICE);
}
if (!g_pIGS) {
	if (!SG_CAST_TO_OBJECT(pIGS, _pGS))
		SG_RETURN_ERR_STR("Failed Casting 2nd Parameter (pGetSet)");
	g_pIGS = pIGS;
}
if (!g_pID) {
	if (!SG_CAST_TO_OBJECT(pID, _pD))
		SG_RETURN_ERR_STR("Failed Casting 3rd Parameter (pDispatcher)");
	g_pID = pID;
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Breakpoint, NO_RETURN_VALUE)
SG_RETURN; // set a breakpoint here
SG_END_FN

SG_BEGIN_FN(KEP_Error, NO_RETURN_VALUE)
const char* errMsg = "";
SG_POPARGS("s", &errMsg);
SG_RETURN_ERR_STR(ToString(errMsg));
SG_END_FN

SG_BEGIN_FN(KEP_ObjectIsValid, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
SG_POPARGS("O", &popObj);
auto valid = (REF_PTR_VALID(popObj) && SG_CAST_TO_OBJECT(igs, popObj));
SG_PUSHARGS("b", valid);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LogMemory, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->LogMemory();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LogObjects, NO_RETURN_VALUE)
::RefPtrLog(true); // verbose
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LogWebCalls, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->LogWebCalls();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LogResources, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double resources = 0.0;
SG_POPARGS("d", &resources);
pICE->LogResources((int)resources);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LogMetrics, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->LogMetrics();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LogMsgProc, NO_RETURN_VALUE)
bool enable = false;
SG_POPARGS("b", &enable);
::MsgProcLog(enable);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LogChat, NO_RETURN_VALUE)
const char* msg = "";
SG_POPARGS("s", &msg);
static LogInstance("Chat");
_LogInfo(ToString(msg));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_IsKIMPresent, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_PUSHARGS("b", false); // DEPRECATED
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetStarId, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
std::string starIdStr = pICE->AppStarId();
SG_PUSHARGS("s", starIdStr.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_IsProduction, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto isProduction = pICE -> AppIsProduction();
SG_PUSHARGS("b", isProduction);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DevMode, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto devMode = pICE -> AppDevMode();
SG_PUSHARGS("b", devMode);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SendSystemServiceEvent, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* userId = "";
SG_POPARGS("s", &userId);
if (userId)
	pICE->SendSystemServiceEvent(ToString(userId));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MetricsEnable, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enable = false;
SG_POPARGS("b", &enable);
pICE->MetricsEnable(enable);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MetricsFpsRender, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double fps = pICE->RenderMetricsFps(true); // filtered
SG_PUSHARGS("d", fps);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MetricsFpsRuntime, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double fps = 0.0;
pICE->MetricsFps(ClientMetrics::FPS_ID::RUNTIME, fps);
SG_PUSHARGS("d", fps);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MetricsFpsZone, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double fps = 0.0;
pICE->MetricsFps(ClientMetrics::FPS_ID::CUSTOM, fps);
SG_PUSHARGS("d", fps);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WindowRefresh, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->FlagWindowRefresh();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MouseClicks, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto clicks = pICE -> MouseClicks();
SG_PUSHARGS("d", (double)clicks);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_IsKeyDown, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double keyCode = 0.0;
SG_POPARGS("d", &keyCode);
auto keyIsDown = pICE -> IsKeyDown((size_t)keyCode);
SG_PUSHARGS("b", keyIsDown);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UrlEncode, ONE_RETURN_VALUE)
const char* str = "";
SG_POPARGS("s", &str);
auto urlStr = UrlEncode(ToString(str), false); // is not a url already!
SG_PUSHARGS("s", urlStr.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ShellOpen, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* URL = "";
SG_POPARGS("s", &URL);

//Make sure that the URL referrs to a .lua or .xml file inside the Local Dev folder
//And does not back out of that folder
std::string URLString(ToString(URL));
int strLength = URLString.length();
std::string extension = URLString.substr(strLength - 4, 4);
size_t foundBackOut = URLString.find("..");
size_t foundLocalDev = URLString.find("My 3D Apps\\LocalDev");
bool checkExtension = false;
if (extension.compare(".lua") == 0 || extension.compare(".LUA") == 0) {
	checkExtension = true;
} else if (extension.compare(".xml") == 0 || extension.compare(".XML") == 0) {
	checkExtension = true;
} else if (extension.compare(".dds") == 0 || extension.compare(".DDS") == 0) {
	checkExtension = true;
} else if (extension.compare(".tga") == 0 || extension.compare(".TGA") == 0) {
	checkExtension = true;
} else if (extension.compare(".bmp") == 0 || extension.compare(".BMP") == 0) {
	checkExtension = true;
} else if (extension.compare(".png") == 0 || extension.compare(".PNG") == 0) {
	checkExtension = true;
} else if (extension.compare(".jpg") == 0 || extension.compare(".JPG") == 0) {
	checkExtension = true;
} else if (extension.compare(".jpeg") == 0 || extension.compare(".JPEG") == 0) {
	checkExtension = true;
}
if (foundBackOut != std::string::npos || foundLocalDev == std::string::npos || !checkExtension)
	SG_RETURN_ERR;
int result = (int)pICE->shellExec(URL, "open", "");
if (result == SE_ERR_NOASSOC) {
	// try again with the "Open With.." dialog
	result = (int)pICE->shellExec("rundll32.exe", "open", (std::string("shell32.dll,OpenAs_RunDLL ") + std::string(URL)).c_str());
}
SG_PUSHARGS("d", (double)result);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlayerIsValid, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* userName = "";
SG_POPARGS("s", &userName);
auto ok = (pICE->GetMovementObjectByNameAsGetSet(ToString(userName)) != NULL);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

ScriptPointer<IGetSet> KEP_GetPlayer() {
	auto pICE = IClientEngine::Instance();
	return pICE ? pICE->GetMovementObjectMeAsGetSet() : nullptr;
}

SG_BEGIN_FN(KEP_GetPlayerByName, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* userName = "";
SG_POPARGS("s", &userName);
auto igs = pICE -> GetMovementObjectByNameAsGetSet(ToString(userName));
if (!igs)
	SG_RETURN_ERR_STR("GetMovementObjByNameAsGetSet() FAILED - '" + ToString(userName) + "'");
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerPosition, FOUR_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
float posX, posY, posZ, rotDeg;
pICE->GetMyPosition(posX, posY, posZ, rotDeg);
SG_PUSHARGS("dddd", (double)posX, (double)posY, (double)posZ, (double)rotDeg);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerByNetworkDefinedID, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double netId = 0.0;
SG_POPARGS("d", &netId);
auto igs = pICE -> GetMovementObjectByNetIdAsGetSet((int)netId);
if (!igs) {
	LogError("GetMovementObjByNetIdAsGetSet() FAILED - netId=" << (int)netId);
	SG_RETURN_NULL;
}
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetActorsCameraIndex, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto igs = pICE -> GetMovementObjectMeAsGetSet();
if (!igs)
	SG_RETURN_ERR_STR("GetMovementObjMeAsGetSet() FAILED");
auto index = pICE -> GetActiveCameraIndexForActor(igs);
if (index == -1)
	SG_RETURN_ERR_STR("GetActorsCameraIndex() FAILED");
SG_PUSHARGS("d", (double)index);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCameraCollision, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool collision = false;
SG_POPARGS("b", &collision);
pICE->SetCameraCollision(collision);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetCameraMoveSpeed, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto speed = pICE -> GetCameraMoveSpeed();
SG_PUSHARGS("d", (double)speed);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCameraMoveSpeed, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double speed = 0.0;
SG_POPARGS("d", &speed);
pICE->SetCameraMoveSpeed(speed);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetCameraLock, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ok = pICE -> GetCameraLock();
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCameraLock, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool lock = false;
SG_POPARGS("b", &lock);
pICE->SetCameraLock(lock);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RenderLabelsOnTop, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool onTop = false;
SG_POPARGS("b", &onTop);
pICE->RenderLabelsOnTop(onTop);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Login, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* email = "";
const char* password = "";
SG_POPARGS("ss", &email, &password);
auto ok = pICE -> Login(ToString(email), ToString(password));
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Logout, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->Logout();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Quit, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->Quit();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Crash, NO_RETURN_VALUE)
double crashType = 0.0;
SG_POPARGS("d", &crashType);
CrashReporter::ForcedCrash((unsigned int)crashType, "Script::Crash");
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UuidCreate, ONE_RETURN_VALUE)
std::string uuidStr;
::UuidCreate(uuidStr);
SG_PUSHARGS("s", uuidStr.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ReportIssue, NO_RETURN_VALUE)
const char* uuid;
const char* reason;
SG_POPARGS("ss", &uuid, &reason);
if (!uuid)
	SG_RETURN_ERR_STR("Invalid 1st Parameter (uuid)");
std::string reasonStr = ToString(reason);
if (reasonStr.empty())
	reasonStr = "Script::ReportIssue";
CrashReporter::SetErrorData("uuidIssue", uuid);
CrashReporter::ForcedCrash(CR_CRASH_TYPE_REPORT_ONLY, reasonStr);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ConsolePrint, NO_RETURN_VALUE)
const char* s;
SG_POPARGS("s", &s);
fprintf(stdout, "%s", ToString(s));
fflush(stdout);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ConsoleError, NO_RETURN_VALUE)
const char* s;
SG_POPARGS("s", &s);
fprintf(stderr, "%s", ToString(s));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlayCharacter, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0.0;
const char* startUrl = nullptr;
SG_POPARGS("ds", &index, &startUrl);
auto ok = pICE -> PlayCharacter((int)index, startUrl);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DeleteCharacter, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0.0;
SG_POPARGS("d", &index);
auto ok = pICE -> DeleteCharacter((int)index);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CreateCharacter, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double spawnCfg = 0.0;
double team = 0.0;
const char* name = "";
double scaleX = 0.0, scaleY = 0.0, scaleZ = 0.0;
SG_POPARGS("Odsdddd", &popObj, &spawnCfg, &name, &team, &scaleX, &scaleY, &scaleZ);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> CreateCharacterOnServerFromMenuGameObject(igs, (int)spawnCfg, ToString(name), (int)team, (int)scaleX, (int)scaleY, (int)scaleZ);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

// UNUSED
SG_BEGIN_FN(KEP_CreateCharacterFromPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double spawnCfg = 0.0;
double team = 0.0;
const char* name = "";
double scaleX = 0.0, scaleY = 0.0, scaleZ = 0.0;
SG_POPARGS("Odsdddd", &popObj, &spawnCfg, &name, &team, &scaleX, &scaleY, &scaleZ);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> CreateCharacterOnServerFromPlayer(igs, (int)spawnCfg, ToString(name), (int)team, (int)scaleX, (int)scaleY, (int)scaleZ);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetCharacterMaxCount, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto maxCount = pICE -> GetCharacterMaxCount();
SG_PUSHARGS("d", (double)maxCount);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlaySoundById, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double id, looping, x, y, z, soundFactor, clipRadius;
SG_POPARGS("ddddddd", &id, &looping, &x, &y, &z, &soundFactor, &clipRadius);
auto ok = pICE -> PlayBySoundID((int)id, (BOOL)looping, x, y, z, (float)soundFactor, (float)clipRadius);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SpawnToRebirthPoint, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->SpawnToRebirthPoint();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ScreenshotCapture, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
RECT rect = { -1, -1, -1, -1 }; // full screen
auto path = pICE -> ScreenshotCapture(rect);
SG_PUSHARGS("s", path.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ScreenshotIsCapturing, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool capturing = pICE->ScreenshotIsCapturing();
SG_PUSHARGS("b", capturing);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddMenuGameObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double game_object_index = 0.0;
double animation_index = 0.0;
SG_POPARGS("dd", &game_object_index, &animation_index);
auto igs = pICE -> AddMenuGameObjectAsGetSet((int)game_object_index, (int)animation_index);
if (!igs)
	SG_RETURN_ERR_STR("AddMenuGameObj() FAILED");
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddMenuGameObjectByEquippableGlid, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0.0;
SG_POPARGS("d", &glid);
auto igs = pICE -> AddMenuGameObjectByEquippableGlidAsGetSet((int)glid);
if (!igs)
	SG_RETURN_ERR_STR("AddMenuGameObjectByEquippableGlid() FAILED");
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddMenuDynamicObjectByPlacement, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
SG_POPARGS("d", &objId);
auto igs = pICE -> AddMenuDynamicObjectByPlacementAsGetSet((int)objId);
if (!igs)
	SG_RETURN_ERR_STR("AddMenuDynamicObjByPlacement() FAILED");
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RemoveMenuGameObject, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
SG_POPARGS("O", &popObj);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->RemoveMenuGameObjectAsGetSet(igs);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectScreenshotCapture, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
const char* fname;
SG_POPARGS("Os", &popObj, &fname);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto path = pICE -> MenuGameObjectScreenshotCaptureAsGetSet(igs, ToString(fname));
SG_PUSHARGS("s", path.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectMoveToTop, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
SG_POPARGS("O", &popObj);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->MenuGameObjectMoveToTopAsGetSet(igs);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectMoveToBottom, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
SG_POPARGS("O", &popObj);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->MenuGameObjectMoveToBottomAsGetSet(igs);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectSetDOZoomFactor, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double zoomFactor = 1.0;
SG_POPARGS("Od", &popObj, &zoomFactor);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->MenuGameObjectSetDOZoomFactorAsGetSet(igs, (float)zoomFactor);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectSetViewportBackgroundColor, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double red = 0.0, gre = 0.0, blu = 0.0;
SG_POPARGS("Oddd", &popObj, &red, &gre, &blu);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->MenuGameObjectSetViewportBackgroundColorAsGetSet(igs, (int)red, (int)gre, (int)blu);
SG_RETURN;
SG_END_FN

// By setting a time the menu game object's animation will freeze frame at that time.
// Setting this to -1 disables this freeze frame and plays the animation as normal.
SG_BEGIN_FN(KEP_MenuGameObjectSetAnimationTime, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double animTimeMs = 0.0;
SG_POPARGS("Od", &popObj, &animTimeMs);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->MenuGameObjectSetAnimationTimeAsGetSet(igs, (TimeMs)animTimeMs);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectSetAnimation, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double animType = 0.0;
double animVer = 0.0;
SG_POPARGS("Odd", &popObj, &animType, &animVer);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->MenuGameObjectSetAnimationByTypeAsGetSet(igs, (eAnimType)(int)animType, (int)animVer);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectSetAnimationSettings, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double animGlid;
bool animDirForward;
double animLoopCtl; // 0=Default, 1=loop off, 2=loop on
SG_POPARGS("Odbd", &popObj, &animGlid, &animDirForward, &animLoopCtl);
AnimSettings animSettings(animGlid, animDirForward, (eAnimLoopCtl)(int)animLoopCtl);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> MenuGameObjectSetAnimationAsGetSet(igs, animSettings);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetInited, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
SG_POPARGS("d", &objId);
bool isInited = pICE->isDynamicObjectInited((int)objId);
SG_PUSHARGS("b", isInited);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetInfo, 16)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
SG_POPARGS("d", &objId);
std::string name, desc, customTexture, textureUrl;
bool mediaSupported, isAttachable, isInteractive, isLocal, isDerivable;
int assetId, globalId, invSubType, friendId, gameItemId, playerId;
DYN_OBJ_TYPE objType = DYN_OBJ_TYPE::INVALID;
pICE->GetDynamicObjectInfo(
	(int)objId,
	name,
	desc,
	mediaSupported,
	isAttachable,
	isInteractive,
	isLocal,
	isDerivable,
	assetId,
	friendId,
	customTexture,
	globalId,
	invSubType,
	textureUrl,
	gameItemId,
	playerId,
	objType);
SG_PUSHARGS("ssbbbbbddsddsddd",
	name.c_str(), desc.c_str(),
	mediaSupported, isAttachable, isInteractive, isLocal, isDerivable,
	(double)assetId, (double)friendId,
	customTexture.c_str(),
	(double)globalId, (double)invSubType,
	textureUrl.c_str(),
	(double)gameItemId, (double)playerId, (double)objType);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetBoundingBox, 6)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
SG_POPARGS("d", &objId);
float x1, y1, z1, x2, y2, z2;
pICE->GetDynamicObjectBoundingBox((int)objId, &x1, &y1, &z1, &x2, &y2, &z2);
SG_PUSHARGS("dddddd", (double)x1, (double)y1, (double)z1, (double)x2, (double)y2, (double)z2);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RotatePointAroundPoint, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double x1 = 0;
double y1 = 0;
double z1 = 0;
double x2 = 0;
double y2 = 0;
double z2 = 0;
double angleX = 0;
double angleY = 0;
double angleZ = 0;
SG_POPARGS("ddddddddd", &x1, &y1, &z1, &x2, &y2, &z2, &angleX, &angleY, &angleZ);
Vector3f outPos;
const Vector3f point((float)x1, (float)y1, (float)z1);
const Vector3f pivot((float)x2, (float)y2, (float)z2);
const Vector3f angle((float)angleX, (float)angleY, (float)angleZ);
pICE->RotatePointAroundPoint(outPos, point, pivot, angle);
SG_PUSHARGS("ddd", (double)outPos.x, (double)outPos.y, (double)outPos.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetPosition, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
Vector3f pos;
pICE->GetDynamicObjectPosition((int)objId, pos);
SG_PUSHARGS("ddd", (double)pos.x, (double)pos.y, (double)pos.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetPositionAnim, FOUR_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
float x, y, z, ry;
if (!pICE->GetDynamicObjectAnimPosRot((int)objId, x, y, z, ry)) {
	SG_PUSHARGS("dddd", (double)0.0, (double)0.0, (double)0.0, (double)0.0);
	SG_RETURN;
}
SG_PUSHARGS("dddd", (double)x, (double)y, (double)z, (double)ry);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetRotation, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
Vector3f rot;
pICE->GetDynamicObjectRotation((int)objId, rot);
SG_PUSHARGS("ddd", (double)rot.x, (double)rot.y, (double)rot.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetVisibility, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
eVisibility vis;
pICE->GetDynamicObjectVisibility((int)objId, vis);
bool bVisible = (vis == eVisibility::VISIBLE);
SG_PUSHARGS("b", bVisible);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectSetVisibility, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
bool bVisible = true;
SG_POPARGS("db", &objId, &bVisible);
eVisibility vis = bVisible ? eVisibility::VISIBLE : eVisibility::HIDDEN_FOR_ME;
bool bSuccess = pICE->SetDynamicObjectVisibility((int)objId, vis);
SG_PUSHARGS("b", (bool)bSuccess);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetDynamicObjectCount, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto count = pICE -> GetDynamicObjectCount();
SG_PUSHARGS("d", (double)count);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetDynamicObjectPlacementIdByPos, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
SG_POPARGS("d", &index);
auto objId = pICE -> GetDynamicObjectPlacementIdByPos((int)index);
SG_PUSHARGS("d", (double)objId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementIsValid, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
SG_POPARGS("d", &soundPlacementId);
bool valid = pICE->SoundPlacementValid((int)soundPlacementId);
SG_PUSHARGS("b", valid);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementGet, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
SG_POPARGS("d", &soundPlacementId);
auto igs = (IGetSet*)pICE -> SoundPlacementGet((int)soundPlacementId);
if (!igs) {
	LogError("SoundPlacementGet() FAILED - soundPlacementId=" << (int)soundPlacementId);
	SG_RETURN_NULL;
}
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementAdd, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
SG_POPARGS("d", &soundPlacementId);
bool ok = pICE->SoundPlacementAdd((int)soundPlacementId);
if (!ok) {
	LogError("SoundPlacementAdd() FAILED - soundPlacementId=" << (int)soundPlacementId);
}
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementDel, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId;
SG_POPARGS("d", &soundPlacementId);
bool ok = pICE->SoundPlacementDel((int)soundPlacementId);
if (!ok) {
	LogError("SoundPlacementDel() FAILED - soundPlacementId=" << (int)soundPlacementId);
}
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementGetPosition, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
SG_POPARGS("d", &soundPlacementId);
Vector3f pos;
if (!pICE->SoundPlacementGetPosition((int)soundPlacementId, pos)) {
	LogError("SoundPlacementGetPosition() FAILED - soundPlacementId=" << (int)soundPlacementId);
	SG_RETURN_NULL;
}
SG_PUSHARGS("ddd", (double)pos.x, (double)pos.y, (double)pos.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementGetRotation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
SG_POPARGS("d", &soundPlacementId);
float radians = 0.0;
if (!pICE->SoundPlacementGetRotationRad((int)soundPlacementId, radians)) {
	LogError("SoundPlacementGetRotationRad() FAILED - soundPlacementId=" << (int)soundPlacementId);
	SG_RETURN_NULL;
}
SG_PUSHARGS("d", (double)radians);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementSetPosition, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
Vector3d pos;
SG_POPARGS("dddd", &soundPlacementId, &pos.x, &pos.y, &pos.z);
if (!pICE->SoundPlacementSetPosition((int)soundPlacementId, Vector3f(pos))) {
	LogError("SoundPlacementSetPosition() FAILED - soundPlacementId=" << (int)soundPlacementId);
	SG_RETURN_NULL;
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementSetRotationVector, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
Vector3d dir;
SG_POPARGS("dddd", &soundPlacementId, &dir.x, &dir.y, &dir.z);
if (!pICE->SoundPlacementSetDirection((int)soundPlacementId, Vector3f(dir))) {
	LogError("SoundPlacementSetDirection() FAILED - soundPlacementId=" << (int)soundPlacementId);
	SG_RETURN_NULL;
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetSoundPlacementIdList, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
std::vector<int> soundPlacementIdList;
if (!pICE->SoundPlacementsGetIds(soundPlacementIdList))
	SG_RETURN_ERR_STR("SoundPlacementsGetIds() FAILED");
auto count = (int)soundPlacementIdList.size();
SG_RESET_RETURN_COUNT(count);
for (int i = 0; i < count; i++) {
	SG_PUSH(soundPlacementIdList[i]);
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementGetInfo, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
SG_POPARGS("d", &soundPlacementId);
std::string name;
GLID glid = GLID_INVALID;
if (!pICE->SoundPlacementGetInfo((int)soundPlacementId, name, glid)) {
	LogError("SoundPlacementGetInfo() FAILED - soundPlacementId=" << (int)soundPlacementId);
	SG_RETURN_NULL;
}
SG_PUSHARGS("sd", name.c_str(), (double)glid);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementSetRotation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0, radians = 0.0;
SG_POPARGS("dd", &soundPlacementId, &radians);
bool ok = pICE->SoundPlacementSetRotationRad((int)soundPlacementId, static_cast<float>(radians));
if (!ok) {
	LogError("SoundPlacementSetRotationRad() FAILED - soundPlacementId=" << (int)soundPlacementId);
}
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_EnableSoundPlacementRender, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool toRender = false;
SG_POPARGS("b", &toRender);
if (!pICE->SoundPlacementsEnableRender(toRender)) {
	LogError("SoundPlacementsEnableRender() FAILED");
	SG_RETURN_NULL;
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlaySoundOnDynamicObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
double glid = 0.0;
SG_POPARGS("dd", &objId, &glid);
int soundPlacementId = pICE->PlaySoundOnDynamicObject((int)objId, (GLID)glid);
if (soundPlacementId == 0) {
	LogError("PlaySoundOnDynamicObject() FAILED - objId=" << (int)objId << " glid=" << (GLID)glid);
}
SG_PUSHARGS("d", (double)soundPlacementId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RemoveSoundOnDynamicObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
double glid = 0.0;
SG_POPARGS("dd", &objId, &glid);
bool ok = pICE->RemoveSoundOnDynamicObject((int)objId, (GLID)glid);
if (!ok) {
	LogError("RemoveSoundOnDynamicObject() FAILED - objId=" << (int)objId << " glid=" << (GLID)glid);
}
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlaySoundOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double netId = 0.0;
double glid = 0.0;
SG_POPARGS("dd", &netId, &glid);
int soundPlacementId = pICE->PlaySoundOnPlayer((int)netId, (GLID)glid);
if (soundPlacementId == 0) {
	LogError("PlaySoundOnPlayer() FAILED - netId=" << (int)netId << " glid=" << (GLID)glid);
}
SG_PUSHARGS("d", (double)soundPlacementId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RemoveSoundOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double netId = 0.0;
double glid = 0.0;
SG_POPARGS("dd", &netId, &glid);
bool ok = pICE->RemoveSoundOnPlayer((int)netId, (GLID)glid);
if (!ok) {
	LogError("RemoveSoundOnPlayer() FAILED - netId=" << (int)netId << " glid=" << (GLID)glid);
}
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementPlaySound, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
double glid = 0.0;
SG_POPARGS("dd", &soundPlacementId, &glid);
bool ok = pICE->SoundPlacementPlaySound((int)soundPlacementId, (GLID)glid);
if (!ok) {
	LogError("SoundPlacementPlaySound() FAILED - soundPlacementId=" << (int)soundPlacementId);
}
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SoundPlacementAddModifier, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double soundPlacementId = 0.0;
double input, output, gain, offset;
SG_POPARGS("ddddd", &soundPlacementId, &input, &output, &gain, &offset);
bool ok = pICE->SoundPlacementAddModifier((int)soundPlacementId, (int)input, (int)output, gain, offset);
if (!ok) {
	LogError("SoundPlacementAddModifier() FAILED - soundPlacementId=" << (int)soundPlacementId);
}
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetName, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objectId = 0;
SG_POPARGS("d", &objectId);
auto name = pICE -> GetDynamicObjectName((int)objectId);
SG_PUSHARGS("s", name.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectGetGLID, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
auto glid = pICE -> GetDynamicObjectGlid((int)objId);
SG_PUSHARGS("d", (double)glid);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectPlacementTemplateSave, ONE_RETURN_VALUE)
#if DEPRECATED
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
auto ok = pICE -> DynamicObjectPlacementTemplateSave((int)objId);
SG_PUSHARGS("b", ok);
SG_RETURN;
#else
SG_PUSHARGS("b", false);
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectRefreshTextures, ONE_RETURN_VALUE)
#if DEPRECATED
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
auto ok = pICE -> DynamicObjectRefreshTextures((int)objId);
SG_PUSHARGS("b", ok);
SG_RETURN;
#else
SG_PUSHARGS("b", false);
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(KEP_AssignDynamicObjectToTemplate, ONE_RETURN_VALUE)
#if DEPRECATED
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
auto ok = pICE -> AssignDynamicObjectToTemplate((int)objId);
SG_PUSHARGS("b", ok);
SG_RETURN;
#else
SG_PUSHARGS("b", false);
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(KEP_StartDynamicObjectDerivationByPlacementId, ONE_RETURN_VALUE)
#if DEPRECATED
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
auto ok = pICE -> StartDynamicObjectDerivationByPlacementId((long)objId);
SG_PUSHARGS("b", ok);
SG_RETURN;
#else
SG_PUSHARGS("b", false);
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(KEP_SubmitDynamicObjectDerivationByPlacementId, ONE_RETURN_VALUE)
#if DEPRECATED
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_DECLARE_POP_OBJ(IEvent, e, sgEvent);
SG_POPARGS("dO", &objId, &sgEvent);
if (!SG_CAST_TO_DERIVED_OBJECT(e, IEvent, IEvent, sgEvent))
	SG_RETURN_ERR_STR("Failed Casting 2nd Parameter (event)");
auto ok = pICE -> SubmitDynamicObjectDerivationByPlacementId((long)objId, e);
SG_PUSHARGS("b", ok);
SG_RETURN;
#else
SG_PUSHARGS("b", false);
SG_RETURN;
#endif
SG_END_FN

SG_BEGIN_FN(KEP_BakeCharacterAnimation, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
SG_POPARGS("d", &glid);
auto igs = pICE -> GetMovementObjectMeAsGetSet();
if (!igs)
	SG_RETURN_ERR_STR("GetMovementObjMeAsGetSet() FAILED");
if (!pICE->BakeAnimation_MovementObj(igs, (GLID)glid))
	SG_RETURN_ERR_STR("BakeCharacterAnimation() FAILED");
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_BakeAnimation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = 0, objId = 0, glid = 0;
SG_POPARGS("ddd", &objType, &objId, &glid);
auto ok = pICE -> ObjectBakeAnimation(DoubleToObjType(objType), (int)objId, (GLID)glid);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MenuGameObjectSetEquippableConfig, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double glid = 0, slot = 0, meshId = 0, matlId = 0;
SG_POPARGS("Odddd", &popObj, &glid, &slot, &meshId, &matlId);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetMenuGameObjectEquippableAltCharConfig(igs, (GLID_OR_BASE)glid, (int)slot, (int)meshId, (int)matlId);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

// DRF - TODO - Change glids to AnimSettings
SG_BEGIN_FN(KEP_PlayerObjectSetEquippableAnimation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double animationGLID = 0, accessoryGLID = 0;
SG_POPARGS("Odd", &popObj, &accessoryGLID, &animationGLID);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetPlayerEquippableAnimation(igs, (GLID)accessoryGLID, (GLID)animationGLID, true);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlayerObjectTryEquippableAnimation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double animationGLID = 0, accessoryGLID = 0;
SG_POPARGS("Odd", &popObj, &accessoryGLID, &animationGLID);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetPlayerEquippableAnimation(igs, (GLID)accessoryGLID, (GLID)animationGLID, false);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlayerObjectSetEquippableConfig, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double glid = 0, slot = 0, mesh = 0, mat = 0;
SG_POPARGS("Odddd", &popObj, &glid, &slot, &mesh, &mat);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetPlayerEquippableAltCharConfig(igs, (int)glid, (int)slot, (int)mesh, (int)mat);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerInfo, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
std::vector<std::string> info;
pICE->GetMyInfo(info);
auto count = (int)info.size();
SG_RESET_RETURN_COUNT(count);
for (int i = 0; i < count; i++) {
	SG_PUSH(info[i].c_str());
}
SG_RETURN;
SG_END_FN

std::vector<GLID> KEP_GetPlayerArmedItems(ScriptPointer<IGetSet> pGS_MO) {
	auto pICE = IClientEngine::Instance();
	return pICE ? pICE->GetPlayerArmedItemsGS(pGS_MO) : std::vector<GLID>();
}

SG_BEGIN_FN(KEP_GetPlayerConfigList, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double playerID = 0;
SG_POPARGS("d", &playerID); // unused
std::vector<std::string> meshIds;
if (!pICE->GetMyPlayerCharConfigBaseMeshIds(meshIds))
	SG_RETURN_ERR_STR("GetMyPlayerCharConfigBaseMeshIds() FAILED");
auto count = (int)meshIds.size();
SG_RESET_RETURN_COUNT(count);
for (int i = 0; i < count; i++) {
	SG_PUSH(meshIds[i].c_str());
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerMaterialList, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double playerID = 0;
SG_POPARGS("d", &playerID); // unused
std::vector<std::string> matlIds;
if (!pICE->GetMyPlayerCharConfigBaseMatlIds(matlIds))
	SG_RETURN_ERR_STR("GetMyPlayerCharConfigBaseMatlIds() FAILED");
auto count = (int)matlIds.size();
SG_RESET_RETURN_COUNT(count);
for (int i = 0; i < count; i++) {
	SG_PUSH(matlIds[i].c_str());
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCharacterAltConfig, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double baseSlot = 0.0, meshId = 0.0, matlId = 0.0;
SG_POPARGS("Oddd", &popObj, &baseSlot, &meshId, &matlId);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetMenuGameObjectAltCharConfig(igs, (int)baseSlot, (int)meshId, (int)matlId);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetActorTypeOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double actorIndex = 0.0;
SG_POPARGS("Od", &popObj, &actorIndex);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetPlayerActorType(igs, (int)actorIndex);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ScaleRuntimePlayer, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double xfactor = 0.0, yfactor = 0.0, zfactor = 0.0;
SG_POPARGS("Oddd", &popObj, &xfactor, &yfactor, &zfactor);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
pICE->ScaleRuntimePlayer(igs, (float)xfactor, (float)yfactor, (float)zfactor);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerConfigIndex, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double configGlid = 0.0, configSlot = 0.0;
SG_POPARGS("Odd", &popObj, &configGlid, &configSlot);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto id = pICE -> GetPlayerCharConfigMeshId(igs, (int)configGlid, (int)configSlot);
SG_PUSHARGS("d", (double)id);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerConfigMaterial, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double configGlid = 0.0, configSlot = 0.0;
SG_POPARGS("Odd", &popObj, &configGlid, &configSlot);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto id = pICE -> GetPlayerCharConfigMatlId(igs, (int)configGlid, (int)configSlot);
SG_PUSHARGS("d", (double)id);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetCharacterAttachmentTypes, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
SG_POPARGS("O", &popObj);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
std::vector<std::string> attachments;
pICE->GetPlayerAttachmentTypes(igs, attachments);
auto count = (int)attachments.size();
SG_RESET_RETURN_COUNT(count);
for (int i = 0; i < count; i++) {
	SG_PUSH(attachments[i].c_str());
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCharacterAltConfigOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double baseSlot = 0.0, meshId = 0.0, matlId = 0.0;
SG_POPARGS("Oddd", &popObj, &baseSlot, &meshId, &matlId);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetPlayerAltCharConfig(igs, (int)baseSlot, (int)meshId, (int)matlId);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCharacterAltConfigColor, ONE_RETURN_VALUE)
#if DEAD_CODE
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double baseSlot = 0.0, meshId = 0.0, R, G, B, A;
SG_POPARGS("Odddddd", &popObj, &baseSlot, &meshId, &R, &G, &B, &A);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> SetMenuGameObjectAltCharConfigColor(igs, (int)baseSlot, (int)meshId, (float)R, (float)G, (float)B, (float)A);
#endif
SG_PUSHARGS("b", false); //ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RequestCharacterInventory, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double invType = 0.0;
SG_POPARGS("d", &invType);
auto ok = pICE -> RequestCharacterInventory((eAttribEventType)(int)invType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RequestCharacterInventoryAttachableObjects, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ok = pICE -> RequestCharacterInventoryAttachableObjects();
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RequestPictureFrames, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ok = pICE -> RequestPictureFrames();
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ArmMenuGameObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double glid = 0, material = 0;
SG_POPARGS("Odd", &popObj, &glid, &material);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> ArmEquippableItem_GS_MGO((GLID)glid, igs, (int)material);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ArmInventoryItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
SG_POPARGS("d", &glid);
auto ok = pICE -> ArmInventoryItem_GS_MO((GLID)glid, nullptr);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ArmInventoryItemOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double glid = 0;
SG_POPARGS("Od", &popObj, &glid);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> ArmInventoryItem_GS_MO((GLID)glid, igs);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ArmEquippableItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double glid = 0, material = 0;
SG_POPARGS("Odd", &popObj, &glid, &material);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> ArmEquippableItem_GS_MO((GLID)glid, igs, (int)material);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DisarmInventoryItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
SG_POPARGS("d", &glid);
auto ok = pICE -> DisarmInventoryItem_GS_MO((GLID)glid, nullptr);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DisarmInventoryItemOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double glid = 0;
SG_POPARGS("Od", &popObj, &glid);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> DisarmInventoryItem_GS_MO((GLID)glid, igs);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DisarmEquippableItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
double glid = 0;
SG_POPARGS("Od", &popObj, &glid);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto ok = pICE -> DisarmEquippableItem_GS_MO((GLID)glid, igs);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UseInventoryItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, invType = 0;
SG_POPARGS("dd", &glid, &invType);
auto ok = pICE -> UseInventoryItem((GLID)glid, (short)invType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UseInventoryItemOnTarget, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, objType, objId, invType;
SG_POPARGS("dddd", &glid, &objType, &objId, &invType);
auto ok = pICE -> UseInventoryItemOnTarget((GLID)glid, (int)objType, (int)objId, (short)invType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RemoveInventoryItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, qty, invType;
SG_POPARGS("ddd", &glid, &qty, &invType);
auto ok = pICE -> RemoveInventoryItem((GLID)glid, (int)qty, (short)invType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RemoveBankItem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, qty, invType;
SG_POPARGS("ddd", &glid, &qty, &invType);
auto ok = pICE -> RemoveBankItem((GLID)glid, (int)qty, (short)invType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

// Put Inventory Item In 'Storage'
SG_BEGIN_FN(KEP_DepositItemToBank, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, nQuantity, nInvType;
SG_POPARGS("ddd", &glid, &nQuantity, &nInvType);
pICE->DepositItemToBank((GLID)glid, (int)nQuantity, (short)nInvType);
SG_RETURN;
SG_END_FN

// Restore 'Storage' Item To Inventory
SG_BEGIN_FN(KEP_WithdrawItemFromBank, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, nQuantity, nInvType;
SG_POPARGS("ddd", &glid, &nQuantity, &nInvType);
pICE->WithdrawItemFromBank((GLID)glid, (int)nQuantity, (short)nInvType);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetRuntimeObjDistance, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double netTraceId;
SG_POPARGS("d", &netTraceId);
auto dist = pICE -> GetMyDistanceToMovementObject((int)netTraceId);
SG_PUSHARGS("d", (double)dist);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WorldToScreen, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double X, Y, Z;
SG_POPARGS("ddd", &X, &Y, &Z);
Vector3f wld, scr;
wld.x = (float)X;
wld.y = (float)Y;
wld.z = (float)Z;
scr.x = scr.y = scr.z = 0.0f;
if (!pICE->WorldToScreen(wld, &scr))
	SG_RETURN_ERR_STR("WorldToScreen() FAILED");
SG_PUSHARGS("sss", std::to_string(scr.x).c_str(), std::to_string(scr.y).c_str(), std::to_string(scr.z).c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMasterVolume, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
double volPct;
SG_POPARGS("d", &volPct);
auto ok = pICE -> SetMasterVolume(volPct);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMasterVolume, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
double volPct = pICE->GetMasterVolume();
SG_PUSHARGS("bd", true, volPct);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMute, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
bool mute;
SG_POPARGS("b", &mute);
auto ok = pICE -> SetMute(mute);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMute, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
bool mute = pICE->GetMute();
SG_PUSHARGS("bb", true, mute);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMediaEnabled, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
bool enabled;
SG_POPARGS("b", &enabled);
auto ok = pICE -> SetMediaEnabled(enabled);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMediaEnabled, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
auto enabled = pICE -> GetMediaEnabled();
SG_PUSHARGS("b", enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMediaParamsOnDynamicObject, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
double objectId;
const char* mUrl = 0;
const char* mParams = 0;
double mVolPct = -1; // no change
double mRadiusAudio = -1; // no change
double mRadiusVideo = -1; // no change
SG_POPARGS("dssddd", &objectId, &mUrl, &mParams, &mVolPct, &mRadiusAudio, &mRadiusVideo);
MediaParams mediaParams(ToString(mUrl), ToString(mParams), mVolPct, mRadiusAudio, mRadiusVideo);
auto ok = pICE -> SetMediaParamsOnDynamicObject((int)objectId, mediaParams);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMediaParamsOnDynamicObject, RETURN_VALUES(6))
SG_DECLARE_ICE_PTR(pICE);
double objectId;
SG_POPARGS("d", &objectId);
MediaParams mediaParams;
auto ok = pICE -> GetMediaParamsOnDynamicObject((int)objectId, mediaParams);
SG_PUSHARGS("bssddd", ok, mediaParams.GetUrl().c_str(), mediaParams.GetParams().c_str(), mediaParams.GetVolume(), mediaParams.GetRadiusAudio(), mediaParams.GetRadiusVideo());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMediaVolumeOnDynamicObject, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
double objectId, volPct;
SG_POPARGS("dd", &objectId, &volPct);
auto ok = pICE -> SetMediaVolumeOnDynamicObject((int)objectId, volPct);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMediaVolumeOnDynamicObject, RETURN_VALUES(2))
SG_DECLARE_ICE_PTR(pICE);
double objectId;
double volPct = 0;
SG_POPARGS("d", &objectId);
auto ok = pICE -> GetMediaVolumeOnDynamicObject((int)objectId, volPct);
SG_PUSHARGS("bd", ok, volPct);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMediaRadiusOnDynamicObject, RETURN_VALUES(1))
SG_DECLARE_ICE_PTR(pICE);
double objectId;
double radiusAudio, radiusVideo;
SG_POPARGS("ddd", &objectId, &radiusAudio, &radiusVideo);
MediaRadius radius(radiusAudio, radiusVideo);
auto ok = pICE -> SetMediaRadiusOnDynamicObject((int)objectId, radius);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMediaRadiusOnDynamicObject, RETURN_VALUES(3))
SG_DECLARE_ICE_PTR(pICE);
double objectId;
MediaRadius radius;
SG_POPARGS("d", &objectId);
auto ok = pICE -> GetMediaRadiusOnDynamicObject((int)objectId, radius);
SG_PUSHARGS("bdd", ok, radius.GetAudio(), radius.GetVideo());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_StartPlayingFlashOnObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double traceId;
const char* mUrl = 0;
const char* mParams = 0;
SG_POPARGS("dss", &traceId, &mUrl, &mParams);
MediaParams mediaParams(ToString(mUrl), ToString(mParams));
auto ok = pICE -> StartMediaOnWorldObjectMesh((int)traceId, mediaParams);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_StartPlayingFlashOnObjectSecure, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double traceId;
const char* mUrl = 0;
const char* mParams = 0;
SG_POPARGS("dss", &traceId, &mUrl, &mParams);
MediaParams mediaParams(ToString(mUrl), ToString(mParams));
auto ok = pICE -> StartMediaOnWorldObjectMesh((int)traceId, mediaParams);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_StopPlayingFlashOnObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double traceId;
SG_POPARGS("d", &traceId);
auto ok = pICE -> StopMediaOnWorldObjectMesh((int)traceId);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_StartPlayingFlashOnNamedObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* name = 0;
const char* mUrl = 0;
const char* mParams = 0;
SG_POPARGS("sss", &name, &mUrl, &mParams);
MediaParams mediaParams(ToString(mUrl), ToString(mParams));
auto ok = pICE -> StartMediaOnWorldObjectMesh(ToString(name), mediaParams);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_StartPlayingFlashOnNamedObjectSecure, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* name = 0;
const char* mUrl = 0;
const char* mParams = 0;
SG_POPARGS("sss", &name, &mUrl, &mParams);
MediaParams mediaParams(ToString(mUrl), ToString(mParams));
auto ok = pICE -> StartMediaOnWorldObjectMesh(ToString(name), mediaParams);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_StopPlayingFlashOnNamedObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* name = 0;
SG_POPARGS("s", &name);
auto ok = pICE -> StopMediaOnWorldObjectMesh(ToString(name));
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetAnimationInfo, 6)
SG_DECLARE_ICE_PTR(pICE);
double objType, objId, glid;
SG_POPARGS("ddd", &objType, &objId, &glid);
TimeMs animDurationMs = 0.0;
TimeMs minPlay = 0.0;
bool looping = false;
float speedScale = 1.0f;
TimeMs cropStart = 0.0;
TimeMs cropEnd = -1.0;
if (pICE->ObjectGetAnimInfo(DoubleToObjType(objType), (int)objId, (GLID)glid, animDurationMs, minPlay, looping, speedScale, cropStart, cropEnd)) {
	SG_PUSHARGS("ddbddd", (double)animDurationMs, (double)minPlay, looping, (double)speedScale, (double)cropStart, (double)cropEnd);
} else {
	LogError("GetAnimationInfo() FAILED");
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetBoneIndexByName, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
SG_DECLARE_POP_OBJ(IGetSet, igs, popObj);
char* boneName = NULL;
SG_POPARGS("Os", &popObj, &boneName);
SG_VALID_CAST_FROM_POP_OBJ(igs, popObj);
auto id = pICE -> GetBoneIndexByName(igs, ToString(boneName));
SG_PUSH((double)id);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_BuyItemFromStore, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, nQuantity, nCurrencyType;
SG_POPARGS("ddd", &glid, &nQuantity, &nCurrencyType);
auto ok = pICE -> BuyItemFromStore((GLID)glid, (int)nQuantity, (int)nCurrencyType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SellItemToStore, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, nQuantity;
SG_POPARGS("dd", &glid, &nQuantity);
auto ok = pICE -> SellItemToStore((GLID)glid, (int)nQuantity);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SendInteractCommand, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->SendInteractCommand();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddUGCTexture, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
const char* element_name;
SG_POPARGS("ds", &glid, &element_name);
pICE->AddUGCTexture((GLID)glid, ToString(element_name));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ChangeAnimationCropping, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType, objId, glid = 0, cropStart, cropEnd;
SG_POPARGS("ddddd", &objType, &objId, &glid, &cropStart, &cropEnd);
pICE->ObjectSetAnimCropTimes(DoubleToObjType(objType), (int)objId, (GLID)glid, (TimeMs)cropStart, (TimeMs)cropEnd);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ChangeAnimationLooping, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType, objId, glid = 0;
bool looping;
SG_POPARGS("dddb", &objType, &objId, &glid, &looping);
pICE->ObjectSetAnimLooping(DoubleToObjType(objType), (int)objId, (GLID)glid, looping);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ChangeAnimationSpeed, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType, objId, glid = 0, speedScale;
SG_POPARGS("dddd", &objType, &objId, &glid, &speedScale);
pICE->ObjectSetAnimSpeedFactor(DoubleToObjType(objType), (int)objId, (GLID)glid, (float)speedScale);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetControlConfiguration, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
std::vector<std::string> config;
if (!pICE->GetControlConfiguration(config))
	SG_RETURN_ERR_STR("GetControlConfiguration() FAILED");
auto count = (int)config.size();
SG_RESET_RETURN_COUNT(1);
std::string configuration = "";
for (int i = 0; i < count; i++) {
	if (i == 0) {
		configuration += config[i];
	} else {
		configuration += ",";
		configuration += config[i];
	}
}
SG_PUSHARGS("s", configuration.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UnassignControl, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index;
SG_POPARGS("d", &index);
auto ok = pICE -> UnassignControlByIndex((int)index);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PendingControlAssignment, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index;
SG_POPARGS("d", &index);
auto ok = pICE -> PendingControlAssignment((int)index);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RestoreDefaultControlConfiguration, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ok = pICE -> RestoreDefaultControlConfiguration();
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SaveControlConfiguration, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ok = pICE -> SaveControlConfiguration();
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(KEP_SelectQuest, ONE_RETURN_VALUE)
SG_PUSHARGS("b", false);
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(KEP_RequestSkills, ONE_RETURN_VALUE)
SG_PUSHARGS("b", false);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SendChatMessage, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double chatType;
const char* msg;
SG_POPARGS("ds", &chatType, &msg);
auto ok = pICE -> SendChatMessage((eChatType)(int)chatType, ToString(msg));
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SendChatMessageBroadcast, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double chatType;
const char* msg;
const char* toDistribution;
const char* fromRecipient;
SG_POPARGS("dsss", &chatType, &msg, &toDistribution, &fromRecipient);
auto ok = pICE -> SendChatMessageBroadcast((eChatType)(int)chatType, ToString(msg), toDistribution, fromRecipient);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(KEP_UseSkillAbility, NO_RETURN_VALUE)
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ConfigOpen, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
const char* fname;
const char* rootNode;
SG_POPARGS("ss", &fname, &rootNode);
std::string errMsg;
auto* ikc = pICE -> OpenConfig(fname, rootNode, errMsg);
if (!ikc) {
	SG_PUSHARGS("ds", (double)0.0, errMsg.c_str());
	SG_RETURN;
}
SG_VALID_CAST_TO_PUSH_OBJ(ikc, pushObj);
SG_PUSHARGS("Os", pushObj, errMsg.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_FileOpen, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
const char* fname;
const char* access;
const char* dir;
SG_POPARGS("sss", &fname, &access, &dir);
std::string errMsg;
auto ikf = pICE -> OpenFile(fname, access, errMsg, dir);
if (!ikf) {
	SG_PUSHARGS("ds", (double)0.0, errMsg.c_str());
	SG_RETURN;
}
SG_VALID_CAST_TO_PUSH_OBJ(ikf, pushObj);
SG_PUSHARGS("Os", pushObj, errMsg.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Delete3DAppAsset, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* name;
double assetType;
SG_POPARGS("sd", &name, &assetType);
auto ok = pICE -> DeleteAsset((int)assetType, name);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetZoneIndex, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto zoneIndex = pICE -> GetZoneIndexAsULong();
SG_PUSHARGS("d", (double)zoneIndex);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetZoneIndexType, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto zoneIndexType = pICE -> GetZoneIndexType();
SG_PUSHARGS("d", (double)zoneIndexType);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetZoneFileName, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto name = pICE -> GetZoneFileName();
SG_PUSHARGS("s", name.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetEnvironment, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto igs = pICE -> GetEnvironment();
if (!igs)
	SG_RETURN_ERR_STR("GetEnvironment() FAILED");
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(KEP_DisableSelectWidgetDisplay, NO_RETURN_VALUE)
SG_RETURN;
SG_END_FN

// DEPRECATED
SG_BEGIN_FN(KEP_EnableSelectWidgetDisplay, NO_RETURN_VALUE)
SG_RETURN;
SG_END_FN

// Override animation settings replace normal avatar animations
SG_BEGIN_FN(KEP_SetCurrentAnimation, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double animType, animVer;
SG_POPARGS("dd", &animType, &animVer);
pICE->setMyOverrideAnimationByType((eAnimType)(int)animType, (int)animVer);
SG_RETURN;
SG_END_FN

// DEPRECATED - Use KEP_SetMyOverrideAnimationSettings() Instead !!!
SG_BEGIN_FN(KEP_SetCurrentAnimationByGLID, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double animGlid = 0;
SG_POPARGS("d", &animGlid);
AnimSettings animSettings(animGlid);
auto ok = pICE -> setMyOverrideAnimationSettings(animSettings);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

// NEW - Use Me !!!
// Override animation settings replace normal avatar animations
SG_BEGIN_FN(KEP_SetMyOverrideAnimationSettings, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double animGlid;
bool animDirForward;
double animLoopCtl; // 0=Default, 1=loop off, 2=loop on
SG_POPARGS("dbd", &animGlid, &animDirForward, &animLoopCtl);
AnimSettings animSettings(animGlid, animDirForward, (eAnimLoopCtl)(int)animLoopCtl);
auto ok = pICE -> setMyOverrideAnimationSettings(animSettings);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

// DEPRECATED - Use KEP_GetMyPrimaryAnimationSettings() Instead !!!
SG_BEGIN_FN(KEP_GetCurrentAnimation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
AnimSettings animSettings;
auto ok = pICE -> getMyPrimaryAnimationSettings(animSettings);
GLID animGlid = ok ? animSettings.m_glid : -1; // legacy return value for error
SG_PUSHARGS("d", (double)animGlid);
SG_RETURN;
SG_END_FN

// NEW - Use Me !!!
// Primary animations are in current use and include normal avatar animations
SG_BEGIN_FN(KEP_GetMyPrimaryAnimationSettings, FOUR_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
AnimSettings animSettings;
auto ok = pICE -> getMyPrimaryAnimationSettings(animSettings);
SG_PUSHARGS("bdbd", ok, (double)animSettings.m_glid, animSettings.m_dirForward, (double)animSettings.m_loopCtl);
SG_RETURN;
SG_END_FN

// NEW - Use Me !!!
// Override animation settings replace normal avatar animations
SG_BEGIN_FN(KEP_GetMyOverrideAnimationSettings, FOUR_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
AnimSettings animSettings;
auto ok = pICE -> getMyOverrideAnimationSettings(animSettings);
SG_PUSHARGS("bdbd", ok, (double)animSettings.m_glid, animSettings.m_dirForward, (double)animSettings.m_loopCtl);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetControlConfig, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index;
SG_POPARGS("d", &index);
pICE->SetControlConfig((int)index);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMenuSelectMode, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool blook;
SG_POPARGS("b", &blook);
pICE->SetMenuSelectMode(blook);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DeleteDynamicObjectPlacement, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
SG_POPARGS("d", &index);
auto ok = pICE -> SendRemoveDynamicObjectEvent((int)index);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddDynamicObjectPlacement, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
const char* type;
SG_POPARGS("ds", &index, &type);
auto ok = pICE -> AddDynamicObjectPlacement((int)index, type);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetDynamicObjectPosition, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double id = 0;
Vector3d pos(0, 0, 0);
SG_POPARGS("dddd", &id, &pos.x, &pos.y, &pos.z);
auto ok = pICE -> SetDynamicObjectPosition((int)id, Vector3f(pos));
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MoveDynamicObjectPlacement, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0, dir = 0, slide = 0, lift = 0;
SG_POPARGS("dddd", &index, &dir, &slide, &lift);
auto ok = pICE -> MoveDynamicObjectPlacement((int)index, dir, slide, lift);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_MoveDynamicObjectPlacementInDirection, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0, x = 0, y = 0, z = 0, dist = 0;
SG_POPARGS("ddddd", &index, &x, &y, &z, &dist);
auto ok = pICE -> MoveDynamicObjectPlacementInDirection((int)index, x, y, z, dist);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayersActiveCamera, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto igs = pICE -> GetActiveCameraForMeAsGetSet();
if (!igs)
	SG_RETURN_ERR_STR("GetPlayersActiveCamera() FAILED");
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerCameraOrientation, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
Vector3f camdir = pICE->GetActiveCameraOrientationForMe();
SG_PUSHARGS("sss", std::to_string(camdir.x).c_str(), std::to_string(camdir.y).c_str(), std::to_string(camdir.z).c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetPlayerLookAt, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double x, y, z;
SG_POPARGS("ddd", &x, &y, &z);
auto ok = pICE -> SetMyLookAt((float)x, (float)y, (float)z);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerCameraUpVector, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
Vector3f up = pICE->GetActiveCameraUpVectorForMe();
SG_PUSHARGS("ddd", (double)up.x, (double)up.y, (double)up.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SaveDynamicObjectPlacementChanges, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
SG_POPARGS("d", &index);
auto ok = pICE -> SaveDynamicObjectPlacementChanges((int)index);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CancelDynamicObjectPlacementChanges, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
SG_POPARGS("d", &index);
auto ok = pICE -> CancelDynamicObjectPlacementChanges((int)index);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_IsDynamicPlacementObjectCustomizable, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
auto customizable = pICE -> IsDynamicObjectCustomizable((int)objId);
SG_PUSHARGS("b", customizable);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SaveWldObjectChanges, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
const char* textureURL;
SG_POPARGS("ds", &index, &textureURL);
pICE->SaveWorldObjectChanges((int)index, textureURL);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CancelWldObjectChanges, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
SG_POPARGS("d", &index);
pICE->CancelWorldObjectChanges((int)index);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetDynamicObjectRotation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
Vector3d rot(0, 0, 0);
SG_POPARGS("dddd", &index, &rot.x, &rot.y, &rot.z);
auto ok = pICE -> SetDynamicObjectRotation((int)index, Vector3f(rot));
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ApplyGiftPicture, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0, giftId = 0;
bool isThumb = false;
SG_POPARGS("ddb", &index, &giftId, &isThumb);
auto ok = pICE -> ApplyGiftPicture((int)index, (int)giftId, isThumb);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ApplyFriendPicture, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0, friendId = 0;
bool isThumb = false;
SG_POPARGS("ddb", &index, &friendId, &isThumb);
auto ok = pICE -> ApplyFriendPicture((int)index, (int)friendId, isThumb);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ApplyCustomTexture, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0, assetId = 0;
bool isThumb = false;
const char* textureURL;
SG_POPARGS("ddbs", &index, &assetId, &isThumb, &textureURL);
if (!textureURL)
	SG_RETURN_ERR_STR("Invalid 4th Parameter (textureURL)");
auto ok = pICE -> ApplyCustomTexture((int)index, (int)assetId, isThumb, textureURL);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ApplyCustomTextureWldObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0, assetId = 0;
bool isThumb = false;
SG_POPARGS("ddb", &index, &assetId, &isThumb);
auto ok = pICE -> ApplyCustomTextureWorldObject((int)index, (int)assetId, isThumb);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetWorldObjectInfo, 5)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
SG_POPARGS("d", &index);
std::string customTexture, textureUrl;
int assetId;
bool canHangPictures = false, canChangeTexture = false;
pICE->GetWorldObjectInfo((int)index, &assetId, &customTexture, &canHangPictures, &canChangeTexture, &textureUrl);
SG_PUSHARGS("dsbbs", (double)assetId, customTexture.c_str(), canHangPictures, canChangeTexture, textureUrl.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ArmPreLoadProgress, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ie = pICE -> GetDispatcher() -> MakeEvent(pICE->GetDownloadZoneEventId());
auto ok = pICE -> ArmPreLoadProgress(ie);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DownloadPatchFile, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double filesize = 0;
const char* zonefilename;
SG_POPARGS("sd", &zonefilename, &filesize);
auto ie = pICE -> GetDispatcher() -> MakeEvent(pICE->GetDownloadZoneEventId());
auto ok = pICE -> DownloadPatchFile(zonefilename, ie, (FileSize)filesize);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CancelAllDownloads, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->CancelAllDownloads();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CancelPatch, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ok = pICE -> CancelPatch();
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DownloadTexturePack, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double filesize = 0;
const char* zonefilename;
SG_POPARGS("sd", &zonefilename, &filesize);
IEvent* e = pICE->GetDispatcher()->MakeEvent(pICE->GetDownloadZoneEventId());
(*e->OutBuffer()) << 0;
std::string preStr = "\\MapsModels\\";
preStr += zonefilename;
int loc = preStr.find_last_of('.');
std::string subDir = preStr.substr(0, loc);
subDir += '\\';
auto ok = pICE -> DownloadPatchFile(zonefilename, subDir.c_str(), e, (FileSize)filesize);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_TestPatchFileForUpdate, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* zonefilename;
SG_POPARGS("s", &zonefilename);
auto ok = pICE -> TestPatchFileForUpdate(zonefilename);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LaunchBrowser, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* url;
SG_POPARGS("s", &url);
auto ok = pICE -> LaunchBrowser(url);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LoadAnimationOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
SG_POPARGS("d", &glid);
auto igs = pICE -> GetMovementObjectMeAsGetSet();
if (!igs)
	SG_RETURN_ERR_STR("GetMovementObjMeAsGetSet() FAILED");
auto ok = pICE -> LoadAnimationOnPlayer(igs, (GLID)glid);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LoadAnimationOnPlayerByNetId, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
double netId;
SG_POPARGS("dd", &glid, &netId);
auto igs = pICE -> GetMovementObjectByNetIdAsGetSet((int)netId);
if (!igs)
	SG_RETURN_ERR_STR("GetMovementObjByNetIdAsGetSet() FAILED");
auto ok = pICE -> LoadAnimationOnPlayer(igs, (GLID)glid);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetItemInfo, 5)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
SG_POPARGS("d", &glid);
std::string texture_name;
RECT texture_coords;
if (!pICE->GetItemInfo((GLID)glid, texture_name, texture_coords))
	SG_RETURN_ERR_STR("GetItemInfo() FAILED");
SG_PUSHARGS("sdddd", texture_name.c_str(), (double)texture_coords.left, (double)texture_coords.top, (double)texture_coords.right, (double)texture_coords.bottom);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DownloadTexture, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double type = 0, assetId = 0;
bool isThumb = false;
const char* url;
SG_POPARGS("ddsb", &type, &assetId, &url, &isThumb);
auto fname = pICE -> DownloadTexture((int)type, (int)assetId, url, isThumb);
SG_PUSHARGS("s", fname.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DownloadTextureAsync, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double type = 0, assetId = 0;
bool isThumb = false;
const char* texUrl;
SG_POPARGS("ddsb", &type, &assetId, &texUrl, &isThumb);
auto texPath = pICE -> DownloadTextureAsync((int)type, (int)assetId, texUrl, isThumb);
SG_PUSHARGS("s", texPath.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DownloadTextureAsyncStr, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* texFileName;
const char* texUrl;
SG_POPARGS("ss", &texFileName, &texUrl);
auto texPath = pICE -> DownloadTextureAsync(ToString(texFileName), ToString(texUrl));
SG_PUSHARGS("s", texPath.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DownloadTextureAsyncToGameId, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* texFileName;
const char* texUrl;
SG_POPARGS("ss", &texFileName, &texUrl);
std::string ctPath = std::to_string(pICE->GetGameId());
auto texPath = pICE -> DownloadTextureAsync(ToString(texFileName), ToString(texUrl), ctPath);
SG_PUSHARGS("s", texPath.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetFileVersion, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto versionStr = pICE -> AppVersion();
SG_PUSHARGS("s", versionStr.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CanAutoLogin, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto autoLogin = pICE -> CanAutoLogin();
SG_PUSHARGS("b", autoLogin);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetLodBias, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double bias;
bool biasAuto;
SG_POPARGS("db", &bias, &biasAuto);
pICE->SetLodBias_MovementObjects((int)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetLodBias, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
int bias;
bool biasAuto;
pICE->GetLodBias_MovementObjects(bias, biasAuto);
SG_PUSHARGS("db", (double)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetParticleBias, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double bias;
bool biasAuto;
SG_POPARGS("db", &bias, &biasAuto);
pICE->SetParticleBias((int)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetParticleBias, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
int bias;
bool biasAuto;
pICE->GetParticleBias(bias, biasAuto);
SG_PUSHARGS("db", (double)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetAnimationBias, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double bias;
bool biasAuto;
SG_POPARGS("db", &bias, &biasAuto);
pICE->SetAnimationBias_MovementObjects((int)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetAnimationBias, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
int bias;
bool biasAuto;
pICE->GetAnimationBias_MovementObjects(bias, biasAuto);
SG_PUSHARGS("db", (double)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetAnimationBiasDynObj, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double bias;
bool biasAuto;
SG_POPARGS("db", &bias, &biasAuto);
pICE->SetAnimationBias_DynamicObjects((int)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetAnimationBiasDynObj, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
int bias;
bool biasAuto;
pICE->GetAnimationBias_DynamicObjects(bias, biasAuto);
SG_PUSHARGS("db", (double)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetInvertY, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enabled;
SG_POPARGS("b", &enabled);
pICE->SetInvertY(enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetInvertY, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enabled = pICE->GetInvertY();
SG_PUSHARGS("b", enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetMLookSwap, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enabled;
SG_POPARGS("b", &enabled);
pICE->SetMLookSwap(enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMLookSwap, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enabled = pICE->GetMLookSwap();
SG_PUSHARGS("b", enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetAvatarNames, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double names;
bool namesAuto;
SG_POPARGS("db", &names, &namesAuto);
pICE->SetAvatarNames((int)names, namesAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetAvatarNames, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
int names;
bool namesAuto;
pICE->GetAvatarNames(names, namesAuto);
SG_PUSHARGS("db", (double)names, namesAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetDOCullBias, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double bias;
bool biasAuto;
SG_POPARGS("db", &bias, &biasAuto);
pICE->SetCullBias_DynamicObjects((int)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetDOCullBias, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
int bias;
bool biasAuto;
pICE->GetCullBias_DynamicObjects(bias, biasAuto);
SG_PUSHARGS("db", (double)bias, biasAuto);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetShadows, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enabled;
SG_POPARGS("b", &enabled);
pICE->SetShadows(enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetShadows, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enabled = pICE->GetShadows();
SG_PUSHARGS("b", enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WriteConfigSettings, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->WriteConfigSettings();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PreloadAnimationByHash, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* animHash = NULL;
double prioDelta = 0;
SG_POPARGS("sd", &animHash, &prioDelta);
pICE->PreloadAnimationByHash(animHash, (int)prioDelta);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PreloadAnimationByGLID, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, prioDelta = 0;
SG_POPARGS("dd", &glid, &prioDelta);
pICE->PreloadAnimation((GLID)glid, (int)prioDelta);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PreloadMesh, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* hash = NULL;
double prioDelta = 0;
SG_POPARGS("sd", &hash, &prioDelta);
pICE->PreloadMesh(hash, (int)prioDelta);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlaceLocalDynamicObj, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double globalId;
double scaleFactor;
bool hasPosition = false; // Default: no overrides
bool hasOrientation = false; // Default: no overrides
double posX, posY, posZ, dX, dY, dZ, sX, sY, sZ;
Vector3f position, direction, slide;

SG_POPARGS("ddbb", &globalId, &scaleFactor, &hasPosition, &hasOrientation);
__popped += 4; // SG_POPARGS does not increment __popped, fix it for subsequent SG_POP calls
if (hasPosition) {
	SG_POP(posX);
	SG_POP(posY);
	SG_POP(posZ);
	position.x = (float)posX;
	position.y = (float)posY;
	position.z = (float)posZ;
}
if (hasOrientation) {
	SG_POP(dX);
	SG_POP(dY);
	SG_POP(dZ);
	SG_POP(sX);
	SG_POP(sY);
	SG_POP(sZ);
	direction.x = (float)dX;
	direction.y = (float)dY;
	direction.z = (float)dZ;
	slide.x = (float)sX;
	slide.y = (float)sY;
	slide.z = (float)sZ;
}
auto plcmtID = pICE -> PlaceLocalDynamicObject((int)globalId, hasPosition == 0 ? NULL : &position, hasOrientation == 0 ? NULL : &direction, hasOrientation == 0 ? NULL : &slide, (float)scaleFactor);
SG_PUSHARGS("d", (double)plcmtID);
SG_RETURN;
SG_END_FN

// DEPRECATED - Use KEP_SetDynamicObjectAnimationSettings() Instead!
SG_BEGIN_FN(KEP_SetAnimationOnDynamicObject, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId, glid;
SG_POPARGS("dd", &objId, &glid);
AnimSettings animSettings(glid);
pICE->SetAnimationOnDynamicObject((int)objId, animSettings);
SG_RETURN;
SG_END_FN

// NEW - Use Me!
SG_BEGIN_FN(KEP_SetDynamicObjectAnimationSettings, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId, glid, loopCtl;
bool dirForward;
SG_POPARGS("ddbd", &objId, &glid, &dirForward, &loopCtl);
AnimSettings animSettings((GLID)glid, dirForward, (eAnimLoopCtl)(int)loopCtl);
pICE->SetAnimationOnDynamicObject((int)objId, animSettings);
SG_RETURN;
SG_END_FN

// DEPRECATED - Use KEP_GetDynamicObjectAnimationSettings() Instead!
SG_BEGIN_FN(KEP_GetDynamicObjectAnimation, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objId;
SG_POPARGS("d", &objId);
AnimSettings animSettings;
std::string name;
pICE->GetDynamicObjectAnimation((int)objId, animSettings, name);
SG_PUSHARGS("ds", (double)animSettings.m_glid, name.c_str());
SG_RETURN;
SG_END_FN

// NEW - Use Me!
SG_BEGIN_FN(KEP_GetDynamicObjectAnimationSettings, FOUR_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objId;
SG_POPARGS("d", &objId);
AnimSettings animSettings;
std::string name;
pICE->GetDynamicObjectAnimation((int)objId, animSettings, name);
SG_PUSHARGS("dbds", (double)animSettings.m_glid, animSettings.m_dirForward, (double)animSettings.m_loopCtl, name.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetDynamicObjectParticleSystem, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objId;
SG_POPARGS("d", &objId);
GLID glid = GLID_INVALID;
std::string name;
pICE->GetDynamicObjectParticleSystem((int)objId, glid, name);
SG_PUSHARGS("ds", (double)glid, name.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_IsModelCustomizable, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double modelIdentity;
SG_POPARGS("d", &modelIdentity);
auto ok = pICE -> IsWorldObjectCustomizableByIdentity((int)modelIdentity);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ApplyCustomTextureLocal, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
char* path = NULL;
SG_POPARGS("ds", &index, &path);
auto ok = pICE -> ApplyCustomTextureLocal((int)index, path);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ApplyCustomTextureWldObjectLocal, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double index = 0;
char* path = NULL;
SG_POPARGS("ds", &index, &path);
auto ok = pICE -> ApplyCustomTextureWorldObjectLocal((int)index, path);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SubmitLocalDynamicObjectByPlacementId, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_DECLARE_POP_OBJ(IEvent, e, sgEvent);
SG_POPARGS("dO", &objId, &sgEvent);
if (!SG_CAST_TO_DERIVED_OBJECT(e, IEvent, IEvent, sgEvent))
	SG_RETURN_ERR_STR("Failed Casting 2nd Parameter (event)");
auto ok = pICE -> SubmitLocalDynamicObjectByPlacementId((long)objId, e);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DisplayTranslationWidget, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE);
SG_POPARGS("d", &objType);
pICE->ObjectDisplayTranslationWidget(DoubleToObjType(objType));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DisplayRotationWidget, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE);
SG_POPARGS("d", &objType);
pICE->ObjectDisplayRotationWidget(DoubleToObjType(objType));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_TurnOffWidget, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->TurnOffWidget();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetWidgetRotation, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double x = 0, y = 0, z = 0;
SG_POPARGS("ddd", &x, &y, &z);
pICE->setWidgetRotation((float)x, (float)y, (float)z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetWidgetPosition, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double x = 0, y = 0, z = 0;
SG_POPARGS("ddd", &x, &y, &z);
pICE->setWidgetPosition((float)x, (float)y, (float)z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WidgetWorldSpace, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->widgetWorldSpace();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WidgetObjectSpace, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->widgetObjectSpace();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WidgetShowWidget, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool show = false;
SG_POPARGS("b", &show);
pICE->widgetShowWidget(show);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WidgetAdvancedMovement, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool adv = true;
SG_POPARGS("b", &adv);
pICE->widgetAdvancedMovement(adv);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetWidgetPosition, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
float x, y, z;
pICE->getWidgetPosition(&x, &y, &z);
SG_PUSHARGS("ddd", (double)x, (double)y, (double)z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetWidgetRotation, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
float x, y, z;
pICE->getWidgetRotation(&x, &y, &z);
SG_PUSHARGS("ddd", (double)x, (double)y, (double)z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetOutlineState, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
bool outlineState = false;
SG_POPARGS("ddb", &objType, &objId, &outlineState);
pICE->ObjectSetOutlineState(DoubleToObjType(objType), (int)objId, outlineState);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetOutlineColor, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0, colorR = 0, colorG = 0, colorB = 0;
SG_POPARGS("ddddd", &objType, &objId, &colorR, &colorG, &colorB);
pICE->ObjectSetOutlineColor(DoubleToObjType(objType), (int)objId, D3DXCOLOR((float)colorR, (float)colorG, (float)colorB, 1.0f));
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetOutlineZSorted, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
bool outlineZSorted = false;
SG_POPARGS("ddb", &objType, &objId, &outlineZSorted);
pICE->ObjectSetOutlineZSorted(DoubleToObjType(objType), (int)objId, outlineZSorted);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SelectObject, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
SG_POPARGS("dd", &objType, &objId);
pICE->ObjectAddToSelection(DoubleToObjType(objType), (int)objId, true);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_LockSelection, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool lock = false;
SG_POPARGS("b", &lock);
pICE->ObjectLockSelection(lock ? TRUE : FALSE);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DeselectObject, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
SG_POPARGS("dd", &objType, &objId);
pICE->ObjectRemoveFromSelection(DoubleToObjType(objType), (int)objId, true);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ClearSelection, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->ObjectClearSelection();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ReplaceSelection, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
SG_POPARGS("dd", &objType, &objId);
pICE->ObjectReplaceSelection(DoubleToObjType(objType), (int)objId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ToggleSelected, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
SG_POPARGS("dd", &objType, &objId);
pICE->ObjectToggleSelected(DoubleToObjType(objType), (int)objId, true);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetNumSelected, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto num = pICE -> ObjectsSelected();
SG_PUSHARGS("d", (double)num);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetSelection, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double index = 0.0;
SG_POPARGS("d", &index);
int selectionIndex = (int)index;
if (selectionIndex < 0 || selectionIndex >= pICE->ObjectsSelected())
	SG_RETURN_ERR_STR("Invalid 1st Parameter (selectionIndex)");
ObjectType objType = ObjectType::NONE;
int objId = -1;
pICE->ObjectGetSelection(&objType, &objId, selectionIndex);
SG_PUSHARGS("dd", ObjTypeToDouble(objType), (double)objId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SnapSelection, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double tolerance = 0.0;
SG_POPARGS("d", &tolerance);
pICE->ObjectSnapSelection((float)tolerance);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetRolloverMode, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE);
bool show = false;
SG_POPARGS("db", &objType, &show);
pICE->ObjectSetRolloverMode(DoubleToObjType(objType), show);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetSelectionPivotOffset, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double index = 0.0;
SG_POPARGS("d", &index);
int selectionIndex = (int)index;
if (selectionIndex < 0 || selectionIndex >= pICE->ObjectsSelected())
	SG_RETURN_ERR_STR("Invalid 1st Parameter (selectionIndex)");
float x, y, z;
pICE->ObjectGetSelectionPivotOffset(&x, &y, &z, selectionIndex);
SG_PUSHARGS("ddd", (double)x, (double)y, (double)z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Mount, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double aiBotId, mountType;
bool isLocal;
SG_POPARGS("ddb", &aiBotId, &mountType, &isLocal);
pICE->Mount((int)aiBotId, (int)mountType, isLocal);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SendMenuEvent, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double numberOfParams;
std::vector<std::string> params;
SG_POP(numberOfParams);
for (int i = 0; i < numberOfParams; i++) {
	const char* param;
	SG_POP(param);
	if (param)
		params.push_back(param);
}
auto rv = pICE -> SendScriptServerMenuEvent(params);
SG_PUSHARGS("d", (double)rv);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Unmount, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->Unmount();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMenuDir, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto dirname = pICE -> GetMenuDir();
SG_PUSHARGS("s", dirname.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetAppMenuDir, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto dirname = pICE -> GetAppMenuDir();
SG_PUSHARGS("s", dirname.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetAppScriptDir, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto dirname = pICE -> GetAppScriptDir();
SG_PUSHARGS("s", dirname.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetNumMenus, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool childMenu;
SG_POPARGS("b", &childMenu);
auto num = pICE -> GetNumMenus(childMenu ? MENU_WORLD : MENU_WOK);
SG_PUSHARGS("d", (double)num);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMenuByIndex, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool childMenu;
double index;
SG_POPARGS("bd", &childMenu, &index);
auto menuName = pICE -> GetMenuByIndex(childMenu ? MENU_WORLD : MENU_WOK, (int)index);
SG_PUSHARGS("s", menuName.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetNumMenuScripts, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool childMenu;
SG_POPARGS("b", &childMenu);
auto num = pICE -> GetNumMenuScripts(childMenu ? MENU_WORLD : MENU_WOK);
SG_PUSHARGS("d", (double)num);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetMenuScriptByIndex, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool childMenu;
double index;
SG_POPARGS("bd", &childMenu, &index);
auto menuName = pICE -> GetMenuScriptByIndex(childMenu ? MENU_WORLD : MENU_WOK, (int)index);
SG_PUSHARGS("s", menuName.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ReloadMenus, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->ReloadMenus();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ObjectExists, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
SG_POPARGS("dd", &objType, &objId);
auto ok = pICE -> ObjectExists(DoubleToObjType(objType), (int)objId);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ObjectSetPosition, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
Vector3d pos(0, 0, 0);
SG_POPARGS("ddddd", &objType, &objId, &pos.x, &pos.y, &pos.z);
if (!pICE->ObjectSetPosition(DoubleToObjType(objType), (int)objId, Vector3f(pos))) {
	LogError("ObjectSetPosition() FAILED - objId=" << (int)objId);
	SG_RETURN_NULL;
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ObjectGetPosition, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
SG_POPARGS("dd", &objType, &objId);
Vector3f pos;
if (!pICE->ObjectGetPosition(DoubleToObjType(objType), (int)objId, pos)) {
	LogError("ObjectGetPosition() FAILED - objId=" << (int)objId);
	SG_RETURN_NULL;
}
SG_PUSHARGS("ddd", pos.x, pos.y, pos.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ObjectSetRotation, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE), objId = 0;
Vector3d rot;
SG_POPARGS("ddddd", &objType, &objId, &rot.x, &rot.y, &rot.z);
if (!pICE->ObjectSetRotation(DoubleToObjType(objType), (int)objId, Vector3f(rot))) {
	LogError("ObjectSetRotation() FAILED - objId=" << (int)objId);
	SG_RETURN_NULL;
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ObjectGetRotation, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE);
double objId = 0;
SG_POPARGS("dd", &objType, &objId);
Vector3f rot;
if (!pICE->ObjectGetRotation(DoubleToObjType(objType), (int)objId, rot)) {
	LogError("ObjectGetRotation() FAILED - objId=" << (int)objId);
	SG_RETURN_NULL;
}
SG_PUSHARGS("ddd", (double)rot.x, (double)rot.y, (double)rot.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetTransformRelativeToBone, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double boneId = 0, x = 0, y = 0, z = 0;
SG_POPARGS("dddd", &boneId, &x, &y, &z);
double outX, outY, outZ;
if (!pICE->GetTransformRelativeToBone((int)boneId, (float)x, (float)y, (float)z, &outX, &outY, &outZ)) {
	LogError("GetTransformRelativeToBone() FAILED - boneId=" << (int)boneId);
	SG_RETURN_NULL;
}
SG_PUSHARGS("ddd", outX, outY, outZ);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddItemThumbnailToDatabase, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = (double)GLID_INVALID;
SG_POPARGS("d", &glid);
if (!IS_VALID_GLID((GLID)glid))
	SG_RETURN_ERR_STR("Invalid 1st Parameter (glid == GLID_INVALID (0))");
auto texFileName = pICE -> AddItemThumbnailToDatabase((GLID)glid);
SG_PUSHARGS("s", texFileName.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UploadScriptServerScript, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* filePath;
const char* filename;
SG_POPARGS("ss", &filename, &filePath);
if (!filename)
	SG_RETURN_ERR_STR("Invalid 1st Parameter (fileName)");
if (!filePath)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (fullPath)");
auto ok = pICE -> UploadAssetTo3DApp(filename, filePath, (int)ScriptServerEvent::SavedScript, 0, 0);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UploadAssetTo3DApp, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* filePath;
const char* filename;
double assetType;
SG_POPARGS("ssd", &filename, &filePath, &assetType);
if (!filename)
	SG_RETURN_ERR_STR("Invalid 1st Parameter (fileName)");
if (!filePath)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (fullPath)");
auto ok = pICE -> UploadAssetTo3DApp(filename, filePath, (int)assetType, 0, 0);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_NotifyServerOfAssetUpload, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* name;
double uploadId, assetType;
SG_POPARGS("dsd", &uploadId, &name, &assetType);
pICE->NotifyServerOfAssetUpload((int)uploadId, name, (int)assetType);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CreateNewUGCParticleSystem, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
GLID glid = (GLID)pICE->CreateNewUGCParticleSystem();
SG_PUSHARGS("d", (double)glid);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetParticleSystemByGLID, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0;
SG_POPARGS("d", &glid);
auto igs = pICE -> GetParticleSystemByGlid((GLID)glid);
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetParticleSystemByName, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
char* name = NULL;
SG_POPARGS("s", &name);
auto igs = pICE -> GetParticleSystemByName(name);
SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(igs, IGetSet, pushObj);
SG_PUSHARGS("O", pushObj);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddParticleSystemToDynamicObject, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
char* effectName = NULL;
SG_POPARGS("ds", &objId, &effectName);
auto ok = pICE -> AddParticleSystemToDynamicObject((int)objId, effectName);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RemoveParticleSystemFromDynamicObject, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
pICE->RemoveParticleSystemFromDynamicObject((int)objId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CanDynamicObjectSupportAnimation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0;
SG_POPARGS("d", &objId);
auto ok = pICE -> CanDynamicObjectSupportAnimation((int)objId);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddAnimationToUGCActor, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glidAvatar = 0, glidAnim = 0;
const char* animName;
SG_POPARGS("dds", &glidAvatar, &glidAnim, &animName);
pICE->addAnimationUGCActor((GLID)glidAvatar, (GLID)glidAnim, animName);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetClientRectSize, TWO_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
auto s = pICE -> getClientRectSize();
SG_PUSHARGS("dd", (double)s.cx, (double)s.cy);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ConfigEntitySubstitute, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double edbIdx, type, globalId;
SG_POPARGS("ddd", &edbIdx, &type, &globalId);
pICE->configMovementObjectSubstitute(static_cast<int>(edbIdx), DoubleToObjType(type), (int)globalId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetNumPlayersInZone, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto num = pICE -> GetNumHumanPlayersInRuntimeList();
SG_PUSHARGS("d", (double)num);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlayParticleEffectOnPlayer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double playerId, particleSlot, glid = 0;
const char* boneName = NULL;
SG_POPARGS("dsdd", &playerId, &boneName, &particleSlot, &glid);
auto ok = pICE -> PlayParticleEffectOnPlayer((int)playerId, boneName, (int)particleSlot, (GLID)glid);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RequestSourceAssetFromAppServer, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double assetType = 0;
const char* name = NULL;
SG_POPARGS("ds", &assetType, &name);
if (!name)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (assetName)");
std::string nameS = name;
auto slashPos = nameS.rfind("\\");
if (slashPos != std::string::npos)
	nameS = nameS.substr(slashPos + 1);
slashPos = nameS.rfind("/");
if (slashPos != std::string::npos)
	nameS = nameS.substr(slashPos + 1);
auto ok = pICE -> requestSourceAssetFromAppServer((int)assetType, nameS, 0);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Count3DAppUnpublishedAssets, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double assetType = 0;
SG_POPARGS("d", &assetType);
auto num = pICE -> countDevToolAssets((int)assetType);
SG_PUSHARGS("d", (double)num);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Get3DAppUnpublishedAsset, THREE_RETURN_VALUES)
SG_DECLARE_ICE_PTR(pICE);
double assetType = 0, index = 0;
SG_POPARGS("dd", &assetType, &index);
std::string name, fullPath;
bool modified = true;
if (!pICE->getDevToolAssetInfo((int)assetType, (int)index, name, fullPath, modified))
	SG_RETURN_ERR_STR("GetDevToolAssetInfo() FAILED");
SG_PUSHARGS("ssb", name.c_str(), fullPath.c_str(), modified);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Get3DAppUnpublishedAssetPath, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double assetType = 0;
SG_POPARGS("d", &assetType);
auto path = pICE -> getDevToolAssetPath((int)assetType);
if (path.empty()) {
	SG_PUSHARGS("d", (double)0.0);
} else {
	SG_PUSHARGS("s", path.c_str());
}
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Import3DAppAsset, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double assetType = 0;
const char* path = NULL;
SG_POPARGS("ds", &assetType, &path);
if (!path)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (path)");
auto ok = pICE -> Import3DAppAsset((int)assetType, path);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Import3DAppAssetFromString, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double assetType = 0;
const char *baseName = NULL, content = NULL;
SG_POPARGS("dss", &assetType, &baseName, &content);
if (!baseName)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (baseName)");
if (!content)
	SG_RETURN_ERR_STR("Invalid 3rd Parameter (content)");
std::string fullPath;
auto ok = pICE -> Import3DAppAssetFromString((int)assetType, baseName, content, fullPath);
SG_PUSHARGS("s", ok ? fullPath.c_str() : "");
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetKanevaUserId, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto userId = pICE -> GetKanevaUserId();
SG_PUSHARGS("d", (double)userId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RayTrace, 6)
SG_DECLARE_ICE_PTR(pICE);
Vector3d origin, dir;
double mask;
SG_POPARGS("ddddddd", &origin.x, &origin.y, &origin.z, &dir.x, &dir.y, &dir.z, &mask);
int type = 0, id = 0;
Vector3f intersection = { 0, 0, 0 };
float distance = 0;
if (!pICE->GlueRayTrace(Vector3f(origin), Vector3f(dir), DoubleToObjType(mask), reinterpret_cast<ObjectType&>(type), id, intersection, distance)) {
	LogError("GlueRayTrace() FAILED - FIX ME - This API Only Works Inside Skybox!");
	//		SG_RETURN_ERR_STR("GlueRayTrace() FAILED");
}
SG_PUSHARGS("dddddd", (double)type, (double)id, (double)intersection.x, (double)intersection.y, (double)intersection.z, (double)distance);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SphereTestObject, 5)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE);
double objId = 0;
// yRotation is the rotation of the sphere about the Y axis, yielding an orientation of the sphere. Used
// for angle checks against maxAngle.
double originX, originY, originZ, radius, yRotation, maxAngle;
SG_POPARGS("dddddddd", &objType, &objId, &originX, &originY, &originZ, &radius, &yRotation, &maxAngle);

// Build orientation vector and convert degrees to radians.
float yRotRad = (float)yRotation * float(M_PI) / 180.f;
float maxAngleRad = (float)maxAngle * float(M_PI) / 180.f;
Vector3f orientation(-sin(yRotRad), 0.f, -cos(yRotRad));

Vector3f origin;
origin.x = (float)originX;
origin.y = (float)originY;
origin.z = (float)originZ;

Vector3f intersection;
float distance;
bool overlap = pICE->SphereTestObject(DoubleToObjType(objType), (int)objId, origin, (float)radius, intersection, distance, orientation, (float)maxAngleRad);
SG_PUSHARGS("ddddd", (double)overlap, (double)intersection.x, (double)intersection.y, (double)intersection.z, (double)distance);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ScreenToRay, 6)
SG_DECLARE_ICE_PTR(pICE);
double screenX, screenY;
SG_POPARGS("dd", &screenX, &screenY);
Vector3f ori, dir;
pICE->GetPickRay(&ori, &dir, (int)screenX, (int)screenY);
SG_PUSHARGS("dddddd", (double)ori.x, (double)ori.y, (double)ori.z, (double)dir.x, (double)dir.y, (double)dir.z);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetAllNetIds, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto netIds = pICE -> getAllMovementObjectNetTraceIds();
std::string resultText = "";
char buffer[64];
for (size_t i = 0; i < netIds.size(); i++) {
	if (i > 0)
		resultText = resultText + " ";
	_itoa_s(netIds[i], buffer, sizeof(buffer), 10);
	resultText = resultText + buffer;
}
SG_PUSHARGS("s", resultText.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetRandomNumber, ONE_RETURN_VALUE)
auto randomNumber = ::GetRandomNumber(); // 0.0 -> 1.0
SG_PUSHARGS("d", (double)randomNumber);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ClearMouseOverSettings, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double mosCategory = MOS::CATEGORY_ALL;
SG_POPARGS("d", &mosCategory);
auto ok = pICE -> MOS_Clear((MOS::CATEGORY)(int)mosCategory);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_AddMouseOverSetting, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objType = ObjTypeToDouble(ObjectType::NONE);
double objId = 0;
double mosMode = MOS::MODE_NONE;
double cursorType = CURSOR_TYPE_DEFAULT;
double mosCategory = MOS::CATEGORY_NONE;
const char* text = "";
SG_POPARGS("dddsdd", &objType, &objId, &mosMode, &text, &cursorType, &mosCategory);
auto ok = pICE -> MOS_Add(DoubleToObjType(objType), (int)objId, (MOS::MODE)(int)mosMode, text, (CURSOR_TYPE)(int)cursorType, (MOS::CATEGORY)(int)mosCategory);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetPlayerPosition, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double newPosX, newPosY, newPosZ;
SG_POPARGS("ddd", &newPosX, &newPosY, &newPosZ);
auto ok = pICE -> SetMyPosition((float)newPosX, (float)newPosY, (float)newPosZ);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetPlayerOrientation, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double rotX, rotY, rotZ;
SG_POPARGS("ddd", &rotX, &rotY, &rotZ);
auto ok = pICE -> SetMyOrientation((float)rotX, (float)rotY, (float)rotZ);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PresetMenuMessage, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId;
const char* menuName = "";
const char* messageType = "";
const char* data1 = "";
const char* data2 = "";
const char* identifier = "";
SG_POPARGS("sdssss", &menuName, &objId, &messageType, &data1, &data2, &identifier);
pICE->presetMenuMessage(menuName, (int)objId, messageType, data1, data2, identifier);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_PlaceDynamicObjectAtPosition, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, invenType, posX, posY, posZ, orientationX, orientationY, orientationZ;
SG_POPARGS("dddddddd", &glid, &invenType, &posX, &posY, &posZ, &orientationX, &orientationY, &orientationZ);
pICE->sendPlaceDynamicObjectAtPositionEvent((GLID)glid, (short)invenType, (float)posX, (float)posY, (float)posZ, (float)orientationX, (float)orientationY, (float)orientationZ);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ClickPlaceObjectByGlid, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double glid = 0, invenType;
SG_POPARGS("dd", &glid, &invenType);
auto ok = pICE -> PlaceDynamicObjectByGlid((GLID)glid, (int)invenType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetGridSpacing, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double spacing;
SG_POPARGS("d", &spacing);
pICE->setGridSpacing((float)spacing);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetGridSpacing, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto spacing = pICE -> getGridSpacing();
SG_PUSHARGS("d", (double)spacing);
SG_RETURN;
SG_END_FN

/** DRF - Needs Better Home - Client & Server
 * Script asynchronous low priority no response html webcall (fire & forget).
 * Creates a new web caller if no instance with matching identifier exists.
 */
SG_BEGIN_FN(KEP_WebCall, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* url;
const char* id;
SG_POPARGS("ss", &url, &id);
if (!url)
	SG_RETURN_ERR_STR("Invalid 1st Parameter (url)");
if (!id)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (id)");
auto ok = pICE -> WebCall(url, id);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

/** DRF - Needs Better Home - Client & Server
 * Script synchronous low priority no response html webcall returns URL_OK.
 * Creates a new web caller if no instance with matching identifier exists.
 */
SG_BEGIN_FN(KEP_WebCallAndWait, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* url;
const char* id;
SG_POPARGS("ss", &url, &id);
if (!url)
	SG_RETURN_ERR_STR("Invalid 1st Parameter (url)");
if (!id)
	SG_RETURN_ERR_STR("Invalid 2nd Parameter (id)");
auto ok = pICE -> WebCallAndWaitOk(url, id);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetTestGroup, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto group = pICE -> AppTestGroup();
SG_PUSHARGS("d", (double)group);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DynamicObjectMakeTranslucent, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double id = 0;
SG_POPARGS("d", &id);
auto ok = pICE -> DynamicObjectMakeTranslucent((int)id);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ResetCursorModesTypes, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto ok = pICE -> ResetCursorModesTypes();
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCursorModeType, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double cursorMode = CURSOR_MODE_NONE;
double cursorType = CURSOR_TYPE_NONE;
SG_POPARGS("dd", &cursorMode, &cursorType);
auto ok = pICE -> SetCursorModeType((CURSOR_MODE)(int)cursorMode, (CURSOR_TYPE)(int)cursorType);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetCursorMode, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double cursorMode = CURSOR_MODE_NONE;
SG_POPARGS("d", &cursorMode);
auto ok = pICE -> SetCursorMode((CURSOR_MODE)(int)cursorMode);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetZoneAllowsLabels, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool allowsLabels;
SG_POPARGS("b", &allowsLabels);
pICE->SetZoneAllowsLabels(allowsLabels);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetPlayerTitle, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto playerTitleStr = pICE -> GetMyTitle();
SG_PUSHARGS("s", playerTitleStr.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_EnableTriggerRender, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool renderDynamic = false, renderWorld = false;
SG_POPARGS("bb", &renderDynamic, &renderWorld);
pICE->EnableTriggerRender(renderDynamic, renderWorld);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_EnableDynamicObjectBoundingBoxRender, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
bool render = false;
SG_POPARGS("db", &objId, &render);
auto ok = pICE -> EnableDynamicObjectBoundingBoxRender((int)objId, render);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetObjectHighlight, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objId = 0.0;
bool highlight = false;
double r = 0.0f, g = 0.0f, b = 0.0f, a = 0.0f;
SG_POPARGS("dbdddd", &objId, &highlight, &r, &g, &b, &a);
auto ok = pICE -> SetDynamicObjectHighlight((int)objId, highlight, r, g, b, a);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_WorldIsAP, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto worldIsAP = pICE -> WorldPassGroupAP();
SG_PUSHARGS("b", worldIsAP);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_IsGM, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
auto isGM = pICE -> MyPassGroupGM();
SG_PUSHARGS("b", isGM);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ShowPhysicsWindow, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double arg = 0;
SG_POPARGS("d", &arg);
int iPrevValue = pICE->ShowPhysicsWindow(arg);
SG_PUSHARGS("d", (double)iPrevValue);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_EnableMediaTriggerRender, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool render = false;
SG_POPARGS("b", &render);
pICE->EnableMediaTriggerRender(render);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ResetTextureQualityLevels, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
pICE->ResetTextureQualityLevels();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_EnableTextureQualityLevel, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double quality = 0.0;
SG_POPARGS("d", &quality);
bool ok = pICE->EnableTextureQualityLevel((unsigned)quality);
SG_PUSHARGS("b", ok);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ForceOnePixelTexture, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool force = false;
SG_POPARGS("b", &force);
pICE->ForceOnePixelTexture(force);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetZoneLoadingUIActive, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool active = false;
SG_POPARGS("b", &active);
pICE->SetZoneLoadingUIActive(active);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_SetZoneLoadingProgressPercent, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double percent = 0;
SG_POPARGS("d", &percent);
pICE->SetZoneLoadingProgressPercent(percent);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ConfigEventDispatchTime, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double inGameTime = 0, loadingTime = 0;
bool legacyMode = false;
SG_POPARGS("ddb", &inGameTime, &loadingTime, &legacyMode);
pICE->ConfigEventDispatchTime((unsigned)inGameTime, (unsigned)loadingTime, legacyMode);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_EnableDownloadPriorityVisualization, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enable = false;
SG_POPARGS("b", &enable);
pICE->EnableDownloadPriorityVisualization(enable);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_IsDownloadPriorityVisualizationEnabled, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
bool enabled = pICE->IsDownloadPriorityVisualizationEnabled();
SG_PUSHARGS("b", enabled);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Encrypt, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* s = "";
SG_POPARGS("s", &s);
std::string x = pICE->Encrypt(ToString(s));
SG_PUSHARGS("s", x.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_Decrypt, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* x = "";
SG_POPARGS("s", &x);
std::string s = pICE->Decrypt(ToString(x));
SG_PUSHARGS("s", s.c_str());
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_ExecuteFunction, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
const char* funcName = "";
SG_POPARGS("s", &funcName);
pICE->ExecuteFunction(funcName);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetDevOptionsDefinedInEngine, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
std::string devOptionsCombinedString = "";
pICE->GetDevOptionsDefinedInEngine(devOptionsCombinedString);
SG_PUSHARGS("s", devOptionsCombinedString.c_str());
SG_RETURN;
SG_END_FN

/*
SG_BEGIN_FN(KEP_TakeControlOfInput, NO_RETURN_VALUE)
SG_DECLARE_CE_PTR(pICE);
pICE->TakeControlOfInput();
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RevokeControlOfInput, NO_RETURN_VALUE)
SG_DECLARE_CE_PTR(pICE);
pICE->RevokeControlOfInput();
SG_RETURN;
SG_END_FN
*/

SG_BEGIN_FN(KEP_SetExperimentalEngineOption, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double option = 0, value = 0;
SG_POPARGS("dd", &option, &value);
pICE->SetExperimentalEngineOption((int)option, (int)value);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetExperimentalEngineOption, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double option = 0;
SG_POPARGS("d", &option);
int value = pICE->GetExperimentalEngineOption((int)option);
SG_PUSHARGS("d", (double)value);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_CreateDynamicTexture, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double width = 0, height = 0;
SG_POPARGS("dd", &width, &height);
unsigned dynTextureId = pICE->CreateDynamicTexture(width, height);
SG_PUSHARGS("d", (double)dynTextureId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_DestroyDynamicTexture, NO_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double dynTextureId = 0;
SG_POPARGS("d", &dynTextureId);
pICE->DestroyDynamicTexture((unsigned)dynTextureId);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GetWorldMapBoundary, RETURN_VALUES(6))
SG_DECLARE_ICE_PTR(pICE);
double options = 0, minRadiusFilter = 0;
SG_POPARGS("dd", &options, &minRadiusFilter);
double minX = 0, minY = 0, minZ = 0, maxX = 0, maxY = 0, maxZ = 0;
pICE->GetWorldMapBoundary((WorldRenderOptions)(int)options, minRadiusFilter, minX, minY, minZ, maxX, maxY, maxZ);
SG_PUSHARGS("dddddd", minX, minY, minZ, maxX, maxY, maxZ);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_GenerateWorldMap, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double dynTextureId = 0;
double style = 0, options = 0, minRadiusFilter = 0;
double minX = 0, minY = 0, minZ = 0, maxX = 0, maxY = 0, maxZ = 0;
SG_POPARGS("dddddddddd", &dynTextureId, &style, &options, &minRadiusFilter, &minX, &minY, &minZ, &maxX, &maxY, &maxZ);
bool res = pICE->GenerateWorldMap((unsigned)dynTextureId, (WorldRenderStyle)(int)style, (WorldRenderOptions)(int)options, minRadiusFilter, minX, minY, minZ, maxX, maxY, maxZ);
SG_PUSHARGS("b", res);
SG_RETURN;
SG_END_FN

// #MLB_BulletSensor - SG_BEGIN_FN LandClaim
SG_BEGIN_FN(KEP_IsObjectInLandClaim, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double objectPid = 0;
double landClaimPid = 0;
SG_POPARGS("dd", &objectPid, &landClaimPid);
bool res = pICE->IsObjectInLandClaim((int)objectPid, (int)landClaimPid);
SG_PUSHARGS("b", res);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_RegisterLandClaim, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double pid = 0;
SG_POPARGS("d", &pid);
bool res = pICE->RegisterLandClaim((int)pid);
SG_PUSHARGS("b", res);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(KEP_UnRegisterLandClaim, ONE_RETURN_VALUE)
SG_DECLARE_ICE_PTR(pICE);
double pid = 0;
SG_POPARGS("d", &pid);
bool res = pICE->UnRegisterLandClaim((int)pid);
SG_PUSHARGS("b", res);
SG_RETURN;
SG_END_FN

ScriptPointer<IGetSet> KEP_npcSpawn(
	SCRIPT_NAMED_PARAM(std::string, name),
	SCRIPT_NAMED_PARAM(NETID, netId),
	SCRIPT_NAMED_PARAM(int, charConfigId),
	SCRIPT_NAMED_PARAM_OPT(int, dbIndex),
	SCRIPT_NAMED_PARAM_OPT(Vector3f, pos),
	SCRIPT_NAMED_PARAM_OPT(double, rotDeg),
	SCRIPT_NAMED_PARAM_OPT(GLID, glidAnimSpecial),
	SCRIPT_NAMED_PARAM_OPT(std::vector<GLID>, glidsArmed)) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return nullptr;
	std::string nameStr = name.Get();
	auto igs = dynamic_cast<IGetSet*>(pICE->SpawnNPC(
		5, // SERVERITEMTYPE::SI_TYPE_PAPER_DOLL
		nameStr,
		(CharConfig*)charConfigId.Get(),
		glidsArmed.GetOptional(std::vector<GLID>()),
		dbIndex.GetOptional(0),
		netId.Get(),
		pos.GetOptional(Vector3f()),
		rotDeg.GetOptional(0),
		glidAnimSpecial.GetOptional(GLID_INVALID),
		false));
	if (!igs)
		LogError("SpawnStaticMovementObject() FAILED - '" << nameStr << "'");
	return igs;
}

bool KEP_npcDeSpawn(SCRIPT_NAMED_PARAM(NETID, netId)) {
	auto pICE = IClientEngine::Instance();
	return pICE ? pICE->DeSpawnNPC(netId.Get()) : false;
}

Vector3f GetRuntimePlayerScale(ScriptPointer<IGetSet> pGSMovementObject) {
	auto pICE = IClientEngine::Instance();
	return pICE ? pICE->GetRuntimePlayerScale(pGSMovementObject.get()) : Vector3f(1.0, 1.0, 1.0);
}

SG_VM_REGISTER(ClientEngineVMRegister)
SG_BEGIN_SG_REGISTER(KEP)
SG_REGISTER(SetInstance, KEP_SetInstance)

// Environment
SG_REGISTER(DevMode, KEP_DevMode)
SG_REGISTER(GetTestGroup, KEP_GetTestGroup)
SG_REGISTER(GetStarId, KEP_GetStarId)
SG_REGISTER(IsProduction, KEP_IsProduction)

// Operating System
SG_REGISTER(Breakpoint, KEP_Breakpoint)
SG_REGISTER(Error, KEP_Error)
SG_REGISTER(Quit, KEP_Quit)
SG_REGISTER(Crash, KEP_Crash)
SG_REGISTER(UuidCreate, KEP_UuidCreate)
SG_REGISTER(ReportIssue, KEP_ReportIssue)
SG_REGISTER(ConsolePrint, KEP_ConsolePrint)
SG_REGISTER(ConsoleError, KEP_ConsoleError)
SG_REGISTER(ConfigOpen, KEP_ConfigOpen)
SG_REGISTER(FileOpen, KEP_FileOpen)
SG_REGISTER(ShellOpen, KEP_ShellOpen)
SG_REGISTER(GetClientRectSize, KEP_GetClientRectSize)
SG_REGISTER(WorldToScreen, KEP_WorldToScreen)
SG_REGISTER(GetFileVersion, KEP_GetFileVersion)
SG_REGISTER(WindowRefresh, KEP_WindowRefresh)
SG_REGISTER(LaunchBrowser, KEP_LaunchBrowser)
SG_REGISTER(ScreenshotCapture, KEP_ScreenshotCapture)
SG_REGISTER(ScreenshotIsCapturing, KEP_ScreenshotIsCapturing)
SG_REGISTER(GetMenuDir, KEP_GetMenuDir)
SG_REGISTER(GetAppMenuDir, KEP_GetAppMenuDir)
SG_REGISTER(GetAppScriptDir, KEP_GetAppScriptDir)
SG_REGISTER(SendSystemServiceEvent, KEP_SendSystemServiceEvent)

// StartCfg Settings
SG_REGISTER(WriteConfigSettings, KEP_WriteConfigSettings)
SG_REGISTER(SetLodBias, KEP_SetLodBias)
SG_REGISTER(GetLodBias, KEP_GetLodBias)
SG_REGISTER(SetShadows, KEP_SetShadows)
SG_REGISTER(GetShadows, KEP_GetShadows)
SG_REGISTER(SetInvertY, KEP_SetInvertY)
SG_REGISTER(GetInvertY, KEP_GetInvertY)
SG_REGISTER(SetMLookSwap, KEP_SetMLookSwap)
SG_REGISTER(GetMLookSwap, KEP_GetMLookSwap)
REGISTER_SCRIPT_FUNCTION("KEP_SetMediaScrapeEnabled", &IClientEngine::SetMediaScrapeEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_GetMediaScrapeEnabled", &IClientEngine::GetMediaScrapeEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_SetParticlesEnabled", &IClientEngine::SetParticlesEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_GetParticlesEnabled", &IClientEngine::GetParticlesEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_SetAnimationsEnabled", &IClientEngine::SetAnimationsEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_GetAnimationsEnabled", &IClientEngine::GetAnimationsEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_SetLimitFPS", &IClientEngine::SetLimitFPS)
REGISTER_SCRIPT_FUNCTION("KEP_GetLimitFPS", &IClientEngine::GetLimitFPS)
REGISTER_SCRIPT_FUNCTION("KEP_SetOnRenderTimeMs", &IClientEngine::SetOnRenderTimeMs)
REGISTER_SCRIPT_FUNCTION("KEP_GetOnRenderTimeMs", &IClientEngine::GetOnRenderTimeMs)
REGISTER_SCRIPT_FUNCTION("KEP_SetAntiAlias", &IClientEngine::SetAntiAlias)
REGISTER_SCRIPT_FUNCTION("KEP_GetAntiAlias", &IClientEngine::GetAntiAlias)
SG_REGISTER(SetDOCullBias, KEP_SetDOCullBias)
SG_REGISTER(GetDOCullBias, KEP_GetDOCullBias)
SG_REGISTER(SetParticleBias, KEP_SetParticleBias)
SG_REGISTER(GetParticleBias, KEP_GetParticleBias)
SG_REGISTER(SetAnimationBias, KEP_SetAnimationBias)
SG_REGISTER(GetAnimationBias, KEP_GetAnimationBias)
SG_REGISTER(SetAnimationBiasDynObj, KEP_SetAnimationBiasDynObj)
SG_REGISTER(GetAnimationBiasDynObj, KEP_GetAnimationBiasDynObj)
SG_REGISTER(SetAvatarNames, KEP_SetAvatarNames)
SG_REGISTER(GetAvatarNames, KEP_GetAvatarNames)
SG_REGISTER(RenderLabelsOnTop, KEP_RenderLabelsOnTop)
SG_REGISTER(SetMenuSelectMode, KEP_SetMenuSelectMode)

// Web Calls
SG_REGISTER(WebCall, KEP_WebCall)
SG_REGISTER(WebCallAndWait, KEP_WebCallAndWait)

// Logging
SG_REGISTER(LogObjects, KEP_LogObjects)
SG_REGISTER(LogMemory, KEP_LogMemory)
SG_REGISTER(LogWebCalls, KEP_LogWebCalls)
SG_REGISTER(LogResources, KEP_LogResources)
SG_REGISTER(LogMetrics, KEP_LogMetrics)
SG_REGISTER(LogMsgProc, KEP_LogMsgProc)
SG_REGISTER(LogChat, KEP_LogChat)

// Metrics
SG_REGISTER(MetricsEnable, KEP_MetricsEnable)
SG_REGISTER(MetricsFpsRender, KEP_MetricsFpsRender)
SG_REGISTER(MetricsFpsRuntime, KEP_MetricsFpsRuntime)
SG_REGISTER(MetricsFpsZone, KEP_MetricsFpsZone)
REGISTER_SCRIPT_FUNCTION("KEP_EnableFrameTimeProfiler", &IClientEngine::EnableFrameTimeProfiler)
REGISTER_SCRIPT_FUNCTION("KEP_IsFrameTimeProfilerEnabled", &IClientEngine::IsFrameTimeProfilerEnabled)

// Misc Utils
SG_REGISTER(UrlEncode, KEP_UrlEncode)
SG_REGISTER(Encrypt, KEP_Encrypt)
SG_REGISTER(Decrypt, KEP_Decrypt)
SG_REGISTER(RotatePointAroundPoint, KEP_RotatePointAroundPoint)
SG_REGISTER(GetTransformRelativeToBone, KEP_GetTransformRelativeToBone)
SG_REGISTER(RayTrace, KEP_RayTrace)
SG_REGISTER(SphereTestObject, KEP_SphereTestObject)
SG_REGISTER(ScreenToRay, KEP_ScreenToRay)
SG_REGISTER(GetRandomNumber, KEP_GetRandomNumber)

// Keyboard Control / Mouse Cursor
SG_REGISTER(IsKeyDown, KEP_IsKeyDown)
SG_REGISTER(MouseClicks, KEP_MouseClicks)
SG_REGISTER(ResetCursorModesTypes, KEP_ResetCursorModesTypes)
SG_REGISTER(SetCursorModeType, KEP_SetCursorModeType)
SG_REGISTER(SetCursorMode, KEP_SetCursorMode)
SG_REGISTER(ClearMouseOverSettings, KEP_ClearMouseOverSettings)
SG_REGISTER(AddMouseOverSetting, KEP_AddMouseOverSetting)
SG_REGISTER(GetControlConfiguration, KEP_GetControlConfiguration)
SG_REGISTER(SetControlConfig, KEP_SetControlConfig)
SG_REGISTER(RestoreDefaultControlConfiguration, KEP_RestoreDefaultControlConfiguration)
SG_REGISTER(SaveControlConfiguration, KEP_SaveControlConfiguration)
SG_REGISTER(UnassignControl, KEP_UnassignControl)
SG_REGISTER(PendingControlAssignment, KEP_PendingControlAssignment)

// Menus
SG_REGISTER(GetNumMenus, KEP_GetNumMenus)
SG_REGISTER(GetMenuByIndex, KEP_GetMenuByIndex)
SG_REGISTER(ReloadMenus, KEP_ReloadMenus)
SG_REGISTER(SendMenuEvent, KEP_SendMenuEvent)
SG_REGISTER(GetNumMenuScripts, KEP_GetNumMenuScripts)
SG_REGISTER(GetMenuScriptByIndex, KEP_GetMenuScriptByIndex)
SG_REGISTER(PresetMenuMessage, KEP_PresetMenuMessage)

// Upload
SG_REGISTER(UploadScriptServerScript, KEP_UploadScriptServerScript)
SG_REGISTER(UploadAssetTo3DApp, KEP_UploadAssetTo3DApp)
SG_REGISTER(NotifyServerOfAssetUpload, KEP_NotifyServerOfAssetUpload)

// Download / Patch
SG_REGISTER(PreloadMesh, KEP_PreloadMesh)
SG_REGISTER(CancelAllDownloads, KEP_CancelAllDownloads)
SG_REGISTER(CancelPatch, KEP_CancelPatch)
SG_REGISTER(DownloadPatchFile, KEP_DownloadPatchFile)
SG_REGISTER(DownloadTexturePack, KEP_DownloadTexturePack)
SG_REGISTER(TestPatchFileForUpdate, KEP_TestPatchFileForUpdate)
SG_REGISTER(EnableDownloadPriorityVisualization, KEP_EnableDownloadPriorityVisualization)
SG_REGISTER(IsDownloadPriorityVisualizationEnabled, KEP_IsDownloadPriorityVisualizationEnabled)
SG_REGISTER(ArmPreLoadProgress, KEP_ArmPreLoadProgress)

// Zone Control
SG_REGISTER(GetZoneIndex, KEP_GetZoneIndex)
SG_REGISTER(GetZoneIndexType, KEP_GetZoneIndexType)
SG_REGISTER(GetZoneFileName, KEP_GetZoneFileName)
SG_REGISTER(GetEnvironment, KEP_GetEnvironment)
SG_REGISTER(SetZoneAllowsLabels, KEP_SetZoneAllowsLabels)
SG_REGISTER(SpawnToRebirthPoint, KEP_SpawnToRebirthPoint)
SG_REGISTER(SetZoneLoadingUIActive, KEP_SetZoneLoadingUIActive)
SG_REGISTER(SetZoneLoadingProgressPercent, KEP_SetZoneLoadingProgressPercent)
SG_REGISTER(GetNumPlayersInZone, KEP_GetNumPlayersInZone)
SG_REGISTER(WorldIsAP, KEP_WorldIsAP)

// User Account
SG_REGISTER(CanAutoLogin, KEP_CanAutoLogin)
SG_REGISTER(Login, KEP_Login)

SG_REGISTER(Logout, KEP_Logout)

SG_REGISTER(GetKanevaUserId, KEP_GetKanevaUserId)
SG_REGISTER(IsGM, KEP_IsGM)

// Players
SG_REGISTER(PlayerIsValid, KEP_PlayerIsValid)
SG_REGISTER(GetAllNetIds, KEP_GetAllNetIds)
REGISTER_SCRIPT_FUNCTION("KEP_GetPlayer", KEP_GetPlayer)
SG_REGISTER(GetPlayerByName, KEP_GetPlayerByName)
SG_REGISTER(GetPlayerByNetworkDefinedID, KEP_GetPlayerByNetworkDefinedID)
SG_REGISTER(GetPlayerTitle, KEP_GetPlayerTitle)
SG_REGISTER(GetPlayerPosition, KEP_GetPlayerPosition)
SG_REGISTER(SetPlayerPosition, KEP_SetPlayerPosition)
SG_REGISTER(SetPlayerOrientation, KEP_SetPlayerOrientation)
SG_REGISTER(GetPlayerInfo, KEP_GetPlayerInfo)
SG_REGISTER(GetPlayerConfigList, KEP_GetPlayerConfigList)
SG_REGISTER(GetPlayerMaterialList, KEP_GetPlayerMaterialList)
SG_REGISTER(PlayerObjectSetEquippableConfig, KEP_PlayerObjectSetEquippableConfig)
SG_REGISTER(PlayerObjectSetEquippableAnimation, KEP_PlayerObjectSetEquippableAnimation)
SG_REGISTER(PlayerObjectTryEquippableAnimation, KEP_PlayerObjectTryEquippableAnimation)
SG_REGISTER(GetBoneIndexByName, KEP_GetBoneIndexByName)
SG_REGISTER(SetActorTypeOnPlayer, KEP_SetActorTypeOnPlayer)
SG_REGISTER(GetPlayerConfigIndex, KEP_GetPlayerConfigIndex)
SG_REGISTER(GetPlayerConfigMaterial, KEP_GetPlayerConfigMaterial)
SG_REGISTER(GetCharacterAttachmentTypes, KEP_GetCharacterAttachmentTypes)
SG_REGISTER(ScaleRuntimePlayer, KEP_ScaleRuntimePlayer)
SG_REGISTER(ConfigEntitySubstitute, KEP_ConfigEntitySubstitute)
SG_REGISTER(SetPlayerLookAt, KEP_SetPlayerLookAt)
REGISTER_SCRIPT_FUNCTION("KEP_GetPlayerArmedItems", KEP_GetPlayerArmedItems)

// Cameras
REGISTER_SCRIPT_FUNCTION("KEP_AttachActiveViewportCameraToObject", &IClientEngine::AttachActiveViewportCameraToObject)
SG_REGISTER(GetActorsCameraIndex, KEP_GetActorsCameraIndex)
SG_REGISTER(GetPlayersActiveCamera, KEP_GetPlayersActiveCamera)
SG_REGISTER(SetCameraCollision, KEP_SetCameraCollision)
SG_REGISTER(GetCameraLock, KEP_GetCameraLock)
SG_REGISTER(SetCameraLock, KEP_SetCameraLock)
SG_REGISTER(GetCameraMoveSpeed, KEP_GetCameraMoveSpeed)
SG_REGISTER(SetCameraMoveSpeed, KEP_SetCameraMoveSpeed)
REGISTER_SCRIPT_FUNCTION("KEP_SetPlayerCameraAutoAzimuth", &IClientEngine::SetActiveCameraAutoAzimuthForMe)
REGISTER_SCRIPT_FUNCTION("KEP_GetPlayerCameraAutoAzimuth", &IClientEngine::GetActiveCameraAutoAzimuthForMe)
REGISTER_SCRIPT_FUNCTION("KEP_SetPlayerCameraAutoElevation", &IClientEngine::SetActiveCameraAutoElevationForMe)
REGISTER_SCRIPT_FUNCTION("KEP_GetPlayerCameraAutoElevation", &IClientEngine::GetActiveCameraAutoElevationForMe)
REGISTER_SCRIPT_FUNCTION("KEP_GetPlayerCameraPosition", &IClientEngine::GetActiveCameraPositionForMe)
SG_REGISTER(GetPlayerCameraOrientation, KEP_GetPlayerCameraOrientation)
SG_REGISTER(GetPlayerCameraUpVector, KEP_GetPlayerCameraUpVector)

// Inventory
SG_REGISTER(RemoveInventoryItem, KEP_RemoveInventoryItem)
SG_REGISTER(UseInventoryItem, KEP_UseInventoryItem)
SG_REGISTER(UseInventoryItemOnTarget, KEP_UseInventoryItemOnTarget)
SG_REGISTER(RequestCharacterInventory, KEP_RequestCharacterInventory)
SG_REGISTER(RequestCharacterInventoryAttachableObjects, KEP_RequestCharacterInventoryAttachableObjects)
SG_REGISTER(GetItemInfo, KEP_GetItemInfo)
SG_REGISTER(AddItemThumbnailToDatabase, KEP_AddItemThumbnailToDatabase)
SG_REGISTER(ApplyFriendPicture, KEP_ApplyFriendPicture)
SG_REGISTER(ApplyGiftPicture, KEP_ApplyGiftPicture)

// Arm/Disarm
SG_REGISTER(ArmInventoryItem, KEP_ArmInventoryItem)
SG_REGISTER(ArmInventoryItemOnPlayer, KEP_ArmInventoryItemOnPlayer)
SG_REGISTER(ArmMenuGameObject, KEP_ArmMenuGameObject)
SG_REGISTER(ArmEquippableItem, KEP_ArmEquippableItem)
SG_REGISTER(DisarmInventoryItem, KEP_DisarmInventoryItem)
SG_REGISTER(DisarmInventoryItemOnPlayer, KEP_DisarmInventoryItemOnPlayer)
SG_REGISTER(DisarmEquippableItem, KEP_DisarmEquippableItem)
SG_REGISTER(Mount, KEP_Mount)
SG_REGISTER(Unmount, KEP_Unmount)

// Animations
SG_REGISTER(GetAnimationInfo, KEP_GetAnimationInfo)
SG_REGISTER(PreloadAnimationByHash, KEP_PreloadAnimationByHash)
SG_REGISTER(PreloadAnimationByGLID, KEP_PreloadAnimationByGLID)
SG_REGISTER(LoadAnimationOnPlayer, KEP_LoadAnimationOnPlayer)
SG_REGISTER(LoadAnimationOnPlayerByNetId, KEP_LoadAnimationOnPlayerByNetId)
SG_REGISTER(BakeCharacterAnimation, KEP_BakeCharacterAnimation)
SG_REGISTER(BakeAnimation, KEP_BakeAnimation)
SG_REGISTER(ChangeAnimationCropping, KEP_ChangeAnimationCropping)
SG_REGISTER(ChangeAnimationLooping, KEP_ChangeAnimationLooping)
SG_REGISTER(ChangeAnimationSpeed, KEP_ChangeAnimationSpeed)
SG_REGISTER(SetCurrentAnimation, KEP_SetCurrentAnimation)
SG_REGISTER(SetMyOverrideAnimationSettings, KEP_SetMyOverrideAnimationSettings)
SG_REGISTER(GetMyOverrideAnimationSettings, KEP_GetMyOverrideAnimationSettings)
SG_REGISTER(GetMyPrimaryAnimationSettings, KEP_GetMyPrimaryAnimationSettings)
SG_REGISTER(AddAnimationToUGCActor, KEP_AddAnimationToUGCActor)
// DEPRECATED
SG_REGISTER(SetCurrentAnimationByGLID, KEP_SetCurrentAnimationByGLID)
SG_REGISTER(GetCurrentAnimation, KEP_GetCurrentAnimation)

// Particles
SG_REGISTER(GetParticleSystemByGLID, KEP_GetParticleSystemByGLID)
SG_REGISTER(GetParticleSystemByName, KEP_GetParticleSystemByName)
SG_REGISTER(AddParticleSystemToDynamicObject, KEP_AddParticleSystemToDynamicObject)
SG_REGISTER(RemoveParticleSystemFromDynamicObject, KEP_RemoveParticleSystemFromDynamicObject)
SG_REGISTER(CreateNewUGCParticleSystem, KEP_CreateNewUGCParticleSystem)
SG_REGISTER(PlayParticleEffectOnPlayer, KEP_PlayParticleEffectOnPlayer)

// Textures
SG_REGISTER(DownloadTexture, KEP_DownloadTexture)
SG_REGISTER(DownloadTextureAsync, KEP_DownloadTextureAsync)
SG_REGISTER(DownloadTextureAsyncStr, KEP_DownloadTextureAsyncStr)
SG_REGISTER(DownloadTextureAsyncToGameId, KEP_DownloadTextureAsyncToGameId)
SG_REGISTER(AddUGCTexture, KEP_AddUGCTexture)
SG_REGISTER(ApplyCustomTexture, KEP_ApplyCustomTexture)
SG_REGISTER(ApplyCustomTextureLocal, KEP_ApplyCustomTextureLocal)
SG_REGISTER(ApplyCustomTextureWldObjectLocal, KEP_ApplyCustomTextureWldObjectLocal)
SG_REGISTER(ResetTextureQualityLevels, KEP_ResetTextureQualityLevels)
SG_REGISTER(EnableTextureQualityLevel, KEP_EnableTextureQualityLevel)
SG_REGISTER(ForceOnePixelTexture, KEP_ForceOnePixelTexture)

// Objects (generalize all objects here)
SG_REGISTER(ObjectIsValid, KEP_ObjectIsValid)
SG_REGISTER(ObjectExists, KEP_ObjectExists)
SG_REGISTER(ObjectSetPosition, KEP_ObjectSetPosition)
SG_REGISTER(ObjectGetPosition, KEP_ObjectGetPosition)
SG_REGISTER(ObjectSetRotation, KEP_ObjectSetRotation)
SG_REGISTER(ObjectGetRotation, KEP_ObjectGetRotation)
SG_REGISTER(GetRuntimeObjDistance, KEP_GetRuntimeObjDistance)
SG_REGISTER(SetOutlineState, KEP_SetOutlineState)
SG_REGISTER(SetOutlineColor, KEP_SetOutlineColor)
SG_REGISTER(SetOutlineZSorted, KEP_SetOutlineZSorted)
SG_REGISTER(ClickPlaceObjectByGlid, KEP_ClickPlaceObjectByGlid)
SG_REGISTER(IsModelCustomizable, KEP_IsModelCustomizable)

// Object Selection
SG_REGISTER(SelectObject, KEP_SelectObject)
SG_REGISTER(LockSelection, KEP_LockSelection)
SG_REGISTER(DeselectObject, KEP_DeselectObject)
SG_REGISTER(ClearSelection, KEP_ClearSelection)
SG_REGISTER(ReplaceSelection, KEP_ReplaceSelection)
SG_REGISTER(ToggleSelected, KEP_ToggleSelected)
SG_REGISTER(GetNumSelected, KEP_GetNumSelected)
SG_REGISTER(GetSelection, KEP_GetSelection)
SG_REGISTER(SnapSelection, KEP_SnapSelection)
SG_REGISTER(SetRolloverMode, KEP_SetRolloverMode)
SG_REGISTER(GetSelectionPivotOffset, KEP_GetSelectionPivotOffset)

// Object Widget / Build
SG_REGISTER(DisableSelectWidgetDisplay, KEP_DisableSelectWidgetDisplay)
SG_REGISTER(EnableSelectWidgetDisplay, KEP_EnableSelectWidgetDisplay)
SG_REGISTER(WidgetWorldSpace, KEP_WidgetWorldSpace)
SG_REGISTER(WidgetObjectSpace, KEP_WidgetObjectSpace)
SG_REGISTER(WidgetShowWidget, KEP_WidgetShowWidget)
SG_REGISTER(WidgetAdvancedMovement, KEP_WidgetAdvancedMovement)
SG_REGISTER(GetWidgetPosition, KEP_GetWidgetPosition)
SG_REGISTER(SetWidgetPosition, KEP_SetWidgetPosition)
SG_REGISTER(GetWidgetRotation, KEP_GetWidgetRotation)
SG_REGISTER(SetWidgetRotation, KEP_SetWidgetRotation)
SG_REGISTER(DisplayTranslationWidget, KEP_DisplayTranslationWidget)
SG_REGISTER(DisplayRotationWidget, KEP_DisplayRotationWidget)
SG_REGISTER(TurnOffWidget, KEP_TurnOffWidget)
SG_REGISTER(SetGridSpacing, KEP_SetGridSpacing)
SG_REGISTER(GetGridSpacing, KEP_GetGridSpacing)

// Dynamic Objects
SG_REGISTER(DynamicObjectGetName, KEP_DynamicObjectGetName)
SG_REGISTER(DynamicObjectGetGLID, KEP_DynamicObjectGetGLID)
SG_REGISTER(DynamicObjectGetPosition, KEP_DynamicObjectGetPosition)
SG_REGISTER(DynamicObjectGetRotation, KEP_DynamicObjectGetRotation)
SG_REGISTER(DynamicObjectGetVisibility, KEP_DynamicObjectGetVisibility)
SG_REGISTER(DynamicObjectSetVisibility, KEP_DynamicObjectSetVisibility)
SG_REGISTER(DynamicObjectGetPositionAnim, KEP_DynamicObjectGetPositionAnim)
SG_REGISTER(GetDynamicObjectCount, KEP_GetDynamicObjectCount)
SG_REGISTER(GetDynamicObjectPlacementIdByPos, KEP_GetDynamicObjectPlacementIdByPos)
SG_REGISTER(DynamicObjectGetInited, KEP_DynamicObjectGetInited)
SG_REGISTER(DynamicObjectGetInfo, KEP_DynamicObjectGetInfo)
SG_REGISTER(DynamicObjectGetBoundingBox, KEP_DynamicObjectGetBoundingBox)
SG_REGISTER(DeleteDynamicObjectPlacement, KEP_DeleteDynamicObjectPlacement)
SG_REGISTER(AddDynamicObjectPlacement, KEP_AddDynamicObjectPlacement)
SG_REGISTER(SetDynamicObjectRotation, KEP_SetDynamicObjectRotation)
SG_REGISTER(MoveDynamicObjectPlacement, KEP_MoveDynamicObjectPlacement)
SG_REGISTER(MoveDynamicObjectPlacementInDirection, KEP_MoveDynamicObjectPlacementInDirection)
SG_REGISTER(SetDynamicObjectPosition, KEP_SetDynamicObjectPosition)
SG_REGISTER(SaveDynamicObjectPlacementChanges, KEP_SaveDynamicObjectPlacementChanges)
SG_REGISTER(CancelDynamicObjectPlacementChanges, KEP_CancelDynamicObjectPlacementChanges)
SG_REGISTER(IsDynamicPlacementObjectCustomizable, KEP_IsDynamicPlacementObjectCustomizable)
SG_REGISTER(PlaceLocalDynamicObj, KEP_PlaceLocalDynamicObj)
SG_REGISTER(PlaceDynamicObjectAtPosition, KEP_PlaceDynamicObjectAtPosition)
SG_REGISTER(SetDynamicObjectAnimationSettings, KEP_SetDynamicObjectAnimationSettings)
SG_REGISTER(GetDynamicObjectAnimationSettings, KEP_GetDynamicObjectAnimationSettings)
SG_REGISTER(GetDynamicObjectParticleSystem, KEP_GetDynamicObjectParticleSystem)
SG_REGISTER(SubmitLocalDynamicObjectByPlacementId, KEP_SubmitLocalDynamicObjectByPlacementId)
SG_REGISTER(CanDynamicObjectSupportAnimation, KEP_CanDynamicObjectSupportAnimation)
SG_REGISTER(DynamicObjectMakeTranslucent, KEP_DynamicObjectMakeTranslucent)
SG_REGISTER(EnableDynamicObjectBoundingBoxRender, KEP_EnableDynamicObjectBoundingBoxRender)
SG_REGISTER(SetObjectHighlight, KEP_SetObjectHighlight)
REGISTER_SCRIPT_FUNCTION("KEP_DynamicObjectEnableCollision", &IClientEngine::DynamicObjectCollisionFilter)

// DEPRECATED
SG_REGISTER(SetAnimationOnDynamicObject, KEP_SetAnimationOnDynamicObject)
SG_REGISTER(GetDynamicObjectAnimation, KEP_GetDynamicObjectAnimation)
SG_REGISTER(DynamicObjectRefreshTextures, KEP_DynamicObjectRefreshTextures)
SG_REGISTER(AssignDynamicObjectToTemplate, KEP_AssignDynamicObjectToTemplate)
SG_REGISTER(StartDynamicObjectDerivationByPlacementId, KEP_StartDynamicObjectDerivationByPlacementId)
SG_REGISTER(SubmitDynamicObjectDerivationByPlacementId, KEP_SubmitDynamicObjectDerivationByPlacementId)
SG_REGISTER(DynamicObjectPlacementTemplateSave, KEP_DynamicObjectPlacementTemplateSave)

// World Objects
SG_REGISTER(GetWorldObjectInfo, KEP_GetWorldObjectInfo)
SG_REGISTER(SaveWldObjectChanges, KEP_SaveWldObjectChanges)
SG_REGISTER(CancelWldObjectChanges, KEP_CancelWldObjectChanges)
SG_REGISTER(ApplyCustomTextureWldObject, KEP_ApplyCustomTextureWldObject)

// Menu Game Objects
SG_REGISTER(AddMenuGameObject, KEP_AddMenuGameObject)
SG_REGISTER(AddMenuGameObjectByEquippableGlid, KEP_AddMenuGameObjectByEquippableGlid)
SG_REGISTER(AddMenuDynamicObjectByPlacement, KEP_AddMenuDynamicObjectByPlacement)
SG_REGISTER(MenuGameObjectSetEquippableConfig, KEP_MenuGameObjectSetEquippableConfig)
SG_REGISTER(RemoveMenuGameObject, KEP_RemoveMenuGameObject)
SG_REGISTER(MenuGameObjectScreenshotCapture, KEP_MenuGameObjectScreenshotCapture)
SG_REGISTER(MenuGameObjectMoveToTop, KEP_MenuGameObjectMoveToTop)
SG_REGISTER(MenuGameObjectMoveToBottom, KEP_MenuGameObjectMoveToBottom)
SG_REGISTER(MenuGameObjectSetDOZoomFactor, KEP_MenuGameObjectSetDOZoomFactor)
SG_REGISTER(MenuGameObjectSetViewportBackgroundColor, KEP_MenuGameObjectSetViewportBackgroundColor)
SG_REGISTER(MenuGameObjectSetAnimationTime, KEP_MenuGameObjectSetAnimationTime)
SG_REGISTER(MenuGameObjectSetAnimation, KEP_MenuGameObjectSetAnimation)
SG_REGISTER(MenuGameObjectSetAnimationSettings, KEP_MenuGameObjectSetAnimationSettings)

// Media
SG_REGISTER(SetMasterVolume, KEP_SetMasterVolume)
SG_REGISTER(GetMasterVolume, KEP_GetMasterVolume)
SG_REGISTER(SetMute, KEP_SetMute)
SG_REGISTER(GetMute, KEP_GetMute)
SG_REGISTER(SetMediaEnabled, KEP_SetMediaEnabled)
SG_REGISTER(GetMediaEnabled, KEP_GetMediaEnabled)
SG_REGISTER(GetMediaParamsOnDynamicObject, KEP_GetMediaParamsOnDynamicObject)
SG_REGISTER(SetMediaParamsOnDynamicObject, KEP_SetMediaParamsOnDynamicObject)
SG_REGISTER(GetMediaVolumeOnDynamicObject, KEP_GetMediaVolumeOnDynamicObject)
SG_REGISTER(SetMediaVolumeOnDynamicObject, KEP_SetMediaVolumeOnDynamicObject)
SG_REGISTER(GetMediaRadiusOnDynamicObject, KEP_GetMediaRadiusOnDynamicObject)
SG_REGISTER(SetMediaRadiusOnDynamicObject, KEP_SetMediaRadiusOnDynamicObject)
SG_REGISTER(EnableMediaTriggerRender, KEP_EnableMediaTriggerRender)
SG_REGISTER(StartPlayingFlashOnObject, KEP_StartPlayingFlashOnObject)
SG_REGISTER(StartPlayingFlashOnObjectSecure, KEP_StartPlayingFlashOnObjectSecure)
SG_REGISTER(StartPlayingFlashOnNamedObject, KEP_StartPlayingFlashOnNamedObject)
SG_REGISTER(StartPlayingFlashOnNamedObjectSecure, KEP_StartPlayingFlashOnNamedObjectSecure)
SG_REGISTER(StopPlayingFlashOnObject, KEP_StopPlayingFlashOnObject)
SG_REGISTER(StopPlayingFlashOnNamedObject, KEP_StopPlayingFlashOnNamedObject)

// Sound Placements
SG_REGISTER(GetSoundPlacementIdList, KEP_GetSoundPlacementIdList)
SG_REGISTER(SoundPlacementIsValid, KEP_SoundPlacementIsValid)
SG_REGISTER(SoundPlacementGetInfo, KEP_SoundPlacementGetInfo)
SG_REGISTER(EnableSoundPlacementRender, KEP_EnableSoundPlacementRender)
SG_REGISTER(SoundPlacementGet, KEP_SoundPlacementGet)
SG_REGISTER(SoundPlacementAdd, KEP_SoundPlacementAdd)
SG_REGISTER(SoundPlacementDel, KEP_SoundPlacementDel)
SG_REGISTER(SoundPlacementGetPosition, KEP_SoundPlacementGetPosition)
SG_REGISTER(SoundPlacementSetPosition, KEP_SoundPlacementSetPosition)
SG_REGISTER(SoundPlacementGetRotation, KEP_SoundPlacementGetRotation)
SG_REGISTER(SoundPlacementSetRotation, KEP_SoundPlacementSetRotation)
SG_REGISTER(SoundPlacementSetRotationVector, KEP_SoundPlacementSetRotationVector)
SG_REGISTER(SoundPlacementPlaySound, KEP_SoundPlacementPlaySound)
SG_REGISTER(PlaySoundOnDynamicObject, KEP_PlaySoundOnDynamicObject)
SG_REGISTER(RemoveSoundOnDynamicObject, KEP_RemoveSoundOnDynamicObject)
SG_REGISTER(PlaySoundOnPlayer, KEP_PlaySoundOnPlayer)
SG_REGISTER(RemoveSoundOnPlayer, KEP_RemoveSoundOnPlayer)
SG_REGISTER(PlaySoundById, KEP_PlaySoundById)

// Chat
SG_REGISTER(SendChatMessage, KEP_SendChatMessage)
SG_REGISTER(SendChatMessageBroadcast, KEP_SendChatMessageBroadcast)

// Geometry
REGISTER_SCRIPT_FUNCTION("KEP_CreateStandardGeometry", &IClientEngine::CreateStandardGeometry)
REGISTER_SCRIPT_FUNCTION("KEP_CreateCustomGeometry", &IClientEngine::CreateCustomGeometry)
REGISTER_SCRIPT_FUNCTION("KEP_DestroyGeometry", &IClientEngine::DestroyGeometry)
REGISTER_SCRIPT_FUNCTION("KEP_SetGeometryVisible", &IClientEngine::SetGeometryVisible)
REGISTER_SCRIPT_FUNCTION("KEP_MoveGeometry", &IClientEngine::MoveGeometry)
REGISTER_SCRIPT_FUNCTION("KEP_RotateGeometry", &IClientEngine::RotateGeometry)
REGISTER_SCRIPT_FUNCTION("KEP_ScaleGeometry", &IClientEngine::ScaleGeometry)
REGISTER_SCRIPT_FUNCTION("KEP_SetGeometryRefObj", &IClientEngine::SetGeometryRefObj)
REGISTER_SCRIPT_FUNCTION("KEP_SetGeometryDrawMode", &IClientEngine::SetGeometryDrawMode)
REGISTER_SCRIPT_FUNCTION("KEP_SetGeometryMaterial", &IClientEngine::SetGeometryMaterial)

// Triggers
SG_REGISTER(EnableTriggerRender, KEP_EnableTriggerRender)

// Physics
SG_REGISTER(ShowPhysicsWindow, KEP_ShowPhysicsWindow)

// Events
SG_REGISTER(ConfigEventDispatchTime, KEP_ConfigEventDispatchTime)
SG_REGISTER(SendInteractCommand, KEP_SendInteractCommand)

//Ankit ----- APIs for dev option menu functionality
SG_REGISTER(ExecuteFunction, KEP_ExecuteFunction)
SG_REGISTER(GetDevOptionsDefinedInEngine, KEP_GetDevOptionsDefinedInEngine)

//Ankit ----- APIs for SATFrameworkMenuScript
REGISTER_SCRIPT_FUNCTION("SAT_PostKeyboardMouseMsg", &IClientEngine::PostKeyboardMouseMsg)
REGISTER_SCRIPT_FUNCTION("SAT_GETGetSetInt", &IClientEngine::GETGetSetInt)
REGISTER_SCRIPT_FUNCTION("SAT_SETGetSetInt", &IClientEngine::SETGetSetInt)
REGISTER_SCRIPT_FUNCTION("SAT_GETGetSetString", &IClientEngine::GETGetSetString)
REGISTER_SCRIPT_FUNCTION("SAT_SETGetSetString", &IClientEngine::SETGetSetString)
REGISTER_SCRIPT_FUNCTION("SAT_LogMsg", &IClientEngine::LogMsgForSAT)
REGISTER_SCRIPT_FUNCTION("SAT_SetScrollBarInMenuToContainIndex", &IClientEngine::SetScrollBarInMenuToContainIndex)
REGISTER_SCRIPT_FUNCTION("SAT_SetTextForEditBoxInMenu", &IClientEngine::SetTextForEditBoxInMenu)
REGISTER_SCRIPT_FUNCTION("SAT_ComboBoxSetSelectedByDataInMenu", &IClientEngine::ComboBoxSetSelectedByDataInMenu)
REGISTER_SCRIPT_FUNCTION("SAT_GetInfoBoxMessage", &IClientEngine::GetInfoBoxMessage)
REGISTER_SCRIPT_FUNCTION("SAT_IsButtonInMenuEnabled", &IClientEngine::IsButtonInMenuEnabled)
REGISTER_SCRIPT_FUNCTION("SAT_UseButtonInsideMenu", &IClientEngine::UseButtonInsideMenu)
REGISTER_SCRIPT_FUNCTION("SAT_UseRadioButtonInsideMenu", &IClientEngine::UseRadioButtonInsideMenu)
REGISTER_SCRIPT_FUNCTION("SAT_CheckIfMenuLoaded", &IClientEngine::CheckIfMenuLoaded)
REGISTER_SCRIPT_FUNCTION("SAT_CheckIfScreenshotImageSaved", &IClientEngine::CheckIfScreenshotImageSaved)
REGISTER_SCRIPT_FUNCTION("SAT_CheckIfPacketRedeemedForUser", &IClientEngine::CheckIfPacketRedeemedForUser)
REGISTER_SCRIPT_FUNCTION("SAT_CheckIfParticleEffectPlayingOnPlayer", &IClientEngine::CheckIfParticleEffectPlayingOnPlayer)
REGISTER_SCRIPT_FUNCTION("SAT_GetItemsArmedByPlayer", &IClientEngine::GetItemsArmedByPlayer)
REGISTER_SCRIPT_FUNCTION("SAT_IsPlayerAP", &IClientEngine::IsPlayerAP)
REGISTER_SCRIPT_FUNCTION("SAT_IsPlayerVIP", &IClientEngine::IsPlayerVIP)
REGISTER_SCRIPT_FUNCTION("SAT_IsDynamicObjectWithPIDValid", &IClientEngine::IsDynamicObjectWithPIDValid)
REGISTER_SCRIPT_FUNCTION("SAT_CreateTestUser", &IClientEngine::CreateTestUser)
REGISTER_SCRIPT_FUNCTION("SAT_CloseAllBrowsers", &IClientEngine::CloseAllBrowsers)
REGISTER_SCRIPT_FUNCTION("SAT_IsBrowserOpenWithTitleAndURL", &IClientEngine::IsBrowserOpenWithTitleAndURL)
REGISTER_SCRIPT_FUNCTION("SAT_IsChatWindowInForeground", &IClientEngine::IsChatWindowInForeground)
REGISTER_SCRIPT_FUNCTION("SAT_BringKanevaClientToForeground", &IClientEngine::BringKanevaClientToForeground)
REGISTER_SCRIPT_FUNCTION("SAT_FinishTestingAndCloseClient", &IClientEngine::FinishTestingAndCloseClient)

// #MLB_BulletSensor - SG_REGISTER LandClaim
SG_REGISTER(IsObjectInLandClaim, KEP_IsObjectInLandClaim)
SG_REGISTER(RegisterLandClaim, KEP_RegisterLandClaim)
SG_REGISTER(UnRegisterLandClaim, KEP_UnRegisterLandClaim)

#if 1 // TODO - DEPRECATED

// Store / Bank
SG_REGISTER(BuyItemFromStore, KEP_BuyItemFromStore)
SG_REGISTER(SellItemToStore, KEP_SellItemToStore)
SG_REGISTER(DepositItemToBank, KEP_DepositItemToBank)
SG_REGISTER(RemoveBankItem, KEP_RemoveBankItem)
SG_REGISTER(WithdrawItemFromBank, KEP_WithdrawItemFromBank)

// Dev Worlds
SG_REGISTER(Delete3DAppAsset, KEP_Delete3DAppAsset)
SG_REGISTER(Count3DAppUnpublishedAssets, KEP_Count3DAppUnpublishedAssets)
SG_REGISTER(Get3DAppUnpublishedAsset, KEP_Get3DAppUnpublishedAsset)
SG_REGISTER(Get3DAppUnpublishedAssetPath, KEP_Get3DAppUnpublishedAssetPath)
SG_REGISTER(Import3DAppAsset, KEP_Import3DAppAsset)
SG_REGISTER(Import3DAppAssetFromString, KEP_Import3DAppAssetFromString)
SG_REGISTER(RequestSourceAssetFromAppServer, KEP_RequestSourceAssetFromAppServer)

// Characters
SG_REGISTER(PlayCharacter, KEP_PlayCharacter)
SG_REGISTER(CreateCharacter, KEP_CreateCharacter)
SG_REGISTER(CreateCharacterFromPlayer, KEP_CreateCharacterFromPlayer)
SG_REGISTER(DeleteCharacter, KEP_DeleteCharacter)
SG_REGISTER(GetCharacterMaxCount, KEP_GetCharacterMaxCount)
SG_REGISTER(RequestPictureFrames, KEP_RequestPictureFrames)
SG_REGISTER(RequestSkills, KEP_RequestSkills)
SG_REGISTER(SelectQuest, KEP_SelectQuest)
SG_REGISTER(SetCharacterAltConfig, KEP_SetCharacterAltConfig)
SG_REGISTER(SetCharacterAltConfigOnPlayer, KEP_SetCharacterAltConfigOnPlayer)
SG_REGISTER(SetCharacterAltConfigColor, KEP_SetCharacterAltConfigColor)
SG_REGISTER(UseSkillAbility, KEP_UseSkillAbility)
#endif

// Experimental engine options
SG_REGISTER(SetExperimentalEngineOption, KEP_SetExperimentalEngineOption)
SG_REGISTER(GetExperimentalEngineOption, KEP_GetExperimentalEngineOption)

// Local, procedurally generated dynamic texture A8R8G8B8. Usage: world map, paintable surface, etc.
SG_REGISTER(CreateDynamicTexture, KEP_CreateDynamicTexture)
SG_REGISTER(DestroyDynamicTexture, KEP_DestroyDynamicTexture)

// World Map Generator
SG_REGISTER(GetWorldMapBoundary, KEP_GetWorldMapBoundary)
SG_REGISTER(GenerateWorldMap, KEP_GenerateWorldMap)

// NPC Functions
REGISTER_SCRIPT_FUNCTION("KEP_npcSpawn", &KEP_npcSpawn)
REGISTER_SCRIPT_FUNCTION("KEP_npcDeSpawn", &KEP_npcDeSpawn)

REGISTER_SCRIPT_FUNCTION("KEP_SetFullScreen", &IClientEngine::SetFullScreen)
REGISTER_SCRIPT_FUNCTION("KEP_IsFullScreen", &IClientEngine::IsFullScreen)

REGISTER_SCRIPT_FUNCTION("KEP_GetVehicleSpeedMPH", []() {
	auto pICE = IClientEngine::Instance();
	return pICE ? KPHToMPH(pICE->GetMyVehicleSpeedKPH()) : 0;
})

REGISTER_SCRIPT_FUNCTION("KEP_EnableClient", &IClientEngine::EnableClient)
REGISTER_SCRIPT_FUNCTION("KEP_DisableClient", &IClientEngine::DisableClient)
REGISTER_SCRIPT_FUNCTION("KEP_IsClientEnabled", &IClientEngine::IsClientEnabled)

REGISTER_SCRIPT_FUNCTION("KEP_SetMsgMoveEnabled", &IClientEngine::SetMsgMoveEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_IsMsgMoveEnabled", &IClientEngine::IsMsgMoveEnabled)

REGISTER_SCRIPT_FUNCTION("KEP_SetMsgGzipSize", &IClientEngine::SetMsgGzipSize)
REGISTER_SCRIPT_FUNCTION("KEP_GetMsgGzipSize", &IClientEngine::GetMsgGzipSize)

REGISTER_SCRIPT_FUNCTION("KEP_SetMsgGzipLogEnabled", &IClientEngine::SetMsgGzipLogEnabled)
REGISTER_SCRIPT_FUNCTION("KEP_IsMsgGzipLogEnabled", &IClientEngine::IsMsgGzipLogEnabled)

REGISTER_SCRIPT_FUNCTION("KEP_PlacementCollisionTest", &IClientEngine::DynamicObjectCollisionTest)

REGISTER_SCRIPT_FUNCTION("KEP_GetRuntimePlayerScale", &GetRuntimePlayerScale)

SG_END_REGISTER
SG_END_VM_REGISTER
