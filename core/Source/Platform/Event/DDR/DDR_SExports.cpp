///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "Event\Base\IEvent.h"
#include "../../Engine/Blades/BladeExports.h"

#include "DDR_S.h"

#include "../../Engine/Blades/IBlade.h"
using namespace KEP;

IMPLEMENT_CREATE(DDR_S)

BEGIN_BLADE_LIST_ENGINE_TYPE(eEngineType::Server)
ADD_BLADE(DDR_S)
END_BLADE_LIST
