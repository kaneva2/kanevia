///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "DDR_S.h"
#include "resource.h"

#include "Plugins/App/DDRModelFacet.h"
#include "..\Events\DDREvent.h"
#include "..\Events\DDRDBEvent.h"
#include "..\Events\ShutdownEvent.h"
#include "..\Base\IDispatcher.h"
#include "..\Glue\PlayerIds.h"
#include "IServerEngine.h"
#include "IPlayer.h"
#include "IGetSet.h"
#include "Common\include\CoreStatic\InstanceId.h"

#include "..\Base\IEncodingFactory.h"

#include <assert.h>

using namespace std;

// Defaults, overridden by database
#define PURGE_TIMEOUT_SECONDS 180
#define NOTIFY_INACTIVE_PLAYER_TIMEOUT_SECONDS 5
#define DANCEFLOOR_LIMIT 10

#include <malloc.h>

namespace KEP {
/*
 *Function Name:DDRStartHandler
 *
 *Parameters:	lparam, item passed when registering the event
 *				dispatcher
 *				the event
 *
 *Description:Called from client to obtain meta-information about the game. Meta data kept
 *				in memory after first load.
 *
 *Returns:	EVENT_PROC_RC::CONSUMED	- ate the event
 *			EVENT_PROC_RC::OK			- not processed, continue on with any other handlers
 *
 */
EVENT_PROC_RC DDR_S::DDRStartHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR_S *me = (DDR_S *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	NDCContextCreator ndc(me->GetLoggingContext());

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	if (!me->LoadGameData(ddre->GetGameId())) {
		return ret;
	}

	IServerEngine *game = dynamic_cast<IServerEngine *>(disp->Game());
	IPlayer *player = game->GetPlayerByNetId(ddre->From(), true);
	if (player == 0) {
		LOG4CPLUS_INFO(me->m_logger, "Player not found for NETID in DDRStartHandler: " << ddre->From());
		return ret;
	}
	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);
	int dynObjectId = ddre->m_objectId;

	me->PurgeStalePlayers();

	// Has max for dance floor been reached
	if (ddre->m_newGame && (!me->DynamicObjectAccessAllowed(player, ddre->GetGameId(), ddre->m_objectId))) {
		LOG4CPLUS_INFO(me->m_logger, "Access denied for player:" << playerId);

		DDREvent *ddre_access = new DDREvent();
		ddre_access->AccessDenied(ddre->GetGameId(), playerId, dynObjectId);
		ddre_access->AddTo(ddre->From());
		me->GetDispatcher()->QueueEvent(ddre_access);
	} else {
		string playerName = player->Values()->GetString(PLAYERIDS_CHARNAME);
		NETID playerNetId = player->Values()->GetULONG(PLAYERIDS_NETID);

		int playerState = player->Values()->GetINT(PLAYERIDS_INTERACTIONSTATES);
		playerState |= DANCE_PARTY;
		player->Values()->SetNumber(PLAYERIDS_INTERACTIONSTATES, playerState);

		// Is the player being tracked already? If not, load the players persisted data from the db
		bool newPlayer = false;
		DDRPlayers *players = NULL;
		DDRPlayerObj *l_player = me->GetPlayer(ddre->GetGameId(), dynObjectId, playerId, playerName, playerNetId, players, newPlayer);

		if (newPlayer) {
			// start async load player data
			me->LoadPlayerData(ddre->GetGameId(), ddre->GetGameLevel(), ddre->From(), ddre->m_newGame, dynObjectId, playerId);
		} else {
			me->CompleteStartGame(l_player, ddre->GetGameLevel(), player, dynObjectId, ddre->m_newGame);
		}
	}
	return ret;
}

/*
 *Function Name:CompleteStartGame
 *
 *Parameters:	gameId this game
 *				gameLevel
 *				who sent it
 *				if a new game or not
 *				id of player starting the game
 *
 *Description:Called during start of game.  If player needs to be loaded async, 
 *				then this is called from return of load, otherwise from start handler
 *
 */
void DDR_S::CompleteStartGame(DDRPlayerObj *l_player, ULONG gameLevel, IPlayer *player, int dynObjectId, bool newGame){
	{ // Return game meta information to client for a specific level
		GameLevelsMap::iterator jj = m_gameLevelsMap.find(l_player->GetGameId());

if (jj != m_gameLevelsMap.end()) {
	DDRLevels *levels = jj->second;

	LevelMap::iterator kk = levels->m_levelsMap.find(gameLevel);

	if (kk != levels->m_levelsMap.end()) {
		DDRLevelObj *l_level = kk->second;

		DDREvent *ddr_gamedata = new DDREvent();
		ddr_gamedata->StartLevelData(l_player->GetPlayerId(),
			l_player->GetGameId(),
			gameLevel,
			l_level->GetSeed(),
			l_level->GetNumSecondsToPlay(),
			l_level->GetNumMoves(),
			l_level->GetNumMisses(),
			l_level->GetLevelBonusTriggerNum(),
			l_level->GetAnimationFlareCount(),
			l_level->GetAnimationFinalFlareCount(),
			l_level->GetAnimationMissCount(),
			l_level->GetAnimationLevelUpCount(),
			l_level->GetAnimationPerfectCount(),
			l_level->GetAnimationIdleCount(),
			l_level->GetNumSubLevels(),
			l_level->GetPerfectScoreCeiling(),
			l_level->GetGreatScoreCeiling(),
			l_level->GetGoodScoreCeiling(),
			l_level->GetLastLevelReductionMilliSeconds(),
			l_level->m_AnimationFlares,
			l_level->m_AnimationFinalFlares,
			l_level->m_AnimationMisses,
			l_level->m_AnimationLevelUps,
			l_level->m_AnimationPerfects,
			l_level->m_AnimationIdles);

		ddr_gamedata->AddTo(l_player->GetNetID());
		GetDispatcher()->QueueEvent(ddr_gamedata);
	}
}

if (newGame) {
	// return score list to player before they record their first score if player just entered
	ULONG score = 0;
	ULONG highScore = 0;
	ULONG allTimeHighScore = 0;
	string allTimeHighScoreCharacterName;

	TrackPlayerScore(player, l_player->GetGameId(), dynObjectId, gameLevel, 0, true, false, false, false, 0, 0, "", "", 0, "", score, highScore, allTimeHighScore, allTimeHighScoreCharacterName); // 0 for score
	PostPlayersScores(l_player->GetNetID(), player, l_player->GetGameId(), dynObjectId, gameLevel, allTimeHighScore, allTimeHighScoreCharacterName);
}
} // namespace KEP
}
;

/*
 *Function Name:DDRRecordScoreHandler
 *
 *Parameters:	lparam, item passed when registering the event
 *				dispatcher
 *				the event
 *
 *Description:Called from client to post a level score
 *
 *Returns:	EVENT_PROC_RC::CONSUMED	- ate the event
 *			EVENT_PROC_RC::OK		- not processed, continue on with any other handlers
 *
 */
EVENT_PROC_RC DDR_S::DDRRecordScoreHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR_S *me = (DDR_S *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	NDCContextCreator ndc(me->GetLoggingContext());

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	IServerEngine *game = dynamic_cast<IServerEngine *>(disp->Game());
	IPlayer *player = game->GetPlayerByNetId(ddre->From(), true);
	if (player == 0) {
		LOG4CPLUS_INFO(me->m_logger, "Player not found for NETID in DDRRecordScoreHandler " << ddre->From());
		return ret;
	}
	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);

	// Check if time elaspsed to purge stale players
	me->PurgeStalePlayers();

	ULONG score = 0;
	ULONG highScore = 0;
	ULONG allTimeHighScore = 0;
	string allTimeHighScoreCharacterName;
	ULONG fameGainAmount = 0;
	int newFameLevel = 0;
	string levelMessage;
	string gainMessage;
	int percentToNextLevel = 0;
	string rewardsAtNextLevel;
	float totalFame = 0;

	if (!ddre->m_gameOver) {
		GameLevelsMap::iterator ii = me->m_gameLevelsMap.find(ddre->GetGameId());
		if (ii != me->m_gameLevelsMap.end()) {
			DDRLevels *levels = ii->second;
			LevelMap::iterator jj = levels->m_levelsMap.find(ddre->GetGameLevel());
			if (jj != levels->m_levelsMap.end()) {
				DDRLevelObj *l_level = jj->second;

				if (ddre->m_perfectAchieved) {
					if (game->UseSkillActionOnPlayer(player, l_level->GetSkillId(), l_level->GetPerfectActionId(), ddre->m_useAlternate, newFameLevel, levelMessage, gainMessage, percentToNextLevel, rewardsAtNextLevel, totalFame)) {
						fameGainAmount = l_level->GetPerfectGainAmount();
					}
				} else {
					if (game->UseSkillActionOnPlayer(player, l_level->GetSkillId(), l_level->GetActionId(), ddre->m_useAlternate, newFameLevel, levelMessage, gainMessage, percentToNextLevel, rewardsAtNextLevel, totalFame)) {
						fameGainAmount = l_level->GetGainAmount();
					}
				}
			}
		}
	}

	// Update in memory list
	me->TrackPlayerScore(player, ddre->GetGameId(), ddre->m_objectId, ddre->GetGameLevel(), ddre->m_numMilliSecondsExpired, false, ddre->m_levelComplete, ddre->m_gameOver, ddre->m_perfectBonus, fameGainAmount, newFameLevel, levelMessage, gainMessage, percentToNextLevel, rewardsAtNextLevel, score /* */, highScore /* */, allTimeHighScore /* */, allTimeHighScoreCharacterName /* */);

	// Send score to client subscribers
	DDREvent *ddr_score = new DDREvent();
	ddr_score->PostDDRScore(playerId,
		ddre->GetGameId(),
		ddre->GetGameLevel(),
		5, // retire the gameInstance parameter, set to 5 temporarily to ignore in client
		score,
		highScore,
		fameGainAmount,
		newFameLevel,
		percentToNextLevel,
		rewardsAtNextLevel,
		gainMessage,
		totalFame);

	ddr_score->AddTo(ddre->From());
	me->GetDispatcher()->QueueEvent(ddr_score);

	// Send all scores to client subscribers of this dynamic object (dance floor)
	me->PostPlayersScores(ddre->From(), player, ddre->GetGameId(), ddre->m_objectId, ddre->GetGameLevel(), allTimeHighScore, allTimeHighScoreCharacterName);

	return ret;
};

/*
 *Function Name:DDRPlayerExitHandler
 *
 *Parameters:	lparam, item passed when registering the event
 *				dispatcher
 *				the event
 *
 *Description:Called from client to alert that player has left the game.
 *
 *Returns:	EVENT_PROC_RC::CONSUMED	- ate the event
 *			EVENT_PROC_RC::OK			- not processed, continue on with any other handlers
 *
 */
EVENT_PROC_RC DDR_S::DDRPlayerExitHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR_S *me = (DDR_S *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	NDCContextCreator ndc(me->GetLoggingContext());

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	IServerEngine *game = dynamic_cast<IServerEngine *>(disp->Game());
	IPlayer *player = game->GetPlayerByNetId(ddre->From(), true);

	if (player == 0) {
		LOG4CPLUS_INFO(me->m_logger, "Player not found for NETID in DDRPlayerExitHandler " << ddre->From());
		return ret;
	}

	me->PlayerExit(player, ddre->GetGameId(), ddre->m_objectId, ddre->m_leftFloor);

	int dynObjectId = ddre->m_objectId;

	DynObjPlayersMap::iterator ii = me->m_dynObjectPlayersMap.find(dynObjectId);

	if (ii == me->m_dynObjectPlayersMap.end()) {
		return ret;
	}

	DDRPlayers *players = ii->second;

	me->PostScoresToInactivePlayers(players, ddre->GetGameId());

	if (players->GetHighScoreDirty()) {
		try {
			DataModel::ptr_t model = PluginHost::singleton().getInterface<DataModel>("AppDataModel");
			DDRModel &ddrModel = model->registry().getFacet<DDRModel>("DDRModel");
			SuppressedDDRModel(ddrModel).recordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(), players->GetHighScore(), players->GetHighScoreCharacterName(), players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex());
		} catch (const PluginNotFoundException &) {
			me->m_db->RecordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(), players->GetHighScore(), players->GetHighScoreCharacterName(), players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex());
		} catch (const FacetUnavailableException &) {
			me->m_db->RecordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(), players->GetHighScore(), players->GetHighScoreCharacterName(), players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex());
		}

		players->SetHighScoreDirty(false);
	}

	return ret;
};

/*
 *Function Name:DDRPlayerPausedHandler
 *
 *Parameters:	lparam, item passed when registering the event
 *				dispatcher
 *				the event
 *
 *Description:Called from client to alert that player is paused/un-paused. Player will not be pruned from active list
 *				while paused.
 *
 *Returns:	EVENT_PROC_RC::CONSUMED	- ate the event
 *			EVENT_PROC_RC::OK			- not processed, continue on with any other handlers
 *
 */
EVENT_PROC_RC DDR_S::DDRPlayerPausedHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR_S *me = (DDR_S *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	NDCContextCreator ndc(me->GetLoggingContext());

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	ddre->ExtractInfo();

	IServerEngine *game = dynamic_cast<IServerEngine *>(disp->Game());
	IPlayer *player = game->GetPlayerByNetId(ddre->From(), true);

	if (player != 0) {
		me->PlayerPaused(player, ddre->m_objectId, ddre->m_isPaused);
	} else {
		LOG4CPLUS_INFO(me->m_logger, "Player not found for NETID in DDRPlayerPausedHandler " << ddre->From());
	}

	return ret;
};

EVENT_PROC_RC DDR_S::DDRDBReplyHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
	DDREvent *ddre = dynamic_cast<DDREvent *>(e);
	jsVerifyReturn(ddre != 0, EVENT_PROC_RC::NOT_PROCESSED);
	DDR_S *me = (DDR_S *)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	NDCContextCreator ndc(me->GetLoggingContext());

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	switch (e->Filter()) {
		case DDREvent::DDR_PLAYER_DATA: {
			IServerEngine *game = dynamic_cast<IServerEngine *>(disp->Game());
			IPlayer *player = game->GetPlayerByNetId(ddre->From(), true);
			if (player == 0) {
				LOG4CPLUS_INFO(me->m_logger, "Player not found after DDR_PLAYER_DATA: " << ddre->From());
				return ret;
			}

			DDRPlayerObj *l_player = 0;
			DynObjPlayersMap::iterator jj = me->m_dynObjectPlayersMap.find(ddre->m_dynObjectId);

			if (jj != me->m_dynObjectPlayersMap.end()) {
				DDRPlayers *players = jj->second;

				ZoneIndex zi(ddre->m_zoneIndexId);
				players->SetDynamicObjectObjectId(ddre->m_objectId);
				players->SetDynamicObjectZoneIndex(zi.getClearedMappedInstanceId());
				players->SetIsPermanentZone(zi.isPermanent());

				auto kk = players->m_playersMap.find(ddre->m_playerId);

				if (kk != players->m_playersMap.end()) {
					l_player = kk->second;
					l_player->SetHighScore(ddre->m_highScore);
				}

				if (ddre->m_highestScore > 0) {
					players->SetHighScore(ddre->m_highestScore);
					players->SetHighScorePlayerId(ddre->m_highestScoreId);
					players->SetHighScoreCharacterName(ddre->m_highestScoreName);
				}

				me->CompleteStartGame(l_player,
					ddre->m_levelId,
					player,
					ddre->m_dynObjectId,
					ddre->m_newGame);
			} else {
				LOG4CPLUS_ERROR(me->m_logger, "Could not find dynamic object: " << ddre->m_dynObjectId << " in DynObjPlayersMap. LoadDDRPlayerData failed.");
				return ret;
			}

		} break;
	}

	return ret;
}

/*
 *Function Name:RegisterEvents
 *
 * this method is called from the DLL entry point before
 * DDR_S::RegisterEventHandlers to register any
 * new events this handler produces
 *
 *Parameters:	dispatcher
 *				encoding factory
 *
 *Description:Registers event handlers for DDR
 *
 *Returns:	true	- ok
 *			false	- error
 *
 */
bool DDR_S::RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *ef) {
	if (dispatcher == 0 || ef == 0) {
		assert(false);
		return false;
	}

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	IGame *game = dispatcher->Game();
	if (game != 0 && game->EngineType() == eEngineType::Server) {
		RegisterEvent<DDREvent>(*dispatcher);
		RegisterEvent<DDRDBEvent>(*dispatcher);

		LOG4CPLUS_DEBUG(m_logger, "Initializing the DB object");

		NDCContextCreator ndc(GetLoggingContext());

		if (!Init(dispatcher)) {
			LOG4CPLUS_INFO(m_logger, "DB initialization failed");
			return false;
		}

		LOG4CPLUS_DEBUG(m_logger, "DB initialization successful");
	} else {
		LOG4CPLUS_INFO(m_logger, "Server DLL not needed on client");
		return false;
	}

	return true;
}

/*
 *Function Name:RegisterEventHandlers
 *
 * this method is called from the DLL entry point after
 * DDR_S::RegisterEvents to register any
 * event handlers.
 *
 *Parameters:	dispatcher
 *				encoding factory
 *
 *Description:Registers event handlers for DDR_S
 *
 *Returns:	true	- ok
 *			false	- error
 *
 */
bool DDR_S::RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *ef) {
	NDCContextCreator ndc(GetLoggingContext());

	LOG4CPLUS_DEBUG(m_logger, "Registering Event Handlers");

	if (dispatcher == 0 || ef == 0) {
		assert(false);
		return false;
	}

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	IGame *game = dispatcher->Game();
	if (game != 0 && game->EngineType() == eEngineType::Server) {
		// now register handlers
		LOG4CPLUS_DEBUG(m_logger, "Registering Event Handlers Start");
		EVENT_ID ddr_id = dispatcher->GetEventId("DDREvent");
		dispatcher->RegisterFilteredHandler(DDR_S::DDRStartHandler, (ULONG)this, "DDR_S::DDRStartHandler", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_START_LEVEL);
		dispatcher->RegisterFilteredHandler(DDR_S::DDRRecordScoreHandler, (ULONG)this, "DDR_S::DDRRecordScoreHandler", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_RECORD_SCORE);
		dispatcher->RegisterFilteredHandler(DDR_S::DDRPlayerExitHandler, (ULONG)this, "DDR_S::DDRPlayerExitHandler", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_PLAYER_EXIT);
		dispatcher->RegisterFilteredHandler(DDR_S::DDRPlayerPausedHandler, (ULONG)this, "DDR_S::DDRPlayerPausedHandler", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_PLAYER_PAUSED);
		dispatcher->RegisterFilteredHandler(DDR_S::DDRDBReplyHandler, (ULONG)this, "DDR_S::DDRDBReplyHandler", DDREvent::ClassVersion(), ddr_id, DDREvent::DDR_PLAYER_DATA, true);

		LOG4CPLUS_DEBUG(m_logger, "Registering Event Handlers End");
		SetDispatcher(dispatcher);
	}

	return true;
}

bool DDR_S::ReRegisterEventHandlers(IDispatcher * /*dispatcher*/, IEncodingFactory * /*factory*/) {
	bool ret = true;
	// do anything here you need to do if re-registering handlers
	return ret;
}

/*
 *Function Name:DDR_S constructor
 *
 *Description:Constructor for DDR_S blade.
 *
 *Returns:	none
 *
 */
DDR_S::DDR_S() :
		m_logger(Logger::getInstance(L"Blade")), m_scriptDirReadOnly(false), m_dbConfig(NULL) {
	m_name = typeid(*this).name();
	m_dispatcher = NULL;
	m_db = NULL;

	// Defaults, overridden by db
	m_inactivePlayerNotificationSeconds = NOTIFY_INACTIVE_PLAYER_TIMEOUT_SECONDS;
	m_purgeTimeoutSeconds = PURGE_TIMEOUT_SECONDS;
	m_bladeContext = "KDP";
}

/*
 *Function Name:DDR_S destructor
 *
 *Description:Destructor for DDR_S blade. Destroy the in memory list of players and game meta data.
 *
 *Returns:	none
 *
 */
DDR_S::~DDR_S() {
	DataModel::ptr_t model = PluginHost::singleton().getInterfaceNoThrow<DataModel>("AppDataModel");
	DDRModel *ddrModel = (model.operator->() == 0 ? (DDRModel *)0 : model->registry().getFacetNoThrow<DDRModel>("DDRModel"));
	for (DynObjPlayersMap::iterator ii = m_dynObjectPlayersMap.begin(); ii != m_dynObjectPlayersMap.end();) {
		int dynObjectId = ii->first; // dynamic object id
		DDRPlayers *players = ii->second; // playerMap

		for (auto jj = players->m_playersMap.begin(); jj != players->m_playersMap.end();) {
			DDRPlayerObj *l_player = jj->second;

			if (ddrModel)
				SuppressedDDRModel(*ddrModel).recordDDRScore(players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex(), dynObjectId, l_player->GetGameId(), l_player->GetPlayerId(), l_player->GetName().c_str(), l_player->GetHighScore(), l_player->GetScore(), l_player->GetNumPerfects(), l_player->GetNumPerfectsInARow());
			else
				m_db->RecordDDRScore(players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex(),
					dynObjectId, l_player->GetGameId(), l_player->GetPlayerId(), l_player->GetName().c_str(),
					l_player->GetHighScore(), l_player->GetScore(),
					l_player->GetNumPerfects(), l_player->GetNumPerfectsInARow());

			delete l_player;
			jj = players->m_playersMap.erase(jj);
		}

		if (players->m_playersMap.empty()) {
			if (players->GetHighScoreDirty() && players->GetHighScorePlayerId() != 0) {
				if (ddrModel) {
					SuppressedDDRModel(*ddrModel).recordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(), players->GetHighScore(), players->GetHighScoreCharacterName(), players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex());
				} else {
					m_db->RecordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(),
						players->GetHighScore(),
						players->GetHighScoreCharacterName(),
						players->IsPermanentZone(),
						players->GetDynamicObjectObjectId(),
						players->GetDynamicObjectZoneIndex());
				}
			}
			delete players;
			ii = m_dynObjectPlayersMap.erase(ii);
		} else {
			ii++;
		}
	}

	// Don't stop db thread before recording scores above.

	if (m_db) {
		m_db->stop();
	}

	for (GameLevelsMap::iterator kk = m_gameLevelsMap.begin(); kk != m_gameLevelsMap.end();) {
		DDRLevels *levels = kk->second;

		for (LevelMap::iterator ll = levels->m_levelsMap.begin(); ll != levels->m_levelsMap.end();) {
			DDRLevelObj *l_level = ll->second;
			l_level->m_AnimationFlares.clear();
			l_level->m_AnimationFinalFlares.clear();
			l_level->m_AnimationMisses.clear();
			l_level->m_AnimationLevelUps.clear();
			l_level->m_AnimationPerfects.clear();
			l_level->m_AnimationIdles.clear();
			delete l_level;
			ll = levels->m_levelsMap.erase(ll);
		}

		if (levels->m_levelsMap.empty()) {
			delete levels;
			kk = m_gameLevelsMap.erase(kk);
		} else {
			kk++;
		}
	}
}

/*
 *Function Name:Init
 *
 * obtains db connection
 *
 *Description:Obtains db connection. Initialize timers
 *
 *Returns:	true	- ok
 *			false	- error
 *
 */
bool DDR_S::Init(IDispatcher *dispatcher) {
	string baseDir = PathBase();

	baseDir += "\\..\\..";

	HANDLE sem_handle = ::CreateSemaphore(NULL, 0L, INT_MAX, L"KDP_DBINIT");

	if (sem_handle == NULL) {
		DWORD lastErr = GetLastError();

		if (lastErr == ERROR_ALREADY_EXISTS) {
			LOG4CPLUS_ERROR(m_logger, "Unable to coordinate DB initialization. The semaphore KDP_DBINIT already exists.");
		} else {
			LOG4CPLUS_ERROR(m_logger, "Unable to coordinate DB initialization. Creation of the semaphore KDP_DBINIT received error:" << lastErr);
		}
		return false;
	}

	jsAssert(m_dbConfig != NULL);
	if (m_dbConfig == NULL) {
		::CloseHandle(sem_handle);
		LOG4CPLUS_ERROR(m_logger, "Unable to coordinate DB initialization. DB connection not configured.");
		return false;
	}

	m_db = new DDRGameDB(baseDir.c_str(), *m_dbConfig, dispatcher);
	m_db->start();

	if (::WaitForSingleObject(sem_handle, 5000) != WAIT_OBJECT_0) {
		::CloseHandle(sem_handle);
		LOG4CPLUS_ERROR(m_logger, "Unable to coordinate DB initialization. Wait for initialization timed out.");
		return false;
	}

	::CloseHandle(sem_handle);
	sem_handle = NULL;

	time(&m_lastPurgePlayersMap);
	time(&m_notifyInactivePlayers);

	return true;
}

void DDR_S::FreeAll() {
	if (GetDispatcher() != NULL) {
		IGame *game = dynamic_cast<IGame *>(GetDispatcher()->Game());
		if (game != 0 && game->EngineType() == eEngineType::Server) {
			GetDispatcher()->RemoveHandler(DDR_S::DDRStartHandler, (ULONG)this, "DDR_S::DDRStartHandler", DDREvent::ClassId());
			GetDispatcher()->RemoveHandler(DDR_S::DDRRecordScoreHandler, (ULONG)this, "DDR_S::DDRRecordScoreHandler", DDREvent::ClassId());
			GetDispatcher()->RemoveHandler(DDR_S::DDRPlayerExitHandler, (ULONG)this, "DDR_S::DDRPlayerExitHandler", DDREvent::ClassId());
			GetDispatcher()->RemoveHandler(DDR_S::DDRPlayerPausedHandler, (ULONG)this, "DDR_S::DDRPlayerPausedHandler", DDREvent::ClassId());
		}
	}
}

/*
 *Function Name:TrackPlayerScore
 *
 *Description:Track the player's score. Deteremine if player has new high score. Determine if player has new all
 *				time high score.
 *
 *Returns:	none
 *
 */
void DDR_S::TrackPlayerScore(IPlayer *player, ULONG gameId, int dynObjectId, ULONG levelId, ULONG numMilliSecondsExpired,
	bool newGame, bool levelComplete, bool gameOver, bool perfectBonus,
	ULONG fameGainAmount, int newFameLevel, string levelMessage, string gainMessage, int percentToNextLevel, string rewardsAtNextLevel,
	ULONG &score, ULONG &highScore,
	ULONG &allTimeHighScore, string &allTimeHighScoreCharacterName) {
	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);
	string playerName = player->Values()->GetString(PLAYERIDS_CHARNAME);
	NETID playerNetId = player->Values()->GetULONG(PLAYERIDS_NETID);

	DDRPlayers *players = NULL;

	bool newPlayer = false;
	DDRPlayerObj *l_player = GetPlayer(gameId, dynObjectId, playerId, playerName, playerNetId, players /*OUT*/, newPlayer);

	if (newGame) {
		LOG4CPLUS_DEBUG(m_logger, "New player: " << playerId << " Score is 0");
		l_player->SetScore(0);
		l_player->SetNumPerfects(0);
		l_player->SetNumPerfectsInARow(0);
		l_player->SetFameGainAmount(0);
		l_player->SetLevelMessage("");
		l_player->SetGainMessage("");
		l_player->SetPercentToNextLevel(0);
		l_player->SetRewardsAtNextLevel("");
	} else {
		// This will update the l_player object
		CalcScore(l_player, gameId, levelId, numMilliSecondsExpired, levelComplete, gameOver, perfectBonus);
		LOG4CPLUS_DEBUG(m_logger, "Player id:" << playerId << " recorded score of:" << l_player->GetScore() << " for DynObj:" << dynObjectId << ". Number milliseconds expired was:" << numMilliSecondsExpired << " Level:" << levelId << " Level complete:" << levelComplete << " Game over:" << gameOver << " Perfect bonus:" << perfectBonus << " Previous Fame Gained: " << l_player->GetFameGainAmount() << " Fame Gained this call: " << fameGainAmount << " New Fame Level: " << newFameLevel);
		l_player->SetFameGainAmount(l_player->GetFameGainAmount() + fameGainAmount);

		if (newFameLevel > 0) {
			l_player->SetLevelMessage(levelMessage);
			l_player->SetGainMessage(gainMessage);
			l_player->SetPercentToNextLevel(percentToNextLevel);
			l_player->SetRewardsAtNextLevel(rewardsAtNextLevel);
		}
	}

	score = l_player->GetScore();
	highScore = l_player->GetHighScore();

	l_player->SetLastUpdateTime();

	if (l_player->GetStatus() == DDRPlayerObj::INACTIVE) {
		l_player->SetStatus(DDRPlayerObj::ACTIVE);
		players->IncrementActiveCount();
	}

	// High score for this instance of the dance floor ?
	if (score > players->GetHighScore()) {
		players->SetHighScore(score);
		players->SetHighScoreDirty(true);
		players->SetHighScoreCharacterName(playerName);
		players->SetHighScorePlayerId(playerId);
	}

	allTimeHighScore = players->GetHighScore();
	allTimeHighScoreCharacterName = players->GetHighScoreCharacterName();
}

/*
 *Function Name:GetPlayer
 *
 *Description:Either returns the current playerId or create it and return it.
 *
 *Returns:	none
 *
 */
DDRPlayerObj *DDR_S::GetPlayer(ULONG gameId, int dynObjectId, int playerId, string playerName, NETID playerNetId, DDRPlayers *&players, bool &newPlayer) {
	newPlayer = false;

	DDRPlayerObj *l_player = NULL;

	DynObjPlayersMap::iterator i = m_dynObjectPlayersMap.find(dynObjectId);

	if (i != m_dynObjectPlayersMap.end()) {
		// Found the floor, get the DDRPlayers obj
		players = i->second;
		auto k = players->m_playersMap.find(playerId);

		if (k != players->m_playersMap.end()) {
			// Found the player
			l_player = k->second;
			l_player->SetNetID(playerNetId); // maybe rejoined with different id
		} else {
			l_player = new DDRPlayerObj();
			l_player->SetStatus(DDRPlayerObj::ACTIVE);
			l_player->SetName(playerName);
			l_player->SetLastUpdateTime();
			l_player->SetGameId(gameId);
			l_player->SetNetID(playerNetId);
			l_player->SetPlayerId(playerId);
			l_player->SetHighScore(0);
			l_player->SetScore(0);
			l_player->SetNumPerfects(0);
			l_player->SetNumPerfectsInARow(0);

			players->m_playersMap.insert(DDRPlayers::PlayerMap::value_type(playerId, l_player));
			players->IncrementActiveCount();

			LOG4CPLUS_DEBUG(m_logger, "Player id:" << playerId << " added to players."
												   << "Active count now:" << players->GetActiveCount());

			newPlayer = true;
		}
	} else {
		players = new DDRPlayers();

		players->SetActiveCount(0);
		players->SetHighScore(0);
		players->SetHighScoreDirty(false);
		players->SetHighScorePlayerId(0);
		players->SetDynamicObjectObjectId(1);
		players->SetDynamicObjectZoneIndex(1);
		players->SetIsPermanentZone(false);
		players->SetLastHighScoreResetDate(0);

		l_player = new DDRPlayerObj();
		l_player->SetStatus(DDRPlayerObj::ACTIVE);
		l_player->SetName(playerName);
		l_player->SetLastUpdateTime();
		l_player->SetGameId(gameId);
		l_player->SetNetID(playerNetId);
		l_player->SetPlayerId(playerId);
		l_player->SetHighScore(0);
		l_player->SetScore(0);
		l_player->SetNumPerfects(0);
		l_player->SetNumPerfectsInARow(0);

		players->m_playersMap.insert(DDRPlayers::PlayerMap::value_type(playerId, l_player));
		players->IncrementActiveCount();

		LOG4CPLUS_DEBUG(m_logger, "Player id:" << playerId << " added to newly constructed players object."
											   << "Active count now:" << players->GetActiveCount());

		m_dynObjectPlayersMap.insert(DynObjPlayersMap::value_type(dynObjectId, players));

		newPlayer = true;
	}

	return l_player;
}

/*
 *Function Name:PostPlayersScores
 *
 *Description:Return the score list to the client via event
 *
 *Returns:	none
 *
 */
void DDR_S::PostPlayersScores(NETID to, IPlayer *player, ULONG gameId, int dynObjectId, ULONG levelId, ULONG highestScore, string highestScoreName) {
	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);

	DynObjPlayersMap::iterator i = m_dynObjectPlayersMap.find(dynObjectId);

	if (i == m_dynObjectPlayersMap.end()) {
		return;
	}

	DDRPlayers *players = i->second;

	DDREvent *ddr_scores = new DDREvent();
	ddr_scores->PostScores(playerId, gameId, levelId, highestScore, highestScoreName.c_str(), players);
	ddr_scores->AddTo(to);
	GetDispatcher()->QueueEvent(ddr_scores);

	PostScoresToInactivePlayers(players, gameId);
}

/*
 *Function Name:PostScoresToInactivePlayers
 *
 *Description:Return the score list to inactive players
 *
 *Returns:	none
 *
 */
void DDR_S::PostScoresToInactivePlayers(DDRPlayers *players, ULONG gameId) {
	time_t current;

	time(&current);

	double elapsed_time = difftime(current, GetNotifyInactivePlayersTime());

	if (elapsed_time > m_inactivePlayerNotificationSeconds) {
		LOG4CPLUS_DEBUG(m_logger, "Inactive players - Sending scores to inactive players");

		// Periodically send the scores to the inactive players
		for (const auto &player : players->m_playersMap) {
			int playerId = player.first; // playerId
			DDRPlayerObj *l_player = player.second;

			LOG4CPLUS_DEBUG(m_logger, "Inactive players - Found player " << l_player->GetName() << " Status is:" << l_player->GetStatus());

			if (((l_player->GetStatus() == DDRPlayerObj::INACTIVE) ||
					(l_player->GetStatus() == DDRPlayerObj::PAUSED)) &&
				(l_player->GetNetID() != 0)) {
				DDREvent *ddr_scores = new DDREvent();
				ddr_scores->PostScores(playerId, gameId, 0, players->GetHighScore(), players->GetHighScoreCharacterName().c_str(), players);
				ddr_scores->AddTo(l_player->GetNetID());
				GetDispatcher()->QueueEvent(ddr_scores);
				LOG4CPLUS_DEBUG(m_logger, "Inactive players - Send scores to player " << l_player->GetName());
			}
		}

		time(&m_notifyInactivePlayers);
	}
}

/*
 *Function Name:PlayerExit
 *
 *Description:Player has left the game. Mark as inactive.
 *
 *Returns:	none
 *
 */
void DDR_S::PlayerExit(IPlayer *player, ULONG /*gameId*/, int dynObjectId, int leftFloor) {
	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);

	DynObjPlayersMap::iterator ii = m_dynObjectPlayersMap.find(dynObjectId);

	if (ii != m_dynObjectPlayersMap.end()) {
		// Found the floor, get the DDRPlayers obj
		DDRPlayers *players = ii->second;

		auto kk = players->m_playersMap.find(playerId);

		if (kk != players->m_playersMap.end()) {
			int playerState = player->Values()->GetINT(PLAYERIDS_INTERACTIONSTATES);
			playerState &= ~DANCE_PARTY;
			player->Values()->SetNumber(PLAYERIDS_INTERACTIONSTATES, playerState);

			if (leftFloor == 1) {
				//int playerId = kk->first; // playerId
				DDRPlayerObj *l_player = kk->second;

				// Set this player's NETID to 0. They have left the floor. Don't send the other
				// player's scores to this inactive player because,
				// 1) they have lost the menu showing scores and they're off to something else
				// 2) they may have joined another dance floor in another location and they shouldn't
				// receive scores from this old dance floor if they haven't been purged yet.
				l_player->SetNetID(0);
			} else {
				//int playerId = kk->first; // playerId
				DDRPlayerObj *l_player = kk->second;

				LOG4CPLUS_DEBUG(m_logger, "Player exit for player id:" << playerId << " current status is:" << l_player->GetStatus());

				if ((l_player->GetStatus() == DDRPlayerObj::ACTIVE) ||
					(l_player->GetStatus() == DDRPlayerObj::PAUSED)) {
					l_player->SetStatus(DDRPlayerObj::INACTIVE);
					players->DecrementActiveCount();

					DDRDBEvent *dbe = new DDRDBEvent();
					dbe->DBRecordScore(players->IsPermanentZone(), players->GetDynamicObjectObjectId(),
						players->GetDynamicObjectZoneIndex(), dynObjectId,
						l_player->GetGameId(), l_player->GetPlayerId(),
						l_player->GetName().c_str(),
						l_player->GetHighScore(), l_player->GetScore(),
						l_player->GetNumPerfects(), l_player->GetNumPerfectsInARow());
					m_db->GetDispatcher()->QueueEvent(dbe);
				}

				LOG4CPLUS_DEBUG(m_logger, "Player id:" << playerId << " exiting."
													   << "Active count now:" << players->GetActiveCount());
			}
		}
	}
}

/*
 *Function Name:PlayerPaused
 *
 *Description:Player has paused or un-paused the game. Mark accordingly.
 *
 *Returns:	none
 *
 */
void DDR_S::PlayerPaused(IPlayer *player, int dynObjectId, int isPaused) {
	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);

	DynObjPlayersMap::iterator ii = m_dynObjectPlayersMap.find(dynObjectId);

	if (ii != m_dynObjectPlayersMap.end()) {
		// Found the floor, get the DDRPlayers obj
		DDRPlayers *players = ii->second;

		auto kk = players->m_playersMap.find(playerId);

		if (kk != players->m_playersMap.end()) {
			//int playerId = kk->first; // playerId
			DDRPlayerObj *l_player = kk->second;

			if (isPaused == 1 && (l_player->GetStatus() == DDRPlayerObj::ACTIVE)) {
				l_player->SetStatus(DDRPlayerObj::PAUSED);
				l_player->SetLastUpdateTime();

				DDRDBEvent *dbe = new DDRDBEvent();
				dbe->DBRecordScore(players->IsPermanentZone(), players->GetDynamicObjectObjectId(),
					players->GetDynamicObjectZoneIndex(), dynObjectId,
					l_player->GetGameId(), l_player->GetPlayerId(),
					l_player->GetName().c_str(),
					l_player->GetHighScore(), l_player->GetScore(),
					l_player->GetNumPerfects(), l_player->GetNumPerfectsInARow());
				m_db->GetDispatcher()->QueueEvent(dbe);

				LOG4CPLUS_DEBUG(m_logger, "Player paused for player id:" << playerId << ". Status now PAUSED");
			} else if (isPaused == 0 && (l_player->GetStatus() == DDRPlayerObj::PAUSED)) {
				l_player->SetStatus(DDRPlayerObj::ACTIVE);
				l_player->SetLastUpdateTime();
				LOG4CPLUS_DEBUG(m_logger, "Player un-paused for player id:" << playerId << ". Status now ACTIVE");
			}
		}
	}
}

/*
 *Function Name:PurgeStalePlayers
 *
 *Description:Purge in memory players who haven't updated elapsed_player_time in m_purgeTimeoutSeconds
 *
 *Returns:	none
 *
 */
void DDR_S::PurgeStalePlayers() {
	DataModel::ptr_t model = PluginHost::singleton().getInterfaceNoThrow<DataModel>("AppDataModel");
	DDRModel *ddrModel = (model.operator->() == 0 ? (DDRModel *)0 : model->registry().getFacetNoThrow<DDRModel>("DDRModel"));

	time_t current;

	time(&current);

	double elapsed_time = difftime(current, GetPlayerListLastPurgeTime());

	if (elapsed_time > m_purgeTimeoutSeconds) {
		LOG4CPLUS_DEBUG(m_logger, loadStr(IDS_PURGE_ATTEMPT));

		for (DynObjPlayersMap::iterator ii = m_dynObjectPlayersMap.begin(); ii != m_dynObjectPlayersMap.end();) {
			int dynObjectId = ii->first;
			DDRPlayers *players = ii->second; // playerMap

			for (auto jj = players->m_playersMap.begin(); jj != players->m_playersMap.end();) {
				//int playerId = jj->first; // playerId
				DDRPlayerObj *l_player = jj->second;

				string pName = l_player->GetName();

				double elapsed_player_time = difftime(current, l_player->GetLastUpdateTime());

				if (elapsed_player_time > m_purgeTimeoutSeconds) // purge timeout should be greater than pause timeout
				// used by client.
				{
					// record player's high score

					if ((l_player->GetStatus() == DDRPlayerObj::ACTIVE) ||
						(l_player->GetStatus() == DDRPlayerObj::PAUSED)) {
						DDRDBEvent *dbe = new DDRDBEvent();
						dbe->DBRecordScore(players->IsPermanentZone(), players->GetDynamicObjectObjectId(),
							players->GetDynamicObjectZoneIndex(), dynObjectId,
							l_player->GetGameId(), l_player->GetPlayerId(),
							l_player->GetName().c_str(),
							l_player->GetHighScore(), l_player->GetScore(),
							l_player->GetNumPerfects(), l_player->GetNumPerfectsInARow());

						m_db->GetDispatcher()->QueueEvent(dbe);

						players->DecrementActiveCount();
					}

					delete l_player;
					jj = players->m_playersMap.erase(jj);

					LOG4CPLUS_DEBUG(m_logger, loadStrPrintf(IDS_PRUNE_PLAYER, pName.c_str(), players->GetActiveCount()).c_str());
				} else {
					jj++;
				}
			}

			/*int count =*/players->m_playersMap.size();

			if (players->m_playersMap.empty()) {
				// save all time high score
				if (players->GetHighScoreDirty() && players->GetHighScorePlayerId() != 0) {
					if (ddrModel) {
						SuppressedDDRModel(*ddrModel).recordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(), players->GetHighScore(), players->GetHighScoreCharacterName(), players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex());
					} else {
						m_db->RecordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(),
							players->GetHighScore(),
							players->GetHighScoreCharacterName(),
							players->IsPermanentZone(),
							players->GetDynamicObjectObjectId(),
							players->GetDynamicObjectZoneIndex());
					}
				}

				delete players;
				ii = m_dynObjectPlayersMap.erase(ii);
			} else if (players->GetHighScoreDirty()) {
				if (ddrModel) {
					SuppressedDDRModel(*ddrModel).recordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(), players->GetHighScore(), players->GetHighScoreCharacterName(), players->IsPermanentZone(), players->GetDynamicObjectObjectId(), players->GetDynamicObjectZoneIndex());
				} else {
					m_db->RecordDDRHighScore(dynObjectId, players->GetHighScorePlayerId(),
						players->GetHighScore(),
						players->GetHighScoreCharacterName(),
						players->IsPermanentZone(),
						players->GetDynamicObjectObjectId(),
						players->GetDynamicObjectZoneIndex());
				}
				players->SetHighScoreDirty(false);

				// Pick up high score inserted by another server
				if (ddrModel) {
					SuppressedDDRModel(*ddrModel).refreshDDRHighScore(dynObjectId, players);
				} else {
					m_db->RefreshDDRHighScore(dynObjectId, players);
				}

				ii++;
			} else {
				if (ddrModel) {
					SuppressedDDRModel(*ddrModel).refreshDDRHighScore(dynObjectId, players);
				} else {
					// Pick up high score inserted by another server
					m_db->RefreshDDRHighScore(dynObjectId, players);
				}

				ii++;
			}
		}

		time(&m_lastPurgePlayersMap);
	}
}

/*
 *Function Name:DynamicObjectAccessAllowed
 *
 *Description:Limit the number of players that can be on an instance of the dance floor.
 *
 *Returns:	true, access allowed
 *			false, access not allowed
 *
 */
bool DDR_S::DynamicObjectAccessAllowed(IPlayer * /*player*/, ULONG gameId, int dynObjectId) {
	DynObjPlayersMap::iterator ii = m_dynObjectPlayersMap.find(dynObjectId);

	GameLevelsMap::iterator jj = m_gameLevelsMap.find(gameId);

	if (ii != m_dynObjectPlayersMap.end() && jj != m_gameLevelsMap.end()) {
		DDRPlayers *players = ii->second;
		DDRLevels *levels = jj->second;

		if (!players->IsPermanentZone()) // Let zone limit control permanent zones
		{
			int count = players->GetActiveCount();
			count++;

			if (count > (int)levels->GetDanceFloorLimit()) {
				LOG4CPLUS_INFO(m_logger, "Access denied. Active count is: " << count << " Dance floor limit is:" << levels->GetDanceFloorLimit());

				return false;
			}
		}
	}

	return true;
}

/*
 *Function Name:CalcScore
 *
 *Description:Calculate the player's score
 *
 *Returns:	none
 *
 */
void DDR_S::CalcScore(DDRPlayerObj *player,
	ULONG gameId,
	ULONG levelId,
	ULONG numMilliSecondsExpired,
	bool levelComplete,
	bool gameOver,
	bool perfectBonus) {
	GameLevelsMap::iterator ii = m_gameLevelsMap.find(gameId);

	if (ii != m_gameLevelsMap.end()) {
		DDRLevels *levels = ii->second;

		LevelMap::iterator kk = levels->m_levelsMap.find(levelId);

		if (kk != levels->m_levelsMap.end()) {
			DDRLevelObj *l_level = kk->second;

			ULONG perfectScore = l_level->GetPerfectScore();
			ULONG greatScore = l_level->GetGreatScore();
			ULONG goodScore = l_level->GetGoodScore();
			ULONG finishBonusScore = l_level->GetFinishBonusScore();
			ULONG levelIncreaseScore = l_level->GetLevelIncreaseScore();
			/*ULONG levelBonusTriggerNum =*/l_level->GetLevelBonusTriggerNum();
			ULONG levelBonusScore = l_level->GetLevelBonusScore();
			ULONG perfectScoreCeiling = l_level->GetPerfectScoreCeiling();
			ULONG greatScoreCeiling = l_level->GetGreatScoreCeiling();
			ULONG goodScoreCeiling = l_level->GetGoodScoreCeiling();

			perfectScoreCeiling = perfectScoreCeiling * 1000;
			greatScoreCeiling = greatScoreCeiling * 1000;
			goodScoreCeiling = goodScoreCeiling * 1000;

			ULONG existingScore = player->GetScore();
			ULONG calcScore = 0;

			if (gameOver) {
				calcScore = existingScore + finishBonusScore;
				player->SetScore(calcScore);

				if (calcScore > player->GetHighScore()) {
					player->SetHighScore(calcScore);
				}
			} else if (numMilliSecondsExpired <= goodScoreCeiling) {
				if (numMilliSecondsExpired <= perfectScoreCeiling) {
					// Now on client side
					player->SetNumPerfectsInARow(0);

					if (perfectBonus) {
						player->SetNumPerfects(player->GetNumPerfects() + 1);
					}
					// player->SetNumPerfectsInARow(player->GetNumPerfectsInARow() + 1);

					if (levelComplete) {
						if (perfectBonus)
						// Now on client side
						// if ( player->GetNumPerfectsInARow() == levelBonusTriggerNum )
						{
							calcScore = existingScore + perfectScore + levelBonusScore + levelIncreaseScore;
							//player->SetNumPerfectsInARow(0);
						} else {
							calcScore = existingScore + perfectScore + levelIncreaseScore;
						}
					} else if (perfectBonus)
					// Now on client side
					//else if ( player->GetNumPerfectsInARow() == levelBonusTriggerNum)
					{
						calcScore = existingScore + perfectScore + levelBonusScore;
						//player->SetNumPerfectsInARow(0);
					} else {
						calcScore = existingScore + perfectScore;
					}
				} else if (numMilliSecondsExpired <= greatScoreCeiling) {
					player->SetNumPerfectsInARow(0);

					if (levelComplete) {
						calcScore = existingScore + greatScore + levelIncreaseScore;
					} else {
						calcScore = existingScore + greatScore;
					}
				} else if (numMilliSecondsExpired <= goodScoreCeiling) {
					player->SetNumPerfectsInARow(0);

					if (levelComplete) {
						calcScore = existingScore + goodScore + levelIncreaseScore;
					} else {
						calcScore = existingScore + goodScore;
					}
				}

				player->SetScore(calcScore);

				if (calcScore > player->GetHighScore()) {
					player->SetHighScore(calcScore);
				}
			}
		}
	}
}

/*
 *Function Name:LoadPlayerData
 *
 *Description:Load all player data into memory. Synchronous call.
 *
 *Returns:	true, ok
 *			false, error
 *
 */
bool DDR_S::LoadPlayerData(ULONG gameId, ULONG gameLevel, NETID from, bool newGame, int dynObjectId, int playerId) {
	bool ret = false;

	DynObjPlayersMap::iterator jj = m_dynObjectPlayersMap.find(dynObjectId);
	DDRPlayers *players = 0;

	if (jj != m_dynObjectPlayersMap.end()) {
		players = jj->second;
		ret = true;
		DDRDBEvent *e = new DDRDBEvent();
		e->LoadPlayerData(gameId, gameLevel, from, newGame,
			dynObjectId, playerId, players->GetDynamicObjectZoneIndex(),
			players->GetDynamicObjectObjectId());
		m_db->GetDispatcher()->QueueEvent(e);
	} else {
		LOG4CPLUS_ERROR(m_logger, "Could not find dynamic object: " << dynObjectId << " in DynObjPlayersMap. LoadDDRPlayerData failed.");
		return ret;
	}

	return ret;
}

/*
 *Function Name:LoadGameData
 *
 *Description:Load all game meta data into memory. Synchronous call.
 *
 *Returns:	true, ok
 *			false, error
 *
 */
bool DDR_S::LoadGameData(ULONG gameId) {
	bool ret = false;
	// Do we have the data for this game yet
	GameLevelsMap::iterator ii = m_gameLevelsMap.find(gameId);

	if (ii == m_gameLevelsMap.end()) {
		LOG4CPLUS_DEBUG(m_logger, loadStrPrintf(IDS_LOAD_GAME_DATA_TRY, gameId).c_str());

		DataModel::ptr_t model = PluginHost::singleton().getInterfaceNoThrow<DataModel>("AppDataModel");
		DDRModel *ddrModel(model.operator->() == 0 ? (DDRModel *)0 : model->registry().getFacetNoThrow<DDRModel>("DDRModel"));
		if (ddrModel)
			ret = SuppressedDDRModel(*ddrModel).loadDDRGameData(gameId, m_gameLevelsMap);
		else
			ret = m_db->LoadDDRGameData(gameId, m_gameLevelsMap);

		if (ret) {
			GameLevelsMap::iterator jj = m_gameLevelsMap.find(gameId);

			if (jj != m_gameLevelsMap.end()) {
				DDRLevels *levels = jj->second;

				// Not level specific at this time.
				m_inactivePlayerNotificationSeconds = levels->GetNotifyInactivePlayerTimeout();
				m_purgeTimeoutSeconds = levels->GetPurgeTimeout();

				LOG4CPLUS_DEBUG(m_logger, loadStrPrintf(IDS_LOAD_GAME_DATA_SUCCESS, gameId, m_purgeTimeoutSeconds, m_inactivePlayerNotificationSeconds).c_str());
			}
		} else {
			LOG4CPLUS_ERROR(m_logger, loadStrPrintf(IDS_LOAD_GAME_DATA_ERROR, gameId).c_str());
		}
	} else {
		ret = true;
	}

	return ret;
}

} // namespace KEP

extern "C" BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD ul_reason_for_call,
	LPVOID /*lpReserved*/
) {
	if (ul_reason_for_call == DLL_PROCESS_ATTACH) {
		StringResource = (HINSTANCE)hModule;
	}
	return TRUE;
}
