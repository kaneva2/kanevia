///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <utility>
#include <string>
#include <vector>
#include <map>

namespace KEP {

class DDRLevelObj {
public:
	DDRLevelObj() {}

	ULONG GetPerfectScore() { return m_perfectScore; }
	ULONG GetGreatScore() { return m_greatScore; }
	ULONG GetGoodScore() { return m_goodScore; }
	ULONG GetFinishBonusScore() { return m_finishBonusScore; }
	ULONG GetLevelIncreaseScore() { return m_levelIncreaseScore; }
	ULONG GetLevelBonusTriggerNum() { return m_levelBonusTriggerNum; }
	ULONG GetLevelBonusScore() { return m_levelBonusScore; }
	ULONG GetPerfectScoreCeiling() { return m_perfectScoreCeiling; }
	ULONG GetGreatScoreCeiling() { return m_greatScoreCeiling; }
	ULONG GetGoodScoreCeiling() { return m_goodScoreCeiling; }

	ULONG GetSeed() { return m_seed; }
	ULONG GetNumSecondsToPlay() { return m_numSecondsToPlay; }
	ULONG GetNumMoves() { return m_numMoves; }
	ULONG GetNumMisses() { return m_numMisses; }

	ULONG GetAnimationFlareCount() { return m_animationFlareCount; }
	ULONG GetAnimationFinalFlareCount() { return m_animationFinalFlareCount; }
	ULONG GetAnimationMissCount() { return m_animationMissCount; }
	ULONG GetAnimationLevelUpCount() { return m_animationLevelUpCount; }
	ULONG GetAnimationPerfectCount() { return m_animationPerfectCount; }
	ULONG GetAnimationIdleCount() { return m_animationIdleCount; }

	ULONG GetNumSubLevels() { return m_numSubLevels; }

	ULONG GetLastLevelReductionMilliSeconds() { return m_lastLevelReductionMilliSeconds; }

	ULONG GetSkillId() { return m_skillId; }
	ULONG GetActionId() { return m_actionId; }
	ULONG GetPerfectActionId() { return m_perfectActionId; }
	ULONG GetGainAmount() { return m_gainAmount; }
	ULONG GetPerfectGainAmount() { return m_perfectGainAmount; }

	void SetPerfectScore(ULONG perfectScore) { m_perfectScore = perfectScore; }
	void SetGreatScore(ULONG greatScore) { m_greatScore = greatScore; }
	void SetGoodScore(ULONG goodScore) { m_goodScore = goodScore; }
	void SetFinishBonusScore(ULONG finishBonusScore) { m_finishBonusScore = finishBonusScore; }
	void SetLevelIncreaseScore(ULONG levelIncreaseScore) { m_levelIncreaseScore = levelIncreaseScore; }
	void SetLevelBonusTriggerNum(ULONG levelBonusTriggerNum) { m_levelBonusTriggerNum = levelBonusTriggerNum; }
	void SetLevelBonusScore(ULONG levelBonusScore) { m_levelBonusScore = levelBonusScore; }
	void SetPerfectScoreCeiling(ULONG perfectScoreCeiling) { m_perfectScoreCeiling = perfectScoreCeiling; }
	void SetGreatScoreCeiling(ULONG greatScoreCeiling) { m_greatScoreCeiling = greatScoreCeiling; }
	void SetGoodScoreCeiling(ULONG goodScoreCeiling) { m_goodScoreCeiling = goodScoreCeiling; }

	void SetSeed(ULONG seed) { m_seed = seed; }
	void SetNumSecondsToPlay(ULONG numSecondsToPlay) { m_numSecondsToPlay = numSecondsToPlay; }
	void SetNumMoves(ULONG numMoves) { m_numMoves = numMoves; }
	void SetNumMisses(ULONG numMisses) { m_numMisses = numMisses; }

	void SetAnimationFlareCount(ULONG animationFlareCount) { m_animationFlareCount = animationFlareCount; }
	void SetAnimationFinalFlareCount(ULONG animationFinalFlareCount) { m_animationFinalFlareCount = animationFinalFlareCount; }
	void SetAnimationMissCount(ULONG animationMissCount) { m_animationMissCount = animationMissCount; }
	void SetAnimationLevelUpCount(ULONG animationLevelUpCount) { m_animationLevelUpCount = animationLevelUpCount; }
	void SetAnimationPerfectCount(ULONG animationPerfectCount) { m_animationPerfectCount = animationPerfectCount; }
	void SetAnimationIdleCount(ULONG animationIdleCount) { m_animationIdleCount = animationIdleCount; }

	void SetNumSubLevels(ULONG numSubLevels) { m_numSubLevels = numSubLevels; }

	void SetLastLevelReductionMilliSeconds(ULONG lastLevelReductionMilliSeconds) { m_lastLevelReductionMilliSeconds = lastLevelReductionMilliSeconds; }

	void SetSkillId(ULONG skillId) { m_skillId = skillId; }
	void SetActionId(ULONG actionId) { m_actionId = actionId; }
	void SetGainAmount(ULONG gainAmount) { m_gainAmount = gainAmount; }
	void SetPerfectActionId(ULONG perfectActionId) { m_perfectActionId = perfectActionId; }
	void SetPerfectGainAmount(ULONG perfectGainAmount) { m_perfectGainAmount = perfectGainAmount; }

	std::vector<std::pair<ULONG, int>> m_AnimationFlares;

	std::vector<std::pair<ULONG, int>> m_AnimationFinalFlares;
	std::vector<std::pair<ULONG, int>> m_AnimationMisses;
	std::vector<std::pair<ULONG, int>> m_AnimationLevelUps;
	std::vector<std::pair<ULONG, int>> m_AnimationPerfects;
	std::vector<std::pair<ULONG, int>> m_AnimationIdles;

private:
	ULONG m_perfectScore;
	ULONG m_greatScore;
	ULONG m_goodScore;
	ULONG m_finishBonusScore;
	ULONG m_levelIncreaseScore;
	ULONG m_levelBonusTriggerNum;
	ULONG m_levelBonusScore;
	ULONG m_perfectScoreCeiling;
	ULONG m_greatScoreCeiling;
	ULONG m_goodScoreCeiling;

	ULONG m_seed;
	ULONG m_numSecondsToPlay;
	ULONG m_numMoves;
	ULONG m_numMisses;

	ULONG m_animationFlareCount;
	ULONG m_animationFinalFlareCount;
	ULONG m_animationMissCount;
	ULONG m_animationLevelUpCount;
	ULONG m_animationPerfectCount;
	ULONG m_animationIdleCount;

	ULONG m_numSubLevels;

	ULONG m_lastLevelReductionMilliSeconds;

	ULONG m_skillId;
	ULONG m_actionId;
	ULONG m_gainAmount;
	ULONG m_perfectActionId;
	ULONG m_perfectGainAmount;
};

typedef std::map<int, DDRLevelObj *> LevelMap;

class DDRLevels {
public:
	DDRLevels() {}

	LevelMap m_levelsMap;

	void SetLevelCount(int count) { m_count = count; }
	int GetLevelCount() { return m_count; }

	void SetPurgeTimeout(ULONG timeout) { m_purgeTimeoutSeconds = timeout; }
	ULONG GetPurgeTimeout() { return m_purgeTimeoutSeconds; }

	void SetNotifyInactivePlayerTimeout(ULONG timeout) { m_notifyInactivePlayerTimeoutSeconds = timeout; }
	ULONG GetNotifyInactivePlayerTimeout() { return m_notifyInactivePlayerTimeoutSeconds; }

	void SetDanceFloorLimit(ULONG limit) { m_danceFloorLimit = limit; }
	ULONG GetDanceFloorLimit() { return m_danceFloorLimit; }

private:
	ULONG m_purgeTimeoutSeconds;
	ULONG m_notifyInactivePlayerTimeoutSeconds;
	ULONG m_danceFloorLimit;

	int m_count;
};

typedef std::map<int, DDRLevels *> GameLevelsMap;

} // namespace KEP
