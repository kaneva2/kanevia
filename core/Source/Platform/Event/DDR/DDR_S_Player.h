///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <time.h>
#include <string>
#include <map>

namespace KEP {

class DDRPlayerObj {
public:
	DDRPlayerObj() {}

	enum player_status { ACTIVE = 0,
		INACTIVE = 1,
		PAUSED = 2 };

	ULONG GetGameId() { return m_gameId; }
	ULONG GetScore() { return m_score; }
	player_status GetStatus() { return m_status; }
	std::string GetName() { return m_characterName; }
	time_t GetLastUpdateTime() { return lastTimestamp; }
	ULONG GetNumPerfects() { return m_numPerfects; }
	ULONG GetHighScore() { return m_highScore; }
	ULONG GetNumPerfectsInARow() { return m_numPerfectsInARow; }
	int GetPlayerId() { return m_playerId; }
	ULONG GetFameGainAmount() { return m_fameGainAmount; }
	std::string GetLevelMessage() { return m_levelMessage; }
	std::string GetGainMessage() { return m_gainMessage; }
	int GetPercentToNextLevel() { return m_percentToNextLevel; }
	std::string GetRewardsAtNextLevel() { return m_rewardsAtNextLevel; }

	void SetGameId(ULONG gameId) { m_gameId = gameId; }
	void SetScore(ULONG score) { m_score = score; }
	void SetStatus(player_status status) { m_status = status; }
	void SetName(std::string name) { m_characterName = name; }
	void SetLastUpdateTime() { time(&lastTimestamp); }
	void SetNumPerfects(ULONG numPerfects) { m_numPerfects = numPerfects; }
	void SetHighScore(ULONG highScore) { m_highScore = highScore; }
	void SetNumPerfectsInARow(ULONG numPerfectsInARow) { m_numPerfectsInARow = numPerfectsInARow; }
	void SetPlayerId(int playerId) { m_playerId = playerId; }
	void SetFameGainAmount(ULONG fameGainAmount) { m_fameGainAmount = fameGainAmount; }
	void SetLevelMessage(std::string levelMessage) { m_levelMessage = levelMessage; }
	void SetGainMessage(std::string gainMessage) { m_gainMessage = gainMessage; }
	void SetPercentToNextLevel(int percentToNext) { m_percentToNextLevel = percentToNext; }
	void SetRewardsAtNextLevel(std::string rewardsAtNext) { m_rewardsAtNextLevel = rewardsAtNext; }

	void SetNetID(NETID id) { m_netID = id; }
	NETID GetNetID() { return m_netID; }

private:
	ULONG m_gameId;
	std::string m_characterName;
	player_status m_status;
	ULONG m_score;
	ULONG m_numPerfects;
	ULONG m_numPerfectsInARow;
	ULONG m_highScore;
	time_t lastTimestamp;
	NETID m_netID;
	int m_playerId;
	ULONG m_fameGainAmount;
	BOOL m_fameLevelUp;
	std::string m_levelMessage;
	std::string m_gainMessage;
	int m_percentToNextLevel;
	std::string m_rewardsAtNextLevel;
};

class DDRPlayers {
public:
	// unique player id and player objects
	typedef std::map<int, DDRPlayerObj *> PlayerMap;

	DDRPlayers() {
		m_activeCount = 0;
		m_highScore = 0;
		m_highScorePlayerId = 0;
	}

	void SetActiveCount(int activeCount) { m_activeCount = activeCount; }
	int GetActiveCount() { return m_activeCount; }
	void IncrementActiveCount() { m_activeCount += 1; }
	void DecrementActiveCount() { m_activeCount -= 1; }

	ULONG GetHighScore() { return m_highScore; }
	void SetHighScore(ULONG highScore) { m_highScore = highScore; }

	bool GetHighScoreDirty() { return m_highScoreDirty; }
	void SetHighScoreDirty(bool dirty) { m_highScoreDirty = dirty; }

	std::string GetHighScoreCharacterName() { return m_highScoreCharacterName; }
	void SetHighScoreCharacterName(std::string name) { m_highScoreCharacterName = name; }

	int GetHighScorePlayerId() { return m_highScorePlayerId; }
	void SetHighScorePlayerId(int playerId) { m_highScorePlayerId = playerId; }

	bool IsPermanentZone() { return m_dynamicObjectInPermanentZone; }
	void SetIsPermanentZone(bool inPermanent) { m_dynamicObjectInPermanentZone = inPermanent; }

	ULONG GetDynamicObjectObjectId() { return m_dynamicObjectObjectId; }
	void SetDynamicObjectObjectId(ULONG objectId) { m_dynamicObjectObjectId = objectId; }

	ULONG GetDynamicObjectZoneIndex() { return m_dynamicObjectZoneIndex; }
	void SetDynamicObjectZoneIndex(ULONG zoneIndex) { m_dynamicObjectZoneIndex = zoneIndex; }

	time_t GetLastHighScoreResetDate() { return m_lastHighScoreResetDate; }
	void SetLastHighScoreResetDate(time_t lastHighScoreResetDate) { m_lastHighScoreResetDate = lastHighScoreResetDate; }

	PlayerMap m_playersMap;

private:
	int m_activeCount;
	ULONG m_highScore;
	std::string m_highScoreCharacterName;
	int m_highScorePlayerId;
	bool m_highScoreDirty;

	// Each dynamic object has its own set of players and high scores.
	// When instancing a dynamic object in a permanent zone, e.g. creating another instance of the dance
	// floor, keep a single high score for both dance floors. This is accomplished by
	// recording the high score using the zoneIndex and objectId(gameId). Normally, the dynamic object id
	// is used which is unique for each instance.
	bool m_dynamicObjectInPermanentZone; // is the dynamic object in a permanent zone?
	ULONG m_dynamicObjectObjectId; // the game id
	ULONG m_dynamicObjectZoneIndex; // the zone index of this dynamic object

	time_t m_lastHighScoreResetDate;
};

// unique zone, a DDRPlayers object
typedef std::map<int, DDRPlayers *> DynObjPlayersMap;

} // namespace KEP
