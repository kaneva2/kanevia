///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "common/include/kepcommon.h"
#include "Event\Base\IEvent.h"
#include "Event\Base\IEventHandler.h"
#include "Event\Base\IEventBlade.h"
#include "Tools\NetworkV2\INetwork.h"
#include "DDRGameDB.h"
#include "DDR_S_Player.h"
#include "DDR_S_GameData.h"
#include "IPlayer.h"
#include "KEPException.h"

namespace KEP {
// Event Handling class
class DDR_S : public IEventBlade {
public:
	// set the name of this handler to the RTTI name ("class KEP::DDR_S", in this case)
	DDR_S();

	~DDR_S();

	bool Init(IDispatcher *dispatcher);
	virtual void SetDBConfig(const DBConfig *dbConfig) { m_dbConfig = dbConfig; }

	// set the blade interface version
	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }

	// IBlade overrides
	virtual ULONG Version() const { return PackVersion(1, 0, 0, 0); }

	// BaseDir is scriptPath For Event Blades
	virtual std::string PathBase() const { return m_scriptDir; }
	virtual void SetPathBase(const std::string &baseDir, bool readonly) {
		m_scriptDir = baseDir;
		m_scriptDirReadOnly = readonly;
	}

	virtual void Delete() { delete this; }
	virtual void FreeAll();
	virtual const char *Name() const { return m_name.c_str(); }

	// IEventBlade overrides
	bool RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);

	static EVENT_PROC_RC DDRHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC DDRStartHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC DDRRecordScoreHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC DDRPlayerExitHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC DDRPlayerPausedHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC DDRDBReplyHandler(ULONG lparam, IDispatcher *disp, IEvent *e);

	virtual IDispatcher *GetDispatcher() { return m_dispatcher; }
	virtual void SetDispatcher(IDispatcher *dispatcher) { m_dispatcher = dispatcher; }

	Logger m_logger;

	virtual bool LoadGameData(ULONG gameId);
	virtual bool LoadPlayerData(ULONG gameId, ULONG gameLevel, NETID from, bool newGame, int dynObjectId, int playerId);
	virtual void CalcScore(DDRPlayerObj *player, ULONG gameId, ULONG levelId, ULONG numMilliSecondsExpired, bool levelComplete, bool gameOver, bool perfectBonus);

	virtual void TrackPlayerScore(IPlayer *player, ULONG gameId, int dynObjectId, ULONG levelId, ULONG numMilliSecondsExpired,
		bool newGame, bool levelComplete, bool gameOver, bool perfectBonus, ULONG fameGainAmount,
		int newFameLevel, std::string levelMessage, std::string gainMessage, int percentToNextLevel, std::string rewardsAtNextLevel,
		ULONG &score, ULONG &highScore, ULONG &allTimeHighScore, std::string &allTimeHighScoreCharacterName);

	virtual void PostPlayersScores(NETID to, IPlayer *player, ULONG gameId, int dynObjectId, ULONG levelId, ULONG highestScore, std::string highestScoreName);
	virtual void PlayerExit(IPlayer *player, ULONG gameId, int dynObjectId, int leftFloor);
	virtual void PlayerPaused(IPlayer *player, int dynObjectId, int isPaused);

	virtual bool DynamicObjectAccessAllowed(IPlayer *player, ULONG gameId, int dynObjectId);
	virtual void PurgeStalePlayers();
	virtual time_t GetPlayerListLastPurgeTime() { return m_lastPurgePlayersMap; }

	virtual void PostScoresToInactivePlayers(DDRPlayers *players, ULONG gameId);
	virtual time_t GetNotifyInactivePlayersTime() { return m_notifyInactivePlayers; }

	virtual DDRPlayerObj *GetPlayer(ULONG gameId, int dynObjectId, int playerId, std::string playerName, NETID playerNetId, DDRPlayers *&players, bool &newPlayer);

protected:
	virtual bool IsBaseDirReadOnly() const { return m_scriptDirReadOnly; }

	std::wstring GetLoggingContext() { return Utf8ToUtf16(m_bladeContext); }

private:
	void CompleteStartGame(DDRPlayerObj *l_player, ULONG gameLevel, IPlayer *player, int dynObjectId, bool newGame);

	// for IBlade support
	std::string m_scriptDir;
	bool m_scriptDirReadOnly;
	std::string m_name;
	IDispatcher *m_dispatcher;

	// A map of dynamic object ids associated to player ids and player objects
	DynObjPlayersMap m_dynObjectPlayersMap;

	GameLevelsMap m_gameLevelsMap; // game and level data

	DDRGameDB *m_db;

	std::string m_bladeContext;

	time_t m_lastPurgePlayersMap;
	time_t m_notifyInactivePlayers;

	ULONG m_inactivePlayerNotificationSeconds;
	ULONG m_purgeTimeoutSeconds;

	const DBConfig *m_dbConfig;
};

} // namespace KEP
