///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Monitored.h"
#include "Event\Base\IDispatcher.h"
#include "EventQueue.h"

#include "Event\Base\EventRegistry.h"

#include "LogHelper.h"

#include "event\base\IWriteableBuffer.h"

#include "event\EventMap.h"

#include <vector>

namespace KEP {

class IGame;
class ISendHandler;

class Dispatcher final : public IDispatcher, public Monitored {
public:
	static Dispatcher* GetEnginesDispatcher();

	Dispatcher();

	virtual ~Dispatcher();

	Monitored::DataPtr monitor() const;

	void Initialize();

	EVENT_ID RegisterEvent(
		const char* name,
		ULONG version,
		CREATE_EVENT_FN fn,
		SET_EVENT_ID_FN setfn,
		EVENT_PRIORITY prio);

	bool RegisterHandler(
		const std::shared_ptr<IEventHandler>& pEH,
		ULONG eventVersion,
		EVENT_ID eventId,
		EVENT_PRIORITY priority = EP_NORMAL);

	bool RegisterHandler(
		HANDLE_EVENT_FN fn,
		ULONG lparam,
		const char* name,
		ULONG eventVersion,
		EVENT_ID eventId,
		EVENT_PRIORITY priority = EP_NORMAL);

	bool RegisterFilteredHandler(
		const std::shared_ptr<IEventHandler>& pEH,
		ULONG eventVersion,
		EVENT_ID eventId,
		ULONG filter = 0,
		bool filterIsMask = false,
		OBJECT_ID filteredObjectId = 0,
		EVENT_PRIORITY priority = EP_NORMAL);

	bool RegisterFilteredHandler(
		HANDLE_EVENT_FN fn,
		ULONG lparam,
		const char* name,
		ULONG eventVersion,
		EVENT_ID eventId,
		ULONG filter = 0,
		bool filterIsMask = false,
		OBJECT_ID filteredObjectId = 0,
		EVENT_PRIORITY priority = EP_NORMAL);

	EVENT_ID GetEventId(const std::string& name) {
		return m_eventMap.GetEventId(name);
	}

	std::string GetEventName(EVENT_ID eventId, ULONG* version = NULL) {
		return m_eventMap.GetEventName(eventId, version);
	}

	bool HasHandler(EVENT_ID eventId, ULONG filter = 0, OBJECT_ID filteredObjectId = 0) {
		return m_eventMap.HasHandler(eventId, filter, filteredObjectId);
	}

	bool RemoveHandler(IEventHandler* eh, EVENT_ID eventId) {
		return m_eventMap.RemoveHandler(eh, eventId);
	}

	bool RemoveHandler(HANDLE_EVENT_FN fn, ULONG lparam, const char* name, EVENT_ID eventId) {
		return m_eventMap.RemoveHandler(fn, lparam, name, eventId);
	}

	IEvent* MakeEvent(PCBYTE buffer, ULONG len, NETID from = SERVER_NETID) {
		return m_eventMap.MakeEvent(buffer, len, from);
	}

	ULONG MakeAndQueueEvent(PCBYTE buffer, ULONG len, NETID from = SERVER_NETID) {
		return QueueEvent(MakeEvent(buffer, len, from));
	}

	virtual IEvent* MakeEvent(EVENT_ID eventId, NETID from = SERVER_NETID) {
		return m_eventMap.MakeEvent(eventId, from);
	}

	ULONG MakeAndQueueEvent(EVENT_ID eventId, NETID from = SERVER_NETID) {
		return QueueEvent(MakeEvent(eventId, from));
	}

	ULONG MakeAndQueueEvent(const std::string& name, NETID from = SERVER_NETID) {
		return QueueEvent(MakeEvent(GetEventId(name), from));
	}

	ULONG QueueEventInFutureMs(IEvent* e, TimeMs timeMs);

	bool RescheduleEventInFutureMs(IEvent* e, TimeMs timeMs) {
		return m_queue.RescheduleEventInFutureMs(e, timeMs);
	}

	bool CancelEvent(IEvent* e) {
		return m_queue.CancelEvent(e);
	}

	IEncodingFactory* EncodingFactory() const {
		return m_encodingFactory;
	}

	void LogMessage(int logType, const std::string& msg);

	void LogAlertMessage(const std::string& msg);

	void StopProcessingEvents();

	/**
	* Single event processing.
	* @param e					Event to be processed.
	* @param processedCount	Pointer to int where a count of events processed (presumably one) is passed.
	*                          Defaults to 0 (null).
	* @param d1                Differentiator. Integer value for use by timing logging. The differentiator
	*                          allows callers of processOneEvent to uniquely identify, or identify calls
	*                          in timing logging.  Defaults to 0.
	* @param d2                See d1.
	*/
	bool ProcessOneEvent(IEvent* e, int* processedCount = 0, int d1 = 0, int d2 = 0);

	// returns true of exactly one processor processed it YOU MUST call e->Delete() to free it
	virtual bool ProcessSynchronousEvent(IEvent* e) override;
	virtual bool ProcessSynchronousEventNoRelease(IEvent* e) override;

	// wait for one in the queue, usually only used internally
	bool ProcessNextEvent(TimeMs waitMs = INFINITE_WAIT);

	// process events until time limit hit, or until no more events
	void ProcessEventsUntil(TimeMs maxTimeMs = INFINITE_WAIT);

	EventMap& eventMap() {
		return m_eventMap;
	}

	bool ReconcileRemoteRegistry(EventRegistry& remoteRegistry);

	void SetNetId(NETID netId) {
		m_myNetId = netId;
	}

	void SetSendHandler(ISendHandler* sh);

	size_t GetEventQueueDepth() const {
		return m_queue.EventCount();
	}

private:
	bool RegisterHandler(ULONG eventVersion, EVENT_ID eventId, EVENT_PRIORITY priority, const char* name, EventHandlerInfo info) {
		return m_eventMap.RegisterHandler(eventVersion, eventId, priority, name ? name : "", info);
	}

	bool RemoveHandler(void* item, ULONG lparam, const char* name, EVENT_ID eventId) {
		return m_eventMap.RemoveHandler(item, lparam, name ? name : "", eventId);
	}

	/** DRF
	* Remaps the local event id to the associated server event id using m_localToServerMapping[].
	*/
	bool RemapLocalToServerEventId(IEvent* e) {
		return m_eventMap.RemapLocalToServerEventId(e);
	}

	bool sendEvent(IEvent* e);

	EventQueue m_queue;

	IEncodingFactory* m_encodingFactory;

	Logger m_timingLogger;

	NETID m_myNetId;
	ISendHandler* m_sendHandler;

	bool m_shutdown;

	fast_recursive_mutex m_sync;
	fast_recursive_mutex& getSync() {
		return m_sync;
	}

	EventMap m_eventMap;

	USHORT m_dispType; // for checking valid send/receive
	ULONG m_thdId;
};

bool RegisterWellKnownEvents(IDispatcher* dispatcher);

} // namespace KEP
