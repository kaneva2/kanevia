///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "BEREncodingFactory.h"
#include "KEPException.h"
#include <float.h>

#include "common\KEPUtil\Helpers.h"
static LogInstance("Exception");

using namespace KEP;

BEREncodingFactory* BEREncodingFactory::m_instance = 0;
int BEREncodingFactory::m_instanceGetCount = 0;

BERReadableBuffer::BERReadableBuffer(PCBYTE buffer, ULONG len) :
		m_decoder(buffer, len) {
}

BERReadableBuffer::BERReadableBuffer(IWriteableBuffer* writeable) :
		m_decoder(0, 0) {
	jsVerify(SetFromWriteable(writeable));
}

static std::string WhyToString(jsBERDecodeException::Reason why) {
	/*		jsBERDecodeException
	        nullPointer,        // Null pointer passed in
	        badParam,           // Some other parm was bad
	        pointerPastEnd,     // the current pointer has moved past the end of the buffer.  This object is now corrupted.
	        incorrectTag,       // when decoding a type, an incorrect tag was encountered
	        tagNotFound,        // the tag passed in was not found
	        unexpectedData,     // unexpected data was decoded
	        incorrectOperation, // tried to shift out enum without shifing valid nextCode
	        bufferTooSmall,     // buffer passed in too, small, current pointer not changed the length needed is in the text in ASCII
	        writeMode,          // attempt to extract when not in read mode
	        badLength,          // encode length is bad (usu too small to extracting expected type) i.e. get short, but size is > 2
			tagTooLong			// an encode tag is more than four bytes.
	*/

	static const char* errorStrings[] = {
		"nullPointer",
		"badParam",
		"pointerPastEnd",
		"incorrectTag",
		"tagNotFound",
		"unexpectedData",
		"incorrectOperation",
		"bufferTooSmall",
		"writeMode",
		"badLength",
		"tagTooLong"
	};

	std::string s("BER Exception: ");
	if ((int)why < _countof(errorStrings))
		s += errorStrings[(int)why];
	else
		s += numToString(why, "%d");
	return s;
}

static std::string WhyToString(jsBEREncodeException::Reason why) {
	/* 		jsBEREncodeException
	        nullPointer,        // Null pointer passed in
	        badParam,           // Some other parm was bad
	        pointerPastEnd,     // the current pointer has moved past the end of the buffer.  This object is now corrupted.
	        cantReallocUserBuffer, // can't reallocate the buffer since this object was constructed with the user's buffer.
	        noMemory,           // got errro reallocating memory
	        invalidInputString, // string sent in is > SHORT_MAX in length
	        incorrectOperation  // attempted to shift in an int without shifting in a nextcode
	*/

	static const char* errorStrings[] = {
		"nullPointer",
		"badParam",
		"pointerPastEnd",
		"cantReallocUserBuffer",
		"noMemory",
		"invalidInputString",
		"incorrectOperation"
	};

	std::string s("BER Exception: ");
	if ((int)why < _countof(errorStrings))
		s += errorStrings[(int)why];
	else
		s += numToString(why, "%d");
	return s;
}

template <class T>
void decode(jsBERDecoder& decoder, T& val) {
	try {
		decoder >> val;
	} catch (jsBERDecodeException* e) {
		std::string s = WhyToString(e->why());
		s += " ";
		s += e->text();
		delete e;
		throw new KEPException(s);
	}
}

void decodeString(jsBERDecoder& decoder, std::string& val) {
	try {
		jsBERTag nextTag = decoder.peekTag();
		if (nextTag == INTEGER_TAG) {
			ULONG t;
			decoder >> t;
			val = numToString(t, "%d");
		} else if (nextTag == REAL_TAG) {
			double t;
			decoder >> t;
			val = numToString(t, "%f");
		} else if (nextTag == OCTET_STRING_TAG) {
			decoder >> val;
		} else {
			throw new jsBERDecodeException("Wrong type for number", jsBERDecodeException::incorrectTag);
		}
	} catch (jsBERDecodeException* e) {
		std::string s = WhyToString(e->why());
		delete e;
		throw new KEPException(s);
	}
}

template <class T, class intType>
void decodeNumber(jsBERDecoder& decoder, T& val, double minVal, double maxVal) {
	minVal; // DRF - unreferenced in release
	maxVal; // DRF - unreferenced in release

	try {
		jsBERTag nextTag = decoder.peekTag();
		if (nextTag == INTEGER_TAG) {
			intType t;
			decoder >> t;
			//			jsAssert( t <= maxVal && t >= minVal );
			val = (T)t;
		} else if (nextTag == REAL_TAG) {
			double t;
			decoder >> t;
			//			jsAssert( t <= maxVal && t >= minVal );
			val = (T)t;
		} else if (nextTag == BOOLEAN_TAG) {
			bool b;
			decoder >> b;
			val = (T)(b ? 1 : 0);
		} else if (nextTag == OCTET_STRING_TAG) { // our float optimization
			float t;
			// to make more compact, encode floats as octet string, cuts about in half
			// not this may not be xplatform
			ULONG length = 0;
			decoder.decodeOctetString((PBYTE)&t, sizeof(t), length);
			//			jsAssert( length == sizeof(t) && t <= maxVal && t >= minVal );
			val = (T)t;
		} else {
			throw new jsBERDecodeException("Wrong type for number", jsBERDecodeException::incorrectTag);
		}
	} catch (jsBERDecodeException* e) {
		std::string s = WhyToString(e->why());
		s += " ";
		s += e->text();
		delete e;
		throw new KEPException(s);
	}
}

// caller will allocate string
void BERReadableBuffer::ReadString(char* s, ULONG maxLen, ULONG& len) {
	try {
		m_decoder.decodeOctetString((PBYTE)s, maxLen, len);

	} catch (jsBERDecodeException* e) {
		if (e->why() == jsBERDecodeException::bufferTooSmall) { // let them retry
			delete e;
			return;
		} else {
			std::string strError = WhyToString(e->why());
			delete e;
			throw new KEPException(strError);
		}
	}
}

void BERReadableBuffer::ReadString(std::string& s) {
	try {
		m_decoder.decodeOctetString(s);
	} catch (jsBERDecodeException* e) {
		std::string errStr = WhyToString(e->why());
		delete e;
		throw new KEPException(errStr);
	}
}

// callee will allocate string
IReadableBuffer& BERReadableBuffer::operator>>(std::string& val) {
	decodeString(m_decoder, val);
	return *this;
}

void BERReadableBuffer::GetBuffer(PCBYTE& buffer, ULONG& len) {
	buffer = m_decoder.buffer();
	len = m_decoder.size();
}

void BERReadableBuffer::GetBufferLeft(PCBYTE& buffer, ULONG& len) {
	buffer = m_decoder.curPtr();
	len = m_decoder.sizeLeft();
}
IReadableBuffer& BERReadableBuffer::operator>>(int& val) {
	// BER only wants int as enum or BOOL
	LONG temp;
	IReadableBuffer& res = operator>>(temp);
	val = temp;
	return res;
}
IReadableBuffer& BERReadableBuffer::operator>>(unsigned int& val) {
	// BER only wants int as enum or BOOL
	ULONG temp;
	IReadableBuffer& res = operator>>(temp);
	val = temp;
	return res;
}
IReadableBuffer& BERReadableBuffer::operator>>(LONG& val) {
	decodeNumber<LONG, LONG>(m_decoder, val, LONG_MIN, LONG_MAX);
	return *this;
}
IReadableBuffer& BERReadableBuffer::operator>>(DOUBLE& val) {
	decodeNumber<DOUBLE, LONG>(m_decoder, val, -DOUBLE_MAX, DOUBLE_MAX);
	return *this;
}
IReadableBuffer& BERReadableBuffer::operator>>(FLOAT& val) {
	decodeNumber<FLOAT, LONG>(m_decoder, val, -FLOAT_MAX, FLOAT_MAX);
	return *this;
}

IReadableBuffer& BERReadableBuffer::operator>>(SHORT& val) {
	decodeNumber<SHORT, LONG>(m_decoder, val, SHRT_MIN, SHRT_MAX);
	return *this;
}
IReadableBuffer& BERReadableBuffer::operator>>(ULONG& val) {
	decodeNumber<ULONG, ULONG>(m_decoder, val, 0, ULONG_MAX);
	return *this;
}
IReadableBuffer& BERReadableBuffer::operator>>(USHORT& val) {
	decodeNumber<USHORT, ULONG>(m_decoder, val, 0, USHRT_MAX);
	return *this;
}
IReadableBuffer& BERReadableBuffer::operator>>(bool& val) {
	int b;
	decodeNumber<int, ULONG>(m_decoder, b, -DOUBLE_MAX, DOUBLE_MAX);
	val = b != 0;
	return *this;
}
IReadableBuffer& BERReadableBuffer::operator>>(LONGLONG& val) {
	jsBERTag nextTag = m_decoder.peekTag();
	if (nextTag == INTEGER_TAG)
		m_decoder >> val;
	else {
		LONG l = 0;
		operator>>(l);
		val = l;
	}
	return *this;
}
IReadableBuffer& BERReadableBuffer::operator>>(ULONGLONG& val) {
	jsBERTag nextTag = m_decoder.peekTag();
	if (nextTag == INTEGER_TAG)
		m_decoder >> val;
	else {
		ULONG l = 0;
		operator>>(l);
		val = l;
	}
	return *this;
}

// init this from a writable buffer object for the case
// of locally marshalled events
bool BERReadableBuffer::SetFromWriteable(IWriteableBuffer* writeable) {
	BERWriteableBuffer* w = dynamic_cast<BERWriteableBuffer*>(writeable);
	if (w) {
		if (m_freeOnDelete != 0) {
			delete m_freeOnDelete;
			m_freeOnDelete = 0;
		}
		PCBYTE buffer = 0;
		ULONG len = 0;
		w->GetBuffer(buffer, len);
		m_decoder.setPtr(len, buffer);
		return true;
	}
	return false;
}

// init this from a readable buffer object for the case
// requeing events
bool BERWriteableBuffer::SetFromReadable(IReadableBuffer* readable) {
	BERReadableBuffer* w = dynamic_cast<BERReadableBuffer*>(readable);
	if (w) {
		PCBYTE buffer = 0;
		ULONG len = 0;
		w->GetBuffer(buffer, len);
		m_encoder.prefixSuffix(buffer, len, 0, 0);
		return true;
	}
	return false;
}

bool BERReadableBuffer::DecodeBuffer(PCBYTE& buffer, ULONG& len) {
	bool ret = false;
	try {
		if (m_decoder.sizeLeft() > 0) {
			if (m_decoder.peekTag() == OCTET_STRING_TAG) {
				// get curPtr
				buffer = m_decoder.curPtr();

				// decode the tag and len, then inc buffer past them
				jsBERTag val;
				buffer += m_decoder.decodeTag(val);
				buffer += m_decoder.decodeLength(len);

				// have decoder skip the whole buffer
				m_decoder.skipLastTaggedItem(OCTET_STRING_TAG);

				ret = true;
			} else {
				throw jsBERDecodeException("Not an octet string", jsBERDecodeException::incorrectTag);
			}
		} else {
			buffer = 0;
			len = 0;
		}
	} catch (jsBERDecodeException* e) {
		std::string s = WhyToString(e->why());
		delete e;
		throw new KEPException(s);
	}

	return ret;
}

ULONG BERReadableBuffer::BytesLeft() {
	return m_decoder.sizeLeft();
}

BERWriteableBuffer::BERWriteableBuffer() {
}

void BERWriteableBuffer::GetBuffer(PCBYTE& buffer, ULONG& len) {
	buffer = m_encoder.buffer();
	len = m_encoder.curSize();
}

template <class T>
void encode(jsBEREncoder& encoder, const T& val) {
	try {
		encoder << val;
	} catch (jsBEREncodeException* e) {
		std::string s = WhyToString(e->why());
		s += " ";
		s += e->text();
		delete e;
		throw new KEPException(s);
	}
}

IWriteableBuffer& BERWriteableBuffer::operator<<(const char* s) {
	encode(m_encoder, s);
	return *this;
}

IWriteableBuffer& BERWriteableBuffer::operator<<(const std::string& s) {
	encode(m_encoder, s);
	return *this;
}

IWriteableBuffer& BERWriteableBuffer::operator<<(int i) {
	encode(m_encoder, (long)i); // BER treats int's only as BOOL or enum, so cast to long
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(unsigned int i) {
	encode(m_encoder, (ULONG)i); // BER treats int's only as BOOL or enum, so cast to long
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(LONG val) {
	encode(m_encoder, val);
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(DOUBLE val) {
	// see if we can encode as integer
	if ((ULONG)val == val)
		encode(m_encoder, (ULONG)val);
	else
		encode(m_encoder, val);
	return *this;
}

IWriteableBuffer& BERWriteableBuffer::operator<<(FLOAT val) {
	// optimization for float, saves size but not xplatform
	m_encoder.encodeOctetString((PCBYTE)&val, sizeof(val));
	return *this;
}

IWriteableBuffer& BERWriteableBuffer::operator<<(SHORT val) {
	encode(m_encoder, val);
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(ULONG val) {
	encode(m_encoder, val);
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(USHORT val) {
	encode(m_encoder, val);
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(bool val) {
	encode(m_encoder, val);
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(LONGLONG val) {
	encode(m_encoder, val);
	return *this;
}
IWriteableBuffer& BERWriteableBuffer::operator<<(ULONGLONG val) {
	encode(m_encoder, val);
	return *this;
}

/************************ ENCODE EVENT TEST CODE
{

	IEvent *ge = m_dispatcher.MakeEvent(m_changeZoneMapId);
	IEvent *e = new RenderTextEvent( "this is another test", ctClan, "distro1", "fromdistro" );
	IWriteableBuffer &out = *(ge->OutBuffer());
	out << 1 << "testing" << 2.0;
	out.EncodeEvent(*(EventHeader*)e->Header(), *e->InBuffer());

	PBYTE buffer = 0;
	ULONG len = 0;
	ge->InBuffer()->SetFromWriteable(&out);
	int i;
	kstring s;
	double d;
	*(ge->InBuffer()) >> i >> s >> d;
	len = 0;
	len = ge->InBuffer()->DecodeBuffer(buffer, len);
	buffer = new BYTE[len];
	len = ge->InBuffer()->DecodeBuffer(buffer, len);

	IEvent *regurgitate = m_dispatcher.MakeEvent( buffer, len );
	RenderTextEvent *rte = dynamic_cast<RenderTextEvent *>(regurgitate);
	kstring text;
	eChatType ct;
	kstring toDistribution;
	kstring fromRecipient;
	rte->ExtractInfo(text, ct, toDistribution, fromRecipient);
	ct = ctTalk;
}
************************ ENCODE EVENT TEST CODE *****/

void BERWriteableBuffer::EncodeEvent(const EventHeader& h, IReadableBuffer* buff) {
	try {
		PCBYTE buffer;
		ULONG len;

		// add the event just like it is if it comes over the wire
		// so the dispatcher can create from the buffer
		jsBEREncoder encoder;

		if (buff) {
			buff->GetBuffer(buffer, len);
			encoder.prefixSuffix(buffer, len, 0, 0);
		}

		encoder.prefixSuffix((PCBYTE)&h, sizeof(h), 0, 0);

		// encode it so we can get it out
		EncodeBuffer(encoder.buffer(), encoder.curSize());
	} catch (jsBEREncodeException* e) {
		std::string s = WhyToString(e->why());
		delete e;
		throw new KEPException(s);
	}
}

void BERWriteableBuffer::EncodeBuffer(PCBYTE buffer, ULONG len) {
	ULONG ret = 0;
	try {
		ret = m_encoder.encodeOctetString(buffer, len);
	} catch (jsBEREncodeException* e) {
		std::string s = WhyToString(e->why());
		delete e;
		throw new KEPException(s);
	}
}

void BERWriteableBuffer::CopyBuffer(PCBYTE buffer, ULONG len) {
	try {
		m_encoder.copyBuffer(buffer, len);
	} catch (jsBEREncodeException* e) {
		std::string s = WhyToString(e->why());
		delete e;
		throw new KEPException(s);
	}
}
