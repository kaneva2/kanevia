/******************************************************************************
 InitScriptEvents.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Engine/Base/EngineStrings.h"
#include "Event/Base/IEventBlade.h"
#include "Engine/Blades/BladeFactory.h"
#include <jsEnumFiles.h>

#include <KEPException.h>

//#include "LogHelper.h"
#include "common/KEPUtil/Helpers.h"

namespace KEP {

// should be in <pathSearch>/BladeScripts or BladeScriptsd
inline std::string PathBladeScript(const std::string& pathSearch) {
	static std::string path;
	if (path.empty()) {
#ifdef _DEBUG
		static const std::string pathBladeScripts = "BladeScriptsd";
#else
		static const std::string pathBladeScripts = "BladeScripts";
#endif
		path = PathFind(pathSearch, pathBladeScripts);
	}
	return path;
}

template <class bladeVector>
bool InitScriptBlades(
	Logger& m_logger, Dispatcher* dispatcher, const std::string& scriptBinDir, const std::string& scriptDir, bool scriptDirReadOnly, const DBConfig* dbConfig, BladeFactory& eventBladeFactory, bladeVector& eventBlades) {
	// DRF - Skip Load If scriptDir Empty
	// This is intended for the DispatcherEngineClient on a server which loads after ServerEngine script blades.
	// In this case we must return true to avoid KEPServerImpl::_dowork() from destroying the engine.
	if (scriptDir.empty()) {
#ifdef _ClientOutput
		LogError("FAILED - scriptDir Empty");
		return false;
#else
		LogInfo("SKIPPING - scriptDir Empty - DispatcherEngineClient");
		return true;
#endif
	}

	// get all the blades, and call their register methods
	try {
		// DRF - TODO - Client BladeScripts Need Moved To .../pathApp/../BladeScripts Same As Server
#ifdef _ClientOutput
		// should be in <scriptDir>/../BladeScripts or BladeScriptsd
		std::string bladeScriptDir = PathBladeScript(PathAdd(scriptDir, ".."));
#else
		// should be in <pathApp>/../BladeScripts or BladeScriptsd
		std::string bladeScriptDir = PathBladeScript(PathAdd(PathApp(), ".."));
#endif
		LogInfo("bladeScriptDir=" << bladeScriptDir);

		if (scriptBinDir.length() == 0)
			eventBladeFactory.Initialize(bladeScriptDir.c_str());
		else
			eventBladeFactory.Initialize(scriptBinDir.c_str());

		BC<IEventBlade> bladeCaster;
		IBladePtrVector blades = eventBladeFactory.CreateBlades(bladeCaster, IEventBlade::InterfaceVersion());
		int numBlades = blades.size();
		LogInfo("numBlades=" << numBlades);
		if (!numBlades) {
			LogWarn("FAILED - No Script Blades");
			return false;
		}

		// Configure Script Blades
		for (auto& pIB : blades) {
			IEventBladePtr pEB = std::dynamic_pointer_cast<IEventBlade>(pIB);
			if (!pEB)
				continue;
			pEB->SetPathBase(scriptDir, scriptDirReadOnly);
			pEB->SetDBConfig(dbConfig);
		}

#ifdef _ClientOutput
		// Script Blades Pre-Initialization
		LogTrace("Script Blades PreInitialize()...");
		for (auto& pIB : blades) {
			IEventBladePtr pEB = std::dynamic_pointer_cast<IEventBlade>(pIB);
			if (!pEB)
				continue;
			if (!pEB->PreInitialize(dispatcher, IEvent::EncodingFactory()))
				LogWarn("PreInitialize(" << pEB->Name() << ") FAILED");
		}
#endif

		// Script Blades Event Registration
		LogTrace("Script Blades RegisterEvents()...");
		for (auto& pIB : blades) {
			IEventBladePtr pEB = std::dynamic_pointer_cast<IEventBlade>(pIB);
			if (!pEB)
				continue;
			if (!pEB->RegisterEvents(dispatcher, IEvent::EncodingFactory()))
				LogWarn("RegisterEvents(" << pEB->Name() << ") FAILED");
		}

		// Script Blades Event Handler Registration
		LogTrace("Script Blades RegisterEventHandlers()...");
		for (auto& pIB : blades) {
			IEventBladePtr pEB = std::dynamic_pointer_cast<IEventBlade>(pIB);
			if (!pEB)
				continue;
			if (!pEB->RegisterEventHandlers(dispatcher, IEvent::EncodingFactory()))
				LogWarn("RegisterEventHandlers(" << pEB->Name() << ") FAILED");
			eventBlades.push_back(pEB);
		}
	} catch (KEPException* e) {
		LogError("FAILED - Exception Thrown - " << e->ToString());
		e->Delete();
		return false;
	}

	LogTrace("DONE");
	return true;
}

template <class bladeVector>
bool PostInitScriptBlades(Logger& logger, Dispatcher* dispatcher, BladeFactory& eventBladeFactory, bladeVector& blades) {
#ifdef _ClientOutput
	int numBlades = blades.size();
	LOG4CPLUS_DEBUG(logger, "PostInitScriptBlades(): numBlades=" << numBlades);
	if (blades.empty()) {
		LOG4CPLUS_WARN(logger, "PostInitScriptBlades(): FAILED - No Script Blades");
		return false;
	}

	// Script Blades Post-Initialization
	LOG4CPLUS_DEBUG(logger, "PostInitScriptBlades(): Script Blades PostInitialize()...");
	for (bladeVector::iterator i = blades.begin(); i != blades.end(); i++) {
		IEventBladePtr h = *i;
		LOG4CPLUS_DEBUG(logger, "PostInitScriptBlades(): PostInitialize(" << (*i)->Name() << ") ...");
		if (!h->PostInitialize(dispatcher, IEvent::EncodingFactory())) {
			LOG4CPLUS_WARN(logger, "PostInitScriptBlades(): PostInitialize(" << (*i)->Name() << ") FAILED");
		}
	}
	LOG4CPLUS_DEBUG(logger, "PostInitScriptBlades(): DONE");
#endif
	return true;
}

inline void FreeAllBlades(EventBladeVector& eventBlades) {
	// make a copy in case this thread gets killed waiting on a blade's FreeAll
	EventBladeVector v = eventBlades;
	eventBlades.clear();
	for (EventBladeVector::iterator i = v.begin(); i != v.end(); i++) {
		IEventBladePtr p = *i;
		p->FreeAll();
	}
	v.clear();
}

} // namespace KEP
