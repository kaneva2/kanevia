///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "EventQueue.h"

using namespace KEP;

EventQueue::EventQueue() :
		_monitorData(Monitored::DataPtr(new MonitorData(&m_queue))) {
}

void EventQueue::DeleteAllEvents() {
#if defined USE_NEW_EVENT_QUEUE
	m_queue.DeleteAllEvents();
#else
	for (IEvent* e : m_queue) {
		e->Release();
	}
	m_queue.clear();
#endif
}

// compare two events for insertion in the queue
// now, we just compare priority
static inline bool operator<(const IEvent& l, const IEvent& r) {
	return l.Priority() < r.Priority();
}

/// add an event to the queue, returns queue count
ULONG EventQueue::AddEvent(IEvent* e) {
	if (!e)
		return 0;

	std::lock_guard<fast_mutex> lock(m_sync);
#if defined USE_NEW_EVENT_QUEUE
	m_queue.AddEvent(e);
	size_t nEvents = m_queue.GetEventCount();

	// Wake up waiters if we became non-empty.
	if (m_hEvent && nEvents == 1)
		SetEvent(m_hEvent.GetHandle());

	return nEvents;
#else
	e->incQueuedCnt(); // allow it to be queued > 1 time

	// find where to insert it
	ULONG ret = 0;
	if (m_queue.empty()) {
		m_queue.push_back(e);
	} else {
		EventQueueImpl::iterator i;
		bool found = false;
		for (i = m_queue.begin(); i != m_queue.end(); i++) {
			if (*(*i) < *e) {
				m_queue.insert(i, e);
				found = true;
				break;
			}
		}
		if (!found)
			m_queue.push_back(e);
		ret = m_queue.size();
	}

	m_event.releaseWaiters();

	return ret;
#endif
}

#if defined USE_NEW_EVENT_QUEUE
IEvent* EventQueue::NextEvent() {
	if (!m_queue.HasEvents())
		return nullptr;

	std::unique_lock<fast_mutex> lock(m_sync);
	IEvent* pEvent = m_queue.GetNextEvent();
	if (pEvent) {
		((MonitorData&)(*_monitorData)).set(*pEvent);
		if (m_hEvent && !m_queue.HasEvents())
			ResetEvent(m_hEvent.GetHandle());
	}

	return pEvent;
}
#endif

IEvent* EventQueue::NextEvent(TimeMs timeoutMs) {
#if defined USE_NEW_EVENT_QUEUE

	IEvent* pEvent = NextEvent();
	if (pEvent)
		return pEvent; // No wait required.

	if (timeoutMs == 0)
		return nullptr; // No wait requested.

	// We need to wait. Make sure event is initialized.
	if (!m_hEvent) {
		std::unique_lock<fast_mutex> lock(m_sync);
		if (!m_hEvent)
			m_hEvent.Set(CreateEvent(nullptr, TRUE, FALSE, nullptr));
	}

	TimeMs timeCurrentMs = fTime::TimeMs();
	TimeMs timeStartMs = timeCurrentMs;
	TimeMs timeoutRemainingMs = timeoutMs;
	do {
		TimeMs timeToWaitMs = timeoutRemainingMs;
		TimeMs timeNextMs = m_queue.NextScheduledEventTimeMs();
		if (timeNextMs != (std::numeric_limits<TimeMs>::max)()) {
			TimeMs timeUntilNextMs = timeNextMs - timeCurrentMs;
			timeToWaitMs = (std::min)(timeToWaitMs, timeUntilNextMs);
		}

		// Round up the wait time so we wait for more than zero time to guarantee that the scheduled
		// event will be ready after the wait.
		timeToWaitMs = std::ceil(timeToWaitMs);
		WaitForSingleObject(m_hEvent.GetHandle(), timeToWaitMs);

		// Return event if we have one.
		pEvent = NextEvent();
		if (pEvent)
			return pEvent;

		// Update current time and timeout remaining.
		// We need to loop again if WaitForSingleObject returns early due to some error
		// or the event that we were waiting for was removed.
		timeCurrentMs = fTime::TimeMs();
		timeoutRemainingMs = timeCurrentMs - timeStartMs;
	} while (timeoutRemainingMs > 0);

	return nullptr;
#else
	IEvent* ret = 0;
	TimeMs timeQueued; //Time event was queued
	TimeMs timeDelay; //How many milliseconds into the future must it wait
	TimeMs timeQueuedWas = 0;
	TimeMs timeDelayWas = 0;
	do {
		TimeMs timeMs = fTime::TimeMs();
		{
			std::lock_guard<fast_mutex> lock(m_sync);

			// do we have one that can be dispatched?
			for (EventQueueImpl::iterator i = m_queue.begin(); i != m_queue.end(); i++) {
				IEvent* pEvent = (*i);
				if (!pEvent)
					continue;
				pEvent->Timestamp(timeQueued, timeDelay);
				if ((timeMs - timeQueued) >= timeDelay) {
					ret = *i;
					m_queue.erase(i);
					((MonitorData&)(*_monitorData)).set(*ret);
					break;
				} else {
					timeQueuedWas = timeQueued;
					timeDelayWas = timeDelay;
				}
			}

			if (ret == 0)
				m_event.haltWaiters(); // didn't get a current one, stop anyone that may be waiting
		}

		if (ret == 0) {
			if (timeDelayWas > 0 && ((timeQueuedWas + timeDelayWas) - timeMs) < timeoutMs) {
				// one in future will fire in our wait window for sure (maybe even sooner)
				m_event.wait(((timeQueuedWas + timeDelayWas) - timeMs) + 100);
			} else {
				// wait for one to appear
				if (m_event.wait(timeoutMs) != WR_OK)
					break; // didn't get one
			}
		}
	} while (ret == 0);

	return ret;
#endif
}

bool EventQueue::RescheduleEventInFutureMs(IEvent* e, TimeMs timeMs) {
#if defined USE_NEW_EVENT_QUEUE
	if (!e)
		return false;

	std::lock_guard<fast_mutex> lock(m_sync);
	return m_queue.RescheduleEventInFutureMs(e, timeMs);
#else
	std::lock_guard<fast_mutex> lock(m_sync);
	for (const auto& pEvent : m_queue) {
		if (pEvent == e) {
			e->SetTimestamp(timeMs);
			return true;
		}
	}

	return false;
#endif
}

bool EventQueue::CancelEvent(IEvent* e) {
	std::lock_guard<fast_mutex> lock(m_sync);
#if defined USE_NEW_EVENT_QUEUE
	m_queue.RemoveEvent(e);
#else
	for (EventQueueImpl::iterator i = m_queue.begin(); i != m_queue.end();) {
		if (*i == e) {
			e->Release();
			i = m_queue.erase(i);
		} else {
			++i;
		}
	}
#endif

	return false;
}

Monitored::DataPtr EventQueue::monitor() const {
	return _monitorData;
}

#if defined USE_NEW_EVENT_QUEUE
void EventQueue::EventQueueImpl::AddEvent(IEvent* pEvent) {
	pEvent->incQueuedCnt();
	AddEventToMaps(pEvent);
	m_nEvents.fetch_add(+1, std::memory_order_relaxed);
}

void EventQueue::EventQueueImpl::AddEventToMaps(IEvent* pEvent) {
	TimeMs tmActivate = pEvent->GetTimeToActivate();
	if (tmActivate == 0) {
		int iPriority = pEvent->Priority();
		m_EventsByPriority[iPriority].push_back(pEvent);
	} else {
		m_EventsByTime.insert(std::make_pair(tmActivate, pEvent));
	}
}

size_t EventQueue::EventQueueImpl::RemoveEventFromMaps(IEvent* pEvent) {
	size_t nRemovals = 0;

	// WARNING!!! Script sometimes passes in a pointer to an already deleted event, so we can't
	// dereference it. We can only hope that another event at the same address has not been created and
	// inserted into the queue.
	// In the future we should hook into the script's garbage collector to detect when the reference to the event
	// has been released by the script so that the event is not deleted while it is still referenced.
#if 0
	int iPriority = pEvent->Priority();
	auto itrListForPriority = m_EventsByPriority.find(iPriority);
	if( itrListForPriority != m_EventsByPriority.end() ) {
#else
	for (auto itrListForPriority = m_EventsByPriority.begin(); itrListForPriority != m_EventsByPriority.end(); ++itrListForPriority) {
#endif
	auto itr = itrListForPriority->second.begin();
	while (itr != itrListForPriority->second.end()) {
		if (*itr != pEvent) {
			++itr;
		} else {
			++nRemovals;
			itr = itrListForPriority->second.erase(itr);
		}
	}
}

// In case event time changed, search through all scheduled events.
for (auto itrListForTime = m_EventsByTime.begin(); itrListForTime != m_EventsByTime.end();) {
	if (itrListForTime->second == pEvent) {
		++nRemovals;
		itrListForTime = m_EventsByTime.erase(itrListForTime);
	} else {
		++itrListForTime;
	}
}

m_nEvents.fetch_sub(nRemovals, std::memory_order_relaxed);
return nRemovals;
}

void EventQueue::EventQueueImpl::RemoveEvent(IEvent* pEvent) {
	size_t nEntries = RemoveEventFromMaps(pEvent);
	for (size_t i = 0; i < nEntries; ++i) {
		pEvent->Release();
	}
}

bool EventQueue::EventQueueImpl::RescheduleEventInFutureMs(IEvent* pEvent, TimeMs timeMs) {
	size_t nEntries = RemoveEventFromMaps(pEvent);
	if (nEntries == 0) // Only reschedule events that are in the queue.
		return false;

	pEvent->SetTimestamp(timeMs);
	for (size_t i = 0; i < nEntries; ++i)
		AddEventToMaps(pEvent);
	return true;
}

void EventQueue::EventQueueImpl::DeleteAllEvents() {
	for (auto pr : m_EventsByPriority) {
		for (IEvent* pEvent : pr.second) {
			pEvent->Release();
		}
	}
	m_EventsByPriority.clear();

	for (auto pr : m_EventsByTime) {
		pr.second->Release();
	}
	m_EventsByTime.clear();

	m_nEvents.store(0, std::memory_order_relaxed);
}

IEvent* EventQueue::EventQueueImpl::GetNextEvent() {
	// If we have any delayed events, check if any of them should be activated.
	if (!m_EventsByTime.empty()) {
		// Move events that have become current from m_EventsByTime to m_EventsByPriority
		TimeMs timeCurrentMs = fTime::TimeMs();
		auto itr = m_EventsByTime.begin();
		while (itr != m_EventsByTime.end()) {
			if (itr->first > timeCurrentMs)
				break;

			IEvent* pEvent = itr->second;
			m_EventsByPriority[pEvent->Priority()].push_back(pEvent);
			itr = m_EventsByTime.erase(itr);
		}
	}

	auto itr = m_EventsByPriority.begin();
	while (itr != m_EventsByPriority.end()) {
		std::deque<IEvent*>& listForPriority = itr->second;
		if (!listForPriority.empty()) {
			IEvent* pEvent = listForPriority.front();
			listForPriority.pop_front();
			m_nEvents.fetch_add(-1, std::memory_order_relaxed);
			return pEvent;
		}

		// This list was empty. Erase it and check the next one.
		itr = m_EventsByPriority.erase(itr);
	}

	return nullptr; // All priority lists were empty.
}
#endif
