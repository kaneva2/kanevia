/******************************************************************************
 BEREncodingFactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Event/Base/IReadableBuffer.h"
#include "Event/Base/IWriteableBuffer.h"
#include "Event/Base/IEncodingFactory.h"

#include <jsBERDecoder.h>
#include <jsBEREncoder.h>

namespace KEP {

class BERReadableBuffer : public IReadableBuffer {
public:
	BERReadableBuffer(IN PCBYTE buffer, IN ULONG len);
	BERReadableBuffer(IWriteableBuffer* writeable);

	virtual void GetBuffer(OUT PCBYTE& buffer, OUT ULONG& len);
	virtual void GetBufferLeft(OUT PCBYTE& buffer, OUT ULONG& len);

	// caller will allocate string
	virtual void ReadString(IN char* s, IN ULONG maxLen, INOUT ULONG& len);
	virtual void ReadString(std::string& s); // drf - added

	// calling will allocate string
	virtual IReadableBuffer& operator>>(std::string& s);

	virtual IReadableBuffer& operator>>(int& i);
	virtual IReadableBuffer& operator>>(unsigned int& i);
	virtual IReadableBuffer& operator>>(LONG& val);
	virtual IReadableBuffer& operator>>(DOUBLE& val);
	virtual IReadableBuffer& operator>>(FLOAT& val);
	virtual IReadableBuffer& operator>>(SHORT& val);
	virtual IReadableBuffer& operator>>(ULONG& val);
	virtual IReadableBuffer& operator>>(USHORT& val);
	virtual IReadableBuffer& operator>>(bool& b);
	virtual IReadableBuffer& operator>>(LONGLONG& val);
	virtual IReadableBuffer& operator>>(ULONGLONG& val);

	// get a raw bunch of bytes, could be a nested event
	// buffer, etc.  returns pointer in buff decode buffer
	virtual bool DecodeBuffer(OUT PCBYTE& buffer, OUT ULONG& len);

	virtual ULONG BytesLeft();

	virtual void Rewind() {
		m_decoder.rewind();
	}

	// init this from a writable buffer object for the case
	// of locally marshalled events
	bool SetFromWriteable(IWriteableBuffer* writeable);

private:
	jsBERDecoder m_decoder;
};

class BERWriteableBuffer : public IWriteableBuffer {
public:
	BERWriteableBuffer();

	virtual void GetBuffer(OUT PCBYTE& buffer, OUT ULONG& len);
	virtual IWriteableBuffer& operator<<(const char* s);
	virtual IWriteableBuffer& operator<<(const std::string& s);
	virtual IWriteableBuffer& operator<<(int i);
	virtual IWriteableBuffer& operator<<(unsigned int i);
	virtual IWriteableBuffer& operator<<(LONG val);
	virtual IWriteableBuffer& operator<<(DOUBLE val);
	virtual IWriteableBuffer& operator<<(FLOAT val);
	virtual IWriteableBuffer& operator<<(SHORT val);
	virtual IWriteableBuffer& operator<<(ULONG val);
	virtual IWriteableBuffer& operator<<(USHORT val);
	virtual IWriteableBuffer& operator<<(bool b);
	virtual IWriteableBuffer& operator<<(LONGLONG val);
	virtual IWriteableBuffer& operator<<(ULONGLONG val);

	virtual void EncodeEvent(const EventHeader& header, IReadableBuffer* buff);
	virtual void EncodeBuffer(PCBYTE buffer, ULONG len);

	virtual bool SetFromReadable(IReadableBuffer* readable);

	virtual void Rewind() {
		m_encoder.rewind();
	}

	virtual void CopyBuffer(PCBYTE buffer, ULONG len);

private:
	jsBEREncoder m_encoder;
};

class BEREncodingFactory : public IEncodingFactory {
public:
	virtual IReadableBuffer* CreateReadable(IN PCBYTE buffer, IN ULONG len) {
		return new BERReadableBuffer(buffer, len);
	}
	virtual IReadableBuffer* CreateReadable(IN IWriteableBuffer* w) {
		return new BERReadableBuffer(w);
	}
	virtual IWriteableBuffer* CreateWriteable() {
		return new BERWriteableBuffer();
	}

	// reference counted!!
	// not thread safe!
	static BEREncodingFactory* GetInstance() {
		if (m_instance == 0)
			m_instance = new BEREncodingFactory();
		m_instanceGetCount++;
		return m_instance;
	}
	static void DeleteInstance() {
		m_instanceGetCount--;
		if (m_instanceGetCount == 0 && m_instance) {
			m_instance->Delete();
			m_instance = 0;
		}
	}

private:
	BEREncodingFactory() {} // must use GetInstance
	static BEREncodingFactory* m_instance;
	static int m_instanceGetCount;
};

} // namespace KEP
