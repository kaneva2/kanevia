///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "Core/Util/Monitored.h"
#include "Core/Util/fast_mutex.h"
#include "Core/Util/WindowsAPIWrappers.h"
#include <deque>

namespace KEP {
#define USE_NEW_EVENT_QUEUE

class EventQueue : public Monitored {
public:
	EventQueue();

	~EventQueue() {
		DeleteAllEvents();
	}

	void DeleteAllEvents();

	ULONG AddEvent(IEvent* e);

	bool HasEvents() const {
		return EventCount() > 0;
	}

	ULONG EventCount() const {
#if defined USE_NEW_EVENT_QUEUE
		return m_queue.GetEventCount();
#else
		return m_queue.size();
#endif
	}

#if defined USE_NEW_EVENT_QUEUE
	IEvent* NextEvent();
#endif

	IEvent* NextEvent(TimeMs timeoutMs);

	bool RescheduleEventInFutureMs(IEvent* e, TimeMs timeMs);

	bool CancelEvent(IEvent* e);

	virtual Monitored::DataPtr monitor() const;

private:
#if defined USE_NEW_EVENT_QUEUE
	EventHandleWrapper m_hEvent; // This is initialized only when waiting for an event is requested.
#else
	jsEvent m_event;
#endif

	fast_mutex m_sync;

#if defined USE_NEW_EVENT_QUEUE
	class EventQueueImpl {
	public:
		void AddEvent(IEvent* pEvent);
		void RemoveEvent(IEvent* pEvent);
		void DeleteAllEvents();
		bool HasEvents() const { return GetEventCount() != 0; }
		size_t GetEventCount() const { return m_nEvents.load(std::memory_order_relaxed); }
		IEvent* GetNextEvent();
		bool RescheduleEventInFutureMs(IEvent* e, TimeMs timeMs);
		TimeMs NextScheduledEventTimeMs() { return m_EventsByTime.empty() ? (std::numeric_limits<TimeMs>::max)() : m_EventsByTime.begin()->first; }

	public:
		size_t RemoveEventFromMaps(IEvent* pEvent); // Returns number of times event was in queue. Does not change event's InQueue count.
		void AddEventToMaps(IEvent* pEvent); //  Does not change event's InQueue count.

	private:
		std::map<int, std::deque<IEvent*>, std::greater<int>> m_EventsByPriority;
		std::multimap<TimeMs, IEvent*> m_EventsByTime;
		std::atomic<size_t> m_nEvents = 0;
	};
#else
	typedef std::list<IEvent*> EventQueueImpl;
#endif

	EventQueueImpl m_queue;

	class MonitorData : public Monitored::Data {
	public:
		MonitorData(EventQueueImpl* queue) :
				_eventName(), _eventId(0), _eventFilter(0), _objectId(0), _queue(queue) {
		}

		virtual std::string toString() const {
			std::lock_guard<fast_mutex> lock(_self);
			std::stringstream ss;
			ss << "<EventQueue depth=\""
#if defined USE_NEW_EVENT_QUEUE
			   << _queue->GetEventCount()
#else
			   << _queue->size()
#endif
			   << "\">"
			   << "<LastEventDequeued "
			   << "id=\"" << _eventId << "\" "
			   << "name=\"" << _eventName << "\" "
			   << "filter=\"" << _eventFilter << "\" "
			   << "objectId=\"" << _objectId << "\"/>"
			   << "</EventQueue>";
			return ss.str();
		}

		MonitorData& set(const IEvent& event) {
			std::lock_guard<fast_mutex> lock(_self);
			_eventName = typeid(event).name();
			_eventId = event.EventId();
			_eventFilter = event.Filter();
			_objectId = event.ObjectId();
			return *this;
		}

	private:
		std::string _eventName;
		int _eventId;
		int _eventFilter;
		int _objectId;
		mutable fast_mutex _self;
		EventQueueImpl* _queue;
	};

	Monitored::DataPtr _monitorData;
};

} // namespace KEP
