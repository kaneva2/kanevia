///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "Dispatcher.h"
#include "Event/Events/ShutdownEvent.h"
#include "Event/Events/GenericEvent.h"
#include "Event/Base/ISendHandler.h"
#include "Event/Base/EventStrings.h"
#include "KEPException.h"
#include "LogTimings.h"
#include <jsLock.h>
#include "BEREncodingFactory.h"
#include "GlobalEventRegistry.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");
static const ULONG THREAD_EXIT_WAIT = 3000;
static thread_local Dispatcher* s_pDispatcher = nullptr;
static Logger alert_logger(Logger::getInstance(L"Alert"));

Dispatcher* Dispatcher::GetEnginesDispatcher() {
	return s_pDispatcher;
}

Dispatcher::Dispatcher() :
		m_timingLogger(Logger::getInstance(L"Timing")),
		m_encodingFactory(BEREncodingFactory::GetInstance()),
		m_myNetId(SERVER_NETID),
		m_sendHandler(0),
		m_eventMap(),
		m_shutdown(false),
		m_thdId(0) {
	IEvent::SetEncodingFactory(m_encodingFactory);
	RegisterWellKnownEvents(this);
}

Dispatcher::~Dispatcher() {
	if (m_sendHandler)
		m_sendHandler->Delete();
	BEREncodingFactory::DeleteInstance();
}

void Dispatcher::Initialize() {
	s_pDispatcher = this;
}

EVENT_ID Dispatcher::RegisterEvent(
	const char* name,
	ULONG version,
	CREATE_EVENT_FN fn,
	SET_EVENT_ID_FN setfn,
	EVENT_PRIORITY prio) {
	return m_eventMap.RegisterEvent(name, version, fn, setfn, prio);
}

bool Dispatcher::RegisterHandler(const std::shared_ptr<IEventHandler>& pEH, ULONG eventVersion, EVENT_ID eventId, EVENT_PRIORITY priority) {
	if (!pEH || !m_eventMap.isValid(eventId)) {
		LogError("Bad Parameter - eventId=" << eventId << " events=" << m_eventMap.Events());
		return false;
	}

	return RegisterHandler(eventVersion, eventId, priority, typeid(*pEH).name(), EventHandlerInfo(pEH, priority));
}

bool Dispatcher::RegisterHandler(HANDLE_EVENT_FN fn, ULONG lparam, const char* name, ULONG eventVersion, EVENT_ID eventId, EVENT_PRIORITY priority) {
	if (!fn || !m_eventMap.isValid(eventId)) {
		LogError("Bad Parameter - func=" << (LONG)fn << " eventId=" << eventId << " name='" << name << "'");
		return false;
	}

	return RegisterHandler(eventVersion, eventId, priority, name, EventHandlerInfo(fn, name, lparam, priority));
}

bool Dispatcher::RegisterFilteredHandler(
	const std::shared_ptr<IEventHandler>& pEH,
	ULONG eventVersion, EVENT_ID eventId,
	ULONG filter, bool filterIsMask,
	OBJECT_ID filteredObjectId,
	EVENT_PRIORITY priority) {
	if (!pEH || !(m_eventMap.isValid(eventId))) {
		LogError("Bad Parameter - eventId=" << eventId << " events=" << m_eventMap.Events());
		return false;
	}

	return RegisterHandler(eventVersion, eventId, priority, typeid(*pEH).name(), EventHandlerInfo(pEH, priority, filter, filterIsMask, filteredObjectId));
}

bool Dispatcher::RegisterFilteredHandler(
	HANDLE_EVENT_FN fn,
	ULONG lparam,
	const char* name,
	ULONG eventVersion, EVENT_ID eventId,
	ULONG filter, bool filterIsMask,
	OBJECT_ID filteredObjectId,
	EVENT_PRIORITY priority) {
	if (!fn || !m_eventMap.isValid(eventId)) {
		LogError("Bad Parameter - func=" << (LONG)fn << " eventId=" << eventId << " name='" << name);
		return false;
	}

	return RegisterHandler(eventVersion, eventId, priority, name, EventHandlerInfo(fn, name, lparam, priority, filter, filterIsMask, filteredObjectId));
}

// used during processing, returns queue count if queued, 0 if sent
ULONG Dispatcher::QueueEventInFutureMs(IEvent* e, TimeMs timeMs) {
	if (!e || (e->EventId() >= m_eventMap.Events()))
		return 0;

	// Send Now ?
	bool doNow = ((timeMs == 0) && (::GetCurrentThreadId() == m_thdId));
	int toSize = e->To().size();
	bool justSelf = ((toSize == 1) && (e->To()[0] == m_myNetId));
	if (doNow && (toSize > 0) && !justSelf) {
		e->AddRef();
		sendEvent(e);
		e->Release();
		return 0;
	}

	// Shutting Down ?
	if (m_shutdown) {
		LogWarn("Ignoring Event " << typeid(*e).name() << " - Shutting Down...");
		return 0;
	}

	// Queue Event
	if (timeMs > 0)
		e->SetTimestamp(timeMs);
	e->CheckBuffer();
	return m_queue.AddEvent(e);
}

Monitored::DataPtr Dispatcher::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();
	p->ss << "<Dispatcher>";
	p->ss << m_queue.monitor()->toString();
	p->ss << "</Dispatcher>";
	return Monitored::DataPtr(p);
}

bool Dispatcher::sendEvent(IEvent* e) {
	// Remap Local To Server Event Id
	if (!RemapLocalToServerEventId(e)) {
		LogError("FAILED RemapLocalToServerEventId()");
		return false;
	}

	// Send Event To Server
	m_sendHandler->ProcessEvent(this, e);

	return true;
}

void Dispatcher::StopProcessingEvents() {
	ProcessOneEvent(new ShutdownEvent());
	m_queue.DeleteAllEvents();
}

bool Dispatcher::ProcessNextEvent(TimeMs waitMs) {
	IEvent* e = m_queue.NextEvent(waitMs);
	if (!e)
		return false;
	bool ok = ProcessOneEvent(e);
	e->Release();
	return ok;
}

void Dispatcher::ProcessEventsUntil(TimeMs maxTimeMs) {
	Timer timer;
	while (timer.ElapsedMs() < maxTimeMs) {
		if (!ProcessNextEvent(0)) // 0 = no wait
			break;
	}
}

// returns true if exactly one processor processed the event.  YOU MUST call e->Delete() to free the event
bool Dispatcher::ProcessSynchronousEvent(IEvent* e) {
	bool ok = ProcessSynchronousEventNoRelease(e);
	e->Release();
	return ok;
}

bool Dispatcher::ProcessSynchronousEventNoRelease(IEvent* e) {
	int processedCount = 0;
	e->AddRef();
	bool ok = ProcessOneEvent(e, &processedCount) && (processedCount == 1);
	return ok;
}

bool Dispatcher::ProcessOneEvent(IEvent* e, int* processedCount, int d1, int d2) {
	if (!e) {
		LogError("event is null");
		return false;
	}

	// Don't Process Events During Shutdown
	if (m_shutdown) {
		LogWarn("Shutting Down - IGNORING");
		e->Delete();
		return false;
	}

	// Valid Event Id ?
	EVENT_ID id = e->EventId();
	if (!m_eventMap.isValid(id)) {
		LogError("eventMap.isValid() FAILED - eventId=" << id);
		e->Delete(); // drf - memory leak fix
		return false;
	}

	if (m_thdId == 0)
		m_thdId = ::GetCurrentThreadId();

	if (e->To().size() > 0 && !(e->To().size() == 1 && e->To()[0] == m_myNetId)) { // have something, and not just self
		e->AddRef();
		sendEvent(e);
		e->Release();
		return true;
	}

	int count = 0;

	e->AddRef(); // avoid getting deleted in processing since we delete

	ULONG filter = e->Filter();
	ULONG objId = (ULONG)e->ObjectId();

	// call all the handlers, in order
	// IMPORTANT NOTE -- used iterator here before, but since
	// processing can change handlers array, iterators would be invalid
	for (UINT j = 0;; ++j) {
		// handlers(id).size() changes on every iteration!
		size_t handlers = m_eventMap.handlers(id).size();
		if (j >= handlers)
			break;

		EventHandlerInfo* pEHI = m_eventMap.handlers(id, j);
		if (!pEHI)
			continue;

		EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;
		e->RewindInput();

		try {
			ret = pEHI->ProcessEvent(this, e);
		} catch (KEPException* exc) {
			ret = EVENT_PROC_RC::NOT_PROCESSED;
			std::string eventName = m_eventMap.GetEventName(id);
			LogWarn("Caught Exception - ProcessEvent() -"
					<< " eventName=" << eventName
					<< " filter=" << filter
					<< " objId=" << objId
					<< " '" << exc->ToString() << "'");
			exc->Delete();
		}

		if (ret != EVENT_PROC_RC::NOT_PROCESSED)
			count++;

		if (ret == EVENT_PROC_RC::CONSUMED)
			break;
	}

	// if they want to know number of processors
	if (processedCount)
		*processedCount = count;

	bool rc = true;

	// if shutdown, don't allow reschedule
	if (id == ShutdownEvent::SHUTDOWN_EVENT_ID || m_shutdown) {
		m_shutdown = true;
		rc = false; // only return false to stop loop
	}

	e->Release();
	return rc;
}

bool Dispatcher::ReconcileRemoteRegistry(EventRegistry& remoteRegistry) {
	return m_eventMap.ReconcileRemoteRegistry(remoteRegistry);
}

void Dispatcher::LogAlertMessage(const std::string& msg) {
	LOG4CPLUS_WARN(alert_logger, msg);
}

void Dispatcher::LogMessage(int logType, const std::string& msg) {
	switch (logType) {
		case log4cplus::TRACE_LOG_LEVEL:
			_LogTrace(msg);
			break;
		case log4cplus::DEBUG_LOG_LEVEL:
			_LogDebug(msg);
			break;
		case log4cplus::INFO_LOG_LEVEL:
			_LogInfo(msg);
			break;
		case log4cplus::WARN_LOG_LEVEL:
			_LogWarn(msg);
			break;
		case log4cplus::ERROR_LOG_LEVEL:
			_LogError(msg);
			break;
		case log4cplus::FATAL_LOG_LEVEL:
			_LogFatal(msg);
			break;
		default:
			LogError("Bad log type from script.  Logging message to Trace." << logType);
			_LogError(msg);
			break;
	}
}

void Dispatcher::SetSendHandler(ISendHandler* sh) {
	if (m_sendHandler)
		delete m_sendHandler;
	m_sendHandler = sh;
}

} // namespace KEP