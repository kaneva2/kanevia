/******************************************************************************
 resource.h

 Copyright (c) 2004-2008 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VwUrlEventHandler.rc
//

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        3600
#define _APS_NEXT_COMMAND_VALUE         3600
#define _APS_NEXT_CONTROL_VALUE         3600
#define _APS_NEXT_SYMED_VALUE           3600
#endif
#endif
