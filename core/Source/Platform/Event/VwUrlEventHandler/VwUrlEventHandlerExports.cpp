///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "Event/Base/IEvent.h"
#include "../../Engine/Blades/BladeExports.h"

#include "VwUrlEventHandler.h"

#include "../../Engine/Blades/IBlade.h"
using namespace KEP;

IMPLEMENT_CREATE(VwEventBlade)

BEGIN_BLADE_LIST_ENGINE_TYPE(eEngineType::Client)
ADD_BLADE(VwEventBlade)
END_BLADE_LIST
