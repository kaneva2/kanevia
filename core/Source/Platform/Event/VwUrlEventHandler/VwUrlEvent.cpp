///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "VwUrlEvent.h"

#include "Event/Base/IEncodingFactory.h"
using namespace KEP;

DECLARE_CLASS_INFO(VwUrlEvent)

VwUrlEvent::VwUrlEvent() :
		IEvent(ClassId()),
		stackIndex(0),
		count(0) {
}

EVENT_CONSTRUCTOR_PROTOTYPE(VwUrlEvent), stackIndex(0), count(0) {}

void VwUrlEvent::ExtractInfo() {
	switch (Filter()) {
		case Urls:
		case Favorites:
			// never to C++, ignore them
			break;

		case GetHistory:
			(*InBuffer()) >> stackIndex >> count;
			break;

		case GotoUrl:
			(*InBuffer()) >> url;
			break;

		case GotoHistory:
		case GotoFavorite:
		case DeleteFavorite:
			(*InBuffer()) >> stackIndex;
			break;

		case GetCurrent:
		case GotoPrev:
		case GotoNext:
			break;

		case AddFavorite: {
			(*InBuffer()) >> url;
		} break;

		default:
			jsAssert(false);
			break;
	}
}

//Set all my favorite url.
void VwUrlEvent::SetUrlsFavorite(const UrlVector& vect) {
	SetFilter(Favorites);
	UrlVector::size_type len = vect.size();

	(*OutBuffer()) << (ULONG)len;

	for (size_t index = 0; index < vect.size(); index++)
		(*OutBuffer()) << vect[index];
}

// set urls into event for returning to menu
void VwUrlEvent::SetUrls(const UrlVector& vect, ULONG startIndex, ULONG numUrls) {
	SetFilter(Urls);

	UrlVector::size_type len = vect.size();
	if (numUrls == 0)
		numUrls = len;

	ULONG totItems = 0;

	// send with newest one on top
	ULONG endIndex = startIndex - (numUrls - 1);
	if ((numUrls - 1) > startIndex)
		endIndex = 0; // out of range, reset

	if (len > 0) {
		if (endIndex >= len)
			endIndex = len - 1;

		totItems = startIndex - endIndex + 1;
	}

	(*OutBuffer()) << (ULONG)len << totItems;
	if (totItems > 0) {
		for (ULONG i = startIndex; i >= endIndex; i--) {
			// send down reverse (1 = end of array)
			(*OutBuffer()) << len - i << vect[i];
			if (i == 0)
				break; // since unsigned for loop rolls over
		}
	}
}
