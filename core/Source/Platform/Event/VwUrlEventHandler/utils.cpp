/******************************************************************************
 utils.cpp

 Copyright (c) 2004-2008 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "windows.h"
#include <tchar.h>
#include <stdio.h>
#include "wchar.h"
#include <assert.h>
#include <string>
using namespace std;

//see if the file is there
BOOL FileExists(const wchar_t *lpFileName) {
	WIN32_FIND_DATA findd;
	HANDLE findHandle;
	findHandle = FindFirstFile(lpFileName, &findd);
	if (findHandle != INVALID_HANDLE_VALUE) {
		FindClose(findHandle);
		return TRUE;
	}
	return FALSE;
}

//safe create directory
void MakeDir(TCHAR *dir) {
	TCHAR *p = dir;
	while (*dir) {
		while ((*dir != _T('\\')) && (*dir != 0))
			dir++;
		if (*dir == 0)
			CreateDirectory(p, NULL);
		else {
			*dir = 0;
			CreateDirectory(p, NULL);
			*dir = _T('\\');
		}
		dir++;
	}
}
//safe compare of CRC
BOOL CRCCmp(char *c1, char *c2) {
	if (c1 == NULL || c2 == NULL)
		return FALSE;

	for (int i = 0; i < 32; i++)
		if (c1[i] != c2[i])
			return FALSE;

	return TRUE;
}

//crc table reading into a struct
bool ReadCRCTable2(const wchar_t *logFile, int count, CRCLIST *cList, const wchar_t *szServerInfoTemp) {
	TCHAR szFormattedSection[] = TEXT("[FILES]");
	int i, listcount = 0;
	TCHAR line[2048];
	BOOL bInSection = FALSE;
	LONG nFilesize = 0;
	TCHAR *filename, *md5, *directory, *serverfile, *bregister, *filesize;

	FILE *data = 0;
	if (_tfopen_s(&data, szServerInfoTemp, TEXT("r")) != 0 && data) {
#ifdef LOGGING
		logMsg(logFile, TEXT("Reading server info"));
#endif
		while (_fgetts(&line[0], 255, data)) {
			if (bInSection) {
				if (_tcslen(line) > 0) {
					TCHAR *tokTemp;
					string csLine = line;
					while (csLine[csLine.length() - 1] == _T(' ') || csLine[csLine.length() - 1] == _T('\t') ||
						   csLine[csLine.length() - 1] == _T('\r') || csLine[csLine.length() - 1] == _T('\n'))
						csLine.resize(csLine.length() - 1);

					if (csLine.empty())
						continue;
#ifdef LOGGING
					logMsg(logFile, TEXT("Reading %s"), line);
#endif
					string::size_type x;
					while ((x = csLine.find(_T(",,"))) != string::npos) {
						csLine.replace(x, 2, _T(", ,"));
					}
					// csLine.Replace(",,", ", ,");
					strcpy_s(line, _countof(line), csLine.c_str());
					filename = _tcstok_s(line, _T("="), &tokTemp);
					if (filename == 0)
						continue;
					md5 = _tcstok_s(NULL, _T(","), &tokTemp);
					if (md5 == 0)
						continue;
					directory = _tcstok_s(NULL, _T(","), &tokTemp);
					if (directory == 0)
						continue;
					serverfile = _tcstok_s(NULL, _T(","), &tokTemp);
					if (serverfile == 0)
						continue;
					bregister = _tcstok_s(NULL, _T(","), &tokTemp);
					if (bregister == 0)
						continue;
					filesize = _tcstok_s(NULL, _T(","), &tokTemp);
					if (filesize == 0)
						continue;

					// trim directory
					string csDirectory = directory;
					if (csDirectory.length() > 1) {
						while (csDirectory[csDirectory.length() - 1] == _T(' ') || csDirectory[csDirectory.length() - 1] == _T('\t'))
							csDirectory.resize(csDirectory.length() - 1);
					}

					if (md5) {
						_tcscpy_s(cList[listcount].filename, _countof(cList[listcount].filename), filename);
						_tcscpy_s(cList[listcount].directory, _countof(cList[listcount].directory), csDirectory.c_str());
						i = _tcslen(serverfile);
						do {
							if (i > 0) {
								i--;
								if (serverfile[i] == _T('\\')) {
									serverfile = serverfile + i + 1;
									break;
								}
							}
						} while (i);
						_tcscpy_s(cList[listcount].serverfile, _countof(cList[listcount].serverfile), serverfile);
						_tcscpy_s(cList[listcount].crc, _countof(cList[listcount].crc), md5);
						if (_tcsncmp(bregister, _T("N"), 1) == 0)
							cList[listcount].bRegister = FALSE;
						else
							cList[listcount].bRegister = TRUE;

						if (filesize)
							cList[listcount].nFilesize = _tcstol(filesize, NULL, 10);
					}

					cList[listcount].bUpToDate = FALSE;

					//Total files
					listcount++;
					if (listcount > count) {
						assert(FALSE);
						fclose(data);
						return false;
					}
				}
				memset(&line[0], 0, 255);
			}

			if (line[0] == _T('[') && _tcsncmp(line, szFormattedSection, _tcslen(szFormattedSection)) == 0)
				bInSection = TRUE;
			else if (line[0] == _T('['))
				bInSection = FALSE;
		}
	}
	fclose(data);
	return true;
}

//Simple peek message loop
VOID DoEvents() {
	MSG m;
	while (PeekMessage(&m, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&m);
		DispatchMessage(&m);
	}
}
