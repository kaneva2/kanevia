///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <js.h>
#include "IClientEngine.h"
#include "VwUrlEventHandler.h"

#include "Event/Events/DisconnectedEvent.h"
#include "Event/Events/GotoPlaceEvent.h"
#include "Event/Events/GenericEvent.h"
#include "VwUrlEvent.h"

#include "Event/Base/IDispatcher.h"

#include "Event/Base/IEncodingFactory.h"
#include "Event/Glue/ClientEngineIds.h"

#include "IServerEngine.h"
#include "IPlayer.h"
#include "IGetSet.h"

#include "KEPConfig.h"
#include "KEPException.h"

#include "TinyXML/tinyxml.h"
#include "jsEnumFiles.h"

static const int MAX_HISTORY = 100;

namespace KEP {

EVENT_PROC_RC VwEventBlade::VwUrlEventHandler(ULONG lparam, IDispatcher *dispatcher, IEvent *e) {
	jsVerifyReturn(lparam != 0 && dispatcher != 0 && e != 0, EVENT_PROC_RC::NOT_PROCESSED);

	VwEventBlade *me = (VwEventBlade *)lparam;
	VwUrlEvent *vue = dynamic_cast<VwUrlEvent *>(e);
	jsVerifyReturn(vue != 0, EVENT_PROC_RC::NOT_PROCESSED);

	vue->ExtractInfo();

	switch (vue->Filter()) {
		case VwUrlEvent::GetHistory:
			// create a new event to send back
			{
				ULONG index = me->m_history.size() - vue->stackIndex;
				if (index < me->m_history.size()) {
					VwUrlEvent *retEvent = new VwUrlEvent();
					retEvent->SetUrls(me->m_history, index, vue->count);
					dispatcher->QueueEvent(retEvent);
				}
			}
			break;

		case VwUrlEvent::GotoUrl:
			me->gotoUrl(vue->url);
			break;

		case VwUrlEvent::GotoHistory: {
			// convert 1-based stackIndex from end to 0-based index from front
			ULONG index = (me->m_history.size() - 1) - vue->stackIndex;
			if (index >= 0 && index < me->m_history.size()) {
				me->m_historyIndex = index;
				me->gotoUrl(me->m_history[index]);
			}
		} break;

		case VwUrlEvent::GotoNext:
			if (me->m_historyIndex + 1 < me->m_history.size()) {
				me->m_historyIndex++;
				me->gotoUrl(me->m_history[me->m_historyIndex]);
			}
			break;

		case VwUrlEvent::GotoPrev:
			if (me->m_historyIndex > 0 && me->m_historyIndex - 1 < me->m_history.size()) {
				me->m_historyIndex--;
				me->gotoUrl(me->m_history[me->m_historyIndex]);
			}
			break;

		case VwUrlEvent::Urls:
			// we produce these to be
			// handled in menu system, we ignore them
#ifdef _DEBUG
		{
			ULONG total, count;
			ULONG index;
			std::string ss;
			(*vue->InBuffer()) >> total >> count;
			LOG4CPLUS_TRACE(me->m_logger, "URLs: Total " << total << " Count " << count);
			for (UINT i = 0; i < count; i++) {
				(*vue->InBuffer()) >> index >> ss;
				LOG4CPLUS_TRACE(me->m_logger, "    " << index << ": " << ss);
			}
		}
#endif
		break;
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC VwEventBlade::ZoneLoadedEventHandler(ULONG lparam, IDispatcher *dispatcher, IEvent *e) {
	jsVerifyReturn(lparam != 0 && dispatcher != 0 && e != 0, EVENT_PROC_RC::NOT_PROCESSED);

	VwEventBlade *me = (VwEventBlade *)lparam;

	// this event has the exg, then url
	std::string exg, url;
	(*e->InBuffer()) >> exg >> url;

	if (!url.empty()) {
		//Did we load the history yet.
		if (!me->m_historyLoaded) {
			me->m_playerName = me->m_client->GetString(CLIENTENGINEIDS_LOGINNAME);
			me->m_brbFile = PathAdd(PathApp(), me->m_playerName + ".brb");
			me->loadHistory();

			// save off the current game name so we know if we're switching
			StpUrl::ExtractGameName(url, me->m_currentGameName);
		}

		//Add the url we are in to the end of the history.
		if (me->m_historyLoaded)
			me->addUrl(url);
	}
	return EVENT_PROC_RC::OK;
}

static UINT MSG_ID = ::RegisterWindowMessageW(L"KEI_PATCH_STATUS_MSG");
static bool m_exitPatch;

void VwEventBlade::gotoUrl(const std::string &url) {
	GotoPlaceEvent *e = new GotoPlaceEvent;

	int charIndex = 0; // TODO: can we get this, do we care?

	// if not our current game, we goto the new game, otherwise, pass it to
	// our server
	std::string gameName;
	StpUrl::ExtractGameName(url, gameName);

	LOG4CPLUS_TRACE(m_logger, "Going to URL: " << url << " from " << m_currentGameName);

	if (STLCompareIgnoreCase(gameName.c_str(), m_currentGameName.c_str()) == 0) {
		// send to our server
		charIndex = -1; // use current
		e->GotoUrl(charIndex, url.c_str());
		e->AddTo(SERVER_NETID);
	} else {
		e->GotoGame(charIndex, gameName.c_str(), url.c_str());
	}
	m_dispatcher->QueueEvent(e);
}

void VwEventBlade::addUrl(const std::string &url) {
	// push back if not empty, and doesn't match last one already in list
	if (!url.empty()) {
		// if gone "back" then get a new url, truncate the list
		if (!m_history.empty() && m_historyIndex + 1 < m_history.size()) {
			if (m_history[m_historyIndex] != url) {
				m_history.erase(m_history.begin() + m_historyIndex + 1, m_history.end());

				m_history.push_back(url);
				m_historyIndex = m_history.size() - 1;
			}
		} else if (m_history.empty() || m_history.back() != url) {
			m_history.push_back(url);
			m_historyIndex = m_history.size() - 1;
		}

#ifdef _DEBUG
		ULONG len = m_history.size();
		LOG4CPLUS_TRACE(m_logger, "Added URL: Total " << len << " Index " << m_historyIndex);
		for (UINT i = 0; i < len; i++) {
			LOG4CPLUS_TRACE(m_logger, "    " << len - i << ": " << m_history[i]);
		}
#endif
	}
}


//Load .bfb(back forward button)
void VwEventBlade::loadHistory() {
	m_historyLoaded = true;
	m_history.clear();
	std::vector<std::string> playerinfo;
	
	KEPConfig cfg;
	try {
		cfg.Load(m_brbFile, "Browser");
		int index;
		if (cfg.GetValue("RecentUrls/Index", index, 0)) {
			TiXmlHandle h(0);
			std::string url;
			int count = 0;
			if (cfg.GetFirstValue(h, "RecentUrls/URL", url, "")) {
				addUrl(url);
				while (cfg.GetNextValue(h, url) && count < MAX_HISTORY) {
					count++;
					addUrl(url);
				}
			}
		}
		if ((ULONG)index >= m_history.size())
			m_historyIndex = m_history.size() - 1; // index > array??
		else
			m_historyIndex = index;
	} catch (KEPException *e) {
		LOG4CPLUS_WARN(m_logger, "Couldn't open browser file " << m_brbFile);
		e->Delete();
	}
}

//Save .bfb(back forward button)
void VwEventBlade::saveHistory() {
	std::vector<std::string> playerinfo;

	bool isDir = false;
	if (!jsFileExists(m_brbFile.c_str(), &isDir)) {
		//Create new config file
		KEPConfig cfgNew;
		cfgNew.Parse("<Browser/>", "Browser");
		cfgNew.SaveAs(m_brbFile);
	}

	KEPConfig cfg;
	try {
		cfg.Load(m_brbFile, "Browser");
		cfg.RemoveValue("RecentUrls");
		cfg.SetValue("RecentUrls/Index", (int)m_historyIndex);
		for (UrlVector::iterator i = m_history.begin(); i != m_history.end(); i++) {
			cfg.SetValue("RecentUrls/URL", i->c_str(), true);
		}
		cfg.Save();
	} catch (KEPException *e) {
		LOG4CPLUS_WARN(m_logger, "Couldn't open browser file " << m_brbFile);
		e->Delete();
	}
}

bool VwEventBlade::RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *ef) {
	jsVerifyReturn(dispatcher != 0 && ef != 0, false);

	m_dispatcher = dispatcher;

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	// register our events that we raise, consume
	RegisterEvent<VwUrlEvent>(*m_dispatcher);

	m_progressEventId = m_dispatcher->RegisterEvent("PatchProgressEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	return true;
}

bool VwEventBlade::RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *ef) {
	jsVerifyReturn(dispatcher != 0 && ef != 0, false);

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	// now register handlers
	// only register this on client
	m_client = dynamic_cast<IGetSet *>(dispatcher->Game());
	IClientEngine *cl = dynamic_cast<IClientEngine *>(dispatcher->Game());
	if (m_client != 0 && cl != 0 && cl->EngineType() == eEngineType::Client) {
		jsVerify(dispatcher->RegisterHandler(VwEventBlade::VwUrlEventHandler, (ULONG)this, "VwUrlEventHandler", VwUrlEvent::ClassVersion(), dispatcher->GetEventId("VwUrlEvent")));
		jsVerify(dispatcher->RegisterHandler(VwEventBlade::ZoneLoadedEventHandler, (ULONG)this, "ZoneLoadedEventHandler", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("ZoneLoadedEvent")));
		jsVerify(dispatcher->RegisterHandler(VwEventBlade::ZoneLoadedEventHandler, (ULONG)this, "ZoneLoadedEventHandler", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("ZoneReloadedEvent")));
	}

	return true;
}

void VwEventBlade::FreeAll() {
	jsVerifyDo(m_dispatcher != 0, return );

	IGame *game = dynamic_cast<IGame *>(m_dispatcher->Game());
	if (game != 0 && game->EngineType() == eEngineType::Client) {
		m_dispatcher->RemoveHandler(VwEventBlade::VwUrlEventHandler, (ULONG)this, "VwUrlEventHandler", m_dispatcher->GetEventId("VwUrlEvent"));
		m_dispatcher->RemoveHandler(VwEventBlade::ZoneLoadedEventHandler, (ULONG)this, "ZoneLoadedEventHandler", m_dispatcher->GetEventId("ZoneLoadedEvent"));
		m_dispatcher->RemoveHandler(VwEventBlade::ZoneLoadedEventHandler, (ULONG)this, "ZoneLoadedEventHandler", m_dispatcher->GetEventId("ZoneReloadedEvent"));
	}

	saveHistory();
}

bool VwEventBlade::ReRegisterEventHandlers(IDispatcher * /*dispatcher*/, IEncodingFactory * /*factory*/) {
	return true;
}

extern "C" BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD ul_reason_for_call,
	LPVOID /*lpReserved*/
) {
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
		StringResource = (HINSTANCE)hModule;
	return TRUE;
}

} // namespace KEP
