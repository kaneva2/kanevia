///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "common/include/kepcommon.h"
#include "Event/Base/EventHandler.h"
#include "VwUrlEvent.h"
#include "KEPHelpers.h"
#include "IClientEngine.h"

#include <log4cplus/logger.h>

namespace KEP {

class VwEventBlade : public IEventBlade {
public:
	VwEventBlade() :
			m_logger(log4cplus::Logger::getInstance(L"VwEventBlade")),
			m_dispatcher(0),
			m_client(0),
			m_historyLoaded(false),
			m_historyIndex(0),
			m_progressEventId(BAD_EVENT_ID) {
		m_name = typeid(*this).name();
	}

	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }

	// IBlade overrides
	virtual ULONG Version() const { return PackVersion(1, 0, 0, 0); }

	// BaseDir is scriptPath For Event Blades
	virtual std::string PathBase() const { return m_scriptDir; }
	virtual void SetPathBase(const std::string &baseDir, bool readonly) {
		m_scriptDir = baseDir;
		m_scriptDirReadOnly = readonly;
	}

	virtual void Delete() { delete this; }

	virtual void FreeAll();

	virtual const char *Name() const { return m_name.c_str(); }

	// IEventBlade overrides
	bool RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);

protected:
	virtual bool IsBaseDirReadOnly() const { return m_scriptDirReadOnly; }

private:
	static EVENT_PROC_RC VwUrlEventHandler(ULONG lparam, IDispatcher *dispatcher, IEvent *e);
	static EVENT_PROC_RC ZoneLoadedEventHandler(ULONG lparam, IDispatcher *dispatcher, IEvent *e);

	log4cplus::Logger m_logger;

	// for IBlade support
	std::string m_scriptDir;
	bool m_scriptDirReadOnly;
	std::string m_name;
	IDispatcher *m_dispatcher;
	IGetSet *m_client;
	EVENT_ID m_progressEventId;

	void loadHistory();
	void saveHistory();

	void addUrl(const std::string &url);
	void gotoUrl(const std::string &url);

	// variables for our handling
	bool m_historyLoaded;
	UrlVector m_history;

	UrlVector::size_type m_historyIndex; // valid only if have items in history
	std::string m_playerName;
	std::string m_currentGameName;
	std::string m_url;
	std::string m_brbFile;
};

} // namespace KEP
