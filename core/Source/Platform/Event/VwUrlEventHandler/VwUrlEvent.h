///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include <string>
#include <vector>

typedef std::vector<std::string> UrlVector;

namespace KEP {

#define UNKNOWN_FROM -1

class VwUrlEvent : public IEvent {
public:
	VwUrlEvent();

	enum Actions {
		// response from handler
		Urls = 1, // totalInHistory, countSending, [id,url]

		// request from menu, returns Urls
		GetHistory, // start index, count
		GetCurrent, // no parms

		// gotos
		GotoUrl, // url
		GotoHistory, // index of item
		GotoPrev, // no parms
		GotoNext, // no parms
		GotoFavorite, // index of item

		// favorites
		Favorites, // returned from handler, count [id,type,depth,name,url]
		GetFavorites, // start index, count, depth
		AddFavorite, // type (folder/url), name, uses current url
		DeleteFavorite, // index
		//... manage favorites..., folders, move, etc.
	};

	void ExtractInfo();

	// set urls into event for returning to menu
	void SetUrls(const UrlVector& vect, ULONG start, ULONG count);

	//set urls favorites into event for returning to menu.
	void SetUrlsFavorite(const UrlVector& vec);

	DEFINE_SECURE_CLASS_INFO(VwUrlEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)

	ULONG stackIndex; // when making a request, staring index in array of list, or index of wanted item
	ULONG count; // when making a request, number of items want back
	std::string url; // when going to a url
};

} // namespace KEP
