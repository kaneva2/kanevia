///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "DragDropEvents.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(DragDropEvent)
DECLARE_CLASS_INFO(DragLeaveEvent)

DragDropEvent::DragDropEvent(bool aInitial, bool aDropped, int aType, const std::string& aFullPath, int aMouseX, int aMouseY, DWORD aKeyState) :
		IEvent(ClassId()) {
	int bInitialTmp = aInitial ? 1 : 0, bDroppedTmp = aDropped ? 1 : 0;

	// pack it
	(*OutBuffer()) << bInitialTmp << bDroppedTmp << aType << aFullPath << aMouseX << aMouseY << aKeyState;
}

EVENT_CONSTRUCTOR_PROTOTYPE(DragDropEvent) //, m_bInitial( false ), m_bDropped( false ), m_type(0), m_mouseX( -1 ), m_mouseY( -1 ), m_keyState( 0 )
{
	// do nothing here
}

void DragDropEvent::ExtractInfo(bool& aInitial, bool& aDropped, int& aType, std::string& aFullPath, int& aMouseX, int& aMouseY, DWORD& aKeyState) {
	int bInitialTmp, bDroppedTmp;
	(*InBuffer()) >> bInitialTmp >> bDroppedTmp >> aType >> aFullPath >> aMouseX >> aMouseY >> aKeyState;

	aInitial = bInitialTmp != 0;
	aDropped = bDroppedTmp != 0;
}

DragLeaveEvent::DragLeaveEvent() :
		IEvent(ClassId()) {
}

EVENT_CONSTRUCTOR_PROTOTYPE(DragLeaveEvent) {
	// do nothing here
}
