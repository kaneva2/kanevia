///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"
#include <instanceid.h>

#include "Glids.h"

#include "Event\PlayListMgr\PLMMediaData.h"

namespace KEP {

/// class for passing params back for forth to DB background thd
/// for Play List management actions
class PlaylistBackgroundEvent : public IBackgroundDBEvent {
public:
	PlaylistBackgroundEvent(EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority),
			m_eType(PL_GET_PLAYLIST_ZONE),
			m_pEventReply(0),
			m_pEventBroadcast(0),
			m_zoneMediaState(0),
			m_playlistId(0),
			m_objectPlacementID(0),
			m_globalID(GLID_INVALID),
			m_playlistMediaIndex(0),
			m_retVal(0) {}

	~PlaylistBackgroundEvent() {
		// If we still own this data and not the event consumer, clean it up.
		if (m_zoneMediaState)
			delete m_zoneMediaState;
	}

	enum eType {
		PL_GET_PLAYLIST_ZONE,
		PL_GET_PLAYLIST_SINGLE,
		PL_GET_PLAYLIST_SINGLE_NEXT_TRACK,
		PL_TYPES
	};

	std::string eTypeStr(eType eventType) {
		const std::string eTypeStr[PL_TYPES] = {
			"GET_PLAYLIST_ZONE",
			"GET_PLAYLIST_SINGLE",
			"GET_PLAYLIST_SINGLE_NEXT_TRACK"
		};
		return (eventType < PL_TYPES) ? eTypeStr[eventType] : "<UNKNOWN>";
	}

	std::string ToStr() {
		std::string str;
		StrAppend(str, "PBE<" << eTypeStr(m_eType));
		if (m_objectPlacementID)
			StrAppend(str, " objId=" << m_objectPlacementID);
		if (m_globalID)
			StrAppend(str, " glid=" << m_globalID);
		if (m_playlistId)
			StrAppend(str, " playlistId=" << m_playlistId);
		if (!m_charName.empty())
			StrAppend(str, " '" << m_charName << "'");
		StrAppend(str, " " << zoneIndex.ToStr() << ">");
		return str;
	}

	// inbound only
	eType m_eType; // what type of action is required on dynamic object

	int m_playerID;

	std::string m_charName; // players name for logging

	// outbound only
	IEvent* m_pEventReply;
	IEvent* m_pEventBroadcast;

	ZoneMediaState* m_zoneMediaState;

	int m_playlistId;
	int m_objectPlacementID;
	int m_globalID;
	size_t m_playlistMediaIndex;
	int m_retVal;

	DEFINE_DB_CLASS_INFO(PlaylistBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP