///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptClientEvent.h"
#include "ScriptEventArgs.h"

#include "TriggerEnums.h"

namespace KEP {

template <typename T>
struct PrimitiveGeomArgs;

// Client handlers for script server events.
// The server includes this header file to read the declarations so it knows
// what types the arguments are supposed to be.
// The handler implementations are all on the client.

// This type is used to determine whether a specialization exists for a given event ID.
struct Unspecialized_SCE_ClientHandlerBase {};

struct SCE_ClientHandlerDefaults {
	// Calling this function in Lua will send the event to all players in the zone.
	static constexpr char const* LuaFunctionNameForZone = nullptr;

	// Calling this function in Lua will send the event to only the specified player.
	// The player ID (NETID) will be the first argument of the function.
	static constexpr char const* LuaFunctionNameForPlayer = nullptr;

	// Settings if the event requires caching by game state manager
	static constexpr ScriptClientEvent::EventType GameStateModifierType = ScriptClientEvent::InvalidEventType;
	template <typename... Args>
	static int GameStateModifierSubType(Args&&... args) { return 0; }
	static constexpr bool RemoveGameStateModifier = false;
};

// This class needs to be specialized in order to handle a ScriptClientEvent
template <ScriptClientEvent::EventType eventType>
struct SCE_ClientHandler : public Unspecialized_SCE_ClientHandlerBase {};

template <>
struct SCE_ClientHandler<ScriptClientEvent::ObjectEquipItem> : SCE_ClientHandlerDefaults {
	static constexpr char const* LuaFunctionNameForZone = "objectEquipItem";
	static constexpr char const* LuaFunctionNameForPlayer = "objectEquipItemForPlayer";
	static void Handle(int placementId, GLID accessoryGlid, const boost::optional<std::string>& optstrBoneOverride);
};

template <>
struct SCE_ClientHandler<ScriptClientEvent::ObjectUnEquipItem> : SCE_ClientHandlerDefaults {
	static constexpr char const* LuaFunctionNameForZone = "objectUnEquipItem";
	static constexpr char const* LuaFunctionNameForPlayer = "objectUnEquipItemForPlayer";
	static void Handle(int placementId, GLID accessoryGlid);
};

template <>
struct SCE_ClientHandler<ScriptClientEvent::ObjectSetEquippedItemAnimation> : SCE_ClientHandlerDefaults {
	static constexpr char const* LuaFunctionNameForZone = "objectSetEquippedItemAnimation";
	static constexpr char const* LuaFunctionNameForPlayer = "objectSetEquippedItemAnimationForPlayer";
	static void Handle(int placementId, GLID accessoryGlid, GLID animationGlid);
};

template <>
struct SCE_ClientHandler<ScriptClientEvent::ObjectRepositionEquippedItem> : SCE_ClientHandlerDefaults {
	static constexpr char const* LuaFunctionNameForZone = "objectRepositionEquippedItem";
	static constexpr char const* LuaFunctionNameForPlayer = "objectRepositionEquippedItemForPlayer";
	static void Handle(int placementId, GLID accessoryGlid, const Vector3f& ptBBoxRelativePosition, const Vector3f& vOffset, const Vector3f& vEulerZXY, const Vector3f& vScale);
};

template <>
struct SCE_ClientHandler<ScriptClientEvent::ObjectSetTriggerV2> : SCE_ClientHandlerDefaults {
	static constexpr char const* LuaFunctionNameForZone = "objectSetTriggerV2";
	static constexpr ScriptClientEvent::EventType GameStateModifierType = ScriptClientEvent::ObjectSetTrigger;
	static int GameStateModifierSubType(ScriptPIDType placementId, int triggerId, int triggerAction, const PrimitiveGeomArgs<float>& volume, int param) { return triggerId; }
	static void Handle(ScriptPIDType placementId, int triggerId, int triggerAction, const PrimitiveGeomArgs<float>& volume, int param);
};

template <>
struct SCE_ClientHandler<ScriptClientEvent::ObjectRemoveTriggerV2> : SCE_ClientHandlerDefaults {
	static constexpr char const* LuaFunctionNameForZone = "objectRemoveTriggerV2";
	static constexpr ScriptClientEvent::EventType GameStateModifierType = ScriptClientEvent::ObjectSetTrigger;
	static int GameStateModifierSubType(ScriptPIDType placementId, int triggerId) { return triggerId; }
	static void Handle(ScriptPIDType placementId, int triggerId);
};

////////////////////////////////////////////////////////////////////////////////////////
// SCE_ClientHandler iteration
// ForEachSCE_ClientHandler() - call a function for each specialized SCE_ClientHandler
//
// Define a template class/struct that takes a ScriptClientEvent::EventType and
// that has a static member function named Call(). Then simply call
// ForEachSCE_ClientHandler<MyTemplateFunction>()
//
// Example:
//
//  template<ScriptClientEvent::EventType eventType>
//  struct MyTemplateFunction {
//  	static void Call() { // Takes any arguments you want to pass through to it.
//  		// Do whatever you need to here
//  	}
//  };

// One item in range and that item has a specialization. Call function.
template <ScriptClientEvent::EventType eventTypeBegin, ScriptClientEvent::EventType eventTypeEnd, template <ScriptClientEvent::EventType> typename TemplateFunctorType, typename... Args>
inline std::enable_if_t<eventTypeBegin + 1 == eventTypeEnd && !std::is_base_of<Unspecialized_SCE_ClientHandlerBase, SCE_ClientHandler<eventTypeBegin>>::value>
RangeForEachSCE_ClientHandler(Args&&... args) {
	TemplateFunctorType<eventTypeBegin>::Call(std::forward<Args>(args)...);
}

// One item in range for a handler that is not specialized. Do nothing.
template <ScriptClientEvent::EventType eventTypeBegin, ScriptClientEvent::EventType eventTypeEnd, template <ScriptClientEvent::EventType> typename TemplateFunctorType, typename... Args>
inline std::enable_if_t<eventTypeBegin + 1 == eventTypeEnd && std::is_base_of<Unspecialized_SCE_ClientHandlerBase, SCE_ClientHandler<eventTypeBegin>>::value>
RangeForEachSCE_ClientHandler(Args&&... args) {}

// More than one item in range. Initialize first half of range then the second half.
template <ScriptClientEvent::EventType eventTypeBegin, ScriptClientEvent::EventType eventTypeEnd, template <ScriptClientEvent::EventType> typename TemplateFunctorType, typename... Args>
inline std::enable_if_t<(eventTypeEnd - eventTypeBegin >= 2)> RangeForEachSCE_ClientHandler(Args&&... args) {
	constexpr ScriptClientEvent::EventType eventTypeMid = ScriptClientEvent::EventType((eventTypeBegin + eventTypeEnd) / 2);
	RangeForEachSCE_ClientHandler<eventTypeBegin, eventTypeMid, TemplateFunctorType>(std::forward<Args>(args)...);
	RangeForEachSCE_ClientHandler<eventTypeMid, eventTypeEnd, TemplateFunctorType>(std::forward<Args>(args)...);
}

// End-user function to call.
// Template argument is a class with a static Call() member function.
// Arguments are forwarded to that function for each EventType with a specialization.
template <template <ScriptClientEvent::EventType> typename ClassWithCallFunction, typename... Args>
inline void ForEachSCE_ClientHandler(Args&&... args) {
	RangeForEachSCE_ClientHandler<ScriptClientEvent::EventType(0), ScriptClientEvent::NumEventTypes, ClassWithCallFunction>(std::forward<Args>(args)...);
}

} // namespace KEP