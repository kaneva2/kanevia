///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "TryOnInventoryEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(TryOnInventoryEvent)

TryOnInventoryEvent::TryOnInventoryEvent() :
		IEvent(ClassId()),
		type(0),
		m_glid(0),
		m_useType(USE_TYPE_NONE),
		m_placement_id(0) {
}

void TryOnInventoryEvent::InvokeAlternateInventoryMenu(int glid, const USE_TYPE& useType) {
	SetFilter(INVOKE_ALTERNATE_INVENTORY_MENU);
	(*OutBuffer()) << glid << (int)useType;
}

void TryOnInventoryEvent::InvokeExpiredInventoryMenu(int glid, const USE_TYPE& useType, int placementId) {
	SetFilter(INVOKE_EXPIRE_INVENTORY_MENU);
	(*OutBuffer()) << glid << (int)useType << placementId;
}

EVENT_CONSTRUCTOR_PROTOTYPE(TryOnInventoryEvent), type(0), m_glid(0), m_useType(USE_TYPE_NONE) {
}

void TryOnInventoryEvent::ExtractInfo() {
	m_glid = 0;
	m_useType = USE_TYPE_NONE;
	m_placement_id = 0;

	type = Filter();

	InBuffer();
	int useTypeInt = USE_TYPE_NONE;
	if (m_inBuffer) {
		switch (type) {
			case INVOKE_ALTERNATE_INVENTORY_MENU:
				(*m_inBuffer) >> m_glid >> useTypeInt;
				m_useType = (USE_TYPE)useTypeInt;
				break;

			case INVOKE_EXPIRE_INVENTORY_MENU:
				(*m_inBuffer) >> m_glid >> useTypeInt >> m_placement_id;
				m_useType = (USE_TYPE)useTypeInt;
				break;

			default:
				jsVerifyReturnVoid(false);
				break;
		}
	}
}
