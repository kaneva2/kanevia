#pragma once

#include "MySQLGetResultSetsEvent.h"

namespace KEP {
class IGetSet;

/// class for more generic getting one or more result sets from the database
/// using this to avoid creating new event id for each type of get from db
class GetResultSetsEvent : public MySQLGetResultSetsEvent {
public:
	// set as filter
	enum eType {
		PLAYER = 1,
		ACCOUNT
	};

	// item id will be set as objectId (player_id, acct_id, etc)

	// in and out
	IGetSet* item;

	DEFINE_DB_CLASS_INFO(GetResultSetsEvent, PackVersion(1, 0, 0, 0))

protected:
	GetResultSetsEvent(IN EVENT_ID id = ClassId(), IN ULONG filter = 0, IN OBJECT_ID objectId = 0, IN EVENT_PRIORITY priority = EP_NORMAL) :
			MySQLGetResultSetsEvent(id, filter, objectId, priority), item(0) {}
};

/// class for getting a player out of the database, used in background processing
class GetPlayerEvent : public GetResultSetsEvent {
public:
	GetPlayerEvent(NETID from, int playerId, int kanevaId,
		bool _isGm,
		ZoneIndex* oZone = 0,
		float oX = 0.0, float oY = 0.0, float oZ = 0.0,
		int rot = 0,
		bool _reconnected = false, bool _reconnecting = false, int /*_selectedChar*/ = -1,
		bool _goingToPlayer = false,
		const char* _url = "",
		IN EVENT_ID id = ClassId(), IN ULONG filter = 0, IN OBJECT_ID objectId = 0, IN EVENT_PRIORITY priority = EP_NORMAL) :
			isGm(_isGm),
			GetResultSetsEvent(id, filter, objectId, priority),
			overrideX(0.0),
			overrideY(0.0),
			overrideZ(0.0),
			rotation(0),
			reconnected(false),
			reconnecting(false),
			selectedChar(-1),
			goingToPlayer(false) {
		SetFrom(from);
		SetObjectId(playerId);
		SetFilter(PLAYER);
		kanevaUserId = kanevaId;
		if (oZone)
			overrideZone = *oZone;
		else
			overrideZone = INVALID_ZONE_INDEX;
		overrideX = oX;
		overrideY = oY;
		overrideZ = oZ;
		rotation = rot;
		reconnected = _reconnected;
		reconnecting = _reconnecting;
		selectedChar = selectedChar;
		goingToPlayer = _goingToPlayer;
		url = _url;
	}

	// passthru for player
	ZoneIndex overrideZone;
	float overrideX, overrideY, overrideZ;
	int rotation;
	bool reconnected;
	bool reconnecting;
	int selectedChar;
	bool goingToPlayer;
	std::string url;
	bool isGm;

	// output only
	std::vector<std::string> broadcastListArray;
};

/// class for getting an account out of the database, used in background processing
class GetAccountEvent : public GetResultSetsEvent {
public:
	GetAccountEvent(IGetSet* acct, NETID from, int kanevaId,
		const char* _name,
		const char* _currentIP,
		char _gender,
		const char* _birthDate,
		bool _showMature,
		IN EVENT_ID id = ClassId(), IN ULONG filter = 0, IN OBJECT_ID objectId = 0, IN EVENT_PRIORITY priority = EP_NORMAL) :
			GetResultSetsEvent(id, filter, objectId, priority),
			gender('U'),
			showMature(false) {
		SetFilter(ACCOUNT);
		item = acct;
		SetFrom(from);
		kanevaUserId = kanevaId;
		name = _name;
		currentIP = _currentIP;
		gender = _gender;
		birthDate = _birthDate;
		showMature = _showMature;
	}

	// passthru for acct
	std::string name;
	std::string currentIP;
	char gender;
	std::string birthDate;
	bool showMature;
};

} // namespace KEP
