///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "ParticleEvents.h"
#include "..\EventSystem\Dispatcher.h"

namespace KEP {

DECLARE_CLASS_INFO(AddParticleEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(AddParticleEvent) {
}

DECLARE_CLASS_INFO(RemoveParticleEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(RemoveParticleEvent) {
}

void RegisterParticleEvents(IDispatcher &disp) {
	RegisterEvent<AddParticleEvent>(disp);
	RegisterEvent<RemoveParticleEvent>(disp);
}

} // namespace KEP