///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "GotoPlaceEvent.h"
#include "Common/include/KEPHelpers.h"
using namespace KEP;

DECLARE_CLASS_INFO(GotoPlaceEvent)

GotoPlaceEvent::GotoPlaceEvent() :
		IEvent(ClassId()),
		m_type(GOTO_TYPE_NONE),
		m_destKanevaUserId(0),
		m_instanceId(0),
		m_port(0),
		m_charIndex(-1),
		m_x(0),
		m_y(0),
		m_z(0),
		m_rotation(0),
		m_parentGameId(0),
		m_childGameId(0),
		m_asAdmin(false) {
}

void GotoPlaceEvent::GotoPlayer(int playerServerId, LONG charIndex, const char* server, ULONG port) {
	SetFilter(GOTO_PLAYER);
	(*OutBuffer()) << playerServerId << charIndex;
	if (server != 0 && *server != '\0') {
		(*OutBuffer()) << server << port;
	} else {
		(*OutBuffer()) << "";
	}
}

void GotoPlaceEvent::GotoApartment(int playerServerId, LONG charIndex, const char* server, ULONG port) {
	SetFilter(GOTO_PLAYERS_APT);
	(*OutBuffer()) << playerServerId << charIndex;
	if (server != 0 && *server != '\0') {
		(*OutBuffer()) << server << port;
	} else {
		(*OutBuffer()) << "";
	}
}

// goto zone
void GotoPlaceEvent::GotoZone(ZoneIndex zi, ULONG instanceId, LONG charIndex, const char* server, ULONG port) {
	SetFilter(GOTO_ZONE);
	(*OutBuffer()) << zi.toLong()
				   << instanceId
				   << charIndex;
	if (server != 0 && *server != '\0') {
		(*OutBuffer()) << server << port;
	} else {
		(*OutBuffer()) << "";
	}
}
void GotoPlaceEvent::GotoZone(ZoneIndex zi, ULONG instanceId, LONG charIndex,
	double x, double y, double z, int rotation,
	const char* server, ULONG port) {
	SetFilter(GOTO_ZONE_COORDS);
	(*OutBuffer()) << zi.toLong()
				   << instanceId
				   << charIndex
				   << x << y << z << rotation;
	if (server != 0 && *server != '\0') {
		(*OutBuffer()) << server << port;
	} else {
		(*OutBuffer()) << "";
	}
}

// for STARs, like GotoZone, only don't specify valid destination zone.
void GotoPlaceEvent::GotoServer(LONG charIndex, const char* server, ULONG port) {
	jsAssert(server != 0);
	SetFilter(GOTO_SERVER);
	(*OutBuffer()) << charIndex << NULL_CK(server) << port;
}

void GotoPlaceEvent::GotoGame(LONG charIndex, const char* gameName, const char* url) {
	jsAssert(gameName != 0 && url != 0);
	SetFilter(GOTO_GAME);
	(*OutBuffer()) << charIndex << gameName << url;
}

// added 4/08
void GotoPlaceEvent::GotoUrl(LONG charIndex, const char* url, ULONG port, const char* server, LONG _parentGameId, LONG _childGameId) {
	jsAssert(url != 0 && strlen(url) > 0);
	m_childGameId = _childGameId;
	m_parentGameId = _parentGameId;
	SetFilter((ULONG)GOTO_URL);
	SetObjectId((OBJECT_ID)port);
	(*OutBuffer()) << charIndex << url;
	if (port > 0) {
		jsAssert(server != 0);
		(*OutBuffer()) << server;
	}
}

void GotoPlaceEvent::GotoTutorialZone(LONG charIndex) {
	SetFilter(GOTO_TUTORIAL_ZONE);
	(*OutBuffer()) << charIndex << ""; // empty server name since never sent to client
}

void GotoPlaceEvent::CloneForClient(const GotoPlaceEvent& src, bool reconnecting) {
	m_destKanevaUserId = 0;
	m_instanceId = 0;
	m_port = 0;
	m_charIndex = -1;
	m_childGameId = 0;
	m_parentGameId = 0;
	m_server.clear();
	m_zi = INVALID_ZONE_INDEX;
	m_type = (GOTO_TYPE)src.Filter();

	switch (src.Filter()) {
		case GOTO_PLAYER:
			GotoPlayer(src.m_destKanevaUserId, src.m_charIndex);
			break;

		case GOTO_PLAYERS_APT:
			GotoApartment(src.m_destKanevaUserId, src.m_charIndex);
			break;

		case GOTO_ZONE:
			GotoZone(src.m_zi, src.m_instanceId, src.m_charIndex);
			break;

		case GOTO_ZONE_COORDS:
			GotoZone(src.m_zi, src.m_instanceId, src.m_charIndex, src.m_x, src.m_y, src.m_z, src.m_rotation);
			break;

		case GOTO_TUTORIAL_ZONE:
			GotoTutorialZone(src.m_charIndex);
			break;

		case GOTO_SERVER:
			GotoServer(src.m_charIndex, src.m_server.c_str(), src.m_port);
			break;

		case GOTO_GAME:
			GotoGame(src.m_charIndex, src.m_gameName.c_str(), src.m_url.c_str());
			break;

		case GOTO_URL:
			GotoUrl(src.m_charIndex, src.m_url.c_str(), m_port, m_server.c_str(), m_parentGameId, m_childGameId);
			break;

		default:
			jsAssert(false);
			break;
	}
	SetObjectId((OBJECT_ID)(reconnecting ? 1 : 0));
}

EVENT_CONSTRUCTOR_PROTOTYPE(GotoPlaceEvent), m_type(GOTO_TYPE_NONE), m_destKanevaUserId(0), m_instanceId(0), m_port(0), m_charIndex(-1),
	m_x(0), m_y(0), m_z(0), m_rotation(0), m_parentGameId(0), m_childGameId(0), m_asAdmin(0) {
}

void GotoPlaceEvent::ExtractInfo() {
	m_destKanevaUserId = 0;
	m_instanceId = 0;
	m_port = 0;
	m_charIndex = -1;
	m_server.clear();
	m_zi = INVALID_ZONE_INDEX;
	m_type = (GOTO_TYPE)Filter();
	m_x = m_y = m_z = 0;
	m_rotation = 0;
	m_url.clear();

	InBuffer();
	if (m_inBuffer) {
		switch (m_type) {
			case GOTO_PLAYER:
			case GOTO_PLAYERS_APT:
				(*m_inBuffer) >> m_destKanevaUserId >> m_charIndex;
				break;

			case GOTO_ZONE: {
				LONG temp;
				(*m_inBuffer) >> temp >> m_instanceId >> m_charIndex;
				m_zi = ZoneIndex(temp);
			} break;

			case GOTO_ZONE_COORDS: {
				LONG temp;
				(*m_inBuffer) >> temp >> m_instanceId >> m_charIndex >> m_x >> m_y >> m_z >> m_rotation;
				m_zi = ZoneIndex(temp);
			} break;

			case GOTO_GAME:
				(*m_inBuffer) >> m_charIndex >> m_gameName >> m_url;
				break;

			case GOTO_TUTORIAL_ZONE:
			case GOTO_SERVER:
				(*m_inBuffer) >> m_charIndex;
				break;

			case GOTO_URL:
				(*m_inBuffer) >> m_charIndex >> m_url;
				m_port = ObjectId();
				if (m_port > 1) // client sends "reconnecting" as 0/1 in objectId
					(*m_inBuffer) >> m_server;
				else
					m_server.clear();
				break;

			default:
				jsVerifyReturnVoid(false);
				break;
		}

		if (m_charIndex >= 0) {
			if (m_type != GOTO_GAME && m_type != GOTO_URL) {
				(*m_inBuffer) >> m_server;
				if (!m_server.empty())
					(*m_inBuffer) >> m_port;
			}
		}
	}
}
