///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <SDKCommon.h>

namespace KEP {

class GenericInfoBoxEvent : public IEvent {
public:
	// Filters
	static const LONG MSG1 = 1;

	GenericInfoBoxEvent();

	// init fns
	void Msg1(const char* msg);

	void ExtractInfo();

	LONG type;

	std::string m_msg;

	DEFINE_SECURE_CLASS_INFO(GenericInfoBoxEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
