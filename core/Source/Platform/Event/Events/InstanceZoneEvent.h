///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include <vector>

namespace KEP {

class InstanceZoneEvent : public IEvent {
public:
	InstanceZoneEvent(bool creating, const char *worldName, LONG channel);

	void ExtractInfo(bool &creating, std::string &worldName, LONG &channel);

	DEFINE_SECURE_CLASS_INFO(InstanceZoneEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
