///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <string>

namespace KEP {

class ClientExitEvent : public IEvent {
public:
	ClientExitEvent(bool reconnectingToOtherServer = false) :
			IEvent(ClassId()) {
		SetFilter(reconnectingToOtherServer ? 1 : 0);
	}

	bool IsReconnecting() const { return Filter() != 0; }

	DEFINE_SECURE_CLASS_INFO(ClientExitEvent, PackVersion(1, 0, 0, 0), EP_HIGH)
};

} // namespace KEP
