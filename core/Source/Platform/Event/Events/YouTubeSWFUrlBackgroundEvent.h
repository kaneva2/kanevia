///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"

namespace KEP {

enum class YouTubeSWFUrlSource { DBOverride,
	WebCurrent };

class YouTubeSWFUrlBackgroundEvent : public IBackgroundDBEvent {
public:
	YouTubeSWFUrlBackgroundEvent(YouTubeSWFUrlSource source, EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority), m_source(source) {}

	std::string getUrl() const { return m_youtubeSWFUrl; }
	void setUrl(const std::string& url) { m_youtubeSWFUrl = url; }

	YouTubeSWFUrlSource getUrlSource() const { return m_source; }

private:
	// Result
	std::string m_youtubeSWFUrl;

	// Request
	YouTubeSWFUrlSource m_source;

	DEFINE_DB_CLASS_INFO(YouTubeSWFUrlBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP