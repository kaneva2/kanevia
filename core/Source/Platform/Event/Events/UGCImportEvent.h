///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

class IUGCImportStateObj;

namespace KEP {

class UGCImportEvent : public IEvent {
public:
	// No encoding - local event handled by Client C++ code only
	UGCImportEvent(IUGCImportStateObj* pStateObj, int type) :
			IEvent(ClassId(), (ULONG)type), m_pStateObj(pStateObj) {}

	void ExtractInfo(IUGCImportStateObj*& pStateObj, int& type) {
		pStateObj = m_pStateObj;
		type = (int)Filter();
	}

	DEFINE_SECURE_CLASS_INFO(UGCImportEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)

private:
	IUGCImportStateObj* m_pStateObj;
};

} // namespace KEP
