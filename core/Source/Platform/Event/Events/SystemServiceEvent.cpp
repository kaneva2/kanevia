///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "SystemServiceEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(SystemServiceEvent)

SystemServiceEvent::SystemServiceEvent(const std::string& userId) :
		IEvent(ClassId()) {
	(*OutBuffer()) << userId.c_str();
}

EVENT_CONSTRUCTOR_PROTOTYPE(SystemServiceEvent) {
}

void SystemServiceEvent::ExtractInfo(std::string& userId) {
	if (InBuffer()) {
		(*m_inBuffer) >> userId;
		m_inBuffer->Rewind();
	}
}
