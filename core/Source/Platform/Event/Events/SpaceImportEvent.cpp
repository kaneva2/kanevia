///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "SpaceImportEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(SpaceImportEvent)

SpaceImportEvent::SpaceImportEvent() :
		IEvent(ClassId()),
		m_newZoneIndex(0),
		m_importZoneIndex(0),
		m_importZoneInstanceId(0),
		m_maintainScripts(0) {
}

EVENT_CONSTRUCTOR_PROTOTYPE(SpaceImportEvent), m_newZoneIndex(0), m_importZoneIndex(0), m_importZoneInstanceId(0), m_maintainScripts(0) {
}

void SpaceImportEvent::ExtractInfo() {
	m_newZoneIndex = 0;
	m_importZoneIndex = 0;
	m_importZoneInstanceId = 0;
	m_maintainScripts = false;

	InBuffer();

	if (m_inBuffer) {
		(*m_inBuffer) >> m_newZoneIndex >> m_importZoneIndex >> m_importZoneInstanceId >> m_maintainScripts;
	}
}
