///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <js.h>

#include "P2pAnimationEvents.h"

using namespace KEP;

DECLARE_CLASS_INFO(P2pAnimRequestEvent)
EVENT_CONSTRUCTOR_PROTOTYPE(P2pAnimRequestEvent) {}

EVENT_CONSTRUCTOR_PROTOTYPE(P2pAnimPermissionEvent) {}
DECLARE_CLASS_INFO(P2pAnimPermissionEvent)

DECLARE_CLASS_INFO(P2pAnimPermissionReplyEvent)
EVENT_CONSTRUCTOR_PROTOTYPE(P2pAnimPermissionReplyEvent) {}

DECLARE_CLASS_INFO(P2pAnimEvent)
EVENT_CONSTRUCTOR_PROTOTYPE(P2pAnimEvent) {}
