///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"

namespace KEP {

class AvatarSelectionEvent : public IEvent {
	// Default Starter Apartment Identifiers (legacy contemporary condo)
	static const int DEFAULT_APT_GLID = 4649; ///< Default Apartment Deed GLID
	static const int DEFAULT_APT_ID = 46; ///< Default Apartment Zone Id

public:
	AvatarSelectionEvent(int avatarIndex, int aptGlid, int aptId);

	void ExtractInfo(int &avatarIndex, int &aptGlid, int &aptId);

	DEFINE_SECURE_CLASS_INFO(AvatarSelectionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline AvatarSelectionEvent::AvatarSelectionEvent(int avatarIndex, int aptGlid, int aptId) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)avatarIndex);

	// Pack Starter Apartment Identifiers
	(*OutBuffer()) << aptGlid << aptId;
}

inline void AvatarSelectionEvent::ExtractInfo(int &avatarIndex, int &aptGlid, int &aptId) {
	avatarIndex = ObjectId();

	// Assume Default Starter Apartment Identifiers
	aptGlid = DEFAULT_APT_GLID;
	aptId = DEFAULT_APT_ID;

	// Extract Starter Apartment Identifiers
	jsVerifyDo(m_inBuffer != 0, return );
	(*m_inBuffer) >> aptGlid >> aptId;
	m_inBuffer->Rewind();
}

} // namespace KEP
