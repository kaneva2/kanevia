///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "WorldObjectEvents.h"
#include "..\EventSystem\Dispatcher.h"

namespace KEP {

DECLARE_CLASS_INFO(UpdateWorldObjectTextureEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UpdateWorldObjectTextureEvent) {
}

void RegisterWorldObjectEvents(IDispatcher &disp) {
	RegisterEvent<UpdateWorldObjectTextureEvent>(disp);
}

} // namespace KEP