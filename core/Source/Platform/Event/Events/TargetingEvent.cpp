///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "TargetingEvent.h"
#include "..\EventSystem\Dispatcher.h"

namespace KEP {

DECLARE_CLASS_INFO(TargetedPositionEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(TargetedPositionEvent) {
}

DECLARE_CLASS_INFO(TargetedPositionEventEx)

EVENT_CONSTRUCTOR_PROTOTYPE(TargetedPositionEventEx) {
}

void RegisterTargetingEvents(IDispatcher &disp) {
	RegisterEvent<TargetedPositionEvent>(disp);
}

} // namespace KEP