///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Missing3DAppCharacterEvent.h"

#include <js.h>

namespace KEP {

DECLARE_CLASS_INFO(Missing3DAppCharacterEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(Missing3DAppCharacterEvent) {
}

Missing3DAppCharacterEvent::Missing3DAppCharacterEvent(LONG parentGameId, LONG appGameId) :
		IEvent(ClassId()) {
	(*OutBuffer()) << parentGameId << appGameId;
}

} // namespace KEP