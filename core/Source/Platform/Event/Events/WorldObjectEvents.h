///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <SDKCommon.h>
#include <string>

#include "Event\Base\IEvent.h"

namespace KEP {
class IDispatcher;

/***********************************************************
CLASS

	UpdateWorldObjectTextureEvent
	
	event that a new texture should be set in a world object
	sent from client

DESCRIPTION

***********************************************************/
class UpdateWorldObjectTextureEvent : public IEvent {
public:
	UpdateWorldObjectTextureEvent(std::string objSerialNumber, int textureAssetId, const char *textureUrl);

	void ExtractInfo(std::string &objSerialNumber, int &textureAssetId, std::string &textureUrl);

	DEFINE_CLASS_INFO(UpdateWorldObjectTextureEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

inline UpdateWorldObjectTextureEvent::UpdateWorldObjectTextureEvent(std::string objSerialNumber,
	int textureAssetId, const char *textureUrl) :
		IEvent(ClassId()) {
	SetFilter(textureAssetId);
	(*OutBuffer()) << objSerialNumber << textureUrl;
}

inline void UpdateWorldObjectTextureEvent::ExtractInfo(std::string &objSerialNumber, int &textureAssetId, std::string &textureUrl) {
	textureAssetId = Filter();
	(*InBuffer()) >> objSerialNumber >> textureUrl;
}

void RegisterWorldObjectEvents(IDispatcher &disp);

} // namespace KEP

