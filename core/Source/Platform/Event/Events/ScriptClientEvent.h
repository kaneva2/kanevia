///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Event/Base/IEvent.h>

#include <common\include\CoreStatic\InstanceId.h>
#include <common\include\MediaParams.h>

namespace KEP {

// Events Sent From Script Server To Client (ClientEngine::ScriptClientEventHandling())
class ScriptClientEvent : public IEvent {
public:
	//NOTE ON THIS ENUM: The python script on the server uses hardcoded values for these enums currently,
	//					 so please add any new event types to the end of the enum so as not to offset
	//					 the values in the python.
	enum EventType // Keep in sync with ScriptServerEventTypes.lua
	{
		InvalidEventType = -1,
		ScriptError = 0,
		PlayerTell = 1,
		PlayerTeleport = 2,
		PlayerLoadMenu = 3,
		PlayerSendEvent = 4,
		ObjectGenerate = 5,
		ObjectMove = 6,
		PlayerGetLocation = 7,
		ObjectDelete = 8,
		ObjectSetTexture = 9,
		PlayerUseItem = 10,
		PlayerCheckInventory = 11,
		PlayerRemoveItem = 12,
		GameZonesInfo = 13,
		GameInstanceInfo = 14,
		CreateInstance = 15,
		PlayerJoinInstance = 16,
		LockInstance = 17,
		PlayerSetAnimation = 18,
		ObjectControl = 19,
		PlayerDefineAnimation = 20,
		ObjectSetTrigger = 21,
		ObjectRemoveTrigger = 22,
		PlayerGotoUrl = 23,
		PlayerSetPhysics = 24,
		PlayerSetCamera = 25, // deprecated
		PlayerMove = 26,
		GetAllScripts = 27,
		ObjectSetParticle = 28,
		ObjectRemoveParticle = 29,
		PlayerSetTitle = 30,
		ObjectRotate = 31,
		PlayerEquipItem = 32,
		PlayerUnEquipItem = 33,
		PlayerSetEquippedItemAnimation = 34,
		PlayerSetName = 35,
		ObjectSetTexturePanning = 36,
		PlayerSetVisibilityFlag = 37,
		PlayerSetParticle = 38,
		PlayerRemoveParticle = 39,
		ObjectScale = 40,
		PlayerScale = 41,
		PlayerCastRay = 42,
		PlayerCameraAttach = 43, // deprecated
		PlayerCameraDetach = 44, // deprecated
		PlayerCameraMove = 45, // deprecated
		ScriptSetEnvironment = 46,
		ScriptSetDayState = 47,
		PlayerLookAt = 48,
		ObjectLookAt = 49,
		SoundAttachTo = 50,
		SoundPlayOnClient = 51,
		Unused_52 = 52,
		Unused_53 = 53,
		ObjectGetNextPlaylistItem = 54,
		ObjectSetNextPlaylistItem = 55,
		ObjectEnumeratePlaylist = 56,
		ObjectSetPlaylist = 57,
		ObjectAddLabels = 58,
		ObjectClearLabels = 59,
		RenderLabelsOnTop = 60,
		ObjectSetOutline = 61,
		ObjectSetFlash = 62,
		SoundStopOnClient = 63,
		DownloadAsset = 64,
		PublishAssetCompleted = 65,
		PublishDeleteAssetCompleted = 66,
		PlayerAddItem = 67,
		PublishResponseEvent = 68,
		ObjectSetCollision = 69,
		ObjectMoveAndRotate = 70,
		ObjectSetMouseOver = 71,
		PlayerOpenYesNoMenu = 72,
		PlayerOpenKeyListenerMenu = 73,
		PlayerClosePresetMenu = 74,
		PlayerOpenTextTimerMenu = 75,
		PlayerOpenButtonMenu = 76,
		PlayerOpenStatusMenu = 77,
		PlayerMovePresetMenu = 78,
		PlayerOpenListBoxMenu = 79,
		PlayerAddIndicator = 80,
		PlayerClearIndicators = 81,
		PlayerUpdateCoinHUD = 82,
		PlayerUpdateHealthHUD = 83,
		PlayerUpdateXPHUD = 84,
		PlayerAddHealthIndicator = 85,
		PlayerRemoveHealthIndicator = 86,
		PlayerOpenShop = 87,
		PlayerAddItemToShop = 88,
		PlayerShowStatusInfo = 89,
		PlayerSetCustomTextureOnAccessory = 90,
		PlayerSetEquipped = 91,
		PlayerRemoveAllAccessories = 92,
		PlaceGameItem = 93,
		ObjectSetDrawDistance = 94,
		PlayerResetCursorModesTypes = 95,
		PlayerSetCursorModeType = 96,
		PlayerSetCursorMode = 97,
		PlayerSetEffectTrack = 98,
		ObjectSetEffectTrack = 99,
		PlayerSaveZoneSpawnPoint = 100,
		PlayerDeleteZoneSpawnPoint = 101,
		PlayerSaveEquipped = 102, // drf - added
		PlayerGetEquipped = 103, // drf - added
		ObjectGetStartLocation = 104,
		OwnedObjectGenerate = 105, // Mo/Jo -- added 7/28/2015. Same as ObjectGenerate, except includes the owner ID of the object.
		ObjectSetMedia = 106, // drf - added
		ObjectSetMediaVolumeAndRadius = 107, // drf - added
		ReplaceGameItem = 108,
		PlayerSpawnVehicle = 109, // Mo/Jo -- Added 10/20/2015
		PlayerLeaveVehicle = 110, // Mo/Jo -- Added 10/20/2015
		SoundAddModifier = 111, // drf - added
		NPCSpawn = 112, // drf - added
		PlayerModifyVehicle = 113,
		GetItemInfo = 114,
		ObjectEquipItem = 115,
		ObjectUnEquipItem = 116,
		ObjectSetEquippedItemAnimation = 117,
		ObjectRepositionEquippedItem = 118,
		ObjectSetTriggerV2 = 119,
		ObjectRemoveTriggerV2 = 120,
		// Also update ScriptClientEventTypes.lua with new types

		NumEventTypes
	};

	enum DestinationScope {
		Scope_Zone = 0,
		Scope_Player = 1,
	};

	enum RoutingOption {
		Routing_Normal = 0, // Standard routing option - send with the most efficient way (if From() is specified, use it as destination, otherwise send through server engine)
		Routing_ServerEngine = 1, // Always go through server engine
		Routine_Client = 2, // Always send to client directly
	};

	ScriptClientEvent(EventType eventType, NETID clientNetId, DestinationScope destScope = Scope_Zone);
	ScriptClientEvent(ScriptClientEvent* sce, DestinationScope destScope = Scope_Player);

	void SetEventType(EventType eventType) { SetFilter((ULONG)eventType); }
	EventType GetEventType() const { return (EventType)Filter(); }

	void SetScope(DestinationScope scope) { m_destinationScope = scope; }
	DestinationScope GetScope() const { return m_destinationScope; }

	void SetRoutingOption(RoutingOption opt) { m_routingOption = opt; }
	RoutingOption GetRoutingOption() const { return m_routingOption; }

	DEFINE_SECURE_CLASS_INFO(ScriptClientEvent, PackVersion(1, 0, 0, 0), EP_NORMAL);

	static ScriptClientEvent* EncodeObjectGenerate(ScriptClientEvent* sce, ULONG objId, ULONG globalId, LONG zoneId, LONG instanceId, double x, double y, double z, double dx, double dy, double dz, int placementType, int gameItemId);
	static void DecodeObjectGenerate(ScriptClientEvent* sce, ULONG& objId, ULONG& globalId, LONG& zoneId, LONG& instanceId, double& x, double& y, double& z, double& dx, double& dy, double& dz, int& placementType, int& gameItemId);

	static ScriptClientEvent* EncodeOwnedObjectGenerate(ScriptClientEvent* sce, ULONG objId, ULONG globalId, LONG zoneId, LONG instanceId, double x, double y, double z, double dx, double dy, double dz, int placementType, int gameItemId, int ownerId);
	static void DecodeOwnedObjectGenerate(ScriptClientEvent* sce, ULONG& objId, ULONG& globalId, LONG& zoneId, LONG& instanceId, double& x, double& y, double& z, double& dx, double& dy, double& dz, int& placementType, int& gameItemId, int& ownerId);

	static ScriptClientEvent* EncodeObjectMove(ScriptClientEvent* sce, ULONG objId, LONG zoneId, LONG instanceId, double x, double y, double z, double timeMs, double accDur, double constVelDur, double accDistFrac, double constVelDistFrac);
	static void DecodeObjectMove(ScriptClientEvent* sce, ULONG& objId, LONG& zoneId, LONG& instanceId, double& x, double& y, double& z, double& timeMs, double& accDur, double& constVelDur, double& accDistFrac, double& constVelDistFrac);

	static ScriptClientEvent* EncodeObjectRotate(ScriptClientEvent* sce, ULONG objId, LONG zoneId, LONG instanceId, double dx, double dy, double dz, double sx, double sy, double sz, double timeMs);
	static void DecodeObjectRotate(ScriptClientEvent* sce, ULONG& objId, LONG& zoneId, LONG& instanceId, double& dx, double& dy, double& dz, double& sx, double& sy, double& sz, double& timeMs);

	static ScriptClientEvent* EncodeObjectDelete(ScriptClientEvent* sce, ULONG objId, LONG zoneId, LONG instanceId);
	static void DecodeObjectDelete(ScriptClientEvent* sce, ULONG& objId, LONG& zoneId, LONG& instanceId);

	static ScriptClientEvent* EncodeObjectSetParticle(ScriptClientEvent* sce, ULONG clientId, ULONG objId, LONG zoneId, LONG instanceId, int glid, const std::string& bone, int slotId, double ofsX, double ofsY, double ofsZ, double dirX, double dirY, double dirZ, double upX, double upY, double upZ);
	static void DecodeObjectSetParticle(ScriptClientEvent* sce, ULONG& clientId, ULONG& objId, LONG& zoneId, LONG& instanceId, int& glid, std::string& bone, int& slotId, double& ofsX, double& ofsY, double& ofsZ, double& dirX, double& dirY, double& dirZ, double& upX, double& upY, double& upZ);

	static ScriptClientEvent* EncodeObjectRemoveParticle(ScriptClientEvent* sce, ULONG clientId, ULONG objId, LONG zoneId, LONG instanceId, const std::string& bone, int slotId);
	static void DecodeObjectRemoveParticle(ScriptClientEvent* sce, ULONG& clientId, ULONG& objId, LONG& zoneId, LONG& instanceId, std::string& bone, int& slotId);

	static ScriptClientEvent* EncodeSoundAttachTo(ScriptClientEvent* sce, ULONG soundPlacementId, ULONG targetId, int attachType, LONG zoneId);
	static void DecodeSoundAttachTo(ScriptClientEvent* sce, ULONG& soundPlacementId, ULONG& targetId, int& attachType, LONG& zoneId);

	// DRF - Added
	static ScriptClientEvent* EncodeSoundAddModifier(ScriptClientEvent* sce, ZoneIndex zoneId, int soundPlacementId, int input, int output, double gain, double offset);
	static void DecodeSoundAddModifier(ScriptClientEvent* sce, ZoneIndex& zoneId, int& soundPlacementId, int& input, int& output, double& gain, double& offset);

	// DRF - Added
	static ScriptClientEvent* EncodeObjectSetMedia(ScriptClientEvent* sce, ZoneIndex zoneId, ULONG objId, const MediaParams& mediaParams);
	static void DecodeObjectSetMedia(ScriptClientEvent* sce, ZoneIndex& zoneId, ULONG& objId, MediaParams& mediaParams);

	// DRF - Added
	static ScriptClientEvent* EncodeObjectSetMediaVolumeAndRadius(ScriptClientEvent* sce, ZoneIndex zoneId, ULONG objId, const MediaParams& mediaParams);
	static void DecodeObjectSetMediaVolumeAndRadius(ScriptClientEvent* sce, ZoneIndex& zoneId, ULONG& objId, MediaParams& mediaParams);

private:
	DestinationScope m_destinationScope;
	RoutingOption m_routingOption;
};

} // namespace KEP