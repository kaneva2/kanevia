///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"
#include <instanceid.h>
#include <string>

namespace KEP {

class CPlayerObject;

/// class for passing params back for forth to DB background thd
class ChannelImportBackgroundEvent : public IBackgroundDBEvent {
public:
	ChannelImportBackgroundEvent(EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority), playerNetId(0), player_id(0), maintainScripts(0), replyEvent(0) {}

	// inbound only
	NETID playerNetId;
	int player_id; // See import_channel SP for details about the parameters
	int dstZoneIndex; // Zone index (zone index plain + zone type) of the zone being copied into (original destination)
	int dstInstanceId;
	int baseZoneIndexPlain; // Zone index plain (== srcZoneIndex.plain()) to be used to construct final zone index (new destination)
	int srcZoneIndex; // Zone index (zone index plain + zone type) of the zone to be copied from (source)
	int srcInstanceId;
	bool maintainScripts;

	CPlayerObject* player;

	// outbound only
	int ret;
	IEvent* replyEvent;

	DEFINE_DB_CLASS_INFO(ChannelImportBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP

