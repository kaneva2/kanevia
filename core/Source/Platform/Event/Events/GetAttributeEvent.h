///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "common\include\KEPCommon.h"

#include "Event\Base\IEvent.h"
#include "Event\Events\AttribEvents.h"

namespace KEP {

class GetAttributeEvent : public IEvent {
public:
	GetAttributeEvent(const ATTRIB_DATA& attribData);

	void GetValues(ATTRIB_DATA& attribData);

	DEFINE_SECURE_CLASS_INFO(GetAttributeEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
