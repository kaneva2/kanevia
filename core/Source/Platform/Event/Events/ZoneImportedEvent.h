///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "Tools/NetworkV2/NetworkTypes.h"
#include "InstanceId.h"

namespace KEP {

class ZoneImportedEvent : public IEvent {
public:
	ZoneImportedEvent(const ZoneIndex& zoneIndex, NETID netId) :
			IEvent(ClassId()) {
		SetObjectId(netId);
		SetFilter(zoneIndex.toLong());
	}

	void ExtractInfo(ZoneIndex& zoneIndex, NETID& netId) {
		zoneIndex.fromLong(static_cast<long>(Filter()));
		netId = ObjectId();
	}

	DEFINE_SECURE_CLASS_INFO(ZoneImportedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
