///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ZoneCustomizationSummaryEvent.h"

namespace KEP {

DECLARE_CLASS_INFO(ZoneCustomizationSummaryEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(ZoneCustomizationSummaryEvent) {
	// do nothing here
}

} // namespace KEP
