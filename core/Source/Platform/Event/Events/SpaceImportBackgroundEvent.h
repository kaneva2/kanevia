///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"
#include <instanceid.h>

namespace KEP {

/// class for passing params back for forth to DB background thd
class SpaceImportBackgroundEvent : public IBackgroundDBEvent {
public:
	SpaceImportBackgroundEvent(EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority),
			newZoneIndex(0),
			importZoneIndex(0),
			importZoneInstanceId(0),
			maintainScripts(0),
			playerId(0),
			currentZoneIndex(0),
			replyEvent(0) {}

	int newZoneIndex;
	int importZoneIndex;
	int importZoneInstanceId;
	bool maintainScripts;
	int playerId;
	int currentZoneIndex;

	// outbound only
	int ret;
	IEvent *replyEvent;

	DEFINE_DB_CLASS_INFO(SpaceImportBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP