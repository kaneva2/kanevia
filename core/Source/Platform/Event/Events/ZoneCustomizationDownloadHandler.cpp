///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ZoneCustomizationDownloadHandler.h"

#include "Event/EventSystem/Dispatcher.h"
#include "SoundEvents.h"
#include "DynamicObjectEvents.h"
#include "ParticleEvents.h"
#include "WorldObjectEvents.h"
#include "GenericEvent.h"
#include "ZoneCustomizationSummaryEvent.h"
#include "UrlParser.h"
#include "PlacementSource.h"

#include "KEPConstants.h"
#include "param_ids.h"
#include "matrixarb.h"
#include "LogHelper.h"

// Un-define min/max macros from windef.h so that we can use the ones from standard library
#undef min
#undef max

namespace KEP {

#define DO_FINAL_INDEX 25
#define DO_PARAM_INDEX DO_FINAL_INDEX + 1

ZoneCustomizationDownloadHandler::ZoneCustomizationDownloadHandler(Dispatcher &disp, Logger &logger) :
		m_disp(disp),
		m_logger(logger),
		m_serverDynObjectEventId(BAD_EVENT_ID),
		m_state(PROCESSING),
		m_currentCount(0),
		m_rc(-1),
		m_totalObjects(-1),
		m_totalWorldObjectCustomizations(-1),
		m_soundPlacements(0) // drf - added
{
	m_serverDynObjectEventId = disp.RegisterEvent("ServerDynObjReplyEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_buffer = new char[BUFF_SIZE];
	init();
}

EVENT_PROC_RC ZoneCustomizationDownloadHandler::ProcessEvent(IDispatcher * /*disp*/, IEvent *e) {
	if (e->EventId() == m_serverDynObjectEventId) {
		std::string s;
		(*e->InBuffer()) >> s;
		BytesDownloaded(const_cast<char *>(s.c_str()), s.size());
		return EVENT_PROC_RC::OK;
	}
	return EVENT_PROC_RC::NOT_PROCESSED;
}

// DRF - CRASH FIX - This function expects null terminated _bytes!!!
bool ZoneCustomizationDownloadHandler::BytesDownloaded(char *_bytes, ULONG len) {
	char *p = 0;
	char *bytes = _bytes;
	char *curPtr = bytes;

	while (m_state != ALL_DONE && *curPtr) {
		switch (m_state) {
			case PROCESSING: {
				char *endP = strchr(curPtr, '\n');
				if (endP == 0) {
					m_state = BUFFER_SPLIT;
					if ((bytes + len) - curPtr < BUFF_SIZE) {
						strncpy_s(m_buffer, BUFF_SIZE, curPtr, len - (curPtr - bytes));
						m_buffer[len - (curPtr - bytes)] = '\0';
						return true;
					} else {
						LogWarn("Got bad buffer split, state is " << m_state);
						LogWarn("    " << curPtr);
						jsAssert(false); // bad news
						return false;
					}
				} else {
					// parse all the data and fire the event
					*endP = 0; // null terminate line
					processBuffer(curPtr);

					curPtr = endP + 1; // jump over eol
				}
			} break;

			case BUFFER_SPLIT:
				if ((p = strchr(curPtr, '\n')) != 0) {
					p++; // jump over \n

					// fill out rest of split one, then go one
					strncat_s(m_buffer, BUFF_SIZE, curPtr, p - bytes);

					processBuffer(m_buffer);

					curPtr = p;
					m_state = PROCESSING;
				} else {
					// case where we don't get end tag in next chunk
					if (strlen(curPtr) + strlen(m_buffer) < BUFF_SIZE) {
						LogInfo("Got small chunk and will append. Lens are: " << strlen(curPtr) << "," << strlen(m_buffer) << "," << BUFF_SIZE);
						strncat_s(m_buffer, BUFF_SIZE, curPtr, _TRUNCATE);
						LogInfo("   new buffer is: " << m_buffer);
						*curPtr = 0;
					} else {
						LogWarn("Got bad buffer after split, state is " << m_state);
						LogWarn("    " << curPtr);
						jsAssert(false); // bad news
						return false;
					}
				}
				break;

			case ALL_DONE:
				// do nothing
				LogDebug("Finished with count of " << m_currentCount);
				break;
		}
	}

	if (m_state == ALL_DONE) {
		LogInfo("Zone customization finished, total: " << m_currentCount);

		//Fire event here to notify zone customization is done.
		//This is handled by python on the server. Currently PlaylistManager.py is the only one to use.
		EVENT_ID eId = m_disp.GetEventId("ZoneCustomizationDLCompleteEvent");
		if (eId != BAD_EVENT_ID) {
			IEvent *e = m_disp.MakeEvent(eId);
			if (e) {
				// DRF - ED-4177 - DynamicObjectSpawnMonitor Bug
				int totalObjects = std::max(m_totalObjects, 0);
				int dynamicObjects = std::max(totalObjects - m_soundPlacements, 0);
				int soundPlacements = std::max(m_soundPlacements, 0);

				// Send to client-side handler
				e = m_disp.MakeEvent(eId);
				e->SetFilter(totalObjects);
				(*e->OutBuffer()) << dynamicObjects << soundPlacements;
				m_disp.QueueEvent(e);
			}
		}
		init();
	}

	return true;
}

void ZoneCustomizationDownloadHandler::init() {
	// init all variables to start state
	m_state = PROCESSING;
	m_currentCount = 0;
	m_rc = -1;
	m_totalObjects = -1;
	m_totalWorldObjectCustomizations = -1;
	m_soundPlacements = 0; // drf - added
}

int ZoneCustomizationDownloadHandler::parseParams(char *ptr) {
	int i = 0;
	m_parsedTokens[i] = 0;

	while (ptr) {
		m_parsedTokens[i++] = ptr;
		ptr = strchr(ptr, '\t');
		if (ptr) {
			*ptr = 0;
			ptr++; // skip tab
		}
		if (i >= _countof(m_parsedTokens)) {
			jsAssert(false);
			return i;
		}
	}
	m_parsedTokens[i] = 0; // null term list
	return i; // count
}

void ZoneCustomizationDownloadHandler::processBuffer(char *curPtr) {
	switch (*curPtr) {
		case DYN_OBJ_CODE:
			processDynamic(curPtr);
			break;
		case WORLD_CODE:
			processWorld(curPtr);
			break;
		case RESULT_CODE:
			processReturnCode(curPtr);
			break;
		case END_CODE:
			m_state = ALL_DONE;
			break;
		default:
			LogError("ZoneCustomHandler got unknown code in buffer " << curPtr);
			break;
	}
}

void ZoneCustomizationDownloadHandler::processReturnCode(char *curPtr) {
	int tokens = parseParams(curPtr);
	if (tokens > 2) {
		// Format: "r <code> <message> <numDynamicObjects> <numWorldObjectCustomizations> <frameworkEnabled>"
		// Example: "r 0 ok 16 22 1"
		if (*m_parsedTokens[1] == '0') {
			m_rc = 0;
			LogInfo("tokens=" << tokens << " rc=" << m_parsedTokens[1]);

			if (tokens > 5) {
				// Dynamic object and world object settings count
				int tmp;
				tmp = atoi(m_parsedTokens[3]);
				assert(tmp >= 0);
				size_t totalObjects = (size_t)std::max(0, tmp);

				tmp = atoi(m_parsedTokens[4]);
				assert(tmp >= 0);
				size_t worldObjectSettingCount = (size_t)std::max(0, tmp);

				int frameworkEnabledFlag = 0; // Use int to allow transparent pass-thru of wokweb data
				frameworkEnabledFlag = atoi(m_parsedTokens[5]);

				m_totalObjects = totalObjects;
				m_totalWorldObjectCustomizations = worldObjectSettingCount;

				unsigned ddsTextureSize = 0, meshSize = 0, meshInstSize = 0;
				if (tokens > 8) {
					ddsTextureSize = atoi(m_parsedTokens[6]);
					meshSize = atoi(m_parsedTokens[7]);
					meshInstSize = atoi(m_parsedTokens[8]);
				}

				// Send summary event
				LogInfo("rc=" << m_rc << " totalObjects=" << totalObjects << " worldObjectSettings=" << worldObjectSettingCount << " frameworkEnabled=" << frameworkEnabledFlag);
				LogInfo("ddsTextureSize=" << ddsTextureSize << " mashSize=" << meshSize << " meshInstSize=" << meshInstSize);
				m_disp.QueueEvent(new ZoneCustomizationSummaryEvent(m_rc, totalObjects, worldObjectSettingCount, frameworkEnabledFlag, ddsTextureSize, meshSize, meshInstSize));
			} else {
				LogWarn("TOKENS < 6 - tokens=" << tokens);

				// Send summary event
				m_disp.QueueEvent(new ZoneCustomizationSummaryEvent(PARSING_ERROR, 0, 0, 0, 0, 0, 0));
			}
		} else {
			LogWarn("BAD TOKENS - token[1]=" << m_parsedTokens[1] << " token[2]=" << m_parsedTokens[2]);
			m_state = ALL_DONE; // error code!
		}
	} else {
		LogWarn("TOKENS < 3 - tokens=" << tokens);
		m_state = ALL_DONE; // error code!
	}
}

// DRF - TODO - Need To Add animSettings !!!
void ZoneCustomizationDownloadHandler::processDynamic(char *buff) {
	jsVerifyReturnVoid(m_rc == 0);

	/* 
		buff is tab delimiter values
		    concat( 'd\t',
		    `obj_placement_id`, '\t',
		    `object_id`, '\t', 
		    player_id, '\t', 
		    `position_x`, '\t', 
		    `position_y`, '\t', 
		    `position_z`, '\t', 
		    `direction_x`, '\t', 
		    `direction_y`, '\t', 
		    `direction_z`, '\t', 
		    `slide_x`, '\t', 
		    `slide_y`, '\t', 
		    `slide_z`, '\t', 
		    do.global_id, '\t', 
		    attachable, '\t', 
		    interactive, '\t', 
		    playFlash, '\t', 
		    collision, '\t', 
		    min_x, '\t', 
		    min_y, '\t', 
		    min_z, '\t', 
		    max_x, '\t', 
		    max_y, '\t', 
		    max_z, '\t', 
		    is_derivable, '\t', 
		    do.inventory_sub_type, '\n' ) as d
			
	*/

	m_currentCount++;

	int numParams = parseParams(buff);

	//
	// Code commented that will go in when the web
	// is updated to send the additional vector to the
	// client.
	//
	jsVerifyReturnVoid(numParams > DO_FINAL_INDEX);
	jsAssert(*m_parsedTokens[0] == DYN_OBJ_CODE);

	long placementId = atol(m_parsedTokens[1]);
	//long objectId	= atol(m_parsedTokens[2]);	// deprecated
	long playerId = atol(m_parsedTokens[3]);
	float x = (float)atof(m_parsedTokens[4]);
	float y = (float)atof(m_parsedTokens[5]);
	float z = (float)atof(m_parsedTokens[6]);
	float dx = (float)atof(m_parsedTokens[7]);
	float dy = (float)atof(m_parsedTokens[8]);
	float dz = (float)atof(m_parsedTokens[9]);
	float sx = (float)atof(m_parsedTokens[10]);
	float sy = (float)atof(m_parsedTokens[11]);
	float sz = (float)atof(m_parsedTokens[12]);
	//
	// If sz + sy + sz == 0 then this is an 'old' DO
	// that didn't store the slide vector.  We can
	// safely assume that this DO's up vector is, indeed,
	// pointing up and calculate a proper slide vector.
	// If the DO is saved again, the slide vector will
	// be stored in the Db.
	//
	if (sx + sy + sz == 0) {
		Vector3f up;
		up.x = 0;
		up.y = 1.f;
		up.z = 0;
		Vector3f dir;
		dir.x = dx;
		dir.y = dy;
		dir.z = dz;
		// {sx, sy, sz} = up x dir
		sx = up.y * dir.z - up.z * dir.y;
		sy = up.z * dir.x - up.x * dir.z;
		sz = up.x * dir.y - up.y * dir.x;
	}
	long globalId = atol(m_parsedTokens[13]);
	bool attachable = *m_parsedTokens[14] == '1';
	bool interactive = *m_parsedTokens[15] == '1';
	bool playFlash = *m_parsedTokens[16] == '1';
	bool collision = *m_parsedTokens[17] == '1';

	float min_x = (float)atof(m_parsedTokens[18]);
	float min_y = (float)atof(m_parsedTokens[19]);
	float min_z = (float)atof(m_parsedTokens[20]);

	float max_x = (float)atof(m_parsedTokens[21]);
	float max_y = (float)atof(m_parsedTokens[22]);
	float max_z = (float)atof(m_parsedTokens[23]);

	bool derivable = *m_parsedTokens[24] == '1';
	long inventory_sub_type = atol(m_parsedTokens[25]);
	// see if we have any params
	std::string paramstr;
	std::string swfParams;
	std::string swf;
	std::string url;
	long friendId = 0;
	long textureAssetId = 0;
	GLID animGlidSpecial = GLID_INVALID;
	GLID particleGLID = GLID_INVALID;
	long gameItemId = INVALID_GAMEITEM;
	long soundId = 0;

	//Anything after the final dynamic object index (inv sub type) will be
	//parameter type/value pairs.  Unpack them into corresponding attributes.
	//If we somehow have an odd number after the base count skip the last one.
	for (int i = DO_PARAM_INDEX; (i + 1) < numParams; i = i + 2) {
		switch (atol(m_parsedTokens[i])) {
			case PARAM_SWF_NAME:
				swf = m_parsedTokens[i + 1];
				break;

			case PARAM_FRIEND_ID:
				friendId = atol(m_parsedTokens[i + 1]);
				break;

			case PARAM_SWF_PARAMETER:
				swfParams = m_parsedTokens[i + 1];
				break;

			case PARAM_TEXTURE_ASSET_ID:
				textureAssetId = atol(m_parsedTokens[i + 1]);
				break;

			case PARAM_TEXTURE_URL:
				url = m_parsedTokens[i + 1];
				break;

			case PARAM_ANIMATION_GLID: // < 0 = reverse animation
				animGlidSpecial = (GLID)atol(m_parsedTokens[i + 1]);
				break;

			case PARAM_PARTICLE_GLID:
				particleGLID = (GLID)strtoul(m_parsedTokens[i + 1], NULL, 10);
				break;

			case PARAM_GAME_ITEM_ID:
				gameItemId = atol(m_parsedTokens[i + 1]);
				break;

			case PARAM_SOUND:
				++m_soundPlacements; // drf - added
				soundId = atol(m_parsedTokens[i + 1]);
				m_disp.QueueEvent(new AddSoundEvent(PLACEMENT_ZONECUSTDL, soundId, static_cast<int>(ObjectType::DYNAMIC), placementId, 0, x, y, z));
				return; //right now if there is a sound parameter that is the only thing on the DO.  This will hopefully change in the near future.

			default:
				LogDebug("Ignoring unknown parameter id " << m_parsedTokens[i] << " value " << m_parsedTokens[i + 1]);
				break;
		}
	}

	unEscapeXml(swfParams);
	unEscapeXml(swf);
	unEscapeXml(url);

	// for the old KGPPlaylist we prepended with playlistUrl, only do it for those
	if (STLCompareIgnoreCase(swf.c_str(), "KGPPlaylist.swf") == 0) {
		swfParams = UrlParser::escapeString(swfParams);
		swfParams = std::string("playlistUrl=") + swfParams;
	}

	m_disp.QueueEvent(new AddDynamicObjectEvent(PLACEMENT_ZONECUSTDL, playerId, globalId, x, y, z, dx, dy, dz, sx, sy, sz, placementId, animGlidSpecial, textureAssetId, url.c_str(), friendId, swf.c_str(), swfParams.c_str(), attachable, interactive, playFlash, collision, min_x, min_y, min_z, max_x, max_y, max_z, derivable, inventory_sub_type, gameItemId));

	if (particleGLID != GLID_INVALID) {
		// Basic "use particle" scenario (as of March 2011 - YC):
		//		Add a particle at DO pivot point (offset=0,0,0), facing positive X, up is positive Y
		// We will improve this with offset and orientations once particle is open to public and build-mode particle adjustment UI available.
		// Additional particle parameters will need to be recorded in DO_PARAMs table and delivered with zone customization data.
		// See also: MySqlEventProcessor::updateDynamicObjectParticle (MySqlEventProcessor.cpp).
		m_disp.QueueEvent(new AddParticleEvent(particleGLID, ObjectType::DYNAMIC, placementId, "", 0, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f));
	}

	LogTrace("ZoneCustomHandler adding dyn object #" << m_currentCount << " id " << placementId);
}

void ZoneCustomizationDownloadHandler::processWorld(char *buff) {
	jsVerifyReturnVoid(m_rc == 0);

	/* also get the custom world objects
			SELECT concat( 'w\t', w.world_object_id, '\t',  w.asset_id, '\t', w.texture_url, '\n'  ) as d
	*/
	m_currentCount++;

	jsVerifyReturnVoid(parseParams(buff) > 3);
	jsAssert(*m_parsedTokens[0] == WORLD_CODE);

	std::string objectId = m_parsedTokens[1];
	long assetId = atol(m_parsedTokens[2]);
	std::string textureUrl = m_parsedTokens[3];

	m_disp.QueueEvent(new UpdateWorldObjectTextureEvent(objectId.c_str(), assetId, textureUrl.c_str()));

	LogTrace("ZoneCustomHandler setting custom texture #" << m_currentCount << " url " << textureUrl << " on " << objectId);
}

} // namespace KEP