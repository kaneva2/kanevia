///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "EventSyncEvent.h"

#include "Event/Base/IEncodingFactory.h"
#include <KEPException.h>

using namespace KEP;

DECLARE_CLASS_INFO(EventSyncEvent)

EventSyncEvent::EventSyncEvent(const EventMap &eventMap, const ULONG characterCount) :
		IEvent(ClassId()) {
	SetFrom(SERVER_NETID); // always from server
	eventMap.streamTo(*(OutBuffer()));
	(*OutBuffer()) << characterCount;
}

EVENT_CONSTRUCTOR_PROTOTYPE(EventSyncEvent) {
	// do nothing here
}

void EventSyncEvent::ExtractRegistry(EventRegistry &registry, ULONG *characterCount) {
	// unpack it

	// decode the string
	// will throw!
	std::string factoryName;
	ULONG factoryVersion;

	jsVerifyDo(m_inBuffer != 0, return );

	(*m_inBuffer) >> factoryName;
	(*m_inBuffer) >> factoryVersion;
	jsVerifyDo(factoryName == typeid(*IEvent::EncodingFactory()).name() &&
				   factoryVersion == IEncodingFactory::Version(),
		throw new KEPException("Version mismatch of encoders")); // probably won't even get here if mismatch

	// count
	ULONG count;
	(*m_inBuffer) >> count;
	jsVerifyDo(count >= 0 && count <= 1000, throw new KEPException("Event count out of range 0-1000"));
	registry.resize(count);

	for (EventRegistry::iterator i = registry.begin(); i != registry.end(); i++) {
		// send over only the name and version
		std::string s;
		(*m_inBuffer) >> s;
		i->name = s.c_str();
		(*m_inBuffer) >> i->m_version;
	}

	*characterCount = 0;
	(*m_inBuffer) >> *characterCount;

	m_inBuffer->Rewind();
}
