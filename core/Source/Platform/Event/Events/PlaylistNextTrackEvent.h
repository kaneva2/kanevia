///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include "InstanceId.h"

namespace KEP {

class PlaylistNextTrackEvent : public IEvent {
public:
	PlaylistNextTrackEvent(const ZoneIndex& zoneIndex, int playlistId, int currentMediaIndex) :
			IEvent(ClassId()) {
		m_zoneIndex = zoneIndex;
		m_playlistId = playlistId;
		m_currentMediaIndex = currentMediaIndex;
	}

	ZoneIndex m_zoneIndex;
	int m_playlistId;
	int m_currentMediaIndex;

	DEFINE_SECURE_CLASS_INFO(PlaylistNextTrackEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
