///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "Event/Base/IDispatcher.h"

// the well-known events
#include "ShutdownEvent.h"
#include "EventSyncEvent.h"
#include "LogonResponseEvent.h"
#include "StringListEvent.h"
#include "GetAttributeEvent.h"
#include "TextEvent.h"
#include "GenericEvent.h"
#include "RenderTextEvent.h"
#include "RenderLocaleTextEvent.h"
#include "ScriptServerEvent.h"
#include "ScriptClientEvent.h"
#include "Missing3DAppCharacterEvent.h"

namespace KEP {

EVENT_ID lastWellKnownEventId = 0;

// these are used for login and must be known on each side
bool RegisterWellKnownEvents(IDispatcher *dispatcher) {
	jsVerifyReturn(dispatcher != 0, false);

	RegisterEvent<ShutdownEvent>(*dispatcher);
	jsVerify(ShutdownEvent::ClassId() == ShutdownEvent::SHUTDOWN_EVENT_ID); // should always be zero!

	RegisterEvent<EventSyncEvent>(*dispatcher);
	RegisterEvent<LogonResponseEvent>(*dispatcher);
	RegisterEvent<GetAttributeEvent>(*dispatcher);
	RegisterEvent<StringListEvent>(*dispatcher);
	RegisterEvent<TextEvent>(*dispatcher);
	RegisterEvent<RenderTextEvent>(*dispatcher);
	RegisterEvent<RenderLocaleTextEvent>(*dispatcher);
	RegisterEvent<ScriptServerEvent>(*dispatcher);
	RegisterEvent<ScriptClientEvent>(*dispatcher);
	RegisterEvent<Missing3DAppCharacterEvent>(*dispatcher);
	EVENT_ID lastId = dispatcher->RegisterEvent("BadCharacterConfigEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	// added for RemoteDispatcher since it only will take well-known events
	lastWellKnownEventId = lastId;

	return true;
}

} // namespace KEP