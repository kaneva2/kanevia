///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "ShutdownEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(ShutdownEvent)

const EVENT_ID ShutdownEvent::SHUTDOWN_EVENT_ID = 0;

EVENT_CONSTRUCTOR_PROTOTYPE(ShutdownEvent) {
	// do nothing here
}
