///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "VerifyStatEvent.h"
#include "IGetSet.h"
#include "CItemClass.h"
#include "CPlayerClass.h"

using namespace KEP;

DECLARE_CLASS_INFO(VerifyStatEvent)

VerifyStatEvent::VerifyStatEvent(CPlayerObject *player, CStatBonusList *requiredStats) :
		IEvent(ClassId()) {
	(*OutBuffer()) << (ULONG) dynamic_cast<IGetSet *>(player);
	if (requiredStats) {
		(*OutBuffer()) << requiredStats->GetCount();
		for (POSITION pos = requiredStats->GetHeadPosition(); pos != NULL;) {
			CStatBonusObj *bonus = (CStatBonusObj *)requiredStats->GetNext(pos);
			(*OutBuffer()) << bonus->m_statID << bonus->m_statBonus;
		}
	} else
		(*OutBuffer()) << 0;
}

EVENT_CONSTRUCTOR_PROTOTYPE(VerifyStatEvent) {
}
