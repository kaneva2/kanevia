///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <string>

namespace KEP {

class StringListEvent : public IEvent {
public:
	StringListEvent(int count, int userInt, NETID from) :
			IEvent(ClassId()) {
		SetFrom(from);
		(*OutBuffer()) << count;
		(*OutBuffer()) << userInt;
	}

	void ExtractPrefix(int &cnt, int &userInt) {
		cnt = 0;
		userInt = 0;
		if (InBuffer()) {
			(*InBuffer()) >> cnt >> userInt;
		}
	}

	void ExtractNextString(std::string &s) {
		s = "";
		if (InBuffer()) {
			(*InBuffer()) >> s;
		}
	}

	DEFINE_SECURE_CLASS_INFO(StringListEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP