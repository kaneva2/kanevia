///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <js.h>

#include "ScriptServerEvent.h"
#include "ScriptPlayerInfo.h"

namespace KEP {

DECLARE_CLASS_INFO(ScriptServerEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(ScriptServerEvent) {
}

ScriptServerEvent::ScriptServerEvent(EventType eventType, ULONG objPlacementId, int zoneId, int instanceId, NETID clientNetId) :
		IEvent(ClassId()) {
	pl_assets = NULL;
	SetEventType(eventType);
	SetFrom(clientNetId);
	(*OutBuffer()) << objPlacementId << zoneId << instanceId;
}

void ScriptServerEvent::ExtractInfo(EventType& eventType, ULONG& objPlacementId, int& zoneId, int& instanceId, NETID& clientNetId) {
	eventType = GetEventType();
	clientNetId = From();
	(*InBuffer()) >> objPlacementId >> zoneId >> instanceId;
	m_objPlacementId = objPlacementId;
	m_zoneId = zoneId;
	m_instanceId = instanceId;
	m_decoded = true;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPlayerInfo& info) {
	int genderInt = 0;
	inBuffer >> info.m_name >> info.m_playerId >> genderInt >> info.m_age >> info.m_showMature >> info.m_permissions >> info.m_title;
	info.m_gender = static_cast<char>(genderInt);
	return inBuffer;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPlayerInfo& info) {
	return outBuffer << info.m_name << info.m_playerId << static_cast<int>(info.m_gender) << info.m_age << info.m_showMature << info.m_permissions << info.m_title;
}

} // namespace KEP
