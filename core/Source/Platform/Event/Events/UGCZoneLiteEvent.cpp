///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <js.h>
#include "UgcZoneLiteEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(UgcZoneLiteEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UgcZoneLiteEvent) {
}

UgcZoneLiteEvent::UgcZoneLiteEvent() :
		IEvent(ClassId()) {
}
