///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

/***********************************************************
CLASS

	ArenaEjectEvent
	
	event that is fired when a client or ai is removed from
	an arena

DESCRIPTION
	this could be from OOB, or GM, or admin UI teleport

***********************************************************/
class ArenaEjectEvent : public IEvent {
public:
	ArenaEjectEvent(LONG channelId, NETID player, int networkAssignedId);

	void ExtractInfo(LONG &channelId, NETID &player, int &networkAssignedId);

	DEFINE_SECURE_CLASS_INFO(ArenaEjectEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP

