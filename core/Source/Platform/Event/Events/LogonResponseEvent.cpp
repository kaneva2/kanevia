///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "LogonResponseEvent.h"

#include "Event/Base/IEncodingFactory.h"
using namespace KEP;

DECLARE_CLASS_INFO(LogonResponseEvent)

LogonResponseEvent::LogonResponseEvent() :
		IEvent(ClassId()), m_hResultCode(E_INVALIDARG) {
}

void LogonResponseEvent::SetErrorCodes(const char* connectionErrorCode,
	HRESULT hResultCode,
	ULONG characterCount,
	LOGON_RET replyCode,
	ULONG discDiagCode) {
	m_connectionErrorCode = connectionErrorCode;
	m_hResultCode = hResultCode;
	m_characterCount = characterCount;
	m_replyCode = replyCode;

	OutBuffer()->Rewind(); // in case called > 1 time
	(*OutBuffer()) << connectionErrorCode;
	(*OutBuffer()) << hResultCode;
	(*OutBuffer()) << m_characterCount;
	(*OutBuffer()) << (SHORT)replyCode;
	(*OutBuffer()) << discDiagCode;
}

EVENT_CONSTRUCTOR_PROTOTYPE(LogonResponseEvent) {
	// decode the string
	// will throw!
	if (m_inBuffer) {
		SHORT tmp;
		std::string s;
		(*m_inBuffer) >> s;
		m_connectionErrorCode = s.c_str();
		(*m_inBuffer) >> m_hResultCode;
		(*m_inBuffer) >> m_characterCount;
		(*m_inBuffer) >> tmp;
		m_replyCode = (LOGON_RET)tmp;
		m_inBuffer->Rewind();
	}
}
