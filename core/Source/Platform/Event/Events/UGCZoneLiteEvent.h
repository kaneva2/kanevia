///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <SDKCommon.h>
#include <string>
#include <InstanceId.h>

namespace KEP {

class UgcZoneLiteEvent : public IEvent {
public:
	// filter values for requests
	static const LONG REQ_CREATE_ZONE = 1;
	static const LONG REQ_LIST_ZONES = 2;
	static const LONG REQ_DELETE_ZONE = 3;
	static const LONG REQ_RENAME_ZONE = 4;
	static const LONG REQ_SET_DEFAULT_ZONE = 5;
	static const LONG REQ_LIST_SPAWNS = 6;
	static const LONG REQ_ADD_SPAWN = 7;
	static const LONG REQ_DELETE_SPAWN = 8;
	static const LONG REQ_SET_INITIAL_ARENA = 9;
	static const LONG REQ_UPDATE_SPAWN = 10;
	static const LONG REQ_RE_CREATE_ZONE = 11;

	// filter values for replies
	static const LONG REPLY_ZONES_LIST = 101;
	static const LONG REPLY_SPAWNS_LIST = 102;

	static const LONG REPLY_CREATE_ZONE = 103;
	static const LONG REPLY_DELETE_ZONE = 104;
	static const LONG REPLY_RENAME_ZONE = 105;
	static const LONG REPLY_SET_DEFAULT_ZONE = 106;
	static const LONG REPLY_ADD_SPAWN = 107;
	static const LONG REPLY_DELETE_SPAWN = 108;
	static const LONG REPLY_SET_INITIAL_ARENA = 109;
	static const LONG REPLY_UPDATE_SPAWN = 110;
	static const LONG REPLY_RE_CREATE_ZONE = 111;

	UgcZoneLiteEvent();

	// unpack request data
	bool ExtractCreateZone(std::string& name, LONG& zoneIndex, LONG& zoneType, bool& usePlayersPos, int& occupancyLimit, LONG& copyZoneIndex, LONG& copyFlag);
	bool ExtractRenameZone(int& zoneIndex, std::string& newname);
	bool ExtractAddSpawnPoint(int& zoneIndex, std::string& name, bool& usePlayerPos, float& x, float& y, float& z, int& rotation);
	bool ExtractUpdateSpawnPoint(int& spawnId, int& zoneIndex, std::string& name, float& x, float& y, float& z, int& rotation);
	bool ExtractReCreateZone(LONG& zoneIndex, LONG& baseZoneIndex, int& maintainObjects, LONG& copyZoneIndex, LONG& copyFlag);

	// reply types
	void SendZones(int count);
	void SendSpawnPoints(LONG zoneIndex, int count);

	void SendReplyCreateZone(LONG zoneIndex, bool retval, std::string zoneName, std::string msg);
	void SendReplyDeleteZone(LONG zoneIndex, bool retval, std::string msg);
	void SendReplyRenameZone(LONG zoneIndex, bool retval, std::string newZoneName, std::string msg);
	void SendReplySetDefaultZone(LONG zoneIndex, bool retval, std::string msg);
	void SendReplyAddSpawn(LONG zoneIndex, bool retval, LONG spawnId, std::string spawnName, std::string msg);
	void SendReplyDeleteSpawn(LONG zoneIndex, bool retval, LONG spawnId, std::string msg);
	void SendReplySetInitialArena(LONG zoneIndex, bool retval, std::string msg);
	void SendReplyUpdateSpawn(LONG zoneIndex, bool retval, LONG spawnId, std::string msg);
	void SendReplyReCreateZone(LONG zoneIndex, bool retval, LONG newZoneIndex, std::string msg);

	DEFINE_SECURE_CLASS_INFO(UgcZoneLiteEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline bool UgcZoneLiteEvent::ExtractCreateZone(std::string& name, LONG& zoneIndex, LONG& zoneType, bool& usePlayerPosAsSpawnPoint, int& occupancyLimit, LONG& copyZoneIndex, LONG& copyFlag) {
	jsVerifyReturn(Filter() == REQ_CREATE_ZONE, false);

	zoneIndex = ObjectId();
	int temp;
	(*InBuffer()) >> name >> zoneType >> temp >> occupancyLimit >> copyZoneIndex >> copyFlag;
	usePlayerPosAsSpawnPoint = temp != 0;
	return true;
}

inline bool UgcZoneLiteEvent::ExtractRenameZone(int& zoneIndex, std::string& newname) {
	jsVerifyReturn(Filter() == REQ_RENAME_ZONE, false);

	zoneIndex = ObjectId();
	(*InBuffer()) >> newname;
	return true;
}

inline bool UgcZoneLiteEvent::ExtractAddSpawnPoint(int& zoneIndex, std::string& name, bool& usePlayerPos, float& x, float& y, float& z, int& rotation) {
	jsVerifyReturn(Filter() == REQ_ADD_SPAWN, false);

	zoneIndex = ObjectId();
	int temp;
	(*InBuffer()) >> name >> temp;
	usePlayerPos = temp != 0;

	if (usePlayerPos) {
		x = y = z = 0.0;
		rotation = 0;
	} else
		(*InBuffer()) >> x >> y >> z >> rotation;
	return true;
}

inline bool UgcZoneLiteEvent::ExtractUpdateSpawnPoint(int& spawnId, int& zoneIndex, std::string& name, float& x, float& y, float& z, int& rotation) {
	jsVerifyReturn(Filter() == REQ_UPDATE_SPAWN, false);

	spawnId = ObjectId();
	(*InBuffer()) >> zoneIndex >> name >> x >> y >> z >> rotation;

	return true;
}

inline bool UgcZoneLiteEvent::ExtractReCreateZone(LONG& zoneIndex, LONG& baseZoneIndex, int& maintainObjects, LONG& copyZoneIndex, LONG& copyFlag) {
	jsVerifyReturn(Filter() == REQ_RE_CREATE_ZONE, false);

	zoneIndex = ObjectId();
	(*InBuffer()) >> baseZoneIndex >> maintainObjects >> copyZoneIndex >> copyFlag;
	return true;
}

inline void UgcZoneLiteEvent::SendZones(int count) {
	SetFilter(REPLY_ZONES_LIST);
	(*OutBuffer()) << count;
}

inline void UgcZoneLiteEvent::SendSpawnPoints(LONG zoneIndex, int count) {
	SetFilter(REPLY_SPAWNS_LIST);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << count;
}

inline void UgcZoneLiteEvent::SendReplyCreateZone(LONG zoneIndex, bool retval, std::string zoneName, std::string msg) {
	SetFilter(REPLY_CREATE_ZONE);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << zoneName << msg;
}

inline void UgcZoneLiteEvent::SendReplyDeleteZone(LONG zoneIndex, bool retval, std::string msg) {
	SetFilter(REPLY_DELETE_ZONE);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << msg;
}

inline void UgcZoneLiteEvent::SendReplyRenameZone(LONG zoneIndex, bool retval, std::string newZoneName, std::string msg) {
	SetFilter(REPLY_RENAME_ZONE);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << newZoneName << msg;
}

inline void UgcZoneLiteEvent::SendReplySetDefaultZone(LONG zoneIndex, bool retval, std::string msg) {
	SetFilter(REPLY_SET_DEFAULT_ZONE);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << msg;
}

inline void UgcZoneLiteEvent::SendReplyAddSpawn(LONG zoneIndex, bool retval, LONG spawnId, std::string spawnName, std::string msg) {
	SetFilter(REPLY_ADD_SPAWN);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << spawnId << spawnName << msg;
}

inline void UgcZoneLiteEvent::SendReplyDeleteSpawn(LONG zoneIndex, bool retval, LONG spawnId, std::string msg) {
	SetFilter(REPLY_DELETE_SPAWN);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << spawnId << msg;
}

inline void UgcZoneLiteEvent::SendReplySetInitialArena(LONG zoneIndex, bool retval, std::string msg) {
	SetFilter(REPLY_SET_INITIAL_ARENA);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << msg;
}

inline void UgcZoneLiteEvent::SendReplyUpdateSpawn(LONG zoneIndex, bool retval, LONG spawnId, std::string msg) {
	SetFilter(REPLY_UPDATE_SPAWN);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << spawnId << msg;
}

inline void UgcZoneLiteEvent::SendReplyReCreateZone(LONG zoneIndex, bool retval, LONG newZoneIndex, std::string msg) {
	SetFilter(REPLY_RE_CREATE_ZONE);
	SetObjectId((OBJECT_ID)zoneIndex);
	(*OutBuffer()) << ((retval == true) ? 1 : 0) << newZoneIndex << msg;
}

} // namespace KEP
