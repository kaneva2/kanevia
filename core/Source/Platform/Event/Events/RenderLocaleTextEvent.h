///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TextEvent.h"

namespace KEP {

class IDispatcher;

class RenderLocaleTextEvent : public IEvent {
public:
	RenderLocaleTextEvent(
		UINT id,
		eChatType chatType,
		const char* s0 = 0,
		const char* s1 = 0,
		const char* s2 = 0,
		const char* s3 = 0,
		const char* s4 = 0,
		const char* s5 = 0,
		const char* s6 = 0,
		const char* s7 = 0,
		const char* s8 = 0,
		const char* s9 = 0);

	bool ExtractInfo(std::string& text, eChatType& chatType);

	DEFINE_SECURE_CLASS_INFO(RenderLocaleTextEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)

	static void SendLocaleTextTo(
		IDispatcher& dispatcher,
		NETID idFromLoc,
		eChatType chatType,
		UINT id,
		const char* s0 = 0,
		const char* s1 = 0,
		const char* s2 = 0,
		const char* s3 = 0,
		const char* s4 = 0,
		const char* s5 = 0,
		const char* s6 = 0,
		const char* s7 = 0,
		const char* s8 = 0,
		const char* s9 = 0);
};

} // namespace KEP
