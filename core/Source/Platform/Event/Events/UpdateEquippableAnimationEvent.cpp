///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "UpdateEquippableAnimationEvent.h"
#include "EventSystem\Dispatcher.h"

using namespace std;

using namespace KEP;

DECLARE_CLASS_INFO(UpdateEquippableAnimationEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UpdateEquippableAnimationEvent) {
}

UpdateEquippableAnimationEvent::UpdateEquippableAnimationEvent(NETID initiatorNetAssignedID, const vector<ItemAnimation>& animations) :
		IEvent(ClassId()) {
	(*OutBuffer()) << initiatorNetAssignedID;
	(*OutBuffer()) << (int)animations.size();
	for (const auto& ia : animations)
		(*OutBuffer()) << ia.m_glidItem << ia.m_glidAnim;
}

void UpdateEquippableAnimationEvent::ExtractInfo(NETID& initiatorNetAssignedID, vector<ItemAnimation>& animations) {
	animations.clear();

	int argCount = 0;
	(*InBuffer()) >> initiatorNetAssignedID;
	(*InBuffer()) >> argCount;
	if (argCount > 0) {
		animations.resize(argCount);
		for (int i = 0; i < argCount; i++)
			(*InBuffer()) >> animations[i].m_glidItem >> animations[i].m_glidAnim;
	}
}
