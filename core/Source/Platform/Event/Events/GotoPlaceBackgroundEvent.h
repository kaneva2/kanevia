///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPConstants.h"
#include "IBackgroundDBEvent.h"
#include "GotoPlaceResultCode.h"

namespace KEP {

const int DEFAULT_SPAWN_POINT = 0;
const int SPAWN_POINT_NOT_USED = -1;
const int BAD_SPAWN_POINT = -2;

/// class to pass data back and forth between
/// foregraound and background threads
/// basically a glorified struct
class GotoPlaceBackgroundEvent : public IBackgroundDBEvent {
public:
	GotoPlaceBackgroundEvent(EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority),
			type(0),
			ret(PLAYER_NOT_FOUND),
			goingToPlayer(false),
			playerId(0),
			destKanevaUserId(0),
			spawnPtIndex(BAD_SPAWN_POINT),
			x(0.0),
			y(0.0),
			z(0.0),
			rotation(0),
			isGm(false),
			serverId(0),
			coverCharge(0),
			zoneOwnerId(0),
			charIndex(0),
			reconnecting(0),
			showMature(false),
			defaultUrl(false),
			zonePermissions(ZP_UNKNOWN),
			parentZoneInstanceId(0) {}

	// inbound only
	LONG type; //< what type of GotoPlace it is, See above
	bool goingToPlayer;
	int playerId; //< player related to this (0 is none)
	int destKanevaUserId;
	bool isGm;
	bool reconnecting;
	int charIndex;
	std::string country;
	std::string birthDate;
	bool showMature;

	// in/out
	std::string url; //< for gotoUrl, inbound, always full url on outbound
	LONG serverId; //< on in: our serverId, on out: if non-zero, client needs to switch to this server
	LONG scriptServerId;
	bool defaultUrl; //< was this a default URL e.g. stp://WOK -- this avoids recursive spawning

	// outbound only
	LONG ret; //< return code from query
	int spawnPtIndex;
	double x, y, z;
	int rotation;
	LONG coverCharge;
	ULONG zoneOwnerId; //< if covercharge, this is who gets it
	BYTE zonePermissions; //< player permission in zone
	unsigned parentZoneInstanceId;

	DEFINE_DB_CLASS_INFO(GotoPlaceBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP