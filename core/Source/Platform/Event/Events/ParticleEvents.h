///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "KEPConstants.h"
#include "glids.h"

namespace KEP {

class IDispatcher;

enum { PARTICLE_ADD = 1,
	PARTICLE_REMOVE,
	PARTICLE_RESTART,
	PARTICLE_MOVE,
	PARTICLE_CHANGE };

class AddParticleEvent : public IEvent {
public:
	AddParticleEvent(
		const GLID& particleGLID,
		ObjectType parentType,
		int parentId,
		const std::string& boneName,
		int particleSlotId,
		float x, float y, float z,
		float dirX, float dirY, float dirZ,
		float upX, float upY, float upZ);

	void ExtractInfo(
		GLID& particleGLID,
		ObjectType& parentType,
		int& parentId,
		std::string& boneName,
		int& particleSlotId,
		float& x, float& y, float& z,
		float& dirX, float& dirY, float& dirZ,
		float& upX, float& upY, float& upZ);

	DEFINE_SECURE_CLASS_INFO(AddParticleEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline AddParticleEvent::AddParticleEvent(
	const GLID& particleGLID,
	ObjectType parentType,
	int parentId,
	const std::string& boneName,
	int particleSlotId,
	float x, float y, float z,
	float dirX, float dirY, float dirZ,
	float upX, float upY, float upZ) :
		IEvent(ClassId()) {
	GLID glidParticle = particleGLID;
	SetFilter((ULONG)PARTICLE_ADD);
	ULONG objId = *((ULONG*)&glidParticle); // drf - this really should not be sent as ULONG since it is an int
	SetObjectId((OBJECT_ID)objId);
	(*OutBuffer())
		<< static_cast<int>(parentType)
		<< parentId
		<< particleSlotId
		<< x << y << z
		<< dirX << dirY << dirZ
		<< upX << upY << upZ
		<< boneName;
}

inline void AddParticleEvent::ExtractInfo(
	GLID& particleGLID,
	ObjectType& parentType,
	int& parentId,
	std::string& boneName,
	int& particleSlotId,
	float& x, float& y, float& z,
	float& dirX, float& dirY, float& dirZ,
	float& upX, float& upY, float& upZ) {
	// DRF - Bug Fix - Glids are int not ULONG
	ULONG objId = (ULONG)ObjectId();
	particleGLID = (GLID) * ((int*)&objId);

	(*InBuffer()) >> reinterpret_cast<int&>(parentType) >> parentId >> particleSlotId >> x >> y >> z;

	try {
		// Backward compatible impl: try/catch can be removed once all 3dapp servers have been upgraded.
		// Legacy event format has only three floats left in the stream, which will cause decoder to throw an exception in next line
		(*InBuffer()) >> dirX >> dirY >> dirZ >> upX >> upY >> upZ >> boneName;
	} catch (...) {
		// If legacy event, discard rotation vector (not used anyway) and set all remaining parameters to default values.
		dirX = 0.0f;
		dirY = 0.0f;
		dirZ = 1.0f;
		upX = 0.0f;
		upY = 1.0f;
		upZ = 0.0f;
		boneName = "";
	}
}

class RemoveParticleEvent : public IEvent {
public:
	RemoveParticleEvent(ObjectType parentType, int parentId, const std::string& boneName, int particleSlotId);

	void ExtractInfo(ObjectType& parentType, int& parentId, std::string& boneName, int& particleSlotId);

	DEFINE_SECURE_CLASS_INFO(RemoveParticleEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline RemoveParticleEvent::RemoveParticleEvent(ObjectType parentType, int parentId, const std::string& boneName, int particleSlotId) :
		IEvent(ClassId()) {
	SetFilter(PARTICLE_REMOVE);
	(*OutBuffer()) << static_cast<int>(parentType) << parentId << particleSlotId << boneName;
}

inline void RemoveParticleEvent::ExtractInfo(ObjectType& parentType, int& parentId, std::string& boneName, int& particleSlotId) {
	(*InBuffer()) >> reinterpret_cast<int&>(parentType) >> parentId >> particleSlotId;

	try {
		// Backward compatible impl: try/catch can be removed once all 3dapp servers have been upgraded.
		(*InBuffer()) >> boneName;
	} catch (...) {
		boneName = "";
	}
}

void RegisterParticleEvents(IDispatcher& disp);

} // namespace KEP
