///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "ContentServiceCompletionEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(ContentServiceCompletionEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(ContentServiceCompletionEvent) {}

ContentServiceCompletionEvent::ContentServiceCompletionEvent(ULONG filter, LPVOID message, int size, const std::vector<GLID>& glids) :
		IEvent(ClassId()) {
	SetFilter(filter);
	(*OutBuffer()) << (ULONG)message << size << (ULONG)glids.size();
	for (size_t i = 0; i < glids.size(); ++i)
		(*OutBuffer()) << (int)glids[i];
}

ContentServiceCompletionEvent::ContentServiceCompletionEvent(const std::vector<GLID>& glids) :
		IEvent(ClassId()) {
	SetFilter(SCRIPT_ICON);
	(*OutBuffer()) << (ULONG)glids.size();
	for (size_t i = 0; i < glids.size(); ++i)
		(*OutBuffer()) << (int)glids[i];
}

void ContentServiceCompletionEvent::ExtractValues(LPVOID& message, int& size, std::vector<GLID>& glids) {
	ULONG msg;
	ULONG numberOfGlids;
	(*InBuffer()) >> msg >> size >> numberOfGlids;
	glids.clear();
	for (ULONG i = 0; i < numberOfGlids; ++i) {
		int glid;
		(*InBuffer()) >> glid;
		glids.push_back((GLID)glid);
	}
	message = reinterpret_cast<LPVOID>(msg);
}

void ContentServiceCompletionEvent::ExtractValues(std::vector<GLID>& glids) {
	if (Filter() != SCRIPT_ICON) {
		assert(false);
		return;
	}

	ULONG numberOfGlids;
	(*InBuffer()) >> numberOfGlids;

	for (ULONG i = 0; i < numberOfGlids; ++i) {
		int glid;
		(*InBuffer()) >> glid;
		glids.push_back((GLID)glid);
	}
}