///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"
#include <instanceid.h>
#include <inventoryType.h>

#include "common\include\KEPHelpers.h"
#include "common\include\Glids.h"
#include "common\include\MediaParams.h"
#include "Common\include\CoreStatic\AnimSettings.h"

namespace KEP {

class ZoneCustomizationBackgroundEvent : public IBackgroundDBEvent {
public:
	ZoneCustomizationBackgroundEvent(EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority),
			type(ZC_ADD),
			playerId(0),
			globalId(GLID_INVALID),
			textureId(0),
			inventorySubType(0),
			expiredDuration(0),
			objPlacementId(0),
			time(0),
			ret(0),
			replyEvent(0),
			broadcastEvent(0),
			x(0),
			y(0),
			z(0),
			dx(0),
			dy(0),
			dz(0),
			sx(0),
			sy(0),
			sz(0),
			channel(INVALID_CHANNEL_ID),
			assetId(0),
			baseGlid(0),
			useType(0),
			tryon(false),
			derivable(false),
			playerIsGm(false),
			networkAssignedId(0),
			playlistId(INT_MAX), // zero is valid
			assetType(0),
			stopPlay(0) {}

	enum eType {
		ZC_ADD,
		ZC_ADD_FRAME,
		ZC_UPDATE_TEXTURE,
		ZC_UPDATE_FRIEND,
		ZC_REMOVE,
		ZC_MOVE,
		ZC_ROTATE,
		ZC_UPDATE_WORLD_OBJ_TEXTURE,
		ZC_UPDATE_ITEM_URL,
		ZC_UPDATE_ANIMATION,
		ZC_UPDATE_PARTICLE,
		ZC_ADD_SOUND,
		ZC_UPDATE_ITEM_MEDIA,
		ZC_UPDATE_MEDIA_VOL_RAD,
		ZC_TYPES
	};

	static std::string eTypeStr(eType etype) {
		const std::string etypeStr[ZC_TYPES] = {
			"ADD",
			"ADD_FRAME",
			"UPDATE_TEXTURE",
			"UPDATE_FRIEND",
			"REMOVE",
			"MOVE",
			"ROTATE",
			"UPDATE_WORLD_OBJ_TEXTURE",
			"UPDATE_ITEM_URL",
			"UPDATE_ANIMATION",
			"UPDATE_PARTICLE",
			"ADD_SOUND",
			"UPDATE_ITEM_MEDIA",
			"UPDATE_MEDIA_VOL_RAD"
		};
		return (etype < ZC_TYPES) ? etypeStr[etype] : "[ZC_UNKNOWN]";
	}

	std::string ToStr() {
		std::string str;
		StrAppend(str, "ZCE<" << eTypeStr(type));
		if (globalId != GLID_INVALID)
			StrAppend(str, " glid=" << globalId);
		if (objPlacementId > 0)
			StrAppend(str, " objId=" << objPlacementId);
		if (textureId > 0)
			StrAppend(str, " texId=" << textureId);
		if (x != 0 || y != 0 || z != 0)
			StrAppend(str, " pos=(" << FMT_DIST << x << "," << y << "," << z << ")");
		if (dx != 0 || dy != 0 || dz != 0)
			StrAppend(str, " dir=(" << FMT_DIST << dx << "," << dy << "," << dz << ")");
		if (!charName.empty())
			StrAppend(str, " '" << charName << "'");
		StrAppend(str, " " << zoneIndex.ToStr() << ">");
		return str;
	}

	// inbound only
	eType type; //< what type of action is required on dynamic object
	int playerId;
	int globalId;
	int textureId;
	int inventorySubType;
	int expiredDuration;
	float x, y, z; //< position of dyn obj
	float dx, dy, dz; //< direction of dyn obj
	float sx, sy, sz; //< slide vector of dyn obj
	std::string charName; //< players name for logging
	int assetId; //< for friend/texture
	std::string worldObjSerialNum;

	// inbound for changing url only
	bool playerIsGm;
	int networkAssignedId;
	int playlistId;
	std::string itemUrl;
	std::string textureUrl;

	// in/out
	int objPlacementId; //< out on add, in on modify/delete
	DWORD time;

	// pass thru for try on
	int baseGlid;
	int useType;
	bool tryon;

	// derivability flag
	bool derivable;

	// outbound only
	int ret;
	IEvent *replyEvent;
	IEvent *broadcastEvent;
	ChannelId channel;

	MediaParams mediaParams;
	int assetType;
	std::string mature;
	int stopPlay;

	// Animation Settings
	AnimSettings animSettings; // drf - added

	DEFINE_DB_CLASS_INFO(ZoneCustomizationBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP