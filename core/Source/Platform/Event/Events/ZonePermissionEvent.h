///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <SDKCommon.h>

#include "Event\Base\IEvent.h"

namespace KEP {
class IDispatcher;

class SetZonePermissionEvent : public IEvent {
public:
	SetZonePermissionEvent(int permission); // see ZP_* CPlayer.h

	void ExtractInfo(int &permissions);

	DEFINE_CLASS_INFO(SetZonePermissionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline SetZonePermissionEvent::SetZonePermissionEvent(int permission) :
		IEvent(ClassId()) {
	(*OutBuffer()) << permission;
}

inline void SetZonePermissionEvent::ExtractInfo(int &permisson) {
	(*InBuffer()) >> permisson;
}

void RegisterZonePermissionEvents(IDispatcher &disp);

} // namespace KEP
