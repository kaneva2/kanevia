///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include "Event\DDR\DDR_S_GameData.h"
#include <SDKCommon.h>

namespace KEP {

class DDRDBEvent : public IEvent {
public:
	static const LONG DDRDB_RECORD_SCORE = 2;
	static const LONG DDRDB_LOAD_GAME_DATA = 3;
	static const LONG DDRDB_LOAD_PLAYER_DATA = 4;

	DDRDBEvent();

	void DBRecordScore(bool isPermanentZone,
		ULONG objectId,
		ULONG zoneIndex,
		int dynObjectId,
		ULONG gameId,
		int playerId,
		const char* playerName,
		ULONG highScore,
		ULONG score,
		ULONG numPerfects,
		ULONG numPerfectsInARows);

	void LoadGameData(ULONG gameId, GameLevelsMap* outData);
	void LoadPlayerData(ULONG gameId, ULONG gameLevel, NETID from, bool newGame, int dynObjectId, int playerId, ULONG zoneIndex, ULONG objectI);

	void ExtractInfo();

	virtual ULONG GetGameId() { return m_gameId; }
	virtual int GetPlayerId() { return m_playerId; }

	LONG type;
	ULONG m_gameId;
	ULONG m_score;
	ULONG m_highScore;
	ULONG m_numPerfects;
	ULONG m_numPerfectsInARow;
	int m_dynObjectId;
	int m_playerId;
	std::string m_playerName;

	bool m_isPermanentZone;
	ULONG m_objectId;
	ULONG m_zoneIndex;
	bool m_newGame;
	ULONG m_gameLevel;

	GameLevelsMap* m_gameLevelsMap;

	DEFINE_SECURE_CLASS_INFO(DDRDBEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
