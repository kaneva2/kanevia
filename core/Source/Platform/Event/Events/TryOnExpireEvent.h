///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Event\Base\IEvent.h>

#include <common\include\CoreStatic\InstanceId.h>
#include <common\include\UseTypes.h>

namespace KEP {

class TryOnExpireEvent : public IEvent {
public:
	// Filters
	static const LONG INFORM_EXPIRATION = 1;

	TryOnExpireEvent();

	// init fns
	void InformExpiration(int playerId, int glid, int baseGlid, const USE_TYPE& useType, int expireDuration, int dynamic_obj_placement_id);

	void ExtractInfo();

	LONG type;

	int m_playerId;
	int m_glid;
	int m_baseGlid;
	int m_expireDuration;

	USE_TYPE m_useType;

	int m_dynamic_obj_placement_id;

	DEFINE_SECURE_CLASS_INFO(TryOnExpireEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
