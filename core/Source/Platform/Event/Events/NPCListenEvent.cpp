///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "NPCListenEvent.h"

#include "Event/Base/IEncodingFactory.h"
using namespace KEP;

DECLARE_CLASS_INFO(NPCListenEvent)

NPCListenEvent::NPCListenEvent(const char* msg, eChatType chatType, ChannelId channel, short radius, float x, float y, float z, const std::vector<int>& ids) :
		IEvent(ClassId()) {
	SetFrom(SERVER_NETID); // always from server
#ifndef REFACTOR_INSTANCEID
	SetFilter(channel);
#else
	SetFilter(channel.toLong());
#endif

	(*OutBuffer()) << msg
				   << (int)chatType
				   << radius
				   << x << y << z;

	(*OutBuffer()) << (ULONG)ids.size();
	for (std::vector<int>::const_iterator i = ids.begin(); i != ids.end(); i++) {
		(*OutBuffer()) << *i;
	}
}

EVENT_CONSTRUCTOR_PROTOTYPE(NPCListenEvent) {
	// do nothing here
}
