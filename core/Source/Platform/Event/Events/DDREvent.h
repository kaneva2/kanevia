///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include "Event\DDR\DDR_S_Player.h"
#include "Event\DDR\DDR_S_GameData.h"
#include <SDKCommon.h>
#include <string>
#include <vector>

namespace KEP {

class DDREvent : public IEvent {
public:
	static const LONG DDR_START_LEVEL = 1;
	static const LONG DDR_START_LEVEL_DATA = 2;
	static const LONG DDR_RECORD_SCORE = 3;
	static const LONG DDR_POST_SCORE = 4;
	static const LONG DDR_START_GAME = 5;
	static const LONG DDR_END_GAME = 6;
	static const LONG DDR_CLOSE_DIALOG = 7;
	static const LONG DDR_GAME_OVER_DIALOG = 8;
	static const LONG DDR_READY_TO_PLAY = 9;
	static const LONG DDR_POST_PLAYERS_SCORES = 10;
	static const LONG DDR_PLAYER_EXIT = 11;
	static const LONG DDR_LIMIT_EXCEEDED = 12;
	static const LONG DDR_PLAYER_ACCESS_DENIED = 13;
	static const LONG DDR_GAME_DATA = 14;
	static const LONG DDR_PLAYER_PAUSED = 15;
	static const LONG DDR_CLI_PAUSED_RECEIVED = 16;
	static const LONG DDR_CLI_PAUSED_SENT = 17;
	static const LONG DDR_PLAYER_DATA = 18;

	static const int DDR_SCORE_PERFECT = 1;
	static const int DDR_SCORE_GREAT = 2;
	static const int DDR_SCORE_GOOD = 3;

	DDREvent();
	~DDREvent();

	void StartLevel(ULONG gameId, ULONG levelId, bool newGame, int objectId);

	// used by background thread to tell main thread db call done
	void GameData(ULONG gameId);
	void PlayerData(NETID from, ULONG gameId, int playerId,
		int gameLevel, bool newGame, int dynObjectId,
		int zoneIndex, int objectId, ULONG highScore);

	void StartLevelData(int playerId,
		ULONG gameId,
		ULONG levelId,
		ULONG seed,
		ULONG num_seconds_to_play,
		ULONG num_moves,
		ULONG num_misses,
		ULONG level_bonus_trigger_num,
		ULONG animation_flare_count,
		ULONG animation_final_flare_count,
		ULONG animation_miss_count,
		ULONG animation_levelup_count,
		ULONG animation_perfect_count,
		ULONG animation_idle_count,
		ULONG num_sub_levels,
		ULONG perfect_score_ceiling,
		ULONG great_score_ceiling,
		ULONG good_score_ceiling,
		ULONG last_level_reduction_ms,
		std::vector<std::pair<ULONG, int>> &animation_flares,
		std::vector<std::pair<ULONG, int>> &animation_final_flares,
		std::vector<std::pair<ULONG, int>> &animation_misses,
		std::vector<std::pair<ULONG, int>> &animation_levelups,
		std::vector<std::pair<ULONG, int>> &animation_perfects,
		std::vector<std::pair<ULONG, int>> &animation_idles);

	void RecordScore(ULONG gameId,
		int objectId,
		ULONG levelId,
		int gameInstanceId,
		ULONG numMilliSecondsExpired,
		bool levelComplete,
		bool gameOver,
		bool perfectBonus,
		bool perfectAchieved,
		bool useAlternate);

	void PostDDRScore(ULONG playerId,
		ULONG gameId,
		ULONG levelId,
		int gameInstanceId,
		ULONG score,
		ULONG highScore,
		ULONG fameGainAmount,
		int newFameLevel,
		int percentToNextLevel,
		std::string rewardsAtNextLevel,
		std::string gainMessage,
		float totalFame);

	void PostScores(int playerId,
		ULONG gameId,
		ULONG levelId,
		ULONG highestScore,
		const char *highestScoreName,
		DDRPlayers *players);

	void PlayerExit(ULONG gameId, int objectId, int leftFloor);

	void AccessDenied(ULONG gameId, int playerId, int zoneIndexId);

	void PlayerPaused(ULONG gameId, int objectId, int isPaused);

	void ExtractInfo();

	void EncryptEvent();

	virtual ULONG GetGameId() { return m_gameId; }
	virtual ULONG GetGameLevel() { return m_levelId; }

	void CloseGameDialog(ULONG gameId);
	void GameOverDialog(ULONG gameId, int famePointsAwarded, int percentToNext, std::string rewardsAtNext, std::string gainMessage);

	void ReadyToPlay(ULONG gameId);

	LONG type;
	int m_playerId;

	ULONG m_gameId;
	int m_objectId;
	int m_dynObjectId;
	ULONG m_numLevels;
	ULONG m_levelId;
	ULONG m_seed;
	ULONG m_num_seconds_to_play;
	ULONG m_num_moves;
	ULONG m_num_misses;
	ULONG m_level_bonus_trigger_num;

	ULONG m_animation_flare_count;
	ULONG m_animation_final_flare_count;
	ULONG m_animation_miss_count;
	ULONG m_animation_levelup_count;
	ULONG m_animation_perfect_count;
	ULONG m_animation_idle_count;

	ULONG m_num_sub_levels;
	ULONG m_perfect_score_ceiling;
	ULONG m_great_score_ceiling;
	ULONG m_good_score_ceiling;
	ULONG m_last_level_reduction_ms;

	bool m_levelComplete;
	int m_leftFloor;
	int m_isPaused;
	int m_zoneIndexId;
	bool m_newGame;
	bool m_gameOver;
	bool m_perfectBonus;
	bool m_perfectAchieved;
	ULONG m_highestScore;
	std::string m_highestScoreName;
	int m_highestScoreId;
	ULONG m_famePointsEarned;
	int m_newFameLevel;
	int m_percentToNextLevel;
	std::string m_rewardsAtNextLevel;
	std::string m_gainMessage;
	bool m_useAlternate;
	ULONG m_totalFame;

	std::vector<std::pair<ULONG, int>> m_animation_flares;
	std::vector<std::pair<ULONG, int>> m_animation_final_flares;
	std::vector<std::pair<ULONG, int>> m_animation_misses;
	std::vector<std::pair<ULONG, int>> m_animation_levelups;
	std::vector<std::pair<ULONG, int>> m_animation_perfects;
	std::vector<std::pair<ULONG, int>> m_animation_idles;

	int m_gameInstanceId;
	ULONG m_score;
	ULONG m_highScore;

	LONG m_networkAssignedId;
	float m_Radius;
	int m_miscInt;
	std::string m_miscStr;
	int m_triggerx;
	int m_triggery;
	int m_triggerz;
	double m_triggerCenterx;
	double m_triggerCentery;
	double m_triggerCenterz;
	int m_ctrlx;
	int m_ctrly;
	int m_ctrlWidth;
	int m_ctrlHeight;

	ULONG m_numMilliSecondsExpired;

	int m_skill_id;
	std::string m_skill_name;
	int m_skill_type;
	float m_skill_gain_increment;
	std::string m_skill_basic_ability_skill;
	float m_skill_basic_ability_exp_mastered;
	int m_skill_basic_ability_function;
	float m_skill_gains_when_failed;
	float m_skill_max_multiplier;
	std::string m_skill_use_internal_timer;
	std::string m_skill_use_filter_eval_level;
	int m_skill_use_filter_current_level;
	double m_skill_use_filter_current_experience;

	DEFINE_SECURE_CLASS_INFO(DDREvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
