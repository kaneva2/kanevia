///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "Event/Base/IEventHandler.h"
#include "DownloadHandler.h"

#include <vector>

namespace KEP {

/*==============================================================================
	class for downloading zone dyn objects, custom textures for world objs
==============================================================================*/
class ZoneCustomizationDownloadHandler : public DownloadHandler, public IEventHandler {
public:
	// state machine
	enum EState {
		PROCESSING,
		BUFFER_SPLIT,
		ALL_DONE
	};

	enum ResultCode {
		OK,
		PARSING_ERROR,
		SERVER_ERROR,
	};

	ZoneCustomizationDownloadHandler(Dispatcher &disp, log4cplus::Logger &logger);

	// codes in first character of each line indicating type of record
	static const char RESULT_CODE = 'r';
	static const char PARAM_CODE = 'p';
	static const char DYN_OBJ_CODE = 'd';
	static const char WORLD_CODE = 'w';
	static const char END_CODE = 'z';

	EVENT_ID zoneCustomId() { return m_serverDynObjectEventId; }

	virtual EVENT_PROC_RC ProcessEvent(IDispatcher *disp, IEvent *e);

	virtual bool BytesDownloaded(char *_bytes, ULONG len);

	virtual void DownloadCanceled() {
		init();
	}

	virtual ~ZoneCustomizationDownloadHandler() {
		delete m_buffer;
	}

private:
	void init();

	// parse params, allowing for multiple delimiters to be together
	int parseParams(char *ptr);

	// process a full buffer by checking code and acting accordingly
	void processBuffer(char *curPtr);

	// process the line with the return code in it
	void processReturnCode(char *curPtr);

	// process the line with a dynamic object in it
	void processDynamic(char *buff);

	// process the line with a world object in it
	void processWorld(char *buff);

	std::vector<std::string> m_params; // parameters that came down before DOs
	char *m_parsedTokens[50]; // parsed param ptrs set in parseParams

	EState m_state;

	int m_currentCount;
	int m_rc; // return code from web
	int m_totalObjects; // dynamicObjects + soundPlacements
	int m_totalWorldObjectCustomizations;
	int m_soundPlacements; // drf - added

	static const int BUFF_SIZE = 2048; // just need enough to hold one that spans buffer
	char *m_buffer;

	Dispatcher &m_disp;
	log4cplus::Logger m_logger;

	EVENT_ID m_serverDynObjectEventId;
};

} // namespace KEP