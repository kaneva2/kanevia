///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <SDKCommon.h>

#include "Event\Base\IEvent.h"

namespace KEP {
class IDispatcher;

class TargetedPositionEvent : public IEvent {
public:
	TargetedPositionEvent(double x, double y, double z, double nx, double ny, double nz);

	void ExtractInfo(double &x, double &y, double &z, double &nx, double &ny, double &nz);

	DEFINE_CLASS_INFO(TargetedPositionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

class TargetedPositionEventEx : public IEvent {
public:
	enum { LBUTTON,
		MBUTTON,
		RBUTTON };
	enum { LBUTTONDOWN = 1 << LBUTTON,
		MBUTTONDOWN = 1 << MBUTTON,
		RBUTTONDOWN = 1 << RBUTTON };

	TargetedPositionEventEx(double x, double y, double z, double nx, double ny, double nz, ULONG mouseButtonState, ULONG keyState, int targetType, int targetId, const std::string &targetName);

	void ExtractInfo(double &x, double &y, double &z, double &nx, double &ny, double &nz, ULONG &mouseButtonState, ULONG &keyState, int &targetType, int &targetId, std::string &targetName);

	DEFINE_CLASS_INFO(TargetedPositionEventEx, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline TargetedPositionEvent::TargetedPositionEvent(double x, double y, double z, double nx, double ny, double nz) :
		IEvent(ClassId()) {
	(*OutBuffer()) << x << y << z << nx << ny << nz;
}

inline void TargetedPositionEvent::ExtractInfo(double &x, double &y, double &z, double &nx, double &ny, double &nz) {
	(*InBuffer()) >> x >> y >> z >> nx >> ny >> nz;
}

inline TargetedPositionEventEx::TargetedPositionEventEx(double x, double y, double z, double nx, double ny, double nz, ULONG mouseButtonState, ULONG keyState, int targetType, int targetId, const std::string &targetName) :
		IEvent(ClassId()) {
	(*OutBuffer()) << x << y << z << nx << ny << nz << mouseButtonState << keyState << targetType << targetId << targetName;
}

inline void TargetedPositionEventEx::ExtractInfo(double &x, double &y, double &z, double &nx, double &ny, double &nz, ULONG &mouseButtonState, ULONG &keyState, int &targetType, int &targetId, std::string &targetName) {
	(*InBuffer()) >> x >> y >> z >> nx >> ny >> nz >> mouseButtonState >> keyState >> targetType >> targetId >> targetName;
}

void RegisterTargetingEvents(IDispatcher &disp);

} // namespace KEP
