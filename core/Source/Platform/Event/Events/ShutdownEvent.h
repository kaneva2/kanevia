///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <string>

namespace KEP {

class ShutdownEvent : public IEvent {
public:
	ShutdownEvent() :
			IEvent(SHUTDOWN_EVENT_ID, (ULONG)0, (OBJECT_ID)0, EP_HIGH + 1) {}

	static const EVENT_ID SHUTDOWN_EVENT_ID;

	DEFINE_SECURE_CLASS_INFO(ShutdownEvent, PackVersion(1, 0, 0, 0), EP_HIGH + 1)
};

} // namespace KEP
