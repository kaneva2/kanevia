///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TextEvent.h"

namespace KEP {

class RenderTextEvent : public IEvent {
public:
	static const LONG PLAYER_LOG_IN = 1;
	static const LONG PLAYER_LOG_OUT = 2;

	RenderTextEvent(const std::string &textMsg, eChatType chatType, const char *toDistribution = nullptr, const char *fromRecipient = nullptr, eChatType origChatType = eChatType::Private);

	void ExtractInfo(std::string &text, eChatType &chatType, std::string &toDistribution, std::string &fromRecipient);

	DEFINE_SECURE_CLASS_INFO(RenderTextEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
