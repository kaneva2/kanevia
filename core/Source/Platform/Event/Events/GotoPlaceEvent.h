///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Event\Base\IEvent.h>

#include <common\include\CoreStatic\InstanceId.h>
#include <common\include\StpUrl.h>

namespace KEP {

class GotoPlaceEvent : public IEvent {
public:
	enum GOTO_TYPE {
		GOTO_TYPE_NONE = 0,
		GOTO_ZONE = 1,
		GOTO_PLAYER = 2,
		GOTO_PLAYERS_APT = 3,
		GOTO_ZONE_COORDS = 4,
		GOTO_TUTORIAL_ZONE = 5,
		GOTO_SERVER = 6,
		GOTO_GAME = 7,
		GOTO_URL = 8,
		GOTO_TYPES
	};

	GotoPlaceEvent();

	// init fns
	void GotoPlayer(int playerServerId, LONG charIndex, const char* server = 0, ULONG port = 0);
	void GotoApartment(int playerServerId, LONG charIndex, const char* server = 0, ULONG port = 0);
	void GotoZone(ZoneIndex zi, ULONG instanceId, LONG charIndex, const char* server = 0, ULONG port = 0);
	// only used on server when sending to specific spot in zone
	void GotoZone(ZoneIndex zi, ULONG instanceId, LONG charIndex,
		double x, double y, double z, int rotation,
		const char* server = 0, ULONG port = 0);

	void GotoTutorialZone(LONG charIndex);

	// for STARs, like GotoZone, only don't specify valid destination zone.
	void GotoServer(LONG charIndex, const char* server, ULONG port);
	void GotoGame(LONG charIndex, const char* gameName, const char* url); // goto another star given the url

	// only used by client to pass up a URL. server will figure out where they're going from there (4/08)
	void GotoUrl(LONG charIndex, const char* url, ULONG port = 0, const char* server = 0, LONG parentGameId = 0, LONG _childGameId = 0);

	bool IsReconnecting() { return ObjectId() != 0; }

	void CloneForClient(const GotoPlaceEvent& src, bool reconnecting = false);

	void ExtractInfo();

	GOTO_TYPE m_type;
	int m_destKanevaUserId;

	ZoneIndex m_zi;
	LONG m_instanceId;
	std::string m_server;
	ULONG m_port;
	LONG m_charIndex;
	double m_x, m_y, m_z; // override coords
	int m_rotation;
	std::string m_gameName;
	std::string m_url;
	LONG m_childGameId;
	LONG m_parentGameId;
	bool m_asAdmin; // treat this as if admin travelling

	DEFINE_SECURE_CLASS_INFO(GotoPlaceEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
