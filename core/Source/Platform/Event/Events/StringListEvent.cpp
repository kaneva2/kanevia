///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "StringListEvent.h"

#include "Event/Base/IEncodingFactory.h"

namespace KEP {

DECLARE_CLASS_INFO(StringListEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(StringListEvent) {
}

} // namespace KEP