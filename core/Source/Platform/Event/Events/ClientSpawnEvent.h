///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"

namespace KEP {

class ClientSpawnEvent : public IEvent {
public:
	ClientSpawnEvent(long zoneIndex, long instanceId, float x, float y, float z) :
			IEvent(ClassId()) {
		SetFilter(zoneIndex);
		(*OutBuffer()) << instanceId << x << y << z;
	}

	void extractInfo(LONG &zoneIndex, long &instanceId, float &x, float &y, float &z) {
		zoneIndex = Filter();
		(*InBuffer()) >> instanceId >> x >> y >> z;
	}

	DEFINE_SECURE_CLASS_INFO(ClientSpawnEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
