///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class ZoneIndex;

// Event sent from web server to update a zone's spin down delay
class UpdateScriptZoneSpinDownDelayEvent : public IEvent {
public:
	UpdateScriptZoneSpinDownDelayEvent(const ZoneIndex& zoneIndex, int spinDownDelayMins, unsigned tiedServerId);

	// use more generic type getting the data out to avoid dragging stuff into CMP
	void ExtractInfo(ZoneIndex& zoneIndex, int& spinDownDelayMins, unsigned& tiedServerId);

	DEFINE_SECURE_CLASS_INFO(UpdateScriptZoneSpinDownDelayEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
