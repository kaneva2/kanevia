///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "DynamicObjectEvents.h"
#include "..\Base\IDispatcher.h"
#include "KEPException.h"
#include "InstanceId.h"

namespace KEP {

void AddDynamicObjectEvent::ExtractInfo(
	PlacementSource &source,
	int &ownerPlayerId,
	int &globalId,
	float &x, float &y, float &z,
	float &dx, float &dy, float &dz,
	float &sx, float &sy, float &sz,
	int &placementId,
	GLID &animGlidSpecial,
	int &assetId,
	std::string &texture,
	int &friendId,
	std::string &swf,
	std::string &swfParams,
	bool &attachable,
	bool &interactive,
	bool &playFlash,
	bool &collision,
	float &min_x, float &min_y, float &min_z,
	float &max_x, float &max_y, float &max_z,
	bool &derivable,
	int &inventory_sub_type,
	int &gameItemId) {
	source = (PlacementSource)ObjectId();
	ownerPlayerId = Filter();

	double _x, _y, _z, _dx, _dy, _dz, _sx, _sy, _sz;
	(*InBuffer()) >> _x >> _y >> _z >> _dx >> _dy >> _dz >> _sx >> _sy >> _sz >> placementId >> animGlidSpecial >> assetId >> texture >> friendId >> swf >> swfParams;

	try {
		(*InBuffer()) >> globalId;
	} catch (KEPException *e) {
		// the globalId wasn't present
		globalId = 0;
		e->Delete();
	}

	double _min_x, _min_y, _min_z, _max_x, _max_y, _max_z;
	try {
		(*InBuffer()) >> attachable >> interactive >> playFlash >> collision >> _min_x >> _min_y >> _min_z >> _max_x >> _max_y >> _max_z;
	} catch (KEPException *e) {
		attachable = false;
		interactive = false;
		playFlash = false;
		collision = false;
		_min_x = 0.0;
		_min_y = 0.0f;
		_min_z = 0.0;
		_max_x = 0.0;
		_max_y = 0.0f;
		_max_z = 0.0;
		e->Delete();
	}

	try {
		(*InBuffer()) >> derivable;
	} catch (KEPException *e) {
		derivable = false;
		e->Delete();
	}

	try {
		(*InBuffer()) >> inventory_sub_type;
	} catch (KEPException *e) {
		inventory_sub_type = IT_NORMAL;
		e->Delete();
	}

	try {
		(*InBuffer()) >> gameItemId;
	} catch (KEPException *e) {
		gameItemId = 0;
		e->Delete();
	}

	x = (float)_x;
	y = (float)_y;
	z = (float)_z;
	dx = (float)_dx;
	dy = (float)_dy;
	dz = (float)_dz;
	sx = (float)_sx;
	sy = (float)_sy;
	sz = (float)_sz;
	min_x = (float)_min_x;
	min_y = (float)_min_y;
	min_z = (float)_min_z;
	max_x = (float)_max_x;
	max_y = (float)_max_y;
	max_z = (float)_max_z;
}

DECLARE_CLASS_INFO(AddDynamicObjectEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(AddDynamicObjectEvent) {
}

DECLARE_CLASS_INFO(RotateDynamicObjectEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(RotateDynamicObjectEvent) {
}

DECLARE_CLASS_INFO(ScaleDynamicObjectEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(ScaleDynamicObjectEvent) {
}

DECLARE_CLASS_INFO(MoveDynamicObjectEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(MoveDynamicObjectEvent) {
}

DECLARE_CLASS_INFO(MoveAndRotateDynamicObjectEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(MoveAndRotateDynamicObjectEvent) {
}

DECLARE_CLASS_INFO(RemoveDynamicObjectEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(RemoveDynamicObjectEvent) {
}

DECLARE_CLASS_INFO(ControlDynamicObjectEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(ControlDynamicObjectEvent) {
}

DECLARE_CLASS_INFO(UpdateDynamicObjectTextureEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UpdateDynamicObjectTextureEvent) {
}

DECLARE_CLASS_INFO(SetDynamicObjectTexturePanningEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(SetDynamicObjectTexturePanningEvent) {
}

DECLARE_CLASS_INFO(UpdatePictureFrameFriendEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UpdatePictureFrameFriendEvent) {
}

// DRF - Added
DECLARE_CLASS_INFO(UpdateDynamicObjectMediaEvent)
EVENT_CONSTRUCTOR_PROTOTYPE(UpdateDynamicObjectMediaEvent) {
}

UpdateDynamicObjectMediaEvent::UpdateDynamicObjectMediaEvent(int placementId, const MediaParams &mediaParams, LONG zoneIndex, int networkAssignedId) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)placementId);
	SetFilter((ULONG)zoneIndex);
	(*OutBuffer())
		<< mediaParams.GetUrl().c_str()
		<< mediaParams.GetParams().c_str()
		<< mediaParams.GetVolume()
		<< mediaParams.GetRadiusAudio()
		<< mediaParams.GetRadiusVideo()
		<< mediaParams.GetPlaylistId()
		<< networkAssignedId
		<< static_cast<int>(mediaParams.GetAllowScriptAccess());
}

void UpdateDynamicObjectMediaEvent::ExtractInfo(int &placementId, MediaParams &mediaParams, int *networkAssignedId) {
	placementId = (int)ObjectId();
	std::string mUrl, mParams;
	double mVolPct;
	double mRadiusAudio;
	double mRadiusVideo;
	int mPlaylistId = 0;
	(*InBuffer()) >> mUrl >> mParams >> mVolPct >> mRadiusAudio >> mRadiusVideo >> mPlaylistId;
	mediaParams = MediaParams(mUrl, mParams, mVolPct, mRadiusAudio, mRadiusVideo, mPlaylistId);

	int netId = 0;
	(*InBuffer()) >> netId;

	if (networkAssignedId) {
		*networkAssignedId = netId;
	}

	// Try/catch for backward compatibility only, can be removed after release
	try {
		int allowScriptAccessInt = static_cast<int>(FlashScriptAccessMode::Never);
		(*InBuffer()) >> allowScriptAccessInt;
		mediaParams.SetAllowScriptAccess(static_cast<FlashScriptAccessMode>(allowScriptAccessInt));
	} catch (KEPException *ex) {
		mediaParams.SetAllowScriptAccess(FlashScriptAccessMode::Auto);
		delete ex;
	}
}

// DRF - Added
DECLARE_CLASS_INFO(UpdateDynamicObjectMediaVolumeAndRadiusEvent)
EVENT_CONSTRUCTOR_PROTOTYPE(UpdateDynamicObjectMediaVolumeAndRadiusEvent) {
}

DECLARE_CLASS_INFO(DynamicObjectMediaChangedEvent)
EVENT_CONSTRUCTOR_PROTOTYPE(DynamicObjectMediaChangedEvent) {
}

DynamicObjectMediaChangedEvent::DynamicObjectMediaChangedEvent(NETID from, const ZoneIndex &zoneIndex, const std::vector<int> &placementIds, const MediaParams &mediaParams) :
		IEvent(ClassId()) {
	SetFrom(from);
	SetFilter(zoneIndex.toLong());

	*OutBuffer()
		<< mediaParams.GetUrl().c_str()
		<< mediaParams.GetParams().c_str()
		<< mediaParams.GetVolume()
		<< mediaParams.GetRadiusAudio()
		<< mediaParams.GetRadiusVideo()
		<< mediaParams.GetPlaylistId();

	*OutBuffer() << placementIds.size();
	for (int pid : placementIds) {
		*OutBuffer() << pid;
	}
}

void DynamicObjectMediaChangedEvent::ExtractInfo(NETID &from, ZoneIndex &zoneIndex, std::vector<int> &placementIds, MediaParams &mediaParams) {
	from = From();
	zoneIndex = ZoneIndex(Filter());

	std::string mUrl, mParams;
	double mVolPct;
	double mRadiusAudio;
	double mRadiusVideo;
	int mPlaylistId = 0;
	*InBuffer() >> mUrl >> mParams >> mVolPct >> mRadiusAudio >> mRadiusVideo >> mPlaylistId;

	mediaParams = MediaParams(mUrl, mParams, mVolPct, mRadiusAudio, mRadiusVideo, mPlaylistId);

	size_t numPids;
	*InBuffer() >> numPids;
	placementIds.resize(numPids);
	for (size_t i = 0; i < numPids; i++) {
		*InBuffer() >> placementIds[i];
	}
}

DECLARE_CLASS_INFO(PlaceDynamicObjectAtPositionEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(PlaceDynamicObjectAtPositionEvent) {
}

DECLARE_CLASS_INFO(DynamicObjectInitializedEvent)

DERIVED_EVENT_CONSTRUCTOR_PROTOTYPE(DynamicObjectInitializedEvent, DynamicObjectNotificationEvent) {
}

DECLARE_CLASS_INFO(DynamicObjectInitDelayedEvent)

DERIVED_EVENT_CONSTRUCTOR_PROTOTYPE(DynamicObjectInitDelayedEvent, DynamicObjectNotificationEvent) {
}

DECLARE_CLASS_INFO(DynamicObjectLoadingFailedEvent)

DERIVED_EVENT_CONSTRUCTOR_PROTOTYPE(DynamicObjectLoadingFailedEvent, DynamicObjectNotificationEvent) {
}

void RegisterDynamicObjectEvents(IDispatcher &disp) {
	RegisterEvent<AddDynamicObjectEvent>(disp);
	RegisterEvent<RotateDynamicObjectEvent>(disp);
	RegisterEvent<MoveDynamicObjectEvent>(disp);
	RegisterEvent<MoveAndRotateDynamicObjectEvent>(disp);
	RegisterEvent<ScaleDynamicObjectEvent>(disp);
	RegisterEvent<RemoveDynamicObjectEvent>(disp);
	RegisterEvent<ControlDynamicObjectEvent>(disp);
	RegisterEvent<UpdateDynamicObjectTextureEvent>(disp);
	RegisterEvent<SetDynamicObjectTexturePanningEvent>(disp);
	RegisterEvent<UpdatePictureFrameFriendEvent>(disp);
	RegisterEvent<UpdateDynamicObjectMediaEvent>(disp); // drf - added
	RegisterEvent<UpdateDynamicObjectMediaVolumeAndRadiusEvent>(disp); // drf - added
	RegisterEvent<DynamicObjectMediaChangedEvent>(disp);
	RegisterEvent<PlaceDynamicObjectAtPositionEvent>(disp);
	RegisterEvent<DynamicObjectInitializedEvent>(disp);
	RegisterEvent<DynamicObjectInitDelayedEvent>(disp);
	RegisterEvent<DynamicObjectLoadingFailedEvent>(disp);
}

} // namespace KEP
