///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <SDKCommon.h>
#include <log4cplus/Logger.h>

#include "Event\Base\IEvent.h"
#include <KEPHelpers.h>

class CDeathObjectList;

namespace KEP {
class IDispatcher;

class DeathEvent : public IEvent {
public:
	// since from the client, server will know the who and what zone
	DeathEvent(ULONG zoneIndex, NETID killed, int killedNetworkAssignedId, NETID killer, int killerNetworkAssignedId,
		double x, double y, double z);

	void ExtractInfo(ULONG &zoneIndex, NETID &killed, int &killedNetworkAssignedId, NETID &killer, int &killerNetworkAssignedId,
		double &x, double &y, double &z);

	DEFINE_SECURE_CLASS_INFO(DeathEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline DeathEvent::DeathEvent(ULONG zoneIndex, NETID killed, int killedNetworkAssignedId, NETID killer, int killerNetworkAssignedId,
	double x, double y, double z) :
		IEvent(ClassId()) {
	(*OutBuffer()) << zoneIndex << killed << killedNetworkAssignedId << killer << killerNetworkAssignedId << x << y << z;
}

inline void DeathEvent::ExtractInfo(ULONG &zoneIndex, NETID &killed, int &killedNetworkAssignedId, NETID &killer, int &killerNetworkAssignedId,
	double &x, double &y, double &z) {
	(*InBuffer()) >> zoneIndex >> killed >> killedNetworkAssignedId >> killer >> killerNetworkAssignedId >> x >> y >> z;
}

} // namespace KEP
