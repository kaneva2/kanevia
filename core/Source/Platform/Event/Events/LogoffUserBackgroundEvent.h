#pragma once

#include "IBackgroundDBEvent.h"

namespace KEP {

class CPlayerObject;
class CSpawnCfgObj;

/// class to pass data back and forth between
/// foregraound and background threads
/// basically a glorified struct
class LogoffUserBackgroundEvent : public IBackgroundDBEvent {
public:
	LogoffUserBackgroundEvent(IN const char *_name, IN int _accountNumber, IN ULONG _kanevaUserId,
		IN const char *_currentIP, IN int _minPlayed,
		IN CPlayerObject *_player, ULONG _reason, IEvent *_replyEvent) :
			IBackgroundDBEvent(ClassId(), 0, 0, EP_NORMAL),
			ret(E_INVALIDARG),
			name(_name),
			accountNumber(_accountNumber),
			currentIP(_currentIP),
			minPlayed(_minPlayed),
			player(_player),
			reason(_reason),
			replyEvent(_replyEvent) {
		kanevaUserId = _kanevaUserId;
	}

	// inbound only
	std::string name;
	int accountNumber;
	std::string currentIP;
	int minPlayed;
	CPlayerObject *player;
	ULONG reason;
	IEvent *replyEvent;

	// in/out

	// passthru only, background doesn't use it

	// outbound only
	std::vector<std::string> broadcastListArray; //< for telling "logged off"
	LONG ret; //< return code from query

	DEFINE_DB_CLASS_INFO(LogoffUserBackgroundEvent, PackVersion(1, 0, 0, 0))

	~LogoffUserBackgroundEvent();
};

} // namespace KEP

