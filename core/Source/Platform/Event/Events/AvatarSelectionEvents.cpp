///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "AvatarSelectionEvents.h"

using namespace KEP;

DECLARE_CLASS_INFO(AvatarSelectionEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(AvatarSelectionEvent) {
	// do nothing here
}