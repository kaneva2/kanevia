///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class IDispatcher;
enum PlacementSource;

class AddSoundEvent : public IEvent {
public:
	AddSoundEvent(PlacementSource source, int soundGLID, int parentType, int parentId, int soundSlot, float x, float y, float z);
	void ExtractInfo(PlacementSource& source, int& soundGLID, int& parentType, int& parentId, int& soundSlot, float& x, float& y, float& z);

	DEFINE_SECURE_CLASS_INFO(AddSoundEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline AddSoundEvent::AddSoundEvent(PlacementSource source, int soundGLID, int parentType, int parentId, int soundSlot, float x, float y, float z) :
		IEvent(ClassId()) {
	SetFilter((ULONG)source);
	SetObjectId((OBJECT_ID)soundGLID);
	(*OutBuffer()) << parentType << parentId << soundSlot << x << y << z;
}

inline void AddSoundEvent::ExtractInfo(PlacementSource& source, int& soundGLID, int& parentType, int& parentId, int& soundSlot, float& x, float& y, float& z) {
	source = (PlacementSource)Filter();
	soundGLID = (int)ObjectId();
	(*InBuffer()) >> parentType >> parentId >> soundSlot >> x >> y >> z;
}

class RemoveSoundEvent : public IEvent {
public:
	RemoveSoundEvent(int parentType, int parentId, int soundSlot);

	void ExtractInfo(int& parentType, int& parentId, int& soundSlot);

	DEFINE_SECURE_CLASS_INFO(RemoveSoundEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline RemoveSoundEvent::RemoveSoundEvent(int parentType, int parentId, int soundSlot) :
		IEvent(ClassId()) {
	(*OutBuffer()) << parentType << parentId << soundSlot;
}

inline void RemoveSoundEvent::ExtractInfo(int& parentType, int& parentId, int& soundSlot) {
	(*InBuffer()) >> parentType >> parentId >> soundSlot;
}

class MoveSoundEvent : public IEvent {
public:
	MoveSoundEvent(int parentType, int parentId, int soundSlot, float x, float y, float z);

	void ExtractInfo(int& parentType, int& parentId, int& soundSlot, float& x, float& y, float& z);

	DEFINE_SECURE_CLASS_INFO(MoveSoundEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline MoveSoundEvent::MoveSoundEvent(int parentType, int parentId, int soundSlot, float x, float y, float z) :
		IEvent(ClassId()) {
	(*OutBuffer()) << parentType << parentId << soundSlot << x << y << z;
}

inline void MoveSoundEvent::ExtractInfo(int& parentType, int& parentId, int& soundSlot, float& x, float& y, float& z) {
	(*InBuffer()) >> parentType >> parentId >> soundSlot >> x >> y >> z;
}

class UpdateSoundEvent : public IEvent {
public:
	UpdateSoundEvent(int parentType, int parentId, int soundSlot);

	void ExtractInfo(int& parentType, int& parentId, int& soundSlot);

	DEFINE_SECURE_CLASS_INFO(UpdateSoundEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline UpdateSoundEvent::UpdateSoundEvent(int parentType, int parentId, int soundSlot) :
		IEvent(ClassId()) {
	(*OutBuffer()) << parentType << parentId << soundSlot;
}

inline void UpdateSoundEvent::ExtractInfo(int& parentType, int& parentId, int& soundSlot) {
	(*InBuffer()) >> parentType >> parentId >> soundSlot;
}

void RegisterSoundEvents(IDispatcher& disp);

} // namespace KEP
