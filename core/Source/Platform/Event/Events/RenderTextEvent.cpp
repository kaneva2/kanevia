///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "RenderTextEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(RenderTextEvent)

RenderTextEvent::RenderTextEvent(
	const std::string &textMsg,
	eChatType chatType,
	const char *toDistribution,
	const char *fromRecipient,
	eChatType chatTypeOrig) :
		IEvent(ClassId()) {
	// pack up the remainder into the buffer
	// we need to encode who sent it, since the existing text messages processing needs it
	(*OutBuffer()) << textMsg.c_str()
				   << (int)chatType;

	if (toDistribution != 0 && fromRecipient != 0) {
		(*OutBuffer()) << toDistribution << fromRecipient << (int)chatTypeOrig;
	}
}

EVENT_CONSTRUCTOR_PROTOTYPE(RenderTextEvent) {
}

void RenderTextEvent::ExtractInfo(
	std::string &text,
	eChatType &chatType,
	std::string &toDistribution,
	std::string &fromRecipient) {
	// decode the string
	// will throw!
	if (InBuffer()) {
		int temp;
		(*m_inBuffer) >> text >> temp;

		chatType = (eChatType)temp;

		if (chatType == eChatType::Private) {
			(*m_inBuffer) >> toDistribution >> fromRecipient >> temp;
			chatType = (eChatType)temp;
		}

		m_inBuffer->Rewind();
	}
}
