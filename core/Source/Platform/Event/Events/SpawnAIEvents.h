///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <string>

namespace KEP {

class StartAISpawnEvent : public IEvent {
public:
	StartAISpawnEvent(const char *spawnGenName, LONG channel, LONG maxSpawn, LONG percent, LONG aiCfgOverride);

	void ExtractInfo(std::string &spawnGenName, LONG &channel, LONG &maxSpawn, LONG &percent, LONG &aiCfgOverride);

	DEFINE_SECURE_CLASS_INFO(StartAISpawnEvent, PackVersion(1, 0, 0, 0), EP_HIGH + 1)
};

class StopAISpawnEvent : public IEvent {
public:
	StopAISpawnEvent(const char *spawnGenName, LONG channel);

	void ExtractInfo(std::string &spawnGenName, LONG &channel);

	DEFINE_SECURE_CLASS_INFO(StopAISpawnEvent, PackVersion(1, 0, 0, 0), EP_HIGH + 1)
};

} // namespace KEP
