///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"
#include "KepConstants.h"

namespace KEP {

/// class for passing params back for forth to DB background thd
/// for
class LoadItemsBackgroundEvent : public IBackgroundDBEvent {
public:
	LoadItemsBackgroundEvent(const std::vector<GLID> &globalids, IEvent *responseEvent = NULL, EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority) {
		m_response = responseEvent;
		for (std::vector<GLID>::const_iterator itGlid = globalids.begin(); itGlid != globalids.end(); ++itGlid) {
			m_glids.push_back(*itGlid);
		}
	}

	void getGlids(std::set<int> &result) const {
		for (unsigned int i = 0; i < m_glids.size(); i++) {
			result.insert((int)m_glids[i]);
		}
	}

	IEvent *getResponse() {
		return m_response;
	}

	void addGlid(GLID glid) {
		m_glids.push_back(glid);
	}

	DEFINE_DB_CLASS_INFO(LoadItemsBackgroundEvent, PackVersion(1, 0, 0, 0))
private:
	IEvent *m_response;
	std::vector<GLID> m_glids;
};

} // namespace KEP