///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"

namespace KEP {

class CStatBonusList;
class CPlayerObject;

class VerifyStatEvent : public IEvent {
public:
	// since from the client, server will know the who and what zone
	VerifyStatEvent(CPlayerObject *player, CStatBonusList *requiredStats);

	DEFINE_SECURE_CLASS_INFO(VerifyStatEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
