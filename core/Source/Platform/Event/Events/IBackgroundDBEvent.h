///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include "InstanceId.h"

namespace KEP {

class IBackgroundDBEvent : public IEvent {
public:
	ZoneIndex zoneIndex; // zoneIndex for this (BAD_INDEX if none)
	int kanevaUserId; // zero if none

protected:
	IBackgroundDBEvent(EVENT_ID id, ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IEvent(id, filter, objectId, priority),
			kanevaUserId(0),
			zoneIndex(INVALID_ZONE_INDEX) {}
};

class BackgroundDBReplyEvent : public IBackgroundDBEvent {
public:
	virtual ~BackgroundDBReplyEvent() {
		if (event)
			event->Delete();
	}

	// in/out
	IEvent *event; //< the event for sending back

	// out
	LONG ret; //< return code operation, 0 = good
protected:
	BackgroundDBReplyEvent(EVENT_ID id, ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority),
			event(0),
			ret(-1) {}
};

// slightly modified version of DEFINE_BASE_CLASS_INFO from IEvent
#define DEFINE_DB_CLASS_INFO(c, v)                                                                                                                     \
public:                                                                                                                                                \
	static void SetClassId(EVENT_ID id) { m_classId = id; }                                                                                            \
	static EVENT_ID ClassId() { return m_classId; }                                                                                                    \
                                                                                                                                                       \
private:                                                                                                                                               \
	static EVENT_ID m_classId;                                                                                                                         \
                                                                                                                                                       \
public:                                                                                                                                                \
	static inline const char *ClassName() { return #c; }                                                                                               \
	static inline ULONG ClassVersion() { return v; }                                                                                                   \
	static inline EVENT_PRIORITY ClassPriority() { return EP_NORMAL; }                                                                                 \
	static IEvent *CreateEvent(EVENT_ID /*id*/, EventHeader * /*eh*/, PCBYTE /*buffer*/, ULONG /*len*/, NETID /*from*/, EVENT_PRIORITY /*priority*/) { \
		jsAssert(false);                                                                                                                               \
		return 0;                                                                                                                                      \
	}

} // namespace KEP