///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class DisconnectedEvent : public IEvent {
public:
	DisconnectedEvent(NETID player, ULONG reason, LONG minutesLoggedOn);

	// set the minutes on rec'd event, this unpacks, and resets the minutes
	void SetMinutes(LONG minutesLoggedOn);

	void ExtractInfo(NETID &player, ULONG &reason, LONG &minutesLoggedOn);

	DEFINE_SECURE_CLASS_INFO(DisconnectedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
