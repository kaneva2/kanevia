///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

/***********************************************************
CLASS

	AutomationEvent
	
	event used by the automation tool to talk to scripts

DESCRIPTION

***********************************************************/
class AutomationEvent : public IEvent {
public:
	// filter values indicate the action
	static const ULONG AE_START_ZONING = 1; // eng->script indicator that zoning can start, logged on ok, etc
	static const ULONG AE_STOP_ZONING = 2; // eng->script indicator that zoning should stop
	static const ULONG AE_SHUTDOWN = 3; // script->eng script want to stop the engine

	AutomationEvent(ULONG filter) :
			IEvent(ClassId()) { SetFilter(filter); }

	DEFINE_SECURE_CLASS_INFO(AutomationEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP

