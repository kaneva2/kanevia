#pragma once

#include "KEPConstants.h"
#include "IBackgroundDBEvent.h"
#include <instanceid.h>

namespace KEP {

/// class for passing params back for forth to DB background thd
/// for Dynamic object actions
class CanSpawnToZoneBackgroundEvent : public IBackgroundDBEvent {
public:
	CanSpawnToZoneBackgroundEvent(IN NETID from,
		IN ULONG _kanevaUserId,
		IN bool _gm,
		IN const char* _country,
		IN const char* _birthDate,
		IN bool _showMature,
		IN int _selectedChar,
		IN int _playerId,
		IN int _altCfgIndex,
		IN int _EDBIndex,
		IN const ZoneIndex& _zi,
		IN const Vector3f& _spawnPt,
		IN int _rotation,
		IN bool _goingToPlayer,
		IN const char* _url,
		IN bool _sendHomeIfFail) :
			IBackgroundDBEvent(ClassId(), 0, 0, EP_NORMAL),
			ret(0),
			gm(_gm),
			country(_country),
			birthDate(_birthDate),
			playerId(_playerId),
			altCfgIndex(_altCfgIndex),
			EDBIndex(_EDBIndex),
			zoneIndex(_zi),
			spawnPt(_spawnPt),
			rotation(_rotation),
			goingToPlayer(_goingToPlayer),
			url(_url ? _url : ""),
			sendHomeIfFail(_sendHomeIfFail),
			isAsync(true),
			selectedChar(_selectedChar),
			newServerId(0),
			scriptServerId(0),
			coverCharge(0),
			zoneOwnerId(0),
			showMature(_showMature),
			zonePermissions(ZP_UNKNOWN),
			parentZoneInstanceId(0) {
		SetFrom(from);
		SetObjectId(_kanevaUserId);
	}

	// inbound only
	int playerId;
	ZoneIndex zoneIndex;
	Vector3f spawnPt;
	int rotation;
	bool goingToPlayer;
	bool sendHomeIfFail;
	std::string country;
	std::string birthDate;
	bool showMature;

	// in/out
	std::string url;
	bool gm;

	// pass thru
	bool isAsync; //< was this called async, or sync
	int altCfgIndex;
	int EDBIndex;
	int selectedChar;

	// outbound only
	int newServerId; //< if switching servers, new server id
	int scriptServerId;
	LONG coverCharge;
	ULONG zoneOwnerId; //< if coverchange, owner who gets money
	BYTE zonePermissions; //< player permission in zone
	unsigned parentZoneInstanceId;
	int ret;

	DEFINE_DB_CLASS_INFO(CanSpawnToZoneBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP