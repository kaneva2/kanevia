///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "GetAttributeEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(GetAttributeEvent)

GetAttributeEvent::GetAttributeEvent(const ATTRIB_DATA& attribData) :
		IEvent(ClassId()) {
	m_header.SetFilter((ULONG)attribData.aeType);

	// pack up the remainder into the buffer
	// we need to encode who sent it, since the existing text messages processing needs it
	(*OutBuffer()) << (short)attribData.aeType;
	(*OutBuffer()) << attribData.aeInt1;
	(*OutBuffer()) << attribData.aeShort1; // usually netTraceId
	(*OutBuffer()) << attribData.aeInt2;
	(*OutBuffer()) << attribData.aeShort2;
}

EVENT_CONSTRUCTOR_PROTOTYPE(GetAttributeEvent) {}

void GetAttributeEvent::GetValues(ATTRIB_DATA& attribData) {
	if (InBuffer()) {
		short aeType;
		(*m_inBuffer) >> aeType;
		(*m_inBuffer) >> attribData.aeInt1;
		(*m_inBuffer) >> attribData.aeShort1; // usually netTraceId
		(*m_inBuffer) >> attribData.aeInt2;
		(*m_inBuffer) >> attribData.aeShort2;
		m_inBuffer->Rewind();
		attribData.aeType = (eAttribEventType)aeType;
	}
}
