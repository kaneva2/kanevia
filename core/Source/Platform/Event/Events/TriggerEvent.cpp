///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include <jsEnumFiles.h>
#include "TriggerEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(TriggerEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(TriggerEvent) {
}

DECLARE_CLASS_INFO(AddTriggerEvent)
DECLARE_CLASS_INFO(RemoveTriggerEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(AddTriggerEvent) {
}

EVENT_CONSTRUCTOR_PROTOTYPE(RemoveTriggerEvent) {
}
