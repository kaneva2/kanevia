///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RefreshPlayerBackgroundEvent.h"
#include "CInventoryClass.h"

// these are in own cpp file so can be included in server engine w/o sucking
// in CInventoryClass.h to the header file

namespace KEP {

void RefreshPlayerBackgroundEvent::PrepareToReQueue(int _invCommitCount, int prevPlayerCash) {
	if (inventory) {
		inventory->SafeDelete();
		delete inventory;
		inventory = 0;
	}
	firstTime = 0;
	arraySize = 0; // parent's count of valid result sets

	invCommitCount = _invCommitCount;
	prevAmount = prevPlayerCash;
}

RefreshPlayerBackgroundEvent::~RefreshPlayerBackgroundEvent() {
	if (inventory) {
		inventory->SafeDelete();
		delete inventory;
	}
}

} // namespace KEP