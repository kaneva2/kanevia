#pragma once

#define BACKLOAD_INVENTORY

#include "GetResultSetsEvent.h"
namespace mysqlpp {
class StoreQueryResult;
}

namespace KEP {

/// class for more generic getting one or more result sets from the database
class MySQLGetResultSetsEvent : public IBackgroundDBEvent {
public:
#ifdef BACKLOAD_INVENTORY
	enum {
		PLAYERS_RS = 0,
		PLAYERS_PRESENCES_RS,
		ZONES_RS,
		INVENTORIES_RS,
		DEFCFG_RS,
		CASH_RS,
		GIFT_RS,
		BANK_RS,
		SKILL_RS,
		STAT_RS,
		GETSET_RS,
		NOTERIETY_RS,
		CLAN_RS,
		PASSLIST_RS,
		NAME_COLOR,
		ITEMS_RS,
		PASSES_RS
	} ResultSetIndex;
#endif

	static const int MAX_RESULT_SETS = 25;

	int arraySize; //< currently used array items

	mysqlpp::StoreQueryResult* results; // if get from DB
#ifdef BACKLOAD_INVENTORY
	// These are pointers to KGenericObjList<CItemObj> but we reference them as void*
	// to avoid include conflicts between mfc.h and windows.h.  Not pretty.  But
	// expedient
	//
	void* pPlayerInventory;
	void* pBankInventory;
#endif

	std::string resultXml; // if get from Web

	// outbound only
	int ret;

protected:
	MySQLGetResultSetsEvent(IN EVENT_ID id, IN ULONG filter = 0, IN OBJECT_ID objectId = 0, IN EVENT_PRIORITY priority = EP_NORMAL);

	~MySQLGetResultSetsEvent();
};

} // namespace KEP