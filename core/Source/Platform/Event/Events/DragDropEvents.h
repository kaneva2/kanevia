///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "IGetSet.h"
#include <string>

namespace KEP {

class DragDropEvent : public IEvent {
public:
	DragDropEvent(bool aInitial, bool aDropped, int aType, const std::string& aFullPath, int aMouseX, int aMouseY, DWORD aKeyState);
	void ExtractInfo(bool& aInitial, bool& aDropped, int& aType, std::string& aFullPath, int& aMouseX, int& aMouseY, DWORD& aKeyState);

	DEFINE_SECURE_CLASS_INFO(DragDropEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

class DragLeaveEvent : public IEvent {
public:
	DragLeaveEvent();

	DEFINE_SECURE_CLASS_INFO(DragLeaveEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
