///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <SDKCommon.h>
#include <string>
#include <InstanceId.h>

namespace KEP {

class ChannelImportEvent : public IEvent {
public:
	ChannelImportEvent();

	void ExtractInfo();

	int m_dstZoneIndex;
	int m_dstInstanceId;
	int m_baseZoneIndexPlain;
	int m_srcZoneIndex;
	int m_srcInstanceId;
	bool m_maintainScripts;

	DEFINE_SECURE_CLASS_INFO(ChannelImportEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
