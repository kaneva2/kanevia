///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class SystemServiceEvent : public IEvent {
public:
	SystemServiceEvent(const std::string& userId);

	void ExtractInfo(std::string& userId);

	DEFINE_CLASS_INFO(SystemServiceEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
