///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <string>

namespace KEP {

class GenericEvent : public IEvent {
public:
	DEFINE_GENERIC_CLASS_INFO(GenericEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
