///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "TryOnExpireEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(TryOnExpireEvent)

TryOnExpireEvent::TryOnExpireEvent() :
		IEvent(ClassId()),
		type(0),
		m_playerId(0),
		m_glid(0),
		m_baseGlid(0),
		m_useType(USE_TYPE_NONE),
		m_expireDuration(0),
		m_dynamic_obj_placement_id(0) {
}

void TryOnExpireEvent::InformExpiration(int playerId, int glid, int baseGlid, const USE_TYPE& useType, int expireDuration, int dynamic_obj_placement_id) {
	SetFilter(INFORM_EXPIRATION);
	(*OutBuffer()) << playerId << glid << baseGlid << (int)useType << expireDuration << dynamic_obj_placement_id;
}

EVENT_CONSTRUCTOR_PROTOTYPE(TryOnExpireEvent), type(0), m_playerId(0), m_glid(0), m_baseGlid(0), m_useType(USE_TYPE_NONE), m_expireDuration(0), m_dynamic_obj_placement_id(0) {
}

void TryOnExpireEvent::ExtractInfo() {
	m_playerId = 0;
	m_glid = 0;
	m_useType = USE_TYPE_NONE;
	m_expireDuration = 0;
	m_dynamic_obj_placement_id = 0;

	type = Filter();

	InBuffer();

	if (m_inBuffer) {
		switch (type) {
			case INFORM_EXPIRATION: {
				int useTypeInt = USE_TYPE_NONE;
				(*m_inBuffer) >> m_playerId >> m_glid >> m_baseGlid >> useTypeInt >> m_expireDuration >> m_dynamic_obj_placement_id;
				m_useType = (USE_TYPE)useTypeInt;
			} break;

			default:
				jsVerifyReturnVoid(false);
				break;
		}
	}
}
