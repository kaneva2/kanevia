///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "ChannelImportEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(ChannelImportEvent)

ChannelImportEvent::ChannelImportEvent() :
		IEvent(ClassId()), m_dstZoneIndex(0), m_dstInstanceId(0), m_baseZoneIndexPlain(0), m_srcZoneIndex(0), m_srcInstanceId(0), m_maintainScripts(0) {
}

EVENT_CONSTRUCTOR_PROTOTYPE(ChannelImportEvent)
, m_dstZoneIndex(0), m_dstInstanceId(0), m_baseZoneIndexPlain(0), m_srcZoneIndex(0), m_srcInstanceId(0), m_maintainScripts(0) {
}

void ChannelImportEvent::ExtractInfo() {
	m_dstZoneIndex = 0;
	m_dstInstanceId = 0;
	m_baseZoneIndexPlain = 0;
	m_srcZoneIndex = 0;
	m_srcInstanceId = 0;
	m_maintainScripts = false;

	InBuffer();
	if (m_inBuffer) {
		(*m_inBuffer) >> m_dstZoneIndex >> m_dstInstanceId >> m_baseZoneIndexPlain >> m_srcZoneIndex >> m_srcInstanceId >> m_maintainScripts;
	}
}
