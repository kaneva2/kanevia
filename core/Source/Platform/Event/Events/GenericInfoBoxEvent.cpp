///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "GenericInfoBoxEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(GenericInfoBoxEvent)

/**
* Send out an event to the menu system for a message box. 
* This will allow error conditions not to interrupt the render 
* loop and cause a disconnect of the client. 
*/
GenericInfoBoxEvent::GenericInfoBoxEvent() :
		IEvent(ClassId()),
		type(0),
		m_msg("") {
}

/**
* Simple message with ok button. 
*/
void GenericInfoBoxEvent::Msg1(const char* msg) {
	SetFilter(MSG1);
	(*OutBuffer()) << msg;
}

EVENT_CONSTRUCTOR_PROTOTYPE(GenericInfoBoxEvent), type(0), m_msg("") {
}

void GenericInfoBoxEvent::ExtractInfo() {
	m_msg.clear();

	type = Filter();

	InBuffer();

	if (m_inBuffer) {
		switch (type) {
			case MSG1:
				(*m_inBuffer) >> m_msg;
				break;

			default:
				jsVerifyReturnVoid(false);
				break;
		}
	}
}
