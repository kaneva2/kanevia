#pragma once

#include "IBackgroundDBEvent.h"

namespace KEP {

/// class to pass data back and forth between
/// foreground and background threads
/// basically a glorified struct
class BroadcastEventBackgroundEvent : public BackgroundDBReplyEvent {
public:
	BroadcastEventBackgroundEvent(IN IEvent *e, IN std::vector<ULONG> *_destKanevaIds, IN ULONG _kanevaUserId) :
			BackgroundDBReplyEvent(ClassId(), 0, 0, EP_NORMAL),
			destKanevaIds(_destKanevaIds) {
		kanevaUserId = _kanevaUserId;
		event = e;
	}
	~BroadcastEventBackgroundEvent() {
		delete destKanevaIds;
	}

	// inbound only
	std::vector<ULONG> *destKanevaIds;

	// outbound in parent's ret and event members

	DEFINE_DB_CLASS_INFO(BroadcastEventBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP