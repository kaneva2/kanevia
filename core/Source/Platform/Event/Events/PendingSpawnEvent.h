///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <vector>

namespace KEP {

class PendingSpawnEvent : public IEvent {
public:
	PendingSpawnEvent(long zoneIndex, unsigned short id, NETID dest, bool initialSpawn = false, long coverCharge = 0, long zoneInstanceId = 0) :
			IEvent(ClassId()) {
		SetFilter((ULONG)zoneIndex);
		SetObjectId((OBJECT_ID)id);
		if (dest != 0)
			AddTo(dest);
		(*OutBuffer()) << initialSpawn << coverCharge << zoneInstanceId;
	}

	void extractInfo(LONG &zoneIndex, USHORT &id, bool &initialSpawn, long &coverCharge, long &zoneInstanceId) {
		zoneIndex = (LONG)Filter();
		id = (USHORT)ObjectId();
		(*InBuffer()) >> initialSpawn;
		(*InBuffer()) >> coverCharge;
		(*InBuffer()) >> zoneInstanceId;
	}

	DEFINE_SECURE_CLASS_INFO(PendingSpawnEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
