///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "TextEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(TextEvent)

TextEvent::TextEvent(
	const char *textMsg,
	SlashCommand commandId,
	int fromNetworkAssignedId,
	eChatType chatType,
	const char *toDistribution,
	const char *fromRecipient,
	eChatType chatTypeOrig,
	bool fromIMClient) :
		IEvent(ClassId()) {
	jsVerifyDo(textMsg != 0, return );

	m_header.SetFilter(static_cast<ULONG>(commandId));
	SetObjectId((OBJECT_ID)(fromIMClient ? 1 : 0));

	// pack up the remainder into the buffer
	// we need to encode who sent it, since the existing text messages processing needs it

	(*OutBuffer()) << (ULONG)fromNetworkAssignedId
				   << textMsg
				   << (int)chatType;

	if (chatType == eChatType::Private || chatType == eChatType::S2S) {
		jsVerifyDo(toDistribution != 0, return );
		jsVerifyDo(fromRecipient != 0, return );
		(*OutBuffer()) << toDistribution << fromRecipient;
		if (chatType == eChatType::Private)
			(*OutBuffer()) << (int)eChatType::Private;
		else
			(*OutBuffer()) << (int)chatTypeOrig;
	}
}

EVENT_CONSTRUCTOR_PROTOTYPE(TextEvent) {
}

void TextEvent::ExtractInfo(
	SlashCommand &commandId,
	int &networkAssignedId,
	std::string &text,
	eChatType &chatType,
	std::string *distro,
	std::string *from,
	eChatType *pChatTypeOrig,
	bool *fromIMClient) {
	commandId = static_cast<SlashCommand>(Filter());

	if (fromIMClient)
		*fromIMClient = (ObjectId() != 0);

	// decode the string
	// will throw!
	InBuffer();
	if (m_inBuffer) {
		int temp;
		(*m_inBuffer) >> networkAssignedId >> text >> temp;

		chatType = (eChatType)temp;
		if (chatType == eChatType::Private || chatType == eChatType::S2S) {
			if (distro && from) {
				(*m_inBuffer) >> *distro >> *from;
				if (pChatTypeOrig) {
					(*m_inBuffer) >> temp;
					*pChatTypeOrig = (eChatType)temp;
				}
			}
		}

		m_inBuffer->Rewind();
	}
}
