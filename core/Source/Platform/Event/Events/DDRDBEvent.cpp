///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <js.h>
#include "DDRDBEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(DDRDBEvent)

DDRDBEvent::DDRDBEvent() :
		IEvent(ClassId()), m_zoneIndex(0), type(0), m_gameId(0), m_playerId(0) {
}

EVENT_CONSTRUCTOR_PROTOTYPE(DDRDBEvent) {
}

void DDRDBEvent::DBRecordScore(bool isPermanentZone, ULONG objectId, ULONG zoneIndex,
	int dynObjectId, ULONG gameId, int playerId,
	const char* playerName, ULONG highScore,
	ULONG score, ULONG numPerfects, ULONG numPerfectsInARow) {
	SetFilter(DDRDB_RECORD_SCORE);
	(*OutBuffer()) << isPermanentZone << objectId << zoneIndex << dynObjectId << gameId << playerId << playerName << highScore << score << numPerfects << numPerfectsInARow;
}

void DDRDBEvent::LoadGameData(ULONG gameId, GameLevelsMap* outData) {
	SetFilter(DDRDB_LOAD_GAME_DATA);
	(*OutBuffer()) << gameId << (LONG)outData;
}

void DDRDBEvent::LoadPlayerData(ULONG gameId, ULONG gameLevel, NETID from, bool newGame, int dynObjectId, int playerId,
	ULONG zoneIndex, ULONG objectId) {
	SetFrom(from);
	SetFilter(DDRDB_LOAD_PLAYER_DATA);

	m_dynObjectId = dynObjectId;
	m_gameId = gameId;
	m_gameLevel = gameLevel;
	m_newGame = newGame;
	m_playerId = playerId;
	m_zoneIndex = zoneIndex;
	m_objectId = objectId;
}

void DDRDBEvent::ExtractInfo() {
	type = Filter();

	//ULONG len = 0;
	//PCBYTE buff = 0;

	InBuffer();

	if (m_inBuffer) {
		switch (type) {
			case DDRDB_RECORD_SCORE:
				(*m_inBuffer) >> m_isPermanentZone >> m_objectId >> m_zoneIndex >> m_dynObjectId >> m_gameId >> m_playerId >> m_playerName >> m_highScore >> m_score >> m_numPerfects >> m_numPerfectsInARow;
				break;
			case DDRDB_LOAD_GAME_DATA:
				LONG ptrToMap;
				(*m_inBuffer) >> m_gameId >> (LONG)ptrToMap;
				m_gameLevelsMap = (GameLevelsMap*)ptrToMap;
				break;
			case DDRDB_LOAD_PLAYER_DATA:
				// using member variables, no need to serialize
				break;
			default:
				jsVerifyReturnVoid(false);
				break;
		}
	}
}
