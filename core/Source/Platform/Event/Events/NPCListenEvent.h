///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "Event/Base/EventRegistry.h"
#include "InstanceId.h"
#include "ChatType.h"
#include <vector>

namespace KEP {

class NPCListenEvent : public IEvent {
public:
	NPCListenEvent(const char* msg, eChatType chatType, ChannelId channel, short radius, float x, float y, float z, const std::vector<int>& ids);

	DEFINE_SECURE_CLASS_INFO(NPCListenEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
