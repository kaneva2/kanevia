#pragma once

#include "IBackgroundDBEvent.h"

namespace KEP {

class CSpawnCfgObj;

/// class to pass data back and forth between
/// foregraound and background threads
/// basically a glorified struct
class ChangeZoneBackgroundEvent : public IBackgroundDBEvent {
public:
	ChangeZoneBackgroundEvent(IN EVENT_ID id = ClassId(), IN ULONG filter = 0, IN OBJECT_ID objectId = 0, IN EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority),
			ret(E_INVALIDARG),
			globalId(0),
			invType(0),
			changeType(0),
			spawnPointCfg(0),
			playerId(0),
			url(""),
			reAddInvOnFail(true) {}

	// inbound only
	int playerId;

	// in/out
	ZoneIndex newZoneIndex;
	ZoneIndex oldZoneIndex;
	LONG changeType; //< apt or channel
	std::string charName;
	short invType; //< IT_* type (gift, reward, etc.)

	// passthru only, background doesn't use it
	int globalId; //< of deed
	CSpawnCfgObj *spawnPointCfg;

	// outbound only
	LONG ret; //< return code from query
	std::string url; //< url built from query

	bool reAddInvOnFail; //if the upgrade fails do we want to return the item to inventory

	DEFINE_DB_CLASS_INFO(ChangeZoneBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP