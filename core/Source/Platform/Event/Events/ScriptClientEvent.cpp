///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <js.h>

#include "ScriptClientEvent.h"

#include "KEPHelpers.h"
#include "KEPConstants.h"

using namespace KEP;

DECLARE_CLASS_INFO(ScriptClientEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(ScriptClientEvent) {
}

ScriptClientEvent::ScriptClientEvent(EventType eventType, NETID clientNetId, DestinationScope destScope) :
		IEvent(ClassId()),
		m_routingOption(Routing_Normal) {
	SetEventType(eventType);
	SetFrom(clientNetId);
	m_destinationScope = destScope;
}

ScriptClientEvent::ScriptClientEvent(ScriptClientEvent* sce, DestinationScope destScope) :
		IEvent(ClassId()),
		m_routingOption(Routing_Normal) {
	PCBYTE buff = 0;
	ULONG len = 0;
	sce->GetOutputBufferNoHeader(buff, len);
	OutBuffer()->CopyBuffer(buff, len);
	m_timeQueued = fTime::TimeMs();
	SetFilter(sce->Filter());
	SetEventId(sce->EventId());
	SetObjectId(sce->ObjectId());
	SetFrom(sce->From());
	SetRoutingOption(sce->GetRoutingOption());
	m_destinationScope = destScope;
}

ScriptClientEvent* ScriptClientEvent::EncodeObjectGenerate(ScriptClientEvent* sce, ULONG objId, ULONG globalId, LONG zoneId, LONG instanceId, double x, double y, double z, double dx, double dy, double dz, int placementType, int gameItemId) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::ObjectGenerate, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectGenerate, 0);
	*sce->OutBuffer() << objId << globalId << zoneId << instanceId << x << y << z << dx << dy << dz << placementType << gameItemId;
	return sce;
}

void ScriptClientEvent::DecodeObjectGenerate(ScriptClientEvent* sce, ULONG& objId, ULONG& globalId, LONG& zoneId, LONG& instanceId, double& x, double& y, double& z, double& dx, double& dy, double& dz, int& placementType, int& gameItemId) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::ObjectGenerate);
	*sce->InBuffer() >> objId >> globalId >> zoneId >> instanceId >> x >> y >> z >> dx >> dy >> dz >> placementType >> gameItemId;
}

ScriptClientEvent* ScriptClientEvent::EncodeOwnedObjectGenerate(ScriptClientEvent* sce, ULONG objId, ULONG globalId, LONG zoneId, LONG instanceId, double x, double y, double z, double dx, double dy, double dz, int placementType, int gameItemId, int ownerId) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::OwnedObjectGenerate, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::OwnedObjectGenerate, 0);
	*sce->OutBuffer() << objId << globalId << zoneId << instanceId << x << y << z << dx << dy << dz << placementType << gameItemId << ownerId;
	return sce;
}

void ScriptClientEvent::DecodeOwnedObjectGenerate(ScriptClientEvent* sce, ULONG& objId, ULONG& globalId, LONG& zoneId, LONG& instanceId, double& x, double& y, double& z, double& dx, double& dy, double& dz, int& placementType, int& gameItemId, int& ownerId) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::OwnedObjectGenerate);
	*sce->InBuffer() >> objId >> globalId >> zoneId >> instanceId >> x >> y >> z >> dx >> dy >> dz >> placementType >> gameItemId >> ownerId;
}

ScriptClientEvent* ScriptClientEvent::EncodeObjectMove(ScriptClientEvent* sce, ULONG objId, LONG zoneId, LONG instanceId, double x, double y, double z, double timeMs, double accDur, double constVelDur, double accDistFrac, double constVelDistFrac) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::ObjectMove, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectMove, 0);
	*sce->OutBuffer() << objId << zoneId << instanceId << x << y << z << timeMs << accDur << constVelDur << accDistFrac << constVelDistFrac;
	return sce;
}

void ScriptClientEvent::DecodeObjectMove(ScriptClientEvent* sce, ULONG& objId, LONG& zoneId, LONG& instanceId, double& x, double& y, double& z, double& timeMs, double& accDur, double& constVelDur, double& accDistFrac, double& constVelDistFrac) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::ObjectMove);
	*sce->InBuffer() >> objId >> zoneId >> instanceId >> x >> y >> z >> timeMs >> accDur >> constVelDur >> accDistFrac >> constVelDistFrac;
}

ScriptClientEvent* ScriptClientEvent::EncodeObjectRotate(ScriptClientEvent* sce, ULONG objId, LONG zoneId, LONG instanceId, double dx, double dy, double dz, double sx, double sy, double sz, double timeMs) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::ObjectRotate, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectRotate, 0);
	*sce->OutBuffer() << objId << zoneId << instanceId << dx << dy << dz << sx << sy << sz << timeMs;
	return sce;
}

void ScriptClientEvent::DecodeObjectRotate(ScriptClientEvent* sce, ULONG& objId, LONG& zoneId, LONG& instanceId, double& dx, double& dy, double& dz, double& sx, double& sy, double& sz, double& timeMs) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::ObjectRotate);
	*sce->InBuffer() >> objId >> zoneId >> instanceId >> dx >> dy >> dz >> sx >> sy >> sz >> timeMs;
}

ScriptClientEvent* ScriptClientEvent::EncodeObjectDelete(ScriptClientEvent* sce, ULONG objId, LONG zoneId, LONG instanceId) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::ObjectDelete, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectDelete, 0);
	*sce->OutBuffer() << objId << zoneId << instanceId;
	return sce;
}

void ScriptClientEvent::DecodeObjectDelete(ScriptClientEvent* sce, ULONG& objId, LONG& zoneId, LONG& instanceId) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::ObjectDelete);
	*sce->InBuffer() >> objId >> zoneId >> instanceId;
}

ScriptClientEvent* ScriptClientEvent::EncodeObjectSetParticle(ScriptClientEvent* sce, ULONG clientId, ULONG objId, LONG zoneId, LONG instanceId, int glid, const std::string& bone, int slotId, double ofsX, double ofsY, double ofsZ, double dirX, double dirY, double dirZ, double upX, double upY, double upZ) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::ObjectSetParticle, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetParticle, clientId);
	*sce->OutBuffer() << objId << zoneId << instanceId << glid << bone << slotId << ofsX << ofsY << ofsZ << dirX << dirY << dirZ << upX << upY << upZ;
	return sce;
}

void ScriptClientEvent::DecodeObjectSetParticle(ScriptClientEvent* sce, ULONG& clientId, ULONG& objId, LONG& zoneId, LONG& instanceId, int& glid, std::string& bone, int& slotId, double& ofsX, double& ofsY, double& ofsZ, double& dirX, double& dirY, double& dirZ, double& upX, double& upY, double& upZ) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::ObjectSetParticle);
	*sce->InBuffer() >> objId >> zoneId >> instanceId >> glid >> bone >> slotId >> ofsX >> ofsY >> ofsZ >> dirX >> dirY >> dirZ >> upX >> upY >> upZ;

	clientId = sce->From();
}

ScriptClientEvent* ScriptClientEvent::EncodeObjectRemoveParticle(ScriptClientEvent* sce, ULONG clientId, ULONG objId, LONG zoneId, LONG instanceId, const std::string& bone, int slotId) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::ObjectRemoveParticle, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectRemoveParticle, clientId);
	*sce->OutBuffer() << objId << zoneId << instanceId << bone << slotId;
	return sce;
}

void ScriptClientEvent::DecodeObjectRemoveParticle(ScriptClientEvent* sce, ULONG& clientId, ULONG& objId, LONG& zoneId, LONG& instanceId, std::string& bone, int& slotId) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::ObjectRemoveParticle);
	*sce->InBuffer() >> objId >> zoneId >> instanceId >> bone >> slotId;

	clientId = sce->From();
}

ScriptClientEvent* ScriptClientEvent::EncodeSoundAttachTo(ScriptClientEvent* sce, ULONG soundPlacementId, ULONG targetId, int attachType, LONG zoneId) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::SoundAttachTo, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::SoundAttachTo, 0);
	*sce->OutBuffer() << soundPlacementId << targetId << attachType << zoneId;
	return sce;
}

void ScriptClientEvent::DecodeSoundAttachTo(ScriptClientEvent* sce, ULONG& soundPlacementId, ULONG& targetId, int& attachType, LONG& zoneId) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::SoundAttachTo);
	*sce->InBuffer() >> soundPlacementId >> targetId >> attachType >> zoneId;
}

ScriptClientEvent* ScriptClientEvent::EncodeSoundAddModifier(ScriptClientEvent* sce, ZoneIndex zoneId, int soundPlacementId, int input, int output, double gain, double offset) {
	jsVerifyReturn(sce == NULL || sce->GetEventType() == ScriptClientEvent::SoundAddModifier, NULL);
	if (sce == NULL)
		sce = new ScriptClientEvent(ScriptClientEvent::SoundAddModifier, 0);
	*sce->OutBuffer()
		<< zoneId.toLong()
		<< soundPlacementId
		<< input
		<< output
		<< gain
		<< offset;
	return sce;
}

void ScriptClientEvent::DecodeSoundAddModifier(ScriptClientEvent* sce, ZoneIndex& zoneId, int& soundPlacementId, int& input, int& output, double& gain, double& offset) {
	jsVerifyReturnVoid(sce != NULL && sce->GetEventType() == ScriptClientEvent::SoundAddModifier);
	long zoneIdLong;
	*sce->InBuffer() >> zoneIdLong >> soundPlacementId >> input >> output >> gain >> offset;
	zoneId = ZoneIndex(zoneIdLong);
}

// DRF - Added
ScriptClientEvent* ScriptClientEvent::EncodeObjectSetMedia(ScriptClientEvent* sce, ZoneIndex zoneId, ULONG objId, const MediaParams& mediaParams) {
	jsVerifyReturn(!sce || sce->GetEventType() == ScriptClientEvent::ObjectSetMedia, NULL);
	if (!sce)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetMedia, 0);
	*sce->OutBuffer()
		<< zoneId.toLong()
		<< objId
		<< mediaParams.GetUrl()
		<< mediaParams.GetParams()
		<< mediaParams.GetVolume()
		<< mediaParams.GetRadiusAudio()
		<< mediaParams.GetRadiusVideo();
	return sce;
}

// DRF - Added
void ScriptClientEvent::DecodeObjectSetMedia(ScriptClientEvent* sce, ZoneIndex& zoneId, ULONG& objId, MediaParams& mediaParams) {
	jsVerifyReturnVoid(sce && sce->GetEventType() == ScriptClientEvent::ObjectSetMedia);
	long zoneIdLong;
	std::string mUrl, mParams;
	double mVolPct, mRadAud, mRadVid;
	*sce->InBuffer() >> zoneIdLong >> objId >> mUrl >> mParams >> mVolPct >> mRadAud >> mRadVid;
	zoneId = ZoneIndex(zoneIdLong);
	mediaParams = MediaParams(mUrl, mParams, mVolPct, mRadAud, mRadVid);
}

// DRF - Added
ScriptClientEvent* ScriptClientEvent::EncodeObjectSetMediaVolumeAndRadius(ScriptClientEvent* sce, ZoneIndex zoneId, ULONG objId, const MediaParams& mediaParams) {
	jsVerifyReturn(!sce || sce->GetEventType() == ScriptClientEvent::ObjectSetMediaVolumeAndRadius, NULL);
	if (!sce)
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetMediaVolumeAndRadius, 0);
	*sce->OutBuffer()
		<< zoneId.toLong()
		<< objId
		<< mediaParams.GetVolume()
		<< mediaParams.GetRadiusAudio()
		<< mediaParams.GetRadiusVideo();
	return sce;
}

// DRF - Added
void ScriptClientEvent::DecodeObjectSetMediaVolumeAndRadius(ScriptClientEvent* sce, ZoneIndex& zoneId, ULONG& objId, MediaParams& mediaParams) {
	jsVerifyReturnVoid(sce && sce->GetEventType() == ScriptClientEvent::ObjectSetMediaVolumeAndRadius);
	long zoneIdLong;
	double mVolPct, mRadAud, mRadVid;
	*sce->InBuffer() >> zoneIdLong >> objId >> mVolPct >> mRadAud >> mRadVid;
	zoneId = ZoneIndex(zoneIdLong);
	mediaParams = MediaParams("", "", mVolPct, mRadAud, mRadVid);
}
