///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include "ChatType.h"
#include "SDKCommon.h"
#include <string>

namespace KEP {

enum class SlashCommand;

class TextEvent : public IEvent {
public:
	TextEvent(const char *textMsg, SlashCommand commandId, int fromNetworkAssignedId, eChatType chatType, const char *toDistribution = nullptr, const char *fromRecipient = nullptr, eChatType chatTypeOrig = eChatType::Talk, bool fromIMClient = false);
	void ExtractInfo(SlashCommand &commandId, int &fromNetworkAssignedId, std::string &text, eChatType &chatType, std::string *distro = nullptr, std::string *from = nullptr, eChatType *pChatTypeOrig = nullptr, bool *fromIMClient = nullptr);

	DEFINE_SECURE_CLASS_INFO(TextEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
