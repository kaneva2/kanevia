#pragma once

#include "Event\Base\IEvent.h"
#include <string>

namespace KEP {

class ObjectTicklerEvent : public IEvent {
public:
	ObjectTicklerEvent();

	enum class ChangeType {
		Invalid = 0,
		AddObject,
		UpdateObject,
		DeleteObject,
		Max // always leave this at end
	};

	enum class ObjectType {
		Invalid = 0,
		ItemPassList,
		Item,
		NPC,
		PlayerInventory,
		BankInventory,
		ZonePassList,
		Store,
		BankZone,
		Quest,
		PlayerPassList,
		MediaPlaylist, // DRF - ED-7522 - Web should tickle server on playlist changes
		Max // always leave this at end
	};

	inline bool ExtractInfo(OUT ObjectType &ot, OUT ChangeType &ct);
	const char *ExtractStringId(OUT std::string &id) {
		(*InBuffer()) >> id;
		return id.c_str();
	}
	ULONG ExtractNumericId() {
		ULONG id = 0;
		(*InBuffer()) >> id;
		return id;
	}

	DEFINE_SECURE_CLASS_INFO(ObjectTicklerEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline bool ObjectTicklerEvent::ExtractInfo(OUT ObjectType &ot, OUT ChangeType &ct) {
	jsVerifyReturn(ObjectId() > static_cast<OBJECT_ID>(ObjectType::Invalid) && ObjectId() < static_cast<OBJECT_ID>(ObjectType::Max) &&
					   Filter() > static_cast<ULONG>(ChangeType::Invalid) && Filter() < static_cast<ULONG>(ChangeType::Max),
		false);

	ot = static_cast<ObjectType>(ObjectId());
	ct = static_cast<ChangeType>(Filter());

	return true;
}

} // namespace KEP
