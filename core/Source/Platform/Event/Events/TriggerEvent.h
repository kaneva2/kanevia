///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#include <SDKCommon.h>
#include <log4cplus/Logger.h>

#include "Event\Base\IEvent.h"
#include <SDKCommon.h>

#include "common\include\CoreStatic\TriggerEnums.h"

namespace KEP {
class IDispatcher;

class TriggerEvent : public IEvent {
public:
	TriggerEvent(
		LONG networkAssignedId,
		LONG objPlacementId,
		LONG globalId,
		eTriggerEvent triggerEvent,
		float radius,
		int miscInt,
		const char* miscStr,
		double x, double y, double z,
		double xCenter, double yCenter, double zCenter,
		int triggerId // drf - added
		) :
			IEvent(ClassId(), globalId, objPlacementId) {
		(*OutBuffer()) << (int)triggerEvent << networkAssignedId << radius << miscInt << miscStr << x << y << z << xCenter << yCenter << zCenter << triggerId;
	}

	void ExtractInfo(
		LONG& networkAssignedId,
		LONG& objPlacementId,
		LONG& globalId,
		eTriggerEvent& triggerEvent,
		float& radius,
		int& miscInt,
		std::string& miscStr,
		double& x, double& y, double& z,
		double& xCenter, double& yCenter, double& zCenter,
		int& triggerId) {
		objPlacementId = ObjectId();
		globalId = Filter();
		int trigEvent;
		double tempRadius;
		(*InBuffer()) >> trigEvent >> networkAssignedId >> tempRadius >> miscInt >> miscStr >> x >> y >> z >> xCenter >> yCenter >> zCenter >> triggerId;
		triggerEvent = (eTriggerEvent)trigEvent;
		radius = (float)tempRadius;
	}

	DEFINE_CLASS_INFO(TriggerEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

class AddTriggerEvent : public IEvent {
public:
	AddTriggerEvent(
		LONG objPlacementId,
		eTriggerAction triggerAction,
		float radius,
		int miscInt,
		const char* miscStr,
		int triggerId) :
			IEvent(ClassId(), 0, objPlacementId) {
		(*OutBuffer()) << (int)triggerAction << radius << miscInt << miscStr << triggerId;
	}

	void ExtractInfo(
		LONG& objPlacementId,
		eTriggerAction& triggerAction,
		float& radius,
		int& miscInt,
		std::string& miscStr,
		int& triggerId) {
		int action;
		objPlacementId = ObjectId();
		(*InBuffer()) >> action >> radius >> miscInt >> miscStr >> triggerId;
		triggerAction = (eTriggerAction)action;
	}

	DEFINE_CLASS_INFO(AddTriggerEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

class RemoveTriggerEvent : public IEvent {
public:
	RemoveTriggerEvent(LONG objPlacementId, int triggerId) :
			IEvent(ClassId(), 0, objPlacementId) {
		(*OutBuffer()) << triggerId;
	}

	void ExtractInfo(LONG& objPlacementId, int& triggerId) {
		objPlacementId = ObjectId();
		(*InBuffer()) >> triggerId;
	}

	DEFINE_CLASS_INFO(RemoveTriggerEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
