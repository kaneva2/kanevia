///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "ZonePermissionEvent.h"
#include "..\EventSystem\Dispatcher.h"

namespace KEP {

DECLARE_CLASS_INFO(SetZonePermissionEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(SetZonePermissionEvent) {
}

void RegisterZonePermissionEvents(IDispatcher &disp) {
	RegisterEvent<SetZonePermissionEvent>(disp);
}

} // namespace KEP