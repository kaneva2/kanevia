///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "MovementObjectCfgEvent.h"

#include "include\Core\Math\Rotation.h"

namespace KEP {

DECLARE_CLASS_INFO(MovementObjectCfgEvent)

MovementObjectCfgEvent::MovementObjectCfgEvent(
	int isItem,
	int networkDefinedID,
	short dbCfg,
	short mount,
	int passGroups,
	unsigned short nameR, unsigned short nameG, unsigned short nameB,
	Vector3f scale, // drf - added
	Vector3f pos, // drf - added
	Vector3f dir, // drf - added
	Vector3f up, // drf - added
	GLID animGlid1, // drf - added
	GLID animGlid2, // drf -added
	const std::vector<ItemAnimation>& itemAnimations) :
		IEvent(ClassId()) {
	// Convert Dir/Up Vectors To Quaternion Rotation (consistent with MOVE_DATA::Encode())
	Vector3<short> rot;
	ConvertRotationZXY(dir, up, out(rot));

	(*OutBuffer())
		<< isItem
		<< networkDefinedID
		<< dbCfg
		<< mount
		<< passGroups
		<< nameR << nameG << nameB
		<< scale
		<< pos
		<< rot
		<< animGlid1
		<< animGlid2;

	(*OutBuffer()) << (size_t)itemAnimations.size();
	for (const auto& ia : itemAnimations)
		(*OutBuffer()) << ia.m_glidItem << ia.m_glidAnim;
}

void MovementObjectCfgEvent::ExtractInfo(
	int& isItem,
	int& netTraceId,
	short& dbCfg,
	short& mount,
	int& passGroups,
	unsigned short& nameR, unsigned short& nameG, unsigned short& nameB,
	Vector3f& scale, // drf - added
	Vector3f& pos, // drf - added
	Vector3f& dir, // drf - added
	Vector3f& up, // drf - added
	GLID& animGlid1, // drf - added
	GLID& animGlid2, // drf - added
	std::vector<ItemAnimation>& itemAnimations) {
	Vector3<short> rot;
	(*InBuffer()) >> isItem >> netTraceId >> dbCfg >> mount >> passGroups >> nameR >> nameG >> nameB >> scale >> pos >> rot >> animGlid1 >> animGlid2;

	// Convert Quaternion Rotation To Dir/Up Vectors (consistent with MOVE_DATA::Decode())
	ConvertRotationZXY(rot, out(dir), out(up));

	itemAnimations.clear();
	size_t animationsNum = 0;
	(*InBuffer()) >> animationsNum;
	if (animationsNum > 0) {
		itemAnimations.resize(animationsNum);
		for (size_t i = 0; i < animationsNum; i++)
			(*InBuffer()) >> itemAnimations[i].m_glidItem >> itemAnimations[i].m_glidAnim;
	}
}

EVENT_CONSTRUCTOR_PROTOTYPE(MovementObjectCfgEvent) {
	// do nothing here
}

} // namespace KEP