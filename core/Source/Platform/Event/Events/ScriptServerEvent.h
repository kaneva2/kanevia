///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class PlaylistAssets;

// Events Sent To Script Server
class ScriptServerEvent : public IEvent {
public:
	// NOTE: Keep in sync with ScriptServerEventTypes.lua !!!
	// NOTE: Also Update EventTypeStr() Below !!!
	enum EventType {
		None = -1,
		Timer = 0,
		Start = 1,
		Touch = 2,
		RemovePlacementObject = 3,
		PlayerArrived = 4,
		PlayerDeparted = 5,
		PlayerLocation = 6,
		ObjectMessage = 7,
		MenuEvent = 8,
		PlayerUseItem = 9,
		PlayerCheckInventory = 10,
		PlayerRemoveItem = 11,
		GameZonesInfo = 12,
		GameInstanceInfo = 13,
		CreateInstance = 14,
		PlayerJoinInstance = 15,
		LockInstance = 16,
		Trigger = 17,
		GetAllScripts = 18,
		UploadScript = 19,
		DeleteScript = 20,
		AttachScript = 21,
		DetachScript = 22,
		ScriptMakeWebcall = 23,
		PlayerEquipItem = 24,
		PlayerUnEquipItem = 25,
		ShutdownScript = 26,
		TransmitGameState = 27,
		PlayerRayIntersects = 28,
		MovePlacementObject = 29,
		ObjectGetNextPlaylistItem = 30,
		ObjectSetNextPlaylistItem = 31,
		ObjectEnumeratePlaylist = 32,
		Deprecated_33 = 33,
		AddPlacementObject = 34,
		Collide = 35,
		DownloadAsset = 36,
		DeleteAsset = 37,
		PlayerAddItem = 38,
		ObjectClick_Deprecated = 39,
		PlayerClick = 40,
		PresetMenuAction_Deprecated = 41,
		GetServerLog = 42,
		ProcessUGCUpload = 43,
		Deprecated_44 = 44,
		PlayerSetEquipped = 45,
		PlayerRemoveAllAccessories = 46,
		PlaceGameItem = 47,
		EndCollide = 48,
		PlayerSaveEquipped = 49,
		PlayerGetEquipped = 50,
		GracefulAPITimeout = 51,
		ItemInfo = 52, // response to ScriptClientEvent::GetItemInfo (kgp.gameGetItemInfo)
		ScriptMakeWebcallAsync = 53,
		ObjectLeftClick = 54, // replace ObjectClick
		ObjectRightClick = 55, // replace ObjectClick
		MenuButton = 56, // replace PresetMenuAction
		MenuClose = 57, // replace PresetMenuAction
		MenuKeyDown = 58, // replace PresetMenuAction
		MenuKeyUp = 59, // replace PresetMenuAction
		MenuListSelect = 60, // replace PresetMenuAction
		MenuShopBuy = 61, // replace PresetMenuAction
		UnloadScript = 62,
		EffectCompletion = 63,
		EventTypes
	};

	// NOTE: Also Update EventTypes Above !!!
	static std::string EventTypeStr(const EventType& eventType) {
		const std::string eventTypeStr[(size_t)EventType::EventTypes] = {
			"Timer",
			"Start",
			"Touch",
			"RemovePlacementObject",
			"PlayerArrived",
			"PlayerDeparted",
			"PlayerLocation",
			"ObjectMessage",
			"MenuEvent",
			"PlayerUseItem",
			"PlayerCheckInventory",
			"PlayerRemoveItem",
			"GameZonesInfo",
			"GameInstanceInfo",
			"CreateInstance",
			"PlayerJoinInstance",
			"LockInstance",
			"Trigger",
			"GetAllScripts",
			"UploadScript",
			"DeleteScript",
			"AttachScript",
			"DetachScript",
			"ScriptMakeWebcall",
			"PlayerEquipItem",
			"PlayerUnEquipItem",
			"ShutdownScript",
			"TransmitGameState",
			"PlayerRayIntersects",
			"MovePlacementObject",
			"ObjectGetNextPlayListItem",
			"ObjectSetNextPlayListItem",
			"ObjectEnumeratePlayList",
			"ObjectSetPlayList",
			"AddPlacementObject",
			"Collide",
			"DownloadAsset",
			"DeleteAsset",
			"PlayerAddItem",
			"ObjectClick_Deprecated",
			"PlayerClick",
			"PresetMenuAction_Deprecated",
			"GetServerLog",
			"ProcessUGCUpload",
			"Deprecated_2",
			"PlayerSetEquipped",
			"PlayerRemoveAllAccessories",
			"PlaceGameItem",
			"EndCollide",
			"PlayerSaveEquipped",
			"PlayerGetEquipped",
			"GracefulAPITimeout",
			"ItemInfo",
			"ScriptMakeWebcallAsync",
			"ObjectLeftClick",
			"ObjectRightClick",
			"MenuButton",
			"MenuClose",
			"MenuKeyDown",
			"MenuKeyUp",
			"MenuListSelect",
			"MenuShopBuy",
			"UnloadScript",
		};

		auto eventTypeNum = (size_t)eventType;
		return (eventTypeNum < (size_t)EventType::EventTypes) ? eventTypeStr[eventTypeNum] : "INVALID";
	}

	enum UploadAssetTypes {
		// Do not renumber these they are used in LUA
		UndefinedAssetType = 0,
		ServerScript = 1,
		MenuXml = 2,
		MenuDDS = 3,
		MenuTGA = 4,
		MenuScript = 5,
		SavedScript = 6,
		MenuBMP = 7,
		MenuPNG = 8,
		MenuJPG = 9,
		MenuJPEG = 10,
		ActionItem = 11, // Original item (shop or local, script only)
		ActionInstance = 12, // Orphaned instance (script only)
		ActionItemParams = 13, // Param-only upload - item
		ActionInstanceParams = 14, // Param-only upload - placed instance
	};

	static const DWORD FILE_TRANSFER_PACKET_DELAY = 0;
	static const int BYTES_BEFORE_ACK = 100000;

	ScriptServerEvent(EventType eventType, ULONG objPlacementId = 0, int zoneId = 0, int instanceId = 0, NETID clientNetId = 0);

	void SetEventType(EventType eventType) { SetFilter((ULONG)eventType); }
	EventType GetEventType() const { return (EventType)Filter(); }

	void ExtractInfo(EventType& eventType, ULONG& objPlacementId, int& zoneId, int& instanceId, NETID& clientNetId);

	DEFINE_SECURE_CLASS_INFO(ScriptServerEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)

	static const short LAST_CHUNK = -1; // for file transfer, indicator that this is last chunk of data
	static const short BUT_WAIT_THERES_MORE = -2;

	void SetPlaylistAssets(PlaylistAssets* assets) { pl_assets = assets; }
	PlaylistAssets* GetPlaylistAssets() { return pl_assets; }

	bool isDecoded() const { return m_decoded; }
	ULONG getObjPlacementId() const { return m_objPlacementId; }
	int getZoneId() const { return m_zoneId; }
	int getInstanceId() const { return m_instanceId; }

private:
	PlaylistAssets* pl_assets;

	bool m_decoded = false; // set to true once ExtractInfo is called and values cached
	ULONG m_objPlacementId = 0;
	int m_zoneId = 0;
	int m_instanceId = 0;
};

struct ScriptPlayerInfo;
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPlayerInfo& info);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPlayerInfo& info);

} // namespace KEP
