///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "UGCImportEvent.h"

namespace KEP {

DECLARE_CLASS_INFO(UGCImportEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UGCImportEvent) {
	// do nothing here
}

} // namespace KEP
