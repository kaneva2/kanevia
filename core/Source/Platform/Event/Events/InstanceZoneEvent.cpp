///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "InstanceZoneEvent.h"

#include "Event/Base/IEncodingFactory.h"
#include <KEPException.h>

using namespace KEP;

DECLARE_CLASS_INFO(InstanceZoneEvent)

InstanceZoneEvent::InstanceZoneEvent(bool creating, const char *worldName, LONG channel) :
		IEvent(ClassId()) {
	(*OutBuffer()) << creating << worldName << channel;
}

EVENT_CONSTRUCTOR_PROTOTYPE(InstanceZoneEvent) {
	// do nothing here
}

void InstanceZoneEvent::ExtractInfo(bool &creating, std::string &worldName, LONG &channel) {
	// unpack it

	jsVerifyDo(m_inBuffer != 0, return );

	(*m_inBuffer) >> creating >> worldName >> channel;

	m_inBuffer->Rewind();
}
