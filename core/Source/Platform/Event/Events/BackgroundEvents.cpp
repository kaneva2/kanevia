///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GotoPlaceBackgroundEvent.h"
#include "ChangeZoneBackgroundEvent.h"
#include "ZoneCustomizationBackgroundEvent.h"
#include "GetResultSetsEvent.h"
#include "CanSpawnToZoneBackgroundEvent.h"
#include "LogoffUserBackgroundEvent.h"
#include "BroadcastEventBackgroundEvent.h"
#include "PlayListBackgroundEvent.h"
#include "YouTubeSWFUrlBackgroundEvent.h"
#include "RefreshPlayerBackgroundEvent.h"
#include "SpaceImportBackgroundEvent.h"
#include "ChannelImportBackgroundEvent.h"
#include "ScriptAvailableItemsBackgroundEvent.h"
#include "LoadItemsBackgroundEvent.h"
#include "UpdateScriptZoneBackgroundEvent.h"

using namespace KEP;

DECLARE_CLASS_INFO(GotoPlaceBackgroundEvent)

DECLARE_CLASS_INFO(ChangeZoneBackgroundEvent)

DECLARE_CLASS_INFO(ZoneCustomizationBackgroundEvent)

DECLARE_CLASS_INFO(GetResultSetsEvent)

DECLARE_CLASS_INFO(CanSpawnToZoneBackgroundEvent)

DECLARE_CLASS_INFO(LogoffUserBackgroundEvent)

DECLARE_CLASS_INFO(BroadcastEventBackgroundEvent)

DECLARE_CLASS_INFO(PlaylistBackgroundEvent)

DECLARE_CLASS_INFO(YouTubeSWFUrlBackgroundEvent)

DECLARE_CLASS_INFO(RefreshPlayerBackgroundEvent)

DECLARE_CLASS_INFO(SpaceImportBackgroundEvent)

DECLARE_CLASS_INFO(ChannelImportBackgroundEvent)

DECLARE_CLASS_INFO(ScriptAvailableItemsBackgroundEvent)

DECLARE_CLASS_INFO(LoadItemsBackgroundEvent)

DECLARE_CLASS_INFO(UpdateScriptZoneBackgroundEvent)
