///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <SDKCommon.h>
#include <string>
#include <InstanceId.h>

namespace KEP {

class SpaceImportEvent : public IEvent {
public:
	SpaceImportEvent();

	void ExtractInfo();

	int m_newZoneIndex;
	int m_importZoneIndex;
	int m_importZoneInstanceId;
	bool m_maintainScripts;

	DEFINE_SECURE_CLASS_INFO(SpaceImportEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
