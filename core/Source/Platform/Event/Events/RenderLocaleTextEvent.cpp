///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include <SDKCommon.h>
#include "RenderLocaleTextEvent.h"
#include "Event/EventSystem/Dispatcher.h"
#include "Event/Base/IEncodingFactory.h"
#include "Common/include/KEPHelpers.h"
#include "LocaleStrings.h"

using namespace KEP;

DECLARE_CLASS_INFO(RenderLocaleTextEvent)

RenderLocaleTextEvent::RenderLocaleTextEvent(UINT id, eChatType chatType, const char* s0,
	const char* s1,
	const char* s2,
	const char* s3,
	const char* s4,
	const char* s5,
	const char* s6,
	const char* s7,
	const char* s8,
	const char* s9) :
		IEvent(ClassId()) {
	std::vector<const char*> strings;
	if (s0 != 0)
		strings.push_back(s0);
	if (s1 != 0)
		strings.push_back(s1);
	if (s2 != 0)
		strings.push_back(s2);
	if (s3 != 0)
		strings.push_back(s3);
	if (s4 != 0)
		strings.push_back(s4);
	if (s5 != 0)
		strings.push_back(s5);
	if (s6 != 0)
		strings.push_back(s6);
	if (s7 != 0)
		strings.push_back(s7);
	if (s8 != 0)
		strings.push_back(s8);
	if (s9 != 0)
		strings.push_back(s9);

	// pack up the remainder into the buffer
	// we need to encode who sent it, since the existing text messages processing needs it
	(*OutBuffer()) << (ULONG)id
				   << (int)chatType
				   << (ULONG)strings.size();
	for (std::vector<const char*>::iterator i = strings.begin(); i != strings.end(); i++)
		(*OutBuffer()) << *i;
}

void RenderLocaleTextEvent::SendLocaleTextTo(IDispatcher& dispatcher, NETID idFromLoc, eChatType chatType, UINT id, const char* s0,
	const char* s1,
	const char* s2,
	const char* s3,
	const char* s4,
	const char* s5,
	const char* s6,
	const char* s7,
	const char* s8,
	const char* s9) {
	RenderLocaleTextEvent* e = new RenderLocaleTextEvent(id, chatType, s0, s1, s2, s3, s4, s5, s6, s7, s8, s9);
	e->AddTo(idFromLoc);
	dispatcher.QueueEvent(e);
}
EVENT_CONSTRUCTOR_PROTOTYPE(RenderLocaleTextEvent) {
}

bool RenderLocaleTextEvent::ExtractInfo(std::string& text, eChatType& chatType) {
	bool ret = false;

	// decode the string
	// will throw!
	if (InBuffer()) {
		int temp;
		ULONG count;
		ULONG id;
		(*m_inBuffer) >> id >> temp >> count;

		chatType = (eChatType)temp;

		jsAssert(count < 11);
		if (count > 10)
			count = 10;

		std::string strings[10];
		memset(&strings, 0, sizeof(strings));
		for (size_t i = 0; i < count; i++) {
			(*m_inBuffer) >> strings[i];
	}	
		ret = loadLocaleString(text, id, strings[0].c_str(), strings[1].c_str(), strings[2].c_str(), strings[3].c_str(), strings[4].c_str(), strings[5].c_str(), strings[6].c_str(), strings[7].c_str(), strings[8].c_str(), strings[9].c_str());

		m_inBuffer->Rewind();
	}
	return ret;
}
