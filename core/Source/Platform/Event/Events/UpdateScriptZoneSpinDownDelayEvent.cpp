///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "UpdateScriptZoneSpinDownDelayEvent.h"
#include "InstanceId.h"

namespace KEP {

DECLARE_CLASS_INFO(UpdateScriptZoneSpinDownDelayEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UpdateScriptZoneSpinDownDelayEvent) {
	// do nothing here
}

UpdateScriptZoneSpinDownDelayEvent::UpdateScriptZoneSpinDownDelayEvent(const ZoneIndex& zoneIndex, int spinDownDelayMins, unsigned tiedServerId) :
		IEvent(ClassId()) {
	*OutBuffer() << zoneIndex.zoneIndex() << zoneIndex.GetInstanceId() << zoneIndex.GetType() << spinDownDelayMins << tiedServerId;
}

void UpdateScriptZoneSpinDownDelayEvent::ExtractInfo(ZoneIndex& zoneIndex, int& spinDownDelayMins, unsigned& tiedServerId) {
	unsigned zoneIndexPlain, zoneInstanceId, zoneType;
	*InBuffer() >> zoneIndexPlain >> zoneInstanceId >> zoneType >> spinDownDelayMins >> tiedServerId;
	zoneIndex = ZoneIndex(zoneIndexPlain, zoneInstanceId, zoneType);
}

} // namespace KEP
