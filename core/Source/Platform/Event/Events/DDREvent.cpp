///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <js.h>
#include "DDREvent.h"
#include "Core/Crypto/CryptDecrypt.h"
#include "Common/Include/config.h"

using namespace KEP;

DECLARE_CLASS_INFO(DDREvent)

DDREvent::DDREvent() :
		IEvent(ClassId()),
		m_gameId(0),
		m_levelId(0),
		type(0),
		m_highestScoreId(0) {
	m_Radius = 0.0;
	m_highestScore = 0;
	m_highestScoreName = "";

	m_skill_id = 0;
	m_skill_name = "";
	m_skill_type = 0;
	m_skill_gain_increment = 0.0;
	m_skill_basic_ability_skill = "";
	m_skill_basic_ability_exp_mastered = 0.0;
	m_skill_basic_ability_function = 0;
	m_skill_gains_when_failed = 0.0;
	m_skill_max_multiplier = 0.0;
	m_skill_use_internal_timer = "";
	m_skill_use_filter_eval_level = "";
	m_skill_use_filter_current_level = 0;
	m_skill_use_filter_current_experience = 0;
}

DDREvent::~DDREvent() {
	m_animation_flares.clear();
	m_animation_final_flares.clear();
	m_animation_misses.clear();
	m_animation_levelups.clear();
	m_animation_perfects.clear();
	m_animation_idles.clear();
}

EVENT_CONSTRUCTOR_PROTOTYPE(DDREvent), m_gameId(0), m_levelId(0), type(0) {
}

void DDREvent::StartLevel(ULONG gameId, ULONG levelId, bool newGame, int objectId) {
	SetFilter(DDR_START_LEVEL);
	(*OutBuffer()) << gameId << objectId << levelId << newGame;
	EncryptEvent();
}

void DDREvent::StartLevelData(int playerId,
	ULONG gameId,
	ULONG levelId,
	ULONG seed,
	ULONG num_seconds_to_play,
	ULONG num_moves,
	ULONG num_misses,
	ULONG level_bonus_trigger_num,
	ULONG animation_flare_count,
	ULONG animation_final_flare_count,
	ULONG animation_miss_count,
	ULONG animation_levelup_count,
	ULONG animation_perfect_count,
	ULONG animation_idle_count,
	ULONG num_sub_levels,
	ULONG perfect_score_ceiling,
	ULONG great_score_ceiling,
	ULONG good_score_ceiling,
	ULONG last_level_reduction_ms,
	std::vector<std::pair<ULONG, int>> &animation_flares,
	std::vector<std::pair<ULONG, int>> &animation_final_flares,
	std::vector<std::pair<ULONG, int>> &animation_misses,
	std::vector<std::pair<ULONG, int>> &animation_levelups,
	std::vector<std::pair<ULONG, int>> &animation_perfects,
	std::vector<std::pair<ULONG, int>> &animation_idles) {
	SetFilter(DDR_START_LEVEL_DATA);

	(*OutBuffer()) << playerId << gameId << levelId << seed << num_seconds_to_play
				   << num_moves << num_misses << level_bonus_trigger_num << animation_flare_count << animation_final_flare_count
				   << animation_miss_count << animation_levelup_count << animation_perfect_count
				   << animation_idle_count << num_sub_levels << perfect_score_ceiling << great_score_ceiling << good_score_ceiling
				   << last_level_reduction_ms;

	for (auto ii = animation_flares.cbegin(); ii != animation_flares.cend(); ii++) {
		(*OutBuffer()) << ii->first;
		(*OutBuffer()) << ii->second;
	}

	for (auto ii = animation_final_flares.cbegin(); ii != animation_final_flares.cend(); ii++) {
		(*OutBuffer()) << ii->first;
		(*OutBuffer()) << ii->second;
	}

	for (auto ii = animation_misses.cbegin(); ii != animation_misses.cend(); ii++) {
		(*OutBuffer()) << ii->first;
		(*OutBuffer()) << ii->second;
	}

	for (auto ii = animation_levelups.cbegin(); ii != animation_levelups.cend(); ii++) {
		(*OutBuffer()) << ii->first;
		(*OutBuffer()) << ii->second;
	}

	for (auto ii = animation_perfects.cbegin(); ii != animation_perfects.cend(); ii++) {
		(*OutBuffer()) << ii->first;
		(*OutBuffer()) << ii->second;
	}

	for (auto ii = animation_idles.cbegin(); ii != animation_idles.cend(); ii++) {
		(*OutBuffer()) << ii->first;
		(*OutBuffer()) << ii->second;
	}
}

void DDREvent::EncryptEvent() {
	// symmetric block cipher
	ULONG len = 0;
	PCBYTE buff = 0;
	m_outBuffer->GetBuffer(buff, len);

	EncryptBytes((char *)buff, len, DDR_KEY);
}

void DDREvent::RecordScore(ULONG gameId, int objectId, ULONG levelId, int gameInstanceId, ULONG numMilliSecondsExpired, bool levelComplete, bool gameOver, bool perfectBonus, bool perfectAchieved, bool useAlternate) {
	SetFilter(DDR_RECORD_SCORE);
	(*OutBuffer()) << gameId << objectId << levelId << gameInstanceId << numMilliSecondsExpired << levelComplete << gameOver << perfectBonus << perfectAchieved << useAlternate;
	EncryptEvent();
}

void DDREvent::PostDDRScore(ULONG playerId, ULONG gameId, ULONG levelId, int gameInstanceId, ULONG score, ULONG highScore, ULONG fameGainAmount, int newFameLevel, int percentToNextLevel, std::string rewardsAtNextLevel, std::string gainMessage, float totalFame) {
	SetFilter(DDR_POST_SCORE);
	(*OutBuffer()) << playerId << gameId << levelId << gameInstanceId << score << highScore << fameGainAmount << newFameLevel << percentToNextLevel << rewardsAtNextLevel << gainMessage << totalFame;
}

void DDREvent::CloseGameDialog(ULONG gameId) {
	SetFilter(DDR_CLOSE_DIALOG);
	(*OutBuffer()) << gameId;
}

void DDREvent::GameOverDialog(ULONG gameId, int famePointsAwarded, int percentToNext, std::string rewardsAtNext, std::string gainMessage) {
	SetFilter(DDR_GAME_OVER_DIALOG);
	(*OutBuffer()) << gameId << famePointsAwarded << percentToNext << rewardsAtNext << gainMessage;
}

void DDREvent::ReadyToPlay(ULONG gameId) {
	SetFilter(DDR_READY_TO_PLAY);
	(*OutBuffer()) << gameId;
}

void DDREvent::PlayerExit(ULONG gameId, int objectId, int leftFloor) {
	SetFilter(DDR_PLAYER_EXIT);
	(*OutBuffer()) << gameId << objectId << leftFloor;
}

void DDREvent::PostScores(int playerId, ULONG gameId, ULONG levelId, ULONG highestScore, const char *highestScoreName, DDRPlayers *players) {
	SetFilter(DDR_POST_PLAYERS_SCORES);

	int num_players = players->m_playersMap.size();

	(*OutBuffer()) << playerId << gameId << levelId << num_players << highestScore << highestScoreName;

	for (const auto &player : players->m_playersMap) {
		(*OutBuffer()) << player.first; // playerId
		DDRPlayerObj *l_player = player.second;
		(*OutBuffer()) << l_player->GetName();
		(*OutBuffer()) << l_player->GetScore();
		(*OutBuffer()) << l_player->GetStatus();
	}
}

void DDREvent::GameData(ULONG gameId) {
	// In response to DDRDBEvent::LoadGameData, ptr to actual data was given to DDRDB.
	// This response to the server indicates the data has been filled out.
	SetFilter(DDR_GAME_DATA);
	(*OutBuffer()) << gameId;
}

void DDREvent::PlayerData(NETID from, ULONG gameId, int playerId,
	int gameLevel, bool newGame, int dynObjectId,
	int zoneIndex, int objectId, ULONG highScore) {
	SetFilter(DDR_PLAYER_DATA);
	SetFrom(from);
	m_gameId = gameId;
	m_newGame = newGame;
	m_playerId = playerId;
	m_zoneIndexId = zoneIndex;
	m_objectId = objectId;
	m_highScore = highScore;
	m_dynObjectId = dynObjectId;
	m_levelId = gameLevel;

	m_highestScore = 0;
	m_highestScoreName = "";
	m_highestScoreId = 0;
}

void DDREvent::AccessDenied(ULONG gameId, int playerId, int zoneIndexId) {
	SetFilter(DDR_PLAYER_ACCESS_DENIED);
	(*OutBuffer()) << gameId << playerId, zoneIndexId;
}

void DDREvent::PlayerPaused(ULONG gameId, int objectId, int isPaused) {
	SetFilter(DDR_PLAYER_PAUSED);
	(*OutBuffer()) << gameId << objectId << isPaused;
}

void DDREvent::ExtractInfo() {
	type = Filter();

	ULONG len = 0;
	PCBYTE buff = 0;

	InBuffer();

	if (m_inBuffer) {
		switch (type) {
			case DDR_START_LEVEL:
				m_inBuffer->GetBuffer(buff, len);
				DecryptBytes((char *)buff, len, DDR_KEY);
				(*m_inBuffer) >> m_gameId >> m_objectId >> m_levelId >> m_newGame;
				break;
			case DDR_GAME_DATA:
				// In response to DDRDBEvent::LoadGameData, ptr to actual data was given to DDRDB.
				// This response to the server indicates the data has been filled out.
				(*m_inBuffer) >> m_gameId;
				break;
			case DDR_START_LEVEL_DATA:
				(*m_inBuffer) >> m_playerId >> m_gameId >> m_levelId >> m_seed >> m_num_seconds_to_play >> m_num_moves >> m_num_misses >> m_level_bonus_trigger_num >> m_animation_flare_count >> m_animation_final_flare_count >> m_animation_miss_count >> m_animation_levelup_count >> m_animation_perfect_count >> m_animation_idle_count >> m_num_sub_levels >> m_perfect_score_ceiling >> m_great_score_ceiling >> m_good_score_ceiling >> m_last_level_reduction_ms;

				for (ULONG ii = 0; ii < m_animation_flare_count; ii++) {
					ULONG animationId = 0;
					int animationIdOrder = 0;
					(*m_inBuffer) >> animationId;
					(*m_inBuffer) >> animationIdOrder;
					m_animation_flares.push_back(std::make_pair(animationId, animationIdOrder));
				}

				for (ULONG ii = 0; ii < m_animation_final_flare_count; ii++) {
					ULONG animationId = 0;
					int animationIdOrder = 0;
					(*m_inBuffer) >> animationId;
					(*m_inBuffer) >> animationIdOrder;
					m_animation_final_flares.push_back(std::make_pair(animationId, animationIdOrder));
				}

				for (ULONG ii = 0; ii < m_animation_miss_count; ii++) {
					ULONG animationId = 0;
					int animationIdOrder = 0;
					(*m_inBuffer) >> animationId;
					(*m_inBuffer) >> animationIdOrder;
					m_animation_misses.push_back(std::make_pair(animationId, animationIdOrder));
				}

				for (ULONG ii = 0; ii < m_animation_levelup_count; ii++) {
					ULONG animationId = 0;
					int animationIdOrder = 0;
					(*m_inBuffer) >> animationId;
					(*m_inBuffer) >> animationIdOrder;
					m_animation_levelups.push_back(std::make_pair(animationId, animationIdOrder));
				}

				for (ULONG ii = 0; ii < m_animation_perfect_count; ii++) {
					ULONG animationId = 0;
					int animationIdOrder = 0;
					(*m_inBuffer) >> animationId;
					(*m_inBuffer) >> animationIdOrder;
					m_animation_perfects.push_back(std::make_pair(animationId, animationIdOrder));
				}

				for (ULONG ii = 0; ii < m_animation_idle_count; ii++) {
					ULONG animationId = 0;
					int animationIdOrder = 0;
					(*m_inBuffer) >> animationId;
					(*m_inBuffer) >> animationIdOrder;
					m_animation_idles.push_back(std::make_pair(animationId, animationIdOrder));
				}

				break;
			case DDR_POST_SCORE:
				(*m_inBuffer) >> m_playerId >> m_gameId >> m_levelId >> m_gameInstanceId >> m_score >> m_highScore >> m_famePointsEarned >> m_newFameLevel >> m_percentToNextLevel >> m_rewardsAtNextLevel >> m_gainMessage >> m_totalFame;
				break;
			case DDR_RECORD_SCORE:
				m_inBuffer->GetBuffer(buff, len);
				DecryptBytes((char *)buff, len, DDR_KEY);
				(*m_inBuffer) >> m_gameId >> m_objectId >> m_levelId >> m_gameInstanceId >> m_numMilliSecondsExpired >> m_levelComplete >> m_gameOver >> m_perfectBonus >> m_perfectAchieved >> m_useAlternate;
				break;
			case DDR_START_GAME:
				(*m_inBuffer) >> m_gameId >> m_objectId >> m_networkAssignedId >> m_Radius >> m_miscInt >> m_miscStr >> m_triggerx >> m_triggery >> m_triggerz >> m_triggerCenterx >> m_triggerCentery >> m_triggerCenterz >> m_ctrlx >> m_ctrly >> m_ctrlWidth >> m_ctrlHeight;
				break;
			case DDR_END_GAME:
				break;
			case DDR_PLAYER_EXIT:
				(*m_inBuffer) >> m_gameId >> m_objectId >> m_leftFloor;
				break;
			case DDR_PLAYER_PAUSED:
				(*m_inBuffer) >> m_gameId >> m_objectId >> m_isPaused;
				break;
			case DDR_PLAYER_ACCESS_DENIED:
				(*m_inBuffer) >> m_gameId >> m_playerId >> m_zoneIndexId;
				break;
			case DDR_PLAYER_DATA:
				// since in memory on server, members set by caller, no extraction needed
				break;
			default:
				jsVerifyReturnVoid(false);
				break;
		}
	}
}
