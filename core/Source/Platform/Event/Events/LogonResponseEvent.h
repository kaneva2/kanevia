///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include <string>

namespace KEP {

#define UNKNOWN_FROM -1

class LogonResponseEvent : public IEvent {
public:
	LogonResponseEvent();

	void SetErrorCodes(const char* connectionErrorCode, HRESULT hResultCode, ULONG characterCount = 0, LOGON_RET replyCode = LR_OK, ULONG discDiagCode = 0);

	std::string m_connectionErrorCode;
	HRESULT m_hResultCode;
	ULONG m_characterCount;
	LOGON_RET m_replyCode;

	DEFINE_SECURE_CLASS_INFO(LogonResponseEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
