///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <vector>

namespace KEP {

class PassListEvent : public IEvent {
public:
	static const int ZONE_LIST = 1;
	static const int PLAYER_LIST = 2;

	PassListEvent(NETID toPlayer, const std::vector<int> *playerList, const std::vector<time_t> *expires, const std::vector<int> *zoneList) :
			IEvent(ClassId()) {
		SetFilter(ZONE_LIST);
		AddTo(toPlayer);
		encode(playerList, expires);
		encode(zoneList);
	}

	PassListEvent(NETID toPlayer, int networkAssignedId, const std::vector<int> *playerList, const std::vector<time_t> *expires) :
			IEvent(ClassId()) {
		SetFilter((ULONG)PLAYER_LIST);
		AddTo(toPlayer);
		SetObjectId((OBJECT_ID)networkAssignedId);
		encode(playerList, expires);
	}

	// if Filter() == ZONE_LIST, passList is player, passList2 is zone
	// if Fitler() == PLAYER_LIST, passList is player
	// networkAssignedId == 0 if zone, else id of player
	void extractInfo(int &networkAssignedId, std::vector<int> &passList, std::vector<int> *passList2 = 0) {
		if (Filter() == PLAYER_LIST)
			networkAssignedId = ObjectId();
		else
			networkAssignedId = 0;

		passList.clear();
		int count = 0;
		(*InBuffer()) >> count;
		if (count != 0) {
			int temp;
			for (int i = 0; i < count; i++) {
				(*InBuffer()) >> temp;
				passList.push_back(temp);
			}
		}

		if (Filter() == ZONE_LIST) {
			if (passList2)
				passList2->clear();

			(*InBuffer()) >> count;
			if (count != 0) {
				int temp;
				for (int i = 0; i < count; i++) {
					(*InBuffer()) >> temp;
					if (passList2)
						passList2->push_back(temp);
				}
			}
		}
	}

private:
	// now only send non-expired passlists
	void encode(const std::vector<int> *list, const std::vector<time_t> *expires = 0) {
		time_t now = time(0);
		if (list) {
			ULONG count = 0;
			if (expires) {
				jsAssert(list->size() == expires->size());
				if (list->size() == expires->size()) {
					for (std::vector<time_t>::size_type i = 0; i < expires->size(); i++) {
						if (expires->at(i) >= now)
							count++;
					}
				} else {
					expires = 0;
				}
			} else {
				count = (ULONG)list->size();
			}

			(*OutBuffer()) << count;
			for (std::vector<int>::size_type i = 0; i < list->size(); i++) {
				if (expires == 0 || expires->at(i) >= now)
					(*OutBuffer()) << list->at(i);
			}
		} else
			(*OutBuffer()) << 0; // count of 0
	}

	DEFINE_SECURE_CLASS_INFO(PassListEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
