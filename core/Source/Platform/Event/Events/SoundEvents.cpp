///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "SoundEvents.h"
#include "..\EventSystem\Dispatcher.h"

namespace KEP {

DECLARE_CLASS_INFO(AddSoundEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(AddSoundEvent) {
}

DECLARE_CLASS_INFO(RemoveSoundEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(RemoveSoundEvent) {
}

DECLARE_CLASS_INFO(MoveSoundEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(MoveSoundEvent) {
}

DECLARE_CLASS_INFO(UpdateSoundEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(UpdateSoundEvent) {
}

void RegisterSoundEvents(IDispatcher &disp) {
	RegisterEvent<AddSoundEvent>(disp);
	RegisterEvent<RemoveSoundEvent>(disp);
	RegisterEvent<MoveSoundEvent>(disp);
	RegisterEvent<UpdateSoundEvent>(disp);
}

} // namespace KEP
