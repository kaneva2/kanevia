///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "IGetSet.h"

namespace KEP {

class ISkillDB;

class UseSkillEvent : public IEvent {
public:
	UseSkillEvent(IGetSet *player, ISkillDB *skillDatabase, int skillType,
		int skillLevel, int abilityIndex,
		ULONG manditoryWaitBeforeSkillUse, ULONG skillUseTimeStamp);

	DEFINE_SECURE_CLASS_INFO(UseSkillEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
