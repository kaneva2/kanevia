///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ClanEvent.h"

#include <js.h>

namespace KEP {

DECLARE_CLASS_INFO(ClanEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(ClanEvent) {
}

} // namespace KEP