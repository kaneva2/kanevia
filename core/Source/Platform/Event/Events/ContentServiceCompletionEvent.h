///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

#include "Event/Base/IEvent.h"

#include "UgcConstants.h"

namespace KEP {

class ContentServiceCompletionEvent : public IEvent {
public:
	// filter values indicate the action (0 is reserved for catch-all)
	static const ULONG BANK_INVENTORY = 1;
	static const ULONG SCRIPT_ICON = 2;
	static const ULONG PLAYER_INVENTORY = 3; // = 0; // DRF - 0 is reserved for catch-all

	ContentServiceCompletionEvent(ULONG filter, LPVOID message, int size, const std::vector<GLID>& glids);
	ContentServiceCompletionEvent(const std::vector<GLID>&);

	void ExtractValues(LPVOID& message, int& size, std::vector<GLID>&);
	void ExtractValues(std::vector<GLID>&);

	DEFINE_SECURE_CLASS_INFO(ContentServiceCompletionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
