///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"
#include "Event/Base/EventRegistry.h"
#include "Event/EventMap.h"
#include <string>

namespace KEP {

class EventSyncEvent : public IEvent {
public:
	EventSyncEvent(const EventMap &eventMap, const ULONG characterCount);
	void ExtractRegistry(EventRegistry &registry, ULONG *characterCount);

	DEFINE_SECURE_CLASS_INFO(EventSyncEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
