///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "DisconnectedEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(DisconnectedEvent)

DisconnectedEvent::DisconnectedEvent(NETID player, ULONG reason, LONG minutesLoggedOn) :
		IEvent(ClassId()) {
	SetFrom(player);
	(*OutBuffer()) << (ULONG)player
				   << minutesLoggedOn
				   << reason;
}

void DisconnectedEvent::SetMinutes(LONG minutesLoggedOn) {
	NETID player;
	LONG temp;
	ULONG reason;

	ExtractInfo(player, reason, temp);

	ResetForOutput();
	SetFrom(player);
	(*OutBuffer()) << (ULONG)player
				   << minutesLoggedOn
				   << reason;
}
EVENT_CONSTRUCTOR_PROTOTYPE(DisconnectedEvent) {
}
void DisconnectedEvent::ExtractInfo(NETID &player, ULONG &reason, LONG &minutesLoggedOn) {
	// decode
	// will throw!
	InBuffer();
	if (m_inBuffer) {
		(*m_inBuffer) >> player >> minutesLoggedOn >> reason;

		m_inBuffer->Rewind();
	}
}
