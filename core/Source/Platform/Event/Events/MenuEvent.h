///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <KEPHelpers.h>
#include <log4cplus/Logger.h>

#include "Event\Base\IEvent.h"
#include <KEPHelpers.h>

namespace KEP {
class IDispatcher;

/***********************************************************
CLASS

	MenuControlSelectedEvent
	

DESCRIPTION

***********************************************************/
class MenuControlSelectedEvent : public IEvent {
public:
	MenuControlSelectedEvent(const char* controlName, EVENT_ID id);

	void ExtractInfo(string& controlName);

	DEFINE_GENERIC_CLASS_INFO(MenuControlSelectedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline MenuControlSelectedEvent::MenuControlSelectedEvent(const char* controlName, EVENT_ID id) :
		IEvent(id) {
	jsVerifyDo(controlName != 0, return );

	(*OutBuffer()) << controlName;
}

inline void MenuControlSelectedEvent::ExtractInfo(string& controlName) {
	(*InBuffer()) >> controlName;
}

/***********************************************************
CLASS

	MenuControlMovedEvent
	

DESCRIPTION

***********************************************************/
class MenuControlMovedEvent : public IEvent {
public:
	MenuControlMovedEvent(const char* controlName, int x, int y, EVENT_ID id);

	void ExtractInfo(string& controlName, int& x, int& y);

	DEFINE_GENERIC_CLASS_INFO(MenuControlMovedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline MenuControlMovedEvent::MenuControlMovedEvent(const char* controlName, int x, int y, EVENT_ID id) :
		IEvent(id) {
	jsVerifyDo(controlName != 0, return );

	(*OutBuffer()) << controlName << x << y;
}

inline void MenuControlMovedEvent::ExtractInfo(string& controlName, int& x, int& y) {
	double new_x, new_y;
	(*InBuffer()) >> controlName >> new_x >> new_y;
	x = (int)new_x;
	y = (int)new_y;
}

/***********************************************************
CLASS

	MenuControlResizedEvent
	

DESCRIPTION

***********************************************************/
class MenuControlResizedEvent : public IEvent {
public:
	MenuControlResizedEvent(const char* controlName, int width, int height, EVENT_ID id);

	void ExtractInfo(string& controlName, int& width, int& height);

	DEFINE_GENERIC_CLASS_INFO(MenuControlResizedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline MenuControlResizedEvent::MenuControlResizedEvent(const char* controlName, int width, int height, EVENT_ID id) :
		IEvent(id) {
	jsVerifyDo(controlName != 0, return );

	(*OutBuffer()) << controlName << width << height;
}

inline void MenuControlResizedEvent::ExtractInfo(string& controlName, int& width, int& height) {
	double new_width, new_height;
	(*InBuffer()) >> controlName >> new_width >> new_height;
	width = (int)new_width;
	height = (int)new_height;
}

///////////////////

/***********************************************************
CLASS

	MenuSelectedEvent
	

DESCRIPTION

***********************************************************/
class MenuSelectedEvent : public IEvent {
public:
	MenuSelectedEvent(EVENT_ID id) :
			IEvent(id) {}

	DEFINE_GENERIC_CLASS_INFO(MenuSelectedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

/***********************************************************
CLASS

	MenuMovedEvent
	

DESCRIPTION

***********************************************************/
class MenuMovedEvent : public IEvent {
public:
	MenuMovedEvent(int x, int y, EVENT_ID id);

	void ExtractInfo(int& x, int& y);

	DEFINE_GENERIC_CLASS_INFO(MenuMovedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline MenuMovedEvent::MenuMovedEvent(int x, int y, EVENT_ID id) :
		IEvent(id) {
	(*OutBuffer()) << x << y;
}

inline void MenuMovedEvent::ExtractInfo(int& x, int& y) {
	double new_x, new_y;
	(*InBuffer()) >> new_x >> new_y;
	x = (int)new_x;
	y = (int)new_y;
}

/***********************************************************
CLASS

	MenuResizedEvent
	

DESCRIPTION

***********************************************************/
class MenuResizedEvent : public IEvent {
public:
	MenuResizedEvent(int width, int height, EVENT_ID id);

	void ExtractInfo(int& width, int& height);

	DEFINE_GENERIC_CLASS_INFO(MenuResizedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline MenuResizedEvent::MenuResizedEvent(int width, int height, EVENT_ID id) :
		IEvent(id) {
	(*OutBuffer()) << width << height;
}

inline void MenuResizedEvent::ExtractInfo(int& width, int& height) {
	double new_width, new_height;
	(*InBuffer()) >> new_width >> new_height;
	width = (int)new_width;
	height = (int)new_height;
}

} // namespace KEP

