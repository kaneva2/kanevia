///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class Missing3DAppCharacterEvent : public IEvent {
public:
	Missing3DAppCharacterEvent(LONG parentGameId, LONG appGameId);

	DEFINE_SECURE_CLASS_INFO(Missing3DAppCharacterEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP