///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "SpawnAIEvents.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(StartAISpawnEvent)

StartAISpawnEvent::StartAISpawnEvent(const char *spawnGenName, LONG channel, LONG maxSpawn, LONG percent, LONG aiCfgOverride) :
		IEvent(ClassId()) {
	jsVerifyDo(spawnGenName != 0, return );

	m_header.SetFilter(channel);

	(*OutBuffer()) << spawnGenName
				   << maxSpawn
				   << percent
				   << aiCfgOverride;
}

EVENT_CONSTRUCTOR_PROTOTYPE(StartAISpawnEvent) {}

void StartAISpawnEvent::ExtractInfo(std::string &spawnGenName, LONG &channel, LONG &maxSpawn, LONG &percent, LONG &aiCfgOverride) {
	channel = Filter();

	// decode the string
	// will throw!
	InBuffer();
	if (m_inBuffer) {
		(*m_inBuffer) >> spawnGenName >> maxSpawn >> percent >> aiCfgOverride;

		m_inBuffer->Rewind();
	}
}

DECLARE_CLASS_INFO(StopAISpawnEvent)

StopAISpawnEvent::StopAISpawnEvent(const char *spawnGenName, LONG channel) :
		IEvent(ClassId()) {
	jsVerifyDo(spawnGenName != 0, return );

	m_header.SetFilter(channel);

	(*OutBuffer()) << spawnGenName;
}

EVENT_CONSTRUCTOR_PROTOTYPE(StopAISpawnEvent) {}

void StopAISpawnEvent::ExtractInfo(std::string &spawnGenName, LONG &channel) {
	channel = Filter();

	// decode the string
	// will throw!
	InBuffer();
	if (m_inBuffer) {
		(*m_inBuffer) >> spawnGenName;

		m_inBuffer->Rewind();
	}
}
