///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class IDispatcher;

class ZoneCustomizationSummaryEvent : public IEvent {
public:
	ZoneCustomizationSummaryEvent(int rc, size_t dynamicObjectCount, size_t worldObjectSettingsCount, int frameworkEnabledFlag, unsigned ddsTextureSize, unsigned meshSize, unsigned meshInstSize) :
			IEvent(ClassId()) {
		SetFilter((unsigned long)rc);
		(*OutBuffer()) << dynamicObjectCount << worldObjectSettingsCount << frameworkEnabledFlag << ddsTextureSize << meshSize << meshInstSize;
	}

	void ExtractInfo(int& rc, size_t& dynamicObjectCount, size_t& worldObjectSettingsCount, int& frameworkEnabledFlag, unsigned& ddsTextureSize, unsigned& meshSize, unsigned& meshInstSize) {
		rc = (int)Filter();
		(*InBuffer()) >> dynamicObjectCount >> worldObjectSettingsCount >> frameworkEnabledFlag >> ddsTextureSize >> meshSize >> meshInstSize;
	}

	DEFINE_CLASS_INFO(ZoneCustomizationSummaryEvent, PackVersion(1, 0, 0, 1), EP_NORMAL)
};

} // namespace KEP
