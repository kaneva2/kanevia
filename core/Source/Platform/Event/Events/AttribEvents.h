///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/include/KEPCommon.h"

// Attribute Event Types
// DRF - TODO - Bitmask This Same As Messages
enum class eAttribEventType : short {
	None = 0,

	//////////////////////////////////////////////////////////////////////////////
	// Attribute Events To Server or Client
	//////////////////////////////////////////////////////////////////////////////

	SystemService = 255,

	//////////////////////////////////////////////////////////////////////////////
	// Attribute Events To Client
	//////////////////////////////////////////////////////////////////////////////

	PlayerHealth = 3,
	PlayerCreatedHealth = 8,
	DeleteEntity = 10,
	DeleteChar = 14,
	ChangeDefCfg = 15,
	OpenStoreMenu = 18,
	PlayerCredits = 19,
	OpenLootMenu = 20,
	PlayerInfo = 21,
	BankCredits = 22,
	OpenMenu = 24,
	CloseMenu = 25,
	InvalidGroupJoin = 26,
	PlayerInventory = 51,
	VendorInventory = 52,
	Generic = 142,
	ArmItem = 143,

	//////////////////////////////////////////////////////////////////////////////
	// Attribute Events To Server
	//////////////////////////////////////////////////////////////////////////////

	GetMovementObjectCfg = 1, // normal players
	GetStaticMovementObjectCfg = 4, // city vendors, paper dolls, etc.
	SelectCharacter = 9,
	DeletePlayer = 10,
	GetCharacterList = 14,
	SetInventoryItem = 15,
	SetDeformableItem = 18,
	BuyItem = 21,
	SellItem = 22,
	BankDepositItem = 24,
	BankWithdrawItem = 25,
	RequestPortal = 26, // drf - deprecated - DRF_OLD_PORTAL
	UseItem = 27,
	CancelTransaction = 29,
	AcceptTradeTerms = 30,
	DestroyItemByGlid = 31,
	QuestHandling = 32,
	UseSkill = 33,
	BuyHouseItem = 39,
	AddHouseItem = 40,
	RemoveHouseItem = 41,
	RemoveHouseCash = 42,
	ReorgBankItems = 43,
	ReorgInventoryItems = 44,
	GetInventoryPlayerAttachable = 47,
	TryOnItem = 48,
	DestroyBankItemByGlid = 49,
	GetInventoryPlayer = 50,
	GetInventoryBank = 51,
	GetInventoryHouse = 52,
	GetInventoryAll = 59
};

// eAttribEventType::ArmItem Actions
enum class eArmItemAction : int {
	Disarm = 0,
	Arm = 1
};

// eAttribEventType::ArmItem Results
enum class eArmItemResult : int {
	Failure = 0,
	Success = 1
};

static int ae_AllMenus = -9;

#define REQUEST_FOR_INVENTORY_ID 51 // attribEvent filter value
