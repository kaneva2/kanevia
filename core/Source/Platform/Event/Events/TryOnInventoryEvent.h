///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Event\Base\IEvent.h>

#include <common\include\CoreStatic\InstanceId.h>
#include <common\include\UseTypes.h>

namespace KEP {

class TryOnInventoryEvent : public IEvent {
public:
	// Filters
	static const LONG INVOKE_ALTERNATE_INVENTORY_MENU = 1;
	static const LONG INVOKE_EXPIRE_INVENTORY_MENU = 2;

	TryOnInventoryEvent();

	// init fns
	void InvokeAlternateInventoryMenu(int glid, const USE_TYPE& useType);
	void InvokeExpiredInventoryMenu(int glid, const USE_TYPE& useType, int m_placement_id);

	void ExtractInfo();

	LONG type;

	int m_glid;

	USE_TYPE m_useType;

	int m_placement_id;

	DEFINE_SECURE_CLASS_INFO(TryOnInventoryEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
