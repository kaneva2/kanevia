///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"

#include "Common\include\CoreStatic\CBoneAnimationNaming.h"

namespace KEP {

// Event from client initiating p2p request
class P2pAnimRequestEvent : public IEvent {
public:
	P2pAnimRequestEvent(int netTraceId, int animIndex) :
			IEvent(ClassId()) {
		SetFilter((ULONG)netTraceId);
		SetObjectId((OBJECT_ID)animIndex);
	}

	void ExtractInfo(int &netTraceId, int &animIndex) {
		netTraceId = (int)Filter();
		animIndex = (int)ObjectId();
	}

	DEFINE_SECURE_CLASS_INFO(P2pAnimRequestEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

class P2pAnimPermissionEvent : public IEvent {
public:
	P2pAnimPermissionEvent(int netTraceId, std::string animName) :
			IEvent(ClassId()) {
		SetFilter((ULONG)netTraceId);
		(*OutBuffer()) << animName;
	}

	void ExtractInfo(int &netTraceId, std::string &animName) {
		netTraceId = (int)Filter();
		(*InBuffer()) >> animName;
	}

	DEFINE_SECURE_CLASS_INFO(P2pAnimPermissionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

class P2pAnimPermissionReplyEvent : public IEvent {
public:
	P2pAnimPermissionReplyEvent(int netTraceId, int answer) :
			IEvent(ClassId()) {
		SetFilter((ULONG)netTraceId);
		SetObjectId((OBJECT_ID)answer);
	}

	void ExtractInfo(int &netTraceId, int &answer) {
		netTraceId = (int)Filter();
		answer = (int)ObjectId();
	}

	DEFINE_SECURE_CLASS_INFO(P2pAnimPermissionReplyEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

enum class eP2pAnimState : int {
	Inactive = 0,
	Begin = 1, // server -> both clients
	Start = 2, // both clients - move players, start animation, schedule End
	End = 3, // both clients - stop animation, return players, Terminate -> server
	Terminate = 4 // both clients -> server -> both clients - transition to Inactive
};

struct P2pAnimData {
	std::string animName; // not sent in events
	eP2pAnimState animState = eP2pAnimState::Inactive;
	int netTraceId_init;
	int netTraceId_target;
	eAnimType animType_init;
	eAnimType animType_target;
	TimeMs animStartTimeMs;
	TimeMs animTimeMs;
	bool animLooping;
};

class P2pAnimEvent : public IEvent {
public:
	P2pAnimEvent(const P2pAnimData &p2pAnimData) :
			IEvent(ClassId()) {
		(*OutBuffer())
			<< (int)p2pAnimData.animState
			<< p2pAnimData.netTraceId_init
			<< p2pAnimData.netTraceId_target
			<< (int)p2pAnimData.animType_init
			<< (int)p2pAnimData.animType_target
			<< p2pAnimData.animStartTimeMs
			<< p2pAnimData.animTimeMs
			<< p2pAnimData.animLooping;
	}

	void ExtractInfo(P2pAnimData &p2pAnimData) {
		int animState, animType_init, animType_target;
		(*InBuffer()) >> animState >> p2pAnimData.netTraceId_init >> p2pAnimData.netTraceId_target >> animType_init >> animType_target >> p2pAnimData.animStartTimeMs >> p2pAnimData.animTimeMs >> p2pAnimData.animLooping;
		p2pAnimData.animState = (eP2pAnimState)animState;
		p2pAnimData.animType_init = (eAnimType)animType_init;
		p2pAnimData.animType_target = (eAnimType)animType_target;
	}

	DEFINE_SECURE_CLASS_INFO(P2pAnimEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
