#pragma once

#include "IBackgroundDBEvent.h"
#include "GetResultSetsEvent.h"
#include "MySQLGetResultSetsEvent.h"

namespace KEP {

class CInventoryObjList;

/// class to pass data back and forth between
/// foreground and background threads
/// basically a glorified struct
class RefreshPlayerBackgroundEvent : public MySQLGetResultSetsEvent {
public:
	/// \brief constructor to refresh inventory/and/or cash on a player
	RefreshPlayerBackgroundEvent(NETID from, const char* playerName, int playerId, int kanevaId, int _uniInt, int _invCommitCount, ULONG _updateFlags, int prevPlayerCash = 0, bool gm = false) :
			MySQLGetResultSetsEvent(ClassId(), 0, 0, EP_NORMAL), updateFlags(_updateFlags), prevAmount(prevPlayerCash), newAmount(0), invCommitCount(_invCommitCount), isGm(gm), uniInt(_uniInt), inventory(0), firstTime(1), m_playerName(playerName) {
		kanevaUserId = kanevaId;
		SetFrom(from);
		SetObjectId(playerId);
	}

	/// \brief called if event has to be run again since inv changed in background
	void PrepareToReQueue(int _invCommitCount, int prevPlayerCash);

	~RefreshPlayerBackgroundEvent();

	// inbound only
	int prevAmount; //< for getting cash used to compare to see if we can update with new Amt
	ULONG updateFlags; //< see static members in IGameStateConstants
	int invCommitCount;
	int firstTime; //< 1 if first time, and only get inv if changed, otherwise will get all of inv regardless of if any changes
	bool isGm; //< is the player a gm (level of inventory, etc. to get)

	// passthru
	int uniInt; //< number echoed back to client when getting inv

	// outbound only
	int newAmount; //< new value of player's cash
	CInventoryObjList* inventory; //< updated inventory
	std::string m_playerName;

	DEFINE_DB_CLASS_INFO(RefreshPlayerBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP