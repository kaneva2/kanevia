///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

enum class ClanAction {
	CREATE,
	DISBAND
};

class ClanEvent : public IEvent {
public:
	ClanEvent(ClanAction clanAction, const std::string& clanName) :
			IEvent(ClassId()) {
		SetClanAction(clanAction);
		(*OutBuffer()) << clanName;
	}

	void SetClanAction(ClanAction clanAction) { SetFilter((ULONG)clanAction); }
	ClanAction GetClanAction() const { return (ClanAction)Filter(); }

	DEFINE_SECURE_CLASS_INFO(ClanEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP