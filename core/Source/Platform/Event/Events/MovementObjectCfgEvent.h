///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Event\Base\IEvent.h>

#include <common\include\CoreStatic\ItemAnimation.h>
#include <common\include\PassGroups.h>

namespace KEP {

class MovementObjectCfgEvent : public IEvent {
public:
	MovementObjectCfgEvent(
		int isItem,
		int networkDefinedID,
		short dbCfg,
		short mount,
		int passGroups,
		unsigned short nameR, unsigned short nameG, unsigned short nameB,
		Vector3f scale, // drf - added
		Vector3f pos, // drf - added
		Vector3f dir, // drf - added
		Vector3f up, // drf - added
		GLID animGlid1, // drf - added
		GLID animGlid2, // drf - added
		const std::vector<ItemAnimation>& itemAnimations);

	void ExtractInfo(
		int& isItem,
		int& networkDefinedID,
		short& dbCfg,
		short& mount,
		int& passGroups,
		unsigned short& nameR, unsigned short& nameG, unsigned short& nameB,
		Vector3f& scale, // drf - added
		Vector3f& pos, // drf - added
		Vector3f& dir, // drf - added
		Vector3f& up, // drf - added
		GLID& animGlid1, // drf - added
		GLID& animGlid2, // drf - added
		std::vector<ItemAnimation>& itemAnimations);

	DEFINE_SECURE_CLASS_INFO(MovementObjectCfgEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP