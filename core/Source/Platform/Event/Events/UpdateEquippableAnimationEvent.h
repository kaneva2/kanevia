///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Event\Base\IEvent.h>

#include <common\include\CoreStatic\ItemAnimation.h>

namespace KEP {

class UpdateEquippableAnimationEvent : public IEvent {
public:
	UpdateEquippableAnimationEvent(NETID networkDefinedID, const std::vector<ItemAnimation>& animations);
	void ExtractInfo(NETID& networkDefinedID, std::vector<ItemAnimation>& animations);

	DEFINE_SECURE_CLASS_INFO(UpdateEquippableAnimationEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
