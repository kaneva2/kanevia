///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "TitleEvent.h"

#include <js.h>

namespace KEP {

DECLARE_CLASS_INFO(TitleEvent)

EVENT_CONSTRUCTOR_PROTOTYPE(TitleEvent) {
}

TitleEvent::TitleEvent(std::string name) :
		IEvent(ClassId()) {
	(*OutBuffer()) << name.c_str();
}

} // namespace KEP