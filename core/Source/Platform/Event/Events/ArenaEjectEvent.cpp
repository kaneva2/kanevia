///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "ArenaEjectEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(ArenaEjectEvent)

ArenaEjectEvent::ArenaEjectEvent(LONG channelId, NETID playerNetId, int networkAssignedId) :
		IEvent(ClassId()) {
	SetFilter(channelId);

	(*OutBuffer()) << (ULONG)playerNetId
				   << networkAssignedId;
}

EVENT_CONSTRUCTOR_PROTOTYPE(ArenaEjectEvent) {
}
void ArenaEjectEvent::ExtractInfo(LONG &channelId, NETID &playerNetId, int &networkAssignedId) {
	channelId = Filter();

	// decode
	// will throw!
	InBuffer();
	if (m_inBuffer) {
		(*m_inBuffer) >> playerNetId >> networkAssignedId;

		m_inBuffer->Rewind();
	}
}
