#pragma once

// rcs for going to a zone or player
const int PLAYER_FOUND = 0; // ok
const int ZONE_FOUND = 0; // ok
const int PLAYER_NOT_LOGGED_ON = 1; // the dest player isn't logged on anywhere
const int PLAYER_NOT_FOUND = 2; // the dest player wasn't found
const int ZONE_NOT_FOUND = 2; // the dest zone wasn't found
const int PLAYER_NOT_ALLOWED = 3; // you can't go there
const int PLAYER_BLOCKED = 4; // can't go there because blocked
const int ZONE_FULL = 5; // can't go there because zone is full
const int SERVER_FULL = 6; // can't go there because server is full
const int MISSING_PASS = 7; // player doesn't have required passes
const int BAD_URL = 8; // URL passed in doesn't parse correctly
const int WRONG_GAME = 9; // URL refers to a different game
const int ALREADY_LOGGED_ON = 10; // user logged into another game or server
