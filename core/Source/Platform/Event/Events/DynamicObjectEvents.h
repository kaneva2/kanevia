///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include "InventoryType.h"
#include "common\include\MediaParams.h"
#include "KEPConstants.h"
#include <string>

namespace KEP {

class IDispatcher;
class ZoneIndex;
enum PlacementSource;

class AddDynamicObjectEvent : public IEvent {
public:
	AddDynamicObjectEvent(
		PlacementSource source,
		int ownerPlayerId,
		int globalId,
		float x, float y, float z,
		float dx, float dy, float dz,
		float sx, float sy, float sz,
		int placementId,
		GLID animGlidSpecial, // <0 = reverse animate
		int assetId,
		const char *texture,
		int friendId,
		const char *swf,
		const char *swfParams,
		bool attachable = false,
		bool interactive = false,
		bool playFlash = false,
		bool collision = false,
		float min_x = 0.0f, float min_y = 0.0f, float min_z = 0.0f,
		float max_x = 0.0f, float max_y = 0.0f, float max_z = 0.0f,
		bool derivable = false,
		int inventory_sub_type = IT_NORMAL,
		int gameItemId = INVALID_GAMEITEM) :
			IEvent(ClassId()) {
		SetObjectId((OBJECT_ID)source);
		SetFilter(ownerPlayerId);
		(*OutBuffer()) << x << y << z << dx << dy << dz << sx << sy << sz << placementId << animGlidSpecial << assetId << texture << friendId << swf << swfParams << globalId << attachable << interactive << playFlash << collision << min_x << min_y << min_z << max_x << max_y << max_z << derivable << inventory_sub_type << gameItemId;
	}

	void ExtractInfo(PlacementSource &source,
		int &ownerplayerId,
		int &globalId,
		float &x, float &y, float &z,
		float &dx, float &dy, float &dz,
		float &sx, float &sy, float &sz,
		int &placementId,
		GLID &animGlidSpecial, // < 0 = reverse animate
		int &assetId,
		std::string &texture,
		int &friendId,
		std::string &swf,
		std::string &swfParams,
		bool &attachable,
		bool &interactive,
		bool &playFlash,
		bool &collision,
		float &min_x, float &min_y, float &min_z,
		float &max_x, float &max_y, float &max_z,
		bool &derivable,
		int &inventory_sub_type,
		int &gameItemId);

	void ExtractBasicInfo(int &placementId) {
		double _x, _y, _z, _dx, _dy, _dz, _sx, _sy, _sz;
		(*InBuffer()) >> _x >> _y >> _z >> _dx >> _dy >> _dz >> _sx >> _sy >> _sz >> placementId;
	}

	DEFINE_SECURE_CLASS_INFO(AddDynamicObjectEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class RotateDynamicObjectEvent : public IEvent {
public:
	RotateDynamicObjectEvent(int placementId, double dx, double dy, double dz, double sx, double sy, double sz, DWORD timeMs = 0) :
			IEvent(ClassId()) {
		SetObjectId((OBJECT_ID)placementId);
		(*OutBuffer()) << dx << dy << dz << sx << sy << sz << timeMs;
	}

	void ExtractInfo(int &placementId, double &dx, double &dy, double &dz, double &sx, double &sy, double &sz, DWORD &timeMs) {
		placementId = (int)ObjectId();
		(*InBuffer()) >> dx >> dy >> dz >> sx >> sy >> sz >> timeMs;
	}

	DEFINE_SECURE_CLASS_INFO(RotateDynamicObjectEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class MoveDynamicObjectEvent : public IEvent {
public:
	MoveDynamicObjectEvent(int placementId, double x, double y, double z, DWORD timeMs = 0, DWORD accDur = 0, DWORD constVelDur = 0, double accDistFrac = 0, double constVelDistFrac = 0) :
			IEvent(ClassId()) {
		SetObjectId((OBJECT_ID)placementId);
		(*OutBuffer()) << x << y << z << timeMs << accDur << constVelDur << accDistFrac << constVelDistFrac;
	}

	void ExtractInfo(int &placementId, double &x, double &y, double &z, DWORD &timeMs, DWORD &accDur, DWORD &constVelDur, double &accDistFrac, double &constVelDistFrac) {
		placementId = (int)ObjectId();
		(*InBuffer()) >> x >> y >> z >> timeMs >> accDur >> constVelDur >> accDistFrac >> constVelDistFrac;
	}

	DEFINE_SECURE_CLASS_INFO(MoveDynamicObjectEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class MoveAndRotateDynamicObjectEvent : public IEvent {
public:
	MoveAndRotateDynamicObjectEvent(int placementId, double x, double y, double z, double dx, double dy, double dz, double sx, double sy, double sz, DWORD timeMs = 0) :
			IEvent(ClassId()) {
		SetObjectId((OBJECT_ID)placementId);
		(*OutBuffer()) << x << y << z << timeMs << dx << dy << dz << sx << sy << sz;
	}

	void ExtractInfo(int &placementId, double &x, double &y, double &z, double &dx, double &dy, double &dz, double &sx, double &sy, double &sz, DWORD &timeMs) {
		placementId = (int)ObjectId();
		(*InBuffer()) >> x >> y >> z >> timeMs >> dx >> dy >> dz >> sx >> sy >> sz;
	}

	DEFINE_SECURE_CLASS_INFO(MoveAndRotateDynamicObjectEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class ScaleDynamicObjectEvent : public IEvent {
public:
	ScaleDynamicObjectEvent(int placementId, double x, double y, double z) :
			IEvent(ClassId()) {
		SetObjectId((OBJECT_ID)placementId);
		(*OutBuffer()) << x << y << z;
	}

	void ExtractInfo(int &placementId, double &x, double &y, double &z) {
		placementId = (int)ObjectId();
		(*InBuffer()) >> x >> y >> z;
	}

	DEFINE_SECURE_CLASS_INFO(ScaleDynamicObjectEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class RemoveDynamicObjectEvent : public IEvent {
public:
	const static int FILTER_SCRIPTSERVER = 1; // Indicate this event is triggered by a script server call

	RemoveDynamicObjectEvent(int placementId) :
			IEvent(ClassId()) {
		SetObjectId((OBJECT_ID)placementId);
	}

	void ExtractInfo(int &placementId) {
		placementId = (int)ObjectId();
	}

	DEFINE_SECURE_CLASS_INFO(RemoveDynamicObjectEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class ControlDynamicObjectEvent : public IEvent {
public:
	ControlDynamicObjectEvent(int placementId, ULONG ctlId, const std::vector<int> &ctlArgs);
	void ExtractInfo(int &placementId, ULONG &ctlId, std::vector<int> &ctlArgs);

	DEFINE_SECURE_CLASS_INFO(ControlDynamicObjectEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class UpdateDynamicObjectTextureEvent : public IEvent {
public:
	UpdateDynamicObjectTextureEvent(int placementId, int textureAssetId, const char *texture, int playerId);
	void ExtractInfo(int &placementId, int &textureAssetId, std::string &texture, int &playerId);

	DEFINE_SECURE_CLASS_INFO(UpdateDynamicObjectTextureEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class SetDynamicObjectTexturePanningEvent : public IEvent {
public:
	SetDynamicObjectTexturePanningEvent(int placementId, int meshId, int uvSetId, float uIncr, float vIncr);
	void ExtractInfo(int &placementId, int &meshId, int &uvSetId, float &uIncr, float &vIncr);

	DEFINE_SECURE_CLASS_INFO(SetDynamicObjectTexturePanningEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

class UpdatePictureFrameFriendEvent : public IEvent {
public:
	UpdatePictureFrameFriendEvent(int placementId, int friendId, const char *textureUrl);
	void ExtractInfo(int &placementId, int &friendId, std::string &textureUrl);

	DEFINE_SECURE_CLASS_INFO(UpdatePictureFrameFriendEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

// DRF - Added
class UpdateDynamicObjectMediaEvent : public IEvent {
public:
	UpdateDynamicObjectMediaEvent(int placementId, const MediaParams &mediaParams, LONG zoneIndex = 0, int networkAssignedId = 0);
	void ExtractInfo(int &placementId, MediaParams &mediaParams, int *networkAssignedId = 0);

	DEFINE_SECURE_CLASS_INFO(UpdateDynamicObjectMediaEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

// DRF - Added
class UpdateDynamicObjectMediaVolumeAndRadiusEvent : public IEvent {
public:
	UpdateDynamicObjectMediaVolumeAndRadiusEvent(int placementId, const MediaParams &mediaParams, LONG zoneIndex = 0, int networkAssignedId = 0);
	void ExtractInfo(int &placementId, MediaParams &mediaParams, int *zoneIndex = NULL, int *networkAssignedId = NULL);

	DEFINE_SECURE_CLASS_INFO(UpdateDynamicObjectMediaVolumeAndRadiusEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

// Server-side media-change notifications for PlaylistMgr or similar modules
class DynamicObjectMediaChangedEvent : public IEvent {
public:
	DynamicObjectMediaChangedEvent(NETID from, const ZoneIndex &zoneIndex, const std::vector<int> &placementIds, const MediaParams &mediaParams);
	void ExtractInfo(NETID &from, ZoneIndex &zoneIndex, std::vector<int> &placementIds, MediaParams &mediaParams);

	DEFINE_CLASS_INFO(DynamicObjectMediaChangedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

class PlaceDynamicObjectAtPositionEvent : public IEvent {
public:
	PlaceDynamicObjectAtPositionEvent(int glid, short invenType, float x, float y, float z, float dx, float dy, float dz);
	void ExtractInfo(int &glid, short &invenType, float &x, float &y, float &z, float &dx, float &dy, float &dz);

	DEFINE_SECURE_CLASS_INFO(PlaceDynamicObjectAtPositionEvent, PackVersion(1, 1, 0, 0), EP_NORMAL)
};

// Superclass for any notification event that carry a dynamic object placement ID
class DynamicObjectNotificationEvent : public IEvent {
public:
	DynamicObjectNotificationEvent(EVENT_ID eventId, int placementId);
	DynamicObjectNotificationEvent(EVENT_ID eventId, EventHeader *eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority) :
			IEvent(eventId, eh, buffer, len, from, priority) {}
	void ExtractInfo(int &placementId);
};

// Triggered when CDynamicPlacementObj::InitPlacement() completes.
class DynamicObjectInitializedEvent : public DynamicObjectNotificationEvent {
public:
	DynamicObjectInitializedEvent(int placementId) :
			DynamicObjectNotificationEvent(ClassId(), placementId) {}
	DEFINE_SECURE_CLASS_INFO(DynamicObjectInitializedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

// Triggered if CDynamicPlacementObj::InitPlacement() will be delayed. Progress menu should considered the object being ready.
class DynamicObjectInitDelayedEvent : public DynamicObjectNotificationEvent {
public:
	DynamicObjectInitDelayedEvent(int placementId) :
			DynamicObjectNotificationEvent(ClassId(), placementId) {}
	DEFINE_SECURE_CLASS_INFO(DynamicObjectInitDelayedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

// Triggered if underlying dynamic object resource failed to load
class DynamicObjectLoadingFailedEvent : public DynamicObjectNotificationEvent {
public:
	DynamicObjectLoadingFailedEvent(int placementId) :
			DynamicObjectNotificationEvent(ClassId(), placementId) {}
	DEFINE_SECURE_CLASS_INFO(DynamicObjectLoadingFailedEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline ControlDynamicObjectEvent::ControlDynamicObjectEvent(int placementId, ULONG ctlId, const std::vector<int> &ctlArgs) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)placementId);
	SetFilter((ULONG)ctlId);
	(*OutBuffer()) << (int)ctlArgs.size();
	for (std::vector<int>::const_iterator it = ctlArgs.begin(); it != ctlArgs.end(); ++it) {
		(*OutBuffer()) << *it;
	}
}

inline void ControlDynamicObjectEvent::ExtractInfo(int &placementId, ULONG &ctlId, std::vector<int> &ctlArgs) {
	placementId = (int)ObjectId();
	ctlId = Filter();
	ctlArgs.clear();

	int argCount = 0;

	(*InBuffer()) >> argCount;
	if (argCount > 0) {
		ctlArgs.resize(argCount);
		for (int i = 0; i < argCount; i++) {
			(*InBuffer()) >> ctlArgs[i];
		}
	}
}

inline UpdateDynamicObjectTextureEvent::UpdateDynamicObjectTextureEvent(int placementId, int textureAssetId, const char *textureUrl, int playerId) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)placementId);
	SetFilter((ULONG)textureAssetId);
	(*OutBuffer()) << textureUrl;
	(*OutBuffer()) << playerId;
}

inline void UpdateDynamicObjectTextureEvent::ExtractInfo(int &placementId, int &textureAssetId, std::string &textureUrl, int &playerId) {
	placementId = (int)ObjectId();
	textureAssetId = (int)Filter();
	(*InBuffer()) >> textureUrl;
	(*InBuffer()) >> playerId;
}

inline SetDynamicObjectTexturePanningEvent::SetDynamicObjectTexturePanningEvent(int placementId, int meshId, int uvSetId, float uIncr, float vIncr) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)placementId);
	(*OutBuffer()) << meshId << uvSetId << uIncr << vIncr;
}

inline void SetDynamicObjectTexturePanningEvent::ExtractInfo(int &placementId, int &meshId, int &uvSetId, float &uIncr, float &vIncr) {
	placementId = (int)ObjectId();
	(*InBuffer()) >> meshId >> uvSetId >> uIncr >> vIncr;
}

inline UpdatePictureFrameFriendEvent::UpdatePictureFrameFriendEvent(int placementId, int friendId, const char *textureUrl) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)placementId);
	SetFilter((ULONG)friendId);
	(*OutBuffer()) << textureUrl;
}

inline void UpdatePictureFrameFriendEvent::ExtractInfo(int &placementId, int &friendId, std::string &textureUrl) {
	placementId = (int)ObjectId();
	friendId = (int)Filter();
	(*InBuffer()) >> textureUrl;
}

// DRF - Added
inline UpdateDynamicObjectMediaVolumeAndRadiusEvent::UpdateDynamicObjectMediaVolumeAndRadiusEvent(int placementId, const MediaParams &mediaParams, LONG zoneIndex, int networkAssignedId) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)placementId);
	SetFilter((ULONG)zoneIndex);
	(*OutBuffer())
		<< mediaParams.GetVolume()
		<< mediaParams.GetRadiusAudio()
		<< mediaParams.GetRadiusVideo()
		<< networkAssignedId;
}

// DRF - Added
inline void UpdateDynamicObjectMediaVolumeAndRadiusEvent::ExtractInfo(int &placementId, MediaParams &mediaParams, int *zoneIndex, int *networkAssignedId) {
	placementId = (int)ObjectId();
	double mVolPct;
	double mRadiusAudio;
	double mRadiusVideo;
	(*InBuffer()) >> mVolPct >> mRadiusAudio >> mRadiusVideo;
	mediaParams = MediaParams("", "", mVolPct, mRadiusAudio, mRadiusVideo);

	if (zoneIndex)
		*zoneIndex = (int)Filter();

	int naid = 0;
	(*InBuffer()) >> naid;
	if (networkAssignedId)
		*networkAssignedId = naid;
}

inline PlaceDynamicObjectAtPositionEvent::PlaceDynamicObjectAtPositionEvent(int glid, short invenType, float x, float y, float z, float dx, float dy, float dz) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)glid);
	SetFilter((ULONG)invenType);
	(*OutBuffer()) << (double)x;
	(*OutBuffer()) << (double)y;
	(*OutBuffer()) << (double)z;
	(*OutBuffer()) << (double)dx;
	(*OutBuffer()) << (double)dy;
	(*OutBuffer()) << (double)dz;
}

inline void PlaceDynamicObjectAtPositionEvent::ExtractInfo(int &glid, short &invenType, float &x, float &y, float &z, float &dx, float &dy, float &dz) {
	double _x, _y, _z, _dx, _dy, _dz;
	glid = (int)ObjectId();
	invenType = (short)Filter();
	(*InBuffer()) >> _x >> _y >> _z >> _dx >> _dy >> _dz;
	x = (float)_x;
	y = (float)_y;
	z = (float)_z;
	dx = (float)_dx;
	dy = (float)_dy;
	dz = (float)_dz;
}

inline DynamicObjectNotificationEvent::DynamicObjectNotificationEvent(EVENT_ID eventId, int placementId) :
		IEvent(eventId) {
	SetObjectId((OBJECT_ID)placementId);
}

inline void DynamicObjectNotificationEvent::ExtractInfo(int &placementId) {
	placementId = (int)ObjectId();
}

void RegisterDynamicObjectEvents(IDispatcher &disp);

} // namespace KEP
