///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include <KEPHelpers.h>

namespace KEP {

class GetFunctionEvent : public IEvent {
public:
	GetFunctionEvent(const char* function, const char* vmName, void* vm, EVENT_ID id) :
			IEvent(id) {
		(*OutBuffer()) << function << vmName << (ULONG)PtrToUint(vm);
	}

	DEFINE_SECURE_GENERIC_CLASS_INFO(GetFunctionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

} // namespace KEP
