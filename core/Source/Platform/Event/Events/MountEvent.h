///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"

namespace KEP {

class MountEvent : public IEvent {
public:
	MountEvent(int AIconfigIndex, long zoneIndex);

	void ExtractInfo(int &AIconfigIndex, long &zoneIndex);

	DEFINE_SECURE_CLASS_INFO(MountEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)
};

inline MountEvent::MountEvent(int AIconfigIndex, long zoneIndex) :
		IEvent(ClassId()) {
	SetObjectId((OBJECT_ID)AIconfigIndex);
	SetFilter((ULONG)zoneIndex);
}

inline void MountEvent::ExtractInfo(int &AIconfigIndex, long &zoneIndex) {
	AIconfigIndex = (int)ObjectId();
	zoneIndex = (long)Filter();
}

} // namespace KEP
