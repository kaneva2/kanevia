///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"

namespace KEP {

class UpdateScriptZoneBackgroundEvent : public IBackgroundDBEvent {
public:
	UpdateScriptZoneBackgroundEvent(unsigned zoneInstanceId, unsigned zoneType, unsigned spinDownDelayMinutes, bool spinUp, EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority),
			m_zoneInstanceId(zoneInstanceId),
			m_zoneType(zoneType),
			m_spinDownDelayMinutes(spinDownDelayMinutes),
			m_spinUp(spinUp) {
	}

	unsigned getZoneInstanceId() const { return m_zoneInstanceId; }
	unsigned getZoneType() const { return m_zoneType; }
	unsigned getSpinDownDelayMinutes() const { return m_spinDownDelayMinutes; }
	bool isSpinUp() const { return m_spinUp; }

	DEFINE_DB_CLASS_INFO(UpdateScriptZoneBackgroundEvent, PackVersion(1, 0, 0, 0))
private:
	unsigned m_zoneInstanceId;
	unsigned m_zoneType;
	unsigned m_spinDownDelayMinutes;
	bool m_spinUp;
};

} // namespace KEP
