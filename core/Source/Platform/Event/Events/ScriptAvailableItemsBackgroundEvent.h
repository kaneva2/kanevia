///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IBackgroundDBEvent.h"

namespace KEP {

/// class for passing params back for forth to DB background thd
/// for
class ScriptAvailableItemsBackgroundEvent : public IBackgroundDBEvent {
public:
	ScriptAvailableItemsBackgroundEvent(EVENT_ID id = ClassId(), ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			IBackgroundDBEvent(id, filter, objectId, priority) {}

	DEFINE_DB_CLASS_INFO(ScriptAvailableItemsBackgroundEvent, PackVersion(1, 0, 0, 0))
};

} // namespace KEP