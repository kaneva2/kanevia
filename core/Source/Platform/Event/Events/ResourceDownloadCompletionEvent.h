///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEvent.h"

namespace KEP {

class ResourceManager;
class IResource;

class ResourceDownloadCompletionEvent : public IEvent {
public:
	ResourceDownloadCompletionEvent(ResourceManager* pResourceManager, IResource* pRes) :
			IEvent(ClassId()),
			m_pResourceManager(pResourceManager),
			m_pRes(pRes) {
		assert(m_pResourceManager != nullptr && m_pRes != nullptr);
	}

	ResourceManager* getResourceManager() const { return m_pResourceManager; }
	IResource* getResource() const { return m_pRes; }

	DEFINE_SECURE_CLASS_INFO(ResourceDownloadCompletionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)

private:
	ResourceManager* m_pResourceManager;
	IResource* m_pRes;
};

} // namespace KEP
