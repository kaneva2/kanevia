///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "UseSkillEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(UseSkillEvent)

UseSkillEvent::UseSkillEvent(IGetSet *player, ISkillDB *skillDatabase, int skillType,
	int skillLevel, int abilityIndex,
	ULONG manditoryWaitBeforeSkillUse, ULONG skillUseTimeStamp) :
		IEvent(ClassId()) {
	SetFrom(SERVER_NETID); // always from server
	SetFilter(skillType);

	// pack it
	(*OutBuffer()) << (ULONG)player
				   << (long)skillDatabase
				   << skillLevel
				   << abilityIndex
				   << manditoryWaitBeforeSkillUse
				   << skillUseTimeStamp;
}

EVENT_CONSTRUCTOR_PROTOTYPE(UseSkillEvent) {
	// do nothing here
}
