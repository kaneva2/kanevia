///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../../Engine/Blades/BladeExports.h"
#include "../Base/IEventBlade.h"
#include "../Base/EventHandler.h"
#include "../Base/IEvent.h"
#include "../Base/IDispatcher.h"
#include "../Events/GenericEvent.h"
#include "../Events/ShutdownEvent.h"
#include "jsThread.h"
#include <io.h>
#include <fcntl.h>
#include "Core/Util/WindowsAPIWrappers.h"

using namespace std;

namespace KEP {

class REPL;
static REPL *s_instance = NULL;

#define MAX_LINE_LENGTH 4096
#define TEE_THREAD_BUFFER_SIZE 1024
#define WORKER_THREAD_STOP_TIMEOUT 3000
#define CTRL_CLOSE_WAIT_TIMEOUT 3000

class TeeThread : public jsThread {
public:
	TeeThread(HANDLE hPipe, HANDLE hConsole) :
			jsThread("REPLTee"),
			m_hPipe(hPipe),
			m_hConsoleOut(hConsole),
			m_hFileOut(NULL),
			m_terminating(false) {}

	void SetFileHandle(HANDLE hFile) { m_hFileOut = hFile; }

	ULONG _doWork() {
		if (!ConnectNamedPipe(m_hPipe, nullptr)) {
			DWORD err = GetLastError();
			if (err != ERROR_PIPE_CONNECTED) {
				//LogError("ConnectNamedPipe error: " << GetLastError());
				return 1;
			}
		}
		while (!m_terminating) {
			unsigned char buffer[TEE_THREAD_BUFFER_SIZE];
			DWORD bytesRead = 0, bytesWritten = 0;
			// Blocking pipe read
			if (ReadFile(m_hPipe, buffer, sizeof(buffer), &bytesRead, NULL) && bytesRead > 0) {
				// Write console
				WriteFile(m_hConsoleOut, buffer, bytesRead, &bytesWritten, NULL);
				jsAssert(bytesRead == bytesWritten);
				// Write file if tee'd
				if (m_hFileOut) {
					WriteFile(m_hFileOut, buffer, bytesRead, &bytesWritten, NULL);
					jsAssert(bytesRead == bytesWritten);
				}
			} else {
				// Error occurred -- could be broken pipe.
				Sleep(50);
			}
		}

		return 0;
	}

	void setTerminating() { m_terminating = true; }

private:
	HANDLE m_hPipe, m_hConsoleOut;
	volatile HANDLE m_hFileOut;
	bool m_terminating;
};

class REPL : public IEventBlade, public jsThread {
public:
	REPL() :
			jsThread("REPL"),
			m_title("KEP Debugging Console"),
			m_hConsoleIn(NULL),
			m_hConsoleOut(NULL),
			m_hConsoleErr(NULL),
			m_hPipeReadStdout(NULL),
			m_hPipeWriteStdout(NULL),
			m_hFileOut(NULL),
			m_evaluating(false),
			m_dispatcher(NULL),
			m_enableEventId(BAD_EVENT_ID),
			m_resultEventId(BAD_EVENT_ID),
			m_inputEventId(BAD_EVENT_ID),
			m_consoleRedirected(false),
			m_mainThreadId(::GetCurrentThreadId()),
			m_enabled(false) {
		jsAssert(s_instance == NULL);
		s_instance = this;
	}

	~REPL() {
		if (m_enabled) {
			// Terminate jsThread gracefully
			jsThread::stop(WORKER_THREAD_STOP_TIMEOUT, false);

			// Flag tee thread for termination
			if (m_teeThread) {
				m_teeThread->setTerminating();
			}
			if (m_stderrThread) {
				m_stderrThread->setTerminating();
			}

			// Write something the the pipe to wake up tee thread
			const char *exitMsg = "Exiting...\n";
			fputs(exitMsg, stdout);
			fflush(stdout);
			fputs(exitMsg, stderr);
			fflush(stderr);

			// Terminate tee thread and dispose it
			if (m_teeThread) {
				m_teeThread->stop(WORKER_THREAD_STOP_TIMEOUT, false);
				m_teeThread.reset();
			}

			if (m_stderrThread) {
				m_stderrThread->stop(WORKER_THREAD_STOP_TIMEOUT, false);
				m_stderrThread.reset();
			}

			// Close pipe
			m_hPipeReadStdout.Close();
			m_hPipeWriteStdout.Close();
			m_hPipeReadStderr.Close();
		}
	}

#pragma warning(push)
#pragma warning(disable : 4481)

	ULONG _doWork() override {
		ready();
		while (!isTerminated()) iterate();
		notee();
		return 0;
	}

	// IEventBlade overrides
	virtual bool RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *ef) override {
		if (dispatcher == 0 || ef == 0) {
			assert(false);
			return false;
		}

		// Init static factory in this DLL
		IEvent::SetEncodingFactory(ef);

		// Register events
		m_dispatcher = dispatcher;
		m_enableEventId = m_dispatcher->RegisterEvent("REPLEnableConsoleEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
		m_resultEventId = m_dispatcher->RegisterEvent("REPLResultEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
		m_inputEventId = m_dispatcher->RegisterEvent("REPLInputEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
		return true;
	};

	virtual bool RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory) override {
		jsAssert(m_enableEventId != BAD_EVENT_ID && m_resultEventId != BAD_EVENT_ID);
		m_dispatcher->RegisterHandler(REPL::REPLEnableConsoleHandler, (ULONG)this, "REPL::REPLEnableConsoleHandler", PackVersion(1, 0, 0, 0), m_enableEventId, EP_NORMAL);
		m_dispatcher->RegisterHandler(REPL::REPLResultHandler, (ULONG)this, "REPL::ReplResultHandler", PackVersion(1, 0, 0, 0), m_resultEventId, EP_NORMAL);
		return true;
	}

	virtual bool ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory) override { return true; }

	// Monitored overrides
	virtual Monitored::DataPtr monitor() const override { return Monitored::DataPtr(new Monitored::Data()); }

	// IBlade overrides
	virtual ULONG Version() const override { return BladeVersion(); }
	virtual string PathBase() const override { return m_pathBase; }
	virtual void SetPathBase(const string &baseDir, bool readonly) override { m_pathBase = baseDir; }
	virtual void FreeAll() override { FreeConsole(); }
	virtual const char *Name() const override { return "REPL"; }
	virtual bool IsBaseDirReadOnly() const override { return true; }

#pragma warning(pop)

	// Blade additions
	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }

	// Event handler
	static EVENT_PROC_RC REPLEnableConsoleHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
		jsVerifyReturn(lparam != 0, EVENT_PROC_RC::NOT_PROCESSED);
		REPL *me = (REPL *)lparam;
		me->enable();
		return EVENT_PROC_RC::CONSUMED;
	}

	static EVENT_PROC_RC REPLResultHandler(ULONG lparam, IDispatcher *disp, IEvent *e) {
		jsVerifyReturn(lparam != 0, EVENT_PROC_RC::NOT_PROCESSED);
		REPL *me = (REPL *)lparam;
		int incomplete;
		string error;
		*e->InBuffer() >> incomplete >> error;

		if (incomplete != 0) {
			me->more();
		} else if (!error.empty()) {
			me->error(error + "\n");
			me->ready();
		} else {
			me->ready();
		}

		return EVENT_PROC_RC::CONSUMED;
	}

	// This handler runs in a separate thread OS created
	static BOOL WINAPI CtrlHandler(DWORD type) {
		HANDLE hMainThread = NULL;

		switch (type) {
			case CTRL_BREAK_EVENT:
			case CTRL_C_EVENT:
				// Stop waiting for results (lost cause)
				s_instance->cancel();
				break;

			case CTRL_CLOSE_EVENT:
			case CTRL_LOGOFF_EVENT:
			case CTRL_SHUTDOWN_EVENT:
				// Tell KEPClient to shutdown instead
				s_instance->sendShutdownRequestEvent();
				// Wait for main thread to terminate
				hMainThread = OpenThread(SYNCHRONIZE, FALSE, s_instance->m_mainThreadId);
				jsAssert(hMainThread != NULL);
				WaitForSingleObject(hMainThread, CTRL_CLOSE_WAIT_TIMEOUT);
				break;
		}

		return TRUE;
	}

	void sendShutdownRequestEvent() {
		EVENT_ID eventId = m_dispatcher->GetEventId("ShutdownRequestEvent");
		jsVerifyReturnVoid(eventId != BAD_EVENT_ID);
		IEvent *e = m_dispatcher->MakeEvent(eventId);
		jsVerifyReturnVoid(e != NULL);
		m_dispatcher->QueueEvent(e);
	}

	void sendRedirectConsoleIOEvent() {
		EVENT_ID eventId = m_dispatcher->GetEventId("RedirectConsoleIOEvent");
		jsVerifyReturnVoid(eventId != BAD_EVENT_ID);
		IEvent *e = m_dispatcher->MakeEvent(eventId);
		jsVerifyReturnVoid(e != NULL);
		*e->OutBuffer() << "" << GetStdOutPipeName() << GetStdErrPipeName();
		m_dispatcher->ProcessSynchronousEvent(e);
		m_consoleRedirected = true;
	}

	void enable();

	bool tee(const string &path);
	bool notee();

	void ready();
	void more();
	void iterate();
	string read();
	void eval(const string &exp); // asynchronous evaluation -- no results yet
	void print(const string &res);
	void error(const string &err);
	void cancel();

	std::string GetStdOutPipeName();
	std::string GetStdErrPipeName();

private:
	// Console I/O handles
	HANDLE m_hConsoleIn, m_hConsoleOut, m_hConsoleErr;
	// Pipe handles for tee-ing
	FileHandleWrapper m_hPipeReadStdout, m_hPipeWriteStdout;
	FileHandleWrapper m_hPipeReadStderr;
	// File handles for tee-ing
	HANDLE m_hFileOut;

	string m_title;
	volatile bool m_evaluating;
	IDispatcher *m_dispatcher;
	EVENT_ID m_enableEventId, m_resultEventId, m_inputEventId;
	string m_pathBase;
	bool m_consoleRedirected;
	DWORD m_mainThreadId;
	std::unique_ptr<TeeThread> m_teeThread;
	std::unique_ptr<TeeThread> m_stderrThread;
	bool m_enabled;
};

static std::string GetBasePipeName() {
	std::ostringstream strmPipeName;
	strmPipeName << "\\\\.\\pipe\\KEPClientREPL." << GetCurrentProcessId();
	return strmPipeName.str();
}

std::string REPL::GetStdOutPipeName() {
	return GetBasePipeName() + ".stdout";
}

std::string REPL::GetStdErrPipeName() {
	return GetBasePipeName() + ".stderr";
}

void REPL::enable() {
	if (!m_enabled) {
		// Allocate console
		if (!AllocConsole()) {
			return;
		}

		// Disable close button on the console window
		DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);

		// Capture console CTRL events
		SetConsoleCtrlHandler(&REPL::CtrlHandler, TRUE);

		// Obtain console I/O handles
		m_hConsoleIn = GetStdHandle(STD_INPUT_HANDLE);
		m_hConsoleOut = GetStdHandle(STD_OUTPUT_HANDLE);
		m_hConsoleErr = GetStdHandle(STD_ERROR_HANDLE);

		// Create two pipes for output redirection
		//jsVerify(TRUE == CreatePipe(&m_hPipeReadStdout, &m_hPipeWriteStdout, NULL, 0));
		auto CreateInboundPipe = [](const std::string &strName) -> FileHandleWrapper {
			const size_t nBufferSize = 8 * 1024;
			return FileHandleWrapper(CreateNamedPipe(Utf8ToUtf16(strName).c_str(), PIPE_ACCESS_INBOUND, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS, PIPE_UNLIMITED_INSTANCES, nBufferSize, nBufferSize, 0, nullptr));
		};
		m_hPipeReadStdout = CreateInboundPipe(GetStdOutPipeName());
		m_hPipeReadStderr = CreateInboundPipe(GetStdErrPipeName());

		//m_hPipeWriteStdout = CreateFile(Utf8ToUtf16(GetStdOutPipeName()).c_str(), GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, 0);

		// Create tee thread
		if (m_hPipeReadStdout) {
			m_teeThread = std::make_unique<TeeThread>(m_hPipeReadStdout.GetHandle(), m_hConsoleOut);
			m_teeThread->start();

			m_stderrThread = std::make_unique<TeeThread>(m_hPipeReadStderr.GetHandle(), m_hConsoleErr);
			m_stderrThread->start();
		}

		// start jsThread
		start();
		m_enabled = true;

		sendRedirectConsoleIOEvent();
	}
}

bool REPL::tee(const string &path) {
	notee();

	// Create or open output file
	m_hFileOut = CreateFile(Utf8ToUtf16(path).c_str(), FILE_APPEND_DATA, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	jsAssert(m_hFileOut != NULL);
	if (m_hFileOut == NULL) {
		stringstream ss;
		ss << "Error creating outfile: " << path << ". Err = " << GetLastError() << "\n";
		error(ss.str());
		return false;
	}

	// Tell tee thread to update output file handle
	m_teeThread->SetFileHandle(m_hFileOut);
	return true;
}

bool REPL::notee() {
	if (m_hFileOut) {
		m_teeThread->SetFileHandle(NULL);
		CloseHandle(m_hFileOut);
		m_hFileOut = NULL;
	}
	return true;
}

void REPL::ready() {
	m_evaluating = false;
	SetConsoleTitle(Utf8ToUtf16(m_title).c_str());
	print("> ");
}

void REPL::more() {
	m_evaluating = false;
	SetConsoleTitle(Utf8ToUtf16(m_title).c_str());
	print(">> ");
}

void REPL::iterate() {
	const int interval = 10;

	if (m_evaluating == TRUE || !m_hConsoleIn) {
		Sleep(interval);
		return;
	}

	string result;
	switch (WaitForSingleObject(m_hConsoleIn, interval)) {
		case WAIT_OBJECT_0:
			eval(read());
			break;

		case WAIT_TIMEOUT:
		case WAIT_ABANDONED:
		case WAIT_FAILED:
			break;
	}
}

string REPL::read() {
	// Read line
	wchar_t buffer[MAX_LINE_LENGTH + 1];
	DWORD charsRead = 0;
	ReadConsoleW(m_hConsoleIn, buffer, MAX_LINE_LENGTH, &charsRead, NULL);
	jsAssert(charsRead <= MAX_LINE_LENGTH);
	buffer[charsRead] = 0;

	string line = Utf16ToUtf8(buffer);

	// Tee to output file if enabled
	if (m_hFileOut) {
		DWORD charsWritten = 0;
		WriteFile(m_hFileOut, line.c_str(), line.size(), &charsWritten, NULL);
		jsAssert(line.size() == charsWritten);
	}

	// Trim off trailing CR/NL characters
	size_t len = line.size();
	while (len > 0 && (line[len - 1] == '\r' || line[len - 1] == '\n')) {
		len--;
	}
	line.resize(len);

	return line;
}

void REPL::print(const string &res) {
	DWORD charsWritten = 0;
	fputs(res.c_str(), stdout);
	fflush(stdout);
	jsAssert(charsWritten == res.size());
}

void REPL::error(const string &res) {
	DWORD charsWritten = 0;
	WriteConsoleW(m_hConsoleErr, Utf8ToUtf16(res).c_str(), res.size(), &charsWritten, NULL);
	jsAssert(charsWritten == res.size());
}

void REPL::cancel() {
	// Stop waiting for result from pending evaluation requests
	IEvent *e = m_dispatcher->MakeEvent(m_resultEventId);
	jsVerifyReturnVoid(e != NULL);
	*e->OutBuffer() << 0 << "User break (Ctrl-C)";
	m_dispatcher->QueueEvent(e);
}

void REPL::eval(const string &exp) {
	if (!m_consoleRedirected) {
		//sendRedirectConsoleIOEvent();
	}

	if (exp.substr(0, 4) == "tee ") {
		// tee command
		string path = exp.substr(4);
		if (tee(path)) {
			print("Logging to file '" + path + "'\n"); // Copied MySQL message
		}
		ready();
	} else if (exp == "notee") {
		// notee command
		if (notee()) {
			print("Outfile disabled\n"); // Copied MySQL message
		}
		ready();
	} else {
		// Lua statement
		m_evaluating = true;
		IEvent *e = m_dispatcher->MakeEvent(m_inputEventId);
		*e->OutBuffer() << exp;
		m_dispatcher->QueueEvent(e);

		// async processing
		SetConsoleTitle(Utf8ToUtf16(m_title + " (Evaluating)").c_str());
	}
}

IBlade *createREPL() {
	return new REPL();
}

} // namespace KEP

using namespace KEP;

BEGIN_BLADE_LIST
ADD_BLADE(REPL)
END_BLADE_LIST

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD ul_reason_for_call,
	LPVOID lpReserved) {
	switch (ul_reason_for_call) {
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}
