///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/include/kepcommon.h"
#include "Core/Util/monitored.h"
#include "Event/Base/ScriptingEventHandler.h"

typedef struct _object PyObject;
typedef struct _ts PyThreadState;

namespace KEP {

class PythonEventHandler : public TScriptingEventHandler<PyObject *, PyObject *> /*fn and module*/
{
public:
	PythonEventHandler();
	~PythonEventHandler();

	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }

	// IEventHandler override
	virtual EVENT_PROC_RC ProcessEvent(IDispatcher *dispatcher, IEvent *e);

	virtual void FreeAll();

	// overrides to register our own handlers, too
	bool RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);

private:
	bool RegisterPythonHandler(IDispatcher *dispatcher, PyObject *pythonFn, ULONG eventVersion, EVENT_ID eventId,
		ULONG filter, bool filterIsMask,
		OBJECT_ID filteredObjectId,
		EVENT_PRIORITY priority);

	// ScriptingEventHandler override
	bool callRegisterFnForEachScript(const char *fnName, IDispatcher *dispatcher, IEncodingFactory *factory, bool vmKeepIfFnExists);
	bool callRegisterFnForVM(const char *fnName, IScriptVM *vm, IDispatcher *dispatcher, IEncodingFactory *factory, std::string filePath);
	PyObject *callFunctionForModule(const char *fnName, PyObject *module, IDispatcher *dispatcher, IEncodingFactory *factory);

	bool FunctionRegistration(IScriptVM &vm);
	bool UnRegisterScriptingEventHandlers(IDispatcher *dispatcher, IScriptVM *vm = 0);

	// callback to register a handler
	static PyObject *PythonRegisterEventHandler(PyObject *, PyObject *);
	static PyObject *PythonRegisterFilteredEventHandler(PyObject *, PyObject *);
	// callback to register an event
	static PyObject *PythonRegisterEvent(PyObject *, PyObject *);

	virtual void ParentVMRegister(IScriptVM *);

	static EVENT_PROC_RC GetFunctionHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC EventHandlerFn(ULONG lparam, IDispatcher *dispatcher, IEvent *e);

	bool m_initialized; // do we need to free anything?
	bool m_installedOk; // does Python appear to be installed

	std::map<std::string, PyObject *> m_loadedModules;
};

} // namespace KEP
