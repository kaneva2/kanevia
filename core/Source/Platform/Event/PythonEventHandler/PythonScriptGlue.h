///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <js.h>

// macros to make glue code for Python
#include <KEPException.h>
#include "IKEPObject.h"

#include "PythonScriptVM.h"

#include "..\KEPUtil\RefPtr.h"
#include "ScriptGlueCommon.h"

namespace KEP {
class IKEPObject;
} // namespace KEP

// avoid benign warning due to Python/log4cplus conflict
#undef HAVE_FTIME

extern "C" {
#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
}

// avoid benign warning due to Python/log4cplus conflict
#undef HAVE_FTIME

#define SG_VM_NAME "Python"
#define SG_NATIVE_VM new PythonScriptVM()

//=============================================================
//
// inlines used by macros below
//
//=============================================================

// overrides of sg_Push for various datatypes, used by macros below -- all refrernce counts here ok
inline void sg_Push(PyObject *pArgs, double value, int index) {
	PyTuple_SetItem(pArgs, index, Py_BuildValue("d", value));
}
inline void sg_Push(PyObject *pArgs, const char *value, int index) {
	PyTuple_SetItem(pArgs, index, Py_BuildValue("s", value));
}
inline void sg_Push(PyObject *pArgs, KEP::IKEPObject *value, int index) {
	PyObject *temp = PyCObject_FromVoidPtr(value, 0);
	PyTuple_SetItem(pArgs, index, Py_BuildValue("O", temp));
	Py_DECREF(temp);
}

template <class T, class M>
void tsg_Pop(PyObject *pArgs, T &value, int index, M method) {
	PyObject *__temp = PyTuple_GetItem(pArgs, index); // borrowed ref
	if (__temp == 0)
		value = 0;
	else
		value = (T)method(__temp);
}

template <class T, class B>
bool tsg_CastToDerivedObject(T *&pTo, PyObject *pFrom) {
	B *b = 0;
	if (PyCObject_Check(pFrom))
		b = (B *)PyCObject_AsVoidPtr(pFrom);
	else if (PyNumber_Check(pFrom))
		b = (B *)PyLong_AsVoidPtr(PyNumber_Long(pFrom));
	else if (PyLong_Check(pFrom))
		b = (B *)PyLong_AsVoidPtr(pFrom);
	else if (PyFloat_Check(pFrom))
		b = (B *)(ULONG)PyLong_AsDouble(pFrom);

	pTo = dynamic_cast<T *>(b);
	return pTo != 0;
}

template <class T>
T *tsg_PopObject(PyObject *pArgs, int index) {
	PyObject *__temp = PyTuple_GetItem(pArgs, index); // borrowed ref
	if (__temp == 0)
		return 0;
	else {
		// may be sent as number..., use to just call PyCObject_AsVoidPtr
		// T *ret = 0;
		// tsg_CastToDerivedObject<T,T>( ret, __temp );
		// return ret;
		return (T *)PyCObject_AsVoidPtr(__temp);
	}
}

inline void sg_Pop(PyObject *pArgs, double &value, int index) {
	tsg_Pop(pArgs, value, index, PyFloat_AsDouble);
}
inline void sg_Pop(PyObject *pArgs, const char *&value, int index) {
	tsg_Pop(pArgs, value, index, PyString_AsString);
}
inline void sg_Pop(PyObject *pArgs, KEP::IKEPObject *&value, int index) {
	value = (KEP::IKEPObject *)tsg_PopObject<KEP::IKEPObject>(pArgs, index);
}
inline void sg_Pop(PyObject *pArgs, PyObject *&value, int index) {
	value = PyTuple_GetItem(pArgs, index);
} // borrowed

inline PyObject *sg_Return(PyObject *__retArgs, int __retCount) {
	if (__retCount == 0) {
		if (__retArgs != 0)
			Py_DECREF(__retArgs);
		Py_RETURN_NONE;
	} else if (__retCount == 1 && PyTuple_Check(__retArgs)) {
		// if only one item, return as just one item which is a bit of a problem
		// when array of one is returned to script
		PyObject *ret = PyTuple_GetItem(__retArgs, 0); // borrowed
		Py_INCREF(ret);
		Py_DECREF(__retArgs);
		return ret;
	} else
		return __retArgs;
}

// set the number of return value, if you don't know at top
inline void sg_ResetReturnCount(PyObject *&__retArgs, int &__retCount, int retCount) {
	if (__retArgs != 0)
		Py_DECREF(__retArgs);
	__retCount = retCount;
	__retArgs = PyTuple_New(retCount);
}
#define SG_RESET_RETURN_COUNT(retCount) sg_ResetReturnCount(__retArgs, __retCount, retCount)

// return normally
#define SG_RETURN return sg_Return(__retArgs, __retCount)

// return an error, if got error pushing or popping
inline PyObject *sg_ReturnErr(PyObject *&__retArgs) {
	if (__retArgs != 0)
		Py_DECREF(__retArgs);
	return 0;
}
#define SG_RETURN_ERR return sg_ReturnErr(__retArgs)

// return a custom error
inline PyObject *sg_ReturnErrStr(PyObject *&__retArgs, const std::string &errStr) {
	if (__retArgs != 0)
		Py_DECREF(__retArgs);
	PyErr_SetString(PyExc_Exception, errStr.c_str());
	return 0;
}
#define SG_RETURN_ERR_STR(errStr) return sg_ReturnErrStr(__retArgs, std::string(__FUNCTION__) + ": " + errStr)

#define SG_RETURN_NULL Py_RETURN_NONE

// Script Glue Internal Function Redirect
#define SG_REDIRECT(fn) return fn(self, args);

// start a function
#define SG_BEGIN_FN(fn, retCount)                             \
	extern "C" PyObject *fn(PyObject *self, PyObject *args) { \
		self;                                                 \
		args;                                                 \
		PyObject *__temp = 0;                                 \
		__temp;                                               \
		int __popped = 0;                                     \
		__popped;                                             \
		int __pushed = 0;                                     \
		__pushed;                                             \
		int __retCount = retCount;                            \
		__retCount;                                           \
		PyObject *__retArgs = 0;                              \
		__retArgs;

// helper to automatically decref on destroy
class sg_DecRef {
public:
	sg_DecRef(PyObject *p) :
			m_p(p) {}
	operator PyObject *() { return m_p; }
	~sg_DecRef() {
		if (m_p)
			Py_DECREF(m_p);
	}

private:
	PyObject *m_p;
};

// Script Glue Push Objects
#define SG_DYNAMIC_CAST_TO_PUSH_OBJ(pFrom, baseType, pushObj) sg_DecRef pushObj = PyCObject_FromVoidPtr(dynamic_cast<baseType *>(pFrom), 0)

#define SG_CAST_TO_PUSH_OBJ(pFrom, pushObj) sg_DecRef pushObj = PyCObject_FromVoidPtr(pFrom, 0)

// DRF - RefPtr Validated Cast To Push Object
#define SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(pFrom, baseType, pushObj) \
	SG_DYNAMIC_CAST_TO_PUSH_OBJ(pFrom, baseType, pushObj);          \
	::RefPtrAdd(typeid(pFrom).name(), pushObj);

// DRF - RefPtr Validated Cast To Push Object
#define SG_VALID_CAST_TO_PUSH_OBJ(pFrom, pushObj) \
	SG_CAST_TO_PUSH_OBJ(pFrom, pushObj);          \
	::RefPtrAdd(typeid(pFrom).name(), pushObj);

// Script Glue Pop Objects
#define SG_DECLARE_POP_OBJ(type, ptr, popObj) \
	type *ptr;                                \
	PyObject *popObj;

// DRF - RefPtr Validated Cast From Pop Object
#define SG_VALID_CAST_FROM_POP_OBJ(igs, popObj)                  \
	if (!REF_PTR_VALID(popObj))                                  \
		SG_RETURN_ERR_STR("Invalid Object Passed As Parameter"); \
	if (!SG_CAST_TO_OBJECT(igs, popObj))                         \
		SG_RETURN_ERR_STR("Failed Casting Object Passed As Parameter");

// SetInstance() Pointers
#define SG_DECLARE_ICE_PTR(ice)           \
	auto ice = IClientEngine::Instance(); \
	if (!ice)                             \
		SG_RETURN_ERR;

#define SG_DECLARE_IGS_PTR(igs) \
	IGetSet *igs = g_pIGS;      \
	if (!igs)                   \
		SG_RETURN_ERR;

// for popping, did cast to object
template <class T>
bool SG_CAST_TO_OBJECT(T *&pTo, PyObject *pFrom) {
	if (PyCObject_Check(pFrom))
		pTo = (T *)PyCObject_AsVoidPtr(pFrom);
	else if (PyFloat_Check(pFrom))
		pTo = (T *)(ULONG)PyFloat_AsDouble(pFrom); // could be number...
	else
		pTo = 0; // null

	return pTo != 0;
}

template <class T, class B>
bool sg_CastToDerivedObject(T *&pTo, PyObject *pFrom) {
	B *b = 0;
	if (PyCObject_Check(pFrom))
		b = (B *)PyCObject_AsVoidPtr(pFrom);
	else if (PyFloat_Check(pFrom))
		b = (B *)(ULONG)PyFloat_AsDouble(pFrom); // could be number...
	else
		b = 0; // null

	pTo = dynamic_cast<T *>(b);
	return pTo != 0;
}

// for popping, did cast to object, first casting to a base class
#define SG_CAST_TO_DERIVED_OBJECT(pTo, derivedType, baseType, pFrom) sg_CastToDerivedObject<derivedType, baseType>(pTo, pFrom)

#define SG_POP_OBJECT(type, name) type *name = tsg_PopObject<type>(args, __popped++);

#define SG_POP_OBJECT_CAST(myType, baseType, name) \
	myType *name = dynamic_cast<myType *>(tsg_PopObject<baseType>(args, __popped++));

#define SG_POP(value) sg_Pop(args, value, __popped++);

inline void sg_CheckRet(PyObject *&__retArgs, int __retCount) {
	if (__retArgs == 0)
		__retArgs = PyTuple_New(__retCount); // new ref returned.  see sg_Return also
}

#define SG_PUSH_OBJECT(p)                                                 \
	{                                                                     \
		sg_CheckRet(__retArgs, __retCount);                               \
		PyObject *temp = PyCObject_FromVoidPtr(p, 0);                     \
		PyTuple_SetItem(__retArgs, __pushed++, Py_BuildValue("O", temp)); \
		Py_DECREF(temp);                                                  \
	}

#define SG_PUSH_OBJECT_CAST(baseType, p)                                                                                                       \
	{                                                                                                                                          \
		sg_CheckRet(__retArgs, __retCount);                                                                                                    \
		PyObject *temp = PyCObject_FromVoidPtr(dynamic_cast<baseType>(p), 0) PyTuple_SetItem(__retArgs, __pushed++, Py_BuildValue("O", temp)); \
		Py_DECREF(temp);                                                                                                                       \
	}

inline void sg_PushX(PyObject *&__retArgs, int &__retCount, int &__pushed, KEP::IKEPObject *value) {
	sg_CheckRet(__retArgs, __retCount);
	sg_Push(__retArgs, value, __pushed++);
}
inline void sg_PushX(PyObject *&__retArgs, int &__retCount, int &__pushed, const char *value) {
	sg_CheckRet(__retArgs, __retCount);
	sg_Push(__retArgs, value, __pushed++);
}
inline void sg_PushX(PyObject *&__retArgs, int &__retCount, int &__pushed, double value) {
	sg_CheckRet(__retArgs, __retCount);
	sg_Push(__retArgs, value, __pushed++);
}
#define SG_PUSH(value) sg_PushX(__retArgs, __retCount, __pushed, value)

// end of the function
#define SG_END_FN }

//=============================================================
//
// Push/pop by varg-method macros
//
//=============================================================

// return TRUE/FALSE for ok
#define SG_POPARGS(...) PyArg_ParseTuple(args, __VA_ARGS__)

// return nonzero for ok
#define SG_PUSHARGS(...) (__retArgs = Py_BuildValue(__VA_ARGS__))

//=============================================================
//
// registration of your callback functions in the VM
//
//=============================================================

// used in header, or by caller to create prototype of registration
#define SG_DEFINE_VM_REGISTER(name) void name(IScriptVM *);

// methods to register one or more methods with the VM
// the registration method itself
#define SG_VM_REGISTER(name) void name(IScriptVM *) {
// make the list of fns to register
#define SG_BEGIN_SG_REGISTER(name) \
	{                              \
		char *__classname = #name; \
		static PyMethodDef PythonMethods[] = {
#define SG_REGISTER(remotename, localname) { #remotename, localname, METH_VARARGS, "Call " #remotename },

#define SG_END_REGISTER                                                           \
	{ NULL, NULL, 0, NULL }                                                       \
	}                                                                             \
	;                                                                             \
	if (Py_InitModule(__classname, PythonMethods) == 0) {                         \
		throw new KEP::KEPException((loadStr(IDS_ES_INIT_MODULE) + __classname)); \
	}                                                                             \
	}

#define SG_END_VM_REGISTER }

#define NO_RETURN_VALUE 0
#define ONE_RETURN_VALUE 1
#define TWO_RETURN_VALUES 2
#define THREE_RETURN_VALUES 3
#define FOUR_RETURN_VALUES 4
#define RETURN_VALUES(x) x

namespace PythonScriptGlue {
class ScriptArgManager : public IScriptArgManager {
public:
	ScriptArgManager(PyObject *pPyArgs) :
			m_pPyArgs(pPyArgs), m_pReturn(nullptr) {}

	virtual std::string GetCallInfo() { return std::string(); }
	virtual bool GetValue(bool *pValue) { return false; }
	virtual bool GetValue(float *pValue) { return false; }
	virtual bool GetValue(double *pValue) { return false; }
	virtual bool GetValue(int32_t *pValue) { return false; }
	virtual bool GetValue(const char **pValue) { return false; }
	virtual bool GetValue(std::string *pValue) { return false; }
	virtual bool GetPointer(void **pValue) { return false; }

	virtual bool PushArgAtIndex(size_t idx) { return false; }
	virtual bool PushTableElem(const char *pszKey) { return false; }
	virtual bool PushArrayElem(size_t idx) { return false; }
	virtual bool Pop() { return false; }

	virtual bool HasArgAtIndex(size_t idxArg) override { return false; }

	virtual bool GetArraySize(size_t *pSize) { return false; }

	virtual bool PushValue(nullptr_t) { return false; }
	virtual bool PushValue(bool value) { return false; }
	virtual bool PushValue(int32_t value) { return false; }
	virtual bool PushValue(float value) { return false; }
	virtual bool PushValue(double value) { return false; }
	virtual bool PushValue(const char *value) { return false; }
	virtual bool PushValue(const std::string &value) { return false; }
	virtual bool PushPointer(void *pValue) { return false; }
	virtual bool PushNewTable(size_t nElements) { return false; }
	virtual bool PushNewArray(size_t nElements) { return false; }
	virtual bool PushKey(const char *pszKey) { return false; }
	virtual bool AddTableElem() { return false; }
	virtual bool SetArrayElem(size_t idx) { return false; }

	PyObject *GetReturnValue() { return m_pReturn; }

private:
private:
	PyObject *m_pPyArgs;
	PyObject *m_pReturn;
};

template <typename FunctionType, FunctionType pFunc>
PyObject *CallClientEngineFunctionFromScript(PyObject *pSelf, PyObject *pPyArgs) {
	ScriptArgManager sam(pPyArgs);
	CallScriptFunctionWithArgs<FunctionType>(&sam, pFunc);
	return sam.GetReturnValue();
}
} // namespace PythonScriptGlue
using namespace PythonScriptGlue;

#define REGISTER_CLIENT_ENGINE_FUNCTION(ScriptFunctionName, FunctionName) \
	{ ScriptFunctionName, &CallClientEngineFunctionFromScript<decltype(&IClientEngine::FunctionName), &IClientEngine::FunctionName>, METH_VARARGS, "Call " ScriptFunctionName },

#define REGISTER_MEMBER_FUNCTION(ScriptFunctionName, ClassName, FunctionName) \
	{ ScriptFunctionName, &CallClientEngineFunctionFromScript<decltype(&ClassName::FunctionName), &ClassName::FunctionName>, METH_VARARGS, "Call " ScriptFunctionName },
#define REGISTER_GLOBAL_FUNCTION(ScriptFunctionName, FunctionName) \
	{ ScriptFunctionName, &CallClientEngineFunctionFromScript<decltype(&FunctionName), &FunctionName>, METH_VARARGS, "Call " ScriptFunctionName },
#define REGISTER_SCRIPT_FUNCTION(ScriptFunctionName, FunctionName) // TODO
