///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "Event/Base/IEvent.h"
#include "../../Engine/Blades/BladeExports.h"

#include "PythonEventHandler.h"

#include "../../Engine/Blades/IBlade.h"
using namespace KEP;

IMPLEMENT_CREATE(PythonEventHandler)

BEGIN_BLADE_LIST
ADD_BLADE(PythonEventHandler)
END_BLADE_LIST
