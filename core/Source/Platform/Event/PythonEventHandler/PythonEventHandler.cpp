///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>

#include "IBlade.h"

#include "KEPException.h"
#include "IGame.h"
#include "Event/Base/IDispatcher.h"
#include "Event/Base/EventStrings.h"
#include "Event/Events/GetFunctionEvent.h"

#include <direct.h>
#include <jsEnumFiles.h>
#include <algorithm>

#include "PythonEventHandler.h"

#include "PythonScriptVM.h"
#include "PythonStateManager.h"

#include "Event/Base/ScriptGlue.h"

#include "LogHelper.h"
static LogInstance("Instance");

using namespace KEP;

#ifdef _DEBUG
#define DEBUG_PYTHON true
#else
#define DEBUG_PYTHON false
#endif

// used when registering
PyObject *g_currentModule = 0;

extern "C" {

SG_BEGIN_FN(PythonEventHandler::PythonRegisterEventHandler, ONE_RETURN_VALUE)

PyObject *pythonFn;
const char *eventName;
double temp;
double v1, v2, v3, v4;

SG_DECLARE_POP_OBJ(IDispatcher, dispatcher, gsDisp)
SG_DECLARE_POP_OBJ(PythonEventHandler, eh, gsEh)

SG_POPARGS("OOOddddsd", &gsDisp, &gsEh, &pythonFn, &v1, &v2, &v3, &v4, &eventName, &temp);
SG_CAST_TO_OBJECT(dispatcher, gsDisp);
SG_CAST_TO_DERIVED_OBJECT(eh, PythonEventHandler, IEventHandler, gsEh);

if (eh == 0) {
	// due to problem with parameter switch, this will do backward compatable check
	SG_CAST_TO_OBJECT(dispatcher, gsEh);
	SG_CAST_TO_DERIVED_OBJECT(eh, PythonEventHandler, IEventHandler, gsDisp);
}

EVENT_PRIORITY priority = std::max(EP_LOW, std::min(EP_HIGH, (EVENT_PRIORITY)temp));

EVENT_ID eventId = dispatcher->GetEventId(eventName ? eventName : "");

if (eventId == BAD_EVENT_ID) {
	PyErr_SetString(PyExc_TypeError, (loadStr(IDS_BAD_EVENT_NAME) + eventName).c_str());
	SG_RETURN_ERR;
}

// get the event handler fn
if (!PyCallable_Check(pythonFn)) {
	PyErr_SetString(PyExc_TypeError, loadStr(IDS_NOT_SCRIPT_METHOD).c_str());
	return NULL;
}
Py_XINCREF(pythonFn); /* Add a reference to new callback */

if (eh && eh->RegisterPythonHandler(dispatcher, pythonFn, PackVersion((BYTE)v1, (BYTE)v2, (BYTE)v3, (BYTE)v4), eventId, 0, false, 0, priority))
	Py_RETURN_TRUE;
else
	Py_RETURN_FALSE;

SG_END_FN

SG_BEGIN_FN(PythonEventHandler::PythonRegisterFilteredEventHandler, ONE_RETURN_VALUE)

PyObject *pythonFn;
const char *eventName;
double temp;
double v1, v2, v3, v4;
double filter, isFilterMask, filteredObjectId;

SG_DECLARE_POP_OBJ(IDispatcher, dispatcher, gsDisp)
SG_DECLARE_POP_OBJ(PythonEventHandler, eh, gsEh)

SG_POPARGS("OOOddddsdddd", &gsDisp, &gsEh, &pythonFn, &v1, &v2, &v3, &v4, &eventName, &filter, &isFilterMask, &filteredObjectId, &temp);
SG_CAST_TO_OBJECT(dispatcher, gsDisp);
SG_CAST_TO_DERIVED_OBJECT(eh, PythonEventHandler, IEventHandler, gsEh);

if (eh == 0) {
	// due to problem with parameter switch, this will do backward compatable check
	SG_CAST_TO_OBJECT(dispatcher, gsEh);
	SG_CAST_TO_DERIVED_OBJECT(eh, PythonEventHandler, IEventHandler, gsDisp);
}

EVENT_PRIORITY priority = std::max(EP_LOW, std::min(EP_HIGH, (EVENT_PRIORITY)temp));

EVENT_ID eventId = dispatcher->GetEventId(eventName ? eventName : "");

if (eventId == BAD_EVENT_ID) {
	PyErr_SetString(PyExc_TypeError, (loadStr(IDS_BAD_EVENT_NAME) + eventName).c_str());
	SG_RETURN_ERR;
}

// get the event handler fn
if (!PyCallable_Check(pythonFn)) {
	PyErr_SetString(PyExc_TypeError, loadStr(IDS_NOT_SCRIPT_METHOD).c_str());
	return NULL;
}
Py_XINCREF(pythonFn); /* Add a reference to new callback */

if (eh && eh->RegisterPythonHandler(dispatcher, pythonFn, PackVersion((BYTE)v1, (BYTE)v2, (BYTE)v3, (BYTE)v4), eventId, (ULONG)filter, isFilterMask != 0.0, (OBJECT_ID)filteredObjectId, priority))
	Py_RETURN_TRUE;
else
	Py_RETURN_FALSE;

SG_END_FN

SG_BEGIN_FN(PythonLogMessage, NO_RETURN_VALUE)

double logType;
const char *s;

SG_POP_OBJECT(PythonEventHandler, eh);
SG_POP(logType);
SG_POP(s);

if (eh) {
	eh->LogMessage((int)logType, s);
}

SG_RETURN;

SG_END_FN

} // ---- end extern "C"

SG_VM_REGISTER(PythonEventHandler::ParentVMRegister)

// we have a couple fns we need to intercept, but look like
SG_BEGIN_SG_REGISTER(ParentHandler)
SG_REGISTER(RegisterEventHandler, PythonEventHandler::PythonRegisterEventHandler)
SG_REGISTER(LogMessage, PythonLogMessage)
SG_REGISTER(RegisterFilteredEventHandler, PythonEventHandler::PythonRegisterFilteredEventHandler)
SG_END_REGISTER

SG_END_VM_REGISTER

bool PythonEventHandler::FunctionRegistration(IScriptVM &vm) {
	PythonScriptVM *myVm = dynamic_cast<PythonScriptVM *>(&vm);
	if (myVm == 0)
		return false; // could be trying Lua

	PSM::State pyState;
	;

	return TScriptingEventHandler<PyObject *, PyObject *>::FunctionRegistration(*myVm);
}

bool PythonEventHandler::UnRegisterScriptingEventHandlers(IDispatcher *dispatcher, IScriptVM *vm /* = 0 */) {
	// unregister any events this VM is listening for
	PyObject *module = 0;
	if (vm != 0) {
		PythonScriptVM *myVm = dynamic_cast<PythonScriptVM *>(vm);

		if (myVm)
			module = myVm->GetModule();

		if (myVm == 0 || module == 0)
			return false; // could be trying Lua
	}

	PSM::State pyState;
	;

	// TODO: not thread safe, ok?
	for (HandlerInfoVect::iterator i = m_handlers.begin(); i != m_handlers.end();) {
		if (vm == 0 || (*i)->vm == module) {
			// remove this handler from dispatcher
			char s[255];
			_snprintf_s(s, _countof(s), _TRUNCATE, "PythonEventHandler id=%d", (*i)->id);
			dispatcher->RemoveHandler(PythonEventHandler::EventHandlerFn, (ULONG)*i, s, (*i)->id);
			delete *i;
			i = m_handlers.erase(i);
		} else
			i++;
	}

	return true;
}

PythonEventHandler::PythonEventHandler() :
		TScriptingEventHandler<PyObject *, PyObject *>("PythonScript"),
		m_initialized(false),
		m_installedOk(false) {
	m_name = typeid(*this).name();

	PSM_Initialize(false);
	m_initialized = true;

	// test to see if traceback is available and assume Python is installed ok
	PSM::State pyState;
	;

	PyObject *main_module = PyImport_AddModule("__main__"); // borrowed ref

	PyObject *main_namespace = PyObject_GetAttrString(main_module, "__dict__"); // new ref

	const char *cmd = "import traceback\n";
	PyObject *ret = PyRun_String(cmd, Py_file_input, main_namespace, main_namespace); // new ref
	if (ret == 0) {
		logPythonErr(m_logger, loadStr(IDS_PYTHON_NOT_INSTALLED).c_str(), true);
	} else {
		m_installedOk = true;
		Py_DECREF(ret);
	}
	Py_DECREF(main_namespace);
}

// update the search path for modules
static void updatePythonPath(const char *BaseDir) {
	// put our path in the Python path, if not already there
	const char *existingPath = ::Py_GetPath();
	if (existingPath) {
		std::string ep(existingPath);

		std::vector<std::string> tokens;
		STLTokenize(ep, tokens, ";");
		std::string pythonlibPath;
		bool foundOurPath = false;
		bool foundSitePackages = false;

		for (auto i = tokens.begin(); i != tokens.end(); i++) {
			std::string temp(*i);
			STLToLower(temp);

			// Windows case insensitive
			if (temp == BaseDir)
				foundOurPath = true;
			else if (temp.find("\\lib\\site-packages") != std::string::npos)
				foundSitePackages = true;
			else if (temp.find("\\lib") == temp.length() - 4)
				pythonlibPath = *i;
		}

		// adjust path as needed
		std::string newPath(BaseDir);
		newPath += ";";
		if (!foundOurPath) {
			newPath += BaseDir;
			PathAddInPlace(newPath, "..\\Scripts;"); // for common ones
		}

		if (!foundSitePackages && !pythonlibPath.empty()) {
			newPath += pythonlibPath;
			PathAddInPlace(newPath, "site-packages;"); // for MySQLdb module
		}

		if (!foundOurPath || !foundSitePackages) {
			newPath += existingPath;
			::PySys_SetPath((char *)newPath.c_str());
		}

	} else
		::PySys_SetPath((char *)BaseDir);

	_wchdir(Utf8ToUtf16(BaseDir).c_str());
}

bool PythonEventHandler::callRegisterFnForEachScript(const char *fnName, IDispatcher *dispatcher, IEncodingFactory *factory, bool /*vmKeepIfFnExists*/) {
	// init static factory in this DLL
	IEvent::SetEncodingFactory(factory);

	PSM::State pyState;
	;

	jsVerifyReturn(dispatcher != 0 && factory != 0, false);

	// Find any Python files, and see if they are event handlers.
	// We load then with this mask since we strip off the extension and load by
	// module name anyway.  Python will compile any py file into a pyc file when imported.
	// The only thing we do it track the module names loaded to avoid running any global
	// code > 1 time.

	for (std::map<std::string, PyObject *>::iterator i = m_loadedModules.begin(); i != m_loadedModules.end(); i++) {
		if (i->second != 0) {
			Py_DECREF(i->second);
		}
	}
	m_loadedModules.clear();

	wchar_t curDir[_MAX_PATH + 1];
	_wgetcwd(curDir, _MAX_PATH);

	updatePythonPath(PathBase().c_str());

	PyObject *pName, *pModule;

	// one time for Python since loaded once (for now), null module
	PythonScriptVM pythonScriptVM;
	FunctionRegistration(pythonScriptVM);

	std::string mask(PathBase());
	PathAddInPlace(mask, "*.py?");

	jsEnumFiles ef(Utf8ToUtf16(mask).c_str());
	while (ef.next()) {
		// the name must not have the extension
		std::string filePath = Utf16ToUtf8(ef.currentFileName());
		size_t extPos = filePath.find_last_of(".py");
		jsAssert(extPos != filePath.npos);
		filePath.resize(extPos - 2); // mask is *.py?, so must have that on the end

		if (m_loadedModules.find(filePath) != m_loadedModules.end())
			continue;

		pName = PyString_FromString(filePath.c_str()); // new ref

		/* Error checking of pName left out */
		pModule = PyImport_Import(pName); // new ref
		m_loadedModules[filePath] = pModule;

		Py_DECREF(pName);

		if (pModule != NULL) {
			PythonScriptVM myMod(pModule);
			if (callRegisterFnForVM(fnName, &myMod, dispatcher, factory, filePath)) {
				continue;
			} else {
				m_loadedModules[filePath] = 0;
				Py_DECREF(pModule);
				return false;
			}
		} else {
			logPythonErr(m_logger, (loadStr(IDS_SCRIPT_LOAD_FAILED) + filePath).c_str(), true);
		}
	}

	_tchdir(curDir);

	return true;
}

bool PythonEventHandler::callRegisterFnForVM(const char *fnName, IScriptVM *vm, IDispatcher *dispatcher, IEncodingFactory *factory, std::string /*filePath*/) {
	PyObject *pModule = 0; // DRF - potentially uninitialized variable

	PythonScriptVM *pvm = dynamic_cast<PythonScriptVM *>(vm);
	if (pvm)
		pModule = pvm->GetModule();
	else
		assert(false); // DRF - pModule uninitialized
	if (pvm == 0 || pModule == 0)
		return false; // could be trying Lua

	PSM::State pyState;
	;

	PyObject *pValue = callFunctionForModule(fnName, pModule, dispatcher, factory);

	if (pValue) {
		Py_DECREF(pValue);
	}

	return true;
}

// retruns a NEW refrernce that must be Py_DECREF'd
PyObject *PythonEventHandler::callFunctionForModule(const char *fnName, PyObject *pModule, IDispatcher *dispatcher, IEncodingFactory * /*factory*/) {
	PyObject *pDict;
	PyObject *pArgs, *pValue;

	pDict = PyModule_GetDict(pModule); // borrowed ref

	// this code from samples, so very different from use of macros as show in ProcessEvent below

	/* pDict is a borrowed reference */
	// call InitializeHandler( dispatcher, eventhandler )
	PyObject *pFunc = PyDict_GetItemString(pDict, fnName); // borrowed ref

	/* pFun: Borrowed reference */
	if (pFunc && PyCallable_Check(pFunc)) {
		pArgs = PyTuple_New(3); // new ref

		pValue = PyCObject_FromVoidPtr(dispatcher, 0); // new ref
		if (!pValue) {
			Py_DECREF(pArgs);
			Py_DECREF(pModule);
			jsAssert(false);
			logPythonErr(m_logger, (loadStr(IDS_SCRIPT_CALL_FAILED) + fnName).c_str(), false);
			return 0;
		}
		/* pValue reference stolen here: */
		PyTuple_SetItem(pArgs, 0, pValue);

		pValue = PyCObject_FromVoidPtr(dynamic_cast<IEventHandler *>(this), 0); // new ref
		if (!pValue) {
			Py_DECREF(pArgs);
			Py_DECREF(pModule);
			jsAssert(false);
			logPythonErr(m_logger, (loadStr(IDS_SCRIPT_CALL_FAILED) + fnName).c_str(), false);
			return 0;
		}

		/* pValue reference stolen here: */
		PyTuple_SetItem(pArgs, 1, pValue);

		pValue = PyFloat_FromDouble(m_logger.getLogLevel()); // new ref
		if (!pValue) {
			Py_DECREF(pArgs);
			Py_DECREF(pModule);
			jsAssert(false);
			logPythonErr(m_logger, (loadStr(IDS_SCRIPT_CALL_FAILED) + fnName).c_str(), false);
			return 0;
		}

		/* pValue reference stolen here: */
		PyTuple_SetItem(pArgs, 2, pValue);

		g_currentModule = pModule;
		PyObject *retValue = PyObject_CallObject(pFunc, pArgs);
		g_currentModule = 0;

		Py_DECREF(pArgs);
		if (retValue != NULL) {
			return retValue;
		} else {
			logPythonErr(m_logger, (loadStr(IDS_HANDLER_REG_EXCEPTION) + fnName).c_str(), false);
			return 0;
		}

		/*
		 * pDict and pFunc are borrowed and must not be
		 * Py_DECREF-ed
		 */
	} else {
		// function not found, just continue
		LOG4CPLUS_DEBUG(m_logger, loadStr(IDS_ENTRY_PT_NOT_FOUND) + fnName);
	}

	return 0;
}

void PythonEventHandler::FreeAll() {
	if (m_dispatcher)
		UnRegisterScriptingEventHandlers(m_dispatcher);

	// this doesn't work.  Garbage collection done in Finalize,
	// and trying to decref causes finalize to crash

	// force garbarge collection

	//const char* cmd = _T("import gc\ngc.collect\n");
	for (HandlerInfoVect::iterator i = m_handlers.begin(); i != m_handlers.end(); i++) {
		if (*i) {
			//		//PyObject *dict = PyModule_GetDict ( i->second.second );
			//		//if ( PyRun_String( cmd, Py_file_input, dict, dict ) == 0 )
			//		//	logPythonErr( m_logger, loadStr( IDS_SCRIPT_LOAD_FAILED ) + "FreeAll", true );
			//
			Py_DECREF((*i)->fn);
			delete *i;
		}
	}
	m_handlers.clear();

	for (std::map<std::string, PyObject *>::iterator i = m_loadedModules.begin(); i != m_loadedModules.end(); i++) {
		if (i->second != 0) {
			Py_DECREF(i->second);
		}
	}
	m_loadedModules.clear();

	if (m_initialized && m_installedOk) {
		m_initialized = false; // in case called > 1 time

		PSM_Uninitialize();
	}
}

/*
 ===============================================================================
 ===============================================================================
 */
PythonEventHandler::~PythonEventHandler() {
	if (m_initialized && m_installedOk) {
		PSM_Uninitialize();
	}
}

/*
 ===============================================================================
 ===============================================================================
 */
EVENT_PROC_RC PythonEventHandler::ProcessEvent(IDispatcher * /*dispatcher*/, IEvent * /*e*/) {
	// never called any more
	jsAssert(false);
	return EVENT_PROC_RC::NOT_PROCESSED;
}

EVENT_PROC_RC PythonEventHandler::EventHandlerFn(ULONG lparam, IDispatcher *dispatcher, IEvent *e) {
	jsVerifyReturn(lparam != 0 && dispatcher != 0 && e != 0, EVENT_PROC_RC::NOT_PROCESSED);

	HandlerInfo *info = (HandlerInfo *)lparam;

	PythonEventHandler *me = dynamic_cast<PythonEventHandler *>(info->parent);
	if (!me)
		return EVENT_PROC_RC::NOT_PROCESSED;

	PSM::State pyState;

	// prep args
	//IDispatcher *notused = 0;
	SG_CAST_TO_PUSH_OBJ(dispatcher, sgDispatcherObj);
	SG_CAST_TO_PUSH_OBJ(e, sgEObj);

	//
	// function eventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	//
	PyObject *__retArgs = 0;

	// call the python method
	SG_PUSHARGS("OdOddd", sgDispatcherObj, (double)(long)e->From(), sgEObj, (double)e->EventId(), (double)(long)e->Filter(), (double)(long)e->ObjectId());

	PyObject *result = 0;
	try // DRF - catch() unreachable
	{
		result = PyEval_CallObject(info->fn, __retArgs); // new ref
	} catch (KEPException *e) {
		LogError(loadStr(IDS_HANDLER_EXCEPTION) << e->m_msg << numToString(e->m_err, " 0x%x ") << e->m_file << ":" << e->m_line);
		e->Delete();
	}
	Py_DECREF(__retArgs);

	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;

	// process result then ....
	if (result != NULL) {
		long lRet = -1;

		if (PyInt_Check(result))
			lRet = PyInt_AsLong(result);
		else if (PyLong_Check(result))
			lRet = PyLong_AsLong(result);
		else if (PyFloat_Check(result))
			lRet = (long)PyFloat_AsDouble(result);

		if (lRet >= static_cast<int>(EVENT_PROC_RC::OK) && lRet <= static_cast<int>(EVENT_PROC_RC::CONSUMED))
			ret = (EVENT_PROC_RC)lRet;
		else {
			char *s = "Unknown";
			PyObject *n = PyObject_Str(info->fn); // new ref
			if (n)
				s = PyString_AS_STRING(n);
			LogWarn(loadStr(IDS_UNEXPECTED_SCRIPT_RETURN) << s << " " << static_cast<int>(ret));
			if (n)
				Py_DECREF(n);
		}
		Py_DECREF(result);
	} else {
		char *s = "Unknown";
		PyObject *n = PyObject_Str(info->fn); // new ref
		if (n)
			s = PyString_AS_STRING(n);

		logPythonErr(m_logger, (loadStr(IDS_SCRIPT_CALL_FAILED) + s).c_str(), false);

		if (n)
			Py_DECREF(n);
	}

	return ret;
}

/*
 ===============================================================================
 ===============================================================================
 */
bool PythonEventHandler::RegisterPythonHandler(IDispatcher *dispatcher,
	PyObject *PythonFn,
	ULONG eventVersion,
	EVENT_ID eventId,
	ULONG filter, bool filterIsMask,
	OBJECT_ID filteredObjectId,
	EVENT_PRIORITY priority) {
	jsVerifyReturn(dispatcher != 0 && PythonFn != 0, false);

	char *s = "Unknown Python Method";
	PyObject *n = PyObject_Str(PythonFn); // new ref
	if (n)
		s = PyString_AS_STRING(n);

	// create a new info object
	HandlerInfo *info = new HandlerInfo(this, eventId, PythonFn, g_currentModule);
	if (!dispatcher->RegisterFilteredHandler(EventHandlerFn, (ULONG)info, s, eventVersion, eventId, filter, filterIsMask,
			filteredObjectId, priority)) {
		delete info;
		return false;
	}

	// add the fn name to our list
	m_handlers.push_back(info);

	std::string s2;
	LOG4CPLUS_DEBUG(m_logger, loadStr(IDS_REGISTERING_SCRIPT_HANDLER) << "Python: " << s << " (" << eventId << ") v" << VersionToString(eventVersion, s2) << " p" << (int)priority);
	if (n)
		Py_DECREF(n);

	return true;
}

EVENT_PROC_RC PythonEventHandler::GetFunctionHandler(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	PythonEventHandler *me = (PythonEventHandler *)lparam;

	GetFunctionEvent *gfe = dynamic_cast<GetFunctionEvent *>(e);

	if (gfe) {
		std::string name, vmName;
		ULONG vm;

		(*gfe->InBuffer()) >> name >> vmName >> vm;
		if (vmName == SG_VM_NAME) {
			PythonScriptVM *pVm = (PythonScriptVM *)vm;
			return me->GetFunctions(name.c_str(), pVm);
		}
	}

	return EVENT_PROC_RC::NOT_PROCESSED;
}

bool PythonEventHandler::RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *factory) {
	if (!m_installedOk)
		return false;

	m_dispatcher = dispatcher;

	return TScriptingEventHandler<PyObject *, PyObject *>::RegisterEvents(dispatcher, factory);
}

bool PythonEventHandler::RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory) {
	if (!m_installedOk)
		return false;

	m_dispatcher = dispatcher;

	RegisterGetFunctionHandlers(dispatcher, PythonEventHandler::GetFunctionHandler, "Python", (ULONG)this);

	return TScriptingEventHandler<PyObject *, PyObject *>::RegisterEventHandlers(dispatcher, factory);
}

// Return whether first element is greater than the second
bool moduleCountCompare(std::pair<std::string, int> elem1, std::pair<std::string, int> elem2) {
	return elem1.second > elem2.second;
}

bool PythonEventHandler::ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory) {
	if (!m_installedOk)
		return false;

	m_dispatcher = dispatcher;

	wchar_t curDir[_MAX_PATH + 1];
	_wgetcwd(curDir, _MAX_PATH);

	PSM::State pyState;
	;

	updatePythonPath(PathBase().c_str());

	// unregister all existing event handlers, which removed them from handlers, so remove from end back
	int count = m_handlers.size();

	// TODO: not thread safe, ok?
	for (int k = count - 1; k >= 0; k--) {
		// remove this handler from dispatcher
		char s[255];
		_snprintf_s(s, _countof(s), _TRUNCATE, "PythonEventHandler id=%d", m_handlers[k]->id);
		dispatcher->RemoveHandler(PythonEventHandler::EventHandlerFn, (ULONG)m_handlers[k], s, m_handlers[k]->id);
		delete m_handlers[k];
	}
	m_handlers.clear();

	std::vector<std::pair<std::string, int>> moduleCounts;

	for (std::map<std::string, PyObject *>::iterator i = m_loadedModules.begin(); i != m_loadedModules.end(); i++) {
		if (i->second != 0) {
			moduleCounts.push_back(std::pair<std::string, int>(i->first, 1));
			PyObject *dependents = callFunctionForModule("ReinitalizeKEPEvents", i->second, dispatcher, factory);
			if (dependents && PyString_Check(dependents)) {
				char *deps = PyString_AsString(dependents);

				std::vector<std::string> tokens;
				STLTokenize(deps, tokens, "\t");
				for (auto itrToken = tokens.begin(); itrToken != tokens.end(); itrToken++) {
					std::vector<std::pair<std::string, int>>::iterator j;
					for (j = moduleCounts.begin(); j != moduleCounts.end(); j++) {
						if (j->first == *itrToken) {
							j->second++;
							break;
						}
					}
					if (j == moduleCounts.end())
						moduleCounts.push_back(std::pair<std::string, int>(*itrToken, 1));
				}
			}
			if (dependents)
				Py_DECREF(dependents);
		}
	}

	std::sort(moduleCounts.begin(), moduleCounts.end(), moduleCountCompare);

	// now reload them, in order of most to least refrence counts
	for (auto j = moduleCounts.begin(); j != moduleCounts.end(); j++) {
		jsOutputToDebugger("%s %d\r\n", j->first.c_str(), j->second);

		/* Error checking of pName left out */
		PyObject *pName, *pModule;
		pName = PyString_FromString(j->first.c_str()); // new ref
		pModule = PyImport_Import(pName); // new ref
		Py_DECREF(pName);

		if (pModule) {
			LOG4CPLUS_DEBUG(m_logger, "Reloading module " << j->first);
			PyObject *newModule = PyImport_ReloadModule(pModule); // new ref
			Py_DECREF(pModule);
			if (newModule == 0) {
				logPythonErr(m_logger, (loadStr(IDS_SCRIPT_LOAD_FAILED) + j->first).c_str(), true);
			} else {
				Py_DECREF(newModule);
			}
		} else
			logPythonErr(m_logger, (loadStr(IDS_SCRIPT_LOAD_FAILED) + j->first).c_str(), true);
	}

	RegisterGetFunctionHandlers(dispatcher, PythonEventHandler::GetFunctionHandler, "Python", (ULONG)this);

	_wchdir(curDir);

	// call this to re-register the handlers
	return TScriptingEventHandler<PyObject *, PyObject *>::ReRegisterEventHandlers(dispatcher, factory);
}

extern "C" BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD ul_reason_for_call,
	LPVOID /*lpReserved*/
) {
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
		StringResource = (HINSTANCE)hModule;
	return TRUE;
}
