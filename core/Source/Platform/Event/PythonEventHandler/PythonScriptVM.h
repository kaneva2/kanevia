/******************************************************************************
 PythonScriptVM.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

extern "C" {
// avoid benign warning due to Python/log4cplus conflict
#undef HAVE_FTIME
#ifdef _DEBUG
#undef _DEBUG
#include "Tools/Python/include/Python.h"
#define _DEBUG
#else
#include "Tools/Python/include/Python.h"
#endif
}

#include "Event/Base/IScriptVM.h"

namespace KEP {

/***********************************************************
CLASS

	PythonScriptVM 
	
	class for use with RTTI to pass around VMs

DESCRIPTION

***********************************************************/
class PythonScriptVM : public IScriptVM {
public:
	// use "current" module
	PythonScriptVM(PyObject *vm = 0) :
			IScriptVM(vm) {}
	PyObject *GetModule() {
		PyObject *ret = (PyObject *)m_vm;
		jsVerifyReturn(PyModule_Check(ret), 0);
		return ret;
	}
};

} // namespace KEP

