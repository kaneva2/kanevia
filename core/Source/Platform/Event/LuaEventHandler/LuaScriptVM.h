/******************************************************************************
 LuaScriptVM.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Event/Base/IScriptVM.h"

struct lua_State;

namespace KEP {

class LuaScriptVM : public IScriptVM {
public:
	LuaScriptVM(lua_State *vm = 0) :
			IScriptVM(vm) {}
	lua_State *GetState() {
		lua_State *ret = (lua_State *)m_vm;
		return ret;
	}
	operator lua_State *() { return GetState(); }
};

} // namespace KEP