///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/include/kepcommon.h"
#include "Core/Util/monitored.h"
#include "Event/Base/ScriptingEventHandler.h"

struct lua_State;

namespace KEP {

class LuaEventHandler : public TScriptingEventHandler<std::string, lua_State *> {
public:
	LuaEventHandler();
	~LuaEventHandler();

	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }
	// IEventHandler override
	virtual EVENT_PROC_RC ProcessEvent(IDispatcher *disp, IEvent *e);

	virtual void FreeAll();

	// overrides to register our own handlers, too
	bool RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);
	bool ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory);

	// DRF - added
	bool PostInitialization(IDispatcher *dispatcher, IEncodingFactory *factory);

	std::string GetVMFileName(lua_State *) const; // drf - actually returns full script path
	static int LuaGetVMFileName(lua_State *); // for script glue

private:
	bool RegisterLuaHandler(IDispatcher *disp, lua_State *state, const char *luaFnName,
		ULONG eventVersion, EVENT_ID eventId,
		ULONG filter, bool filterIsMask,
		OBJECT_ID filteredObjectId,
		EVENT_PRIORITY priority);

	bool RemoveLuaHandler(IDispatcher *disp, lua_State *state, const char *luaFnName, EVENT_ID eventId);

	// ScriptingEventHandler override
	bool callRegisterFnForEachScript(const char *fnName, IDispatcher *dispatcher, IEncodingFactory *factory, bool vmKeepIfFnExists);
	bool callRegisterFnForVM(const char *fnName, IScriptVM *vm, IDispatcher *dispatcher, IEncodingFactory *factory, std::string filePath);

	bool FunctionRegistration(IScriptVM &vm);
	bool UnRegisterScriptingEventHandlers(IDispatcher *dispatcher, IScriptVM *vm = 0);

	typedef std::map<std::string, lua_State *> VMMap; // drf - <scriptFilePath, luaState>

	VMMap m_vmMap; // track all the ones we loaded so we can close them

	static int LuaRegisterEventHandler(lua_State *);
	static int LuaRegisterFilteredEventHandler(lua_State *);
	static int LuaRemoveEventHandler(lua_State *);

	// callback to register an event
	static int LuaRegisterEvent(lua_State *);
	virtual void ParentVMRegister(IScriptVM *);

	static EVENT_PROC_RC GetFunctionHandler(ULONG lparam, IDispatcher *disp, IEvent *e);
	static EVENT_PROC_RC EventHandlerFn(ULONG lparam, IDispatcher *dispatcher, IEvent *e);
};

} // namespace KEP
