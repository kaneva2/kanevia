#include <js.h>
#include <direct.h>
#include <jsEnumFiles.h>

#include "IBlade.h"

#include "KEPException.h"
#include "IGame.h"

#include "Event/Base/IDispatcher.h"
#include "Event/Base/EventStrings.h"
#include "Event/Base/ScriptGlue.h"

#include "Event/Events/GetFunctionEvent.h"

#include "LuaHelper.h"
#include "LuaScriptVM.h"
#include "LuaEventHandler.h"
#include "LuaErrorHandler.h"
#include "KEPFileNames.h"

#include "common\KEPUtil\Helpers.h"

#include "GlobalEventRegistry.h"
#include <Common/include/PreciseTime.h>

#include "LogHelper.h"
static LogInstance("Instance");

#define DRF_ERROR_DATA_LUA_CALL 0
#if (DRF_ERROR_DATA_LUA_CALL == 1)
#include "Common\KEPUtil\CrashReporter.h"
#endif

using namespace KEP;

#define BAD_ERROR_MESSAGE "(no error message available)"

extern "C" {

SG_BEGIN_FN(LuaEventHandler::LuaRegisterEventHandler, ONE_RETURN_VALUE)
const char *luaFnName = "";
const char *eventName = "";
EVENT_ID eventId = 0;
double temp;
double v1, v2, v3, v4;

SG_DECLARE_POP_OBJ(IDispatcher, dispatcher, gsDisp);
SG_DECLARE_POP_OBJ(LuaEventHandler, eh, gsEh);

SG_POPARGS("OOsddddsd", &gsDisp, &gsEh, &luaFnName, &v1, &v2, &v3, &v4, &eventName, &temp);
SG_CAST_TO_OBJECT(dispatcher, gsDisp);
SG_CAST_TO_OBJECT(eh, gsEh);

if (!dispatcher) {
	SG_RETURN_ERR_STR("NULL_DISPATCHER");
}

eventId = dispatcher->GetEventId(eventName ? eventName : "");
EVENT_PRIORITY priority = std::max(EP_LOW, std::min(EP_HIGH, (EVENT_PRIORITY)temp));
if (eventId == BAD_EVENT_ID) {
	//	SG_RETURN_ERR_STR((loadStr(IDS_BAD_EVENT_NAME) + (eventName ? eventName : "")));
	LogError("EVENT_NOT_REGISTERED - '" << (eventName ? eventName : "") << "'");
	return 0;
} else if (eh) {
	if (eh->RegisterLuaHandler(dispatcher, state, luaFnName, PackVersion((BYTE)v1, (BYTE)v2, (BYTE)v3, (BYTE)v4), eventId, 0, false, 0, priority))
		return 0;
	else
		SG_RETURN_ERR_STR("LUA_FAILED_REG");
} else
	SG_RETURN_ERR_STR("NULL_EVENT");

SG_END_FN

SG_BEGIN_FN(LuaEventHandler::LuaRegisterFilteredEventHandler, ONE_RETURN_VALUE)

const char *luaFnName = "";
const char *eventName = "";
EVENT_ID eventId = 0;
double temp;
double v1, v2, v3, v4;
double filter, filterIsMask, filteredObjectId;

SG_DECLARE_POP_OBJ(IDispatcher, dispatcher, gsDisp);
SG_DECLARE_POP_OBJ(LuaEventHandler, eh, gsEh);

SG_POPARGS("OOsddddsdddd", &gsDisp, &gsEh, &luaFnName, &v1, &v2, &v3, &v4, &eventName, &filter, &filterIsMask, &filteredObjectId, &temp);
SG_CAST_TO_OBJECT(dispatcher, gsDisp);
SG_CAST_TO_OBJECT(eh, gsEh);

if (!dispatcher) {
	SG_RETURN_ERR_STR("NULL_DISPATCHER");
}

eventId = dispatcher->GetEventId(eventName ? eventName : "");
EVENT_PRIORITY priority = std::max(EP_LOW, std::min(EP_HIGH, (EVENT_PRIORITY)temp));
if (eventId == BAD_EVENT_ID) {
	//	SG_RETURN_ERR_STR((loadStr(IDS_BAD_EVENT_NAME) + (eventName ? eventName : "")));
	LogError("EVENT_NOT_REGISTERED - '" << (eventName ? eventName : "") << "'");
	return 0;
} else if (eh) {
	if (eh->RegisterLuaHandler(dispatcher, state, luaFnName, PackVersion((BYTE)v1, (BYTE)v2, (BYTE)v3, (BYTE)v4), eventId, (ULONG)filter, filterIsMask != 0.0, (OBJECT_ID)filteredObjectId, priority))
		return 0;
	else
		SG_RETURN_ERR_STR("LUA_FAILED_REG");
} else
	SG_RETURN_ERR_STR("NULL_EVENT");

SG_END_FN

SG_BEGIN_FN(LuaEventHandler::LuaRemoveEventHandler, ONE_RETURN_VALUE)

const char *luaFnName = "";
const char *eventName = "";
EVENT_ID eventId = 0;

SG_DECLARE_POP_OBJ(IDispatcher, dispatcher, gsDisp);
SG_DECLARE_POP_OBJ(LuaEventHandler, eh, gsEh);

SG_POPARGS("OOss", &gsDisp, &gsEh, &luaFnName, &eventName);
SG_CAST_TO_OBJECT(dispatcher, gsDisp);
SG_CAST_TO_OBJECT(eh, gsEh);

if (!dispatcher) {
	SG_RETURN_ERR_STR("NULL_DISPATCHER");
}

eventId = dispatcher->GetEventId(eventName ? eventName : "");
if (eventId == BAD_EVENT_ID) {
	//	SG_RETURN_ERR_STR((loadStr(IDS_BAD_EVENT_NAME) + (eventName ? eventName : "")));
	LogError("EVENT_NOT_REGISTERED - '" << (eventName ? eventName : "") << "'");
	return 0;
} else if (eh) {
	if (eh->RemoveLuaHandler(dispatcher, state, luaFnName, eventId))
		return 0;
	else
		SG_RETURN_ERR_STR("LUA_FAILED_REG");
} else
	SG_RETURN_ERR_STR("NULL_EVENT");
SG_END_FN

SG_BEGIN_FN(LuaLogMessage, NO_RETURN_VALUE)
double logType;
const char *s;
SG_POP_OBJECT(LuaEventHandler, eh);
SG_POP(logType);
SG_POP(s);
if (eh && s)
	eh->LogMessage((int)logType, s);
SG_RETURN;
SG_END_FN

SG_BEGIN_FN(LuaEventHandler::LuaGetVMFileName, ONE_RETURN_VALUE)
SG_DECLARE_POP_OBJ(LuaEventHandler, eh, gsEh);
SG_POPARGS("O", &gsEh);
SG_CAST_TO_OBJECT(eh, gsEh);
if (eh) {
	std::string filePath = eh->GetVMFileName(state);
	SG_PUSH(filePath.c_str());
	SG_RETURN;
} else
	SG_RETURN_ERR_STR("NULL_EVENT");
SG_END_FN

} // end extern "C"

SG_VM_REGISTER(LuaEventHandler::ParentVMRegister)

// we have a couple fns we need to intercept, but look like
SG_BEGIN_SG_REGISTER(ParentHandler)
SG_REGISTER(RegisterEventHandler, LuaEventHandler::LuaRegisterEventHandler)
SG_REGISTER(LogMessage, LuaLogMessage)
SG_REGISTER(RegisterFilteredEventHandler, LuaEventHandler::LuaRegisterFilteredEventHandler)
SG_REGISTER(RemoveEventHandler, LuaEventHandler::LuaRemoveEventHandler)
SG_REGISTER(GetVMFileName, LuaEventHandler::LuaGetVMFileName)
SG_END_REGISTER

SG_END_VM_REGISTER

void GetVMFilePathAndName(LuaEventHandler *eh, lua_State *state, std::string &filePath, std::string &fileName) {
	if (filePath.empty() && eh)
		filePath = eh->GetVMFileName(state);
	if (!filePath.empty())
		fileName = StrStripFilePath(filePath);
	if (fileName.empty()) {
		char str[32];
		_snprintf_s(str, sizeof(str), "<%p>", state);
		fileName.assign(str);
	}
}

bool LuaEventHandler::FunctionRegistration(IScriptVM &vm) {
	lua_State *state = 0;
	LuaScriptVM *myVm = dynamic_cast<LuaScriptVM *>(&vm);
	if (myVm)
		state = myVm->GetState();
	if (myVm == 0 || state == 0)
		return false; // could be trying Python

	return TScriptingEventHandler<std::string, lua_State *>::FunctionRegistration(*myVm);
}

bool LuaEventHandler::UnRegisterScriptingEventHandlers(IDispatcher *dispatcher, IScriptVM *vm /* = 0 */) {
	// unregister any events this VM is listening for
	lua_State *state = 0;
	if (vm != 0) {
		LuaScriptVM *myVm = dynamic_cast<LuaScriptVM *>(vm);
		if (myVm)
			state = myVm->GetState();

		if (myVm == 0 || state == 0)
			return false; // could be trying Python
	}

	// TODO: not thread safe, ok?
	for (VMMap::iterator j = m_vmMap.begin(); j != m_vmMap.end();) {
		if (vm == 0 || j->second == state) {
			LUA_GARBAGE_COLLECT(j->second);
			lua_close(j->second);
			j = m_vmMap.erase(j);
		} else
			j++;
	}

	for (HandlerInfoVect::iterator i = m_handlers.begin(); i != m_handlers.end();) {
		if (vm == 0 || (*i)->vm == state) {
			// remove this handler from dispatcher
			char s[255];
			_snprintf_s(s, _countof(s), _TRUNCATE, "LuaEventHandler id=%d", (*i)->id);
			dispatcher->RemoveHandler(LuaEventHandler::EventHandlerFn, (ULONG)*i, s, (*i)->id);

			delete *i;
			i = m_handlers.erase(i);
		} else
			i++;
	}

	return true;
}

LuaEventHandler::LuaEventHandler() :
		TScriptingEventHandler<std::string, lua_State *>("LuaScript") {
	m_name = typeid(*this).name();
	LogInfo("Using " << LuaVersion());
}

// top-level script execution
static int my_dofile(lua_State *L, int nresults, const char *name, lua_CFunction errorHandler = NULL) {
	if (errorHandler)
		lua_pushcfunction(L, errorHandler);

	// load the script
	int rc = LuaLoadFileProtected(L, name, false); // Dynamically generated scripts are not allowed under ClientScripts folder
	if (rc)
		return rc; // there was a compile-time error. it's on the top of the stack

	// there were no compilation errors. run the script
	// if there is a runtime error it is on the top of the stack
	return lua_pcall(L, 0, nresults, errorHandler ? -2 : 0);
}

// Custom in-script dofile() call
static int lua_dofile(lua_State *L) {
	// luaB_dofile() from Lua 5.1.5
	const char *fname = luaL_optstring(L, 1, NULL);

	// The dofile path is assumed to be relative to ClientScripts folder
	std::string sFileName(fname ? fname : "");
	if (!IsAbsolutePath(sFileName)) {
		sFileName = PathAdd(GAMEFILES, PathAdd(CLIENTSCRIPTSDIR, sFileName));
	}

	int n = lua_gettop(L);
	if (LuaLoadFileProtected(L, sFileName.c_str(), true) != 0)
		lua_error(L);
	lua_call(L, 0, LUA_MULTRET);
	return lua_gettop(L) - n;
}

bool LuaEventHandler::callRegisterFnForEachScript(const char *fnName, IDispatcher *dispatcher, IEncodingFactory *factory, bool vmKeepIfFnExists) {
	jsVerifyReturn(dispatcher != 0 && factory != 0, false);

	// init static factory in this DLL
	IEvent::SetEncodingFactory(factory);

	LuaLoadScriptInfo(PathBase());
	LuaLoadScriptInfo(PathAdd(PathAdd(PathBase(), "..\\"), SCRIPTDIR));
	LuaLoadScriptInfo(PathAdd(PathAdd(PathBase(), "..\\"), MENUSCRIPTSDIR));
	LuaLoadScriptInfo(PathAdd(PathAdd(PathBase(), "..\\"), SCRIPTDATADIR));

	// find any lua files, and see if they are event handlers
	std::string filePath;
	std::string mask(PathBase());
	PathAddInPlace(mask, "*.lua");
	jsEnumFiles ef(Utf8ToUtf16(mask).c_str());
	while (ef.next()) {
		lua_State *state = 0;
		bool newVM = false;

		auto fileName = Utf16ToUtf8(ef.currentFileName());
		if (STLCompareIgnoreCase(fileName, SCRIPT_INDEX_FILE) == 0) {
			// Skip index file (not a Lua script)
			continue;
		}

		// aleady created a VM for this file?
		filePath = Utf16ToUtf8(ef.currentPath());
		VMMap::const_iterator f = m_vmMap.find(filePath);
		if (f == m_vmMap.end()) {
			state = LUA_OPEN();
			newVM = true;

			// Open Lua Libraries (unrestricted)
			LUA_OPENLIBS(state, false);

			// Define global variable __SCRIPT__ for current script file name
			lua_pushstring(state, Utf16ToUtf8(ef.currentFileName()).c_str());
			lua_setglobal(state, "__SCRIPT__");

			// Define global variable __LUA_COMPAT_LEVEL__
			lua_pushnumber(state, CURRENT_SCRIPT_LUA_COMPAT_LEVEL);
			lua_setglobal(state, "__LUA_COMPAT_LEVEL__");

			// Define global variable __LUA_VERSION__
			lua_pushnumber(state, LUA_VERSION_NUM);
			lua_setglobal(state, "__LUA_VERSION__");

			lua_pushnumber(state, Helpers_GetDevMode());
			lua_setglobal(state, "__DEV_MODE__");

			// replace the base library's dofile with our version
			lua_pushstring(state, PathBase().c_str());
			lua_pushcclosure(state, lua_dofile, 1);
			lua_setglobal(state, "dofile");

			// register our functions one time into the state
			LuaScriptVM luaScriptVM(state);
			FunctionRegistration(luaScriptVM);

			// Lua Do File
			int rc = my_dofile(state, 1, filePath.c_str(), LuaErrorHandler);
			if (rc != 0) {
				const char *str = lua_tostring(state, -1);
				const char *msg = str ? str : BAD_ERROR_MESSAGE;
				LogError("FAILED Loading " << filePath << "\n"
										   << msg);
				lua_close(state);
				continue;
			}
		} else {
			state = f->second;
		}

		// Call VM Named Function
		LuaScriptVM myState(state);
		filePath = Utf16ToUtf8(ef.currentPath());
		if (callRegisterFnForVM(fnName, &myState, dispatcher, factory, filePath)) {
			// Keep This New VM?
			if (newVM && vmKeepIfFnExists)
				m_vmMap[filePath] = state;
		} else if (newVM) {
			lua_close(state);
		}
	}

	return true;
}

bool LuaEventHandler::callRegisterFnForVM(const char *fnName, IScriptVM *vm, IDispatcher *dispatcher, IEncodingFactory * /*factory*/, std::string filePath) {
	LuaScriptVM *myVM = dynamic_cast<LuaScriptVM *>(vm);
	lua_State *state = 0;
	if (myVM)
		state = myVM->GetState();

	if (myVM == 0 || state == 0)
		return false; // could be trying Python

	// Get Script File Path And Name (for debug only)
	std::string fileName;
	GetVMFilePathAndName(this, state, filePath, fileName);
	LogTrace(fileName << "::" << fnName << "()");

	// push the error handler function
	lua_pushcfunction(state, LuaErrorHandler);

	lua_getglobal(state, fnName);
	if (!lua_isfunction(state, -1)) {
		// script not an event handler
		lua_pop(state, 2); // pop the error handler and whatever getglobal added (obviously not the function we wanted)
		return false;
	}

	// Push 1st Parameter - dispatcher
	lua_pushlightuserdata(state, dispatcher);

	// Push 2nd Parameter - parentHandler
	lua_pushlightuserdata(state, this);

	// Push 3rd Parameter - debugLevel (script type: 0.0=menu/server, 1.0=client)
	bool isClientScript = STLContainsIgnoreCase(filePath, "\\clientscript");
	double debugLevel = isClientScript ? 1.0 : 0.0;
	lua_pushnumber(state, debugLevel);

	// Push 4th Parameter - scriptName (xxx.lua)
	lua_pushstring(state, fileName.c_str());

	// Call Lua Script Function
	const int luaArgsIn = 4; // 4 Parameters In (dispatcher, parentHandler, debugLevel, scriptName)
	const int luaArgsOut = 1; // 1 Return Value Out (0=ok, !0=error)
	const int luaArgErrorHandler = -(luaArgsIn + 2);
	int ret = lua_pcall(state, luaArgsIn, luaArgsOut, luaArgErrorHandler);
	if (ret != 0) {
		const char *str = lua_tostring(state, -1);
		const char *msg = str ? str : BAD_ERROR_MESSAGE;
		lua_pop(state, 2); // the error handler and the error message
		LogError("Error in " << fileName << "::" << fnName << "()\n"
							 << msg);
	} else {
		double luaRet = lua_tonumber(state, -1);
		lua_pop(state, 2); // the return value (or null) and error handler
		if (luaRet == 0.0) {
			return true;
		} else {
			// TODO: remove added ones in the event one added, one failed??
			// probably already logged
		}
	}
	return false;
}

void LuaEventHandler::FreeAll() {
	if (m_dispatcher != 0)
		UnRegisterScriptingEventHandlers(m_dispatcher);

	for (VMMap::iterator i = m_vmMap.begin(); i != m_vmMap.end(); i++) {
		LUA_GARBAGE_COLLECT(i->second);
		lua_close(i->second);
	}
	m_vmMap.clear();

	// clear out all the names
	for (const auto &pHandler : m_handlers) {
		if (pHandler)
			delete pHandler;
	}

	TScriptingEventHandler<std::string, lua_State *>::FreeAll();
}

LuaEventHandler::~LuaEventHandler() {}

EVENT_PROC_RC LuaEventHandler::ProcessEvent(IDispatcher * /*dispatcher*/, IEvent * /*e*/) {
	// never called any more
	jsAssert(false);
	return EVENT_PROC_RC::NOT_PROCESSED;
}

EVENT_PROC_RC LuaEventHandler::EventHandlerFn(ULONG lparam, IDispatcher *dispatcher, IEvent *e) {
	jsVerifyReturn(lparam != 0 && dispatcher != 0 && e != 0, EVENT_PROC_RC::NOT_PROCESSED);

	HandlerInfo *info = (HandlerInfo *)lparam;

	if (info == NULL || info->parent == NULL)
		return EVENT_PROC_RC::NOT_PROCESSED;

	lua_State *state = info->vm;
	if (!state)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Get Script File Path And Name (for debug only)
	std::string filePath;
	std::string fileName;
	LuaEventHandler *eh = dynamic_cast<LuaEventHandler *>(info->parent);
	GetVMFilePathAndName(eh, state, filePath, fileName);

	// Get Script Full Name ('info' could be invalid after lua_pcall)
	std::string funcName = info->fn.empty() ? "<null>" : info->fn;
	std::string fullName = fileName + "::" + funcName + "()";

	// Valid Function ?
	if (info->fn.empty()) {
		_LogError("Null Function");
		_LogError("... " << fullName);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// Push error handler function
	lua_pushcfunction(state, LuaErrorHandler);

	// see if they actually have defined the function they registered to handle the event
	lua_getglobal(state, funcName.c_str());
	if (!lua_isfunction(state, -1)) {
		lua_pop(state, 1); // remove whatever getglobal pushed
		lua_pop(state, 1); // remove error handler function
		_LogError("Undefined Function");
		_LogError("... " << fullName);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	auto eFrom = (long)e->From();
	auto eEventId = (long)e->EventId();
	auto eFilter = (long)e->Filter();
	auto eObjId = (long)e->ObjectId();

	// call handle event (function lua::eventHandler(dispatcher, fromNetid, event, eventid, filter, objectid ))
	int __retArgs = 0;
	SG_PUSHARGS("OdOddd",
		dispatcher,
		(double)eFrom, // fromNetId
		e, // event
		(double)eEventId,
		(double)eFilter,
		(double)eObjId);

#if (DRF_ERROR_DATA_LUA_CALL == 1)
	// DRF - Set 'luaCall' Crash Report Error Data
	CrashReporter::SetErrorData("luaCall", fullName);
#endif

	// ED-7834 - Warn On Stalling
	Timer timer;

	// Perform Lua Script Function Call
	bool ok = false;
	try {
		// nresults == 1, so lua_pcall will push one value (which might be null) onto the stack
		ok = lua_pcall(state, 6, 1, -8) == 0;
	} catch (...) {
		// Catch Everything As Failure
	}

	// ED-7834 - Warn On Stalling
	auto timeMs = timer.ElapsedMs();
	if (timeMs > 100) {
		LogWarn("SLOW(" << FMT_TIME << timeMs << "ms) - " << GlobalEventName(eEventId) << "->" << fullName << " filter=" << eFilter << " objId=" << eObjId);
	}

#if (DRF_ERROR_DATA_LUA_CALL == 1)
	// DRF - Clear 'luaCall' Crash Report Error Data
	CrashReporter::SetErrorData("luaCall", "");
#endif

	// Lua Error ?
	if (!ok) {
		const char *str = lua_tostring(state, -1);
		const char *msg = str ? str : BAD_ERROR_MESSAGE;
		lua_pop(state, 2); // remove the error message and error handler
		LogError("Error in " << fullName << " ...\n"
							 << msg);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// Get Lua Return Code
	int luaRet = (int)lua_tonumber(state, -1);
	lua_pop(state, 2); // remove the return code (or null) and the error handler

	if (luaRet < static_cast<int>(EVENT_PROC_RC::OK) || luaRet > static_cast<int>(EVENT_PROC_RC::CONSUMED)) {
		LogError("Failed calling " << fullName << " luaRet=" << luaRet);
		jsAssert(false);
		return EVENT_PROC_RC::OK;
	}

	return (EVENT_PROC_RC)luaRet;
}

bool LuaEventHandler::RegisterLuaHandler(IDispatcher *dispatcher, lua_State *state, const char *luaFnName,
	ULONG eventVersion, EVENT_ID eventId,
	ULONG filter, bool filterIsMask,
	OBJECT_ID filteredObjectId,
	EVENT_PRIORITY priority) {
	jsVerifyReturn(dispatcher != 0 && luaFnName != 0 && *luaFnName != 0, false);

	// create a new info object
	HandlerInfo *info = new HandlerInfo(this, eventId, luaFnName, state);
	if (!dispatcher->RegisterFilteredHandler(EventHandlerFn, (ULONG)info, luaFnName, eventVersion, eventId, filter, filterIsMask,
			filteredObjectId, priority)) {
		delete info;
		return false;
	}

	// add the fn name to our list
	m_handlers.push_back(info);
	std::string s;
	LogDebug(luaFnName << " (" << eventId << ") v" << VersionToString(eventVersion, s) << " p" << (int)priority);

	return true;
}

bool LuaEventHandler::RemoveLuaHandler(IDispatcher *dispatcher, lua_State * /*state*/, const char *luaFnName, EVENT_ID eventId) {
	for (HandlerInfoVect::iterator i = m_handlers.begin(); i != m_handlers.end();) {
		if (((*i)->id == eventId) && ((*i)->fn == luaFnName)) {
			// remove this handler from the dispatcher
			char s[255];
			_snprintf_s(s, _countof(s), _TRUNCATE, "LuaEventHandler id=%d", (*i)->id);
			dispatcher->RemoveHandler(LuaEventHandler::EventHandlerFn, (ULONG)*i, s, (*i)->id);

			// remove the fn name from our list
			delete *i;
			i = m_handlers.erase(i);
		} else {
			i++;
		}
	}
	return true;
}

EVENT_PROC_RC LuaEventHandler::GetFunctionHandler(ULONG lparam, IDispatcher * /*disp*/, IEvent *e) {
	LuaEventHandler *me = (LuaEventHandler *)lparam;
	if (!me)
		return EVENT_PROC_RC::NOT_PROCESSED;

	GetFunctionEvent *gfe = dynamic_cast<GetFunctionEvent *>(e);
	if (!gfe)
		return EVENT_PROC_RC::NOT_PROCESSED;

	std::string name, vmName;
	ULONG vm;
	(*gfe->InBuffer()) >> name >> vmName >> vm;
	if (vmName != SG_VM_NAME || !vm)
		return EVENT_PROC_RC::NOT_PROCESSED;

	return me->GetFunctions(name.c_str(), (LuaScriptVM *)vm);
}

bool LuaEventHandler::RegisterEvents(IDispatcher *dispatcher, IEncodingFactory *factory) {
	m_dispatcher = dispatcher;
	return TScriptingEventHandler<std::string, lua_State *>::RegisterEvents(dispatcher, factory);
}

bool LuaEventHandler::RegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory) {
	m_dispatcher = dispatcher;
	RegisterGetFunctionHandlers(dispatcher, LuaEventHandler::GetFunctionHandler, "Lua", (ULONG)this);
	return TScriptingEventHandler<std::string, lua_State *>::RegisterEventHandlers(dispatcher, factory);
}

bool LuaEventHandler::ReRegisterEventHandlers(IDispatcher *dispatcher, IEncodingFactory *factory) {
	m_dispatcher = dispatcher;
	RegisterGetFunctionHandlers(dispatcher, LuaEventHandler::GetFunctionHandler, "Lua", (ULONG)this);
	return TScriptingEventHandler<std::string, lua_State *>::ReRegisterEventHandlers(dispatcher, factory);
}

// Returns full script file path and name '<path>\<script>.lua'
std::string LuaEventHandler::GetVMFileName(lua_State *state) const {
	for (VMMap::const_iterator i = m_vmMap.begin(); i != m_vmMap.end(); ++i) {
		if (i->second == state)
			return i->first;
	}
	return std::string();
}

extern "C" BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID /*lpReserved*/) {
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
		StringResource = (HINSTANCE)hModule;
	return TRUE;
}
