/******************************************************************************
 LuaScriptGlue.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <js.h>

#include "Event/Base/EventStrings.h"
#include "KEPHelpers.h"
#include "LuaScriptVM.h"

namespace KEP {
class IKEPObject;
} // namespace KEP

#include "LuaHelper.h"

#include "..\KEPUtil\RefPtr.h"

#define SG_VM_NAME "Lua"
#define SG_NATIVE_VM new LuaScriptVM(state)

//=============================================================
//
// inlines used by macros below, for the varg type
//
//=============================================================
// overrides of sg_Push for various datatypes, used by macros below
inline void sg_Push(lua_State *state, double value) {
	lua_pushnumber(state, value);
}
inline void sg_Push(lua_State *state, const char *value) {
	lua_pushstring(state, value ? value : "");
}
inline void sg_Push(lua_State *state, void *value) {
	lua_pushlightuserdata(state, value);
}

// overrides of sg_Pop for various datatypes, used by macros below
inline void sg_Pop(lua_State *state, double &value, int idx) {
	value = lua_tonumber(state, idx);
}
inline void sg_Pop(lua_State *state, const char *&value, int idx) {
	value = lua_tostring(state, idx);
	if (!value)
		value = "";
}
inline void sg_Pop(lua_State *state, void *&value, int idx) {
	value = lua_touserdata(state, idx);
}

// Script Glue Internal Function Redirect
#define SG_REDIRECT(fn) return fn(state);

// set the number of return value, if you don't know at top
inline void sg_ResetReturnCount(int &__retValue, int retCount) {
	__retValue = retCount;
}
#define SG_RESET_RETURN_COUNT(retCount) sg_ResetReturnCount(__retValue, retCount)

// return normally
inline int sg_Return(int __retValue) {
	return __retValue;
}
#define SG_RETURN return sg_Return(__retValue)

// return an error, if got error pushing or popping
// lua_error does a longjump, to no return needed, but included to avoid compiler warning
inline int sg_ReturnErr(lua_State *state) {
	lua_error(state);
	return 0;
}
#define SG_RETURN_ERR return sg_ReturnErr(state)

// return a custom error
inline int sg_ReturnErrStr(lua_State *state, const std::string &errStr) {
	lua_pushstring(state, errStr.c_str());
	lua_error(state);
	return 0;
}
#define SG_RETURN_ERR_STR(errStr) return sg_ReturnErrStr(state, std::string(__FUNCTION__) + ": " + errStr)

// push lua nil instead of 0
inline int sg_ReturnNull(lua_State *state) {
	lua_pushnil(state);
	return 1;
}
#define SG_RETURN_NULL return sg_ReturnNull(state)

// Begin Script Glue Function
#define SG_BEGIN_FN(fn, retCount)         \
	extern "C" int fn(lua_State *state) { \
		state;                            \
		int __popped = 1;                 \
		__popped;                         \
		int __retValue = retCount;        \
		__retValue;                       \
		int __retArgs = 0;                \
		__retArgs;

// Script Glue Push Objects
#define SG_DYNAMIC_CAST_TO_PUSH_OBJ(pFrom, baseType, pushObj) \
	void *pushObj = (void *)dynamic_cast<baseType *>(pFrom)

#define SG_CAST_TO_PUSH_OBJ(pFrom, pushObj) \
	void *pushObj = (void *)pFrom

// DRF - RefPtr Validated Cast To Push Object
#define SG_VALID_DYNAMIC_CAST_TO_PUSH_OBJ(pFrom, baseType, pushObj) \
	SG_DYNAMIC_CAST_TO_PUSH_OBJ(pFrom, baseType, pushObj);          \
	::RefPtrAdd(typeid(pFrom).name(), pushObj)

// DRF - RefPtr Validated Cast To Push Object
#define SG_VALID_CAST_TO_PUSH_OBJ(pFrom, pushObj) \
	SG_CAST_TO_PUSH_OBJ(pFrom, pushObj);          \
	::RefPtrAdd(typeid(pFrom).name(), pushObj)

// Script Glue Pop Objects
#define SG_DECLARE_POP_OBJ(type, ptr, popObj) \
	type *ptr;                                \
	void *popObj;

// DRF - RefPtr Validated Cast From Pop Object
#define SG_VALID_CAST_FROM_POP_OBJ(igs, popObj)                  \
	if (!REF_PTR_VALID(popObj))                                  \
		SG_RETURN_ERR_STR("Invalid Object Passed As Parameter"); \
	if (!SG_CAST_TO_OBJECT(igs, popObj))                         \
		SG_RETURN_ERR_STR("Failed Casting Object Passed As Parameter");

// SetInstance() Pointers
#define SG_DECLARE_ICE_PTR(ice)           \
	auto ice = IClientEngine::Instance(); \
	if (!ice)                             \
		SG_RETURN_ERR;

#define SG_DECLARE_IGS_PTR(igs) \
	IGetSet *igs = g_pIGS;      \
	if (!igs)                   \
		SG_RETURN_ERR;

// for popping, did cast to object
template <class T>
bool SG_CAST_TO_OBJECT(T *&pTo, void *pFrom) {
	pTo = (T *)(pFrom);
	return pTo != 0;
}

template <class T, class B>
bool sg_CastToDerivedObject(T *&pTo, void *pFrom) {
	pTo = dynamic_cast<T *>((B *)pFrom);
	return pTo != 0;
}

// for popping, did cast to object, first casting to a base class
#define SG_CAST_TO_DERIVED_OBJECT(pTo, derivedType, baseType, pFrom) sg_CastToDerivedObject<derivedType, baseType>(pTo, pFrom)

#define SG_POP_OBJECT(type, name) type *name = (type *)lua_touserdata(state, __popped++);

#define SG_POP_OBJECT_CAST(myType, baseType, name) myType *name = dynamic_cast<myType *>((baseType *)lua_touserdata(state, __popped++));

#define SG_POP(value) sg_Pop(state, value, __popped++);

#define SG_PUSH_OBJECT(p) lua_pushlightuserdata(state, p);

#define SG_PUSH_OBJECT_CAST(baseType, p) lua_pushlightuserdata(state, dynamic_cast<baseType>(p));

#define SG_PUSH(value) sg_Push(state, value)

// end of the function
#define SG_END_FN }

//=============================================================
//
// Push/pop by varg-method macros
//
//=============================================================
#define ToLuaBool(b) ((double)((b) ? 1.0 : 0.0))
#define FromLuaBool(b) (b > 0.1)

#define SG_POPARGS(...) sgPopArgs(state, __VA_ARGS__)

// simulate the Python way since the Lua way makes Python much slower,
// but the Python way had only a slight impact on lua
// return TRUE for ok
inline int sgPopArgs(lua_State *state, const char *fmt, ...) {
	if (!fmt)
		return FALSE;
	va_list marker;
	va_start(marker, fmt);
	for (int i = 1, count = ::strlen(fmt); i <= count; ++i) {
		switch (*fmt) {
			case 's': { // String Type
				const char **v = va_arg(marker, const char **);
				if (v) {
					sg_Pop(state, *v, i);
					if (*v == NULL)
						*v = ""; // drf - crash fix
				}
			} break;

			case 'd': { // Double Type
				double *v = va_arg(marker, double *);
				if (v)
					sg_Pop(state, *v, i);
			} break;

			case 'f': { // Float Type (cast from double)
				float *v = va_arg(marker, float *);
				double vLua = 0.0;
				if (v) {
					sg_Pop(state, vLua, i);
					*v = (float)vLua;
				}
			} break;

			case 'O': { // Object Type
				void **v = va_arg(marker, void **);
				if (v)
					sg_Pop(state, *v, i);
			} break;

			case 'b': { // Bool Type (cast from double: 0.0, 1.0)
				bool *v = va_arg(marker, bool *);
				double vLua = 0.0;
				if (v) {
					sg_Pop(state, vLua, i);
					*v = FromLuaBool(vLua);
				}
			} break;

			default: { // Unknown Type
				jsAssert(false);
				lua_pushstring(state, (loadStr(IDS_ES_INVALID_TYPE) + fmt).c_str());
				lua_error(state);
				va_end(marker);
			}
				return FALSE;
		}
		fmt++;
	}
	va_end(marker);
	return TRUE;
}

#define SG_PUSHARGS(...) (__retArgs = sgPushArgs(state, __VA_ARGS__))

inline int sgPushArgs(lua_State *state, const char *fmt, ...) {
	va_list marker;
	auto count = ::strlen(fmt);
	va_start(marker, fmt);
	for (size_t i = 1; i <= count; i++) {
		switch (*fmt) {
			case 's': { // String Type
				const char *v = va_arg(marker, const char *);
				sg_Push(state, v);
			} break;

			case 'd': { // Double Type
				double v = va_arg(marker, double);
				sg_Push(state, v);
			} break;

			case 'f': { // Float Type (cast to double)
				double v = (double)va_arg(marker, float);
				sg_Push(state, v);
			} break;

			case 'O': { // Object Type
				void *v = va_arg(marker, void *);
				sg_Push(state, v);
			} break;

			case 'b': { // Bool Type (cast to double: 0.0, 1.0)
				double v = ToLuaBool(va_arg(marker, bool));
				sg_Push(state, v);
			} break;

			default: {
				jsAssert(false);
				lua_pushstring(state, (loadStr(IDS_ES_INVALID_TYPE) + fmt).c_str());
				lua_error(state);
			}
				return FALSE;
		}
		fmt++;
	}
	va_end(marker);
	return TRUE;
}

//=============================================================
//
// registration of your callback functions in the VM
//
//=============================================================
// used in header, or by caller to create prototype of registration
#define SG_DEFINE_VM_REGISTER(name) void name(IScriptVM *state);

// methods to register one or more methods with the VM
// the registration method itself
#define SG_VM_REGISTER(name)      \
	void name(IScriptVM *state) { \
		std::string __className;

// make the list of fns to register
#define SG_BEGIN_SG_REGISTER(name) __className = #name;
#define SG_REGISTER(scriptname, cppname) lua_register(((LuaScriptVM *)state)->GetState(), (__className + "_" #scriptname).c_str(), cppname);
#define SG_END_REGISTER

// end the fn
#define SG_END_VM_REGISTER }

#define NO_RETURN_VALUE 0
#define ONE_RETURN_VALUE 1
#define TWO_RETURN_VALUES 2
#define THREE_RETURN_VALUES 3
#define FOUR_RETURN_VALUES 4
#define RETURN_VALUES(x) x

namespace LuaScriptGlue {
// The upvalue is a value associated with each call to the function. Different pszFunctionExportName values can use the same function pointer,
// but different upvalues to differentiate them.
inline void RegisterLuaCFunction(KEP::IScriptVM *pScriptVM, const LuaCFunction &luafunc) {
	lua_State *pLuaState = ((KEP::LuaScriptVM *)pScriptVM)->GetState();
	lua_pushinteger(pLuaState, luafunc.m_iFunctionUpValue);
	lua_pushcclosure(pLuaState, luafunc.m_pFunctionAddress, 1);
	lua_setglobal(pLuaState, luafunc.m_pszFunctionName);
}

template <typename FunctionType>
inline void RegisterScriptFunction(KEP::IScriptVM *pScriptVM, const char *pszFunctionExportName, FunctionType &&f) {
	LuaCFunction luaFunction = CreateLuaCFunction(pszFunctionExportName, f);
	RegisterLuaCFunction(pScriptVM, luaFunction);
}

} // namespace LuaScriptGlue
using namespace LuaScriptGlue;

#define REGISTER_SCRIPT_FUNCTION(ScriptFunctionName, Function) \
	RegisterScriptFunction(state, ScriptFunctionName, Function);
