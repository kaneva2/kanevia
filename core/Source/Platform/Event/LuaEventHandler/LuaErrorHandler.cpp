///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "LuaErrorHandler.h"

#include "LuaHelper.h"

namespace KEP {

int LuaErrorHandler(lua_State *L) {
	/* Save off the existing error message */
	const char *msg = lua_tostring(L, -1);

	/* Create a stack trace */
	lua_getglobal(L, "debug");
	lua_pushstring(L, "traceback");
	lua_gettable(L, -2);

	if (lua_isfunction(L, -1)) {
		/* Call debug.traceback */
		if (lua_pcall(L, 0, 1, 0) == 0) {
			/* Push a new message on the stack that has the original error message
				   (if provided) and the traceback information */
			const char *traceback = lua_tostring(L, -1);
			lua_pop(L, 1);
			if (msg) {
				lua_pushfstring(L, "%s\n%s", msg, traceback);
			} else {
				lua_pushfstring(L, "%s", traceback);
			}
			return 1;
		}
	}

	lua_pop(L, 3);

	/* Problem making stack trace, push the old error message back on the stack */
	lua_pushstring(L, msg);

	return 1;
}

} // namespace KEP