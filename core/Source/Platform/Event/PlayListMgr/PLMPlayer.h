///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "InstanceId.h"
#include <string>
#include <map>

namespace KEP {
typedef ULONG NETID;

class PLMPlayer {
public:
	PLMPlayer() {}

	NETID GetNetID() { return m_netID; }
	void SetNetID(NETID id) { m_netID = id; }

	int GetPlayerID() { return m_playerID; }
	void SetPlayerID(int id) { m_playerID = id; }

	bool GetIsAICtrl() { return m_isAICtrl; }
	void SetIsAICtrl(bool isAICtrl) { m_isAICtrl = isAICtrl; }

	std::string GetWorldName() { return m_worldName; }
	void SetWorldName(std::string worldName) { m_worldName = worldName; }

	ZoneIndex GetPreviousZoneIndex() { return m_previousZoneIndex; }
	void SetPreviousZoneIndex(const ZoneIndex& previousZoneIndex) { m_previousZoneIndex = previousZoneIndex; }

	ZoneIndex GetZoneIndex() { return m_zoneIndex; }
	void SetZoneIndex(const ZoneIndex& zoneIndex) { m_zoneIndex = zoneIndex; }

	std::string GetPlayerName() { return m_playerName; }
	void SetPlayerName(std::string playerName) { m_playerName = playerName; }

private:
	NETID m_netID;
	int m_playerID;
	bool m_isAICtrl;
	std::string m_worldName;
	ZoneIndex m_previousZoneIndex;
	ZoneIndex m_zoneIndex;
	std::string m_playerName;
};

class PLM_Players {
public:
	// unique NETID and player object
	typedef std::map<NETID, PLMPlayer*> PlayerMap;

	PLM_Players() {}

	~PLM_Players() {
		for (const auto& pr : m_playersMap) {
			PLMPlayer* player = pr.second;
			delete player;
		}
		m_playersMap.clear();
	}

	std::pair<PlayerMap::iterator, bool> addPlayer(NETID netId, PLMPlayer* player) {
		return m_playersMap.insert(std::make_pair(netId, player));
	}

	PLMPlayer* removePlayer(NETID netId) {
		auto itr = m_playersMap.find(netId);
		if (itr != m_playersMap.end()) {
			auto res = itr->second;
			m_playersMap.erase(itr);
			return res;
		}
		return nullptr;
	}

	PLMPlayer* getPlayer(NETID netId) {
		auto itr = m_playersMap.find(netId);
		if (itr != m_playersMap.end()) {
			return itr->second;
		}

		return nullptr;
	}

	size_t getPlayerCount() const {
		return m_playersMap.size();
	}

	void getAllPlayers(std::vector<PLMPlayer*>& outVector) {
		outVector.clear();
		outVector.reserve(m_playersMap.size());
		for (const auto& pr : m_playersMap) {
			outVector.push_back(pr.second);
		}
	}

private:
	PlayerMap m_playersMap;
};

// unique zone, a PLM_Players object
typedef std::map<ZoneIndex, PLM_Players*> ZonePlayerMap;

} // namespace KEP
