//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PlayListMgr.rc
//
#define IDS_STRING102                   102
#define IDS_LOAD_GAME_DATA_TRY          104
#define IDS_LOAD_GAME_DATA_SUCCESS      105
#define IDS_LOAD_GAME_DATA_ERROR        106
#define IDS_PURGE_ATTEMPT               107
#define IDS_PRUNE_PLAYER                108

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
