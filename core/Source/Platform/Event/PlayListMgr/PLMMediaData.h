///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include <map>
#include <set>
#include <string>
#include <vector>
#include <functional>

enum class eMediaType : int {
	INVALID = 0,
	GAME = 1,
	VIDEO = 2,
	MP3 = 4,
	TV = 7,
	WIDGET = 8
};

enum class eMediaSubType {
	NONE = 0,
	YOUTUBE = 1,
};

namespace KEP {

class PlaylistNextTrackEvent;

class PlaylistAsset {
public:
	PlaylistAsset() :
			m_assetID(0),
			m_assetTypeID(eMediaType::INVALID),
			m_assetSubTypeID(eMediaSubType::NONE),
			m_played(false),
			m_runtimeSeconds((TimeSec)0.0) {
		m_thumbNailMediumPath.clear();
		m_mediaPath.clear();
	}

	void SetAssetID(int assetID) {
		m_assetID = assetID;
	}
	int GetAssetID() const {
		return m_assetID;
	}

	void SetAssetTypeID(eMediaType assetTypeID) {
		m_assetTypeID = assetTypeID;
	}
	eMediaType GetAssetTypeID() const {
		return m_assetTypeID;
	}

	void SetAssetSubTypeID(eMediaSubType assetSubTypeID) {
		m_assetSubTypeID = assetSubTypeID;
	}
	eMediaSubType GetAssetSubTypeID() const {
		return m_assetSubTypeID;
	}

	void SetOffsiteAssetID(const std::string& offsiteAssetID) {
		m_offsiteAssetID = offsiteAssetID;
	}
	std::string GetOffsiteAssetID() const {
		return m_offsiteAssetID;
	}

	void SetRuntimeSeconds(TimeSec runtimeSeconds) {
		m_runtimeSeconds = runtimeSeconds;
	}
	TimeSec GetRuntimeSeconds() const {
		return m_runtimeSeconds;
	}

	void SetThumbNailMediumPath(const std::string& thumbNailMediumPath) {
		m_thumbNailMediumPath = thumbNailMediumPath;
	}
	std::string GetThumbNailMediumPath() const {
		return m_thumbNailMediumPath;
	}

	void SetMediaPath(const std::string& mediaPath) {
		m_mediaPath = mediaPath;
	}
	std::string GetMediaPath() const {
		return m_mediaPath;
	}

	void SetAssetName(const std::string& assetName) {
		m_assetName = assetName;
	}
	std::string GetAssetName() const {
		return m_assetName;
	}

	// DRF - ED-7153 - Playlist Proper Shuffle
	void SetPlayedFlag(bool played) {
		m_played = played;
	}
	bool GetPlayedFlag() const {
		return m_played;
	}

private:
	int m_assetID;

	bool m_played; // DRF - ED-7153 - Playlist Proper Shuffle

	TimeSec m_runtimeSeconds;
	eMediaType m_assetTypeID;
	eMediaSubType m_assetSubTypeID;
	std::string m_offsiteAssetID;

	std::string m_thumbNailMediumPath;
	std::string m_mediaPath;
	std::string m_assetName;
};

typedef std::vector<PlaylistAsset*> PlaylistAssetList;

class PlaylistAssets {
public:
	PlaylistAssets(bool shuffle) :
			m_playlistId(0),
			m_playlistChanged(false),
			m_shuffle(shuffle),
			m_mediaIndex(0),
			m_mediaIndexInc(1),
			m_trackCounter(0) {}

	~PlaylistAssets() {
		disposeAssets();
	}

	void disposeAssets() {
		for (auto asset : m_assetList)
			delete asset;
		m_assetList.clear();
	}

	PlaylistAsset* getAsset(size_t index) {
		return (index < m_assetList.size()) ? m_assetList[index] : nullptr;
	}

	const PlaylistAssetList& getAssetList() const {
		return m_assetList;
	}

	size_t getAssetListSize() const {
		return m_assetList.size();
	}

	bool setPlaylistId(int playlistId) {
		m_playlistId = playlistId;
		return true;
	}
	int getPlaylistId() {
		return m_playlistId;
	}

	bool setPlaylistChanged(bool changed) {
		m_playlistChanged = changed;
		return true;
	}
	bool getPlaylistChanged() const {
		return m_playlistChanged;
	}

	void init() {
		if (m_shuffle)
			incrementMediaIndex();
	}

	void incrementMediaIndex() {
		auto assets = getAssetListSize();
		if (assets == 0) {
			m_mediaIndex = 0;
		} else {
			if (m_shuffle)
				shuffleMediaIndex();
			else
				m_mediaIndex = (m_mediaIndex + 1) % assets;
		}
		m_trackCounter++;
	}

	// DRF - ED-7153 - Playlist Proper Shuffle
	void shuffleMediaIndex() {
		std::vector<int> notPlayed;
		size_t i = 0;
		for (const auto& pAsset : m_assetList) {
			if (!pAsset->GetPlayedFlag())
				notPlayed.push_back(i);
			++i;
		}
		if (notPlayed.empty()) {
			size_t j = 0;
			for (const auto& pAsset : m_assetList) {
				pAsset->SetPlayedFlag(false);
				notPlayed.push_back(j);
				++j;
			}
		}
		size_t s = rand() % notPlayed.size();
		m_mediaIndex = notPlayed[s];
		m_assetList[m_mediaIndex]->SetPlayedFlag(true);
	}

	bool setMediaIndex(size_t mediaIndex) {
		auto assets = getAssetListSize();
		if (assets == 0) {
			m_mediaIndex = 0;
		} else {
			m_mediaIndex = mediaIndex % assets;
		}
		m_trackCounter++;
		return true;
	}

	size_t getMediaIndex() const {
		auto assets = getAssetListSize();
		if (assets == 0)
			return 0;
		return m_mediaIndex % assets;
	}

	size_t getNextMediaIndex() const {
		auto assets = getAssetListSize();
		if (assets == 0)
			return 0;
		return (m_mediaIndex + 1) % assets;
	}

	size_t getTrackCounter() const {
		return m_trackCounter;
	}

	bool addAssetNoDupes(PlaylistAsset* pAsset_new) {
		for (const auto& pAsset : m_assetList) {
			if (pAsset->GetAssetID() == pAsset_new->GetAssetID())
				return true;
		}
		m_assetList.push_back(pAsset_new);
		return false;
	}

	void migrateAssetPlayedFlags(PlaylistAssets* pAssets_new) {
		if (!pAssets_new)
			return;
		for (auto& pAsset_new : pAssets_new->m_assetList) {
			for (const auto& pAsset_me : m_assetList)
				if (pAsset_new->GetAssetID() == pAsset_me->GetAssetID())
					pAsset_new->SetPlayedFlag(pAsset_me->GetPlayedFlag());
		}
	}

	// Update assets list while keeping the current playback status
	void replaceAssets(PlaylistAssets* pAssets_new) {
		if (!pAssets_new)
			return;
		migrateAssetPlayedFlags(pAssets_new);
		disposeAssets();
		m_assetList = pAssets_new->m_assetList;
		pAssets_new->m_assetList.clear();
		setPlaylistChanged(false);
	}

private:
	int m_playlistId;
	bool m_playlistChanged;
	bool m_shuffle;
	size_t m_mediaIndex;
	size_t m_mediaIndexInc;
	size_t m_trackCounter;
	PlaylistAssetList m_assetList;
};

class MediaPlayerConfig {
public:
	MediaPlayerConfig(int playlistId, double videoRange = 0, double audioRange = 0, double volumePercentage = 100) :
			m_playlistId(playlistId),
			m_videoRange(videoRange),
			m_audioRange(audioRange),
			m_volumePercentage(volumePercentage) {}

	int getPlaylistId() const {
		return m_playlistId;
	}
	void setPlaylistId(int playlistId) {
		m_playlistId = playlistId;
	}

	double getVideoRange() const {
		return m_videoRange;
	}
	void setVideoRange(double videoRange) {
		m_videoRange = videoRange;
	}

	double getAudioRange() const {
		return m_audioRange;
	}
	void setAudioRange(double audioRange) {
		m_audioRange = audioRange;
	}

	double getVolumePercentage() const {
		return m_volumePercentage;
	}
	void setVolumePercentage(double volumePercentage) {
		m_volumePercentage = volumePercentage;
	}

private:
	int m_playlistId;
	double m_videoRange;
	double m_audioRange;
	double m_volumePercentage;
};

typedef std::map<int, MediaPlayerConfig> PlacementMediaPlayerConfigMap; // objectPlacementId -> MediaPlayerConfig

typedef std::map<int, PlaylistNextTrackEvent*> PlaylistNextTrackEventMap; // playlistId -> One pending event per playlist

typedef std::map<int, std::set<int>> PlaylistPlacementMap; // playlistId -> set<objectPlacementIds>

typedef std::map<int, PlaylistAssets*> PlaylistAssetsMap; // playlistId -> PlayListAssets*

class ZoneMediaState {
public:
	ZoneMediaState() {}

	~ZoneMediaState() {
		clearPendingPlaylistNextTrackEvents(); // Dispatcher owns event once it is queued.
		clearPlacementsPlaylistIds(); // Simple types
		disposePlaylistsAssets(); // Destroy playlist asset lists
	}

	void addPlacementMediaPlayerCfg(int placementId, const MediaPlayerConfig& mpCfg) {
		m_placementMediaPlayerConfigMap.insert(std::make_pair(placementId, mpCfg));
		if (mpCfg.getPlaylistId() != 0)
			assignPlacementToPlaylist(mpCfg.getPlaylistId(), placementId);
	}

	void setPlacementRangeAndVolume(int placementId, double videoRange, double audioRange, double volumePercentage) {
		auto itr = m_placementMediaPlayerConfigMap.find(placementId);
		if (itr == m_placementMediaPlayerConfigMap.end())
			itr = m_placementMediaPlayerConfigMap.insert(std::make_pair(placementId, MediaPlayerConfig(0))).first;

		if (videoRange != -1)
			itr->second.setVideoRange(videoRange);

		if (audioRange != -1)
			itr->second.setAudioRange(audioRange);

		if (volumePercentage != -1)
			itr->second.setVolumePercentage(volumePercentage);
	}

	void setPlacementPlaylistId(int placementId, int playlistId) {
		auto itr = m_placementMediaPlayerConfigMap.find(placementId);
		if (itr != m_placementMediaPlayerConfigMap.end()) {
			int oldPlaylistId = itr->second.getPlaylistId();
			itr->second.setPlaylistId(playlistId);

			if (oldPlaylistId != 0) {
				jsVerify(true == removePlacementFromPlaylist(oldPlaylistId, placementId));
			}
			if (playlistId != 0) {
				assignPlacementToPlaylist(playlistId, placementId);
			}
		} else {
			addPlacementMediaPlayerCfg(placementId, MediaPlayerConfig(playlistId));
		}
	}

	bool removePlacementPlaylistId(int placementId) {
		auto itr = m_placementMediaPlayerConfigMap.find(placementId);
		if (itr != m_placementMediaPlayerConfigMap.end()) {
			int oldPlaylistId = itr->second.getPlaylistId();
			m_placementMediaPlayerConfigMap.erase(itr);
			removePlacementFromPlaylist(oldPlaylistId, placementId);
			return true;
		}
		return false;
	}

	int getPlacementPlaylistId(int placementId) const {
		auto itr = m_placementMediaPlayerConfigMap.find(placementId);
		return (itr != m_placementMediaPlayerConfigMap.end()) ? itr->second.getPlaylistId() : 0;
	}

	const MediaPlayerConfig& getPlacementMediaPlayerCfg(int placementId) const {
		static MediaPlayerConfig s_mpCfg(0);
		auto itr = m_placementMediaPlayerConfigMap.find(placementId);
		return (itr != m_placementMediaPlayerConfigMap.end()) ? itr->second : s_mpCfg;
	}

	int getFirstPlacementPlaylistId() const {
		auto itr = m_placementMediaPlayerConfigMap.begin();
		return (itr != m_placementMediaPlayerConfigMap.end()) ? itr->second.getPlaylistId() : 0;
	}

	void getAllPlacementIds(std::vector<int>& outVector) const {
		outVector.reserve(m_placementMediaPlayerConfigMap.size());
		for (const auto& pr : m_placementMediaPlayerConfigMap)
			outVector.push_back(pr.first);
	}

	void getAllPlacementPlaylistIds(std::set<int>& outSet) const {
		for (const auto& pr : m_placementMediaPlayerConfigMap) {
			outSet.insert(pr.second.getPlaylistId());
		}
	}

	size_t getPlacementsCount() const {
		return m_placementMediaPlayerConfigMap.size();
	}

	void clearPlacementsPlaylistIds() {
		m_placementMediaPlayerConfigMap.clear();
		m_playlistPlacementMap.clear();
	}

	bool setPlaylistChanged(int playlistId) {
		auto itr = m_playlistAssetsMap.find(playlistId);
		if (itr == m_playlistAssetsMap.end())
			return false;
		PlaylistAssets* pAssets_find = itr->second;
		return pAssets_find ? pAssets_find->setPlaylistChanged(true) : false;
	}

	bool getPlaylistChanged(int playlistId) const {
		auto itr = m_playlistAssetsMap.find(playlistId);
		if (itr == m_playlistAssetsMap.end())
			return false;
		PlaylistAssets* pAssets_find = itr->second;
		return pAssets_find ? pAssets_find->getPlaylistChanged() : false;
	}

	void replacePlaylistAssets(int playlistId, PlaylistAssets* pAssets) {
		if (!pAssets)
			return;
		auto itr = m_playlistAssetsMap.find(playlistId);
		if (itr == m_playlistAssetsMap.end()) {
			m_playlistAssetsMap.insert(std::make_pair(playlistId, pAssets));
			return;
		}
		PlaylistAssets* pAssets_find = itr->second;
		if (pAssets_find)
			pAssets_find->replaceAssets(pAssets);
	}

	PlaylistAssets* removePlaylistAssets(int playlistId) {
		auto itr = m_playlistAssetsMap.find(playlistId);
		if (itr == m_playlistAssetsMap.end())
			return nullptr;
		PlaylistAssets* pAssets_find = itr->second;
		m_playlistAssetsMap.erase(itr);
		return pAssets_find;
	}

	PlaylistAssets* getPlaylistAssets(int playlistId) const {
		auto itr = m_playlistAssetsMap.find(playlistId);
		if (itr == m_playlistAssetsMap.end())
			return nullptr;
		PlaylistAssets* pAssets_find = itr->second;
		return pAssets_find;
	}

	void disposePlaylistsAssets() {
		for (const auto& pr : m_playlistAssetsMap)
			delete pr.second;
		m_playlistAssetsMap.clear();
	}

	size_t getPlaylistCount() const {
		return m_playlistAssetsMap.size();
	}

	bool addPendingPlaylistNextTrackEvent(int playlistId, PlaylistNextTrackEvent* e) {
		//jsAssert(playlistId == e->m_playlistId);
		bool res = false;
		if (playlistId != 0) {
			auto itr = m_playlistNextTrackEventMap.find(playlistId);
			if (itr == m_playlistNextTrackEventMap.end()) {
				m_playlistNextTrackEventMap.insert(std::make_pair(playlistId, e));
				res = true;
			} else {
				res = false;
			}
		}
		return res;
	}

	void clearPendingPlaylistNextTrackEvents() {
		m_playlistNextTrackEventMap.clear();
	}

	// Remove SWF event based on playlist ID and placement ID
	PlaylistNextTrackEvent* removePendingPlaylistNextTrackEvent(int playlistId) {
		auto itr = m_playlistNextTrackEventMap.find(playlistId);
		if (itr == m_playlistNextTrackEventMap.end())
			return nullptr;
		PlaylistNextTrackEvent* pPNTE = itr->second;
		m_playlistNextTrackEventMap.erase(itr);
		return pPNTE;
	}

	size_t getPlaylistUsageCount(int playlistId) const {
		auto itr = m_playlistPlacementMap.find(playlistId);
		return (itr != m_playlistPlacementMap.end()) ? itr->second.size() : 0;
	}

	int getFirstPlacementByPlaylistId(int playlistId) {
		auto itr = m_playlistPlacementMap.find(playlistId);
		if (itr == m_playlistPlacementMap.end())
			return 0;
		const auto& pids = itr->second;
		return pids.empty() ? 0 : *pids.begin();
	}

protected:
	void assignPlacementToPlaylist(int playlistId, int placementId) {
		auto itr = m_playlistPlacementMap.find(playlistId);
		if (itr == m_playlistPlacementMap.end()) {
			m_playlistPlacementMap.insert(make_pair(playlistId, std::set<int>{ placementId }));
			return;
		}
		auto& pids = itr->second;
		pids.insert(placementId);
	}

	bool removePlacementFromPlaylist(int playlistId, int placementId) {
		auto itr = m_playlistPlacementMap.find(playlistId);
		if (itr == m_playlistPlacementMap.end())
			return false;
		auto& pids = itr->second;
		auto itrPid = pids.find(placementId);
		if (itrPid == pids.end())
			return false;
		pids.erase(itrPid);
		if (pids.empty())
			m_playlistPlacementMap.erase(itr);
		return true;
	}

	void reassignPlaylist(int oldPlaylistId, int newPlaylistId) {
		std::set<int> pids;
		if (oldPlaylistId == 0) {
			// Remap all playlists to the new ID
			for (const auto& itr : m_playlistPlacementMap) {
				const auto& placementSet = itr.second;
				pids.insert(placementSet.begin(), placementSet.end());
			}
			m_playlistPlacementMap.clear();
		} else {
			// Remap one playlist to the new ID
			auto itr = m_playlistPlacementMap.find(oldPlaylistId);
			if (itr != m_playlistPlacementMap.end()) {
				const auto& placementSet = itr->second;
				pids.insert(placementSet.begin(), placementSet.end());
			}
			m_playlistPlacementMap.erase(itr);
		}

		if (newPlaylistId != 0 && !pids.empty())
			m_playlistPlacementMap.insert(make_pair(newPlaylistId, pids));
	}

private:
	PlacementMediaPlayerConfigMap m_placementMediaPlayerConfigMap;

	// a map of playlist ids and an asset list for the given playlist
	PlaylistAssetsMap m_playlistAssetsMap;

	// a map from playlist back to object placement IDs
	PlaylistPlacementMap m_playlistPlacementMap;

	// a list of pending next track events
	PlaylistNextTrackEventMap m_playlistNextTrackEventMap;
};

} // namespace KEP
