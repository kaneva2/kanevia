///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <windows.h>

#include <js.h>

#include "PlayListMgr.h"

#include "resource.h"

#include "KEPCommon.h"

#include "..\Event\ServerEvents.h"

#include "KEPHelpers.h"
#include "KEPConfig.h"
#include "KEPException.h"
#include "InstanceId.h"
#include "IServerEngine.h"
#include "IPlayer.h"
#include "IGetSet.h"

#include "..\Base\IEncodingFactory.h"
#include "Event/Events/YouTubeSWFUrlBackgroundEvent.h"
#include "FlashScriptAccessMode.h"

#include "common\include\NetworkCfg.h"

using namespace std;

#include "LogHelper.h"
static LogInstance("Instance");

// Implementation notes:
//
//	Currently, if a dynamic object has a playlist, then this playlist is shared with any other object in the zone that has
// 	a playlist. The dynamic objects do not have a unique playlists. This behavior can be controlled via the
//	class PlayListMgr m_singleSwfMode member. Since each object shares the playlist, the playlist is not duplicated for each object,
// 	an optimization.
//
// 	When an object has its playlist set to none, the object will not share the playlist of other objects. When an object sets it playlist,
// 	any other object in the zone will reset to the given playlist. All objects of the same type, e.g. 13" TV, will always broadcast the same playlist
// 	regardless since the mesh is shared.

extern "C" BOOL APIENTRY DllMain(
	HANDLE hModule,
	DWORD ul_reason_for_call,
	LPVOID /*lpReserved*/
) {
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
		StringResource = (HINSTANCE)hModule;
	return TRUE;
}

namespace KEP {

PlaylistMgr::PlaylistMgr() :
		m_scriptDirReadOnly(false),
		m_lastYouTubeSWFUrlDBOverrideUpdateTime(0),
		m_lastYouTubeSWFUrlWebCurrentUpdateTime(0) {
	m_name = typeid(*this).name();
	m_dispatcher = NULL;

	// Defaults, overridden by db
	m_youTubeSWFUrlLastKnownGood.clear();
	m_youTubeSWFUrlDBOverride.clear();
	m_kanevaSWF.clear();
	m_defaultThumbURL.clear();
	m_baseStreamingURL.clear();
	m_baseImageURL.clear();
}

PlaylistMgr::~PlaylistMgr() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"SHUTDOWN"), "PlaylistMgr::~PlaylistMgr()");
	// Clear pending player arrary
	for (const auto& itr : m_pendingPlayerMap)
		delete itr.second;
	m_pendingPlayerMap.clear();

	// Clear tracked players per zone. Additionally, clear zone when empty.
	clearZonePlayers();

	// Clear PlayListAssetsMap. PlacementMediaMap is vector of simple types, no need to clean up.
	RemoveZoneMediaMap(true);
	LOG4CPLUS_TRACE(Logger::getInstance(L"SHUTDOWN"), "PlaylistMgr::~PlaylistMgr() exiting cleanly.");
}

bool PlaylistMgr::RegisterEvents(IDispatcher* dispatcher, IEncodingFactory* ef) {
	if (!dispatcher || !ef)
		return false;

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	IGame* game = dispatcher->Game();
	if (!game || (game->EngineType() != eEngineType::Server))
		return false;

	RegisterEvent<PlaylistBackgroundEvent>(*dispatcher);
	RegisterEvent<UpdateDynamicObjectMediaEvent>(*dispatcher); // DRF - Added
	RegisterEvent<UpdateDynamicObjectMediaVolumeAndRadiusEvent>(*dispatcher); // DRF - Added
	RegisterEvent<PlaylistNextTrackEvent>(*dispatcher);
	RegisterEvent<ScriptServerEvent>(*dispatcher);
	RegisterEvent<DynamicObjectMediaChangedEvent>(*dispatcher); // Media update notification sent from server engine event processor
	RegisterEvent<ObjectTicklerEvent>(*dispatcher); // DRF - Added
	RegisterEvent<YouTubeSWFUrlBackgroundEvent>(*dispatcher);

	if (!Init(dispatcher)) {
		LogError("DB initialization failed");
		return false;
	}

	return true;
}

bool PlaylistMgr::RegisterEventHandlers(IDispatcher* dispatcher, IEncodingFactory* ef) {
	if (!dispatcher || !ef)
		return false;

	// init static factory in this DLL
	IEvent::SetEncodingFactory(ef);

	auto pISE = dynamic_cast<IServerEngine*>(dispatcher->Game());
	if (!pISE || (pISE->EngineType() != eEngineType::Server))
		return false;

	// now register handlers
	dispatcher->RegisterHandler(PlaylistMgr::ZoneDownloadCompleteEventHandler, (ULONG)this, "ZoneCustomizationDLCompleteEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("ZoneCustomizationDLCompleteEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::PlayerSpawnedEventHandler, (ULONG)this, "PlayerSpawnedEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("PlayerSpawnedEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::DisconnectedEventHandler, (ULONG)this, "DisconnectedEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("DisconnectedEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::ClientExitEventHandler, (ULONG)this, "ClientExitEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("ClientExitEvent"), EP_HIGH + 1);
	dispatcher->RegisterHandler(PlaylistMgr::RemoveDynamicObjectHandler, (ULONG)this, "RemoveDynamicObjectEvent", PackVersion(1, 1, 0, 0), dispatcher->GetEventId("RemoveDynamicObjectEvent"), EP_HIGH);
	dispatcher->RegisterHandler(PlaylistMgr::DynamicObjectMediaChangedHandler, (ULONG)this, "DynamicObjectMediaChangedEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("DynamicObjectMediaChangedEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::UpdateDynamicObjectMediaVolumeAndRadiusHandler, (ULONG)this, "UpdateDynamicObjectMediaVolumeAndRadiusEvent", PackVersion(1, 1, 0, 0), dispatcher->GetEventId("UpdateDynamicObjectMediaVolumeAndRadiusEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::PlaylistBackgroundEventHandler, (ULONG)this, "PlaylistBackgroundEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("PlaylistBackgroundEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::PlaylistNextTrackEventHandler, (ULONG)this, "PlaylistNextTrackEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("PlaylistNextTrackEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::ScriptClientEventHandler, (ULONG)this, "ServerEngine::ScriptClientEventHandler", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("ScriptClientEvent"), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::ObjectTicklerEventHandler, (ULONG)this, "ObjectTicklerEventHandler", ObjectTicklerEvent::ClassVersion(), ObjectTicklerEvent::ClassId(), EP_NORMAL);
	dispatcher->RegisterHandler(PlaylistMgr::YouTubeSWFUrlBackgroundEventHandler, (ULONG)this, "YouTubeSWFUrlBackgroundEvent", PackVersion(1, 0, 0, 0), dispatcher->GetEventId("YouTubeSWFUrlBackgroundEvent"), EP_NORMAL);
	SetDispatcher(dispatcher);
	SetGameStateDispatcher(pISE->GetGameStateDispatcher());

	// Refresh YouTube SWF player URL from YouTube.com
	refreshYouTubeSWFUrlAsync(YouTubeSWFUrlSource::WebCurrent);
	m_lastYouTubeSWFUrlWebCurrentUpdateTime = fTime::TimeMs();
	return true;
}

void PlaylistMgr::GetPlaylistZone(NETID fromID, const ZoneIndex& zoneIndex, int playerID, string playerName) {
	auto pEvent = new PlaylistBackgroundEvent();
	pEvent->m_eType = PlaylistBackgroundEvent::PL_GET_PLAYLIST_ZONE;
	pEvent->zoneIndex = zoneIndex;
	pEvent->m_playerID = playerID;
	pEvent->m_charName = playerName;
	pEvent->SetFrom(fromID);
	GetGameStateDispatcher()->QueueEvent(pEvent);
}

void PlaylistMgr::GetPlaylistSingle(NETID fromID, const ZoneIndex& zoneIndex, int playerID, string playerName, int playlistId, int mediaIndex, int objId) {
	auto pEvent = new PlaylistBackgroundEvent();
	pEvent->m_eType = PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE;
	pEvent->zoneIndex = zoneIndex;
	pEvent->m_playerID = playerID;
	pEvent->m_charName = playerName;
	pEvent->m_playlistId = playlistId;
	pEvent->m_playlistMediaIndex = mediaIndex;
	pEvent->m_objectPlacementID = objId;
	pEvent->SetFrom(fromID);
	GetGameStateDispatcher()->QueueEvent(pEvent);
}

void PlaylistMgr::GetPlaylistSingleNextTrack(const ZoneIndex& zoneIndex, int playlistId, int mediaIndex, int objId) {
	auto pEvent = new PlaylistBackgroundEvent();
	pEvent->m_eType = PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE_NEXT_TRACK;
	pEvent->zoneIndex = zoneIndex;
	pEvent->m_playlistId = playlistId;
	pEvent->m_playlistMediaIndex = mediaIndex;
	pEvent->m_objectPlacementID = objId;
	GetGameStateDispatcher()->QueueEvent(pEvent);
}

// static
EVENT_PROC_RC PlaylistMgr::PlayerSpawnedEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	PlaylistMgr* me = (PlaylistMgr*)lparam;
	if (!me)
		return EVENT_PROC_RC::NOT_PROCESSED;

	ZoneIndex zoneIndex = e->Filter();
	NETID netId = e->From();

	// Decode the event.
	int networkAssignedID = 0;
	long player_object_ptr = 0;
	int isAICtrl = 0;
	string worldName;
	ULONG prevZoneIndexInt = 0;
	(*e->InBuffer()) >> networkAssignedID >> player_object_ptr >> isAICtrl >> worldName >> prevZoneIndexInt;

	IServerEngine* game = dynamic_cast<IServerEngine*>(disp->Game());
	IPlayer* player = game->GetPlayerByNetId(netId, true);
	if (player == 0) {
		LogWarn("GetPlayerByNetId() FAILED - " << zoneIndex.ToStr() << " netId=" << netId);
		return EVENT_PROC_RC::OK;
	}

	LogInfo(player->ToStr() << " '" << worldName << "'");

	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);
	string playerName = player->Values()->GetString(PLAYERIDS_CHARNAME);

	auto itr = me->m_pendingPlayerMap.find(netId);
	if (itr != me->m_pendingPlayerMap.end()) {
		LogError("Already Tracking Pending Player - " << zoneIndex.ToStr() << " netId=" << netId << " playerId=" << playerId);
		return EVENT_PROC_RC::OK;
	}

	// Store spawned player data.
	// Wait for handleZoneDLCompleteEvent so that the zone has all the dynamic objects
	// loaded. Otherwise, performing work on spawn may not give the client time enough to
	// load all the dynamic objects.
	PLMPlayer* l_player = new PLMPlayer();
	l_player->SetNetID(netId);
	l_player->SetPlayerID(playerId);
	l_player->SetPlayerName(playerName);
	l_player->SetIsAICtrl(isAICtrl != 0);
	l_player->SetWorldName(worldName);
	l_player->SetPreviousZoneIndex(ZoneIndex(prevZoneIndexInt));
	l_player->SetZoneIndex(zoneIndex);
	me->m_pendingPlayerMap.insert(make_pair(netId, l_player));
	return EVENT_PROC_RC::OK;
}

// static
EVENT_PROC_RC PlaylistMgr::ZoneDownloadCompleteEventHandler(ULONG lparam, IDispatcher* /*disp*/, IEvent* e) {
	PlaylistMgr* me = (PlaylistMgr*)lparam;
	if (!me)
		return EVENT_PROC_RC::NOT_PROCESSED;

	ZoneIndex zoneIndex = e->Filter();
	NETID netId = e->From();

	LogInfo(zoneIndex.ToStr() << " netId=" << netId);

	auto itr = me->m_pendingPlayerMap.find(netId);
	if (itr == me->m_pendingPlayerMap.end())
		return EVENT_PROC_RC::OK;

	PLMPlayer* l_player = NULL;
	l_player = itr->second;
	if (l_player->GetIsAICtrl()) {
		delete l_player;
		me->m_pendingPlayerMap.erase(itr);
	} else {
		bool alreadyInZone = false;
		bool firstPlayerInZone = me->addPlayerToZone(l_player, alreadyInZone);

		if (l_player->GetZoneIndex() != l_player->GetPreviousZoneIndex() && l_player->GetPreviousZoneIndex() != INVALID_ZONE_INDEX)
			me->removePlayerFromZone(l_player->GetPreviousZoneIndex(), l_player->GetNetID());

		// Remove from pending players array. Don't delete the player object, we inserted it into the zone players array.
		// Let management of those arrays clean it up.
		me->m_pendingPlayerMap.erase(itr);

		if (alreadyInZone) {
			// The dynamic objects are re-created, even if the player isn't changing zones,
			// so the players need to be given the current media, even if the player isn't going
			// to a new zone.
			me->BroadcastMediaChangeZone(l_player->GetZoneIndex(), false, l_player->GetNetID(), true);

			// already represented in the zone. Delete the pending player object.
			delete l_player;
		} else {
			// First player in zone
			if (firstPlayerInZone) {
				me->GetPlaylistZone(netId, l_player->GetZoneIndex(), l_player->GetPlayerID(), l_player->GetPlayerName());
			} else {
				me->BroadcastMediaChangeZone(l_player->GetZoneIndex(), false, l_player->GetNetID(), true);
			}
		}
	}

	return EVENT_PROC_RC::OK;
}

// static
EVENT_PROC_RC PlaylistMgr::ScriptClientEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	auto pSCE = dynamic_cast<ScriptClientEvent*>(e);
	if (!me || !pSCE)
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto eventType = pSCE->GetEventType();
	switch (eventType) {
		case ScriptClientEvent::ObjectGetNextPlaylistItem:
		case ScriptClientEvent::ObjectEnumeratePlaylist: {
			int zoneIndexInt;
			ULONG objId;
			NETID clientNetId;
			PlaylistAssets* pAssets = nullptr;
			*pSCE->InBuffer() >> clientNetId >> zoneIndexInt >> objId;
			ZoneIndex zoneIndex = (ZoneIndex)zoneIndexInt;

			LogInfo("ObjectGetNextPlaylistItem - " << zoneIndex.ToStr() << " objId=" << objId << " netId=" << clientNetId);

			auto zoneMediaState = me->getZoneMediaState(zoneIndex);
			if (zoneMediaState) {
				if (objId != 0) {
					auto playlistId = zoneMediaState->getPlacementPlaylistId(objId);
					if (playlistId != 0) {
						pAssets = zoneMediaState->getPlaylistAssets(playlistId);
						if (!pAssets) {
							LogError("No Assets Found - " << zoneIndex.ToStr() << " objId=" << objId);
						}
					} else {
						LogError("Object Placement Not Found - " << zoneIndex.ToStr() << " objId=" << objId);
					}
				}
			} else {
				LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr() << " objId=" << objId);
			}

			ScriptServerEvent::EventType sseEventType =
				((eventType == ScriptClientEvent::ObjectGetNextPlaylistItem) ?
						ScriptServerEvent::ObjectGetNextPlaylistItem :
						ScriptServerEvent::ObjectEnumeratePlaylist);

			auto pSSE = new ScriptServerEvent(sseEventType, objId, zoneIndexInt, 0, clientNetId);
			pSSE->SetPlaylistAssets(pAssets);
			pSSE->SetFrom(clientNetId);
			me->GetDispatcher()->QueueEvent(pSSE);

			return EVENT_PROC_RC::OK;
		} break;

		case ScriptClientEvent::ObjectSetNextPlaylistItem: {
			// Select track on a specific playlist (all objects sharing the same playlist will be updated together)
			int success = 0;
			int zoneIndexInt;
			ULONG objId;
			NETID clientNetId;
			int playlistItem;
			int playlistId = 0;
			PlaylistAssets* pAssets = nullptr;
			*pSCE->InBuffer() >> clientNetId >> playlistItem >> zoneIndexInt >> objId;
			ZoneIndex zoneIndex(zoneIndexInt);

			LogInfo("ObjectSetNextPlaylistItem - " << zoneIndex.ToStr() << " objId=" << objId << " netId=" << clientNetId);

			if (playlistItem < 0)
				playlistItem = 0;

			auto zoneMediaState = me->getZoneMediaState(zoneIndex);
			if (zoneMediaState) {
				if (objId != 0) {
					auto playlistId2 = zoneMediaState->getPlacementPlaylistId(objId);
					if (playlistId2 != 0) {
						pAssets = zoneMediaState->getPlaylistAssets(playlistId2);
						if (pAssets) {
							// This is more validation than anything. Technically, we just need the zoneMediaState and the objId
							// but it does allow us to return the currently playing media item in case of failure.
							if (me->IncrementMediaIndex(zoneMediaState, (size_t)playlistItem, playlistId)) {
								me->CancelPlaylistNextTrackEvents(zoneMediaState, playlistId2, true);
								me->BroadcastMediaChangeSingle(zoneIndex, objId);
								success = true;
							} else {
								LogError("IncrementMediaIndex() FAILED - " << zoneIndex.ToStr() << " objId=" << objId << " playlistItem=" << playlistItem);
							}
						} else {
							LogError("Assets Not Found - " << zoneIndex.ToStr() << " objId=" << objId);
						}
					} else {
						LogError("Object Placement Not Found - " << zoneIndex.ToStr() << " objId=" << objId);
					}
				}
			} else {
				LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr() << " objId=" << objId);
			}

			if (success && pAssets != NULL) {
				playlistItem = (int)pAssets->getMediaIndex();
				playlistId = pAssets->getPlaylistId();
			}

			ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::ObjectSetNextPlaylistItem, objId, zoneIndexInt, 0, clientNetId);
			sse->SetFrom(clientNetId);
			*sse->OutBuffer() << success << playlistId << playlistItem;
			me->GetDispatcher()->QueueEvent(sse);

			return EVENT_PROC_RC::OK;
		} break;

		case ScriptClientEvent::ObjectSetPlaylist: {
			int success = 0;
			int zoneIndexInt;
			ULONG objId;
			NETID clientNetId;
			int playlistId = 0;
			int playlistItem;
			bool stopBroadcast;

			*pSCE->InBuffer() >> clientNetId >> playlistId >> playlistItem >> stopBroadcast >> zoneIndexInt >> objId;

			// Set Media Params (don't change volume or radius)
			MediaParams mediaParams("", "", -1, -1, -1, playlistId);

			ZoneIndex zoneIndex(zoneIndexInt);

			LogInfo("ObjectSetPlaylist - " << zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << playlistId << " netId=" << clientNetId);

			auto pISE = dynamic_cast<IServerEngine*>(disp->Game());
			IPlayer* pPlayer = pISE->GetPlayerByNetId(clientNetId, true);
			if (!pPlayer) {
				LogError("GetPlayerByNetId() FAILED - netId=" << clientNetId);
			} else {
				int playerId = pPlayer->Values()->GetINT(PLAYERIDS_HANDLE);
				string playerName = pPlayer->Values()->GetString(PLAYERIDS_CHARNAME);

				auto zoneMediaState = me->getZoneMediaState(zoneIndex);
				if (zoneMediaState != nullptr) {
					int oldPlaylistId = zoneMediaState->getPlacementPlaylistId(objId);
					if (oldPlaylistId != 0)
						me->CancelPlaylistNextTrackEvents(zoneMediaState, oldPlaylistId, false);
					me->ZoneBroadcastChangeMedia(zoneMediaState, zoneIndex, playerId, playerName, clientNetId, objId, mediaParams, playlistId, playlistItem, stopBroadcast);
				} else {
					LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr());
					me->ZoneBroadcastChangeMedia(NULL, zoneIndex, playerId, playerName, clientNetId, objId, mediaParams, playlistId, playlistItem, stopBroadcast);
				}
				success = 1;
			}

			auto pSSE = new ScriptServerEvent(ScriptServerEvent::ObjectSetNextPlaylistItem, objId, zoneIndexInt, 0, clientNetId);
			pSSE->SetFrom(clientNetId);
			*pSSE->OutBuffer() << success << playlistId << playlistItem;
			me->GetDispatcher()->QueueEvent(pSSE);

			return EVENT_PROC_RC::OK;
		} break;
	}

	return EVENT_PROC_RC::NOT_PROCESSED;
}

// static
EVENT_PROC_RC PlaylistMgr::PlaylistBackgroundEventHandler(ULONG lparam, IDispatcher* /*disp*/, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	auto pEvent = dynamic_cast<PlaylistBackgroundEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	ZoneIndex zoneIndex = pEvent->zoneIndex;
	auto playlistId = pEvent->m_playlistId;
	auto objId = pEvent->m_objectPlacementID;
	auto eType = pEvent->m_eType;

	LogInfo(pEvent->ToStr());

	auto zoneMediaState = me->getZoneMediaState(zoneIndex);

	bool getZone = (eType == PlaylistBackgroundEvent::PL_GET_PLAYLIST_ZONE);
	bool getSingle = (eType == PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE);
	bool getSingleNextTrack = (eType == PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE_NEXT_TRACK);

	if (getZone && !pEvent->m_zoneMediaState) {
		LogInfo("ZONE PLAYLIST UNASSIGNED - " << zoneIndex.ToStr());
		pEvent->m_zoneMediaState = new ZoneMediaState();
	}

	if ((getSingle || getSingleNextTrack) && (!pEvent->m_zoneMediaState || !pEvent->m_zoneMediaState->getPlaylistCount())) {
		// Exception 1: no playlist for the PID?
		LogInfo("OBJECT PLAYLIST UNASSIGNED - objId=" << objId << " playlistId=" << playlistId);
		if (pEvent->m_zoneMediaState) {
			delete pEvent->m_zoneMediaState;
			pEvent->m_zoneMediaState = nullptr;
		}
	} else if (getZone && zoneMediaState) {
		// Exception 2: playlist already loaded for this zone. PL_GET_PLAYLIST_ALL might have been received previously.
		// To catch a case that shouldn't happen. If it does, log it and investigate.
		LogError("ZONE ALREADY HAS MEDIA");
		if (pEvent->m_zoneMediaState) {
			delete pEvent->m_zoneMediaState;
			pEvent->m_zoneMediaState = nullptr;
		}
	} else if (!me->getZonePlayers(zoneIndex)) {
		// Exception 3: Background thread responded with data but no players are in the zone.
		// They may have zoned elsewhere before the background thread collected the data.
		LogError("ZONE EMPTY");
		if (pEvent->m_zoneMediaState) {
			delete pEvent->m_zoneMediaState;
			pEvent->m_zoneMediaState = nullptr;
		}
	} else {
		// Normal scenario
		switch (eType) {
			case PlaylistBackgroundEvent::PL_GET_PLAYLIST_ZONE: {
				me->addZoneMediaState(zoneIndex, pEvent->m_zoneMediaState);
				me->BroadcastMediaChangeZone(zoneIndex, true, 0UL, true);
			} break;

			case PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE_NEXT_TRACK:
			case PlaylistBackgroundEvent::PL_GET_PLAYLIST_SINGLE: {
				if (!zoneMediaState) {
					me->addZoneMediaState(zoneIndex, pEvent->m_zoneMediaState);
				} else {
					me->ReplacePlaylistInZone(zoneIndex, zoneMediaState, pEvent->m_zoneMediaState, objId, playlistId);
					if (pEvent->m_zoneMediaState) {
						delete pEvent->m_zoneMediaState;
						pEvent->m_zoneMediaState = nullptr;
					}
				}

				// Advance Track Index ?
				if (getSingleNextTrack)
					me->IncrementMediaIndex(zoneMediaState, 0, playlistId);

				// Broadcast Media Change For Media Object
				me->BroadcastMediaChangeSingle(zoneIndex, objId);
			} break;
		}

		pEvent->m_zoneMediaState = nullptr; // We're responsible for cleanup.
	}

	if (pEvent->m_pEventReply)
		me->GetDispatcher()->QueueEvent(pEvent->m_pEventReply);

	return EVENT_PROC_RC::OK;
}

// static
EVENT_PROC_RC PlaylistMgr::PlaylistNextTrackEventHandler(ULONG lparam, IDispatcher* /*disp*/, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	auto pEvent = dynamic_cast<PlaylistNextTrackEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	ZoneIndex zoneIndex = pEvent->m_zoneIndex;
	int playlistId = pEvent->m_playlistId;
	int mediaIndex = pEvent->m_currentMediaIndex;

	// This normally fails if player leaves zone
	auto zoneMediaState = me->getZoneMediaState(zoneIndex);
	if (!zoneMediaState)
		return EVENT_PROC_RC::OK;

	// use any media placement ID associated with this playlist - client will synchronize them
	int objId = zoneMediaState->getFirstPlacementByPlaylistId(playlistId);

	bool playlistChanged = zoneMediaState->getPlaylistChanged(playlistId);

	LogInfo(zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << playlistId << " mediaIndex=" << mediaIndex << (playlistChanged ? " PLAYLIST_CHANGED" : ""));

	// Cancel event for the playlist (shared by multiple media players)
	me->CancelPlaylistNextTrackEvents(zoneMediaState, playlistId, true);

	// DRF - ED-7522 - Web should tickle server on playlist changes
	// Has Playlist Changed ?
	if (playlistChanged) {
		// Schedule Playlist Refresh Pre-Track Change (will broadcast to all clients)
		me->GetPlaylistSingleNextTrack(zoneIndex, playlistId, mediaIndex, objId);
	} else {
		// Next Track
		me->IncrementMediaIndex(zoneMediaState, 0, playlistId);

		// Broadcast Media Change For Media Object
		me->BroadcastMediaChangeSingle(zoneIndex, objId);
	}

	return EVENT_PROC_RC::OK;
}

// DRF - ED-7522 - Web should tickle server on playlist changes
// static - handles updates to items from web
EVENT_PROC_RC PlaylistMgr::ObjectTicklerEventHandler(ULONG lparam, IDispatcher* /*disp*/, IEvent* e) {
	PlaylistMgr* me = (PlaylistMgr*)lparam;
	ObjectTicklerEvent* evt = dynamic_cast<ObjectTicklerEvent*>(e);
	if (!me || !evt)
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto ot = ObjectTicklerEvent::ObjectType::Invalid;
	auto ct = ObjectTicklerEvent::ChangeType::Invalid;
	if (!evt->ExtractInfo(ot, ct))
		return EVENT_PROC_RC::NOT_PROCESSED;

	if (ot != ObjectTicklerEvent::ObjectType::MediaPlaylist)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LONG playlistId = evt->ExtractNumericId();
	LogInfo("MediaPlaylist Changed - playlistId=" << playlistId);
	me->setPlaylistChanged(playlistId);

	return EVENT_PROC_RC::OK;
}

// static
EVENT_PROC_RC PlaylistMgr::DisconnectedEventHandler(ULONG lparam, IDispatcher* /*disp*/, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	if (!me)
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto zoneIndexInt = e->Filter();
	ZoneIndex zoneIndex(zoneIndexInt);
	NETID netId = e->From();

	LogInfo(zoneIndex.ToStr() << " netId=" << netId);

	bool trackingPlayer = false;
	if (zoneIndexInt == 0) {
		// No zone index, brute force find and remove of player
		auto player = me->getZonePlayerByNetId(netId);
		if (player != nullptr) {
			if (trackingPlayer) {
				LogError("Player Found In Multiple Zones - zoneIndex=" << player->GetZoneIndex() << " netId=" << netId);
			}
			trackingPlayer = true;
			me->removePlayerFromZone(player->GetZoneIndex(), netId);
		}
	} else {
		auto zonePlayers = me->getZonePlayers(zoneIndex);
		if (zonePlayers) {
			auto player = zonePlayers->getPlayer(netId);
			if (player) {
				trackingPlayer = true;
				me->removePlayerFromZone(zoneIndex, netId);
			} else {
				LogWarn("Not Tracking Player - " << zoneIndex.ToStr() << " netId=" << netId);
			}
		} else {
			LogWarn("Not Tracking Zone - " << zoneIndex.ToStr() << " netId=" << netId);
		}
	}

	if (!trackingPlayer) {
		// If we weren't tracking the player and they disconnected, may still be in pending player queue.
		auto itr = me->m_pendingPlayerMap.find(netId);
		if (itr != me->m_pendingPlayerMap.end()) {
			PLMPlayer* l_player = itr->second;
			delete l_player;
			me->m_pendingPlayerMap.erase(itr);
			LogWarn("Stop Tracking Player In Zone - " << zoneIndex.ToStr() << " netId=" << netId);
		}
	}

	return EVENT_PROC_RC::OK;
}

// static
EVENT_PROC_RC PlaylistMgr::ClientExitEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	if (!me || !disp || !e)
		return EVENT_PROC_RC::NOT_PROCESSED;

	NETID netId = e->From();

	IServerEngine* pISE = dynamic_cast<IServerEngine*>(disp->Game());
	IPlayer* player = pISE->GetPlayerByNetId(netId, true);
	if (!player)
		return EVENT_PROC_RC::OK; // player has left - just accept it

	int zoneIndexInt = player->Values()->GetINT(PLAYERIDS_CURRENTZONEINDEX);
	ZoneIndex zoneIndex(zoneIndexInt);

	LogInfo(zoneIndex.ToStr() << " netId=" << netId);

	bool trackingPlayer = false;
	auto zonePlayers = me->getZonePlayers(zoneIndex);
	if (zonePlayers != nullptr) {
		auto zonePlayer = zonePlayers->getPlayer(netId);
		if (zonePlayer != nullptr) {
			trackingPlayer = true;
			me->removePlayerFromZone(zoneIndex, netId);
		} else {
			LogWarn("Not Tracking Player - " << zoneIndex.ToStr() << " netId=" << netId);
		}
	} else {
		LogWarn("Not Tracking Zone - " << zoneIndex.ToStr() << " netId=" << netId);
	}

	if (!trackingPlayer && zoneIndexInt != 0) {
		// If we weren't tracking the player and they disconnected, may still be in pending player queue.
		auto itr = me->m_pendingPlayerMap.find(netId);
		if (itr != me->m_pendingPlayerMap.end()) {
			PLMPlayer* l_player = itr->second;
			delete l_player;
			me->m_pendingPlayerMap.erase(itr);
		}
	}

	return EVENT_PROC_RC::OK;
}

// static
EVENT_PROC_RC PlaylistMgr::RemoveDynamicObjectHandler(ULONG lparam, IDispatcher* /*disp*/, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	if (!me || !e)
		return EVENT_PROC_RC::NOT_PROCESSED;

	ZoneIndex zoneIndex(e->Filter());
	int objId = e->ObjectId();
	NETID netId = e->From();

	// Dynamic Object Has Media Assigned ?
	auto playlistId = me->GetMediaObjectPlaylistId(zoneIndex, objId);
	if (playlistId == 0)
		return EVENT_PROC_RC::OK;

	LogInfo(zoneIndex.ToStr() << " objId=" << objId << " netId=" << netId);

	me->RemoveMediaObjectFromZone(zoneIndex, objId);

	return EVENT_PROC_RC::OK;
}

// static
EVENT_PROC_RC PlaylistMgr::DynamicObjectMediaChangedHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	auto pEvent = dynamic_cast<DynamicObjectMediaChangedEvent*>(e);
	assert(pEvent != nullptr);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	NETID netId;
	ZoneIndex zoneIndex;
	std::vector<int> affectedPlacementIds;
	MediaParams mediaParams;
	pEvent->ExtractInfo(netId, zoneIndex, affectedPlacementIds, mediaParams);

	int placementIdForDebug = 0; // First affected PID
	if (!affectedPlacementIds.empty()) {
		placementIdForDebug = affectedPlacementIds[0];
	}

	LogInfo(zoneIndex.ToStr() << " objId=" << placementIdForDebug << " netId=" << netId);

	IServerEngine* game = dynamic_cast<IServerEngine*>(disp->Game());
	IPlayer* player = game->GetPlayerByNetId(netId, true);
	if (!player) {
		LogWarn("GetPlayerByNetId() FAILED - netId=" << netId);
		return EVENT_PROC_RC::OK;
	}

	int playerId = player->Values()->GetINT(PLAYERIDS_HANDLE);
	string playerName = player->Values()->GetString(PLAYERIDS_CHARNAME);

	for (int placementId : affectedPlacementIds) {
		auto zoneMediaState = me->getZoneMediaState(zoneIndex);
		if (zoneMediaState != nullptr) {
			int oldPlaylistId = zoneMediaState->getPlacementPlaylistId(placementId);
			if (oldPlaylistId != 0)
				me->CancelPlaylistNextTrackEvents(zoneMediaState, oldPlaylistId, false);
			me->ZoneBroadcastChangeMedia(zoneMediaState, zoneIndex, playerId, playerName, netId, placementId, mediaParams, 0, 0, false);
		} else {
			LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr() << " objId=" << placementIdForDebug << " netId=" << netId);
			me->ZoneBroadcastChangeMedia(NULL, zoneIndex, playerId, playerName, netId, placementId, mediaParams, 0, 0, false);
		}
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC PlaylistMgr::UpdateDynamicObjectMediaVolumeAndRadiusHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (PlaylistMgr*)lparam;
	auto pEvent = dynamic_cast<UpdateDynamicObjectMediaVolumeAndRadiusEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Decode event
	int objPlacementId = 0, zi = 0;
	MediaParams mediaParams;
	pEvent->ExtractInfo(objPlacementId, mediaParams, &zi);

	// Validate paramters
	if (objPlacementId == 0) {
		LogError("objPlacementId must not be 0");
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	if (zi == 0) {
		LogError("ZoneIndex must not be 0");
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	ZoneIndex zoneIndex(zi);

	// Record new settings
	ZoneMediaState* zoneMediaState = me->getZoneMediaState(zoneIndex);
	if (!zoneMediaState) {
		LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}
	zoneMediaState->setPlacementRangeAndVolume(objPlacementId, mediaParams.GetRadiusVideo(), mediaParams.GetRadiusAudio(), mediaParams.GetVolume());

	// Broadcast to clients
	me->sendUpdateDynamicObjectMediaVolumeAndRadiusEvent(zoneIndex, objPlacementId, mediaParams);

	return EVENT_PROC_RC::OK;
}

int PlaylistMgr::GetMediaObjectPlaylistId(const ZoneIndex& zoneIndex, int objId) const {
	// Get Zone Media State
	auto zoneMediaState = getZoneMediaState(zoneIndex);
	if (!zoneMediaState) {
		LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr() << " objId=" << objId);
		return 0;
	}

	// Return Media Object Playlist Id (0=not assigned)
	return zoneMediaState->getPlacementPlaylistId(objId);
}

bool PlaylistMgr::RemoveMediaObjectFromZone(const ZoneIndex& zoneIndex, int objId) {
	// Get Media Object Playlist Id
	auto playlistId = GetMediaObjectPlaylistId(zoneIndex, objId);
	if (playlistId == 0)
		return true;

	// Get Zone Media State
	auto zoneMediaState = getZoneMediaState(zoneIndex);
	if (!zoneMediaState) {
		LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr() << " objId=" << objId);
		return false;
	}

	LogInfo(zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << playlistId);

	// Remove Playlist From Object
	zoneMediaState->removePlacementPlaylistId(objId);

	// Remove Playlist If No Longer Referenced
	int count = zoneMediaState->getPlaylistUsageCount(playlistId);
	if (count == 0) {
		auto pAssets = zoneMediaState->removePlaylistAssets(playlistId);
		if (!pAssets) {
			LogError("removePlaylist() FAILED - " << zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << playlistId << " refCount=" << count);
			return false;
		}

		delete pAssets;
	}

	// Remove Media State If No Playlists In Zone
	if (zoneMediaState->getPlaylistCount() == 0) {
		removeZoneMediaState(zoneIndex);
		delete zoneMediaState;
	}

	return true;
}

bool PlaylistMgr::addPlayerToZone(PLMPlayer* pPlayer, bool& alreadyInZone) {
	bool firstPlayer = false;
	PLM_Players* pPlayers = getZonePlayers(pPlayer->GetZoneIndex());
	if (pPlayers) {
		// Currently tracking zone, add this player to it.
		auto pr = pPlayers->addPlayer(pPlayer->GetNetID(), pPlayer);
		if (!pr.second) {
			alreadyInZone = true;
		}
	} else {
		// Is this a zone we're currently not tracking:
		//  a) create new player list
		//  b) add player to it
		//  c) add new zone to zone list
		pPlayers = new PLM_Players();
		pPlayers->addPlayer(pPlayer->GetNetID(), pPlayer);
		addZonePlayers(pPlayer->GetZoneIndex(), pPlayers);
		firstPlayer = true;
	}

	return firstPlayer;
}

void PlaylistMgr::removePlayerFromZone(const ZoneIndex& zoneIndex, NETID netId) {
	auto zonePlayers = getZonePlayers(zoneIndex);
	if (!zonePlayers) {
		LogError("Zone Not Found - " << zoneIndex.ToStr() << " netId=" << netId);
		return;
	}

	auto player = zonePlayers->removePlayer(netId);
	if (!player) {
		LogError("Player Not In Zone - " << zoneIndex.ToStr() << " netId=" << netId);
		return;
	}

	delete player;

	// Empty zone?
	if (zonePlayers->getPlayerCount() == 0) {
		removeZonePlayers(zoneIndex);
		delete zonePlayers;
		RemoveZoneMediaMap(false, zoneIndex);
	}
}

bool PlaylistMgr::IncrementMediaIndex(ZoneMediaState* zoneMediaState, size_t targetMediaIndex, int playlistId) {
	if (!zoneMediaState || !playlistId)
		return false;

	auto pAssets = zoneMediaState->getPlaylistAssets(playlistId);
	if (!pAssets)
		return false;

	if (targetMediaIndex > 0)
		pAssets->setMediaIndex(targetMediaIndex - 1);
	else
		pAssets->incrementMediaIndex();

	return true;
}

bool PlaylistMgr::BroadcastMediaChangeZone(const ZoneIndex& zoneIndex, bool allPlayers, NETID sendToPlayer, bool sendRangeAndVolume) {
	// Get Zone Media State
	auto zoneMediaState = getZoneMediaState(zoneIndex);
	if (!zoneMediaState) {
		LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr());
		return false;
	}

	// Get Media Objects
	vector<int> placementIdArray;
	zoneMediaState->getAllPlacementIds(placementIdArray);

	// Broadcast Media Change To All Media Objects
	for (auto pid : placementIdArray)
		BroadcastMediaChange(zoneMediaState, zoneIndex, pid, allPlayers, sendToPlayer, sendRangeAndVolume);

	return true;
}

bool PlaylistMgr::BroadcastMediaChangeSingle(const ZoneIndex& zoneIndex, int placementId, bool allPlayers, NETID sendToPlayer, bool sendRangeAndVolume) {
	// Get Zone Media State
	auto zoneMediaState = getZoneMediaState(zoneIndex);
	if (!zoneMediaState) {
		LogError("getZoneMediaState() FAILED - " << zoneIndex.ToStr());
		return false;
	}

	// Broadcast Media Change To Media Object
	BroadcastMediaChange(zoneMediaState, zoneIndex, placementId, allPlayers, sendToPlayer, sendRangeAndVolume);

	return true;
}

void PlaylistMgr::BroadcastMediaChange(
	ZoneMediaState* zoneMediaState,
	const ZoneIndex& zoneIndex,
	int objId,
	bool allPlayers,
	NETID sendToPlayer,
	bool sendRangeAndVolume) {
	// Whenever we tell client to play something new (with some minimum interval), check if YouTube AS3 player URL has changed
	if (fTime::ElapsedMs(m_lastYouTubeSWFUrlDBOverrideUpdateTime) >= YOUTUBE_SWF_URL_DBOVERRIDE_REFRESH_INTERVAL) {
		// Check DB override
		refreshYouTubeSWFUrlAsync(YouTubeSWFUrlSource::DBOverride);
		m_lastYouTubeSWFUrlDBOverrideUpdateTime = fTime::TimeMs();
	} else if (fTime::ElapsedMs(m_lastYouTubeSWFUrlWebCurrentUpdateTime) >= YOUTUBE_SWF_URL_WEBCURRENT_REFRESH_INTERVAL) {
		// Check YouTube website for current settings
		refreshYouTubeSWFUrlAsync(YouTubeSWFUrlSource::WebCurrent);
		m_lastYouTubeSWFUrlWebCurrentUpdateTime = fTime::TimeMs();
	}

	// Get Media Configuration For Media Object
	auto mediaPlayerConfig = zoneMediaState->getPlacementMediaPlayerCfg(objId);

	// Get Media Object Playlist Id
	double videoRange = -1, audioRange = -1, volumePercentage = -1;
	int playlistId = mediaPlayerConfig.getPlaylistId();
	if (sendRangeAndVolume) {
		videoRange = mediaPlayerConfig.getVideoRange();
		audioRange = mediaPlayerConfig.getAudioRange();
		volumePercentage = mediaPlayerConfig.getVolumePercentage();

		if (playlistId == 0) {
			// Send volume and radius only
			sendUpdateDynamicObjectMediaEvent(zoneIndex, objId, MediaParams("", "playlistUrl=None", volumePercentage, audioRange, videoRange), 0, allPlayers ? 0 : sendToPlayer);
			return;
		}
	}

	// Get Playlist Assets
	auto pAssets = zoneMediaState->getPlaylistAssets(playlistId);
	if (!pAssets) {
		LogError("Empty Playlist - objId=" << objId << " playlistId=" << playlistId);
		return;
	}

	// Get Current Playlist Asset
	size_t mediaIndex = pAssets->getMediaIndex();
	PlaylistAsset* pAsset = pAssets->getAsset(mediaIndex);
	if (!pAsset) {
		LogError("Empty Playlist - objId=" << objId << " playlistId=" << playlistId << " mediaIndex=" << mediaIndex);
		return;
	}

	// Broadcast Update Media Event To All Players
	BroadcastUpdateDynamicObjectMediaEvent(
		zoneIndex,
		objId,
		playlistId,
		pAsset,
		pAssets->getTrackCounter(),
		allPlayers,
		sendToPlayer,
		videoRange,
		audioRange,
		volumePercentage);

	if (!allPlayers)
		return;

	// DRF - ED-7153 - Playlist Proper Shuffle
	// Set Asset Played Flag
	pAsset->SetPlayedFlag(true);

	// Calculate Time To Next Track Change
	TimeMs nextTrackTimeMs = (pAsset->GetRuntimeSeconds() + 3.0) * MS_PER_SEC;

	// Schedule Next Track Change Event
	auto pEvent = new PlaylistNextTrackEvent(zoneIndex, playlistId, mediaIndex);
	if (zoneMediaState->addPendingPlaylistNextTrackEvent(playlistId, pEvent))
		GetDispatcher()->QueueEventInFutureMs(pEvent, nextTrackTimeMs);
	else
		delete pEvent;
}

bool PlaylistMgr::UpdateDynamicObjectMediaEventParams(
	int playlistId,
	PlaylistAsset* assetObj,
	size_t trackCounter,
	double videoRange,
	double audioRange,
	double volumePercentage,
	MediaParams& mediaParams) {
	if (!assetObj)
		return false;

	// Determine Media Url & Params
	string swfUrl;
	string swfParam;
	eMediaType assetTypeID = assetObj->GetAssetTypeID();
	eMediaSubType assetSubTypeID = assetObj->GetAssetSubTypeID();
	FlashScriptAccessMode allowScriptAccess = FlashScriptAccessMode::Never;

	if (assetSubTypeID == eMediaSubType::YOUTUBE) {
		swfUrl = m_youTubeSWFUrlDBOverride.empty() ? m_youTubeSWFUrlLastKnownGood : m_youTubeSWFUrlDBOverride;
		if (swfUrl.empty()) {
			LogError("No valid YouTube SWF Url available");
			assert(false);
			return false;
		}

		STLStringSubstitute(swfUrl, "{0}", assetObj->GetOffsiteAssetID()); // New YouTube "watch_as3.swf" URL no longer has {0} token
		if (!m_youTubeLoaderUrl.empty()) {
			string loaderUrl = m_youTubeLoaderUrl;
			int loaderUrlId = playlistId; // Use playlistId if available, otherwise use asset ID
			if (loaderUrlId == 0)
				loaderUrlId = assetObj->GetAssetID();
			STLStringSubstitute(loaderUrl, "{0}", numToString(loaderUrlId, "%d"));
			loaderUrl = UrlEncode(loaderUrl, false); // Pass false to encode entire url to be used in a query string
			swfParam = "loaderUrl=" + loaderUrl + "&YouTubeVideoId=" + assetObj->GetOffsiteAssetID();
			allowScriptAccess = FlashScriptAccessMode::Domain;
		}
	} else if (assetTypeID == eMediaType::TV || assetTypeID == eMediaType::WIDGET) {
		swfUrl = assetObj->GetOffsiteAssetID();
		StrAppend(swfUrl, "&autoplay=1");
	} else {
		swfUrl = m_kanevaSWF;
		string thumbURL = assetObj->GetThumbNailMediumPath();
		if (thumbURL.empty() || thumbURL == "")
			thumbURL = m_defaultThumbURL;
		swfParam = "assetId=";
		StrAppend(swfParam, assetObj->GetAssetID());
		StrAppend(swfParam, "&mediumUrl=" << m_baseStreamingURL);
		if (m_baseStreamingURL[m_baseStreamingURL.length() - 1] != '/')
			swfParam.append(1, '/');
		swfParam.append(assetObj->GetMediaPath());
		StrAppend(swfParam, "&lengthSecs=" << (int)assetObj->GetRuntimeSeconds());
		StrAppend(swfParam, "&isRaved=0&type=" << (int)assetObj->GetAssetTypeID());
		StrAppend(swfParam, "&thumbUrl=" << m_baseImageURL);
		if (m_baseImageURL[m_baseImageURL.length() - 1] != '/')
			swfParam.append(1, '/');
		swfParam.append(thumbURL);
	}

	// Track counter
	swfParam.append("&trackCounter=");
	swfParam.append(std::to_string(trackCounter));

	if (playlistId != 0) {
		StrAppend(swfParam, "&playlistId=" << playlistId);
	}

	// Update Media Params
	mediaParams = MediaParams(swfUrl, swfParam, volumePercentage, audioRange, videoRange, playlistId);
	mediaParams.SetAllowScriptAccess(allowScriptAccess);

	return true;
}

void PlaylistMgr::BroadcastUpdateDynamicObjectMediaEvent(
	const ZoneIndex& zoneIndex,
	int objId,
	int playListID,
	PlaylistAsset* pAsset,
	size_t trackCounter,
	bool allPlayers,
	NETID sendToPlayer,
	double videoRange,
	double audioRange,
	double volumePercentage) {
	if (!pAsset)
		return;

	// Update Media Params
	MediaParams mediaParams;
	UpdateDynamicObjectMediaEventParams(playListID, pAsset, trackCounter, videoRange, audioRange, volumePercentage, mediaParams);

	LogInfo(zoneIndex.ToStr()
			<< " objId=" << objId
			<< " playlistId=" << playListID
			<< " assetId=" << pAsset->GetAssetID()
			<< " " << mediaParams.ToStr(true));

	// Broadcast UpdateDynamicObjectMediaEvent To All Players
	sendUpdateDynamicObjectMediaEvent(zoneIndex, objId, mediaParams, pAsset->GetAssetID(), allPlayers ? 0 : sendToPlayer);
}

//Replace playlist ID for existing media players. Radii and volume settings are not updated (ignored selected data from newZoneMediaInfo)
void PlaylistMgr::ReplacePlaylistInZone(
	const ZoneIndex& zoneIndex,
	ZoneMediaState* zoneMediaState,
	ZoneMediaState* zoneMediaState_new,
	int objId,
	int playlistId_new) {
	if (!zoneMediaState)
		return;

	LogInfo(zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << playlistId_new);

	// Verify event has an updated asset list for this playListID before scratching our existing playList
	PlaylistAssets* pAssets = zoneMediaState_new->removePlaylistAssets(playlistId_new);
	if (!pAssets) {
		LogError("Playlist Assets Not Found - " << zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << playlistId_new);
		return;
	}

	// Support for change in playListID, may or may not be the same
	int playlistId_old = zoneMediaState->getPlacementPlaylistId(objId);
	SetMediaObjectPlaylistId(zoneMediaState, objId, playlistId_new);

	if (playlistId_old != 0)
		CancelPlaylistNextTrackEvents(zoneMediaState, playlistId_old, false);

	// Add the objId and playListID to our tracked objects. May exist already if updating.
	zoneMediaState->replacePlaylistAssets(playlistId_new, pAssets);
}

void PlaylistMgr::SetMediaObjectPlaylistId(ZoneMediaState* zoneMediaState, int objId, int playlistId) {
	if (!zoneMediaState)
		return;
	zoneMediaState->setPlacementPlaylistId(objId, playlistId);
}

void PlaylistMgr::CancelPlaylistNextTrackEvents(ZoneMediaState* zoneMediaState, int playlistId, bool ignoreRefCount) {
	if (!zoneMediaState)
		return;

	bool cancelEvents = (ignoreRefCount || !zoneMediaState->getPlaylistUsageCount(playlistId));
	if (!cancelEvents)
		return;

	auto pEvent = zoneMediaState->removePendingPlaylistNextTrackEvent(playlistId);
	if (pEvent)
		GetDispatcher()->CancelEvent(pEvent);
}

void PlaylistMgr::ZoneBroadcastChangeMedia(
	ZoneMediaState* zoneMediaState,
	const ZoneIndex& zoneIndex,
	int playerID,
	const string& playerName,
	NETID fromID,
	int objId,
	const MediaParams& mediaParams,
	int playlistId,
	int playlistMediaIndex,
	bool stopBroadcast) {
	// If swfParam contains CANCEL_BROADCAST then always call RemoveMediaFromZone and finish.
	bool isParamsCancelBroadcast = mediaParams.IsParamsCancelBroadcast();
	if (isParamsCancelBroadcast) {
		bool isUrlSwf = mediaParams.IsUrlSwf();
		if (!isUrlSwf) {
			// Although this cleans up media assets in the playlistmgr
			// it doesn't actually do anything to tell clients to stop playing
			// the current playlist item.
			RemoveMediaObjectFromZone(zoneIndex, objId);
			return;
		} else {
			LogInfo("CANCEL_BROADCAST SWF - Ignoring...");
		}
	}

	// If we've not been told to stop the broadcast but playListId was not
	// provided as a function argument, look in the params for the playListId.
	// If we don't have a playListID at all, stop the broadcast anyway.
	string mParams = mediaParams.GetParams();
	if (!stopBroadcast && playlistId == 0) {
		string playListIDStr;
		size_t pos = mParams.find('=');
		if (pos != string::npos) {
			string tmp = mParams.substr(pos);
			for (size_t ii = 0; ii < tmp.length(); ii++) {
				if (isdigit(tmp.at(ii)))
					playListIDStr.push_back(tmp.at(ii));
			}
			playlistId = atoi(playListIDStr.c_str());
			LogInfo("Playlist Parsed - " << zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << playlistId << " " << mediaParams.ToStr());
		} else {
			stopBroadcast = true;
		}
	}

	// Check again to see if we have now met the conditions for continuing the broadcast.
	if (!stopBroadcast) {
		auto mediaIndex = (int)(playlistMediaIndex > 0 ? playlistMediaIndex - 1 : 0);
		GetPlaylistSingle(fromID, zoneIndex, playerID, playerName, playlistId, mediaIndex, objId);
	} else {
		StopMediaBroadcastToObj(zoneIndex, zoneMediaState, objId);
	}
}

void PlaylistMgr::StopMediaBroadcastToObj(const ZoneIndex& zoneIndex, ZoneMediaState* zoneMediaState, int objId) {
	if (!zoneMediaState)
		return;

	LogInfo(zoneIndex.ToStr() << " objId=" << objId);

	int oldPlaylistId = zoneMediaState->getPlacementPlaylistId(objId);
	if (oldPlaylistId != 0)
		CancelPlaylistNextTrackEvents(zoneMediaState, oldPlaylistId, false);

	// Support for change in playListID
	int origPlayListID = zoneMediaState->getPlacementPlaylistId(objId);

	// Update single object that used this playList. Since we're setting the playlist to none, only update our object.
	SetMediaObjectPlaylistId(zoneMediaState, objId, 0);

	// Check if we should remove this playlist (either 0 ref or single SWF mode)
	int count = zoneMediaState->getPlaylistUsageCount(origPlayListID);
	if (count == 0) {
		auto pl_assets = zoneMediaState->removePlaylistAssets(origPlayListID);
		if (pl_assets) {
			delete pl_assets;
			LogInfo("OK - playlistId=" << origPlayListID << " assets cleared");
		} else {
			LogError("Playlist not found - " << zoneIndex.ToStr() << " objId=" << objId << " playlistId=" << origPlayListID << " refCount=" << count);
		}
	}

	// Are any objects left after removing this playlist?
	if (zoneMediaState->getPlaylistCount() == 0) {
		// Is nothing left in the zone, remove it.
		removeZoneMediaState(zoneIndex);
		delete zoneMediaState;
	}

	// Send Update To Stop Playlist (don't change volume or radius)
	sendUpdateDynamicObjectMediaEvent(zoneIndex, objId, MediaParams("", "playlistUrl=None", -1, -1, -1));
}

void PlaylistMgr::RemoveZoneMediaMap(bool RemoveAll, const ZoneIndex& zoneIndex) {
	if (RemoveAll) {
		// Clear PlayListAssetsMap. PlacementMediaMap is vector of simple types, no need to clean up.
		LogInfo("RemoveAll");
		vector<ZoneMediaState*> zoneMediaStates;
		getAllZoneMediaStates(zoneMediaStates);
		for (auto zoneMediaState : zoneMediaStates) {
			delete zoneMediaState;
		}
		clearZoneMediaMap();
	} else {
		LogInfo(zoneIndex.ToStr());
		auto zoneMediaState = removeZoneMediaState(zoneIndex);
		if (zoneMediaState) {
			delete zoneMediaState;
		} else {
			LogError("Zone Not Found - " << zoneIndex.ToStr());
		}
	}
}

void PlaylistMgr::getNetworkCfgSettings(string baseDir) {
	string path = PathAdd(baseDir.c_str(), "Network.cfg");
	try {
		KEPConfig cfg;
		cfg.Load(path.c_str(), "NetworkConfig");
		m_youTubeLoaderUrl = cfg.Get("PlayList_YouTube_LoaderUrl", NET_DEFAULT_ASSET_DETAILS_URL);
		m_kanevaSWF = cfg.Get("PlayList_Kaneva_SWF", "KGPPlaylist.swf");
		m_defaultThumbURL = cfg.Get("PlayList_Default_ThumbNail_Audio", "KanevaIconAudio_me.gif");
		m_baseStreamingURL = cfg.Get("PlayList_BaseStreaming_URL", NET_STREAMING);
		m_baseImageURL = cfg.Get("PlayList_BaseImage_URL", NET_IMAGES);
	} catch (KEPException* e) {
		e->Delete();
		LogError("Caught Exception - Can not load settings from network.cfg '" << path << "'");
	}
}

bool PlaylistMgr::Init(IDispatcher* dispatcher) {
	getNetworkCfgSettings(PathBase() + "\\..\\..");

	// Get initial YouTube SWF URL settings from DB (will continue update it with YouTubeSWFUrlBackgroundEvent)
	auto pEngine = dynamic_cast<IServerEngine*>(dispatcher->Game());
	assert(pEngine != nullptr);
	if (pEngine) {
		bool rc = pEngine->GetYouTubeSWFUrlsFromDB(m_youTubeSWFUrlLastKnownGood, m_youTubeSWFUrlDBOverride); // Server engine will query DB - note that Init() is called in ServerEngine thread thus no DB connection contention
		if (!rc) {
			LogError("Error obtaining YouTube SWF URL from DB");
		} else {
			LogInfo("YouTube SWF URL Init: LastKnownGood=[" << m_youTubeSWFUrlLastKnownGood << "], Override=[" << m_youTubeSWFUrlDBOverride << "]");
			m_lastYouTubeSWFUrlDBOverrideUpdateTime = fTime::TimeMs();
		}
	} else {
		LogError("Server engine is NULL");
	}

	return true;
}

void PlaylistMgr::FreeAll() {
	auto dispatcher = GetDispatcher();
	if (!dispatcher)
		return;

	auto game = dispatcher->Game();
	if (game && (game->EngineType() == eEngineType::Server)) {
		dispatcher->RemoveHandler(PlaylistMgr::ZoneDownloadCompleteEventHandler, (ULONG)this, "ZoneCustomizationDLComleteEvent", dispatcher->GetEventId("ZoneCustomizationDLCompleteEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::PlayerSpawnedEventHandler, (ULONG)this, "PlayerSpawnedEvent", dispatcher->GetEventId("PlayerSpawnedEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::DisconnectedEventHandler, (ULONG)this, "DisconnectedEvent", dispatcher->GetEventId("DisconnectedEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::PlaylistBackgroundEventHandler, (ULONG)this, "PlaylistBackgroundEvent", dispatcher->GetEventId("PlaylistBackgroundEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::PlaylistNextTrackEventHandler, (ULONG)this, "PlaylistNextSwfEvent", dispatcher->GetEventId("PlaylistNextSwfEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::ClientExitEventHandler, (ULONG)this, "ClientExitEvent", dispatcher->GetEventId("ClientExitEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::RemoveDynamicObjectHandler, (ULONG)this, "RemoveDynamicObjectEvent", dispatcher->GetEventId("RemoveDynamicObjectEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::DynamicObjectMediaChangedHandler, (ULONG)this, "DynamicObjectMediaChangedEvent", dispatcher->GetEventId("DynamicObjectMediaChangedEvent"));
		dispatcher->RemoveHandler(PlaylistMgr::ScriptClientEventHandler, (ULONG)this, "ScriptClientEvent", dispatcher->GetEventId("ScriptClientEvent"));
	}
}

void PlaylistMgr::sendUpdateDynamicObjectMediaEvent(
	const ZoneIndex& zoneIndex,
	int objId,
	const MediaParams& mediaParams,
	int assetId,
	NETID netIdFilter) {
#ifndef REFACTOR_INSTANCEID
	auto pEvent = new UpdateDynamicObjectMediaEvent(objId, mediaParams, (LONG)zoneIndex, assetId);
#else
	auto pEvent = new UpdateDynamicObjectMediaEvent(objId, mediaParams, zoneIndex.toLong(), assetId);
#endif
	bool sendEvent = false;
	auto zonePlayers = getZonePlayers(zoneIndex);
	if (zonePlayers) {
		if (netIdFilter == 0) {
			vector<PLMPlayer*> players;
			zonePlayers->getAllPlayers(players);
			for (const auto& player : players) {
				pEvent->AddTo(player->GetNetID());
				sendEvent = true;
			}
		} else {
			auto player = zonePlayers->getPlayer(netIdFilter);
			if (player) {
				pEvent->AddTo(player->GetNetID());
				sendEvent = true;
			}
		}
	}

	if (sendEvent)
		GetDispatcher()->QueueEvent(pEvent);
	else
		delete pEvent;
}

void PlaylistMgr::sendUpdateDynamicObjectMediaVolumeAndRadiusEvent(
	const ZoneIndex& zoneIndex,
	int placementId,
	const MediaParams& mediaParams,
	NETID netIdFilter) {
#ifndef REFACTOR_INSTANCEID
	auto pEvent = new UpdateDynamicObjectMediaVolumeAndRadiusEvent(placementId, mediaParams, (LONG)zoneIndex);
#else
	auto pEvent = new UpdateDynamicObjectMediaVolumeAndRadiusEvent(placementId, mediaParams, zoneIndex.toLong());
#endif
	bool sendEvent = false;
	auto zonePlayers = getZonePlayers(zoneIndex);
	if (zonePlayers) {
		if (netIdFilter == 0) {
			vector<PLMPlayer*> players;
			zonePlayers->getAllPlayers(players);
			for (const auto& player : players) {
				pEvent->AddTo(player->GetNetID());
				sendEvent = true;
			}
		} else {
			auto player = zonePlayers->getPlayer(netIdFilter);
			if (player) {
				pEvent->AddTo(player->GetNetID());
				sendEvent = true;
			}
		}
	}

	if (sendEvent)
		GetDispatcher()->QueueEvent(pEvent);
	else
		delete pEvent;
}

void PlaylistMgr::refreshYouTubeSWFUrlAsync(YouTubeSWFUrlSource sourceType) {
	GetGameStateDispatcher()->QueueEvent(new YouTubeSWFUrlBackgroundEvent(sourceType));

	// Continue on YouTubeSWFUrlBackgroundEventHandler - will update m_youTubeSWFUrlDBOverride or m_youTubeSWFLastKnownGood
}

EVENT_PROC_RC PlaylistMgr::YouTubeSWFUrlBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	PlaylistMgr* me = reinterpret_cast<PlaylistMgr*>(lparam);

	YouTubeSWFUrlBackgroundEvent* pEvent = static_cast<YouTubeSWFUrlBackgroundEvent*>(e);

	switch (pEvent->getUrlSource()) {
		case YouTubeSWFUrlSource::DBOverride:
			if (pEvent->getUrl() != me->m_youTubeSWFUrlDBOverride) {
				me->m_youTubeSWFUrlDBOverride = pEvent->getUrl();
				if (me->m_youTubeSWFUrlDBOverride.empty()) {
					LogInfo("YouTube SWF player URL Override is cleared. Fallback to [" << me->m_youTubeSWFUrlLastKnownGood << "]");
				} else {
					LogInfo("YouTube SWF player URL Override is updated. New URL is [" << me->m_youTubeSWFUrlDBOverride << "]");
				}
			}
			break;

		case YouTubeSWFUrlSource::WebCurrent:
			if (pEvent->getUrl() != me->m_youTubeSWFUrlLastKnownGood) {
				if (pEvent->getUrl().empty()) {
					// Don't update if empty
					LogWarn("Error parsing YouTube result. Last-known-good is [" << me->m_youTubeSWFUrlLastKnownGood << "]");
				} else {
					me->m_youTubeSWFUrlLastKnownGood = pEvent->getUrl();
					LogInfo("YouTube SWF player URL is updated. New URL is [" << me->m_youTubeSWFUrlDBOverride << "]" << (me->m_youTubeSWFUrlDBOverride.empty() ? "" : " (Inactive)"));
				}
			}
			break;
	}

	return EVENT_PROC_RC::CONSUMED;
}

} // namespace KEP
