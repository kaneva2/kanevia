///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEventHandler.h"
#include "Event\Base\IEventBlade.h"

#include "PLMPlayer.h"
#include "PLMMediaData.h"

#include "common\include\MediaParams.h"

#define YOUTUBE_SWF_URL_DBOVERRIDE_REFRESH_INTERVAL 10000 // Refresh "YouTubeSWFUrlOverride", every 10 seconds
#define YOUTUBE_SWF_URL_WEBCURRENT_REFRESH_INTERVAL 3600000 // Parse YouTube web for actual SWF URL, every hour

namespace KEP {

class IGameStateDispatcher;
class PlaylistNextTrackEvent;
class ZoneMediaState;
class PlaylistAsset;
enum class YouTubeSWFUrlSource;

typedef std::map<ZoneIndex, ZoneMediaState*> ZoneMediaMap;

class PlaylistMgr : public IEventBlade {
public: // statics
	static EVENT_PROC_RC PlayerSpawnedEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ZoneDownloadCompleteEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC PlaylistBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC PlaylistNextTrackEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC DisconnectedEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ClientExitEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC RemoveDynamicObjectHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC DynamicObjectMediaChangedHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdateDynamicObjectMediaVolumeAndRadiusHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ScriptClientEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ObjectTicklerEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC YouTubeSWFUrlBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);

public:
	PlaylistMgr();

	~PlaylistMgr();

	bool Init(IDispatcher* dispatcher);

	// set the blade interface version
	static ULONG BladeVersion() {
		return PackVersion(1, 0, 0, 0);
	}

	Monitored::DataPtr monitor() const override {
		return Monitored::DataPtr(new Monitored::Data());
	}

	ULONG Version() const override {
		return PackVersion(1, 0, 0, 0);
	}

	// BaseDir is scriptPath For Event Blades
	std::string PathBase() const override {
		return m_scriptDir;
	}

	void SetPathBase(const std::string& baseDir, bool readonly) override {
		m_scriptDir = baseDir;
		m_scriptDirReadOnly = readonly;
	}

	void FreeAll() override;

	const char* Name() const override {
		return m_name.c_str();
	}

	// IEventBlade overrides
	bool RegisterEvents(IDispatcher* dispatcher, IEncodingFactory* factory) override;
	bool RegisterEventHandlers(IDispatcher* dispatcher, IEncodingFactory* factory) override;
	bool ReRegisterEventHandlers(IDispatcher* dispatcher, IEncodingFactory* factory) override {
		return true;
	}

	IDispatcher* GetDispatcher() {
		return m_dispatcher;
	}
	void SetDispatcher(IDispatcher* dispatcher) {
		m_dispatcher = dispatcher;
	}

	IGameStateDispatcher* GetGameStateDispatcher() {
		return m_gameStateDispatcher;
	}
	void SetGameStateDispatcher(IGameStateDispatcher* gameStateDispatcher) {
		m_gameStateDispatcher = gameStateDispatcher;
	}

	void GetPlaylistZone(NETID fromID, const ZoneIndex& zoneIndex, int playerID, std::string playerName);

	void GetPlaylistSingle(NETID fromID, const ZoneIndex& zoneIndex, int playerID, std::string playerName, int playlistId, int mediaIndex, int objId);

	void GetPlaylistSingleNextTrack(const ZoneIndex& zoneIndex, int playlistId, int mediaIndex, int objId);

	bool BroadcastMediaChangeZone(const ZoneIndex& zoneIndex, bool allPlayers = true, NETID sendToPlayer = 0, bool sendRangeAndVolume = false);

	bool BroadcastMediaChangeSingle(const ZoneIndex& zoneIndex, int placementId, bool allPlayers = true, NETID sendToPlayer = 0, bool sendRangeAndVolume = false);

	void BroadcastMediaChange(ZoneMediaState* zoneMediaState, const ZoneIndex& zoneIndex, int placementId, bool allPlayers, NETID sendToPlayer, bool sendRangeAndVolume);

	void BroadcastUpdateDynamicObjectMediaEvent(const ZoneIndex& zoneIndex, int objectPlacementID, int playListID, PlaylistAsset* assetObj, size_t trackCounter, bool allPlayers, NETID sendToPlayer, double videoRange, double audioRange, double volumePercentage);

	void QueuePlaylistNextTrackEventsForZone(const ZoneIndex& zoneIndex);

	void QueuePlaylistNextTrackEvent(ZoneMediaState* zoneMediaState, const ZoneIndex& zoneIndex, int playlistId, int placementId);

	void CancelPlaylistNextTrackEvents(ZoneMediaState* zoneMediaState, int playlistId, bool ignoreRefCount);

	void RemoveZoneMediaMap(bool RemoveAll, const ZoneIndex& zoneIndex = INVALID_ZONE_INDEX);

	int GetMediaObjectPlaylistId(const ZoneIndex& zoneIndex, int objId) const;

	bool RemoveMediaObjectFromZone(const ZoneIndex& zoneIndex, int objectPlacementID);

	void ZoneBroadcastChangeMedia(
		ZoneMediaState* zoneMediaState,
		const ZoneIndex& zoneIndex,
		int playerID,
		const std::string& playerName,
		NETID fromID,
		int objectPlacementID,
		const MediaParams& mediaParams,
		int playlistId,
		int playlistItem,
		bool stopBroadcast);

	void StopMediaBroadcastToObj(const ZoneIndex& zoneIndex, ZoneMediaState* zoneMediaState, int objectPlacementID);

	void ReplacePlaylistInZone(const ZoneIndex& zoneIndex, ZoneMediaState* zoneMediaState, ZoneMediaState* newZoneMediaInfo, int objectPlacementID, int playListId);

	void SetMediaObjectPlaylistId(ZoneMediaState* zoneMediaState, int objectPlacementID, int playlistId);

protected:
	bool IsBaseDirReadOnly() const override {
		return m_scriptDirReadOnly;
	}

	bool IncrementMediaIndex(ZoneMediaState* zoneMediaState, size_t targetMediaIndex = 0, int objectPlacementID = 0); // specify targetMediaIndex as zero to continue with current list

	bool UpdateDynamicObjectMediaEventParams(int playlistId, PlaylistAsset* assetObj, size_t trackCounter, double videoRange, double audioRange, double volumePercentage, MediaParams& mediaParams);

	void getNetworkCfgSettings(std::string baseDir);

	// m_zonePlayerMap accessors
	void addZonePlayers(const ZoneIndex& zoneIndex, PLM_Players* players) {
		m_zonePlayerMap.insert(std::make_pair(zoneIndex, players));
	}

	PLM_Players* removeZonePlayers(const ZoneIndex& zoneIndex) {
		auto itr = m_zonePlayerMap.find(zoneIndex);
		if (itr != m_zonePlayerMap.end()) {
			auto res = itr->second;
			m_zonePlayerMap.erase(itr);
			return res;
		}
		return nullptr;
	}

	void clearZonePlayers() {
		for (const auto& pr : m_zonePlayerMap) {
			PLM_Players* zonePlayers = pr.second;
			delete zonePlayers;
		}
		m_zonePlayerMap.clear();
	}

	PLM_Players* getZonePlayers(const ZoneIndex& zoneIndex) {
		auto itr = m_zonePlayerMap.find(zoneIndex);
		if (itr != m_zonePlayerMap.end()) {
			return itr->second;
		}
		return nullptr;
	}

	PLMPlayer* getZonePlayerByNetId(NETID netId) {
		for (const auto& pr : m_zonePlayerMap) {
			PLM_Players* zonePlayers = pr.second;
			auto player = zonePlayers->getPlayer(netId);
			if (player != nullptr)
				return player;
		}
		return nullptr;
	}

	bool addPlayerToZone(PLMPlayer* l_player, bool& alreadyInZone);

	void removePlayerFromZone(const ZoneIndex& zoneIndex, NETID netId);

	size_t getNumPlayersInZone(const ZoneIndex& zoneIndex) {
		auto zonePlayers = getZonePlayers(zoneIndex);
		if (zonePlayers != nullptr) {
			return zonePlayers->getPlayerCount();
		}
		return 0;
	}

	void addZoneMediaState(const ZoneIndex& zoneIndex, ZoneMediaState* zoneMediaState) {
		m_zoneMediaMap.insert(std::make_pair(zoneIndex, zoneMediaState));
	}

	ZoneMediaState* removeZoneMediaState(const ZoneIndex& zoneIndex) {
		auto itr = m_zoneMediaMap.find(zoneIndex);
		if (itr != m_zoneMediaMap.end()) {
			ZoneMediaState* pZMS = itr->second;
			m_zoneMediaMap.erase(itr);
			return pZMS;
		}
		return nullptr;
	}

	void clearZoneMediaMap() {
		m_zoneMediaMap.clear();
	}

	ZoneMediaState* getZoneMediaState(const ZoneIndex& zoneIndex) const {
		auto itr = m_zoneMediaMap.find(zoneIndex);
		return (itr != m_zoneMediaMap.end()) ? itr->second : nullptr;
	}

	void getAllZoneMediaStates(std::vector<ZoneMediaState*>& outVector) {
		outVector.reserve(m_zoneMediaMap.size());
		for (const auto& itr : m_zoneMediaMap) {
			ZoneMediaState* pZMS = itr.second;
			if (!pZMS)
				continue;
			outVector.push_back(pZMS);
		}
	}

	void setPlaylistChanged(int playlistId) {
		for (const auto& itr : m_zoneMediaMap) {
			ZoneMediaState* pZMS = itr.second;
			if (!pZMS)
				continue;
			pZMS->setPlaylistChanged(playlistId);
		}
	}

	void sendUpdateDynamicObjectMediaEvent(const ZoneIndex& zoneIndex, int objId, const MediaParams& mediaParams, int assetId = 0, NETID netIdFilter = 0);
	void sendUpdateDynamicObjectMediaVolumeAndRadiusEvent(const ZoneIndex& zoneIndex, int placementId, const MediaParams& mediaParams, NETID netIdFilter = 0);

	void refreshYouTubeSWFUrlAsync(YouTubeSWFUrlSource sourceType);

private:
	// for IBlade support
	std::string m_scriptDir;
	bool m_scriptDirReadOnly;
	std::string m_name;

	IDispatcher* m_dispatcher;
	IGameStateDispatcher* m_gameStateDispatcher;

	// players that have spawned but not zoned yet. Zone isn't fully initialized.
	PLM_Players::PlayerMap m_pendingPlayerMap;

	// List of zones and players in zone
	ZonePlayerMap m_zonePlayerMap;

	// List of zones and media in zone
	ZoneMediaMap m_zoneMediaMap;

	// From network.cfg
	std::string m_youTubeSWFUrlLastKnownGood;
	std::string m_youTubeSWFUrlDBOverride;
	std::string m_youTubeLoaderUrl;
	std::string m_kanevaSWF;
	std::string m_defaultThumbURL;
	std::string m_baseStreamingURL;
	std::string m_baseImageURL;

	// Last time we updated YouTube AS3 player URL
	TimeMs m_lastYouTubeSWFUrlDBOverrideUpdateTime;
	TimeMs m_lastYouTubeSWFUrlWebCurrentUpdateTime;
};

} // namespace KEP
