///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <windows.h>

#include "event/EventMap.h"

#include "event/base/ievent.h"
#include "event/base/EventRegistry.h"
#include "event/events/GenericEvent.h"
#include "LogHelper.h"
#include "Common/Include/KEPHelpers.h"

#include "GlobalEventRegistry.h"

namespace KEP {

static LogInstance("EventMap");

#define TestGroupEventMap() (g_testGroup & DRF_TEST_GROUP_EVENT_MAP)
static int g_testGroup = DRF_TEST_GROUP_OFF;
void EventMap::SetTestGroup(int testGroup) {
	g_testGroup = testGroup;
}

EVENT_ID EventMap::EventId(size_t id) {
	if (id >= (size_t)BAD_EVENT_ID) {
		LogError("EventId() >= BAD_EVENT_ID, possible loss of data");
	}
	return (EVENT_ID)id;
}

const EventMap& EventMap::streamTo(IWriteableBuffer& buffer) const {
	// pack it
	buffer << typeid(*IEvent::EncodingFactory()).name();
	buffer << IEncodingFactory::Version();

	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	buffer << (ULONG)m_registry.size();
	for (auto i = m_registry.begin(); i != m_registry.end(); i++) {
		buffer << i->name.c_str();
		buffer << i->m_version;
	}

	return *this;
}

// on clients, reconciles our already loaded local registry with
// some info from the remote server.
// The remoteRegistry will only have the name and id, nothing else
// the jist here is that the ids of the events on the server must match those
// on the client
bool EventMap::ReconcileRemoteRegistry(EventRegistry& remoteRegistry) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	// first find any not in our list and add "auto-register" them
	// as generic so only the server-side need create the event
	for (size_t j = 0; j < remoteRegistry.size(); j++) {
		bool found = false;
		for (size_t i = 0; i < m_registry.size(); i++) {
			if (StrSameIgnoreCase(m_registry[i].name, remoteRegistry[j].name)) {
				found = true;
				break;
			}
		}
		if (!found) {
			// auto register it
			RegisterEvent(remoteRegistry[j].name.c_str(), remoteRegistry[j].m_version, GenericEvent::CreateEvent, 0, remoteRegistry[j].m_priority);
		}
	}

	if (m_localToServerMapping != 0)
		delete[] m_localToServerMapping;
	if (m_serverToLocalMapping != 0)
		delete[] m_serverToLocalMapping;

	m_ltsCount = m_registry.size();
	m_localToServerMapping = new int[m_ltsCount];
	memset(m_localToServerMapping, 0xffffffff, sizeof(int) * m_ltsCount);

	m_stlCount = remoteRegistry.size();
	m_serverToLocalMapping = new int[m_stlCount];
	memset(m_serverToLocalMapping, 0xffffffff, sizeof(int) * m_stlCount);

	for (size_t j = 0; j < m_ltsCount; j++) {
		for (size_t i = 0; i < m_stlCount; i++) {
			if (StrSameIgnoreCase(m_registry[j].name, remoteRegistry[i].name)) {
				m_localToServerMapping[j] = i;
				m_serverToLocalMapping[i] = j;
				break;
			}
		}
	}

	return true;
}

EventHandlerInfo* EventMap::handlers(EVENT_ID eventId, unsigned int index) {
	if (!isValid(eventId))
		throw RangeException(SOURCELOCATION);

	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	EventHandlerVect& handlers = m_registry[eventId].m_handlers;
	size_t nsize = handlers.size();
	if (index >= nsize)
		throw RangeException(SOURCELOCATION);

	return &(handlers[index]);
}

const EventHandlerVect& EventMap::handlers(EVENT_ID eventId) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);
	return m_registry[eventId].m_handlers;
}

EVENT_ID EventMap::GetEventId(const std::string& name) const {
	if (name.empty())
		return BAD_EVENT_ID;

	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	EVENT_ID id = BAD_EVENT_ID;
	size_t events = m_registry.size();
	for (size_t i = 0; i < events; i++) {
		if (StrSameIgnoreCase(m_registry[i].name, name)) {
			id = EventId(i); // DRF - possible loss of data
			break;
		}
	}

	if (id == BAD_EVENT_ID)
		LogWarn("Event name not registered: '" << name << "'");

	return id;
}

EVENT_ID EventMap::RegisterEvent(
	const char* name,
	ULONG version,
	CREATE_EVENT_FN fn,
	SET_EVENT_ID_FN setfn,
	EVENT_PRIORITY prio) {
	std::unique_lock<fast_recursive_mutex> lock(m_registryMutex);

	// Event Name Already Registered ?
	EVENT_ID id = BAD_EVENT_ID;
	size_t events = m_registry.size();
	for (size_t i = 0; i < events; i++) {
		if (!StrSameIgnoreCase(m_registry[i].name, name))
			continue;
		id = (version == m_registry[i].m_version) ? EventId(i) : BAD_EVENT_ID;
		break;
	}
	if (id != BAD_EVENT_ID)
		return id;

	// Register New Event Name If Not Found
	id = GlobalEventRegister(name); // return index

	// Should Be Last Event
	events = m_registry.size();
	EVENT_ID localId = EventId(events);
	if (localId != id) {
		// fill in our vector to make ids match
		for (size_t i = localId; i < id; i++)
			m_registry.push_back(EventRegistryItem("~~~PLACEHOLDER~~~", PackVersion(0, 0, 0, 0), 0, 0, 0));
	}

	m_registry.push_back(EventRegistryItem(name, version, fn, setfn, prio));

	lock.unlock(); // unlock before logging

#ifdef _DEBUG
	// DRF - Added As DRF_TEST_GROUP_DRF_PREFS
	if (!TestGroupEventMap()) {
		LogInfo(id << " " << name);
	}
#endif

	return id;
}

bool EventMap::RegisterHandler(ULONG eventVersion, EVENT_ID eventId, EVENT_PRIORITY priority, const std::string& /*name*/, EventHandlerInfo info) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	if (!VersionEqual(m_registry[eventId].m_version, eventVersion, 2)) {
		std::string s, s2;
		LogError("MISMATCH - " << VersionToString(m_registry[eventId].m_version, s) << " != " << VersionToString(eventVersion, s2));
		return false;
	}

	// find out where to put it, higher priority items go first in the list
	// if equal, first one added is first
	bool found = false;
	for (auto i = m_registry[eventId].m_handlers.begin(); i != m_registry[eventId].m_handlers.end(); i++) {
		if (i->m_priority < priority) {
			m_registry[eventId].m_handlers.insert(i, info);
			found = true;
			break;
		}
	}

	if (!found)
		m_registry[eventId].m_handlers.push_back(info);

	return true;
}

bool EventMap::RemoveHandler(IEventHandler* eh, EVENT_ID eventId) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	size_t events = m_registry.size();
	if (eh == 0 || eventId == BAD_EVENT_ID || eventId >= events) {
		LogError("Bad parameter passed to unregister handler: " << (LONG)eh << " " << eventId << " " << events);
		return false;
	}

	return RemoveHandler(eh, 0, typeid(*eh).name(), eventId);
}

bool EventMap::RemoveHandler(HANDLE_EVENT_FN fn, ULONG lparam, const char* name, EVENT_ID eventId) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	size_t events = m_registry.size();
	if (fn == 0 || eventId == BAD_EVENT_ID || eventId >= events) {
		LogError("Bad parameter passed to unregister handler: " << (LONG)fn << " " << eventId << " " << events);
		return false;
	}

	return RemoveHandler((void*)fn, lparam, name, eventId);
}

bool EventMap::RemoveHandler(void* item, ULONG lparam, const std::string& /*name*/, EVENT_ID eventId) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	if (eventId == BAD_EVENT_ID) {
		// remove for all events
		for (auto j = m_registry.begin(); j != m_registry.end(); j++) {
			for (auto i = j->m_handlers.begin(); i != j->m_handlers.end(); i++) {
				if ((*i).HasItem(item, lparam)) {
					j->m_handlers.erase(i);
					break;
				}
			}
		}
		return true;
	} else {
		for (auto i = m_registry[eventId].m_handlers.begin(); i != m_registry[eventId].m_handlers.end(); i++) {
			if ((*i).HasItem(item, lparam)) {
				m_registry[eventId].m_handlers.erase(i);
				return true;
			}
		}
	}

	return false;
}

bool EventMap::RemapLocalToServerEventId(IEvent* e) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	// Remap Local To Server Event Id
	EVENT_ID origId = e->EventId();
	std::string name = GetEventName(origId);
	if (m_localToServerMapping != 0) {
		if (origId >= m_ltsCount) {
			LogError("FAILED local eventId=" << origId << " '" << name << "' > localToServerCount=" << m_ltsCount);
			return false;
		}
		int intEventId = m_localToServerMapping[origId];
		if (intEventId < 0) {
			LogError("FAILED localToServerMapping for local eventId=" << origId << " '" << name << "'");
			return false;
		}
		EVENT_ID eventId = EventId((size_t)intEventId); // DRF - possible loss of data
		e->SetEventId(eventId);
	}

	size_t events = m_registry.size();
	if (e->EventId() < 1 || e->EventId() >= events) {
		LogError("FAILED local eventId=" << origId << " '" << name << "' -> server eventId=" << e->EventId() << " >= events=" << events);
		return false;
	}

	return true;
}

bool EventMap::RemapServerToLocalEventId(EventHeader* e) const {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	// Remap Server To Local Event Id
	EVENT_ID origId = e->EventId();
	if (m_serverToLocalMapping != 0) {
		if (origId >= m_stlCount) {
			LogError("Invalid server eventId=" << origId << " > serverToLocalCount=" << m_stlCount);
			return false;
		}
		int intEventId = m_serverToLocalMapping[origId];
		if (intEventId < 0) {
			LogError("Invalid serverToLocalMapping for server eventId=" << origId);
			return false;
		}
		EVENT_ID eventId = EventId((size_t)intEventId); // DRF - possible loss of data
		e->SetEventId(eventId);
	}

	size_t events = m_registry.size();
	if (e->EventId() >= events) {
		LogError("FAILED local eventId=" << origId << " -> server eventId=" << e->EventId() << " >= events=" << events);
		return false;
	}

	return true;
}

IEvent* EventMap::MakeEvent(EVENT_ID eventId, NETID from) const {
	if (eventId == BAD_EVENT_ID)
		return nullptr;

	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	size_t events = m_registry.size();
	if (eventId >= events)
		return 0;

	if (!m_registry[eventId].m_pFuncCreate)
		return nullptr;

	return m_registry[eventId].m_pFuncCreate(eventId, 0, 0, 0, from, m_registry[eventId].m_priority);
}

// DRF - Remaps server events --> client events
IEvent* EventMap::MakeEvent(PCBYTE buffer, ULONG len, NETID from) const {
	if (len < sizeof(EventHeader))
		return nullptr;

	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	// Remap Server To Local Event Id
	EventHeader* eh = (EventHeader*)buffer;
	if (!RemapServerToLocalEventId(eh)) {
		LogError("FAILED RemapServerToLocalEventId()");
		return nullptr;
	}

	if (!m_registry[eh->EventId()].m_pFuncCreate)
		return nullptr;

	// creating from incoming network buffer
	return m_registry[eh->EventId()].m_pFuncCreate(eh->EventId(), eh, buffer + sizeof(EventHeader), len - sizeof(EventHeader), from, m_registry[eh->EventId()].m_priority);
}

bool EventMap::HasHandler(EVENT_ID eventId, ULONG filter, OBJECT_ID filteredObjectId) {
	std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);

	size_t events = m_registry.size();
	if (eventId < events && m_registry[eventId].m_handlers.size() > 0) {
		if (filter == 0 && filteredObjectId == 0) {
			return true;
		} else {
			for (const auto& ehi : m_registry[eventId].m_handlers) {
				if (ehi.CanProcess(filter, filteredObjectId))
					return true;
			}
		}
	}

	return false;
}

} // namespace KEP