///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "event/base/IWriteableBuffer.h"
#include "event/base/ievent.h"
#include "event/base/EventRegistry.h"
#include "tools/networkV2/inetwork.h"
#include "Core/Util/fast_mutex.h"

namespace KEP {

class EventMap {
	// Disallow Copy
	EventMap::EventMap(const EventMap& other) {}
	EventMap& operator=(const EventMap& rhs) {}

public:
	static void SetTestGroup(int testGroup);

	// DRF - Converts (size_t) to EVENT_ID (USHORT)
	static EVENT_ID EventId(size_t id);

	EventMap() :
			m_localToServerMapping(0), m_serverToLocalMapping(0), m_ltsCount(0), m_stlCount(0) {}

	virtual ~EventMap() {
		if (m_localToServerMapping)
			delete[] m_localToServerMapping;
		if (m_serverToLocalMapping)
			delete[] m_serverToLocalMapping;
	}

	/**
	 * Streams the contents of this event map into an IWriteableBuffer.
	 */
	const EventMap& streamTo(IWriteableBuffer& buffer) const;

	size_t Events() const {
		std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);
		return m_registry.size();
	}

	EVENT_ID GetEventId(const std::string& name) const;

	std::string GetEventName(EVENT_ID id, ULONG* version = NULL) {
		std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);
		if (id >= Events())
			return "";
		if (version)
			*version = m_registry[id].m_version;
		return m_registry[id].name;
	}

	IEvent* MakeEvent(PCBYTE buffer, ULONG len, NETID from = SERVER_NETID) const;

	IEvent* MakeEvent(EVENT_ID id, NETID from = SERVER_NETID) const;

	bool ReconcileRemoteRegistry(EventRegistry& remoteRegistry);

	/** DRF
	 * Remaps the local event id to the associated server event id using m_localToServerMapping[].
	 */
	bool RemapLocalToServerEventId(IEvent* e);

	/** DRF
	 * Remaps the server event id to the associated local event id using m_serverToLocalMapping[].
	 */
	bool RemapServerToLocalEventId(EventHeader* e) const;

	/**
	 * Determines if the event id is BAD_EVENT_ID or the id (which is just an
	 * index into the registry), is within the range of the registry indexes.
	 */
	bool isValid(EVENT_ID eventId) const {
		return ((eventId != BAD_EVENT_ID) && (eventId < Events()));
	}

	bool RegisterHandler(ULONG eventVersion, EVENT_ID eventId, EVENT_PRIORITY priority, const std::string& name, EventHandlerInfo info);

	EVENT_ID RegisterEvent(
		const char* name,
		ULONG version,
		CREATE_EVENT_FN fn,
		SET_EVENT_ID_FN setfn,
		EVENT_PRIORITY prio);

	bool RemoveHandler(void* item, ULONG lparam, const std::string& name, EVENT_ID eventId);

	bool RemoveHandler(HANDLE_EVENT_FN fn, ULONG lparam, const char* name, EVENT_ID eventId);

	bool RemoveHandler(IEventHandler* eh, EVENT_ID eventId);

	bool HasHandler(EVENT_ID eid, ULONG filter, OBJECT_ID filteredObjectId);

	EVENT_PRIORITY priority(EVENT_ID eventId) const {
		std::lock_guard<fast_recursive_mutex> lock(m_registryMutex);
		return m_registry[eventId].m_priority;
	}

	EventHandlerInfo* handlers(EVENT_ID eventId, unsigned int index);

	const EventHandlerVect& handlers(EVENT_ID eventId);

private:
	EventRegistry m_registry;
	int* m_localToServerMapping;
	int* m_serverToLocalMapping;
	size_t m_ltsCount;
	size_t m_stlCount;
	USHORT m_dispType; // for checking valid send/receive

	mutable fast_recursive_mutex m_registryMutex;
};

} // namespace KEP