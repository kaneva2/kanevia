///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "IEvent.h"

namespace KEP {

IEncodingFactory* IEvent::m_encodingFactory = 0;

void IEvent::CheckBuffer() {
	if (m_outBuffer == 0 && m_inBuffer && m_inBuffer->BufferToFree() == 0) {
		// need to copy the buffer
		PCBYTE buffer;
		ULONG len;
		m_inBuffer->GetBuffer(buffer, len);

		// Then delete the ReadableBuffer.  N.B. this is a call to Delete() is not to be confused
		// with delete.  This method checks to see if a "buffer" to be freed has been specified,
		// deletes it if one was has been, then calls delete self.
		// Note that it does NOT free the buffer to which we just acquired a reference.
		//
		m_inBuffer->Delete();

		// copy the buffer
		BYTE* newBuffer = 0;
		if (len > 0) {
			newBuffer = new BYTE[len];
			memcpy(newBuffer, buffer, len);
		}

		// Use this copy of the buffer to create a new ReadableBuffer.
		//
		m_inBuffer = m_encodingFactory->CreateReadable(newBuffer, len);
		// Tell the ReadableBuffer, that it can manage it's own deletion.
		m_inBuffer->FreeOnDelete(newBuffer);
	}
}

} // namespace KEP
