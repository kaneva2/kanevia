#pragma once

#include "Event/Base/IEvent.h"
#include "Event/Base/IEventHandler.h"
#include <string>
#include <vector>

namespace KEP {

class EventHandlerInfo {
public:
	EventHandlerInfo(
		HANDLE_EVENT_FN f,
		const char* nm,
		ULONG lp,
		EVENT_PRIORITY p,
		ULONG _filter = 0,
		bool _filterIsMask = false,
		OBJECT_ID _filteredObjectId = 0) :
			m_pFunc(f),
			m_lparam(lp),
			m_priority(p),
			m_name(nm != nullptr ? nm : ""),
			m_filter(_filter),
			m_filterIsMask(_filterIsMask),
			m_filteredObjectId(_filteredObjectId) {
	}

	EventHandlerInfo(
		const std::shared_ptr<IEventHandler>& pEH,
		EVENT_PRIORITY p,
		ULONG _filter = 0,
		bool _filterIsMask = false,
		OBJECT_ID _filteredObjectId = 0) :
			m_pEH(pEH),
			m_priority(p),
			m_pFunc(0),
			m_lparam(0),
			m_name(typeid(*pEH).name()),
			m_filter(_filter),
			m_filterIsMask(_filterIsMask),
			m_filteredObjectId(_filteredObjectId) {
	}

	~EventHandlerInfo() {}

	HANDLE_EVENT_FN m_pFunc;

	ULONG m_lparam;

	std::shared_ptr<IEventHandler> m_pEH;

	EVENT_PRIORITY m_priority;

	ULONG m_filter;
	bool m_filterIsMask;
	OBJECT_ID m_filteredObjectId;

	std::string m_name; // mainly for logging (mostly unused)

	EventHandlerInfo(const EventHandlerInfo& src) {
		operator=(src);
	}

	EventHandlerInfo& operator=(const EventHandlerInfo& src) {
		if (this != &src) {
			m_pFunc = src.m_pFunc;
			m_name = src.m_name;
			m_lparam = src.m_lparam;
			m_pEH = src.m_pEH;
			m_priority = src.m_priority;
			m_filter = src.m_filter;
			m_filterIsMask = src.m_filterIsMask;
			m_filteredObjectId = src.m_filteredObjectId;
		}
		return *this;
	}

	// used when removing items from arry to remove a fn or eh for code reuse
	bool HasItem(void* item, IN ULONG lp) const {
		return (m_pFunc == item && m_lparam == lp) || m_pEH.operator->() == item;
	}

	// can we process this event?  Does is pass any filtering?
	bool CanProcess(ULONG _filter, OBJECT_ID _filteredObjectId) const {
		if (m_filter != 0 &&
			((m_filterIsMask && (_filter & m_filter) != m_filter) ||
				!m_filterIsMask && _filter != m_filter))
			return false;

		if (m_filteredObjectId != 0 && m_filteredObjectId != _filteredObjectId)
			return false;

		return true;
	}

	// process the event by calling the class or fn ptr
	EVENT_PROC_RC ProcessEvent(IDispatcher* disp, IEvent* e) {
		if (!CanProcess(e->Filter(), e->ObjectId()))
			return EVENT_PROC_RC::NOT_PROCESSED;

		if (m_pFunc)
			return m_pFunc(m_lparam, disp, e);

		if (m_pEH)
			return m_pEH->ProcessEvent(disp, e);

		return EVENT_PROC_RC::NOT_PROCESSED;
	}
};

typedef std::vector<EventHandlerInfo> EventHandlerVect;

class EventRegistryItem {
public:
	EventRegistryItem(
		const char* n = "",
		ULONG v = 0,
		CREATE_EVENT_FN c = 0,
		SET_EVENT_ID_FN s = 0,
		EVENT_PRIORITY p = EP_NORMAL) :
			name(n),
			m_version(v), m_pFuncCreate(c), m_priority(p) {
	}

	EventRegistryItem(const EventRegistryItem& src) {
		operator=(src);
	}

	~EventRegistryItem() {}

	EventRegistryItem& operator=(const EventRegistryItem& src) {
		if (this != &src) {
			name = src.name;
			m_pFuncCreate = src.m_pFuncCreate;
			m_version = src.m_version;
			m_priority = src.m_priority;
			m_handlers = src.m_handlers;
		}
		return *this;
	}

	std::string name;
	EventHandlerVect m_handlers;
	ULONG m_version;
	EVENT_PRIORITY m_priority;
	CREATE_EVENT_FN m_pFuncCreate;
};

typedef std::vector<EventRegistryItem> EventRegistry;

} // namespace KEP
