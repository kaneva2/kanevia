#pragma once

#include <SDKCommon.h>

#include <memory>
#include <string>

#include "IGame.h"
#include "IEvent.h"
#include "IEventHandler.h"
#include "IEncodingFactory.h"

namespace KEP {

class IGetSet;

class IDispatcher {
public:
	// version of this class interface
	static BYTE InterfaceVersion() {
		return 1;
	}

	// used during init
	virtual EVENT_ID RegisterEvent(
		const char* name,
		ULONG version,
		CREATE_EVENT_FN fn,
		SET_EVENT_ID_FN setfn,
		EVENT_PRIORITY prio) = 0;

	virtual EVENT_ID GetEventId(const std::string& name) = 0;

	virtual std::string GetEventName(EVENT_ID id, ULONG* version = NULL) = 0;

	virtual bool RegisterHandler(
		const std::shared_ptr<IEventHandler>& pEH,
		ULONG eventVersion,
		EVENT_ID eventId,
		EVENT_PRIORITY priority = EP_NORMAL) = 0;

	virtual bool RegisterHandler(
		HANDLE_EVENT_FN fn,
		ULONG lparam,
		const char* name,
		ULONG eventVersion,
		EVENT_ID eventId,
		EVENT_PRIORITY priority = EP_NORMAL) = 0;

	virtual bool RegisterFilteredHandler(
		const std::shared_ptr<IEventHandler>& pEH,
		ULONG eventVersion,
		EVENT_ID eventId,
		ULONG filter = 0,
		bool filterIsMask = false,
		OBJECT_ID filteredObjectId = 0,
		EVENT_PRIORITY priority = EP_NORMAL) = 0;

	virtual bool RegisterFilteredHandler(
		HANDLE_EVENT_FN fn,
		ULONG lparam,
		const char* name,
		ULONG eventVersion,
		EVENT_ID eventId,
		ULONG filter = 0,
		bool filterIsMask = false,
		OBJECT_ID filteredObjectId = 0,
		EVENT_PRIORITY priority = EP_NORMAL) = 0;

	virtual bool HasHandler(EVENT_ID, ULONG filter = 0, OBJECT_ID filteredObjectId = 0) = 0;

	virtual bool RemoveHandler(IEventHandler* eh, EVENT_ID eventId) = 0;

	virtual bool RemoveHandler(HANDLE_EVENT_FN fn, ULONG lparam, const char* name, EVENT_ID eventId) = 0;

	ULONG QueueEvent(IEvent* e) {
		return QueueEventInFutureMs(e, (TimeMs)0.0);
	}
	virtual ULONG QueueEventInFutureMs(IEvent* e, TimeMs timeMs) = 0;

	virtual IEvent* MakeEvent(PCBYTE buffer, ULONG len, NETID from = SERVER_NETID) = 0;
	virtual IEvent* MakeEvent(EVENT_ID id, NETID from = SERVER_NETID) = 0;

	virtual bool ProcessSynchronousEvent(IEvent* e) = 0;
	virtual bool ProcessSynchronousEventNoRelease(IEvent* e) = 0;

	bool RescheduleEvent(IEvent* e) {
		return RescheduleEventInFutureMs(e, (TimeMs)0.0);
	}
	virtual bool RescheduleEventInFutureMs(IEvent* e, TimeMs timeMs) = 0;

	virtual bool CancelEvent(IEvent* e) = 0;

	virtual IEncodingFactory* EncodingFactory() const = 0;

	virtual void LogMessage(int logType, const std::string& msg) = 0;

	virtual void LogAlertMessage(const std::string& msg) = 0;

	// game may be null, if on Editor, etc.
	IGame* Game() {
		return m_game;
	}
	void SetGame(IGame* game) {
		m_game = game;
	}

protected:
	IDispatcher(void) :
			m_game(0) {
	}

	virtual ~IDispatcher(void) {
	}

	IGame* m_game;
};

template <class event>
void RegisterEvent(IDispatcher& dispatcher) {
	event::SetClassId(dispatcher.RegisterEvent(event::ClassName(), event::ClassVersion(), event::CreateEvent, event::SetClassId, event::ClassPriority()));
}

} // namespace KEP
