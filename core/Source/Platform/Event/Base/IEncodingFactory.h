/******************************************************************************
 IEncodingFactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "IReadableBuffer.h"
#include "IWriteableBuffer.h"

namespace KEP {

/***********************************************************
CLASS

	IEncodingFactory

	factory for getting the method of encoding events

DESCRIPTION

***********************************************************/
class IEncodingFactory {
public:
	static inline ULONG Version() {
		return PackVersion(1, 0, 0, 0);
	}

	virtual IReadableBuffer* CreateReadable(IN PCBYTE buffer, IN ULONG len) = 0;
	virtual IReadableBuffer* CreateReadable(IN IWriteableBuffer* w) = 0;
	virtual IWriteableBuffer* CreateWriteable() = 0;
	virtual void Delete() {
		delete this;
	}

protected:
	virtual ~IEncodingFactory() {}
};

} // namespace KEP

