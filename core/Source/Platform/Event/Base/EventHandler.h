#pragma once

#include "IScriptingEventHandler.h"
#include "IEventBlade.h"
#include <map>
#include "Core/Util/Unicode.h"

namespace KEP {

class EventHandler : public IScriptingEventHandler, public IEventBlade {
public:
	virtual ~EventHandler() {}

	// to allow scripts to log
	void LogMessage(int logType, const char* s);

	// IBlade overrides
	virtual ULONG Version() const {
		return PackVersion(1, 0, 0, 0);
	}

	// PathBase is scriptPath For Event Blades
	virtual std::string PathBase() const {
		return m_scriptDir;
	}
	virtual void SetPathBase(const std::string& baseDir, bool readonly) {
		m_scriptDir = baseDir;
		m_scriptDirReadOnly = readonly;
	}

	virtual void Delete() {
		delete this;
	}
	virtual void FreeAll() {}
	virtual const char* Name() const {
		return m_name.c_str();
	}

protected:
	EventHandler(const char* /*logName*/) :
			m_scriptDirReadOnly(false) {}

	virtual bool IsBaseDirReadOnly() const {
		return m_scriptDirReadOnly;
	}

	// for IBlade support
	std::string m_name;

private:
	std::string m_scriptDir;
	bool m_scriptDirReadOnly;
};

} // namespace KEP
