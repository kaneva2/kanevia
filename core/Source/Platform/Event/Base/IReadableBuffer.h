#pragma once

#include <string>
#include "SDKCommon.h"

namespace boost {
template <typename T>
class optional;
}

namespace KEP {

class IWriteableBuffer;

class IReadableBuffer {
public:
	// caller will allocate string
	virtual void ReadString(IN char* s, IN ULONG maxLen, INOUT ULONG& len) = 0;
	virtual void ReadString(std::string& s) = 0;

	virtual void GetBuffer(OUT PCBYTE& buffer, OUT ULONG& len) = 0;
	virtual void GetBufferLeft(OUT PCBYTE& buffer, OUT ULONG& len) = 0;

	// calling will allocate string
	virtual IReadableBuffer& operator>>(std::string& s) = 0;

	virtual IReadableBuffer& operator>>(int& i) = 0;
	virtual IReadableBuffer& operator>>(unsigned int& val) = 0;
	virtual IReadableBuffer& operator>>(LONG& val) = 0;
	virtual IReadableBuffer& operator>>(DOUBLE& val) = 0;
	virtual IReadableBuffer& operator>>(FLOAT& val) = 0;
	virtual IReadableBuffer& operator>>(SHORT& val) = 0;
	virtual IReadableBuffer& operator>>(ULONG& val) = 0;
	virtual IReadableBuffer& operator>>(USHORT& val) = 0;
	virtual IReadableBuffer& operator>>(bool& b) = 0;
	virtual IReadableBuffer& operator>>(LONGLONG& val) = 0;
	virtual IReadableBuffer& operator>>(ULONGLONG& val) = 0;

	// get a raw bunch of bytes, could be a nested event
	// buffer, etc.  returns pointer in buff decode buffer
	virtual bool DecodeBuffer(OUT PCBYTE& buffer, OUT ULONG& len) = 0;

	// get total number of bytes left, not too useful except as bool
	virtual ULONG BytesLeft() = 0;

	virtual void Rewind() = 0;

	virtual void Delete() {
		delete this;
	}

	// call delete on this pointer when we go away
	// used when event created from buffer and we need to free
	// the buffer when event goes away
	void FreeOnDelete(PBYTE b) {
		m_freeOnDelete = b;
	}
	PBYTE BufferToFree() {
		return m_freeOnDelete;
	}

	// init this from a writable buffer object for the case
	// of locally marshalled events
	virtual bool SetFromWriteable(IWriteableBuffer* writeable) = 0;

protected:
	IReadableBuffer(void) :
			m_freeOnDelete(0) {
	}

	virtual ~IReadableBuffer(void) {
		if (m_freeOnDelete)
			delete m_freeOnDelete;
	}

	PBYTE m_freeOnDelete;
};

template <size_t N, typename T>
class Vector;
template <size_t N, typename T>
inline IReadableBuffer& operator>>(IReadableBuffer& buffer, Vector<N, T>& val) {
	for (size_t i = 0; i < N; ++i)
		buffer >> val[i];
	return buffer;
}

template <typename T>
inline IReadableBuffer& operator>>(IReadableBuffer& buffer, boost::optional<T>& val) {
	bool b = val.is_initialized();
	buffer >> b;
	if (b) {
		T t;
		buffer >> t;
		val = std::move(t);
	} else {
		val.reset();
	}

	return buffer;
}

inline void StreamFrom(IReadableBuffer& rb) {}
template <typename NextArg, typename... Args>
inline void StreamFrom(IReadableBuffer& rb, NextArg& nextArg, Args&... args) {
	rb >> nextArg;
	StreamFrom(rb, args...);
}

} // namespace KEP
