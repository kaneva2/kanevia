///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "EventHandler.h"
#include "KEPHelpers.h"

#include "Event/Base/EventStrings.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

void EventHandler::LogMessage(int logType, const char* s) {
	switch (logType) {
		case log4cplus::TRACE_LOG_LEVEL:
			_LogTrace(s);
			break;
		case log4cplus::DEBUG_LOG_LEVEL:
			_LogDebug(s);
			break;
		case log4cplus::INFO_LOG_LEVEL:
			_LogInfo(s);
			break;
		case log4cplus::WARN_LOG_LEVEL:
			_LogWarn(s);
			break;
		case log4cplus::ERROR_LOG_LEVEL:
			_LogError(s);
			break;
		case log4cplus::FATAL_LOG_LEVEL:
			_LogFatal(s);
			break;
	}
}

} // namespace KEP
