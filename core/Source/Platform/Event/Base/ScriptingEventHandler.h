#pragma once

#include "Event/Base/EventHandler.h"
#include "EventHeader.h"
#include <map>
#include <string>
#include "EventHeader.h"

namespace KEP {

typedef void (*RegistrationFn)(IScriptVM*);

class ScriptingEventHandler : public EventHandler {
public:
	/** DRF - Menu Scripts
	 * This function is called 1st.  It calls menu script KEP_PreInitialize() function defined in KEP.lua for every script.
	 * This is used to automatically get all script globals before initialization.
	 */
	bool PreInitialize(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath);

	/** DRF - Menu Scripts
	 * This function is called 2nd. It calls menu script InitializeKEPEvents() function optionally defined in each script.
	 */
	bool RegisterScriptingEvents(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath);

	/** DRF - Menu Scripts
	 * This function is called 3rd. It calls menu script InitializeKEPEventHandlers() function optionally defined in each script.
	 */
	bool RegisterScriptingEventHandlers(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath);

	/** DRF - Menu Scripts
	 * This function is called 4th.  It calls menu script KEP_PostInitialize() function defined in KEP.lua for every script.
	 * This is used to automatically get all script functions after initialization.
	 */
	bool PostInitialize(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath);

	/** DRF - Client Scripts
	 * This function is called 1st.  It calls client script KEP_PreInitialize() function defined in KEP.lua for every script.
	 * This is used to automatically get all script globals before initialization.
	 */
	virtual bool PreInitialize(IN IDispatcher* dispatcher, IN IEncodingFactory* factory);

	/** DRF - Client Scripts
	 * This function is called 2nd. It calls client script InitializeKEPEvents() function optionally defined in each script.
	 */
	virtual bool RegisterEvents(IN IDispatcher* dispatcher, IN IEncodingFactory* factory);

	/** DRF - Client Scripts
	 * This function is called 3rd. It calls client script InitializeKEPEventHandlers() function optionally defined in each script.
	 */
	virtual bool RegisterEventHandlers(IN IDispatcher* dispatcher, IN IEncodingFactory* factory);
	virtual bool ReRegisterEventHandlers(IN IDispatcher* dispatcher, IN IEncodingFactory* factory);

	/** DRF - Client Scripts
	 * This function is called 4th.  It calls client script KEP_PostInitialize() function defined in KEP.lua for every script.
	 * This is used to automatically get all script functions after initization.
	 */
	virtual bool PostInitialize(IN IDispatcher* dispatcher, IN IEncodingFactory* factory);

	virtual bool callRegisterFnForEachScript(IN const char* fnName, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, bool vmKeepIfFnExists) = 0;
	virtual bool callRegisterFnForVM(IN const char* fnName, IN IScriptVM* vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, IN std::string filePath) = 0;

	virtual void FreeAll() {}

protected:
	ScriptingEventHandler(IN const char* logName) :
			EventHandler(logName), m_dispatcher(0) {}

	void RegisterGetFunctionHandlers(IN IDispatcher* dispatcher, IN HANDLE_EVENT_FN fn, IN const char* namePrefix, IN ULONG lparam);

	// function registration helpers
	EVENT_PROC_RC GetFunctions(IN const char* name, IN IScriptVM* pVm);
	bool FunctionRegistration(IN IScriptVM& vm);
	virtual void ParentVMRegister(IScriptVM*) = 0;

	IDispatcher* m_dispatcher;
};

// Called On Menu Scripts 1st During Initialization
inline bool ScriptingEventHandler::PreInitialize(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) {
	return callRegisterFnForVM("KEP_PreInitialize", &vm, dispatcher, factory, filePath); // defined in KEP.lua for all scripts
}

// Called On Menu Scripts 2nd During Initialization
inline bool ScriptingEventHandler::RegisterScriptingEvents(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) {
	return callRegisterFnForVM("InitializeKEPEvents", &vm, dispatcher, factory, filePath); // optionally defined in each script
}

// Called On Menu Scripts 3rd During Initialization
inline bool ScriptingEventHandler::RegisterScriptingEventHandlers(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) {
	return callRegisterFnForVM("InitializeKEPEventHandlers", &vm, dispatcher, factory, filePath); // optionally defined in each script
}

// Called On Menu Scripts 4th During Initialization
inline bool ScriptingEventHandler::PostInitialize(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) {
	return callRegisterFnForVM("KEP_PostInitialize", &vm, dispatcher, factory, filePath); // defined in KEP.lua for all scripts
}

// Called On Client Scripts 1st During Initialization
inline bool ScriptingEventHandler::PreInitialize(IN IDispatcher* dispatcher, IN IEncodingFactory* factory) {
	// Keep VM If Script Only Has KEP_PreInitialize Defined And No Event Handlers
	return callRegisterFnForEachScript("KEP_PreInitialize", dispatcher, factory, true);
}

// Called On Client Scripts 2nd During Initialization
inline bool ScriptingEventHandler::RegisterEventHandlers(IN IDispatcher* dispatcher, IN IEncodingFactory* factory) {
	// Keep VM If Script Has Event Handlers Defined
	return callRegisterFnForEachScript("InitializeKEPEventHandlers", dispatcher, factory, true);
}

// Called On Client Scripts During Rezone
inline bool ScriptingEventHandler::ReRegisterEventHandlers(IN IDispatcher* dispatcher, IN IEncodingFactory* factory) {
	// Keep VM If Script Has Event Handlers Defined
	return callRegisterFnForEachScript("InitializeKEPEventHandlers", dispatcher, factory, true);
}

// Called On Client Scripts 3rd During Initialization
inline bool ScriptingEventHandler::PostInitialize(IN IDispatcher* dispatcher, IN IEncodingFactory* factory) {
	// Don't Keep VM If Script Only Has KEP_PostInitialize Defined And No Event Handlers
	return callRegisterFnForEachScript("KEP_PostInitialize", dispatcher, factory, false);
}

#define GET_ARENA_FN_EVENT ScriptingEventHandler::GetFnEventNames[0]
#define GET_SERVER_FN_EVENT ScriptingEventHandler::GetFnEventNames[1]
#define GET_XWIN_FN_EVENT ScriptingEventHandler::GetFnEventNames[2]
#define GET_AI_FN_EVENT ScriptingEventHandler::GetFnEventNames[3]
#define GET_PLAYER_FN_EVENT ScriptingEventHandler::GetFnEventNames[4]
#define GET_SKILLDB_FN_EVENT ScriptingEventHandler::GetFnEventNames[5]
#define GET_KEPFILE_FN_EVENT ScriptingEventHandler::GetFnEventNames[6]
#define GET_AI_SERVER_FN_EVENT ScriptingEventHandler::GetFnEventNames[7]

/***********************************************************
CLASS

	TScriptingEventHandler

	template base event handler for scripting

DESCRIPTION
	adds map via template parameters

***********************************************************/
template <class fnType, class stateType>
class TScriptingEventHandler : public ScriptingEventHandler {
protected:
	TScriptingEventHandler(IN const char* logName) :
			ScriptingEventHandler(logName) {}

	class HandlerInfo {
	public:
		HandlerInfo(TScriptingEventHandler* p, EVENT_ID i, const fnType& f, const stateType& v) :
				parent(p), id(i), fn(f), vm(v) {}
		HandlerInfo(const HandlerInfo& src) {
			operator=(src);
		}
		~HandlerInfo() {}
		HandlerInfo& operator=(const HandlerInfo& src) {
			if (this != &src) {
				parent = src.parent
							 id = src.id;
				fn = src.fn;
				vm = src.vm;
			}
			return *this;
		}
		TScriptingEventHandler* parent; // the containing object
		EVENT_ID id;
		fnType fn; // string for Lua, PyObject (fn) for Python
		stateType vm;
	};

	typedef std::vector<HandlerInfo*> HandlerInfoVect;

	HandlerInfoVect m_handlers;
};

} // namespace KEP
