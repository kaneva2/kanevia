#pragma once

#include "Engine\Blades\IBlade.h"
#include <memory>
#include <vector>
#include "SDKCommon.h"

namespace KEP {

class IDispatcher;
class IEncodingFactory;
class DBConfig;

class IEventBlade : public IBlade {
public:
	IEventBlade() {}

	virtual ~IEventBlade() {}

	virtual void SetDBConfig(const DBConfig* /*dbConfig*/) {}

	virtual bool RegisterEvents(IDispatcher* dispatcher, IEncodingFactory* factory) = 0;

	virtual bool RegisterEventHandlers(IDispatcher* dispatcher, IEncodingFactory* factory) = 0;

	virtual bool ReRegisterEventHandlers(IDispatcher* dispatcher, IEncodingFactory* factory) = 0;

	// This function is called Pre Initialization, before all blades are initialized.
	virtual bool PreInitialize(IDispatcher* /*dispatcher*/, IEncodingFactory* /*factory*/) {
		return true;
	}

	// This function is called Post Initialization, after all blades are initialized.
	virtual bool PostInitialize(IDispatcher* /*dispatcher*/, IEncodingFactory* /*factory*/) {
		return true;
	}

	static ULONG InterfaceVersion() {
		return PackVersion(1, 0, 0, 0);
	}
};

typedef std::shared_ptr<IEventBlade> IEventBladePtr;
typedef std::vector<IEventBladePtr> EventBladeVector;

} // namespace KEP
