///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <sstream>
#include <limits.h>
#include "common\include\CoreStatic\CStructuresMisl.h"

namespace KEP {

typedef USHORT EVENT_ID;
const EVENT_ID BAD_EVENT_ID = USHRT_MAX;
typedef ULONG OBJECT_ID;

#pragma pack(push, 1)

// MUST BE DERIVED FROM MSG_HEADER FOR NETWORK MESSAGE COMPATIBILITY!
// PASSED OVER NETWORK IN PACKED BYTE ORDER AND DECODED BY MSG_HEADER!
class EventHeader : MSG_HEADER {
	EVENT_ID m_eventId;
	OBJECT_ID m_objectId;
	ULONG m_filter;

public:
	EventHeader() :
			MSG_HEADER(MSG_CAT::EVENT), m_eventId(0), m_objectId(0), m_filter(0) {}

	void SetEventId(EVENT_ID eventId) {
		m_eventId = eventId;
	}
	EVENT_ID EventId() const {
		return m_eventId;
	}

	void SetObjectId(OBJECT_ID objectId) {
		m_objectId = objectId;
	}
	OBJECT_ID ObjectId() const {
		return m_objectId;
	}

	void SetFilter(ULONG filter) {
		m_filter = filter;
	}
	ULONG Filter() const {
		return m_filter;
	}
};
#pragma pack(pop)

} // namespace KEP
