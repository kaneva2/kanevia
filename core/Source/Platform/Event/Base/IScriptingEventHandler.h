#pragma once

#include "IEventHandler.h"

namespace KEP {

class IScriptVM;

class IScriptingEventHandler : public IEventHandler {
public:
	virtual bool PreInitialize(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) = 0; // drf - added
	virtual bool RegisterScriptingEventHandlers(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) = 0;
	virtual bool RegisterScriptingEvents(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) = 0;
	virtual bool PostInitialize(IN IScriptVM& vm, IN IDispatcher* dispatcher, IN IEncodingFactory* factory, std::string filePath) = 0; // drf - added

	virtual bool FunctionRegistration(IN IScriptVM& vm) = 0;
	virtual bool UnRegisterScriptingEventHandlers(IN IDispatcher* dispatcher, IN IScriptVM* vm = 0) = 0;
};

} // namespace KEP
