/******************************************************************************
 IWriteableBuffer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <string>
#include "EventHeader.h"
#include "js.h"

namespace boost {
template <typename T>
class optional;
}

namespace KEP {

class IReadableBuffer;

/***********************************************************
CLASS

	IWriteableBuffer

	class used for serializing data for sending over the
	wire

DESCRIPTION

***********************************************************/
class IWriteableBuffer {
public:
	virtual void GetBuffer(OUT PCBYTE& buffer, OUT ULONG& len) = 0;
	virtual IWriteableBuffer& operator<<(const char* s) = 0;
	virtual IWriteableBuffer& operator<<(const std::string& s) = 0;
	virtual IWriteableBuffer& operator<<(int i) = 0;
	virtual IWriteableBuffer& operator<<(unsigned int i) = 0;
	virtual IWriteableBuffer& operator<<(LONG val) = 0;
	virtual IWriteableBuffer& operator<<(DOUBLE val) = 0;
	virtual IWriteableBuffer& operator<<(FLOAT val) = 0;
	virtual IWriteableBuffer& operator<<(SHORT val) = 0;
	virtual IWriteableBuffer& operator<<(ULONG val) = 0;
	virtual IWriteableBuffer& operator<<(USHORT val) = 0;
	virtual IWriteableBuffer& operator<<(bool b) = 0;
	virtual IWriteableBuffer& operator<<(LONGLONG val) = 0;
	virtual IWriteableBuffer& operator<<(ULONGLONG val) = 0;

	// to nest event inside another
	virtual void EncodeEvent(const EventHeader& header, IReadableBuffer* buff) = 0;

	// a raw bunch of bytes
	virtual void EncodeBuffer(PCBYTE buffer, ULONG len) = 0;

	virtual void Rewind() = 0;

	virtual void Delete() {
		delete this;
	}

	virtual bool SetFromReadable(IReadableBuffer* r) = 0;

	virtual void CopyBuffer(PCBYTE buffer, ULONG len) = 0;

protected:
	IWriteableBuffer(void) {
	}

	virtual ~IWriteableBuffer(void) {
	}
};

template <size_t N, typename T>
class Vector;
template <size_t N, typename T>
inline IWriteableBuffer& operator<<(IWriteableBuffer& buffer, const Vector<N, T>& val) {
	for (size_t i = 0; i < N; ++i)
		buffer << val[i];
	return buffer;
}

template <typename T>
inline IWriteableBuffer& operator<<(IWriteableBuffer& buffer, const boost::optional<T>& val) {
	bool b = val.is_initialized();
	buffer << b;
	if (b)
		buffer << val.get();
	return buffer;
}

inline void StreamTo(IWriteableBuffer& wb) {}
template <typename NextArg, typename... Args>
inline void StreamTo(IWriteableBuffer& wb, const NextArg& nextArg, const Args&... args) {
	wb << nextArg;
	StreamTo(wb, args...);
}

} // namespace KEP

