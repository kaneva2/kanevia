/******************************************************************************
 IArena.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "IKEPObject.h"
#include "Core/Math/Vector.h"

namespace KEP {

class IArena : public IKEPObject {
public:
	// version of this class interface
	static unsigned char Version() {
		return 1;
	}

	virtual int GetSpawnPointCount() = 0;
	virtual Vector3f GetSpawnPoint(int index) = 0;
};

} // namespace KEP

