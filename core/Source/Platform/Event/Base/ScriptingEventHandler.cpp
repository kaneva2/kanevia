///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "ScriptingEventHandler.h"
#include "Event/EventSystem/Dispatcher.h"
#include "Event/Events/GetFunctionEvent.h"
#include "Event/Base/ScriptGlue.h"
#include "config.h"

using namespace KEP;

SG_DEFINE_VM_REGISTER(EventVMRegister)
SG_DEFINE_VM_REGISTER(GameVMRegister)
SG_DEFINE_VM_REGISTER(DispatcherVMRegister)
SG_DEFINE_VM_REGISTER(GetSetVMRegister)
SG_DEFINE_VM_REGISTER(InstanceIdVMRegister)
SG_DEFINE_VM_REGISTER(ClientEngineVMRegister)
SG_DEFINE_VM_REGISTER(ArenaVMRegister)
SG_DEFINE_VM_REGISTER(KEPFileVMRegister)
SG_DEFINE_VM_REGISTER(KEPConfigVMRegister)
SG_DEFINE_VM_REGISTER(UGCImportVMRegister)
class RegInfo {
public:
	const char* name;
	RegistrationFn fn;
};

static RegInfo RegistrationInfo[] = {
	{ "ArenaFunctions", ArenaVMRegister },
	{ "ServerGameFunctions", GameVMRegister },
	{ "XWinFunctions", ClientEngineVMRegister },
	{ "KEPFileFunctions", KEPFileVMRegister },
	{ "KEPConfigFunctions", KEPConfigVMRegister },
	{ "UGCImportFunctions", UGCImportVMRegister },
	{ 0, 0 }
};

// common function to register or re-register handlers for GetFunction events
void ScriptingEventHandler::RegisterGetFunctionHandlers(IDispatcher* dispatcher, HANDLE_EVENT_FN fn, const char* namePrefix, ULONG lparam) {
	for (int i = 0; RegistrationInfo[i].name != 0; i++) {
		dispatcher->RegisterHandler(fn, lparam, (std::string(namePrefix) + RegistrationInfo[i].name).c_str(), GetFunctionEvent::ClassVersion(),
			dispatcher->GetEventId(RegistrationInfo[i].name));
	}
}

bool ScriptingEventHandler::RegisterEvents(IDispatcher* dispatcher, IEncodingFactory* factory) {
	for (int i = 0; RegistrationInfo[i].name != 0; i++) {
		dispatcher->RegisterEvent(RegistrationInfo[i].name, GetFunctionEvent::ClassVersion(), GetFunctionEvent::CreateEvent, 0, GetFunctionEvent::ClassPriority());
	}

	// Keep VM If Script Has Events Defined
	return callRegisterFnForEachScript("InitializeKEPEvents", dispatcher, factory, true);
}

bool ScriptingEventHandler::FunctionRegistration(IScriptVM& vm) {
	EventVMRegister(&vm);
	ParentVMRegister(&vm);
	DispatcherVMRegister(&vm);
	GetSetVMRegister(&vm);
	InstanceIdVMRegister(&vm);

	return true;
}

EVENT_PROC_RC ScriptingEventHandler::GetFunctions(const char* name, IScriptVM* pVm) {
	for (int i = 0; RegistrationInfo[i].name != 0 && RegistrationInfo[i].fn != 0; i++) {
		if (::strcmp(name, RegistrationInfo[i].name) == 0) {
			RegistrationInfo[i].fn(pVm);
			return EVENT_PROC_RC::CONSUMED;
		}
	}
	return EVENT_PROC_RC::NOT_PROCESSED;
}
