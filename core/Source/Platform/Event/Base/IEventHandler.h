///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include "SDKCommon.h"

namespace KEP {

class IEncodingFactory;
class IEvent;
class IDispatcher;

enum class EVENT_PROC_RC {
	OK, // didn't change event
	NOT_PROCESSED, // I don't want this event
	CONSUMED // consumed event, don't let others process it
};

class IEventHandler {
public:
	virtual ~IEventHandler() {}

	// version of this class interface
	static BYTE Version() {
		return 1;
	}

	virtual bool Initialize(IDispatcher* /*disp*/, IEncodingFactory* /*ef*/) {
		return true;
	}

	virtual EVENT_PROC_RC ProcessEvent(IDispatcher* disp, IEvent* e) = 0;

	virtual void Delete() {
		delete this;
	}

protected:
	IEventHandler() {}
};

typedef EVENT_PROC_RC (*HANDLE_EVENT_FN)(ULONG lparam, IDispatcher* disp, IEvent* e);

} // namespace KEP
