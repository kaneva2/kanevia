///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#if defined(LUA_EVENT_HANDLER)
#include "LuaScriptGlue.h"
#elif defined(PYTHON_EVENT_HANDLER)
#include "PythonScriptGlue.h"
#endif
