#pragma once

#include <windows.h>
#include "SDKCommon.h"
#include "KEPNetwork.h"
#include "Core/Util/HighPrecisionTime.h"
#include "IEncodingFactory.h"
#include "EventHeader.h"

#include <string>
#include <vector>

#include <include\Core\Math\Vector.h>
#include <boost/intrusive_ptr.hpp>

namespace KEP {

class IReadableBuffer;
class IWriteableBuffer;
class MTEventQueue;

// priority for events and handlers
typedef BYTE EVENT_PRIORITY;

// some common priority settings
const EVENT_PRIORITY EP_LOW = 10;
const EVENT_PRIORITY EP_NORMAL = 100;
const EVENT_PRIORITY EP_HIGH = 200;

// Event Flags
const BYTE EF_GUARANTEED = 0x01;
const BYTE EF_NOTGUARANTEED = 0x00; // just for readability
const BYTE EF_SENDIMMEDIATE = 0x02; // don't use outbox to send it

class IEvent {
public:
	static BYTE InterfaceVersion() {
		return 1;
	}

	static IEncodingFactory* EncodingFactory() {
		return m_encodingFactory;
	}

	static void SetEncodingFactory(IEncodingFactory* encodingFactory) {
		m_encodingFactory = encodingFactory;
	}

	EVENT_ID EventId() const {
		return m_header.EventId();
	}

	void SetEventId(EVENT_ID eventId) {
		m_header.SetEventId(eventId);
	}

	OBJECT_ID ObjectId() const {
		return m_header.ObjectId();
	}

	void SetObjectId(OBJECT_ID oid) {
		m_header.SetObjectId(oid);
	}

	ULONG Filter() const {
		return m_header.Filter();
	}

	void SetFilter(ULONG f) {
		m_header.SetFilter(f);
	}

	void Timestamp(TimeMs& timeQueued, TimeMs& timeDelta) const {
		timeQueued = m_timeQueued;
		timeDelta = m_timeDelta;
	}

	TimeMs GetTimeToActivate() const {
		return m_timeQueued + m_timeDelta;
	}

	void SetTimestamp(TimeMs timeDelta) {
		m_timeQueued = fTime::TimeMs();
		m_timeDelta = timeDelta;
	}

	EVENT_PRIORITY Priority() const {
		return m_priority;
	}

	ULONG Flags() const {
		return m_flags;
	}

	void SetFlags(ULONG f) {
		m_flags = f;
	}

	// who from/to
	NETID From() const {
		return m_from;
	}

	void SetFrom(NETID from) {
		m_from = from;
	}

	NETIDVect& To() {
		return m_to;
	}

	void AddTo(NETID id) {
		m_to.push_back(id);
	}

	// buffer to read for incoming event, may be null
	IReadableBuffer* InBuffer();
	void RewindInput() {
		if (m_inBuffer)
			m_inBuffer->Rewind();
	}

	// buffer for writing for outgoing event
	IWriteableBuffer* OutBuffer() {
		if (m_outBuffer == 0)
			m_outBuffer = m_encodingFactory->CreateWriteable();
		return m_outBuffer;
	}

	// Dump entire event including header to a buffer
	inline void SerializeToBuffer(std::vector<unsigned char>& buffer);

	// Return internal buffer (NO header prepended)
	inline void GetOutputBufferNoHeader(PCBYTE& buff, ULONG& len);

	void ResetForOutput() {
		if (m_inBuffer) {
			m_inBuffer->Delete();
			m_inBuffer = 0;
		}
		if (m_outBuffer != 0) {
			m_outBuffer->Delete();
			m_outBuffer = 0;
		}
	}

	virtual void Delete() {
		if (m_queuedCount == 0)
			delete this;
	}

	// helper for nesting events
	const EventHeader* Header() const {
		return &m_header;
	}

	void CheckBuffer();

	void AddRef() { incQueuedCnt(); }
	void Release() {
		if (decQueuedCnt() == 0)
			Delete();
	}

protected:
	LONG incQueuedCnt() {
		return InterlockedIncrement(&m_queuedCount);
	}
	LONG decQueuedCnt() {
		return InterlockedDecrement(&m_queuedCount);
	}

	EventHeader m_header;

	static IEncodingFactory* m_encodingFactory;

	IReadableBuffer* m_inBuffer;
	IWriteableBuffer* m_outBuffer;

	TimeMs m_timeQueued;
	TimeMs m_timeDelta;

	NETID m_from;
	NETIDVect m_to;
	ULONG m_flags;
	EVENT_PRIORITY m_priority;
	LONG m_queuedCount;

	IEvent(EVENT_ID eventId, ULONG filter = 0, OBJECT_ID objectId = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			m_inBuffer(0),
			m_outBuffer(0),
			m_timeQueued(0),
			m_timeDelta(0),
			m_from(0),
			m_flags(EF_GUARANTEED),
			m_priority(priority),
			m_queuedCount(0) {
		SetEventId(eventId);
		m_header.SetObjectId(objectId);
		m_header.SetFilter(filter);
		m_timeQueued = fTime::TimeMs();
	}

	IEvent(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from = 0, EVENT_PRIORITY priority = EP_NORMAL) :
			m_inBuffer(0),
			m_outBuffer(0),
			m_timeQueued(0),
			m_timeDelta(0),
			m_from(from),
			m_flags(EF_GUARANTEED),
			m_priority(priority),
			m_queuedCount(0) {
		SetEventId(eventId);
		if (eh && buffer) {
			m_inBuffer = IEvent::EncodingFactory()->CreateReadable(buffer, len);
			m_header.SetEventId(eh->EventId());
			m_header.SetObjectId(eh->ObjectId());
			m_header.SetFilter(eh->Filter());
		}
		m_timeQueued = fTime::TimeMs();
	}

	virtual ~IEvent(void) {
		if (m_outBuffer)
			m_outBuffer->Delete();
		if (m_inBuffer)
			m_inBuffer->Delete();
	}

	// for inc/dec access
	friend class Dispatcher;
	friend class EventQueue;
	friend class MTEventQueue;
};

using IEventSharedPtr = boost::intrusive_ptr<IEvent>;
inline void intrusive_ptr_add_ref(IEvent* pEvent) {
	pEvent->AddRef();
}

inline void intrusive_ptr_release(IEvent* pEvent) {
	pEvent->Release();
}

inline void IEvent::SerializeToBuffer(std::vector<unsigned char>& buffer) {
	PCBYTE buff = 0;
	ULONG len = 0;
	OutBuffer()->GetBuffer(buff, len);

	// Reserve total bytes required for the entire event
	buffer.resize(sizeof(m_header) + len);

	// Dump header
	PCBYTE pHeaderBytes = (PCBYTE)&m_header;
	std::copy(pHeaderBytes, pHeaderBytes + sizeof(m_header), buffer.begin());

	// Append payload (might be empty)
	if (len > 0) {
		std::copy(buff, buff + len, buffer.begin() + sizeof(m_header));
	}
}

inline void IEvent::GetOutputBufferNoHeader(PCBYTE& buff, ULONG& len) {
	buff = 0;
	len = 0;
	m_outBuffer->GetBuffer(buff, len);
}

inline IReadableBuffer* IEvent::InBuffer() {
	if (m_inBuffer == 0 && m_outBuffer != 0) {
		// copy the out to in
		m_inBuffer = m_encodingFactory->CreateReadable(m_outBuffer);
	}
	return m_inBuffer;
}

// used when registering events
typedef IEvent* (*CREATE_EVENT_FN)(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority);
typedef void (*SET_EVENT_ID_FN)(EVENT_ID eventId);

#define DEFINE_CLASS_INFO_BASE(c, v, p)                                                             \
public:                                                                                             \
	static void SetClassId(EVENT_ID eventId) { m_classId = eventId; }                               \
	static EVENT_ID ClassId() { return m_classId; }                                                 \
                                                                                                    \
private:                                                                                            \
	static EVENT_ID m_classId;                                                                      \
                                                                                                    \
public:                                                                                             \
	c(EVENT_ID id, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority); \
	static inline const char* ClassName() { return #c; }                                            \
	static inline ULONG ClassVersion() { return v; }                                                \
	static inline EVENT_PRIORITY ClassPriority() { return p; }

#define DECLARE_CLASS_INFO(c) EVENT_ID c::m_classId = BAD_EVENT_ID;

#define DEFINE_CLASS_INFO(c, v, p)  \
	DEFINE_CLASS_INFO_BASE(c, v, p) \
	static IEvent* CreateEvent(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority) { return ((!eh || eh->EventId() == eventId) && eventId == ClassId()) ? (new c(eventId, eh, buffer, len, from, priority)) : nullptr; }

#define DEFINE_GENERIC_CLASS_INFO(c, v, p) \
	DEFINE_CLASS_INFO_BASE(c, v, p)        \
	static IEvent* CreateEvent(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority) { return ((!eh || eh->EventId() == eventId)) ? (new c(eventId, eh, buffer, len, from, priority)) : nullptr; }

#define DEFINE_SECURE_GENERIC_CLASS_INFO(c, v, p) \
	DEFINE_CLASS_INFO_BASE(c, v, p)               \
	static IEvent* CreateEvent(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority) { return ((!eh || eh->EventId() == eventId)) ? (new c(eventId, eh, buffer, len, from, priority)) : nullptr; }

#define DEFINE_SECURE_CLASS_INFO(c, v, p) \
	DEFINE_CLASS_INFO_BASE(c, v, p)       \
	static IEvent* CreateEvent(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority) { return ((!eh || eh->EventId() == eventId) && eventId == ClassId()) ? (new c(eventId, eh, buffer, len, from, priority)) : nullptr; }

#define EVENT_CONSTRUCTOR_PROTOTYPE(c) \
	c::c(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority) : IEvent(eventId, eh, buffer, len, from, priority)

#define DERIVED_EVENT_CONSTRUCTOR_PROTOTYPE(c, b) \
	c::c(EVENT_ID eventId, EventHeader* eh, PCBYTE buffer, ULONG len, NETID from, EVENT_PRIORITY priority) : b(eventId, eh, buffer, len, from, priority)

} // namespace KEP
