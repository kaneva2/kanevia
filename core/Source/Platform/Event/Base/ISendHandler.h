///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Base/IEventHandler.h"

namespace KEP {

class ISendHandler : public IEventHandler {
public:
	virtual EVENT_PROC_RC ProcessEvent(IDispatcher* disp, IEvent* e) override {
		return SendEventTo(e);
	}

protected:
	virtual EVENT_PROC_RC SendEventTo(IEvent* e) = 0;
};

} // namespace KEP
