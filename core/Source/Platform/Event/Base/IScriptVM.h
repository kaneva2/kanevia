/******************************************************************************
 IScriptVM.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

/***********************************************************
CLASS

	ScriptVM

	class for use with RTTI to pass around VMs

DESCRIPTION

***********************************************************/
class IScriptVM {
public:
	virtual void* GetVM() {
		return m_vm; // virtual for RTTI
	}
	IScriptVM& operator=(IScriptVM& src) {
		m_vm = src.m_vm;
		return *this;
	}

protected:
	IScriptVM(void* vm) :
			m_vm(vm) {}
	void* m_vm;
};

} // namespace KEP

