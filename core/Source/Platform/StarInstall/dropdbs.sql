-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Temp for testing
DROP DATABASE IF EXISTS SHARD_INFO;
DROP DATABASE IF EXISTS KGP_COMMON;
