///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// will return TRUE if ok, FALSE otherwise. throws mysqlpp Exceptions.
__declspec(dllimport) BOOL testConnectionEx(const char* scriptDir,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	const char* dbName,
	HWND statusWindowHandle);

// will return TRUE if ok, FALSE otherwise. throws mysqlpp Exceptions.
__declspec(dllimport) BOOL installScriptsEx(const char* scriptDir, const char* scriptName,
	const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle);

// returns the dbVersion for the database dbName, 0 if doesn't exist, < 0 on error. throws mysqlpp Exceptions.
__declspec(dllimport) int getDBVersionEx(const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle);

// will return TRUE if ok, FALSE otherwise. throws mysqlpp Exceptions.
__declspec(dllimport) BOOL dropDBEx(const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle);

// will return TRUE if ok, FALSE otherwise. throws mysqlpp Exceptions.
__declspec(dllimport) BOOL createDBEx(const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle);

