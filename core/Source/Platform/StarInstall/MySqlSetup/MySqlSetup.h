///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "errmsg.h" // mysql 5

namespace mysqlpp {
class Connection;
}

// mysql++
#include <mysql++/mysql++.h>
#include <mysql++/transaction.h>
#include <mysql++/ssqls.h>
using namespace mysqlpp;

bool getCmdLineParams(std::string &scriptDir,
	std::string &scriptName,
	std::string &dbHost,
	ULONG &dbPort,
	std::string &dbUser,
	std::string &dbPass,
	std::string &dbName,
	ULONG &statusWindowHandle);

bool executeMySQLMasterScript(Connection *conn,
	std::string scriptDir,
	std::string scriptName,
	HWND statusWindowHandle);

bool executeMySQLScript(Connection *conn,
	std::string scriptDir,
	std::string scriptName,
	HWND statusWindowHandle);

bool readLines(FILE *fp,
	std::string &line,
	bool &eof,
	int &lineNum,
	bool using_delimiter);

class RAII_FILE {
public:
	RAII_FILE(FILE *fp) :
			m_fp(fp) {}
	~RAII_FILE() {
		if (m_fp)
			fclose(m_fp);
		m_fp = NULL;
	}

	FILE *get() { return m_fp; }

private:
	FILE *m_fp;
};

