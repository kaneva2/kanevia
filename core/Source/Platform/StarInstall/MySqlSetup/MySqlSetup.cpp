///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


// MySqlSetup.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "MySqlSetup.h"
#include "exdll.h"
#include <windows.h>
#include <mysqld_error.h>
#include "MySqlSetupException.h"

#define MAX_LINE_LEN 2048

using namespace std;

#ifdef _MANAGED
#pragma managed(push, off)
#endif

HINSTANCE g_hInstance;

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD /*ul_reason_for_call*/,
	LPVOID /*lpReserved*/
) {
	g_hInstance = hModule;
	return TRUE;
}

__declspec(dllexport) BOOL testConnectionEx(const char* /*scriptDir*/,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	const char* dbName,
	HWND statusWindowHandle) {
	BOOL ret = FALSE;

	try {
		auto_ptr<mysqlpp::Connection> conn(new Connection(use_exceptions));

		if (conn.get()) {
			conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
			conn->set_option(new ReconnectOption(true));

			conn->connect(dbName ? dbName : "", dbHost, dbUser, dbPass, dbPort);

			if (conn->connected()) {
				if (statusWindowHandle != NULL) {
					::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "Success");
				}

				ret = TRUE;
			} else {
				std::ostringstream os;
				os << "Unable to connect to MySQL using -\r\nHost=" << dbHost << "\r\nPort=" << dbPort << "\r\nUser=" << dbUser << "\r\nPass" << dbPass;

				if (statusWindowHandle != NULL) {
					::SendMessage((HWND)statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "Error");
				}

				throw new MySqlSetupException("testConnection", "MySql Connection Error", os.str());
			}
		}
	} catch (const mysqlpp::Exception& er) {
		throw new MySqlSetupException("testConnection", "MySql Setup Error", er.what());
	}

	return ret;
}

extern "C" __declspec(dllexport) int testConnection(HWND hwndParent, int string_size,
	char* variables, stack_t** stacktop,
	extra_parameters* /*extra*/)

{
	int ret = 0;

	EXDLL_INIT();

	{
		string scriptDir, scriptName;
		string dbHost, dbUser, dbPass;
		string dbName;
		ULONG dbPort = 0;
		ULONG statusWindowHandle = 0;

		if (!getCmdLineParams(scriptDir, scriptName, dbHost, dbPort, dbUser, dbPass, dbName, statusWindowHandle)) {
			return 0; // error
		}

		try {
			ret = testConnectionEx(scriptDir.c_str(), dbHost.c_str(), dbPort, dbUser.c_str(), dbPass.c_str(), "", 0);
		} catch (MySqlSetupException* e) {
			::MessageBoxA(hwndParent, e->getMessage().c_str(), e->getTitle().c_str(), MB_OK);
			delete e;
		}

		if (ret == 1) {
			pushstring("success");
		} else {
			pushstring("error");
		}

		return ret;
	}
}

extern "C" __declspec(dllexport) int validateDB(HWND hwndParent, int string_size,
	char* variables, stack_t** stacktop,
	extra_parameters* /*extra*/)

{
	int ret = 0;

	EXDLL_INIT();

	{
		string scriptDir, scriptName;
		string dbHost, dbUser, dbPass;
		string dbName;
		ULONG dbPort = 0;
		ULONG statusWindowHandle = 0;

		if (!getCmdLineParams(scriptDir, scriptName, dbHost, dbPort, dbUser, dbPass, dbName, statusWindowHandle)) {
			return 0; // error
		}

		try {
			ret = testConnectionEx(scriptDir.c_str(), dbHost.c_str(), dbPort, dbUser.c_str(), dbPass.c_str(), dbName.c_str(), 0);
		} catch (MySqlSetupException* e) {
			string unknownErr;
			unknownErr.assign("Unknown database");
			if (e->getMessage().compare(0, unknownErr.length() - 1, unknownErr) == 0) {
				::MessageBoxA(hwndParent, e->getMessage().c_str(), "MySql Setup Error", MB_OK);
			}
			delete e;
		}

		if (ret == 1) {
			pushstring("success");
		} else {
			pushstring("error");
		}

		return ret;
	}
}

__declspec(dllexport) BOOL installScriptsEx(const char* scriptDir,
	const char* scriptName,
	const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle) {
	BOOL ret = FALSE;

	if (statusWindowHandle != NULL) {
		::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "");
	}

	try {
		auto_ptr<mysqlpp::Connection> conn(new Connection(use_exceptions));

		if (conn.get()) {
			conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
			conn->set_option(new ReconnectOption(true));

			conn->connect(dbName, dbHost, dbUser, dbPass, dbPort);

			if (conn->connected()) {
				if (executeMySQLMasterScript(conn.get(), scriptDir, scriptName, statusWindowHandle)) {
					ret = TRUE;
				}
			} else {
				std::ostringstream os;
				os << "Unable to connect to MySQL using -\r\nHost=" << dbHost << "\r\nPort=" << dbPort << "\r\nUser=" << dbUser << "\r\nPass" << dbPass;
				throw new MySqlSetupException("installScripts", "MySql Setup Error", os.str());
			}
		}
	} catch (const mysqlpp::Exception& er) {
		throw new MySqlSetupException("installScripts", "MySql Setup Error", er.what());
	}

	return ret;
}

extern "C" __declspec(dllexport) int installScripts(HWND hwndParent, int string_size,
	char* variables, stack_t** stacktop,
	extra_parameters* /*extra*/)

{
	int ret = 0;

	EXDLL_INIT();

	{
		string scriptDir, scriptName;
		string dbHost, dbUser, dbPass;
		string dbName;
		ULONG dbPort = 0;
		ULONG statusWindowHandle = 0;

		//NOTE: dbName parameter is ignored by NSIS installScript function.
		if (!getCmdLineParams(scriptDir, scriptName, dbHost, dbPort, dbUser, dbPass, dbName, statusWindowHandle)) {
			pushstring("error");
			return 0; // error
		}

		try {
			//Pass empty string as dbName
			ret = installScriptsEx(scriptDir.c_str(), scriptName.c_str(), "", dbHost.c_str(), dbPort, dbUser.c_str(), dbPass.c_str(), (HWND)statusWindowHandle);
		} catch (MySqlSetupException* e) {
			MessageBoxA(hwndParent, e->getMessage().c_str(), e->getTitle().c_str(), MB_OK);
			delete e;
		}

		if (ret == 1) {
			pushstring("success");
		} else {
			pushstring("error");
		}

		return ret;
	}
}

extern "C" __declspec(dllexport) int testMySqlSetupDLL(HWND /*hwndParent*/, int string_size,
	char* variables, stack_t** stacktop,
	extra_parameters* /*extra*/) {
	EXDLL_INIT();
	//MessageBoxA(NULL,"Waaaz up!?","Test",MB_OK);
	pushstring("1");
	//setuservariable(INST_0, "1");
	return 1;
}

__declspec(dllexport) BOOL getMySQLVersionEx(const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND /*statusWindowHandle*/,
	string& version) {
	BOOL ret = FALSE;

	StoreQueryResult results;

	try {
		auto_ptr<mysqlpp::Connection> conn(new Connection(use_exceptions));

		if (conn.get()) {
			conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
			conn->set_option(new ReconnectOption(true));

			conn->connect("", dbHost, dbUser, dbPass, dbPort);

			if (conn->connected()) {
				Query query = conn->query();
				query << "SELECT Version();";
				results = query.store();

				if (results.size() == 1) {
					version = results.at(0).at(0);
					ret = TRUE;
				}
			} else {
				std::ostringstream os;
				os << "Unable to connect to MySQL using -\r\nHost=" << dbHost << "\r\nPort=" << dbPort << "\r\nUser=" << dbUser << "\r\nPass" << dbPass;
				throw MySqlSetupException("getMySQLVersion", "MySql Setup Error", os.str());
			}
		}
	} catch (const mysqlpp::BadQuery& er) {
		throw MySqlSetupException("getMySQLVersion", "MySql Setup Error", er.what(), er.errnum());
	} catch (const mysqlpp::BadConversion& er) {
		throw MySqlSetupException("getMySQLVersion", "MySql Setup Error", er.what());
	} catch (const mysqlpp::Exception& er) {
		throw MySqlSetupException("getMySQLVersion", "MySql Setup Error", er.what());
	}

	return ret;
}

extern "C" __declspec(dllexport) int getMySQLVersion(HWND hwndParent, int string_size,
	char* variables, stack_t** stacktop,
	extra_parameters* /*extra*/)

{
	int ret = 0;

	EXDLL_INIT();

	{
		string scriptDir, scriptName;
		string dbHost, dbUser, dbPass;
		string dbName;
		string version;
		ULONG dbPort = 0;
		ULONG statusWindowHandle = 0;

		if (!getCmdLineParams(scriptDir, scriptName, dbHost, dbPort, dbUser, dbPass, dbName, statusWindowHandle)) {
			return 0; // error
		}

		try {
			ret = getMySQLVersionEx(dbHost.c_str(), dbPort, dbUser.c_str(), dbPass.c_str(), 0, version);
		} catch (MySqlSetupException* e) {
			MessageBoxA(hwndParent, e->getMessage().c_str(), e->getTitle().c_str(), MB_OK);
			delete e;
		}

		if (ret == 1) {
			pushstring(version.c_str());
			pushstring("success");
		} else {
			pushstring("error");
		}

		return ret;
	}
}

bool getCmdLineParams(string& scriptDir,
	string& scriptName,
	string& dbHost,
	ULONG& dbPort,
	string& dbUser,
	string& dbPass,
	string& dbName,
	ULONG& statusWindowHandle) {
	char param[MAX_PATH + 1];

	memset(param, 0, sizeof(param));

	// Install location to get db import scripts from
	if (popstring(param) || param[0] == NULL) {
		throw new MySqlSetupException("getCmdLineParams", "MySql Setup Error", "Install directory not specified by NSIS installer.");
	} else {
		scriptDir.assign(param);
		memset(param, 0, sizeof(param));
	}

	// Script name
	if (popstring(param) || param[0] == NULL) {
		throw new MySqlSetupException("getCmdLineParams", "MySql Setup Error", "Script name not specified by NSIS installer.");
	} else {
		scriptName.assign(param);
		memset(param, 0, sizeof(param));
	}

	// Name of host machine to contact for MySql
	if (popstring(param) || param[0] == NULL) {
		throw new MySqlSetupException("getCmdLineParams", "MySql Setup Error", "MySql host not specified by NSIS installer.");
	} else {
		dbHost.assign(param);
		memset(param, 0, sizeof(param));
	}

	// Name of host machine port to contact for MySql
	if (popstring(param) || param[0] == NULL) {
		throw new MySqlSetupException("getCmdLineParams", "MySql Setup Error", "MySql port not specified by NSIS installer.");
	} else {
		dbPort = atol(param);
		memset(param, 0, sizeof(param));
	}

	// User to contact for MySql
	if (popstring(param) || param[0] == NULL) {
		throw new MySqlSetupException("getCmdLineParams", "MySql Setup Error", "MySql user not specified by NSIS installer.");
	} else {
		dbUser.assign(param);
		memset(param, 0, sizeof(param));
	}

	// Password of user to contact for MySql
	if (popstring(param) || param[0] == NULL) {
		// Let blank password thru
		dbPass.clear();
	} else {
		dbPass.assign(param);
		memset(param, 0, sizeof(param));
	}
	// Name of db if applicable
	if (popstring(param) || param[0] == NULL) {
		throw new MySqlSetupException("getCmdLineParams", "MySql Setup Error", "MySql database name not specified by NSIS installer.");
	} else {
		dbName.assign(param);
		memset(param, 0, sizeof(param));
	}

	// Status Text Window Handle
	if (popstring(param) || param[0] == NULL) {
		throw new MySqlSetupException("getCmdLineParams", "MySql Setup Error", "MySql status text window handle not specified by NSIS installer.");
	} else {
		statusWindowHandle = atol(param);
		memset(param, 0, sizeof(param));
	}
	return 1;
}

bool executeMySQLMasterScript(Connection* conn,
	string scriptDir,
	string scriptName,
	HWND statusWindowHandle)

{
	bool ret = true;

	string line;
	string scriptFullPath;
	scriptFullPath.assign(scriptDir);
	scriptFullPath.append("\\");
	scriptFullPath.append(scriptName);

	FILE* fp = 0;

	if (fopen_s(&fp, scriptFullPath.c_str(), "r") != 0) {
		int x = errno;
		std::ostringstream os;
		os << "Unable to open master script file " << scriptFullPath.c_str() << " " << x;
		throw new MySqlSetupException("executeMySQLMasterScript", "MySql Setup Error", os.str());
	} else {
		RAII_FILE rfp(fp); //automatically close file before returning

		bool eof = false;

		int lineNum = 0;

		while ((ret == true) && (eof == false)) {
			if (readLines(fp, line, eof, lineNum, false) == false) {
				if (eof == false) {
					// ErrMsg
					ret = false;
				}
			} else {
				try {
					if (!executeMySQLScript(conn, scriptDir, line, statusWindowHandle)) {
						ret = false;
					}
				} catch (...) {
					//Simulate the missing FINALLY keyword - does not worth another specific RAII class
					if (statusWindowHandle != NULL) {
						::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "");
					}

					throw;
				}

				//Unfortunately we have to write it again for normal code path
				if (statusWindowHandle != NULL) {
					::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "");
				}
			}
		}

		fp = NULL; //pre-caution as rfp and fp have different scopes
	}

	return ret;
}

bool executeMySQLScript(Connection* conn,
	string scriptDir,
	string scriptName,
	HWND statusWindowHandle) {
	bool ret = true;

	string line;
	string scriptFullPath;
	scriptFullPath.assign(scriptDir);
	scriptFullPath.append("\\");
	scriptFullPath.append(scriptName);

	FILE* fp = 0;

	if (fopen_s(&fp, scriptFullPath.c_str(), "r") != 0) {
		int x = errno;
		std::ostringstream os;
		os << "Unable to open script file " << scriptFullPath.c_str() << " " << x;
		throw new MySqlSetupException("executeMySQLScript", "MySql Setup Error", os.str());
	} else {
		RAII_FILE rfp(fp); //automatically close file before returning

		bool eof = false;

		int lineNum = 0;

		string statusMsg;
		statusMsg.assign("Executing script -> ");
		statusMsg.append(scriptName);

		if (statusWindowHandle != NULL) {
			::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM)statusMsg.c_str());
		}

		while ((ret == true) && (eof == false)) {
			if (readLines(fp, line, eof, lineNum, true) == false) {
				if (eof == false) {
					// ErrMsg
					ret = false;
				}
			}

			if (ret && !line.empty()) {
				// line is a filename to execute
				// if line starts with the MySQL "\. " syntax for a file, strip it off
				if (line.find("\\. ") == 0) {
					line.erase(0, 3);
					while (line.find(" ") == 0)
						line.erase(0, 1);
					ret = executeMySQLScript(conn, scriptDir, line, statusWindowHandle);
				} else {
					try {
						Query query = conn->query();
						query << line.c_str();
						query.store();
						// pull off and throw away any result sets, otherwise will fail on next call
						for (int i = 1; query.more_results(); ++i) {
							StoreQueryResult res = query.store_next();
						}
					} catch (const mysqlpp::BadQuery& er) {
						std::ostringstream os;
						os << "Received error executing script.\r\nScript name=" << scriptName << "\r\nLine=" << lineNum << "\r\nSQL error=" << er.what() << "\r\nQuery=" << line.c_str();
						throw new MySqlSetupException("executeMySQLScript", "MySql Setup Error", os.str(), er.errnum());
					} catch (const mysqlpp::BadConversion& er) {
						throw new MySqlSetupException("executeMySQLScript", "MySql Setup Error", er.what());
					} catch (const mysqlpp::Exception& er) {
						//Not sure why we didn't have a MessageBox call in this case. Throw it anyway. User should now see a message box for it in the Editor/Installer.
						throw new MySqlSetupException("executeMySQLScript", "MySql Setup Error", er.what());
					}
				}
			}
		}

		fp = NULL; //pre-caution as rfp and fp have different scopes
	}

	return ret;
}

bool readLines(FILE* fp,
	string& line,
	bool& eof,
	int& lineNum,
	bool using_delimiter) {
	eof = false;

	bool ret = true;

	line.erase();

	char buffer[MAX_LINE_LEN + 1];

	string delimiter;
	delimiter = ";";

	if (fp != 0) {
		bool complete_block = false;
		bool leave_cr_lf = false;

		do {
			if (fgets(buffer, MAX_LINE_LEN, fp) == 0) {
				if (feof(fp)) {
					eof = true;
				}

				ret = false;
				complete_block = true;
			} else {
				lineNum++;

				if (!leave_cr_lf) {
					size_t len = ::strlen(buffer);
					while (len > 0 && (buffer[len - 1] == '\r' || buffer[len - 1] == '\n')) {
						// strip crlf
						buffer[len - 1] = '\0';
						len--;
					}
				}
				char* p = strstr(buffer, "--");
				if (p != 0)
					*p = '\0'; // remove single-line comments

				// is this a nested file?
				if (strncmp(buffer, "\\. ", 3) == 0) {
					line = buffer;
					return true;
				}

				string tmpline = buffer;

				tmpline = buffer;

				if (using_delimiter == true) {
					// Throw away comment
					if ((tmpline.length() >= 2) && ((tmpline.compare(0, 2, "--") == 0) || (tmpline.compare(0, 2, "/*") == 0))) {
					} else if (tmpline.empty()) {
					}
					// change the delimiter
					else if ((tmpline.length() >= 11) && (tmpline.compare(0, 10, "DELIMITER ") == 0)) {
						delimiter.assign(tmpline.substr(10, tmpline.length() - 10));
						leave_cr_lf = true; // for functions and stored procedures.
					} else if ((tmpline.length() >= delimiter.length()) && (tmpline.find(delimiter.c_str()) != string::npos)) {
						if (delimiter.compare(0, 1, ";") != 0) {
							if (!leave_cr_lf) {
								size_t len = ::strlen(buffer);
								while (len > 0 && (buffer[len - 1] == '\r' || buffer[len - 1] == '\n')) {
									// strip crlf
									buffer[len - 1] = '\0';
									len--;
								}
							}

							string newline;
							string delimiterline = buffer;

							string::size_type loc = delimiterline.find(delimiter.c_str());

							newline = delimiterline.substr(0, delimiterline.length() - (delimiterline.length() - loc));

							line.append(" ");
							line.append(newline);
							line.append("\n");
							complete_block = true;
						} else {
							line.append(" ");
							line.append(buffer);
							complete_block = true;
						}
					} else {
						line.append(" ");
						line.append(buffer);
						if (delimiter.compare(0, 1, ";") != 0) {
							line.append(" ");
						}
					}
				} else {
					// Reading one line at a time
					line = tmpline;
					complete_block = true;
				}
			}
		} while (!complete_block);
	} else {
		ret = false;
	}

	if (line.find_first_not_of(" \r\n\t") == string::npos)
		line.clear(); // all whitespace

	return ret;
}

// returns 0 if schema_version doesn't exist, -1 if error
__declspec(dllexport) int getDBVersionEx(const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle) {
	int ret = -1;

	if (statusWindowHandle != NULL) {
		::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "");
	}

	try {
		auto_ptr<mysqlpp::Connection> conn(new Connection(use_exceptions));

		if (conn.get()) {
			conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
			conn->set_option(new ReconnectOption(true));

			conn->connect("", dbHost, dbUser, dbPass, dbPort);

			if (conn->connected()) {
				mysqlpp::Query query = conn->query();
				query << "SELECT FLOOR(MAX(version)), NOW() FROM `" << dbName << "`.schema_versions where version > 0";
				StoreQueryResult r = query.store();
				//At this point the database does exist
				if (r.at(0).at(0).is_null())
					ret = 1; // version less than 1, return 1 to drop the db and recreate
				else
					ret = (long)(r.at(0).at(0)); // return version

			} else {
				std::ostringstream os;
				os << "Unable to connect to MySQL using -\r\nHost=" << dbHost << "\r\nPort=" << dbPort << "\r\nUser=" << dbUser << "\r\nPass" << dbPass;
				throw new MySqlSetupException("getDBVersion", "MySql Setup Error", os.str());
			}
		}
	} catch (const mysqlpp::BadQuery& bq) {
		// ok if get doesn't exist error
		// Table 'asdf.schema_versions' doesn't exist
		if (bq.errnum() == ER_NO_SUCH_TABLE) {
			ret = 0; // return 0 to avoid error msg box and drop logic
		} else {
			throw new MySqlSetupException("getDBVersion", "MySql Setup Error", bq.what(), bq.errnum());
		}
	} catch (const mysqlpp::Exception& er) {
		throw new MySqlSetupException("getDBVersion", "MySql Setup Error", er.what());
	}

	return ret;
}

// UNTESTED VERSION TO BE CALLED FROM NSIS
extern "C" __declspec(dllexport) int getDBVersion(HWND hwndParent, int string_size,
	char* variables, stack_t** stacktop,
	extra_parameters* /*extra*/)

{
	int ret = -1;

	EXDLL_INIT();

	{
		string scriptName;
		string dbName;
		string throwAway;
		string dbHost, dbUser, dbPass;
		ULONG dbPort = 0;
		ULONG statusWindowHandle = 0;

		if (!getCmdLineParams(scriptName, throwAway, dbHost, dbPort, dbUser, dbPass, dbName, statusWindowHandle)) {
			return 0; // error
		}

		try {
			ret = getDBVersionEx(dbName.c_str(), dbHost.c_str(), dbPort, dbUser.c_str(), dbPass.c_str(), 0);
		} catch (MySqlSetupException* e) {
			::MessageBoxA(hwndParent, e->getMessage().c_str(), e->getTitle().c_str(), MB_OK);
			delete e;
		}

		if (ret > 0) {
			std::ostringstream os;
			os << ret;
			pushstring(os.str().c_str());
		} else {
			pushstring("error");
		}

		return ret;
	}
}

// will return 1 if ok, 0 otherwise.  Will show message box
__declspec(dllexport) BOOL dropDBEx(const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle) {
	BOOL ret = FALSE;

	if (statusWindowHandle != NULL) {
		::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "");
	}

	try {
		auto_ptr<mysqlpp::Connection> conn(new Connection(use_exceptions));

		if (conn.get()) {
			conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
			conn->set_option(new ReconnectOption(true));

			conn->connect("", dbHost, dbUser, dbPass, dbPort);

			if (conn->connected()) {
				mysqlpp::Query query = conn->query();
				query << "DROP DATABASE `" << dbName << "`";
				query.execute();
				ret = TRUE;
			} else {
				std::ostringstream os;
				os << "Unable to connect to MySQL using -\r\nHost=" << dbHost << "\r\nPort=" << dbPort << "\r\nUser=" << dbUser << "\r\nPass" << dbPass;
				string errMsg(os.str());
				throw new MySqlSetupException("dropDB", "MySql Setup Error", errMsg);
			}
		}
	} catch (const mysqlpp::Exception& er) {
		throw new MySqlSetupException("dropDB", "MySql Setup Error", er.what());
	}

	return ret;
}

extern "C" __declspec(dllexport) int dropDB(HWND hwndParent, int string_size,
	char* variables, stack_t** stacktop,
	extra_parameters* /*extra*/)

{
	int ret = 0;

	EXDLL_INIT();

	{
		string scriptDir, scriptName;
		string dbHost, dbUser, dbPass;
		string dbName;
		ULONG dbPort = 0;
		ULONG statusWindowHandle = 0;

		if (!getCmdLineParams(scriptDir, scriptName, dbHost, dbPort, dbUser, dbPass, dbName, statusWindowHandle)) {
			pushstring("error");
			return 0; // error
		}

		try {
			ret = dropDBEx(dbName.c_str(), dbHost.c_str(), dbPort, dbUser.c_str(), dbPass.c_str(), (HWND)statusWindowHandle);
		} catch (MySqlSetupException* e) {
			::MessageBoxA(hwndParent, e->getMessage().c_str(), e->getTitle().c_str(), MB_OK);
			delete e;
		}

		if (ret == 1) {
			pushstring("success");
		} else {
			pushstring("error");
		}

		return ret;
	}
}

// will return 1 if ok, 0 otherwise.  Will show message box
__declspec(dllexport) BOOL createDBEx(const char* dbName,
	const char* dbHost, ULONG dbPort,
	const char* dbUser, const char* dbPass,
	HWND statusWindowHandle) {
	BOOL ret = FALSE;

	if (statusWindowHandle != NULL) {
		::SendMessage(statusWindowHandle, WM_SETTEXT, 0, (LPARAM) "");
	}

	try {
		auto_ptr<mysqlpp::Connection> conn(new Connection(use_exceptions));

		if (conn.get()) {
			conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
			conn->set_option(new ReconnectOption(true));

			conn->connect("", dbHost, dbUser, dbPass, dbPort);

			if (conn->connected()) {
				mysqlpp::Query query = conn->query();
				query << "CREATE DATABASE `" << dbName << "`";
				query.execute();
				ret = TRUE;
			} else {
				std::ostringstream os;
				os << "Unable to connect to MySQL using -\r\nHost=" << dbHost << "\r\nPort=" << dbPort << "\r\nUser=" << dbUser << "\r\nPass" << dbPass;
				string errMsg(os.str());
				throw new MySqlSetupException("createDB", "MySql Setup Error", errMsg);
			}
		}
	} catch (const mysqlpp::Exception& er) {
		throw new MySqlSetupException("createDB", "MySql Setup Error", er.what());
	}

	return ret;
}
#ifdef _MANAGED
#pragma managed(pop)
#endif
