///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

class MySqlSetupException {
public:
	MySqlSetupException(const std::string& source, const std::string& title, const std::string& message, int mysqlErr = 0) :
			m_source(source), m_title(title), m_message(message), m_mySqlErr(mysqlErr) {}

	std::string getSource() const { return m_source; }
	std::string getTitle() const { return m_title; }
	std::string getMessage() const { return m_message; }
	int getMySqlErrNum() const { return m_mySqlErr; }

private:
	std::string m_source;
	std::string m_title;
	std::string m_message;
	int m_mySqlErr;
};
