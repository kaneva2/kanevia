; Script generated by the HM NIS Edit Script Wizard.

; NIS Includes
!include 'LogicLib.nsh'

; HM NIS Edit Wizard helper defines
!define PRODUCT_NAME "Kaneva STAR"
!define SHORT_PRODUCT_NAME "Kaneva STAR"
!define PRODUCT_VERSION "- 1.0 Beta"
!define PRODUCT_PUBLISHER "Kanvea, Inc."
!define PRODUCT_WEB_SITE "http://www.kaneva.com"
!define PRODUCT_DIR_REGKEY "SOFTWARE\Kaneva\STAR"
!define PRODUCT_EDITOR_DIR "Software\Kaneva\Kaneva Editor Version 2.0\NewGame";

!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\Kaneva STAR"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_ROOT_KEY "HKLM"

; if pass in BLADE_DIR, use it for server_dir, too (TFS build), otherwise use default
!ifdef BLADE_DIR
!define SERVER_DIR ${BLADE_DIR} ; comment to avoid continuation w/ trailing slash
!else
!define BLADE_DIR "..\..\..\Deploy\GameContent\blades"
!ifndef REMOTE_BUILD
        !define SERVER_DIR "..\..\..\Deploy\GameContent\bin"
!endif
!endif

; our pre-built binaries in TFS
;!define BIN_DIR "..\..\..\Deploy\GameContent\bin"
!define GME_CNTNT_BIN_DIR "..\..\..\Deploy\GameContent\bin"

!ifdef REMOTE_BUILD
	!define BIN_DIR "..\..\..\..\Binaries\win32\release"
	!define SERVER_DIR "..\..\..\..\Binaries\win32\release"
!else
	!define BIN_DIR "..\..\..\Deploy\GameContent\bin"
!endif



; MUI 1.67 compatible ------
!include "MUI2.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"
;--------------------------------
;Interface Configuration

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "..\..\..\Deploy\BuildProcess\Kaneva_Logo_small2.bmp"


; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
!insertmacro MUI_PAGE_LICENSE "..\..\..\Deploy\BuildProcess\STAR_TOS.rtf"
; Directory page
!insertmacro MUI_PAGE_DIRECTORY

!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"



XPStyle on

Name "${SHORT_PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "CWTemplateSetup.exe"
InstallDir "$PROGRAMFILES\Kaneva\STARPlatform"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" "Install_Dir"
ShowInstDetails hide
ShowUnInstDetails hide
AutoCloseWindow false


Section "EditorTemplateFiles"
    SetOverwrite ifnewer

    SetOutPath "$INSTDIR\templates"
    File /r /x *.pdb /x *.lib /x *.exp "..\..\..\Deploy\GameContent\templates\CardboardWorld"

SectionEnd


Section Uninstall


SectionEnd
