# Look at Readme.txt for usage alongside with the Modern UI

!include "WinMessages.nsh"
!include "WinVer.nsh"
!include "LogicLib.nsh"

!include "WordFunc.nsh"
!include "TextFunc.nsh"

!define VDIRNAME STARAdmin
;!define VDIRSKIPALL
!include "..\..\..\Deploy\BuildProcess\Virtual Directory.nsh"
!include "..\..\..\Deploy\BuildProcess\dotnetdetectver.nsh"

; net .NET 2.0+
!define DOT_MAJOR "2"
!define DOT_MINOR "0"

Name "VDir Test Install"

OutFile "VDirTest.exe"

ShowInstDetails show

Section
    DetailPrint "Testing VDir Installation"
    Call CreateVDir
	DetailPrint ""
	
	DetailPrint "Checking Windows Version against XP SP3+ or Vista+"
   ${If} ${IsWinXP}
   ${AndIf} ${AtLeastServicePack} 3
   ${OrIf} ${AtLeastWinVista}
     DetailPrint "Running WinXP SP3 or above (Vista)"
     Goto NextCk
   ${Else}
     DetailPrint "Running less than WinXP SP3"
   ${EndIf}

NextCk:
	DetailPrint "Checking Windows Version against XP SP3+ or 2003+"
   ${If} ${IsWinXP}
   ${AndIf} ${AtLeastServicePack} 3
   ${OrIf} ${AtLeastWin2003}
     DetailPrint "Running WinXP SP3 or above (2003)"
     Goto DotNetCk
   ${Else}
     DetailPrint "Running less than WinXP SP3"
   ${EndIf}

DotNetCk:
    DetailPrint "Testing .NET Installation"
        ; check for IIS and .NET
        Call IsDotNETInstalled
        Pop $R3
        StrCmp $R3 0 NoDotNet
        DetailPrint "Found .NET"
        Goto byebye
        
NoDotNet:
    DetailPrint "Can't find .NET!"

byebye:
SectionEnd

!include "WordFunc.nsh"

Section
    SetOutPath "c:\temp"
	File "..\..\..\Deploy\GameContent\templates\Shared\ServerLog.tmp"
	StrCpy $R0 0
	${LineFind} "ServerLog.tmp" "$OUTDIR\ServerLog.cfg" "1:-1" "ReplaceLogDir"
	IfErrors 0 +2
	MessageBox MB_OK "Error replacing" IDOK +2
	MessageBox MB_OK "Changed lines=$R0"
SectionEnd

Section
        nsSCM::Start "KGPController"
        Pop $0 ; return error/success TRUE/FALSE
        StrCmp $0 "success" end
        MessageBox MB_OK|MB_ICONEXCLAMATION "The KGPController service could not be started."
        end:
SectionEnd

Function ReplaceLogDir
	StrCpy $1 $R9

	${WordReplace} '$R9' '%INSTDIR%' '$INSTDIR' '+*' $R9

	StrCmp $1 $R9 +2
	IntOp $R0 $R0 + 1
	;$R0   count of changed lines

	Push $0
FunctionEnd
