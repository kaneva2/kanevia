///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <sstream>

bool getCmdLineParamsDefaultServer(std::string& defaultServerXmlPath,
	std::string& contentPath);

bool getCmdLineParamsDbCfg(std::string& contentPath,
	std::string& user,
	std::string& pass,
	std::string& dbName);
