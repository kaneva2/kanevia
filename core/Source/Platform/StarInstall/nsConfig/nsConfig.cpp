///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// nsConfig.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "nsConfig.h"
#include "KEPConfig.h"
#include "KEPException.h"
#ifndef DECOUPLE_DBCONFIG
#else
#include "Common/include/DBConfigExporter.h"
#endif
#include "DBConfigValues.h"
#include "exdll.h"
#include <winsock2.h>

using namespace std;
using namespace KEP;

#ifdef _MANAGED
#pragma managed(push, off)
#endif

HINSTANCE g_hInstance;

HWND g_hwndParent;

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD /*ul_reason_for_call*/,
	LPVOID /*lpReserved*/
) {
	g_hInstance = hModule;
	return TRUE;
}

extern "C" __declspec(dllexport) int updateDefaultServerXML(HWND hwndParent, int string_size,
	char *variables, stack_t **stacktop,
	extra_parameters * /*extra*/)

{
	EXDLL_INIT();

	{
		int ret = 1;

		g_hwndParent = hwndParent;

		string defaultServerXmlPath;
		string contentPath;

		if (!getCmdLineParamsDefaultServer(defaultServerXmlPath, contentPath)) {
			pushstring("error");
			return 0; // error
		}

		try {
			WSADATA WSAData;

			KEPConfig cfg;

			char szHostName[128] = "";

			// Initialize winsock dll
			int err = ::WSAStartup(MAKEWORD(1, 0), &WSAData);

			if (err) {
				std::wostringstream os;
				os << L"Unable to initialize winsock to determine the server's host name. Error:" << err;
				wstring errMsg(os.str());
				MessageBoxW(g_hwndParent, errMsg.c_str(), L"Configuration Update Error", MB_OK);
				pushstring("error");
				return 0; // error
			}

			if (::gethostname(szHostName, sizeof(szHostName) - 1)) {
				err = WSAGetLastError();
				std::wostringstream os;
				os << L"Unable to determine the server's host name. Error:" << err;
				wstring errMsg(os.str());
				MessageBoxW(g_hwndParent, errMsg.c_str(), L"Configuration Update Error", MB_OK);
				pushstring("error");
				return 0; // error
			}

			WSACleanup();

			string directoryPath;
			directoryPath = contentPath;
			directoryPath += "\\ServerFiles";

			string scriptsDirectoryPath;
			scriptsDirectoryPath = contentPath;
			scriptsDirectoryPath += "\\ServerFiles\\GameFiles\\ServerScripts";

			cfg.Load(defaultServerXmlPath.c_str(), "EngineConfig");
			cfg.SetValue("Directory", directoryPath.c_str());
			cfg.SetValue("ScriptDir", scriptsDirectoryPath.c_str());
			cfg.SetValue("IpAddress", szHostName);
			cfg.Save();
		} catch (KEPException *e) {
			MessageBoxW(g_hwndParent, Utf8ToUtf16(e->ToString()).c_str(), L"Configuration Update Error", MB_OK);
			ret = 0;
			e->Delete();
		}

		if (ret == 1) {
			pushstring("success");
		} else {
			pushstring("error");
		}

		return ret;
	}
}

extern "C" __declspec(dllexport) int updateDbCfg(HWND hwndParent, int string_size,
	char *variables, stack_t **stacktop,
	extra_parameters * /*extra*/)

{
	EXDLL_INIT();

	{
		int ret = 1;

		g_hwndParent = hwndParent;

		string contentPath;
		string user;
		string pass;
		string dbName;

		if (!getCmdLineParamsDbCfg(contentPath, user, pass, dbName)) {
			pushstring("error");
			return 0; // error
		}

		try {
			KEPConfig config;
			DBConfig dbConfig(config);

			string dbServer, dbNameOld, dbUser, dbPassword;
			int dbPort;
			bool dontShowDlg;

			// Retrieve settings from configuration file
			config.Load(PathAdd(contentPath, DB_CFG_NAME).c_str(), DB_CFG_ROOT_NODE);
			if (!config.IsLoaded() ||
				!dbConfig.Get(dbServer, dbPort, dbNameOld, dbUser, dbPassword, dontShowDlg)) {
				// Loading failed
				std::wostringstream os;
				os << "Unable to load database configuration file db.cfg at " << contentPath.c_str();
				wstring errMsg(os.str());
				MessageBoxW(g_hwndParent, errMsg.c_str(), L"Configuration Update Error", MB_OK);
				ret = 0;
			} else {
				// Save with new user name, password and DB name
#ifndef DECOUPLE_DBCONFIG
				if (!DBConfig::Export(config, dbServer.c_str(), dbPort, dbName.c_str(), user.c_str(), pass.c_str(), dontShowDlg) ||
#else
				if (!DBConfigExporter::Export(config, dbServer.c_str(), dbPort, dbName.c_str(), user.c_str(), pass.c_str(), dontShowDlg) ||
#endif
					!config.Save()) {
					std::wostringstream os;
					os << L"Unable to save database configuration file db.cfg at " << contentPath.c_str();
					wstring errMsg(os.str());
					MessageBoxW(g_hwndParent, errMsg.c_str(), L"Configuration Update Error", MB_OK);
					ret = 0;
				}
			}
		} catch (KEPException *e) {
			MessageBoxW(g_hwndParent, Utf8ToUtf16(e->ToString()).c_str(), L"Configuration Update Error", MB_OK);
			ret = 0;
			e->Delete();
		} catch (KEPException &e) {
			MessageBoxW(g_hwndParent, Utf8ToUtf16(e.ToString()).c_str(), L"Configuration Update Error", MB_OK);
			ret = 0;
		}

		if (ret == 1) {
			pushstring("success");
		} else {
			pushstring("error");
		}

		return ret;
	}
}

bool getCmdLineParamsDefaultServer(string &defaultServerXmlPath,
	string &contentPath) {
	char param[MAX_PATH + 1];

	memset(param, 0, sizeof(param));

	// Path to the DefaultServer.xml file
	if (popstring(param) || param[0] == NULL) {
		MessageBoxW(g_hwndParent, L"The path to the DefaultServer.xml configuration file was not specified by NSIS installer.", L"Configuration Update Error", MB_OK);
		return 0;
	} else {
		defaultServerXmlPath.assign(param);
		memset(param, 0, sizeof(param));
	}

	// Content path
	if (popstring(param) || param[0] == NULL) {
		MessageBoxW(g_hwndParent, L"The path to the server content was not specified by NSIS installer.", L"Configuration Update Error", MB_OK);
		return 0;
	} else {
		contentPath.assign(param);
		memset(param, 0, sizeof(param));
	}

	return 1;
}

bool getCmdLineParamsDbCfg(string &contentPath,
	string &user,
	string &pass,
	string &dbName) {
	char param[MAX_PATH + 1];

	memset(param, 0, sizeof(param));

	// Path to the content
	if (popstring(param) || param[0] == NULL) {
		MessageBoxW(g_hwndParent, L"The path to the content directory was not specified by NSIS installer.", L"Configuration Update Error", MB_OK);
		return 0;
	} else {
		contentPath.assign(param);
		memset(param, 0, sizeof(param));
	}

	// user name
	if (popstring(param) || param[0] == NULL) {
		MessageBoxW(g_hwndParent, L"The user to update the db.cfg content file was not specified by NSIS installer.", L"Configuration Update Error", MB_OK);
		return 0;
	} else {
		user.assign(param);
		memset(param, 0, sizeof(param));
	}

	// password
	if (popstring(param) || param[0] == NULL) {
		// Let blank password thru
		pass.clear();
	} else {
		pass.assign(param);
		memset(param, 0, sizeof(param));
	}

	if (popstring(param) || param[0] == NULL) {
		MessageBoxW(g_hwndParent, L"The database name to update the db.cfg content file was not specified by NSIS installer.", L"Configuration Update Error", MB_OK);
		return 0;
	} else {
		dbName.assign(param);
		memset(param, 0, sizeof(param));
	}

	return 1;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif
