///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/ErrorSpec.h"

#define DEFAULT_RESTRESULT_TAG "result"
#define DEFAULT_ERRORSPEC_TAG "error"

namespace KEP {

/**
 * Class hierarchy for parsing a standardized "response" to a rest call.
 * Premised on responses being in the format <result><<value>></result>
 * All implementations found in this file store a reference to an
 * external variable of the specified type, thus binding it with that variable.
 * Hence, whenever "parse()" is called, the results of that parse operation
 * can be found automagically in the external variable.
 */
class RestResultParser {
public:
	RestResultParser(const std::string& tag = DEFAULT_RESTRESULT_TAG) :
			m_responseContent(), m_tag(tag) {}
	virtual ~RestResultParser() {}
	virtual void parse(const void* content, size_t contentLen) = 0;
	const std::string& responseContent() const { return m_responseContent; }

protected:
	void setResponseContent(const void* content, size_t contentLen) {
		m_responseContent.clear();
		m_responseContent.append((const char*)content, contentLen);
	}

	std::string m_responseContent;
	std::string m_tag;
};

class IntParser : public RestResultParser {
public:
	IntParser(int& output, const std::string& tag = DEFAULT_RESTRESULT_TAG) :
			RestResultParser(tag), m_output(output) {}
	virtual ~IntParser() {}
	virtual void parse(const void* content, size_t contentLen);

private:
	int& m_output;
};

class UIntParser : public RestResultParser {
public:
	UIntParser(unsigned int& output, const std::string& tag = DEFAULT_RESTRESULT_TAG) :
			RestResultParser(tag), m_output(output) {}
	virtual ~UIntParser() {}
	virtual void parse(const void* content, size_t contentLen);

private:
	unsigned int& m_output;
};

class StringParser : public RestResultParser {
public:
	StringParser(std::string& output, const std::string& tag = DEFAULT_RESTRESULT_TAG) :
			RestResultParser(tag), m_output(output) {}
	virtual ~StringParser() {}
	virtual void parse(const void* content, size_t contentLen);

private:
	std::string& m_output;
};

class SimpleRestResultParser : public RestResultParser {
public:
	SimpleRestResultParser(const std::string& tag = DEFAULT_RESTRESULT_TAG) :
			RestResultParser(tag) {}
	virtual ~SimpleRestResultParser() {}
	void parse(const void* content, size_t size) { setResponseContent(content, size); }
};

// Marshal rest response for generalized error specification
//
// <error code="{errcode}">{errmsg}</error>"
//
class ErrorSpecParser : public RestResultParser {
public:
	ErrorSpecParser(ErrorSpec& spec, const std::string& tag = DEFAULT_ERRORSPEC_TAG);
	virtual ~ErrorSpecParser() {}
	void parse(const void* content, size_t size);
	ErrorSpec& m_output;
};

} // namespace KEP