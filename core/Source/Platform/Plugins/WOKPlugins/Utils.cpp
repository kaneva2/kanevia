///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Incubator.h"
#include "Utils.h"

namespace KEP {

UINT64 GetSystemTimeUINT64() {
	ULARGE_INTEGER timestamp;
	FILETIME ft;
	SYSTEMTIME st;
	GetSystemTime(&st);
	SystemTimeToFileTime(&st, &ft);

	timestamp.LowPart = ft.dwLowDateTime;
	timestamp.HighPart = ft.dwHighDateTime;

	return timestamp.QuadPart;
};

/**
 * @deprecated Moved to KEP::Algorithms.  Use KEP::Algorithms::getLocalHostName()
 */
std::string GetLocalHostName() {
	wchar_t buffer[1024];
	DWORD nameLen = _countof(buffer);

	if (GetComputerNameEx(ComputerNameDnsHostname, buffer, &nameLen)) {
		return Utf16ToUtf8(buffer);
	} else {
		return "localhost";
	}
}

std::string convertToLocalTime(UINT64 timeIn) {
	std::wstring ret;

	FILETIME ft, lft;
	SYSTEMTIME lt;
	ULARGE_INTEGER timestamp;
	wchar_t convDateTime[80];

	timestamp.QuadPart = timeIn;
	ft.dwHighDateTime = timestamp.HighPart;
	ft.dwLowDateTime = timestamp.LowPart;

	if (FileTimeToLocalFileTime(&ft, &lft) && FileTimeToSystemTime(&lft, &lt)) {
		if (GetDateFormat(LOCALE_USER_DEFAULT, 0, &lt, NULL, convDateTime, 80) != 0) {
			ret = convDateTime;

			if (GetTimeFormat(0, 0, &lt, NULL, convDateTime, 80) != 0) {
				ret += L" ";
				ret += convDateTime;
			}
		}
	}

	return Utf16ToUtf8(ret);
}

} // namespace KEP