///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

typedef std::map<AppId, std::pair<UINT64, int>> AppHeartbeatMap;

std::stringstream& operator<<(std::stringstream& ss, const AppHeartbeatMap& hbm);
AppHeartbeatMap DecodeHeartbeats(const std::string& str);

} // namespace KEP