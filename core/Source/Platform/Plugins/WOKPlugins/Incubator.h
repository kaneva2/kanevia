///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "plugin.h"

#define SUPPORT_LEGACY_APP
#define LOGICAL_LEGACY_APP_INDICATOR "LEGACYAPP=Y"

namespace KEP {

typedef unsigned int TaskId;
typedef int AppId;

template <typename T, typename StubT, const char* N>
class UniversalPluginFactory : public AbstractPluginFactory {
public:
	UniversalPluginFactory() :
			AbstractPluginFactory(N) {}
	virtual ~UniversalPluginFactory() {}
	virtual PluginPtr createPlugin(const TIXMLConfig::Item& configItem) const {
		TiXmlElement* config = configItem._internal;
		int isRemote = 0;
		if (config && config->QueryIntAttribute("remote", &isRemote) == TIXML_SUCCESS && isRemote != 0) {
			return PluginPtr(new StubT(config));
		}
		return PluginPtr(new T(config));
	}
};

} // namespace KEP

#define MODULE_NAME_REST_CLIENT "RESTClient"
#define MODULE_NAME_REST_CLIENTW _T("RESTClient")
#define MODULE_NAME_ALLOCATOR "Allocator"
#define MODULE_NAME_PROVISIONER "Provisioner"
#define MODULE_NAME_APPLEASEMANAGER "AppLeaseManager"
#define MODULE_NAME_APPREGISTRY "AppRegistry"
#define MODULE_NAME_HOSTLEASEMANAGER "HostLeaseManager"
#define MODULE_NAME_HOSTREGISTRY "HostRegistry"
#define MODULE_NAME_APPCONTROLLER "AppController"
#define MODULE_NAME_NOTIFIER "Notifier"
#define MODULE_NAME_ALERTSERVICE "AlertService"
#define MODULE_NAME_TEMPLATEREPOSITORY "TemplateRepository"
#define MODULE_NAME_WOKPROVISIONER "WOKProvisioner"

#define INTERFACE_NAME_REST_CLIENT "IRESTClient"
#define INTERFACE_NAME_APPALLOCATOR "IAppAllocator"
#define INTERFACE_NAME_APPPROVISIONER "IAppProvisioner"
#define INTERFACE_NAME_APPLEASEMANAGER "IAppLeaseManager"
#define INTERFACE_NAME_APPHEARTBEATMONITOR "IAppHeartbeatMonitor"
#define INTERFACE_NAME_APPREGISTRY "IAppRegistry"
#define INTERFACE_NAME_HOSTLEASEMANAGER "IHostLeaseManager"
#define INTERFACE_NAME_HOSTHEARTBEATMONITOR "IHostHeartbeatMonitor"
#define INTERFACE_NAME_HOSTREGISTRY "IHostRegistry"
#define INTERFACE_NAME_APPCONTROLLER "IAppController"
#define INTERFACE_NAME_NOTIFIER "INotifier"
#define INTERFACE_NAME_ALERTSERVICE "IAlertService"
#define INTERFACE_NAME_TEMPLATEREPOSITORY "ITemplateRepository"
#define INTERFACE_NAME_WOKPROVISIONER "IWOKProvisioner"

#define GET_PLUGIN(host, moduleName, castType) (dynamic_cast<castType*>(&*host->getPlugin(moduleName)))
#define GET_PLUGIN_INTERFACE(host, intf) host->DEPRECATED_getInterfaceByNameNoThrow<intf>(#intf)

enum {
	APIN_SUCCESS = 0,
	APIN_ERROR_START = 0x1000,

	APIN_INTERNAL_ERROR_START = 0x1000,
	APIN_ERR_MISSING_PLUGIN = 0x1000, // Missing platform module
	APIN_ERR_BAD_CONFIG = 0x1001, // Missing/invalid platform configuration
	APIN_ERR_DEAD_REMOTE_HOST = 0x1002, // Remote host failed checking in
	APIN_ERR_IO_FAILED = 0x1003, // IO Error
	APIN_ERR_DB_FAILED = 0x1004, // DB Error
	APIN_ERR_ALGORITHM = 0x1005, // Bug!

	APIN_CLIENT_ERROR_START = 0x1100,
	APIN_GENERIC_ERROR = 0x1100, // Generic error
	APIN_INVALID_REQUEST_PARAM = 0x1101, // Bad parameter
	APIN_INVALID_REQUEST_VERB = 0x1102, // Reserved
	APIN_INVALID_TASKID = 0x1103, // Invalid task ID for IsTaskCompleted call

	APIN_REQUEST_ERROR_START = 0x1200,
	APIN_APP_ID_NOT_FOUND = 0x1200, // Reserved
	APIN_ALLOCATOR_TASK_TIMEOUT = 0x1201, // Timeout by allocator housekeeping
	APIN_PROVISION_FAILED_NO_RESOURCE = 0x1202, // No resource available (for either user or maintenance provisioning request)
	APIN_PROVISION_FAILED_TIMEOUT = 0x1203, // Reserved
	APIN_ALLOCATION_FAILED_UNKNOWN_APPID = 0x1204, // App request failed - not previously registered
	APIN_ALLOCATION_FAILED_POLICY = 0x1205, // App request rejected by policy (e.g. over per-user limit)
	APIN_ALLOCATION_FAILED_NO_SERVER = 0x1206, // No server is available to run requested app
	APIN_ALLOCATION_FAILED_NO_SLOT = 0x1207, // Reserved
	APIN_APP_NOT_PROVISIONED = 0x1208, // Reserved
	APIN_APP_ALREADY_STARTED = 0x1209, // Reserved
	APIN_STARTING_FAILED_TIMEOUT = 0x120A, // StartServer: request timeout - KGPServer process crashs/stalls at startup
	APIN_STARTING_FAILED_MISSING_PARAM = 0x120B, // StartServer: missing/empty server files directory parameter
	APIN_STARTING_FAILED_BAD_APPDIR = 0x120C, // StartServer: invalid server files directory
	APIN_STOPPING_FAILED_TIMEOUT = 0x120D, // StopServer: request timeout
	APIN_STOPPING_FAILED_NOT_FOUND = 0x120E, // StopServer: unknown or previously stopped app
	APIN_STOPPING_FAILED_BAD_HANDLE = 0x120F, // StopServer: internal error
	APIN_STOPPING_FAILED_ALREADY_TERMINATED = 0x1210, // StopServer: process already terminated (unexpectedly)
	APIN_RESERVED_1 = 0x1211, // Reserved
	APIN_HOSTREGISTRY_HOST_ALREADY_EXISTS = 0x1212, // Host name already exists in HostRegistry
	APIN_HOSTREGISTRY_INVALID_HOST = 0x1213, // Host name not found in HostRegistry
	APIN_APPLEASE_ALREADY_ACTIVE = 0x1214, // App already leased
	APIN_APPLEASE_NOT_FOUND = 0x1215, // Requested app not found in lease table
	APIN_APPLEASE_APPMISMATCH = 0x1216, // Bad AppRegistry data
	APIN_PREALLOCATION_FAILED_NO_SERVER = 0x1217, // App Pre-allocation failed: no server available
	APIN_PREALLOCATION_FAILED_CAPACITY = 0x1218, // Resource Pre-allocation failed: capacity exhausted
	APIN_REST_CALL_FAILED = 0x1219, // Error calling a remote module
	APIN_ALLOCATION_FAILED_NAME_CONFLICT = 0x121A, // App name parameter conflicts with a pending or completed task
	APIN_PREALLOCATION_FAILED_NO_RESOURCE = 0x121B, // App Pre-allocation failed: missing underlying resource
	APIN_APPLEASE_BAD_STATUS = 0x121C, // Unable to grant lease to requested app because it status prohibits it (DELETED/ARCHIVED)
	APIN_TEMPLATE_NOT_FOUND = 0x121D, // Template not found based on provided template ID and version
	APIN_PLATFORM_NOT_FOUND = 0x121E, // Platform not found based on provided platform version

	APIN_WOKWEB_ERROR_START = 0x1300,
	APIN_WOKWEB_ERROR_GENERIC = 0x1300, // Unspecific wokweb error
	APIN_WOKWEB_HTTP_ERROR_404 = 0x1310, // 404-not-found
	APIN_WOKWEB_HTTP_ERROR_500 = 0x1311, // 500 server error (currently including network error, timeout etc)
	APIN_WOKWEB_HTTP_ERROR_5xx = 0x1312, // 5xx Other server error
	APIN_WOKWEB_HTTP_ERROR_3xx = 0x1313, // 3xx temporarily unavailable
	APIN_WOKWEB_HTTP_ERROR_MISC = 0x1314, // Other HTTP status
	APIN_WOKWEB_BAD_RESULT = 0x1320, // Wokweb returned invalid result
	APIN_WOKWEB_REQUEST_FAILED = 0x1321, // Wokweb request failed with non-zero result code
	APIN_WOKWEB_ERROR_LAST = 0x13FF,

	APIN_PENDING = 0x2000,
	APIN_INITIALIZING = 0x2001, // System initialization in progress (unable to process any request)
	APIN_MAINTENANCE_IN_PROG = 0x2002, // Maintenance in progress (unable to process any request)
	APIN_POWERCYCLE_IN_PROG = 0x2003, // Another power cycle task in progress (unable to process another power cycle request)
};

#define NULL_APP_ID -1
#define NULL_MODULE_NAME "%"
#define NULL_SERVER_NAME "%"
#define NULL_TEMPLATE_ID -1
#define LIVE_TEMPLATE_VERSION "Live"
#define LIVE_TEMPLATE_REVISION ""
#define NULL_TASK_ID 0xFFFFFFFF
#define NULL_RES_ID -1
#define NULL_PORT -1

#define WORKER_THREAD_STOPPING_TIMEOUT 3000
#define HEARTBEAT_CACHE_EXPIRATION_TIME 60000 // threshold for old heartbeat in the cache just in case
#define DEFAULT_HOST_HEARTBEAT_EXPIRATION_TIME 60000 * 10 // host heartbeat expiration: 10 minutes(TEST)
#define DEFAULT_APP_HEARTBEAT_EXPIRATION_TIME 60000 * 5 // app heartbeat expiration: 5 minutes(TEST)

#define CLAMP_VALUE(x, min, max) (((x) < (min)) ? (min) : ((x) > (max)) ? (max) : (x))
#define CLAMP_VALUE_W_DEFAULT(x, min, max, def) (((x) < (min)) ? (def) : ((x) > (max)) ? (def) : (x))

//Event IDs
#define EVENT_COUNTER_REVIEW_TIMER 0x7FFFFFFF
#define EVENT_APPCONTROLLER_DOWN 101
#define EVENT_APPCONTROLLER_UP 102
#define EVENT_KGPSERVER_DOWN 103 // App heartbeat expired
#define EVENT_KGPSERVER_UP 104 // App heartbeat resumed
#define EVENT_RESERVED 105 // Reserved - old semantic no longer used
#define EVENT_DBERROR 106
#define EVENT_WOKWEBERROR 107
#define EVENT_APPPRECREATION_NORES 108
#define EVENT_RESPRECREATION_FAILED 109
#define EVENT_RESPRECREATION_LOWCAP 110
#define EVENT_RESPRECREATION_NOCAP 111
//Reserve 200-299 for task reporting
#define EVENT_TASK_COMPLETED_BASE 200
#define EVENT_TASK_COMPLETED_LAST 249
#define EVENT_TASK_FAILED_BASE 250
#define EVENT_TASK_FAILED_LAST 299
#define EVENT_TASK_COMPLETED(TYPE_ENUM) CLAMP_VALUE_W_DEFAULT(EVENT_TASK_COMPLETED_BASE + TYPE_ENUM, EVENT_TASK_COMPLETED_BASE, EVENT_TASK_COMPLETED_LAST, EVENT_TASK_COMPLETED_BASE)
#define EVENT_TASK_FAILED(TYPE_ENUM) CLAMP_VALUE_W_DEFAULT(EVENT_TASK_FAILED_BASE + TYPE_ENUM, EVENT_TASK_FAILED_BASE, EVENT_TASK_FAILED_LAST, EVENT_TASK_FAILED_BASE)

#define IF_STR_EMPTY(x, y) ((x).empty() ? (y) : (x))
#define IF_NULL(x, y) ((x) ? (x) : (y))
