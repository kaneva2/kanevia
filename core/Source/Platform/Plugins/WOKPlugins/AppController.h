///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Incubator.h"

namespace KEP {

class IAppController {
public:
	/**
	 * Start KGPServer process for a 3D app.
	 *
	 * @param	[in] id - 3D app ID
	 * @param	[in] port - port number - allow multi-server per game (WOK), use 0 if incubator.
	 * @param	[in] platformPath - Full path to Platform directory
	 * @param	[in] appDir - Full path to ServerFiles directory
	 * @param	[in] logicals - Semi-colon delimited logical key-value pairs for substitutions in parametrized configurations (e.g. GAME_ID=12345;DB_NAME=DB_12345). See TemplateRepository.
	 */
	virtual void StartServer(AppId id, unsigned int port, const std::string& platformPath, const std::string& appDir, const std::string& logicals, const std::string& callbackModuleName, unsigned int callbackData = NULL, const std::string& callbackHostName = "localhost") = 0;

	/**
	 * Stop KGPServer process for a 3D app.
	 *
	 * @param	[in] id - 3D app ID
	 * @param	[in] port - port number - allow multi-server per game (WOK), use 0 if incubator.
	 */
	virtual void StopServer(AppId id, unsigned int port, const std::string& callbackModuleName, unsigned int callbackData = NULL, const std::string& callbackHostName = "localhost") = 0;

	/**
	 * Notify KGPServer to get ready to go live
	 *
	 * @param	[in] id - 3D app ID
	 */
	virtual void BootstrapServer(AppId id) = 0;

	/** 
	 * Not used yet
	 */
	virtual void ReportMetrics(AppId id) = 0;
};

inline std::string GetAppControllerModuleName(const std::string& hostName) {
	std::string instanceName(MODULE_NAME_APPCONTROLLER);
	instanceName += "." + hostName;
	return instanceName;
}

AbstractPluginFactory* createPluginFactory(IAppController* pFactoryTypeHint);

} // namespace KEP
