///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "RestResultParser.h"

#define BEGIN_STUB_CLASS(INTF, MODULE_NAME)                                                 \
	class INTF##Stub : public I##INTF, public AbstractPlugin {                              \
	public:                                                                                 \
		INTF##Stub(const TiXmlElement *config) : AbstractPlugin(MODULE_NAME) {              \
			addSupportedInterface("I" #INTF);                                               \
			std::string remoteHost;                                                              \
			if (config->QueryValueAttribute("remote_host", &remoteHost) == TIXML_SUCCESS) { \
				m_remoteHost = remoteHost;                                                  \
			}                                                                               \
		}                                                                                   \
                                                                                            \
		virtual ~INTF##Stub() {}

#define BEGIN_STUB_CLASS2(INTF, INTF2, MODULE_NAME)                                         \
	class INTF##Stub : public I##INTF, public I##INTF2, public AbstractPlugin {             \
	public:                                                                                 \
		INTF##Stub(const TiXmlElement *config) : AbstractPlugin(MODULE_NAME) {              \
			addSupportedInterface("I" #INTF);                                               \
			addSupportedInterface("I" #INTF2);                                              \
			std::string remoteHost;                                                              \
			if (config->QueryValueAttribute("remote_host", &remoteHost) == TIXML_SUCCESS) { \
				m_remoteHost = remoteHost;                                                  \
			}                                                                               \
		}                                                                                   \
                                                                                            \
		virtual ~INTF##Stub() {}

#define BEGIN_STUB_CLASS3(INTF, INTF2, INTF3, MODULE_NAME)                                       \
	class INTF##Stub : public I##INTF, public I##INTF2, public I##INTF3, public AbstractPlugin { \
	public:                                                                                      \
		INTF##Stub(const TiXmlElement *config) : AbstractPlugin(MODULE_NAME) {                   \
			addSupportedInterface("I" #INTF);                                                    \
			addSupportedInterface("I" #INTF2);                                                   \
			addSupportedInterface("I" #INTF3);                                                   \
			std::string remoteHost;                                                                   \
			if (config->QueryValueAttribute("remote_host", &remoteHost) == TIXML_SUCCESS) {      \
				m_remoteHost = remoteHost;                                                       \
			}                                                                                    \
		}                                                                                        \
                                                                                                 \
		virtual ~INTF##Stub() {}

#define END_STUB_CLASS   \
private:                 \
	std::string m_remoteHost; \
	}                    \
	;

#define MARSHAL_DO_REMOTE_CALL(REMOTEHOST, URI, PARAMS, RESPARSER, ASYNC, TTL) \
	if (ASYNC)                                                                 \
		pRestClient->Invoke(REMOTEHOST, URI, PARAMS, TTL);                     \
	else                                                                       \
		pRestClient->Call(REMOTEHOST, URI, PARAMS, RESPARSER);

#define MARSHAL_SHARED__0(URI, FUNC, RESPARSER, ASYNC, TTL)                                                                 \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		pRestClient->Call(m_remoteHost, URI, std::map<std::string, std::string>(), RESPARSER);                              \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__1(URI, FUNC, A1, RESPARSER, ASYNC, TTL)                                                             \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		paramstream ss1;                                                                                                    \
		ss1 << A1;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__2(URI, FUNC, A1, A2, RESPARSER, ASYNC, TTL)                                                         \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2;                                                                                         \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__2__PARAMAP(URI, FUNC, A1, A2, PARAMAP, RESPARSER, ASYNC, TTL)                                       \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2;                                                                                         \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		std::map<std::string, std::string> params = PARAMAP;                                                                \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__3(URI, FUNC, A1, A2, A3, RESPARSER, ASYNC, TTL)                                                     \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3;                                                                                    \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__4(URI, FUNC, A1, A2, A3, A4, RESPARSER, ASYNC, TTL)                                                 \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3, ss4;                                                                               \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		ss4 << A4;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		params.insert(std::make_pair(#A4, ss4.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__5(URI, FUNC, A1, A2, A3, A4, A5, RESPARSER, ASYNC, TTL)                                             \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3, ss4, ss5;                                                                          \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		ss4 << A4;                                                                                                          \
		ss5 << A5;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		params.insert(std::make_pair(#A4, ss4.str()));                                                                      \
		params.insert(std::make_pair(#A5, ss5.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__6(URI, FUNC, A1, A2, A3, A4, A5, A6, RESPARSER, ASYNC, TTL)                                         \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3, ss4, ss5, ss6;                                                                     \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		ss4 << A4;                                                                                                          \
		ss5 << A5;                                                                                                          \
		ss6 << A6;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		params.insert(std::make_pair(#A4, ss4.str()));                                                                      \
		params.insert(std::make_pair(#A5, ss5.str()));                                                                      \
		params.insert(std::make_pair(#A6, ss6.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__7(URI, FUNC, A1, A2, A3, A4, A5, A6, A7, RESPARSER, ASYNC, TTL)                                     \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3, ss4, ss5, ss6, ss7;                                                                \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		ss4 << A4;                                                                                                          \
		ss5 << A5;                                                                                                          \
		ss6 << A6;                                                                                                          \
		ss7 << A7;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		params.insert(std::make_pair(#A4, ss4.str()));                                                                      \
		params.insert(std::make_pair(#A5, ss5.str()));                                                                      \
		params.insert(std::make_pair(#A6, ss6.str()));                                                                      \
		params.insert(std::make_pair(#A7, ss7.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__8(URI, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, RESPARSER, ASYNC, TTL)                                 \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8;                                                           \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		ss4 << A4;                                                                                                          \
		ss5 << A5;                                                                                                          \
		ss6 << A6;                                                                                                          \
		ss7 << A7;                                                                                                          \
		ss8 << A8;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		params.insert(std::make_pair(#A4, ss4.str()));                                                                      \
		params.insert(std::make_pair(#A5, ss5.str()));                                                                      \
		params.insert(std::make_pair(#A6, ss6.str()));                                                                      \
		params.insert(std::make_pair(#A7, ss7.str()));                                                                      \
		params.insert(std::make_pair(#A8, ss8.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__9(URI, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, A9, RESPARSER, ASYNC, TTL)                             \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8, ss9;                                                      \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		ss4 << A4;                                                                                                          \
		ss5 << A5;                                                                                                          \
		ss6 << A6;                                                                                                          \
		ss7 << A7;                                                                                                          \
		ss8 << A8;                                                                                                          \
		ss9 << A9;                                                                                                          \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		params.insert(std::make_pair(#A4, ss4.str()));                                                                      \
		params.insert(std::make_pair(#A5, ss5.str()));                                                                      \
		params.insert(std::make_pair(#A6, ss6.str()));                                                                      \
		params.insert(std::make_pair(#A7, ss7.str()));                                                                      \
		params.insert(std::make_pair(#A8, ss8.str()));                                                                      \
		params.insert(std::make_pair(#A9, ss9.str()));                                                                      \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_SHARED__10(URI, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, RESPARSER, ASYNC, TTL)                       \
	if (m_remoteHost.empty()) {                                                                                             \
		throw new KEP::KEPException("Remote host parameter not defined in configuration for a stub class", APIN_ERR_BAD_CONFIG); \
	}                                                                                                                       \
                                                                                                                            \
	IRESTClient *pRestClient = GET_PLUGIN_INTERFACE(this->host(), IRESTClient);                                             \
	if (pRestClient) {                                                                                                      \
		std::stringstream ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8, ss9, ss10;                                                \
		ss1 << A1;                                                                                                          \
		ss2 << A2;                                                                                                          \
		ss3 << A3;                                                                                                          \
		ss4 << A4;                                                                                                          \
		ss5 << A5;                                                                                                          \
		ss6 << A6;                                                                                                          \
		ss7 << A7;                                                                                                          \
		ss8 << A8;                                                                                                          \
		ss9 << A9;                                                                                                          \
		ss10 << A10;                                                                                                        \
		std::map<std::string, std::string> params;                                                                          \
		params.insert(std::make_pair(#A1, ss1.str()));                                                                      \
		params.insert(std::make_pair(#A2, ss2.str()));                                                                      \
		params.insert(std::make_pair(#A3, ss3.str()));                                                                      \
		params.insert(std::make_pair(#A4, ss4.str()));                                                                      \
		params.insert(std::make_pair(#A5, ss5.str()));                                                                      \
		params.insert(std::make_pair(#A6, ss6.str()));                                                                      \
		params.insert(std::make_pair(#A7, ss7.str()));                                                                      \
		params.insert(std::make_pair(#A8, ss8.str()));                                                                      \
		params.insert(std::make_pair(#A9, ss9.str()));                                                                      \
		params.insert(std::make_pair(#A10, ss10.str()));                                                                    \
		MARSHAL_DO_REMOTE_CALL(m_remoteHost, URI, params, RESPARSER, ASYNC, TTL);                                           \
	} else {                                                                                                                \
		throw new KEP::KEPException("RestClient plugin not available", APIN_ERR_MISSING_PLUGIN);                            \
	}

#define MARSHAL_PROC__0(FUNC)                                      \
	virtual void FUNC() {                                          \
		MARSHAL_SHARED__0("/" #FUNC, FUNC, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__0(FUNC, RETYPE, PARSER)                         \
	virtual RETYPE FUNC() {                                           \
		RETYPE res;                                                   \
		PARSER parser(res);                                           \
		MARSHAL_SHARED__0("/" #FUNC, FUNC, &parser, false, INFINITE); \
		return res;                                                   \
	}

#define MARSHAL_PROC__1(FUNC, T1, A1)                                  \
	virtual void FUNC(T1 A1) {                                         \
		MARSHAL_SHARED__1("/" #FUNC, FUNC, A1, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__1(FUNC, T1, A1, RETYPE, PARSER)                     \
	virtual RETYPE FUNC(T1 A1) {                                          \
		RETYPE res;                                                       \
		PARSER parser(res);                                               \
		MARSHAL_SHARED__1("/" #FUNC, FUNC, A1, &parser, false, INFINITE); \
		return res;                                                       \
	}

#define MARSHAL_PROC__2(FUNC, T1, A1, T2, A2)                              \
	virtual void FUNC(T1 A1, T2 A2) {                                      \
		MARSHAL_SHARED__2("/" #FUNC, FUNC, A1, A2, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__2(FUNC, T1, A1, T2, A2, RETYPE, PARSER)                 \
	virtual RETYPE FUNC(T1 A1, T2 A2) {                                       \
		RETYPE res;                                                           \
		PARSER parser(res);                                                   \
		MARSHAL_SHARED__2("/" #FUNC, FUNC, A1, A2, &parser, false, INFINITE); \
		return res;                                                           \
	}

#define MARSHAL_PROC__2_PARAMAP(FUNC, T1, A1, T2, A2, PARAMAP)                               \
	virtual void FUNC(T1 A1, T2 A2, const std::map<std::string, std::string> &PARAMAP) {     \
		MARSHAL_SHARED__2__PARAMAP("/" #FUNC, FUNC, A1, A2, PARAMAP, NULL, false, INFINITE); \
	}

#define MARSHAL_PROC__3(FUNC, T1, A1, T2, A2, T3, A3)                          \
	virtual void FUNC(T1 A1, T2 A2, T3 A3) {                                   \
		MARSHAL_SHARED__3("/" #FUNC, FUNC, A1, A2, A3, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__3(FUNC, T1, A1, T2, A2, T3, A3, RETYPE, PARSER)             \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3) {                                    \
		RETYPE res;                                                               \
		PARSER parser(res);                                                       \
		MARSHAL_SHARED__3("/" #FUNC, FUNC, A1, A2, A3, &parser, false, INFINITE); \
		return res;                                                               \
	}

#define MARSHAL_PROC__4(FUNC, T1, A1, T2, A2, T3, A3, T4, A4)                      \
	virtual void FUNC(T1 A1, T2 A2, T3 A3, T4 A4) {                                \
		MARSHAL_SHARED__4("/" #FUNC, FUNC, A1, A2, A3, A4, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__4(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, RETYPE, PARSER)         \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3, T4 A4) {                                 \
		RETYPE res;                                                                   \
		PARSER parser(res);                                                           \
		MARSHAL_SHARED__4("/" #FUNC, FUNC, A1, A2, A3, A4, &parser, false, INFINITE); \
		return res;                                                                   \
	}

#define MARSHAL_PROC__5(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5)                  \
	virtual void FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5) {                             \
		MARSHAL_SHARED__5("/" #FUNC, FUNC, A1, A2, A3, A4, A5, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__5(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, RETYPE, PARSER)     \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5) {                              \
		RETYPE res;                                                                       \
		PARSER parser(res);                                                               \
		MARSHAL_SHARED__5("/" #FUNC, FUNC, A1, A2, A3, A4, A5, &parser, false, INFINITE); \
		return res;                                                                       \
	}

#define MARSHAL_PROC__6(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6)              \
	virtual void FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6) {                          \
		MARSHAL_SHARED__6("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__6(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, RETYPE, PARSER) \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6) {                           \
		RETYPE res;                                                                           \
		PARSER parser(res);                                                                   \
		MARSHAL_SHARED__6("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, &parser, false, INFINITE); \
		return res;                                                                           \
	}

#define MARSHAL_PROC__7(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7)          \
	virtual void FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7) {                       \
		MARSHAL_SHARED__7("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__7(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7, RETYPE, PARSER) \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7) {                            \
		RETYPE res;                                                                                   \
		PARSER parser(res);                                                                           \
		MARSHAL_SHARED__7("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, &parser, false, INFINITE);     \
		return res;                                                                                   \
	}

#define MARSHAL_PROC__8(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7, T8, A8)      \
	virtual void FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7, T8 A8) {                    \
		MARSHAL_SHARED__8("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__8(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7, T8, A8, RETYPE, PARSER) \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7, T8 A8) {                             \
		RETYPE res;                                                                                           \
		PARSER parser(res);                                                                                   \
		MARSHAL_SHARED__8("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, &parser, false, INFINITE);         \
		return res;                                                                                           \
	}

#define MARSHAL_PROC__9(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7, T8, A8, T9, A9)  \
	virtual void FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7, T8 A8, T9 A9) {                 \
		MARSHAL_SHARED__9("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, A9, NULL, false, INFINITE); \
	}

#define MARSHAL_FUNC__9(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7, T8, A8, T9, A9, RETYPE, PARSER) \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7, T8 A8, T9 A9) {                              \
		RETYPE res;                                                                                                   \
		PARSER parser(res);                                                                                           \
		MARSHAL_SHARED__9("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, A9, &parser, false, INFINITE);             \
		return res;                                                                                                   \
	}

#define MARSHAL_PROC__10(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7, T8, A8, T9, A9, T10, A10) \
	virtual void FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7, T8 A8, T9 A9, T10 A10) {                  \
		MARSHAL_SHARED__10("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, NULL, false, INFINITE);     \
	}

#define MARSHAL_FUNC__10(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, T7, A7, T8, A8, T9, A9, T10, A10, RETYPE, PARSER) \
	virtual RETYPE FUNC(T1 A1, T2 A2, T3 A3, T4 A4, T5 A5, T6 A6, T7 A7, T8 A8, T9 A9, T10 A10) {                                \
		RETYPE res;                                                                                                              \
		PARSER parser(res);                                                                                                      \
		MARSHAL_SHARED__10("/" #FUNC, FUNC, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, &parser, false, INFINITE);                  \
		return res;                                                                                                              \
	}

#define MARSHAL_PROC__0CB(FUNC) \
	MARSHAL_PROC__3(FUNC, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_FUNC__0CB(FUNC) \
	MARSHAL_FUNC__3(FUNC, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_PROC__1CB(FUNC, T1, A1) \
	MARSHAL_PROC__4(FUNC, T1, A1, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_FUNC__1CB(FUNC, T1, A1) \
	MARSHAL_FUNC__4(FUNC, T1, A1, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_PROC__2CB(FUNC, T1, A1, T2, A2) \
	MARSHAL_PROC__5(FUNC, T1, A1, T2, A2, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_FUNC__2CB(FUNC, T1, A1, T2, A2) \
	MARSHAL_FUNC__5(FUNC, T1, A1, T2, A2, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_PROC__3CB(FUNC, T1, A1, T2, A2, T3, A3) \
	MARSHAL_PROC__6(FUNC, T1, A1, T2, A2, T3, A3, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_FUNC__3CB(FUNC, T1, A1, T2, A2, T3, A3) \
	MARSHAL_FUNC__6(FUNC, T1, A1, T2, A2, T3, A3, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_PROC__4CB(FUNC, T1, A1, T2, A2, T3, A3, T4, A4) \
	MARSHAL_PROC__7(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_FUNC__4CB(FUNC, T1, A1, T2, A2, T3, A3, T4, A4) \
	MARSHAL_FUNC__7(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_PROC__5CB(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5) \
	MARSHAL_PROC__8(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_FUNC__5CB(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5) \
	MARSHAL_FUNC__8(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_PROC__6CB(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6) \
	MARSHAL_PROC__9(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
#define MARSHAL_FUNC__6CB(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6) \
	MARSHAL_FUNC__9(FUNC, T1, A1, T2, A2, T3, A3, T4, A4, T5, A5, T6, A6, const std::string &, _callback, unsigned int, _callbackdata, const std::string &, _callbackhostname)
