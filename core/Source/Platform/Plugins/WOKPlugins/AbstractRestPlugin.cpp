///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "log4cplus\logger.h"

#include "HTTPServer.h"

#include "TaskSystem.h"
#include "TaskObject.h"

#include "Incubator.h"
#include "AbstractRestPlugin.h"
#include "Notifier.h"
#include <common/include/KEPException.h>

using namespace log4cplus;

namespace KEP {

class AsyncNotificationTask : public TaskObject {
public:
	AsyncNotificationTask(AbstractRESTPlugin* handler, const std::string& type, const std::map<std::string, std::string>& params) :
			m_handler(handler), m_type(type), m_params(params) {
	}

	void ExecuteTask() {
		try {
			m_handler->_doNotify(m_type, m_params);
		} catch (KEPException* e) {
			LOG4CPLUS_ERROR(m_handler->getLogger(), "Unhandled exception in async notification task for module[" << m_handler->AbstractPlugin::name() << "]"
																												 << ", type=[" << m_type << "]: " << e->ToString());
			delete e;
		} catch (const KEPException& e) {
			LOG4CPLUS_ERROR(m_handler->getLogger(), "Unhandled exception in async notification task for module[" << m_handler->AbstractPlugin::name() << "]"
																												 << ", type=[" << m_type << "]: " << e.ToString());
		} catch (...) {
			LOG4CPLUS_ERROR(m_handler->getLogger(), "Unhandled exception in async notification task for module[" << m_handler->AbstractPlugin::name() << "]"
																												 << ", type=[" << m_type << "]: Unknown Exception");
		}
	}

private:
	AbstractRESTPlugin* m_handler;
	std::string m_type;
	std::map<std::string, std::string> m_params;
};

AbstractRESTPlugin::AbstractRESTPlugin(const char* name, bool listenToNotification /*= false*/, bool asyncNotification /*= false*/, const char* loggerName /*= NULL*/) :
		AbstractPlugin(name),
		HTTPServer::RequestHandler(name),
		m_listenToNotification(listenToNotification),
		m_port(0),
		m_asyncNotification(asyncNotification),
		m_asyncNotificationQueue(NULL),
		m_logger(Logger::getInstance(Utf8ToUtf16(loggerName != NULL ? loggerName : name != NULL ? name : "UnknownPlugin"))) {
	if (m_listenToNotification && m_asyncNotification) {
		m_asyncNotificationQueue = new TaskSystem("AbstractRESTPlugin");
	}
}

AbstractRESTPlugin::~AbstractRESTPlugin() {
	if (m_asyncNotificationQueue) {
		delete m_asyncNotificationQueue;
		m_asyncNotificationQueue = NULL;
	}
}

void AbstractRESTPlugin::addEndPoint(const std::string& ep) {
	m_restEndPoints.push_back(ep);
}

Plugin& AbstractRESTPlugin::startPlugin() {
	Plugin& res = AbstractPlugin::startPlugin();

	HTTPServer* pHttpServer = GET_PLUGIN_INTERFACE(host(), HTTPServer);

	if (pHttpServer) {
		for (auto it = m_restEndPoints.begin(); it != m_restEndPoints.end(); ++it) {
			pHttpServer->registerHandler(*this, *it);
		}
	} else {
		throw new KEPException("HttpServer plugin unavailable", APIN_ERR_MISSING_PLUGIN);
	}

	if (m_listenToNotification) {
		INotifier* pNotifier = GET_PLUGIN_INTERFACE(host(), INotifier);

		if (pNotifier) {
			pNotifier->AddListener(AbstractPlugin::name(), this);
		} else {
			//TODO: log error
		}

		if (m_asyncNotification) {
			m_asyncNotificationQueue->Initialize(1);
		}
	}

	return res;
}

ACTPtr AbstractRESTPlugin::stopPlugin() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin()");
	if (m_listenToNotification) {
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): Finalizing asyncNotification");
		if (m_asyncNotification) {
			m_asyncNotificationQueue->Finalize();
		}

		INotifier* pNotifier = GET_PLUGIN_INTERFACE(host(), INotifier);
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): Acquiring notifier plugin");

		if (pNotifier) {
			LOG4CPLUS_DEBUG(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): Removing listener");
			pNotifier->RemoveListener(MODULE_NAME_ALLOCATOR);
		} else {
			LOG4CPLUS_ERROR(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): Unabled to acquire notifier plugin");
			//TODO: log error
		}
	}

	LOG4CPLUS_DEBUG(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): Acquiring HTTPServer plugin");
	HTTPServer* pHttpServer = GET_PLUGIN_INTERFACE(host(), HTTPServer);

	if (pHttpServer) {
		LOG4CPLUS_DEBUG(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): Deregistering HTTP request handlers");
		for (auto it = m_restEndPoints.begin(); it != m_restEndPoints.end(); ++it) {
			pHttpServer->deregisterHandler(*it);
		}
	} else {
		LOG4CPLUS_ERROR(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): Failed to acquire HTTPServer plugin!");
		throw new KEPException("HttpServer plugin unavailable", APIN_ERR_MISSING_PLUGIN);
	}

	LOG4CPLUS_DEBUG(Logger::getInstance(L"Incubator"), "AbstractRestPlugin::stopPlugin(): return.");
	return AbstractPlugin::stopPlugin();
}

void AbstractRESTPlugin::Notify(const std::string& type, const std::map<std::string, std::string>& params) {
	if (m_asyncNotification) {
		if (m_asyncNotificationQueue) {
			//Queue notification task for async processing
			m_asyncNotificationQueue->QueueTask(new AsyncNotificationTask(this, type, params));
		} else {
			//Something is wrong
			LOG4CPLUS_ERROR(m_logger, "Unable to queue async notification because queue is not initialized: notification type = " << type);
		}
	} else {
		//Call notification handler directly
		_doNotify(type, params);
	}
}

} // namespace KEP