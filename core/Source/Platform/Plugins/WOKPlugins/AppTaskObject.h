///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "js.h"
#include "jsEvent.h"

#include "TaskObject.h"
#include <common/include/KEPException.h>

class AppTaskObject : public TaskObject {
public:
	AppTaskObject() :
			m_exception(NULL), m_returnCode(0) {}
	~AppTaskObject() { ClearException(); }

	void WaitForCompletion(bool throwIfException = true) {
		m_completionEvent.wait();

		if (m_exception) {
			if (throwIfException) {
				throw m_exception;
			}
		}

		ClearException();
	}

	int GetReturnCode() const { return m_returnCode; }

	virtual void GetTaskStatus(map<string, string>& state) = 0;

protected:
	void NotifyCompletion() { m_completionEvent.releaseWaiters(); }

	KEPException* GetException() { return m_exception; }

	void ClearException() {
		if (m_exception) {
			delete m_exception;
		}
		m_exception = NULL;
	}

	void ExecuteTask() {
		try {
			m_returnCode = ExecuteAppTask();
		} catch (KEPException* e) {
			m_exception = e;
		} catch (const KEPException& e) {
			m_exception = new KEPException(e.m_msg.c_str(), e.m_err, e.m_file.c_str(), e.m_line);
		}

		NotifyCompletion();
	}

	virtual int ExecuteAppTask() = 0;

private:
	jsEvent m_completionEvent;
	KEPException* m_exception;
	int m_returnCode;
};
