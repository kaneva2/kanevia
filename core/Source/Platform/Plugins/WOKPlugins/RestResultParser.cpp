///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "TinyXML/tinyxml.h"

#include "Incubator.h"
#include "RestResultParser.h"
#include "Core/Util/ErrorSpec.h"

namespace KEP {

void IntParser::parse(const void* content, size_t contentLen) {
	setResponseContent(content, contentLen);
	m_output = -1;

	TiXmlDocument doc;
	doc.Parse(responseContent().c_str());

	TiXmlElement* elem = doc.FirstChildElement("result");
	if (elem && elem->GetText()) {
		m_output = atoi(elem->GetText());
	}
}

void UIntParser::parse(const void* content, size_t contentLen) {
	setResponseContent(content, contentLen);
	m_output = (unsigned int)-1;

	TiXmlDocument doc;
	doc.Parse(responseContent().c_str());

	TiXmlElement* elem = doc.FirstChildElement("result");
	if (elem && elem->GetText()) {
		m_output = strtoul(elem->GetText(), NULL, 10);
	}
}

void StringParser::parse(const void* content, size_t contentLen) {
	setResponseContent(content, contentLen);
	m_output = -1;

	TiXmlDocument doc;
	doc.Parse(responseContent().c_str());

	TiXmlElement* elem = doc.FirstChildElement(m_tag.c_str());
	if (elem) {
		const char* tmp = elem->GetText();
		m_output = tmp ? tmp : "";
	}
}

// Marshal rest response for generalized error specification
//
// <error code="{errcode}">{errmsg}</error>"
//
ErrorSpecParser::ErrorSpecParser(ErrorSpec& spec, const std::string& tag) :
		RestResultParser(tag), m_output(spec) {}
void ErrorSpecParser::parse(const void* content, size_t contentLen) {
	setResponseContent(content, contentLen);
	//	ErrorSpec error;

	TiXmlDocument doc;
	doc.Parse(responseContent().c_str());

	TiXmlElement* elem = doc.FirstChildElement(m_tag.c_str());
	if (!elem)
		return;

	const char* pcCode = elem->Attribute("code");
	if (pcCode == 0)
		return; // log warning

	m_output.first = atoi(pcCode);

	const char* pcReason = elem->GetText();
	if (pcReason == 0)
		return; // log warning

	m_output.second = pcReason;
}

} // namespace KEP