///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "TinyXML/tinyxml.h"
#include "log4cplus/logger.h"
#include "js.h"
#include "jsThread.h"
#include "jsLock.h"
#include "jsSemaphore.h"
#include "TaskObject.h"
#include "TaskSystem.h"
#include "HTTPServer.h"
#include "Mailer.h"
#include "AlertService.h"
#include "RestClient.h"
#include "AbstractRestPlugin.h"
#include "Utils.h"
#include "RestHandlerHelper.h"
#include "StubHelper.h"

using namespace log4cplus;

namespace KEP {

///////////////////////////////////////////////////////////////////////////
// Alert Policy Data
struct AlertMessage {
	std::string subject;
	std::string body;
};

struct AlertTrigger {
	std::string source;
	std::string preState;
	int eventId;
	UINT eventCount;
	UINT durationMins;

	AlertTrigger() :
			eventId(0), eventCount(1), durationMins(0) {}
};

struct AlertAction {
	int level;
	int messageId;
	std::string postState;

	AlertAction() :
			level(0), messageId(0) {}
};

struct AlertRule {
	AlertTrigger trigger;
	AlertAction action;
};

struct EventSourceState {
	std::string currState;
	UINT64 counterTimestamp;
	int currEvent;
	UINT currEventCount;

	EventSourceState() :
			counterTimestamp(0), currEvent(0), currEventCount(0) {}
};

struct AlertPolicy {
	std::string name;
	bool enabled;
	std::string initState;
	std::vector<AlertRule> rules;

	jsFastSync sync;
	std::map<std::string, EventSourceState> runtimeStates;

	AlertPolicy() :
			enabled(true) {}
};

typedef std::map<int, AlertMessage *> MessageMap;
typedef std::vector<AlertPolicy *> Policies;

///////////////////////////////////////////////////////////////////////////
// Alert Service Plugin
class AlertServicePlugin : public IAlertService, public AbstractRESTPlugin, public TaskSystem, public jsThread {
public:
	AlertServicePlugin(const TiXmlElement *config);
	~AlertServicePlugin();

	virtual Plugin &startPlugin();
	virtual ACTPtr stopPlugin();

	virtual int handle(const HTTPServer::Request &request, HTTPServer::EndPoint &endPoint);

	virtual void ReportEvent(const std::string &source, int eventId);
	virtual void SendAlert(AlertLevel level, const std::string &subject, const std::string &body);

protected:
	void _reportEvent(UINT64 timestamp, const std::string &source, int eventId);

	bool _processTimer(UINT64 timestamp, AlertPolicy &policy, const AlertRule &rule);
	bool _processEvent(UINT64 timestamp, const std::string &source, int eventId, AlertPolicy &policy, const AlertRule &rule);

	void _executeAlertAction(const std::string &source, AlertPolicy &policy, const AlertRule &rule, const EventSourceState *stateSnapshot);
	void _updateSourceState(const std::string &source, AlertPolicy &policy, const std::string &newState);

	void _sendAlert(AlertLevel level, const std::string &subject, const std::string &body);

	std::string _formatText(const std::string &format, const std::string &source, const AlertRule &rule, const EventSourceState *stateSnapshot);

	ULONG _doWork();

	static AlertLevel ParseAlertLevel(const std::string &sLevel);
	static std::string FormatAlertLevel(AlertLevel level);

	static std::string AlertLevelNames[NUM_ALERT_LEVELS];

private:
	MessageMap m_messageMap;
	Policies m_policies;

	jsSemaphore m_semExiting;

	friend class ReportEventTask;
};

class ReportEventTask : public TaskObject {
public:
	ReportEventTask(AlertServicePlugin &alertService, const std::string &source, int eventId) :
			m_alertService(alertService), m_source(source), m_eventId(eventId) {
		m_timestamp = GetSystemTimeUINT64();
	}

	virtual void ExecuteTask() {
		m_alertService._reportEvent(m_timestamp, m_source, m_eventId);
	}

private:
	AlertServicePlugin &m_alertService;
	std::string m_source;
	int m_eventId;
	UINT64 m_timestamp;
};

std::string AlertServicePlugin::AlertLevelNames[NUM_ALERT_LEVELS] = { "GRN", "BLU", "YEL", "ORN", "RED" };

AlertServicePlugin::AlertServicePlugin(const TiXmlElement *config) :
		TaskSystem("AlertServicePlugin"),
		AbstractRESTPlugin(MODULE_NAME_ALERTSERVICE),
		jsThread(MODULE_NAME_ALERTSERVICE) {
	addSupportedInterface(INTERFACE_NAME_ALERTSERVICE);

	if (config) {
		const TiXmlElement *elem;

		// <messages>
		int messageCount = 0;
		const TiXmlElement *elemMessages = config->FirstChildElement("messages");
		if (elemMessages) {
			for (const TiXmlElement *elemMessage = elemMessages->FirstChildElement("message"); elemMessage; elemMessage = elemMessage->NextSiblingElement("message"), messageCount++) {
				int messageId = 0;
				if (TIXML_SUCCESS == elemMessage->QueryIntAttribute("id", &messageId)) {
					elem = elemMessage->FirstChildElement("subject");
					if (elem && elem->GetText() && *elem->GetText()) {
						AlertMessage *message = new AlertMessage();
						message->subject = elem->GetText();

						elem = elemMessage->FirstChildElement("body");
						if (elem && elem->GetText() && *elem->GetText()) {
							message->body = elem->GetText();
						} else {
							message->body = message->subject;
						}

						//Save current entry
						m_messageMap.insert(std::make_pair(messageId, message));

						LOG4CPLUS_INFO(m_logger, "Init: messages[" << messageId << "] = {" << message->subject << ", " << message->body << "}");
					} else {
						LOG4CPLUS_WARN(m_logger, "Init: Invalid/missing <subject> for <message> ID:" << messageId << ", tag skipped");
					}
				} else {
					LOG4CPLUS_WARN(m_logger, "Init: Invalid/missing id attribute for <message> #" << messageCount << ", tag skipped");
				}
			}
		}

		LOG4CPLUS_INFO(m_logger, "Init: *** Loaded " << messageCount << " alert messages ***");

		// <policies>
		int policyCount = 0;
		const TiXmlElement *elemPolicies = config->FirstChildElement("policies");
		if (elemPolicies) {
			for (const TiXmlElement *elemPolicy = elemPolicies->FirstChildElement("policy"); elemPolicy; elemPolicy = elemPolicy->NextSiblingElement("policy"), policyCount++) {
				AlertPolicy *policy = new AlertPolicy();

				//policy name: optional
				if (elemPolicy->Attribute("name")) {
					policy->name = elemPolicy->Attribute("name");
				}

				//enabled flag: optional, default = 1
				int enabled = 1;
				if (TIXML_SUCCESS == elemPolicy->QueryIntAttribute("enabled", &enabled)) {
					policy->enabled = enabled != 0;
				}

				//initial state: optional, default=EMPTY
				elem = elemPolicy->FirstChildElement("initial_state");
				if (elem && elem->GetText() && *elem->GetText()) {
					policy->initState = elem->GetText();
				}

				//alert rules
				int ruleCount = 0;
				const TiXmlElement *elemRules = elemPolicy->FirstChildElement("rules");
				if (elemRules) {
					for (const TiXmlElement *elemRule = elemRules->FirstChildElement("rule"); elemRule; elemRule = elemRule->NextSiblingElement("rule"), ruleCount++) {
						AlertRule rule;

						const TiXmlElement *elemTrigger = elemRule->FirstChildElement("trigger");
						if (!elemTrigger) {
							//missing mandatory fields
							LOG4CPLUS_WARN(m_logger, "Init: missing <trigger> element for rule#" << ruleCount << " of policy#" << policyCount << ", rule ignored");
							delete policy;
							break;
						}

						//trigger event Id (required)
						elem = elemTrigger->FirstChildElement("event");
						if (elem && elem->GetText() && *elem->GetText()) {
							rule.trigger.eventId = atoi(elem->GetText());
						} else {
							//missing mandatory field
							LOG4CPLUS_WARN(m_logger, "Init: missing <trigger><event> element for rule#" << ruleCount << " of policy#" << policyCount << ", rule ignored");
							delete policy;
							break;
						}

						//trigger event source: optional, default=EMPTY (ANY)
						elem = elemTrigger->FirstChildElement("source");
						if (elem && elem->GetText() && *elem->GetText()) {
							rule.trigger.source = elem->GetText();
						}

						//trigger preState: optional, default=EMPTY (dont care)
						elem = elemTrigger->FirstChildElement("state");
						if (elem && elem->GetText() && *elem->GetText()) {
							rule.trigger.preState = elem->GetText();
						}

						//trigger event count: optional, default=1
						elem = elemTrigger->FirstChildElement("count");
						if (elem && elem->GetText() && *elem->GetText()) {
							rule.trigger.eventCount = std::max(atoi(elem->GetText()), 1);
						}

						//event sampling duration: optional, default=0 (as long as it takes)
						elem = elemTrigger->FirstChildElement("duration");
						if (elem && elem->GetText() && *elem->GetText()) {
							rule.trigger.durationMins = atoi(elem->GetText());
						}

						//post state: optional, default=EMPTY (no change)
						elem = elemRule->FirstChildElement("new_state");
						if (elem && elem->GetText() && *elem->GetText()) {
							rule.action.postState = elem->GetText();
						}

						//alert level and message ID: optional, default message ID=0 (no alert)
						const TiXmlElement *elemAlert = elemRule->FirstChildElement("alert");
						if (elemAlert) {
							const TiXmlElement *elemLvl = elemAlert->FirstChildElement("level");
							const TiXmlElement *elemMsg = elemAlert->FirstChildElement("message");
							if (elemLvl && elemLvl->GetText() && *elemLvl->GetText() &&
								elemMsg && elemMsg->GetText() && *elemMsg->GetText() && atoi(elemMsg->GetText()) != 0) {
								rule.action.level = ParseAlertLevel(elemLvl->GetText());
								rule.action.messageId = atoi(elemMsg->GetText());
							}
						}

						policy->rules.push_back(rule);
					}
				}

				m_policies.push_back(policy);
				LOG4CPLUS_INFO(m_logger, "Init: policy[" << policyCount << "] = [" << policy->name << "], enabled=" << (policy->enabled ? "Y" : "N") << ", " << ruleCount << " rules");
			}
		}

		LOG4CPLUS_INFO(m_logger, "Init: *** Loaded " << policyCount << " policies ***");
	}

	addEndPoint("/ReportEvent");
	addEndPoint("/SendAlert");
}

AlertServicePlugin::~AlertServicePlugin() {
	for (Policies::iterator it = m_policies.begin(); it != m_policies.end(); ++it) {
		delete *it;
	}

	m_policies.clear();
}

Plugin &AlertServicePlugin::startPlugin() {
	Plugin &res = AbstractRESTPlugin::startPlugin();

	//TaskSystem Init
	Initialize(1);

	//Start worker thread
	jsThread::start();

	return res;
}

ACTPtr AlertServicePlugin::stopPlugin() {
	//Terminate worker thread
	m_semExiting.increment();
	jsThread::join(WORKER_THREAD_STOPPING_TIMEOUT);

	//Reset runtime state
	for (Policies::iterator it = m_policies.begin(); it != m_policies.end(); ++it) {
		(*it)->runtimeStates.clear();
	}

	//TaskSystem Cleanup
	Finalize();
	return AbstractRESTPlugin::stopPlugin();
}

void AlertServicePlugin::ReportEvent(const std::string &source, int eventId) {
	LOG4CPLUS_INFO(m_logger, "ReportEvent: source=[" << source << "], event=" << eventId);
	QueueTask(new ReportEventTask(*this, source, eventId));
}

void AlertServicePlugin::SendAlert(AlertLevel level, const std::string &subject, const std::string &body) {
	LOG4CPLUS_INFO(m_logger, "SendAlert: [" << subject << "], level=" << FormatAlertLevel(level) << ", subject=" << subject << ", body=" << body);
	_sendAlert(level, subject, body);
}

void AlertServicePlugin::_reportEvent(UINT64 timestamp, const std::string &source, int eventId) {
	for (Policies::iterator itPol = m_policies.begin(); itPol != m_policies.end(); ++itPol) {
		AlertPolicy &policy = **itPol;

		//For each policy enabled
		if (policy.enabled) {
			//Iterate through policy rules
			for (std::vector<AlertRule>::iterator itRul = policy.rules.begin(); itRul != policy.rules.end(); ++itRul) {
				AlertRule &rule = *itRul;

				switch (eventId) {
					case EVENT_COUNTER_REVIEW_TIMER:
						//Timer event for counter review
						_processTimer(timestamp, policy, rule);
						break;
					default:
						//Standard events
						_processEvent(timestamp, source, eventId, policy, rule);
						break;
				}
			}
		}
	}
}

bool AlertServicePlugin::_processTimer(UINT64 timestamp, AlertPolicy &policy, const AlertRule &rule) {
	bool res = false;

	if (rule.trigger.durationMins != 0) {
		jsFastLock lock(policy.sync);

		for (std::map<std::string, EventSourceState>::iterator it = policy.runtimeStates.begin(); it != policy.runtimeStates.end(); ++it) {
			const std::string &source = it->first;
			EventSourceState &ess = it->second;
			if ((rule.trigger.preState.empty() || ess.currState == rule.trigger.preState) &&
				timestamp - ess.counterTimestamp >= rule.trigger.durationMins * 60 * NUM_CLUNKS_PER_SECOND &&
				ess.currEventCount >= rule.trigger.eventCount) {
				//State Snapshot
				EventSourceState snapshot = ess;

				//flush counter
				ess.currEventCount = 0;
				ess.counterTimestamp = timestamp;

				//Fire alert action
				_executeAlertAction(source, policy, rule, &snapshot);
				res = true;
			}
		}
	}

	return res;
}

bool AlertServicePlugin::_processEvent(UINT64 timestamp, const std::string &source, int eventId, AlertPolicy &policy, const AlertRule &rule) {
	bool res = false;

	if (rule.trigger.eventId == eventId && (rule.trigger.source.empty() || rule.trigger.source == source)) {
		if (rule.trigger.preState.empty() && rule.trigger.durationMins == 0 && rule.trigger.eventCount == 1) {
			//Simple trigger: shortcut state management
			res = true;
			_executeAlertAction(source, policy, rule, NULL);
		} else {
			//Standard trigger
			jsFastLock lock(policy.sync);

			bool isNew = policy.runtimeStates.find(source) == policy.runtimeStates.end();

			EventSourceState &ess = policy.runtimeStates[source];

			if (isNew) {
				ess.currState = policy.initState;
			}

			if (rule.trigger.preState.empty() || ess.currState == rule.trigger.preState) {
				//Reset counter if different event
				if (ess.currEvent != eventId) {
					ess.counterTimestamp = 0;
					ess.currEvent = eventId;
					ess.currEventCount = 0;
				}

				//Record counter timestamp if first time
				if (ess.counterTimestamp == 0) {
					ess.counterTimestamp = timestamp;
				}

				//counter increment
				ess.currEventCount++;

				if (ess.currEventCount == rule.trigger.eventCount && rule.trigger.durationMins == 0) {
					//take a snapshot of current state
					EventSourceState snapshot = ess;

					//reset counter
					ess.currEventCount = 0;
					ess.counterTimestamp = 0;

					//trigger now
					_executeAlertAction(source, policy, rule, &snapshot);
					res = true;
				}
			}
		}
	}

	return res;
}

void AlertServicePlugin::_executeAlertAction(const std::string &source, AlertPolicy &policy, const AlertRule &rule, const EventSourceState *stateSnapshot) {
	if (rule.action.messageId != 0) {
		std::string subject, body;

		MessageMap::const_iterator itMsgMap = m_messageMap.find(rule.action.messageId);
		if (itMsgMap != m_messageMap.end()) {
			subject = _formatText(itMsgMap->second->subject, source, rule, stateSnapshot);
			body = _formatText(itMsgMap->second->body, source, rule, stateSnapshot);
		} else {
			subject = "Undefined alert #" + rule.action.messageId;
			body = subject;
		}

		_sendAlert((AlertLevel)rule.action.level, subject, body);
	}

	if (!source.empty() && !rule.action.postState.empty()) {
		_updateSourceState(source, policy, rule.action.postState);
	}
}

void AlertServicePlugin::_updateSourceState(const std::string &source, AlertPolicy &policy, const std::string &newState) {
	//Return if source not defined or newState is empty (=no change)
	if (source.empty() || newState.empty()) {
		return;
	}

	jsFastLock lock(policy.sync);

	std::map<std::string, EventSourceState>::iterator it = policy.runtimeStates.find(source);
	if (it == policy.runtimeStates.end()) {
		policy.runtimeStates.insert(make_pair(source, EventSourceState()));
		it = policy.runtimeStates.find(source);
		it->second.currState = policy.initState;
	}

	if (it->second.currState != newState) {
		//Update state
		it->second.currState = newState;

		//Reset event counter
		it->second.counterTimestamp = 0;
		it->second.currEvent = 0;
		it->second.currEventCount = 0;
	}
}

//Send cached alerts
void AlertServicePlugin::_sendAlert(AlertLevel level, const std::string &subject, const std::string &body) {
	LOG4CPLUS_DEBUG(m_logger, "_sendAlert: [" << subject << "], level=" << FormatAlertLevel(level) << ", subject=" << subject << ", body=" << body);

	MailerPlugin *pMailer = dynamic_cast<MailerPlugin *>(&*host()->getPlugin("Mailer"));
	jsAssert(pMailer != NULL);
	if (pMailer == NULL) {
		LOG4CPLUS_ERROR(m_logger, "Missing mailer plugin");
		return;
	}

	std::stringstream ssSubject, ssBody;

	ssSubject
		<< "[" << FormatAlertLevel(level) << "] "
		<< subject;

	ssBody
		<< convertToLocalTime(GetSystemTimeUINT64()) << std::endl
		<< body << std::endl
		<< std::endl;

	try {
		pMailer->send(ssSubject.str(), ssBody.str());
	} catch (const MailException &e) {
		LOG4CPLUS_ERROR(m_logger, "_sendAlert failed: " << e.str());
	}
}

std::string AlertServicePlugin::_formatText(const std::string &format, const std::string &source, const AlertRule &rule, const EventSourceState *stateSnapshot) {
	//Convert numbers to strings
	std::stringstream ssEventId, ssTriggerCount, ssDuration;
	ssEventId << rule.trigger.eventId;
	ssTriggerCount << rule.trigger.eventCount;
	ssDuration << rule.trigger.durationMins;

	//Format string
	std::string output = format;
	STLStringSubstitute(output, "%SOURCE%", source);
	STLStringSubstitute(output, "%PRESTATE%", rule.trigger.preState);
	STLStringSubstitute(output, "%EVENT%", ssEventId.str());
	STLStringSubstitute(output, "%TRIGGERCOUNT%", ssTriggerCount.str());
	STLStringSubstitute(output, "%DURATION%", ssDuration.str());
	STLStringSubstitute(output, "%POSTSTATE%", rule.action.postState);

	if (stateSnapshot) {
		std::stringstream ssCurrCount;
		ssCurrCount << stateSnapshot->currEventCount;
		STLStringSubstitute(output, "%CURRCOUNT%", ssCurrCount.str());
		STLStringSubstitute(output, "%CURRSTATE%", stateSnapshot->currState);
	}

	return output;
}

ULONG AlertServicePlugin::_doWork() {
	while (true) {
		jsWaitRet wr = m_semExiting.waitAndDecrement(60000);
		if (wr == WR_OK) {
			break;
		}

		QueueTask(new ReportEventTask(*this, "", EVENT_COUNTER_REVIEW_TIMER));
	}

	return 0;
}

AlertLevel AlertServicePlugin::ParseAlertLevel(const std::string &sLevel) {
	for (int i = 0; i < NUM_ALERT_LEVELS; i++) {
		if (STLCompareIgnoreCase(AlertLevelNames[i], sLevel) == 0) {
			return (AlertLevel)i;
		}
	}

	//Unknown level, return RED
	return ALERT_RED;
}

std::string AlertServicePlugin::FormatAlertLevel(AlertLevel level) {
	if (level >= 0 && level < NUM_ALERT_LEVELS) {
		return AlertLevelNames[level];
	}

	//Unknown level, return "RED"
	return AlertLevelNames[ALERT_RED];
}

////////////////////////////////////////////////////////////////////////
// REST handler

BEGIN_REST_HANDLER_MAP(AlertService)
REST_PROC__2(ReportEvent, std::string, source, NOCAST, int, eventId, atoi);
REST_PROC__3(SendAlert, AlertLevel, level, (AlertLevel)atoi, std::string, subject, NOCAST, std::string, body, NOCAST);
END_REST_HANDLER_MAP

////////////////////////////////////////////////////////////////////////
// Stub class for remote interface

BEGIN_STUB_CLASS(AlertService, MODULE_NAME_ALERTSERVICE)
MARSHAL_PROC__2(ReportEvent, const std::string &, source, int, eventId);
MARSHAL_PROC__3(SendAlert, AlertLevel, level, const std::string &, subject, const std::string &, body);
END_STUB_CLASS

////////////////////////////////////////////////////////////////////////
// PluginFactory
char gAlertServiceModuleName[] = MODULE_NAME_ALERTSERVICE;

AbstractPluginFactory *createPluginFactory(IAlertService *pFactoryTypeHint) {
	return new UniversalPluginFactory<AlertServicePlugin, AlertServiceStub, gAlertServiceModuleName>();
}

} // namespace KEP