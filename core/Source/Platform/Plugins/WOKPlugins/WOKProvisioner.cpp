///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// WOK platform provisioner plugin - YC 12/2013
// * Fixed WOK provisioning
//	 - register servers by configurations
//	 - call controller to launch servers
// * Dynamic WOK provisioning - TBD

#include "WOKProvisioner.h"
#include "HostHeartbeatMonitor.h"
#include "AppHeartbeatMonitor.h"
#include "AbstractRestPlugin.h"
#include "RestClient.h"
#include "HostRegistry.h"
#include "AppController.h"
#include "Core/MySqlPLib/MySqlConnector.h"
#include "js.h"
#include "jsLock.h"
#include "jsSync.h"
#include "jsThread.h"
#include "RestHandlerHelper.h"
#include "StubHelper.h"

using namespace mysqlpp;

#define DEFAULT_PLATFORM_PATH "C:\\Program Files (x86)\\Kaneva\\Kaneva Platform"
#define DEFAULT_WOK_GAME_ID 3296
#define DEFAULT_SLOTS_PER_HOST 100
#define WORKER_THREAD_STOPPING_TIMEOUT 3000

#define alert_logger m_logger // For DB macros

namespace KEP {

class WOKProvisionerPlugin : public IWOKProvisioner, public IHostHeartbeatMonitor, public IAppHeartbeatMonitor, public AbstractRESTPlugin, public jsThread {
public:
	WOKProvisionerPlugin(const TiXmlElement* config);
	~WOKProvisionerPlugin();

	// Plugin interface
	virtual Plugin& startPlugin();
	virtual ACTPtr stopPlugin();

	// HTTPServer::RequestHandler
	virtual int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint);

	// IWOKProvisioner interface
	virtual void StartWOKServer(const std::string& hostName, unsigned int port);
	virtual void StopWOKServer(const std::string& hostName, unsigned int port);

	// IHostHeartbeatMonitor interface
	virtual void ReportHostHeartbeat(const std::string& hostName, unsigned int port, BOOL initial);

	// IAppHeartbeatMonitor interface
	virtual void ReportAppHeartbeats(const std::string& hostName, const AppHeartbeatMap& data);

protected:
	virtual ULONG _doWork();

	bool _getServerList(KGP_Connection* conn, unsigned int gameId, const std::string& hostName, unsigned int numSlots, OUT std::map<unsigned int, unsigned int>& serverList);
	bool _addServer(KGP_Connection* conn, unsigned int gameId, unsigned int userId, const std::string& hostName, unsigned int slotId, OUT unsigned int& outPort, OUT unsigned int& outServerId);
	bool _insertServerRecord(KGP_Connection* conn, unsigned int gameId, unsigned int userId, const std::string& hostName, unsigned int port, unsigned int serverId);
	bool _launchServer(IAppController* pCntlr, const std::string& hostName, unsigned int port);
	bool _terminateServer(IAppController* pCntlr, const std::string& hostName, unsigned int port);

	bool m_ready;
	unsigned int m_WOKGameId;
	unsigned int m_WOKUserId;
	unsigned int m_numSlotsPerHost;
	std::string m_dsn;
	std::string m_localPingDsn;
	std::string m_platformPath;

	std::map<std::string, bool> m_hosts;
	std::map<std::string, std::set<AppId>> m_servers;
	jsFastSync m_sync;

	Logger m_heartbeatLogger;
};

WOKProvisionerPlugin::WOKProvisionerPlugin(const TiXmlElement* config) :
		AbstractRESTPlugin(MODULE_NAME_WOKPROVISIONER),
		jsThread(MODULE_NAME_WOKPROVISIONER),
		m_numSlotsPerHost(DEFAULT_SLOTS_PER_HOST),
		m_WOKGameId(DEFAULT_WOK_GAME_ID),
		m_WOKUserId(0),
		m_ready(false),
		m_heartbeatLogger(Logger::getInstance(L"HostHeartBeat")) {
	addSupportedInterface(INTERFACE_NAME_WOKPROVISIONER);
	addSupportedInterface(INTERFACE_NAME_HOSTHEARTBEATMONITOR);
	addSupportedInterface(INTERFACE_NAME_APPHEARTBEATMONITOR);

	addEndPoint("/StartWOKServer");
	addEndPoint("/StopWOKServer");
	addEndPoint("/ReportHostHeartbeat");
	addEndPoint("/ReportAppHeartbeats");

	if (config) {
		const TiXmlElement* elem;

		elem = config->FirstChildElement("wok_game_id");
		if (elem && elem->GetText()) {
			m_WOKGameId = atoi(elem->GetText());
		}

		if (m_WOKGameId == 0) {
			LOG4CPLUS_ERROR(m_logger, "Missing or invalid configuration: wok_game_id");
		}

		elem = config->FirstChildElement("wok_user_id");
		if (elem && elem->GetText()) {
			m_WOKUserId = atoi(elem->GetText());
		}

		if (m_WOKUserId == 0) {
			LOG4CPLUS_ERROR(m_logger, "Missing or invalid configuration: wok_user_id");
		}

		elem = config->FirstChildElement("num_slots_per_host");
		if (elem && elem->GetText()) {
			m_numSlotsPerHost = (unsigned int)std::max(1, atoi(elem->GetText()));
		}

		elem = config->FirstChildElement("db_connection");
		if (elem && elem->GetText()) {
			m_dsn = elem->GetText();
		}

		elem = config->FirstChildElement("local_ping_db_connection");
		if (elem && elem->GetText()) {
			m_localPingDsn = elem->GetText();
		}

		if (m_dsn.empty()) {
			LOG4CPLUS_ERROR(m_logger, "Missing or invalid configuration: db_connection");
		}

		elem = config->FirstChildElement("platform_path");
		if (elem && elem->GetText()) {
			m_platformPath = elem->GetText();
			if (m_platformPath.empty()) {
				LOG4CPLUS_WARN(m_logger, "Invalid configuration: platform_path");
			}
		}
	}

	if (m_platformPath.empty()) {
		LOG4CPLUS_WARN(m_logger, "Use default platform path: " << m_platformPath);
		m_platformPath = DEFAULT_PLATFORM_PATH;
	}

	m_ready = m_WOKGameId != 0 && m_WOKUserId != 0 && !m_dsn.empty();
}

WOKProvisionerPlugin::~WOKProvisionerPlugin() {
}

Plugin& WOKProvisionerPlugin::startPlugin() {
	if (m_ready) {
		// Start worker thread
		jsThread::start();
	}

	return AbstractRESTPlugin::startPlugin();
}

ACTPtr WOKProvisionerPlugin::stopPlugin() {
	ACTPtr res = AbstractRESTPlugin::stopPlugin();

	if (jsThread::isRunning()) {
		jsThread::join(WORKER_THREAD_STOPPING_TIMEOUT);
	}

	return res;
}

ULONG WOKProvisionerPlugin::_doWork() {
	IMySqlConnector* pMysqlConnector = NULL;
	IHostRegistry* pHostReg = NULL;
	KGP_Connection *conn = NULL, *conn2 = NULL;

	while (!jsThread::isTerminated()) {
		// Wait until MySQL connector plugin is available
		if (pMysqlConnector == NULL) {
			pMysqlConnector = GET_PLUGIN_INTERFACE(host(), IMySqlConnector);
			if (pMysqlConnector == NULL) {
				LOG4CPLUS_WARN(m_logger, "WOKProvisionerPlugin: MySqlConnector plugin not ready");
				Sleep(1000);
				continue;
			}
		}

		// Wait until Host registry plugin is available
		if (pHostReg == NULL) {
			pHostReg = GET_PLUGIN_INTERFACE(host(), IHostRegistry);
			if (pHostReg == NULL) {
				LOG4CPLUS_WARN(m_logger, "WOKProvisionerPlugin: HostRegistry plugin not ready");
				Sleep(1000);
				continue;
			}
		}

		// Obtain MySQL connection
		if (conn == NULL) {
			conn = pMysqlConnector->GetConnection(m_dsn);
			if (conn == NULL) {
				LOG4CPLUS_WARN(m_logger, "WOKProvisionerPlugin: mysql connection failed");
				Sleep(1000);
				continue;
			}
		}

		if (conn2 == NULL && !m_localPingDsn.empty()) {
			conn2 = pMysqlConnector->GetConnection(m_localPingDsn);
			if (conn == NULL) {
				LOG4CPLUS_WARN(m_logger, "WOKProvisionerPlugin: mysql local ping connection failed");
				Sleep(1000);
				continue;
			}
		}

		// Make a copy of current hosts list
		std::map<std::string, bool> hosts;
		{
			jsFastLock lock(m_sync);
			hosts = m_hosts;
		}

		// Iterate through all hosts
		for (auto it = hosts.begin(); it != hosts.end() && !jsThread::isTerminated(); ++it) {
			bool provisioned = it->second;

			if (!provisioned) {
				std::string hostName = it->first;
				IAppController* pCntlr = host()->DEPRECATED_getInterfaceNoThrow<IAppController>(GetAppControllerModuleName(hostName));
				if (!pCntlr) {
					LOG4CPLUS_ERROR(m_logger, "_doWork: AppController not available for " << hostName);
					continue;
				}

				// Get host info from registry
				HostInfo hi = pHostReg->GetHostInfo(hostName);
				if (!hi.avail) {
					LOG4CPLUS_TRACE(m_logger, "_doWork: skip " << hostName << " - host marked unavailable");
					continue;
				}

				// Query DB for existing server records
				std::map<unsigned int, unsigned int> serverList;
				if (!_getServerList(conn, m_WOKGameId, hostName, hi.numSlots, serverList)) {
					// Failed
					LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": _getServerList failed");
					continue;
				}

				// Analyze server list returned from DB
				for (unsigned int slotId = 1; slotId <= hi.numSlots && !jsThread::isTerminated(); slotId++) {
					unsigned int port = 0;
					unsigned int serverId = 0;
					if (serverList.find(slotId) == serverList.end()) {
						// Server not registered?
						if (!_addServer(conn, m_WOKGameId, m_WOKUserId, hostName, slotId, port, serverId) || port == 0 || serverId == 0) {
							// Failed
							LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": _addServer failed");
							continue;
						}

						serverList[slotId] = port;
						if (conn2 && !_insertServerRecord(conn2, m_WOKGameId, m_WOKUserId, hostName, port, serverId)) {
							LOG4CPLUS_WARN(m_logger, __FUNCTION__ ": _insertServerRecord to local ping DB failed");
						}
					}

					port = serverList[slotId];
					if (port == 0) {
						// Bad record found in game_servers
						LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": invalid DB record (port=0) for slot " << slotId << " on game " << m_WOKGameId);
						continue;
					}

					// Call AppController to launch server (if not already started)
					if (!_launchServer(pCntlr, hostName, port)) {
						// Failed
						LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": _launchServer failed");
						continue;
					}
				}

				// Update "provisioned" flag
				jsFastLock lock(m_sync);
				m_hosts[hostName] = true;
			}
		}

		Sleep(1000);
	}

	return 0;
}

bool WOKProvisionerPlugin::_getServerList(KGP_Connection* conn, unsigned int gameId, const std::string& hostName, unsigned int numSlots, OUT std::map<unsigned int, unsigned int>& serverList) {
	bool res = false;

	try {
		serverList.clear();

		CHECK_CONNECTION2(conn);

		BEGIN_TRY_SQL();

		Query query(conn);

		query << "SELECT server_id, port FROM game_servers "
			  << " WHERE game_id=%0 AND server_name=%1q "
			  << "   AND port<>25857"; // HACK to filter out legacy servers
		query.parse();

		LOG4CPLUS_TRACE(m_logger, query.str(gameId, hostName.c_str()));
		StoreQueryResult results = query.store(gameId, hostName.c_str());

		// Parse server list from DB
		res = true;
		for (unsigned int i = 0; i < results.num_rows() && i < numSlots; i++) {
			Row row = results.at(i);
			unsigned int serverId = row.at(0);
			unsigned int port = row.at(1);
			unsigned slotId = i + 1;
			serverList.insert(std::make_pair(slotId, port));
		}

		END_TRY_SQL_LOG_ONLY(conn);
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e->ToString());
		delete e;
	} catch (KEPException& e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e.ToString());
	}

	return res;
}

bool WOKProvisionerPlugin::_addServer(KGP_Connection* conn, unsigned int gameId, unsigned int userId, const std::string& hostName, unsigned int slotId, OUT unsigned int& outPort, OUT unsigned int& outServerId) {
	bool res = false;

	try {
		CHECK_CONNECTION2(conn);

		BEGIN_TRY_SQL();

		Query query(conn);

		//call add_game_server( @userId, @licenseKey, @gameId, @hostName, @port, @patchUrl, @alternatePatchUrl, @patchType, @clientPath, @rc, @ipPort, @serverId );
		query << "CALL add_game_server( %0, NULL, %1, %2q, 0, '', '', '', '', @rc, @ipPort, @serverId); "
			  << "SELECT CAST(@rc as UNSIGNED INT) as rc, CAST(@ipPort as UNSIGNED INT) as ipPort, CAST(@serverId as UNSIGNED INT) as serverId; ";
		query.parse();

		LOG4CPLUS_DEBUG(m_logger, query.str(userId, gameId, hostName.c_str()));
		StoreQueryResult results = query.store(userId, gameId, hostName.c_str()); // SP call
		results = query.store_next(); // SELECT

		if (results.num_rows() == 1) {
			Row row = results.at(0);
			unsigned rc = row.at(0);
			unsigned ipPort = row.at(1);
			unsigned serverId = row.at(2);

			if (rc == 0 && ipPort != 0) {
				outPort = ipPort;
				outServerId = serverId;
				res = true;
			}
		}

		END_TRY_SQL_LOG_ONLY(conn);
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e->ToString());
		delete e;
	} catch (KEPException& e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e.ToString());
	}

	return res;
}

bool WOKProvisionerPlugin::_insertServerRecord(KGP_Connection* conn, unsigned int gameId, unsigned int userId, const std::string& hostName, unsigned int port, unsigned int serverId) {
	bool res = false;

	try {
		CHECK_CONNECTION2(conn);

		BEGIN_TRY_SQL();

		Query query(conn);

		//CALL add_game_servers (@server_name, @game_id, @visibility_id, @server_started_date, @ip_address, @port, @number_of_players, @not_used, @last_ping_datetime, @modifiers_id, @server_id)";
		query << "SET @serverId=%0; CALL add_game_servers(%1q, %2, 1, NOW(), '', %3, 0, 0, NOW(), %4, @serverId);";
		query.parse();

		LOG4CPLUS_DEBUG(m_logger, query.str(serverId, hostName.c_str(), gameId, port, userId));
		query.store(serverId, hostName.c_str(), gameId, port, userId); // SET
		query.store_next(); // SP call

		END_TRY_SQL_LOG_ONLY(conn);
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e->ToString());
		delete e;
	} catch (KEPException& e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e.ToString());
	}

	return res;
}

bool WOKProvisionerPlugin::_launchServer(IAppController* pCntlr, const std::string& hostName, unsigned int port) {
	jsVerifyReturn(pCntlr != NULL, false);

	bool res = false;

	try {
		// Pass in port number though command line.
		// Currently it's used by DefaultServer.xml (as logical value) and ServerLog.cfg (as environment variable)
		std::stringstream ssLogicals;
		ssLogicals << "$PORT=" << port; // Use "$" modifier for both logical and environment variable

		pCntlr->StartServer(m_WOKGameId, port, m_platformPath, "%", ssLogicals.str(), NULL_MODULE_NAME);
		res = true;
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e->ToString());
		delete e;
	} catch (KEPException& e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e.ToString());
	}

	return res;
}

bool WOKProvisionerPlugin::_terminateServer(IAppController* pCntlr, const std::string& hostName, unsigned int port) {
	jsVerifyReturn(pCntlr != NULL, false);

	bool res = false;

	try {
		pCntlr->StopServer(m_WOKGameId, port, NULL_MODULE_NAME);
		res = true;
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e->ToString());
		delete e;
	} catch (KEPException& e) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": error occurred - " << e.ToString());
	}

	return res;
}

void WOKProvisionerPlugin::ReportHostHeartbeat(const std::string& hostName, unsigned int port, BOOL initial) {
	LOG4CPLUS_DEBUG(m_heartbeatLogger, "Received heartbeat from " << hostName << ":" << port << (initial ? ", initial" : ""));

	IHostRegistry* pHostRegistry = GET_PLUGIN_INTERFACE(host(), IHostRegistry);
	if (pHostRegistry == NULL) {
		LOG4CPLUS_ERROR(m_logger, "Missing HostRegistry plugin");
	} else {
		jsFastLock lock(m_sync);

		if (m_hosts.find(hostName) == m_hosts.end()) {
			// New server? Try add to host registry
			try {
				m_hosts[hostName] = false;
				pHostRegistry->AddHost(hostName, m_numSlotsPerHost);
			} catch (KEPException* e) {
				if (e->m_err == APIN_HOSTREGISTRY_HOST_ALREADY_EXISTS) {
					delete e;
					return;
				}

				throw;
			}

			//Newly reported: mount remote plugin
			std::stringstream ssConfigXml;
			ssConfigXml << "<" << MODULE_NAME_APPCONTROLLER << " remote=\"1\" remote_host=\"" << hostName << ":" << port << "\" />";

			TIXMLConfig doc;
			doc.parse(ssConfigXml.str());

			std::string instanceName = MODULE_NAME_APPCONTROLLER;
			instanceName += "." + hostName;

			host()->mountPlugin(MODULE_NAME_APPCONTROLLER, instanceName, &(doc.root())).setUpPlugin(instanceName).startPlugin(instanceName);
		} else if (initial) // If initial heartbeat, mark host as not-provisioned
		{
			m_hosts[hostName] = false;
		}
	}
}

void WOKProvisionerPlugin::ReportAppHeartbeats(const std::string& hostName, const AppHeartbeatMap& data) {
	jsFastLock lock(m_sync);

	// Now process server heartbeats
	for (AppHeartbeatMap::const_iterator it = data.begin(); it != data.end(); ++it) {
		m_servers[hostName].insert(it->first);
	}
}

void WOKProvisionerPlugin::StartWOKServer(const std::string& hostName, unsigned int port) {
	LOG4CPLUS_INFO(m_logger, "StartWOKServer(" << hostName << ", " << port << ")");

	if (hostName.empty()) {
		throw new KEPException("Parameter `host' is required", APIN_INVALID_REQUEST_PARAM);
	}

	jsFastLock lock(m_sync);

	if (m_hosts.find(hostName) == m_hosts.end()) {
		throw new KEPException("Unknown host", APIN_INVALID_REQUEST_PARAM);
	}

	IAppController* pCntlr = host()->DEPRECATED_getInterfaceNoThrow<IAppController>(GetAppControllerModuleName(hostName));
	if (!pCntlr) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": AppController not available for " << hostName);
		throw new KEPException("Internal error: AppController not found for the requested host", APIN_ERR_ALGORITHM);
	}

	if (port == NULL_PORT) {
		// Start all servers (if not already)
		// simply mark this host as unprovisioned. _doWork() will pick it up and call controller to start servers.
		m_hosts[hostName] = false;
	} else {
		// Start one server
		if (!_launchServer(pCntlr, hostName, port)) {
			// Failed
			LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": _launchServer failed");
			throw new KEPException("Internal error: _launchServer failed", APIN_ERR_ALGORITHM);
		}
	}
}

void WOKProvisionerPlugin::StopWOKServer(const std::string& hostName, unsigned int port) {
	LOG4CPLUS_INFO(m_logger, "StopWOKServer(" << hostName << ", " << port << ")");

	if (hostName.empty()) {
		throw new KEPException("Parameter `host' is required", APIN_INVALID_REQUEST_PARAM);
	}

	jsFastLock lock(m_sync);

	if (m_hosts.find(hostName) == m_hosts.end()) {
		throw new KEPException("Unknown host", APIN_INVALID_REQUEST_PARAM);
	}

	IAppController* pCntlr = host()->DEPRECATED_getInterfaceNoThrow<IAppController>(GetAppControllerModuleName(hostName));
	if (!pCntlr) {
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": AppController not available for " << hostName);
		throw new KEPException("Internal error: AppController not found for the requested host", APIN_ERR_ALGORITHM);
	}

	if (!_terminateServer(pCntlr, hostName, port)) {
		// Failed
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": _launchServer failed");
		throw new KEPException("Internal error: _launchServer failed", APIN_ERR_ALGORITHM);
	}
}

////////////////////////////////////////////////////////////////////////
// REST handler

BEGIN_REST_HANDLER_MAP(WOKProvisioner)
// IWOKProvisioner interface
REST_PROC__2(StartWOKServer, std::string, host, NOCAST, unsigned int, port, STRTOUL)
REST_PROC__2(StopWOKServer, std::string, host, NOCAST, unsigned int, port, STRTOUL)
// IHostHeartbeatMonitor interface
REST_CALL__3__PREPROC("/ReportHostHeartbeat", ReportHostHeartbeat, std::string, host, NOCAST, unsigned int, port, STRTOUL, BOOL, initial, atoi)
int res = 0;
ReportHostHeartbeat(host, port, initial);
REST_CALL__2__POSTPROC
// IAppHeartbeatMonitor interface
REST_CALL__2__PREPROC("/ReportAppHeartbeats", ReportAppHeartbeats, std::string, host, NOCAST, AppHeartbeatMap, data, DecodeHeartbeats)
int res = 0;
ReportAppHeartbeats(host, data);
REST_CALL__1__POSTPROC
END_REST_HANDLER_MAP

////////////////////////////////////////////////////////////////////////
// Stub class for remote interface

BEGIN_STUB_CLASS3(WOKProvisioner, HostHeartbeatMonitor, AppHeartbeatMonitor, MODULE_NAME_HOSTREGISTRY)
// IWOKProvisioner interface
MARSHAL_PROC__2(StartWOKServer, const std::string&, host, unsigned int, port)
MARSHAL_PROC__2(StopWOKServer, const std::string&, host, unsigned int, port)
// IHostHeartbeatMonitor interface
virtual void ReportHostHeartbeat(const std::string& host, unsigned int port, BOOL initial) {
	MARSHAL_SHARED__3("/ReportHostHeartbeat", ReportHostHeartbeat, host, port, initial, NULL, true, 180000);
}
// IAppHeartbeatMonitor interface
virtual void ReportAppHeartbeats(const std::string& host, const AppHeartbeatMap& data) {
	MARSHAL_SHARED__2("/ReportAppHeartbeats", ReportAppHeartbeats, host, data, NULL, true, 60000);
}
END_STUB_CLASS

////////////////////////////////////////////////////////////////////////
// PluginFactory
char gWOKProvisionerModuleName[] = MODULE_NAME_WOKPROVISIONER;

AbstractPluginFactory* createPluginFactory(IWOKProvisioner* pFactoryTypeHint) {
	return new UniversalPluginFactory<WOKProvisionerPlugin, WOKProvisionerStub, gWOKProvisionerModuleName>();
}

} // namespace KEP