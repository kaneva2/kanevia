///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "AppController.h"
#include "RestClient.h"
#include "AbstractRestPlugin.h"
#include "Notifier.h"
#include "AppHeartbeatMonitor.h"
#include "HostHeartbeatMonitor.h"
#include "Utils.h"
#include "AlertService.h"
#include "RestHandlerHelper.h"
#include "StubHelper.h"
#include "HTTPServer.h"
#include "TaskObject.h"
#include "TaskSystem.h"
#include "NamedMutexHelper.h"
#include "common\KEPUtil\Helpers.h"
#include "js.h"
#include "jsLock.h"
#include "jsThread.h"
#include "jsEnumFiles.h"
#include "TinyXML/tinyxml.h"
#include "log4cplus/logger.h"

using namespace log4cplus;

// Defaults
#define BIN_DIR "bin"
#define KGPSERVER_BINARY "KGPServer.exe"
#define DEFAULT_KGPSERVER_STOPPING_TIMEOUT (30.0 * 1000.0)
#define DEFAULT_REQUEST_PROCESSOR_THREADS 4
#define DEFAULT_HOUSEKEEPING_INTERVAL (30.0 * 1000.0)
#define MINIMUM_HOUSEKEEPING_INTERVAL (3 * 1000)
#define DEFAULT_HOUSEKEEPING_WARMUP_DELAY (120.0 * 1000.0) // Housekeeping thread will not perform selected maintenance tasks during the first two minutes
#define DEFAULT_STARTUP_SPAWN_THROTTLING 60.0 // Spawn up to 60 KGPServer processes per minute at startup
#define MAXIMUM_STARTUP_SPAWN_INTERVAL (60.0 * 1000.0)
#define HEARTBEAT_MONITOR_WINDOW_CLASS L"kanevaKGPServerHBMonitorClass"
#define HEARTBEAT_MONITOR_WINDOW_TITLE L"kanevaKGPServerHBMonitorWindow"
#define KGPSERVER_HEARTBEAT_MESSAGE L"kanevaMsgKGPServerHeartbeat"
#define KGPSERVER_ALERT_MESSAGE L"kanevaMsgKGPServerAlert"

#define APP_CONTROLLER_PROCESSMAP_FILE "processes.xml"

namespace KEP {

class AppControllerTask;

class AppControllerPlugin : public IAppController, public AbstractRESTPlugin, public jsThread, public TaskSystem {
public:
	AppControllerPlugin(const TiXmlElement* config);
	virtual ~AppControllerPlugin();

	// Plugin interface
	virtual Plugin& startPlugin();
	virtual ACTPtr stopPlugin();

	// HTTPServer::Request
	virtual int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint);

	// IAppController
	virtual void StartServer(AppId id, unsigned int port, const std::string& platformPath, const std::string& appDir, const std::string& logicals, const std::string& cbMod, unsigned int cbData, const std::string& cbHost);
	virtual void StopServer(AppId id, unsigned int port, const std::string& cbMod, unsigned int cbData, const std::string& cbHost);
	virtual void BootstrapServer(AppId id);
	virtual void ReportHeartbeat(AppId id, int numUsers); // Takes heartbeat from local KGPServers and caches them before sending out in batch to incubator.
	virtual void ReportMetrics(AppId id);

	IAlertService* GetAlertPlugin();

private:
	virtual ULONG _doWork();

	int _spawnKgpServer(AppControllerTask* task);
	int _stopKgpServer(AppControllerTask* task);

	void _loadProcessMap();
	void _saveProcessMap();

	void _sendGroupedHeartbeatReport(bool& firstHeartbeat);
	void _restartDeadServers();

	void RegisterHeartbeatMonitorWindowClass();
	void UnregisterHeartbeatMonitorWindowClass();
	void CreateHeartbeatMonitorWindow();
	void DestroyHeartbeatMonitorWindow();
	static LRESULT CALLBACK HeartbeatMonitorWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	void ProcessWindowMessageUntil(TimeMs durationInMs);

	struct ServerInfo {
		DWORD m_processId;
		DWORD m_threadId;
		HANDLE m_processHandle;
		std::string m_platformPath;
		std::string m_appDir;
		std::string m_logicals;

		ServerInfo(DWORD processId, DWORD threadId, HANDLE processHandle, const std::string& platformPath, const std::string& appDir, const std::string& logicals) :
				m_processId(processId), m_threadId(threadId), m_processHandle(processHandle), m_platformPath(platformPath), m_appDir(appDir), m_logicals(logicals) {}
	};

	typedef std::map<std::pair<AppId, unsigned int>, ServerInfo> ProcessMap;
	TimeMs m_kgpServerStoppingTimeout;
	UINT m_numTaskThreads;
	TimeMs m_houseKeepingIntervalMillis;
	TimeMs m_houseKeepingWarmUpDelayMillis; // Start-up wait time before housekeeping thread starts executing selected "delayed" maintenance tasks
	TimeMs m_startupSpawnThrottling; // Maximum # of KGPServer processes per minute to spawn at startup. 0 = no limit
	bool m_appDirRequired;
	bool m_restartCrashedServer;

	jsFastSync m_syncProcessMap;
	ProcessMap m_processMap;

	bool m_processMapLoaded;

	jsFastSync m_syncHeartbeats;

	AppHeartbeatMap m_heartbeats;

	typedef std::vector<std::pair<ProcessMap, AppHeartbeatMap>> vector_processMapHBMap;

	vector_processMapHBMap m_enumVec;

	bool m_terminating;
	std::string m_hostName;

	HWND m_hwndHeartbeatMonitor;
	UINT m_uMsgKgpServerHeartbeat;
	UINT m_uMsgKgpServerAlert;

	Logger m_heartbeatLogger;

public:
	virtual vector_processMapHBMap EnumerateServers();
	friend HTTPServer::Response& operator<<(HTTPServer::Response& resp, const AppControllerPlugin::vector_processMapHBMap& servers);
};

typedef int (AppControllerPlugin::*TaskHandler)(AppControllerTask* task);

jsFastSync m_enumVecSync;

class AppControllerTask : public TaskObject {
public:
	AppControllerTask(const std::string& type, AppControllerPlugin* controller, AppId id, unsigned port,
		const std::string& platformPath, const std::string& appDir, const std::string& logicals,
		const std::string& cbMod, unsigned int cbData, const std::string& cbHost, TaskHandler func);

	void ExecuteTask();

	std::string m_taskType;
	int m_appId;
	unsigned int m_port;
	std::string m_platformPath;
	std::string m_appDir;
	std::string m_logicals;
	std::string m_cbMod;
	unsigned int m_cbData;
	std::string m_cbHost;

private:
	AppControllerPlugin* m_controller;
	TaskHandler m_func;
};

AppControllerPlugin::AppControllerPlugin(const TiXmlElement* config) :
		TaskSystem("AppControllerPlugin"),
		AbstractRESTPlugin(MODULE_NAME_APPCONTROLLER),
		jsThread(MODULE_NAME_APPCONTROLLER),
		m_kgpServerStoppingTimeout(DEFAULT_KGPSERVER_STOPPING_TIMEOUT),
		m_numTaskThreads(DEFAULT_REQUEST_PROCESSOR_THREADS),
		m_houseKeepingIntervalMillis(DEFAULT_HOUSEKEEPING_INTERVAL),
		m_houseKeepingWarmUpDelayMillis(DEFAULT_HOUSEKEEPING_WARMUP_DELAY),
		m_startupSpawnThrottling(DEFAULT_STARTUP_SPAWN_THROTTLING),
		m_appDirRequired(false),
		m_restartCrashedServer(false),
		m_terminating(false),
		m_hwndHeartbeatMonitor(NULL),
		m_uMsgKgpServerHeartbeat(0),
		m_uMsgKgpServerAlert(0),
		m_processMapLoaded(false),
		m_heartbeatLogger(Logger::getInstance(L"AppHeartBeat")) {
	m_hostName = GetLocalHostName();

	if (config) {
		const TiXmlElement* elem;
		const TiXmlElement* nodeKgpServer = config->FirstChildElement("kgpserver");
		if (nodeKgpServer) {
			elem = nodeKgpServer->FirstChildElement("stopping_timeout");
			if (elem && elem->GetText()) {
				m_kgpServerStoppingTimeout = (TimeMs)std::max(0, atoi(elem->GetText()));
			}
		}

		elem = config->FirstChildElement("request_processor_threads");
		if (elem && elem->GetText()) {
			m_numTaskThreads = (UINT)std::max(1, atoi(elem->GetText()));
		}

		elem = config->FirstChildElement("housekeeping_interval");
		if (elem == NULL) {
			// For compatibility with old config files - TO BE REMOVED
			elem = config->FirstChildElement("reporting_interval");
		}
		if (elem && elem->GetText()) {
			m_houseKeepingIntervalMillis = (TimeMs)std::max(MINIMUM_HOUSEKEEPING_INTERVAL, atoi(elem->GetText()));
		}

		elem = config->FirstChildElement("housekeeping_warmup_delay");
		if (elem && elem->GetText()) {
			m_houseKeepingWarmUpDelayMillis = (TimeMs)atoi(elem->GetText());
		}

		elem = config->FirstChildElement("startup_spawn_throttling");
		if (elem && elem->GetText()) {
			m_startupSpawnThrottling = (UINT)std::max(0, atoi(elem->GetText()));
		}

		std::string hostName;
		if (config->QueryValueAttribute("local_host", &hostName) == TIXML_SUCCESS) {
			m_hostName = hostName;
		}

		int port = 0;
		if (config->QueryIntAttribute("local_port", &port) == TIXML_SUCCESS) {
			m_port = (UINT)port;
		}

		elem = config->FirstChildElement("appdir_required");
		if (elem && elem->GetText()) {
			m_appDirRequired = atoi(elem->GetText()) != 0;
		}

		elem = config->FirstChildElement("restart_crashed_server");
		if (elem && elem->GetText()) {
			m_restartCrashedServer = atoi(elem->GetText()) != 0;
		}
	}

	addEndPoint("/StartServer");
	addEndPoint("/StopServer");
	addEndPoint("/BootstrapServer");
	addEndPoint("/ReportMetrics/AppController");
	addEndPoint("/EnumerateServers");

	//Register a window class for receiving HB from KGPServers
	RegisterHeartbeatMonitorWindowClass();

	//Load process map from previous run (for recovery)
	_loadProcessMap();
}

AppControllerPlugin::~AppControllerPlugin() {
	UnregisterHeartbeatMonitorWindowClass();
}

Plugin& AppControllerPlugin::startPlugin() {
	Plugin& res = AbstractRESTPlugin::startPlugin();

	Initialize(m_numTaskThreads);

	TimeMs spawnInterval = 0;
	if (m_startupSpawnThrottling > 0) {
		spawnInterval = std::min((60.0 * 1000.0) / m_startupSpawnThrottling, MAXIMUM_STARTUP_SPAWN_INTERVAL);
	}

	LOG4CPLUS_INFO(m_logger, "StartPlugin: KGPServer spawn interval = " << FMT_TIME << spawnInterval << " ms");

	//Restart all Kgpservers which had been taken down without our knowledge (either crashed or killed)
	TimeMs lastSpawnTime = 0;
	for (ProcessMap::iterator itPM = m_processMap.begin(); itPM != m_processMap.end();) {
		if (itPM->second.m_processHandle == NULL) {
			if (itPM->second.m_platformPath.empty() || m_appDirRequired && itPM->second.m_appDir.empty() || itPM->second.m_logicals.empty()) {
				// Invalid record
				LOG4CPLUS_ERROR(m_logger, "StartPlugin: Unable to launch app " << itPM->first.first << "/" << itPM->first.second
																			   << ", platform: [" << itPM->second.m_platformPath << "]"
																			   << ", app dir: [" << itPM->second.m_appDir << "]"
																			   << ", logicals: [" << itPM->second.m_logicals << "]");
				++itPM;
				continue;
			}

			TimeMs timeMs = fTime::TimeMs();
			if (lastSpawnTime != 0 && ((timeMs - lastSpawnTime) < spawnInterval)) {
				TimeMs sleepMs = std::min(lastSpawnTime + spawnInterval - timeMs, spawnInterval);
				fTime::SleepMs(sleepMs);
			}
			lastSpawnTime = fTime::TimeMs();

			// Processing thread not started yet. This will only queue the task. No contention issue for process map.
			StartServer(itPM->first.first, itPM->first.second, itPM->second.m_platformPath, itPM->second.m_appDir, itPM->second.m_logicals, "", NULL, "");
			itPM = m_processMap.erase(itPM);
		} else {
			++itPM;
		}
	}

	m_terminating = false;
	jsThread::start();

	return res;
}

ACTPtr AppControllerPlugin::stopPlugin() {
	m_terminating = true;

	jsThread::join(WORKER_THREAD_STOPPING_TIMEOUT);

	Finalize();

	return AbstractRESTPlugin::stopPlugin();
}

void AppControllerPlugin::StartServer(AppId id, unsigned int port, const std::string& platformPath, const std::string& appDir, const std::string& logicals, const std::string& cbMod, unsigned int cbData, const std::string& cbHost) {
	LOG4CPLUS_DEBUG(m_logger, "AppController::StartServer( id=" << id << ", port=" << port << ", platform=[" << platformPath << "], appDir=[" << appDir << "], [" << logicals << "] )");

	QueueTask(new AppControllerTask("StartServer", this, id, port, platformPath, appDir, logicals, cbMod, cbData, cbHost, &AppControllerPlugin::_spawnKgpServer));
}

void AppControllerPlugin::StopServer(AppId id, unsigned int port, const std::string& cbMod, unsigned int cbData, const std::string& cbHost) {
	LOG4CPLUS_DEBUG(m_logger, "AppController::StopServer( id = " << id << ", port=" << port << " )");

	if (port == NULL_PORT) {
		// Stop all KGPServer processes from same app ID
		jsFastLock lock(m_syncProcessMap);
		for (ProcessMap::const_iterator it = m_processMap.begin(); it != m_processMap.end(); ++it) {
			if (it->first.first == id) {
				QueueTask(new AppControllerTask("StopServer", this, id, it->first.second, "", "", "", cbMod, cbData, cbHost, &AppControllerPlugin::_stopKgpServer));
			}
		}
	} else {
		QueueTask(new AppControllerTask("StopServer", this, id, port, "", "", "", cbMod, cbData, cbHost, &AppControllerPlugin::_stopKgpServer));
	}
}

void AppControllerPlugin::BootstrapServer(AppId id) {
	LOG4CPLUS_DEBUG(m_logger, "AppController::BootstrapServer( id = " << id << " )");

	std::wstringstream ssEventName;
	ssEventName << L"KGPSERVER_REFRESH_PINGER_" << id;
	HANDLE hRefreshPingerEvent = ::OpenEvent(READ_CONTROL | EVENT_MODIFY_STATE, FALSE, ssEventName.str().c_str());

	if (hRefreshPingerEvent) {
		//Signal KGPServer to refresh pinger
		::SetEvent(hRefreshPingerEvent);

		//Close event handle
		CloseHandle(hRefreshPingerEvent);
	} else {
		LOG4CPLUS_WARN(m_logger, "BootstrapServer(AppID=" << id << ") failed: named event not found");
	}
}

void AppControllerPlugin::ReportHeartbeat(AppId id, int numUsers) {
	LOG4CPLUS_TRACE(m_heartbeatLogger, "AppController::ReportHeartbeat( id = " << id << ", numUsers = " << numUsers << " )");

	UINT64 timestamp = GetSystemTimeUINT64();

	jsFastLock lock(m_syncHeartbeats);
	m_heartbeats[id].first = timestamp;
	m_heartbeats[id].second = numUsers;
}

AppControllerPlugin::vector_processMapHBMap AppControllerPlugin::EnumerateServers() {
	LOG4CPLUS_DEBUG(m_logger, "AppController::EnumerateServers");

	jsFastLock lock(m_enumVecSync);

	m_enumVec.clear();
	m_enumVec.push_back(make_pair(m_processMap, m_heartbeats));
	return m_enumVec;
}

void AppControllerPlugin::ReportMetrics(AppId id) {
	LOG4CPLUS_DEBUG(m_logger, "AppController::ReportMetrics");

	//TODO, pending spec
}

IAlertService* AppControllerPlugin::GetAlertPlugin() {
	return GET_PLUGIN_INTERFACE(host(), IAlertService);
}

ULONG AppControllerPlugin::_doWork() {
	bool firstHeartbeat = true;
	bool startMaintenance = false;
	Timer warmUpTimer;

	//Create a hidden window for heartbeat monitoring
	CreateHeartbeatMonitorWindow();

	while (!m_terminating) {
		TimeMs mark = fTime::TimeMs();
		bool exceptionOccurred = false;

		try {
			if (!startMaintenance && (warmUpTimer.ElapsedMs() > m_houseKeepingWarmUpDelayMillis)) {
				startMaintenance = true;
			}

			// Report host and app heartbeats
			_sendGroupedHeartbeatReport(firstHeartbeat);

			// Maintenance tasks
			if (startMaintenance) {
				// Restart crashed KGPServer process
				if (m_restartCrashedServer) {
					_restartDeadServers();
				}
			}

			ProcessWindowMessageUntil(m_houseKeepingIntervalMillis);
		} catch (KEPException* e) {
			LOG4CPLUS_ERROR(m_logger, "Unhandled KEPException occurred in AppController main loop: " << e->ToString());
			delete e;
			exceptionOccurred = true;
		} catch (KEPException& e) {
			LOG4CPLUS_ERROR(m_logger, "Unhandled KEPException occurred in AppController main loop: " << e.ToString());
			exceptionOccurred = true;
		} catch (...) {
			LOG4CPLUS_ERROR(m_logger, "Unknown exception occurred in AppController main loop");
			exceptionOccurred = true;
		}

		if (exceptionOccurred && !m_terminating) {
			// In case of exception, make sure there is a reasonable interval between two iterations so that this thread wouldn't use too much CPU cycles.
			TimeMs elapsed = fTime::ElapsedMs(mark);
			if (elapsed < MINIMUM_HOUSEKEEPING_INTERVAL) {
				fTime::SleepMs(MINIMUM_HOUSEKEEPING_INTERVAL - elapsed);
			}
		}
	}

	//Destroy hidden window
	DestroyHeartbeatMonitorWindow();

	return 0;
}

int AppControllerPlugin::_spawnKgpServer(AppControllerTask* task) {
	// Obtain a pointer to Notifier
	INotifier* pNotifier = GET_PLUGIN_INTERFACE(host(), INotifier);
	if (pNotifier == NULL) {
		throw new KEPException("Notifier plugin not registered", APIN_ERR_MISSING_PLUGIN);
	}

	// Prepare ServerInfo record
	ServerInfo info(0, 0, 0, task->m_platformPath, task->m_appDir, task->m_logicals);

	// Prevent race condition
	std::stringstream ssMutex;
	ssMutex << "KGPSERVER_CTL_" << task->m_appId << "_" << task->m_port;

	NamedMutexHelper mutexHelper(ssMutex.str());
	if (mutexHelper.lock(10000) != WAIT_OBJECT_0) {
		throw new KEPException("Request timeout", APIN_STARTING_FAILED_TIMEOUT);
	}

	// Check if service already running
	{
		jsFastLock lock(m_syncProcessMap);
		ProcessMap::iterator it = m_processMap.find(std::make_pair(task->m_appId, task->m_port));

		if (it != m_processMap.end() && it->second.m_processHandle != NULL) {
			//Check if still running
			DWORD exitCode = -1;
			if (GetExitCodeProcess(it->second.m_processHandle, &exitCode) && exitCode == STILL_ACTIVE) {
				info = it->second;
				LOG4CPLUS_TRACE(m_logger, "_spawnKgpServer: KGPServer already running for app [" << task->m_appId << "], port [" << task->m_port << "]: pid [" << info.m_processId << "]");
			} else {
				LOG4CPLUS_WARN(m_logger, "_spawnKgpServer: previously started KGPServer for app [" << task->m_appId << "], port [" << task->m_port << "] had exited or crashed, pid [" << it->second.m_processId << "]");

				//TODO: report event to AlertService

				//Remove out-dated record from process map
				m_processMap.erase(it);
			}
		}
	}

	if (info.m_processId == 0) {
		//Not previously running
		try {
#ifdef SUPPORT_LEGACY_APP
			bool isLegacy = STLCompareIgnoreCase(task->m_logicals, LOGICAL_LEGACY_APP_INDICATOR) == 0;
#endif

			//Double check resource readiness
			if (m_appDirRequired) {
				if (task->m_appDir.empty()) {
					throw new KEPException("AppDir not provided", APIN_STARTING_FAILED_MISSING_PARAM);
				}

				bool isDir = false;
				if (!jsFileExists(task->m_appDir.c_str(), &isDir) || !isDir) {
					throw new KEPException("Invalid AppDir", APIN_STARTING_FAILED_BAD_APPDIR);
				}

				std::string sPathDBCfg = task->m_appDir + "\\db.cfg";
				std::string sPathNWCfg = task->m_appDir + "\\network.cfg";

				if (!jsFileExists(sPathDBCfg.c_str(), &isDir) ||
					!jsFileExists(sPathNWCfg.c_str(), &isDir)) {
					throw new KEPException("Invalid AppDir", APIN_STARTING_FAILED_BAD_APPDIR);
				}
			}

			// Start service
			std::string sWorkingDirectory = task->m_platformPath + "\\" + BIN_DIR;
			std::string sKgpServerFullPath = sWorkingDirectory + "\\" + KGPSERVER_BINARY;

			std::stringstream ssCommandLine;
			ssCommandLine
				<< "\"" << sKgpServerFullPath << "\"" // Executable
				<< " -r" // Run as application
#ifdef SUPPORT_LEGACY_APP
				<< (isLegacy ? "" : " -s");
#else
				<< " -s"; // Using shared directory
#endif

			//Do not pass in instance ID unless appDir is provided. Otherwise KGPServer will be confused.
			//Hosted apps always have valid appDir so they are not affectd.
			if (!task->m_appDir.empty()) {
				ssCommandLine
					<< " " << task->m_appDir // Game dir
					<< " " << task->m_appId; // Instance ID
			}

#ifdef SUPPORT_LEGACY_APP
			if (!isLegacy && !task->m_logicals.empty())
#else
			if (!task->m_logicals.empty())
#endif
			{
				ssCommandLine << " -D " << task->m_logicals; // Custom variables
			}

			PROCESS_INFORMATION pi;
			STARTUPINFO si;
			memset(&pi, 0, sizeof(pi));
			memset(&si, 0, sizeof(si));
			si.cb = sizeof(si);

			const std::string& strCommandLine = ssCommandLine.str();
			LOG4CPLUS_INFO(m_logger, "_spawnKgpServer: " << sKgpServerFullPath.c_str() << " " << strCommandLine);
			std::vector<wchar_t> awchCommandLine(strCommandLine.c_str(), strCommandLine.c_str() + strCommandLine.size() + 1);

			if (CreateProcessW(Utf8ToUtf16(sKgpServerFullPath).c_str(), awchCommandLine.data(), NULL, NULL, FALSE, CREATE_NEW_PROCESS_GROUP | DETACHED_PROCESS, NULL, Utf8ToUtf16(sWorkingDirectory).c_str(), &si, &pi)) {
				CloseHandle(pi.hThread);

				info.m_processId = pi.dwProcessId;
				info.m_processHandle = pi.hProcess;
				info.m_threadId = pi.dwThreadId;

				LOG4CPLUS_INFO(m_logger, "_spawnKgpServer: started KGPServer for app [" << task->m_appId << "], port [" << task->m_port << "], process Id = " << info.m_processId);

				jsFastLock lock(m_syncProcessMap);
				m_processMap.insert(std::make_pair(std::make_pair(task->m_appId, task->m_port), info));
				_saveProcessMap();
			}
		} catch (KEPException* e) {
			SEND_ERROR_CALLBACK(task, e->m_err, e->ToString(), m_logger);
			int res = e->m_err;
			delete e;
			return res;
		} catch (const KEPException& e) {
			SEND_ERROR_CALLBACK(task, e.m_err, e.ToString(), m_logger);
			return e.m_err;
		}
	}

	if (!task->m_cbMod.empty() && task->m_cbMod != NULL_MODULE_NAME) {
		std::map<std::string, std::string> params;

		std::stringstream ssCBData, ssAppId, ssPid;
		ssCBData << (unsigned int)task->m_cbData;
		ssAppId << task->m_appId;
		ssPid << info.m_processId;

		params.insert(make_pair("_module", task->m_cbMod));
		params.insert(make_pair("_callbackdata", ssCBData.str()));
		params.insert(make_pair("_type", task->m_taskType));
		params.insert(make_pair("appid", ssAppId.str()));
		params.insert(make_pair("pid", ssPid.str()));

		pNotifier->Notify(task->m_cbMod, task->m_taskType, params);
	}

	return 0;
}

int AppControllerPlugin::_stopKgpServer(AppControllerTask* task) {
	DWORD exitCode = -1;
	int res = APIN_SUCCESS;

	// Prevent race condition
	std::stringstream ssMutex;
	ssMutex << "KGPSERVER_CTL_" << task->m_appId << "_" << task->m_port;

	NamedMutexHelper mutexHelper(ssMutex.str());
	if (mutexHelper.lock((DWORD)m_kgpServerStoppingTimeout) != WAIT_OBJECT_0) {
		throw new KEPException("Request timeout", APIN_STOPPING_FAILED_TIMEOUT);
	}

	jsFastLock processMapLock(m_syncProcessMap);
	ProcessMap::iterator it = m_processMap.find(std::make_pair(task->m_appId, task->m_port));

	if (it != m_processMap.end()) {
		try {
			ServerInfo info = it->second;
			m_processMap.erase(it);
			_saveProcessMap();
			processMapLock.unlock();

			//Check if still running
			if (!GetExitCodeProcess(info.m_processHandle, &exitCode)) {
				throw new KEPException("Error querying process", APIN_STOPPING_FAILED_BAD_HANDLE);
			}

			if (exitCode != STILL_ACTIVE) {
				throw new KEPException("Process already terminated", APIN_STOPPING_FAILED_ALREADY_TERMINATED);
			}

			//Try stopping the service
			std::wstringstream ssEventName;
			ssEventName << L"KGPSERVER_PID_" << info.m_processId;

			HANDLE hKgpServerEvent = ::OpenEvent(READ_CONTROL | EVENT_MODIFY_STATE, FALSE, ssEventName.str().c_str());
			if (hKgpServerEvent) {
				//Signal KGPServer to exit
				::SetEvent(hKgpServerEvent);

				//Close event handle
				CloseHandle(hKgpServerEvent);
			} else {
				//Something is wrong. Use plan B.
				GenerateConsoleCtrlEvent(CTRL_BREAK_EVENT, info.m_processId);
			}

			if (WaitForSingleObject(info.m_processHandle, (DWORD)m_kgpServerStoppingTimeout) == WAIT_TIMEOUT) {
				//Kill it
				TerminateProcess(info.m_processHandle, -1);

				throw new KEPException("Stopping process timed out", APIN_STOPPING_FAILED_TIMEOUT);
			} else {
				LOG4CPLUS_INFO(m_logger, "_stopKgpServer: gracefully stopped KGPServer for app [" << task->m_appId << "], port [" << task->m_port << "], process Id = " << info.m_processId);
			}

			//Pull the exit code
			GetExitCodeProcess(info.m_processHandle, &exitCode);

			//Close handle
			CloseHandle(info.m_processHandle);
		} catch (KEPException* e) {
			res = e->m_err;
			delete e;
		} catch (const KEPException& e) {
			res = e.m_err;
		}
	} else {
		processMapLock.unlock();
		res = APIN_STOPPING_FAILED_NOT_FOUND;
	}

	if (!task->m_cbMod.empty() && task->m_cbMod != NULL_MODULE_NAME) {
		// Obtain a pointer to Notifier
		std::map<std::string, std::string> params;
		INotifier* pNotifier = GET_PLUGIN_INTERFACE(host(), INotifier);
		if (pNotifier == NULL) {
			//log error
			return 1;
		}

		std::stringstream ssCBData, ssAppId, ssRes, ssExitCode;
		ssCBData << (unsigned int)task->m_cbData;
		ssAppId << task->m_appId;
		ssRes << res;
		ssExitCode << exitCode;

		params.insert(std::make_pair("_module", task->m_cbMod));
		params.insert(std::make_pair("_callbackdata", ssCBData.str()));
		params.insert(std::make_pair("_type", task->m_taskType));
		params.insert(std::make_pair("appid", ssAppId.str()));
		params.insert(std::make_pair("result", ssRes.str()));
		params.insert(std::make_pair("exit_code", ssExitCode.str()));

		pNotifier->Notify(task->m_cbMod, task->m_taskType, params);
	}
	return 0;
}

void AppControllerPlugin::_loadProcessMap() {
	LOG4CPLUS_DEBUG(m_logger, "AppController::_loadProcessMap");

	if (m_processMapLoaded) {
		LOG4CPLUS_WARN(m_logger, "_loadProcessMap: already loaded");
		return;
	}

	m_processMapLoaded = true;

	bool isDir = false;
	if (!jsFileExists(APP_CONTROLLER_PROCESSMAP_FILE, &isDir) || isDir) {
		LOG4CPLUS_WARN(m_logger, "_loadProcessMap: data file not found: " << APP_CONTROLLER_PROCESSMAP_FILE);
		return;
	}

	TiXmlDocument doc;
	if (doc.LoadFile(APP_CONTROLLER_PROCESSMAP_FILE)) {
		TiXmlElement* root = doc.RootElement();
		if (root) {
			TiXmlElement* record;

			int recNo = 0;

			for (record = root->FirstChildElement("Record"); record != NULL; record = record->NextSiblingElement("Record"), ++recNo) {
				TiXmlElement* elemAppId = record->FirstChildElement("appId");
				TiXmlElement* elemPort = record->FirstChildElement("port");
				TiXmlElement* elemPid = record->FirstChildElement("processId");
				TiXmlElement* elemTid = record->FirstChildElement("threadId");
				TiXmlElement* elemPlatformPath = record->FirstChildElement("platformPath");
				TiXmlElement* elemAppDir = record->FirstChildElement("appDir");
				TiXmlElement* elemLogicals = record->FirstChildElement("logicals");

				// Allow empty <platformPath> and <logicals> for now to avoid wiping out legitimate records by accident
				// Records with empty logicals will not be processed (with error dumped in log) but will be remembered.
				// TODO: make both tags mandatory once we convert all legacy processes.xml.
				if (!elemAppId || !elemAppId->GetText() ||
					!elemPid || !elemPid->GetText() ||
					!elemTid || !elemTid->GetText() ||
					m_appDirRequired && (!elemAppDir || !elemAppDir->GetText())) {
					LOG4CPLUS_WARN(m_logger, "_loadProcessMap: skip invalid record #" << recNo);
				} else {
					AppId appId = atoi(elemAppId->GetText());
					unsigned int port = (elemPort && elemPort->GetText()) ? atoi(elemPort->GetText()) : 0;

					ServerInfo info(
						atoi(elemPid->GetText()),
						atoi(elemTid->GetText()),
						NULL,
						(elemPlatformPath && elemPlatformPath->GetText()) ? elemPlatformPath->GetText() : "",
						(elemAppDir && elemAppDir->GetText()) ? elemAppDir->GetText() : "",
						(elemLogicals && elemLogicals->GetText()) ? elemLogicals->GetText() : "");

					//Check if process is still around
					std::wstringstream ssEventName;
					ssEventName << "KGPSERVER_PID_" << info.m_processId;

					HANDLE hEvent = ::OpenEvent(READ_CONTROL, FALSE, ssEventName.str().c_str());
					if (hEvent) {
						//Process is still there, use it
						CloseHandle(hEvent);
						info.m_processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | SYNCHRONIZE, FALSE, info.m_processId);
						if (info.m_processHandle == NULL) {
							LOG4CPLUS_ERROR(m_logger, "_loadProcessMap: error opening process, pid=" << info.m_processId << ", app [" << appId << "], port [" << port << "]");
						} else {
							m_processMap.insert(std::make_pair(std::make_pair(appId, port), info));
						}
					} else {
						LOG4CPLUS_INFO(m_logger, "_loadProcessMap: queue a task to start server, app [" << appId << "], port [" << port << "]");
						m_processMap.insert(std::make_pair(std::make_pair(appId, port), info));
					}
				}
			}
		}
	} else {
		LOG4CPLUS_WARN(m_logger, "_loadProcessMap: reading data file failed: " << APP_CONTROLLER_PROCESSMAP_FILE);
		return;
	}
}

void AppControllerPlugin::_saveProcessMap() {
	LOG4CPLUS_DEBUG(m_logger, "AppController::_saveProcessMap");

	TiXmlDocument doc;
	doc.Parse("<ProcessMap></ProcessMap");

	TiXmlElement* root = doc.RootElement();
	if (!root) {
		LOG4CPLUS_WARN(m_logger, "_saveProcessMap: missing root");
		return;
	}

	for (ProcessMap::const_iterator it = m_processMap.begin(); it != m_processMap.end(); ++it) {
		TiXmlElement record("Record");

		TiXmlElement elemAppId("appId");
		std::stringstream ssAppId;
		ssAppId << it->first.first;
		elemAppId.InsertEndChild(TiXmlText(ssAppId.str()));
		record.InsertEndChild(elemAppId);

		TiXmlElement elemPort("port");
		std::stringstream ssPort;
		ssPort << it->first.second;
		elemPort.InsertEndChild(TiXmlText(ssPort.str()));
		record.InsertEndChild(elemPort);

		TiXmlElement elemPid("processId");
		std::stringstream ssPid;
		ssPid << it->second.m_processId;
		elemPid.InsertEndChild(TiXmlText(ssPid.str()));
		record.InsertEndChild(elemPid);

		TiXmlElement elemTid("threadId");
		std::stringstream ssTid;
		ssTid << it->second.m_threadId;
		elemTid.InsertEndChild(TiXmlText(ssTid.str()));
		record.InsertEndChild(elemTid);

		TiXmlElement elemPlatformPath("platformPath");
		elemPlatformPath.InsertEndChild(TiXmlText(it->second.m_platformPath));
		record.InsertEndChild(elemPlatformPath);

		TiXmlElement elemAppDir("appDir");
		elemAppDir.InsertEndChild(TiXmlText(it->second.m_appDir));
		record.InsertEndChild(elemAppDir);

		TiXmlElement elemLogicals("logicals");
		elemLogicals.InsertEndChild(TiXmlText(it->second.m_logicals));
		record.InsertEndChild(elemLogicals);

		root->InsertEndChild(record);
	}

	if (!doc.SaveFile(APP_CONTROLLER_PROCESSMAP_FILE)) {
		LOG4CPLUS_WARN(m_logger, "_saveProcessMap: writing data file failed: " << APP_CONTROLLER_PROCESSMAP_FILE);
	}
}

void AppControllerPlugin::RegisterHeartbeatMonitorWindowClass() {
	WNDCLASS wc;
	memset(&wc, 0, sizeof(wc));
	wc.lpfnWndProc = HeartbeatMonitorWndProc;
	wc.hInstance = GetModuleHandle(NULL);
	wc.lpszClassName = HEARTBEAT_MONITOR_WINDOW_CLASS;

	RegisterClass(&wc);

	if (m_uMsgKgpServerHeartbeat == 0) {
		m_uMsgKgpServerHeartbeat = RegisterWindowMessage(KGPSERVER_HEARTBEAT_MESSAGE);
	}

	if (m_uMsgKgpServerAlert == 0) {
		m_uMsgKgpServerAlert = RegisterWindowMessage(KGPSERVER_ALERT_MESSAGE);
	}
}

void AppControllerPlugin::UnregisterHeartbeatMonitorWindowClass() {
	UnregisterClass(HEARTBEAT_MONITOR_WINDOW_CLASS, GetModuleHandle(NULL));
}

void AppControllerPlugin::CreateHeartbeatMonitorWindow() {
	if (m_hwndHeartbeatMonitor == NULL) {
		m_hwndHeartbeatMonitor = CreateWindow(HEARTBEAT_MONITOR_WINDOW_CLASS, HEARTBEAT_MONITOR_WINDOW_TITLE, 0, 0, 0, 0, 0, NULL, NULL, NULL, this);
	}
}

void AppControllerPlugin::DestroyHeartbeatMonitorWindow() {
	if (m_hwndHeartbeatMonitor != NULL) {
		DestroyWindow(m_hwndHeartbeatMonitor);
		m_hwndHeartbeatMonitor = NULL;
	}
}

static AppControllerPlugin* spController = NULL;
LRESULT CALLBACK AppControllerPlugin::HeartbeatMonitorWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
		case WM_CREATE: {
			LPCREATESTRUCT lpcs = (LPCREATESTRUCT)lParam;
			if (lpcs) {
				spController = (AppControllerPlugin*)lpcs->lpCreateParams;
			}
		} break;

		case WM_DESTROY:
			spController = NULL;
			break;

		default:
			if (spController) {
				if (uMsg == spController->m_uMsgKgpServerHeartbeat) {
					// Heartbeats
					AppId appId = (AppId)wParam;
					UINT numUsers = (UINT)lParam;

					if (appId != 0) // Filter invalid data reported during server initialization
					{
						spController->ReportHeartbeat(appId, numUsers);
					}
					break;
				} else if (uMsg == spController->m_uMsgKgpServerAlert) {
					// Alerts
					AppId appId = (AppId)wParam;
					int eventId = (int)lParam;

					IAlertService* pAlertSvc = spController->GetAlertPlugin();

					if (pAlertSvc) {
						std::stringstream ssSourceName;
						ssSourceName << "KGPServer_" << appId;

						pAlertSvc->ReportEvent(ssSourceName.str(), eventId);
					}
					break;
				}
			}

			return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	return S_OK;
}

void AppControllerPlugin::ProcessWindowMessageUntil(TimeMs durationInMs) {
	Timer timer;
	while (!m_terminating && (timer.ElapsedMs() < durationInMs)) {
		MSG msg;
		while (!m_terminating && ::PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
			::DispatchMessageW(&msg);

		fTime::SleepMs(100);
	}
}

void AppControllerPlugin::_sendGroupedHeartbeatReport(bool& firstHeartbeat) {
	UINT64 now = GetSystemTimeUINT64();

	//Collect cached heartbeats
	jsFastLock lock(m_syncHeartbeats);
	AppHeartbeatMap temp;
	for (AppHeartbeatMap::const_iterator it = m_heartbeats.begin(); it != m_heartbeats.end(); ++it) {
		if (now <= it->second.first + HEARTBEAT_CACHE_EXPIRATION_TIME * NUM_CLUNKS_PER_MSEC) {
			temp.insert(*it);
		}
	}

	m_heartbeats.clear();
	lock.unlock();

	//Report heartbeat to HostLeaseManager
	IHostHeartbeatMonitor* pHostMgr = GET_PLUGIN_INTERFACE(host(), IHostHeartbeatMonitor);
	if (pHostMgr) {
		try {
			pHostMgr->ReportHostHeartbeat(m_hostName, m_port, firstHeartbeat ? TRUE : FALSE);
			firstHeartbeat = false;
		} catch (KEPException* e) {
			LOG4CPLUS_WARN(m_logger, __FUNCTION__ ": error reporting host heartbeat: " << e->ToString());
			delete e;
		} catch (KEPException& e) {
			LOG4CPLUS_WARN(m_logger, __FUNCTION__ ": error reporting host heartbeat: " << e.ToString());
		}
	} else {
		//log errors
		LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": HostLeaseManager plugin not found");
	}

	//Report heartbeats to AppLeaseManager
	if (!temp.empty()) {
		IAppHeartbeatMonitor* pAppMgr = GET_PLUGIN_INTERFACE(host(), IAppHeartbeatMonitor);
		if (pAppMgr) {
			try {
				pAppMgr->ReportAppHeartbeats(m_hostName, temp);
			} catch (KEPException* e) {
				LOG4CPLUS_WARN(m_logger, __FUNCTION__ ": error reporting app heartbeats: " << e->ToString());
				delete e;
			} catch (KEPException& e) {
				LOG4CPLUS_WARN(m_logger, __FUNCTION__ ": error reporting app heartbeats: " << e.ToString());
			}
		} else {
			//log errors
			LOG4CPLUS_ERROR(m_logger, __FUNCTION__ ": AppLeaseManager plugin not found");
		}
	}
}

void AppControllerPlugin::_restartDeadServers() {
	jsFastLock lock(m_syncProcessMap);

	for (ProcessMap::const_iterator it = m_processMap.begin(); it != m_processMap.end(); ++it) {
		AppId appId = it->first.first;
		UINT port = it->first.second;
		const ServerInfo& info = it->second;

		if (info.m_processHandle != NULL) {
			//Check if still running
			DWORD exitCode = -1;
			if (!GetExitCodeProcess(info.m_processHandle, &exitCode)) {
				LOG4CPLUS_WARN(m_logger, "_restartDeadServers: GetExitCodeProcess failed, GetLastError()=" << GetLastError() << ", app [" << appId << "], port [" << port << "]: pid [" << info.m_processId << "]");
			} else if (exitCode != STILL_ACTIVE) {
				LOG4CPLUS_WARN(m_logger, "KGPServer for app [" << appId << "], port [" << port << "] had exited or crashed, pid [" << it->second.m_processId << "]");
				LOG4CPLUS_INFO(m_logger, "Restarting KGPServer for app [" << appId << "], port [" << port << "]");

				// Queue a task to restart it
				StartServer(appId, port, info.m_platformPath, info.m_appDir, info.m_logicals, "", NULL, "");

				// Ease-in slowing by restarting up to 1 crashed server per housekeeping cycle
				// If servers crash faster than maintenance task can restart, we will need manual intervention anyway.

				// Send alert
				IAlertService* pAlertSvc = GetAlertPlugin();

				if (pAlertSvc) {
					std::stringstream ssSubject, ssBody;
					ssSubject << "Controller[" << m_hostName << "] restarted a KGPServer process";
					ssBody << "Controller[" << m_hostName << "] just restarted a crashed/exited KGPServer process for app [" << appId << "], port [" << port << "]";
					pAlertSvc->SendAlert(ALERT_RED, ssSubject.str(), ssBody.str());
				}
				break;
			}
		}
	}
}

AppControllerTask::AppControllerTask(const std::string& type, AppControllerPlugin* provisioner, AppId id, unsigned int port,
	const std::string& platformPath, const std::string& appDir, const std::string& logicals,
	const std::string& cbMod, unsigned int cbData, const std::string& cbHost, TaskHandler func) :
		m_taskType(type),
		m_appId(id),
		m_port(port),
		m_platformPath(platformPath),
		m_appDir(appDir),
		m_logicals(logicals),
		m_controller(provisioner),
		m_cbMod(cbMod),
		m_cbData(cbData),
		m_cbHost(cbHost),
		m_func(func) {
}

void AppControllerTask::ExecuteTask() {
	LOG4CPLUS_DEBUG(m_controller->getLogger(), "AppControllerTask::ExecuteTask for " << m_taskType << ", app [" << m_appId << "], port [" << m_port << "]");

	try {
		(m_controller->*m_func)(this);
	} catch (KEPException* e) {
		LOG4CPLUS_ERROR(m_controller->getLogger(), "Exception in AppController task for " << m_taskType << ", app [" << m_appId << "], port [" << m_port << "]: " << e->ToString());
		delete e;
	} catch (const KEPException& e) {
		LOG4CPLUS_ERROR(m_controller->getLogger(), "Exception in AppController task for " << m_taskType << ", app [" << m_appId << "], port [" << m_port << "]: " << e.ToString());
	}
}

////////////////////////////////////////////////////////////////////////
// REST handler
BEGIN_REST_HANDLER_MAP(AppController)
REST_PROC__5CB(StartServer, AppId, appid, atoi, unsigned int, port, STRTOUL, std::string, platformpath, NOCAST, std::string, appdir, NOCAST, std::string, logicals, NOCAST);
REST_PROC__2CB(StopServer, AppId, appid, atoi, unsigned int, port, STRTOUL);
REST_PROC__1(BootstrapServer, AppId, appid, atoi);
REST_CALL__1__PREPROC("/ReportMetrics/AppController", ReportMetrics, AppId, appid, atoi)
int res = 0;
ReportMetrics(appid);
REST_CALL__1__POSTPROC
REST_FUNC__0(EnumerateServers, AppControllerPlugin::vector_processMapHBMap);
END_REST_HANDLER_MAP

////////////////////////////////////////////////////////////////////////
// Stub class for remote interface
BEGIN_STUB_CLASS(AppController, MODULE_NAME_APPCONTROLLER)
MARSHAL_PROC__5CB(StartServer, AppId, appid, unsigned int, port, const std::string&, platformpath, const std::string&, appdir, const std::string&, logicals);
MARSHAL_PROC__2CB(StopServer, AppId, appid, unsigned int, port);
MARSHAL_PROC__1(BootstrapServer, AppId, appid);
MARSHAL_PROC__1(ReportMetrics, AppId, appid);
END_STUB_CLASS

////////////////////////////////////////////////////////////////////////
// PluginFactory
char gAppControllerModuleName[] = MODULE_NAME_APPCONTROLLER;

AbstractPluginFactory* createPluginFactory(IAppController* pFactoryTypeHint) {
	return new UniversalPluginFactory<AppControllerPlugin, AppControllerStub, gAppControllerModuleName>();
}

////////////////////////////////////////////////////////////////////////
// Enumerate servers streaming
HTTPServer::Response& operator<<(HTTPServer::Response& resp, const AppControllerPlugin::vector_processMapHBMap& servers) {
	jsFastLock lock(m_enumVecSync);

	resp << "<servers number=\"";
	resp << servers.size();
	resp << "\">";

	for (AppControllerPlugin::vector_processMapHBMap::const_iterator it = servers.begin(); it != servers.end(); ++it) {
		AppControllerPlugin::ProcessMap pm_map = it->first;
		AppHeartbeatMap hb_map = it->second;

		for (AppControllerPlugin::ProcessMap::const_iterator it2 = pm_map.begin(); it2 != pm_map.end(); ++it2) {
			AppId id = it2->first.first;
			unsigned int port = it2->first.second;

			const AppControllerPlugin::ServerInfo& svrInfo = it2->second;

			resp << "<server appID=\"" << id << "\" port=\"" << port << "\">";
			resp << "<pid>" << svrInfo.m_processId << "</pid>";
			resp << "<tid>" << svrInfo.m_threadId << "</tid>";
			resp << "<platformPath>" << svrInfo.m_platformPath.c_str() << "</platformPath>";
			resp << "<appDir>" << svrInfo.m_appDir.c_str() << "</appDir>";
			resp << "<logicals>" << svrInfo.m_logicals.c_str() << "</logicals>";

			if (hb_map.size() != 0) {
				AppHeartbeatMap::iterator it_hb = hb_map.find(id);

				if (it_hb != hb_map.end()) {
					resp << "<heartbeat appID=\"" << id << "\">";
					resp << "<time>" << convertToLocalTime(it_hb->second.first) << "</time>";
					resp << "<numusers>" << it_hb->second.second << "</numusers>";
					resp << "</heartbeat>";
				}
			}

			resp << "</server>";
		}
	}

	resp << "</servers>";

	return resp;
}

} // namespace KEP