///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "RestResultParser.h"

#define REST_CONTAINER_PRIMARY_DELIMITER "{,}"
#define REST_CONTAINER_SECONDARY_DELIMITER "{=}"

template <typename ElemType, typename ElemParser>
class VectorParser : public RestResultParser {
public:
	VectorParser(vector<ElemType>& output, const string& tag = DEFAULT_RESTRESULT_TAG) :
			RestResultParser(tag), m_output(output) {}
	void parse(const void* content, size_t size) {
		setResponseContent(content, size);

		TiXmlDocument doc;
		doc.Parse(responseContent().c_str());

		TiXmlElement* elem = doc.FirstChildElement("result");
		if (elem && elem->GetText()) {
			string text = elem->GetText();

			string::size_type srchOfs = 0;
			for (string::size_type dlmOfs = text.find(REST_CONTAINER_PRIMARY_DELIMITER, srchOfs); dlmOfs != string::npos; dlmOfs = text.find(REST_CONTAINER_PRIMARY_DELIMITER, srchOfs)) {
				string token = text.substr(srchOfs, dlmOfs - srchOfs);
				token = "<result>" + token + "</result>";
				ElemType elemRes;
				ElemParser elemParser(elemRes);
				elemParser.parse(token.c_str(), token.length());
				m_output.push_back(elemRes);

				srchOfs = dlmOfs + strlen(REST_CONTAINER_PRIMARY_DELIMITER);
			}
		}
	}

private:
	vector<ElemType>& m_output;
};

template <typename ElemType>
HTTPServer::Response& operator<<(HTTPServer::Response& resp, const std::vector<ElemType>& container) {
	for (std::vector<ElemType>::const_iterator it = container.begin(); it != container.end(); ++it) {
		resp << *it << REST_CONTAINER_PRIMARY_DELIMITER;
	}

	return resp;
}

#if 0 // NOT TESTED YET
template<typename ElemType>
HTTPServer::Response & operator <<(HTTPServer::Response& resp, const std::set<ElemType> & container)
{
	for( std::set<ElemType>::const_iterator it = container.begin(); it!=container.end(); ++it )
	{
		resp << *it << REST_CONTAINER_PRIMARY_DELIMITER;
	}

	return resp;
}

template<typename ElemType>
HTTPServer::Response & operator <<(HTTPServer::Response& resp, const std::queue<ElemType> & container)
{
	for( std::queue<ElemType>::const_iterator it = container.begin(); it!=container.end(); ++it )
	{
		resp << *it << REST_CONTAINER_PRIMARY_DELIMITER;
	}

	return resp;
}

template<typename KeyType, typename ValueType>
HTTPServer::Response & operator <<(HTTPServer::Response& resp, const std::map<KeyType, ValueType> & container)
{
	for( std::map<KeyType, ValueType>::const_iterator it = container.begin(); it!=container.end(); ++it )
	{
		resp << it->first << REST_CONTAINER_SECONDARY_DELIMITER << it->second << REST_CONTAINER_PRIMARY_DELIMITER;
	}

	return resp;
}
#endif