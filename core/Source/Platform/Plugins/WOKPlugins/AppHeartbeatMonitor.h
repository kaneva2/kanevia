///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Incubator.h"
#include "AppHeartbeatMap.h"
#include <string>

namespace KEP {

class IAppHeartbeatMonitor {
public:
	/**
	 * Report heartbeats for a set of active 3d apps.
	 *
	 * This function receives and monitors app heartbeats from AppController.
	 *
	 * @param	[in] data - Map of heartbeat data (appID-timestamp pair)
	 */
	virtual void ReportAppHeartbeats(const std::string& hostName, const AppHeartbeatMap& data) = 0;
};

} // namespace KEP