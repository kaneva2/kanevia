///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Incubator.h"

class IHostHeartbeatMonitor {
public:
	/**
	 * Report host-level heartbeat
	 *
	 * This function receives and monitors host heartbeats from AppController
	 *
	 * @param	[in] hostName - Remote AppController host name
	 * @param	[in] port - Remote AppController port number
	 * @param	[in] init - TRUE if it's the first heartbeat since host starts
	 */
	virtual void ReportHostHeartbeat(const std::string& hostName, UINT port, BOOL initial) = 0;
};
