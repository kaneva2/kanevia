///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "RestResultParser.h"

namespace KEP {

struct HostInfo {
	std::string hostName; // Host name
	unsigned int numSlots; // Total number of slots available
	UINT64 lastPing; // Last ping timestamp (64-bit file-time formatted timestamp)
	bool avail; // Whether or not this host is available
	unsigned int numUsedSlots; // Number of slots used by apps

	HostInfo(const std::string& host = "", unsigned int slots = 0) :
			hostName(host), numSlots(slots), lastPing(0), avail(true), numUsedSlots(0) {}
	HostInfo(const HostInfo& other) :
			hostName(other.hostName), numSlots(other.numSlots), lastPing(other.lastPing), avail(other.avail), numUsedSlots(other.numUsedSlots) {}
};

typedef std::map<std::string, HostInfo> HostInfoMap;

class __declspec(dllexport) HostInfoParser : public RestResultParser {
public:
	HostInfoParser(HostInfo& output) :
			m_output(output) {}
	void parse(const void* content, size_t contentLen);

private:
	HostInfo& m_output;
};

// Currently, implemented for test purposes only.
//
class __declspec(dllexport) HostInfoMapParser : public RestResultParser {
public:
	HostInfoMapParser(HostInfoMap& output); //: m_output(output) {}
	virtual void parse(const void* content, size_t contentLen);

	/**
	 * Obtain unprocessed string representation of the response.
	 */
	const std::string& content() { return m_content; }

private:
	HostInfoMap& m_output;
	std::string m_content;
};

} // namespace KEP