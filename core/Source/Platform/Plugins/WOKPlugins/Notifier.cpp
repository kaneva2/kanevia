///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "TinyXML/tinyxml.h"
#include "log4cplus/logger.h"
#include <common/include/KEPException.h>
#include "HTTPServer.h"
#include "Notifier.h"
#include "RestClient.h"

using namespace log4cplus;

namespace KEP {

static Logger logger = Logger::getInstance(_T(MODULE_NAME_NOTIFIER));

class NotifierPlugin : public INotifier, public AbstractPlugin, public HTTPServer::RequestHandler {
public:
	NotifierPlugin(const TiXmlElement* config);
	~NotifierPlugin();

	virtual Plugin& startPlugin();
	virtual ACTPtr stopPlugin();

	virtual int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint);

	virtual void AddListener(const std::string& module, INotificationListener* listener);
	virtual void RemoveListener(const std::string& module);

	virtual void Notify(const std::string& module, const std::string& type, const std::map<std::string, std::string>& params);

private:
	std::map<std::string, INotificationListener*> m_listeners;
	bool m_acceptRemoteNotification;
	std::string m_forwardingRecipient;
};

NotifierPlugin::NotifierPlugin(const TiXmlElement* config) :
		AbstractPlugin(MODULE_NAME_NOTIFIER),
		HTTPServer::RequestHandler(MODULE_NAME_NOTIFIER),
		m_acceptRemoteNotification(true) {
	addSupportedInterface(INTERFACE_NAME_NOTIFIER);

	if (config) {
		const TiXmlElement *group, *elem;

		group = config->FirstChildElement("remote_notification");
		if (group) {
			elem = group->FirstChildElement("receiving");
			if (elem && elem->GetText()) {
				m_acceptRemoteNotification = atoi(elem->GetText()) != 0;
			}

			elem = group->FirstChildElement("forwarding");
			if (elem && elem->GetText()) {
				m_forwardingRecipient = elem->GetText();
			}
		}
	}
}

NotifierPlugin::~NotifierPlugin() {
}

// This is setup logic.  Rename to setup().  Also, move the acquisition of the HTTPClient here.
// Notifier is, conditionally depending on the m_acceptRemoteNotification. dependent on HTTPServer
// and HTTPClient.  Setup and Teardown are where these issues should be handled.  All dependencies
// should be resolved prior to start/stop sequence.
//
// Get testing framework in place and then modify using the tests to verify the refactor.
//
Plugin& NotifierPlugin::startPlugin() {
	if (m_acceptRemoteNotification) {
		// Register a REST handler to receive remote notification forwarded from other nodes
		HTTPServer* pHttpServer = GET_PLUGIN_INTERFACE(host(), HTTPServer);

		if (pHttpServer) {
			pHttpServer->registerHandler(*this, "/Notify");
		} else {
			throw new KEPException("HTTPServer plugin unavailable", APIN_ERR_MISSING_PLUGIN);
		}
	}

	return AbstractPlugin::startPlugin();
}

// This is teardown logic.  use tearDown instead.
//
ACTPtr NotifierPlugin::stopPlugin() {
	if (m_acceptRemoteNotification) {
		HTTPServer* pHttpServer = GET_PLUGIN_INTERFACE(host(), HTTPServer);

		if (pHttpServer) {
			pHttpServer->deregisterHandler("/Notify");
		} else {
			throw new KEPException("HTTPServer plugin unavailable", APIN_ERR_MISSING_PLUGIN);
		}
	}

	return AbstractPlugin::stopPlugin();
}

/**
 * Accepts calls of format /Notify?_module=test&_type=test
 */
int NotifierPlugin::handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
	if (m_acceptRemoteNotification && request._uri == "/Notify") {
		auto itModName = request._paramMap.find("_module");
		auto itType = request._paramMap.find("_type");

		if (itModName == request._paramMap.end() || itType == request._paramMap.end()) {
			//Missing parameter
			endPoint.sendError(501, "Invalid request");
		} else {
			HTTPServer::Response resp;

			try {
				Notify(itModName->second, itType->second, request._paramMap);
				resp << "<result>" << 0 << "</result>";
			} catch (KEPException* e) {
				resp << "<error>" << e->ToString() << "</error>";
				delete e;
			} catch (const KEPException& e) {
				resp << "<error>" << e.ToString() << "</error>";
			}
			endPoint.write(resp);
		}
		return 1;
	}

	return 0;
}

void NotifierPlugin::AddListener(const std::string& module, INotificationListener* listener) {
	RemoveListener(module);
	m_listeners.insert(make_pair(module, listener));
}

void NotifierPlugin::RemoveListener(const std::string& module) {
	auto it = m_listeners.find(module);
	if (it != m_listeners.end()) {
		m_listeners.erase(it);
	}
}

void NotifierPlugin::Notify(const std::string& module, const std::string& type, const std::map<std::string, std::string>& params) {
	if (module == NULL_MODULE_NAME) {
		//Should not have made this call. Check callback module parameter first.
		LOG4CPLUS_WARN(logger, "Notify: invalid parameter, module=" << module << ", type=" << type);
		return;
	}

	std::map<std::string, INotificationListener*>::const_iterator itLsnr = m_listeners.find(module);
	if (itLsnr == m_listeners.end()) {
		if (!m_forwardingRecipient.empty()) {
			LOG4CPLUS_DEBUG(logger, "Notify: no local listener, forward to " << m_forwardingRecipient);

			IRESTClient* pRestCli = GET_PLUGIN_INTERFACE(host(), IRESTClient);
			if (pRestCli == NULL) {
				LOG4CPLUS_ERROR(logger, "Notify: cannot forward because RESTClient plugin is missing");
			} else if (params.find("_module") == params.end() || params.find("_type") == params.end()) {
				LOG4CPLUS_ERROR(logger, "Notify: cannot forward because [_module] and [_type] missing from parameter table");
			} else {
				pRestCli->Invoke(m_forwardingRecipient, "/Notify", params);
			}
		} else {
			LOG4CPLUS_DEBUG(logger, "Notify: no listener registered, module=" << module << ", type=" << type);
		}
	} else {
		itLsnr->second->Notify(type, params);
	}
}

////////////////////////////////////////////////////////////////////////
// PluginFactory
char gNotifierModuleName[] = MODULE_NAME_NOTIFIER;

AbstractPluginFactory* createPluginFactory(INotifier* pFactoryTypeHint) {
	return new UniversalPluginFactory<NotifierPlugin, NotifierPlugin, gNotifierModuleName>();
}

} // namespace KEP