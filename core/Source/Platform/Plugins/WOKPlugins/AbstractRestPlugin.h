///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Notifier.h"
#include "HTTPServer.h"

namespace KEP {

class TaskSystem;

class AbstractRESTPlugin : public AbstractPlugin, public HTTPServer::RequestHandler, public INotificationListener {
public:
	/**
	 * Constructor
	 *
	 * @param	[in] name - Module name, will be used as plugin name and HTTP request handler name.
	 * @param	[in] listenToNotification - TRUE = Enable notification listener for this plugin. FALSE = disable.
	 * @param	[in] asyncNotification - TRUE = Process notification asynchronously (listToNotification must be TRUE). FALSE = synchronously.
	 * @param	[in] loggerName - Logger name override. If NULL, use module name for logging.
	 */
	AbstractRESTPlugin(const char* name, bool listenToNotification = false, bool asyncNotification = false, const char* loggerName = NULL);

	/**
	 * Destructor
	 */
	~AbstractRESTPlugin();

	/**
	 * Register a REST end point
	 *
	 * Note that this function only defines an end point but does not register directly with HTTPServer. 
	 * It remembers the URL and does actual registration when startPlugin is called and deregistration when
	 * stopPlugin is called.
	 *
	 * @param	[in] ep - URL of the endpoint
	 */
	void addEndPoint(const std::string& ep);

	// Plugin interface override
	virtual Plugin& startPlugin();
	virtual KEP::ACTPtr stopPlugin();

	// INotificationListener override
	void Notify(const std::string& type, const std::map<std::string, std::string>& params);

	// Return Logger
	log4cplus::Logger& getLogger() { return m_logger; }

private:
	std::vector<std::string> m_restEndPoints;
	bool m_listenToNotification;

	TaskSystem* m_asyncNotificationQueue;
	bool m_asyncNotification;

protected:
	UINT m_port;

	log4cplus::Logger m_logger;

	/**
	 * Plugin logic to process incoming notifications.
	 *
	 * Override this function if the plugin listens to external notification.
	 *
	 * @param	[in] type - Notification type. A use-case-specific string keyword.
	 * @param	[in] params - Key-value assocations for all parameters applicable for the notification type. Use-case-specific.
	 */
	virtual void _doNotify(const std::string& type, const std::map<std::string, std::string>& params) {}

	friend class AsyncNotificationTask;
};

} // namespace KEP