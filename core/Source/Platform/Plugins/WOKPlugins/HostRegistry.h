///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Incubator.h"
#include "HostInfo.h"

namespace KEP {

class IHostRegistry {
public:
	/**
	 * Get the name of an available server based on load balancing.
	 *
	 * @return	server host name
	 */
	virtual std::string GetNextAvailServer() = 0;

	/**
	 * Add an active app to the app list of a hosting server (recording for load-balancing only)
	 *
	 * @param	[in] hostName - server host name
	 * @param	[in] id - app ID
	 */
	virtual void AssignAppToHost(const std::string& hostName, AppId id) = 0;

	/**
	 * Remove a suspended app from the app list of a hosting server (recording for load-balancing only)
	 *
	 * @param	[in] hostName - server host name
	 * @param	[in] id - app ID
	 */
	virtual void ReleaseAppFromHost(const std::string& hostName, AppId id) = 0;

	/**
	 * Add new server to hosting server list.
	 *
	 * @param	[in] hostName - server host name
	 * @param	[in] numSlots - maximum number of slots this new server can host (currently it's advisory only)
	 */
	virtual void AddHost(const std::string& hostName, int numSlots) = 0;

	/**
	 * Remove an existing server from hosting server list.
	 * 
	 * Consider using MarkHostUnavailable instead.
	 *
	 * @param	[in] hostName - server host name
	 */
	virtual void RemoveHost(const std::string& hostName) = 0;

	/**
	 * Return definitions and stats for a hosting server.
	 * 
	 * @param	[in] hostName - server host name
	 * @return	Host information record.
	 */
	virtual HostInfo GetHostInfo(const std::string& hostName) = 0;

	/**
	 * Mark a hosting server as available.
	 * 
	 * An unavailable server will stop serving new hosting request. Existing apps will continue to run until explicitly suspended.
	 * 
	 * @param	[in] hostName - server host name
	 */
	virtual void MarkHostAvailable(const std::string& hostName) = 0;

	/**
	 * Mark a hosting server as unavailable.
	 * 
	 * An unavailable server will stop serving new hosting request. Existing apps will continue to run until explicitly suspended.
	 * 
	 * @param	[in] hostName - server host name
	 */
	virtual void MarkHostUnavailable(const std::string& hostName) = 0;

	/**
	 * Get definitions and stats for all hosting servers.g request. Existing apps will continue to run until explicitly suspended.
	 * 
	 * @return	Map of records of all hosting servers.
	 */
	virtual std::map<std::string, HostInfo> EnumerateHosts() = 0;
};

AbstractPluginFactory* createPluginFactory(IHostRegistry* pFactoryTypeHint);

} // namespace KEP
