///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "js.h"
#include "jsEvent.h"
#include "TinyXML/tinyxml.h"
#include "log4cplus/logger.h"
#include "HTTPServer.h"
#include "RestResultParser.h"
#include "UrlParser.h"
#include "TaskSystem.h"
#include "TaskObject.h"
#include "RestClient.h"
#include "Utils.h"
#include "Tools\HttpClient\GetBrowserPage.h"

using namespace log4cplus;

#define DEFAULT_WEBCALL_THREADS 8

namespace KEP {

static Logger logger = Logger::getInstance(MODULE_NAME_REST_CLIENTW);

class RestClientPlugin : public IRESTClient, public AbstractPlugin, public TaskSystem {
public:
	RestClientPlugin(const TiXmlElement* config);
	~RestClientPlugin();

	// Plugin interface
	virtual Plugin& startPlugin();
	virtual ACTPtr stopPlugin();

	//Synchronous
	virtual int Call(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, RestResultParser* parser = NULL);
	virtual int Call(const URL& url, RestResultParser* parser = NULL);

	//Asynchronous
	virtual void Invoke(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, unsigned int ttl);

private:
	void _composeUrl(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, std::string& out);

	std::string m_localHostName;
	UINT m_numTaskThreads;
};

class GetBrowserPageTask : public TaskObject {
public:
	GetBrowserPageTask(const std::string& url, std::string* resultText, Logger& logger, DWORD* httpStatusCode, std::string* httpStatusDescription, BOOL* res, TimeMs ttl = INFINITE) :
			m_url(url), m_resultText(resultText), m_logger(logger), m_httpStatusCode(httpStatusCode), m_httpStatusDescription(httpStatusDescription), m_res(res), m_ttl(ttl) {
	}

	virtual void ExecuteTask() {
		DWORD httpStatusCode;
		std::string httpStatusDesc;
		std::string resultText;
		BOOL res;

		//Check if task expired
		TimeMs elapsed = m_timer.ElapsedMs();
		if (elapsed <= m_ttl) {
			//Execute task
			size_t resultSize = 0;
			res = GetBrowserPage(m_url, resultText, resultSize, httpStatusCode, httpStatusDesc);
		} else {
			//Drop task
			LOG4CPLUS_WARN(logger, "Drop expired web task: " << m_url);
			httpStatusCode = 500; // Use 500 for now to stay compatible with GetBrowserPage - to be changed
			res = FALSE;
		}

		if (m_resultText) {
			*m_resultText = resultText;
		}

		if (m_httpStatusCode) {
			*m_httpStatusCode = httpStatusCode;
		}

		if (m_httpStatusDescription) {
			*m_httpStatusDescription = httpStatusDesc;
		}

		if (m_res) {
			*m_res = res;
		}

		m_completionEvent.releaseWaiters();
	}

	void wait(TimeMs timeoutMs = INFINITE_WAIT) {
		m_completionEvent.wait((ULONG)timeoutMs);
	}

private:
	std::string m_url;
	Logger& m_logger;

	std::string* m_resultText;
	DWORD* m_httpStatusCode;
	std::string* m_httpStatusDescription;
	BOOL* m_res;

	Timer m_timer;
	TimeMs m_ttl;

	jsEvent m_completionEvent;
};

RestClientPlugin::RestClientPlugin(const TiXmlElement* config) :
		TaskSystem("RestClientPlugin"),
		AbstractPlugin(MODULE_NAME_REST_CLIENT),
		m_numTaskThreads(DEFAULT_WEBCALL_THREADS) {
	addSupportedInterface(INTERFACE_NAME_REST_CLIENT);

	m_localHostName = GetLocalHostName();

	if (config) {
		std::string localHostName;
		if (TIXML_SUCCESS == config->QueryValueAttribute("local_host", &localHostName)) {
			m_localHostName = localHostName;
		}

		const TiXmlElement* elem;
		elem = config->FirstChildElement("webcall_threads");
		if (elem && elem->GetText()) {
			m_numTaskThreads = (UINT)std::max(1, atoi(elem->GetText()));
		}
	}
}

RestClientPlugin::~RestClientPlugin() {
}

Plugin& RestClientPlugin::startPlugin() {
	Plugin& res = AbstractPlugin::startPlugin();

	Initialize(m_numTaskThreads);

	return res;
}

ACTPtr RestClientPlugin::stopPlugin() {
	Finalize();

	return AbstractPlugin::stopPlugin();
}

int RestClientPlugin::Call(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, RestResultParser* parser /*= NULL*/) {
	try {
		std::string fullUrl;
		_composeUrl(remoteHost, uri, params, fullUrl);
		return Call(URL::parse(fullUrl), parser);
	} catch (const ChainedException& httpe) {
		LOG4CPLUS_ERROR(logger, httpe.str().c_str());
		return APIN_REST_CALL_FAILED;
	}
}

int RestClientPlugin::Call(const URL& url, RestResultParser* parser /*= NULL*/) {
	std::string resultText;
	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;
	BOOL res = FALSE;

	GetBrowserPageTask* task = new GetBrowserPageTask(url.toString(), &resultText, logger, &httpStatusCode, &httpStatusDescription, &res);
	QueueTask(task);
	task->wait();

	if (!(res && httpStatusCode == 200)) {
		std::stringstream ss;
		ss << "Call to " << url.toString() << " failed " << httpStatusCode << " " << httpStatusDescription;
		throw HTTPException(SOURCELOCATION, ss.str());
	}

	LOG4CPLUS_DEBUG(logger, "Call to " << url.toString() << " succeeded " << httpStatusCode << " " << httpStatusDescription);
	if (parser)
		parser->parse(resultText.c_str(), resultText.length());
	return 0;
}

void RestClientPlugin::Invoke(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, unsigned int ttl) {
	std::string fullUrl;
	_composeUrl(remoteHost, uri, params, fullUrl);

	GetBrowserPageTask* task = new GetBrowserPageTask(fullUrl, NULL, logger, NULL, NULL, NULL, ttl);
	QueueTask(task);
}

void RestClientPlugin::_composeUrl(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, std::string& out) {
	out = "http://" + remoteHost + uri;
	if (!params.empty()) {
		for (auto it = params.cbegin(); it != params.cend(); ++it) {
			std::string key = UrlParser::escapeString(it->first);
			std::string val = UrlParser::escapeString(it->second);

			if (it == params.begin()) {
				out += "?";
			} else {
				out += "&";
			}

			out += key + "=" + val;
		}
	}
}

////////////////////////////////////////////////////////////////////////
// PluginFactory
char gRestClientModuleName[] = MODULE_NAME_REST_CLIENT;

AbstractPluginFactory* createPluginFactory(IRESTClient* pFactoryTypeHint) {
	return new UniversalPluginFactory<RestClientPlugin, RestClientPlugin, gRestClientModuleName>();
}

} // namespace KEP