///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Plugin.h"

namespace KEP {

enum AlertLevel { ALERT_GREEN,
	ALERT_BLUE,
	ALERT_YELLOW,
	ALERT_ORANGE,
	ALERT_RED,
	NUM_ALERT_LEVELS }; // System alert level

//Undef Win32 macro
#undef ReportEvent

class IAlertService {
public:
	/* 
	 * Report an isolated platform event
	 */
	virtual void ReportEvent(const std::string& source, int eventId) = 0;
	virtual void SendAlert(AlertLevel level, const std::string& subject, const std::string& body) = 0;
};

AbstractPluginFactory* createPluginFactory(IAlertService* pFactoryTypeHint);

} // namespace KEP