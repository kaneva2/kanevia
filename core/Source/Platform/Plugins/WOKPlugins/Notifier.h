///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Incubator.h"

namespace KEP {

class INotificationListener {
public:
	virtual void Notify(const std::string& type, const std::map<std::string, std::string>& params) = 0;
};

/**
 * General purpose pub/sub event dispatching mechanism.
 * @see NotifierTests.h for usage examples.
 */
class INotifier {
public:
	/**
	 * Subscribe to listen for notifications.
	 *
	 * @param	module		Named module, but in reality it is simply an arbitrary key under 
	 *						which the listener is registered. Subsequent calls to INotifier::Notify()
	 *						with this key will cause the type of notice and parameters to be
	 *						be passed to INotifiactionListener::Notify().
	 *
	 *						Note that for any given key, there can be only one listener.  Adding a 
	 *						listener with the same name as a previously added listener, will cause the 
	 *						previous listener to be removed.
	 *
	 * @param   listener	The actual listener to which notifications, identified by key,
	 *						are dispatched.
	 */
	virtual void AddListener(const std::string& module, INotificationListener* listener) = 0;

	/**
	 * Unsubscribe from notifications
	 */
	virtual void RemoveListener(const std::string& module) = 0;

	/**
	 * Dispatch a notification/event.
	 *
	 * @param	module		Key identifying the listener.  Note that for any given key, there
	 *						can be only one listener.  Adding a listener with the same name
	 *						as a previously added listener, will cause the previous listener
	 *						to be removed.
	 * @param	type		Passed to the listener.
	 * @param	params		Passed to the listener.
	 */
	virtual void Notify(const std::string& module, const std::string& type, const std::map<std::string, std::string>& params) = 0;
};

AbstractPluginFactory* createPluginFactory(INotifier* pFactoryTypeHint);

} // namespace KEP