///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Incubator.h"

namespace KEP {

class IWOKProvisioner {
public:
	/**
	 * Start one or all KGPServer process
	 *
	 * @param	[in] hostName
	 * @param	[in] port
	 */
	virtual void StartWOKServer(const std::string& hostName, unsigned int port) = 0;

	/**
	 * Stop one or all KGPServer process
	 *
	 * @param	[in] hostName
	 * @param	[in] port	// -1 = all active instances
	 */
	virtual void StopWOKServer(const std::string& hostName, unsigned int port) = 0;

	/**
	 * Query WOK server status
	 *
	 * @param	[in] hostName	// "" = all known hosts
	 * @param	[in] port		// -1 = all active instances
	 */
	//virtual void QueryWOKServer( const std::string & hostName, unsigned int port ) = 0;

	/**
	 * Update capacity for a specific server
	 *
	 * @param	[in] hostName - server host name
	 * @param	[in] numSlots - maximum number of slots this new server can host
	 */
	//virtual void SetHostCapacity( const std::string & hostName, int numSlots ) = 0;
};

AbstractPluginFactory* createPluginFactory(IWOKProvisioner* pFactoryTypeHint);

} // namespace KEP