///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "incubator.h"
#include "HostInfo.h"
#include "RestHandlerHelper.h"
#include "Utils.h"

#include "HTTPServer.h"

#include "TinyXML/tinyxml.h"

namespace KEP {

////////////////////////////////////////////////////////////////////////
// HostInfo streaming
HTTPServer::Response& operator<<(HTTPServer::Response& resp, const HostInfo& hi) {
	std::stringstream ssLastPing;
	ssLastPing << hi.lastPing;

	// Seems to me that this info should be encapsulated in hostinfo tags
	//
	return resp << "<name>" << hi.hostName << "</name>"
				<< "<slots>" << hi.numSlots << "</slots>"
				<< "<last_ping>" << ssLastPing.str() << "</last_ping>"
				<< "<used>" << hi.numUsedSlots << "</used>"
				<< "<available>" << hi.avail << "</available>";
}

////////////////////////////////////////////////////////////////////////
// Enumerate hosts streaming
HTTPServer::Response& operator<<(HTTPServer::Response& resp, const HostInfoMap& hosts) {
	std::string pingDateTime;

	resp << "<hosts number=\"";
	resp << hosts.size();
	resp << "\">";

	for (HostInfoMap::const_iterator it = hosts.begin(); it != hosts.end(); ++it) {
		std::string hostname = it->first;
		const HostInfo& hi = it->second;
		std::string pingDateTime = convertToLocalTime(hi.lastPing);
		std::stringstream ssLastPing;
		ssLastPing << hi.lastPing;

		resp << "<host name=\"" << hostname.c_str() << "\">";
		resp << "<slots>" << hi.numSlots << "</slots>";
		resp << "<available>" << hi.avail << "</available>";
		resp << "<lastPing>" << pingDateTime << "</lastPing>";
		resp << "<last_ping>" << ssLastPing.str() << "</last_ping>"; // non-formatted ping time
		resp << "</host>";
	}

	resp << "</hosts>";

	return resp;
}

////////////////////////////////////////////////////////////////////////
// Parser
void HostInfoParser::parse(const void* content, size_t contentLen) {
	m_output.numSlots = 0;
	m_output.lastPing = 0;
	m_output.numUsedSlots = 0;

	std::string sContent;
	sContent.append((const char*)content, contentLen);

	TiXmlDocument doc;
	doc.Parse(sContent.c_str());

	TiXmlElement* elem = doc.FirstChildElement("result");
	if (elem) {
		TiXmlElement* name = elem->FirstChildElement("name");
		TiXmlElement* numSlots = elem->FirstChildElement("slots");
		TiXmlElement* lastPing = elem->FirstChildElement("last_ping");
		TiXmlElement* numUsedSlots = elem->FirstChildElement("used");
		TiXmlElement* available = elem->FirstChildElement("available");
		if (name && name->GetText() && numSlots && numSlots->GetText() && lastPing && lastPing->GetText() && numUsedSlots && numUsedSlots->GetText()) {
			m_output.hostName = name->GetText();
			m_output.numSlots = strtoul(numSlots->GetText(), NULL, 10);
			m_output.lastPing = _atoi64(lastPing->GetText());
			m_output.numUsedSlots = strtoul(numUsedSlots->GetText(), NULL, 10);
			m_output.avail = atoi(available->GetText()) != 0;
		}
	}
}

HostInfoMapParser::HostInfoMapParser(HostInfoMap& output) :
		m_output(output) {}

//	<result>
//		<hosts number=1>
//			<host name="TestHost01">
//				<slots>4</slots>
//				<avail>1</avail>
//				<lastPing>4/4/2012 1:21:48 AM</lastPing>
//				<last_ping>999999</last_ping>
//			</host>
//		</hosts>
//	</result>
void HostInfoMapParser::parse(const void* content, size_t contentLen) {
	m_content.append((const char*)content, contentLen);

	TiXmlDocument doc;
	doc.Parse(m_content.c_str());

	TiXmlElement* resultElem = doc.FirstChildElement("result");
	if (resultElem == 0)
		return;
	TiXmlElement* hostsElement = resultElem->FirstChildElement("hosts");
	if (hostsElement == 0)
		return;

	TiXmlNode* hostNode;
	for (hostNode = hostsElement->FirstChild("host"); hostNode != 0; hostNode = hostNode->NextSibling()) {
		TiXmlElement* hostElement = (TiXmlElement*)hostNode;
		std::string hostName = hostElement->Attribute("name");

		TiXmlElement* slotsElement = hostNode->FirstChild("slots")->ToElement();
		if (!slotsElement)
			continue; // error

		TiXmlElement* availElement = hostNode->FirstChild("available")->ToElement();
		if (!availElement)
			continue; // error

		TiXmlElement* lastPingElement = hostNode->FirstChild("last_ping")->ToElement();
		if (!lastPingElement)
			continue; // error

		std::string name = hostElement->Attribute("name");
		HostInfo hi(name, atoi(slotsElement->FirstChild()->Value()));
		hi.avail = atoi(availElement->FirstChild()->Value()) != 0;
		hi.lastPing = _atoi64(lastPingElement->FirstChild()->Value());
		m_output.insert(std::pair<std::string, HostInfo>(name, HostInfo(hi)));
	}
};

} // namespace KEP