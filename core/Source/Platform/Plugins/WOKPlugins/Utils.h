///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define NUM_CLUNKS_PER_MSEC 10000ULL // conversion between 100nsecs and secs
#define NUM_CLUNKS_PER_SECOND (NUM_CLUNKS_PER_MSEC * 1000) // conversion between 100nsecs and secs

namespace KEP {

UINT64 GetSystemTimeUINT64();
std::string GetLocalHostName();
std::string convertToLocalTime(UINT64 timeIn);

} // namespace KEP