///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "incubator.h"
#include "AppHeartbeatMap.h"

#define HBMAP_DELIMITER "!"
#define HBMAP_EQUAL ":"

namespace KEP {

////////////////////////////////////////////////////////////////////////
// Parameter encoding/decoding
std::stringstream& operator<<(std::stringstream& ss, const AppHeartbeatMap& hbm) {
	for (AppHeartbeatMap::const_iterator it = hbm.begin(); it != hbm.end(); ++it) {
		ss << it->first << HBMAP_EQUAL << it->second.first << HBMAP_EQUAL << it->second.second << HBMAP_DELIMITER;
	}

	return ss;
}

AppHeartbeatMap DecodeHeartbeats(const std::string& str) {
	AppHeartbeatMap res;

	std::string::size_type lastOfs = 0;
	std::string::size_type currOfs;

	while ((currOfs = str.find(HBMAP_DELIMITER, lastOfs)) != std::string::npos) {
		std::string heartbeatToken = str.substr(lastOfs, currOfs - lastOfs);

		std::vector<std::string> tokens;
		STLTokenize(heartbeatToken, tokens, HBMAP_EQUAL);

		jsAssert(tokens.size() == 3);

		if (tokens.size() == 3) {
			std::string sAppId = tokens[0];
			std::string sTimestamp = tokens[1];
			std::string sNumUsers = tokens[2];

			if (!sAppId.empty() && !sTimestamp.empty() && !sNumUsers.empty()) {
				AppId appId = atoi(sAppId.c_str());
				UINT64 timestamp = _strtoui64(sTimestamp.c_str(), NULL, 10);
				int numUsers = atoi(sNumUsers.c_str());

				res.insert(std::make_pair(appId, std::make_pair(timestamp, numUsers)));
			}
		}

		lastOfs = currOfs + 1;
	}

	return res;
}

} // namespace KEP