///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "HostRegistry.h"
#include "RestClient.h"
#include "AbstractRestPlugin.h"
#include "Utils.h"
#include "HTTPServer.h"
#include "TinyXML/tinyxml.h"
#include "log4cplus/logger.h"
#include <common/include/KEPException.h>
#include <set>
#include "js.h"
#include "jsSync.h"
#include "jsLock.h"
#include "RestHandlerHelper.h"
#include "StubHelper.h"

using namespace log4cplus;

namespace KEP {

static Logger logger = Logger::getInstance(_T(MODULE_NAME_HOSTREGISTRY));

HTTPServer::Response &operator<<(HTTPServer::Response &resp, const HostInfo &hi);
HTTPServer::Response &operator<<(HTTPServer::Response &resp, const HostInfoMap &hosts);

jsFastSync m_sync;

class HostRegistryPlugin : public IHostRegistry, public AbstractRESTPlugin {
public:
	HostRegistryPlugin(const TiXmlElement *config);
	~HostRegistryPlugin();

	// HTTPServer::RequestHandler
	virtual int handle(const HTTPServer::Request &request, HTTPServer::EndPoint &endPoint);

	// IHostRegistry
	virtual std::string GetNextAvailServer();
	virtual void AssignAppToHost(const std::string &hostName, AppId id);
	virtual void ReleaseAppFromHost(const std::string &hostName, AppId id);
	virtual void AddHost(const std::string &hostName, int numSlots);
	virtual void RemoveHost(const std::string &hostName);
	virtual HostInfo GetHostInfo(const std::string &hostName);
	virtual void MarkHostAvailable(const std::string &hostName);
	virtual void MarkHostUnavailable(const std::string &hostName);
	virtual std::map<std::string, HostInfo> EnumerateHosts();

private:
	HostInfoMap m_hosts;
	std::map<std::string, std::set<AppId>> m_apps;
};

HostRegistryPlugin::HostRegistryPlugin(const TiXmlElement *config) :
		AbstractRESTPlugin(MODULE_NAME_HOSTREGISTRY) {
	addSupportedInterface(INTERFACE_NAME_HOSTREGISTRY);

	addEndPoint("/GetNextAvailServer");
	addEndPoint("/AssignAppToHost");
	addEndPoint("/ReleaseAppFromHost");
	addEndPoint("/AddHost");
	addEndPoint("/RemoveHost");
	addEndPoint("/GetHostInfo");
	addEndPoint("/MarkHostAvailable");
	addEndPoint("/MarkHostUnavailable");
	addEndPoint("/EnumerateHosts");
}

HostRegistryPlugin::~HostRegistryPlugin() {
}

std::string HostRegistryPlugin::GetNextAvailServer() {
	LOG4CPLUS_DEBUG(m_logger, "HostRegistry::GetNextAvailServer");

	std::string candName;
	float candCap = 999999.0f;
	int randomizationCounter = 1;

	jsFastLock lock(m_sync);

	for (HostInfoMap::const_iterator it = m_hosts.begin(); it != m_hosts.end(); ++it) {
		std::map<std::string, std::set<AppId>>::const_iterator itApps = m_apps.find(it->first);

		const HostInfo &hi = it->second;
		size_t totalApps = (size_t)itApps->second.size();

		if (hi.avail) {
			float cap = totalApps * 1.0f / hi.numSlots;
			if (cap < candCap) {
				candCap = cap;
				candName = it->first;

				// Selected new candidate with less load - reset randomization counter
				randomizationCounter = 1;
			} else if (cap == candCap) {
				// Found another equally-loaded candidate - select randomly
				randomizationCounter++;

				if (rand() % randomizationCounter == 0) // 2nd candidate has 50% chance to be selected , 3rd has 33%, etc.
				{
					// Use new store
					candName = it->first;
				}
			}
		}
	}

	if (candCap + 1E-3 > 1.0f) {
		//Log over-subscription
		LOG4CPLUS_WARN(m_logger, "HostRegistry::GetNextAvailServer: use oversubscribed server [" << candName << "], current load " << (floor(candCap * 10000 + 0.5f) / 100.0f) << "%");
	}

	LOG4CPLUS_INFO(m_logger, "HostRegistry::GetNextAvailServer returned " << candName);
	return candName;
}

void HostRegistryPlugin::AssignAppToHost(const std::string &hostName, AppId id) {
	LOG4CPLUS_DEBUG(m_logger, "HostRegistry::AssignAppToHost( hostName = " << hostName << ", id = " << id << " )");

	jsFastLock lock(m_sync);

	HostInfoMap::iterator itHosts = m_hosts.find(hostName);
	if (itHosts == m_hosts.end()) {
		lock.unlock();
		throw new KEPException("Invalid host name", APIN_HOSTREGISTRY_INVALID_HOST);
	}

	m_apps[hostName].insert(id);
	itHosts->second.numUsedSlots = m_apps[hostName].size();
}

void HostRegistryPlugin::ReleaseAppFromHost(const std::string &hostName, AppId id) {
	LOG4CPLUS_DEBUG(m_logger, "HostRegistry::ReleaseAppFromHost( hostName = " << hostName << ", id = " << id << " )");

	jsFastLock lock(m_sync);

	HostInfoMap::iterator itHosts = m_hosts.find(hostName);
	if (itHosts == m_hosts.end()) {
		lock.unlock();
		throw new KEPException("Invalid host name", APIN_HOSTREGISTRY_INVALID_HOST);
	}

	std::set<AppId>::iterator itApp = m_apps[hostName].find(id);
	if (itApp != m_apps[hostName].end()) {
		m_apps[hostName].erase(itApp);
	}

	itHosts->second.numUsedSlots = m_apps[hostName].size();
}

void HostRegistryPlugin::AddHost(const std::string &hostName, int numSlots) {
	LOG4CPLUS_DEBUG(m_logger, "HostRegistry::AddHost( hostName = " << hostName << ", numSlots = " << numSlots << " )");

	jsFastLock lock(m_sync);

	HostInfoMap::iterator it = m_hosts.find(hostName);
	if (it != m_hosts.end()) {
		lock.unlock();
		throw new KEPException("Host already exists", APIN_HOSTREGISTRY_HOST_ALREADY_EXISTS);
	}

	m_hosts.insert(make_pair(hostName, HostInfo(hostName, numSlots)));
	m_apps.insert(make_pair(hostName, std::set<AppId>()));
}

void HostRegistryPlugin::RemoveHost(const std::string &hostName) {
	LOG4CPLUS_DEBUG(m_logger, "HostRegistry::RemoveHost( hostName = " << hostName << " )");

	jsFastLock lock(m_sync);

	HostInfoMap::iterator itHosts = m_hosts.find(hostName);
	if (itHosts == m_hosts.end()) {
		lock.unlock();
		throw new KEPException("Invalid host name", APIN_HOSTREGISTRY_INVALID_HOST);
	}

	m_hosts.erase(itHosts);

	std::map<std::string, std::set<AppId>>::iterator itApps = m_apps.find(hostName);
	if (itApps != m_apps.end()) {
		m_apps.erase(itApps);
	}
}

HostInfo HostRegistryPlugin::GetHostInfo(const std::string &hostName) {
	LOG4CPLUS_TRACE(m_logger, "HostRegistry::GetHostInfo( hostName = " << hostName << " )");

	static HostInfo null;
	jsFastLock lock(m_sync);

	std::map<std::string, HostInfo>::iterator itHosts = m_hosts.find(hostName);
	if (itHosts != m_hosts.end()) {
		return itHosts->second;
	}

	return null;
}

void HostRegistryPlugin::MarkHostAvailable(const std::string &hostName) {
	LOG4CPLUS_TRACE(m_logger, "HostRegistry::MarkHostAvailable( hostName = " << hostName << " )");

	jsFastLock lock(m_sync);

	HostInfoMap::iterator itHosts = m_hosts.find(hostName);
	if (itHosts == m_hosts.end()) {
		lock.unlock(); // superfluous.  unlocked implecently when this call is popped off the stack.
		throw new KEPException("Invalid host name", APIN_HOSTREGISTRY_INVALID_HOST);
	}

	if (!itHosts->second.avail) {
		LOG4CPLUS_INFO(m_logger, "HostRegistry::MarkHostAvailable( hostName = " << hostName << " ) : set avail flag to TRUE");
		itHosts->second.avail = true;
	}
}

void HostRegistryPlugin::MarkHostUnavailable(const std::string &hostName) {
	LOG4CPLUS_TRACE(m_logger, "HostRegistry::MarkHostUnavailable( hostName = " << hostName << " )");

	jsFastLock lock(m_sync);

	HostInfoMap::iterator itHosts = m_hosts.find(hostName);
	if (itHosts == m_hosts.end()) {
		lock.unlock(); // This lock will dissolve on the throw.  This call is superfluous.
		throw new KEPException("Invalid host name", APIN_HOSTREGISTRY_INVALID_HOST);
	}

	if (itHosts->second.avail) {
		itHosts->second.avail = false;
		LOG4CPLUS_INFO(m_logger, "HostRegistry::MarkHostUnavailable( hostName = " << hostName << " ) : set avail flag to " << (itHosts->second.avail ? "TRUE" : "FALSE"));
	}
}

HostInfoMap HostRegistryPlugin::EnumerateHosts() {
	LOG4CPLUS_DEBUG(m_logger, "HostRegistry::EnumerateHosts");

	// for copy constructor
	jsFastLock lock(m_sync);

	return m_hosts;
}

////////////////////////////////////////////////////////////////////////
// REST handler
BEGIN_REST_HANDLER_MAP(HostRegistry)
// Initialize pseudorandom number generator for GetNextAvailServer function.
srand((unsigned int)time(NULL));
REST_FUNC__0(GetNextAvailServer, std::string);
REST_PROC__2(AssignAppToHost, std::string, host, NOCAST, AppId, appid, atoi);
REST_PROC__2(ReleaseAppFromHost, std::string, host, NOCAST, AppId, appid, atoi);
REST_PROC__2(AddHost, std::string, name, NOCAST, int, slots, atoi);
REST_PROC__1(RemoveHost, std::string, name, NOCAST);
REST_FUNC__1(GetHostInfo, std::string, name, NOCAST, HostInfo);
REST_PROC__1(MarkHostAvailable, std::string, name, NOCAST);
REST_PROC__1(MarkHostUnavailable, std::string, name, NOCAST);
REST_FUNC__0(EnumerateHosts, HostInfoMap);
END_REST_HANDLER_MAP

////////////////////////////////////////////////////////////////////////
// Stub class for remote interface

BEGIN_STUB_CLASS(HostRegistry, MODULE_NAME_HOSTREGISTRY)
MARSHAL_FUNC__0(GetNextAvailServer, std::string, StringParser);
MARSHAL_PROC__2(AssignAppToHost, const std::string &, name, int, slots);
MARSHAL_PROC__2(ReleaseAppFromHost, const std::string &, name, int, slots);
MARSHAL_PROC__2(AddHost, const std::string &, name, int, slots);
MARSHAL_PROC__1(RemoveHost, const std::string &, name);
MARSHAL_FUNC__1(GetHostInfo, const std::string &, server, HostInfo, HostInfoParser);
MARSHAL_PROC__1(MarkHostAvailable, const std::string &, name);
MARSHAL_PROC__1(MarkHostUnavailable, const std::string &, name);
MARSHAL_FUNC__0(EnumerateHosts, HostInfoMap, HostInfoMapParser); // not implemented
END_STUB_CLASS

////////////////////////////////////////////////////////////////////////
// PluginFactory
char gHostRegistryModuleName[] = MODULE_NAME_HOSTREGISTRY;

AbstractPluginFactory *createPluginFactory(IHostRegistry *pFactoryTypeHint) {
	return new UniversalPluginFactory<HostRegistryPlugin, HostRegistryStub, gHostRegistryModuleName>();
}

} // namespace KEP