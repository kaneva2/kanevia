///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Incubator.h"
#include "RestClient.h"
#include "HostRegistry.h"
#include "AppController.h"
#include "Notifier.h"
#include "AlertService.h"
#include "WOKProvisioner.h"

using namespace KEP;

#define TYPEHINT(T) ((T*)NULL)

HINSTANCE StringResource = 0;

extern "C" __declspec(dllexport) void EnumeratePlugins(PluginFactoryVecPtr v) {
	// Add our plugin factory to the library
	// We allocate and forget about it.
	//
	v->push_back(PluginFactoryPtr(createPluginFactory(TYPEHINT(IRESTClient))));
	v->push_back(PluginFactoryPtr(createPluginFactory(TYPEHINT(IHostRegistry))));
	v->push_back(PluginFactoryPtr(createPluginFactory(TYPEHINT(IAppController))));
	v->push_back(PluginFactoryPtr(createPluginFactory(TYPEHINT(INotifier))));
	v->push_back(PluginFactoryPtr(createPluginFactory(TYPEHINT(IAlertService))));
	v->push_back(PluginFactoryPtr(createPluginFactory(TYPEHINT(IWOKProvisioner))));
}

extern "C" BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD ul_reason_for_call,
	LPVOID lpReserved) {
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
		StringResource = (HINSTANCE)hModule;
	return TRUE;
}
