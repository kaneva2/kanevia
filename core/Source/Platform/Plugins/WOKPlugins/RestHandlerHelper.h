///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define BEGIN_REST_HANDLER_MAP(INTF) int INTF##Plugin::handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
#define BEGIN_REST_HANDLER_CUSTOM(CLASS) int CLASS::handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
#define END_REST_HANDLER_MAP \
	return 0;                \
	}

#define NOCAST(x) (x)
#define STRTOUL(x) strtoul(x, NULL, 10)
#define STRTOPTR(x) ((LPVOID)strtoul(x, NULL, 10))
#define NO_ERROR_CB(type, data, err, errMsg)

//Marshaling helper stream
class paramstream : public std::stringstream {
public:
	paramstream() {}
	paramstream(const std::string& str) :
			std::stringstream(str) {}

	template <typename T>
	void encodeStructMemberRaw(const std::string& name, const T& val) {
		*this << name << ":" << val << ",";
	}

	void encodeStructMemberStr(const std::string& name, const std::string& val) {
		std::string valEncoded = val;
		STLStringSubstitute(valEncoded, "`", "``");
		STLStringSubstitute(valEncoded, ",", "`,");
		STLStringSubstitute(valEncoded, ":", "`:");
		*this << name << ":" << valEncoded << ",";
	}

	void decodeStructMember(std::string& name, std::string& val) {
		name.clear();
		val.clear();

		// Decode name
		while (!eof()) {
			char ch;
			*this >> ch;

			if (ch == ':') {
				break;
			}

			name.push_back(ch);
		}

		// Decode value
		while (!eof()) {
			char ch;
			*this >> ch;

			if (ch == ',') {
				break;
			}

			if (ch == '`') {
				if (eof()) {
					break;
				}

				*this >> ch;
			}

			val.push_back(ch);
		}
	}
};

template <typename T>
inline T ParamStreamDecoder(const char* encoded) {
	std::string str(encoded);
	paramstream ps(str);

	T out;
	ps >> out;

	return out;
}

#define TRY_REST_CALLBACK \
	try {
#define CATCH_REST_CALLBACK_EXCEPTIONS(LOGGER)                                                                                                                               \
	}                                                                                                                                                                        \
	catch (KEP::KEPException * e) {                                                                                                                                          \
		unsigned int callbackData = 0xFFFFFFFF;                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itCBData = params.find("_callbackdata");                                                                          \
		if (itCBData != params.end()) {                                                                                                                                      \
			callbackData = STRTOUL(itCBData->second.c_str());                                                                                                                \
		}                                                                                                                                                                    \
		if (LOGGER != NULL) {                                                                                                                                                \
			LOG4CPLUS_WARN((*LOGGER), "Unhandled exception occurred in REST callback handler, type=" << type << ", callbackData=" << callbackData << ": " << e->ToString()); \
		}                                                                                                                                                                    \
		delete e;                                                                                                                                                            \
	}                                                                                                                                                                        \
	catch (const KEP::KEPException& e) {                                                                                                                                     \
		unsigned int callbackData = 0xFFFFFFFF;                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itCBData = params.find("_callbackdata");                                                                          \
		if (itCBData != params.end()) {                                                                                                                                      \
			callbackData = STRTOUL(itCBData->second.c_str());                                                                                                                \
		}                                                                                                                                                                    \
		if (LOGGER != NULL) {                                                                                                                                                \
			LOG4CPLUS_WARN((*LOGGER), "Unhandled exception occurred in REST callback handler, type=" << type << ", callbackData=" << callbackData << ": " << e.ToString());  \
		}                                                                                                                                                                    \
	}

#define REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC)                                                       \
	{                                                                                                                  \
		std::map<std::string, std::string>::const_iterator itCBData = params.find("_callbackdata");                    \
		std::map<std::string, std::string>::const_iterator itErr = params.find("_err");                                \
		std::map<std::string, std::string>::const_iterator itMsg = params.find("_errmsg");                             \
		if (itCBData != params.end() && itErr != params.end() && itMsg != params.end()) {                              \
			ERRFUNC(NOTIFICATION_TYPE, STRTOUL(itCBData->second.c_str()), atoi(itErr->second.c_str()), itMsg->second); \
			return;                                                                                                    \
		}                                                                                                              \
	}

#define GET_CALLBACK_PARAMETERS                                                         \
	std::map<std::string, std::string>::const_iterator itCallback;                      \
	std::string callbackModuleName;                                                     \
	itCallback = request._paramMap.find("_callback");                                   \
	if (itCallback != request._paramMap.end()) {                                        \
		callbackModuleName = itCallback->second;                                        \
	}                                                                                   \
                                                                                        \
	std::map<std::string, std::string>::const_iterator itCallbackData;                  \
	unsigned int callbackData = 0;                                                      \
	itCallbackData = request._paramMap.find("_callbackdata");                           \
	if (itCallbackData != request._paramMap.end()) {                                    \
		callbackData = (unsigned int)strtoul(itCallbackData->second.c_str(), NULL, 10); \
	}                                                                                   \
                                                                                        \
	std::map<std::string, std::string>::const_iterator itCallbackHostName;              \
	std::string callbackHostName = "localhost";                                         \
	itCallbackHostName = request._paramMap.find("_callbackhostname");                   \
	if (itCallbackHostName != request._paramMap.end()) {                                \
		callbackHostName = itCallbackData->second;                                      \
	}

#define REST_CALL__0__PREPROC(URI, FUNC) \
	if (request._uri == URI) {           \
		HTTPServer::Response resp;       \
                                         \
		try {
#define REST_CALL__0__POSTPROC                                                   \
	resp << "<result>" << res << "</result>";                                    \
	}                                                                            \
	catch (KEP::KEPException * e) {                                              \
		resp << "<error code=\"" << e->m_err << "\">" << e->m_msg << "</error>"; \
		delete e;                                                                \
	}                                                                            \
	catch (const KEP::KEPException& e) {                                         \
		resp << "<error code=\"" << e.m_err << "\">" << e.m_msg << "</error>";   \
	}                                                                            \
	endPoint.write(resp);                                                        \
	return 1;                                                                    \
	}

#define REST_CALL__1__PREPROC(URI, FUNC, TYPE1, ARG1, CAST1)                        \
	if (request._uri == URI) {                                                      \
		std::map<std::string, std::string>::const_iterator itArg1 = request._paramMap.find(#ARG1); \
		if (itArg1 == request._paramMap.end()) {                                    \
			/* Missing parameter */                                                 \
			endPoint.sendError(501, "Invalid request");                             \
		} else {                                                                    \
			HTTPServer::Response resp;                                              \
                                                                                    \
			try {                                                                   \
				TYPE1 ARG1 = CAST1(itArg1->second.c_str());

#define REST_CALL__1__POSTPROC                                                   \
	resp << "<result>" << res << "</result>";                                    \
	}                                                                            \
	catch (KEP::KEPException * e) {                                              \
		resp << "<error code=\"" << e->m_err << "\">" << e->m_msg << "</error>"; \
		delete e;                                                                \
	}                                                                            \
	catch (const KEP::KEPException& e) {                                         \
		resp << "<error code=\"" << e.m_err << "\">" << e.m_msg << "</error>";   \
	}                                                                            \
	endPoint.write(resp);                                                        \
	}                                                                            \
	return 1;                                                                    \
	}

#define REST_CALL__2__PREPROC(URI, FUNC, TYPE1, ARG1, CAST1, TYPE2, ARG2, CAST2)      \
	if (request._uri == URI) {                                                        \
		std::map<std::string, std::string>::const_iterator itArg1 = request._paramMap.find(#ARG1);   \
		std::map<std::string, std::string>::const_iterator itArg2 = request._paramMap.find(#ARG2);   \
		if (itArg1 == request._paramMap.end() || itArg2 == request._paramMap.end()) { \
			/* Missing parameter */                                                   \
			endPoint.sendError(501, "Invalid request");                               \
		} else {                                                                      \
			HTTPServer::Response resp;                                                \
                                                                                      \
			try {                                                                     \
				TYPE1 ARG1 = CAST1(itArg1->second.c_str());                           \
				TYPE2 ARG2 = CAST2(itArg2->second.c_str());

#define REST_CALL__2__POSTPROC REST_CALL__1__POSTPROC

#define REST_CALL__3__PREPROC(URI, FUNC, TYPE1, ARG1, CAST1, TYPE2, ARG2, CAST2, TYPE3, ARG3, CAST3)                       \
	if (request._uri == URI) {                                                                                             \
		std::map<std::string, std::string>::const_iterator itArg1 = request._paramMap.find(#ARG1);                         \
		std::map<std::string, std::string>::const_iterator itArg2 = request._paramMap.find(#ARG2);                         \
		std::map<std::string, std::string>::const_iterator itArg3 = request._paramMap.find(#ARG3);                         \
		if (itArg1 == request._paramMap.end() || itArg2 == request._paramMap.end() || itArg3 == request._paramMap.end()) { \
			/* Missing parameter */                                                                                        \
			endPoint.sendError(501, "Invalid request");                                                                    \
		} else {                                                                                                           \
			HTTPServer::Response resp;                                                                                     \
                                                                                                                           \
			try {                                                                                                          \
				TYPE1 ARG1 = CAST1(itArg1->second.c_str());                                                                \
				TYPE2 ARG2 = CAST2(itArg2->second.c_str());                                                                \
				TYPE3 ARG3 = CAST3(itArg3->second.c_str());

#define REST_CALL__3__POSTPROC REST_CALL__1__POSTPROC

#define REST_CALL__4__PREPROC(URI, FUNC, TYPE1, ARG1, CAST1, TYPE2, ARG2, CAST2, TYPE3, ARG3, CAST3, TYPE4, ARG4, CAST4)                                        \
	if (request._uri == URI) {                                                                                                                                  \
		std::map<std::string, std::string>::const_iterator itArg1 = request._paramMap.find(#ARG1);                                                              \
		std::map<std::string, std::string>::const_iterator itArg2 = request._paramMap.find(#ARG2);                                                              \
		std::map<std::string, std::string>::const_iterator itArg3 = request._paramMap.find(#ARG3);                                                              \
		std::map<std::string, std::string>::const_iterator itArg4 = request._paramMap.find(#ARG4);                                                              \
		if (itArg1 == request._paramMap.end() || itArg2 == request._paramMap.end() || itArg3 == request._paramMap.end() || itArg4 == request._paramMap.end()) { \
			/* Missing parameter */                                                                                                                             \
			endPoint.sendError(501, "Invalid request");                                                                                                         \
		} else {                                                                                                                                                \
			HTTPServer::Response resp;                                                                                                                          \
                                                                                                                                                                \
			try {                                                                                                                                               \
				TYPE1 ARG1 = CAST1(itArg1->second.c_str());                                                                                                     \
				TYPE2 ARG2 = CAST2(itArg2->second.c_str());                                                                                                     \
				TYPE3 ARG3 = CAST3(itArg3->second.c_str());                                                                                                     \
				TYPE4 ARG4 = CAST4(itArg4->second.c_str());

#define REST_CALL__4__POSTPROC REST_CALL__1__POSTPROC

#define REST_CALL__5__PREPROC(URI, FUNC, TYPE1, ARG1, CAST1, TYPE2, ARG2, CAST2, TYPE3, ARG3, CAST3, TYPE4, ARG4, CAST4, TYPE5, ARG5, CAST5)                                                         \
	if (request._uri == URI) {                                                                                                                                                                       \
		std::map<std::string, std::string>::const_iterator itArg1 = request._paramMap.find(#ARG1);                                                                                                   \
		std::map<std::string, std::string>::const_iterator itArg2 = request._paramMap.find(#ARG2);                                                                                                   \
		std::map<std::string, std::string>::const_iterator itArg3 = request._paramMap.find(#ARG3);                                                                                                   \
		std::map<std::string, std::string>::const_iterator itArg4 = request._paramMap.find(#ARG4);                                                                                                   \
		std::map<std::string, std::string>::const_iterator itArg5 = request._paramMap.find(#ARG5);                                                                                                   \
		if (itArg1 == request._paramMap.end() || itArg2 == request._paramMap.end() || itArg3 == request._paramMap.end() || itArg4 == request._paramMap.end() || itArg5 == request._paramMap.end()) { \
			/* Missing parameter */                                                                                                                                                                  \
			endPoint.sendError(501, "Invalid request");                                                                                                                                              \
		} else {                                                                                                                                                                                     \
			HTTPServer::Response resp;                                                                                                                                                               \
                                                                                                                                                                                                     \
			try {                                                                                                                                                                                    \
				TYPE1 ARG1 = CAST1(itArg1->second.c_str());                                                                                                                                          \
				TYPE2 ARG2 = CAST2(itArg2->second.c_str());                                                                                                                                          \
				TYPE3 ARG3 = CAST3(itArg3->second.c_str());                                                                                                                                          \
				TYPE4 ARG4 = CAST4(itArg4->second.c_str());                                                                                                                                          \
				TYPE5 ARG5 = CAST5(itArg5->second.c_str());

#define REST_CALL__5__POSTPROC REST_CALL__1__POSTPROC

#define REST_CALL__8__PREPROC(URI, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8)                                                                                                                                                            \
	if (request._uri == URI) {                                                                                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA1 = request._paramMap.find(#A1);                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA2 = request._paramMap.find(#A2);                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA3 = request._paramMap.find(#A3);                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA4 = request._paramMap.find(#A4);                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA5 = request._paramMap.find(#A5);                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA6 = request._paramMap.find(#A6);                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA7 = request._paramMap.find(#A7);                                                                                                                                                                                                      \
		std::map<std::string, std::string>::const_iterator itA8 = request._paramMap.find(#A8);                                                                                                                                                                                                      \
		if (itA1 == request._paramMap.end() || itA2 == request._paramMap.end() || itA3 == request._paramMap.end() || itA4 == request._paramMap.end() || itA5 == request._paramMap.end() || itA6 == request._paramMap.end() || itA7 == request._paramMap.end() || itA8 == request._paramMap.end()) { \
			/* Missing parameter */                                                                                                                                                                                                                                                                 \
			endPoint.sendError(501, "Invalid request");                                                                                                                                                                                                                                             \
		} else {                                                                                                                                                                                                                                                                                    \
			HTTPServer::Response resp;                                                                                                                                                                                                                                                              \
                                                                                                                                                                                                                                                                                                    \
			try {                                                                                                                                                                                                                                                                                   \
				T1 A1 = C1(itA1->second.c_str());                                                                                                                                                                                                                                                   \
				T2 A2 = C2(itA2->second.c_str());                                                                                                                                                                                                                                                   \
				T3 A3 = C3(itA3->second.c_str());                                                                                                                                                                                                                                                   \
				T4 A4 = C4(itA4->second.c_str());                                                                                                                                                                                                                                                   \
				T5 A5 = C5(itA5->second.c_str());                                                                                                                                                                                                                                                   \
				T6 A6 = C6(itA6->second.c_str());                                                                                                                                                                                                                                                   \
				T7 A7 = C7(itA7->second.c_str());                                                                                                                                                                                                                                                   \
				T8 A8 = C8(itA8->second.c_str());

#define REST_CALL__8__POSTPROC REST_CALL__1__POSTPROC

#define REST_CALL__9__PREPROC(URI, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9)                                                                                                                                                                                   \
	if (request._uri == URI) {                                                                                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA1 = request._paramMap.find(#A1);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA2 = request._paramMap.find(#A2);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA3 = request._paramMap.find(#A3);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA4 = request._paramMap.find(#A4);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA5 = request._paramMap.find(#A5);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA6 = request._paramMap.find(#A6);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA7 = request._paramMap.find(#A7);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA8 = request._paramMap.find(#A8);                                                                                                                                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA9 = request._paramMap.find(#A9);                                                                                                                                                                                                                                         \
		if (itA1 == request._paramMap.end() || itA2 == request._paramMap.end() || itA3 == request._paramMap.end() || itA4 == request._paramMap.end() || itA5 == request._paramMap.end() || itA6 == request._paramMap.end() || itA7 == request._paramMap.end() || itA8 == request._paramMap.end() || itA9 == request._paramMap.end()) { \
			/* Missing parameter */                                                                                                                                                                                                                                                                                                    \
			endPoint.sendError(501, "Invalid request");                                                                                                                                                                                                                                                                                \
		} else {                                                                                                                                                                                                                                                                                                                       \
			HTTPServer::Response resp;                                                                                                                                                                                                                                                                                                 \
                                                                                                                                                                                                                                                                                                                                       \
			try {                                                                                                                                                                                                                                                                                                                      \
				T1 A1 = C1(itA1->second.c_str());                                                                                                                                                                                                                                                                                      \
				T2 A2 = C2(itA2->second.c_str());                                                                                                                                                                                                                                                                                      \
				T3 A3 = C3(itA3->second.c_str());                                                                                                                                                                                                                                                                                      \
				T4 A4 = C4(itA4->second.c_str());                                                                                                                                                                                                                                                                                      \
				T5 A5 = C5(itA5->second.c_str());                                                                                                                                                                                                                                                                                      \
				T6 A6 = C6(itA6->second.c_str());                                                                                                                                                                                                                                                                                      \
				T7 A7 = C7(itA7->second.c_str());                                                                                                                                                                                                                                                                                      \
				T8 A8 = C8(itA8->second.c_str());                                                                                                                                                                                                                                                                                      \
				T9 A9 = C9(itA9->second.c_str());

#define REST_CALL__9__POSTPROC REST_CALL__1__POSTPROC

#define REST_CALL__10__PREPROC(URI, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9, T10, A10, C10)                                                                                                                                                                                                       \
	if (request._uri == URI) {                                                                                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA1 = request._paramMap.find(#A1);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA2 = request._paramMap.find(#A2);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA3 = request._paramMap.find(#A3);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA4 = request._paramMap.find(#A4);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA5 = request._paramMap.find(#A5);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA6 = request._paramMap.find(#A6);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA7 = request._paramMap.find(#A7);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA8 = request._paramMap.find(#A8);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA9 = request._paramMap.find(#A9);                                                                                                                                                                                                                                                                             \
		std::map<std::string, std::string>::const_iterator itA10 = request._paramMap.find(#A10);                                                                                                                                                                                                                                                                           \
		if (itA1 == request._paramMap.end() || itA2 == request._paramMap.end() || itA3 == request._paramMap.end() || itA4 == request._paramMap.end() || itA5 == request._paramMap.end() || itA6 == request._paramMap.end() || itA7 == request._paramMap.end() || itA8 == request._paramMap.end() || itA9 == request._paramMap.end() || itA10 == request._paramMap.end()) { \
			/* Missing parameter */                                                                                                                                                                                                                                                                                                                                        \
			endPoint.sendError(501, "Invalid request");                                                                                                                                                                                                                                                                                                                    \
		} else {                                                                                                                                                                                                                                                                                                                                                           \
			HTTPServer::Response resp;                                                                                                                                                                                                                                                                                                                                     \
                                                                                                                                                                                                                                                                                                                                                                           \
			try {                                                                                                                                                                                                                                                                                                                                                          \
				T1 A1 = C1(itA1->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T2 A2 = C2(itA2->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T3 A3 = C3(itA3->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T4 A4 = C4(itA4->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T5 A5 = C5(itA5->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T6 A6 = C6(itA6->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T7 A7 = C7(itA7->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T8 A8 = C8(itA8->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T9 A9 = C9(itA9->second.c_str());                                                                                                                                                                                                                                                                                                                          \
				T10 A10 = C10(itA10->second.c_str());

#define REST_CALL__10__POSTPROC REST_CALL__1__POSTPROC

#define NOTIFICATION_CALL(MODULE_NAME)                                         \
	REST_CALL__1__PREPROC("/Notify/" MODULE_NAME, FUNC, string, _type, NOCAST) \
	int res = 0;                                                               \
	Notify(_type, request._paramMap);                                          \
	REST_CALL__1__POSTPROC

////////////////////////////////////////////////////////////////////////
// Handle REST call: 0 parameter
#define REST_FUNC__0(FUNC, RET)            \
	REST_CALL__0__PREPROC("/" #FUNC, FUNC) \
	RET res = FUNC();                      \
	REST_CALL__0__POSTPROC

#define REST_FUNC__0CB(FUNC, RET)                                       \
	REST_CALL__0__PREPROC("/" #FUNC, FUNC)                              \
	GET_CALLBACK_PARAMETERS                                             \
	RET res = FUNC(callbackModuleName, callbackData, callbackHostName); \
	REST_CALL__0__POSTPROC

#define REST_PROC__0(FUNC)                 \
	REST_CALL__0__PREPROC("/" #FUNC, FUNC) \
	int res = 0;                           \
	FUNC();                                \
	REST_CALL__0__POSTPROC

#define REST_PROC__0CB(FUNC)                                  \
	REST_CALL__0__PREPROC("/" #FUNC, FUNC)                    \
	GET_CALLBACK_PARAMETERS                                   \
	int res = 0;                                              \
	FUNC(callbackModuleName, callbackData, callbackHostName); \
	REST_CALL__0__POSTPROC

////////////////////////////////////////////////////////////////////////
// Handle REST call: 1 parameter
#define REST_FUNC__1(FUNC, T1, A1, C1, RET)            \
	REST_CALL__1__PREPROC("/" #FUNC, FUNC, T1, A1, C1) \
	RET res = FUNC(A1);                                \
	REST_CALL__1__POSTPROC

#define REST_FUNC__1CB(FUNC, T1, A1, C1, RET)                               \
	REST_CALL__1__PREPROC("/" #FUNC, FUNC, T1, A1, C1)                      \
	GET_CALLBACK_PARAMETERS                                                 \
	RET res = FUNC(A1, callbackModuleName, callbackData, callbackHostName); \
	REST_CALL__1__POSTPROC

#define REST_PROC__1(FUNC, T1, A1, C1)                 \
	REST_CALL__1__PREPROC("/" #FUNC, FUNC, T1, A1, C1) \
	int res = 0;                                       \
	FUNC(A1);                                          \
	REST_CALL__1__POSTPROC

#define REST_PROC__1CB(FUNC, T1, A1, C1)                          \
	REST_CALL__1__PREPROC("/" #FUNC, FUNC, T1, A1, C1)            \
	GET_CALLBACK_PARAMETERS                                       \
	int res = 0;                                                  \
	FUNC(A1, callbackModuleName, callbackData, callbackHostName); \
	REST_CALL__1__POSTPROC

////////////////////////////////////////////////////////////////////////
// Handle REST call: 2 parameters
#define REST_FUNC__2(FUNC, T1, A1, C1, T2, A2, C2, RET)            \
	REST_CALL__2__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2) \
	RET res = FUNC(A1, A2);                                        \
	REST_CALL__2__POSTPROC

#define REST_FUNC__2CB(FUNC, T1, A1, C1, T2, A2, C2, RET)                       \
	REST_CALL__2__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2)              \
	GET_CALLBACK_PARAMETERS                                                     \
	RET res = FUNC(A1, A2, callbackModuleName, callbackData, callbackHostName); \
	REST_CALL__2__POSTPROC

#define REST_PROC__2(FUNC, T1, A1, C1, T2, A2, C2)                 \
	REST_CALL__2__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2) \
	int res = 0;                                                   \
	FUNC(A1, A2);                                                  \
	REST_CALL__2__POSTPROC

#define REST_PROC__2CB(FUNC, T1, A1, C1, T2, A2, C2)                  \
	REST_CALL__2__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2)    \
	GET_CALLBACK_PARAMETERS                                           \
	int res = 0;                                                      \
	FUNC(A1, A2, callbackModuleName, callbackData, callbackHostName); \
	REST_CALL__2__POSTPROC

////////////////////////////////////////////////////////////////////////
// Handle REST call: 3 parameters
#define REST_FUNC__3(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, RET)            \
	REST_CALL__3__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3) \
	RET res = FUNC(A1, A2, A3);                                                \
	REST_CALL__3__POSTPROC

#define REST_FUNC__3CB(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, RET)               \
	REST_CALL__3__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3)      \
	GET_CALLBACK_PARAMETERS                                                         \
	RET res = FUNC(A1, A2, A3, callbackModuleName, callbackData, callbackHostName); \
	REST_CALL__3__POSTPROC

#define REST_PROC__3(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3)                 \
	REST_CALL__3__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3) \
	int res = 0;                                                               \
	FUNC(A1, A2, A3);                                                          \
	REST_CALL__3__POSTPROC

#define REST_PROC__3CB(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3)               \
	REST_CALL__3__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3) \
	GET_CALLBACK_PARAMETERS                                                    \
	int res = 0;                                                               \
	FUNC(A1, A2, A3, callbackModuleName, callbackData, callbackHostName);      \
	REST_CALL__3__POSTPROC

////////////////////////////////////////////////////////////////////////
// Handle REST call: 4 parameters
#define REST_FUNC__4(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, RET)            \
	REST_CALL__4__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4) \
	RET res = FUNC(A1, A2, A3, A4);                                                        \
	REST_CALL__4__POSTPROC

#define REST_FUNC__4CB(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, RET)          \
	REST_CALL__4__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4) \
	GET_CALLBACK_PARAMETERS                                                                \
	RET res = FUNC(A1, A2, A3, A4, callbackModuleName, callbackData, callbackHostName);    \
	REST_CALL__4__POSTPROC

#define REST_PROC__4(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4)                 \
	REST_CALL__4__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4) \
	int res = 0;                                                                           \
	FUNC(A1, A2, A3, A4);                                                                  \
	REST_CALL__4__POSTPROC

#define REST_PROC__4CB(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4)               \
	REST_CALL__4__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4) \
	GET_CALLBACK_PARAMETERS                                                                \
	int res = 0;                                                                           \
	FUNC(A1, A2, A3, A4, callbackModuleName, callbackData, callbackHostName);              \
	REST_CALL__4__POSTPROC

////////////////////////////////////////////////////////////////////////
// Handle REST call: 5 parameters
#define REST_FUNC__5(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, RET)            \
	REST_CALL__5__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5) \
	RET res = FUNC(A1, A2, A3, A4, A5);                                                                \
	REST_CALL__5__POSTPROC

#define REST_FUNC__5CB(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, RET)          \
	REST_CALL__5__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5) \
	GET_CALLBACK_PARAMETERS                                                                            \
	RET res = FUNC(A1, A2, A3, A4, A5, callbackModuleName, callbackData, callbackHostName);            \
	REST_CALL__5__POSTPROC

#define REST_PROC__5(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5)                 \
	REST_CALL__5__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5) \
	int res = 0;                                                                                       \
	FUNC(A1, A2, A3, A4, A5);                                                                          \
	REST_CALL__5__POSTPROC

#define REST_PROC__5CB(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5)               \
	REST_CALL__5__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5) \
	GET_CALLBACK_PARAMETERS                                                                            \
	int res = 0;                                                                                       \
	FUNC(A1, A2, A3, A4, A5, callbackModuleName, callbackData, callbackHostName);                      \
	REST_CALL__5__POSTPROC

////////////////////////////////////////////////////////////////////////
// Extra
#define REST_PROC__8(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8)                 \
	REST_CALL__8__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8) \
	int res = 0;                                                                                                                           \
	FUNC(A1, A2, A3, A4, A5, A6, A7, A8);                                                                                                  \
	REST_CALL__8__POSTPROC

#define REST_FUNC__9(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9, RET)            \
	REST_CALL__9__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9) \
	RET res = FUNC(A1, A2, A3, A4, A5, A6, A7, A8, A9);                                                                                                \
	REST_CALL__9__POSTPROC

#define REST_PROC__9(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9)                 \
	REST_CALL__9__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9) \
	int res = 0;                                                                                                                                       \
	FUNC(A1, A2, A3, A4, A5, A6, A7, A8, A9);                                                                                                          \
	REST_CALL__9__POSTPROC

#define REST_FUNC__10(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9, T10, A10, C10, RET)            \
	REST_CALL__10__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9, T10, A10, C10) \
	RET res = FUNC(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10);                                                                                                           \
	REST_CALL__10__POSTPROC

#define REST_PROC__10(FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9, T10, A10, C10)                 \
	REST_CALL__10__PREPROC("/" #FUNC, FUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9, T10, A10, C10) \
	int res = 0;                                                                                                                                                       \
	FUNC(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10);                                                                                                                     \
	REST_CALL__10__POSTPROC

////////////////////////////////////////////////////////////////////////
// Handle async callback through notification interface
#define BEGIN_REST_CALLBACK_MAP_SHARED(MEMBER_FUNC, INTF)                                                                          \
	void INTF##Plugin::MEMBER_FUNC(const string& type, const std::map<std::string, std::string>& params) {                         \
		LOG4CPLUS_DEBUG(m_logger, #INTF " received notification, type=" << type);                                                  \
		{                                                                                                                          \
			std::map<std::string, std::string>::const_iterator itErr = params.find("_err");                                        \
			if (itErr != params.end()) {                                                                                           \
				std::map<std::string, std::string>::const_iterator itErrMsg = params.find("_errmsg");                              \
				if (itErrMsg != params.end()) {                                                                                    \
					LOG4CPLUS_WARN(m_logger, #INTF " [" << type << "] error: " << itErr->second << ", msg: " << itErrMsg->second); \
				} else {                                                                                                           \
					LOG4CPLUS_WARN(m_logger, #INTF " [" << type << "] error: " << itErr->second);                                  \
				}                                                                                                                  \
			}                                                                                                                      \
		}

#define BEGIN_REST_CALLBACK_MAP(INTF) BEGIN_REST_CALLBACK_MAP_SHARED(Notify, INTF)
#define BEGIN_REST_CALLBACK_MAP_ASYNC(INTF) BEGIN_REST_CALLBACK_MAP_SHARED(_doNotify, INTF)

#define END_REST_CALLBACK_MAP }

#define REST_CALLBACK__0(NOTIFICATION_TYPE, FUNC, ERRFUNC)        \
	if (type == NOTIFICATION_TYPE) {                              \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC); \
		FUNC();                                                   \
		return;                                                   \
	}

#define REST_CALLBACK__1(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1) \
	if (type == NOTIFICATION_TYPE) {                                   \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);      \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);   \
		if (itA1 != params.end()) {                                    \
			T1 A1 = C1(itA1->second.c_str());                          \
			FUNC(A1);                                                  \
		}                                                              \
		return;                                                        \
	}

#define REST_CALLBACK__2(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2) \
	if (type == NOTIFICATION_TYPE) {                                               \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                  \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);\
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);\
		if (itA1 != params.end() && itA2 != params.end()) {                        \
			T1 A1 = C1(itA1->second.c_str());                                      \
			T2 A2 = C2(itA2->second.c_str());                                      \
			FUNC(A1, A2);                                                          \
		}                                                                          \
		return;                                                                    \
	}

#define REST_CALLBACK__3(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3) \
	if (type == NOTIFICATION_TYPE) {                                                           \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                              \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);            \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);            \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);            \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end()) {            \
			T1 A1 = C1(itA1->second.c_str());                                                  \
			T2 A2 = C2(itA2->second.c_str());                                                  \
			T3 A3 = C3(itA3->second.c_str());                                                  \
			FUNC(A1, A2, A3);                                                                  \
		}                                                                                      \
		return;                                                                                \
	}

#define REST_CALLBACK__4(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4)  \
	if (type == NOTIFICATION_TYPE) {                                                                        \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                                           \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);                         \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);                         \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);                         \
		std::map<std::string, std::string>::const_iterator itA4 = params.find(#A4);                         \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end() && itA4 != params.end()) { \
			T1 A1 = C1(itA1->second.c_str());                                                               \
			T2 A2 = C2(itA2->second.c_str());                                                               \
			T3 A3 = C3(itA3->second.c_str());                                                               \
			T4 A4 = C4(itA4->second.c_str());                                                               \
			FUNC(A1, A2, A3, A4);                                                                           \
		}                                                                                                   \
		return;                                                                                             \
	}

#define REST_CALLBACK__5(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5)              \
	if (type == NOTIFICATION_TYPE) {                                                                                                \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                                                                   \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);                                                 \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);                                                 \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);                                                 \
		std::map<std::string, std::string>::const_iterator itA4 = params.find(#A4);                                                 \
		std::map<std::string, std::string>::const_iterator itA5 = params.find(#A5);                                                 \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end() && itA4 != params.end() && itA5 != params.end()) { \
			T1 A1 = C1(itA1->second.c_str());                                                                                       \
			T2 A2 = C2(itA2->second.c_str());                                                                                       \
			T3 A3 = C3(itA3->second.c_str());                                                                                       \
			T4 A4 = C4(itA4->second.c_str());                                                                                       \
			T5 A5 = C5(itA5->second.c_str());                                                                                       \
			FUNC(A1, A2, A3, A4, A5);                                                                                               \
		}                                                                                                                           \
		return;                                                                                                                     \
	}

#define REST_CALLBACK__6(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6)                          \
	if (type == NOTIFICATION_TYPE) {                                                                                                                        \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                                                                                           \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);                                                                         \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);                                                                         \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);                                                                         \
		std::map<std::string, std::string>::const_iterator itA4 = params.find(#A4);                                                                         \
		std::map<std::string, std::string>::const_iterator itA5 = params.find(#A5);                                                                         \
		std::map<std::string, std::string>::const_iterator itA6 = params.find(#A6);                                                                         \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end() && itA4 != params.end() && itA5 != params.end() && itA6 != params.end()) { \
			T1 A1 = C1(itA1->second.c_str());                                                                                                               \
			T2 A2 = C2(itA2->second.c_str());                                                                                                               \
			T3 A3 = C3(itA3->second.c_str());                                                                                                               \
			T4 A4 = C4(itA4->second.c_str());                                                                                                               \
			T5 A5 = C5(itA5->second.c_str());                                                                                                               \
			T6 A6 = C6(itA6->second.c_str());                                                                                                               \
			FUNC(A1, A2, A3, A4, A5, A6);                                                                                                                   \
		}                                                                                                                                                   \
		return;                                                                                                                                             \
	}

#define REST_CALLBACK__7(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7)                                      \
	if (type == NOTIFICATION_TYPE) {                                                                                                                                                \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                                                                                                                   \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA4 = params.find(#A4);                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA5 = params.find(#A5);                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA6 = params.find(#A6);                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA7 = params.find(#A7);                                                                                                 \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end() && itA4 != params.end() && itA5 != params.end() && itA6 != params.end() && itA7 != params.end()) { \
			T1 A1 = C1(itA1->second.c_str());                                                                                                                                       \
			T2 A2 = C2(itA2->second.c_str());                                                                                                                                       \
			T3 A3 = C3(itA3->second.c_str());                                                                                                                                       \
			T4 A4 = C4(itA4->second.c_str());                                                                                                                                       \
			T5 A5 = C5(itA5->second.c_str());                                                                                                                                       \
			T6 A6 = C6(itA6->second.c_str());                                                                                                                                       \
			T7 A7 = C7(itA7->second.c_str());                                                                                                                                       \
			FUNC(A1, A2, A3, A4, A5, A6, A7);                                                                                                                                       \
		}                                                                                                                                                                           \
		return;                                                                                                                                                                     \
	}

#define REST_CALLBACK__8(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8)                                                  \
	if (type == NOTIFICATION_TYPE) {                                                                                                                                                                        \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                                                                                                                                           \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA4 = params.find(#A4);                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA5 = params.find(#A5);                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA6 = params.find(#A6);                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA7 = params.find(#A7);                                                                                                                         \
		std::map<std::string, std::string>::const_iterator itA8 = params.find(#A8);                                                                                                                         \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end() && itA4 != params.end() && itA5 != params.end() && itA6 != params.end() && itA7 != params.end() && itA8 != params.end()) { \
			T1 A1 = C1(itA1->second.c_str());                                                                                                                                                               \
			T2 A2 = C2(itA2->second.c_str());                                                                                                                                                               \
			T3 A3 = C3(itA3->second.c_str());                                                                                                                                                               \
			T4 A4 = C4(itA4->second.c_str());                                                                                                                                                               \
			T5 A5 = C5(itA5->second.c_str());                                                                                                                                                               \
			T6 A6 = C6(itA6->second.c_str());                                                                                                                                                               \
			T7 A7 = C7(itA7->second.c_str());                                                                                                                                                               \
			T8 A8 = C8(itA8->second.c_str());                                                                                                                                                               \
			FUNC(A1, A2, A3, A4, A5, A6, A7, A8);                                                                                                                                                           \
		}                                                                                                                                                                                                   \
		return;                                                                                                                                                                                             \
	}

#define REST_CALLBACK__9(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9)                                                              \
	if (type == NOTIFICATION_TYPE) {                                                                                                                                                                                                \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                                                                                                                                                                   \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA4 = params.find(#A4);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA5 = params.find(#A5);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA6 = params.find(#A6);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA7 = params.find(#A7);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA8 = params.find(#A8);                                                                                                                                                 \
		std::map<std::string, std::string>::const_iterator itA9 = params.find(#A9);                                                                                                                                                 \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end() && itA4 != params.end() && itA5 != params.end() && itA6 != params.end() && itA7 != params.end() && itA8 != params.end() && itA9 != params.end()) { \
			T1 A1 = C1(itA1->second.c_str());                                                                                                                                                                                       \
			T2 A2 = C2(itA2->second.c_str());                                                                                                                                                                                       \
			T3 A3 = C3(itA3->second.c_str());                                                                                                                                                                                       \
			T4 A4 = C4(itA4->second.c_str());                                                                                                                                                                                       \
			T5 A5 = C5(itA5->second.c_str());                                                                                                                                                                                       \
			T6 A6 = C6(itA6->second.c_str());                                                                                                                                                                                       \
			T7 A7 = C7(itA7->second.c_str());                                                                                                                                                                                       \
			T8 A8 = C8(itA8->second.c_str());                                                                                                                                                                                       \
			T9 A9 = C9(itA9->second.c_str());                                                                                                                                                                                       \
			FUNC(A1, A2, A3, A4, A5, A6, A7, A8, A9);                                                                                                                                                                               \
		}                                                                                                                                                                                                                           \
		return;                                                                                                                                                                                                                     \
	}

#define REST_CALLBACK__14(NOTIFICATION_TYPE, FUNC, ERRFUNC, T1, A1, C1, T2, A2, C2, T3, A3, C3, T4, A4, C4, T5, A5, C5, T6, A6, C6, T7, A7, C7, T8, A8, C8, T9, A9, C9, T10, A10, C10, T11, A11, C11, T12, A12, C12, T13, A13, C13, T14, A14, C14)                                                                                                               \
	if (type == NOTIFICATION_TYPE) {                                                                                                                                                                                                                                                                                                                             \
		REST_CALLBACK_ERROR_HANDLING(NOTIFICATION_TYPE, ERRFUNC);                                                                                                                                                                                                                                                                                                \
		std::map<std::string, std::string>::const_iterator itA1 = params.find(#A1);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA2 = params.find(#A2);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA3 = params.find(#A3);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA4 = params.find(#A4);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA5 = params.find(#A5);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA6 = params.find(#A6);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA7 = params.find(#A7);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA8 = params.find(#A8);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA9 = params.find(#A9);                                                                                                                                                                                                                                                                              \
		std::map<std::string, std::string>::const_iterator itA10 = params.find(#A10);                                                                                                                                                                                                                                                                            \
		std::map<std::string, std::string>::const_iterator itA11 = params.find(#A11);                                                                                                                                                                                                                                                                            \
		std::map<std::string, std::string>::const_iterator itA12 = params.find(#A12);                                                                                                                                                                                                                                                                            \
		std::map<std::string, std::string>::const_iterator itA13 = params.find(#A13);                                                                                                                                                                                                                                                                            \
		std::map<std::string, std::string>::const_iterator itA14 = params.find(#A14);                                                                                                                                                                                                                                                                            \
		if (itA1 != params.end() && itA2 != params.end() && itA3 != params.end() && itA4 != params.end() && itA5 != params.end() && itA6 != params.end() && itA7 != params.end() && itA8 != params.end() && itA9 != params.end() && itA10 != params.end() && itA11 != params.end() && itA12 != params.end() && itA13 != params.end() && itA14 != params.end()) { \
			T1 A1 = C1(itA1->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T2 A2 = C2(itA2->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T3 A3 = C3(itA3->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T4 A4 = C4(itA4->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T5 A5 = C5(itA5->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T6 A6 = C6(itA6->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T7 A7 = C7(itA7->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T8 A8 = C8(itA8->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T9 A9 = C9(itA9->second.c_str());                                                                                                                                                                                                                                                                                                                    \
			T10 A10 = C10(itA10->second.c_str());                                                                                                                                                                                                                                                                                                                \
			T11 A11 = C11(itA11->second.c_str());                                                                                                                                                                                                                                                                                                                \
			T12 A12 = C12(itA12->second.c_str());                                                                                                                                                                                                                                                                                                                \
			T13 A13 = C13(itA13->second.c_str());                                                                                                                                                                                                                                                                                                                \
			T14 A14 = C14(itA14->second.c_str());                                                                                                                                                                                                                                                                                                                \
			FUNC(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14);                                                                                                                                                                                                                                                                                   \
		}                                                                                                                                                                                                                                                                                                                                                        \
		return;                                                                                                                                                                                                                                                                                                                                                  \
	}

#define SEND_ERROR_CALLBACK(TASK, ERR_NUM, ERR_MSG, LOGGER)                                               \
	{                                                                                                     \
		LOG4CPLUS_WARN((LOGGER), __FUNCTION__ "(AppID=" << (TASK)->m_appId << ") failed: " << (ERR_MSG)); \
                                                                                                          \
		if (!(TASK)->m_cbMod.empty() && (TASK)->m_cbMod != NULL_MODULE_NAME) {                            \
			std::map<std::string, std::string> params;                                                    \
                                                                                                          \
			std::stringstream ssCBData, ssAppId, ssErr;                                                   \
			ssCBData << (unsigned int)(TASK)->m_cbData;                                                   \
			ssAppId << (TASK)->m_appId;                                                                   \
			ssErr << (ERR_NUM);                                                                           \
                                                                                                          \
			params.insert(make_pair("_module", (TASK)->m_cbMod));                                         \
			params.insert(make_pair("_callbackdata", ssCBData.str()));                                    \
			params.insert(make_pair("_type", (TASK)->m_taskType));                                        \
			params.insert(make_pair("_err", ssErr.str()));                                                \
			params.insert(make_pair("_errmsg", (ERR_MSG)));                                               \
			params.insert(make_pair("appid", ssAppId.str()));                                             \
                                                                                                          \
			pNotifier->Notify((TASK)->m_cbMod, (TASK)->m_taskType, params);                               \
		}                                                                                                 \
	}
