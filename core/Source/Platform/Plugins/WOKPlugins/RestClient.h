///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/URL.h"
#include "Incubator.h"

namespace KEP {

CHAINED(HTTPException); // if needs be, move this to a more central location, e.g. http.h

class RestResultParser;

class IRESTClient {
public:
	/**
	 * Synchronous
	 * @param remoteHost	the name of the remote host
	 * @param uri			the uri to be requested from host
	 * @param params		String map containing parameters to be applied to the url
	 * @param parser		Reference to a provided parser that will be used to marshall the response to this call
	 *
	 * @returns 0 on success, else APIN_REST_CALL_FAILED
	 */
	virtual int Call(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, RestResultParser* parser = NULL) = 0;

	/**
	 * Synchronous call
	 * @param url			the url to call
	 * @param parser		Reference to a provided parser that will be used to marshall the response to this call
	 * @throws				HTTPException
	 * @returns 0 on success else HTTPException
	 */
	virtual int Call(const KEP::URL& url, RestResultParser* parser /*= NULL*/) = 0;

	//Asynchronous
	virtual void Invoke(const std::string& remoteHost, const std::string& uri, const std::map<std::string, std::string>& params, unsigned int ttl = INFINITE) = 0;
};

AbstractPluginFactory* createPluginFactory(IRESTClient* pFactoryTypeHint);

} // namespace KEP