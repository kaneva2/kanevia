///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "PluginHostApplication.h"

int main(int argc, char** argv) {
	RunPluginHostApplication(argc, argv, "PluginHostApp", "PluginHost Application");
}
