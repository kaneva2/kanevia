///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "common/include/KEPCommon.h"
#include "Plugins/App/DDRModelFacet.h"
#include "Event/DDR/DDR_S_GameData.h"
#include "mysql++/mysql++.h"
#include "Server/Database/MySqlUtil.h"
#include "core/MySqlPLib/MySqlConnector.h"
#include "Plugins/App/ModelHelper.h"
#include "Event\Events\DDRDBEvent.h"
#include "Event\Events\DDREvent.h"

using namespace mysqlpp;
using namespace log4cplus;

namespace KEP {

sql_create_17(ddr_game_data,
	1, 17,
	ULONG, seed,
	ULONG, num_seconds_to_play,
	ULONG, num_moves,
	ULONG, num_misses,
	ULONG, num_sub_levels,
	ULONG, level_id,
	ULONG, perfect_score,
	ULONG, great_score,
	ULONG, good_score,
	ULONG, finish_bonus_score,
	ULONG, level_increase_score,
	ULONG, level_bonus_trigger_num,
	ULONG, level_bonus_score,
	ULONG, perfect_score_ceiling,
	ULONG, great_score_ceiling,
	ULONG, good_score_ceiling,
	ULONG, last_level_reduction_ms);

sql_create_4(ddr_animations,
	1, 4,
	std::string, type,
	ULONG, level_id,
	ULONG, animation_id,
	int, play_order);

sql_create_6(dance_level_actions,
	1, 6,
	ULONG, dance_level,
	ULONG, skill_id,
	ULONG, action_id,
	ULONG, perfect_action_id,
	ULONG, gain_amt,
	ULONG, perfect_gain_amt);

// Impl definition
//
bool DDRModelFacet::loadDDRPlayerData(const DDRDBEvent* ddre, DDREvent* replyEvent) {
	Logger alert_logger(_logger);

	try {
		KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
		KGP_Connection& conn = *p;
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		Query query = conn.query();
		BOOL unusedBool = FALSE;

		// Determine if the dynamic object is in a permanent zone. If so, score will be tracked
		// by zoneIndex and objectId so that multiple instances of a dynamic object can have the
		// same high score. Otherwise, all instances are unique.
		//
		// Note this only has meaning in wok.  Everything else is hosted and unique to the app.
		//
		query << "CALL hosted_apps.get_ddr_player_data( %0, %1, %2, %3, %4, %5 )";
		query.parse();

		LOG4CPLUS_TRACE(_logger, query.str((int)ddre->m_gameId, ddre->m_playerId, ddre->m_dynObjectId, (int)ddre->m_zoneIndex, (int)ddre->m_objectId, false));

		// Note that this has the potential to raise exception 1264 from the database (out of range) as the C32 int is far larger than MySqls definition of int.
		//
		StoreQueryResult results = query.store((int)ddre->m_gameId, ddre->m_playerId, ddre->m_dynObjectId, (int)ddre->m_zoneIndex, (int)ddre->m_objectId, unusedBool);

		if (results.size() != 1)
			return false;

		Row r = results.at(0);
		long rc = r.at(0);
		if (rc != 0) { // !ok
			LOG4CPLUS_ERROR(_logger, "Object not found for " << query.str(ddre->m_gameId, ddre->m_dynObjectId, ddre->m_playerId, ddre->m_zoneIndex, unusedBool));
			return false;
		}

		replyEvent->PlayerData(ddre->From(), ddre->m_gameId, ddre->m_playerId, ddre->m_gameLevel, ddre->m_newGame, ddre->m_dynObjectId,
			r.at(1), r.at(2), r.at(3));

		// Not sure why there would be multiple results, or why it matters.  The wantsHighScore flag
		// is always false in this method. checkMultipleResultSets does nothing with them
		// anyway.  Is this some requirement by mysql++?  Comment for checkMultipleResults says
		// required for sp calls. odd that.
		//
		checkMultipleResultSets(query, "get_ddr_player_data");
		//	    LOG4CPLUS_DEBUG(    _logger
		//                            , "Loaded player data for game id:"     << ddre->m_gameId
		//                                << " player id:"                    << ddre->m_playerId
		//                                << " dynamic object id:"            << ddre->m_dynObjectId
		//                                << " Zone:" << ddre->m_dynObjectId  << ":" << ddre->m_zoneIndex);
		return true;
	}
	STD_DB_EXCEPTION_HANDLING
	catch (const IMySqlConnector::Exception& e) {
		DataModel::Exception dme(SOURCELOCATION, "Connection error!");
		dme.chain(e);
		throw dme;
	}

	return false;
}

// remap game_id to ddr_id
bool DDRModelFacet::recordDDRScore(bool isPermanentZone, ULONG objectId, ULONG zoneIndex, int dynObjectId, ULONG ddrId, int playerId, const std::string& playerName, ULONG /*highScore*/
	,
	ULONG score, ULONG numPerfects, ULONG numPerfectsInARow) {
	Logger alert_logger(_logger);
	bool ret = false;

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;
	KGP_Connection* m_conn = &conn;
	try {
		Transaction trans(*m_conn);

		Query query = m_conn->query();

		if (isPermanentZone) {
			query << "INSERT INTO hosted_apps.ddr_scores ( game_id, ddr_id, player_id, dynamic_object_id, score, num_perfects, num_perfects_inarow, player_name, created_date, object_id, zone_index ) VALUES (%0,%1,%2,%3,%4,%5,%6,%7q,NOW(),%8,%9)";
			query.parse();

			LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), (int)ddrId, (int)playerId, (int)dynObjectId, (int)score, (int)numPerfects, (int)numPerfectsInARow, playerName.c_str(), (int)objectId, (int)zoneIndex));
			SimpleResult res = query.execute((int)hostModel().gameId(), (int)ddrId, (int)playerId, (int)dynObjectId, (int)score, (int)numPerfects, (int)numPerfectsInARow, playerName.c_str(), (int)objectId, (int)zoneIndex);

			/*ULONG instanceId =*/(ULONG) res.insert_id();
		} else {
			query << "INSERT INTO hosted_apps.ddr_scores ( game_id, ddr_id,player_id, dynamic_object_id, score, num_perfects, num_perfects_inarow, player_name, created_date ) VALUES (%0,%1,%2,%3,%4,%5,%6,%7q,NOW())";
			query.parse();

			LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), (int)ddrId, (int)playerId, (int)dynObjectId, (int)score, (int)numPerfects, (int)numPerfectsInARow, playerName.c_str()));
			SimpleResult res = query.execute((int)hostModel().gameId(), (int)ddrId, (int)playerId, (int)dynObjectId, (int)score, (int)numPerfects, (int)numPerfectsInARow, playerName.c_str());

			/*ULONG instanceId =*/(ULONG) res.insert_id();
		}

		trans.commit();

		ret = true;
	}
	STD_DB_EXCEPTION_HANDLING

	return ret;
}

bool DDRModelFacet::recordDDRHighScore(int dynObjectId, int playerId, ULONG highScore, const std::string& playerName, bool isPermanentZone, ULONG objectId, ULONG zoneIndex) {
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;

	try {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		StoreQueryResult results;
		Transaction trans(conn);
		Query query = conn.query();

		// Retrieve previous high score if there is one.  Note that if the zone
		// is permanent, we are looking for the high score for the specified
		// zone id and object id
		//
		if (isPermanentZone) {
			query << "SELECT high_score, player_id, object_id, zone_index "
				  << "FROM hosted_apps.ddr_high_scores "
				  << "WHERE game_id=%0 and object_id=%1 AND zone_index=%2";
			query.parse();
			LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), (int)objectId, (int)zoneIndex));
			results = query.store((int)hostModel().gameId(), (int)objectId, (int)zoneIndex);
		} else {
			// Else we are looking for the high score for the specified
			// Dynamic object id.
			//
			query << "SELECT high_score, player_id, dynamic_object_id"
				  << " FROM hosted_apps.ddr_high_scores "
				  << " WHERE game_id=%0 and dynamic_object_id = %1";
			query.parse();
			LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), dynObjectId));
			results = query.store((int)hostModel().gameId(), dynObjectId);
		}

		if (results.size() <= 0) {
			// We haven't recorded a high score for this ddr instance before
			// Insert it now
			//
			query.reset();
			// SimpleResult res; // Compiler bug: won't automatically generate assignment operator and tries to link it in.
			if (isPermanentZone) {
				query << "INSERT INTO hosted_apps.ddr_high_scores (game_id,dynamic_object_id, player_id,high_score,player_name,created_date,object_id,zone_index) VALUES ( %0,%1,%2,%3,%4q,NOW(),%5,%6 )";
				query.parse();
				LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), dynObjectId, playerId, (int)highScore, playerName, (int)objectId, (int)zoneIndex));
				SimpleResult res = query.execute((int)hostModel().gameId(), dynObjectId, playerId, (int)highScore, playerName.c_str(), (int)objectId, (int)zoneIndex);
				/*ULONG instanceId =*/(int)res.insert_id();
			} else {
				query << "INSERT INTO hosted_apps.ddr_high_scores (game_id,dynamic_object_id, player_id, high_score, player_name, created_date ) VALUES (%0,%1,%2,%3,%4q, NOW() )";
				query.parse();
				LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), dynObjectId, playerId, (int)highScore, playerName));
				SimpleResult res = query.execute((int)hostModel().gameId(), dynObjectId, playerId, (int)highScore, playerName.c_str());
				/*ULONG instanceId =*/(int)res.insert_id();
			}
			trans.commit();
			return true;
		}

		// Okay, we have the currently recorded high score for this zone...
		//
		// Guard.  We should only have a retrieved a single record.  If we didn't something is wrong. Bug out
		//
		if (results.size() != 1) {
			std::stringstream ss;
			if (isPermanentZone)
				ss << "Result != 1 for -> SELECT high_score FROM hosted_apps.ddr_high_scores WHERE object_id = '" << objectId << "' AND zone_index = '" << zoneIndex << "'";
			else
				ss << "Result != 1 for -> SELECT high_score FROM hosted_apps.ddr_high_scores WHERE dynamic_object_id = '" << dynObjectId << "'";
			throw ModelException(SOURCELOCATION, ss.str());
		}

		// Extract results of our query for the current high score.
		//
		ULONG dbHighScore = results.at(0).at(0);
		ULONG dbPlayerId = results.at(0).at(1);
		ULONG dbObjectId = 0;
		ULONG dbZoneIndex = 0;
		ULONG dbDynamicObjectId = 0;

		if (isPermanentZone) {
			dbObjectId = results.at(0).at(2);
			dbZoneIndex = results.at(0).at(3);
		} else {
			dbDynamicObjectId = results.at(0).at(2);
		}

		// Evaluate these values to determine if we should attempt to update the
		// high score.  E.g. if the high score that we are attempting to record
		// a score that isn't the highest, don't update.
		//
		if (dbHighScore > highScore)
			return true; // May have been updated by another server after

		if (isPermanentZone && dbPlayerId == (ULONG)playerId && dbHighScore == highScore && dbObjectId == objectId && dbZoneIndex == zoneIndex)
			return true; // Already has high score

		if (!isPermanentZone && dbPlayerId == (ULONG)playerId && dbHighScore == highScore && dbDynamicObjectId == (ULONG)dynObjectId)
			return true; // Already has high score // int -> ULONG ?

		// Okay, update the high score.
		query.reset();

		if (isPermanentZone) {
			query << "UPDATE hosted_apps.ddr_high_scores SET player_id = %0, high_score = %1, player_name = %2q, created_date = NOW() "
				  << "WHERE game_id=%3 and object_id=%4 AND zone_index=%5";
			query.parse();
			LOG4CPLUS_TRACE(_logger, query.str(playerId, (int)highScore, playerName.c_str(), (int)hostModel().gameId(), (int)objectId, (int)zoneIndex));
			query.execute(playerId, (int)highScore, playerName.c_str(), (int)hostModel().gameId(), (int)objectId, (int)zoneIndex);
		} else {
			query << "UPDATE hosted_apps.ddr_high_scores SET player_id=%0,high_score=%1,player_name=%2q,created_date=NOW() "
				  << "WHERE game_id=%3 and dynamic_object_id=%4";
			query.parse();
			LOG4CPLUS_TRACE(_logger, query.str(playerId, (int)highScore, playerName, (int)hostModel().gameId(), dynObjectId));
			query.execute(playerId, (int)highScore, playerName, (int)hostModel().gameId(), dynObjectId);
		}
		trans.commit();
	}
	STD_DB_EXCEPTION_HANDLING
	return true;
}

bool DDRModelFacet::refreshDDRHighScore(int dynObjectId, DDRPlayers* players) {
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;

	try {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		Query query = conn.query();
		StoreQueryResult results;

		LOG4CPLUS_DEBUG(_logger, "Attempting refresh of players high score data for dynamic object id:"
									 << dynObjectId);

		if (players->IsPermanentZone()) {
			query << "SELECT high_score, player_id, player_name "
				  << "FROM hosted_apps.ddr_high_scores  "
				  << "WHERE game_id=%0 AND object_id=%1 AND zone_index=%2";
			query.parse();

			LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), (int)players->GetDynamicObjectObjectId(), (int)players->GetDynamicObjectZoneIndex()));

			results = query.store((int)hostModel().gameId(), (int)players->GetDynamicObjectObjectId(), (int)players->GetDynamicObjectZoneIndex());
		} else {
			query << "SELECT high_score, player_id, player_name"
				  << " FROM hosted_apps.ddr_high_scores  "
				  << " WHERE game_id=%0 AND dynamic_object_id = %1";
			query.parse();
			LOG4CPLUS_TRACE(_logger, query.str((int)hostModel().gameId(), dynObjectId));
			results = query.store((int)hostModel().gameId(), dynObjectId);
		}

		if (results.size() == 1) {
			ULONG high_score = results.at(0).at(0);
			int player_id = results.at(0).at(1);
			std::string playerName = results.at(0).at(2);

			// If db high score greater than our in memory high score refresh
			if (players->GetHighScore() < high_score) {
				players->SetHighScore(high_score);
				players->SetHighScorePlayerId(player_id);
				players->SetHighScoreCharacterName(playerName);
				LOG4CPLUS_DEBUG(_logger,
					"Refresh players high score data for player id:" << player_id
																	 << " dynamic object id:" << dynObjectId
																	 << " high score:" << high_score
																	 << ":" << playerName.c_str());
				return true;
			}
		}
	}
	STD_DB_EXCEPTION_HANDLING

	return false;
}
bool DDRModelFacet::loadDDRGameData(ULONG ddrId, GameLevelsMap& gameData) {
	Logger alert_logger(_logger);
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;
	KGP_Connection* m_conn = &conn;

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	try {
		Query query = m_conn->query();

		query << "SELECT max_players, notify_inactive, purge_timeout"
			  << " FROM hosted_apps.ddr_globals  "
			  << " WHERE ddr_id = %0";

		// It would be nice to put this in the executor logic,  however, there isn't a good way to get the string
		// from the query without supplying params, and we don't want the string to reflect the params
		// since there could be sensitive data in the params e.g. password.
		//
		TRACE_QUERY(query, _logger);

		query.parse();
		StoreQueryResult results = ModelHelper::storeResults(query, SQLQueryParms() << (sql_mediumint_unsigned)ddrId);

		if (results.size() != 1)
			return false;

		ULONG max_players = results.at(0).at(0);
		ULONG notify_timeout = results.at(0).at(1);
		ULONG purge_timeout = results.at(0).at(2);

		query.reset();

		std::stringstream ss;
		ss << "SELECT seed, num_seconds_to_play, num_moves, num_misses,"
		   << " num_sub_levels, level_id, perfect_score, great_score, good_score, finish_bonus_score, level_increase_score,"
		   << " level_bonus_trigger_num, level_bonus_score, perfect_score_ceiling, great_score_ceiling, good_score_ceiling, "
		   << " last_level_reduction_ms "
		   << " FROM hosted_apps.ddr  "
		   << " WHERE ddr_id = %0 ORDER BY level_id asc";
		LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), ss.str());
		query << ss.str().c_str();
		query.parse();

		std::vector<ddr_game_data> gameDataResult;
#ifdef FIXED_MYSQLPP_STOREIN_WITH_SQLQUERYPARMS_BUG
		//LOG4CPLUS_TRACE( m_logger, query.str((int)ddrId) );

		SQLQueryParms parms;
		parms << (int)ddrId;
		try {
			//    query.storein(gameDataResult, parms );
			query.storein_sequence<std::vector<ddr_game_data>>(gameDataResult, parms);
		} catch (const mysqlpp::BadQuery& er) {
			LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), er.query_string());
		}

//    storeResults( query, parms, gameDataResult );
#endif
		query.storein(gameDataResult, (int)ddrId);

		if (gameDataResult.empty())
			return false;

		query.reset();

		query
			<< "SELECT type_id, level_id, animation_id, play_order "
			<< "FROM hosted_apps.ddr_animations "
			<< "WHERE ddr_id = %0 ORDER BY type_id, level_id, play_order asc";

		TRACE_QUERY(query, _logger);

		query.parse();

		std::vector<ddr_animations> animationResult;
		animationResult.clear();

		query.storein(animationResult, (int)ddrId);

		query.reset();
		query << "SELECT dance_level, skill_id, action_id, perfect_action_id, gain_amt, perfect_gain_amt "
			  << "FROM hosted_apps.dance_level_actions_view";
		TRACE_QUERY(query, _logger);
		query.parse();

		std::vector<dance_level_actions> actionsResult;
		actionsResult.clear();
		query.storein(actionsResult);

		DDRLevels* levels = new DDRLevels();

		for (std::vector<ddr_game_data>::iterator ii = gameDataResult.begin(); ii != gameDataResult.end(); ii++) {
			DDRLevelObj* l_level = new DDRLevelObj();

			l_level->SetSeed(ii->seed);
			l_level->SetNumSecondsToPlay(ii->num_seconds_to_play);
			l_level->SetNumMoves(ii->num_moves);
			l_level->SetNumMisses(ii->num_misses);
			l_level->SetNumSubLevels(ii->num_sub_levels);
			l_level->SetLastLevelReductionMilliSeconds(ii->last_level_reduction_ms);

			l_level->SetPerfectScore(ii->perfect_score);
			l_level->SetGreatScore(ii->great_score);
			l_level->SetGoodScore(ii->good_score);
			l_level->SetFinishBonusScore(ii->finish_bonus_score);
			l_level->SetLevelIncreaseScore(ii->level_increase_score);
			l_level->SetLevelBonusTriggerNum(ii->level_bonus_trigger_num);
			l_level->SetLevelBonusScore(ii->level_bonus_score);
			l_level->SetPerfectScoreCeiling(ii->perfect_score_ceiling);
			l_level->SetGreatScoreCeiling(ii->great_score_ceiling);
			l_level->SetGoodScoreCeiling(ii->good_score_ceiling);

			for (std::vector<ddr_animations>::iterator jj = animationResult.begin(); jj != animationResult.end(); jj++) {
				if (ii->level_id != jj->level_id)
					continue;
				if (jj->type == "FLARE") {
					l_level->m_AnimationFlares.push_back(std::make_pair(jj->animation_id, jj->play_order));
					continue;
				}
				if (jj->type == "FINAL_FLARE") {
					l_level->m_AnimationFinalFlares.push_back(std::make_pair(jj->animation_id, jj->play_order));
					continue;
				}
				if (jj->type == "MISS") {
					l_level->m_AnimationMisses.push_back(std::make_pair(jj->animation_id, jj->play_order));
					continue;
				}
				if (jj->type == "LEVELUP") {
					l_level->m_AnimationLevelUps.push_back(std::make_pair(jj->animation_id, jj->play_order));
					continue;
				}
				if (jj->type == "PERFECT") {
					l_level->m_AnimationPerfects.push_back(std::make_pair(jj->animation_id, jj->play_order));
					continue;
				}
				if (jj->type == "IDLE") {
					l_level->m_AnimationIdles.push_back(std::make_pair(jj->animation_id, jj->play_order));
					continue; // superfluous but stabilizes future types.
				}
			}

			for (std::vector<dance_level_actions>::iterator kk = actionsResult.begin(); kk != actionsResult.end(); kk++) {
				if (ii->level_id != kk->dance_level)
					continue;
				l_level->SetSkillId(kk->skill_id);
				l_level->SetActionId(kk->action_id);
				l_level->SetGainAmount(kk->gain_amt);
				l_level->SetPerfectActionId(kk->perfect_action_id);
				l_level->SetPerfectGainAmount(kk->perfect_gain_amt);
			}

			l_level->SetAnimationFlareCount(l_level->m_AnimationFlares.size());
			l_level->SetAnimationFinalFlareCount(l_level->m_AnimationFinalFlares.size());
			l_level->SetAnimationMissCount(l_level->m_AnimationMisses.size());
			l_level->SetAnimationLevelUpCount(l_level->m_AnimationLevelUps.size());
			l_level->SetAnimationPerfectCount(l_level->m_AnimationPerfects.size());
			l_level->SetAnimationIdleCount(l_level->m_AnimationIdles.size());

			levels->m_levelsMap.insert(LevelMap::value_type(ii->level_id, l_level));
		}

		levels->SetLevelCount(levels->m_levelsMap.size());
		levels->SetDanceFloorLimit(max_players);
		levels->SetNotifyInactivePlayerTimeout(notify_timeout);
		levels->SetPurgeTimeout(purge_timeout);

		gameData.insert(GameLevelsMap::value_type(ddrId, levels));

		return true;
	}
	STD_DB_EXCEPTION_HANDLING
	return false;
}

// Facet impl
DDRModelFacet::DDRModelFacet() :
		ModelFacet("DDRModel"), _logger(Logger::getInstance(L"DDRModel")) {}

DDRModelFacet::DDRModelFacet(const DDRModelFacet& other) :
		ModelFacet(other), _logger(Logger::getInstance(L"DDRModel")) {} // inherently dangerous

DDRModelFacet::~DDRModelFacet() {
}
DDRModelFacet& DDRModelFacet::operator=(const DDRModelFacet& rhs) {
	ModelFacet::operator=(rhs);
	return *this;
}

// Suppression Wrapper implementation
// Suppression Wrapper implementation
// Suppression Wrapper implementation
// Suppression Wrapper implementation

SuppressedDDRModel::SuppressedDDRModel(DDRModel& impl) :
		_impl(impl) {}
SuppressedDDRModel::~SuppressedDDRModel() {}
SuppressedDDRModel& SuppressedDDRModel::operator=(const SuppressedDDRModel& /* other */) {
	return *this;
}

SuppressedDDRModel& setConnector(IMySqlConnector* /*connector*/) {
	throw UnsupportedException(SOURCELOCATION);
}
SuppressedDDRModel& setConnectorName(const std::string& /*connectorName*/) {
	throw UnsupportedException(SOURCELOCATION);
}

bool SuppressedDDRModel::loadDDRPlayerData(const DDRDBEvent* ddre, DDREvent* replyEvent) {
	try {
		return _impl.loadDDRPlayerData(ddre, replyEvent);
	} catch (const ChainedException& exc) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"AppDataModel"), exc.str());
	}
	return false;
}

bool SuppressedDDRModel::recordDDRScore(bool isPermanentZone, ULONG objectId, ULONG zoneIndex, int dynObjectId, ULONG ddrId, int playerId, const std::string& playerName, ULONG highScore, ULONG score, ULONG numPerfects, ULONG numPerfectsInARow) {
	try {
		return _impl.recordDDRScore(isPermanentZone, objectId, zoneIndex, dynObjectId, ddrId, playerId, playerName, highScore, score, numPerfects, numPerfectsInARow);
	} catch (const ChainedException& exc) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"AppDataModel"), exc.str());
	}
	return false;
}

bool SuppressedDDRModel::recordDDRHighScore(int dynObjectId, int playerId, ULONG highScore, const std::string& playerName, bool isPermanentZone, ULONG objectId, ULONG zoneIndex) {
	try {
		return _impl.recordDDRHighScore(dynObjectId, playerId, highScore, playerName, isPermanentZone, objectId, zoneIndex);
	} catch (const ChainedException& exc) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"AppDataModel"), exc.str());
	}
	return false;
}

bool SuppressedDDRModel::refreshDDRHighScore(int dynObjectId, DDRPlayers* players) {
	try {
		return _impl.refreshDDRHighScore(dynObjectId, players);
	} catch (const ChainedException& exc) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"AppDataModel"), exc.str());
	}
	return false;
}

bool SuppressedDDRModel::loadDDRGameData(ULONG ddrId, GameLevelsMap& gameData) {
	try {
		return _impl.loadDDRGameData(ddrId, gameData);
	} catch (const ChainedException& exc) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"AppDataModel"), exc.str());
	}
	return false;
}

} // namespace KEP