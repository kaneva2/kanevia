///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "mysql++\mysql++.h"
#include "Server/Database/MySqlUtil.h"
#include "core/MySqlPLib/MySqlConnector.h"

namespace KEP {

// todo: Tuck this in somewhere.  For now leave it as a free function
//
class ModelHelper {
public:
	static mysqlpp::StoreQueryResult storeResults(mysqlpp::Query& query, mysqlpp::SQLQueryParms& params);

	template <typename ssqlT>
	void storeResults(mysqlpp::Query& query, mysqlpp::SQLQueryParms& parms, std::vector<ssqlT>& target) {
		try {
			LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), query.str(parms).c_str());
			query.storein(target, parms);
		} catch (const mysqlpp::BadQuery& er) {
			LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), er.query_string());
			//        ss << "Database query error: " << er.what() <<, %s SQL: %s Connection: %s:%d %s\\%s"
			//                            , er.what()
			//                            , er.query_string()
			//                            , m_conn->server()
			//                            , m_conn->port()
			//                            , m_conn->db()
			//                            , m_conn->user() );
			//            string s = buffer;
			//            ErrorSpec spec(er.errnum(),s );
			std::stringstream ss;
			ss << er.what() << " while executing statement[" << er.query_string() << "]";
			throw ModelException(SOURCELOCATION, ErrorSpec(er.errnum(), ss.str()));
		} catch (const mysqlpp::BadConversion& er) {
			throw ModelException(SOURCELOCATION, ErrorSpec(DWORD(-1), er.what()));
		} catch (const mysqlpp::Exception& er) {
			throw ModelException(SOURCELOCATION, ErrorSpec(DWORD(-1), er.what()));
		}
	}
};

#define STD_DB_EXCEPTION_HANDLING                                                \
	catch (const mysqlpp::BadQuery& er) {                                        \
		throw ModelException(SOURCELOCATION, ErrorSpec(er.errnum(), er.what())); \
	}                                                                            \
	catch (const mysqlpp::BadConversion& er) {                                   \
		throw ModelException(SOURCELOCATION, ErrorSpec(DWORD(-1), er.what()));   \
	}                                                                            \
	catch (const mysqlpp::Exception& er) {                                       \
		throw ModelException(SOURCELOCATION, ErrorSpec(DWORD(-1), er.what()));   \
	}
// END DB LAYER

} // namespace KEP