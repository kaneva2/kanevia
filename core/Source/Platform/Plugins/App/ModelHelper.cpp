///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Plugins\App\ModelHelper.h"
#include "Plugins\App\AppDataModel.h"

using namespace mysqlpp;

namespace KEP {

/**
 * Execute the pre-parsed query using the supplied query params
 * @param The query to be executed.
 * @param The parameters to be used to be used during execution.
 * @returns StoreQueryResult
 * @throws ModelException
 */
StoreQueryResult ModelHelper::storeResults(Query& query, SQLQueryParms& params) {
	try {
		//  LOG4CPLUS_TRACE( Logger::getInstance( L"AppDataModel" ), query.str().c_str() );
		return query.store(params);
	} catch (const mysqlpp::BadQuery& er) {
		//        ss << "Database query error: " << er.what() <<, %s SQL: %s Connection: %s:%d %s\\%s"
		//                            , er.what()
		//                            , er.query_string()
		//                            , m_conn->server()
		//                            , m_conn->port()
		//                            , m_conn->db()
		//                            , m_conn->user() );
		//            string s = buffer;
		//            ErrorSpec spec(er.errnum(),s );
		std::stringstream ss;
		ss << er.what() << " while executing statement[" << er.query_string() << "]";
		throw ModelException(SOURCELOCATION, ErrorSpec(er.errnum(), ss.str()));
	} catch (const mysqlpp::BadConversion& er) {
		throw ModelException(SOURCELOCATION, ErrorSpec(DWORD(-1), er.what()));
	} catch (const mysqlpp::Exception& er) {
		throw ModelException(SOURCELOCATION, ErrorSpec(DWORD(-1), er.what()));
	}
}

} // namespace KEP