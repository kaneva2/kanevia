///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Plugins/App/AppDataModel.h"
#include "Plugins/App/ModelObjects.h"

namespace KEP {

// Creating a new facade
// 1) Define the facades interface.  Note that you should implement
//      default/copy constructor and operator=()
//
// 2) Define the Facet that implements our model interface.  Should derive
//      from the model defined in step 1 and the ModelFacet naming interface
//
// 3) Optionally, implement a Suppressor.
//
class PlayerModel {
public:
	PlayerModel() {}
	PlayerModel(const PlayerModel&) {}
	virtual ~PlayerModel() {}
	PlayerModel& operator=(const PlayerModel&) { return *this; }
	virtual PlayerModel& updateSkills(int playerId, KEPModel::SkillVector& skills) = 0;
};

class PlayerModelFacet : public ModelFacet, public PlayerModel {
public:
	PlayerModelFacet() :
			_logger(log4cplus::Logger::getInstance(L"PlayerModel")), ModelFacet("PlayerModel"), PlayerModel() {}

	PlayerModelFacet(const PlayerModelFacet& other) :
			_logger(other._logger), ModelFacet(other), PlayerModel(other) {}

	virtual ~PlayerModelFacet() {}

	PlayerModelFacet& operator=(const PlayerModelFacet& rhs) {
		PlayerModel::operator=(rhs);
		ModelFacet::operator=(rhs);
		_logger = rhs._logger;
		return (*this);
	}

	virtual PlayerModelFacet& updateSkills(int playerId, KEPModel::SkillVector& skills);

private:
	mutable log4cplus::Logger _logger;
};

} // namespace KEP