///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

/**
 * This header file defines plain object versions of any highly coupled classes used at the application
 * level.  For example CSkillObject is coupled with MFC.  So, in order for the DataModel to use
 * these objects (see DataModel::updateSkills()), the model must coupled with MFC as well.  
 *
 * So candidates for this collection are highly coupled application level objects that we want
 * to decouple.
 *
 * Each of the classes that are implemented should provide an adapter/conversion path.
 * Each class defined here is essentially a Plain Old Cpp Object (POCO).  This language is 
 * borrowed from* the Java "Plain Old Java Object"POJO paradigm
 */
#pragma once

#include "AppPluginLibrary.h"
#include <vector>

// remap for:
//   class  CSkillObject : public CObject ADMIN_BASE
namespace KEPModel {

class APPPLUGINLIBRARY_API Skill {
public:
	Skill(int skillType, int currentLevel, float currentXP) :
			_currentXP(currentXP), _currentLevel(currentLevel), _skillType(skillType) {}

	virtual ~Skill() {}

	Skill(const Skill& other) :
			_skillType(other._skillType), _currentLevel(other._currentLevel), _currentXP(other._currentXP) {}

	Skill& operator=(const Skill& rhs) {
		_skillType = rhs._skillType;
		_currentLevel = rhs._currentLevel;
		_currentXP = rhs._currentXP;
		return *this;
	}

	float currentExperience() const { return _currentXP; }
	int type() const { return _skillType; }
	int currentLevel() const { return _currentLevel; }

private:
	float _currentXP; //depends
	int _currentLevel; //depends
	int _skillType; //individual number like an id
};
typedef std::vector<Skill> SkillVector;
} // namespace KEPModel
