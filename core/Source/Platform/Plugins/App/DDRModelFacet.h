///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Plugins\App\AppDataModel.h"

namespace KEP {

class DDRDBEvent;

class APPPLUGINLIBRARY_API DDRModel {
public:
	DDRModel() {}
	DDRModel(const DDRModel& /* other */) {}
	virtual ~DDRModel() {}
	virtual DDRModel& operator=(const DDRModel& /* rhs */) {
		return *this;
	}

	virtual bool loadDDRPlayerData(const KEP::DDRDBEvent* ddre, KEP::DDREvent* replyEvent) = 0;

	virtual bool recordDDRScore(bool isPermanentZone, ULONG objectId, ULONG zoneIndex, int dynObjectId, ULONG ddrId, int playerId, const std::string& playerName, ULONG highScore, ULONG score, ULONG numPerfects, ULONG numPerfectsInARow) = 0;

	virtual bool recordDDRHighScore(int dynObjectId, int playerId, ULONG highScore, const std::string& playerName, bool isPermanentZone, ULONG objectId, ULONG zoneIndex) = 0;

	virtual bool refreshDDRHighScore(int dynObjectId, KEP::DDRPlayers* players) = 0;

	virtual bool loadDDRGameData(ULONG ddrId, KEP::GameLevelsMap& gameData) = 0;
};

class APPPLUGINLIBRARY_API DDRModelFacet : public ModelFacet, public DDRModel {
public:
	DDRModelFacet();
	DDRModelFacet(const DDRModelFacet& other);
	virtual ~DDRModelFacet();
	virtual DDRModelFacet& operator=(const DDRModelFacet& rhs);

	virtual bool loadDDRPlayerData(const KEP::DDRDBEvent* ddre, KEP::DDREvent* replyEvent);

	virtual bool recordDDRScore(bool isPermanentZone, ULONG objectId, ULONG zoneIndex, int dynObjectId, ULONG ddrId, int playerId, const std::string& playerName, ULONG highScore, ULONG score, ULONG numPerfects, ULONG numPerfectsInARow);

	virtual bool recordDDRHighScore(int dynObjectId, int playerId, ULONG highScore, const std::string& playerName, bool isPermanentZone, ULONG objectId, ULONG zoneIndex);

	virtual bool refreshDDRHighScore(int dynObjectId, KEP::DDRPlayers* players);

	virtual bool loadDDRGameData(ULONG ddrId, KEP::GameLevelsMap& gameData);

private:
	mutable log4cplus::Logger _logger;
	KEP::fast_recursive_mutex m_sync;
};

/**
 * Going forward, whenever a database exception is raised, we should be rethrowing the exception
 * in a consistent manner so that clients of this object model can perform any context specific
 * handling, instead of forcing the response at this level.  E.g. Some calls should simply
 * log, some may need to treated as critical.  To that end DataModel throws exceptions.
 * 
 * That being said however, when integrating with existing code, which is not prepared to handle
 * these exceptions, we should suppress these exceptions.  This class provides that functionality
 * by encapsulating another DataModel implementation, forward requests to this implementation
 * and then catching any exceptions.
 */
class APPPLUGINLIBRARY_API SuppressedDDRModel : public DDRModel {
public:
	SuppressedDDRModel(DDRModel& impl);
	virtual ~SuppressedDDRModel();
	virtual SuppressedDDRModel& operator=(const SuppressedDDRModel& other);

	virtual bool loadDDRPlayerData(const KEP::DDRDBEvent* ddre, KEP::DDREvent* replyEvent);

	virtual bool recordDDRScore(bool isPermanentZone, ULONG objectId, ULONG zoneIndex, int dynObjectId, ULONG ddrId, int playerId, const std::string& playerName, ULONG highScore, ULONG score, ULONG numPerfects, ULONG numPerfectsInARow);

	virtual bool recordDDRHighScore(int dynObjectId, int playerId, ULONG highScore, const std::string& playerName, bool isPermanentZone, ULONG objectId, ULONG zoneIndex);

	virtual bool refreshDDRHighScore(int dynObjectId, KEP::DDRPlayers* players);

	virtual bool loadDDRGameData(ULONG ddrId, KEP::GameLevelsMap& gameData);

private:
	DDRModel& _impl;
};

} // namespace KEP