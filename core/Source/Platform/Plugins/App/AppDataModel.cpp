///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Common/KEPUtil/Application.h"
#include "Common/include/CoreStatic/CTitleObject.h"
#include "Common/include/CoreStatic/CSkillClass.h"
#include "Server/Database/MySqlUtil.h"
#include "Common/include/KEPHelpers.h"
#include "core/MySqlPLib/MySqlConnector.h"
#include "Plugins/App/ModelHelper.h"
#include "Plugins/App/AppDataModel.h"

#include "Plugins\App\DDRModelFacet.h"
#include "Plugins\App\PlayerModel.h"
#include "Plugins\App\ClanModelFacet.h"

using namespace std;
using namespace log4cplus;
using namespace mysqlpp;

namespace KEP {

// Nascent Database layer.
//    Current implemented to provide consistent exception handling without the shortcomings of
// BEGIN_TRY_SQL/END_TRY_SQL.  In the future will provide query building services and lease
// control.
//
//
// TODO:
// Move this to an Builder/Executor/Lease model
// Note that there seem to be issues with the use of SQLQueryParms and the storein_sequence function,
// a necessity for generic handling, so do not call this method until this resolved.  It may require
// upgrading to the 3.0 MySql++ libraries.
//
// In the mean time, use a simple define mechanism that doesn't query the baggage of
// BEGIN_TRY_SQL/END_TRY_SQL (e.g. forcing variable names and resources on us)
//
/**
 * Execute the pre-parsed query using the supplied query params and place them 
 * in the supplied sequence container (where sequence container means, std::list, std::vector, std::deque
 * etc.
 *
 * @param The query to be executed.
 * @param The parameters to be used to be used during execution.
 * @returns StoreQueryResult
 * @throws ModelException
 */
// END DB LAYER

// Suppression Wrapper implementation
// Suppression Wrapper implementation
// Suppression Wrapper implementation
// Suppression Wrapper implementation

SuppressedModel::SuppressedModel(DataModel& impl) :
		_impl(impl) {}
SuppressedModel::~SuppressedModel() {}
SuppressedModel& SuppressedModel::operator=(const SuppressedModel& /* other */) {
	return *this;
}

SuppressedModel& setConnector(IMySqlConnector* /*connector*/) {
	throw UnsupportedException(SOURCELOCATION);
}
SuppressedModel& setConnectorName(const std::string& /*connectorName*/) {
	throw UnsupportedException(SOURCELOCATION);
}

bool SuppressedModel::get3DAppPlayerTitles(int playerId, CTitleObjectList*& titles, std::string& title) {
	try {
		return _impl.get3DAppPlayerTitles(playerId, titles, title);
	} catch (const ChainedException& exc) {
		LOG4CPLUS_ERROR(Logger::getInstance(L"AppDataModel"), exc.str());
	}
	return false;
}

/**
 * Concrete implementation of our data model targeted at and Application Server.
 */
class APPPLUGINLIBRARY_API AppDataModel : public CommonModel {
public:
	AppDataModel();
	virtual ~AppDataModel();

	virtual bool get3DAppPlayerTitles(int playerId, CTitleObjectList*& titles, std::string& title);
	virtual ModelFacetRegistry& registry() { return _registry; }

private:
	ModelFacetRegistry _registry;
	fast_recursive_mutex m_sync;
	mutable log4cplus::Logger m_logger;
};

/**
 * Plugin that provides access to a DataModel
 */
class _declspec(dllexport) AppModelPlugin
		: public AbstractPlugin
		,
		  public DataModel {
public:
	AppModelPlugin();
	AppModelPlugin(const AppModelPlugin& other);
	virtual ~AppModelPlugin();

	virtual AppModelPlugin& operator=(const AppModelPlugin& other);
	virtual AppModelPlugin& setUp();
	virtual AppModelPlugin& tearDown();
	virtual ACTPtr stopPlugin() {
		return AbstractPlugin::stopPlugin();
	}
	virtual AppModelPlugin& enable(bool doEnable = true);

	/**
	 * Set the MetricsAgent implementation to be used by this plugin
	 *
	 */
	AppModelPlugin& setImpl(DataModel::ptr_t impl) {
		_impl = impl;
		return *this;
	}

	AppModelPlugin& setConnector(IMySqlConnector* connector) {
		_impl->setConnector(connector);
		return *this;
	}

	AppModelPlugin& setConnectorName(const std::string& name) {
		_impl->setConnectorName(name);
		return *this;
	}

	virtual IMySqlConnector* connector() {
		return _impl->connector();
	}

	virtual const std::string& connectorName() {
		return _impl->connectorName();
	}

	virtual AppModelPlugin& setGameId(unsigned long);
	virtual unsigned long gameId();

	virtual bool get3DAppPlayerTitles(int playerId, CTitleObjectList*& titles, std::string& title);
	virtual ModelFacetRegistry& registry() { return _impl->registry(); }

private:
	shared_ptr<DataModel> _impl;
};

AppModelPluginFactory::~AppModelPluginFactory() {}

AppDataModel::AppDataModel() :
		CommonModel(), m_logger(Logger::getInstance(L"AppDataModel")), _registry() {
	// keep memory management simple.  Simply put it on the heap.  The registry
	// will clean up on destruction.
	//
	// in our destructor.
	//
	_registry.bindToModel(*this);
	_registry.registerModelFacet(new DDRModelFacet())
		.registerModelFacet(new PlayerModelFacet())
		.registerModelFacet(new ClanModelFacet());
}

AppDataModel::~AppDataModel() {}

const string name() {
	return "AppDataModel";
}

unsigned long AppModelPlugin::gameId() {
	return _impl->gameId();
}

AppModelPlugin& AppModelPlugin::setGameId(unsigned long gameId) {
	_impl->setGameId(gameId);
	return *this;
}

bool AppDataModel::get3DAppPlayerTitles(int playerId, CTitleObjectList*& titles, std::string& title) {
	Logger alert_logger(m_logger);

	try {
		KGP_Connection::ConnectionPtr p = connector()->getConnection(connectorName());
		KGP_Connection& conn = *p;
		KGP_Connection* m_conn = &conn;

		BEGIN_TRY_SQL();
		Query query = m_conn->query();

		query << "SELECT "
			  << "t.title_id"
			  << ", t.name"
			  << ", pt.active "
			  << "FROM "
			  << "hosted_apps.player_titles pt "
			  << "join kgp_common.titles t on t.title_id=pt.title_id  "
			  << "WHERE "
			  << "player_id = %0 "
			  << "ORDER BY pt.active DESC";

		query.parse();
		LOG4CPLUS_TRACE(m_logger, query.str((int)playerId));
		StoreQueryResult results = query.store((int)playerId);

		if (results.num_rows() == 0)
			return false;
		titles = new CTitleObjectList();

		for (UINT i = 0; i < results.num_rows(); i++) {
			Row r = results.at(i);

			CTitleObject* o = new CTitleObject();
			o->m_titleID = r.at(0);
			o->m_titleName = r.at(1);
			bool active = r.at(2);
			titles->AddTail(o);

			if (active)
				title = o->m_titleName;
		}
		END_TRY_SQL();

	} catch (const IMySqlConnector::Exception& e) {
		DataModel::Exception dme(SOURCELOCATION, "Connection error!");
		dme.chain(e);
		throw dme;
	}

	return true;
}

// AppModelPlugin implementation
// AppModelPlugin implementation
// AppModelPlugin implementation
// AppModelPlugin implementation
//
AppModelPlugin::AppModelPlugin() :
		AbstractPlugin("AppDataModel"), _impl(DataModel::ptr_t(new AppDataModel())) {
}

AppModelPlugin::~AppModelPlugin() {}
AppModelPlugin& AppModelPlugin::enable(bool) {
	return *this;
} // tbi
AppModelPlugin& AppModelPlugin::setUp() {
	try {
		IMySqlConnector* connector = host()->DEPRECATED_getInterface<IMySqlConnector>("MySqlConnector");
		LOG4CPLUS_DEBUG(Logger::getInstance(L"DataModel"), "[0x" << hex << this << "]AppModelPlugin::setup()impl=0x" << hex << _impl.operator->() << ")");
		_impl->setConnector(connector);
	} catch (const PluginException& /* exception */) {
		throw ModelException(SOURCELOCATION, "No MySqlConnector plugin available!");
	}
	return *this;
}
AppModelPlugin& AppModelPlugin::tearDown() {
	return *this;
}
AppModelPlugin& AppModelPlugin::operator=(const AppModelPlugin& /*rhs*/) {
	return *this;
}

bool AppModelPlugin::get3DAppPlayerTitles(int playerId, CTitleObjectList*& titles, std::string& title) {
	return _impl->get3DAppPlayerTitles(playerId, titles, title);
}

// AppModelPluginFactory implementation
// AppModelPluginFactory implementation
// AppModelPluginFactory implementation
// AppModelPluginFactory implementation
//
AppModelPluginFactory::AppModelPluginFactory() :
		AbstractPluginFactory("AppDataModel") {}
PluginPtr
AppModelPluginFactory::createPlugin(const TIXMLConfig::Item& config) const {
	LOG4CPLUS_TRACE(Logger::getInstance(L"DataModel"), "AppModelPluginFactory::createPlugin()");

	//  <PluginConfig>
	//      <dbconnector>unifieddb</dbconnector>
	//	</PluginConfig>

	// Guard clause.  If no configuration provided, we can go with
	// default settings.  We're done here.
	//
	if (config == 0)
		return PluginPtr(new AppModelPlugin());

	try {
		TIXMLConfig::Item pe = config.firstChild("dbconnector");
		if (pe == TIXMLConfig::nullItem()) {
			// Cannot start throw exception
			PluginPtr p(new AppModelPlugin());
			return p;
		}
		string connectorName(pe.text());

		pe = config.firstChild("gameid");
		if (pe == TIXMLConfig::nullItem()) {
			// Cannot start throw exception
			PluginPtr p(new AppModelPlugin());
			return p;
		}
		string gameId(pe.text());

		// if and when we "unabandon" the MySqlMetricsAgent, this will need to be modified
		// to switch based on the configuration.
		//
		AppDataModel* impl = new AppDataModel();
		AppModelPlugin* pPlugin = new AppModelPlugin();
		pPlugin->setImpl(DataModel::ptr_t(impl));
		pPlugin->setConnectorName(connectorName);
		pPlugin->setGameId(atol(gameId.c_str()));
		// Now let's start disabling various facets
		//
		TIXMLConfig::Item disabled = config.firstChild("disabled");
		if (disabled == TIXMLConfig::nullItem())
			return PluginPtr(pPlugin);

		vector<string> disabledVect;
		for (TIXMLConfig::Item disabledFacet = disabled.firstChild("facet"); disabledFacet != TIXMLConfig::nullItem(); disabledFacet = disabled.nextSibling()) {
			string disabledName = disabledFacet.attribute("name");
			pPlugin->registry().disableFacet(disabledName);
		}
		return PluginPtr(pPlugin);
	} catch (const ParseException& /* exc */) {
		// LOG4CPLUS_ERROR( Logger::getInstance( L"MetricsAgent" ), "Failed to parse url [" << pcURL << "] Using null metrics implementation [" << exc.toString() << "]" );
		return PluginPtr(new AppModelPlugin());
	}
}

} // namespace KEP