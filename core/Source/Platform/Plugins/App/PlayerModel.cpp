///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common/include/KEPCommon.h"

#include "Plugins/App/PlayerModel.h"
#include "Event/DDR/DDR_S_GameData.h"
#include "mysql++/mysql++.h"
#include "Server/Database/MySqlUtil.h"
#include "core/MySqlPLib/MySqlConnector.h"
#include "Plugins/App/ModelHelper.h"
#include "Event\Events\DDRDBEvent.h"
#include "Event\Events\DDREvent.h"
#include "Common/include/CoreStatic/CSkillClass.h"

using namespace mysqlpp;
using namespace log4cplus;

namespace KEP {

PlayerModelFacet& PlayerModelFacet::updateSkills(int playerId, KEPModel::SkillVector& skills) {
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;
	try {
		Query cleanup = conn.query("delete from hosted_apps.player_skills where game_id = %0 and player_id = %1");
		cleanup.parse();
		LOG4CPLUS_TRACE(_logger, cleanup.str((int)hostModel().gameId(), (int)playerId));
		cleanup.execute((int)hostModel().gameId(), (int)playerId);

		if (skills.size() == 0)
			return *this;

		Query query = conn.query();
		query << "INSERT INTO hosted_apps.player_skills "
			  << "( game_id, player_id, skill_id, current_level, current_experience) "
			  << " VALUES "; // ( %0, %1, %2, %3 )");

		char delim = ' ';

		// re-add
		for (KEPModel::SkillVector::iterator i = skills.begin(); i != skills.end(); i++) {
			// inventory loop
			query << delim << "(" << hostModel().gameId()
				  << "," << playerId
				  << "," << i->type()
				  << "," << i->currentLevel()
				  << "," << i->currentExperience() << ")";
			delim = ',';
		}
		LOG4CPLUS_DEBUG(_logger, query.str());
		query.parse();
		query.execute();
		// todo: investigate existing data re: unique index.
	}
	STD_DB_EXCEPTION_HANDLING
	return *this;
}

} // namespace KEP