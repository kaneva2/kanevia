#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/WebCall.h"
#include "Common/KEPUtil/URL.h"
#include "Plugins/App/WebServicesProxy.h"

TEST(WebServicesProxyTests, Test01) {
	WebServicesProxy proxy("dev-wok.kaneva.com");
	vector<ServerListItem> v = proxy.enumerateGameServers(5316);
	ASSERT_TRUE(v.size() > 0);
}