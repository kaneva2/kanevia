///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/Tests/PluginHostTestHelper.h"
#include "Plugins/App/AppDataModel.h"
#include "Event/Events/DDRDBEvent.h"
#include "Event/Events/DDREvent.h"
#include "Plugins/App/DDRModelFacet.h"

class AppModelFacetTests : public PluginHostTests {
public:
	AppModelFacetTests() :
			PluginHostTests() {
	}

	virtual void SetUp() {
		try {
			stringstream ss1;

			// This is going to become cumbersome.  Being able construct a template
			// and modify it will be a good thing.
			//
			ss1 << "<Config>"
				<< "<PluginHost>"
				<< "<Libraries>"
				<< "<Library path=\"CorePluginLibrary.dll\"/>"
				<< "<Library path=\"AppPluginLibrary.dll\"/>"
				<< "</Libraries>"
				<< "<Plugins>"
				// Configure a database connector.
				<< "<Plugin factory_name=\"MySqlConnector\" disabled=\"false\" >"
				<< "<PluginConfig>"
				<< "<databases>"
				<< "<database connection_name=\"hosted_apps\">" // Look out...this is gonna break something.
				<< "<DB_Server>localhost</DB_Server>"
				<< "<DB_Port>3306</DB_Port>"
				<< "<DB_Name>kgp3d</DB_Name>" // connect to an app db...not the hosted_apps
				<< "<DB_User>unifieddb</DB_User>"
				<< "<DB_PasswordClear>unifieddb</DB_PasswordClear>"
				<< "<DB_DontShowEdit>1</DB_DontShowEdit>"
				<< "<DB_Disabled>0</DB_Disabled>"
				<< "<LocalPlayers>0</LocalPlayers>"
				<< "</database>"
				<< "</databases>"
				<< "<AlertMailer mail_host=\"mail.kanevia.com\" to=\"recipient@kanevia.com\" from=\"mailer@kanevia.com\" />"
				<< "</PluginConfig>"
				<< "</Plugin>"

				<< "<Plugin name=\"AppDataModel\" factory_name=\"AppDataModel\" disabled=\"false\">"
				<< "<PluginConfig>" // enabled=\"1\">"
				<< "<dbconnector>hosted_apps</dbconnector>"
				<< "<gameid>4</gameid>"
				<< "<disabled>"
				<< "<facet name=\"DDRModel\"/>"
				<< "</disabled>"
				<< "</PluginConfig>"
				<< "</Plugin>"

				<< "</Plugins>"
				<< "</PluginHost>"
				<< "</Config>";
			addConfig("TestHost01", ss1.str());
			PluginHostTests::SetUp();
		} catch (const ChainedException& exc) {
			FAIL() << exc.toString().c_str();
		}
	}

	virtual void TearDown() {
		PluginHostTests::TearDown();
	}
};

TEST_F(AppModelFacetTests, FacetTest01) {
	DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
	ASSERT_TRUE(model > 0);
	ASSERT_THROW(model->registry().getFacet<DDRModel>("DDRModel"), FacetUnavailableException);
}

TEST_F(AppModelFacetTests, FacetTest02) {
	DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
	ASSERT_TRUE(model > 0);
	try {
		model->registry().getFacetNoThrow<DDRModel>("DDRModel");
	} catch (const FacetUnavailableException&) {
		FAIL() << "Unexpected exception [FacetUnavailableException]." << endl;
	}
}

TEST_F(AppModelFacetTests, FacetTest03) {
	DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
	ASSERT_TRUE(model > 0);
	try {
		DDRModel* ddrModel = model->registry().getFacetNoThrow<DDRModel>("DDRModel");
		ASSERT_TRUE(ddrModel == 0);
	} catch (const FacetUnavailableException&) {
		FAIL() << "Unexpected exception [FacetUnavailableException]." << endl;
	}
}

class AppModelPluginTests : public PluginHostTests {
public:
	AppModelPluginTests() :
			PluginHostTests() {
		stringstream ss1;

		// This is going to become cumbersome.  Being able construct a template
		// and modify it will be a good thing.
		//
		ss1 << "<Config>"
			<< "<PluginHost>"
			<< "<Libraries>"
			<< "<Library path=\"CorePluginLibrary.dll\"/>"
			<< "<Library path=\"AppPluginLibrary.dll\"/>"
			<< "</Libraries>"
			<< "<Plugins>"
			// Configure a database connector.
			<< "<Plugin factory_name=\"MySqlConnector\" disabled=\"false\" >"
			<< "<PluginConfig>"
			<< "<databases>"
			<< "<database connection_name=\"hosted_apps\">" // Look out...this is gonna break something.
			<< "<DB_Server>localhost</DB_Server>"
			<< "<DB_Port>3306</DB_Port>"
			<< "<DB_Name>kgp3d</DB_Name>" // connect to an app db...not the hosted_apps
			<< "<DB_User>unifieddb</DB_User>"
			<< "<DB_PasswordClear>unifieddb</DB_PasswordClear>"
			<< "<DB_DontShowEdit>1</DB_DontShowEdit>"
			<< "<DB_Disabled>0</DB_Disabled>"
			<< "<LocalPlayers>0</LocalPlayers>"
			<< "</database>"
			<< "</databases>"
			<< "<AlertMailer mail_host=\"mail.kanevia.com\" to=\"recipient@kanevia.com\" from=\"mailer@kanevia.com\" />"
			<< "</PluginConfig>"
			<< "</Plugin>"

			<< "<Plugin name=\"AppDataModel\" factory_name=\"AppDataModel\" disabled=\"false\">"
			<< "<PluginConfig>" // enabled=\"1\">"
			<< "<dbconnector>hosted_apps</dbconnector>"
			<< "<gameid>4</gameid>"
			<< "</PluginConfig>"
			<< "</Plugin>"

			<< "</Plugins>"
			<< "</PluginHost>"
			<< "</Config>";
		addConfig("TestHost01", ss1.str());
	}

	virtual void SetUp() {
		try {
			PluginHostTests::SetUp();
		} catch (const ChainedException& exc) {
			FAIL() << exc.toString().c_str();
		}
	}

	virtual void TearDown() {
		PluginHostTests::TearDown();
	}
};

TEST_F(AppModelPluginTests, DISABLED_AppModelPluginTest01) {
	PluginHost& host = hosts.find("TestHost01")->second.second;
	try {
		DataModel::Ptr p = host.getInterface<DataModel>("AppModel");
		CTitleObjectList* l;
		string s;
		p->get3DAppPlayerTitles(0, l, s);
	} catch (const DataModel::Exception& exc) {
		FAIL() << exc.toString().c_str();
	} catch (const PluginException& exc) {
		FAIL() << exc.toString().c_str();
	}
}

#include <mysql++.h>
#include <ssqls.h>
#include <qparms.h>

sql_create_4(test_rec,
	1, 4, int, ddr_id, int, level_id, int, seed, int, num_moves);
//    string, type,
//  int, play_order

TEST(DiscoTests, DISABLED_SQLQueryParmesan) {
	// Connect to the sample database.
	mysqlpp::Connection conn(false);

	if (!conn.connect("hosted_apps", "localhost", "unifieddb", "unifieddb")) {
		FAIL() << "Connection Failed: " << conn.error() << endl;
	}

	mysqlpp::Query query = conn.query();

	std::stringstream ss;
	ss << "SELECT ddr_id, level_id, seed, num_moves "
	   << "FROM hosted_apps.ddr  "
	   << "WHERE ddr_id = %0 ORDER BY level_id asc";
	LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), ss.str());
	query << ss.str().c_str();
	query.parse();

	std::vector<test_rec> testRec;

	mysqlpp::SQLQueryParms parms;
	parms << (int)369;
	try {
		query.storein_sequence<std::vector<test_rec>>(testRec, parms);
	} catch (const mysqlpp::BadQuery& er) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), er.query_string());
	} catch (const mysqlpp::Exception& exc) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), exc.what());
	} catch (...) {
		LOG4CPLUS_TRACE(Logger::getInstance(L"AppDataModel"), "Fail!");
	}

	/*
    // Retrieve a subset of the sample stock table set up by resetdb
    // and display it.
    mysqlpp::Query query = conn.query("select game_id from ddr");
    if (mysqlpp::StoreQueryResult res = query.store()) {
        cout << "We have:" << endl;
        mysqlpp::StoreQueryResult::const_iterator it;
        for (it = res.begin(); it != res.end(); ++it) {
            mysqlpp::Row row = *it;
            cout << '\t' << row[0] << endl;
        }
    }
    else {
        cerr << "Failed to get item list: " << query.error() << endl;
    }
    */
}

TEST_F(AppModelPluginTests, loadDDRGameDataTest01) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);

		GameLevelsMap glMap;
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").loadDDRGameData(369, glMap)); // Know ddr id from ddr_globals
		ASSERT_TRUE(glMap.size() > 0);
		ASSERT_TRUE(glMap.find(369)->first > 0);
		ASSERT_EQ(10, glMap.find(369)->second->m_levelsMap.size());
	} catch (const ChainedException& exc) {
		FAIL() << exc.toString();
	}
}
TEST_F(AppModelPluginTests, DISABLED_TEMPORARILTY_FacetAcquisitionTest01) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);
		GameLevelsMap glMap;
		ModelFacetRegistry& registry = model->registry();
		ASSERT_THROW(registry.getFacet<DDRModel>("adadf"), ElementNotFoundException);
	} catch (const ChainedException& exc) {
		FAIL() << "Unexpected exception: " << exc.toString();
	}
}

TEST_F(AppModelPluginTests, DISABLED_TEMPORARILTY_FacetAcquisitionTest02) {
	DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
	ASSERT_TRUE(model > 0);
	GameLevelsMap glMap;
	ModelFacetRegistry& registry = model->registry();
	ASSERT_NO_THROW(registry.getFacet<DDRModel>("DDRModel"));
}

TEST_F(AppModelPluginTests, loadDDRPlayerTest01) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");

		// The fact that we are having to pull in all of these application level classes is an indication that the
		// boundary is defined incorrectly.
		//
		KEP::DDRDBEvent in;
		KEP::DDREvent out;
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").loadDDRPlayerData(&in, &out));
	} catch (const ChainedException& ex) {
		FAIL() << ex.toString();
	}
}

TEST_F(AppModelPluginTests, recordDDRScoreTest01) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").recordDDRScore(true // isPermanentZone  - requires a test variant
			,
			0 // ULONG                 objectId
			,
			0 // ULONG                 zoneIndex
			,
			0 // int                   dynObjectId
			,
			369 // ULONG                 ddrId
			,
			0 // int                   playerId
			,
			"BillyA" // const std::string&    playerName
			,
			0 // ULONG                 highScore
			,
			0 // ULONG                 score
			,
			0 // ULONG                 numPerfects
			,
			0)); // ULONG                 numPerfectsInARow   )   = 0;

	} catch (const ChainedException& ex) {
		FAIL() << ex.toString();
	}
}

#if 0
TEST_F( AppModelPluginTests, recordDDRScoreTest02 ) {
    try {
        DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
        ASSERT_TRUE( model > 0 );

        // ASSERT_TRUE( model->loadDDRPlayerData( &in, &out ) );
        ASSERT_TRUE( model->registry().getFacet<DDRModel>("DDRModel").recordDDRScore(     true                // isPermanentZone  - requires a test variant
                            , 0             // ULONG                 objectId
                            , 0             // ULONG                 zoneIndex
                            , 0             // int                   dynObjectId
                            , 369             // ULONG                 ddrId
                            , 0             // int                   playerId
                            , "BillyA"      // const std::string&    playerName
                            , 0             // ULONG                 highScore
                            , 0             // ULONG                 score
                            , 0             // ULONG                 numPerfects
                            , 0     ) ) );      // ULONG                 numPerfectsInARow   )   = 0;
    }
    catch( const ChainedException& ex ) {
        FAIL() << ex.toString();
    }
}
#endif

TEST_F(AppModelPluginTests, recordDDRScoreTest02) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);

		// ASSERT_TRUE( model->loadDDRPlayerData( &in, &out ) );
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").recordDDRScore(false // isPermanentZone  - requires a test variant
			,
			0 // ULONG                 objectId
			,
			0 // ULONG                 zoneIndex
			,
			0 // int                   dynObjectId
			,
			369 // ULONG                 ddrId
			,
			0 // int                   playerId
			,
			"BillyA" // const std::string&    playerName
			,
			0 // ULONG                 highScore
			,
			0 // ULONG                 score
			,
			0 // ULONG                 numPerfects
			,
			0)); // ULONG                 numPerfectsInARow   )   = 0;
	} catch (const ChainedException& ex) {
		FAIL() << ex.toString();
	}
}

TEST_F(AppModelPluginTests, recordDDRHighScoreTest01) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);

		// The fact that we are having to pull in all of these application level classes is an indication that the
		// boundary is defined incorrectly.
		//
		//    KEP::DDRDBEvent in;
		//    KEP::DDREvent   out;

		// ASSERT_TRUE( model->loadDDRPlayerData( &in, &out ) );
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").recordDDRHighScore(0 //int                   dynObjectId
			,
			1 //int                   playerId
			,
			2 // ULONG                 highScore
			,
			"BillyA" // const std::string&    playerName
			,
			true // bool                  isPermanentZone
			,
			3 // ULONG                 objectId
			,
			4)); // ULONG                 zoneIndex    ) );

		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").recordDDRHighScore(0 //int                   dynObjectId
			,
			1 //int                   playerId
			,
			3 // ULONG                 highScore
			,
			"BillyA" // const std::string&    playerName
			,
			true // bool                  isPermanentZone
			,
			3 // ULONG                 objectId
			,
			4)); // ULONG                 zoneIndex    ) );
	} catch (const ChainedException& ex) {
		FAIL() << ex.toString();
	}
}

TEST_F(AppModelPluginTests, recordDDRHighScoreTest02) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);

		// The fact that we are having to pull in all of these application level classes is an indication that the
		// boundary is defined incorrectly.
		//
		//    KEP::DDRDBEvent in;
		//    KEP::DDREvent   out;

		// ASSERT_TRUE( model->loadDDRPlayerData( &in, &out ) );
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").recordDDRHighScore(0 //int                   dynObjectId
			,
			1 //int                   playerId
			,
			2 // ULONG                 highScore
			,
			"BillyA" // const std::string&    playerName
			,
			false // bool                  isPermanentZone
			,
			3 // ULONG                 objectId
			,
			4)); // ULONG                 zoneIndex    ) );

		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").recordDDRHighScore(0 //int                   dynObjectId
			,
			1 //int                   playerId
			,
			3 // ULONG                 highScore
			,
			"BillyA" // const std::string&    playerName
			,
			false // bool                  isPermanentZone
			,
			3 // ULONG                 objectId
			,
			4)); // ULONG                 zoneIndex    ) );
	} catch (const ChainedException& ex) {
		FAIL() << ex.toString();
	}
}

TEST_F(AppModelPluginTests, refreshDDRHighScoreTest01) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);

		DDRPlayers playaz;
		playaz.SetDynamicObjectObjectId(3);
		playaz.SetDynamicObjectZoneIndex(4);
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").refreshDDRHighScore(3, &playaz));
		ASSERT_EQ(3, playaz.GetHighScore());
	} catch (const ChainedException& ex) {
		FAIL() << ex.toString();
	}
}

TEST_F(AppModelPluginTests, refreshDDRHighScoreTest02) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);

		DDRPlayers playaz;
		playaz.SetDynamicObjectObjectId(0);
		playaz.SetDynamicObjectZoneIndex(4);
		playaz.SetIsPermanentZone(false);
		ASSERT_TRUE(model->registry().getFacet<DDRModel>("DDRModel").refreshDDRHighScore(0, &playaz));
		ASSERT_EQ(3, playaz.GetHighScore());
	} catch (const ChainedException& ex) {
		FAIL() << ex.toString();
	}
}

class TestFacetImpl01 : public ModelFacet {
public:
	TestFacetImpl01() :
			ModelFacet("TestFacetImpl01") {}

private:
};

class TestFacetImpl02 : public ModelFacet {
public:
	TestFacetImpl02() :
			ModelFacet("TestFacetImpl02") {}

private:
};

TEST(AppModelPluigntests, DISABLED_TEMPORARILTY_FacetInstantiationTest01) {
	TestFacetImpl01 facet;
	ASSERT_STREQ("TestFacetImpl01", facet.name().c_str());
}

// Assert that on a name miss, ElementNotFoundException is thrown
TEST(AppModelPluigntests, DISABLED_TEMPORARILTY_FacetTest01) {
	try {
		ModelFacetRegistry registry;
		registry.registerModelFacet(new TestFacetImpl01())
			.registerModelFacet(new TestFacetImpl02());
		registry.getFacet<TestFacetImpl01>("nohit");
		FAIL() << "Expected ElementNotFoundException";
	} catch (const ElementNotFoundException& enf) {
		ASSERT_TRUE(StringHelpers::startsWith("ElementNotFoundException: Facet with name [nohit] not found.", enf.toString()));
	}
}

// Assert that on type miss, ElementNotFoundException is thrown
TEST(AppModelPluigntests, DISABLED_TEMPORARILTY_FacetTest02) {
	try {
		ModelFacetRegistry registry;
		registry.registerModelFacet(new TestFacetImpl01())
			.registerModelFacet(new TestFacetImpl02());
		registry.getFacet<TestFacetImpl02>("TestFacetImpl01");
		FAIL() << "Expected ElementNotFoundException";
	} catch (const ElementNotFoundException& enf) {
		cout << enf.toString() << endl;
		ASSERT_TRUE(StringHelpers::startsWith("ElementNotFoundException: Facet with name [TestFacetImpl01] type not found.", enf.toString()));
	}
}

TEST(AppModelPluigntests, DISABLED_TEMPORARILTY_FacetTest03) {
	try {
		ModelFacetRegistry registry;
		registry.registerModelFacet(new TestFacetImpl01())
			.registerModelFacet(new TestFacetImpl02());
		registry.getFacet<TestFacetImpl01>("TestFacetImpl01");
	} catch (const ElementNotFoundException& enf) {
		FAIL() << "Unexpected exception: " << enf.toString().c_str();
	}
}

TEST(AppModelPluigntests, DISABLED_TEMPORARILTY_DDRFacetTest01) {
	try {
		ModelFacetRegistry registry;
		registry.registerModelFacet(new DDRModelFacet());
		registry.getFacet<TestFacetImpl01>("TestFacetImpl01");
	} catch (const ElementNotFoundException& enf) {
		FAIL() << "Unexpected exception: " << enf.toString().c_str();
	}
}
