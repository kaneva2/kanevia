#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/Tests/PluginHostTestHelper.h"
#include "Plugins/App/AppDataModel.h"
#include "Plugins/App/ClanModelFacet.h"
class ClanModelFacetTests : public PluginHostTests {
public:
	ClanModelFacetTests() :
			PluginHostTests() {
	}

	virtual void SetUp() {
		try {
			stringstream ss1;

			// This is going to become cumbersome.  Being able construct a template
			// and modify it will be a good thing.
			//
			ss1 << "<Config>"
				<< "<PluginHost>"
				<< "<Libraries>"
				<< "<Library path=\"CorePluginLibrary.dll\"/>"
				<< "<Library path=\"AppPluginLibrary.dll\"/>"
				<< "</Libraries>"
				<< "<Plugins>"
				// Configure a database connector.
				<< "<Plugin factory_name=\"MySqlConnector\" disabled=\"false\" >"
				<< "<PluginConfig>"
				<< "<databases>"
				<< "<database connection_name=\"hosted_apps\">" // Look out...this is gonna break something.
				<< "<DB_Server>localhost</DB_Server>"
				<< "<DB_Port>3306</DB_Port>"
				<< "<DB_Name>kgp3d</DB_Name>" // connect to an app db...not the hosted_apps
				<< "<DB_User>unifieddb</DB_User>"
				<< "<DB_PasswordClear>unifieddb</DB_PasswordClear>"
				<< "<DB_DontShowEdit>1</DB_DontShowEdit>"
				<< "<DB_Disabled>0</DB_Disabled>"
				<< "<LocalPlayers>0</LocalPlayers>"
				<< "</database>"
				<< "</databases>"
				<< "<AlertMailer mail_host=\"mail.kanevia.com\" to=\"recipient@kanevia.com\" from=\"mailer@kanevia.com\" />"
				<< "</PluginConfig>"
				<< "</Plugin>"

				<< "<Plugin name=\"AppDataModel\" factory_name=\"AppDataModel\" disabled=\"false\">"
				<< "<PluginConfig>" // enabled=\"1\">"
				<< "<dbconnector>hosted_apps</dbconnector>"
				<< "<gameid>4</gameid>"
				<< "<disabled>"
				<< "<facet name=\"ClanModel\"/>"
				<< "</disabled>"
				<< "</PluginConfig>"
				<< "</Plugin>"

				<< "</Plugins>"
				<< "</PluginHost>"
				<< "</Config>";
			addConfig("TestHost01", ss1.str());
			PluginHostTests::SetUp();
		} catch (const ChainedException& exc) {
			FAIL() << exc.toString().c_str();
		}
	}

	virtual void TearDown() {
		PluginHostTests::TearDown();
	}
};

/**
 * Test that disabled clan model facet raises appropriate event when attempt is made to acquire it.
 * This is actually a redundant test, as this will work for all facets and it is already tested by
 * DDRModel
 */
TEST_F(ClanModelFacetTests, FacetTest01) {
	DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
	ASSERT_TRUE(model > 0);
	ASSERT_THROW(model->registry().getFacet<ClanModel>("ClanModel"), FacetUnavailableException);
}

/**
 * Test the NoThrow version of getFacet() Assert that it does NOT throw an exception.  Again this is a 
 * redundant test.  NoThrow is intended for use in legacy code that is not up to snuff re: using 
 * exceptions for errors instead of return codes.
 */
TEST_F(ClanModelFacetTests, FacetTest02) {
	DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
	ASSERT_TRUE(model > 0);
	try {
		model->registry().getFacetNoThrow<ClanModel>("ClanModel");
	} catch (const FacetUnavailableException&) {
		FAIL() << "Unexpected exception [FacetUnavailableException]." << endl;
	}
}

/**
 * 
 */
TEST_F(ClanModelFacetTests, FacetTest03) {
	DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
	ASSERT_TRUE(model > 0);
	try {
		ClanModel* clanModel = model->registry().getFacetNoThrow<ClanModel>("ClanModel");
		ASSERT_TRUE(clanModel == 0);
	} catch (const FacetUnavailableException&) {
		FAIL() << "Unexpected exception [FacetUnavailableException]." << endl;
	}
}

class ClanModelPluginTests : public PluginHostTests {
public:
	ClanModelPluginTests() :
			PluginHostTests() {
		stringstream ss1;

		// This is going to become cumbersome.  Being able construct a template
		// and modify it will be a good thing.
		//
		ss1 << "<Config>"
			<< "<PluginHost>"
			<< "<Libraries>"
			<< "<Library path=\"CorePluginLibrary.dll\"/>"
			<< "<Library path=\"AppPluginLibrary.dll\"/>"
			<< "</Libraries>"
			<< "<Plugins>"
			// Configure a database connector.
			<< "<Plugin factory_name=\"MySqlConnector\" disabled=\"false\" >"
			<< "<PluginConfig>"
			<< "<databases>"
			<< "<database connection_name=\"hosted_apps\">" // Look out...this is gonna break something.
			<< "<DB_Server>localhost</DB_Server>"
			<< "<DB_Port>3306</DB_Port>"
			<< "<DB_Name>kgp3d</DB_Name>" // connect to an app db...not the hosted_apps
			<< "<DB_User>unifieddb</DB_User>"
			<< "<DB_PasswordClear>unifieddb</DB_PasswordClear>"
			<< "<DB_DontShowEdit>1</DB_DontShowEdit>"
			<< "<DB_Disabled>0</DB_Disabled>"
			<< "<LocalPlayers>0</LocalPlayers>"
			<< "</database>"
			<< "</databases>"
			<< "<AlertMailer mail_host=\"mail.kanevia.com\" to=\"recipient@kanevia.com\" from=\"mailer@kanevia.com\" />"
			<< "</PluginConfig>"
			<< "</Plugin>"

			<< "<Plugin name=\"AppDataModel\" factory_name=\"AppDataModel\" disabled=\"false\">"
			<< "<PluginConfig>" // enabled=\"1\">"
			<< "<dbconnector>hosted_apps</dbconnector>"
			<< "<gameid>4</gameid>"
			<< "</PluginConfig>"
			<< "</Plugin>"

			<< "</Plugins>"
			<< "</PluginHost>"
			<< "</Config>";
		addConfig("TestHost01", ss1.str());
	}

	virtual void SetUp() {
		try {
			PluginHostTests::SetUp();
		} catch (const ChainedException& exc) {
			FAIL() << exc.toString().c_str();
		}
	}

	virtual void TearDown() {
		PluginHostTests::TearDown();
	}
};

#include <mysql++.h>
#include <ssqls.h>
#include <qparms.h>

TEST_F(ClanModelPluginTests, createClanTest01) {
	try {
		DataModel* model = hosts.find("TestHost01")->second.second.getInterface<DataModel>("AppDataModel");
		ASSERT_TRUE(model > 0);

		GameLevelsMap glMap;
		ClanModel& facet = model->registry().getFacet<ClanModel>("ClanModel");
		unsigned long clanId = facet.createClan(0, 100, "TestClan");
		ASSERT_TRUE(clanId > 0); // Know ddr id from ddr_globals
		facet.addMember(0, clanId, 101);

		// To properly test this need a feed back method like this.
		// this will come in time as continue to work through the model
		//
		//        facet.loadClan(0,clanId);
		facet.removeMember(0, clanId, 101);
		facet.deleteClansByOwner(0, 100);
	} catch (const ChainedException& exc) {
		FAIL() << exc.toString();
	}
}
