///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/ChainedException.h"
#include "Common/KEPUtil/Algorithms.h"
#include "Common/KEPUtil/WebCall.h"
#include "Common/KEPUtil/URL.h"

class ServerListItem {
public:
	ServerListItem() {}
	ServerListItem(const ServerListItem& other) :
			_serverId(other._serverId) //0
			,
			_serverName(other._serverName) //1
			,
			_gameName(other._gameName) //2
			,
			_ipAddress(other._ipAddress) //3
			,
			_port(other._port) //4
			,
			_gameExe(other._gameExe) //5
			,
			_patchURL(other._patchURL) //6
			,
			_numPlayers(other._numPlayers) //7
			,
			_maxPlayers(other._maxPlayers) //8
			,
			_serverStatus(other._serverStatus) //9
			,
			_patch_type(other._patch_type) //10
			,
			_clientPath(other._clientPath) //11
	{}

	virtual ~ServerListItem() {}

	ServerListItem& operator=(const ServerListItem& rhs) {
		_serverId = rhs._serverId; //0
		_serverName = rhs._serverName; //1
		_gameName = rhs._gameName; //2
		_ipAddress = rhs._ipAddress; //3
		_port = rhs._port; //4
		_gameExe = rhs._gameExe; //5
		_patchURL = rhs._patchURL; //6
		_numPlayers = rhs._numPlayers; //7
		_maxPlayers = rhs._maxPlayers; //8
		_serverStatus = rhs._serverStatus; //9
		_patch_type = rhs._patch_type; //10
		_clientPath = rhs._clientPath; //11
		return *this;
	}

	ServerListItem& parseWebResponse(const std::string response) {
		if (response.length() == 0)
			return *this;
		string content = response + ";";
		vector<string> parts;
		StringHelpers::explode(parts, content, ";");
		if (parts.size() < 12)
			throw ChainedException(SOURCELOCATION, "Stack underflow: " + response);
		_serverId = parts[0];
		_serverName = parts[1];
		_gameName = parts[2];
		_ipAddress = parts[3];
		_port = parts[4];
		_gameExe = parts[5];
		_patchURL = parts[6];
		_numPlayers = parts[7];
		_maxPlayers = parts[8];
		_serverStatus = parts[9];
		_patch_type = parts[10];
		_clientPath = parts[11];
		return *this;
	}

	string _serverId; //0
	string _serverName; //1
	string _gameName; //2
	string _ipAddress; //3
	string _port; //4
	string _gameExe; //5
	string _patchURL; //6
	string _numPlayers; //7
	string _maxPlayers; //8
	string _serverStatus; //9
	string _patch_type; //10
	string _clientPath; //11
};

// http://dev-wok.kaneva.com/kgp/gamelogin.aspx?user=user@email.com
// http://wok.kaneva.com/kgp/serverlist.aspx?id=3296&ver=8&v=2
// Bravo YC on versioning your protocols.  However, it does mean that
// this proxy will be need to be able to facilitate this.
// but we will simply implement straight out and expand later.
//
const string GameLoginURL("http://wok.kaneva.com/kgp/gamelogin.aspx?user=user@email.com");

// Can explicitly alter URL after the fact
const string ServerListURL("http://wok.kaneva.com/kgp/serverlist.aspx?id=3296&ver=8&v=2" );

/**
 * This class provides access to wok web services on callers behalf.
 * I.e. isolates web access logic and marshalls results. 
 * This is a nascent implementation hence no abstractions have been
 * put in place.  In fact it is currently a header-only implementation
 * that has not been exposed in the plugin framework.
 */
class WebServicesProxy {
public:
	WebServicesProxy() :
			_gameLoginURL(GameLoginURL), _serverListURL(ServerListURL), _host("localhost"), _port(80), _webCaller("") {}

	WebServicesProxy(const std::string host, unsigned short port = 80) :
			_gameLoginURL(GameLoginURL), _serverListURL(ServerListURL), _host(host), _port(port), _webCaller("") // should prolly be a guid version.
	{}

	WebServicesProxy(const WebServicesProxy& other) :
			_gameLoginURL(GameLoginURL), _serverListURL(ServerListURL), _host(other._host), _port(other._port), _webCaller("") {}

	virtual ~WebServicesProxy() {}

	WebServicesProxy& operator=(const WebServicesProxy& rhs) {
		_host = rhs._host;
		_port = rhs._port;
		return *this;
	}

	bool operator==(const WebServicesProxy& rhs) {
		if (this == &rhs)
			return true;
		if (rhs._host != _host)
			return false;
		if (rhs._port != _port)
			return false;
		return true;
	}

	template <typename CollectedT>
	const CollectedT* find(const CollectedT& needle, const vector<CollectedT>& haystack) {
		for (vector<CollectedT>::const_iterator i = haystack.cbegin(); i != haystack.cend(); i++) {
			if (needle == *i)
				return &(*i);
		}
		return 0;
	}

	std::vector<ServerListItem> enumerateGameServers(unsigned long gameId, const vector<string>& excludedHosts = vector<string>()) {
		vector<ServerListItem> ret;
		try {
			URL url = URL::parse(ServerListURL);
			url.setParam("id", StringHelpers::parseNumber(gameId))
				.setHost(_host)
				.setPort(_port);
			string s = url.toString();

			// Do Web Call (blocking + response)
			WebCallOpt wco;
			wco.url = s;
			wco.response = true;
			WebCallActPtr p = _webCaller.CallAndWait(wco);
			if (p->IsError()) {
				throw ChainedException(SOURCELOCATION, p->ErrorStr());
			}

			// Parse Response String
			s = p->ResponseStr();
			vector<string> lines;
			StringHelpers::explode(lines, s, "\n");

			// The response looks looks like:
			//
			// test-server;Dev Star;test-server.kaneva.com;25857;KanevaTray.exe;http://dev-patch.kaneva.com/4.2.0.1_a/;0;2000;1;;
			//                    "
			for (vector<string>::const_iterator iter = lines.begin(); iter != lines.end(); iter++) {
				ServerListItem item;
				item.parseWebResponse(*iter);
				if (find(item._serverName, excludedHosts) == 0)
					ret.push_back(item);
			};
		} catch (const URLParseException& pe) {
			ChainedException ce(SOURCELOCATION);
			ce.chain(pe);
			throw ce;
		}
		return ret;
	}

	WebServicesProxy& setServicesHost(const std::string& servicesHost) {
		_host = servicesHost;
		return *this;
	}

	WebServicesProxy& setServicesPort(unsigned short servicesPort) {
		_port = servicesPort;
		return *this;
	}

private:
	string _gameLoginURL;
	string _serverListURL;
	WebCaller _webCaller;
	string _host;
	unsigned short _port;
};
