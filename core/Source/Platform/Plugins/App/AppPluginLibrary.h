///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#ifdef APPPLUGINLIBRARY_EXPORTS
#define APPPLUGINLIBRARY_API __declspec(dllexport)
#else
#define APPPLUGINLIBRARY_API __declspec(dllimport)
#endif
