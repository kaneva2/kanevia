///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Plugins/App/ClanModelFacet.h"
#include "Plugins/App/ModelHelper.h"
using namespace mysqlpp;

namespace KEP {

ClanModelFacet::ClanModelFacet() :
		ClanModel(), ModelFacet("ClanModel"), _logger(Logger::getInstance(L"DDRModel"))

{}

ClanModelFacet::ClanModelFacet(const ClanModelFacet& other) :
		ClanModel(other), ModelFacet(other), _logger(other._logger) {}

ClanModelFacet::~ClanModelFacet() {}

ClanModelFacet& ClanModelFacet::operator=(const ClanModelFacet& rhs) {
	_logger = rhs._logger;
	return *this;
}

/**
 * @see ClanModel
 */
unsigned long ClanModelFacet::createClan(unsigned long gameId, unsigned long ownerId, const std::string& clanName) {
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;
	KGP_Connection* m_conn = &conn;
	unsigned long ret = 0;

	try {
		Transaction trans(*m_conn);
		Query query = m_conn->query();

		query << "INSERT INTO clans ( owner_id, clan_name ) VALUES (%0, %1q)";
		query.parse();
		LOG4CPLUS_TRACE(_logger, query.str(ownerId, clanName));

		NoExceptions ne(query);

		// NOTE mysql++ 2.3.2 got this confused with const char *, len, so we switched params around
		// SimpleResult res = query.execute( clanName, player->m_handle );
		SimpleResult res = query.execute((int)ownerId, clanName);

		int sqlErr = m_conn->errnum();

		if (sqlErr == ER_DUP_ENTRY)
			throw ModelException(SOURCELOCATION, "Clan already exists!");

		if (sqlErr != 0) {
			std::stringstream ss;
			ss << "Error creating clan [" << clanName << "]! " << m_conn->error();
			throw ModelException(SOURCELOCATION, ss.str());
		}

		ret = (ULONG)res.insert_id();

		addMemberInternal(gameId, ret, ownerId, conn);
		trans.commit();
	}
	STD_DB_EXCEPTION_HANDLING

	return ret;
}

/**
 * @see ClanModel
 */
ClanModelFacet& ClanModelFacet::deleteClansByOwner(unsigned long /* gameId */
	,
	unsigned long ownerId) {
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;
	KGP_Connection* m_conn = &conn;

	try {
		Transaction trans(*m_conn);
		Query query = m_conn->query();

		query << "DELETE FROM clans where owner_id = %0";
		query.parse();

		LOG4CPLUS_TRACE(_logger, query.str((int)ownerId));
		SimpleResult res = query.execute((int)ownerId);

		trans.commit();
	}
	STD_DB_EXCEPTION_HANDLING
	return *this;
}

/**
 * @see ClanModel
 */
ClanModelFacet& ClanModelFacet::addMember(unsigned long gameId, unsigned long clanId, unsigned long playerId) {
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;
	addMemberInternal(gameId, clanId, playerId, conn);
	return *this;
}

ClanModelFacet& ClanModelFacet::addMemberInternal(unsigned long /* gameId */
	,
	unsigned long clanId, unsigned long playerId, KGP_Connection& conn) {
	try {
		Transaction trans(conn);
		Query query = conn.query();
		query << "INSERT INTO clan_members (clan_id, player_id) VALUES ( %0, %1 ) ON DUPLICATE KEY UPDATE clan_id = %2";
		query.parse();

		LOG4CPLUS_TRACE(_logger, query.str((int)clanId, (int)playerId, (int)clanId));
		SimpleResult res = query.execute((int)clanId, (int)playerId, (int)clanId);

		trans.commit();
	}
	STD_DB_EXCEPTION_HANDLING
	return *this;
}

ClanModelFacet& ClanModelFacet::removeMember(unsigned long /* gameId */
	,
	unsigned long clanId, unsigned long playerId) {
	KGP_Connection::ConnectionPtr p = hostModel().connector()->getConnection(hostModel().connectorName());
	KGP_Connection& conn = *p;
	KGP_Connection* m_conn = &conn;
	try {
		Transaction trans(*m_conn);
		Query query = m_conn->query();
		query << "DELETE FROM clan_members where clan_id = %0 and player_id = %1";
		query.parse();

		LOG4CPLUS_TRACE(_logger, query.str((int)clanId, (int)playerId));
		SimpleResult res = query.execute((int)clanId, (int)playerId);
		trans.commit();
	}
	STD_DB_EXCEPTION_HANDLING
	return *this;
}

} // namespace KEP