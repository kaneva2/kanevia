///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Plugins\App\AppDataModel.h"

namespace KEP {

/**
 * Interface for ClanModel.  Note that gameId parameter has been
 * added to all relevant methods in preparation for Unified Model.
 */
class ClanModel {
public:
	ClanModel() {}
	ClanModel(const ClanModel& /* other */) {}
	virtual ~ClanModel() {}

	ClanModel& operator=(const ClanModel& /*rhs */) { return *this; }

	/**
     * Create a clan in the model.  Returns a reference to a 
     * Clan object.
     *
     * @param gameId            Id of game with which new clan will be associated. (currently unused)
     * @param ownerPlayerId     Id of owner of new clan.
     * @param clanName          The name of the new clan.
     * @return                  Id of newly created clan.
     * @throws ModelException in the event that the clan cannot be created.
     */
	virtual unsigned long createClan(unsigned long gameId, unsigned long ownerPlayerIid, const std::string& clanName) = 0;

	/**
     * Delete all clans for a particular owner.  Note that the prior semantic was "delete clan".
     * Effectively however, the logic was "delete clans owned by user", hence the name change here.
     * It's nonsensical, but it is consistent.
     *
     * N.B.: When we move this to unified, we will need to add a game id discriminator, also note
     * that prior, this method did not bother to remove the related clan_member records.  Unified
     * model will do this via a cascading constraint.
     *
     * @param gameId            Id of game with clans are associated. (currently unused)
     * @param ownerId           Id of the owning player.
     * @return Reference to self.
     * @throws ModelException
     */
	virtual ClanModel& deleteClansByOwner(unsigned long gameId, unsigned long ownerId) = 0;

	/**
     * Add a member identified by playerId to the clan identified by clanId.
     * @param gameId    Id of game with which clan is associated. (currently unused)
     * @param clanId    Id of the clan to which the user will be added.
     * @param playerId  Id of the user to be added to the clan
     * @return Reference to self.
     * @throws ModelException
     */
	virtual ClanModel& addMember(unsigned long gameId, unsigned long clanId, unsigned long playerId) = 0;

	/**
     * Remove member identified by playerId from clan identified by clanId
     * @param gameId    Id of game to which clan belongs.  Currently unused.
     * @param clanId    Id of clan from which player will be removed.
     * @param playerId  Id of player to be removed from clan
     * @return Reference to self.
     * @throws ModelException.
     */
	virtual ClanModel& removeMember(unsigned long gameId, unsigned long clanId, unsigned long playerId) = 0;
};

class ClanModelFacet : public ClanModel, public ModelFacet {
public:
	/**
     * Default constructor
     */
	ClanModelFacet();

	/**
     * Copy constructor
     */
	ClanModelFacet(const ClanModelFacet& other);

	/**
     * Dtor
     */
	virtual ~ClanModelFacet();

	/**
     * Assignment operator
     */
	virtual ClanModelFacet& operator=(const ClanModelFacet& rhs);

	/**
     * @see ClanModel
     */
	virtual unsigned long createClan(unsigned long gameId, unsigned long ownerPlayerIid, const std::string& clanName);
	/**
     *  @see ClanModel
     */
	virtual ClanModelFacet& deleteClansByOwner(unsigned long gameId, unsigned long clanId);

	/**
     * @see ClanModel
     */
	virtual ClanModelFacet& addMember(unsigned long gameId, unsigned long clanId, unsigned long playerId);

	/**
     * @see ClanModel
     */
	virtual ClanModelFacet& removeMember(unsigned long gameId, unsigned long clanId, unsigned long playerId);

private:
	ClanModelFacet& ClanModelFacet::addMemberInternal(unsigned long /* gameId */
		,
		unsigned long clanId, unsigned long playerId, KGP_Connection& conn);

	mutable log4cplus::Logger _logger;
};

} // namespace KEP