///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AppPluginLibrary.h"
#include "Common/include/kepcommon.h"
#include <log4cplus/logger.h>
#include "Common/KEPUtil/Plugin.h"
#include "Core/Util/ChainedException.h"
#include "Common/include/KEPConfig.h"
#include <map>
#include <set>

#include "Common/KEPUtil/ExtendedConfig.h"
#include "Event/DDR/DDR_S_GameData.h"
#include "ModelObjects.h"

namespace KEP {

CHAINED(ModelException)
CHAINED_DERIVATIVE(ModelException, FacetUnavailableException)

class IMySqlConnector;
class DataModel;
class DDRDBEvent;
class DDREvent;
class DDRPlayers;
class CSkillObjectList;
class CTitleObjectList;
class KGP_Connection;

/**
 * Naming interface.  ModelFacet
 */
class APPPLUGINLIBRARY_API ModelFacet {
public:
	ModelFacet(const std::string& name) :
			_name(name), _model(0) {}

	/**
     * Copy constructor
     *
     * @param other The facet to be copied from.
     */
	ModelFacet(const ModelFacet& other) :
			_name(other._name), _model(other._model) {}

	/**
     * Destructor
     */
	virtual ~ModelFacet() {}

	/**
     * Assignment operator
     * @param   rhs     Facet from which to assign.
     *
     * @returns reference to self
     */
	virtual ModelFacet& operator=(const ModelFacet& /* rhs */) { return *this; }

	virtual bool operator==(const ModelFacet& rhs) {
		if (&rhs == this)
			return true;
		if (rhs.isNull() && isNull())
			return true;
		return false;
	}

	const std::string& name() const { return _name; }

	DataModel& hostModel() {
		if (_model == 0)
			throw NullPointerException(SOURCELOCATION);
		return *_model;
	}

	ModelFacet& bindToModel(DataModel& model) {
		_model = &model;
		return *this;
	}

	virtual bool isNull() const { return false; }

private:
	DataModel* _model;
	std::string _name;
};

class ModelFacetRegistry {
public:
	ModelFacetRegistry() :
			_model(0) {}

	virtual ~ModelFacetRegistry() {
		_model = 0;
		_facetMap.clear();
	}

	ModelFacetRegistry& bindToModel(DataModel& model) {
		_model = &model;
		return *this;
	}

	ModelFacetRegistry& registerModelFacet(ModelFacet* facet) {
		if (_model == 0)
			throw ModelException(SOURCELOCATION, "Unbound facet registry!");
		if (facet == 0)
			throw NullPointerException(SOURCELOCATION);

		// Bind the facet to the model
		facet->bindToModel(*_model);
		_facetMap.insert(FacetMapEntryT(facet->name(), facet));
		return *this;
	}

	template <typename FacetT>
	FacetT& getFacet(const std::string& facetName) {
		FacetMapT::iterator iter = _facetMap.find(facetName);
		std::stringstream ss;
		if (iter == _facetMap.end()) {
			ss << "Facet with name [" << facetName << "] not found.";
			throw FacetUnavailableException(SOURCELOCATION, ss.str());
		}

		ModelFacet* modelFacet = iter->second;
		FacetT* pret = dynamic_cast<FacetT*>(modelFacet);
		if (pret == 0) {
			ss << "Facet with name [" << facetName << "] type not found.";
			throw FacetUnavailableException(SOURCELOCATION, ss.str());
		}
		if (_disabledFacets.find(modelFacet->name()) != _disabledFacets.end()) {
			ss << "Facet with name [" << facetName << "] disabled.";
			throw FacetUnavailableException(SOURCELOCATION, ss.str());
		}
		return *pret;
	}

	template <typename FacetT>
	FacetT* getFacetNoThrow(const std::string& facetName) {
		try {
			FacetT& facet = getFacet<FacetT>(facetName);
			return &facet;
		} catch (const FacetUnavailableException&) {
			// fall through and return 0
		}
		return 0;
	}

	ModelFacetRegistry& disableFacet(const std::string& facetName) {
		_disabledFacets.insert(facetName);
		return *this;
	}

private:
	typedef std::pair<std::string, ModelFacet*> FacetMapEntryT;
	typedef std::map<std::string, ModelFacet*> FacetMapT;
	FacetMapT _facetMap;
	DataModel* _model;
	std::set<std::string> _disabledFacets;
};

/**
 * Interface definition for our data layer (model)
 * Note that this interface features "NoThrow" versions of each method.  These
 * methods should be considered to be deprecated and only exist for the
 * purpose of easing integration with existing usage which is not expecting
 * a throw.
 *
 * Note that ddr_id is synonymous with game_id in the context ofthe legacy DDR logic.  
 * This old game_id is inconsistent with the larger meaning of game_id (app_id) and
 * has been renamed for clarity.
 */
class APPPLUGINLIBRARY_API DataModel {
public:
	typedef std::shared_ptr<DataModel> ptr_t;

	CHAINED(Exception);

	DataModel() {}
	DataModel(const DataModel& /* other */) {}
	DataModel& operator=(const DataModel& /* other */) { return *this; }
	virtual ~DataModel() {}

	virtual DataModel& setConnector(IMySqlConnector* connector) = 0;
	virtual DataModel& setConnectorName(const std::string& connectorName) = 0;
	virtual IMySqlConnector* connector() = 0;
	virtual const std::string& connectorName() = 0;

	/**
     * Called to fill out a player's titles Currently used in 3D Apps to get 
     * player titles for DDR game.
     *
     * @param playerId  Id of player.
     * @param titles    titles availabe to player with playerId
     * @param title     default title of player with playerId.
     * @returns         true if player data found, else false.
     */
	virtual bool get3DAppPlayerTitles(int playerId, KEP::CTitleObjectList*& titles, std::string& title) = 0;

	/**
     * Set the game id associated with this server.
     */
	virtual DataModel& setGameId(unsigned long gameId) = 0;

	/**
     * Acquire the game id associated with this server.
     */
	virtual unsigned long gameId() = 0;
	virtual ModelFacetRegistry& registry() = 0;

private:
};

/**
 * Initial abstraction
 */
class APPPLUGINLIBRARY_API CommonModel : public DataModel {
public:
	CommonModel() :
			_gameId(0xffff) {}
	virtual ~CommonModel() {}

	CommonModel& setConnector(IMySqlConnector* connector) {
		_connector = connector;
		return *this;
	}
	CommonModel& setConnectorName(const std::string& connectorName) {
		_connectorName = connectorName;
		return *this;
	}

	IMySqlConnector* connector() {
		return _connector;
	}
	const std::string& connectorName() { return _connectorName; }
	virtual unsigned long gameId() { return _gameId; }
	virtual CommonModel& setGameId(unsigned long gameId) {
		_gameId = gameId;
		return *this;
	}

private:
	IMySqlConnector* _connector;
	std::string _connectorName;
	unsigned long _gameId;
};

class APPPLUGINLIBRARY_API AppModelPluginFactory : public AbstractPluginFactory {
public:
	AppModelPluginFactory();
	AppModelPluginFactory(KEP::KEPConfig* config);
	virtual ~AppModelPluginFactory();
	PluginPtr createPlugin(const KEP::TIXMLConfig::Item& config) const;

private:
	KEP::KEPConfig* _config;
};

class APPPLUGINLIBRARY_API NullDataModel : public DataModel {
public:
	NullDataModel() {}
	NullDataModel(const DataModel& /* other */) {}
	NullDataModel& operator=(const DataModel& /* other */) { return *this; }
	virtual ~NullDataModel() {}

	virtual DataModel& setConnector(IMySqlConnector* /* connector */) { return *this; }
	virtual DataModel& setConnectorName(const std::string& /* connectorName*/) { return *this; }
	virtual IMySqlConnector* connector() { throw UnsupportedException(SOURCELOCATION); }
	virtual const std::string& connectorName() { throw UnsupportedException(SOURCELOCATION); }
	virtual bool get3DAppPlayerTitles(int /* playerId*/,
		KEP::CTitleObjectList*& /* titles*/,
		std::string& /* title*/) { throw UnsupportedException(SOURCELOCATION); }

	virtual NullDataModel& setGameId(unsigned long) { throw UnsupportedException(SOURCELOCATION); }
	virtual unsigned long gameId() { throw UnsupportedException(SOURCELOCATION); }
	virtual ModelFacetRegistry& registry() { throw UnsupportedException(SOURCELOCATION); }
};

/**
 * Going forward, whenever a database exception is raised, we should be re-throwing the exception
 * in a consistent manner so that clients of this object model can perform any context specific
 * handling, instead of forcing the response at this level.  E.g. Some calls should simply
 * log, some may need to treated as critical.  To that end DataModel throws exceptions.
 * 
 * That being said however, when integrating with existing code, which is not prepared to handle
 * these exceptions, we should suppress these exceptions.  This class provides that functionality
 * by encapsulating another DataModel implementation, forward requests to this implementation
 * and then catching any exceptions.
 */
class APPPLUGINLIBRARY_API SuppressedModel : public NullDataModel {
public:
	SuppressedModel(DataModel& impl);
	virtual ~SuppressedModel();
	virtual SuppressedModel& operator=(const SuppressedModel& other);

	virtual bool get3DAppPlayerTitles(int playerId, KEP::CTitleObjectList*& titles, std::string& title);

	virtual ModelFacetRegistry& registry() { return _impl.registry(); }

private:
	DataModel& _impl;
};

} // namespace KEP