///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Tools/GeometricTools/GteIntrLine3Triangle3.h"
#include "Core/Util/Parameter.h"

namespace KEP {
template <typename T>
inline bool IntersectLineTriangle(const Vector<3, T>& ptLineOrigin, const Vector<3, T>& vLineDir,
	const Vector<3, T>& ptTriangle1, const Vector<3, T>& ptTriangle2, const Vector<3, T>& ptTriangle3,
	T* pLineParam = nullptr, T* pTriBaryA = nullptr, T* pTriBaryB = nullptr) {
	gte::FIQuery<T, gte::Line3<T>, gte::Triangle3<T>> query;
	gte::Line3<T> gte_line(
		reinterpret_cast<const gte::Vector<3, T>&>(ptLineOrigin),
		reinterpret_cast<const gte::Vector<3, T>&>(vLineDir));
	gte::Triangle<3, T> gte_tri(
		reinterpret_cast<const gte::Vector<3, T>&>(ptTriangle1),
		reinterpret_cast<const gte::Vector<3, T>&>(ptTriangle2),
		reinterpret_cast<const gte::Vector<3, T>&>(ptTriangle3));
	auto result = query(gte_line, gte_tri);
	if (pTriBaryA)
		*pTriBaryA = result.triangleBary[1];
	if (pTriBaryB)
		*pTriBaryB = result.triangleBary[2];
	if (pLineParam)
		*pLineParam = result.parameter;
	return result.intersect;
}

template <typename T>
inline bool IntersectTriangleLine(
	const Vector<3, T>& ptTriangle1, const Vector<3, T>& ptTriangle2, const Vector<3, T>& ptTriangle3,
	const Vector<3, T>& ptLineOrigin, const Vector<3, T>& vLineDir,
	T* pTriBaryA = nullptr, T* pTriBaryB = nullptr, T* pLineParam = nullptr) {
	return IntersectLineTriangle(ptLineOrigin, vLineDir, ptTriangle1, ptTriangle2, ptTriangle3, pLineParam, pTriBaryA, pTriBaryB);
}

// Calculates point of closest approach of two lines.
// Returns false if lines are parallel. (In this case there is no unique point of closest approach).
// If the lines intersect, the returned value  will be the intersection point.
// Lines can be any dimension >= 2
// The first version of the function returns the intersection point as a relative distance from the line origin.
template <size_t N, typename T>
inline bool ClosestPointOnLines(const Vector<N, T>& ptLine1, const Vector<N, T>& vLineDir1, const Vector<N, T>& ptLine2, const Vector<N, T>& vLineDir2,
	std::common_type_t<T>* pT1, std::common_type_t<T>* pT2) {
	// Parametric lines:
	// L1(t1) = ptLine1 + vLineDir1 * t1
	// L2(t2) = ptLine2 + vLineDir1 * t2
	// Distance squared between L1 and L2 is dot product of their difference: (L2-L1) . (L2-L1)
	// Minimize this distance by taking partial derivatives with respect to t1 and t2 and setting to zero.
	// This will give us a system of two equations in two variables to solve.
	T fDir1DotDir2 = vLineDir1.Dot(vLineDir2);
	T fDir1DotDir1 = vLineDir1.Dot(vLineDir1);
	T fDir2DotDir2 = vLineDir2.Dot(vLineDir2);
	// (After dropping a factor of two from each term)
	// Equation1:   fDir1DotDir1 * t1  +  -fDir1DotDir2 * t2  =  fDir1DotPointDiff
	// Equation2:  -fDir1DotDir2 * t1  +   fDir2DotDir2 * t2  = -fDir2DotPointDiff
	T fSquaredLengthsProduct = fDir1DotDir1 * fDir2DotDir2;
	T fDet = fSquaredLengthsProduct - Square(fDir1DotDir2);
	static const T tolerance = NumericTraits<T>::CosineSquaredTolerance();
	if (fDet <= tolerance * fSquaredLengthsProduct)
		return false;

	T fInverseDet = 1 / fDet;
	Vector<N, T> vPointDiff = ptLine2 - ptLine1;
	T fDir1DotPointDiff = vLineDir1.Dot(vPointDiff);
	T fDir2DotPointDiff = vLineDir2.Dot(vPointDiff);
	if (pT1)
		*pT1 = (fDir1DotPointDiff * fDir2DotDir2 - fDir1DotDir2 * fDir2DotPointDiff) * fInverseDet;
	if (pT2)
		*pT2 = (fDir1DotPointDiff * fDir1DotDir2 - fDir1DotDir1 * fDir2DotPointDiff) * fInverseDet;

	return true;
}

// Version of ClosestPointOnLines that returns points instead of relative distance from line origin.
template <size_t N, typename T>
inline bool ClosestPointOnLines(const Vector<N, T>& ptLine1, const Vector<N, T>& vLineDir1, const Vector<N, T>& ptLine2, const Vector<N, T>& vLineDir2,
	Vector<N, T>* pPointOnLine1, Vector<N, T>* pPointOnLine2) {
	T t1, t2;
	if (!ClosestPointOnLines(ptLine1, vLineDir1, ptLine2, vLineDir2, &t1, &t2))
		return false;

	if (pPointOnLine1)
		*pPointOnLine1 = ptLine1 + t1 * vLineDir1;
	if (pPointOnLine2)
		*pPointOnLine2 = ptLine2 + t2 * vLineDir2;
	return true;
}

// Optimized version of ClosestPointOnLines for when one of the lines is one of the coordinate axes.
template <size_t N, typename T>
inline bool ClosestPointOnAxis(size_t iAxis, const Vector<N, T>& ptLine, const Vector<N, T>& vLineDir, std::common_type_t<T>* pT1, std::common_type_t<T>* pT2) {
	T fDir1DotDir2 = vLineDir[iAxis];
	T fDir2DotDir2 = vLineDir.Dot(vLineDir);
	T fSquaredLengthsProduct = fDir2DotDir2;
	T fDet = fSquaredLengthsProduct - Square(vLineDir[iAxis]);
	static const T tolerance = NumericTraits<T>::CosineSquaredTolerance();
	if (fDet <= tolerance * fSquaredLengthsProduct)
		return false;

	T fInverseDet = 1 / fDet;
	T fDir1DotPointDiff = ptLine[iAxis];
	T fDir2DotPointDiff = vLineDir.Dot(ptLine);
	if (pT1)
		*pT1 = (fDir1DotPointDiff * fDir2DotDir2 - fDir1DotDir2 * fDir2DotPointDiff) * fInverseDet;
	if (pT2)
		*pT2 = (fDir1DotPointDiff * fDir1DotDir2 - fDir2DotPointDiff) * fInverseDet;

	return true;
}

// Version of ClosestPointOnAxis that returns points on the lines.
template <size_t N, typename T>
inline bool ClosestPointOnAxis(size_t iAxis, const Vector<N, T>& ptLine, const Vector<N, T>& vLineDir,
	Vector<N, T>* pPointOnAxis, Vector<N, T>* pPointOnLine) {
	T t1, t2;
	if (!ClosestPointOnAxis(iAxis, ptLine, vLineDir, &t1, &t2))
		return false;

	if (pPointOnAxis) {
		for (size_t i = 0; i < iAxis; ++i)
			(*pPointOnAxis)[i] = 0;
		(*pPointOnAxis)[iAxis] = t1;
		for (size_t i = iAxis + 1; i < N; ++i)
			(*pPointOnAxis)[i] = 0;
	}
	if (pPointOnLine)
		*pPointOnLine = ptLine + t2 * vLineDir;
	return true;
}

template <typename T>
inline bool SolveLinearSystem(const Vector<2, T>& coeff0, const Vector<2, T>& coeff1,
	const Vector<2, T>& rhs, Out<Vector<2, T>> result) {
	T det = coeff0[0] * coeff1[1] - coeff0[1] * coeff1[0];
	if (det == 0)
		return false;
	T recip_det = Reciprocal(det);
	T num0 = rhs[0] * coeff1[1] - rhs[1] * coeff1[0];
	T num1 = coeff0[0] * rhs[1] - coeff0[1] * rhs[0];
	result.get()[0] = num0 * recip_det;
	result.get()[1] = num1 * recip_det;
	return true;
}

template <
	size_t TriPointDimension,
	size_t TestPointDimension,
	typename T>
inline bool BarycentricCoordinatesProjected(
	const Vector<TestPointDimension, T>& ptTest,
	size_t iTestAxis0, size_t iTestAxis1,
	const Vector<TriPointDimension, T>& pt0, const Vector<TriPointDimension, T>& pt1, const Vector<TriPointDimension, T>& pt2,
	size_t iTriAxis0, size_t iTriAxis1,
	Out<Vector<2, T>> baryCoords) {
	// Vectors from triangle point 0 to to point 1, 2, and test point respectively.
	Vector<2, T> v0_1(pt1[iTriAxis0] - pt0[iTriAxis0], pt1[iTriAxis1] - pt0[iTriAxis1]);
	Vector<2, T> v0_2(pt2[iTriAxis0] - pt0[iTriAxis0], pt2[iTriAxis1] - pt0[iTriAxis1]);
	Vector<2, T> v0_pt(ptTest[iTestAxis0] - pt0[iTriAxis0], ptTest[iTestAxis1] - pt0[iTriAxis1]);

	if (!SolveLinearSystem(v0_1, v0_2, v0_pt, baryCoords))
		return false;

	return true;
}

// Barycentric coordinates as projected onto 2 arbitrary coordinate axes.
template <
	size_t iTestAxis0, size_t iTestAxis1, size_t TestPointDimension,
	size_t iTriAxis0, size_t iTriAxis1, size_t TriPointDimension,
	typename T>
inline bool BarycentricCoordinatesProjected(const Vector<TestPointDimension, T>& ptTest, const Vector<TriPointDimension, T>& pt0,
	const Vector<TriPointDimension, T>& pt1, const Vector<TriPointDimension, T>& pt2,
	Out<Vector<2, T>> baryCoords) {
	// Vectors from triangle point 0 to to point 1, 2, and test point respectively.
	Vector<2, T> v0_1(pt1[iTriAxis0] - pt0[iTriAxis0], pt1[iTriAxis1] - pt0[iTriAxis1]);
	Vector<2, T> v0_2(pt2[iTriAxis0] - pt0[iTriAxis0], pt2[iTriAxis1] - pt0[iTriAxis1]);
	Vector<2, T> v0_pt(ptTest[iTestAxis0] - pt0[iTriAxis0], ptTest[iTestAxis1] - pt0[iTriAxis1]);

	if (!SolveLinearSystem(v0_1, v0_2, v0_pt, baryCoords))
		return false;

	return true;
}

template <
	size_t iTestAxis0, size_t iTestAxis1, size_t TestPointDimension,
	size_t iTriAxis0, size_t iTriAxis1, size_t TriPointDimension,
	typename T>
inline bool PointInTriangle(const Vector<TestPointDimension, T>& ptTest, const Vector<TriPointDimension, T>& pt0,
	const Vector<TriPointDimension, T>& pt1, const Vector<TriPointDimension, T>& pt2) {
	Vector<2, T> result;
	if (!BarycentricCoordinatesProjected<
			iTestAxis0, iTestAxis1, TestPointDimension,
			iTriAxis0, iTriAxis1, TriPointDimension,
			T>(ptTest, pt0, pt1, pt2, out(result))) {
		return false;
	}

	if (result[0] < 0 || result[1] < 0 || result[0] + result[1] > 1)
		return false; // Point outside triangle.

	return true;
}

template <
	size_t iTriAxis0, size_t iTriAxis1, size_t TriPointDimension,
	typename T>
inline bool PointInTriangle(const Vector<2, T>& ptTest, const Vector<TriPointDimension, T>& pt0,
	const Vector<TriPointDimension, T>& pt1, const Vector<TriPointDimension, T>& pt2) {
	return PointInTriangle<0, 1, 2, iTriAxis0, iTriAxis1, TriPointDimension, T>(ptTest, pt0, pt1, pt2);
}

template <size_t N, typename T>
inline bool BarycentricCoordinates(const Vector<N, T>& ptTest, const Vector<N, T>& pt0,
	const Vector<N, T>& pt1, const Vector<N, T>& pt2,
	Out<Vector<2, T>> baryCoords) {
	Vector<N, T> v0 = pt1 - pt0;
	Vector<N, T> v1 = pt2 - pt0;
	Vector<N, T> vt = ptTest - pt0;
	T v0_dot_v1 = v0.Dot(v1);
	Vector<2, T> sCoeff(v0.Dot(v0), v0_dot_v1);
	Vector<2, T> tCoeff(v0_dot_v1, v1.Dot(v1));
	Vector<2, T> rhs(v0.Dot(vt), v1.Dot(vt));

	if (!SolveLinearSystem(sCoeff, tCoeff, rhs, baryCoords))
		return false;

	return true;
}

template <size_t N, typename T>
inline void DominantAxes(const Vector<N, T>& v0, const Vector<N, T>& v1, Out<size_t> iDominantAxis0, Out<size_t> iDominantAxis1) {
	// Find axis of v0 with largest magnitude
	size_t iAxis0 = 0;
	T absAxis0 = std::abs(v0[iAxis0]);
	for (size_t i = 1; i < N; ++i) {
		T absThis = std::abs(v0[i]);
		if (absThis > absAxis0) {
			iAxis0 = i;
			absAxis0 = absThis;
		}
	}

	// Find axis of vector perpendicular to v0 with largest magnitude
	Vector<N, T> vPerp = v0.Dot(v0) * v1 - v0.Dot(v1) * v0;
	vPerp[iAxis0] = 0; // Can't re-use axis 0.
	size_t iAxis1 = 0;
	T absAxis1 = std::abs(vPerp[iAxis1]);
	for (size_t i = 1; i < N; ++i) {
		T absThis = std::abs(vPerp[i]);
		if (absThis > absAxis1) {
			iAxis1 = i;
			absAxis1 = absThis;
		}
	}

	iDominantAxis0.set(iAxis0);
	iDominantAxis1.set(iAxis1);
}

template <typename T>
inline void DominantAxes(const Vector<3, T>& v0, const Vector<3, T>& v1, Out<size_t> iDominantAxis0, Out<size_t> iDominantAxis1) {
	Vector<3, T> vNormal = v0.Cross(v1);
	size_t iAxis2 = 0;
	T absAxis2 = std::abs(vNormal[iAxis2]);
	for (size_t i = 1; i < 3; ++i) {
		T absThis = std::abs(vNormal[i]);
		if (absThis > absAxis2) {
			iAxis2 = i;
			absAxis2 = absThis;
		}
	}

	static size_t aMod[4] = { 1, 2, 0, 1 };
	iDominantAxis0.set(aMod[iAxis2]);
	iDominantAxis1.set(aMod[iAxis2 + 1]);
}

template <typename T>
inline void DominantAxes(const Vector<2, T>& v0, const Vector<2, T>& v1, Out<size_t> iDominantAxis0, Out<size_t> iDominantAxis1) {
	iDominantAxis0.set(0);
	iDominantAxis1.set(1);
}

// Determine the two dominant axes of the triangle, then calculate the barycentric coordinates
// after projecting to those axes.
// More accurate than BarycentricCoordinates(), but slower if the dimension is large.
// Should be faster for N <= 3, so we will overload BarycentricCoordinates to call this for those dimensions.
template <size_t N, typename T>
inline bool BarycentricCoordinatesPrecise(const Vector<N, T>& ptTest, const Vector<N, T>& pt0,
	const Vector<N, T>& pt1, const Vector<N, T>& pt2, Out<Vector<2, T>> baryCoords) {
	Vector<N, T> v0 = pt1 - pt0;
	Vector<N, T> v1 = pt2 - pt0;

	size_t iAxis0, iAxis1;
	DominantAxes(v0, v1, out(iAxis0), out(iAxis1));

	// Calculate barycentric coordinates of points projected to these major axes.
	Vector<2, T> v0_1(pt1[iAxis0] - pt0[iAxis0], pt1[iAxis1] - pt0[iAxis1]);
	Vector<2, T> v0_2(pt2[iAxis0] - pt0[iAxis0], pt2[iAxis1] - pt0[iAxis1]);
	Vector<2, T> v0_pt(ptTest[iAxis0] - pt0[iAxis0], ptTest[iAxis1] - pt0[iAxis1]);

	if (!SolveLinearSystem(v0_1, v0_2, v0_pt, baryCoords))
		return false;

	return true;
}

// BarycentricCoordinatesPrecise is faster for N==2
template <typename T>
inline bool BarycentricCoordinates(const Vector<2, T>& ptTest, const Vector<2, T>& pt0,
	const Vector<2, T>& pt1, const Vector<2, T>& pt2, Out<Vector<2, T>> baryCoords) {
	return BarycentricCoordinatesPrecise(ptTest, pt0, pt1, pt2, baryCoords);
}

// BarycentricCoordinatesPrecise is faster for N==3
template <typename T>
inline bool BarycentricCoordinates(const Vector<3, T>& ptTest, const Vector<3, T>& pt0,
	const Vector<3, T>& pt1, const Vector<3, T>& pt2, Out<Vector<2, T>> baryCoords) {
	return BarycentricCoordinatesPrecise(ptTest, pt0, pt1, pt2, baryCoords);
}

#if 0 // Work in progress. Untested.
template<typename T>
inline bool IntersectSegmentBox(const Vector<3,T>& ptSeg0, const Vector<3,T>& ptSeg1,
	const Vector<3,T>& ptBoxMin, const Vector<3,T>& ptBoxMax)
{
	// Numerator and denominator of t0 and t1 end points of segment.
	T t0_n = 0;
	T t0_d = 1;
	T t1_n = 1;
	T t1_d = 1;
	Vector3<T> ptBoxCenter = T(0.5) * (ptBoxMin + ptBoxMax);
	Vector3<T> vBoxSize = ptBoxMax - ptBoxMin;
	Vector3<T> vBoxHalfSize = T(0.5) * vBoxSize;
	Vector3<T> ptSegRel = { ptSeg0 - ptBoxCenter, ptSeg1 - ptBoxCenter };
	for( size_t iAxis = 0; iAxis < 3; ++iAxis ) {
		for( int i = -1; i <= 1; i += 2 ) {
			T rel_dist0 = i * ptSegRel[0][iAxis] - vBoxHalfSize[iAxis];
			T rel_dist1 = i * ptSegRel[1][iAxis] - vBoxHalfSize[iAxis];
			T d_n = rel_dist0_n - rel_dist1_n;
			if( rel_dist0 * rel_dist1 < 0 ) {
				// Plane intersection.
				if( rel_dist0 > 0 ) {
					// Point zero is on positive side of plane.
					T d = rel_dist0 - rel_dist1;
					if( rel_dist0 * t0_d > t0_n * d ) {
						t0_n = rel_dist0;
						t0_d = d;
					}
				} else {
					// Point one is on positive side of plane.
					T d = rel_dist1 - rel_dist0;
					if( rel_dist0 * t1_d < t1_n * d ) {
						t1_n = rel_dist0;
						t1_d = d;
					}
				}
			}
		}
	}

	//if( t0 <= t1 ) {
	//if( t0_n / t0_d <= t1_n / t1_d ) {
	if( t0_n * t1_d > t1_n * t0_d )
		return false;
#if 0 //Calculate clipped segment
	T t0 = t0_n / t0_d;
	T t1 = t1_n / t1_d;
	Vector3<T> vSlope = ptSeg1 - ptSeg0;
	if( pptNewSeg0 )
		pptNewSeg0 = ptSeg0 + t0 * vSlope;
	if( pptNewSet1 )
		pptNewSeg1 = ptSeg1 + t1 * vSlope;
#endif
	return true;
}
#endif

// Use separating axis theorem to test for intersection.
// Test planes of box, plane of triangle, and each plane that is parallel to a pair of edges (with 1 from box and 1 from triangle)
// Assumes ptBoxMax >= ptBoxMin
template <typename T>
inline bool IntersectTriangleBox(const Vector<3, T>& ptTri0, const Vector<3, T>& ptTri1, const Vector<3, T>& ptTri2,
	const Vector<3, T>& ptBoxMin, const Vector<3, T>& ptBoxMax) {
	// No intersection if bounding boxes don't intersect.
	for (size_t i = 0; i < 3; ++i) {
		if (ptTri0[i] < ptBoxMin[i]) {
			if (ptTri1[i] < ptBoxMin[i] && ptTri2[i] < ptBoxMin[i])
				return false;
		} else {
			if (ptTri0[i] > ptBoxMax[i] && ptTri1[i] > ptBoxMax[i] && ptTri2[i] > ptBoxMax[i])
				return false;
		}
	}

	Vector3<T> ptBoxCenter = T(0.5) * (ptBoxMin + ptBoxMax);
	Vector3<T> vBoxSize = ptBoxMax - ptBoxMin;
	Vector3<T> vBoxHalfSize = T(0.5) * vBoxSize;

	Vector3<T> avTriEdges[3]; // Initialize the array elements as needed.
	avTriEdges[0] = ptTri1 - ptTri0;
	avTriEdges[1] = ptTri2 - ptTri1;
	Vector3<T> vTriNormal = avTriEdges[0].Cross(avTriEdges[1]);
	Vector3<T> aptTri[3]; // Initialize the array elements as needed.
	aptTri[0] = ptTri0 - ptBoxCenter;

	// No intersection if box is entirely on one side of the plane of the triangle.
	T triDist = vTriNormal.Dot(aptTri[0]);
	T boxDist = 0;
	for (size_t i = 0; i < 3; ++i)
		boxDist += std::abs(vTriNormal[i] * vBoxHalfSize[i]);
	if (boxDist < std::abs(triDist))
		return false;

	aptTri[1] = ptTri1 - ptBoxCenter;
	aptTri[2] = ptTri2 - ptBoxCenter;

	// Move triangle to be relative to center of box.
	avTriEdges[2] = ptTri0 - ptTri2;
	// No intersection if no projection onto any of the coordinate planes intersect.
	for (size_t iAxis = 0; iAxis < 3; ++iAxis) { // For each axis
		size_t idx0 = (iAxis + 1) % 3;
		size_t idx1 = (iAxis + 2) % 3;
		for (size_t j = 0; j < 3; ++j) { // For each triangle edge
			boxDist = std::abs(avTriEdges[j][idx0] * vBoxHalfSize[idx1]) + std::abs(avTriEdges[j][idx1] * vBoxHalfSize[idx0]);
			T aTriDist[3];
			for (size_t k = 0; k < 3; ++k) { // For each triangle point
				aTriDist[k] = avTriEdges[j][idx1] * aptTri[k][idx0] - avTriEdges[j][idx0] * aptTri[k][idx1];
			}
			if (aTriDist[0] < 0) {
				aTriDist[0] = -aTriDist[0];
				aTriDist[1] = -aTriDist[1];
				aTriDist[2] = -aTriDist[2];
			}
			if (boxDist < aTriDist[0] && boxDist < aTriDist[1] && boxDist < aTriDist[2])
				return false;
		}
	}

	return true;
}

} // namespace KEP