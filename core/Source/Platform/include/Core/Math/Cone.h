///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Vector.h"
namespace KEP {

template <typename T>
class Cone {
public:
	Cone() {}
	Cone(const Vector3<T>& ptApex, const Vector3<T>& vDir, T fCosine) :
			m_ptApex(ptApex), m_vDir(vDir), m_fCosine(fCosine) {}

	void SetApex(const Vector3<T>& pt) { m_ptApex = pt; }
	void SetDirection(const Vector3<T>& vDir) { m_vDir = vDir; }

	const Vector3<T>& GetApex() const { return m_ptApex; }
	const Vector3<T>& GetDirection() const { return m_vDir; }
	T GetCosine() const { return m_fCosine; }

	bool IsOutside(const Vector3<T>& pt) const {
		return !CosineGreaterThan(pt, m_fCosine);
	}
	bool IsInside(const Vector3<T>& pt) const {
		return CosineGreaterThan(pt, m_fCosine);
	}

	//
	bool IsOutside(const Vector3<T>& pt, T fToleranceAngleCosine) const {
		Vector3<T> vDir(pt - m_ptApex);

		T fTestCosineScaled = m_vDir.Dot(vDir);
		T dirLenSq = vDir.LengthSquared();
		T coneDirLenSq = m_vDir.LengthSquared();

		if (false) // Optional: Rescale to avoid overflow.
		{
			int iNormalizationScale = -ilogb(dirLenSq) / 2;
			fTestCosineScaled = scalbn(fTestCosineScaled, iNormalizationScale);
			dirLenSq = scalbn(dirLenSq, 2 * iNormalizationScale);
		}

		T fScale = dirLenSq * coneDirLenSq;
		T fToleranceAngleCosineSquared = Square(fToleranceAngleCosine);
		T fToleranceAngleSineSquared = 1 - fToleranceAngleCosineSquared;
		T fToleranceAngleSine = Sqrt(fToleranceAngleSineSquared);
		T fConeCosine = m_fCosine;
		T fConeCosineSquared = Square(fConeCosine);
		T fConeSineSquared = 1 - fConeCosineSquared;
		T fConeSine = Sqrt(fConeSineSquared);
		if (fToleranceAngleCosine * fConeSine + fConeCosine * fToleranceAngleSine < 0) // Sine of angle sum is positive
			return false; // Cone encloses entire space when including tolerance.
		T fConePlusToleranceCosine = fConeCosine * fToleranceAngleCosine - fConeSine * fToleranceAngleSine;
		//if( fTestCosineScaled * ReciprocalSqrt(fScale) < fConePlusToleranceCosine )
		if (fTestCosineScaled * std::abs(fTestCosineScaled) < fScale * fConePlusToleranceCosine * std::abs(fConePlusToleranceCosine))
			return true;
		return false;
	}

	// Exclusive of border, but may be affected by roundoff.
	bool ContainsInside(const Vector3<T>& pt) const {
		return CosineGreaterThan(pt, m_fCosine);
	}
	bool IsEmpty() const { return m_fCosine > 1; }
	void MakeEmpty() {
		m_ptApex.Set(0, 0, 0);
		m_vDir.Set(0, 0, 0);
		m_fCosine = 2;
	}

private:
	bool CosineGreaterThan(const Vector3<T>& pt, float fCosineMax) const {
		Vector3<T> vDir(pt - m_ptApex);
		T dot = m_vDir.Dot(vDir);
		return dot * std::abs(dot) > fCosineMax * std::abs(fCosineMax) * m_vDir.LengthSquared() * vDir.LengthSquared();
	}

private:
	Vector3<T> m_ptApex;
	Vector3<T> m_vDir;
	T m_fCosine;
};

} //namespace KEP
