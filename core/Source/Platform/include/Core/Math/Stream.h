///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>
#include "Vector.h"

namespace KEP {
template <size_t N, typename T>
inline std::istream& operator>>(std::istream& istrm, KEP::Vector<N, T>& v) {
	int ch;
	bool bHasParentheses = false;
	ch = istrm.peek();
	if (ch == '(') {
		istrm.get();
		bHasParentheses = true;
	}

	for (size_t i = 0; i < N; ++i) {
		if (i != 0) {
			ch = istrm.get();
			if (ch != ',') {
				istrm.setstate(std::ios::failbit);
				return istrm;
			}
		}
		istrm >> v[i];
	}

	if (bHasParentheses) {
		ch = istrm.get();
		if (ch != ')') {
			istrm.setstate(std::ios::failbit);
			return istrm;
		}
	}
	return istrm;
}

template <size_t N, typename T>
inline std::ostream& operator<<(std::ostream& ostrm, const KEP::Vector<N, T>& v) {
	ostrm << '(';
	for (size_t i = 0; i < N; ++i) {
		if (i != 0)
			ostrm << ',';
		ostrm << v[i];
	}
	ostrm << ')';
	return ostrm;
}
} // namespace KEP
