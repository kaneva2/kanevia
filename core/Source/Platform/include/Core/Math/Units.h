///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

template <size_t N, typename T>
class Vector;
template <size_t M, size_t N, typename T>
class Matrix;
class Simd4f;
class Simd2d;

namespace KEPMathDetail {
template <typename T>
struct FloatingPointTypeOf;

template <>
struct FloatingPointTypeOf<float> { typedef float type; };
template <>
struct FloatingPointTypeOf<double> { typedef double type; };
template <>
struct FloatingPointTypeOf<long double> { typedef long double type; };
template <>
struct FloatingPointTypeOf<Simd4f> { typedef Simd4f type; };
template <>
struct FloatingPointTypeOf<Simd2d> { typedef Simd2d type; };
template <size_t N, typename T>
struct FloatingPointTypeOf<Vector<N, T>> : FloatingPointTypeOf<T> {};

template <typename T>
using FloatingPointTypeOf_t = typename FloatingPointTypeOf<T>::type;

template <typename T>
static inline /*constexpr*/ T PoundsToKilogramsFactor() {
	static /*constexpr*/ T pounds_to_kilograms = static_cast<T>(0.45359237);
	return pounds_to_kilograms;
}

template <typename T>
static inline /*constexpr*/ T KilogramsToPoundsFactor() {
	static /*constexpr*/ T kilograms_to_pounds = static_cast<T>(1.0 / 0.45359237);
	return kilograms_to_pounds;
}

template <typename T>
static inline /*constexpr*/ T InchesToCentimetersFactor() {
	static /*constexpr*/ T inches_to_centimeters = static_cast<T>(2.54);
	return inches_to_centimeters;
}

template <typename T>
static inline /*constexpr*/ T CentimetersToInchesFactor() {
	static /*constexpr*/ T centimeters_to_inches = static_cast<T>(1.0 / 2.54);
	return centimeters_to_inches;
}

template <typename T>
static inline /*constexpr*/ T FeetToMetersFactor() {
	static /*constexpr*/ T feet_to_meters = T(.3048); //12.0 * 2.54 / 100.0
	return feet_to_meters;
}

template <typename T>
static inline constexpr T MetersToFeetFactor() {
	return static_cast<T>(100.0 / (12.0 * 2.54));
	/*
		static constexpr T meters_to_feet = static_cast<T>(100.0 / (12.0 * 2.54));
		return meters_to_feet;
		*/
}

template <typename T>
static inline /*constexpr*/ T WattsToHorsepowerFactor() {
	static /*constexpr*/ T watts_to_horsepower = T(1.0 / 745.69987158227);
	return watts_to_horsepower;
}

template <typename T>
static inline /*constexpr*/ T HorsepowerToWattsFactor() {
	static /*constexpr*/ T horsepower_to_watts = T(745.69987158227);
	return horsepower_to_watts;
}

// Kilometers per hour to meters per second
template <typename T>
static inline /*constexpr*/ T KPHToMPSFactor() {
	static /*constexpr*/ T kph_to_mps = T(1000.0 / 3600.0);
	return kph_to_mps;
}

// Meters per second to kilometers per hour
template <typename T>
static inline /*constexpr*/ T MPSToKPHFactor() {
	static /*constexpr*/ T mps_to_kph = T(3.6);
	return mps_to_kph;
}

// Miles per hour to meters per second
template <typename T>
static inline /*constexpr*/ T MPHToMPSFactor() {
	static /*constexpr*/ T mph_to_mps = T(0.44704);
	return mph_to_mps;
}

// Meters per second to miles per hour
template <typename T>
static inline /*constexpr*/ T MPSToMPHFactor() {
	static /*constexpr*/ T mps_to_mph = T(2.236936292054402290622763063708);
	return mps_to_mph;
}

template <typename T>
static inline /*constexpr*/ T MilesToKMFactor() {
	static /*constexpr*/ T miles_to_km = T(1.609344);
	return miles_to_km;
}

template <typename T>
static inline /*constexpr*/ T KMToMilesFactor() {
	static /*constexpr*/ T km_to_miles = T(0.62137119223733396961743418436332);
	return km_to_miles;
}

} // namespace KEPMathDetail

template <typename T>
inline T PoundsToKilograms(const T& t) {
	return t * KEPMathDetail::PoundsToKilogramsFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T KilogramsToPounds(const T& t) {
	return t * KEPMathDetail::KilogramsToPoundsFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T FeetToMeters(const T& t) {
	return t * KEPMathDetail::FeetToMetersFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline constexpr T MetersToFeet(const T& t) {
	return t * KEPMathDetail::MetersToFeetFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

// Convert translation part of a matrix
template <size_t N, typename T, typename = std::enable_if_t<(N == 3 || N == 4)>>
inline Matrix<4, N, T> ConvertMatrixTranslation(const Matrix<4, N, T>& mtxIn, float fFactor) {
	Matrix<4, N, T> mtxOut;
	for (size_t i = 0; i < 3; ++i) {
		for (size_t j = 0; j < N; ++j)
			mtxOut(i, j) = mtxIn(i, j);
	}
	for (size_t j = 0; j < N; ++j)
		mtxOut(3, j) = fFactor * mtxIn(3, j);
	if (N == 4)
		mtxOut(3, 3) = mtxIn(3, 3);

	return mtxOut;
}

template <size_t N, typename T, typename = std::enable_if_t<(N == 3 || N == 4)>>
inline Matrix<4, N, T> MetersToFeet(const Matrix<4, N, T>& mtxMeters) {
	return ConvertMatrixTranslation(mtxMeters, KEPMathDetail::MetersToFeetFactor<T>());
}

template <size_t N, typename T, typename = std::enable_if_t<(N == 3 || N == 4)>>
inline Matrix<4, N, T> FeetToMeters(const Matrix<4, N, T>& mtxFeet) {
	return ConvertMatrixTranslation(mtxFeet, KEPMathDetail::FeetToMetersFactor<T>());
}

template <typename T>
inline T HorsepowerToWatts(const T& t) {
	return t * KEPMathDetail::HorsepowerToWattsFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T WattsToHorsepower(const T& t) {
	return t * KEPMathDetail::WattsToHorsepowerFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T MPSToKPH(const T& t) {
	return t * KEPMathDetail::MPSToKPHFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T KPHToMPS(const T& t) {
	return t * KEPMathDetail::KPHToMPSFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T MilesToKM(const T& t) {
	return t * KEPMathDetail::MilesToKMFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T KMToMiles(const T& t) {
	return t * KEPMathDetail::KMToMilesFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T MPHToKPH(const T& t) {
	return t * KEPMathDetail::MilesToKMFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T KPHToMPH(const T& t) {
	return t * KEPMathDetail::KMToMilesFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T MPHToMPS(const T& t) {
	return t * KEPMathDetail::MPHToMPSFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

template <typename T>
inline T MPSToMPH(const T& t) {
	return t * KEPMathDetail::MPSToMPHFactor<KEPMathDetail::FloatingPointTypeOf_t<T>>();
}

} // namespace KEP
