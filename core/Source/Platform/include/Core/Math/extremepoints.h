///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Vector.h"
namespace KEP {

// Structure for storing min and max values of a set, along with the object
// associated with the min and max values.
// E.g. to store extreme values for a single bounding box direction and the vertex associated with that min and max box value.
template <typename Iterator, typename T>
struct ExtremePair {
	T m_fMinValue;
	Iterator m_MinIterator;
	T m_fMaxValue;
	Iterator m_MaxIterator;

	void Update(const Iterator& itr, T fValue) {
		if (fValue < m_fMinValue) {
			m_fMinValue = fValue;
			m_MinIterator = itr;
		}
		if (fValue > m_fMaxValue) {
			m_fMaxValue = fValue;
			m_MaxIterator = itr;
		}
	}
};

template <typename T>
inline T GetExtremePointScaleByIndex(size_t idx) {
	if (idx < 3)
		return T(1); // 1 / sqrt(1)
	if (idx < 7)
		return T(0.57735026918962576450914878050196); // 1 / sqrt(3)
	if (idx < 13)
		return T(0.70710678118654752440084436210485); // 1 / sqrt(2)
	if (idx < 25)
		return T(0.44721359549995793928183473374626); // 1 / sqrt(5)
	if (idx < 37)
		return T(0.40824829046386301636621401245098); // 1 / sqrt(6)
	if (idx < 49)
		return T(0.33333333333333333333333333333333); // 1 / sqrt(9)
	return 0;
}

template <typename T>
inline Vector3<T> GetExtremePointDirection(size_t idx) {
	static const /*constexpr*/ Vector3<T> s_aDirections[49] = {
		Vector3<T>(1, 0, 0),
		Vector3<T>(0, 1, 0),
		Vector3<T>(0, 0, 1),

		Vector3<T>(1, 1, 1),
		Vector3<T>(1, 1, -1),
		Vector3<T>(1, -1, 1),
		Vector3<T>(1, -1, -1),

		Vector3<T>(1, 1, 0),
		Vector3<T>(1, -1, 0),
		Vector3<T>(1, 0, 1),
		Vector3<T>(1, 0, -1),
		Vector3<T>(0, 1, 1),
		Vector3<T>(0, 1, -1),

		Vector3<T>(0, 1, 2),
		Vector3<T>(0, 2, 1),
		Vector3<T>(1, 0, 2),
		Vector3<T>(2, 0, 1),
		Vector3<T>(1, 2, 0),
		Vector3<T>(2, 1, 0),
		Vector3<T>(0, 1, -2),
		Vector3<T>(0, 2, -1),
		Vector3<T>(1, 0, -2),
		Vector3<T>(2, 0, -1),
		Vector3<T>(1, -2, 0),
		Vector3<T>(2, -1, 0),

		Vector3<T>(1, 1, 2),
		Vector3<T>(2, 1, 1),
		Vector3<T>(1, 2, 1),
		Vector3<T>(1, -1, 2),
		Vector3<T>(1, 1, -2),
		Vector3<T>(1, -1, -2),
		Vector3<T>(2, -1, 1),
		Vector3<T>(2, 1, -1),
		Vector3<T>(2, -1, -1),
		Vector3<T>(1, -2, 1),
		Vector3<T>(1, 2, -1),
		Vector3<T>(1, -2, -1),

		Vector3<T>(2, 2, 1),
		Vector3<T>(1, 2, 2),
		Vector3<T>(2, 1, 2),
		Vector3<T>(2, -2, 1),
		Vector3<T>(2, 2, -1),
		Vector3<T>(2, -2, -1),
		Vector3<T>(1, -2, 2),
		Vector3<T>(1, 2, -2),
		Vector3<T>(1, -2, -2),
		Vector3<T>(2, -1, 2),
		Vector3<T>(2, 1, -2),
		Vector3<T>(2, -1, -2),
	};
	return s_aDirections[idx];
}

// If nDirections is 3, the bounding box is calculated.
// If nDirections is 7, the extreme points along the coordinate axes as well as in the direction of the box corners are calculated.
// If bUseNormalizedDirections is true, the dot product with the normalized directions is calculated.
// 3:  (1, 0, 0), (0, 1, 0), (0, 0, 1)
// 4:  (1, 1, 1), (1, 1, -1), (1, -1, 1), (1, -1, -1)
// 6:  (1, 1, 0), (1, -1, 0), (1, 0, 1), (1, 0, -1), (0, 1, 1), (0, 1, -1)
// 12: (0, 1, 2), (0, 2, 1), (1, 0, 2), (2, 0, 1), (1, 2, 0), (2, 1, 0)
//     (0, 1, -2), (0, 2, -1), (1, 0, -2), (2, 0, -1), (1, -2, 0), (2, -1, 0)
// 12: (1, 1, 2), (2, 1, 1), (1, 2, 1), (1, -1, 2), (1, 1, -2), (1, -1, -2)
//     (2, -1, 1), (2, 1, -1), (2, -1, -1), (1, -2, 1), (1, 2, -1), (1, -2, -1)
// 12: (2, 2, 1), (1, 2, 2), (2, 1, 2), (2, -2, 1), (2, 2, -1), (2, -2, -1)
//     (1, -2, 2), (1, 2, -2), (1, -2, -2), (2, -1, 2), (2, 1, -2), (2, -1, -2)
template <size_t nDirections, typename T, typename Iterator>
inline bool CalculateExtremePoints(ExtremePair<Iterator, T> (&aExtremePairs)[nDirections], Iterator b, Iterator e, bool bUseNormalizedDirections = true) {
	static_assert(nDirections == 3 || nDirections == 7 || nDirections == 13 || nDirections == 25 || nDirections == 37 || nDirections == 49,
		"Unsupported number of directions.");

	if (b == e)
		return false;

	for (size_t i = 0; i < nDirections; ++i) {
		ExtremePair<Iterator, T>& ep = aExtremePairs[i];
		ep.m_fMinValue = std::numeric_limits<T>::max();
		ep.m_fMaxValue = -std::numeric_limits<T>::max();
	}

	for (Iterator itr = b; itr != e; ++itr) {
		// Variables are named for the vector whose dot product is taken against the test variable:
		// p1p0m1 is named for the vector (+1,+0,-1). Its value is pt.Dot( (1,0,-1) )

		// (1, 0, 0), (0, 1, 0), (0, 0, 1)
		const Vector3<T>& pt = *itr;
		T p1p0p0 = pt[0]; // (1, 0, 0)
		aExtremePairs[0].Update(itr, p1p0p0);
		T p0p1p0 = pt[1]; // (0, 1, 0)
		aExtremePairs[1].Update(itr, p0p1p0);
		T p0p0p1 = pt[2]; // (0, 0, 1)
		aExtremePairs[2].Update(itr, p0p0p1);

		if (nDirections == 3)
			continue;

		T p1p1p0 = p1p0p0 + p0p1p0; // (1, 1, 0)
		T p1m1p0 = p1p0p0 - p0p1p0; // (1, -1, 0)

		// (1, 1, 1), (1, 1, -1), (1, -1, 1), (1, -1, -1)
		T p1p1p1 = p1p1p0 + p0p0p1; // (1, 1, 1)
		aExtremePairs[3].Update(itr, p1p1p1);
		T p1p1m1 = p1p1p0 - p0p0p1; // (1, 1, -1)
		aExtremePairs[4].Update(itr, p1p1m1);
		T p1m1p1 = p1m1p0 + p0p0p1; // (1, -1, 1)
		aExtremePairs[5].Update(itr, p1m1p1);
		T p1m1m1 = p1m1p0 - p0p0p1; // (1, -1, -1)
		aExtremePairs[6].Update(itr, p1m1m1);

		if (nDirections == 7)
			continue;

		// (1, 1, 0), (1, -1, 0), (1, 0, 1), (1, 0, -1), (0, 1, 1), (0, 1, -1)
		//T p1p1p0 = p1p0p0 + p0p1p0; // (1, 1, 0)
		aExtremePairs[7].Update(itr, p1p1p0);
		//T p1m1p0 = p1p0p0 - p0p1p0; // (1, -1, 0)
		aExtremePairs[8].Update(itr, p1m1p0);
		T p1p0p1 = p1p0p0 + p0p0p1; // (1, 0, 1)
		aExtremePairs[9].Update(itr, p1p0p1);
		T p1p0m1 = p1p0p0 - p0p0p1; // (1, 0, -1)
		aExtremePairs[10].Update(itr, p1p0m1);
		T p0p1p1 = p0p1p0 + p0p0p1; // (0, 1, 1)
		aExtremePairs[11].Update(itr, p0p1p1);
		T p0p1m1 = p0p1p0 - p0p0p1; // (0, 1, -1)
		aExtremePairs[12].Update(itr, p0p1m1);

		if (nDirections == 13)
			continue;

		// (0, 1, 2), (0, 2, 1), (1, 0, 2), (2, 0, 1), (1, 2, 0), (2, 1, 0)
		// (0, 1, -2), (0, 2, -1), (1, 0, -2), (2, 0, -1), (1, -2, 0), (2, -1, 0)
		T p0p1p2 = p0p1p1 + p0p0p1; // (0, 1, 2)
		aExtremePairs[13].Update(itr, p0p1p2);
		T p0p2p1 = p0p1p1 + p0p1p0; // (0, 2, 1)
		aExtremePairs[14].Update(itr, p0p2p1);
		T p1p0p2 = p1p0p1 + p0p0p1; // (1, 0, 2)
		aExtremePairs[15].Update(itr, p1p0p2);
		T p2p0p1 = p1p0p1 + p1p0p0; // (2, 0, 1)
		aExtremePairs[16].Update(itr, p2p0p1);
		T p1p2p0 = p1p1p0 + p0p1p0; // (1, 2, 0)
		aExtremePairs[17].Update(itr, p1p2p0);
		T p2p1p0 = p1p1p0 + p1p0p0; // (2, 1, 0)
		aExtremePairs[18].Update(itr, p2p1p0);

		T p0p1m2 = p0p1m1 - p0p0p1; // (0, 1, -2)
		aExtremePairs[19].Update(itr, p0p1m2);
		T p0p2m1 = p0p1m1 + p0p1p0; // (0, 2, -1)
		aExtremePairs[20].Update(itr, p0p2m1);
		T p1p0m2 = p1p0m1 - p0p0p1; // (1, 0, -2)
		aExtremePairs[21].Update(itr, p1p0m2);
		T p2p0m1 = p1p0m1 + p1p0p0; // (2, 0, -1)
		aExtremePairs[22].Update(itr, p2p0m1);
		T p1m2p0 = p1m1p0 - p0p1p0; // (1, -2, 0)
		aExtremePairs[23].Update(itr, p1m2p0);
		T p2m1p0 = p1m1p0 + p1p0p0; // (2, -1, 0)
		aExtremePairs[24].Update(itr, p2m1p0);

		if (nDirections == 25)
			continue;

		// (1, 1, 2), (2, 1, 1), (1, 2, 1), (1, -1, 2), (1, 1, -2), (1, -1, -2)
		// (2, -1, 1), (2, 1, -1), (2, -1, -1), (1, -2, 1), (1, 2, -1), (1, -2, -1)
		T p1p1p2 = p1p1p1 + p0p0p1; // (1, 1, 2)
		aExtremePairs[25].Update(itr, p1p1p2);
		T p2p1p1 = p1p1p1 + p1p0p0; // (2, 1, 1)
		aExtremePairs[26].Update(itr, p2p1p1);
		T p1p2p1 = p1p1p1 + p0p1p0; // (1, 2, 1)
		aExtremePairs[27].Update(itr, p1p2p1);
		T p1m1p2 = p1m1p1 + p0p0p1; // (1, -1, 2)
		aExtremePairs[28].Update(itr, p1m1p2);
		T p1p1m2 = p1p1m1 - p0p0p1; // (1, 1, -2)
		aExtremePairs[29].Update(itr, p1p1m2);
		T p1m1m2 = p1m1m1 - p0p0p1; // (1, -1, -2)
		aExtremePairs[30].Update(itr, p1m1m2);

		T p2m1p1 = p1m1p1 - p1p0p0; // (2, -1, 1)
		aExtremePairs[31].Update(itr, p2m1p1);
		T p2p1m1 = p1p1m1 + p1p0p0; // (2, 1, -1)
		aExtremePairs[32].Update(itr, p2p1m1);
		T p2m1m1 = p1m1m1 + p1p0p0; // (2, -1, -1)
		aExtremePairs[33].Update(itr, p2m1m1);
		T p1m2p1 = p1m1p1 - p0p1p0; // (1, -2, 1)
		aExtremePairs[34].Update(itr, p1m2p1);
		T p1p2m1 = p1p1m1 + p0p1p0; // (1, 2, -1)
		aExtremePairs[35].Update(itr, p1p2m1);
		T p1m2m1 = p1m1m1 - p0p1p0; // (1, -2, -1)
		aExtremePairs[36].Update(itr, p1m2m1);

		if (nDirections == 37)
			continue;

		// (2, 2, 1), (1, 2, 2), (2, 1, 2), (2, -2, 1), (2, 2, -1), (2, -2, -1)
		// (1, -2, 2), (1, 2, -2), (1, -2, -2), (2, -1, 2), (2, 1, -2), (2, -1, -2)
		T p2p2p1 = p1p2p1 + p1p0p0; // (2, 2, 1)
		aExtremePairs[37].Update(itr, p2p2p1);
		T p1p2p2 = p1p2p1 + p0p0p1; // (1, 2, 2)
		aExtremePairs[38].Update(itr, p1p2p2);
		T p2p1p2 = p2p1p1 + p0p0p1; // (2, 1, 2)
		aExtremePairs[39].Update(itr, p2p1p2);
		T p2m2p1 = p2m1p1 - p0p1p0; // (2, -2, 1)
		aExtremePairs[40].Update(itr, p2m2p1);
		T p2p2m1 = p1p2m1 + p1p0p0; // (2, 2, -1)
		aExtremePairs[41].Update(itr, p2p2m1);
		T p2m2m1 = p2m1m1 - p0p1p0; // (2, -2, -1)
		aExtremePairs[42].Update(itr, p2m2m1);

		T p1m2p2 = p1m1p2 - p0p1p0; // (1, -2, 2)
		aExtremePairs[43].Update(itr, p1m2p2);
		T p1p2m2 = p1p2m1 - p0p0p1; // (1, 2, -2)
		aExtremePairs[44].Update(itr, p1p2m2);
		T p1m2m2 = p1m1m2 - p0p1p0; // (1, -2, -2)
		aExtremePairs[45].Update(itr, p1m2m2);
		T p2m1p2 = p2m1p1 + p0p0p1; // (2, -1, 2)
		aExtremePairs[46].Update(itr, p2m1p2);
		T p2p1m2 = p2p1m1 - p0p0p1; // (2, 1, -2)
		aExtremePairs[47].Update(itr, p2p1m2);
		T p2m1m2 = p2m1m1 - p0p0p1; // (2, -1, -2)
		aExtremePairs[48].Update(itr, p2m1m2);
	}

	if (bUseNormalizedDirections) {
		for (size_t i = 0; i < nDirections; ++i) {
			aExtremePairs[i].m_fMinValue *= GetExtremePointScaleByIndex<T>(i);
			aExtremePairs[i].m_fMaxValue *= GetExtremePointScaleByIndex<T>(i);
		}
	}

	return true;
}

template <typename T, typename Iterator>
inline bool CalculateExtremePoints(ExtremePair<Iterator, T> aExtremePairs[], size_t nDirections, Iterator b, Iterator e, bool bUseNormalizedDirections = true) {
	switch (nDirections) {
		case 0:
			return true;
		case 3:
			return CalculateExtremePoints<3>((ExtremePair<Iterator, T>(&)[3]) * aExtremePairs, b, e, bUseNormalizedDirections);
		case 7:
			return CalculateExtremePoints<7>((ExtremePair<Iterator, T>(&)[7]) * aExtremePairs, b, e, bUseNormalizedDirections);
		case 13:
			return CalculateExtremePoints<13>((ExtremePair<Iterator, T>(&)[13]) * aExtremePairs, b, e, bUseNormalizedDirections);
		case 25:
			return CalculateExtremePoints<25>((ExtremePair<Iterator, T>(&)[25]) * aExtremePairs, b, e, bUseNormalizedDirections);
		case 37:
			return CalculateExtremePoints<37>((ExtremePair<Iterator, T>(&)[37]) * aExtremePairs, b, e, bUseNormalizedDirections);
		case 49:
			return CalculateExtremePoints<49>((ExtremePair<Iterator, T>(&)[49]) * aExtremePairs, b, e, bUseNormalizedDirections);
	}
	return false; // Invalid number of directions.
}

} //namespace KEP
