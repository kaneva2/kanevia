///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Vector.h"

namespace KEP {
template <size_t N, typename T>
inline T GetCosineAngleBetween(const Vector<N, T>& v1, const Vector<N, T>& v2) {
	T cosAngle = v1.Dot(v2) * ReciprocalSqrt(v1.LengthSquared() * v2.LengthSquared());

	// Make sure result is in range [-1..+1] regardless of any roundoff error in calculation.
	if (std::abs(cosAngle) <= 1)
		return cosAngle;
	return std::copysign<T>(1, cosAngle);
}

template <size_t N, typename T>
inline T GetAngleBetween(const Vector<N, T>& v1, const Vector<N, T>& v2) {
	T cosAngle = GetCosineAngleBetween(v1, v2);
	return std::acos(cosAngle);
}

// Returns the signed angle between v1 and v2 when viewed from the vUp direction
// e.g. if vUp == (0,0,1), then the angle is as typically defined for the XY plane
// with counterclockwise direction being positive.
template <size_t N, typename T>
inline T GetSignedAngleBetween(const Vector<N, T>& v1, const Vector<N, T>& v2, const Vector<N, T>& vUp) {
	T cosAngle = GetCosineAngleBetween(v1, v2);
	T angle = std::acos(cosAngle);
	if (vUp.Dot(v1.Cross(v2)) < 0)
		angle = -angle;
	return angle;
}

#if 0
// Returns the signed angle between v1 and v2 after projecting onto a plane with normal vNormal.
// Results will be the same as GetSignedAngleBetween() if vNormal is perpendicular to v1 and v2.
template<size_t N, typename T>
inline T GetProjectedAngleBetween(const Vector<N,T>& v1, const Vector<N,T>& v2, const Vector<N,T>& vNormal)
{
	Vector<N,T> v1Perp = v1.GetPartPerpendicularTo(vNormal);
	Vector<N,T> v2Perp = v2.GetPartPerpendicularTo(vNormal);
	return GetAngleBetween(v1Perp, v2Perp, vNormal);
}
#endif

} // namespace KEP