///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Primitive.h"
#include "PrimitiveType.h"
#include "PrimitiveMesh.h"
#include <assert.h>
#include <algorithm>

namespace KEP {

template <typename T, typename FWD>
class RectangularFrustum : public Primitive<T> {
	using RIGHT = typename FWD::NextAxis;
	using UP = typename FWD::PrevAxis;

public:
	RectangularFrustum(T fov, T aspect, T nearPlane, T farPlane) :
			m_fov(fov),
			m_aspect(aspect),
			m_near(nearPlane),
			m_far(farPlane),
			m_tanHalfFov(tan(m_fov / 2 * M_PI / 180)) {
		SetDegenerated(m_fov == 0 || m_near >= m_far);
	}

	virtual int GetType() const override { return PrimitiveType::RectangularFrustum; }

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt) const override { return Contains_ObjectSpace(pt, static_cast<T>(0)); }

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt, T fTolerance) const override {
		if (IsDegenerated()) {
			return false;
		}

		// Primary axis
		T distFromApex = FWD::Get(pt);
		if (distFromApex < m_near - fTolerance || distFromApex > m_far + fTolerance || distFromApex < -fTolerance) {
			return false;
		}

		distFromApex = (std::max)((std::min)(distFromApex, m_far), m_near);

		// Projection on FovY / FovX
		T maxDistOnFovY = m_tanHalfFov * distFromApex;
		return fabs(UP::Get(pt)) <= maxDistOnFovY + fTolerance &&
			   fabs(RIGHT::Get(pt)) <= maxDistOnFovY * m_aspect + fTolerance;
	}

	virtual void GenerateVisualMesh(TriList& triangleList) const override {
		// Winding order: CW
		T nearHalfH = m_tanHalfFov * m_near;
		T nearHalfW = nearHalfH * m_aspect;
		T farHalfH = m_tanHalfFov * m_far;
		T farHalfW = farHalfH * m_aspect;
		PrimitiveMesh<T>::AddHexahedron(
			{ FWD::Normal(-nearHalfH, -nearHalfW) + FWD::Tangent(m_near), -FWD::Basis() },
			{ FWD::Normal(nearHalfH, -nearHalfW) + FWD::Tangent(m_near), -FWD::Basis() },
			{ FWD::Normal(nearHalfH, nearHalfW) + FWD::Tangent(m_near), -FWD::Basis() },
			{ FWD::Normal(-nearHalfH, nearHalfW) + FWD::Tangent(m_near), -FWD::Basis() },
			{ FWD::Normal(-farHalfH, -farHalfW) + FWD::Tangent(m_far), FWD::Basis() },
			{ FWD::Normal(farHalfH, -farHalfW) + FWD::Tangent(m_far), FWD::Basis() },
			{ FWD::Normal(farHalfH, farHalfW) + FWD::Tangent(m_far), FWD::Basis() },
			{ FWD::Normal(-farHalfH, farHalfW) + FWD::Tangent(m_far), FWD::Basis() },
			triangleList);
	}

private:
	T m_fov;
	T m_aspect;
	T m_near;
	T m_far;
	T m_tanHalfFov;
};

} // namespace KEP
