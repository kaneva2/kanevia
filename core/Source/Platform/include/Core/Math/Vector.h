///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <type_traits>
#include <initializer_list>
#include <stdint.h>
#include "KEPMathUtil.h"
#include "Core/Util/Parameter.h"

namespace KEP {

template <size_t N, typename T>
struct VectorData {
	T m_coords[N];
};

// Specializations for sizes 2, 3, and 4
template <typename T>
struct VectorDataBase2 {
protected:
	union {
		T m_coords[2];
		struct {
			T x;
			T y;
		};
	};
};

template <typename T>
struct VectorData<2, T> : VectorDataBase2<T> {
	using VectorDataBase2<T>::x;
	using VectorDataBase2<T>::y;
};

template <typename T>
struct VectorDataBase3 {
protected:
	union {
		T m_coords[3];
		struct {
			T x;
			T y;
			T z;
		};
	};
};

template <typename T>
struct VectorData<3, T> : VectorDataBase3<T> {
	using VectorDataBase3<T>::x;
	using VectorDataBase3<T>::y;
	using VectorDataBase3<T>::z;
};

template <typename T>
struct VectorDataBase4 {
protected:
	union {
		T m_coords[4];
		struct {
			T x;
			T y;
			T z;
			T w;
		};
	};
};

template <typename T>
struct VectorData<4, T> : VectorDataBase4<T> {
	using VectorDataBase4<T>::x;
	using VectorDataBase4<T>::y;
	using VectorDataBase4<T>::z;
	using VectorDataBase4<T>::w;
};

template <size_t N, typename T>
class Vector : public VectorData<N, T> {
	static_assert(sizeof(VectorData<N, T>) == sizeof(T) * N, "Vector should not contain any padding or extra variables.");

public:
	static const /*constexpr*/ size_t DIM = N;
	enum { Dim = N };
	typedef T value_type;

	// Default constructor, destructor, copy constructor, assignment.
	Vector() {}
	~Vector() = default;
	Vector(const Vector&) = default;
	Vector& operator=(const Vector&) = default;

	//explicit Vector(const T* values) { for(size_t i = 0; i < N; ++i) this->m_coords[i] = values[i]; }
	template <typename T>
	explicit Vector(const T (&values)[N]) {
		for (size_t i = 0; i < N; ++i) this->m_coords[i] = values[i];
	}

	// Explicit conversion between vectors of different types.
	template <size_t M, typename U, typename = std::enable_if_t<(M >= N)>>
	explicit Vector(const Vector<M, U>& v) {
		for (size_t i = 0; i < N; ++i) this->m_coords[i] = static_cast<T>(v[i]);
	}
	// Explicit conversion between vectors of different types and sizes.
	template <size_t M, typename U, typename = std::enable_if_t<(M < N)>>
	explicit Vector(const Vector<M, U>& v, const T& fillValue) {
		for (size_t i = 0; i < M; ++i) this->m_coords[i] = static_cast<T>(v[i]);
		for (size_t i = M; i < N; ++i) this->m_coords[i] = fillValue;
	}

	//template<size_t M, typename U, typename=std::enable_if_t<(M<=N)>>
	//explicit operator Vector<M,U>() const { Vector<M,U> v; for(size_t i = 0; i < M; ++i) v[i] = static_cast<U>(this->m_coords[i]); return v; }

	// fill with constant value.
	explicit Vector(const T& fillValue) { Fill(fillValue); }

	// Scalar argument list constructor
	template <typename... Args>
	Vector(const std::enable_if_t<(N > 1) && sizeof...(Args) == N - 1, T>& arg0, Args&&... args) { DoSet(m_coords, arg0, std::forward<Args>(args)...); }

	template <typename... Args>
	std::enable_if_t<sizeof...(Args) == N> Set(Args&&... args) { DoSet(m_coords, std::forward<Args>(args)...); }
	void Fill(const T& value) {
		for (size_t i = 0; i < N; ++i) m_coords[i] = value;
	}
	void MakeZero() { Fill(0); }

	// operator overloads.
	T& operator[](size_t i);
	const T& operator[](size_t i) const;

	Vector<N, T>& operator+=(const Vector& right);
	Vector<N, T>& operator-=(const Vector& right);
	Vector<N, T>& operator*=(const Vector& right); // component-wise
	Vector<N, T>& operator/=(const Vector& right); // component-wise

	Vector<N, T>& operator*=(const T& right);
	Vector<N, T>& operator/=(const T& right);

	// Dot-product, j, and normalization
	T Dot(const Vector& right) const;
	T LengthSquared() const;
	T Length() const;
	T ReciprocalLength() const;
	void NormalizeUnchecked(); // Normalizes *this (must not be zero).
	bool Normalize(); // Does nothing if *this is zero, otherwise normalizes *this.
	Vector GetNormalizedUnchecked() const; // Returns normalized version of *this (must not be zero).
	Vector GetNormalized() const; // Returns zero vector if *this is zero, else GetNormalizedUnchecked()

	// Cross product.
	template <typename = std::enable_if_t<N == 3>>
	Vector<3, T> Cross(const Vector<3, T>& right) const; // infix: C = A.Cross(B);
	template <typename = std::enable_if_t<N == 3>>
	void Cross(const Vector<3, T>& left, const Vector<3, T>& right); // Self-modifying: C.Cross(A,B);
	template <typename = std::enable_if_t<N == 2>>
	T Cross(const Vector<2, T>& right) const; // infix: scalar = A.Cross(B);

	// Projections
	Vector<N, T> GetProjectedOnto(const Vector<N, T>& v) const;
	Vector<N, T> GetProjectedOntoNormalized(const Vector<N, T>& v) const; // Same as GetProjectedOnto but assumes v is normalized.
	Vector<N, T> GetPartParallelTo(const Vector<N, T>& v) const { return GetProjectedOnto(v); }
	Vector<N, T> GetPartPerpendicularTo(const Vector<N, T>& v) const { return *this - GetPartParallelTo(v); }
	void RemovePartParallelTo(const Vector<N, T>& v) const { *this -= GetPartParallelTo(v); }

	// Component access.
	template <typename = std::enable_if_t<(N >= 1), void>>
	T& X() { return m_coords[0]; }
	template <typename = std::enable_if_t<(N >= 1), void>>
	const T& X() const { return m_coords[0]; }
	template <typename = std::enable_if_t<(N >= 2), void>>
	T& Y() { return m_coords[1]; }
	template <typename = std::enable_if_t<(N >= 2), void>>
	const T& Y() const { return m_coords[1]; }
	template <typename = std::enable_if_t<(N >= 3), void>>
	T& Z() { return m_coords[2]; }
	template <typename = std::enable_if_t<(N >= 3), void>>
	const T& Z() const { return m_coords[2]; }
	template <typename = std::enable_if_t<(N >= 4), void>>
	T& W() { return m_coords[3]; }
	template <typename = std::enable_if_t<(N >= 4), void>>
	const T& W() const { return m_coords[3]; }

	float* begin() { return &m_coords[0]; }
	const float* begin() const { return &m_coords[0]; }
	float* end() { return &m_coords[0] + N; }
	const float* end() const { return &m_coords[0] + N; }
	const float* cbegin() const { return &m_coords[0]; }
	const float* cend() const { return &m_coords[0] + N; }

	template <size_t Length>
	Vector<Length, T>& SubVector(size_t idxStart = 0) { return reinterpret_cast<Vector<Length, T>&>(m_coords[idxStart]); }
	template <size_t Length>
	const Vector<Length, T>& SubVector(size_t idxStart = 0) const { return reinterpret_cast<const Vector<Length, T>&>(m_coords[idxStart]); }

	// Return a basis vector: all components are zero except for index 'I', which is one.
	template <size_t I>
	static Vector Basis() {
		Vector ret;
		for (size_t i = 0; i < I; ++i) ret[i] = 0;
		ret[I] = 1;
		for (size_t i = I + 1; i < N; ++i) ret[i] = 0;
		return ret;
	}
	static Vector Basis(size_t I) {
		Vector ret;
		for (size_t i = 0; i < I; ++i) ret[i] = 0;
		ret[I] = 1;
		for (size_t i = I + 1; i < N; ++i) ret[i] = 0;
		return ret;
	}

	// Return the zero vector.
	static Vector Zero() {
		Vector ret;
		for (size_t i = 0; i < N; ++i) ret[i] = 0;
		return ret;
	}

	T (&RawData())
	[N] { return reinterpret_cast<T(&)[N]>(m_coords); }
	const T (&RawData() const)[N] { return reinterpret_cast<const T(&)[N]>(m_coords); }

private:
	static void DoSet(T* pNextCoord) {}
	template <typename... Args>
	static void DoSet(T* pNextCoord, const T& nextArg, Args&&... args) {
		*pNextCoord = nextArg;
		DoSet(pNextCoord + 1, std::forward<Args>(args)...);
	}
};

template <size_t N, typename T>
inline T& Vector<N, T>::operator[](size_t i) {
	return m_coords[i];
}

template <size_t N, typename T>
inline const T& Vector<N, T>::operator[](size_t i) const {
	return m_coords[i];
}

template <size_t N, typename T>
inline Vector<N, T>& Vector<N, T>::operator+=(const Vector<N, T>& right) {
	for (size_t i = 0; i < N; ++i)
		m_coords[i] += right[i];
	return *this;
}

template <size_t N, typename T>
inline Vector<N, T>& Vector<N, T>::operator-=(const Vector<N, T>& right) {
	for (size_t i = 0; i < N; ++i)
		m_coords[i] -= right[i];
	return *this;
}

template <size_t N, typename T>
inline Vector<N, T>& Vector<N, T>::operator*=(const Vector<N, T>& right) {
	for (size_t i = 0; i < N; ++i)
		m_coords[i] *= right[i];
	return *this;
}

template <size_t N, typename T>
inline Vector<N, T>& Vector<N, T>::operator/=(const Vector<N, T>& right) {
	for (size_t i = 0; i < N; ++i)
		m_coords[i] = Divide(m_coords[i], right[i]);
	return *this;
}

template <size_t N, typename T>
inline Vector<N, T>& Vector<N, T>::operator*=(const T& right) {
	for (size_t i = 0; i < N; ++i)
		m_coords[i] *= right;
	return *this;
}

template <size_t N, typename T>
inline Vector<N, T>& Vector<N, T>::operator/=(const T& right) {
	return operator*=(Reciprocal(right));
}

template <size_t N, typename T>
inline Vector<N, T> operator+(const Vector<N, T>& left, const Vector<N, T>& right) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = left[i] + right[i];
	return ret;
}

template <size_t N, typename T>
inline Vector<N, T> operator-(const Vector<N, T>& left, const Vector<N, T>& right) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = left[i] - right[i];
	return ret;
}

template <size_t N, typename T>
inline Vector<N, T> operator-(const Vector<N, T>& v) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = -v[i];
	return ret;
}

template <size_t N, typename T>
inline const Vector<N, T>& operator+(const Vector<N, T>& v) {
	return v;
}

template <size_t N, typename T>
inline Vector<N, T> operator*(const Vector<N, T>& left, const typename std::common_type<T, T>::type& right) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = left[i] * right;
	return ret;
}

template <size_t N, typename T>
inline Vector<N, T> operator*(const typename std::common_type<T, T>::type& left, const Vector<N, T>& right) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = left * right[i];
	return ret;
}

template <size_t N, typename T>
inline Vector<N, T> operator*(const Vector<N, T>& left, const Vector<N, T>& right) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = left[i] * right[i];
	return ret;
}

template <size_t N, typename T>
inline Vector<N, T> operator/(const Vector<N, T>& left, const Vector<N, T>& right) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = Divide(left[i], right[i]);
	return ret;
}

template <size_t N, typename T>
inline bool operator==(const Vector<N, T>& left, const Vector<N, T>& right) {
	for (size_t i = 0; i < N; ++i) {
		if (left[i] != right[i])
			return false;
	}
	return true;
}

template <size_t N, typename T>
inline bool operator!=(const Vector<N, T>& left, const Vector<N, T>& right) {
	return !operator==(left, right);
}

template <size_t N, typename T>
inline Vector<N, T> operator/(const Vector<N, T>& left, const typename std::common_type<T, T>::type& right) {
	T reciprocalRight = Reciprocal(right);
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = left[i] * reciprocalRight;
	return ret;
}

template <size_t N, typename T>
inline T Vector<N, T>::Dot(const Vector& right) const {
	T ret = (*this)[0] * right[0];
	for (size_t i = 1; i < N; ++i)
		ret += (*this)[i] * right[i];
	return ret;
}

template <size_t N, typename T>
inline T Vector<N, T>::LengthSquared() const {
	T ret = (*this)[0] * (*this)[0];
	for (size_t i = 1; i < N; ++i)
		ret += (*this)[i] * (*this)[i];
	return ret;
}

template <size_t N, typename T>
inline T Vector<N, T>::Length() const {
	return std::sqrt(LengthSquared());
}

template <size_t N, typename T>
inline T Vector<N, T>::ReciprocalLength() const {
	return ReciprocalSqrt(LengthSquared());
}

template <size_t N, typename T>
inline void Vector<N, T>::NormalizeUnchecked() {
	(*this) *= ReciprocalLength();
}

template <size_t N, typename T>
inline bool Vector<N, T>::Normalize() {
	T len2 = LengthSquared();
	if (len2 == 0)
		return false;
	(*this) *= ReciprocalSqrt(len2);
	return true;
}

template <size_t N, typename T>
inline Vector<N, T> Vector<N, T>::GetNormalizedUnchecked() const {
	return (*this) * ReciprocalSqrt(LengthSquared());
}

template <size_t N, typename T>
inline Vector<N, T> Vector<N, T>::GetNormalized() const {
	T len2 = LengthSquared();
	if (len2 == 0)
		return *this;
	return (*this) * ReciprocalSqrt(len2);
}

template <size_t N, typename T>
template <typename>
inline Vector<3, T> Vector<N, T>::Cross(const Vector<3, T>& right) const {
	Vector<3, T> ret;
	ret.Cross(*this, right);
	return ret;
}

template <size_t N, typename T>
template <typename>
inline void Vector<N, T>::Cross(const Vector<3, T>& left, const Vector<3, T>& right) {
	this->X() = left.Y() * right.Z() - left.Z() * right.Y();
	this->Y() = left.Z() * right.X() - left.X() * right.Z();
	this->Z() = left.X() * right.Y() - left.Y() * right.X();
}

template <size_t N, typename T>
template <typename>
inline T Vector<N, T>::Cross(const Vector<2, T>& right) const {
	const Vector<2, T>& left = *this;
	return left.X() * right.Y() - left.Y() * right.X();
}

template <typename T>
inline T Cross(const Vector<2, T>& left, const Vector<2, T>& right) {
	return left.Cross(right);
}

template <typename T>
inline Vector<3, T> Cross(const Vector<3, T>& left, const Vector<3, T>& right) {
	Vector<3, T> ret;
	ret.Cross(left, right);
	return ret;
}

template <size_t N, typename T>
inline Vector<N, T> Vector<N, T>::GetProjectedOnto(const Vector<N, T>& v) const {
	T len2 = v.LengthSquared();
	if (len2 != 0)
		return (this->Dot(v) * Reciprocal(len2)) * v;
	else
		return Vector<N, T>::Zero();
}

template <size_t N, typename T>
inline Vector<N, T> Vector<N, T>::GetProjectedOntoNormalized(const Vector<N, T>& v) const {
	return this->Dot(v) * v;
}

template <size_t N, typename T>
inline Vector<N, T> Abs(const Vector<N, T>& v) {
	Vector<N, T> ret;
	for (size_t i = 0; i < N; ++i)
		ret[i] = std::abs(v[i]);
	return ret;
}

template <size_t N, typename T>
inline T DistanceSquared(const Vector<N, T>& left, const Vector<N, T>& right) {
	return (left - right).LengthSquared();
}

template <size_t N, typename T>
inline T Distance(const Vector<N, T>& left, const Vector<N, T>& right) {
	return (left - right).Length();
}

template <size_t N, typename T>
inline bool ArePerpendicular(const Vector<N, T>& v1, const Vector<N, T>& v2, T tol = NumericTraits<T>::CosineSquaredTolerance()) {
	return Square(v1.Dot(v2)) <= tol * v1.LengthSquared() * v2.LengthSquared();
}

// Parallel or anti-parallel
template <size_t N, typename T>
inline bool AreParallel(const Vector<N, T>& v1, const Vector<N, T>& v2, T tol = NumericTraits<T>::CosineSquaredTolerance()) {
	T v1LSq = v1.LengthSquared();
	T v2LSq = v2.LengthSquared();

	return v1LSq * v2LSq * (1 - tol) < Square(v1.Dot(v2));
}

// Convenience function to test distance while avoiding a square root
template <size_t N, typename T>
inline bool AreWithinDistance(const Vector<N, T>& v1, const Vector<N, T>& v2, T distance) {
	return DistanceSquared(v1, v2) < Square(distance);
}

// Returns true if distance between vectors is small relative to their lengths.
template <size_t N, typename T>
inline bool RelativelyClose(const Vector<N, T>& v1, const Vector<N, T>& v2, T relative_tolerance = NumericTraits<T>::DistanceTolerance()) {
	T dSq = DistanceSquared(v1, v2);
	T v1LSq = v1.LengthSquared();
	T v2LSq = v2.LengthSquared();

	return dSq < (v1LSq + v2LSq) * Square(relative_tolerance);
}

template <typename VectorType>
struct LexicographicallyLess {};

template <size_t N, typename T>
struct LexicographicallyLess<Vector<N, T>> {
	template <size_t N, typename T>
	inline bool operator()(const Vector<N, T>& left, const Vector<N, T>& right) const {
		for (size_t i = 0; i < N - 1; ++i) {
			if (left[i] != right[i])
				return left[i] < right[i];
		}
		return left[N - 1] < right[N - 1];
	}
};

template <size_t N, typename T>
void UpdateBoundingBox(const Vector<N, T>& v, InOut<Vector<N, T>> vMin, InOut<Vector<N, T>> vMax) {
	for (size_t i = 0; i < N; ++i)
		UpdateMinMax(v[i], inout(vMin.get()[i]), inout(vMax.get()[i]));
}

template <size_t N, typename T, typename Iterator>
bool CalculateBoundingBox(const Iterator& begin, const Iterator& end, Out<Vector<N, T>> vMinOut, Out<Vector<N, T>> vMaxOut) {
	if (begin == end)
		return false;
	Iterator itr = begin;
	Vector<N, T> vMin = *itr;
	Vector<N, T> vMax = *itr;
	++itr;
	for (; itr != end; ++itr) {
		UpdateBoundingBox(*itr, inout(vMin), inout(vMax));
	}
	vMinOut.set(vMin);
	vMaxOut.set(vMax);
	return true;
}

template <size_t N, typename T>
inline /*constexpr*/ Vector<N, T> ToDegrees(const Vector<N, T>& vRadians) {
	return vRadians * KEPMathDetail::RadiansToDegreesFactor<T>();
}
template <size_t N, typename T>
inline /*constexpr*/ Vector<N, T> ToRadians(const Vector<N, T>& vRadians) {
	return vRadians * KEPMathDetail::DegreesToRadiansFactor<T>();
}

template <>
inline /*constexpr*/ Vector<4, float> Clamp<Vector<4, float>>(const Vector<4, float>& t, const std::common_type_t<Vector<4, float>>& min, const std::common_type_t<Vector<4, float>>& max) {
	alignas(16) Vector<4, float> v;
	_mm_store_ps(v.RawData(), Clamp(_mm_loadu_ps(t.RawData()), _mm_loadu_ps(min.RawData()), _mm_loadu_ps(max.RawData())));
	return v;
}

// Specialization for ReciprocalSqrt_Approx for Vector4f
template <>
inline Vector<4, float> ReciprocalSqrt_Approx<Vector<4, float>>(const Vector<4, float>& t) {
	__m128 input = _mm_loadu_ps(t.RawData());
	__m128 guess = _mm_rsqrt_ps(input); // Relative error is 1.5 * 2^-12 for Intel processors. AMD between 11-14 bits.
	Vector<4, float> result;
	_mm_storeu_ps(&result[0], guess);
	return result;
}

// Specialization for ReciprocalSqrt_Exact for Vector4f
template <>
inline Vector<4, float> ReciprocalSqrt_Close<Vector<4, float>>(const Vector<4, float>& t) {
	__m128 input = _mm_loadu_ps(t.RawData());
	__m128 guess = _mm_rsqrt_ps(input); // Relative error is 1.5 * 2^-12 for Intel processors. AMD between 11-14 bits.

	// Do one iteration of Newton-Raphson.
	// This approximately doubles the precision, close enough
	// to full float precision of 24 bits to call it "exact".
	__m128 half = _mm_set_ps1(0.5f);
	__m128 half_input = _mm_mul_ps(half, input);
	__m128 guess_squared = _mm_mul_ps(guess, guess);
	__m128 one_and_half = _mm_set_ps1(1.5f);
	__m128 refined_once = _mm_mul_ps(guess, _mm_sub_ps(one_and_half, _mm_mul_ps(half_input, guess_squared)));
	Vector<4, float> result;
	_mm_storeu_ps(&result[0], refined_once);
	return result;
}

template <>
inline Vector<4, float> ReciprocalSqrt_Exact<Vector<4, float>>(const Vector<4, float>& t) {
	__m128 input = _mm_loadu_ps(t.RawData());
	__m128 guess = _mm_rsqrt_ps(input); // Relative error is 1.5 * 2^-12 for Intel processors. AMD between 11-14 bits.

	// Do one iteration of Newton-Raphson.
	// This approximately doubles the precision, close enough
	// to full float precision of 24 bits to call it "exact".
	__m128 half = _mm_set_ps1(0.5f);
	__m128 half_input = _mm_mul_ps(half, input);
	__m128 guess_squared = _mm_mul_ps(guess, guess);
	__m128 one_and_half = _mm_set_ps1(1.5f);
	__m128 refined_once = _mm_mul_ps(guess, _mm_sub_ps(one_and_half, _mm_mul_ps(half_input, guess_squared)));
	__m128 refined_twice = _mm_mul_ps(refined_once, _mm_sub_ps(one_and_half, _mm_mul_ps(half_input, _mm_mul_ps(refined_once, refined_once))));
	Vector<4, float> result;
	_mm_storeu_ps(&result[0], refined_twice);
	return result;
}

template <>
inline Vector<4, float> Reciprocal_Approx<Vector<4, float>>(const Vector<4, float>& t) {
	__m128 input = _mm_loadu_ps(t.RawData());
	__m128 guess = _mm_rcp_ps(input);
	Vector<4, float> vRecip;
	_mm_storeu_ps(vRecip.RawData(), guess);
	return vRecip;
}

template <>
inline Vector<4, float> Reciprocal_Close<Vector<4, float>>(const Vector<4, float>& t) {
	__m128 input = _mm_loadu_ps(t.RawData());
	__m128 guess = _mm_rcp_ps(input);

	// Do one iteration of Newton-Raphson.
	// This approximately doubles the precision, giving us a very close reciprocal
	// x_new = x * (2.0 - t * x)
	__m128 two = _mm_set1_ps(2.0f);
	__m128 refined_once = _mm_mul_ps(guess, _mm_sub_ps(two, _mm_mul_ps(input, guess)));
	Vector<4, float> result;
	_mm_storeu_ps(&result[0], refined_once);
	return result;
}

template <>
inline Vector<4, float> Reciprocal_Exact<Vector<4, float>>(const Vector<4, float>& t) {
#if 1
	__m128 input = _mm_loadu_ps(t.RawData());
	__m128 one = _mm_set1_ps(1.0f);
	__m128 recip = _mm_div_ps(one, input);
	Vector<4, float> result;
	_mm_storeu_ps(&result[0], recip);
	return result;
#else
	__m128 input = _mm_loadu_ps(t.RawData());
	__m128 guess = _mm_rcp_ps(input);

	// Do two iterations of Newton-Raphson.
	__m128 two = _mm_set1_ps(2.0f);
	__m128 refined_once = _mm_mul_ps(guess, _mm_sub_ps(two, _mm_mul_ps(input, guess)));
	__m128 refined_twice = _mm_mul_ps(refined_once, _mm_sub_ps(two, _mm_mul_ps(input, refined_once)));
	Vector<4, float> result;
	_mm_storeu_ps(&result[0], refined_twice);
	return result;
#endif
}

template <typename T>
using Vector2 = Vector<2, T>;
template <typename T>
using Vector3 = Vector<3, T>;
template <typename T>
using Vector4 = Vector<4, T>;
typedef Vector<2, float> Vector2f;
typedef Vector<3, float> Vector3f;
typedef Vector<4, float> Vector4f;
typedef Vector<2, double> Vector2d;
typedef Vector<3, double> Vector3d;
typedef Vector<4, double> Vector4d;
typedef Vector<2, uint8_t> Vector2b;
typedef Vector<3, uint8_t> Vector3b;
typedef Vector<4, uint8_t> Vector4b;
typedef Vector<2, int> Vector2i;
typedef Vector<3, int> Vector3i;
typedef Vector<4, int> Vector4i;

} // namespace KEP