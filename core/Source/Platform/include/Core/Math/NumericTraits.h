///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <limits>

namespace KEP {

template <typename T, bool IsIntegral = std::numeric_limits<T>::is_integer>
struct NumericTraits_IsIntegral {};

template <typename T>
struct NumericTraits_IsIntegral<T, false> {
#if 0 // Syntax not supported by Visual Studio 2013
	static constexpr T s_DistanceTolerance = std::numeric_limits<T>::epsilon();
	static constexpr T s_CosineSquaredTolerance = std::numeric_limits<T>::epsilon();
	static constexpr T s_CosineTolerance = std::sqrt<T>(s_CosineSquaredTolerance);
#endif

	static const T DistanceTolerance() { return std::numeric_limits<T>::epsilon(); }
	static const T CosineSquaredTolerance() { return std::numeric_limits<T>::epsilon(); }
	//static const T CosineTolerance() { return std::sqrt<T>(CosineSquaredTolerance()); }
};

template <typename T>
struct NumericTraits_IsIntegral<T, true> {
#if 0 // Syntax not supported by Visual Studio 2013
	static constexpr T s_DistanceTolerance = 0;
	static constexpr T s_CosineSquaredTolerance = 0;
	static constexpr T s_CosineTolerance = 0;
#endif

	static const T DistanceTolerance() { return std::numeric_limits<T>::epsilon(); }
	static const T CosineSquaredTolerance() { return std::numeric_limits<T>::epsilon(); }
	//static const T CosineTolerance() { return std::sqrt<T>(CosineSquaredTolerance()); }
};

template <typename T>
struct NumericTraits
		: public NumericTraits_IsIntegral<T, std::numeric_limits<T>::is_integer> {
};

} // namespace KEP
