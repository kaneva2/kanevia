///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace gte {
// Forward declarations of gte math types.
template <int N, typename Real>
class Vector;

template <int NumRows, int NumCols, typename Real>
class Matrix;

template <typename Real>
class Quaternion;
}; // namespace gte

namespace KEP {

// Forward declarations of KEP math types.
template <size_t N, typename Real>
class Vector;

template <size_t NumRows, size_t NumCols, typename Real>
class Matrix;

template <typename Real>
class Quaternion;

// KEP <-> gte conversions.
template <size_t N, typename T>
inline gte::Vector<N, T>& ToGTE(Vector<N, T>& v) {
	return reinterpret_cast<gte::Vector<N, T>&>(v);
}

template <size_t N, typename T>
inline const gte::Vector<N, T>& ToGTE(const Vector<N, T>& v) {
	return reinterpret_cast<const gte::Vector<N, T>&>(v);
}

template <size_t M, size_t N, typename T>
inline gte::Matrix<M, N, T>& ToGTE(Matrix<M, N, T>& m) {
	return reinterpret_cast<gte::Matrix<M, N, T>&>(m);
}

template <size_t M, size_t N, typename T>
inline const gte::Matrix<M, N, T>& ToGTE(const Matrix<M, N, T>& m) {
	return reinterpret_cast<const gte::Matrix<M, N, T>&>(m);
}

template <typename T>
inline gte::Quaternion<T>& ToGTE(Quaternion<T>& q) {
	return reinterpret_cast<gte::Quaternion<T>&>(q);
}
template <typename T>
inline const gte::Quaternion<T>& ToGTE(const Quaternion<T>& q) {
	return reinterpret_cast<const gte::Quaternion<T>&>(q);
}

}; // namespace KEP
