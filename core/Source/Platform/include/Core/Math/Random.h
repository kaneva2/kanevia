///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Simd.h"

namespace KEP {

template <uint32_t a = 1664525, uint32_t c = 1013904223>
class TRandomGeneratorI32 {
public:
	TRandomGeneratorI32(uint32_t initialState = 0) :
			m_state(initialState) {}
	uint32_t GetNext() {
		uint32_t newstate = m_state * a + c;
		m_state = newstate;
		return newstate;
	}

private:
	uint32_t m_state;
};
using RandomGeneratorI32 = TRandomGeneratorI32<>;

inline uint32_t Random32() {
	static RandomGeneratorI32 s_generator(0);
	return s_generator.GetNext();
}

inline uint32_t RandomBool() {
	return Random32() >> 31;
}

// Returns a float in the half-open interval [0,1)
inline float RandomFloat() {
	uint32_t iRand = Random32();
	__m128i rand_1_2_int = _mm_cvtsi32_si128(0x3f800000 | (iRand >> 9)); // Will be in interval [1,2)
	__m128 rand_1_2_float = _mm_castsi128_ps(rand_1_2_int);
	__m128 fOne = _mm_set_ss(1.0f);
	__m128 rand_0_1_float = _mm_sub_ss(rand_1_2_float, fOne);
	float fRet = _mm_cvtss_f32(rand_0_1_float);
	return fRet;
}

// Returns a float in the closed interval [0,1]
inline float RandomFloatClosed() {
	uint32_t iRand = Random32() >> 1; // Clear sign bit
	static constexpr float kfMultiplier = 1.0 / 0x7fffffff;
	__m128 fMult = _mm_set_ss(kfMultiplier);
	__m128 uninitialized;
	__m128 fRand = _mm_cvt_si2ss(uninitialized, iRand);
	__m128 rand_0_1 = _mm_mul_ss(fMult, fRand);
	float fRet = _mm_cvtss_f32(rand_0_1);
	return fRet;
}

inline Simd4i32 Random32Simd() {
	static uint32_t state32 alignas(16)[4] = {
		0x18308175,
		0x5822c182,
		0x99a28d5f,
		0xd0897324,
	};

	Simd4i32 a(1664525);
	Simd4i32 c(1013904223);
	Simd4i32& state = reinterpret_cast<Simd4i32&>(state32);
	Simd4i32 newstate = state * a + c;
	state = newstate;
	return newstate;
}

// Returns 4 floats in the half-open interval [0,1)
inline Simd4f RandomFloatSimd() {
	Simd4i32 iRand = Random32Simd();
	Simd4i32 rand_1_2_int = Simd4i32(0x3f800000) | ((iRand >> 9) & Simd4i32(0x7fffff)); // SSE only has signed right shift so we need to clear the high bits.
	Simd4f rand_1_2_float = _mm_castsi128_ps(rand_1_2_int);
	Simd4f fOne(1.0f);
	Simd4f rand_0_1_float = rand_1_2_float - fOne;
	return rand_0_1_float;
}

} // namespace KEP
