///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPMathConfig.h"
#include "Vector.h"
#include "Matrix.h"
#include "Quaternion.h"
#include <limits>

namespace KEP {

//KEPMATH_EXPORT bool TestKEPMath();

} // namespace KEP
