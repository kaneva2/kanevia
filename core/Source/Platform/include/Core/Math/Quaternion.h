///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Vector.h"

namespace KEP {

template <typename T>
class Quaternion : public Vector<4, T> {
public:
	Quaternion() = default;
	~Quaternion() = default;
	Quaternion(const Quaternion&) = default;
	Quaternion& operator=(const Quaternion&) = default;

	Quaternion(const T& xx, const T& yy, const T& zz, const T& ww);
	Quaternion(const Vector<3, T>& vAxis, const T& angleRadians);
	Quaternion(unsigned int iAxis, const T& angleRadians);
	explicit Quaternion(const Vector<4, T>& v);

	void MakeIdentity();
	static const Quaternion& GetIdentity();

	void MakeRotation(const Vector<3, T>& vAxis, const T& angleRadians);
	void MakeRotation(unsigned int iAxis, const T& angleRadians);
	Quaternion GetConjugate() const;

	// Note: Axis will be the zero vector if quaternion represents rotation of zero degrees.
	const Vector<3, T>& GetAxis() const;
	Vector<3, T>& GetAxis();

	Quaternion& operator*=(const Quaternion& right);
};

template <typename T>
inline Quaternion<T>::Quaternion(const T& xx, const T& yy, const T& zz, const T& ww) :
		Vector(xx, yy, zz, ww) {
}

template <typename T>
inline Quaternion<T>::Quaternion(const Vector<3, T>& axis, const T& angleRadians) {
	this->MakeRotation(axis, angleRadians);
}

template <typename T>
inline Quaternion<T>::Quaternion(unsigned int iAxis, const T& angleRadians) {
	this->MakeRotation(iAxis, angleRadians);
}

template <typename T>
inline Quaternion<T>::Quaternion(const Vector<4, T>& v) :
		Vector<4, T>(v) {
}

template <typename T>
inline void Quaternion<T>::MakeIdentity() {
	Set(0, 0, 0, 1);
}

template <typename T>
inline const Quaternion<T>& Quaternion<T>::GetIdentity() {
	static const /*constexpr*/ Quaternion qIdent(0, 0, 0, 1);
	return qIdent;
}

template <typename T>
inline void Quaternion<T>::MakeRotation(const Vector<3, T>& vAxis, const T& angleRadians) {
	T halfAngle = angleRadians * T(0.5);
	T sinHalf = std::sin(halfAngle);
	T cosHalf = std::cos(halfAngle);
	T recipLen = vAxis.ReciprocalLength();
	T factor = sinHalf * recipLen;
	this->SubVector<3>() = vAxis * factor;
	this->w = cosHalf;
}

template <typename T>
inline void Quaternion<T>::MakeRotation(unsigned int iAxis, const T& angleRadians) {
	T halfAngle = angleRadians * T(0.5);
	T sinHalf = std::sin(halfAngle);
	T cosHalf = std::cos(halfAngle);
	(*this)[0] = 0;
	(*this)[1] = 0;
	(*this)[2] = 0;
	(*this)[iAxis] = sinHalf;
	this->w = cosHalf;
}

template <typename T>
inline const Vector<3, T>& Quaternion<T>::GetAxis() const {
	return SubVector<3>(0);
}

template <typename T>
inline Vector<3, T>& Quaternion<T>::GetAxis() {
	return SubVector<3>(0);
}

template <typename T>
inline Quaternion<T> Quaternion<T>::GetConjugate() const {
	return Quaternion(-x, -y, -z, w);
}

// Assumes normalized quaternion
template <typename T>
inline Vector<3, T> TransformVector(const Quaternion<T>& q, const Vector<3, T>& v) {
	Quaternion<T> q2;
	q2.GetAxis() = q.W() * v + q.GetAxis().Cross(v);
	q2.W() = -q.GetAxis().Dot(v);
	return q.W() * q2.GetAxis() - q2.W() * q.GetAxis() - q2.GetAxis().Cross(q.GetAxis());
}

template <typename T>
inline Vector<3, T> TransformPoint(const Quaternion<T>& q, const Vector<3, T>& pt) {
	return TransformVector(q, pt); // Rotations transform vectors and points the same way.
}

template <typename T>
inline Quaternion<T> operator*(const Quaternion<T>& left, const Quaternion<T>& right) {
	Quaternion<T> q;
	q.w = left.W() * right.W() - left.GetAxis().Dot(right.GetAxis());
	q.GetAxis() = left.W() * right.GetAxis() + right.W() * left.GetAxis() + left.GetAxis().Cross(right.GetAxis());
	return q;
}

template <typename T>
inline Quaternion<T>& Quaternion<T>::operator*=(const Quaternion<T>& right) {
	*this = *this * right;
}

typedef Quaternion<float> Quaternionf;
typedef Quaternion<double> Quaterniond;

} // namespace KEP