///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Vector.h"

namespace KEP {

// First template parameter is the number of dimensions of the space that the plane is embedded in.
// Typically we will use N==3 for a plane in 3-D space.
template <size_t N, typename T>
class Plane {
public:
	Plane() {}
	Plane(const Plane& pln) :
			m_Coefficients(pln.m_Coefficients) {}
	Plane(const Vector<N, T>& pt, const Vector<N, T>& vNormal) {
		SetNormal(vNormal);
		SetOffset(-pt.Dot(vNormal));
	}
	Plane(const Vector<N, T>& pt0, const Vector<N, T>& pt1, const Vector<N, T>& pt2) { InitFromPoints(pt0, pt1, pt2); }
	explicit Plane(const Vector<N + 1, T>& vCoefficients) :
			m_Coefficients(vCoefficients) {}

	template <typename... Args>
	Plane(const std::enable_if_t<sizeof...(Args) == N, T>& arg0, Args&&... args) { m_Coefficients.Set(arg0, args...); }

	Plane& operator=(const Plane& pln) {
		m_Coefficients = pln.m_Coefficients;
		return *this;
	}

	// Set coefficients
	template <typename... Args>
	void Set(const std::enable_if_t<sizeof...(Args) == N, T>& arg0, Args&&... args) { m_Coefficients.Set(arg0, args...); }
	void Set(const Vector<N + 1, T>& vCoefficients) { m_Coefficients = vCoefficients; }

	void InitFromPointNormal(const Vector<N, T>& pt, const Vector<N, T>& vNormal) {
		SetNormal(vNormal);
		SetOffset(-pt.Dot(vNormal));
	}
	void InitFromPoints(const Vector<N, T>& pt0, const Vector<N, T>& pt1, const Vector<N, T>& pt2) { InitFromPointVectors(pt0, pt1 - pt0, pt2 - pt0); }
	void InitFromPointVectors(const Vector<N, T>& pt, const Vector<N, T>& v0, const Vector<N, T>& v1) { SetFromPointNormal(pt, v0.Cross(v1)); }
	void InitFromPointsVector(const Vector<N, T>& pt0, const Vector<N, T>& pt1, const Vector<N, T>& v) { InitFromPointVectors(pt0, pt1 - pt0, v); }

	// Get/Set the normal vector
	Vector<N, T>& GetNormal() { return m_Coefficients.SubVector<N>(); }
	const Vector<N, T>& GetNormal() const { return m_Coefficients.SubVector<N>(); }
	void SetNormal(const Vector<N, T>& v) { m_Coefficients.SubVector<N>() = v; }

	T GetNormalLength() const { return GetNormal().Length(); }
	T GetNormalLengthSquared() const { return GetNormal().LengthSquared(); }
	void Normalize() {
		T len2 = GetNormalLengthSquared();
		if (len2 != 0)
			m_Coefficients *= ReciprocalSqrt(len2);
	}
	void NormalizeUnchecked() {
		T len2 = GetNormalLengthSquared();
		m_Coefficients *= ReciprocalSqrt(len2);
	}
	Plane GetNormalized() const {
		Plane pln(*this);
		pln.Normalize();
		return pln;
	}
	Plane GetNormalizedUnchecked() const {
		Plane pln(*this);
		pln.NormalizeUnchecked();
		return pln;
	}

	// Get/Set the offset.
	// The offset is the coefficient that is not part of the normal.
	// The 'w' coordinate in 3D.
	T& GetOffset() { return m_Coefficients[N]; }
	const T& GetOffset() const { return m_Coefficients[N]; }
	void SetOffset(const T& d) { m_Coefficients[N] = d; }

	T SignedRelativeDistance(const Vector<N, T>& v) const { return GetNormal().Dot(v) + GetOffset(); }
	T RelativeDistance(const Vector<N, T>& v) const { return std::abs(SignedRelativeDistance(v)); }
	T SignedDistance(const Vector<N, T>& v) const { return SignedRelativeDistance(v) * ReciprocalSqrt(GetNormalLengthSquared()); }
	T Distance(const Vector<N, T>& v) const { return std::abs(SignedDistance(v)); }

	static Plane<N, T> CreateFromPointNormal(const Vector<N, T>& pt, const Vector<N, T>& vNormal) {
		Plane<N, T> pln;
		pln.InitFromPointNormal(pt, vNormal);
		return pln;
	}
	static Plane<N, T> CreateFromPoints(const Vector<N, T>& pt0, const Vector<N, T>& pt1, const Vector<N, T>& pt2) {
		Plane<N, T> pln;
		pln.InitFromPoints(pt0, pt1, pt2);
		return pln;
	}
	static Plane<N, T> CreateFromPointVectors(const Vector<N, T>& pt, const Vector<N, T>& v0, const Vector<N, T>& v1) {
		Plane<N, T> pln;
		pln.InitFromPointVectors(pt, v0, v1);
		return pln;
	}
	static Plane<N, T> CreateFromPointsVector(const Vector<N, T>& pt0, const Vector<N, T>& pt1, const Vector<N, T>& v) {
		Plane<N, T> pln;
		pln.InitFromPointsVector(pt0, pt1, v);
		return pln;
	}

	Vector<N + 1, T>& AsVector() { return m_Coefficients; }
	const Vector<N + 1, T>& AsVector() const { return m_Coefficients; }

	T& operator[](size_t i) { return m_Coefficients[i]; }
	const T& operator[](size_t i) const { return m_Coefficients[i]; }

private:
	Vector<N + 1, T> m_Coefficients;
};

// Test whether a box is outside a plane. Outside is on the side of the plane that the normal is pointing.
// The box is specified by its center point and a vector containing half the size of the box.
// ptBoxCenter = 0.5f * (ptBoxMin + ptBoxMax)
// vBoxHalfSize = 0.5f * (ptBoxMax - ptBoxMin)
template <size_t N, typename T>
inline bool IsBoxCenterHalfSizeOutsideOfPlane(const Vector<N, T>& ptBoxCenter, const Vector<N, T>& vBoxHalfSize, const Plane<N, T>& plane) {
	T dist = plane.SignedRelativeDistance(ptBoxCenter);
	if (dist > std::abs(vBoxHalfSize[0] * plane[0]) + std::abs(vBoxHalfSize[1] * plane[1]) + std::abs(vBoxHalfSize[2] * plane[2]))
		return true;
	return false;
}

template <size_t N, typename T>
inline bool IsSphereOutsideOfNormalizedPlane(const Vector<N, T>& ptSphereCenter, const T& radius, const Plane<N, T>& plane) {
	T dist = plane.SignedRelativeDistance(ptSphereCenter);
	if (dist > radius)
		return true;
	return false;
}

using Plane2f = Plane<2, float>;
using Plane2d = Plane<2, double>;
using Plane3f = Plane<3, float>;
using Plane3d = Plane<3, double>;

} // namespace KEP
