///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Primitive.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>

namespace KEP {

template <typename T>
class PrimitiveMesh {
public:
	struct Vert {
		Vector3<T> pos;
		Vector3<T> nml;
	};

	static void AddVert(const Vector3<T>& pos, const Vector3<T>& nml, typename Primitive<T>::TriList& triangleList) {
		assert(triangleList.positions.size() == triangleList.normals.size());
		triangleList.positions.push_back(pos);
		triangleList.normals.push_back(nml);
	}

	static void AddVert(const Vert& v, typename Primitive<T>::TriList& triangleList) {
		AddVert(v.pos, v.nml, triangleList);
	}

	static void AddQuad(const Vert& v0, const Vert& v1, const Vert& v2, const Vert& v3, bool recalcNormal, typename Primitive<T>::TriList& triangleList) {
		assert(triangleList.positions.size() % 3 == 0);
		Vector3<T> calculatedNormal;
		if (recalcNormal) {
			auto vec1 = v1.pos - v0.pos;
			auto vec2 = v2.pos - v1.pos;
			calculatedNormal = vec1.Cross(vec2);
		}
		// Triangle 1
		AddVert(v0.pos, recalcNormal ? calculatedNormal : v0.nml, triangleList);
		AddVert(v1.pos, recalcNormal ? calculatedNormal : v1.nml, triangleList);
		AddVert(v2.pos, recalcNormal ? calculatedNormal : v2.nml, triangleList);
		// Triangle 2
		AddVert(v2.pos, recalcNormal ? calculatedNormal : v2.nml, triangleList);
		AddVert(v3.pos, recalcNormal ? calculatedNormal : v3.nml, triangleList);
		AddVert(v0.pos, recalcNormal ? calculatedNormal : v0.nml, triangleList);
	}

	static void AddSymmetricTrianglesByCenter(size_t startIndex, size_t endIndex, const Vector3<T>& center, typename Primitive<T>::TriList& triangleList) {
		assert(triangleList.positions.size() == triangleList.normals.size());
		assert((endIndex - startIndex) % 3 == 0);
		Vector3<T> posSum = center * 2;
		for (size_t vertIndex = startIndex; vertIndex < endIndex; ++vertIndex) {
			triangleList.positions.push_back(posSum - triangleList.positions[vertIndex]);
			triangleList.normals.push_back(-triangleList.normals[vertIndex]);
		}
	}

	// Generate triangle list for a hexahedron.
	// Take a near quad and a far quad, both begins with left bottom vertex and runs clockwise order when looked into them from the front
	static void AddHexahedron(const Vert& vn0, const Vert& vn1, const Vert& vn2, const Vert& vn3, const Vert& vf0, const Vert& vf1, const Vert& vf2, const Vert& vf3, typename Primitive<T>::TriList& triangleList) {
		assert(triangleList.positions.size() == triangleList.normals.size());

		// Winding order: CW
		// Near
		AddQuad(vn0, vn1, vn2, vn3, false, triangleList);
		// Far
		AddQuad(vf1, vf0, vf3, vf2, false, triangleList);
		// Left
		AddQuad(vn0, vf0, vf1, vn1, true, triangleList);
		// Right
		AddQuad(vn3, vn2, vf2, vf3, true, triangleList);
		// Bottom
		AddQuad(vn0, vn3, vf3, vf0, true, triangleList);
		// Top
		AddQuad(vn1, vf1, vf2, vn2, true, triangleList);
	}

	// Generate triangle list for a circle on a plane perpendicular to the UP axis. Clockwise parameter is used for
	// triangle winding order when look into the triangle from positive UP.
	template <typename UP>
	static void AddCircle(const Vector3<T>& center, T r, size_t numSectors, bool clockwise, typename Primitive<T>::TriList& triangleList) {
		assert(triangleList.positions.size() == triangleList.normals.size());

		for (size_t sec = 1; sec <= numSectors / 2; sec++) {
			T lastRad = M_PI * 2 / numSectors * (sec - 1);
			T currRad = M_PI * 2 / numSectors * sec;
			if (clockwise) {
				std::swap(lastRad, currRad);
			}

			Vector3<T> lastPos = UP::Normal(cos(lastRad) * r, sin(lastRad) * r);
			Vector3<T> currPos = UP::Normal(cos(currRad) * r, sin(currRad) * r);
			Vector3<T> normal(clockwise ? -UP::Basis() : UP::Basis());

			AddVert(center + lastPos, normal, triangleList);
			AddVert(center + currPos, normal, triangleList);
			AddVert(center, normal, triangleList);

			AddVert(center - lastPos, normal, triangleList);
			AddVert(center - currPos, normal, triangleList);
			AddVert(center, normal, triangleList);
		}
	}

	// Generate triangle list for a circular prism with a primary axis parallel to the UP axis
	template <typename UP>
	static void AddCircularPrism(const Vector3<T>& centerOfBase, T r0, T r1, T h, size_t numSectors, typename Primitive<T>::TriList& triangleList) {
		assert(triangleList.positions.size() == triangleList.normals.size());

		// Base
		AddCircle<UP>(centerOfBase, r0, numSectors, true, triangleList);

		// Top
		Vector3<T> centerOfTop = centerOfBase + UP::Tangent(h);
		AddCircle<UP>(centerOfTop, r1, numSectors, false, triangleList);

		// Tube
		Vector3<T> lastNormal(UP::Normal(1, 0));

		for (size_t sec = 1; sec <= numSectors / 2; sec++) {
			T radian = M_PI * 2 / numSectors * sec;
			Vector3<T> currNormal(UP::Normal(cos(radian), sin(radian)));

			// Winding order: CW
			for (size_t half = 0; half < 2; half++) {
				AddQuad(
					{ centerOfBase + lastNormal * r0, lastNormal },
					{ centerOfTop + lastNormal * r1, lastNormal },
					{ centerOfTop + currNormal * r1, currNormal },
					{ centerOfBase + currNormal * r0, currNormal },
					false, triangleList);

				// Flip normals for the symmetric quad
				lastNormal = -lastNormal;
				currNormal = -currNormal;
			}

			lastNormal = currNormal;
		}
	}

	// Generate triangle list for a UV sphere of radius = 1 at the origin
	static void AddUnitUVSphere(size_t numSegments, size_t numRings, typename Primitive<T>::TriList& triangleList) {
		assert(triangleList.positions.size() == triangleList.normals.size());
		size_t startIndex = triangleList.positions.size();

		std::vector<Vert> lastRingVertices, currRingVertices;

		// pole
		lastRingVertices.resize(numRings);
		for (size_t ring = 0; ring < numRings; ring++) {
			lastRingVertices[ring].pos = lastRingVertices[ring].nml = Vector3<T>::Basis(1);
		}

		currRingVertices.resize(numRings);

		// down to equator
		for (size_t seg = 1; seg <= numSegments / 2; seg++) {
			T altitude = M_PI_2 - M_PI / numSegments * seg;
			T y = sin(altitude);
			T xz = cos(altitude);

			currRingVertices[0].pos.x = xz;
			currRingVertices[0].pos.y = y;
			currRingVertices[0].pos.z = 0;
			currRingVertices[0].nml = currRingVertices[0].pos;

			// vertices on latitude circle
			for (size_t ring = 1; ring < numRings; ring++) {
				currRingVertices[ring].pos.x = xz * cos(M_PI * 2 / numRings * ring);
				currRingVertices[ring].pos.y = y;
				currRingVertices[ring].pos.z = xz * sin(M_PI * 2 / numRings * ring);
				currRingVertices[ring].nml = currRingVertices[ring].pos;
			}

			// add triangles
			for (size_t ring = 0; ring < numRings; ring++) {
				size_t ringNext = (ring + 1) % numRings;
				// Winding order: CW
				AddQuad(lastRingVertices[ring], lastRingVertices[ringNext], currRingVertices[ringNext], currRingVertices[ring], false, triangleList);
			}

			// Roll over
			lastRingVertices = currRingVertices;
		}

		size_t endIndex = triangleList.positions.size();

		// Mirror it for the bottom hemisphere
		AddSymmetricTrianglesByCenter(startIndex, endIndex, Vector3<T>::Zero(), triangleList);
	}
};

} // namespace KEP