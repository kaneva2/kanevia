///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Rotation.h"

namespace KEP {

template <size_t N, typename T>
class Plane;

template <size_t N, typename T>
inline void PreTranslate(const Vector<3, T>& vOffset, InOut<Matrix<4, N, T>>& transformInOut) {
	static_assert(N == 3 || N == 4, "Improper size matrix");
	float fOffsetX = vOffset.Dot(transformInOut.get().GetColumn<3>(0));
	float fOffsetY = vOffset.Dot(transformInOut.get().GetColumn<3>(1));
	float fOffsetZ = vOffset.Dot(transformInOut.get().GetColumn<3>(2));
	transformInOut.get()(3, 0) += fOffsetX;
	transformInOut.get()(3, 1) += fOffsetY;
	transformInOut.get()(3, 2) += fOffsetZ;
}

template <size_t N, typename T>
inline void PreTranslate(const Matrix<4, N, T>& transformIn, const Vector<3, T>& vOffset, Out<Matrix<4, N, T>>& transformOut) {
	static_assert(N == 3 || N == 4, "Improper size matrix");
	transformOut.set(transformIn);
	PreTranslate(vOffset, inout(transformOut.get()));
}

template <size_t N = 4, typename T>
inline Matrix<4, N, T> TranslationToMatrix(const Vector<3, T>& vTranslation) {
	Matrix<4, N, T> transform;
	static_assert(N == 3 || N == 4, "Improper size matrix");
	for (size_t i = 0; i < 3; ++i)
		transform[i] = Vector<N, T>::Basis(i);
	for (size_t i = 0; i < 3; ++i)
		transform(3, i) = vTranslation[i];
	if (N == 4)
		transform(3, 3) = 1;
	return transform;
}

template <size_t N, typename T>
inline void TranslationToMatrix(const Vector<3, T>& vTranslation, Out<Matrix<4, N, T>> transform) {
	transform.set(TranslationtoMatrix(vTranslation));
}

// Rotation, translation - in that order
template <size_t N, typename T>
inline void RotationTranslationToMatrix(const Quaternion<T>& qRotation, const Vector<3, T>& vTranslation,
	Out<Matrix<4, N, T>> transform) {
	static_assert(N == 3 || N == 4, "Improper size matrix");
	ConvertRotation(qRotation, out(transform));
	for (size_t i = 0; i < 3; ++i) {
		transform.get()(3, i) = vTranslation[i];
	}
}

// Scale, rotation, translation - in that order
template <size_t N, typename T>
inline void ScaleRotationTranslationToMatrix(const Vector<3, T>& vScale, const Quaternion<T>& qRotation, const Vector<3, T>& vTranslation,
	Out<Matrix<4, N, T>> transform) {
	static_assert(N == 3 || N == 4, "Improper size matrix");
	ConvertRotation(qRotation, out(transform));
	for (size_t i = 0; i < 3; ++i) {
		for (size_t j = 0; j < N; ++j) {
			transform.get()(i, j) *= vScale[i];
		}
		transform.get()(3, i) = vTranslation[i];
	}
}

// Convert a 4x3 or 4x4 matrix transform into a scale, rotation, and translation - to be applied in that order.
template <size_t N, typename T>
inline void MatrixToScaleRotationTranslation(const Matrix<4, N, T>& transform,
	Out<Vector<3, T>> vScale, Out<Quaternion<T>> qRotation, Out<Vector<3, T>> vTranslation) {
	static_assert(N == 3 || N == 4, "Improper size matrix");
	Matrix33f m33 = transform.GetSubMatrixRefAt<3, 3>(0, 0);
	for (size_t i = 0; i < 3; ++i) {
		vScale.get()[i] = m33.GetRow(i).Length();
		if (vScale.get()[i] != 0)
			m33.GetRow(i) *= 1 / vScale.get()[i];
	}
	ConvertRotation(m33, out(qRotation));

	vTranslation.get() = transform.GetTranslation();
}

// Create a perspective matrix conforming to D3D projection space standard
// Microsoft #defines "near" and "far" (as empty) so we can't use them as variable names.
template <typename T>
inline void MakeD3DStylePerspective(std::common_type_t<T> fovY, std::common_type_t<T> aspectXY,
	std::common_type_t<T> near_, std::common_type_t<T> far_, Out<Matrix<4, 4, T>> matrix) {
	T tanFOVY = std::tan(fovY / 2);
	T cotFOVY = 1 / tanFOVY;
	T far_over_far_minus_near = far_ / (far_ - near_);
	matrix.get().MakeZero();
	matrix.get()(0, 0) = cotFOVY / aspectXY;
	matrix.get()(1, 1) = cotFOVY;
	matrix.get()(2, 2) = far_over_far_minus_near;
	matrix.get()(3, 2) = -near_ * far_over_far_minus_near;
	matrix.get()(2, 3) = 1;
}

// Create view matrix matching D3DXMatrixLookAtLH behavior.
template <typename T>
inline void MakeD3DStyleLookAtLH(const Vector<3, T>& ptEye, const Vector<3, T>& ptAt, const Vector<3, T>& vUp,
	Out<Matrix<4, 4, T>> matrix) {
	Vector<3, T> vDir = ptAt - ptEye;
	Vector<3, T> vZ = vDir.GetNormalized();
	Vector<3, T> vX = vUp.Cross(vZ).GetNormalized();
	Vector<3, T> vY = vZ.Cross(vX).GetNormalized();
	matrix.get().GetColumnRef(0) = vX;
	matrix.get().GetColumnRef(1) = vY;
	matrix.get().GetColumnRef(2) = vZ;
	matrix.get()(3, 0) = -vX.Dot(ptEye);
	matrix.get()(3, 1) = -vY.Dot(ptEye);
	matrix.get()(3, 2) = -vZ.Dot(ptEye);
	matrix.get().GetColumnRef(3) = Vector<4, T>(0, 0, 0, 1);
}

// Creates a rotation matrix given orthonormal basis components.
// idxMissing is the basis vector not provided:
// idxMissing == 0 -> given vectors are Y and Z,
// idxMissing == 1 -> given vectors are Z and X,
// idxMissing == 2 -> given vectors are X and Y,
// Only modifies upper left 3x3 submatrix
template <size_t idxMissing, size_t M, size_t N, typename T, typename = std::enable_if_t<(M == 3 || M == 4) && (N == 3 || N == 4)>>
inline bool SetOrientationFromOrthonormal(
	const Vector<3, T>& v1,
	const Vector<3, T>& v2,
	InOut<Matrix<M, N, T>> matrix) {
	Vector<3, T> v3 = v1.Cross(v2);
	if (v3 == Vector<3, T>::Zero())
		return false;
	matrix.get()[idxMissing].SubVector<3>() = v3;
	matrix.get()[(idxMissing + 1) % 3].SubVector<3>() = v1;
	matrix.get()[(idxMissing + 1) % 3].SubVector<3>() = v2;
	return true;
}

// Create a transform so that the local directions (given in the function name) will point in the
// directions given by the first two function arguments. The second argument will be forced to be orthogonal to the first.
// The -FromOrthonormal function (used to implement the others) requires orthonormal inputs and takes as template parameter
// the index of the direction that is missing. I.e. 0 for the YZ version, 1 for XZ, and 2 for XY.
// For the 3 argument function, the local origin will be at ptPosition.
template <size_t idxMissing, size_t M, size_t N, typename T, typename = std::enable_if_t<(M == 3 || M == 4) && (N == 3 || N == 4)>>
inline bool ToCoordSysFromOrthonormal(
	const Vector<3, T>& v1,
	const Vector<3, T>& v2,
	Out<Matrix<M, N, T>> matrix) {
	Vector<3, T> v3 = v1.Cross(v2);
	if (v3 == Vector<3, T>::Zero())
		return false;
	matrix.get().SetRow(idxMissing, v3, 0.0f);
	matrix.get().SetRow((idxMissing + 1) % 3, v1, 0.0f);
	matrix.get().SetRow((idxMissing + 2) % 3, v2, 0.0f);
	if (M == 4)
		matrix.get().SetRow(3, Vector<3, T>(0, 0, 0), 1.0f);
	return true;
}

// See documentation above.
// This version of the function includes the position.
template <size_t idxMissing, size_t M, size_t N, typename T>
inline bool ToCoordSysFromOrthonormal(
	const Vector<3, T>& ptPosition,
	const Vector<3, T>& v1,
	const Vector<3, T>& v2,
	Out<Matrix<M, N, T>> matrix) {
	Vector<3, T> v3 = v1.Cross(v2);
	if (v3 == Vector<3, T>::Zero())
		return false;
	matrix.get().SetRow(idxMissing, v3, 0.0f);
	matrix.get().SetRow((idxMissing + 1) % 3, v1, 0.0f);
	matrix.get().SetRow((idxMissing + 2) % 3, v2, 0.0f);
	matrix.get().SetRow(3, ptPosition, 1.0f);
	return true;
}

// Creates a transform that places an object in the world such that its local X axis points in the
// direction vX and its local Y axis points in the direction vY.
template <size_t M, size_t N, typename T>
inline bool ToCoordSysXY(const Vector<3, T>& vX, const Vector<3, T>& vY, Out<Matrix<M, N, T>> matrix) {
	return ToCoordSysFromOrthonormal<2>(vX.GetNormalized(), vY.GetPartPerpendicularTo(vX).GetNormalized(), out(matrix));
}

// Creates a transform that places an object in the world such that its local Z axis points in the
// direction vZ and its local Y axis points in the direction vY.
template <size_t M, size_t N, typename T>
inline bool ToCoordSysZY(const Vector<3, T>& vZ, const Vector<3, T>& vY, Out<Matrix<M, N, T>> matrix) {
	return ToCoordSysFromOrthonormal<0>(vY.GetPartPerpendicularTo(vZ).GetNormalized(), vZ.GetNormalized(), out(matrix));
}

// Creates a transform that places an object in the world at position ptPosition and so that its
// local X axis points in the direction vX and its local Y axis points in the direction vY.
template <size_t M, size_t N, typename T>
inline bool ToCoordSysPosXY(const Vector<3, T>& ptPosition, const Vector<3, T>& vX, const Vector<3, T>& vY, Out<Matrix<M, N, T>> matrix) {
	return ToCoordSysFromOrthonormal<2>(ptPosition, vX.GetNormalized(), vY.GetPartPerpendicularTo(vX).GetNormalized(), out(matrix));
}

// Creates a transform that places an object in the world at position ptPosition and so that its
// local X axis points in the direction vX and its local Z axis points in the direction vZ.
template <size_t M, size_t N, typename T>
inline bool ToCoordSysPosXZ(const Vector<3, T>& ptPosition, const Vector<3, T>& vX, const Vector<3, T>& vZ, Out<Matrix<M, N, T>> matrix) {
	return ToCoordSysFromOrthonormal<1>(ptPosition, vZ.GetNormalized(), vX.GetPartPerpendicularTo(vZ).GetNormalized(), out(matrix));
}

// Creates a transform that places an object in the world at position ptPosition and so that its
// local Z axis points in the direction vZ and its local Y axis points in the direction vY.
template <size_t M, size_t N, typename T>
inline bool ToCoordSysPosZY(const Vector<3, T>& ptPosition, const Vector<3, T>& vZ, const Vector<3, T>& vY, Out<Matrix<M, N, T>> matrix) {
	return ToCoordSysFromOrthonormal<0>(ptPosition, vY.GetPartPerpendicularTo(vZ).GetNormalized(), vZ.GetNormalized(), out(matrix));
}

template <typename T>
inline bool IsRigidTransform(const Matrix<4, 4, T>& mtx) {
	const Vector<3, T>& v0 = mtx[0].SubVector<3>();
	const Vector<3, T>& v1 = mtx[1].SubVector<3>();
	const Vector<3, T>& v2 = mtx[2].SubVector<3>();

	const T tolerance = std::numeric_limits<T>::epsilon() * 16;
	if (!AreEqual(v0.LengthSquared(), 1.0f, tolerance))
		return false;
	if (!AreEqual(v1.LengthSquared(), 1.0f, tolerance))
		return false;
	if (!AreEqual(v2.LengthSquared(), 1.0f, tolerance))
		return false;

	if (!AreEqual(v0.Dot(v1), 0.0f, tolerance))
		return false;
	if (!AreEqual(v1.Dot(v2), 0.0f, tolerance))
		return false;
	if (!AreEqual(v2.Dot(v0), 0.0f, tolerance))
		return false;

	// No tolerance used for last column.
	if (mtx(0, 3) != 0.0f)
		return false;
	if (mtx(1, 3) != 0.0f)
		return false;
	if (mtx(2, 3) != 0.0f)
		return false;
	if (mtx(3, 3) != 1.0f)
		return false;

	return true;
}

// Creates up to 6 planes based on the given projection matrix.
// Order is -Y, -X, +Y, +X, -Z, +Z
// Bottom, Left, Top, Right, Near, Far
//
// Let M be our Projection matrix (optionally integrating world/model/view matrices to get
// the resulting planes in a the desired coordinate system).
// P is a view volume plane (column vector). V is a vertex (row vector) on the plane.
// V' is V in projection space and P' is P in projection space.
// Plane is defined as V * P = 0.
// V' * P' = 0
// V' = V * M
// (V * M) * P' = 0
// V * (M * P') = 0
// So P = M * P'
// Planes in projection space are (0,-1,0,-1), (-1,0,0,-1), (0,1,0,-1), (1,0,0,-1), (0,0,-1,0), (0,0,1,-1).
// We just add/subtraction the corresponding matrix columns.
template <typename T>
inline void CreateD3DViewVolumePlanes(const Matrix<4, 4, T>& mtxProj, Plane<3, T> aViewVolumePlanes[], size_t nMaxPlanes) {
	if (nMaxPlanes > 0)
		aViewVolumePlanes[0].Set(-mtxProj.GetColumn(1) - mtxProj.GetColumn(3));
	if (nMaxPlanes > 1)
		aViewVolumePlanes[1].Set(-mtxProj.GetColumn(0) - mtxProj.GetColumn(3));
	if (nMaxPlanes > 2)
		aViewVolumePlanes[2].Set(mtxProj.GetColumn(1) - mtxProj.GetColumn(3));
	if (nMaxPlanes > 3)
		aViewVolumePlanes[3].Set(mtxProj.GetColumn(0) - mtxProj.GetColumn(3));
	if (nMaxPlanes > 4)
		aViewVolumePlanes[4].Set(-mtxProj.GetColumn(2));
	if (nMaxPlanes > 5)
		aViewVolumePlanes[5].Set(mtxProj.GetColumn(2) - mtxProj.GetColumn(3));

	// Normalize planes for distance measurements.
	for (size_t i = 0; i < std::min<size_t>(6, nMaxPlanes); ++i)
		aViewVolumePlanes[i].Normalize();
}

// Create additional planes for complete culling of the view cone (ignoring near and far planes) against a bounding box.
// E.g. It is possible that a box is not outside either the left or top planes individually, but is outside
// the two in combination.
// The planes generated by this function handle this condition.
// It does not handle combinations of planes that include the near or far planes because these tend to be less useful.
// Formula is derived from the Separating Axis Theorem.
//
// aFrustumPlanes must have the at least 4 entries created from a call to CreateD3DFrustumPlanes.
template <typename T>
inline void CreateExtendedFrustumPlanesForBBoxTest(const Vector<3, T>& ptCamera, const Plane<3, T>* aFrustumPlanes, Out<std::vector<Plane<3, T>>> aAdditionalFrustumPlanes) {
	Vector3f avNormals[15] = {
		Vector3f(1, 0, 0),
		Vector3f(0, 1, 0),
		Vector3f(0, 0, 1),
	};
	Vector3f avFrustumEdges[4] = {
		aFrustumPlanes[1].GetNormal().Cross(aFrustumPlanes[0].GetNormal()),
		aFrustumPlanes[2].GetNormal().Cross(aFrustumPlanes[1].GetNormal()),
		aFrustumPlanes[3].GetNormal().Cross(aFrustumPlanes[2].GetNormal()),
		aFrustumPlanes[0].GetNormal().Cross(aFrustumPlanes[3].GetNormal()),
	};
	for (size_t i = 0; i < 3; ++i) {
		for (size_t j = 0; j < 4; ++j) {
			Vector3f vCross = avNormals[i].Cross(avFrustumEdges[j]);
			avNormals[3 + 4 * i + j] = vCross;
		}
	}
	for (size_t i = 0; i < 15; ++i) {
		Vector3f vNormal = avNormals[i];
		bool bPositive = false;
		bool bNegative = false;
		for (size_t j = 0; j < 4; ++j) {
			if (avFrustumEdges[j].Dot(vNormal) < 0)
				bNegative = true;
			else
				bPositive = true;
		}
		if (bNegative && bPositive)
			continue;
		if (bPositive)
			vNormal *= -1;
		else if (!bNegative)
			continue; // Invalid normal

		aAdditionalFrustumPlanes.get().push_back(Plane<3, T>(ptCamera, vNormal));
	}
}

} // namespace KEP
