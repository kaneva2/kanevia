///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

struct PrimitiveType {
	enum {
		Sphere,
		Cuboid,
		Cylinder,
		RectangularFrustum,
		CircularFrustum,
		NUM_TYPES
	};
};

inline const char* PrimitiveTypeStr(int primType) {
	static const char* s_primStr[] = {
		"Sphere",
		"Cuboid",
		"Cylinder",
		"RectFrust",
		"CircFrust"
	};
	return s_primStr[primType];
}

} // namespace KEP
