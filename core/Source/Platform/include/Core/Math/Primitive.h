///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/Matrix.h"
#include <memory>
#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>

namespace KEP {

template <typename T>
class Primitive {
public:
	// Axis 0/1/2 -> X/Y/Z
	template <size_t A>
	class Axis {
		template <size_t A>
		struct Next {};
		template <>
		struct Next<0> { static const int value = 1; };
		template <>
		struct Next<1> { static const int value = 2; };
		template <>
		struct Next<2> { static const int value = 0; };

	public:
		typedef Axis<Next<A>::value> NextAxis;
		typedef Axis<Next<Next<A>::value>::value> PrevAxis;

		static const size_t value = A;

		static const T& Get(const Vector3<T>& v) { return v[A]; }
		static T& Get(Vector3<T>& v) { return v[A]; }

		static Vector3<T> Basis() { return Tangent(static_cast<T>(1)); }
		static Vector3<T> Tangent(T dist) {
			Vector3<T> v;
			v[A] = dist;
			v[Next<A>::value] = 0;
			v[Next<Next<A>::value>::value] = 0;
			return v;
		}
		static Vector3<T> Normal(T right, T forward) {
			Vector3<T> v;
			v[A] = 0;
			v[Next<A>::value] = forward;
			v[Next<Next<A>::value>::value] = right;
			return v;
		}
		static bool Same(const Vector3<T>& v) { return v[A] == static_cast<T>(1) && v[Next<A>::value] == 0 && v[Next<Next<A>::value>::value] == 0; }
		static bool Colinear(const Vector3<T>& v) { return v[Next<A>::value] == 0 && v[Next<Next<A>::value>::value] == 0; }
		static T Dist(const Vector3<T>& v) { return v[Next<A>::value] * v[Next<A>::value] + v[Next<Next<A>::value>::value] * v[Next<Next<A>::value>::value]; }
	};

	Primitive() :
			m_degenerated(false),
			m_transformed(false) {
		m_objectToWorld.MakeIdentity();
		m_worldToObject.MakeIdentity();
	}

	virtual ~Primitive() {}

	// Primitive type - see PrimitiveType.h
	virtual int GetType() const = 0;

	// Update transform for primitive. Primitive class should implement following defaults: pt = (0, 0, 0), dir = (0, 0, 1) and up = (0, 1, 0)
	virtual void SetTransform(const Vector3<T>& pt, const Vector3<T>& dir, const Vector3<T>& up) {
		// Validate directional vectors
		assert(fabs(dir.LengthSquared() - 1.0f) < 1E-2);
		assert(fabs(up.LengthSquared() - 1.0f) < 1E-2);

		// Generic implementation - a subclass can override if it does not use dir and/or up vectors.
		m_objectToWorld.MakeIdentity();
		m_objectToWorld.GetRowZ() = dir;
		m_objectToWorld.GetRowY() = up;
		m_objectToWorld.GetRowX() = up.Cross(dir);

		m_worldToObject.MakeTransposeOf(m_objectToWorld);
		m_objectToWorld.GetTranslation() = pt;
		m_worldToObject.GetTranslation() = -TransformPoint_NonPerspective(m_worldToObject, pt);

		m_transformed = true;
	}

	const Vector3<T>& GetOrigin() const { return m_objectToWorld.GetTranslation(); }

	// Check if the (transformed) primitive contains a point in world space.
	virtual bool Contains(const Vector3<T>& pt) const {
		return m_transformed ?
				   Contains_ObjectSpace(TransformPoint_NonPerspective(m_worldToObject, pt)) :
				   Contains_ObjectSpace(pt);
	}

	// Check if the (transformed) primitive contains a point in world space (with tolerance)
	virtual bool Contains(const Vector3<T>& pt, T fTolerance) const {
		return m_transformed ?
				   Contains_ObjectSpace(TransformPoint_NonPerspective(m_worldToObject, pt), fTolerance) :
				   Contains_ObjectSpace(pt, fTolerance);
	}

	// Check if the primitive contains a point in object (primitive) space.
	virtual bool Contains_ObjectSpace(const Vector3<T>& pt) const = 0;

	// Check if the primitive contains a point in object (primitive) space (with tolerance)
	virtual bool Contains_ObjectSpace(const Vector3<T>& pt, T fTolerance) const = 0;

	// Visual mesh
	struct TriList {
		std::vector<Vector3<T>> positions;
		std::vector<Vector3<T>> normals;
	};

	// Generate a list of triangle that can be used for rendering the visual of the primitive
	virtual void GenerateVisualMesh(TriList& triangleList) const = 0;

	// Return the transformation matrix to be used in conjunction with the mesh returned from GenerateVisualMesh() for rendering
	virtual void GetVisualMeshMatrix(Matrix44<T>& matrix) const {
		matrix = m_objectToWorld; // By default visual matrix is the same as object-to-world transform
	}

protected:
	const Matrix44<T>& GetObjectToWorldMatrix() const { return m_objectToWorld; }
	const Matrix44<T>& GetWorldToObjectMatrix() const { return m_worldToObject; }

	void SetDegenerated(bool val) { m_degenerated = val; }
	bool IsDegenerated() const { return m_degenerated; }

private:
	bool m_degenerated;
	bool m_transformed;
	Matrix44<T> m_objectToWorld;
	Matrix44<T> m_worldToObject;
};

typedef Primitive<float> PrimitiveF;
typedef Primitive<double> PrimitiveD;

typedef std::shared_ptr<PrimitiveF> PrimitiveF_Ptr;
typedef std::shared_ptr<PrimitiveD> PrimitiveD_Ptr;

} // namespace KEP
