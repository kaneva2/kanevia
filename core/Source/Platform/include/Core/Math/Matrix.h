///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

//#include <type_traits> // Not yet necessary except for buggy VS2010. Try without after upgrade.
#include <initializer_list>
#include "KEPMathUtil.h"
#include "Vector.h"
#include "../Tools/GeometricTools/GteMatrix.h"

namespace KEP {

template <size_t M, size_t N, typename T>
class Matrix {
	// Helper objects to represent references to a subset of the matrix.
	template <typename MatrixType = Matrix<M, N, T>>
	struct MatrixRowColumnRef {
		MatrixRowColumnRef(MatrixType& mat, size_t iRowStart, size_t iColStart) :
				m_mat(mat), m_iRowStart(iRowStart), m_iColStart(iColStart) {}
		void operator=(const MatrixRowColumnRef&) = delete; // Silence warning about implicitly deleted assingment operator.
	protected:
		MatrixType& m_mat;
		size_t m_iRowStart;
		size_t m_iColStart;
	};

	template <size_t RowStride, size_t ColumnStride, typename MatrixType>
	struct SliceRefTemplate : private MatrixRowColumnRef<MatrixType> {
		SliceRefTemplate(MatrixType& mat, size_t iRowStart, size_t iColStart) :
				MatrixRowColumnRef(mat, iRowStart, iColStart) {}

		template <size_t L>
		operator Vector<L, T>() const {
			// We can only statically test for vectors being larger than number of rows or columns of the matrix.
			// Overflow due to using a non-zero matrix index must be done at run time.
			static_assert(L * RowStride <= M && L * ColumnStride <= N, "Vector too large");
			Vector<L, T> v;
			for (size_t i = 0; i < L; ++i)
				v[i] = m_mat(m_iRowStart + i * RowStride, m_iColStart + i * ColumnStride);
			return v;
		}

		template <size_t L>
		void operator=(const Vector<L, T>& v) {
			// We can only statically test for vectors being larger than number of rows or columns of the matrix.
			// Overflow due to using a non-zero matrix index must be done at run time.
			static_assert(L * RowStride <= M && L * ColumnStride <= N, "Vector too large");
			for (size_t i = 0; i < L; ++i)
				m_mat(m_iRowStart + i * RowStride, m_iColStart + i * ColumnStride) = v[i];
		}
	};
	template <size_t RowStride, size_t ColumnStride>
	using SliceConstRef = const SliceRefTemplate<RowStride, ColumnStride, const Matrix<M, N, T>>;
	template <size_t RowStride, size_t ColumnStride>
	using SliceRef = SliceRefTemplate<RowStride, ColumnStride, Matrix<M, N, T>>;

	template <typename MatrixType, size_t subM = M, size_t subN = N>
	struct SubMatrixRefTemplate : private MatrixRowColumnRef<MatrixType> {
		SubMatrixRefTemplate(MatrixType& mat, size_t iRowStart, size_t iColStart) :
				MatrixRowColumnRef(mat, iRowStart, iColStart) {}

		template <size_t K, size_t L>
		operator Matrix<K, L, T>() const {
			// We can only statically test for matrix being larger than number of rows or columns of original.
			// Overflow due to using a non-zero matrix index must be done at run time.
			static_assert(K <= subM && L <= subN, "Matrix overflow");
			Matrix<K, L, T> m;
			for (size_t i = 0; i < K; ++i) {
				for (size_t j = 0; j < L; ++j)
					m(i, j) = m_mat(m_iRowStart + i, m_iColStart + j);
			}

			return m;
		}

		template <size_t K, size_t L>
		SubMatrixRefTemplate& operator=(const Matrix<K, L, T>& m) {
			// We can only statically test for matrix being larger than number of rows or columns of original.
			// Overflow due to using a non-zero matrix index must be done at run time.
			static_assert(K <= subM && L <= subN, "Matrix overflow");
			for (size_t i = 0; i < K; ++i) {
				for (size_t j = 0; j < L; ++j)
					m_mat(m_iRowStart + i, m_iColStart + j) = m(i, j);
			}
		}

		template <typename OtherMatrixType, size_t K, size_t L>
		SubMatrixRefTemplate& operator=(const SubMatrixRefTemplate<OtherMatrixType, K, L>& subMatrixRef) {
			static_assert(K <= subM && L <= subN, "Matrix overflow");
			for (size_t i = 0; i < K; ++i) {
				for (size_t j = 0; j < L; ++j)
					m_mat(m_iRowStart + i, m_iColStart + j) = subMatrixRef.m_mat(i, j);
			}
			return *this;
		}

		SubMatrixRefTemplate& operator=(const SubMatrixRefTemplate& m) {
			return operator=<MatrixType, subM, subN>(m);
		}
	};
	template <size_t subM, size_t subN>
	using SubMatrixConstRef = const SubMatrixRefTemplate<const Matrix<M, N, T>, subM, subN>;
	template <size_t subM, size_t subN>
	using SubMatrixRef = SubMatrixRefTemplate<Matrix<M, N, T>, subM, subN>;

public:
	static const /*constexpr*/ size_t ROWS = M;
	static const /*constexpr*/ size_t COLUMNS = N;
	enum { NumRows = M,
		NumColumns = N };

	Matrix() = default;
	~Matrix() = default;

	// Allows one-liner construction. E.g. Matrix(Matrix::MakeIdentity)
	Matrix(void (Matrix<M, N, T>::*func)()) { (this->*func)(); }
	Matrix(std::initializer_list<T> initlist);

	template <typename U>
	Matrix(const Matrix<M, N, U>& other) { *this = other; }
	template <typename U>
	Matrix& operator=(const Matrix<M, N, U>& other) {
		std::copy(other.begin(), other.end(), begin());
		return *this;
	}

	// Row access
	template <size_t L = N>
	Vector<L, T>& GetRow(size_t iRow) { return m_Rows[iRow].SubVector<L>(); }
	template <size_t L = N>
	const Vector<L, T>& GetRow(size_t iRow) const { return m_Rows[iRow].SubVector<L>(); }
	template <size_t L>
	void SetRow(size_t iRow, const Vector<L, T>& v) { m_Rows[iRow].SubVector<L>() = v; }
	template <size_t L>
	void SetRow(size_t iRow, const Vector<L, T>& v, const T& fill);

	// RowX, RowY, RowZ, RowW methods for readability of existing common usages.
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 1)>>
	Vector<L, T>& GetRowX() { return GetRow<L>(0); }
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 1)>>
	const Vector<L, T>& GetRowX() const { return GetRow<L>(0); }
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 2)>>
	Vector<L, T>& GetRowY() { return GetRow<L>(1); }
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 2)>>
	const Vector<3, T>& GetRowY() const { return GetRow<L>(1); }
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 3)>>
	Vector<3, T>& GetRowZ() { return GetRow<L>(2); }
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 3)>>
	const Vector<3, T>& GetRowZ() const { return GetRow<L>(2); }
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 4)>>
	Vector<3, T>& GetRowW() { return GetRow<L>(3); }
	template <size_t L = 3, typename = std::enable_if<(L <= N && M >= 4)>>
	const Vector<3, T>& GetRowW() const { return GetRow<L>(3); }

	template <size_t L, typename = std::enable_if<(L <= N && M >= 1)>>
	void SetRowX(const Vector<L, T>& v) { GetRowRef(0) = v; }
	template <size_t L, typename = std::enable_if<(L <= N && M >= 2)>>
	void SetRowY(const Vector<L, T>& v) { GetRowRef(1) = v; }
	template <size_t L, typename = std::enable_if<(L <= N && M >= 3)>>
	void SetRowZ(const Vector<L, T>& v) { GetRowRef(2) = v; }
	template <size_t L, typename = std::enable_if<(L <= N && M >= 4)>>
	void SetRowW(const Vector<L, T>& v) { GetRowRef(3) = v; }

	// Get the translation part of this matrix, assuming NxN or Nx(N-1) homogeneous transformation.
	template <typename = std::enable_if_t<M == N || M + 1 == N>>
	Vector<M - 1, T>& GetTranslation();
	template <typename = std::enable_if_t<M == N || M + 1 == N>>
	const Vector<M - 1, T>& GetTranslation() const;

	// These are row-major matrices, so index operator returns the requested row as a vector.
	const Vector<N, T>& operator[](size_t iRow) const { return m_Rows[iRow]; }
	Vector<N, T>& operator[](size_t iRow) { return m_Rows[iRow]; }

	// Column access
	template <size_t L = M, typename = std::enable_if<(L <= M)>>
	Vector<L, T> GetColumn(size_t iCol, size_t iOffset = 0) const {
		Vector<L, T> ret;
		for (size_t i = 0; i < L; ++i) ret[i] = m_Rows[i + iOffset][iCol];
		return ret;
	}
	template <size_t L, typename = std::enable_if<(L <= M)>>
	void SetColumn(size_t iCol, const Vector<L, T>& v) {
		for (size_t i = 0; i < L; ++i) m_Rows[i][iCol] = v[i];
	}
	template <size_t L, typename = std::enable_if<(L <= M)>>
	void SetColumn(size_t iCol, const Vector<L, T>& v, const T& fill) {
		for (size_t i = 0; i < L; ++i) m_Rows[i][iCol] = v[i];
		for (size_t i = L; i < M; ++i) m_Rows[i][iCol] = fill;
	}

	// Row/column reference to be used for assignment
	SliceRef<0, 1> GetRowRefAt(size_t iRow, size_t iCol) { return SliceRef<0, 1>{ *this, iRow, iCol }; }
	SliceRef<1, 0> GetColumnRefAt(size_t iRow, size_t iCol) { return SliceRef<1, 0>{ *this, iRow, iCol }; }
	SliceRef<1, 1> GetDiagonalRefAt(size_t iRow = 0, size_t iCol = 0) { return SliceRef<1, 1>{ *this, iRow, iCol }; }
	SliceRef<0, 1> GetRowRef(size_t iRow) { return GetRowRefAt(iRow, 0); }
	SliceRef<1, 0> GetColumnRef(size_t iCol) { return GetColumnRefAt(0, iCol); }
	SliceRef<1, 1> GetDiagonalRef() { return GetDiagonalRefAt(0, 0); }
	template <size_t subM, size_t subN>
	SubMatrixRef<subM, subN> GetSubMatrixRefAt(size_t iRow, size_t iCol) { return SubMatrixRef<subM, subN>{ *this, iRow, iCol }; }
	SliceConstRef<0, 1> GetRowRefAt(size_t iRow, size_t iCol) const { return SliceConstRef<0, 1>{ *this, iRow, iCol }; }
	SliceConstRef<1, 0> GetColumnRefAt(size_t iRow, size_t iCol) const { return SliceConstRef<1, 0>{ *this, iRow, iCol }; }
	SliceConstRef<1, 1> GetDiagonalRefAt(size_t iRow = 0, size_t iCol = 0) const { return SliceConstRef<1, 1>{ *this, iRow, iCol }; }
	SliceConstRef<0, 1> GetRowRef(size_t iRow) const { return GetRowRefAt(iRow, 0); }
	SliceConstRef<1, 0> GetColumnRef(size_t iCol) const { return GetColumnRefAt(0, iCol); }
	SliceConstRef<1, 1> GetDiagonalRef() const { return GetDiagonalRefAt(0, 0); }
	template <size_t subM, size_t subN>
	SubMatrixConstRef<subM, subN> GetSubMatrixRefAt(size_t iRow, size_t iCol) const { return SubMatrixConstRef<subM, subN>{ *this, iRow, iCol }; }

	// ColumnX, ColumnY, ColumnZ, ColumnW methods for readability.
	template <typename = std::enable_if<M == 3 || M == 4>>
	Vector<3, T> GetColumnX() const { return GetColumnRef(0); }
	template <typename = std::enable_if<M == 3 || M == 4>>
	Vector<3, T> GetColumnY() const { return GetColumnRef(1); }
	template <typename = std::enable_if<M == 3 || M == 4>>
	Vector<3, T> GetColumnZ() const { return GetColumnRef(2); }
	template <typename = std::enable_if<M == 3 || M == 4>>
	Vector<3, T> GetColumnW() const { return GetColumnRef(3); }
	template <typename = std::enable_if<N == 3 || N == 4>>
	void SetColumnX(const Vector<3, T>& v) { GetColumnRef(0) = v; }
	template <typename = std::enable_if<N == 3 || N == 4>>
	void SetColumnY(const Vector<3, T>& v) { GetColumnRef(1) = v; }
	template <typename = std::enable_if<N == 3 || N == 4>>
	void SetColumnZ(const Vector<3, T>& v) { GetColumnRef(2) = v; }
	template <typename = std::enable_if<N == 3 || N == 4>>
	void SetColumnW(const Vector<3, T>& v) { GetColumnRef(3) = v; }

	// Construction functions
	// "Make" functions modify *this.
	void MakeZero();
	void MakeIdentity();
	template <typename = std::enable_if_t<M == N || M == N + 1>>
	void MakeTranslation(const Vector<M - 1, T>& v);
	template <typename = std::enable_if_t<M == N || M == N + 1>>
	void MakeScale(const Vector<M - 1, T>& v);
	template <typename = std::enable_if_t<M == N>>
	void MakeScale(const Vector<M, T>& v);
	void MakeScale(const T& uniform_scale); // Assumes homogeneous matrix - lower right element is set to 1.0
	void MakeScale_NonHomogeneous(const T& uniform_scale); // Assumes nonhomogeneous matrix - lower right element is set to the scale value.
	void MakeScale_Homogeneous(const T& uniform_scale); // Assumes homogeneous matrix - lower right element is set to 1.0

	template <typename = std::enable_if_t<M == N>>
	bool MakeInverseOf(const Matrix<M, N, T>& m, T* pDeterminant = nullptr);
	template <typename = std::enable_if_t<M == N>>
	void MakeTransposeOf(const Matrix<M, N, T>& m);

	// In-place modification
	template <typename = std::enable_if_t<M == N>>
	void Transpose();

	// Iterator access
	T* begin() { return RawData()[0]; }
	const T* begin() const { return cbegin(); }
	const T* cbegin() const { return RawData()[0]; }
	T* end() { return begin() + M * N; }
	const T* end() const { return cend(); }
	const T* cend() const { return begin() + M * N; }

	T (&RawData())
	[M][N] { return reinterpret_cast<T(&)[M][N]>(m_Rows[0][0]); }
	const T (&RawData() const)[M][N] { return reinterpret_cast<const T(&)[M][N]>(m_Rows[0][0]); }

	const T& operator()(size_t iRow, size_t iCol) const { return m_Rows[iRow][iCol]; }
	T& operator()(size_t iRow, size_t iCol) { return m_Rows[iRow][iCol]; }

	// Constant matrices.
	static const Matrix& GetIdentity() {
		static /*constexpr*/ const Matrix<M, N, T> s_Identity(&Matrix::MakeIdentity);
		return s_Identity;
	}
	static const Matrix& GetZero() {
		static /*constexpr*/ const Matrix<M, N, T> s_Identity(&Matrix::MakeZero);
		return s_Identity;
	}

private:
	Vector<N, T> m_Rows[M];
};

template <size_t M, size_t N, typename T>
inline Matrix<M, N, T>::Matrix(std::initializer_list<T> initlist) {
	size_t nValues = std::min<size_t>(M * N, initlist.size());
	std::copy(initlist.begin(), initlist.end(), begin());
	std::fill(begin() + nValues, end(), T());
}

template <size_t M, size_t N, typename T>
inline void Matrix<M, N, T>::MakeZero() {
	for (size_t iRow = 0; iRow < M; ++iRow)
		for (size_t iCol = 0; iCol < N; ++iCol)
			(*this)(iRow, iCol) = 0;
}

template <size_t M, size_t N, typename T>
inline void Matrix<M, N, T>::MakeIdentity() {
	for (size_t iRow = 0; iRow < M; ++iRow)
		for (size_t iCol = 0; iCol < N; ++iCol)
			(*this)(iRow, iCol) = static_cast<T>(iRow == iCol);
}

template <size_t M, size_t N, typename T>
template <typename>
inline void Matrix<M, N, T>::MakeTranslation(const Vector<M - 1, T>& v) {
	for (size_t i = 0; i < M - 1; ++i)
		for (size_t j = 0; j < N; ++j)
			(*this)(i, j) = (i == j);
	this->SetRow(M - 1, v, 1);
}

template <size_t M, size_t N, typename T>
template <typename /*=std::enable_if_t<M==N||M==N+1>*/>
inline void Matrix<M, N, T>::MakeScale(const Vector<M - 1, T>& v) {
	for (size_t i = 0; i < M; ++i) {
		for (size_t j = 0; j < M; ++j)
			(*this)(i, j) = j == i ? v[i] : T(0);
		for (size_t j = M; j < N; ++j)
			(*this)(i, j) = 0;
	}

	if (M == N)
		(*this)(M - 1, M - 1) = 1;
}

template <size_t M, size_t N, typename T>
template <typename /*=std::enable_if_t<M==N>*/>
inline void Matrix<M, N, T>::MakeScale(const Vector<M, T>& v) {
	for (size_t i = 0; i < M; ++i)
		for (size_t j = 0; j < N; ++j)
			(*this)(i, j) = j == i ? v[i] : T(0);
}

template <size_t M, size_t N, typename T>
inline void Matrix<M, N, T>::MakeScale(const T& uniform_scale) {
	MakeScale_Homogeneous(uniform_scale);
}

template <size_t M, size_t N, typename T>
inline void Matrix<M, N, T>::MakeScale_NonHomogeneous(const T& uniform_scale) {
	for (size_t i = 0; i < M; ++i)
		for (size_t j = 0; j < N; ++j)
			(*this)(i, j) = j == i ? uniform_scale : T(0);
}

template <size_t M, size_t N, typename T>
inline void Matrix<M, N, T>::MakeScale_Homogeneous(const T& uniform_scale) {
	for (size_t i = 0; i < M; ++i)
		for (size_t j = 0; j < N; ++j)
			(*this)(i, j) = j == i ? uniform_scale : T(0);
	if (M == N)
		(*this)(M - 1, N - 1) = T(1);
}

template <size_t M, size_t N, typename T>
template <typename>
inline bool Matrix<M, N, T>::MakeInverseOf(const Matrix<M, N, T>& m, T* pDeterminant) {
	T determinant = T(0);
	const T* ptm = &m(0, 0);
	T* ptm_inv = &(*this)(0, 0);
	bool bSucceeded = gte::GaussianElimination<T>()(N, ptm, ptm_inv, determinant,
		nullptr, nullptr, nullptr, 0, nullptr);
	if (pDeterminant)
		*pDeterminant = determinant;

	return bSucceeded;
}

template <size_t M, size_t N, typename T>
template <typename>
inline void Matrix<M, N, T>::MakeTransposeOf(const Matrix<M, N, T>& m) {
	if (&m != this) {
		for (size_t i = 0; i < M; ++i) {
			for (size_t j = 0; j < N; ++j)
				(*this)(i, j) = m(j, i);
		}
	} else {
		this->Transpose();
	}
}

template <size_t M, size_t N, typename T>
template <typename>
inline Vector<M - 1, T>& Matrix<M, N, T>::GetTranslation() {
	return m_Rows[M - 1].SubVector<N - 1>(0);
}

template <size_t M, size_t N, typename T>
template <typename>
inline const Vector<M - 1, T>& Matrix<M, N, T>::GetTranslation() const {
	return m_Rows[M - 1].SubVector<N - 1>(0);
}

template <size_t M, size_t N, typename T>
template <size_t L>
inline void Matrix<M, N, T>::SetRow(size_t iRow, const Vector<L, T>& v, const T& fill) {
	for (size_t i = 0; i < (L < N ? L : N); ++i)
		operator()(iRow, i) = v[i];
	for (size_t i = L; i < N; ++i)
		operator()(iRow, i) = fill;
}

template <size_t M, size_t N, typename T>
template <typename>
inline void Matrix<M, N, T>::Transpose() {
	for (size_t i = 1; i < M; ++i) {
		for (size_t j = 0; j < i; ++j)
			std::swap((*this)(i, j), (*this)(j, i));
	}
}

template <size_t M, size_t N, size_t L, typename T>
inline Matrix<M, L, T> operator*(const Matrix<M, N, T>& left, const Matrix<N, L, T>& right) {
	Matrix<M, L, T> result;
	for (size_t i = 0; i < M; ++i) {
		for (size_t j = 0; j < L; ++j) {
			T t = left(i, 0) * right(0, j);
			for (size_t k = 1; k < N; ++k) {
				t += left(i, k) * right(k, j);
			}
			result(i, j) = t;
		}
	}
	return result;
}

template <size_t M, size_t N, typename T, typename U, typename = std::enable_if_t<std::is_arithmetic<U>::value>>
inline Matrix<M, N, T> operator*(const U& left, const Matrix<M, N, T>& right) {
	Matrix<M, N, T> result;
	for (size_t i = 0; i < M; ++i) {
		for (size_t j = 0; j < N; ++j) {
			result(i, j) = left * right(i, j);
		}
	}
	return result;
}

template <size_t M, size_t N, typename T, typename U, typename = std::enable_if_t<std::is_arithmetic<U>::value>>
inline Matrix<M, N, T> operator*(const Matrix<M, N, T>& left, const U& right) {
	return right * left;
}

template <size_t M, size_t N, typename T>
inline Matrix<M, N, T> operator+(const Matrix<M, N, T>& left, const Matrix<M, N, T>& right) {
	Matrix<M, N, T> result;
	for (size_t i = 0; i < M; ++i) {
		for (size_t j = 0; j < N; ++j) {
			result(i, j) = left(i, j) + right(i, j);
		}
	}
	return result;
}

template <size_t M, size_t N, typename T>
inline Matrix<M, N, T> operator-(const Matrix<M, N, T>& left, const Matrix<M, N, T>& right) {
	Matrix<M, N, T> result;
	for (size_t i = 0; i < M; ++i) {
		for (size_t j = 0; j < N; ++j) {
			result(i, j) = left(i, j) - right(i, j);
		}
	}
	return result;
}

// Transform homogeneous vector by matrix
template <size_t M, size_t N, typename T>
inline Vector<N, T> TransformVector(const Matrix<M, N, T>& m, const Vector<N, T>& v) {
	Vector<N, T> result = v[0] * m[0];
	for (size_t i = 1; i < M; ++i)
		result += v[i] * m[i];
	return result;
}

// Transform (N-1) vector by matrix (ignoring translation and perspective part).
template <size_t N, typename T>
inline Vector<N - 1, T> TransformVector(const Matrix<N, N, T>& m, const Vector<N - 1, T>& v) {
	Vector<N - 1, T> result = v[0] * m[0].SubVector<N - 1>();
	for (size_t i = 1; i < N - 1; ++i)
		result += v[i] * m[i].SubVector<N - 1>();
	return result;
}

inline Vector<3, float> TransformVector(const Matrix<4, 4, float>& m, const Vector<3, float>& v) {
	__m128 row0 = _mm_loadu_ps(&m[0][0]);
	__m128 row1 = _mm_loadu_ps(&m[1][0]);
	__m128 row2 = _mm_loadu_ps(&m[2][0]);
	__m128 result;
	__m128 v2 = _mm_load1_ps(&v[2]);
	__m128 scaled_row2 = _mm_mul_ps(row2, v2);
	result = scaled_row2;
	__m128 v1 = _mm_load1_ps(&v[1]);
	__m128 scaled_row1 = _mm_mul_ps(row1, v1);
	result = _mm_add_ps(result, scaled_row1);
	__m128 v0 = _mm_load1_ps(&v[0]);
	__m128 scaled_row0 = _mm_mul_ps(row0, v0);
	result = _mm_add_ps(result, scaled_row0);
	return reinterpret_cast<const Vector3f&>(result);
}
// Transform an (N-1) dimensional point with implied homogeneous (w) coordinate of 1
template <size_t N, typename T>
inline Vector<N, T> TransformPointToHomogeneousVector(const Matrix<N, N, T>& m, const Vector<N - 1, T>& v) {
	Vector<N, T> homogeneous_result = m[N - 1];
	for (size_t i = 0; i < N - 1; ++i)
		homogeneous_result += v[i] * m[i];
	return homogeneous_result;
}

// Optimized SSE version for 4x4 matrix.
namespace detail {
inline __m128 TransformPointToHomogeneousVectorSSE(const Matrix<4, 4, float>& m, const Vector<3, float>& v) {
	__m128 row0 = _mm_loadu_ps(&m[0][0]);
	__m128 row1 = _mm_loadu_ps(&m[1][0]);
	__m128 row2 = _mm_loadu_ps(&m[2][0]);
	__m128 row3 = _mm_loadu_ps(&m[3][0]);
	__m128 result = row3;
	__m128 v2 = _mm_load1_ps(&v[2]);
	__m128 scaled_row2 = _mm_mul_ps(row2, v2);
	result = _mm_add_ps(result, scaled_row2);
	__m128 v1 = _mm_load1_ps(&v[1]);
	__m128 scaled_row1 = _mm_mul_ps(row1, v1);
	result = _mm_add_ps(result, scaled_row1);
	__m128 v0 = _mm_load1_ps(&v[0]);
	__m128 scaled_row0 = _mm_mul_ps(row0, v0);
	result = _mm_add_ps(result, scaled_row0);
	return result;
}
} // namespace detail

inline Vector<4, float> TransformPointToHomogeneousVector(const Matrix<4, 4, float>& m, const Vector<3, float>& v) {
	__m128 result = detail::TransformPointToHomogeneousVectorSSE(m, v);
	Vector4f vResult;
	_mm_storeu_ps(&vResult[0], result);
	return vResult;
}

// Transform an (N-1) dimensional point with implied homogeneous (w) coordinate of 1
template <size_t N, typename T>
inline Vector<N - 1, T> TransformPoint(const Matrix<N, N, T>& m, const Vector<N - 1, T>& v) {
	Vector<N, T> homogeneous_result = TransformPointToHomogeneousVector(m, v);
	if (homogeneous_result[N - 1] != 0) {
		return homogeneous_result.SubVector<N - 1>() / homogeneous_result[N - 1];
	} else {
		// Error: Zero homogeneous component. Point can't be correctly transformed.
		return homogeneous_result.SubVector<N - 1>();
	}
}

inline Vector<3, float> TransformPoint(const Matrix<4, 4, float>& m, const Vector<3, float>& v) {
	__m128 resultH = detail::TransformPointToHomogeneousVectorSSE(m, v);
	__m128 resultW = _mm_shuffle_ps(resultH, resultH, 0xff);
	__m128 zero = _mm_setzero_ps();
	__m128 mask = _mm_cmpneq_ps(resultW, zero);
	__m128 one = _mm_set1_ps(1.0f);
	__m128 resultW_or_1 = _mm_or_ps(_mm_and_ps(mask, resultW), _mm_andnot_ps(mask, one));
	__m128 result_divided = _mm_div_ps(resultH, resultW_or_1);
	Vector4f vResult4;
	_mm_storeu_ps(&vResult4[0], result_divided);
	return vResult4.SubVector<3>();
}

// Assumes a non-perspective matrix. I.e. the final column of the matrix is [0, .. 0, 1]
template <size_t N, typename T>
inline Vector<N - 1, T> TransformPoint_NonPerspective(const Matrix<N, N, T>& m, const Vector<N - 1, T>& v) {
	Vector<N - 1, T> result = m[N - 1].SubVector<N - 1>();
	for (size_t i = 0; i < N - 1; ++i)
		result += v[i] * m[i].SubVector<N - 1>();
	return result;
}

inline Vector<3, float> TransformPoint_NonPerspective(const Matrix<4, 4, float>& m, const Vector<3, float>& v) {
	__m128 resultH = detail::TransformPointToHomogeneousVectorSSE(m, v);
	Vector4f vResult4;
	_mm_storeu_ps(&vResult4[0], resultH);
	return vResult4.SubVector<3>();
}

template <size_t N, typename T>
inline void TransformPoints(const Matrix<N, N, T>& m,
	const Vector<N - 1, T>* pIn, size_t strideIn,
	Vector<N - 1, T>* pOut, size_t strideOut,
	size_t nPoints) {
	if (m(0, 3) == 0 && m(1, 3) == 0 && m(2, 3) == 0 && m(3, 3) == 1) {
		for (size_t i = 0; i < nPoints; ++i) {
			*pOut = TransformPoint_NonPerspective(m, *pIn);
			pIn = reinterpret_cast<decltype(pIn)>(reinterpret_cast<const char*>(pIn) + strideIn);
			pOut = reinterpret_cast<decltype(pOut)>(reinterpret_cast<char*>(pOut) + strideOut);
		}
	} else {
		for (size_t i = 0; i < nPoints; ++i) {
			*pOut = TransformPoint(m, *pIn);
			pIn = reinterpret_cast<decltype(pIn)>(reinterpret_cast<const char*>(pIn) + strideIn);
			pOut = reinterpret_cast<decltype(pOut)>(reinterpret_cast<char*>(pOut) + strideOut);
		}
	}
}

template <size_t N, typename T>
inline void TransformVectors(const Matrix<N, N, T>& m,
	const Vector<N, T>* pIn, size_t strideIn,
	Vector<N, T>* pOut, size_t strideOut,
	size_t nVectors) {
	for (size_t i = 0; i < nVectors; ++i) {
		*pOut = TransformVector(m, *pIn);
		pIn = reinterpret_cast<decltype(pIn)>(reinterpret_cast<const char*>(pIn) + strideIn);
		pOut = reinterpret_cast<decltype(pOut)>(reinterpret_cast<char*>(pOut) + strideOut);
	}
}

template <size_t N, typename T>
inline void TransformVectors(const Matrix<N, N, T>& m,
	const Vector<N - 1, T>* pIn, size_t strideIn,
	Vector<N - 1, T>* pOut, size_t strideOut,
	size_t nVectors) {
	for (size_t i = 0; i < nVectors; ++i) {
		*pOut = TransformVector(m, *pIn);
		pIn = reinterpret_cast<decltype(pIn)>(reinterpret_cast<const char*>(pIn) + strideIn);
		pOut = reinterpret_cast<decltype(pOut)>(reinterpret_cast<char*>(pOut) + strideOut);
	}
}

template <size_t M, size_t N, typename T>
inline Matrix<M, N, T> InverseOf(const Matrix<M, N, T>& mtx) {
	Matrix<M, N, T> mtxInverse;
	mtxInverse.MakeInverseOf(mtx);
	return mtxInverse;
}

template <typename T>
using Matrix22 = Matrix<2, 2, T>;
template <typename T>
using Matrix32 = Matrix<3, 2, T>;
template <typename T>
using Matrix33 = Matrix<3, 3, T>;
template <typename T>
using Matrix43 = Matrix<4, 3, T>;
template <typename T>
using Matrix44 = Matrix<4, 4, T>;
using Matrix22f = Matrix22<float>;
using Matrix22d = Matrix22<double>;
using Matrix32f = Matrix32<float>;
using Matrix32d = Matrix32<double>;
using Matrix33f = Matrix33<float>;
using Matrix33d = Matrix33<double>;
using Matrix43f = Matrix43<float>;
using Matrix43d = Matrix43<double>;
using Matrix44f = Matrix44<float>;
using Matrix44d = Matrix44<double>;

typedef __declspec(align(16)) Matrix44f Matrix44fA16; // Matrix 4x4 of float aligned to 16 bytes

} // namespace KEP