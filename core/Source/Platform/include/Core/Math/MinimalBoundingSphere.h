///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <list>
#include "Sphere.h"
#include "ExtremePoints.h"

namespace KEP {

// Future optimizations:
// 1. Find some extreme points (using dot product with collection of vectors such as (1,0,0), (0,1,0), (0,0,1), (1,1,1), (1,-1,1), (1,1,0), (0,1,1), etc.).
//    Then move those extreme points to the front of the list.
//#define ENABLE_MINSPHERE_LOG // Testing aid
template <typename T>
class MinimalBoundingSphereCalculator {
	// Custom list doubles performance for low number of points.
	// Makes nonrecursive method faster than recursive method for low number of points.
	class SimplePointList {
		struct Node {
			Node* m_pNext;
			Node* m_pPrev;
			Vector3<T> m_pt;
		};

	public:
		template <typename Iterator>
		SimplePointList(Iterator b, Iterator e) {
			size_t nItems = e - b;
			m_aNodes.resize(nItems + 1);
			Node* pCurrent = m_aNodes.data();
			size_t i = 0;
			for (Iterator itr = b; itr < e; ++itr) {
				pCurrent->m_pt = *itr;
				pCurrent->m_pNext = pCurrent + 1;
				pCurrent->m_pPrev = pCurrent - 1;
				++pCurrent;
			}
			pCurrent->m_pNext = pCurrent + 1;
			pCurrent->m_pPrev = pCurrent - 1;
			m_pBegin = m_aNodes.data();
			m_pEnd = pCurrent;
		}

		class iterator : public std::iterator<std::bidirectional_iterator_tag, Vector3<T>> {
		public:
			iterator() {}
			iterator(Node* pNode) { m_pNode = pNode; }

			iterator& operator++() {
				m_pNode = m_pNode->m_pNext;
				return *this;
			}
			iterator& operator--() {
				m_pNode = m_pNode->m_pPrev;
				return *this;
			}
			bool operator==(const iterator& other) const { return m_pNode == other.m_pNode; }
			bool operator!=(const iterator& other) const { return m_pNode != other.m_pNode; }
			Vector3<T>& operator*() { return m_pNode->m_pt; }
			Vector3<T>* operator->() { return &m_pNode->m_pt; }

		private:
			friend class SimplePointList;
			Node* m_pNode;
		};

		iterator begin() { return iterator(m_pBegin); }
		iterator end() { return iterator(m_pEnd); }
		size_t size() { return m_aNodes.size() - 1; }
		void splice(iterator pos, SimplePointList& other_container, iterator it) {
			Node* pPosition = pos.m_pNode;
			Node* pNode = it.m_pNode;
			if (pNode == pPosition)
				return;
			if (pNode != m_pBegin)
				pNode->m_pPrev->m_pNext = pNode->m_pNext;
			else
				m_pBegin = pNode->m_pNext;
			pNode->m_pNext->m_pPrev = pNode->m_pPrev;
			pNode->m_pPrev = pPosition->m_pPrev;
			pNode->m_pNext = pPosition;
			if (pPosition != m_pBegin)
				pPosition->m_pPrev->m_pNext = pNode;
			else
				m_pBegin = pNode;
			pPosition->m_pPrev = pNode;
		}

	private:
		std::vector<Node> m_aNodes;
		Node* m_pBegin;
		Node* m_pEnd;
	};

public:
	MinimalBoundingSphereCalculator() {}

	// Non-recursive version of Welzl's algorithm.
	template <typename Iterator>
	bool CalculateNonRecursive(Iterator begin, Iterator end) {
		// As expected, vectors are faster for a small number of points but lists are faster for many points.
		//return Calculate(std::list<Vector3<T>>(begin, end));
		//return Calculate(std::vector<Vector3<T>>(begin, end));
		return Calculate(SimplePointList(begin, end));
	}

	// Recursive version is a straightforward implementation of Welzl's algorithm.
	// This is slower than the non-recursive version, unless there are very few points.
	// There is also the risk of stack overflow with a large number of points.
	template <typename Iterator>
	bool CalculateRecursive(Iterator begin, Iterator end) {
		std::vector<Vector3<T>> apts(begin, end);
		m_nTotalPoints = apts.size();
		return CalculateRecursive(apts.data(), apts.size(), apts.data(), 0);
	}

	template <typename Iterator>
	bool Calculate(Iterator begin, Iterator end) {
		return CalculateNonRecursive(begin, end);
		//return CalculateRecursive(begin, end);
	}

	const SphereSquared<T>& GetSphereSquared() const { return m_Sphere; }
	Sphere<T> GetSphere() const { return Sphere<T>(m_Sphere.GetCenter(), m_Sphere.GetRadiusSquared() < 0 ? m_Sphere.GetRadiusSquared() : Sqrt(m_Sphere.GetRadiusSquared())); }

private:
	// Pick some points on the convex hull and move them to the front of the list because they're more likely to be on the sphere boundary.
	template <typename Container>
	void OptimizeOrder(Container& container) {
		typedef typename Container::iterator Iterator;
		if (!std::is_same<SimplePointList, Container>::value && !std::is_same<std::list<Vector3<T>>, Container>::value)
			return; // Only support lists. Vectors shouldn't be to difficult to support but they don't seem worth keeping so I'm saving the effort.

		size_t nPoints = container.size();

		ExtremePair<Iterator, T> aExtremePairs[49];
		//size_t nDirections = 3; // Only 10% increase in execution time for perfectly ordered points. For 1 million random points, 7 directions was found faster by about 6x.
		//size_t nDirections = 7; // If points are already optimally ordered, can double execution time. For poorly ordered points, can be orders of magnitude faster.
		//size_t nDirections = 13; // Slower than 7 for 1 million random points.
		size_t nDirections = 0;
		if (nPoints < 10)
			nDirections = 0;
		else if (nPoints <= 1000)
			nDirections = 3;
		else //if( nPoints < 1000000 )
			nDirections = 7;

		if (!CalculateExtremePoints(aExtremePairs, nDirections, container.begin(), container.end()))
			return; // No points or unsupported number of directions.

		// Sort pairs by distance between min and max.
		std::sort(aExtremePairs, aExtremePairs + nDirections, [](const ExtremePair<Iterator, T>& left, const ExtremePair<Iterator, T>& right) {
			return left.m_fMaxValue - left.m_fMinValue < right.m_fMaxValue - right.m_fMinValue;
		});

		// Put extreme points first in list.
		// Points on longer axes are moved to the front last, so they will end up being prioritized over points along shorter axes.
		for (size_t i = 0; i < nDirections; ++i) {
			RotateRight1(container, container.begin(), aExtremePairs[i].m_MinIterator);
			RotateRight1(container, container.begin(), aExtremePairs[i].m_MaxIterator);
		}
	}

	// Allow about 6 bits of error for float, 13 for double
	static const T GetInSphereTolerance() { return std::numeric_limits<T>::epsilon() * ldexp(T(1), std::numeric_limits<T>::digits / 4); }
	static const T GetRadiusScaleFactor() { return 1 + GetInSphereTolerance(); }

	template <typename Iterator>
	static bool UpdateSphere(Iterator (&aBoundaryIter)[4], size_t nBoundaryPoints, Out<SphereSquared<T>> sphere) {
		//++m_nUpdateSphereCalls; // Performance measurement
		switch (nBoundaryPoints) {
			case 0:
				sphere.get().Set(Vector3<T>(0, 0, 0), -1);
				return true;
			case 1:
				sphere.get().Set(*aBoundaryIter[0], 0);
				return true;
			case 2: {
				const Vector3<T>& pt0 = *aBoundaryIter[0];
				const Vector3<T>& pt1 = *aBoundaryIter[1];
				Vector3<T> ptSphereCenter = 0.5f * (pt0 + pt1);
				sphere.get().Set(ptSphereCenter, DistanceSquared(pt0, ptSphereCenter));
				return true;
			}
			case 3: {
				const Vector3<T>& pt0 = *aBoundaryIter[0];
				const Vector3<T>& pt1 = *aBoundaryIter[1];
				const Vector3<T>& pt2 = *aBoundaryIter[2];
				if (!CreateSphereSquaredThroughPoints(pt0, pt1, pt2, out(sphere)))
					return false;
				return true;
			}
			case 4: {
				const Vector3<T>& pt0 = *aBoundaryIter[0];
				const Vector3<T>& pt1 = *aBoundaryIter[1];
				const Vector3<T>& pt2 = *aBoundaryIter[2];
				const Vector3<T>& pt3 = *aBoundaryIter[3];
				if (!CreateSphereSquaredThroughPoints(pt0, pt1, pt2, pt3, out(sphere)))
					return false;
				return true;
			}
		}
		return false;
	}

	bool CalculateRecursive(Vector3<T>* papts, size_t nPoints, Vector3<T>* paptsBoundary, size_t nBoundary) {
#if defined(ENABLE_MINSPHERE_LOG)
		std::stringstream strm;
		strm << "[B] ";
		strm.precision(2);
		strm.setf(std::ios::fixed, std::ios::floatfield);
		for (size_t i = 0; i < nBoundary; ++i) {
			const Vector3<T>& pt = paptsBoundary[i];
			strm << "(" << pt.X() << "," << pt.Y() << "," << pt.Z() << ")";
		}
		strm << "[P] ";
		for (size_t i = 0; i < nPoints; ++i) {
			const Vector3<T>& pt = papts[i];
			strm << "(" << pt.X() << "," << pt.Y() << "," << pt.Z() << ")";
		}
		strm << "[E] ";
		for (size_t i = nPoints; i < m_nTotalPoints - nBoundary; ++i) {
			const Vector3<T>& pt = papts[i];
			strm << "(" << pt.X() << "," << pt.Y() << "," << pt.Z() << ")";
		}
		strm << "\n";
		printf("%s", strm.str().c_str());
#endif

		if (nPoints == 0 || nBoundary == 4) {
			typedef Vector3<T>*(&ArrayRef)[4];
			Vector3<T>* apptBoundary[4];
			for (size_t i = 0; i < nBoundary; ++i)
				apptBoundary[i] = &paptsBoundary[i];
			bool bCreatedSphere = UpdateSphere<Vector3<T>*>(apptBoundary, nBoundary, out(m_Sphere));
#if defined(ENABLE_MINSPHERE_LOG)
			printf("New sphere (%u): (%.2f,%.2f,%.2f),%.2f\n", (unsigned int)nBoundary, m_Sphere.GetCenter().X(), m_Sphere.GetCenter().Y(), m_Sphere.GetCenter().Z(), sqrt(m_Sphere.GetRadiusSquared()));
#endif
			return bCreatedSphere;
		}

		CalculateRecursive(papts, nPoints - 1, paptsBoundary, nBoundary);

		T fPointToSphereCenterDistanceSquared = DistanceSquared(papts[nPoints - 1], m_Sphere.GetCenter());
		bool bPointInSphere = fPointToSphereCenterDistanceSquared <= m_Sphere.GetRadiusSquared() * GetRadiusScaleFactor();
#if defined(ENABLE_MINSPHERE_LOG)
		Vector3<T> ptCurrent = papts[nPoints - 1];
		printf("(%.2f,%.2f,%.2f) %s in sphere\n", papts[nPoints - 1].X(), papts[nPoints - 1].Y(), papts[nPoints - 1].Z(), bPointInSphere ? "is" : "is not");
#endif
		if (!bPointInSphere) {
			std::rotate(papts, papts + (nPoints - 1), papts + nPoints);
			if (!CalculateRecursive(papts + 1, nPoints - 1, paptsBoundary, nBoundary + 1))
				return false;
		}

		return true;
	}

	// Moves itrLast (which must be) to be before itrFirst, keeping the elements in between in the same relative order.
	// Updates itrFirst and itrLast to point to the new locations of the moved objects.
	// I.e. they will be pointing to second and first positions respectively of the range
	template <typename T>
	void RotateRight1(std::list<T>& list, InOut<typename std::list<T>::iterator> itrFirst, InOut<typename std::list<T>::iterator> itrLast) {
		list.splice(itrFirst.get(), list, itrLast.get());
	}

	template <typename Container>
	void RotateRight1(Container& container, InOut<typename Container::iterator> itrFirst, InOut<typename Container::iterator> itrLast) {
		std::rotate(itrFirst.get(), itrLast.get(), std::next(itrLast.get()));
		itrLast.get() = itrFirst.get();
		itrFirst.get() = std::next(itrFirst.get());
	}
	void RotateRight1(SimplePointList& list, InOut<typename SimplePointList::iterator> itrFirst, InOut<typename SimplePointList::iterator> itrLast) {
		list.splice(itrFirst.get(), list, itrLast.get());
	}
	// RotateRight1 without updating input iterators.
	template <typename Container>
	void RotateRight1(Container& container, typename Container::iterator itrFirst, typename Container::iterator itrLast) {
		return RotateRight1(container, inout(itrFirst), inout(itrLast));
	}

	template <typename PointContainer>
	bool Calculate(PointContainer pointsInSphere) {
		OptimizeOrder(pointsInSphere);

		SphereSquared<T> sphere(Vector3<T>(0, 0, 0), -1);
		T fRadiusScaleFactor = GetRadiusScaleFactor();

		PointContainer::iterator aBoundaryIter[4]; // First 'nBoundaryPoints' items in list. For easier access when calculating spheres based on boundary points.
		PointContainer::iterator itrFirstNonBoundary = pointsInSphere.begin();
		PointContainer::iterator itrSubSphereEnd = pointsInSphere.begin();
		PointContainer::iterator aItrSubSphereEnds[4];
		size_t nBoundaryPoints = 0;
		PointContainer::iterator itrNext = itrFirstNonBoundary;
		while (true) {
#if defined(ENABLE_MINSPHERE_LOG)
			std::stringstream strm;
			strm << "(" << nBoundaryPoints << ") ";
			strm << "[B] ";
			auto itr = pointsInSphere.begin();
			while (true) {
				if (itr == itrFirstNonBoundary)
					strm << "[P] ";
				if (itr == itrNext)
					strm << "[+] ";
				if (itr == itrSubSphereEnd)
					strm << "[E] ";
				if (itr == pointsInSphere.end())
					break;
				strm.precision(2);
				strm.setf(std::ios::fixed, std::ios::floatfield);
				strm << "(" << itr->X() << "," << itr->Y() << "," << itr->Z() << ")";
				++itr;
			}
			strm << "\n";
			printf("%s", strm.str().c_str());
#endif

			if (itrNext == itrSubSphereEnd) {
				// All points are in current subsphere.

				if (nBoundaryPoints == 0) {
					if (itrSubSphereEnd == pointsInSphere.end())
						break; // No more points.
					++itrSubSphereEnd; // Add another point.
				} else {
					--itrFirstNonBoundary;
					--nBoundaryPoints;
					itrSubSphereEnd = aItrSubSphereEnds[nBoundaryPoints];
				}
				continue;
			}

			T fPointToSphereCenterDistanceSquared = DistanceSquared(*itrNext, sphere.GetCenter());
			bool bPointInSphere = fPointToSphereCenterDistanceSquared <= sphere.GetRadiusSquared() * fRadiusScaleFactor;
#if defined(ENABLE_MINSPHERE_LOG)
			printf("Point (%.2f,%.2f,%.2f) %s in sphere (%.2f,%.2f,%.2f),%.2f\n", itrNext->X(), itrNext->Y(), itrNext->Z(), bPointInSphere ? "is" : "is not", sphere.GetCenter().X(), sphere.GetCenter().Y(), sphere.GetCenter().Z(), sqrt(sphere.GetRadiusSquared()));
#endif
			if (bPointInSphere) {
				// Test next point in subsphere.
				++itrNext;
				continue;
			} else {
				// This point is not in the current subsphere. It must be on the boundary of the sphere containing the points up to itrNext.
				PointContainer::iterator itrNewSubSphereEnd = std::next(itrNext); // Look for subsphere containing all points up to and include itrNext

				// Add itrNext to boundary.
				if (itrNext == itrFirstNonBoundary)
					++itrFirstNonBoundary; // itrNext is already in the correct position.
				else
					RotateRight1(pointsInSphere, inout(itrFirstNonBoundary), inout(itrNext)); // Move itrNext to be first non boundary point

				// Put point in the boundary list.
				aBoundaryIter[nBoundaryPoints] = itrNext;

				// Calculate sphere
				if (!UpdateSphere(aBoundaryIter, nBoundaryPoints + 1, out(sphere))) {
#if defined(ENABLE_MINSPHERE_LOG)
					printf("Sphere error!\n");
#endif
					return false;
				}
#if defined(ENABLE_MINSPHERE_LOG)
				printf("New sphere (%u): (%.2f,%.2f,%.2f),%.2f\n", (unsigned int)nBoundaryPoints, sphere.GetCenter().X(), sphere.GetCenter().Y(), sphere.GetCenter().Z(), sqrt(sphere.GetRadiusSquared()));
#endif

				itrNext = itrFirstNonBoundary;
				if (nBoundaryPoints != 4) {
					++nBoundaryPoints;
					aItrSubSphereEnds[nBoundaryPoints - 1] = itrSubSphereEnd;
					itrSubSphereEnd = itrNewSubSphereEnd;
				} else {
					--itrFirstNonBoundary;
				}
			}
		}

		m_Sphere = sphere;
		return true;
	}

private:
	//static
	//size_t m_nUpdateSphereCalls = 0;
	size_t m_nTotalPoints; // For debugging recursive calculator.
	SphereSquared<T> m_Sphere;
};

template <typename T>
inline bool CreateMinimalBoundingSphere(const Vector3<T>* pPoints, size_t nPoints, Out<SphereSquared<T>> sphere) {
	MinimalBoundingSphereCalculator<T> calculator;
	if (!calculator.Calculate(pPoints, pPoints + nPoints))
		return false;
	sphere.get() = calculator.GetSphereSquared();
	return true;
}

template <typename T>
inline bool CreateMinimalBoundingSphere(const Vector3<T>* pPoints, size_t nPoints, Out<Sphere<T>> sphere) {
	MinimalBoundingSphereCalculator<T> calculator;
	if (!calculator.Calculate(pPoints, pPoints + nPoints))
		return false;
	sphere.get() = calculator.GetSphere();
	return true;
}

template <typename T>
inline bool CreateMinimalBoundingSphere(const std::vector<Vector3<T>>& aPoints, Out<SphereSquared<T>> sphere) {
	return CreateMinimalBoundingSphere(aPoints.data(), aPoints.size(), sphere);
}

template <typename T>
inline bool CreateMinimalBoundingSphere(const std::vector<Vector3<T>>& aPoints, Out<Sphere<T>> sphere) {
	return CreateMinimalBoundingSphere(aPoints.data(), aPoints.size(), sphere);
}

} // namespace KEP