///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Primitive.h"
#include "PrimitiveType.h"
#include "PrimitiveMesh.h"

namespace KEP {

template <typename T, typename UP>
class Cylinder : public Primitive<T> {
	using FWD = typename UP::NextAxis;
	using RIGHT = typename UP::PrevAxis;

public:
	Cylinder(T radius, T height, T base) :
			m_radius(radius),
			m_height(height),
			m_base(base),
			m_up(UP::Basis()),
			m_centerOfBase(m_up * m_base) {
		SetDegenerated(m_radius == 0 || m_height == 0);
	}

	virtual int GetType() const override { return PrimitiveType::Cylinder; }

	virtual void SetTransform(const Vector3<T>& pt, const Vector3<T>& dir, const Vector3<T>& up) override {
		Primitive<T>::SetTransform(pt, dir, up);
		m_up = GetObjectToWorldMatrix().GetRow<3>(UP::value);
		m_centerOfBase = pt + m_up * m_base;
	}

	virtual bool Contains(const Vector3<T>& pt) const override { return Contains(pt, static_cast<T>(0)); }

	virtual bool Contains(const Vector3<T>& pt, T fTolerance) const override {
		if (IsDegenerated()) {
			return false;
		}

		Vector3<T> vecFromBase = pt - m_centerOfBase;
		if (UP::Same(m_up)) {
			// m_up is the same as cylinder UP (external transform is translation only)
			return Contains_ObjectSpace(vecFromBase, fTolerance);
		}

		// m_up is transformed and pointing at an arbitrary direction
		T projOnDir = m_up.Dot(vecFromBase);
		if (projOnDir < -fTolerance || projOnDir > m_height + fTolerance) {
			return false;
		}

		T distSqFromAxis = vecFromBase.LengthSquared() - (projOnDir * projOnDir);
		return distSqFromAxis <= (m_radius + fTolerance) * (m_radius + fTolerance);
	}

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt) const override { return Contains_ObjectSpace(pt, static_cast<T>(0)); }

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt, T fTolerance) const override {
		return !IsDegenerated() &&
			   UP::Get(pt) >= m_base - fTolerance &&
			   UP::Get(pt) <= m_base + m_height + fTolerance &&
			   UP::Dist(pt) <= (m_radius + fTolerance) * (m_radius + fTolerance);
	}

	virtual void GenerateVisualMesh(TriList& triangleList) const override {
		// Winding order: CW
		const size_t NUM_SECTORS = 12;
		PrimitiveMesh<T>::AddCircularPrism<UP>(Vector3<T>::Zero(), 1, 1, 1, NUM_SECTORS, triangleList); // Unit cylinder (with major axis parallel to Z) for possible reuse of vertex buffers
	}

	virtual void GetVisualMeshMatrix(Matrix44<T>& matrix) const override {
		Primitive<T>::GetVisualMeshMatrix(matrix);
		// Additional scaling with radius since returned visual mesh is for a unit cylinder
		matrix.GetRow(RIGHT::value) *= m_radius; // Non-uniform scaling
		matrix.GetRow(UP::value) *= m_height;
		matrix.GetRow(FWD::value) *= m_radius;
		matrix.GetTranslation() = m_centerOfBase;
	}

private:
	T m_radius;
	T m_height;
	T m_base;
	Vector3<T> m_centerOfBase;
	Vector3<T> m_up;
};

} // namespace KEP
