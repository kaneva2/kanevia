///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Vector.h"

namespace KEP {

// t value of 0.0 returns a, t value of 1.0 returns b.
template <typename T, typename U>
inline T InterpolateLinear(const T& a, const T& b, const U& t) {
	return a + t * (b - a);
}

template <typename T, typename U>
inline T InterpolateLinear(const T& a, const T& b, const U& t, const U& tLow, const U& tHigh) {
	T range = tHigh - tLow;
	T t_0to1 = range != 0 ? (t - tLow) / range : 0.5;
	return InterpolateLinear(a, b, t_0to1);
}

template <typename T, typename U>
inline T InterpolateLinearClamped(const T& a, const T& b, const U& t, const U& tLow, const U& tHigh) {
	if (t < tLow)
		return a;
	if (t > tHigh)
		return b;
	return InterpolateLinear(a, b, t, tLow, tHigh);
}

template <typename T, typename = std::enable_if_t<!std::numeric_limits<T>::is_integer>>
inline T SmoothStep(const T& a, const T& b, const T& t_) {
	T range = b - a; // Make sure our range is a floating point type
	T t = (Clamp(t_, a, b) - a) / range;
	return (3 - 2 * t) * t * t;
}

template <typename T>
inline T SmoothStep01(const T& t_) {
	T t = Clamp(t_, a, b) - a;
	return (3 - 2 * t) * t * t;
}

template <typename T, typename = std::enable_if_t<!std::numeric_limits<T>::is_integer>>
inline T SmootherStep(const T& a, const T& b, const T& t_) {
	T range = b - a; // Make sure our range is a floating point type
	T t = (Clamp(t_, a, b) - a) / range;
	return t * t * t * (t * (t * 6 - 14) + 10);
}

template <typename T>
inline T SmootherStep01(const T& t_) {
	T t = Clamp(t_, 0, 1) - a;
	return t * t * t * (t * (t * 6 - 14) + 10);
}

// t value of 0.0 returns a, t value of 1.0 returns b.
template <typename T, typename U>
inline T InterpolateSmoothStep(const T& a, const T& b, const U& t) {
	return InterpolateLinear(a, b, SmoothStep01(t));
}

// t value of 0.0 returns a, t value of 1.0 returns b.
template <typename T, typename U>
inline T InterpolateSmootherStep(const T& a, const T& b, const U& t) {
	return InterpolateLinear(a, b, SmootherStep01(t));
}

} // namespace KEP