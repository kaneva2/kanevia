///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Vector.h"

namespace KEP {

// 2x2 system of linear equations by method of determinants.
// Solves for result where result[0] * vColumn0 + result[1] * vColumn1 = vRHS
template <typename T>
bool SolveLinearByColumn2x2(const Vector2<T>& vColumn0, const Vector2<T>& vColumn1, const Vector2<T>& vRHS, Out<Vector2<T>> result) {
	T fDenom = vColumn0.Cross(vColumn1);
	if (vColumn0.LengthSquared() * vColumn1.LengthSquared() * (1 - 4 * NumericTraits<T>::CosineSquaredTolerance()) < Square(fDenom))
		return false;

	T fReciprocalDenom = Reciprocal_Exact(fDenom);
	T fNumerator0 = vRHS.Cross(vColumn1);
	T fNumerator1 = vColumn0.Cross(vRHS);
	result.set(Vector2<T>(fNumerator0, fNumerator1) * fReciprocalDenom);
	return true;
}

template <typename T>
bool SolveLinearByRow2x2(const Vector2<T>& vRow0, const Vector2<T>& vRow1, const Vector2<T>& vRHS, Out<Vector2<T>> result) {
	return SolveLinearByColumn2x2(Vector2<T>(vRow0[0], vRow1[0]), Vector2<T>(vRow0[1], vRow1[1]), vRHS, result);
}

template <typename T>
bool SolveLinearByRow2x3(const Vector3<T>& vRow0, const Vector3<T>& vRow1, Out<Vector2<T>> result) {
	return SolveLinearByColumn2x2(Vector2<T>(vRow0[0], vRow1[0]), Vector2<T>(vRow0[1], vRow1[1]), Vector2<T>(-vRow0[2], -vRow1[2]), result);
}

template <typename T>
inline bool SolveLinear(T m00, T m01, T m02, T m10, T m11, T m12, T m20, T m21, T m22, T rhs0, T rhs1, T rhs2, T* x0, T* x1, T* x2) {
	Vector3<T> vCross01 = Vector3<T>(m00, m10, m20).Cross(Vector3<T>(m01, m11, m21));
	T fDenom = vCross01.Dot(Vector3<T>(m02, m12, m22)); // Determinant of 3x3 matrix.
	if (fDenom == 0)
		return false;

	T fRecipDenom = Reciprocal_Exact(fDenom);

	// Determinants of matrix with respective columns replaced by RHS
	Vector3<T> vCross2RHS = Vector3<T>(m02, m12, m22).Cross(Vector3<T>(rhs0, rhs1, rhs2));
	T fNumerator0 = vCross2RHS.Dot(Vector3<T>(m01, m11, m21));
	T fNumerator1 = -Vector3<T>(m00, m10, m20).Dot(vCross2RHS); //vColumn0.Dot(vRHS.Cross(vColumn2));
	T fNumerator2 = vCross01.Dot(Vector3<T>(rhs0, rhs1, rhs2));

	*x0 = fNumerator0 * fRecipDenom;
	*x1 = fNumerator1 * fRecipDenom;
	*x2 = fNumerator2 * fRecipDenom;

	return true;
}

template <typename T>
inline bool SolveLinear(T m00, T m01, T m02, T m10, T m11, T m12, T m20, T m21, T m22, T rhs0, T rhs1, T rhs2, T* x0, T* x1, T* x2, T tolerance) {
	Vector3<T> vCross01 = Vector3<T>(m00, m10, m20).Cross(Vector3<T>(m01, m11, m21));
	T fDenom = vCross01.Dot(Vector3<T>(m02, m12, m22)); // Determinant of 3x3 matrix.
	if (Square(fDenom) <= Square(tolerance) * Vector3<T>(m00, m10, m20).LengthSquared() * Vector3<T>(m01, m11, m21).LengthSquared() * Vector3<T>(m02, m12, m22).LengthSquared())
		return false;

	T fRecipDenom = Reciprocal_Exact(fDenom);

	// Determinants of matrix with respective columns replaced by RHS
	Vector3<T> vCross2RHS = Vector3<T>(m02, m12, m22).Cross(Vector3<T>(rhs0, rhs1, rhs2));
	T fNumerator0 = vCross2RHS.Dot(Vector3<T>(m01, m11, m21));
	T fNumerator1 = -Vector3<T>(m00, m10, m20).Dot(vCross2RHS); //vColumn0.Dot(vRHS.Cross(vColumn2));
	T fNumerator2 = vCross01.Dot(Vector3<T>(rhs0, rhs1, rhs2));

	*x0 = fNumerator0 * fRecipDenom;
	*x1 = fNumerator1 * fRecipDenom;
	*x2 = fNumerator2 * fRecipDenom;

	return true;
}

// 3x3 system of linear equations by method of determinants.
// Solves for result where result[0] * vColumn0 + result[1] * vColumn1 + result[2] * vColumn2 = vRHS
template <typename T>
inline bool SolveLinearByColumn3x3(const Vector3<T>& vColumn0, const Vector3<T>& vColumn1, const Vector3<T>& vColumn2, const Vector3<T>& vRHS, Out<Vector3<T>> result) {
	Vector3<T> vCross01 = vColumn0.Cross(vColumn1);
	T fDenom = vCross01.Dot(vColumn2); // Determinant of 3x3 matrix.
	if (fDenom == 0)
		return false;

	T fRecipDenom = Reciprocal_Exact(fDenom);

	// Determinants of matrix with respective columns replaced by RHS
	Vector3<T> vCross2RHS = vColumn2.Cross(vRHS);
	T fNumerator0 = vCross2RHS.Dot(vColumn1);
	T fNumerator1 = -vColumn0.Dot(vCross2RHS); //vColumn0.Dot(vRHS.Cross(vColumn2));
	T fNumerator2 = vCross01.Dot(vRHS);

	result.set(Vector3<T>(fNumerator0, fNumerator1, fNumerator2) * fRecipDenom);

	return true;
}

template <typename T>
inline bool SolveLinearByRow3x3(const Vector3<T>& vRow0, const Vector3<T>& vRow1, const Vector3<T>& vRow2, const Vector3<T>& vRHS, Out<Vector3<T>> result) {
	return SolveLinearByColumn3x3(Vector3<T>(vRow0[0], vRow1[0], vRow2[0]), Vector3<T>(vRow0[1], vRow1[1], vRow2[1]), Vector3<T>(vRow0[2], vRow1[2], vRow2[2]), vRHS, result);
}

template <typename T>
inline bool SolveLinearByRow3x4(const Vector4f& vRow0, const Vector4f& vRow1, const Vector4f& vRow2, Out<Vector3<T>> result) {
	return SolveLinearByColumn3x3(Vector3<T>(vRow0[0], vRow1[0], vRow2[0]), Vector3<T>(vRow0[1], vRow1[1], vRow2[1]), Vector3<T>(vRow0[2], vRow1[2], vRow2[2]), Vector3<T>(-vRow0[3], -vRow1[3], -vRow2[3]), result);
}

} // namespace KEP
