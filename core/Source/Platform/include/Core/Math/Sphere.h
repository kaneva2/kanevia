///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "LinearSystem.h"
#include "Primitive.h"
#include "PrimitiveType.h"
#include "PrimitiveMesh.h"

namespace KEP {

// Basic sphere class with center and radius.
template <typename T>
class Sphere : public Primitive<T> {
public:
	Sphere() {}
	Sphere(const Vector3<T>& ptCenter, T fRadius) :
			m_ptCenter(ptCenter), m_fRadius(fRadius) { SetDegenerated(fRadius <= 0); }
	void Set(const Vector3<T>& ptCenter, T fRadius) {
		m_ptCenter = ptCenter;
		m_fRadius = fRadius;
		SetDegenerated(fRadius <= 0);
	}

	const Vector3<T>& GetCenter() const { return m_ptCenter; }
	T GetRadius() const { return m_fRadius; }

	virtual int GetType() const override { return PrimitiveType::Sphere; }

	virtual void SetTransform(const Vector3<T>& pt, const Vector3<T>& dir, const Vector3<T>& up) override {
		Primitive<T>::SetTransform(pt, dir, up);
		m_ptCenter = pt;
	}

	virtual bool Contains(const Vector3<T>& pt, T fTolerance) const override {
		if (IsDegenerated()) {
			return false;
		}

		T dist2 = DistanceSquared(pt, m_ptCenter);
		T radius2 = Square(m_fRadius + fTolerance);
		return dist2 <= radius2;
	}

	virtual bool Contains(const Vector3<T>& pt) const override { return !IsDegenerated() && DistanceSquared(pt, m_ptCenter) <= Square(m_fRadius); }

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt) const override { return !IsDegenerated() && pt.LengthSquared() <= Square(m_fRadius); }
	virtual bool Contains_ObjectSpace(const Vector3<T>& pt, T fTolerance) const override { return !IsDegenerated() && pt.LengthSquared() <= Square(m_fRadius + fTolerance); }

	bool IsEmpty() const { return IsDegenerated(); }
	static Sphere GetEmpty() { return Sphere(Vector3<T>(0, 0, 0), -1); }

	virtual void GenerateVisualMesh(TriList& triangleList) const override {
		// Winding order: CCW
		const size_t NUM_SEGMENTS = 12;
		const size_t NUM_RINGS = 20;
		PrimitiveMesh<T>::AddUnitUVSphere(NUM_SEGMENTS, NUM_RINGS, triangleList); // Return a unit sphere for possible reuse of vertex buffers
	}

	virtual void GetVisualMeshMatrix(Matrix44<T>& matrix) const override {
		Primitive<T>::GetVisualMeshMatrix(matrix);
		// Additional scaling with radius since returned visual mesh is for a unit sphere
		matrix.GetRowX() *= m_fRadius;
		matrix.GetRowY() *= m_fRadius;
		matrix.GetRowZ() *= m_fRadius;
	}

private:
	Vector3<T> m_ptCenter;
	T m_fRadius;
};

// A sphere storing the squared radius instead of the radius.
// This simplifies calculations of distance to points and avoids a square root in construction the sphere.
template <typename T>
class SphereSquared {
public:
	SphereSquared() {}
	SphereSquared(const Vector3<T>& ptCenter, T fRadiusSquared) :
			m_ptCenter(ptCenter), m_fRadiusSquared(fRadiusSquared) {}
	void Set(const Vector3<T>& ptCenter, T fRadiusSquared) {
		m_ptCenter = ptCenter;
		m_fRadiusSquared = fRadiusSquared;
	}

	const Vector3<T>& GetCenter() const { return m_ptCenter; }
	T GetRadiusSquared() const { return m_fRadiusSquared; }
	bool Contains(const Vector3<T>& pt) const { return DistanceSquared(pt, m_ptCenter) <= m_fRadiusSquared; }
	bool IsEmpty() const { return m_fRadiusSquared < 0; }
	static SphereSquared GetEmpty() { return SphereSquared(Vector3<T>(0, 0, 0), -1); }

private:
	Vector3<T> m_ptCenter;
	T m_fRadiusSquared;
};

template <typename T>
Sphere<T> CreateSphereThroughPoints(const Vector3<T>& pt0, const Vector3<T>& pt1) {
	return Sphere(T(0.5) * (pt0 + pt1), T(0.5) * Distance(pt0, pt1));
}

template <typename T>
SphereSquared<T> CreateSphereSquaredThroughPoints(const Vector3<T>& pt0, const Vector3<T>& pt1) {
	return SphereSquared(T(0.5) * (pt0 + pt1), T(0.25) * DistanceSquared(pt0, pt1));
}

// Algorithm from Wikipedia. Calculates circumcenter using lengths of sides to calculate barycentric coordinates.
// Slower than version that solves via perpendiculars.
// Performance analysis:
// With tolerance: 21 *, 31 +, 1 <, 1 /
// Zero tolerance: 21 *, 31 +, 1 ==, 1 /
template <typename T>
bool CalculateCircumcenterOfPoints_Lengths(const Vector3<T>& pt0, const Vector3<T>& pt1, const Vector3<T>& pt2, Out<Vector3<T>> ptCenter) {
	Vector3<T> v01 = pt1 - pt0;
	Vector3<T> v12 = pt2 - pt1;
	Vector3<T> v20 = pt0 - pt2;
	T fLengthSq01 = v01.LengthSquared();
	T fLengthSq12 = v12.LengthSquared();
	T fLengthSq20 = v20.LengthSquared();

	T fBaryCoord01 = fLengthSq01 * (fLengthSq20 + fLengthSq12 - fLengthSq01);
	T fBaryCoord12 = fLengthSq12 * (fLengthSq01 + fLengthSq20 - fLengthSq12);
	T fBaryCoord20 = fLengthSq20 * (fLengthSq12 + fLengthSq01 - fLengthSq20);

	T fLengthSqSum = fLengthSq01 + fLengthSq12 + fLengthSq20;
	T fBaryCoordSum = fBaryCoord01 + fBaryCoord12 + fBaryCoord20;
	//T fTolerance = std::numeric_limits<T>::epsilon();
	//if( fBaryCoordSum  < fTolerance * fLengthSqSum )
	if (fBaryCoordSum == 0)
		return false; // Collinear points.

	T fBaryNormalizationFactor = Reciprocal_Exact(fBaryCoordSum);
	T fNormalizedBary01 = fBaryCoord01 * fBaryNormalizationFactor;
	//T fNormalizedBary12 = fBaryCoord12 * fBaryNormalizationFactor;
	T fNormalizedBary20 = fBaryCoord20 * fBaryNormalizationFactor;
	ptCenter.set(pt0 + v01 * fNormalizedBary20 - v20 * fNormalizedBary01);

	return true;
}

// Returns the offset from pt0 to the center of the sphere through pt0, pt1, and pt2
// So center = pt0 + ptRelativeCenter, radius = ptRelativeCenter.Length()
// Solve for point that is perpendicular bisector of segments (pt0,pt1) and (pt0,pt2)
// Segment slopes are V01 = pt1-pt0 and V02 = pt2-pt0
// Define center (C) as linear combination of input points with variables 'a' and 'b': C = pt0 + a * V01 + b * V02
// Midpoints are M01 = 0.5*(pt0+pt1) and M02 = 0.5*(pt0+pt2)
// Equations are V01.Dot(C-M01) == 0 and V02.Dot(C-M02) == 0
// After some algebra, final equations are:
//     a * V01.Dot(V01) + b * V01.Dot(V02) = 0.5 * V01.Dot(V01)
//     a * V01.Dot(V02) + b * V02.Dot(V02) = 0.5 * V02.Dot(V02)
// Performance analysis:
// With tolerance: 22 *, 22 +, 1 <, 1 /
// Zero tolerance: 21 *, 22 +, 1 ==, 1 /
template <typename T>
bool CalculateRelativeCircumcenterOfPoints(const Vector3<T>& pt0, const Vector3<T>& pt1, const Vector3<T>& pt2, Out<Vector3<T>> ptRelativeCenter) {
	Vector3<T> v01 = pt1 - pt0;
	Vector3<T> v02 = pt2 - pt0;
	T fLengthSquared01 = v01.LengthSquared();
	T fLengthSquared02 = v02.LengthSquared();
	T fDot_01_02 = v01.Dot(v02);

	// Since the rhs duplicates values in the lhs (scaled by 0.5), we can remove redundant calculations if we inline the SolveLinear call.
	// Since it's only 2x2, it doesn't add much complexity.
#if 0
	Vector2<T> vRow0(fLengthSquared01, fDot_01_02);
	Vector2<T> vRow1(fDot_01_02, fLengthSquared02);
	Vector2<T> vRHS(0.5f * fLengthSquared01, 0.5f * fLengthSquared02);

	Vector2<T> vRelativeCenter;
	if( !SolveLinearByRow2x2(vRow0, vRow1, vRHS, out(vRelativeCenter)) )
		return false; // Collinear points.
#else
	T fDot_01_02_Sq = Square(fDot_01_02);
	T fLengthSquared_01_02 = fLengthSquared01 * fLengthSquared02;
	//T fTolerance = (1-NumericTraits<T>::CosineSquaredTolerance())
	//if( fLengthSquared_01_02 * fTolerance <= fDot_01_02_Sq )
	//	return false; // Collinear points.
	T fDenom = fLengthSquared_01_02 - fDot_01_02_Sq;
	if (fDenom == 0)
		return false; // Collinear points.

	T fTwiceDenom = fDenom + fDenom;
	T fReciprocalTwiceDenom = Reciprocal_Exact(fTwiceDenom);

	T fTwiceNumerator0 = (fLengthSquared_01_02 - fDot_01_02 * fLengthSquared02);
	T fTwiceNumerator1 = (fLengthSquared_01_02 - fLengthSquared01 * fDot_01_02);

	Vector2<T> vRelativeCenter;
	vRelativeCenter[0] = fTwiceNumerator0 * fReciprocalTwiceDenom;
	vRelativeCenter[1] = fTwiceNumerator1 * fReciprocalTwiceDenom;
#endif
	ptRelativeCenter.set(vRelativeCenter[0] * v01 + vRelativeCenter[1] * v02);

	return true;
}

// Circumcenter of a 2D circle
template <typename T>
bool CalculateCircumcenterOfPoints(const Vector3<T>& pt0, const Vector3<T>& pt1, const Vector3<T>& pt2, Out<Vector3<T>> ptCenter) {
	Vector3<T> ptRelativeCenter;
	if (!CalculateRelativeCircumcenterOfPoints(pt0, pt1, pt2, out(ptRelativeCenter)))
		return false;

	ptCenter.set(pt0 + ptRelativeCenter);
	return true;
}

template <typename T>
inline bool CreateSphereSquaredThroughPoints(const Vector3<T>& pt0, const Vector3<T>& pt1, const Vector3<T>& pt2, Out<SphereSquared<T>> sphereSquared) {
	Vector3<T> ptRelativeCenter;
	if (!CalculateRelativeCircumcenterOfPoints(pt0, pt1, pt2, out(ptRelativeCenter)))
		return false;
	sphereSquared.get().Set(ptRelativeCenter + pt0, ptRelativeCenter.LengthSquared());
	return true;
}

// Spherical circumcenter.
// Solve for point that is perpendicular bisector of segments (pt0,pt1), (pt0,pt2), and (pt0,pt3)
// Segment slopes are V01 = pt1-pt0, V03 = pt3-pt0, and V02 = pt3-pt0
// These points and slopes define 3 planes. We calculate the intersection of these planes to get the center of the sphere.
// The center point returned is relative to pt0. I.e. the true center is pt0 + ptCenter.
template <typename T>
inline bool CalculateRelativeCircumcenterOfPoints(const Vector3<T>& pt0, const Vector3<T>& pt1, const Vector3<T>& pt2, const Vector3<T>& pt3, Out<Vector3<T>> ptRelativeCenter) {
	Vector3<T> vNormal01 = pt1 - pt0;
	Vector3<T> vNormal02 = pt2 - pt0;
	Vector3<T> vNormal03 = pt3 - pt0;

	//Vector3<T> ptMid01 = 0.5f * vNormal01;
	//Vector3<T> ptMid02 = 0.5f * vNormal02;
	//Vector3<T> ptMid03 = 0.5f * vNormal03;

	// Apply the 0.5f factor after the dot product to reduce the number of multiplications.
	Vector3<T> vEquals(0.5f * vNormal01.Dot(vNormal01), 0.5f * vNormal02.Dot(vNormal02), 0.5f * vNormal03.Dot(vNormal03));

	if (!SolveLinearByRow3x3(vNormal01, vNormal02, vNormal03, vEquals, out(ptRelativeCenter)))
		return false;

	//Vector3<T> ptCenter = ptRelativeCenter + pt0;
	//T fRadiusSquared = ptRelativeCenter.LengthSquared();

	return true;
}

template <typename T>
inline bool CalculateCircumcenterOfPoints(const Vector3<T>& pt0, const Vector3<T>& pt1, const Vector3<T>& pt2, const Vector3<T>& pt3, Out<Vector3<T>> ptCenter) {
	Vector3<T> ptRelativeCenter;
	if (!CalculateRelativeCircumcenterOfPoints(pt0, pt1, pt2, pt3, out(ptRelativeCenter)))
		return false;
	ptCenter.set(ptRelativeCenter + pt0);
	return true;
}

template <typename T>
inline bool CreateSphereSquaredThroughPoints(const Vector3<T>& pt0, const Vector3<T>& pt1, const Vector3<T>& pt2, const Vector3<T>& pt3, Out<SphereSquared<T>> sphereSquared) {
	Vector3<T> ptRelativeCenter;
	if (!CalculateRelativeCircumcenterOfPoints(pt0, pt1, pt2, pt3, out(ptRelativeCenter)))
		return false;
	sphereSquared.get().Set(ptRelativeCenter + pt0, ptRelativeCenter.LengthSquared());
	return true;
}

} // namespace KEP
