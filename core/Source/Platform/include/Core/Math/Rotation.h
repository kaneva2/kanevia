///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Quaternion.h"
#include "Matrix.h"
#include "GTE_Util.h"

#include "Core/Util/Parameter.h"

#include "../Tools/GeometricTools/GteRotation.h"

namespace KEP {

template <typename T>
inline void ConvertRotation(const Quaternion<T>& q, Out<Matrix<4, 4, T>> m) {
	Quaternion<T> q2 = q;
	if (!q2.Normalize()) {
		// Invalid zero quaternion. Use identity matrix.
		m.get().MakeIdentity();
		return;
	}
	gte::Rotation<4, T> r(reinterpret_cast<const gte::Quaternion<T>&>(q2));
	reinterpret_cast<gte::Matrix<4, 4, T>&>(m.get()) = r;
}

template <typename T>
inline void ConvertRotation(const Matrix<4, 4, T>& m, Out<Quaternion<T>> q) {
	gte::Rotation<4, T> r(reinterpret_cast<const gte::Matrix<4, 4, T>&>(m));
	reinterpret_cast<gte::Quaternion<T>&>(q.get()) = r;
}

template <typename T>
inline void ConvertRotation(const Matrix<3, 3, T>& m, Out<Quaternion<T>> q) {
	gte::Rotation<3, T> r(reinterpret_cast<const gte::Matrix<3, 3, T>&>(m));
	reinterpret_cast<gte::Quaternion<T>&>(q.get()) = r;
}

template <typename T>
inline void ConvertRotation(const Vector<3, T>& axis, const T& angleRadians, Out<Matrix<4, 4, T>> m) {
	ConvertRotation(Quaternion<T>(axis, angleRadians), m);
}

// Rotations around a coordinate axis
template <typename T, typename U>
inline void ConvertRotation(unsigned int iAxis, const U& angleRadians, Out<Matrix<4, 4, T>> m) {
	ConvertRotation(Quaternion<T>(iAxis, static_cast<T>(angleRadians)), m);
}

// Convert to euler angles applied in given coordinate order.
template <typename Source, typename TEuler, typename TOther, size_t MatrixDim = 3, std::enable_if_t<!std::is_integral<TEuler>::value, nullptr_t> = nullptr>
inline void ConvertRotation_Implementation(const Vector<3, int>& vAxes, const Source& src, Out<Vector<3, TEuler>> vEuler) {
	gte::Rotation<MatrixDim, TOther> rot(ToGTE(src));
	gte::EulerAngles<TOther> ea = rot(vAxes[0], vAxes[1], vAxes[2]);
	vEuler.get() = Vector3f(ea.angle[0], ea.angle[1], ea.angle[2]);
}

// Scale euler angle to range of integral type.
template <typename Source, typename TEuler, typename TOther, size_t MatrixDim = 3, std::enable_if_t<std::is_integral<TEuler>::value, nullptr_t> = nullptr>
inline void ConvertRotation_Implementation(const Vector<3, int>& vAxes, const Source& src, Out<Vector<3, TEuler>> vEulerI) {
	Vector3<TOther> vEulerF;
	ConvertRotation_Implementation<Source, TOther, TOther>(vAxes, src, out(vEulerF));
	constexpr TOther scaleFactor = (TOther(1) + (std::numeric_limits<TEuler>::max)() - (std::numeric_limits<TEuler>::min)()) / TOther(2 * M_PI);
	for (size_t i = 0; i < 3; ++i) {
		TOther fCoord = vEulerF[i] * scaleFactor + (std::numeric_limits<TEuler>::min)();
		vEulerI.get()[i] = std::lround(fCoord);
	}
}

// Convert from euler angles applied in give coordinate order.
template <typename Dest, typename TEuler, typename TOther, size_t MatrixDim = 3, std::enable_if_t<!std::is_integral<TEuler>::value, nullptr_t> = nullptr>
inline void ConvertRotation_Implementation(const Vector<3, int>& vAxes, const Vector<3, TEuler>& vEuler, Out<Dest> dest) {
	gte::EulerAngles<TOther> ea(vAxes[0], vAxes[1], vAxes[2], vEuler.x, vEuler.y, vEuler.z);
	gte::Rotation<MatrixDim, TOther> rot(ea);
	ToGTE(dest.get()) = rot;
}

// Scale euler angle to range of integral type.
template <typename Dest, typename TEuler, typename TOther, size_t MatrixDim = 3, std::enable_if_t<std::is_integral<TEuler>::value, nullptr_t> = nullptr>
inline void ConvertRotation_Implementation(const Vector<3, int>& vAxes, const Vector<3, TEuler>& vEulerI, Out<Dest> dest) {
	Vector3<TOther> vEulerF;
	constexpr TOther scaleFactor = TOther(2 * M_PI) / (TOther(1) + (std::numeric_limits<TEuler>::max)() - (std::numeric_limits<TEuler>::min)());
	for (size_t i = 0; i < 3; ++i)
		vEulerF[i] = (TOther(vEulerI[i]) - (std::numeric_limits<TEuler>::min)()) * scaleFactor;
	ConvertRotation_Implementation<Dest, TOther, TOther>(vAxes, vEulerF, dest);
}

// Convert quaternion to euler angles.
template <typename T, typename U>
inline void ConvertRotation(const Vector<3, int>& vAxes, const Quaternion<T>& q, Out<Vector<3, U>> vEuler) {
	Quaternion<T> q2 = q;
	q2.Normalize();
	ConvertRotation_Implementation<Quaternion<T>, U, T>(vAxes, q2, vEuler);
}

// Convert 3x3 matrix to euler angles.
template <typename T, typename U>
inline void ConvertRotation(const Vector<3, int>& vAxes, const Matrix<3, 3, T>& m, Out<Vector<3, U>> vEuler) {
	ConvertRotation_Implementation<Matrix33<T>, U, T>(vAxes, m, vEuler);
}

// Convert 4x4 matrix to euler angles.
template <typename T, typename U>
inline void ConvertRotation(const Vector<3, int>& vAxes, const Matrix<4, 4, T>& m, Out<Vector<3, U>> vEuler) {
	ConvertRotation_Implementation<Matrix<4, 4, T>, U, T, 4>(vAxes, m, vEuler);
}

// Convert direction (Z) and up (Y) vectors to euler angles.
template <typename T, typename U>
inline bool ConvertRotation(const Vector<3, int>& vAxes, const Vector3<T>& dir, const Vector3<T>& up, Out<Vector<3, U>> vEuler) {
	Matrix33<T> m;
	m[0] = up.Cross(dir);
	m[1] = up.GetPartPerpendicularTo(dir);
	if (m[0].Normalize() && m[1].Normalize()) {
		m[2] = dir.GetNormalizedUnchecked();
		ConvertRotation(vAxes, m, vEuler);
		return true;
	} else {
		vEuler.get().Set(0, 0, 0);
		return false;
	}
}

// Convert euler angles to quaternion.
template <typename T, typename U>
inline void ConvertRotation(const Vector<3, int>& vAxes, const Vector<3, T>& vEuler, Out<Quaternion<U>> q) {
	ConvertRotation_Implementation<Quaternion<U>, T, U>(vAxes, vEuler, q);
}

// Convert euler angles to Matrix44.
template <typename T, typename U>
inline void ConvertRotation(const Vector<3, int>& vAxes, const Vector<3, T>& vEuler, Out<Matrix<4, 4, U>> mat) {
	ConvertRotation_Implementation<Matrix<4, 4, U>, T, U, 4>(vAxes, vEuler, mat);
}

// Convert euler angles to Matrix33.
template <typename T, typename U>
inline void ConvertRotation(const Vector<3, int>& vAxes, const Vector<3, T>& vEuler, Out<Matrix<3, 3, U>> mat) {
	ConvertRotation_Implementation<Matrix<3, 3, U>, T, U, 3>(vAxes, vEuler, mat);
}

// Convert euler angles to direction (Z) and up (Y) vector.
template <typename T, typename U>
inline void ConvertRotation(const Vector<3, int>& vAxes, const Vector<3, T>& vEuler, Out<Vector3<U>> dir, Out<Vector3<U>> up) {
	Matrix33<U> m;
	ConvertRotation(vAxes, vEuler, out(m));
	dir.set(m[2]);
	up.set(m[1]);
}

// XYZ order
template <typename T, typename Dest>
inline void ConvertRotationXYZ(const Vector<3, T>& vEuler, Out<Dest> dest) {
	ConvertRotation(Vector<3, int>(0, 1, 2), vEuler, dest);
}

template <typename T, typename Src>
inline void ConvertRotationXYZ(const Src& src, Out<Vector<3, T>> vEuler) {
	ConvertRotation(Vector<3, int>(0, 1, 2), src, vEuler);
}

// ZYX order
template <typename T, typename Dest>
inline void ConvertRotationZYX(const Vector<3, T>& vEuler, Out<Dest> dest) {
	ConvertRotation(Vector<3, int>(2, 1, 0), Vector<3, T>(vEuler.Z(), vEuler.Y(), vEuler.X()), dest);
}

template <typename T, typename Src>
inline void ConvertRotationZYX(const Src& src, Out<Vector<3, T>> vEuler) {
	Vector<3, T> v;
	ConvertRotation(Vector<3, int>(2, 1, 0), src, out(v));
	vEuler.get().Set(v.Z(), v.Y(), v.X());
}

// ZXY order
template <typename T, typename Dest>
inline void ConvertRotationZXY(const Vector<3, T>& vEuler, Out<Dest> dest) {
	ConvertRotation(Vector<3, int>(2, 0, 1), Vector<3, T>(vEuler.Z(), vEuler.X(), vEuler.Y()), dest);
}

template <typename T, typename U>
inline void ConvertRotationZXY(const Vector<3, T>& vEuler, Out<Vector3<U>> dir, Out<Vector3<U>> up) {
	ConvertRotation(Vector<3, int>(2, 0, 1), Vector<3, T>(vEuler.Z(), vEuler.X(), vEuler.Y()), dir, up);
}

template <typename T, typename Src>
inline void ConvertRotationZXY(const Src& src, Out<Vector<3, T>> vEuler) {
	Vector<3, T> v;
	ConvertRotation(Vector<3, int>(2, 0, 1), src, out(v));
	vEuler.get().Z() = v[0];
	vEuler.get().X() = v[1];
	vEuler.get().Y() = v[2];
}

template <typename T, typename U>
inline void ConvertRotationZXY(const Vector3<U>& dir, const Vector3<U>& up, Out<Vector<3, T>> vEuler) {
	Vector<3, T> v;
	ConvertRotation(Vector<3, int>(2, 0, 1), dir, up, out(v));
	vEuler.get().Z() = v[0];
	vEuler.get().X() = v[1];
	vEuler.get().Y() = v[2];
}

// Direct construction of a quaternion should be just as easy as calling a function,
// but may want to include it for completeness
#if 0
// Rotations around a coordinate axis, output to a quaternion
template<typename T, typename U>
inline void ConvertRotation(unsigned int iAxis, const U& angleRadians, Out<Quaternion<T>> q)
{
	q.get() = Quaternion<T>(iAxis, static_cast<T>(angleRadians));
}
#endif

template <typename T, typename U>
inline void ConvertRotationX(const U& angleRadians, Out<Matrix<4, 4, T>> m) {
	ConvertRotation(0, static_cast<T>(angleRadians), m);
}

template <typename T, typename U>
inline void ConvertRotationY(const U& angleRadians, Out<Matrix<4, 4, T>> m) {
	ConvertRotation(1, static_cast<T>(angleRadians), m);
}

template <typename T, typename U>
inline void ConvertRotationZ(const U& angleRadians, Out<Matrix<4, 4, T>> m) {
	ConvertRotation(2, static_cast<T>(angleRadians), m);
}

} // namespace KEP