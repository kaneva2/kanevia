///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Tools/GeometricTools/GteDistPointTriangleExact.h"
#include "GTE_Util.h"

namespace KEP {

template <typename T>
inline T DistanceSquaredPointTriangle(const Vector<3, T>& pt,
	const Vector<3, T>& ptTriangle1, const Vector<3, T>& ptTriangle2, const Vector<3, T>& ptTriangle3,
	Vector<3, T>* pClosestPointOnTriangle) {
	gte::DistancePointTriangleExact<3, T> query;
	gte::Triangle<3, T> gte_tri(
		ToGTE(ptTriangle1),
		reinterpret_cast<const gte::Vector<3, T>&>(ptTriangle2),
		reinterpret_cast<const gte::Vector<3, T>&>(ptTriangle3));
	gte::Vector<3, T> gte_pt = reinterpret_cast<const gte::Vector<3, T>&>(pt);

	// This call computes more than we actually need, so there is room for creating
	// an optimized version if needed.
	gte::DistancePointTriangleExact<3, T>::Result result = query(gte_pt, gte_tri);
	if (pClosestPointOnTriangle)
		*pClosestPointOnTriangle = reinterpret_cast<Vector<3, T>&>(result.closest);
	return result.sqrDistance;
}

template <size_t N, typename T>
inline Vector<N, T> OffsetBoxToPoint(const Vector<N, T>& pt, const Vector<N, T>& ptBoxMin, const Vector<N, T>& ptBoxMax) {
	Vector<N, T> vDirection;
	for (size_t i = 0; i < N; ++i) {
		const T& coord = pt[i];
		const T& coordMin = ptBoxMin[i];
		const T& coordMax = ptBoxMax[i];
		T coordDiff = 0;
		if (coord < coordMin)
			coordDiff += coordMin - coord;
		else if (coord > coordMax)
			coordDiff += coord - coordMax;
		vDirection[i] = coordDiff;
	}

	return vDirection;
}

template <size_t N, typename T>
inline T DistanceSquaredBoxToPoint(const Vector<N, T>& pt, const Vector<N, T>& ptBoxMin, const Vector<N, T>& ptBoxMax, Vector<N, T>* pvDirection = nullptr) {
	Vector<N, T> vOffset = OffsetBoxToPoint(pt, ptBoxMin, ptBoxMax);
	if (pvDirection)
		*pvDirection = vOffset;
	return vOffset.LengthSquared();
}

} // namespace KEP