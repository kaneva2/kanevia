///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Quaternion.h"

namespace KEP {

// Rotation and translation only
template <typename T>
class RigidTransform {
public:
	RigidTransform();

	void MakeIdentity();

private:
	Vector<3, T> m_vTranslation;
	Quaternion<T> m_qRotation;
};

template <typename T>
inline RigidTransform<T>::RigidTransform() {
}

template <typename T>
inline void RigidTransform<T>::MakeIdentity() {
	m_vTranslation.MakeZero();
	m_qRotation.MakeIdentity();
}

typedef RigidTransform<float> RigidTransformf;
} // namespace KEP