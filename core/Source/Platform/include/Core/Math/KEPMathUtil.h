///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "NumericTraits.h"
#include "Core/Util/Parameter.h"
#include "KEPMathConfig.h"

#define _USE_MATH_DEFINES // Otherwise VisualStudio does not #define M_PI when including math.h
#include <math.h>
#include <emmintrin.h>
#include <stdint.h>
#include <type_traits>

namespace KEP {

template <typename T>
inline T ReciprocalSqrt_Exact(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return 1 / std::sqrt(t);
}

template <typename T>
inline T ReciprocalSqrt_Close(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return 1 / std::sqrt(t);
}

template <typename T>
inline T ReciprocalSqrt_Approx(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return 1 / std::sqrt(t);
}

template <typename T>
inline T ReciprocalSqrt(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return ReciprocalSqrt_Close(t);
}

// Should be off by 12 or 13 bits.
// Tests on Intel Ivy Bridge processor reveal a maximum relative error of 1/3345
template <>
inline float ReciprocalSqrt_Approx<float>(const float& t) {
	__m128 input = _mm_set_ss(t);
	__m128 guess = _mm_rsqrt_ss(input); // Relative error is 1.5 * 2^-12 for Intel processors. AMD between 11-14 bits.
	return _mm_cvtss_f32(guess); // Approximate result
}

// Should be off by only 1 or 2 bits.
// Tests on Intel Ivy Bridge processor reveal a maximum relative error of 1/3735447
// Maximum possible precision for float would be 1/8388608
template <>
inline float ReciprocalSqrt_Close<float>(const float& t) {
	__m128 input = _mm_set_ss(t);
	__m128 guess = _mm_rsqrt_ss(input); // Relative error is 1.5 * 2^-12 for Intel processors. AMD between 11-14 bits.

	// Do one iteration of Newton-Raphson.
	// This approximately doubles the precision, getting us very close to 24 bits.
	// x_new = x * (1.5 - 0.5 * t * (x*x))
	__m128 half_input = _mm_set_ss(t * 0.5f);
	__m128 guess_squared = _mm_mul_ss(guess, guess);
	__m128 one_and_half = _mm_set_ss(1.5f);
	__m128 refined_once = _mm_mul_ss(guess, _mm_sub_ss(one_and_half, _mm_mul_ss(half_input, guess_squared)));
	float fResult = _mm_cvtss_f32(refined_once);
	return fResult;
}

template <>
inline float ReciprocalSqrt_Exact<float>(const float& t) {
	__m128 input = _mm_set_ss(t);
	__m128 guess = _mm_rsqrt_ss(input); // Relative error is 1.5 * 2^-12 for Intel processors. AMD between 11-14 bits.

	// Do two iterations of Newton-Raphson.
	__m128 half_input = _mm_set_ss(t * 0.5f);
	__m128 one_and_half = _mm_set_ss(1.5f);
	__m128 refined_once = _mm_mul_ss(guess, _mm_sub_ss(one_and_half, _mm_mul_ss(half_input, _mm_mul_ss(guess, guess))));
	__m128 refined_twice = _mm_mul_ss(refined_once, _mm_sub_ss(one_and_half, _mm_mul_ss(half_input, _mm_mul_ss(refined_once, refined_once))));
	float fResult = _mm_cvtss_f32(refined_twice);
	return fResult;
}

template <typename T>
inline T Sqrt_Exact(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return std::sqrt(t);
}

template <typename T>
inline T Sqrt_Close(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return std::sqrt(t);
}

template <typename T>
inline T Sqrt_Approx(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return std::sqrt(t);
}

template <typename T>
inline T Sqrt(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return Sqrt_Close(t);
}

template <>
inline float Sqrt_Exact<float>(const float& t) {
	return std::sqrt(t);
}

template <>
inline float Sqrt_Close<float>(const float& t) {
	return t == 0 ? 0 : t * ReciprocalSqrt_Close(t);
}

template <>
inline float Sqrt_Approx<float>(const float& t) {
	return t == 0 ? 0 : t * ReciprocalSqrt_Approx(t);
}

template <typename T>
inline T Reciprocal_Close(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return 1 / t;
}

template <typename T>
inline T Reciprocal_Exact(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return 1 / t;
}

template <typename T>
inline T Reciprocal_Approx(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return 1 / t;
}

template <typename T>
inline T Reciprocal(const T& t) {
	static_assert(std::is_floating_point<T>::value, "Template argument for this function needs to be a floating point type.");
	return Reciprocal_Close(t);
}

// Should be off by 12 or 13 bits.
// Tests on Intel Ivy Bridge processor reveal a maximum relative error of 1/3330
template <>
inline float Reciprocal_Approx<float>(const float& t) {
	__m128 input = _mm_set_ss(t);
	__m128 guess = _mm_rcp_ss(input);
	return _mm_cvtss_f32(guess);
}

// Should be off by only 1 or 2 bits.
// Tests on Intel Ivy Bridge processor reveal a maximum relative error of 1/4254466
// Maximum possible precision for float would be 1/8388608
template <>
inline float Reciprocal_Close<float>(const float& t) {
	__m128 input = _mm_set_ss(t);
	__m128 guess = _mm_rcp_ss(input);

	// Do one iteration of Newton-Raphson.
	// This approximately doubles the precision, giving us a very close reciprocal
	// x_new = x * (2.0 - t * x)
	__m128 two = _mm_set_ss(2.0f);
	__m128 refined_once = _mm_mul_ss(guess, _mm_sub_ss(two, _mm_mul_ss(input, guess)));
	float fResult = _mm_cvtss_f32(refined_once);
	return fResult;
}

template <>
inline float Reciprocal_Exact<float>(const float& t) {
	__m128 input = _mm_set_ss(t);
	__m128 guess = _mm_rcp_ss(input);

	// Do two iterations of Newton-Raphson.
	__m128 two = _mm_set_ss(2.0f);
	__m128 refined_once = _mm_mul_ss(guess, _mm_sub_ss(two, _mm_mul_ss(input, guess)));
	__m128 refined_twice = _mm_mul_ss(refined_once, _mm_sub_ss(two, _mm_mul_ss(input, refined_once)));
	float fResult = _mm_cvtss_f32(refined_twice);
	return fResult;
}

template <typename T1, typename T2>
inline decltype(*(T1*)0 / *(T2*)0) Divide(const T1& dividend, const T2& divisor) {
	return dividend / divisor;
}

template <typename T1>
inline decltype(*(T1*)0 / 1.0f) Divide(const T1& dividend, const float& divisor) {
	return dividend * Reciprocal(divisor);
}

// Function to square a number to increasing readablity of some expressions.
template <typename T>
inline /*constexpr*/ T Square(const T& t) {
	return t * t;
}

// Returns the first parameter clamped to the range [min,max]
template <typename T>
inline /*constexpr*/ T Clamp(const T& t, const std::common_type_t<T>& min, const std::common_type_t<T>& max) {
	return t < min ? min : t > max ? max : t;
}

template <>
inline /*constexpr*/ float Clamp<float>(const float& t, const std::common_type_t<float>& min, const std::common_type_t<float>& max) {
	return _mm_cvtss_f32(_mm_min_ss(_mm_max_ss(_mm_set_ss(t), _mm_set_ss(min)), _mm_set_ss(max)));
}

template <>
inline /*constexpr*/ __m128 Clamp<__m128>(const __m128& t, const std::common_type_t<__m128>& min, const std::common_type_t<__m128>& max) {
	return _mm_min_ps(_mm_max_ps(t, min), max);
}

template <typename T>
inline /*constexpr*/ T Min(const T& a, const std::common_type_t<T>& b) {
	return a < b ? a : b;
}

template <>
inline /*constexpr*/ float Min<float>(const float& a, const std::common_type_t<float>& b) {
	return _mm_cvtss_f32(_mm_min_ss(_mm_set_ss(a), _mm_set_ss(b)));
}

template <>
inline /*constexpr*/ __m128 Min<__m128>(const __m128& a, const std::common_type_t<__m128>& b) {
	return _mm_min_ps(a, b);
}

template <typename T>
inline /*constexpr*/ T Max(const T& a, const std::common_type_t<T>& b) {
	return a < b ? b : a;
}

template <>
inline /*constexpr*/ float Max<float>(const float& a, const std::common_type_t<float>& b) {
	return _mm_cvtss_f32(_mm_max_ss(_mm_set_ss(a), _mm_set_ss(b)));
}

template <>
inline /*constexpr*/ __m128 Max<__m128>(const __m128& a, const std::common_type_t<__m128>& b) {
	return _mm_max_ps(a, b);
}

// Sets minValue to minimum of "value" and its current value.
// Sets maxValue to maximum of "value" and its current value.
template <typename T>
inline void UpdateMinMax(const T& value, InOut<T> minValue_, InOut<T> maxValue_) {
	T& minValue = minValue_.get();
	T& maxValue = maxValue_.get();
	minValue = value < minValue ? value : minValue;
	maxValue = value > maxValue ? value : maxValue;
}

// std::abs is not defined as constexpr
// so define our own version to be used in constexpr intializations.
template <typename T>
inline /*constexpr*/ T ConstExprAbs(const T& t) {
	return !(t < 0) ? t : -t;
}

// Comparison for equality, using a tolerance.
template <typename T, typename U, typename V>
inline /*constexpr*/ bool AreEqual(const T& left, const U& right, const V& tolerance) {
	return ConstExprAbs(left - right) < tolerance;
}

namespace KEPMathDetail {
template <typename T>
static inline /*constexpr*/ T DegreesToRadiansFactor() {
	static /*constexpr*/ T degrees_to_radians = static_cast<T>(M_PI / 180);
	return degrees_to_radians;
}

template <typename T>
static inline /*constexpr*/ T RadiansToDegreesFactor() {
	static /*constexpr*/ T radians_to_degrees = static_cast<T>(180 / M_PI);
	return radians_to_degrees;
}

template <typename T, typename = std::enable_if<!std::numeric_limits<T>::is_integer>::type>
static inline /*constexpr*/ T DegreesToRadians(T degrees) {
	return degrees * DegreesToRadiansFactor<T>();
}

template <typename T>
static inline /*constexpr*/ T RadiansToDegrees(T radians) {
	return radians * RadiansToDegreesFactor<T>();
}
} // namespace KEPMathDetail

inline /*constexpr*/ float ToRadians(float degrees) {
	return KEPMathDetail::DegreesToRadians(degrees);
}
inline /*constexpr*/ double ToRadians(double degrees) {
	return KEPMathDetail::DegreesToRadians(degrees);
}
inline /*constexpr*/ float ToDegrees(float radians) {
	return KEPMathDetail::RadiansToDegrees(radians);
}
inline /*constexpr*/ double ToDegrees(double radians) {
	return KEPMathDetail::RadiansToDegrees(radians);
}
inline /*constexpr*/ float DegreesToRadians(float degrees) {
	return KEPMathDetail::DegreesToRadians(degrees);
}
inline /*constexpr*/ double DegreesToRadians(double degrees) {
	return KEPMathDetail::DegreesToRadians(degrees);
}
inline /*constexpr*/ float RadiansToDegrees(float radians) {
	return KEPMathDetail::RadiansToDegrees(radians);
}
inline /*constexpr*/ double RadiansToDegrees(double radians) {
	return KEPMathDetail::RadiansToDegrees(radians);
}

// Like the standard library function fmod, but always returns a positive value.
// FModPositive(+1,+4) == 1
// FModPositive(+1,-4) == 1
// FModPositive(-1,+4) == 3
// FModPositive(-1,-4) == 3
template <typename T, typename = std::enable_if_t<!std::is_integral<T>::value>>
inline T FModPositive(T x, T y) {
	T f = std::fmod(x, y);
	if (f < 0)
		f += std::abs(y);
	return f;
}

template <typename T>
inline T FModRange(T x, T low, T high) {
	return low + FModPositive(x - low, high - low);
}

// Rounds first argument to nearest multiple of the second.
template <typename T>
T Quantize(const T& value, const std::enable_if_t<std::is_floating_point<T>::value, T>& quantization) {
	T modulus = std::fmod(value, quantization); // modulus has same sign as value
	T result = value - modulus;
	T positive_quantization = std::abs(quantization);
	if (modulus < 0) {
		if (-modulus * 2 > positive_quantization)
			result -= positive_quantization;
	} else {
		if (modulus * 2 > positive_quantization)
			result += positive_quantization;
	}
	return result;
}

inline int32_t NearestInt(float f) {
	return _mm_cvtss_si32(_mm_set_ss(f));
}

inline int32_t NearestInt(double f) {
	return _mm_cvtsd_si32(_mm_set_sd(f));
}

namespace KEPMathDetail {
constexpr size_t knSinCosLookupTableEntries = 512;
extern KEPMATH_EXPORT float g_SinCosLookupTable[knSinCosLookupTableEntries];

// Table index is taken modulo knSinCosLookupTableEntries.
inline void SinCosTableLookup(float fIdx, Out<float> sine, Out<float> cosine) {
	uint32_t idx = NearestInt(fIdx);
	idx = idx % knSinCosLookupTableEntries;
	sine.set(g_SinCosLookupTable[(idx - knSinCosLookupTableEntries / 4) % knSinCosLookupTableEntries]);
	cosine.set(g_SinCosLookupTable[-int32_t(idx) % knSinCosLookupTableEntries]);
}

inline float SinTableLookup(float fIdx) {
	uint32_t idx = NearestInt(fIdx);
	idx = idx % knSinCosLookupTableEntries;
	float ret = g_SinCosLookupTable[(idx - knSinCosLookupTableEntries / 4) % knSinCosLookupTableEntries];
	return ret;
}

inline float CosTableLookup(float fIdx) {
	uint32_t idx = NearestInt(fIdx);
	idx = idx % knSinCosLookupTableEntries;
	float ret = g_SinCosLookupTable[-int32_t(idx) % knSinCosLookupTableEntries];
	return ret;
}
} // namespace KEPMathDetail

inline void SinCosCycles_Approx(float fCycles, Out<float> sine, Out<float> cosine) {
	static constexpr float fMultiplier = KEPMathDetail::knSinCosLookupTableEntries;
	return KEPMathDetail::SinCosTableLookup(fCycles * fMultiplier, sine, cosine);
}

inline void SinCosDegrees_Approx(float fDegrees, Out<float> sine, Out<float> cosine) {
	static constexpr float fMultiplier = KEPMathDetail::knSinCosLookupTableEntries / 360.0f;
	return KEPMathDetail::SinCosTableLookup(fDegrees * fMultiplier, sine, cosine);
}

inline void SinCosRadians_Approx(float fRadians, Out<float> sine, Out<float> cosine) {
	static constexpr float fMultiplier = float(KEPMathDetail::knSinCosLookupTableEntries / (2 * M_PI));
	return KEPMathDetail::SinCosTableLookup(fRadians * fMultiplier, sine, cosine);
}

inline float SinCycles(float fCycles) {
	static constexpr float fMultiplier = KEPMathDetail::knSinCosLookupTableEntries;
	return KEPMathDetail::SinTableLookup(fCycles * fMultiplier);
}

inline float SinDegrees_Approx(float fDegrees) {
	static constexpr float fMultiplier = KEPMathDetail::knSinCosLookupTableEntries / 360.0f;
	return KEPMathDetail::SinTableLookup(fDegrees * fMultiplier);
}

inline float SinRadians_Approx(float fRadians) {
	static constexpr float fMultiplier = float(KEPMathDetail::knSinCosLookupTableEntries / (2 * M_PI));
	return KEPMathDetail::SinTableLookup(fRadians * fMultiplier);
}

inline float CosCycles(float fCycles) {
	static constexpr float fMultiplier = KEPMathDetail::knSinCosLookupTableEntries;
	return KEPMathDetail::CosTableLookup(fCycles * fMultiplier);
}

inline float CosDegrees_Approx(float fDegrees) {
	static constexpr float fMultiplier = KEPMathDetail::knSinCosLookupTableEntries / 360.0f;
	return KEPMathDetail::CosTableLookup(fDegrees * fMultiplier);
}

inline float CosRadians_Approx(float fRadians) {
	static constexpr float fMultiplier = float(KEPMathDetail::knSinCosLookupTableEntries / (2 * M_PI));
	return KEPMathDetail::CosTableLookup(fRadians * fMultiplier);
}

} // namespace KEP
