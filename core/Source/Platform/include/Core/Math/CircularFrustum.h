///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Primitive.h"
#include "PrimitiveType.h"
#include "PrimitiveMesh.h"
#include "Cone.h"

namespace KEP {

// Truncated Circular Cone
template <typename T, typename FWD>
class CircularFrustum : public Primitive<T> {
	using RIGHT = typename FWD::NextAxis;
	using UP = typename FWD::PrevAxis;

public:
	CircularFrustum(T fov, T nearPlane, T farPlane) :
			m_halfFovRadians(fov * M_PI / 180 / 2),
			m_near(nearPlane),
			m_far(farPlane),
			m_cone(Vector3<T>(0, 0, 0), FWD::Basis(), cos(m_halfFovRadians)) {
		SetDegenerated(fov == 0 || m_near >= m_far);
	}

	virtual int GetType() const override { return PrimitiveType::CircularFrustum; }

	virtual void SetTransform(const Vector3<T>& pt, const Vector3<T>& dir, const Vector3<T>& up) override {
		Primitive<T>::SetTransform(pt, dir, up);
		m_cone.SetApex(pt);
		m_cone.SetDirection(GetObjectToWorldMatrix().GetRow<3>(FWD::value));
	}

	virtual bool Contains(const Vector3<T>& pt) const override { return Contains(pt, static_cast<T>(0)); }

	virtual bool Contains(const Vector3<T>& pt, T fTolerance) const override {
		if (IsDegenerated()) {
			return false;
		}

		if (!m_cone.IsInside(pt)) {
			return false;
		}

		// Near/far limits
		Vector3<T> vecFromApex = pt - m_cone.GetApex();
		T projOnDir = vecFromApex.Dot(m_cone.GetDirection());
		return projOnDir >= m_near - fTolerance && projOnDir <= m_far + fTolerance;
	}

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt) const override { return Contains_ObjectSpace(pt, static_cast<T>(0)); }

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt, T fTolerance) const override {
		if (IsDegenerated()) {
			return false;
		}

		Cone<T> cone(Vector3<T>(0, 0, 0), FWD::Basis(), m_cone.GetCosine());
		if (!cone.IsInside(pt)) {
			return false;
		}

		// Near/far limits
		T distFromApex = FWD::Get(pt);
		return distFromApex >= m_near - fTolerance && distFromApex <= m_far + fTolerance;
	}

	virtual void GenerateVisualMesh(TriList& triangleList) const override {
		// Winding order: CW
		const size_t NUM_SECTORS = 12;

		// Center of base (in object space: move m_near along primary axis from origin)
		Vector3<T> centerOfBase = FWD::Tangent(m_near);

		// tan(FOV/2)
		T tanHalfFov = tan(m_halfFovRadians);

		assert(m_far >= m_near);
		PrimitiveMesh<T>::AddCircularPrism<FWD>(centerOfBase, m_near * tanHalfFov, m_far * tanHalfFov, m_far - m_near, NUM_SECTORS, triangleList);
	}

private:
	T m_halfFovRadians;
	T m_near;
	T m_far;
	Cone<T> m_cone;
};

} // namespace KEP
