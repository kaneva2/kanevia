///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Primitive.h"
#include "PrimitiveType.h"
#include "PrimitiveMesh.h"

namespace KEP {

template <typename T>
class Cuboid : public Primitive<T> {
public:
	Cuboid(const Vector3<T>& min, const Vector3<T>& max) :
			m_min(min),
			m_max(max) {
		SetDegenerated(m_min.x == m_max.x || m_min.y == m_max.y || m_min.z == m_max.x);
	}

	virtual int GetType() const override { return PrimitiveType::Cuboid; }

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt) const override { return Contains_ObjectSpace(pt, static_cast<T>(0)); }

	virtual bool Contains_ObjectSpace(const Vector3<T>& pt, T fTolerance) const override {
		return !IsDegenerated() &&
			   pt.x >= m_min.x - fTolerance && pt.x <= m_max.x + fTolerance &&
			   pt.y >= m_min.y - fTolerance && pt.y <= m_max.y + fTolerance &&
			   pt.z >= m_min.z - fTolerance && pt.z <= m_max.z + fTolerance;
	}

	virtual void GenerateVisualMesh(TriList& triangleList) const override {
		// Winding order: CW
		PrimitiveMesh<T>::AddHexahedron(
			{ Vector3<T>(m_min.x, m_min.y, m_min.z), -Vector3<T>::Basis(2) },
			{ Vector3<T>(m_min.x, m_max.y, m_min.z), -Vector3<T>::Basis(2) },
			{ Vector3<T>(m_max.x, m_max.y, m_min.z), -Vector3<T>::Basis(2) },
			{ Vector3<T>(m_max.x, m_min.y, m_min.z), -Vector3<T>::Basis(2) },
			{ Vector3<T>(m_min.x, m_min.y, m_max.z), Vector3<T>::Basis(2) },
			{ Vector3<T>(m_min.x, m_max.y, m_max.z), Vector3<T>::Basis(2) },
			{ Vector3<T>(m_max.x, m_max.y, m_max.z), Vector3<T>::Basis(2) },
			{ Vector3<T>(m_max.x, m_min.y, m_max.z), Vector3<T>::Basis(2) },
			triangleList);
	}

	Vector3<T> GetMin() const {
		return m_min;
	}

	Vector3<T> GetMax() const {
		return m_max;
	}

private:
	Vector3<T> m_min;
	Vector3<T> m_max;
};

} // namespace KEP
