///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Vector.h"

namespace KEP {

class Simd4f;
class Simd2d;
//class Simd16i8;
class Simd16u8;
class Simd8i16;
//class Simd8u16;
class Simd4i32;
//class Simd4u32;
//class Simd2i64;
class Simd2u64;

template <typename T>
struct SimdTraits {
	static constexpr bool is_simd = false;
};

template <typename T>
struct SimdFloatingPointTraits {
	typedef T scalar_type;
	static constexpr bool is_simd = true;
	static constexpr bool is_integer = false;
};

template <typename T>
struct SimdIntegerTraits {
	typedef T scalar_type;
	static constexpr bool is_simd = true;
	static constexpr bool is_integer = true;
};
template <>
struct SimdTraits<Simd2d> : SimdFloatingPointTraits<double> {};
template <>
struct SimdTraits<Simd4f> : SimdFloatingPointTraits<float> {};
//template<> struct SimdTraits<Simd16i8> : SimdIntegerTraits<int8_t> {};
template <>
struct SimdTraits<Simd16u8> : SimdIntegerTraits<uint8_t> {};
template <>
struct SimdTraits<Simd8i16> : SimdIntegerTraits<int16_t> {};
//template<> struct SimdTraits<Simd8u16> : SimdIntegerTraits<uint16_t> {};
template <>
struct SimdTraits<Simd4i32> : SimdIntegerTraits<int32_t> {};
//template<> struct SimdTraits<Simd4u32> : SimdIntegerTraits<uint32_t> {};
//template<> struct SimdTraits<Simd2i64> : SimdIntegerTraits<int64_t> {};
template <>
struct SimdTraits<Simd2u64> : SimdIntegerTraits<uint64_t> {};

// Simd vector holding 4 floats
class Simd4f {
public:
	Simd4f() = default;
	Simd4f(const Simd4f&) = default;
	Simd4f(Simd4f&&) = default;
	explicit Simd4f(const Simd4i32& value);
	Simd4f(const __m128& values) :
			m_values(values) {}
	Simd4f(float x, float y, float z, float w) { Set(x, y, z, w); }
	explicit Simd4f(float value) { Set(value); } // Duplicates value into all fields of this
	explicit Simd4f(const Vector3f& values, float w) { Set(values, w); }
	explicit Simd4f(const Vector4f& values) { Set(values); }
	Simd4f& operator=(const Simd4f&) = default;
	Simd4f& operator=(Simd4f&&) = default;

	void Set(__m128 values) { m_values = values; }
	void Set(float x, float y, float z, float w) { this->Set(_mm_setr_ps(x, y, z, w)); }
	void Set(float value) { this->Set(_mm_set1_ps(value)); }
	const Vector4f& AsVector4f() const { return reinterpret_cast<const Vector4f&>(m_values); }
	Vector4f& AsVector4f() { return reinterpret_cast<Vector4f&>(m_values); }
	const Vector3f& AsVector3f() const { return reinterpret_cast<const Vector3f&>(m_values); }
	Vector3f& AsVector3f() { return reinterpret_cast<Vector3f&>(m_values); }
	explicit operator const Vector4f&() const { return AsVector4f(); }
	explicit operator Vector4f&() { return AsVector4f(); }
	void Set(const Vector4f& values) { this->Set(_mm_loadu_ps(values.RawData())); }
	void Set(const Vector3f& values, float w) { Set(values.X(), values.Y(), values.Z(), w); }
	operator const __m128&() const { return m_values; }
	operator __m128&() { return m_values; }

	template <size_t i0, size_t i1, size_t i2, size_t i3>
	inline Simd4f GetShuffled() const {
		static_assert(i0 < 4 && i1 < 4 && i2 < 4 && i3 < 4, "Index out of range");
		return _mm_shuffle_ps(*this, *this, i0 + (i1 << 2) + (i2 << 4) + (i3 << 6));
	}

	Simd16u8 PackSatu8() const;
	Simd4i32 TruncatedI32() const;

	Simd4f Dot(const Simd4f& other) const;
	Simd4f Dot3(const Simd4f& other) const;
	Simd4f GetNormalized() const;
	Simd4f GetNormalized3() const;
	Simd4f Normalize();
	Simd4f Normalize3();
	Simd4f Length() const;
	Simd4f Length3() const;
	Simd4f Length_Approx() const;
	Simd4f Length3_Approx() const;
	Simd4f LengthSquared() const;
	Simd4f LengthSquared3() const;

	float GetScalar() const { return _mm_cvtss_f32(m_values); }
	uint32_t BooleanMask() const { return _mm_movemask_ps(m_values); } // Extracts high bit of each 32 bit sub-part. Results in 4 bit mask.

	bool AllTrue() const { return BooleanMask() == 0xf; }
	bool AnyTrue() const { return BooleanMask() != 0; }
	bool NoneTrue() const { return BooleanMask() == 0; }

private:
	__m128 m_values;
};

// Simd vector holding 2 doubles
class Simd2d {
public:
	Simd2d() = default;
	Simd2d(const Simd2d&) = default;
	Simd2d(Simd2d&&) = default;
	//explicit Simd2d(const Simd2i64& value);
	Simd2d(const __m128d& values) :
			m_values(values) {}
	Simd2d(double x, double y) { Set(x, y); }
	explicit Simd2d(double value) { Set(value); } // Duplicates value into all fields of this
	explicit Simd2d(const Vector2d& values) { Set(values); }
	Simd2d& operator=(const Simd2d&) = default;
	Simd2d& operator=(Simd2d&&) = default;

	void Set(__m128d values) { m_values = values; }
	void Set(double x, double y) { this->Set(_mm_setr_pd(x, y)); }
	void Set(double value) { this->Set(_mm_set1_pd(value)); }
	const Vector2d& AsVector2d() const { return reinterpret_cast<const Vector2d&>(m_values); }
	Vector2d& AsVector2d() { return reinterpret_cast<Vector2d&>(m_values); }
	operator const __m128d&() const { return m_values; }
	void Set(const Vector2d& values) { this->Set(_mm_loadu_pd(values.RawData())); }

	double GetScalar() const { return _mm_cvtsd_f64(m_values); }
	uint32_t BooleanMask() const { return _mm_movemask_pd(m_values); } // Extracts high bit of each 64 bit sub-part. Results in 2 bit mask.

	bool AllTrue() const { return BooleanMask() == 0x3; }
	bool AnyTrue() const { return BooleanMask() != 0; }
	bool NoneTrue() const { return BooleanMask() == 0; }

private:
	__m128d m_values;
};

class Simdi128 {
public:
	Simdi128() = default;
	Simdi128(const Simdi128&) = default;
	Simdi128(Simdi128&&) = default;
	Simdi128(__m128i values) :
			m_values(values) {}
	Simdi128& operator=(const Simdi128&) = default;
	Simdi128& operator=(Simdi128&&) = default;

	void Set(__m128i values) { m_values = values; }
	const __m128i& GetValues() const { return m_values; }
	operator const __m128i&() const { return m_values; }
	operator __m128i&() { return m_values; }

	int32_t GetLow32Bits() const { return _mm_cvtsi128_si32(*this); }
	uint32_t BooleanMask8() const { return _mm_movemask_epi8(m_values); } // Extracts high bit of each 8 bit sub-part. Results in 16 bit mask.

	bool AllTrue() const { return BooleanMask8() == 0xffff; }
	bool AnyTrue() const { return !NoneTrue(); }
	bool AnyFalse() const { return !AllTrue(); }
	bool NoneTrue() const { return BooleanMask8() == 0; }

private:
	__m128i m_values;
};

// Simd vector holding 16 uint8_ts
class Simd16u8 : public Simdi128 {
public:
	Simd16u8() = default;
	Simd16u8(const Simd16u8&) = default;
	Simd16u8(Simd16u8&&) = default;
	explicit Simd16u8(__m128i values) :
			Simdi128(values) {}
	Simd16u8(uint8_t i0, uint8_t i1, uint8_t i2, uint8_t i3, uint8_t i4, uint8_t i5, uint8_t i6, uint8_t i7, uint8_t i8, uint8_t i9, uint8_t i10, uint8_t i11, uint8_t i12, uint8_t i13, uint8_t i14, uint8_t i15) { Set(i0, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15); }
	explicit Simd16u8(uint8_t value) { Set(value); } // Duplicates value into all fields of this
	Simd16u8& operator=(const Simd16u8&) = default;
	Simd16u8& operator=(Simd16u8&&) = default;

	using Simdi128::Set;
	void Set(uint8_t i0, uint8_t i1, uint8_t i2, uint8_t i3, uint8_t i4, uint8_t i5, uint8_t i6, uint8_t i7, uint8_t i8, uint8_t i9, uint8_t i10, uint8_t i11, uint8_t i12, uint8_t i13, uint8_t i14, uint8_t i15) { this->Set(_mm_setr_epi8(i0, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15)); }
	void Set(uint8_t value) { this->Set(_mm_set1_epi8(value)); }

	// Return first scalar value in this object (element X, element at lowest address when stored in memory)
	uint8_t GetScalar() const { return GetLow32Bits() & 0xff; }
};

// Simd vector holding 8 uint16_ts
class Simd8i16 : public Simdi128 {
public:
	Simd8i16() = default;
	Simd8i16(const Simd8i16&) = default;
	Simd8i16(Simd8i16&&) = default;
	explicit Simd8i16(__m128i values) :
			Simdi128(values) {}
	Simd8i16(int16_t i0, int16_t i1, int16_t i2, int16_t i3, int16_t i4, int16_t i5, int16_t i6, int16_t i7) { Set(i0, i1, i2, i3, i4, i5, i6, i7); }
	explicit Simd8i16(int16_t value) { Set(value); } // Duplicates value into all fields of this
	Simd8i16& operator=(const Simd8i16&) = default;
	Simd8i16& operator=(Simd8i16&&) = default;

	using Simdi128::Set;
	void Set(int16_t i0, int16_t i1, int16_t i2, int16_t i3, int16_t i4, int16_t i5, int16_t i6, int16_t i7) { this->Set(_mm_setr_epi16(i0, i1, i2, i3, i4, i5, i6, i7)); }
	void Set(int16_t value) { this->Set(_mm_set1_epi16(value)); }

	// Signed 16 to unsigned 8 bit integer conversion with saturation.
	// Negative numbers map to 0. Numbers greater than 255 map to 255.
	// Upper and lower halfs of output are identical.
	Simd16u8 PackSatu8() const { return Simd16u8(_mm_packus_epi16(*this, *this)); }

	// Return first scalar value in this object (element X, element at lowest address when stored in memory)
	int16_t GetScalar() const { return GetLow32Bits() & 0xffff; }
};

// Simd vector holding 4 uint32_ts
class Simd4i32 : public Simdi128 {
public:
	Simd4i32() = default;
	Simd4i32(const Simd4i32&) = default;
	Simd4i32(Simd4i32&&) = default;
	explicit Simd4i32(__m128i values) :
			Simdi128(values) {}
	Simd4i32(int32_t x, int32_t y, int32_t z, int32_t w) { Set(x, y, z, w); }
	explicit Simd4i32(const Simd4f& v) :
			Simdi128(_mm_cvtps_epi32(v)) {} // If an i32 is too small to hold any of the floats, 0x80000000 will be stored in that int.
	explicit Simd4i32(int32_t value) { Set(value); } // Duplicates value into all fields of this
	explicit Simd4i32(const Vector3i& values, int32_t w) { Set(values.X(), values.Y(), values.Z(), w); }
	explicit Simd4i32(const Vector4i& values) { Set(values); }
	Simd4i32& operator=(const Simd4i32&) = default;
	Simd4i32& operator=(Simd4i32&&) = default;

	using Simdi128::Set;
	void Set(int32_t x, int32_t y, int32_t z, int32_t w) { this->Set(_mm_setr_epi32(x, y, z, w)); }
	void Set(int32_t value) { this->Set(_mm_set1_epi32(value)); }
	const Vector4i& AsVector4i() const { return reinterpret_cast<const Vector4i&>(GetValues()); }
	Vector4i& AsVector4i() { return reinterpret_cast<Vector4i&>(*this); }
	void Set(const Vector4i& values) { this->Set(_mm_loadu_si128(reinterpret_cast<const __m128i*>(values.RawData()))); }

	// Signed 32 to signed 16 bit integer conversion with saturation.
	// Upper and lower halfs of output are identical.
	Simd8i16 PackSati16() const { return Simd8i16(_mm_packs_epi32(*this, *this)); }

	// Signed 32 to unsigned 8 bit integer conversion with saturation.
	// This will hold 4 copies of the same packed value.
	Simd16u8 PackSatu8() const { return PackSati16().PackSatu8(); }

	// Return first scalar value in this object (element X, element at lowest address when stored in memory)
	int32_t GetScalar() const { return GetLow32Bits(); }
	template <size_t i0, size_t i1, size_t i2, size_t i3>
	Simd4i32 GetShuffled() const {
		static_assert(i0 < 4 && i1 < 4 && i2 < 4 && i3 < 4, "Index out of range");
		return Simd4i32(_mm_shuffle_epi32(*this, i0 + (i1 << 2) + (i2 << 4) + (i3 << 6)));
	}
	template <size_t i0>
	int32_t AtIndex() const { return GetShuffled<i0, i0, i0, i0>().AtIndex<0>(); }
	template <>
	int32_t AtIndex<0>() const { return GetScalar(); }
};

// Simd vector holding 2 uint64_ts
class Simd2u64 : public Simdi128 {
public:
	Simd2u64() = default;
	Simd2u64(const Simd2u64&) = default;
	Simd2u64(Simd2u64&&) = default;
	explicit Simd2u64(__m128i values) :
			Simdi128(values) {}
	Simd2u64(int32_t x, int32_t y) { Set(x, y); }
	explicit Simd2u64(uint64_t value) { Set(value); } // Duplicates value into all fields of this
	explicit Simd2u64(const Vector2<uint64_t>& values) { Set(values); }
	Simd2u64& operator=(const Simd2u64&) = default;
	Simd2u64& operator=(Simd2u64&&) = default;

	using Simdi128::Set;
	void Set(uint64_t x, uint64_t y) { this->Set(_mm_setr_epi64x(x, y)); }
	void Set(uint64_t value) { this->Set(_mm_set1_epi64x(value)); }
	explicit operator Vector2<uint64_t>() const { return reinterpret_cast<const Vector2<uint64_t>&>(GetValues()); }
	void Set(const Vector2<uint64_t>& values) { this->Set(_mm_loadu_si128(reinterpret_cast<const __m128i*>(values.RawData()))); }
};

// Wrapper to do delayed evaluation to take advantage of the sse combined andnot operation.
struct NotSimdi128 {
	NotSimdi128(const Simdi128& i) :
			m_value(i) {}
	operator Simdi128() const { return operator __m128i(); }
	operator __m128i() const {
		static __m128i allbits = _mm_set1_epi32(0xffffffff);
		return _mm_xor_si128(allbits, m_value);
	}

private:
	__m128i m_value;
};

// This wraps the result of the bitwise NOT operator (~) so that we
// can optimize by using the and-not intrinsic where appropriate.
template <typename T>
struct TNotSimd {
	explicit TNotSimd(const T& value) :
			m_value(value) {}
	operator T() const { return NotValue(); }

	T NotValue() const { return T(Not(m_value)); }
	const T& BaseValue() const { return m_value; }

private:
	static __m128i Not(__m128i value) {
		__m128i allbits = _mm_set1_epi32(0xffffffff);
		return _mm_xor_si128(allbits, value);
	}
	static __m128 Not(__m128 value) {
		return _mm_castsi128_ps(Not(_mm_castps_si128(value)));
	}
	static __m128d Not(__m128d value) {
		return _mm_castsi128_pd(Not(_mm_castpd_si128(value)));
	}

private:
	T m_value;
};

template <size_t i0, size_t i1, size_t i2, size_t i3>
inline Simd4f Shuffle(const Simd4f& value) {
	return value.GetShuffled<i0, i1, i2, i3>();
}

template <size_t i0, size_t i1, size_t i2, size_t i3>
inline Simd4i32 Shuffle(const Simd4i32& value) {
	return value.GetShuffled<i0, i1, i2, i3>();
}

template <size_t i0, size_t i1, size_t i2, size_t i3>
inline Simd4f Shuffle(const Simd4f& lowSource, const Simd4f& highSource) {
	return _mm_shuffle_ps(lowSource, highSource, i0 + (i1 << 2) + (i2 << 4) + (i3 << 6));
}

template <size_t i0, size_t i1>
inline Simd4f Shuffle(const Simd2d& lowSource, const Simd2d& highSource) {
	return _mm_shuffle_pd(lowSource, highSource, i0 + (i1 << 2));
}

inline Simd4f MergeLowHalves(const Simd4f& lowHalfSource, const Simd4f& highHalfSource) {
	return _mm_movelh_ps(lowHalfSource, highHalfSource);
}

inline Simd4f MergeHighHalves(const Simd4f& lowHalfSource, const Simd4f& highHalfSource) {
	return _mm_movehl_ps(highHalfSource, lowHalfSource);
}

inline Simd2d operator==(const Simd2d& left, const Simd2d& right) {
	return _mm_cmpeq_pd(left, right);
}

inline Simd4f operator==(const Simd4f& left, const Simd4f& right) {
	return _mm_cmpeq_ps(left, right);
}

inline Simd4i32 operator==(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_cmpeq_epi32(left, right));
}

inline Simd8i16 operator==(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_cmpeq_epi16(left, right));
}

inline Simd16u8 operator==(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_cmpeq_epi8(left, right));
}

inline Simd2d operator!=(const Simd2d& left, const Simd2d& right) {
	return _mm_cmpneq_pd(left, right);
}

inline Simd4f operator!=(const Simd4f& left, const Simd4f& right) {
	return _mm_cmpneq_ps(left, right);
}

// No SSE instruction available.
// Use operator== and invert it.
//inline Simd4i32 operator!=(const Simd4i32& left, const Simd4i32& right) {}
//inline Simd8i16 operator!=(const Simd8i16& left, const Simd8i16& right) {}

inline Simd4f operator<(const Simd4f& left, const Simd4f& right) {
	return _mm_cmplt_ps(left, right);
}

inline Simd2d operator<(const Simd2d& left, const Simd2d& right) {
	return _mm_cmplt_pd(left, right);
}

inline Simd4i32 operator<(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_cmplt_epi32(left, right));
}

inline Simd8i16 operator<(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_cmplt_epi16(left, right));
}

// No SSE instruction available.
//inline Simd16u8 operator<(const Simd16u8& left, const Simd16u8& right) {
//	return Simd16u8(_mm_cmplt_epu8(left, right));
//}

inline Simd2d operator<=(const Simd2d& left, const Simd2d& right) {
	return _mm_cmple_pd(left, right);
}

inline Simd4f operator<=(const Simd4f& left, const Simd4f& right) {
	return _mm_cmple_ps(left, right);
}

// No SSE instruction available.
//inline Simd4i32 operator<=(const Simd4i32& left, const Simd4i32& right) {}
//inline Simd8i16 operator<=(const Simd8i16& left, const Simd8i16& right) {}
//inline Simd16u8 operator<=(const Simd16u8& left, const Simd16u8& right) {}

inline Simd2d operator>(const Simd2d& left, const Simd2d& right) {
	return _mm_cmpgt_pd(left, right);
}

inline Simd4f operator>(const Simd4f& left, const Simd4f& right) {
	return _mm_cmpgt_ps(left, right);
}

inline Simd4i32 operator>(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_cmpgt_epi32(left, right));
}

inline Simd8i16 operator>(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_cmpgt_epi16(left, right));
}

// No SSE instruction available.
//inline Simd16u8 operator>(const Simd16u8& left, const Simd16u8& right) {
//	return Simd16u8(_mm_cmpgt_epu8(left, right));
//}

inline Simd2d operator>=(const Simd2d& left, const Simd2d& right) {
	return _mm_cmpge_pd(left, right);
}

inline Simd4f operator>=(const Simd4f& left, const Simd4f& right) {
	return _mm_cmpge_ps(left, right);
}

// No SSE instruction available.
//inline Simd4i32 operator>=(const Simd4i32& left, const Simd4i32& right) {}
//inline Simd8i16 operator>=(const Simd8i16& left, const Simd8i16& right) {}
//inline Simd16u8 operator>=(const Simd16u8& left, const Simd16u8& right) {}

inline Simd2d operator&(const Simd2d& left, const Simd2d& right) {
	return _mm_and_pd(left, right);
}

inline Simd4f operator&(const Simd4f& left, const Simd4f& right) {
	return _mm_and_ps(left, right);
}

inline Simd4i32 operator&(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_and_si128(left, right));
}

inline Simd8i16 operator&(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_and_si128(left, right));
}

inline Simd16u8 operator&(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_and_si128(left, right));
}

inline Simd2d operator|(const Simd2d& left, const Simd2d& right) {
	return _mm_or_pd(left, right);
}

inline Simd4f operator|(const Simd4f& left, const Simd4f& right) {
	return _mm_or_ps(left, right);
}

inline Simd4i32 operator|(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_or_si128(left, right));
}

inline Simd8i16 operator|(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_or_si128(left, right));
}

inline Simd16u8 operator|(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_or_si128(left, right));
}

inline Simd2d operator^(const Simd2d& left, const Simd2d& right) {
	return _mm_xor_pd(left, right);
}

inline Simd4f operator^(const Simd4f& left, const Simd4f& right) {
	return _mm_xor_ps(left, right);
}

inline Simd4i32 operator^(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_xor_si128(left, right));
}

inline Simd8i16 operator^(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_xor_si128(left, right));
}

inline Simd16u8 operator^(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_xor_si128(left, right));
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline TNotSimd<T> operator~(const T& value) {
	return TNotSimd<T>(value);
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_integer, nullptr_t> = nullptr>
inline T SimdAndNot(const T& left, const T& right) {
	return T(_mm_andnot_si128(right, left));
}

inline Simd2d SimdAndNot(const Simd2d& left, const Simd2d& right) {
	return Simd2d(_mm_andnot_pd(right, left));
}

inline Simd4f SimdAndNot(const Simd4f& left, const Simd4f& right) {
	return Simd4f(_mm_andnot_ps(right, left));
}

template <typename T>
inline T operator&(const T& left, const TNotSimd<T>& right) {
	return SimdAndNot(left, right.BaseValue());
}

template <typename T>
inline T operator&(const TNotSimd<T>& left, const T& right) {
	return SimdAndNot(right, left.BaseValue());
}

template <typename T>
inline TNotSimd<T> operator&(const TNotSimd<T>& left, const TNotSimd<T>& right) {
	return TNotSimd<T>(left.BaseValue() | right.BaseValue());
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T operator!(const T& value) {
	return value == T(0);
}

inline Simd4i32 operator<<(const Simd4i32& value, uint32_t shift) {
	return Simd4i32(_mm_slli_epi32(value, shift));
}

inline Simd8i16 operator<<(const Simd8i16& value, uint32_t shift) {
	return Simd8i16(_mm_slli_epi16(value, shift));
}

// Shift as 16 bit values, then apply a mask to clear the bits that should have zero shifted in.
inline Simd16u8 operator<<(const Simd16u8& value, uint32_t shift) {
#if 1
	// Use uint32_t to initialize memory so that the compiler does not
	// do initialization at runtime.
	static constexpr uint32_t table_u32 alignas(16)[16 * 4] = {
		0xffffffff,
		0xffffffff,
		0xffffffff,
		0xffffffff,
		0xfefefefe,
		0xfefefefe,
		0xfefefefe,
		0xfefefefe,
		0xfcfcfcfc,
		0xfcfcfcfc,
		0xfcfcfcfc,
		0xfcfcfcfc,
		0xf8f8f8f8,
		0xf8f8f8f8,
		0xf8f8f8f8,
		0xf8f8f8f8,
		0xf0f0f0f0,
		0xf0f0f0f0,
		0xf0f0f0f0,
		0xf0f0f0f0,
		0xe0e0e0e0,
		0xe0e0e0e0,
		0xe0e0e0e0,
		0xe0e0e0e0,
		0xc0c0c0c0,
		0xc0c0c0c0,
		0xc0c0c0c0,
		0xc0c0c0c0,
		0x80808080,
		0x80808080,
		0x80808080,
		0x80808080,

		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
	};
	const __m128i* table128 = reinterpret_cast<const __m128i*>(table_u32);
	Simd16u8 shifted128(_mm_slli_epi16(value, shift));
	Simd16u8 mask(table128[shift & 0xf]);
	return shifted128 & mask;
#else
	Simd8i16 low_byte_mask(0x00ff);
	Simd8i16 high_byte_mask(int16_t(0xff00));
	Simd16u8 mask((high_byte_mask << shift) | low_byte_mask);
	Simd16u8 shifted16(Simd8i16(value) << shift);
	return shifted16 & mask;
#endif
}

inline Simd4i32 operator>>(const Simd4i32& value, uint32_t shift) {
	return Simd4i32(_mm_srai_epi32(value, shift));
}

inline Simd8i16 operator>>(const Simd8i16& value, uint32_t shift) {
	return Simd8i16(_mm_srai_epi16(value, shift));
}

// No SSE instruction available.
// Shift as 16 bit values, then apply a mask to clear the bits that should have zero shifted in.
inline Simd16u8 operator>>(const Simd16u8& value, uint32_t shift) {
#if 1
	// Use uint32_t to initialize memory so that the compiler does not
	// do initialization at runtime.
	static constexpr uint32_t table_u32 alignas(16)[16 * 4] = {
		0xffffffff,
		0xffffffff,
		0xffffffff,
		0xffffffff,
		0x7f7f7f7f,
		0x7f7f7f7f,
		0x7f7f7f7f,
		0x7f7f7f7f,
		0x3f3f3f3f,
		0x3f3f3f3f,
		0x3f3f3f3f,
		0x3f3f3f3f,
		0x1f1f1f1f,
		0x1f1f1f1f,
		0x1f1f1f1f,
		0x1f1f1f1f,
		0x0f0f0f0f,
		0x0f0f0f0f,
		0x0f0f0f0f,
		0x0f0f0f0f,
		0x07070707,
		0x07070707,
		0x07070707,
		0x07070707,
		0x03030303,
		0x030c0303,
		0x03030303,
		0x03030303,
		0x01010101,
		0x01010101,
		0x01010101,
		0x01010101,

		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000,
	};
	const __m128i* table128 = reinterpret_cast<const __m128i*>(table_u32);
	Simd16u8 shifted128(_mm_srli_epi16(value, shift));
	Simd16u8 mask(table128[shift & 0xf]);
	return shifted128 & mask;
#else
	Simd8i16 low_byte_mask(0x00ff);
	Simd8i16 high_byte_mask(int16_t(0xff00));
	Simd16u8 mask((low_byte_mask >> shift) | high_byte_mask);
	Simd16u8 shifted16(Simd8i16(value) >> shift);
	return shifted16 & mask;
#endif
}

inline Simd2d operator+(const Simd2d& left, const Simd2d& right) {
	return _mm_add_pd(left, right);
}

inline Simd4f operator+(const Simd4f& left, const Simd4f& right) {
	return _mm_add_ps(left, right);
}

inline Simd4i32 operator+(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_add_epi32(left, right));
}

inline Simd8i16 operator+(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_add_epi16(left, right));
}

inline Simd16u8 operator+(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_add_epi8(left, right));
}

inline Simd2d operator-(const Simd2d& left, const Simd2d& right) {
	return Simd2d(_mm_sub_pd(left, right));
}

inline Simd4f operator-(const Simd4f& left, const Simd4f& right) {
	return Simd4f(_mm_sub_ps(left, right));
}

inline Simd4i32 operator-(const Simd4i32& left, const Simd4i32& right) {
	return Simd4i32(_mm_sub_epi32(left, right));
}

inline Simd8i16 operator-(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_sub_epi16(left, right));
}

inline Simd16u8 operator-(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_sub_epi8(left, right));
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T operator+(const T& value) {
	return value;
}

inline Simd2d operator-(const Simd2d& value) {
	Simd2d highbit(-0.0);
	return value ^ highbit;
}

inline Simd4f operator-(const Simd4f& value) {
	Simd4f highbit(-0.0f);
	return value ^ highbit;
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_integer, nullptr_t> = nullptr>
inline T operator-(const T& value) {
	return T(0) - value;
}

inline Simd2d operator*(const Simd2d& left, const Simd2d& right) {
	return Simd2d(_mm_mul_pd(left, right));
}

inline Simd4f operator*(const Simd4f& left, const Simd4f& right) {
	return Simd4f(_mm_mul_ps(left, right));
}

// No SSE instruction available.
// Using two expanding 32x32->64 multiplies, then merging.
// If you can stand the precision loss, conversion to Simd4f may be faster.
inline Simd4i32 operator*(const Simd4i32& left, const Simd4i32& right) {
	Simd2u64 mul02_64(_mm_mul_epu32(left, right));
	Simd4i32 leftH(_mm_shuffle_epi32(left, 0xf5));
	Simd4i32 rightH(_mm_shuffle_epi32(right, 0xf5));
	Simd2u64 mul13_64(_mm_mul_epu32(leftH, rightH));
	Simd4i32 mul02_32(_mm_shuffle_epi32(mul02_64, 0x88));
	Simd4i32 mul13_32(_mm_shuffle_epi32(mul13_64, 0x88));
	return Simd4i32(_mm_unpacklo_epi32(mul02_32, mul13_32));
}

inline Simd8i16 operator*(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_mullo_epi16(left, right));
}

// No SSE instruction. Convert to Simd8i16, do two multiplies, then convert back.
inline Simd16u8 operator*(const Simd16u8& left, const Simd16u8& right) {
	Simd8i16 leftLow(_mm_unpacklo_epi8(left, _mm_setzero_si128()));
	Simd8i16 rightLow(_mm_unpacklo_epi8(right, _mm_setzero_si128()));
	Simd8i16 productLow = leftLow * rightLow;
	Simd8i16 leftHigh(_mm_unpackhi_epi8(left, _mm_setzero_si128()));
	Simd8i16 rightHigh(_mm_unpackhi_epi8(right, _mm_setzero_si128()));
	Simd8i16 productHigh = leftHigh * rightHigh;
	return Simd16u8(_mm_packus_epi16(productLow, productHigh));
}

inline Simd2d operator/(const Simd2d& left, const Simd2d& right) {
	return Simd2d(_mm_div_pd(left, right));
}

inline Simd4f operator/(const Simd4f& left, const Simd4f& right) {
	return Simd4f(_mm_div_ps(left, right));
}

// No SSE instruction available.
//inline Simd4i32 operator/(const Simd4i32& left, const Simd4i32& right) {}
//inline Simd8i16 operator/(const Simd8i16& left, const Simd8i16& right) {}
//inline Simd16u8 operator/(const Simd16u8& left, const Simd16u8& right) {}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T& operator&=(T& left, const std::common_type_t<T>& right) {
	return left = left & right;
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T& operator&=(T& left, const TNotSimd<T>& right) {
	return left = left & right;
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T& operator|=(T& left, const std::common_type_t<T>& right) {
	return left = left | right;
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T& operator^=(T& left, const std::common_type_t<T>& right) {
	return left = left ^ right;
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T& operator+=(T& left, const std::common_type_t<T>& right) {
	return left = left + right;
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T& operator-=(T& left, const std::common_type_t<T>& right) {
	return left = left - right;
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T& operator*=(T& left, const std::common_type_t<T>& right) {
	return left = left * right;
}

template <typename T, std::enable_if_t<!SimdTraits<T>::is_integer, nullptr_t> = nullptr>
inline T& operator/=(T& left, const std::common_type_t<T>& right) {
	return left = left * right;
}

inline Simd4i32 Min(const Simd4i32& left, const Simd4i32& right) {
	Simd4i32 less = left < right;
	return (right & ~less) | (left & less);

	// Requires SSE4.1
	//return _mm_min_epi32(left, right);
}

inline Simd2d Min(const Simd2d& left, const Simd2d& right) {
	return _mm_min_pd(left, right);
}

inline Simd4f Min(const Simd4f& left, const Simd4f& right) {
	return _mm_min_ps(left, right);
}

inline Simd8i16 Min(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_min_epi16(left, right));
}

inline Simd16u8 Min(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_min_epu8(left, right));
}

inline Simd2d Max(const Simd2d& left, const Simd2d& right) {
	return _mm_max_pd(left, right);
}

inline Simd4f Max(const Simd4f& left, const Simd4f& right) {
	return _mm_max_ps(left, right);
}

inline Simd4i32 Max(const Simd4i32& left, const Simd4i32& right) {
	Simd4i32 less = left < right;
	return (right & less) | (left & ~less);

	// Requires SSE4.1
	//return _mm_max_epi32(left, right);
}

inline Simd8i16 Max(const Simd8i16& left, const Simd8i16& right) {
	return Simd8i16(_mm_max_epi16(left, right));
}

inline Simd16u8 Max(const Simd16u8& left, const Simd16u8& right) {
	return Simd16u8(_mm_max_epu8(left, right));
}

inline Simd2d Abs(const Simd2d& value) {
	Simd2d highbit(-0.0);
	return value & ~highbit;
}

inline Simd4f Abs(const Simd4f& value) {
	Simd4f highbit(-0.0f);
	return value & ~highbit;
}

inline Simd4i32 Abs(const Simd4i32& value) {
	Simd4i32 sign = value < Simd4i32(0);
	return (value ^ sign) - sign;

	// _mm_abs_epi32 not available until SSSE3
	//return Simd4i32(_mm_abs_epi32(value));
}

inline Simd8i16 Abs(const Simd8i16& value) {
	return Max(value, Simd8i16(0) - value);

	// _mm_abs_epi16 not available until SSSE3
	//return Simd8i16(_mm_abs_epi16(value));
}

template <size_t nBytes, typename T, std::enable_if_t<SimdTraits<T>::is_integer, nullptr_t> = nullptr>
inline T ShiftLeftBytes(const T& value) {
	return T(_mm_slli_si128(value, nBytes));
}

template <size_t nBytes, typename T, std::enable_if_t<SimdTraits<T>::is_integer, nullptr_t> = nullptr>
inline T ShiftRightBytes(const T& value) {
	return T(_mm_srli_si128(value, nBytes));
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T IfThenElse(const T& boolean, const T& if_true, const T& if_false) {
	return (if_true & boolean) | (if_false & ~boolean);
}

template <typename T, std::enable_if_t<SimdTraits<T>::is_simd, nullptr_t> = nullptr>
inline T IfThenElse(const TNotSimd<T>& boolean, const T& if_true, const T& if_false) {
	return (if_true & boolean) | (if_false & ~boolean);
}

template <typename To, std::enable_if_t<std::is_same<To, Simd4f>::value, nullptr_t> = nullptr>
inline Simd4f ReinterpretCast(const Simdi128& from) {
	return To(_mm_castsi128_ps(from));
}

template <typename To, std::enable_if_t<std::is_same<To, Simd2d>::value, nullptr_t> = nullptr>
inline Simd2d ReinterpretCast(const Simdi128& from) {
	return To(_mm_castsi128_pd(from));
}

template <typename To, std::enable_if_t<SimdTraits<To>::is_integer, nullptr_t> = nullptr>
inline To ReinterpretCast(const Simd4f& from) {
	return To(_mm_castps_si128(from));
}

template <typename To, std::enable_if_t<std::is_same<To, Simd2d>::value, nullptr_t> = nullptr>
inline Simd2d ReinterpretCast(const Simd4f& from) {
	return To(_mm_castps_pd(from));
}

template <typename To, std::enable_if_t<SimdTraits<To>::is_integer, nullptr_t> = nullptr>
inline To ReinterpretCast(const Simd2d& from) {
	return To(_mm_castpd_si128(from));
}

template <typename To, std::enable_if_t<std::is_same<To, Simd4f>::value, nullptr_t> = nullptr>
inline Simd4f ReinterpretCast(const Simd2d& from) {
	return To(_mm_castpd_ps(from));
}

inline Simd4f ReciprocalSqrt_Approx(const Simd4f& value) {
	Simd4f guess = _mm_rsqrt_ps(value); // Relative error is 1.5 * 2^-12 for Intel processors. AMD between 11-14 bits.
	return guess;
}

inline Simd4f ReciprocalSqrt_Close(const Simd4f& value) {
	Simd4f guess = ReciprocalSqrt_Approx(value);

	// Do one iteration of Newton-Raphson.
	// This approximately doubles the precision, getting us very close to 24 bits.
	// x_new = x * (1.5 - 0.5 * t * (x*x))
	/*
	Simd4f half_value = Simd4f(0.5f) * value;
	Simd4f one_and_half = Simd4f(1.5f);
	guess = guess * (one_and_half - half_value * (guess * guess));
	*/
	Simd4f three = Simd4f(3.0f);
	Simd4f half = Simd4f(0.5f);
	guess = (half * guess) * (three - value * (guess * guess));

	return guess;
}

inline Simd4f ReciprocalSqrt_Exact(const Simd4f& value) {
	Simd4f guess = ReciprocalSqrt_Approx(value);

	// Do two iterations of Newton-Raphson.
	Simd4f three = Simd4f(3.0f);
	Simd4f half = Simd4f(0.5f);
	guess = (half * guess) * (three - value * (guess * guess));
	guess = (half * guess) * (three - value * (guess * guess));

	return guess;
}

inline Simd4f ReciprocalSqrt(const Simd4f& value) {
	return ReciprocalSqrt_Close(value);
}

inline Simd4f Reciprocal_Approx(const Simd4f& value) {
	return _mm_rcp_ps(value);
}

inline Simd4f Reciprocal_Close(const Simd4f& value) {
	Simd4f guess = Reciprocal_Approx(value);

	// Do one iteration of Newton-Raphson.
	// This approximately doubles the precision, giving us a very close reciprocal
	// x_new = x * (2.0 - t * x)
	Simd4f two(2.0f);
	guess = guess * (two - value * guess);
	return guess;
}

inline Simd4f Reciprocal_Exact(const Simd4f& value) {
	Simd4f guess = Reciprocal_Approx(value);

	// Do two iterations of Newton-Raphson.
	Simd4f two(2.0f);
	guess = guess * (two - value * guess);
	guess = guess * (two - value * guess);
	return guess;
}

inline Simd4f Reciprocal(const Simd4f& value) {
	return Reciprocal_Close(value);
}

inline Simd4f SumElements(const Simd4f& value) {
	Simd4f tmpA = value + Shuffle<1, 0, 3, 2>(value);
	Simd4f tmpB = Shuffle<2, 3, 0, 1>(tmpA) + tmpA;
	return tmpB;
}

inline Simd4f SumElements3(const Simd4f& value) {
	Simd4f mask = ReinterpretCast<Simd4f>(Simd4i32(0xffffffff, 0xffffffff, 0xffffffff, 0));
	Simd4f masked = value & mask;
	return SumElements(masked);
}

inline Simd4f Dot(const Simd4f& A, const Simd4f& B) {
	return SumElements(A * B);
}

inline Simd4f Dot3(const Simd4f& A, const Simd4f& B) {
	return SumElements3(A * B);
}

inline Simd4f Normalize(const Simd4f& value) {
	Simd4f dot = Dot(value, value);
	return value * ReciprocalSqrt(dot);
}

inline Simd4f Normalize3(const Simd4f& value) {
	Simd4f dot = Dot3(value, value);
	return value * ReciprocalSqrt(dot);
}

inline Simd4f Normalize_Approx(const Simd4f& value) {
	Simd4f dot = Dot(value, value);
	return value * ReciprocalSqrt_Approx(dot);
}

inline Simd4f Normalize3_Approx(const Simd4f& value) {
	Simd4f dot = Dot3(value, value);
	return value * ReciprocalSqrt_Approx(dot);
}

inline Simd4f::Simd4f(const Simd4i32& value) :
		m_values(_mm_cvtepi32_ps(value)) {
}

inline Simd4f Simd4f::Dot(const Simd4f& other) const {
	return ::KEP::Dot(*this, other);
}

inline Simd4f Simd4f::Dot3(const Simd4f& other) const {
	return ::KEP::Dot3(*this, other);
}

inline Simd4f Simd4f::GetNormalized() const {
	return ::KEP::Normalize(*this);
}

inline Simd4f Simd4f::GetNormalized3() const {
	return ::KEP::Normalize3(*this);
}

inline Simd4f Simd4f::Normalize() {
	*this = ::KEP::Normalize(*this);
}

inline Simd4f Simd4f::Normalize3() {
	*this = ::KEP::Normalize3(*this);
}

inline Simd4f Simd4f::LengthSquared() const {
	Simd4f dot = this->Dot(*this);
	return dot;
}

inline Simd4f Simd4f::LengthSquared3() const {
	Simd4f dot = this->Dot3(*this);
	return dot;
}

inline Simd4f Simd4f::Length() const {
	Simd4f dot = this->Dot(*this);
	return dot * ReciprocalSqrt(dot);
}

inline Simd4f Simd4f::Length3() const {
	Simd4f dot = this->Dot3(*this);
	return dot * ReciprocalSqrt(dot);
}

inline Simd4f Simd4f::Length_Approx() const {
	Simd4f dot = this->Dot(*this);
	return dot * ReciprocalSqrt_Approx(dot);
}

inline Simd4f Simd4f::Length3_Approx() const {
	Simd4f dot = this->Dot3(*this);
	return dot * ReciprocalSqrt_Approx(dot);
}

// float to unsigned 8 bit integer conversion with saturation.
// WARNING: If any float value does not fit in a signed 32 bit integer, 0x00 will end up in the corresponding uint8_t field.
// The Simd16u8 will hold 4 copies of the same packed value.
inline Simd16u8 Simd4f::PackSatu8() const {
	return Simd4i32(*this).PackSatu8();
}

inline Simd4i32 Simd4f::TruncatedI32() const {
	return Simd4i32(_mm_cvttps_epi32(*this));
}

// Transposes 4 SIMD vectors
inline void Transpose(const Simd4f& x, const Simd4f& y, const Simd4f& z, const Simd4f& w, Out<Simd4f> v0, Out<Simd4f> v1, Out<Simd4f> v2, Out<Simd4f> v3) {
	Simd4f v23xzxz = _mm_unpackhi_ps(x, z);
	Simd4f v01xzxz = _mm_unpacklo_ps(x, z);
	Simd4f v23ywyw = _mm_unpackhi_ps(y, w);
	Simd4f v01ywyw = _mm_unpacklo_ps(y, w);
	v0.set(_mm_unpacklo_ps(v01xzxz, v01ywyw));
	v1.set(_mm_unpackhi_ps(v01xzxz, v01ywyw));
	v2.set(_mm_unpacklo_ps(v23xzxz, v23ywyw));
	v3.set(_mm_unpackhi_ps(v23xzxz, v23ywyw));
}

// Transposes 3 SIMD vectors each holding a single coordinate of a different vector into 4 XYZ SIMD vectors.
// The fourth coordinate of the output vectors will be given the value zero.
inline void Transpose(const Simd4f& x, const Simd4f& y, const Simd4f& z, Out<Simd4f> v0, Out<Simd4f> v1, Out<Simd4f> v2, Out<Simd4f> v3) {
	Transpose(x, y, z, Simd4f(0), v0, v1, v2, v3);
}

// Transposes 4 SIMD vectors holding X, Y, Z values (fourth value ignored) into 3 SIMD vectors, one holding all the X coordinates,
// one holding all of the Y components, and one holding all of the Z components.
inline void Transpose(const Simd4f& v0, const Simd4f& v1, const Simd4f& v2, const Simd4f& v3, Out<Simd4f> x, Out<Simd4f> y, Out<Simd4f> z) {
	Simd4f v02zzww = _mm_unpackhi_ps(v0, v2);
	Simd4f v02xxyy = _mm_unpacklo_ps(v0, v2);
	Simd4f v13zzww = _mm_unpackhi_ps(v1, v3);
	Simd4f v13xxyy = _mm_unpacklo_ps(v1, v3);
	x.set(_mm_unpacklo_ps(v02xxyy, v13xxyy));
	y.set(_mm_unpackhi_ps(v02xxyy, v13xxyy));
	z.set(_mm_unpacklo_ps(v02zzww, v13zzww));
}

} //namespace KEP
