///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
BOOL SetupCryptoClient();
BOOL EncryptAndEncodeString(const char *szPassword, std::string &encryptPwd, const char *szKey);
BOOL DecodeAndDecryptString(const char *szEncryptPwd, std::string &szPassword, const char *szKey);
BOOL EncryptBytes(const char *bytesToEncrypt, DWORD len, const char *szKey);
BOOL DecryptBytes(const char *bytesToDecrypt, DWORD len, const char *szKey);
