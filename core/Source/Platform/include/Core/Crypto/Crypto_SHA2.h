///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define SHA256_DIGEST_LEN 256 / 8

namespace KEP {

bool SHA256VerifyData(const unsigned char* dataBuffer, unsigned bufferLen, const unsigned char* digest, unsigned digestLen);

}
