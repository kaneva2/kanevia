///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

typedef uint64_t FileSize; // from KEPHelpers.h

namespace KEP {

#define MD5_DIGEST_LEN 16
typedef struct {
	unsigned char s[MD5_DIGEST_LEN];
} MD5_DIGEST, *LPMD5_DIGEST;

typedef struct {
	char s[MD5_DIGEST_LEN * 2 + 1];
} MD5_DIGEST_STR, *LPMD5_DIGEST_STR;

///---------------------------------------------------------
// hash a string, such as a password to an ASCII string
//
// [in] str string to hash
// [out] s hash, as a string
//
void hashString(const char* str, MD5_DIGEST_STR& s);

///---------------------------------------------------------
// hash a string, such as a password to a byte array
//
// [in] str string to hash
// [out] digest the hash
//
void hashString(const char* str, MD5_DIGEST& digest);

///---------------------------------------------------------
// hash a file to an ASCII string
//
// [in] fname filename to open and create hash
// [out] errMsg if returns false, an error message
// [out] digest the hash
// [in] loopSleep number of ms to sleep between reads, to slow down if in a background thd
//
// [Returns]
//		true if succeeded
bool hashFile(const char* fname, MD5_DIGEST_STR& s, ULONG loopSleep = 0);

///---------------------------------------------------------
// hash a file
//
// [in] fname filename to open and create hash
// [out] errMsg if returns false, an error message
// [out] digest the hash
// [in] loopSleep number of ms to sleep between reads, to slow down if in a background thd
//
// [Returns]
//		true if succeeded
bool hashFile(const char* fname, MD5_DIGEST& digest, ULONG loopSleep = 0);

inline std::string Md5File(const std::string& filePath) {
	MD5_DIGEST_STR md5;
	hashFile(filePath.c_str(), md5);
	return md5.s;
}

///---------------------------------------------------------
// hash a data buffer
//
// [in] data	data buffer to be hashed
// [in] len		number of bytes in data buffer
// [out] digest the hash
// [in] loopSleep number of ms to sleep between reads, to slow down if in a background thd
//
// [Returns]
//		true if succeeded
void hashData(const unsigned char* data, FileSize len, MD5_DIGEST_STR& s, ULONG loopSleep = 0);

///---------------------------------------------------------
// hash a data buffer
//
// [in] data	data buffer to be hashed
// [in] len		number of bytes in data buffer
// [out] digest the hash
// [in] loopSleep number of ms to sleep between reads, to slow down if in a background thd
//
// [Returns]
//		true if succeeded
void hashData(const unsigned char* data, FileSize len, MD5_DIGEST& digest, ULONG loopSleep = 0);

bool decodeHashString(const char* hashString, unsigned char* hashBytes);

///---------------------------------------------------------
// hash a data buffer and compare the result with the digest passed in
//
// [in] dataBuffer	data buffer to be hashed
// [in] bufferLen	number of bytes in data buffer
// [in] digest		expected MD5 digest
// [in] digestLen	number of bytes in digest
//
// [Returns]
//		true if succeeded
bool MD5VerifyData(const unsigned char* dataBuffer, FileSize bufferLen, const unsigned char* digest, size_t digestLen);

} // namespace KEP
