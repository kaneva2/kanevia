///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>

typedef uint64_t FileSize; // from KEPHelpers.h

namespace KEP {

BYTE* KRXEncrypt(BYTE* in_buf, FileSize len, const char* salt, const unsigned char* data_hash = NULL, int data_hash_len = 0);
BYTE* KRXDecrypt(const BYTE* in_buf, FileSize len, const char* salt, const unsigned char* data_hash, int data_hash_len);

} // namespace KEP
