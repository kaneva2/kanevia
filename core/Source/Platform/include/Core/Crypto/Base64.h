//  //
// ####   ###     ##                -= Base64 library =-                 //
// #   # #       # # Base64.h - Base64 encoder/decoder                   //
// ####  ####   #  #                                                     //
// #   # #   # ##### Encodes and decodes base64 strings                  //
// #   # #   #     # Ideas taken from work done by Bob Withers           //
// ####   ###      # R1                      2002-05-07 by Markus Ewald  //
//  //
#pragma once

#include <string>

namespace Base64 {

/// Encode string to base64
inline std::string encode(const std::string &sString);
/// Encode base64 into string
inline std::string decode(const std::string &sString);

/// Encode buffer to base64
inline std::string encode(const unsigned char *buffer, unsigned long len);

/// Encode base64 into buffer
inline void decode(const std::string &sString, unsigned char **buffer, unsigned long *len);

}; // namespace Base64

// ####################################################################### //
// # Base64::encode()                                                    # //
// ####################################################################### //
/** Encodes the specified string to base64

    @param  sString  String to encode
    @return Base64 encoded string
*/
inline std::string Base64::encode(const std::string &sString) {
	return encode((const unsigned char *)sString.c_str(), sString.length());
}

inline std::string Base64::encode(const unsigned char *buffer, unsigned long nLength) {
	static const std::string sBase64Table(
		// 0000000000111111111122222222223333333333444444444455555555556666
		// 0123456789012345678901234567890123456789012345678901234567890123
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
	static const char cFillChar = '=';
	std::string sResult;

	if (nLength == 0)
		return sResult;

	// Allocate memory for the converted string
	sResult.reserve(nLength * 8 / 6 + 1);

	for (unsigned long nPos = 0; nPos < nLength; nPos++) {
		char cCode;

		// Encode the first 6 bits
		cCode = (buffer[nPos] >> 2) & 0x3f;
		sResult.append(1, sBase64Table[cCode]);

		// Encode the remaining 2 bits with the next 4 bits (if present)
		cCode = (buffer[nPos] << 4) & 0x3f;
		if (++nPos < nLength)
			cCode |= (buffer[nPos] >> 4) & 0x0f;
		sResult.append(1, sBase64Table[cCode]);

		if (nPos < nLength) {
			cCode = (buffer[nPos] << 2) & 0x3f;
			if (++nPos < nLength)
				cCode |= (buffer[nPos] >> 6) & 0x03;

			sResult.append(1, sBase64Table[cCode]);
		} else {
			++nPos;
			sResult.append(1, cFillChar);
		}

		if (nPos < nLength) {
			cCode = buffer[nPos] & 0x3f;
			sResult.append(1, sBase64Table[cCode]);
		} else {
			sResult.append(1, cFillChar);
		}
	}

	return sResult;
}

// ####################################################################### //
// # Base64::decode()                                                    # //
// ####################################################################### //
/** Decodes the specified base64 string

    @param  sString  Base64 string to decode
    @return Decoded string
*/
inline std::string Base64::decode(const std::string &sString) {
	unsigned char *buffer = 0;
	unsigned long len = 0;
	std::string sResult;

	decode(sString, &buffer, &len);
	if (buffer && strlen((const char *)buffer) == len)
		sResult.assign((const char *)buffer, len);

	if (buffer)
		free(buffer);

	return sResult;
}

inline void Base64::decode(const std::string &sString, unsigned char **_sResult, unsigned long *nLen) {
	static const std::string::size_type np = std::string::npos;
	static const std::string::size_type DecodeTable[] = {
		// 0   1   2   3   4   5   6   7   8   9
		np, np, np, np, np, np, np, np, np, np, //   0 -   9
		np, np, np, np, np, np, np, np, np, np, //  10 -  19
		np, np, np, np, np, np, np, np, np, np, //  20 -  29
		np, np, np, np, np, np, np, np, np, np, //  30 -  39
		np, np, np, 62, np, np, np, 63, 52, 53, //  40 -  49
		54, 55, 56, 57, 58, 59, 60, 61, np, np, //  50 -  59
		np, np, np, np, np, 0, 1, 2, 3, 4, //  60 -  69
		5, 6, 7, 8, 9, 10, 11, 12, 13, 14, //  70 -  79
		15, 16, 17, 18, 19, 20, 21, 22, 23, 24, //  80 -  89
		25, np, np, np, np, np, np, 26, 27, 28, //  90 -  99
		29, 30, 31, 32, 33, 34, 35, 36, 37, 38, // 100 - 109
		39, 40, 41, 42, 43, 44, 45, 46, 47, 48, // 110 - 119
		49, 50, 51, np, np, np, np, np, np, np, // 120 - 129
		np, np, np, np, np, np, np, np, np, np, // 130 - 139
		np, np, np, np, np, np, np, np, np, np, // 140 - 149
		np, np, np, np, np, np, np, np, np, np, // 150 - 159
		np, np, np, np, np, np, np, np, np, np, // 160 - 169
		np, np, np, np, np, np, np, np, np, np, // 170 - 179
		np, np, np, np, np, np, np, np, np, np, // 180 - 189
		np, np, np, np, np, np, np, np, np, np, // 190 - 199
		np, np, np, np, np, np, np, np, np, np, // 200 - 209
		np, np, np, np, np, np, np, np, np, np, // 210 - 219
		np, np, np, np, np, np, np, np, np, np, // 220 - 229
		np, np, np, np, np, np, np, np, np, np, // 230 - 239
		np, np, np, np, np, np, np, np, np, np, // 240 - 249
		np, np, np, np, np, np // 250 - 256
	};
	static const char cFillChar = '=';

	*nLen = 0;
	std::string::size_type nLength = sString.length();
	if (nLength == 0) {
		*_sResult = 0;
		return;
	}

	*_sResult = (unsigned char *)malloc(nLength);
	unsigned char *sResult = *_sResult;

	for (std::string::size_type nPos = 0; nPos < nLength; nPos++) {
		unsigned char c, c1;

		c = (char)DecodeTable[(unsigned char)sString[nPos]];
		nPos++;
		c1 = (char)DecodeTable[(unsigned char)sString[nPos]];
		c = (c << 2) | ((c1 >> 4) & 0x3);
		sResult[(*nLen)++] = c;

		if (++nPos < nLength) {
			c = sString[nPos];
			if (cFillChar == c)
				break;

			c = (char)DecodeTable[(unsigned char)sString[nPos]];
			c1 = ((c1 << 4) & 0xf0) | ((c >> 2) & 0xf);
			sResult[(*nLen)++] = c1;
		}

		if (++nPos < nLength) {
			c1 = sString[nPos];
			if (cFillChar == c1)
				break;

			c1 = (char)DecodeTable[(unsigned char)sString[nPos]];
			c = ((c << 6) & 0xc0) | c1;
			sResult[(*nLen)++] = c;
		}
	}
}

