///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <sstream>
#include <memory>
#include <string>
#include <vector>
#include <iostream>

#include "Core/Util/CoreUtil.h"

#pragma comment(lib, "dbghelp.lib")

/**
 * @file
 * Class defining objects comprised of a string and an unsigned long
 * where the string contains the name of a source file and the
 * unsigned long indicates a line number within that file.
 */
class COREUTIL_API SourceLocation {
public:
	/**
	 * Default constructor
	 */
	SourceLocation();

	/**
	 * Construct with a filename and a line number.
	 */
	SourceLocation(const char* file, unsigned long line, const char* fn);

	/**
	 * Copy constructor
	 */
	SourceLocation(const SourceLocation& other);

	/**
	 * Destructor
	 */
	virtual ~SourceLocation();

	/**
	 * Assignment operator
	 */
	SourceLocation& operator=(const SourceLocation& rhs);

	/**
	 * Acquire constant reference to the filename for this
	 * source location.
	 */
	const std::string& file() const;

	/**
	 * Set the filename for this source location.
	 * @return reference to self.
	 */
	SourceLocation& setFile(char* fileName);

	/**
	 * Acquire the line number for this source location
	 */
	unsigned long line() const;

	/**
	 * Set the line number for this source location.
	 * @return reference to self.
	 */
	SourceLocation& setLine(unsigned long line);

	/**
	 * Acquire the name of the function of this stack frame
	 */
	const std::string& fn() const;

	/**
	 * Set the frames function name
	 */
	SourceLocation& setFn(char* fnName);

private:
	std::string _file;
	unsigned long _line;
	std::string _function;
};

/**
 * Short hand to place a SourceLocation object on the stack.
 */
#define SOURCELOCATION SourceLocation(__FILE__, __LINE__, __FUNCTION__)

/**
 * @file
 * Representation of a single entry in a StackTrace.  Comprised of symbol info as well as source 
 * location info.
 */
class COREUTIL_API StackFrame {
public:
	/**
	 * Default Constructor.
	 */
	StackFrame() :
			_symbol((char*)0), _location() {}

	/**
	 * Construct from pointer to a byte buffer containing the symbol information for this
	 * frame and the SourceLocation for the frame.
	 */
	StackFrame(char* symbol, const SourceLocation& location) :
			_symbol(symbol), _location(location) {}

	/**
	 * Copy constructor
	 */
	StackFrame(const StackFrame& other) :
			_symbol(other._symbol), _location(other._location) {}

	/**
	 * Destructor
	 */
	virtual ~StackFrame() {}

	/**
	 * Assignment operator
	 */
	StackFrame& operator=(const StackFrame& rhs) {
		_location = rhs._location;
		_symbol = rhs._symbol;
		return *this;
	}

	/**
	 * Acquire a reference to the Frame's internal SYMBOL_INFO struct as pointer to char.
	 * Caller will have to cast to SYMBOL_INFO* to use it.  We return this as a char* 
	 * to avoid adding include dependencies on the DbgHelp.h
	 *
	 * You should not need to access this external to StackTrace but it is provided here as a
	 * convenience.
	 */
	char* symbol() const { return _symbol; }

	/**
	 * Acquire const reference to the SourceLocation for this exception.  This location identifies
	 * the source file and line number corresponding to this frame.
	 * @returns const reference to this frames SourceLocation.
	 */
	const SourceLocation& location() const { return _location; }

	/**
	 * Convenience method for acquiring the file name component of this frame's SourceLocation.
	 * Synonymous to StackFrame::SourceLocation::file()
	 *
	 * @return string containing the file name component of this frame's SourceLocation.
	 */
	const std::string& file() const { return _location.file(); }

	/**
	 * Convenience method for acquiring the line number component of this frame's SourceLocation.
	 * Synonymous to StackTrace::location().line()
	 *
	 * @return unsigned long containing the line component of this frames SourceLocation.
	 */
	unsigned long line() const { return _location.line(); }

	/**
	 * Convenience method for acquiring the name this frame's symbol component.
	 */
	std::string symbolName() const; //	{ return ((SYMBOL_INFO*)_symbol)->Name; 	}

	std::string str() const {
		std::stringstream ss;
		ss << symbolName(); // << "(" << file() << ":" << line() << ")";
		return ss.str();
	}

private:
	char* _symbol; // pointer to head of memory block
		// containing symbol information
		// for the call stack
	SourceLocation _location; // Vector containing corresponding
		// source location for
};

/**
 * Class to capture the call stack at the point it is constructed.
 *
 * This class is used in ChainedExceptions to provide call stack functionality.
 * At the point when an exception is thrown, the application may be unstable
 * so allocation of memory on the heap should be minimized.  To that end
 * this class maintains a reference to the stack data, thus allowing us to 
 * copy the StackTrace around without recapturing the stack and or allocating
 * any stack memory beyond it's initial construction.
 */
class COREUTIL_API StackTrace {
public:
	typedef std::vector<StackFrame> FrameVector;

	/**
	 * Default Constructor
	 */
	StackTrace();

	/**
	 * Copy Constructor
	 */
	StackTrace(const StackTrace& tocopy);

	/**
	 * Destructor
	 */
	virtual ~StackTrace();

	/**
	 * Assignment operator
	 */
	StackTrace& operator=(const StackTrace& rhs);

	/**
	 * Equivalency operator
	 */
	bool operator==(const StackTrace& rhs);

	/**
	 * @deprecated.  Use stack traversal functionality
	 * to facilitate performing functions such as this
	 * external the StackTrace itself.
	 */
	void printStack() const;

	/**
	 * Stream operator
	 */
	std::ostream& streamOut(std::ostream& os) const;

	/**
	 * Each StackTrace has a hash code that can be used
	 * for testing equivalency.
	 */
	unsigned long hashCode() const;

	/**
	 * Acquire reference to the traces internal vector of stack frames
	 * 
	 */
	const FrameVector& frames() const { return _internal->_frameVec; }

	/**
	 * Output stream operator
	 */
	friend COREUTIL_API std::ostream& operator<<(std::ostream& o, const StackTrace& st);

	std::string str() const {
		std::stringstream ss;
		ss << *this;
		return ss.str();
	}

private:
	class InternalState {
	public:
		InternalState();
		virtual ~InternalState();

		bool captureStack(unsigned long frameStart = 3, unsigned long frames = 100);

		unsigned long _hash;
		char* _symbol; // pointer to head of memory block
			// containing symbol information
			// for the call stack
		std::vector<StackFrame> _frameVec;

	private:
		// disabled.  You can copy pointers to me.
		// but not me.
		InternalState(const InternalState& /*other*/) {}
		InternalState& operator=(const InternalState& /*rhs*/) {}
	};
	typedef std::shared_ptr<InternalState> InternalStatePtr;

	InternalStatePtr _internal;
};
