///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <set>

/**
 * Wrapper around std::set<>.  Provides simple generalized functionality that 
 * frees the consumer of iterator management.  
 *
 * N.B. the use of the word Simple.  This class provides no thread safety.
 * Furthermore, the original intent of this class was as a replacement 
 * for the highly MS centric, highly specific CBlockIPList class.  To this 
 * end, this class replaces only functionality needed to replace CBlockIPList.  
 * For example, it does not expose the underlying implementation (std::set<>) 
 * and it no functionality for iterating over the set. I.e. if you see missing
 * functionality, feel free to add it.
 */
template <typename contentT>
class SimpleSet {
public:
	SimpleSet() {}

	~SimpleSet() {}

	/**
	 * Determine if the supplied string is present in the set.
	 * @param str String to searchr for.
	 * @return true if the supplied string is present in the set, else false.
	 */
	bool exists(const contentT& val) const {
		return _set.find(val) != _set.cend();
	}

	/**
	 * Add a string to the set.
	 * @param str   String to be added to the set
	 * @return reference to self
	 */
	SimpleSet& add(const contentT& str) {
		_set.insert(str);
		return *this;
	}

	/**
	 * Remove the provided string from the set.
	 * @param str The string to be removed from the set.
	 * @return reference to self
	 */
	SimpleSet& remove(const contentT& val) {
		std::set<contentT>::iterator iter = _set.find(val);
		if (iter != _set.end())
			_set.erase(iter);
		return *this;
	}

	/**
	 * Determine the number of strings in the set.
	 * @return	The numbe of strings in the set.
	 */
	unsigned long size() const {
		return _set.size();
	}

	/**
	 * Clear the set.
	 * @returns reference to self
	 */
	SimpleSet& clear() {
		_set.clear();
		return *this;
	}

private:
	std::set<contentT> _set;
};
