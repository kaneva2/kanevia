///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <sstream>
#include <xmemory>
#include <utility>

/**
 * Much of the socket API is premised on the pattern of:
 *   1) Perform Socket operation.
 *   2) Call WSAGetLastError() to find out if the operation was successful
 *      and if not, why?
 *   3) If error occurred, dig up error texgt for the error.
 *
 * This class simply allows these two return values (error code and error message) to 
 * be passed back as a single return value (as opposed to using output parameters.
 */
class ErrorSpec : public std::pair<unsigned long, std::string> {
	typedef std::pair<unsigned long, std::string> super;

public:
	/**
	 * Default constructor.  Error code 0 (no error) and empty
	 * err message (again indicating no error).
	 */
	ErrorSpec() :
			super(0, "") {}

	/**
     * Construct with an error code only
     */
	ErrorSpec(unsigned long code) :
			super(code, "") {}

	/**
	 * Construct with an error code and associated message.
	 * @param code	The code of the error represented by instances of this class.
	 * @param text  Descriptive text giving more detailed information about the
	 *				error indicated by code.  It is sufficient to have an error code,
	 *				but at the very least an empty "" must be supplied.  This
	 *				parameter is not defaulted to insure that return an ErrorSpec for
	 *				for a bool or int generates an error message from the compiler. 
	 */
	ErrorSpec(unsigned long code, const std::string& text) :
			super(code, text) // , nested(0)
	{}

	/**
	 * Copy constructor.
	 * @param toCopy    Reference to error spec to be copied from.
	 */
	ErrorSpec(const ErrorSpec& toCopy) :
			super(toCopy.first, toCopy.second) //, nested( toCopy.nested )
	{}

	/**
	 * stringstream semantics for appending/populating
	 * the message component of the ErrorSpec
	 */
	ErrorSpec& operator<<(const std::string& s) {
		std::stringstream ss;
		ss << second << s;
		second = ss.str();
		return *this;
	}

	/**
	* stringstream semantics for appending/populating
	* the message component of the ErrorSpec
	*/
	ErrorSpec& operator<<(unsigned long dval) {
		std::stringstream ss;
		ss << second << dval;
		second = ss.str();
		return *this;
	}

	/**
	 * Support for streaming the contents of the error spec
	 */
	friend std::ostream& operator<<(std::ostream& os, const ErrorSpec& espec) {
		os << espec.str(); // obj.streamStack(os, 0);
		return os;
	}

	/**
	 * Assignment operator.
	 * @param rhs reference to ErrorSpec to assign from.
	 */
	ErrorSpec& operator=(const ErrorSpec& rhs) {
		first = rhs.first;
		second = rhs.second;
		//nested	= rhs.nested;
		return *this;
	}

	/**
	 * Equality operator.
	 * @param rhs reference to ErrorSpec to be compared to.
	 */
	bool operator==(const ErrorSpec& rhs) {
		if (this == &rhs)
			return true;
		return first == rhs.first;
	}

	/**
	 * Success operator.  Comparing an instance of ErrorSpec to a boolean implies a 
	 * test for success or failure.
	 * @param b    True to test for no error
	 *			   False to test for failure.
	 * @return
	 */
	bool operator==(const bool b) {
		return noError() == b;
	}

	/**
	 * Determine if this Error spec indicates "no error"
	 * @returns true this ErrorSpec reprsents "no error" (0), else false.
	 */
	bool noError() { return first == 0; }

	/**
	 * Determine if this Error spec indicates "error"
	 * @returns true this ErrorSpec reprsents "error" (!0), else false.
	 */
	bool error() { return !noError(); }

	/**
	 * Acquire the code component of the Error Spec. Something more intuitive than ErrorSpec::first 
	 */
	unsigned long code() const {
		return super::first;
	}

	/**
	 * Acquire the message component of the Error Spec. Something more intuitive than ErrorSpec::second
	 */
	const std::string& message() const {
		return super::second;
	}

	/**
	 * @deprecated in favor str() which is consistent with std:: methods
	 */
	std::string toString() const {
		std::stringstream ss;
		ss << "ErrorSpec[code=" << first << ";message=\"" << second << "\"]";
		return ss.str();
	}

	std::string str() const {
		std::stringstream ss;
		ss << "ErrorSpec[code=" << first << ";message=\"" << second << "\"]";
		return ss.str();
	}

	//	ErrorSpec nested; // noError if not set.
};
