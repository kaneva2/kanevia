///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <type_traits>

template <typename EnumType>
struct enable_scoped_enum_bit_flags : public std::false_type {};

template <typename EnumType>
typename std::enable_if<enable_scoped_enum_bit_flags<EnumType>::value, EnumType>::type
operator|(EnumType lhs, EnumType rhs) {
	return static_cast<EnumType>(static_cast<std::underlying_type<EnumType>::type>(lhs) | static_cast<std::underlying_type<EnumType>::type>(rhs));
}

template <typename EnumType>
typename std::enable_if<enable_scoped_enum_bit_flags<EnumType>::value, EnumType>::type
operator&(EnumType lhs, EnumType rhs) {
	return static_cast<EnumType>(static_cast<std::underlying_type<EnumType>::type>(lhs) & static_cast<std::underlying_type<EnumType>::type>(rhs));
}

template <typename EnumType>
typename std::enable_if<enable_scoped_enum_bit_flags<EnumType>::value, EnumType>::type
operator~(EnumType lhs) {
	return static_cast<EnumType>(~static_cast<std::underlying_type<EnumType>::type>(lhs));
}

#define ENABLE_SCOPED_ENUM_BIT_FLAGS(EnumType) \
	template <>                                \
	struct enable_scoped_enum_bit_flags<EnumType> { static const bool value = true; }
