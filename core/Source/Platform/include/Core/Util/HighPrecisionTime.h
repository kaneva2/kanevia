///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include "Core/Util/CoreUtil.h"

// High-Precision Time Support
class COREUTIL_API fTime {
public:
	typedef double Ns; // nanoseconds
	typedef double Us; // microseconds
	typedef double Ms; // milliseconds
	typedef double Sec; // seconds

	static double Time(double scale);
	static inline Ns TimeNs() {
		return Time(1e9);
	}
	static inline Us TimeUs() {
		return Time(1e6);
	}
	static inline Ms TimeMs() {
		return Time(1e3);
	}
	static inline Sec TimeSec() {
		return Time(1.0);
	}

	static inline Ns ElapsedNs(const Ns& ns) {
		return TimeNs() - ns;
	}
	static inline Us ElapsedUs(const Us& us) {
		return TimeUs() - us;
	}
	static Ms ElapsedMs(const Ms& ms);
	//{
	//	return TimeMs() - ms;
	//}
	static inline Sec ElapsedSec(const Sec& sec) {
		return TimeSec() - sec;
	}

	static void SleepNs(const Ns& ns);
	static inline void SleepUs(const Us& us) {
		SleepNs(us * 1e3);
	}
	static void SleepMs(const Ms& ms);
	static void SleepSec(const Sec& sec);

	static Ms SetTimeSyncMs(const Ms& ms);
	static Ms TimeSyncMs();
};

// Convenience Types
typedef fTime::Ns TimeNs;
typedef fTime::Us TimeUs;
typedef fTime::Ms TimeMs;
typedef fTime::Sec TimeSec;

// High-Precision Timer Support
class COREUTIL_API Timer {
public:
	enum class State {
		Paused,
		Running
	};

	inline void SetState(const State& state) {
		m_state = state;
	}

	inline bool IsPaused() const {
		return (m_state == State::Paused);
	}

	inline bool IsRunning() const {
		return (m_state == State::Running);
	}

	Timer(const State& state = State::Running);

	Timer& Start();
	//{
	//	if (IsRunning())
	//		return *this;
	//	m_startNs = fTime::TimeNs();
	//	SetState(State::Running);
	//	m_count++;
	//	return *this;
	//}

	Timer& Pause();
	//{
	//	if (IsPaused())
	//		return *this;
	//	m_elapseNs += fTime::ElapsedNs(m_startNs);
	//	SetState(State::Paused);
	//	return *this;
	//}

	Timer& Reset(bool resetElapse = true);
	//{
	//	if (resetElapse)
	//		m_elapseNs = 0.0;
	//	m_count = 0;
	//	m_startNs = (IsRunning() ? fTime::TimeNs() : 0.0);
	//	return *this;
	//}

	inline size_t Count() const {
		return m_count;
	}

	inline Timer& ElapseNs(const TimeNs& ns) {
		m_elapseNs += ns;
		return *this;
	}
	inline Timer& ElapseUs(const TimeUs& us) {
		return ElapseNs(us * 1e3);
	}
	inline Timer& ElapseMs(const TimeMs& ms) {
		return ElapseNs(ms * 1e6);
	}
	inline Timer& ElapseSec(const TimeSec& sec) {
		return ElapseNs(sec * 1e9);
	}

	inline TimeNs ElapsedNs() const {
		return IsPaused() ? m_elapseNs : (fTime::ElapsedNs(m_startNs) + m_elapseNs);
	}
	inline TimeUs ElapsedUs() const {
		return ElapsedNs() / 1e3;
	}
	TimeMs ElapsedMs() const; /* {
		return ElapsedNs() / 1e6;
	}*/
	inline TimeSec ElapsedSec() const {
		return ElapsedNs() / 1e9;
	}

private:
	State m_state;
	TimeNs m_startNs;
	TimeNs m_elapseNs;
	size_t m_count;
};

class TimeDelay {
public:
	TimeDelay(Timer::State state = Timer::State::Paused) :
			m_delayMs(0.0),
			m_timer(state) {}

	TimeDelay(TimeMs delayTime, Timer::State state = Timer::State::Paused) :
			m_delayMs(delayTime),
			m_timer(state) {}

	virtual ~TimeDelay() {}

	TimeDelay setDelayTime(TimeMs timeMs) {
		m_delayMs = timeMs;
		return *this;
	}

	TimeMs delayTime() const {
		return m_delayMs;
	}

	Timer& timer() {
		return m_timer;
	}

	bool elapsed() const {
		return (m_timer.ElapsedMs() >= m_delayMs);
	}

private:
	TimeMs m_delayMs;
	Timer m_timer;
};

#include <iostream>
#include <iomanip>
#define FMT_TIME std::setprecision(1) << std::fixed
