///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "fast_mutex.h"
#include "Core/Util/CoreUtil.h"
#include <functional>
#include <memory>
#include <vector>

namespace KEP {

class CallbackSystem {
public:
	COREUTIL_API CallbackSystem();
	COREUTIL_API ~CallbackSystem();

	CallbackSystem(const CallbackSystem&) = delete;
	void operator=(const CallbackSystem&) = delete;

	static CallbackSystem* Instance() {
		return &s_Instance;
	}

	// pCallbackProvider is the system that triggers the callback, such as ResourceManager*, ContentService*
	COREUTIL_API bool RegisterCallback(void* pCallbackProvider, uint64_t callbackId, void* pCallbackOwner, const std::weak_ptr<void>& pWeakCallbackOwner, std::function<void()> callback);

	COREUTIL_API bool RegisterCallback(void* pCallbackProvider, const std::vector<uint64_t>& aCallbackIds, void* pCallbackOwner, const std::weak_ptr<void>& pWeakCallbackOwner, std::function<void()> callback);

	// Unregisters a single callback
	COREUTIL_API size_t UnregisterCallback(void* pCallbackProvider, uint64_t callbackId, void* pCallbackOwner);

	// Unregisters all callbacks with a particular owner. Usually used when owner is destroyed.
	COREUTIL_API size_t UnregisterAllCallbacksWithOwner(void* pCallbackOwner);

	// Called by provider when callbacks associated with callbackId should be called.
	COREUTIL_API void CallbackReady(void* pCallbackProvider, uint64_t callbackId);

	// Execute callbacks that have been marked as Ready
	COREUTIL_API void ExecuteCallbacks();

private:
	static COREUTIL_API CallbackSystem s_Instance;

	fast_mutex m_mtxCallbackMaps;
	class CallbackMap;
	std::unique_ptr<CallbackMap> m_pCallbacksPending;
	std::unique_ptr<CallbackMap> m_pCallbacksReady;
};

} // namespace KEP
