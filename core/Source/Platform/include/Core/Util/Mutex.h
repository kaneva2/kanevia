///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "fast_mutex.h"

namespace KEP {

using Mutex = KEP::fast_recursive_mutex; //jsFastSync;
using MutexGuardLock = std::unique_lock<Mutex>; //jsFastLock;

} // namespace KEP