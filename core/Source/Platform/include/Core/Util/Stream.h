///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>

namespace KEP {

// NullOStreamBuf
// A streambuf that ignores everything written to it.
template <typename Char>
class Basic_NullOStreamBuf : public std::basic_streambuf<Char> {
public:
	Basic_NullOStreamBuf() {}

private:
	virtual int_type overflow(int_type ch) override {
		return ch;
	}
};

// NullOStream
// A stream the ignores everything written to it.
template <typename Char>
class Basic_NullOStream : private Basic_NullOStreamBuf<Char>, public std::basic_ostream<Char> {
public:
	Basic_NullOStream() :
			Basic_NullOStreamBuf(), std::ostream(static_cast<Basic_NullOStreamBuf<Char>*>(this)) {
	}
};

using NullOStream = Basic_NullOStream<char>;
using WNullOStream = Basic_NullOStream<wchar_t>;

// OStringStreamBuf
// A streambuf that writes to a string.
// Similar to std::basic_stringbuf but can write to an existing string and allows direct access to the contained string.
template <typename Char>
class OStringStreamBuf : public std::basic_streambuf<Char> {
public:
	OStringStreamBuf() :
			m_str(m_strOwned) {}
	OStringStreamBuf(std::basic_string<Char>& str) :
			m_str(str) {}
	OStringStreamBuf(OStringStreamBuf&& other) :
			m_str(&other.m_strOwned == &other.m_str ? m_strOwned : other.m_str), m_strOwned(std::move(other.m_strOwned)) {}

	const std::basic_string<Char>& GetString() const { return m_str; }
	std::basic_string<Char>& GetString() { return m_str; }

private:
	virtual std::streamsize xsputn(const char* p, std::streamsize count) {
		m_str.append(p, count);
		return count;
	}
	virtual int_type overflow(int_type ch) override {
		m_str.push_back(ch);
		return ch;
	}
	virtual int sync() override {
		return 0;
	}

private:
	std::basic_string<Char> m_strOwned;
	std::basic_string<Char>& m_str;
};

// Basic_OStringStream
// A streambuf that writes to a string.
// Similar to std::basic_ostringstream but can write to an existing string and allows direct access to the contained string.
// Using inheritance instead of containment for OStringStreamBuf because we need it constructed before std::ostream so we can pass it as a constructor parameter.
template <typename Char>
class Basic_OStringStream : private OStringStreamBuf<Char>, public std::ostream {
public:
	Basic_OStringStream() :
			OStringStreamBuf(), std::ostream(static_cast<OStringStreamBuf*>(this)) {
	}
	Basic_OStringStream(Basic_OStringStream&& other) :
			OStringStreamBuf<Char>(std::move(other)), std::ostream(static_cast<OStringStreamBuf*>(this)) {
	}

	explicit Basic_OStringStream(std::basic_string<Char>& str) :
			OStringStreamBuf(str), std::ostream(static_cast<OStringStreamBuf*>(this)) {
	}

	using OStringStreamBuf<Char>::GetString;

private:
};

using OStringStream = Basic_OStringStream<char>;
using WOStringStream = Basic_OStringStream<wchar_t>;

template <typename Char>
inline Basic_OStringStream<Char> AsStream(std::basic_string<Char>& str) {
	return Basic_OStringStream<Char>(str);
}

namespace detail {
template <typename Char>
inline void StreamToStringHelper(Basic_OStringStream<Char>& strm) {
}
template <typename Char, typename NextArg, typename... Args>
inline void StreamToStringHelper(Basic_OStringStream<Char>& strm, NextArg&& nextArg, Args&&... args) {
	strm << std::forward<NextArg>(nextArg);
	StreamToStringHelper<Char, Args...>(strm, std::forward<Args>(args)...);
}
} // namespace detail

template <typename... Args>
inline std::string StreamToString(Args&&... args) {
	OStringStream strm;
	detail::StreamToStringHelper(strm, args...);
	return std::move(strm.GetString());
}

template <typename... Args>
inline std::wstring StreamToWString(Args&&... args) {
	OWStringStream strm;
	detail::StreamToStringHelper(strm, args...);
	return std::move(strm.GetString());
}

#if 0
// LoggerStreamBuf
// A streambuf that writes to the log on sync().
class LoggerStreamBuf : public OStringStreamBuf<char> {
public:
	LoggerStreamBuf(const log4cplus::Logger& logger, int iLogLevel, const char* pszFileName = nullptr, int iLineNumber = -1)
		: m_logger(logger)
		, m_iLogLevel(iLogLevel)
		, m_pszFileName(pszFileName)
		, m_iLineNumber(iLineNumber)
		, m_nSyncedChars(0)
	{}
	~LoggerStreamBuf() {
		sync();
	}
private:
	virtual int sync() override {
		m_logger.log(m_iLogLevel, KEP::Utf8ToUtf16(GetString().c_str()+m_nSyncedChars), m_pszFileName, m_iLineNumber);
		m_nSyncedChars = GetString().size();
		return 0;
	}
private:
	size_t m_nSyncedChars;
	int m_iLogLevel;
	const char* m_pszFileName;
	int m_iLineNumber;
	log4cplus::Logger m_logger;
};


// LoggerStreamBuf can't be a member because we need it constructed before std::ostream so we can pass it as a constructor parameter.
class LoggerStream : private LoggerStreamBuf, public std::ostream {
public:
	LoggerStream(const log4cplus::Logger& logger, int iLogLevel, const char* pszFileName = nullptr, int iLineNumber = -1)
		: LoggerStreamBuf(logger, iLogLevel, pszFileName, iLineNumber)
		, std::ostream(static_cast<LoggerStreamBuf*>(this))
	{
	}
private:
};
//#define LogInfo(txt) LoggerStream(m_logger, log4cplus::INFO_LOG_LEVEL, __FILE__, __LINE__) << txt;


class IntervalLogger {
	IntervalLogger(const log4cplus::Logger& logger, int iLogLevel, float iIntervalSeconds)
		: m_iLogInterval(iIntervalSeconds * 1000)
		, m_LoggerStream(logger, iLogLevel)
	{
	}

	void UpdateEnabled() {
		unsigned int curr_tick = GetTickCount();
		if( curr_tick - m_iLastLogTick >= m_iLogInterval ) {
			m_iLastLogTick = curr_tick;
			m_bLogEnabled = true;
		} else {
			m_bLogEnabled = false;
		}
	}

	std::ostream& Stream() {
		if( m_bLogEnabled )
			return m_LoggerStream;
		else
			return m_NullStream;
	}

private:
	LoggerStream m_LoggerStream;
	NullOStream<char> m_NullStream;
	unsigned int m_iLastLogTick = 0;
	unsigned int m_iLogInterval = 10000;
	bool m_bLogEnabled = true;
};
#endif

} // namespace KEP