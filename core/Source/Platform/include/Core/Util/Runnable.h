///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <functional>

#include "Core/Util/ChainedException.h"

namespace KEP {

class RunResult {
private:
	int _n;
	ChainedException _e;

public:
	RunResult(int n = 0) :
			_n(n) {}
	RunResult(const ChainedException& e) :
			_n(e.errorSpec().code()), _e(e) {}
	RunResult(const RunResult& toCopy) :
			_n(toCopy._n) {}
	RunResult& operator=(const RunResult& rhs) { _n = rhs._n; }
	RunResult& setException(const ChainedException& e) { _e = e; }
};

typedef std::shared_ptr<RunResult> RunResultPtr;

class StringRunResult : public RunResult {
private:
	std::string _s;

public:
	StringRunResult(int n = 0, const std::string& s = "") :
			RunResult(n), _s(s) {}
	StringRunResult(const StringRunResult& toCopy) :
			RunResult(toCopy), _s(toCopy._s) {}
	StringRunResult& operator=(const StringRunResult& rhs) {
		RunResult::operator=(rhs);
		_s = rhs._s;
		return *this;
	}

	const std::string& str() const { return _s; }
};

/**
 * Analog to the java Runnable interface.
 */
class Runnable {
public:
	Runnable() {}
	virtual ~Runnable() {}
	virtual RunResultPtr run() = 0;
};
typedef std::shared_ptr<Runnable> RunnablePtr;

class NoopRunnable : public Runnable {
public:
	NoopRunnable() {}
	virtual ~NoopRunnable() {}
	virtual RunResultPtr run() override {
		return RunResultPtr();
	}
};

class FunctionRunner : public Runnable {
public:
	FunctionRunner(std::function<RunResultPtr()> func) :
			_func(func) {}

	virtual ~FunctionRunner() {}

	virtual RunResultPtr run() override {
		return _func();
	}

private:
	std::function<RunResultPtr()> _func;
};

} // namespace KEP
