///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>
#include <memory>
#include "Core/Util/StackTrace.h"
#include "Core/Util/ErrorSpec.h"

struct TraceFlag {};
#define TRACETHROW TraceFlag()

class ExceptionContext {
public:
	ExceptionContext() :
			_stackTrace(), _location(), _errorSpec(), _chained(0) {}

	ExceptionContext(const SourceLocation& location, const ErrorSpec& errorSpec) :
			_stackTrace(), _location(location), _errorSpec(errorSpec), _chained(0) {}

	ExceptionContext(const ExceptionContext& other) :
			_stackTrace(other._stackTrace), _location(other._location), _errorSpec(other._errorSpec), _chained(0), _exceptionType(other._exceptionType) {
		if (other._chained > 0)
			_chained = new ExceptionContext(*(other._chained));
	}

	ExceptionContext(StackTrace* stackTrace) :
			_stackTrace(stackTrace), _location(), _errorSpec(), _chained(0) {}

	ExceptionContext(StackTrace* stackTrace, const ErrorSpec& errorSpec) :
			_stackTrace(stackTrace), _errorSpec(errorSpec), _location(), _chained(0) {}

	ExceptionContext(const ErrorSpec& errorSpec) :
			_stackTrace(), _errorSpec(errorSpec), _location(), _chained(0) {}

	ExceptionContext(const SourceLocation& location, const ErrorSpec& errorSpec, StackTrace* stackTrace) :
			_location(location), _errorSpec(errorSpec), _stackTrace(stackTrace), _chained(0) {}

	virtual ~ExceptionContext() {
		if (_chained != 0)
			delete _chained;
	}

	const ExceptionContext& printStack(int level) const {
		streamStack(std::cout, level);
		return *this;
	}

	const ExceptionContext& streamStack(std::ostream& os, int level) const {
		//cout << "ExceptionContext::printStack(" << level << "):" << this << endl;
		os << _exceptionType << ": " << _errorSpec.second << std::endl;
		os << _location.file() << ":" << _location.line() << std::endl;

		// Acquire the stack and iterate through each of the frames and display them.
		//
		if (_stackTrace.operator->() == 0) {
			if (_chained != 0) {
				os << "Caused by: ";
				_chained->streamStack(os, level);
			}
			return *this;
		}
		const StackTrace::FrameVector& vec = _stackTrace->frames();

		int counter = -1;
		int max = vec.size() - 1;
		for (StackTrace::FrameVector::const_reverse_iterator iter = vec.rbegin(); iter != vec.rend(); iter++) {
			counter++;
			if (counter < level)
				continue;

			level++;

			if (counter >= max)
				continue; // break out.

			os << "    at " << iter->symbolName()
			   << "(" << iter->file()
			   << ":" << iter->line()
			   << ")\n";
		}

		if (_chained > 0) {
			os << "Caused by: ";
			_chained->streamStack(os, counter);
		}
		return *this;
	}

	std::string toString() const {
		std::stringstream ss;
		streamStack(ss, 0);
		return ss.str();
	}

	ExceptionContext& chain(const ExceptionContext& toChain) {
		// cout << "ExceptionContext::chain(" << &toChain << "):" << this << endl;
		_chained = new ExceptionContext(toChain);
		return *this;
	}

private:
	ExceptionContext& setExceptionType(const std::string& exceptionType) {
		_exceptionType = exceptionType;
		return *this;
	}

	const ExceptionContext* _chained;
	std::shared_ptr<StackTrace> _stackTrace;
	SourceLocation _location;
	ErrorSpec _errorSpec;
	std::string _message;
	std::string _exceptionType; // in the absence of any real reflection
	friend class ChainedException;
};

/**
 * @file
 * Extends KEPException by adding the ability to chain exceptions together.  This allows
 * the exception be much more informative because it contains more context info.
 *
 * Enabling call stack tracing.
 *
 * The process of capturing the call stack at runtime should not be considered trivial.
 * Hence, by default, StackTracing is disabled.  Whether an exception captures a stack
 * trace is determined when the exception is instantiated by explicitly passing a TraceFlag
 * instance as the first parameter on the constructors argument list.  e.g.:
 *
 *      try									{ throw ChainedException(TraceFlag());		} 
 *		catch( const ChainedException& e )	{ throw ChainedException(TraceFlag(),e);    }
 *
 * The TRACETHROW macro is provided as memorable mechanism for enabling stack traces. e.g.:
 * 
 *      try									{ throw ChainedException(TRACETHROW);		} 
 *		catch( const ChainedException& e )	{ throw ChainedException(TraceFlag(),e);	}
 *
 *
 * See ChainedExceptionTests.h for usage examples.
 */
class COREUTIL_API ChainedException : public std::exception {
public:
	typedef std::pair<unsigned long, std::string> FrameLocation;

	/** 
	 * Default constructor
	 */
	ChainedException() :
			_context(), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Construct exception which maintains a StackTrace.
	 * @param doTrace		TraceFlag instance indicating that a StackTrace
	 *						is to be maintained by this exception.
	 */
	ChainedException(TraceFlag /*doTrace*/) :
			_context(new StackTrace()), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Construct exception with an error spec.  Do maintain a stack trace.
	 * @param errorSpec		ErrorSpec instance which describes the exception.
	 */
	ChainedException(const ErrorSpec& errorSpec) :
			_context(errorSpec), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Construct exception with an error spec.  Maintain a stack trace.
	 * @param doTrace	Traceflag instance indicating that a StackTrace 
	 *					is to be maintained by this exception.
	 * @param errorSpec ErrorSpec instance which describes cause of the 
	 * exception.
	 */
	ChainedException(TraceFlag /*doTrace*/, const ErrorSpec& errorSpec) :
			_context(new StackTrace(), errorSpec), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Construct exception with an error spec.  Maintain a stack trace.
	 * @param doTrace	Traceflag instance indicating that a StackTrace 
	 *					is to be maintained by this exception.
	 * @param location	SourceLocation instance indicating where the
	 *					the exception was raised.
	 * @param errorSpec ErrorSpec instance which describes cause of the 
	 * exception.
	 */
	ChainedException(TraceFlag /*doTrace*/, const SourceLocation& location, const ErrorSpec& errorSpec) :
			_context(location, errorSpec, new StackTrace()), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Construct exception with source location and an error spec.  Does not maintain 
	 * a stack trace.
	 *
	 * @param location	SourceLocation instance indicating where the
	 *					the exception was raised.
	 * @param errorSpec ErrorSpec instance which describes cause of the 
	 * exception.
	 */
	ChainedException(const SourceLocation& location, const ErrorSpec& errorSpec) :
			_context(location, errorSpec), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Construct exception from a source location and a message.
	 * @param location	Reference to a SourceLocation instance that indicates the
	 * the location where the exception was instantiated.
	 * @param Text that describes the exception and it's parameters
	 */
	ChainedException(const SourceLocation& location, const std::string& msg = "") :
			_context(location, ErrorSpec(0xffff, msg)), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Construct exception from a source location and a message which maintains a StackTrace
	 */
	ChainedException(TraceFlag /*doTrace*/, const SourceLocation& location, const std::string& msg = "") :
			_context(location, ErrorSpec(0xffff, msg), new StackTrace()), _msgComputed(false) { setExceptionType("ChainedException"); }

	/**
	 * Copy Constructor.
	 *
	 * @param other		Reference ChainedException that caused this ChainedException to be 
	 *						instantiated.  
	 */
	ChainedException(const ChainedException& other) :
			_context(other._context), _msgComputed(false) {}

	/**
	 * dtor
	 */
	virtual ~ChainedException() {}

	/**
     * ostream support
     */
	friend std::ostream& operator<<(std::ostream& os, const ChainedException& obj) {
		obj.streamStack(os, 0);
		return os;
	}

	/**
	 * Display this chained exception's StackTrace.  Note that if the exception
	 * is not maintaining a stack trace, will simply out the Exception context
	 * path without intermediate call stack information.
	 */
	const ChainedException& printStack() const {
		streamStack(std::cout, 0);
		return *this;
	}

	/**
	 * Stream the contents of the exception to an output stream
	 */
	const ChainedException& streamStack(std::ostream& os) const {
		streamStack(os, 0);
		return *this;
	}

	std::string str() const {
		return _context.toString();
	}

	/**
	 * @see std::exception
	 */
	const char* what() const override {
		// Documentation for std::exception::what() states that the char pointer
		// is valid until the source exception (this) is destructed or until
		// a non-const method is called.
		//
		// In our case we compute the string hence the c_str() was coming from an
		// rvalue.  We have to store this in an lvalue.
		//
		if (_msgComputed)
			return _computedMsg.c_str();
		_computedMsg = str();
		_msgComputed = true;
		return _computedMsg.c_str();
	}

	/**
	 * Chain an exception to this exception.  
	 *
	 * @param toChain	reference to the exception to be chained.
	 * @ee ChainedExceptionTests.h for usage examples.
	 */
	ChainedException& chain(const ChainedException& toChain) {
		_context.chain(toChain._context);
		return *this;
	}

	/**
	 * Acquire reference to internal ErrorSpec instance.
	 * @returns reference to internal ErrorSpec instance.
	 */
	const ErrorSpec& errorSpec() const {
		return _context._errorSpec;
	}

	/**
	 * @deprecated	use standard streaming operator
	 */
	ChainedException report(std::ostream& o = std::cerr) const {
		o << str() << std::endl;
		return *this;
	}

protected:
	ChainedException& setExceptionType(const std::string& exceptionType) {
		_context.setExceptionType(exceptionType);
		return *this;
	}

private:
	// The following privatizations arecritical.  Privitizing the new and delete operators for this class we can insure that
	// instances of this class are never place on the heap.  We do this in support Throw By Value.  Buy supplying a
	// suitable copy constructor and always placing exceptions on the stack, we insure that the c++ exception handling
	// logic handles all memory handling.
	//
	static void* operator new(size_t /* size */) { return 0; };
	static void operator delete(void* /* ptr  */){};

	/**
	 * output the stack to standard out.
	 */
	const ChainedException& printStack(int level) const {
		_context.streamStack(std::cout, level);
		return *this;
	}

	/**
	 * stream the exception, with stack info into an output stream
	 * @param os    The std::ostream to which the exception contents will be
	 *              streamed.
	 * @param level Indicates the first entry in the call stack to be included.
	 *				This is used to remove duplicate stack entries from
	 *				being displayed for chained exceptions
	 */
	const ChainedException& streamStack(std::ostream& os, int level) const {
		_context.streamStack(os, level);
		return *this;
	}

	ExceptionContext _context;
	mutable std::string _computedMsg;
	mutable bool _msgComputed;
};

/**
 * In most instances simply creating a specialized exception is all that one requires of an
 * exception.  The macro below aids in doing just that.  Defines a derivative of 
 * ChainedException that adds no functionality.
 *
 * @see unit tests for usage.
 *
 */
#define CHAINED(specialization)                                                                                                                                                               \
	class specialization : public ChainedException {                                                                                                                                          \
	public:                                                                                                                                                                                   \
		specialization() : ChainedException() { setExceptionType(#specialization); }                                                                                                          \
                                                                                                                                                                                              \
		specialization(const ErrorSpec& errorSpec) : ChainedException(errorSpec) { setExceptionType(#specialization); }                                                                       \
		specialization(TraceFlag doTrace, const ErrorSpec& errorSpec) : ChainedException(doTrace, errorSpec) { setExceptionType(#specialization); }                                           \
		specialization(TraceFlag doTrace, const SourceLocation& location, const ErrorSpec& errorSpec) : ChainedException(doTrace, location, errorSpec) { setExceptionType(#specialization); } \
		specialization(const SourceLocation& location, const ErrorSpec& errorSpec) : ChainedException(location, errorSpec) { setExceptionType(#specialization); }                             \
                                                                                                                                                                                              \
		specialization(TraceFlag doTrace) : ChainedException(doTrace) { setExceptionType(#specialization); }                                                                                  \
		specialization(const SourceLocation& location, const std::string& msg = "") : ChainedException(location, msg) { setExceptionType(#specialization); }                                  \
		specialization(TraceFlag doTrace, const SourceLocation& location, const std::string& msg = "") : ChainedException(doTrace, location, msg) { setExceptionType(#specialization); }      \
		specialization(const ChainedException& cause) : ChainedException(cause) {}                                                                                                            \
		virtual ~specialization() {}                                                                                                                                                          \
                                                                                                                                                                                              \
	protected:                                                                                                                                                                                \
		specialization& operator=(const specialization& copy) {                                                                                                                               \
			ChainedException::operator=(copy);                                                                                                                                                \
			return *this;                                                                                                                                                                     \
		}                                                                                                                                                                                     \
	};

#define CHAINED_DERIVATIVE(super, specialization)                                                                                                                                  \
	class specialization : public super {                                                                                                                                          \
	public:                                                                                                                                                                        \
		specialization() : super() { setExceptionType(#specialization); }                                                                                                          \
                                                                                                                                                                                   \
		specialization(const ErrorSpec& errorSpec) : super(errorSpec) { setExceptionType(#specialization); }                                                                       \
		specialization(TraceFlag doTrace, const ErrorSpec& errorSpec) : super(doTrace, errorSpec) { setExceptionType(#specialization); }                                           \
		specialization(TraceFlag doTrace, const SourceLocation& location, const ErrorSpec& errorSpec) : super(doTrace, location, errorSpec) { setExceptionType(#specialization); } \
		specialization(const SourceLocation& location, const ErrorSpec& errorSpec) : super(location, errorSpec) { setExceptionType(#specialization); }                             \
                                                                                                                                                                                   \
		specialization(TraceFlag doTrace) : super(doTrace) { setExceptionType(#specialization); }                                                                                  \
		specialization(const SourceLocation& location, const std::string& msg = "") : super(location, msg) { setExceptionType(#specialization); }                                  \
		specialization(TraceFlag doTrace, const SourceLocation& location, const std::string& msg = "") : super(doTrace, location, msg) { setExceptionType(#specialization); }      \
		specialization(const ChainedException& cause) : super(cause) {}                                                                                                            \
		virtual ~specialization() {}                                                                                                                                               \
                                                                                                                                                                                   \
	protected:                                                                                                                                                                     \
		specialization& operator=(const specialization& copy) {                                                                                                                    \
			super::operator=(copy);                                                                                                                                                \
			return *this;                                                                                                                                                          \
		}                                                                                                                                                                          \
	};

#define CHAINEDT(specialization, specname)                                                                                                                                              \
	class specialization : public ChainedException {                                                                                                                                    \
	public:                                                                                                                                                                             \
		specialization() : ChainedException() { setExceptionType(#specname); }                                                                                                          \
                                                                                                                                                                                        \
		specialization(const ErrorSpec& errorSpec) : ChainedException(errorSpec) { setExceptionType(#specname); }                                                                       \
                                                                                                                                                                                        \
		specialization(TraceFlag doTrace, const ErrorSpec& errorSpec) : ChainedException(doTrace, errorSpec) { setExceptionType(#specname); }                                           \
                                                                                                                                                                                        \
		specialization(TraceFlag doTrace, const SourceLocation& location, const ErrorSpec& errorSpec) : ChainedException(doTrace, location, errorSpec) { setExceptionType(#specname); } \
                                                                                                                                                                                        \
		specialization(const SourceLocation& location, const ErrorSpec& errorSpec) : ChainedException(location, errorSpec) { setExceptionType(#specname); }                             \
                                                                                                                                                                                        \
		specialization(TraceFlag doTrace) : ChainedException(doTrace) { setExceptionType(#specname); }                                                                                  \
                                                                                                                                                                                        \
		specialization(const SourceLocation& location, const std::string& msg = "") : ChainedException(location, msg) { setExceptionType(#specname); }                                  \
                                                                                                                                                                                        \
		specialization(TraceFlag doTrace, const SourceLocation& location, const std::string& msg = "") : ChainedException(doTrace, location, msg) { setExceptionType(#specname); }      \
                                                                                                                                                                                        \
		specialization(const ChainedException& cause) : ChainedException(cause) {}                                                                                                      \
                                                                                                                                                                                        \
		virtual ~specialization() {}                                                                                                                                                    \
                                                                                                                                                                                        \
	protected:                                                                                                                                                                          \
		specialization& operator=(const specialization& copy) {                                                                                                                         \
			ChainedException::operator=(copy);                                                                                                                                          \
			return *this;                                                                                                                                                               \
		}                                                                                                                                                                               \
	};

#define CHAINED_DERIVATIVET(super, specialization, specname)                                                                                                                 \
	class specialization : public super {                                                                                                                                    \
	public:                                                                                                                                                                  \
		specialization() : super() { setExceptionType(#specname); }                                                                                                          \
                                                                                                                                                                             \
		specialization(const ErrorSpec& errorSpec) : super(errorSpec) { setExceptionType(#specname); }                                                                       \
		specialization(TraceFlag doTrace, const ErrorSpec& errorSpec) : super(doTrace, errorSpec) { setExceptionType(#specname); }                                           \
		specialization(TraceFlag doTrace, const SourceLocation& location, const ErrorSpec& errorSpec) : super(doTrace, location, errorSpec) { setExceptionType(#specname); } \
		specialization(const SourceLocation& location, const ErrorSpec& errorSpec) : super(location, errorSpec) { setExceptionType(#specname); }                             \
                                                                                                                                                                             \
		specialization(TraceFlag doTrace) : super(doTrace) { _exceptionType = #specname; }                                                                                   \
		specialization(const SourceLocation& location, const std::string& msg = "") : super(location, msg) { setExceptionType(#specname); }                                  \
		specialization(TraceFlag doTrace, const SourceLocation& location, const std::string& msg = "") : super(doTrace, location, msg) { setExceptionType(#specname); }      \
		specialization(const ChainedException& cause) : superException(cause) {}                                                                                             \
		virtual ~specialization() {}                                                                                                                                         \
                                                                                                                                                                             \
	protected:                                                                                                                                                               \
		specialization& operator=(const specialization& copy) {                                                                                                              \
			super::operator=(copy);                                                                                                                                          \
			return *this;                                                                                                                                                    \
		}                                                                                                                                                                    \
	};

/**
 * Generalized exception for file operations
 */
CHAINED(FileException);

/**
 * Generalized exception indicating that some key failure has occurred
 */
CHAINED(KeyNotFoundException);

/**
 * Generalized exception indicating that an attempt was made to perform
 * an unsupported operation.
 */
CHAINED(UnsupportedException);

/**
 * Generalized exception indicating that:
 * <ul>
 *  <li>an initialization has occurred.</li>
 *  <li>an attempt was made to operate on an object which has not been initialized</li>
 *  <li>an attempt was made to operate on an object that failed to initialize correctly</li>
 */
CHAINED(UninitializedException);

/**
 * Generally used by reference classes, i.e. a class that serves as a reference to
 * another object via a pointer, to indicate that the reference in invalid. 
 * While this is the typical usage, there is no reason why this can't be thrown
 * any where a null pointer is encountered.
 * Analogous to the exception of the same name in Java.
 */
CHAINED(NullPointerException);

/**
 * Generalized exception for parsing errors
 */
CHAINED(ParseException);

/**
 * Generalized exception for parameter validation
 */
CHAINED(IllegalArgumentException);

/**
 * Generalized exception for failed collection searches/queries.
 */
CHAINED(ElementNotFoundException);

/**
 * Generalized exception for ranged operations, e.g. array access.
 */
CHAINED(RangeException);

/**
 * Generalized exception for configuration operations.
 */
CHAINED(ConfigException);

CHAINED(FatalException);
