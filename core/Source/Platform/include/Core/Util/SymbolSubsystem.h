///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CoreUtil.h"

/**
 * @file
 * Class for managing the initialization of the process symbol handler.
 * The handler is initialized via a call SymInitialize() and cleaned up by
 * by SymCleanup().
 *
 * The API documentation for SymInitialize notes the following:
 *
 *		A process that calls SymInitialize should not 
 *		call it again unless it calls SymCleanup first.
 *
 * This means that not only must we insure that all Init/Cleanup calls
 * are balanced, that we also need to consider if any other system has 
 * initialized the handler.  As it happens, Virtual Leak Detector (VLD)
 * does initialize this handler.
 * 
 * VLD is only active on debug builds.  So in debug builds we need not, indeed,
 * should not manipulate this handler.   It can and will break the
 * output from VLD.
 *
 * To use this class, simply place an instance of SymbolSubsystem on the stack.
 * E.g.:
 *
 *			void main( int argc, char** argv ) {				
 *				SymbolSubsystem ss;
 *				// program does some stuff here.
 *				// and when main() exits, the SymbolSubsystem will go off the
 *				// the stack causing the SymbolSubystem destructor to be called
 *				// which in turn calls SymCleanup().
 *			}
 *
 * Note that if we are building in debug mode, it is assumed that VLD
 * is active and therefore these calls are completely inert.
 */
class COREUTIL_API SymbolSubsystem {
public:
	/**
	 * Default Constructor.
	 */
	SymbolSubsystem();

	/**
	 * Destructor.
	 */
	virtual ~SymbolSubsystem();
};
