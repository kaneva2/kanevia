///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include "Core/Util/CoreUtil.h"
#include "Core/Util/fast_mutex.h"

namespace KEP {
/**
 * This class is intended to be a true singleton, statically
 * linked into one and only one shared library, for the
 * purpose of addressing problems that arise when a
 * compilation is statically linked into two or more
 * dynamically linked libraries and/or a process executable.
 *
 * Consider a singleton implementation foo.  Like most singleton
 * implementations, foo declares a static reference that
 * stores the single instance, hides the constructor for foo
 * and implements a methods that uses the RAII idiom to
 * instantiate the single insance.
 *
 * foo exists in * the static library foo.lib.  Now consider that
 * two dynamic libraries use foo and hence statically link in
 * foo.lib.  This means each dynamic library, will have it's own
 * static reference to the single foo instance, thus breaking
 * the Singleton pattern.
 *
 * ObjectRegistry resolves this issue by proferring the contract
 * that the ObjectReistry is only statically linked once, thus
 * insuring that a traditional RAII based singleton instance of
 * the ObjectRegistry exists.  Once this mechanism is in place
 * a guaranteed singleton can be implemented for other objects
 *
 *
 */

class Destructor {
public:
	Destructor() {}
	Destructor(const Destructor& /*destructable*/) {}
	Destructor& operator=(const Destructor* /*destructable*/) {}
	virtual ~Destructor() {}
	virtual void destroy(void*) = 0;
};

/**
 * A null implementation of Destructable.  That is to say that it
 * satisfies the Destructable interface but it destroys nothing.
 */
class NullDestructor : public Destructor {
public:
	virtual void destroy(void*) {}
};

template <typename DestructableT>
class BasicDestructor : public Destructor {
public:
	BasicDestructor<DestructableT>() {}
	virtual ~BasicDestructor<DestructableT>() {}
	virtual void destroy(void* p) {
		delete ((DestructableT*)p);
	}
};

class COREUTIL_API ObjectRegistry {
public:
	/**
		 * Acquire to well known singleton.
		 */
	static ObjectRegistry& instance();

	/**
		 * Default constructor
		 */
	ObjectRegistry();

	/**
	 	 * dtor
		 */
	virtual ~ObjectRegistry();

	/**
		 * Place a pointer to an object in the registry and and associate
		 * it with the provided key.  Note that this function does update
		 * the item.  Once set, subsequent calls to registerObject for the
		 * same key have no effect on the registry state.
		 *
		 * @param key			Key associated with the item being registered.
		 * @param value			Pointer to object to be referenced by the registry
		 *						as a void*
		 * @param destructor	Pointer to an ObjectRegistry::Destructor implementation.
		 *						If no Destructor is provided operator delete() is called.
		 *						
		 * @return      returns reference to self.
		 */
	ObjectRegistry& registerObject(const std::string& key, void* value, Destructor* destructor = 0);

	/**
		 * Remove a pointer to the object identified by specified key.
		 *
		 * @parm key   finds the object pointer associated with this key
		 * @ret        returns the object pointer associated with key or
		 *             null if not found.
		 */
	void* removeObject(const std::string& key);

	/**
		 * Remove object from registry and deletion logic.
		 * @parm key   finds the object pointer associated with this key
		 */
	void destroyObject(const std::string& key);

	/**
		 * Acquire a pointer to the object identified by specified key.
		 *
		 * @parm key   finds the object pointer associated with this key
		 * @ret        returns the object pointer associated with key or
		 *             null if not found.
		 */
	void* query(const std::string& key);

	/**
		 * Acquire a string representation of the registry.
		 */
	std::string toString();

private:
	friend class ObjectRegistryCleaner;

	// disable the copy constructor
	ObjectRegistry(const ObjectRegistry& toCopy) {}

	typedef std::map<std::string, void*> MapT;
	MapT _map;

	// This sync object is not accessible external to this class,
	// so suppress the warning that clients can't access via dll.
	static fast_recursive_mutex _sync;
	static ObjectRegistry* _instance;
};
} // namespace KEP
