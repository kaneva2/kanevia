///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

namespace KEP {

template <typename T>
inline void AlignedDelete(T* p) {
	p->~T();
	_aligned_free(p);
}

template <typename T>
struct AlignedDeleter {
	void operator()(T* p) const {
		AlignedDelete(p);
	}
};

template <typename T, size_t Alignment, typename... Args>
inline std::unique_ptr<T, AlignedDeleter<T>> AlignedNew(Args&&... args) {
	void* p = _aligned_malloc(sizeof(T), Alignment);
	return std::unique_ptr<T, AlignedDeleter<T>>(new (p) T(std::forward<Args>(args)...));
}

template <typename T, typename... Args>
inline std::unique_ptr<T, AlignedDeleter<T>> AlignedNew(Args&&... args) {
	return AlignedNew<T, alignof(T)>(std::forward<Args>(args)...);
}

// Derive from this class to get aligned operator new and operator new[]
// for your class.
template <size_t alignment>
class AlignedOperatorNewBase {
public:
	static void* operator new(size_t size) {
		void* p = _aligned_malloc(size, alignment);
		if (p)
			return p;
		else
			throw std::bad_alloc();
	}

	static void operator delete(void* p) {
		_aligned_free(p);
	}

	static void* operator new[](size_t size) {
		return operator new(size);
	}

	static void operator delete[](void* p) {
		return operator delete(p);
	}
};

template <typename T>
inline bool IsWeakPtrEmpty(const std::weak_ptr<T>& p) {
	std::weak_ptr<void> pNull;
	return !p.owner_before(pNull) && !pNull.owner_before(p);
}

template <typename T>
struct SafeDeleterThenDelete {
	void operator()(T* p) const {
		if (p) {
			p->SafeDelete();
			delete p;
		}
	}
};

template <typename T>
struct SafeDeleterNoDelete {
	void operator()(T* p) const {
		if (p)
			p->SafeDelete();
	}
};

template <typename T, typename... ConstructorArgs>
inline std::unique_ptr<T, SafeDeleterThenDelete<T>> MakeUniqueSafeDeleteThenDelete(ConstructorArgs... constructorArgs) {
	return std::unique_ptr<T, SafeDeleterThenDelete<T>>(new T(std::forward<ConstructorArgs>(constructorArgs)...));
}

template <typename T, typename... ConstructorArgs>
inline std::unique_ptr<T, SafeDeleterNoDelete<T>> MakeUniqueSafeDeleteNoDelete(ConstructorArgs... constructorArgs) {
	return std::unique_ptr<T, SafeDeleterNoDelete<T>>(new T(std::forward<ConstructorArgs>(constructorArgs)...));
}

} // namespace KEP
