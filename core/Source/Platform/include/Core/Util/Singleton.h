///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include <js.h>
#include "Core/Util/ObjectRegistry.h"

namespace KEP {

/**
 * Helper class for implementing loose singleton semantics. These semantics are 
 * considered loose in that this template provides singleton style access to an
 * instance of a class, it does not enforce the notion that only one instance of that
 * class can exist.
 *
 * Singleton<T> is implemented using using ObjectRegistry.  This means the singularity
 * can not be broken in the same way that singletons defined in static libraries can
 * be broken.  To wit, a singleton that is declared in a static library, but linked
 * in twice, will cause two instances to exist.
 *
 * Usage:
 *
 * Declare your singleton in your class
 *
 *    class MyClass {
 *    public:
 *         MyClass();
 *         static MyClass& singleton();				// A static accessor
 *    private:
 *         static Singleton<MyClass> _singleton;	// A static instance of singleton<MyClass>
 *    };
 *
 * In your implementation (.cpp) file declare your static singleton instance 
 * e.g.:
 *
 *    // This example declares _singleton as a static.  This actually simply
 *    // and optimization
 *    Singleton<MyClass>   MyClass::_singleton( "MyClass" );
 *
 * And implement your singleton accessor
 *
 *    MyClass& MyClass::singleton() { 
 *      // Note the use of dereference operator*.  Singleton
 *      // maintains a reference to the actual object of interest
 *      // Dereferencing like this will return a reference to the
 *		// atual singleton.
 *		//
 *		return (*_singleton);   
 *	  }
 *
 * Limitations:
 * 1)  Currently, this class only supports singletons of classes with an exposed
 *     default constructor.  A Factory would need to be introduced if we 
 *     need to resolve this limitation.
 */
template <class SingletonT>
class Singleton {
public:
	/**
	 * Default  constructor
	 *
	 * The actual subject singleton is kept in the ObjectRegistry with name
	 * 
	 *     Singleton:<typename>
	 *
	 */
	Singleton() :
			_name(std::string("Singleton:") + typeid(SingletonT).name()), _singleton(0) {}

	/**
	 * Construct with an explicit ObjectRegistry key.
	 */
	Singleton(const std::string name) :
			_name(name), _singleton(0) {}

	/**
	 * Defined to insure destructors are called.
	 */
	virtual ~Singleton() {}

	/**
	 * Explicitly acquire reference to the singularity
	 */
	SingletonT& singleton() {
		// If we've already acquired our local reference...then simply return it.
		if (_singleton)
			return *_singleton;

		// else, we've got some work to do because for all we know CGlobalInventoryObjList from one of the other
		// dlls is coming into this call as well....in fact there really isn't a good way to do it, beyond a full
		// blown lease implementation.  One option except to force the issue by calling GetInstance early in the
		// run when concurrency isn't an issue...which while we're in proof of concept...that's the tack
		// that will be taken.  Once the fix is validated, then evaluate a more robust initialization method that
		// leverages the fact that we know the ObjectRegistry is a bonafide singleton.
		//
		// First acquire to our initialization sync object
		//
		std::lock_guard<KEP::fast_recursive_mutex> lock(_sync);

		// now that we've acquired the lock, reevaluate to make sure that nobody
		// constructed our singleton instance during lock acquisition
		//
		if (_singleton)
			return *_singleton;

		// It's possible, as this class is statically linked into multiple dll's, that our pointer
		// to the single instance is null, but that an instance already exists.  Let's look in
		// the ObjectRegistry to find out.
		//
		KEP::ObjectRegistry& registry = KEP::ObjectRegistry::instance();

		_singleton = (SingletonT*)registry.query(_name);

		if (_singleton)
			// Yep, our static instance indicated that an instance had not been instantiated yet...
			// but another dll had already created one.
			//
			return *_singleton;

		// Okay, this is truly the first attempt to get our singleton instance.
		// Create the instance
		//
		_singleton = new SingletonT();

		// Register the instance in the ObjectRegistry
		//
		registry.registerObject(_name, (void*)_singleton
			//									, new KEP::NullDestructor<SingletonT>()     // We should not need to delete as crt does takes cares of these
			,
			new KEP::NullDestructor() // statics automatically
		);

		// And hand the instance off to the user.
		//
		return *_singleton;
	}

	/**
	 * Access singularity as a reference.
	 */
	SingletonT& operator*() { return singleton(); }

	/**
	 * Access singularity as a pointer.
	 */
	SingletonT* operator->() { return &(singleton()); }

private:
	SingletonT* _singleton;
	std::string _name;
	KEP::fast_recursive_mutex _sync;
};

} // namespace KEP