///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

template <typename HandleTraits>
class WindowsHandleWrapper : public HandleTraits {
	typedef typename HandleTraits::HandleType HandleType;

private:
	WindowsHandleWrapper(const WindowsHandleWrapper&) = delete;
	void operator=(const WindowsHandleWrapper&) = delete;

public:
	explicit WindowsHandleWrapper(HandleType h = InvalidHandleValue()) :
			m_handle(h) {}
	WindowsHandleWrapper(WindowsHandleWrapper&& other) :
			m_handle(other.Release()) {}
	WindowsHandleWrapper& operator=(WindowsHandleWrapper&& other) {
		// Protect against self assignment by using temporary.
		WindowsHandleWrapper tmp(std::move(other)); // If *this is other, then *this is now empty.
		DoCloseHandle(m_handle);
		m_handle = tmp.Release();
		return *this;
	}
	virtual ~WindowsHandleWrapper() { Close(); }
	virtual void Set(HandleType h) {
		Close();
		m_handle = h;
	}
	virtual void Close() {
		if (IsValid()) {
			DoCloseHandle(m_handle);
			m_handle = InvalidHandleValue();
		}
	}
	HandleType GetHandle() const { return m_handle; }
	bool IsValid() const {
		return IsValidHandleValue(m_handle);
	}
	explicit operator bool() const { return IsValid(); }
	HANDLE Release() {
		HANDLE h = m_handle;
		m_handle = InvalidHandleValue();
		return h;
	}

private:
	HandleType m_handle;
};

template <typename HandleType_, HandleType_ kInvalidHandleValue>
struct GenericHandleTraits {
	typedef HandleType_ HandleType;
	static bool IsValidHandleValue(HandleType h) { return h != InvalidHandleValue(); }
	static HandleType InvalidHandleValue() { return reinterpret_cast<HandleType>(kInvalidHandleValue); }
};

template <typename HandleType>
struct GDIHandleTraits : public GenericHandleTraits<HandleType, NULL> {
	static bool DoCloseHandle(HandleType h) { return ::DeleteObject(h) != FALSE; }
};

// Some kernel functions return NULL and others return INVALID_HANDLE_VALUE on error.
template <HANDLE kInvalidHandleValue>
struct KernelHandleTraits : public GenericHandleTraits<HANDLE, kInvalidHandleValue> {
	static bool DoCloseHandle(HANDLE h) { return ::CloseHandle(h) != FALSE; }
};

struct SharedMemoryHandleTraits : public KernelHandleTraits<NULL> {};
struct EventHandleTraits : public KernelHandleTraits<NULL> {};
struct ThreadHandleTraits : public KernelHandleTraits<NULL> {};
struct ProcessHandleTraits : public KernelHandleTraits<NULL> {};
struct FileHandleTraits : public KernelHandleTraits<INVALID_HANDLE_VALUE> {};

struct KernelObjectHandleTraits : public KernelHandleTraits<INVALID_HANDLE_VALUE> {};

typedef WindowsHandleWrapper<EventHandleTraits> EventHandleWrapper;
typedef WindowsHandleWrapper<FileHandleTraits> FileHandleWrapper;
typedef WindowsHandleWrapper<ThreadHandleTraits> ThreadHandleWrapper;
typedef WindowsHandleWrapper<ProcessHandleTraits> ProcessHandleWrapper;

template <typename HANDLE_TYPE>
class GDIHandleWrapper : public WindowsHandleWrapper<GDIHandleTraits<HANDLE_TYPE>> {
public:
	explicit GDIHandleWrapper(HANDLE_TYPE h = NULL) :
			WindowsHandleWrapper(h) {}
};

class MappedFileView {
public:
	explicit MappedFileView(void* pMappedMemory = nullptr) :
			m_pMappedMemory(pMappedMemory) {}
	MappedFileView(MappedFileView&& other) :
			m_pMappedMemory(other.m_pMappedMemory) {
		other.m_pMappedMemory = nullptr;
	}
	MappedFileView& operator=(MappedFileView&& other) {
		MappedFileView tmp(other.m_pMappedMemory);
		other.m_pMappedMemory = nullptr;
		Close();
		m_pMappedMemory = tmp.m_pMappedMemory;
		tmp.m_pMappedMemory = nullptr;
		return *this;
	}
	void Set(void* pMappedMemory) {
		Close();
		m_pMappedMemory = pMappedMemory;
	}
	void Close() {
		if (m_pMappedMemory) {
			::UnmapViewOfFile(m_pMappedMemory);
			m_pMappedMemory = nullptr;
		}
	}
	void* GetPointer() const { return m_pMappedMemory; }
	explicit operator bool() const { return m_pMappedMemory != nullptr; }

private:
	void* m_pMappedMemory;
};

class SharedMemory : public WindowsHandleWrapper<KernelHandleTraits<NULL>> {
public:
	explicit SharedMemory(HANDLE h = NULL) :
			WindowsHandleWrapper(h) {}
	~SharedMemory() { Close(); }
#if 0
	void* Map(uint64_t iOffset, size_t length, bool bWritable) {
		void* p = MapViewOfFile(GetHandle(), bWritable ? FILE_MAP_WRITE : FILE_MAP_READ, static_cast<uint32_t>(iOffset >> 32), static_cast<uint32_t>(iOffset&0xffffffff), length);
		if( p )
			m_aMappings.push_back(p);
		return p;
	}
#else
	MappedFileView Map(uint64_t iOffset, size_t length, bool bWritable) {
		void* p = MapViewOfFile(GetHandle(), bWritable ? FILE_MAP_WRITE : FILE_MAP_READ, static_cast<uint32_t>(iOffset >> 32), static_cast<uint32_t>(iOffset & 0xffffffff), length);
		if (p)
			m_aMappings.push_back(p);
		return MappedFileView(p);
	}
#endif
	void Close() {
		for (void* p : m_aMappings)
			UnmapViewOfFile(p);
		m_aMappings.clear();
		WindowsHandleWrapper::Close();
	}
	SharedMemory& operator=(SharedMemory&& other) {
		if (&other != this) {
			Close();
			WindowsHandleWrapper::operator=(std::move(other));
			m_aMappings = std::move(other.m_aMappings);
			other.m_aMappings.clear();
		}
		return *this;
	}

private:
	std::vector<void*> m_aMappings;
};
using WindowsBitmapWrapper = GDIHandleWrapper<HBITMAP>;

// Creates a DC to access a bitmap.
struct BitmapDC {
	BitmapDC(HBITMAP hBitmap) {
		m_hMemDC = NULL;
		m_hOldBitmap = NULL;
		Create(hBitmap);
	}
	~BitmapDC() {
		Clear();
	}
	HDC GetDC() { return m_hMemDC; }
	bool Create(HBITMAP hBitmap) {
		Clear();
		if (!hBitmap)
			return false;
		m_hMemDC = ::CreateCompatibleDC(NULL);
		if (m_hMemDC) {
			m_hOldBitmap = ::SelectObject(m_hMemDC, hBitmap);
			if (m_hOldBitmap)
				return true;
		}
		Clear();
		return false;
	}
	void Clear() {
		if (m_hOldBitmap) {
			::SelectObject(m_hMemDC, m_hOldBitmap);
			m_hOldBitmap = NULL;
		}
		if (m_hMemDC) {
			::DeleteObject(m_hMemDC);
			m_hMemDC = NULL;
		}
	}

private:
	HDC m_hMemDC;
	HGDIOBJ m_hOldBitmap;
};

} // namespace KEP