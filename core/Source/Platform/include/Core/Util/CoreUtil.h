///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef COREUTIL_DLL
#define COREUTIL_API __declspec(dllexport)
#elif defined(STATIC_COREUTIL)
#define COREUTIL_API
#else
#define COREUTIL_API __declspec(dllimport)
#endif

COREUTIL_API void forceCoreUtilLinkage();
