///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

template <typename F, typename Enable>
struct _callableTester : std::false_type {};
template <typename F>
struct _callableTester<F, std::void_t<std::result_of_t<F>>> : std::true_type {};
template <typename F>
struct IsCallable : _callableTester<F, void> {};
template <typename F>
constexpr bool IsCallable_t = IsCallable<F>::value;

// ==============================================================
// Static For Loop (for each enumerate value in range)

// 1) Call a functor (with a static `Call' method) by type
// Leaf call
template <typename RangeType, RangeType rangeBegin, RangeType rangeEnd, template <RangeType, typename...> typename TemplateFunctorType, typename... Args>
inline std::enable_if_t<rangeBegin + 1 == rangeEnd>
ForEachInRange(Args&&... args) {
	TemplateFunctorType<rangeBegin>::Call(args...);
}

// Binary recursion adopted from Mark's RangeForEachSCE_ClientHandler
template <typename RangeType, RangeType rangeBegin, RangeType rangeEnd, template <RangeType, typename...> typename TemplateFunctorType, typename... Args>
inline std::enable_if_t<(rangeEnd - rangeBegin >= 2)>
ForEachInRange(Args&&... args) {
	constexpr RangeType midPoint = RangeType((rangeBegin + rangeEnd) / 2);
	ForEachInRange<RangeType, rangeBegin, midPoint, TemplateFunctorType>(args...); // *NOT* forwarding the rvalues (unsafe to be reused in a loop as pointed out by Mark). Will be a compile error if f has any rvalue ref argument.
	ForEachInRange<RangeType, midPoint, rangeEnd, TemplateFunctorType>(args...);
}

// Null case
template <typename RangeType, RangeType rangeBegin, RangeType rangeEnd, template <RangeType, typename...> typename TemplateFunctorType, typename... Args>
inline std::enable_if_t<(rangeEnd <= rangeBegin)>
ForEachInRange(Args&&... args) {}

// 2) Call a functor (with a static `Call' method) instance by reference
// Leaf call
template <typename RangeType, RangeType rangeBegin, RangeType rangeEnd, typename FunctorType, typename... Args>
inline std::enable_if_t<rangeBegin + 1 == rangeEnd && !IsCallable_t<FunctorType(std::integral_constant<RangeType, rangeBegin>, Args...)>>
ForEachInRange(FunctorType&& f, Args&&... args) {
	f.Call<rangeBegin>(args...);
}

// 3) Call a lambda or function who takes a std::integral_constant as first argument
// Leaf call
template <typename RangeType, RangeType rangeBegin, RangeType rangeEnd, typename Callable, typename... Args>
inline std::enable_if_t<rangeBegin + 1 == rangeEnd && IsCallable_t<Callable(std::integral_constant<RangeType, rangeBegin>, Args...)>>
ForEachInRange(Callable&& f, Args&&... args) {
	f(std::integral_constant<RangeType, rangeBegin>(), args...);
}

// Binary recursion adopted from Mark's RangeForEachSCE_ClientHandler (shared by 2 and 3)
template <typename RangeType, RangeType rangeBegin, RangeType rangeEnd, typename Callable, typename... Args>
inline std::enable_if_t<(rangeEnd - rangeBegin >= 2)>
ForEachInRange(Callable&& f, Args&&... args) {
	constexpr RangeType midPoint = RangeType((rangeBegin + rangeEnd) / 2);
	ForEachInRange<RangeType, rangeBegin, midPoint>(f, args...); // *NOT* forwarding the rvalues (unsafe to be reused in a loop as pointed out by Mark). Will be a compile error if f has any rvalue ref argument.
	ForEachInRange<RangeType, midPoint, rangeEnd>(f, args...);
}

// Null case (shared by 2 and 3)
template <typename RangeType, RangeType rangeBegin, RangeType rangeEnd, typename Callable, typename... Args>
inline std::enable_if_t<(rangeEnd <= rangeBegin)>
ForEachInRange(Callable&& f, Args&&... args) {}
