///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <vector>
#include <future>
#include "Core/Util/ChainedException.h"
#include "Core/Util/Factory.h"
#include "Core/Util/Mutex.h"
#include "Core/Util/fast_mutex.h"
#include "Core/Util/BlockingQueue.h"

#undef min
#undef max

namespace KEP {

// Guarantees:
//
// 1)	Any object sourced to the pool will remain valid even after the pool
//		has been destructed.
// 2)   Provided all references to an object sourced to the pool are destroyed,
//		all objects sourced to the pool will be destroyed.
//
// Nota Bene:
//
// 1) DO NOT store copies of the pooled objects raw pointer value.
//		guaranteed integrity/race condition when object thought
//		to be available for loan, is loaned out while someone else
//		is modifying the object.
// 2) BE AWARE when you are storing references to the pooled object.
//		The object will not be released back to the pool as long as
//		that reference exists.
//
// Future Enhancements
//
// * Fixed size semantics
// * Warm Up/Cool Down logic to be supported by allowing specification of a
//   life cycle manager class that functions both as a factory and a deleter.
//   The factory could create multiple objects at once in anticipation of
//   more requests being made. Furthermore, Deleter can implement logic to
//   free some objects as they become available and thus allow the pool to
//   cool and shrink.  Consider that the life cycle manager can insure that
//   that any given time that at least 25% of the total allocated objects are
//   available for loan.  It would achieve this by allocating as many objects
//   as it needed to assert  that SIZEallocated = SIZEloaned * 1.25.  When
//   ever an object becomes available, if
//
// that allows for a singular object to determine if, when an object comes free
// it is returned to the pool or it's resources are reclaimed.
//
// Limitations
//
// * Very basic pooling mechanism.  Ask and you shall receive.  It will
//   create objects as needed.  I.e. there are no ceiling limitations.
//
// * No explicit collection support.  More a behavior than a limitation, auto
//   collection was a goal of this implementation.  Still, I can imagine
//	 where the could arise for explicit control of the pooled objects.
//   Pooled objects are returned to the pool when the last reference to the
//   object is destructed.
//
// * No resource invalidation.  One goal of these pool implementations is to
//   provide a mechanism where access to the pooled object (read as resource)
//   can be revoke.  This may be functionality for another class entirely

CHAINED(PoolException);

template <typename pooled_t>
class BasicPool {
	// forward reference
	//
	class binding;

	typedef std::promise<pooled_t*> promise_t;
	typedef std::shared_ptr<promise_t> promise_ptr;
	typedef std::future<pooled_t*> future_t;

public:
	// Why 5?  Who knows.
	//
	static const int default_max_threads = 5;

	// short hand for any object loaned out by this pool.
	//
	typedef std::shared_ptr<pooled_t> borrowed_t;

	// short hand for the factory type used by this pool.
	//
	typedef BasicFactory<pooled_t> pool_factory_t;

	/**
	* Construct with a shared pointer to a factory.
	* @param factory		shared_ptr(BasicFactory) This factory may be
	*						accessed when fulfilling requests.
	*/
	BasicPool(std::shared_ptr<pool_factory_t> f, unsigned int max = default_max_threads) :
			_factory(f), _binding(new binding(*this)), _allocated(0), _max(max), m_syncAvailable(), _maxBacklog(0), _recycleCount(0), _borrowCount(0) {}

	virtual ~BasicPool() {
		// When we get destructed our shared_ptr to the binding instance
		// will destruct, thus invalidating all weak_ptr we
		// generated when loaning out widgets.
		//
		// Only clean up those items that are available.
		// all others are out on loan and will be
		// cleaned up automatically
		//
		for (auto o : _available)
			delete o;
	}

	/**
	* Reset the pool.  Any objects out on loan remain
	* valid until the last reference goes out of scope
	* at which point they will be automatically released.
	*/
	virtual BasicPool& reset() {
		// simply clear the
		_allocated.clear();
		for (auto o : _available)
			delete o;
		_available.clear();
		return *this;
	}

	size_t backlog() const {
		return _backlog.size();
	}

	size_t recycled() const {
		return _recycleCount;
	}

	/**
	* Obtain the number of pooled objects in the freestore.
	*/
	virtual unsigned long available() const {
		std::lock_guard<KEP::fast_mutex> Lock(m_syncAvailable);
		return _available.size();
	}

	/**
	* Determine the size of the pool.  This represents the total number of
	* pooled objects allocated.
	*/
	virtual unsigned long size() {
		std::lock_guard<KEP::fast_mutex> Lock(m_syncAllocated);
		return _allocated.size();
	}

	virtual size_t maxbacklog() {
		return _maxBacklog;
	}
	/**
	* Acquire a shared_ptr to an object sourced to this pool.
	* If no objects are available for loan, the factory
	* is used to create a new one.
	*
	* Okay, now we need to modify this block when the pool is
	* exhausted.  exhausted is defined as all allocated instances
	* are on loan and we have hit the maximum pool size.
	*
	* This in and of itself isn't terribly complicated.  However
	* we need to insure that requests are handled in the order they are
	* requested to insure that any waiters aren't inadvertently
	* starved.
	*
	*/
	virtual borrowed_t borrow() {
		_borrowCount.fetch_add(1, std::memory_order_relaxed);
		pooled_t* o;
		promise_ptr pr = checkout();
		future_t futu = pr->get_future();
		o = futu.get();
		return wrap(o);
	}

	size_t borrowed() const {
		return _borrowCount;
	}

protected:
	virtual promise_ptr checkout() {
		pooled_t* o;

		// first see if there is an instance already available
		//
		std::lock_guard<KEP::fast_mutex> availableLock(m_syncAvailable);
		promise_ptr pr(new promise_t());

		if (!_available.empty()) {
			o = _available.back();
			_available.pop_back();
			pr->set_value(o);
			_recycleCount.fetch_add(1, std::memory_order_relaxed);
			return pr;
		}

		// If we got here, there wasn't an instance available
		// Note that the lock on the available bin is still
		// intact.
		//
		int nallocated = _allocated.size();
		if (nallocated < _max) {
			try {
				o = _factory->create();
				std::lock_guard<KEP::fast_mutex> lock(m_syncAllocated);
				_allocated.push_back(o);
			} catch (const std::exception& e) {
				throw PoolException(SOURCELOCATION, ErrorSpec(-1, "Failed to instantiate pooled instance [") << e.what() << "]");
			}
			pr->set_value(o);
			return pr;
		}
		// Okay, there wasn't available and we aren't allow to manufacture any more.
		// Let's make a reservation for the next.
		//
		_backlog.push(pr);
		_maxBacklog = std::max(_maxBacklog, _backlog.size());
		return pr;
	}

	virtual BasicPool& restock(pooled_t* stock) {
		// check reservations
		std::lock_guard<KEP::fast_mutex> availableLock(m_syncAvailable);
		if (_backlog.size() > 0) {
			promise_ptr pr = _backlog.front();
			_backlog.pop();
			pr->set_value(stock);
		} else
			_available.push_back(stock);

		return *this;
	}

private:
	static void collect(pooled_t* c, std::weak_ptr<binding> poolBinding) {
		// Acquire a shared pointer from the weak pointer
		std::shared_ptr<binding> _sh = poolBinding.lock();

		// If we acquired it, then it is safe to call it.  In thise
		// case we will simply query the bound object (our pool) and
		// tell it to restock the widget
		//
		if (_sh.get())
			_sh->pool().restock(c);
		else
			delete c;
	}

	borrowed_t wrap(pooled_t* o) {
		return borrowed_t(o // the object to be shared
			,
			std::bind(&BasicPool::collect, std::placeholders::_1, std::weak_ptr<binding>(_binding)));
	}

	// Shared pointer to our binding.  When we loan objects out,
	// we will specify a Deleter that is passed a weak_ptr to
	// this binding.  When pooled objects are being deleted,
	// this Deleter will be called.  The deleter will attempt
	// to lock the weak_ptr.  If the lock is successful, it
	// indicates that this pool still exists and a restocck call
	// is forwarded to the pool through the binding.  If the
	// lock attempt fails, it means that the object has out lived
	// the pool that created it.  In this case it's resources
	// are reclaimed.
	//
	std::shared_ptr<binding> _binding;

	// Holds raw pointer to all objects allocated.
	// Predominantly exists for not losing complete track of the objects
	// and also, allows us to detect saturation.
	//
	std::vector<pooled_t*> _allocated;

	// Holds raw pointer to all objects available for loan.  We check this
	// collection first when a request is made for a pooled object.
	//
	std::vector<pooled_t*> _available;

	// Shared pointer to the factory.  When an object is requested and
	// none are available, the pool will use this factory to fabricate
	// a new object
	//
	std::shared_ptr<pool_factory_t> _factory;

	unsigned int _max;
	KEP::fast_mutex m_syncAllocated;
	mutable KEP::fast_mutex m_syncAvailable;
	KEP::SynchronizedQueue<promise_ptr> _backlog;
	size_t _maxBacklog;
	std::atomic<size_t> _recycleCount;
	std::atomic<size_t> _borrowCount;

	// very simple class to bind an object.
	// By capturing a shared_ptr of this
	// class we can issue weak_ptrs to this class
	//
	// Exercising logic acquires shared_ptr<binding>
	// via weak_ptr<binding>.lock() and accesses
	// the bound object via the bound() operator.
	//
	// Intended usage is as follows:
	//
	//  shared_ptr<binding> shared(new binding());
	//  ...
	//	...
	//  weak_ptr<binding>   weak(shared);
	//  shared_ptr<binding> locked = weak.lock();
	//  if ( locked )
	//       locked->bound().foo()
	//
	class binding {
	public:
		binding(BasicPool<pooled_t>& pool) :
				_pool(pool) {}

		BasicPool<pooled_t>& pool() { return _pool; }

	private:
		BasicPool<pooled_t>& _pool;
	};
};

} // namespace KEP