///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <stack>
#include "Core/Util/SynchronizedQueue.h"
#include "Core/Util/Runnable.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/AsynchronousCompletionToken.h"
#include "Core/Util/Executor.h"
#include "Core/Util/Accumulator.h"
#include "Common/KEPUtil/Plugin.h"

namespace KEP {

/**
 * Mechanism for managing a set of Threads and allocating work to those threads
 *
 * Implemented using the Executor class.
 *
 * Prioritization and Thread Type.  The priority and type of threads managed by the 
 * thread pool is homogenous.  Support for multiple priorities and thread types
 * is implemented by creating multiple ThreadPool instances, each with the desired
 * priority/type.
 * 
 */
class ThreadPool {
public:
	typedef std::shared_ptr<ThreadPool> ptr_t;

	enum RunState {
		cold // not started yet.
		,
		starting,
		running // started
		,
		shuttingDown // shutdown in progress
		,
		closed // shutdown complete
	};

	enum ShutdownMode {
		none,
		ponderous // not implemented.   Doesn't do anything until all work is
		// completed while allowing for new tasks to be queued.

		,
		patient // implemented to facilitate unit testing.
		// Disallows further submissions.

		,
		hurried // implemented for real life.
		// Disallows further submissions.
		// Releases all pending tasks (i.e. only active tasks are completed, pending tasks are not)

		,
		murderous // work in progress
		// Disallows further submissions
		// Releases all pending tasks
		// Kills processing on all active tasks.
	};

	/**
	 * Constructor
	 * @param sz		The number of threads to be managed.  Note that the thread
	 *					pool creates the threads internally (i.e. the consumer of 
	 *					ThreadPool does not get to do this.
	 * @param name		Optional name for the thread pool.  Handy for debugging and
	 *					logging purposes.  Note that this name is used when 
	 *					constructing a name for each of the Executors, which is
	 *					displayed as the thread name in the debugger.
	 * @param tprio		The priority assigned to all threads in this pool.  Note that
	 *					a single pool can only manage threads of the same priority,
	 *				    i.e. there is no prioritization implemented in this class.
	 *					(see jsThreadTypes.h)
	 */
	ThreadPool() {}

	virtual ~ThreadPool() {}

	/**
	 * Start the individual threads in the pool
	 */
	virtual ACTPtr start() = 0;

	/**
	 * Shutdown the thread pool.  Note: the current implementation does not
	 * support restarting the pool.
	 *
	 * @param mode		Specifies the urgency of the shutdown.  Possible
	 *					values include:
	 * 
	 *					ponderous		not implemented.   Doesn't do anything until all work is 
     *									completed while allowing for new tasks to be queued.
	 *
	 *					patient			implemented to facilitate unit testing.  
	 *									Disallows further submissions.
	 *
	 *					hurried			implemented for real life.  
	 *									Disallows further submissions.
	 *									Releases all pending tasks (i.e. only active tasks are completed, pending tasks are not)
	 *
	 *					murderous		work in progress
	 *									Disallows further submissions
	 *									Releases all pending tasks
	 *									Kills processing on all active tasks.
	 * @returns			CompletionToken.  This call is asynchronous.  If you need to block on the shutdown,  use the ACT.
	 *										(see AsynchronousCompletionToken.h)
	 */
	virtual ACTPtr shutdown(ShutdownMode mode = hurried) = 0;

	virtual ThreadPool& setVerbose(bool v) = 0;

	/**
	 * Acquire the number of pending tasks to be executed.
	 */
	virtual size_t pending() = 0;

	/**
	 * Acquire the number of threads that are currently idled.  Note
	 * that there is no "working()" counter part.  We could add one
	 * convenience, but it would simply be implemented as
	 *
	 *       capacity() - idle()
	 */
	virtual size_t idle() = 0;

	/**
	 * Acquire the capacity of this thread pool. Specifically how many threads does the pool
	 * manage.
	 */
	virtual size_t capacity() const = 0;

	/**
	 * Acquire the number of tasks processed since the pool was started.
	 */
	virtual size_t processed() = 0;

	/**
	 * Acquire a constant reference to the name of this thread pool.
	 */
	virtual const std::string& name() = 0;

	/**
	 * Acquire a string representation of this threadpools current state.
	 */
	virtual std::string toString() = 0;

	/**
	 * Return this pool's shutdown mode.  If the pool is not in the process
	 * or has completed shutdown, this will return shutdownMode::none.
	 */
	virtual ShutdownMode shutdownMode() const = 0;

	/**
     * Return this pool's runstate.
     */
	virtual RunState runState() const = 0;

	/**
	 * Place a "Runnable" task in the ThreadPool's input queue.
	 */
	virtual ACTPtr enqueue(RunnablePtr runnable) = 0;

	virtual ACTPtr enqueue(std::function<RunResultPtr()> func) = 0;
};

class AbstractThreadPool : public ThreadPool {
};

/**
 * Mechanism for managing a set of Threads and allocating work to those threads
 *
 * Implemented using the Executor class.
 *
 * Prioritization and Thread Type.  The priority and type of threads managed by the 
 * thread pool is homogenous.  Support for multiple priorities and thread types
 * is implemented by creating multiple ThreadPool instances, each with the desired
 * priority/type.
 * 
 */
class ConcreteThreadPool : public AbstractThreadPool {
public:
	/**
	 * Constructor
	 * @param sz		The number of threads to be managed.  Note that the thread
	 *					pool creates the threads internally (i.e. the consumer of 
	 *					ThreadPool does not get to do this.
	 * @param name		Optional name for the thread pool.  Handy for debugging and
	 *					logging purposes.  Note that this name is used when 
	 *					constructing a name for each of the Executors, which is
	 *					displayed as the thread name in the debugger.
	 * @param tprio		The priority assigned to all threads in this pool.  Note that
	 *					a single pool can only manage threads of the same priority,
	 *				    i.e. there is no prioritization implemented in this class.
	 *					(see jsThreadTypes.h)
	 */
	ConcreteThreadPool(unsigned int sz, const std::string& name = std::string(""), jsThdPriority tprio = PR_NORMAL, jsThreadType ttype = TT_NORMAL) :
			_executors(), _runState(cold), _shutdownMode(none), _name(name), _processed(), _shutdownToken(new ACT(RunnablePtr(new NoopRunnable()))), _termCount(0), _startupToken(new ACT(RunnablePtr(new NoopRunnable()))), _startCount(0), _verbose(false) {
		for (unsigned int n = 0; n < sz; n++) {
			std::stringstream strstream;
			strstream << _name << "[" << n << "]";
			jsThreadExecutor* executor = new jsThreadExecutor(strstream.str(), tprio, ttype);
			ExecutorPtr ptr(executor);
			auto listener = std::make_shared<PooledExecutorListener>(*this, ptr);
			ptr->addListener(listener);
			_executors.push_back(ptr);
		}
	}

	// TODO: Move to a different location in this class def.
	class PonderousArrestor : public jsThreadExecutor::Arrestor {
	public:
		PonderousArrestor(ConcreteThreadPool& tp) :
				_tp(tp), jsThreadExecutor::Arrestor() {}

		PonderousArrestor(const PonderousArrestor& other) :
				jsThreadExecutor::Arrestor(other), _tp(other._tp) {}

		virtual ~PonderousArrestor() {}

		virtual PonderousArrestor& operator=(const PonderousArrestor&) { return *this; }

		virtual RunResultPtr run() override {
			if (_tp.pending() == 0)
				_tp.enqueue(RunnablePtr(new jsThreadExecutor::Arrestor()));
			else
				_tp.enqueue(RunnablePtr(new PonderousArrestor(*this)));
			return RunResultPtr();
		}

	private:
		ConcreteThreadPool& _tp;
	};

	virtual ~ConcreteThreadPool() {
		// In the event that the pool wasn't shutdown, do it now.
		shutdown()->wait();

		// One question, unresolved is should the constructor attempt
		// to shutdown the thread pool?  The complication comes from
		// the fact that we may not want to wait for the shut down,
		// but if it is done here, we must wait for it or we risk serious
		// destabilization.
		//
		// So, for now, keep it simple and put the onus on the ThreadPool
		// consumer to shut the pool down prior to collection of the
		// thread pool resources.

		// We have a mutual reference between each executor and it's listener.
		// As these are both counted references, the executor and listener
		// will not get deleted unless we break this embrace.
		//
		// So, do make sure everything get's removed, reset the listeners.
		//
		// cout << "Pending: " << pending();
		for (unsigned int n = 0; n < _executors.size(); n++) {
			auto listener = std::make_shared<Executor::DefaultListener>();
			_executors[n]->addListener(listener);
		}
	}

	/**
	 * Start the individual threads in the pool
	 */
	ACTPtr start() {
		_runState = starting;
		size_t l = _executors.size();
		for (unsigned int n = 0; n < l; n++) _executors[n]->start();
		_timer.Start();
		return _startupToken;
	}

	/**
	 * Shutdown the thread pool.  Note: the current implementation does not
	 * support restarting the pool.
	 *
	 * @param mode		Specifies the urgency of the shutdown.  Possible
	 *					values include:
	 * 
	 *					ponderous		not implemented.   Doesn't do anything until all work is 
     *									completed while allowing for new tasks to be queued.
	 *
	 *					patient			implemented to facilitate unit testing.  
	 *									Disallows further submissions.
	 *
	 *					hurried			implemented for real life.  
	 *									Disallows further submissions.
	 *									Releases all pending tasks (i.e. only active tasks are completed, pending tasks are not)
	 *
	 *					murderous		work in progress
	 *									Disallows further submissions
	 *									Releases all pending tasks
	 *									Kills processing on all active tasks.
	 * @returns			CompletionToken.  This call is asynchronous.  If you need to block on the shutdown,  use the ACT.
	 *										(see AsynchronousCompletionToken.h)
	 */
	ACTPtr shutdown(ShutdownMode mode = hurried) {
		LOG4CPLUS_TRACE(log4cplus::Logger::getInstance(L"ThreadPool"), "ThreadPool[" << _name << "]::shutdown(" << mode << ")");

		if (_runState == cold) {
			// mark the token such that it won't block on wait.
			//
			_shutdownToken->complete();
			return _shutdownToken;
		}

		// This pool has already been shutdown
		//
		if (_runState == shuttingDown || _runState == closed) {
			log4cplus::Logger logger = log4cplus::Logger::getInstance(L"ThreadPool");
			LOG4CPLUS_INFO(logger, "ThreadPool(" << name() << ")::shutdown(): Already shutdown!");
			if (logger.getLogLevel() == log4cplus::TRACE_LOG_LEVEL) {
				StackTrace st;
				LOG4CPLUS_TRACE(logger, st.str().c_str());
			}
			_shutdownToken->complete();
			return _shutdownToken;
		}

		// block incoming jobs
		_shutdownMode = mode;
		_runState = shuttingDown;

		switch (_shutdownMode) {
			case murderous:
				clearPending();
				for (unsigned int n = 0; n < capacity(); n++) _executors[n]->kill();
				break;
			case hurried:
				LOG4CPLUS_DEBUG(log4cplus::Logger::getInstance(L"ThreadPool"), "ThreadPool[" << _name << "]::shutDown(hurried)");
				clearPending();

				LOG4CPLUS_DEBUG(log4cplus::Logger::getInstance(L"ThreadPool"), "ThreadPool[" << _name << "]::shutDown(): Queueing arrestors");
				for (unsigned int n = 0; n < capacity(); n++)
					enqueueInternal(RunnablePtr(new jsThreadExecutor::Arrestor()));
				break;
			case patient:
				for (unsigned int n = 0; n < capacity(); n++) enqueueInternal(RunnablePtr(new jsThreadExecutor::Arrestor()));
				break;
			case ponderous:
				for (unsigned int n = 0; n < capacity(); n++) enqueueInternal(RunnablePtr(new PonderousArrestor(*this)));
				break;
		};
		return _shutdownToken;
	}

	ConcreteThreadPool& setVerbose(bool v) {
		_verbose = v;
		return *this;
	}

	/**
	 * Acquire the number of pending tasks to be executed.
	 */
	size_t pending() { return _inputQueue.size(); }

	/**
	 * Acquire the number of threads that are currently idled.  Note
	 * that there is no "working()" counter part.  We could add one
	 * convenience, but it would simply be implemented as
	 *
	 *       capacity() - idle()
	 */
	size_t idle() { return _idlers.size(); }

	/**
	 * Acquire the capacity of this thread pool. Specifically how many threads does the pool
	 * manage.
	 */
	size_t capacity() const { return _executors.size(); }

	/**
	 * Acquire the number of tasks processed since the pool was started.
	 */
	size_t processed() { return _processed.count(); }

	/**
	 * Acquire a constant reference to the name of this thread pool.
	 */
	const std::string& name() { return _name; }

	/**
	 * Acquire a string representation of this threadpools current state.
	 */
	std::string toString() {
		std::stringstream ss(name());
		std::string n = name();
		ss << "ThreadPool[name=\"" << name()
		   << "\";pending=" << _inputQueue.size()
		   << ";executors=" << _executors.size()
		   << ";idle=" << _idlers.size() << ";];" << std::endl;
		for (std::vector<ExecutorPtr>::iterator i = _executors.begin(); i != _executors.end(); i++) {
			ss << (*i)->toString();
		}
		return ss.str();
	}

	/**
	 * Return this pools shutdown mode.  If the pool is not in the process
	 * or has completed shutdown, this will return shutdownMode::none.
	 */
	ShutdownMode shutdownMode() const { return _shutdownMode; }

	/**
     * Return this pool's runstate.
     */
	virtual RunState runState() const { return _runState; }

	ACTPtr enqueue(std::function<RunResultPtr()> func) {
		return enqueue(RunnablePtr(new FunctionRunner(func)));
	}

	/**
	 * Place a "Runnable" task in the ThreadPool's input queue.
	 */
	ACTPtr enqueue(RunnablePtr runnable) {
		// Jobs can be queued prior to start, just not after the pool has been told
		// not to shut down, in which case the pool is in the state of shutting down
		// or is closed.
		//
		if (_runState == shuttingDown && _shutdownMode == ponderous) {
			return enqueueInternal(runnable);
		}

		if (_runState == shuttingDown || _runState == closed) {
			// Pool is in the process of shutting down or is already shutdown
			// We don't want anyone blocking waiting for something that is
			// never going to happen.
			//
			// consider: throw an exception
			ACT* pact = new ACT(runnable);
			pact->complete();
			return ACTPtr(pact);
		}

		return enqueueInternal(runnable);
	}

private:
	/**
	 * Place a "Runnable" task in the ThreadPool's input queue.
	 */
	ACTPtr enqueueInternal(RunnablePtr runnable) {
		// Jobs can be queued prior to start, just not after the pool has been told
		// not to shut down, in which case the pool is in the state of shutting down
		// or is closed.
		//
		if (_verbose)
			std::cout << "ThreadPool::enqueue()" << std::endl;
		std::lock_guard<fast_recursive_mutex> lock(_sync);

		// Now create an Executable with this ACTPtr
		//
		ExecutablePtr exePtr(new Executable(runnable));

		// Get a shared pointer to the act now...it is possible
		// that the task would finish execution, the executor
		// gets deleted, thus removing the final reference to
		// the act before we get to the return...
		//
		ACTPtr ret = exePtr->act();

		// Wrap the runnable up in an Executable object on the heap
		//
		if (_idlers.size() > 0) {
			// this task directly to one of the tasks
			//
			ExecutorPtr executor = _idlers.top();
			_idlers.pop();
			executor->execute(exePtr);
		} else {
			// put this runnable in our input queue.
			// The next executor that comes available will pick off the first item in this queue (LIFO).
			//
			_inputQueue.push(exePtr);
		}
		return ret;
	}

	/**
	 * Implementation of ExecutorListener.  Executors can exist
	 * outside of the scope of a ThreadPool, and hence know nothing
	 * about the mechanics of a thread pool (e.g. how are the executors
	 * managed.)  However, the thread pool must be aware of when an
	 * executor has completed a task so that it assign more work.
	 * 
	 * This listener is the pool is made aware of this without hard
	 * coding logic specific to pooled executors inside the executor
	 * class.
	 *
	 * In this case, the listener simply notifies the thread pool that
	 * the executor has been idled by calling ThreadPool::executorIdled()
	 */
	class PooledExecutorListener : public Executor::Listener {
	public:
		ExecutorPtr _executor;
		ConcreteThreadPool& _threadPool;

		PooledExecutorListener(ConcreteThreadPool& threadPool, ExecutorPtr executor) :
				_executor(executor), _threadPool(threadPool) {
			//cout << "PooledExecutorListener::PooledExecutorListener()" << endl;
		}

		virtual ~PooledExecutorListener() {
			//cout << "PooledExecutorListener::~PooledExecutorListener()" << endl;
		}

		virtual PooledExecutorListener& operator=(const PooledExecutorListener&) { return *this; }

		void stateChanged(Executor& executor) {
			// cout << "ThreadPool::PooledExecutorListener::stateChanged()[" << executor.getState() << "]" << endl;
			switch (executor.state()) {
				case Executor::running: _threadPool.executorStarted(_executor); break;
				case Executor::polling: _threadPool.executorIdled(_executor); break;
				case Executor::pending: break;
				case Executor::busy: break;
				case Executor::stopped: _threadPool.executorTerminated(_executor); break;
			}
		}
	};

	void executorStarted(ExecutorPtr executor) {
		//	cout << "ThreadPool[" << _name << "]::executorStarted[" << executor->name() << "]()" << endl;

		// if we haven't terminated all executors...bug out
		if (++_startCount < _executors.size())
			return;

		// all executors are now dead.
		// Change the pool state
		// stop the main timer.
		// Release any waiters on the shutdown token.
		//
		_runState = running;
		//		_timer.start();
		//		cout << "Releasing waiters!" << endl;
		_startupToken->complete();
	}

	void executorTerminated(ExecutorPtr executor) {
		// cout << "ThreadPool[" << _name << "]::executorTerminated[" << executor->name() << "](): Pending [" << pending() << "]"   << endl;

		// if we haven't terminated all executors...bug out
		if (++_termCount < _executors.size())
			return;

		// all executors are now dead.
		// Change the pool state
		// stop the main timer.
		// Release any waiters on the shutdown token.
		//
		_runState = closed;
		_timer.Pause();
		//cout << "Releasing waiters!" << endl;
		_shutdownToken->complete();
	}

	/**
	 * We associate a ThreadPool listener with every instance
	 * of thread pool that listens specifically for Executors
	 * being idled (completing a task).  This listener calls
	 * this function to actually handle idling the executor.
	 *
	 * @param executor The executor being idled.
	 */
	ConcreteThreadPool& executorIdled(ExecutorPtr executor) {
		std::lock_guard<fast_recursive_mutex> lock(_sync);

		if (_shutdownMode == murderous)
			return *this;

		unsigned long timeMs = (unsigned long)executor->timer().ElapsedMs();
		if (_processed.accumulate(timeMs).count() % 1000 == 0) {
			_timer.Reset();
		}

		// Is there any work available?
		if (_inputQueue.size() > 0) {
			// There's work to be done...send the executor on it's merry way.
			ExecutablePtr executable = _inputQueue.front();
			_inputQueue.pop();

			executor->execute(executable);
		} else {
			_idlers.push(executor);
		}
		return *this;
	}

	/**
	 * Clear any pending tasks.  Used during hurried/murderous shutdown
	 */
	void clearPending() {
		// cout << "ThreadPool::clearPending()" << endl;
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		while (!_inputQueue.empty()) _inputQueue.pop();
	}

	ShutdownMode _shutdownMode;
	RunState _runState;

	SynchronizedQueue<ExecutablePtr> _inputQueue; // FIFO queue of tasks to be performed.
	fast_recursive_mutex _sync; // For protection of internal resources e.g. executors, idlers etc.
	std::vector<ExecutorPtr> _executors; // vector of all available executors idle or busy.

	std::stack<ExecutorPtr> _idlers; // stack of executors that are not doing anything.
	std::string _name; // Mostly for debugging/testing/monitoring.  The name of this pool.

	ACTPtr _startupToken;
	unsigned long _startCount;

	ACTPtr _shutdownToken; //
	unsigned long _termCount;

	Accumulator<unsigned long> _processed;
	Timer _timer;

	bool _verbose;
};
#if 0
// Plugin support
class ThreadPoolPlugin 
	:	public ThreadPool               // Use composition instead of inheritance here.
		, public AbstractPlugin {
public:
	ThreadPoolPlugin(ThreadPool& impl)
		: _impl(impl)
		, AbstractPlugin("ThreadPool")   
	{}

	ThreadPoolPlugin(const ThreadPoolPlugin& other )
		:	_impl(other._impl)
			, AbstractPlugin( "ThreadPool" )
			, ThreadPool(other._impl)
	{	}

	ThreadPool& operator=(const ThreadPoolPlugin& ) { throw UnsupportedException();				}

	virtual ~ThreadPoolPlugin()	
	{ delete (&_impl);								}

	virtual ACTPtr start() 												
	{ return _impl.start();							}

	virtual ACTPtr shutdown( ShutdownMode mode = hurried )
	{ return _impl.shutdown( mode);					}

	// Plugin support
	//
	const	std::string&	name()			
	{ return AbstractPlugin::name();				}

	unsigned int			version()		
	{ return AbstractPlugin::version();				}

	Plugin&					startPlugin()			{ 
		_impl.start();
		return AbstractPlugin::startPlugin();	
	}

	ACTPtr					stopPlugin()			{ 
		return shutdown();
	}

	virtual ThreadPool& setVerbose(bool v)				{ return _impl.setVerbose(v);		}

	/**
	 * Acquire the number of pending tasks to be executed.
	 */
	virtual size_t pending()							{ return _impl.pending();			}

	/**
	 * Acquire the number of threads that are currently idled.  Note
	 * that there is no "working()" counter part.  We could add one
	 * convenience, but it would simply be implemented as
	 *
	 *       capacity() - idle()
	 */
	virtual size_t idle()								{ return _impl.idle();				}

	/**
	 * Acquire the capacity of this thread pool. Specifically how many threads does the pool
	 * manage.
	 */
	virtual size_t capacity() const						{ return _impl.capacity();			}

	/**
	 * Acquire the number of tasks processed since the pool was started.
	 */
	virtual size_t processed()							{ return _impl.processed();			}

	/**
	 * Acquire a string representation of this threadpools current state.
	 */
	virtual std::string toString()						{ return _impl.toString();			}

	/**
	 * Return this pools shutdown mode.  If the pool is not in the process
	 * or has completed shutdown, this will return shutdownMode::none.
	 */
	virtual ShutdownMode shutdownMode() const			{ return _impl.shutdownMode();		}

    /**
     * Return this pool's runstate.
     */
    virtual RunState runState() const                   { return _impl.runState();          }

	/**
	 * Place a "Runnable" task in the ThreadPool's input queue.
	 */
	virtual ACTPtr enqueue( RunnablePtr runnable  )		{ return _impl.enqueue(runnable);	}	
	virtual ACTPtr enqueue(std::function<RunResultPtr ()> func) { return _impl.enqueue(func); }
private:
	ThreadPool& _impl;
};

class __declspec(dllexport) ThreadPoolPluginFactory : public AbstractPluginFactory {
public:
	ThreadPoolPluginFactory();
	ThreadPoolPluginFactory( KEPConfig* config );
	virtual	~ThreadPoolPluginFactory();

	const std::string&	name() const;
	PluginPtr			createPlugin( const TIXMLConfig::Item& config ) const;
private:
	KEPConfig* _config;
};
#endif
} // namespace KEP
