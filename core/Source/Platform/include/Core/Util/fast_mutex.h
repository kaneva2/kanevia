///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Not all uses of this file #include Windows.h first. We need for the synchronization primitives.
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN // Exclude rarely-used stuff from Windows headers
#endif
#include <Windows.h>

#include <atomic>
#include <mutex> // Make sure lock_guard et al are available when including this file.

//#define JS_TEST
#ifdef JS_TEST
#include <../../Tools/jsTools-4.0.1/js.h>
#include <../../Tools/jsTools-4.0.1/jsSync.h>
class js_mutex {
	js_mutex(const js_mutex&) = delete;
	const js_mutex& operator=(const js_mutex&) = delete;

public:
	js_mutex() {
	}
	~js_mutex() {
	}
	void lock() {
		m_jsSync.lock();
	}
	/*
	bool try_lock() {
		return m_jsSync.tryLock();
	}
	*/
	void unlock() {
		m_jsSync.unlock();
	}

	//bool is_locked() const { return m_jsSync.peekLock(); }
private:
	jsFastSync m_jsSync;
};
namespace KEP {
class fast_recursive_mutex : public js_mutex {};
class fast_mutex : public js_mutex {};
} // namespace KEP
#else
namespace KEP {

class fast_mutex {
	fast_mutex(const fast_mutex&) = delete;
	const fast_mutex& operator=(const fast_mutex&) = delete;

public:
	fast_mutex() {
		m_hEvent = ::CreateEvent(nullptr, FALSE, FALSE, nullptr);
	}
	~fast_mutex() {
		::CloseHandle(m_hEvent);
	}
	void lock() {
		size_t nOldLocks = m_nLocks.fetch_add(1, std::memory_order_acquire);
		if (nOldLocks != 0)
			::WaitForSingleObject(m_hEvent, INFINITE);
#if defined(_DEBUG)
		m_dwOwningThreadId = ::GetCurrentThreadId();
#endif
	}
	bool try_lock() {
		// Avoid (relatively) expensive fetch_add when locked.
		if (m_nLocks.load(std::memory_order_relaxed) > 0)
			return false;

		size_t nOldLocks = m_nLocks.fetch_add(1, std::memory_order_acquire);
		if (nOldLocks == 0) {
#if defined(_DEBUG)
			m_dwOwningThreadId = ::GetCurrentThreadId();
#endif
			return true;
		}

		size_t nBeforeRelease = m_nLocks.fetch_add(-1, std::memory_order_acquire);
		if (nBeforeRelease == 1)
			::SetEvent(m_hEvent);
		return false;
	}
	void unlock() {
#if defined(_DEBUG)
		m_dwOwningThreadId = 0;
#endif
		size_t nOldLocks = m_nLocks.fetch_add(-1, std::memory_order_release);
		//assert(nOldLocks != 0);
		if (nOldLocks != 1)
			::SetEvent(m_hEvent); // Documented to waken a single thread.
	}

	// Some functions helpful for debugging.

	// Number of threads trying to lock this mutex (including this one).
	size_t current_users() const {
		return m_nLocks.load(std::memory_order_relaxed);
	}

	bool is_locked() const {
		return current_users() > 0;
	}

private:
	std::atomic<size_t> m_nLocks = 0;
	HANDLE m_hEvent = INVALID_HANDLE_VALUE;

#if defined(_DEBUG)
	DWORD m_dwOwningThreadId = 0;
#endif
};

class fast_recursive_mutex {
	fast_recursive_mutex(const fast_recursive_mutex&) = delete;
	const fast_recursive_mutex& operator=(const fast_recursive_mutex&) = delete;

public:
	fast_recursive_mutex() {
		m_hEvent = ::CreateEvent(nullptr, FALSE, FALSE, nullptr);
	}
	~fast_recursive_mutex() {
		::CloseHandle(m_hEvent);
	}
	void lock() {
		if (m_dwOwningThreadId.load(std::memory_order_relaxed) == ::GetCurrentThreadId()) {
			++m_iRecursionCount;
			return;
		}

		size_t nOldLocks = m_nLocks.fetch_add(1, std::memory_order_acquire);
		if (nOldLocks != 0)
			::WaitForSingleObject(m_hEvent, INFINITE);
		m_dwOwningThreadId = ::GetCurrentThreadId();
	}
	bool try_lock() {
		if (m_dwOwningThreadId.load(std::memory_order_relaxed) == ::GetCurrentThreadId()) {
			++m_iRecursionCount;
			return true;
		}

		// Avoid (relatively) expensive fetch_add when locked.
		if (m_nLocks.load(std::memory_order_relaxed) > 0)
			return false;

		size_t nOldLocks = m_nLocks.fetch_add(1, std::memory_order_acquire);
		if (nOldLocks == 0) {
			m_dwOwningThreadId = ::GetCurrentThreadId();
			return true;
		}

		size_t nBeforeRelease = m_nLocks.fetch_add(-1, std::memory_order_acquire);
		if (nBeforeRelease == 1)
			::SetEvent(m_hEvent);
		return false;
	}
	void unlock() {
		//assert(m_dwOwningThreadId.load(std::memory_order_relaxed) == ::GetCurrentThreadId());
		if (m_iRecursionCount > 0) {
			--m_iRecursionCount;
			return;
		}
		m_dwOwningThreadId.store(0, std::memory_order_relaxed);
		size_t nOldLocks = m_nLocks.fetch_add(-1, std::memory_order_release);
		//assert(nOldLocks != 0);
		if (nOldLocks != 1)
			::SetEvent(m_hEvent); // Documented to waken a single thread.
	}

	// Some functions helpful for debugging.

	// Number of threads trying to lock this mutex (including this one).
	size_t current_users() const {
		return m_nLocks.load(std::memory_order_relaxed);
	}

	bool is_locked() const {
		return current_users() > 0;
	}

	// Only safe to call on the thread owning the mutex.
	size_t recursion_count() const {
		return m_iRecursionCount;
	}

	size_t owned_by_this_thread() {
		return m_dwOwningThreadId.load(std::memory_order_relaxed) == ::GetCurrentThreadId();
	}

private:
	std::atomic<size_t> m_nLocks = 0;
	HANDLE m_hEvent = INVALID_HANDLE_VALUE;

	std::atomic<DWORD> m_dwOwningThreadId = 0;
	size_t m_iRecursionCount = 0;
};

} // namespace KEP
#endif
