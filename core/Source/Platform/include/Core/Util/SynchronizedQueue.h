///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <queue>
#include "Core/Util/fast_mutex.h"

namespace KEP {

/**
 * A thread safe queueing mechanism.
 *
 * One issue is the behavior of the underlying queue container implementations
 * (e.g. deque<T>, queue<T> etc) to call the destructor when objects are
 * popped of the queue.  This means that the pattern used to fetch and
 * remove an element from a queue is as follows:
 *
 *        myobj = queue.front();	// notice the lack of reference to force 
 *									// call of assignment op.
 *        queue.pop();              // now remove the element from the list
 *       
 * This is a serious drawback because it means that other threads can 
 * access the queue between the operations.  The interloper may not get what 
 * they expect.
 *
 * There are a couple of ways to address this.
 *
 * pattern 1:
 *       queue.getAndPop( objectRef );
 *
 * pattern 2:
 *        queue.acquire();
 *        queue.front();
 *        queue.pop();
 *        queue.release();
 *	
 * Has complication of exception handling when queue is empty.  e.g. it
 * will throw an exception and care must be taken to remove the lock.
 *
 * pattern 3:
 *        {
 *				QueueLock ql = queue.acquire();
 *				queue.front();
 *				queue.pop();
 *		  }  // ql rolls off the stack and the lock is released.
 *
 */
template <typename T>
class SynchronizedQueue {
public:
	/**
	 * Default constructor.
	 */
	SynchronizedQueue<T>() :
			_impl(), _sync() {}

	virtual ~SynchronizedQueue<T>() {}

	/**
	 * Push a new instance of T on to the queue.
	 */
	SynchronizedQueue<T>& push(const T& x) {
		// cout << "SynchronizedQueue::push() Acquiring lock" << endl;
		std::lock_guard<fast_recursive_mutex> lock(_sync);

		// cout << "SynchronizedQueue::push() Lock acquired" << endl;
		_impl.push(x);
		return *this;
	}

	/**
	 * Class for externalizing the synchronizing element of the
	 * SynchronizedQueue.
	 */
	class Lock {
	public:
		/**
		 * Construct a lock with an instance of a SynchronizedQueue.
		 */
		Lock(SynchronizedQueue<T>& queue) :
				_lock(new std::lock_guard<fast_recursive_mutex>(queue._sync)) {}

		/**
		 * Destructor releases the lock.
		 */
		virtual ~Lock() {
			delete _lock;
		}

	private:
		std::lock_guard<fast_recursive_mutex>* _lock;
	};

	friend class Lock;

	/**
	 * Obtain an external lock to this SynchronizedQueue.  Will
	 * block until locks from any other threads are released.
	 */
	Lock& lock() {
		return *(new Lock(_sync));
	}

	/**
	 * Pop element from the back of the queue.
	 */
	void pop() {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		_impl.pop();
	}

	/**
	 * Retrieve element from back of the queue.
	 */
	T& back() {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		return _impl.back();
	}

	/**
	 * Retrieve element from back of the queue.
	 */
	const T& back() const {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		return _impl.back();
	}

	/**
	 * Retrieve element from front of the queue.
	 */
	T& front() {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		return _impl.front();
	}

	/**
	 * Retrieve element from front of the queue.
	 */
	const T& front() const {
		std::lock_guard<fast_recursive_mutex> lock(_sync);
		return _impl.front();
	}

	/**
	 * Determine if the queue is empty.
	 */
	bool empty() const {
		return _impl.empty();
	}

	/**
	 * Determine the number of elements in the queue
	 */
	size_t size() const {
		return _impl.size();
	}

private:
	fast_recursive_mutex _sync;
	std::queue<T> _impl;
};
} // namespace KEP
