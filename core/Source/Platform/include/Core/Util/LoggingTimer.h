///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "log4cplus/Logger.h"

// I don't really like this as it makes core util dependent on log4cplus (may be already).
// But, here's where it's going for now.
//
class COREUTIL_API LoggingTimer {
	static unsigned long m_errorLimit;
	static unsigned long m_infoLimit;

public:
	LoggingTimer(log4cplus::Logger &timingLogger, const char *msg) :
			m_timingLogger(timingLogger),
			m_msg(msg),
			m_logged(false) {}

	virtual ~LoggingTimer();

	void CheckTime();

	virtual void LogMessage(log4cplus::Logger &logger, bool errorLimit, TimeMs elapsed);

	static void SetTimeLimits(unsigned long errorLimit, unsigned long infoLimit);

	const char *m_msg;
	log4cplus::Logger &m_timingLogger;
	Timer m_timer;
	bool m_logged;
};

//#undef LOG_TIMINGS
//#ifdef LOG_TIMINGS
//#define START_TIMING( timingLogger, msg ) LoggingTimer _lgt_( timingLogger, msg );
//#else
//#define START_TIMING( timingLogger, msg ) Timer _lgt_
//#endif
