///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <sstream>
#include <vector>
#include "Core/Util/StringHelpers.h"

/**
 * A collection of classes, born out of the database model unit testing,
 * to facilitate building various classes of SQL Statements.
 */

/**
 * All builders are derived from this class
 */
class SQLStatement {
public:
	virtual std::string str() const = 0;
};

// unit tests.

//SQLInsert i1("developer.active_logins");
//i1.value("user_id", "5295053")
//	.value("server_id", "8820")
//	.value("game_id", "5316")
//	.value("created_date", "now()")
//	.value("ip_address", "'10.1.201.79'");

//// ASSERT_STREQ("insert into developer.active_logins(user_id,server_id,game_id,created_date,ip_address) values (5295053,8820,5316,now(),'10.1.201.79')", i1.str().c_str());

//SQLInsert i2("wok.players");
//i2.values(
//{
//	{ "player_id", "1094703" }
//	,{ "user_id", "1074370" }
//	,{ "name", "'groovyd'" }
//	,{ "title", "'Legendary Builder'" }
//	,{ "spawn_config_index", "109" }
//	,{ "original_EDB", "15" }
//	,{ "team_id", "0" }
//	,{ "race_id", "0" }
//	,{ "last_update_time", "now()" }
//	,{ "in_game", "'T'" }
//	,{ "server_id", "8820" }
//	,{ "kaneva_user_id", "5295053" }
//	,{ "created_date", "now()" }
//	,{ "is_active", "'Y'" }
//}
//);
//ASSERT_STREQ("insert into wok.players(player_id,user_id,name,title,spawn_config_index,original_EDB,team_id,race_id,last_update_time,in_game,server_id,kaneva_user_id,created_date,is_active) values (1094703,1074370,'groovyd','Legendary Builder',109,15,0,0,now(),'T',8820,5295053,now(),'Y')", i2.str().c_str());
