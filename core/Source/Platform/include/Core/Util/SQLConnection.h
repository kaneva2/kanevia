///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <log4cplus/logger.h>
#include <tchar.h>
#include "Core/Util/Pool.h"
#include "Core/Util/FunctionTraits.h"
#include "Core/Util/Unicode.h"
#include "errmsg.h" // mysql 5
#include "mysqld_error.h" // mysql 5

#include "Core/Util/fast_mutex.h"
#include "Core/Util/SQLStatementBuilder.h"

// mysql++.h reenables warning 4244 and maybe others.
// We don't want mysql++ to be messing with our chosen warning level.
#pragma warning(push)
#include <mysql++/mysql++.h>
#pragma warning(pop)

#include <mysql++/transaction.h>
#include <mysql++/ssqls.h>

namespace KEP {

static const unsigned short RETRY_LIMIT = 2;

class SQLConnection;

/**
* SQLExecutor provides an interface for making SQL calls to a
* SQLConnection
*/
class SQLExecutor {
public:
	typedef std::shared_ptr<SQLExecutor> Ptr;

	// overrides this just to save off db, server, port, user for use in excpetions
	SQLExecutor(SQLConnection& conn, const std::string& callingFunction, const std::string& logName) :
			m_conn(conn), m_callingFunction(callingFunction), m_logName(logName) {}

	SQLExecutor& setCallingFunction(const std::string& callingFunction) {
		m_callingFunction = callingFunction;
		return *this;
	}

	SQLExecutor& setLogName(const std::string& logName) {
		m_logName = logName;
		return *this;
	}

private:
	template <typename Callable>
	SQLExecutor& execSQL(const bool throws, Callable&& callable) {
		// Lock the connection to prevent possible threading issues.
		m_conn.lock();

		log4cplus::Logger logger = log4cplus::Logger::getInstance(KEP::Utf8ToUtf16(m_logName));

		// Verify connection.
		if (!m_conn.connected()) {
			m_conn.ping();
			if (!m_conn.connected() && throws) {
				std::string s = "Database not connected.  Aborting action.";
				LOG4CPLUS_WARN(logger, s);
				throw ChainedException(ErrorSpec(m_conn.errnum(), s));
			}
		}

		// Check again, in no-throw case.
		if (m_conn.connected()) {
			int sqlTries = 0;
			for (; sqlTries < RETRY_LIMIT; sqlTries++) {
				try {
					bool bContinue = true;
					CallPossiblyVoidFunction<bool, Callable, SQLConnection&>(&bContinue, std::forward<Callable>(callable), m_conn);
					break;
				} catch (const mysqlpp::BadQuery& er) {
					int sqlErr = m_conn.errnum();
					if (sqlErr != CR_SERVER_LOST) {
						std::string s = formatBadQuery(m_callingFunction.c_str(), er.what(), er.query_string(), m_conn.server(), m_conn.port(), m_conn.db(), m_conn.user());
						LOG4CPLUS_WARN(logger, s);
						if (throws)
							throw ChainedException(ErrorSpec(sqlErr, s));
					} else {
						LOG4CPLUS_WARN(logger, "Lost connection in " << m_callingFunction << " retrying since got " << sqlErr << " " << er.what());
					}
				} catch (const mysqlpp::BadConversion& er) {
					std::string s = "Database conversion error in " + m_callingFunction + ".  Error: " + er.what();
					LOG4CPLUS_WARN(logger, s);
					if (throws)
						throw ChainedException(ErrorSpec(m_conn.errnum() != 0 ? m_conn.errnum() : E_INVALIDARG, s));
				} catch (const mysqlpp::Exception& er) {
					std::string s = "Database error in " + m_callingFunction + ".  Error: " + er.what();
					LOG4CPLUS_WARN(logger, s);
					if (throws)
						throw ChainedException(ErrorSpec(m_conn.errnum() != 0 ? m_conn.errnum() : E_INVALIDARG, s));
				}
				if (sqlTries >= RETRY_LIMIT) {
					LOG4CPLUS_WARN(logger, "Retry limit hit for " + m_callingFunction);
				}
			}
		}
		return *this;
	}

public:
	template <typename Callable>
	SQLExecutor& execSQLThrow(Callable&& callable) {
		return execSQL(true, callable);
	}

	template <typename Callable>
	SQLExecutor& execSQLNoThrow(Callable&& callable) {
		return execSQL(false, callable);
	}

	template <typename... Args>
	mysqlpp::StoreQueryResult execSelectNoThrow(const std::string& selectSQL, Args&&... args) {
		mysqlpp::StoreQueryResult ret;
		execSQLNoThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << selectSQL;
			query.parse();
			ret = query.store(std::forward<Args>(args)...);
		});
		return ret;
	}

	template <typename... Args>
	mysqlpp::StoreQueryResult execSelectThrow(const std::string& selectSQL, Args&&... args) {
		mysqlpp::StoreQueryResult ret;
		execSQLThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << selectSQL;
			query.parse();
			ret = query.store(std::forward<Args>(args)...);
		});
		return ret;
	}

	template <typename... Args>
	bool execUpdateNoThrow(const std::string& updateSQL, Args&&... args) {
		bool ret = false;
		execSQLNoThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << updateSQL;
			query.parse();
			ret = query.execute(std::forward<Args>(args)...);
		});
		return ret;
	}

	template <typename... Args>
	SQLExecutor& execUpdateThrow(const std::string& updateSQL, Args&&... args) {
		bool ret = false;
		execSQLThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << updateSQL;
			query.parse();
			ret = query.execute(std::forward<Args>(args)...);
		});
		return *this;
	}

	template <typename... Args>
	mysqlpp::StoreQueryResult execProcNoThrow(const std::string& selectSQL, const int resultSet, Args&&... args) {
		mysqlpp::StoreQueryResult ret;
		execSQLNoThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << selectSQL;
			query.parse();
			ret = query.store(std::forward<Args>(args)...);
			if (resultSet >= 1) {
				for (int i = 0; i < resultSet; i++) {
					ret = query.store_next();
				}
			}
			checkMultipleResultSets(query, m_callingFunction.c_str());
		});
		return ret;
	}

	template <typename... Args>
	SQLExecutor& execUpdateThrow(const SQLStatement& updateSQL, Args&&... args) {
		return execUpdateThrow(updateSQL.str(), std::forward<Args>(args)...);
	}

	template <typename... Args>
	mysqlpp::StoreQueryResult execProcThrow(const std::string& selectSQL, const int resultSet, Args&&... args) {
		mysqlpp::StoreQueryResult ret;
		execSQLThrow([&](SQLConnection& conn) {
			mysqlpp::Query query = conn.query();
			query << selectSQL;
			query.parse();
			ret = query.store(std::forward<Args>(args)...);
			if (resultSet >= 1) {
				for (int i = 0; i < resultSet; i++) {
					ret = query.store_next();
				}
			}
			checkMultipleResultSets(query, m_callingFunction.c_str());
		});
		return ret;
	}

private:
	SQLConnection& m_conn;
	std::string m_callingFunction;
	std::string m_logName;
};

/**
* SQLConnection encapsulates a relational database connection and provides
* the capability to make SQL calls.
*/
class SQLConnection : public mysqlpp::Connection {
public:
	typedef std::shared_ptr<SQLConnection> Ptr;

	// overrides this just to save off db, server, port, user for use in excpetions
	SQLConnection(const char* db, const char* server = 0, const char* user = 0, const char* password = 0, unsigned int port = 3306) :
			Connection(db, server, user, password, port) {
		m_db = db;
		m_server = server;
		m_port = port;
		m_user = user;
	}

	SQLConnection(const std::string& db, const std::string& server, const std::string& user, const std::string& password, unsigned int port = 3306) :
			Connection(db.c_str(), server.c_str(), user.c_str(), password.c_str(), port) {
		m_db = db;
		m_server = server;
		m_port = port;
		m_user = user;
	}

	SQLConnection(bool te = true) :
			Connection(te), m_port(3306) {}

	SQLConnection(const SQLConnection& src) {
		operator=(src);
	}

	SQLConnection& operator=(const SQLConnection& src) {
		Connection::operator=(src);
		if (&src != this) {
			m_db = src.m_db;
			m_server = src.m_server;
			m_port = src.m_port;
			m_user = src.m_user;
		}
		return *this;
	}

	/// pass thru to base class, saving off params
	virtual bool connect(const char* _db = 0, const char* _server = 0, const char* _user = 0, const char* _password = 0, unsigned int _port = 3306) {
		// will use default values if not passed in
		if (_db)
			m_db = _db;
		if (_server)
			m_server = _server;
		if (_port)
			m_port = _port;
		if (_user)
			m_user = _user;
		if (_password)
			m_password = _password;

		set_option(new mysqlpp::MultiStatementsOption(true)); // need this for calling SPs
		set_option(new mysqlpp::ReconnectOption(true));
		std::string pw(m_password);
		m_password.clear(); // fwiw
		return Connection::connect(m_db.c_str(), m_server.c_str(), m_user.c_str(), pw.c_str(), m_port);
	}

	/// load the configuration from the config file already opened
	const char* db() const {
		return m_db.c_str();
	}

	SQLConnection& setDb(const std::string& db) {
		m_db = db;
		return *this;
	}

	const char* server() const {
		return m_server.c_str();
	}

	SQLConnection& setServer(const std::string& srvr) {
		m_server = srvr;
		return *this;
	}

	unsigned int port() const {
		return m_port;
	}

	SQLConnection& setPort(unsigned short port) {
		m_port = port;
		return *this;
	}

	const char* user() const {
		return m_user.c_str();
	}

	SQLConnection& setUser(const std::string& user) {
		m_user = user;
		return *this;
	}

	SQLConnection& setPassword(std::string& pwd) {
		m_password = pwd;
		return *this;
	}

	COREUTIL_API std::shared_ptr<SQLExecutor> getSQLExecutor(const std::string& callingFunction = "", const std::string& logName = "");

	std::shared_ptr<std::lock_guard<KEP::fast_mutex>> lock() {
		return std::make_shared<std::lock_guard<KEP::fast_mutex>>(m_mutex);
	}

private:
	std::string m_db;
	std::string m_server;
	unsigned int m_port;
	std::string m_user;
	std::string m_password;
	KEP::fast_mutex m_mutex;
};

class SQLConnectionFactory : public BasicFactory<SQLConnection> {
public:
	SQLConnectionFactory(const std::string& host, unsigned short port, const std::string& user, const std::string& password, const std::string& database) :
			_host(host), _port(port), _user(user), _password(password), _db(database) {}

	SQLConnection* create(void) {
		SQLConnection* pc = new SQLConnection();
		pc->connect(_db.c_str(), _host.c_str(), _user.c_str(), _password.c_str(), _port);
		if (!pc->connected())
			throw ChainedException(ErrorSpec(pc->errnum(), pc->error()));

		return pc;
	}

	std::string _host;
	unsigned short _port;
	std::string _user;
	std::string _password;
	std::string _db;
};
typedef BasicPool<SQLConnection> SQLConnectionPool;

/**
* Helper fn to pull off any pending result sets, usually 
* required when calling SPs.
*/
inline void checkMultipleResultSets(mysqlpp::Query& query, const char* name) {
	for (int i = 1; query.more_results(); ++i) {
		mysqlpp::StoreQueryResult res = query.store_next();
	}
}

/**
* Load and format an error message.
*/
inline std::string formatBadQuery(const char* msg, const char* what, const char* query, const char* server, unsigned int port, const char* db, const char* user) {
	// This is a copy of loadStrPrintf, only with a much larger buffer to
	// avoid truncation.
	// TODO: avoid big chunk on stack, use TLS?
	static const int BUF_SIZE = 1024;
	char buffer[BUF_SIZE];
	buffer[BUF_SIZE - 1] = _T('\0');

	_snprintf_s(buffer, _countof(buffer), _TRUNCATE, "Database query error in %s. Error: %s SQL: %s Connection: %s:%d %s\\%s", msg, what, query, server, port, db, user);

	return std::string(buffer);
}

inline bool isValidDBName(const std::string& name) {
	const char* invalidCharacters = "`\xFF"; // If quoted, DB name can be any character other than backtick, \0 or \xff
	for (unsigned int i = 0; i < name.length(); i++) {
		if (strchr(invalidCharacters, name[i]) != NULL) {
			return false;
		}
	}

	return true;
}

} // namespace KEP