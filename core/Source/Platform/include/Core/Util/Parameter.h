///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

// Allow tagging parameters as input, output, or both.
// This can be used as documention of the function and it makes the usage clear at the call site,
// improving readability.
//
// Declare functions like this:
// void foo(In<int> input_parameter, Out<float> output_parameter, InOut<std::string> input_output_parameter);
//
// Call function like this:
// std::string my_string_variable = "A";
// float my_float_variable;
// foo(in(3), out(my_float_variable), inout(my_string_variable));

template <typename T>
class InOut {
public:
	explicit InOut(T& value) :
			m_value(value) {}

	T& get() const { return m_value; }

	template <typename U>
	void set(U&& u) { m_value = std::forward<U>(u); }

private:
	T& m_value;
};

template <typename T>
inline InOut<T> inout(T& t) {
	return InOut<T>(t);
}

template <typename T>
class Out {
public:
	explicit Out(T& value) :
			m_value(value) {}
	Out(InOut<T>& io) :
			m_value(io.get()) {}

	T& get() const { return m_value; }

	template <typename U>
	void set(U&& u) { m_value = std::forward<U>(u); }

private:
	T& m_value;
};

template <typename T>
inline Out<T> out(T& t) {
	return Out<T>(t);
}

template <typename T>
inline Out<T> out(Out<T>& t) {
	return t;
}

template <typename T>
class In {
public:
	explicit In(const T& value) :
			m_value(value) {}
	In(const InOut<T>& io) :
			m_value(io.get()) {}

	const T& get() const { return m_value; }

private:
	const T& m_value;
};

template <typename T>
inline In<T> in(const T& t) {
	return In<T>(t);
}

template <typename T>
inline In<T> in(const In<T>& t) {
	return t;
}

} // namespace KEP