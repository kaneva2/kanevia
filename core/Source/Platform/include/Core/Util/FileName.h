///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <direct.h>
#include <fstream>
#include "Core/Util/CoreUtil.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/StringHelpers.h"

namespace KEP {

/**
 * @file
 * Wrapper around splitpath to facilitate parsing filenames
 */
class COREUTIL_API FileName {
public:
	/**
	 * FileName component specifiers suitable for use in a mask
	 */
	static const unsigned short Drive = 0x0001;
	static const unsigned short Path = 0x0002;
	static const unsigned short Name = 0x0004;
	static const unsigned short Ext = 0x0008;

	/**
	 * Default constructor
	 */
	FileName();

	/**
	 * @deprecated use FileName::parse()
	 */
	FileName(const std::string& subject);
	FileName(const char* fileName);

	/**
	 * Copy constructor
	 */
	FileName(const FileName& fileName);

	/**
	 * destructor
	 */
	virtual ~FileName();

	/**
	 * AssignmentOperator
	 * @param fileName		FileName to being assigned to the FileName
	 * @return reference to self.
	 */
	FileName& operator=(const FileName& fileName);

	/**
	 * Stream a file name into a stringstream
	 *
	 * todo: Replace this with true stream support e.g. operator<<() and operator>>()
	 */
	void streamOut(std::stringstream& ss) const;

	/**
	 * Acquire a string representation of the file name containing the specified components
	 * 
	 * @param		makeup unsigned short that specifies what components of the file name
	 *				are to be contained in the string.  See constants above.  
	 *				Defaults to a file name containing all of the components of the file
	 *				name.
	 * @returns		A string representation of the file name comprised of the specified
	 *				filename components
	 * @deprecated	User str()
	 */
	std::string toString(unsigned short makeup = Drive | Path | Name | Ext) const;

	/**
	 * Acquire a string representation of the file name containing the specified components
	 * 
	 * @param	makeup unsigned short that specifies what components of the file name
	 *			are to be contained in the string.  See constants above.  
	 *			Defaults to a file name containing all of the components of the file
	 *			name.
	 * @returns A string representation of the file name comprised of the specified
	 *			filename components
	 */
	inline std::string str(unsigned short makeup = Drive | Path | Name | Ext) const;

	/**
	 * Acquire the drive component of the file name
	 */
	const std::string& drive() const;

	/**
	 * Set the drive component of the file name
	 * @param new drive value for this FileName
	 * @return reference to self
	 */
	FileName& setDrive(const std::string& drive);

	/**
	 * Acquire the directory component of this FileName
	 * @return the directory component of this FileName
	 */
	std::string dir() const;

	/**
	 * Set the directory component of this FileName.
	 * @return reference to self
	 */
	FileName& setDir(const std::string& dir);

	FileName& appendDir(const std::string& dir);

	/**
	 * Acquire the filename component of this FileName
	 * @return the filename component of this FileName
	 */
	const std::string& fname() const;

	/**
	 * Acquire the filename and ext components of this FileName
	 * @return the 'filename.ext' components of this FileName
	 */
	std::string toNameExt() const;

	/**
	 * Set the filename component of this FileName
	 * @return reference to self
	 */
	FileName& setFName(const std::string& fname);

	/**
	 * Acquire the extension component of this FileName.  Note that the 
     * extension is returned without the "." delimiter prepended.
     * 
	 * @return the extension component of this FileName
	 */
	const std::string& ext() const;

	/**
     * Intended for internal use, but exposed should it be helpful.
     * Obtain the extension with the "." delimiter prepended.
     * If the extension is empty, returns an empty string.
	 * @return the extension component of this FileName with the "."
     * prepended.  If the extension is empty, an empty string is returned.
     */
	std::string extWithDelim() const;

	/**
	 * Set the extension component of this FileName
	 * @return reference to self.
	 */
	FileName& setExt(const std::string& ext);

	/**
     * Static method that returns the current working directory as a FileName object.
     * @return FileName representing the current working directory.
     */
	static FileName getCWD();

	/**
	 * Factory method for creating a FileName instance based on the supplied
	 * file name string.
	 * 
	 * @param fileString		A string that specifies a well formed file name
	 * @param A new FileName instance.
	 */
	static FileName parse(const std::string& fileString);

	/**
	 * Given some formatting data, create a FileName representing a 
	 * guaranteed unique temporary file.
	 *
	 * @param	extension   An extension to be assigned to the file name as 
	 *						indicated above.
	 * @param   prefix		A prefix to be assigned to the file name as 
	 *						indicated above.
	 * @param   suffix      A suffix to be assigned to the file name as 
	 *						indicated above.
	 * @returns A FileName object representing the name of a guaranteed unique
	 *			file name of the format
	 *
	 *                 <usertempdir><prefix><GUID><suffix>.<ext>
	 *
	 *          where USERTEMPDIR is the value returned by the system call ::GetTempPath()
	 */
	static FileName createTemp(const std::string& extension, const std::string& prefix = "", const std::string& suffix = "");

	static std::string tempMask(const std::string& extension, const std::string& prefix = "", const std::string& suffix = "");

	/**
	 * Ultimately, this method may not reside here, but it's being place here
	 * for the purpose of obsolescing GenUtil.cpp/.h
	 *
	 * Given a string, any string that has a \ or a / (path delim)
     * returns any trailing text.  
	 */
	static void GetShortNames(const std::string& pathSource, std::string& pathDest, const char* subDirSource = 0, std::string* subDirDest = 0);

private:
	std::string _drive;
	//	std::string _dir;
	std::string _fname;
	std::string _ext;
	std::vector<std::string> _pathComponents;
};

} // namespace KEP
