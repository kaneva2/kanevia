///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <js.h>
#include "jsSemaphore.h"
#include "Core/Util/SynchronizedQueue.h"

namespace KEP {

/**
 * Queue mechanism which allows for blocking until an item is available
 * in the queue.  Handy for use in pipelinig and communicating across
 * thread boundaries.  It is implemented using SynchronizedQueue and hence
 * is thread safe.
 */
template <typename T>
class BlockingQueue {
public:
	/**
	 * Create a BlockingQueue with an option initial size and maximum depth.
	 */
	BlockingQueue<T>(unsigned long initialCount = 0, unsigned long maxDepth = LONG_MAX) :
			_semaphore(initialCount, maxDepth), _closed(false), _queue() {
		//cout << "BlockingQueue( " << initialCount << ", " << maxDepth << ")" << endl;
	}

	/**
	 * Push an element onto the queue.  Notifies any waiters that there is now
	 * content in the queue.
	 */
	BlockingQueue<T>& push(const T& item) {
		//		cout << "BlockingQueue::push()" << endl;

		// if there are available executors, hand it off
		//
		_queue.push(item);
		_semaphore.increment();

		return *this;
	}

	/**
	 * Determine the number of elements in the queue.
	 */
	size_t size() const { return _queue.size(); }

	/**
	 * Pop an element off of the back of the queue.
	 * 
	 * Warning!  Passing of the reference demands a assignment 
	 * operator. The only reason this is working is because is sole usage
	 * is storing shared_ptr(s).
	 * This method will destruct the original
	 */
	jsWaitRet pop(T& retObj, unsigned long timeout = INFINITE_WAIT) {
		//		cout << "BlockingQueue::pop()[" << this <<  "]: timeout=" << timeout << ";closed=" << _closed << endl;
		jsWaitRet waitret = _semaphore.waitAndDecrement(timeout);
		if (_closed)
			waitret = WR_ERROR;
		if (waitret != WR_OK)
			return waitret;

		SynchronizedQueue<T>::Lock lock(_queue);
		retObj = _queue.front();
		_queue.pop();
		return waitret;
	}

	/**
	 * Closes the queue, releasing any waiters.
	 */
	void close() {
		_closed = true;
		_semaphore.increment();
	}

private:
	SynchronizedQueue<T> _queue;
	jsSemaphore _semaphore;
	bool _closed;
};
} // namespace KEP
