///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <sstream>
#include <memory>

class Monitored {
public:
	Monitored() {}

	virtual ~Monitored() {}

	class Data {
	public:
		virtual ~Data() {}

		virtual std::string toString() const {
			return std::string();
		}

		virtual std::string str() const {
			return std::string();
		}
	};

	typedef std::shared_ptr<Monitored::Data> DataPtr;

	virtual DataPtr monitor() const = 0;
};

class BasicMonitorData : public Monitored::Data {
public:
	BasicMonitorData& operator<<(const std::string& is) {
		ss << is;
		return *this;
	}

	BasicMonitorData& operator<<(const Monitored::DataPtr other) {
		ss << other->toString();
		return *this;
	}

	BasicMonitorData& operator<<(unsigned short s) {
		ss << s;
		return *this;
	}

	BasicMonitorData& operator<<(size_t st) {
		ss << st;
		return *this;
	}

	BasicMonitorData& operator<<(unsigned long ul) {
		ss << ul;
		return *this;
	}

	BasicMonitorData& operator<<(int n) {
		ss << n;
		return *this;
	}

	static Monitored::DataPtr newInstance() {
		return Monitored::DataPtr(new BasicMonitorData());
	}
	virtual std::string str() const {
		return ss.str();
	}
	virtual std::string toString() const {
		return ss.str();
	}

	std::stringstream ss;
};
typedef Monitored::DataPtr BasicMonitorDataPtr;
