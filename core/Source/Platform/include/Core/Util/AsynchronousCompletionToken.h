///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <js.h>
#include "Core/Util/Runnable.h"
#include "Core/Util/ChainedException.h"

namespace KEP {

/**
 * Class for use when executing logic asynchronously.  Allows one thread to be notified
 * when a task exeuting on another thread is complete and allows for the
 * end state of that execution to be determined.
 */
class ACT {
public:
	/**
	 * Interface for listening to changes to ACT's state.
	 */
	class ACTListener {
	public:
		/**
		 * Called when the ACT is marked as complete.  I.e. called
		 * when the subject runnable is marked complete.
		 */
		virtual void ACTComplete(ACT& act) = 0;
	};

	/**
	 * Construct with an instance of runnable.  It is completion of the 
	 * runnable instance that we are waiting for.  It is incumbent 
	 * the Runnable implementation to communicate the results via
	 * it's state upon completion.
	 */
	ACT(RunnablePtr runnable = RunnablePtr(new NoopRunnable())) :
			_signal(::CreateEvent(0, true, false, 0)), _isComplete(false), _verbose(false), _runnable(runnable), _chained(), _inError(false), _result() {}

	virtual ~ACT() {
		::CloseHandle(_signal);
	}

	/**
	 * Enable/disable verbose execution.  This is intended for 
	 * debug/testing purposes and will likely be obsolesced.
	 */
	ACT& enableVerbose() {
		_verbose = true;
		return *this;
	}

	/**
	 * Obtain the results of the asynchronous action.  Note that
	 * if the async task has not yet been complete, this call will 
	 * block for timeoutMS until it is.
	 * 
	 * TODO: This needs some serious massage until it's right.
	 *
	 */
	jsWaitRet wait(unsigned long timeoutMS = INFINITE_WAIT) {
		if (_isComplete)
			return WR_OK;

		// Wait for the event to be signalled
		//
		switch (::WaitForSingleObject(_signal, timeoutMS)) {
			case WAIT_TIMEOUT: return WR_TIMEOUT;
			case WAIT_OBJECT_0: return WR_OK;
			default: return WR_ERROR;
		};
	}

	/**
	 * Current implementation is hehaviorly the same getResultsAsync().
	 * Use as follows:
	 *
	 *   if ( act.wait() == jsWaitOk )
	 *      if ( act.
	 * @disabled.  Further refinement of this class is deferred until we actually put it
	 *				to use.
	 */
	RunnablePtr getResults(unsigned long /*timeoutMS*/ = INFINITE_WAIT) const {
		return _runnable;
	}

	RunResultPtr getResult() const {
		return _result;
	}

	bool isComplete() const { return _isComplete; }

	/**
	 * Retrieve the results of the asynchronous call.  Returns
	 * immediately.
	 */
	RunnablePtr getResultsAsync() const { return _runnable; }

	/**
	 * Mark this ACT as completed with Exception.
	 */
	ACT& complete(const ChainedException& exception) {
		_inError = true;
		_chained = exception;
		::SetEvent(_signal);
		return *this;
	}

	/**
	 * Mark this ACT as complete.  Releases any waiters.
	 */
	ACT& complete(RunResultPtr result = RunResultPtr()) {
		_isComplete = true;
		_result = result;
		::SetEvent(_signal);
		return *this;
	}

	/**
     * 
     */
	bool inError() const { return _inError; }

	ChainedException chainedException() const {
		return _chained;
	}

	/**
	 * Subscribe a listener to Listen for an ACT's state changes.
	 * Currently not implemented.
	 */
	ACT& addListener(ACTListener& /*listener*/) { return *this; }

	/**
	 * Unsubscribe a listener to Listen for an ACT's state changes.
	 * Currently not implemented.
	 */
	ACT& removeListener(ACTListener& /*listener*/) { return *this; }

private:
	bool _isComplete;
	bool _verbose;
	RunnablePtr _runnable;
	ChainedException _chained;
	bool _inError;
	HANDLE _signal;
	RunResultPtr _result;
};

typedef std::shared_ptr<ACT> ACTPtr;
} // namespace KEP
