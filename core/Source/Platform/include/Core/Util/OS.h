///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Util/CoreUtil.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/FileName.h"
#include <thread>

namespace KEP {

CHAINED(OSException);

/**
 * @file
 * Class that wraps select operating system functions.
 */
class COREUTIL_API OS {
public:
	/**
	 * Acquire dos message string relevant to the supplied
	 * error number.
	 */
	static std::string dosMessage(unsigned long errno);

	/**
     * Determine the amount of memory in use by this process.
     */
	static size_t privateBytes();

	/**
	 * Acquire a string representation of the users temporary file 
	 * directory by calling the Windows function ::GetTempPath()
	 */
	static std::string getUserTempPath();

	/**
	 * Create a UUID by calling the windows function UuidCreate()
	 * @returns a string representation of the GUID
	 */
	static std::string makeGUID();

	/**
	 * Given some formatting data, create a string representation of a 
	 * guaranteed unique temporary file.
	 *
	 * @param	extension   An extension to be assigned to the file name as 
	 *						indicated above.
	 * @param   prefix		A prefix to be assigned to the file name as 
	 *						indicated above.
	 * @param   suffix      A suffix to be assigned to the file name as 
	 *						indicated above.
	 * @returns A string representing the the Name of a guaranteed unique name
	 *          of the format
	 *
	 *                 <usertempdir><prefix><GUID><suffix>.<ext>
	 *
	 *          where USERTEMPDIR is the value returned by the system call ::GetTempPath()
	 */
	static std::string makeTempFileName(const std::string& extension = "", const std::string& prefix = "", const std::string& suffix = "");

	static std::string makeTempFileMask(const std::string& extension = "", const std::string& prefix = "", const std::string& suffix = "");

	/**
	 * Acquire the name of the current module (.exe) using the system
	 * call ::GetModuleName()
	 * @throws OSException in the event that the call to ::GetModuleName() fails.
	 */
	static FileName moduleFileName();

	///---------------------------------------------------------
	// convert an error number to a meaningful string for
	// OS-type error messages
	//
	// [in] errNo the error number
	// [out] msg the resulting message or "Error number 0x%x"
	//
	// [Returns]
	//		msg.c_str(), for convenience
	static const wchar_t* errorNumToString(ULONG dwLastError, std::wstring& msg);

	//{

	//	// copied largely from MSDN to include network error codes

	//	HMODULE hModule = NULL; // default to system source
	//	wchar_t* MessageBuffer;
	//	DWORD dwBufferLength;

	//	DWORD dwFormatFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER |
	//		FORMAT_MESSAGE_IGNORE_INSERTS |
	//		FORMAT_MESSAGE_FROM_SYSTEM;

	//	//
	//	// If dwLastError is in the network range,
	//	//  load the message source.
	//	//

	//	if (dwLastError >= NERR_BASE && dwLastError <= MAX_NERR) {
	//		hModule = LoadLibraryExW(
	//			L"netmsg.dll",
	//			NULL,
	//			LOAD_LIBRARY_AS_DATAFILE
	//			);

	//		if (hModule != NULL)
	//			dwFormatFlags |= FORMAT_MESSAGE_FROM_HMODULE;
	//	}

	//	//
	//	// Call FormatMessage() to allow for message
	//	//  text to be acquired from the system
	//	//  or from the supplied module handle.
	//	//
	//	dwBufferLength = FormatMessageW(
	//		dwFormatFlags,
	//		hModule, // module to get message from (NULL == system)
	//		dwLastError,
	//		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // default language
	//		(wchar_t*)&MessageBuffer,
	//		0,
	//		NULL
	//		);
	//	if (dwBufferLength) {
	//		if (dwBufferLength > 2 &&
	//			(MessageBuffer[dwBufferLength - 2] == '\r' || MessageBuffer[dwBufferLength - 2] == '\n'))
	//			dwBufferLength -= 2; // remove \r\n

	//		msg.assign(MessageBuffer, dwBufferLength);

	//		//
	//		// Free the buffer allocated by the system.
	//		//
	//		LocalFree(MessageBuffer);
	//	}
	//	else {

	//		if (HRESULT_FACILITY(dwLastError) == FACILITY_ITF) {
	//			// TODO: if our custom error message, load a string
	//		}
	//		wchar_t s[80];
	//		_snwprintf_s(s, _countof(s), _TRUNCATE, L"Error number 0x%x", dwLastError);
	//		msg = s;
	//	}

	//	//
	//	// If we loaded a message source, unload it.
	//	//
	//	if (hModule != NULL)
	//		FreeLibrary(hModule);

	//	return msg.c_str();
	//}

	static std::string errorNumToString(ULONG dwLastError);

	static void setThreadName(std::thread& thrd, const std::string& szThreadName);
};

} // namespace KEP
