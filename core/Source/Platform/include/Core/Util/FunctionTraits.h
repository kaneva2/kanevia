///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdlib.h>
#include <functional>

// Get the number of arguments to a function. Result is in member "value".
template <typename FunctionType>
struct NumberOfFunctionArguments;
template <typename ReturnType>
struct NumberOfFunctionArguments<ReturnType()> { static const size_t value = 0; };
template <typename ReturnType, typename... Args>
struct NumberOfFunctionArguments<ReturnType(Args...)> { static const size_t value = sizeof...(Args); };
template <typename ReturnType, typename... Args>
struct NumberOfFunctionArguments<ReturnType (*)(Args...)> { static const size_t value = sizeof...(Args); };
template <typename ReturnType, typename... Args>
struct NumberOfFunctionArguments<std::function<ReturnType(Args...)>> { static const size_t value = sizeof...(Args); };
template <typename ClassName, typename ReturnType, typename... Args>
struct NumberOfFunctionArguments<ReturnType (ClassName::*)(Args...)> { static const size_t value = sizeof...(Args); };

// Get the type of template parameter #N. Result is in member "type".
template <size_t N, typename Arg0, typename... Args>
struct NthArgType { typedef typename NthArgType<N - 1, Args...>::type type; };
template <typename Arg0, typename... Args>
struct NthArgType<0, Arg0, Args...> { typedef Arg0 type; };
template <size_t N, typename... Args>
using NthArgType_t = typename NthArgType<N, Args...>::type;

// Get the type of function parameter #N. Result is in member "type".
template <size_t N, typename FunctionType>
struct NthArgTypeOfFunction;
template <size_t N, typename ClassType, typename ReturnType, typename... Args>
struct NthArgTypeOfFunction<N, ReturnType (ClassType::*)(Args...)> { typedef NthArgType_t<N, Args...> type; };
template <size_t N, typename ReturnType, typename... Args>
struct NthArgTypeOfFunction<N, ReturnType (*)(Args...)> { typedef NthArgType_t<N, Args...> type; };
template <size_t N, typename ReturnType, typename... Args>
struct NthArgTypeOfFunction<N, std::function<ReturnType(Args...)>> { typedef NthArgType_t<N, Args...> type; };
template <size_t N, typename FunctionType>
using NthArgTypeOfFunction_t = typename NthArgTypeOfFunction<N, FunctionType>::type;

// Get the return type of a function.
template <typename FunctionType>
struct ReturnTypeOfFunction;
template <typename ReturnType, typename ClassType, typename... Args>
struct ReturnTypeOfFunction<ReturnType (ClassType::*)(Args...)> { typedef ReturnType type; };
template <typename ReturnType, typename... Args>
struct ReturnTypeOfFunction<ReturnType (*)(Args...)> { typedef ReturnType type; };
template <typename ReturnType, typename... Args>
struct ReturnTypeOfFunction<std::function<ReturnType(Args...)>> { typedef ReturnType type; };
template <typename FunctionType>
using ReturnTypeOfFunction_t = typename ReturnTypeOfFunction<FunctionType>::type;

// Class type of member function. void for non-member functions.
// Examples: ClassTypeOfFunction<decltype(&std::vector::size)>::type --> std::vector
//           ClassTypeOfFunction<decltype(&printf)>::type --> void
template <typename FunctionType>
struct ClassTypeOfFunction;
template <typename ReturnType, typename ClassType, typename... Args>
struct ClassTypeOfFunction<ReturnType (ClassType::*)(Args...)> { typedef ClassType type; };
template <typename ReturnType, typename... Args>
struct ClassTypeOfFunction<ReturnType (*)(Args...)> { typedef void type; };
template <typename FunctionType>
using ClassTypeOfFunction_t = typename ClassTypeOfFunction<FunctionType>::type;

// Call a function with arguments and if it's not a void function, assign its return value to the first argument.
template <typename ReturnType, typename Callable, typename... Args>
__forceinline auto CallPossiblyVoidFunction(ReturnType* pReturnValue, Callable&& callable, Args&&... args) -> std::enable_if_t<!std::is_same<std::result_of_t<Callable(Args&&...)>, void>::value> {
	*pReturnValue = callable(std::forward<Args>(args)...);
}

template <typename ReturnType, typename Callable, typename... Args>
__forceinline auto CallPossiblyVoidFunction(ReturnType* pReturnValue, Callable&& callable, Args&&... args) -> std::enable_if_t<std::is_same<std::result_of_t<Callable(Args&&...)>, void>::value> {
	callable(std::forward<Args>(args)...);
}
