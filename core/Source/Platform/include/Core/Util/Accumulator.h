///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/fast_mutex.h"

namespace KEP {

// Type specific traits.
// This is done because if we accumulating complex types, like say Immutable<T>, the definition of ZERO may not be quite so obvious.
// This trait however will address most cases.
//

// In this case, accumulate is a noun, not a verb, the word for things that an
// accumulator accumulates.  Specifically, this template ensures that the
// accumulator is initialized with an appropriate "zero" value.
//
//
template <typename T>
class Accumulate {
public:
	T ZERO() { return 0ul; }
};

// defines for streaming support
//
template <class T>
class Accumulator;

template <class T>
std::ostream& operator<<(std::ostream& out_stream, Accumulator<T>& Obj);

/**
 * Generic accumulator capable of accumulating any of the intrinsic numeric types as
 * well as the implementations of Numeric<>.
 *
 * Each time a value is accumulated, an internal counter is incremented.
 * This allows for determing a mean accumulation amount.
 *
 * Note that concurrency support is implemented for the purpose of Numeric<> 
 * implementations.  A possible future optimization would be to compile time
 * template reflection and not perform synchrony ops for intrinsic numeric types.
 */
template <typename T>
class Accumulator {
public:
	/*
	// For more dynamic accumulator sets, get factory class working.  This will allow
	// the AccumulatorSet to be instantiated with a factory.  Then whenever a request
	// is made for a nonexistent Accumulator, the AccumulatorSet can use the factory
	// to create a new one.
	// template <typename T>
	class Factory {
	public:
		Factory<T>() {}

		Accumulator<T>* createInstance() {
			return new Accumulator<T>();
		}
	};
	*/

	/**
	 * Construct an accumulator with a name.  This name is used as a key when
	 * acquiring an Accumulator from an AccumulatorSet.
	 */
	Accumulator(const std::string& name = "") :
			_count(0), _name(name), _accumulation(Accumulate<T>().ZERO()), _enabled(true) {}

	/**
	 * Copy constructor
	 */
	Accumulator(const Accumulator<T>& toCopy) :
			_count(toCopy._count), _name(toCopy._name), _accumulation(toCopy._accumulation), _enabled(toCopy._enabled)

	{}

	/**
	 * Assignment operator
	 */
	Accumulator<T>& operator=(const Accumulator<T>& rhs) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);

		_count = rhs._count;
		_name = rhs._name;
		_accumulation = rhs._accumulation;
		_enabled = rhs._enabled;
		return *this;
	}

	/**
	 * Accumulate a value and increment the counter.
	 */
	Accumulator<T>& accumulate(const T& accumulate) {
		if (!_enabled)
			return *this;

		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		_count++;
		_accumulation += accumulate;
		return *this;
	}

	/**
	 * Enable/disable this Accumulator
	 */
	Accumulator<T>& setEnabled(bool enable) {
		_enabled = enable;
		return *this;
	}

	/**
	 * Determine if this Accumulator is enabled or disabled
	 * @return true if enabled, else false.
	 */
	bool isEnabled() { return _enabled; }

	/**
	 * Equality operator.
	 */
	bool operator==(const Accumulator<T>& rhs) {
		// If we're looking at an accumulator sitting in the same memory location
		// then it must be equal.
		//
		if (&rhs == this)
			return true;

		// Else, perform deep comparison
		//
		return (_count == rhs._count) && (_name == rhs._name) && (_accumulation == rhs._accumulation) && (_enabled == rhs._enabled);
	}

	/**
	 * Acquire the name of this Accumulator.
	 */
	const std::string& name() { return _name; }

	/**
	 * Acquire the count of this Accumulator.
	 */
	unsigned long count() { return _count; }

	/**
	 * Acquire the accumulated value of the Accumulator.
	 */
	const T& accumulation() const { return _accumulation; }

	/**
     * Clear the accumulator in preparation for fresh operation.
     */
	Accumulator<T>& clear() {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		_count = 0;
		_accumulation = 0;
		return *this;
	}

	/**
	 * This method is an function external to this class that streams an instance
	 * of Accumulator to an output stream.
	 */
	friend std::ostream& operator<<<>(std::ostream& out_stream, Accumulator<T>& Obj);

private:
	unsigned long _count;
	std::string _name;
	bool _enabled;
	T _accumulation;
	fast_recursive_mutex m_sync; // protect the accumulator
};

/**
 * Implementation of the output operater.
 */
template <class T>
std::ostream& operator<<(std::ostream& out_stream, Accumulator<T>& Obj) {
	float mean = Obj._count == 0 ? float(0) : (Obj._accumulation / Obj._count);
	out_stream << "<accumulator name=\"" << Obj._name << "\" count=\"" << Obj._count << "\" cummulative=\"" << Obj._accumulation << "\" mean=\"" << mean << "\"/>";
	return out_stream;
}

} // namespace KEP