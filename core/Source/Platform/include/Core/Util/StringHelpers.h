///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>

namespace KEP {
typedef std::pair<std::string, std::string> StringPairT;

// Traits about Numbers which are used insure that parsing when Numbers to string
// we get the expected representation.
//
enum NumberFormat {
	Signed // i.e. range short is -32768 to 32767
	,
	Unsigned // i.e. range short is 0 to 65535
	,
	FloatingPoint
};

// Internally we use sprintf to format the numeric.  Thus far in most cases
// %d will get the disired effect.  However for unsigned long/int (32bit) we
// need to adjust the format string to %u.
//
// Partial.  What is used in cases where we haven't declared specific implementations
//
template <typename T>
struct NumberTraits { static const NumberFormat format = Signed; };

// Traits specific to unsigned int
template <>
struct NumberTraits<unsigned int> { static const NumberFormat format = Unsigned; };

// Traits specific to unsigned long.
template <>
struct NumberTraits<unsigned long> { static const NumberFormat format = Unsigned; };

// Traits specific to double
template <>
struct NumberTraits<double> { static const NumberFormat format = FloatingPoint; };

class StringHelpers {
public:
#define STRINGHELPERS_SPACES " \t\r\n"

	static std::string rtrim(const std::string& s, const std::string& t = STRINGHELPERS_SPACES) {
		std::string d(s);
		std::string::size_type i(d.find_last_not_of(t));
		if (i == std::string::npos)
			return "";
		else
			return d.erase(d.find_last_not_of(t) + 1);
	}

	static std::string ltrim(const std::string& s, const std::string& t = STRINGHELPERS_SPACES) {
		std::string d(s);
		return d.erase(0, s.find_first_not_of(t));
	}

	static std::string trim(const std::string& s, const std::string& t = STRINGHELPERS_SPACES) {
		std::string d(s);
		return ltrim(rtrim(d, t), t);
	}

	static std::string findandreplace(const std::string& source, const std::string& find, const std::string& replace) {
		if (find == replace)
			return source;

		std::string s(source);
		size_t i;
		for (; (i = s.find(find)) != std::string::npos;)
			s.replace(i, find.length(), replace);

		return s;
	}

	static std::string strip(const std::string& s, char c) {
		std::string ss(s);
		ss.erase(remove(ss.begin(), ss.end(), c), ss.end());
		return ss;
	}

	static std::string strip(const std::string& s, std::vector<char> chars) {
		std::string ss(s);
		for (auto itr = chars.begin(); itr != chars.end(); itr++) {
			ss.erase(remove(ss.begin(), ss.end(), *itr), ss.end());
		}
		return ss;
	}

	static bool endsWith(const std::string& needle, const std::string& haystack) {
		return endsWith(needle.c_str(), haystack.c_str());
	}

	static std::vector<std::string>& split(const std::string& s, char d, std::vector<std::string>& elems) {
		std::stringstream ss(s);
		std::string item;

		while (getline(ss, item, d)) {
			elems.push_back(item);
		}

		return elems;
	}

	static std::vector<std::string> split(const std::string& s, char d) {
		std::vector<std::string> elems;
		return split(s, d, elems);
	}

	static std::string lower(const std::string& subject) {
		std::string copy(subject);
		transform(copy.begin(), copy.end(), copy.begin(), ::tolower);
		return copy;
	}

	static std::string& lower(std::string& subject) {
		transform(subject.begin(), subject.end(), subject.begin(), ::tolower);
		return subject;
	}

	static std::string upper(const std::string& subject) {
		std::string copy(subject);
		transform(copy.begin(), copy.end(), copy.begin(), ::toupper);
		return copy;
	}

	static std::string& upper(std::string& subject) {
		transform(subject.begin(), subject.end(), subject.begin(), ::toupper);
		return subject;
	}

	static __declspec(dllexport) StringPairT parseKeyValuePair(const std::string& keyvalpairString, char keyValueDelimiter = '=');
	static __declspec(dllexport) std::vector<StringPairT> parseKeyValuePairs(const std::string& paramString, char pairDelimiter = ';', char keyValueDelimiter = '=');
	static __declspec(dllexport) std::vector<StringPairT> parseKeyValuePairsWithOption(const std::string& paramString, char optionFilter = '\0', char pairDelimiter = ';', char keyValueDelimiter = '=');

	/**
	 * Parses a number into a string.
	 * @see StringHelpersTests.h for eamples of usage.
	 */
	template <typename T>
	static std::string parseNumber(T source) {
		static char* formats[] = { "%d", "%u", "%f" };

		char buf[30] = { 0 }; // one extra byte for null
		sprintf_s(buf, formats[NumberTraits<T>::format], source);
		return std::string(buf);
	}

	/**
     * Modeled after PHP explode() function to tokenize string and place components
     * in supplied vector.  Note that explode does not clear the vector.  This means
     * that artifacts of this function can be aggregated over multiple calls.  It
     * also means that if you want the vector cleared, you need to do it yourself.
     * 
     * @param   target      StringVector into which components will be appended.
     * @param   explosive   The string to be blown up.
     * @param   delim       String that is used to detect components in the explosive.
     * @returns reference to target
     */
	static std::vector<std::string>& explode(std::vector<std::string>& target, const std::string& explosive, const std::string& delim = ",") {
		int trailer = -1;
		int len = explosive.length();
		int leader;
		for (leader = 0; leader < len; leader++) {
			if (strchr(delim.c_str(), explosive[leader]) == 0)
				continue;

			if (trailer == leader)
				continue;

			std::string subStr = "";
			if ((leader - trailer) != 1)
				subStr = explosive.substr(trailer + 1, leader - trailer - 1);
			target.push_back(subStr);

			trailer = leader;
		}

		if ((trailer + 1) < len)
			target.push_back(explosive.substr(trailer + 1));

		return target;
	}

	// DRF - Added
	static std::vector<int>& explode(std::vector<int>& target, const std::string& explosive, const std::string& delim = ",") {
		// Explode To vector<string>
		std::vector<std::string> stringVector;
		explode(stringVector, explosive, delim);

		// Convert To vector<int>
		size_t vecSize = stringVector.size();
		for (size_t i = 0; i < vecSize; ++i)
			target.push_back(atoi(stringVector[i].c_str()));

		return target;
	}

	// DRF - Added
	template <class T>
	static std::string& implode(const std::vector<T>& implosive, std::string& target, const std::string& delim = ",") {
		// Convert Vector To Delimited String
		size_t vecSize = implosive.size();
		std::stringstream ss;
		for (size_t i = 0; i < vecSize; ++i)
			ss << (i ? delim : "") << implosive[i];
		target = ss.str();
		return target;
	}

	/**
	 * Less efficient version of implode.  Prior is optimized to preclude string copy operations
	 * by requiring the caller to provide a reference to a string that will contain the imploded
	 * output.  This version opts for ease of use by removing this requirement at the expense
	 * of guaranteed copy operations.
	 */
	template <class T>
	static std::string implode(const std::vector<T>& implosive, const std::string& delim = ",") {
		std::string ret;
		return implode(implosive, ret, delim);
	}

	/**
	 * Determine if one string starts with another
	 *
	 * @param needle		The target string.  The string we are looking for
	 * @param haystack		The test subject.  The string that is being searched.
	 * @return				true if haystack begins with the value contained in needle
	 */
	static bool startsWith(const char* needle, const char* haystack) {
		if (needle == 0)
			return false;
		if (haystack == 0)
			return false;
		unsigned int needleLen = strlen(needle);
		if (needleLen == 0)
			return false;
		if (needleLen > strlen(haystack))
			return false;
		unsigned int lcv1;
		for (lcv1 = 0; lcv1 < needleLen; lcv1++)
			if (!(needle[lcv1] == haystack[lcv1]))
				return false;
		return true;
	}

	/**
	 * Determine if one string starts with another
	 *
	 * @param needle		The target string.  The string we are looking for
	 * @param haystack		The test subject.  The string that is being searched.
	 * @return				true if haystack begins with the value contained in needle
	 */
	static bool endsWith(const char* needle, const char* haystack) {
		if (needle == 0)
			return false;
		if (haystack == 0)
			return false;
		unsigned int needleLen = strlen(needle);
		if (needleLen == 0)
			return false;
		if (needleLen > strlen(haystack))
			return false;

		unsigned int haystackLen = strlen(haystack);
		const char* h = haystack + haystackLen - needleLen;
		const char* n = needle;

		for (int ii = 0; ii < needleLen; ii++, n++, h++) {
			if (!(*n == *h))
				return false;
		}
		return true;
	}

	static bool startsWith(const std::string& needle, const std::string& haystack) {
		return startsWith(needle.c_str(), haystack.c_str());
	}
};
} // namespace KEP