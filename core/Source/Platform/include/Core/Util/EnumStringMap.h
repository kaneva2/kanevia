///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/key_extractors.hpp>

namespace KEP {

template <typename EnumType>
class EnumStringMap {
	// Map definition
	struct StringComparator {
		StringComparator(int (*CompareFunction)(const char*, const char*)) :
				m_CompareFunction(CompareFunction) {}
		bool operator()(const char* left, const char* right) const { return m_CompareFunction(left, right) < 0; }

		int (*m_CompareFunction)(const char*, const char*);
	};

	typedef std::pair<EnumType, const char*> value_type;
	typedef boost::multi_index::member<value_type, EnumType, &value_type::first> KeyExtractor1;
	typedef boost::multi_index::member<value_type, const char*, &value_type::second> KeyExtractor2;
	typedef boost::multi_index_container<
		value_type,
		boost::multi_index::indexed_by<
			boost::multi_index::ordered_non_unique<KeyExtractor1>,
			boost::multi_index::ordered_unique<KeyExtractor2, StringComparator>>>
		MapType;

public:
	// First argument is an array of std::pair of (enum value, enum name).
	// Second and third arguments are default return values for failed lookups.
	// Fourth argument is to validate that all enum values have strings associated with them. It should be the total number of unique enum values in the given array.
	// Each enum value may have multiple strings. The first one provided is returned for lookups by enum.
	template <size_t N>
	EnumStringMap(
		std::pair<EnumType, const char*> (&aEnumStrings)[N],
		EnumType enumForNotFound,
		const char* stringForNotFound,
		size_t nEnumValues = N,
		int (*StringCompareFunction)(const char*, const char*) = stricmp) :
			m_EnumForNotFound(enumForNotFound),
			m_StringForNotFound(stringForNotFound), m_container(boost::make_tuple(boost::make_tuple(KeyExtractor1(), std::less<EnumType>()), boost::make_tuple(KeyExtractor2(), StringComparator(StringCompareFunction)))) {
		Init(aEnumStrings, nEnumValues);
	}

	template <size_t N>
	void Init(std::pair<EnumType, const char*> (&aEnumStrings)[N], size_t nEnumValues = N) {
		size_t nAdded = 0;
		for (const std::pair<EnumType, const char*>& enumString : aEnumStrings) {
			// Verify string is not already in map.
			if (m_container.get<1>().find(enumString.second) != m_container.get<1>().end()) {
				ASSERT(false);
				continue;
			}

			// Insert after any existing entries with the same enumeration value.
			auto itr = m_container.get<0>().upper_bound(enumString.first);
			bool bUniqueEnum = true;
			if (itr != m_container.get<0>().begin()) {
				decltype(itr) itrprev = itr;
				--itrprev;
				if (itrprev->first == enumString.first)
					bUniqueEnum = false;
			}

			if (bUniqueEnum)
				++nAdded;

			m_container.get<0>().insert(itr, enumString);
		}
		ASSERT(nAdded == nEnumValues);
	}

	// Look up returning false for invalid values.
	bool LookUp(const char* pszName, Out<EnumType> value) const {
		if (pszName) {
			auto itr = m_container.get<1>().find(pszName);
			if (itr != m_container.get<1>().end()) {
				value.set(itr->first);
				return true;
			}
		}
		return false;
	}
	bool LookUp(EnumType id, Out<const char*> value) const {
		auto itr = m_container.get<0>().find(id);
		if (itr != m_container.get<0>().end()) {
			value.set(itr->second);
			return true;
		}
		return false;
	}

	// Look up specifying default return values.
	EnumType LookUp(const char* pszName, EnumType notFoundReturnValue) const {
		EnumType found;
		if (LookUp(pszName, out(found)))
			return found;
		return notFoundReturnValue;
	}
	const char* LookUp(EnumType id, const char* pszNotFoundReturnValue) const {
		const char* found;
		if (LookUp(id, out(found)))
			return found;
		return pszNotFoundReturnValue;
	}

	// Look up using predefined default return values.
	EnumType LookUp(const char* pszName) const { return LookUp(pszName, m_EnumForNotFound); }
	const char* LookUp(EnumType id) const { return LookUp(id, m_StringForNotFound); }

private:
	EnumType m_EnumForNotFound;
	const char* m_StringForNotFound;

	MapType m_container;
};

template <typename EnumType>
class EnumStringMapCaseInsensitive : public EnumStringMap<EnumType> {
public:
	template <size_t N>
	EnumStringMapCaseInsensitive(std::pair<EnumType, const char*> (&aEnumStrings)[N], EnumType enumForNotFound, const char* stringForNotFound, size_t nEnumValues = N) :
			EnumStringMap(aEnumStrings, enumForNotFound, stringForNotFound, nEnumValues, _stricmp) {
	}
};

template <typename EnumType>
class EnumStringMapCaseSensitive : public EnumStringMap<EnumType> {
public:
	template <size_t N>
	EnumStringMapCaseSensitive(std::pair<EnumType, const char*> (&aEnumStrings)[N], EnumType enumForNotFound, const char* stringForNotFound, size_t nEnumValues = N) :
			EnumStringMap(aEnumStrings, enumForNotFound, stringForNotFound, nEnumValues, _strcmp) {
	}
};

template <typename EnumType>
const EnumStringMap<EnumType>& GetEnumStringMap() {
	static_assert(std::is_enum<EnumType>::value, "Must be enum");
	static_assert(EnumType(), "This function should be specialized for your enum");
}
template <typename EnumType>
const char* EnumToString(EnumType enumValue) {
	return GetEnumStringMap<EnumType>().LookUp(enumValue);
}
template <typename EnumType>
EnumType StringToEnum(const char* pszString) {
	return GetEnumStringMap<EnumType>().LookUp(pszString);
}
template <typename EnumType>
EnumType StringToEnum(const std::string& str) {
	return StringToEnum<EnumType>(str.c_str());
}

} // namespace KEP