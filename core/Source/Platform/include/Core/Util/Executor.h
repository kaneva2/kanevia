///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Runnable.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/AsynchronousCompletionToken.h"
#include "Core/Util/HandoffBox.h"

#include <memory>

#include <jsThread.h>

namespace KEP {

/**
 * Executor executes executables.  Executable serves as a wrapper around a runnable
 * object that maintains information specific to running under an Executor.  Also
 * Runnable makes no assumptions about asynchrony.  This class provides an ACT
 * that allows for asynchronous retrieval of the Runnable instance.
 */
class __declspec(dllexport) Executable {
public:
	/**
	 * Construct with an instance of Runnable.
	 */
	Executable(RunnablePtr runnable) :
			_runnable(runnable), _actPtr(ACTPtr(new ACT(runnable))) {}

	virtual ~Executable() {}

	/**
	 * Execute the encapsulated Runnable object.  Unblocks the ACT when execution is complete.
	 */
	void Execute() {
		try {
			_actPtr->complete(_runnable->run()); // Force the results off of the Runnables stack.
				// Note that this requires deep copy support.
		} catch (const ChainedException& exc) {
			_actPtr->complete(exc);
		} catch (...) {
			_actPtr->complete(ChainedException(ErrorSpec(-1, "Unknown exception!")));
		}
	}

	/**
	 * Obtain this Executable's ACT.
	 */
	ACTPtr act() { return _actPtr; }

private:
	RunnablePtr _runnable;
	ACTPtr _actPtr;
};
typedef std::shared_ptr<Executable> ExecutablePtr;

class __declspec(dllexport) Executor {
public:
	enum runState {
		running // loop is running
		,
		polling // blocked, waiting for work.
		,
		pending // work is pending in the handoff box
		,
		busy // currently executing a task
		,
		stopped // loop is exited
	};

	/**
	 * Listener can be registered with an Executor to listen for state changes in and executor.
	 */
	class Listener {
	public:
		virtual ~Listener() {}
		virtual void stateChanged(Executor& executor) = 0;
	};

	/**
	 * Rather than constantly test if a valid listener has been associated with this Executor,
	 * Create a do null listener that is used to instantiate an Executor.
	 */
	class DefaultListener : public Listener {
	public:
		virtual ~DefaultListener() {}
		void stateChanged(Executor& /*executor*/) { /* noop */
		}
	};

	/**
     * Default constructor
     */
	Executor() {}

	/**
     * dtor
     */
	virtual ~Executor() {}

	/**
     * Return this executor's run state.  See Executor::runState
	 * @return This executors run state.
	 */
	virtual runState state() = 0;

	/**
	 * Causes Executor to exit it's thread loop.  If the thread is currently
	 * executing a task.  It will exit after the task is complete.
	 */
	//    virtual Executor&   stop() = 0;
	virtual ACTPtr stop() = 0;

	/**
	 * Associate an ExecutorListener with this listener.
	 * Note that currently only supports a single listener which is used by ThreadPool
	 * to detect the executors state.  I.e.  Using this method on pooled Executors prior
	 * to the addition of multiple listener support WILL break the thread implementation.
	 */
	virtual void addListener(const std::shared_ptr<Listener>& listener) = 0;

	/**
	 * Depending on the application, we may need to wait for our executor
	 * to stop.  Acquire an ACT upon which we can block until this executor
	 * is finished. 
	 * 
	 */
	virtual ACTPtr act() = 0;
};

class __declspec(dllexport) AbstractExecutor : public Executor {
public:
	/**
     * Construct with a name.
     */
	AbstractExecutor(const std::string& name = "") :
			Executor(), _stopACT(new ACT()), _runState(Executor::stopped), _name(name), _handoff() {
		_listener = std::make_shared<Executor::DefaultListener>();
	}

	virtual ~AbstractExecutor() {
		if (_runState == Executor::running)
			stop()->wait();
	}

	/**
     * Every executive has a name, though it could be empty.  This method returns that name. 
     * Note that the underlying thread implementation may manage this value
     */
	virtual const std::string& name() const { return _name; }

	/**
	 * Executor maintains a timer to tracks the number of tasks it has executed and the 
	 * amount of time spent processing those tasks.  This time does not include idle time.
	 */
	Timer& timer() { return _timer; }

	/**
	 * Depending on the application, we may need to wait for our executor
	 * to stop.  Acquire an ACT upon which we can block until this executor
	 * is finished. 
	 * 
	 */
	ACTPtr act() { return _stopACT; }

	/**
	 * Associate an ExecutorListener with this listener.
	 * Note that currently only supports a single listener which is used by ThreadPool
	 * to detect the executors state.  I.e.  Using this method on pooled Executors prior
	 * to the addition of multiple listener support WILL break the thread implementation.
	 */
	void addListener(const std::shared_ptr<Executor::Listener>& listener) { _listener = listener; }

	/**
	 * @return This executors run state.
	 */
	Executor::runState state() { return _runState; }

	/**
	 * Obtain string representation of this Executor.
	 */
	std::string toString() {
		// todo: name should be sufficient for a toString method
		// the logic below is used mostly for performance testing
		// and is really too busy/intense for general use.  It
		// really belongs in some kind of instrumentation class.
		//
		std::stringstream ss;
		TimeMs e = timer().ElapsedMs();
		size_t c = timer().Count();
		double mean = 0;
		if (c > 0)
			mean = ((double)e) / ((double)c);
		ss << "Executor[name=\"" << name()
		   << "\";processed=" << timer().Count()
		   << ";elapsed=" << timer().ElapsedMs()
		   << ";mean=" << mean << ";];" << std::endl;
		return ss.str();
	}

	/**
	 * Submit supplied executable to the Executor's thread loop for 
	 * processing.
	 */
	Executor& execute(ExecutablePtr executable) {
		//	cout << "Executor::execute[" << name() << "]()" << endl;
		_handoff.push(executable);

		//	setState(pending);
		return *this;
	}

	/**
	 * Causes Executor to exit it's thread loop.  If the thread is currently
	 * executing a task.  It will exit after the task is complete.
	 */
	virtual ACTPtr stop() {
		// cout << "Executor[" << name() << "]::stop()" << endl;
		// unblock the executor
		// NOTE: the execution loop is either executing, in which case we'll
		// let it finish what it is doing, or it is blocked waiting for a new
		// task.  In either case, closing the handoff box will cause the loop
		// to exit.  There is no need to stop() the thread.
		//
		_handoff.close();

		// jsThread::stop();
		return this->_stopACT;
	}

protected:
	Executor& setState(Executor::runState state) {
		if (state == _runState)
			return *this;
		// cout << "Executor::setState(" << state << ")" << endl;
		_runState = state;
		_listener->stateChanged(*this);
		return *this;
	}

	HandoffBox<ExecutablePtr> _handoff;

private:
	Timer _timer;
	ACTPtr _stopACT;
	Executor::runState _runState;
	std::shared_ptr<Executor::Listener> _listener;
	std::string _name;
};

class jsThreadExecutor : public AbstractExecutor, public jsThread {
public:
	/**
	 * Construct with optional name, thread priority and thread type.
	 *
	 * @param name				Optional std::string used to identify this thread pool in the
	 *							debugger and logging.  Defaults to empty string.
	 * @param priority Optional jsThdPriority specying of the run priority of the thread used by
	 *							this executor.  
	 *							See jsThread.h Defaults to PR_NORMAL.
	 * @param ttype				jsThreadType specifying the type of thread used by this executor.
	 *							See jsThread.h.  Defaults to TT_NORMAL.
	 */
	__declspec(dllexport) jsThreadExecutor(const std::string& name = "", jsThdPriority priority = PR_NORMAL, jsThreadType ttype = TT_NORMAL);

	/**
	 * Destructor.
	 */
	__declspec(dllexport) virtual ~jsThreadExecutor();

	/**
	 * An arrestor is a specialized Runnable object that, when it is executed, 
	 * will stop the executor.  This is handy for implementations that queue
	 * tasks into an executor.   By placing an Arrestor in the queue, the 
     * developer can insure that the executor isn't stopped until all work
	 * allocated to that executor has been completed.  See 
	 * ThreadPool::patientShutdown logic for an example.
	 */
	class Arrestor : public Runnable {
	public:
		Arrestor() {}

		RunResultPtr run() override {
			try {
				jsThreadExecutor::getCurrent().stop(); // Tell Executor, running on this thread
					// to stop.  It will exit after this call
					// rolls off of the stack.
			} catch (const ChainedException& e) {
				return std::make_shared<RunResult>(e);
			}
			return RunResultPtr();
		}
	};

	/**
	 * Causes Executor to exit it's thread loop.  If the thread is currently
	 * executing a task.  It will exit after the task is complete.
	 */
	virtual ACTPtr stop() override {
		_closed = true;
		// unblock the executor
		// NOTE: the execution loop is either executing, in which case we'll
		// let it finish what it is doing, or it is blocked waiting for a new
		// task.  In either case, closing the handoff box will cause the loop
		// to exit.  There is no need to stop() the thread.
		//
		return AbstractExecutor::stop();
	}

	virtual ULONG _doWork() override;

private:
	/**
	 * Function to allow the logic running in the executors thread to actually 
	 * obtain a reference to the executor
	 */
	__declspec(dllexport) static jsThreadExecutor& getCurrent();

	static thread_local jsThreadExecutor* s_pCurrentExecutor;

private:
	bool _closed;
	friend class ThreadPool;
};
typedef std::shared_ptr<jsThreadExecutor> ExecutorPtr;

} // namespace KEP
