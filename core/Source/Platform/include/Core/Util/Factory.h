///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

/**
 * The simplest of Factory interfaces.  
 */
template <typename output_t>
class BasicFactory {
public:
	virtual output_t* create() = 0;
};
