///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// KEPUTIL_EXPORT defintion
#if defined(_WIN32)
#if defined(BUILD_KEP_DLL)
#define KEPUTIL_EXPORT __declspec(dllexport)
#elif defined(STATIC_KEP_UTIL)
#define KEPUTIL_EXPORT
#else
#define KEPUTIL_EXPORT __declspec(dllimport)
#endif
#else
#define KEPUTIL_EXPORT
#endif
