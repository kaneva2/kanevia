///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <vector>
#include "Core/Util/SQLConnection.h"
#include "KEP/BusinessObjects/BusinessObjects.h"

#ifdef DATAMODEL_DLL
#define DATAMODEL_API __declspec(dllexport)
#elif defined(STATIC_DATAMODEL)
#define DATAMODEL_API
#else
#define DATAMODEL_API __declspec(dllimport)
#endif

namespace KEP {

// Retirements
// 1) wok.get_player_server_ids sproc
class DATAMODEL_API DataModel {
public:
	// This will possibly end up disabled.
	DataModel(const std::string& host, unsigned int port, const std::string& user, const std::string& pwd, const std::string& schema = "");

	std::vector<std::shared_ptr<Sound>> getSoundsFor(unsigned long zoneIndex, unsigned long instanceId);

	DataModel& updateSound(unsigned long zoneIndex, unsigned long instanceId, const Sound& sound);

	/**
	 * Submit a map comprised of playerid,serverid pairs and have it updated
	 * with the serverid for the player id.  
	 */
	PlayersByServerT& GetPlayerServers(const vec_player_ids_t& playerIds, PlayersByServerT& playerServerMap);

private:
	SQLConnectionPool* _connectionPool;
};

} // namespace KEP