///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPImageFormat.h"
#include <string>
#include <map>

namespace KEP {

extern unsigned FCC_DXT1, FCC_DXT3, FCC_DXT5, FCC_ZERO;

struct ImageInfo {
	unsigned width;
	unsigned height;
	unsigned depth; // Bits per pixel
	unsigned pixelFormat; // Use D3DFORMAT values at this moment
	unsigned compression; // Compression method (meaning varies per image format, 0 = not compressed)
	unsigned numSubImages; // Number of embedded sub images such as mipmaps, thumbnails, separate alpha channel image, etc.
	ImageOpacity opacity;
	bool allZeroAlpha; // true if all alpha values are zero (sometimes it means alpha is not used). Calculated for DDS only.
	ImageInfo() :
			width(0), height(0), depth(0), pixelFormat(0), compression(0), numSubImages(0), opacity(ImageOpacity::FULLY_OPAQUE), allZeroAlpha(false) {}
};

//=======================================================================
// Generic image decoder with FreeImage

// FreeImage API requests that the buffer being mutable
unsigned char* decodeImage(unsigned char* rawBuffer, unsigned rawBufSize, ImageInfo& info, unsigned& pixBufSize);

//=======================================================================
// Specialized encoders/decoders

// DDS decoder with opacity detection
unsigned char* decodeDDS(const unsigned char* rawBuffer, unsigned rawBufSize, ImageInfo& info, unsigned& pixBufSize);

// Fast DDS encoder assuming the input already follows DDS pixel profile.
unsigned char* encodeDDS_Fast(const unsigned char* rawBuffer, unsigned rawBufSize, ImageInfo& info, unsigned& DDSBufSize);

// Full DDS encoder with nvidia-texture-tools
unsigned char* encodeDDS_NVTT(const unsigned char* rawBuffer, unsigned rawBufSize, ImageInfo& info, unsigned& DDSBufSize);

// JPG with libjpeg-turbo (FreeImage uses non-SIMD libjpeg)
// NOTE: first argument `rawBuffer' should be const however libjpeg takes a mutable buffer as input for some reason. Not sure if the content will be modified.
unsigned char* decodeJPG(unsigned char* rawBuffer, unsigned rawBufSize, ImageInfo& info, unsigned& pixBufSize);

// NOTE: first argument `pixels' should be const however libjpeg takes a mutable buffer as input for some reason. Not sure if the content will be modified.
unsigned char* encodeJPG(unsigned char* pixels, unsigned pixBufSize, const ImageInfo& info, unsigned& rawBufSize, int jpegQuality);

//=======================================================================
// Utilities

// Format detection
std::string detectImageFormat(const unsigned char* rawBuffer, unsigned rawBufSize);

// Image analysis
unsigned getAverageColor(unsigned char* pixBuf, unsigned pixBufSize, unsigned width, unsigned height);

} // namespace KEP
