///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

// Image file formats
enum IMG_FORMAT {
	IMG_UNK, // Unknown

	IMG_JPG,
	IMG_PNG,
	IMG_GIF,
	IMG_BMP,
	IMG_TGA,

	IMG_DDS_ANY = 0x10,
	IMG_DDS_DXT1 = 0x11,
	IMG_DDS_DXT2 = 0x12,
	IMG_DDS_DXT3 = 0x13,
	IMG_DDS_DXT4 = 0x14,
	IMG_DDS_DXT5 = 0x15,
};

// Image opacity: must match item_path_textures.opacity column
enum class ImageOpacity {
	UNKNOWN,
	FULLY_OPAQUE, // Fully opaque
	ONE_BIT_ALPHA, // One-bit on-off transparency
	TRANSLUCENT, // Gradient alpha
	DISCRETE_ALPHA, // Not used at this moment
};

enum TEXTURE_TYPE {
	TEXTURE_1D,
	TEXTURE_2D,
	TEXTURE_3D,
	TEXTURE_CUBE,
	TEXTURE_UNK, // unknown type
};

enum TEXTURE_WRAP_TYPE {
	WRAP_REPEAT, // value = values[ u%1, v%1 ]
	WRAP_MIRROR, // value = (0,0)<=(u%2,v%2)<=(1,1) ? values[ u%2,v%2 ] : values[ 2-u%2,2-v%2 ]
	WRAP_CLAMP, // value = (0,0)<=(u,v)<=(1,1) ? values[ u, v ] : 0
};
} // namespace KEP