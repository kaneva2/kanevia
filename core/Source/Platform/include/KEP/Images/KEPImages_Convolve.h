///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// Image convolution: integer version

#pragma once

#include <vector>

namespace KEP {

struct KernelFlt;
struct KernelInt;
class PixelSampler;

// =========================================================================
// Image convolution
// =========================================================================
#define CONV_ALLOW_CUSTOM_PITCH // Allowing pitch >= width * 4 if defined. Otherwise pitch must equal to width * 4

// Convolve image data with kernel matrix and optional pixel sampler
// Float Kernel
bool convolve(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelFlt& kernel, PixelSampler* sampler, void** pOutPixels);
// Int Kernel (float kernel * 256)
bool convolve(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelInt& kernel, PixelSampler* sampler, void** pOutPixels);

// Convolve image data in two passes (vertical + horizontal) and combine the results. Kernel matrix passed in will be used in vertical pass. It will then be transposed and used in horizontal pass.
// Float Kernel
bool convolve2(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelFlt& kernel, PixelSampler* sampler, unsigned brightness, bool grayScale, unsigned lumThres, unsigned lumMax, void** pOutPixels);
// Int Kernel (float kernel * 256)
bool convolve2(unsigned* pixels, size_t pitch, size_t width, size_t height, const KernelInt& kernel, PixelSampler* sampler, unsigned brightness, bool grayScale, unsigned lumThres, unsigned lumMax, void** pOutPixels);

// =========================================================================
// Kernel
// =========================================================================

#define MAX_KERNEL_SIZE 100

struct KernelInt {
	size_t w, h;
	int m[MAX_KERNEL_SIZE];
};

struct KernelFlt {
	size_t w, h;
	float m[MAX_KERNEL_SIZE];

	explicit operator KernelInt() const {
		KernelInt res;
		res.w = w;
		res.h = h;
		for (size_t i = 0; i < w * h && i < MAX_KERNEL_SIZE; i++) {
			res.m[i] = (int)(m[i] * 256.0f + 0.5f);
		}
		return res;
	}
};

// =========================================================================
// Pixel Sampling Helpers
// =========================================================================

class PixelSampler {
public:
	PixelSampler(size_t blockW, size_t blockH) :
			m_blockW(blockW), m_blockH(blockH), m_minX(-(int)blockW / 2), m_maxX((int)(blockW - 1) / 2), m_minY(-(int)blockH / 2), m_maxY((int)(blockH - 1) / 2) {}

	virtual ~PixelSampler() {}
	virtual unsigned get(unsigned* pixels, size_t pitch, size_t width, size_t height, int x, int y) = 0;

	size_t getBlockWidth() const { return m_blockW; }
	size_t getBlockHeight() const { return m_blockH; }
	int getMinX() const { return m_minX; }
	int getMaxX() const { return m_maxX; }
	int getMinY() const { return m_minY; }
	int getMaxY() const { return m_maxY; }

private:
	size_t m_blockW, m_blockH;
	const int m_minX, m_maxX, m_minY, m_maxY;
};

// Returning per-channel median values of all sampled neighbors
class PixelSampler_Median : public PixelSampler {
public:
	PixelSampler_Median(size_t blockW, size_t blockH) :
			PixelSampler(blockW, blockH) {}
	virtual unsigned get(unsigned* pixels, size_t pitch, size_t width, size_t height, int x, int y) override;

protected:
	static unsigned getMedian(unsigned* array, size_t size);
};

// All variations of neighboring pixel average / interpolation
class PixelSampler_Weighted : public PixelSampler {
public:
	// `weights' is an two dimension array of pixel weights in the size of (xSpan*2+1, ySpan*2+1)
	PixelSampler_Weighted(size_t blockW, size_t blockH, int weights[]);
	PixelSampler_Weighted(size_t blockW, size_t blockH, float weights[]);

	virtual unsigned get(unsigned* pixels, size_t pitch, size_t width, size_t height, int x, int y) override;

private:
	std::vector<int> m_weights;
};

} // namespace KEP
