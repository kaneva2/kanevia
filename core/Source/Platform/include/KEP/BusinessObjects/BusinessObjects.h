///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <map>
#ifdef BUSINESSOBJECTS_DLL
#define BUSINESSOBJECTS_API __declspec(dllexport)
#elif defined(STATIC_BUSINESSOBJECTS)
#define BUSINESSOBJECTS_API
#else
#define BUSINESSOBJECTS_API __declspec(dllimport)
#endif

// BUSINESSOBJECTS_API void forceBusinessObjectsLinkage();

struct Sound {
	//query << "SELECT obj_placement_id, pitch, gain, max_distance, roll_off, `loop`, loop_delay"
	//	<< ", dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle "
	//	"FROM sound_customizations WHERE zone_index=%0 AND instance_id=%1";
	int ObjectPlacementId;
	int Pitch;
	int Gain;
	int MaxDistance;
	int RollOff;
	bool loop; // Flag indicating that the sound should be
		// loop when played. Does it make sense to
		// store this data elsewhere?
	int loopDelay; // milli? sec?
	int DirX;
	int DirY;
	int DirZ;
	int ConeOuterGain;
	int ConeInnerAngle;
	int ConeOuterAngle;

	//for (int i = 0; i < res.size(); i++) {
	//	Row r = res[i];

	//	TiXmlElement* sound_Tag = new TiXmlElement("S");
	//	root->LinkEndChild(sound_Tag);
	//	// Use of the xml parser seems a bit over the top.
	//	// simply stream it
	//	//
	//	XmlInsertChildElement(sound_Tag, "p", r[1]);
	//	XmlInsertChildElement(sound_Tag, "g", r[2]);
	//	XmlInsertChildElement(sound_Tag, "m", r[3]);
	//	XmlInsertChildElement(sound_Tag, "r", r[4]);
	//	XmlInsertChildElement(sound_Tag, "l", r[5]);
	//	XmlInsertChildElement(sound_Tag, "d", r[6]);
	//	XmlInsertChildElement(sound_Tag, "x", r[7]);
	//	XmlInsertChildElement(sound_Tag, "y", r[8]);
	//	XmlInsertChildElement(sound_Tag, "z", r[9]);
	//	XmlInsertChildElement(sound_Tag, "og", r[10]);
	//	XmlInsertChildElement(sound_Tag, "ia", r[11]);
	//	XmlInsertChildElement(sound_Tag, "oa", r[12]);
	//}

	bool operator==(const Sound& rhs) {
		if (&rhs == this)
			return true;
		Sound& self = *this;
		return self.ObjectPlacementId == rhs.ObjectPlacementId && self.Pitch == rhs.Pitch && self.Gain == rhs.Gain && self.MaxDistance == rhs.MaxDistance && self.RollOff == rhs.RollOff && self.loop == rhs.loop && self.loopDelay == rhs.loopDelay && self.DirX == rhs.DirX && self.DirY == rhs.DirY && self.DirZ == rhs.DirZ && self.ConeOuterGain == rhs.ConeOuterGain && self.ConeInnerAngle == rhs.ConeInnerAngle && self.ConeOuterAngle == rhs.ConeOuterAngle;
	}
};

typedef std::vector<unsigned long> vulong_t;
typedef vulong_t vec_player_ids_t;
typedef std::map<unsigned long, std::vector<unsigned long>> PlayersByServerT;