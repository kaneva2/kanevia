///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "..\PatchLib\PatchLibGlobals.h"
#include "..\PatchLib\GameServer.h"
#include "Common\KEPUtil\RuntimeReporter.h"
#include "Common\KEPUtil\CrashReporter.h"


namespace KEP {

class LauncherCommandLineInfo : public CCommandLineInfo {
public:
	BOOL m_bHelp; // /H /? /HELP
	BOOL m_bFromURL; // -url
	BOOL m_bSuccess; // all switches ok
	int numparameters;
	std::string m_strParameter[4];

	LauncherCommandLineInfo(void);

	virtual void ParseParam(const wchar_t* lpszParam, BOOL bSwitch, BOOL /*bLast*/) override;
};

class CServerObList : public CObList {
public:
	virtual ~CServerObList() {
		POSITION pos;
		for (pos = GetHeadPosition(); pos != NULL;) {
			delete (CGameServer*)GetNext(pos);
		}
	}
};

class CKanevaLauncherApp : public CWinApp {
public:
	CKanevaLauncherApp() {}

	~CKanevaLauncherApp();

	virtual BOOL InitInstance() override;

	/** DRF - Overrides CWinApp::ProcessWndProcException()
	* This is where we land with some MFC exceptions. If we needed to show a message or something, we 
	* could do that here. However, in most cases we just want to cause MFC to throw the exception out 
	* to CrashRpt.
	*/
	virtual LRESULT ProcessWndProcException(CException* e, const MSG* pMsg) override { THROW_LAST(); }

	// DRF - Returns the wok (kgp) url of the runtime environment. (eg. 'http://wok.kaneva.com/kgp/')
	std::string WokUrl() const;

	// DRF - Returns Patch Metrics String
	bool PatchAndPlay(std::string& patchMetrics);

	// Assigns GameId & GameUrl from given url
	bool AssignGameIdFromUrl(const std::string& url);

	DECLARE_MESSAGE_MAP()

private:
	bool PullParamsFromURL();
	bool PullParamsFromCommandLine();
	bool getPatchInfo(ULONG gameId, std::vector<PatchInfo>& patchesInfo, const char* serverListUrl);
	bool getPatchInfoTimeout(CObList& servers, std::vector<PatchInfo>& patchesInfo, TimeMs timeoutMs);
	bool ValidateServerPatchUrls(CGameServer* pGS, TimeMs timeoutMs);

	void ClearServerPatchUrls();

	LONG m_gameId;
	std::string m_gameUrl;
	std::string m_cmdLine;
	std::string m_email;
	std::string m_patchUrlOverride;
	std::string m_parentGameIdStr;
	std::string m_playNowGuid;
	//bool m_hackPatch; // use hack patch
	//bool m_hackServers; // use hack serverlist

	LauncherCommandLineInfo m_cmdInfo;
	std::map<std::string, ServerStatus> m_serverPatchUrls; // url -> ServerStatus
	CObList* m_gameServerList;

	bool m_parentPatchesCached;
	std::vector<PatchInfo> m_parentPatches;

	bool m_childPatchesCached;
	std::vector<PatchInfo> m_childPatches;

public:
	// RuntimeReporter Support
	bool RR_Install();
	void RR_AppCallback(RuntimeReporter::CallbackStruct& cbs);

	// CrashReporter Support
	bool CR_Install();
	void CR_AppCallback(CrashReporter::CallbackStruct& cbs);
};

} // namespace KEP