///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KLauncher.h"
#include "../PatchLib/GameServer.h"
#include "../PatchLib/ConfigValues.h"
#include "StpUrl.h"
#include "../PatchClientCtrl/PatchClientCtrl.h"
#include "common/KEPUtil/URL.h"
#include "KEPStar.h"
#include "UnicodeWindowsHelpers.h"
#include "KanevaErrorFile.h"
#include "Common/KEPUtil/WebCall.h"
#include "../KEPUtil/Helpers.h"
#include "KanevaLauncherDlg.h"

#define PROJECT_NAME "Kaneva Launcher"
#define REG_APP_NAME "KanevaLauncher"
#define REG_KEY_NAME "Status"

namespace KEP {
using std::string;

static LogInstance("Instance");
CKanevaLauncherApp appInstance; ///< singleton global app instance

// DRF - Added
static std::string g_patchMetrics;

BEGIN_MESSAGE_MAP(CKanevaLauncherApp, CWinApp)
ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

static std::string getAppCommandLine() {
	auto pApp = AfxGetApp();
	return Utf16ToUtf8(pApp->m_lpCmdLine).c_str();
}

static std::string getAppCommandLineParameter(std::string key) {
	auto strVec = StrSplit(getAppCommandLine());
	key = std::string("-") + key;
	for (auto& str : strVec)
		if (str.rfind(key, 0) == 0)
			return str.c_str() + key.size();
	return "";
}

static bool existsAppCommandLineParameter(std::string key) {
	auto strVec = StrSplit(getAppCommandLine());
	key = std::string("-") + key;
	for (auto& str : strVec)
		if (str.rfind(key, 0) == 0)
			return true;
	return false;
}


LauncherCommandLineInfo::LauncherCommandLineInfo(void) :
		CCommandLineInfo(),
		m_bHelp(FALSE),
		m_bFromURL(FALSE),
		m_bSuccess(TRUE),
		numparameters(0) {
}

void LauncherCommandLineInfo::ParseParam(const wchar_t* lpszParamW, BOOL bSwitch, BOOL /*bLast*/) {
	std::string strParam = Utf16ToUtf8(lpszParamW);
	const char* lpszParam = strParam.c_str();
	if (bSwitch) {
		if (0 == lstrcmpiA("help", lpszParam)) {
			m_bHelp = TRUE;
		} else if (0 == lstrcmpiA("url", lpszParam)) {
			m_bFromURL = TRUE;
		} else {
			// the for loop enables 'compound' switches like "/XYZ"
			BOOL bContinue = TRUE;
			for (int i = 0; (i < lstrlenA(lpszParam)) && m_bSuccess && bContinue; i++) {
				switch (lpszParam[i]) {
					case '?':
					case 'h':
					case 'H':
						m_bHelp = TRUE;
						break;
					default:
						m_bSuccess = bContinue = FALSE;
						break;
				}
			}
		}
	} else {
		m_strParameter[numparameters] = lpszParam;
		numparameters++;
	}
}

CKanevaLauncherApp::~CKanevaLauncherApp() {
	if (!m_gameServerList)
		return;
	POSITION pos = m_gameServerList->GetHeadPosition();
	while (pos != NULL) {
		CGameServer* server = (CGameServer*)m_gameServerList->GetNext(pos);
		delete server;
	}
	delete m_gameServerList;
}

std::string CKanevaLauncherApp::WokUrl() const {
	static std::string wokUrl;
	if (wokUrl.empty()) {
		std::string gameIdStr;
		StrBuild(gameIdStr, m_gameId);
		wokUrl = StarWokUrl(gameIdStr);
	}
	return wokUrl;
}

void CKanevaLauncherApp::ClearServerPatchUrls() {
	m_serverPatchUrls.clear();
}

BOOL CKanevaLauncherApp::InitInstance() 
{
	m_parentPatchesCached = false; // drf - bug fix
	m_childPatchesCached = false; // drf - bug fix

	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Install Crash Reporter (must be before creating any threads)
	CR_Install();

	// Initialize Logger
	// NOTE: Patcher Has 2 Loggers At Same Time:
	// - KanevaLauncherExe - statically linked
	// - PatchClientCtrl - dynamically linked
	LogInitLauncher();
	LogInfo("====================================================================");
	LogInfo("===== " << K_LAUNCHER_EXE << " - v" << VersionApp());
	LogInfo("====================================================================");
	LogInfo("appPath=" << PathApp());
	LogInfo("cmdLine=" << Utf16ToUtf8(::GetCommandLineW()).c_str());

	// Already Running ?
	auto launcherSem = ::CreateSemaphoreW(NULL, 1L, INT_MAX, K_LAUNCHER_SEMAPHORE_NAME);
	bool launcherRunning = (GetLastError() == ERROR_ALREADY_EXISTS);
	if (launcherRunning) {
		LogFatal("LAUNCHER ALREADY RUNNING");
		if (launcherSem)
			::CloseHandle(launcherSem);
		return FALSE;
	}

	// Parse Command Line
	ParseCommandLine(m_cmdInfo);
	bool ok = m_cmdInfo.m_bFromURL ? PullParamsFromURL() : PullParamsFromCommandLine();
	if (!ok) {
		LogFatal("ParseCommandLine() FAILED");
		if (launcherSem)
			::CloseHandle(launcherSem);
		return FALSE;
	}

	LogInfo("cmdLine='" << m_cmdLine << "'");
	LogInfo("starId=" << m_gameId);
	LogInfo("wokUrl='" << WokUrl() << "'");
	LogInfo("playNowGuid='" << m_playNowGuid << "'");

	// Install Runtime Reporter
	RR_Install();

	// Set Runtime Reporter State Running
	RuntimeReporter::ProfileStateRunning();

	// Disable Sleep During Patching
	if (!SetThreadExecutionState(ES_AWAYMODE_REQUIRED | ES_CONTINUOUS | ES_SYSTEM_REQUIRED))
		LogWarn("SetThreadExecutionState(DISABLE_SLEEP) FAILED");


	// Collect INFO on patches
	ConfigValues cfg;
	cfg.LoadValues(m_gameId);
	auto serverListGameUrl = cfg.ServerListUrl(m_gameId);
	m_parentPatchesCached = getPatchInfo(m_gameId, m_parentPatches, serverListGameUrl);
	if (!m_parentPatchesCached) {
		LogWarn("getPatchInfo(parentPatches) FAILED");
	}

	// End Collect patch info

	// Perform All Patching & Launch Client
	if (!PatchAndPlay(g_patchMetrics))
	{
		LogFatal("PatchAndPlay() FAILED");
		return FALSE;
	}

	// Testing Only!
	std::string pathCrash = PathAdd(PathApp(), "crash.txt");
	if (FileHelper::Exists(pathCrash)) {
		LogFatal("===== FORCED CRASH =====");
		CrashReporter::ForcedCrash(3, "crash.txt");
	}

	// Set Runtime Reporter State Stopped
	RuntimeReporter::ProfileStateStopped();
	RuntimeReporter::WaitForReportResponseComplete();

	WebCallerLogInstances();

	// Runtime Too Long Forced Report ? (>1 hour)
	//	if (RuntimeReporter::RuntimeSec() > (60.0 * 60.0)) {
	//		LogWarn("ForcedCrash(CR_CRASH_TYPE_REPORT_ONLY, 'RUNTIME') ...");
	//		CrashReporter::ForcedCrash(CR_CRASH_TYPE_REPORT_ONLY, "RUNTIME");
	//	}

	// Close Semaphore
	if (launcherSem)
		::CloseHandle(launcherSem);
	
	return true;
}

// Assigns Game Id From Url (eg. 'kaneva://3296' -> 3296)
bool CKanevaLauncherApp::AssignGameIdFromUrl(const std::string& url) {
	// Assure Prefix 'kaneva://'
	m_gameUrl = url;
	StpUrl::CheckPrefix(m_gameUrl);

	// Extract Game Id From Url
	std::string gameName;
	StpUrl::ExtractGameName(m_gameUrl, gameName);
	m_gameId = atol(gameName.c_str());
	if (m_gameId <= 0) {
		LogWarn("Bad Game Id - Using " << DEFAULT_GAME_ID);
		m_gameId = DEFAULT_GAME_ID;
		m_gameUrl = DEFAULT_GAME_ID_URL;
		m_patchUrlOverride = HACK_PATCH_URL;
	}

	return true;
}

bool CKanevaLauncherApp::ValidateServerPatchUrls(CGameServer* pGS, TimeMs timeoutMs) 
{
	// Validate Server Patch Urls
	size_t failed = 0;
	size_t size = pGS->GetPatchUrls().size();
	LogInfo(pGS->ToString() << " urls=" << size);
	for (const auto& url : pGS->GetPatchUrls()) {
		std::string urlVI = PathAdd(url, "versioninfo.dat");

		// Get Unique Url Response Times
		bool ok = false;
		auto it = m_serverPatchUrls.find(url);
		if (it != m_serverPatchUrls.end()) {
			ok = it->second.m_valid;
		}
		else {
			GBPO gbpo(urlVI);
			LogInfo(" ... '" << urlVI << "' timeoutMs=" << FMT_TIME << gbpo.timeoutMs);
			ok = GetBrowserPage(gbpo);
			m_serverPatchUrls[url] = ServerStatus(ok, gbpo.resultTimeMs);
		}

		if (!ok)
			++failed;
	}

	// Valid Patch Url Found ?
	return (failed != size);
}

bool CKanevaLauncherApp::PullParamsFromURL() {
	// Game Id Url Must Be First Param!
	std::string url = m_cmdInfo.m_strParameter[0];

	URL urlParse = URL::parse(url);
	urlParse.setScheme("kaneva");

	// Assign Game Id From Url.
	m_gameUrl = urlParse.str(); // This will be the real test.  Does it behave when url is not molested?
	m_gameId = atol(urlParse.host().c_str());
	if (m_gameId <= 0) {
		LogWarn("Bad Game Id - Using " << DEFAULT_GAME_ID);
		m_gameId = DEFAULT_GAME_ID;
		m_gameUrl = DEFAULT_GAME_ID_URL;
	}

	auto mapIter = urlParse.paramMap().find("userid");
	if (mapIter != urlParse.paramMap().end())
		m_email = mapIter->second;

	mapIter = urlParse.paramMap().find("parent");
	if (mapIter != urlParse.paramMap().end())
		m_parentGameIdStr = mapIter->second;

	m_patchUrlOverride = HACK_PATCH_URL;

	mapIter = urlParse.paramMap().find("cmd");
	if (mapIter != urlParse.paramMap().end())
		m_cmdLine = mapIter->second;

	mapIter = urlParse.paramMap().find("playnowguid");
	if (mapIter != urlParse.paramMap().end())
		m_playNowGuid = mapIter->second;

	return true;
}

bool CKanevaLauncherApp::PullParamsFromCommandLine() {
	// Game Id Url Must Be First Param!
	std::string url = m_cmdInfo.m_strParameter[0];
	StpUrl::CheckPrefix(url);

	// Extract Game Id From Url
	AssignGameIdFromUrl(url);

	// email optional second param
	if (m_cmdInfo.numparameters >= 2)
		m_email = m_cmdInfo.m_strParameter[1];

	m_cmdLine = getAppCommandLine();

	// was the parent game id passed in?  If not we'll assume gameId
	// extracted from the URL is the id we use for patching
	m_parentGameIdStr = getAppCommandLineParameter("p");

	return true;
}

bool CKanevaLauncherApp::PatchAndPlay(std::string& patchMetrics) 
{
	// See if we need to override the game ID with the parent game ID
	if (!m_parentGameIdStr.empty()) {
		int temp = atol(m_parentGameIdStr.c_str());
		if (temp > 0)
			m_gameId = temp;
	}

	//Call the patcher
	PatchClientController* patcherController = NULL;
	PatchClientController::PatcherState ps = PatchClientController::PatchAndPlayAsync(patcherController, m_gameId, m_gameUrl.c_str(), NULL, NULL, m_patchUrlOverride.c_str(), getAppCommandLine().c_str(), m_parentPatches, m_childPatches);
	if (ps == PatchClientController::PatcherState::ErrorPatcherAlreadyRunning)
	{
		AfxMessageBox(L"Patcher alrady running", MB_ICONERROR);
		return false;
	}
	else if (ps == PatchClientController::PatcherState::Error || patcherController == NULL)
	{
		AfxMessageBox(L"Error creating patcher", MB_ICONERROR);
		return false;
	}

	DoLauncherDlg(m_pMainWnd, patcherController);

	ps = patcherController->GetState();

	// DRF - Added - Get Patcher Metrics
	wchar_t metricsStrW[1024] = L"";
	patchMetrics = Utf16ToUtf8(metricsStrW);

	return true;
}

bool CKanevaLauncherApp::getPatchInfo(ULONG gameId, std::vector<PatchInfo>& patchesInfo, const char* serverListUrl) {
	CServerObList gameServerList;
	// Get Patches Info Progressively Timing Out (1000ms -> 3000ms)
	TimeMs timeoutMs = 1000.0;
	while (!getPatchInfoTimeout(gameServerList, patchesInfo, timeoutMs)) {
		LogWarn("GetPatchInfoTimeout() FAILED - timeoutMs=" << FMT_TIME << timeoutMs);
		timeoutMs += 1000.0;
		if (timeoutMs > 3000.0) 
		{
			std::string errMsg;
			StrBuild(errMsg, "No Game Servers Found [" << gameId << "]");
			LogError(errMsg);
			PostQuitMessage(-1);
			return false;
		}
	}

	return true;
}

bool CKanevaLauncherApp::getPatchInfoTimeout(
	CObList& servers, 
	std::vector<PatchInfo>& patchesInfo, 
	TimeMs timeoutMs
) {
	// DRF - Added
	ClearServerPatchUrls();

	// Override Patch Url ?
	bool ok = false;
	bool overridePatchUrl = (m_patchUrlOverride.find("http") != std::string::npos);
	if (!overridePatchUrl) {
		// For All Game Servers
		for (POSITION pos = servers.GetHeadPosition(); pos;) {
			auto pGS = (CGameServer*)servers.GetNext(pos);
			if (!pGS)
				continue;

			// Validate Server Patch Urls
			if (!ValidateServerPatchUrls(pGS, timeoutMs)) {
				LogWarn("ValidateServerLocations() FAILED - timeoutMs=" << FMT_TIME << timeoutMs);
			}

			// Accumulate Valid Patch Urls Into Patch List
			for (const auto& url : pGS->GetPatchUrls()) {
				// DRF - Added - Get Server Location Status
				ServerStatus serverStatus;
				auto it = m_serverPatchUrls.find(url);
				if (it != m_serverPatchUrls.end())
					serverStatus = it->second;

				// Only Add Valid Patch Locations
				if (serverStatus.m_valid) {
					PatchInfo newEntry;
					newEntry.patchUrl = url;
					newEntry.patchType = pGS->GetPatchType();
					newEntry.patchPath = pGS->GetPatchPath();
					newEntry.serverStatus = serverStatus;
					patchesInfo.push_back(newEntry);
					ok = true;
				}
			}
		}
	}
	else {
		auto pGS = new CGameServer();
		std::string url = m_patchUrlOverride;
		pGS->AddPatchURL(url);

		// Validate Server Patch Urls
		if (!ValidateServerPatchUrls(pGS, timeoutMs)) {
			LogWarn("ValidateServerLocations() FAILED - timeoutMs=" << FMT_TIME << timeoutMs);
		}

		// DRF - Added - Get Server Location Status
		ServerStatus serverStatus;
		auto it = m_serverPatchUrls.find(url);
		if (it != m_serverPatchUrls.end())
			serverStatus = it->second;

		// Only Add Valid Patch Locations
		if (serverStatus.m_valid) {
			PatchInfo newEntry;
			newEntry.patchUrl = url;
			newEntry.serverStatus = serverStatus;
			patchesInfo.push_back(newEntry);
			ok = true;
		}

		delete pGS;
	}

	return ok;
}

///////////////////////////////////////////////////////////////////////////////
// RuntimeReporter Support
///////////////////////////////////////////////////////////////////////////////
#include "common/KEPUtil/WebCall.h"
using namespace RuntimeReporter;
static void RR_AppCallback(RuntimeReporter::CallbackStruct& cbs) {
	appInstance.RR_AppCallback(cbs);
}

bool CKanevaLauncherApp::RR_Install() {
	// Install Runtime Reporter
	return RuntimeReporter::Install(REG_APP_NAME, REG_KEY_NAME, WokUrl() + "runtimeReport.aspx?", ::KEP::RR_AppCallback);
}

static void RR_WebCallerParams(RuntimeReporterParams& params, const std::string& wcId, const std::string& paramId) {
	// Build WebCaller Runtime Report Params String
	WebCaller* pWC = WebCaller::GetInstance(wcId, false);
	if (pWC) {
		params.Add(paramId + ".webCalls", pWC->CallsCompleted());
		params.Add(paramId + ".webCallsMs", pWC->CallTimeMs());
		params.Add(paramId + ".webCallsResponseMs", pWC->ResponseTimeMs());
		params.Add(paramId + ".webCallsBytes", pWC->ResponseBytes());
		params.Add(paramId + ".webCallsErrored", pWC->CallsErrored());
	}
}

void CKanevaLauncherApp::RR_AppCallback(RuntimeReporter::CallbackStruct& cbs) {
	std::string strParams;
	RuntimeReporterWebParams params;

	// Append Report Type Parameters
	switch (cbs.type) {
		case REPORT_TYPE_RUNNING: {
			params.Add("playNowGuid", m_playNowGuid);
			strParams = params.GetString();
		} break;

		case REPORT_TYPE_STOPPED:
		case REPORT_TYPE_CRASHED: {
			// Append WebCaller Stats
			RR_WebCallerParams(params, "RuntimeReporter", "RR");
			strParams = params.GetString();

			// Append Patcher Metrics
			strParams += g_patchMetrics;
		} break;
	}

	// Update CallbackStruct
	cbs.params = strParams;
}

///////////////////////////////////////////////////////////////////////////////
// CrashReporter Support
///////////////////////////////////////////////////////////////////////////////
using namespace CrashReporter;
static void CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	appInstance.CR_AppCallback(cbs);
}

bool CKanevaLauncherApp::CR_Install() {
	// Install Crash Reporter (default version)
	CR_MINIDUMP crMinidump = CR_MINIDUMP_LOW;
	bool ok = CrashReporter::Install(K_LAUNCHER_NAME, "", ::KEP::CR_AppCallback, crMinidump, false);

	// Add Crash Report Attachments
	AddFile(PathAdd(PathApp(), "*.log*"), "Launcher Log");
	AddFile(PathAdd(PathApp(), "KepDiag.txt"), "Launcher DxDiag");
	AddRegistryKey("HKEY_CURRENT_USER\\Software\\Kaneva");

	return ok;
}

void CKanevaLauncherApp::CR_AppCallback(CrashReporter::CallbackStruct& cbs) {
	// Is Not Crash (report only) ?
	if (cbs.isNotCrash)
		return;

	// Identify As 'Launcher Crash' If No Known Reason
	ErrorData* pED = CrashReporter::GetErrorData();
	bool noReason = (pED && pED->reason.empty());
	if (noReason) {
		pED->reason = "Launcher Crash";
		pED->type = ErrorData::TYPE_CRASH;
	}

	// Set Runtime Reporter State
	if (pED && pED->type != ErrorData::TYPE_CRASH)
		RuntimeReporter::ProfileStateStopped();
	else
		RuntimeReporter::ProfileStateCrashed();

	// Wait For Runtime Report Response Complete
	RuntimeReporter::WaitForReportResponseComplete();
}

} // namespace KEP