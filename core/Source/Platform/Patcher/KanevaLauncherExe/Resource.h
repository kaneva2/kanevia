//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by KLauncher.rc
//
#define IDS_WOK_INSTALL_NOT_FOUND       102
#define IDS_PATCHCLIENTCTRL_NOT_FOUND   103
#define IDS_CLASS_FACTORY_CREATE_FAILED 104
#define IDS_QUERY_INTERFACE_FAILED      105
#define IDS_NO_SERVERS_AVAILABLE        106
#define IDS_ERROR_OBTAINING_SERVERS     107
#define IDS_EXCEPTION_OBTAINING_SERVERS 108

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
