///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "afxwin.h"

#include "..\PatchLib\PatchLibGlobals.h"

#include "StaticEx.h"
#include "CKanevaButton.h"
#include "KanevaDialogSkin.h"
#include "..\PatchClientCtrl\PatchClientCtrl.h"
#include "ImageLib\ImageStatic.h"
#include "ImageLib\ImageButton.h"
#include "..\KanevaLauncherLib\KanevaLauncherRes.h"

namespace KEP {

class CLoginProgressDlg : public CKanevaDialogSkin {
	DECLARE_DYNAMIC(CLoginProgressDlg)

public:
	CLoginProgressDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CLoginProgressDlg();

	// Dialog Data
	enum { IDD = IDD_LOGIN_PROGRESS_DIALOG };

	virtual BOOL SetTransparent(BYTE bAlpha);

	virtual BOOL SetTransparentColor(COLORREF col, BOOL bTrans = TRUE);

	void StartProgress(UINT idNewMsg = 0);
	void StopProgress();

protected:
	typedef CKanevaDialogSkin baseclass;
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

protected:
	CStaticEx m_lblConnect;
	CStaticEx m_lblDots;
	CBrush m_bkBrush;

	CStringA m_dotsStr;

	DECLARE_MESSAGE_MAP()
public:
};

} // namespace KEP