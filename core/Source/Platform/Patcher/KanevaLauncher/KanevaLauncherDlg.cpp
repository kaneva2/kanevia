///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "KanevaLauncherDlg.h"

#include <comdef.h>

#include "KEPConfig.h"
#include "KEPFileNames.h"
#include "StpUrl.h"
#include "KEPException.h"
#include "UnicodeMFCHelpers.h"

#include "../KanevaLauncherLib/KanevaLauncherRes.h"
#include "../KanevaRes/resource.h"

#include "../PatchLib/ConfigValues.h"
#include "../PatchClientCtrl/ProgressDlg.h"
#include "../PatchClientCtrl/resource.h"

// PatchLib
#include "../PatchLib/CommUtil.h"
#include "../PatchLib/GameServer.h"
#include "common/include/NetworkCfg.h"
#include "common/KEPUtil/WebCall.h"
#include "common/KEPUtil/RuntimeReporter.h"
#include "common/include/KEPHelpers.h"
#include "../PatchLib/HTTPWorker.h"

// Careful there are namespace conflicts between std and Gdi.  Try not to include all of std
using namespace Gdiplus;
using std::string;
using std::wstring;
using std::vector;

#ifndef BACKGROUND_COLOR
#define BACKGROUND_COLOR RGB(0xff, 0xff, 0xff)
#endif

#ifndef TOOLBAR_TEXTCOLOR
#define TOOLBAR_TEXTCOLOR RGB(0xB3, 0xD6, 0xF2)
#endif

#define FONT_NAME "Arial"

#define BROWSER_TOOLBAR_TEXCOLOR RGB(0x28, 0x50, 0x5f)

#define TIMER_ID_CANCEL_TOPMOST 9999
#define TIMER_ID_WAITED_TOOLONG 9998

namespace KEP {

static LogInstance("Instance");

extern string g_emailadd;

// DRF - Added
PatchMetrics g_kl_pm = { 0 };

static void RR_WebCallerParams(RuntimeReporterParams& params, const string& wcId, const string& paramId) {
	// Build WebCaller Runtime Report Params String
	WebCaller* pWC = WebCaller::GetInstance(wcId, false);
	if (pWC) {
		params.Add(paramId + ".webCalls", pWC->CallsCompleted());
		params.Add(paramId + ".webCallsMs", pWC->CallTimeMs());
		params.Add(paramId + ".webCallsResponseMs", pWC->ResponseTimeMs());
		params.Add(paramId + ".webCallsBytes", pWC->ResponseBytes());
		params.Add(paramId + ".webCallsErrored", pWC->CallsErrored());
	}
}

string PatchMetricsStr() {
	RuntimeReporterWebParams params;

	// Append WebCaller Stats
	WebCallerLogInstances();
	RR_WebCallerParams(params, "GetBrowserPage", "GBP");
	RR_WebCallerParams(params, "Downloader", "DL");

	// Append Patcher Metrics
	params.Add("DL.NumOk", g_kl_pm.dlNumOk + g_ru_pm.dlNumOk);
	params.Add("DL.NumErr", g_kl_pm.dlNumErr + g_ru_pm.dlNumErr);
	params.Add("DL.Ms", g_kl_pm.dlMs + g_ru_pm.dlMs);
	params.Add("DL.Bytes", g_kl_pm.dlBytes + g_ru_pm.dlBytes);
	params.Add("DL.Retries", g_ru_pm.dlRetries);
	params.Add("DL.RetriesErr", g_ru_pm.dlRetriesErr);
	params.Add("ZIP.NumOk", g_hw_pm.zipNumOk);
	params.Add("ZIP.NumErr", g_hw_pm.zipNumErr);
	params.Add("ZIP.Ms", g_hw_pm.zipMs);
	params.Add("ZIP.Bytes", g_hw_pm.zipBytes);
	params.Add("PAK.NumOk", g_hw_pm.pakNumOk);
	params.Add("PAK.NumErr", g_hw_pm.pakNumErr);
	params.Add("PAK.Ms", g_hw_pm.pakMs);
	params.Add("PAK.Bytes", g_hw_pm.pakBytes);
	params.Add("patchUrl", g_hw_pm.patchUrl);
	params.Add("patchMs", g_hw_pm.patchMs);
	params.Add("patchRv", g_hw_pm.patchRv);

	return params.GetString();
}

HINSTANCE kanevaSkinRes = 0; // TODO need to free
HINSTANCE KanevaSkinRes() {
	return kanevaSkinRes;
}

class LauncherDlgCommandLineInfo : public CCommandLineInfo {
public:
	LauncherDlgCommandLineInfo() {}

	bool m_bDevEnv = false;

	virtual void ParseParam(const char* lpszParam, BOOL bSwitch, BOOL bLast) {
		if (bSwitch) {
			//3dapp editor passes -dev parameter to indicate this is an auto-launched session from Editor in which case we will
			//retry in CCommUtil::getGameServers if wokweb returns empty result while local server is still being started.
			if (0 == lstrcmpiA("dev", lpszParam)) {
				m_bDevEnv = true;
			}
		}
	}
};

bool GetAppDriveDir(wchar_t* drive, wchar_t* dir) {
	if (!drive || !dir) {
		LogError("null drive/dir");
		return false;
	}

	// Get App Drive / Dir
	static bool init = false;
	static wstring s_drive;
	static wstring s_dir;
	if (!init) {
		init = true;
		string s_appPath = ::PathApp();
		s_drive = Utf8ToUtf16(StrStripRightFirst(s_appPath, "\\"));
		s_dir = Utf8ToUtf16(StrStripLeftFirst(s_appPath, ":"));
	}

	// Return Drive / Dir
	StrCpyW(drive, s_drive.c_str());
	StrCpyW(dir, s_dir.c_str());

	return true;
}

void ExtractNameAndExtension(const char* buffer, std::string& name, std::string& ext) {
	std::string cb = buffer;
	size_t index = cb.find(".");
	size_t len = cb.length();
	name = cb.substr(0, index);
	ext = cb.substr(index + 1, len - (index + 1));
}

// helper fn to launch this dlg w/o having to include KanevaLauncherDlg.h
DWORD DoLauncherDlg(CWnd*& mainWnd, PatchClientController* patcherController) {
	// Initialize Google Analytics
	wchar_t drive[_MAX_DRIVE] = L"";
	wchar_t dir[_MAX_DIR] = L"";
	GetAppDriveDir(drive, dir);

	AfxEnableControlContainer();
	kanevaSkinRes = AfxGetInstanceHandle();
	CKanevaDialogSkin::SetResourceInstance(kanevaSkinRes);

	auto pApp = AfxGetApp();
	pApp->m_pCmdInfo = new LauncherDlgCommandLineInfo();
	pApp->ParseCommandLine(*(pApp->m_pCmdInfo));

	// Initialize GDI+.
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// Create Launcher Dialog And Do It (actually runs patcher)
	CKanevaLauncherDlg ddg(patcherController);
	mainWnd = &ddg;
	ddg.DoModal();

	return NO_ERROR;
}

// CKanevaLauncherDlg dialog
CKanevaLauncherDlg::CKanevaLauncherDlg(PatchClientController* patcherController, CWnd* pParent /*=NULL*/) :
		CKanevaDialogSkin(CKanevaLauncherDlg::IDD, pParent),
		m_childGameId(0),
		m_hIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME)),
		m_totaldownload(0),
		m_estimate(0),
		m_complete(false),
		m_progressDlg(0),
		m_loginProgDlg(0),
		m_patcherController(patcherController),
		m_parentPatchesCached(false), // drf - bug fix
		m_childPatchesCached(false), // drf - bug fix
		m_gameExe(K_CLIENT_EXE) // default name of client exe
{
	m_patchUrlOverride = patcherController->GetPatchUrlOverride();

	string gameIdStr;
	string temp(patcherController->GetStpUrl());
	StpUrl::ExtractGameName(temp, gameIdStr);
	m_childGameId = atol(gameIdStr.c_str());
	if (m_childGameId == patcherController->GetGameId())
		m_childGameId = 0; // not going to a child

	LogInfo("<" << this << ">: OK");
	LogInfo(" ... gameId=" << patcherController->GetGameId());
	LogInfo(" ... stpUrl='" << (const char*)patcherController->GetStpUrl() << "'");
	LogInfo(" ... email='" << (const char*)patcherController->GetEmail() << "'");
	LogInfo(" ... patchUrlOverride='" << (const char*)m_patchUrlOverride.c_str() << "'");
	LogInfo(" ... gameExe='" << (const char*)m_gameExe << "'");
}

CKanevaLauncherDlg::~CKanevaLauncherDlg() {
	LogInfo("<" << this << "> : OK");

	if (m_progressDlg)
		delete m_progressDlg;

	if (m_loginProgDlg)
		delete m_loginProgDlg;
}

string CKanevaLauncherDlg::getAppCommandLineParameter(string key) {
	auto strVec = StrSplit(m_patcherController->GetLauncherCommandLine());
	key = string("-") + key;
	for (auto& str : strVec)
		if (str.rfind(key, 0) == 0)
			return str.c_str() + key.size();
	return "";
}

bool CKanevaLauncherDlg::existsAppCommandLineParameter(string key) {
	auto strVec = StrSplit(m_patcherController->GetLauncherCommandLine());
	key = string("-") + key;
	for (auto& str : strVec)
		if (str.rfind(key, 0) == 0)
			return true;
	return false;
}

void CKanevaLauncherDlg::DoDataExchange(CDataExchange* pDX) {
	CKanevaDialogSkin::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KANEVAWEBSITE, m_kanevacom);
	DDX_Control(pDX, IDC_TECH, m_techsupport);
	DDX_Control(pDX, IDC_FORUM, m_forum);
	DDX_Control(pDX, IDC_QUITLAUNCHER, m_exit);
	DDX_Control(pDX, IDC_BROWSERHEADER1, m_browserheader1);
	DDX_Control(pDX, IDC_BROWSERHEADER2, m_browserheader2);
	DDX_Control(pDX, IDC_FORUM3, m_buttonhelp);
	DDX_Control(pDX, IDC_VERSION, m_version);
}

BEGIN_MESSAGE_MAP(CKanevaLauncherDlg, CKanevaDialogSkin)
ON_MESSAGE(DM_GETDEFID, OnGetDefID)
ON_MESSAGE(WM_STARTPATCH, OnStartPatch)
ON_MESSAGE(WM_CLOSEPATCH, OnCloseApp)
ON_MESSAGE(WM_APPSTATUS, OnAppStatus)
ON_MESSAGE(WM_ALLOWCLOSEPATCHER, OnAllowClosePatcher)
ON_WM_SYSCOMMAND()
ON_WM_PAINT()
ON_WM_CREATE()
ON_WM_DESTROY()
ON_WM_QUERYDRAGICON()
ON_WM_CTLCOLOR()
ON_WM_ACTIVATEAPP()
ON_WM_TIMER()
ON_BN_CLICKED(IDC_KANEVAWEBSITE, OnBnClickedKanevawebsite)
ON_BN_CLICKED(IDC_FORUM, OnBnClickedForum)
ON_BN_CLICKED(IDC_QUITLAUNCHER, OnCancel)
ON_BN_CLICKED(IDC_FORUM3, OnBnClickedSupport)
END_MESSAGE_MAP()

//Disable the enter key.
LRESULT CKanevaLauncherDlg::OnGetDefID(WPARAM wp, LPARAM lp) {
	return MAKELONG(0, DC_HASDEFID);
}

LRESULT CKanevaLauncherDlg::OnAppStatus(WPARAM wp, LPARAM lp) {
	TimerInfo* info = (TimerInfo*)lp;
	m_complete = wp != 0;
	m_estimate = info->estimatedtime;
	m_totaldownload = info->downloadsize;
	return TRUE;
}

LRESULT CKanevaLauncherDlg::OnAllowClosePatcher(WPARAM wp, LPARAM lp) {
	m_exit.ShowWindow(SW_SHOW);
	return TRUE;
}

LRESULT CKanevaLauncherDlg::OnCloseApp(WPARAM wp, LPARAM lp) {
	return FALSE;
}

LRESULT CKanevaLauncherDlg::OnStartPatch(WPARAM wp, LPARAM lp) {
	LogInfo("OK");
	return TRUE;
}

// DRF - This is the Windows call into the Launcher, where it actually begins!
// Initiate the Main Kaneva Launcher Dialog.
BOOL CKanevaLauncherDlg::OnInitDialog() {
	wchar_t drive[_MAX_DRIVE] = L"";
	wchar_t dir[_MAX_DIR] = L"";
	wchar_t buffer[_MAX_PATH] = L"";
	GetAppDriveDir(drive, dir);
	_wmakepath_s(buffer, _countof(buffer), drive, dir, 0, 0);
	m_baseDir = Utf16ToUtf8(buffer).c_str();

	CKanevaDialogSkin::OnInitDialog();
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	SetWindowText(Utf8ToUtf16(OLD_WOK_GAME_NAME).c_str());
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	m_bkBrush.CreateSolidBrush(BACKGROUND_COLOR);

	// Control scope of resource context change
	{
		ResourceContext rc(CKanevaDialogSkin::GetResourceInstance());

		m_kanevacom.SetImageId(IDR_BUTTON_KANEVACOM);
		m_kanevacom.SetImageId(IDR_BUTTON_KANEVACOM_CLICK, Pressed, true);
		m_kanevacom.SetTextAttributes(FONT_NAME, 10, Enabled, TOOLBAR_TEXTCOLOR, true);
		m_kanevacom.SetTextAttributes(FONT_NAME, 10, Pressed, TOOLBAR_TEXTCOLOR, true);
		m_kanevacom.UseHandPointer(true);

		m_buttonhelp.SetImageId(IDR_BUTTON_HELP);
		m_buttonhelp.SetImageId(IDR_BUTTON_HELP_CLICK, Pressed, true);
		m_buttonhelp.SetTextAttributes(FONT_NAME, 10, Enabled, TOOLBAR_TEXTCOLOR, true);
		m_buttonhelp.SetTextAttributes(FONT_NAME, 10, Pressed, TOOLBAR_TEXTCOLOR, true);
		m_buttonhelp.UseHandPointer(true);

		m_techsupport.SetImageId(IDR_BUTTON_SUPPORT);
		m_techsupport.SetImageId(IDR_BUTTON_SUPPORT_CLICK, Pressed, true);
		m_techsupport.SetTextAttributes(FONT_NAME, 10, Enabled, TOOLBAR_TEXTCOLOR, true);
		m_techsupport.SetTextAttributes(FONT_NAME, 10, Pressed, TOOLBAR_TEXTCOLOR, true);
		m_techsupport.UseHandPointer(true);

		m_forum.SetImageId(IDR_BUTTON_FORUM);
		m_forum.SetImageId(IDR_BUTTON_FORUM_CLICK, Pressed, true);
		m_forum.SetTextAttributes(FONT_NAME, 10, Enabled, TOOLBAR_TEXTCOLOR, true);
		m_forum.UseHandPointer(true);

		m_exit.SetImageId(IDR_QUITBUTTON, Enabled, true);
		m_exit.UseHandPointer(true);

		m_browserheader1.SetImageId(IDR_BROWSERHEADER);
		m_browserheader2.SetImageId(IDR_BROWSERHEADER);
		m_browserheader1.SetTextAttributes(FONT_NAME, 9, BROWSER_TOOLBAR_TEXCOLOR, true, false);
		m_browserheader2.SetTextAttributes(FONT_NAME, 9, BROWSER_TOOLBAR_TEXCOLOR, true, false);
		m_browserheader1.OffsetText(10, 5);
		m_browserheader2.OffsetText(10, 3);

		m_browserheader1.ShowWindow(SW_HIDE);
		m_browserheader2.ShowWindow(SW_HIDE);
	}

	//Create the Web Controls on the Dialog.
	m_browserleft.CreateFromControl(this, IDC_BROWSER);
	m_browserright.CreateFromControl(this, IDC_BROWSER2);

	//Read the Game Config
	ConfigValues cfg;
	cfg.LoadValues(m_patcherController->GetGameId());

	// if going to a child and not prod, must have parent game'd network.cfg before patching
	if (m_childGameId != 0 && m_childGameId != m_patcherController->GetGameId()) {
		if (m_patcherController->GetGameId() != WOK_GAME_ID) {
			struct _stat stt;
			if (_stat(cfg.m_gamePath + "\\network.cfg", &stt) != 0) {
				AfxMessageBox(L"For 3D Apps not in production, you must patch WOK before accessing the 3D App", MB_ICONEXCLAMATION);
				PostQuitMessage(8);
				return FALSE;
			}
		}
	}

	string gameInfoUrl = cfg.m_gameInfoUrl;
	string::size_type p = gameInfoUrl.find("{0}");
	if (p != string::npos) {
		CStringA gameIdStr;
		gameIdStr.Format("%d", m_patcherController->GetGameId());
		gameInfoUrl = gameInfoUrl.replace(p, 3, gameIdStr);
	}

	try {
		SetImage(IDR_BACKGROUND2);

		SetStyle(LO_DEFAULT); // resize dialog to the size of the bitmap

		SetWindowTextUtf8(*AfxGetMainWnd(), m_gameName);

		// Create the login progress dialog
		m_loginProgDlg = new CLoginProgressDlg();
		m_loginProgDlg->Create(IDD_LOGIN_PROGRESS_MINI_DIALOG, this);
		m_loginProgDlg->SetTransparent(0);
		m_loginProgDlg->ShowWindow(SW_HIDE);

		// Display a minimalist patcher window
		SetWindowPos(NULL, 0, 0, 400, 100, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
		m_browserleft.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_browserright.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_browserheader1.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_browserheader2.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_kanevacom.EnableWindow(FALSE);
		m_techsupport.EnableWindow(FALSE);
		m_forum.EnableWindow(FALSE);
		m_buttonhelp.EnableWindow(FALSE);
		m_kanevacom.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_techsupport.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_forum.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_buttonhelp.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER | SWP_HIDEWINDOW);
		m_version.SetWindowPos(NULL, 130, 5, 0, 0, SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOZORDER);
		SetImage(IDR_BACKGROUND_MINI);

		AlignContentsToWindow();

		EnableEasyMove();

		m_version.SetTextAttributes(FONT_NAME, 10, TOOLBAR_TEXTCOLOR, true);

		// Start Patch
		int parentGameId = 0;
		int gameId = m_patcherController->GetGameId();
		int zoneindex = 0;
		PostMessage(WM_STARTPATCH, parentGameId ? parentGameId : gameId, zoneindex);
	} catch (KEPException* e) {
		CStringA sGameInfo;
		CStringA sErrMsg;
		if (m_gameName.IsEmpty()) {
			sGameInfo.Format("Game ID: %d\n", m_patcherController->GetGameId());
		} else {
			sGameInfo.Format("Game name: %s\n", m_gameName);
		}

		sErrMsg.Format(
			"Unable to obtain game information from server. \n"
			"Please check your Internet connection and firewall settings.\n\n"
			"%s",
			sGameInfo.GetString());

		AfxMessageBoxUtf8(sErrMsg, MB_ICONERROR);
		e->Delete();
		PostMessage(WM_QUIT);
	}

	SetTimer(TIMER_ID_WAITED_TOOLONG, 1000 * 60 * 10, NULL);

	// Show Progress Dlg
	m_progressDlg = new CProgressDlg(m_patcherController,
		K_CLIENT_EXE,
		m_gameName,
		0,
		cfg.m_gamePath,
		1,
		false,
		"0",
		0,
		m_loginProgDlg);
	m_progressDlg->Create(IDD_PROGRESS_MINI);
	m_progressDlg->ShowWindow(true);
	return FALSE;
}

void CKanevaLauncherDlg::AlignContentsToWindow() {
	CRect rect;
	CRect rect2;
	CRect rect3;
	CRect winrect;
	GetClientRect(&winrect);

	//Move the progress Dialog.
	m_loginProgDlg->GetWindowRect(&rect);
	rect.top += 30;
	rect.bottom += 30;
	m_loginProgDlg->MoveWindow(&rect);

	//Keep the exit button align to the window size.
	GetClientRect(&rect2);
	m_exit.GetClientRect(&rect3);
	rect3.left = rect2.Width() - 20;
	rect3.right = rect3.left + 15;
	rect3.top = 5;
	rect3.bottom = rect3.top + 15;
	m_exit.MoveWindow(&rect3);

	//Hide exit button by default
	m_exit.ShowWindow(SW_HIDE);
}

//Handles Clearing the Background color of the KanevaLauncherDialog.
HBRUSH CKanevaLauncherDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = CKanevaDialogSkin::OnCtlColor(pDC, pWnd, nCtlColor);
	LOGBRUSH brush;
	switch (nCtlColor) {
		case CTLCOLOR_EDIT:
		case CTLCOLOR_BTN:
		case CTLCOLOR_STATIC:
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(TOOLBAR_TEXTCOLOR);
			pDC->SetBkColor(BACKGROUND_COLOR);
			brush.lbColor = HOLLOW_BRUSH;
			brush.lbHatch = 0;
			brush.lbStyle = 0;
			return CreateBrushIndirect(&brush);
		case CTLCOLOR_DLG:
			return static_cast<HBRUSH>(m_bkBrush.GetSafeHandle());
	}

	return hbr;
}

void CKanevaLauncherDlg::OnDestroy() {
	CKanevaDialogSkin::OnDestroy();
	m_bkBrush.DeleteObject();
}

void CKanevaLauncherDlg::OnCancel() {
	CStringA msg;
	LoadSkinString(msg, IDS_EXIT);

	if (AfxMessageBoxUtf8(msg, MB_YESNO | MB_ICONQUESTION) == IDNO)
		return;

	SendMessage(WM_CLOSEPATCH, 0, 0);

	PostQuitMessage(-1);
}

int CKanevaLauncherDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	//Bring it to front
	SetWindowPos(&CWnd::wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	SetTimer(TIMER_ID_CANCEL_TOPMOST, 10, NULL);
	Sleep(10);

	if (CKanevaDialogSkin::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

void CKanevaLauncherDlg::OnSysCommand(UINT nID, LPARAM lParam) {
	CKanevaDialogSkin::OnSysCommand(nID, lParam);
}

void CKanevaLauncherDlg::OnPaint() {
	if (IsIconic()) {
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	} else {
		CKanevaDialogSkin::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CKanevaLauncherDlg::OnQueryDragIcon() {
	return static_cast<HCURSOR>(m_hIcon);
}

void CKanevaLauncherDlg::OnBnClickedMyprofile() {
	// NOTHING TO DO
}

void CKanevaLauncherDlg::OnBnClickedKanevawebsite() {
	// NOTHING TO DO
}

void CKanevaLauncherDlg::OnBnClickedForum() {
	// NOTHING TO DO
}

//Fires the Not a Member Yet button.
void CKanevaLauncherDlg::OnBnClickedKanevawebsite2() {
	CStringW s;
	ResourceContext c = CKanevaDialogSkin::GetResourceInstance();
	s.LoadString(IDS_SUPPORT_URL);
	ShellExecute(AfxGetMainWnd()->m_hWnd, L"open", s, NULL, L"", SW_SHOWNORMAL);
}

void CKanevaLauncherDlg::OnBnClickedSupport() {
	// NOTHING TO DO
}

void CKanevaLauncherDlg::CreateShortCutLnk(CStringA m_strItem, CStringA basepath, CStringA m_strFile, CStringA _gameurl, int nFolder) {
	CoInitialize(NULL);

	CStringA gameurl;
	gameurl.Format("\"%s\"", _gameurl);

	if (m_strItem.IsEmpty())
		return;

	LPITEMIDLIST pidl;
	HRESULT hr = SHGetSpecialFolderLocation(NULL, nFolder, &pidl);

	wchar_t szPath[_MAX_PATH];
	BOOL f = SHGetPathFromIDListW(pidl, szPath);

	LPMALLOC pMalloc;
	hr = SHGetMalloc(&pMalloc);
	pMalloc->Free(pidl);
	pMalloc->Release();

	CStringA szLinkName = m_strItem;
	szLinkName += ".lnk";
	CStringA m_szCurrentDirectory = Utf16ToUtf8(szPath).c_str();
	CStringA szTemp = szLinkName;
	szLinkName.Format("%s\\%s", m_szCurrentDirectory, szTemp);

	HRESULT hres = NULL;
	IShellLink* psl = NULL;

	hres = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink,
		reinterpret_cast<void**>(&psl));

	if (SUCCEEDED(hres)) {
		IPersistFile* ppf = NULL;
		psl->SetPath(Utf8ToUtf16(m_strFile).c_str());
		psl->SetArguments(Utf8ToUtf16(gameurl).c_str());
		psl->SetWorkingDirectory(Utf8ToUtf16(basepath).c_str());
		hres = psl->QueryInterface(IID_IPersistFile, reinterpret_cast<void**>(&ppf));
		if (SUCCEEDED(hres)) {
			hres = ppf->Save(Utf8ToUtf16(szLinkName).c_str(), TRUE);
			ppf->Release();
		}
		psl->Release();
	}
}

void CKanevaLauncherDlg::OnTimer(UINT_PTR nIDEvent) {
	CKanevaDialogSkin::OnTimer(nIDEvent);

	if (nIDEvent == TIMER_ID_CANCEL_TOPMOST) {
		// Cancel window topmost mode
		SetWindowPos(&CWnd::wndNoTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

		// Kill timer
		KillTimer(nIDEvent);
	} else if (nIDEvent == TIMER_ID_WAITED_TOOLONG) {
		// Allow user to close patcher window if waited to long
		PostMessage(WM_ALLOWCLOSEPATCHER, 0, 0);

		// Kill timer
		KillTimer(nIDEvent);
	}
}

} // namespace KEP