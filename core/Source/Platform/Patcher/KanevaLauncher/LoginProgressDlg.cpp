///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "LoginProgressDlg.h"

#include <direct.h>
#include <objbase.h>

#include "..\KanevaLauncherLib\KanevaLauncherRes.h"
#include "UnicodeMFCHelpers.h"

#ifndef FONT_NAME
#define FONT_NAME "Arial"
#endif

#ifndef BACKGROUND_TEXTCOLOR
#define BACKGROUND_TEXTCOLOR RGB(165, 204, 233)
#endif

#define TEXT_COLOR RGB(0x0, 0x0, 0x0)
#define BACKGROUND_COLOR RGB(0xff, 0xff, 0xff)

#if _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace KEP {

// CLoginProgressDlg dialog
IMPLEMENT_DYNAMIC(CLoginProgressDlg, baseclass)

CLoginProgressDlg::CLoginProgressDlg(CWnd* pParent /*=NULL*/) :
		baseclass(CLoginProgressDlg::IDD, pParent) {
}

CLoginProgressDlg::~CLoginProgressDlg() {
}

void CLoginProgressDlg::DoDataExchange(CDataExchange* pDX) {
	baseclass::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_CONNECT, m_lblConnect);
	DDX_Control(pDX, IDC_STATIC_DOTS, m_lblDots);
}

BEGIN_MESSAGE_MAP(CLoginProgressDlg, CKanevaDialogSkin)
ON_WM_DESTROY()
ON_WM_CTLCOLOR()
ON_WM_TIMER()
END_MESSAGE_MAP()

//#define PBS_MARQUEE 0x08
//#define PBM_SETMARQUEE (WM_USER + 10)

BOOL CLoginProgressDlg::OnInitDialog() {
	baseclass::OnInitDialog();
	ResourceContext context(CKanevaDialogSkin::GetResourceInstance());
	m_bkBrush.CreateSolidBrush(BACKGROUND_COLOR);

	m_lblConnect.SetBackgroundTransparentColor(BACKGROUND_TEXTCOLOR, true);
	m_lblDots.SetBackgroundTransparentColor(BACKGROUND_TEXTCOLOR, true);

	EnableEasyMove(false);
	return FALSE;
}

void CLoginProgressDlg::OnDestroy() {
	baseclass::OnDestroy();
	m_bkBrush.DeleteObject();
}

//Handles Clearing the Background color of the KanevaLauncherDialog.
HBRUSH CLoginProgressDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = baseclass::OnCtlColor(pDC, pWnd, nCtlColor);

	switch (nCtlColor) {
		case CTLCOLOR_BTN:
		case CTLCOLOR_STATIC:
			return (HBRUSH)GetStockObject(NULL_BRUSH);
		case CTLCOLOR_EDIT:
			pDC->SetTextColor(TEXT_COLOR);
			pDC->SetBkColor(BACKGROUND_COLOR);
			return (HBRUSH)(m_bkBrush.GetSafeHandle());
		case CTLCOLOR_MSGBOX:
			pDC->SetTextColor(BACKGROUND_TEXTCOLOR);
			pDC->SetBkColor(BACKGROUND_COLOR);
			return (HBRUSH)(m_bkBrush.GetSafeHandle());
		case CTLCOLOR_DLG:
			if (IsTransparent()) {
				pDC->SetTextColor(BACKGROUND_TEXTCOLOR);
				return static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));
			} else {
				pDC->SetTextColor(BACKGROUND_TEXTCOLOR);
				return static_cast<HBRUSH>(m_bkBrush.GetSafeHandle());
			}
	}
	return hbr;
}

BOOL CLoginProgressDlg::SetTransparent(BYTE bAlpha) {
	CKanevaDialogSkin::SetTransparent(bAlpha);
	m_lblConnect.SetBackgroundAlpha(bAlpha);
	m_lblDots.SetBackgroundAlpha(bAlpha);
	return FALSE;
}

BOOL CLoginProgressDlg::SetTransparentColor(COLORREF col, BOOL bTrans) {
	CKanevaDialogSkin::SetTransparentColor(col, bTrans);
	m_lblConnect.SetBackgroundTransparentColor(col, bTrans == TRUE);
	m_lblDots.SetBackgroundTransparentColor(col, bTrans == TRUE);
	return FALSE;
}

void CLoginProgressDlg::StartProgress(UINT idNewMsg) {
	ShowWindow(SW_SHOW);

	m_dotsStr.Empty();
	SetTimer(0, 500, NULL);
	if (idNewMsg == 0) {
		idNewMsg = IDS_DEFAULT_PROGRESS_MSG;
	}
	CStringW s;
	if (s.LoadString(idNewMsg))
		m_lblConnect.SetWindowText(s);
	m_lblConnect.Invalidate();

	// since we use this > 1 time and we have transparency need to have parent repaint under the text control
	RECT myRect;
	RECT parentRect;
	m_lblConnect.GetWindowRect(&myRect);
	GetParent()->GetWindowRect(&parentRect);
	myRect.top -= parentRect.top;
	myRect.bottom -= parentRect.top;
	myRect.right -= parentRect.left;
	myRect.left -= parentRect.left;
	GetParent()->InvalidateRect(&myRect);

	OnTimer(0);
}

void CLoginProgressDlg::StopProgress() {
	KillTimer(0);
	m_dotsStr.Empty();
	ShowWindow(SW_HIDE);
}

void CLoginProgressDlg::OnTimer(UINT_PTR nIDEvent) {
	if (m_dotsStr.GetLength() == 0 || m_dotsStr.GetLength() > 30) {
		m_dotsStr = " ";
	} else {
		m_dotsStr = m_dotsStr + ". ";
	}

	SetWindowTextUtf8(m_lblDots, m_dotsStr);
	m_lblDots.Invalidate();
}

} // namespace KEP