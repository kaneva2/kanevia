///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include "afxwin.h"

#include "KanevaBrowser.h"
#include "UserAndPassDlg.h"
#include "LoginProgressDlg.h"
#include "KanevaDialogSkin.h"
#include "ckanevabutton.h"
#include "imagelib\imagestatic.h"

#include <string>
#include <vector>
#include <map>

#define STATUS_ACTIVE 1
#define STATUS_ERROR 2

namespace KEP {

class CGameServer;
class CProgressDlg;

struct TimerInfo {
	unsigned long downloadsize;
	unsigned long estimatedtime;
};

#define HACK_PATCH_URL NET_PATCH_VERSION_URL

class CKanevaLauncherDlg : public CKanevaDialogSkin {
public:
	CKanevaLauncherDlg(PatchClientController* patcherController, CWnd* pParent = NULL); // standard constructor
	virtual ~CKanevaLauncherDlg();
	enum { IDD = IDD_KANEVALAUNCHER_DIALOG };

protected:
	inline CCommandLineInfo* GetCmdInfo() { return AfxGetApp()->m_pCmdInfo; }
	HICON m_hIcon;
	CBrush m_bkBrush;

	bool ValidateServerPatchUrls(CGameServer* pGS, TimeMs timeoutMs);

	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnCancel();
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnGetDefID(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnStartPatch(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnCloseApp(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnAppStatus(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnAllowClosePatcher(WPARAM wp, LPARAM lp);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()

private:
	void CreateShortCutLnk(CStringA m_strItem, CStringA basepath, CStringA m_strFile, CStringA url, int nFolder);

	LONG m_childGameId;
	CKanevaBrowser m_browserleft;
	CKanevaBrowser m_browserright;

	CLoginProgressDlg* m_loginProgDlg;

	/*
	// DRF - Post-Kaneva Hacks (command line -hackServers, -hackPatch, -hacks)
	bool m_hackPatch = false; // use hack patch
	bool m_hackServers = false; // use hack serverlist
	*/

	CStringA m_gameName;
	CStringA m_baseDir;
	std::string m_patchUrlOverride;
	PatchClientController* m_patcherController;
	CProgressDlg* m_progressDlg;

	bool m_parentPatchesCached;
	std::vector<PatchInfo> m_parentPatches;

	bool m_childPatchesCached;
	std::vector<PatchInfo> m_childPatches;

	CStringA m_gameExe; // Client Exe Name

public:
	CImageButton m_kanevacom;
	CImageButton m_techsupport;
	CImageButton m_forum;
	CImageButton m_exit;
	CImageButton m_buttonhelp;
	unsigned long m_totaldownload;
	unsigned long m_estimate;
	bool m_complete;

public:
	afx_msg void OnBnClickedMyprofile();
	afx_msg void OnBnClickedKanevawebsite();
	afx_msg void OnBnClickedForum();
	afx_msg void OnBnClickedKanevawebsite2();
	afx_msg void OnBnClickedSupport();
	void AlignContentsToWindow();

	std::string getAppCommandLineParameter(std::string key);
	bool existsAppCommandLineParameter(std::string key);

private:
	CImageStatic m_version;
	CImageStatic m_browserheader1;
	CImageStatic m_browserheader2;
};

DWORD DoLauncherDlg(CWnd*& mainWnd, PatchClientController* patcherController);

// DRF - Added
struct PatchMetrics {
	size_t dlNumOk;
	size_t dlNumErr;
	size_t dlMs;
	size_t dlBytes;
};
extern PatchMetrics g_kl_pm;

// DRF - Added - Returns patcher metrics as a parameter string for RuntimeReporter
std::string PatchMetricsStr();

} // namespace KEP