///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDR_KANEVALAUNCHER              115
#define IDR_ATLKANEVALAUNCHER           116
#define IDD_ABOUTBOX                    200
#define IDR_MAINFRAME                   228
#define IDI_ICON1                       236

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        246
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1038
#define _APS_NEXT_SYMED_VALUE           117
#endif
#endif
