///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "PatchLibGlobals.h"
#include "KEPHelpers.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

UINT gMsgId = ::RegisterWindowMessageW(L"KEI_PATCH_STATUS_MSG");
UINT gMsgIdTrayPatcher = ::RegisterWindowMessageW(TRAY_PATCHER_MSG_NAME);

PatchProcess::PatchProcess() {
	memset(this, 0, sizeof(*this));
}

PatchProcess::~PatchProcess() {
}

PatchProcess::PatchProcess(const PatchProcess& src) {
	memset(this, 0, sizeof(*this));
	operator=(src);
}

PatchProcess& PatchProcess::operator=(const PatchProcess& src) {
	if (&src != this) {
		strcpy_s(Username, _countof(Username), src.Username);
		strcpy_s(Password, _countof(Password), src.Password);
		localFilePath = src.localFilePath;
		strcpy_s(ClientString, _countof(ClientString), src.ClientString);
		strcpy_s(ExeTitle, _countof(ExeTitle), src.ExeTitle);
		strcpy_s(ExeProgram, _countof(ExeProgram), src.ExeProgram);
		strcpy_s(PingHost, _countof(PingHost), src.PingHost);
		strcpy_s(requiredPrefix, _countof(requiredPrefix), src.requiredPrefix);
		strcpy_s(requiredSuffix, _countof(requiredSuffix), src.requiredSuffix);
		strcpy_s(newGameServer, _countof(newGameServer), src.newGameServer);

		// copy simple types, could be issues with handle copies
		hWnd = src.hWnd;
		gameId = src.gameId;
		zoneindex = src.zoneindex;
		hInternet = src.hInternet;
		hSession = src.hSession;
		hEv = src.hEv;
		hEvClosing = src.hEvClosing;
		newGamePort = src.newGamePort;
		childGameId = src.childGameId;
		patchInfo = src.patchInfo;

		m_urls = src.m_urls;

		// DRF - Added
		m_filesToProcess = src.m_filesToProcess;
		m_filesProcessed = src.m_filesProcessed;
		m_filesErrored = src.m_filesErrored;
		m_currentPatchStatus = src.m_currentPatchStatus;
		m_completed = src.m_completed;
	}
	return *this;
}

void PatchProcess::ClearUrls() {
	m_urls.clear();
}

void PatchProcess::AddUrl(const std::string& url) {
	m_urls.push_back(url);
}

std::string PatchProcess::GetUrl(size_t patch) {
	return (patch >= m_urls.size()) ? "" : m_urls[patch];
}

std::string PatchProcess::GetUrlFile(size_t patch, const std::string& file) {
	return PathAdd(GetUrl(patch), file);
}

void PatchProcess::HTTPSetStatus(LONG cnTotalSize, LONG gGameId, const std::string& msg,
	long nTotalSizeComplete, long nSubSizeComplete,	int nErrorCode) 
{
	MutexGuardLock lock(m_statusMutex);
	if (nErrorCode != 0)
		_snprintf_s(m_currentPatchStatus.wcsStatus, _countof(m_currentPatchStatus.wcsStatus), sizeof(m_currentPatchStatus.wcsStatus), "%s (%d)", msg.c_str(), nErrorCode);
	else
		_snprintf_s(m_currentPatchStatus.wcsStatus, _countof(m_currentPatchStatus.wcsStatus), sizeof(m_currentPatchStatus.wcsStatus), "%s", msg.c_str());

	m_currentPatchStatus.nTotalSizeComplete = nTotalSizeComplete;
	m_currentPatchStatus.nSubSizeComplete = nSubSizeComplete;
	m_currentPatchStatus.nTotalSize = cnTotalSize;
	m_currentPatchStatus.gameId = gGameId;
}

void PatchProcess::HTTPGetStatus(STATUSU &status)
{
	MutexGuardLock lock(m_statusMutex);

	strncpy_s(status.wcsStatus, _countof(status.wcsStatus), m_currentPatchStatus.wcsStatus, _TRUNCATE);
	status.nTotalSizeComplete = m_currentPatchStatus.nTotalSizeComplete;
	status.nSubSizeComplete = m_currentPatchStatus.nSubSizeComplete;
	status.nTotalSize = m_currentPatchStatus.nTotalSize;
	status.gameId = m_currentPatchStatus.gameId;
}

void PatchProcess::FilesReset() {
	MutexGuardLock lock(m_filesMutex);
	m_filesErrored = 0;
	m_filesProcessed = 0;
	m_filesToProcess = 0;
	std::string msg = "Connecting ...";
	HTTPSetStatus(m_filesToProcess, gameId, msg, m_filesProcessed, m_filesProcessed, 0);
}

void PatchProcess::FileProcessed() {
	MutexGuardLock lock(m_filesMutex);
	++m_filesProcessed;
	LogInfo(m_filesProcessed << " of " << m_filesToProcess);
	std::string msg;
	StrBuild(msg, "File Processed (" << m_filesProcessed << " of " << m_filesToProcess << ")");
	HTTPSetStatus(m_filesToProcess, gameId, msg, m_filesProcessed, m_filesProcessed, 0);
}

void PatchProcess::FileErrored() {
	MutexGuardLock lock(m_filesMutex);
	++m_filesErrored;
	LogError(m_filesErrored << " of " << m_filesToProcess);
	std::string msg;
	StrBuild(msg, "File Errored (" << m_filesErrored << " of " << m_filesToProcess << ")");
	HTTPSetStatus(m_filesToProcess, gameId, msg, m_filesProcessed, m_filesProcessed, 0);
}

} // namespace KEP

using namespace KEP;

namespace PatchLibUtils {

bool FileExists(const std::string& filePath) {
	WIN32_FIND_DATA findd;
	HANDLE findHandle;
	findHandle = ::FindFirstFileW(KEP::Utf8ToUtf16(filePath).c_str(), &findd);
	if (findHandle != INVALID_HANDLE_VALUE) {
		::FindClose(findHandle);
		return true;
	}
	return false;
}

bool MakeDir(const std::string& filePath) {
	auto rv = ::SHCreateDirectoryExW(NULL, KEP::Utf8ToUtf16(filePath).c_str(), NULL);
	return (rv == ERROR_SUCCESS) || (rv == ERROR_ALREADY_EXISTS) || (rv == ERROR_FILE_EXISTS);
}

void DoEvents() {
	MSG m;
	while (::PeekMessage(&m, NULL, 0, 0, PM_REMOVE)) {
		::TranslateMessage(&m);
		::DispatchMessage(&m);
	}
}

bool MoveFileWithoutSecurityDescriptor(const std::string& existingFileName, const std::string& newFileName) {
	std::wstring strExistingFileNameW = KEP::Utf8ToUtf16(existingFileName);
	if (::CopyFileW(strExistingFileNameW.c_str(), KEP::Utf8ToUtf16(newFileName).c_str(), FALSE)) {
		::DeleteFile(strExistingFileNameW.c_str());
		return true;
	}
	return false;
}

} // namespace PatchLibUtils
