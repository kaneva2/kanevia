///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

namespace KEP {

// wrapper over getting registry key and network config details
class ConfigValues {
public:
	ConfigValues();

	// if 0 only grabs base info and
	// tries for base network.cfg override
	BOOL LoadValues(LONG gameId = 0);

	BOOL LoadValues(const std::string& netCfgPath);

	CStringA m_gamePath; // path to client star folder
	CStringA m_launcherPath;
	CStringA m_authUrl;
	CStringA m_loginUrl;
	CStringA m_gameInfoUrl;
	CStringA m_allowAccessUrl;
	CStringA m_startAppUrl;

	// helpers to centralize code that was copied around
	static bool FillInGameLoginUrl(std::string& gameLoginUrl, const char* userid, const char* gotoUrl);
	static bool FillInValidateUserUrl(std::string& validateUserUrl, const char* userid, const char* password, LONG gameId);
	static bool FillInStartAppUrl(std::string& startAppUrl, const char* gameUrl);

	CStringA& ServerListUrl(LONG gameId);

private:
	CStringA m_prodServerListUrl;
	CStringA m_serverListUrl;
};

} // namespace KEP