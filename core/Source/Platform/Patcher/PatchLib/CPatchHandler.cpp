///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CPatchHandler.h"

#include "GameServer.h"
#include "CommUtil.h"
#include "ConfigValues.h"

#include "StpUrl.h"
#include "PatchReturns.h"
#include "KEPHelpers.h"
#include "NetworkCfg.h"

#include "../../Client/WokwebHelper.h"
#include "LogHelper.h"
#include "WebCall.h"

#define PH_MONITOR_WINDOW_CLASS L"PatchHandlerMonitorClass"
#define PH_MONITOR_WINDOW_TITLE L"PatchHandlerMonitorTitle"

namespace KEP {

static LogInstance("Instance");

bool DoPatchExe(PatchProcess* pPP);

CPatchHandler::CPatchHandler(HWND hwnd, const std::string& baseDir, const std::string& email, const std::string& url, long gameId, long parentGameId, long& ret) :
		m_hwndMsgMonitor(NULL), m_ret(ret) {
	clear();
	m_hwnd = hwnd;
	m_baseDir = baseDir;
	m_email = email;
	m_url = url;
	m_gameId = gameId;
	m_parentGameId = parentGameId;

	std::string resultDescription;
	if (createMonitorWindow(resultDescription) == false) {
		sendPatchError(E_INVALIDARG, resultDescription);
	}
}

CPatchHandler::~CPatchHandler() {
	clear();
}

bool CPatchHandler::cancel() {
	return false;
}

bool CPatchHandler::start() {
	// reentry guard
	if (m_pendingPatches.empty() == false) {
		sendPatchError(E_INVALIDARG, "Patching already in progress");
		return false;
	}

	// first resolve number of patches we have to process
	GL_RESULT glResult = GL_UNDEFINED;
	std::string resultDescription;
	if (getPatchesInfo(glResult, resultDescription) == false) {
		sendPatchError((HRESULT)(int)glResult, resultDescription);
		return false;
	} else {
		if (m_ret == UP_SAME_GAME || m_ret == UP_PARENT_GAME) {
			SendMessage(m_hwnd, KEI_MSG_ID, MSG_COMPLETE, m_ret);
			return true;
		}
	}

	// check that we found some patches
	if (m_pendingPatches.empty()) {
		sendPatchError(E_INVALIDARG, "No patch urls for game");
		return false;
	}

	return processNextPatch();
}

void CPatchHandler::clear() {
	m_hwnd = 0;
	m_baseDir.clear();
	m_email.clear();
	m_url.clear();
	m_gameId = DEFAULT_GAME_ID;
	m_parentGameId = 0;
	m_completeMsg.clear();

	m_serverIp.clear();
	m_serverPort = 0;
	m_childGameId = 0;
	m_gameName.clear();

	if (m_hwndMsgMonitor != NULL) {
		DestroyWindow(m_hwndMsgMonitor);
		m_hwndMsgMonitor = NULL;
		UnregisterClassW(PH_MONITOR_WINDOW_CLASS, GetModuleHandle(NULL));
	}
}

void CPatchHandler::patchCompleted(COMPLETE_MSG* pMsg) {
	if (pMsg) {
		// aggreate data for patches we are handling
		m_completeMsg.TotalPatches += 1;
		m_completeMsg.TotalFiles += pMsg->totalFiles;

		// could dop this elsewhere to avoid repating
		// since this data is the same accross all patches we handled
		m_completeMsg.GameID = pMsg->gameId;
		m_completeMsg.NewGamePort = pMsg->newGamePort;
		m_completeMsg.NewGameServer = pMsg->newGameServer;
		m_completeMsg.ChildGameId = pMsg->childGameId;
	}
}

bool CPatchHandler::processNextPatch() {
	// check if we are done
	if (m_pendingPatches.empty()) {
		sendPatchCompleted();
		return true;
	}

	PatchInfo nextPatch = m_pendingPatches.top();
	m_pendingPatches.pop();

	// build parms for HTTPWorker patch thread
	PatchProcess patchProcess;

	patchProcess.hWnd = m_hwndMsgMonitor;
	strcpy_s(patchProcess.ClientString, _countof(patchProcess.ClientString), "unused");
	strcpy_s(patchProcess.newGameServer, _countof(patchProcess.newGameServer), m_serverIp.c_str());
	patchProcess.newGamePort = m_serverPort;
	patchProcess.gameId = m_parentGameId;
	patchProcess.childGameId = m_childGameId;
	patchProcess.zoneindex = 0;

	patchProcess.patchInfo = nextPatch;

	patchProcess.ClearUrls();

	std::string localFilePath;
	if (!getGamePath(m_parentGameId, localFilePath)) {
		std::string errMsg = "Error reading install directory for game:" + std::to_string(static_cast<long long>(m_parentGameId));
		sendPatchError(E_INVALIDARG, errMsg);
		return false;
	}

	patchProcess.localFilePath = nextPatch.patchPath.empty() ? localFilePath : PathAdd(localFilePath, nextPatch.patchPath);
	patchProcess.AddUrl(nextPatch.patchUrl);

	if (!DoPatchExe(&patchProcess)) {
		sendPatchError(E_INVALIDARG, "Failed to start patch");
		return false;
	}

	return true;
}

bool CPatchHandler::getPatchesInfo(GL_RESULT& glResult, std::string& resultDescription) {
	m_ret = UP_DIFFERENT_GAME;
	DWORD httpStatusCode;
	std::string httpStatusDescription;
	std::string resultText;

	StpUrl::CheckPrefix(m_url);

	std::string netCfgPath = PathAdd(m_baseDir, "Network.cfg");
	LogInfo("netCfgPath='" << netCfgPath << "'");

	ConfigValues cfg;
	cfg.LoadValues(netCfgPath);

	std::string gameLoginUrl = cfg.m_loginUrl;
	ConfigValues::FillInGameLoginUrl(gameLoginUrl, m_email.c_str(), m_url.c_str());

	long _gameId = m_gameId;
	std::string _serverip = "";
	long _serverport = 0;
	long _parentGameId = 0;
	long _gameIdFromLogin = 0;
	long _subGameId = 0;
	long _newGameId = 0;
	std::string _gameName = "";

	// call game login to validate game url
	size_t resultSize = 0;
	if (GetBrowserPage(gameLoginUrl, resultText, resultSize, httpStatusCode, httpStatusDescription)) {
		long _userId = 0;
		std::string _username = "";
		long _zoneindex = 0;
		std::string _gender = "";
		std::string _serverVersion;
		bool _incubatorHosted = false;
		long _errorCode = 0;
		std::string _templatePatchDir;
		std::string _appPatchDir;

		if (ParseGameLoginResult(
				resultText,
				glResult,
				resultDescription,
				_userId,
				_username,
				_serverip,
				_serverport,
				_serverVersion, // drf - added
				_zoneindex,
				_gender,
				_gameIdFromLogin,
				_gameName,
				_incubatorHosted,
				_parentGameId,
				_errorCode,
				_templatePatchDir,
				_appPatchDir)) {
			if (glResult == GL_OK) {
				m_gameId = _gameIdFromLogin;
				m_serverIp = _serverip;
				m_serverPort = _serverport;

				if (_parentGameId != 0) {
					_subGameId = m_gameId;
					_newGameId = _subGameId;
					m_childGameId = _subGameId;
					m_gameId = _parentGameId;
				} else {
					_newGameId = m_gameId;
				}
			} else {
				return false;
			}
		}
	} else {
		glResult = GL_HTTP_ERROR;
		resultDescription = "Failed to open URL: " + gameLoginUrl;
		return false;
	}

	if (_gameId != 0) {
		if (_gameId == _newGameId) {
			m_ret = UP_SAME_GAME; // don't patch
			return true;
		} else if (_newGameId == m_parentGameId) {
			m_ret = UP_PARENT_GAME; // don't patch parent since already patched
			return true;
		} else if (m_parentGameId == _parentGameId) {
			m_ret = UP_SIBLING_GAME;
			// fall thru and patch
		}
		// else new game
	}

	m_parentGameId = _parentGameId;

	std::string serverListUrl = cfg.ServerListUrl(_gameIdFromLogin);
	std::string::size_type p = serverListUrl.find("{0}");
	if (p != std::string::npos) {
		char gameIdStr[30];
		_ltoa_s(_gameIdFromLogin, gameIdStr, _countof(gameIdStr), 10);
		serverListUrl = serverListUrl.replace(p, 3, gameIdStr);
	}

	CCommUtil comm;
	CObList gameServerList;
	comm.getGameServers(serverListUrl.c_str(), &gameServerList);

	for (POSITION pos = gameServerList.GetHeadPosition(); pos;) {
		auto pGS = (CGameServer*)gameServerList.GetNext(pos);
		for (const auto& url : pGS->GetPatchUrls()) {
			PatchInfo newEntry;
			newEntry.patchUrl = url;
			newEntry.patchType = pGS->GetPatchType();
			newEntry.patchPath = pGS->GetPatchPath();
			m_pendingPatches.push(newEntry);
		}
	}

	return true;
}

void CPatchHandler::forwardMsg(WPARAM wp, LPARAM lp) {
	SendMessage(m_hwnd, KEI_MSG_ID, wp, lp);
}

void CPatchHandler::sendPatchCompleted() {
	SendMessage(m_hwnd, KEI_MSG_ID, MSG_PATCH_HANDLER_COMPLETE, (LONG)&m_completeMsg);
}

void CPatchHandler::sendPatchError(HRESULT rc, std::string msg) {
	ERROR_MSG_VERBOSE err(m_gameId, rc, msg.c_str());
	SendMessage(m_hwnd, KEI_MSG_ID, MSG_VERBOSE_ERROR, (LONG)&err);
}

bool CPatchHandler::getGamePath(long gameId, std::string& gamePath) {
	ConfigValues cfg;
	cfg.LoadValues(gameId);
	gamePath = cfg.m_gamePath;
	return true;
}

bool CPatchHandler::createMonitorWindow(std::string& resultDescription) {
	if (m_hwndMsgMonitor == NULL) {
		WNDCLASSW wc;
		memset(&wc, 0, sizeof(wc));
		wc.lpfnWndProc = MessageMonitorWindProc;
		wc.hInstance = GetModuleHandle(NULL);
		wc.lpszClassName = PH_MONITOR_WINDOW_CLASS;

		RegisterClassW(&wc);
		KEI_MSG_ID = RegisterWindowMessageW(KEI_PATCH_STATUS_MSG_ID);

		m_hwndMsgMonitor = CreateWindowW(PH_MONITOR_WINDOW_CLASS, PH_MONITOR_WINDOW_TITLE, 0, 0, 0, 0, 0, NULL, NULL, NULL, this);

		if (m_hwndMsgMonitor == NULL) {
			std::ostringstream oss;
			oss << "Can't create message window:" << GetLastError();
			resultDescription = oss.str();
			return false;
		}
	}

	return true;
}

// **********************************************************************
// Handle ALL messages comming back from thread
// **********************************************************************
static CPatchHandler* pHandler = NULL;
LRESULT CALLBACK CPatchHandler::MessageMonitorWindProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp) {
	LPCREATESTRUCT lpcs;
	COMPLETE_MSG* pHttpWorkerCompleteMsg = NULL;

	switch (msg) {
		case WM_CREATE:
			lpcs = (LPCREATESTRUCT)lp;
			if (lpcs) {
				pHandler = (CPatchHandler*)lpcs->lpCreateParams;
			}
			break;

		case WM_DESTROY:
			pHandler = NULL;
			break;

		default:
			if (pHandler) {
				if (msg == pHandler->KEI_MSG_ID) {
					// All messages from HTTPWorker
					switch (wp) {
						case MSG_COMPLETENM:
							pHttpWorkerCompleteMsg = (COMPLETE_MSG*)lp;
							pHandler->patchCompleted(pHttpWorkerCompleteMsg);
							pHandler->processNextPatch();
							break;
						case MSG_INIT:
						case MSG_STATUS:
						case MSG_RECONNECT:
						case MSG_GAREPORT:
						case MSG_ERROR:
						case MSG_VERBOSE_ERROR:
						case MSG_OTHERERROR:
						case MSG_UNPACKINGGZ:
						case MSG_UNPACKING:
						case MSG_ABORT:
						default:
							pHandler->forwardMsg(wp, lp);
							break;
					}
				}
			}
	}

	return DefWindowProc(hWnd, msg, wp, lp);
}

} // namespace KEP