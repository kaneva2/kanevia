///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "PatchLibGlobals.h"
#include "HTTPWorker.h"
#include "UnicodeWindowsHelpers.h"
#include "KEPHelpers.h"

#define THREAD_KILL_WAIT 5000

namespace KEP {

static LogInstance("Instance");

HANDLE m_hEv;
HANDLE m_hEvClosing;
static CWinThread* s_threadPatch = NULL;

void CancelPatch() {
	SetEvent(m_hEvClosing);
	::WaitForSingleObject(m_hEv, THREAD_KILL_WAIT);
}

bool DoPatchExe(PatchProcess* pPP) {
	// Create Local Dir (deep)
	std::string path = pPP->localFilePath;
	if (!FileHelper::CreateFolderDeep(path)) {
		LogFatal("FileHelper::CreateFolderDeep() FAILED - '" << path << "'");
		return false;
	}

	// cancel if already running
	SetEvent(pPP->hEvClosing);
	fTime::SleepMs(100);

	if (s_threadPatch)
		s_threadPatch = NULL; // assume it was autodeleted

	ResetEvent(pPP->hEvClosing);

	m_hEv = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hEvClosing = CreateEvent(NULL, TRUE, FALSE, NULL);
	pPP->hEv = m_hEv;
	pPP->hEvClosing = m_hEvClosing;
	VERIFY(m_hEv != NULL && m_hEvClosing != NULL);

	// Start New Patch Process
	s_threadPatch = AfxBeginThread((AFX_THREADPROC)PatchThread, pPP, THREAD_PRIORITY_NORMAL);
	if (!s_threadPatch) {
		std::string msg = "AfxBeginThread(PatchThread) FAILED";
		::MessageBoxA(::GetActiveWindow(), msg.c_str(), msg.c_str(), MB_OK | MB_ICONEXCLAMATION);
		LogFatal(msg);
		PostQuitMessage(0);
	}

	return true;
}

} // namespace KEP