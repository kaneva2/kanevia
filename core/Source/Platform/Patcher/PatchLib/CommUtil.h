///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <afxinet.h>
#include <string>

#include "common\include\KEPCommon.h"

namespace KEP {

class CCommUtil {
public:
	CCommUtil() {}
	~CCommUtil() {}
	void getGameServers(const std::string& url, CObList* gameServerList, bool retryOnEmptyResult = false);
};

} // namespace KEP