///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Mutex.h"
#include "Core/Util/HighPrecisionTime.h"

#include "..\Common\PatcherConstants.h"

#include <vector>
#include <string>

#include <WinInet.h>

#define UPDATE_FAIL 0x10000
#define SERVER_ERROR 0x20000

#define MSG_STARTTHREAD WM_USER + 1
#define MSG_STATUS WM_USER + 2
#define MSG_COMPLETE WM_USER + 3
#define MSG_ABORT WM_USER + 4
#define MSG_ERROR WM_USER + 5
#define MSG_COMPLETENM_OLD WM_USER + 6
#define MSG_INIT WM_USER + 7
#define MSG_RECONNECT WM_USER + 8
#define MSG_VERBOSE_ERROR WM_USER + 9
#define MSG_CONNECTED WM_USER + 10
#define MSG_UNPACKING WM_USER + 11
#define MSG_UNPACKINGGZ WM_USER + 12
#define MSG_COMPLETENM WM_USER + 13
#define MSG_ALLOWED WM_USER + 14
#define MSG_OTHERERROR WM_USER + 15
#define MSG_GAREPORT WM_USER + 16
#define MSG_GOTOAPP WM_USER + 17

#define WM_STARTPATCH WM_USER + 100
#define WM_CLOSEPATCH WM_USER + 101
#define WM_APPSTATUS WM_USER + 102
#define WM_ALLOWCLOSEPATCHER WM_USER + 103

#define ERR_UNKNOWN 0
#define ERR_NOTCONNECTED ERR_UNKNOWN + 1
#define ERR_TIMEOUT ERR_UNKNOWN + 2
#define ERR_NOVERSION ERR_UNKNOWN + 3

// return codes from patcher on verbose msgs
#define RC_PROMPT_ALLOW 3

// DRF - Added
#define DEFAULT_GAME_ID 3296
#define DEFAULT_GAME_ID_URL "kaneva://3296"
#define KEI_PATCH_STATUS_MSG_ID L"KEI_PATCH_STATUS_MSG"

namespace KEP {

// DRF - Added
struct ServerStatus {
	bool m_valid;
	TimeMs m_timeMs;
	ServerStatus() :
			m_valid(false), m_timeMs(0.0) {}
	ServerStatus(bool valid, TimeMs timeMs) :
			m_valid(valid), m_timeMs(timeMs) {}
};

struct PatchInfo {
	std::string patchUrl;
	std::string patchType;
	std::string patchPath;
	ServerStatus serverStatus;
};

struct STATUSU {
	char wcsStatus[255];
	LONG gameId;
	LONG nTotalSizeComplete; // overall total size complete (bytes)
	LONG nSubSizeComplete; // sub item size complete (bytes)
	LONG nTotalSize;
};

class PatchProcess {
public:
	PatchProcess();

	PatchProcess(const PatchProcess& src);

	~PatchProcess();

	PatchProcess& operator=(const PatchProcess& src);

	void ClearUrls();
	void AddUrl(const std::string& url);
	std::string GetUrl(size_t patch);
	std::string GetUrlFile(size_t patch, const std::string& file);
	void HTTPSetStatus(LONG cnTotalSize, LONG gGameId, const std::string& msg,
		long nTotalSizeComplete, long nSubSizeComplete, int nErrorCode);
	void HTTPGetStatus(STATUSU &status);

	HWND hWnd;
	LONG gameId;
	LONG zoneindex;
	char Username[50];
	char Password[50];
	HINTERNET hInternet;
	HINTERNET hSession;
	std::string localFilePath;

	std::vector<std::string> m_urls;

	char ClientString[255];
	char ExeTitle[255];
	char ExeProgram[MAX_PATH];
	char PingHost[20];
	HANDLE hEv;
	HANDLE hEvClosing;
	char requiredPrefix[255]; // to limit files in patch
	char requiredSuffix[255]; // to limit files in patch
	char newGameServer[255]; // parsed out of web call
	int newGamePort;
	LONG childGameId;
	STATUSU m_currentPatchStatus;

	PatchInfo patchInfo;

	void FilesReset();

	void FilesToProcess(size_t files) {
		MutexGuardLock lock(m_filesMutex);
		m_filesToProcess = files;
	}
	size_t FilesToProcess() const {
		MutexGuardLock lock(m_filesMutex);
		return m_filesToProcess;
	}

	void FileProcessed();
	size_t FilesProcessed() const {
		MutexGuardLock lock(m_filesMutex);
		return m_filesProcessed;
	}

	void FileErrored();
	size_t FilesErrored() const {
		MutexGuardLock lock(m_filesMutex);
		return m_filesErrored;
	}

	bool FilesComplete() const {
		MutexGuardLock lock(m_filesMutex);
		return ((FilesErrored() + FilesProcessed()) >= FilesToProcess());
	}

	void OnCompleted()
	{
		m_completed = true;
	}

	bool IsCompleted() const
	{
		return m_completed;
	}

private:
	mutable Mutex m_filesMutex;
	mutable Mutex m_statusMutex;
	size_t m_filesToProcess;
	size_t m_filesProcessed;
	size_t m_filesErrored;
	bool m_completed;
};

extern UINT gMsgId;
extern UINT gMsgIdTrayPatcher;

struct INIT_TOTALS {
	LONG gameId;
	LONG nTotalFiles;
	LONG nTotalSize;
	LONG nTotalSubSize;
	LONG nTotalSizeToDownload; //Added by Jerome
	bool isUpdate;
};

struct ERROR_MSG {
	ERROR_MSG(LONG argGameId, LONG argErrorCode) {
		gameId = argGameId;
		errorCode = argErrorCode;
	}

	LONG gameId;
	LONG errorCode; // only ERR_* from above
};

struct ERROR_MSG_VERBOSE {
	ERROR_MSG_VERBOSE(LONG argGameId, LONG argErrorCode, const char* argMsg = 0) {
		gameId = argGameId;
		errorCode = argErrorCode;
		msg[0] = TEXT('\0');
		if (argMsg) {
			strncpy_s(msg, _countof(msg), argMsg, _TRUNCATE);
		}
	}

	LONG gameId;
	LONG errorCode; // any error num
	char msg[255]; // text version
};

struct GOTOAPP_MSG {
	GOTOAPP_MSG(LONG argGameId, const char* argMsg = 0) {
		gameId = argGameId;
		msg[0] = TEXT('\0');
		if (argMsg) {
			strncpy_s(msg, _countof(msg), argMsg, _TRUNCATE);
		}
	}

	LONG gameId;
	char msg[255];
};

struct COMPLETE_MSG {
#define TOTALFILES_NA_MAGIC 0xE73D74F5 // A random number
	COMPLETE_MSG(LONG gId, const char* server = 0, LONG port = 0, LONG childGId = 0, UINT numTotalFiles = TOTALFILES_NA_MAGIC) :
			gameId(gId), newGamePort(port), childGameId(childGId), totalFiles(numTotalFiles) {
		newGameServer[0] = TEXT('\0');
		if (server)
			strcpy_s(newGameServer, _countof(newGameServer), server);
	}
	LONG gameId;
	LONG childGameId;
	char newGameServer[255];
	LONG newGamePort;
	UINT totalFiles;
};

} // namespace KEP

namespace PatchLibUtils {

bool FileExists(const std::string& filePath);

bool MakeDir(const std::string& filePath);

void DoEvents();

//Alternative move file function without copying of security descriptor from source
bool MoveFileWithoutSecurityDescriptor(const std::string& existingFileName, const std::string& newFileName);
} // namespace PatchLibUtils