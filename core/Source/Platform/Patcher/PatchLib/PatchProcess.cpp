///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "httpworker.h"
#include "PatchLibGlobals.h"
#include "PatchReturns.h"
#include "KEPFileNames.h"
#include "CVersionInfo.h"
#include "UnicodeWindowsHelpers.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "LooseFile.h"
#include "Common\KepUtil\CrashReporter.h"
#include "Common\KepUtil\WebCall.h"
#include "Common\KepUtil\kpack.h"
#include "Common\include\CompressHelper.h"
#include "jsEnumFiles.h"
#include <experimental/filesystem>

using namespace std;
namespace filesystem = std::experimental::filesystem::v1;

// NOTE: Web Caller Shared With DownloadManager!
#define PATCH_WEBCALLER "Downloader"
#define PATCH_THREADS 4
#define PATCH_TIMEOUT_MS (60.0 * 1000.0)

#define NOT_SET -1
#define MIN_VAL -2
#define MAX_VAL -3

namespace KEP {

static LogInstance("Instance");

// Patch Metrics
hw_PatchMetrics g_hw_pm = { 0 };
Mutex g_hw_pm_mutex;
ru_PatchMetrics g_ru_pm = { 0 };
Mutex g_ru_pm_mutex;

// Patch File Threads Busy RAII
static size_t s_busy = 0;
static Mutex s_busy_mutex;
struct BusyRAII {
	BusyRAII() {
		MutexGuardLock lock(s_busy_mutex);
		++s_busy;
		if (s_busy > 16)
			LogWarn("BUSY=" << s_busy);
	}

	~BusyRAII() {
		MutexGuardLock lock(s_busy_mutex);
		--s_busy;
	}

	static size_t Busy() {
		MutexGuardLock lock(s_busy_mutex);
		return s_busy;
	}
};

// Patch Temporary Path
static inline string PathTemp() {
	return PathAdd(PathApp(), "temp");
}

static string pathHelper(const string& installDir, const string& subDir, const string& VIFile) {
	ostringstream buf;
	buf << installDir << "\\" << subDir << "\\" << VIFile;
	return StringHelpers::findandreplace(buf.str(), "\\\\", "\\");
}

static bool CreateFolderStructure(PatchProcess* pPP, CVersionInfo& VIFile) {
	if (!pPP)
		return false;

	// Precreate all directories found in VIFile
	vector<string> directories;
	for (const auto& it : VIFile.getFileEntries())
		directories.push_back(pathHelper(pPP->localFilePath, "", it.second.get_Directory()));

	// Filter out duplicates
	directories.erase(unique(directories.begin(), directories.end()), directories.end());

	// Create Folder Tree (deep)
	for (const auto& dirPath : directories) {
		if (!FileHelper::CreateFolderDeep(dirPath)) {
			LogError("FileHelper::CreateFolderDeep() FAILED - '" << dirPath << "'");
			return false;
		}
	}

	return true;
}

static void ProcessZoneManifest(CVersionInfo& VIFile, const string& zoneManifestFile, int czoneindex) {
	// Open File For Read
	FILE* file = 0;
	wstring zoneManifestFileW = Utf8ToUtf16(zoneManifestFile);
	if (_wfopen_s(&file, zoneManifestFileW.c_str(), L"rt") != 0)
		return;

	// Parse File
	int zoneindex = 0;
	char zone;
	char zonename[0xff];
	char zonefilename[0xff];
	char buffer[MAX_PATH];
	while (ftell(file) < _filelength(_fileno(file))) {
		fgets(buffer, 0xff, file);
		CStringA csLine = buffer;
		csLine.Replace(",,", ", ,");
		strcpy_s(buffer, _countof(buffer), csLine);
		sscanf_s(buffer, "%c", &zone, sizeof(zone));

		if (zone == 'z') {
			sscanf_s(buffer, "%c %d %s\n", &zone, sizeof(zone), &zoneindex, zonename, _countof(zonename));
		} else if (zone == 'f' && zoneindex == czoneindex) {
			sscanf_s(buffer, "%c %s\n", &zone, sizeof(zone), zonefilename, _countof(zonefilename));

			// mark versionInfo fileEntry as required
			// we have map but still need to iterate because of case insensitive compare
			for (auto& it : VIFile.getFileEntries()) {
				if (CompareCaseInsensitive(it.second.get_Filename(), zonefilename)) {
					it.second.set_Required(true);
					break;
				}
			}
		}
	}

	// Close File
	fclose(file);

	// Delete File
	FileHelper::Delete(zoneManifestFile);
}

static bool UpdateLooseFiles(const string& dir, const char* cfName, FileDirectoryList* pFiles) {
	bool ret = true;
	string cfilename;
	string directory = dir;
	if (cfName == 0) {
		cfilename = PathAdd(directory, "localTextureHash.dat");
		directory = PathAdd(directory, "MapsModels");
		pFiles = new FileDirectoryList;
	}

	jsEnumFiles ef(Utf8ToUtf16(PathAdd(directory, "*.*")).c_str(), true);
	while (ef.next() && ret) {
		string currentPath = Utf16ToUtf8(ef.currentPath());
		if (ef.isDirectory()) {
			ret = UpdateLooseFiles(currentPath, cfilename.c_str(), pFiles);
		} else if (_wcsicmp(ef.currentFileName(), L"textureinfo.dat") == 0) {
			ret = pFiles->CompareDirectories(cfilename, currentPath, cfName == 0); // first time pass in true to load src
		}
	}

	if (cfName == 0)
		delete pFiles;

	return ret;
}

static bool RemoveUnusedFiles(PatchProcess* pPP, CVersionInfo& localVIFiles, CVersionInfo& serverVIFiles) {
	if (!pPP)
		return false;
	FileEntryMap& localFileMap = localVIFiles.getFileEntries();
	FileEntryMap& serverFileMap = serverVIFiles.getFileEntries();
	for (const auto& itr : localFileMap) {
		auto found = serverFileMap.find(itr.first);
		if (found != serverFileMap.end())
			continue;
		string fileName = pathHelper(pPP->localFilePath, "", itr.second.FileNameFull());
		FileHelper::Delete(fileName);
	}
	return true;
}

struct FileContext {
	PatchProcess* pPP;
	CVIFileEntry* fileEntry;
	string filePath;
	FileContext(PatchProcess* pp, CVIFileEntry* fe, const string& fp) :
			pPP(pp), fileEntry(fe), filePath(fp) {}
};

static bool _UncompressFile(const FileContext& fc) {
	// File Is Compressed ?
	if (::CompressType(fc.filePath) == COMPRESS_TYPE_NONE)
		return true;

	// Uncompress File (and cleanup original)
	Timer timer;
	auto size = FileHelper::Size(fc.filePath);
	bool ok = ::UncompressFile(fc.filePath, true);
	TimeMs timeMs = timer.ElapsedMs();

	// Update Patch Metrics
	g_hw_pm_mutex.lock();
	g_hw_pm.zipMs += timeMs;
	g_hw_pm.zipBytes += size;
	if (!ok) {
		LogError("::UncompressFile() FAILED - '" << fc.filePath << "'");
		++g_hw_pm.zipNumErr;
	} else
		++g_hw_pm.zipNumOk;
	g_hw_pm_mutex.unlock();

	return ok;
}

static bool _MoveFile(const FileContext& fc) {
	// Move Uncompressed File To Final Local Path
	string filePath = fc.filePath;
	filePath = ::CompressTypeExtDel(filePath);
	string localFilePath = pathHelper(fc.pPP->localFilePath, "", fc.fileEntry->FileNameFull());
	if (!FileHelper::Move(filePath, localFilePath)) {
		LogError("FileHelper::Move() FAILED - '" << filePath << "' -> '" << localFilePath << "'");
		return false;
	}

	return true;
}

static bool _ValidateFile(const FileContext& fc) {
	// Ignore Kaneva Setup MD5 (jeff is lazy)
	if (STLCompareIgnoreCase(fc.filePath, "KanevaSetup.exe"))
		return true;

	// Validate Local File Against Server VersionInfo MD5
	string localFilePath = pathHelper(fc.pPP->localFilePath, "", fc.fileEntry->FileNameFull());
	string serverMd5 = fc.fileEntry->get_MD5();
	string localMd5 = Md5File(localFilePath);
	if (localMd5 != serverMd5) {
		LogError("MD5 MISMATCH - '" << localFilePath << "'");
		FileHelper::Delete(localFilePath);
		return false;
	}

	return true;
}

static bool _UnpackFile(const FileContext& fc) {
	// File Is Packed ?
	string localFilePath = pathHelper(fc.pPP->localFilePath, "", fc.fileEntry->FileNameFull());
	if (!StrSameFileExt(localFilePath, "pak"))
		return true;

	// Unpack File (and leave original for future MD5 validation)
	Timer timer;
	auto size = FileHelper::Size(localFilePath);
	size_t numFiles = 0;
	string localDir = pathHelper(fc.pPP->localFilePath, "", fc.fileEntry->get_Directory());

	bool ok = false;
	try {
		KPack::UnpackFiles(localFilePath, localDir, numFiles);
		ok = true;
	}
	catch (const exception& exception) {
		LogError("FAILED un-packing - '" << localFilePath << "' - " << exception.what());
		error_code errorCode;
		filesystem::remove(localFilePath, errorCode);
		if (errorCode) {
			LogError("FAILED deleting - '" << localFilePath << "' - " << errorCode);
		}
	}

	TimeMs timeMs = timer.ElapsedMs();
	if (ok) {
		// Log Unpacked files count
		string timeStr;
		if (timeMs) {
			StrBuild(timeStr, " " << FMT_TIME << timeMs << "ms");
		}
		LogInfo("OK (" << numFiles << " files" << timeStr << ") - '" << localFilePath << "'");
	}

	// Update Patch Metrics
	g_hw_pm_mutex.lock();
	g_hw_pm.pakMs += timeMs;
	g_hw_pm.pakBytes += size;
	if (!ok) {		
		++g_hw_pm.pakNumErr;
	} else
		++g_hw_pm.pakNumOk;
	g_hw_pm_mutex.unlock();

	return ok;
}

static DWORD DownloadAsyncFileCallback(void* vThreadParm) {
	BusyRAII busy;

	// Cast WebCallAct*
	auto pWCA = static_cast<WebCallAct*>(vThreadParm);
	if (!pWCA) {
		LogError("static_cast<WebCallAct*>(" << vThreadParm << ") FAILED");
		return 0;
	}

	// Cast FileContext (app added data)
	FileContext* pfc = static_cast<FileContext*>(pWCA->wco.pAddData);
	if (!pfc) {
		LogError("FileContext null");
		return 0;
	}

	// Get Web Call Result
	string url = pWCA->wco.url;
	string filePath = pWCA->wco.responseFilePath;
	auto bytes = pWCA->ResponseSize();
	TimeMs timeMs = pWCA->ResponseTimeMs();

	// Web Call Ok ?
	bool ok = pWCA->IsOk();
	if (!ok) {
		// Update Patch Metrics
		g_ru_pm_mutex.lock();
		++g_ru_pm.dlNumErr;
		g_ru_pm_mutex.unlock();

		DWORD httpStatusCode = pWCA->ErrorCode();
		string httpStatusDesc = pWCA->ErrorStr();
		LogError("DownloadAsync() FAILED - "
				 << "[" << httpStatusCode << "]" << httpStatusDesc
				 << " - '" << url << "' -> '" << filePath << "'");

		// Delete File (possibly corrupt, try again next time)
		FileHelper::Delete(filePath);
	} else {
		// Update Patch Metrics
		g_ru_pm_mutex.lock();
		++g_ru_pm.dlNumOk;
		g_ru_pm.dlBytes += bytes;
		g_ru_pm.dlMs += timeMs;
		g_ru_pm_mutex.unlock();

		string timeStr;
		if (timeMs)
			StrBuild(timeStr, " " << FMT_TIME << timeMs << "ms");
		LogInfo("OK (" << bytes << " bytes" << timeStr << ") '" << url << "' -> '" << filePath << "'");

		// Uncompress File
		ok &= _UncompressFile(*pfc);

		// Move File To Final Local Path
		ok &= _MoveFile(*pfc);

		// Validate File (MD5)
		ok &= _ValidateFile(*pfc);

		// Unpack File
		ok &= _UnpackFile(*pfc);

		// Update Server File Entry Required (incase of retries)
		pfc->fileEntry->set_Required(!ok);
	}

	// Update Progress Bar
	if (ok)
		pfc->pPP->FileProcessed();
	else
		pfc->pPP->FileErrored();

	// Delete Added Data
	delete pfc;

	return 0;
}

static bool DownloadAsyncFile(PatchProcess* pPP, WebCaller* pWC, CVIFileEntry* fileEntry) {
	if (!pPP || !pWC || !fileEntry)
		return false;

	// Valid Url & File ?
	string filePathFrom = fileEntry->get_ServerFile();
	string filePathTo = pathHelper(PathTemp(), "", fileEntry->get_ServerFile());
	if (filePathFrom.empty() || filePathTo.empty())
		return false;

	// Determine Patch Url
	auto url = pPP->GetUrlFile(0, filePathFrom);

	// Configure Web Call Options
	WebCallOpt wco;
	wco.url = url;
	wco.timeoutMs = PATCH_TIMEOUT_MS;
	wco.response = true;
	wco.responseFilePath = filePathTo;
	wco.cbFunc = DownloadAsyncFileCallback;
	wco.pAddData = new FileContext(pPP, fileEntry, filePathTo);

	// Do Web Call (no blocking + response + DownloadAsyncCallback)
	return (pWC->Call(wco) != NULL);
}

static bool DownloadAsyncWait(PatchProcess* pPP, WebCaller* pWC) {
	if (!pPP || !pWC)
		return false;

	// Wait For All Downloads To Complete
	Timer timer;
	FOREVER {
		// Complete ?
		if (pPP->FilesComplete()) {
			LogInfo("COMPLETE (" << FMT_TIME << timer.ElapsedMs() << "ms) -"
								 << " filesToProcess=" << pPP->FilesToProcess()
								 << " filesProcessed=" << pPP->FilesProcessed()
								 << " filesErrored=" << pPP->FilesErrored());
			fTime::SleepMs(100.0); // just incase
			break;
		}

		// Closed ?
		if (::WaitForSingleObject(pPP->hEvClosing, 0) == WAIT_OBJECT_0) {
			LogFatal("CLOSING (" << FMT_TIME << timer.ElapsedMs() << "ms) -"
								 << " filesToProcess=" << pPP->FilesToProcess()
								 << " filesProcessed=" << pPP->FilesProcessed()
								 << " filesErrored=" << pPP->FilesErrored()
								 << " callsActive=" << pWC->CallsActive()
								 << " busyThreads=" << BusyRAII::Busy());
			return false;
		}

		// Check Again in 100ms
		fTime::SleepMs(100.0);
	}

	return true;
}

static bool _DownloadSyncFile(
	PatchProcess* pPP,
	size_t patch,
	const char* csFromFilename,
	const char* csToFilename,
	size_t& fileSize) {
	fileSize = 0;
	if (!pPP)
		return false;

	// Valid Url & File ?
	if (!csFromFilename || !csToFilename)
		return false;
	string filePathFrom = csFromFilename;
	string filePathTo = csToFilename;

	// Determine Patch Url
	auto url = pPP->GetUrlFile(patch, filePathFrom);

	// Download File
	Timer pmTimer;
	GBPO gbpo(url);
	gbpo.resultFile = filePathTo;
	bool ok = GetBrowserPage(gbpo);

	// Update Patch Metrics
	g_ru_pm_mutex.lock();
	if (!ok) {
		++g_ru_pm.dlNumErr;
	} else {
		++g_ru_pm.dlNumOk;
		g_ru_pm.dlBytes += gbpo.resultSize;
		g_ru_pm.dlMs += pmTimer.ElapsedMs();
		fileSize = gbpo.resultSize;
	}
	g_ru_pm_mutex.unlock();

	if (!ok) {
		FileHelper::Delete(filePathTo);
		::SendMessage(pPP->hWnd, gMsgId, MSG_RECONNECT, 0);
		return false;
	}

	return true;
}

static bool DownloadSyncFile(
	PatchProcess* pPP,
	const char* csFromFilename,
	const char* csToFilename,
	HANDLE hCancel,
	size_t& patch) {
	if (!pPP)
		return false;

	size_t numPatches = pPP->m_urls.size();
	if (patch >= numPatches)
		patch = 0;

	// try each url max of 5 times
	const size_t maxRetries = 5;
	for (size_t retry = 0; retry < maxRetries; retry++) {
		size_t patchCur = patch;
		for (size_t j = 0; j < numPatches; j++) {
			Timer timer;
			size_t fileSize = 0;
			bool ok = _DownloadSyncFile(
				pPP,
				patchCur,
				csFromFilename,
				csToFilename,
				fileSize // downloaded file size
			);
			TimeMs timeMs = timer.ElapsedMs();

			if (!ok) {
				LogError("_DownloadFile() FAILED - '" << csFromFilename << "' (try " << (retry + 1) << " of " << maxRetries << ")");

				// Update Patch Metrics
				g_ru_pm_mutex.lock();
				++g_ru_pm.dlRetries;
				g_ru_pm_mutex.unlock();

				if (j == numPatches - 1)
					break; // we've tried 'em all this pass

				patchCur++;
				if (patchCur >= numPatches)
					patchCur = 0; // looping back to beginning

				if (::WaitForSingleObject(hCancel, 0) == WAIT_OBJECT_0)
					return false; // canceled
			} else {
				string timeStr;
				if (timeMs)
					StrBuild(timeStr, " " << FMT_TIME << timeMs << "ms");
				LogInfo("OK (" << fileSize << " bytes" << timeStr << ") - '" << csFromFilename << "'");

				// return the patch that worked to try it again first next time
				patch = patchCur;
				return true;
			}
		}
	}

	LogError("FAILED - '" << csFromFilename << "'");

	g_ru_pm_mutex.lock();
	++g_ru_pm.dlRetriesErr;
	g_ru_pm_mutex.unlock();

	return false;
}

static int PatchThreads() {
	return PATCH_THREADS;
}

static bool ProcessFiles(PatchProcess* pPP, CVersionInfo& serverVIFile, CVersionInfo& localVIFile) {
	if (!pPP)
		return false;

	// Reset Files Progress
	pPP->FilesReset();

	// Determine Files Needing Processed
	FileEntryMapPtr filesToProcess;
	FileEntryMap& serverVIFileEntries = serverVIFile.getFileEntries();
	FileEntryMap& localVIFileEntries = localVIFile.getFileEntries();
	for (auto& itr : serverVIFileEntries) {
		CVIFileEntry& serverFile = itr.second;

		string serverFilePath = serverFile.get_Filename();

		// DRF - ADDED
		// OldK Files No Longer Required
		const vector<string> c_files = {
			"aaaa",
			"ca-certs",
			"ClientBlades---dll",
			"CorePluginLibrary",
			"CrashRpt",
			"CrashSender",
			"dll.pak",
			"GameFiles_BladeScripts---dll",
			"GameFiles_Menus",
			"GameFiles_MenuMcripts",
			"GameFiles_ClientScripts",
			"GameFiles_Scripts",
			"gtk",
			"rabbitmq",
			"KEPlog",
			"KEPClient",
			"KEPMath",
			"kanevaclient",
			"kanevatray",
			"kanevamedia",
			"libmysql",
			"mysqlpp",
			"network.cfg",
			"pixmaps",
			"plugins---dll",
			"sasl2---dll",
			"SimpleAmqpClient",
			"minidump",
			"sounds_purple",
			"zzzz"
		};
		auto contains = any_of(c_files.cbegin(), c_files.cend(), [&](string s){ return STLContainsIgnoreCase(serverFilePath, s); });
		if (contains) {
			LogInfo(" ... Skipping(OLD_K_NOT_REQUIRED): " << serverFilePath);
			serverFile.set_Required(false);
		}

		// File Not Required ?
		if (!serverFile.get_Required())
			continue;

		// File Already Processed ?
		FileEntryMap::iterator found = localVIFileEntries.find(serverFilePath);
		if (found == localVIFileEntries.end()) {
			filesToProcess.insert(make_pair(serverFilePath, &serverFile));
			LogInfo(" ... Adding(NEW): " << serverFilePath);
		} else {
			CVIFileEntry& localFile = found->second;
			string localFilePath = pathHelper(pPP->localFilePath, "", localFile.FileNameFull());
			if (FileHelper::Exists(localFilePath)) {
				string serverMd5 = serverFile.get_MD5();
				string localMd5 = localFile.get_MD5();
				if (StrSameFileExt(localFilePath, "pak"))
					localMd5 = Md5File(localFilePath);
				if (localMd5 != serverMd5) {
					filesToProcess.insert(make_pair(serverFilePath, &serverFile));
					LogInfo(" ... Adding(MD5 " << serverMd5 << " != " << localMd5 << "): " << serverFilePath);
					continue;
				}
			} else {
				filesToProcess.insert(make_pair(serverFilePath, &serverFile));
				LogInfo(" ... Adding(MISSING): " << serverFilePath);
				continue;
			}

			LogInfo(" ... Skipping(MD5 MATCH): " << serverFilePath);
		}
	}
	pPP->FilesToProcess(filesToProcess.size());
	size_t filesToProcessSize = 0;
	for (const auto itr : filesToProcess)
		filesToProcessSize += itr.second->get_ServerFileSize();
	LogInfo("filesInPatch=" << serverVIFile.get_TotalFiles() << " (" << serverVIFile.get_TotalSize() << " bytes)");
	LogInfo("filesToProcess=" << pPP->FilesToProcess() << " (" << filesToProcessSize << " bytes)");

	// Files To Process ?
	if (!filesToProcess.size())
		return true;

	// Create Patch Downloader
	auto pWC = WebCaller::GetInstance(PATCH_WEBCALLER, true, PatchThreads());
	if (!pWC) {
		LogFatal("WebCaller::GetInstance() FAILED");
		return false;
	}

	// Queue All Files To Download (uncompress & unzip)
	for (auto& itr : filesToProcess)
		DownloadAsyncFile(pPP, pWC, itr.second);

	// Wait For All Files To Complete
	if (!DownloadAsyncWait(pPP, pWC)) {
		LogFatal("DownloadAsyncWait() FAILED - Aborting ...");
		return false;
	}

	return true;
}

UINT PatchThread(LPVOID lParam) {
	PATCH_RV rv = PATCH_NO;

	CVersionInfo serverVIFile;
	CVersionInfo localVIFile;

	string localVIFilePath;
	string serverVIFilePath;
	string zoneManifestFile;
	string pathTemp;
	long nTotalFiles = 0;

	LogInfo("BEGIN");

	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// Valid PatchProcess ?
	PatchProcess* pPP = (PatchProcess*)lParam;
	if (!pPP) {
		LogFatal("PatchProcess null");
		rv = PATCH_PROCESS_NULL;
		goto EndPatchThread;
	}

	// Log Patch Process Params
	if (pPP->m_urls.size()) {
		string url = pPP->GetUrl(0);
		TimeMs patchMs = pPP->patchInfo.serverStatus.m_timeMs;
		LogInfo("patchUrl='" << url << "' patchMs=" << FMT_TIME << patchMs);
		g_hw_pm.patchUrl = url;
		g_hw_pm.patchMs = patchMs;
	}

	// DRF - Bug Fix - Delete Existing Temp Folder
	pathTemp = PathTemp();
	LogInfo("tempPath='" << pathTemp << "'");
	if (!FileHelper::DeleteFolder(pathTemp)) {
		LogError("DeleteFolder() FAILED - '" << pathTemp << "'");
	}

	// Create Temp Folder
	if (!FileHelper::CreateFolder(pathTemp)) {
		LogFatal("CreateFolder() FAILED - '" << pathTemp << "'");
		rv = PATCH_CREATE_TEMP;
		goto EndPatchThread;
	}

	// Temporary VersionInfo & Zone Manifest File Paths
	serverVIFilePath = PathAdd(pathTemp, "server.tmp");
	zoneManifestFile = PathAdd(pathTemp, "zones.tmp");

	// Local Manifest File Is In Game Folder 'star\####\versioninfo.dat'
	if (pPP->patchInfo.patchType == "" && pPP->childGameId != 0) {
		// -- backwards compat -
		string tmpFileName = "versioninfo-" + to_string(static_cast<long long>(pPP->childGameId)) + ".dat";
		localVIFilePath = PathAdd(pPP->localFilePath, tmpFileName);
	} else {
		localVIFilePath = PathAdd(pPP->localFilePath, VERSIONINFO_DAT);
	}

	// Download VersionInfo Manfest File 'server.tmp' -> 'versioninfo.dat'
	size_t patch = 0;
	size_t numVersionInfoRetries = 0;
	bool ok = false;
	do {
		ok = DownloadSyncFile(
			pPP,
			VERSIONINFO_DAT,
			serverVIFilePath.c_str(),
			pPP->hEvClosing,
			patch);

		if (!ok) {
			if (pPP->childGameId != 0) { // if child patch, don't wait forever since probably in-game
				if (::WaitForSingleObject(pPP->hEvClosing, 0) != WAIT_OBJECT_0) {
					LogFatal("DownloadFile::WaitForSingleObject(Closing 1) FAILED");
					ERROR_MSG err(pPP->gameId, ERR_TIMEOUT);
					::SendMessage(pPP->hWnd, gMsgId, MSG_ERROR, (LONG)&err);
				}
				rv = PATCH_CLOSED_1;
				goto EndPatchThread;
			}
			fTime::SleepSec(10.0);
			numVersionInfoRetries++;
			if (pPP->hWnd && numVersionInfoRetries == 3)
				::PostMessage(pPP->hWnd, gMsgId, MSG_OTHERERROR, 0);
		}
	} while (!ok);

	// Download Zone Manfest File 'zones.tmp' -> 'zoneinfo.dat' ?
	bool getZoneManifest = (pPP->patchInfo.patchType == "" && pPP->childGameId == 0);
	if (getZoneManifest) {
		DownloadSyncFile(
			pPP,
			ZONEINFO_DAT,
			zoneManifestFile.c_str(),
			pPP->hEvClosing,
			patch);
	}

	PatchLibUtils::DoEvents();
	if (::WaitForSingleObject(pPP->hEvClosing, 0) == WAIT_OBJECT_0) {
		LogFatal("WaitForSingleObject(Closing 2) FAILED");
		ERROR_MSG err(pPP->gameId, ERR_TIMEOUT);
		::SendMessage(pPP->hWnd, gMsgId, MSG_ERROR, (LONG)&err);
		rv = PATCH_CLOSED_2;
		goto EndPatchThread;
	}

	// Load Server Version Info File 'temp\versioninfo.dat'
	LogInfo("BEGIN - Loading Server Manifest '" << serverVIFilePath << "'");
	if (!serverVIFile.loadFile(serverVIFilePath)) {
		LogFatal("CVersionInfo::loadFile() FAILED - '" << serverVIFilePath << "'");
		ERROR_MSG_VERBOSE err(0, E_INVALIDARG, "Error Loading Server Manifest");
		::SendMessage(pPP->hWnd, gMsgId, MSG_VERBOSE_ERROR, (LONG)&err);
		rv = PATCH_SERVER_MANIFEST;
		goto EndPatchThread;
	}
	nTotalFiles = serverVIFile.get_TotalFiles();
	LogInfo("END - Loading Server Manifest (" << nTotalFiles << " files)");

	// Ovewrite patch info from server with zone flags
	LogInfo("BEGIN - Process Zone Manifest '" << zoneManifestFile << "'");
	ProcessZoneManifest(serverVIFile, zoneManifestFile, pPP->zoneindex);
	LogInfo("END - Process Zone Manifest");

	// Load Local Version Info File 'star\####\versioninfo.dat'
	LogInfo("BEGIN - Loading Local Manifest '" << localVIFilePath << "' ...");
	bool localVIFileOk = localVIFile.loadFile(localVIFilePath);
	if (!localVIFileOk) {
		LogWarn("CVersionInfo::loadFile() FAILED - '" << localVIFilePath << "'");
	}
	LogInfo("END - Loading Local Manifest");

	// If the server patch UGPV and the local UGPV in the verionsinfo.dat don't match,
	// remove the star directory. Compare UGPV only if we have a local versioninfo.dat and we are not patching a 3dapp
	LogInfo("BEGIN - UGPV Flag Processing");
	if (localVIFileOk && (pPP->childGameId == 0) && (localVIFile.get_Ugpv() != serverVIFile.get_Ugpv())) {
		LogInfo(" ... LocalVIFile UGPV[" << localVIFile.get_Ugpv() << "] different serverVIfile UGPV[" << serverVIFile.get_Ugpv() << "]");

		// Check to make sure we are relatively in the Star directory and not perhaps in the c:\ root directory.
		// In addition, if our star game folder doesn't exist,  then we don't need to delete it.
		if (CStringA(pPP->localFilePath.c_str()).MakeLower().Find("\\star\\") != -1 && FileHelper::Exists(pPP->localFilePath)) {
			string gameFolder = pPP->localFilePath;
			LogWarn("DELETING FOLDER '" << gameFolder << "' ...");
			bool deleteResult = FileHelper::DeleteFolder(gameFolder);
			if (deleteResult) {
				localVIFile.clear();
				LogInfo("SUCCESS");
			} else {
				LogError("FAILED");
			}
		}
	}
	LogInfo("END - UGPV Flag Processing");

	// Remove Local Files No Longer Needed
	LogInfo("BEGIN - Remove Unused Files");
	RemoveUnusedFiles(pPP, localVIFile, serverVIFile);
	LogInfo("END - Remove Unused Files");

	// Create Folder Structure For All Files In Patch
	LogInfo("BEGIN - Create Folder Structure");
	if (!CreateFolderStructure(pPP, serverVIFile)) {
		LogFatal("CreateFolderStructure() FAILED");
		ERROR_MSG_VERBOSE err(0, E_FAIL, "Failed Creating Folders");
		strncpy_s(err.msg, _countof(err.msg), "Failed Creating Folders", _TRUNCATE);
		::SendMessage(pPP->hWnd, gMsgId, MSG_VERBOSE_ERROR, (LONG)&err);
		rv = PATCH_CREATE_FOLDERS;
		goto EndPatchThread;
	}
	LogInfo("END - Create Folder Structure");

	// Process All Files Until No Errors
	LogInfo("BEGIN - Process Files");
	do {
		if (!ProcessFiles(pPP, serverVIFile, localVIFile)) {
			LogFatal("ProcessFiles() FAILED - Aborting ...");
			rv = PATCH_FILES_FAIL;
			goto EndPatchThread;
		}

		// Files Errored ?
		auto filesErrored = pPP->FilesErrored();
		if (filesErrored) {
			g_ru_pm_mutex.lock();
			++g_ru_pm.dlRetries;
			g_ru_pm_mutex.unlock();
			LogError("FILES ERRORED - " << filesErrored << " of " << pPP->FilesToProcess() << " - Retrying ...");
			fTime::SleepSec(5.0);
		}
	} while (pPP->FilesErrored());
	LogInfo("END - Process Files");

	// Update Loose Files
	LogInfo("BEGIN - Update Loose Files");
	UpdateLooseFiles(pPP->localFilePath, NULL, NULL);
	LogInfo("END - Update Loose Files");

	// Create CustomTexture Folder ?
	if (pPP->patchInfo.patchType == "" || pPP->patchInfo.patchType == "WOK") {
		string pathCT = PathAdd(pPP->localFilePath, "CustomTexture");
		if (!FileHelper::CreateFolderDeep(pathCT)) {
			LogError("FileHelper::CreateFolderDeep() FAILED - '" << pathCT << "'");
		}
	}

	// Move Server Manifest To Local Manifest For Next Patch
	FileHelper::Move(serverVIFilePath, localVIFilePath);

	rv = PATCH_OK;

EndPatchThread:

	// Set Return Value Into Metrics
	g_hw_pm.patchRv = rv;

	// Tell Patch Progress Window We're Done (spawns child patches if there are any)
	pPP->OnCompleted();

	if (::WaitForSingleObject(pPP->hEvClosing, 0) == WAIT_OBJECT_0)
		::SendMessage(pPP->hWnd, gMsgId, MSG_ABORT, UP_CANCELED);

	SetEvent(pPP->hEv);

	if (rv == PATCH_OK) {
		LogInfo("SUCCESS");
		return TRUE;
	} else {
		LogFatal("FAILED - rv=" << rv);
		return FALSE;
	}
}

} // namespace KEP