///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "PatchLibGlobals.h"

#include "ConfigValues.h"
#include <KEPHelpers.h>
#include <KEPConfig.h>
#include <KEPException.h>
#include "NetworkCfg.h"
#include "KEPStar.h"

namespace KEP {

ConfigValues::ConfigValues() {
	// set default values
	m_loginUrl = DEFAULT_LOGIN_URL;
	m_serverListUrl = DEFAULT_SERVERLIST_URL;
	m_prodServerListUrl = DEFAULT_PROD_SERVERLIST_URL;
	m_authUrl = DEFAULT_AUTH_URL;
	m_gameInfoUrl = DEFAULT_GAMEINFO_URL;
	m_allowAccessUrl = DEFAULT_ALLOW_ACCESS_URL;
	m_startAppUrl = DEFAULT_START_APP_URL;
}

CStringA& ConfigValues::ServerListUrl(LONG gameId) {
	return (gameId == WOK_GAME_ID) ? m_prodServerListUrl : m_serverListUrl;
}

BOOL ConfigValues::LoadValues(const std::string& netCfgPath) {
	KEPConfig cfg;
	try {
		cfg.Load(netCfgPath, NETCFG_ROOT);
		m_loginUrl = cfg.Get(CLIENTDEFAULTLOGINURL_TAG, m_loginUrl).c_str();
		m_prodServerListUrl = cfg.Get(PRODSERVERLISTURL_TAG, m_prodServerListUrl).c_str();
		m_serverListUrl = cfg.Get(SERVERLISTURL_TAG, m_serverListUrl).c_str();
		m_authUrl = cfg.Get(CLIENTDEFAULTAUTHURL_TAG, m_authUrl).c_str();
		m_gameInfoUrl = cfg.Get(GAMEINFOURL_TAG, m_gameInfoUrl).c_str();
		m_allowAccessUrl = cfg.Get(ALLOWACCESSURL_TAG, m_allowAccessUrl).c_str();
		m_startAppUrl = cfg.Get(CLIENTDEFAULTSTARTAPPURL_TAG, m_startAppUrl).c_str();

		// DRF - Added - ParseGameLoginResult() Only Parses Xml Format Now
		std::string loginUrl = (const char*)m_loginUrl;
		if (!STLContainsIgnoreCase(loginUrl, "format=xml"))
			StrAppend(loginUrl, "&format=xml");
		m_loginUrl = loginUrl.c_str();
	} catch (...) {
		return FALSE;
	}
	return TRUE;
}

//Get the Kaneva Register Edit Info.
BOOL ConfigValues::LoadValues(LONG gameId) {
	m_launcherPath = PathApp().c_str();
	m_launcherPath += "\\";
	if (gameId != 0)
	{
		m_gamePath = m_launcherPath;
		wchar_t gameIdStr[64] = L"";
		_ltow_s(gameId, gameIdStr, _countof(gameIdStr), 10);
		m_gamePath += "Star\\";
		m_gamePath += gameIdStr;
		m_gamePath += "\\";
		auto netCfgPath = PathAdd(m_gamePath, NETCFG_CFG);
		if (LoadValues(netCfgPath))
			return TRUE;
	}
	auto netCfgPath = PathAdd(m_launcherPath, NETCFG_CFG);
	if (LoadValues(netCfgPath))
		return TRUE;
	return FALSE;
}

bool ConfigValues::FillInGameLoginUrl(std::string& gameLoginUrl, const char* userid, const char* gotoUrl) {
	// DRF - Added - ParseGameLoginResult() Only Parses Xml Format Now
	if (!STLContainsIgnoreCase(gameLoginUrl, "format=xml"))
		StrAppend(gameLoginUrl, "&format=xml");

	bool ret = false;
	std::string::size_type p = gameLoginUrl.find("{0}");
	if (p != std::string::npos) {
		gameLoginUrl = gameLoginUrl.replace(p, 3, userid);
		p = gameLoginUrl.find("{1}");
		if (p != std::string::npos) {
			gameLoginUrl = gameLoginUrl.replace(p, 3, gotoUrl);
		} else {
			gameLoginUrl.append("&goto=u&id=");
			gameLoginUrl.append(gotoUrl);
		}
		ret = true; // require only 1
	}
	return ret;
}

bool ConfigValues::FillInValidateUserUrl(std::string& validateUserUrl, const char* userid, const char* password, LONG gameId) {
	bool ret = false;
	std::string::size_type p = validateUserUrl.find("{0}");
	char gameIdStr[30];
	_ltoa_s(gameId, gameIdStr, _countof(gameIdStr), 10);
	if (p != std::string::npos) {
		validateUserUrl = validateUserUrl.replace(p, 3, userid);
		if ((p = validateUserUrl.find("{1}")) != std::string::npos) {
			validateUserUrl = validateUserUrl.replace(p, 3, password);
			if ((p = validateUserUrl.find("{2}")) != std::string::npos) {
				validateUserUrl = validateUserUrl.replace(p, 3, gameIdStr);
				ret = true; // require all 3
			}
		}
	}
	return ret;
}

bool ConfigValues::FillInStartAppUrl(std::string& startAppUrl, const char* gameUrl) {
	bool ret = false;
	std::string::size_type p = startAppUrl.find("{0}");
	if (p != std::string::npos) {
		startAppUrl = startAppUrl.replace(p, 3, gameUrl);
		ret = true; // require only 1
	}

	return ret;
}

} // namespace KEP
