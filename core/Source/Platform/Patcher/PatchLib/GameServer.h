///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>

namespace KEP {

class CGameServer : public CObject {
public:
	CGameServer();
	virtual ~CGameServer();

	std::string ToString() const;

	inline void SetEngineId(const std::string& token) { m_engineId = token; }
	inline std::string GetEngineId() const { return m_engineId; }

	inline void SetServerName(const std::string& token) { m_serverName = token; }
	inline std::string GetServerName() const { return m_serverName; }

	inline void SetGameName(const std::string& token) { m_gameName = token; }
	inline std::string GetGameName() const { return m_gameName; }

	inline void SetIPAddress(const std::string& token) { m_ipAddress = token; }
	inline std::string GetIPAddress() const { return m_ipAddress; }

	inline void SetPort(const std::string& token) { m_port = token; }
	inline std::string GetPort() const { return m_port; }

	inline void SetGameExe(const std::string& token) { m_gameExe = token; }
	inline std::string GetGameExe() const { return m_gameExe; }

	inline void SetNumPlayers(const std::string& token) { m_numPlayers = token; }
	inline std::string GetNumPlayers() const { return m_numPlayers; }

	inline void SetMaxPlayers(const std::string& token) { m_maxPlayers = token; }
	inline std::string GetMaxPlayers() const { return m_maxPlayers; }

	inline void SetServerStatus(const std::string& token) { m_serverStatus = token; }
	inline std::string GetServerStatus() const { return m_serverStatus; }

	inline void AddPatchURL(const std::string& token);
	inline std::string GetPatchURL(int index) const { return m_patchUrls[index]; }
	inline std::vector<std::string>& GetPatchUrls() { return m_patchUrls; }

	inline void SetPatchType(const std::string& token) { m_patchType = token; }
	inline std::string GetPatchType() const { return m_patchType; }

	inline void SetPatchPath(const std::string& token) { m_patchPath = token; }
	inline std::string GetPatchPath() const { return m_patchPath; }

private:
	std::string m_engineId;
	std::string m_serverName;
	std::string m_gameName;
	std::string m_ipAddress;
	std::string m_port;
	std::string m_gameExe;
	std::string m_numPlayers;
	std::string m_maxPlayers;
	std::string m_serverStatus;
	std::vector<std::string> m_patchUrls;
	std::string m_patchType;
	std::string m_patchPath;
};

inline void CGameServer::AddPatchURL(const std::string& token) {
	// always add slash so callers can concat easier
	m_patchUrls.push_back(token);
	if (m_patchUrls.back().length() > 0 &&
		(m_patchUrls.back().at(m_patchUrls.back().length() - 1) != '/')) {
		m_patchUrls.back() += "/";
	}
}

} // namespace KEP