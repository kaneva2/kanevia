///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "PatchLibGlobals.h"
#include <stack>
enum GL_RESULT;

#define MSG_PATCH_HANDLER_STARTED (WM_APP + 1)
#define MSG_PATCH_HANDLER_COMPLETE (WM_APP + 2)

namespace KEP {

typedef struct _PATCHING_COMPLETE_MSG {
	_PATCHING_COMPLETE_MSG() {
		clear();
	}

	void clear() {
		GameID = 0;
		NewGamePort = 0;
		NewGameServer = "";
		ChildGameId = 0;
		TotalPatches = 0;
		TotalFiles = 0;
	}

	long GameID;
	long NewGamePort;
	std::string NewGameServer;
	long ChildGameId;
	int TotalPatches;
	int TotalFiles;
	std::string TemplatePatchDir;
	std::string AppPatchDir;
} PATCHING_COMPLETE_MSG;

class CPatchHandler {
public: // infastructure
	CPatchHandler(HWND hwnd, const std::string& baseDir, const std::string& email, const std::string& url, long gameId, long parentGameId, long& ret);
	~CPatchHandler();

public: // methods
	bool start();
	bool cancel();

private: // members
	HWND m_hwnd;
	std::string m_baseDir;
	std::string m_email;
	std::string m_url;
	long m_gameId;
	long m_parentGameId;
	long& m_ret;

	std::stack<PatchInfo> m_pendingPatches;

	HWND m_hwndMsgMonitor;

	PATCHING_COMPLETE_MSG m_completeMsg;

	std::string m_serverIp;
	int m_serverPort;
	long m_childGameId;
	std::string m_gameName;

private: // callback
	UINT KEI_MSG_ID;
	static LRESULT CALLBACK MessageMonitorWindProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp);

private: // helpers
	void clear();
	void patchCompleted(COMPLETE_MSG* pMsg);
	bool processNextPatch();

	void forwardMsg(WPARAM wp, LPARAM lp);
	void sendPatchCompleted();
	void sendPatchError(HRESULT rc, std::string msg);

	// populates pendingPatches
	bool getPatchesInfo(GL_RESULT& glResult, std::string& resultDescription);

	bool createMonitorWindow(std::string& resultDescription);
	bool getGamePath(long gameId, std::string& gameDir);
};

} // namespace KEP