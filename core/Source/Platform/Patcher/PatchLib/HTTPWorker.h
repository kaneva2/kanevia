///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPHelpers.h"
#include "Core/Util/HighPrecisionTime.h"
#include <string>

namespace KEP {

enum PATCH_RV {
	PATCH_OK,
	PATCH_NO,
	PATCH_PROCESS_NULL,
	PATCH_CREATE_TEMP,
	PATCH_CLOSED_1,
	PATCH_CLOSED_2,
	PATCH_SERVER_MANIFEST,
	PATCH_TERMINATE_TRAY,
	PATCH_CREATE_FOLDERS,
	PATCH_FILES_FAIL
};

// threaded function - lParam is of type class PatchProcess
UINT PatchThread(LPVOID lParam);

// DRF - Added
struct hw_PatchMetrics {
	size_t zipNumOk;
	size_t zipNumErr;
	TimeMs zipMs;
	FileSize zipBytes;
	size_t pakNumOk;
	size_t pakNumErr;
	TimeMs pakMs;
	FileSize pakBytes;
	std::string patchUrl;
	TimeMs patchMs;
	size_t patchRv;
};
extern hw_PatchMetrics g_hw_pm;

// DRF - Added
struct ru_PatchMetrics {
	size_t dlNumOk;
	size_t dlNumErr;
	TimeMs dlMs;
	size_t dlBytes;
	size_t dlRetries;
	size_t dlRetriesErr;
};
extern ru_PatchMetrics g_ru_pm;

} // namespace KEP