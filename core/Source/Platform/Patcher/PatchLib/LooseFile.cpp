///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "LooseFile.h"
#include "Core/Util/Unicode.h"

using namespace KEP;

//Search an Specific Directory for an specific extension.
int SearchDirectory(std::vector<std::string>& refvecFiles,
	std::string& refcstrRootDirectory,
	std::string& refcstrExtension,
	bool bSearchSubdirectories) {
	std::string strFilePath; // Filepath
	std::string strPattern; // Pattern
	std::string strExtension; // Extension
	HANDLE hFile; // Handle to file
	WIN32_FIND_DATA FileInformation; // File information
	strPattern = refcstrRootDirectory + "\\*.*";
	hFile = ::FindFirstFile(Utf8ToUtf16(strPattern).c_str(), &FileInformation);
	if (hFile != INVALID_HANDLE_VALUE) {
		do {
			if (FileInformation.cFileName[0] != '.') {
				strFilePath.erase();
				strFilePath = refcstrRootDirectory + "\\" + Utf16ToUtf8(FileInformation.cFileName);

				if (FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					if (bSearchSubdirectories) {
						// Search subdirectory
						int iRC = SearchDirectory(refvecFiles,
							strFilePath,
							refcstrExtension,
							bSearchSubdirectories);
						if (iRC)
							return iRC;
					}
				} else {
					// Check extension
					strExtension = Utf16ToUtf8(FileInformation.cFileName);
					strExtension = strExtension.substr(strExtension.rfind(".") + 1);

					char extension[0xff];
					strcpy_s(extension, _countof(extension), strExtension.c_str());
					for (size_t i = 0; i < strlen(extension); i++)
						extension[i] = (char)tolower((int)extension[i]);

					strExtension = extension;
					strcpy_s(extension, _countof(extension), refcstrExtension.c_str());
					for (size_t i = 0; i < strlen(extension); i++)
						extension[i] = (char)tolower((int)extension[i]);

					refcstrExtension = "dat";

					if (strExtension != refcstrExtension) {
						// Save filename
						refvecFiles.push_back(strFilePath);
					}
				}
			}
		} while (::FindNextFile(hFile, &FileInformation) == TRUE);

		// Close handle
		::FindClose(hFile);

		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
			return dwError;
	}

	return 0;
}

FileDirectory::FileDirectory() {
	dirname = "";
}
FileDirectory::FileDirectory(const std::string name) {
	dirname = name;
}
FileDirectory::~FileDirectory() {
	Clear();
}
//Adding an new file in an directory.
void FileDirectory::AddFile(const std::string filename, const std::string crc, int filesize) {
	LooseFile loosefile;
	loosefile.crc = crc;
	loosefile.filesize = filesize;
	directoryfiles[filename] = loosefile;
}

//Get an Loose File.
LooseFile* FileDirectory::GetFile(const std::string filename) {
	std::map<std::string, LooseFile>::iterator w = directoryfiles.find(filename);
	if (w == directoryfiles.end())
		return NULL;
	return &w->second;
}

void FileDirectory::Erase(const std::string filename) {
	std::map<std::string, LooseFile>::iterator w = directoryfiles.find(filename);
	if (w == directoryfiles.end())
		return;
	directoryfiles.erase(w);
}

//Delete the specified file.
void FileDirectory::DeleteFile(const std::string filename) {
	std::map<std::string, LooseFile>::iterator w = directoryfiles.find(filename);

	if (w == directoryfiles.end())
		return;

	std::string fullpath = dirname + "\\" + w->first;
	std::wstring fullpathW = Utf8ToUtf16(fullpath);
	::SetFileAttributesW(fullpathW.c_str(), FILE_ATTRIBUTE_NORMAL);
	::DeleteFileW(fullpathW.c_str());
	directoryfiles.erase(w);
}

//Output .dat file similar to localtexturehash.dat, but
//it is sorted.
void FileDirectory::WriteDat(const std::string filename, const std::string rdmode) {
	FILE* file;
	if (fopen_s(&file, filename.c_str(), rdmode.c_str()) == 0) {
		std::map<std::string, LooseFile>::iterator w;
		for (w = directoryfiles.begin(); w != directoryfiles.end(); w++)
			fprintf(file, "%s,%s,%s,%d\n", dirname.c_str(), w->first.c_str(), w->second.crc.c_str(), w->second.filesize);

		fclose(file);
	}
}
//Clear the entire list
void FileDirectory::Clear() {
	directoryfiles.clear();
}

//Delete everything that the FileDirectory is referencing.
void FileDirectory::DeleteAll() {
	std::map<std::string, LooseFile>::iterator w;
	for (w = directoryfiles.begin(); w != directoryfiles.end(); w++) {
		std::string fullpath = dirname + w->first;
		std::wstring fullpathW = Utf8ToUtf16(fullpath);
		::SetFileAttributes(fullpathW.c_str(), FILE_ATTRIBUTE_NORMAL);
		::DeleteFile(fullpathW.c_str());
		directoryfiles.erase(w);
	}
}

//Get an Directory Handle.
FileDirectory* FileDirectoryList::GetDirectory(std::string dirname) {
	std::map<std::string, FileDirectory>::iterator w = m_dirlist.find(dirname);
	if (w == m_dirlist.end())
		return NULL;
	return &w->second;
}

//Clear all the directories.
void FileDirectoryList::Clear() {
	std::map<std::string, FileDirectory>::iterator w;
	for (w = m_dirlist.begin(); w != m_dirlist.end(); w++) {
		w->second.Clear();
	}
	m_dirlist.clear();
}
bool FileDirectoryList::DirectoryAlreadyExist(std::string dirname) {
	std::map<std::string, FileDirectory>::iterator w = m_dirlist.find(dirname);
	if (w == m_dirlist.end())
		return false;
	return true;
}
//Add an new directory.
void FileDirectoryList::AddDirectory(std::string dirname) {
	if (DirectoryAlreadyExist(dirname) == false) {
		FileDirectory dir(dirname);
		m_dirlist[dirname] = dir;
	}
}

void FileDirectoryList::WriteDat(const std::string filename) {
	//Create the file and make sure it is blank.
	FILE* file = 0;
	if (fopen_s(&file, filename.c_str(), "wt") == 0)
		fclose(file);

	std::map<std::string, FileDirectory>::iterator w;
	for (w = m_dirlist.begin(); w != m_dirlist.end(); w++) {
		w->second.WriteDat(filename, "a+");
	}
}

//Load an .dat file in the format of localtexturehash.dat
bool FileDirectoryList::LoadFilesAndDirectories(std::string filename) {
	FILE* file;
	fopen_s(&file, filename.c_str(), "rt");
	if (file) {
		while (ftell(file) < _filelength(_fileno(file))) {
			char buffer[512];
			char* gdirectory;
			char* gfilename;
			char* gcrc;
			char* size;
			int fsize = 0;
			fgets(buffer, 512, file);

			char* tokhelper = 0;

			if ((gdirectory = strtok_s(buffer, ",", &tokhelper)) != nullptr &&
				(gfilename = strtok_s(NULL, ",", &tokhelper)) != nullptr &&
				(gcrc = strtok_s(NULL, ",", &tokhelper)) != nullptr &&
				(size = strtok_s(NULL, ",", &tokhelper)) != nullptr) {
				fsize = atol(size);

				size_t len = strlen(gdirectory);
				for (size_t index = 0; index < len; index++) {
					if (gdirectory[index] == '_') {
						gdirectory[index] = '/';
					}
				}
				AddDirectory(gdirectory);
				FileDirectory* pdir = GetDirectory(gdirectory);
				pdir->AddFile(gfilename, gcrc, fsize);
			} else {
				// TODO: load from rc, or have caller do it.
				char buf[2 * MAX_PATH];
				sprintf_s(buf, _countof(buf), "Corrupt file detected: %s", filename.c_str());
				MessageBoxW(GetActiveWindow(), Utf8ToUtf16(buf).c_str(), L"Patcher", MB_ICONEXCLAMATION);
				return false;
			}
		}
		fclose(file);
	}
	return true;
}

//This function takes two .dat files which represent the files that needs updating.
//It will compare the CRC's, if that does not match, or the file is not
//in the source but in the destination, it will delete that file.
//The same applies if the src.dat file doesn't exist, it will clear
//the folder which the dst.dat is pointing to.
bool FileDirectoryList::CompareDirectories(std::string src, std::string dst, bool reloadsrc) {
	FILE* file = 0;
	if (fopen_s(&file, src.c_str(), "rt") != 0) {
		// mtp - disabled 04/07/09, this was deleting files downloaded dynamically by kepclient

		////The source file does not exist, therefore.
		////we need to clear the dst folder of all
		////the textures.
		//vector<string> allfiles;
		//string extension="*.*";
		//int lastslash = dst.find_last_of("\\");
		//string directory = dst.substr(0,lastslash);
		//SearchDirectory(allfiles,directory,extension,false);
		//for(size_t fileindex=0;fileindex<allfiles.size();fileindex++)
		//{
		//	string mfilename = allfiles[fileindex];
		//	SetFileAttributes(mfilename.c_str(),FILE_ATTRIBUTE_NORMAL);
		//	DeleteFile(mfilename.c_str());
		//}
		return true;
	} else
		fclose(file);

	FileDirectoryList destdir;

	//Check to see if we want to reload the source file.
	if (reloadsrc) {
		Clear();
		if (!LoadFilesAndDirectories(src))
			return false;
	}

	if (!destdir.LoadFilesAndDirectories(dst))
		return false;

	//Go through each directory found in the source.
	std::map<std::string, FileDirectory>::iterator srcbegin;
	for (srcbegin = m_dirlist.begin(); srcbegin != m_dirlist.end(); srcbegin++) {
		FileDirectory* srcdir = &srcbegin->second;

		//Check to see if the destination directory does exist in the source.
		FileDirectory* dstdir = destdir.GetDirectory(srcbegin->first);
		if (dstdir) {
			std::map<std::string, LooseFile>::iterator dstloose;

			//Since both directory are sorted, once we found something that is on the destionation
			//But not on the src, we can just go and break out or delete the files in there.
			for (dstloose = dstdir->directoryfiles.begin(); dstloose != dstdir->directoryfiles.end(); dstloose++) {
				LooseFile* srcloose = srcdir->GetFile(dstloose->first);
				if (!srcloose) {
					//If the file is not in the local hash we need to delete it from that folder.
					//If it exist.
					srcdir->DeleteFile(dstloose->first);
					continue;
				} else {
					LooseFile* destloose = dstdir->GetFile(dstloose->first);
					if (srcloose->crc != destloose->crc) {
						srcdir->DeleteFile(dstloose->first);
					} else {
						//Cut down the number of objects on the map,
						//without actually deleting the image itself.
						//so the next search is faster.
						srcdir->Erase(dstloose->first);
					}
				}
			}
		}
	}
	destdir.Clear();

	return true;
}