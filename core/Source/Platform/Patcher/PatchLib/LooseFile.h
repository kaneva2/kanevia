///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <windows.h>
#include <io.h>
#include <vector>
#include <string>
#include <map>

//Definition for an single loose file.
class LooseFile {
public:
	LooseFile() {
		crc = "";
		filesize = 0;
	}
	std::string crc;
	int filesize;
};

//An single directory.
class FileDirectory {
public:
	FileDirectory();
	FileDirectory(const std::string name);
	virtual ~FileDirectory();
	void Clear();
	void AddFile(const std::string filename, const std::string crc, int filesize);
	void DeleteFile(const std::string filename);
	void Erase(const std::string filename);
	void DeleteAll();
	void WriteDat(const std::string filename, const std::string rdmode = "wt");
	LooseFile* GetFile(const std::string filename);
	std::map<std::string, LooseFile> directoryfiles;

protected:
	std::string dirname;
};

//Holds the list of all the directories.
class FileDirectoryList {
protected:
	std::map<std::string, FileDirectory> m_dirlist;

public:
	virtual ~FileDirectoryList() { Clear(); }
	void Clear();
	void AddDirectory(std::string dirname);
	void WriteDat(const std::string filename);
	bool LoadFilesAndDirectories(std::string filename);
	bool CompareDirectories(std::string src, std::string dst, bool reloadsrc = true);
	bool DirectoryAlreadyExist(std::string dirname);
	int NumDirectories() { return (int)m_dirlist.size(); }
	FileDirectory* GetDirectory(std::string dirname);
};
