///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <afxtempl.h>

#include "CommUtil.h"
#include "gameserver.h"
#include "ConfigValues.h"
#include "..\KanevaRes\resource.h"

#include "WebCall.h"

namespace KEP {

static LogInstance("Instance");

// DRF - ED-1969 - Webcall Unification - Client/Server/Launcher::CommUtil::getGameServers()
// Expected URL is '<NET_KGP>/serverlist.aspx?id=<star>&v=2'
void CCommUtil::getGameServers(const std::string& url, CObList* gameServerList, bool retryOnEmptyResult /*= false*/) {
	// Reset Game Server List
	gameServerList->RemoveAll();

	// Get Game Servers List From Web
	std::string resultText;
	size_t resultSize = 0;
	DWORD httpStatusCode;
	std::string httpStatusDesc;
	bool ok = false;
	int retries = 5;
	do {
		ok = GetBrowserPage(url, resultText, resultSize, httpStatusCode, httpStatusDesc);
		if (!ok) {
			Sleep(5000); // 5 sec retry
			--retries;
		}
	} while (!ok && retries);
	if (!ok) {
		LogError("GetBrowserPage() FAILED - '" << url << "'");
		return;
	}

	if (!resultText.size()) {
		LogError("No Game Servers");
		return;
	}

	// Parse Into Lines (separated by \r\n)
	std::vector<std::string> lines;
	std::string current;
	for (size_t i = 0; i < resultText.size(); ++i) {
		char ch = resultText[i];
		if (ch == '\r')
			continue;
		if (ch == '\n') {
			lines.push_back(current);
			current.clear();
			continue;
		}
		current += resultText[i];
	}

	// Parse Servers From Lines
	for (const std::string& line : lines) {
		CStringA patchUrl;
		CGameServer* server = new CGameServer();

		bool more = true;
		{
			const char* tokenBegin = line.c_str();
			const char* tokenEnd;

			int ii = 0;
			while (more) {
				tokenEnd = strchr(tokenBegin, ';');
				if (tokenEnd == NULL) {
					tokenEnd = line.c_str() + line.size();
					more = false;
				}
				std::string strToken(tokenBegin, tokenEnd);
				const char* token = strToken.c_str();
				tokenBegin = tokenEnd + 1;

				switch (ii) {
					case 0:
						server->SetEngineId(token);
						break;
					case 1:
						server->SetServerName(token);
						break;
					case 2:
						server->SetGameName(token);
						break;
					case 3:
						server->SetIPAddress(token);
						break;
					case 4:
						server->SetPort(token);
						break;
					case 5:
						server->SetGameExe(token);
						break;
					case 6:
						patchUrl = token;
						break;
					case 7:
						server->SetNumPlayers(token);
						break;
					case 8:
						server->SetMaxPlayers(token);
						break;
					case 9:
						server->SetServerStatus(token);
						break;
					case 10:
						server->SetPatchType(token);
						break;
					case 11:
						server->SetPatchPath(token);
						break;
				}

				ii++;
			}
		}

		bool addServer = true; // add this server to the list?

		// do we already have this server in the list?
		// if so, replace "server" with one from list and add patch url to existing one
		for (POSITION p = gameServerList->GetHeadPosition(); p != 0;) {
			CGameServer* g = (CGameServer*)gameServerList->GetNext(p);
			if (g->GetEngineId() == server->GetEngineId() && g->GetPatchType() == server->GetPatchType()) {
				delete server;
				server = g;
				addServer = false;
			}
		}

		if (!patchUrl.IsEmpty()) {
			char tempbuffer[1024];
			strcpy_s(tempbuffer, _countof(tempbuffer), (const char*)patchUrl);

			char* token;
			char* tokenEnd = tempbuffer - 1;
			more = TRUE;

			while (more) {
				token = tokenEnd + 1;
				tokenEnd = strchr(token, ',');
				if (tokenEnd == NULL) {
					tokenEnd = token + strlen(token);
					more = false;
				} else {
					*tokenEnd = 0;
				}

				server->AddPatchURL(token);
			}
		}

		if (addServer)
			gameServerList->AddTail(server);
	}

	LogInfo("OK - gameServers=" << gameServerList->GetSize());
	for (POSITION p = gameServerList->GetHeadPosition(); p;) {
		auto pGS = (CGameServer*)gameServerList->GetNext(p);
		LogInfo(" ... " << pGS->ToString());
	}
}

} // namespace KEP