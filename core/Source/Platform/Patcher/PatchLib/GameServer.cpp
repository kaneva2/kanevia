///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GameServer.h"
#include "KEPHelpers.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace KEP {

CGameServer::CGameServer() {
}

CGameServer::~CGameServer() {
	m_patchUrls.clear();
}

std::string CGameServer::ToString() const {
	std::string str;
	StrBuild(str, "GameServer<"
		<< m_engineId << " "
		<< "'" << m_serverName << "' "
		<< "[" << m_ipAddress << "] "
		<< "status=" << m_serverStatus << " "
		<< "players=" << m_numPlayers << "/" << m_maxPlayers
		<< ">");
	return str;
}

} // namespace KEP