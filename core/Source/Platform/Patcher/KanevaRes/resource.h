///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDB_MBHEADER                    5002
#define IDS_PATCHCLIENTCTRL_NOT_FOUND   5003
#define IDS_CLASS_FACTORY_CREATE_FAILED 5004
#define IDB_BROWSERTAB                  5005
#define IDS_QUERY_INTERFACE_FAILED      5006
#define IDB_BROWSERTABINACTIVE          5007
#define IDS_NO_SERVERS_AVAILABLE        5008
#define IDS_ERROR_OBTAINING_SERVERS     5009
#define IDS_EXCEPTION_OBTAINING_SERVERS 5010
#define IDS_NOT_YET_IMPLEMENTED         5011
#define IDS_SERVER_NOT_RESPONDING       5012
#define IDS_VALIDATION_ERROR            5013
#define IDS_EMAIL_ERROR                 5014
#define IDS_VALIDATION_FAILED           5015
#define IDS_EXIT                        5016
#define IDS_WEBADDRESS1                 5017
#define IDS_WEBADDRESS2                 5019
#define IDR_BUTTON_DISABLED             5020
#define IDS_WEBADDRESS3                 5021
#define IDR_BUTTON_ENABLED              5022
#define IDS_KANEVA_ERROR_REPORT         5023
#define IDR_TOOLBAR                     5024
#define IDS_NO_ERROR_REPORT             5025
#define IDR_BROWSERHEADER               5026
#define IDS_SEND_ERROR_REPORT           5027
#define IDS_ERROR_REPORT_ALREADY_SEND   5028
#define IDS_ERROR_SEND                  5029
#define IDR_BACKGROUND2                 5030
#define IDS_STRING5030                  5030
#define IDS_REPORT_FAILED               5031
#define IDS_SUPPORT_URL                 5032
#define IDS_KAUTHENTICATION             5033
#define IDR_BUTTON_FORUM                5036
#define IDR_BUTTON_KANEVACOM            5037
#define IDR_BUTTON_SUPPORT              5038
#define IDR_BUTTON_HELP                 5039
#define IDR_QUITBUTTON                  5040
#define IDR_BUTTON_FORUM_CLICK          5041
#define IDR_BUTTON_KANEVACOM_CLICK      5042
#define IDR_BUTTON_HELP_CLICK           5043
#define IDR_PNG1                        5044
#define IDR_BUTTON_SUPPORT_CLICK        5044
#define IDR_BACKGROUND_MINI             5045

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        5046
#define _APS_NEXT_COMMAND_VALUE         5036
#define _APS_NEXT_CONTROL_VALUE         5037
#define _APS_NEXT_SYMED_VALUE           5038
#endif
#endif
