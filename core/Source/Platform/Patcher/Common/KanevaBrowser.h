///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include "SimpleBrowser.h"
#include "afxwin.h"

namespace KEP {

class CKanevaBrowser : public SimpleBrowser {
public:
	virtual bool OnBeforeNavigate2(CStringA URL, CStringA frame,
		void* post_data, int post_data_size,
		CStringA headers);
	virtual void OnDocumentComplete(CStringA URL);
	virtual void OnDownloadBegin();
	virtual void OnProgressChange(int progress, int progress_max);
	virtual void OnDownloadComplete();
	virtual void OnNavigateComplete2(CStringA URL);
	virtual void OnStatusTextChange(CStringA text);
	virtual void OnTitleChange(CStringA text);
};

} // namespace KEP