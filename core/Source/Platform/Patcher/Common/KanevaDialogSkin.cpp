///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KanevaDialogSkin.h"
#include "WindowLayering.h"
#include "ImageLib\ImageLoader.h"
#include "ImageLib\ImageRenderer.h"
#include "Core/Util/Unicode.h"

namespace KEP {

HINSTANCE CKanevaDialogSkin::s_resourceInstance(NULL);

CKanevaDialogSkin::CKanevaDialogSkin(CWnd* pParent /*=NULL*/) {
	Init();
}

CKanevaDialogSkin::CKanevaDialogSkin(UINT uResourceID, CWnd* pParent) :
		CDialog(uResourceID, pParent) {
	Init();
}

CKanevaDialogSkin::CKanevaDialogSkin(const wchar_t* pszResourceID, CWnd* pParent) :
		CDialog(pszResourceID, pParent) {
	Init();
}

CKanevaDialogSkin::~CKanevaDialogSkin() {
	FreeResources();
}

BOOL CKanevaDialogSkin::OnInitDialog() {
	CDialog::OnInitDialog();

	return TRUE;
}

BOOL CKanevaDialogSkin::SetTransparent(BYTE bAlpha) {
	m_transparent = m_layering.AddAlpha(m_hWnd, bAlpha) && bAlpha < 255;
	return m_transparent;
}

BOOL CKanevaDialogSkin::SetTransparentColor(COLORREF col, BOOL bTrans) {
	m_transparent = m_layering.AddTransparencyMask(m_hWnd, col, bTrans) && bTrans;
	return m_transparent;
}

void CKanevaDialogSkin::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
}

BOOL CKanevaDialogSkin::LoadSkinString(CStringA& str, UINT id) {
	CStringW strW;
	BOOL ret = strW.LoadString(GetResourceInstance(), id);
	str = Utf16ToUtf8(strW).c_str();
	return ret;
}

ImagePointer CKanevaDialogSkin::LoadSkinImage(UINT nID) {
	HINSTANCE hInstResource = NULL;

	// Find correct resource handle
	if (s_resourceInstance == NULL) {
		hInstResource = AfxFindResourceHandle(MAKEINTRESOURCE(nID), RT_BITMAP);
	} else {
		hInstResource = s_resourceInstance;
	}

	return CImageLoader::LoadImageFromResource(hInstResource, nID);
}

HBITMAP CKanevaDialogSkin::LoadBitmap(UINT nBitmap) {
	HBITMAP hBitmap = NULL;
	HINSTANCE hInstResource = NULL;

	// Find correct resource handle
	if (s_resourceInstance == NULL) {
		hInstResource = AfxFindResourceHandle(MAKEINTRESOURCE(nBitmap), RT_BITMAP);
	} else {
		hInstResource = s_resourceInstance;
	}

	// Load bitmap In
	hBitmap = (HBITMAP)::LoadImage(hInstResource, MAKEINTRESOURCE(nBitmap),
		IMAGE_BITMAP, 0, 0, 0);

	ASSERT(hBitmap);

	return hBitmap;
}

BEGIN_MESSAGE_MAP(CKanevaDialogSkin, CDialog)
//{{AFX_MSG_MAP(CKanevaDialogSkin)
ON_WM_LBUTTONDOWN()
ON_WM_ERASEBKGND()
ON_WM_SIZE()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKanevaDialogSkin message handlers

void CKanevaDialogSkin::OnLButtonDown(UINT nFlags, CPoint point) {
	if (m_bEasyMove)
		PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));

	CDialog::OnLButtonDown(nFlags, point);
}

void CKanevaDialogSkin::Init() {
	m_transparent = false;
	m_bEasyMove = FALSE;
	m_loStyle = LO_DEFAULT;
}

void CKanevaDialogSkin::FreeResources() {
}

DWORD CKanevaDialogSkin::SetBitmap(HBITMAP bitmap) {
	m_background = ImagePointer(new Gdiplus::Bitmap(bitmap, NULL));
	return IsImageValid(m_background);
}

DWORD CKanevaDialogSkin::SetBitmap(int nBitmap) {
	return SetBitmap(LoadBitmap(nBitmap));
}

DWORD CKanevaDialogSkin::SetImage(int imageID) {
	return SetImage(CImageLoader::LoadImageFromResource(GetResourceInstance(), imageID));
}

//Load an image by filename
DWORD CKanevaDialogSkin::SetImage(const char* filename) {
	return SetImage(CImageLoader::LoadImageFromFile(filename));
}

DWORD CKanevaDialogSkin::SetImage(ImagePointer image) {
	// Free any loaded resource
	FreeResources();

	if (image) {
		m_background = image;

		// Get bitmap size
		m_dwWidth = (DWORD)m_background->GetWidth();
		m_dwHeight = (DWORD)m_background->GetHeight();
		return 1;
	}

	if (IsWindow(this->GetSafeHwnd()))
		Invalidate();

	return 0;
}

DWORD CKanevaDialogSkin::SetBitmap(const char* lpszFileName) {
	HBITMAP hBitmap = NULL;
	hBitmap = (HBITMAP)::LoadImage(0, Utf8ToUtf16(lpszFileName).c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	return SetBitmap(hBitmap);
}

BOOL CKanevaDialogSkin::OnEraseBkgnd(CDC* pDC) {
	BOOL bRetValue = TRUE;

	if (NullImage(m_background)) {
		return CDialog::OnEraseBkgnd(pDC);
	}

	CRect rect;
	GetClientRect(rect);

	CImageRenderer r(*pDC);

	//Due to the fact that we are doing all the rendering ourselves,
	//we need to tell it when to clear.
	//	Invalidate(TRUE);

	if (m_loStyle == LO_DEFAULT || m_loStyle == LO_RESIZE) {
		r.PaintImage(m_background, rect);
	} else if (m_loStyle == LO_TILE) {
		int ixOrg, iyOrg;

		for (iyOrg = 0; iyOrg < rect.Height(); iyOrg += m_dwHeight) {
			for (ixOrg = 0; ixOrg < rect.Width(); ixOrg += m_dwWidth) {
				rect.MoveToXY(ixOrg, iyOrg);
				r.PaintImage(m_background, rect);
			}
		}
	} else if (m_loStyle == LO_CENTER) {
		int ixOrg = (rect.Width() - m_dwWidth) / 2;
		int iyOrg = (rect.Height() - m_dwHeight) / 2;
		rect.MoveToXY(ixOrg, iyOrg);
		r.PaintImage(m_background, rect);
	} else if (m_loStyle == LO_STRETCH) {
		rect.InflateRect(CSize(m_dwWidth, m_dwHeight));
		r.PaintImage(m_background, rect);
	}

	return bRetValue;
}

void CKanevaDialogSkin::OnSize(UINT nType, int cx, int cy) {
	CDialog::OnSize(nType, cx, cy);

	if (m_background) {
		Invalidate();
	}
}

void CKanevaDialogSkin::EnableEasyMove(BOOL pEnable) {
	m_bEasyMove = pEnable;
}

void CKanevaDialogSkin::SetStyle(LayOutStyle style) {
	m_loStyle = style;
	if (m_loStyle == LO_RESIZE && m_background) {
		SetWindowPos(0, 0, 0, m_dwWidth, m_dwHeight, SWP_NOMOVE | SWP_NOREPOSITION);
	}
}

} // namespace KEP