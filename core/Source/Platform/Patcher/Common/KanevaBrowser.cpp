///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KanevaBrowser.h"

namespace KEP {

bool CKanevaBrowser::OnBeforeNavigate2(CStringA URL,
	CStringA frame,
	void *post_data,
	int post_data_size,
	CStringA headers) {
	SimpleBrowser::OnBeforeNavigate2(URL, frame, post_data, post_data_size, headers);
	return false;
}

void CKanevaBrowser::OnDocumentComplete(CStringA URL) {
	SimpleBrowser::OnDocumentComplete(URL);
}

void CKanevaBrowser::OnDownloadBegin() {
	// While the page is loading, hide the web browser control.
	this->ShowWindow(SW_HIDE);
	SimpleBrowser::OnDownloadBegin();
}

void CKanevaBrowser::OnProgressChange(int progress, int progress_max) {
	SimpleBrowser::OnProgressChange(progress, progress_max);
}

void CKanevaBrowser::OnDownloadComplete() {
	SimpleBrowser::OnDownloadComplete();

	// Once the web document has been downloaded in its entirety,
	// display the web control.
	this->ShowWindow(SW_SHOW);
	this->UpdateWindow();
}

void CKanevaBrowser::OnNavigateComplete2(CStringA URL) {
	SimpleBrowser::OnNavigateComplete2(URL);
}

void CKanevaBrowser::OnStatusTextChange(CStringA text) {
	SimpleBrowser::OnStatusTextChange(text);
}

void CKanevaBrowser::OnTitleChange(CStringA text) {
	SimpleBrowser::OnTitleChange(text);
}

} // namespace KEP