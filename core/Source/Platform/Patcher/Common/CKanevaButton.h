///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "WindowLayering.h"
#include "KanevaDialogSkin.H"

namespace KEP {

typedef UINT(CALLBACK* MSIMG32callBack)(HDC, CONST PTRIVERTEX, DWORD, CONST PVOID, DWORD, DWORD);

class CKanevaButton : public CButton {
	/////////////////////////////////////////////////////////////////////////////
	// Construction
public:
	//! Standard constructor.
	CKanevaButton();

	//! Standard destructor.
	virtual ~CKanevaButton();

	/////////////////////////////////////////////////////////////////////////////
	// Attributes
public:
	//! Button types
	enum Type {
		//! Static button
		staticButton = 0,
		//! Standard pushbutton
		pushButton,
		//! Hot tracked pushbutton
		hotPushButton,
		//! Standard pushbutton with dropdown
		pushButtonDropDown,
		//! Hot tracked pushbutton with dropdown
		hotPushButtonDropDown,
		//! Multi pushbutton with dropdown
		pushButtonMulti,
		//! Hot tracked multi pushbutton with dropdown
		hotPushButtonMulti,
		//! Standard check button
		checkButton,
		//! Hot tracked check button
		hotCheckButton,
		//! Standard checkbox
		checkBox,
		//! Standard radio button
		radio,
		//! Hyperlink
		hyperlink,
	};

	//! Text styles
	enum Text {
		//! No text displayed
		none = 0,
		//! Single-line left-justified text
		singleLine,
		//! Single-line centered text
		singleLineCenter,
		//! Multi-line left-justified text
		multiLine,
	};

	//! Focus styles
	enum Focus {
		//! No focus rectangle displayed
		noFocus = 0,
		//! Show focus rectangle
		normalFocus,
		//! Show focus rectangle and defualt button indicator
		defaultFocus,
	};

	/////////////////////////////////////////////////////////////////////////////
	// Operations
public:
	//! Gets the button's type.
	//! @return   The button's type.
	CKanevaButton::Type getType() { return m_type; }

	//! Sets the button's type.
	//! @param  type    The button's type.
	void setType(CKanevaButton::Type type);

	//! Gets the button's text style.
	//! @return   The button's text style.
	CKanevaButton::Text getTextStyle() { return m_textStyle; }

	//! Sets the button's text style.
	//! @param  textStyle   The button's text style.
	void setTextStyle(CKanevaButton::Text textStyle);

	//! Gets the button's text color.
	//! @return   The button's text color.
	COLORREF getTextColor() { return m_rgbText; }

	//! Sets the button's text color.
	//! @param  rgbText   The button's text color.
	void setTextColor(COLORREF rgbText) { m_rgbText = rgbText; }

	//! Gets the button's focus style.
	//! @return   The button's focus style.
	CKanevaButton::Focus getFocusStyle() { return m_focusStyle; }

	//! Sets the button's focus style.
	//! @param  focusStyle   The button's focus style.
	void setFocusStyle(CKanevaButton::Focus focusStyle);

	void setFont(const char* name, int size, bool bold = false, bool italic = false);

	//! Sets a transparency mask color for the background of the button
	//! @return   Success/Fail
	bool setBackgroundTransparentColor(COLORREF rgb, bool trans);

	//! Sets and alpha value for transparency of the background of the button
	//! @return Success/Fail
	bool setBackgroundAlpha(BYTE bAlpha);

	//! Gets the button's gradient property.
	//! @return   The button's gradient property.
	bool isGradient() { return m_bGradient; }

	//! Sets the button's gradient property.
	//! @param  bGradient   Flag: draw gradient background.
	void setGradient(bool bGradient) { m_bGradient = bGradient; }

	//! Sets the button's background color property.
	//! @param  bGradient   Flag: draw color background on button.
	void setColoredButton(bool bcolor) { m_bcolor = bcolor; }

	//! Sets the button's background color.
	//! @param  bGradient   Flag: button color.
	void colorButton(COLORREF m_color) { m_backcolor = m_color; }

	//! Sets the button's bitmap id.
	//! @param  nBitmapId       Bitmap id (-1 means none).
	//! @param  rgbTransparent  Bitmap's transparency color (default = RGB(255,0,255)
	void setBitmapId(int nBitmapId, COLORREF rgbTransparent = RGB(255, 0, 255), bool sizeToFit = true);

	//! Sets the button's bitmap id for the disabled state.
	//! @param  nBitmapId       Bitmap id (-1 means none).
	//! @param  rgbTransparent  Bitmap's transparency color (default = RGB(255,0,255)
	void setDisabledBitmapId(int nBitmapId, COLORREF rgbTransparent = RGB(255, 0, 255));

	//! Gets the button's checked state.
	//! @return   The button's checked state.
	bool isChecked() { return m_bChecked; }

	//! Sets the button's checked state if the button type is checkButton or
	//! hotCheckButton.
	//! @param    bChecked    Flag: button is checked.
	void check(bool bChecked);

	//! Checks if the multi pushbutton's drop-down was clicked.
	//! @return   true if the button's drop-down was clicked, false otherwise.
	bool isMultiClicked() { return m_bMultiClicked; }

	//! Sets a multi pushbutton to its normal unpressed state.
	void clearMultiClick();

	//! Displays a popup menu if the button type is pushButton, pushButtonDropDown,
	//! hotPushButton or hotPushButtonDropDown.
	//! @param    pMenu   The popup menu to be displayed.
	//! @return   The menu selection.
	int displayPopupMenu(CMenu* pMenu);

	//! Enables/disables the button.
	//! @param    bEnable   Flag: button is enabled.
	void enable(bool bEnable);

	//! Adds the button to a named button group.
	//! @param    strGroupName    Name of button group.
	//! @return   true if successful, false otherwise.
	bool addToGroup(CStringA strGroupName);

	//! Removes the button from its parent button group.
	//! @return   true if successful, false otherwise.
	bool removeFromGroup();

	//! Gets the ID for the bitmap to paint with, depending
	//! on the enabled state of the button
	inline int GetBitmapID() const { return this->IsWindowEnabled() ? m_nBitmapId : m_nDisabledBitmapId; }

	/////////////////////////////////////////////////////////////////////////////
	// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKanevaButton)
public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

	/////////////////////////////////////////////////////////////////////////////
	// Implementation
	// Generated message map functions
protected:
	//{{AFX_MSG(CKanevaButton)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	virtual void PreSubclassWindow();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	//! button font
	CFont m_font;

	CBitmap m_backgroundBitmap;

	//! Button type.
	CKanevaButton::Type m_type;

	//! Text style.
	CKanevaButton::Text m_textStyle;

	//! Focus style.
	CKanevaButton::Focus m_focusStyle;

	// Text color.
	COLORREF m_rgbText;

	//! Flag: mouse is being tracked.
	BOOL m_bTracking;

	//! Bitmap id.
	int m_nBitmapId;

	//! Bitmap id.
	int m_nDisabledBitmapId;

	//! fontsize
	int m_fontsize;

	//! Flag: draw single-line centered text.
	bool m_bCenter;

	//! Flag: draw multi-line text
	bool m_bMultiLine;

	//! Flag: button is checked.
	bool m_bChecked;

	//! Flag: display text.
	bool m_bText;

	//! Flag: display dropdown indicator.
	bool m_bDropDown;

	//! Flag: behave as multi button.
	bool m_bMulti;

	//! Flag: the button's drop-down was clicked.
	bool m_bMultiClicked;

	//! Flag: display as static control.
	bool m_bStatic;

	//! Flag: behave as hot button.
	bool m_bHot;

	//! Flag: display as hyperlink
	bool m_bHyperlink;

	//! Flag: draw gradient background for pushbuttons.
	bool m_bGradient;

	//! Flag: draw colored bacground.
	bool m_bcolor;

	// Bitmap transparency color.
	COLORREF m_rgbTransparent;

	// Bitmap transparency color.
	COLORREF m_rgbDisabledTransparent;

	// Background color.
	COLORREF m_backcolor;

	// Flag: is default button
	bool m_bDefaultButton;

	// Parent button group
	// CStringA m_strButtonGroup;

	// Do we want a transparent background?
	bool m_backgroundTransparent;

	// Do we change our size to suit the image, or the image to suit us
	bool m_autoSize;

	// Class to help with transparency
	CWindowLayering m_layering;

	// Number of CKanevaButton instances.
	static int m_nRefs;

	// The collection of button groups.
	// static CMapStringToPtr  m_btnGroups;

	//! Handle to msimg32.dll.
	static HINSTANCE m_hMsimg32;

	//! Pointer to gradient fill function in msimg32.dll.
	static MSIMG32callBack m_dllGradientFillFunc;

	//! Top color of gradient button background.
	static COLORREF m_rgbTopGradient;

	//! Top color of hot gradient button background.
	static COLORREF m_rgbTopGradientHot;

	//! Bottom color of gradient button background.
	static COLORREF m_rgbBottomGradient;

	//! Bottom color of hot gradient button background.
	static COLORREF m_rgbBottomGradientHot;

	/////////////////////////////////////////////////////////////////////////////
	// Helper functions
protected:
	//! Draws the button's frame
	void drawFrame(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct);

	//! Draws the button's frame for a radio/checkbox button
	void drawRadioCheckFrame(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct,
		int& nLeftEdge);

	//! Draw the colored text.
	void drawColoredButton(LPDRAWITEMSTRUCT lpDrawItemStruct);

	//! Draws the button's bitmap
	void drawBitmap(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct,
		int& nBitmapRightEdge);

	//! Draws the button's caption
	void drawCaption(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct,
		int nLeftEdge,
		int& nRightEdge);

	//! Draws the button's drop-down indicator
	void drawDropDown(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct);

	//! Draws a multi-button's pressed drop-down region
	void drawMultiDropDownRegion(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct);

	//! Draws the button's focus rectangle
	void drawFocus(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct,
		int nLeftEdge,
		int nRightEdge);

	//! Draws the border for a default button
	void drawDefaultBorder(CDC* pDC,
		LPDRAWITEMSTRUCT lpDrawItemStruct);

	//! Draws a bitmap with transparency color.
	//! See http://www.codeguru.com/Cpp/G-M/bitmap/specialeffects/article.php/c1749/
	void TransparentBlt(HDC hdcDest, int nXDest, int nYDest, int nWidth,
		int nHeight, HBITMAP hBitmap, int nXSrc, int nYSrc,
		COLORREF colorTransparent, HPALETTE hPal);

	//! Draws a bitmap as disabled.
	//! See http://www.codeguru.com/Cpp/G-M/bitmap/specialeffects/article.php/c1699/
	void DisabledBlt(HDC hdcDest, int nXDest, int nYDest, int nWidth,
		int nHeight, HBITMAP hbm, int nXSrc, int nYSrc);

	//! Draws the button's drop-down triangle.
	void drawDropDownIndicator(HDC hdcDest, int nX, int nY, int nWidthDropDown, int nHeightDropDown);

	//! Draws the button's frame when hot tracking.
	void drawHotButtonFrame(LPDRAWITEMSTRUCT lpDrawItemStruct);

	//! Checks if the mouse was clicked on multi part of button.
	void multiHitTest(CPoint pt);

	//! Loads a bitmap from the skin, or the exe, if not present
	HBITMAP loadSkinBitmap(int ID);

	//! Gets a button group by name
	//! @param  strGroupName    Group name
	//! @param  pGroup          Returned group
	static void getButtonGroup(CStringA strGroupName,
		void*& pGroup);

	//! Resets static storage used by the class.
	static void reset();

	//! Draws a gradient rectangle.
	//! See http://www.codeproject.com/staticctrl/gradient_static.asp
	static void drawGradientRect(CDC* pDC,
		CRect r,
		COLORREF cLeft,
		COLORREF cRight,
		BOOL bVertical);
};

} // namespace KEP