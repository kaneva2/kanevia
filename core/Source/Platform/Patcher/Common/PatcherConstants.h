#pragma once

const char* const OLD_WOK_GAME_NAME = "World of Kaneva";
const char* const WOK_GAME_ID_STR = "3296";
const DWORD WOK_GAME_ID = 3296;

#define TRAY_CALLBACK_WNDCLASS L"wndClassKanevaTrayCallback"
#define TRAY_CALLBACK_WNDTITLE L"wndKanevaTrayCallback_%d"
#define TRAY_PATCHER_MSG_NAME L"msgKanevaTrayPatcher"
enum TrayPatcherMsgType { PATCHING_DONE,
	PATCHING_CANCELED,
	PLEASE_EXIT };

#define POLITE_TRAY_EXITING_WAIT_TIME 30000 // wait up to 30 seconds for kanevatray to respond to PLEASE_EXIT message
#define TRAY_WMQUIT_WAIT_TIME 35000 // wait up to another 5 seconds for kanevatray to respond to WM_QUIT
#define MAX_TRAY_EXITING_WAIT_TIME 40000 // wait up to a total of 40 seconds for kanevatray to exit
#define MAX_TRAY_MUTEX_CREATION_WAIT_TIME 3000 // wait up to 3 seconds for kanevatray to create mutex

#define GA_ENABLE_LOGGING
#define GA_PAGE_PATCHER_INIT "patcher-init"
#define GA_PAGE_PATCHER_EXIT "patcher-exit"
#define GA_PAGE_PATCHER_START "patcher-patch-start"
#define GA_PAGE_PATCHER_COMPLETE "patcher-patch-complete"
#define GA_PAGE_PATCHER_COMPLETE_ZEROFILES "patcher-patch-complete-zerofiles"
#define GA_PAGE_PATCHER_SPAWN_INIT "patcher-spawn-init"
#define GA_PAGE_PATCHER_SPAWN "patcher-spawn"
#define GA_PAGE_PATCHER_SPAWN_ERROR "patcher-spawn-error"
#define GA_PAGE_PATCHER_SPAWN_MUTEX_NA "patcher-spawn-mutex-na"
#define GA_PAGE_PATCHER_CLIENTEXISTS "patcher-clientexists"
#define GA_PAGE_PATCHER_CLIENTEXISTS_WINDOW_NA "patcher-clientexists-window-na"
#define GA_PAGE_PATCHER_TRAYEXISTS "patcher-trayexists"
#define GA_PAGE_PATCHER_ORPHANTMPGZBUG "patcher-orphantmpgzbug"
#define GA_PAGE_PATCHER_UNPACK_FAILED "patcher-unpack-failed"
#define GA_PAGE_PATCHER_UNPACK_NOTFOUND "patcher-unpack-notfound"
#define GA_PAGE_PATCHER_UNPACKPREV_FAILED "patcher-unpackprev-failed"
#define GA_PAGE_PATCHER_UNPACKPREV_NOTFOUND "patcher-unpackprev-notfound"
#define GA_PAGE_PATCHER_UNZIP_MOVEFILEFAILED "patcher-unzip-movefilefailed"
#define GA_PAGE_PATCHER_UNZIPPREV_MOVEFILEFAILED "patcher-unzipprev-movefilefailed"
#define GA_PAGE_PATCHER_CLOSETRAY_GRACEFULEXIT "patcher-killtray-gracefulexit"
#define GA_PAGE_PATCHER_CLOSETRAY_FORCEQUIT "patcher-killtray-forcequit"
#define GA_PAGE_PATCHER_CLOSETRAY_BRUTALKILL "patcher-killtray-brutalkill"
#define GA_PAGE_PATCHER_CLOSETRAY_NORESPONSE "patcher-killtray-noresponse"

#define STR_NOT_CONNECTED "It seems you are not connected, would you like to try anyway?"
#define STR_THREAD_FAILED "Failed to start thread, please re-start this program."
#define STR_NO_TEMP_PATH "Can not find the TEMP folder."
#define STR_UPDATE_COMPLETE "Update Complete"
#define STR_DOWNLOADING "Downloading updates (%d of %d)"
#define STR_CONNECTING "Connecting to Server"
#define STR_ERROR_CONNECTING "Error Connecting"
#define STR_ERROR_RETRIEVE "Error Retrieving Version Info"
#define STR_SCANNING "Scanning Files (%d of %d)"
#define STR_PREPARING "Preparing to receive a changed file..."
#define STR_ERROR_ATTR "Failed to Change Attributes on File"
#define STR_ERROR_DELETE "Failed to Delete File"
#define STR_ERROR_MOVE "Failed to Move File"
#define STR_ERROR_CRC "Bad File CRC"
#define STR_REGISTERING "Registering a File..."
#define STR_NO_DEST_PATH "No destination path"
