///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StaticEx.h"
#include "WindowLayering.h"
#include "Core/Util/Unicode.h"

namespace KEP {

CBrush CStaticEx::m_bkBrush = NULL;
int once = 0;
CStaticEx::CStaticEx() {
	m_over = false;
	m_link = "";
	SetColor(0xff, 0xff, 0xff);
	m_underline = false;
	m_italic = false;
	m_size = 8;
	m_transparent = false;
	m_bold = false;
	m_backcolor = GetSysColor(COLOR_BTNFACE);
	if (once == 0)
		m_bkBrush.CreateStockObject(NULL_BRUSH);
	once = 1;
}

CStaticEx::CStaticEx(bool underline, bool italic, int size) {
	m_over = false;
	m_link = "";
	SetColor(0xff, 0xff, 0xff);
	m_underline = underline;
	m_italic = italic;
	m_size = size;
	m_transparent = false;
	m_bold = false;
	m_backcolor = GetSysColor(COLOR_BTNFACE);
	if (once == 0)
		m_bkBrush.CreateStockObject(NULL_BRUSH);
	once = 1;
}

CStaticEx::~CStaticEx() {
	m_underlineFont.DeleteObject();
}

BEGIN_MESSAGE_MAP(CStaticEx, CStatic)
ON_WM_CTLCOLOR_REFLECT()
ON_WM_ERASEBKGND()
ON_WM_MOUSEMOVE()
ON_CONTROL_REFLECT(STN_CLICKED, OnClicked)
END_MESSAGE_MAP()

void CStaticEx::PreSubclassWindow() {
	DWORD dwstyle = GetStyle();
	::SetWindowLong(GetSafeHwnd(), GWL_STYLE, dwstyle | SS_NOTIFY);

	m_cursor = LoadCursor(NULL, IDC_HAND);

	CFont* font = GetFont();

	LOGFONT lg;
	font->GetLogFont(&lg);

	lg.lfUnderline = m_underline;
	lg.lfItalic = m_italic;
	lg.lfWeight = m_bold ? FW_BOLD : FW_NORMAL;
	lg.lfHeight = -MulDiv(m_size, GetDeviceCaps(::GetDC(this->GetSafeHwnd()), LOGPIXELSY), 72);

	m_underlineFont.CreateFontIndirect(&lg);
	SetFont(&m_underlineFont);

	CStatic::PreSubclassWindow();
}

void CStaticEx::OnClicked() {
	if (m_underline)
		::ShellExecute(0, L"open", Utf8ToUtf16(m_link).c_str(), 0, 0, SW_SHOWNORMAL);
}

HBRUSH CStaticEx::CtlColor(CDC* pDC, UINT nCtlColor) {
	pDC->SetTextColor(RGB(rgb[0], rgb[1], rgb[2]));
	if (m_transparent) {
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	} else {
		return (HBRUSH)GetStockObject(WHITE_BRUSH);
	}
}

bool CStaticEx::SetBackgroundTransparentColor(COLORREF rgb, bool trans) {
	if (m_layering.AddTransparencyMask(m_hWnd, rgb, trans)) {
		m_transparent = trans;
		return true;
	} else {
		m_transparent = false;
		return false;
	}
}

//Update the text on the window.
void CStaticEx::setWindowText(const char* lpszString) {
	CWnd* pParent = GetParent();
	if (pParent) {
		CRect rcStatic;
		GetWindowRect(&rcStatic);
		pParent->ScreenToClient(rcStatic);
		pParent->InvalidateRect(rcStatic);
		pParent->SendMessage(WM_PAINT);
	}
	CStatic::SetWindowText(Utf8ToUtf16(lpszString).c_str());
	Invalidate();
}
bool CStaticEx::SetBackgroundAlpha(BYTE bAlpha) {
	if (m_layering.AddAlpha(m_hWnd, bAlpha)) {
		m_transparent = (bAlpha < 255);
		return true;
	} else {
		m_transparent = false;
		return false;
	}
}

BOOL CStaticEx::OnEraseBkgnd(CDC* pDC) {
	CRect rect;
	GetClientRect(&rect);

	if (!m_transparent) {
		pDC->FillSolidRect(rect, m_backcolor);
	} else {
		if (m_parentSectionBitmap.GetSafeHandle() == NULL) {
			CRect Rect;
			GetWindowRect(&Rect);
			CWnd* pParent = GetParent();
			ASSERT(pParent);
			pParent->ScreenToClient(&Rect); //convert our corrdinates to our parents

			//copy what's on the parents at this point
			CDC* pDC = pParent->GetDC();
			CDC MemDC;
			MemDC.CreateCompatibleDC(pDC);
			m_parentSectionBitmap.CreateCompatibleBitmap(pDC, Rect.Width(), Rect.Height());
			CBitmap* pOldBmp = MemDC.SelectObject(&m_parentSectionBitmap);

			MemDC.BitBlt(0, 0, Rect.Width(), Rect.Height(), pDC, Rect.left, Rect.top, SRCCOPY);

			MemDC.SelectObject(pOldBmp);

			pParent->ReleaseDC(pDC);
		} else //copy what we copied off the parent the first time back onto the parent
		{
			CRect Rect;
			GetClientRect(Rect);
			CDC MemDC;
			MemDC.CreateCompatibleDC(pDC);
			CBitmap* pOldBmp = MemDC.SelectObject(&m_parentSectionBitmap);
			pDC->BitBlt(0, 0, Rect.Width(), Rect.Height(), &MemDC, 0, 0, SRCCOPY);
			MemDC.SelectObject(pOldBmp);
		}
	}

	if (m_Bmp.GetSafeHandle() != NULL) {
		CRect Rect;
		GetClientRect(Rect);
		CDC MemDC;
		MemDC.CreateCompatibleDC(pDC);
		CBitmap* pOldBmp = MemDC.SelectObject(&m_Bmp);
		pDC->BitBlt(0, 0, Rect.Width(), Rect.Height(), &MemDC, 0, 0, SRCCOPY);
		MemDC.SelectObject(pOldBmp);
	}

	return TRUE;
}

void CStaticEx::LoadBitmap(UINT bitmapID) {
	VERIFY(m_Bmp.LoadBitmap(bitmapID));
}

void CStaticEx::LoadBitmap(HINSTANCE hInstance, UINT bitmapID) {
	HBITMAP hBmp = (HBITMAP)::LoadImage(hInstance, MAKEINTRESOURCE(bitmapID), IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR);
	if (hBmp) {
		m_Bmp.Attach(hBmp);
	}
}

void CStaticEx::OnMouseMove(UINT nFlags, CPoint point) {
	//Set the mouse cursor to the hand cursor.
	if (m_underline)
		::SetCursor(m_cursor);
}

} // namespace KEP