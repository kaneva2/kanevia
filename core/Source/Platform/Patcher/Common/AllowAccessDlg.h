///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "..\Common\KanevaBrowser.h"
#include "..\Common\PatcherCommon.h"
#include <string>

namespace KEP {

// CAllowAccessDlg dialog

class IBrowserCallback {
public:
	virtual bool OnBeforeNavigate(CStringA &URL, CStringA &frame,
		void *post_data, int post_data_size,
		CStringA &headers) = 0;
};

class CCallbackBrowser : public CKanevaBrowser {
public:
	CCallbackBrowser(IBrowserCallback *cb) :
			m_cb(cb) {}

	virtual bool OnBeforeNavigate2(CStringA URL, CStringA frame,
		void *post_data, int post_data_size,
		CStringA headers) {
		if (m_cb == 0 || m_cb->OnBeforeNavigate(URL, frame, post_data, post_data_size, headers))
			return CKanevaBrowser::OnBeforeNavigate2(URL, frame, post_data, post_data_size, headers);
		else
			return false; // cancel or handled navigation
	}

	IBrowserCallback *m_cb;
};

class CAllowAccessDlg : public CDialog, public IBrowserCallback {
	DECLARE_DYNAMIC(CAllowAccessDlg)

public:
	CAllowAccessDlg(const char *link, CWnd *pParent = NULL, HWND callBack = 0, ULONG lastAllowedId = 0);
	virtual ~CAllowAccessDlg();

	// Dialog Data
	enum { IDD = IDD_ALLOW_DLG };

	virtual bool OnBeforeNavigate(CStringA &URL, CStringA &frame,
		void *post_data, int post_data_size,
		CStringA &headers);

	CCallbackBrowser m_browser;

	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	virtual INT_PTR DoModal();

protected:
	virtual void DoDataExchange(CDataExchange *pDX); // DDX/DDV support

	std::string m_link;
	ULONG m_lastAllowedId;
	HWND m_callback;
	bool m_isModal;

	DECLARE_MESSAGE_MAP()
};

} // namespace KEP