///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include ".\imageloader.h"
#define SAFE_DELETES(p) \
	{                   \
		if (p) {        \
			delete[] p; \
			p = NULL;   \
		}               \
	}

namespace KEP {

CImageLoader::CImageLoader(void) {
}

CImageLoader::~CImageLoader(void) {
}

ImagePointer CImageLoader::LoadImageFromResource(HINSTANCE hInst, UINT id) {
	return LoadImageFromResource_Main(hInst, id);
}

ImagePointer CImageLoader::LoadImageFromFile(const char* filename) {
	return LoadImageFromFile_Main(filename);
}
ImagePointer CImageLoader::LoadImageFromResource(UINT id) {
	return LoadImageFromResource_Main(AfxGetResourceHandle(), id);
}

//Load an Image from file.
ImagePointer CImageLoader::LoadImageFromFile_Main(const char* filename) {
	ImagePointer image;
	FILE* file = 0;
	if (fopen_s(&file, filename, "rb") != 0) {
		image = ImagePointer();
		return image;
	}

	//Read the PNG Image data.
	fseek(file, 0, SEEK_END);
	DWORD imageSize = ftell(file);
	fseek(file, 0, SEEK_SET);
	char* pResourceData = new char[imageSize];
	fread(pResourceData, imageSize, 1, file);

	HGLOBAL hBuffer = ::GlobalAlloc(GMEM_MOVEABLE, imageSize);
	if (hBuffer) {
		void* pBuffer = ::GlobalLock(hBuffer);
		if (pBuffer) {
			CopyMemory(pBuffer, pResourceData, imageSize);
			SAFE_DELETES(pResourceData);
			IStream* pStream = NULL;
			if (::CreateStreamOnHGlobal(hBuffer, FALSE, &pStream) == S_OK) {
				image = ImagePointer(new Gdiplus::Image(pStream));
				pStream->Release();
			}
			::GlobalUnlock(hBuffer);
		}
		::GlobalFree(hBuffer);
	}
	if (NotNullImage(image) && image->GetLastStatus() != Gdiplus::Ok) {
		image = ImagePointer();
	}

	fclose(file);
	return image;
}

ImagePointer CImageLoader::LoadImageFromResource_Main(HINSTANCE hInst, UINT id) {
	ImagePointer image;

	HRSRC hResource = ::FindResource(hInst, MAKEINTRESOURCE(id), _T("PNG"));

	ASSERT(hResource);

	if (hResource) {
		DWORD imageSize = ::SizeofResource(hInst, hResource);
		if (imageSize) {
			const void* pResourceData = ::LockResource(::LoadResource(hInst, hResource));

			if (pResourceData) {
				HGLOBAL hBuffer = ::GlobalAlloc(GMEM_MOVEABLE, imageSize);
				if (hBuffer) {
					void* pBuffer = ::GlobalLock(hBuffer);
					if (pBuffer) {
						CopyMemory(pBuffer, pResourceData, imageSize);

						IStream* pStream = NULL;
						if (::CreateStreamOnHGlobal(hBuffer, FALSE, &pStream) == S_OK) {
							image = ImagePointer(new Gdiplus::Image(pStream));
							pStream->Release();
						}
						::GlobalUnlock(hBuffer);
					}
					::GlobalFree(hBuffer);
				}
			}
		}
	}
	if (NotNullImage(image) && image->GetLastStatus() != Gdiplus::Ok) {
		image = ImagePointer();
	}
	return image;
}

} // namespace KEP