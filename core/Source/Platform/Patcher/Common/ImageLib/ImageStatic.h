///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ImageLibTypes.h"

namespace KEP {

// CImageStatic
class CImageStatic : public CStatic {
	DECLARE_DYNAMIC(CImageStatic)

public:
	CImageStatic();
	virtual ~CImageStatic();
	BOOL SetImageId(int ID);
	void SetTextAttributes(const char* fontName, int size, COLORREF color = RGB(0, 0, 0), bool bold = false, bool italic = false, bool underline = false);
	void OffsetText(int x, int y);

protected:
	DECLARE_MESSAGE_MAP()
	int m_x, m_y;
	ImagePointer m_image;
	CBitmap m_backgroundBitmap;
	CDrawTextInfo m_drawTextInfo;
	virtual void CreateBackgroundImage();

public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
};

} // namespace KEP