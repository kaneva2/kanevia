///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define IMAGE_SUCCESS Gdiplus::Ok
#include <memory>
#include <algorithm>
namespace Gdiplus
{
	using std::min;
	using std::max;
}
#include <gdiplus.h>

#define NullImage(a) !a
#define NotNullImage(a) a
#define IsImageValid(a) NotNullImage(a) && a->GetLastStatus() == Ok
#define ImagePointerFromHBITMAP(hBitmap) Gdiplus::Bitmap::FromHBITMAP(hBitmap, NULL)

namespace KEP {

typedef Gdiplus::Image ImageType;
typedef std::shared_ptr<ImageType> ImagePointer;
typedef ImagePointer ImagePointerRef;

enum TextAlignment {
	Left,
	Center,
	Right
};

struct CDrawTextInfo {
	CStringA FontName;
	COLORREF TextColor;
	int Size;
	bool Italic;
	bool Underline;
	bool Bold;
	TextAlignment Align;

	CDrawTextInfo() {
		FontName = "Arial";
		TextColor = RGB(0, 0, 0);
		Size = 8;
		Italic = false;
		Underline = false;
		Bold = false;
		Align = Center;
	}
	static CDrawTextInfo Default;
};

} // namespace KEP