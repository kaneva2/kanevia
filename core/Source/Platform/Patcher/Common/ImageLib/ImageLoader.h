///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ImageLibTypes.h"

namespace KEP {

class CImageLoader {
public:
	CImageLoader(void);
	~CImageLoader(void);

	static ImagePointer LoadImageFromResource(HINSTANCE hInst, UINT id);
	static ImagePointer LoadImageFromResource(UINT id);
	static ImagePointer LoadImageFromFile(const char* filename);

protected:
	static ImagePointer LoadImageFromResource_Main(HINSTANCE hInst, UINT id);
	static ImagePointer LoadImageFromFile_Main(const char* filename);
};

} // namespace KEP