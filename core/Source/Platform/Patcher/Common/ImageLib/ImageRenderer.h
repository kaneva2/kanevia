///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ImageLibTypes.h"

using namespace Gdiplus;

namespace KEP {

class CImageRenderer {
	Graphics m_g;

public:
	CImageRenderer(HDC destDC);
	~CImageRenderer(void);

	BOOL PaintImage(ImagePointerRef image);

	BOOL PaintImage(ImagePointerRef image, LPPOINT destPoint);

	BOOL PaintImage(ImagePointerRef image, LPRECT destRect);

	BOOL EraseRectangle(LPRECT rect);

	BOOL SolidFillRectangle(LPRECT rect, COLORREF color);

	BOOL PaintText(const char* lpszText, LPPOINT destPoint, CDrawTextInfo* info = NULL);

	BOOL PaintText(const char* lpszText, LPRECT destRect, CDrawTextInfo* info = NULL);

	BOOL PaintText(const wchar_t* lpszText, LPPOINT destPoint, CDrawTextInfo* info = NULL);

	BOOL PaintText(const wchar_t* lpszText, LPRECT destRect, CDrawTextInfo* info = NULL);

protected:
	BOOL PaintImage(ImagePointerRef image, const Rect& rect);
};

} // namespace KEP