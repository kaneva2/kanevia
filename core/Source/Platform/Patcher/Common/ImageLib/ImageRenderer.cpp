///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include ".\imagerenderer.h"
#include "atlconv.h"
#include "Core/Util/Unicode.h"
#pragma warning(disable : 4267)

namespace KEP {

CDrawTextInfo CDrawTextInfo::Default;

CImageRenderer::CImageRenderer(HDC destDC) :
		m_g(destDC) {
	m_g.SetInterpolationMode(Gdiplus::InterpolationModeHighQuality);
	m_g.SetCompositingQuality(Gdiplus::CompositingQualityHighQuality);
	m_g.SetCompositingMode(Gdiplus::CompositingModeSourceOver);
}

CImageRenderer::~CImageRenderer(void) {
}

BOOL CImageRenderer::PaintImage(ImagePointerRef image) {
	return PaintImage(image, Rect(0, 0, image->GetWidth(), image->GetHeight()));
}

BOOL CImageRenderer::PaintImage(ImagePointerRef image, LPPOINT destPoint) {
	Rect r(Point(destPoint->x, destPoint->y), Size(image->GetWidth(), image->GetHeight()));
	return PaintImage(image, r);
}

BOOL CImageRenderer::PaintImage(ImagePointerRef image, LPRECT destRect) {
	Rect r(destRect->left, destRect->top, destRect->right - destRect->left, destRect->bottom - destRect->top);
	return PaintImage(image, r);
}

BOOL CImageRenderer::PaintImage(ImagePointerRef image, const Rect& rect) {
	if (NullImage(image))
		return false;

	return Ok == m_g.DrawImage(&*image, rect);
}

BOOL CImageRenderer::PaintText(const char* lpszText, LPPOINT destPoint, CDrawTextInfo* info) {
	return PaintText(Utf8ToUtf16(lpszText).c_str(), destPoint, info);
}

BOOL CImageRenderer::PaintText(const wchar_t* lpszText, LPPOINT destPoint, CDrawTextInfo* info) {
	USES_CONVERSION;

	if (info == NULL) {
		info = &CDrawTextInfo::Default;
	}

	Color c(GetRValue(info->TextColor), GetGValue(info->TextColor), GetBValue(info->TextColor));
	SolidBrush brush(c);
	FontFamily family(Utf8ToUtf16(info->FontName).c_str());
	FontStyle style = FontStyleRegular;

	if (info->Bold && info->Italic) {
		style = FontStyleBoldItalic;
	} else if (info->Bold) {
		style = FontStyleBold;
	} else if (info->Italic) {
		style = FontStyleItalic;
	}

	if (info->Underline) {
		style = FontStyleUnderline;
	}

	Font font(&family, (REAL)info->Size, style);
	PointF origin((REAL)destPoint->x, (REAL)destPoint->y);
	return Ok == m_g.DrawString(lpszText, wcslen(lpszText), &font, origin, &brush);
}

BOOL CImageRenderer::PaintText(const char* lpszText, LPRECT destRect, CDrawTextInfo* info) {
	return PaintText(Utf8ToUtf16(lpszText).c_str(), destRect, info);
}

BOOL CImageRenderer::PaintText(const wchar_t* lpszText, LPRECT destRect, CDrawTextInfo* info) {
	USES_CONVERSION;

	if (info == NULL) {
		info = &CDrawTextInfo::Default;
	}

	Color c(GetRValue(info->TextColor), GetGValue(info->TextColor), GetBValue(info->TextColor));
	SolidBrush brush(c);
	FontFamily family(Utf8ToUtf16(info->FontName).c_str());
	FontStyle style = FontStyleRegular;

	if (info->Bold && info->Italic) {
		style = FontStyleBoldItalic;
	} else if (info->Bold) {
		style = FontStyleBold;
	} else if (info->Italic) {
		style = FontStyleItalic;
	}

	if (info->Underline) {
		style = FontStyleUnderline;
	}

	Font font(&family, (REAL)info->Size, style);
	RectF rect((REAL)destRect->left,
		(REAL)destRect->top,
		(REAL)destRect->right - (REAL)destRect->left,
		(REAL)destRect->bottom - (REAL)destRect->top);

	StringFormat* format = StringFormat::GenericDefault()->Clone();
	switch (info->Align) {
		case Left:
			format->SetLineAlignment(StringAlignmentNear);
			format->SetAlignment(StringAlignmentNear);
			break;
		case Center:
			format->SetLineAlignment(StringAlignmentCenter);
			format->SetAlignment(StringAlignmentCenter);
			break;
		case Right:
			format->SetLineAlignment(StringAlignmentFar);
			format->SetAlignment(StringAlignmentFar);
			break;
	}
	return Ok == m_g.DrawString(lpszText, wcslen(lpszText), &font, rect, format, &brush);
}

BOOL CImageRenderer::SolidFillRectangle(LPRECT rect, COLORREF color) {
	Color c(GetRValue(color), GetGValue(color), GetBValue(color));

	SolidBrush b(c);

	Rect r(Point(rect->left, rect->top), Size(rect->right - rect->left, rect->bottom - rect->top));

	return Ok == m_g.FillRectangle(&b, r);
}

BOOL CImageRenderer::EraseRectangle(LPRECT rect) {
	SolidBrush b(Color(ARGB(0)));

	Rect r(Point(rect->left, rect->top), Size(rect->right - rect->left, rect->bottom - rect->top));

	return Ok == m_g.FillRectangle(&b, r);
}

} // namespace KEP