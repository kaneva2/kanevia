///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ImageLoader.h"

namespace KEP {

// CImageButton

enum ButtonType {
	Enabled,
	Disabled,
	Pressed,
	MouseOver,
	ButtonTypeCount
};

class CImageButton : public CButton {
	DECLARE_DYNAMIC(CImageButton)
	static const int clickXOffset = 0; //2;
	static const int clickYOffset = 0; //2;
public:
	CImageButton();
	virtual ~CImageButton();
	BOOL SetImageId(int id, ButtonType type = Enabled, bool sizeButtonToFit = false);
	void SetTextAttributes(const char* fontName, int size, ButtonType type = Enabled, COLORREF textColor = RGB(0, 0, 0),
		bool bold = false, bool italic = false, bool underline = false);
	void UseHandPointer(bool set) { m_handpointer = set; }

protected:
	ImagePointer m_images[ButtonTypeCount];
	CDrawTextInfo m_drawTextInfo[ButtonTypeCount];
	bool m_handpointer;
	ImagePointerRef GetImage(ButtonType);
	CBitmap m_backgroundBitmap;

protected:
	virtual void OnMouseMove(UINT nFlags, CPoint point);
	virtual void PreSubclassWindow();
	virtual BOOL OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL CreateCommon(LPCREATESTRUCT lpCreateStruct);
	virtual void CreateBackgroundImage();

	DECLARE_MESSAGE_MAP()
public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pwnd, UINT nCtlColor);
};


} // namespace KEP