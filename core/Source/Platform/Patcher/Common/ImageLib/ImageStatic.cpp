///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ImageStatic.h"
#include "ImageLoader.h"
#include "ImageRenderer.h"
#include "ImageStatic.h"
#include "Core/Util/Unicode.h"

namespace KEP {

IMPLEMENT_DYNAMIC(CImageStatic, CStatic)
CImageStatic::CImageStatic() {
	m_x = m_y = 0;
}

CImageStatic::~CImageStatic() {
}

BOOL CImageStatic::SetImageId(int ID) {
	m_image = CImageLoader::LoadImageFromResource(ID);
	return IsImageValid(m_image);
}

void CImageStatic::OffsetText(int x, int y) {
	m_x = x;
	m_y = y;
}

void CImageStatic::SetTextAttributes(const char* fontName, int size, COLORREF color, bool bold, bool italic, bool underline) {
	m_drawTextInfo.FontName = fontName;
	m_drawTextInfo.Size = size;
	m_drawTextInfo.Bold = bold;
	m_drawTextInfo.Italic = italic;
	m_drawTextInfo.Underline = underline;
	m_drawTextInfo.TextColor = color;
}

BEGIN_MESSAGE_MAP(CImageStatic, CStatic)
ON_WM_ERASEBKGND()
ON_WM_PAINT()
ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

// CImageStatic message handlers
BOOL CImageStatic::OnEraseBkgnd(CDC* pDC) {
	CImageRenderer r(*pDC);

	CRect rect;

	GetClientRect(&rect);

	VERIFY(r.EraseRectangle(rect));

	return TRUE;
}

void CImageStatic::OnPaint() {
	CPaintDC dc(this);
	CImageRenderer r(dc);

	CRect rect;
	GetClientRect(&rect);

	CreateBackgroundImage();

	// This is to copy in a section of the parent window into our background.
	if (m_backgroundBitmap.GetSafeHandle() != NULL) {
		CDC MemDC;
		CDC* pDC = CDC::FromHandle(dc);
		MemDC.CreateCompatibleDC(pDC);
		CBitmap* pOldBmp = MemDC.SelectObject(&m_backgroundBitmap);
		pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, SRCCOPY);
		MemDC.SelectObject(pOldBmp);
	}

	if (NotNullImage(m_image)) {
		VERIFY(r.PaintImage(m_image, &rect));
	}

	CStringW text;
	GetWindowText(text);
	if (!text.IsEmpty()) {
		CPoint textPoint(rect.left + m_x, rect.top + m_y);
		VERIFY(r.PaintText(Utf16ToUtf8(text).c_str(), &textPoint, &m_drawTextInfo));
	}
}

HBRUSH CImageStatic::CtlColor(CDC* pDC, UINT /*nCtlColor*/) {
	// TODO:  Change any attributes of the DC here
	pDC->SetBkMode(TRANSPARENT);

	// TODO:  Return a non-NULL brush if the parent's handler should not be called
	return (HBRUSH)GetStockObject(NULL_BRUSH);
}

void CImageStatic::CreateBackgroundImage() {
	if (m_backgroundBitmap.GetSafeHandle() == NULL) {
		CRect Rect;
		GetWindowRect(&Rect);
		CWnd* pParent = GetParent();
		ASSERT(pParent);
		pParent->ScreenToClient(&Rect); //convert our corrdinates to our parents

		//copy what's on the parents at this point
		CDC* pDC = pParent->GetDC();
		CDC MemDC;
		MemDC.CreateCompatibleDC(pDC);
		m_backgroundBitmap.CreateCompatibleBitmap(pDC, Rect.Width(), Rect.Height());
		CBitmap* pOldBmp = MemDC.SelectObject(&m_backgroundBitmap);

		MemDC.BitBlt(0, 0, Rect.Width(), Rect.Height(), pDC, Rect.left, Rect.top, SRCCOPY);

		MemDC.SelectObject(pOldBmp);

		pParent->ReleaseDC(pDC);
	}
}

} // namespace KEP