///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ImageButton.h"
#include "ImageRenderer.h"

#ifndef IDC_HAND
#define IDC_HAND MAKEINTRESOURCE(32649)
#endif

namespace KEP {

IMPLEMENT_DYNAMIC(CImageButton, CButton)
CImageButton::CImageButton() {
	m_handpointer = false;
}

CImageButton::~CImageButton() {
}

BOOL CImageButton::SetImageId(int id, ButtonType type, bool sizeButtonToFit) {
	m_images[type] = CImageLoader::LoadImageFromResource(id);

	if (IsImageValid(m_images[type])) {
		if (sizeButtonToFit)
			SetWindowPos(NULL, 0, 0, m_images[type]->GetWidth() + clickXOffset,
				m_images[type]->GetHeight() + clickYOffset,
				SWP_NOZORDER | SWP_NOMOVE);
		return TRUE;
	} else {
		return FALSE;
	}
}

HBRUSH CImageButton::OnCtlColor(CDC* pDC, CWnd* pwnd, UINT nCtlColor) {
	LOGBRUSH brush;
	pDC->SetBkMode(TRANSPARENT);
	brush.lbColor = HOLLOW_BRUSH;
	brush.lbHatch = 0;
	brush.lbStyle = 0;
	return CreateBrushIndirect(&brush);
}
void CImageButton::SetTextAttributes(const char* fontName, int size, ButtonType type, COLORREF textColor,
	bool bold, bool italic, bool underline) {
	m_drawTextInfo[type].FontName = fontName;
	m_drawTextInfo[type].Size = size;
	m_drawTextInfo[type].Bold = bold;
	m_drawTextInfo[type].Italic = italic;
	m_drawTextInfo[type].Underline = underline;
	m_drawTextInfo[type].TextColor = textColor;
}

void CImageButton::PreSubclassWindow() {
	CreateCommon(NULL);
}

BOOL CImageButton::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	return CreateCommon(lpCreateStruct);
}

BOOL CImageButton::CreateCommon(LPCREATESTRUCT lpCreateStruct) {
	if (lpCreateStruct != NULL) {
		lpCreateStruct->style |= BS_OWNERDRAW;
	} else {
		ModifyStyle(NULL, BS_OWNERDRAW);
	}

	return true;
}

BEGIN_MESSAGE_MAP(CImageButton, CButton)
ON_WM_ERASEBKGND()
ON_WM_MOUSEMOVE()
ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

//Mouse movement handler.
void CImageButton::OnMouseMove(UINT nFlags, CPoint point) {
	if (m_handpointer) {
		HCURSOR hCursor = ::LoadCursor(NULL, IDC_HAND);
		if (NULL != hCursor)
			::SetCursor(hCursor);
	} else {
		HCURSOR hCursor = ::LoadCursor(NULL, IDC_ARROW);
		if (NULL != hCursor)
			::SetCursor(hCursor);
	}
	CButton::OnMouseMove(nFlags, point);
}
// CImageButton message handlers
void CImageButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) {
	ButtonType type;

	if (lpDrawItemStruct->itemState & ODS_SELECTED) {
		type = Pressed;
	} else if (lpDrawItemStruct->itemState & ODS_HOTLIGHT) {
		type = MouseOver;
	} else if (lpDrawItemStruct->itemState & ODS_DISABLED) {
		type = Disabled;
	} else {
		type = Enabled;
	}

	ImagePointerRef pImage = GetImage(type);
	if (NullImage(pImage))
		return;

	ASSERT(IsImageValid(pImage));

	CRect rect;
	GetClientRect(&rect);

	CreateBackgroundImage();

	// This is to copy in a section of the parent window into our background.
	if (m_backgroundBitmap.GetSafeHandle() != NULL) {
		CDC MemDC;
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		MemDC.CreateCompatibleDC(pDC);
		CBitmap* pOldBmp = MemDC.SelectObject(&m_backgroundBitmap);
		pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, SRCCOPY);
		MemDC.SelectObject(pOldBmp);
	}

	CImageRenderer r(lpDrawItemStruct->hDC);

	//Only paint in the bounds of the actual window
	//Otherwise, the image will stretch and leave the client area.
	rect.DeflateRect(clickXOffset, clickYOffset);

	if (lpDrawItemStruct->itemState & ODS_SELECTED) {
		rect.OffsetRect(clickXOffset, clickYOffset);
	}

	//Draw the Image.
	VERIFY(r.PaintImage(pImage, &rect));

	CStringW text;
	GetWindowText(text);
	if (!text.IsEmpty()) {
		r.PaintText(text, &rect, &m_drawTextInfo[type]);
	}
}

void CImageButton::CreateBackgroundImage() {
	if (m_backgroundBitmap.GetSafeHandle() == NULL) {
		CRect Rect;
		GetWindowRect(&Rect);
		CWnd* pParent = GetParent();
		ASSERT(pParent);
		pParent->ScreenToClient(&Rect); //convert our corrdinates to our parents

		//copy what's on the parents at this point
		CDC* pDC = pParent->GetDC();
		CDC MemDC;
		MemDC.CreateCompatibleDC(pDC);
		m_backgroundBitmap.CreateCompatibleBitmap(pDC, Rect.Width(), Rect.Height());
		CBitmap* pOldBmp = MemDC.SelectObject(&m_backgroundBitmap);

		MemDC.BitBlt(0, 0, Rect.Width(), Rect.Height(), pDC, Rect.left, Rect.top, SRCCOPY);

		MemDC.SelectObject(pOldBmp);

		pParent->ReleaseDC(pDC);
	}
}

ImagePointerRef CImageButton::GetImage(ButtonType type) {
	switch (type) {
		case Enabled:
			return m_images[type];
		case Disabled:
		case Pressed:
		case MouseOver:
			if (NotNullImage(m_images[type])) {
				return m_images[type];
			} else {
				return m_images[Enabled];
			}
		default:
			return m_images[Enabled];
	};
}

BOOL CImageButton::OnEraseBkgnd(CDC* pDC) {
	CImageRenderer r(*pDC);
	CRect rect;
	GetClientRect(&rect);
	VERIFY(r.EraseRectangle(&rect));
	return TRUE;
}

} // namespace KEP