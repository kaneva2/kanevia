///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AllowAccessDlg.h"
#include "..\PatchLib\PatchLibGlobals.h"

namespace KEP {

IMPLEMENT_DYNAMIC(CAllowAccessDlg, CDialog)

static UINT KEI_MSG_ID = ::RegisterWindowMessageW(L"KEI_PATCH_STATUS_MSG");

CAllowAccessDlg::CAllowAccessDlg(const char* link, CWnd* pParent, HWND callBack, ULONG lastAllowedId) :
		CDialog(CAllowAccessDlg::IDD, pParent),
		m_link(link),
		m_callback(callBack),
		m_lastAllowedId(lastAllowedId),
		m_isModal(false),
		m_browser(this) {
}

CAllowAccessDlg::~CAllowAccessDlg() {
}

void CAllowAccessDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
}

INT_PTR CAllowAccessDlg::DoModal() {
	m_isModal = true;
	INT_PTR ret = CDialog::DoModal();
	m_isModal = false;
	return ret;
}

const long DEFAULT_WIDTH = 500;
const long DEFAULT_HEIGHT = 300;

BOOL CAllowAccessDlg::OnInitDialog() {
	m_browser.CreateFromControl(this, IDC_ALLOW_BROWSER);
	m_browser.Navigate(m_link.c_str());

	GetParent()->EnableWindow(FALSE);
	SetWindowPos(0, 0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT, SWP_NOZORDER | SWP_NOMOVE);
	m_browser.SetWindowPos(0, 0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT, SWP_NOZORDER | SWP_NOMOVE);
	CenterWindow();

	return CDialog::OnInitDialog();
}

#define OURNAV_PREFIX "bogus://"
#define OURNAV_CANCEL OURNAV_PREFIX "cancel"
#define OURNAV_CLOSE OURNAV_PREFIX "close"
#define OURNAV_OK OURNAV_PREFIX "ok"

bool CAllowAccessDlg::OnBeforeNavigate(CStringA& URL, CStringA& frame,
	void* post_data, int post_data_size,
	CStringA& headers) {
	if (URL.Left(strlen(OURNAV_PREFIX)) == OURNAV_PREFIX) {
		// use Left since browser may append / or other stuff
		if (URL.Left(strlen(OURNAV_CANCEL)) == OURNAV_CANCEL ||
			URL.Left(strlen(OURNAV_CLOSE)) == OURNAV_CLOSE)
			OnCancel();
		else if (URL.Left(strlen(OURNAV_OK)) == OURNAV_OK)
			OnOK();
		else
			return true;

		return false; // we handled it, don't let browser navigate
	} else
		return true; // let it navigate to whereever
}

void CAllowAccessDlg::OnOK() {
	GetParent()->EnableWindow();
	if (m_isModal) {
		CDialog::OnOK();
	} else {
		::PostMessage(m_callback, KEI_MSG_ID, MSG_ALLOWED, m_lastAllowedId);
		DestroyWindow();
		delete this;
	}
}

void CAllowAccessDlg::OnCancel() {
	GetParent()->EnableWindow();
	if (m_isModal) {
		CDialog::OnCancel();
	} else {
		::PostMessage(m_callback, KEI_MSG_ID, MSG_ABORT, 0);
		DestroyWindow();
		delete this;
	}
}

BEGIN_MESSAGE_MAP(CAllowAccessDlg, CDialog)
END_MESSAGE_MAP()

// CAllowAccessDlg message handlers

} // namespace KEP