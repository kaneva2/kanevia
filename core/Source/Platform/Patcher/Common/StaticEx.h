///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "WindowLayering.h"

namespace KEP {

//CStaticEx
class CStaticEx : public CStatic {
protected:
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	CStaticEx();
	CStaticEx(bool underline, bool italic, int size = 8);
	virtual void PreSubclassWindow();
	virtual ~CStaticEx();
	bool m_over;
	HCURSOR m_cursor;
	CFont m_underlineFont;
	static CBrush m_bkBrush;
	afx_msg void OnClicked();

	void SetURL(CStringA s) { m_link = s; }

	void setWindowText(const char* lpszString);
	void SetUnderline(bool underline) { m_underline = underline; }

	void SetColor(unsigned char red,
		unsigned char green,
		unsigned char blue) {
		rgb[0] = red;
		rgb[1] = green;
		rgb[2] = blue;
	}

	void SetBackColor(COLORREF rgb) { m_backcolor = rgb; }

	void SetItalic(bool italic) { m_italic = italic; }

	void SetBold(bool bold) { m_bold = bold; }

	void SetFontSize(int size) { m_size = size; }

	bool SetBackgroundTransparentColor(COLORREF rgb, bool trans);

	bool SetBackgroundAlpha(BYTE bAlpha);

	void LoadBitmap(UINT bitmapID);

	void LoadBitmap(HINSTANCE hInstance, UINT bitmapID);

protected:
	CStringA m_link;
	unsigned char rgb[3];
	COLORREF m_backcolor;
	bool m_bold;
	bool m_underline;
	bool m_italic;
	int m_size;
	bool m_transparent;
	CWindowLayering m_layering;
	CBitmap m_Bmp;
	CBitmap m_parentSectionBitmap;
};

} // namespace KEP