///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define LogInitLauncher() _LogInitLauncher(__FUNCTION__)
inline bool _LogInitLauncher(const std::string& func) {
	return _LogInit(func, PathAdd(PathApp(), K_LAUNCHER_LOG_CFG));
}
