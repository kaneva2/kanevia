///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include ".\windowlayering.h"

//  this variables should have been defined in some standard header but is not
#define WS_EX_LAYERED 0x00080000

//  ===========================================================================
//  Function pointer for layering API in User32.dll
//  ===========================================================================
typedef BOOL(WINAPI *lpfnSetLayeredWindowAttributes)(HWND hWnd, COLORREF cr, BYTE bAlpha, DWORD dwFlags);

lpfnSetLayeredWindowAttributes g_pSetLayeredWindowAttributes;

CWindowLayering::CWindowLayering(void) {
	//  get the function from the user32.dll
	HMODULE hUser32 = GetModuleHandle(_T("USER32.DLL"));
	g_pSetLayeredWindowAttributes = (lpfnSetLayeredWindowAttributes)
		GetProcAddress(hUser32, "SetLayeredWindowAttributes");
}

BOOL CWindowLayering::AddTransparencyMask(HWND hwnd, COLORREF rgb, BOOL bTrans) {
	if (g_pSetLayeredWindowAttributes == NULL)
		return FALSE;

	if (bTrans) {
		SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
		g_pSetLayeredWindowAttributes(hwnd, rgb, 0, LWA_COLORKEY);
	} else {
		SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) & ~WS_EX_LAYERED);
	}

	//Force an repaint on the current window and all of his children.
	::RedrawWindow(hwnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME | RDW_ALLCHILDREN);
	return TRUE;
}

BOOL CWindowLayering::AddAlpha(HWND hwnd, BYTE bAlpha) {
	if (g_pSetLayeredWindowAttributes == NULL)
		return FALSE;

	if (bAlpha < 255) {
		//  set layered style for the dialog
		SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) | WS_EX_LAYERED);

		//  call it with 255 as alpha - opacity
		g_pSetLayeredWindowAttributes(hwnd, 0, bAlpha, LWA_ALPHA);
	} else {
		SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) & ~WS_EX_LAYERED);
	}

	::RedrawWindow(hwnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME | RDW_ALLCHILDREN);
	return TRUE;
}
