///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "comdef.h"
#include "mshtml.h"
#include "mshtmcid.h"
#include "mshtmhst.h"
#include "exdispid.h"

#include "SimpleBrowser.h"
#include "Core/Util/Unicode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define Unused(parameter) parameter // avoid compile warnings about unused parameters

namespace KEP {

SimpleBrowser::SimpleBrowser() :
		_Browser(NULL),
		_BrowserDispatch(NULL) {
}

SimpleBrowser::~SimpleBrowser() {
	// release browser interfaces
	if (_Browser != NULL) {
		_Browser->Release();
		_Browser = NULL;
	}

	if (_BrowserDispatch != NULL) {
		_BrowserDispatch->Release();
		_BrowserDispatch = NULL;
	}
}

// Standard create
BOOL SimpleBrowser::Create(DWORD dwStyle,
	const RECT &rect,
	CWnd *pParentWnd,
	UINT nID,
	CStringA URL) {
	BOOL results = TRUE;

	_Browser = NULL;

	// create this window

	CWnd *window = this;
	results = window->Create(AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW),
		NULL,
		dwStyle,
		rect,
		pParentWnd,
		nID);

	if (!results)
		return FALSE;

	CRect browser_window_rect(0, 0, (rect.right - rect.left), (rect.bottom - rect.top));

	results = _BrowserWindow.CreateControl(CLSID_WebBrowser,
		NULL,
		(WS_VISIBLE | WS_TABSTOP),
		browser_window_rect,
		this,
		AFX_IDW_PANE_FIRST);
	if (!results) {
		DestroyWindow();
		return FALSE;
	}

	// get control interfaces

	LPUNKNOWN unknown = _BrowserWindow.GetControlUnknown();

	//	HRESULT hr=CoCreateInstance(CLSID_WebBrowser,unknown,CLSCTX_LOCAL_SERVER,
	//								IID_IWebBrowser2, (void**)&_Browser);

	HRESULT hr = unknown->QueryInterface(IID_IWebBrowser2, (void **)&_Browser);
	if (SUCCEEDED(hr)) {
		hr = unknown->QueryInterface(IID_IDispatch, (void **)&_BrowserDispatch);
	}

	if (!SUCCEEDED(hr)) {
		_BrowserWindow.DestroyWindow();
		DestroyWindow();
		return FALSE;
	}

	// navigate to blank page; wait for document creation

	if (_Browser != NULL) {
		Navigate(URL);

		IHTMLDocument2 *document = NULL;

		while ((document == NULL) && (hr == S_OK)) {
			Sleep(0);

			IDispatch *document_dispatch = NULL;

			hr = _Browser->get_Document(&document_dispatch);

			// if dispatch interface available, retrieve document interface

			if (SUCCEEDED(hr) && (document_dispatch != NULL)) {
				// retrieve document interface

				hr = document_dispatch->QueryInterface(IID_IHTMLDocument2, (void **)&document);

				document_dispatch->Release();
			}
		}

		if (document != NULL) {
			document->Release();
		}
	}

	return TRUE;
}

// Create in place of dialog control
BOOL SimpleBrowser::CreateFromControl(CWnd *pParentWnd, UINT nID) {
	BOOL result = FALSE;

	ASSERT(pParentWnd != NULL);

	CWnd *control = pParentWnd->GetDlgItem(nID);

	if (control != NULL) {
		// get control location

		CRect rect;

		control->GetWindowRect(&rect);
		pParentWnd->ScreenToClient(&rect);

		// destroy control, since the browser will take its place

		control->DestroyWindow();

		// create browser

		result = Create((WS_CHILD | WS_VISIBLE),
			rect,
			pParentWnd,
			nID);
	}

	return result;
}

// Destruction

void SimpleBrowser::PostNcDestroy() {
	// release browser interfaces
	if (_Browser != NULL) {
		_Browser->Release();
		_Browser = NULL;
	}

	if (_BrowserDispatch != NULL) {
		_BrowserDispatch->Release();
		_BrowserDispatch = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Controls
/////////////////////////////////////////////////////////////////////////////

// Navigate to URL

void SimpleBrowser::Navigate(const char *URL) {
	if (_Browser != NULL) {
		CStringA url(URL);

		_variant_t flags(0L, VT_I4);
		_variant_t target_frame_name("");
		_variant_t post_data("");
		_variant_t headers("");

		_Browser->Navigate(url.AllocSysString(),
			&flags,
			&target_frame_name,
			&post_data,
			&headers);
	}
}

// Navigate to HTML document in resource

void SimpleBrowser::NavigateResource(int resource_ID) {
	if (_Browser != NULL) {
		CStringA resource_string;

		// load HTML document from resource

		HRSRC resource_handle = FindResource(AfxGetResourceHandle(),
			MAKEINTRESOURCE(resource_ID),
			RT_HTML);

		if (resource_handle != NULL) {
			HGLOBAL resource = LoadResource(AfxGetResourceHandle(),
				resource_handle);

			if (resource != NULL) {
				LPVOID resource_memory = LockResource(resource);

				if (resource_memory != NULL) {
					DWORD resource_size = SizeofResource(AfxGetResourceHandle(),
						resource_handle);

					// identify the resource document as MBCS (e.g. ANSI)
					// or UNICODE

					bool UNICODE_document = false;

					wchar_t *UNICODE_memory = (wchar_t *)resource_memory;
					int UNICODE_size = resource_size / sizeof(wchar_t);

					if (UNICODE_size >= 1) {
						// check for UNICODE byte order mark

						if (*UNICODE_memory == L'\xFEFF') {
							UNICODE_document = true;
							UNICODE_memory += 1;
							UNICODE_size -= 1;
						}

						// otherwise, check for UNICODE leading tag

						else if (UNICODE_size >= 5) {
							if ((UNICODE_memory[0] == L'<') &&
								(towupper(UNICODE_memory[1]) == L'H') &&
								(towupper(UNICODE_memory[2]) == L'T') &&
								(towupper(UNICODE_memory[3]) == L'M') &&
								(towupper(UNICODE_memory[4]) == L'L')) {
								UNICODE_document = true;
							}
						}

						// Note: This logic assumes that the UNICODE resource document is
						//       in little-endian byte order, which would be typical for
						//       any HTML document used as a resource in a Windows application.
					}

					// convert resource document if required

#if !defined(UNICODE)

					if (UNICODE_document) {
						char *MBCS_buffer = resource_string.GetBufferSetLength(resource_size + 1);

						int MBCS_length = ::WideCharToMultiByte(CP_ACP,
							0,
							UNICODE_memory,
							UNICODE_size,
							MBCS_buffer,
							resource_size + 1,
							NULL,
							NULL);

						resource_string.ReleaseBuffer(MBCS_length);

					}

					else {
						resource_string = CStringA((char *)resource_memory, resource_size);
					}

#else

					if (UNICODE_document) {
						resource_string = Utf16ToUtf8(std::wstring(UNICODE_memory, UNICODE_size)).c_str();
					} else {
						resource_string = CStringA((char *)resource_memory, resource_size);
					}

#endif
				}
			}
		}

		Clear();
		Write(resource_string);
	}
}

// Append string to current document

void SimpleBrowser::Write(const char *string) {
	if (_Browser != NULL) {
		// get document interface

		IHTMLDocument2 *document = GetDocument();

		if (document != NULL) {
			// construct text to be written to browser as SAFEARRAY

			SAFEARRAY *safe_array = SafeArrayCreateVector(VT_VARIANT, 0, 1);

			VARIANT *variant;

			SafeArrayAccessData(safe_array, (LPVOID *)&variant);

			variant->vt = VT_BSTR;
			variant->bstrVal = CStringW(Utf8ToUtf16(string).c_str()).AllocSysString();

			SafeArrayUnaccessData(safe_array);

			// write SAFEARRAY to browser document

			document->write(safe_array);

			document->Release();
			document = NULL;
		}
	}
}

void SimpleBrowser::Clear() {
	if (_Browser != NULL) {
		// if document interface available, close/re-open document to clear display

		IHTMLDocument2 *document = GetDocument();

		if (document != NULL) {
			// close and re-open document to empty contents

			document->close();

			VARIANT open_name;
			VARIANT open_features;
			VARIANT open_replace;
			IDispatch *open_window = NULL;

			::VariantInit(&open_name);

			open_name.vt = VT_BSTR;
			open_name.bstrVal = ::SysAllocString(L"_self");

			::VariantInit(&open_features);
			::VariantInit(&open_replace);

			HRESULT hr = document->open(
				::SysAllocString(L"text/html"),
				open_name,
				open_features,
				open_replace,
				&open_window);

			if (hr == S_OK) {
				Refresh();
			}

			if (open_window != NULL) {
				open_window->Release();
			}

		}

		// otherwise, navigate to about:blank and wait for document ready

		else {
			Navigate("about:blank");

			HRESULT hr = S_OK;

			while ((document == NULL) && (hr == S_OK)) {
				Sleep(0);

				IDispatch *document_dispatch = NULL;

				hr = _Browser->get_Document(&document_dispatch);

				// if dispatch interface available, retrieve document interface

				if (SUCCEEDED(hr) && (document_dispatch != NULL)) {
					// retrieve document interface

					hr = document_dispatch->QueryInterface(IID_IHTMLDocument2, (void **)&document);

					document_dispatch->Release();
				}
			}

			if (document != NULL) {
				document->Release();
			}
		}
	}
}

// Navigation operations

void SimpleBrowser::GoBack() {
	if (_Browser != NULL) {
		_Browser->GoBack();
	}
}

void SimpleBrowser::GoForward() {
	if (_Browser != NULL) {
		_Browser->GoForward();
	}
}

void SimpleBrowser::GoHome() {
	if (_Browser != NULL) {
		_Browser->GoHome();
	}
}

void SimpleBrowser::Refresh() {
	if (_Browser != NULL) {
		_Browser->Refresh();
	}
}

void SimpleBrowser::Stop() {
	if (_Browser != NULL) {
		_Browser->Stop();
	}
}

// Print contents

void SimpleBrowser::Print(const char *header, const char *footer) {
	if (_Browser != NULL) {
		// construct two element SAFEARRAY;
		// first element is header string, second element is footer string

		HRESULT hr;

		VARIANT header_variant;
		VariantInit(&header_variant);
		V_VT(&header_variant) = VT_BSTR;
		V_BSTR(&header_variant) = CStringW(Utf8ToUtf16(header).c_str()).AllocSysString();

		VARIANT footer_variant;
		VariantInit(&footer_variant);
		V_VT(&footer_variant) = VT_BSTR;
		V_BSTR(&footer_variant) = CStringW(Utf8ToUtf16(footer).c_str()).AllocSysString();

		long index;

		SAFEARRAYBOUND parameter_array_bound[1];
		SAFEARRAY *parameter_array = NULL;

		parameter_array_bound[0].cElements = 2;
		parameter_array_bound[0].lLbound = 0;

		parameter_array = SafeArrayCreate(VT_VARIANT, 1, parameter_array_bound);

		index = 0;
		hr = SafeArrayPutElement(parameter_array, &index, &header_variant);

		index = 1;
		hr = SafeArrayPutElement(parameter_array, &index, &footer_variant);

		VARIANT parameter;
		VariantInit(&parameter);
		V_VT(&parameter) = VT_ARRAY | VT_BYREF;
		V_ARRAY(&parameter) = parameter_array;

		// start printing browser contents

		hr = _Browser->ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DODEFAULT, &parameter, NULL);

		// Note: There is no simple way to determine that printing has completed.
		//       In fact, if the browser is destroyed while printing is in progress,
		//       only part of the contents will be printed.

		// release SAFEARRAY

		if (!SUCCEEDED(hr)) {
			VariantClear(&header_variant);
			VariantClear(&footer_variant);
			if (parameter_array != NULL) {
				SafeArrayDestroy(parameter_array);
			}
		}
	}
}

// Miscellaneous

bool SimpleBrowser::GetBusy() {
	bool busy = false;

	if (_Browser != NULL) {
		VARIANT_BOOL variant_bool;

		HRESULT hr = _Browser->get_Busy(&variant_bool);
		if (SUCCEEDED(hr)) {
			busy = (variant_bool == VARIANT_TRUE);
		}
	}

	return busy;
}

CStringA SimpleBrowser::GetLocationName() {
	CStringA location_name = "";

	if (_Browser != NULL) {
		BSTR location_name_BSTR = NULL;

		HRESULT hr = _Browser->get_LocationName(&location_name_BSTR);
		if (SUCCEEDED(hr)) {
			location_name = Utf16ToUtf8(location_name_BSTR).c_str();
		}

		::SysFreeString(location_name_BSTR);
	}

	return location_name;
}

CStringA SimpleBrowser::GetLocationURL() {
	CStringA location_URL = "";

	if (_Browser != NULL) {
		BSTR location_URL_BSTR = NULL;

		HRESULT hr = _Browser->get_LocationURL(&location_URL_BSTR);
		if (SUCCEEDED(hr)) {
			location_URL = Utf16ToUtf8(location_URL_BSTR).c_str();
		}

		::SysFreeString(location_URL_BSTR);
	}

	return location_URL;
}

READYSTATE SimpleBrowser::GetReadyState() {
	READYSTATE readystate = READYSTATE_UNINITIALIZED;

	if (_Browser != NULL) {
		_Browser->get_ReadyState(&readystate);
	}

	return readystate;
}

bool SimpleBrowser::GetSilent() {
	bool silent = false;

	if (_Browser != NULL) {
		VARIANT_BOOL silent_variant;

		HRESULT hr = _Browser->get_Silent(&silent_variant);
		if (SUCCEEDED(hr)) {
			silent = (silent_variant == VARIANT_TRUE);
		}
	}

	return silent;
}

void SimpleBrowser::PutSilent(bool silent) {
	if (_Browser != NULL) {
		VARIANT_BOOL silent_variant;

		if (silent)
			silent_variant = VARIANT_TRUE;
		else
			silent_variant = VARIANT_FALSE;

		_Browser->put_Silent(silent_variant);
	}
}

IHTMLDocument2 *SimpleBrowser::GetDocument() {
	IHTMLDocument2 *document = NULL;

	if (_Browser != NULL) {
		// get browser document's dispatch interface

		IDispatch *document_dispatch = NULL;

		HRESULT hr = _Browser->get_Document(&document_dispatch);

		if (SUCCEEDED(hr) && (document_dispatch != NULL)) {
			// get the actual document interface
			hr = document_dispatch->QueryInterface(IID_IHTMLDocument2, (void **)&document);

			// release dispatch interface
			document_dispatch->Release();
		}
	}

	return document;
}

/////////////////////////////////////////////////////////////////////////////
// Message handlers
/////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(SimpleBrowser, CWnd)
//{{AFX_MSG_MAP(SimpleBrowser)
ON_WM_SIZE()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// Resize control window as this window is resized

void SimpleBrowser::OnSize(UINT nType, int cx, int cy) {
	CWnd::OnSize(nType, cx, cy);

	if (_Browser != NULL) {
		CRect rect(0, 0, cx, cy);
		_BrowserWindow.MoveWindow(&rect);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Event handlers
/////////////////////////////////////////////////////////////////////////////

BEGIN_EVENTSINK_MAP(SimpleBrowser, CWnd)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_BEFORENAVIGATE2,
	_OnBeforeNavigate2,
	VTS_DISPATCH VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PBOOL)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_DOCUMENTCOMPLETE,
	_OnDocumentComplete,
	VTS_DISPATCH VTS_PVARIANT)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_DOWNLOADBEGIN,
	_OnDownloadBegin,
	VTS_NONE)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_PROGRESSCHANGE,
	_OnProgressChange,
	VTS_I4 VTS_I4)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_DOWNLOADCOMPLETE,
	_OnDownloadComplete,
	VTS_NONE)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_NAVIGATECOMPLETE2,
	_OnNavigateComplete2,
	VTS_DISPATCH VTS_PVARIANT)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_STATUSTEXTCHANGE,
	_OnStatusTextChange,
	VTS_BSTR)
ON_EVENT(SimpleBrowser, AFX_IDW_PANE_FIRST,
	DISPID_TITLECHANGE,
	_OnTitleChange,
	VTS_BSTR)
END_EVENTSINK_MAP()

SimpleBrowser::Notification::Notification(HWND hwnd, UINT ID, NotificationType type) {
	hdr.hwndFrom = hwnd;
	hdr.idFrom = ID;
	hdr.code = type;
	URL = "";
	frame = "";
	post_data = NULL;
	post_data_size = 0;
	headers = "";
	progress = 0;
	progress_max = 0;
	text = "";
}

// Called before navigation begins; application may cancel if required

void SimpleBrowser::_OnBeforeNavigate2(LPDISPATCH lpDisp,
	VARIANT FAR *URL,
	VARIANT FAR *Flags,
	VARIANT FAR *TargetFrameName,
	VARIANT FAR *PostData,
	VARIANT FAR *Headers,
	VARIANT_BOOL *Cancel) {
	Unused(Flags); // Note: flags value is reserved

	if (lpDisp == _BrowserDispatch) {
		CStringW URL_string;
		CStringW frame;
		unsigned char *post_data = NULL;
		int post_data_size = 0;
		CStringW headers;

		if ((URL != NULL) &&
			(V_VT(URL) == VT_BSTR)) {
			URL_string = V_BSTR(URL);
		}

		if ((TargetFrameName != NULL) &&
			(V_VT(TargetFrameName) == VT_BSTR)) {
			frame = V_BSTR(TargetFrameName);
		}

		if ((PostData != NULL) &&
			(V_VT(PostData) == (VT_VARIANT | VT_BYREF))) {
			VARIANT *PostData_variant = V_VARIANTREF(PostData);

			if ((PostData_variant != NULL) &&
				(V_VT(PostData_variant) != VT_EMPTY)) {
				SAFEARRAY *PostData_safearray = V_ARRAY(PostData_variant);

				if (PostData_safearray != NULL) {
					char *post_data_array = NULL;

					SafeArrayAccessData(PostData_safearray, (void HUGEP **)&post_data_array);

					long lower_bound = 1;
					long upper_bound = 1;

					SafeArrayGetLBound(PostData_safearray, 1, &lower_bound);
					SafeArrayGetUBound(PostData_safearray, 1, &upper_bound);

					post_data_size = (int)(upper_bound - lower_bound + 1);

					post_data = new unsigned char[post_data_size];

					memcpy(post_data, post_data_array, post_data_size);

					SafeArrayUnaccessData(PostData_safearray);
				}
			}
		}

		bool cancel = OnBeforeNavigate2(Utf16ToUtf8(URL_string).c_str(),
			Utf16ToUtf8(frame).c_str(),
			post_data, post_data_size,
			Utf16ToUtf8(headers).c_str());

		if (Cancel != NULL) {
			if (cancel)
				*Cancel = VARIANT_TRUE;
			else
				*Cancel = VARIANT_FALSE;
		}

		delete[] post_data;
	}
}

bool SimpleBrowser::OnBeforeNavigate2(CStringA URL,
	CStringA frame,
	void *post_data, int post_data_size,
	CStringA headers) {
	bool cancel = false;

	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), BeforeNavigate2);

		notification.URL = URL;
		notification.frame = frame;
		notification.post_data = post_data;
		notification.post_data_size = post_data_size;
		notification.headers = headers;

		LRESULT result = parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);

		if (result) {
			cancel = true;
		}
	}

	return cancel;
}

// Document loaded and initialized

void SimpleBrowser::_OnDocumentComplete(LPDISPATCH lpDisp, VARIANT *URL) {
	if (lpDisp == _BrowserDispatch) {
		CStringW URL_string;

		if ((URL != NULL) &&
			(V_VT(URL) == VT_BSTR)) {
			URL_string = V_BSTR(URL);
		}

		OnDocumentComplete(Utf16ToUtf8(URL_string).c_str());
	}
}

void SimpleBrowser::OnDocumentComplete(CStringA URL) {
	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), DocumentComplete);

		notification.URL = URL;

		/*LRESULT result = */ parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);
	}
}

// Navigation/download progress

void SimpleBrowser::_OnDownloadBegin() {
	OnDownloadBegin();
}

void SimpleBrowser::OnDownloadBegin() {
	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), DownloadBegin);

		/*LRESULT result = */ parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);
	}
}

void SimpleBrowser::_OnProgressChange(long progress, long progress_max) {
	OnProgressChange((int)progress, (int)progress_max);
}

void SimpleBrowser::OnProgressChange(int progress, int progress_max) {
	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), ProgressChange);

		notification.progress = progress;
		notification.progress_max = progress_max;

		/*LRESULT result = */ parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);
	}
}

void SimpleBrowser::_OnDownloadComplete() {
	OnDownloadComplete();
}

void SimpleBrowser::OnDownloadComplete() {
	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), DownloadComplete);

		/*LRESULT result = */ parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);
	}
}

// Navigation to a link has completed

void SimpleBrowser::_OnNavigateComplete2(LPDISPATCH lpDisp, VARIANT *URL) {
	if (lpDisp == _BrowserDispatch) {
		CStringW URL_string;

		if ((URL != NULL) &&
			(V_VT(URL) == VT_BSTR)) {
			URL_string = V_BSTR(URL);
		}

		OnNavigateComplete2(Utf16ToUtf8(URL_string).c_str());
	}
}

void SimpleBrowser::OnNavigateComplete2(CStringA URL) {
	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), NavigateComplete2);

		notification.URL = URL;

		/*LRESULT result = */ parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);
	}
}

// Status text has changed

void SimpleBrowser::_OnStatusTextChange(BSTR bstrText) {
	CStringA text;

	if (bstrText != NULL) {
		text = (const char *)bstrText;
	}

	OnStatusTextChange(text);
}

void SimpleBrowser::OnStatusTextChange(CStringA text) {
	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), StatusTextChange);

		notification.text = text;

		/*LRESULT result = */ parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);
	}
}

// Title text has changed

void SimpleBrowser::_OnTitleChange(BSTR bstrText) {
	CStringA text;

	if (bstrText != NULL) {
		text = (const char *)bstrText;
	}

	OnTitleChange(text);
}

void SimpleBrowser::OnTitleChange(CStringA text) {
	CWnd *parent = GetParent();

	if (parent != NULL) {
		Notification notification(m_hWnd, GetDlgCtrlID(), TitleChange);

		notification.text = text;

		/*LRESULT result = */ parent->SendMessage(WM_NOTIFY,
			notification.hdr.idFrom,
			(LPARAM)&notification);
	}
}

} // namespace KEP