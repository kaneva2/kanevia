///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

class CWindowLayering {
public:
	CWindowLayering();
	BOOL AddTransparencyMask(HWND hwnd, COLORREF rgb, BOOL bTrans);
	BOOL AddAlpha(HWND hwnd, BYTE bAlpha);
};
