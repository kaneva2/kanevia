///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <afxcmn.h>
#include "WindowLayering.h"
#include "ImageLib\ImageLibTypes.h"

namespace KEP {

enum LayOutStyle {
	LO_DEFAULT,
	LO_TILE, // Tile the background picture
	LO_CENTER, // Center the background picture
	LO_STRETCH, // Stretch the background picture to the dialog window size
	LO_RESIZE // Resize the dialog so that it just fits the background
};

class ResourceContext {
private:
	HINSTANCE m_oldHinstance;
	ResourceContext(const ResourceContext& rhs);
	ResourceContext& operator=(const ResourceContext& rhs);

public:
	ResourceContext(HINSTANCE newContext) {
		m_oldHinstance = AfxGetResourceHandle();
		AfxSetResourceHandle(newContext);
	}
	~ResourceContext() {
		AfxSetResourceHandle(m_oldHinstance);
	}
};

class CKanevaDialogSkin : public CDialog {
public:
	CKanevaDialogSkin(CWnd* pParent = NULL);
	CKanevaDialogSkin(UINT uResourceID, CWnd* pParent = NULL);
	CKanevaDialogSkin(const wchar_t* pszResourceID, CWnd* pParent = NULL);
	virtual ~CKanevaDialogSkin();

	DWORD SetImage(const char* filename);

	DWORD SetImage(ImagePointer image);

	DWORD SetImage(int imageID);

	DWORD SetBitmap(HBITMAP hBitmap);

	DWORD SetBitmap(int nBitmap);

	DWORD SetBitmap(const char* lpszFileName);

	void SetStyle(LayOutStyle style);

	void EnableEasyMove(BOOL pEnable = TRUE);

	virtual BOOL SetTransparent(BYTE bAlpha);

	virtual BOOL SetTransparentColor(COLORREF col, BOOL bTrans = TRUE);

	BOOL IsTransparent() const {
		return m_transparent;
	}

	BOOL HasBitmap() const {
		return NotNullImage(m_background != nullptr);
	}

	static BOOL LoadSkinString(CStringA&, UINT id);

	static HBITMAP LoadBitmap(UINT nBitmap);

	static ImagePointer LoadSkinImage(UINT nID);

	// Sets an alternate instance which will be searched
	// for resources.
	static void SetResourceInstance(HINSTANCE instance) {
		s_resourceInstance = instance;
	}

	// Sets an alternate instance which will be searched
	// for resources.
	static HINSTANCE GetResourceInstance() {
		return s_resourceInstance;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL OnInitDialog();

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CKanevaDialogSkin)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CWindowLayering m_layering;

private:
	BOOL m_bEasyMove;
	void FreeResources();
	void Init();
	ImagePointer m_background;
	DWORD m_dwWidth; // Width of bitmap
	DWORD m_dwHeight; // Height of bitmap
	LayOutStyle m_loStyle; // LayOutStyle style
	BOOL m_transparent; // Is transparency applied?
	static HINSTANCE s_resourceInstance; // Handle to resources, supplied by caller.
};

} // namespace KEP