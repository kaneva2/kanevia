!include "MUI2.nsh"
!include "UAC.nsh"
!include "LogicLib.nsh"

!define MUI_ICON "kanevaIcon3D.ico"
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_LANGUAGE "English"


Name "Kaneva"
OutFile "KanevaInstaller.exe"
BrandingText " "
InstallDir "$PROGRAMFILES\Kaneva"
ShowInstDetails nevershow
AddBrandingImage top 145
RequestExecutionLevel admin

!define Explode "!insertmacro Explode"

!macro  Explode Length  Separator   String
    Push    `${Separator}`
    Push    `${String}`
    Call    Explode
    Pop     `${Length}`
!macroend
 
Function Explode
  ;  Initialize variables
  Var /GLOBAL explString
  Var /GLOBAL explSeparator
  Var /GLOBAL explStrLen
  Var /GLOBAL explSepLen
  Var /GLOBAL explOffset
  Var /GLOBAL explTmp
  Var /GLOBAL explTmp2
  Var /GLOBAL explTmp3
  Var /GLOBAL explArrCount
 
  ; Get input from user
  Pop $explString
  Pop $explSeparator
 
  ; Calculates initial values
  StrLen $explStrLen $explString
  StrLen $explSepLen $explSeparator
  StrCpy $explArrCount 1
 
  ${If}   $explStrLen <= 1          ;   If we got a single character
  ${OrIf} $explSepLen > $explStrLen ;   or separator is larger than the string,
    Push    $explString             ;   then we return initial string with no change
    Push    1                       ;   and set array's length to 1
    Return
  ${EndIf}
 
  ; Set offset to the last symbol of the string
  StrCpy $explOffset $explStrLen
  IntOp  $explOffset $explOffset - 1
 
  ; Clear temp string to exclude the possibility of appearance of occasional data
  StrCpy $explTmp   ""
  StrCpy $explTmp2  ""
  StrCpy $explTmp3  ""
 
  ; Loop until the offset becomes negative
  ${Do}
    ;   If offset becomes negative, it is time to leave the function
    ${IfThen} $explOffset == -1 ${|} ${ExitDo} ${|}
 
    ;   Remove everything before and after the searched part ("TempStr")
    StrCpy $explTmp $explString $explSepLen $explOffset
 
    ${If} $explTmp == $explSeparator
        ;   Calculating offset to start copy from
        IntOp   $explTmp2 $explOffset + $explSepLen ;   Offset equals to the current offset plus length of separator
        StrCpy  $explTmp3 $explString "" $explTmp2
 
        Push    $explTmp3                           ;   Throwing array item to the stack
        IntOp   $explArrCount $explArrCount + 1     ;   Increasing array's counter
 
        StrCpy  $explString $explString $explOffset 0   ;   Cutting all characters beginning with the separator entry
        StrLen  $explStrLen $explString
    ${EndIf}
 
    ${If} $explOffset = 0                       ;   If the beginning of the line met and there is no separator,
                                                ;   copying the rest of the string
        ${If} $explSeparator == ""              ;   Fix for the empty separator
            IntOp   $explArrCount   $explArrCount - 1
        ${Else}
            Push    $explString
        ${EndIf}
    ${EndIf}
 
    IntOp   $explOffset $explOffset - 1
  ${Loop}
 
  Push $explArrCount
FunctionEnd


!define downloadSetup "!insertmacro downloadSetup"
 
!macro downloadSetup url
  Push `${url}`
  Call downloadSetup
!macroend


Function downloadSetup
  Var /GLOBAL downloadURL
  pop $downloadURL
  ; MessageBox MB_OK $downloadURL
  DetailPrint "Getting Kaneva Setup Files..."
  
  StrCpy $r1 0
  ${Do}
    IntOp $r1 $R1 + 1
    inetc::get /NOCANCEL "$downloadURL/KanevaSetup.exe" "$TEMP\KanevaSetup.exe"
    Pop $0
    ${If} $r1 >= 5
      MessageBox MB_RETRYCANCEL "Your internet service maybe down.$\r$\nError : $0 " IDRETRY true IDCANCEL false
      true:
         goto next
      false:
         StrCpy $0 'OK'
      next:
    ${EndIf}
  ${LoopUntil} $0 == 'OK'

FunctionEnd

!define getSetupURL "!insertmacro getSetupURL"
 
!macro getSetupURL setupURL
  Call getSetupURL
  Pop     `${setupURL}`
!macroend

Function getSetupURL
  Var /GLOBAL result
  Var /GLOBAL firstLine
  Var /GLOBAL position
  Var /GLOBAL value
  Var /GLOBAL length
  var /GLOBAL url

  ; Set default KanevaSetup.exe path
  strcpy $url "http://cdn3.kaneva.net/patchdata/install/detection"

  inetc::get /NOCANCEL /SILENT "http://wok.kaneva.com/kgp/serverlist.aspx?v=2&ver=10000&id=3296" "$TEMP\serverlist.txt"
  Pop $result


  ${If} $result != "OK"
	strcpy $url "http://cdn3.kaneva.net/patchdata/install/detection"
  ${Else}
  FileOpen $result "$TEMP\serverlist.txt" r
  FileRead $result $firstLine ; we re ad until the end of line (including carriage return and new line) and save it to $1
  ;MessageBox MB_OK $firstLine
  FileClose $result ; and close the file
  strcpy $position 0
   ${Explode}  $length ";" $firstLine
    ;MessageBox  MB_OK "$length elements in array"
    ${For} $position 1 $length
        Pop $value
       ; MessageBox MB_OK "Element #$position: $value"
       ; The  7 the value has the URL store it in the $url variable and push on end of function
        ${If} $position == 7
	   strcpy $url $value
        ${EndIf}
    ${Next}
  ${Endif}

  push $url
FunctionEnd
 
Section
  SetAutoClose true
  SetBrandingImage "$INSTDIR\kaneva_banner.bmp"
  !insertmacro UAC_RunElevated

  call cleanOldSetup
  ${getSetupURL} $url
  

  ${downloadSetup} $url

  call startSetup

SectionEnd

Function cleanOldSetup
  ;Remove KanevaSetup.exe if it already exists
  
  delete "$TEMP\KanevaSetup.exe" 
  delete "$TEMP\serverlist.txt"
FunctionEnd


Function startSetup
   IfFileExists $TEMP\KanevaSetup.exe yeslbl nolbl
   nolbl:
      MessageBox MB_OK "Please run KanevaSetup.exe at the website that will now open.$\r$\nUnable to find setup file."
      ExecShell "open" "http://www.kaneva.com/community/Install3dLB.aspx"
    goto quit
    yeslbl:
       ExecShell "" "$TEMP\KanevaSetup.exe"
    quit:
FunctionEnd