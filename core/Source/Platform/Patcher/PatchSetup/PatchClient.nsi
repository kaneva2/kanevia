;  this is the middle level folder name (e.g. "Star")
!IFNDEF STAR_NAME
  !error "STAR_NAME is not defined. Must be passed as /DSTAR_NAME=Star"
!endif

; this is the game name, used for display purposes (e.g. WOK, CardboardWorld, etc.)
!IFNDEF PRODUCT_NAME
  !error "PRODUCT_NAME is not defined. Must be passed as /DPRODUCT_NAME=WOK"
!endif

; this is the game id, used for registry and folder names
!IFNDEF GAME_ID
  !error "GAME_ID is not defined. Must be passed as /DGAME_ID=3296"
!endif

; URL to use on shortcut (e.g. kaneva://WOK)
!define URL_PREFIX "kaneva"
!IFNDEF SHORTCUT_URL
  !error "SHORTCUT_URL is not defined. Must be passed as /DSHORTCUT_URL=${URL_PREFIX}://3296"
!endif


!define PRODUCT_VERSION "4.0"
!define PRODUCT_VCREDIST_VERSION "14.0.23506.0"
!define PRODUCT_VCREDIST_URL "http://cdn3.kaneva.net"
!define PRODUCT_VCREDIST_PATH "/patchdata/vcredist/${PRODUCT_VCREDIST_VERSION}"
!define PRODUCT_PUBLISHER "Kaneva, Inc."
!define PRODUCT_WEB_SITE "http://www.kaneva.com"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${GAME_ID}"
!define PRODUCT_DIR_REGKEY "SOFTWARE\Kaneva\"
!define metricReportURL "http://pv-wok.kaneva.com/kgp/scriptGameItemMetrics.aspx?reportName=KanevaSetup&reportParams="
; !define PRODUCT_VISTA_REGKEY "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\"
!define PREAPPROVED_ACTIVEX_SLIPPATCHER_REGKEY "Software\Microsoft\Windows\CurrentVersion\Ext\PreApproved\{8F90609B-FF0A-45C4-BD5A-430CA68DBDDA}"

; DRF - NOTE - Uuid Must Match All Of The Following:
; - PatchClientCtrl:: PatchClientCtrl.idl
; - ClientEngine::patchClientCtrlUuid
; - PatchSetup::PatchClient.nsi (this)
!define PREAPPROVED_ACTIVEX_PATCHCLIENTCTRL_REGKEY "Software\Microsoft\Windows\CurrentVersion\Ext\PreApproved\{8E041C5C-4CA8-4334-BB3D-E95354F05B8A}"
;!define PREAPPROVED_ACTIVEX_PATCHCLIENTCTRL_REGKEY "Software\Microsoft\Windows\CurrentVersion\Ext\PreApproved\{E6BE1576-97E3-4DEB-BEC3-8CF1C640FB1C}"

!define PRODUCT_DIR_REGKEY_PARENT "SOFTWARE\Kaneva"
!define MOZILLA_DIR_REGKEY_PARENT "SOFTWARE\Mozilla"
!define PRODUCT_DIR_REGKEY_VIRTUALSTORE "Software\Classes\VirtualStore\MACHINE\SOFTWARE\Kaneva"
!define PRODUCT_DIR_REGKEY_VIRTUALSTORE64 "Software\Classes\VirtualStore\MACHINE\SOFTWARE\Wow6432Node\Kaneva"

!define PRODUCT_ROOT_KEY "HKLM"
!define PRODUCT_ROOT_KEY2 "HKCU"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

; MUI 1.67 compatible ------
!include "MUI.nsh"
!include 'LogicLib.nsh'
!include 'WordFunc.nsh'
!include "nsDialogs.nsh"
!include "UAC.nsh"
!include "x64.nsh"
!include "WinVer.nsh"
!include FileFunc.nsh


!insertmacro GetParameters
!insertmacro GetOptions

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "kanevaIcon3D.ico"
;!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
;!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

!define MUI_CUSTOMFUNCTION_GUIINIT myGuiInit

; Welcome page

!insertmacro MUI_PAGE_INSTFILES

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; MUI end ------

; Utils
!include "..\..\..\..\deploy\buildprocess\GetWindowsVer.nsh"
!include "..\..\..\..\Deploy\BuildProcess\isUserAdmin.nsh"

XPStyle on

Name "${PRODUCT_NAME} v${PRODUCT_VERSION}"
OutFile "WorldofKanevaSetup.exe"
InstallDir "$PROGRAMFILES\Kaneva"
RequestExecutionLevel user
ShowInstDetails nevershow
ShowUnInstDetails nevershow
AddBrandingImage top 145
AutoCloseWindow true
; Restore following two lines on check-in
ChangeUI IDD_INST      "..\Release\PatchSetupUI.exe"
ChangeUI IDD_INSTFILES "..\Release\PatchSetupUI.exe"
;ChangeUI IDD_INSTFILES  "..\Release\LoadingBar.exe"

LicenseText "" "Accept"
Var no_directx
Var no_directx_9c
Var chks_fail
Var error_message
Var download_message
Var download_list
Var win_ver
Var mozilla_reg_key
Var mozilla_reg_key_index
Var mozilla_plugin_path
Var game_reg_key
Var game_reg_key_index
Var other_game_path
Var os_ver
Var outputFolder
Var bWOKAlreadyInstalled
Var targetEnvironment
Var previousVersionDetected
Var updateVisitStats
Var virtualStorePath
Var installMode
Var unsentCrashReportsPath

;---------------------------------------------
; initialization checks
;---------------------------------------------
  Function GetDXVersion 
    Push $0
    Push $1
    DetailPrint "Checking for DirectX..."
    ReadRegStr $0 HKLM "Software\Microsoft\DirectX" "Version"
    IfErrors noDirectX
 
    StrCpy $1 $0 2 5    ; get the minor version
    StrCpy $0 $0 2 2    ; get the major version
    IntOp $0 $0 * 100   ; $0 = major * 100 + minor
    IntOp $0 $0 + $1
    Goto done
 
    noDirectX:
      DetailPrint "DirectX not found..."
      StrCpy $0 0
    
    done:
      Pop $1
      Exch $0
  FunctionEnd

;--------------------------------------------------
;Check for VS2005 x_86 redistribution file.
;--------------------------------------------------
Function VS2005_X86_Redistribution
   ;If VCRedist is already running wait for it to complete
   FindProcDLL::FindProc "Vcredist_x86.exe"
     StrCmp $R0 1 alreadyRunning checkForRedist
   alreadyRunning:
     MessageBox MB_OK "You may have another install aleady running please complete it and click OK"
     Processes::KillProcess "Vcredist_x86.exe"
     sleep 3000
     FindProcDLL::FindProc "Vcredist_x86.exe"
     StrCmp $R0 1 alreadyRunning checkForRedist
     
     
   checkForRedist:
   DetailPrint "Checking for VCRedist..."
   IfFileExists $INSTDIR\Vcredist_x86.exe installRedist downloadRedist
   downloadRedist:
      DetailPrint "Starting VCRedist Download..."
      call downloadVCREDIST 
   goto installRedist
    
  installRedist:
  
  DetailPrint "Installing VCRedist"
  IfSilent vcredistdone 0
  
  StrCpy $r1 0
  Exec '"$INSTDIR\Vcredist_x86.exe" /q /norestart '
  DetailPrint "Installing VCRedist"
  Sleep 2000
  goto processCheck
     
  processCheck:    
  ${Do}
    DetailPrint "Installing VCRedist ."
    Sleep 100
    DetailPrint "Installing VCRedist .."
    Sleep 100
    DetailPrint "Installing VCRedist ..."
    Sleep 100 
    DetailPrint "Installing VCRedist ....."
    Sleep 100
    DetailPrint "Installing VCRedist ........"
    Sleep 100
    DetailPrint "Installing VCRedist ..........."
    Sleep 100
    DetailPrint "Installing VCRedist ..............."
    Sleep 100
    DetailPrint "Installing VCRedist ...................."
    Sleep 100
    DetailPrint "Installing VCRedist ..........................."
    Sleep 100
    DetailPrint "Installing VCRedist ..................................."
    Sleep 100
    FindProcDLL::FindProc "Vcredist_x86.exe"
  ${LoopUntil} $R0 == 0
  
  sleep 100
  IntOp $r1 $R1 + 1
  
  StrCmp $r1 "10" tooManyAttempts

  ExecWait 'regsvr32 /s "$INSTDIR\PatchClientCtrl.dll"'
  IfErrors processCheck
  
  
  
  ; Workaround for installing vcredist 2015 on Windows 7 RTM
  ; If vcredist failed with error code 23 (ERROR_CRC), very likely that the computer is running Win 7 RTM and does not
  ; support vcredist 2015. There is a bug with vcredist installer that will leave CRT partially installed. We should
  ; remove this partial installation now so that it can be correctly installed later once SP1 is applied.
  IntCmp $1 23 uninst_vcredist vcredistdone vcredistdone
uninst_vcredist:
  DetailPrint "Uninstalling VCRedist..."
  ExecWait '"$INSTDIR\Vcredist_x86.exe" /uninstall /q'

  IfFileExists "$WINDIR\system32\wuapp.exe" run_windows_update service_pack_info

  ; If wuapp.exe exists, run it to bring up <Windows Update>
run_windows_update:
  MessageBox MB_OK|MB_ICONINFORMATION "Your system must be on the latest service pack for Kaneva to run. Please check for and install Windows updates then try Kaneva Setup again. Windows updates will now open."
  Exec '"$WINDIR\system32\wuapp.exe" startmenu'
  Quit

  ; Otherwise redirect user to a Microsoft webpage
service_pack_info:
  MessageBox MB_OKCANCEL|MB_ICONINFORMATION "Your system must be on the latest service pack for Kaneva to run. Please check for and install Windows updates then try Kaneva Setup again.$\r$\n$\r$\nTo learn about Windows service packs, click <OK>.$\r$\nTo abort installation, click <Cancel>." IDOK 0 IDCANCEL +2
  ExecShell "open" "http://windows.microsoft.com/en-US/windows/service-packs-download"
  Quit

tooManyAttempts:
vcredistdone:

FunctionEnd


; ### google analytics start
; The static items of the GA URL

!define GA_UNIQUE_ID_PREVIEW "http://preview.kaneva.com/register/kaneva/registerHelperService.aspx?action=GetUniqueId"
!define GA_UNIQUE_ID_PROD "http://www.kaneva.com/register/kaneva/registerHelperService.aspx?action=GetUniqueId"
!define GA_UNIQUE_ID_DEV "http://kanevadev:8080/register/kaneva/registerHelperService.aspx?action=GetUniqueId"
!define GA_UNIQUE_ID_3D "http://3dapps.kaneva.com/register/kaneva/registerHelperService.aspx?action=GetUniqueId"

!define GA_DOMAIN "install.kaneva.com"
!define GA_ACCOUNT "UA-10115643-18"
!define GA_URL_BASE "http://www.google-analytics.com/__utm.gif"
!define GA_URL_VERSION "utmwv=4.7.2"
!define GA_URL_UNIQUEIDVERSION "utmn=2138265338"
!define GA_URL_HOSTNAME "utmhn=${GA_DOMAIN}"
!define GA_URL_LANG_ENCODING "utmcs=UTF-8"
!define GA_URL_SCREEN_RES "utmsr=800x600"
!define GA_URL_SCREEN_COLOR_DEPTH "utmsc=32-bit"
!define GA_URL_BROWSER_LANG "utmul=en-us"
!define GA_URL_JAVA_ENABLED "utmje=1"
!define GA_URL_FLASH_VERSION "utmfl=10.0%20r2"
!define GA_URL_AD_SENSE "utmhid=1881419132" ; random number (ad sense id?) 
!define GA_URL_REFERRAL "utmr=-"
!define GA_URL_DOC_URI "utmp=/"
!define GA_URL_ACCOUNT_STRING "utmac=${GA_ACCOUNT}" ; setup on the server, must match
!define GA_URL_COOKIE "utmcc="
!define GA_URL_COOKIE_MAIN "__utma%3D"
!define GA_URL_DOMAIN_HASH "123249363" ; generated from javascript code http://www.google.com/support/forum/p/Google%20Analytics/thread?tid=626b0e277aaedc3c&hl=en
!define GA_URL_UTMZ_RANDOM "__utmz%3D114580437"
!define GA_URL_VERSION_RANDOM "1.1"
!define GA_URL_UTMCSR "utmcsr%3D(direct)%7C"
!define GA_URL_UTMCCN "utmccn%3D(direct)%7C"
!define GA_URL_UTMCMD "utmcmd%3D(none)%3B"

; The dynamic items of the GA URL
Var /GLOBAL step
Var /GLOBAL CURRENT_TIME
Var /GLOBAL GA_URL
Var /GLOBAL GA_URL_PAGE_TITLE
Var /GLOBAL GA_GUID
Var /GLOBAL GA_URL_PAGE_REFERRAL
Var /GLOBAL GA_URL_FIRST_VISIT_DATETIME
Var /GLOBAL GA_URL_PREVIOUS_VISIT_DATETIME
Var /GLOBAL GA_URL_CURRENT_VISIT_DATETIME
Var /GLOBAL GA_URL_NUMBER_OF_VISITS
VAR /GLOBAL unique_ga_Id
; ### google analytics end


!macro getGUID un
Function ${un}getGUID
   ReadINIStr $0 "$TEMP\gacli.ini" GA GUID
   IfErrors 0 skip_create_guid
   
   StrCmp $targetEnvironment "preview" 0 try3d
   StrCpy $2 "${GA_UNIQUE_ID_PREVIEW}"
   Inetc::get /SILENT "${GA_UNIQUE_ID_PREVIEW}" "$TEMP\ga_unique.dat" /END
   Pop $0
   StrCmp $0 'OK' ga_parse_uniqueid_file ga_create_guid_error
    
   try3d:
   StrCmp $targetEnvironment "3d" 0 trydev
   StrCpy $2 "${GA_UNIQUE_ID_3D}"
   Inetc::get /SILENT "${GA_UNIQUE_ID_3D}" "$TEMP\ga_unique.dat" /END
   Pop $0
   StrCmp $0 'OK' ga_parse_uniqueid_file ga_create_guid_error

   trydev:
   StrCmp $targetEnvironment "dev" 0 tryprod
   StrCpy $2 "${GA_UNIQUE_ID_DEV}"
   Inetc::get /SILENT "${GA_UNIQUE_ID_DEV}" "$TEMP\ga_unique.dat" /END
   Pop $0
   
   StrCmp $0 'OK' ga_parse_uniqueid_file ga_create_guid_error

   tryprod:
   StrCpy $2 "${GA_UNIQUE_ID_PROD}"
   Inetc::get /SILENT "${GA_UNIQUE_ID_PROD}" "$TEMP\ga_unique.dat" /END
   Pop $0
   
   StrCmp $0 'OK' ga_parse_uniqueid_file ga_create_guid_error
   
   ga_create_guid_error:
    ; MessageBox MB_ICONEXCLAMATION "Could not obtain a GA ID. URL=$2"
    goto ga_error

	ga_parse_uniqueid_file:

   ; parse ga_unique file for the ID
   ClearErrors
   FileOpen $0 "$TEMP\ga_unique.dat" r
   IfErrors ga_error

   FileRead $0 $1
   StrCpy $R0 "ga_unique.dat read error"
   StrCmp $1 "" parse_ga_error
   ${WordFind2X} "$1" "<ReturnCode>" "</ReturnCode>" "E+1" $R0
   IfErrors parse_ga_error
   ${If} $R0 == 0
	${WordFind2X} "$1" "<Unique>" "</Unique>" "E+1" $R0
	IfErrors parse_ga_error
	StrCpy $unique_ga_Id $R0
	FileClose $0
	goto uniquedone
   ${Else}
       	goto ga_id_error_return
   ${EndIf}

parse_ga_error:
   DetailPrint $1
   FileClose $0

   ; MessageBox MB_ICONEXCLAMATION "Error parsing GA ID results:$R0"

   Delete "$TEMP\ga_unique.dat"

   goto ga_error

ga_id_error_return:
   MessageBox MB_OK "getGUID ga_id_error_return"
   DetailPrint $1
   FileClose $0

   ; MessageBox MB_ICONEXCLAMATION "Error from GA ID. Result:$R0"

   Delete "$TEMP\ga_unique.dat"

   goto ga_error

uniquedone:
   Delete "$TEMP\ga_unique.dat"
   StrCpy $0 $unique_ga_Id
   return
   
skip_create_guid:
	Pop $0
ga_error:
FunctionEnd
!macroend

!insertmacro getGUID ""
!insertmacro getGUID "un."


Function downloadVCREDIST
  DetailPrint "Downloading VCRedist..."

  StrCpy $r1 0
  ${Do}
    IntOp $r1 $R1 + 1
    DetailPrint "Downloading VCRedist..."
    inetc::get /NOCANCEL "${PRODUCT_VCREDIST_URL}${PRODUCT_VCREDIST_PATH}/vcredist_x86.exe" "$INSTDIR\vcredist_x86.exe"
    Pop $0
    ${If} $0 != 'OK'
        inetc::get /NOCANCEL "http://patch.kaneva.com/patch2/vcredist/${PRODUCT_VCREDIST_VERSION}/vcredist_x86.exe" "$INSTDIR\vcredist_x86.exe"
        Pop $0
    ${EndIf}
    ${If} $r1 >= 5
      MessageBox MB_RETRYCANCEL "Your internet service maybe down.$\r$\nError : $0 $\r$\n${PRODUCT_VCREDIST_URL}${PRODUCT_VCREDIST_PATH}/vcredist_x86.exe" IDRETRY true IDCANCEL false
      true:
         goto next
      false:
         MessageBox MB_OK|MB_ICONEXCLAMATION "Please check your internet service and try installing Kaneva again.$\r$\n${PRODUCT_VCREDIST_URL}${PRODUCT_VCREDIST_PATH}/vcredist_x86.exe"
         Quit
      next:
    ${EndIf}
    sleep 1000;
  ${LoopUntil} $0 == 'OK'

FunctionEnd

!macro CheckForKanevaTray un
   ; Check for KanevaTray
   DetailPrint "Checking for Kaneva Tray Process..."
   checkforkim:
   FindProcDLL::FindProc "KanevaTray.exe"
   StrCmp $R0 1 kill_kanevatray_trial1 0
   FindProcDLL::FindProc "kanevatray.exe"
   StrCmp $R0 1 kill_kanevatray_trial1 0
   goto endcheckforkim
   
kill_kanevatray_trial1:
   FindWindow $0 'wndClassKanevaTrayCallback' ''
   IntCmp $0 0 checkforkim2   ; KanevaTray running but window not found, ask user to quit manually
   System::Call 'user32::RegisterWindowMessageA(t "msgKanevaTrayPatcher") i .r1'
   IntCmp $1 0 checkforkim2   ; If message registration failed, ask user to quit manually
    
   ; Notify KanevaTray to quit
   SendMessage $0 $1 2 0
    
   ; Wait for 500ms and check again
   checkforkim2:
   sleep 500
   FindProcDLL::FindProc "KanevaTray.exe"
   StrCmp $R0 1 kill_kanevatray_trial2 0
   FindProcDLL::FindProc "kanevatray.exe"
   StrCmp $R0 1 kill_kanevatray_trial2 0
   goto endcheckforkim
   
kill_kanevatray_trial2:
   ; Wait for another 500ms and do final check
   sleep 500
   FindProcDLL::FindProc "KanevaTray.exe"
   StrCmp $R0 1 kill_kanevatray_failed 0
   FindProcDLL::FindProc "kanevatray.exe"
   StrCmp $R0 1 kill_kanevatray_failed 0
   goto endcheckforkim
   
kill_kanevatray_failed:
   Processes::KillProcess "KanevaTray.exe"
   StrCmp $R0 "1" endcheckforkim
   Processes::KillProcess "kanevatray.exe"
   StrCmp $R0 "1" endcheckforkim
   MessageBox MB_OKCANCEL|MB_ICONQUESTION "The Kaneva Tray application is currently running. Please exit the Kaneva Tray application before continuing with the ${un}install." IDOK checkforkim IDCANCEL 0
   Quit

   
   endcheckforkim:
!macroend

!macro DeleteVirtualStoreFiles
    StrCmp $LOCALAPPDATA "" macro_skip_virtualstore_cleanup	; Do nothing if LOCALAPPDATA not defined (WinXP?)
    StrCpy $0 $PROGRAMFILES 2 1
    StrCmp $0 ":\" 0 macro_skip_virtualstore_cleanup		; Do nothing if $PROGRAMFILES does not begin with "?:\"
    StrCpy $0 $PROGRAMFILES "" 3
    StrCpy $virtualStorePath "$LOCALAPPDATA\VirtualStore\$0\Kaneva"
    IfFileExists "$virtualStorePath\*.*" 0 macro_skip_virtualstore_cleanup
    RmDir /r "$virtualStorePath"
macro_skip_virtualstore_cleanup:
!macroend

!macro DeleteUnsentCrashReports
    StrCmp $LOCALAPPDATA "" macro_skip_unsentcrashreports_cleanup	; Do nothing if LOCALAPPDATA not defined (WinXP?)
    StrCpy $0 $PROGRAMFILES 2 1
    StrCmp $0 ":\" 0 macro_skip_unsentcrashreports_cleanup		; Do nothing if $PROGRAMFILES does not begin with "?:\"
    StrCpy $0 $PROGRAMFILES "" 3
    StrCpy $unsentCrashReportsPath "$LOCALAPPDATA\CrashRpt\UnsentCrashReports"
    IfFileExists "$unsentCrashReportsPath\*.*" 0 macro_skip_unsentcrashreports_cleanup
    RmDir /r "$unsentCrashReportsPath"
macro_skip_unsentcrashreports_cleanup:
!macroend

Function myGUIInit
	; Doing this and the SetWindowPos seems to get it on top ok.
	BringToFront

	; Get window handle of installer into register 0.
	; This only works in onGUIInit! (so you still can't silently call InetBgDL from onInit)
	StrCpy $0 $HWNDPARENT
	
	; Make window always-on-top. Yes, this is bad but we are only doing it temporarily!
	; This prevents InetBgDL's hidden dialog from getting foreground precedence over the installer.
	; This must be done before any InetBgDL calls.
	; -1 = HWND_TOPMOST, 3 = SWP_NOSIZE|SWP_NOMOVE
	System::Call "user32::SetWindowPos(i r0, i -1, i 0, i 0, i 0, i 0, i 3)"
FunctionEnd

Function .onInit
   DetailPrint "Beginning Kaneva Install..."
   StrCpy $installMode ""
   StrCpy $GA_GUID ""
   
   ${GetParameters} $R0
   ClearErrors
   ${GetOptions} $R0 /MODE= $installMode

UAC_Elevate:
   DetailPrint "Requesting Elevation for Installation..."
   !insertmacro UAC_RunElevated 
   StrCmp 1223 $0 UAC_UserAborted   ; UAC dialog aborted by user
   StrCmp 1062 $0 UAC_Err	    ; Logon service not available
   StrCmp 0 $0 0 UAC_Err	    ; Check for other errors
   StrCmp 1 $1 0 UAC_Success	    ; $0==0 and $1!=1 -> Inner Process, or WinXP
   
   ;$0==0 and $1==1 -> Outer process
   ;Launch Kaneva automatically if installation completes
   GetErrorLevel $3
   StrCmp 0 $3 0 Install_Cancel_Or_Error
   ClearErrors
   Call LaunchLink
Install_Cancel_Or_Error:
   Quit
 
UAC_Err:
   ;All other errors
   MessageBox mb_iconstop "Unable to request privileges required to install Kaneva, error $0"
   Quit
 
UAC_UserAborted:
   ;Cancelled by user
   MessageBox mb_iconstop "Kaneva setup requires permission to run.  Please run setup again and select Yes when prompted to enjoy Kaneva."
   Quit
 
UAC_Success:
   ;Inner Process
   StrCmp 0 $3 0 UAC_End	    ; Admin?
   StrCmp 3 $1 0 UAC_UserAborted    ; Try again?
   MessageBox mb_iconstop "You must have Administrative privileges to install Kaneva. Please use with an account with these privileges."
   goto UAC_Elevate

UAC_End:
   ClearErrors
   
   SetAutoClose true
   
   StrCpy $no_directx "0"
   StrCpy $no_directx_9c "0"
   StrCpy $chks_fail "0"
   StrCpy $error_message ""
   StrCpy $download_message ""
   StrCpy $download_list ""
   StrCpy $os_ver ""
	
   IntOp $previousVersionDetected 0 + 0 ; default
   IntOp $updateVisitStats 1 + 0	; initialize to 1: we only update GA visit stats (numVisits, currVisit, prevVisit) once per install
   
   StrCpy $targetEnvironment "Prod" ; default to Production
   ; Check previous installation
   ReadRegStr $1 ${PRODUCT_ROOT_KEY} "${PRODUCT_DIR_REGKEY}" "InstallPathSub"
   StrCmp $1 "" newInstall
   StrCpy $0 $1 1 -1
   StrCmp $0 "\" 0 skipTrimSlash
   StrCpy $1 $1 -1
   
   skipTrimSlash:
   
   ; Validate existing ga.ini
   ReadINIStr $0 "$1\ga.ini" GA Domain
   StrCmp $0 ${GA_DOMAIN} 0 deleteGAINI

   ReadINIStr $0 "$1\ga.ini" GA Account
   StrCmp $0 ${GA_ACCOUNT} 0 deleteGAINI
   
   Goto reuseGAINI
   
   deleteGAINI:
   
   ; Delete existing ga.ini if domain or account does not match
   Delete $1\ga.ini
   Goto newInstall
   
   reuseGAINI:
   
   ; Otherwise, reuse ga.ini by copying it to $TEMP
   CopyFiles /SILENT /FILESONLY "$1\ga.ini" "$TEMP\gacli.ini"
   
   newInstall:

   Push "install-init"
   Call GA
   
    ; Check if any Kaneva executables still running
    
    !insertmacro CheckForKanevaTray ""
    DetailPrint "Checking for CrashSender Process..."
    FindProcDLL::FindProc "CrashSender1403.exe"
    StrCmp $R0 1 0 +2
    Processes::KillProcess "CrashSender1403.exe"
    
    checkforlauncher:
    DetailPrint "Checking for Launcher Process..."
    FindProcDLL::FindProc "KanevaLauncher.exe"
    StrCmp $R0 1 0 +8
    Processes::KillProcess "KanevaLauncher.exe"
    StrCmp $R0 "1" +6
    sleep 3000
    FindProcDLL::FindProc "KanevaLauncher.exe"
    StrCmp $R0 1 0 +3
    MessageBox MB_OKCANCEL|MB_ICONQUESTION "The KanevaLauncher application is currently running.$\r$\nPlease exit the KanevaLauncher application before continuing with the install." IDOK checkforlauncher IDCANCEL 0
    Quit

	
	System::Call 'kernel32::CreateMutexA(i 0, i 0, t "WOKMUTEX") i .r1 ?e'
	Pop $R0
	StrCmp $R0 0 AnotherInstance
	MessageBox MB_OK|MB_ICONEXCLAMATION "An instance of the installer is already running."
	Abort

	AnotherInstance:
	System::Call 'kernel32::CreateMutexA(i 0, i 0, t "GAMELAUNCHER") i .r1 ?e'
	Pop $R0
	StrCmp $R0 0 RegEntryAlreadyExists
	MessageBox MB_OK|MB_ICONEXCLAMATION "Cannot install launcher while an instance of it is still open."
	Abort

  RegEntryAlreadyExists:
  loop2:
    EnumRegKey $game_reg_key ${PRODUCT_ROOT_KEY} ${PRODUCT_DIR_REGKEY_PARENT} $game_reg_key_index
    StrCmp $game_reg_key "" done2
    StrCmp $game_reg_key "${GAME_ID}" Checkifrunning
    IntOp $game_reg_key_index $game_reg_key_index + 1
  Goto loop2
  
  Checkifrunning:
  IntOp $game_reg_key_index $game_reg_key_index + 1
  ;Prevent from uninstalling while kepclient is running.
  System::Call 'kernel32::CreateMutex(i 0, i 0, t "GAME_ENGINE_$game_reg_key") i .r1 ?e'
  Pop $R0
  StrCmp $R0 0 loop2
  MessageBox MB_OK|MB_ICONEXCLAMATION "Cannot install WOK while an instance of it is still open."
  Quit
  done2:
  StrCpy $game_reg_key_index "0"  
 
  ;Reset:
  StrCpy $game_reg_key_index "0"

  
  loop:
    EnumRegKey $game_reg_key ${PRODUCT_ROOT_KEY} ${PRODUCT_DIR_REGKEY_PARENT} $game_reg_key_index
    StrCmp $game_reg_key "" done
    StrCmp $game_reg_key "${GAME_ID}" IntUninstall
    IntOp $game_reg_key_index $game_reg_key_index + 1
  Goto loop
    
  IntUninstall:
    IntOp $game_reg_key_index $game_reg_key_index + 1
    ReadRegStr $other_game_path ${PRODUCT_ROOT_KEY} "SOFTWARE\Kaneva\$game_reg_key" "InstallPath"
    StrCmp $other_game_path "" +3
	
	DeleteRegKey ${PRODUCT_ROOT_KEY} "SOFTWARE\Kaneva\$game_reg_key"
	DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "Software\Microsoft\Windows\CurrentVersion\Uninstall\$game_reg_key" 
    DeleteRegKey /ifempty HKLM "${PRODUCT_DIR_REGKEY_PARENT}" 
  
    Delete "$SMPROGRAMS\Kaneva\$game_reg_key.lnk"
    Delete "$SMPROGRAMS\Kaneva\$game_reg_key\Uninstall.lnk"
    
    RMDir "$SMPROGRAMS\Kaneva\$game_reg_key"
    Delete "$DESKTOP\$game_reg_key.lnk"
    Delete "$other_game_path\..\install.log"
    RMDir /r "$other_game_path\"
	
  done:
  	DetailPrint "Checking for Windows Version..."
    Call GetWindowsVersion
    Pop $R0
    StrCpy $os_ver $R0
    StrCmp $R0 "95" OSNotSupported
    StrCmp $R0 "98" OSNotSupported
    StrCmp $R0 "ME" OSNotSupported
    StrCpy $win_ver $R0
    StrCpy $0 $R0 2
    StrCmp $0 "NT" OSNotSupported
    StrCmp $0 "XP" OSNotSupported
    
    goto AdminCk

   OSNotSupported:
    DetailPrint "Found Unsupported Windows Version..."
    Call myGUIInit
    MessageBox MB_OK|MB_ICONSTOP "Kaneva requires a PC with Windows 7 or newer.$\r$\nWe look forward to seeing you on a compatible PC."
    Abort

   AdminCk:
    Call IsUserAdmin
    Pop $R0
    StrCmp $R0 "true" DirectXCk
    MessageBox MB_OK|MB_ICONSTOP "This user does not have Administrative privileges. Please install with an account with these privileges."
    Abort

  DirectXCk:
  StrCmp $os_ver "Vista" WriteExtraReg
  Goto InstallDx
 
  WriteExtraReg:
  ;WriteRegStr ${PRODUCT_ROOT_KEY2} "${PRODUCT_VISTA_REGKEY}" "$INSTDIR\WOK\KepClient.exe" "WINXPSP2"
  ;WriteRegStr ${PRODUCT_ROOT_KEY2} "${PRODUCT_VISTA_REGKEY}" "$INSTDIR\World of Kaneva\KepClient.exe" "WINXPSP2"
  ;WriteRegStr ${PRODUCT_ROOT_KEY2} "${PRODUCT_VISTA_REGKEY}" "$INSTDIR\Star\${GAME_ID}\KepClient.exe" "WINXPSP2"
   
  InstallDx:
  Call GetDXVersion
  Pop $R3
  IntCmp $R3 900 +3 0 +3
    MessageBox "MB_OK" "Requires DirectX 9.0 or later."
  Abort
 
  StrCpy $outputFolder "${STAR_NAME}\${GAME_ID}"
  
  SetOutPath "$INSTDIR"
  File ".\kaneva_banner.bmp"

FunctionEnd

Section
  SetBrandingImage "$INSTDIR\kaneva_banner.bmp"
SectionEnd

Section "KANEVA Patch Client" SEC01

  Push "install-start"
  Call GA
   StrCpy $bWOKAlreadyInstalled 0;
   IfFileExists $INSTDIR\*.* installed notInstalled
   installed:
   StrCpy $bWOKAlreadyInstalled 1;
   notInstalled:
    
   ;Check their version of Kaneva and uninstall where nessecary
   ReadRegStr $0 HKCU "Software\Kaneva\World of Kaneva" "Product Version"
   
   ; If the key exists,  then we may need to uninstall the previous version if it doesn't match
   ; the version which we are currently installing.
   ${If} $0 != ""
       ; Clean up redistributable file if kaneva is already installed.
       delete "$INSTDIR\vcredist_x86.exe"
	   ${VersionCompare} $0 ${PRODUCT_VERSION} $1
	   ${If} $1 != 0 ;${PRODUCT_VERSION} > $0
			;RmDir /r "$INSTDIR\"
			;Rename "$INSTDIR\World of Kaneva" "$INSTDIR\Star\3296"
            !ifdef SPECIFIC_RELEASE
  			  ${If} ${SPECIFIC_RELEASE} == "prod"
				DetailPrint "Uninstalling older Kaneva version."
				RmDir /r "$INSTDIR\Preview\"
				RmDir /r "$INSTDIR\World of Kaneva\"
				RmDir /r "$INSTDIR\Star\"
			  ${Endif}
            !endif
	   ${Endif}
   ${Endif}

   WriteRegStr HKCU "Software\Kaneva\World of Kaneva" "Product Version" "${PRODUCT_VERSION}"

  ;LogSet on

    StrCmp $os_ver "XP" skip_virtualstore_cleanup

  ;Delete Kaneva VirtualStore from registry
    DeleteRegKey HKCU "${PRODUCT_DIR_REGKEY_VIRTUALSTORE}"
    DeleteRegKey HKCU "${PRODUCT_DIR_REGKEY_VIRTUALSTORE64}"

  ;Scan and cleanup Kaneva VirtualStore folder for selected cases
    !insertmacro DeleteVirtualStoreFiles
    
skip_virtualstore_cleanup:

  !insertmacro DeleteUnsentCrashReports

  !ifdef SPECIFIC_RELEASE
    SetOverwrite on
    SetOutPath "$INSTDIR\$outputFolder"
    File "..\..\..\..\Deploy\GameContent\templates\shared\${SPECIFIC_RELEASE}\network.cfg"
    SetOutPath "$INSTDIR"
    File "..\..\..\..\Deploy\GameContent\templates\shared\${SPECIFIC_RELEASE}\network.cfg"
  !endif
  SetOverwrite off

  ; if old kaneva, need up update it's launcher
  IfFileExists "$INSTDIR\World Of Kaneva" 0 skip

	SetOverwrite on
    SetOutPath "$INSTDIR\World Of Kaneva"
	File "..\release\KanevaLauncher.exe"
	SetOverwrite off

skip:
  SetOutPath "$SYSDIR"
  ; from MS
  File "$%windir%\system32\msvcr71.dll" ; CRT
  ;File "$%windir%\system32\mfc71enu.dll"; MFC Lang
  ;File "$%windir%\system32\mfc71.dll"   ; MFC
  ;File "$%windir%\system32\msvcp71.dll" ; STL

  ; WriteRegStr HKCU "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers" "$INSTDIR\$outputFolder\KepClient.exe" "WINXPSP2"

  ; only copy gdiplus if not XP
  Call GetWindowsVersion
  Pop $R0
  StrCmp $R0 "XP" +2
     File "..\..\Tools\GDIPlus\gdiplus.dll"

  SetOverwrite on


  SetOutPath "$INSTDIR\"
  SetOverwrite try
  trypatchclientctrl:
  ClearErrors
  File "..\release\PatchClientCtrl.dll"
  IfErrors locked notlocked
  locked:
  MessageBox MB_OKCANCEL|MB_ICONQUESTION "The patchclientctrl.dll cannot be overwritten. Please close all browser instances." IDOK trypatchclientctrl IDCANCEL 0
  Quit
  notlocked:
  SetOverwrite on
  File "..\release\Log4CPlusU.dll"
  File "..\release\slipPatcher2.dll"
  File "..\..\..\..\Deploy\GameContent\bin\zlib1.dll"
  File "..\..\..\..\Deploy\GameContent\bin\lzmalib.dll"
  File "..\PatchSetup\favicon.ico"
  File "..\KanevaLauncher\KanevaLauncher.exe.manifest"
  File "..\release\KanevaLauncher.exe"
  File "..\release\oalinst.exe"
  File "..\..\..\..\Deploy\GameContent\templates\shared\KanevaLauncherLogCfg.cfg"
  File "..\..\ToolsBuilt\CrashRpt_v.1.4.3_r1645\bin\crashrpt_lang.ini"
  File "..\..\ToolsBuilt\CrashRpt_v.1.4.3_r1645\bin\CrashRpt1403.dll"
  File "..\..\ToolsBuilt\CrashRpt_v.1.4.3_r1645\bin\CrashSender1403.exe"
  
  ;Check to see if we need to install Vcredist_x86.exe
  ClearErrors
  DetailPrint "Detecting VCRedist.."
  ReadRegStr $0 HKLM "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{23daf363-3020-4059-b3ae-dc4ad39fed19}" "BundleVersion"
  ${If} ${Errors}
   DetailPrint "VCRedist Not Found.."
   ; Key Not Found, Install redistributable
   Call VS2005_X86_Redistribution
  ${Else}
      DetailPrint "VCRedist Already Installed..."
	  ${IF} $0 != "${PRODUCT_VCREDIST_VERSION}"
	     DetailPrint "VCRedist Different Version..."
         ; FOUND VCREDIST KEY BUT VERSION DOESN'T MATCH: 
         Call VS2005_X86_Redistribution 
      ${ENDIF}
  ${EndIf}
  

  ;Install OpenAL redistributable
  DetailPrint "Installing oalinst..."
  ExecWait '"$INSTDIR\oalinst.exe" --silent --install'

  SetOverwrite ifnewer

  StrCpy $r1 0
  resumeInstall:
  
  ClearErrors
  DetailPrint "Registering PatchClientCtrl..."
  ExecWait 'regsvr32 /s "$INSTDIR\PatchClientCtrl.dll"'
  IfErrors registrationError
  
  ClearErrors
  DetailPrint "Registering slippatcher..."
  RegDLL "$INSTDIR\slipPatcher2.dll"
  IfErrors registrationError2
  

  WriteRegStr ${PRODUCT_ROOT_KEY} "${PRODUCT_DIR_REGKEY}" "InstallPathFull" "$INSTDIR\${STAR_NAME}\"
  WriteRegStr ${PRODUCT_ROOT_KEY} "${PRODUCT_DIR_REGKEY}" "InstallPathSub" "$INSTDIR\"
  
  CALL CreateURLHandler

  ; Preapprove the two activeX .dlls for IE
  DetailPrint "Registering Browser DLL's"
  WriteRegStr ${PRODUCT_ROOT_KEY} "${PREAPPROVED_ACTIVEX_PATCHCLIENTCTRL_REGKEY}" "" ""
  WriteRegStr ${PRODUCT_ROOT_KEY} "${PREAPPROVED_ACTIVEX_SLIPPATCHER_REGKEY}" "" ""

  ClearErrors
  DetailPrint "Creating Links Part 1"
  CreateDirectory "$SMPROGRAMS\Kaneva"
  CreateShortCut "$SMPROGRAMS\Kaneva\${PRODUCT_NAME}.lnk" "$INSTDIR\KanevaLauncher.exe" ${SHORTCUT_URL}
  IfErrors startMenuShortCutError

  ClearErrors
  DetailPrint "Creating Links Part 2"
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\KanevaLauncher.exe" ${SHORTCUT_URL}
  !ifdef SPECIFIC_RELEASE
    DetailPrint "Creating Links Part 3"
    strcmp ${SPECIFIC_RELEASE} "prod" 0 +2
    CreateShortCut "$DESKTOP\World of Kaneva.lnk" "$INSTDIR\KanevaLauncher.exe" "${URL_PREFIX}://3296" ;Update Old WOK
  !endif
  IfErrors desktopShortCutError

  
  StrCpy $mozilla_reg_key 0
  StrCpy $mozilla_reg_key_index 0

; Iterate over all the Mozilla registry keys taking the last Extension path we find.
  DetailPrint "Registering Firefox Plugin"
  loop:
    EnumRegKey $mozilla_reg_key ${PRODUCT_ROOT_KEY} ${MOZILLA_DIR_REGKEY_PARENT} $mozilla_reg_key_index
    StrCmp $mozilla_reg_key "" done
    IntOp $mozilla_reg_key_index $mozilla_reg_key_index + 1
    ReadRegStr $mozilla_plugin_path ${PRODUCT_ROOT_KEY} "SOFTWARE\Mozilla\$mozilla_reg_key\Extensions" "Plugins"
    StrCmp $mozilla_plugin_path "" +3
        SetOutPath $mozilla_plugin_path
        SetOverwrite on
	    File "..\MozillaPatcher\release\npkanevapatch.dll"
    Goto loop
  done:
  DetailPrint "Done registering Firefox Plugin"
; Reference the Mozilla plugin for the kaneva patcher in the MozillaPlugins directory as well. If it doesn't exist
; go ahead and create it so that if Mozilla or Chrome are installed after Kaneva, they will pick it up and work. Chrome
; will reference the MozillaPlugins registry entry as well.
  DetailPrint "Installing Kaneva Play now Plugin..."
  SetOutPath "$INSTDIR"
  SetOverwrite on
  File "..\MozillaPatcher\release\npkanevapatch.dll"
  WriteRegStr ${PRODUCT_ROOT_KEY2} "Software\MozillaPlugins\@kaneva.com/KanevaPatch" "Path" "$INSTDIR\npkanevapatch.dll"

  SetOutPath "$INSTDIR\$outputFolder"

  ;As opposed to its name suggests, ClearOnFile actually sets ACL to a new one as specified by parameters (inherited entries are not affected).
  
  
  ${If} $bWOKAlreadyInstalled == 0
     DetailPrint "Setting Access Control $INSTDIR"
     AccessControl::SetOnFile "$INSTDIR/" "(S-1-5-11)" "FullAccess"
     DetailPrint "Setting Access Control 2"
     AccessControl::GrantOnRegKey HKLM "Software\Kaneva" "(S-1-5-11)" "FullAccess"
     DetailPrint "Done Setting Access Control"
  ${ENDIF}
  
  Push "install-complete"
  DetailPrint "Analytic Report GA"
  Call GA
  DetailPrint "Done Analytic Report GA"
  Delete "$TEMP\gacli.ini"

  DetailPrint "Goto End"
  Goto end

  registrationError:
     DetailPrint "Error Registering PatchClient DLL..."
     IntOp $r1 $R1 + 1
     ${If} $r1 >= 2
        MessageBox MB_OK|MB_ICONEXCLAMATION "Error registering DLL's.  Please run windows updates and re-install. $r1"
     ${Else}
        DetailPrint "Retrying Install of vcredist attempt: $r1"
        Sleep 1000
        Call VS2005_X86_Redistribution
        goto resumeInstall
     ${ENDIF}
     Quit

  registrationError2:
     DetailPrint "Error Registering slip patcher DLL..."
     MessageBox MB_OK|MB_ICONEXCLAMATION "Error registering slip patcher activeX control"
     Quit

     
  startMenuShortCutError:
     MessageBox MB_OK|MB_ICONEXCLAMATION "Error creating start menu shortcut"
     Abort

  desktopShortCutError:
     MessageBox MB_OK|MB_ICONEXCLAMATION "Error creating desktop shortcut"
     Abort

  end:
  
SectionEnd

Section -AdditionalIcons
  ;CreateShortCut "$SMPROGRAMS\Kaneva\Uninstall.lnk" "$INSTDIR\uninst.exe"
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  
  ;Launch shortcut now if UAC not enabled or Windows XP
  ${If} ${UAC_IsInnerInstance}
    ;Outer process will do the launching
  ${Else}
    ;Not inner process, we have to launch by ourself
    ClearErrors
    Call LaunchLink
  ${EndIf}
 
SectionEnd

Function .OnInstFailed
  SetErrorLevel 1
FunctionEnd
 
Function .OnInstSuccess
  ;Clear Error Level so the outer process can proceed with launching
  SetErrorLevel 0
FunctionEnd

Function CreateURLHandler
   WriteRegStr HKCR "kaneva" "" "URL:Kaneva Protocol"
   WriteRegStr HKCR "kaneva" "URL Protocol" ""
   WriteRegStr HKCR "kaneva\shell" "" ""
   WriteRegStr HKCR "kaneva\shell\open" "" ""
   WriteRegStr HKCR "kaneva\shell\open\command" "" '"$INSTDIR\KanevaLauncher.exe" -url "%1"'
FunctionEnd

Function un.RemoveURLHandler
   DeleteRegKey HKCR "kaneva"
FunctionEnd

!macro GA un
Function ${un}GA
   Pop $step
   ; url encode any underscores 
   ${WordReplace} $step "_" "%5F" "+*" $R0
   StrCpy $step $R0

   Call ${un}UnixEpochTime
   Pop $1 
   ${If} $1 == 'error'
       ; try once more
       Call ${un}UnixEpochTime
       Pop $1 
   ${ElseIf} $1 == 'success'
       Call ${un}UnixEpochTime
       Pop $1 
   ${EndIf}
   
   StrCpy $CURRENT_TIME $1

   ${If} $CURRENT_TIME == 'error'
     	goto ga_error
   ${EndIf}

   ClearErrors

   ${If} $GA_GUID == ''
      Call ${un}getGUID
      Pop $0
      StrCpy $GA_GUID $0
   ${EndIf}
   
   ReadINIStr $1 "$TEMP\gacli.ini" GA FirstVisit
   IfErrors 0 skip_create_firstvisit
   StrCpy $1 $CURRENT_TIME

skip_create_firstvisit:
   StrCpy $GA_URL_FIRST_VISIT_DATETIME $1

   ReadINIStr $1 "$TEMP\gacli.ini" GA PreviousVisit

   IfErrors 0 skip_create_previousvisit
   StrCpy $1 $CURRENT_TIME

skip_create_previousvisit:
   StrCpy $GA_URL_PREVIOUS_VISIT_DATETIME $1

   ReadINIStr $1 "$TEMP\gacli.ini" GA CurrentVisit

   IfErrors 0 skip_create_currentvisit
   StrCpy $1 $CURRENT_TIME

skip_create_currentvisit:
   StrCpy $GA_URL_CURRENT_VISIT_DATETIME $1

   ; update the times before this run
   ${If} $updateVisitStats == 1
      StrCpy $GA_URL_PREVIOUS_VISIT_DATETIME $GA_URL_CURRENT_VISIT_DATETIME
      StrCpy $GA_URL_CURRENT_VISIT_DATETIME $CURRENT_TIME
   ${EndIf}

   StrCpy $GA_URL "$\""
   StrCpy $GA_URL "$GA_URL${GA_URL_BASE}"
   StrCpy $GA_URL "$GA_URL?"
   StrCpy $GA_URL "$GA_URL${GA_URL_VERSION}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_UNIQUEIDVERSION}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_HOSTNAME}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_LANG_ENCODING}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_SCREEN_RES}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_SCREEN_COLOR_DEPTH}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_BROWSER_LANG}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_JAVA_ENABLED}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_FLASH_VERSION}"
   StrCpy $GA_URL "$GA_URL&"

   StrCpy $GA_URL_PAGE_TITLE "utmdt=$step"
   StrCpy $GA_URL "$GA_URL$GA_URL_PAGE_TITLE"

   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_AD_SENSE}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_REFERRAL}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_DOC_URI}"

   StrCpy $GA_URL_PAGE_REFERRAL $step
   StrCpy $GA_URL "$GA_URL$GA_URL_PAGE_REFERRAL"

   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_ACCOUNT_STRING}"
   StrCpy $GA_URL "$GA_URL&"
   StrCpy $GA_URL "$GA_URL${GA_URL_COOKIE}"
   StrCpy $GA_URL "$GA_URL${GA_URL_COOKIE_MAIN}"
   StrCpy $GA_URL "$GA_URL${GA_URL_DOMAIN_HASH}"
   StrCpy $GA_URL "$GA_URL."
   StrCpy $GA_URL "$GA_URL$GA_GUID"
   StrCpy $GA_URL "$GA_URL."
   StrCpy $GA_URL "$GA_URL$GA_URL_FIRST_VISIT_DATETIME"
   StrCpy $GA_URL "$GA_URL."
   StrCpy $GA_URL "$GA_URL$GA_URL_PREVIOUS_VISIT_DATETIME"
   StrCpy $GA_URL "$GA_URL."
   StrCpy $GA_URL "$GA_URL$GA_URL_CURRENT_VISIT_DATETIME"
   StrCpy $GA_URL "$GA_URL."

   ReadINIStr $1 "$TEMP\gacli.ini" GA NumberVisit

   IfErrors 0 skip_create_numbervisit
   StrCpy $1 0 ; initilize to zero

skip_create_numbervisit:
   ; increment number of visits
   ${If} $updateVisitStats == 1
      IntOp $1 $1 + 1
   ${EndIf}
   StrCpy $GA_URL_NUMBER_OF_VISITS $1

   StrCpy $GA_URL "$GA_URL$GA_URL_NUMBER_OF_VISITS"
   StrCpy $GA_URL "$GA_URL%3B%2B"

   StrCpy $GA_URL "$GA_URL__utmb%3D${GA_URL_DOMAIN_HASH}.1.10.$GA_URL_FIRST_VISIT_DATETIME"
   StrCpy $GA_URL "$GA_URL%3B%2B__utmc%3D${GA_URL_DOMAIN_HASH}"
   StrCpy $GA_URL "$GA_URL%3B%2B"

   StrCpy $GA_URL "$GA_URL${GA_URL_UTMZ_RANDOM}"
   StrCpy $GA_URL "$GA_URL."
   StrCpy $GA_URL "$GA_URL$GA_URL_CURRENT_VISIT_DATETIME"
   StrCpy $GA_URL "$GA_URL."
   StrCpy $GA_URL "$GA_URL${GA_URL_VERSION_RANDOM}"
   StrCpy $GA_URL "$GA_URL."
   StrCpy $GA_URL "$GA_URL${GA_URL_UTMCSR}"
   StrCpy $GA_URL "$GA_URL${GA_URL_UTMCCN}"
   StrCpy $GA_URL "$GA_URL${GA_URL_UTMCMD}"
   StrCpy $GA_URL "$GA_URL$\""

   WriteINIStr "$TEMP\gacli.ini" GA URL$step $GA_URL

   ; debug MessageBox MB_ICONEXCLAMATION "GA call for $step - URL: $GA_URL"

   InetBgDL::get $GA_URL "$TEMP\ga_gif" /END
   Pop $0
   
   Delete "$TEMP\ga_gif"

   ; debug MessageBox MB_ICONEXCLAMATION "GA call for $step returned:$0"

   StrCmp $0 'OK' 0 ga_error
   WriteINIStr "$TEMP\gacli.ini" GA Domain ${GA_DOMAIN}
   WriteINIStr "$TEMP\gacli.ini" GA Account ${GA_ACCOUNT}
   WriteINIStr "$TEMP\gacli.ini" GA GUID $GA_GUID
   WriteINIStr "$TEMP\gacli.ini" GA FirstVisit $GA_URL_FIRST_VISIT_DATETIME
   WriteINIStr "$TEMP\gacli.ini" GA PreviousVisit $GA_URL_PREVIOUS_VISIT_DATETIME
   WriteINIStr "$TEMP\gacli.ini" GA CurrentVisit $GA_URL_CURRENT_VISIT_DATETIME
   WriteINIStr "$TEMP\gacli.ini" GA NumberVisit $GA_URL_NUMBER_OF_VISITS
   WriteINIStr "$TEMP\gacli.ini" GA CookieTimestamp $CURRENT_TIME
   ${If} $previousVersionDetected >= 1
      WriteINIStr "$TEMP\gacli.ini" GA Upgrade "true"
   ${EndIf}
   ; Copy the ga.ini file to the install directory to be used by patcher/tray/client apps
   CreateDirectory $INSTDIR
   CopyFiles /SILENT /FILESONLY "$TEMP\gacli.ini" "$INSTDIR\ga.ini"
   IntOp $updateVisitStats 0 + 0
   goto ga_end

ga_error:

   ; MessageBox MB_ICONEXCLAMATION "Analytics failed."

ga_end:

FunctionEnd
!macroend

!insertmacro GA ""
!insertmacro GA "un."

; To duplicate the javascript functionality
; for google analytics. Time needs to be in
; Unix Epoch which Windows implements differently
; thus the magic numbers. 
!macro UnixEpochTime un
Function ${un}UnixEpochTime

   system::call *(&i16,l)i.s 
   system::call 'kernel32::GetSystemTime(isr0)' 
   IntOp $1 $0 + 16 
   system::call 'kernel32::SystemTimeToFileTime(ir0,ir1)' 
   system::call *$1(l.r1) 
   system::free $0 
   system::Int64Op $1 / 10000000 
   Pop $1 
   system::Int64Op $1 - 11644473601	; Adjust epoch offset to match the value used in C++ code ganalytics.cpp (don't ask me which one is correct)
   Pop $1

FunctionEnd
!macroend

!insertmacro UnixEpochTime ""
!insertmacro UnixEpochTime "un."

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit  

UAC_Elevate:
   !insertmacro UAC_RunElevated 
   StrCmp 1223 $0 UAC_UserAborted   ; UAC dialog aborted by user
   StrCmp 1062 $0 UAC_Err	    ; Logon service not available
   StrCmp 0 $0 0 UAC_Err	    ; Check for other errors
   StrCmp 1 $1 0 UAC_Success	    ; $0==0 and $1!=1 -> Inner Process
   
   ;$0==0 and $1==1 -> Outer process
   Quit
 
UAC_Err:
   ;All other errors
   MessageBox mb_iconstop "Unable to request Administrative privileges required to uninstall ${PRODUCT_NAME}, error $0"
   Abort
 
UAC_UserAborted:
   ;Cancelled by user
   MessageBox mb_iconstop "Kaneva uninstaller requires Administrative privileges to proceed."
   Quit
 
UAC_Success:
   ;Inner Process
   StrCmp 0 $3 0 UAC_End	    ; Admin?
   StrCmp 3 $1 0 UAC_UserAborted    ; Try again?
   MessageBox mb_iconstop "This user does not have Administrative privileges. Please uninstall Kaneva using an account with these privileges."
   goto UAC_Elevate

UAC_End:
   ClearErrors
   
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
  Abort
  
  !insertmacro CheckForKanevaTray "un"

  ;Prevent from uninstalling while kepclient is running.
  System::Call 'kernel32::CreateMutex(i 0, i 0, t "GAME_ENGINE_${GAME_ID}") i .r1 ?e'
  Pop $R0
  StrCmp $R0 0 CheckLauncher
  
  sleep 1000
  System::Call 'kernel32::CreateMutex(i 0, i 0, t "GAME_ENGINE_${GAME_ID}") i .r1 ?e'
  Pop $R0
  StrCmp $R0 0 CheckLauncher
  
  MessageBox MB_OK|MB_ICONEXCLAMATION "Cannot uninstall launcher while an instance of it is still open."
  Abort

  ;Prevent from uninstalling the launcher while it is running.	
  CheckLauncher:
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "GAMELAUNCHER") i .r1 ?e'
  Pop $R0
  StrCmp $R0 0 EndCheckingInstances
  MessageBox MB_OK|MB_ICONEXCLAMATION "Cannot uninstall launcher while an instance of it is still open."
  Abort
  
  EndCheckingInstances:
FunctionEnd

Function LaunchLink
  ;Create desktop shortcut just in case the RunAs user is different from current user
  IfFileExists "$DESKTOP\${PRODUCT_NAME}.lnk" launch 0
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\KanevaLauncher.exe" ${SHORTCUT_URL}
  launch:
  StrCmp $installMode "3dappsetup" skip 0
  ;DONTRUN
  ExecShell "" "$DESKTOP\${PRODUCT_NAME}.lnk"
  skip:
FunctionEnd

Section Uninstall

  SetBrandingImage "$INSTDIR\kaneva_banner.bmp"
  SetOutPath "$INSTDIR"

  ClearErrors

  ;Init GA
  IntOp $updateVisitStats 1 + 0	; initialize to 1: we only update GA visit stats (numVisits, currVisit, prevVisit) once per (un)install
  StrCpy $targetEnvironment "Prod" ; default to Production
  CopyFiles /SILENT /FILESONLY "$INSTDIR\ga.ini" "$TEMP\gacli.ini"

  ;Report uninstallation
  Push "uninstall"
  Call un.GA

  ;Cleanup GA
  Delete "$TEMP\gacli.ini"

  ;RMDir "$SMPROGRAMS\Kaneva\Star\${GAME_ID}"
  RMDir /r "$INSTDIR\Star\${GAME_ID}"
  RMDir /r "$INSTDIR\Star\"
  RMDir /r "$INSTDIR\World of Kaneva\"
  ;UnRegDLL "$INSTDIR\PatchClientCtrl.dll"  
 
  ClearErrors
  ;UnRegDLL "$INSTDIR\slipPatcher2.dll"

  CALL un.RemoveURLHandler

  ;DeleteRegKey ${PRODUCT_ROOT_KEY} "${PRODUCT_DIR_REGKEY}"
  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  
  StrCpy $game_reg_key 0
  StrCpy $game_reg_key_index 0
  
; If another game exists, register its patchclientctrl.dll and slipPatcher2.dll
  loop:
    EnumRegKey $game_reg_key ${PRODUCT_ROOT_KEY} ${PRODUCT_DIR_REGKEY_PARENT} $game_reg_key_index
    StrCmp $game_reg_key "" done
    IntOp $game_reg_key_index $game_reg_key_index + 1
    ReadRegStr $other_game_path ${PRODUCT_ROOT_KEY} "SOFTWARE\Kaneva\$game_reg_key" "InstallPath"
    StrCmp $other_game_path "" +3    		
		RegDLL "$other_game_path\PatchClientCtrl.dll"
		RegDLL "$other_game_path\slipPatcher2.dll"
		goto done
    Goto loop
  done:
   
  ;DeleteRegKey /ifempty HKLM "${PRODUCT_DIR_REGKEY_PARENT}"   
  Delete "$SMPROGRAMS\Kaneva\${PRODUCT_NAME}.lnk"
  ;Delete "$SMPROGRAMS\Kaneva\Uninstall.lnk"
  ;RMDir "$SMPROGRAMS\Kaneva\$outputFolder"
  ;RMDir "$SMPROGRAMS\Kaneva"

  Delete "$DESKTOP\${PRODUCT_NAME}.lnk"

  ;Delete "$INSTDIR\..\install.log"
  ;RMDir /r "$INSTDIR\"
    Delete "$INSTDIR\bg2.png"
    Delete "$INSTDIR\npkanevapatch.dll"
	Delete "$INSTDIR\favicon.ico"
	Delete "$INSTDIR\info.dat"
	Delete "$INSTDIR\KanevaLauncher.exe"
	Delete "$INSTDIR\KanevaLauncher.exe.manifest"
	Delete "$INSTDIR\KepDiag.txt"
	Delete "$INSTDIR\network.cfg"
	Delete "$INSTDIR\PatchClientCtrl.dll"
	Delete "$INSTDIR\preview1.png"
	Delete "$INSTDIR\slippatcher2.dll"
	Delete "$INSTDIR\uninst.exe"
	Delete "$INSTDIR\vcredist_x86.exe"
	Delete "$INSTDIR\oalinst.exe"
	Delete "$INSTDIR\ga.ini"
	Delete "$INSTDIR\kaneva_banner.bmp"
	Delete "$INSTDIR\KanevaLauncherLog.log"
	Delete "$INSTDIR\KanevaLauncherLogCfg.cfg"
    Delete "$INSTDIR\crashrpt_lang.ini"
    Delete "$INSTDIR\CrashRpt1402.dll"
    Delete "$INSTDIR\CrashSender1402.exe"
    Delete "$INSTDIR\CrashRpt1403.dll"
    Delete "$INSTDIR\CrashSender1403.exe"
	Delete "$INSTDIR\Log4CPlusU.dll"
	Delete "$INSTDIR\Zlib.dll"
	Delete "$INSTDIR\Zlib1.dll"
	Delete "$INSTDIR\lzmalib.dll"
	RMDir  "$INSTDIR\temp"
  SetAutoClose true

SectionEnd