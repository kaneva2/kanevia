@echo off

if exist "NOSIGN" (
	ECHO *** NOSIGN
) else (
    ECHO *** Signing %1 with SHA1 digest and certificate ...
    CALL sign-with-pwd-sha1.cmd %1

    ECHO *** Signing %1 with SHA2 digest and certificate ...
    CALL sign-with-pwd-sha2.cmd %1
)
