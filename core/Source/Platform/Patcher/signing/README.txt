-----------------------------------------------------------------

A. Binary files to be signed:

* KanevaLauncher.exe
* PatchClientCtrl.dll
* slipPatcher.dll
* WorldOfKanevaSetup.exe
* KEPClient.exe
* KEPEdit.exe
* 3DAppSetup.exe
* WOKSetup.exe
* KanevaTray.exe

-----------------------------------------------------------------

B. Certificate from CA

Our CA (verisign) provides certificate in PFX format.


-----------------------------------------------------------------

C. Code-signing utilities

* signtool.exe (available from Windows SDKs). With default Windows SDK 8.1 installation, 
  it's located under following directory:

  C:\Program Files (x86)\Windows Kits\8.1\bin

* sign-with-pwd.cmd

  A batch file calling signtool.exe with parameters for signing with SHA1 digests.

* sign-with-pwd-256.cmd

  A batch file calling signtool.exe with parameters for signing with SHA2 digests.

* password helper (obsolete)

  We used to use a separate password helper utility with the old signcode.exe GUI 
  application. It's no longer necessary. 

-----------------------------------------------------------------

D. Dual-signing

Due to security vulnerability of SHA1 digiest algorithm, Microsoft has decided that it will
discontinue support for SHA1 signed applications on Windows 8 and later beginning Jan 1 2016.
Any application signed after the deadline will need to be signed with SHA2 digests. However,
older OS platform such as Windows 7 and earlier do not have proper SHA2 support yet. 

To allow an application to be distributed to older platform, the application must be dual-
signed with both SHA1 and SHA2. To do it, we will need to call SHA1 signer first (sign-with-pwd.cmd),
followed by the SHA2 signer (sign-with-pwd-256.cmd).

Two signers use different code signing certificates and different timestamps. The SHA2 signer
also requires a RFC 3161 timestamp server that provides timestamps on SHA2 digests. For dual
signing, SHA2 signer must pass in /as switch for signtool.exe to overlay the signature over
the existing SHA1 one.

-----------------------------------------------------------------

E. Other Issues

Old signtool.exe does not support /as switch. The newer version (Win SDK 8.0/8.1) supports it.
However when use /as switch with signtool on Windows 7, it throws following error:

SignTool Error: A required function is not present.
        This error likely means that you are running SignTool on an OS that
        does not support the options you've specified.

It also appears that the SHA2 timestamping process crashes on Windows 7 (when /as is not used). 
More investigation needed.
