@echo off

set strFile=%1
set SERVERLIST=(http://timestamp.digicert.com)

set timestampErrors=0

REM ----------------------------------------
REM - Most common failure is the timestamp server being down. Try for up to 300
REM - times, with a 2 second delay between attempts. 
REM ----------------------------------------

for /L %%a in (1,1,300) do (

    for %%s in %SERVERLIST% do (

        ECHO signtool sign /f kanevacodesign_sha2.pfx /fd sha256 /p ******* /tr %%s /td sha256 /as %strFile%

        REM -----------------------------------------------------------------------------
        REM - If someone can get to this directory, then they already can use the same tools
        REM - we have to use our code signing cert.  We used sign-pwd to get past the issue of 
        REM - the UI blocking, the encryption was a bonus.
        REM -----------------------------------------------------------------------------

	signtool sign /f kanevacodesign_sha2.pfx /fd sha256 /p "<y83uM?'R}}<vc92" /tr %%s /td sha256 /as %strFile%
        ECHO Exit code: %errorlevel%

        if errorlevel 0 goto end_success
        set /a timestampErrors+=1
    )

    echo Failed... waiting 2 seconds and trying again
    choice /N /T:2 /D:Y >NUL
)

:end_error
echo Timestamp Failed %timestampErrors% times.
exit /b 1

:end_success
echo Timestamp Completed after %timestampErrors% errors.
exit /b 0
