///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "../PatchLib/PatchLibGlobals.h"

#include "PatchClientCtrl.h"
#include "ProgressDlg.h"
#include "../KanevaRes/resource.h"
#include "../PatchLib/ConfigValues.h"
#include "config.h"
#include "LoginProgressDlg.h"
#include "UnicodeMFCHelpers.h"
#include "KEPHelpers.h"
#include "LogHelper.h"

#include "common/KEPUtil/RuntimeReporter.h"

#define BACKGROUND_COLOR RGB(0xff, 0xff, 0xff)
#define TEXT_COLOR RGB(0, 0, 0)
#define PROGRESS_BAR_BKCOLOR RGB(0xff, 0xff, 0xff)
#define PROGRESS_BAR_COLOR RGB(255, 140, 0)

#ifndef TOOLBAR_TEXTCOLOR
#define TOOLBAR_TEXTCOLOR RGB(0xB3, 0xD6, 0xF2)
#endif

#define FONT_NAME "Arial"

#define NOT_SET -1
#define MIN_VAL -2
#define MAX_VAL -3

#define TIMER_ID_PROGRESS 9994

namespace KEP {

static LogInstance("Instance");

struct TimerInfo {
	unsigned long downloadsize;
	unsigned long estimatedtime;
};

// message id from lib
extern UINT gMsgId;
extern UINT gMsgIdTrayPatcher;

// Constant for the string we read from the registry around
// displaying just one bar ("1") or both ("0")
const char* CProgressDlg::s_ShowSimple("1");

extern HWND kaneva_ClientFindWindow();

// Routine for shifting the children of this dialog along the Y-axis by a given amount
static BOOL WINAPI MoveWindows(HWND hWnd, LPARAM lParam) {
	int yDifference = (int)lParam;
	if (yDifference) {
		CWnd* wnd = CWnd::FromHandle(hWnd);
		if (wnd) {
			CRect clientRect;
			wnd->GetWindowRect(&clientRect);
			clientRect.MoveToY(clientRect.top - yDifference);
			wnd->GetParent()->ScreenToClient(clientRect);
			wnd->MoveWindow(&clientRect);
		}
	}
	return TRUE;
}

// lowest version that used encrypted pw on command line, 4.0.9.*
//#define MIN_ENCRYPT_VERSION 0x0004000000090000i64

static INT64 getFileVersion(const char* gamePath) {
	const char* csEntry = "FileVersion";

	std::wstring gamePathW = Utf8ToUtf16(gamePath).c_str();
	HMODULE hLib = LoadLibrary(gamePathW.c_str());
	if (hLib == 0) {
		return false;
	}

	DWORD unused;
	DWORD size = GetFileVersionInfoSize(gamePathW.c_str(), &unused);
	if (size == 0)
		return false;

	void* data = malloc(size);
	if (!GetFileVersionInfo(gamePathW.c_str(), unused, size, data)) {
		free(data);
		return false;
	}

	VS_FIXEDFILEINFO* info;
	UINT len;
	if (!VerQueryValue(data, L"\\", (void**)&info, &len)) {
		free(data);
		return false;
	}

	// see if we're above 4.0
	INT64 ver = info->dwFileVersionMS;
	ver = ver << 32;
	ver |= info->dwFileVersionLS;
	return ver;
}

IMPLEMENT_DYNAMIC(CProgressDlg, CKanevaDialogSkin)

static PatchInfo* BestPatchInfo(std::vector<PatchInfo>& v, size_t& id) {
	if (v.size() == 0)
		return NULL;
	PatchInfo* pPI = NULL;
	TimeMs curTimeMs = (id < v.size()) ? v[id].serverStatus.m_timeMs : 0.0;
	size_t nxtId = 0;
	TimeMs nxtTimeMs = 10000000.0;
	for (size_t i = 0; i < v.size(); ++i) {
		if (!v[i].serverStatus.m_valid)
			continue;
		TimeMs timeMs = v[i].serverStatus.m_timeMs;
		if ((timeMs > curTimeMs) && (timeMs < nxtTimeMs)) {
			nxtId = i;
			nxtTimeMs = timeMs;
			pPI = &v[i];
		}
	}
	id = nxtId;
	return pPI;
}


CProgressDlg::CProgressDlg(PatchClientController* ctrl,
		const char* gameExe,
		const char* gameName,
		const char* URL,
		const char* gameInstallDir,
		int onComplete,
		bool gamerunning,
		const char* showdetail,
		LONG zoneindex,
		CLoginProgressDlg* loginProgressDlg,
		CWnd* pParent) :
		baseclass(CProgressDlg::IDD, pParent),
		m_zoneindex(zoneindex),
		m_patcherController(ctrl),
		m_Url(URL),
		m_gameExe(gameExe),
		m_gameName(gameName),
		m_gameInstallDir(gameInstallDir),
		m_complete(false),
		m_gamerunning(gamerunning),
		m_playOnComplete(onComplete),
		m_play(false),
		m_totalFiles(0),
		m_totalSize(0),
		m_totalSubSize(0),
		m_sTotalSize("0 KB / 0 KB"),
		m_sSubSize("0 KB / 0 KB"),
		m_sTotalUpdates(""),
		m_showdetail(showdetail),
		m_downloadgameupdate(false, false, 12),
		m_itemStatus(false, false, 8),
		m_totalStatus(false, false, 8),
		m_itemByteCounter(false, false, 8),
		m_totalByteCounter(false, false, 8),
		m_statusText(false, false, 8),
	    m_DisplayReady(false),
		m_loginProgressDlg(loginProgressDlg) {
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_startTime = 0;
}

CProgressDlg::~CProgressDlg() {
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX) {
	baseclass::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS, m_progress);
	DDX_Control(pDX, IDC_FILE_PROGRESS, m_fileProgress);
	DDX_Check(pDX, IDC_PLAYCHECK, m_playOnComplete);
	::KEP::DDX_Text(pDX, IDC_TOTAL_SIZE, m_sTotalSize);
	::KEP::DDX_Text(pDX, IDC_SUB_SIZE, m_sSubSize);
	::KEP::DDX_Text(pDX, IDC_TOTAL_UPDATES, m_sTotalUpdates);
	DDX_Control(pDX, IDC_PLAY, m_buttonplay);
	DDX_Control(pDX, IDC_GAME_UPDATE, m_downloadgameupdate);
	DDX_Control(pDX, IDC_ITEM_UPDATE, m_itemStatus);
	DDX_Control(pDX, IDC_TOTAL_UPDATES, m_totalStatus);
	DDX_Control(pDX, IDC_SUB_SIZE, m_itemByteCounter);
	DDX_Control(pDX, IDC_TOTAL_SIZE, m_totalByteCounter);
	DDX_Control(pDX, IDC_STATUS_TEXT, m_statusText);
}

BEGIN_MESSAGE_MAP(CProgressDlg, CKanevaDialogSkin)
ON_WM_SYSCOMMAND()
ON_WM_PAINT()
ON_WM_TIMER()
ON_WM_QUERYDRAGICON()
ON_WM_CTLCOLOR()
ON_WM_DESTROY()
ON_REGISTERED_MESSAGE(gMsgId, OnStatusMessage)
ON_BN_CLICKED(IDC_PLAY, OnBnClickedPlay)
ON_BN_CLICKED(IDC_PLAYCHECK, OnBnClickedPlaycheck)
ON_NOTIFY(NM_CUSTOMDRAW, IDC_FILE_PROGRESS, OnNMCustomdrawFileProgress)
END_MESSAGE_MAP()

extern bool DoPatchExe(PatchProcess* pPP);

extern void CancelPatch();

static bool once = false;

afx_msg LRESULT CProgressDlg::OnStatusMessage(WPARAM wp, LPARAM lp) {
	switch (wp) {

		case MSG_STARTTHREAD:
			break;

		case MSG_STATUS:
			break;

		case MSG_INIT: 
			LogInfo("MSG_INIT: OK");
			break;

		case MSG_COMPLETE:
			LogInfo("MSG_COMPLETE: OK");
			break;

		case MSG_ABORT:
			LogError("MSG_ABORT: OK");
			break;

		case MSG_ERROR: {
			CStringW t;
			t = lp == ERR_NOTCONNECTED ? L"Not connected" : L"Timeout";
			CStringW s;
			s.Format(L"Error from update engine: %s", t);
			LogError("MSG_ERROR: " << Utf16ToUtf8(s));

			m_statusText.ShowWindow(true);
			m_statusText.setWindowText(Utf16ToUtf8(s).c_str());
			m_statusText.ShowWindow(SW_SHOW);
			GetParent()->PostMessage(WM_ALLOWCLOSEPATCHER, 0, 0);
		} break;

		case MSG_VERBOSE_ERROR: {
			ERROR_MSG_VERBOSE* errMsg = (ERROR_MSG_VERBOSE*)lp;
			if (errMsg->errorCode == RC_PROMPT_ALLOW) {
				LogError("MSG_VERBOSE_ERROR: RC_PROMPT_ALLOW");
				// Ignore This Error
			} else {
				LogError("MSG_VERBOSE_ERROR: " << errMsg->msg);
				m_statusText.ShowWindow(true);
				m_statusText.setWindowText(errMsg->msg);
				m_statusText.ShowWindow(SW_SHOW);
			}
			GetParent()->PostMessage(WM_ALLOWCLOSEPATCHER, 0, 0);
		} break;

		case MSG_GOTOAPP: {
			GOTOAPP_MSG* msg = (GOTOAPP_MSG*)lp;

			LogInfo("MSG_GOTOAPP: " << msg->msg);
			m_statusText.ShowWindow(true);
			m_statusText.setWindowText(msg->msg);
			m_statusText.ShowWindow(SW_SHOW);
			GetParent()->PostMessage(WM_ALLOWCLOSEPATCHER, 0, 0);
		} break;

		case MSG_OTHERERROR:
			LogError("MSG_OTHER_ERROR: OK");
			GetParent()->PostMessage(WM_ALLOWCLOSEPATCHER, 0, 0);
			break;

		case MSG_RECONNECT: {
			CStringW s;
			s = L"Connection Lost,Trying to reconnect.";

			LogError("MSG_RECONNECT: " << Utf16ToUtf8(s));
			m_statusText.setWindowText(Utf16ToUtf8(s).c_str());
			if (m_statusText.IsWindowVisible() == SW_HIDE)
				m_statusText.ShowWindow(SW_SHOW);
		} break;

		case MSG_CONNECTED:
			LogInfo("MSG_CONNECTED: OK");
			m_statusText.setWindowText(" ");
			if (m_statusText.IsWindowVisible() == SW_SHOW)
				m_statusText.ShowWindow(SW_HIDE);
			break;

		case MSG_COMPLETENM:
			LogInfo("MSG_COMPLETENM: OK");
			break;

		case MSG_UNPACKING: {
			STATUSU* stat = (STATUSU*)lp;
			int size = stat->nSubSizeComplete;
			if (m_unpackStarted == false)
				m_progress.SetPos(0);

			std::string text = stat->wcsStatus;

			LogDebug("MSG_UNPACKING: " << text);
			m_totalStatus.setWindowText(text.c_str());
			m_unpackStarted = true;
			LONG totalsize = static_cast<LONG>((sqrtf((float)stat->nTotalSizeComplete) / sqrtf((float)size)) * 100.0f);
			m_progress.SetPos(totalsize);
		} break;

		case MSG_UNPACKINGGZ: {
			STATUSU* stat = (STATUSU*)lp;
			int size = stat->nSubSizeComplete;
			if (m_unpackZStarted == false)
				m_progress.SetPos(0);

			std::string text = stat->wcsStatus;

			LogDebug("MSG_UNPACKINGGZ: " << text);
			m_totalStatus.setWindowText(text.c_str());
			m_unpackZStarted = true;
			LONG totalsize = static_cast<LONG>((sqrtf((float)stat->nTotalSizeComplete) / sqrtf((float)size)) * 100.0f);
			m_progress.SetPos(totalsize);
		} break;

		default:
			break;
	}
	m_statusText.Invalidate();

	return 0;
}

BOOL CProgressDlg::OnInitDialog() {
	baseclass::OnInitDialog();
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL) {
		CStringW strAboutMenu;
		strAboutMenu = L"&About...";
		if (!strAboutMenu.IsEmpty()) {
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	int result = SetTimer(TIMER_ID_PROGRESS, 500, NULL);
	// Set the icon for this dialog.  The framework does this automatically
	// when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE); // Set big icon
	SetIcon(m_hIcon, FALSE); // Set small icon

	SetTransparent(0);
	m_bkBrush.CreateSolidBrush(BACKGROUND_COLOR);

	m_progress.SetBkColor(PROGRESS_BAR_BKCOLOR);
	m_progress.SendMessage(PBM_SETBARCOLOR, 0, PROGRESS_BAR_COLOR);
	m_progress.SetRange(0, 100);
	m_progress.SetPos(0);

	m_fileProgress.SetBkColor(PROGRESS_BAR_BKCOLOR);
	m_fileProgress.SendMessage(PBM_SETBARCOLOR, 0, PROGRESS_BAR_COLOR);
	m_fileProgress.SetRange(0, 100);
	m_fileProgress.SetPos(0);

	CStringW s;
	s.Format(L"Updating ""%s"" ...", Utf8ToUtf16(m_gameName).c_str());
	SetWindowText(s);

	if (m_showdetail == s_ShowSimple) {
		m_itemStatus.ShowWindow(SW_HIDE);
		m_fileProgress.ShowWindow(SW_HIDE);
		m_itemByteCounter.ShowWindow(SW_HIDE);
		m_totalByteCounter.ShowWindow(SW_HIDE);
		m_downloadgameupdate.ShowWindow(SW_SHOW);

		//Also, move the visible windows up
		//The "current item" label is at the height we want to be.
		CRect topItem;
		m_itemStatus.GetWindowRect(&topItem);

		//What's the difference between the height of the "downloading updates"
		//and where we want to be?
		CRect labelRect;
		m_downloadgameupdate.GetWindowRect(&labelRect);

		int yDifference = labelRect.top - topItem.top;

		if (0 < yDifference) {
			//Move all the associated controls up by this amount
			EnumChildWindows(GetSafeHwnd(), &MoveWindows, yDifference);
		}
		CRect m_rect;
		GetClientRect(&m_rect);
		m_rect.DeflateRect(0, 25, 0, 0);
		MoveWindow(&m_rect);
	} else {
		m_itemStatus.ShowWindow(SW_HIDE);
		m_fileProgress.ShowWindow(SW_HIDE);
		m_itemByteCounter.ShowWindow(SW_HIDE);
		m_downloadgameupdate.ShowWindow(SW_SHOW);
		CRect m_rect;
		GetClientRect(&m_rect);
		m_rect.DeflateRect(0, 25, 0, 0);
		MoveWindow(&m_rect);
	}

	m_buttonplay.SetImageId(IDR_BUTTON_ENABLED, Enabled, false);
	m_buttonplay.SetImageId(IDR_BUTTON_DISABLED, Disabled, false);
	m_buttonplay.SetTextAttributes(FONT_NAME, 8, Enabled, RGB(0xFF, 0xFF, 0xFF), true);
	m_buttonplay.SetTextAttributes(FONT_NAME, 8, Disabled, RGB(0xFF, 0xFF, 0xFF), true);

	return TRUE;
}

void CProgressDlg::OnOK() {

	// Play Game ?
	if (!m_play) {
		LogWarn("GAME NOT FOR PLAY");
		PostQuitMessage(0);
		return;
	}

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));
	si.cb = sizeof(si);

	CStringA gamePath(m_gameInstallDir);
	if (gamePath.Right(1) != '\\')
		gamePath += "\\";
	gamePath += m_gameExe;

	CStringA cmdLine;
	cmdLine = "\"";
	cmdLine += gamePath;
	cmdLine += "\" ";

	CStringA cmdArgs;
	CStringA cmdLineFromPatcher;
	if (m_patcherController)
	{
		cmdLineFromPatcher = m_patcherController->GetLauncherCommandLine();
	}

	if (m_gamerunning) {
		LogInfo("GAME RUNNING - Messaging Client...");

		// args passed to running client only takes one param
		// -gu<url> | -cmd<cmdLine>
		if (cmdLineFromPatcher.GetLength() > 0) {
			cmdArgs += "-cmd";
			cmdArgs += cmdLineFromPatcher;
		} else {
			// no need to quote param
			cmdArgs += "-gu";
			cmdArgs += m_Url;
		}

		HWND clientHwnd = kaneva_ClientFindWindow();
		if (clientHwnd != INVALID_HANDLE_VALUE) {
			COPYDATASTRUCT patch_cds;
			patch_cds.cbData = cmdArgs.GetLength() + 1;
			patch_cds.dwData = 1;
			patch_cds.lpData = cmdArgs.GetBuffer();

			if (::IsIconic(clientHwnd))
				::ShowWindow(clientHwnd, SW_RESTORE);

			// Give it focus
			::SetWindowPos(clientHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
			::SendMessage(clientHwnd, WM_COPYDATA, 0, (LPARAM)&patch_cds);
			cmdArgs.ReleaseBuffer();
		} else {
			LogError("ClientFindWindow(2) FAILED");
			AfxMessageBox(L"Failed Finding Client Window", MB_ICONERROR);
			GetParent()->PostMessage(WM_ALLOWCLOSEPATCHER, 0, 0);
		}

		PostQuitMessage(0);
		return;
	}
	
	LogInfo("GAME NOT RUNNING - Launching Client...");

	// args passed to launched client
	// -gu<url> [-try<id>:<type>]
//	cmdArgs += "\"-gu";
//	cmdArgs += m_Url;
//	cmdArgs += "\"";

	if (cmdLineFromPatcher.GetLength() > 0) {
		cmdArgs += " -cmd";
		cmdArgs += cmdLineFromPatcher;
	}

	CStringA extraArgs;
	extraArgs.Format("\"-k%s\"", m_defaultPatchUrl);
	cmdLine += cmdArgs;
	cmdLine += extraArgs;

	// Launch Client
	CStringW cmdLineW = Utf8ToUtf16(cmdLine).c_str();
	CStringW installDirW = Utf8ToUtf16(m_gameInstallDir).c_str();
	LogInfo("Launching '" << cmdLine << "' ...");
	if (!::CreateProcessW(
			NULL,
			cmdLineW.GetBuffer(),
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			installDirW.GetBuffer(),
			&si,
			&pi)) {
		std::string msg;
		StrBuild(msg, "Failed Launching '" << gamePath.GetBuffer() << "'");
		::MessageBoxA(::GetActiveWindow(), msg.c_str(), "Failed Launching", MB_OK | MB_ICONEXCLAMATION);
		LogError(msg);
		GetParent()->PostMessage(WM_ALLOWCLOSEPATCHER, 0, 0);
	} else {
		LogInfo("OK");
	}

	ShowWindow(SW_HIDE);
	::CloseHandle(pi.hProcess);
	::CloseHandle(pi.hThread);

	PostQuitMessage(0);
}

void CProgressDlg::OnCancel() {
	if (!m_complete) {
		std::string msg = "Are you sure you want to cancel the update?  You may not be able to run the game.";
		if (::MessageBoxA(::GetActiveWindow(), msg.c_str(), "Confirm", MB_YESNO | MB_ICONQUESTION) == IDNO)
			return;
		CancelPatch();
		PostQuitMessage(-1);
	} else {
		std::string msg = "Are you sure you want to exit?";
		if (::MessageBoxA(::GetActiveWindow(), msg.c_str(), "Confirm", MB_YESNO | MB_ICONQUESTION) == IDNO)
			return;
		PostQuitMessage(-1);
	}
}

void CProgressDlg::OnBnClickedPlay() {
	UpdateData();
	m_play = m_complete;
	OnOK();
}

void CProgressDlg::OnSysCommand(UINT nID, LPARAM lParam) {
	baseclass::OnSysCommand(nID, lParam);
}

void CProgressDlg::UpdateStatus()
{
	CStringA s;
	STATUSU stat;

	m_patcherController->HTTPGetStatus(stat);
	s = stat.wcsStatus;
	if (s.GetLength() > 0)
		m_statusText.setWindowText(s);

	LogDebug("MSG_STATUS: " << s.GetBuffer());

	int totalPercent = 0;
	int subPercent = 0;
	float ctime = 0;
	if (stat.nTotalSizeComplete > 0) {
		if (m_startTime != 0) {
			ctime = (float)((GetTickCount() - m_startTime) * .001f);
		}
		totalPercent = static_cast<int>((sqrtf((float)stat.nTotalSizeComplete) / sqrtf((float)stat.nTotalSize)) * 100.0f);
		TimerInfo info;
		info.downloadsize = stat.nTotalSize;
		info.estimatedtime = static_cast<unsigned long>(ctime);
		GetParent()->SendMessage(WM_APPSTATUS, m_complete ? 1 : 0, (LPARAM)&info);
	}
	else if (stat.nTotalSizeComplete == MAX_VAL) {
		if (m_startTime != 0) {
			ctime = (float)((GetTickCount() - m_startTime) * .001f);
		}
		totalPercent = 100;
		m_sTotalUpdates = "Computing estimated downloading time.";
		TimerInfo info;
		info.downloadsize = stat.nTotalSize;
		info.estimatedtime = static_cast<unsigned long>(ctime);
		GetParent()->SendMessage(WM_APPSTATUS, 1, (LPARAM)&info);
		GetParent()->SendMessage(WM_CLOSEPATCH, 1, (LPARAM)&info);
	}

	if (stat.nTotalSizeComplete != NOT_SET)
		m_progress.SetPos(totalPercent);

	if (stat.nTotalSizeComplete > 0) {
		FormatFileProgress(m_sTotalSize, stat.nTotalSizeComplete, m_totalSize);

		m_totalStatus.Invalidate();
		m_totalByteCounter.Invalidate();
		char buffer[MAX_PATH];
		if (totalPercent > 0)
			sprintf_s(buffer, _countof(buffer), "Completed: %d%%", totalPercent);
		else
			sprintf_s(buffer, _countof(buffer), "Scanning downloaded files.");

		m_sTotalUpdates.SetString(buffer);
	}

	if (stat.nSubSizeComplete != NOT_SET)
		m_fileProgress.SetPos(subPercent);

	UpdateData(FALSE);
}

void CProgressDlg::OnTimer(UINT_PTR nIDEvent) 
{
	CKanevaDialogSkin::OnTimer(nIDEvent);
	if (!m_DisplayReady)
	{
		m_DisplayReady = true;

		//Begin the Download Thread.
		if (m_patcherController)
			m_patcherController->BeginPatchThread();
	}
	else
	{
		UpdateStatus();
		if (!m_complete)
		{
			if (m_patcherController && m_patcherController->IsCompleted())
			{
				m_complete = true;

				if (m_playOnComplete != BST_CHECKED) {
					GetDlgItem(IDC_PLAYCHECK)->ShowWindow(SW_HIDE);
					m_downloadgameupdate.setWindowText("");
					m_totalStatus.setWindowText(" ");
					m_totalStatus.ShowWindow(SW_HIDE);
					m_progress.ShowWindow(SW_HIDE);
					RECT r;
					GetClientRect(&r);
					GetParent()->InvalidateRect(&r);
					m_buttonplay.ShowWindow(SW_SHOW);
					m_buttonplay.EnableWindow();
				}
				else {
					m_play = true;
					OnOK();
				}
			}
		}
	}
}

void CProgressDlg::OnPaint() {
	if (IsIconic()) {
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	} else {
		baseclass::OnPaint();
	}
}

HCURSOR CProgressDlg::OnQueryDragIcon() {
	return static_cast<HCURSOR>(m_hIcon);
}

HBRUSH CProgressDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = baseclass::OnCtlColor(pDC, pWnd, nCtlColor);

	switch (nCtlColor) {
		case CTLCOLOR_BTN:
			return static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));

		case CTLCOLOR_STATIC: 
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(0xff, 0xff, 0xff));
			pDC->SetBkColor(RGB(0, 0, 0));
			return static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));

		case CTLCOLOR_MSGBOX:
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(TEXT_COLOR);
			pDC->SetBkColor(BACKGROUND_COLOR);
			return (HBRUSH)GetStockObject(WHITE_BRUSH);

		case CTLCOLOR_DLG:
			if (IsTransparent())
				return static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));
			else
				return static_cast<HBRUSH>(m_bkBrush.GetSafeHandle());
			break;

		default:
			break;
	}

	return hbr;
}

void CProgressDlg::OnDestroy() {
	baseclass::OnDestroy();
	m_bkBrush.DeleteObject();
}

void CProgressDlg::FormatFileProgress(CStringA& dest, LONG curSize, LONG totalSize) {
	CStringA sCurSizePrefix = "KB";
	CStringA sTotalSizePrefix = "KB";
	double curSizeScaled = (double)curSize / 1024.0f;
	double totalSizeScaled = (double)totalSize / 1024.0f;

	if (curSizeScaled >= 1024.0f) {
		curSizeScaled = curSizeScaled / 1024.0f;
		sCurSizePrefix = "MB";
	}

	if (totalSizeScaled >= 1024.0f) {
		totalSizeScaled = totalSizeScaled / 1024.0f;
		sTotalSizePrefix = "MB";
	}

	dest.Format("%.1f %s / %.1f %s", curSizeScaled, sCurSizePrefix, totalSizeScaled, sTotalSizePrefix);

	//Make sure to update the control...
	m_itemByteCounter.Invalidate();
}

CStringA CProgressDlg::GetRemainingText(double lfPercent, double lfSecsRemaining) {
	CStringA str;
	int nSeconds = (int)fmod(lfSecsRemaining, 60.0);

	if (lfSecsRemaining <= 60 && lfSecsRemaining > 0) {
		CStringA sFormat;
		sFormat = "%d second%s remaining";
		str.Format(sFormat, nSeconds, nSeconds == 1 ? "" : "s");
	} else {
		int nMinutes = (int)(lfSecsRemaining / 60.0);
		CStringA sFormat;
		sFormat = "%d minute%s, %d second%s remaining";

		if (nMinutes == 0 && nSeconds == 0)
			return "";

		str.Format(sFormat, nMinutes, nMinutes == 1 ? "" : "s", nSeconds, nSeconds == 1 ? "" : "s");
	}
	return str;
}

BOOL CProgressDlg::SetTransparent(BYTE bAlpha) {
	m_downloadgameupdate.SetBackgroundAlpha(bAlpha);
	m_itemStatus.SetBackgroundAlpha(bAlpha);
	m_totalStatus.SetBackgroundAlpha(bAlpha);
	m_itemByteCounter.SetBackgroundAlpha(bAlpha);
	m_totalByteCounter.SetBackgroundAlpha(bAlpha);
	m_statusText.SetBackgroundAlpha(bAlpha);
	return baseclass::SetTransparent(bAlpha);
}

BOOL CProgressDlg::SetTransparentColor(COLORREF col, BOOL bTrans) {
	m_downloadgameupdate.SetBackgroundTransparentColor(col, bTrans == TRUE);
	m_itemStatus.SetBackgroundTransparentColor(col, bTrans == TRUE);
	m_totalStatus.SetBackgroundTransparentColor(col, bTrans == TRUE);
	m_itemByteCounter.SetBackgroundTransparentColor(col, bTrans == TRUE);
	m_totalByteCounter.SetBackgroundTransparentColor(col, bTrans == TRUE);
	m_statusText.SetBackgroundTransparentColor(col, bTrans == TRUE);
	return baseclass::SetTransparentColor(col, bTrans);
}

void CProgressDlg::OnBnClickedPlaycheck() {
	UpdateData();
}

void CProgressDlg::OnNMCustomdrawFileProgress(NMHDR* pNMHDR, LRESULT* pResult) {
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	*pResult = 0;
}

} // namespace KEP