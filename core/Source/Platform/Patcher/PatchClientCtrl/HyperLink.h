#pragma once
// CHyperLink
class CHyperLink : public CStatic {
protected:
	virtual void PreSubclassWindow();
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	CHyperLink();
	CHyperLink(bool underline, bool italic, int size = 8);
	virtual ~CHyperLink();
	bool m_over;
	HCURSOR m_cursor;
	CFont m_underlineFont;
	CBrush m_bkBrush;
	afx_msg void OnClicked();

	void SetURL(CString s) { m_link = s; }

	void SetUnderline(bool underline) { m_underline = underline; }

	void SetColor(unsigned char red,
		unsigned char green,
		unsigned char blue) {
		rgb[0] = red;
		rgb[1] = green;
		rgb[2] = blue;
	}

	void SetItalic(bool italic) { m_italic = italic; }

	void SetFontSize(int size) { m_size = size; }

protected:
	CString m_link;
	unsigned char rgb[3];
	bool m_underline;
	bool m_italic;
	int m_size;
};
