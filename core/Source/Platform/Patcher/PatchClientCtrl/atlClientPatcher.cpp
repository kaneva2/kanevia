///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "atlClientPatcher.h"
#include <CommDlg.h>
#include <js.h>
#include <comdef.h>
#include <jsEnumFiles.h>
#include <StpUrl.h>
#include "version.h"
#include "../PatchLib/ConfigValues.h"
#include "../KanevaLauncher/KanevaLauncherDlg.h"
#include "Common/KEPUtil/Helpers.h"
#include "KanevaErrorFile.h"

#define PLAY_ON_UPDATE "PlayOnUpdate"

namespace KEP 
{

	static LogInstance("Instance");

	/** DRF
	* Returns client window handle if running, otherwise INVALID_HANDLE_VALUE.
	*/
	HWND kaneva_ClientFindWindow() {
		HWND hwnd = FindWindow(K_CLIENT_WINDOW_NAME, NULL);
		return (hwnd == NULL) ? (HWND)INVALID_HANDLE_VALUE : hwnd;
	}

	/** DRF
	* Switches desktop focus to client window if found and brings it to the top.
	*/
	void kaneva_SwitchToClientWindow() {
		HWND hwnd = kaneva_ClientFindWindow();
		if (hwnd != (HWND)INVALID_HANDLE_VALUE)
			SwitchToThisWindow(hwnd, FALSE);
	}
}
