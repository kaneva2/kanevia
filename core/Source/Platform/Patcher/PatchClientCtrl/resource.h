//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PatchClientCtrl.rc
//
#define IDM_ABOUTBOX                    0x10
#define IDS_PROJNAME                    100
#define IDC_PLAY                        3001
#define IDD_ABOUTBOX                    3003
#define IDR_PATCHCLIENTCTRL             3004
#define IDR_CLIENTPATCHER               3005
#define IDR_CLIENTPATCHER2              3006
#define IDC_PROGRESS                    3008
#define IDD_PROGRESS                    3009
#define IDC_STATUS_TEXT                 3010
#define IDC_FILE_PROGRESS               3011
#define IDC_PLAYCHECK                   3012
#define IDR_MAINFRAME                   3014
#define IDD_ICON                        3015
#define IDC_TOTAL_SIZE                  3016
#define IDC_SUB_SIZE                    3017
#define IDC_TOTAL_UPDATES               3018
#define IDC_RICHEDIT_LICENSE            3019
#define IDC_GAME_UPDATE                 3020
#define IDC_ITEM_UPDATE                 3021
#define IDD_LICENSE                     3022
#define IDD_PROGRESS2                   3024
#define IDD_PROGRESS_MINI               3025
#define IDS_BASE_REG_KEY                3029
#define IDS_DOWNLOADING_PATCH           3030
#define IDS_DOWNLOADED                  3031
#define IDS_CONFIRM_CANCEL              3032
#define IDS_COMPLETE                    3033
#define IDS_ERROR                       3034
#define IDS_NOT_CONNECT                 3035
#define IDS_TIMEOUT                     3036
#define IDS_CANT_WRITE_REG_KEY          3037
#define IDS_NEW_GAME_FOLDER             3038
#define IDS_NEW_INSTALL                 3039
#define IDS_UNKNOWN_MSG                 3040
#define IDS_CLOSE                       3041
#define IDS_CANT_START_GAME             3042
#define IDS_NEWPATCHER_AVAILABLE        3043
#define IDS_CANT_EDIT_CONDAT            3044
#define IDS_BAD_URL                     3045
#define IDS_CANT_CREATE_KEY             3046
#define IDS_CANT_ACCESS_SPECIAL_FOLDER  3047
#define IDS_TOTAL_UPDATES               3048
#define IDS_TOTAL_UPDATES_EST_TIME      3049
#define IDS_LESS_THAN_A_SECOND          3051
#define IDS_SECONDS_REMAINING           3053
#define IDS_MINS_AND_SECS_REMAINING     3054
#define IDS_GAME_ALREADY_RUNNING        3056
#define IDS_GAME_WINDOW_NOT_FOUND       3057
#define IDS_PATCHER_ALREADY_RUNNING     3058
#define IDS_CONNECTION_LOST             3059
#define IDS_LAUNCHER_NOT_FOUND          3060
#define IDS_ABOUTBOX                    3061
#define IDS_UNPACKING                   3062
#define IDC_RETRY                       3063
#define IDC_CANCELS                     3064
#define IDR_URLPATCHER                  3065
#define IDS_WOK_INSTALL_KEY_NOT_FOUND   3066

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        3062
#define _APS_NEXT_COMMAND_VALUE         3063
#define _APS_NEXT_CONTROL_VALUE         3066
#define _APS_NEXT_SYMED_VALUE           3067
#endif
#endif
