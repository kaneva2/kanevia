#include "stdafx.h"
#include "HyperLink.h"

#define IDC_HAND 32649

CHyperLink::CHyperLink() {
	m_over = false;
	m_link = "";
	SetColor(0, 0, 0xff);
	m_underline = false;
	m_italic = false;
	m_size = 8;
}

CHyperLink::CHyperLink(bool underline, bool italic, int size) {
	m_over = false;
	m_link = "";
	SetColor(0, 0, 0xff);
	m_underline = underline;
	m_italic = italic;
	m_size = size;
}

CHyperLink::~CHyperLink() {
	m_underlineFont.DeleteObject();
}

BEGIN_MESSAGE_MAP(CHyperLink, CStatic)
ON_WM_CTLCOLOR_REFLECT()
ON_WM_ERASEBKGND()
ON_WM_MOUSEMOVE()
ON_CONTROL_REFLECT(STN_CLICKED, OnClicked)
END_MESSAGE_MAP()

void CHyperLink::PreSubclassWindow() {
	DWORD dwstyle = GetStyle();
	::SetWindowLong(GetSafeHwnd(), GWL_STYLE, dwstyle | SS_NOTIFY);

	m_cursor = LoadCursor(NULL, MAKEINTRESOURCE(IDC_HAND));

	CFont* font = GetFont();

	LOGFONT lg;
	font->GetLogFont(&lg);

	lg.lfUnderline = m_underline;
	lg.lfItalic = m_italic;
	lg.lfHeight = -MulDiv(m_size, GetDeviceCaps(::GetDC(this->GetSafeHwnd()), LOGPIXELSY), 72);

	m_underlineFont.CreateFontIndirect(&lg);
	SetFont(&m_underlineFont);

	CStatic::PreSubclassWindow();
}

void CHyperLink::OnClicked() {
	if (m_underline)
		::ShellExecute(0, _T("open"), m_link, 0, 0, SW_SHOWNORMAL);
}

HBRUSH CHyperLink::CtlColor(CDC* pDC, UINT nCtlColor) {
	pDC->SetTextColor(RGB(rgb[0], rgb[1], rgb[2]));
	return (HBRUSH)GetStockObject(NULL_BRUSH);
}

BOOL CHyperLink::OnEraseBkgnd(CDC* pDC) {
	CRect rect;
	GetClientRect(&rect);
	pDC->FillSolidRect(rect, ::GetSysColor(COLOR_3DFACE));
	return TRUE;
}

void CHyperLink::OnMouseMove(UINT nFlags, CPoint point) {
	//Set the mouse cursor to the hand cursor.
	if (m_underline)
		::SetCursor(m_cursor);
}
