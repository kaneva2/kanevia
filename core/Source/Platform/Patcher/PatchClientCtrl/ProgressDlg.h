///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "afxcmn.h"
#include "afxwin.h"

#include <vector>

#include "..\PatchLib\PatchLibGlobals.h"

#include "resource.h"

#include "CKanevaButton.h"
#include "KanevaDialogSkin.h"
#include "StaticEx.h"
#include "imagelib\imagebutton.h"

namespace KEP {

class CLoginProgressDlg;

class CProgressDlg : public CKanevaDialogSkin {
	typedef CKanevaDialogSkin baseclass;
	DECLARE_DYNAMIC(CProgressDlg)

public:
	CProgressDlg(PatchClientController* ctrl,
		const char* gameExe,
		const char* gameName,
		const char* URL,
		const char* gameInstallDir,
		int onComplete,
		bool gamerunning,
		const char* showdetail,
		LONG zoneindex,
		CLoginProgressDlg* loginProgressDlg,
		CWnd* pParent = NULL);

	virtual ~CProgressDlg();

	bool ShouldPlay() const { return m_play; }
	int PlayOnComplete() const { return m_playOnComplete; }

	// Dialog Data
	enum { IDD = IDD_PROGRESS2 };

protected:
	HICON m_hIcon;
	CBrush m_bkBrush;
	CStringA m_gameExe;
	CStringA m_gameInstallDir;
	PatchClientController *m_patcherController;
	PatchInfo* m_currentPatchInfo;
	CStringA m_gameName;
	CStringA m_sTotalSize; // label
	CStringA m_sSubSize; // label
	CStringA m_sTotalUpdates;
	CStringA m_showdetail;
	CStringA m_Url;
	CStringA m_defaultPatchUrl;
	LONG m_totalFiles;
	LONG m_totalSize;
	LONG m_totalSubSize;
	LONG m_zoneindex;
	bool m_update;
	bool m_complete;
	bool m_play;
	bool m_gamerunning;
	unsigned long m_startTime;
	clock_t m_start;

	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	void FormatFileProgress(CStringA& dest, LONG curSize, LONG totalSize);
	CStringA GetRemainingText(double lfPercent, double lfSecsRemaining);

	virtual BOOL SetTransparent(BYTE bAlpha);
	virtual BOOL SetTransparentColor(COLORREF col, BOOL bTrans = TRUE);

	static const char* s_ShowSimple;
	void UpdateStatus();

	afx_msg LRESULT OnStatusMessage(WPARAM wp, LPARAM lp);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedPlay();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedPlaycheck();
	void GetSubDirectory();

protected:
	virtual void OnCancel();
	virtual void OnOK();

public:
	CProgressCtrl m_fileProgress;
	CProgressCtrl m_progress;
	int m_playOnComplete;

	CImageButton m_buttonplay;
	CImageButton m_buttoncancel;
	afx_msg void OnBnClickedCancel();
	CStaticEx m_downloadgameupdate;
	CStaticEx m_itemStatus;
	CStaticEx m_totalStatus;
	CStaticEx m_itemByteCounter;
	CStaticEx m_totalByteCounter;
	CStaticEx m_statusText;
	//void ClearPatcherMemory();
	afx_msg void OnNMCustomdrawFileProgress(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedAuto();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedRetry();
	afx_msg void OnBnClickedCancels();

private:
	bool m_patchStarted;
	bool m_unpackStarted;
	bool m_unpackZStarted;
	bool m_firstPatch;
	bool m_DisplayReady;
	CLoginProgressDlg* m_loginProgressDlg;
};

} // namespace KEP