#pragma once

#include "KEPCommon.h"
#include "KEPHelpers.h"
#include "..\PatchLib\PatchLibGlobals.h"

namespace KEP {

	class KEPUTIL_EXPORT PatchClientController
	{
	public:
		KEPUTIL_EXPORT enum PatcherState
		{
			Initializing,
			Patching,
			UpToDate,
			FinishedPatching,
			ErrorPatcherAlreadyRunning,
			Error
		};

		long GetGameId() const { return m_GameId; }
		const char* GetStpUrl() const { return m_StpUrl.c_str(); }
		const char* GetTryOn() const { return m_tryOn.c_str(); }
		const char* GetEmail() const { return m_email.c_str(); }
		const char* GetPatchUrlOverride() const { return m_patchUrlOverride.c_str(); }
		const char* GetLauncherCommandLine() const { return m_cmdLine.c_str(); }

		void ClearPatcherMemory();
		void HTTPGetStatus(STATUSU &status);
		bool IsCompleted() const;
		bool BeginPatchThread();
	private:
		PatchClientController();
		~PatchClientController();

		bool Init(long gameId, const char* stpUrl, const char* tryOn,
			const char* email, const char* patchUrlOverride, const char* cmdLine,
			std::vector<PatchInfo>& parentPatches, std::vector<PatchInfo>& childPatches);
	public:
		KEPUTIL_EXPORT static PatcherState PatchAndPlayAsync(PatchClientController* &outController, long gameId, const char* stpUrl, const char* tryOn, 
			const char* email, const char* patchUrlOverride, const char* cmdLine, std::vector<PatchInfo>& parentPatches, std::vector<PatchInfo>& childPatches);
		// Ask the patcher to cancel patching.  This can be called from any thread.
		KEPUTIL_EXPORT bool Cancel();
		// Get the version number of the patcher.  (Lets increment this from 4 to 5).  This can be called from any thread.
		KEPUTIL_EXPORT char * Version();
		// Get the current patching state.  This can be called from any thread.
		KEPUTIL_EXPORT char * GetMetrics();
		// Get the current state
		KEPUTIL_EXPORT PatcherState GetState();
	private:
		log4cplus::Logger m_logger;
		PatchProcess m_patchProcess;
		PatchInfo* m_currentPatchInfo;
		std::vector<PatchInfo> m_parentPatches;
		std::vector<PatchInfo> m_childPatches;
		size_t m_parentPatchIndex;
		size_t m_childPatchIndex;

		long m_GameId;
		std::string m_StpUrl;
		std::string m_tryOn;
		std::string m_email;
		std::string m_patchUrlOverride;
		std::string m_cmdLine;
		mutable Mutex m_initMutex;
	};

}