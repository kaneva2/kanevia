///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "Common/KEPUtil/Helpers.h"
#include "KanevaErrorFile.h"
#include "LogHelper.h"
#include "../PatchLib/ConfigValues.h"

#include "PatchClientCtrl.h"

/** DRF
* Returns client window handle if running, otherwise INVALID_HANDLE_VALUE.
*/
HWND kaneva_ClientFindWindow() {
	HWND hwnd = FindWindow(K_CLIENT_WINDOW_NAME, NULL);
	return (hwnd == NULL) ? (HWND)INVALID_HANDLE_VALUE : hwnd;
}

/** DRF
* Switches desktop focus to client window if found and brings it to the top.
*/
void kaneva_SwitchToClientWindow() {
	HWND hwnd = kaneva_ClientFindWindow();
	if (hwnd != (HWND)INVALID_HANDLE_VALUE)
		SwitchToThisWindow(hwnd, FALSE);
}

namespace KEP {

	extern bool DoPatchExe(PatchProcess* pPP);

	static PatchInfo* BestPatchInfo(std::vector<PatchInfo>& v, size_t& id) {
		if (v.size() == 0)
			return NULL;
		PatchInfo* pPI = NULL;
		TimeMs curTimeMs = (id < v.size()) ? v[id].serverStatus.m_timeMs : 0.0;
		size_t nxtId = 0;
		TimeMs nxtTimeMs = 10000000.0;
		for (size_t i = 0; i < v.size(); ++i) {
			if (!v[i].serverStatus.m_valid)
				continue;
			TimeMs timeMs = v[i].serverStatus.m_timeMs;
			if ((timeMs > curTimeMs) && (timeMs < nxtTimeMs)) {
				nxtId = i;
				nxtTimeMs = timeMs;
				pPI = &v[i];
			}
		}
		id = nxtId;
		return pPI;
	}

	PatchClientController::PatchClientController()
	{
		m_GameId = 0;
		m_currentPatchInfo = NULL;
	}

	PatchClientController::~PatchClientController()
	{

	}

	void PatchClientController::ClearPatcherMemory() {
		m_patchProcess.ClearUrls();
	}

	PatchClientController::PatcherState PatchClientController::PatchAndPlayAsync(
		PatchClientController* &outController, 
		long gameId, 
		const char* stpUrl, 
		const char* tryOn, 
		const char* email, 
		const char* patchUrlOverride, 
		const char* cmdLine, 
		std::vector<PatchInfo>& parentPatches, 
		std::vector<PatchInfo>& childPatches
	) {
		outController = NULL;
		// clear this to make sure we don't overwrite existing controller.  Caller should make sure the out param pointer doesn't still hold anything

		// Patcher Already Running ?
		HANDLE patcherSem = ::CreateSemaphoreW(NULL, 1L, INT_MAX, K_PATCHER_SEMAPHORE_NAME);
		bool patcherRunning = (GetLastError() == ERROR_ALREADY_EXISTS);
		if (patcherRunning)
		{
			if (patcherSem)
			{
				::CloseHandle(patcherSem);
			}
			return ErrorPatcherAlreadyRunning;
		}
		outController = new PatchClientController();

		bool bInitResult = outController->Init(gameId, stpUrl, tryOn, email, patchUrlOverride, cmdLine, parentPatches, childPatches);
		if (!bInitResult)
		{
			delete outController;
			outController = NULL;
			return Error;
		}
		else
		{
			return Initializing;
		}
	}

	bool PatchClientController::Init(
		long gameId, 
		const char* stpUrl, 
		const char* tryOn, 
		const char* email, 
		const char* patchUrlOverride, 
		const char* cmdLine,
		std::vector<PatchInfo>& parentPatches, 
		std::vector<PatchInfo>& childPatches
	) {
		MutexGuardLock lock(m_initMutex);

		m_GameId = gameId;
		m_StpUrl = stpUrl;
		if (tryOn != NULL)
		{
			m_tryOn = tryOn; // we can probably kill this feature
		}
		if (email != NULL)
		{
			m_email = email;
		}
		if (patchUrlOverride != NULL)
		{
			m_patchUrlOverride = patchUrlOverride;
		}
		if (cmdLine != NULL)
		{
			m_cmdLine = cmdLine;
		}

		ConfigValues cfg;
		cfg.LoadValues(gameId);
		std::string gamePath = cfg.m_gamePath;

		static LogInstance("Instance");
		LogInfo("gamePath='" << gamePath << "'");
		LogInfo("stpUrl='" << stpUrl << "'");
		LogInfo("patchUrlOverride='" << m_patchUrlOverride << "'");

		// Parent Patches First Then Child Patches
		// Once all parent patches are complete in OnStatusMessage(MSG_COMPLETENM) then the
		// child patches will begin until all of those are also complete.
		m_parentPatches.insert(m_parentPatches.end(), parentPatches.begin(), parentPatches.end());
		m_parentPatchIndex = m_parentPatches.size();
		m_childPatches.insert(m_childPatches.end(), childPatches.begin(), childPatches.end());
		m_childPatchIndex = m_childPatches.size();

		// Begin With Best Parent Patch First
		m_currentPatchInfo = BestPatchInfo(m_parentPatches, m_parentPatchIndex);
		if (!m_currentPatchInfo)
			return false;

		// Set all the parameters
		strcpy_s(m_patchProcess.ClientString, _countof(m_patchProcess.ClientString), "unused");

		m_patchProcess.patchInfo = *m_currentPatchInfo;
		m_patchProcess.ClearUrls();

		// Populate Patch LocalDir & Patch Urls
		m_patchProcess.localFilePath = m_currentPatchInfo->patchPath.empty() ? gamePath : PathAdd(gamePath, m_currentPatchInfo->patchPath);
		m_patchProcess.AddUrl(m_currentPatchInfo->patchUrl);

		m_patchProcess.zoneindex = 0; // TODO support zoneIndex
		m_patchProcess.gameId = gameId;

		return true;
	}

	bool PatchClientController::BeginPatchThread()
	{
		MutexGuardLock lock(m_initMutex);

		// Spawns HttpWorker::PatchThread()
		return DoPatchExe(&m_patchProcess);
	}

	void PatchClientController::HTTPGetStatus(STATUSU &status)
	{
		m_patchProcess.HTTPGetStatus(status);
	}

	bool PatchClientController::IsCompleted() const
	{
		return m_patchProcess.IsCompleted();
	}

	bool PatchClientController::Cancel()
	{
		return false;
	}

	char * PatchClientController::Version()
	{
		return "5";
	}

	char * PatchClientController::GetMetrics()
	{
		return NULL;
	}

	PatchClientController::PatcherState PatchClientController::GetState()
	{
		return Error;
	}

}