//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by KLauncherLib.rc
//
#define IDC_PLAY                        3001
#define IDC_PROGRESS                    3008
#define IDC_STATUS_TEXT                 3010
#define IDC_FILE_PROGRESS               3011
#define IDC_PLAYCHECK                   3012
#define IDC_TOTAL_SIZE                  3016
#define IDC_SUB_SIZE                    3017
#define IDC_TOTAL_UPDATES               3018
#define IDC_GAME_UPDATE                 3020
#define IDC_ITEM_UPDATE                 3021
#define IDD_PROGRESS_MINI               3025
#define IDD_KANEVALAUNCHER_DIALOG       4000
#define IDC_VERSION                     4001
#define IDR_HTML_ERRORREPORT            4002
#define IDD_USERNAME_AND_PASSWORD_DIALOG 4003
#define IDD_INVALID_PASSWORD_DIALOG     4004
#define IDD_CUSTOM_MESSAGEBOX_DIALOG    4005
#define IDD_REPORT_DIALOG               4006
#define IDD_SUPPORT_DIALOG              4007
#define IDC_BROWSER                     4008
#define IDD_LOGIN_PROGRESS_DIALOG       4008
#define IDC_BROWSER2                    4009
#define IDD_LOGIN_PROGRESS_MINI_DIALOG  4009
#define IDC_EMAIL                       4010
#define IDC_COMMENTS                    4011
#define IDC_PASSWORD                    4012
#define IDC_KANEVAWEBSITE               4013
#define IDC_SIGNING                     4014
#define IDC_TECH                        4015
#define IDC_BUTTON1                     4016
#define IDC_EXIT                        4017
#define IDC_FORUM                       4018
#define IDC_BUTTON2                     4019
#define IDC_FORGOTPASSWORD              4020
#define IDC_FORUM3                      4021
#define IDC_NOTAMEMBERYET               4022
#define IDC_STATIC_EMAIL                4023
#define IDC_STATIC_PASSWORD             4024
#define IDC_STATIC_DOTS                 4024
#define IDC_BUTTON3                     4025
#define IDC_REMAKE                      4026
#define IDC_TEXT1                       4027
#define IDC_TEXT2                       4028
#define IDC_TEXT                        4029
#define IDC_GroupBox                    4030
#define IDC_CREATEONE                   4031
#define IDC_PROGRESS1                   4032
#define IDC_ERROR_REPORT                4033
#define IDC_LOGO                        4034
#define IDC_QUITLAUNCHER                4035
#define IDC_BROWSERHEADER1              4036
#define IDC_BROWSERHEADER2              4037
#define IDS_DEFAULT_PROGRESS_MSG        4039
#define IDC_SCROLLBAR1                  4040
#define IDS_LAUNCHING_CLIENT            4040
#define IDC_STATIC_TXT                  4041
#define IDS_Test                        4041
#define IDC_STATIC_CONNECT              4042

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        4041
#define _APS_NEXT_COMMAND_VALUE         4039
#define _APS_NEXT_CONTROL_VALUE         4043
#define _APS_NEXT_SYMED_VALUE           4041
#endif
#endif
