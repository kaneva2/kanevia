///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CContainmentClass.h"

namespace KEP {

CContainmentObject::CContainmentObject() {
	m_containmentBasis = 0; //0=NONE 1=resource
	m_miscType = 0;
	m_respawnTime = 20000;
	m_lastRespawnTime = 20000;
	m_currentAmount = 10;
	m_maxAmount = 10;
	m_dispersalAmount = 1;
}

int CContainmentObject::UseIt() {
	if (m_currentAmount < m_maxAmount) {
		KEP_ASSERT(m_lastRespawnTime >= 0 && m_respawnTime >= 0);
		if (fTime::ElapsedMs(m_lastRespawnTime) > m_respawnTime) {
			m_currentAmount = m_maxAmount;
			m_lastRespawnTime = fTime::TimeMs();
		}
		if (m_currentAmount >= m_dispersalAmount) {
			m_currentAmount -= m_dispersalAmount;
			return m_dispersalAmount;
		}
		if (m_currentAmount > 0) {
			m_currentAmount = 0;
			return m_currentAmount;
		}
		return 0;
	}

	if (m_currentAmount == m_maxAmount) { //full
		m_lastRespawnTime = fTime::TimeMs();
		m_currentAmount -= m_dispersalAmount;

		if (m_currentAmount >= m_dispersalAmount) {
			return m_dispersalAmount;
		}
		if (m_currentAmount > 0) {
			return m_currentAmount;
		}
	} //end full
	return 0;
}

void CContainmentObject::Clone(CContainmentObject** copy) {
	CContainmentObject* copyLocal = new CContainmentObject();
	copyLocal->m_containmentBasis = m_containmentBasis;
	copyLocal->m_currentAmount = m_currentAmount;
	copyLocal->m_dispersalAmount = m_dispersalAmount;
	copyLocal->m_maxAmount = m_maxAmount;
	copyLocal->m_miscType = m_miscType;
	copyLocal->m_respawnTime = m_respawnTime;
	copyLocal->m_lastRespawnTime = 0;
	*copy = copyLocal;
}

void CContainmentObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_containmentBasis
		   << m_miscType
		   << m_respawnTime
		   << (int)m_lastRespawnTime
		   << m_currentAmount
		   << m_maxAmount
		   << m_dispersalAmount;

	} else {
		int spawnTime;
		ar >> m_containmentBasis >> m_miscType >> m_respawnTime >> spawnTime >> m_currentAmount >> m_maxAmount >> m_dispersalAmount;
		m_lastRespawnTime = spawnTime;
	}
}

void CContainmentObjectList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CContainmentObject* cntObject = (CContainmentObject*)GetNext(posLoc);
		delete cntObject;
		cntObject = 0;
	}
	RemoveAll();
}

IMPLEMENT_SERIAL(CContainmentObject, CObject, 0)
IMPLEMENT_SERIAL(CContainmentObjectList, CObList, 0)

BEGIN_GETSET_IMPL(CContainmentObject, IDS_CONTAINMENTOBJECT_NAME)
GETSET_RANGE(m_containmentBasis, gsInt, GS_FLAGS_DEFAULT, 0, 0, CONTAINMENTOBJECT, CONTAINMENTBASIS, INT_MIN, INT_MAX)
GETSET_RANGE(m_miscType, gsInt, GS_FLAGS_DEFAULT, 0, 0, CONTAINMENTOBJECT, MISCTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_respawnTime, gsDouble, GS_FLAGS_DEFAULT, 0, 0, CONTAINMENTOBJECT, RESPAWNTIME, DOUBLE_MIN, DOUBLE_MAX)
GETSET_RANGE(m_lastRespawnTime, gsLong, GS_FLAGS_DEFAULT, 0, 0, CONTAINMENTOBJECT, LASTRESPAWNTIME, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_currentAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, CONTAINMENTOBJECT, CURRENTAMOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_maxAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, CONTAINMENTOBJECT, MAXAMOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_dispersalAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, CONTAINMENTOBJECT, DISPERSALAMOUNT, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP