/******************************************************************************
 CLightClass.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "resource.h"
#include <afxtempl.h>
#include <d3d8.h>
#include "CLightClass.h"

//DELETION
void CLightObj::SafeDelete(LPDIRECT3DDEVICE7 device,
	CLightTrackerObjList* lightTrackList) {
	if (!m_lightFrame)
		return;
	m_lightFrame->SafeDelete();
	delete m_lightFrame;
	m_lightFrame = 0;
	lightTrackList->LightHasBeenRemoved(m_imLight);
	device->LightEnable(m_imLight, FALSE);
}
//set light poistion
void CLightObj::SetLightPositionForRender(LPDIRECT3DDEVICE7 device, D3DVECTOR offset) {
	if (!m_on)
		return;

	D3DVECTOR currentPos;
	m_lightFrame->GetPosition(NULL, &currentPos);
	currentPos.x -= offset.x;
	currentPos.y -= offset.y;
	currentPos.z -= offset.z;
	m_imStruct.dvPosition = currentPos;
	device->SetLight(m_imLight, &m_imStruct);
}

//CONSTRUCTION
CLightObj::CLightObj(
	BOOL L_linearTypeAtten,
	BOOL L_ficker,
	int L_flickerOnRange,
	float L_atenuationAdjustVarial,
	float L_attenuationVarial,
	int L_ocilatingVarial,
	float L_colorRed,
	float L_colorGreen,
	float L_colorBlue,
	float L_attenuation,
	float L_atenuationAdjust,
	BOOL L_ocilating,
	int L_ocilatingSteps,
	int L_delay,
	int L_delayVarial,
	BOOL L_on,
	CFrameObj* L_lightFrame,
	float L_range) {
	m_range = L_range;
	m_linearTypeAtten = L_linearTypeAtten;
	m_flickerOnRange = L_flickerOnRange;
	m_ficker = L_ficker;
	m_flickerOnRange = L_flickerOnRange;
	m_atenuationAdjustVarial = L_atenuationAdjustVarial;
	m_attenuationVarial = L_attenuationVarial;
	m_ocilatingVarial = L_ocilatingVarial;
	m_lightFrame = L_lightFrame;
	//m_light = L_light;
	m_colorRed = L_colorRed;
	m_colorGreen = L_colorGreen;
	m_colorBlue = L_colorBlue;
	m_attenuation = L_attenuation;
	m_atenuationAdjust = L_atenuationAdjust;
	m_ocilating = L_ocilating;
	m_ocilatingSteps = L_ocilatingSteps;
	m_delay = L_delay;
	m_delayVarial = L_delayVarial;
	m_on = L_on;
	m_imLight = -1;
	ZeroMemory(&m_imStruct, sizeof(D3DLIGHT7));
}

void CLightObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_colorRed
		   << m_colorGreen
		   << m_colorBlue
		   << m_attenuation
		   << m_atenuationAdjust
		   << m_ocilating
		   << m_ocilatingSteps
		   << m_delay
		   << 0 //m_delayVarial
		   << m_on
		   << 0 //m_ocilatingVarial;
		   << m_ficker
		   << m_flickerOnRange
		   << m_linearTypeAtten
		   << m_range;
	} else {
		ar >> m_colorRed >> m_colorGreen >> m_colorBlue >> m_attenuation >> m_atenuationAdjust >> m_ocilating >> m_ocilatingSteps >> m_delay >> m_delayVarial >> m_on >> m_ocilatingVarial >> m_ficker >> m_flickerOnRange >> m_linearTypeAtten >> m_range;

		ZeroMemory(&m_imStruct, sizeof(D3DLIGHT7));
		m_atenuationAdjustVarial = 0;
		m_delayVarial = 0;
		m_attenuationVarial = 0;
		m_attenuationVarial = 0;
		m_lightFrame = 0;
		m_ocilatingVarial = 0;
		//m_light=0;
		m_imLight = -1;
	}
}

IMPLEMENT_SERIAL(CLightObj, CObject, 0)
IMPLEMENT_SERIAL(CLightObjList, CObList, 0)

// begin serialize.pl generated code
BEGIN_GETSET_IMPL(CLightObj, IDS_LIGHTOBJ_NAME)
GETSET_RANGE(m_colorRed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, COLORRED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_colorGreen, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, COLORGREEN, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_colorBlue, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, COLORBLUE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_attenuation, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, ATTENUATION, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_atenuationAdjust, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, ATENUATIONADJUST, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_ocilating, gsInt, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, OCILATING, INT_MIN, INT_MAX)
GETSET_RANGE(m_ocilatingSteps, gsInt, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, OCILATINGSTEPS, INT_MIN, INT_MAX)
GETSET_RANGE(m_delay, gsInt, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, DELAY, INT_MIN, INT_MAX)
GETSET(0, gs, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, 0)
GETSET(m_on, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, ON)
GETSET(0, gs, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, 0)
GETSET(m_ficker, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, FICKER)
GETSET_RANGE(m_flickerOnRange, gsInt, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, FLICKERONRANGE, INT_MIN, INT_MAX)
GETSET(m_linearTypeAtten, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, LINEARTYPEATTEN)
GETSET_RANGE(m_range, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LIGHTOBJ, RANGE, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL
// end of generated code
