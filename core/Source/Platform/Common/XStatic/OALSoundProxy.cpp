///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "OALSoundProxy.h"
#include "IClientEngine.h"
#include "OALOggSound.h"
#include "SoundRM.h"
#include "LogHelper.h"

static LogInstance("Instance");

namespace KEP {

OALSoundProxy::OALSoundProxy(SoundRM* library, const GLID& glid) :
		IResource(library, glid), m_durationMS(0) {
	SetState(IResource::State::INITIALIZED_NOT_LOADED);

	// Sound assets are always encrypted
	SetKRXEncrypted(true);
}

OALSoundProxy::OALSoundProxy(SoundRM* library, const GLID& localGlid, BYTE* theBuff, FileSize sizeBytes) :
		IResource(library, localGlid, true),
		m_durationMS(0) {
	SetState(IResource::State::INITIALIZED_NOT_LOADED);

	std::unique_ptr<OALOggSound> pOS(new OALOggSound());
	if (!pOS)
		return;

	TimeMs durationMs = 0;
	if (!pOS->OpenOgg(theBuff, (size_t)sizeBytes, durationMs)) {
		LogError("OpenOgg() FAILED - glid<" << localGlid << ">");
		return;
	}

	m_buffer = std::move(pOS);
	m_durationMS = durationMs;

	// Resource Raw Buffer No Longer Needed
	FreeRawBuffer();

	SetState(State::LOADED_AND_CREATED);
}

OALSoundBuffer* OALSoundProxy::GetSoundBuffer() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return nullptr;

	SoundRM* soundRM = pICE->GetSoundRM();
	if (!soundRM)
		return nullptr;

	switch (GetState()) {
		case State::LOADED_AND_CREATED:
			return m_buffer.get();

		case State::INITIALIZED_NOT_LOADED:
			soundRM->QueueResource(this);
			return nullptr;
	}

	return nullptr;
}

bool OALSoundProxy::CreateResource() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	std::unique_ptr<OALOggSound> pOS(new OALOggSound());
	if (!pOS)
		return false;

	TimeMs durationMs = 0;
	if (!pOS->OpenOgg(this, durationMs)) {
		LogError("OpenOgg() FAILED");
		return false;
	}

	m_buffer = std::move(pOS);
	m_durationMS = durationMs;

	// Resource Raw Buffer No Longer Needed
	FreeRawBuffer();

	SetState(LOADED_AND_CREATED);

	return true;
}

} // namespace KEP
