///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CTextReadoutClass.h"
#include "CMaterialClass.h"
#include "CAdvancedVertexClass.h"
#include "CTextRenderClass.h"
#include "CStateManagementClass.h"
#include "Event/EventSystem/Dispatcher.h"
#include "Event/Events/GenericEvent.h"
#include "Event/Events/RenderTextEvent.h"

namespace KEP {

CStringObj::CStringObj() {
	m_pTRO = NULL;
	m_red = 1.0f;
	m_green = 1.0f;
	m_blue = 1.0f;
}

void CStringObj::Clone(CStringObj** copy) {
	CStringObj* copyPtr = new CStringObj();
	copyPtr->m_text = m_text;

	*copy = copyPtr;
}

void CStringObj::SafeDelete() {
	if (m_pTRO) {
		m_pTRO->SafeDelete();
		delete m_pTRO;
		m_pTRO = NULL;
	}
}

void CStringObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_text;
	} else {
		ar >> m_text;
		m_red = 1.0f;
		m_green = 1.0f;
		m_blue = 1.0f;
	}
}

void CStringObjList::Clone(CStringObjList** copy) {
	CStringObjList* copyLocal = new CStringObjList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStringObj* objPtr = (CStringObj*)GetNext(posLoc);
		CStringObj* cpyPtr = NULL;
		objPtr->Clone(&cpyPtr);
		copyLocal->AddTail(cpyPtr);
	}
	*copy = copyLocal;
}

CtextReadoutObj::CtextReadoutObj() {
	m_vertSpacing = 5;
	m_historyCount = 35;
	m_maxLineLength = 65;
	m_verticleLineVistCount = 3;
	m_timeOption = 4000;
	m_locationX = 1000;
	m_locationY = 1000;
	m_textLines = NULL;
	m_cgLocationX = -.59f;
	m_cgLocationY = -.4f;
	m_cgVertSpacing = .02f;
	m_cgBlendMode = 2;
	m_cgFontSizeX = .007f;
	m_cgFontSizeY = .009f;
	m_currentOffset = 0;
	m_scrollStamp = fTime::TimeMs();
	m_maxMeasuredLength = .54f;

	m_attribEventID = NULL;
	m_dispatcher = NULL;
}

void CtextReadoutObj::SetDispatcher(Dispatcher* d) {
	m_dispatcher = d;
	m_attribEventID = m_dispatcher->RegisterEvent("AttribEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
}

void CtextReadoutObj::SafeDelete() {
	if (m_textLines) {
		for (POSITION posLoc = m_textLines->GetHeadPosition(); posLoc != NULL;) {
			CStringObj* strPtr = (CStringObj*)m_textLines->GetNext(posLoc);
			strPtr->SafeDelete();
			delete strPtr;
			strPtr = 0;
		}
		m_textLines->RemoveAll();
		delete m_textLines;
		m_textLines = 0;
	}
}

void CtextReadoutObj::Clone(CtextReadoutObj** copy) {
	CtextReadoutObj* copyPtr = new CtextReadoutObj();
	copyPtr->m_vertSpacing = m_vertSpacing;
	copyPtr->m_historyCount = m_historyCount;
	copyPtr->m_maxLineLength = m_maxLineLength;
	copyPtr->m_verticleLineVistCount = m_verticleLineVistCount;
	copyPtr->m_timeOption = m_timeOption;
	copyPtr->m_locationX = m_locationX;
	copyPtr->m_locationY = m_locationY;
	copyPtr->m_maxMeasuredLength = m_maxMeasuredLength;
	copyPtr->m_dispatcher = m_dispatcher;
	copyPtr->m_attribEventID = m_attribEventID;

	copyPtr->m_textLines = NULL;
	if (m_textLines)
		m_textLines->Clone(&copyPtr->m_textLines);

	*copy = copyPtr;
}

CStringA CtextReadoutObj::FloatToStingCnv(float number) {
	int decimal, sign;
	char buffer[32];
	float source = number;
	_fcvt_s(buffer, _countof(buffer), source, 7, &decimal, &sign);
	CStringA final = buffer;
	if (decimal == 0) {
		final = operator+(".", final);
	} else if (decimal < 0) {
		for (int loop = 0; loop < abs(decimal); loop++) {
			final = operator+("0", final);
		}
		final = operator+(".", final);
	} else {
		CStringA tempFinal;
		for (int loop = 0; loop < final.GetLength(); loop++) {
			if (loop == decimal)
				tempFinal = operator+(tempFinal, ".");

			tempFinal = operator+(tempFinal, final.GetAt(loop));
		}
		final = tempFinal;
	}
	if (sign != 0)
		final = operator+("-", final);

	return final;
}

void CtextReadoutObj::AddLine(const std::string& text, eChatType chatType) {
	IEvent* e = new RenderTextEvent(text, chatType);
	m_dispatcher->QueueEvent(e);
}

void CtextReadoutObj::RenderText(HDC hdc) {
	int displayTrack = 0;
	if (m_textLines)
		for (POSITION posLoc = m_textLines->GetTailPosition(); posLoc != NULL;) {
			CStringObj* strPtr = (CStringObj*)m_textLines->GetPrev(posLoc);
			std::wstring textW = Utf8ToUtf16(strPtr->m_text);
			ExtTextOutW(hdc,
				m_locationX,
				(m_locationY - (m_vertSpacing * displayTrack)), 0, 0,
				textW.c_str(),
				textW.size(), 0);
			displayTrack++;
			if (displayTrack >= m_verticleLineVistCount)
				break;
		}
}

void CtextReadoutObj::RenderTextCG(CStateManagementObj* statemanager, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	int displayTrack = 0;
	BOOL reinitializeAbove = 0;

	int totalCount = 0;

	if (m_textLines)
		totalCount = (int)m_textLines->GetCount();

	int startPosition = (totalCount - m_currentOffset) - 1;

	if (startPosition < 0)
		startPosition = 0;

	if (m_textLines) {
		for (POSITION posLoc = m_textLines->FindIndex(startPosition); posLoc != NULL;) {
			CStringObj* strPtr = (CStringObj*)m_textLines->GetPrev(posLoc);

			if (!strPtr->m_pTRO) {
				reinitializeAbove = TRUE;
			} else {
				g_pd3dDevice->SetMaterial(&strPtr->m_pTRO->m_materialObj->m_useMaterial);
				statemanager->SetDiffuseSource(strPtr->m_pTRO->m_vertexObject->m_advancedFixedMode);
				statemanager->SetVertexType(strPtr->m_pTRO->m_vertexObject->m_uvCount);
				strPtr->m_pTRO->m_vertexObject->Render(g_pd3dDevice, statemanager->m_currentVertexType);
			}
			displayTrack++;
			if (displayTrack >= m_verticleLineVistCount)
				break;
		}
	}
}

void CtextReadoutObj::ScrollUp(LPDIRECT3DDEVICE9 g_pd3dDevice, CSymbolMapperObjList* symbolMapSys, int speechMode) {
	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_scrollStamp) < 100)
		return;

	m_scrollStamp = timeMs;

	if (m_textLines)
		m_historyCount = (int)m_textLines->GetCount();

	if (m_historyCount > m_currentOffset) {
		m_currentOffset++;
		ReinitVisualSection(g_pd3dDevice, symbolMapSys, speechMode);
	}
}

void CtextReadoutObj::ScrollDown(LPDIRECT3DDEVICE9 g_pd3dDevice, CSymbolMapperObjList* symbolMapSys, int speechMode) {
	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_scrollStamp) < 100)
		return;

	m_scrollStamp = timeMs;

	if (m_currentOffset > 0) {
		m_currentOffset--;
		ReinitVisualSection(g_pd3dDevice, symbolMapSys, speechMode);
	}
}

void CtextReadoutObj::ReinitVisualSection(LPDIRECT3DDEVICE9 g_pd3dDevice, CSymbolMapperObjList* symbolMapSys, int speechMode) {
	int displayTrack = 0;
	BOOL reinitializeAbove = 0;

	int totalCount = 0;

	if (m_textLines)
		totalCount = (int)m_textLines->GetCount();

	int startPosition = (totalCount - m_currentOffset) - 1;

	if (startPosition < 0)
		startPosition = 0;

	if (m_textLines) {
		for (POSITION posLoc = m_textLines->FindIndex(startPosition); posLoc != NULL;) {
			CStringObj* strPtr = (CStringObj*)m_textLines->GetPrev(posLoc);

			if (strPtr->m_pTRO) {
				strPtr->m_pTRO->SafeDelete();
				delete strPtr->m_pTRO;
				strPtr->m_pTRO = 0;
			}
			reinitializeAbove = TRUE;

			strPtr->m_pTRO = new CTextRenderObj(
				(const char*)strPtr->m_text,
				1,
				m_cgLocationX,
				(m_cgLocationY + (m_cgVertSpacing * displayTrack)),
				m_cgFontSizeX,
				m_cgFontSizeY,
				true,
				strPtr->m_red,
				strPtr->m_green,
				strPtr->m_blue);

			strPtr->m_pTRO->UpdateGeometry(g_pd3dDevice, (const char*)strPtr->m_text);

			displayTrack++;
			if (displayTrack >= m_verticleLineVistCount)
				break;
		}
	}
}

CStringObj* CtextReadoutObj::GetHeadVisual() {
	if (m_textLines) {
		for (POSITION posLoc = m_textLines->GetTailPosition(); posLoc != NULL;) {
			CStringObj* strPtr = (CStringObj*)m_textLines->GetPrev(posLoc);
			if (strPtr->m_pTRO)
				return strPtr;
		}
	}
	return NULL;
}

void CtextReadoutObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_vertSpacing
		   << m_historyCount
		   << m_maxLineLength
		   << m_verticleLineVistCount
		   << m_timeOption
		   << m_locationX
		   << m_locationY
		   << m_textLines
		   << m_cgLocationX
		   << m_cgLocationY
		   << m_cgVertSpacing
		   << m_cgBlendMode
		   << m_cgFontSizeX
		   << m_cgFontSizeY;
	} else {
		ar >> m_vertSpacing >> m_historyCount >> m_maxLineLength >> m_verticleLineVistCount >> m_timeOption >> m_locationX >> m_locationY >> m_textLines >> m_cgLocationX >> m_cgLocationY >> m_cgVertSpacing >> m_cgBlendMode >> m_cgFontSizeX >> m_cgFontSizeY;

		m_scrollStamp = fTime::TimeMs();
		m_maxMeasuredLength = .54f;
	}
}

void CtextReadoutObjList::Clone(CtextReadoutObjList** copy) {
	CtextReadoutObjList* copyLocal = new CtextReadoutObjList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CtextReadoutObj* objPtr = (CtextReadoutObj*)GetNext(posLoc);
		CtextReadoutObj* cpyPtr = NULL;
		objPtr->Clone(&cpyPtr);
		copyLocal->AddTail(cpyPtr);
	}
	*copy = copyLocal;
}

IMPLEMENT_SERIAL(CtextReadoutObj, CObject, 0)
IMPLEMENT_SERIAL(CStringObj, CObject, 0)
IMPLEMENT_SERIAL(CtextReadoutObjList, CObject, 0)
IMPLEMENT_SERIAL(CStringObjList, CObject, 0)

BEGIN_GETSET_IMPL(CStringObj, IDS_STRINGOBJ_NAME)
GETSET_MAX(m_text, gsCString, GS_FLAGS_DEFAULT, 0, 0, STRINGOBJ, TEXT, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP