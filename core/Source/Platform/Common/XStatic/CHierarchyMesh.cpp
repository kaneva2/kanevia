///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include "d3d9.h"
#include <afxtempl.h>
#include "CHierarchyMesh.h"
#include <SerializeHelper.h>
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#ifdef _ClientOutput
#include "ReMesh.h"
#include "ReMeshStream.h"
#include "IClientEngine.h"
#include "ReMeshCreate.h"
#endif

#include "UnicodeMFCHelpers.h"
#include "Common/include/IMemSizeGadget.h"

namespace KEP {

CHiarcleVisObj::CHiarcleVisObj() {
	m_mesh = NULL;
	m_reMesh = NULL;
	m_isCloned = FALSE;
}

void CHiarcleVisObj::Clone(CHiarcleVisObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
#ifdef _ClientOutput
	CHiarcleVisObj* copyLocal = new CHiarcleVisObj();

	if (m_mesh) {
		m_mesh->Clone(&copyLocal->m_mesh, g_pd3dDevice);
	}

	if (m_reMesh && copyLocal->m_mesh) {
		ReMesh* mesh = 0;
		m_reMesh->Clone(&mesh, true);
		// Try adding the mesh, just incase it doesn't already exist.
		// Why it wouldn't exist is an unanswered question; where did
		// it come from?  Loaded with some other object type I guess,
		// and not added to the mesh manager at that time, which might
		// be a bug.
		ReMesh* addedMesh = MeshRM::Instance()->AddMesh(mesh, MeshRM::RMP_ID_LIBRARY);
		mesh = NULL;
		m_reMesh->Clone(&mesh, true);
		ASSERT(mesh);
		if (mesh) {
			copyLocal->m_reMesh = mesh;
			copyLocal->m_reMesh->SetWorldMatrix(&copyLocal->m_mesh->GetWorldTransform());
			copyLocal->m_reMesh->SetWorldViewProjMatrix(&copyLocal->m_WorldViewProjMatrix);
			copyLocal->m_reMesh->SetWorldInvTransposeMatrix(&copyLocal->m_WorldInvTransposeMatrix);
			copyLocal->m_reMesh->SetRadius(copyLocal->m_mesh->GetBoundingRadius());
			copyLocal->m_reMesh->SetLightCache(NULL);
		}
		if (addedMesh) {
			//delete addedMesh; // Don't free here crashes.  Don't understand the lifetime of these.
		}
	}

	copyLocal->m_isCloned = TRUE;

	*clone = copyLocal;
#endif
}

void CHiarcleVisObj::SafeDelete() {
#ifdef _ClientOutput
	if (m_mesh) {
		m_mesh->SafeDelete();
		delete m_mesh;
		m_mesh = 0;
	}

	//if(m_isCloned)
	{
		if (m_reMesh) {
			delete m_reMesh;
			m_reMesh = NULL;
		}
	}
#else
	ASSERT(0);
#endif
}

void CHiarcleVisObj::Optimize() {
	if (m_mesh) {
		m_mesh->OptimizeMesh();
		//m_mesh->GenerateSmoothNormals();
	}
}

void CHiarcleVisObj::Serialize(CArchive& ar) {
#ifdef _ClientOutput
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(1);
		ASSERT(ar.GetObjectSchema() == 1);

		// do NOT stream vertex bufffers
		if (m_mesh) {
			m_mesh->m_VertexArrayValid = FALSE;
			ar << m_mesh;
		}

		// write the ReMesh
		if (m_reMesh) { // RENDERENGINE INTERFACE
			// get a ReMeshStream object
			ReMeshStream streamer;
			// stream out the ReMesh object
			streamer.Write(m_reMesh, ar);
		}
	} else {
		int version = ar.GetObjectSchema();

		ar >> m_mesh;
		m_isCloned = FALSE;

		if (version == 0) {
			// optimize the mesh
			m_mesh->OptimizeMesh();
			// create a new ReMesh the legacy way....
			SetReMesh(MeshRM::Instance()->CreateReStaticMesh(m_mesh)); // no pivot point needed - CreateReStaticMeshLibrary
		} else if (version >= 1) {
			// get an ReMeshStream object
			ReMeshStream streamer;
			// stream the ReMesh
			auto pICE = IClientEngine::Instance();
			if (!pICE)
				return;
			streamer.Read(&m_reMesh, ar, pICE->GetReDeviceState());
			// create the ReMaterial
			MeshRM::Instance()->CreateReMaterial(m_mesh->m_materialObject, "CharacterTextures");

			for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
				m_mesh->m_materialObject->m_texNameHashOriginal[loop] = m_mesh->m_materialObject->m_texNameHash[loop];
			}
		}
		// set the state of the ReMesh
		// m_reMesh could still be null if streamer.Read() failed
		if (m_reMesh) {
			m_reMesh->SetWorldMatrix(&m_mesh->GetWorldTransform());
			m_reMesh->SetWorldViewProjMatrix(&m_WorldViewProjMatrix);
			m_reMesh->SetWorldInvTransposeMatrix(&m_WorldInvTransposeMatrix);
			m_reMesh->SetRadius(m_mesh->GetBoundingRadius());
			m_reMesh->SetLightCache(NULL);
		}
	}

#else
	ASSERT(0);
#endif
}

void CHiarcleVisList::Optimize() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHiarcleVisObj* vObj = (CHiarcleVisObj*)GetNext(posLoc);
		vObj->Optimize();
	}
}

void CHiarcleVisList::Clone(CHiarcleVisList** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CHiarcleVisList* hVisList = new CHiarcleVisList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHiarcleVisObj* vObj = (CHiarcleVisObj*)GetNext(posLoc);
		CHiarcleVisObj* cloneObj = NULL;
		vObj->Clone(&cloneObj, g_pd3dDevice);
		hVisList->AddTail(cloneObj);
	}
	*clone = hVisList;
}

void CHiarcleVisList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHiarcleVisObj* vObj = (CHiarcleVisObj*)GetNext(posLoc);
		if (vObj) { // sometimes we have NULL Lods dued to bad formatted file
			vObj->SafeDelete();
			delete vObj;
			vObj = 0;
		}
	}
	RemoveAll();
}

CHiarchyMesh::CHiarchyMesh() {
	m_lodChain = NULL;
}

void CHiarchyMesh::Clone(CHiarchyMesh** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CHiarchyMesh* copyLocal = new CHiarchyMesh();
	m_lodChain->Clone(&copyLocal->m_lodChain, g_pd3dDevice);

	*clone = copyLocal;
}

void CHiarchyMesh::SafeDelete() {
	if (m_lodChain) {
		m_lodChain->SafeDelete();
		delete m_lodChain;
		m_lodChain = 0;
	}
}

void CHiarchyMesh::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_lodChain;
	} else {
		ar >> m_lodChain;
	}
}

void CHiarchyMesh::SetFadeOut(TimeMs duration) {
	if (!m_lodChain)
		return;
	for (POSITION posLoc = m_lodChain->GetHeadPosition(); posLoc != NULL;) {
		CHiarcleVisObj* vObj = (CHiarcleVisObj*)m_lodChain->GetNext(posLoc);
		vObj->m_mesh->m_materialObject->SetFadeOut(duration);
	}
}

void CHiarchyMesh::SetFadeIn(TimeMs duration) {
	if (!m_lodChain)
		return;
	for (POSITION posLoc = m_lodChain->GetHeadPosition(); posLoc != NULL;) {
		CHiarcleVisObj* vObj = (CHiarcleVisObj*)m_lodChain->GetNext(posLoc);
		vObj->m_mesh->m_materialObject->SetFadeIn(duration, TRUE);
	}
}

void CHiarchyMesh::Load(CStringA filename) {
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, filename, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return;
	}
	m_lodChain->SafeDelete();
	delete m_lodChain;
	m_lodChain = 0;

	BEGIN_SERIALIZE_TRY

	CArchive the_in_Archive(&fl, CArchive::load);
	the_in_Archive >> m_lodChain;
	the_in_Archive.Close();
	fl.Close();

	END_SERIALIZE_TRY_NO_LOGGER
}

void CHiarchyMesh::Save(CStringA filename) {
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, filename, CFile::modeCreate | CFile::modeWrite, &exc);
	if (res) {
		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);

		the_out_Archive << m_lodChain;

		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY_NO_LOGGER

	} else
		LOG_EXCEPTION_NO_LOGGER(exc)
}

BOOL CHiarchyMesh::SetLevel(int index, CStringA filename) {
	CFileException exc;
	CFile fl;

	POSITION lodPos = m_lodChain->FindIndex(0);

	CMaterialObject* primaryMaterial = NULL;
	if (lodPos) {
		CHiarcleVisObj* delPtr = (CHiarcleVisObj*)m_lodChain->GetAt(lodPos);
		delPtr->m_mesh->m_materialObject->Clone(&primaryMaterial);
	}

	BOOL res = OpenCFile(fl, filename, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	CHiarcleVisObj* vObj = NULL;

	CArchive the_in_Archive(&fl, CArchive::load);

	BEGIN_SERIALIZE_TRY

	the_in_Archive >> vObj;
	the_in_Archive.Close();
	fl.Close();

	if (primaryMaterial) {
		delete vObj->m_mesh->m_materialObject;
		vObj->m_mesh->m_materialObject = NULL;
		primaryMaterial->Clone(&vObj->m_mesh->m_materialObject);
	}

	POSITION posLoc = m_lodChain->FindIndex(index);
	if (!posLoc) {
		m_lodChain->AddTail(vObj);
	} else { //replace
		m_lodChain->InsertAfter(posLoc, vObj);
		CHiarcleVisObj* delPtr = (CHiarcleVisObj*)m_lodChain->GetAt(posLoc);
		delPtr->SafeDelete();
		delete delPtr;
		delPtr = 0;
		m_lodChain->RemoveAt(posLoc);
	} //end replace

	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

CHiarcleVisObj* CHiarchyMesh::GetByIndex(int index) {
	// Fall back to nearest LOD if no match
	if (index >= m_lodChain->GetCount())
		index = m_lodChain->GetCount() - 1;

	POSITION posLoc = m_lodChain->FindIndex(index);
	if (!posLoc)
		return NULL;
	CHiarcleVisObj* delPtr = (CHiarcleVisObj*)m_lodChain->GetAt(posLoc);
	return delPtr;
}

BOOL CHiarchyMesh::SaveLevel(int index, CStringA filename) {
	if (!m_lodChain)
		return FALSE;

	POSITION posLoc = m_lodChain->FindIndex(index);
	if (!posLoc)
		return FALSE;

	CHiarcleVisObj* vObj = (CHiarcleVisObj*)m_lodChain->GetAt(posLoc);

	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, filename, CFile::modeCreate | CFile::modeWrite, &exc);
	if (!res) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	BEGIN_SERIALIZE_TRY

	CArchive the_out_Archive(&fl, CArchive::store);

	the_out_Archive << vObj;

	the_out_Archive.Close();
	fl.Close();
	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

void CHiarchyMesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_lodChain);
		pMemSizeGadget->AddObject(m_name);
	}
}

void CHiarcleVisObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_mesh);

#ifdef _ClientOutput
		pMemSizeGadget->AddObject(m_reMesh);
#endif
	}
}

void CHiarcleVisList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CHiarcleVisObj* pCHiarcleVisObj = static_cast<const CHiarcleVisObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCHiarcleVisObj);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CHiarchyMesh, CObject)
IMPLEMENT_SERIAL_SCHEMA(CHiarcleVisObj, CObject) // version 1: ReMesh support
IMPLEMENT_SERIAL_SCHEMA(CHiarcleVisList, CObject)

BEGIN_GETSET_IMPL(CHiarcleVisObj, IDS_HIARCLEVISOBJ_NAME)
GETSET_OBJ_PTR(m_mesh, GS_FLAGS_DEFAULT, 0, 0, HIARCLEVISOBJ, MESH, CEXMeshObj)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CHiarchyMesh, IDS_HIARCHYMESH_NAME)
GETSET_OBLIST_PTR(m_lodChain, GS_FLAGS_DEFAULT, 0, 0, HIARCHYMESH, LODCHAIN)
END_GETSET_IMPL

} // namespace KEP
