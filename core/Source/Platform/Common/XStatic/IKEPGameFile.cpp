///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ikepgamefile.h"

namespace KEP {

IKEPGameFile::IKEPGameFile(void) {
}

IKEPGameFile::~IKEPGameFile(void) {
}

} // namespace KEP