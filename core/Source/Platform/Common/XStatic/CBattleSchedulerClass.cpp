///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include <KEPHelpers.h>
#include "Event/EventSystem/Dispatcher.h"
#include "Event/Events/GenericEvent.h"
#include "js.h"
#include "jsThread.h"

#include "CBattleSchedulerClass.h"

namespace KEP {

#define checkFreqMs 30000
#define defaultMaxSignupTimeMs 120000
// make them the same by default
#define defaultMinSignupTimeMs 120000

// to avoid each CBattleSchedulerObj creating the events, and can't be static since can run > 1 game in server process
// ALWAYS UPDATE THE COUNT!!
static int tlsEventIds = -1;
#define ARENA_SIGNUP_ID 0
#define ARENA_SIGNOUT_ID 1
#define ARENA_READY_ID 2
#define ARENA_QUERY_SIGNUP_ID 3
#define ARENA_EVENT_COUNT 4

// so far only one type of query to scripting
#define GET_SIGNUP_QUERY 1

CBattleSchedulerObj::CBattleSchedulerObj() :
		// runtime only data, not serialized
		m_lastSelectedPoint(-1),
		m_battleIsActive(false),
		m_signupStartTime(fTime::TimeMs()) {
	if (tlsEventIds == -1)
		tlsEventIds = jsThread::createThreadData();

	EVENT_ID* ids = (EVENT_ID*)jsThread::getThreadData(tlsEventIds);
	if (ids == 0) {
		Dispatcher* dispatcher = Dispatcher::GetEnginesDispatcher();
		if (dispatcher) {
			ids = new EVENT_ID[ARENA_EVENT_COUNT];
			jsVerify(BAD_EVENT_ID != (ids[ARENA_SIGNUP_ID] = dispatcher->RegisterEvent("ArenaSignup", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL)));
			jsVerify(BAD_EVENT_ID != (ids[ARENA_SIGNOUT_ID] = dispatcher->RegisterEvent("ArenaSignout", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL)));
			jsVerify(BAD_EVENT_ID != (ids[ARENA_READY_ID] = dispatcher->RegisterEvent("ArenaReadyCheck", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL)));
			jsVerify(BAD_EVENT_ID != (ids[ARENA_QUERY_SIGNUP_ID] = dispatcher->RegisterEvent("ArenaQuerySignup", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL)));
			jsThread::setThreadData(tlsEventIds, (ULONG)ids);
		}
	}
	m_maxSignupTimeMs = defaultMaxSignupTimeMs;
	m_minSignupTimeMs = m_maxSignupTimeMs; // now that always have arenas, use max time
	m_battleStartTime = 0;
	m_type = BT_LAST_MAN_STANDING; //0=last man standing 1=Team Arena
	m_battleMinimum = 10; //will cancel match is not enough play (notify players)
	m_battleMaximum = 30;
	m_battleDuration = 30000;
	//restriction
	m_levelRangeMax = 18;
	m_levelRangeMin = 0;
	//signupList
	m_signups = NULL;
	m_currentSignupCount = 0;
	//portal vars
	m_zoneIndex.clear(); //file to load //wid is this right?
	m_channel.clear(); //channel
	m_spawnPointCount = 0;
	m_spawnPositions = NULL;
	m_haveEnoughTeams = false;

	m_scriptName = "";
	m_maxScore = 5;
	m_minTeams = 2;
	m_maxTeams = 2;
}

void CBattleSchedulerObj::ResetBattle() {
	m_currentSignupCount = 0;
	m_signupStartTime = fTime::TimeMs();
	m_haveEnoughTeams = false;
}

void CBattleSchedulerObj::RemoveSignup(NETID playerHandle) {
	Dispatcher* dispatcher = Dispatcher::GetEnginesDispatcher();
	if (dispatcher) {
		EVENT_ID* ids = (EVENT_ID*)jsThread::getThreadData(tlsEventIds);
		jsAssert(ids != 0);
		IEvent* e = dispatcher->MakeEvent(ids[ARENA_SIGNOUT_ID]);
		(*e->OutBuffer()) << (ULONG)(IGetSet*)this << playerHandle;

		if (dispatcher->ProcessSynchronousEventNoRelease(e)) { // Keep the event
			// get the current count
			(*e->InBuffer()) >> m_currentSignupCount;
		} else {
			std::string msg = loadStr(IDS_ARENA_FN_FAILED);
			msg += "RemoveSignup";
			dispatcher->LogMessage(log4cplus::WARN_LOG_LEVEL, msg);
		}
		e->Release(); // Release now
	} else {
		jsAssert(false);
	}
}

EArenaReady CBattleSchedulerObj::ReadyToStart() {
	TimeMs timeSinceStart = fTime::ElapsedMs(m_signupStartTime);
	EArenaReady res = ArenaTimedOut;

	Dispatcher* dispatcher = Dispatcher::GetEnginesDispatcher();
	if (dispatcher) {
		EVENT_ID* ids = (EVENT_ID*)jsThread::getThreadData(tlsEventIds);
		jsAssert(ids != 0);
		IEvent* e = dispatcher->MakeEvent(ids[ARENA_READY_ID]);
		(*e->OutBuffer()) << (ULONG)(IGetSet*)this << (ULONG)(IGetSet*)this << (ULONG)timeSinceStart << (ULONG)m_minSignupTimeMs;

		if (dispatcher->ProcessSynchronousEventNoRelease(e)) { // only one guy should have process it
			// get their answer
			int r;
			(*e->InBuffer()) >> r;
			res = (EArenaReady)r;
		} else {
			std::string msg = loadStr(IDS_ARENA_READY_FAILED);
			msg += " ";
			msg += m_scriptName;
			dispatcher->LogMessage(log4cplus::DEBUG_LOG_LEVEL, msg);
		}
		e->Release(); // Release now

	} else {
		jsAssert(false);
	}

	return res;
}

BOOL CBattleSchedulerObj::AddSignup(NETID playerHandle, std::string& msg) {
	Dispatcher* dispatcher = Dispatcher::GetEnginesDispatcher();
	BOOL res = FALSE;

	if (dispatcher) {
		EVENT_ID* ids = (EVENT_ID*)jsThread::getThreadData(tlsEventIds);
		jsAssert(ids != 0);
		IEvent* e = dispatcher->MakeEvent(ids[ARENA_SIGNUP_ID]);
		(*e->OutBuffer()) << (ULONG)(IGetSet*)this << playerHandle;

		if (dispatcher->ProcessSynchronousEventNoRelease(e)) { // Keep the event
			// get their answer
			bool eventRes = false;
			(*e->InBuffer()) >> eventRes;
			(*e->InBuffer()) >> m_currentSignupCount;
			(*e->InBuffer()) >> msg;
			res = eventRes != 0;
		} else {
			std::string errorMsg = loadStr(IDS_ARENA_FN_FAILED);
			errorMsg += "AddSignup";
			dispatcher->LogMessage(log4cplus::WARN_LOG_LEVEL, errorMsg);
		}
		e->Release(); // Release now

	} else {
		jsAssert(false);
	}

	return res;
}

void CBattleSchedulerObj::AddSpawnPoint(Vector3f position) {
	if (m_spawnPointCount == 0 || m_spawnPositions == NULL) {
		//init
		if (m_spawnPositions) {
			delete[] m_spawnPositions;
			m_spawnPositions = 0;
		}
		m_spawnPositions = new Vector3f[m_spawnPointCount + 1];
		m_spawnPositions[m_spawnPointCount].x = position.x;
		m_spawnPositions[m_spawnPointCount].y = position.y;
		m_spawnPositions[m_spawnPointCount].z = position.z;
		m_spawnPointCount++;
		return;
	} //end init

	Vector3f* spawnArrayTemp = new Vector3f[m_spawnPointCount + 1];
	for (int i = 0; i < m_spawnPointCount; i++) {
		spawnArrayTemp[i].x = m_spawnPositions[i].x;
		spawnArrayTemp[i].y = m_spawnPositions[i].y;
		spawnArrayTemp[i].z = m_spawnPositions[i].z;
	}

	spawnArrayTemp[m_spawnPointCount].x = position.x;
	spawnArrayTemp[m_spawnPointCount].y = position.y;
	spawnArrayTemp[m_spawnPointCount].z = position.z;
	m_spawnPointCount = m_spawnPointCount + 1;

	delete[] m_spawnPositions;
	m_spawnPositions = 0;
	m_spawnPositions = spawnArrayTemp;
}

void CBattleSchedulerObj::DeleteSpawnPoint(int index) {
	if (!m_spawnPositions)
		return;

	int newCount = m_spawnPointCount - 1;
	if (newCount < 1) {
		delete[] m_spawnPositions;
		m_spawnPositions = 0;
		m_spawnPointCount = 0;
		return;
	}
	Vector3f* tempArray = new Vector3f[m_spawnPointCount - 1];
	int tracer = 0;
	for (int i = 0; i < m_spawnPointCount; i++) {
		if (i == index)
			continue;

		tempArray[tracer].x = m_spawnPositions[i].x;
		tempArray[tracer].y = m_spawnPositions[i].y;
		tempArray[tracer].z = m_spawnPositions[i].z;
		tracer++;
	}

	delete[] m_spawnPositions;
	m_spawnPositions = 0;

	m_spawnPositions = tempArray;
	m_spawnPointCount--;
}

void CBattleSchedulerObj::ReplaceSpawnPoint(int index, Vector3f position) {
	if (m_spawnPointCount == 0 || m_spawnPositions == NULL) {
		//init
		return;
	} //end init

	m_spawnPositions[index].x = position.x;
	m_spawnPositions[index].y = position.y;
	m_spawnPositions[index].z = position.z;
}

Vector3f CBattleSchedulerObj::GetSpawnPointData(int index) {
	if (m_spawnPointCount == 0 || m_spawnPositions == NULL) {
		//init
		Vector3f initPos(0, 0, 0);
		return initPos;
	} //end init

	return m_spawnPositions[index];
}

Vector3f CBattleSchedulerObj::GetNextSpawnPoint() {
	if (m_spawnPointCount == 0 || m_spawnPositions == NULL) {
		//init
		Vector3f initPos(0, 0, 0);
		return initPos;
	} //end init

	m_lastSelectedPoint++;
	if (m_lastSelectedPoint >= m_spawnPointCount)
		m_lastSelectedPoint = m_spawnPointCount - 1;

	return m_spawnPositions[m_lastSelectedPoint];
}

void CBattleSchedulerObj::SafeDelete() {
	if (m_signups) {
		delete[] m_signups;
		m_signups = 0;
	}

	if (m_spawnPositions) {
		delete[] m_spawnPositions;
		m_spawnPositions = 0;
	}
}

CBattleSchedulerList::CBattleSchedulerList() :
		m_nextInstanceId(1) {
}

// create a new channel id using the channel passed in and our next instance id
ChannelId CBattleSchedulerList::GetNextInstanceId(const ChannelId& channel) {
	ChannelId ret(channel.channelId(), m_nextInstanceId++, CI_ARENA);
	return ret;
}

void CBattleSchedulerList::RemoveSignup(NETID playerHandle) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CBattleSchedulerObj* arenaObj = (CBattleSchedulerObj*)GetNext(posLoc);
		arenaObj->RemoveSignup(playerHandle);
	}
}

BOOL CBattleSchedulerList::AddSignup(CBattleSchedulerObj* arenaObj, NETID playerHandle, std::string& msg) {
	// since multiple signups are offered at one time, remove them from any existing ones first
	// which could include one they are signing up for a second time
	RemoveSignup(playerHandle);

	// TEMPTEMPTEMP
	CBattleSchedulerObj* obj = GetSignupForPlayer(playerHandle);

	BOOL ret = arenaObj->AddSignup(playerHandle, msg);

	// TEMPTEMPTEMP
	obj = GetSignupForPlayer(playerHandle);

	return ret;
}
void CBattleSchedulerList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CBattleSchedulerObj* spnPtr = (CBattleSchedulerObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CBattleSchedulerList::FreeData() {
	EVENT_ID* ids = (EVENT_ID*)jsThread::getThreadData(tlsEventIds);
	if (ids) {
		delete[] ids;
		jsThread::setThreadData(tlsEventIds, 0);
	}
}

CBattleSchedulerObj* CBattleSchedulerList::GetSignupForPlayer(NETID playerNetId) {
	Dispatcher* dispatcher = Dispatcher::GetEnginesDispatcher();
	CBattleSchedulerObj* pFoundObj = nullptr;

	if (dispatcher) {
		EVENT_ID* ids = (EVENT_ID*)jsThread::getThreadData(tlsEventIds);
		jsAssert(ids != 0);
		IEvent* e = dispatcher->MakeEvent(ids[ARENA_QUERY_SIGNUP_ID]);
		(*e->OutBuffer()) << GET_SIGNUP_QUERY << (LONG)playerNetId;

		if (dispatcher->ProcessSynchronousEventNoRelease(e)) { // Keep the event
			// get their answer
			bool eventRes = false;
			(*e->InBuffer()) >> eventRes;
			if (eventRes) {
				LONG arenaId;
				(*e->InBuffer()) >> arenaId;
				pFoundObj = dynamic_cast<CBattleSchedulerObj*>((IGetSet*)arenaId);
			}
		} else {
			std::string msg = loadStr(IDS_ARENA_FN_FAILED);
			msg += "GetSignupForPlayer";
			dispatcher->LogMessage(log4cplus::WARN_LOG_LEVEL, msg);
		}
		e->Release(); // Release now

	} else {
		jsAssert(false);
	}

	return pFoundObj;
}

CBattleSchedulerObj* CBattleSchedulerList::GetByIndex(int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;
	CBattleSchedulerObj* tempPtr = (CBattleSchedulerObj*)GetAt(posLoc);
	return tempPtr;
}

void CBattleSchedulerObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		ar << m_name
		   << m_type
		   << m_battleMinimum
		   << m_battleMaximum
		   << (int)m_battleDuration
		   << m_levelRangeMax
		   << m_levelRangeMin
		   << m_zoneIndex
		   << m_spawnPointCount
		   << m_channel;

		for (int loop = 0; loop < m_spawnPointCount; loop++) {
			ar << m_spawnPositions[loop].x
			   << m_spawnPositions[loop].y
			   << m_spawnPositions[loop].z;
		}

		// version 2 items
		ar << m_maxSignupTimeMs;
		ar << m_minSignupTimeMs;

		// version 3 items
		ar << m_scriptName
		   << m_maxScore
		   << m_minTeams
		   << m_maxTeams;

	} else {
		int schema = ar.GetObjectSchema();
		int batDur;
		ar >> m_name >> m_type >> m_battleMinimum >> m_battleMaximum >> batDur >> m_levelRangeMax >> m_levelRangeMin >> m_zoneIndex >> m_spawnPointCount >> m_channel;
		m_battleDuration = batDur;
		m_spawnPositions = NULL;
		if (m_spawnPointCount > 0)
			m_spawnPositions = new Vector3f[m_spawnPointCount];

		for (int loop = 0; loop < m_spawnPointCount; loop++) {
			ar >> m_spawnPositions[loop].x >> m_spawnPositions[loop].y >> m_spawnPositions[loop].z;
		}

		if (schema >= 2) {
			ar >> m_maxSignupTimeMs;
			ar >> m_minSignupTimeMs;
		} else {
			m_maxSignupTimeMs = defaultMaxSignupTimeMs;
			m_minSignupTimeMs = defaultMinSignupTimeMs;
		}

		if (schema >= 3) {
			ar >> m_scriptName >> m_maxScore >> m_minTeams >> m_maxTeams;
		} else {
			m_scriptName = "";
			m_maxScore = 5;
			m_minTeams = 2;
			m_maxTeams = 2;
		}

		m_signups = NULL;
		m_currentSignupCount = 0;
	}
}

IMPLEMENT_SERIAL_SCHEMA(CBattleSchedulerObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CBattleSchedulerList, CKEPObList)

BEGIN_GETSET_IMPL(CBattleSchedulerObj, IDS_BATTLESCHEDULEROBJ_NAME)
GETSET_MAX(m_name, gsCString, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, NAME, STRLEN_MAX)
GETSET_RANGE(m_type, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, TYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_battleMinimum, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, BATLLEMINIMUM, INT_MIN, INT_MAX)
GETSET_RANGE(m_battleMaximum, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, BATTLEMAXIMUM, INT_MIN, INT_MAX)
GETSET_RANGE(m_battleDuration, gsDouble, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, BATTLEDURATION, DOUBLE_MIN, DOUBLE_MAX)
GETSET_RANGE(m_levelRangeMax, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, LEVELRANGEMAX, INT_MIN, INT_MAX)
GETSET_RANGE(m_levelRangeMin, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, LEVELRANGEMIN, INT_MIN, INT_MAX)
GETSET_RANGE(m_zoneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, VISUALWLDINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_spawnPointCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, SPAWNPOINTCOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_channel, gsInt, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, WLDCHANNEL, INT_MIN, INT_MAX)
GETSET_MAX(m_scriptName, gsCString, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, SCRIPTNAME, STRLEN_MAX)
GETSET_RANGE(m_maxScore, gsUlong, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, MAXSCORE, 0, 1000000)
GETSET_RANGE(m_minTeams, gsUlong, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, MINTEAMS, 0, 1000000)
GETSET_RANGE(m_maxTeams, gsUlong, GS_FLAGS_DEFAULT, 0, 0, BATTLESCHEDULEROBJ, MAXTEAMS, 0, 1000000)
END_GETSET_IMPL

} // namespace KEP