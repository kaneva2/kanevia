///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FlashControlServer.h"
#include "FlashClientServerAPI.h"
#include "SetThreadName.h"
#include "Common/KEPUtil/RuntimeReporter.h"
#include "WindowsMixer.h"

#include <memory>
#include <mutex>
#include <map>

#include "f_in_box/f_in_box.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {
namespace FlashControl {

class FlashControlFactory {
public:
	FlashControlFactory();
	~FlashControlFactory();

	bool Connect(ProcessHandleWrapper&& hClientProcess, SharedMemory&& hSharedMemory, EventHandleWrapper&& hNotifyClientEvent, EventHandleWrapper&& hNotifyServerEvent);

private:
	bool IsInSeparateProcess() {
		return m_bIsInSeparateProcess;
	}
	bool Initialize(ProcessHandleWrapper&& hClientProcess, SharedMemory&& hSharedMemory, EventHandleWrapper&& hNotifyClientEvent, EventHandleWrapper&& hNotifyServerEvent);
	void SendCurrentStateToClient();

	bool EventLoop();

	// Returns true if message processed.
	bool ProcessWindowMessage();
	void ProcessClientRequests();
	void AddDeleteProviders(uint32_t iProviderListUpdateId);
	void ProcessProviderRequests();
	void UpdateGlobalVolume();
	void ApplyGlobalVolume();

	static DWORD WINAPI StaticMonitorClientProcessFunc(void* p);
	DWORD MonitorClientProcessFunc();
	void SetQuit() {
		if (m_bQuit.exchange(true, std::memory_order_acq_rel) == false) {
			LogInfo("Exiting FlashControl factory.");
			::SetEvent(m_hNotifyClientMonitorEvent.GetHandle());

			// Exit quickly if we're running in a separate process.
			// Don't leave this process around if threads are blocked or in an infinite loop.
			if (IsInSeparateProcess()) {
				RuntimeReporter::ProfileStateStopped();
				ExitProcess(0);
			}
		}
	}
	bool ShouldQuit() const {
		return m_bQuit.load(std::memory_order_acquire);
	}

	bool CreateOwnerWindow();
	static LRESULT CALLBACK OwnerWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

private:
	std::atomic<bool> m_bQuit = false;
	bool m_bIsInSeparateProcess = false;
	uint32_t m_iClientProcessId = 0;
	ProcessHandleWrapper m_hClientProcess;
	EventHandleWrapper m_hNotifyClientEvent;
	EventHandleWrapper m_hNotifyServerEvent;
	SharedMemory m_hSharedMemory;
	MappedFileView m_hSharedMemoryView;
	FlashFactorySharedMemoryBlock* m_pSharedMemory = nullptr;

	// Thread to monitor the client handle and set the quit flag if it exits.
	// Can't rely on waiting in main loop because the wait is not performed if the CPU is heavily loaded.
	ThreadHandleWrapper m_hClientMonitorThread;
	EventHandleWrapper m_hNotifyClientMonitorEvent; // To cleanly exit from client monitor thread.

	TripleBufferWriterStateMgr m_ServerDataStateMgr;
	TripleBufferReaderStateMgr m_ClientDataStateMgr;
	FactoryServerStateData m_ServerState;
	FactoryClientStateData m_ClientState;

	float m_fGlobalVolume = -1;
	bool m_bGlobalMute = false;
	std::shared_ptr<IWindowsMixerProxy> m_pWindowsMixer;

	// Handle volume set by windows mixer. This is not guaranteed to occur on our main thread, so we synchronize.
	TripleBufferSynchronizer m_MixerVolumeSynchronizer;
	TripleBufferWriterStateMgr m_MixerVolumeWriteStateMgr; // Written by mixer callback
	TripleBufferReaderStateMgr m_MixerVolumeReadStateMgr; // Read by main event loop
	VolumeSetting m_aMixerSetVolume[3];

	struct ServerMapData {
		uint32_t m_iValidForProviderListId;
		std::shared_ptr<IFlashControlServer> m_pServer;
	};
	std::map<uint32_t, ServerMapData> m_ActiveServerMap;
};

FlashControlFactory::FlashControlFactory() {
	m_MixerVolumeWriteStateMgr.SetSynchronizer(&m_MixerVolumeSynchronizer);
	m_MixerVolumeReadStateMgr.SetSynchronizer(&m_MixerVolumeSynchronizer);

	CreateOwnerWindow();
}

FlashControlFactory::~FlashControlFactory() {
	if (m_hClientMonitorThread) {
		SetQuit();
		WaitForSingleObject(m_hClientMonitorThread.GetHandle(), INFINITE);
	}
}

bool FlashControlFactory::Connect(ProcessHandleWrapper&& hClientProcess, SharedMemory&& hSharedMemory, EventHandleWrapper&& hNotifyClientEvent, EventHandleWrapper&& hNotifyServerEvent) {
	if (!Initialize(std::move(hClientProcess), std::move(hSharedMemory), std::move(hNotifyClientEvent), std::move(hNotifyServerEvent))) {
		LogError("Could not initialize");
		return false;
	}

	if (!EventLoop())
		return false;

	return true;
}

bool FlashControlFactory::Initialize(ProcessHandleWrapper&& hClientProcess, SharedMemory&& hSharedMemory, EventHandleWrapper&& hNotifyClientEvent, EventHandleWrapper&& hNotifyServerEvent) {
	// Initialize client communication channel.
	m_hClientProcess = std::move(hClientProcess);
	m_iClientProcessId = GetProcessId(m_hClientProcess.GetHandle());
	m_bIsInSeparateProcess = m_iClientProcessId != GetCurrentProcessId();
	m_hNotifyClientEvent = std::move(hNotifyClientEvent);
	m_hNotifyServerEvent = std::move(hNotifyServerEvent);
	m_hSharedMemory = std::move(hSharedMemory);
	m_hSharedMemoryView = m_hSharedMemory.Map(0, sizeof(FlashFactorySharedMemoryBlock), true);
	if (!m_hSharedMemoryView)
		return false;

	m_pSharedMemory = reinterpret_cast<FlashFactorySharedMemoryBlock*>(m_hSharedMemoryView.GetPointer());
	m_ServerDataStateMgr.SetSynchronizer(&m_pSharedMemory->m_ServerDataSynchronizer);
	m_ClientDataStateMgr.SetSynchronizer(&m_pSharedMemory->m_ClientDataSynchronizer);
	m_ServerState.UpdateFrom(m_pSharedMemory->m_aServerData[0]);

	// Initialize mixer interface.
	if (!m_pWindowsMixer) {
		m_pWindowsMixer = IWindowsMixerProxy::New();
		if (IsInSeparateProcess() && m_pWindowsMixer) {
			m_pWindowsMixer->SetVolumeChangeCallback([this](float fNewVolume, bool bNewMute) {
				LogInfo("Volume change from mixer: " << fNewVolume);
				this->m_aMixerSetVolume[this->m_MixerVolumeWriteStateMgr.GetWorkAreaIndex()] = VolumeSetting{ fNewVolume, bNewMute };
				this->m_MixerVolumeWriteStateMgr.CompletedWork();
				::SetEvent(this->m_hNotifyServerEvent.GetHandle()); // Wake our processing thread.
			});
		}
	}

	// Initialize client monitor thread.
	m_hNotifyClientMonitorEvent.Set(CreateEvent(nullptr, TRUE, FALSE, nullptr));
	if (!m_hNotifyClientMonitorEvent)
		return false;

	m_hClientMonitorThread.Set(CreateThread(NULL, 0, StaticMonitorClientProcessFunc, this, 0, 0));
	if (!m_hClientMonitorThread)
		return false;

	// Tell client that we are initialized.
	// Only update data if we are the first flash process running.
	if (m_ServerState.m_InitData.GetVersion() == 0) {
		bool bFlashIsInstalled = IsFlashInstalled();
		std::string strFlashVersion;
		if (bFlashIsInstalled)
			strFlashVersion = GetFlashVersion();

		FactoryServerStateData::InitData& initData = m_ServerState.m_InitData.GetRefToUpdate();
		initData.m_bFlashIsInstalled = bFlashIsInstalled;
		initData.m_strFlashVersion.Set(strFlashVersion);
	}
	SendCurrentStateToClient();

	return true;
}

void FlashControlFactory::SendCurrentStateToClient() {
	uint32_t idx = m_ServerDataStateMgr.GetWorkAreaIndex();
	m_pSharedMemory->m_aServerData[idx].UpdateFrom(m_ServerState);
	m_ServerDataStateMgr.CompletedWork();
	::SetEvent(m_hNotifyClientEvent.GetHandle());
}

// Process windows messages, requests to add or remove FlashControlServers, and requests to specific FlashControlServers.
bool FlashControlFactory::EventLoop() {
	HANDLE aWaitHandles[] = {
		m_hNotifyServerEvent.GetHandle(), // Client request.
		// m_hClientProcess.GetHandle(), // Client process. Handled in client process monitor thread.
	};
	size_t nWaitHandles = std::extent<decltype(aWaitHandles)>::value;

	while (!ShouldQuit()) {
		while (!ShouldQuit()) {
			// Process client requests.
			ProcessClientRequests();

			// Handle volume change.
			if (m_MixerVolumeReadStateMgr.ClaimBuffer()) {
				VolumeSetting vs = m_aMixerSetVolume[m_MixerVolumeReadStateMgr.GetBufferIndex()];
				m_ServerState.m_Volume.UpdateValue(vs);
				SendCurrentStateToClient();
			}

			if (!ShouldQuit()) {
				// Process one Windows message.
				if (!ProcessWindowMessage())
					break; // Nothing more to process. Loop back to wait for our events to be signaled.
			}
		}

		// No more client requests or window messages to process.
		// Wait for an event to wake us up.
		if (!ShouldQuit()) {
			// If the wait exits because a message is available, iWaitResult is set to (WAIT_OBJECT_0 + nWaitHandles).
			DWORD iWaitResult = MsgWaitForMultipleObjects(nWaitHandles, aWaitHandles, FALSE, INFINITE, QS_ALLINPUT);
			if (iWaitResult < WAIT_OBJECT_0 || iWaitResult > WAIT_OBJECT_0 + nWaitHandles) {
				LogError("Wait failed unexpectedly.");
				SetQuit();
			}
		}
	}

	return true;
}

void FlashControlFactory::ProcessClientRequests() {
	if (!m_ClientDataStateMgr.ClaimBuffer())
		return; // No client requests.

	uint32_t idx = m_ClientDataStateMgr.GetBufferIndex();
	if (idx >= 3)
		return; // Invalid request index.

	const FactoryClientStateData* pClientData = &m_pSharedMemory->m_aClientData[idx];

	if (pClientData->m_Quit != FactoryClientStateData::eQuit::DONT_QUIT) {
		// Quit request
		LogInfo("Quit request from client");
		if (pClientData->m_Quit == FactoryClientStateData::eQuit::QUIT_CRASH) {
			LogInfo("Crash request from client.");
			*((volatile int*)0) = 0;
			LogError("We should have crashed before reaching this message.");
		}
		SetQuit();
		return;
	}

	bool bUpdateProviderList = pClientData->m_aProviderIds.IsNewerThan(m_ClientState.m_aProviderIds);
	bool bUpdateVolume = pClientData->m_Volume.IsNewerThan(m_ClientState.m_Volume);
	bool bHandleProviderRequests = pClientData->m_iIndividualRequestId != m_ClientState.m_iIndividualRequestId;
	m_ClientState.UpdateFrom(*pClientData);

	if (bUpdateProviderList) {
		uint32_t iNewProviderListVersion = m_ClientState.m_aProviderIds.GetVersion();
		AddDeleteProviders(iNewProviderListVersion);
	}

	if (bUpdateVolume)
		UpdateGlobalVolume();

	// Process all individual requests
	if (bHandleProviderRequests)
		ProcessProviderRequests();
}

// Processes a single windows message if any are available. Returns true if message processed.
bool FlashControlFactory::ProcessWindowMessage() {
	MSG msg;
	if (!::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE | PM_NOYIELD) != 0)
		return false;

	if (msg.message == WM_QUIT) {
	} else {
		if (msg.message == WM_SETFOCUS) {
			// The client is unexpectedly losing focus in some cases. Added this log
			// message to help determine whether the focus is going to a Flash controlled window.
			bool bTopFlashWindow = false;
			uint32_t iProviderOwningWindow = 0;
			for (const auto& mapEntry : m_ActiveServerMap) {
				IFlashControlServer* pServer = mapEntry.second.m_pServer.get();
				if (pServer) {
					if (pServer->OwnsWindow(msg.hwnd, &bTopFlashWindow)) {
						iProviderOwningWindow = mapEntry.first;
						break;
					}
				}
			}
			LogInfo("SetFocus windows message received in Flash control factory."
					<< " Message to provider " << iProviderOwningWindow
					<< ". It is " << (bTopFlashWindow ? "" : "not ") << "the main Flash control window.");
		}
		::TranslateMessage(&msg);
		::DispatchMessage(&msg);
		if (msg.message == WM_CLOSE) {
			// Shut down provider?
		}
	}
	return true;
}

// Synchronizes the providers (FlashControlServers) with the list of providers currently needed by the client.
void FlashControlFactory::AddDeleteProviders(uint32_t iProviderListUpdateId) {
	// Update m_iValidForProviderListId for each item in list.
	uint32_t nProviders = m_ClientState.m_aProviderIds.GetValue().size();
	for (uint32_t i = 0; i < nProviders; ++i) {
		uint32_t iProviderId = m_ClientState.m_aProviderIds.GetValue()[i];
		if (iProviderId == 0)
			continue;
		m_ActiveServerMap[iProviderId].m_iValidForProviderListId = iProviderListUpdateId;
	}

	// Add/Remove items from map.
	auto itr = m_ActiveServerMap.begin();
	while (itr != m_ActiveServerMap.end()) {
		if (itr->second.m_iValidForProviderListId != iProviderListUpdateId) {
			// Delete server
			if (itr->second.m_pServer) {
				LogInfo(itr->second.m_pServer->GetIdString() << "Stopping provider: " << itr->first);
				itr->second.m_pServer->StopProcessing();
			}
			LogInfo("Deleting provider: " << itr->first);
			itr = m_ActiveServerMap.erase(itr);
		} else {
			if (!itr->second.m_pServer) {
				// Create new server
				LogInfo("Creating provider: " << itr->first);
				itr->second.m_pServer = IFlashControlServer::Create();
				uint32_t iProviderId = itr->first;
				std::wstring wstrServerSharedMemory = ServerSharedMemoryName(m_iClientProcessId, iProviderId);
				HANDLE hSharedMemory = OpenFileMappingW(FILE_MAP_READ | FILE_MAP_WRITE, FALSE, wstrServerSharedMemory.c_str());
				//LogInfo("Opening file mapping " << Utf16ToUtf8(wstrServerSharedMemory));
				IFlashControlServer* pServer = itr->second.m_pServer.get();
				if (pServer->StartServer(m_iClientProcessId, iProviderId, hSharedMemory, INVALID_HANDLE_VALUE, false)) {
					if (!m_pWindowsMixer)
						pServer->SetGlobalVolume(m_fGlobalVolume);
				} else {
					LogError("Could not start server " << iProviderId);
					// Leave it in the map so we don't try creating it again.
					//itr->second.m_pServer.reset();
				}
			}
			++itr;
		}
	}
}

// Loop through each IFlashControlServer and let it check for and process any requests.
void FlashControlFactory::ProcessProviderRequests() {
	for (auto& mapEntry : m_ActiveServerMap) {
		IFlashControlServer* pServer = mapEntry.second.m_pServer.get();
		if (pServer) {
			pServer->ProcessNextRequest();
		}
	}
}

void FlashControlFactory::UpdateGlobalVolume() {
	VolumeSetting vsNew = m_ClientState.m_Volume.GetValue();
	float fNewGlobalVolume = vsNew.m_fVolume;
	bool bNewGlobalMute = vsNew.m_bMute;

	m_fGlobalVolume = fNewGlobalVolume;
	m_bGlobalMute = bNewGlobalMute;
	ApplyGlobalVolume();
}

void FlashControlFactory::ApplyGlobalVolume() {
	if (m_pWindowsMixer) {
		// Only modify volume if we're running in a separate process. Otherwise let client control it.
		if (IsInSeparateProcess()) {
			bool bNewMute = m_bGlobalMute;
			float fNewVolume = m_fGlobalVolume;
			bool bOldMute;
			float fOldVolume;
			LogInfo("Setting global volume: " << fNewVolume);
			if (m_pWindowsMixer->GetVolume(out(fOldVolume)) && std::abs(fNewVolume - fOldVolume) > .001f)
				m_pWindowsMixer->SetVolume(fNewVolume);
			if (m_pWindowsMixer->GetMute(out(bOldMute)) && bNewMute != bOldMute)
				m_pWindowsMixer->SetMute(bNewMute);
		}
	} else {
		for (auto pr : m_ActiveServerMap) {
			IFlashControlServer* pServer = pr.second.m_pServer.get();
			if (pServer)
				pServer->SetGlobalVolume(m_bGlobalMute ? 0 : m_fGlobalVolume);
		}
	}
}

// static
DWORD WINAPI FlashControlFactory::StaticMonitorClientProcessFunc(void* p) {
	SetThreadName("Client Process Monitor");
	return static_cast<FlashControlFactory*>(p)->MonitorClientProcessFunc();
}

// Calls SetQuit if client process exits.
DWORD FlashControlFactory::MonitorClientProcessFunc() {
	HANDLE ahWaitHandles[] = {
		m_hClientProcess.GetHandle(),
		m_hNotifyClientMonitorEvent.GetHandle()
	};
	while (true) {
		DWORD dwWaitResult = WaitForMultipleObjects(2, ahWaitHandles, FALSE, INFINITE);
		if (dwWaitResult == WAIT_OBJECT_0) {
			SetQuit(); // Process exited.
			break;
		}
		if (dwWaitResult == WAIT_OBJECT_0 + 1) {
			// This thread was requested to exit.
			if (!ShouldQuit()) {
				LogError("Expecting a quit notification.");
				continue;
			}
			break;
		}
		LogError("Wait failed");
		SetQuit();
		break;
	}
	return 0;
}

bool FlashControlFactory::CreateOwnerWindow() {
	const wchar_t* const OWNER_WINDOW_CLASS = L"KEP_FPC_OWNER_CLASS";
	const wchar_t* const OWNER_WINDOW_CAPTION = L"KEP_FPC_OWNER";

	HWND hwndOwner = IFlashControlServer::GetOwnerWindow();
	assert(hwndOwner == NULL);

	if (hwndOwner == NULL) {
		HINSTANCE hInstance = (HINSTANCE)GetModuleHandle(NULL); // Handle of EXE module

		// Create an owner window for FPC window handles so that we can process WM_NOTIFY messages
		WNDCLASS wc = { 0 };
		wc.lpszClassName = OWNER_WINDOW_CLASS;
		wc.hInstance = hInstance;
		wc.lpfnWndProc = OwnerWindowProc;
		wc.cbWndExtra = sizeof(LONG_PTR); // Reserve space for FlashControlFactory::this

		if (!RegisterClass(&wc)) {
			LogError("Error registering window class, GetLastError=" << GetLastError());
			// Continue in case it's already registered
		}

		hwndOwner = CreateWindow(OWNER_WINDOW_CLASS, OWNER_WINDOW_CAPTION, WS_POPUP, 0, 0, 0, 0, NULL, NULL, hInstance, nullptr);
		assert(hwndOwner != NULL);
		if (hwndOwner == NULL) {
			LogError("Error creating owner window, GetLastError=" << GetLastError());
			return false;
		}

		// Record this pointer for WindowProc
		SetWindowLongPtr(hwndOwner, 0, reinterpret_cast<LONG_PTR>(this));
	}

	// hwndOwner is shared by all FlashControlServer instances
	IFlashControlServer::SetOwnerWindow(hwndOwner);
	return true;
}

LRESULT CALLBACK FlashControlFactory::OwnerWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (WM_NOTIFY == uMsg) {
		LPNMHDR lpNMHDR = (LPNMHDR)lParam;

		FlashControlFactory* me = reinterpret_cast<FlashControlFactory*>(GetWindowLongPtr(hWnd, 0));
		assert(me != nullptr);

		// Look for FlashControlServer instance by sender's HWND
		for (const auto& pr : me->m_ActiveServerMap) {
			const ServerMapData& pData = pr.second;
			if (pData.m_pServer->OwnsWindow(lpNMHDR->hwndFrom, nullptr)) {
				switch (lpNMHDR->code) {
					case FPCN_FLASHCALL:
						// An "external interface" call from action script
						pData.m_pServer->HandleFlashCall(reinterpret_cast<SFPCFlashCallInfoStruct*>(lpNMHDR));
						return 1;
				}
				break;
			}
		}
	}

	return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
}

int FlashProcessMain(int argc, wchar_t* argv[]) {
	auto StringToHandle = [](const wchar_t* pwz) -> HANDLE {
		uintptr_t iHandle = reinterpret_cast<uintptr_t>(INVALID_HANDLE_VALUE);
		if (pwz)
			std::wistringstream(pwz) >> iHandle;
		return reinterpret_cast<HANDLE>(iHandle);
	};

	if (argc != 3) {
		// Warning: when used in-process, handles may leak here. Solution: Pass in the correct number of arguments!
		LogError("Incorrect number of arguments: " << argc);
		return 1;
	}
	ProcessHandleWrapper hClientProcess(StringToHandle(argv[1]));
	uint32_t iFactoryId = 0;
	std::wistringstream(argv[2]) >> iFactoryId;
	if (iFactoryId == 0) {
		LogError("Invalid factory ID: " << argv[2]);
		return 1;
	}
	DWORD iClientProcessId = GetProcessId(hClientProcess.GetHandle());
	if (iClientProcessId == 0) {
		LogError("Invalid client process");
		return 1;
	}
	std::wstring strSharedMemory = FlashFactorySharedMemoryName(iClientProcessId, iFactoryId);
	SharedMemory hSharedMemoryHandle(OpenFileMappingW(FILE_MAP_READ | FILE_MAP_WRITE, FALSE, strSharedMemory.c_str()));
	std::wstring strNotifyClientEvent = FlashFactoryNotifyClientEventName(iClientProcessId, iFactoryId);
	EventHandleWrapper hClientEventHandle(OpenEvent(SYNCHRONIZE | EVENT_MODIFY_STATE, FALSE, strNotifyClientEvent.c_str()));
	std::wstring strNotifyServerEvent = FlashFactoryNotifyServerEventName(iClientProcessId, iFactoryId);
	EventHandleWrapper hServerEventHandle(OpenEvent(SYNCHRONIZE | EVENT_MODIFY_STATE, FALSE, strNotifyServerEvent.c_str()));

	if (!hSharedMemoryHandle || !hClientEventHandle || !hServerEventHandle) {
		LogError("Invalid synchronization handles: " << hSharedMemoryHandle.GetHandle() << ", " << hClientEventHandle.GetHandle() << ", " << hServerEventHandle.GetHandle());
		LogError("Factory ID: " << iFactoryId << " argv[2]: " << argv[2]);
		return 1;
	}

	HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
	if (hr != S_OK && hr != S_FALSE && hr != RPC_E_CHANGED_MODE) // COM is initialized with any of these codes.
		LogError("Could not initialize COM. Return code: " << hr);

	FlashControlFactory m_FlashControlFactory;
	m_FlashControlFactory.Connect(std::move(hClientProcess), std::move(hSharedMemoryHandle), std::move(hClientEventHandle), std::move(hServerEventHandle));

	return 0;
}

} // namespace FlashControl
} // namespace KEP
