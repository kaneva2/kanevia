///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KepWidgets.h"
#include "CEXMeshClass.h"
#include "Core/Math/Rotation.h"

#ifdef _ClientOutput
#include "ReMeshCreate.h"
#include "DynamicObj.h"
#include "IClientEngine.h"
#include "MeshStrings.h"
#endif

#include "Common\KEPUtil\Helpers.h"
#include "Core/Math/Intersect.h"
static LogInstance("Instance");

namespace KEP {

CMaterialObject* KEPWidgetMeshes::ms_xMaterial = NULL;
CMaterialObject* KEPWidgetMeshes::ms_yMaterial = NULL;
CMaterialObject* KEPWidgetMeshes::ms_zMaterial = NULL;
CMaterialObject* KEPWidgetMeshes::ms_xyplaneMaterial = NULL;
CMaterialObject* KEPWidgetMeshes::ms_xzplaneMaterial = NULL;
CMaterialObject* KEPWidgetMeshes::ms_yzplaneMaterial = NULL;

const D3DXCOLOR KEPWidgetMeshes::X_COLOR(1.00f, 0.25f, 0.50f, 1.0f);
const D3DXCOLOR KEPWidgetMeshes::Y_COLOR(0.50f, 1.00f, 0.25f, 1.0f);
const D3DXCOLOR KEPWidgetMeshes::Z_COLOR(0.25f, 0.50f, 1.00f, 1.0f);
const D3DXCOLOR KEPWidgetMeshes::PLANE_COLOR(0.85f, 0.85f, 0.10f, 0.25f);

const float MATERIAL_EMISSIVE = 0.10f; //0.10f;	// Fraction of diffuse color that non-lit areas receive
const float ROLLOVER_EMISSIVE = 0.40f; // Fraction of diffuse color used in brightening rollovers

ReMesh* KEPWidgetMeshes::loadReMeshOBJ(const char* objdata, const Matrix44f* matrix) {
	std::vector<Vector3f> invertex;
	std::vector<Vector3f> innormal;
	std::vector<int> facevertex, facenormal;

	while (objdata) {
		const char* p = strstr(objdata, "\n");

		Vector3f vec;

		// Try to read a vertex
		int n = sscanf_s(objdata, "v %f %f %f\n", &vec.x, &vec.y, &vec.z);

		if (n == 3) {
			invertex.push_back(vec);
		} else {
			// Try to read a vertex normal
			n = sscanf_s(objdata, "vn %f %f %f\n", &vec.x, &vec.y, &vec.z);

			if (n == 3) {
				innormal.push_back(vec);
			} else {
				int pi[3] = { 0, 0, 0 }, ni[3] = { 0, 0, 0 };

				// Try to read a face
				n = sscanf_s(
					objdata,
					"f %d//%d %d//%d %d//%d\n",
					&pi[0], &ni[0],
					&pi[1], &ni[1],
					&pi[2], &ni[2]);

				if (n == 6) {
					// OBJ files use 1-based indices for stuff so we subtract 1 here

					facevertex.push_back(pi[0] - 1);
					facevertex.push_back(pi[1] - 1);
					facevertex.push_back(pi[2] - 1);

					facenormal.push_back(ni[0] - 1);
					facenormal.push_back(ni[1] - 1);
					facenormal.push_back(ni[2] - 1);
				}
			}
		}

		// Advance to next line if possible
		objdata = p ? (p + 1) : NULL;
	}

	// Build non-indexed mesh

	std::vector<Vector3f> outvertex(facevertex.size());
	std::vector<Vector3f> outnormal(facevertex.size());
	std::vector<Vector2f> outuv(facevertex.size());

	for (size_t fv = 0; fv < facevertex.size(); fv++) {
		int vi = facevertex[fv];

		const Vector3f* srcV = &invertex[vi];
		Vector3f* dstV = &outvertex[fv];

		const float scale = 1.0f;
		*dstV = scale * *srcV;

		int ni = facenormal[fv];

		const Vector3f* srcN = &innormal[ni];
		Vector3f* dstN = &outnormal[fv];
		*dstN = srcN->GetNormalized();

		Vector2f* dstUV = &outuv[fv];
		*dstUV = Vector2f(0, 0);
	}

	ReMeshData data;
	ZeroMemory(&data, sizeof(data));

	data.pP = &outvertex[0];
	data.pN = &outnormal[0];
	data.pUv = &outuv[0];
	data.pI = 0;
	data.PrimType = ReMeshPrimType::TRILIST;
	data.NumV = facevertex.size();
	data.NumUV = 1;

	if (matrix)
		data.applyMatrix(matrix);

	ReMesh* mesh = MeshRM::Instance()->CreateReStaticMesh(&data, false);

	return mesh;
}

CMaterialObject* KEPWidgetMeshes::getXMaterial() {
	if (ms_xMaterial) {
		return ms_xMaterial;
	} else {
		ms_xMaterial = new CMaterialObject();

		ms_xMaterial->m_useMaterial.Diffuse = X_COLOR;
		ms_xMaterial->m_useMaterial.Emissive = MATERIAL_EMISSIVE * X_COLOR;
		ms_xMaterial->m_useMaterial.Ambient = D3DXCOLOR(0, 0, 0, 0);

		ms_xMaterial->m_baseMaterial.Emissive = ms_xMaterial->m_useMaterial.Emissive;
		ms_xMaterial->m_baseMaterial.Diffuse = ms_xMaterial->m_useMaterial.Diffuse;
		ms_xMaterial->m_baseMaterial.Ambient = ms_xMaterial->m_useMaterial.Ambient;

#ifdef _ClientOutput
		MeshRM::Instance()->CreateReMaterial(ms_xMaterial);
		ms_xMaterial->GetReMaterial()->PreLight(FALSE);
#endif
		return ms_xMaterial;
	}
}

CMaterialObject* KEPWidgetMeshes::getYMaterial() {
	if (ms_yMaterial) {
		return ms_yMaterial;
	} else {
		ms_yMaterial = new CMaterialObject();

		ms_yMaterial->m_useMaterial.Diffuse = Y_COLOR;
		ms_yMaterial->m_useMaterial.Emissive = MATERIAL_EMISSIVE * Y_COLOR;
		ms_yMaterial->m_useMaterial.Ambient = D3DXCOLOR(0, 0, 0, 0);

		ms_yMaterial->m_baseMaterial.Emissive = ms_yMaterial->m_useMaterial.Emissive;
		ms_yMaterial->m_baseMaterial.Diffuse = ms_yMaterial->m_useMaterial.Diffuse;
		ms_yMaterial->m_baseMaterial.Ambient = ms_yMaterial->m_useMaterial.Ambient;

#ifdef _ClientOutput
		MeshRM::Instance()->CreateReMaterial(ms_yMaterial);
		ms_yMaterial->GetReMaterial()->PreLight(FALSE);
#endif
		return ms_yMaterial;
	}
}

CMaterialObject* KEPWidgetMeshes::getZMaterial() {
	if (ms_zMaterial) {
		return ms_zMaterial;
	} else {
		ms_zMaterial = new CMaterialObject();

		ms_zMaterial->m_useMaterial.Diffuse = Z_COLOR;
		ms_zMaterial->m_useMaterial.Emissive = MATERIAL_EMISSIVE * Z_COLOR;
		ms_zMaterial->m_useMaterial.Ambient = D3DXCOLOR(0, 0, 0, 0);

		ms_zMaterial->m_baseMaterial.Emissive = ms_zMaterial->m_useMaterial.Emissive;
		ms_zMaterial->m_baseMaterial.Diffuse = ms_zMaterial->m_useMaterial.Diffuse;
		ms_zMaterial->m_baseMaterial.Ambient = ms_zMaterial->m_useMaterial.Ambient;

#ifdef _ClientOutput
		MeshRM::Instance()->CreateReMaterial(ms_zMaterial);
		ms_zMaterial->GetReMaterial()->PreLight(FALSE);
#endif
		return ms_zMaterial;
	}
}

CMaterialObject* KEPWidgetMeshes::getXYPlaneMaterial() {
	if (ms_xyplaneMaterial) {
		return ms_xyplaneMaterial;
	} else {
		ms_xyplaneMaterial = new CMaterialObject();
		ms_xyplaneMaterial->m_blendMode = 1;
		ms_xyplaneMaterial->m_useMaterial.Diffuse = PLANE_COLOR;
		ms_xyplaneMaterial->m_useMaterial.Emissive = MATERIAL_EMISSIVE * PLANE_COLOR;
		ms_xyplaneMaterial->m_useMaterial.Ambient = D3DXCOLOR(0, 0, 0, 0);

		ms_xyplaneMaterial->m_baseMaterial.Emissive = ms_zMaterial->m_useMaterial.Emissive;
		ms_xyplaneMaterial->m_baseMaterial.Diffuse = ms_zMaterial->m_useMaterial.Diffuse;
		ms_xyplaneMaterial->m_baseMaterial.Ambient = ms_zMaterial->m_useMaterial.Ambient;

#ifdef _ClientOutput
		MeshRM::Instance()->CreateReMaterial(ms_xyplaneMaterial);
		ms_xyplaneMaterial->GetReMaterial()->PreLight(FALSE);
#endif
		return ms_xyplaneMaterial;
	}
}

CMaterialObject* KEPWidgetMeshes::getXZPlaneMaterial() {
	if (ms_xzplaneMaterial) {
		return ms_xzplaneMaterial;
	} else {
		ms_xzplaneMaterial = new CMaterialObject();
		ms_xzplaneMaterial->m_blendMode = 1;
		ms_xzplaneMaterial->m_useMaterial.Diffuse = PLANE_COLOR;
		ms_xzplaneMaterial->m_useMaterial.Emissive = MATERIAL_EMISSIVE * PLANE_COLOR;
		ms_xzplaneMaterial->m_useMaterial.Ambient = D3DXCOLOR(0, 0, 0, 0);

		ms_xzplaneMaterial->m_baseMaterial.Emissive = ms_zMaterial->m_useMaterial.Emissive;
		ms_xzplaneMaterial->m_baseMaterial.Diffuse = ms_zMaterial->m_useMaterial.Diffuse;
		ms_xzplaneMaterial->m_baseMaterial.Ambient = ms_zMaterial->m_useMaterial.Ambient;

#ifdef _ClientOutput
		MeshRM::Instance()->CreateReMaterial(ms_xzplaneMaterial);
		ms_xzplaneMaterial->GetReMaterial()->PreLight(FALSE);
#endif
		return ms_xzplaneMaterial;
	}
}

CMaterialObject* KEPWidgetMeshes::getYZPlaneMaterial() {
	if (ms_yzplaneMaterial) {
		return ms_yzplaneMaterial;
	} else {
		ms_yzplaneMaterial = new CMaterialObject();
		ms_yzplaneMaterial->m_blendMode = 1;
		ms_yzplaneMaterial->m_useMaterial.Diffuse = PLANE_COLOR;
		ms_yzplaneMaterial->m_useMaterial.Emissive = MATERIAL_EMISSIVE * PLANE_COLOR;
		ms_yzplaneMaterial->m_useMaterial.Ambient = D3DXCOLOR(0, 0, 0, 0);

		ms_yzplaneMaterial->m_baseMaterial.Emissive = ms_zMaterial->m_useMaterial.Emissive;
		ms_yzplaneMaterial->m_baseMaterial.Diffuse = ms_zMaterial->m_useMaterial.Diffuse;
		ms_yzplaneMaterial->m_baseMaterial.Ambient = ms_zMaterial->m_useMaterial.Ambient;

#ifdef _ClientOutput
		MeshRM::Instance()->CreateReMaterial(ms_yzplaneMaterial);
		ms_yzplaneMaterial->GetReMaterial()->PreLight(FALSE);
#endif
		return ms_yzplaneMaterial;
	}
}

ReMesh* KEPWidgetMeshes::getXTransWidget() {
	return getTransWidget(0);
}

ReMesh* KEPWidgetMeshes::getYTransWidget() {
	return getTransWidget(1);
}

ReMesh* KEPWidgetMeshes::getZTransWidget() {
	return getTransWidget(2);
}

ReMesh* KEPWidgetMeshes::getTransWidget(int axis) {
	ReMesh* mesh = NULL;
#ifdef _ClientOutput
	CMaterialObject* material;
	Matrix44f matrix, rY;

	if (axis == 0) {
		ConvertRotationY(M_PI_2, out(rY));
		ConvertRotationZ(-M_PI_2, out(matrix));
		matrix = rY * matrix;
		mesh = loadReMeshOBJ(MeshStrings::TRANSLATION_WIDGET, &matrix);
		material = getXMaterial();
	} else if (axis == 1) {
		mesh = loadReMeshOBJ(MeshStrings::TRANSLATION_WIDGET);
		material = getYMaterial();
	} else {
		ConvertRotationY(-M_PI_2, out(rY));
		ConvertRotationX(-M_PI_2, out(matrix));
		matrix = rY * matrix;
		mesh = loadReMeshOBJ(MeshStrings::TRANSLATION_WIDGET, &matrix);
		material = getZMaterial();
	}

	if (material) {
		mesh->SetMaterial(material->GetReMaterial());
	}
#endif
	return mesh;
}

ReMesh* KEPWidgetMeshes::getPlaneWidget(Axis axis1, Axis axis2) {
	ReMesh* mesh = NULL;
#ifdef _ClientOutput
	CMaterialObject* material;

	if (axis1 == XAxis || axis2 == XAxis) {
		if (axis1 == YAxis || axis2 == YAxis) {
			// xy plane
			Matrix44f matrix, ry;

			ConvertRotationX(M_PI_2, out(matrix));
			ConvertRotationY(M_PI_2, out(ry));
			matrix = matrix * ry;
			mesh = loadReMeshOBJ(MeshStrings::PLANE_WIDGET, &matrix);
			material = getYZPlaneMaterial();
			if (mesh && material) {
				mesh->SetMaterial(material->GetReMaterial());
			}
		} else if (axis1 == ZAxis || axis2 == ZAxis) {
			// xz plane
			Matrix44f matrix;
			ConvertRotationZ(-M_PI_2, out(matrix));
			mesh = loadReMeshOBJ(MeshStrings::PLANE_WIDGET, &matrix);
			material = getXZPlaneMaterial();
			if (mesh && material) {
				mesh->SetMaterial(material->GetReMaterial());
			}
		} else {
			return NULL;
		}
	} else if (axis1 == YAxis || axis2 == YAxis) {
		if (axis1 == ZAxis || axis2 == ZAxis) {
			// yz plane
			mesh = loadReMeshOBJ(MeshStrings::PLANE_WIDGET);
			material = getXYPlaneMaterial();
			if (mesh && material) {
				mesh->SetMaterial(material->GetReMaterial());
			}
		} else {
			return NULL;
		}
	} else {
		return NULL;
	}
#endif
	return mesh;
}

ReMesh* KEPWidgetMeshes::getRotationWidget(Axis axis) {
	ReMesh* mesh = NULL;
#ifdef _ClientOutput
	CMaterialObject* material;
	Matrix44f matrix, rY;

	if (axis == XAxis) {
		ConvertRotationY(M_PI_2, out(rY));
		ConvertRotationZ(-M_PI_2, out(matrix));
		matrix = rY * matrix;
		mesh = loadReMeshOBJ(MeshStrings::ROTATION_WIDGET, &matrix);
		material = getXMaterial();
	} else if (axis == YAxis) {
		mesh = loadReMeshOBJ(MeshStrings::ROTATION_WIDGET);
		material = getYMaterial();
	} else {
		ConvertRotationY(-M_PI_2, out(rY));
		ConvertRotationX(-M_PI_2, out(matrix));
		matrix = rY * matrix;
		mesh = loadReMeshOBJ(MeshStrings::ROTATION_WIDGET, &matrix);
		material = getZMaterial();
	}

	if (material && mesh) {
		mesh->SetMaterial(material->GetReMaterial());
	}

#endif
	return mesh;
}

ReMesh* KEPWidgetMeshes::getConeMesh(double length, double radius, int rimCount) {
	CEXMeshObj* coneCEXMesh = new CEXMeshObj();
	coneCEXMesh->m_abVertexArray.m_uvCount = 0;
	coneCEXMesh->m_abVertexArray.m_indecieCount = rimCount * 2 * 3;
	coneCEXMesh->m_abVertexArray.m_vertexCount = rimCount + 2;
	coneCEXMesh->m_materialObject = new CMaterialObject(); // make a throw away material

	UINT* indexArray = new UINT[rimCount * 2 * 3];
	ABVERTEX* vertexArray = new ABVERTEX[rimCount + 2];

	ZeroMemory(vertexArray, (sizeof(ABVERTEX) * (rimCount + 2)));

	vertexArray[0].x = 0.0f;
	vertexArray[0].y = 0.0f;
	vertexArray[0].z = 0.0f;
	for (int i = 0; i < rimCount; i++) {
		double angle = i * 2.0 * M_PI / rimCount;
		vertexArray[i + 1].x = cos(angle) * radius;
		vertexArray[i + 1].y = sin(angle) * radius;
		vertexArray[i + 1].z = length;
	}
	vertexArray[rimCount + 1].x = 0.0f;
	vertexArray[rimCount + 1].y = 0.0f;
	vertexArray[rimCount + 1].z = length;

	for (int i = 0; i < rimCount; i++) {
		indexArray[(6 * i) + 1] = 0;
		indexArray[(6 * i) + 0] = i + 1;
		indexArray[(6 * i) + 3] = rimCount + 1;
		indexArray[(6 * i) + 4] = i + 1;
		if (i < rimCount - 1) {
			indexArray[(6 * i) + 2] = i + 2;
			indexArray[(6 * i) + 5] = i + 2;
		} else {
			indexArray[(6 * i) + 2] = 1;
			indexArray[(6 * i) + 5] = 1;
		}
	}

	coneCEXMesh->m_abVertexArray.SetABVERTEXStyle(NULL, vertexArray, rimCount + 2);
	coneCEXMesh->m_abVertexArray.SetVertexIndexArray(indexArray, rimCount * 2 * 3, rimCount + 2);
	ReMesh* rm = NULL;
#ifdef _ClientOutput
	rm = MeshRM::Instance()->CreateReStaticMesh(coneCEXMesh);
#endif
	coneCEXMesh->SafeDelete();
	delete coneCEXMesh;
	delete[] indexArray;
	delete[] vertexArray;
	return rm;
}

inline Vector3f ringNormal(const Vector3f& ringCenter, float x, float y, float z) {
	Vector3f normal;
	normal.x = x - ringCenter.x;
	normal.y = y - ringCenter.y;
	normal.z = z - ringCenter.z;

	normal.Normalize();
	return normal;
}

ReMesh* KEPWidgetMeshes::getRingMesh(double angle, double radius, int angleCount /*= 36*/, double height /*= 0.5*/, double width /*= 0.5*/) {
	int sectionCount = angleCount - 1;
	const int VertsPerTri = 3;
	const int VertsPerSec = 4;
	const int TrisPerSec = 8;

	UINT* indexArray = new UINT[sectionCount * TrisPerSec * VertsPerTri];
	ABVERTEX* vertexArray = new ABVERTEX[angleCount * VertsPerSec];

	ZeroMemory(vertexArray, (sizeof(ABVERTEX) * angleCount * VertsPerSec));
	ZeroMemory(indexArray, (sizeof(UINT) * sectionCount * TrisPerSec * VertsPerTri));

	for (int i = 0; i < angleCount; i++) {
		double tempAngle = (i * angle / (double)(angleCount - 1.0)) - (angle / 2.0) + (M_PI / 2.0);
		if (tempAngle < 0)
			tempAngle = tempAngle + (2.0 * M_PI);

		Vector3f center, tempNormal;
		center.x = (float)(cos(tempAngle) * (radius - (width / 2.0)));
		center.y = 0.0f;
		center.z = (float)(sin(tempAngle) * (radius - (width / 2.0)));

		vertexArray[(VertsPerSec * i) + 0].x = (float)(cos(tempAngle) * (radius - width));
		vertexArray[(VertsPerSec * i) + 0].y = (float)(height / 2.0);
		vertexArray[(VertsPerSec * i) + 0].z = (float)(sin(tempAngle) * (radius - width));
		tempNormal = ringNormal(center,
			vertexArray[(VertsPerSec * i) + 0].x,
			vertexArray[(VertsPerSec * i) + 0].y,
			vertexArray[(VertsPerSec * i) + 0].z);
		vertexArray[(VertsPerSec * i) + 0].nx = tempNormal.x;
		vertexArray[(VertsPerSec * i) + 0].ny = tempNormal.y;
		vertexArray[(VertsPerSec * i) + 0].nz = tempNormal.z;

		vertexArray[(VertsPerSec * i) + 1].x = (float)(cos(tempAngle) * radius);
		vertexArray[(VertsPerSec * i) + 1].y = (float)(height / 2.0);
		vertexArray[(VertsPerSec * i) + 1].z = (float)(sin(tempAngle) * radius);
		tempNormal = ringNormal(center,
			vertexArray[(VertsPerSec * i) + 1].x,
			vertexArray[(VertsPerSec * i) + 1].y,
			vertexArray[(VertsPerSec * i) + 1].z);
		vertexArray[(VertsPerSec * i) + 1].nx = tempNormal.x;
		vertexArray[(VertsPerSec * i) + 1].ny = tempNormal.y;
		vertexArray[(VertsPerSec * i) + 1].nz = tempNormal.z;

		vertexArray[(VertsPerSec * i) + 2].x = (float)(cos(tempAngle) * (radius - width));
		vertexArray[(VertsPerSec * i) + 2].y = (float)(-1.0 * height / 2.0);
		vertexArray[(VertsPerSec * i) + 2].z = (float)(sin(tempAngle) * (radius - width));
		tempNormal = ringNormal(center,
			vertexArray[(VertsPerSec * i) + 2].x,
			vertexArray[(VertsPerSec * i) + 2].y,
			vertexArray[(VertsPerSec * i) + 2].z);
		vertexArray[(VertsPerSec * i) + 2].nx = tempNormal.x;
		vertexArray[(VertsPerSec * i) + 2].ny = tempNormal.y;
		vertexArray[(VertsPerSec * i) + 2].nz = tempNormal.z;

		vertexArray[(VertsPerSec * i) + 3].x = (float)(cos(tempAngle) * radius);
		vertexArray[(VertsPerSec * i) + 3].y = (float)(-1.0f * height / 2.0f);
		vertexArray[(VertsPerSec * i) + 3].z = (float)(sin(tempAngle) * radius);
		tempNormal = ringNormal(center,
			vertexArray[(VertsPerSec * i) + 3].x,
			vertexArray[(VertsPerSec * i) + 3].y,
			vertexArray[(VertsPerSec * i) + 3].z);
		vertexArray[(VertsPerSec * i) + 3].nx = tempNormal.x;
		vertexArray[(VertsPerSec * i) + 3].ny = tempNormal.y;
		vertexArray[(VertsPerSec * i) + 3].nz = tempNormal.z;
	}

	for (int i = 0; i < sectionCount; i++) {
		indexArray[(TrisPerSec * VertsPerTri * i) + 0] = (VertsPerSec * i) + 1;
		indexArray[(TrisPerSec * VertsPerTri * i) + 1] = (VertsPerSec * i) + 0;
		indexArray[(TrisPerSec * VertsPerTri * i) + 2] = (VertsPerSec * i) + 4;

		indexArray[(TrisPerSec * VertsPerTri * i) + 3] = (VertsPerSec * i) + 5;
		indexArray[(TrisPerSec * VertsPerTri * i) + 4] = (VertsPerSec * i) + 1;
		indexArray[(TrisPerSec * VertsPerTri * i) + 5] = (VertsPerSec * i) + 4;

		indexArray[(TrisPerSec * VertsPerTri * i) + 6] = (VertsPerSec * i) + 6;
		indexArray[(TrisPerSec * VertsPerTri * i) + 7] = (VertsPerSec * i) + 0;
		indexArray[(TrisPerSec * VertsPerTri * i) + 8] = (VertsPerSec * i) + 2;

		indexArray[(TrisPerSec * VertsPerTri * i) + 9] = (VertsPerSec * i) + 6;
		indexArray[(TrisPerSec * VertsPerTri * i) + 10] = (VertsPerSec * i) + 4;
		indexArray[(TrisPerSec * VertsPerTri * i) + 11] = (VertsPerSec * i) + 0;

		indexArray[(TrisPerSec * VertsPerTri * i) + 12] = (VertsPerSec * i) + 3;
		indexArray[(TrisPerSec * VertsPerTri * i) + 13] = (VertsPerSec * i) + 7;
		indexArray[(TrisPerSec * VertsPerTri * i) + 14] = (VertsPerSec * i) + 6;

		indexArray[(TrisPerSec * VertsPerTri * i) + 15] = (VertsPerSec * i) + 2;
		indexArray[(TrisPerSec * VertsPerTri * i) + 16] = (VertsPerSec * i) + 3;
		indexArray[(TrisPerSec * VertsPerTri * i) + 17] = (VertsPerSec * i) + 6;

		indexArray[(TrisPerSec * VertsPerTri * i) + 18] = (VertsPerSec * i) + 1;
		indexArray[(TrisPerSec * VertsPerTri * i) + 19] = (VertsPerSec * i) + 5;
		indexArray[(TrisPerSec * VertsPerTri * i) + 20] = (VertsPerSec * i) + 7;

		indexArray[(TrisPerSec * VertsPerTri * i) + 21] = (VertsPerSec * i) + 3;
		indexArray[(TrisPerSec * VertsPerTri * i) + 22] = (VertsPerSec * i) + 1;
		indexArray[(TrisPerSec * VertsPerTri * i) + 23] = (VertsPerSec * i) + 7;
	}

	std::vector<Vector3f> outvertex(sectionCount * TrisPerSec * VertsPerTri);
	std::vector<Vector3f> outnormal(sectionCount * TrisPerSec * VertsPerTri);
	std::vector<Vector2f> outuv(sectionCount * TrisPerSec * VertsPerTri);

	for (int fv = 0; fv < sectionCount * TrisPerSec * VertsPerTri; fv++) {
		int vertexIndex = indexArray[fv];

		outvertex[fv].x = vertexArray[vertexIndex].x;
		outvertex[fv].y = vertexArray[vertexIndex].y;
		outvertex[fv].z = vertexArray[vertexIndex].z;

		outnormal[fv].x = vertexArray[vertexIndex].nx;
		outnormal[fv].y = vertexArray[vertexIndex].ny;
		outnormal[fv].z = vertexArray[vertexIndex].nz;

		outuv[fv].x = 0.0f;
		outuv[fv].y = 0.0f;
	}

	ReMeshData data;
	ZeroMemory(&data, sizeof(data));

	data.pP = &outvertex[0];
	data.pN = &outnormal[0];
	data.pUv = &outuv[0];
	data.pI = 0;
	data.PrimType = ReMeshPrimType::TRILIST;
	data.NumV = sectionCount * TrisPerSec * VertsPerTri;
	data.NumUV = 1;
	ReMesh* mesh = MeshRM::Instance()->CreateReStaticMesh(&data, false);
	delete[] indexArray;
	delete[] vertexArray;

	return mesh;
}

KEP3DObjectWidget::KEP3DObjectWidget() :
		m_xMesh(NULL),
		m_yMesh(NULL),
		m_zMesh(NULL),
		m_xyPlane(NULL),
		m_xzPlane(NULL),
		m_yzPlane(NULL),
		m_showWidget(true),
		m_visible(false),
		m_advancedMovement(true),
		m_currentlyDragging(NoAxis),
		m_dragStartAngle(0.0f),
		m_planeDist(0.0f),
		m_axisOrientation(ObjectSpace),
		m_dragMultiplier(0.0f),
		m_currentSelectionMode(Unselected) {
	m_locationMat.MakeIdentity();
	m_rotationMat[0].MakeIdentity();
	m_rotationMat[1].MakeIdentity();
	m_rotationMat[2].MakeIdentity();
	m_rotationAngle.Set(0, 0, 0);
	m_widgetOffset.Set(0, 0, 0);
	m_dragStartPos.Set(0, 0);
}

KEP3DObjectWidget::~KEP3DObjectWidget() {
	DeleteMeshesAndPlanes();
}

void KEP3DObjectWidget::DeleteMeshesAndPlanes() {
	if (m_xMesh)
		delete m_xMesh;
	m_xMesh = NULL;
	if (m_yMesh)
		delete m_yMesh;
	m_yMesh = NULL;
	if (m_zMesh)
		delete m_zMesh;
	m_zMesh = NULL;
	if (m_xyPlane)
		delete m_xyPlane;
	m_xyPlane = NULL;
	if (m_xzPlane)
		delete m_xzPlane;
	m_xzPlane = NULL;
	if (m_yzPlane)
		delete m_yzPlane;
	m_yzPlane = NULL;
}

Vector3f KEP3DObjectWidget::getWidgetPosition() {
	return m_locationMat.GetTranslation();
}

void KEP3DObjectWidget::switchTranslation() {
	// DRF - Delete Meshes & Planes
	DeleteMeshesAndPlanes();

	// Create New Meshes
	m_xMesh = KEPWidgetMeshes::getXTransWidget();
	m_yMesh = KEPWidgetMeshes::getYTransWidget();
	m_zMesh = KEPWidgetMeshes::getZTransWidget();

	// Create New Planes
	m_xzPlane = KEPWidgetMeshes::getPlaneWidget(XAxis, ZAxis);
	m_xyPlane = KEPWidgetMeshes::getPlaneWidget(XAxis, YAxis);
	m_yzPlane = KEPWidgetMeshes::getPlaneWidget(YAxis, ZAxis);
}

void KEP3DObjectWidget::switchRotation() {
	// DRF - Delete Meshes & Planes
	DeleteMeshesAndPlanes();

	// Create New Meshes
	m_yMesh = KEPWidgetMeshes::getRotationWidget(YAxis);
}

void KEP3DObjectWidget::ModeOff() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
	m_visible = false;
	if (pICE->ObjectsSelected() == 0) {
		m_currentSelectionMode = Unselected;
	} else if (pICE->ObjectsSelected() == 1) {
		m_currentSelectionMode = Selected_Single_None;
	} else if (pICE->ObjectsSelected() > 1) {
		m_currentSelectionMode = Selected_Multi_None;
	}
}

void KEP3DObjectWidget::ModeTranslate(float pivotX, float pivotY, float pivotZ) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
	m_visible = m_showWidget;
	switchTranslation();
	m_currentSelectionMode = (pICE->ObjectsSelected() > 1) ? Selected_Multi_Translation : Selected_Single_Translation;
	if (m_axisOrientation == WorldSpace)
		m_locationMat.MakeIdentity();
	m_locationMat.GetTranslation() = Vector3f(pivotX, pivotY, pivotZ);
}

void KEP3DObjectWidget::ModeRotate(float pivotX, float pivotY, float pivotZ) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
	m_visible = m_showWidget;
	switchRotation();
	m_currentSelectionMode = (pICE->ObjectsSelected() > 1) ? Selected_Multi_Rotation : Selected_Single_Rotation;
	m_locationMat.GetTranslation() = Vector3f(pivotX, pivotY, pivotZ);
}

bool CalculateAxisGrabPointFromRay(int axis, const Vector3f& rayOrigin, const Vector3f& rayDirection, float* pfAxisCoordinate) {
	Vector3f vAxis = Vector3f::Basis(axis);
	if (ClosestPointOnLines(Vector3f(0, 0, 0), vAxis, rayOrigin, rayDirection, pfAxisCoordinate, nullptr))
		return true;
	*pfAxisCoordinate = 0;
	return false;
}

bool KEP3DObjectWidget::handleTranslationAxisDragging(int axis, Vector3f osDirection, Vector3f osOrigin) {
#ifdef _ClientOutput
	float fPositionOnAxis;
	if (CalculateAxisGrabPointFromRay(axis, osOrigin, osDirection, &fPositionOnAxis)) {
		float fDelta = fPositionOnAxis - m_dragStartPos.x;
		m_locationMat[3].SubVector<3>() += fDelta * m_locationMat[axis].SubVector<3>();
		return true;
	}
#endif
	return false;
}

static void setCursorPos(POINT& pnt) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	POINT newPnt;
	WINDOWINFO pwi;
	::GetWindowInfo(pICE->GetWindowHandle(), &pwi);
	newPnt.x = pnt.x + pwi.rcClient.left;
	newPnt.y = pnt.y + pwi.rcClient.top;
	if (newPnt.x < pwi.rcClient.left) {
		newPnt.x = pwi.rcClient.left;
	}
	if (newPnt.x > pwi.rcClient.right) {
		newPnt.x = pwi.rcClient.right;
	}
	if (newPnt.y < pwi.rcClient.top) {
		newPnt.y = pwi.rcClient.top;
	}
	if (newPnt.y > pwi.rcClient.bottom) {
		newPnt.y = pwi.rcClient.bottom;
	}
	::SetCursorPos(newPnt.x, newPnt.y);
}

static POINT getScreenPos(Vector3f& worldPos) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return POINT();

	Vector3f screenPos;
	pICE->WorldToScreen(worldPos, &screenPos);
	POINT result;
	result.x = screenPos.x;
	result.y = screenPos.y;
	return result;
}

static POINT getScreenPos(Matrix44f& mat) {
	Vector3f worldPos = mat.GetTranslation();
	return getScreenPos(worldPos);
}

static POINT getCursorPos() {
	POINT win_cursor_pos;
	::GetCursorPos(&win_cursor_pos);
	return win_cursor_pos;
}

void KEP3DObjectWidget::saveCursorPos(const Vector3f& intersection) {
	m_cursorPos = intersection;
}

void KEP3DObjectWidget::restoreCursorPos() {
	// convert the widget space cursor position back to world space
	// using the current widget world transform
	Vector3f cursorPos = TransformPoint(m_locationMat, m_cursorPos);
	// then convert to screen space
	POINT pt = getScreenPos(cursorPos);
	// and set the cursor pos
	setCursorPos(pt);
}

bool KEP3DObjectWidget::handleTranslationPlaneDragging(int axis, Vector3f osDirection, Vector3f osOrigin) {
#ifdef _ClientOutput
	if (m_advancedMovement && axis == OnObject) {
		axis = XZPlane;
	}
	if (axis != OnObject) {
		Vector3f planeNormal, intersection;
		float planeDist;

		// Get plane equation for whichever plane is currently being used to drag
		// (drag plane is returned in object space)
		getDragPlane(planeNormal, planeDist, axis);

		// Find object-space point where ray intersects plane
		if (rayIntersectPlane(intersection, osOrigin, osDirection, planeNormal, planeDist)) {
			// Keep widget offset from intersection point by fixed amount (m_widgetOffset is in object space!)
			intersection += m_widgetOffset;

			// Transform object-space intersection to world-space ... this accounts for the mode
			// of the widget (absolute vs. object) because in absolute mode m_locationMat will be identity.
			Vector3f worldIntersection = TransformPoint(m_locationMat, intersection);

			// Update transformation matrix
			m_locationMat.GetTranslation() = worldIntersection;
		}
	} else {
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return false;

		RECT window_rect;
		::GetWindowRect(pICE->GetWindowHandle(), &window_rect);
		POINT center;
		center.x = (window_rect.left + window_rect.right) / 2;
		center.y = (window_rect.top + window_rect.bottom) / 2;

		POINT pnt = getCursorPos();
		static float SCALE = 0.02f; // 0.02 is similar to the speed of movement when grabbing the widget close to the camera.
		float diff[2] = { (center.x - pnt.x) * SCALE, (center.y - pnt.y) * SCALE };

		// need to calculate the direction of vertical mouse movement, so use the
		// direction the camera is pointing projected onto XZ and normalized

		// This won't work if the camera is vertical, because the camDir
		// will come back with very small values for x and z.
		Vector3f camDir = pICE->GetActiveCameraOrientationForMe();
		Vector3f verticalMouseMovement = Vector3f(camDir.x, 0.f, camDir.z);
		if (verticalMouseMovement.LengthSquared() < 0.2f) {
			// if the camera direction approaches the vertical, then use the
			// up vector, which will be tilted in the direction I want to move.
			// The camera can't roll, so I don't need to worry about that case.
			Vector3f camUp = pICE->GetActiveCameraUpVectorForMe();
			verticalMouseMovement = Vector3f(camUp.x, 0.f, camUp.z);
		}

		verticalMouseMovement.Normalize();

		// Find the opposite direction for horizontal mouse movement.
		Vector3f horizontalMouseMovement;
		Vector3f upVec(0, 1.f, 0);
		horizontalMouseMovement = Cross(verticalMouseMovement, upVec);

		// this might not be necessary in this case
		horizontalMouseMovement.Normalize();

		// Use the mouse movement as a factor to scale the direction vectors and use the result to move
		// the object in world space.
		Matrix44f newLocation = m_locationMat;

		newLocation(3, 0) += horizontalMouseMovement.x * diff[0] + verticalMouseMovement.x * diff[1]; // x
		newLocation(3, 1) += 0; // y
		newLocation(3, 2) += horizontalMouseMovement.z * diff[0] + verticalMouseMovement.z * diff[1]; // z

		pnt = getScreenPos(newLocation);
		POINT oldPnt = getScreenPos(m_locationMat);
		m_dragStartPos.x = pnt.x;
		m_dragStartPos.y = pnt.y;
		if (pnt.x > 0 && pnt.x < window_rect.right - window_rect.left &&
			pnt.y > 0 && pnt.y < window_rect.bottom - window_rect.top) {
			m_locationMat = newLocation; // only move the object if that new location is on the screen.
		} else if (pnt.x < 0 && pnt.x > oldPnt.x) {
			if ((pnt.y < 0 && pnt.y >= oldPnt.y) ||
				(pnt.y > window_rect.bottom - window_rect.top && pnt.y <= oldPnt.y) ||
				(pnt.y > 0 && pnt.y < window_rect.bottom - window_rect.top)) {
				m_locationMat = newLocation; // off screen to left moving right
			}
		} else if (pnt.x > window_rect.right - window_rect.left && pnt.x <= oldPnt.x) {
			if ((pnt.y < 0 && pnt.y >= oldPnt.y) ||
				(pnt.y > window_rect.bottom - window_rect.top && pnt.y <= oldPnt.y) ||
				(pnt.y > 0 && pnt.y < window_rect.bottom - window_rect.top)) {
				m_locationMat = newLocation; // off screen to the right moving left
			}
		} else if (pnt.y < 0 && pnt.y >= oldPnt.y) {
			if ((pnt.x < 0 && pnt.x >= oldPnt.x) ||
				(pnt.x > window_rect.right - window_rect.left && pnt.x <= oldPnt.x) ||
				(pnt.x > 0 && pnt.x < window_rect.right - window_rect.left)) {
				m_locationMat = newLocation; // off screen to the top moving down
			}
		} else if (pnt.y > window_rect.bottom - window_rect.top && pnt.y <= oldPnt.y) {
			if ((pnt.x < 0 && pnt.x >= oldPnt.x) ||
				(pnt.x > window_rect.right - window_rect.left && pnt.x <= oldPnt.x) ||
				(pnt.x > 0 && pnt.x < window_rect.right - window_rect.left)) {
				m_locationMat = newLocation; // off screen to the bottom  moving up
			}
		}

		if (!m_advancedMovement)
			::SetCursorPos(center.x, center.y);
	}
#endif
	return true;
}

bool KEP3DObjectWidget::handleRotationAxisDragging(Axis axis, Vector3f osDirection, Vector3f osOrigin) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	float mousePos = pICE->GetMouseNormalizedX();
	float diff = mousePos - m_dragStartPos.x;
	if (m_axisOrientation == WorldSpace) {
		float angle = m_dragStartAngle - (4 * M_PI * diff * m_dragMultiplier);
		switch (axis) {
			case XAxis:
				m_rotationAngle.x = angle;
				break;
			case YAxis:
				m_rotationAngle.y = angle;
				break;
			case ZAxis:
				m_rotationAngle.z = angle;
				break;
			default:
				// error msg
				break;
		}
		//
		// Update world-space rotations
		//
		setRotation(m_rotationAngle.x, m_rotationAngle.y, m_rotationAngle.z);
	} else {
		// It's a relative angle, for object space transform.
		float angle = 4 * M_PI * diff * m_dragMultiplier;
		m_dragStartPos.x = mousePos;
		//
		// Because the DO expects rotations about WS axes, I need for the rotation widget to
		// convert back, so that Select.lua can pass WS rotations to the DO rotate functions.
		// In other words, rotating in object space is simulated by the widget in order to
		// avoid additional complexity in the dynamic object.
		//
		// Find the local axis about which we would like to rotate
		Vector3f localAxis(0, 0, 0);
		switch (axis) {
			case XAxis:
				localAxis = m_locationMat.GetRowX();
				break;
			case YAxis:
				localAxis = m_locationMat.GetRowY();
				break;
			case ZAxis:
				localAxis = m_locationMat.GetRowZ();
				break;
			default:
				// error msg
				break;
		}
		Vector3f pos = m_locationMat.GetTranslation();
		localAxis.Normalize(); // shouldn't be necessary
		// Construct the rotation matrix
		Matrix44f rotMat;
		ConvertRotation(localAxis, angle, out(rotMat));
		// Apply the rotation to the current transform
		m_locationMat = m_locationMat * rotMat;
		m_locationMat.GetTranslation() = pos;

		// Get the world space rotation angles from the current m_locationMat.
		MatrixARB::RotationMatrixToYRot(m_locationMat, m_rotationAngle.x, m_rotationAngle.y, m_rotationAngle.z);
	}
#endif

	return true;
}

bool KEP3DObjectWidget::handleMouse(
	int clickState,
	int axis,
	const Vector3f& intersection,
	const Vector3f& origin,
	const Vector3f& direction,
	const Vector3f& cameraPos) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	FLOAT determinant = 0.0f;
	Matrix44f matVisualInv;

	if (m_axisOrientation == ObjectSpace) {
		matVisualInv.MakeInverseOf(m_locationMat, &determinant);
	} else {
		// world space
		Matrix44f tempTrans;
		tempTrans.MakeTranslation(m_locationMat.GetTranslation());
		matVisualInv.MakeInverseOf(tempTrans, &determinant);
	}

	if (clickState == KEP_WIDGET_L_MOUSE_UP) {
		// on mouse up release any widgets you are currently dragging
		m_currentlyDragging = NoAxis;
		return false;
	}

	if (determinant > 1e-5) {
		Vector3f osOrigin, osDirection;

		// get ray in object space
		/* transform ray origin */
		osOrigin = TransformPoint_NonPerspective(matVisualInv, origin);

		/* transform and normalize ray direction */
		osDirection = TransformVector(matVisualInv, direction);
		osDirection.Normalize();

		if (clickState == KEP_WIDGET_L_MOUSE_DRAG && isWidgetModeTranslation() && (m_currentlyDragging != NoAxis)) {
			if (m_currentlyDragging == XAxis) {
				return handleTranslationAxisDragging(0, osDirection, osOrigin);
			} else if (m_currentlyDragging == YAxis) {
				return handleTranslationAxisDragging(1, osDirection, osOrigin);
			} else if (m_currentlyDragging == ZAxis) {
				return handleTranslationAxisDragging(2, osDirection, osOrigin);
			} else if (m_currentlyDragging > ZAxis) {
				return handleTranslationPlaneDragging(m_currentlyDragging, osDirection, osOrigin);
			}
		}

		if (clickState == KEP_WIDGET_L_MOUSE_DRAG && isWidgetModeRotation() && (m_currentlyDragging != NoAxis)) {
			return handleRotationAxisDragging(m_currentlyDragging, osDirection, osOrigin);
		} else if (clickState == KEP_WIDGET_L_MOUSE_DRAG && m_currentlyDragging == NoAxis) {
			// sanity check we should not be in this state unless the menu blade screws with
			// input events
			return false;
		} else if (clickState == KEP_WIDGET_L_MOUSE_DOWN) {
			m_currentlyDragging = (Axis)axis;
			if (m_advancedMovement && m_currentlyDragging == OnObject)
				m_currentlyDragging = XZPlane;

			if (isWidgetModeTranslation()) {
				switch (m_currentlyDragging) {
					case XAxis:
					case YAxis:
					case ZAxis: {
						float fPositionOnAxis;
						size_t iAxis = m_currentlyDragging == XAxis ? 0 : m_currentlyDragging == YAxis ? 1 : 2;
						if (CalculateAxisGrabPointFromRay(iAxis, osOrigin, osDirection, &fPositionOnAxis))
							m_dragStartPos.x = fPositionOnAxis;
						else
							m_dragStartPos.x = intersection[iAxis];
						return true;
					}
					case XYPlane:
						m_widgetOffset = -intersection;
						m_planeDist = intersection.z;
						return true;
					case XZPlane:
						m_widgetOffset = -intersection;
						m_planeDist = intersection.y;
						return true;
					case YZPlane:
						m_widgetOffset = -intersection;
						m_planeDist = intersection.x;
						return true;
					case OnObject: {
						m_widgetOffset = Vector3f(0, 0, 0);
						m_planeDist = 0.0f;

						// testing
						POINT pnt = getScreenPos(m_locationMat);
						m_dragStartPos.x = pnt.x;
						m_dragStartPos.y = pnt.y;

						if (!m_advancedMovement) {
							RECT window_rect;
							::GetWindowRect(pICE->GetWindowHandle(), &window_rect);
							::SetCursorPos((window_rect.left + window_rect.right) / 2, (window_rect.bottom + window_rect.top) / 2);
						}
					}
						return true;
					case NoAxis:
						// clicked on something other than a movable object
						m_widgetOffset = Vector3f(0, 0, 0);
						m_planeDist = 0.0f;
						return false;
				}
			} else if (isWidgetModeRotation()) {
				if (axis != NoAxis) {
					m_dragStartPos.x = pICE->GetMouseNormalizedX();
					switch (axis) {
						case XAxis:
							m_dragStartAngle = m_rotationAngle.x;
							break;
						case YAxis:
							m_dragStartAngle = m_rotationAngle.y;
							break;
						case ZAxis:
							m_dragStartAngle = m_rotationAngle.z;
							break;
					}

					// Get world space point of intersection
					Vector3f worldIntersection = TransformPoint(m_locationMat, intersection);

					// Compute vectors from widget origin to intersection point, and to camera
					Vector3f p, q;
					p = worldIntersection - getWidgetPosition();
					q = cameraPos - getWidgetPosition();

					if (p.Dot(q) > 0.0f) {
						m_dragMultiplier = -1.0f;
					} else {
						m_dragMultiplier = 1.0f;
					}
					return true;
				} else {
					return false;
				}
			}
		}
	}
#endif
	return false;
}

void KEP3DObjectWidget::setPosition(float x, float y, float z) {
	m_locationMat.GetTranslation() = Vector3f(x, y, z);
	for (int i = 0; i < 3; ++i) {
		m_rotationMat[i].GetTranslation() = Vector3f(x, y, z);
	}
}

void KEP3DObjectWidget::setRotation(float x, float y, float z) {
	// DRF - Bug Fix
	// Platform Only Supports Y rotation!
	x = z = 0;
	y = RadNorm(y);

	// Save Current Position
	Vector3f pos = m_locationMat.GetTranslation();

	// Set Widget Rotation
	m_rotationAngle.Set(x, y, z);

	// Update world-space rotations.
	//
	ConvertRotationX(x, out(m_rotationMat[0]));
	ConvertRotationY(y, out(m_rotationMat[1]));
	ConvertRotationZ(z, out(m_rotationMat[2]));
	//
	// The angles are very likely to have come from a rotation
	// matrix.  Make sure I re-build the same matrix by re-applying
	// the rotations in the correct order, i.e. ZYX
	//
	m_locationMat = m_rotationMat[2] * m_rotationMat[1] * m_rotationMat[0];

	m_locationMat.GetTranslation() = pos;
	for (int i = 0; i < 3; ++i) {
		m_rotationMat[i].GetTranslation() = pos;
	}
}

void KEP3DObjectWidget::switchWorldSpace() {
	//
	// Switch the widget into world space orientation, so that
	// translations and rotations happens along and around world
	// space axes.  In other words, don't have a rotation in the
	// transform for the widget, only the dynamic object itself.
	//
	m_axisOrientation = WorldSpace;
	ConvertRotationX(m_rotationAngle.x, out(m_rotationMat[0]));
	ConvertRotationY(m_rotationAngle.y, out(m_rotationMat[1]));
	ConvertRotationZ(m_rotationAngle.z, out(m_rotationMat[2]));
	m_locationMat.MakeTranslation(m_locationMat.GetTranslation());
}

void KEP3DObjectWidget::showWidget(bool show) {
	m_visible = m_showWidget = show;
}

void KEP3DObjectWidget::advancedMovement(bool adv) {
	m_advancedMovement = adv;
}

void KEP3DObjectWidget::switchObjectSpace() {
	//
	// Transform the widget into object space, using the current world-space
	// rotations.
	//
	m_axisOrientation = ObjectSpace;
	setRotation(m_rotationAngle.x, m_rotationAngle.y, m_rotationAngle.z);
}

void KEP3DObjectWidget::setHover(Axis axis, bool hover) {
	// This got complicated because of charles's render state sorting have to go thru CMaterial.
	ReMesh* mesh = NULL;
	CMaterialObject* material = NULL;
	D3DXCOLOR col;
	switch (axis) {
		case XAxis:
			mesh = m_xMesh;
			material = KEPWidgetMeshes::getXMaterial();
			col = KEPWidgetMeshes::X_COLOR;
			break;
		case YAxis:
			mesh = m_yMesh;
			material = KEPWidgetMeshes::getYMaterial();
			col = KEPWidgetMeshes::Y_COLOR;
			break;
		case ZAxis:
			mesh = m_zMesh;
			material = KEPWidgetMeshes::getZMaterial();
			col = KEPWidgetMeshes::Z_COLOR;
			break;
		case XYPlane:
			mesh = m_xyPlane;
			material = KEPWidgetMeshes::getXYPlaneMaterial();
			col = KEPWidgetMeshes::PLANE_COLOR;
			break;
		case XZPlane:
			mesh = m_xzPlane;
			material = KEPWidgetMeshes::getXZPlaneMaterial();
			col = KEPWidgetMeshes::PLANE_COLOR;
			break;
		case YZPlane:
			mesh = m_yzPlane;
			material = KEPWidgetMeshes::getYZPlaneMaterial();
			col = KEPWidgetMeshes::PLANE_COLOR;
			break;
		default:
			break;
	}
	if (mesh && material) {
		if (hover) {
			material->m_useMaterial.Emissive = (ROLLOVER_EMISSIVE + MATERIAL_EMISSIVE) * col;
			material->m_baseMaterial.Emissive = material->m_useMaterial.Emissive;
#ifdef _ClientOutput
			MeshRM::Instance()->CreateReMaterial(material);
			material->GetReMaterial()->PreLight(FALSE);
			mesh->SetMaterial(material->GetReMaterial());
#endif
		} else {
			material->m_useMaterial.Emissive = MATERIAL_EMISSIVE * col;
			material->m_baseMaterial.Emissive = material->m_useMaterial.Emissive;
#ifdef _ClientOutput
			MeshRM::Instance()->CreateReMaterial(material);
			material->GetReMaterial()->PreLight(FALSE);
			mesh->SetMaterial(material->GetReMaterial());
#endif
		}
	}
}

void KEP3DObjectWidget::updateHoverState(int axis) {
	switch (axis) {
		case XAxis:
			setHover(XAxis, true);
			setHover(YAxis, false);
			setHover(ZAxis, false);
			setHover(XYPlane, false);
			setHover(XZPlane, false);
			setHover(YZPlane, false);
			break;

		case YAxis:
			setHover(XAxis, false);
			setHover(YAxis, true);
			setHover(ZAxis, false);
			setHover(XYPlane, false);
			setHover(XZPlane, false);
			setHover(YZPlane, false);
			break;

		case ZAxis:
			setHover(XAxis, false);
			setHover(YAxis, false);
			setHover(ZAxis, true);
			setHover(XYPlane, false);
			setHover(XZPlane, false);
			setHover(YZPlane, false);
			break;

		case XYPlane:
			setHover(XAxis, true);
			setHover(YAxis, true);
			setHover(ZAxis, false);
			setHover(XYPlane, true);
			setHover(XZPlane, false);
			setHover(YZPlane, false);
			break;

		case XZPlane:
			setHover(XAxis, true);
			setHover(YAxis, false);
			setHover(ZAxis, true);
			setHover(XYPlane, false);
			setHover(XZPlane, true);
			setHover(YZPlane, false);
			break;

		case YZPlane:
			setHover(XAxis, false);
			setHover(YAxis, true);
			setHover(ZAxis, true);
			setHover(XYPlane, false);
			setHover(XZPlane, false);
			setHover(YZPlane, true);
			break;

		default:
			setHover(XAxis, false);
			setHover(YAxis, false);
			setHover(ZAxis, false);
			setHover(XYPlane, false);
			setHover(XZPlane, false);
			setHover(YZPlane, false);
			break;
	}
}

bool KEP3DObjectWidget::getWidgetMatrixInverse(Matrix44f* m, float epsilon) {
	float determinant = 0.0f;
	m->MakeInverseOf(m_locationMat, &determinant);

	// Determinant should always be positive since only
	// rotation and translation taking place, but check anyway
	return determinant >= epsilon;
}

static bool transformRay(const Matrix44f& transform,
	const Vector3f& rayOrigin, const Vector3f& rayDirection,
	Vector3f& origin, Vector3f& direction) {
	FLOAT determinant = 0.0f;
	Matrix44f matInverse;
	matInverse.MakeInverseOf(transform, &determinant);

	if (determinant > 1e-5) {
		/* transform ray origin */
		origin = TransformPoint_NonPerspective(matInverse, rayOrigin);

		/* transform and normalize ray direction */
		direction = TransformVector(matInverse, rayDirection);
		direction.Normalize();
		return true;
	}
	return false;
}

int KEP3DObjectWidget::mouseOverTest(const Vector3f& rayOrigin, const Vector3f& rayDirection, Vector3f* location) const {
	Axis axis = NoAxis;
	float closest = FLT_MAX, dist;
	bool xzTest = false;
	Vector3f xzLoc;

	xzLoc.x = 0.0f;
	xzLoc.y = 0.0f;
	xzLoc.z = 0.0f;

	if (!m_visible) {
		return NoAxis;
	}

	// This is used for the transform in world or object space and the rotation
	// in object space.  Rotation in world space is a special case.
	Vector3f osOrigin, osDirection;
	if (transformRay(m_locationMat, rayOrigin, rayDirection, osOrigin, osDirection)) {
		Vector3f loc;

		//
		// If we're in object space, use m_locationMat for everything, otherwise,
		// if we're in rotation mode, use m_rotationMat for the axes.
		//
		if ((m_axisOrientation == WorldSpace) && isWidgetModeRotation()) {
			// in world space and rotation mode, so I need to use the inverse of the model's individual transform
			if (transformRay(m_rotationMat[0], rayOrigin, rayDirection, osOrigin, osDirection)) {
				if (m_xMesh && m_xMesh->IntersectRay(osOrigin, osDirection, &closest, &loc)) {
					axis = XAxis;
					if (location)
						*location = loc;
				}
			}

			if (transformRay(m_rotationMat[1], rayOrigin, rayDirection, osOrigin, osDirection)) {
				if (m_yMesh && m_yMesh->IntersectRay(osOrigin, osDirection, &dist, &loc) && dist < closest) {
					closest = dist;
					axis = YAxis;
					if (location)
						*location = loc;
				}
			}

			if (transformRay(m_rotationMat[2], rayOrigin, rayDirection, osOrigin, osDirection)) {
				if (m_zMesh && m_zMesh->IntersectRay(osOrigin, osDirection, &dist, &loc) && dist < closest) {
					closest = dist;
					axis = ZAxis;
					if (location)
						*location = loc;
				}
			}
		} else {
			// Translation and rotation in object space always use the m_locationMat transform.
			if (m_xMesh && m_xMesh->IntersectRay(osOrigin, osDirection, &closest, &loc)) {
				axis = XAxis;
				if (location)
					*location = loc;
			}

			if (m_yMesh && m_yMesh->IntersectRay(osOrigin, osDirection, &dist, &loc) && dist < closest) {
				closest = dist;
				axis = YAxis;
				if (location)
					*location = loc;
			}

			if (m_zMesh && m_zMesh->IntersectRay(osOrigin, osDirection, &dist, &loc) && dist < closest) {
				closest = dist;
				axis = ZAxis;
				if (location)
					*location = loc;
			}
		}

		if (closest == FLT_MAX) {
			// Only test planes if we missed the axis otherwise its too hard to select them
			if (m_xzPlane && m_xzPlane->IntersectRay(osOrigin, osDirection, &dist, &loc) && dist < closest) {
				closest = dist;
				axis = XZPlane;
				if (location) {
					*location = loc;
					xzLoc = loc;
					xzTest = true;
				}
			}

			if (m_xyPlane && m_xyPlane->IntersectRay(osOrigin, osDirection, &dist, &loc) && dist < closest) {
				closest = dist;
				axis = XYPlane;
				if (location)
					*location = loc;
			}

			if (m_yzPlane && m_yzPlane->IntersectRay(osOrigin, osDirection, &dist, &loc) && dist < closest) {
				closest = dist;
				axis = YZPlane;
				if (location)
					*location = loc;
			}
		}
	}

	if ((axis == YZPlane || axis == XYPlane) && xzTest) {
		// special case hitting xz and another plane
		*location = xzLoc;
		axis = XZPlane;
	}

	return axis;
}

void KEP3DObjectWidget::render() {
	if (!m_visible)
		return;

#ifdef _ClientOutput
	if (isWidgetModeTranslation()) {
		if (m_xMesh == NULL)
			switchTranslation();

		if (m_xMesh) {
			if (m_xMesh) {
				m_xMesh->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_xMesh, KEP_WIDGET_MESH_CACHE);
			}
			if (m_yMesh) {
				m_yMesh->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_yMesh, KEP_WIDGET_MESH_CACHE);
			}
			if (m_zMesh) {
				m_zMesh->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_zMesh, KEP_WIDGET_MESH_CACHE);
			}

			if (m_xzPlane) {
				m_xzPlane->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_xzPlane, KEP_WIDGET_MESH_CACHE);
			}
			if (m_xyPlane) {
				m_xyPlane->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_xyPlane, KEP_WIDGET_MESH_CACHE);
			}
			if (m_yzPlane) {
				m_yzPlane->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_yzPlane, KEP_WIDGET_MESH_CACHE);
			}
		}
	} else if (isWidgetModeRotation()) {
		if (m_yMesh == NULL)
			switchRotation();

		if (m_axisOrientation == WorldSpace) {
			// For world space, m_locationMat has no rotation component, which is fine for
			// translation, but I want to individually orient the rotation models to reflect
			// rotations about each world space axis, so I use the m_rotationMat matrices.
			if (m_xMesh) {
				m_xMesh->SetWorldMatrix(&m_rotationMat[0]);
				MeshRM::Instance()->LoadMeshCache(m_xMesh, KEP_WIDGET_MESH_CACHE);
			}
			if (m_yMesh) {
				m_yMesh->SetWorldMatrix(&m_rotationMat[1]);
				MeshRM::Instance()->LoadMeshCache(m_yMesh, KEP_WIDGET_MESH_CACHE);
			}
			if (m_zMesh) {
				m_zMesh->SetWorldMatrix(&m_rotationMat[2]);
				MeshRM::Instance()->LoadMeshCache(m_zMesh, KEP_WIDGET_MESH_CACHE);
			}
		} else {
			if (m_xMesh) {
				m_xMesh->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_xMesh, KEP_WIDGET_MESH_CACHE);
			}
			if (m_yMesh) {
				m_yMesh->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_yMesh, KEP_WIDGET_MESH_CACHE);
			}
			if (m_zMesh) {
				m_zMesh->SetWorldMatrix(&m_locationMat);
				MeshRM::Instance()->LoadMeshCache(m_zMesh, KEP_WIDGET_MESH_CACHE);
			}
		}
	}
#endif
}

void KEP3DObjectWidget::getDragPlane(Vector3f& normal, float& distance, int axis) {
	switch (axis) {
		case XYPlane:
			normal = Vector3f(0, 0, 1);
			distance = m_planeDist;
			break;

		case XZPlane:
			normal = Vector3f(0, 1, 0);
			distance = m_planeDist;
			break;

		case YZPlane:
			normal = Vector3f(1, 0, 0);
			distance = m_planeDist;
			break;
	}
}

//
// KEP3DObjectWidget::rayIntersectPlane
//   Computes the intersection point (if any) between a ray and the given plane.
//   'rayDirection' and 'planeNormal' need to be normalized
//
bool KEP3DObjectWidget::rayIntersectPlane(
	Vector3f& intersection,
	const Vector3f& rayOrigin,
	const Vector3f& rayDirection,
	const Vector3f& planeNormal,
	float planeDistance,
	float epsilon) {
	// (P + tD) dot N - d = 0
	// t = (d - (P dot N)) / (D dot N)

	float DDotN = rayDirection.Dot(planeNormal);
	float PDotN = rayOrigin.Dot(planeNormal);

	if (fabs(DDotN) > epsilon) {
		float t = (planeDistance - PDotN) / DDotN;

		if (t > 0.0f) {
			intersection = rayOrigin + t * rayDirection;
			return true;
		}
	}

	return false;
}

} // namespace KEP
