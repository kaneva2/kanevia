///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ContentService.h"

#include "KEPConfig.h"
#include "IClientEngine.h"
#include "ClientEngine_HttpClient.h"
#include "KEPHelpers.h"
#include "NetworkCfg.h"
#include "Event\EventSystem\Dispatcher.h"
#include "common\include\CompressHelper.h"
#include "common\KEPUtil\Helpers.h"
#include <Core/Util/CallbackSystem.h>

#undef max
#undef min

namespace KEP {

static LogInstance("Instance");

// Metadata
#define TAG_GLOBAL_ID "global_id"
#define TAG_BASE_GLOBAL_ID "base_global_id"
#define TAG_USE_TYPE "use_type"
#define TAG_USE_VALUE "use_value"
#define TAG_PASS_GROUP "pass_group_id"
#define TAG_EXCLUSION_GROUPS "exclusion_groups"
#define TAG_NAME "name"
#define TAG_DESCRIPTION "description"
#define TAG_RECORD "Record"
#define TAG_METADATA "metadata"
#define TAG_RETURN_CODE "ReturnCode"
#define TAG_RESULT_DESC "ResultDescription"

// Download URLs
#define TAG_PRIMARY_URL "primary_url" // Planned main URL replacement for "main_gz_url" and "mesh_url"
#define TAG_TEXTURE_URL "texture_url"
#define TAG_TEM_GZ_URL "template_gz_url" // now also linked to type 20 (.dds.lzma, DXT1 when possible, otherwise DXT5) and overrides type 10 (.krx.gz, mostly DXT5)
#define TAG_TEM_JPG_URL "tem_jpg" // JPG texture path template, include a quality macro in addition to {subid}. See TEXTURE_QUALITY_SUFFIX_MACRO.

// Texture average colors
#define TAG_TEXTURES "textures" // Will replace TAG_TEX_COLORS soon
#define TAG_TEXTURES_ATTR_FS "fs" // (Unique) Texture file store
#define TAG_TEX_COLORS "texture_colors"
#define TAG_T "t"
#define TAG_T_ATTR_O "o" // Texture selector key
#define TAG_T_ATTR_C "c" // Texture average color
#define TAG_T_ATTR_U "u" // Texture unique ID

#define TAG_THUMB_XL "thumbnailXLarge"

#define TAG_EXP_RUNS "expRuns"
#define TAG_EXP_TIME "expTime"
#define TAG_HITS "hits"

#define TAG_LOCAL_HASH "localHash"

#define ATTR_CACHE_VERSION "cacheVersion"
#define ATTR_PRELOAD_LIST_VERSION "preloadListVersion" // Version
#define ATTR_CACHE_DURATION "cacheDuration"
#define ATTR_TEXTURE_NUMBER_RADIX "textureNumberRadix"

static const TimeMs s_updateFlushMs = 100.0; // metadata request queue flush time (ms)

using namespace std;

ContentService ContentService::s_Instance;

string ContentMetadata::ToStr() const {
	string texStr = "none";
	if (!getPrimaryTextureUrl().empty()) {
		StrBuild(texStr, "'" << getPrimaryTextureUrl() << "' => '" << getPrimaryTexturePath() << "'");
	}
	string thumbStr = "none";
	if (!getThumbnailPath().empty()) {
		StrBuild(thumbStr, "'" << LogPath(getThumbnailPath()) << "'");
	}
	string str;
	StrBuild(str, "CM<"
					  << "hits=" << HitsGet()
					  << " exp={" << ExpRunsLeft() << " " << ExpTimeLeft() << "}"
					  << " glid<" << getGlobalId() << ":" << getBaseGlobalId()
					  << (PassGroupAP() ? " AP" : "")
					  << (PassGroupVIP() ? " VIP" : "")
					  << (TypeGameItem() ? " GI" : "")
					  << ">"
					  << " '" << getItemName() << "'"
					  << " tex<" << texStr << ">"
					  << " thumb<" << thumbStr << ">");
	return str;
}

ContentService::ContentService() {
	m_preloadListVersion = 0;
}

ContentService::~ContentService() {
}

ContentMetadata::ContentMetadata(const GLID& glid, bool cacheIfNotFound) {
	if (!ContentService::Instance()->ContentMetadataCacheGet(*this, glid, cacheIfNotFound))
		Reset();
}

void ContentService::ContentMetadataCacheAsync(const GLID& glid, IEvent* e) {
	std::function<void()> fnCallback;

	if (e) {
		// VS2015 limitation: can't capture std::unique_ptr in a std::function, so we have to wrap the event in a struct 'CallbackData'
		// that we capture as a std::shared_ptr.
		struct EventDelete {
			void operator()(IEvent* pEvent) const {
				if (pEvent)
					pEvent->Delete();
			}
		};
		struct CallbackData {
			std::unique_ptr<IEvent, EventDelete> m_pEvent;
		};
		std::shared_ptr<CallbackData> pCallbackData = std::make_shared<CallbackData>();
		pCallbackData->m_pEvent.reset(e);

		fnCallback = [this, pCallbackData]() { this->DispatchEvent(pCallbackData->m_pEvent.get()); };
	}

	ContentMetadataCacheAsync(glid, fnCallback, e);
}

void ContentService::ContentMetadataCacheAsync(const vector<GLID>& glids, std::function<void()> fnCallback, void* pCallbackOwner) {
	// Gather Valid Glids Not Already Cached
	vector<GLID> glidsValid;
	for (const auto& glid : glids) {
		if (!IS_VALID_CONTENTMETADATA_GLID(glid) || ContentMetadataIsCached(glid))
			continue;
		glidsValid.push_back(glid);
	}

	// Nothing To Do ? (shortcut the callback)
	if (glidsValid.empty()) {
		if (fnCallback)
			fnCallback();
		return;
	}

	// Register Callback Function (usually an event from function above)
	if (fnCallback) {
		std::vector<uint64_t> ids(glidsValid.begin(), glidsValid.end());
		CallbackSystem::Instance()->RegisterCallback(this, ids, pCallbackOwner, std::weak_ptr<void>(), std::move(fnCallback));
	}

	// Queue Glids Not Already Queued Or In Process For Metadata Download
	std::lock_guard<fast_recursive_mutex> lock(m_pendingRequestsSync);
	for (const auto& glid : glidsValid) {
		bool inQueue = (m_glidsQueued.find(glid) != m_glidsQueued.end());
		bool inProcess = !inQueue && (m_glidsInProcess.find(glid) != m_glidsInProcess.end());
		if (!inQueue && !inProcess) {
			m_glidsQueued.insert(glid);
			m_updateTimer.Reset();
		}
	}
}

// DRF - Builds an array of glids into a request set which aren't in the cache including
// everything if forceGet is true and then breaks it up into a number of manageable length
// contentMetadata.aspx webcalls to get the information for all glids.
void ContentService::ContentMetadataCacheSync(const vector<GLID>& glids, bool forceGet) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// Only Request Glids Not Already Cached
	set<GLID> request;
	for (size_t i = 0; i < glids.size(); i++)
		if (!ContentMetadataIsCached(glids[i]) || forceGet)
			request.insert(glids[i]);
	if (!request.size())
		return;

	// Break Into Manageable Length Webcalls
	const size_t maxGlids = GLIDS_PER_WEBCALL_MAX;
	bool done = false;
	size_t count = 0;
	set<GLID> subset;
	auto i = request.begin();
	while (!done) {
		// Build Subset Of Glids (only ones that actually have UGC metadata)
		subset.clear();
		while (i != request.end() && count < maxGlids) {
			GLID glid = *i;
			if (IS_VALID_CONTENTMETADATA_GLID(glid))
				subset.insert(glid);
			++count;
			++i;
		}
		count = 0;
		if (i == request.end())
			done = true;

		// Make ContentMetadata Request Webcall
		string requestUrl = MakeRequestURL(subset);
		if (!requestUrl.empty()) {
			DWORD status = 0;
			string resultText, statusDesc;
			if (pICE->_GetBrowserPage(requestUrl.c_str(), 0, 1, resultText, &status, statusDesc)) {
				// Parse Webcall Response Content Metadata Xml
				TiXmlDocument doc;
				doc.Parse(resultText.c_str());

				// Unpack Content Metadata Xml Into Cache (reset expiry, content being used right now)
				ContentMetadataXmlUnpackToCache(doc.RootElement(), true);
			}
		}
	}
}

size_t ContentService::ContentMetadataXmlUnpackToCache(const string& xml, bool overwrite) {
	// Valid Content Metadata Xml?
	if (xml.empty())
		return 0;

	// Parse Content Metadata Xml
	TiXmlDocument doc;
	doc.Parse(xml.c_str());
	if (doc.Error()) {
		LogError("xml.Parse() FAILED - xml=...\n"
				 << xml);
		return 0;
	}

	// Unpack Content Metadata Xml Into Cache (reset expiry, content being used right now)
	return ContentMetadataXmlUnpackToCache(doc.RootElement(), true, overwrite);
}

// DRF - Parses ContentMetadataUrl Request Response And Caches Glid Metadata
size_t ContentService::ContentMetadataXmlUnpackToCache(const TiXmlElement* rootElem, bool expiryReset, bool overwrite) {
	if (!rootElem) {
		LogError("rootElem is null");
		return 0;
	}

	// Cache UGC Metadata (one per glid)
	size_t numCached = 0;
	string strGlids = "< ";

	const TiXmlElement* elemRecord = rootElem->FirstChildElement(TAG_RECORD);

	while (elemRecord) {
		ContentMetadata md;
		if (md.loadFromXML(elemRecord)) {
			ContentMetadataCacheSet(md, expiryReset, overwrite);
			++numCached;
			if (numCached < 10) {
				StrAppend(strGlids, (ULONG)md.getGlobalId() << " ");
			}
		}
		elemRecord = elemRecord->NextSiblingElement(TAG_RECORD);
	}
	if (numCached >= 10) {
		StrAppend(strGlids, "... ");
	}
	StrAppend(strGlids, ">");

	if (numCached) {
		LogInfo("OK - cacheSize=" << ContentMetadataCacheSize() << " numCached=" << numCached << " glids" << strGlids);
	} else {
		LogWarn("NOTHING CACHED - cacheSize=" << ContentMetadataCacheSize());
	}

	return numCached;
}

void ContentService::ContentMetadataCacheLog(bool verbose) {
	std::lock_guard<fast_recursive_mutex> lock(m_glidsCachedMutex);
	LogInfo("cacheSize=" << ContentMetadataCacheSize() << " timeNowMin=" << ContentMetadata::TimeNowMin());
	if (!verbose)
		return;
	for (const auto& itr : m_glidsCached) {
		ContentMetadata md = itr.second;
		_LogInfo(" ... " << md.ToStr());
	}
}

size_t ContentService::ContentMetadataCacheSize() {
	std::lock_guard<fast_recursive_mutex> lock(m_glidsCachedMutex);
	return m_glidsCached.size();
}

bool ContentService::ContentMetadataIsCached(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_glidsCachedMutex);
	return (m_glidsCached.find(glid) != m_glidsCached.end());
}

bool ContentService::ContentMetadataCacheSet(ContentMetadata& md, bool expiryReset, bool overwrite) {
	std::lock_guard<fast_recursive_mutex> lock(m_glidsCachedMutex);

	// Content Metadata Is Already Cached ?
	if (ContentMetadataIsCached(md.getGlobalId())) {
		if (!overwrite) {
			return false;
		}

		LogWarn("ALREADY CACHED glid<" << md.getGlobalId() << "> - Overwriting...");
	}

	// Reset Expiry & Add Content Metadata To Cache
	if (expiryReset)
		md.ExpRunsReset();
	m_glidsCached[md.getGlobalId()] = md;
	return true;
}

ContentMetadata* ContentService::ContentMetadataCacheGetPtr(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_glidsCachedMutex);

	// Find It In Cache
	auto itr = m_glidsCached.find(glid);
	if (itr == m_glidsCached.end())
		return nullptr;

	// Found - Reset Expiry & Copy Metadata From Cache
	ContentMetadata* pMD = &(itr->second);
	pMD->ExpRunsReset();

	return pMD;
}

bool ContentService::ContentMetadataCacheGet(ContentMetadata& md, const GLID& glid, bool cacheIfNotFound) {
	std::lock_guard<fast_recursive_mutex> lock(m_glidsCachedMutex);

	// If Not In Cache Then Start Process Of Caching It
	if (cacheIfNotFound) {
		if (!ContentMetadataIsCached(glid))
			ContentMetadataCacheSync(glid);
	}

	// Find It In Cache
	ContentMetadata* pMD = ContentMetadataCacheGetPtr(glid);
	if (!pMD)
		return false;
	md = *pMD;
	return true;
}

bool ContentService::LocalCacheFileLoad() {
	// Get LocalCache File Path
	string pathLocalCache = LocalCacheFilePath();
	LogInfo("Loading '" << LogPath(pathLocalCache) << "' ...");

	// Load LocalCache.xml Content Metadata Xml
	TiXmlDocument doc;
	if (!doc.LoadFile(pathLocalCache) || !doc.RootElement()) {
		//		LogWarn("LoadFile() FAILED"); // happens on first launch gets created

		// Proceed to call wokweb for item preload list
		UpdateItemPreloadList(0);
		return false;
	}

	// Check previously cached item preload list
	const char* preloadListVersion = doc.RootElement()->Attribute(ATTR_PRELOAD_LIST_VERSION);
	int localPreloadListVersion = 0;
	if (preloadListVersion != nullptr) {
		localPreloadListVersion = atoi(preloadListVersion);
	}
	m_preloadListVersion = localPreloadListVersion;
	UpdateItemPreloadList(localPreloadListVersion);

	// Cache Version Match
	const char* cacheVersion = doc.RootElement()->Attribute(ATTR_CACHE_VERSION);
	jsAssert(cacheVersion != nullptr);
	if (cacheVersion == nullptr || !StrSame(cacheVersion, CACHE_VERSION)) {
		// Cache version upgraded - ignore old cache file
		_LogInfo("Localcache.xml ignored because of cacheVersion MISMATCH - " << (cacheVersion ? cacheVersion : "(null)") << "!=" << CACHE_VERSION);
		return false;
	}

	// Unpack Content Metadata Xml Into Cache (don't reset expiry, content may never get used, don't overwrite item preload list)
	size_t numCached = ContentMetadataXmlUnpackToCache(doc.RootElement(), false, false);
	return (numCached > 0);
}

bool ContentService::LocalCacheFileSave() {
	// Get LocalCache File Path
	string pathLocalCache = LocalCacheFilePath();
	LogInfo("Saving '" << LogPath(pathLocalCache) << "' ...");

	// Build Xml Header & Version
	TiXmlDocument doc;
	TiXmlDeclaration* decl = new TiXmlDeclaration("1.0", "", "");
	doc.LinkEndChild(decl);
	TiXmlElement* elemMetadata = new TiXmlElement(TAG_METADATA);
	doc.LinkEndChild(elemMetadata);
	elemMetadata->SetAttribute(ATTR_CACHE_VERSION, CACHE_VERSION);
	elemMetadata->SetAttribute(ATTR_PRELOAD_LIST_VERSION, std::to_string(m_preloadListVersion));

	// Pack Cached Content Metadata Into Xml
	std::lock_guard<fast_recursive_mutex> lock(m_glidsCachedMutex);
	size_t cacheSizeSaved = 0;
	for (const auto& itr : m_glidsCached) {
		ContentMetadata md = itr.second;

		// Expired Metadata Does Not Get Packed
		TiXmlElement* elemRecord = new TiXmlElement(TAG_RECORD);
		if (md.saveToXML(elemRecord)) {
			elemMetadata->LinkEndChild(elemRecord);
			++cacheSizeSaved;
			if (cacheSizeSaved >= CACHE_SIZE_SAVED_MAX) {
				LogWarn("cacheSize=" << ContentMetadataCacheSize() << " > cacheSizeSavedMax=" << CACHE_SIZE_SAVED_MAX << " - Truncating...");
				break;
			}
		} else {
			delete elemRecord;
		}
	}

	// Save LocalCache Xml
	if (!doc.SaveFile(pathLocalCache.c_str())) {
		LogError("SaveFile() FAILED");
		return false;
	}

	// Log Saved Cache Size
	LogInfo("OK - cacheSize=" << cacheSizeSaved);

	return true;
}

string ContentService::ContentMetadataUrl() {
	// Only Get This Once
	static string url;
	if (url.empty()) {
		KEPConfig cfg;
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return "";
		string netCfgPath = PathAdd(pICE->PathBase(), "network.cfg");
		cfg.Load(netCfgPath, "NetworkConfig");
		url = cfg.Get(CONTENTMETADATAURL_TAG, DEFAULT_CONTENTMETADATA_URL);
	}
	return url;
}

// DRF - Builds ContentMetadata Request Url From A Set Of Glids
string ContentService::MakeRequestURL(const set<GLID>& glids) {
	// Valid Glids ?
	if (glids.empty())
		return "";

	// Accumulate Glids Into Url Parameters
	string params;
	size_t numGlids = 0;
	auto itGlid = glids.begin();
	StrAppend(params, (ULONG)*itGlid);
	++numGlids;
	++itGlid;
	for (; itGlid != glids.end(); ++itGlid) {
		StrAppend(params, "," << (ULONG)*itGlid);
		++numGlids;
	}

	// Get wokweb URL & Replace {0} With Glid Parameters
	string url = ContentMetadataUrl();
	string::size_type index = url.find("{0}");
	if (index == string::npos)
		return "";
	url.replace(index, 3, params);

	LogInfo("glids=" << numGlids << " '" << url << "'");

	return url;
}

string ContentService::MakePreloadRequestURL(int localPreloadListVersion) {
	// Get wokweb URL & Replace {0} With empty string
	string url = ContentMetadataUrl();
	string::size_type index = url.find("{0}");
	if (index == string::npos)
		return "";
	url.replace(index, 3, "");

	// Append preload parameters
	url.append("&preload=1&curver=");
	url.append(std::to_string(localPreloadListVersion));
	return url;
}

string ContentMetadata::LocalTexturePathFromUrl(const std::string& texUrl) {
	return ::CompressTypeExtDel(StrStripFilePath(texUrl));
}

bool ContentMetadata::loadFromXML(const TiXmlElement* root) {
	if (!root)
		return false;

	// GLID (required)
	setGlobalId((GLID)XmlGetFirstChildTextAsInt(root, TAG_GLOBAL_ID, GLID_INVALID));
	if (getGlobalId() == GLID_INVALID) {
		LogError("XmlGetFirstChildTextAsInt(" << TAG_GLOBAL_ID << ") FAILED");
		Reset();
		return false;
	}

	// Base GLID
	setBaseGlobalId((GLID)XmlGetFirstChildTextAsInt(root, TAG_BASE_GLOBAL_ID, GLID_INVALID));

	// Texture Urls
	setV4Texture((XmlGetFirstChildText(root, TAG_TEM_GZ_URL) != NULL));
	string texUrlTag = isV4Texture() ? TAG_TEM_GZ_URL : TAG_TEXTURE_URL;
	setPrimaryTextureUrl(XmlGetFirstChildTextAsStr(root, texUrlTag));

	setJPEGTextureUrl(XmlGetFirstChildTextAsStr(root, TAG_TEM_JPG_URL));

	// Local Texture Paths
	setPrimaryTexturePath(LocalTexturePathFromUrl(getPrimaryTextureUrl()));
	setJPEGTexturePath(LocalTexturePathFromUrl(getJPEGTextureUrl()));

	// Individual texture info
	clearTextureInfo();
	auto elemTextures = XmlGetFirstChildElem(root, TAG_TEXTURES); // New <textures> tag to replace <texture_colors>, will carry more information
	if (!elemTextures) {
		// Fall-back to <texture_colors> for backward compatibility (remove after June 2016)
		elemTextures = XmlGetFirstChildElem(root, TAG_TEX_COLORS);
	}
	if (elemTextures) {
		auto szFileStore = XmlGetAttr(elemTextures, TAG_TEXTURES_ATTR_FS);
		if (szFileStore && szFileStore[0]) {
			m_uniqueTextureFileStore = szFileStore;
		}

		auto elem = XmlGetFirstChildElem(elemTextures, TAG_T);
		while (elem) {
			auto szOrdinal = XmlGetAttr(elem, TAG_T_ATTR_O);
			if (szOrdinal && szOrdinal[0]) {
				auto ordinal = _atoi64(szOrdinal); // Range: -1 ~ UINT_MAX
				if (ordinal >= 0 && ordinal <= std::numeric_limits<unsigned>::max()) {
					auto szColor = XmlGetAttr(elem, TAG_T_ATTR_C);
					if (szColor && szColor[0]) {
						auto color = strtoul(szColor, NULL, 10); // Range: 0 ~ UINT_MAX
						addTextureColor((unsigned)ordinal, color);
					}
					auto szUniqueId = XmlGetAttr(elem, TAG_T_ATTR_U);
					if (szUniqueId && szUniqueId[0]) {
						auto uniqueId = strtoul(szUniqueId, NULL, 10); // Range: 1 ~ UINT_MAX
						if (uniqueId > 0) {
							addTextureUniqueId((unsigned)ordinal, uniqueId);
						}
					}
				}
			}
			elem = elem->NextSiblingElement();
		}
	}

	// Use Type
	setUseType((USE_TYPE)XmlGetFirstChildTextAsInt(root, TAG_USE_TYPE, USE_TYPE_NONE));

	// Use Value
	setUseValue(XmlGetFirstChildTextAsInt(root, TAG_USE_VALUE));

	// Pass Group
	setPassGroups(XmlGetFirstChildTextAsInt(root, TAG_PASS_GROUP, PASS_GROUP_NONE));

	// Exclusion Groups
	setExclusionGroups(XmlGetFirstChildTextAsStr(root, TAG_EXCLUSION_GROUPS));

	// Primary asset Url
	auto assetUrlPriElem = XmlGetFirstChildElem(root, TAG_PRIMARY_URL);
	if (assetUrlPriElem != nullptr && assetUrlPriElem->GetText() != nullptr) {
		setPrimaryAsset(
			assetUrlPriElem->GetText(),
			(FileSize)XmlGetAttrAsInt(assetUrlPriElem, "size"),
			XmlGetAttrAsStr(assetUrlPriElem, "hash"),
			XmlGetAttrAsInt(assetUrlPriElem, "u"));
	}

	// Item Name
	string nameDefault = "UGC #" + numToString(getGlobalId(), "%d");
	setItemName(XmlGetFirstChildTextAsStr(root, TAG_NAME, nameDefault));

	// Item Description
	setItemDesc(XmlGetFirstChildTextAsStr(root, TAG_DESCRIPTION));

	// Thumbnail Path
	auto thumbPath_XL = XmlGetFirstChildTextAsStr(root, TAG_THUMB_XL);
	setThumbnailPath_XL(thumbPath_XL);

	// ED-8603 - Menu icon texture should use large size thumbnail not xlarge
	auto thumbPath_L = thumbPath_XL;
	thumbPath_L = StrReplace(thumbPath_L, "_020.", "."); // strip _020 (animation frame number, use 1st frame instead)
	thumbPath_L = StrReplace(thumbPath_L, "_021.", "."); // strip _021 (animation frame number, use 1st frame instead)
	thumbPath_L = StrReplace(thumbPath_L, "_022.", "."); // strip _022 (animation frame number, use 1st frame instead)
	thumbPath_L = StrReplace(thumbPath_L, "_ad.", "_la."); // 'large' version is big enough
	setThumbnailPath_L(thumbPath_L);

	// Expiry Runs (add 1)
	ExpRunsSet(XmlGetFirstChildTextAsInt(root, TAG_EXP_RUNS) + 1);

	// Expiry Time (utc mins)
	int expTimeDefault = ContentMetadata::TimeNowMin();
	ExpTimeSet(XmlGetFirstChildTextAsInt(root, TAG_EXP_TIME, expTimeDefault));

	// Cache Hits
	HitsSet(XmlGetFirstChildTextAsInt(root, TAG_HITS));

	// Local Resource File Hash
	LocalHashSet(XmlGetFirstChildTextAsStr(root, TAG_LOCAL_HASH));

	// Cache Duration (mins) Attribute
	int cacheDurationDefault = ContentMetadata::CACHE_DURATION;
	CacheDurationSet(XmlGetAttrAsInt(root, ATTR_CACHE_DURATION, cacheDurationDefault));

	// Purge Time Expired Items
	if (ExpiredTime()) {
		LogWarn("md.ExpiredTime cacheDuration=" << CacheDurationGet() << " - glid<" << getGlobalId() << ":" << getBaseGlobalId() << "> '" << getItemName() << "'");
		Reset();
		return false;
	}

	return true;
}

bool ContentMetadata::saveToXML(TiXmlElement* root) const {
	// DRF - Added - Don't Save Expired Runs MetaData
	if (ExpiredRuns()) {
		LogWarn("md.ExpiredRuns - glid<" << getGlobalId() << ":" << getBaseGlobalId() << "> '" << getItemName() << "'");
		return false;
	}

	TiXmlText* text;
	string tmp;

	// GLID
	TiXmlElement* globalIdElem = new TiXmlElement(TAG_GLOBAL_ID);
	tmp = numToString(getGlobalId(), "%d");
	text = new TiXmlText(tmp);
	globalIdElem->LinkEndChild(text);
	root->LinkEndChild(globalIdElem);

	// base GLID
	TiXmlElement* baseGlobalIdElem = new TiXmlElement(TAG_BASE_GLOBAL_ID);
	tmp = numToString(getBaseGlobalId(), "%d");
	text = new TiXmlText(tmp);
	baseGlobalIdElem->LinkEndChild(text);
	root->LinkEndChild(baseGlobalIdElem);

	// texture URL
	TiXmlElement* textureUrlElem = new TiXmlElement(isV4Texture() ? TAG_TEM_GZ_URL : TAG_TEXTURE_URL);
	textureUrlElem->LinkEndChild(new TiXmlText(getPrimaryTextureUrl()));
	root->LinkEndChild(textureUrlElem);

	textureUrlElem = new TiXmlElement(TAG_TEM_JPG_URL);
	textureUrlElem->LinkEndChild(new TiXmlText(getJPEGTextureUrl()));
	root->LinkEndChild(textureUrlElem);

	// Texture info
	std::set<ItemTextureOrdinal> textureKeys;
	for (const auto& pr : m_textureColors) {
		textureKeys.insert(pr.first);
	}
	for (const auto& pr : m_textureUniqueIds) {
		textureKeys.insert(pr.first);
	}

	if (!textureKeys.empty()) {
		TiXmlElement* texturesElem = new TiXmlElement(TAG_TEXTURES);
		if (!m_uniqueTextureFileStore.empty()) {
			texturesElem->SetAttribute(TAG_TEXTURES_ATTR_FS, m_uniqueTextureFileStore);
		}

		for (ItemTextureOrdinal key : textureKeys) {
			TiXmlElement* elem = new TiXmlElement(TAG_T);
			elem->SetAttribute(TAG_T_ATTR_O, std::to_string(key));

			// Texture average color
			auto itrColors = m_textureColors.find(key);
			if (itrColors != m_textureColors.end()) {
				elem->SetAttribute(TAG_T_ATTR_C, std::to_string(itrColors->second));
			}

			// Texture unique ID
			auto itrUniqueIds = m_textureUniqueIds.find(key);
			if (itrUniqueIds != m_textureUniqueIds.end()) {
				elem->SetAttribute(TAG_T_ATTR_U, std::to_string(itrUniqueIds->second));
			}

			texturesElem->LinkEndChild(elem);
		}
		root->LinkEndChild(texturesElem);
	}

	// use type
	TiXmlElement* useTypeElem = new TiXmlElement(TAG_USE_TYPE);
	tmp = numToString((int)getUseType(), "%d");
	text = new TiXmlText(tmp);
	useTypeElem->LinkEndChild(text);
	root->LinkEndChild(useTypeElem);

	// use value
	TiXmlElement* useValueElem = new TiXmlElement(TAG_USE_VALUE);
	tmp = numToString(getUseValue(), "%d");
	text = new TiXmlText(tmp);
	useValueElem->LinkEndChild(text);
	root->LinkEndChild(useValueElem);

	// pass group
	TiXmlElement* passGroupElem = new TiXmlElement(TAG_PASS_GROUP);
	tmp = numToString(getPassGroups(), "%d");
	text = new TiXmlText(tmp);
	passGroupElem->LinkEndChild(text);
	root->LinkEndChild(passGroupElem);

	// exclusion groups
	TiXmlElement* exclusionGroupsElem = new TiXmlElement(TAG_EXCLUSION_GROUPS);
	text = new TiXmlText(getExclusionGroups());
	exclusionGroupsElem->LinkEndChild(text);
	root->LinkEndChild(exclusionGroupsElem);

	// Primary asset URL (save if not empty)
	{
		string assetUrl, fileHash;
		FileSize fileSize = 0;
		unsigned uniqueId = 0;
		getPrimaryAsset(assetUrl, fileSize, fileHash, uniqueId);
		if (!assetUrl.empty()) {
			TiXmlElement* assetUrlElem = new TiXmlElement(TAG_PRIMARY_URL);
			text = new TiXmlText(assetUrl);
			assetUrlElem->LinkEndChild(text);
			assetUrlElem->SetAttribute("size", (unsigned int)fileSize);
			assetUrlElem->SetAttribute("hash", fileHash.c_str());
			if (uniqueId != 0) {
				assetUrlElem->SetAttribute("u", uniqueId);
			}
			root->LinkEndChild(assetUrlElem);
		}
	}

	// name
	TiXmlElement* nameElem = new TiXmlElement(TAG_NAME);
	text = new TiXmlText(getItemName());
	nameElem->LinkEndChild(text);
	root->LinkEndChild(nameElem);

	// description
	TiXmlElement* descElem = new TiXmlElement(TAG_DESCRIPTION);
	text = new TiXmlText(getItemDesc());
	descElem->LinkEndChild(text);
	root->LinkEndChild(descElem);

	// thumbnailXLarge
	TiXmlElement* thumbXlElem = new TiXmlElement(TAG_THUMB_XL);
	text = new TiXmlText(getThumbnailPath_XL());
	thumbXlElem->LinkEndChild(text);
	root->LinkEndChild(thumbXlElem);

	// expRuns
	TiXmlElement* expRunsElem = new TiXmlElement(TAG_EXP_RUNS);
	tmp = numToString(ExpRunsGet(), "%d");
	text = new TiXmlText(tmp);
	expRunsElem->LinkEndChild(text);
	root->LinkEndChild(expRunsElem);

	// expTime
	TiXmlElement* expTimeElem = new TiXmlElement(TAG_EXP_TIME);
	tmp = numToString(ExpTimeGet(), "%d");
	text = new TiXmlText(tmp);
	expTimeElem->LinkEndChild(text);
	root->LinkEndChild(expTimeElem);

	// hits
	TiXmlElement* hitsElem = new TiXmlElement(TAG_HITS);
	tmp = numToString(HitsGet(), "%d");
	text = new TiXmlText(tmp);
	hitsElem->LinkEndChild(text);
	root->LinkEndChild(hitsElem);

	// Local Resource File Hash
	TiXmlElement* localHashElem = new TiXmlElement(TAG_LOCAL_HASH);
	text = new TiXmlText(LocalHashGet());
	localHashElem->LinkEndChild(text);
	root->LinkEndChild(localHashElem);

	// cacheDuration
	root->SetAttribute(ATTR_CACHE_DURATION, numToString(CacheDurationGet(), "%d"));

	return true;
}

void ContentService::OnUpdate(bool bFlushRequests) {
	auto pICE = IClientEngine::Instance();
	if (!pICE || !pICE->GetDispatcher())
		return;

	if (m_glidsQueued.empty())
		return;

	// Periodically Flush Updates
	bool updateFlush = (m_updateTimer.ElapsedMs() > s_updateFlushMs);
	if (!bFlushRequests && !updateFlush)
		return;
	m_updateTimer.Reset();

	std::lock_guard<fast_recursive_mutex> lock(m_pendingRequestsSync);

	// only request items which aren't cached
	set<GLID> notCached;
	auto itr = m_glidsQueued.begin();
	while (itr != m_glidsQueued.end()) {
		GLID glidQueued = *itr;
		++itr;
		if (!ContentMetadataIsCached(glidQueued)) {
			notCached.insert(glidQueued);
			m_glidsInProcess.insert(glidQueued);
		} else {
			CallbackSystem::Instance()->CallbackReady(this, glidQueued);
		}

		// ED-3923 - DRF - Enforce Max Glids Per Webcall
		if ((itr == m_glidsQueued.end() && !notCached.empty()) || notCached.size() >= GLIDS_PER_WEBCALL_MAX) {
			// Create Glid Metadata Results Event For Async Processing
			IEvent* e = pICE->GetDispatcher()->MakeEvent(pICE->GetItemsMetadataResultEventId());
			if (e) {
				(*e->OutBuffer()) << (int)notCached.size();
				for (const auto& glid : notCached)
					(*e->OutBuffer()) << (int)glid;

				// Async Glid Metadata Webcall
				// Have to hit the web for some items.  Once the web request completes
				// then we'll fire the event, and all the items in the request will
				// arm inside the handler.
				string requestURL = MakeRequestURL(notCached);
				if (!requestURL.empty())
					pICE->GetBrowserPageAsync(requestURL.c_str(), e);
			} else {
				LogError("MakeEvent(GetItemsMetadataResultEvent) FAILED");
			}
			notCached.clear();
		}
	}
	m_glidsQueued.clear();
}

EVENT_PROC_RC ContentService::ItemsMetadataResultHandler(ULONG lparam, IDispatcher* dispatcher, IEvent* e) {
	return ContentService::Instance()->ItemsMetadataResultHandlerImpl(lparam, dispatcher, e);
}

EVENT_PROC_RC ContentService::ItemsMetadataResultHandlerImpl(ULONG lparam, IDispatcher* dispatcher, IEvent* e) {
	// Unpack All Processed Glids
	set<GLID> glids;
	int glidCount;
	(*e->InBuffer()) >> glidCount;
	for (int i = 0; i < glidCount; i++) {
		int glid;
		(*e->InBuffer()) >> glid;
		glids.insert((GLID)glid);
	}

	// Unpack All Processed Glid Metadata
	string resultText;
	int code = 0;
	(*e->InBuffer()) >> resultText >> code;
	if (resultText.size() > 0)
		ContentMetadataXmlUnpackToCache(resultText);

	// Remove All Glids From InProcess List
	{
		std::lock_guard<fast_recursive_mutex> lock(m_pendingRequestsSync);
		for (const auto& glid : glids) {
			auto itGlidInProc = m_glidsInProcess.find(glid);
			if (itGlidInProc != m_glidsInProcess.end())
				m_glidsInProcess.erase(itGlidInProc);
		}
	}

	// Inform Callback System Of All Glids Ready
	for (const auto& glid : glids)
		CallbackSystem::Instance()->CallbackReady(this, glid);

	return EVENT_PROC_RC::OK;
}

void ContentService::DispatchEvent(IEvent* pEvent) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	IDispatcher* pDispatcher = pICE->GetDispatcher();
	if (!pDispatcher)
		return;

	// The event handler expects to find result text from the web so we'll tack
	// a blank string on here for consistency.
	(*pEvent->OutBuffer()) << "";
	pDispatcher->QueueEvent(pEvent);
}

string ContentService::LocalCacheFilePath() {
	return PathAdd(PathApp(), "MapsModels\\LocalCache.xml");
}

bool ContentService::UpdateItemPreloadList(int localPreloadListVersion) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	// Make a blocking call to contentMetadata.aspx?preload=1
	std::string url = MakePreloadRequestURL(localPreloadListVersion);
	LogInfo(url);

	assert(!url.empty());
	if (url.empty()) {
		LogError("Error obtaining wokweb URL");
		return false;
	}

	DWORD status = 0;
	string resultText, statusDesc;

	// Blocking call (the result might be a few hundred kilobytes)
	if (!pICE->_GetBrowserPage(url.c_str(), 0, 1, resultText, &status, statusDesc)) {
		LogError("Web call failed");
		return false;
	}

	// Parse Webcall Response Content Metadata Xml
	TiXmlDocument doc;
	doc.Parse(resultText.c_str());

	assert(doc.RootElement() != nullptr);
	if (!doc.RootElement()) {
		LogError("Result is not valid XML: " << resultText.substr(0, 20) << " ...");
		return false;
	}

	// Parse list version number
	const char* preloadListVersion = doc.RootElement()->Attribute(ATTR_PRELOAD_LIST_VERSION);
	if (preloadListVersion == nullptr) {
		// Already up-to-date?
		auto elem = XmlGetFirstChildElem(doc.RootElement(), TAG_RETURN_CODE);
		if (elem && elem->GetText() && *elem->GetText()) {
			int returnCode = atoi(elem->GetText());
			elem = XmlGetFirstChildElem(doc.RootElement(), TAG_RESULT_DESC);
			if (elem && elem->GetText()) {
				// Recognized result format
				LogInfo("Return code: " << returnCode << ", message: " << elem->GetText());
				return false;
			}
		}
		LogInfo("Unknown XML result: " << resultText.substr(0, 20) << " ...");
		return false;
	}

	int newPreloadListVersion = atoi(preloadListVersion);
	LogInfo("REMOTE preloadListVersion = " << newPreloadListVersion);

	// Unpack Content Metadata Xml Into Cache (keep expiry, no overwrite)
	ContentMetadataXmlUnpackToCache(doc.RootElement(), false, false);

	// Update local list version number
	m_preloadListVersion = newPreloadListVersion;
	return true;
}

} // namespace KEP