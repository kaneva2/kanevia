///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "ClientStrings.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "afxext.h"
#include "CStaticMeshObject.h"
#include "CWldObject.h"
#include "IClientEngine.h"
#include "CReferenceLibrary.h"
#include <KEPException.h>
#include <ServerSerialize.h>
#include "..\..\..\RenderEngine\ReD3DX9.h"
#include "..\..\..\RenderEngine\ReMesh.h"
#include "MaterialPersistence.h"
#include "PrimitiveGeomArgs.h"
#ifdef _ClientOutput
#include "ReD3DX9.h"
#include "NvTriStrip.h"
#include "IClientEngine.h"
#include "ReMeshCache.h"
#include "ReMeshStream.h"
#include "ReMeshCreate.h"
#include "ReMesh.h"
#endif
#include "CSSoundRange.h"
#include "CRTLightClass.h"
#include "CNodeObject.h"
#include "CContainmentClass.h"
#include "CTriggerClass.h"
#include "CAttributeClass.h"

#pragma comment(lib, "Rpcrt4.lib")

namespace KEP {

CWldObject::CWldObject(int GlobalTraceNumber) :
		m_pivot(0.0f, 0.0f, 0.0f), m_isMeshAssigned(FALSE) {
	m_groupNumber = 0;
	m_collisionType = 0;
	m_identity = GlobalTraceNumber;
	m_popoutRadius = 0.0f; //popout radius//calculate later
#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	m_poppedOut = FALSE; //popped out
#endif
	m_lightObject = NULL; //light interface
	m_attribute = NULL;
	m_baseSoundBuffer = NULL;
	m_buffer3DControl = NULL;
	m_soundFileName = "NONE";
	m_useSound = FALSE;
	m_soundFactor = .03f; //L_soundFactor,
	m_isPlaying = FALSE; //L_isPlaying,
	m_validRange = 0.0f; //L_validRange
	m_environmentAttached = FALSE; //environment attached
	m_miscFloat = 0.0f;
	m_miscInt = 0;
	m_usedAsSpawnPoint = -1;
	m_removeEnvironment = FALSE;
	m_popoutBox.max.x = 0.0f;
	m_popoutBox.max.y = 0.0f;
	m_popoutBox.max.z = 0.0f;
	m_popoutBox.min.x = 0.0f;
	m_popoutBox.min.y = 0.0f;
	m_popoutBox.min.z = 0.0f;
	m_showBox = FALSE;
	m_containmentObject = NULL;
	m_lodVisualList = NULL;
	m_meshObject = NULL;
	m_renderInMirrorTag = FALSE;
	m_particleDirection.x = 0.0f;
	m_particleDirection.y = 0.0f;
	m_particleDirection.z = 1.0f;
	m_runtimeParticleId = 0;
	m_haloLink = 0;
	m_lensFlareType = 0;
	m_fileNameRef = "N/A";
	m_collisionInfoFilter = FALSE;
	m_renderFilter = FALSE;
	m_particleLink = -1;
	m_soundRange = NULL;
	m_filterGroup = -1;
	m_node = NULL;
	m_animModule = NULL;
	m_envReactionAttrib = 0;
	m_triggerList = NULL;
	m_customizableTexture = FALSE;
	m_assetId = 0;
	m_canHangPictures = FALSE;
	m_customMaterial = NULL;
	memcpy(&m_uniqueId, &GUID_NULL, sizeof(m_uniqueId));
	m_customTexture = "";
	m_customTexturePath = eTexturePath::CustomTexture;
	m_customTextureSubDir = "";
	m_min.x = 0.0f;
	m_min.y = 0.0f;
	m_min.z = 0.0f;
	m_min.w = 1.0f;
	m_max.x = 0.0f;
	m_max.y = 0.0f;
	m_max.z = 0.0f;
	m_max.w = 1.0f;
	m_cached_box = false;
	m_culled = TRUE;
	m_ReMesh = NULL;
	m_externalSource = NULL;
	m_material = NULL;
	for (UINT i = 0; i < m_maxLODs; i++) {
		m_ReLODMesh[i] = NULL;
	}

	m_material = NULL;

	m_outline = false;
	m_outlineColor = D3DXCOLOR(1, 1, 1, 1);
	m_outlineZSorted = false;
}

CWldObject::~CWldObject() {
	if (m_customMaterial) {
		delete m_customMaterial;
	}
	delete m_material;
}

void CWldObject::SafeDelete() {
	if (m_node) {
		m_node->SafeDelete();
		delete m_node;
		m_node = 0;
	}

	//delete temp lists
	if (m_soundRange) {
		m_soundRange->SafeDelete();
		delete m_soundRange;
		m_soundRange = 0;
	}

	if (m_triggerList) {
		delete m_triggerList;
		m_triggerList = 0;
	}

	if (m_meshObject) {
		m_meshObject->SafeDelete();
		delete m_meshObject;
		m_meshObject = 0;
	}

	if (m_baseSoundBuffer)
		m_baseSoundBuffer->Release();
	m_baseSoundBuffer = 0;
	if (m_buffer3DControl)
		m_buffer3DControl->Release();
	m_buffer3DControl = 0;

	if (m_containmentObject) {
		delete m_containmentObject;
		m_containmentObject = 0;
	}

	if (m_lodVisualList) {
		m_lodVisualList->SafeDelete();
		delete m_lodVisualList;
		m_lodVisualList = 0;
	}

	if (m_attribute)
		delete m_attribute;
	m_attribute = 0;
	if (m_lightObject) {
		delete m_lightObject;
		m_lightObject = 0;
	}
	//end content list
	if (m_animModule) {
		m_animModule->SafeDelete();
		delete m_animModule;
		m_animModule = 0;
	}
#ifdef _ClientOutput
	for (int lod = 0; lod < m_maxLODs; lod++) {
		delete m_ReLODMesh[lod];
		m_ReLODMesh[lod] = 0;
	}
	delete m_ReMesh;
	m_ReMesh = 0;
#endif
}

void CWldObject::InitReMeshes() {
#ifdef _ClientOutput
	// clone the ReMesh off the Reference object mesh
	if (m_node) {
		CReferenceObj* refObj = GetReferenceObj();
		if (refObj != NULL) {
			if (refObj->GetReMesh() != NULL) {
				// clone mesh if it's changed
				if (!GetReMesh() || !refObj->GetReMesh()->EqualTo(GetReMesh())) {
					CMaterialObject* matPtr = GetCustomMaterial() == NULL ? refObj->m_material : GetCustomMaterial();
					refObj->GetReMesh()->SetMaterial(matPtr->GetReMaterial());

					ReMesh* newMesh = 0;
					refObj->GetReMesh()->Clone(&newMesh, true);
					// Add the mesh to the mesh pool
					ReMesh* mesh;
					ReMesh* addedMesh = MeshRM::Instance()->AddMesh(newMesh, MeshRM::RMP_ID_LEVEL);
					newMesh->Clone(&mesh, true);
					ASSERT(mesh);
					if (mesh) {
						SetReMesh(mesh);
						// set transform state of ReMesh
						mesh->SetWorldMatrix(&m_node->GetWorldTransform());
						mesh->SetWorldViewProjMatrix(&m_WorldViewProjMatrix);
						mesh->SetWorldInvTransposeMatrix(&m_WorldInvTransposeMatrix);
						mesh->SetRadius(refObj->GetBoundingRadius());
						mesh->SetLightCache(&m_lightCache);
					}
					if (addedMesh) {
						//delete addedMesh;  // Should be able to free this, but it crashes
					}
				}
			}

			// clone LOD ReMeshes and set state from the refObj lod meshes
			if (refObj->m_lodVisualList) {
				UINT numLODs = 0;
				CMaterialObject* matPtr = GetCustomMaterial() == NULL ? refObj->m_material : GetCustomMaterial();
				for (POSITION posLoc = refObj->m_lodVisualList->GetHeadPosition(); posLoc != NULL;) {
					CMeshLODObject* lodMesh = (CMeshLODObject*)refObj->m_lodVisualList->GetNext(posLoc);
					if (lodMesh && lodMesh->GetReMesh()) {
						if (numLODs < m_maxLODs) {
							// clone LOD mesh IF it has changed
							if (!m_ReLODMesh[numLODs] || !lodMesh->GetReMesh()->EqualTo(m_ReLODMesh[numLODs])) {
								lodMesh->GetReMesh()->SetMaterial(matPtr->GetReMaterial());
								m_ReLODMesh[numLODs] = 0;
								ReMesh* mesh = NULL;
								ReMesh* addedMesh = NULL;
								lodMesh->GetReMesh()->Clone(&mesh, true);
								if (mesh) {
									// put one copy in global mesh cache
									addedMesh = MeshRM::Instance()->AddMesh(mesh, MeshRM::RMP_ID_LEVEL);
								}
								// store other copy in lodChain
								lodMesh->GetReMesh()->Clone(&m_ReLODMesh[numLODs], true);
								// Why do we need 2 copies here.  I don't understand this system, but it stops the crashing.
								if (m_ReLODMesh[numLODs]) {
									// set the render state
									m_ReLODMesh[numLODs]->SetWorldMatrix(&m_node->GetWorldTransform());
									m_ReLODMesh[numLODs]->SetWorldViewProjMatrix(&m_WorldViewProjMatrix);
									m_ReLODMesh[numLODs]->SetWorldInvTransposeMatrix(&m_WorldInvTransposeMatrix);
									m_ReLODMesh[numLODs]->SetRadius(refObj->GetBoundingRadius());
									m_ReLODMesh[numLODs]->SetLightCache(&m_lightCache);
								}
								if (addedMesh) {
									//delete addedMesh;  // Should be able to free, but crashes.
								}
							}
						}
					}
					numLODs++;
				}
			}
		}
		// always get this in distro else ASSERT( FALSE );	// if refObj!=NULL: ref object not found or NULL (deleted) in RLB
	} else if (m_meshObject) {
		// init the non-node ReMesh
		auto pReMesh = GetReMesh();
		if (pReMesh && !pReMesh->GetWorldMatrix()) {
			pReMesh->SetWorldMatrix(&m_meshObject->GetWorldTransform());
			pReMesh->SetLightCache(&m_lightCache);
			pReMesh->SetRadius(GetBoundingRadius());
			CMaterialObject* matPtr = GetCustomMaterial() == NULL ? m_meshObject->m_materialObject : GetCustomMaterial();
			pReMesh->SetMaterial(matPtr->GetReMaterial());
		}

		// clone the lod meshes
		if (m_lodVisualList) {
			UINT numLODs = 0;
			CMaterialObject* matPtr = GetCustomMaterial() == NULL ? m_meshObject->m_materialObject : GetCustomMaterial();
			for (POSITION posLoc = m_lodVisualList->GetHeadPosition(); posLoc != NULL;) {
				CMeshLODObject* lodMesh = (CMeshLODObject*)m_lodVisualList->GetNext(posLoc);
				if (lodMesh && lodMesh->GetReMesh()) {
					if (numLODs < m_maxLODs) {
						// clone LOD mesh IF it has changed
						if (!m_ReLODMesh[numLODs] || !lodMesh->GetReMesh()->EqualTo(m_ReLODMesh[numLODs])) {
							lodMesh->GetReMesh()->SetMaterial(matPtr->GetReMaterial());
							m_ReLODMesh[numLODs] = 0;
							ReMesh* mesh = NULL;
							ReMesh* addedMesh = NULL;
							lodMesh->GetReMesh()->Clone(&mesh, true);
							// Add the mesh to the mesh pool
							if (mesh) {
								// Add a copy to global mesh cache
								addedMesh = MeshRM::Instance()->AddMesh(mesh, MeshRM::RMP_ID_LEVEL);
							}
							// Runtime use copy
							lodMesh->GetReMesh()->Clone(&m_ReLODMesh[numLODs], true);
							if (m_ReLODMesh[numLODs]) {
								// set the render state
								m_ReLODMesh[numLODs]->SetWorldMatrix(&m_meshObject->GetWorldTransform());
								m_ReLODMesh[numLODs]->SetWorldViewProjMatrix(&m_WorldViewProjMatrix);
								m_ReLODMesh[numLODs]->SetWorldInvTransposeMatrix(&m_WorldInvTransposeMatrix);
								m_ReLODMesh[numLODs]->SetRadius(GetBoundingRadius());
								m_ReLODMesh[numLODs]->SetLightCache(&m_lightCache);
							}
							if (addedMesh) {
								//delete addedMesh; // should be able to free but crashes.
							}
						}
					}
				}
				numLODs++;
			}
		}
	}
#endif
}

BOOL CWldObject::UpdateLods() {
	if (!m_lodVisualList)
		return FALSE;

	for (POSITION posLoc = m_lodVisualList->GetHeadPosition(); posLoc != NULL;) {
		CMeshLODObject* meshLod = (CMeshLODObject*)m_lodVisualList->GetNext(posLoc);
		if (meshLod->m_material)
			delete meshLod->m_material;
		meshLod->m_material = 0;
		m_meshObject->m_materialObject->Clone(&meshLod->m_material);

		CopyMemory(meshLod->m_material->m_texBlendMethod, m_meshObject->m_materialObject->m_texBlendMethod, (sizeof(int) * 10));
		CopyMemory(meshLod->m_material->m_texNameHash, m_meshObject->m_materialObject->m_texNameHash, (sizeof(int) * 10));

		for (int i = 0; i < 8; i++)
			meshLod->m_material->m_texFileName[i] = m_meshObject->m_materialObject->m_texFileName[i];

		meshLod->m_material->Reset();
	}
	return TRUE;
}

void TransformPopoutBox(CWldObject* aWorldObject, const ABBOX& aPreXFormBBox, const ABBOX& aPostXFormBBox) {
	// update popout box
	Vector3f preCenter(0.5f * (aPreXFormBBox.minX + aPreXFormBBox.maxX), 0.5f * (aPreXFormBBox.minY + aPreXFormBBox.maxY), 0.5f * (aPreXFormBBox.minZ + aPreXFormBBox.maxZ));
	Vector3f preSides(0.5f * (aPreXFormBBox.maxX - aPreXFormBBox.minX), 0.5f * (aPreXFormBBox.maxY - aPreXFormBBox.minY), 0.5f * (aPreXFormBBox.maxZ - aPreXFormBBox.minZ));
	Vector3f postCenter(0.5f * (aPostXFormBBox.minX + aPostXFormBBox.maxX), 0.5f * (aPostXFormBBox.minY + aPostXFormBBox.maxY), 0.5f * (aPostXFormBBox.minZ + aPostXFormBBox.maxZ));
	Vector3f postSides(0.5f * (aPostXFormBBox.maxX - aPostXFormBBox.minX), 0.5f * (aPostXFormBBox.maxY - aPostXFormBBox.minY), 0.5f * (aPostXFormBBox.maxZ - aPostXFormBBox.minZ));

	Vector3f sideScale(postSides.x / preSides.x, postSides.y / preSides.y, postSides.z / preSides.z);

	Vector3f pobCenter(0.5f * ((Vector3f)aWorldObject->m_popoutBox.max + (Vector3f)aWorldObject->m_popoutBox.min));
	Vector3f pobSides(0.5f * ((Vector3f)aWorldObject->m_popoutBox.max - (Vector3f)aWorldObject->m_popoutBox.min));
	pobCenter += (postCenter - preCenter);
	pobSides.x *= sideScale.x;
	pobSides.y *= sideScale.y;
	pobSides.z *= sideScale.z;
	aWorldObject->m_popoutBox.min = pobCenter - pobSides;
	aWorldObject->m_popoutBox.max = pobCenter + pobSides;
}

const Matrix44f& CWldObject::GetWorldTransform() const {
	if (m_meshObject) {
		return m_meshObject->GetWorldTransform();
	} // if..m_meshObject
	else if (m_node) {
		return m_node->GetWorldTransform();
	} // if..else if..m_node

	return Matrix44f::GetIdentity();
}

void CWldObject::Transform(const Vector3f& aScale, const Vector3f& aRotate,
	const Vector3f& aTranslate, const Vector3f* aPivotOverride) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	if (!GetReMesh())
		return;

	if (m_node) {
		CReferenceObj* refObj = GetReferenceObj();
		if (!refObj)
			return;
	} else if (!m_meshObject) {
		return;
	}

	ABBOX preXFormBBox, postXFormBBox;

	// get the scale & rotation xform
	SetScaleTranslationRotation(aScale, aTranslate, aRotate);
	preXFormBBox = GetBoundingBox();
	postXFormBBox = CacheBoundingBox();

	// adjust the popout box
	TransformPopoutBox(this, preXFormBBox, postXFormBBox);

	// update the GPU shader matrices
	m_WorldViewProjMatrix = GetWorldTransform() * reinterpret_cast<const Matrix44f&>(*pICE->GetReDeviceState()->GetRenderState()->GetProjView());
	m_WorldInvTransposeMatrix.MakeInverseOf(GetWorldTransform());
#endif
}

bool CWldObject::GetBoundingBox(Vector4f& min, Vector4f& max) {
	if (!m_cached_box)
		CacheBoundingBox();

	min = m_min;
	max = m_max;
	return true;
}

ABBOX CWldObject::CacheBoundingBox() {
	ABBOX postXFormBBox = { 0, 0, 0, 0, 0, 0 };

#ifdef _ClientOutput

	// Call ReMesh interface to compute bounding box
	postXFormBBox = MeshRM::Instance()->ComputeBoundingBoxWithMatrix(GetReMesh(), &GetWorldTransform());

	m_cached_box = true;
#endif

	// adjust the persistent world object AABB
	m_min.x = postXFormBBox.minX;
	m_min.y = postXFormBBox.minY;
	m_min.z = postXFormBBox.minZ;
	m_max.x = postXFormBBox.maxX;
	m_max.y = postXFormBBox.maxY;
	m_max.z = postXFormBBox.maxZ;

	return postXFormBBox;
}

FLOAT CWldObject::GetBoundingRadius() {
	if (m_meshObject)
		return m_meshObject->GetBoundingRadius();

	if (m_node) {
		CReferenceObj* pRefObj = GetReferenceObj();
		if (pRefObj != NULL) {
			return pRefObj->GetBoundingRadius();
		}
	}

	return 0;
}

bool CWldObject::IntersectRay(
	const Vector3f& origin,
	const Vector3f& direction,
	float* distance,
	Vector3f* location,
	Vector3f* normal) {
	bool intersect = false;

	Matrix44f matWorld;
	ReMesh* reMesh = NULL;

	if (m_meshObject) {
		matWorld = m_meshObject->GetWorldTransform();
		reMesh = GetReMesh();
	} else if (m_meshObject == 0 && m_node != 0) {
		CReferenceObj* refObj = GetReferenceObj();

		if (refObj) {
			matWorld = m_node->GetWorldTransform();
			reMesh = refObj->GetReMesh();
		}
	}

	_ASSERTE(reMesh);

	if (reMesh != NULL) {
		/* invert world matrix */
		Matrix44f matWorldInv;
		float determinant = 0.0f;
		matWorldInv.MakeInverseOf(matWorld, &determinant);

		if (determinant > 1e-5) {
			/* transform ray into object space */
			Vector3f osOrigin, osDirection;

			/* ray origin */
			osOrigin = TransformPoint_NonPerspective(matWorldInv, origin);

			/* ray direction */
			osDirection = TransformVector(matWorldInv, direction);

			/* intersect against ReMesh */
			intersect = reMesh->IntersectRay(osOrigin, osDirection, NULL, location, normal);

			if (intersect) {
				/* transform point of intersection back into world space */
				Vector3f loc = TransformPoint_NonPerspective(matWorld, *location);

				if (location) {
					*location = loc;
				}

				if (normal) {
					*normal = TransformVector(matWorld, *normal);
				}

				if (distance) {
					Vector3f d = loc - origin;
					*distance = d.Length();
				}
			}
		}
	}

	return intersect;
}

float CWldObject::GetDistToCam(Vector3f* camPosition) {
	Vector3f result;
	const Vector3f* worldObjPos = NULL;

	//	Node ref, or mesh ref?
	if (m_node)
		worldObjPos = (&m_node->GetTranslation());
	else if (m_meshObject)
		worldObjPos = (&m_meshObject->GetTranslation());
	else {
		//	No position data
		return 0.0f;
	}

	result = *worldObjPos - *camPosition;
	return result.Length();
}

void CWldObject::CalibrateCenteredBoundingBox(int sightRange) {
	m_popoutBox.max.x = ((sightRange + 1) + m_meshObject->GetMeshCenter().x);
	m_popoutBox.max.y = ((sightRange + 2) + m_meshObject->GetMeshCenter().y);
	m_popoutBox.max.z = ((sightRange + 3) + m_meshObject->GetMeshCenter().z);
	m_popoutBox.min.x = -(sightRange + 4) + m_meshObject->GetMeshCenter().x;
	m_popoutBox.min.y = -(sightRange + 5) + m_meshObject->GetMeshCenter().y;
	m_popoutBox.min.z = -(sightRange + 6) + m_meshObject->GetMeshCenter().z;
}

const Vector3f& CWldObject::GetScale() const {
	static const Vector3f defaultScale(1.0f, 1.0f, 1.0f);
	if (m_meshObject)
		return m_meshObject->GetScale();
	else if (m_node)
		return m_node->GetScale();

	return defaultScale;
}

const Vector3f& CWldObject::GetRotation() const {
	static const Vector3f defaultRotation(0.0f, 0.0f, 0.0f);
	if (m_meshObject)
		return m_meshObject->GetRotation();
	else if (m_node)
		return m_node->GetRotation();

	return defaultRotation;
}

const Vector3f& CWldObject::GetTranslation() const {
	static const Vector3f defaultTranslation(0.0f, 0.0f, 0.0f);
	if (m_meshObject)
		return m_meshObject->GetTranslation();
	else if (m_node)
		return m_node->GetTranslation();

	return defaultTranslation;
}

void CWldObject::Serialize(CArchive& ar) {
	Vector3f vectorReserved;
	vectorReserved.x = 0.0f;
	vectorReserved.y = 0.0f;
	vectorReserved.z = 0.0f;

	float reservedFloat = 0.0f;
	int typeSaveVersion = 1;
	CStringA reservedCString;

	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		if (memcmp(&m_uniqueId, &GUID_NULL, sizeof(GUID)) == 0) {
			VERIFY(UuidCreate(&m_uniqueId) == S_OK);
		}

		ar.SetObjectSchema(CArchiveObjectSchemaVersion<CWldObject>);

		if (m_animModule)
			typeSaveVersion = 2;

		// stream the legacy vertex arrays if NOT using ReMesh
		if (m_meshObject) {
			m_meshObject->m_VertexArrayValid = (m_ReMesh == NULL);
		}

		ar << m_identity;
		ar << m_popoutRadius;
		ar << static_cast<int>(ObjectType::NONE); //m_surfaceProperty
		if (ServerSerialize)
			ar << (CRTLightObj*)0; // server doesn't need this
		else
			ar << m_lightObject;

		ar << m_collisionType;
		ar << m_filterGroup;
		ar << m_envReactionAttrib;
		ar << typeSaveVersion;
		ar << reservedCString;
		ar << m_popoutBox.min.x;
		ar << m_popoutBox.min.y;
		ar << m_popoutBox.min.z;
		ar << m_popoutBox.max.x;
		ar << m_popoutBox.max.y;
		ar << m_popoutBox.max.z;
		ar << m_groupNumber;
		ar << reservedFloat;
		ar << m_attribute;
		ar << m_soundFileName;
		ar << m_useSound;
		ar << m_soundFactor;
		ar << m_isPlaying;
		ar << m_validRange;
		ar << m_environmentAttached;
		ar << m_usedAsSpawnPoint;
		ar << m_removeEnvironment;
		ar << m_triggerList;
		ar << m_containmentObject;
		ar << m_lodVisualList;
		ar << m_renderInMirrorTag;
		ar << m_particleDirection.x;
		ar << m_particleDirection.y;
		ar << m_particleDirection.z;
		ar << m_haloLink;
		ar << m_lensFlareType;
		ar << m_particleLink;
		ar << m_fileNameRef;
		ar << m_meshObject;
		ar << m_collisionInfoFilter;
		ar << m_renderFilter;
		ar << m_soundRange;
		ar << m_node;

		if (typeSaveVersion == 2) {
			ar << m_animModule;
		}

		ar << m_description; //version 2
		ar << m_customizableTexture;
		ar << m_name; //version 3
		ar.Write(&m_uniqueId, sizeof(m_uniqueId)); // version 4
		ar << m_canHangPictures;
		ar.Write(&m_min, sizeof(D3DXVECTOR4)); // version 6 12_27_06 CPITZER....persistent AABB support
		ar.Write(&m_max, sizeof(D3DXVECTOR4));
		ar.Write(&m_cached_box, sizeof(bool));

#ifdef _ClientOutput
		// write the ReMesh
		if (!::ServerSerialize && m_ReMesh && m_node == NULL) {
			// get a ReMeshStream object
			ReMeshStream streamer;
			// stream out the ReMesh object
			streamer.Write(m_ReMesh, ar);
		}
#endif

		// V10 addition
		// Decomposed matrix data -- ready for both zone object and RLB object
		// Note: will cause redundant data for RLB (CNodeObject::Serialize). Will clean up CNodeObject once this is stabilized.
		ar << GetScale().x << GetScale().y << GetScale().z;
		ar << GetRotation().x << GetRotation().y << GetRotation().z;
		ar << GetTranslation().x << GetTranslation().y << GetTranslation().z;

		ar << m_pivot.x << m_pivot.y << m_pivot.z;

		ASSERT(IsReference() || m_isMeshAssigned);
		ar << m_isMeshAssigned;
	} else {
		m_customizableTexture = FALSE;
		m_canHangPictures = FALSE;
		int version = ar.GetObjectSchema();
		int surfaceProperty_Deprecated;

		ar >> m_identity;
		ar >> m_popoutRadius;
		ar >> surfaceProperty_Deprecated;
		ar >> m_lightObject;
		ar >> m_collisionType;
		ar >> m_filterGroup;
		ar >> m_envReactionAttrib;
		ar >> typeSaveVersion;
		ar >> reservedCString;
		ar >> m_popoutBox.min.x;
		ar >> m_popoutBox.min.y;
		ar >> m_popoutBox.min.z;
		ar >> m_popoutBox.max.x;
		ar >> m_popoutBox.max.y;
		ar >> m_popoutBox.max.z;
		ar >> m_groupNumber;
		ar >> reservedFloat;
		ar >> m_attribute;
		ar >> m_soundFileName;
		ar >> m_useSound;
		ar >> m_soundFactor;
		ar >> m_isPlaying;
		ar >> m_validRange;
		ar >> m_environmentAttached;
		ar >> m_usedAsSpawnPoint;
		ar >> m_removeEnvironment;
		ar >> m_triggerList;
		ar >> m_containmentObject;
		ar >> m_lodVisualList;
		ar >> m_renderInMirrorTag;
		ar >> m_particleDirection.x;
		ar >> m_particleDirection.y;
		ar >> m_particleDirection.z;
		ar >> m_haloLink;
		ar >> m_lensFlareType;
		ar >> m_particleLink;
		ar >> m_fileNameRef;
		ar >> m_meshObject;
		ar >> m_collisionInfoFilter;
		ar >> m_renderFilter;
		ar >> m_soundRange;
		ar >> m_node;

		/////////////////////////////////////////////////////////////////////////////////////
		// Correct particle direction for legacy data -YC Jan 2011
		// -----------------------------------------------------------------------------------------------------------
		// The old default value for particle direction is (0,1,0), which is incorrect because the direction is supposed
		// to be emitter's forward direction (positive-Z) not up direction. It hadn't been causing problems because the value
		// wasn't actually used by particle system. Since we are now supporting emitter orientation, the legacy value
		// of this variable must be corrected, otherwise all world object particles will face the wrong direction.
		if (version < 11) {
			// With pre-11 version of the world objects, emitter orientation isn't working anyway.
			// Set it to (0,0,1) to make sure emitter face default direction.
			m_particleDirection.x = 0.0f;
			m_particleDirection.y = 0.0f;
			m_particleDirection.z = 1.0f;
		}
		// End correct particle direction for legacy data
		/////////////////////////////////////////////////////////////////////////////////////

		m_animModule = NULL;

		if (typeSaveVersion == 2) {
			ar >> m_animModule;
		}

		if (version > 1) {
			ar >> m_description; //version 1
			ar >> m_customizableTexture;
		}

		if (version > 2) {
			ar >> m_name;
		} else {
			m_name = m_fileNameRef; //default use filename
		}

		if (version > 3) {
			ar.Read(&m_uniqueId, sizeof(m_uniqueId));
		} else {
			// otherwise set to null and generate only on save
			memcpy(&m_uniqueId, &GUID_NULL, sizeof(m_uniqueId));
		}

		if (version > 4) {
			ar >> m_canHangPictures;
		}

		if (version > 5) { // version 6 12_27_06 CPITZER....persistent AABB support
			ar.Read(&m_min, sizeof(D3DXVECTOR4));
			ar.Read(&m_max, sizeof(D3DXVECTOR4));
			ar.Read(&m_cached_box, sizeof(bool));
		} else {
			m_min.x = 0.0;
			m_min.y = 0.0;
			m_min.z = 0.0;
			m_max.x = 0.0;
			m_max.y = 0.0;
			m_max.z = 0.0;
			m_cached_box = false;
		}

		// force recalculation of the boundbox
		m_cached_box = false;
		m_culled = TRUE;

		m_runtimeParticleId = 0;

#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
		m_poppedOut = FALSE;
#endif
		m_miscInt = 0;
		m_miscFloat = 0;
		m_baseSoundBuffer = 0;
		m_buffer3DControl = 0;
		m_showBox = FALSE;
		if (m_meshObject)
			m_meshObject->m_groupReference = m_groupNumber;

		// set the vector on the triggers to our position
		m_triggerList->ForEachTrigger([this](CTriggerObject* pTO) {
			if (m_meshObject)
				pTO->SetPosition(m_meshObject->GetMeshCenter());
			else if (m_node)
				pTO->SetPosition(m_node->GetTranslation());
		});

		// init the ReMesh object
		SetReMesh(NULL);
		// init the ReMesh LOD's
		for (UINT i = 0; i < m_maxLODs; i++) {
			m_ReLODMesh[i] = NULL;
		}

		if (version < 10) {
			// Use mesh center as pivot point for old assets
			if (m_meshObject) {
				const ABBOX& bbox = m_meshObject->GetBoundingBox();
				m_pivot.x = (bbox.minX + bbox.maxX) * 0.5f;
				m_pivot.y = (bbox.minY + bbox.maxY) * 0.5f;
				m_pivot.z = (bbox.minZ + bbox.maxZ) * 0.5f;

				// Set flag
				m_isMeshAssigned = TRUE;
			} else {
				// as of 03/09/2009, pivot point for RLB objects is world origin (0, 0, 0).
				m_pivot.x = m_pivot.y = m_pivot.z = 0.0f;
			}
		}

#ifdef _ClientOutput
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;

		// ReMesh-enabled version....stream it in from file
		if (version >= 7) {
			m_externalSource = NULL;
			m_material = NULL;

			// ReMesh used....not using legacy vertex arrays
			if (m_meshObject) { // RENDERENGINE INTERFACE
				// the new ReSkinMesh
				ReMesh* newMesh;
				// get an ReMeshStream object
				ReMeshStream streamer;
				// stream the ReMesh
				streamer.Read(&newMesh, ar, pICE->GetReDeviceState());
				// check for dups

				// This is a problem.  I actually want to allow dupes right now.
				// Or otherwise clone the mesh from AddMesh.  Or, I need to make sure
				// that m_ReMesh is always managed by ReMeshCreate.  Maybe it's supposed
				// to be?  Check, to see if it's supposed to be a reference to a shared
				// instance.

				ReMesh* mesh = NULL;
				ReMesh* addedMesh = NULL;
				newMesh->Clone(&mesh, true); // force the clone
					// add to global mesh pool
				ASSERT(mesh);
				if (mesh) {
					addedMesh = MeshRM::Instance()->AddMesh(mesh, MeshRM::RMP_ID_LEVEL);
				}
				mesh = NULL;
				// clone runtime copy.
				newMesh->Clone(&mesh, true); // force the clone
				ASSERT(mesh);
				if (mesh) {
					SetReMesh(mesh);
				}
				// pool the ReMaterials
				// check to see if this is the
				// special texture called "member_splash.tga", which
				// we always want to display at full res.
				if (StrSameIgnoreCase(m_meshObject->m_materialObject->m_texFileName[0], "member_splash.tga"))
					m_meshObject->m_materialObject->m_forceFullRes = true;

				MeshRM::Instance()->CreateReMaterial(m_meshObject->m_materialObject, Utf16ToUtf8(ar.m_strFileName).c_str());
				m_meshObject->m_materialObject->Clone(&m_material);
			}
		}
		// old file version
		else {
			// optimize the mesh objects
			if (m_meshObject) {
				//	Even for legacy objects we need to associate a texture to a material when this object loads
				m_meshObject->InitBaseMaterialTextures(pICE->GetTextureDataBase(), Utf16ToUtf8(ar.m_strFileName).c_str());
				m_meshObject->m_VertexArrayValid = FALSE;
				m_meshObject->m_pd3dDevice = pICE->GetReDeviceState()->GetDevice();

				// don't optimize vertexes for vertex animations
				if (!m_animModule)
					m_meshObject->OptimizeMesh();

				// create a new ReMesh the legacy way...
				SetReMesh(MeshRM::Instance()->CreateReStaticMesh(m_meshObject, m_pivot, Utf16ToUtf8(ar.m_strFileName).c_str())); //Pivot point needed - CreateReStaticMeshLevel

				// adjust the matrix position to compensate coords offset caused by ReMeshCreate
				// NOTE: once adjusted, the World Transform should only be used for ReMesh.
				Matrix44f pivotMatrix;
				pivotMatrix.MakeTranslation(m_pivot);
				m_meshObject->SetWorldTransform(pivotMatrix * m_meshObject->GetWorldTransform());

				// convert LOD's meshes into ReMesh
				if (m_lodVisualList) {
					for (POSITION lodPos = m_lodVisualList->GetHeadPosition(); lodPos != NULL;) {
						CMeshLODObject* mLod = (CMeshLODObject*)m_lodVisualList->GetNext(lodPos);

						// Generate ReMesh from CEXMeshObj. We are forced to use x_backward_compat_meshLodLevel here
						// because currently there is no other way to retrieve CEXMeshObj from CMeshLODObject.
						//@@Todo: let CMeshLodObject generate ReMesh by itself. No need to pass a CEXMeshObj out of CMeshLODObject.
						//@@Todo: once this is done (and also in CWldObject), we can remove x_backward_compat_meshLodLevel.

						mLod->x_backward_compat_meshLodLevel->InitBuffer(m_meshObject->m_pd3dDevice, NULL);
						mLod->x_backward_compat_meshLodLevel->OptimizeMesh();

						// create a new ReMesh the legacy way, using same pivot point as CWldObject
						mLod->SetReMesh(MeshRM::Instance()->CreateReStaticMesh(mLod->x_backward_compat_meshLodLevel, m_pivot, Utf16ToUtf8(ar.m_strFileName).c_str())); //Pivot point needed - CreateReStaticMeshLevel

						// copy matrix from base mesh
						mLod->SetMatrix(m_meshObject->GetWorldTransform());
					}
				}
			}
		}
#endif

		// Transformation
		if (version == 8 || version >= 10) {
			Vector3f scale, rotation, translation;
			ar >> scale.x >> scale.y >> scale.z;
			ar >> rotation.x >> rotation.y >> rotation.z;
			ar >> translation.x >> translation.y >> translation.z;

			if (version != 8) {
				// set matrix for zone object only because some node objects still have out-of-sync matrix and xform components
				// (e.g. drainage 264 at City_Zone.exg). At this time Refobj (see CNodeObject::Serialize) sets matrix by itself.
				// I will add detection code later to allow studio user to reconcile discrepancies before I can re-enable this call
				// for ref objs.
				if (m_meshObject)
					m_meshObject->SetScaleTranslationRotation(scale, translation, rotation);
			}
		} // if..version == 8 || version == 10

		if (version >= 7 && version < 10) {
			// Use old scheme for pre-V10 CWldObject (zone object only)
			if (m_meshObject)
				m_meshObject->SetTranslation(m_pivot); // Does not apply to RLB object
		}

		// Version 9 does nothing but only to remove data added by version 8

		// Version 10 restores transformation, adds pivot point and "mesh assigned" flag
		if (version >= 10) {
			ar >> m_pivot.x >> m_pivot.y >> m_pivot.z;
			ar >> m_isMeshAssigned;
		}
	}
}

void CWldObjectList::CacheUnimediateData() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWldObject* wldObj = (CWldObject*)GetNext(posLoc);
		if (wldObj->m_meshObject) {
			if (!wldObj->m_animModule) {
				if (wldObj->m_meshObject->m_materialObject) {
					//NOTE: only first two sets are checked in legacy code. Might need to be changed to check all if anyone cares. --YC 2011/01
					if (wldObj->m_meshObject->m_materialObject->m_enableSet[0] == FALSE &&
						wldObj->m_meshObject->m_materialObject->m_enableSet[1] == FALSE &&
						wldObj->m_meshObject->m_chromeWrap == FALSE) {
						wldObj->m_meshObject->m_abVertexArray.CacheUnimediateData();
					}
				}
			}
		}
	}
}

void CWldObjectList::IntegrityCheck() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWldObject* wldPtr = (CWldObject*)GetNext(posLoc);

		if (wldPtr->m_meshObject) {
			if (wldPtr->m_meshObject->m_abVertexArray.IntegrityCheck()) {
				CStringW s;
				s.LoadString(IDS_ERROR_INCONSISTENCYCORRECTED);
				AfxMessageBox(s + Utf8ToUtf16(wldPtr->m_fileNameRef).c_str());
			}
		}
	}
}

void CWldObjectList::CompileDirectAccess() {
	int objCount = (int)GetCount();
	if (objCount == 0)
		return;

	if (directAccess) {
		delete[] directAccess;
		directAccess = 0;
	}
	directAccess = new DirectAccessStructWLD[objCount];
	int tracer = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		directAccess[tracer].object = (CWldObject*)GetNext(posLoc);
		tracer++;
	}
	directAccessObjectCount = tracer;
}

void CWldObject::SetCustomMaterial(CMaterialObject* mat) {
	if (m_customMaterial) {
		mat->Clone(&m_customMaterial);
	} else {
		m_customMaterial = new CMaterialObject();
		mat->Clone(&m_customMaterial);
	}
}

void CWldObject::ResetCustomMaterial() {
#ifdef _ClientOutput
	if (m_customMaterial)
		delete m_customMaterial;
	m_customMaterial = NULL;
	m_customTexture = "";
	m_customTexturePath = eTexturePath::CustomTexture;
	m_customTextureSubDir = "";

	// update the ReMaterial
	CEXMeshObj* exMeshPtr = (CEXMeshObj*)this->m_meshObject;
	if (exMeshPtr && exMeshPtr->m_materialObject->GetReMaterial()) {
		ReRenderStateData state;
		exMeshPtr->m_materialObject->GetReRenderState(&state);
		exMeshPtr->m_materialObject->GetReMaterial()->Update(&state);
	}
#endif
}

CMaterialObject* CWldObject::GetCustomMaterial() const {
	return m_customMaterial;
}

void CWldObject::SetCustomTexture(const std::string& texFileName, eTexturePath texPath, const char* subDir) {
	// DRF - Added
	if (texFileName.empty()) {
		ResetCustomMaterial();
		return;
	}

	m_customTexture = texFileName;
	m_customTexturePath = texPath;

	if (subDir == NULL)
		m_customTextureSubDir.clear();
	else
		m_customTextureSubDir = subDir;
}

void CWldObject::UpdateCustomMaterials() {
#ifdef _ClientOutput
	//do this only if there have been changes
	if (m_customTexture.length() <= 0) {
		//no texture set previously...so delete it
		delete m_customMaterial;
		m_customMaterial = 0;
		return;
	}

	if (!m_customMaterial) {
		//no custom material previously set, copying material from refreence lib
		CEXMeshObj* exMeshPtr = (CEXMeshObj*)this->m_meshObject;
		if (exMeshPtr == NULL) {
			// no mesh on this ob? Can't set custom material
			// and need to avoid a crash.
			return;
		}
		SetCustomMaterial(exMeshPtr->m_materialObject);
	}

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	auto pTexDB = pICE->GetTextureDataBase();
	if (!pTexDB)
		return;

	DWORD nameHash = pTexDB->CustomTextureAdd(
		m_customMaterial->m_texNameHashStored[0],
		this->m_customTexture,
		m_customTextureSubDir,
		m_customMaterial->m_texColorKey[0],
		m_customTexturePath);

	if (ValidTextureNameHash(nameHash)) {
		m_customMaterial->m_texNameHash[0] = nameHash; //update lv 1 texture

		ReD3DX9DeviceState* dev = pICE->GetReDeviceState();

		// get the ReRenderState dataof this material
		ReRenderStateData data;
		m_customMaterial->GetReRenderState(&data);

		// create a temp ReRenderState on stack
		ReD3DX9RenderState state(NULL, &data);

		// create new ReMaterial
		ReMaterialPtr reMat(new ReD3DX9Material(dev, &data));

		// set the new material
		m_customMaterial->SetReMaterial(reMat);

		// update ReMesh pointer to new material -- previous one may have been deleted
		if (GetReMesh())
			GetReMesh()->SetMaterial(reMat);
	}
#endif
}

void CWldObject::SaveChanges() {
	m_customTextureOrig = m_customTexture;
}

void CWldObject::CancelChanges() {
	//roll back the changes
	m_customTexture = m_customTextureOrig;
	UpdateCustomMaterials();
}

void CWldObjectList::Serialize(CArchive& ar) {
	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CWldObject* wPtr = (CWldObject*)GetNext(posLoc);
			if (wPtr->m_meshObject && wPtr->m_meshObject->m_materialObject) {
				matCompressor->addMaterial(wPtr->m_meshObject->m_materialObject);
			}
			if (wPtr->m_lightObject && wPtr->m_lightObject->m_material) {
				matCompressor->addMaterial(wPtr->m_lightObject->m_material.get());
			}
		}
		matCompressor->compress();
		matCompressor->writeToArchive(ar); // write compressed materials
		CKEPObList::Serialize(ar);
	} else {
		int version = ar.GetObjectSchema();
		if (version >= 1) {
			matCompressor->readFromArchive(ar);
		}
		CKEPObList::Serialize(ar);
	}
	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
}

void CWldObject::SetScaleTranslationRotation(const Vector3f& scale, const Vector3f& translation, const Vector3f& rotation) {
	if (m_meshObject) {
		m_meshObject->SetScaleTranslationRotation(scale, translation, rotation);

		// update the LODs
		if (m_lodVisualList)
			m_lodVisualList->SetMatrixForAllLODs(m_meshObject->GetWorldTransform());
	} else if (m_node)
		m_node->SetScaleTranslationRotation(scale, translation, rotation);
}

void CWldObject::SetScale(const Vector3f& scale) {
	if (m_meshObject) {
		m_meshObject->SetScale(scale);

		// update the LODs
		if (m_lodVisualList)
			m_lodVisualList->SetMatrixForAllLODs(m_meshObject->GetWorldTransform());
	} else if (m_node)
		m_node->SetScale(scale);
}

void CWldObject::SetRotation(const Vector3f& rotation) {
	if (m_meshObject) {
		m_meshObject->SetRotation(rotation);

		// update the LODs
		if (m_lodVisualList)
			m_lodVisualList->SetMatrixForAllLODs(m_meshObject->GetWorldTransform());
	} else if (m_node)
		m_node->SetRotation(rotation);
}

void CWldObject::SetTranslation(const Vector3f& translation) {
	if (m_meshObject) {
		m_meshObject->SetTranslation(translation);

		// update the LODs
		if (m_lodVisualList)
			m_lodVisualList->SetMatrixForAllLODs(m_meshObject->GetWorldTransform());
	} else if (m_node)
		m_node->SetTranslation(translation);
}

IMPLEMENT_SERIAL_SCHEMA(CWldObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CWldObjectList, CKEPObList)

//! \brief Replace mesh with new data, rebuild ReMesh (refactored from KepEditView)
//! \param newMesh CEXMeshObj containing mesh data.
//! \param bUpdateMatrix Set to TRUE if matrix needs to be copied from CEXMeshObj as well.
void CWldObject::SetMesh(CEXMeshObj* newMesh, BOOL bUpdateMatrix /*= TRUE*/) {
#ifdef _ClientOutput

	ASSERT(newMesh != NULL);
	ASSERT(m_node == NULL); // call this function for World Obj only

	// Dispose old mesh object if exists
	if (m_meshObject) {
		m_meshObject->SafeDelete();
		delete m_meshObject;
		m_meshObject = 0;
		SetReMesh(NULL);
	}

	m_meshObject = newMesh;

	// For world objects, we need to apply transformation to all vertices.
	m_meshObject->BakeVertices();

	// Compute initial bounding box (untransformed)
	ABBOX bbox = m_meshObject->ComputeBoundingVolumes();

	if (!m_isMeshAssigned) {
		// First import: assign pivot point
		m_pivot.x = (bbox.minX + bbox.maxX) / 2;
		m_pivot.y = (bbox.minY + bbox.maxY) / 2;
		m_pivot.z = (bbox.minZ + bbox.maxZ) / 2;
		m_isMeshAssigned = TRUE;
	}

	// optimize the mesh objects
	if (m_meshObject && !m_animModule) {
		m_meshObject->OptimizeMesh();

		// create a new ReMesh the legacy way....
		SetReMesh(MeshRM::Instance()->CreateReStaticMesh(m_meshObject, m_pivot)); //Pivot point needed - CreateReStaticMeshLevel

		// locate the matrix by pivot point
		if (bUpdateMatrix) { // only update matrix for initial imports
			// adjust the matrix position to compensate coords offset caused by ReMeshCreate
			// NOTE: once adjusted, the World Transform should only be used for ReMesh.
			Matrix44f pivotMatrix;
			pivotMatrix.MakeTranslation(m_pivot);
			m_meshObject->SetWorldTransform(pivotMatrix * m_meshObject->GetWorldTransform());
		}
	}
#endif
}

//! \brief Replace material, rebuild ReMaterial (refactored from KepEditView)
//! \param newMaterial CMaterialObject containing material data.
//! \param zoneName Name of current zone. (used for creating ReMaterial)
void CWldObject::SetMaterial(CMaterialObject* newMaterial, const char* zoneName) {
#ifdef _ClientOutput
	m_meshObject->m_materialObject = newMaterial;
	if (!m_meshObject->m_materialObject->GetReMaterial()) {
		MeshRM::Instance()->CreateReMaterial(m_meshObject->m_materialObject, zoneName ? zoneName : "");
	} else {
		ReRenderStateData renderStateData;
		m_meshObject->m_materialObject->GetReRenderState(&renderStateData);
		m_meshObject->m_materialObject->GetReMaterial()->Update(&renderStateData);
	}
#endif
}

const Matrix44f& CWldObject::GetMatrix() const {
	ASSERT(m_meshObject != NULL || m_node != NULL);

	if (m_meshObject != NULL)
		return m_meshObject->GetWorldTransform();

	return m_node->GetWorldTransform(); // Transform of reference is per-instance
}

const ABBOX& CWldObject::GetBoundingBox() const {
	static ABBOX dummy = { 0.0f, 0.0f, 0.0f, 0.01f, 0.01f, 0.01f };

	ASSERT(m_meshObject != NULL || m_node != NULL);

	if (m_meshObject != NULL)
		return m_meshObject->GetBoundingBox();

	if (m_node != NULL)
		return m_node->m_clipBox;

	return dummy;
}

void CWldObject::SetBoundingBox(const ABBOX& box) {
	if (m_meshObject) {
		m_meshObject->SetBoundingBox(box);

		// update the LODs
		if (m_lodVisualList)
			m_lodVisualList->SetBoundingBoxForAllLODs(box);
	}

	else if (m_node) {
		m_node->m_clipBox = box;
	}
}

const CMaterialObject* CWldObject::GetMaterial() const {
	ASSERT(m_meshObject != NULL || m_node != NULL);

	if (m_meshObject != NULL)
		return m_material;

	if (m_node != NULL) {
		CReferenceObj* pRefObj = GetReferenceObj();
		if (pRefObj != NULL)
			return pRefObj->GetMaterial(); // Shared material among instances
		else
			ASSERT(FALSE);
	}

	return NULL;
}

std::string CWldObject::GetName() const {
	return (const char*)m_fileNameRef; // m_name??
}

CReferenceObj* CWldObject::GetReferenceObj() const {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return nullptr;

	CReferenceObjList* refLib = pICE->GetReferenceLibrary();
	if (!refLib)
		return nullptr;

	POSITION nodeLoc = refLib->FindIndex(m_node->m_libraryReference);
	if (nodeLoc == NULL)
		return NULL;

	CReferenceObj* refObj = (CReferenceObj*)refLib->GetAt(nodeLoc);
	return refObj;
}

Vector3f CWldObject::GetPivot() const {
	if (!IsReference())
		return m_pivot;

	CReferenceObj* pRefObj = GetReferenceObj();
	ASSERT(pRefObj != NULL);

	if (pRefObj)
		return pRefObj->GetPivot();

	return Vector3f(0.0f, 0.0f, 0.0f);
}

} // namespace KEP