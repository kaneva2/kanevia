///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <afxtempl.h>
#include <d3d9.h>
#include "CGroupClass.h"

namespace KEP {

//class is basically used for popout
CGroupObj::CGroupObj(
	float L_groupRadius,
	Vector3f L_groupCenter,
	BOOL L_valid) {
	m_groupRadius = L_groupRadius;
	m_groupCenter = L_groupCenter;
	m_valid = L_valid;
}

void CGroupObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_groupRadius
		   << m_groupCenter.x
		   << m_groupCenter.y
		   << m_groupCenter.z
		   << TRUE; //m_valid;
	} else {
		ar >> m_groupRadius >> m_groupCenter.x >> m_groupCenter.y >> m_groupCenter.z >> m_valid;
	}
}

IMPLEMENT_SERIAL(CGroupObj, CObject, 0)
IMPLEMENT_SERIAL(CGroupList, CObList, 0)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CGroupObj, IDS_GROUPOBJ_NAME)
GETSET_RANGE(m_groupRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, GROUPOBJ, GROUPRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_D3DVECTOR(m_groupCenter, GS_FLAGS_DEFAULT, 0, 0, GROUPOBJ, GROUPCENTER)
END_GETSET_IMPL

} // namespace KEP