///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <d3d9.h>
#include <d3dX9.h>
#include "DynamicObjectSpawnMonitor.h"
#include "dynamicobj.h"
#include "IDispatcher.h"
#include "IClientEngine.h" // TODO: reduce/eliminate use of IClientEngine::Instance inside XStatic
#include "CoreItems.h"
#include "common\include\LogHelper.h"

static LogInstance("Instance");

namespace KEP {

DynamicObjectSpawnMonitor::DynamicObjectSpawnMonitor() :
		m_isSpawning(false),
		m_customizationComplete(false),
		m_spawnStart(0) {}

void DynamicObjectSpawnMonitor::SetSpawning() {
	m_isSpawning = true;
	m_customizationComplete = false;
	m_spawnStart = fTime::TimeMs();
	m_DOSet.clear();
}

void DynamicObjectSpawnMonitor::CustomizationComplete(int dynamicObjects) {
	m_customizationComplete = true;
	if (dynamicObjects < 1)
		completeSpawn();
}

void DynamicObjectSpawnMonitor::CheckAddDynObjToMonitor(CDynamicPlacementObj* pDPO, Vector3f playerPos) {
	if (!m_isSpawning || !pDPO)
		return;

	// Paper doll NPC will always get local GLIDs. GLID_PAPERDOLL_NPC is only a placeholder in the DB.
	assert(pDPO->GetGlobalId() != GLID_PAPERDOLL_NPC);
	if (IS_LOCAL_GLID(pDPO->GetGlobalId())) {
		// Do not track paper doll NPC placements
		return;
	}

	Vector4f minB, maxB;
	pDPO->GetBoundingBoxAnimAdjust(minB, maxB);
	bool intersects = AABB_Sphere_Test(minB.SubVector<3>(), maxB.SubVector<3>(), playerPos, CHECK_RADIUS);
	if (intersects)
		m_DOSet.insert(pDPO->GetGlobalId());
}

//Returns true if intersecting the sphere or comepletely within it.
bool DynamicObjectSpawnMonitor::AABB_Sphere_Test(Vector3f aabbMin, Vector3f aabbMax, Vector3f center, float radius) {
	float dmin = 0;
	float r2 = radius * radius;
	for (int i = 0; i < 3; i++) {
		float sqrDist = 0;
		if (center[i] < aabbMin[i]) {
			sqrDist = (center[i] - aabbMin[i]) * (center[i] - aabbMin[i]);
			dmin += sqrDist;
		} else if (center[i] > aabbMax[i]) {
			sqrDist = (center[i] - aabbMax[i]) * (center[i] - aabbMax[i]);
			dmin += sqrDist;
		}
	}

	return (dmin <= r2);
}

void DynamicObjectSpawnMonitor::CheckDynObjLoadComplete(const GLID& glid) {
	if (!m_isSpawning)
		return;

	if (fTime::ElapsedMs(m_spawnStart) > SPAWN_WAIT) {
		std::stringstream ss;
		ss << "TIMEOUT - remainingDO=" << m_DOSet.size();
		if (!m_DOSet.empty()) {
			// Log a few pending global IDs for diagnostics
			ss << " - ( ";
			size_t count = 0;
			for (auto globalId : m_DOSet) {
				count++;
				if (count > 3) {
					ss << "... ";
					break;
				}
				ss << globalId << " ";
			}
			ss << ")";
		}
		LogWarn(ss.str());
		completeSpawn();
		return;
	}

	// Remove From Dynamic Object Set
	m_DOSet.erase(glid);

	// Customization Complete ?
	if (m_customizationComplete && m_DOSet.empty())
		completeSpawn();
}

void DynamicObjectSpawnMonitor::completeSpawn() {
	m_isSpawning = false;
	m_customizationComplete = false;
	m_DOSet.clear();
	m_spawnStart = 0;

#ifdef _ClientOutput
	// Send an event to notify UI that loading of critical DOs has completed

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	IDispatcher* dispatcher = pICE->GetDispatcher();
	if (dispatcher) {
		EVENT_ID eventId = dispatcher->GetEventId("DynamicObjectSpawnMonitorCompletedEvent");
		if (eventId != BAD_EVENT_ID) {
			IEvent* e = dispatcher->MakeEvent(eventId);
			dispatcher->QueueEvent(e);
		}
	}
#endif
}

} // namespace KEP
