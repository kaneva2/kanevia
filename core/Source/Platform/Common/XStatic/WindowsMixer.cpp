///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


// January 2016
// DirectX9 SDK (Oct 2004) and Windows 7.1A SDK
// Can't use stdafx.h because there are incompatibilities between the DirectX SDK used for the rest of the system and the current Windows SDK.
// Even though we don't use DirectX in this file, the DirectX include path is set after the Windows SDK path and overrides it.
// Some files in the Windows SDK are also in the DirectX sdk. We have a Windows header including one of these files, and instead of
// picking up the version in the Windows SDK, it's picking up an incompatible file in the DirectX SDK.
//
// I worked around this problem by adding the Windows SDK to the include path a second time (for this file only) in order to override
// the conflicting files in the DirectX SDK.
// I expect that this workaround can be removed
//#include <stdafx.h>

// ---------------------------------------------------------------------------------------------------
// From stdafx.h
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN // Exclude rarely-used stuff from Windows headers
#endif

#ifndef WINVER // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501 // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501 // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINDOWS // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE // Allow use of features specific to IE 4.0 or later.
#define _WIN32_IE 0x0501 // Change this to the appropriate value to target IE 5.0 or later.
#endif
#include <afxwin.h> // MFC core and standard components
// End from stdafx.h
// ---------------------------------------------------------------------------------------------------

#include "WindowsMixer.h"
#include "Core/Util/Unicode.h"
#include "Core/Math/KEPMathUtil.h"

#include <CGuid.h>
#include <Strmif.h>
#include <ks.h>
// Warning in Windows SDK header when building with VS2015
#pragma warning(push)
#pragma warning(disable : 4091) // 'typedef ': ignored on left of '' when no variable is declared
#include <Audiopolicy.h>
#pragma warning(pop)
#include <Mmdeviceapi.h>
#include <atlcomcli.h>

#include <atomic>

namespace KEP {

class WindowsMixerProxy : public IWindowsMixerProxy, public std::enable_shared_from_this<WindowsMixerProxy> {
public:
	WindowsMixerProxy() {}
	virtual ~WindowsMixerProxy() {
		UnregisterEventHandler();
	}

	virtual bool Initialize() override;
	virtual bool SetLabel(const char* pszLabel) override;
	virtual bool SetIconPath(const char* pszIconPath) override;
	virtual bool SetVolume(float fVolumeFraction) override;
	virtual bool GetVolume(Out<float> fVolumeFraction) override;
	virtual bool SetMute(bool bMute) override;
	virtual bool GetMute(Out<bool> bMute) override;
	virtual bool SetVolumeChangeCallback(std::function<void(float fNewVolume, bool bNewMute)> callback) override;

private:
	bool RegisterEventHandler();
	void UnregisterEventHandler();
	static CComPtr<IMMDevice> GetAudioDevice(EDataFlow flow = eRender, ERole role = eConsole);
	static CComPtr<IAudioSessionManager> GetAudioSessionManager(ERole role = eConsole, EDataFlow flow = eRender);

	void OnVolumeChanged(float fNewVolume, bool bNewMute);

private:
	struct SessionEventHandler : IAudioSessionEvents {
		SessionEventHandler(WindowsMixerProxy* pSessionEventHandler) :
				m_pOwner(pSessionEventHandler->shared_from_this()) {}
		virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, __RPC__deref_out void __RPC_FAR* __RPC_FAR* ppvObject) {
			if (riid == IID_IUnknown) {
				*ppvObject = static_cast<IUnknown*>(this);
				AddRef();
				return S_OK;
			}
			if (riid == __uuidof(IAudioSessionEvents)) {
				*ppvObject = static_cast<IAudioSessionEvents*>(this);
				AddRef();
				return S_OK;
			}
			*ppvObject = nullptr;
			return E_NOINTERFACE;
		}
		virtual ULONG STDMETHODCALLTYPE AddRef() {
			return m_iRefCount.fetch_add(1, std::memory_order_relaxed) + 1;
		}
		virtual ULONG STDMETHODCALLTYPE Release() {
			unsigned long iNewCount = m_iRefCount.fetch_add(-1, std::memory_order_relaxed) - 1;
			if (iNewCount == 0)
				delete this;
			return iNewCount;
		}

		virtual HRESULT STDMETHODCALLTYPE OnDisplayNameChanged(__in LPCWSTR NewDisplayName, LPCGUID EventContext) {
			return S_OK;
		}
		virtual HRESULT STDMETHODCALLTYPE OnIconPathChanged(__in LPCWSTR NewIconPath, LPCGUID EventContext) {
			return S_OK;
		}
		virtual HRESULT STDMETHODCALLTYPE OnSimpleVolumeChanged(__in float fNewVolume, __in BOOL NewMute, LPCGUID EventContext) {
			std::shared_ptr<WindowsMixerProxy> pOwner(m_pOwner);
			if (pOwner) {
				pOwner->OnVolumeChanged(fNewVolume, NewMute != FALSE);
			}
			return S_OK;
		}
		virtual HRESULT STDMETHODCALLTYPE OnChannelVolumeChanged(__in DWORD ChannelCount, __in_ecount(ChannelCount) float NewChannelVolumeArray[], __in DWORD ChangedChannel, LPCGUID EventContext) {
			return S_OK;
		}
		virtual HRESULT STDMETHODCALLTYPE OnGroupingParamChanged(__in LPCGUID NewGroupingParam, LPCGUID EventContext) {
			return S_OK;
		}
		virtual HRESULT STDMETHODCALLTYPE OnStateChanged(__in AudioSessionState NewState) {
			return S_OK;
		}
		virtual HRESULT STDMETHODCALLTYPE OnSessionDisconnected(__in AudioSessionDisconnectReason DisconnectReason) {
			return S_OK;
		}

		std::atomic<unsigned long> m_iRefCount = 0;
		std::weak_ptr<WindowsMixerProxy> m_pOwner;
	};

	CComPtr<IAudioSessionControl> m_pAudioSessionControl;
	CComPtr<ISimpleAudioVolume> m_pSimpleAudioVolume;
	CComPtr<SessionEventHandler> m_pSessionEventHandler;
	std::function<void(float fNewVolume, bool bNewMute)> m_VolumeChangeCallback;

	// Callbacks can occur on other threads, so we need to protect these.
	std::atomic<float> m_fLastVolume;
	std::atomic<bool> m_bLastMute;
};

std::shared_ptr<IWindowsMixerProxy> IWindowsMixerProxy::New() {
	std::shared_ptr<IWindowsMixerProxy> pWindowsMixerProxy = std::make_shared<WindowsMixerProxy>();
	if (!pWindowsMixerProxy->Initialize())
		return nullptr;

	return pWindowsMixerProxy;
}

CComPtr<IMMDevice> WindowsMixerProxy::GetAudioDevice(EDataFlow flow, ERole role) {
	HRESULT hr;
	CComPtr<IMMDeviceEnumerator> pEnumerator;
	CComPtr<IMMDevice> pDevice;

	// Get the enumerator for the audio endpoint devices
	// on this system.
	hr = CoCreateInstance(
		__uuidof(MMDeviceEnumerator),
		NULL, CLSCTX_INPROC_SERVER,
		__uuidof(IMMDeviceEnumerator),
		reinterpret_cast<void**>(&pEnumerator));
	if (hr != S_OK)
		return pDevice;

	hr = pEnumerator->GetDefaultAudioEndpoint(flow, role, &pDevice);
	if (hr != S_OK)
		return pDevice;

	return pDevice;
}

CComPtr<IAudioSessionManager> WindowsMixerProxy::GetAudioSessionManager(ERole role, EDataFlow flow) {
	HRESULT hr;
	CComPtr<IMMDevice> pDevice = GetAudioDevice(flow, role);
	if (!pDevice)
		return false;

	// Get the session manager for the endpoint device.
	CComPtr<IAudioSessionManager> pManager;
	hr = pDevice->Activate(
		__uuidof(IAudioSessionManager),
		CLSCTX_INPROC_SERVER, NULL,
		reinterpret_cast<void**>(&pManager));

	return pManager;
}

bool WindowsMixerProxy::Initialize() {
	CComPtr<IAudioSessionManager> pManager = GetAudioSessionManager();
	if (!pManager)
		return false;

	HRESULT hr;
	bool bSucceeded = true;
	hr = pManager->GetAudioSessionControl(nullptr, FALSE, &m_pAudioSessionControl);
	if (hr != S_OK)
		bSucceeded &= hr == S_OK;

	hr = pManager->GetSimpleAudioVolume(nullptr, FALSE, &m_pSimpleAudioVolume);
	if (hr != S_OK)
		bSucceeded &= hr == S_OK;

	// Default values for if GetVolume()/GetMute() fail.
	float fInitialVolume = 1;
	bool bInitialMute = false;
	GetVolume(out(fInitialVolume));
	GetMute(out(bInitialMute));
	m_bLastMute.store(bInitialMute, std::memory_order_relaxed);
	m_fLastVolume.store(fInitialVolume, std::memory_order_relaxed);

	RegisterEventHandler();

	return bSucceeded;
}

bool WindowsMixerProxy::SetLabel(const char* pszLabel) {
	if (!m_pAudioSessionControl)
		return false;

	HRESULT hr = m_pAudioSessionControl->SetDisplayName(Utf8ToUtf16(pszLabel).c_str(), NULL);
	return hr == S_OK;
}

bool WindowsMixerProxy::SetIconPath(const char* pszIconPath) {
	if (!m_pAudioSessionControl)
		return false;

	HRESULT hr = m_pAudioSessionControl->SetIconPath(Utf8ToUtf16(pszIconPath).c_str(), NULL);
	return hr == S_OK;
}

bool WindowsMixerProxy::SetMute(bool bMute) {
	if (!m_pSimpleAudioVolume)
		return false;

	m_bLastMute.store(bMute, std::memory_order_relaxed);
	HRESULT hr = m_pSimpleAudioVolume->SetMute(bMute, NULL);
	if (hr != S_OK)
		return false;

	return true;
}

bool WindowsMixerProxy::GetMute(Out<bool> bMute) {
	if (!m_pSimpleAudioVolume)
		return false;

	BOOL b = FALSE;
	HRESULT hr = m_pSimpleAudioVolume->GetMute(&b);
	if (hr == S_OK) {
		bMute.set(b != FALSE);
		return true;
	} else {
		return false;
	}
}
bool WindowsMixerProxy::SetVolume(float fVolume) {
	if (!m_pSimpleAudioVolume)
		return false;

	float fToSet = Clamp(fVolume, 0, 1);
	m_fLastVolume.store(fToSet, std::memory_order_relaxed);
	HRESULT hr = m_pSimpleAudioVolume->SetMasterVolume(fToSet, NULL);
	if (hr != S_OK)
		return false;

	return true;
}

bool WindowsMixerProxy::GetVolume(Out<float> fVolume) {
	if (!m_pSimpleAudioVolume)
		return false;

	float f = -1;
	HRESULT hr = m_pSimpleAudioVolume->GetMasterVolume(&f);
	if (hr == S_OK) {
		fVolume.set(f);
		return true;
	} else {
		return false;
	}
}

void WindowsMixerProxy::UnregisterEventHandler() {
	if (m_pAudioSessionControl && m_pSessionEventHandler)
		m_pAudioSessionControl->UnregisterAudioSessionNotification(m_pSessionEventHandler);
}

bool WindowsMixerProxy::RegisterEventHandler() {
	if (!m_pAudioSessionControl || !m_VolumeChangeCallback)
		return false;

	if (!m_pSessionEventHandler)
		m_pSessionEventHandler = new SessionEventHandler(this);
	m_pAudioSessionControl->RegisterAudioSessionNotification(m_pSessionEventHandler);
	return true;
}

bool WindowsMixerProxy::SetVolumeChangeCallback(std::function<void(float fNewVolume, bool bNewMute)> callback) {
	UnregisterEventHandler();
	m_VolumeChangeCallback = callback;
	if (!RegisterEventHandler())
		return false;
	return true;
}

void WindowsMixerProxy::OnVolumeChanged(float fNewVolume, bool bNewMute) {
	if (!m_VolumeChangeCallback)
		return;

	if (fNewVolume != m_fLastVolume.load(std::memory_order_relaxed) || bNewMute != m_bLastMute.load(std::memory_order_relaxed)) {
		m_fLastVolume.store(fNewVolume, std::memory_order_relaxed);
		m_bLastMute.store(bNewMute, std::memory_order_relaxed);
		m_VolumeChangeCallback(fNewVolume, bNewMute);
	}
}

} // namespace KEP