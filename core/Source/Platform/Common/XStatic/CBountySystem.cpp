///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CBountySystem.h"

namespace KEP {

CBountyObj::CBountyObj() {
	m_bountyAmount = 0;
	m_playerHandle = 0;
}

void CBountyObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CBountyObj* spnPtr = (CBountyObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CBountyObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_playerName
		   << m_bountyAmount
		   << m_playerHandle;
	} else {
		//int version = ar.GetObjectSchema();

		ar >> m_playerName >> m_bountyAmount;
		ar >> m_playerHandle;
	}
}

int CBountyObjList::AddBounty(const CStringA& playerName,
	int bountyAmount,
	PLAYER_HANDLE playerHandle,
	int maxBountyCount) {
	POSITION posLast;
	POSITION posLoc;
	for (posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CBountyObj* bPtr = (CBountyObj*)GetNext(posLoc);

		if (playerHandle == bPtr->m_playerHandle) {
			bPtr->m_bountyAmount = bountyAmount;
			return 2; //dont add twice just update him or her
		}
	}

	BOOL added = FALSE;
	for (posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CBountyObj* bPtr = (CBountyObj*)GetNext(posLoc);

		if (bountyAmount > bPtr->m_bountyAmount) {
			CBountyObj* bountyPtr = new CBountyObj();
			bountyPtr->m_playerName = playerName;
			bountyPtr->m_bountyAmount = bountyAmount;
			bountyPtr->m_playerHandle = playerHandle;
			InsertBefore(posLast, bountyPtr);
			added = TRUE;
			break;
		}
	}

	if (!added) {
		CBountyObj* bountyPtr = new CBountyObj();
		bountyPtr->m_playerName = playerName;
		bountyPtr->m_bountyAmount = bountyAmount;
		bountyPtr->m_playerHandle = playerHandle;
		AddTail(bountyPtr);
	}

	int trace = 0;
	for (posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CBountyObj* bPtr = (CBountyObj*)GetNext(posLoc);
		if (trace > maxBountyCount) {
			delete bPtr;
			bPtr = 0;
			RemoveAt(posLast);
		}

		trace++;
	}

	return 1;
}

void CBountyObjList::RemoveBountyByHandle(PLAYER_HANDLE mHandle) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CBountyObj* bPtr = (CBountyObj*)GetNext(posLoc);
		if (mHandle == bPtr->m_playerHandle) {
			delete bPtr;
			bPtr = 0;
			RemoveAt(posLast);
			return;
		}
	}
}

IMPLEMENT_SERIAL(CBountyObj, CObject, 0)
IMPLEMENT_SERIAL(CBountyObjList, CObList, 0)

BEGIN_GETSET_IMPL(CBountyObj, IDS_BOUNTYOBJ_NAME)
GETSET_MAX(m_playerName, gsCString, GS_FLAGS_DEFAULT, 0, 0, BOUNTYOBJ, PLAYERNAME, STRLEN_MAX)
GETSET_RANGE(m_bountyAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, BOUNTYOBJ, BOUNTYAMOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_playerHandle, gsInt, GS_FLAGS_DEFAULT, 0, 0, BOUNTYOBJ, PLAYERHANDLE, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP