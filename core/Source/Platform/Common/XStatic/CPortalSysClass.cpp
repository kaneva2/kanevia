///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CPortalSysClass.h"

#if (DRF_OLD_PORTALS == 1)

#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "gsD3dvectorValue.h"

namespace KEP {

CPortalSysObj::CPortalSysObj() :
		m_rotation(0) {
	m_keyRequired = 0; //0=no key required
	m_targetZoneIndex.clear();
	m_fromChannel.clear();
	m_wldPosition.Set(0, 0, 0); //target location
	ZeroMemory(&m_portalZone, sizeof(ABBOX));
}

BOOL CPortalSysObj::InZone(Vector3f currentLocation, const ChannelId& channel) {
	if (!channel.channelIdEquals(m_fromChannel))
		return FALSE;

	return BoundBoxPointCheck(currentLocation, m_portalZone);
}

BOOL CPortalSysObj::BoundBoxPointCheck(Vector3f& point,
	ABBOX& box) {
	if (point.x >= box.minX &&
		point.x <= box.maxX &&
		point.y >= box.minY &&
		point.y <= box.maxY &&
		point.z >= box.minZ &&
		point.z <= box.maxZ)
		return TRUE;
	return FALSE;
}

CPortalSysObj* CPortalSysList::ReturnPortal(Vector3f currentPosition, const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CPortalSysObj* spnPtr = (CPortalSysObj*)GetNext(posLoc);
		if (spnPtr->InZone(currentPosition, channel))
			return spnPtr;
	}
	return NULL;
}

void CPortalSysList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CPortalSysObj* spnPtr = (CPortalSysObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CPortalSysObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_portalName
		   << m_keyRequired
		   << m_targetZoneIndex.getClearedInstanceId()
		   << m_wldPosition.x
		   << m_wldPosition.y
		   << m_wldPosition.z
		   << m_portalZone.maxX
		   << m_portalZone.maxY
		   << m_portalZone.maxZ
		   << m_portalZone.minX
		   << m_portalZone.minY
		   << m_portalZone.minZ
		   << m_fromChannel
		   << m_rotation
		   << m_targetZoneIndex.GetInstanceId();
	} else {
		ar >> m_portalName >> m_keyRequired >> m_targetZoneIndex >> m_wldPosition.x >> m_wldPosition.y >> m_wldPosition.z >> m_portalZone.maxX >> m_portalZone.maxY >> m_portalZone.maxZ >> m_portalZone.minX >> m_portalZone.minY >> m_portalZone.minZ >> m_fromChannel;

		int schema = ar.GetObjectSchema();

		if (schema > 1)
			ar >> m_rotation;
		else
			m_rotation = 0;
		if (schema > 2) {
			long temp;
			ar >> temp;
			m_targetZoneIndex.clearInstance();
			m_targetZoneIndex.setInstanceId(temp);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CPortalSysObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CPortalSysList, CKEPObList)

BEGIN_GETSET_IMPL(CPortalSysObj, IDS_PORTALSYSOBJ_NAME)
GETSET_MAX(m_portalName, gsCString, GS_FLAGS_DEFAULT, 0, 0, PORTALSYSOBJ, PORTALNAME, STRLEN_MAX)
GETSET_RANGE(m_keyRequired, gsInt, GS_FLAGS_DEFAULT, 0, 0, PORTALSYSOBJ, KEYREQUIRED, INT_MIN, INT_MAX)
GETSET_RANGE(m_targetZoneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, PORTALSYSOBJ, TARGETWORLD, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_wldPosition, GS_FLAGS_DEFAULT, 0, 0, PORTALSYSOBJ, WLDPOSITION)
GETSET_RANGE(m_fromChannel, gsInt, GS_FLAGS_DEFAULT, 0, 0, PORTALSYSOBJ, ZONEWLDID, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP

#else
void CPortalSysClass_4221() {}
#endif
