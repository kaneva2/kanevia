///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#ifdef _ClientOutput
#include ".\stdafx.h"
#else
#include "..\..\engine\clientblades\kepmenu\dxstdafx.h"
#endif

#include "DirectShowMesh.h"

#if (DRF_DIRECT_SHOW_MESH == 1)

#include <TinyXML/tinyxml.h>
#include <js.h>
#include <jsThread.h>
#include "DownloadManager.h"

#include <mmsystem.h>
#include "../../tools/WMSDK/WindowsMedia.h"

#include "..\..\RenderEngine\ReD3DX9.h"
#include "..\..\RenderEngine\Utils\ReUtils.h"

#include "..\..\Tools\zlib\zlib.h"

#include "KEPException.h"
#include <d3d9.h>
#include <d3dx9tex.h>
#include <sys/stat.h>

#include "common\include\MediaParams.h"
#include "ResourceType.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

static size_t s_meshIds = 0; // meshes since launch

bool DirectShowMesh::s_enabledGlobal = true;
double DirectShowMesh::s_volPctGlobal = 100.0;

enum ST_RET {
	ST_VIDEO,
	ST_STILL,
	ST_ASXPLAYLIST,
	ST_NOT_MEDIA
};

static ST_RET urlType(const char* href) {
	// video
	static const char* vtypes[] = {
		".WMA",
		".WMV",
		".ASF",
		".MPEG",
		".AVI",
		".QT",
		".WAV",
		".AIFF",
		".AU",
		".SND",
		".MIDI",
		0
	};

	// still
	static const char* stypes[] = {
		".BMP",
		".GIF",
		".JPG",
		".TGA",
		0
	};

	std::string temp(href);
	STLToUpper(temp);

	// look at end for extension
	for (int i = 0; vtypes[i] != 0; i++) {
		std::string::size_type where = temp.rfind(vtypes[i]);
		if (where != std::string::npos &&
			where == (temp.length() - strlen(vtypes[i]))) {
			return ST_VIDEO;
		}
	}

	for (int i = 0; stypes[i] != 0; i++) {
		std::string::size_type where = temp.rfind(stypes[i]);
		if (where != std::string::npos &&
			where == (temp.length() - strlen(stypes[i]))) {
			return ST_STILL;
		}
	}

	// if the URL ends in .asx, then it's definitely an asx playlist
	std::string::size_type where = temp.rfind(".ASX");
	if (where != std::string::npos && where == (temp.length() - 4))
		return ST_ASXPLAYLIST;

	// assume video if mms
	if (strncmp(href, "mms://", 6) == 0)
		return ST_VIDEO;

	return ST_NOT_MEDIA;
}

/*
Here's a sample from TBS
<ASX version = "3.0">

    <Title>broadband video on tbs.com</Title>

                <entry clientskip="NO">
                        <ref href="http://ads.tbs.com/html.ng/site=tbs&tbs_rollup=video&tbs_section=shows&tbs_subsection=341124&tbs_pgtype=video&tbs_position=preroll_lg&tile=&Params.User.UserID="/>
                </entry>

    <Entry>
        <Title>10 Items or Less: What Women Want - Full Episode, Part 3</Title>
        <Abstract>Greens & Grains gets a little awkward after Leslie kisses Ingrid and Todd kisses Yolanda.</Abstract>
        <Ref href="mms://wmsturner.stream.aol.com/turner/gl/tbs/protected/10items_104_3_300k.wmv?auth=da.dva5aKd5dgdNdYacbKadaEaXd6c0agc6-bgAlWS-4q-mwDyxBu-q4t7kWmXqUn7kTtTsbmQ&aifp=abcd" />
    </Entry>

</ASX>*/
class AsxDownloadHandler : public jsThread, public DownloadHandler {
public:
	static const int BUFF_SIZE = (64 * 1024);
	static const int ASX_NEST_LIMIT = 16;

	AsxDownloadHandler(DownloadManager* dlMgr, const char* url, Logger& logger, WmvPlaylist** playlist) :
			jsThread("AsxDownloadHandler"),
			m_logger(logger),
			m_currentCount(0),
			m_playlist(playlist),
			m_downloadMgr(dlMgr),
			m_url(url),
			m_curDepth(0) {
		m_buffer = (char*)malloc(BUFF_SIZE);
		m_allocSize = BUFF_SIZE;
		DownloadCanceled(); // inits
	}

	virtual ~AsxDownloadHandler() {
		if (m_buffer)
			free(m_buffer);
	}

	std::string ToStr() {
		std::string str;
		StrBuild(str, "ASX<" << this << "> '" << m_url << "'");
		return str;
	}

	// parse the ASX "XML"
	virtual void DownloadComplete() {
		LogInfo(ToStr());

		/*
		 unfortunately can't use XML parse since XML is far from standard
		 found that attributes do *not* encoded ampersands, and element names
		 have varying cases
		Here's a sample from TBS
		<ASX version = "3.0">

		    <Title>broadband video on tbs.com</Title>

		                <entry clientskip="NO">
		                        <ref href="http://ads.tbs.com/html.ng/site=tbs&tbs_rollup=video&tbs_section=shows&tbs_subsection=341124&tbs_pgtype=video&tbs_position=preroll_lg&tile=&Params.User.UserID="/>
		                </entry>

		    <Entry>
		        <Title>10 Items or Less: What Women Want - Full Episode, Part 3</Title>
		        <Abstract>Greens & Grains gets a little awkward after Leslie kisses Ingrid and Todd kisses Yolanda.</Abstract>
		        <Ref href="mms://wmsturner.stream.aol.com/turner/gl/tbs/protected/10items_104_3_300k.wmv?auth=da.dva5aKd5dgdNdYacbKadaEaXd6c0agc6-bgAlWS-4q-mwDyxBu-q4t7kWmXqUn7kTtTsbmQ&aifp=abcd" />
		    </Entry>

		</ASX>

		// can ALSO be this format!!
		[Reference]
		Ref1=http://servername/path/asf-file1.asf
		Ref2= mms://servername/path/asf-file1.asf
		BaseURL = http://servername/path/
		*/
		m_buffer[m_currentCount] = '\0';
		std::string holdingBuffer(m_buffer); // in case we recurse, save off buffer
		std::string lowerBuffer(m_buffer);
		STLToLower(lowerBuffer);

		std::string::size_type curPos = 0;

		// need to find first entry, case insensitive
		if (lowerBuffer.find("<entry") != std::string::npos) {
			while ((curPos = lowerBuffer.find("<entry", curPos)) != std::string::npos) {
				bool ok = false;
				std::string::size_type nextPos = lowerBuffer.find("</entry>", curPos + 6);

				if (nextPos != std::string::npos) {
					// find the ref
					std::string::size_type refPos = lowerBuffer.find("<ref", curPos + 6);
					if (refPos != std::string::npos && refPos < nextPos) {
						std::string::size_type hrefPos = lowerBuffer.find("href", refPos + 4);
						if (hrefPos != std::string::npos && hrefPos < nextPos) {
							std::string::size_type startQuotePos = lowerBuffer.find("\"", hrefPos + 4);
							if (startQuotePos != std::string::npos && startQuotePos < nextPos) {
								std::string::size_type endQuotePos = lowerBuffer.find("\"", startQuotePos + 1);
								if (endQuotePos != std::string::npos && endQuotePos < nextPos) {
									std::string href(holdingBuffer.substr(startQuotePos + 1, endQuotePos - (startQuotePos + 1)));
									ST_RET stRet = urlType(href.c_str());

									// Add it to the playlist if it's a video stream or if
									// it's an unknown media type.  If it's unknown, we'll
									// just rely on the Windows Media SDK to hopefully
									// do the right thing with it.
									if (stRet == ST_VIDEO || stRet == ST_NOT_MEDIA) {
										LogInfo(ToStr() << " - ASX VIDEO '" << href << "'");
										m_workingPlaylist->push_back(href);
									} else if (stRet == ST_ASXPLAYLIST) {
										if (m_curDepth + 1 < ASX_NEST_LIMIT) {
											// assume asx, get the next one, recursively
											m_curDepth++;
											LogInfo(ToStr() << " - ASX PLAYLIST '" << href << "' depth=" << m_curDepth);
											m_currentCount = 0; // reset buffer
											m_downloadMgr->DownloadFileSync(ResourceType::DIRECTSHOW, href, this);
											m_curDepth--;
										} else {
											LogWarn(ToStr() << " - ASX PLAYLIST DEPTH LIMIT '" << href << "' depth=" << m_curDepth);
										}
									} else {
										LogWarn(ToStr() << " - ASX UNKNOWN '" << href << "'");
									}
									ok = true;
								}
							}
						}
						curPos = nextPos + 7;
					}
				}
				if (!ok) {
					LogError(ToStr() << " ASX INVALID XML");
				}
			}
		} else if (lowerBuffer.find("[reference") != std::string::npos) {
			/*
			[Reference]
			Ref1=http://servername/path/asf-file1.asf
			Ref2= mms://servername/path/asf-file1.asf
			BaseURL = http://servername/path/
			*/
			std::string::size_type startPos = 0;
			startPos = lowerBuffer.find("ref1=");
			if (startPos != std::string::npos) {
				std::string::size_type endPos = 0;
				endPos = lowerBuffer.find_first_of("\r\n", startPos);
				std::string href;
				if (endPos != std::string::npos) {
					href = holdingBuffer.substr(startPos + 5, endPos - (startPos + 5));
				} else {
					// take to end of buffer
					href = holdingBuffer.substr(startPos + 5);
				}
				ST_RET stRet = urlType(href.c_str());
				if (stRet == ST_VIDEO) {
					LogInfo(ToStr() << " - ASX VIDEO '" << href << "'");
					m_workingPlaylist->push_back(href);
				} else {
					LogWarn(ToStr() << " - ASX UNKNOWN '" << href << "'");
				}
			}
		} else {
			LogError(ToStr() << " - ASX EMPTY");
		}
	}

	virtual bool BytesDownloaded(char* _bytes, ULONG len) {
		if (this->isTerminated())
			return false;

		if (m_currentCount == 0) {
			// we only want asx files which start with <asx> or [reference]
			std::string s(_bytes, std::min(len, 50UL));
			STLToLower(s);
			if (s.find("<asx") == std::string::npos &&
				s.find("[reference") == std::string::npos) {
				LogWarn(ToStr() << " - ASX DOWNLOAD BAD GUESS");
				return false;
			}
		}

		if (m_currentCount + len > m_allocSize) {
			m_allocSize = m_currentCount + len + 512;
			m_buffer = (char*)realloc(m_buffer, m_allocSize);
		}

		if (m_buffer == 0) {
			LogError(ToStr() << " - NULL BUFFER");
			return false;
		}

		memcpy(m_buffer + m_currentCount, _bytes, len);
		m_currentCount += len;

		return true;
	}

	virtual void DownloadCanceled() {
		m_currentCount = 0;
	}

protected:
	virtual ULONG _doWork() {
		// get the first file, which will read any nested ones in it
		m_workingPlaylist = new WmvPlaylist;
		LogInfo(ToStr() << " - Downloading...");
		m_downloadMgr->DownloadFileSync(ResourceType::DIRECTSHOW, m_url, this);
		::InterlockedExchange((LONG*)m_playlist, (LONG)m_workingPlaylist);
		return 0;
	}

	ULONG m_allocSize;
	ULONG m_currentCount;

	char* m_buffer;

	Logger m_logger;
	WmvPlaylist** m_playlist;
	WmvPlaylist* m_workingPlaylist;
	DownloadManager* m_downloadMgr;
	std::string m_url;
	int m_curDepth;
};

///////////////////////////////////////////////////////////////////////////////
// DirectShow Mesh Functions
///////////////////////////////////////////////////////////////////////////////

DirectShowMesh::DirectShowMesh(DownloadManager* dlMgr) :
		m_renderer(NULL),
		m_3dDev(NULL),
		m_dlMgr(dlMgr),
		m_playlist(NULL),
		m_startedPlaying(false),
		m_fileDownloader(NULL),
		m_destruct(false) {
	// Assign Mesh Id
	++s_meshIds;
	m_meshId = s_meshIds;
}

DirectShowMesh::~DirectShowMesh() {
	LogInfo(StrThis() << " OK");
	m_destruct = true;
	if (m_renderer)
		DirectShowPlayerClose();
}

std::string DirectShowMesh::StrThis(bool verbose) const {
	std::string str;
	if (verbose) {
		StrAppend(str, "DSM<" << m_meshId << " " << m_mediaParams.ToStr(true) << ">");
	} else {
		StrAppend(str, "DSM<" << m_meshId << ">");
	}
	return str;
}

bool DirectShowMesh::DirectShowPlayerCreate(
	LPDIRECT3DDEVICE9 dev,
	const MediaParams& mediaParams,
	uint32_t /*width*/, uint32_t /*height*/,
	bool /*loop*/,
	int32_t /*x*/, int32_t /*y*/
) {
	// Valid Media Url & Params ?
	if (!mediaParams.IsUrlAndParamsValid()) {
		LogError("IsUrlAndParamsValid() FAILED - " << mediaParams.ToStr());
		return false;
	}

	// Setup Renderer
	m_3dDev = dev;
	m_renderer = new WMTexture();

	// Set Media Params (don't allow rewind)
	return SetMediaParams(mediaParams, false);
}

// This function immediately cleans up all directshow mesh resources and finally deletes the mesh.
bool DirectShowMesh::DirectShowPlayerClose() {
	LogInfo(StrThis() << " OK");

	// Delete All Resources
	if (m_fileDownloader) {
		m_fileDownloader->stop(2000, true);
		delete m_fileDownloader;
	}
	m_fileDownloader = NULL;
	if (m_renderer)
		delete m_renderer;
	m_renderer = NULL;
	if (m_playlist)
		delete m_playlist;
	m_playlist = NULL;

	// Delete Flash Mesh If Not Already Destructing
	if (!m_destruct)
		delete this;

	return true;
}

// start playing this video, called from one-off, or if asx playlist
bool DirectShowMesh::playVideo(const std::string& url) {
	// Set Media Url & Params
	m_mediaParams.SetUrl(url);

	// flush the texture buf
	SetTexture(nullptr);

	HRESULT hr = m_renderer->Initialize(url, m_3dDev);
	if (FAILED(hr)) {
		std::string msg;
		LogError("Initialize() FAILED - '" << url << "' " << errorNumToString(hr, msg));
		return false;
	}

	m_startedPlaying = true;
	return true;
}

bool DirectShowMesh::loadPlaylist() {
	// Get Media Params
	std::string url = m_mediaParams.GetUrl();

	// Play Stills Or Video (Playlist)
	ST_RET ut = urlType(url.c_str());
	if (ut == ST_ASXPLAYLIST) {
		if (m_fileDownloader) {
			m_fileDownloader->stop(200, true);
			delete m_fileDownloader;
			m_fileDownloader = NULL;
		}
		if (m_playlist)
			DELETE_AND_ZERO(m_playlist);
		m_fileDownloader = new AsxDownloadHandler(m_dlMgr, url.c_str(), m_logger, &m_playlist);
		m_fileDownloader->start();
	} else {
		// create a playlist of one item so it can loop
		m_playlist = new WmvPlaylist();
		m_playlist->push_back(url);
	}

	m_startedPlaying = false;
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// IExternalMesh Interface
///////////////////////////////////////////////////////////////////////////////

bool DirectShowMesh::SetMediaParams(const MediaParams& mediaParams, bool /*allowRewind*/) {
	// Set Super Class Media Params & Add To Meshes
	IExternalMesh::SetMediaParams(mediaParams);

	// Always Just Start Fresh
	loadPlaylist();

	return true;
}

// DRF - TODO
bool DirectShowMesh::SetVolume(double volPct) {
	// Set Super Class Volume
	IExternalMesh::SetVolume(volPct);

	// TODO - Set DirectShow Volume
	//	DWORD volSet = ((GetVolumeGlobal() / 100.0) * GetVolume();

	LogInfo(StrThis() << " OK - volPct=" << GetVolume());
	return true;
}

int DirectShowMesh::UpdateTexture() {
	if (m_renderer) {
		// TODO: if looping is made into an option, here's the check
		bool loop = true;
		bool endOfStream = m_renderer->CheckStatus();

		// play first/next video
		if (m_playlist && (!m_startedPlaying || endOfStream)) {
			if (m_playlist->size() > 0) {
				playVideo(m_playlist->front().c_str()); // always play top of list
				m_playlist->pop_front();

				// if empty clear it out for next reload of playlist
				if (m_playlist->size() == 0) {
					delete m_playlist;
					m_playlist = 0;
				}
			}
		} else if (m_startedPlaying && endOfStream && loop) {
			// reload playlist if hit end
			loadPlaylist();
		}

		if (GetTexture() == 0) {
			if (m_renderer->Texture()) {
				SetTexture(m_renderer->Texture());
			}
		}
	}

	return 0;
}

void DirectShowMesh::Delete() {
	LogInfo(StrThis() << " OK");
	DirectShowPlayerClose(); // immediately deletes directshow mesh
}

///////////////////////////////////////////////////////////////////////////////
// Static Functions
///////////////////////////////////////////////////////////////////////////////

// static
DirectShowMesh* DirectShowMesh::Create(
	DirectShowMesh* pDSM,
	LPDIRECT3DDEVICE9 dev,
	const MediaParams& mediaParams,
	uint32_t width, uint32_t height,
	bool loop,
	int32_t x, int32_t y) {
	LogInstance("Instance");

	// Valid DirectShowMesh ?
	if (!pDSM) {
		LogError("null DirectShowMesh");
		return nullptr;
	}

	// Create DirectShow Player With Media Params
	if (!pDSM->DirectShowPlayerCreate(dev, mediaParams, width, height, loop, x, y)) {
		LogError("DirectShowPlayerCreate() FAILED - " << mediaParams.ToStr());
		return nullptr;
	}

	return pDSM;
}

// static
bool DirectShowMesh::SetVolumeGlobal(double volPct) {
	// Set Global DirectShow Volume
	MEDIA_SET_VOLPCT(s_volPctGlobal, volPct);

	// TODO
	return true;
}

} // namespace KEP

#endif
