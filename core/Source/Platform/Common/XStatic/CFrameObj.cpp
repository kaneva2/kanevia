///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include "math.h"

#include "CFrameObj.h"

#include "Event/Glue/FrameIds.h"
#include "matrixarb.h"
#include "../../tools/wkglib/wkgtypes.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

CFrameObj::CFrameObj(CFrameObj* pFO_parent) {
	m_pFO_parent = pFO_parent;
	if (pFO_parent) {
		// add itself to parent
		if (!m_pFO_parent->m_pFOL_children)
			m_pFO_parent->m_pFOL_children = new CFrameObjList();
		m_frameMatrix = pFO_parent->m_frameMatrix;
		m_pFO_parent->m_pFOL_children->AddTail(this);
	} else {
		m_frameMatrix.MakeIdentity();
	}
	m_pFOL_children = nullptr;
}

void CFrameObj::SafeDelete() {
	//remove yourself from parents list
	if (m_pFO_parent) {
		POSITION posLast;
		if (m_pFO_parent->m_pFOL_children) {
			//update children
			for (POSITION posLoc = m_pFO_parent->m_pFOL_children->GetHeadPosition(); (posLast = posLoc) != NULL;) {
				auto pFO = (CFrameObj*)m_pFO_parent->m_pFOL_children->GetNext(posLoc);
				if (pFO == this) {
					m_pFO_parent->m_pFOL_children->RemoveAt(posLast);
					break;
				}
			}
		}
	}

	//if there are children attached move them to scene dependancy
	if (m_pFOL_children) {
		for (POSITION posLoc = m_pFOL_children->GetHeadPosition(); posLoc != NULL;) {
			auto pFO = (CFrameObj*)m_pFOL_children->GetNext(posLoc);
			pFO->m_pFO_parent = NULL;
		}
		m_pFOL_children->RemoveAll();
		delete m_pFOL_children;
		m_pFOL_children = nullptr;
	}
}

void CFrameObj::SetParent(CFrameObj* pFO_parent) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	//remove yourself from parents list
	if (m_pFO_parent) {
		POSITION posLast;
		for (POSITION posLoc = m_pFO_parent->m_pFOL_children->GetHeadPosition(); (posLast = posLoc) != NULL;) {
			auto pFO = (CFrameObj*)m_pFO_parent->m_pFOL_children->GetNext(posLoc);
			if (pFO == this) {
				m_pFO_parent->m_pFOL_children->RemoveAt(posLast);
				break;
			}
		}
	}
	m_pFO_parent = pFO_parent;

	//if valid parent add yourself
	if (pFO_parent) {
		if (!pFO_parent->m_pFOL_children)
			pFO_parent->m_pFOL_children = new CFrameObjList();
		pFO_parent->m_pFOL_children->AddTail(this);
	}
}

void CFrameObj::UpdateChildren(const Matrix44f& diffMatrix) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	//update children
	for (POSITION posLoc = m_pFOL_children->GetHeadPosition(); posLoc != NULL;) {
		auto pFO = (CFrameObj*)m_pFOL_children->GetNext(posLoc);
		pFO->m_frameMatrix = pFO->m_frameMatrix * diffMatrix;
		pFO->UpdateChildren(diffMatrix);
	}
}

void CFrameObj::AddTransformBefore2(CFrameObj* pFO, const Matrix44f& matrix) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	//transforms applied before makes object move local to itself
	if (m_pFOL_children->GetCount() > 0) {
		//Has Children
		if (!pFO) {
			//set to world space
			Matrix44f curMatrix;
			curMatrix = matrix * m_frameMatrix;
			Matrix44f inverseMatrix, differenceMatrix;
			inverseMatrix.MakeInverseOf(m_frameMatrix);
			differenceMatrix = inverseMatrix * curMatrix;
			m_frameMatrix = curMatrix;
			UpdateChildren(differenceMatrix);
		} else {
			//use ref
			Matrix44f curMatrix;
			curMatrix = matrix * pFO->m_frameMatrix;
			Matrix44f inverseMatrix, differenceMatrix;
			inverseMatrix.MakeInverseOf(m_frameMatrix);
			differenceMatrix = inverseMatrix * curMatrix;
			m_frameMatrix = curMatrix;
			UpdateChildren(differenceMatrix);
		}
	} else {
		//no children
		if (!pFO) {
			m_frameMatrix = matrix * m_frameMatrix;
		} else {
			m_frameMatrix = matrix * pFO->m_frameMatrix;
		}
	}
}

void CFrameObj::AddTransformBefore(const Matrix44f& matrix) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	//transforms applied before makes object move local to itself
	if (m_pFOL_children->GetCount() > 0) {
		//Has Children
		Matrix44f curMatrix;
		curMatrix = matrix * m_frameMatrix;
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(m_frameMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = curMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		//no children
		m_frameMatrix = matrix * m_frameMatrix;
	}
}

void CFrameObj::AddTransformAfter(const Matrix44f& matrix) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	//transforms applied before makes object move local to itself
	if (m_pFOL_children->GetCount() > 0) {
		//has children
		Matrix44f curMatrix = m_frameMatrix;
		Vector3f storeVect = m_frameMatrix.GetTranslation();
		curMatrix.GetTranslation().Set(0, 0, 0);
		curMatrix = curMatrix * matrix;
		curMatrix.GetTranslation() = storeVect + matrix.GetTranslation();
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(m_frameMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = curMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		// no children
		Vector3f storeVect = m_frameMatrix.GetTranslation();
		m_frameMatrix.GetTranslation().Set(0, 0, 0);
		m_frameMatrix = m_frameMatrix * matrix;
		m_frameMatrix.GetTranslation() = storeVect + matrix.GetTranslation();
	}
}

void CFrameObj::AddTransformReplace(const Matrix44f& matrix) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	if (m_pFOL_children->GetCount() > 0) {
		//Update children
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(m_frameMatrix);
		differenceMatrix = inverseMatrix * matrix;
		m_frameMatrix = matrix;
		UpdateChildren(differenceMatrix);
	} else {
		m_frameMatrix = matrix;
	}
}

void CFrameObj::AddTransformReplaceNoChildAdjust(const Matrix44f& matrix) {
	m_frameMatrix = matrix;
}

void CFrameObj::AddTransformReplaceRotationOnly(const Matrix44f& matrix) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	if (m_pFOL_children->GetCount() > 0) {
		//Update children
		Matrix44f curMatrix = m_frameMatrix;
		Vector3f cPosition = curMatrix.GetTranslation();
		curMatrix = matrix;
		curMatrix.GetTranslation() = cPosition;
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(m_frameMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = curMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		Vector3f cPosition = m_frameMatrix.GetTranslation();
		m_frameMatrix = matrix;
		m_frameMatrix.GetTranslation() = cPosition;
	}
}

void CFrameObj::GetTransform(CFrameObj* pFO, Matrix44f* matrix) const {
	if (!pFO) {
		//if refrence frame is world coordinate sys (NULL)
		*matrix = m_frameMatrix;
	} else {
		//if based on other frame matrix- i dont know how to do this one
		*matrix = m_frameMatrix;
	}
}

void CFrameObj::GetInverseTransform(Matrix44f* matrix) {
	matrix->MakeInverseOf(m_frameMatrix);
}

void CFrameObj::GetPosition(Vector3f& pos, const CFrameObj* pFO) const {
	pos = m_frameMatrix.GetTranslation();
	if (!pFO)
		return;

	//has a ref frame to it- localize the point from world space to local of ref frame
	pos = MatrixARB::GetLocalPositionFromMatrixView(pFO->m_frameMatrix, pos);
}

void CFrameObj::SetPosition(const Vector3f& pos, const CFrameObj* pFO) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	if (m_pFOL_children->GetCount() > 0) {
		//Has Children
		//set up
		Matrix44f curMatrix = m_frameMatrix;
		if (!pFO) {
			//set to world space
			curMatrix.GetTranslation() = pos;
		} else {
			//use ref
			Matrix44f matrixApply;
			matrixApply.MakeTranslation(pos);
			matrixApply = matrixApply * pFO->m_frameMatrix;
			curMatrix.GetTranslation() = matrixApply.GetTranslation();
		}

		//end set up
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(m_frameMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = curMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		//no children
		if (!pFO) {
			//set to world space
			m_frameMatrix.GetTranslation() = pos;
		} else {
			//use ref
			Matrix44f matrixApply;
			matrixApply.MakeTranslation(pos);
			matrixApply = matrixApply * pFO->m_frameMatrix;
			m_frameMatrix.GetTranslation() = matrixApply.GetTranslation();
		}
	}
}

void CFrameObj::GetOrientation(Vector3f& dir, Vector3f& up) const {
	Vector3f upVectLoc, dirVectLoc;
	upVectLoc.x = 0.0f;
	upVectLoc.y = 1.0f;
	upVectLoc.z = 0.0f;
	dirVectLoc.x = 0.0f;
	dirVectLoc.y = 0.0f;
	dirVectLoc.z = 1.0f;
	upVectLoc = TransformVector(m_frameMatrix, upVectLoc); //get position in world space
	dirVectLoc = TransformVector(m_frameMatrix, dirVectLoc); //get position in world space
	up = upVectLoc;
	dir = dirVectLoc;
}

int CFrameObj::GetOrientationAngle() const {
	Vector3f up, dir;
	GetOrientation(dir, up);
	Vector3f dirVector(dir.x, dir.y, dir.z);
	float theta = RadiansToDegrees(atan2(dirVector.x, dirVector.z));
	if (theta < 0.0f)
		theta += 360.0f;
	return (int)theta;
}

void CFrameObj::SetOrientation(const Vector3f& dirVect, const Vector3f& upVect) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	Matrix44f startMatrix = m_frameMatrix;
	if (m_pFOL_children->GetCount() > 0) {
		//Has Children
		Matrix44f inverseMatrix, differenceMatrix;
		Matrix44f curMatrix = m_frameMatrix;
		curMatrix.SetRowX(upVect.Cross(dirVect));
		curMatrix.SetRowY(upVect);
		curMatrix.SetRowZ(dirVect);
		curMatrix(3, 3) = 1.0;
		inverseMatrix.MakeInverseOf(startMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = m_frameMatrix * differenceMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		//no children
		m_frameMatrix.SetRowX(upVect.Cross(dirVect));
		m_frameMatrix.SetRowY(upVect);
		m_frameMatrix.SetRowZ(dirVect);
		m_frameMatrix(3, 3) = 1.0;
	}
}

void CFrameObj::LookAt(CFrameObj* pFO_target) {
	Vector3f VectTarget;
	pFO_target->GetPosition(VectTarget);
	FaceTowardPoint(VectTarget);
}

void CFrameObj::FaceTowardPoint(const Vector3f& targetVect) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	Matrix44f startMatrix = m_frameMatrix;

	float opp = 0;
	float adj = 0;
	float yaw = 0;
	float pitch = 0;

	Vector3f thisVect;
	GetPosition(thisVect);

	opp = thisVect.z - targetVect.z;
	adj = thisVect.x - targetVect.x;
	yaw = atan2(adj, opp);

	adj = thisVect.y - (targetVect.y + 0.001f);

	opp = sqrt((targetVect.x - thisVect.x) * (targetVect.x - thisVect.x) +
			   (targetVect.z - thisVect.z) * (targetVect.z - thisVect.z));
	pitch = atan2(adj, opp);
	yaw = -yaw;
	pitch = pitch;

	float cosyaw = cos(yaw);
	float sinyaw = sin(yaw);
	float cospitch = cos(pitch);
	float sinpitch = sin(pitch);

	if (m_pFOL_children->GetCount() > 0) {
		//Has children
		Matrix44f curMatrix = m_frameMatrix;

		if (yaw && pitch) {
			curMatrix(0, 0) = cosyaw;
			curMatrix(1, 0) = sinpitch * sinyaw;
			curMatrix(2, 0) = -cospitch * sinyaw;
			curMatrix(1, 1) = cospitch;
			curMatrix(2, 1) = sinpitch;
			curMatrix(0, 2) = sinyaw;
			curMatrix(1, 2) = -sinpitch * cosyaw;
			curMatrix(2, 2) = cospitch * cosyaw;

		} else if (yaw) {
			curMatrix(0, 0) = cosyaw;
			curMatrix(0, 2) = -sinyaw;
			curMatrix(2, 0) = sinyaw;
			curMatrix(2, 2) = cosyaw;
		} else if (pitch) {
			curMatrix(1, 1) = cospitch;
			curMatrix(2, 1) = sinpitch;
			curMatrix(1, 2) = -sinpitch;
			curMatrix(2, 2) = cospitch;
		}

		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(startMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = m_frameMatrix * differenceMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		//no children
		if (yaw && pitch) {
			m_frameMatrix(0, 0) = cosyaw;
			m_frameMatrix(1, 0) = sinpitch * sinyaw;
			m_frameMatrix(2, 0) = -cospitch * sinyaw;
			m_frameMatrix(1, 1) = cospitch;
			m_frameMatrix(2, 1) = sinpitch;
			m_frameMatrix(0, 2) = sinyaw;
			m_frameMatrix(1, 2) = -sinpitch * cosyaw;
			m_frameMatrix(2, 2) = cospitch * cosyaw;

		} else if (yaw) {
			m_frameMatrix(0, 0) = cosyaw;
			m_frameMatrix(0, 2) = -sinyaw;
			m_frameMatrix(2, 0) = sinyaw;
			m_frameMatrix(2, 2) = cosyaw;
		} else if (pitch) {
			m_frameMatrix(1, 1) = cospitch;
			m_frameMatrix(2, 1) = sinpitch;
			m_frameMatrix(1, 2) = -sinpitch;
			m_frameMatrix(2, 2) = cospitch;
		}
	}
}

void CFrameObj::AddTranslationBefore(float Xloc, float Yloc, float Zloc) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	Matrix44f startMatrix = m_frameMatrix;
	Matrix44f matrixApply;
	matrixApply.MakeTranslation(Vector3f(Xloc, Yloc, Zloc));

	if (m_pFOL_children->GetCount() > 0) {
		//Has Children
		Matrix44f curMatrix = m_frameMatrix;
		curMatrix = matrixApply * curMatrix;
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(startMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = m_frameMatrix * differenceMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		//has no children
		m_frameMatrix = matrixApply * m_frameMatrix;
	}
}

void CFrameObj::AddTranslationAfter(float Xloc, float Yloc, float Zloc) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();

	Matrix44f startMatrix = m_frameMatrix;

	if (m_pFOL_children->GetCount() > 0) {
		//Has Children
		Matrix44f curMatrix = m_frameMatrix;
		curMatrix[3][0] = Xloc + curMatrix[3][0];
		curMatrix[3][1] = Yloc + curMatrix[3][1];
		curMatrix[3][2] = Zloc + curMatrix[3][2];
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(startMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = m_frameMatrix * differenceMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		//has children
		m_frameMatrix[3][0] = Xloc + m_frameMatrix[3][0];
		m_frameMatrix[3][1] = Yloc + m_frameMatrix[3][1];
		m_frameMatrix[3][2] = Zloc + m_frameMatrix[3][2];
	}
}

void CFrameObj::AddTranslationReplace(float Xloc, float Yloc, float Zloc) {
	if (!m_pFOL_children)
		m_pFOL_children = new CFrameObjList();
	Matrix44f startMatrix = m_frameMatrix;

	if (m_pFOL_children->GetCount() > 0) {
		//Has children
		Matrix44f curMatrix = m_frameMatrix;
		curMatrix[3][0] = Xloc;
		curMatrix[3][1] = Yloc;
		curMatrix[3][2] = Zloc;
		Matrix44f inverseMatrix, differenceMatrix;
		inverseMatrix.MakeInverseOf(startMatrix);
		differenceMatrix = inverseMatrix * curMatrix;
		m_frameMatrix = m_frameMatrix * differenceMatrix;
		UpdateChildren(differenceMatrix);
	} else {
		//no children
		m_frameMatrix[3][0] = Xloc;
		m_frameMatrix[3][1] = Yloc;
		m_frameMatrix[3][2] = Zloc;
	}
}

void CFrameObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
}

void CFrameObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CFrameObj* ptrLoc = (CFrameObj*)GetNext(posLoc);
		ptrLoc->m_pFO_parent = NULL; //Invalidate directly
	}
	RemoveAll();
}

double CFrameObj::GetNumber(ULONG index) {
	initGetSet();

	switch (index) {
		case FRAMEIDS_POSITIONX: {
			Vector3f position;
			GetPosition(position);
			return position.x;
		}

		case FRAMEIDS_POSITIONY: {
			Vector3f position;
			GetPosition(position);
			return position.y;
		}

		case FRAMEIDS_POSITIONZ: {
			Vector3f position;
			GetPosition(position);
			return position.z;
		}
	}

	return GetSet::GetNumber(index);
}

void CFrameObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		pMemSizeGadget->AddObject(m_pFO_parent);
		pMemSizeGadget->AddObject(m_pFOL_children);
	}
}

void CFrameObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CFrameObj* pCFrameObj = static_cast<const CFrameObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCFrameObj);
		}
	}
}

IMPLEMENT_SERIAL(CFrameObj, CObject, 0)
IMPLEMENT_SERIAL(CFrameObjList, CObList, 0)

BEGIN_GETSET_IMPL(CFrameObj, IDS_FRAMEOBJ_NAME)
GETSET_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, FRAMEOBJ, POSITIONX, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, FRAMEOBJ, POSITIONY, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, FRAMEOBJ, POSITIONZ, INT_MIN, INT_MAX)
GETSET2_D3DVECTOR(m_frameMatrix(0, 0), GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, IDS_FRAMEOBJ_MATRIXROW1, IDS_FRAMEOBJ_MATRIXROW1)
GETSET2_D3DVECTOR(m_frameMatrix(1, 0), GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, IDS_FRAMEOBJ_MATRIXROW2, IDS_FRAMEOBJ_MATRIXROW2)
GETSET2_D3DVECTOR(m_frameMatrix(2, 0), GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, IDS_FRAMEOBJ_MATRIXROW3, IDS_FRAMEOBJ_MATRIXROW3)
GETSET2_D3DVECTOR(m_frameMatrix(3, 0), GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, IDS_FRAMEOBJ_MATRIXROW4, IDS_FRAMEOBJ_MATRIXROW4)
END_GETSET_IMPL

} // namespace KEP
