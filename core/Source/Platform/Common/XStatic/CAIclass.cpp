///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "AIclass.h"
#include "CFrameObj.h"
#include "CAIWEBCLASS.h"
#include "CAIScriptClass.h"
#include "CCollisionClass.h"
#include "CtextReadoutClass.h"
#include "CInventoryClass.h"
#include "CPhysicsEmuClass.h"
#include "CGlobalInventoryClass.h"
#include "ListItemMap.h"

namespace KEP {

CAIObjList* CAIObjList::m_pInstance = 0;

CAIObj::CAIObj() {
	m_curExperienceLevel = 0.0f;
	m_team = 0;
	m_energyToRetreat = 40;
	m_blitzAttack = FALSE;
	m_takeCover = FALSE;
	m_closeEnoughDist = 100;
	m_turnTolerance = 10;
	m_targetFrame = NULL;
	m_sightType = 0;
	m_sightRandVariable = 100;
	m_maxSightRadius = 1500;
	m_difficulty = 2;
	m_walkingType = TRUE;
	m_startAttackZone = 200;
	m_startAttackDist = 300;
	m_randChillTime = 10;
	m_pathfindingMethod = 0;
	m_randKamakazi = 2;
	m_AreaToRoam = 0.0f;

	m_currentMissileSpeed = 1.0f;

	m_condition = 0; //0=nothin  1=under attack
	m_desiredGoal = 0; //0=not sure  1=roam 2=defense 3=retreat 4=kill
	m_lastKnownEnergy = UNINITIALIZED_ENERGY;

	//speed overides
	m_normalSpeed = 20.0f;
	m_hussleSpeed = 30.0f;
	m_turnSpeed = 1.5f;

	//new map tree
	m_routePointCount = 0;
	m_currentRoute = NULL;
	m_routeCurPos = 0;
	m_curRadius = 0.0f;
	m_wldPos.x = 0.0f;
	m_wldPos.y = 0.0f;
	m_wldPos.z = 0.0f;
	m_mapCurLocation = 0;
	m_curMap = -1;

	//scripts
	m_curScript = NULL;
	m_spawnScript = -1;
	m_lookAt.x = 0.0f;
	m_lookAt.y = 0.0f;
	m_lookAt.z = 0.0f;

	m_feelerSys = NULL;
	m_leftBlocked = FALSE;
	m_rightBlocked = FALSE;

	m_floorTolerance = .7f;
	m_wallMinimumHeight = .3f;
	m_feelerRange = 10.0f;
	m_feelerDepth = -10.0f;
	m_feelerCycleTime = 300;
	m_feelerWidth = 3.0f;
	m_feelerHeight = 2.0f;
	m_feelerEnable = FALSE;
	m_aiCfgName = "Bot Cfg";
	m_useOverridePhysics = FALSE;
	m_safeDistance = 3000.0f;
	m_minAttackDistance = 100.0f;
	m_percentageAttack = 25;
	m_accuracy = 50;
	m_exponentTurning = TRUE;
	m_controlled = FALSE;
	m_travelingDesiredDist = 2.0f;
	m_needToDefend = FALSE;
	m_engageRadius = 100.0f;
	m_currentSpeedOverride = 0.0f;
	m_attackDelayStamp = 0;
	m_delayOfAttacks = 3000;
	m_followMaxDistance = 100; //100
	m_followMinDistance = 20; //20
	m_distanceQuerryDuration = 2000;
	m_distanceQuerryStamp = 0;
	m_attackMaxDistance = 150; //150
	m_attackMinDistance = 20; //20
	m_eyeLevel = 7.0f;
	m_sightCheckDuration = 500;
	m_sightCheckStamp = 0;
	m_targetInSight = TRUE;
	m_enableVision = TRUE;
	m_timeStampWhenTargetNotInSight = 0;
	m_durationTillTargetLost = 30000;

	m_attackDurationMax = 3000; //suto holding trigger down ;)
	m_attackDurationMin = 1000; //suto holding trigger down ;)
	m_attackTimeBetweenMax = 7000; //suto time before holding trigger down again
	m_attackTimeBetweenMin = 1000; //suto time before holding trigger down again
	m_attackingCurrently = FALSE; //currently holding trigger down
	m_attackStamp = 0;
	m_attackDurationCurrent = 0; //current round decision
	m_attackTimeBetweenCurrent = 0; //current round decision

	m_enableJumpAbility = TRUE;

	m_accuracyErrorMax = 6.0f;
	m_accuracyErrorMin = 2.0f;

	m_prefernceAboveTarget = 0.0f;
	m_agressive = TRUE;

	m_enableCashApplied = TRUE;
	m_maxCash = 100;
	m_minCash = 10;
	m_routeCurTarget = 0;

	m_neverAttacks = FALSE;
	//radius system
	m_radiusSysEnabled = FALSE;
	m_radiusSysInUse = FALSE;
	m_radiusSysCenter.x = 0.0f;
	m_radiusSysCenter.y = 0.0f;
	m_radiusSysCenter.z = 0.0f;
	m_radiusSysAttackOnExtents = FALSE;
	m_radiusSysManditoryReturn = FALSE;
	m_radiusSysLastRoutePosition = 0;
	m_radiusSysRange = 500.0f;
	m_radiusSysPriorDest.x = 0.0f;
	m_radiusSysPriorDest.y = 0.0f;
	m_radiusSysPriorDest.z = 0.0f;
	m_radiusSysIgnoreY = TRUE;
	m_dbIndex = 0;

	m_nameOvveride = "NAME";
	m_inventoryCfg = NULL;
	m_pMeshIds = NULL;
	m_baseMeshes = 0;
	m_entityCfgDBindexRef = 0;
	m_physicsModuleOverride = NULL;
	m_percentageOfDropping = 100;
	m_overriderMaxEnergy = 0;
	m_collisionDBOverride = -1;

	m_retreatStamp = fTime::TimeMs();
	m_retreating = FALSE;
}

void CAIObj::Clone(CAIObj** clone) {
	CAIObj* copyLocal = new CAIObj();

	copyLocal->m_team = m_team;
	copyLocal->m_energyToRetreat = m_energyToRetreat;
	copyLocal->m_blitzAttack = m_blitzAttack;
	copyLocal->m_takeCover = m_takeCover;
	copyLocal->m_closeEnoughDist = m_closeEnoughDist;
	copyLocal->m_turnTolerance = m_turnTolerance;
	copyLocal->m_sightType = m_sightType;
	copyLocal->m_sightRandVariable = m_sightRandVariable;
	copyLocal->m_maxSightRadius = m_maxSightRadius;
	copyLocal->m_difficulty = m_difficulty;
	copyLocal->m_walkingType = m_walkingType;
	copyLocal->m_startAttackZone = m_startAttackZone;
	copyLocal->m_startAttackDist = m_startAttackDist;
	copyLocal->m_randChillTime = m_randChillTime;
	copyLocal->m_pathfindingMethod = m_pathfindingMethod;
	copyLocal->m_randKamakazi = m_randKamakazi;
	copyLocal->m_AreaToRoam = m_AreaToRoam;
	copyLocal->m_condition = m_condition; //0=nothin  1=under attack
	copyLocal->m_desiredGoal = m_desiredGoal; //0=not sure  1=roam 2=defense 3=retreat 4=kill
	copyLocal->m_lastKnownEnergy = m_lastKnownEnergy;
	copyLocal->m_normalSpeed = m_normalSpeed;
	copyLocal->m_hussleSpeed = m_hussleSpeed;
	copyLocal->m_turnSpeed = m_turnSpeed;
	copyLocal->m_currentMissileSpeed = 1.0f;
	copyLocal->m_curScript = NULL;
	copyLocal->m_spawnScript = m_spawnScript;
	copyLocal->m_floorTolerance = m_floorTolerance;
	copyLocal->m_wallMinimumHeight = m_wallMinimumHeight;
	copyLocal->m_feelerRange = m_feelerRange;
	copyLocal->m_feelerDepth = m_feelerDepth;
	copyLocal->m_feelerCycleTime = m_feelerCycleTime;
	copyLocal->m_feelerWidth = m_feelerWidth;
	copyLocal->m_feelerHeight = m_feelerHeight;
	copyLocal->m_feelerEnable = m_feelerEnable;
	copyLocal->m_aiCfgName = m_aiCfgName;
	copyLocal->m_useOverridePhysics = m_useOverridePhysics;
	copyLocal->m_safeDistance = m_safeDistance;
	copyLocal->m_minAttackDistance = m_minAttackDistance;
	copyLocal->m_percentageAttack = m_percentageAttack;
	copyLocal->m_accuracy = m_accuracy;
	copyLocal->m_exponentTurning = m_exponentTurning;
	copyLocal->m_controlled = m_controlled;
	copyLocal->m_travelingDesiredDist = m_travelingDesiredDist;
	copyLocal->m_needToDefend = m_needToDefend;
	copyLocal->m_delayOfAttacks = m_delayOfAttacks;
	copyLocal->m_engageRadius = m_engageRadius;
	copyLocal->m_currentSpeedOverride = m_currentSpeedOverride;
	copyLocal->m_travelingDesiredDist = m_closeEnoughDist;
	copyLocal->m_followMaxDistance = m_followMaxDistance;
	copyLocal->m_followMinDistance = m_followMinDistance;
	copyLocal->m_distanceQuerryDuration = m_distanceQuerryDuration;
	copyLocal->m_distanceQuerryStamp = m_distanceQuerryStamp;
	copyLocal->m_attackMaxDistance = m_attackMaxDistance;
	copyLocal->m_attackMinDistance = m_attackMinDistance;
	copyLocal->m_eyeLevel = m_eyeLevel;
	copyLocal->m_sightCheckDuration = m_sightCheckDuration;
	copyLocal->m_enableVision = m_enableVision;
	copyLocal->m_timeStampWhenTargetNotInSight = m_timeStampWhenTargetNotInSight;
	copyLocal->m_durationTillTargetLost = m_durationTillTargetLost;
	copyLocal->m_attackDurationMax = m_attackDurationMax; //suto holding trigger down ;)
	copyLocal->m_attackDurationMin = m_attackDurationMin; //suto holding trigger down ;)
	copyLocal->m_attackTimeBetweenMax = m_attackTimeBetweenMax; //suto time before holding trigger down again
	copyLocal->m_attackTimeBetweenMin = m_attackTimeBetweenMin; //suto time before holding trigger down again
	copyLocal->m_enableJumpAbility = m_enableJumpAbility;
	copyLocal->m_accuracyErrorMax = m_accuracyErrorMax;
	copyLocal->m_accuracyErrorMin = m_accuracyErrorMax;
	copyLocal->m_prefernceAboveTarget = m_prefernceAboveTarget;
	copyLocal->m_agressive = m_agressive;
	copyLocal->m_enableCashApplied = m_enableCashApplied;
	copyLocal->m_maxCash = m_maxCash;
	copyLocal->m_minCash = m_minCash;
	copyLocal->m_routeCurTarget = m_routeCurTarget;
	copyLocal->m_radiusSysEnabled = m_radiusSysEnabled;
	copyLocal->m_radiusSysAttackOnExtents = m_radiusSysAttackOnExtents;
	copyLocal->m_radiusSysRange = m_radiusSysRange;
	copyLocal->m_dbIndex = m_dbIndex;

	copyLocal->m_baseMeshes = m_baseMeshes;
	copyLocal->m_nameOvveride = m_nameOvveride;
	copyLocal->m_entityCfgDBindexRef = m_entityCfgDBindexRef;
	copyLocal->m_percentageOfDropping = m_percentageOfDropping;
	copyLocal->m_curExperienceLevel = m_curExperienceLevel;
	copyLocal->m_overriderMaxEnergy = m_overriderMaxEnergy;
	copyLocal->m_collisionDBOverride = m_collisionDBOverride;

	if (m_inventoryCfg)
		m_inventoryCfg->Clone(&copyLocal->m_inventoryCfg);

	if (m_physicsModuleOverride)
		m_physicsModuleOverride->Clone(&copyLocal->m_physicsModuleOverride);

	if (m_baseMeshes > 0) {
		copyLocal->m_pMeshIds = new int[m_baseMeshes];
		CopyMemory(copyLocal->m_pMeshIds, m_pMeshIds, sizeof(int) * m_baseMeshes);
	}

	copyLocal->m_targetFrame = new CFrameObj(NULL);
	*clone = copyLocal;
}

void CAIObj::SafeDelete() {
	if (m_feelerSys) {
		delete m_feelerSys;
		m_feelerSys = 0;
	}

	if (m_targetFrame) {
		m_targetFrame->SafeDelete();
		delete m_targetFrame;
		m_targetFrame = 0;
	}

	if (m_currentRoute) {
		delete[] m_currentRoute;
		m_currentRoute = 0;
	}

	if (m_physicsModuleOverride) {
		delete m_physicsModuleOverride;
		m_physicsModuleOverride = 0;
	}

	if (m_pMeshIds) {
		delete[] m_pMeshIds;
		m_pMeshIds = 0;
	}

	if (m_inventoryCfg) {
		m_inventoryCfg->SafeDelete();
		delete m_inventoryCfg;
		m_inventoryCfg = 0;
	}

	if (m_curScript) {
		m_curScript->SafeDelete();
		delete m_curScript;
		m_curScript = 0;
	}
}

void CAIObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_team;
		ar << m_energyToRetreat;
		ar << m_blitzAttack;
		ar << m_takeCover;
		ar << m_closeEnoughDist;
		ar << m_turnTolerance;
		ar << m_sightType;
		ar << m_sightRandVariable;
		ar << m_maxSightRadius;
		ar << m_difficulty;
		ar << m_walkingType;
		ar << m_startAttackZone;
		ar << m_startAttackDist;
		ar << m_randChillTime;
		ar << m_pathfindingMethod;
		ar << m_randKamakazi;
		ar << m_AreaToRoam;
		ar << m_condition;
		ar << m_desiredGoal;
		ar << m_lastKnownEnergy;
		ar << m_currentMissileSpeed;
		ar << m_normalSpeed;
		ar << m_hussleSpeed;
		ar << m_turnSpeed;
		ar << m_routePointCount;
		ar << m_curRadius;
		ar << m_mapCurLocation;
		ar << m_curMap;
		ar << m_aiCfgName;
		ar << m_spawnScript;
		ar << m_floorTolerance;
		ar << m_wallMinimumHeight;
		ar << m_feelerRange;
		ar << m_feelerDepth;
		ar << (int)m_feelerCycleTime;
		ar << m_feelerWidth;
		ar << m_feelerHeight;
		ar << m_feelerEnable;
		ar << m_useOverridePhysics;
		ar << m_safeDistance;
		ar << m_minAttackDistance;
		ar << m_percentageAttack;
		ar << m_accuracy;
		ar << m_exponentTurning;
		ar << m_travelingDesiredDist;
		ar << m_followMaxDistance;
		ar << m_followMinDistance;
		ar << (int)m_distanceQuerryDuration;
		ar << m_attackMaxDistance;
		ar << m_attackMinDistance;
		ar << m_eyeLevel;
		ar << m_sightCheckDuration;
		ar << m_targetInSight;
		ar << m_enableVision;
		ar << (int)m_durationTillTargetLost;
		ar << m_attackDurationMax;
		ar << m_attackDurationMin;
		ar << m_attackTimeBetweenMax;
		ar << m_attackTimeBetweenMin;
		ar << m_attackingCurrently;
		ar << m_attackDurationCurrent;
		ar << m_attackTimeBetweenCurrent;
		ar << m_enableJumpAbility;
		ar << m_accuracyErrorMax;
		ar << m_accuracyErrorMin;
		ar << m_prefernceAboveTarget;
		ar << m_agressive;
		ar << m_enableCashApplied;
		ar << m_maxCash;
		ar << m_minCash;
		ar << m_radiusSysEnabled;
		ar << m_radiusSysAttackOnExtents;
		ar << m_radiusSysIgnoreY;
		ar << m_radiusSysRange;
		ar << m_physicsModuleOverride;
		ar << m_nameOvveride;
		ar << m_baseMeshes;
		ar << m_entityCfgDBindexRef;
		ar << m_inventoryCfg;
		ar << m_percentageOfDropping;
		ar << m_curExperienceLevel;
		ar << m_overriderMaxEnergy;
		ar << m_collisionDBOverride;
		;

		for (int loop = 0; loop < m_baseMeshes; loop++)
			ar << m_pMeshIds[loop];

	} else {
		int cycleTime;
		int dqd;
		int dttl;
		ar >> m_team >> m_energyToRetreat >> m_blitzAttack >> m_takeCover >> m_closeEnoughDist >> m_turnTolerance >> m_sightType >> m_sightRandVariable >> m_maxSightRadius >> m_difficulty >> m_walkingType >> m_startAttackZone >> m_startAttackDist >> m_randChillTime >> m_pathfindingMethod >> m_randKamakazi >> m_AreaToRoam >> m_condition >> m_desiredGoal >> m_lastKnownEnergy >> m_currentMissileSpeed >> m_normalSpeed >> m_hussleSpeed >> m_turnSpeed >> m_routePointCount >> m_curRadius >> m_mapCurLocation;
		ar >> m_curMap >> m_aiCfgName >> m_spawnScript >> m_floorTolerance >> m_wallMinimumHeight >> m_feelerRange >> m_feelerDepth >> cycleTime >> m_feelerWidth >> m_feelerHeight >> m_feelerEnable >> m_useOverridePhysics >> m_safeDistance >> m_minAttackDistance >> m_percentageAttack >> m_accuracy >> m_exponentTurning >> m_travelingDesiredDist >> m_followMaxDistance >> m_followMinDistance >> dqd >> m_attackMaxDistance >> m_attackMinDistance >> m_eyeLevel >> m_sightCheckDuration >> m_targetInSight >> m_enableVision >> dttl >> m_attackDurationMax >> m_attackDurationMin >> m_attackTimeBetweenMax >> m_attackTimeBetweenMin >> m_attackingCurrently >> m_attackDurationCurrent >> m_attackTimeBetweenCurrent >> m_enableJumpAbility >> m_accuracyErrorMax >> m_accuracyErrorMin >> m_prefernceAboveTarget >> m_agressive >> m_enableCashApplied >> m_maxCash >> m_minCash >> m_radiusSysEnabled >> m_radiusSysAttackOnExtents >> m_radiusSysIgnoreY >> m_radiusSysRange >> m_physicsModuleOverride >> m_nameOvveride >> m_baseMeshes >> m_entityCfgDBindexRef >> m_inventoryCfg >> m_percentageOfDropping >> m_curExperienceLevel >> m_overriderMaxEnergy >> m_collisionDBOverride;
		m_feelerCycleTime = cycleTime;
		m_distanceQuerryDuration = dqd;
		m_durationTillTargetLost = dttl;

		if (m_baseMeshes > 0)
			m_pMeshIds = new int[m_baseMeshes];

		for (int loop = 0; loop < m_baseMeshes; loop++)
			ar >> m_pMeshIds[loop];

		m_retreatStamp = fTime::TimeMs();
		m_retreating = FALSE;

		m_targetFrame = 0;
		m_needToDefend = FALSE;
		m_controlled = FALSE;
		m_currentMissileSpeed = 1.0f;
		m_delayOfAttacks = 3000; //three seconds
		m_engageRadius = 100;
		m_currentSpeedOverride = 0.0f;

		m_attackStamp = 0;
		m_timeStampWhenTargetNotInSight = 0;
		m_sightCheckStamp = 0;
		m_attackDelayStamp = 0;
		m_distanceQuerryStamp = 0;

		m_attackingCurrently = 0;
		m_attackTimeBetweenCurrent = 0;
		m_routeCurTarget = 0;

		m_radiusSysInUse = FALSE;
		m_radiusSysCenter.x = 0.0f;
		m_radiusSysCenter.y = 0.0f;
		m_radiusSysCenter.z = 0.0f;
		m_radiusSysManditoryReturn = FALSE;
		m_radiusSysLastRoutePosition = 0;
		m_radiusSysPriorDest.x = 0.0f;
		m_radiusSysPriorDest.y = 0.0f;
		m_radiusSysPriorDest.z = 0.0f;
		m_dbIndex = 0;
	}
}

BOOL CAIObj::RadiusSysAllowMovement(Vector3f wldSpcDestination,
	Vector3f& newDestination,
	int& targetTraceRef) {
	if (!m_radiusSysEnabled)
		return TRUE;

	if (!m_radiusSysInUse)
		return TRUE;

	Vector3f offset = m_radiusSysCenter - wldSpcDestination;
	if (m_radiusSysIgnoreY)
		offset.y = 0;
	float distance = offset.Length();

	if (distance < m_radiusSysRange && m_radiusSysManditoryReturn == FALSE) {
		//this is a good
		return TRUE;
	} //end this is a good
	else if (m_radiusSysAttackOnExtents == FALSE && m_radiusSysManditoryReturn == FALSE) {
		//target is out of extents return to center
		//reset destination to
		m_radiusSysManditoryReturn = TRUE;
		newDestination.x = m_radiusSysCenter.x;
		newDestination.y = m_radiusSysCenter.y;
		newDestination.z = m_radiusSysCenter.z;
		m_lookAt.x = m_radiusSysCenter.x;
		m_lookAt.y = m_radiusSysCenter.y;
		m_lookAt.z = m_radiusSysCenter.z;

		//nullify targets
		targetTraceRef = -1;

		return FALSE;
	} else if (m_radiusSysManditoryReturn == TRUE) {
		//when you get back to manditory becan set previous target then return to route
		if (distance <= m_closeEnoughDist) {
			newDestination.x = m_radiusSysPriorDest.x;
			newDestination.y = m_radiusSysPriorDest.y;
			newDestination.z = m_radiusSysPriorDest.z;
			m_lookAt.x = m_radiusSysPriorDest.x;
			m_lookAt.y = m_radiusSysPriorDest.y;
			m_lookAt.z = m_radiusSysPriorDest.z;

			m_radiusSysInUse = FALSE;
			m_radiusSysManditoryReturn = FALSE;
			targetTraceRef = -1;
		}
		return TRUE;
	}

	return FALSE;
}

BOOL CAIObj::EnableRadiusSystem(Vector3f& currentPosition,
	int curRouteDestNode,
	Vector3f& currentDestination) {
	if (!m_radiusSysEnabled)
		return FALSE;

	if (m_radiusSysInUse)
		return FALSE; //even if target changes dont move pivot

	m_radiusSysInUse = TRUE;
	m_radiusSysLastRoutePosition = curRouteDestNode;

	if (currentDestination.x != 0.0f || currentDestination.y != 0.0f || currentDestination.z != 0.0f)
		m_radiusSysPriorDest = currentDestination;
	else
		m_radiusSysPriorDest = currentPosition;

	m_radiusSysCenter = currentPosition;

	return TRUE;
}

BOOL CAIObj::CheckVisionIsClear(Vector3f startPoint, Vector3f endPoint, CCollisionObj* m_collisionBase) {
	Vector3f intersect, normal;
	int objectHit;

	if (TRUE == m_collisionBase->CollisionSegmentIntersect(endPoint, startPoint, 50.0f, true, false, &intersect, &normal, &objectHit))
		return FALSE;

	return TRUE;
}

void CAIObj::FeelerSystemContruction(float distance,
	float depth,
	TimeMs cycleTime,
	float width,
	float height) {
	if (m_feelerSys) {
		delete m_feelerSys;
		m_feelerSys = 0;
	}

	m_feelerSys = new FeelerSystem;
	ZeroMemory(m_feelerSys, sizeof(FeelerSystem));
	m_feelerSys->m_cycleTime = cycleTime;
	m_feelerSys->m_status = FALSE;
	//poly 0 North

	m_feelerSys->m_startPoint.x = 0.1f;
	m_feelerSys->m_startPoint.y = height;
	m_feelerSys->m_startPoint.z = 0.0f;
	m_feelerSys->m_endPoint.x = -0.1f;
	m_feelerSys->m_endPoint.y = height;
	m_feelerSys->m_endPoint.z = -distance;

	m_feelerSys->m_incrementOnWall = 3.5f;

	m_feelerSys->m_wallDirection = 0; //default right

	m_feelerSys->m_wallFeelerStartPoint.Set(0, 0, 0);
	m_feelerSys->m_wallFeelerEndPoint.Set(0, 0, 0);

	m_feelerSys->m_mostRecentWallNormal.x = 0.0f;
	m_feelerSys->m_mostRecentWallNormal.y = 0.0f;
	m_feelerSys->m_mostRecentWallNormal.z = 1.0f;
}

BOOL CAIObj::UpdateFeelerStatus(CCollisionObj* collisionSys,
	Matrix44f positionMatrix,
	float floorTolerance,
	float wallMinHeight,
	float currentYpos,
	CtextReadoutObj* debug,
	BOOL m_reachDestination,
	Vector3f currentAIDestinationRef) {
	if (!m_feelerSys)
		return FALSE;

	Vector3f startPos = TransformPoint(positionMatrix, m_feelerSys->m_startPoint);
	Vector3f endPos = TransformPoint(positionMatrix, m_feelerSys->m_endPoint);

	Vector3f origin = positionMatrix.GetTranslation();

	if (m_feelerSys->m_onWallCurrently == TRUE) {
		//wall checker
		BOOL onWallStill = FALSE;
		BOOL floorStillhere = FALSE;
		//update wall vector based on position change ;)))))))))))))
		Vector3f offsetPosVector;
		offsetPosVector.x = startPos.x - m_feelerSys->m_wallFeelerStartPoint.x;
		offsetPosVector.y = startPos.y - m_feelerSys->m_wallFeelerStartPoint.y;
		offsetPosVector.z = startPos.z - m_feelerSys->m_wallFeelerStartPoint.z;

		m_feelerSys->m_wallFeelerStartPoint.x += offsetPosVector.x;
		m_feelerSys->m_wallFeelerStartPoint.y += offsetPosVector.y;
		m_feelerSys->m_wallFeelerStartPoint.z += offsetPosVector.z;
		m_feelerSys->m_wallFeelerEndPoint.x += offsetPosVector.x;
		m_feelerSys->m_wallFeelerEndPoint.y += offsetPosVector.y;
		m_feelerSys->m_wallFeelerEndPoint.z += offsetPosVector.z;

		Vector3f normalDump;
		collisionSys->CollisionSegmentSIValidPath(
			m_feelerSys->m_wallFeelerStartPoint,
			m_feelerSys->m_wallFeelerEndPoint,
			3.0f,
			m_floorTolerance,
			m_wallMinimumHeight,
			currentYpos,
			origin,
			&onWallStill,
			&floorStillhere,
			&normalDump);
		if (!onWallStill) {
			m_feelerSys->m_onWallCurrently = FALSE;
		}
	} //end wall checker

	BOOL wallInWay = FALSE;
	BOOL floorExists = FALSE;

	collisionSys->CollisionSegmentSIValidPath(
		startPos,
		endPos,
		3.0f,
		m_floorTolerance,
		m_wallMinimumHeight,
		currentYpos,
		origin,
		&wallInWay,
		&floorExists,
		&m_feelerSys->m_mostRecentWallNormal);

	if (wallInWay == TRUE /* || floorExists == FALSE*/) {
		m_feelerSys->m_status = TRUE;
		//set Up wall hook
		if (!m_feelerSys->m_onWallCurrently) {
			//hook onto wall
			m_feelerSys->m_onWallCurrently = TRUE;
			//copy currentFeeler to wall feeler
			m_feelerSys->m_wallFeelerStartPoint = startPos;
			m_feelerSys->m_wallFeelerEndPoint = endPos;
			//create matrix
			Matrix44f genOriMatrix;
			Vector3f tempUpVect, tempDirVector;
			tempUpVect.Set(0, 1, 0);
			tempDirVector.x = m_feelerSys->m_mostRecentWallNormal.x;
			tempDirVector.y = 0.0f;
			tempDirVector.z = m_feelerSys->m_mostRecentWallNormal.z;
			tempDirVector.Normalize();
			genOriMatrix.MakeIdentity();
			MatrixARB::SetOrientation(&genOriMatrix, tempDirVector, tempUpVect);
			genOriMatrix.GetTranslation() = positionMatrix.GetTranslation();
			//select logical progression
			Vector3f localDestRef = MatrixARB::GetLocalPositionFromMatrixView(genOriMatrix, currentAIDestinationRef);
			if (localDestRef.x < 0)
				m_feelerSys->m_wallDirection = 0; //right
			else
				m_feelerSys->m_wallDirection = 1; //left
		} //end hook onto wall

	} else
		m_feelerSys->m_status = FALSE;

	return TRUE;
}

void CAIObj::UpdatePolygonTransform(Matrix44f posMatrix, Vector3f polygonLoc[3], Vector3f outPut[3]) {
	for (size_t i = 0; i < 3; ++i)
		outPut[i] = TransformPoint_NonPerspective(posMatrix, polygonLoc[i]);
}

void CAIObjList::ResetIndexReferences() {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIObj* aiPtr = (CAIObj*)GetNext(posLoc);
		aiPtr->m_dbIndex = trace;
		trace++;
	}
}

CAIObj* CAIObjList::GetByIndex(int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;

	CAIObj* aiPtr = (CAIObj*)GetAt(posLoc);
	return aiPtr;
}

void CAIObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIObj* delPtr = (CAIObj*)GetNext(posLoc);
		delPtr->SafeDelete();
		delete delPtr;
		delPtr = 0;
	}
	RemoveAll();
}

void CAIObjList::RefreshData(CGlobalInventoryObjList* globalInvList) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIObj* aiPtr = (CAIObj*)GetNext(posLoc);

		if (aiPtr->m_inventoryCfg)
			for (POSITION aiInvPos = aiPtr->m_inventoryCfg->GetHeadPosition(); aiInvPos != NULL;) {
				CInventoryObj* invObj = (CInventoryObj*)aiPtr->m_inventoryCfg->GetNext(aiInvPos);

				CGlobalInventoryObj* glPtr = globalInvList->GetByGLID(invObj->m_itemData->m_globalID);
				if (glPtr) {
					if (glPtr->m_itemData)
						if (invObj->m_itemData) {
							invObj->m_itemData = 0;
							invObj->m_itemData = glPtr->m_itemData;
						}
				}
			}
	}
}

ListItemMap CAIObjList::GetBots() {
	ListItemMap botMap;

	int index = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIObj* aiPtr = dynamic_cast<CAIObj*>(GetNext(posLoc));
		if (aiPtr) {
			botMap.insert(ListItemMap::value_type(index, aiPtr->m_aiCfgName.GetString()));
			index++;
		} else {
			ASSERT(0);
		}
	}

	return botMap;
}

IMPLEMENT_SERIAL(CAIObj, CObject, 0)
IMPLEMENT_SERIAL(CAIObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CAIObj, IDS_AIOBJ_NAME)
GETSET_RANGE(m_team, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, TEAM, INT_MIN, INT_MAX)
GETSET_RANGE(m_energyToRetreat, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ENERGYTORETREAT, INT_MIN, INT_MAX)
GETSET(m_blitzAttack, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, BLITZATTACK)
GETSET(m_takeCover, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, TAKECOVER)
GETSET_RANGE(m_closeEnoughDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, CLOSEENOUGHDIST, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_turnTolerance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, TURNTOLERANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_sightType, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, SIGHTTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_sightRandVariable, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, SIGHTRANDVARIABLE, INT_MIN, INT_MAX)
GETSET_RANGE(m_maxSightRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, MAXSIGHTRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_difficulty, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, DIFFICULTY, INT_MIN, INT_MAX)
GETSET(m_walkingType, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, WALKINGTYPE)
GETSET_RANGE(m_startAttackZone, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, STARTATTACKZONE, INT_MIN, INT_MAX)
GETSET_RANGE(m_startAttackDist, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, STARTATTACKDIST, INT_MIN, INT_MAX)
GETSET_RANGE(m_randChillTime, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RANDCHILLTIME, INT_MIN, INT_MAX)
GETSET_RANGE(m_pathfindingMethod, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, PATHFINDINGMETHOD, INT_MIN, INT_MAX)
GETSET_RANGE(m_randKamakazi, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RANDKAMAKAZI, INT_MIN, INT_MAX)
GETSET_RANGE(m_AreaToRoam, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, AREATOROAM, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_condition, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, CONDITION, INT_MIN, INT_MAX)
GETSET_RANGE(m_desiredGoal, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, DESIREDGOAL, INT_MIN, INT_MAX)
GETSET_RANGE(m_lastKnownEnergy, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, LASTKNOWNENERGY, INT_MIN, INT_MAX)
GETSET_RANGE(m_currentMissileSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, CURRENTMISSILESPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_normalSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, NORMALSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_hussleSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, HUSSLESPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_turnSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, TURNSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_routePointCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ROUTEPOINTCOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_curRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, CURRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_mapCurLocation, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, MAPCURLOCATION, INT_MIN, INT_MAX)
GETSET_RANGE(m_curMap, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, CURMAP, INT_MIN, INT_MAX)
GETSET_MAX(m_aiCfgName, gsCString, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, AICFGNAME, STRLEN_MAX)
GETSET_RANGE(m_spawnScript, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, SPAWNSCRIPT, INT_MIN, INT_MAX)
GETSET_RANGE(m_floorTolerance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FLOORTOLERANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_wallMinimumHeight, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, WALLMINIMUMHEIGHT, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_feelerRange, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FEELERRANGE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_feelerDepth, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FEELERDEPTH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_feelerCycleTime, gsDouble, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FEELERCYCLETIME, DOUBLE_MIN, DOUBLE_MAX)
GETSET_RANGE(m_feelerWidth, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FEELERWIDTH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_feelerHeight, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FEELERHEIGHT, FLOAT_MIN, FLOAT_MAX)
GETSET(m_feelerEnable, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FEELERENABLE)
GETSET(m_useOverridePhysics, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, USEOVERRIDEPHYSICS)
GETSET_RANGE(m_safeDistance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, SAFEDISTANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_minAttackDistance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, MINATTACKDISTANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_percentageAttack, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, PERCENTAGEATTACK, INT_MIN, INT_MAX)
GETSET_RANGE(m_accuracy, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ACCURACY, FLOAT_MIN, FLOAT_MAX)
GETSET(m_exponentTurning, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, EXPONENTTURNING)
GETSET_RANGE(m_travelingDesiredDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, TRAVELINGDESIREDDIST, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_followMaxDistance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FOLLOWMAXDISTANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_followMinDistance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, FOLLOWMINDISTANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_distanceQuerryDuration, gsDouble, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, DISTANCEQUERRYDURATION, DOUBLE_MIN, DOUBLE_MAX)
GETSET_RANGE(m_attackMaxDistance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKMAXDISTANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_attackMinDistance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKMINDISTANCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_eyeLevel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, EYELEVEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_sightCheckDuration, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, SIGHTCHECKDURATION, LONG_MIN, LONG_MAX)
GETSET(m_targetInSight, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, TARGETINSIGHT)
GETSET(m_enableVision, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ENABLEVISION)
GETSET_RANGE(m_durationTillTargetLost, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, DURATIONTILLTARGETLOST, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_attackDurationMax, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKDURATIONMAX, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_attackDurationMin, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKDURATIONMIN, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_attackTimeBetweenMax, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKTIMEBETWEENMAX, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_attackTimeBetweenMin, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKTIMEBETWEENMIN, LONG_MIN, LONG_MAX)
GETSET(m_attackingCurrently, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKINGCURRENTLY)
GETSET_RANGE(m_attackDurationCurrent, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKDURATIONCURRENT, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_attackTimeBetweenCurrent, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ATTACKTIMEBETWEENCURRENT, LONG_MIN, LONG_MAX)
GETSET(m_enableJumpAbility, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ENABLEJUMPABILITY)
GETSET_RANGE(m_accuracyErrorMax, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ACCURACYERRORMAX, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_accuracyErrorMin, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ACCURACYERRORMIN, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_prefernceAboveTarget, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, PREFERNCEABOVETARGET, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_agressive, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, AGRESSIVE, INT_MIN, INT_MAX)
GETSET(m_enableCashApplied, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ENABLECASHAPPLIED)
GETSET_RANGE(m_maxCash, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, MAXCASH, INT_MIN, INT_MAX)
GETSET_RANGE(m_minCash, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, MINCASH, INT_MIN, INT_MAX)
GETSET(m_radiusSysEnabled, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RADIUSSYSENABLED)
GETSET(m_radiusSysAttackOnExtents, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RADIUSSYSATTACKONEXTENTS)
GETSET(m_radiusSysIgnoreY, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RADIUSSYSIGNOREY)
GETSET_RANGE(m_radiusSysRange, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RADIUSSYSRANGE, FLOAT_MIN, FLOAT_MAX)
GETSET_OBJ_PTR(m_physicsModuleOverride, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, PHYSICSMODULEOVERRIDE, CMovementCaps)
GETSET_MAX(m_nameOvveride, gsCString, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, NAMEOVVERIDE, STRLEN_MAX)
GETSET_RANGE(m_baseMeshes, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, SPAWNCFGCOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_entityCfgDBindexRef, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, ENTITYCFGDBINDEXREF, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_inventoryCfg, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, INVENTORYCFG)
GETSET_RANGE(m_percentageOfDropping, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, PERCENTAGEOFDROPPING, INT_MIN, INT_MAX)
GETSET_RANGE(m_curExperienceLevel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, CUREXPERIENCELEVEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_overriderMaxEnergy, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, OVERRIDERMAXENGERY, INT_MIN, INT_MAX)
GETSET_RANGE(m_collisionDBOverride, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, COLLISIONDBOVERRIDE, INT_MIN, INT_MAX)
GETSET_RANGE(m_pMeshIds, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, SPAWNCFG, INT_MIN, INT_MAX)
GETSET(m_currentSpeedOverride, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, CURRENTSPEEDOVERRIDE)
GETSET(m_needToDefend, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, NEEDTODEFEND)
GETSET(m_retreating, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RETREATING)
GETSET(m_retreatStamp, gsDouble, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RETREATTIMESTAMP)
GETSET(m_radiusSysInUse, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIOBJ, RADIUSSYSINUSE)
END_GETSET_IMPL

} // namespace KEP