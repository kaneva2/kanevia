///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <afxtempl.h>
#include "CTextClass.h"

namespace KEP {

//------------text entry------------------//
CTextEntry::CTextEntry(CStringA L_text,
	int L_percX,
	int L_percY) {
	m_text = L_text;
	m_percX = L_percX;
	m_percY = L_percY;
}

void CTextEntry::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_text
		   << m_percX
		   << m_percY;

	} else {
		ar >> m_text >> m_percX >> m_percY;
	}
}

IMPLEMENT_SERIAL(CTextEntry, CObject, 0)
IMPLEMENT_SERIAL(CTextEntryList, CObList, 0)

//text collection
CTextCollection::CTextCollection(CStringA L_collectionName,
	CTextEntryList* L_textEntryList,
	int L_timeVarial,
	int L_timeLength,
	int L_fontSel,
	BOOL L_displayIsOn,
	COLORREF L_textColor) {
	m_textColor = L_textColor;
	m_displayIsOn = L_displayIsOn;
	m_collectionName = L_collectionName;
	m_textEntryList = L_textEntryList;
	m_timeVarial = L_timeVarial;
	m_timeLength = L_timeLength;
	m_fontSel = L_fontSel;
}

void CTextCollection::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_collectionName
		   << m_textEntryList
		   << m_timeVarial
		   << m_timeLength
		   << m_fontSel
		   << m_textColor;

	} else {
		ar >> m_collectionName >> m_textEntryList >> m_timeVarial >> m_timeLength >> m_fontSel >> m_textColor;

		m_displayIsOn = FALSE;
	}
}

IMPLEMENT_SERIAL(CTextCollection, CObject, 0)
IMPLEMENT_SERIAL(CTextCollectionList, CKEPObList, 0)
//font list
CFontObj::CFontObj(HFONT L_fontObject,
	CStringA L_fontName,
	int L_sizeX,
	int L_sizeY) {
	m_sizeX = L_sizeX;
	m_sizeY = L_sizeY;
	m_fontObject = L_fontObject;
	m_fontName = L_fontName;
}

void CFontObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_fontName
		   << m_sizeX
		   << m_sizeY;
	} else {
		ar >> m_fontName >> m_sizeX >> m_sizeY;
	}
}

IMPLEMENT_SERIAL(CFontObj, CObject, 0)
IMPLEMENT_SERIAL(CFontObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CTextEntry, IDS_TEXTENTRY_NAME)
GETSET_MAX(m_text, gsCString, GS_FLAGS_DEFAULT, 0, 0, TEXTENTRY, TEXT, STRLEN_MAX)
GETSET_RANGE(m_percX, gsInt, GS_FLAGS_DEFAULT, 0, 0, TEXTENTRY, PERCX, INT_MIN, INT_MAX)
GETSET_RANGE(m_percY, gsInt, GS_FLAGS_DEFAULT, 0, 0, TEXTENTRY, PERCY, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP