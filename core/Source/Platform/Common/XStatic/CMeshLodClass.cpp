///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CStaticMeshObject.h"
#include "CMaterialClass.h"
#include "CMeshLodClass.h"
#include "IAssetsDatabase.h"
#include "IClientEngine.h"

#ifdef _ClientOutput
#include "ReD3DX9.h"
#include "ReMeshCreate.h"
#include "ReMeshStream.h"
#endif

#define DEFAULT_LOD_ACTIVATION_DISTANCE_INC 50

namespace KEP {

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CMeshLODObject::CMeshLODObject() {
	init();
}

CMeshLODObject::CMeshLODObject(CEXMeshObj* apExMesh, const Vector3f& aPivot,
	const CMaterialObject* apMaterial,
	const Matrix44f& aMatrix, const ABBOX& aBoundingBox,
	float aActivationDist, const ABBOX* apActivationBox,
	BOOL aBakeVertices /*= TRUE*/,
	BOOL aOptimizeMesh /*= TRUE*/) {
	init();
	SetBoundingBox(aBoundingBox);
	SetMesh(apExMesh, aPivot, aBakeVertices, aOptimizeMesh);
	SetMaterial(apMaterial);
	SetMatrix(aMatrix);
	SetActivationDistance(aActivationDist);
	if (apActivationBox != NULL)
		SetActivationBox(*apActivationBox);
	else
		ClearActivationBox();
}

void CMeshLODObject::init() {
	m_distance = 0.0f; //greater than activate it
	ClearActivationBox();
	x_backward_compat_meshLodLevel = NULL; // To be removed once all clients stop using x_backward_compat_meshLodLevel
	m_ReMesh = NULL;
	m_material = NULL;
	ClearBoundingBox();
}

void CMeshLODObject::Serialize(CArchive& ar) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(CArchiveObjectSchemaVersion<CMeshLODObject>);

		ar << m_distance
		   << m_boxOption.maxX
		   << m_boxOption.maxY
		   << m_boxOption.maxZ
		   << m_boxOption.minX
		   << m_boxOption.minY
		   << m_boxOption.minZ;

		ar.Write((const void*)&m_matrix, sizeof(Matrix44f));
		ar.Write((const void*)&m_boundBox, sizeof(ABBOX));

		ar << m_material;

		// get a ReMeshStream object
		ReMeshStream streamer;
		// stream out the ReMesh object
		streamer.Write(m_ReMesh, ar);
	} else {
		int version = ar.GetObjectSchema();

		ar >> m_distance >> m_boxOption.maxX >> m_boxOption.maxY >> m_boxOption.maxZ >> m_boxOption.minX >> m_boxOption.minY >> m_boxOption.minZ;

		if (version < 2) {
			CEXMeshObj* pTmpLodMesh = NULL;
			ar >> pTmpLodMesh;
			pTmpLodMesh->m_materialObject->Clone(&m_material);
			m_matrix = pTmpLodMesh->GetWorldTransform();
			m_boundBox = pTmpLodMesh->GetBoundingBox();
			x_backward_compat_meshLodLevel = pTmpLodMesh; // To be removed once all clients stop using x_backward_compat_meshLodLevel
		} else {
			ar.Read((void*)&m_matrix, sizeof(Matrix44f));
			ar.Read((void*)&m_boundBox, sizeof(ABBOX));
			ar >> m_material;
			MeshRM::Instance()->CreateReMaterial(m_material);
			x_backward_compat_meshLodLevel = NULL; // To be removed once all clients stop using x_backward_compat_meshLodLevel
		}

		// init the ReMesh object
		SetReMesh(NULL);

		if (version >= 1) { // VERSION 1: m_ReMesh
			// the new ReSkinMesh
			ReMesh* newMesh;
			// get an ReMeshStream object
			ReMeshStream streamer;

			streamer.Read(&newMesh, ar, pICE->GetReDeviceState());
			ReMesh* addedMesh = MeshRM::Instance()->AddMesh(newMesh, MeshRM::RMP_ID_LEVEL);
			ReMesh* mesh = NULL;

			newMesh->Clone(&mesh, true);
			ASSERT(mesh);

			if (mesh) {
				SetReMesh(mesh);
			}

			if (addedMesh) {
				// plug mem leak...
				//delete addedMesh; // can't free here crashes
			}

			// pool the ReMaterials
			MeshRM::Instance()->CreateReMaterial(m_material, Utf16ToUtf8(ar.m_strFileName).c_str());
		}
	}
#else
	ASSERT(0);
#endif
}

void CMeshLODObject::SafeDelete() {
	if (m_material) {
		delete m_material;
		m_material = NULL;
	}
}

CMeshLODObject*
CMeshLODObjectList::GetLodVisual(const Vector3f& viewPoint,
	const Vector3f& objPoint,
	float currentFov,
	ReMesh** reMesh) {
	//distance based
	if (m_lodType == 0) {
		float distance = Distance(viewPoint, objPoint);
		float adViewVar = currentFov / .68f; //
		distance = distance * adViewVar;

		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
			if (distance > skPtr->m_distance) {
				refMesh = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
				*reMesh = skPtr->GetReMesh();
			}
		}
		if (refMesh)
			return refMesh;
	}

	//box based
	if (m_lodType == 1) {
		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
			if (BoundBoxPointCheck(viewPoint, skPtr->m_boxOption) == FALSE) {
				refMesh = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
				*reMesh = skPtr->GetReMesh();
			}
		}
		if (refMesh)
			return refMesh;
	}

	return NULL;
}

CMeshLODObject*
CMeshLODObjectList::GetLodVisualExt(const Vector3f& viewPoint,
	const Vector3f& objPoint,
	float currentFov,
	CMeshLODObject** refObj2,
	float* obj1AlpaOpt,
	float* obj2AlpaOpt,
	ReMesh** reMesh) { // RENDERENGINE INTERFACE
	//m_lodType = 2;

	if (m_lodType == 0) {
		//distance based
		float distance = Distance(viewPoint, objPoint);
		float adViewVar = currentFov / .68f; //
		distance = distance * adViewVar;

		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
			if (distance > skPtr->m_distance) {
				refMesh = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
				*reMesh = skPtr->GetReMesh(); // RENDERENGINE INTERFACE
				//break;
			}
		}
		if (refMesh)
			return refMesh;
	} //end distance based

	if (m_lodType == 1) {
		//box based
		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);

			if (BoundBoxPointCheck(viewPoint, skPtr->m_boxOption) == FALSE) {
				refMesh = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
				*reMesh = skPtr->GetReMesh(); // RENDERENGINE INTERFACE
				//break;
			}
		}
		if (refMesh)
			return refMesh;
	} //end distance based

	if (m_lodType == 2) {
		//ranged adv fade based
		float distance = Distance(viewPoint, objPoint);
		//m_optionalOverlapDist
		CMeshLODObject* lastMesh = NULL;
		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			//account list
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
			if (distance > (skPtr->m_distance - m_optionalOverlapDist)) {
				if (distance < (skPtr->m_distance + m_optionalOverlapDist)) {
					//within transition
					*obj2AlpaOpt = 1.0f * (((skPtr->m_distance + m_optionalOverlapDist) - distance) / (m_optionalOverlapDist * 2.0f));
					*obj1AlpaOpt = 1.0f - *obj2AlpaOpt;

					*refObj2 = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
					*reMesh = skPtr->GetReMesh(); // RENDERENGINE INTERFACE

					return lastMesh;
				} //end within transition
				else {
					//clean change
					*obj1AlpaOpt = 1.0f;
					*obj2AlpaOpt = 1.0f;
					refMesh = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
					*reMesh = skPtr->GetReMesh(); // RENDERENGINE INTERFACE
				} //end clean change
			}
			lastMesh = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
		}
		if (refMesh)
			return refMesh;
	} //end ranged adv fade based

	return NULL;
}

CMeshLODObject*
CMeshLODObjectList::GetLodVisual(const Vector3f& viewPoint,
	const Vector3f& objPoint,
	float currentFov,
	UINT& index) {
	INT lodCount = -1;
	CMeshLODObject* refMesh = NULL;

	//distance based
	if (m_lodType == 0) {
		float distance = Distance(viewPoint, objPoint);
		float adViewVar = currentFov / .68f; //
		distance = distance * adViewVar;

		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
			if (distance > skPtr->m_distance) {
				if (skPtr->GetReMesh()) {
					refMesh = skPtr;
					index = ++lodCount;
				}
			}
		}
	}
	// boundbox based
	else if (m_lodType == 1) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);

			if (BoundBoxPointCheck(viewPoint, skPtr->m_boxOption) == FALSE) {
				if (skPtr->GetReMesh()) {
					refMesh = skPtr;
					index = ++lodCount;
				}
			}
		}
	}

	return refMesh;
}

CMeshLODObject*
CMeshLODObjectList::GetLodVisualExt(const Vector3f& viewPoint,
	const Vector3f& objPoint,
	float currentFov,
	CMeshLODObject** refObj2,
	float* obj1AlpaOpt,
	float* obj2AlpaOpt,
	UINT& index) {
	if (m_lodType == 0) {
		float distance = Distance(viewPoint, objPoint);
		float adViewVar = currentFov / .68f; //
		distance = distance * adViewVar;

		INT lodCount = -1;
		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
			if (distance > skPtr->m_distance) {
				refMesh = skPtr;
				index = ++lodCount;
			}
		}
		if (refMesh)
			return refMesh;
	} //end distance based

	if (m_lodType == 1) {
		//box based
		INT lodCount = -1;
		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);

			if (BoundBoxPointCheck(viewPoint, skPtr->m_boxOption) == FALSE) {
				refMesh = skPtr;
				index = ++lodCount;
			}
		}
		if (refMesh)
			return refMesh;
	} //end distance based

	if (m_lodType == 2) {
		//ranged adv fade based
		float distance = Distance(viewPoint, objPoint);
		//m_optionalOverlapDist
		INT lodCount = -1;
		CMeshLODObject* lastMesh = NULL;
		CMeshLODObject* refMesh = NULL;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			//account list
			CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
			if (distance > (skPtr->m_distance - m_optionalOverlapDist)) {
				if (distance < (skPtr->m_distance + m_optionalOverlapDist)) {
					//within transition
					*obj2AlpaOpt = 1.0f * (((skPtr->m_distance + m_optionalOverlapDist) - distance) / (m_optionalOverlapDist * 2.0f));
					*obj1AlpaOpt = 1.0f - *obj2AlpaOpt;

					*refObj2 = skPtr;
					index = ++lodCount;

					return lastMesh;
				} //end within transition
				else {
					//clean change
					*obj1AlpaOpt = 1.0f;
					*obj2AlpaOpt = 1.0f;
					refMesh = skPtr;
					index = ++lodCount;
				} //end clean change
			}
			lastMesh = skPtr; //->m_meshLodLevel;	// Use CMeshLODObject now as we no longer provide CEXMeshObj (m_meshLodLevel) member
		}
		if (refMesh)
			return refMesh;
	} //end ranged adv fade based

	return NULL;
}
/*
BOOL CMeshLODObjectList::AddLOD(int type,
								const ABBOX * box,
								float distance,
								CEXMeshObj* tmpMeshObj,
								CMaterialObject * materialObject,
								BOOL referenceObject)
{
#ifdef _ClientOutput
	//if( 0 == IAssetsDatabase::Instance() )
	//	return FALSE;

	//CEXMeshObj* tmpMeshObj = IAssetsDatabase::Instance()->ImportStaticMesh(filename);
	if( 0 == tmpMeshObj )
		return FALSE;

	CMeshLODObject * meshObj;

	if(m_lodType == 0)
	{//distance based
		meshObj = new CMeshLODObject();
		meshObj->m_distance = distance;
	}
	else if(m_lodType == 1 && box != NULL)
	{//distance based
		meshObj = new CMeshLODObject();
		meshObj->m_boxOption.maxX = box->maxX;
		meshObj->m_boxOption.maxY = box->maxY;
		meshObj->m_boxOption.maxZ = box->maxZ;
		meshObj->m_boxOption.minX = box->minX;
		meshObj->m_boxOption.minY = box->minY;
		meshObj->m_boxOption.minZ = box->minZ;
	}
	else
	{
		tmpMeshObj->SafeDelete();
		delete tmpMeshObj;
		return FALSE;
	}

	materialObject->Clone(&tmpMeshObj->m_materialObject);

	// build the ReMesh
	tmpMeshObj->OptimizeMesh();

	// init new members
	tmpMeshObj->m_materialObject->Clone(&meshObj->m_material);
	meshObj->m_matrix	=	tmpMeshObj->m_currentMatrix;
	meshObj->m_boundBox	=	tmpMeshObj->m_boundingBox;

	// create a new ReMesh the legacy way....
	if (referenceObject)
	{
		meshObj->SetReMesh(ReMeshCreate::Instance()->CreateReStaticMeshLibrary(tmpMeshObj));
	}
	else
	{
		meshObj->SetReMesh(ReMeshCreate::Instance()->CreateReStaticMeshLevel(tmpMeshObj,NULL));
	}

	// CEXMeshObj no longer used
	delete tmpMeshObj;

	AddTail(meshObj);
	return TRUE;
#else
	ASSERT(0);
	return FALSE;
#endif
}
*/

//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//-----Get File Ext--------------------------------------------//
CStringA CMeshLODObjectList::GetFileExt(CStringA currentName) {
	CStringA ext;
	int length = currentName.GetLength();
	BOOL done = FALSE;
	while (done == FALSE) {
		CStringA chr(currentName.GetAt(length - 1));

		if (chr == ".")
			break;

		ext = operator+(chr, ext);
		length = length - 1;
	}
	return ext;
}

BOOL CMeshLODObjectList::BoundBoxPointCheck(Vector3f point, ABBOX box) {
	if (point.x >= box.minX &&
		point.x <= box.maxX &&
		point.y >= box.minY &&
		point.y <= box.maxY &&
		point.z >= box.minZ &&
		point.z <= box.maxZ)
		return TRUE;
	return FALSE;
}

void CMeshLODObject::Clone(CMeshLODObject** copy, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CMeshLODObject* copyLocal = new CMeshLODObject();
	copyLocal->m_distance = m_distance;
	copyLocal->m_boxOption.maxX = m_boxOption.maxX;
	copyLocal->m_boxOption.maxY = m_boxOption.maxY;
	copyLocal->m_boxOption.maxZ = m_boxOption.maxZ;
	copyLocal->m_boxOption.minX = m_boxOption.minX;
	copyLocal->m_boxOption.minY = m_boxOption.minY;
	copyLocal->m_boxOption.minZ = m_boxOption.minZ;

	copyLocal->m_matrix = m_matrix;
	copyLocal->m_boundBox = m_boundBox;

	m_material->Clone(&copyLocal->m_material);

	copyLocal->m_ReMesh = m_ReMesh;

	*copy = copyLocal;
}

CMeshLODObjectList::CMeshLODObjectList() {
	m_lodType = 0; //default distance type
	m_optionalOverlapDist = 10.0f; //test var
}

void CMeshLODObjectList::Clone(CMeshLODObjectList** copy, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CMeshLODObjectList* copyLocal = new CMeshLODObjectList();
	copyLocal->m_lodType = m_lodType;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
		CMeshLODObject* newPtr = NULL;
		skPtr->Clone(&newPtr, g_pd3dDevice);
		copyLocal->AddTail(newPtr);
	}
	*copy = copyLocal;
}
/*
void CMeshLODObjectList::RemoveLod(int index)
{
   int indexTrace = 0;
   POSITION posLast;
	for(POSITION posLoc = GetHeadPosition();(posLast = posLoc) !=NULL;){//account list
        CMeshLODObject * skPtr = (CMeshLODObject *)GetNext(posLoc);
		if(index == indexTrace){
			skPtr->SafeDelete();
			delete skPtr;
			skPtr=0;
            RemoveAt(posLast);
           return;
		}
		indexTrace++;
	}
}
*/

void CMeshLODObjectList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CMeshLODObject* skPtr = (CMeshLODObject*)GetNext(posLoc);
		skPtr->SafeDelete();
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}

void CMeshLODObjectList::Serialize(CArchive& ar) {
	CObList::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(CArchiveObjectSchemaVersion<CMeshLODObjectList>);
		ar << m_lodType;
	} else {
		int version;
		version = ar.GetObjectSchema(); //VERSION 1: m_ReMesh
		ar >> m_lodType;
		m_optionalOverlapDist = 10.0f; //test var
	}
}

IMPLEMENT_SERIAL_SCHEMA(CMeshLODObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CMeshLODObjectList, CObList)

CMeshLODObject* CMeshLODObjectList::GetLOD(int lodLevel) {
#ifdef _ClientOutput
	ASSERT(lodLevel > 0); // LOD0 held in parent class
	if (lodLevel == 0)
		return NULL;

	POSITION lodPos = FindIndex(lodLevel - 1); // lodIndex = lodLevel-1
	if (lodPos == NULL)
		return NULL;
	return dynamic_cast<CMeshLODObject*>(GetAt(lodPos));
#else
	ASSERT(0);
	return NULL;
#endif
}

CMeshLODObject* CMeshLODObjectList::GetLastLOD() {
#ifdef _ClientOutput
	return dynamic_cast<CMeshLODObject*>(GetTail());
#else
	ASSERT(0);
	return NULL;
#endif
}

BOOL CMeshLODObjectList::ExtendLODChain(int lodLevel, float distanceIncrement, ReMesh* pBaseMesh, const CMaterialObject* pBaseMaterial, const Matrix44f& baseMatrix, const ABBOX& baseBoundingBox) {
#ifdef _ClientOutput
	if (GetCount() >= lodLevel)
		return TRUE; // Skip if already has it

	CMeshLODObject* lastLOD;
	if (GetCount() == 0) {
		// Insert LOD1 as a clone of LOD0 with activation distance increased
		ABBOX defaultBox;
		defaultBox.minX = -distanceIncrement;
		defaultBox.minY = -distanceIncrement;
		defaultBox.minZ = -distanceIncrement;
		defaultBox.maxX = distanceIncrement;
		defaultBox.maxY = distanceIncrement;
		defaultBox.maxZ = distanceIncrement;

		lastLOD = new CMeshLODObject;
		lastLOD->SetReMesh(pBaseMesh);
		lastLOD->SetMaterial(pBaseMaterial);
		if (m_lodType == 0) // Set activation parameters
			lastLOD->SetActivationDistance(distanceIncrement);
		else
			lastLOD->SetActivationBox(defaultBox);

		// Override matrix & bounding box
		lastLOD->SetMatrix(baseMatrix);
		lastLOD->SetBoundingBox(baseBoundingBox);

		AddTail(lastLOD);
	} else {
		// Get current maximal LOD level
		lastLOD = GetLOD(GetCount());
	}

	ABBOX activationBox = lastLOD->m_boxOption;
	float activationDistance = lastLOD->m_distance;
	int tmpLodNo = GetCount() + 1;
	for (; tmpLodNo <= lodLevel; tmpLodNo++) {
		// Extend activation distance
		if (m_lodType == 1) {
			activationBox.minX -= distanceIncrement;
			activationBox.minY -= distanceIncrement;
			activationBox.minZ -= distanceIncrement;
			activationBox.maxX += distanceIncrement;
			activationBox.maxY += distanceIncrement;
			activationBox.maxZ += distanceIncrement;
		} else if (m_lodType == 0) {
			activationDistance += distanceIncrement;
		}

		// Copy from last LOD
		CMeshLODObject* newLOD = new CMeshLODObject;
		newLOD->SetReMesh(lastLOD->GetReMesh());
		newLOD->SetMaterial(lastLOD->GetMaterial());
		if (m_lodType == 0) // Set activation parameters
			newLOD->SetActivationDistance(activationDistance);
		else
			newLOD->SetActivationBox(activationBox);
		newLOD->SetMatrix(lastLOD->GetMatrix());
		newLOD->SetBoundingBox(lastLOD->GetBoundingBox());

		AddTail(newLOD);
	}

	return TRUE;
#else
	ASSERT(0);
	return FALSE;
#endif
}

void CMeshLODObject::SetMesh(CEXMeshObj* apNewMesh, const Vector3f& aPivot, BOOL aBakeVertices /*= TRUE*/, BOOL aOptimizeMesh /*= TRUE*/) {
#ifdef _ClientOutput
	// Dispose old data
	//@@Note: m_ReMesh might still be used by other reference objects and should not be disposed here.

	if (aBakeVertices)
		apNewMesh->BakeVertices();

	// build the ReMesh
	if (aOptimizeMesh)
		apNewMesh->OptimizeMesh();

	// Compute bounding box
	apNewMesh->ComputeBoundingVolumes();

	// create a new ReMesh the legacy way....
	ReMesh* pReMesh;
	pReMesh = MeshRM::Instance()->CreateReStaticMesh(apNewMesh, aPivot); //Pivot point provided by caller - use origin if RLB

	// Set ReMesh
	SetReMesh(pReMesh);

#else
	ASSERT(0);
#endif
}

void CMeshLODObject::SetMaterial(const CMaterialObject* newMaterial) {
#ifdef _ClientOutput
	// Dispose existing material if available
	if (m_material != NULL)
		delete m_material;
	m_material = NULL;

	// Clone from incoming material
	if (newMaterial != NULL)
		newMaterial->Clone(&m_material);
#else
	ASSERT(0);
#endif
}

int CMeshLODObjectList::InsertLOD(int desiredLodLevel, CMeshLODObject* lodObj) {
#ifdef _ClientOutput
	ASSERT(lodObj != NULL);
	ASSERT(desiredLodLevel != 0); // LOD0 held in parent class
	if (desiredLodLevel == 0)
		return -1;

	if (desiredLodLevel == -1) // -1: Append a new LOD
		desiredLodLevel = GetCount() + 1;

	int lodIndex = desiredLodLevel - 1;
	if (lodIndex >= GetCount()) {
		// Append
		AddTail(lodObj);
		return GetCount(); // GetCount() - 1 + 1
	}

	POSITION lodPos = FindIndex(desiredLodLevel - 1); // lodIndex = lodLevel-1
	ASSERT(lodPos != NULL);

	InsertBefore(lodPos, lodObj);
	return desiredLodLevel;
#else
	ASSERT(0);
	return -1;
#endif
}

void CMeshLODObjectList::DeleteLOD(int lodLevel) {
#ifdef _ClientOutput
	ASSERT(lodLevel > 0); // LOD0 held in parent class
	if (lodLevel == 0)
		return;

	POSITION lodPos = FindIndex(lodLevel - 1); // lodIndex = lodLevel-1
	if (lodPos == NULL)
		return;
	CMeshLODObject* lodObj = dynamic_cast<CMeshLODObject*>(GetAt(lodPos));

	// Remove from list
	RemoveAt(lodPos);

	// Dispose memory
	lodObj->SafeDelete();
	delete lodObj;
#else
	ASSERT(0);
#endif
}

void CMeshLODObjectList::SetLODMesh(int lodLevel, CEXMeshObj* newMesh, BOOL bExtendLODChain, CStaticMeshObj* pParent /*= NULL*/, BOOL aBakeVertices /*= TRUE*/, BOOL aOptimizeMesh /*= TRUE*/) {
#ifdef _ClientOutput
	CMeshLODObject* lodObj = GetLOD(lodLevel);
	if (lodObj == NULL) {
		if (!bExtendLODChain) {
			ASSERT(FALSE);
			return;
		}

		// Specified LOD number not found in LOD chain. Extend LOD chain to include it.
		ExtendLODChain(lodLevel, DEFAULT_LOD_ACTIVATION_DISTANCE_INC, pParent->GetReMesh(), pParent->GetMaterial(), pParent->GetMatrix(), pParent->GetBoundingBox());
		lodObj = GetLOD(lodLevel);
		ASSERT(lodObj != NULL);
	}

	if (lodObj != NULL)
		lodObj->SetMesh(newMesh, pParent->GetPivot(), aBakeVertices, aOptimizeMesh);

#else
	ASSERT(0);
#endif
}

void CMeshLODObjectList::UpdateLODSettings(int lodLevel, float activationDist, ABBOX* activationBox) {
#ifdef _ClientOutput
	CMeshLODObject* lodObj = GetLOD(lodLevel);
	ASSERT(lodObj != NULL);
	if (lodObj == NULL)
		return;

	if (activationDist != -1.0f)
		lodObj->SetActivationDistance(activationDist);
	if (activationBox)
		lodObj->SetActivationBox(*activationBox);
#else
	ASSERT(0);
#endif
}

void CMeshLODObjectList::SetMatrixForAllLODs(const Matrix44f& matrix) {
#ifdef _ClientOutput
	for (POSITION pos = GetHeadPosition(); pos != NULL;) {
		CMeshLODObject* pLOD = dynamic_cast<CMeshLODObject*>(GetNext(pos));
		if (pLOD)
			pLOD->SetMatrix(matrix);
	}
#else
	ASSERT(0);
#endif
}

void CMeshLODObjectList::SetBoundingBoxForAllLODs(const ABBOX& bbox) {
#ifdef _ClientOutput
	for (POSITION pos = GetHeadPosition(); pos != NULL;) {
		CMeshLODObject* pLOD = dynamic_cast<CMeshLODObject*>(GetNext(pos));
		if (pLOD)
			pLOD->SetBoundingBox(bbox);
	}
#else
	ASSERT(0);
#endif
}

BEGIN_GETSET_IMPL(CMeshLODObject, IDS_MESHLODOBJECT_NAME)
GETSET_RANGE(m_distance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MESHLODOBJECT, DISTANCE, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP