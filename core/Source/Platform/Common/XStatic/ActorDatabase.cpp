///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "ActorDatabase.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

UGCActor::UGCActor() :
		m_actorGlid(GLID_INVALID),
		m_DOGlid(GLID_INVALID),
		m_walkGlid(GLID_INVALID),
		m_runGlid(GLID_INVALID),
		m_standGlid(GLID_INVALID),
		m_jumpGlid(GLID_INVALID) {
}

UGCActor::~UGCActor() {
}

void UGCActor::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

ActorDatabase::ActorDatabase(CMovementObjList* actors) {
	m_actors = actors;
}

ActorDatabase::~ActorDatabase() {
	// If this is destroyed before the Runtime entity list very bad things will happpen
	size_t actors = m_ugcActors.size();
	for (size_t i = 0; i < actors; i++) {
		delete m_ugcActors[i];
		m_ugcActors[i] = NULL;
	}
	m_ugcActors.clear();
}

UGCActor* ActorDatabase::getActorData(const GLID& glid) {
	size_t actors = m_ugcActors.size();
	for (size_t i = 0; i < actors; i++) {
		if (m_ugcActors[i]->m_actorGlid == glid)
			return m_ugcActors[i];
	}
	return NULL;
}

void ActorDatabase::CreateActorBasedOnDO(const GLID& actorGlid, const GLID& DOGlid) {
	if (getActorData(actorGlid) != NULL) {
		return; // this UGC avatar already exists
	}
	UGCActor* newActor = new UGCActor();
	newActor->m_actorGlid = actorGlid;
	newActor->m_DOGlid = DOGlid;
	m_ugcActors.push_back(newActor);
}

void ActorDatabase::AddSupportedAnimation(const GLID& actorGlid, const GLID& animGlid, const std::string& animName) {
	for (unsigned int i = 0; i < m_ugcActors.size(); i++) {
		if (m_ugcActors[i]->m_actorGlid == actorGlid) {
			if (animName.find("idle") != std::string::npos || animName.find("stand") != std::string::npos) {
				m_ugcActors[i]->m_standGlid = animGlid;
			} else if (animName.find("run") != std::string::npos) {
				m_ugcActors[i]->m_runGlid = animGlid;
			} else if (animName.find("walk") != std::string::npos) {
				m_ugcActors[i]->m_walkGlid = animGlid;
			} else if (animName.find("jump") != std::string::npos) {
				m_ugcActors[i]->m_jumpGlid = animGlid;
			}
		}
	}
}

} // namespace KEP
