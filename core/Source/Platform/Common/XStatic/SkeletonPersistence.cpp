///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CBoneClass.h"
#include "CFrameObj.h"
#include "matrixarb.h"

namespace KEP {

#define CUR_SKELETON_FILE_VERSION 1

bool RuntimeSkeleton::write(RuntimeSkeleton* toSave, CArchive& ar) {
	toSave->m_apSkeletonAnimHeaders.clear(); // For now we do not want any animation headers in our archives.
	ar << RuntimeSkeleton::skeleton_archive_code;
	ar << CUR_SKELETON_FILE_VERSION;

	return writeV1(toSave, ar);
}

std::shared_ptr<RuntimeSkeleton> RuntimeSkeleton::read(CArchive& ar) {
	unsigned char code;
	int version;
	ar >> code;
	ar >> version;
	if (code != RuntimeSkeleton::skeleton_archive_code) {
		AfxThrowArchiveException(CArchiveException::badClass, L"skeleton");
	}
	switch (version) {
		case 1:
			return readV1(ar);
		default:
			AfxThrowArchiveException(CArchiveException::badClass, L"unknown format");
			break;
	}
}

bool RuntimeSkeleton::writeV1(RuntimeSkeleton* toSave, CArchive& ar) {
	unsigned int boneCount = toSave->m_pBoneList->GetCount();
	ar << CStringA(toSave->m_skeletonName.c_str());
	ar << toSave->m_scaleVector.x << toSave->m_scaleVector.y << toSave->m_scaleVector.z;
	ar << boneCount;
	for (unsigned int i = 0; i < boneCount; i++) {
		POSITION IHateCOBListPos = toSave->m_pBoneList->FindIndex(i);
		CBoneObject* bone = (CBoneObject*)toSave->m_pBoneList->GetAt(IHateCOBListPos);
		CStringA boneNameStr = bone->m_boneName.c_str();
		CStringA parentNameStr = bone->m_parentName.c_str();
		ar << boneNameStr;
		ar << parentNameStr;
		ar << bone->m_secondaryOverridable;
		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 4; k++) {
				ar << bone->m_startPosition[j][k]; // bind pose matrix
			}
		}
	}
	return true;
}

std::shared_ptr<RuntimeSkeleton> RuntimeSkeleton::readV1(CArchive& ar) {
	unsigned int boneCount = 0;
	std::shared_ptr<RuntimeSkeleton> pRS_new = RuntimeSkeleton::New();
	pRS_new->m_pBoneList = new CBoneList();

	CStringA skeletonName;
	ar >> skeletonName;
	pRS_new->m_skeletonName = skeletonName.GetString();

	ar >> pRS_new->m_scaleVector.x >> pRS_new->m_scaleVector.y >> pRS_new->m_scaleVector.z;
	ar >> boneCount;
	for (unsigned int i = 0; i < boneCount; i++) {
		CBoneObject* pBO_new = new CBoneObject();
		CStringA boneNameStr;
		CStringA parentNameStr;
		ar >> boneNameStr;
		ar >> parentNameStr;
		ar >> pBO_new->m_secondaryOverridable;
		pBO_new->m_boneName = (const char*)boneNameStr;
		pBO_new->m_parentName = (const char*)parentNameStr;
		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 4; k++) {
				ar >> pBO_new->m_startPosition[j][k]; // bind pose matrix
			}
		}
		pBO_new->m_startPositionInv.MakeInverseOf(pBO_new->m_startPosition);
		MatrixARB::Transpose(pBO_new->m_startPositionTranspose, pBO_new->m_startPosition);

		pBO_new->m_boneFrame = new CFrameObj(NULL);
		pBO_new->m_boneFrame->AddTransformReplace(pBO_new->m_startPosition);
		pBO_new->m_lastToTargetTimeStamp[0] = 0;
		pBO_new->m_lastToTargetTimeStamp[1] = 0;
		pBO_new->m_lastPositionsAreInitialized[0] = FALSE;
		pBO_new->m_lastPositionsAreInitialized[1] = FALSE;
		pBO_new->m_procAnimmat = -1;
		pBO_new->m_referencedAnimList = FALSE;

		pRS_new->m_pBoneList->AddTail(pBO_new);
	}

	pRS_new->scaleSystem(pRS_new->m_scaleVector.x, pRS_new->m_scaleVector.y, pRS_new->m_scaleVector.z);
	pRS_new->buildHierarchy();
	pRS_new->initBones();

	return pRS_new;
}

} // namespace KEP