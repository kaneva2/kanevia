///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <afxtempl.h>
#include "CItemClass.h"
#include <PassList.h>
#include "CStatClass.h"
#include "CMaterialClass.h"
#include "CitemExpanseObj.h"
#include "CElementStatusClass.h"
#include "common/include/kepcommon.h"
#ifdef DEPLOY_CLEANER
#include "common/keputil/Algorithms.h"
#else
#endif
#include "common/include/IMemSizeGadget.h"
#include "InventoryType.h"

namespace KEP {

Logger CItemObj::m_logger = Logger::getInstance(L"Engine");

CAttachmentRefObj::CAttachmentRefObj(UINT64 type) {
	m_attachmentType = type;
}

std::string CAttachmentRefObj::toString() const {
	std::stringstream ret;
	ret << m_attachmentType;
	return ret.str();
}

std::string CAttachmentRefList::toString() const {
	std::stringstream ss;
	ss << "[" << GetSize() << "] elements[";
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAttachmentRefObj* spnPtr = (CAttachmentRefObj*)GetNext(posLoc);
		ss << spnPtr->toString();
	}
	ss << "]";
	return ss.str();
}

#ifdef DEPLOY_CLEANER
std::string CAttachmentRefList::toXML() const {
	std::stringstream ss;
	ss << "<CAttachmentRefList>";
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAttachmentRefObj* spnPtr = (CAttachmentRefObj*)GetNext(posLoc);
		ss << spnPtr->toString();
	}
	ss << "</CAttachmentRefList>";
	return ss.str();
}
#else
#endif

void CAttachmentRefList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAttachmentRefObj* spnPtr = (CAttachmentRefObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CAttachmentRefList::Clone(CAttachmentRefList** clone) {
	CAttachmentRefList* copyLocal = new CAttachmentRefList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAttachmentRefObj* bnPtr = (CAttachmentRefObj*)GetNext(posLoc);
		CAttachmentRefObj* newObject = new CAttachmentRefObj();
		newObject->m_attachmentType = bnPtr->m_attachmentType;
		copyLocal->AddTail(newObject);
	}
	*clone = copyLocal;
}

void CAttachmentRefObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		int tempint = (int)m_attachmentType;
		ar << tempint;
	} else {
		int tempint;
		ar >> tempint;
		m_attachmentType = (UINT64)tempint;
	}
}

CItemObj::CItemObj() :
		m_requiredStats(0), m_passList(0), m_defaultArmedItem(false), m_useAnywhere(false), m_passGroup(PASS_GROUP_INVALID) // drf - added
		,
		m_attachmentRefList(NULL),
		m_marketCost(0),
		m_sellingPrice(0),
		m_equiped(FALSE),
		m_disarmable(TRUE),
		m_globalID(INVALID_GLID) // drf - todo change to GLID_INVALID
		,
		m_animAccessValue(-1),
		m_skillUseRef(-1),
		m_minimumLevel(0),
		m_masteredLevel(0),
		m_permanent(FALSE),
		m_useType(USE_TYPE_NONE),
		m_miscUseValue(0),
		m_raceID(0),
		m_nonDrop(FALSE),
		m_statBonuses(0),
		m_dbSrcIndex(0),
		m_chargeType(0),
		m_chargeCurrent(0),
		m_maxCharges(0),
		m_keyID(0),
		m_elementDefenses(NULL),
		m_nonTransferable(FALSE),
		m_destroyOnUse(FALSE),
		m_requiredSkillType(0),
		m_requiredSkillLevel(0),
		m_stackable(FALSE),
		m_expansiveCollection(NULL),
		m_defensesSpecific(-1),
		m_creationTimeStamp(0),
		m_timoutValue(0),
		m_visualRef(0),
		m_yJetforce(0.0f),
		m_zJetforce(0.0f),
		m_reloadTime(0),
		m_actorGroup(0),
		m_itemType(IT_NORMAL | IT_GIFT),
		m_expiredDuration(0),
		m_baseGlid(GLID_INVALID),
		m_materialOverride(NULL),
		m_derivable(false) {
}

CItemObj::~CItemObj() {
	if (m_attachmentRefList) {
		m_attachmentRefList->SafeDelete();
		delete m_attachmentRefList;
		m_attachmentRefList = 0;
	}

	if (m_elementDefenses) {
		m_elementDefenses->SafeDelete();
		delete m_elementDefenses;
		m_elementDefenses = 0;
	}

	if (m_statBonuses) {
		m_statBonuses->SafeDelete();
		delete m_statBonuses;
		m_statBonuses = 0;
	}

	if (m_requiredStats) {
		m_requiredStats->SafeDelete();
		delete m_requiredStats;
		m_requiredStats = 0;
	}

	if (m_expansiveCollection) {
		m_expansiveCollection->SafeDelete();
		delete m_expansiveCollection;
		m_expansiveCollection = 0;
	}
	if (m_passList) {
		delete m_passList;
		m_passList = 0;
	}

	if (m_materialOverride) {
		delete m_materialOverride;
		m_materialOverride = 0;
	}
}

std::string CItemObj::toString() const {
	std::stringstream ret;
	ret
		<< "globalID=" << m_globalID
		<< ";baseGlid=" << m_baseGlid
		<< ";derivable=" << m_derivable
		<< ";itemType=" << m_itemType
		<< ";marketCost=" << m_marketCost
		<< ";sellingPrice=" << m_sellingPrice
		<< ";equiped=" << m_equiped
		<< ";disarmable=" << m_disarmable
		<< ";animAccessValue=" << m_animAccessValue
		<< ";skillUseRef=" << m_skillUseRef
		<< ";minimumLevel=" << m_minimumLevel
		<< ";masteredLevel=" << m_masteredLevel
		<< ";permanent=" << m_permanent
		<< ";useType=" << (int)m_useType
		<< ";miscUseValue=" << m_miscUseValue
		<< ";armourClass=" << (int)0 //m_armourClass
		<< ";passGroup=" << (int)m_passGroup // drf -added
		<< ";raceID=" << m_raceID
		<< ";nonDrop=" << m_nonDrop
		<< ";dbSrcIndex=" << m_dbSrcIndex
		<< ";chargeType=" << m_chargeType
		<< ";chargeCurrent=" << m_chargeCurrent
		<< ";maxCharges=" << m_maxCharges
		<< ";keyID=" << m_keyID
		<< ";nonTransferable=" << m_nonTransferable
		<< ";destroyOnUse=" << m_destroyOnUse
		<< ";requiredSkillType=" << m_requiredSkillType
		<< ";requiredSkillLevel=" << m_requiredSkillLevel
		<< ";stackable=" << m_stackable
		<< ";defensesSpecific=" << m_defensesSpecific
		<< ";timoutValue=" << m_timoutValue
		<< ";visualRef=" << m_visualRef
		<< ";yJetforce=" << m_yJetforce
		<< ";zJetforce=" << m_zJetforce
		<< ";creationTimeStamp=" << m_creationTimeStamp
		<< ";reloadTime=" << m_reloadTime
		<< ";actorGroup=" << m_actorGroup
		<< ";defaultArmedItem=" << m_defaultArmedItem
		<< ";useAnywhere=" << m_useAnywhere
		<< ";itemDescription=" << m_itemDescription
		<< ";visualRef=" << m_visualRef
		<< ";defaultArmedItem=" << m_defaultArmedItem
		<< ";permanent=" << m_permanent
		<< ";statBonuses=" << m_statBonuses
		<< ";requiredStats=" << m_requiredStats
		<< ";attachmentRefList="
		<< "[" << m_attachmentRefList << "]" << (m_attachmentRefList ? m_attachmentRefList->toString() : "")
		<< ";elementDefenses=" << m_elementDefenses
		<< ";expansiveCollection=" << m_expansiveCollection
		<< ";passList="
		<< "[" << m_passList << "]" << (m_passList ? m_passList->toString() : "")
		<< ";m_materialOverride=" << m_materialOverride
		<< ";itemname=" << m_itemName;

	return ret.str();
}
#ifdef DEPLOY_CLEANER
std::string CItemObj::toXML() const {
	std::stringstream ret;
	ret << "<CItemObj"
		<< " globalID=\"" << m_globalID << "\""
		<< " baseGlid=\"" << m_baseGlid << "\""
		<< " derivable=\"" << m_derivable << "\""
		<< " itemType=\"" << m_itemType << "\""
		<< " marketCost=\"" << m_marketCost << "\""
		<< " sellingPrice=\"" << m_sellingPrice << "\""
		<< " equiped=\"" << m_equiped << "\""
		<< " disarmable=\"" << m_disarmable << "\""
		<< " animAccessValue=\"" << m_animAccessValue << "\""
		<< " skillUseRef=\"" << m_skillUseRef << "\""
		<< " minimumLevel=\"" << m_minimumLevel << "\""
		<< " masteredLevel=\"" << m_masteredLevel << "\""
		<< " permanent=\"" << m_permanent << "\""
		<< " useType=\"" << (int)m_useType << "\""
		<< " miscUseValue=\"" << m_miscUseValue << "\""
		<< " armourClass=\"" << (int)0 << "\"" // deprecated
		<< " passGroup=\"" << (int)m_passGroup << "\"" // drf - added
		<< " raceID=\"" << m_raceID << "\""
		<< " nonDrop=\"" << m_nonDrop << "\""
		<< " dbSrcIndex=\"" << m_dbSrcIndex << "\""
		<< " chargeType=\"" << m_chargeType << "\""
		<< " chargeCurrent=\"" << m_chargeCurrent << "\""
		<< " maxCharges=\"" << m_maxCharges << "\""
		<< " keyID=\"" << m_keyID << "\""
		<< " nonTransferable=\"" << m_nonTransferable << "\""
		<< " destroyOnUse=\"" << m_destroyOnUse << "\""
		<< " requiredSkillType=\"" << m_requiredSkillType << "\""
		<< " requiredSkillLevel=\"" << m_requiredSkillLevel << "\""
		<< " stackable=\"" << m_stackable << "\""
		<< " defensesSpecific=\"" << m_defensesSpecific << "\""
		<< " timoutValue=\"" << m_timoutValue << "\""
		<< " visualRef=\"" << m_visualRef << "\""
		<< " yJetforce=\"" << m_yJetforce << "\""
		<< " zJetforce=\"" << m_zJetforce << "\""
		<< " creationTimeStamp=\"" << m_creationTimeStamp << "\""
		<< " reloadTime=\"" << m_reloadTime << "\""
		<< " actorGroup=\"" << m_actorGroup << "\""
		<< " defaultArmedItem=\"" << m_defaultArmedItem << "\""
		<< " useAnywhere=\"" << m_useAnywhere << "\""
		<< " statBonuses=\"" << m_statBonuses << "\""
		<< " requiredStats=\"" << m_requiredStats << "\""
		<< " elementDefenses=\"" << m_elementDefenses << "\""
		<< " expansiveCollection=\"" << m_expansiveCollection << "\""
		<< " passList=\""
		<< "[" << m_passList << "]" << (m_passList ? m_passList->toString() : "") << "\""
		<< " materialOverride=\"" << m_materialOverride << "\">"
		<< "<name>" << Algorithms::xmlEscape(std::string((const char*)m_itemName)) << "</name>"
		<< "<description>" << Algorithms::xmlEscape(std::string((const char*)m_itemDescription)) << "</description>"
		<< (m_attachmentRefList ? m_attachmentRefList->toXML() : "")
		<< "</CItemObj>";

	return ret.str();
}
#else
#endif

CItemObj::CItemObj(const CItemObj& toCopy) :
		m_passList(0), m_statBonuses(0), m_requiredStats(0), m_elementDefenses(0), m_attachmentRefList(0), m_expansiveCollection(0), m_materialOverride(0) {
	operator=(toCopy);
}

CItemObj&
CItemObj::operator=(const CItemObj& assignFrom) {
	m_itemName = assignFrom.m_itemName;
	m_marketCost = assignFrom.m_marketCost;
	m_sellingPrice = assignFrom.m_sellingPrice;
	m_equiped = assignFrom.m_equiped;
	m_disarmable = assignFrom.m_disarmable;
	m_globalID = assignFrom.m_globalID;
	m_animAccessValue = assignFrom.m_animAccessValue;
	m_skillUseRef = assignFrom.m_skillUseRef;
	m_minimumLevel = assignFrom.m_minimumLevel;
	m_masteredLevel = assignFrom.m_masteredLevel;
	m_permanent = assignFrom.m_permanent;
	m_useType = assignFrom.m_useType;
	m_miscUseValue = assignFrom.m_miscUseValue;
	m_passGroup = assignFrom.m_passGroup; // drf - added
	m_raceID = assignFrom.m_raceID;
	m_nonDrop = assignFrom.m_nonDrop;
	m_dbSrcIndex = assignFrom.m_dbSrcIndex;
	m_chargeType = assignFrom.m_chargeType;
	m_chargeCurrent = assignFrom.m_chargeCurrent;
	m_maxCharges = assignFrom.m_maxCharges;
	m_keyID = assignFrom.m_keyID;
	m_nonTransferable = assignFrom.m_nonTransferable;
	m_destroyOnUse = assignFrom.m_destroyOnUse;
	m_requiredSkillType = assignFrom.m_requiredSkillType;
	m_requiredSkillLevel = assignFrom.m_requiredSkillLevel;
	m_stackable = assignFrom.m_stackable;
	m_defensesSpecific = assignFrom.m_defensesSpecific;
	m_timoutValue = assignFrom.m_timoutValue;
	m_visualRef = assignFrom.m_visualRef;
	m_yJetforce = assignFrom.m_yJetforce;
	m_zJetforce = assignFrom.m_zJetforce;
	m_dimConfigurations = assignFrom.m_dimConfigurations;
	m_exclusionGroupIDs = assignFrom.m_exclusionGroupIDs;
	m_creationTimeStamp = assignFrom.m_creationTimeStamp;
	m_reloadTime = assignFrom.m_reloadTime;
	m_actorGroup = assignFrom.m_actorGroup;
	m_defaultArmedItem = assignFrom.m_defaultArmedItem;
	m_useAnywhere = assignFrom.m_useAnywhere;
	m_itemDescription = assignFrom.m_itemDescription;
	m_itemType = assignFrom.m_itemType;
	m_baseGlid = assignFrom.m_baseGlid;
	m_visualRef = assignFrom.m_visualRef;
	m_defaultArmedItem = assignFrom.m_defaultArmedItem;
	m_permanent = assignFrom.m_permanent;
	m_derivable = assignFrom.m_derivable;

	if (assignFrom.m_statBonuses)
		assignFrom.m_statBonuses->Clone(&m_statBonuses);
	if (assignFrom.m_requiredStats)
		assignFrom.m_requiredStats->Clone(&m_requiredStats);
	if (assignFrom.m_attachmentRefList)
		assignFrom.m_attachmentRefList->Clone(&m_attachmentRefList);
	if (assignFrom.m_elementDefenses)
		assignFrom.m_elementDefenses->Clone(&m_elementDefenses);
	if (assignFrom.m_expansiveCollection)
		assignFrom.m_expansiveCollection->Clone(&m_expansiveCollection);

	if (assignFrom.m_passList && !assignFrom.m_passList->empty()) {
		if (this->m_passList == 0)
			this->m_passList = new PassList(*assignFrom.m_passList);
		else
			*this->m_passList = *assignFrom.m_passList;
	} else {
		DELETE_AND_ZERO(m_passList);
	}

	if (assignFrom.m_materialOverride)
		assignFrom.m_materialOverride->Clone(&this->m_materialOverride);
	else
		this->m_materialOverride = NULL;

	return *this;
}

CItemObj& CItemObj::inheritFrom(const CItemObj& baseItem) {
	// assert that this item is the base of the new item
	jsAssert(m_baseGlid == baseItem.m_globalID);

	CItemObj baseCopy(baseItem);
	baseCopy.derivate(*this);
	operator=(baseCopy);
	return *this;
}

CItemObj& CItemObj::derivate(const CItemObj& derivative) {
	// assert that this item is the base of the new item
	jsAssert(derivative.m_baseGlid == m_globalID);

	m_itemName = derivative.m_itemName;
	m_itemDescription = derivative.m_itemDescription;
	m_marketCost = derivative.m_marketCost;
	m_sellingPrice = derivative.m_sellingPrice;
	m_requiredSkillType = derivative.m_requiredSkillType;
	m_requiredSkillLevel = derivative.m_requiredSkillLevel;
	m_useType = derivative.m_useType;
	m_miscUseValue = derivative.m_miscUseValue;
	m_passGroup = derivative.m_passGroup; // drf -added
	m_itemType = derivative.m_itemType;
	m_baseGlid = derivative.m_baseGlid;
	m_expiredDuration = derivative.m_expiredDuration;
	m_globalID = derivative.m_globalID;
	m_visualRef = derivative.m_visualRef;
	m_defaultArmedItem = derivative.m_defaultArmedItem;
	m_permanent = derivative.m_permanent;
	m_derivable = derivative.m_derivable;
	m_animations = derivative.m_animations;
	m_creationTimeStamp = derivative.m_creationTimeStamp;

	if (derivative.m_passList && !derivative.m_passList->empty()) {
		// If there is no pass list allocation a new one.
		if (m_passList == 0)
			m_passList = new PassList(*derivative.m_passList);

		// but if there is one, copy the contents.
		else
			*m_passList = *derivative.m_passList;
	} else {
		DELETE_AND_ZERO(m_passList);
	}
	return *this;
}

void CItemObj::Clone(CItemObj** clone) {
	CItemObj* copyObj = new CItemObj();
	copyObj->m_itemName = m_itemName;
	copyObj->m_marketCost = m_marketCost;
	copyObj->m_sellingPrice = m_sellingPrice;
	copyObj->m_equiped = FALSE;
	copyObj->m_disarmable = m_disarmable;
	copyObj->m_globalID = m_globalID;
	copyObj->m_animAccessValue = m_animAccessValue;
	copyObj->m_skillUseRef = m_skillUseRef;
	copyObj->m_minimumLevel = m_minimumLevel;
	copyObj->m_masteredLevel = m_masteredLevel;
	copyObj->m_permanent = m_permanent;
	copyObj->m_useType = m_useType;
	copyObj->m_miscUseValue = m_miscUseValue;
	copyObj->m_passGroup = m_passGroup; // drf - added
	copyObj->m_raceID = m_raceID;
	copyObj->m_nonDrop = m_nonDrop;
	copyObj->m_dbSrcIndex = m_dbSrcIndex;
	copyObj->m_chargeType = m_chargeType;
	copyObj->m_chargeCurrent = m_chargeCurrent;
	copyObj->m_maxCharges = m_maxCharges;
	copyObj->m_keyID = m_keyID;
	copyObj->m_nonTransferable = m_nonTransferable;
	copyObj->m_destroyOnUse = m_destroyOnUse;
	copyObj->m_requiredSkillType = m_requiredSkillType;
	copyObj->m_requiredSkillLevel = m_requiredSkillLevel;
	copyObj->m_stackable = m_stackable;
	copyObj->m_defensesSpecific = m_defensesSpecific;
	copyObj->m_timoutValue = m_timoutValue;
	copyObj->m_visualRef = m_visualRef;
	copyObj->m_yJetforce = m_yJetforce;
	copyObj->m_zJetforce = m_zJetforce;

	copyObj->m_dimConfigurations = m_dimConfigurations;
	copyObj->m_exclusionGroupIDs = m_exclusionGroupIDs;

	copyObj->m_creationTimeStamp = fTime::TimeMs();
	copyObj->m_reloadTime = m_reloadTime;
	copyObj->m_actorGroup = m_actorGroup;

	if (m_statBonuses)
		m_statBonuses->Clone(&copyObj->m_statBonuses);

	if (m_requiredStats)
		m_requiredStats->Clone(&copyObj->m_requiredStats);

	if (m_attachmentRefList)
		m_attachmentRefList->Clone(&copyObj->m_attachmentRefList);

	if (m_elementDefenses)
		m_elementDefenses->Clone(&copyObj->m_elementDefenses);

	if (m_expansiveCollection)
		m_expansiveCollection->Clone(&copyObj->m_expansiveCollection);

	copyObj->m_defaultArmedItem = m_defaultArmedItem;
	copyObj->m_useAnywhere = m_useAnywhere;
	if (m_passList && !m_passList->empty()) {
		if (copyObj->m_passList == 0)
			copyObj->m_passList = new PassList(*m_passList);
		else
			*copyObj->m_passList = *m_passList;
	} else {
		DELETE_AND_ZERO(copyObj->m_passList);
	}

	copyObj->m_itemDescription = m_itemDescription;
	copyObj->m_itemType = m_itemType;
	copyObj->m_baseGlid = m_baseGlid;

	if (m_materialOverride)
		m_materialOverride->Clone(&copyObj->m_materialOverride);
	else
		copyObj->m_materialOverride = NULL;

	copyObj->m_visualRef = m_visualRef;
	copyObj->m_defaultArmedItem = m_defaultArmedItem;
	copyObj->m_permanent = m_permanent;

	copyObj->m_derivable = m_derivable;

	*clone = copyObj;
}

void CItemObj::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_globalID
		   << m_expansiveCollection;

	} else {
		ar >> m_globalID >> m_expansiveCollection;

		// DRF - Legacy INVALID_GLID(-1) Conversion
		if (m_globalID == -1)
			m_globalID = GLID_INVALID;
	}
}

void CItemObj::Serialize(CArchive& ar) {
	char reservedByte = 0;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_itemName
		   << m_marketCost
		   << (int)0 // m_requiredDex
		   << (int)0 // m_requiredStr
		   << (int)0 // m_requiredKnowledge
		   << m_attachmentRefList
		   << m_equiped
		   << m_disarmable
		   << m_globalID
		   << m_animAccessValue
		   << m_permanent
		   << (int)m_useType
		   << m_miscUseValue
		   << (int)0 //m_armourClass
		   << m_raceID
		   << m_nonDrop
		   << m_dbSrcIndex
		   << m_statBonuses
		   << m_nonTransferable
		   << m_destroyOnUse
		   << m_requiredSkillType
		   << m_requiredSkillLevel
		   << m_stackable
		   << m_timoutValue
		   << m_elementDefenses
		   << m_chargeType
		   << m_chargeCurrent
		   << m_maxCharges
		   << m_keyID
		   << m_expansiveCollection //migrated
		   << m_defensesSpecific
		   << m_visualRef
		   << m_yJetforce
		   << m_zJetforce

		   << reservedByte
		   << m_reloadTime
		   << m_sellingPrice
		   << m_requiredStats;

		ar << m_actorGroup;
		if (m_passList) {
			m_passList->SerializeOut(ar);
		} else
			ar << (int)0; // count

		ar << m_defaultArmedItem
		   << m_useAnywhere;

		ar << m_itemDescription
		   << m_itemType;

		SerializeDimCfgsOut(ar);

		ar << m_exclusionGroupIDs.size();
		for (std::vector<int>::iterator exID = m_exclusionGroupIDs.begin(); exID != m_exclusionGroupIDs.end(); exID++) {
			ar << *exID;
		}
	} else {
		int version = ar.GetObjectSchema();

		//Used to convert from char to short for old formats
		char oldCfgIndex1, oldCfgIndex2, oldCfgIndex3, oldCfgIndex4;
		char oldCfgTo1, oldCfgTo2, oldCfgTo3, oldCfgTo4;
		char oldCfgBack1, oldCfgBack2, oldCfgBack3, oldCfgBack4;
		int oldMaterial1, oldMaterial2, oldMaterial3, oldMaterial4;
		oldMaterial1 = oldMaterial2 = oldMaterial3 = oldMaterial4 = -1;

		//structures to upgrade from old formats
		dimConfig cfg1, cfg2, cfg3, cfg4;
		cfg1.dimMaterial = cfg2.dimMaterial = cfg3.dimMaterial = cfg4.dimMaterial = -1;
		cfg1.dimMatPreserve = cfg2.dimMatPreserve = cfg3.dimMatPreserve = cfg4.dimMatPreserve = FALSE;

		int oldDex, oldStr, oldKnowledge;

		int oldLocationID = -1;

		ar >> m_itemName >> m_marketCost >> oldDex >> oldStr >> oldKnowledge >> m_attachmentRefList >> m_equiped;

		if (version < 12) {
			ar >> oldLocationID;
		}

		int useTypeInt = USE_TYPE_NONE;
		int deprecated;
		ar >> m_disarmable >> m_globalID >> m_animAccessValue >> m_permanent >> useTypeInt >> m_miscUseValue >> deprecated //m_armourClass
			>> m_raceID >> m_nonDrop >> m_dbSrcIndex >> m_statBonuses >> m_nonTransferable //updated from here down
			>> m_destroyOnUse >> m_requiredSkillType >> m_requiredSkillLevel >> m_stackable >> m_timoutValue >> m_elementDefenses >> m_chargeType >> m_chargeCurrent >> m_maxCharges >> m_keyID >> m_expansiveCollection //migrated
			>> m_defensesSpecific; //migrated;

		// DRF - Legacy INVALID_GLID(-1) Conversion
		if (m_globalID == -1)
			m_globalID = GLID_INVALID;

		m_useType = (USE_TYPE)useTypeInt;
		m_visualRef = 0;
		m_reloadTime = 0;

		if (version == 2) {
			ar >> m_visualRef >> m_yJetforce >> m_zJetforce;

			cfg1.dimCfgIndex = -1;
			cfg1.dimCfgChangeTo = -1;
			cfg1.dimCfgChangeBack = -1;
		} else if (version >= 3) {
			ar >> m_visualRef >> m_yJetforce >> m_zJetforce;

			if (version < 12) {
				ar >> oldCfgIndex1 >> oldCfgTo1 >> oldCfgBack1;

				cfg1.dimCfgIndex = oldCfgIndex1;
				cfg1.dimCfgChangeTo = oldCfgTo1;
				cfg1.dimCfgChangeBack = oldCfgBack1;
			}

			ar >> reservedByte >> m_reloadTime;
		}

		if (version >= 4) {
			ar >> m_sellingPrice;
		} else {
			m_sellingPrice = m_marketCost / 4;
		}

		if (version >= 5) {
			ar >> m_requiredStats;
		} else {
			if (oldStr > 0 || oldDex > 0 || oldKnowledge > 0) {
				m_requiredStats = new CStatBonusList();
				if (oldStr > 0)
					m_requiredStats->AddHead(new CStatBonusObj(2, oldStr)); // 2 = old hard-coded id of STR
				if (oldDex > 0)
					m_requiredStats->AddHead(new CStatBonusObj(3, oldDex)); // 2 = old hard-coded id of DEX
				if (oldKnowledge > 0)
					m_requiredStats->AddHead(new CStatBonusObj(4, oldKnowledge)); // 2 = old hard-coded id of WIS
			}
		}
		if (version >= 6 && version < 12) {
			ar >> oldCfgIndex2 >> oldCfgTo2 >> oldCfgBack2 >> oldCfgIndex3 >> oldCfgTo3 >> oldCfgBack3 >> oldCfgIndex4 >> oldCfgTo4 >> oldCfgBack4;

			cfg2.dimCfgIndex = oldCfgIndex2;
			cfg2.dimCfgChangeTo = oldCfgTo2;
			cfg2.dimCfgChangeBack = oldCfgBack2;
			cfg3.dimCfgIndex = oldCfgIndex3;
			cfg3.dimCfgChangeTo = oldCfgTo3;
			cfg3.dimCfgChangeBack = oldCfgBack3;
			cfg4.dimCfgIndex = oldCfgIndex4;
			cfg4.dimCfgChangeTo = oldCfgTo4;
			cfg4.dimCfgChangeBack = oldCfgBack4;
		}

		if (version >= 7 && version < 12) {
			ar >> oldMaterial1 >> cfg1.dimMatPreserve

				>> oldMaterial2 >> cfg2.dimMatPreserve

				>> oldMaterial3 >> cfg3.dimMatPreserve

				>> oldMaterial4 >> cfg4.dimMatPreserve;

			cfg1.dimMaterial = oldMaterial1;
			cfg2.dimMaterial = oldMaterial2;
			cfg3.dimMaterial = oldMaterial3;
			cfg4.dimMaterial = oldMaterial4;
		}
		if (version >= 8) {
			ar >> m_actorGroup;
		}
		DELETE_AND_ZERO(m_passList);
		if (version > 8) {
			m_passList = PassList::SerializeIn(ar);
			ar >> m_defaultArmedItem;
		} else {
			m_defaultArmedItem = false;
		}
		if (version > 9) {
			ar >> m_useAnywhere;
		} else {
			m_useAnywhere = false;
		}

		if (version > 10) {
			ar >> m_itemDescription;
			ar >> m_itemType;
			// 8/08 removed temp check
		} else {
			m_itemDescription = "";
			m_itemType = IT_NORMAL;
		}

		if (version > 11) {
			SerializeDimCfgsIn(ar);

			int exID = -1;
			std::vector<int>::size_type count;
			ar >> count;
			if (count > 0) {
				for (std::vector<int>::size_type i = 0; i < count; i++) {
					ar >> exID;
					m_exclusionGroupIDs.push_back(exID);
				}
			}
		} else {
			m_exclusionGroupIDs.push_back(oldLocationID);

			if (version >= 3) {
				m_dimConfigurations.push_back(cfg1);
				if (version >= 6) {
					m_dimConfigurations.push_back(cfg2);
					m_dimConfigurations.push_back(cfg3);
					m_dimConfigurations.push_back(cfg4);
				}
			}
		}

		m_skillUseRef = -1;
		m_minimumLevel = 0;
		m_masteredLevel = 0;
		m_creationTimeStamp = 0;
	}
}

void CItemObj::SerializeDimCfgsOut(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_dimConfigurations.size();
		for (DimVector::iterator i = m_dimConfigurations.begin(); i != m_dimConfigurations.end(); i++) {
			ar << (*i).dimCfgIndex;
			ar << (*i).dimCfgChangeTo;
			ar << (*i).dimCfgChangeBack;
			ar << (*i).dimMaterial;
			ar << (*i).dimMatPreserve;
		}
	} else {
		jsAssert(false);
	}
}

void CItemObj::SerializeDimCfgsIn(CArchive& ar) {
	if (ar.IsStoring()) {
		jsAssert(false);
	} else {
		DimVector::size_type count;
		dimConfig cfg;
		ar >> count;
		if (count > 0) {
			for (DimVector::size_type i = 0; i < count; i++) {
				ar >> cfg.dimCfgIndex;
				ar >> cfg.dimCfgChangeTo;
				ar >> cfg.dimCfgChangeBack;
				ar >> cfg.dimMaterial;
				ar >> cfg.dimMatPreserve;

				m_dimConfigurations.push_back(cfg);
			}
		}
	}
}

// Clone the item and overwrite the specific fields that are specified in the db
// from the newItem object.
void CItemObj::CloneFromBaseGLID(CItemObj** clone, CItemObj* newItem, int global_id) {
	Clone(clone);
	(*clone)->m_itemName = newItem->m_itemName;
	(*clone)->m_itemDescription = newItem->m_itemDescription;
	(*clone)->m_marketCost = newItem->m_marketCost;
	(*clone)->m_sellingPrice = newItem->m_sellingPrice;
	(*clone)->m_requiredSkillType = newItem->m_requiredSkillType;
	(*clone)->m_requiredSkillLevel = newItem->m_requiredSkillLevel;
	(*clone)->m_useType = newItem->m_useType;
	(*clone)->m_passGroup = newItem->m_passGroup; // drf -added
	(*clone)->m_miscUseValue = newItem->m_miscUseValue;
	(*clone)->m_itemType = newItem->m_itemType;
	(*clone)->m_baseGlid = newItem->m_baseGlid;
	(*clone)->m_expiredDuration = newItem->m_expiredDuration;
	(*clone)->m_globalID = global_id;
	(*clone)->m_visualRef = newItem->m_visualRef;
	(*clone)->m_defaultArmedItem = newItem->m_defaultArmedItem;
	(*clone)->m_permanent = newItem->m_permanent;
	(*clone)->m_derivable = newItem->m_derivable;
	(*clone)->m_animations = newItem->m_animations;

	if (newItem->m_passList && !newItem->m_passList->empty()) {
		if ((*clone)->m_passList == 0)
			(*clone)->m_passList = new PassList(*newItem->m_passList);
		else
			*(*clone)->m_passList = *newItem->m_passList;
	} else {
		DELETE_AND_ZERO((*clone)->m_passList);
	}
}

void CItemObjList::Clone(CItemObjList** clone) {
	CItemObjList* newList = new CItemObjList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CItemObj* bnPtr = (CItemObj*)GetNext(posLoc);
		CItemObj* newObject = NULL;
		bnPtr->Clone(&newObject);
		newList->AddTail(newObject);
	}
	*clone = newList;
}

CItemObj* CItemObjList::GetByGLID(const GLID& glid) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CItemObj* bnPtr = (CItemObj*)GetNext(posLoc);
		if (bnPtr->m_globalID == glid)
			return bnPtr;
	}
	return NULL;
}

void CItemObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CItemObj* spnPtr = (CItemObj*)GetNext(posLoc);
		delete spnPtr; // CItemObj::SafeDelete now merged with destructor
		spnPtr = 0;
	}
	RemoveAll();
}

CStatBonusObj::CStatBonusObj(int id, int bonus) :
		m_statID(id), m_statBonus(bonus) {
}

void CStatBonusObj::Clone(CStatBonusObj** clone) {
	CStatBonusObj* copyObj = new CStatBonusObj();
	copyObj->m_statID = m_statID;
	copyObj->m_statBonus = m_statBonus;

	*clone = copyObj;
}

void CStatBonusObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_statID
		   << m_statBonus;
	} else {
		ar >> m_statID >> m_statBonus;
	}
}

int CStatBonusList::GetBonusByID(int id) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatBonusObj* bPtr = (CStatBonusObj*)GetNext(posLoc);
		if (bPtr->m_statID == id)
			return bPtr->m_statBonus;
	}
	return 0;
}

void CStatBonusList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatBonusObj* bPtr = (CStatBonusObj*)GetNext(posLoc);
		delete bPtr;
		bPtr = 0;
	}
	RemoveAll();
}

void CStatBonusList::Clone(CStatBonusList** clone) {
	CStatBonusList* copyLocal = new CStatBonusList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatBonusObj* bnPtr = (CStatBonusObj*)GetNext(posLoc);
		CStatBonusObj* newObject = NULL;
		bnPtr->Clone(&newObject);
		copyLocal->AddTail(newObject);
	}
	*clone = copyLocal;
}

// compare passed in requirements to this bonus list to make sure
// the requirements meet the min requirements of *this* list
bool CStatBonusList::MeetsRequirements(CStatObjList& playersCurStats) {
	for (POSITION statPos = GetHeadPosition(); statPos != NULL;) {
		bool found = false;
		CStatBonusObj* minStat = (CStatBonusObj*)GetNext(statPos);
		for (POSITION reqdStatPos = playersCurStats.GetHeadPosition(); reqdStatPos != NULL;) {
			CStatObj* playersCurStat = (CStatObj*)playersCurStats.GetNext(reqdStatPos);
			if (minStat->m_statID == playersCurStat->m_id) {
				if (playersCurStat->m_currentValue < minStat->m_statBonus) {
					return false; // found, but didn't meet min
				}
				found = true;
				break;
			}
		}
		if (!found) {
			return false;
		}
	}

	return true;
}

bool CItemObj::ParseExclusionGroupsCSV(const std::string& sExclusionGroups) {
	if (sExclusionGroups.empty())
		return false;

	std::vector<int> result;

	// Parse comma separated values
	size_t startOfs = 0;
	while (startOfs != std::string::npos) {
		std::string token;
		size_t commaOfs = sExclusionGroups.find(',', startOfs);
		if (commaOfs == std::string::npos) {
			// Last token
			token = sExclusionGroups.substr(startOfs);
			startOfs = std::string::npos;
		} else {
			token = sExclusionGroups.substr(startOfs, commaOfs - startOfs);
			startOfs = commaOfs + 1;
		}

		if (token.empty()) {
			// bad data
			return false;
		}

		int nExclusionGroup = atoi(token.c_str());
		if (errno == ERANGE || errno == EINVAL || nExclusionGroup == 0 && token != "0") {
			// bad data
			return false;
		} else {
			result.push_back(nExclusionGroup);
		}
	}

	m_exclusionGroupIDs.clear();
	if (!result.empty()) {
		m_exclusionGroupIDs.resize(result.size());
		copy(result.begin(), result.end(), m_exclusionGroupIDs.begin());
	}
	return true;
}

void CAttachmentRefObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CItemObj::dimConfig::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CItemObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_animations);
		pMemSizeGadget->AddObject(m_attachmentRefList);
		pMemSizeGadget->AddObject(m_customTexture);
		pMemSizeGadget->AddObject(m_dimConfigurations);
		pMemSizeGadget->AddObject(m_elementDefenses);
		pMemSizeGadget->AddObject(m_exclusionGroupIDs);
		pMemSizeGadget->AddObject(m_expansiveCollection);
		pMemSizeGadget->AddObject(m_itemDescription);
		pMemSizeGadget->AddObject(m_itemName);
		pMemSizeGadget->AddObject(m_materialOverride);
		pMemSizeGadget->AddObject(m_passList);
		pMemSizeGadget->AddObject(m_requiredStats);
		pMemSizeGadget->AddObject(m_statBonuses);
	}
}

void CStatBonusObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CAttachmentRefList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CAttachmentRefObj* pCAttachmentRefObj = static_cast<const CAttachmentRefObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCAttachmentRefObj);
		}
	}
}

void CItemObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CItemObj* pCItemObj = static_cast<const CItemObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCItemObj);
		}
	}
}

void CStatBonusList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CStatBonusList* pCStatBonusList = static_cast<const CStatBonusList*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCStatBonusList);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CStatBonusObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CStatBonusList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CAttachmentRefObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CAttachmentRefList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CItemObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CItemObjList, CObList)

BEGIN_GETSET_IMPL(CAttachmentRefObj, IDS_ATTACHMENTREFOBJ_NAME)
GETSET_RANGE(m_attachmentType, gsInt, GS_FLAGS_DEFAULT, 0, 0, ATTACHMENTREFOBJ, ATTACHMENTTYPE, INT_MIN, INT_MAX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CItemObj, IDS_ITEMOBJ_NAME)
GETSET_MAX(m_itemName, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, ITEMOBJ, ITEMNAME, STRLEN_MAX)
GETSET_RANGE(m_marketCost, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, MARKETCOST, INT_MIN, INT_MAX)
GETSET_RANGE(m_sellingPrice, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, SELLINGPRICE, INT_MIN, INT_MAX)
GETSET(m_equiped, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, EQUIPED)
GETSET(m_disarmable, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, DISARMABLE)
GETSET_RANGE(m_globalID, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, GLOBALID, INT_MIN, INT_MAX)
GETSET_RANGE(m_animAccessValue, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, ANIMACCESSVALUE, INT_MIN, INT_MAX)
GETSET(m_permanent, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, PERMANENT)
GETSET_RANGE(m_useType, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, USETYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_miscUseValue, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, MISCUSEVALUE, INT_MIN, INT_MAX)
GETSET(m_raceID, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, RACEID)
GETSET(m_nonDrop, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, NONDROP)
GETSET_RANGE(m_dbSrcIndex, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, DBSRCINDEX, INT_MIN, INT_MAX)
GETSET(m_nonTransferable, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, NONTRANSFERABLE)
GETSET(m_destroyOnUse, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, DESTROYONUSE)
GETSET_RANGE(m_requiredSkillType, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, REQUIREDSKILLTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_requiredSkillLevel, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, REQUIREDSKILLLEVEL, INT_MIN, INT_MAX)
GETSET(m_stackable, gsBOOL, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, STACKABLE)
GETSET_RANGE(m_timoutValue, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, TIMOUTVALUE, INT_MIN, INT_MAX)
GETSET_RANGE(m_chargeType, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, CHARGETYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_chargeCurrent, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, CHARGECURRENT, INT_MIN, INT_MAX)
GETSET_RANGE(m_maxCharges, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, MAXCHARGES, INT_MIN, INT_MAX)
GETSET_RANGE(m_keyID, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, KEYID, INT_MIN, INT_MAX)
GETSET_RANGE(m_defensesSpecific, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, DEFENSESSPECIFIC, INT_MIN, INT_MAX)
GETSET_RANGE(m_visualRef, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, VISUALREF, INT_MIN, INT_MAX)
GETSET_RANGE(m_yJetforce, gsFloat, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, YJETFORCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_zJetforce, gsFloat, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, ZJETFORCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_reloadTime, gsInt, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, RELOADTIME, INT_MIN, INT_MAX)
GETSET_PTR(m_passList, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, PASSLIST)
GETSET_MAX(m_itemDescription, gsCString, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, ITEMDESCRIPTION, STRLEN_MAX)
GETSET(m_itemType, gsShort, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, ITEMTYPE)
GETSET(m_defaultArmedItem, gsBool, GS_FLAG_EXPOSE, 0, 0, ITEMOBJ, DEFAULTARMEDITEM)
GETSET2(m_actorGroup, gsInt, GS_FLAG_EXPOSE, 0, 0, IDS_ITEMOBJ_ACTORGROUP, IDS_ITEMOBJ_ACTORGROUP)
END_GETSET_IMPL

} // namespace KEP