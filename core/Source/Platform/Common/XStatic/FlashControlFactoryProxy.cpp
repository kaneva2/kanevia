///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "FlashControlFactoryProxy.h"
#include "FlashClientServerAPI.h"
#include <mutex>
#include <thread>
#include "SetThreadName.h"
#include "Core/Util/Parameter.h"
#include "PreciseTime.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {
namespace FlashControl {

// Use one or the other or both (which will try to start the process first and then the thread if that fails).
#define USE_FLASH_IN_PROCESS
//#define USE_FLASH_IN_THREAD

static bool BuildServerArgs(uint32_t iFactoryId, Out<std::wstring> strExeFullPath, Out<std::wstring> strClientProcessHandle, Out<std::wstring> strFactoryId, Out<ProcessHandleWrapper> hClientProcessDup) {
	HANDLE hRawClientProcessDup = INVALID_HANDLE_VALUE;
	if (!::DuplicateHandle(GetCurrentProcess(), GetCurrentProcess(), GetCurrentProcess(), &hRawClientProcessDup, 0, TRUE, DUPLICATE_SAME_ACCESS)) {
		LogError("Could not duplicate handle");
		return false;
	}
	hClientProcessDup.get().Set(hRawClientProcessDup);

	std::wstring wstrPath;
	wchar_t achFileName[MAX_PATH + 1] = { 0 };
	if (GetModuleFileNameW(NULL, achFileName, std::extent<decltype(achFileName)>::value)) {
		wstrPath = achFileName;
		while (!wstrPath.empty()) {
			wchar_t chLast = wstrPath.back();
			if (chLast == L'/' || chLast == L'\\' || chLast == L':')
				break;
			wstrPath.pop_back();
		}
	}
	const wchar_t* pszExeName = _T(K_CLIENT_MEDIA_EXE);
	strExeFullPath.set(wstrPath + pszExeName);
	std::wostringstream strm;
	strm << reinterpret_cast<uintptr_t>(hClientProcessDup.get().GetHandle());
	strClientProcessHandle.set(strm.str());
	std::wostringstream strmFactoryId;
	strmFactoryId << iFactoryId;
	strFactoryId.set(strmFactoryId.str());

	return true;
}

#ifdef USE_FLASH_IN_PROCESS
static ProcessHandleWrapper StartServerProcess(uint32_t iFactoryId) {
	ProcessHandleWrapper hNewProcess;
	ProcessHandleWrapper hClientProcessDup;
	std::wstring strExeFullPath;
	std::wstring strClientProcessHandle;
	std::wstring strFactoryId;
	if (!BuildServerArgs(iFactoryId, out(strExeFullPath), out(strClientProcessHandle), out(strFactoryId), out(hClientProcessDup)))
		return hNewProcess;

		// We want the child process to only inherit the handles we specify.
		// The API that Windows provides to do this is only available in Vista.
		// We use LoadProcAddress so we can use it when available.
		// On systems that don't support it (i.e. Windows XP), we inherit all inheritable handles.
		// Unfortunately, this will cause log file handles to be inherited and they will remain
		// open in the child when we try to roll them over in the client. This will cause the rename
		// to fail.
#ifndef PROC_THREAD_ATTRIBUTE_HANDLE_LIST
#define PROC_THREAD_ATTRIBUTE_NUMBER 0x0000FFFF
#define PROC_THREAD_ATTRIBUTE_THREAD 0x00010000 // Attribute may be used with thread creation
#define PROC_THREAD_ATTRIBUTE_INPUT 0x00020000 // Attribute is input only
#define PROC_THREAD_ATTRIBUTE_ADDITIVE 0x00040000 // Attribute may be "accumulated," e.g. bitmasks, counters, etc.
	typedef enum _PROC_THREAD_ATTRIBUTE_NUM {
		ProcThreadAttributeParentProcess = 0,
		ProcThreadAttributeHandleList = 2,
#if (_WIN32_WINNT >= _WIN32_WINNT_WIN7)
		ProcThreadAttributeGroupAffinity = 3,
		ProcThreadAttributePreferredNode = 4,
		ProcThreadAttributeIdealProcessor = 5,
		ProcThreadAttributeUmsThread = 6,
		ProcThreadAttributeMitigationPolicy = 7,
#endif
#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8)
		ProcThreadAttributeSecurityCapabilities = 9,
#endif
		ProcThreadAttributeProtectionLevel = 11,
	} PROC_THREAD_ATTRIBUTE_NUM;
#define ProcThreadAttributeValue(Number, Thread, Input, Additive) \
	(((Number)&PROC_THREAD_ATTRIBUTE_NUMBER) |                    \
		((Thread != FALSE) ? PROC_THREAD_ATTRIBUTE_THREAD : 0) |  \
		((Input != FALSE) ? PROC_THREAD_ATTRIBUTE_INPUT : 0) |    \
		((Additive != FALSE) ? PROC_THREAD_ATTRIBUTE_ADDITIVE : 0))
#define PROC_THREAD_ATTRIBUTE_HANDLE_LIST \
	ProcThreadAttributeValue(ProcThreadAttributeHandleList, FALSE, TRUE, FALSE)
	typedef struct _STARTUPINFOEX {
		STARTUPINFO StartupInfo;
		PPROC_THREAD_ATTRIBUTE_LIST lpAttributeList;
	} STARTUPINFOEX, *LPSTARTUPINFOEX;
	typedef struct _PROC_THREAD_ATTRIBUTE_LIST *PPROC_THREAD_ATTRIBUTE_LIST, *LPPROC_THREAD_ATTRIBUTE_LIST;

	HMODULE hModule = LoadLibraryW(L"kernel32.dll"); // We don't bother unloading.
	typedef BOOL(WINAPI * InitializeProcThreadAttributeListType)(LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList, DWORD dwAttributeCount, DWORD dwFlags, PSIZE_T lpSize);
	typedef BOOL(WINAPI * UpdateProcThreadAttributeType)(LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList, DWORD dwFlags, DWORD_PTR Attribute, PVOID lpValue, SIZE_T cbSize, PVOID lpPreviousValue, PSIZE_T lpReturnSize);
	typedef VOID(WINAPI * DeleteProcThreadAttributeListType)(LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList);
	InitializeProcThreadAttributeListType InitializeProcThreadAttributeList = reinterpret_cast<InitializeProcThreadAttributeListType>(GetProcAddress(hModule, "InitializeProcThreadAttributeList"));
	UpdateProcThreadAttributeType UpdateProcThreadAttribute = reinterpret_cast<UpdateProcThreadAttributeType>(GetProcAddress(hModule, "UpdateProcThreadAttribute"));
	DeleteProcThreadAttributeListType DeleteProcThreadAttributeList = reinterpret_cast<DeleteProcThreadAttributeListType>(GetProcAddress(hModule, "DeleteProcThreadAttributeList"));
	bool bHaveThreadListAPI = (InitializeProcThreadAttributeList && UpdateProcThreadAttribute && DeleteProcThreadAttributeList);
#else
#pragma message(This code can be removed when Vista is our minimum required Windows version.)
	bool bHaveThreadListAPI = true;
#endif

	std::wstring strCmdLine = L"\"" + strExeFullPath + L"\" " + strClientProcessHandle + L" " + strFactoryId;
	std::vector<wchar_t> achCmdLine(strCmdLine.begin(), strCmdLine.end()); // CreateProcess requires a pointer to non-const characters.
	achCmdLine.push_back(0);
	STARTUPINFOEX startupInfoEx = { 0 };
	STARTUPINFO& startupInfo = startupInfoEx.StartupInfo;
	startupInfo.cb = sizeof(startupInfoEx);
	// Ignore standard input, output, and error.
	// Not useful, and suppresses the Flash startup message "Vector smash protection is enabled."
	startupInfo.dwFlags |= STARTF_USESTDHANDLES;
	startupInfo.hStdInput = INVALID_HANDLE_VALUE;
	startupInfo.hStdOutput = INVALID_HANDLE_VALUE;
	startupInfo.hStdError = INVALID_HANDLE_VALUE;
	DWORD dwThreadCreationFlags = NORMAL_PRIORITY_CLASS;

	// In addition to deallocating memory, we have to call DeleteProcThreadAttributeList after InitializeProcThreadAttributeList
	// so we have two unique_ptrs pointing to the same object.
	std::unique_ptr<_PROC_THREAD_ATTRIBUTE_LIST, decltype(free)*> pAttributeListAllocated(nullptr, &free);
	std::unique_ptr<_PROC_THREAD_ATTRIBUTE_LIST, DeleteProcThreadAttributeListType> pAttributeList(nullptr, DeleteProcThreadAttributeList);
	if (bHaveThreadListAPI) {
		SIZE_T size = 0;
		if (InitializeProcThreadAttributeList(NULL, 1, 0, &size) || GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
			pAttributeListAllocated.reset(reinterpret_cast<LPPROC_THREAD_ATTRIBUTE_LIST>(malloc(size)));
			if (pAttributeListAllocated && InitializeProcThreadAttributeList(pAttributeListAllocated.get(), 1, 0, &size))
				pAttributeList.reset(pAttributeListAllocated.get());
			size_t nHandles = 1;
			HANDLE handle = hClientProcessDup.GetHandle();
			if (pAttributeList && UpdateProcThreadAttribute(pAttributeList.get(), 0, PROC_THREAD_ATTRIBUTE_HANDLE_LIST, &handle, nHandles * sizeof(HANDLE), NULL, NULL)) {
				dwThreadCreationFlags |= EXTENDED_STARTUPINFO_PRESENT;
				startupInfoEx.lpAttributeList = pAttributeList.get();
			}
		}
	}

	PROCESS_INFORMATION processInfo = { 0 };
	if (!::CreateProcessW(strExeFullPath.c_str(), achCmdLine.data(), NULL, NULL, TRUE, dwThreadCreationFlags, nullptr, nullptr, &startupInfo, &processInfo)) {
		LogError("Could not create process: " << Utf16ToUtf8(strExeFullPath) << "  Error: " << GetLastError());
		return hNewProcess;
	}
	hNewProcess.Set(processInfo.hProcess);
	ThreadHandleWrapper hThread(processInfo.hThread); // Put thread in wrapper to make sure it gets closed. Or we could just call CloseHandle() on it.

	return hNewProcess;
}
#endif

#ifdef USE_FLASH_IN_THREAD
int FlashProcessMain(int argc, wchar_t* argv[]);
static ThreadHandleWrapper StartServerThread(uint32_t iFactoryId) {
	ProcessHandleWrapper hClientProcessDup;
	std::wstring strExeFullPath;
	std::wstring strClientProcessHandle;
	std::wstring strFactoryId;
	if (!BuildServerArgs(iFactoryId, out(strExeFullPath), out(strClientProcessHandle), out(strFactoryId), out(hClientProcessDup)))
		return ThreadHandleWrapper();

	std::vector<size_t> aiArgvOffsets;
	std::vector<wchar_t> achArgvData;
	aiArgvOffsets.push_back(achArgvData.size());
	std::copy(strExeFullPath.begin(), strExeFullPath.end(), std::back_inserter(achArgvData));
	achArgvData.push_back(0);
	aiArgvOffsets.push_back(achArgvData.size());
	std::copy(strClientProcessHandle.begin(), strClientProcessHandle.end(), std::back_inserter(achArgvData));
	achArgvData.push_back(0);
	aiArgvOffsets.push_back(achArgvData.size());
	std::copy(strFactoryId.begin(), strFactoryId.end(), std::back_inserter(achArgvData));
	achArgvData.push_back(0);
	achArgvData.push_back(0);

	static std::vector<wchar_t> s_achArgvData;
	static std::vector<wchar_t*> s_achArgv;
	s_achArgvData = achArgvData;
	s_achArgv.clear();
	for (size_t iOffset : aiArgvOffsets)
		s_achArgv.push_back(s_achArgvData.data() + iOffset);
	struct LocalThread {
		static DWORD WINAPI ThreadFunc(void* p) {
			SetThreadName("FlashProcess");
			return FlashProcessMain(s_achArgv.size(), s_achArgv.data());
		}
	};

	ThreadHandleWrapper hFlashThread(CreateThread(NULL, 0, LocalThread::ThreadFunc, NULL, 0, 0));
	if (hFlashThread) {
		// Leave handles open for the thread to use.
		hClientProcessDup.Release();
	} else {
		LogError("Could not start Flash server thread");
	}

	return hFlashThread;
}
#endif

class FlashControlFactoryProxy : public IFlashControlFactoryProxy {
	enum class eServerInitializedState {
		NotStarted,
		Initializing,
		Initialized,
		Quitting,
		UnknownError
	};

	enum class eWaitServerResult {
		Notify, // Notify event was triggered.
		ProcessStop, // The process exited.
		Error
	};

public:
	FlashControlFactoryProxy(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback);
	virtual ~FlashControlFactoryProxy();

	virtual bool IsFlashInstalled() override final;
	virtual std::string GetVersion() override final;
	virtual void SetGlobalVolume(float fVolume, bool bMute) override final;
	virtual bool WaitForServerToInitialize(double dTimeOutSeconds = kiDefaultTimeoutSeconds) override final;

	virtual bool Start() override final;
	virtual bool Stop() override final;
	virtual bool IsStopped() const override final;
	virtual void CrashFlashProcess() override final;

	virtual bool StartFlashControl(uint32_t iMeshId) override final;
	virtual bool StopFlashControl(uint32_t iMeshId) override final;
	virtual void SignalRequestAvailable(uint32_t iMeshId) override final;

	virtual size_t GetFlashProcessCrashCount() override final;
	virtual size_t GetPeakFlashInstanceCount() override final;

private:
	bool StartFlashServer();
	void StopFlashServer(double dWaitFor);
	eWaitServerResult WaitServer();
	void ProcessServerRequests();

	bool InitializeSyncObjects();
	void SetQuit();
	void StopThread();
	bool ShouldQuit() const;
	static DWORD WINAPI StaticServerCommThreadFunc(void* p);
	DWORD ServerCommThreadFunc();

	void SendCurrentStateToServer_HoldingMutex(); // m_mtxClientState must be held when calling this function.

private:
	static uint32_t s_iNextFactoryId;
	uint32_t m_iFactoryId = 0;

	std::atomic<eServerInitializedState> m_ServerInitializedState = eServerInitializedState::NotStarted;
	EventHandleWrapper m_InitializedEvent; // Set when m_ServerInitializedState is set to initialized (or an error occurred during initialization).
	std::atomic<double> m_dStartInitializeTime = DBL_MAX; // When this object started the thread. Used to measure initialization timeouts.

	std::function<void(float fNewVolume, bool bNewMute)> m_volumeChangeCallback;
	bool m_bFlashIsInstalled = false;
	std::string m_strFlashVersion;
	size_t m_nFlashProcessCrashes = 0;

	ThreadHandleWrapper m_hServerCommThread;
	ProcessHandleWrapper m_ServerProcess;
	ThreadHandleWrapper m_ServerThread;
	EventHandleWrapper m_hClientEventHandle;
	EventHandleWrapper m_hNotifyServerEvent;
	SharedMemory m_hSharedMemory;
	MappedFileView m_hSharedMemoryMapping;
	FlashFactorySharedMemoryBlock* m_pSharedMemory = nullptr;

	// Server state.
	TripleBufferReaderStateMgr m_ServerDataStateMgr;
	FactoryServerStateData m_ServerState;

	// Client state is protected by m_mtxClientState so that requests can be made from any thread.
	std::mutex m_mtxClientState;
	TripleBufferWriterStateMgr m_ClientDataStateMgr;
	FactoryClientStateData m_ClientState;
	std::vector<uint32_t> m_aActiveMeshIds;

	static std::atomic<size_t> s_nCurrentMeshCount;
	static std::atomic<size_t> s_nPeakMeshCount;
};

std::atomic<size_t> FlashControlFactoryProxy::s_nCurrentMeshCount = 0;
std::atomic<size_t> FlashControlFactoryProxy::s_nPeakMeshCount = 0;

std::unique_ptr<IFlashControlFactoryProxy> IFlashControlFactoryProxy::Create(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback) {
	return std::make_unique<FlashControlFactoryProxy>(volumeChangeCallback);
}

uint32_t FlashControlFactoryProxy::s_iNextFactoryId = 1;

FlashControlFactoryProxy::FlashControlFactoryProxy(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback) {
	m_volumeChangeCallback = volumeChangeCallback;
	m_iFactoryId = s_iNextFactoryId++;
}

bool FlashControlFactoryProxy::InitializeSyncObjects() {
	if (m_InitializedEvent)
		return true; // Already initialized.

	// Extra logging because some users have their Flash processes unable to open
	// the event and shared memory objects.
	auto LogIfNull = [](const char* name, HANDLE h) {
		if (h)
			return;

		DWORD dwError = GetLastError();
		LogError("Failed to create " << name << ". Error code: " << dwError);
	};

	// Events
	// This is a manual-reset event. Once signaled, it stays signaled because we stay initialized.
	m_InitializedEvent.Set(CreateEvent(nullptr, TRUE, FALSE, nullptr));
	LogIfNull("m_InitializedEvent", m_InitializedEvent.GetHandle());

	std::wstring strNotifyClientEvent = FlashFactoryNotifyClientEventName(GetCurrentProcessId(), m_iFactoryId);
	m_hClientEventHandle.Set(CreateEventW(nullptr, FALSE, FALSE, strNotifyClientEvent.c_str()));
	LogIfNull("m_hClientEventHandle", m_hClientEventHandle.GetHandle());
	std::wstring strNotifyServerEvent = FlashFactoryNotifyServerEventName(GetCurrentProcessId(), m_iFactoryId);
	m_hNotifyServerEvent.Set(CreateEventW(nullptr, FALSE, FALSE, strNotifyServerEvent.c_str()));
	LogIfNull("m_hNotifyServerEvent", m_hNotifyServerEvent.GetHandle());

	// Shared memory
	size_t iSharedMemorySize = sizeof(FlashFactorySharedMemoryBlock);
	std::wstring strSharedMemory = FlashFactorySharedMemoryName(GetCurrentProcessId(), m_iFactoryId);
	m_hSharedMemory.Set(CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, iSharedMemorySize, strSharedMemory.c_str()));
	LogIfNull("m_hSharedMemory", m_hSharedMemory.GetHandle());
	m_hSharedMemoryMapping = m_hSharedMemory.Map(0, iSharedMemorySize, true);
	m_pSharedMemory = static_cast<FlashFactorySharedMemoryBlock*>(m_hSharedMemoryMapping.GetPointer());

	if (!m_InitializedEvent || !m_hClientEventHandle || !m_hNotifyServerEvent || !m_hSharedMemory || !m_hSharedMemoryMapping || !m_pSharedMemory) {
		LogError("Failed to create Flash synchronization objects.");
		m_InitializedEvent.Close();
		m_hClientEventHandle.Close();
		m_hNotifyServerEvent.Close();
		m_hSharedMemory.Close();
		m_hSharedMemoryMapping.Close();
		m_pSharedMemory = nullptr;
		return false;
	}

	return true;
}

FlashControlFactoryProxy::~FlashControlFactoryProxy() {
	StopThread();
}

bool FlashControlFactoryProxy::IsFlashInstalled() {
	if (!WaitForServerToInitialize())
		return false;
	return m_bFlashIsInstalled;
}

void FlashControlFactoryProxy::SetGlobalVolume(float fVolume, bool bMute) {
	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		VolumeSetting& vs = m_ClientState.m_Volume.GetRefToUpdate();
		vs.m_bMute = bMute;
		vs.m_fVolume = fVolume;

		this->SendCurrentStateToServer_HoldingMutex();
	}
}

std::string FlashControlFactoryProxy::GetVersion() {
	if (!WaitForServerToInitialize())
		return std::string(); // If wait failed, unsafe to directly access m_strFlashVersion without a mutex because it may be updated on another thread.
	return m_strFlashVersion;
}

bool FlashControlFactoryProxy::WaitForServerToInitialize(double dTimeOutSeconds) {
	eServerInitializedState serverInitializedState = m_ServerInitializedState.load(std::memory_order_acquire);
	switch (serverInitializedState) {
		case eServerInitializedState::Initialized:
			return true; // No wait required.

		case eServerInitializedState::Initializing:
			break; // Wait for server to start.

		case eServerInitializedState::NotStarted:
			return false; // Should not be waiting for a server that hasn't been started yet.

		case eServerInitializedState::UnknownError:
		default:
			return false; // Error occurred so server won't be starting.
	}

	double dTimeRemaining = GetCurrentTimeInSeconds() - m_dStartInitializeTime.load(std::memory_order_relaxed);
	if (dTimeRemaining > dTimeOutSeconds)
		return false; // Already exceeded the timeout.

	DWORD dwWaitResult = WaitForSingleObject(m_InitializedEvent.GetHandle(), ceil(dTimeRemaining * 1000));
	if (dwWaitResult == WAIT_FAILED) {
		// Event destroyed?
		LogError("Error waiting for Flash server process to start.");
		return false;
	}

	// reload serverInitializedState
	serverInitializedState = m_ServerInitializedState.load(std::memory_order_acquire);

	if (serverInitializedState == eServerInitializedState::Initialized)
		return true;

	if (serverInitializedState == eServerInitializedState::Initializing)
		LogWarn("Timeout waiting for Flash server process to start.");

	// Process did not start due to timeout or error.
	return false;
}

bool FlashControlFactoryProxy::Start() {
	if (!IsStopped())
		return false;

	double dCurrentTime = GetCurrentTimeInSeconds();
	double dExpectedStartTime = DBL_MAX;
	if (!m_dStartInitializeTime.compare_exchange_strong(dExpectedStartTime, dCurrentTime, std::memory_order_relaxed))
		return false; // Already started.

	if (!InitializeSyncObjects())
		return false;

	eServerInitializedState notStartedState = eServerInitializedState::NotStarted;
	if (!m_ServerInitializedState.compare_exchange_strong(notStartedState, eServerInitializedState::Initializing, std::memory_order_acq_rel, std::memory_order_relaxed))
		return false; // Already started.

	m_hServerCommThread.Set(CreateThread(NULL, 0, &StaticServerCommThreadFunc, this, 0, nullptr));
	if (!m_hServerCommThread)
		m_ServerInitializedState.store(eServerInitializedState::UnknownError, std::memory_order_release);

	return true;
}

bool FlashControlFactoryProxy::Stop() {
	if (IsStopped())
		return true;

	SetQuit();
	WaitForSingleObject(m_hServerCommThread.GetHandle(), INFINITE);
	m_hServerCommThread.Close();
	m_ServerProcess.Close();
	m_ServerInitializedState.store(eServerInitializedState::NotStarted, std::memory_order_release);
	return true;
}

bool FlashControlFactoryProxy::IsStopped() const {
	return m_ServerInitializedState.load(std::memory_order_acquire) == eServerInitializedState::NotStarted;
}

void FlashControlFactoryProxy::CrashFlashProcess() {
	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		m_ClientState.m_Quit = FactoryClientStateData::eQuit::QUIT_CRASH;
		SendCurrentStateToServer_HoldingMutex();
	}
}

bool FlashControlFactoryProxy::StartFlashControl(uint32_t iMeshId) {
	if (ShouldQuit())
		return false;

	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		m_aActiveMeshIds.push_back(iMeshId);
		size_t iCurrent = 1 + s_nCurrentMeshCount.fetch_add(1, std::memory_order_relaxed);
		std::atomic_thread_fence(std::memory_order_release);
		size_t iOldPeak = s_nPeakMeshCount.load(std::memory_order_relaxed);
		while (iCurrent > iOldPeak && !s_nPeakMeshCount.compare_exchange_weak(iOldPeak, iCurrent, std::memory_order_relaxed)) {
		}
		m_ClientState.m_aProviderIds.GetRefToUpdate().push_back(iMeshId);
	}
	SendCurrentStateToServer_HoldingMutex();
	return true;
}

bool FlashControlFactoryProxy::StopFlashControl(uint32_t iMeshId) {
	if (ShouldQuit())
		return false;

	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		auto itr = std::find(m_aActiveMeshIds.begin(), m_aActiveMeshIds.end(), iMeshId);
		if (itr == m_aActiveMeshIds.end())
			return false; // We aren't managing this control.

		s_nCurrentMeshCount.fetch_add(-1, std::memory_order_relaxed);
		m_aActiveMeshIds.erase(itr);

		// Since m_aActiveMeshIds can hold more elements than m_aProviderIds we would have to do
		// an erase then a possible push_back. It is simpler to just copy everything. (And since erasing is O(N) anyway, why not?)
		m_ClientState.m_aProviderIds.GetRefToUpdate().assign(m_aActiveMeshIds.begin(), m_aActiveMeshIds.end());
	}
	SendCurrentStateToServer_HoldingMutex();
	return true;
}

void FlashControlFactoryProxy::SendCurrentStateToServer_HoldingMutex() {
	uint32_t idx = m_ClientDataStateMgr.GetWorkAreaIndex();
	m_pSharedMemory->m_aClientData[idx].UpdateFrom(m_ClientState);
	m_ClientDataStateMgr.CompletedWork();
	::SetEvent(m_hNotifyServerEvent.GetHandle());
}

void FlashControlFactoryProxy::SignalRequestAvailable(uint32_t iMeshId) {
	if (IsStopped()) {
		Start();
		return;
	}

	// iMeshId is ignored. Current implementation uses a single event for the server.
	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		++m_ClientState.m_iIndividualRequestId;
		SendCurrentStateToServer_HoldingMutex();
	}
}

size_t FlashControlFactoryProxy::GetFlashProcessCrashCount() {
	return m_nFlashProcessCrashes;
}

size_t FlashControlFactoryProxy::GetPeakFlashInstanceCount() {
	return s_nPeakMeshCount.load(std::memory_order_relaxed);
}

void FlashControlFactoryProxy::SetQuit() {
	if (eServerInitializedState::Quitting == m_ServerInitializedState.exchange(eServerInitializedState::Quitting, std::memory_order_release))
		return; // Already quitting.

	// Tell server to quit.
	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		m_ClientState.m_Quit = FactoryClientStateData::eQuit::QUIT_NORMAL;
		SendCurrentStateToServer_HoldingMutex();
	}

	// Wake our thread so it will quit.
	::SetEvent(m_hClientEventHandle.GetHandle());
}

void FlashControlFactoryProxy::StopThread() {
	SetQuit();
	WaitForSingleObject(m_hServerCommThread.GetHandle(), INFINITE);
	m_hServerCommThread.Close();
}

bool FlashControlFactoryProxy::ShouldQuit() const {
	return m_ServerInitializedState.load(std::memory_order_relaxed) == eServerInitializedState::Quitting;
}

void FlashControlFactoryProxy::ProcessServerRequests() {
	if (m_ServerDataStateMgr.ClaimBuffer()) {
		uint32_t idx = m_ServerDataStateMgr.GetBufferIndex();
		const FactoryServerStateData* pServerState = &m_pSharedMemory->m_aServerData[idx];
		bool bVolumeChanged = pServerState->m_Volume.IsNewerThan(m_ServerState.m_Volume);
		bool bInitChanged = pServerState->m_InitData.IsNewerThan(m_ServerState.m_InitData);

		m_ServerState.UpdateFrom(*pServerState);
		if (bVolumeChanged) {
			if (m_volumeChangeCallback)
				m_volumeChangeCallback(m_ServerState.m_Volume.GetValue().m_fVolume, m_ServerState.m_Volume.GetValue().m_bMute);
		}

		if (bInitChanged) {
			m_strFlashVersion = m_ServerState.m_InitData.GetValue().m_strFlashVersion.GetString();
			m_bFlashIsInstalled = m_ServerState.m_InitData.GetValue().m_bFlashIsInstalled;
			if (m_bFlashIsInstalled)
				LogInfo("Flash is installed. Version = " << m_strFlashVersion);
			else
				LogInfo("Flash IS NOT installed");
		}
	}
}

bool FlashControlFactoryProxy::StartFlashServer() {
	// Initialize/Refresh shared memory area.
	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		m_pSharedMemory = new (m_pSharedMemory) FlashFactorySharedMemoryBlock;
		m_ServerDataStateMgr.SetSynchronizer(&m_pSharedMemory->m_ServerDataSynchronizer);
		m_ClientDataStateMgr.SetSynchronizer(&m_pSharedMemory->m_ClientDataSynchronizer);
		// Set initial server state.
		for (size_t i = 0; i < 3; ++i)
			m_pSharedMemory->m_aServerData[i].UpdateFrom(m_ServerState);

		// Set initial client state. Turn off crash command.
		m_ClientState.m_Quit = FactoryClientStateData::eQuit::DONT_QUIT;
		m_pSharedMemory->m_aClientData[m_ClientDataStateMgr.GetWorkAreaIndex()].UpdateFrom(m_ClientState);
		m_ClientDataStateMgr.CompletedWork();
	}

	bool bStarted = false;
#ifdef USE_FLASH_IN_PROCESS
	if (!bStarted) {
		LogInfo("Starting Flash server process");
		m_ServerProcess = StartServerProcess(m_iFactoryId);
		if (m_ServerProcess)
			bStarted = true;
	}
#endif

#ifdef USE_FLASH_IN_THREAD
	if (!bStarted) {
		LogInfo("Starting Flash server thread");
		m_ServerThread = StartServerThread(m_iFactoryId);
		if (m_ServerThread)
			bStarted = true;
	}
#endif

	bool bServerReady = false;
	if (bStarted) {
		// Wait for process to be ready.
		switch (WaitServer()) {
			case eWaitServerResult::Notify:
				break;
			case eWaitServerResult::ProcessStop:
			case eWaitServerResult::Error:
				// Server failed to initialize.
				SetQuit();
				break;
		}

		if (!ShouldQuit())
			ProcessServerRequests(); // To initialize flash availability and version string.

		if (m_bFlashIsInstalled) {
			m_ServerInitializedState.exchange(eServerInitializedState::Initialized, std::memory_order_acq_rel);
			bServerReady = true;
		}
	} else {
		SetQuit();
	}

	return bServerReady;
}

// This should only be called on our server management thread.
void FlashControlFactoryProxy::StopFlashServer(double dWaitFor) {
	// Tell server to quit.
	{
		std::lock_guard<std::mutex> lock(m_mtxClientState);
		m_ClientState.m_Quit = FactoryClientStateData::eQuit::QUIT_NORMAL;
		SendCurrentStateToServer_HoldingMutex();
	}

	// Wait for server to exit.
	if (m_ServerProcess) {
		if (dWaitFor > 0) {
			if (WaitForSingleObject(m_ServerProcess.GetHandle(), ceil(dWaitFor * 1000)) != WAIT_OBJECT_0) {
				LogError("Flash process not ending.");
				if (!TerminateProcess(m_ServerProcess.GetHandle(), 1))
					LogWarn("Could not terminate flash process.");
			}
		}
		m_ServerProcess.Close();
	}

	if (m_ServerThread) {
		// Not safe to terminate thread. Keep waiting for it.
		while (WaitForSingleObject(m_ServerThread.GetHandle(), ceil(dWaitFor * 1000)) != WAIT_OBJECT_0) {
			LogError("Flash server thread did not exit! Continuing wait..");
			dWaitFor = std::min<double>(dWaitFor, 10);
		}
		LogInfo("Flash server thread has exited.");
		m_ServerThread.Close();
	}
}

FlashControlFactoryProxy::eWaitServerResult FlashControlFactoryProxy::WaitServer() {
	HANDLE ahWaitHandles[] = {
		m_ServerProcess ? m_ServerProcess.GetHandle() : m_ServerThread.GetHandle(),
		m_hClientEventHandle.GetHandle(),
	};
	size_t nWaitHandles = std::extent<decltype(ahWaitHandles)>::value;

	DWORD iEventId = WaitForMultipleObjects(nWaitHandles, ahWaitHandles, FALSE, INFINITE);
	switch (iEventId) {
		case WAIT_OBJECT_0:
			return eWaitServerResult::ProcessStop;

		case WAIT_OBJECT_0 + 1:
			return eWaitServerResult::Notify;

		default:
			return eWaitServerResult::Error;
	}
}

DWORD WINAPI FlashControlFactoryProxy::StaticServerCommThreadFunc(void* p) {
	return static_cast<FlashControlFactoryProxy*>(p)->ServerCommThreadFunc();
}

DWORD FlashControlFactoryProxy::ServerCommThreadFunc() {
	SetThreadName("Flash client");

	const double kdMinRestartIntervalSeconds = 15.0; // Time to wait between restarting each flash process.
	const double kdMultipleFailureIntervalSeconds = 15.0 * 60.0;
	const size_t knMaxMultipleFailureCount = 5; // No more than this many failures per kdMultipleFailureInterval.
	std::deque<double> adStartTimes; // Most recent start is in the front.

	while (!ShouldQuit()) {
		// Calculate at what time we should restart the server to satisfy the restart interval requirements.
		double dCurrentTime = GetCurrentTimeInSeconds();
		double dTimeToStart = dCurrentTime;
		if (!adStartTimes.empty())
			dTimeToStart = adStartTimes.front() + kdMinRestartIntervalSeconds;
		if (adStartTimes.size() >= knMaxMultipleFailureCount)
			dTimeToStart = std::max<double>(dTimeToStart, adStartTimes.back() + kdMultipleFailureIntervalSeconds);

		// Wait a minimum time between server restarts.
		while (!ShouldQuit()) {
			double dWaitTime = dTimeToStart - GetCurrentTimeInSeconds();
			if (dWaitTime <= 0.01)
				break;
			LogInfo("Waiting " << dWaitTime << " seconds before restarting the Flash server.");
			DWORD dwWaitResult = WaitForSingleObject(m_hClientEventHandle.GetHandle(), ceil(dWaitTime * 1000));
			if (dwWaitResult != WAIT_OBJECT_0 && dwWaitResult != WAIT_TIMEOUT) {
				LogError("Wait error");
				SetQuit();
			}
		}

		// Start server.
		if (!ShouldQuit()) {
			if (!StartFlashServer())
				SetQuit();
		}

		// Signal those waiting for us to start that we did so (or failed to).
		SetEvent(m_InitializedEvent.GetHandle());

		// Add this start time to the list.
		// The time we use is when we began starting the server, not from when the server completed initializing.
		adStartTimes.push_front(dCurrentTime);
		adStartTimes.resize(std::min<size_t>(adStartTimes.size(), knMaxMultipleFailureCount));

		// Process communication loop.
		while (!ShouldQuit()) {
			ProcessServerRequests();

			if (ShouldQuit())
				break;

			eWaitServerResult waitResult = WaitServer();

			if (waitResult == eWaitServerResult::Notify) {
			} else if (waitResult == eWaitServerResult::ProcessStop) {
				if (!ShouldQuit())
					++m_nFlashProcessCrashes;
				LogError("Flash process exited.");
				break;
			} else {
				// Error waiting. Event destroyed?
				if (waitResult != eWaitServerResult::Error)
					LogError("Unexpected wait result");
				SetQuit();
				break;
			}
		}
	}

	static const double kdServerExitTimeout;
	StopFlashServer(kdServerExitTimeout);

	return 0;
}

} // namespace FlashControl
} // namespace KEP
