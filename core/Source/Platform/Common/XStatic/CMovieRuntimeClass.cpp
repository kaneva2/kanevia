///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include <mmsystem.h>
#include "mmstream.h"
#include "amstream.h"
#include "ddstream.h"
#include "dsound.h"
#include "CMovieRuntimeClass.h"

namespace KEP {

CMovieRuntimeObj::CMovieRuntimeObj(BOOL L_playing,
	BOOL L_looping,
	CStringA L_fileName,
	IDirectDrawSurface7* L_pSurface4Loc,
	IDirectDrawStreamSample* L_pSampleLoc,
	IAMMultiMediaStream* L_pSStreamLoc,
	IAMMultiMediaStream* L_pAMStreamLoc,
	IMediaStream* L_pPrimaryVidStreamLoc,
	IDirectDrawMediaStream* L_pDDStreamLoc,
	RECT L_rectMov,
	RECT L_changingRect) {
	m_changingRect = L_changingRect;
	m_looping = L_looping;
	m_playing = L_playing;
	m_fileName = L_fileName;
	m_pSurface4Loc = L_pSurface4Loc;
	m_pSampleLoc = L_pSampleLoc;
	m_pSStreamLoc = L_pSStreamLoc;
	m_pAMStreamLoc = L_pAMStreamLoc;
	m_pPrimaryVidStreamLoc = L_pPrimaryVidStreamLoc;
	m_pDDStreamLoc = L_pDDStreamLoc;
	m_rectMov = L_rectMov;
}

void CMovieRuntimeObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_fileName
		   << m_looping
		   << m_rectMov.top
		   << m_rectMov.left
		   << m_rectMov.right
		   << m_rectMov.bottom;

	} else {
		ar >> m_fileName >> m_looping >> m_rectMov.top >> m_rectMov.left >> m_rectMov.right >> m_rectMov.bottom;

		m_pSurface4Loc = 0;
		m_pSampleLoc = 0;
		m_pSStreamLoc = 0;
		m_pAMStreamLoc = 0;
		m_pPrimaryVidStreamLoc = 0;
		m_pDDStreamLoc = 0;
		m_playing = FALSE;
		m_changingRect.top = 10;
		m_changingRect.bottom = 20;
		m_changingRect.right = 20;
		m_changingRect.left = 10;
	}
}

IMPLEMENT_SERIAL(CMovieRuntimeObj, CObject, 0)
IMPLEMENT_SERIAL(CMovieRuntimeObjList, CObList, 0)

BEGIN_GETSET_IMPL(CMovieRuntimeObj, IDS_MOVIERUNTIMEOBJ_NAME)
GETSET_MAX(m_fileName, gsCString, GS_FLAGS_DEFAULT, 0, 0, MOVIERUNTIMEOBJ, FILENAME, STRLEN_MAX)
GETSET(m_looping, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MOVIERUNTIMEOBJ, LOOPING)
GETSET(m_rectMov, gsRect, GS_FLAGS_DEFAULT, 0, 0, MOVIERUNTIMEOBJ, RECTMOV)
END_GETSET_IMPL

} // namespace KEP