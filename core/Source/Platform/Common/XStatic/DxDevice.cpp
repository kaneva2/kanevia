///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <string>
#include <dxdiag.h>
#include <D3D9.h>
#include "DxDevice.h"
#include "RenderEngine/Utils/ReUtils.h"
#include "KEP/Images/KEPImages.h"
#include "Common\KEPUtil\Helpers.h"
#include "Common\KEPUtil\CrashReporter.h"
#include "LogHelper.h"

using namespace std;

namespace KEP {

static LogInstance("Instance");

// DRF - Warning - This should be a DLL not a static library!
DxDevice* DxDevice::Instance = NULL; // Singleton Instance

void DxDevice::TextureMemoryStats::Log() {
	LogInfo("texMemAvailMB=" << m_avail / BYTES_PER_MB << " [" << m_min / BYTES_PER_MB << " " << m_avg / BYTES_PER_MB << " " << m_max / BYTES_PER_MB << "]"
							 << " texMemUsageMB=" << m_usedVid / BYTES_PER_MB
							 << " texMemExpVidMB=" << m_expVid / BYTES_PER_MB
							 << " texMemExpSysMB=" << m_expSys / BYTES_PER_MB);
}

void DxDevice::SetDevice(LPDIRECT3DDEVICE9 pDevice) {
	// Set GlobalD3DDevice
	::GlobalD3DDevice(pDevice);

	// Reset Texture Memory Statistics
	GetTextureMemoryStats(true);
}

DxDevice* DxDevice::Ptr() {
	if (!Instance)
		Instance = new DxDevice;
	return Instance;
}

DxDevice::TextureMemoryStats DxDevice::GetTextureMemoryStats(bool resetStats) {
	// Valid GlobalD3DDevice ?
	auto pDevice = ::GlobalD3DDevice();
	if (!pDevice)
		resetStats = true;

	{
		std::lock_guard<fast_mutex> lock(m_mtxTms);
		// Reset Statistics ?
		if (resetStats)
			m_tms.Reset();

		// Update Statistics (ignore 0 - out of view)
		if (pDevice)
			m_tms.m_avail = pDevice->GetAvailableTextureMem();
		if (m_tms.m_avail > 0) {
			UPDATE_NUM(m_tms.m_num);
			UPDATE_AVG(m_tms.m_avg, m_tms.m_num, m_tms.m_avail);
			UPDATE_MIN(m_tms.m_min, m_tms.m_avail);
			UPDATE_MAX(m_tms.m_max, m_tms.m_avail);
		}

		return m_tms;
	}
}

bool DxDevice::IsInView() {
	// Valid GlobalD3DDevice ?
	auto pDevice = ::GlobalD3DDevice();
	if (!pDevice)
		return false;

	// Is Client Window In View ?
	static bool isInViewWas = false;
	bool tclOk = (pDevice->TestCooperativeLevel() == D3D_OK);
	bool isInView = tclOk && GetTextureMemoryStats().m_avail;
	if (isInViewWas != isInView) {
		isInViewWas = isInView;
		LogInfo(LogBool(isInView));
	}
	return isInView;
}

DxDevice::eCreateTextureResult DxDevice::HResultFailedOutOfView(HRESULT hr, const string& msg, bool sleepOutOfView) {
	// Result Failed ?
	bool failed = FAILED(hr);
	if (!failed)
		return eCreateTextureResult::SUCCESS;

	// In View - Log As Error
	if (IsInView()) {
		LogError(msg << " - " << DXGetErrorDescription9A(hr));
		return eCreateTextureResult::FAIL;
	}

	// Out Of View - Log As Warning And Sleep Thread Until In View
	if (sleepOutOfView) {
		LogWarn("Out Of View - Sleeping...");
		while (!IsInView())
			fTime::SleepSec(1.0);
		LogWarn("In View - Continuing");
	}
	return eCreateTextureResult::RETRY;
}

bool DxDevice::GetSurfaceDesc(const LPDIRECT3DTEXTURE9& pTex, D3DSURFACE_DESC& texSurfaceDesc_out) {
	// Get Texture Surface
	IDirect3DSurface9* pSurface;
	if (!pTex || (D3D_OK != pTex->GetSurfaceLevel(0, &pSurface)) || !pSurface)
		return false;

	// Get Texture Surface Description
	bool ok = (D3D_OK == pSurface->GetDesc(&texSurfaceDesc_out));
	pSurface->Release();
	return ok;
}

DxDevice::eCreateTextureResult DxDevice::CreateTextureFromFileInMemoryEx(
	const string& name,
	LPCVOID pSrcData,
	UINT srcDataSize,
	LPDIRECT3DTEXTURE9* ppTex_out,
	D3DSURFACE_DESC& texSurfaceDesc_out,
	D3DRESOURCETYPE resourcetype,
	bool useTexLod,
	UINT width,
	UINT height,
	D3DCOLOR colorKey,
	bool forceFullRes,
	bool transcodeJPG,
	bool retryOutOfView) {
	// Reset Return Values
	(*ppTex_out) = nullptr;
	memset(&texSurfaceDesc_out, 0, sizeof(D3DSURFACE_DESC));

	// Valid GlobalD3DDevice ?
	auto pDevice = ::GlobalD3DDevice();
	if (!pDevice)
		return eCreateTextureResult::FAIL;

	// Texture Memory Limits
	const UINT TEXTURE_MEMORY_AVAIL_WARN = (65 * BYTES_PER_MB); // downsize textures to prevent error
	const UINT TEXTURE_MEMORY_AVAIL_ERROR = (5 * BYTES_PER_MB); // ignore textures to avoid crash

	// Texture Memory Error ? (ignore texture, 0=outOfView)
	auto tms = GetTextureMemoryStats();
	bool isInView = IsInView() && (tms.m_avail > 0);
	bool memError = isInView && (tms.m_avail < TEXTURE_MEMORY_AVAIL_ERROR);
	if (memError) {
		LogError("OUT OF MEMORY (availTexMemory=" << tms.m_avail / BYTES_PER_MB << " mb) - Ignoring Texture '" << name << "'");
		return eCreateTextureResult::FAIL;
	}

	// Transcode JPG ?
	std::unique_ptr<unsigned char> dxtBuf;
	if (transcodeJPG && (detectImageFormat((const unsigned char*)pSrcData, srcDataSize) == "JPG")) {
		ImageInfo info;
		info.pixelFormat = D3DFMT_X8R8G8B8;
		unsigned int pixBufSize = 0;
		std::unique_ptr<unsigned char> pixBuf(decodeJPG((unsigned char*)pSrcData, srcDataSize, info, pixBufSize));
		if (!pixBuf || pixBufSize == 0) {
			LogError("OUT OF MEMORY (availTexMemory=" << tms.m_avail / BYTES_PER_MB << " mb) while decoding JPG - Ignoring Texture '" << name << "'");
			return eCreateTextureResult::FAIL;
		}

		unsigned int dxtBufSize = 0;
		dxtBuf = std::unique_ptr<unsigned char>(encodeDDS_Fast(pixBuf.get(), pixBufSize, info, dxtBufSize));
		if (!dxtBuf || dxtBufSize == 0) {
			LogError("OUT OF MEMORY (availTexMemory=" << tms.m_avail / BYTES_PER_MB << " mb) while encoding DXT1 - Ignoring Texture '" << name << "'");
			return eCreateTextureResult::FAIL;
		}

		pSrcData = dxtBuf.get();
		srcDataSize = dxtBufSize;
	}

	// Create Texture
	D3DXIMAGE_INFO iInfo;
	memset(&iInfo, 0, sizeof(D3DXIMAGE_INFO));
	HRESULT hr;
	LPDIRECT3DTEXTURE9 pTex_tmp;
	while (true) {
		hr = D3DXCreateTextureFromFileInMemoryEx(
			pDevice, // Pointer to the D3D device.
			pSrcData, // Pointer to the string containing the source filename.
			srcDataSize, // Size, in bytes, of the file.
			width, // Number of pixels wide.
			height, // Number of pixels high.
			D3DX_DEFAULT, // Number of mips to generate.
			0, // How the texture is to be used.
			D3DFMT_UNKNOWN, // Pixel format of the texture.
			D3DPOOL_MANAGED, // Location in memory to place the texture.
			D3DX_DEFAULT, // Bit flags of how the image is to be filtered.
			D3DX_DEFAULT, // Bit flags of how the mips are to be filtered.
			colorKey, // Color key value (0 is diable).
			&iInfo, // Structure to contain the image information.
			nullptr, // Pointer to the pallete.
			&pTex_tmp // Pointer to the texture generated
		);
		if (eCreateTextureResult::RETRY != HResultFailedOutOfView(hr, "D3DXCreateTextureFromFileInMemoryEx() FAILED", retryOutOfView)) {
			break;
		} else {
			if (!retryOutOfView)
				return eCreateTextureResult::RETRY;
		}
	}
	if (FAILED(hr)) {
		LogError("D3DXCreateTextureFromFileInMemoryEx() FAILED - '" << name << "'");
		return eCreateTextureResult::FAIL;
	}

	// Downsize Texture ?
	auto ams = ::GetAppMemoryStats();
	auto sysMemAvail = ams.m_statex.ullAvailVirtual;
	tms = GetTextureMemoryStats();
	auto tmsNeedVid = (tms.m_usedVid > tms.m_expVid) ? 0 : (tms.m_expVid - tms.m_usedVid);
	auto tmsNeedSys = (tms.m_usedVid > tms.m_expSys) ? 0 : (tms.m_expSys - tms.m_usedVid);
	bool memWarn = isInView && (tms.m_avail < TEXTURE_MEMORY_AVAIL_WARN);
	bool memDownVid = isInView && (tms.m_avail < tmsNeedVid);
	bool memDownSys = isInView && (sysMemAvail < tmsNeedSys);
	if (!forceFullRes && (memWarn || memDownVid || memDownSys)) {
		if (memWarn) {
			LogWarn("VID MEMORY LOW (vidMemAvail=" << tms.m_avail / BYTES_PER_MB << "mb) - Downsizing '" << name << "'");
			CrashReporter::SetErrorData("vidMemLowMB", tms.m_avail / BYTES_PER_MB);
		} else if (memDownVid) {
			LogWarn("VID DOWNSIZE (vidMemAvail=" << tms.m_avail / BYTES_PER_MB << "mb vidMemNeed=" << tmsNeedVid / BYTES_PER_MB << "mb) - Downsizing '" << name << "'");
			CrashReporter::SetErrorData("vidMemAvailMB", tms.m_avail / BYTES_PER_MB);
			CrashReporter::SetErrorData("vidMemNeedMB", tmsNeedVid / BYTES_PER_MB);
			CrashReporter::SetErrorData("vidMemExpMB", tms.m_expVid / BYTES_PER_MB);
			CrashReporter::SetErrorData("vidMemUsedMB", tms.m_usedVid / BYTES_PER_MB);
		} else if (memDownSys) {
			LogWarn("SYS DOWNSIZE (sysMemAvail=" << sysMemAvail / BYTES_PER_MB << "mb sysMemNeed=" << tmsNeedSys / BYTES_PER_MB << "mb) - Downsizing '" << name << "'");
			CrashReporter::SetErrorData("sysMemAvailMB", sysMemAvail / BYTES_PER_MB);
			CrashReporter::SetErrorData("sysMemNeedMB", tmsNeedSys / BYTES_PER_MB);
			CrashReporter::SetErrorData("sysMemExpMB", tms.m_expSys / BYTES_PER_MB);
			CrashReporter::SetErrorData("vidMemUsedMB", tms.m_usedVid / BYTES_PER_MB);
		}
		while (true) {
			hr = DownsizeTexture(pTex_tmp, ppTex_out);
			if (eCreateTextureResult::RETRY != HResultFailedOutOfView(hr, "DownsizeTexture() FAILED", retryOutOfView)) {
				break;
			} else {
				if (!retryOutOfView)
					return eCreateTextureResult::RETRY;
			}
		}
	} else {
		pTex_tmp->AddRef();
		(*ppTex_out) = pTex_tmp;
	}

	// Release Temp Texture
	if (pTex_tmp)
		pTex_tmp->Release();

	// Get Surface Description
	if (!GetSurfaceDesc(*ppTex_out, texSurfaceDesc_out))
		return eCreateTextureResult::FAIL;

	// Increment Texture Memory Stats Usage
	auto texUsage = GetTextureMemoryUsage(texSurfaceDesc_out);
	{
		std::lock_guard<fast_mutex> lock(m_mtxTms);
		m_tms.m_usedVid += texUsage;
	}

	return eCreateTextureResult::SUCCESS;
}

bool DxDevice::UnloadTexture(LPDIRECT3DTEXTURE9 pTex, D3DSURFACE_DESC& texSurfaceDesc_out) {
	// Decrement Texture Memory Stats Usage
	auto texUsage = GetTextureMemoryUsage(texSurfaceDesc_out);
	{
		std::lock_guard<fast_mutex> lock(m_mtxTms);
		m_tms.m_usedVid = (texUsage > m_tms.m_usedVid) ? 0 : (m_tms.m_usedVid - texUsage);
	}

	memset(&texSurfaceDesc_out, 0, sizeof(D3DSURFACE_DESC));

	return true;
}

HRESULT DxDevice::DownsizeTexture(const LPDIRECT3DTEXTURE9& pTex_in, LPDIRECT3DTEXTURE9* ppTex_out) {
	// Valid GlobalD3DDevice ?
	auto pDevice = ::GlobalD3DDevice();
	if (!pDevice)
		return E_FAIL;

	// See how many mipmaps we have, take a low-res one if we need to reduce texture memory
	// and create a new texture with only this low-res surface then discard the full texture.
	int srcLevel = ((int)pTex_in->GetLevelCount()) - 1;
	if (srcLevel <= 0) {
		pTex_in->GenerateMipSubLevels();
		srcLevel = ((int)pTex_in->GetLevelCount()) - 1;
	}

	const int MAX_LEVELS = 7; // The output texture will have no more than MAX_LEVELS mipmaps.
	int dstLevel;
	if (srcLevel >= MAX_LEVELS) {
		dstLevel = MAX_LEVELS;
	} else {
		// The source texture is already as
		// small or smaller than we want,
		// so there's no need to copy anything.
		//
		// Just make ppTex point to a "temporary"
		// texture, which is the one loaded from
		// memory, and ensure that the caller's
		// Release() doesn't actually release the
		// texture (which should wait until the user
		// of *ppTex calls Release() instead).
		//
		pTex_in->AddRef();
		(*ppTex_out) = pTex_in;
		return S_OK;
	}

	// copy the surfaces
	HRESULT hr = S_OK;
	for (; dstLevel >= 0 && srcLevel >= 0; --dstLevel, --srcLevel) {
		// get the source surface for this level
		IDirect3DSurface9* pSurfaceSrc;
		hr = pTex_in->GetSurfaceLevel(srcLevel, &pSurfaceSrc);
		if (FAILED(hr)) {
			if (*ppTex_out)
				(*ppTex_out)->Release();
			return D3DCheck(hr);
		}

		// If we don't have one, I need to create
		// the texture into which the surfaces will
		// be copied.  Should just be done the first
		// time through.  I do it here to make use
		// of the pSurfaceSrc, which needs to
		// be made available for each level anyway.
		if ((*ppTex_out) == nullptr) {
			// get the source surface description, which can be
			// used to create the initial texture
			D3DSURFACE_DESC desc;
			hr = pSurfaceSrc->GetDesc(&desc);
			if (FAILED(hr)) {
				return D3DCheck(hr);
			}

			// create a new, probably smaller texture
			hr = D3DXCreateTexture(
				pDevice,
				(int)pow(2.0, dstLevel),
				(int)pow(2.0, dstLevel),
				D3DX_DEFAULT,
				desc.Usage,
				desc.Format,
				desc.Pool,
				ppTex_out);
			if (FAILED(hr)) {
				return D3DCheck(hr);
			}
		}

		// get the destination surface for this level
		IDirect3DSurface9* pSurfaceDst;
		hr = (*ppTex_out)->GetSurfaceLevel(dstLevel, &pSurfaceDst);
		if (FAILED(hr)) {
			(*ppTex_out)->Release();
			pSurfaceSrc->Release();
			return D3DCheck(hr);
		}

		// make the copy
		hr = D3DXLoadSurfaceFromSurface(
			pSurfaceDst,
			NULL,
			NULL,
			pSurfaceSrc,
			NULL,
			NULL,
			D3DX_DEFAULT,
			0);
		if (FAILED(hr)) {
			(*ppTex_out)->Release();
			pSurfaceDst->Release();
			pSurfaceSrc->Release();
			return D3DCheck(hr);
		}

		// release the surfaces
		pSurfaceSrc->Release();
		pSurfaceDst->Release();
	}
	return hr;
}

} // namespace KEP