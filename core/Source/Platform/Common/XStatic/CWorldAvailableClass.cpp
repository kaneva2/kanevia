///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <d3d9.h>
#include "resource.h"
#include <afxtempl.h>
#include "CWorldAvailableClass.h"

#include <jsEnumFiles.h> // for jsExists

#include <TinyXML/tinyxml.h>
#include <KEPException.h>
#include "KEPConfigurationsXML.h"
#include <sstream>

namespace KEP {

#define DEFAULT_MAX_OCCUPANCY 30
#define DEFAULT_VISIBILITY 100
CWorldAvailableObj::CWorldAvailableObj() :
		m_aiServer(0), m_maxOccupancy(DEFAULT_MAX_OCCUPANCY), m_visibility(DEFAULT_VISIBILITY), m_index(-1) {
	m_worldName = "None";
	m_channel.clear();
}

void CWorldAvailableObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_worldName
		   << m_channel
		   << m_maxOccupancy;
	} else {
		int schema = ar.GetObjectSchema();

		ar >> m_worldName >> m_channel;

		if (schema >= 2)
			ar >> m_maxOccupancy;
		else
			m_maxOccupancy = DEFAULT_MAX_OCCUPANCY;

		ASSERT(m_maxOccupancy > 0);
		if (m_maxOccupancy <= 0)
			m_maxOccupancy = DEFAULT_MAX_OCCUPANCY;

		//m_channel = 0;
	}
}

LONG CWorldAvailableObjList::FindAvailableId() {
	int i = 0;
	for (; i < GetCount(); i++) {
		bool found = false;
		POSITION posLoc = GetHeadPosition();
		while (posLoc != NULL) {
			CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
			if (wPtr->m_index == i) {
				found = true;
				break; // found it
			}
		}
		if (!found)
			return i; // didn't find this one
	}
	return i;
}

//We need the wldName in case we have to add a new WorldAvailableObj if its not found
ChannelId CWorldAvailableObjList::AddWorldIfAvailableByIndex(LONG index, const CStringA& wldName) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
		if (wPtr->m_index == index) {
			// should never have anything but channel
			ASSERT(wPtr->m_channel.GetInstanceId() == 0 &&
				   wPtr->m_channel.GetType() == 0);
			wPtr->m_channel.clearInstanceAndType();
			return wPtr->m_channel;
		}
	}

	ASSERT(FALSE); //wid this could be a problem creating a new channel w/o checking

	CWorldAvailableObj* wPtr = new CWorldAvailableObj();
	wPtr->m_worldName = wldName;
	wPtr->m_index = FindAvailableId();
	AddTail(wPtr);
	return ChannelId(wPtr->m_index, 0, 0); // basically create a new instance
}

ChannelId CWorldAvailableObjList::AddWorldIfAvailable(const CStringA& wldName) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
		if (wPtr->m_worldName.CompareNoCase(wldName) == 0) {
			// should never have anything but channel
			ASSERT(wPtr->m_channel.GetInstanceId() == 0 &&
				   wPtr->m_channel.GetType() == 0);
			wPtr->m_channel.clearInstanceAndType();
			return wPtr->m_channel;
		}
	}

	ASSERT(FALSE); //wid this could be a problem creating a new channel w/o checking

	CWorldAvailableObj* wPtr = new CWorldAvailableObj();
	wPtr->m_worldName = wldName;
	wPtr->m_index = FindAvailableId();
	AddTail(wPtr);
	return ChannelId(wPtr->m_index, 0, 0); // basically create a new instance
}

void CWorldAvailableObjList::AddNewWorld(LONG index, const char* wldName, const char* dispName, UINT maxOccupancy) {
	CWorldAvailableObj* wPtr = new CWorldAvailableObj();
	wPtr->m_worldName = wldName;
#ifndef REFACTOR_INSTANCEID
	wPtr->m_index = wPtr->m_channel = index;
#else
	wPtr->m_index = index;
	wPtr->m_channel.fromLong(index);
#endif
	wPtr->m_displayName = dispName;
	wPtr->m_maxOccupancy = maxOccupancy;
	AddTail(wPtr);
}

ZoneIndex CWorldAvailableObjList::GetZeroBasedIndex(const CStringA& wldName) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
		if (wPtr->m_worldName == wldName)
			return ZoneIndex(wPtr->m_index, 0, 0);
	}
	return INVALID_ZONE_INDEX; //world not found
}

void CWorldAvailableObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
		delete wPtr;
		wPtr = 0;
	}
	RemoveAll();
}

CWorldAvailableObj* CWorldAvailableObjList::GetByZoneIndex(const ZoneIndex& index) {
	if (index == INVALID_ZONE_INDEX)
		return NULL;

	LONG zi = index.zoneIndex();

	return GetByIndex(zi);
}

CWorldAvailableObj* CWorldAvailableObjList::GetByIndex(LONG zi) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
		if (wPtr->m_index == zi)
			return wPtr;
	}

	return NULL;
}

void CWorldAvailableObjList::DeleteAndRemove(LONG index) {
	POSITION posLast = 0;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
		if (wPtr->m_index == index) {
			delete wPtr;
			RemoveAt(posLast);
		}
	}
}

///---------------------------------------------------------
// CWorldAvailableObjList::SerializeFromXML
//
// [in] fname
//
void CWorldAvailableObjList::SerializeFromXML(const std::string& fname) {
	bool xmlExists = FileHelper::Exists(fname);
	if (xmlExists) {
		TiXmlDocument dom;
		if (dom.LoadFile(fname)) {
			TiXmlElement* root = dom.FirstChildElement(WORLDS_TAG);
			if (root) {
				TiXmlNode* node;
				TiXmlElement* child = 0;

				// Clear any previously loaded items
				if (this) {
					this->SafeDelete();

					LONG index = 0;
					while ((node = root->IterateChildren(WORLD_TAG, child)) != 0 && (child = node->ToElement()) != 0) {
						TiXmlText* nameStr = TiXmlHandle(child->FirstChild(WORLD_NAME_TAG)).FirstChild().Text();
						TiXmlText* dispNameStr = TiXmlHandle(child->FirstChild(WORLD_DISPNAME_TAG)).FirstChild().Text();
						TiXmlText* idInt = TiXmlHandle(child->FirstChild(ID_TAG)).FirstChild().Text();
						TiXmlText* maxOccupancyInt = TiXmlHandle(child->FirstChild(MAX_OCCUPANCY_TAG)).FirstChild().Text();
						TiXmlText* visibilityInt = TiXmlHandle(child->FirstChild(VISIBILITY_TAG)).FirstChild().Text();
						TiXmlText* zoneIndexInt = TiXmlHandle(child->FirstChild(ZONE_ID_TAG)).FirstChild().Text();

						CStringA s, ds;
						int i = 0;
						int maxOcc = DEFAULT_MAX_OCCUPANCY;
						int vis = DEFAULT_VISIBILITY;
						int zi = index;
						if (dispNameStr)
							ds = dispNameStr->Value();
						if (nameStr)
							s = nameStr->Value();
						if (idInt)
							i = atoi(idInt->Value());
						if (maxOccupancyInt)
							maxOcc = atoi(maxOccupancyInt->Value());
						if (visibilityInt)
							vis = atoi(visibilityInt->Value());
						if (zoneIndexInt) // use zone_index from XML, if have it otherwise, just use index as before
							zi = atoi(zoneIndexInt->Value());

						CWorldAvailableObj* obj = new CWorldAvailableObj();
						obj->m_channel.fromLong(i);
						obj->m_worldName = s;
						if (ds.IsEmpty())
							obj->m_displayName = s;
						else
							obj->m_displayName = ds;
						obj->m_maxOccupancy = std::max(maxOcc, 1);
						obj->m_visibility = vis;
						obj->m_index = zi;

						AddTail(obj);
						index++;
					}
				}
				return;
			}
		}
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}
}

void CWorldAvailableObjList::SerializeToXML(const std::string& fname) {
#ifdef _ClientOutput // For client/editor only
	TiXmlDocument dom(fname);
	TiXmlElement root(WORLDS_TAG);
	TiXmlNode* rootNode = dom.InsertEndChild(root);
	if (rootNode == 0) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}

	char s[34];

	for (POSITION pos = this->GetHeadPosition(); pos != NULL;) {
		CWorldAvailableObj* obj = (CWorldAvailableObj*)GetNext(pos);

		TiXmlElement child(WORLD_TAG);

		TiXmlElement nameStr(WORLD_NAME_TAG);
		TiXmlText text(obj->m_worldName);
		if (nameStr.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement dispNameStr(WORLD_DISPNAME_TAG);
		TiXmlText text2(obj->m_displayName);
		if (dispNameStr.InsertEndChild(text2) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement idInt(ID_TAG);
		_ltoa_s(obj->m_channel.toLong(), s, _countof(s), 10);
		text.SetValue(s);
		if (idInt.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement ziInt(ZONE_ID_TAG);
		_ltoa_s(obj->m_index, s, _countof(s), 10);
		text.SetValue(s);
		if (ziInt.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement maxOccupancyInt(MAX_OCCUPANCY_TAG);
		_ltoa_s(obj->m_maxOccupancy, s, _countof(s), 10);
		text.SetValue(s);
		if (maxOccupancyInt.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement visibilityInt(VISIBILITY_TAG);
		_ltoa_s(obj->m_visibility, s, _countof(s), 10);
		text.SetValue(s);
		if (visibilityInt.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		if (child.InsertEndChild(ziInt) == 0 ||
			child.InsertEndChild(idInt) == 0 ||
			child.InsertEndChild(maxOccupancyInt) == 0 ||
			child.InsertEndChild(nameStr) == 0 ||
			child.InsertEndChild(dispNameStr) == 0 ||
			child.InsertEndChild(visibilityInt) == 0 ||
			rootNode->InsertEndChild(child) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}
	}

	if (!dom.SaveFile()) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}
#else
	throw new KEPException("CWorldAvailableObjList::SerializeToXML disabled for XStaticServer", 0, __FILE__, __LINE__);
#endif
}

CSpawnCfgObj::CSpawnCfgObj() :
		m_id(0) {
	m_position.x = 0.0f;
	m_position.y = 0.0f;
	m_position.z = 0.0f;
	m_zoneIndex.clear();
	m_spawnPointName = "NONE";
	m_rotation = 0;
}

void CSpawnCfgObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_zoneIndex
		   << m_position.x
		   << m_position.y
		   << m_position.z
		   << m_spawnPointName
		   << m_rotation;
	} else {
		int schema = ar.GetObjectSchema();

		ar >> m_zoneIndex >> m_position.x >> m_position.y >> m_position.z >> m_spawnPointName;
		if (schema > 1)
			ar >> m_rotation;
		else
			m_rotation = 0;
	}
}

void CSpawnCfgObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSpawnCfgObj* eventPtr = (CSpawnCfgObj*)GetNext(posLoc);
		delete eventPtr;
		eventPtr = 0;
	}
	RemoveAll();
}

void CSpawnCfgObjList::SerializeFromXML(const std::string& fname) {
	bool xmlExists = FileHelper::Exists(fname);
	if (xmlExists) {
		TiXmlDocument dom;
		if (dom.LoadFile(fname)) {
			TiXmlElement* root = dom.FirstChildElement(SPAWNS_TAG);
			if (root) {
				TiXmlNode* node;
				TiXmlElement* child = 0;

				// Clear any previously loaded items
				if (this) {
					this->SafeDelete();

					while ((node = root->IterateChildren(SPAWN_TAG, child)) != 0 && (child = node->ToElement()) != 0) {
						TiXmlText* nameStr = TiXmlHandle(child->FirstChild(SPAWN_NAME_TAG)).FirstChild().Text();
						TiXmlText* idRefInt = TiXmlHandle(child->FirstChild(REFID_TAG)).FirstChild().Text();
						TiXmlText* posXFloat = TiXmlHandle(child->FirstChild(POS_X)).FirstChild().Text();
						TiXmlText* posYFloat = TiXmlHandle(child->FirstChild(POS_Y)).FirstChild().Text();
						TiXmlText* posZFloat = TiXmlHandle(child->FirstChild(POS_Z)).FirstChild().Text();
						TiXmlText* idRotInt = TiXmlHandle(child->FirstChild(ROTATION_TAG)).FirstChild().Text();

						CStringA s;
						int i = 0;
						int rot = 0;
						float x = 0;
						float y = 0;
						float z = 0;

						if (nameStr)
							s = nameStr->Value();
						if (idRefInt)
							i = atoi(idRefInt->Value());
						if (posXFloat)
							x = atof(posXFloat->Value());
						if (posYFloat)
							y = atof(posYFloat->Value());
						if (posZFloat)
							z = atof(posZFloat->Value());
						if (idRotInt) {
							// eventually this is divide by 2
							// and sent to client in a byte, so
							// we scrub a bit here.  (wacky, but constitent with NPC impl of rotation)
							rot = atoi(idRotInt->Value());
							if (rot > 359 || rot < -179)
								rot = 0;
							else if (rot > 180)
								rot = rot - 360; // make it negative
						}

						CSpawnCfgObj* obj = new CSpawnCfgObj();

						obj->m_spawnPointName = s;
						obj->m_zoneIndex.fromLong(i);
						obj->m_zoneIndex.clearInstance(); // never read instance from file
						obj->m_position.x = x;
						obj->m_position.y = y;
						obj->m_position.z = z;
						obj->m_rotation = rot;

						AddTail(obj);
					}
				}
				return;
			}
		}
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}
}

void CSpawnCfgObjList::SerializeToXML(const std::string& fname) {
#ifdef _ClientOutput // For client/editor only
	TiXmlDocument dom(fname);
	TiXmlElement root(SPAWNS_TAG);
	TiXmlNode* rootNode = dom.InsertEndChild(root);
	if (rootNode == 0) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}

	char buf[34];

	for (POSITION pos = this->GetHeadPosition(); pos != NULL;) {
		CSpawnCfgObj* obj = (CSpawnCfgObj*)GetNext(pos);

		TiXmlElement child(SPAWN_TAG);

		TiXmlElement nameStr(SPAWN_NAME_TAG);
		TiXmlText text(obj->m_spawnPointName);
		if (nameStr.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement idRefInt(REFID_TAG);
		_ltoa_s(obj->m_zoneIndex.toLong(), buf, _countof(buf), 10);
		text.SetValue(buf);

		if (idRefInt.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement posXFloat(POS_X);
		std::stringstream s1;
		s1 << obj->m_position.x;
		text.SetValue(s1.str());

		if (posXFloat.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement posYFloat(POS_Y);
		std::stringstream s2;
		s2 << obj->m_position.y;
		text.SetValue(s2.str());

		if (posYFloat.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement posZFloat(POS_Z);
		std::stringstream s3;
		s3 << obj->m_position.z;
		text.SetValue(s3.str());

		if (posZFloat.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement idRotInt(ROTATION_TAG);
		_ltoa_s(obj->m_rotation, buf, _countof(buf), 10);
		text.SetValue(buf);

		if (idRotInt.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		if (child.InsertEndChild(posZFloat) == 0 ||
			child.InsertEndChild(posYFloat) == 0 ||
			child.InsertEndChild(posXFloat) == 0 ||
			child.InsertEndChild(idRefInt) == 0 ||
			child.InsertEndChild(nameStr) == 0 ||
			child.InsertEndChild(idRotInt) == 0 ||
			rootNode->InsertEndChild(child) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}
	}

	if (!dom.SaveFile()) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}
#else
	throw new KEPException("CSpawnCfgObjList::SerializeToXML disabled for XStaticServer", 0, __FILE__, __LINE__);
#endif
}

IMPLEMENT_SERIAL_SCHEMA(CWorldAvailableObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CWorldAvailableObjList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CSpawnCfgObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CSpawnCfgObjList, CObList)

BEGIN_GETSET_IMPL(CWorldAvailableObj, IDS_WORLDAVAILABLEOBJ_NAME)
GETSET_MAX(m_worldName, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, WORLDAVAILABLEOBJ, WORLDNAME, STRLEN_MAX)
GETSET_RANGE(m_channel, gsInt, GS_FLAG_EXPOSE, 0, 0, WORLDAVAILABLEOBJ, ID, INT_MIN, INT_MAX)
GETSET_MAX(m_displayName, gsCString, GS_FLAG_EXPOSE, 0, 0, WORLDAVAILABLEOBJ, DISPLAYNAME, STRLEN_MAX)
GETSET_RANGE(m_maxOccupancy, gsUlong, GS_FLAG_EXPOSE, 0, 0, WORLDAVAILABLEOBJ, MAX_OCCUPANCY, 1, INT_MAX)
GETSET_RANGE(m_visibility, gsUlong, GS_FLAG_EXPOSE, 0, 0, WORLDAVAILABLEOBJ, VISIBILITY, 0, INT_MAX)
GETSET2_RANGE(m_index, gsInt, GS_FLAG_EXPOSE, 0, 0, IDS_ZONEOBJECTS_ZONEINDEX, IDS_ZONEOBJECTS_ZONEINDEX, 0, INT_MAX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CSpawnCfgObj, IDS_SPAWNCFGOBJ_NAME)
GETSET(m_zoneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNCFGOBJ, ZONEINDEX)
GETSET(m_position, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, SPAWNCFGOBJ, POSITION)
GETSET2(m_rotation, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_SPAWNCFGOBJ_ROTATION, IDS_SPAWNCFGOBJ_ROTATION_HELP)
GETSET_MAX(m_spawnPointName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SPAWNCFGOBJ, SPAWNPOINTNAME, STRLEN_MAX)
GETSET(m_id, gsInt, GS_FLAGS_ACTION, 0, 0, SPAWNCFGOBJ, ID)
END_GETSET_IMPL

} // namespace KEP