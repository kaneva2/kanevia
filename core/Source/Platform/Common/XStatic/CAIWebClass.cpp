///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <afxtempl.h>
#include "CAIWebClass.h"
#include "resource.h"
#include "ClientStrings.h"

namespace KEP {

//-----------------------------------------------------//
//-----------------Route To Object---------------------//
//-----------------------------------------------------//
CAIRouteToObj::CAIRouteToObj() {
	m_mapNodeCount = 0;
	m_map = NULL;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIRouteToObj::SafeDelete() {
	if (m_map) {
		delete[] m_map;
		m_map = 0;
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIRouteToObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_mapNodeCount;
		for (int loop = 0; loop < m_mapNodeCount; loop++)
			ar << m_map[loop];
	} else {
		ar >> m_mapNodeCount;
		if (m_mapNodeCount > 0)
			m_map = new int[m_mapNodeCount];
		for (int loop = 0; loop < m_mapNodeCount; loop++)
			ar >> m_map[loop];
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIRouteToObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIRouteToObj* rtPtr = (CAIRouteToObj*)GetNext(posLoc);
		rtPtr->SafeDelete();
		delete rtPtr;
		rtPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
IMPLEMENT_SERIAL(CAIRouteToObj, CObject, 0)
IMPLEMENT_SERIAL(CAIRouteToObjList, CObList, 0)

//-----------------------------------------------------//
//-------------End Route To Object---------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
//------------------web Object-------------------------//
//-----------------------------------------------------//
CAIWebObj::CAIWebObj() {
	m_name = "NA";
	m_web = NULL;
	m_compiled = TRUE;
}
BOOL CAIWebObj::IsFinalized() {
	return m_web == NULL ||
		   m_web->IsEmpty() ||
		   m_compiled;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebObj::SafeDelete() {
	if (m_web) {
		m_web->SafeDelete();
		delete m_web;
		m_web = 0;
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int* CAIWebObj::GetRoute(int currentPos, int dest, int* nodeCount) {
	if (!m_web)
		return NULL;
	POSITION wPos = m_web->FindIndex(currentPos);
	if (!wPos)
		return NULL;
	CAIWebNode* locationNode = (CAIWebNode*)m_web->GetAt(wPos);
	return locationNode->GetRoute(dest, nodeCount);
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_name
		   << m_web;
	} else {
		ar >> m_name >> m_web;
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebObj::AddNode(CAIWebNode* wNode) {
	m_web->AddTail(wNode);
	m_compiled = FALSE;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebObj::RemoveNode(POSITION posLoc) {
	m_web->RemoveAt(posLoc);
	m_compiled = FALSE;
}

void CAIWebObj::CompileAllNodes(int duration) {
	//compile all nodes and set compiled to be true
	m_web->CompileAll(duration);
	m_compiled = TRUE;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIWebObj* wObj = (CAIWebObj*)GetNext(posLoc);
		wObj->SafeDelete();
		delete wObj;
		wObj = 0;
	}
	RemoveAll();
}

IMPLEMENT_SERIAL(CAIWebObj, CObject, 0)
IMPLEMENT_SERIAL(CAIWebObjList, CKEPObList, 0)
//-----------------------------------------------------//
//------------------End web Object---------------------//
//-----------------------------------------------------//
CAIWebNode::CAIWebNode() {
	m_locationName = "NONE";
	m_routeToAllFromLocationList = NULL;
	m_directLinks = NULL;
	m_directLinkCount = 0;
	m_position.x = 0.0f;
	m_position.y = 0.0f;
	m_position.z = 0.0f;
	m_radius = 20.0f;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int* CAIWebNode::GetRoute(int index, int* mapNodeCount) {
	if (m_routeToAllFromLocationList == 0)
		return NULL;

	POSITION posLoc = m_routeToAllFromLocationList->FindIndex(index);
	if (!posLoc) {
		*mapNodeCount = 0;
		return NULL;
	}
	CAIRouteToObj* routePtr = (CAIRouteToObj*)m_routeToAllFromLocationList->GetAt(posLoc);
	*mapNodeCount = routePtr->m_mapNodeCount;
	if (routePtr->m_mapNodeCount > 0) {
		int* mapNew = new int[routePtr->m_mapNodeCount];
		CopyMemory(mapNew, routePtr->m_map, sizeof(int) * routePtr->m_mapNodeCount);
		return mapNew;
	}
	return NULL;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebNode::SafeDelete() {
	if (m_routeToAllFromLocationList) {
		m_routeToAllFromLocationList->SafeDelete();
		delete m_routeToAllFromLocationList;
		m_routeToAllFromLocationList = 0;
	}
	if (m_directLinks) {
		delete[] m_directLinks;
		m_directLinks = 0;
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebNode::AddDirectLink(int newInt) {
	int count = m_directLinkCount;
	int* array = new int[(count + 1)];

	for (int loop = 0; loop < count; loop++)
		array[loop] = m_directLinks[loop];

	array[count] = newInt;
	count++;
	if (m_directLinks)
		delete[] m_directLinks;
	m_directLinks = 0;
	m_directLinkCount = count;
	m_directLinks = array;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebNode::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_locationName
		   << m_routeToAllFromLocationList
		   << m_directLinkCount
		   << m_position.x
		   << m_position.y
		   << m_position.z
		   << m_radius;

		for (int loop = 0; loop < m_directLinkCount; loop++)
			ar << m_directLinks[loop];
	} else {
		ar >> m_locationName >> m_routeToAllFromLocationList >> m_directLinkCount >> m_position.x >> m_position.y >> m_position.z >> m_radius;

		if (m_directLinkCount > 0)
			m_directLinks = new int[m_directLinkCount];
		for (int loop = 0; loop < m_directLinkCount; loop++)
			ar >> m_directLinks[loop];
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
float CAIWebNodeList::GetRadius(int index) {
	POSITION posLoc = FindIndex(index);
	if (!posLoc) {
		return 0.0f;
	}
	CAIWebNode* webPtr = (CAIWebNode*)GetAt(posLoc);
	return webPtr->m_radius;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
Vector3f CAIWebNodeList::GetPosition(int index) {
	POSITION posLoc = FindIndex(index);
	if (!posLoc) {
		Vector3f nullVect;
		nullVect.x = 0.0f;
		nullVect.y = 0.0f;
		nullVect.z = 0.0f;
		AfxMessageBox(IDS_MSG_NULLVECTOR);
		return nullVect;
	}
	CAIWebNode* webPtr = (CAIWebNode*)GetAt(posLoc);
	return webPtr->m_position;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebNodeList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIWebNode* aiPtr = (CAIWebNode*)GetNext(posLoc);
		aiPtr->SafeDelete();
		delete aiPtr;
		aiPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int* CAIWebNodeList::AddIntegerIntoArray(int* intArray, int* arrayCount, int newInt) {
	int count = *arrayCount;
	int* array = new int[(count + 1)];

	for (int loop = 0; loop < count; loop++)
		array[loop] = intArray[loop];

	array[count] = newInt;
	count++;
	if (intArray)
		delete[] intArray;
	intArray = 0;
	*arrayCount = count;
	return array;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int CAIWebNodeList::GetNearestNode(Vector3f location) {
	int trace = 0;
	int winner = 0;
	float distancePersist = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		//Node loop
		CAIWebNode* wMainObj = (CAIWebNode*)GetNext(posLoc);
		float distance = Distance(wMainObj->m_position, location);
		if (trace == 0) {
			distancePersist = distance;
			winner = trace;
		}

		if (distance < distancePersist) {
			distancePersist = distance;
			winner = trace;
		}

		trace++;
	} //end Node loop
	return winner;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CAIWebNodeList::CompileAll(int querryDuration) {
	srand((unsigned)time(NULL));
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		//Node loop
		CAIWebNode* wMainObj = (CAIWebNode*)GetNext(posLoc);
		if (!wMainObj->m_routeToAllFromLocationList)
			wMainObj->m_routeToAllFromLocationList = new CAIRouteToObjList();
		wMainObj->m_routeToAllFromLocationList->SafeDelete();

		for (POSITION desPos = GetHeadPosition(); desPos != NULL;) {
			//destination loop
			CAIWebNode* destObj = (CAIWebNode*)GetNext(desPos);
			CAIRouteToObj* routePtr = new CAIRouteToObj();
			CompileNode(wMainObj, querryDuration, destObj, routePtr);
			wMainObj->m_routeToAllFromLocationList->AddTail(routePtr); //add
		} //end destrination loop
	} //End Node loop
	return TRUE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CAIWebNodeList::UntraveledPathExists(int* currentLinksTraveled,
	int& currentLinkTraveledCount,
	int& directLinkCount,
	int* pathDirectLinks,
	int& randomPathSelection) {
	int untraveledPathCount = 0;
	if (directLinkCount == 0)
		return FALSE;
	int* goodPathArray = new int[directLinkCount];

	for (int loop = 0; loop < directLinkCount; loop++) {
		int possiblePath = pathDirectLinks[loop];
		BOOL traveled = FALSE;
		for (int traveledPathLoop = 0; traveledPathLoop < currentLinkTraveledCount; traveledPathLoop++) {
			if (currentLinksTraveled[traveledPathLoop] == possiblePath)
				traveled = TRUE;
		}

		if (!traveled) {
			goodPathArray[untraveledPathCount] = possiblePath;
			untraveledPathCount++;
			//return TRUE;
		}
	}

	if (untraveledPathCount > 0) {
		randomPathSelection = goodPathArray[(rand() % untraveledPathCount)];
		delete[] goodPathArray;
		goodPathArray = 0;
		return TRUE;
	} else {
		delete[] goodPathArray;
		goodPathArray = 0;
	}
	return FALSE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CAIWebNodeList::CompileNode(CAIWebNode* wMainObj,
	TimeMs querryDuration,
	CAIWebNode* destObj,
	CAIRouteToObj* routePtr) {
	CAIWebNode* currentObj = wMainObj;
	CAIWebNode* lastObj = wMainObj;
	//find route to dest object
	TimeMs timeStamp = fTime::TimeMs();
	TimeMs thoughtDuration = querryDuration;

	int* currentRouteBuild = NULL;
	int currentRouteNodeCount = 0;
	BOOL foundRoute = FALSE;
	int lastMapNode = -1;
	int curMapIndexPoint = 0;
	KEP_ASSERT(timeStamp >= 0 && thoughtDuration >= 0);
	while (fTime::ElapsedMs(timeStamp) < thoughtDuration) { //cycle for duration till u find fastest dest objects from main
		int linkCount = currentObj->m_directLinkCount;
		BOOL hasLinks = TRUE;
		if (linkCount <= 1 && currentRouteNodeCount != 0)
			hasLinks = FALSE;

		//currentRouteNodeCount
		//currentRouteBuild
		int selectedPathLink = 0;
		if (!UntraveledPathExists(currentRouteBuild,
				currentRouteNodeCount,
				linkCount,
				currentObj->m_directLinks,
				selectedPathLink))
			hasLinks = FALSE;

		if (hasLinks == TRUE && linkCount != 0) { //has links
			if (currentObj == destObj) //already there
				foundRoute = TRUE;
			else { //go through process

				int currentProspect = selectedPathLink;

				lastMapNode = curMapIndexPoint;
				curMapIndexPoint = currentProspect;
				//add
				lastObj = currentObj;
				if (curMapIndexPoint < 0) {
					jsAssert(false);
					return;
				}
				POSITION posLastObj = FindIndex(curMapIndexPoint);
				currentObj = (CAIWebNode*)GetAt(posLastObj);
				currentRouteBuild = AddIntegerIntoArray(currentRouteBuild, &currentRouteNodeCount, curMapIndexPoint);
				if (currentObj == destObj)
					foundRoute = TRUE;
			} //end go through process
		} //end has links
		else { //end of the road?
			if (currentObj == destObj) //already there
				foundRoute = TRUE;
			else { //nada
				lastObj = wMainObj;
				currentObj = wMainObj;
				lastMapNode = -1;
				curMapIndexPoint = 0;
				if (currentRouteBuild) {
					delete[] currentRouteBuild;
					currentRouteBuild = 0;
				}
				currentRouteNodeCount = 0;
			} //end nada
		} //end end of the road?
		if (foundRoute == TRUE) { //shift and set
			if (currentRouteNodeCount < routePtr->m_mapNodeCount || routePtr->m_map == NULL) {
				//found better path
				if (routePtr->m_map) {
					delete[] routePtr->m_map;
					routePtr->m_map = 0;
				}

				routePtr->m_mapNodeCount = currentRouteNodeCount;
				if (currentRouteNodeCount > 0) {
					routePtr->m_map = new int[currentRouteNodeCount];
					CopyMemory(routePtr->m_map, currentRouteBuild, (sizeof(int) * currentRouteNodeCount));
					delete[] currentRouteBuild;
				}
				currentRouteBuild = 0;
				currentRouteNodeCount = 0;
				lastMapNode = -1;
				curMapIndexPoint = 0;
				lastObj = wMainObj;
				currentObj = wMainObj; //reset
				foundRoute = FALSE;
			} //end found better path
		} //end shift and set
	} //end cycle for duration till u find fastest dest objects from main
	//end find route to dest object
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CAIWebNodeList::GetShortestRoute(int current, int target) {
	//start tree-build lowest level root
	CAITree* m_baseTree = new CAITree();
	int backPath = -1;
	GenerateNodeInfo(backPath,
		current,
		m_baseTree); //build web

	return TRUE; //found route successfully (is possible)
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CAIWebNodeList::BeenThereAlready(CAITree* m_baseTree,
	int destination) {
	for (POSITION posLoc = m_baseTree->GetHeadPosition(); posLoc != NULL;) {
		CAITreeNode* node = (CAITreeNode*)m_baseTree->GetNext(posLoc);
		if (node->m_currentNode == destination)
			return TRUE;
	}
	return FALSE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CAIWebNodeList::GenerateNodeInfo(int& backPath,
	int& currentIndex,
	CAITree* m_currentTree) {
	return TRUE;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
CAIWebNode* CAIWebNodeList::GetByIndex(int index) {
	POSITION rPos = FindIndex(index);
	if (!rPos)
		return NULL;
	return (CAIWebNode*)GetAt(rPos);
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int CAIWebNodeList::GetIndexFromPointer(CAIWebNode* wbPtr) {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIWebNode* cmpPtr = (CAIWebNode*)GetNext(posLoc);
		if (wbPtr == cmpPtr)
			return trace;
		trace++;
	}
	return -1;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CAIWebNodeList::VerifyMoreInList(CAIWebNode* wbPtr) {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIWebNode* cmpPtr = (CAIWebNode*)GetNext(posLoc);
		if (wbPtr != cmpPtr)
			return TRUE;
		trace++;
	}
	return FALSE;
}

IMPLEMENT_SERIAL(CAIWebNode, CObject, 0)
IMPLEMENT_SERIAL(CAIWebNodeList, CObList, 0)

BEGIN_GETSET_IMPL(CAIRouteToObj, IDS_AIROUTETOOBJ_NAME)
GETSET_RANGE(m_mapNodeCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIROUTETOOBJ, MAPNODECOUNT, INT_MIN, INT_MAX)
GETSET_SIMPLE_ARRAY(m_mapNodeCount, int, gsInt, m_map, GS_FLAGS_DEFAULT, 0, 0, AIROUTETOOBJ, MAP)
END_GETSET_IMPL

} // namespace KEP