///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "direct.h"
#include <SerializeHelper.h>
#include "UnicodeMFCHelpers.h"
#include "CSoundManagerClass.h"

namespace KEP {

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
CSoundSampleObj::CSoundSampleObj() {
	m_soundBuffer = NULL;
	m_3dBuffer = NULL;
	m_fileName = "NONE";
	m_loopable = FALSE;
	m_soundFactor = .01f;
	m_selfDeleteing = TRUE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CSoundSampleObj::UpdatePosition(Vector3f& positionalUpdate) {
	if (!m_3dBuffer)
		return FALSE;

	m_3dBuffer->SetPosition(-positionalUpdate.x * m_soundFactor,
		-positionalUpdate.y * m_soundFactor,
		-positionalUpdate.z * m_soundFactor,
		DS3D_IMMEDIATE);
	return TRUE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CSoundSampleObj::SetToDelete() {
	if (!m_soundBuffer)
		return FALSE;

	if (m_loopable)
		m_soundBuffer->Stop();

	m_loopable = FALSE;
	m_selfDeleteing = TRUE;

	return TRUE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundSampleObj::Clone(CSoundSampleObj** clone, IDirectSound8* directSoundPtr) {
	CSoundSampleObj* clonePtr = new CSoundSampleObj();
	//copyLocal->
	//base data
	clonePtr->m_fileName = m_fileName;
	clonePtr->m_soundFactor = m_soundFactor;
	//base sound buffer
	if (directSoundPtr) { //valid direct sound ptr
		CloneSoundObjects(m_soundBuffer,
			m_3dBuffer,
			&clonePtr->m_soundBuffer,
			&clonePtr->m_3dBuffer,
			directSoundPtr);
	} //end valid direct sound ptr
	else {
		clonePtr->m_soundBuffer = NULL;
		clonePtr->m_3dBuffer = NULL;
	}

	*clone = clonePtr;
}
//-------------------------End Function-----------------------------------------------//
//------------------------------------------------------------------------------------//
//-------------clone sound objects-------------------------------------------//
BOOL CSoundSampleObj::CloneSoundObjects(IDirectSoundBuffer* baseSoundBuffer,
	IDirectSound3DBuffer* buffer3DControl,
	IDirectSoundBuffer** newSndBuf,
	IDirectSound3DBuffer** New3DSndBuf,
	IDirectSound8* DirSnd) {
	if (!baseSoundBuffer)
		return FALSE;
	if (!buffer3DControl)
		return FALSE;

	IDirectSoundBuffer* newSoundBuffer = NULL;
	IDirectSound3DBuffer* new3DSound = NULL;

	DirSnd->DuplicateSoundBuffer(baseSoundBuffer, &newSoundBuffer);
	if (!newSoundBuffer)
		return FALSE;
	HRESULT er = newSoundBuffer->QueryInterface(IID_IDirectSound3DBuffer, (LPVOID*)&new3DSound);
	if
		SUCCEEDED(er) { // Set 3-D parameters of this sound
			new3DSound->SetMode(DS3DMODE_HEADRELATIVE, DS3D_IMMEDIATE);
			*newSndBuf = newSoundBuffer;
			*New3DSndBuf = new3DSound;
			return TRUE;
		}
	return FALSE; ///:)
	//return TRUE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundSampleObj::Play(BOOL looping) {
	m_loopable = looping;
	if (!looping)
		m_soundBuffer->Play(0, 0, 0);
	else
		m_soundBuffer->Play(0, 0, DSBPLAY_LOOPING);
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CSoundSampleObj::Initialize(IDirectSound8* directSoundPtr) {
	if (InitFromFile(directSoundPtr, m_fileName) == FALSE) {
		//AfxMessageBox(m_fileName);
		return FALSE;
	}
	return TRUE;
}
//------------------------------------------------------------------//
//------------------------------------------------------------------//
//--------------------Constructor-----------------------------------//
BOOL CSoundSampleObj::InitFromFile(IDirectSound8* directSoundPtr, CStringA fileName) {
	// TODO: get the fully qualified name passed in

	if (directSoundPtr) { //valid sound system
		//destroy existing pointers
		if (m_soundBuffer)
			m_soundBuffer->Release();
		m_soundBuffer = 0;
		if (m_3dBuffer)
			m_3dBuffer->Release();
		m_3dBuffer = 0;
		//end destroy existing pointers
		m_soundBuffer = DSLoad3DSoundBuffer(directSoundPtr, fileName);
		if (!m_soundBuffer) {
			return FALSE;
		}

		m_fileName = fileName;
		if (!m_soundBuffer) {
			m_soundBuffer = NULL;
			m_3dBuffer = NULL;
			return FALSE;
		}

		HRESULT er = m_soundBuffer->QueryInterface(IID_IDirectSound3DBuffer, (LPVOID*)&m_3dBuffer);
		if
			SUCCEEDED(er) { // Set 3-D parameters of this sound
				m_3dBuffer->SetMode(DS3DMODE_HEADRELATIVE, DS3D_IMMEDIATE);
				return TRUE;
			}
	} //end valid sound system
	else
		return FALSE;
	return TRUE;
}
//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
IDirectSoundBuffer* CSoundSampleObj::DSLoad3DSoundBuffer(IDirectSound8* pDS, const char* lpName) {
	IDirectSoundBuffer* pDSB = NULL;
	DSBUFFERDESC dsBD = { 0 };
	BYTE* pbWaveData;
	void* pvBase;

	dsBD.dwSize = sizeof(dsBD);
	dsBD.dwFlags = DSBCAPS_STATIC | DSBCAPS_CTRL3D | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLFREQUENCY | DSBCAPS_MUTE3DATMAXDISTANCE;

	if (DSGetWaveResource(NULL, lpName, &dsBD.lpwfxFormat, &pbWaveData, &dsBD.dwBufferBytes)) {
		if (SUCCEEDED(IDirectSound_CreateSoundBuffer(pDS, &dsBD, &pDSB, NULL))) {
			if (!DSFillSoundBuffer(pDSB, pbWaveData, dsBD.dwBufferBytes)) {
				IDirectSoundBuffer_Release(pDSB);
				pDSB = NULL;
			}
		} else {
			pDSB = NULL;
		}
	} else if (DSGetWaveFile(NULL, lpName, &dsBD.lpwfxFormat, &pbWaveData,
				   &dsBD.dwBufferBytes, &pvBase)) {
		if (SUCCEEDED(IDirectSound_CreateSoundBuffer(pDS, &dsBD, &pDSB, NULL))) {
			if (!DSFillSoundBuffer(pDSB, pbWaveData, dsBD.dwBufferBytes)) {
				IDirectSoundBuffer_Release(pDSB);
				pDSB = NULL;
			}
		} else {
			pDSB = NULL;
		}
		UnmapViewOfFile(pvBase);
	}

	return pDSB;
}
///////////////////////////////////////////////////////////////////////////////
//
// DSGetWaveResource
//
///////////////////////////////////////////////////////////////////////////////
BOOL CSoundSampleObj::DSGetWaveResource(HMODULE hModule, const char* lpName,
	WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize) {
	HRSRC hResInfo;
	HGLOBAL hResData;
	void* pvRes;

	if (((hResInfo = FindResource(hModule, Utf8ToUtf16(lpName).c_str(), L"WAV")) != NULL) &&
		((hResData = LoadResource(hModule, hResInfo)) != NULL) &&
		((pvRes = LockResource(hResData)) != NULL) &&
		DSParseWaveResource(pvRes, ppWaveHeader, ppbWaveData, pcbWaveSize)) {
		return TRUE;
	}

	return FALSE;
}
///////////////////////////////////////////////////////////////////////////////
//
// DSParseWaveResource
//
///////////////////////////////////////////////////////////////////////////////
BOOL CSoundSampleObj::DSParseWaveResource(void* pvRes, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize) {
	DWORD* pdw;
	DWORD* pdwEnd;
	DWORD dwRiff;
	DWORD dwType;
	DWORD dwLength;

	if (ppWaveHeader)
		*ppWaveHeader = NULL;

	if (ppbWaveData)
		*ppbWaveData = NULL;

	if (pcbWaveSize)
		*pcbWaveSize = 0;

	pdw = (DWORD*)pvRes;
	dwRiff = *pdw++;
	dwLength = *pdw++;
	dwType = *pdw++;

	if (dwRiff != mmioFOURCC('R', 'I', 'F', 'F'))
		goto exit; // not even RIFF

	if (dwType != mmioFOURCC('W', 'A', 'V', 'E'))
		goto exit; // not a WAV

	pdwEnd = (DWORD*)((BYTE*)pdw + dwLength - 4);

	while (pdw < pdwEnd) {
		dwType = *pdw++;
		dwLength = *pdw++;

		switch (dwType) {
			case mmioFOURCC('f', 'm', 't', ' '):
				if (ppWaveHeader && !*ppWaveHeader) {
					if (dwLength < sizeof(WAVEFORMAT))
						goto exit; // not a WAV

					*ppWaveHeader = (WAVEFORMATEX*)pdw;

					if ((!ppbWaveData || *ppbWaveData) &&
						(!pcbWaveSize || *pcbWaveSize)) {
						return TRUE;
					}
				}
				break;

			case mmioFOURCC('d', 'a', 't', 'a'):
				if ((ppbWaveData && !*ppbWaveData) ||
					(pcbWaveSize && !*pcbWaveSize)) {
					if (ppbWaveData)
						*ppbWaveData = (LPBYTE)pdw;

					if (pcbWaveSize)
						*pcbWaveSize = dwLength;

					if (!ppWaveHeader || *ppWaveHeader)
						return TRUE;
				}
				break;
		}

		pdw = (DWORD*)((BYTE*)pdw + ((dwLength + 1) & ~1));
	}

exit:
	return FALSE;
}
///////////////////////////////////////////////////////////////////////////////
//
// DSFillSoundBuffer
//
///////////////////////////////////////////////////////////////////////////////
BOOL CSoundSampleObj::DSFillSoundBuffer(IDirectSoundBuffer* pDSB, BYTE* pbWaveData, DWORD cbWaveSize) {
	if (pDSB && pbWaveData && cbWaveSize) {
		LPVOID pMem1, pMem2;
		DWORD dwSize1, dwSize2;

		if (SUCCEEDED(IDirectSoundBuffer_Lock(pDSB, 0, cbWaveSize,
				&pMem1, &dwSize1, &pMem2, &dwSize2, 0))) {
			CopyMemory(pMem1, pbWaveData, dwSize1);

			if (0 != dwSize2)
				CopyMemory(pMem2, pbWaveData + dwSize1, dwSize2);

			IDirectSoundBuffer_Unlock(pDSB, pMem1, dwSize1, pMem2, dwSize2);
			return TRUE;
		}
	}

	return FALSE;
}
///////////////////////////////////////////////////////////////////////////////
//
// DSGetWaveFile
//
///////////////////////////////////////////////////////////////////////////////
BOOL CSoundSampleObj::DSGetWaveFile(HMODULE hModule, const char* lpName,
	WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize,
	void** ppvBase) {
	void* pvRes;
	HANDLE hFile, hMapping;

	*ppvBase = NULL;

	hFile = CreateFileW(Utf8ToUtf16(lpName).c_str(), GENERIC_READ, FILE_SHARE_READ,
		NULL, OPEN_EXISTING, 0, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;

	hMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if (hMapping == INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
		return FALSE;
	}

	CloseHandle(hFile);

	pvRes = MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0);
	if (pvRes == NULL) {
		CloseHandle(hMapping);
		return FALSE;
	}

	CloseHandle(hMapping);

	if (DSParseWaveResource(pvRes, ppWaveHeader, ppbWaveData, pcbWaveSize) == FALSE) {
		UnmapViewOfFile(pvRes);
		return FALSE;
	}

	*ppvBase = pvRes;
	return TRUE;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundSampleObj::SafeDelete() {
	if (m_soundBuffer) {
		m_soundBuffer->Stop();
		m_soundBuffer->Release();
		m_soundBuffer = 0;
	}
	if (m_3dBuffer)
		m_3dBuffer->Release();
	m_3dBuffer = 0;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundSampleObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_fileName
		   << m_soundFactor;
	} else {
		ar >> m_fileName >> m_soundFactor;

		m_3dBuffer = 0;
		m_soundBuffer = 0;
		m_loopable = FALSE;
		m_selfDeleteing = TRUE;
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundSampleObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSoundSampleObj* sndPtr = (CSoundSampleObj*)GetNext(posLoc);
		sndPtr->SafeDelete();
		delete sndPtr;
		sndPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundSampleObjList::CallBack() {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CSoundSampleObj* sndPtr = (CSoundSampleObj*)GetNext(posLoc);
		if (!sndPtr->m_selfDeleteing)
			continue;
		DWORD result;
		sndPtr->m_soundBuffer->GetStatus(&result);

		if (result != DSBSTATUS_PLAYING && sndPtr->m_loopable == FALSE) {
			//done so remove from list
			sndPtr->SafeDelete();
			delete sndPtr;
			sndPtr = 0;
			RemoveAt(posLast);
		} //end done so remove from list
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
CSoundBankObj::CSoundBankObj() {
	m_primary = NULL;
	m_soundChain = NULL;
	m_id = 0;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CSoundBankObj::InitPrimary(CStringA fileName, IDirectSound8* directSoundPtr) {
	if (m_primary) {
		m_primary->SafeDelete();
		delete m_primary;
		m_primary = 0;
	}
	if (!m_soundChain)
		m_soundChain = new CSoundSampleObjList;
	m_primary = new CSoundSampleObj();
	m_primary->m_fileName = fileName;

	return m_primary->Initialize(directSoundPtr);
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
CSoundSampleObj* CSoundBankObj::Play(BOOL looping, IDirectSound8* directSoundPtr) {
	//	Play sample in 2D
	CSoundSampleObj* sample = NULL;
	if (!m_soundChain)
		return NULL;

	if (!m_primary)
		return NULL;

	if (!m_primary->m_3dBuffer)
		return NULL;

	DWORD result;
	m_primary->m_soundBuffer->GetStatus(&result);
	if (result != DSBSTATUS_PLAYING) {
		m_primary->m_3dBuffer->SetMode(DS3DMODE_DISABLE, DS3D_IMMEDIATE);
		m_primary->Play(looping);
		m_primary->m_3dBuffer->SetMode(DS3DMODE_HEADRELATIVE, DS3D_IMMEDIATE);

		return m_primary;
	}

	if (m_soundChain->GetCount() > 3)
		return NULL;

	m_primary->Clone(&sample, directSoundPtr);

	if (sample) {
		if (sample->m_3dBuffer) {
			m_primary->m_3dBuffer->SetMode(DS3DMODE_DISABLE, DS3D_IMMEDIATE);
			sample->Play(looping);
			m_primary->m_3dBuffer->SetMode(DS3DMODE_HEADRELATIVE, DS3D_IMMEDIATE);

			m_soundChain->AddTail(sample);
			return sample;
		}
	}

	if (sample) {
		sample->SafeDelete();
		delete sample;
		sample = 0;
	}

	return NULL;
}

CSoundSampleObj* CSoundBankObj::Play(BOOL looping,
	IDirectSound8* directSoundPtr,
	Vector3f& initialPosition,
	float& soundFactor,
	float clipRadius) {
	CSoundSampleObj* sample = NULL;
	if (!m_soundChain)
		return NULL;

	if (!m_primary)
		return NULL;

	if (!m_primary->m_3dBuffer)
		return NULL;

	//sound clipping
	Vector3f positionZero(0, 0, 0); //head location localized
	float dist = Distance(positionZero, initialPosition);
	if (dist > clipRadius)
		return NULL;

	DWORD result;
	m_primary->m_soundBuffer->GetStatus(&result);
	if (result != DSBSTATUS_PLAYING) {
		m_primary->m_soundFactor = soundFactor;
		m_primary->m_3dBuffer->SetPosition(-initialPosition.x * m_primary->m_soundFactor,
			-initialPosition.y * m_primary->m_soundFactor,
			-initialPosition.z * m_primary->m_soundFactor,
			DS3D_IMMEDIATE);
		m_primary->Play(looping);
		return m_primary;
	}

	if (m_soundChain->GetCount() > 3)
		return NULL;

	m_primary->Clone(&sample, directSoundPtr);
	if (sample)
		if (sample->m_3dBuffer) {
			sample->m_soundFactor = soundFactor;
			sample->m_3dBuffer->SetPosition(-initialPosition.x * sample->m_soundFactor,
				-initialPosition.y * sample->m_soundFactor,
				-initialPosition.z * sample->m_soundFactor,
				DS3D_IMMEDIATE);
			sample->Play(looping);
			m_soundChain->AddTail(sample);
			return sample;
		}

	if (sample) {
		sample->SafeDelete();
		delete sample;
		sample = 0;
	}

	return NULL;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundBankObj::SafeDelete() {
	if (m_primary) {
		m_primary->SafeDelete();
		delete m_primary;
		m_primary = 0;
	}
	if (m_soundChain) {
		m_soundChain->SafeDelete();
		delete m_soundChain;
		m_soundChain = 0;
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundBankObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_soundChain
		   << m_primary
		   << m_id;
	} else {
		ar >> m_soundChain >> m_primary >> m_id;

		m_soundChain->SafeDelete();
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundBankList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		sndPtr->SafeDelete();
		delete sndPtr;
		sndPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int CSoundBankList::GetUniqueID() {
	int id = -1;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		if (sndPtr->m_id > id)
			id = sndPtr->m_id;
	}

	return id + 1;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
BOOL CSoundBankList::CreateSoundBank(CStringA fileName) {
	CSoundBankObj* newSBBank = new CSoundBankObj();
	newSBBank->m_id = GetUniqueID();

	BOOL result = newSBBank->InitPrimary(fileName, m_directSoundPtr);
	if (result) {
		AddTail(newSBBank);
		return TRUE;
	} else {
		delete newSBBank;
		newSBBank = 0;

		return FALSE;
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
CSoundSampleObj* CSoundBankList::PlayByID(int id, BOOL looping) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		if (sndPtr->m_id == id) {
			return sndPtr->Play(looping, m_directSoundPtr);
		}
	}
	return NULL;
}

CSoundSampleObj* CSoundBankList::PlayByID(int id,
	BOOL looping,
	Vector3f& initialPosition,
	float& soundFactor,
	float clipRadius) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		if (sndPtr->m_id == id) {
			return sndPtr->Play(looping,
				m_directSoundPtr,
				initialPosition,
				soundFactor,
				clipRadius);
		}
	}
	return NULL;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int CSoundBankList::GetID(CStringA filename) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		if (sndPtr->m_primary->m_fileName == filename) {
			return sndPtr->m_id;
		}
	}
	return -1;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundBankList::Initialize(IDirectSound8* directSoundPtr) {
	m_directSoundPtr = directSoundPtr;
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		sndPtr->m_primary->Initialize(directSoundPtr);
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
int CSoundBankList::GetCurrentRuntimeSoundCount() {
	int count = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		count += sndPtr->m_soundChain->GetCount();
	}
	return count;
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundBankList::CallBack() {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CSoundBankObj* sndPtr = (CSoundBankObj*)GetNext(posLoc);
		sndPtr->m_soundChain->CallBack();
	}
}
//-----------------------------------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//
void CSoundBankList::Save(CStringA fileName) {
	CFileException exc;
	CFile fl;
	if (OpenCFile(fl, fileName, CFile::modeCreate | CFile::modeWrite, &exc)) {
		BEGIN_SERIALIZE_TRY

		CArchive the_out_Archive(&fl, CArchive::store);
		the_out_Archive << this;
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY_NO_LOGGER

	} else
		LOG_EXCEPTION_NO_LOGGER(exc)
}

IMPLEMENT_SERIAL(CSoundBankObj, CObject, 0)
IMPLEMENT_SERIAL(CSoundBankList, CKEPObList, 0)
IMPLEMENT_SERIAL(CSoundSampleObj, CObject, 0)
IMPLEMENT_SERIAL(CSoundSampleObjList, CObList, 0)

BEGIN_GETSET_IMPL(CSoundSampleObj, IDS_SOUNDSAMPLEOBJ_NAME)
GETSET_MAX(m_fileName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SOUNDSAMPLEOBJ, FILENAME, STRLEN_MAX)
GETSET_RANGE(m_soundFactor, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SOUNDSAMPLEOBJ, SOUNDFACTOR, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP