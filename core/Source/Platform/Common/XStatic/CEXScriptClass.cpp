///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <afxtempl.h>
#include "KEPException.h"
#include "CEXScriptClass.h"
#include <TinyXML/tinyxml.h>
#include <KEPException.h>
#include "LogHelper.h"

namespace KEP {

CEXScriptObj::CEXScriptObj(ACTION_ATTR L_actionAttribute,
	CStringA L_miscString,
	int L_miscInt) {
	m_actionAttribute = L_actionAttribute;
	m_miscString = L_miscString;
	m_miscInt = L_miscInt;
}

void CEXScriptObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << (DWORD)m_actionAttribute
		   << m_miscString
		   << m_miscInt;
	} else {
		DWORD actionAttr;
		ar >> actionAttr >> m_miscString >> m_miscInt;
		m_actionAttribute = (ACTION_ATTR)actionAttr;
	}
}

// names of elements
static const char* const SCRIPT_TAG = "Script";
static const char* const ID_TAG = "id";
static const char* const COMMAND_TAG = "Command";
static const char* const INT_TAG = "Int";
static const char* const STRING_TAG = "String";
static const char* const DISABLED_TAG = "disabled";

void CEXScriptObjList::SerializeToXML(const FileName& fileName) {
	static LogInstance("Instance");
	LogInfo("fileName='" << fileName.toString() << "'");

#ifdef _ClientOutput // For client/editor only
	TiXmlDocument dom(fileName.toString().c_str());
	TiXmlElement root(SCRIPT_TAG);
	TiXmlNode* rootNode = dom.InsertEndChild(root);
	if (rootNode == 0) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}

	char s[34];

	for (POSITION pos = this->GetHeadPosition(); pos != NULL;) {
		CEXScriptObj* obj = (CEXScriptObj*)GetNext(pos);

		TiXmlElement child(COMMAND_TAG);
		_ltoa_s((DWORD)obj->m_actionAttribute, s, _countof(s), 10);
		child.SetAttribute(ID_TAG, s);

		TiXmlElement miscStr(STRING_TAG);
		TiXmlText text(obj->m_miscString);
		if (miscStr.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement miscInt(INT_TAG);
		_ltoa_s(obj->m_miscInt, s, _countof(s), 10);
		text.SetValue(s);

		if (miscInt.InsertEndChild(text) == 0 ||
			child.InsertEndChild(miscInt) == 0 ||
			child.InsertEndChild(miscStr) == 0 ||
			rootNode->InsertEndChild(child) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}
	}

	if (!dom.SaveFile()) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}

	m_lastSavedPath = fileName;
	m_dirty = FALSE;
#else
	throw new KEPException("CSpawnCfgObjList::SerializeToXML disabled for XStaticServer", 0, __FILE__, __LINE__);
#endif
}

void CEXScriptObjList::SerializeFromXML(const FileName& fileName) {
	static LogInstance("Instance");
	LogInfo("'" << LogPath(fileName.toString()) << "'");

	TiXmlDocument dom;
	if (dom.LoadFile(fileName.toString().c_str())) {
		TiXmlElement* root = dom.FirstChildElement(SCRIPT_TAG);
		if (root) {
			TiXmlNode* node;
			TiXmlElement* child = 0;
			while ((node = root->IterateChildren(COMMAND_TAG, child)) != 0 && (child = node->ToElement()) != 0) {
				int disabled;
				if (TIXML_SUCCESS == child->QueryIntAttribute(DISABLED_TAG, &disabled) && disabled != 0)
					continue;

				// Process Action Attribute
				int intAttr = 0;
				if (TIXML_SUCCESS == child->QueryIntAttribute(ID_TAG, &intAttr)) {
					ACTION_ATTR actionAttr = (ACTION_ATTR)intAttr;
					TiXmlText* miscStr = TiXmlHandle(child->FirstChild(STRING_TAG)).FirstChild().Text();
					TiXmlText* miscInt = TiXmlHandle(child->FirstChild(INT_TAG)).FirstChild().Text();

					std::string s;
					int i = 0;
					if (miscStr)
						s = miscStr->Value();
					if (miscInt)
						i = atoi(miscInt->Value());

					//					LogInstance("Instance");
					//					LogInfo(ActionAttrStr(actionAttr)<<" '"<<s<<"' int="<<i);

					AddTail(new CEXScriptObj(actionAttr, s.c_str(), i));
				}
			}

			m_lastSavedPath = fileName;
			m_dirty = FALSE;
			return;
		}
	}
	throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
}

IMPLEMENT_SERIAL(CEXScriptObj, CObject, 0)
IMPLEMENT_SERIAL(CEXScriptObjList, CObList, 0)

} // namespace KEP