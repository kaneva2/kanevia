///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CTimeSystem.h"
#include "CEXMeshClass.h"
#include "CDayStateClass.h"
#include "CStormClass.h"
#include "CLightningEffectClass.h"
#include "CSoundManagerClass.h"
#include "LogHelper.h"

namespace KEP {

#define DS_DURATION_TO_MINUTES(a) ((a)*1.f / 60000.f)
#define DS_MINUTES_TO_DURATION(a) ((long)((a)*60000.f))

static Logger m_logger = Logger::getInstance(L"CTimeSystem");

CTimeSystemObj::CTimeSystemObj() {
	m_timeStamp = (TimeMs)0.0;
	m_currentSystemDay = 0;
	m_currentSystemMonth = 0;
	m_currentSystemYear = 0;
	m_currentDayTime = 0;

	//cycle variable
	m_monthsPerYear = 12;
	m_daysPerMonth = 30;

	//Generate values
	m_millisecondsPerDay = 0;
	m_dayStateList = NULL;
	m_pDSO = NULL;
	m_visual = NULL;

	m_currentSunPosition.x = 0.0f;
	m_currentSunPosition.y = 0.0f;
	m_currentSunPosition.z = 0.0f;

	m_stormInUse = FALSE;
	m_stormPtr = NULL;
	m_frequencyStormCheck = 5000;
	m_stormCheckTimeStamp = 0;

	m_stormDB = NULL;
}

void CTimeSystemObj::SafeDelete() {
	if (m_dayStateList) {
		m_dayStateList->SafeDelete();
		delete m_dayStateList;
		m_dayStateList = 0;
	}

	if (m_pDSO) {
		m_pDSO->SafeDelete();
		delete m_pDSO;
		m_pDSO = 0;
	}

	if (m_visual) {
		m_visual->SafeDelete();
		delete m_visual;
		m_visual = 0;
	}

	if (m_stormDB) {
		m_stormDB->SafeDelete();
		delete m_stormDB;
		m_stormDB = 0;
	}
}

CDayStateObj* CTimeSystemObj::MakeDayState(float minutes, float fogStartRange, float fogEndRange, float fogColorRed, float fogColorGreen, float fogColorBlue, float sunColorRed, float sunColorGreen, float sunColorBlue, float ambientColorRed, float ambientColorGreen, float ambientColorBlue, float sunRotationX, float sunRotationY, float sunDistance) {
	CDayStateObj* statePtr = new CDayStateObj();
	statePtr->m_duration = DS_MINUTES_TO_DURATION(minutes);
	statePtr->m_fogColorBlue = fogColorBlue;
	statePtr->m_fogColorGreen = fogColorGreen;
	statePtr->m_fogColorRed = fogColorRed;
	statePtr->m_fogStartRange = fogStartRange;
	statePtr->m_fogEndRange = fogEndRange;
	statePtr->m_sunColorRed = sunColorRed;
	statePtr->m_sunColorGreen = sunColorGreen;
	statePtr->m_sunColorBlue = sunColorBlue;
	statePtr->m_ambientColorRed = ambientColorRed;
	statePtr->m_ambientColorGreen = ambientColorGreen;
	statePtr->m_ambientColorBlue = ambientColorBlue;
	statePtr->m_sunRotationX = sunRotationX;
	statePtr->m_sunRotationY = sunRotationY;
	statePtr->m_sunDistance = (long)sunDistance;

	return statePtr;
}

void CTimeSystemObj::AddDayState(CDayStateObj* _dayState) {
	return AddDayState(DS_DURATION_TO_MINUTES(_dayState->m_duration),
		_dayState->m_fogStartRange,
		_dayState->m_fogEndRange,
		_dayState->m_fogColorRed,
		_dayState->m_fogColorGreen,
		_dayState->m_fogColorBlue,
		_dayState->m_sunColorRed,
		_dayState->m_sunColorGreen,
		_dayState->m_sunColorBlue,
		_dayState->m_ambientColorRed,
		_dayState->m_ambientColorGreen,
		_dayState->m_ambientColorBlue,
		_dayState->m_sunRotationX,
		_dayState->m_sunRotationY,
		_dayState->m_sunDistance);
}

void CTimeSystemObj::AddDayState(float minutes,
	float fogStartRange,
	float fogEndRange,
	float fogColorRed,
	float fogColorGreen,
	float fogColorBlue,
	float sunColorRed,
	float sunColorGreen,
	float sunColorBlue,
	float ambientColorRed,
	float ambientColorGreen,
	float ambientColorBlue,
	float sunRotationX,
	float sunRotationY,
	float sunDistance) {
	CDayStateObj* statePtr = MakeDayState(minutes, fogStartRange, fogEndRange, fogColorRed, fogColorGreen, fogColorBlue,
		sunColorRed, sunColorGreen, sunColorBlue, ambientColorRed, ambientColorGreen, ambientColorBlue,
		sunRotationX, sunRotationY, sunDistance);

	if (NULL == m_dayStateList) {
		m_dayStateList = new CDayStateObjList();
	}

	m_dayStateList->AddTail(statePtr);
}

bool CTimeSystemObj::InsertDayStateBefore(CDayStateObj* _dayState, int _index) {
	return InsertDayStateBefore(DS_DURATION_TO_MINUTES(_dayState->m_duration),
		_dayState->m_fogStartRange,
		_dayState->m_fogEndRange,
		_dayState->m_fogColorRed,
		_dayState->m_fogColorGreen,
		_dayState->m_fogColorBlue,
		_dayState->m_sunColorRed,
		_dayState->m_sunColorGreen,
		_dayState->m_sunColorBlue,
		_dayState->m_ambientColorRed,
		_dayState->m_ambientColorGreen,
		_dayState->m_ambientColorBlue,
		_dayState->m_sunRotationX,
		_dayState->m_sunRotationY,
		_dayState->m_sunDistance,
		_index);
}

bool CTimeSystemObj::InsertDayStateBefore(float minutes,
	float fogStartRange,
	float fogEndRange,
	float fogColorRed,
	float fogColorGreen,
	float fogColorBlue,
	float sunColorRed,
	float sunColorGreen,
	float sunColorBlue,
	float ambientColorRed,
	float ambientColorGreen,
	float ambientColorBlue,
	float sunRotationX,
	float sunRotationY,
	float sunDistance,
	int _index) {
	if (_index < 0 || _index >= GetNumDayStates()) {
		LogError("Invalid index sent to InsertDaySateBefore: " << _index << "!");
		return false;
	}

	POSITION pos = m_dayStateList->FindIndex(_index);
	if (NULL == pos) {
		LogError("Could not look up index " << _index << " for InsertDayStateBefore!");
		return false;
	}

	CDayStateObj* statePtr = MakeDayState(minutes, fogStartRange, fogEndRange, fogColorRed, fogColorGreen, fogColorBlue,
		sunColorRed, sunColorGreen, sunColorBlue, ambientColorRed, ambientColorGreen, ambientColorBlue,
		sunRotationX, sunRotationY, sunDistance);

	m_dayStateList->InsertBefore(pos, statePtr);
	return true;
}

bool CTimeSystemObj::InsertDayStateAfter(CDayStateObj* _dayState, int _index) {
	return InsertDayStateAfter(DS_DURATION_TO_MINUTES(_dayState->m_duration),
		_dayState->m_fogStartRange,
		_dayState->m_fogEndRange,
		_dayState->m_fogColorRed,
		_dayState->m_fogColorGreen,
		_dayState->m_fogColorBlue,
		_dayState->m_sunColorRed,
		_dayState->m_sunColorGreen,
		_dayState->m_sunColorBlue,
		_dayState->m_ambientColorRed,
		_dayState->m_ambientColorGreen,
		_dayState->m_ambientColorBlue,
		_dayState->m_sunRotationX,
		_dayState->m_sunRotationY,
		_dayState->m_sunDistance,
		_index);
}

bool CTimeSystemObj::InsertDayStateAfter(float minutes,
	float fogStartRange,
	float fogEndRange,
	float fogColorRed,
	float fogColorGreen,
	float fogColorBlue,
	float sunColorRed,
	float sunColorGreen,
	float sunColorBlue,
	float ambientColorRed,
	float ambientColorGreen,
	float ambientColorBlue,
	float sunRotationX,
	float sunRotationY,
	float sunDistance,
	int _index) {
	if (_index < 0 || _index >= GetNumDayStates()) {
		LogError("Invalid index sent to InsertDaySateBefore: " << _index << "!");
		return false;
	}

	POSITION pos = m_dayStateList->FindIndex(_index);
	if (NULL == pos) {
		LogError("Could not look up index " << _index << " for InsertDayStateBefore!");
		return false;
	}

	CDayStateObj* statePtr = MakeDayState(minutes, fogStartRange, fogEndRange, fogColorRed, fogColorGreen, fogColorBlue,
		sunColorRed, sunColorGreen, sunColorBlue, ambientColorRed, ambientColorGreen, ambientColorBlue,
		sunRotationX, sunRotationY, sunDistance);

	m_dayStateList->InsertAfter(pos, statePtr);
	return true;
}

void CTimeSystemObj::UpdateDayState(int _index, const CDayStateObj* _dayState) {
	if (NULL == _dayState) {
		LogError("NULL Day State passed to UpdateState function!");
		return;
	}

	float duration = DS_DURATION_TO_MINUTES(_dayState->m_duration);
	float sunDistance = (float)_dayState->m_sunDistance;

	return UpdateDayState(_index, &duration,
		&_dayState->m_fogStartRange,
		&_dayState->m_fogEndRange,
		&_dayState->m_fogColorRed,
		&_dayState->m_fogColorGreen,
		&_dayState->m_fogColorBlue,
		&_dayState->m_sunColorRed,
		&_dayState->m_sunColorGreen,
		&_dayState->m_sunColorBlue,
		&_dayState->m_ambientColorRed,
		&_dayState->m_ambientColorGreen,
		&_dayState->m_ambientColorBlue,
		&_dayState->m_sunRotationX,
		&_dayState->m_sunRotationY,
		&sunDistance);
}

void CTimeSystemObj::UpdateDayState(int _index, const float* _minutes,
	const float* _fogStartRange,
	const float* _fogEndRange,
	const float* _fogColorRed,
	const float* _fogColorGreen,
	const float* _fogColorBlue,
	const float* _sunColorRed,
	const float* _sunColorGreen,
	const float* _sunColorBlue,
	const float* _ambientColorRed,
	const float* _ambientColorGreen,
	const float* _ambientColorBlue,
	const float* _sunRotationX,
	const float* _sunRotationY,
	const float* _sunDistance) {
	CDayStateObj* dsObj = GetDayState(_index);
	if (NULL == dsObj) {
		LogError("UpdateState could not look up index " << _index << " in Day State List!");
		return;
	}

	if (_minutes) {
		dsObj->m_duration = DS_MINUTES_TO_DURATION(*_minutes);
	}

	if (_fogStartRange) {
		dsObj->m_fogStartRange = *_fogStartRange;
	}

	if (_fogEndRange) {
		dsObj->m_fogEndRange = *_fogEndRange;
	}

	if (_fogColorRed) {
		dsObj->m_fogColorRed = *_fogColorRed;
	}

	if (_fogColorGreen) {
		dsObj->m_fogColorGreen = *_fogColorGreen;
	}

	if (_fogColorBlue) {
		dsObj->m_fogColorBlue = *_fogColorBlue;
	}

	if (_sunColorRed) {
		dsObj->m_sunColorRed = *_sunColorRed;
	}

	if (_sunColorGreen) {
		dsObj->m_sunColorGreen = *_sunColorGreen;
	}

	if (_sunColorBlue) {
		dsObj->m_sunColorBlue = *_sunColorBlue;
	}

	if (_ambientColorRed) {
		dsObj->m_ambientColorRed = *_ambientColorRed;
	}

	if (_ambientColorGreen) {
		dsObj->m_ambientColorGreen = *_ambientColorGreen;
	}

	if (_ambientColorBlue) {
		dsObj->m_ambientColorBlue = *_ambientColorBlue;
	}

	if (_sunRotationX) {
		dsObj->m_sunRotationX = *_sunRotationX;
	}

	if (_sunRotationY) {
		dsObj->m_sunRotationY = *_sunRotationY;
	}

	if (_sunDistance) {
		dsObj->m_sunDistance = *_sunDistance;
	}
}

bool CTimeSystemObj::RemoveDayState(int _index) {
	if (NULL == m_dayStateList) {
		LogError("RemoveDayState called before Day State List initialized!");
		return false;
	}

	POSITION pos = m_dayStateList->FindIndex(_index);
	if (NULL == pos) {
		LogError("RemoveDayState attempting to remove invalid index " << _index << "!");
		return false;
	}

	m_dayStateList->RemoveAt(pos);
	return true;
}

int CTimeSystemObj::GetNumDayStates() {
	if (NULL == m_dayStateList) {
		LogError("GetNumDayStates called before Day State List initialized!");
		return -1;
	}

	return m_dayStateList->GetSize();
}

CDayStateObj* CTimeSystemObj::GetDayState(int _index) {
	if (NULL == m_dayStateList) {
		LogError("GetDayState called before Day State List initialized!");
		return NULL;
	}

	POSITION pos = m_dayStateList->FindIndex(_index);
	if (NULL == pos) {
		LogError("GetDayState attempting to retrieve invalid index " << _index << "!");
		return NULL;
	}

	return (CDayStateObj*)m_dayStateList->GetAt(pos);
}

void CTimeSystemObj::StartSystem() {
	TimeMs curTimeMs = fTime::TimeMs();
	m_timeStamp = curTimeMs;
	m_currentDayTime = 0;
	m_stormCheckTimeStamp = curTimeMs;
	if (m_dayStateList) {
		m_millisecondsPerDay = m_dayStateList->GetCompleteCycleLength();
		if (m_millisecondsPerDay > 0)
			m_currentDayTime = (TimeMs)(((uint64_t)curTimeMs) % (uint64_t)m_millisecondsPerDay);
	}
}

void CTimeSystemObj::AddStorm(long duration,
	long frequency,
	CStringA stormName,
	long stormTransition,
	int particleSysInt) {
	if (!m_stormDB) {
		m_stormDB = new CStormObjList();
	}

	CStormObj* strmPtr = new CStormObj();
	strmPtr->m_stormTransition = stormTransition;
	strmPtr->m_stormName = stormName;
	strmPtr->m_particleSystemRef = particleSysInt;
	strmPtr->m_frequency = frequency;
	strmPtr->m_duration = duration;
	m_stormDB->AddTail(strmPtr);
}

void CTimeSystemObj::UpdateSystem(
	CLightningObjList* lightningEffectSystem,
	Matrix44f soundCenter,
	CSoundBankList* soundManager) {
	TimeMs curTimeMs = fTime::TimeMs();
	m_currentDayTime += (curTimeMs - m_timeStamp);
	m_timeStamp = curTimeMs;

	//evaluate storms
	if (!m_stormPtr) {
		//storm not in use get random storm if random succeeds
		if ((curTimeMs - m_stormCheckTimeStamp) > m_frequencyStormCheck) {
			if (m_stormDB) {
				for (POSITION posLoc = m_stormDB->GetHeadPosition(); posLoc != NULL;) {
					CStormObj* strmPtr = (CStormObj*)m_stormDB->GetNext(posLoc);
					if ((rand() % 100) < strmPtr->m_frequency) {
						strmPtr->Clone(&m_stormPtr);
						m_stormCheckTimeStamp = curTimeMs;
						break;
					}
				}
			}
			m_stormCheckTimeStamp = curTimeMs;
		}
	} //end storm not in use get random storm if random succeeds
	else {
		//storm in use
		//manage lightning strikes
		if (m_stormPtr->m_totalStrikeCountPerMinute > 0) {
			if (m_stormPtr->m_VariationLightningCount > 0) {
				//strike is possible
				if ((curTimeMs - m_stormPtr->m_lightningStamp) > m_stormPtr->m_strikeDuration) {
					//strike check
					int percentage = 100 / (100 / m_stormPtr->m_totalStrikeCountPerMinute);

					if ((rand() % 100) < percentage) {
						//strike succeeded
						int strikeSelection = rand() % m_stormPtr->m_VariationLightningCount;

						if (strikeSelection == m_stormPtr->m_VariationLightningCount) {
							strikeSelection = m_stormPtr->m_VariationLightningCount - 1;
						}

						float stormRadius = m_stormPtr->m_stormRadius;
						Vector3f spawnPosition;
						spawnPosition.x = 0.0f;
						spawnPosition.y = 0.0f;
						spawnPosition.z = 0.0f;
						spawnPosition = CABFunctionLib::GetRandomPointInRadius(stormRadius, spawnPosition, (stormRadius * .5f));
						spawnPosition.y += m_stormPtr->m_strikeSpawnHeight;

						lightningEffectSystem->AddLightningObject(spawnPosition,
							m_stormPtr->m_strikePanelWidth,
							m_stormPtr->m_strikeDepth,
							m_stormPtr->m_strikeVariationArray[strikeSelection].textureByIndex,
							1000,
							TRUE);
						Vector3f localPosition = TransformVector(soundCenter, spawnPosition);
						soundManager->PlayByID(m_stormPtr->m_strikeVariationArray[strikeSelection].m_sndID,
							FALSE,
							localPosition,
							m_stormPtr->m_strikeVariationArray[strikeSelection].m_soundFactor,
							50000.0f);
					} //end strike succeeded

					m_stormPtr->m_lightningStamp = curTimeMs;
				} //end strike check
			} //end strike is possible

			//end manage lightning strikes
			if ((curTimeMs - m_stormCheckTimeStamp) > m_stormPtr->m_duration) {
				//storm timed up
				m_stormPtr->SafeDelete();
				delete m_stormPtr;
				m_stormPtr = 0;
			} //end storm timed up
		} //end storm in use
	} // if..m_totalStrikeCountPerMinute > 0

	//end evaluate storms
	if (!m_dayStateList) { //state database
		return;
	}

	if (m_currentDayTime >= m_millisecondsPerDay) {
		//day
		m_currentSystemDay++;
		m_currentDayTime = 0;
	} //end day

	if (m_currentSystemDay >= m_daysPerMonth) {
		//month
		m_currentSystemDay = 0;
		m_currentSystemMonth++;
	} //end month

	if (m_currentSystemMonth >= m_monthsPerYear) {
		//year
		m_currentSystemMonth = 0;
		m_currentSystemYear++;
	} //end year

	if (!m_pDSO) {
		m_pDSO = new CDayStateObj();
	}

	float m_currentDuration = 0.0f;
	float m_currentTimePassed = 0.0f;

	CDayStateObj* curState = NULL;
	CDayStateObj* targetState = NULL;

	BOOL result = m_dayStateList->GetCurrentDayState(m_currentDayTime,
		&curState,
		&targetState,
		&m_currentDuration,
		&m_currentTimePassed);

	if (result) {
		if (m_currentTimePassed > m_currentDuration)
			m_currentTimePassed = m_currentDuration;

		//fog
		m_pDSO->m_fogColorBlue = MatrixARB::InterpolateFloatsHiRes(curState->m_fogColorBlue, targetState->m_fogColorBlue, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_fogColorRed = MatrixARB::InterpolateFloatsHiRes(curState->m_fogColorRed, targetState->m_fogColorRed, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_fogColorGreen = MatrixARB::InterpolateFloatsHiRes(curState->m_fogColorGreen, targetState->m_fogColorGreen, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_fogEndRange = MatrixARB::InterpolateFloatsHiRes(curState->m_fogEndRange, targetState->m_fogEndRange, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_fogStartRange = MatrixARB::InterpolateFloatsHiRes(curState->m_fogStartRange, targetState->m_fogStartRange, m_currentDuration, m_currentTimePassed);
		//end fog

		//sun
		m_pDSO->m_sunColorBlue = MatrixARB::InterpolateFloatsHiRes(curState->m_sunColorBlue, targetState->m_sunColorBlue, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_sunColorRed = MatrixARB::InterpolateFloatsHiRes(curState->m_sunColorRed, targetState->m_sunColorRed, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_sunColorGreen = MatrixARB::InterpolateFloatsHiRes(curState->m_sunColorGreen, targetState->m_sunColorGreen, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_sunDistance = (long)MatrixARB::InterpolateFloatsHiRes((float)curState->m_sunDistance, (float)targetState->m_sunDistance, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_sunRotationX = MatrixARB::InterpolateFloatsHiRes(curState->m_sunRotationX, targetState->m_sunRotationX, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_sunRotationY = MatrixARB::InterpolateFloatsHiRes(curState->m_sunRotationY, targetState->m_sunRotationY, m_currentDuration, m_currentTimePassed);
		//end sun

		//Ambient Color
		m_pDSO->m_ambientColorRed = MatrixARB::InterpolateFloatsHiRes(curState->m_ambientColorRed, targetState->m_ambientColorRed, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_ambientColorGreen = MatrixARB::InterpolateFloatsHiRes(curState->m_ambientColorGreen, targetState->m_ambientColorGreen, m_currentDuration, m_currentTimePassed);
		m_pDSO->m_ambientColorBlue = MatrixARB::InterpolateFloatsHiRes(curState->m_ambientColorBlue, targetState->m_ambientColorBlue, m_currentDuration, m_currentTimePassed);
		//end Ambient Color
	}
}

void CTimeSystemObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << (DWORD)0 //m_timeStamp
		   << m_currentSystemDay
		   << m_currentSystemMonth
		   << m_currentSystemYear
		   << (int)m_currentDayTime
		   << m_monthsPerYear
		   << m_daysPerMonth
		   << (int)m_millisecondsPerDay
		   << m_dayStateList
		   << m_visual
		   << m_frequencyStormCheck
		   << m_stormDB;

	} else {
		int ms;
		int dt;
		DWORD curTime;
		ar >> curTime //m_timeStamp
			>> m_currentSystemDay >> m_currentSystemMonth >> m_currentSystemYear >> dt >> m_monthsPerYear >> m_daysPerMonth >> ms >> m_dayStateList >> m_visual >> m_frequencyStormCheck >> m_stormDB;
		m_timeStamp = (TimeMs)curTime;
		m_millisecondsPerDay = ms;
		m_currentDayTime = dt;
		m_currentSunPosition.x = 0.0f;
		m_currentSunPosition.y = 0.0f;
		m_currentSunPosition.z = 0.0f;

		if (m_dayStateList)
			m_dayStateList->m_lastDayState = NULL;

		m_stormInUse = FALSE;
		m_stormPtr = NULL;
		m_stormCheckTimeStamp = 0;
	}
}

IMPLEMENT_SERIAL(CTimeSystemObj, CObject, 0)

BEGIN_GETSET_IMPL(CTimeSystemObj, IDS_TIMESYSTEMOBJ_NAME)
GETSET_RANGE(m_monthsPerYear, gsLong, GS_FLAGS_DEFAULT, 1, 4, TIMESYSTEMOBJ, MONTHSPERYEAR, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_daysPerMonth, gsLong, GS_FLAGS_DEFAULT, 1, 4, TIMESYSTEMOBJ, DAYSPERMONTH, LONG_MIN, LONG_MAX)
GETSET_OBLIST_PTR(m_dayStateList, GS_FLAGS_DEFAULT, 2, 5, TIMESYSTEMOBJ, DAYSTATELIST)
GETSET_OBJ_PTR(m_visual, GS_FLAGS_DEFAULT, 2, 9, TIMESYSTEMOBJ, VISUAL, CEXMeshObj)
GETSET_OBLIST_PTR(m_stormDB, GS_FLAGS_DEFAULT, 2, 6, TIMESYSTEMOBJ, STORMDB)
END_GETSET_IMPL

} // namespace KEP