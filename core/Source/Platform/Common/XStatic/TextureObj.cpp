///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "textureObj.h"

#ifdef _ClientOutput
#include "IClientEngine.h"
#include "DxDevice.h"
#endif

#include "TextureManager.h"
#include "Common\include\CompressHelper.h"
#include "DownloadPriorityDistanceModel.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

// Texture manager link
TextureRM* CTextureEX::ms_pTextureRM = NULL;

// Predefined available quality levels
std::map<TextureQuality, std::string> CTextureEX::ms_availQualityLevels{
	{ TEX_LOW, "005" },
	{ TEX_MED, "025" },
	{ TEX_HIGH, "050" },
	{ TEX_DDS, "" }, // TEX_DDS does not use quality token
};

// Enabled quality levels for current client session -- to be set at runtime
std::map<TextureQuality, std::string> CTextureEX::ms_enabledQualityLevels;

bool CTextureEX::ms_forceOnePixel = false;
bool CTextureEX::ms_priorityVisualizationEnabled = false;
std::vector<IDirect3DTexture9*> CTextureEX::ms_priorityVisTex;

CTextureEX::CTextureEX(D3DCOLOR colorKey) :
		IResource(ms_pTextureRM, 0),
		m_pTexture(nullptr),
		m_colorKey(colorKey),
		m_encryptionHashOverride(0),
		m_altColor(0),
		m_altColorSet(false),
		m_pTextureOnePixel(nullptr),
		m_lowerRes(nullptr),
		m_higherRes(nullptr),
		m_activeRes(nullptr),
		m_quality(TEX_NON),
		m_chainHead(false),
		m_chainReady(false),
		m_transcodeJPG(false),
		m_forceFullRes(false),
		m_priorityRangeSelector(0) {
	memset(&m_texSurfaceDesc, 0, sizeof(m_texSurfaceDesc));
}

CTextureEX::CTextureEX(const std::string& texName, const std::string& subFolder, eTexturePath texPath, const std::string& pathOverride, const std::string& urlOverride, D3DCOLOR colorKey) :
		IResource(ms_pTextureRM, 0),
		m_pTexture(nullptr),
		m_colorKey(colorKey),
		m_texPathToUse(texPath),
		m_encryptionHashOverride(0),
		m_altColor(0),
		m_altColorSet(false),
		m_pTextureOnePixel(nullptr),
		m_lowerRes(nullptr),
		m_higherRes(nullptr),
		m_activeRes(nullptr),
		m_quality(TEX_NON),
		m_chainHead(false),
		m_chainReady(false),
		m_transcodeJPG(false),
		m_forceFullRes(false),
		m_priorityRangeSelector(0) {
	memset(&m_texSurfaceDesc, 0, sizeof(D3DSURFACE_DESC));

	// Set Texture Path
	std::string absPath;
	if (!pathOverride.empty())
		absPath = PathAdd(pathOverride, texName);
	else
		absPath = PathAdd(PathAdd(IResource::BaseTexturePath(m_texPathToUse), subFolder), texName);
	SetFilePath(absPath);

	// Set Texture Url
	if (!urlOverride.empty())
		SetURL(urlOverride);
	else {
#ifdef _ClientOutput
		CStringA urlFolder("MapsModels_");
		if (!subFolder.empty()) {
			urlFolder = urlFolder + subFolder.c_str();
			urlFolder.Replace("\\", "_");
			if (urlFolder.Right(1) == "_")
				urlFolder = urlFolder.Left(urlFolder.GetLength() - 1) + "/";
		}
		std::string url;
		StrBuild(url, ResourceManager::GetPatchAssetLocation() << urlFolder << texName);
		url = ::CompressTypeExtAdd(url);
		SetURL(url);
#endif
	}
}

CTextureEX::CTextureEX(const CTextureEX& rhs) :
		IResource(rhs),
		m_pTexture(rhs.m_pTexture),
		m_texSurfaceDesc(rhs.m_texSurfaceDesc),
		m_colorKey(rhs.m_colorKey),
		m_texPathToUse(rhs.m_texPathToUse),
		m_encryptionHashOverride(0),
		m_altColor(0),
		m_altColorSet(false),
		m_pTextureOnePixel(nullptr),
		m_lowerRes(nullptr),
		m_higherRes(nullptr),
		m_activeRes(nullptr),
		m_quality(TEX_NON),
		m_chainHead(false),
		m_chainReady(false),
		m_transcodeJPG(false),
		m_priorityRangeSelector(0) {}

CTextureEX::~CTextureEX() {
#ifdef _ClientOutput
	// Call ClientEngine::OnTextureDestroy() required to remove texture from TextureDatabase::m_texMap
	auto pICE = IClientEngine::Instance();
	if (pICE)
		pICE->OnTextureDestroy(this);
#endif

	// Unload Texture To Free D3D Memory
	UnloadTexture();
}

std::string CTextureEX::ToStr() const {
	std::string str;
	StrBuild(str, "TEX<'" << LogPath(GetFilePath()) << "'"
						  << " (" << GetWidth() << " x " << GetHeight() << ")"
						  << " bytes=" << GetTextureMemoryUsage()
						  << " fmt=x" << std::hex << GetFormat()
						  << (IsErrored() ? " ERRORED" : "")
						  << ">");
	return str;
}

// static
void CTextureEX::ClearAllTextureMemoryUsage() {
#ifdef _ClientOutput
	auto pDxD = DxDevice::Ptr();
	if (pDxD)
		pDxD->ClearAllTextureMemoryUsage();
#endif
}

// static
ULONGLONG CTextureEX::GetAllTextureMemoryUsage() {
#ifdef _ClientOutput
	auto pDxD = DxDevice::Ptr();
	return pDxD ? pDxD->GetAllTextureMemoryUsage() : 0;
#else
	return 0;
#endif
}

ULONGLONG CTextureEX::GetTextureMemoryUsage() const {
#ifdef _ClientOutput
	return DxDevice::GetTextureMemoryUsage(m_texSurfaceDesc);
#else
	return 0;
#endif
}

IDirect3DTexture9* CTextureEX::GetD3DTextureIfLoadedOrQueueToLoad(bool bImmediate) {
#ifdef _ClientOutput
	// Already Created ?
	if (StateIsLoadedAndCreated())
		return m_pTexture;

	// Ready To Read ?
	if (!StateIsInitializedNotLoaded())
		return nullptr;

	// Read Immediately Or Queue Asynchronously ?
	if (bImmediate) {
		if (TextureRM::Instance()->QueueFileForRead(this, true, true) && StateIsLoadedAndCreated())
			return m_pTexture;
	} else {
		TextureRM::Instance()->QueueResource(this);
	}
#endif

	return nullptr;
}

IDirect3DTexture9* CTextureEX::GetOrQueueTexture() {
	return GetD3DTextureIfLoadedOrQueueToLoad(false);
}

void CTextureEX::UnloadTexture() {
	if (!m_ResourceManagerData.TryUnload())
		return; // Can't unload textures that are in the process of loading. Need to remove from ResourceManager queue first.

#ifdef _ClientOutput
	// DRF - ED-7035 - Investigate texture memory impact
	// Call ClientEngine::OnTextureUnload() required to also release texture from KepMenu
	auto pICE = IClientEngine::Instance();
	if (pICE)
		pICE->OnTextureUnload(this);
#endif

	// Reset State & Error
	SetState(IResource::State::INITIALIZED_NOT_LOADED);
	SetError(IResource::Error::ERROR_NONE);

#ifdef _ClientOutput
	// Unload Texture From DxDevice
	auto pDxD = DxDevice::Ptr();
	if (pDxD)
		pDxD->UnloadTexture(m_pTexture, m_texSurfaceDesc);
#endif

	// Free D3D Texture Memory
	m_pTexture.Release();
	m_pTextureOnePixel.Release();
}

void CTextureEX::Callback(LPDIRECT3DDEVICE9 pd3dDevice, int stage, int& type, float& parameter) {
	// used for texture animation and such
}

eCreateResourceResult CTextureEX::CreateResourceEx() {
#ifdef _ClientOutput

	if (m_pTexture)
		return eCreateResourceResult::SUCCESS;

	if (!GetRawBuffer())
		return eCreateResourceResult::FAIL;

	//	Raw texture data loaded, create D3DX resource
	D3DRESOURCETYPE resourcetype = D3DRTYPE_TEXTURE;
	DxDevice::eCreateTextureResult res = DxDevice::Ptr()->CreateTextureFromFileInMemoryEx(
		GetFileName(),
		GetRawBuffer(),
		(UINT)GetRawSize(),
		&m_pTexture,
		m_texSurfaceDesc,
		resourcetype,
		true,
		D3DX_DEFAULT,
		D3DX_DEFAULT,
		GetColorKey(),
		GetForceFullRes(),
		GetTranscodeJPG(),
		false);
	if (res == DxDevice::eCreateTextureResult::SUCCESS)
		return eCreateResourceResult::SUCCESS;
	if (res == DxDevice::eCreateTextureResult::RETRY)
		return eCreateResourceResult::RETRY;
	return eCreateResourceResult::FAIL;
#else
	return eCreateResourceResult::FAIL;
#endif
}

// WARN - Not Added To Texture DB - Must Unload Yourself!
bool CTextureEX::LoadTextureByName(const std::string& texName, int mips, bool managed, bool UseTexLod, D3DCOLOR colorKey) {
#ifdef _ClientOutput
	// Set Texture Attributes
	SetColorKey(colorKey);
	SetFilePath(PathAdd(IResource::BaseTexturePath(m_texPathToUse), texName));

	// Read Resource File (immediate or async download)
	return TextureRM::Instance()->QueueFileForRead(this, true, true);
#else
	return false;
#endif
}

// WARN - Not Added To Texture DB - Must Unload Yourself!
bool CTextureEX::LoadTextureByPath(const std::string& texPath, int mips, bool UseTexLod, D3DCOLOR colorKey, bool attemptAsyncDownload) {
#ifdef _ClientOutput
	// Set Texture Attributes
	SetColorKey(colorKey);
	SetFilePath(texPath);

	// Read Resource File (immediate or possibly async download)
	return TextureRM::Instance()->QueueFileForRead(this, true, attemptAsyncDownload);
#else
	return false;
#endif
}

unsigned char* CTextureEX::GetEncryptionHashOverride(int& hash_len) {
	if (m_encryptionHashOverride == 0)
		return IResource::GetEncryptionHashOverride(hash_len);
	hash_len = sizeof(m_encryptionHashOverride);
	return (unsigned char*)&m_encryptionHashOverride;
}

void CTextureEX::InitChain() {
	m_chainHead = true;
	m_quality = TEX_DDS;
}

void CTextureEX::ChainToLowerRes(CTextureEX* pLowerResTex, TextureQuality quality) {
	// Make sure the low-res texture is not in any of the chains
	pLowerResTex->m_quality = quality;

	// Look for an insertion point
	auto pTex = this;
	while (pTex->m_lowerRes) {
		if (pTex->m_lowerRes->m_quality < pLowerResTex->m_quality)
			break;
		pTex = pTex->m_lowerRes;
	}

	if (pTex->m_lowerRes != nullptr) {
		pTex->m_lowerRes->m_higherRes = pLowerResTex;
		pLowerResTex->m_lowerRes = pTex->m_lowerRes;
	}
	pTex->m_lowerRes = pLowerResTex;
	pLowerResTex->m_higherRes = pTex;

	// Reset active link in case we entered render code before chaining
	m_activeRes = nullptr;
}

void CTextureEX::ChainBootstrap() {
	if (IsForcingOnePixel())
		return;

	// Forward traversal: highest -> lowest res
	// Traverse all textures with quality within range [min, max] until we find one that already exists on the disk.
	auto pTex = this;
	for (; pTex->m_lowerRes != nullptr; pTex = pTex->m_lowerRes) {
		if (FileHelper::Exists(pTex->GetFilePath())) // Stop at first available texture, if any
			break;
	}

	m_activeRes = pTex;

	// Set download priorities: current resource gets default priority. Higher res -> lower priority.
	// Reverse traversal: curr -> highest res
	unsigned currPrioRangeSel = 0;
	for (; pTex != nullptr; pTex = pTex->m_higherRes, currPrioRangeSel++) {
		pTex->m_priorityRangeSelector = currPrioRangeSel; // First available texture has factor of zero
	}
}

// ED-7733 - Load all DO textures before you look at them
void CTextureEX::ChainLoad() {
	if (IsForcingOnePixel())
		return;

	// Load All Unloaded Textures In Chain
	auto pTex = this;
	for (; pTex->m_lowerRes; pTex = pTex->m_lowerRes) {
	}
	for (; pTex; pTex = pTex->m_higherRes) {
		if (pTex->StateIsInitializedNotLoaded())
			pTex->GetD3DTextureIfLoadedOrQueueToLoad();
	}
}

IDirect3DTexture9* CTextureEX::getTexturePtr(bool chainLoad) {
	if (IsChainHead()) {
		// Texture chain
		if (!m_chainReady) {
			// First time use - scan local disk for best resolution available
			m_chainReady = true;
			ChainBootstrap();
		}

		// ED-7733 - Load all DO textures before you look at them
		if (chainLoad)
			ChainLoad();
	} else {
		// Singular texture: use self
		m_activeRes = this;
	}

	IDirect3DTexture9* pTex = nullptr;
	if (m_activeRes) {
		if (m_activeRes->StateIsLoadedAndCreated()) {
			// Possible to move to higher quality ?
			if (m_activeRes->m_higherRes && (m_activeRes->m_higherRes->m_quality != TEX_DDS || IsQualityLevelEnabled(TEX_DDS))) {
				switch (m_activeRes->m_higherRes->GetState()) {
					case IResource::State::INITIALIZED_NOT_LOADED:
						// Trigger it to load
						m_activeRes->m_higherRes->GetD3DTextureIfLoadedOrQueueToLoad();
						break;

					case IResource::State::LOADED_AND_CREATED:
						// Trade up
						m_activeRes->UnloadTexture(); // Unload active texture to save d3d memory
						m_activeRes = m_activeRes->m_higherRes; // Switch to higher quality
						break;
				}
			}
		}

		bool canLoad = (!IsForcingOnePixel() || !m_altColorSet);
		if (canLoad)
			pTex = m_activeRes->GetD3DTextureIfLoadedOrQueueToLoad();
	}

	// Not ready or error - try alternative color
	if (!pTex) {
		if (ms_priorityVisualizationEnabled) {
			// Visualize loading priority with color
			pTex = getTextureForPriorityVisualization();
		} else if (m_altColorSet) {
			// Otherwise use average texture color
			pTex = getTextureForAverageColor();
		}
	}

	return pTex;
}

// static
bool CTextureEX::EnableQualityLevel(TextureQuality quality) {
	auto itr = ms_availQualityLevels.find(quality);
	if (itr != ms_availQualityLevels.end()) {
		ms_enabledQualityLevels.insert(std::make_pair(quality, itr->second));
		return true;
	}
	return false;
}

IDirect3DTexture9* CTextureEX::getTextureForAverageColor() {
	if (!m_pTextureOnePixel) {
		// I'm going to construct a one-pixel texture here. Not sure if there is a better way to do it without using shader.
		m_pTextureOnePixel = createSingleColorTexture(m_altColor);
		if (!m_pTextureOnePixel) {
			// Failed
			m_altColorSet = false; // Reset the flag so we will not try again
		}
	}

	return m_pTextureOnePixel;
}

IDirect3DTexture9* CTextureEX::getTextureForPriorityVisualization() {
	if (ms_priorityVisTex.empty()) {
		// Determine number of colors required
		unsigned minBucketIndex = GetDownloadPriorityModel().getPriorityBucket(ResourceType::DYNAMIC_OBJECT, 0, 0);
		unsigned maxBucketIndex = GetDownloadPriorityModel().getPriorityBucket(ResourceType::DYNAMIC_OBJECT, 0, 2000); // Use different color for everything within 2000 units away
		unsigned numColors = maxBucketIndex - minBucketIndex + 1;

		//LogInfo("Creating textures for " << numColors << " priority buckets");
		for (unsigned i = 0; i < numColors; i++) {
			unsigned r = 0xFF, g = 0xFF;
			if (i < numColors / 2) {
				// First half (#0 ~ #15): RED to YELLOW
				g = 0xFF * i / (numColors / 2 - 1);
			} else if (i >= (numColors + 1) / 2) {
				// Second half (#16 ~ #31): YELLOW to GREEN
				r = 0xFF - 0xFF * (i - (numColors + 1) / 2) / (numColors / 2 - 1);
			}
			unsigned color = (r << 16) | (g << 8);
			ms_priorityVisTex.push_back(createSingleColorTexture(color));
		}
	}

	if (ms_priorityVisTex.empty())
		return nullptr;

	// Select from one of the colors from the hue gradient between RED (closest) and GREEN (furthest)
	// Visualize with priority of the owning dynamic object to avoid complexities
	unsigned bucket = GetDownloadPriorityModel().getPriorityBucket(ResourceType::DYNAMIC_OBJECT, 0, GetCamDistance());
	if (bucket > ms_priorityVisTex.size() - 1)
		bucket = ms_priorityVisTex.size() - 1;

	return ms_priorityVisTex[bucket];
}

IDirect3DTexture9* CTextureEX::createSingleColorTexture(unsigned color) {
	G_DEVICE(nullptr);

	IDirect3DTexture9* pTexture = nullptr;
	HRESULT hrCreate = S_OK, hrLock = S_OK;
	hrCreate = gDevice->CreateTexture(1, 1, 1, 0, D3DFMT_X8R8G8B8, D3DPOOL_MANAGED, &pTexture, NULL);
	if (hrCreate == D3D_OK) {
		if (pTexture) {
			D3DLOCKED_RECT lockRect = { 0, 0 };
			hrLock = pTexture->LockRect(0, &lockRect, NULL, 0);
			if (hrLock == D3D_OK)
				*((unsigned*)lockRect.pBits) = color & 0x00FFFFFF;
			pTexture->UnlockRect(0);
		}
	}

	return pTexture;
}

} // namespace KEP
