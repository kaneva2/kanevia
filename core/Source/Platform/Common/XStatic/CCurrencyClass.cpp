///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <afxtempl.h>

#include "CCurrencyClass.h"

namespace KEP {

CCurrencyObj::CCurrencyObj() {
	m_currencyName = "CREDITS";
	m_amount = 0;
}

void CCurrencyObj::Clone(CCurrencyObj** clone) {
	CCurrencyObj* copyLocal = new CCurrencyObj();
	copyLocal->m_amount = m_amount;
	copyLocal->m_currencyName = m_currencyName;

	*clone = copyLocal;
}

void CCurrencyObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_currencyName << m_amount;

	} else {
		ar >> m_currencyName >> m_amount;
	}
}

void CCurrencyObj::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_currencyName << m_amount;

	} else {
		ar >> m_currencyName >> m_amount;
	}
}

IMPLEMENT_SERIAL(CCurrencyObj, CObject, 0)

BEGIN_GETSET_IMPL(CCurrencyObj, IDS_CURRENCYOBJ_NAME)
GETSET_MAX(m_currencyName, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, CURRENCYOBJ, CURRENCYNAME, STRLEN_MAX)
GETSET_RANGE(m_amount, gsLong, GS_FLAGS_DEFAULT, 0, 0, CURRENCYOBJ, CURRENCYAMT, 0, LONG_MAX)
END_GETSET_IMPL

} // namespace KEP