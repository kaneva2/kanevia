///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CWaterZoneClass.h"
#include "CEXMeshClass.h"
#include "IAssetsDatabase.h"
#include "AssetTree.h"

namespace KEP {

CWaterZoneObj::CWaterZoneObj() {
	ZeroMemory(&m_waterArea, sizeof(ABBOX));
	m_isExclusionZone = FALSE;
}

BOOL CWaterZoneObj::InitFromARW(CStringA filename, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CEXMeshObj* newMesh;
	if (0 == (newMesh = IAssetsDatabase::Instance()->ImportStaticMesh(filename)))
		return FALSE;

	newMesh->InitAfterLoad(g_pd3dDevice);

	CopyMemory(&m_waterArea, &newMesh->GetBoundingBox(), sizeof(ABBOX));

	newMesh->SafeDelete();
	delete newMesh;
	newMesh = 0;

	return TRUE;
}

BOOL CWaterZoneObj::BoundBoxPointCheck(Vector3f& point, ABBOX box) const {
	if (point.x >= box.minX &&
		point.x <= box.maxX &&
		point.y >= box.minY &&
		point.y <= box.maxY &&
		point.z >= box.minZ &&
		point.z <= box.maxZ)
		return TRUE;
	return FALSE;
}

BOOL CWaterZoneObj::IsPositionInZone(Vector3f position) const {
	return BoundBoxPointCheck(position, m_waterArea);
}

void CWaterZoneObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_waterArea.maxX
		   << m_waterArea.maxY
		   << m_waterArea.maxZ
		   << m_waterArea.minX
		   << m_waterArea.minY
		   << m_waterArea.minZ
		   << m_isExclusionZone
		   << m_waterZoneName;
	} else {
		ar >> m_waterArea.maxX >> m_waterArea.maxY >> m_waterArea.maxZ >> m_waterArea.minX >> m_waterArea.minY >> m_waterArea.minZ >> m_isExclusionZone >> m_waterZoneName;
	}
}

BOOL CWaterZoneObjList::IsPositionInWater(Vector3f position, float* submergedValue) const {
	BOOL inWater = FALSE;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWaterZoneObj* spnPtr = (CWaterZoneObj*)GetNext(posLoc);
		BOOL result = spnPtr->IsPositionInZone(position);
		if (result == TRUE && spnPtr->m_isExclusionZone == TRUE)
			return FALSE; //exclusion ovverrides

		if (result == TRUE) {
			*submergedValue = position.y - spnPtr->m_waterArea.maxY;
			inWater = TRUE;
		}
	}
	return inWater;
}

void CWaterZoneObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWaterZoneObj* spnPtr = (CWaterZoneObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

IMPLEMENT_SERIAL(CWaterZoneObj, CObject, 0)
IMPLEMENT_SERIAL(CWaterZoneObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CWaterZoneObj, IDS_WATERZONEOBJ_NAME)
GETSET(m_isExclusionZone, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, WATERZONEOBJ, ISEXCLUSIONZONE)
GETSET_MAX(m_waterZoneName, gsCString, GS_FLAGS_DEFAULT, 0, 0, WATERZONEOBJ, WATERZONENAME, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP