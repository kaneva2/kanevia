///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#define D3D_OVERLOADS
#include <afxtempl.h>

#include "CCameraClass.h"

#include "IClientEngine.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/TransformUtil.h"
#include "Core/Math/KEPMathUtil.h"
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9math.h>

#include "Common\include\glAdjust.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

// Startup Glitch On First Copy Of 'DefaultCamera_3'
static bool StartupGlitch(CCameraObj* pCO) {
	static bool s_startupGlitch = true;
	if (STLContains(pCO->m_cameraName, "DefaultCamera_3") && s_startupGlitch) {
		s_startupGlitch = false;
		return true;
	}
	return false;
}

CCameraObjList* CCameraObjList::m_pInstance = nullptr;

CCameraObj::CCameraObj(
	const std::string& cameraName,
	float farPlaneDist,
	float fovY,
	Vector3f initFloatingTarget) :
		m_cameraName(cameraName),
		m_attachAuto(true),
		m_attachObjType(ObjectType::NONE),
		m_attachObjId(0),
		m_camWorldVectorX(1.0f, 0.0f, 0.0f),
		m_camWorldVectorY(0.0f, 1.0f, 0.0f),
		m_camWorldVectorZ(0.0f, 0.0f, 1.0f),
		m_camWorldPosition(0.0f, 0.0f, 0.0f),
		//	m_fovY_old(fovView),
		m_fovY(fovY),
		m_nearPlaneDist(1.0f),
		m_farPlaneDist(farPlaneDist),
		m_inCameraRotMode(false),
		m_useCollision(TRUE),
		m_floatingCamTarget(initFloatingTarget),
		m_initialPos(0.0, 0.0, 0.0),
		m_orbitAziInit(0.0f),
		m_orbitEleInit(0.0f),
		m_orbitAzimuth(0.0f),
		m_orbitElevation(0.0f),
		m_orbitFocus(0.0f, 5.0f, 0.0f),
		m_orbitMinDist(5.0f), // ED-7611 - Camera Should Not Clip My Avatar
		m_orbitMaxDist(30.0f),
		m_orbitStartDist(20.0f),
		m_orbitDist(20.0f),
		m_orbitDistStep(0.1f),
		m_orbitAutoFollow(true),
		m_orbitAutoAzimuth(DegreesToRadians(0.0)), // strict - straight ahead
		m_orbitAutoElevation(DegreesToRadians(20.0)), // lazy - 20%
		m_heightBaseAspect(1.0f),
		m_baseFrame(nullptr),
		m_floatingCamParentFrame(nullptr),
		m_floatingFocusFrame(nullptr),
		m_pitchRotation(0.0f),
		m_cameraScale(1.0f, 1.0f, 1.0f) {}

// Copy all except name and FrameObjs
CCameraObj& CCameraObj::operator=(const CCameraObj& rhs) {
	m_attachAuto = rhs.m_attachAuto;
	m_attachObjType = rhs.m_attachObjType;
	m_attachObjId = rhs.m_attachObjId;
	m_camWorldVectorX = rhs.m_camWorldVectorX;
	m_camWorldVectorY = rhs.m_camWorldVectorY;
	m_camWorldVectorZ = rhs.m_camWorldVectorZ;
	m_camWorldPosition = rhs.m_camWorldPosition;
	m_fovY = rhs.m_fovY;
	m_nearPlaneDist = rhs.m_nearPlaneDist;
	m_farPlaneDist = rhs.m_farPlaneDist;
	m_inCameraRotMode = rhs.m_inCameraRotMode;
	m_useCollision = rhs.m_useCollision;
	m_floatingCamTarget = rhs.m_floatingCamTarget;
	m_initialPos = rhs.m_initialPos;
	m_orbitAziInit = rhs.m_orbitAziInit;
	m_orbitEleInit = rhs.m_orbitEleInit;
	m_orbitAzimuth = rhs.m_orbitAzimuth;
	m_orbitElevation = rhs.m_orbitElevation;
	m_orbitFocus = rhs.m_orbitFocus;
	m_orbitMinDist = rhs.m_orbitMinDist;
	m_orbitMaxDist = rhs.m_orbitMaxDist;
	m_orbitStartDist = rhs.m_orbitStartDist;
	m_orbitDist = rhs.m_orbitDist;
	m_orbitDistStep = rhs.m_orbitDistStep;
	m_orbitAutoFollow = rhs.m_orbitAutoFollow;
	m_orbitAutoAzimuth = rhs.m_orbitAutoAzimuth;
	m_orbitAutoElevation = rhs.m_orbitAutoElevation;
	m_heightBaseAspect = rhs.m_heightBaseAspect;
	m_pitchRotation = rhs.m_pitchRotation;
	m_cameraScale = rhs.m_cameraScale;
	ObjectAttachUpdate();
	if (!StartupGlitch(this))
		UpdatePosition();
	return *this;
}

void CCameraObj::SafeDelete() {
	ObjectAttachSetVisibility(true);

	if (m_baseFrame) {
		m_baseFrame->SafeDelete();
		delete m_baseFrame;
		m_baseFrame = nullptr;
	}

	if (m_floatingCamParentFrame) {
		m_floatingCamParentFrame->SafeDelete();
		delete m_floatingCamParentFrame;
		m_floatingCamParentFrame = nullptr;
	}

	if (m_floatingFocusFrame) {
		m_floatingFocusFrame->SafeDelete();
		delete m_floatingFocusFrame;
		m_floatingFocusFrame = nullptr;
	}
}

bool CCameraObj::ObjectAttach(const ObjectType& objType, int objId) {
	m_attachAuto = false;
	ObjectAttachSetNeedsUpdate(false);
	ObjectAttachSetVisibility(true);
	m_attachAuto = true;
	m_attachObjType = objType;
	m_attachObjId = objId;
	ObjectAttachSetNeedsUpdate(true);
	return ObjectAttachUpdate();
}

void CCameraObj::ObjectAttachGet(ObjectType& objType, int& objId) const {
	objType = (m_attachObjType == ObjectType::DYNAMIC) ? ObjectType::DYNAMIC : ObjectType::MOVEMENT;
	objId = m_attachObjId;
}

bool CCameraObj::ObjectAttachToMe() {
	return ObjectAttach(ObjectType::MOVEMENT, 0);
}

bool CCameraObj::ObjectAttachIsMe() const {
	ObjectType objType;
	int objId;
	ObjectAttachGet(objType, objId);
	auto pICE = IClientEngine::Instance();
	return pICE ? pICE->ObjectIsMe(objType, objId) : true;
}

CFrameObj* CCameraObj::ObjectAttachGetBaseFrame() {
	ObjectType objType;
	int objId;
	ObjectAttachGet(objType, objId);
	auto pICE = IClientEngine::Instance();
	auto pFO = pICE ? pICE->ObjectGetBaseFrame(objType, objId, true) : nullptr; // use posses
	if (!pFO && objId && m_attachAuto) {
		ObjectAttachToMe();
		pFO = ObjectAttachGetBaseFrame();
	}
	return pFO;
}

bool CCameraObj::ObjectAttachSetVisibility(bool visible) {
	ObjectType objType;
	int objId;
	ObjectAttachGet(objType, objId);
	auto pICE = IClientEngine::Instance();
	return pICE ? pICE->ObjectSetVisibility(objType, objId, visible) : false;
}

// ED-8445 - Force Object Updates If Camera Attached
bool CCameraObj::ObjectAttachSetNeedsUpdate(bool update) {
	ObjectType objType;
	int objId;
	ObjectAttachGet(objType, objId);
	auto pICE = IClientEngine::Instance();
	return pICE ? pICE->ObjectSetNeedsUpdate(objType, objId, update) : false;
}

bool CCameraObj::ObjectAttachUpdate() {
	if (!m_baseFrame)
		return false;

	LogInfo(ToStr());

	auto pFO = ObjectAttachGetBaseFrame();
	if (!pFO)
		return false;

	Matrix44f matrixLoc;
	ConvertRotationY(ToRadians(180.0f), out(matrixLoc));
	m_baseFrame->AddTransformAfter(matrixLoc);

	if (m_floatingCamParentFrame) {
		delete m_floatingCamParentFrame;
		m_floatingCamParentFrame = nullptr;
	}
	m_floatingCamParentFrame = new CFrameObj();
	m_floatingCamParentFrame->SetPosition(Vector3f(0.0f, 0.0f, 0.0f), pFO);

	m_baseFrame->SetPosition(m_initialPos, m_floatingCamParentFrame);

	if (m_floatingFocusFrame) {
		delete m_floatingFocusFrame;
		m_floatingFocusFrame = nullptr;
	}
	m_floatingFocusFrame = new CFrameObj();
	m_floatingFocusFrame->SetPosition(m_floatingCamTarget, pFO);

	return false;
}

void CCameraObj::EngineCameraCallback() {
	if (!m_baseFrame)
		return;
	Matrix44f matrixBase, adjust;
	m_baseFrame->GetTransform(NULL, &matrixBase);
	ConvertRotationX(ToRadians(m_pitchRotation), out(adjust));
	matrixBase = adjust * matrixBase;
	m_camWorldVectorX = matrixBase[0].SubVector<3>();
	m_camWorldVectorY = matrixBase[1].SubVector<3>();
	m_camWorldVectorZ = matrixBase[2].SubVector<3>();
	m_camWorldPosition = matrixBase[3].SubVector<3>();
}

bool CCameraObj::AutoFollow() {
	if (!m_orbitAutoFollow)
		return false;

	TimeSec frameTimeSec = RenderMetrics::GetFrameTimeMs() / MS_PER_SEC;

	// Auto Azimuth
	auto orbitAzimuth = fabs(m_orbitAzimuth);
	if (orbitAzimuth > m_orbitAutoAzimuth) {
		float delta = (2.0 * D3DX_PI) * frameTimeSec; // 360/sec
		if (delta > (orbitAzimuth - m_orbitAutoAzimuth))
			delta = (orbitAzimuth - m_orbitAutoAzimuth);
		m_orbitAzimuth += m_orbitAzimuth > 0 ? -delta : delta;
	}

	// ED-7506 - Auto Elevation
	auto orbitElevation = fabs(m_orbitElevation);
	if (orbitElevation > m_orbitAutoElevation) {
		float delta = D3DX_PI * frameTimeSec; // 180/sec
		if (delta > (orbitElevation - m_orbitAutoElevation))
			delta = (orbitElevation - m_orbitAutoElevation);
		m_orbitElevation += (m_orbitElevation > 0) ? -delta : delta;
	}

	return true;
}

static float CameraClamp(float val, float low, float high) {
	if (val <= low)
		return low;
	else if (val >= high)
		return high;
	else
		return val;
}

// if the azimuth is greater than 180 degrees, then
// compute a new azimuth that rotates in the other
// direction.  this is needed so that the auto-follow
// code always takes the shortest path back to
// directly behind the player.
void CCameraObj::AdjustAzimuth(float rad) {
	m_orbitAzimuth += rad;
	m_orbitAzimuth = fmodf(m_orbitAzimuth, 2 * D3DX_PI);
	if (m_orbitAzimuth > D3DX_PI)
		m_orbitAzimuth = -2 * D3DX_PI + m_orbitAzimuth;
	else if (m_orbitAzimuth < -D3DX_PI)
		m_orbitAzimuth = 2 * D3DX_PI + m_orbitAzimuth;
}

// ED-7611 - Camera Should Not Clip My Avatar
void CCameraObj::AdjustElevation(float rad) {
	m_orbitElevation += rad;
	m_orbitElevation = CameraClamp(m_orbitElevation, DegreesToRadians(-60.0), DegreesToRadians(90.0));
}

// Step size for one mouse wheel click on my mouse is 10.0
void CCameraObj::AdjustDist(float step) {
	float amount = step * m_orbitDistStep;
	auto orbitDist = GetDist();
	if (orbitDist < m_orbitMinDist) {
		if (amount <= 0.0)
			orbitDist = 0.0;
		else
			orbitDist = m_orbitMinDist;
	} else {
		orbitDist += amount;
		if (orbitDist < m_orbitMinDist)
			orbitDist = 0.0;
		else if (orbitDist > m_orbitMaxDist)
			orbitDist = m_orbitMaxDist;
	}
	SetDist(orbitDist);
}

void CCameraObj::SetDist(float orbitDist) {
	m_orbitDist = CameraClamp(orbitDist, 0.0, m_orbitMaxDist);
	bool visible = (m_orbitMinDist <= 0.0) || (m_orbitDist >= m_orbitMinDist);
	ObjectAttachSetVisibility(visible);
}

void CCameraObj::OrientToViewDir(CMovementObj* pMO, float rotateY) {
	if (!pMO || !m_baseFrame)
		return;

	Matrix44f matCamera;
	m_baseFrame->GetTransform(NULL, &matCamera);

	Vector3f viewX(-matCamera(0, 0), -matCamera(0, 1), -matCamera(0, 2));
	viewX.Normalize();

	Vector3f up(0, 1, 0);

	// build a basis for the player based on the direction
	// the camera is looking and the up vector.
	Vector3f playerX, playerZ;

	playerZ = viewX.Cross(up);
	playerZ.Normalize();

	playerX = viewX;

	// rotate the x and z vectors by an amount dependent
	// on the mouse delta-x
	Matrix44f r;
	ConvertRotationY(rotateY + m_orbitAziInit, out(r));
	playerX = TransformVector(r, playerX);
	playerZ = TransformVector(r, playerZ);

	// get the player's current transformation matrix.
	// overwrite the rotation part of it with the basis we just built.
	Matrix44f matPlayer = pMO->getBaseFrameMatrix();
	matPlayer.GetRowRef(0) = playerX;
	matPlayer.GetRowRef(1) = Vector3f(0, 1, 0);
	matPlayer.GetRowRef(2) = playerZ;
	pMO->setBaseFrameMatrix(matPlayer, true);
}

void CCameraObj::UpdatePosition() {
	if (!m_baseFrame || !m_floatingFocusFrame)
		return;

	// Get Attached Object Base Frame
	auto pFO = ObjectAttachGetBaseFrame();
	if (!pFO)
		return;

	// Get attached position and matrix
	Vector3f attachPos;
	pFO->GetPosition(attachPos);
	Matrix44f attachMtx(pFO->m_frameMatrix);

	// Don't propagate attached object elevation and twist to camera
	Vector3f vZ = attachMtx.GetRowZ();
	if (!AreParallel(vZ, Vector3f(0, 1, 0))) {
		vZ.Y() = 0; // Project look direction to XZ plane
		Vector3f vY(0, 1, 0); // Up direction is in Y direction.
		ToCoordSysPosZY(attachMtx.GetTranslation(), vZ, vY, out(attachMtx));
	}

	// Build a rotation matrix about the Y axis.
	// Rotate by 180 so camera is behind attached object when azimuth is 0.
	// Also reverse the direction of rotation so that a positive
	// azimuth moves you to the right.
	Matrix44f azimuthMtx, elevationMtx, transformMtx;
	float azimuth = D3DX_PI - m_orbitAzimuth - m_orbitAziInit;
	float elevation = m_orbitElevation + m_orbitEleInit;
	ConvertRotationY(azimuth, out(azimuthMtx));
	ConvertRotationX(elevation, out(elevationMtx));
	transformMtx = elevationMtx * azimuthMtx * attachMtx;

	Vector3f camPosFinal(0, 0, 0);

	// m_orbitFocus is in the actor's local coordinate system, so
	// transform it here into world space
	Vector3f focusOffsetWorld;
	Vector3f scaledFocus = m_orbitFocus * m_cameraScale;
	focusOffsetWorld = TransformVector(attachMtx, scaledFocus);

	Vector3f focusPos = attachPos + focusOffsetWorld;

	Vector3f initialDir(0, 0, -1), offset;
	offset = TransformVector(transformMtx, initialDir);
	offset.Normalize();
	offset.x = offset.x * m_cameraScale.x;
	offset.y = offset.y * m_cameraScale.y;
	offset.z = offset.z * m_cameraScale.z;

	// scale offset vector according to zoom amount, and add it
	// to focusPos to get the final camera position
	offset *= m_orbitDist;
	camPosFinal = focusPos + offset;

	m_baseFrame->AddTransformReplace(transformMtx);
	m_baseFrame->SetPosition(camPosFinal);
	m_floatingFocusFrame->SetPosition(m_floatingCamTarget, pFO);
}

void CCameraObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(8);
		ar << (int)0 // m_type
		   << CString(m_cameraName.c_str())
		   << m_farPlaneDist
		   << m_fovY
		   << m_initialPos.x
		   << m_initialPos.y
		   << m_initialPos.z
		   << 0.0f // m_floatingMovePwr
		   << m_useCollision
		   << 0.0f // m_floatingTolerance
		   << m_floatingCamTarget.x
		   << m_floatingCamTarget.y
		   << m_floatingCamTarget.z
		   << m_heightBaseAspect
		   << m_pitchRotation
		   << (int)0 // m_miscVal1
		   << (BOOL)FALSE // m_filterFirstPersonMode
		   << CStringA("") // m_script_type
		   << (BOOL)FALSE // m_private
		   << (BOOL)FALSE // m_removable
		   << (BOOL)FALSE // m_updatable
		   << (int)0 // m_camera_mode
		   << 0.0f // m_floatingPosInterpPwr
		   << m_orbitFocus.x
		   << m_orbitFocus.y
		   << m_orbitFocus.z
		   << m_orbitAziInit
		   << m_orbitEleInit
		   << m_orbitMinDist
		   << m_orbitMaxDist
		   << m_orbitStartDist
		   << m_orbitDistStep
		   << m_orbitAutoFollow;
	} else {
		float tmpfloat;
		CStringA tmpCString;
		BOOL tmpBOOL;
		int tmpInt;

		int version = ar.GetObjectSchema();
		ar >> tmpInt; // m_type;
		if (version < 5)
			ar >> tmpBOOL // _lookAt
				>> tmpCString; // _fileRefVisualOptional;
		ar >> tmpCString >> m_farPlaneDist >> m_fovY;
		m_cameraName = tmpCString.GetString();
		if (version < 5)
			ar >> tmpfloat // _fixedAfterPosX
				>> tmpfloat // _fixedAfterPosY
				>> tmpfloat; // _fixedAfterPosZ;
		ar >> m_initialPos.x >> m_initialPos.y >> m_initialPos.z >> tmpfloat // m_floatingMovePwr
			>> m_useCollision;
		if (version < 5)
			ar >> tmpfloat // _zoomInSpeed
				>> tmpfloat // _zoomOutSpeed
				>> tmpfloat; // _zoomInStop;
		ar >> tmpfloat // m_floatingTolerance
			>> m_floatingCamTarget.x >> m_floatingCamTarget.y >> m_floatingCamTarget.z;
		if (version < 5)
			ar >> tmpfloat // _cameraMaxPitch
				>> tmpfloat // _cameraPitchSpeed
				>> tmpfloat // _cameraMaxYaw
				>> tmpfloat; // _cameraYawSpeed;
		ar >> m_heightBaseAspect;

		m_pitchRotation = 0.0f;

		if (version > 1)
			ar >> m_pitchRotation;

		if (version > 2) {
			ar >> tmpInt; // m_miscVal1;
			if (version < 5) {
				ar >> tmpfloat // _miscVal2
					>> tmpfloat // _miscVal3
					>> tmpfloat; // _miscVal4;
			}
			ar >> tmpBOOL; // m_filterFirstPersonMode;
		}

		if (version > 3)
			ar >> tmpCString; // m_script_type;

		if (version > 5) {
			ar >> tmpBOOL // m_private
				>> tmpBOOL // m_removable
				>> tmpBOOL; // m_updatable;
		}

		if (version > 6) {
			ar >> tmpInt; // m_camera_mode;
		}

		if (version > 7) {
			ar >> tmpfloat; // m_floatingPosInterpPwr
		}

		if (version > 8) {
			ar >> m_orbitFocus.x >> m_orbitFocus.y >> m_orbitFocus.z >> m_orbitAziInit >> m_orbitEleInit >> m_orbitMinDist >> m_orbitMaxDist >> m_orbitStartDist >> m_orbitDistStep >> m_orbitAutoFollow;
		}

		m_baseFrame = nullptr;
		m_floatingFocusFrame = nullptr;
		m_floatingCamParentFrame = nullptr;
	}
}

IMPLEMENT_SERIAL_SCHEMA(CCameraObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CCameraObjList, CKEPObList)

BEGIN_GETSET_IMPL(CCameraObj, IDS_CAMERAOBJ_NAME)
GETSET_RANGE(m_nearPlaneDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, CAMERAOBJ, NEAR_PLANE_DIST, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_farPlaneDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, CAMERAOBJ, FAR_PLANE_DIST, FLOAT_MIN, FLOAT_MAX)
GETSET2_RANGE(m_orbitAzimuth, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2_RANGE(m_orbitElevation, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2_RANGE(m_orbitDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2_D3DVECTOR(m_orbitFocus, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME)
GETSET2_RANGE(m_orbitMinDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2_RANGE(m_orbitMaxDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2_RANGE(m_orbitAziInit, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2_RANGE(m_orbitEleInit, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2(m_orbitAutoFollow, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME)
GETSET2_RANGE(m_orbitStartDist, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
GETSET2_RANGE(m_orbitDistStep, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_CAMERAOBJ_NAME, IDS_CAMERAOBJ_NAME, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP