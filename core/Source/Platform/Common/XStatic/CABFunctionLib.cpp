///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include <afxtempl.h>
#include <math.h>
#include "d3d9.h"
#include "mmsystem.h"
#include "CABFunctionLib.h"
#include "Core/Math/Rotation.h"

namespace KEP {

//-----add a new file ext--------------------------------------//
void CABFunctionLib::AddNewFileExt(CStringA* currentName, CStringA newExt) {
	CStringA tempName = *currentName;
	CStringA builtName;
	int length = tempName.GetLength();
	for (int loop = 0; loop < length; loop++) {
		CStringA chr = (CStringA)tempName.GetAt(loop);
		builtName = operator+(builtName, chr);
		if (chr == ".")
			break;
	}
	builtName = operator+(builtName, newExt);
	*currentName = builtName;
}

//-----remove a new file ext-----------------------------------//
void CABFunctionLib::RemoveFileExt(CStringA* currentName) {
	CStringA tempName = *currentName;
	CStringA builtName;
	int length = tempName.GetLength();
	for (int loop = 0; loop < length; loop++) {
		CStringA chr = (CStringA)tempName.GetAt(loop);
		if (chr == ".")
			break;
		builtName = operator+(builtName, chr);
	}
	*currentName = builtName;
}

//-----Get File Ext--------------------------------------------//
CStringA CABFunctionLib::GetFileExt(CStringA currentName) {
	CStringA ext;
	int length = currentName.GetLength();
	if (length == 0)
		return "";

	BOOL done = FALSE;
	while (done == FALSE) {
		CStringA chr = (CStringA)currentName.GetAt(length - 1);

		if (chr == ".")
			break;

		ext = operator+(chr, ext);
		length = length - 1;

		if (length == 0)
			break;
	}
	return ext;
}

//--------------get random point in radius--------------------------------------------//
Vector3f CABFunctionLib::GetRandomPointInRadius(float radius, const Vector3f& origin, float minimum) {
	Vector3f locationBuild;
	locationBuild.x = 0.0f;
	locationBuild.y = 0.0f;
	locationBuild.z = 0.0f;
	Matrix44f matrixDistloc;
	Matrix44f matrixOriginloc;
	Matrix44f matrixRatationloc;
	float RadiusDist = (float)(rand() % ((int)radius));
	if (RadiusDist < minimum)
		RadiusDist = minimum;
	float RotationDist = (float)(rand() % 360);
	matrixOriginloc.MakeTranslation(origin); //origin of roam area
	matrixDistloc.MakeTranslation(Vector3f(0.0f, 0.0f, RadiusDist)); //z dist
	ConvertRotationY(ToRadians(RotationDist), out(matrixRatationloc)); //yaw rotation
	//end get new location to travel to in radius
	//move point
	locationBuild = TransformPoint_NonPerspective(matrixDistloc, locationBuild); //translation
	locationBuild = TransformPoint_NonPerspective(matrixRatationloc, locationBuild); //rotation
	locationBuild = TransformPoint_NonPerspective(matrixOriginloc, locationBuild); //offset from origin
	return locationBuild;
}

} // namespace KEP
