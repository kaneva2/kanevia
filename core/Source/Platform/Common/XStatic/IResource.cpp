///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "IResource.h"
#include "ResourceType.h"
#include "ResourceManager.h"
#include "DownloadManager.h"
#include "IClientEngine.h"
#include "DownloadPriorityDistanceModel.h"

namespace KEP {

std::string IResource::mBaseTexturePath[eTexturePath::NumPaths];
std::string IResource::mBaseAnimPath[eAnimPath::NumPaths];
std::string IResource::mBaseMeshPath[eMeshPath::NumPaths];
std::string IResource::mBaseDolPath[NUMBER_OF_DOL_PATHS];
std::string IResource::mBaseEqpPath[NUMBER_OF_EQP_PATHS];
std::string IResource::mBaseSoundPath[NUMBER_OF_SOUND_PATHS];
DownloadPriorityDistanceModel IResource::ms_priorityModel;

IResource::IResource(ResourceManager* pRM, UINT64 hashKey, bool isLocal) :
		m_rawBufferSize(0), m_hashKey(hashKey), m_resourceType(ResourceType::NONE), m_priority(RES_DL_PRIO_DEFAULT), m_camDistance(0), m_camDistInited(false), m_preload(false), m_expectedSize(0), m_expectedHash(""), m_verificationFailures(0), m_verificationLastHash(""), m_pRM(pRM), m_isLocal(isLocal), m_krxEncrypted(false), m_aesEncrypted(false), m_glid(GLID_INVALID) {
	// Null For Server Binaries
	if (!m_pRM)
		return;

	// Assign Resource Type From Resource Manager Type
	m_resourceType = m_pRM->GetResourceType();

	// Resource Already Being Managed?
	if (m_hashKey != 0) {
		m_filePath = m_pRM->GetResourceFilePath(m_hashKey);
		m_url = m_pRM->GetResourceURL(m_hashKey);
	}
}

IResource::IResource(const IResource& res) :
		m_status(res.m_status), m_hashKey(res.m_hashKey), m_resourceType(res.m_resourceType), m_rawBufferSize(res.m_rawBufferSize), m_filePath(res.m_filePath), m_url(res.m_url), m_priority(res.m_priority), m_camDistance(res.m_camDistance), m_camDistInited(res.m_camDistInited), m_preload(res.m_preload), m_expectedSize(res.m_expectedSize), m_expectedHash(res.m_expectedHash), m_verificationFailures(res.m_verificationFailures), m_verificationLastHash(res.m_verificationLastHash), m_pRM(res.m_pRM), m_isLocal(res.m_isLocal), m_krxEncrypted(res.m_krxEncrypted), m_aesEncrypted(false), m_glid(res.m_glid) {
	if (res.m_pRawBuffer) {
		m_pRawBuffer.reset(new BYTE[m_rawBufferSize]);
		memcpy(m_pRawBuffer.get(), res.m_pRawBuffer.get(), (size_t)m_rawBufferSize);
	}
}

unsigned char* IResource::GetEncryptionHashOverride(int& hash_len) {
	// Default is NULL. KRX will generate it from data automatically
	hash_len = 0;
	return nullptr;
}

void IResource::FreeRawBuffer() {
	m_pRawBuffer.reset();
	m_rawBufferSize = 0;
}

void IResource::SetRawBuffer(std::unique_ptr<BYTE[]> rawBuffer, FileSize rawSize) {
	FreeRawBuffer();
	m_pRawBuffer = std::move(rawBuffer);
	m_rawBufferSize = rawSize;
}

std::string IResource::GetFileName() const {
	return StrStripFilePath(m_filePath);
}

int IResource::GetDownloadPriority() const {
#ifdef LOAD_RESOURCE_BY_DISTANCE
	unsigned bias = GetDownloadPriorityModel().getPriorityBucket(GetResourceType(), GetPriorityRangeSelector(), GetCamDistance());
	return m_priority - bias;
#else
	return m_priority;
#endif
}

} // namespace KEP
