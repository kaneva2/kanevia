///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CAIICollectionQuest.h"
#include "CInventoryClass.h"
#include "CSkillClass.h"

namespace KEP {

CAICollectionQuestObj::CAICollectionQuestObj() :
		m_channel(INVALID_CHANNEL_ID),
		m_respawnZoneIndex(INVALID_ZONE_INDEX),
		m_rotation(0) {
	m_title = "Unknown";
	m_questDesription = "Quest Description";
	m_questCompletionMessage = "Quest Completion";
	m_type = 0; //0= collection quest
	m_requiredItems = NULL;
	m_reward = NULL;
	m_skillTypeBonus = -1;
	m_expBonus = 0.0f;
	m_maxExpLimit = 0.0f;

	m_spawnAiCfg = -1; //-1 =  none/ no spawn
	m_spawnPosition.Set(0, 0, 0);
	m_spawnedAiLifeline = 0;
	m_randomRewardType = FALSE;
	m_spawnToLocationOverride = FALSE;
	m_makeTargetRebirthPoint = FALSE;
	m_spawnStamp = fTime::TimeMs();
	m_raceSpecificAccess = -1;
	m_miscAction = 0;
	m_completedKeyEnabled = 0;
	m_progessBasedByQuestJournal = FALSE;
	m_cashReward = 0;
	m_requiredSkillType = 0;
	m_requiredSkillLevel = 0;
	m_mobToHuntID = 0;
	m_maxDropCount = 0;
	m_dropGlid = 0;
	m_dropPercentage = 100;
	m_autocomplete = FALSE;
	m_openDialogWindow = FALSE;
	m_menuIdToOpen = -1;
	m_optionalIntruction = "";
	m_listProgressionOptions = FALSE;
	m_undeletable = FALSE;
	m_spawnOnFail = FALSE;
	m_portalLimitMsg = "";
	m_portalLimitID = 0;
	m_closeJournalEntryByName = "";
}

void CAICollectionQuestObj::SafeDelete() {
	if (m_requiredItems) {
		m_requiredItems->SafeDelete();
		delete m_requiredItems;
		m_requiredItems = 0;
	}

	if (m_reward) {
		m_reward->SafeDelete();
		delete m_reward;
		m_reward = 0;
	}
}

BOOL CAICollectionQuestObj::EvaluateQuest(CInventoryObjList* playersInventory,
	CSkillObjectList* skillDatabase,
	CSkillObjectList* skillMainDatabase,
	float* expGained) {
	if (!m_requiredItems) {
		return FALSE;
	}

	if (!m_reward) {
		return FALSE;
	}

	if (!playersInventory) {
		return FALSE;
	}

	CInventoryObjList* tempHoldingList = NULL;
	playersInventory->Clone(&tempHoldingList);

	BOOL hasRequiredItems = TRUE;

	POSITION posLoc;
	for (posLoc = m_requiredItems->GetHeadPosition(); posLoc != NULL;) {
		// verification loop
		CInventoryObj* invObj = (CInventoryObj*)m_requiredItems->GetNext(posLoc);
		if (!tempHoldingList->DeleteByGLID(invObj->m_itemData->m_globalID)) {
			// no go
			hasRequiredItems = FALSE;
			break;
		} // end no go
	} // end verification loop

	tempHoldingList->SafeDelete();
	delete tempHoldingList;
	tempHoldingList = 0;

	if (!hasRequiredItems) {
		return FALSE; // havent completed it
	}

	// if you made it this far he has done quest correctly
	for (posLoc = m_requiredItems->GetHeadPosition(); posLoc != NULL;) {
		// remove requirement item loop
		CInventoryObj* invObj = (CInventoryObj*)m_requiredItems->GetNext(posLoc);
		playersInventory->DeleteByGLID(invObj->m_itemData->m_globalID);
	} // end remove requirement item loop

	if (!m_randomRewardType) {
		// regular reward type
		for (posLoc = m_reward->GetHeadPosition(); posLoc != NULL;) {
			// remove requirement item loop
			CInventoryObj* invObj = (CInventoryObj*)m_reward->GetNext(posLoc);
			playersInventory->AddInventoryItem(1, FALSE, invObj->m_itemData, invObj->inventoryType());
		} // end remove requirement item loop
	} // end regular reward type
	else if ((int)m_reward->GetCount() > 0) {
		// random reward
		int selectionCur = rand() % ((int)m_reward->GetCount());
		if (selectionCur > -1) {
			POSITION rendPos = m_reward->FindIndex(selectionCur);
			if (rendPos) {
				CInventoryObj* invObj = (CInventoryObj*)m_reward->GetAt(rendPos);
				playersInventory->AddInventoryItem(1, FALSE, invObj->m_itemData, invObj->inventoryType());
			}
		}
	} // end randome reward

	if (m_expBonus != 0.0f) {
		if (!skillDatabase) {
			skillDatabase = new CSkillObjectList();
		}

		CSkillObject* skillPtr = skillDatabase->GetSkillByType(m_skillTypeBonus);

		//
		// see if we have used this skill before-if not add it to our
		// database
		//
		if (!skillPtr) {
			// need to add this skill
			CSkillObject* dbSkillPtr = skillMainDatabase->GetSkillByType(m_skillTypeBonus);
			if (!dbSkillPtr) {
				return FALSE;
			}

			dbSkillPtr->CloneMinimum(&skillPtr);
			skillDatabase->AddTail(skillPtr);
		} // end need to add this skill

		if (skillPtr) {
			if (skillPtr->m_currentExperience < m_maxExpLimit) {
				// you can still gain on this
				skillPtr->m_currentExperience += m_expBonus;
				*expGained = m_expBonus;
			} // end you can still gain on this
		}
	}

	return TRUE;
}

void CAICollectionQuestObj::Clone(CAICollectionQuestObj** clone) {
	CAICollectionQuestObj* copyLocal = new CAICollectionQuestObj();

	copyLocal->m_title = m_title;
	copyLocal->m_questDesription = m_questDesription;
	copyLocal->m_questCompletionMessage = m_questCompletionMessage;
	copyLocal->m_type = m_type; //0= collection quest
	copyLocal->m_skillTypeBonus = m_skillTypeBonus;
	copyLocal->m_expBonus = m_expBonus;
	copyLocal->m_maxExpLimit = m_maxExpLimit;
	copyLocal->m_spawnAiCfg = m_spawnAiCfg;
	copyLocal->m_spawnPosition = m_spawnPosition;
	copyLocal->m_channel = m_channel;
	copyLocal->m_respawnZoneIndex = m_respawnZoneIndex;
	copyLocal->m_spawnedAiLifeline = m_spawnedAiLifeline;
	copyLocal->m_randomRewardType = m_randomRewardType;
	copyLocal->m_spawnToLocationOverride = m_spawnToLocationOverride;
	copyLocal->m_makeTargetRebirthPoint = m_makeTargetRebirthPoint;
	copyLocal->m_raceSpecificAccess = m_raceSpecificAccess;
	copyLocal->m_optionalCommand = m_optionalCommand;
	copyLocal->m_miscAction = m_miscAction;
	copyLocal->m_completedKeyEnabled = m_completedKeyEnabled;
	copyLocal->m_progessBasedByQuestJournal = m_progessBasedByQuestJournal;
	copyLocal->m_cashReward = m_cashReward;
	copyLocal->m_requiredSkillType = m_requiredSkillType;
	copyLocal->m_requiredSkillLevel = m_requiredSkillLevel;
	copyLocal->m_mobToHuntID = m_mobToHuntID;
	copyLocal->m_maxDropCount = m_maxDropCount;
	copyLocal->m_dropGlid = m_dropGlid;
	copyLocal->m_dropPercentage = m_dropPercentage;
	copyLocal->m_autocomplete = m_autocomplete;
	copyLocal->m_openDialogWindow = m_openDialogWindow;
	copyLocal->m_menuIdToOpen = m_menuIdToOpen;
	copyLocal->m_optionalIntruction = m_optionalIntruction;
	copyLocal->m_listProgressionOptions = m_listProgressionOptions;
	copyLocal->m_undeletable = m_undeletable;
	copyLocal->m_spawnOnFail = m_spawnOnFail;
	copyLocal->m_portalLimitMsg = m_portalLimitMsg;
	copyLocal->m_portalLimitID = m_portalLimitID;
	copyLocal->m_closeJournalEntryByName = m_closeJournalEntryByName;
	copyLocal->m_rotation = m_rotation;

	if (m_requiredItems) {
		m_requiredItems->Clone(&copyLocal->m_requiredItems);
	}

	if (m_reward) {
		m_reward->Clone(&copyLocal->m_reward);
	}

	*clone = copyLocal;
}

void CAICollectionQuestList::Clone(CAICollectionQuestList** clone) {
	CAICollectionQuestList* copyLocal = new CAICollectionQuestList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAICollectionQuestObj* spnPtr = (CAICollectionQuestObj*)GetNext(posLoc);
		CAICollectionQuestObj* newPtr = NULL;
		spnPtr->Clone(&newPtr);
		copyLocal->AddTail(newPtr);
	}

	*clone = copyLocal;
}

CAICollectionQuestObj* CAICollectionQuestList::GetByAvailIndex(int baseSelection, int raceID, int* retTraceVis) {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAICollectionQuestObj* spnPtr = (CAICollectionQuestObj*)GetNext(posLoc);
		if (spnPtr->m_raceSpecificAccess > -1) {
			if (spnPtr->m_raceSpecificAccess != raceID) {
				continue;
			}
		}

		if (trace == baseSelection) {
			*retTraceVis = trace;
			return spnPtr;
		}

		trace++;
	}

	return NULL;
}

CAICollectionQuestObj* CAICollectionQuestList::GetByIndex(int selection) {
	if (selection < 0) {
		return NULL;
	}

	POSITION posLoc = FindIndex(selection);
	if (!posLoc) {
		return NULL;
	}

	CAICollectionQuestObj* spnPtr = (CAICollectionQuestObj*)GetAt(posLoc);

	return spnPtr;
}

void CAICollectionQuestList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAICollectionQuestObj* spnPtr = (CAICollectionQuestObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}

	RemoveAll();
}

void CAICollectionQuestObj::Serialize(CArchive& ar) {
	CStringA reservedString = "Reserved";

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(5);
		ASSERT(ar.GetObjectSchema() == 5);

		ar << m_title
		   << m_questDesription
		   << m_questCompletionMessage
		   << m_type
		   << m_requiredItems
		   << m_reward
		   << m_skillTypeBonus
		   << m_expBonus
		   << m_maxExpLimit
		   << m_spawnAiCfg
		   << m_spawnPosition.x
		   << m_spawnPosition.y
		   << m_spawnPosition.z;
		if (m_spawnToLocationOverride) {
			ar << m_respawnZoneIndex.getClearedInstanceId();
			ar << m_respawnZoneIndex.GetInstanceId();
		} else {
			ar << m_channel.getClearedInstanceId();
			ar << m_channel.GetInstanceId();
		}
		ar << m_spawnedAiLifeline
		   << m_randomRewardType
		   << m_spawnToLocationOverride
		   << m_makeTargetRebirthPoint;

		ar << m_optionalCommand
		   << m_miscAction
		   << m_raceSpecificAccess
		   << m_completedKeyEnabled
		   << m_progessBasedByQuestJournal;

		ar << m_cashReward
		   << m_autocomplete
		   << m_openDialogWindow
		   << m_requiredSkillType
		   << m_requiredSkillLevel;

		ar << m_optionalIntruction
		   << m_portalLimitMsg
		   << m_closeJournalEntryByName
		   << reservedString;

		ar << m_mobToHuntID
		   << m_maxDropCount
		   << m_dropGlid
		   << m_dropPercentage
		   << m_listProgressionOptions
		   << m_undeletable
		   << m_spawnOnFail
		   << m_portalLimitID;

		ar << m_menuIdToOpen;
		ar << m_rotation;
	} else {
		int version = ar.GetObjectSchema();
		int temp = 0;
		int instanceId = 0;

		ar >> m_title >> m_questDesription >> m_questCompletionMessage >> m_type >> m_requiredItems >> m_reward >> m_skillTypeBonus >> m_expBonus >> m_maxExpLimit >> m_spawnAiCfg >> m_spawnPosition.x >> m_spawnPosition.y >> m_spawnPosition.z >> temp;

		if (version > 7) {
			ar >> instanceId;
		}
		ar >> m_spawnedAiLifeline >> m_randomRewardType >> m_spawnToLocationOverride >> m_makeTargetRebirthPoint;

		m_raceSpecificAccess = -1;
		m_requiredSkillType = 0;
		m_requiredSkillLevel = 0;
		m_mobToHuntID = 0;
		m_maxDropCount = 0;
		m_dropGlid = 0;
		m_dropPercentage = 0;
		m_portalLimitID = 0;
		m_openDialogWindow = FALSE;

		if (version == 2) {
			ar >> m_optionalCommand >> m_miscAction >> m_raceSpecificAccess >> m_completedKeyEnabled >> m_progessBasedByQuestJournal;

			m_cashReward = 0;
		} else if (version == 3) {
			ar >> m_optionalCommand >> m_miscAction >> m_raceSpecificAccess >> m_completedKeyEnabled >> m_progessBasedByQuestJournal;

			ar >> m_cashReward >> m_autocomplete >> m_openDialogWindow >> m_requiredSkillType >> m_requiredSkillLevel;
		} else if (version == 4) {
			ar >> m_optionalCommand >> m_miscAction >> m_raceSpecificAccess >> m_completedKeyEnabled >> m_progessBasedByQuestJournal;

			ar >> m_cashReward >> m_autocomplete >> m_openDialogWindow >> m_requiredSkillType >> m_requiredSkillLevel;

			ar >> reservedString >> reservedString >> reservedString >> reservedString;
		} else if (version >= 5) {
			ar >> m_optionalCommand >> m_miscAction >> m_raceSpecificAccess >> m_completedKeyEnabled >> m_progessBasedByQuestJournal;

			ar >> m_cashReward >> m_autocomplete >> m_openDialogWindow >> m_requiredSkillType >> m_requiredSkillLevel;

			ar >> m_optionalIntruction >> m_portalLimitMsg >> m_closeJournalEntryByName >> reservedString;

			ar >> m_mobToHuntID >> m_maxDropCount >> m_dropGlid >> m_dropPercentage >> m_listProgressionOptions >> m_undeletable >> m_spawnOnFail >> m_portalLimitID;
		}

		if (version > 5) {
			ar >> m_menuIdToOpen;
		}
		if (version > 6)
			ar >> m_rotation;
		else
			m_rotation = 0;

		if (m_spawnToLocationOverride) {
			ZoneIndex z(temp);
			m_respawnZoneIndex = ZoneIndex(z.zoneIndex(), instanceId, z.GetType());
			if (instanceId == 0)
				m_respawnZoneIndex.clearInstance();
		} else {
			ChannelId c(temp);
			m_channel = ChannelId(c.channelId(), instanceId, c.GetType());
			if (instanceId == 0)
				m_channel.clearInstance();
		}

		if (m_optionalIntruction == "Reserved") {
			m_optionalIntruction = "";
		}

		m_spawnStamp = fTime::TimeMs();
	}
}

IMPLEMENT_SERIAL_SCHEMA(CAICollectionQuestObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CAICollectionQuestList, CObList)

BEGIN_GETSET_IMPL(CAICollectionQuestObj, IDS_AICOLLECTIONQUESTOBJ_NAME)
GETSET_MAX(m_title, gsCString, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, TITLE, STRLEN_MAX)
GETSET_MAX(m_questDesription, gsCString, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, QUESTDESRIPTION, STRLEN_MAX)
GETSET_MAX(m_questCompletionMessage, gsCString, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, QUESTCOMPLETIONMESSAGE, STRLEN_MAX)
GETSET_RANGE(m_type, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, TYPE, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_requiredItems, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, REQUIREDITEMS)
GETSET_OBLIST_PTR(m_reward, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, REWARD)
GETSET_RANGE(m_skillTypeBonus, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, SKILLTYPEBONUS, INT_MIN, INT_MAX)
GETSET_RANGE(m_expBonus, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, EXPBONUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_maxExpLimit, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, MAXEXPLIMIT, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_spawnAiCfg, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, SPAWNAICFG, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_spawnPosition, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, SPAWNPOSITION)
GETSET_RANGE(m_channel, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, WORLDID, INT_MIN, INT_MAX)
GETSET_RANGE(m_spawnedAiLifeline, gsLong, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, SPAWNEDAILIFELINE, LONG_MIN, LONG_MAX)
GETSET(m_randomRewardType, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, RANDOMREWARDTYPE)
GETSET(m_spawnToLocationOverride, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, SPAWNTOLOCATIONOVERRIDE)
GETSET(m_makeTargetRebirthPoint, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, MAKETARGETREBIRTHPOINT)
GETSET_MAX(m_optionalCommand, gsCString, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, OPTIONALCOMMAND, STRLEN_MAX)
GETSET_RANGE(m_miscAction, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, MISCACTION, INT_MIN, INT_MAX)
GETSET_RANGE(m_raceSpecificAccess, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, RACESPECIFICACCESS, INT_MIN, INT_MAX)
GETSET_RANGE(m_completedKeyEnabled, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, COMPLETEDKEYENABLED, INT_MIN, INT_MAX)
GETSET(m_progessBasedByQuestJournal, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, PROGESSBASEDBYQUESTJOURNAL)
GETSET_RANGE(m_cashReward, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, CASHREWARD, INT_MIN, INT_MAX)
GETSET(m_autocomplete, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, AUTOCOMPLETE)
GETSET(m_openDialogWindow, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, OPENDIALOGWINDOW)
GETSET_RANGE(m_requiredSkillType, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, REQUIREDSKILLTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_requiredSkillLevel, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, REQUIREDSKILLLEVEL, INT_MIN, INT_MAX)
GETSET_MAX(m_optionalIntruction, gsCString, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, OPTIONALINTRUCTION, STRLEN_MAX)
GETSET_MAX(m_portalLimitMsg, gsCString, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, PORTALLIMITMSG, STRLEN_MAX)
GETSET_MAX(m_closeJournalEntryByName, gsCString, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, CLOSEJOURNALENTRYBYNAME, STRLEN_MAX)
GETSET_RANGE(m_mobToHuntID, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, MOBTOHUNTID, INT_MIN, INT_MAX)
GETSET_RANGE(m_maxDropCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, MAXDROPCOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_dropGlid, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, DROPGLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_dropPercentage, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, DROPPERCENTAGE, INT_MIN, INT_MAX)
GETSET(m_listProgressionOptions, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, LISTPROGRESSIONOPTIONS)
GETSET(m_undeletable, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, UNDELETABLE)
GETSET_RANGE(m_spawnOnFail, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, SPAWNONFAIL, INT_MIN, INT_MAX)
GETSET_RANGE(m_portalLimitID, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICOLLECTIONQUESTOBJ, PORTALLIMITID, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP