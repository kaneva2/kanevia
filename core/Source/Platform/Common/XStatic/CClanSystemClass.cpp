///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include "ClientStrings.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "direct.h"
#include <SerializeHelper.h>
#include "CPlayerClass.h"
#include "CClanSystemClass.h"
#include "CNoterietyClass.h"
#include "CAccountClass.h"
#include "UnicodeMFCHelpers.h"

namespace KEP {

CClanMemberObj::CClanMemberObj() {
	m_memberHandle = 0;
}

void CClanMemberObj::Clone(CClanMemberObj** clone) {
	CClanMemberObj* copyLocal = new CClanMemberObj();
	copyLocal->m_memberHandle = m_memberHandle;
	copyLocal->m_memberName = m_memberName;

	*clone = copyLocal;
}

void CClanMemberList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CClanMemberObj* spnPtr = (CClanMemberObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CClanMemberList::Clone(CClanMemberList** clone) {
	CClanMemberList* copyLocal = new CClanMemberList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CClanMemberObj* skPtr = (CClanMemberObj*)GetNext(posLoc);
		CClanMemberObj* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}

CClanMemberObj* CClanMemberList::GetByHandle(PLAYER_HANDLE handle) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CClanMemberObj* spnPtr = (CClanMemberObj*)GetNext(posLoc);
		if (spnPtr->m_memberHandle == handle)
			return spnPtr;
	}
	return NULL;
}

CClanMemberObj* CClanMemberList::GetByName(CStringA& name) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CClanMemberObj* spnPtr = (CClanMemberObj*)GetNext(posLoc);
		if (spnPtr->m_memberName == name)
			return spnPtr;
	}
	return NULL;
}

BOOL CClanMemberList::RemoveMemberByName(CStringA membername, PLAYER_HANDLE* playerId) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CClanMemberObj* spnPtr = (CClanMemberObj*)GetNext(posLoc);
		if (spnPtr->m_memberName == membername) {
			if (playerId) {
				*playerId = spnPtr->m_memberHandle;
			}
			delete spnPtr;
			spnPtr = 0;
			RemoveAt(posLast);
			return TRUE;
		}
	}
	return FALSE;
}

void CClanMemberObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_memberHandle
		   << m_memberName;
	} else {
		ar >> m_memberHandle;
		ar >> m_memberName;
	}
}

CClanSystemObj::CClanSystemObj() :
		m_clanId(0) {
	m_clanName;
	m_abbrev;
	m_guildMasterHandle = 0;
	m_memberList = NULL;
	m_houseLink = -1;
	m_warsWon = 0;
	m_clanWarCount = 0;
	m_clanWarList = NULL;
	m_clanArenaPoints = 0;
}

void CClanSystemObj::Clone(CClanSystemObj** clone) {
	CClanSystemObj* copyLocal = new CClanSystemObj();
	copyLocal->m_clanName = m_clanName;
	copyLocal->m_abbrev = m_abbrev;
	copyLocal->m_guildMasterHandle = m_guildMasterHandle;
	copyLocal->m_houseLink = m_houseLink;
	copyLocal->m_warsWon = m_warsWon;
	copyLocal->m_clanWarCount = m_clanWarCount;
	copyLocal->m_clanArenaPoints = m_clanArenaPoints;

	if (m_clanWarCount > 0)
		if (copyLocal->m_clanWarList) {
			copyLocal->m_clanWarList = new PLAYER_HANDLE[m_clanWarCount];
			CopyMemory(copyLocal->m_clanWarList, m_clanWarList, sizeof(PLAYER_HANDLE) * m_clanWarCount);
		}

	if (m_memberList)
		m_memberList->Clone(&copyLocal->m_memberList);

	*clone = copyLocal;
}

BOOL CClanSystemObj::VerifyClanOnWarList(PLAYER_HANDLE clanHandle) {
	if (!m_clanWarList)
		return FALSE;

	for (int i = 0; i < m_clanWarCount; i++)
		if (m_clanWarList[i] == clanHandle)
			return TRUE;

	return FALSE;
}

BOOL CClanSystemObj::AddClanToWarList(PLAYER_HANDLE clanHandle) {
	if (VerifyClanOnWarList(clanHandle))
		return FALSE;

	m_clanWarCount++;
	PLAYER_HANDLE* newList = new PLAYER_HANDLE[m_clanWarCount];

	if (m_clanWarList) {
		if (m_clanWarCount > 1)
			CopyMemory(newList, m_clanWarList, (sizeof(PLAYER_HANDLE) * (m_clanWarCount - 1)));
		delete[] m_clanWarList;
		m_clanWarList = 0;
	}

	newList[(m_clanWarCount - 1)] = clanHandle;
	m_clanWarList = newList;

	return TRUE;
}

BOOL CClanSystemObj::RemoveClanFromWarList(PLAYER_HANDLE clanHandle) {
	if (!VerifyClanOnWarList(clanHandle)) //not there
		return FALSE;

	if (!m_clanWarList) {
		m_clanWarCount = 0;
		return FALSE;
	}

	int oldCount = m_clanWarCount;
	if (m_clanWarCount > 0) {
		m_clanWarCount--;
		if (m_clanWarCount == 0) {
			delete[] m_clanWarList;
			m_clanWarList = 0;
			return TRUE;
		}

		PLAYER_HANDLE* newList = new PLAYER_HANDLE[m_clanWarCount];
		int trace = 0;
		for (int i = 0; i < oldCount; i++) {
			if (m_clanWarList[i] != clanHandle) {
				newList[trace] = m_clanWarList[i];
				trace++;
			}
		}
		delete[] m_clanWarList;
		m_clanWarList = 0;
		m_clanWarList = newList;
	}

	return FALSE;
}

void CClanSystemObj::SafeDelete() {
	if (m_memberList) {
		m_memberList->SafeDelete();
		delete m_memberList;
		m_memberList = 0;
	}
	if (m_clanWarList) {
		delete[] m_clanWarList;
		m_clanWarList = 0;
	}
}

void CClanSystemObj::AddMemberByHandle(PLAYER_HANDLE handle, CStringA memberName, bool checkForDupe) {
	if (!m_memberList)
		m_memberList = new CClanMemberList();

	if (checkForDupe)
		if (m_memberList->GetByHandle(handle))
			return;

	CClanMemberObj* newMember = new CClanMemberObj();
	newMember->m_memberHandle = handle;
	newMember->m_memberName = memberName;
	m_memberList->AddTail(newMember);
}

PLAYER_HANDLE CClanSystemObj::RemoveMemberByHandle(PLAYER_HANDLE handle) {
	if (!m_memberList)
		return NO_CLAN;

	POSITION posLast = NULL;
	for (POSITION posLoc = m_memberList->GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CClanMemberObj* spnPtr = (CClanMemberObj*)m_memberList->GetNext(posLoc);
		if (spnPtr->m_memberHandle == handle) {
			delete spnPtr;
			spnPtr = 0;
			m_memberList->RemoveAt(posLast);
			return handle;
		}
	}
	return NO_CLAN;
}

void CClanSystemObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CClanSystemObj* spnPtr = (CClanSystemObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

int CClanSystemObjList::CreateClan(CStringA clanFullName,
	CStringA clanAbbrev,
	PLAYER_HANDLE guildMasterHandle,
	CClanSystemObj** newClan, bool checkForDupe) {
	//lengths
	if (clanAbbrev.GetLength() > 35)
		return 3; //too long

	if (clanAbbrev.GetLength() <= 0)
		return 3; //too long

	//verify name differences
	if (checkForDupe) {
		//verify clan with this handle doesnt exists
		if (GetClanByHandle(guildMasterHandle))
			return 0; //clan already exists

		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CClanSystemObj* spnPtr = (CClanSystemObj*)GetNext(posLoc);
			if (spnPtr->m_abbrev.CompareNoCase(clanAbbrev) == 0)
				return 2; //already exists
		}
	}

	//create it
	CClanSystemObj* clanObject = new CClanSystemObj();
	clanObject->m_abbrev = clanAbbrev;
	clanObject->m_clanName = clanFullName;
	clanObject->m_guildMasterHandle = guildMasterHandle;
	clanObject->m_memberList = new CClanMemberList();

	AddTail(clanObject);

	if (newClan)
		*newClan = clanObject;

	return 1; //created successfully
}

CClanSystemObj* CClanSystemObjList::GetClanByHandle(PLAYER_HANDLE m_guildMasterHandle) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CClanSystemObj* spnPtr = (CClanSystemObj*)GetNext(posLoc);
		if (spnPtr->m_guildMasterHandle == m_guildMasterHandle)
			return spnPtr;
	}
	return NULL;
}

CClanSystemObj* CClanSystemObjList::GetClanByClanId(LONG clanId) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CClanSystemObj* spnPtr = (CClanSystemObj*)GetNext(posLoc);
		if (spnPtr->m_clanId == clanId)
			return spnPtr;
	}
	return NULL;
}

CClanSystemObj* CClanSystemObjList::GetClanByPreFix(CStringA& prefix) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CClanSystemObj* spnPtr = (CClanSystemObj*)GetNext(posLoc);
		if (spnPtr->m_abbrev.CompareNoCase(prefix) == 0)
			return spnPtr;
	}
	return NULL;
}

BOOL CClanSystemObjList::DeleteClanByHandle(PLAYER_HANDLE m_guildMasterHandle) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CClanSystemObj* spnPtr = (CClanSystemObj*)GetNext(posLoc);
		if (spnPtr->m_guildMasterHandle == m_guildMasterHandle) {
			spnPtr->SafeDelete();
			delete spnPtr;
			spnPtr = 0;
			RemoveAt(posLast);
			return TRUE;
		}
	}
	return FALSE;
}

void CClanSystemObjList::DumpClanDataToFile(CStringA filename, CAccountObjectList* accountDB) {
	if (!accountDB) {
		AfxMessageBox(IDS_ERROR_NOACCTAVAILABLE);
		return;
	}
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, filename, CFile::modeCreate | CFile::modeWrite, &exc);
	if (res) {
		BEGIN_SERIALIZE_TRY

		CArchive the_out_Archive(&fl, CArchive::store);

		POSITION posLast;
		for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
			CClanSystemObj* clanSys = (CClanSystemObj*)GetNext(posLoc);

			int memberCount = 0;
			if (clanSys->m_memberList)
				memberCount = (int)clanSys->m_memberList->GetCount();
			int totalMurderCount = 0;
			int totalMinutesPlayed = 0;
			WriteString(the_out_Archive, "");
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, "=Clan=");
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, clanSys->m_abbrev);
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, "=Total Members=");
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, ConvToString((memberCount + 1)));
			WriteString(the_out_Archive, "\n");

			CPlayerObject* pPOGuildMaster = accountDB->GetPlayerObjectByPlayerHandle(clanSys->m_guildMasterHandle);

			WriteString(the_out_Archive, "=Clan Leader=");
			WriteString(the_out_Archive, "\n");
			if (pPOGuildMaster) {
				CAccountObject* accdata = accountDB->GetAccountObjectByPlayerHandle(clanSys->m_guildMasterHandle);
				if (pPOGuildMaster->m_noteriety)
					totalMurderCount += pPOGuildMaster->m_noteriety->m_currentMurders;
				if (accdata)
					totalMinutesPlayed += accdata->m_timePlayedMinutes;

				WriteString(the_out_Archive, ConvToString(pPOGuildMaster->m_handle));
				WriteString(the_out_Archive, ",");
				WriteString(the_out_Archive, pPOGuildMaster->m_charName);
				WriteString(the_out_Archive, "\n");
			} else {
				WriteString(the_out_Archive, "Uknown");
				WriteString(the_out_Archive, "\n");
			}

			POSITION posLoc2;
			for (posLoc2 = clanSys->m_memberList->GetHeadPosition(); posLoc2 != NULL;) {
				CClanMemberObj* memberPtr = (CClanMemberObj*)clanSys->m_memberList->GetNext(posLoc2);
				CPlayerObject* pPO = accountDB->GetPlayerObjectByPlayerHandle(memberPtr->m_memberHandle);
				if (pPO) {
					CAccountObject* accdata = accountDB->GetAccountObjectByPlayerHandle(memberPtr->m_memberHandle);
					if (pPO->m_noteriety)
						totalMurderCount += pPO->m_noteriety->m_currentMurders;

					if (accdata)
						totalMinutesPlayed += accdata->m_timePlayedMinutes;
				}
			}

			WriteString(the_out_Archive, "=Total Murder counts=");
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, ConvToString(totalMurderCount));
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, "=Total In Game Minutes=");
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, ConvToString(totalMinutesPlayed));
			WriteString(the_out_Archive, "\n");
			WriteString(the_out_Archive, "=Roster=");
			WriteString(the_out_Archive, "\n");

			for (posLoc2 = clanSys->m_memberList->GetHeadPosition(); posLoc2 != NULL;) {
				CClanMemberObj* memberPtr = (CClanMemberObj*)clanSys->m_memberList->GetNext(posLoc2);
				WriteString(the_out_Archive, ConvToString(memberPtr->m_memberHandle));
				WriteString(the_out_Archive, ",");
				WriteString(the_out_Archive, memberPtr->m_memberName);
				WriteString(the_out_Archive, "\n");
			}
		}

		the_out_Archive.Close();
		fl.Close();

		END_SERIALIZE_TRY_NO_LOGGER
	} else
		LOG_EXCEPTION_NO_LOGGER(exc)
}

//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//-----converts a long into string-----------------------------//
CStringA CClanSystemObjList::ConvToString(int number) {
	if (number == 0)
		return "0";

	int decimal, sign;
	char buffer[32];
	CStringA temp;
	_fcvt_s(buffer, _countof(buffer), number, 0, &decimal, &sign);
	temp = buffer;
	if (sign != 0)
		temp = operator+("-", temp);

	return temp;
}

//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//-------------------------------------------------------------//
BOOL CClanSystemObjList::AreTheseClansAtWar(PLAYER_HANDLE clanHandle_1, PLAYER_HANDLE clanHandle_2) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CClanSystemObj* skPtr = (CClanSystemObj*)GetNext(posLoc);
		if (skPtr->m_guildMasterHandle == clanHandle_1) {
			if (skPtr->VerifyClanOnWarList(clanHandle_2))
				return TRUE;
			else
				return FALSE;
		}
	}
	return FALSE;
}
//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//-------------------------------------------------------------//
BOOL CClanSystemObjList::TheseClansHaveAgreedToWar(PLAYER_HANDLE clanHandle_1, PLAYER_HANDLE clanHandle_2) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CClanSystemObj* skPtr = (CClanSystemObj*)GetNext(posLoc);
		if (skPtr->m_guildMasterHandle == clanHandle_1) {
			skPtr->AddClanToWarList(clanHandle_2);
		}
		if (skPtr->m_guildMasterHandle == clanHandle_2) {
			skPtr->AddClanToWarList(clanHandle_1);
		}
	}
	return FALSE;
}
//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//-------------------------------------------------------------//
BOOL CClanSystemObjList::TheseClansHaveStoppedTheWar(PLAYER_HANDLE clanHandle_1, PLAYER_HANDLE clanHandle_2) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CClanSystemObj* skPtr = (CClanSystemObj*)GetNext(posLoc);
		if (skPtr->m_guildMasterHandle == clanHandle_1) {
			skPtr->RemoveClanFromWarList(clanHandle_2);
		}
		if (skPtr->m_guildMasterHandle == clanHandle_2) {
			skPtr->RemoveClanFromWarList(clanHandle_1);
		}
	}
	return FALSE;
}

BOOL CClanSystemObjList::GetPlayersClan(PLAYER_HANDLE playerId, PLAYER_HANDLE& clanHandle, CStringA& clanName) {
	clanHandle = 0;
	clanName = "";

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		//account list
		CClanSystemObj* clan = (CClanSystemObj*)GetNext(posLoc);
		if (clan->m_guildMasterHandle == playerId) {
			clanHandle = playerId;
			clanName = clan->m_abbrev;
			return TRUE;
		}
		for (POSITION memberPos = clan->m_memberList->GetHeadPosition(); memberPos != NULL;) {
			CClanMemberObj* member = (CClanMemberObj*)GetNext(memberPos);
			if (member->m_memberHandle == playerId) {
				clanHandle = clan->m_guildMasterHandle;
				clanName = clan->m_abbrev;
				return TRUE;
			}
		}
	}
	return FALSE;
}

void CClanSystemObjList::Clone(CClanSystemObjList** clone) {
	CClanSystemObjList* copyLocal = new CClanSystemObjList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CClanSystemObj* skPtr = (CClanSystemObj*)GetNext(posLoc);
		CClanSystemObj* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}

void CClanSystemObj::Serialize(CArchive& ar) {
	int reserved = 0;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(2);
		ASSERT(ar.GetObjectSchema() == 2);

		ar << m_memberList
		   << m_clanName
		   << m_abbrev
		   << m_guildMasterHandle
		   << m_houseLink;

		ar << m_clanArenaPoints
		   << reserved
		   << reserved;
	} else {
		int version = ar.GetObjectSchema();

		ar >> m_memberList >> m_clanName >> m_abbrev;
		ar >> m_guildMasterHandle;
		ar >> m_houseLink;

		if (version > 1) {
			ar >> m_clanArenaPoints >> reserved >> reserved;
		}

		//m_houseLink = -1;
		m_warsWon = 0;
		m_clanWarList = NULL;
		m_clanWarCount = 0;
		//m_clanArenaPoints = 0;
	}
}

IMPLEMENT_SERIAL_SCHEMA(CClanMemberObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CClanMemberList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CClanSystemObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CClanSystemObjList, CObList)

BEGIN_GETSET_IMPL(CClanMemberObj, IDS_CLANMEMBEROBJ_NAME)
GETSET_RANGE(m_memberHandle, gsInt, GS_FLAGS_DEFAULT, 0, 0, CLANMEMBEROBJ, MEMBERHANDLE, INT_MIN, INT_MAX)
GETSET_MAX(m_memberName, gsCString, GS_FLAGS_DEFAULT, 0, 0, CLANMEMBEROBJ, MEMBERNAME, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP