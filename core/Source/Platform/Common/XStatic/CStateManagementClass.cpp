///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <dxerr9.h>
#include <d3dx9.h>
#include <afxtempl.h>
#include <direct.h>
#include <math.h>
#include "CStateManagementClass.h"
#include <KEPHelpers.h>
#include "CMaterialClass.h"
#include <log4cplus/logger.h>
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "TextureObj.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Rendering");

CStateManagementObj::RenderStateParameter CStateManagementObj::RenderState::c_params[MAX_RSPARAMS];

bool CStateManagementObj::RenderState::c_paramsInitialized = false;

CStateManagementObj::CStateManagementObj(const char* baseDir, CShaderObjList*& vertexShaderList, CShaderObjList*& fxShaderList) :
		m_baseDir(baseDir),
		m_shderSystemRef(vertexShaderList),
		m_pxShderSystemRef(fxShaderList),
		m_ambLight(D3DVECTOR{ 0.0f, 0.0f, 0.0f }),
		m_spcLight(D3DVECTOR{ 0.0f, 0.0f, 0.0f }),
		m_fogEnabled(false),
		m_fogCanCull(false),
		m_fogRange(0.0f, 0.0f),
		m_fogColor(D3DVECTOR{ 1.0f, 1.0f, 1.0f }) {
	m_globalBlend = -1;
	m_globalZBias = 0;
	m_globalFilterFog = FALSE;
	m_globalEnvFogOn = FALSE;
	m_multiTextureSupported = 0;
	m_stateChangesPerPass = 0;
	GL_cullOn = TRUE;
	GL_wireFillState = FALSE;
	GL_lastTextureWrapMode = 0;
	m_fogOverride = FALSE;
	m_oldFogColor = D3DCOLOR_XRGB(0, 0, 0);
	m_currentVertexType = -1;
	m_dotProductEnabled = FALSE;
	m_shaderMode = 0;

	m_camWorldPosition.x = 0.0f;
	m_camWorldPosition.y = 0.0f;
	m_camWorldPosition.z = 0.0f;

	m_specularEnable = FALSE;
	m_shderSystemPtr = NULL;
	m_pxShderSystemPtr = NULL;
	m_shderSystemRef = NULL;
	m_pxShderSystemRef = NULL;
	m_currentShader = -99;
	m_currentPxShader = -99;
	m_hightColorRangeStart = 0;
	m_hightColorRangeEnd = 200;
	m_currentDiffuseSource = 0;
	m_supportBUMPENVMAP = FALSE;
	m_stencilMode = FALSE;
	m_stencilBufferAble = FALSE;
	m_lightingMode = TRUE;
	m_shadeMode = 0;
	m_tangentExists = 0;
	m_currentHSLeffect = NULL;
	m_fxShaderOverride = false;

	m_currentAlphaPixelTestState = FALSE;
	m_currentZWritableState = TRUE;
	m_cameraSpaceEnabled = FALSE;

	ZeroMemory(m_matrixInit, sizeof(m_matrixInit));
	m_matrixInit._11 = m_matrixInit._22 = m_matrixInit._33 = m_matrixInit._44 = 1.0f;

	m_invViewMatrix = m_matrixInit;

	ZeroMemory(&m_lastMaterial, sizeof(D3DMATERIAL9));

	m_pVertexDeclaration1UVT = NULL;
	m_pVertexDeclaration2UVT = NULL;
	m_pVertexDeclaration3UVT = NULL;
	m_pVertexDeclaration4UVT = NULL;
	m_pVertexDeclaration5UVT = NULL;
	m_pVertexDeclaration6UVT = NULL;

	for (int i = 0; i < MAX_LIGHTS; i++) {
		m_lastDirLight[i].x = 0.0f;
		m_lastDirLight[i].y = 0.0f;
		m_lastDirLight[i].z = 0.0f;
	}

	for (int loop = 0; loop < 10; loop++) {
		GlobalTextureIndex[loop] = -1;
		GlobalBlendLevels[loop] = -1;
		GlobalColorArg1Levels[loop] = 50;
		GlobalColorArg2Levels[loop] = 50;
	}

	m_pD3dDevice = 0;
	m_currentState = RenderStatePtr(new RenderState());
}

void CStateManagementObj::InitCaps() {
	if (!m_pD3dDevice)
		return;

	m_pD3dDevice->GetDeviceCaps(&m_pCaps);

	m_multiTextureSupported = m_pCaps.MaxSimultaneousTextures;

	if (m_pCaps.TextureOpCaps & D3DTEXOPCAPS_DOTPRODUCT3)
		m_dotProductEnabled = TRUE;

	if (m_pCaps.TextureOpCaps & D3DTEXOPCAPS_BUMPENVMAP)
		m_supportBUMPENVMAP = TRUE;

	if (m_pCaps.MaxActiveLights <= 0 || m_pCaps.MaxActiveLights > 128 /* just in case */) {
		static bool logged = false; // stop spamming the log with this message on machines that report their caps badly
		if (!logged) {
			LogError("Unreasonable value for D3DCAPS9 MaxActiveLights = " << m_pCaps.MaxActiveLights << "; using 8 instead");
			logged = true;
		}
		m_maxActiveLights = 8 - 1;
	} else {
		m_maxActiveLights = m_pCaps.MaxActiveLights - 1; // TODO illamas: remove -1 and modify related code
	}

	//allocate custom vertex formats
	if (!m_pVertexDeclaration1UVT) {
		D3DVERTEXELEMENT9 declU0[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU0, &m_pVertexDeclaration0UV);

		D3DVERTEXELEMENT9 declU1[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 }, // v3
			{ 0, 28, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			D3DDECL_END()
		};

		m_pD3dDevice->CreateVertexDeclaration(declU1, &m_pVertexDeclaration1UV);
		D3DVERTEXELEMENT9 declU2[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU2, &m_pVertexDeclaration2UV);

		D3DVERTEXELEMENT9 declU3[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU3, &m_pVertexDeclaration3UV);

		D3DVERTEXELEMENT9 declU4[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU4, &m_pVertexDeclaration4UV);

		D3DVERTEXELEMENT9 declU5[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			{ 0, 56, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU5, &m_pVertexDeclaration5UV);

		D3DVERTEXELEMENT9 declU6[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			{ 0, 56, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
			{ 0, 64, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 5 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU6, &m_pVertexDeclaration6UV);

		D3DVERTEXELEMENT9 declU7[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			{ 0, 56, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
			{ 0, 64, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 5 },
			{ 0, 72, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 6 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU7, &m_pVertexDeclaration7UV);

		D3DVERTEXELEMENT9 declU8[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			{ 0, 56, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
			{ 0, 64, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 5 },
			{ 0, 72, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 6 },
			{ 0, 80, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 7 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU8, &m_pVertexDeclaration8UV);

		D3DVERTEXELEMENT9 declU0D[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 }, // v3
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU0D, &m_pVertexDeclaration0UVD);

		D3DVERTEXELEMENT9 declU1D[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 }, // v3
			{ 0, 28, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU1D, &m_pVertexDeclaration1UVD);

		D3DVERTEXELEMENT9 declU2D[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 }, // v3
			{ 0, 28, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 36, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declU2D, &m_pVertexDeclaration2UVD);

		D3DVERTEXELEMENT9 decl0[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 }, // v8
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(decl0, &m_pVertexDeclaration0UVT);

		D3DVERTEXELEMENT9 decl1[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 }, // v8
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(decl1, &m_pVertexDeclaration1UVT);

		D3DVERTEXELEMENT9 decl2[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 }, // v8
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(decl2, &m_pVertexDeclaration2UVT);

		D3DVERTEXELEMENT9 decl3[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 }, // v8
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(decl3, &m_pVertexDeclaration3UVT);

		D3DVERTEXELEMENT9 decl4[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			{ 0, 56, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 }, // v8
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(decl4, &m_pVertexDeclaration4UVT);

		D3DVERTEXELEMENT9 decl5[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			{ 0, 56, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
			{ 0, 64, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 }, // v8
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(decl5, &m_pVertexDeclaration5UVT);

		D3DVERTEXELEMENT9 decl6[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 }, // v3
			{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 }, // v7
			{ 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			{ 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 3 },
			{ 0, 56, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 4 },
			{ 0, 64, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 5 },
			{ 0, 72, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 }, // v8
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(decl6, &m_pVertexDeclaration6UVT);

		D3DVERTEXELEMENT9 declLeafDecl[] = {
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 }, // v0
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
			{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 }, // v7
			{ 0, 28, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
			{ 0, 36, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
			D3DDECL_END()
		};
		m_pD3dDevice->CreateVertexDeclaration(declLeafDecl, &m_pVertexLeafUVx);
	}
}

CStateManagementObj::RS_RESULT CStateManagementObj::ResetRenderState() {
	if (!m_pD3dDevice)
		return RS_FAILURE;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return RS_FAILURE;

	InitCaps();

	for (int loop = 0; loop < 10; loop++) {
		GlobalTextureIndex[loop] = -1;
		GlobalBlendLevels[loop] = -1;
		GlobalColorArg1Levels[loop] = 50;
		GlobalColorArg2Levels[loop] = 50;
	}

	for (int stage = 0; stage < 8; stage++) {
		GlobalTextureIndex[stage] = -1;
		m_pD3dDevice->SetTexture(stage, 0); //no texture
		m_pD3dDevice->SetTextureStageState(stage, D3DTSS_TEXCOORDINDEX, stage);
		m_pD3dDevice->SetSamplerState(stage, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pD3dDevice->SetSamplerState(stage, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pD3dDevice->SetSamplerState(stage, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
		m_pD3dDevice->SetSamplerState(stage, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		m_pD3dDevice->SetSamplerState(stage, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		m_pD3dDevice->SetTextureStageState(stage, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		m_pD3dDevice->SetTextureStageState(stage, D3DTSS_COLORARG2, D3DTA_CURRENT);
		m_pD3dDevice->SetTextureStageState(stage, D3DTSS_COLOROP, D3DTOP_DISABLE);
		m_pD3dDevice->SetTextureStageState(stage, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pD3dDevice->SetTextureStageState(stage, D3DTSS_ALPHAARG2, D3DTA_CURRENT);
		m_pD3dDevice->SetTextureStageState(stage, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
	}
	m_pD3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	m_pD3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	GL_lastTextureWrapMode = 0;

	ZeroMemory(&m_lastMaterial, sizeof(D3DMATERIAL9));
	m_pD3dDevice->SetMaterial(&m_lastMaterial);

	InitShaderGlobals();

	SetRenderState(D3DRS_LIGHTING, TRUE);
	m_lightingMode = TRUE;
	SetSpecular(TRUE);
	SetDiffuseSource(0);

	SetShadeMode(0);
	SetWireFillState(FALSE);

	SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	GL_cullOn = 1;

	SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	m_currentZWritableState = TRUE;
	SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	SetZBias(0);

	SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_globalBlend = -1;

	SetAlphaPixelTest(FALSE);
	SetRenderState(D3DRS_ALPHAREF, DWORD(1));
	SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	SetRenderState(D3DRS_DITHERENABLE, TRUE);

	// ED-8089 - Fix Anti-Aliasing Issues
	SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
	SetRenderState(D3DRS_MULTISAMPLEMASK, 0xFFFFFFFFL);

	m_currentVertexType = -1;

	SetStencilMode(FALSE);

	m_fxShaderOverride = false;

	ReD3DX9DeviceRenderState* pRS = pICE->GetReDeviceState()->GetRenderState();
	if (pRS && pRS->GetFogRange()->x != pRS->GetFogRange()->y) {
		m_pD3dDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);
		static bool bUsePerPixelFog = true;
		if (bUsePerPixelFog) {
			m_pD3dDevice->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
			m_pD3dDevice->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);
		} else {
			m_pD3dDevice->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
			m_pD3dDevice->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_LINEAR);
		}

		// Range fog calcuates fog as distance from camera instead of Z distance
		static BOOL bUseRangeFog = TRUE;
		m_pD3dDevice->SetRenderState(D3DRS_RANGEFOGENABLE, bUseRangeFog);

		D3DXVECTOR4 vec = *pRS->GetFogColor();
		vec *= 255.f;
		D3DCOLOR color = D3DCOLOR_XRGB((int)(vec.x), (int)(vec.y), (int)(vec.z));
		m_pD3dDevice->SetRenderState(D3DRS_FOGCOLOR, color);
		m_pD3dDevice->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&pRS->GetFogRange()->x));
		m_pD3dDevice->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&pRS->GetFogRange()->y));
	} else {
		m_pD3dDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);
	}

	return RS_SUCCESS;
}

void CStateManagementObj::SafeDelete() {
	if (m_pVertexDeclaration1UVT) {
		m_pVertexDeclaration1UVT->Release();
		m_pVertexDeclaration1UVT = 0;
	}

	if (m_pVertexDeclaration2UVT) {
		m_pVertexDeclaration2UVT->Release();
		m_pVertexDeclaration2UVT = 0;
	}

	if (m_pVertexDeclaration3UVT) {
		m_pVertexDeclaration3UVT->Release();
		m_pVertexDeclaration3UVT = 0;
	}
	if (m_pVertexDeclaration4UVT) {
		m_pVertexDeclaration4UVT->Release();
		m_pVertexDeclaration4UVT = 0;
	}
	if (m_pVertexDeclaration5UVT) {
		m_pVertexDeclaration5UVT->Release();
		m_pVertexDeclaration5UVT = 0;
	}
	if (m_pVertexDeclaration6UVT) {
		m_pVertexDeclaration6UVT->Release();
		m_pVertexDeclaration6UVT = 0;
	}
	if (m_pVertexLeafUVx) {
		m_pVertexLeafUVx->Release();
		m_pVertexLeafUVx = 0;
	}
}

void CStateManagementObj::SetLightingMode(BOOL state) {
	if (m_lightingMode == state)
		return;

	if (state == FALSE) {
		SetRenderState(D3DRS_LIGHTING, FALSE);
		m_lightingMode = FALSE;
	} else {
		SetRenderState(D3DRS_LIGHTING, TRUE);
		m_lightingMode = TRUE;
	}
}

void CStateManagementObj::SetShadeMode(int state) {
	if (m_shadeMode == state)
		return;

	if (state == 0) {
		//disable
		SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
		m_shadeMode = 0;
	} else if (state == 1) {
		//enable
		SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);
		m_shadeMode = 1;
	} else if (state == 2) {
		//enable
		SetRenderState(D3DRS_SHADEMODE, D3DSHADE_PHONG);
		m_shadeMode = 2;
	}
}

void CStateManagementObj::SetStencilMode(BOOL state) {
	if (!m_stencilBufferAble)
		return;

	if (state == m_stencilMode)
		return;

	if (state == FALSE) {
		//disable
		SetRenderState(D3DRS_STENCILENABLE, FALSE);
		m_stencilMode = FALSE;
	} else {
		//enable
		SetRenderState(D3DRS_STENCILENABLE, TRUE);
		m_stencilMode = TRUE;
	}
}

void CStateManagementObj::SetTextureState(DWORD textureIndex, int texturePass, TextureDatabase* pTDB) {
	if (!m_pD3dDevice)
		return;

	if (GlobalTextureIndex[texturePass] == textureIndex)
		return;

	if (textureIndex < 0) {
		m_pD3dDevice->SetTexture(texturePass, 0); //no texture
		GlobalTextureIndex[texturePass] = textureIndex;
		m_stateChangesPerPass++;
		return;
	}

	if (!pTDB)
		return;

	int tex_type;
	float parameter = 0.0f;
	pTDB->TextureGet(textureIndex)->Callback(m_pD3dDevice, texturePass, tex_type, parameter);

	CTextureEX* texture = pTDB->TextureGet(textureIndex);
	if (texture) {
		m_pD3dDevice->SetTexture(texturePass, texture->GetD3DTextureIfLoadedOrQueueToLoad());
		GlobalTextureIndex[texturePass] = textureIndex;
		if (texturePass == 0)
			m_stateChangesPerPass++;
	}
}

void CStateManagementObj::SetBlendLevel(int level, int type) {
	if (!m_pD3dDevice)
		return;

	if (GlobalBlendLevels[level] == type)
		return;

	GlobalBlendLevels[level] = type;

	switch (type) {
		case CMaterialObject::TOP_DISABLE:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_DISABLE);
			break;
		case CMaterialObject::TOP_MODULATE:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_MODULATE);
			break;
		case CMaterialObject::TOP_ADD:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_ADD);
			break;
		case CMaterialObject::TOP_SUBTRACT:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_SUBTRACT);
			break;
		case CMaterialObject::TOP_ADDSMOOTH:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_ADDSMOOTH);
			break;
		case CMaterialObject::TOP_ADDSIGNED:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_ADDSIGNED);
			break;
		case CMaterialObject::TOP_MODULATE2X:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_MODULATE2X);
			break;
		case CMaterialObject::TOP_TYPE6:
			if (m_dotProductEnabled)
				m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_DOTPRODUCT3);
			else
				m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_MODULATE);
			break;
		case CMaterialObject::TOP_TYPE7:
			if (m_supportBUMPENVMAP)
				m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_BUMPENVMAP);
			else
				m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_MODULATE);
			break;
		case CMaterialObject::TOP_SELECTARG1:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
			break;
		case CMaterialObject::TOP_SELECTARG2:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
			break;
		case CMaterialObject::TOP_MODULATE4X:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_MODULATE4X);
			break;
		case CMaterialObject::TOP_ADDSIGNED2X:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_ADDSIGNED2X);
			break;
		case CMaterialObject::TOP_PREMODULATE:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_PREMODULATE);
			break;
		case CMaterialObject::TOP_MULTIPLYADD:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_MULTIPLYADD);
			break;
		case CMaterialObject::TOP_BLENDCURRENTALPHA:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLOROP, D3DTOP_BLENDCURRENTALPHA);
			if (level > 0)
				m_pD3dDevice->SetTextureStageState(level - 1, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
			return;
			break;
	}

	// set default to no alpha op
	if (level > 1)
		m_pD3dDevice->SetTextureStageState(level - 1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
	else if (level > 0)
		m_pD3dDevice->SetTextureStageState(level - 1, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

void CStateManagementObj::SetColorArg2Level(int level, char type) {
	if (!m_pD3dDevice)
		return;

	if (GlobalColorArg2Levels[level] == type)
		return;

	GlobalColorArg2Levels[level] = type;

	switch (type) {
		case 0:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
			break;
		case 1:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_CURRENT);
			break;
		case 2:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_TEXTURE);
			break;
		case 3:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_TFACTOR);
			break;
		case 4:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_SPECULAR);
			break;
		case 5:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_SELECTMASK);
			break;
		case 6:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_DIFFUSE | D3DTA_COMPLEMENT); //inverse diffuse
			break;
		case 7:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_CURRENT | D3DTA_COMPLEMENT); //inverse current
			break;
		case 8:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_TEMP | D3DTA_COMPLEMENT); //inverse temp
			break;
		case 9:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_TEXTURE | D3DTA_COMPLEMENT); //inverse texture
			break;
		case 10:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG2, D3DTA_TEXTURE | D3DTA_ALPHAREPLICATE); //texture with alpha
			break;
	}
}

void CStateManagementObj::SetColorArg1Level(int level, char type) {
	if (!m_pD3dDevice)
		return;

	if (GlobalColorArg1Levels[level] == type)
		return;

	GlobalColorArg1Levels[level] = type;

	switch (type) {
		case 0:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
			break;
		case 1:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_CURRENT);
			break;
		case 2:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			break;
		case 3:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_TFACTOR);
			break;
		case 4:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_SPECULAR);
			break;
		case 5:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_SELECTMASK);
			break;
		case 6:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_DIFFUSE | D3DTA_COMPLEMENT); //inverse diffuse
			break;
		case 7:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_CURRENT | D3DTA_COMPLEMENT); //inverse current
			break;
		case 8:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_TEMP | D3DTA_COMPLEMENT); //inverse temp
			break;
		case 9:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_TEXTURE | D3DTA_COMPLEMENT); //inverse texture
			break;
		case 10:
			m_pD3dDevice->SetTextureStageState(level, D3DTSS_COLORARG1, D3DTA_TEXTURE | D3DTA_ALPHAREPLICATE); //texture with alpha
			break;
	}
}

void CStateManagementObj::SetDiffuseSource(int type) {
	if (m_currentDiffuseSource == type)
		return;

	m_currentDiffuseSource = type;

	switch (m_currentDiffuseSource) {
		case 0:
			SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_MATERIAL);
			break;
		case 1:
			SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
			break;
	}
}

void CStateManagementObj::SetVertexType(int uvCount) {
	if (!m_pD3dDevice)
		return;

	int tempType = uvCount;

	if (m_currentDiffuseSource == 1)
		tempType = tempType + 9;

	m_currentVertexType = tempType;

	switch (m_currentVertexType) {
		case 0:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration0UV);
			break;
		case 1:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration1UV);
			break;
		case 2:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration2UV);
			break;
		case 3:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration3UV);
			break;
		case 4:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration4UV);
			break;
		case 5:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration5UV);
			break;
		case 6:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration6UV);
			break;
		case 7:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration7UV);
			break;
		case 8:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration8UV);
			break;
		case 9:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration0UVD);
			break;
		case 10:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration1UVD);
			break;
		case 11:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration2UVD);
			break;
		case 12:
			m_pD3dDevice->SetFVF((D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX3));
			break;
		case 13:
			m_pD3dDevice->SetFVF((D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX4));
			break;
		case 14:
			m_pD3dDevice->SetFVF((D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX5));
			break;
		case 15:
			m_pD3dDevice->SetFVF((D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX6));
			break;
		case 16:
			m_pD3dDevice->SetFVF((D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX7));
			break;
		case 17:
			m_pD3dDevice->SetFVF((D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX8));
			break;
		case 18:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration0UVT);
			break;
		case 19:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration1UVT);
			break;
		case 20:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration2UVT);
			break;
		case 21:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration3UVT);
			break;
		case 22:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration4UVT);
			break;
		case 23:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration5UVT);
			break;
		case 24:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration6UVT);
			break;
		case 50:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexDeclaration3UVT);
			break;
		case 70:
			m_pD3dDevice->SetVertexDeclaration(m_pVertexLeafUVx);
			break;
	}
}

void CStateManagementObj::SetWindGlobal(D3DVECTOR curPush) {
	if (!m_pD3dDevice)
		return;

	m_pD3dDevice->SetVertexShaderConstantF(VAL_WIND, (float*)&curPush, 4);
}

void CStateManagementObj::SetZWritableState(BOOL state) {
	if (m_currentZWritableState == state)
		return;

	m_currentZWritableState = state;

	SetRenderState(D3DRS_ZWRITEENABLE, state);
}

void CStateManagementObj::SetAlphaPixelTest(BOOL state) {
	if (m_currentAlphaPixelTestState == state)
		return;

	m_currentAlphaPixelTestState = state;

	SetRenderState(D3DRS_ALPHATESTENABLE, state);
}

BOOL CStateManagementObj::SetZBias(int bias) {
	if (m_globalZBias == bias)
		return FALSE;

	float depthBias = bias * (1.0f / 65536.0f);
	float slopeScaleBias = 0;
	SetRenderState(D3DRS_DEPTHBIAS, *((DWORD*)(&depthBias)));
	SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, *((DWORD*)(&slopeScaleBias)));
	m_globalZBias = bias;
	return TRUE;
}

BOOL CStateManagementObj::SetBlendType(int type) {
	//-1 = none 1 = even opacity 2 = opacity map darker is more transparent
	if (m_globalBlend == type) //same
		return FALSE;

	switch (type) {
		case -1:
			SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
			m_globalBlend = type;
			break;
		case 0:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			m_globalBlend = type;
			break;
		case 1:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			m_globalBlend = type;
			break;
		case 2:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			m_globalBlend = type;
			break;
		case 3:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCCOLOR);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_DESTCOLOR);
			m_globalBlend = type;
			break;
		case 4: //inversion amplification
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCCOLOR);
			m_globalBlend = type;
			break;
		case 5:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCCOLOR);
			m_globalBlend = type;
			break;
		case 6:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
			m_globalBlend = type;
			break;
		case 7:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			m_globalBlend = type;
			break;
		case 8:
			SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
			SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			m_globalBlend = type;
			break;
	}

	return TRUE;
}

BOOL CStateManagementObj::SetCullState(int onType) {
	if (onType == GL_cullOn)
		return FALSE;

	if (onType == 1)
		SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	else if (onType == 0)
		SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	else if (onType == 2)
		SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);

	GL_cullOn = onType;
	return TRUE;
}

BOOL CStateManagementObj::SetFogOverrideState(BOOL on) {
	if (on == m_fogOverride)
		return FALSE;

	if (on == FALSE) {
		SetRenderState(D3DRS_FOGCOLOR, m_oldFogColor);
	} else {
		D3DCOLOR colorLoc = D3DCOLOR_XRGB(0, 0, 0);
		GetRenderState(D3DRS_FOGCOLOR, &m_oldFogColor);
		SetRenderState(D3DRS_FOGCOLOR, colorLoc);
	}

	m_fogOverride = on;
	return TRUE;
}

BOOL CStateManagementObj::SetWireFillState(BOOL on) {
	if (on == GL_wireFillState)
		return FALSE;

	if (on == FALSE)
		SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	else
		SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

	GL_wireFillState = on;
	return TRUE;
}

BOOL CStateManagementObj::SetTextureTileMode(int mode) {
	if (!m_pD3dDevice)
		return FALSE;

	if (GL_lastTextureWrapMode == mode)
		return FALSE;

	switch (mode) {
		case 0:
			m_pD3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
			m_pD3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
			break;
		case 1:
			m_pD3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
			m_pD3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
			break;
		case 2:
			m_pD3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
			m_pD3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
			break;
	}

	GL_lastTextureWrapMode = mode;
	return TRUE;
}

BOOL CStateManagementObj::InitShaderGlobals() {
	if (!m_pD3dDevice)
		return FALSE;

	D3DXVECTOR4 vZero(0.0f, 0.0f, 0.0f, 0.0f);
	D3DXVECTOR4 vOne(1.0f, 1.0f, 1.0f, 1.0f);
	D3DXVECTOR4 half(0.5f, 0.5f, 0.5f, 0.5f);
	D3DXVECTOR4 maxPower(128.0f, 128.0f, 128.0f, 128.0f);
	m_pD3dDevice->SetVertexShaderConstantF(VAL_ONE, (float*)&vOne, 1);
	m_pD3dDevice->SetVertexShaderConstantF(VAL_ZERO, (float*)&vZero, 1);
	m_pD3dDevice->SetVertexShaderConstantF(VAL_HALF, (float*)&half, 1);
	m_pD3dDevice->SetVertexShaderConstantF(MAX_POWER, (float*)&maxPower, 1);

	SetWindGlobal(D3DVECTOR{ 0, 0, 0 });

	return TRUE;
}

void CStateManagementObj::SetWorldMatrix(const D3DXMATRIX* pWorld) {
	if (!m_pD3dDevice)
		return;

	m_pD3dDevice->SetTransform(D3DTS_WORLD, pWorld);

	if (m_currentShader == -1)
		return;

	D3DXMatrixTranspose(&m_tempMatrix, pWorld);
	m_pD3dDevice->SetVertexShaderConstantF(WORLD_MATRIX, (float*)&m_tempMatrix, 4);
}

void CStateManagementObj::SetViewMatrix(const D3DXMATRIX* pView) {
	if (!m_pD3dDevice)
		return;

	m_pD3dDevice->SetTransform(D3DTS_VIEW, pView);
	m_viewMatrixUTP = *pView; //untransposed
	D3DXMatrixInverse(&m_invViewMatrix, NULL, pView);
	D3DXMatrixTranspose(&m_viewMatrix, pView);
}

void CStateManagementObj::SetProjectionMatrix(const D3DXMATRIX* pProj) {
	if (!m_pD3dDevice)
		return;

	m_pD3dDevice->SetTransform(D3DTS_PROJECTION, pProj);
	m_projMatrixUTP = *pProj; //untransposed
	D3DXMatrixTranspose(&m_projMatrix, pProj);
	m_pD3dDevice->SetVertexShaderConstantF(CLIP_MATRIX, (float*)&m_projMatrix, 4);
}

void CStateManagementObj::SetMaterial(const D3DMATERIAL9* material) {
	if (!m_pD3dDevice)
		return;

	if (material == 0)
		return;

	CopyMemory(&m_lastMaterial, material, sizeof(D3DMATERIAL9));

	if (material) {
		if (material->Specular.a == 0.0f) {
			SetSpecular(FALSE);
		} else {
			SetSpecular(TRUE);
		}

		m_pD3dDevice->SetMaterial(material);
	} else {
		SetSpecular(FALSE);
		return;
	}

	if (m_currentShader == -1)
		return;

	m_pD3dDevice->SetVertexShaderConstantF(DIFFUSE_COLOR, (float*)&material->Diffuse, 1);
	m_pD3dDevice->SetVertexShaderConstantF(AMBIENT_COLOR, (float*)&material->Ambient, 1);
	m_pD3dDevice->SetVertexShaderConstantF(EMMISIVE_COLOR, (float*)&material->Emissive, 1);
}

void CStateManagementObj::SetDirLight(D3DVECTOR dirVect, float r, float g, float b) {
	if (!m_pD3dDevice)
		return;

	D3DXVECTOR4 dirColor(r, g, b, 0.0f);
	m_lastDirLight[0] = dirVect; // TODO: gframe this is most likely wrong we are using this in the STLight now
	m_pD3dDevice->SetVertexShaderConstantF(DIR_LIGHTVECT, (float*)&dirVect, 1);
	m_pD3dDevice->SetVertexShaderConstantF(DIR_LIGHTCOLOR, (float*)&dirColor, 1);
}

void CStateManagementObj::SetSTLight(int index, D3DVECTOR position, float r, float g, float b, float rg) {
	if (!m_pD3dDevice)
		return;

	if (index < 1 || index > MAX_LIGHTS)
		return;

	D3DXVECTOR4 lightDir(position.x, position.y, position.z, rg);
	D3DXVECTOR4 LightColor(r, g, b, 0.0f);

	m_pD3dDevice->SetVertexShaderConstantF(POINT_LIGHT1 + (2 * (index - 1)), (float*)&lightDir, 1);
	m_pD3dDevice->SetVertexShaderConstantF(POINTLIGHT1_COLOR + (2 * (index - 1)), (float*)&LightColor, 1);

	m_lastDirLight[index] = position;
	m_lastColorLight[index].x = LightColor.x;
	m_lastColorLight[index].y = LightColor.y;
	m_lastColorLight[index].z = LightColor.z;
}

void CStateManagementObj::SetSpecular(BOOL state) {
	m_specularEnable = state;
	SetRenderState(D3DRS_SPECULARENABLE, m_specularEnable);
}

void CStateManagementObj::SetFogConstant(float startPoint, float endPoint) {
	if (!m_pD3dDevice)
		return;

	D3DXVECTOR4 fogVal(startPoint, endPoint, 1.0f / (endPoint - startPoint), 0.0f);
	m_pD3dDevice->SetVertexShaderConstantF(FOG_RANGE, (float*)&fogVal, 1);
}

void CStateManagementObj::SetVertexShader(CShaderObjList* shaderSystem, int ShaderIndexID) {
	if (!m_pD3dDevice)
		return;

	// moved up before return, set it regardless to let reload set the new ptr
	if (shaderSystem)
		m_shderSystemPtr = shaderSystem;
	else if (&m_shderSystemRef)
		m_shderSystemPtr = m_shderSystemRef;
	else
		m_shderSystemPtr = NULL;

	if (m_currentShader == ShaderIndexID && m_currentHSLeffect == NULL)
		return;

	m_currentShader = ShaderIndexID;

	if (m_shderSystemPtr)
		m_shderSystemPtr->SetShaderByIndex(m_pD3dDevice, ShaderIndexID, 0, &m_currentHSLeffect, m_currentEffectParamCountCRC, &m_currentHSLcode, NULL); //-1 fixed pipeline
}

void CStateManagementObj::SetPxShader(CShaderObjList* shaderSystem, int ShaderIndexID) {
	if (!m_pD3dDevice)
		return;

	// moved up before return, set it regardless to let reload set the new ptr
	if (shaderSystem)
		m_pxShderSystemPtr = shaderSystem;
	else if (&m_pxShderSystemRef)
		m_pxShderSystemPtr = m_pxShderSystemRef;
	else
		m_pxShderSystemPtr = NULL;

	if (m_currentPxShader == ShaderIndexID && m_currentHSLeffect == NULL)
		return;

	m_currentPxShader = ShaderIndexID;
	m_currentHSLeffect = NULL;
	m_currentPxFxShader = NULL;

	if (m_pxShderSystemPtr)
		m_pxShderSystemPtr->SetShaderByIndex(m_pD3dDevice, ShaderIndexID, 1, &m_currentHSLeffect, m_currentEffectParamCountCRC, &m_currentHSLcode, &m_currentPxFxShader); //-1 fixed pipeline
}

void CStateManagementObj::OverrideFXShader(LPD3DXEFFECT aFXShader) {
	if (!m_pD3dDevice)
		return;

	SetPxShader(0, -1);

	if (aFXShader == 0) {
		m_fxShaderOverride = false;
		return;
	}

	m_fxShaderOverride = true;

	m_currentHSLeffect = aFXShader;
}

void CStateManagementObj::SetFXShaderParameters(CMaterialObject* aMaterial, const DWORD* aTextureIdxList, TextureDatabase* aTextureDB, const D3DXMATRIX& aWorldMatrix) {
	if (!m_pD3dDevice)
		return;

	if (m_currentHSLeffect == 0)
		return;

	if (aMaterial->m_paramCount == 0 || aMaterial->m_paramCount != m_currentEffectParamCountCRC)
		aMaterial->InitParamBlockFXShadersHSL(m_currentHSLeffect);

	HRESULT errHr = 0;
	int curTextureAssign = 0;

	for (int pLoop = 0; pLoop < aMaterial->m_paramCount; pLoop++) {
		switch (aMaterial->m_paramBlock[pLoop].type) {
			case ARBP_UKNOWN: //this will do nothing, because default setting are embedded
				break;

			case ARBP_Projection:
				errHr = m_currentHSLeffect->SetMatrix(m_currentHSLeffect->GetParameter(NULL, pLoop), &m_projMatrixUTP);
				break;

			case ARBP_WorldInverseTranspose:
				D3DXMatrixInverse(&m_worldInverseTransposeRegister, NULL, &aWorldMatrix);
				D3DXMatrixTranspose(&m_worldInverseTransposeRegister, &m_worldInverseTransposeRegister);
				errHr = m_currentHSLeffect->SetMatrix(m_currentHSLeffect->GetParameter(NULL, pLoop), &m_worldInverseTransposeRegister);
				break;

			case ARBP_WorldViewProjection:
				m_worldViewProjectionMatrixRegister = aWorldMatrix * m_viewMatrixUTP * m_projMatrixUTP;
				errHr = m_currentHSLeffect->SetMatrix(m_currentHSLeffect->GetParameter(NULL, pLoop), &m_worldViewProjectionMatrixRegister);
				break;

			case ARBP_WorldView:
				m_worldViewMatrixRegister = aWorldMatrix * m_viewMatrixUTP;
				errHr = m_currentHSLeffect->SetMatrix(m_currentHSLeffect->GetParameter(NULL, pLoop), &m_worldViewMatrixRegister);
				break;

			case ARBP_World:
				errHr = m_currentHSLeffect->SetMatrix(m_currentHSLeffect->GetParameter(NULL, pLoop), &aWorldMatrix);
				break;

			case ARBP_ViewInverse:
				errHr = m_currentHSLeffect->SetMatrix(m_currentHSLeffect->GetParameter(NULL, pLoop), &m_invViewMatrix);
				break;

			case ARBP_Time:
				errHr = m_currentHSLeffect->SetFloat(m_currentHSLeffect->GetParameter(NULL, pLoop), fTime::TimeMs() * .001f);
				break;

			case ARBP_DirLight: {
				float ldv4[4];
				ldv4[0] = m_lastDirLight[0].x;
				ldv4[1] = m_lastDirLight[0].y;
				ldv4[2] = m_lastDirLight[0].z;
				ldv4[3] = 1.0f;
				errHr = m_currentHSLeffect->SetValue(m_currentHSLeffect->GetParameter(NULL, pLoop), ldv4, aMaterial->m_paramBlock[pLoop].dataSize);
			} break;

			case ARBP_Diffuse:
				errHr = m_currentHSLeffect->SetValue(m_currentHSLeffect->GetParameter(NULL, pLoop), &aMaterial->m_useMaterial.Diffuse, aMaterial->m_paramBlock[pLoop].dataSize);
				break;

			case ARBP_Specular:
				errHr = m_currentHSLeffect->SetValue(m_currentHSLeffect->GetParameter(NULL, pLoop), &aMaterial->m_useMaterial.Specular, aMaterial->m_paramBlock[pLoop].dataSize);
				break;

			case ARBP_Emmisive:
				errHr = m_currentHSLeffect->SetValue(m_currentHSLeffect->GetParameter(NULL, pLoop), &aMaterial->m_useMaterial.Emissive, aMaterial->m_paramBlock[pLoop].dataSize);
				break;

			case ARBP_Ambient:
				errHr = m_currentHSLeffect->SetValue(m_currentHSLeffect->GetParameter(NULL, pLoop), &aMaterial->m_useMaterial.Ambient, aMaterial->m_paramBlock[pLoop].dataSize);
				break;

			case ARBP_SpecPower:
				errHr = m_currentHSLeffect->SetValue(m_currentHSLeffect->GetParameter(NULL, pLoop), &aMaterial->m_useMaterial.Power, aMaterial->m_paramBlock[pLoop].dataSize);
				break;

			case ARBP_Texture:
				curTextureAssign;
				LogError("ARBP_Texture deprecated");
				break;

			case ARBP_VolumeTexturePrcdrl: {
				//volume texture proc handle

				if (m_currentPxFxShader->m_shaderUsedOnce == 0) {
					//needs initialization for proceedural info
					LPD3DXTEXTURESHADER pTextureShader = NULL;
					LPD3DXBUFFER pFunction = NULL;
					D3DXPARAMETER_DESC localDescStruct;
					D3DXHANDLE hAnnot;
					D3DXPARAMETER_DESC AnnotDesc;
					int Width, Height, Depth;
					const char *textureType, *pstrFunction, *pstrTarget, *pstrName;
					textureType = pstrFunction = pstrTarget = pstrName = NULL;

					if (m_currentHSLeffect->GetParameterDesc(m_currentHSLeffect->GetParameter(NULL, pLoop), &localDescStruct) != S_OK)
						break;

					if (localDescStruct.Annotations > 0) {
						for (UINT iAnnot = 0; iAnnot < localDescStruct.Annotations; iAnnot++) {
							hAnnot = m_currentHSLeffect->GetAnnotation(m_currentHSLeffect->GetParameter(NULL, pLoop), iAnnot);
							m_currentHSLeffect->GetParameterDesc(hAnnot, &AnnotDesc);
							if (_strcmpi(AnnotDesc.Name, "name") == 0)
								m_currentHSLeffect->GetString(hAnnot, &pstrName);
							else if (_strcmpi(AnnotDesc.Name, "function") == 0)
								m_currentHSLeffect->GetString(hAnnot, &pstrFunction);
							else if (_strcmpi(AnnotDesc.Name, "target") == 0)
								m_currentHSLeffect->GetString(hAnnot, &pstrTarget);
							else if (_strcmpi(AnnotDesc.Name, "width") == 0)
								m_currentHSLeffect->GetInt(hAnnot, &Width);
							else if (_strcmpi(AnnotDesc.Name, "height") == 0)
								m_currentHSLeffect->GetInt(hAnnot, &Height);
							else if (_strcmpi(AnnotDesc.Name, "depth") == 0)
								m_currentHSLeffect->GetInt(hAnnot, &Depth);
							else if (_strcmpi(AnnotDesc.Name, "type") == 0)
								m_currentHSLeffect->GetString(hAnnot, &textureType);
						}
					}

					HRESULT hr = 0;

					if (pstrTarget == NULL)
						pstrTarget = "tx_1_0";

					LPD3DXBUFFER pBufferErrors = NULL;
					if (SUCCEEDED(hr = D3DXCompileShader(
									  m_currentHSLcode,
									  (UINT)m_currentHSLcode.GetLength(),
									  NULL,
									  NULL,
									  pstrFunction,
									  pstrTarget,
									  0,
									  &pFunction,
									  &pBufferErrors,
									  NULL))) {
						//compile tx shder
						if (SUCCEEDED(hr = D3DXCreateTextureShader((DWORD*)pFunction->GetBufferPointer(), &pTextureShader))) {
							//create texture shader
							pTextureShader->SetDefaults();
							if (Width == D3DX_DEFAULT)
								Width = 64;
							if (Height == D3DX_DEFAULT)
								Height = 64;
							if (Depth == D3DX_DEFAULT)
								Depth = 64;

							LPDIRECT3DVOLUMETEXTURE9 pVolumeTex = NULL;
							if (SUCCEEDED(hr = D3DXCreateVolumeTexture(m_pD3dDevice,
											  Width, Height, Depth, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &pVolumeTex))) {
								if (SUCCEEDED(hr = D3DXFillVolumeTextureTX(pVolumeTex, pTextureShader)))
									hr = m_currentHSLeffect->SetTexture(m_currentHSLeffect->GetParameter(NULL, pLoop), pVolumeTex);
							}

							//SAFE_RELEASE(pTex);
							pTextureShader->Release();
							pTextureShader = 0;
						} //end create texture shader
						pFunction->Release();
					} //end compile tx shder
				} //end needs initialization for proceedural info
			} break;

			case ARBP_WorldInverse:
				D3DXMatrixInverse(&m_worldInverseTransposeRegister, NULL, &aWorldMatrix);
				errHr = m_currentHSLeffect->SetMatrix(m_currentHSLeffect->GetParameter(NULL, pLoop), &m_worldInverseTransposeRegister);
				break;

			case ARBP_USEDEFAULT:
				errHr = m_currentHSLeffect->SetValue(m_currentHSLeffect->GetParameter(NULL, pLoop),
					aMaterial->m_paramBlock[pLoop].data,
					aMaterial->m_paramBlock[pLoop].dataSize);
				break;
		}
	} //end set params

	m_currentPxFxShader->m_shaderUsedOnce = 1;
}

void CStateManagementObj::RenderState::ResetParams() {
	if (c_paramsInitialized)
		return;
	c_paramsInitialized = true;

	for (UINT idx = 0; idx < MAX_RSPARAMS; idx++) {
		c_params[idx].paramEnum = -1;
		c_params[idx].paramFlags = 0;
	}

#define RS_SETPARAM(param)                  \
	{                                       \
		c_params[param].paramEnum = param;  \
		c_params[param].paramDesc = #param; \
	}

#define RS_SETFLOAT(param) \
	c_params[param].paramFlags = RenderStateParameter::RSPARAM_ISFLOAT;

	RS_SETPARAM(D3DRS_ZENABLE)
	RS_SETPARAM(D3DRS_FILLMODE)
	RS_SETPARAM(D3DRS_SHADEMODE)
	RS_SETPARAM(D3DRS_ZWRITEENABLE)
	RS_SETPARAM(D3DRS_ALPHATESTENABLE)
	RS_SETPARAM(D3DRS_LASTPIXEL)
	RS_SETPARAM(D3DRS_SRCBLEND)
	RS_SETPARAM(D3DRS_DESTBLEND)
	RS_SETPARAM(D3DRS_CULLMODE)
	RS_SETPARAM(D3DRS_ZFUNC)
	RS_SETPARAM(D3DRS_ALPHAREF)
	RS_SETPARAM(D3DRS_ALPHAFUNC)
	RS_SETPARAM(D3DRS_DITHERENABLE)
	RS_SETPARAM(D3DRS_ALPHABLENDENABLE)
	RS_SETPARAM(D3DRS_FOGENABLE)
	RS_SETPARAM(D3DRS_SPECULARENABLE)
	RS_SETPARAM(D3DRS_FOGCOLOR)
	RS_SETPARAM(D3DRS_FOGTABLEMODE)
	RS_SETPARAM(D3DRS_FOGSTART)
	RS_SETFLOAT(D3DRS_FOGSTART)
	RS_SETPARAM(D3DRS_FOGEND)
	RS_SETFLOAT(D3DRS_FOGEND)
	RS_SETPARAM(D3DRS_FOGDENSITY)
	RS_SETFLOAT(D3DRS_FOGDENSITY)
	RS_SETPARAM(D3DRS_RANGEFOGENABLE)
	RS_SETPARAM(D3DRS_STENCILENABLE)
	RS_SETPARAM(D3DRS_STENCILFAIL)
	RS_SETPARAM(D3DRS_STENCILZFAIL)
	RS_SETPARAM(D3DRS_STENCILPASS)
	RS_SETPARAM(D3DRS_STENCILFUNC)
	RS_SETPARAM(D3DRS_STENCILREF)
	RS_SETPARAM(D3DRS_STENCILMASK)
	RS_SETPARAM(D3DRS_STENCILWRITEMASK)
	RS_SETPARAM(D3DRS_TEXTUREFACTOR)
	RS_SETPARAM(D3DRS_WRAP0)
	RS_SETPARAM(D3DRS_WRAP1)
	RS_SETPARAM(D3DRS_WRAP2)
	RS_SETPARAM(D3DRS_WRAP3)
	RS_SETPARAM(D3DRS_WRAP4)
	RS_SETPARAM(D3DRS_WRAP5)
	RS_SETPARAM(D3DRS_WRAP6)
	RS_SETPARAM(D3DRS_WRAP7)
	RS_SETPARAM(D3DRS_CLIPPING)
	RS_SETPARAM(D3DRS_LIGHTING)
	RS_SETPARAM(D3DRS_AMBIENT)
	RS_SETPARAM(D3DRS_FOGVERTEXMODE)
	RS_SETPARAM(D3DRS_COLORVERTEX)
	RS_SETPARAM(D3DRS_LOCALVIEWER)
	RS_SETPARAM(D3DRS_NORMALIZENORMALS)
	RS_SETPARAM(D3DRS_DIFFUSEMATERIALSOURCE)
	RS_SETPARAM(D3DRS_SPECULARMATERIALSOURCE)
	RS_SETPARAM(D3DRS_AMBIENTMATERIALSOURCE)
	RS_SETPARAM(D3DRS_EMISSIVEMATERIALSOURCE)
	RS_SETPARAM(D3DRS_VERTEXBLEND)
	RS_SETPARAM(D3DRS_CLIPPLANEENABLE)
	RS_SETPARAM(D3DRS_POINTSIZE)
	RS_SETFLOAT(D3DRS_POINTSIZE)
	RS_SETPARAM(D3DRS_POINTSIZE_MIN)
	RS_SETFLOAT(D3DRS_POINTSIZE_MIN)
	RS_SETPARAM(D3DRS_POINTSPRITEENABLE)
	RS_SETPARAM(D3DRS_POINTSCALEENABLE)
	RS_SETPARAM(D3DRS_POINTSCALE_A)
	RS_SETFLOAT(D3DRS_POINTSCALE_A)
	RS_SETPARAM(D3DRS_POINTSCALE_B)
	RS_SETFLOAT(D3DRS_POINTSCALE_B)
	RS_SETPARAM(D3DRS_POINTSCALE_C)
	RS_SETFLOAT(D3DRS_POINTSCALE_C)
	RS_SETPARAM(D3DRS_MULTISAMPLEANTIALIAS)
	RS_SETPARAM(D3DRS_MULTISAMPLEMASK)
	RS_SETPARAM(D3DRS_PATCHEDGESTYLE)
	RS_SETPARAM(D3DRS_DEBUGMONITORTOKEN)
	RS_SETPARAM(D3DRS_POINTSIZE_MAX)
	RS_SETFLOAT(D3DRS_POINTSIZE_MAX)
	RS_SETPARAM(D3DRS_INDEXEDVERTEXBLENDENABLE)
	RS_SETPARAM(D3DRS_COLORWRITEENABLE)
	RS_SETPARAM(D3DRS_TWEENFACTOR)
	RS_SETFLOAT(D3DRS_TWEENFACTOR)
	RS_SETPARAM(D3DRS_BLENDOP)
	RS_SETPARAM(D3DRS_POSITIONDEGREE)
	RS_SETPARAM(D3DRS_NORMALDEGREE)
	RS_SETPARAM(D3DRS_SCISSORTESTENABLE)
	RS_SETPARAM(D3DRS_SLOPESCALEDEPTHBIAS)
	RS_SETPARAM(D3DRS_ANTIALIASEDLINEENABLE)
	RS_SETPARAM(D3DRS_MINTESSELLATIONLEVEL)
	RS_SETPARAM(D3DRS_MAXTESSELLATIONLEVEL)
	RS_SETPARAM(D3DRS_ADAPTIVETESS_X)
	RS_SETPARAM(D3DRS_ADAPTIVETESS_Y)
	RS_SETPARAM(D3DRS_ADAPTIVETESS_Z)
	RS_SETPARAM(D3DRS_ADAPTIVETESS_W)
	RS_SETPARAM(D3DRS_ENABLEADAPTIVETESSELLATION)
	RS_SETPARAM(D3DRS_TWOSIDEDSTENCILMODE)
	RS_SETPARAM(D3DRS_CCW_STENCILFAIL)
	RS_SETPARAM(D3DRS_CCW_STENCILZFAIL)
	RS_SETPARAM(D3DRS_CCW_STENCILPASS)
	RS_SETPARAM(D3DRS_CCW_STENCILFUNC)
	RS_SETPARAM(D3DRS_COLORWRITEENABLE1)
	RS_SETPARAM(D3DRS_COLORWRITEENABLE2)
	RS_SETPARAM(D3DRS_COLORWRITEENABLE3)
	RS_SETPARAM(D3DRS_BLENDFACTOR)
	RS_SETPARAM(D3DRS_SRGBWRITEENABLE)
	RS_SETPARAM(D3DRS_DEPTHBIAS)
	RS_SETPARAM(D3DRS_WRAP8)
	RS_SETPARAM(D3DRS_WRAP9)
	RS_SETPARAM(D3DRS_WRAP10)
	RS_SETPARAM(D3DRS_WRAP11)
	RS_SETPARAM(D3DRS_WRAP12)
	RS_SETPARAM(D3DRS_WRAP13)
	RS_SETPARAM(D3DRS_WRAP14)
	RS_SETPARAM(D3DRS_WRAP15)
	RS_SETPARAM(D3DRS_SEPARATEALPHABLENDENABLE)
	RS_SETPARAM(D3DRS_SRCBLENDALPHA)
	RS_SETPARAM(D3DRS_DESTBLENDALPHA)
	RS_SETPARAM(D3DRS_BLENDOPALPHA)
}

CStateManagementObj::RS_RESULT CStateManagementObj::RenderState::SetState(D3DRENDERSTATETYPE aState, DWORD aValue) {
	if (!m_pD3dDevice)
		return RS_FAILURE;

	RS_RESULT retRes(RS_SUCCESS);
	ASSERT((aState >= 0) && (aState < MAX_RSPARAMS));
	m_values[aState].value_ulong = aValue;
	m_values[aState].valueFlags |= RenderStateValue::RSVALUE_EVERSET;

	HRESULT err = m_pD3dDevice->SetRenderState(aState, aValue);
	if (FAILED(err)) {
		LogError("FAILED Setting " << aState);
		retRes = RS_FAILURE;
	}

	return retRes;
}

CStateManagementObj::RS_RESULT CStateManagementObj::RenderState::GetState(D3DRENDERSTATETYPE aState, DWORD* aValue) {
	*aValue = m_values[aState].value_ulong;
	RS_RESULT retRes = RS_SUCCESS;
	return retRes;
}

} // namespace KEP