///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "afxext.h"
#include "ViewportClass.h"
#include "IClientEngine.h"
#include "RuntimeCameras.h"
#include "CCameraClass.h"

#include "include\Core\Math\KEPMathUtil.h"
#include "common\KEPUtil\FastMath.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

CViewportObj::CViewportObj(const std::string& name, int x1, int y1, int x2, int y2) :
		m_name(name),
		m_id(0),
		m_pCO(nullptr),
		m_pCO_default(nullptr),
		m_activated(false),
		m_X1(x1),
		m_Y1(y1),
		m_X2(x2),
		m_Y2(y2),
		m_camWorldPosition(0.0f, 0.0f, 0.0f) {
	m_imViewPort.X = x1;
	m_imViewPort.Y = y1;
	m_imViewPort.Width = x2;
	m_imViewPort.Height = y2;
	m_imViewPort.MinZ = 0.0f;
	m_imViewPort.MaxZ = 1.0f;
	m_camWorldMatrix.MakeIdentity();
	m_viewMatrix.MakeIdentity();
	m_projMatrix.MakeIdentity();

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// Add Default Camera
	RuntimeCameras* pRC = pICE->GetRuntimeCameras();
	if (pRC)
		m_pCO_default = pRC->AddDefaultCamera();
}

CViewportObj::~CViewportObj() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// Remove Default Camera
	RuntimeCameras* pRC = pICE->GetRuntimeCameras();
	if (pRC)
		pRC->Del(m_pCO_default);
	m_pCO_default = nullptr;
}

float CViewportObj::GetCameraFovY() const {
	auto pCO = GetCamera();
	return pCO ? pCO->GetFovY() : 0.0;
}

void CViewportObj::DelCameraCallback(CCameraObj* pCO) {
	if (!pCO || !m_pCO_default)
		return;

	// Removing Active Camera ?
	if (pCO != m_pCO)
		return;

	// Copy Active Camera View To Default Camera Before Removal
	*m_pCO_default = *pCO;

	// Clear Local Reference (being deleted by RuntimeCameras)
	m_pCO = nullptr; // will be deleted
}

void CViewportObj::Activate(LPDIRECT3DDEVICE9 pD3dDevice, int aSurfaceWidth, int aSurfaceHeight) {
	if (!pD3dDevice)
		return;

	if (m_activated)
		return;
	m_activated = true;

	Vector2f pos(m_X1 * aSurfaceWidth * 0.01f, m_Y1 * aSurfaceHeight * 0.01f);
	SetPos(pos);
	float width = (m_X2 * aSurfaceWidth * 0.01) - pos.x;
	SetWidth(width);
	float height = (m_Y2 * aSurfaceHeight * 0.01) - pos.y;
	SetHeight(height);
	pD3dDevice->SetViewport(&m_imViewPort);
	float aspectRatio = GetAspectRatio();
	GenerateMatrices(aspectRatio);
}

void CViewportObj::GenerateMatrices(float aViewportAspectRatio) {
	CCameraObj* pCO = GetCamera();
	if (!pCO)
		return;

	// generate camera world transform from individual vectors
	m_camWorldMatrix.GetRowRef(0) = pCO->m_camWorldVectorX;
	m_camWorldMatrix.GetRowRef(1) = pCO->m_camWorldVectorY;
	m_camWorldMatrix.GetRowRef(2) = pCO->m_camWorldVectorZ;
	m_camWorldMatrix.GetRowRef(3) = pCO->m_camWorldPosition;
	m_camWorldMatrix.GetColumnRef(3) = Vector4f(0, 0, 0, 1);

	m_camWorldPosition = pCO->m_camWorldPosition;

	// generate View Matrix
	m_viewMatrix.MakeInverseOf(m_camWorldMatrix);

	// generate Projection Matrix
	auto pCustProjMatrix = pCO->GetCustomProjectionMatrix();
	if (pCustProjMatrix) {
		m_projMatrix = *pCustProjMatrix;
	} else {
		SetProjectionMatrix(
			m_projMatrix,
			pCO->GetFovY(),
			aViewportAspectRatio,
			pCO->m_nearPlaneDist,
			pCO->m_farPlaneDist,
			pCO->m_heightBaseAspect);
	}
}

// Sets the passed in 4x4 matrix to a perpsective projection
// matrix built from the vertical field-of-view (fov in the y axis)
// aspect ratio (width / height), distance to near plane
// and distance to far plane.
// https://msdn.microsoft.com/en-us/library/windows/desktop/bb147302(v=vs.85).aspx
HRESULT CViewportObj::SetProjectionMatrix(Matrix44f& mat, FLOAT fFOV, FLOAT fAspect, FLOAT fNearPlane, FLOAT fFarPlane, float heightBase) {
	// Make sure we have valid near/far distances.
	if (fNearPlane <= 0)
		fNearPlane = 1;
	if (fFarPlane <= fNearPlane)
		fFarPlane = fNearPlane * 1000.0f;

	FLOAT halfFov = 0.5f * fFOV;
	FLOAT sinFov = sinf(halfFov);
	FLOAT cosFov = cosf(halfFov);
	FLOAT ratio = cosFov / sinFov;
	FLOAT w = fAspect * ratio;
	FLOAT h = heightBase * ratio;
	FLOAT Q = fFarPlane / (fFarPlane - fNearPlane);

	mat.MakeZero();
	mat(0, 0) = w;
	mat(1, 1) = h;
	mat(2, 2) = Q;
	mat(3, 2) = -Q * fNearPlane;
	mat(2, 3) = 1.0f;

	return S_OK;
}

// draw outline (border) around viewport
//  thickness is in pixels
//  use D3DCOLOR_ARGB(255, 255, 255, 0) to pass in a color
void CViewportObj::DrawOutline(IDirect3DDevice9* pd3dDevice, int thickness, D3DCOLOR color) {
	auto pos = GetPos();
	int start_x = pos.x;
	int start_y = pos.y;
	int end_x = start_x + GetWidth();
	int end_y = start_y + GetHeight();

	struct CUSTOMVERTEX {
		float x, y, z, rhw;
		DWORD color;
	};

	CUSTOMVERTEX top_strip[] = {
		{ float(start_x), float(start_y + thickness), 0, 1, color },
		{ float(start_x), float(start_y), 0, 1, color },
		{ float(end_x), float(start_y + thickness), 0, 1, color },
		{ float(end_x), float(start_y), 0, 1, color }
	};

	CUSTOMVERTEX bottom_strip[] = {
		{ float(start_x), float(end_y), 0, 1, color },
		{ float(start_x), float(end_y - thickness), 0, 1, color },
		{ float(end_x), float(end_y), 0, 1, color },
		{ float(end_x), float(end_y - thickness), 0, 1, color }
	};

	CUSTOMVERTEX left_strip[] = {
		{ float(start_x), float(end_y - thickness), 0, 1, color },
		{ float(start_x), float(start_y + thickness), 0, 1, color },
		{ float(start_x + thickness), float(end_y - thickness), 0, 1, color },
		{ float(start_x + thickness), float(start_y + thickness), 0, 1, color }
	};

	CUSTOMVERTEX right_strip[] = {
		{ float(end_x - thickness), float(end_y - thickness), 0, 1, color },
		{ float(end_x - thickness), float(start_y + thickness), 0, 1, color },
		{ float(end_x), float(end_y - thickness), 0, 1, color },
		{ float(end_x), float(start_y + thickness), 0, 1, color }
	};

	pd3dDevice->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);
	pd3dDevice->SetRenderState(D3DRS_COLORVERTEX, TRUE);
	pd3dDevice->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
	pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

	pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, top_strip, sizeof(CUSTOMVERTEX));
	pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, bottom_strip, sizeof(CUSTOMVERTEX));
	pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, left_strip, sizeof(CUSTOMVERTEX));
	pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, right_strip, sizeof(CUSTOMVERTEX));
}

// go through all viewports and remove any instances of a camera
void CRuntimeViewportList::DelCameraCallback(CCameraObj* pCO) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pVO = (CViewportObj*)GetNext(posLoc);
		if (!pVO)
			continue;
		pVO->DelCameraCallback(pCO);
	}
}

int CRuntimeViewportList::AddViewport(CViewportObj* pVO) {
	if (!pVO)
		return 0;
	int id = GetUniqueId();
	pVO->SetID(id);
	if (IsEmpty())
		m_active_viewport_id = id;
	AddTail(pVO);
	return id;
}

CViewportObj* CRuntimeViewportList::GetViewportById(int id) const {
	if (id <= 0)
		return nullptr;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pVO = (CViewportObj*)GetNext(posLoc);
		if (pVO->GetID() == id)
			return pVO;
	}
	return nullptr;
}

CViewportObj* CRuntimeViewportList::GetActiveViewport() const {
	CViewportObj* pVO = nullptr;
	if (!IsEmpty() && m_active_viewport_id != 0)
		pVO = GetViewportById(m_active_viewport_id);
	return pVO;
}

bool CRuntimeViewportList::RemoveViewport(int id) {
	if (id <= 0)
		return false;

	POSITION delPos;
	for (POSITION posLoc = GetHeadPosition(); (delPos = posLoc) != NULL;) {
		auto pVO = (CViewportObj*)GetNext(posLoc);
		if (pVO->GetID() != id)
			continue;

		// removing active viewport?
		if (m_active_viewport_id == id) {
			// if removing the only viewport, none left to make active
			if (GetCount() == 1)
				m_active_viewport_id = 0;
			else {
				POSITION p = delPos;
				CViewportObj* pVO_find = 0;

				// if removing the last one, make the prev the active one
				if (delPos == GetTailPosition()) {
					POSITION prevPos = p;
					GetPrev(prevPos);
					pVO_find = (CViewportObj*)GetAt(prevPos);
				}
				// otherwise, the next one will become the active one
				else
					pVO_find = (CViewportObj*)GetAt(posLoc);

				if (pVO_find)
					m_active_viewport_id = pVO_find->GetID();
			}
		}
		delete pVO;
		pVO = nullptr;
		RemoveAt(delPos);
		return true;
	}

	return false;
}

// goal is to ensure each runtime viewport has a unique name to make identifying them easier
// the format of the new name will be: name_n, where n is an integer
// given the base_name (which will usually be the name of the template viewport used to instantiate
// this runtime viewport) a search will be made to find any other occurrences of this name as a prefix
// and return a new name with the suffix incremented by 1
std::string CRuntimeViewportList::GetNewViewportName(const std::string& base_name) {
	CStringA name = "";
	int highest_suffix = 0;
	int index = -1;
	CStringA cBaseName = base_name.c_str();
	cBaseName += "_";
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pVO = (CViewportObj*)GetNext(posLoc);
		CStringA cName = pVO->GetName().c_str();
		index = cName.Find(cBaseName);
		if (index > -1) {
			int suffix_index = cName.ReverseFind('_');
			CStringA suffix = cName.Mid(suffix_index + 1);
			int suffix_val = atoi(suffix);
			if (suffix_val > highest_suffix)
				highest_suffix = suffix_val;
		}
	}
	name.Format("%s%d", cBaseName, highest_suffix + 1);
	return name.GetString();
}

void CRuntimeViewportList::SelectViewportForInput(int x, int y) {
	if (GetCount() < 2)
		return;

	POINT pt = { x, y };

	// start at the last viewport (active / topmost )
	// and search for a viewport that contains the point
	POSITION prev;
	for (POSITION pos = GetTailPosition(); (prev = pos) != NULL;) {
		auto pVO = (CViewportObj*)GetPrev(pos);
		if (!pVO)
			continue;

		CRect rect;
		rect.left = (int)pVO->m_imViewPort.X;
		rect.top = (int)pVO->m_imViewPort.Y;
		rect.right = rect.left + (int)pVO->GetWidth();
		rect.bottom = rect.top + (int)pVO->GetHeight();
		if (rect.PtInRect(pt)) {
			m_active_viewport_id = pVO->GetID();
			return;
		}
	}
}

BEGIN_GETSET_IMPL(CViewportObj, IDS_VIEWPORTOBJ_NAME)
GETSET_RANGE(m_X1, gsInt, GS_FLAGS_DEFAULT, 0, 0, VIEWPORTOBJ, X1, INT_MIN, INT_MAX)
GETSET_RANGE(m_Y1, gsInt, GS_FLAGS_DEFAULT, 0, 0, VIEWPORTOBJ, Y1, INT_MIN, INT_MAX)
GETSET_RANGE(m_X2, gsInt, GS_FLAGS_DEFAULT, 0, 0, VIEWPORTOBJ, X2, INT_MIN, INT_MAX)
GETSET_RANGE(m_Y2, gsInt, GS_FLAGS_DEFAULT, 0, 0, VIEWPORTOBJ, Y2, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP