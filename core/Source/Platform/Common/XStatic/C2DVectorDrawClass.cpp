///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include "math.h"
#include <afxtempl.h>

#include "C2DVectorDrawClass.h"

namespace KEP {

CEX2DVectObj::CEX2DVectObj() {
	m_xyPointCount = 0;
	m_xyPointArray = NULL;
	m_centerPointX = 0.0f;
	m_centerPointY = 0.0f;
	m_radius = 0.0f;
}

void CEX2DVectObj::SafeDelete() {
	if (m_xyPointCount > 0)
		delete[] m_xyPointArray;
	m_xyPointArray = 0;
}

BOOL CEX2DVectObj::Clone(CEX2DVectObj** copy) {
	CEX2DVectObj* copyLocal = new CEX2DVectObj();

	*copy = copyLocal;
	return TRUE;
}

BOOL CEX2DVectObj::Scale(float size) {
	if (m_radius == 0 || size == 0 || m_xyPointCount == 0)
		return FALSE;

	float scaler = (size / m_radius);

	m_radius = m_radius * scaler;
	for (int loop = 0; loop < m_xyPointCount; loop++) {
		//loop through polygons
		m_xyPointArray[loop].startX *= scaler;
		m_xyPointArray[loop].startX *= scaler;
		m_xyPointArray[loop].startX *= scaler;
		m_xyPointArray[loop].startX *= scaler;
		m_xyPointArray[loop].endX *= scaler;
		m_xyPointArray[loop].endX *= scaler;
		m_xyPointArray[loop].endX *= scaler;
		m_xyPointArray[loop].endX *= scaler;

		m_xyPointArray[loop].startY *= scaler;
		m_xyPointArray[loop].startY *= scaler;
		m_xyPointArray[loop].startY *= scaler;
		m_xyPointArray[loop].startY *= scaler;
		m_xyPointArray[loop].endY *= scaler;
		m_xyPointArray[loop].endY *= scaler;
		m_xyPointArray[loop].endY *= scaler;
		m_xyPointArray[loop].endY *= scaler;
	} //end loop through polygons

	return TRUE;
}

BOOL CEX2DVectObj::GetCenterAndRadius() {
	if (m_xyPointCount == 0)
		return FALSE;
	float largestX, smallestX, largestY, smallestY, largestZ, smallestZ;

	int quantity = m_xyPointCount;
	largestX = m_xyPointArray[0].startX;
	smallestX = m_xyPointArray[0].startX;
	largestY = m_xyPointArray[0].startY;
	smallestY = m_xyPointArray[0].startY;
	largestZ = 0.0f;
	smallestZ = 0.0f;

	Vector3f frameOrigin;
	frameOrigin.x = 0.0f;
	frameOrigin.y = 0.0f;
	frameOrigin.z = 0.0f;

	for (int loop = 0; loop < quantity; loop++) {
		//loop through polygons
		if (largestX < m_xyPointArray[loop].startX)
			largestX = m_xyPointArray[loop].startX;
		if (smallestX > m_xyPointArray[loop].startX)
			smallestX = m_xyPointArray[loop].startX;
		if (largestX < m_xyPointArray[loop].endX)
			largestX = m_xyPointArray[loop].endX;
		if (smallestX > m_xyPointArray[loop].endX)
			smallestX = m_xyPointArray[loop].endX;

		if (largestY < m_xyPointArray[loop].startY)
			largestY = m_xyPointArray[loop].startY;
		if (smallestY > m_xyPointArray[loop].startY)
			smallestY = m_xyPointArray[loop].startY;
		if (largestX < m_xyPointArray[loop].endY)
			largestX = m_xyPointArray[loop].endY;
		if (smallestX > m_xyPointArray[loop].endY)
			smallestX = m_xyPointArray[loop].endY;
	} //end loop through polygons

	m_centerPointX = (largestX + smallestX) * .5f;
	m_centerPointY = (largestY + smallestY) * .5f;

	m_radius = MagnitudeSquared(m_centerPointX,
		m_centerPointY,
		0.0f,
		largestX,
		largestY,
		0.0f);
	/*//CENTER THEN RECALC
	for(loop=0;loop<m_xyPointCount;loop++)
	{//loop through polygons
	   m_xyPointArray[loop].startX-=m_centerPointX;
	   m_xyPointArray[loop].endX-=m_centerPointX;

	   m_xyPointArray[loop].startY-=m_centerPointY;
	   m_xyPointArray[loop].endY-=m_centerPointY;
	}//end loop through polygons
	for(loop=0;loop<quantity;loop++)
	{//loop through polygons
	   if(largestX < m_xyPointArray[loop].startX)
	      largestX = m_xyPointArray[loop].startX;
	   if(smallestX > m_xyPointArray[loop].startX)
		  smallestX = m_xyPointArray[loop].startX;
	   if(largestX < m_xyPointArray[loop].endX)
	      largestX = m_xyPointArray[loop].endX;
	   if(smallestX > m_xyPointArray[loop].endX)
		  smallestX = m_xyPointArray[loop].endX;

	   if(largestY < m_xyPointArray[loop].startY)
	      largestY = m_xyPointArray[loop].startY;
	   if(smallestY > m_xyPointArray[loop].startY)
		  smallestY = m_xyPointArray[loop].startY;
	   if(largestX < m_xyPointArray[loop].endY)
	      largestX = m_xyPointArray[loop].endY;
	   if(smallestX > m_xyPointArray[loop].endY)
		  smallestX = m_xyPointArray[loop].endY;
	}//end loop through polygons

	m_centerPointX = (largestX + smallestX)*.5f;
	m_centerPointY = (largestY + smallestY)*.5f;*/

	return TRUE;
}

void CEX2DVectObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_xyPointCount;
		for (int loop = 0; loop < m_xyPointCount; loop++) { //store vertex array
			ar << m_xyPointArray[loop].startX
			   << m_xyPointArray[loop].startY
			   << m_xyPointArray[loop].endX
			   << m_xyPointArray[loop].endY
			   << m_xyPointArray[loop].circV;
		} //end store vertex array

	} else {
		ar >> m_xyPointCount;
		if (m_xyPointCount > 0)
			m_xyPointArray = new LINEEXG[m_xyPointCount];
		for (int loop = 0; loop < m_xyPointCount; loop++) { //store vertex array
			ar >> m_xyPointArray[loop].startX >> m_xyPointArray[loop].startY >> m_xyPointArray[loop].endX >> m_xyPointArray[loop].endY >> m_xyPointArray[loop].circV;
		} //end store vertex array
	}
}
//-------------------Draw IT---------------------------//
BOOL CEX2DVectObj::DrawIt(HDC hdcLocal, float locX, float locY, float resizeX, float resizeY) {
	if (!m_xyPointArray)
		return FALSE;
	if (m_xyPointCount == 0)
		return FALSE;

	for (int loop = 0; loop < m_xyPointCount; loop++) {
		if (m_xyPointArray[loop].circV == 0.0f) { //if line
			MoveToEx(hdcLocal, // handle to device context
				((int)((m_xyPointArray[loop].startX * resizeX) + locX)), // x-coordinate of new current position
				((int)((m_xyPointArray[loop].startY * resizeY) + locY)), // y-coordinate of new current position
				NULL); // pointer to old current position);
			LineTo(hdcLocal,
				((int)((m_xyPointArray[loop].endX * resizeX) + locX)), // x-coordinate of line's ending point
				((int)((m_xyPointArray[loop].endY * resizeY) + locY))); // y-coordinate of line's ending point);
		} //end if line
		else if (m_xyPointArray[loop].circV != 0.0f) { //if circle
			float radius = m_xyPointArray[loop].circV * .5f;
			RECT buildRect;
			buildRect.left = ((int)((m_xyPointArray[loop].startX - radius) + locX));
			buildRect.right = ((int)((m_xyPointArray[loop].startX + radius) + locX));
			buildRect.top = ((int)((m_xyPointArray[loop].startY + radius) + locY));
			buildRect.bottom = ((int)((m_xyPointArray[loop].startY - radius) + locY));

			Arc(hdcLocal,
				buildRect.left, buildRect.top,
				buildRect.right, buildRect.bottom,
				buildRect.left, buildRect.top,
				buildRect.left, buildRect.top);

		} //end if circle
	}
	return TRUE;
}
//-------------------DXF File In-----------------------//
BOOL CEX2DVectObj::LoadDxfFile(CStringA filename) {
	std::wstring filenameW = Utf8ToUtf16(filename);
	CFile xfile(filenameW.c_str(), CFile::modeRead);
	int bufferSize = (int)xfile.GetLength();
	char* pbuf = new char[bufferSize];
	UINT nBytesRead = xfile.Read(pbuf, bufferSize);
	if (nBytesRead != bufferSize)
		return FALSE;
	for (int loopForSize = 0; loopForSize < 2; loopForSize++) {
		if (loopForSize == 1) //allocate ?
			m_xyPointArray = new LINEEXG[m_xyPointCount];
		int indexTraceCounter = 0;
		for (int loop = 0; loop < bufferSize; loop++) { //loop through buffer
			//GET Vector Data
			//AcDbLine = tag for line
			if (pbuf[loop] == 'A' &&
				pbuf[loop + 1] == 'c' &&
				pbuf[loop + 2] == 'D' &&
				pbuf[loop + 3] == 'b' &&
				pbuf[loop + 4] == 'L' &&
				pbuf[loop + 5] == 'i' &&
				pbuf[loop + 6] == 'n' &&
				pbuf[loop + 7] == 'e' &&
				pbuf[loop + 8] != 't') {
				//found vector data
				loop = loop + 7; //skip past garbage
				//ENTERDATA IF U KNOW SIZE
				if (loopForSize == 0)
					m_xyPointCount++;
				else { //Do IT
					while (pbuf[loop] == ' ' || pbuf[loop] == '\n' || pbuf[loop] == '{' || pbuf[loop] == '}') //get next data
						loop++;
					loop += 8;
					for (int indexer = 0; indexer < 5; indexer++) { //loop A
						CStringA tempBuild;
						while (pbuf[loop] != ' ' && pbuf[loop] != '\n') { //build float
							tempBuild = operator+(tempBuild, pbuf[loop]);
							loop++;
						} //end build float
						if (indexer != 2) { //skip Z
							if (indexer == 0)
								m_xyPointArray[indexTraceCounter].startX = (float)atof(tempBuild); //RECORD;
							if (indexer == 1)
								m_xyPointArray[indexTraceCounter].startY = (float)atof(tempBuild); //RECORD;
							if (indexer == 3)
								m_xyPointArray[indexTraceCounter].endX = (float)atof(tempBuild); //RECORD;
							if (indexer == 4)
								m_xyPointArray[indexTraceCounter].endY = (float)atof(tempBuild); //RECORD;
							m_xyPointArray[indexTraceCounter].circV = 0.0f;
						} //END skip Z
						while (pbuf[loop] == ' ' || pbuf[loop] == '\n') //get next data
							loop++;
						loop += 4;
					} //end loop A
					indexTraceCounter++;
				} //end do it
			} //found vector data
			//HANDLE CIRCLES
			//AcDbCircle = tag for line
			if (pbuf[loop] == 'A' &&
				pbuf[loop + 1] == 'c' &&
				pbuf[loop + 2] == 'D' &&
				pbuf[loop + 3] == 'b' &&
				pbuf[loop + 4] == 'C' &&
				pbuf[loop + 5] == 'i' &&
				pbuf[loop + 6] == 'r' &&
				pbuf[loop + 7] == 'c' &&
				pbuf[loop + 8] == 'l' &&
				pbuf[loop + 9] == 'e') {
				//found circle data
				if (loopForSize == 0)
					m_xyPointCount++;
				else { //Do IT 3
					while (pbuf[loop] == ' ' || pbuf[loop] == '\n' || pbuf[loop] == '{' || pbuf[loop] == '}') //get next data
						loop++;
					loop += 17;
					for (int indexer = 0; indexer < 4; indexer++) { //loop A
						CStringA tempBuild;
						while (pbuf[loop] != ' ' && pbuf[loop] != '\n') { //build float
							tempBuild = operator+(tempBuild, pbuf[loop]);
							loop++;
						} //end build float
						if (indexer == 0)
							m_xyPointArray[indexTraceCounter].startX = (float)atof(tempBuild); //RECORD;
						if (indexer == 1)
							m_xyPointArray[indexTraceCounter].startY = (float)atof(tempBuild); //RECORD;
						if (indexer == 3)
							m_xyPointArray[indexTraceCounter].circV = (float)(atof(tempBuild) * 2.0f); //RECORD radius;
						m_xyPointArray[indexTraceCounter].endX = 0.0f; //Init;
						m_xyPointArray[indexTraceCounter].endY = 0.0f; //Init;
						while (pbuf[loop] == ' ' || pbuf[loop] == '\n') //get next data
							loop++;
						loop += 4;
					} //end loop A
					indexTraceCounter++;
				} //end Do It 2
			} //end found circle data
			//RECTANGLE
			//AcDbPolyline = filled areas for line
			/*	if(pbuf[loop] == 'A' &&
				   pbuf[loop+1] == 'c' &&
				   pbuf[loop+2] == 'D' &&
				   pbuf[loop+3] == 'b' &&
				   pbuf[loop+4] == 'P' &&
				   pbuf[loop+5] == 'o' &&
				   pbuf[loop+6] == 'l' &&
				   pbuf[loop+7] == 'y' &&
				   pbuf[loop+8] == 'l' &&
				   pbuf[loop+9] == 'i' &&
				   pbuf[loop+10] == 'n' &&
				   pbuf[loop+11] == 'e' &&
				   )
				{//found pline data
			    if(loopForSize == 0)
			    m_xyPointCount++;
				else{//Do IT 3
			    while(pbuf[loop] == ' ' || pbuf[loop] == '\n' || pbuf[loop] == '{' || pbuf[loop] == '}')//get next data
				   loop++;
				   loop+=19;
				 indexTraceCounter++;
				}//end Do It 2
				}//end found pline data*/
			//END ENTERDATA IF U KNOW SIZE
		} //end loop through buffer
	}
	xfile.Close();
	delete[] pbuf;
	pbuf = 0;

	GetCenterAndRadius();

	return TRUE;
}
//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//--this functiongets the distance between 2 3dpoints----------//
float CEX2DVectObj::MagnitudeSquared(float Mx1, float My1,
	float Mz1, float Mx2,
	float My2, float Mz2) {
	float temp_Mxyz = (((Mx1 - Mx2) * (Mx1 - Mx2)) + ((My1 - My2) * (My1 - My2)) + ((Mz1 - Mz2) * (Mz1 - Mz2)));

	return (float)sqrt(temp_Mxyz);
}
IMPLEMENT_SERIAL(CEX2DVectObj, CObject, 0)
IMPLEMENT_SERIAL(CEX2DVectObjList, CObList, 0)

BEGIN_GETSET_IMPL(CEX2DVectObj, IDS_EX2DVECTOBJ_NAME)
GETSET_RANGE(m_xyPointCount, gsWord, GS_FLAGS_DEFAULT, 0, 0, EX2DVECTOBJ, XYPOINTCOUNT, WORD_MIN, WORD_MAX)
END_GETSET_IMPL

} // namespace KEP