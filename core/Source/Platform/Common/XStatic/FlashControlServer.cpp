///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FlashControlServer.h"
#include "FlashClientServerAPI.h"
#include "SetThreadName.h"
#include "LogHelper.h"
#include "Core/Math/KEPMathUtil.h"
#include "FlashScriptAccessMode.h"
#include "Common/include/config.h"
#include "KEPHelpers.h"

#include <memory>
#include <mutex>
#include <map>
#include <regex>

#include "f_in_box/f_in_box.h"

#ifdef _DEBUG
#define F_IN_BOX_LIB_NAME "f_in_boxd.lib"
#else
#define F_IN_BOX_LIB_NAME "f_in_box.lib"
#endif

#pragma comment(lib, F_IN_BOX_LIB_NAME)

static LogInstance("Instance");
static const int kDefaultFlashThreadPriority = THREAD_PRIORITY_BELOW_NORMAL;

namespace KEP {
namespace FlashControl {

bool IsFlashInstalled() {
	static bool s_bIsFlashInstalled = FPCIsFlashInstalled() != FALSE;
	return s_bIsFlashInstalled;
}

std::string GetFlashVersion() {
	auto BuildFlashVersionString = []() -> std::string {
		SFPCVersion version;
		if (IsFlashInstalled() && GetInstalledFlashVersionEx(&version)) {
			std::stringstream strm;
			strm << "Flash " << version.v[3] << "." << version.v[2] << "." << version.v[1] << "." << version.v[0];
			return strm.str();
		} else {
			return "Flash Not Installed";
		}
	};
	//static std::string s_strVersion = BuildFlashVersionString();
	return BuildFlashVersionString();
}

// Interfaces with f_in_box to directly manage a Flash control.
class FlashControlServer : public IFlashControlServer, public std::enable_shared_from_this<FlashControlServer> {
public:
	FlashControlServer();
	~FlashControlServer();
	virtual bool StartServer(uintptr_t iClientId, uint32_t iMeshId, HANDLE hSharedMemory, HANDLE hRequestAvailableEvent, bool bStartInThread = false) override final;
	virtual bool StopProcessing() override final;
	virtual bool ProcessNextRequest() override final;
	virtual void SetGlobalVolume(float fGlobalVolume) override final;
	virtual std::string GetIdString() override final;
	virtual bool OwnsWindow(HWND hWnd, bool* pbTopControlWindow) override final;
	virtual void HandleFlashCall(SFPCFlashCallInfoStruct* pInfo) override;

private:
	bool SetMediaParams(const std::string& strParams, const std::string& strURL, FlashScriptAccessMode allowScriptAccess);
	bool PutAllowScriptAccess(const std::string& accessStr);
	bool PutFlashVars(const std::string& swfParams);
	bool PutMovie(const std::string& swfUrl);
	bool Play();
	bool Stop();
	bool Rewind();
	bool UpdateRect(uintptr_t /*HANDLE*/ hSharedMemory, int32_t x, int32_t y, uint32_t width, uint32_t height, uint32_t iCropTop, uint32_t iCropLeft, uint32_t iCropBottom, uint32_t iCropRight);
	bool UpdateBitmap(uint32_t iRequestId) {
		return false;
	}
	bool UpdateVolume();
	bool SetFocus();

	bool GenerateBitmap(uint32_t idxBitmap);

	void LoadYouTubeVideoById(const std::string& sYouTubeVideoId);
	std::string FilterFlashURL(const std::string& strURL);
	std::string FilterFlashParams(const std::string& strParams);
	void ResetYouTubeState() { m_sYouTubeVideoId.clear(); }

private:
	bool InitFlashPlayerControl();
	static HFPC GetOrCreateNewFlashPlayerHandle();

	bool SendMessageToFlashControl(UINT msg, WPARAM wParam, LPARAM lParam);
	template <typename T>
	bool SendFlashControlMessage(UINT msg, T* pInfo);

	static DWORD WINAPI StaticFlashThreadFunc(LPVOID lpParameter); // Invokes FlashThreadFunc().
	int FlashThreadFunc();

	uint32_t GetMeshId() const {
		return m_iMeshId;
	}
	std::string GetIdString(bool bVerbose) const {
		return BuildFlashMeshIdString("", GetMeshId(), bVerbose);
	}

	bool RecreateBitmaps();
	bool CloseBitmaps();
	bool CloseBitmapSharedMemory();
	bool CloseStateSharedMemory();

	bool IsVisibleFlashControl() const {
		return m_CurrentState.m_CreateSettings.GetValue().m_bWantInput;
	}
	void SetQuit() {
		m_CurrentState.m_bQuit = true;
	}
	bool ShouldQuit() const {
		return m_CurrentState.m_bQuit;
	}

private: // data members
	bool m_bReadyForCommunications = false; // Shared memory initialized.
	bool m_bInitializedFlashPlayerControl = false; // Initialized f_in_box flash player control.
	bool m_bInitialized = false; // Initialized connection to client.

	uint32_t m_iMeshId = 0;
	uintptr_t m_iClientId = 0;
	HFPC m_hFPC; // Flash player control handle. Never destroyed. Put back in cache for reuse when this object is destroyed.
	HWND m_hwndFPC = NULL; // handle to flash player window
	std::string m_sYouTubeVideoId; // If set, we are currently playing a YouTube video (some special handling required)

	ThreadHandleWrapper m_hFPCThread; // flash player thread handle

	FlashStateData m_CurrentState; // Accumulation of client requests.
	float m_fGlobalVolume = 1;

	std::shared_ptr<FlashControlServer> m_pThisKeepAliveInThread; // Pointer to this object to keep it alive while in thread.

	SharedMemory m_SharedMemory;
	MappedFileView m_SharedMemoryView;
	FlashSharedMemoryBlock* m_pSharedMemory = nullptr;
	SharedMemory m_BitmapSharedMemory;
	WindowsBitmapWrapper m_ahBitmaps[3];
	void* m_apBitmapData[3]; // Direct access to bitmap data.
	TripleBufferWriterStateMgr m_ServerStateMgr;
	TripleBufferReaderStateMgr m_ClientStateMgr;

	EventHandleWrapper m_RequestAvailableEvent;
	GeneratedBitmapStateData m_GeneratedBitmapState;

	// All HFPC objects created and whether they are currently being used.
	static std::mutex s_hfpcMutex;
	static std::map<HFPC, bool> s_hfpcMap;
};

std::mutex FlashControlServer::s_hfpcMutex;
std::map<HFPC, bool> FlashControlServer::s_hfpcMap;
HWND IFlashControlServer::s_hwndOwner = NULL;

std::shared_ptr<IFlashControlServer> IFlashControlServer::Create() {
	return std::make_shared<FlashControlServer>();
}

FlashControlServer::FlashControlServer() :
		m_hFPC(NULL), m_hwndFPC(0) {
	assert(GetOwnerWindow() != NULL); // Factory must set it before constructing first FlashControlServer
}

FlashControlServer::~FlashControlServer() {
	// Let other FlashMeshes reuse the handle.
	if (m_hFPC) {
		std::lock_guard<std::mutex> lock(s_hfpcMutex);
		s_hfpcMap[m_hFPC] = false;
	}

	CloseBitmapSharedMemory();
	CloseStateSharedMemory();
}

bool FlashControlServer::StartServer(uintptr_t iClientId, uint32_t iMeshId, HANDLE hSharedMemory, HANDLE hRequestAvailableEvent, bool bStartInThread) {
	m_iClientId = iClientId;
	m_iMeshId = iMeshId;
	m_SharedMemory.Set(hSharedMemory);
	m_SharedMemoryView = m_SharedMemory.Map(0, sizeof(FlashSharedMemoryBlock), true);
	if (m_SharedMemoryView) {
		m_pSharedMemory = static_cast<FlashSharedMemoryBlock*>(m_SharedMemoryView.GetPointer());
		m_ServerStateMgr.SetSynchronizer(&m_pSharedMemory->m_ServerAcknowledgmentSynchronizer);
		m_ClientStateMgr.SetSynchronizer(&m_pSharedMemory->m_ClientRequestSynchronizer);
	} else {
		LogError("Could not map shared memory.");
		return false;
	}

	m_RequestAvailableEvent.Set(hRequestAvailableEvent);

	// Flash Installed?
	if (!IsFlashInstalled()) {
		LogError(GetIdString() << " FlashIsInstalled() FAILED");
		return false;
	}

	if (bStartInThread) {
		LogWarn("Threaded FlashControlServer is untested since refactoring"); // Because I don't think we'll be using it anymore.

		// Thread Flash Player (f_in_box texture scraped from wnd created by this thread)
		m_pThisKeepAliveInThread = this->shared_from_this(); // Don't allow deletion of FlashControlServer while thread is executing.
		m_hFPCThread.Set(::CreateThread(NULL, 0, StaticFlashThreadFunc, this, 0, nullptr));
		if (!m_hFPCThread) {
			m_pThisKeepAliveInThread.reset(); // Thread never started. Cancel keep-alive since thread can't do it.
			LogError(GetIdString() << " CreateThread(FlashThreadFunc) FAILED");
			return false;
		}

		// Set Thread Priority
		if (!::SetThreadPriority(m_hFPCThread.GetHandle(), kDefaultFlashThreadPriority))
			LogError(GetIdString() << " SetThreadPriority(FlashThreadFunc) FAILED");
	}
	m_bReadyForCommunications = true;
	return true;
}

bool FlashControlServer::CloseStateSharedMemory() {
	m_SharedMemory.Close();
	m_SharedMemoryView.Close();
	m_pSharedMemory = nullptr;
	return true;
}

bool FlashControlServer::CloseBitmaps() {
	for (WindowsBitmapWrapper& h : m_ahBitmaps)
		h.Close();
	return true;
}

bool FlashControlServer::CloseBitmapSharedMemory() {
	CloseBitmaps();
	m_BitmapSharedMemory.Close();
	//m_pBitmapSharedMemory = nullptr;
	return true;
}

bool FlashControlServer::RecreateBitmaps() {
	if (!m_BitmapSharedMemory)
		return false;

	const FlashStateData::RectSettings& rectSettings = m_CurrentState.m_RectSettings.GetValue();
	return CreateBitmapsInSharedMemory(m_BitmapSharedMemory.GetHandle(), m_ahBitmaps, m_apBitmapData, 0, rectSettings.GetBitmapWidth(), rectSettings.GetBitmapHeight());
}

bool FlashControlServer::UpdateVolume() {
	// Valid Flash Player ?
	if (!m_hFPC) {
		LogError(GetIdString() << " Invalid flash player handle");
		return false;
	}

	// Set Flash Volume
	const DWORD kMaxVolume = 0x0000fffe; // DEF_MAX_FLASH_AUDIO_VOLUME IS WRONG!
	double dVolume = Clamp(m_fGlobalVolume * m_CurrentState.m_LocalVolume.GetValue(), 0, 1);
	DWORD volSet = kMaxVolume * dVolume;
	bool ok = (FPC_SetSoundVolume(m_hFPC, volSet) == NO_ERROR);
	if (!ok) {
		LogError(GetIdString() << " FPC_SetSoundVolume() FAILED - volSet=" << volSet);
		return false;
	}
	LogInfo(GetIdString() << "Set volume fraction to " << dVolume);

	return true;
}

bool FlashControlServer::UpdateRect(uintptr_t /*HANDLE*/ hSharedMemory, int32_t x, int32_t y, uint32_t width, uint32_t height, uint32_t iCropTop, uint32_t iCropLeft, uint32_t iCropBottom, uint32_t iCropRight) {
	CloseBitmapSharedMemory();

	m_BitmapSharedMemory.Set(reinterpret_cast<HANDLE>(hSharedMemory));

	FlashStateData::RectSettings& rectSettings = m_CurrentState.m_RectSettings.GetRefToUpdate();
	rectSettings.m_iWidth = width;
	rectSettings.m_iHeight = height;
	rectSettings.m_iPositionX = x;
	rectSettings.m_iPositionY = y;

	rectSettings.m_iCropTop = iCropTop;
	rectSettings.m_iCropLeft = iCropLeft;
	rectSettings.m_iCropBottom = iCropBottom;
	rectSettings.m_iCropRight = iCropRight;

	return (SetWindowPos(m_hwndFPC, 0, x, y, width, height, SWP_NOZORDER) == TRUE);
}

// ED-8256, ED-8362
static void WINAPI ADBlockURLPreprocessor(HFPC hFPC, LPARAM lParam, LPWSTR* pszURL, BOOL* pbContinue) {
	const static std::vector<std::wstring> AD_URL_PATTERNS = { L"doubleclick.net", L"javascript:" }; // Filtering any URL containing any of these tokens
	static LPWSTR AD_URL_SUBSTITUTE = L"file:///null"; // If matched, replace the URL with this

	*pbContinue = TRUE;
	if (pszURL && *pszURL) {
		std::wstring wsURL(*pszURL);
		for (const std::wstring& pat : AD_URL_PATTERNS) {
			if (wsURL.length() >= pat.length() && wsURL.find(pat) != std::wstring::npos) { // case sensitive
				*pszURL = AD_URL_SUBSTITUTE;
				return;
			}
		}
	}
}

// Creates a new handle if the cache is empty.
HFPC FlashControlServer::GetOrCreateNewFlashPlayerHandle() {
	HFPC hFPC = NULL;

	// Flash Player Installed ?
	if (!IsFlashInstalled()) {
		LogError("Flash not installed");
		return NULL;
	}

	// Try to reuse a discarded handle.
	std::lock_guard<std::mutex> lock(s_hfpcMutex);
	for (auto& it : s_hfpcMap) {
		if (!it.second) {
			it.second = true;
			hFPC = it.first;
			return hFPC;
		}
	}

	// Create a new handle.
	hFPC = FPC_LoadRegisteredOCX();
	if (!hFPC) {
		LogError("FPC_LoadRegisteredOCX() FAILED");
		return NULL;
	}

	// Set AD-blocking URL filter
	FPC_SetPreProcessURLHandler(hFPC, ADBlockURLPreprocessor, 0);

	// Flag Flash Player as being used.
	s_hfpcMap[hFPC] = true;
	LogInfo("flashPlayerHandles=" << s_hfpcMap.size());

	return hFPC;
}

DWORD WINAPI FlashControlServer::StaticFlashThreadFunc(LPVOID lpParameter) {
	SetThreadName("FlashServer");
	return static_cast<FlashControlServer*>(lpParameter)->FlashThreadFunc();
}

// Unused. FlashControlFactory currently manages the processing loop.
int FlashControlServer::FlashThreadFunc() {
	LogInstance("FlashMesh");

	// Log Thread Start
	LogInfo(GetIdString() << " START");

	// Initialize windows message queue.
	{
		MSG msg;
		PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOYIELD | PM_NOREMOVE);
	}

	// Process Message Loop Until Flash Player Window Closes
	MSG msg;
	HANDLE ahWaitHandles[] = {
		m_RequestAvailableEvent.GetHandle()
	};

	while (!ShouldQuit()) {
		DWORD iEventIndex = MsgWaitForMultipleObjects(std::extent<decltype(ahWaitHandles)>::value, ahWaitHandles, FALSE, INFINITE, QS_ALLINPUT);
		(void)iEventIndex;
		//ResetEvent(m_RequestAvailableEvent.GetHandle());

		bool bCheckForMoreMessages = true;
		while (bCheckForMoreMessages) {
			bool bHadMessage = false;

			if (ProcessNextRequest())
				bHadMessage = true;

			if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE | PM_NOYIELD) != 0) {
				bHadMessage = true;

				if (msg.message == WM_CLOSE || msg.message == WM_QUIT) {
					SetQuit();
				} else {
					if (msg.message == WM_SETFOCUS) {
						// The client is unexpectedly losing focus in some cases. Added this log
						// message to help determine whether the focus is going to a Flash controlled window.
						LogInfo("SetFocus windows message received in Flash server.");
					}
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			} else {
				bCheckForMoreMessages = false;
			}

			if (!bHadMessage) {
				// Possible, but unlikely. Message could have been added between the wait and the processing, so that the event does
				// not get cleared even though all messages are processed.
			}
		}
	}

	// Make sure window is destroyed.
	if (m_hwndFPC) {
		::DestroyWindow(m_hwndFPC);
		m_hwndFPC = NULL;
	}

	// Log Thread End
	LogInfo(GetIdString() << " END");
	m_pThisKeepAliveInThread.reset(); // May cause deletion of FlashMesh, which will delete this.

	return 0;
}

bool FlashControlServer::InitFlashPlayerControl() {
	if (m_bInitializedFlashPlayerControl) {
		LogError("Can only be initialized once.");
		return false;
	}
	m_bInitializedFlashPlayerControl = true;

	// Get handle to flash player control.
	m_hFPC = GetOrCreateNewFlashPlayerHandle();
	if (!m_hFPC)
		return false;

	// Create F-In-Box Flash Player Window
	// IMPORTANT NOTE ON WINDOW STYLES
	// If WS_CHILD, you need a parent, and Flash will do SendMessage on notifications
	// so you *must* run PeekMessage loop when blocking on shutdown
	// Now we use no parent and WS_POPUP, which works fine.
	m_hwndFPC = 0;
	DWORD style = 0;
	DWORD exStyle = 0;
	HWND hwndParent = 0;
	if (IsVisibleFlashControl()) {
		// SetCursor does not work if FPCS_TRANSPARENT is set.
		style = WS_CHILD | WS_VISIBLE;

		// Parent required for visible flash players.
		hwndParent = reinterpret_cast<HWND>(m_CurrentState.m_CreateSettings.GetValue().m_iParentWindowHandle);
	} else {
		// adding visible helps getting mouse/keyboard
		style = WS_POPUP;

		// The flags FPCS_TRANSPARENT and WS_EX_LAYERED cause f_in_box to do extra processing of the bitmap to
		// determine which pixels are transparent.
		// Disable transparency to improve performance.
		// If some flash objects need transparency, we can enable it on demand.
		static const bool bTransparent = false;
		if (bTransparent) {
			style |= FPCS_TRANSPARENT;
			exStyle |= WS_EX_LAYERED;
		}

		// Owner HWND for processing WM_NOTIFY messages
		hwndParent = GetOwnerWindow();
	}

	try {
		const FlashStateData::RectSettings& rectSettings = m_CurrentState.m_RectSettings.GetValue();
		m_hwndFPC = FPC_CreateWindow(m_hFPC,
			exStyle, NULL, style,
			(int)rectSettings.m_iPositionX,
			(int)rectSettings.m_iPositionY,
			(int)rectSettings.m_iWidth,
			(int)rectSettings.m_iHeight,
			hwndParent, NULL, NULL, NULL);
	} catch (...) {
		LogError(GetIdString() << " Caught Exception - FPC_CreateWindow()");
	}
	if (!m_hwndFPC || !IsWindow(m_hwndFPC)) {
		LogError(GetIdString() << " FPC_CreateWindow() FAILED - '" << GetLastError() << "'");
		return false;
	}

	// Unlock F-In-Box Full Version (not demo version)
	FPC_SetContext(m_hwndFPC, kpsz_f_in_box_key);

	// ReStyle Window
	LONG gwlStyle = GetWindowLong(m_hwndFPC, GWL_STYLE);
	::SetWindowLong(m_hwndFPC, GWL_STYLE, gwlStyle & ~(WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX));

	LogInfo(GetIdString() << " OK - " << (IsVisibleFlashControl() ? " PARENTED" : " HIDDEN"));

	return true;
}

bool FlashControlServer::GenerateBitmap(uint32_t idxBitmap) {
	if (IsVisibleFlashControl())
		return false; // Caller should not be requesting scrapes of visible windows.

	uint32_t iWidth = m_CurrentState.m_RectSettings.GetValue().GetBitmapWidth();
	uint32_t iHeight = m_CurrentState.m_RectSettings.GetValue().GetBitmapHeight();

	// Get Frame Bitmap From Flash Player
	SFPCGetFrameBitmap FPCGetFrameBitmap = { 0 };
	::SendMessage(m_hwndFPC, FPCM_GET_FRAME_BITMAP, 0, (LPARAM)&FPCGetFrameBitmap);
	GDIHandleWrapper<HBITMAP> hBitmap(FPCGetFrameBitmap.hBitmap);
	if (!hBitmap)
		return false;

	BitmapDC bitmapDCSource(hBitmap.GetHandle());
	BitmapDC bitmapDCDestination(m_ahBitmaps[idxBitmap].GetHandle());
	uint32_t iCopyWidth = iWidth;
	uint32_t iCopyHeight = iHeight;
	int32_t iSrcOffsetX = m_CurrentState.m_RectSettings.GetValue().m_iCropLeft;
	int32_t iSrcOffsetY = m_CurrentState.m_RectSettings.GetValue().m_iCropTop;
	int32_t iDestOffsetX = 0;
	int32_t iDestOffsetY = 0;

#if 1
	bool bCopySucceeded = (TRUE == ::StretchBlt(bitmapDCDestination.GetDC(), iDestOffsetX, iDestOffsetY, iCopyWidth, iCopyHeight, bitmapDCSource.GetDC(), iSrcOffsetX, iCopyHeight + iSrcOffsetY, iCopyWidth, (-1) * iCopyHeight, SRCCOPY));
#else
	(void)iDestOffsetX;
	(void)iDestOffsetY;
	// Solid color (blue) fill for testing.
	COLORREF clr(0xff0000);
	::SetBkColor(bitmapDCDestination.GetDC(), clr);
	CRect rect(-1000, -1000, 1000, 1000);
	::ExtTextOut(bitmapDCDestination.GetDC(), 0, 0, ETO_OPAQUE, &rect, NULL, 0, NULL);
	bool bCopySucceeded = (TRUE == ::StretchBlt(bitmapDCDestination.GetDC(), 64, 64, 64, 64, bitmapDCSource.GetDC(), iSrcOffsetX, iSrcOffsetY, iCopyWidth, iCopyHeight, SRCCOPY));
#endif
	if (!bCopySucceeded)
		return false;

	return true;
}

bool FlashControlServer::SetFocus() {
	SendMessage(m_hwndFPC, WM_SETFOCUS, 0, 0);
	return true;
}

bool FlashControlServer::StopProcessing() {
	SetQuit();
	m_hFPCThread.Close();
	if (m_hwndFPC) {
		::DestroyWindow(m_hwndFPC);
		m_hwndFPC = NULL;
	}
	return true;
}

bool FlashControlServer::SendMessageToFlashControl(UINT msg, WPARAM wParam, LPARAM lParam) {
	if (!m_hwndFPC)
		return false;

	::SendMessageW(m_hwndFPC, msg, wParam, lParam);
	return true;
}

template <typename T>
bool FlashControlServer::SendFlashControlMessage(UINT msg, T* pInfo) {
	if (!SendMessageToFlashControl(msg, 0, (LPARAM)pInfo))
		return false;
	if (FAILED(pInfo->hr)) {
		LogError(GetIdString() << " FAILED - hr=" << pInfo->hr);
		return false;
	}
	return true;
}

bool FlashControlServer::PutAllowScriptAccess(const std::string& allowStr) {
	SFPCPutAllowScriptAccessW info;
	std::wstring allowStrW = Utf8ToUtf16(allowStr);
	info.lpszBuffer = allowStrW.c_str();

	SendFlashControlMessage(FPCM_PUT_ALLOWSCRIPTACCESS, &info);

	return true;
}

bool FlashControlServer::PutFlashVars(const std::string& strFlashVars) {
	SFPCPutFlashVarsW info;
	std::wstring swfParamsW = Utf8ToUtf16(strFlashVars);
	info.lpszBuffer = swfParamsW.c_str();
	SendFlashControlMessage(FPCM_PUT_FLASHVARS, &info);

	return true;
}

bool FlashControlServer::PutMovie(const std::string& strUrl) {
	SFPCPutMovieW info;
	std::wstring strUrlW = Utf8ToUtf16(strUrl);
	info.lpszBuffer = strUrlW.c_str();
	SendFlashControlMessage(FPCM_PUT_MOVIE, &info);
	return true;
}

// TODO - Doesn't Work
bool FlashControlServer::Play() {
	SFPCPlay info;
	SendFlashControlMessage(FPCM_PLAY, &info);

	return true;
}

// TODO - Doesn't Work
bool FlashControlServer::Stop() {
	ResetYouTubeState();
#if DOESNT_WORK
	SFPCStopPlay info;
	SendFlashControlMessage(FPCM_STOP, &info);

	return true;
#else
	return PutMovie("KanevaIdle.swf");
#endif
}

// TODO - Doesn't Work
bool FlashControlServer::Rewind() {
#if 1
	return false;
#else
	SFPCPlay playInfo;
	SFPCStopPlay stopInfo;
	SFPCRewind rewindInfo;
	SendFlashControlMessage(FPCM_STOP, &stopInfo);
	SendFlashControlMessage(FPCM_REWIND, &rewindInfo);
	SendFlashControlMessage(FPCM_PLAY, &playInfo);

	return true;
#endif
}

bool FlashControlServer::SetMediaParams(const std::string& strParams, const std::string& strURL, FlashScriptAccessMode eAllowScriptAccess) {
	// Command Flash Player With New Media Params
	LogInfo(GetIdString() << "Starting URL \"" << strURL << "\" with params \"" << strParams << "\"");
	Stop();
	std::string sURL = FilterFlashURL(strURL);
	std::string sFlashVars = FilterFlashParams(strParams);
	PutFlashVars(sFlashVars);
	PutMovie(sURL);

	std::string strAllowScriptAccess;

	switch (eAllowScriptAccess) {
		case FlashScriptAccessMode::Auto:
			// Legacy: remove after 12/31/2016
			if (m_sYouTubeVideoId.empty()) {
				// Non-YouTube: disable script access to prevent popups from flash
				strAllowScriptAccess = "never";
			} else {
				// Now must enable script access to allow YouTube AS3 player to callback into our code
				strAllowScriptAccess = "sameDomain";
			}
			break;
		case FlashScriptAccessMode::Always:
			strAllowScriptAccess = "always"; // Use "always" only if the SWF is trusted and it won't work without "always" option
			assert(!m_sYouTubeVideoId.empty()); // Bug detection: to be removed
			break;
		case FlashScriptAccessMode::Domain:
			strAllowScriptAccess = "sameDomain"; // Use "sameDomain" if the SWF is trusted and requires script access
			break;
		case FlashScriptAccessMode::Never:
			strAllowScriptAccess = "never"; // Use "never" for everything else
			assert(m_sYouTubeVideoId.empty()); // Bug detection: to be removed
			break;
		default:
			assert(false);
			strAllowScriptAccess = "never";
			break;
	}

	LogDebug("AllowScriptAccess=" << strAllowScriptAccess);
	PutAllowScriptAccess(strAllowScriptAccess);
	return true;
}

void FlashControlServer::SetGlobalVolume(float fGlobalVolume) {
	m_fGlobalVolume = fGlobalVolume;
	UpdateVolume();
}

std::string FlashControlServer::GetIdString() {
	return GetIdString(false);
}

bool FlashControlServer::OwnsWindow(HWND hWndArg, bool* pbTopControlWindow) {
	if (!m_hwndFPC)
		return false; // This server does not own any windows.

	HWND hWnd = hWndArg;
	while (hWnd) {
		if (hWnd == m_hwndFPC) {
			if (pbTopControlWindow)
				*pbTopControlWindow = (hWnd == hWndArg);
			return true;
		}
		hWnd = ::GetParent(hWnd);
	}

	return false;
}

namespace {
struct MemberChangedTester {
	// Returns true if any variables are unequal.
	bool operator()() const {
		return false;
	}

	template <typename T, typename... Args>
	bool operator()(T FlashStateData::*pMember, Args... remainingArgs) const {
		// bitwise OR to avoid short-circuiting the update of remaining arguments.
		return (m_pCurrentState->*pMember != m_pRequestedState->*pMember) | operator()(remainingArgs...);
	}

	FlashStateData* m_pCurrentState;
	const FlashStateData* m_pRequestedState;
};
} // namespace

bool FlashControlServer::ProcessNextRequest() {
	if (!m_bReadyForCommunications)
		return false;

	if (!m_ClientStateMgr.ClaimBuffer())
		return false; // Nothing to process.

	uint32_t idxRequest = m_ClientStateMgr.GetBufferIndex();
	FlashSharedMemoryBlock::ClientRequest* pRequest = &m_pSharedMemory->m_aClientRequests[idxRequest];
	const FlashStateData* pRequestedState = &pRequest->m_FlashState;
	MemberChangedTester changeTester{ &m_CurrentState, pRequestedState };
	const FlashStateData::RectSettings& oldRectSettings = m_CurrentState.m_RectSettings.GetValue();
	const FlashStateData::RectSettings& newRectSettings = pRequestedState->m_RectSettings.GetValue();

	// Determine what changed.
	bool bInitialize = !m_bInitialized && pRequestedState->m_CreateSettings.IsNewerThan(m_CurrentState.m_CreateSettings);
	bool bRewind = changeTester(&FlashStateData::m_iRewindRequestId);
	bool bSetFocusChanged = changeTester(&FlashStateData::m_iSetFocusId);
	bool bVolumeChanged = pRequestedState->m_LocalVolume.IsNewerThan(m_CurrentState.m_LocalVolume);
	bool bBitmapMemoryChanged = changeTester(&FlashStateData::m_iBitmapSharedMemoryId);
	bool bNewBitmapRect = pRequestedState->m_RectSettings.IsNewerThan(m_CurrentState.m_RectSettings);
	bool bBitmapSizeChanged = bNewBitmapRect &&
							  (oldRectSettings.GetBitmapWidth() != newRectSettings.GetBitmapWidth() || oldRectSettings.GetBitmapHeight() != newRectSettings.GetBitmapHeight());
	bool bWindowPositionChanged = bNewBitmapRect &&
								  (oldRectSettings.m_iPositionX != newRectSettings.m_iPositionX || oldRectSettings.m_iPositionY != newRectSettings.m_iPositionY);
	bool bGenerateBitmap = changeTester(&FlashStateData::m_iGenerateBitmapId);
	bool bMediaParamsChanged = pRequestedState->m_MediaSettings.IsNewerThan(m_CurrentState.m_MediaSettings);

	// Don't accept any updates until we're initialized.
	if (!m_bInitialized && !bInitialize)
		return false;

	// Update our current state so we can operate on it.
	m_CurrentState.UpdateFrom(*pRequestedState);

	if (bInitialize) {
		if (!InitFlashPlayerControl()) {
			SetQuit();
			return true;
		}
		m_bInitialized = true;
	}

	if (bBitmapSizeChanged || bBitmapMemoryChanged) {
		CloseBitmaps();
		if (bBitmapMemoryChanged) {
			// We got a new bitmap memory handle.
			CloseBitmapSharedMemory();
			if (m_CurrentState.m_iBitmapSharedMemoryId != 0) {
				std::wstring wstrBitmapSharedMemory = BitmapSharedMemoryName(m_iClientId, GetMeshId(), m_CurrentState.m_iBitmapSharedMemoryId);
				HANDLE hBitmapSharedMemory = OpenFileMappingW(FILE_MAP_READ | FILE_MAP_WRITE, FALSE, wstrBitmapSharedMemory.c_str());
				m_BitmapSharedMemory.Set(hBitmapSharedMemory);
			}
		}
		RecreateBitmaps();

		//const FlashStateData::RectSettings& rectSettings = m_CurrentState.m_RectSettings.GetValue();
		//LogInfo("Recreated bitmaps at size " << rectSettings.GetBitmapWidth() << "," << rectSettings.GetBitmapHeight());
	}

	const FlashStateData::RectSettings& rectSettings = m_CurrentState.m_RectSettings.GetValue();
	if (bBitmapSizeChanged || bWindowPositionChanged) {
		//LogInfo("Changed window size to " << rectSettings.m_iWidth << "," << rectSettings.m_iHeight);
		SetWindowPos(
			m_hwndFPC,
			0,
			(int)rectSettings.m_iPositionX,
			(int)rectSettings.m_iPositionY,
			(int)rectSettings.m_iWidth,
			(int)rectSettings.m_iHeight,
			SWP_NOZORDER | SWP_NOACTIVATE);
	}

	if (bMediaParamsChanged) {
		const FlashStateData::MediaSettings& mediaSettings = m_CurrentState.m_MediaSettings.GetValue();
		SetMediaParams(mediaSettings.m_bufferFlashVars.GetString(), mediaSettings.m_bufferURL.GetString(), mediaSettings.m_eAllowScriptAccess);
	} else if (bRewind) {
		// Can only rewind if media parameters haven't changed.
		Rewind();
	}

	if (bSetFocusChanged) {
		LogInfo(GetIdString() << "Handling SetFocus request.");
		SetFocus();
	}

	if (bVolumeChanged)
		UpdateVolume();

	uint32_t idxAcknowledgement = m_ServerStateMgr.GetWorkAreaIndex();
	if (bGenerateBitmap) {
		uint32_t idxGenerateBitmap = idxAcknowledgement;
		if (!GenerateBitmap(idxGenerateBitmap))
			idxGenerateBitmap = 3;
		m_GeneratedBitmapState.m_idxBitmapBuffer = idxGenerateBitmap;
		m_GeneratedBitmapState.m_iGenerateBitmapId = pRequestedState->m_iGenerateBitmapId;
	}

	FlashSharedMemoryBlock::ServerAcknowledgement* pAcknowledgement = &m_pSharedMemory->m_aServerAcknowledgements[idxAcknowledgement];
	pAcknowledgement->m_BitmapState = m_GeneratedBitmapState;

	m_ServerStateMgr.CompletedWork();
	m_ClientStateMgr.ReleaseBuffer();
	return true;
}

std::string FlashControlServer::FilterFlashURL(const std::string& strURL) {
	return strURL;
}

std::string FlashControlServer::FilterFlashParams(const std::string& strParams) {
	const char* YOUTUBE_VIDEO_ID_PREFIX = "YouTubeVideoId=";

	// Tokenize params by "&" delimiter
	std::regex tokenParser("&");
	std::sregex_token_iterator tokenItr(strParams.begin(), strParams.end(), tokenParser, -1); // submatch = -1: select mismatched portion (skip delimiter)
	std::sregex_token_iterator endItr; // end-of-sequence iterator

	// Parse YouTubeVideoId token if exists and use it in future flash communications
	for (; tokenItr != endItr; ++tokenItr) {
		std::string token = *tokenItr;
		if (STLBeginWithIgnoreCase(token, YOUTUBE_VIDEO_ID_PREFIX)) {
			m_sYouTubeVideoId = token.substr(strlen(YOUTUBE_VIDEO_ID_PREFIX));
			assert(!m_sYouTubeVideoId.empty());
			LogInfo("YouTubeVideoId = " << m_sYouTubeVideoId);
			break;
		}
	}

	// Pass original string to flash (unrecognized vars will be ignored)
	return strParams;
}

void FlashControlServer::HandleFlashCall(SFPCFlashCallInfoStructW* pInfo) {
	// Handle flash call on YouTube videos only
	if (m_sYouTubeVideoId.empty()) {
		return;
	}

	assert(pInfo->request != nullptr);
	if (pInfo->request == nullptr) {
		return;
	}

	std::wstring sRequest = pInfo->request;
	LogDebug(sRequest);

	std::wstring sResponse;

	// Some hacky JS handling
	if (sRequest.find(L"return document.location.href;") != std::wstring::npos) {
		// Must be a youtube/google trusted URL
		// Cannot be the original iframe embed url for some reason - breaks playback
		sResponse = L"<string>https://www.youtube.com/v/";
		sResponse.append(Utf8ToUtf16(m_sYouTubeVideoId));
		sResponse.append(L"</string>");
	} else if (sRequest.find(L"return document.referrer;") != std::wstring::npos) {
		// It's OK to return empty string here - AS3 player does not seem to use it
		sResponse = L"<string></string>";
	} else if (sRequest.find(L"<invoke name=\"yt.util.activity.init\"") != std::wstring::npos) {
		// Do nothing for now
	} else if (sRequest.find(L"<invoke name=\"onYouTubePlayerReady\"") != std::wstring::npos) {
		// Start YouTube video
		LoadYouTubeVideoById(m_sYouTubeVideoId);
	} else if (sRequest.find(L"<invoke name = \"yt.abuse.botguardInitialized\"") != std::wstring::npos) {
		// Tell action script that the botguard is NOT ready.
		sResponse = _T("<false />"); // Not sure if this is correct format but it does work
	}

	FPCSetReturnValue(m_hwndFPC, sResponse.c_str());
}

void FlashControlServer::LoadYouTubeVideoById(const std::string& sYouTubeVideoId) {
	const size_t MAX_RESPONSE_LEN = 1023;

	std::wstring sRequest = L"<invoke name=\"loadVideoById\" returntype=\"xml\"><arguments><string>" + Utf8ToUtf16(sYouTubeVideoId) + L"</string></arguments></invoke>";
	LogDebug("Request: " << sRequest);

	wchar_t szResponse[MAX_RESPONSE_LEN + 1];
	DWORD responseLen = MAX_RESPONSE_LEN;

	if (S_OK == FPCCallFunction(m_hwndFPC, sRequest.c_str(), szResponse, &responseLen)) {
		assert(responseLen <= MAX_RESPONSE_LEN);
		LogDebug("Response: " << szResponse);
	} else {
		LogWarn("FPCCallFunction failed - " << sRequest);
	}
}

} // namespace FlashControl
} // namespace KEP
