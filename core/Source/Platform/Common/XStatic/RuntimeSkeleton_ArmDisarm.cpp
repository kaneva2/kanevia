///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "RuntimeSkeleton.h"

#include "CArmedInventoryClass.h"
#include "CGlobalInventoryClass.h"
#include "CSkeletonObject.h"
#include "SkeletonConfiguration.h"
#include "EquippableSystem.h"
#include "AIClass.h"
#include "IClientEngine.h" // TODO: reduce/eliminate use of IClientEngine::Instance inside XStatic
#include "Common/KEPUtil/Helpers.h"
#include "TextureProvider.h"
#include "ContentService.h"
#include "CMovementObj.h"

namespace KEP {

static LogInstance("Instance");

////////////////////////////////////////////////////////////////////////
// Equippable Item Arm/Disarm Functions
////////////////////////////////////////////////////////////////////////

RuntimeSkeleton::eEquipArmState RuntimeSkeleton::ArmEquippableItem(
	const GLID& glid,
	const std::set<int>& exclusionIDs,
	CMovementObj* pMO,
	SkeletonConfiguration* config,
	const CharConfigItem* pConfigItem,
	const boost::optional<std::string>& optstrBoneOverride) {
	PendingEquippableData data;
	data.m_glidOrBase = glid;
	data.m_exclusionIds = exclusionIDs;
	data.m_pCCI = NULL;
	data.m_pSC = config;
	data.m_boneOverride = optstrBoneOverride;
	if (pConfigItem) {
		data.m_pCCI = new CharConfigItem();
		data.m_pCCI->setGlidOrBase(glid);
		data.m_pCCI->updateSlotCount(pConfigItem->getSlotCount());

		// Insert slots
		for (size_t s = 0; s < pConfigItem->getSlotCount(); s++) {
			CharConfigItemSlot* pSlot = data.m_pCCI->getSlot(s);
			assert(pSlot != nullptr);
			*pSlot = *pConfigItem->getSlot(s);
		}
	}
	return ArmEquippableItem(std::move(data), pMO);
}

RuntimeSkeleton::eEquipArmState RuntimeSkeleton::ArmEquippableItem(PendingEquippableData&& data, CMovementObj* pMO) {
#if defined(_ClientOutput)
	G_DEVICE(eEquipArmState::Failed);

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return eEquipArmState::Failed;

	// check if there are pending equippable items conflicting with this item
	if (!data.m_exclusionIds.empty()) {
		for (auto itPEI = m_pendingEquippableItems.begin(); itPEI != m_pendingEquippableItems.end();) {
			bool bConflict = false;
			const PendingEquippableData& pendingEquip = *itPEI;
			if (pendingEquip.m_glidOrBase == data.m_glidOrBase) {
				++itPEI;
				continue;
			}

			auto pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(pendingEquip.m_glidOrBase);
			if (pGIO && pGIO->m_itemData) {
				for (const auto& excId : pGIO->m_itemData->m_exclusionGroupIDs) {
					if (data.m_exclusionIds.find(excId) != data.m_exclusionIds.end()) {
						//conflicting item
						bConflict = true;
					}
				}
			}

			if (bConflict) {
				itPEI = m_pendingEquippableItems.erase(itPEI);
				continue;
			}

			++itPEI;
		}
	}

	CArmedInventoryObj* pAIO = EquippableRM::Instance()->GetProxy(data.m_glidOrBase, true)->GetData();
	if (!pAIO) {
		// Progressive Load
		m_pendingEquippableItems.push_back(std::move(data));
		EquippableRM::Instance()->RegisterResourceLoadedCallback(data.m_glidOrBase, shared_from_this(), std::bind(&RuntimeSkeleton::EquippableReadyCallback, this, data.m_glidOrBase, pMO));
		return eEquipArmState::Pending;
	}

	// Look at other equip items and remove them if there is a conflict.  This is a change in logic before we
	// would arm both at once.
	if (m_armedInventoryList) {
		int remindex = 0;
		for (POSITION posLoc = m_armedInventoryList->GetHeadPosition(); posLoc != NULL;) {
			auto pAIO_find = (CArmedInventoryObj*)m_armedInventoryList->GetNext(posLoc);
			if ((pAIO_find->getLocationOccupancy() == pAIO->getLocationOccupancy()) || (IS_HEAD_ITEM(pAIO_find->getLocationOccupancy()) && IS_HEAD_ITEM(pAIO->getLocationOccupancy())) || (IS_HAIR_ITEM(pAIO_find->getLocationOccupancy()) && IS_HAIR_ITEM(pAIO->getLocationOccupancy()))) {
				// there is another item of this type that must be removed
				POSITION removePos = m_armedInventoryList->FindIndex(remindex);
				m_armedInventoryList->RemoveAt(removePos);
				delete pAIO_find;
				// restart the search from the beginning
				remindex = 0;
				posLoc = m_armedInventoryList->GetHeadPosition();
			} else {
				remindex = remindex + 1;
			}
		}
	}

	CArmedInventoryObj* pAIO_new = NULL;
	pAIO->Clone(&pAIO_new, gDevice);
	pAIO_new->setGlobalId(data.m_glidOrBase); // set type to glid just in case using old assets.
	if (data.m_boneOverride.is_initialized()) {
		int iBoneIndex = getBoneIndexByName(*data.m_boneOverride);
		pAIO_new->setAttachedBone(iBoneIndex, *data.m_boneOverride);
		pAIO_new->setLocationOccupancy(iBoneIndex);
	} else {
		const std::string& strBoneName = pAIO->getAttachedBoneName();
		// Some attachments are attached by index instead of name. I don't know why.
		if (!strBoneName.empty())
			pAIO_new->setAttachedBoneIndex(getBoneIndexByName(strBoneName));
	}
	if (!m_armedInventoryList)
		m_armedInventoryList = new CArmedInventoryList();

	// Get UGC material override
	if (!IS_LOCAL_GLID(data.m_glidOrBase)) { // Skip local EIs
		if (IS_UGC_GLID(data.m_glidOrBase)) {
			ContentMetadata md(data.m_glidOrBase);
			assert(md.IsValid());
			if (md.IsValid()) {
				auto textureProvider = std::make_shared<UGCItemTextureProvider>(md);

				RuntimeSkeleton* pSkeleton;
				pSkeleton = pAIO_new->getSkeleton(eVisualType::FirstPerson);
				if (pSkeleton != nullptr) {
					pSkeleton->setUGCItemTexture(textureProvider);
					pSkeleton->updateCustomMaterials(pICE->GetTextureDataBase());
				}

				pSkeleton = pAIO_new->getSkeleton(eVisualType::ThirdPerson);
				if (pSkeleton != nullptr) {
					pSkeleton->setUGCItemTexture(textureProvider);
					pSkeleton->updateCustomMaterials(pICE->GetTextureDataBase());
				}
			}
		}
	}

	pAIO_new->realizeDimConfig(eVisualType::FirstPerson, pAIO);
	pAIO_new->realizeDimConfig(eVisualType::ThirdPerson, pAIO);

	m_armedInventoryList->AddTail(pAIO_new);

	// Set Custom Animation On Item If It Has One
	GLID animGlidSpecial = (data.m_pCCI ? data.m_pCCI->getAnimationGlid() : GLID_INVALID);
	if (IS_VALID_GLID(animGlidSpecial)) {
		AnimSettings animSettings;
		animSettings.setGlidSpecial(animGlidSpecial);
		pAIO_new->setPrimaryAnimationSettings(animSettings);
	} else if (IS_HEAD_ITEM(pAIO_new->getLocationOccupancy())) {
		pAIO_new->getSkeleton()->setPrimaryAnimationByAnimTypeAndVer(eAnimType::Stand);
	}

	// object is loaded and ready to go!
	if (data.m_pSC) {
		// Apply position/orientation/scale settings
		if (data.m_pPlacementOverride) {
			if (pAIO_new) {
				pAIO_new->setTranslation(data.m_pPlacementOverride->m_ptPosition);
				pAIO_new->setRotationZXY(data.m_pPlacementOverride->m_vEulerZXY);
				pAIO_new->setScale(data.m_pPlacementOverride->m_vScale);
				pAIO_new->setIgnoreParentScale(data.m_pPlacementOverride->m_bIgnoreParentScale);
			}
		}

		if (data.m_pCCI && IS_HEAD_ITEM(pAIO_new->getLocationOccupancy())) {
			if (m_armedInventoryList && (m_armedInventoryList->GetCount() > 0)) {
				auto pHeadSkeletonCfg = pAIO_new->getSkeletonConfig();

				auto pHeadSkeleton = pAIO_new->getSkeleton();

				// Mesh configuration for avatar head
				CharConfigItem* pHeadConfigItem = data.m_pCCI;

				auto pHeadSkeletonRef = pAIO->getSkeleton();

				if (pHeadSkeletonCfg && pHeadSkeleton && pHeadConfigItem && pHeadSkeletonRef) {
					pHeadSkeletonCfg->m_newConfig.initBaseItem(pHeadSkeleton->getSkinnedMeshCount());

					for (size_t slot = 0; slot < pHeadSkeleton->getSkinnedMeshCount() && slot < pHeadConfigItem->getSlotCount(); slot++) {
						CharConfigItemSlot* pSlot = pHeadConfigItem->getSlot(slot);
						assert(pSlot != nullptr);
						pHeadSkeletonCfg->HotSwapDim(slot, pSlot->m_meshId, pHeadSkeletonRef->getSkinnedMeshCount(), false);
						pHeadSkeletonCfg->HotSwapMat(slot, pSlot->m_meshId, pSlot->m_matlId, FALSE);
						pHeadSkeletonCfg->HotSwapRGB(slot, pSlot->m_r, pSlot->m_g, pSlot->m_b, pHeadSkeleton);
						data.m_pSC->m_charConfig.setItemSlotMeshId(pHeadConfigItem->getGlidOrBase(), slot, pSlot->m_meshId);
						data.m_pSC->m_charConfig.setItemSlotMaterialId(pHeadConfigItem->getGlidOrBase(), slot, pSlot->m_matlId);
					}
				}
			}
		}

		// Remove existing hair if applicable
		if (IS_HAIR_ITEM(pAIO_new->getLocationOccupancy()) && pMO)
			pMO->removeWOKHair();

		if (data.m_pCCI && !data.m_pCCI->getTexUrl().empty())
			pICE->SetPlayerCustomTextureOnAccessoryLocal(this, data.m_pCCI->getGlidOrBase(), data.m_pCCI->getTexUrl());
	}

	pAIO_new->realizeDimConfig(eVisualType::ThirdPerson, pAIO);

#endif

	return eEquipArmState::Success;
}

bool RuntimeSkeleton::DisarmEquippableItem(const GLID& glid) {
	size_t nRemoved = 0;

	// Disarm currently armed equippable items
	if (m_armedInventoryList)
		nRemoved += m_armedInventoryList->deleteAllWithGLID(glid);

	// Disarm pending equippable items
	for (auto itr = m_pendingEquippableItems.begin(); itr != m_pendingEquippableItems.end();) {
		if (itr->m_glidOrBase == glid) {
			itr = m_pendingEquippableItems.erase(itr);
			++nRemoved;
		} else {
			++itr;
		}
	}

	return nRemoved > 0;
}

} // namespace KEP