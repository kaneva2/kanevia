///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CWorldGroupClass.h"

#include "GetSetStrings.h"

namespace KEP {

CWldGroupObjList* CWldGroupObjList::m_pInstance = 0;

//------------Main object list------------------//

CWldGroupObj::CWldGroupObj(const CStringA& groupName, int groupInt) {
	m_groupInt = groupInt;
	m_groupName = groupName;
} // CWldGroupObj::CWldGroupObj

void CWldGroupObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_groupInt << m_groupName;
	} else {
		ar >> m_groupInt >> m_groupName;
	}
} // CWldGroupObj::Serialize

ListItemMap
CWldGroupObjList::GetGroups() {
	ListItemMap groupMap;

	int index = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWldGroupObj* wldPtr = static_cast<CWldGroupObj*>(GetNext(posLoc));
		CStringA str = wldPtr->m_groupName;

		str.Format("%s ID::%d", str, wldPtr->m_groupInt);
		groupMap.insert(ListItemMap::value_type(wldPtr->m_groupInt, static_cast<const char*>(str)));
		index++;
	}

	return groupMap;
} // CWldGroupObjList::GetGroups

CWldGroupObj*
CWldGroupObjList::FindByID(int groupID) {
	for (POSITION groupPosition = GetHeadPosition(); groupPosition;) {
		CWldGroupObj* pWldGroupObj;
		pWldGroupObj = static_cast<CWldGroupObj*>(GetNext(groupPosition));
		if (pWldGroupObj && (pWldGroupObj->m_groupInt == groupID))
			return pWldGroupObj;
	} // for..groupPosition

	return 0;
} // CWldGroupObjList::FindByID

CWldGroupObj*
CWldGroupObjList::FindByName(const CStringA& name) {
	for (POSITION groupPosition = GetHeadPosition(); groupPosition;) {
		CWldGroupObj* pWldGroupObj;
		pWldGroupObj = static_cast<CWldGroupObj*>(GetNext(groupPosition));
		if (pWldGroupObj && (name == pWldGroupObj->m_groupName))
			return pWldGroupObj;
	} // for..groupPosition

	return 0;
} // CWldGroupObjList::FindByName

IMPLEMENT_SERIAL(CWldGroupObj, CObject, 0)
IMPLEMENT_SERIAL(CWldGroupObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CWldGroupObj, IDS_WLDGROUPOBJ_NAME)
GETSET_RANGE(m_groupInt, gsInt, GS_FLAGS_DEFAULT, 0, 0, WLDGROUPOBJ, GROUPINT, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP