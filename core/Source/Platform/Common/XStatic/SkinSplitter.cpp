///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include "math.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "afxext.h"

#include <SerializeHelper.h>
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "CBoneClass.h"
#include "VertexDeclaration.h"
#include "CDeformableMesh.h"
#include "CMaterialEffect.h"

#include "skinsplitter.h"
#include "skinsplitterindexlist.h"

#include <algorithm>

namespace KEP {

/* skin data API */
SkinSplitterSkin*
SkinSplitterSkinCreate(const CEXMeshObj* mesh, const VertexAssignmentBlended* pVertexDataSource) {
	int numVertices = mesh->m_abVertexArray.m_vertexCount;

	SkinSplitterSkin* skin = new SkinSplitterSkin;

	skin->vertexMaps.matrixWeights = new SkinSplitterMatrixWeights[numVertices];
	skin->vertexMaps.matrixIndices = new int[numVertices];

	skin->boneData.numBones = mesh->mBoneData.mNumBones;
	skin->boneData.invBoneToSkinMat = new Matrix44f[skin->boneData.numBones]; /* UNUSED FOR NOW */

	/* set up the per-vertex bone index array */
	int* newBoneIx = skin->vertexMaps.matrixIndices;
	if (newBoneIx) {
		for (int i = 0; i < numVertices; i++) {
			//	Set indices and weights to 0
			int boneIx[4] = { 0, 0, 0, 0 };

			//	No more than 4 bones can influence a vertex.
			int indexCount = (std::min)(4u, pVertexDataSource->vertexBoneWeights.size());

			for (int j = 0; j < indexCount; j++) {
				boneIx[j] = pVertexDataSource->vertexBoneWeights[j].boneIndex;
			}

			newBoneIx[i] = boneIx[0] + (boneIx[1] << 8) + (boneIx[2] << 16) + (boneIx[3] << 24); /* watch the order here...SM's seems to differ */

			pVertexDataSource++;
		}
	} else {
		return NULL;
	}

	/* set up the per-vertex bone weight array */
	SkinSplitterMatrixWeights* newBoneWts = skin->vertexMaps.matrixWeights;

	if (newBoneWts) {
		for (int i = 0; i < numVertices; i++) {
			//	No more than 4 bones can influence a vertex.
			size_t indexCount = (std::min)(4u, pVertexDataSource->vertexBoneWeights.size());
			for (size_t wi = 0; wi < 4; wi++) {
				newBoneWts[i].w[wi] = 0.0f;
				if (wi < indexCount) {
					newBoneWts[i].w[wi] = pVertexDataSource->vertexBoneWeights[wi].weight;
				}
			}

			pVertexDataSource++;
		}
	} else {
		return NULL;
	}

	return skin;
}

void SkinSplitterSkinDestroy(SkinSplitterSkin* skin) {
	if (skin) {
		delete[] skin->vertexMaps.matrixWeights;
		delete[] skin->vertexMaps.matrixIndices;
		delete[] skin->boneData.invBoneToSkinMat;
		delete skin;
	}
}

/* morph target (vertex data) API */
/* TODO: fill in vertex position, normal data */
SkinSplitterMorphTarget*
SkinSplitterMorphTargetCreate(const CEXMeshObj* mesh, const VertexAssignmentBlended* pVertexDataSource) {
	int numVertices = mesh->m_abVertexArray.m_vertexCount;
	SkinSplitterMorphTarget* morphTarget = new SkinSplitterMorphTarget;
	morphTarget->verts = new Vector3f[numVertices];
	morphTarget->normals = new Vector3f[numVertices];

	return morphTarget;
}

void SkinSplitterMorphTargetDestroy(SkinSplitterMorphTarget* morphTarget) {
	if (morphTarget) {
		delete[] morphTarget->verts;
		delete[] morphTarget->normals;
		delete morphTarget;
	}
}

/* skinned geometry data API */
/* TODO: fill in vertex texcoord data */
SkinSplitterGeometry*
SkinSplitterGeometryCreate(const CEXMeshObj* mesh, const VertexAssignmentBlended* pVertexDataSource) {
	SkinSplitterGeometry* geom = new SkinSplitterGeometry;

	geom->numTriangles = mesh->m_abVertexArray.m_indecieCount / 3;
	geom->numVertices = mesh->m_abVertexArray.m_vertexCount;
	geom->numTexCoordSets = 1;

	/* allocate the morph target and skin */
	geom->morphTarget = SkinSplitterMorphTargetCreate(mesh, pVertexDataSource);
	if (!geom->morphTarget) {
		return NULL;
	}

	geom->skin = SkinSplitterSkinCreate(mesh, pVertexDataSource);
	if (!geom->skin) {
		return NULL;
	}

	/* create the material list info (only 1 material) */
	geom->matList.numMaterials = 1;
	geom->matList.materials = new CMaterialObject*[geom->matList.numMaterials];
	geom->matList.materials[0] = mesh->m_materialObject;

	/* create the triangle list */
	geom->triangles = new SkinSplitterTriangle[geom->numTriangles];
	for (int i = 0; i < geom->numTriangles; i++) {
		geom->triangles[i].matIndex = 0;
		for (int j = 0; j < 3; j++) {
			geom->triangles[i].vertIndex[j] = mesh->m_abVertexArray.m_ushort_indexArray[i * 3 + j];
		}
	}
	/* create the per-vertex tex coords */
	geom->texCoords[0] = new SkinSplitterTexCoords[geom->numVertices];

	return geom;
}

void SkinSplitterGeometryDestroy(SkinSplitterGeometry* geom) {
	if (geom) {
		SkinSplitterSkinDestroy(geom->skin);
		SkinSplitterMorphTargetDestroy(geom->morphTarget);
		delete[] geom->texCoords[0];
		delete[] geom->triangles;
		delete geom;
	}
}

} // namespace KEP