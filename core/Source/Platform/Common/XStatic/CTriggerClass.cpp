///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <d3dx9.h>
#include <afxtempl.h>

#include "CTriggerClass.h"

#include "CABFunctionLib.h"
#include "KepWidgets.h"
#include "MeshStrings.h"
#include "ReMesh.h"
#include "CMaterialClass.h"
#include "ReMeshCreate.h"
#include "Core/Math/Sphere.h"
#include "Core/Math/Cuboid.h"
#include "PrimitiveGeomArgs.h"
#include "common/include/IMemSizeGadget.h"
#include "common/include/MemSizeSpecializations.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

const D3DXCOLOR TRIGGER_COLOR_DIFFUSE(0.00f, 0.00f, 0.00f, 0.99f);
const D3DXCOLOR TRIGGER_COLOR_EMISSIVE(0.60f, 0.15f, 0.75f, 0.99f);

void CTriggerObject::Serialize(CArchive& ar) {
	BOOL reservedBOOL = FALSE;
	float reservedFloat = 0.0f;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		assert(false);
		ar << (int)m_triggerAction
		   << m_intParam
		   << GetSphereRadius()
		   << m_vectorParam.x
		   << m_vectorParam.y
		   << m_vectorParam.z
		   << (BOOL)(m_oneTimeOnly ? TRUE : FALSE)
		   << reservedBOOL
		   << reservedFloat
		   << (CStringA)m_strParam.c_str();
	} else {
		float radius;
		BOOL oneTimeOnly;
		int triggerAction;
		CStringA cstr;
		ar >> triggerAction >> m_intParam >> radius >> m_vectorParam.x >> m_vectorParam.y >> m_vectorParam.z >> oneTimeOnly >> reservedBOOL >> reservedFloat >> cstr;
		m_strParam = cstr.GetString();
		m_triggerAction = (eTriggerAction)triggerAction;
		m_pPrimitive = std::make_shared<Sphere<float>>(Vector3f(0.0f, 0.0f, 0.0f), radius);
		m_oneTimeOnly = oneTimeOnly == TRUE;
		m_everActuated = false;
		m_inTrigger = false;
	}
}

void CTriggerObject::Clone(CTriggerObject** ppTO_out) {
	auto pTO_new = new CTriggerObject();
	pTO_new->m_triggerId = m_triggerId;
	pTO_new->m_triggerAction = m_triggerAction;
	pTO_new->m_intParam = m_intParam;
	pTO_new->m_pPrimitive = m_pPrimitive;
	pTO_new->m_vectorParam.x = m_vectorParam.x;
	pTO_new->m_vectorParam.y = m_vectorParam.y;
	pTO_new->m_vectorParam.z = m_vectorParam.z;
	pTO_new->m_oneTimeOnly = m_oneTimeOnly;
	pTO_new->m_strParam = m_strParam;
	*ppTO_out = pTO_new;
}

eTriggerState CTriggerObject::EvaluatePosition(const Vector3f& pt) {
	if (m_pendingDelete)
		return eTriggerState::Deleted;

	if (m_oneTimeOnly && m_everActuated)
		return eTriggerState::TriggerOnce;

	if (!m_pPrimitive)
		return eTriggerState::Outside;

	if (m_pPrimitive->Contains(pt)) {
		if (m_inTrigger)
			return eTriggerState::Inside;

		m_inTrigger = true;
		m_everActuated = true;
		return eTriggerState::MovedInside;
	}

	if (m_inTrigger) {
		m_inTrigger = false;
		return eTriggerState::MovedOutside;
	}

	return eTriggerState::Outside;
}

eTriggerState CTriggerObject::ExitTrigger() {
	if (m_pendingDelete)
		return eTriggerState::Deleted;

	if (m_oneTimeOnly && m_everActuated)
		return eTriggerState::TriggerOnce;

	if (m_inTrigger) {
		m_inTrigger = false;
		return eTriggerState::MovedOutside;
	}

	return eTriggerState::Outside;
}

ReMesh* CTriggerObject::GetMesh() {
#ifdef _ClientOutput
	if (!m_pPrimitive)
		return nullptr;

	if (!m_pRM) {
		Primitive<float>::TriList triangleList;
		m_pPrimitive->GenerateVisualMesh(triangleList);

		// Validate
		size_t numV = triangleList.positions.size();
		assert(numV == triangleList.normals.size());
		if (numV == 0)
			return nullptr;

		ReMeshData data;
		ZeroMemory(&data, sizeof(data));
		data.PrimType = ReMeshPrimType::TRILIST;
		data.NumV = numV;
		data.NumUV = 1;
		data.pP = &triangleList.positions[0];
		data.pN = &triangleList.normals[0];

		std::vector<Vector2f> tmpUVs;
		tmpUVs.resize(numV);
		for (size_t i = 0; i < numV; ++i) {
			tmpUVs[i].MakeZero();
		}
		data.pUv = &tmpUVs[0];

		m_pRM = MeshRM::Instance()->CreateReStaticMesh(&data, false);
	}

	if (!m_pMaterial) {
		CMaterialObject* mat = new CMaterialObject();
		mat->m_baseMaterial.Diffuse = TRIGGER_COLOR_DIFFUSE;
		mat->m_baseMaterial.Emissive = TRIGGER_COLOR_EMISSIVE;
		mat->m_baseMaterial.Ambient = D3DXCOLOR(.1f, .1f, .1f, 1.0f);
		mat->m_useMaterial = mat->m_baseMaterial;
		mat->m_blendMode = 0;
		mat->m_cullOn = FALSE; // Render both faces

		MeshRM::Instance()->CreateReMaterial(mat);
		mat->GetReMaterial()->PreLight(FALSE);

		m_pMaterial = mat;
		m_pRM->SetMaterial(m_pMaterial->GetReMaterial());
	}

	return m_pRM;
#else
	return nullptr;
#endif
}

Matrix44f* CTriggerObject::GetMatrix() {
	if (!m_pMtx)
		m_pMtx = new Matrix44f();

	if (m_pPrimitive) {
		// Update and return matrix
		m_pPrimitive->GetVisualMeshMatrix(*m_pMtx);
	}

	return m_pMtx;
}

void CTriggerObjectList::Log(const std::string& name) {
	_LogInfo(name << " - triggers=" << GetTriggerCount());
	ForEachTrigger([](CTriggerObject* pTO) {
		if (pTO) {
			_LogInfo(" ... " << pTO->ToStr());
		}
	});
}

void CTriggerObjectList::Clone(CTriggerObjectList** ppTOL_out) {
	auto pTOL_new = new CTriggerObjectList();
	ForEachTrigger([pTOL_new](CTriggerObject* pTO) {
		CTriggerObject* pTO_clone;
		pTO->Clone(&pTO_clone);
		pTOL_new->m_pTOs.push_back(std::unique_ptr<CTriggerObject>(pTO_clone));
	});
	*ppTOL_out = pTOL_new;
}

CTriggerObject* CTriggerObjectList::AddTrigger(
	int triggerId,
	eTriggerAction triggerAction,
	int intParam,
	const PrimitiveGeomArgs<float>& volumeArgs,
	const std::string& strParam,
	const Vector3f& vectorParam,
	const Vector3f& origin,
	const Vector3f& dir,
	bool oneTimeOnly,
	int placementId) {
	auto pTO = std::make_unique<CTriggerObject>(triggerId, triggerAction, intParam, PrimitiveF_Ptr(volumeArgs.createPrimitive()), strParam);
	pTO->SetVector(vectorParam);
	pTO->SetTransform(origin, dir);
	pTO->SetOneTimeOnly(oneTimeOnly);
	pTO->SetAttachObjId(placementId);
	return AddTrigger(std::move(pTO));
}

CTriggerObject* CTriggerObjectList::AddTrigger(std::shared_ptr<CTriggerObject> pTO) {
	m_pTOs.push_back(pTO);
	return m_pTOs.back().get();
}

CTriggerObject* CTriggerObjectList::GetTriggerByPlacementId(int placementId) {
	for (auto itr = m_pTOs.begin(); itr != m_pTOs.end(); ++itr) {
		auto pTO = *itr;
		if (!pTO)
			continue;
		if (pTO->GetAttachObjId() == placementId)
			return itr->get();
	}
	return nullptr;
}

void CTriggerObjectList::CleanupPendingDeletions() {
	for (auto itr = m_pTOs.begin(); itr != m_pTOs.end();) {
		auto pTO = *itr;
		if (!pTO)
			continue;
		if (pTO->IsPendingDelete()) {
			//LogWarn("DELETING " << pTO->ToStr());
			itr = m_pTOs.erase(itr);
		} else {
			++itr;
		}
	}
}

void CTriggerObjectList::Serialize(CArchive& ar) {
	std::unique_ptr<CObList> pObjList = std::make_unique<CObList>();
	if (ar.IsLoading()) {
		pObjList->Serialize(ar);
		for (POSITION pos = pObjList->GetHeadPosition(); pos;) {
			std::unique_ptr<CTriggerObject> pTriggerObject(static_cast<CTriggerObject*>(pObjList->GetNext(pos)));
			m_pTOs.push_back(std::move(pTriggerObject));
		}
	} else {
		for (const auto& pTrigger : m_pTOs)
			pObjList->AddTail(pTrigger.get());
		pObjList->Serialize(ar);
	}
}

float CTriggerObject::GetSphereRadius() const {
	if (!m_pPrimitive || m_pPrimitive->GetType() != PrimitiveType::Sphere)
		return 0;
	const Sphere<float>* pSphere = static_cast<const Sphere<float>*>(m_pPrimitive.get());
	return pSphere->GetRadius();
}

Vector3f CTriggerObject::GetHalfExtents() const {
	if (!m_pPrimitive || m_pPrimitive->GetType() != PrimitiveType::Cuboid)
		return Vector3f::Zero();
	const Cuboid<float>* pCuboid = static_cast<const Cuboid<float>*>(m_pPrimitive.get());
	Vector3f halfExtents = (pCuboid->GetMax() - pCuboid->GetMin()) * 0.5f;
	return halfExtents;
}

void CTriggerObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pMaterial);
		pMemSizeGadget->AddObjectSizeof(m_pMtx);
		pMemSizeGadget->AddObject(m_strParam);
		pMemSizeGadget->AddObject(m_pRM);
		pMemSizeGadget->AddObject(m_pPrimitive);
	}
}

void CTriggerObjectList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pTOs);
	}
}

IMPLEMENT_SERIAL(CTriggerObject, CObject, 0)
IMPLEMENT_SERIAL(CTriggerObjectList, CObject, 0)

} // namespace KEP