///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#define NAMEMAX 200 // string size
#define TIMERID 1 // timer ID to use
#define TIMERINTERVAL 1000 // timer interval
#define MAXSTRLEN 200 // maximum size of temp strings
#include "resource.h"
#include "ClientStrings.h"
#include "d3d9.h"
#include "afxtempl.h"
#include "math.h"
#include "cabd3dfunctionlib.h"

#include "common\include\Rect.h"
#include "common\KEPUtil\Helpers.h"
#include "UnicodeMFCHelpers.h"
static LogInstance("Instance");

static const wchar_t c_szWAV[] = L"WAV";

int CABD3DFunctionLib::avgPacketSize;
BOOL CABD3DFunctionLib::deviceIsForceFeedback[4];
int CABD3DFunctionLib::curdriver;
int CABD3DFunctionLib::totalDisplayModes;
int CABD3DFunctionLib::videoMDWidth[50];
int CABD3DFunctionLib::videoMDHeight[50];
int CABD3DFunctionLib::videoMDBpp[50];
int CABD3DFunctionLib::NumDrivers;
BOOL CABD3DFunctionLib::driverHardware[10];
BOOL CABD3DFunctionLib::driverRGB[10];
BOOL CABD3DFunctionLib::driverAntialias[10];
BOOL CABD3DFunctionLib::driverBiLinearTexture[10];
BOOL CABD3DFunctionLib::driverAlphaBlending[10];
BOOL CABD3DFunctionLib::driverFogging[10];
BOOL CABD3DFunctionLib::driverDither[10];
BOOL CABD3DFunctionLib::driverZbuffer[10];
BOOL CABD3DFunctionLib::driverMemType[10];
GUID CABD3DFunctionLib::DriverGUID[10]; /* GUIDs of the available D3D drivers */
char CABD3DFunctionLib::DriverName[10][50]; /* names of the available D3D drivers */

int CABD3DFunctionLib::HowManyJoysticks;
LPDIRECTINPUTDEVICE2 CABD3DFunctionLib::deviceFound[4]; //allow for four devices

CABD3DFunctionLib::CABD3DFunctionLib() {
	NumDrivers = 0;
	totalDisplayModes = 0;
	curdriver = -1;
	currentVideoMode = 0;
	HowManyJoysticks = 0;
	deviceFound[0] = 0;
	deviceFound[1] = 0;
	deviceFound[2] = 0;
	deviceFound[3] = 0;
	directInput = 0;
	directKeyboard = 0;
	directMouse = 0;
	avgPacketSize = 0;
	serverPopoutCount = 150;
	clientMsgQue = -1;
	clientMsgQueAttribute = -1;
	ChatModeOn = FALSE;
	TakingKeyStrokes = TRUE;
	PrimarySurfaceRef = NULL;
	m_curModeRect = RECT({ 0, 0, 0, 0 });
}

BOOL FAR PASCAL CABD3DFunctionLib::EnumFFJoysticksCallback(LPCDIDEVICEINSTANCE pInst, LPVOID lpvContext) {
	LPDIRECTINPUT pdi = (LPDIRECTINPUT)lpvContext;
	HRESULT hr;
	LPDIRECTINPUTDEVICE pDevice;

	// obtain an interface to the enumerated force feedback joystick.
	hr = pdi->CreateDevice(pInst->guidInstance, &pDevice, NULL);

	// if it failed, then we can't use this joystick for some
	// bizarre reason.  (Maybe the user unplugged it while we
	// were in the middle of enumerating it.)  So continue enumerating
	if (FAILED(hr))
		return DIENUM_CONTINUE;

	// query for IDirectInputDevice2
	hr = pDevice->QueryInterface(IID_IDirectInputDevice2, (LPVOID*)&deviceFound[HowManyJoysticks]);

	pDevice->Release(); // done with old interface now

	if (FAILED(hr)) {
		return DIENUM_CONTINUE; // try again
	}

	// we successfully created an IDirectInputDevice2.  So stop looking
	// for another one.
	//deviceIsForceFeedback[HowManyJoysticks]=TRUE;
	HowManyJoysticks++;

	return DIENUM_STOP;
}

BOOL FAR PASCAL CABD3DFunctionLib::InitJoystickInput(LPCDIDEVICEINSTANCE pdinst, LPVOID pvRef) {
	LPDIRECTINPUT pdi = (LPDIRECTINPUT)pvRef;
	LPDIRECTINPUTDEVICE pdev;
	DIPROPRANGE diprg;
	if (pdi->CreateDevice(pdinst->guidInstance, &pdev, NULL) != DI_OK) { // create the DirectInput joystick device
		//OutputDebugString("IDirectInput::CreateDevice FAILED\n");
		return DIENUM_CONTINUE;
	} //end create the DirectInput joystick device

	if (pdev->SetDataFormat(&c_dfDIJoystick) != DI_OK) { // set joystick data format
		pdev->Release();
		return DIENUM_CONTINUE;
	} //end set joystick data format

	// set X-axis range to (-1000 ... +1000)
	// This lets us test against 0 to see which way the stick is pointed.
	diprg.diph.dwSize = sizeof(diprg);
	diprg.diph.dwHeaderSize = sizeof(diprg.diph);
	diprg.diph.dwObj = DIJOFS_X;
	diprg.diph.dwHow = DIPH_BYOFFSET;
	diprg.lMin = -1000;
	diprg.lMax = +1000;

	if (pdev->SetProperty(DIPROP_RANGE, &diprg.diph) != DI_OK) { //set property
		pdev->Release();
		return FALSE;
	} //end set property

	//
	// And again for Y-axis range
	//
	diprg.diph.dwObj = DIJOFS_Y;

	if (pdev->SetProperty(DIPROP_RANGE, &diprg.diph) != DI_OK) { //set property Y
		pdev->Release();
		return FALSE;
	} //end set property

	// set X axis dead zone to 50% (to avoid accidental turning)
	// Units are ten thousandths, so 50% = 5000/10000.
	if (SetDIDwordProperty(pdev, DIPROP_DEADZONE, DIJOFS_X, DIPH_BYOFFSET, 1500) != DI_OK) {
		pdev->Release();
		return FALSE;
	}

	// set Y axis dead zone to 50% (to avoid accidental thrust)
	// Units are ten thousandths, so 50% = 5000/10000.
	if (SetDIDwordProperty(pdev, DIPROP_DEADZONE, DIJOFS_Y, DIPH_BYOFFSET, 5000) != DI_OK) {
		pdev->Release();
		return FALSE;
	}

	AddInputDevice(pdev, pdinst);

	return DIENUM_CONTINUE;
}

void CABD3DFunctionLib::AddInputDevice(LPDIRECTINPUTDEVICE pdev, LPCDIDEVICEINSTANCE pdi) {
	if (HowManyJoysticks < 4) {
		HRESULT hRes;

		/*
		 *  Convert it to a Device2 so we can Poll() it.
		 */

		hRes = pdev->QueryInterface(IID_IDirectInputDevice2,
			(LPVOID*)&deviceFound[HowManyJoysticks]);

		//deviceIsForceFeedback[HowManyJoysticks]=FALSE;
		HowManyJoysticks++;
	}
}

HRESULT CABD3DFunctionLib::SetDIDwordProperty(LPDIRECTINPUTDEVICE pdev, REFGUID guidProperty,
	DWORD dwObject, DWORD dwHow, DWORD dwValue) {
	DIPROPDWORD dipdw;

	dipdw.diph.dwSize = sizeof(dipdw);
	dipdw.diph.dwHeaderSize = sizeof(dipdw.diph);
	dipdw.diph.dwObj = dwObject;
	dipdw.diph.dwHow = dwHow;
	dipdw.dwData = dwValue;

	return pdev->SetProperty(guidProperty, &dipdw.diph);
}

bool CABD3DFunctionLib::InitializeDirectInput(HWND hwndLoc, HINSTANCE instanceHandleLoc) {
	// Initialize DirectInput
	HRESULT err;
	err = DirectInput8Create(
		instanceHandleLoc,
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(VOID**)&directInput,
		NULL);
	if (err != 0) {
		LogError("DirectInput8Create() FAILED");
		return false;
	}

	// Keyboard DirectInput Device
	err = directInput->CreateDevice(GUID_SysKeyboard, &directKeyboard, NULL);
	if (err != 0 || !directKeyboard) {
		LogError("CreateDevice(directKeyboard) FAILED");
		return false;
	}
	directKeyboard->SetCooperativeLevel(hwndLoc, DISCL_EXCLUSIVE | DISCL_NOWINKEY | DISCL_BACKGROUND);
	directKeyboard->SetDataFormat(&c_dfDIKeyboard);
	directKeyboard->Acquire();

	// Mouse DirectInput Device
	err = directInput->CreateDevice(GUID_SysMouse, &directMouse, NULL);
	if (err != 0 || !directMouse) {
		LogError("CreateDevice(directMouse) FAILED");
		return false;
	}
	directMouse->SetCooperativeLevel(hwndLoc, DISCL_EXCLUSIVE | DISCL_BACKGROUND);
	directMouse->SetDataFormat(&c_dfDIMouse);
	directMouse->Acquire();

	return true;
}

bool CABD3DFunctionLib::AcquireDirectInput(bool acquire) {
	if (!directKeyboard || !directMouse) {
		//		LogError("directInput is null"); // happens on first call immediately after OnActivate()
		return false;
	}

	bool ok = true;
	if (acquire) {
		ok &= (directKeyboard->Acquire() == DI_OK);
		ok &= (directMouse->Acquire() == DI_OK);
	} else {
		ok &= (directKeyboard->Unacquire() == DI_OK);
		ok &= (directMouse->Unacquire() == DI_OK);
	}
	if (!ok) {
		LogError("FAILED - acquire=" << LogBool(acquire));
	}

	return ok;
}

void CABD3DFunctionLib::BlackOut(LPDIRECTDRAWSURFACE7 surfaceLoc) {
	DDBLTFX bltfx;
	memset(&bltfx, 0, sizeof(bltfx));
	bltfx.dwSize = sizeof(bltfx);
	bltfx.dwFillColor = 0;
	surfaceLoc->Blt(0, 0, 0, DDBLT_COLORFILL | DDBLT_WAIT, &bltfx);
}

void CABD3DFunctionLib::PostMessageD3D(LPDIRECTDRAW7 ddrawLoc, CStringA textLoc, BOOL fullscreenOn) {
	if (fullscreenOn == TRUE)
		ddrawLoc->FlipToGDISurface();
	KEP::AfxMessageBoxUtf8(textLoc);
}

HRESULT CABD3DFunctionLib::RenderStreamToSurface(const char* pszFileName, LPDIRECTDRAW7 pDD, LPDIRECTDRAWSURFACE7 pPrimary, RECT rect) {
	HRESULT hr = 0;
	IDirectDrawStreamSample* pSample = NULL;
	LPDIRECTDRAWSURFACE7 pSurface4 = NULL;
	IAMMultiMediaStream* pStreamLoc = NULL;
	IAMMultiMediaStream* pAMStreamLoc = NULL;
	IMediaStream* pPrimaryVidStreamLoc = NULL;
	IDirectDrawMediaStream* pDDStreamLoc = NULL;
	RECT rectLoc;

	GetMovieInterfaces(pszFileName,
		pDD,
		&pSurface4,
		&pSample,
		&pStreamLoc,
		&pAMStreamLoc,
		&pPrimaryVidStreamLoc,
		&pDDStreamLoc,
		&rectLoc);

	while (true) {
		//allow user to break from movie
		BYTE globalForceKeys[256];
		if (directKeyboard->GetDeviceState(256, globalForceKeys) != DI_OK)
			break;
		if (globalForceKeys[DIK_ESCAPE] & 0x80)
			break;

		if (pSample->Update(0, NULL, NULL, 0) != S_OK) {
			break;
		}

		DDBLTFX bltfx;
		memset(&bltfx, 0, sizeof(bltfx));
		bltfx.dwSize = sizeof(bltfx);
		bltfx.dwFillColor = 0;
		pPrimary->Blt(0, 0, 0, DDBLT_COLORFILL | DDBLT_WAIT, &bltfx);

		pPrimary->Blt(&rect, pSurface4, &rectLoc, DDBLT_WAIT, NULL);
		PrimarySurfaceRef->Flip(0, NULL);
	}

	pPrimaryVidStreamLoc->Release();
	pPrimaryVidStreamLoc = 0;
	pDDStreamLoc->Release();
	pDDStreamLoc = 0;
	pSample->Release();
	pSample = 0;
	pSurface4->Release();
	pSurface4 = 0;
	pAMStreamLoc->Release();
	pAMStreamLoc = 0;

	return hr;
}

void CABD3DFunctionLib::PlayMovie(const char* pszFileName, LPDIRECTDRAWSURFACE7 pPrimary, BOOL fullscreenLoc, LPDIRECTDRAW7 pDD) {
	//make sure u set display to 640 x 480 and black out screen
	BlackOut(pPrimary);
	RECT rLoc; //make 560 X _320_
	rLoc.right = 600;
	rLoc.left = 40;
	rLoc.top = 80;
	rLoc.bottom = 400;
	RenderStreamToSurface(pszFileName, pDD, pPrimary, rLoc);
	BlackOut(pPrimary);
}

BOOL CABD3DFunctionLib::MakeDirectSound(HWND hwnd, IDirectSound8** lpds) {
	if
		FAILED(DirectSoundCreate8(NULL, lpds, NULL))
	return FALSE;

	if
		FAILED(IDirectSound_SetCooperativeLevel(*lpds, hwnd, DSSCL_PRIORITY))
	return FALSE;

	IDirectSound8* localDs = *lpds;
	LPDIRECTSOUNDBUFFER lpDSBuff;
	DSBUFFERDESC dsbd;
	memset(&dsbd, 0, sizeof(DSBUFFERDESC));
	dsbd.dwSize = sizeof(DSBUFFERDESC);
	dsbd.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRL3D;
	if (localDs->CreateSoundBuffer(&dsbd, &lpDSBuff, NULL) == 0) {
		// Make the primary 44.1 KHz so that it sounds better
		WAVEFORMATEX wfx;
		ZeroMemory(&wfx, sizeof(WAVEFORMATEX));
		wfx.wFormatTag = WAVE_FORMAT_PCM;
		wfx.nChannels = 1; //2;
		wfx.nSamplesPerSec = 22050; //44100
		wfx.nAvgBytesPerSec = 22050 * 2; //22050*2*2;
		wfx.nBlockAlign = 2; //4;
		wfx.wBitsPerSample = 16;
		wfx.cbSize = 0;
		HRESULT err = lpDSBuff->SetFormat(&wfx);
		if (err != 0) {
			AfxMessageBox(IDS_ERROR_DMUSICFMTSETFAILED);
		}
	}

	return TRUE;
}

BOOL CABD3DFunctionLib::GetMovieInterfaces(const char* pszFileName,
	LPDIRECTDRAW7 pDD,
	IDirectDrawSurface7** pSurface4Loc,
	IDirectDrawStreamSample** pSampleLoc,
	IAMMultiMediaStream** pSStreamLoc,
	IAMMultiMediaStream** pAMStreamLoc,
	IMediaStream** pPrimaryVidStreamLoc,
	IDirectDrawMediaStream** pDDStreamLoc,
	RECT* rectMov) {
	HRESULT hr = 0;
	IDirectDrawStreamSample* pSample;
	IAMMultiMediaStream* pSStream;
	//open str
	IAMMultiMediaStream* pAMStream;

	hr = CoCreateInstance(CLSID_AMMultiMediaStream, NULL, CLSCTX_INPROC_SERVER,
		IID_IAMMultiMediaStream, (void**)&pAMStream);
	hr = pAMStream->Initialize(STREAMTYPE_READ, 0, NULL);
	hr = pAMStream->AddMediaStream(pDD, &MSPID_PrimaryVideo, 0, NULL);
	hr = pAMStream->AddMediaStream(NULL, &MSPID_PrimaryAudio, AMMSF_ADDDEFAULTRENDERER, NULL);

	WCHAR wPath[MAX_PATH];
	MultiByteToWideChar(CP_ACP, 0, pszFileName, -1, wPath, sizeof(wPath) / sizeof(wPath[0]));

	hr = pAMStream->OpenFile(wPath, 0);
	if (hr != 0)
		return FALSE;

	pSStream = pAMStream;

	if (pAMStream == NULL)
		return FALSE;
	//close str
	IMediaStream* pPrimaryVidStream = NULL;
	IDirectDrawMediaStream* pDDStream = NULL;
	LPDIRECTDRAWSURFACE pSurface = NULL;
	LPDIRECTDRAWSURFACE7 pSurface4 = NULL;

	RECT rectLoc;

	hr = pSStream->GetMediaStream(MSPID_PrimaryVideo, &pPrimaryVidStream);
	hr = pPrimaryVidStream->QueryInterface(IID_IDirectDrawMediaStream, (void**)&pDDStream);

	hr = pDDStream->CreateSample(NULL, NULL, 0, &pSample);
	hr = pSample->GetSurface(&pSurface, &rectLoc);
	*rectMov = rectLoc;

	hr = pSStream->SetState(STREAMSTATE_RUN);

	hr = pSurface->QueryInterface(IID_IDirectDrawSurface7, (void**)&pSurface4);
	pSurface->Release();
	pSurface = 0;

	*pSurface4Loc = pSurface4;
	*pPrimaryVidStreamLoc = pPrimaryVidStream;
	*pSStreamLoc = pSStream;
	*pDDStreamLoc = pDDStream;
	*pAMStreamLoc = pAMStream;
	*pSampleLoc = pSample;

	return TRUE;
}

IDirectSoundBuffer* CABD3DFunctionLib::DSLoad3DSoundBuffer(IDirectSound* pDS, const char* lpName) {
	IDirectSoundBuffer* pDSB = NULL;
	DSBUFFERDESC dsBD = { 0 };
	BYTE* pbWaveData;
	void* pvBase;

	dsBD.dwSize = sizeof(dsBD);
	dsBD.dwFlags = DSBCAPS_STATIC | DSBCAPS_CTRL3D | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLFREQUENCY | DSBCAPS_LOCSOFTWARE | DSBCAPS_STICKYFOCUS;

	if (DSGetWaveResource(NULL, lpName, &dsBD.lpwfxFormat, &pbWaveData, &dsBD.dwBufferBytes)) {
		if (SUCCEEDED(IDirectSound_CreateSoundBuffer(pDS, &dsBD, &pDSB, NULL))) {
			if (!DSFillSoundBuffer(pDSB, pbWaveData, dsBD.dwBufferBytes)) {
				IDirectSoundBuffer_Release(pDSB);
				pDSB = NULL;
			}
		} else {
			pDSB = NULL;
		}
	} else if (DSGetWaveFile(NULL, lpName, &dsBD.lpwfxFormat, &pbWaveData,
				   &dsBD.dwBufferBytes, &pvBase)) {
		if (SUCCEEDED(IDirectSound_CreateSoundBuffer(pDS, &dsBD, &pDSB, NULL))) {
			if (!DSFillSoundBuffer(pDSB, pbWaveData, dsBD.dwBufferBytes)) {
				IDirectSoundBuffer_Release(pDSB);
				pDSB = NULL;
			}
		} else {
			pDSB = NULL;
		}
		UnmapViewOfFile(pvBase);
	}

	return pDSB;
}

BOOL CABD3DFunctionLib::DSGetWaveResource(HMODULE hModule, const char* lpName,
	WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize) {
	HRSRC hResInfo;
	HGLOBAL hResData;
	void* pvRes;

	if (((hResInfo = FindResource(hModule, KEP::Utf8ToUtf16(lpName).c_str(), c_szWAV)) != NULL) &&
		((hResData = LoadResource(hModule, hResInfo)) != NULL) &&
		((pvRes = LockResource(hResData)) != NULL) &&
		DSParseWaveResource(pvRes, ppWaveHeader, ppbWaveData, pcbWaveSize)) {
		return TRUE;
	}

	return FALSE;
}

BOOL CABD3DFunctionLib::DSFillSoundBuffer(IDirectSoundBuffer* pDSB, BYTE* pbWaveData, DWORD cbWaveSize) {
	if (pDSB && pbWaveData && cbWaveSize) {
		LPVOID pMem1, pMem2;
		DWORD dwSize1, dwSize2;

		if (SUCCEEDED(IDirectSoundBuffer_Lock(pDSB, 0, cbWaveSize,
				&pMem1, &dwSize1, &pMem2, &dwSize2, 0))) {
			CopyMemory(pMem1, pbWaveData, dwSize1);

			if (0 != dwSize2)
				CopyMemory(pMem2, pbWaveData + dwSize1, dwSize2);

			IDirectSoundBuffer_Unlock(pDSB, pMem1, dwSize1, pMem2, dwSize2);
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CABD3DFunctionLib::DSGetWaveFile(HMODULE hModule, const char* lpName,
	WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize,
	void** ppvBase) {
	void* pvRes;
	HANDLE hFile, hMapping;

	*ppvBase = NULL;

	hFile = CreateFile(KEP::Utf8ToUtf16(lpName).c_str(), GENERIC_READ, FILE_SHARE_READ,
		NULL, OPEN_EXISTING, 0, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;

	hMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if (hMapping == INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
		return FALSE;
	}

	CloseHandle(hFile);

	pvRes = MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0);
	if (pvRes == NULL) {
		CloseHandle(hMapping);
		return FALSE;
	}

	CloseHandle(hMapping);

	if (DSParseWaveResource(pvRes, ppWaveHeader, ppbWaveData, pcbWaveSize) == FALSE) {
		UnmapViewOfFile(pvRes);
		return FALSE;
	}

	*ppvBase = pvRes;
	return TRUE;
}

BOOL CABD3DFunctionLib::DSParseWaveResource(void* pvRes, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize) {
	DWORD* pdw;
	DWORD* pdwEnd;
	DWORD dwRiff;
	DWORD dwType;
	DWORD dwLength;

	if (ppWaveHeader)
		*ppWaveHeader = NULL;

	if (ppbWaveData)
		*ppbWaveData = NULL;

	if (pcbWaveSize)
		*pcbWaveSize = 0;

	pdw = (DWORD*)pvRes;
	dwRiff = *pdw++;
	dwLength = *pdw++;
	dwType = *pdw++;

	if (dwRiff != mmioFOURCC('R', 'I', 'F', 'F'))
		goto exit; // not even RIFF

	if (dwType != mmioFOURCC('W', 'A', 'V', 'E'))
		goto exit; // not a WAV

	pdwEnd = (DWORD*)((BYTE*)pdw + dwLength - 4);

	while (pdw < pdwEnd) {
		dwType = *pdw++;
		dwLength = *pdw++;

		switch (dwType) {
			case mmioFOURCC('f', 'm', 't', ' '):
				if (ppWaveHeader && !*ppWaveHeader) {
					if (dwLength < sizeof(WAVEFORMAT))
						goto exit; // not a WAV

					*ppWaveHeader = (WAVEFORMATEX*)pdw;

					if ((!ppbWaveData || *ppbWaveData) &&
						(!pcbWaveSize || *pcbWaveSize)) {
						return TRUE;
					}
				}
				break;

			case mmioFOURCC('d', 'a', 't', 'a'):
				if ((ppbWaveData && !*ppbWaveData) ||
					(pcbWaveSize && !*pcbWaveSize)) {
					if (ppbWaveData)
						*ppbWaveData = (LPBYTE)pdw;

					if (pcbWaveSize)
						*pcbWaveSize = dwLength;

					if (!ppWaveHeader || *ppWaveHeader)
						return TRUE;
				}
				break;
		}

		pdw = (DWORD*)((BYTE*)pdw + ((dwLength + 1) & ~1));
	}

exit:
	return FALSE;
}
