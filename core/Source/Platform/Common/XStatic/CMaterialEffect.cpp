///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "d3d9.h"
#include "CMaterialEffect.h"
#include "VertexDeclaration.h"
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "EffectRenderData.h"
#include "TextureObj.h"
#include "TextureDatabase.h"
#include "RenderEngine/Utils/ReUtils.h"

#define DEBUGMSG(...)

namespace KEP {

MaterialEffect::MaterialEffect() :
		m_effect(0) {
	memset(&m_technique, 0, sizeof(D3DXHANDLE));
	m_effect = NULL;
}

MaterialEffect::~MaterialEffect() {
}

EFFECT_MAP MaterialEffect::gEffectMap;

MaterialEffect* MaterialEffect::GetMaterialEffect(std::string effect, LPDIRECT3DDEVICE9 pDevice) {
	//	First see if this effect exists in the effect cache
	EFFECT_MAP::iterator iter = gEffectMap.find(effect);

	MaterialEffect* pEffect;

	if (gEffectMap.end() == iter) {
		//	Not found, create and load into map
		pEffect = new MaterialEffect();

#ifdef _DEBUG
		//std::string mEffectName = "C:/source/KEP/Deploy/gamecontentdbg/templates/clientbladesd/effects/";
		pEffect->mEffectName = "clientbladesd/effects/";
#else
		pEffect->mEffectName = "../templates/clientblades/effects/";
#endif

		pEffect->mEffectName += effect;

		if (pEffect->Create(pEffect->mEffectName.c_str(), pDevice))
			gEffectMap.insert(EFFECT_MAP::value_type(effect, pEffect));
		else
			return NULL;
	} else {
		//	Found, return cached
		pEffect = iter->second;
	}

	return pEffect;
}

bool MaterialEffect::Create(const char* filename, LPDIRECT3DDEVICE9 pDevice) {
	ID3DXBuffer* errors = 0;

	mpDevice = pDevice;

	DWORD flags = D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG;

	HRESULT hr;
	hr = D3DXCreateEffectFromFileW(
		mpDevice,
		Utf8ToUtf16(filename).c_str(),
		0,
		0,
		flags,
		0,
		&m_effect,
		&errors);

	if (FAILED(hr)) {
		if (errors) {
			char* errorString = (char*)errors->GetBufferPointer();
			DEBUGMSG("%s", errorString);
			printf("%s\n", errorString);
			errors->Release();
			errors = 0;
		} else {
			D3DCheck(hr);
		}
		//ASSERT(0);
		return false;
	}

	if (m_effect) {
		// might as well find a valid technique for this shader
		hr = m_effect->FindNextValidTechnique(0, &m_technique);

		if (FAILED(hr)) {
			DEBUGMSG("Unable to find valid technique for effect %s", filename);
			m_effect->Release();
			m_effect = 0;

			return false;
		}

		m_effect->SetTechnique(m_technique);
	}

	return true;
}

void MaterialEffect::SetTextures(EffectRenderData* pRenderData) {
	//	Lets just set texture stage 0 for now, since the material has no
	//	way of knowing how many textures exist. Setting the entire list
	//	of 10 seems a waste.
	DWORD texIndex = pRenderData->mpMaterial->m_texNameHash[0];
	CTextureEX* texture = pRenderData->mpTextureDatabase->TextureGet(texIndex);

	//	TODO: Multiple texture stage states.
	if (texture)
		m_effect->SetTexture("BaseTexture", texture->GetD3DTextureIfLoadedOrQueueToLoad());
}

bool MaterialEffect::Prepare(RawBoneData* pBoneData, EffectRenderData* pRenderData) {
	Matrix44f world, view, proj, wvp;

	static bool reload = false;

	if (reload || m_effect == NULL) {
		if (m_effect)
			m_effect->Release();

		if (!Create(mEffectName.c_str(), mpDevice)) {
			m_effect = NULL;
			return false;
		}

		reload = false;
	}

	//	Set Textures
	SetTextures(pRenderData);

	//	If we have new bone data, we need to re-send this data to the card
	if (mPrevBoneData != pBoneData && pBoneData != NULL) {
		int numBones = pBoneData->mNumBones;

		D3DCheck(m_effect->SetMatrixArray("mBones", (const D3DXMATRIX*)pBoneData->mBoneMatrices, numBones));
	}

	//mpDevice->GetTransform(D3DTS_WORLD, (D3DXMATRIX *)&world);

	for (auto r = 0; r < 4; r++) {
		for (auto c = 0; c < 4; c++) {
			world[r][c] = pRenderData->mpWorldTransform->m[r][c];
		}
	}

	mpDevice->GetTransform(D3DTS_VIEW, (D3DXMATRIX*)&view);
	mpDevice->GetTransform(D3DTS_PROJECTION, (D3DXMATRIX*)&proj);

	wvp = world * view * proj;

	D3DCheck(m_effect->SetMatrix("worldViewProj", (D3DXMATRIX*)&wvp));

	//SetMatrix("WorldViewProj", wvp);
	//SetMatrix("worldViewProj", wvp);
	SetMatrix("wvpMatrix", wvp);
	SetMatrix("mvpMatrix", wvp);

	Matrix44f wv = world * view;
	SetMatrix("worldViewMatrix", wvp);

	wv = InverseOf(wv);
	SetMatrix("worldViewMatrixI", wv);

	SetMatrix("viewMatrix", view);

	view = InverseOf(view);
	SetMatrix("viewInverse", view);
	SetMatrix("viewIMatrix", view);
	SetMatrix("viewInverseMatrix", view);

	SetMatrix("world", world);
	SetMatrix("worldMatrix", world);

	world = InverseOf(world);
	SetMatrix("worldIMatrix", world);
	SetMatrix("worldInverse", world);

	world.Transpose();
	SetMatrix("worldInverseTranspose", world);

	return true;
}

int MaterialEffect::Begin() {
	UINT passes;

	if (m_effect) {
		m_effect->Begin(&passes, 0);
		return passes;
	} else {
		return 0;
	}
}

void MaterialEffect::BeginPass(unsigned int pass) {
	if (m_effect)
		m_effect->BeginPass(pass);
}

void MaterialEffect::EndPass() {
	if (m_effect)
		m_effect->EndPass();
}

void MaterialEffect::End() {
	if (m_effect)
		m_effect->End();
}

void MaterialEffect::CommitChanges() {
	if (m_effect)
		m_effect->CommitChanges();
}

bool MaterialEffect::SetMatrix(const char* parameter, const Matrix44f& m) {
	D3DXMATRIX d3dm;

	d3dm._11 = m(0, 0);
	d3dm._12 = m(0, 1);
	d3dm._13 = m(0, 2);
	d3dm._14 = m(0, 3);
	d3dm._21 = m(1, 0);
	d3dm._22 = m(1, 1);
	d3dm._23 = m(1, 2);
	d3dm._24 = m(1, 3);
	d3dm._31 = m(2, 0);
	d3dm._32 = m(2, 1);
	d3dm._33 = m(2, 2);
	d3dm._34 = m(2, 3);
	d3dm._41 = m(3, 0);
	d3dm._42 = m(3, 1);
	d3dm._43 = m(3, 2);
	d3dm._44 = m(3, 3);

	HRESULT hr = m_effect->SetMatrix(parameter, &d3dm);
	return SUCCEEDED(hr);
}

bool MaterialEffect::SetVector(const char* parameter, const Vector4f& v) {
	//	D3DXHANDLE h = m_effect->GetParameterByName(0, parameter);
	//	ASSERT(h);

	D3DXVECTOR4 v4;
	v4.x = v.x;
	v4.y = v.y;
	v4.z = v.z;
	v4.w = v.w;
	HRESULT hr = m_effect->SetVector(parameter, &v4);
	return SUCCEEDED(hr);
}

bool MaterialEffect::SetVector(const char* parameter, const Vector3f& v) {
	return SetVector(parameter, Vector4f(v.x, v.y, v.z, 1.0f));
}

bool MaterialEffect::SetFloat(const char* parameter, float f) {
	HRESULT hr = m_effect->SetFloat(parameter, f);
	return SUCCEEDED(hr);
}

bool MaterialEffect::SetInt(const char* parameter, unsigned int dw) {
	HRESULT hr = m_effect->SetInt(parameter, dw);
	return SUCCEEDED(hr);
}

bool MaterialEffect::SetTexture(const char* parameter, LPDIRECT3DTEXTURE9 t) {
	HRESULT hr = m_effect->SetTexture(parameter, t);
	return SUCCEEDED(hr);
}

bool MaterialEffect::SetColor(const char* parameter, const D3DCOLOR& color) {
	return SUCCEEDED(m_effect->SetValue(parameter, (LPVOID)&color, sizeof(D3DCOLOR)));
}

void MaterialEffect::OnLostDevice() {
	if (m_effect)
		m_effect->OnLostDevice();
}

void MaterialEffect::OnResetDevice() {
	if (m_effect)
		m_effect->OnResetDevice();
}

} // namespace KEP
