///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#ifdef XSTATIC_BUILD
#include ".\stdafx.h"
#else
#include <windows.h>
#endif

#include "PreciseTime.h"

#include "FlashMesh.h"

#include "Core/Util/WindowsAPIWrappers.h"

#include "KEPConfig.h"
#include "KEPException.h"
#include "..\..\Tools\zlib\zlib.h"
#include "..\..\RenderEngine\ReD3DX9.h"
#include "..\..\RenderEngine\Utils\ReUtils.h"
#include "SetThreadName.h"
#include "common\include\MediaParams.h"
#include "Core/Math/KEPMathUtil.h"
#include "Core/Util/ParameterBuffer.h"

#include "common\include\MediaParams.h"
#include "FlashControlProxy.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

bool FlashMesh::s_enabledGlobal = true;
double FlashMesh::s_volPctGlobal = 100.0;
std::set<IFlashControlProxy*> FlashMesh::s_FlashControlProxys;
std::mutex FlashMesh::s_mtxFlashControlProxys;

// RAII Access For Texture DC
class TextureDC {
public:
	TextureDC(LPDIRECT3DTEXTURE9 pTex) {
		m_pSurface = NULL;
		m_hSurfaceDC = NULL;
		memset(&m_d3dsd, 0, sizeof(m_d3dsd));
		Create(pTex);
	}
	~TextureDC() {
		Clear();
	}
	bool Create(LPDIRECT3DTEXTURE9 pTex) {
		Clear();
		if (!pTex)
			return false;

		if (pTex->GetSurfaceLevel(0, &m_pSurface) == D3D_OK && m_pSurface) {
			if (D3D_OK == pTex->GetLevelDesc(0, &m_d3dsd)) {
				if (m_pSurface->GetDC(&m_hSurfaceDC) == D3D_OK && m_hSurfaceDC)
					return true;
			}
		}
		Clear();
		return false;
	}
	HDC GetDC() {
		return m_hSurfaceDC;
	}
	uint32_t GetWidth() {
		return (uint32_t)m_d3dsd.Width;
	}
	uint32_t GetHeight() {
		return (uint32_t)m_d3dsd.Height;
	}
	void Clear() {
		if (m_hSurfaceDC) {
			m_pSurface->ReleaseDC(m_hSurfaceDC);
			m_hSurfaceDC = NULL;
		}
		if (m_pSurface) {
			m_pSurface->Release();
			m_pSurface = NULL;
		}
		memset(&m_d3dsd, 0, sizeof(m_d3dsd));
	}

private:
	D3DSURFACE_DESC m_d3dsd;
	IDirect3DSurface9* m_pSurface;
	HDC m_hSurfaceDC;
};

static uint32_t GetYouTubeControlsHeight(uint32_t iFlashWindowHeight) {
	return (iFlashWindowHeight / 30) * 8;
}

FlashMesh::FlashMesh() {
}

FlashMesh::~FlashMesh() {
	LogInfo(StrThis() << " DESTRUCT");
	FlashMeshesDel(m_pFlashControlProxy.get());
	m_pFlashControlProxy->StopProcessing(); // Does not wait for thread exit. m_pFlashControlProxy remains alive until thread exits.
	m_pFlashControlProxy.reset();
}

unsigned int FlashMesh::GetMeshId() const {
	return m_pFlashControlProxy ? m_pFlashControlProxy->GetMeshId() : 0;
}

std::string FlashMesh::StrThis(bool verbose) const {
	std::string str;
	if (verbose) {
		StrAppend(str, "FM<" << GetMeshId() << " " << m_mediaParams.ToStr(true) << ">");
	} else {
		StrAppend(str, "FM<" << GetMeshId() << ">");
	}
	return str;
}

bool FlashMesh::Initialize(
	IFlashSystem* pFlashSystem,
	HWND parent,
	const MediaParams& mediaParams,
	bool wantInput,
	int32_t x, int32_t y,
	uint32_t width, uint32_t height) {
	LogInfo(mediaParams.ToStr(true));

	if (!pFlashSystem)
		return false;

	m_pFlashControlProxy = pFlashSystem->CreateFlashControlProxy();
	FlashMeshesAdd(m_pFlashControlProxy.get());

	// Default Resolution (half 720p)
	static const uint32_t kiDefaultFlashWidth = 640;
	static const uint32_t kiDefaultFlashHeight = 480;

	IExternalMesh::SetMediaParams(mediaParams);

	// Validate parameters. Use default size if invalid values passed in.
	if (width == 0 || width > (1 << 12))
		width = kiDefaultFlashWidth;
	if (height == 0 || height > (1 << 12))
		height = kiDefaultFlashHeight;

	bool bIsYouTube = GetMediaParams().IsUrlYouTube();
	uint32_t iYCrop = bIsYouTube ? GetYouTubeControlsHeight(height) / 2 : 0;
	IFlashControlProxy::InitializeArgs args;
	args.m_bWantInput = wantInput;
	args.m_hwndParent = parent;
	args.m_fLocalVolume = GetMediaParams().GetVolume() * .01f; // mediaparams volume is in percent
	args.m_iPositionX = x;
	args.m_iPositionY = y;
	args.m_iWidth = width;
	args.m_iHeight = height;
	args.m_iCropTop = iYCrop;
	args.m_iCropLeft = 0;
	args.m_iCropBottom = iYCrop;
	args.m_iCropRight = 0;
	args.m_strParams = GetMediaParams().GetParams();
	args.m_strURL = GetMediaParams().GetUrl();
	args.m_eAllowScriptAccess = GetMediaParams().GetAllowScriptAccess();
	if (!m_pFlashControlProxy->Initialize(args))
		return false;

	return true;
}

// static
ReMaterialPtr FlashMesh::CreateReMaterial(ReD3DX9DeviceState* dev) {
	if (!dev)
		return nullptr;

	ReRenderStateData data;
	data.Mat.Diffuse.x = 1.f;
	data.Mat.Diffuse.y = 1.f;
	data.Mat.Diffuse.z = 1.f;
	data.Mat.Diffuse.w = 1.f;
	data.Mat.Ambient.x = 1.f;
	data.Mat.Ambient.y = 1.f;
	data.Mat.Ambient.z = 1.f;
	data.Mat.Ambient.w = 1.f;
	data.Mat.Specular.x = .0f;
	data.Mat.Specular.y = .0f;
	data.Mat.Specular.z = .0f;
	data.Mat.Specular.w = .0f;
	data.Mat.Emissive.x = 1.f;
	data.Mat.Emissive.y = 1.f;
	data.Mat.Emissive.z = 1.f;
	data.Mat.Emissive.w = 0.f;
	data.Mat.Power = 10.f;

	data.Blend.Srcblend = 9;
	data.Blend.Dstblend = 2;

	data.ColorSrc = dev->GetRenderState()->GetPreLighting() ? ReColorSource::VERTEX : ReColorSource::MATERIAL;

	data.Mode.Alphaenable = 0;
	data.Mode.Zenable = 1;
	data.Mode.Shademode = 0;
	data.Mode.Zwriteenable = 1;
	data.Mode.Alphatestenable = 0;
	data.Mode.CullMode = D3DCULL_CCW;
	data.Mode.ZBias = 0;

	data.MatHash = ReHash((BYTE*)&data.Mat, sizeof(ReMatProps), 0);
	data.ModeHash = ReHash((BYTE*)&data.Mode, sizeof(ReModeProps), 0);
	data.BlendHash = ReHash((BYTE*)&data.Blend, sizeof(ReBlendProps), 0);

	data.BlendHash &= 0x7fffffff;

	for (UINT i = 0; i < 4; i++) {
		data.TexPtr[i] = NULL;
		data.ResPtr[i] = NULL;

		data.Tex[i].ColorOp = D3DTOP_DISABLE;
		data.Tex[i].AlphaOp = D3DTOP_DISABLE;
		data.Tex[i].ColorArg1 = D3DTA_TEXTURE;
		data.Tex[i].ColorArg2 = D3DTA_CURRENT;
	}

	data.NumTexStages = 1;
	data.Tex[0].ColorOp = D3DTOP_SELECTARG1;

	data.TexHash[0] = ReHash((BYTE*)&data.Tex[0], sizeof(ReTexProps), 0);

	return ReMaterialPtr(new ReD3DX9Material(dev, &data));
}

// Updates the D3D texture owned by base class IExternalMesh (accessed with GetTexture() call)
// to be the given size. If already the correct size, nothing is done. If fails to set the size,
// the texture is set to NULL.
void FlashMesh::UpdateD3DTextureToSize(ReD3DX9DeviceState* dev, uint32_t width, uint32_t height) {
	// Texture Change ?
	LPDIRECT3DTEXTURE9 pTex = GetTexture();
	if (pTex && width == m_texWidth && height == m_texHeight)
		return;

	// Create New Texture
	IDirect3DTexture9* pTexNew = nullptr;
	HRESULT hr = D3DXCreateTexture(
		dev->GetDevice(),
		(UINT)width,
		(UINT)height,
		1,
		0,
		D3DFMT_R5G6B5,
		D3DPOOL_MANAGED,
		&pTexNew);

	SetTexture(pTexNew);

	if (hr == S_OK) {
		m_texWidth = width;
		m_texHeight = height;
	} else {
		m_texWidth = 0;
		m_texHeight = 0;
	}

	// Valid Texture ?
	if (!pTexNew) {
		static Timer timer;
		if (timer.ElapsedMs() > 5000) {
			timer.Reset();
			LogError(StrThis() << " D3DXCreateTexture() FAILED - hr=" << numToString(hr));
		}
	}
}

// Updates the D3D texture owned by base class IExternalMesh (accessed with GetTexture() call)
// to contain the pixel data provided by the flash control.
void FlashMesh::UpdateD3DTextureWithAvailableData(ReD3DX9DeviceState* dev) {
	// create the ReMaterial if not created yet
	bool bNeedToInitializeTexture = false;
	if (!m_ReMat) {
		m_ReMat = CreateReMaterial(dev);
		if (!m_ReMat)
			return;

		bNeedToInitializeTexture = true; // Texture needs to be initialized.
	}

	FlashWindowBitmap flashWindowBitmap;
	void* pBitmapBits = 0;
	uint32_t bitmapWidth, bitmapHeight;
	if (!(m_pFlashControlProxy->GetAvailableBitmap(&pBitmapBits, &bitmapWidth, &bitmapHeight))) {
		if (bNeedToInitializeTexture) {
			static uint16_t aBlackBits[16] = { 0 };
			pBitmapBits = aBlackBits;
			bitmapWidth = 4;
			bitmapHeight = 4;
		}
	}

	// Creates a DC to access a D3D texture.
	if (!pBitmapBits)
		return;

	// Update (or create) our texture with desired size.
	UpdateD3DTextureToSize(dev, bitmapWidth, bitmapHeight);
	IDirect3DTexture9* pTex = GetTexture();

	if (pTex) {
		D3DLOCKED_RECT lockedRect;
		HRESULT hr = pTex->LockRect(0, &lockedRect, NULL, D3DLOCK_DISCARD);
		if (hr == D3D_OK) {
			int numberOfBytesToCopy = bitmapWidth * 2;
			int numberOfBytesToStepForwardInSrc = (numberOfBytesToCopy + 3) & ~3;
			if ((numberOfBytesToCopy == numberOfBytesToStepForwardInSrc) && (numberOfBytesToCopy == lockedRect.Pitch)) {
				memcpy((unsigned char*)(lockedRect.pBits), (unsigned char*)pBitmapBits, sizeof(unsigned char) * bitmapHeight * bitmapWidth * 2);
			} else {
				for (int i = 0; i < bitmapHeight; i++)
					memcpy((unsigned char*)(lockedRect.pBits) + (i * lockedRect.Pitch), (unsigned char*)pBitmapBits + (i * numberOfBytesToStepForwardInSrc), sizeof(unsigned char) * numberOfBytesToCopy);
			}
			pTex->UnlockRect(0);
		} else {
			SetTexture(nullptr);
		}
	}

	m_ReMat->Update(GetTextureShared(), 0);
}

void FlashMesh::UpdateBitmapAsync() {
	m_pFlashControlProxy->UpdateBitmap();
}

// static
bool FlashMesh::GetMovieSize(const std::string& filePath, uint32_t& width, uint32_t& height) {
	width = height = 0;

	auto getBits = [](BYTE* buff, int& currentBit, int bits) -> ULONG {
		int i = 0;
		int rtn = 0;

		for (i = currentBit; i < currentBit + bits; i++) {
			if ((buff[i / 8] & (1 << (7 - (i % 8)))) > 0) {
				rtn += 1 << (bits - (i - currentBit) - 1);
			}
		}
		currentBit += bits;
		return rtn;
	};

	typedef struct {
		BYTE f_magic[3]; // 'FWS' or 'CWS'
		BYTE f_version;
		unsigned long f_file_length;
	} swf_header;

	FILE* f = 0;
	if (fopen_s(&f, filePath.c_str(), "rb") != 0 || !f) {
		LogError("FILE NOT FOUND - '" << filePath << "'");
		return false;
	}

	const ULONG TWIP = 20;
	bool ret = false;
	swf_header h;
	const ULONG BUFF_SIZE = 1024;
	BYTE buff[BUFF_SIZE];
	BYTE dest[BUFF_SIZE];
	unsigned long len = BUFF_SIZE;
	BYTE* decodedBuff = buff;

	// read the header
	fread(&h, 1, sizeof(h), f);

	// read 1K, just to get the rest
	ULONG readBytes = (ULONG)fread(buff, 1, BUFF_SIZE, f);

	if (h.f_magic[1] == 'W' && h.f_magic[2] == 'S') {
		if (h.f_magic[0] == 'C') { // compressed
			int zret = uncompress(dest, &len, buff, readBytes);
			if (zret == Z_OK || zret == Z_BUF_ERROR) {
				decodedBuff = dest;
				ret = true;
			}
		} else if (h.f_magic[0] == 'F') {
			ret = true;
		}

		if (ret) {
			int currentBits = 0;
			ULONG b = getBits(decodedBuff, currentBits, 5);
			ULONG minX = getBits(decodedBuff, currentBits, b) / TWIP;
			ULONG maxX = getBits(decodedBuff, currentBits, b) / TWIP;
			ULONG minY = getBits(decodedBuff, currentBits, b) / TWIP;
			ULONG maxY = getBits(decodedBuff, currentBits, b) / TWIP;

			LONG delX = (LONG)maxX - (LONG)minX;
			LONG delY = (LONG)maxY - (LONG)minY;
			width = (delX >= 0) ? delX : -delX;
			height = (delY >= 0) ? delY : -delY;
			LogInfo("size=[" << width << " x " << height << "] '" << filePath << "'");
		}
	}

	fclose(f);

	return ret;
}

///////////////////////////////////////////////////////////////////////////////
// IExternalMesh Interface
///////////////////////////////////////////////////////////////////////////////

bool FlashMesh::SetMediaParams(const MediaParams& mediaParams, bool allowRewind) {
	// Rewind Same Url And Params ?
	// This is never true. mediaParams include a track number option which is incremented when a URL repeats.
	bool rewind = allowRewind && m_mediaParams.IsUrlAndParamsSame(mediaParams);

	// Set Super Class Media Params
	IExternalMesh::SetMediaParams(mediaParams);

	LogInfo(mediaParams.ToStr(true));

	// Send message to flash thread.
	if (rewind)
		m_pFlashControlProxy->Rewind();
	else
		m_pFlashControlProxy->SetMediaParams(GetMediaParams().GetParams(), GetMediaParams().GetUrl(), GetMediaParams().GetAllowScriptAccess());
	return true;
}

void FlashMesh::SetFocus() {
	m_pFlashControlProxy->SetFocus();
}

bool FlashMesh::SetVolume(double volPct) {
	IExternalMesh::SetVolume(volPct);
	m_pFlashControlProxy->SetLocalVolume(GetVolume() * .01f);
	return true;
}

bool FlashMesh::UpdateRect(int32_t x, uint32_t width, int32_t y, uint32_t height) {
	m_pFlashControlProxy->UpdateRect(x, y, width, height, 0, 0, 0, 0);
	return true;
}

void FlashMesh::Delete() {
	LogInfo(StrThis() << " OK");
	delete this;
}

///////////////////////////////////////////////////////////////////////////////
// Static Functions
///////////////////////////////////////////////////////////////////////////////

// static
FlashMesh* FlashMesh::Create(
	IFlashSystem* pFlashSystem,
	HWND parent,
	const MediaParams& mediaParams,
	uint32_t width, uint32_t height,
	bool loop, bool wantInput,
	int32_t x, int32_t y) {
	LogInstance("Instance");

	// Create New Flash Mesh
	FlashMesh* pFM = new FlashMesh;
	if (!pFM)
		return NULL;

	// Initialize Flash Mesh With Media Parameters
	if (!pFM->Initialize(pFlashSystem, parent, mediaParams, wantInput, x, y, width, height)) {
		LogError("Initialize() FAILED - " << mediaParams.ToStr());
		delete pFM;
		return NULL;
	}

	return pFM;
}

// static
size_t FlashMesh::FlashMeshesNum() {
	std::lock_guard<std::mutex> lock(s_mtxFlashControlProxys);
	return s_FlashControlProxys.size();
}

// static
void FlashMesh::FlashMeshesLog() {
	// Log All Flash Meshes
	std::lock_guard<std::mutex> lock(s_mtxFlashControlProxys);
	LogInfo("meshes=" << FlashMeshesNum());
	for (const IFlashControlProxy* pFCW : s_FlashControlProxys) {
		if (!pFCW)
			continue;
		LogInfo(" ... " << pFCW->GetIdString(true));
	}
}

// static
bool FlashMesh::FlashMeshesAdd(IFlashControlProxy* pFCW) {
	if (!pFCW)
		return false;

	std::lock_guard<std::mutex> lock(s_mtxFlashControlProxys);
	s_FlashControlProxys.insert(pFCW);
	return true;
}

// static
bool FlashMesh::FlashMeshesDel(IFlashControlProxy* pFCW) {
	std::lock_guard<std::mutex> lock(s_mtxFlashControlProxys);
	return s_FlashControlProxys.erase(pFCW) != 0;
}

} // namespace KEP