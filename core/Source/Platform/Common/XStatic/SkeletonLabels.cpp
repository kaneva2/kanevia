///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SkeletonLabels.h"

#include "IClientEngine.h"
#include "CTextRenderClass.h"
#include "Common/Include/IMemSizeGadget.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

SkeletonLabels::SkeletonLabels() {
}

SkeletonLabels::~SkeletonLabels() {
	// Reset All Overhead Labels (CTextRenderObj)
	reset();
}

void SkeletonLabels::reset() {
	// Reset All Overhead Labels (CTextRenderObj)
	size_t labels = m_labels.size();
	for (size_t i = 0; i < labels; i++) {
		if (m_labels[i].pTRO) {
			m_labels[i].pTRO->SafeDelete();
			delete m_labels[i].pTRO;
			m_labels[i].pTRO = NULL;
		}
	}
	m_labels.clear();
}

void SkeletonLabels::render(const Vector3f& displayPos) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	const double labelOffset = -0.05; // moves all labels (-down, +up)
	const double labelSpace = 0.005; // space between labels
	const double labelMaxDist = 2000.0; // dist which object is not visible
	const double labelMinDist = 0.0;

	// DRF - ED-632 - Label Text Move Closer
	// Calculate Total Labels Size
	double totSize = -labelSpace;
	size_t labels = m_labels.size();
	for (size_t i = 0; i < labels; ++i) {
		std::string labelText = m_labels[i].displayText;
		if (labelText.empty())
			continue;
		totSize += (FontSize(i) + labelSpace);
	}

	// Render All Overhead Labels (CTextRenderObj)
	double offset = labelOffset + totSize;
	for (size_t i = 0; i < labels; ++i) {
		std::string labelText = m_labels[i].displayText;
		if (labelText.empty())
			continue;
		pICE->DisplayTextBasedOn3dPosition(
			displayPos,
			m_cameraMatrix,
			labelText,
			labelMaxDist,
			labelMinDist,
			FontSize(i),
			m_labels[i].pTRO,
			m_labels[i].color,
			offset);
		offset -= (FontSize(i) + labelSpace);
	}
}

void SkeletonLabels::addLabel(const std::string& textStr, double fontSize, const Vector3d& color, unsigned int position) {
	// Labels Require Text
	if (textStr.empty())
		return;

	// Create New Overhead Label
	LabelStruct newGuy(textStr, fontSize, color);

	// Add Label To Label Stack
	if (position == SL_POS_END || position >= m_labels.size())
		m_labels.push_back(newGuy);
	else
		m_labels.insert(m_labels.begin() + position, newGuy);
}

void SkeletonLabels::setCameraMatrix(const Matrix44f& cameraMatrix) {
	m_cameraMatrix = cameraMatrix;
}

void SkeletonLabels::addLabels(const SkeletonLabels& skeletonLabels) {
	for (auto it = skeletonLabels.m_labels.begin(); it != skeletonLabels.m_labels.end(); ++it)
		addLabel(it->displayText, it->fontSize, it->color, SL_POS_END);
}

bool SkeletonLabels::removeByText(const std::string& textStr) {
	bool found = false;
	std::vector<LabelStruct>::iterator foundAt;
	for (auto it = m_labels.begin(); it != m_labels.end(); it++) {
		if (it->displayText == textStr) {
			it->pTRO->SafeDelete();
			delete it->pTRO;
			it->pTRO = NULL;
			foundAt = it;
			found = true;
			break;
		}
	}
	if (found)
		m_labels.erase(foundAt);

	return found;
}

void SkeletonLabels::LabelStruct::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(displayText);
		pMemSizeGadget->AddObject(pTRO);
	}
}

void SkeletonLabels::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_labels);
	}
}

} // namespace KEP
