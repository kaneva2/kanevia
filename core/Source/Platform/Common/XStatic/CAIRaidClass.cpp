///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CAIRaidClass.h"
#include <KEPHelpers.h>

namespace KEP {

//---------------------------------
CAICfgOption::CAICfgOption() {
	m_aiCfg = 0;
}

void CAICfgOptionList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAICfgOption* spnPtr = (CAICfgOption*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CAICfgOption::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_aiCfg;

	} else {
		ar >> m_aiCfg;
	}
}
//-------------------------------
CAISpawnPoint::CAISpawnPoint() {
	m_position.Set(0, 0, 0);
	m_channel.clear();
	m_spawnCfg = 0;
	m_cfgList = NULL; //will randomize spawn
	m_commander = FALSE;
}

void CAISpawnPoint::SafeDelete() {
	if (m_cfgList) {
		m_cfgList->SafeDelete();
		delete m_cfgList;
		m_cfgList = 0;
	}
}

void CAISpawnPointList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAISpawnPoint* spnPtr = (CAISpawnPoint*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CAISpawnPoint::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_channel
		   << m_spawnCfg
		   << m_cfgList
		   << m_commander
		   << m_position.x
		   << m_position.y
		   << m_position.z;

	} else {
		ar >> m_channel >> m_spawnCfg >> m_cfgList >> m_commander >> m_position.x >> m_position.y >> m_position.z;
	}
}

//-------------------------------
CAIRaidObj::CAIRaidObj() {
	m_raidDuration = 600000;
	m_raidManditoryWait = 1200000;
	m_bonusPoints = 1.0f;
	m_spawnPoints = NULL;
	m_raidAlertRadius = 4000.0f;
	m_raidAlertChannel.clear();
	m_percentSpawn = 100; //100 max
	m_raidAlertPosition.Set(0, 0, 0);
	m_lastRaidTimeStamp = fTime::TimeMs();
	m_manualStart = TRUE;
}

void CAIRaidObj::SafeDelete() {
	if (m_spawnPoints) {
		m_spawnPoints->SafeDelete();
		delete m_spawnPoints;
		m_spawnPoints = 0;
	}
}

void CAIRaidObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIRaidObj* spnPtr = (CAIRaidObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

CAIRaidObj* CAIRaidObjList::CallBack() {
	TimeMs timeMs = fTime::TimeMs();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIRaidObj* rdPtr = (CAIRaidObj*)GetNext(posLoc);
		if (!rdPtr->m_manualStart) { // don't do manual ones
			if ((timeMs - rdPtr->m_lastRaidTimeStamp) > rdPtr->m_raidManditoryWait) {
				if ((rand() % 100) < rdPtr->m_percentSpawn) {
					rdPtr->m_lastRaidTimeStamp = timeMs;
					return rdPtr;
				}
			}
		}
	}
	return NULL;
}

CAIRaidObj* CAIRaidObjList::FindByName(const char* name) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAIRaidObj* rdPtr = (CAIRaidObj*)GetNext(posLoc);

		if (rdPtr->m_raidName.CompareNoCase(name) == 0) {
			return rdPtr;
		}
	}
	return 0;
}

void CAIRaidObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_raidName
		   << m_raidDuration
		   << (int)m_raidManditoryWait
		   << m_bonusPoints
		   << m_spawnPoints
		   << m_raidAlertMessage
		   << m_raidAlertRadius
		   << m_raidAlertChannel
		   << m_raidAlertPosition.x
		   << m_raidAlertPosition.y
		   << m_raidAlertPosition.z
		   << m_percentSpawn
		   << m_manualStart;

	} else {
		m_manualStart = TRUE;
		int version = ar.GetObjectSchema();
		int manWait;
		ar >> m_raidName >> m_raidDuration >> manWait >> m_bonusPoints >> m_spawnPoints >> m_raidAlertMessage >> m_raidAlertRadius >> m_raidAlertChannel >> m_raidAlertPosition.x >> m_raidAlertPosition.y >> m_raidAlertPosition.z >> m_percentSpawn;
		m_raidManditoryWait = manWait;
		if (version > 1)
			ar >> m_manualStart;

		m_lastRaidTimeStamp = fTime::TimeMs();
	}
}

IMPLEMENT_SERIAL_SCHEMA(CAICfgOption, CObject)
IMPLEMENT_SERIAL_SCHEMA(CAICfgOptionList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CAISpawnPoint, CObject)
IMPLEMENT_SERIAL_SCHEMA(CAISpawnPointList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CAIRaidObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CAIRaidObjList, CKEPObList)

BEGIN_GETSET_IMPL(CAICfgOption, IDS_AICFGOPTION_NAME)
GETSET_RANGE(m_aiCfg, gsInt, GS_FLAGS_DEFAULT, 0, 0, AICFGOPTION, AICFG, INT_MIN, INT_MAX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CAIRaidObj, IDS_AIRAIDOBJ_NAME)
GETSET_MAX(m_raidName, gsCString, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, RAIDNAME, STRLEN_MAX)
GETSET_RANGE(m_raidDuration, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, RAIDDURATION, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_raidManditoryWait, gsDouble, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, RAIDMANDITORYWAIT, DOUBLE_MIN, DOUBLE_MAX)
GETSET_RANGE(m_bonusPoints, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, BONUSPOINTS, FLOAT_MIN, FLOAT_MAX)
GETSET_OBLIST_PTR(m_spawnPoints, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, SPAWNPOINTS)
GETSET_MAX(m_raidAlertMessage, gsCString, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, RAIDALERTMESSAGE, STRLEN_MAX)
GETSET_RANGE(m_raidAlertRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, RAIDALERTRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET(m_raidAlertChannel, gsLong, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, RAIDALERTCHANNEL)
GETSET(m_raidAlertPosition, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, RAIDALERTPOSITION)
GETSET_RANGE(m_percentSpawn, gsInt, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, PERCENTSPAWN, INT_MIN, INT_MAX)
GETSET(m_manualStart, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, AIRAIDOBJ, MANUALSTART)
END_GETSET_IMPL

} // namespace KEP