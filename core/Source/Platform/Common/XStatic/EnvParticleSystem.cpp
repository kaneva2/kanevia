///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EnvParticleSystem.h"
#include "KEPHelpers.h"
#include "Event/Glue/ParticleSysIds.h"
#include "CArchiveHelper.h"
#include "ContentService.h"
#include "IClientEngine.h"
#include "TextureDatabase.h"
#include "TextureObj.h"
#include "KEPFileNames.h"
#include <d3d9.h>
#include <d3dx9.h>
#include "TextureProvider.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

unsigned short EnvParticleSystem::ms_magicHeader = 0x5350; // "PS"
std::string EnvParticleSystem::mResourceStore;
TypedResourceManager<EnvParticleSystem>* TypedResourceManager<EnvParticleSystem>::ms_pInstance = nullptr;

EnvParticleSystem::EnvParticleSystem(ResourceManager* library, UINT64 resKey, const std::string& name) :
		IResource(library, resKey),
		m_def(new EffectDef()),
		m_systemName(name) {
	SetState(IResource::State::INITIALIZED_NOT_LOADED);
}

EnvParticleSystem::EnvParticleSystem(const EnvParticleSystem& ps) :
		IResource(ps) {
	m_systemName = ps.m_systemName;
	m_def = new EffectDef(*ps.m_def);
}

EnvParticleSystem::~EnvParticleSystem() {
	if (m_def != NULL)
		delete m_def;
	m_def = NULL;
}

bool EnvParticleSystem::CreateResource() {
	auto pRM = dynamic_cast<ParticleRM*>(GetResourceManager());
	if (!GetRawBuffer() || !pRM || !m_def)
		return false;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	bool ret = false;

	CMemFile file;
	file.Attach(GetRawBuffer(), GetRawSize());
	CArchive ar(&file, CArchive::load);

	try {
		Read(ar);
		ContentMetadata md;

		if (pICE->GetCachedUGCMetadata((GLID)GetHashKey(), md)) {
			std::vector<std::string> runtimeTextureKeys;
			std::vector<std::string> runtimeTextureName;

			auto pTDB = pICE->GetTextureDataBase();
			if (!pTDB)
				return false;

			UGCItemTextureProvider customTextureProvider(md);

			for (const auto& sTexKey : m_def->particle.textures.handles) {
				assert(!sTexKey.empty() && sTexKey[0] != '-');
				if (sTexKey.empty() || sTexKey[0] == '-') {
					continue;
				}

				unsigned texKey = strtoul(sTexKey.c_str(), nullptr, 10);
				std::string texUrl = customTextureProvider.getUrl(texKey, false);

				// customTexturePath is a URL.  Get the filename from it.
				std::string texFileName = texUrl;
				int p = texFileName.find_last_of("\\/");
				if (p != std::string::npos) {
					// i is guaranteed not to be npos because the string starts with http://
					texFileName = texFileName.substr(p + 1);
				}

				// Add Texture To Texture Database
				DWORD nameHash = INVALID_TEXTURE_NAMEHASH;
				pTDB->TextureAdd(nameHash, texFileName, "", texUrl);
				if (!ValidTextureNameHash(nameHash)) {
					LogError("AddTextureToDatabase() FAILED - texName='" << texFileName << "' texUrl='" << texUrl << "'");
				} else {
					std::stringstream ss;
					ss << nameHash;
					runtimeTextureKeys.push_back(ss.str());
					runtimeTextureName.push_back(texFileName);
					if (texKey != 0) {
						CTextureEX* pTex = pTDB->TextureGet(nameHash);
						if (pTex) {
							//update encryption hash
							pTex->SetEncryptionHashOverride(texKey);
						}
					}
				}
			}

			// swap out texture list
			m_def->particle.textures.fileNames = runtimeTextureName;
			m_def->particle.textures.handles = runtimeTextureKeys;
		} else {
			LogError("ERROR(1)");
		}

		ret = pRM->UpdateEffectDefinition(m_systemName.c_str(), m_def);
	} catch (CArchiveException*) {
		LogError("ERROR(2)");
	} catch (CException*) {
		LogError("ERROR(3)");
	}

	if (!ret)
		SetState(State::ERRORED);
	else
		SetState(LOADED_AND_CREATED);

	return ret;
}

BYTE* EnvParticleSystem::GetSerializedData(FileSize& dataLen) {
	dataLen = 0;
	CMemFile memFile;
	CArchive ar(&memFile, CArchive::store);
	Write(ar);
	ar.Close();
	memFile.Flush();
	dataLen = (FileSize)memFile.GetLength();
	return dataLen ? memFile.Detach() : nullptr;
}

void EnvParticleSystem::SetNumber(ULONG index, double number) {
	if (index == PARTICLESYSIDS_APPLYCHANGES) {
		auto pRM = dynamic_cast<ParticleRM*>(GetResourceManager());
		if (!pRM)
			return;
		pRM->UpdateEffectDefinition(m_systemName.c_str(), m_def);
	} else {
		GetSet::SetNumber(index, number);
	}
}

static ReadSTLVectorFunc<std::string, ReadSTLStringFunc> ReadStringVector;
static WriteSTLVectorFunc<std::string, WriteSTLStringFunc> WriteStringVector;
static ReadSTLMapFunc<unsigned int, float> ReadAnimatedFloatValue;
static WriteSTLMapFunc<unsigned int, float> WriteAnimatedFloatValue;

#define SerializeV1(IO, ar)                                             \
	{                                                                   \
		IO##StringVector(ar, m_def->particle.textures.fileNames);       \
		IO##StringVector(ar, m_def->particle.textures.handles);         \
		IO##Value(ar, m_def->particle.textures.customFrameRate);        \
		IO##Value(ar, m_def->particle.textures.additiveAlpha);          \
		IO##Value(ar, m_def->particle.textures.randomInitFrame);        \
		IO##Value(ar, m_def->particle.textures.randomXMirroring);       \
		IO##Value(ar, m_def->particle.textures.randomYMirroring);       \
		IO##AnimatedFloatValue(ar, m_def->particle.colorFunc.r);        \
		IO##AnimatedFloatValue(ar, m_def->particle.colorFunc.g);        \
		IO##AnimatedFloatValue(ar, m_def->particle.colorFunc.b);        \
		IO##AnimatedFloatValue(ar, m_def->particle.colorFunc.a);        \
		IO##AnimatedFloatValue(ar, m_def->particle.sizeFunc);           \
		IO##Value(ar, m_def->particle.rotation.randomDirection);        \
		IO##Value(ar, m_def->particle.rotation.initAngle);              \
		IO##Value(ar, m_def->particle.rotation.initAngleVar);           \
		IO##AnimatedFloatValue(ar, m_def->particle.rotation.angleFunc); \
		IO##Value(ar, m_def->particle.gravityCoeff);                    \
		IO##Value(ar, m_def->particle.dampingCoeff);                    \
		IO##Value(ar, m_def->particle.dampingByVelocity);               \
		IO##Value(ar, m_def->particle.dampingBySize);                   \
                                                                        \
		IO##Value(ar, m_def->emitter.shape);                            \
		IO##Value(ar, m_def->emitter.m_center.x);                       \
		IO##Value(ar, m_def->emitter.m_center.y);                       \
		IO##Value(ar, m_def->emitter.m_center.z);                       \
		IO##Value(ar, m_def->emitter.m_size.x);                         \
		IO##Value(ar, m_def->emitter.m_size.y);                         \
		IO##Value(ar, m_def->emitter.m_size.z);                         \
		IO##AnimatedFloatValue(ar, m_def->emitter.birthRateFunc);       \
		IO##AnimatedFloatValue(ar, m_def->emitter.birthRateVarFunc);    \
		IO##AnimatedFloatValue(ar, m_def->emitter.particleLifeFunc);    \
		IO##AnimatedFloatValue(ar, m_def->emitter.particleLifeVarFunc); \
		IO##AnimatedFloatValue(ar, m_def->emitter.initVelFunc.x);       \
		IO##AnimatedFloatValue(ar, m_def->emitter.initVelFunc.y);       \
		IO##AnimatedFloatValue(ar, m_def->emitter.initVelFunc.z);       \
		IO##AnimatedFloatValue(ar, m_def->emitter.initVelVarFunc.x);    \
		IO##AnimatedFloatValue(ar, m_def->emitter.initVelVarFunc.y);    \
		IO##AnimatedFloatValue(ar, m_def->emitter.initVelVarFunc.z);    \
		IO##AnimatedFloatValue(ar, m_def->emitter.ptcSizeCoeffFunc);    \
		IO##AnimatedFloatValue(ar, m_def->emitter.ptcSizeCoeffVarFunc); \
		IO##AnimatedFloatValue(ar, m_def->emitter.ptcRotCoeffFunc);     \
		IO##AnimatedFloatValue(ar, m_def->emitter.ptcRotCoeffVarFunc);  \
                                                                        \
		IO##Value(ar, m_def->offset.x);                                 \
		IO##Value(ar, m_def->offset.y);                                 \
		IO##Value(ar, m_def->offset.z);                                 \
		IO##Value(ar, m_def->emitterLife);                              \
		IO##Value(ar, m_def->emitterLifeVar);                           \
		IO##Value(ar, m_def->loopEmitter);                              \
	}

#define SerializeV2(IO, ar)                        \
	{                                              \
		SerializeV1(IO, ar);                       \
		IO##Value(ar, m_def->emitIntoObjectSpace); \
	}

void EnvParticleSystem::Write(CArchive& ar) {
	if (!m_def)
		return;

	unsigned short version = 2;
	WriteValue(ar, ms_magicHeader);
	WriteValue(ar, version);

	SerializeV2(Write, ar);
}

void EnvParticleSystem::Read(CArchive& ar) {
	if (!m_def)
		return;

	unsigned short magicHeader;
	unsigned short version;
	ReadValue(ar, magicHeader);
	ReadValue(ar, version);

	if (magicHeader != ms_magicHeader || version < 1 || version > 2) {
		CStringW fileName = L"Unknown";
		if (ar.GetFile() != NULL && !ar.GetFile()->GetFileName().IsEmpty())
			fileName = ar.GetFile()->GetFileName();
		throw new CArchiveException(CArchiveException::badSchema, fileName);
		return;
	}

	switch (version) {
		case 1:
			SerializeV1(Read, ar);
			break;

		case 2:
			SerializeV2(Read, ar);
			break;
	}

	// UGC objects have always emitted into object space.
	// For compatibility, we force the flag on.
	// If we want to enable the option again, we need to convert all old UGC
	// particle effects to have this flag set to true.
	m_def->emitIntoObjectSpace = true;
}

RuntimeParticleEffect::RuntimeParticleEffect(GLID glid, const std::string& name) :
		m_glid(glid),
		m_name(name),
		m_blade_effect_handle(INVALID_EFFECT_HANDLE),
		m_systemOn(false),
		m_on(false),
		m_regenerate(false) {
	D3DXMatrixIdentity((D3DXMATRIX*)&m_passedInMatrix);
}

ParticleRM::~ParticleRM() {
	ClearRuntimeEffects();
}

void ParticleRM::AddStockItem(const std::string& name, GLID glid) {
	UINT64 resKey = (UINT64)glid;
	if (glid == GLID_INVALID)
		resKey = m_stockItems.size() + 1;

	IResource* pRes = AddResource(resKey, name);
	assert(pRes != nullptr);

	pRes->SetState(IResource::State::LOADED_AND_CREATED);
	m_stockItems.push_back(pRes->GetHashKey());
}

EnvParticleSystem* ParticleRM::GetStockItemByIndex(UINT index) {
	if (index >= m_stockItems.size())
		return nullptr;

	UINT64 key = m_stockItems[index];
	return static_cast<EnvParticleSystem*>(GetResource(key));
}

// this func is intended for main ParticleDatabase where each particle system has a unique name
EnvParticleSystem* ParticleRM::GetByName(const std::string& name) {
	std::vector<UINT64> allKeys;
	GetAllResourceKeys(allKeys);

	for (const auto& key : allKeys) {
		auto pEPS = static_cast<EnvParticleSystem*>(GetResource(key));
		if (pEPS && name == pEPS->m_systemName) {
			return pEPS;
		}
	}

	return nullptr;
}

std::string ParticleRM::GetResourceLocalStore(const std::string& gameFilesDir) {
	std::string localStore;
	if (gameFilesDir == "~") // If parent folder is not provided, use game default
		localStore = EnvParticleSystem::mResourceStore;
	else
		localStore = PathAdd(gameFilesDir, "Particles");

	// Create directory if not already exists
	FileHelper::CreateFolderDeep(localStore);

	return localStore;
}

std::string ParticleRM::GetResourceFileName(UINT64 hash) {
	CStringA fileName;
	fileName.Format("%010I64u.par", hash);
	return fileName.GetString();
}

std::string ParticleRM::GetResourceFilePath(UINT64 hash) {
	return PathAdd(GetResourceLocalStore("~"), GetResourceFileName(hash));
}

int ParticleRM::AddRuntimeEffect(const std::string& effectName) {
	EnvParticleSystem* ps = GetByName(effectName);
	if (!ps)
		return 0;
	UINT id = AllocateNextRuntimeEffectId();
	auto it = m_runtimeEffects.insert(std::make_pair(id, RuntimeParticleEffect((GLID)ps->GetHashKey(), effectName))).first;
	it->second.m_on = true;
	it->second.m_regenerate = true;
	return id;
}

bool ParticleRM::RemoveRuntimeEffect(UINT runtimeId, bool killExistingParticles) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end())
		return false;

	// Disable Particle Effect
	SetRuntimeEffectStatus(runtimeId, -1, 0, 0);

	// Remove Particle Effect From List
	auto pCPB = GetParticleBlade();
	if (pCPB && it->second.m_blade_effect_handle != INVALID_EFFECT_HANDLE) {
		if (killExistingParticles)
			pCPB->RemoveEffect(it->second.m_blade_effect_handle);
		else
			pCPB->SetEffectWaitingToDie(it->second.m_blade_effect_handle, true);
		it->second.m_blade_effect_handle = INVALID_EFFECT_HANDLE;
	}
	m_runtimeEffects.erase(it);

	return true;
}

void ParticleRM::ClearRuntimeEffects() {
	auto pCPB = GetParticleBlade();
	if (pCPB)
		pCPB->ClearRuntimeEffects();
	m_runtimeEffects.clear();
}

void ParticleRM::FreeTextures() {
	auto pCPB = GetParticleBlade();
	if (pCPB)
		pCPB->FreeTextures();
}

void ParticleRM::GetAllRuntimeEffectsIds(std::vector<UINT>& idList) const {
	for (const auto& it : m_runtimeEffects)
		idList.push_back(it.first);
}

bool ParticleRM::SetRuntimeEffectStatus(UINT runtimeId, int bSysOn, int bOn, int bRegen) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end())
		return false;

	// Set Particle Effect Status
	if (bSysOn != -1)
		it->second.m_systemOn = (bSysOn != 0);
	if (bOn != -1)
		it->second.m_on = (bOn != 0);
	if (bRegen != -1)
		it->second.m_regenerate = (bRegen != 0);

	return true;
}

bool ParticleRM::GetRuntimeEffectStatus(UINT runtimeId, int* bSysOn, int* bOn, int* bRegen) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end())
		return false;

	// Get Particle Effect Status
	if (bSysOn)
		*bSysOn = (it->second.m_systemOn ? 1 : 0);
	if (bOn)
		*bOn = (it->second.m_on ? 1 : 0);
	if (bRegen)
		*bRegen = (it->second.m_regenerate ? 1 : 0);

	return true;
}

bool ParticleRM::SetRuntimeEffectMatrix(UINT runtimeId, const Matrix44f& matrix) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end())
		return false;

	// Set Particle Effect Matrix
	it->second.m_passedInMatrix = matrix;

	return true;
}

bool ParticleRM::GetRuntimeEffectMatrix(UINT runtimeId, Matrix44f& matrix) const {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end())
		return false;

	// Get Particle Effect Matrix
	matrix = it->second.m_passedInMatrix;

	return true;
}

EffectHandle ParticleRM::GetRuntimeEffectHandle(UINT runtimeId) const {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end())
		return INVALID_EFFECT_HANDLE;

	// Return Particle Effect Handle
	return it->second.m_blade_effect_handle;
}

bool ParticleRM::ActivateRuntimeEffect(UINT runtimeId, const Vector3f& pos, const Quaternionf& ori) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end())
		return false;

	RuntimeParticleEffect& RPE = it->second;
	if (RPE.m_blade_effect_handle != INVALID_EFFECT_HANDLE)
		return false;

	std::string name = RPE.m_name;
	EnvParticleSystem* ps = GetByName(name);
	if (!ps)
		return false;

	if (ps->StateIsLoadedAndCreated()) {
		auto pCPB = GetParticleBlade();
		if (!pCPB)
			return false;

		EffectHandle handle = pCPB->CreateEffect(
			name.c_str(),
			pos.x, pos.y, pos.z,
			ori.x, ori.y, ori.z, ori.w);
		RPE.m_blade_effect_handle = handle;
		return true;
	} else if (ps->StateIsInitializedNotLoaded()) {
		QueueResource(ps);
	}

	return false;
}

bool ParticleRM::MoveRuntimeEffect(UINT runtimeId, const Vector3f& pos, const Quaternionf& ori) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end() || it->second.m_blade_effect_handle == INVALID_EFFECT_HANDLE)
		return false;

	auto pCPB = GetParticleBlade();
	if (!pCPB)
		return false;

	pCPB->SetEffectPosition(it->second.m_blade_effect_handle, pos.x, pos.y, pos.z);
	pCPB->SetEffectOrientation(it->second.m_blade_effect_handle, ori.x, ori.y, ori.z, ori.w);
	return true;
}

bool ParticleRM::DeactivateRuntimeEffect(UINT runtimeId) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end() || it->second.m_blade_effect_handle == INVALID_EFFECT_HANDLE)
		return false;

	auto pCPB = GetParticleBlade();
	if (!pCPB)
		return false;

	// Don't immediately destroy the particles but instead set their
	// 'Waiting To Die' flag, so that they no longer emit particles
	// and will destroy themselves when all of their currently 'living'
	// particles 'die'.
	pCPB->SetEffectWaitingToDie(it->second.m_blade_effect_handle, true);
	it->second.m_blade_effect_handle = INVALID_EFFECT_HANDLE;
	return true;
}

bool ParticleRM::GetRuntimeEffect(UINT runtimeId, GLID& glid, std::string& effectName) {
	// Find Particle Effect In List
	auto it = m_runtimeEffects.find(runtimeId);
	if (it == m_runtimeEffects.end() || it->second.m_blade_effect_handle == INVALID_EFFECT_HANDLE)
		return false;

	glid = it->second.m_glid;
	effectName = it->second.m_name;
	return true;
}

EnvParticleSystem* ParticleRM::GetByGLID(const GLID& glid, bool bCreateIfNotExist) {
	EnvParticleSystem* pParticleSystem = (EnvParticleSystem*)FindResource((UINT64)glid);
	if (!pParticleSystem && bCreateIfNotExist) {
		// Add to resource database
		UINT64 resKey = (UINT64)glid;
		pParticleSystem = AddResource(resKey, GetResourceFileName(resKey));
		assert(pParticleSystem != nullptr);

		if (pParticleSystem) {
			// Create an entry in the particle blade with empty definition (will be updated later in CreateResource)
			pParticleSystem->SetNumber(PARTICLESYSIDS_APPLYCHANGES, 1);

			// Read file or triggering download
			QueueResource(pParticleSystem);
		}
	}

	return pParticleSystem;
}

bool ParticleRM::UpdateEffectDefinition(const char* effectName, EffectDef* pDef) {
	auto pCPB = GetParticleBlade();
	if (!pCPB)
		return false;

	std::lock_guard<fast_recursive_mutex> lock(m_particleBladeSync);
	return pCPB->LoadKEPDefinitionsFromBuffer(effectName, pDef);
}

// GetSet helpers for direct access from script
class GetSetAdapter : public IGetSet {
	// dummy overrides
	virtual UINT GetNameId() {
		return 0;
	}
	virtual Vector3f& GetVector3f(ULONG /*index*/) {
		static Vector3f dummy;
		ASSERT(FALSE);
		return dummy;
	}
	virtual ULONG& GetULONG(ULONG /*index*/) {
		static ULONG dummy = 0;
		ASSERT(FALSE);
		return dummy;
	}
	virtual INT& GetINT(ULONG /*index*/) {
		static INT dummy = 0;
		ASSERT(FALSE);
		return dummy;
	}
	virtual BOOL& GetBOOL(ULONG /*index*/) {
		static BOOL dummy = FALSE;
		ASSERT(FALSE);
		return dummy;
	}
	virtual bool& Getbool(ULONG /*index*/) {
		static bool dummy = false;
		ASSERT(FALSE);
		return dummy;
	}
	virtual std::string& Getstring(ULONG /*index*/) {
		static std::string dummy;
		ASSERT(FALSE);
		return dummy;
	}
	virtual IGetSet* GetObjectPtr(ULONG /*index*/) {
		ASSERT(FALSE);
		return NULL;
	}
	virtual void GetColor(ULONG /*index*/, float* /*r*/, float* /*g*/, float* /*b*/, float* /*a*/ = 0) {
		ASSERT(FALSE);
	}
	virtual double GetNumber(ULONG /*index*/) {
		ASSERT(FALSE);
		return 0;
	}
	virtual std::string GetString(ULONG /*index*/) {
		ASSERT(FALSE);
		return "";
	}
	virtual void SetString(ULONG /*index*/, const char* /*s*/) {
		ASSERT(FALSE);
	}
	virtual void SetNumber(ULONG /*index*/, double /*number*/) {
		ASSERT(FALSE);
	}
	virtual void SetNumber(ULONG index, LONG number) {
		SetNumber(index, (double)number);
	}
	virtual void SetNumber(ULONG index, ULONG number) {
		SetNumber(index, (double)number);
	}
	virtual void SetNumber(ULONG index, BOOL value) {
		SetNumber(index, (double)value);
	}
	virtual void SetNumber(ULONG index, bool value) {
		SetNumber(index, (double)value);
	}
	virtual void SetVector3f(ULONG /*index*/, Vector3f /*v*/) {
		ASSERT(FALSE);
	}
	virtual void SetColor(ULONG /*index*/, float /*r*/, float /*g*/, float /*b*/, float /*a*/ = 0) {
		ASSERT(FALSE);
	}
	virtual ULONG AddNewIntMember(const char* /*name*/, int /*defaultValue*/, ULONG /*flags*/ = amfTransient) {
		ASSERT(FALSE);
		return 0;
	}
	virtual ULONG AddNewDoubleMember(const char* /*name*/, double /*defaultValue*/, ULONG /*flags*/ = amfTransient) {
		ASSERT(FALSE);
		return 0;
	}
	virtual ULONG AddNewStringMember(const char* /*name*/, const char* /*defaultValue*/, ULONG /*flags*/ = amfTransient) {
		ASSERT(FALSE);
		return 0;
	}
	virtual ULONG FindIdByName(const char* /*name*/, EGetSetType* /*type*/ = 0) {
		ASSERT(FALSE);
		return 0;
	}
	virtual ULONG GetArrayCount(ULONG /*indexOfArray*/) {
		ASSERT(FALSE);
		return 0;
	}
	virtual IGetSet* GetObjectInArray(ULONG /*indexOfArray*/, ULONG /*indexInArray*/) {
		ASSERT(FALSE);
		return NULL;
	}
};

template <typename ValueType>
class gsVector : public GetSetAdapter {
public:
	gsVector(std::vector<ValueType>& dataSource) :
			m_data(dataSource) {}

	// Key enumeration
	virtual ULONG GetArrayCount(ULONG indexOfArray) {
		if (indexOfArray == (ULONG)-1)
			return m_data.size();
		else
			return 0;
	}
	virtual IGetSet* GetObjectInArray(ULONG indexOfArray, ULONG indexInArray) {
		if (indexOfArray == (ULONG)-1) {
			if (indexInArray < m_data.size()) {
				return *(IGetSet**)&indexInArray;
			}
		}

		return NULL;
	}

protected:
	std::vector<ValueType>& m_data;
};

class gsStringVector : public gsVector<std::string> {
public:
	gsStringVector(std::vector<std::string>& dataSource) :
			gsVector<std::string>(dataSource) {}

	virtual std::string GetString(ULONG index) {
		if (index < m_data.size())
			return m_data[index];
		return "";
	}

	virtual void SetString(ULONG index, const char* s) {
		// Resize vector if necessary
		if (index + 1 > m_data.size())
			m_data.resize(index + 1);

		// Set value at index
		m_data[index] = s;
	}

	virtual void SetNumber(ULONG index, double number) {
		if (index == (ULONG)-1 && number == 0) {
			// if set number#-1 to 0, truncate this array
			m_data.clear();
		} else {
			// otherwise operation unsupported
			ASSERT(FALSE);
		}
	}
};

class gsParticleFunc : public GetSetAdapter {
public:
	gsParticleFunc(std::map<unsigned int, float>& dataSource) :
			m_data(dataSource) {}

	// Key enumeration
	virtual ULONG GetArrayCount(ULONG indexOfArray) {
		if (indexOfArray == (ULONG)-1)
			return m_data.size();
		else
			return 0;
	}
	virtual IGetSet* GetObjectInArray(ULONG indexOfArray, ULONG indexInArray) {
		if (indexOfArray == (ULONG)-1) {
			if (indexInArray < m_data.size()) {
				ULONG idx = 0;
				for (std::map<unsigned int, float>::iterator it = m_data.begin(); it != m_data.end(); ++it, ++idx) {
					if (idx == indexInArray) {
						return *(IGetSet**)&it->first;
					}
				}
			}
		}

		return NULL;
	}

	// Get value by key
	virtual double GetNumber(ULONG index) {
		if (m_data.find(index) != m_data.end())
			return m_data[index];

		ASSERT(FALSE);
		return -1;
	}

	// Set value by key
	virtual void SetNumber(ULONG index, double number) {
		if (index == (ULONG)-1) {
			// clear function
			m_data.clear();
		} else {
			// append data
			m_data[index] = (float)number;
		}
	}

private:
	std::map<unsigned int, float>& m_data;
};

#define GETSET_USER_PS_STRVECTOR(var, flags, indent, groupId, strId) \
	AddGetSetRec(new gsStringVector(var), gsGetSetPtr, flags, indent, groupId, strId, strId, 0.0, 0.0);

#define GETSET_USER_PS_FUNC(var, flags, indent, groupId, strId) \
	AddGetSetRec(new gsParticleFunc(var), gsGetSetPtr, flags, indent, groupId, strId, strId, 0.0, 0.0);

typedef EnvParticleSystem ParticleSys;

#define IDS_PARTICLESYSTEM IDS_TEXTENTRY_NAME
#define DUMMY_RESOURCE IDS_TEXTENTRY_TEXT
#define IDS_PARTICLESYSTEM_DEF_OFFSET DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTERLIFE DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTERLIFEVAR DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_LOOPEMITTER DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITINTOOBJECTSPACE DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_FILENAMES DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_HANDLES DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_CUSTOMFRAMERATE DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_ADDITIVEALPHA DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMINITFRAME DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMXMIRRORING DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMYMIRRORING DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_R DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_G DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_B DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_A DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_SIZEFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_RANDOMDIRECTION DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_STARTINGANGLE DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_STARTINGANGLEVAR DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_ANGLEFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_GRAVITYCOEFF DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGCOEFF DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGBYVELOCITY DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGBYSIZE DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_SHAPE DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_CENTER DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_SIZE DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_BIRTHRATEFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_BIRTHRATEVARFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_PARTICLELIFEFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_PARTICLELIFEVARFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELFUNC_X DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELFUNC_Y DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELFUNC_Z DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELVARFUNC_X DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELVARFUNC_Y DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELVARFUNC_Z DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_PTCSIZECOEFFFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_DEF_EMITTER_PTCROTATIONCOEFFFUNC DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_APPLYCHANGES DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_APPLYCHANGES_HELP DUMMY_RESOURCE
#define IDS_PARTICLESYSTEM_SYSTEMNAME DUMMY_RESOURCE

BEGIN_GETSET_IMPL(ParticleSys, IDS_PARTICLESYSTEM)
GETSET2_D3DVECTOR(m_def->offset, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_OFFSET, IDS_PARTICLESYSTEM_DEF_OFFSET)
GETSET2(m_def->emitterLife, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTERLIFE, IDS_PARTICLESYSTEM_DEF_EMITTERLIFE)
GETSET2(m_def->emitterLifeVar, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTERLIFEVAR, IDS_PARTICLESYSTEM_DEF_EMITTERLIFEVAR)
GETSET2(m_def->loopEmitter, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTERLIFE, IDS_PARTICLESYSTEM_DEF_LOOPEMITTER)
GETSET2(m_def->emitIntoObjectSpace, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITINTOOBJECTSPACE, IDS_PARTICLESYSTEM_DEF_EMITINTOOBJECTSPACE)
GETSET_USER_PS_STRVECTOR(m_def->particle.textures.fileNames, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_FILENAMES)
GETSET_USER_PS_STRVECTOR(m_def->particle.textures.handles, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_HANDLES)
GETSET2(m_def->particle.textures.customFrameRate, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_CUSTOMFRAMERATE, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_CUSTOMFRAMERATE)
GETSET2(m_def->particle.textures.additiveAlpha, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_ADDITIVEALPHA, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_ADDITIVEALPHA)
GETSET2(m_def->particle.textures.randomInitFrame, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMINITFRAME, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMINITFRAME)
GETSET2(m_def->particle.textures.randomXMirroring, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMXMIRRORING, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMXMIRRORING)
GETSET2(m_def->particle.textures.randomYMirroring, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMYMIRRORING, IDS_PARTICLESYSTEM_DEF_PARTICLE_TEXTURES_RANDOMYMIRRORING)
GETSET_USER_PS_FUNC(m_def->particle.colorFunc.r, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_R)
GETSET_USER_PS_FUNC(m_def->particle.colorFunc.g, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_G)
GETSET_USER_PS_FUNC(m_def->particle.colorFunc.b, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_B)
GETSET_USER_PS_FUNC(m_def->particle.colorFunc.a, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_COLORFUNC_A)
GETSET_USER_PS_FUNC(m_def->particle.sizeFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_SIZEFUNC)
GETSET2(m_def->particle.rotation.randomDirection, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_RANDOMDIRECTION, IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_RANDOMDIRECTION)
GETSET2(m_def->particle.rotation.initAngle, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_STARTINGANGLE, IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_STARTINGANGLE)
GETSET2(m_def->particle.rotation.initAngleVar, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_STARTINGANGLEVAR, IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_STARTINGANGLEVAR)
GETSET_USER_PS_FUNC(m_def->particle.rotation.angleFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_ROTATION_ANGLEFUNC)
GETSET2(m_def->particle.gravityCoeff, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_GRAVITYCOEFF, IDS_PARTICLESYSTEM_DEF_PARTICLE_GRAVITYCOEFF)
GETSET2(m_def->particle.dampingCoeff, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGCOEFF, IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGCOEFF)
GETSET2_RANGE(m_def->particle.dampingByVelocity, gsUint, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGBYVELOCITY, IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGBYVELOCITY, LINEAR, QUADRATIC)
GETSET2_RANGE(m_def->particle.dampingBySize, gsUint, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGBYSIZE, IDS_PARTICLESYSTEM_DEF_PARTICLE_DAMPINGBYSIZE, LINEAR, QUADRATIC)
GETSET2_RANGE(m_def->emitter.shape, gsUint, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_SHAPE, IDS_PARTICLESYSTEM_DEF_EMITTER_SHAPE, BOX, SPHERE)
GETSET2_D3DVECTOR(m_def->emitter.m_center, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_CENTER, IDS_PARTICLESYSTEM_DEF_EMITTER_CENTER)
GETSET2_D3DVECTOR(m_def->emitter.m_size, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_SIZE, IDS_PARTICLESYSTEM_DEF_EMITTER_SIZE)
GETSET_USER_PS_FUNC(m_def->emitter.birthRateFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_BIRTHRATEFUNC)
GETSET_USER_PS_FUNC(m_def->emitter.birthRateVarFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_BIRTHRATEVARFUNC)
GETSET_USER_PS_FUNC(m_def->emitter.particleLifeFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_PARTICLELIFEFUNC)
GETSET_USER_PS_FUNC(m_def->emitter.particleLifeVarFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_PARTICLELIFEVARFUNC)
GETSET_USER_PS_FUNC(m_def->emitter.initVelFunc.x, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELFUNC_X)
GETSET_USER_PS_FUNC(m_def->emitter.initVelFunc.y, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELFUNC_Y)
GETSET_USER_PS_FUNC(m_def->emitter.initVelFunc.z, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELFUNC_Z)
GETSET_USER_PS_FUNC(m_def->emitter.initVelVarFunc.x, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELVARFUNC_X)
GETSET_USER_PS_FUNC(m_def->emitter.initVelVarFunc.y, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELVARFUNC_Y)
GETSET_USER_PS_FUNC(m_def->emitter.initVelVarFunc.z, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_INITVELVARFUNC_Z)
GETSET_USER_PS_FUNC(m_def->emitter.ptcSizeCoeffFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_PTCSIZECOEFFFUNC)
GETSET_USER_PS_FUNC(m_def->emitter.ptcSizeCoeffVarFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_PTCSIZECOEFFFUNC)
GETSET_USER_PS_FUNC(m_def->emitter.ptcRotCoeffFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_PTCROTATIONCOEFFFUNC)
GETSET_USER_PS_FUNC(m_def->emitter.ptcRotCoeffVarFunc, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_DEF_EMITTER_PTCROTATIONCOEFFFUNC)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, PARTICLESYSTEM, APPLYCHANGES, 0, 0)
GETSET2(m_systemName, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_PARTICLESYSTEM_SYSTEMNAME, IDS_PARTICLESYSTEM_SYSTEMNAME)
END_GETSET_IMPL

} // namespace KEP