/******************************************************************************
 ImageEXMesh.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"

#include <ImageEXMesh.h>
#include "CEXMeshClass.h"
#include "CStateManagementClass.h"
#include "TextureDatabase.h"

using namespace KEP;

// identical to BrowserEXMesh.cpp
int ImageEXMesh::Render(LPDIRECT3DDEVICE9 pd3dDevice, IN CEXMeshObj* mesh, int vertexType) {
	int ret = 0;

	//this will need to have some management added into it
	m_pDIRECT3DDEVICE9 = pd3dDevice;

	m_pTexture = CreateTextureFromWindow();

	// copied from DrawTexturedQuad
	m_stateManager->SetMaterial(pd3dDevice, &mesh->m_materialObject->m_useMaterial);

	pd3dDevice->SetTexture(0, m_pTexture); // set texture

	m_stateManager->SetDiffuseSource(mesh->m_abVertexArray.m_advancedFixedMode, pd3dDevice);
	m_stateManager->SetVertexType(mesh->m_abVertexArray.m_uvCount, pd3dDevice);
	ret = mesh->m_abVertexArray.Render(pd3dDevice, vertexType);

	m_stateManager->SetTextureState(-1, 0, pd3dDevice, m_textureDatabase);

	return ret;
}
