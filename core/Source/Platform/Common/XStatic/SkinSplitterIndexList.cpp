///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "skinsplitterindexlist.h"
/*
 Creates and initializes an SkinSplitterIndexList object with "size" elements
*/
SkinSplitterIndexList* IndexListCreate(const unsigned int maxSize, const unsigned int maxValue) {
	SkinSplitterIndexList* newList = new SkinSplitterIndexList;
	if (!newList) {
		return NULL;
	}
	newList->indexes = new unsigned int[maxSize];
	if (!newList->indexes) {
		return NULL;
	}
	if (maxValue > 0) {
		newList->usedMap = new unsigned char[maxValue];
		if (!newList->usedMap) {
			return NULL;
		}
		memset((void*)newList->usedMap, 0, maxValue);
	} else {
		newList->usedMap = NULL;
	}

	newList->maxSize = maxSize;
	newList->maxValue = maxValue;
	newList->currentSize = 0;

	return newList;
}

/*
 Destroys the SkinSplitterIndexList object
*/
void IndexListDestroy(SkinSplitterIndexList* list) {
	delete[] list->usedMap;
	delete[] list->indexes;
	delete list;
}

/*
 Re-initializes "list"
*/
void IndexListReset(SkinSplitterIndexList* list) {
	memset((void*)list->usedMap, 0, list->maxValue);
	list->currentSize = 0;
}

/*
 Deletes the last "n" indexes of "list"
*/
void IndexListDeleteIndexes(SkinSplitterIndexList* list, unsigned int n) {
	unsigned int i;

	if (n > list->currentSize) {
		n = list->currentSize;
	}

	for (i = list->currentSize - n; i < list->currentSize; i++) {
		list->usedMap[i] = 0;
	}

	list->currentSize -= n;
}

/*
 Invalidates the index "i" in "list"
*/
void IndexListInvalidateIndex(SkinSplitterIndexList* list, unsigned int i) {
	if (list->usedMap[i] == 0) {
		list->usedMap[i] = 2;
	}
}

/*
 Re-validates the index "i" in "list"
*/
void IndexListRevalidateIndex(SkinSplitterIndexList* list, unsigned int i) {
	list->usedMap[i] = 0;
}

/*
 Re-validates all invalidated indexes in "list"
*/
void IndexListRevalidateIndexes(SkinSplitterIndexList* list) {
	unsigned int i;

	for (i = 0; i < list->maxValue; i++) {
		if (list->usedMap[i] == 2) {
			list->usedMap[i] = 0;
		}
	}
}

/*
 Returns TRUE if "index" is in "list", else FALSE
*/
unsigned int IndexListContainsIndex(const SkinSplitterIndexList* list, const unsigned int index) {
	if (index < list->maxValue) {
		if (list->usedMap[index] != 0) {
			return 1;
		}
	}
	return 0;
}

/*
 Returns if "list" is full
*/
BOOL IndexListFull(SkinSplitterIndexList* list) {
	return list->currentSize == list->maxSize;
}

/*
 Returns the index list
*/
unsigned int* IndexListGetList(SkinSplitterIndexList* list) {
	return list->indexes;
}

/*
 Returns current size of "list"
*/
unsigned int IndexListGetSize(const SkinSplitterIndexList* list) {
	return list->currentSize;
}

/*
 Returns if index "i" appears in "list"
*/
BOOL IndexListIndexUsed(const SkinSplitterIndexList* list, const unsigned int i) {
	if (i < list->maxValue) {
		return list->usedMap[i] != 0;
	}
	return FALSE;
}

/*
 Returns if  index "i" is invalid in "list"
*/
BOOL IndexListInvalidIndex(const SkinSplitterIndexList* list, const unsigned int i) {
	return i >= list->maxValue;
}

/*
 Returns "ith" element of "list"
*/
unsigned int IndexListGetIndex(const SkinSplitterIndexList* list, const unsigned int i) {
	if (i < list->currentSize) {
		return list->indexes[i];
	}
	return 0;
}

/*
 Sets "ith" element of "list" to "val"
*/
unsigned int IndexListSetIndex(const SkinSplitterIndexList* list, const unsigned int i, const unsigned int val) {
	if (i < list->currentSize) {
		list->indexes[i] = val;
		list->usedMap[i] = 1;
	}
	return 0;
}

/*
 Adds "index" to "list" and returns TRUE.....if "list" is full or "index" invalid or already contained, returns FALSE
*/
BOOL IndexListAddIndex(SkinSplitterIndexList* list, const unsigned int index) {
	if (IndexListFull(list)) {
		return FALSE;
	}
	if (list->usedMap) {
		if (index >= list->maxValue) {
			return FALSE;
		}
		if (list->usedMap[index] != 0) {
			return FALSE;
		}
		list->usedMap[index] = 1;
	}
	list->indexes[(list->currentSize)++] = index;
	return TRUE;
}

/*
 Adds "n" "indexes" to "list" and returns TRUE.....if "list" is full or "indexes" invalid or already contained, returns FALSE
*/
BOOL IndexListAddIndexes(SkinSplitterIndexList* list, const unsigned int* indexes, const unsigned int n) {
	unsigned int i;
	BOOL ok = TRUE;

	for (i = 0; i < n; i++) {
		if (!IndexListAddIndex(list, indexes[i])) {
			ok = FALSE;
		}
	}
	return ok;
}

/*
 Deletes the last "n" elements that were added to "list"
*/
static void IndexListDeleteLastIndexes(SkinSplitterIndexList* list, unsigned int n) {
	unsigned int j;

	if (!list->currentSize) {
		return;
	}
	for (j = 0; j < n; j++) {
		list->usedMap[list->indexes[--(list->currentSize)]] = 0;
	}
}

/*
 Returns TRUE if "list" merged into "targetList" without exceeding its size...only NEW elements are merged in.
*/
BOOL IndexListMerge(SkinSplitterIndexList* targetList, const SkinSplitterIndexList* list) {
	unsigned int i, numNew = 0;

	for (i = 0; i < list->currentSize; i++) {
		if (IndexListFull(targetList)) {
			IndexListDeleteLastIndexes(targetList, numNew);
			return FALSE;
		}
		if (IndexListAddIndex(targetList, list->indexes[i])) {
			numNew++;
		}
	}
	return TRUE;
}

/*
 Returns the first, starting at "ix", unused index of "list".....returns SKINSPLITTERINVALIDINDEX is list full
*/
unsigned int IndexListGetFirstUnused(const SkinSplitterIndexList* list, const unsigned int ix) {
	unsigned int i;

	for (i = ix; i < list->maxValue; i++) {
		if (list->usedMap[i] == 0) {
			return i;
		}
	}
	return SKINSPLITTERINVALIDINDEX;
}

/*
 Sorts the input list into increasing (or decreasing) order
 ....returns the value of the top of the sorted list.
 Insertion sort....O(n) on a nearly sorted list.
*/
unsigned int IndexListSort(SkinSplitterIndexList* list, const BOOL increasingOrder) {
	int i, j;
	unsigned int key;

	if (increasingOrder) {
		for (i = 1; i < (int)list->currentSize; i++) {
			key = list->indexes[i];
			j = i - 1;
			while (list->indexes[j] > key) {
				list->indexes[j + 1] = list->indexes[j];
				if (--j < 0)
					break;
			}
			list->indexes[j + 1] = key;
		}
	} else {
		for (i = 1; i < (int)list->currentSize; i++) {
			key = list->indexes[i];
			j = i - 1;
			while (list->indexes[j] < key) {
				list->indexes[j + 1] = list->indexes[j];
				if (--j < 0)
					break;
			}
			list->indexes[j + 1] = key;
		}
	}
	return list->indexes[0];
}

/*
 Makes temp copy of "list", sorts it, and returns in "list"
 the sorted INDEXES corresponding to the original sorted values.
 ....returns the index of the top of the sorted list.
 Insertion sort....O(n) on a nearly sorted list.
*/
unsigned int IndexListIndexSort(SkinSplitterIndexList* list, const BOOL increasingOrder) {
	int i, j;
	unsigned int key;
	unsigned int* val = new unsigned int[list->currentSize];

	// make the temp copy of the index array values
	memcpy((void*)val, (void*)list->indexes, list->currentSize * sizeof(unsigned int));
	// convert the index value array into a consecutive index array
	for (i = 0; i < (int)list->currentSize; i++) {
		list->indexes[i] = i;
	}
	// sort the temp copy, AND the corresponding index array
	if (increasingOrder) {
		for (i = 1; i < (int)list->currentSize; i++) {
			key = val[i];
			j = i - 1;
			while (val[j] > key) {
				val[j + 1] = val[j];
				list->indexes[j + 1] = list->indexes[j];
				if (--j < 0)
					break;
			}
			val[j + 1] = key;
			list->indexes[j + 1] = i;
		}
	} else {
		for (i = 1; i < (int)list->currentSize; i++) {
			key = val[i];
			j = i - 1;
			while (val[j] < key) {
				val[j + 1] = val[j];
				list->indexes[j + 1] = list->indexes[j];
				if (--j < 0)
					break;
			}
			val[j + 1] = key;
			list->indexes[j + 1] = i;
		}
	}
	delete[] val;
	return list->indexes[0];
}
