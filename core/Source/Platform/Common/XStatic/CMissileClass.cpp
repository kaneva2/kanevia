///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CMovementObj.h"
#include "CMissileClass.h"
#include "CSoundManagerClass.h"
#include "MaterialPersistence.h"
#include "CPhysicsEmuClass.h"
#include "SkeletonConfiguration.h"
#include "RuntimeSkeleton.h"
#include "CSkeletonObject.h"
#include "CRTLightClass.h"
#include "CLaserClass.h"
#include "CEXMeshClass.h"

namespace KEP {

CMissileObj::CMissileObj() :
		m_pRuntimeSkeleton(nullptr) {
	m_contactAttribute = 0;
	m_skillType = -1;
	m_abilityType = -1;
	m_traceOfTarget = -1;
	m_heatSeekingPower = 1.0f;
	m_forceExplodeAtEnd = FALSE;
	m_contactPointStored.x = 0.0f;
	m_contactPointStored.y = 0.0f;
	m_contactPointStored.z = 0.0f;
	m_collidedNormalStore.x = 0.0f;
	m_collidedNormalStore.y = 0.0f;
	m_collidedNormalStore.z = 0.0f;
	m_lastPosition.x = 0.0f;
	m_lastPosition.y = 0.0f;
	m_lastPosition.z = 0.0f;
	m_lifeTime = 2000; //milliseconds
	m_ownerTrace = -1;
	m_damageValue = 1;
	m_orientateToCamera = FALSE;
	m_particleLink = -1;
	m_collisionRadius = 5.0f;
	m_startTimeStamp = 0;
	m_explosionLink = -1;
	m_typeOfProjectile = 0;
	m_missileName = "Missile";
	m_criteriaForSkillUse = 0;
	m_pushPower = 0.0f;

	m_runtimeParticleId = 0;
	m_baseFrame = NULL;
	m_basicMesh = NULL;
	m_physicDynamics = NULL;

	m_enableSound = FALSE;
	m_distanceFactor = .03f;
	m_needInitPlay = TRUE;
	m_laserObject = NULL;
	m_missileDBRef = -1;
	m_sndID = 0;
	m_soundHandle = NULL;
	m_burstForceType = FALSE;
	m_burstAppliedState = FALSE;
	m_useFullPhysics = FALSE;
	m_useElastics = FALSE;
	m_light = NULL;
	m_damageChannel = 0;
	m_filterCollision = FALSE;
	m_teamRef = -1;
	m_damageSpecific = -1;
	m_baseKickPower = 0.0f;
}

void CMissileObj::Clone(CMissileObj** copy, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CMissileObj* copyLocal = new CMissileObj();

	copyLocal->m_contactAttribute = m_contactAttribute;
	copyLocal->m_skillType = m_skillType;
	copyLocal->m_abilityType = m_abilityType;
	copyLocal->m_traceOfTarget = m_traceOfTarget;
	copyLocal->m_heatSeekingPower = m_heatSeekingPower;
	copyLocal->m_forceExplodeAtEnd = m_forceExplodeAtEnd;
	copyLocal->m_contactPointStored.x = m_contactPointStored.x;
	copyLocal->m_contactPointStored.y = m_contactPointStored.y;
	copyLocal->m_contactPointStored.z = m_contactPointStored.z;
	copyLocal->m_collidedNormalStore.x = m_collidedNormalStore.x;
	copyLocal->m_collidedNormalStore.y = m_collidedNormalStore.y;
	copyLocal->m_collidedNormalStore.z = m_collidedNormalStore.z;
	copyLocal->m_lastPosition.x = m_lastPosition.x;
	copyLocal->m_lastPosition.y = m_lastPosition.y;
	copyLocal->m_lastPosition.z = m_lastPosition.z;
	copyLocal->m_lifeTime = m_lifeTime;
	copyLocal->m_ownerTrace = m_ownerTrace;
	copyLocal->m_damageValue = m_damageValue;
	copyLocal->m_orientateToCamera = m_orientateToCamera;
	copyLocal->m_particleLink = m_particleLink;
	copyLocal->m_collisionRadius = m_collisionRadius;
	copyLocal->m_explosionLink = m_explosionLink;
	copyLocal->m_typeOfProjectile = m_typeOfProjectile;
	copyLocal->m_missileName = m_missileName;
	copyLocal->m_criteriaForSkillUse = m_criteriaForSkillUse;
	copyLocal->m_pushPower = m_pushPower;
	copyLocal->m_sndID = m_sndID;
	copyLocal->m_enableSound = m_enableSound;

	copyLocal->m_startTimeStamp = 0;
	copyLocal->m_baseFrame = NULL;
	copyLocal->m_pRuntimeSkeleton = NULL;
	copyLocal->m_basicMesh = NULL;
	copyLocal->m_physicDynamics = NULL;
	copyLocal->m_burstForceType = m_burstForceType;
	copyLocal->m_burstAppliedState = m_burstAppliedState;
	copyLocal->m_useFullPhysics = m_useFullPhysics;
	copyLocal->m_useElastics = m_useElastics;
	copyLocal->m_damageChannel = m_damageChannel;
	copyLocal->m_filterCollision = m_filterCollision;
	copyLocal->m_damageSpecific = m_damageSpecific;
	copyLocal->m_baseKickPower = m_baseKickPower;

	if (m_light)
		m_light->Clone(&copyLocal->m_light);

	if (m_physicDynamics)
		m_physicDynamics->Clone(&copyLocal->m_physicDynamics);

	if (m_pRuntimeSkeleton) {
		m_pRuntimeSkeleton->Clone(&copyLocal->m_pRuntimeSkeleton, g_pd3dDevice, NULL);
	}

	if (m_basicMesh)
		m_basicMesh->Clone(&copyLocal->m_basicMesh, g_pd3dDevice);

	if (m_laserObject)
		m_laserObject->Clone(&copyLocal->m_laserObject, NULL);

	*copy = copyLocal;
}

void CMissileObj::SafeDelete() {
	if (m_baseFrame) {
		m_baseFrame->SafeDelete();
		delete m_baseFrame;
		m_baseFrame = 0;
	}
	m_pRuntimeSkeleton.reset();
	if (m_basicMesh) {
		m_basicMesh->SafeDelete();
		delete m_basicMesh;
		m_basicMesh = 0;
	}
	if (m_physicDynamics) {
		delete m_physicDynamics;
		m_physicDynamics = 0;
	}
	if (m_laserObject) {
		m_laserObject->SafeDelete();
		delete m_laserObject;
		m_laserObject = 0;
	}
	if (m_soundHandle) {
		m_soundHandle->SetToDelete();
		m_soundHandle = NULL;
	}
	if (m_light) {
		delete m_light;
		m_light = 0;
	}
}

void CMissileObj::Serialize(CArchive& ar) {
	CSkeletonObject* pTmpSkeleton = nullptr;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		if (m_pRuntimeSkeleton != nullptr) {
			// Wrap runtime skeleton into a temporary skeleton object
			pTmpSkeleton = new CSkeletonObject(m_pRuntimeSkeleton, nullptr);
		}

		ar << m_contactAttribute
		   << m_skillType
		   << m_abilityType
		   << m_traceOfTarget
		   << m_heatSeekingPower
		   << m_forceExplodeAtEnd
		   << m_contactPointStored.x
		   << m_contactPointStored.y
		   << m_contactPointStored.z
		   << m_collidedNormalStore.x
		   << m_collidedNormalStore.y
		   << m_collidedNormalStore.z
		   << m_lastPosition.x
		   << m_lastPosition.y
		   << m_lastPosition.z
		   << (int)m_lifeTime
		   << m_ownerTrace
		   << m_damageValue
		   << m_orientateToCamera
		   << pTmpSkeleton
		   << m_basicMesh
		   << m_particleLink
		   << m_collisionRadius
		   << m_physicDynamics
		   << m_explosionLink
		   << m_typeOfProjectile
		   << m_missileName
		   << m_criteriaForSkillUse
		   << m_damageChannel
		   << m_enableSound
		   << m_pushPower
		   << m_distanceFactor
		   << m_laserObject
		   << m_sndID
		   << m_burstForceType
		   << m_useFullPhysics
		   << m_useElastics
		   << m_light
		   << m_filterCollision
		   << m_damageSpecific //migrated
		   << m_baseKickPower; //migrated

		if (pTmpSkeleton) {
			pTmpSkeleton->detach();
			delete pTmpSkeleton;
			pTmpSkeleton = nullptr;
		}

	} else {
		int lifetime;
		ar >> m_contactAttribute >> m_skillType >> m_abilityType >> m_traceOfTarget >> m_heatSeekingPower >> m_forceExplodeAtEnd >> m_contactPointStored.x >> m_contactPointStored.y >> m_contactPointStored.z >> m_collidedNormalStore.x >> m_collidedNormalStore.y >> m_collidedNormalStore.z >> m_lastPosition.x >> m_lastPosition.y >> m_lastPosition.z >> lifetime >> m_ownerTrace >> m_damageValue >> m_orientateToCamera >> pTmpSkeleton >> m_basicMesh >> m_particleLink >> m_collisionRadius >> m_physicDynamics >> m_explosionLink >> m_typeOfProjectile >> m_missileName >> m_criteriaForSkillUse >> m_damageChannel >> m_enableSound >> m_pushPower >> m_distanceFactor >> m_laserObject >> m_sndID >> m_burstForceType >> m_useFullPhysics >> m_useElastics >> m_light >> m_filterCollision >> m_damageSpecific //migrated
			>> m_baseKickPower; //migrated;

		if (pTmpSkeleton) {
			// Extract runtime skeleton and dispose the temporary object
			m_pRuntimeSkeleton = pTmpSkeleton->getRuntimeSkeletonShared();
			auto pSkeletonCfg = pTmpSkeleton->getSkeletonConfiguration();
			if (pSkeletonCfg) {
				delete pSkeletonCfg;
			}
			pTmpSkeleton->detach();
			delete pTmpSkeleton;
			pTmpSkeleton = nullptr;
		}

		m_lifeTime = lifetime;

		m_teamRef = -1;
		m_baseFrame = NULL;
		m_startTimeStamp = 0;
		m_runtimeParticleId = 0;
		m_needInitPlay = TRUE;
		m_missileDBRef = -1;
		m_soundHandle = NULL;
		//m_burstForceType     = FALSE;
		m_burstAppliedState = FALSE;
		//m_useFullPhysics     = FALSE;
		//m_useElastics        = FALSE;
	}
}

void CMissileObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CMissileObj* misPtr = (CMissileObj*)GetNext(posLoc);
		misPtr->SafeDelete();
		delete misPtr;
		misPtr = 0;
	}
	RemoveAll();
}

void CMissileObjList::Serialize(CArchive& ar) {
	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CMissileObj* mPtr = (CMissileObj*)GetNext(posLoc);
			if (mPtr->getRuntimeSkeleton()) {
				matCompressor->addMaterials(mPtr->getRuntimeSkeleton(), nullptr);
			}
			if (mPtr->m_laserObject) {
				if (mPtr->m_laserObject->m_meshObject && mPtr->m_laserObject->m_meshObject->m_materialObject) {
					matCompressor->addMaterial(mPtr->m_laserObject->m_meshObject->m_materialObject);
				}
			}
			if (mPtr->m_basicMesh && mPtr->m_basicMesh->m_materialObject) {
				matCompressor->addMaterial(mPtr->m_basicMesh->m_materialObject);
			}
		}
		matCompressor->compress();
		matCompressor->writeToArchive(ar); // write compressed materials

		CKEPObList::Serialize(ar);
	} else {
		int version = ar.GetObjectSchema();
		if (version >= 1) {
			matCompressor->readFromArchive(ar);
		}
		CKEPObList::Serialize(ar);
	}
	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
}

IMPLEMENT_SERIAL_SCHEMA(CMissileObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CMissileObjList, CKEPObList)

} // namespace KEP
