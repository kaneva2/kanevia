///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common/include/GILCleaner.h"
#include "common/include/corestatic/CGlobalInventoryClass.h"

namespace KEP {

GILCleaner::GILCleaner() {}
GILCleaner::~GILCleaner() {}

ConcreteGILCleaner::ConcreteGILCleaner() :
		_pGIL(CGlobalInventoryObjList::GetInstance()), _glidMap(), _resource() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"GIL"), "GILCleaner::GILCleaner()");
}

ConcreteGILCleaner::~ConcreteGILCleaner() {
	LOG4CPLUS_TRACE(Logger::getInstance(L"GIL"), "GILCleaner::~GILCleaner()");
}

GILCleaner&
ConcreteGILCleaner::itemAdded(unsigned long glid) {
	std::lock_guard<fast_recursive_mutex> lock(_resource);
	LOG4CPLUS_TRACE(Logger::getInstance(L"GIL"), "GILCleaner::itemAdded(" << glid << ")");
	GLIDMapT::iterator entryIter = _glidMap.find(glid);
	if (entryIter == _glidMap.end()) {
		_glidMap[glid] = 0;
	}
	_glidMap[glid]++;
	return *this;
}

GILCleaner&
ConcreteGILCleaner::itemRemoved(unsigned long glid) {
	std::lock_guard<fast_recursive_mutex> lock(_resource);
	LOG4CPLUS_TRACE(Logger::getInstance(L"GIL"), "GILCleaner::itemRemoved(" << glid << ")");
	GLIDMapT::iterator mapIter = _glidMap.find(glid);
	if (mapIter == _glidMap.end()) {
		// FAULT.  Report it.
		//
		LOG4CPLUS_ERROR(Logger::getInstance(L"GIL"), "GILCleaner::itemRemoved(" << glid << ") glid not found!");
		return *this;
	}
	mapIter->second--;
	if (mapIter->second > 0)
		return *this;
	CGlobalInventoryObj* pObj = _pGIL->removeItemWithGLID(glid);
	if (pObj) {
		pObj->SafeDelete();
		delete pObj;
	}
	_glidMap.erase(mapIter);
	return *this;
}

std::string
ConcreteGILCleaner::toXML() const {
	std::lock_guard<fast_recursive_mutex> lock(_resource);
	std::stringstream ss;
	ss << "<GILCleaner size=\"" << _glidMap.size() << "\">";
	for (GLIDMapT::const_iterator iter = _glidMap.begin(); iter != _glidMap.end(); iter++) {
		ss << "<entry glid=\"" << iter->first << "\" count=\"" << iter->second << "\"/>";
	}
	ss << "</GILCleaner>";
	return ss.str();
}

std::string
ConcreteGILCleaner::toXMLSummary() const {
	std::lock_guard<fast_recursive_mutex> lock(_resource);
	std::stringstream ss;
	ss << "<GILCleaner size=\"" << _glidMap.size() << "\"/>";
	return ss.str();
}

std::string
MockGILCleaner::toXML() const {
	std::stringstream ss;
	ss << "<GILCleaner implementation=\"MockGILCleaner\"/>";
	return ss.str();
}

std::string
MockGILCleaner::toXMLSummary() const {
	return toXML();
}

} // namespace KEP