///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS
#include "resource.h"
#include "math.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "afxext.h"

#include "SerializeHelper.h"
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "CBoneClass.h"
#include "VertexDeclaration.h"
#include "CCharonaClass.h"
#include "CDeformableMesh.h"
#include "CMaterialEffect.h"
#include "ReMaterial.h"

#include "CSkeletonObject.h"
#include "MaterialPersistence.h"
#include "TextureDatabase.h"

#include "FlashEXMesh.h"
#include "CGlobalMaterialShaderLib.h"
#include "KEPHelpers.h"

#include "common\KEPUtil\Helpers.h"
#include "Core/Math/TransformUtil.h"
#include "Core/Math/Angle.h"
#include "UnicodeMFCHelpers.h"
#include "common/include/IMemSizeGadget.h"
#ifdef _ClientOutput
#include "IMeshRenderBlade.h"
#include "IClientEngine.h"
#endif

namespace KEP {

static LogInstance("Instance");

const int g_fileVersionCEXMeshObj = 7;

CEXMeshObj::CEXMeshObj() :
		m_externalSource(0) {
	blade_mesh = 0;
	mpBoneInfo = NULL;
	mbIsSkinned = false;
	mpEffectShader = NULL;
	mpOffsetVectsBuffer = NULL;

	mBoneData.mBoneMatrices = NULL;
	mBoneData.mNumBones = 0;

	m_lastTimeStamp = 0;
	m_chromeWrap = FALSE;
	m_chromeUvSet = 0;
	m_groupReference = -1;

	m_boundingBox.minX = 0.0f;
	m_boundingBox.maxX = 0.0f;
	m_boundingBox.minY = 0.0f;
	m_boundingBox.maxY = 0.0f;
	m_boundingBox.minZ = 0.0f;
	m_boundingBox.maxZ = 0.0f;
	m_boundingRadius = 0.0f;

	m_tempDistance = 0;
	m_blendType = -1; // 1 = none 1 = even opacity 2 = opacity map darker is more transparent
	m_textureSetList = 0;

	m_meshCenter.Set(0, 0, 0);
	m_meshBoundingSphereRadius = 0.0f;
	m_materialObject = NULL;
	m_visibilityTrack = 1;

	m_charona = NULL;
	m_shadowTemp = NULL;
	m_validUvCount = 0;
	m_byteFlagTemp = 0;
	m_fileName = "";
	m_pd3dDevice = NULL;

	m_indexBuffiltered = FALSE;
	m_Rendered = TRUE;
	m_tempSupportedMaterials = NULL;
	m_VertexArrayValid = TRUE; // RENDERENGINE INTERFACE

	m_translation.Set(0, 0, 0);
	m_scale.Set(1, 1, 1);
	m_rotation.Set(0, 0, 0);
	m_worldTransform.MakeIdentity();
}

CEXMeshObj::~CEXMeshObj() {
}

void CEXMeshObj::SetScaleTranslationRotation(const Vector3f& scale, const Vector3f& translation, const Vector3f& rotation) {
	m_scale = scale;
	m_rotation = rotation;
	m_translation = translation;
	BuildTransform();
} // CEXMeshObj::SetScaleTranslationRotation

void CEXMeshObj::SetScale(const Vector3f& scale) {
	m_scale = scale;
	BuildTransform();
} // CEXMeshObj::SetScale

void CEXMeshObj::SetRotation(const Vector3f& rotation) {
	m_rotation = rotation;
	BuildTransform();
} // CNodeObj::SetRotation

void CEXMeshObj::SetTranslation(const Vector3f& translation) {
	m_translation = translation;
	BuildTransform();
} // CNodeObj::SetTranslation

void CEXMeshObj::SetWorldTransform(const Matrix44f& worldTransform) {
	m_worldTransform = worldTransform;
	DecomposeTransform();
} // CEXMeshObj::SetWorldTransform

void CEXMeshObj::ResetWorldTransform() {
	m_scale.Set(1, 1, 1);
	m_translation.Set(0, 0, 0);
	m_rotation.Set(0, 0, 0);

	m_worldTransform.MakeIdentity();
} // CEXMeshObj::ResetWorldTransform

void CEXMeshObj::BuildTransform() {
	// [MS] Original version of CEXMeshObj::BuildTransform() built the matrix in the order: Rotation * Scale * Translation
	// However, CEXMeshObj::DecomposeTransform() decomposed it using D3DXMatrixDecompose() which uses the order: Scale * Rotation * Translation
	// S*R*T is more commonly used, so I am assuming that was the intention.
	Quaternionf q;
	ConvertRotationXYZ(ToRadians(m_rotation), out(q));
	ScaleRotationTranslationToMatrix(m_scale, q, m_translation, out(m_worldTransform));
} // CEXMeshObj::BuildTransform

void CEXMeshObj::DecomposeTransform() {
	Quaternionf qRot;
	MatrixToScaleRotationTranslation(m_worldTransform, out(m_scale), out(qRot), out(m_translation));
	Vector3f vAngles;
	ConvertRotationXYZ(qRot, out(vAngles));
	m_rotation = ToDegrees(vAngles);
} // CEXMeshObj::DecomposeTransform

void CEXMeshObj::InitBaseMaterialTextures(TextureDatabase* pDataBase, const char* subDir) {
	for (DWORD i = 0; i < ReDEVICEMAXTEXTURESTAGES; i++)
		pDataBase->TextureGet(m_materialObject->m_texNameHash[i], m_materialObject->m_texFileName[i], subDir);
}

void CEXMeshObj::scaleMat(float Xt, float Yt, float Zt, float TMatrix[4][4]) {
	TMatrix[0][0] = Xt;
	TMatrix[0][1] = 0;
	TMatrix[0][2] = 0;
	TMatrix[0][3] = 0;
	TMatrix[1][0] = 0;
	TMatrix[1][1] = Yt;
	TMatrix[1][2] = 0;
	TMatrix[1][3] = 0;
	TMatrix[2][0] = 0;
	TMatrix[2][1] = 0;
	TMatrix[2][2] = Zt;
	TMatrix[2][3] = 0;
	TMatrix[3][0] = 0;
	TMatrix[3][1] = 0;
	TMatrix[3][2] = 0;
	TMatrix[3][3] = 1;
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::OptimizeMesh() {
	int barProgress = 0;

	ABVERTEX* tempArray = NULL;
	int vCount = 0;

	if (m_indexBuffiltered == TRUE) {
		return;
	}

	m_abVertexArray.GetABVERTEXStyle(&tempArray, &vCount);
	UINT* indexConversionTable = new UINT[m_abVertexArray.m_indecieCount];
	int uniqueVerts = 0;

	for (UINT temp = 0; temp < m_abVertexArray.m_indecieCount; temp++) {
		indexConversionTable[temp] = -1;
	}
	int newVertexCount = m_abVertexArray.m_vertexCount;
	for (UINT mainLoop = 0; mainLoop < m_abVertexArray.m_vertexCount; mainLoop++) {
		// main loop
		//int currentValue = m_abVertexArray.GetVertexIndex ( mainLoop );
		for (UINT secondaryLoop = mainLoop; secondaryLoop < m_abVertexArray.m_vertexCount; secondaryLoop++) {
			// secondary loop
			//int inVal = m_abVertexArray.GetVertexIndex ( secondaryLoop );
			if (secondaryLoop != mainLoop) {
				// not the same
				if (tempArray[mainLoop].x == tempArray[secondaryLoop].x &&
					tempArray[mainLoop].y == tempArray[secondaryLoop].y &&
					tempArray[mainLoop].z == tempArray[secondaryLoop].z &&
					tempArray[mainLoop].nx == tempArray[secondaryLoop].nx &&
					tempArray[mainLoop].ny == tempArray[secondaryLoop].ny &&
					tempArray[mainLoop].nz == tempArray[secondaryLoop].nz &&
					tempArray[mainLoop].tx0.tu == tempArray[secondaryLoop].tx0.tu &&
					tempArray[mainLoop].tx0.tv == tempArray[secondaryLoop].tx0.tv &&
					tempArray[mainLoop].tx1.tu == tempArray[secondaryLoop].tx1.tu &&
					tempArray[mainLoop].tx1.tv == tempArray[secondaryLoop].tx1.tv &&
					tempArray[mainLoop].tx2.tu == tempArray[secondaryLoop].tx2.tu &&
					tempArray[mainLoop].tx2.tv == tempArray[secondaryLoop].tx2.tv &&
					tempArray[mainLoop].tx3.tu == tempArray[secondaryLoop].tx3.tu &&
					tempArray[mainLoop].tx3.tv == tempArray[secondaryLoop].tx3.tv &&
					tempArray[mainLoop].tx4.tu == tempArray[secondaryLoop].tx4.tu &&
					tempArray[mainLoop].tx4.tv == tempArray[secondaryLoop].tx4.tv &&
					tempArray[mainLoop].tx5.tu == tempArray[secondaryLoop].tx5.tu &&
					tempArray[mainLoop].tx5.tv == tempArray[secondaryLoop].tx5.tv &&
					tempArray[mainLoop].tx6.tu == tempArray[secondaryLoop].tx6.tu &&
					tempArray[mainLoop].tx6.tv == tempArray[secondaryLoop].tx6.tv &&
					tempArray[mainLoop].tx7.tu == tempArray[secondaryLoop].tx7.tu &&
					tempArray[mainLoop].tx7.tv == tempArray[secondaryLoop].tx7.tv) {
					// match
					//m_abVertexArray.SetVertexIndex ( secondaryLoop, m_abVertexArray.GetVertexIndex(mainLoop) );
					if (indexConversionTable[secondaryLoop] != -1) {
						// We havn't already counted it
						// newVertexCount--;
					}
					if (indexConversionTable[mainLoop] == -1) {
						indexConversionTable[mainLoop] = uniqueVerts;
					}
					indexConversionTable[secondaryLoop] = indexConversionTable[mainLoop];

				} // end match
			} // end not the same
		}
		if (indexConversionTable[mainLoop] == -1) {
			// Lone vertex that doesn't exist elsewhere
			indexConversionTable[mainLoop] = uniqueVerts;
			uniqueVerts++;
		} else if (indexConversionTable[mainLoop] == uniqueVerts) {
			// We found match
			uniqueVerts++;
		}
		// end secondary loop

		barProgress++;
	}
	newVertexCount = uniqueVerts;

	// end main loop

	// allocate new array
	ABVERTEX* newVertexArray = new ABVERTEX[newVertexCount];

	// build non duplicate indecie array//
	UINT* newIndecieArray;
	UINT numIndices;
	m_abVertexArray.GetVertexIndexArray(&newIndecieArray, numIndices);

	// Rebuild vertexArray//
	int dupTrace = 0;
	for (UINT indecieLoop = 0; indecieLoop < m_abVertexArray.m_vertexCount; indecieLoop++) {
		// 1
		//int		currentValue = m_abVertexArray.GetVertexIndex ( indecieLoop );
		BOOL logIt = TRUE;
		UINT count;
		for (count = 0; count < indecieLoop; count++) {
			// check to see if this has been added // newIndecieArray[indecieLoop] == indexConversionTable[count]
			if (tempArray[indecieLoop].x == tempArray[count].x &&
				tempArray[indecieLoop].y == tempArray[count].y &&
				tempArray[indecieLoop].z == tempArray[count].z &&
				tempArray[indecieLoop].nx == tempArray[count].nx &&
				tempArray[indecieLoop].ny == tempArray[count].ny &&
				tempArray[indecieLoop].nz == tempArray[count].nz &&
				tempArray[indecieLoop].tx0.tu == tempArray[count].tx0.tu &&
				tempArray[indecieLoop].tx0.tv == tempArray[count].tx0.tv &&
				tempArray[indecieLoop].tx1.tu == tempArray[count].tx1.tu &&
				tempArray[indecieLoop].tx1.tv == tempArray[count].tx1.tv &&
				tempArray[indecieLoop].tx2.tu == tempArray[count].tx2.tu &&
				tempArray[indecieLoop].tx2.tv == tempArray[count].tx2.tv &&
				tempArray[indecieLoop].tx3.tu == tempArray[count].tx3.tu &&
				tempArray[indecieLoop].tx3.tv == tempArray[count].tx3.tv &&
				tempArray[indecieLoop].tx4.tu == tempArray[count].tx4.tu &&
				tempArray[indecieLoop].tx4.tv == tempArray[count].tx4.tv &&
				tempArray[indecieLoop].tx5.tu == tempArray[count].tx5.tu &&
				tempArray[indecieLoop].tx5.tv == tempArray[count].tx5.tv &&
				tempArray[indecieLoop].tx6.tu == tempArray[count].tx6.tu &&
				tempArray[indecieLoop].tx6.tv == tempArray[count].tx6.tv &&
				tempArray[indecieLoop].tx7.tu == tempArray[count].tx7.tu &&
				tempArray[indecieLoop].tx7.tv == tempArray[count].tx7.tv) {
				// if already has been used dont log
				logIt = FALSE;
				//newIndecieArray[indecieLoop] = indexConversionTable[newIndecieArray[indecieLoop]];
				break;
			} // end if already has been used dont log
		} // end check to see if this has been added

		if (logIt == TRUE) {
			// set vertex data
			newVertexArray[dupTrace].x = tempArray[count].x;
			newVertexArray[dupTrace].y = tempArray[count].y;
			newVertexArray[dupTrace].z = tempArray[count].z;
			newVertexArray[dupTrace].nx = tempArray[count].nx;
			newVertexArray[dupTrace].ny = tempArray[count].ny;
			newVertexArray[dupTrace].nz = tempArray[count].nz;
			newVertexArray[dupTrace].tx0.tu = tempArray[count].tx0.tu;
			newVertexArray[dupTrace].tx0.tv = tempArray[count].tx0.tv;
			newVertexArray[dupTrace].tx1.tu = tempArray[count].tx1.tu;
			newVertexArray[dupTrace].tx1.tv = tempArray[count].tx1.tv;
			newVertexArray[dupTrace].tx2.tu = tempArray[count].tx2.tu;
			newVertexArray[dupTrace].tx2.tv = tempArray[count].tx2.tv;
			newVertexArray[dupTrace].tx3.tu = tempArray[count].tx3.tu;
			newVertexArray[dupTrace].tx3.tv = tempArray[count].tx3.tv;
			newVertexArray[dupTrace].tx4.tu = tempArray[count].tx4.tu;
			newVertexArray[dupTrace].tx4.tv = tempArray[count].tx4.tv;
			newVertexArray[dupTrace].tx5.tu = tempArray[count].tx5.tu;
			newVertexArray[dupTrace].tx5.tv = tempArray[count].tx5.tv;
			newVertexArray[dupTrace].tx6.tu = tempArray[count].tx6.tu;
			newVertexArray[dupTrace].tx6.tv = tempArray[count].tx6.tv;
			newVertexArray[dupTrace].tx7.tu = tempArray[count].tx7.tu;
			newVertexArray[dupTrace].tx7.tv = tempArray[count].tx7.tv;

			// adjust indecies
			//for ( UINT indLoop = 0; indLoop < m_abVertexArray.m_indecieCount; indLoop++ )
			//{				// check to see if this has been added
			//	if ( m_abVertexArray.GetVertexIndex ( indLoop ) == currentValue )
			//	{			// if already has been used dont log
			//		newIndecieArray[indLoop] = dupTrace;
			//	}			// end if already has been used dont log
			//}				// end check to see if this has been added
			//newIndecieArray[indecieLoop] = indexConversionTable[indecieLoop];
			dupTrace++;
		}
		barProgress++;
	} // end 1
	// Rebuild Index Array
	for (UINT indecieLoop = 0; indecieLoop < m_abVertexArray.m_indecieCount; indecieLoop++) {
		newIndecieArray[indecieLoop] = indexConversionTable[newIndecieArray[indecieLoop]];
	}

	if (tempArray) {
		delete[] tempArray;
		tempArray = 0;
	}

	m_abVertexArray.SetABVERTEXStyle(NULL, newVertexArray, newVertexCount);
	m_abVertexArray.SetVertexIndexArray(newIndecieArray, numIndices, newVertexCount);
	if (newIndecieArray) {
		delete[] newIndecieArray;
		newIndecieArray = 0;
	}
	if (newVertexArray) {
		delete[] newVertexArray;
		newVertexArray = 0;
	}
	if (indexConversionTable) {
		delete[] indexConversionTable;
		indexConversionTable = 0;
	}

	InitBuffer(m_pd3dDevice, NULL);

	m_indexBuffiltered = TRUE;
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::OptimizeMesh(float smoothShadeNormalReqAngle) {
	int barProgress = 0;

	ABVERTEX* tempArray = NULL;
	int vCount = 0;

	m_abVertexArray.GetABVERTEXStyle(&tempArray, &vCount);

	Vector3f tempOne, tempTwo;
	int newVertexCount = m_abVertexArray.m_vertexCount;
	for (UINT mainLoop = 0; mainLoop < m_abVertexArray.m_indecieCount; mainLoop++) {
		// main loop
		int currentValue = m_abVertexArray.GetVertexIndex(mainLoop);
		for (UINT secondaryLoop = 0; secondaryLoop < m_abVertexArray.m_indecieCount; secondaryLoop++) {
			// secondary loop
			int inVal = m_abVertexArray.GetVertexIndex(secondaryLoop);
			if (secondaryLoop != mainLoop && inVal != currentValue) {
				// not the same
				if (tempArray[currentValue].x != tempArray[inVal].x) {
					continue;
				}

				if (tempArray[currentValue].y == tempArray[inVal].y &&
					tempArray[currentValue].z == tempArray[inVal].z &&
					tempArray[currentValue].tx0.tu == tempArray[inVal].tx0.tu &&
					tempArray[currentValue].tx0.tv == tempArray[inVal].tx0.tv &&
					tempArray[currentValue].tx1.tu == tempArray[inVal].tx1.tu &&
					tempArray[currentValue].tx1.tv == tempArray[inVal].tx1.tv &&
					tempArray[currentValue].tx2.tu == tempArray[inVal].tx2.tu &&
					tempArray[currentValue].tx2.tv == tempArray[inVal].tx2.tv &&
					tempArray[currentValue].tx3.tu == tempArray[inVal].tx3.tu &&
					tempArray[currentValue].tx3.tv == tempArray[inVal].tx3.tv &&
					tempArray[currentValue].tx4.tu == tempArray[inVal].tx4.tu &&
					tempArray[currentValue].tx4.tv == tempArray[inVal].tx4.tv &&
					tempArray[currentValue].tx5.tu == tempArray[inVal].tx5.tu &&
					tempArray[currentValue].tx5.tv == tempArray[inVal].tx5.tv &&
					tempArray[currentValue].tx6.tu == tempArray[inVal].tx6.tu &&
					tempArray[currentValue].tx6.tv == tempArray[inVal].tx6.tv &&
					tempArray[currentValue].tx7.tu == tempArray[inVal].tx7.tu &&
					tempArray[currentValue].tx7.tv == tempArray[inVal].tx7.tv)

				{
					// match
					tempOne.x = tempArray[inVal].nx;
					tempOne.y = tempArray[inVal].ny;
					tempOne.z = tempArray[inVal].nz;
					tempTwo.x = tempArray[currentValue].nx;
					tempTwo.y = tempArray[currentValue].ny;
					tempTwo.z = tempArray[currentValue].nz;

					float angleDiff = GetAngleBetween(tempOne, tempTwo);

					if (angleDiff <= smoothShadeNormalReqAngle) {
						m_abVertexArray.SetVertexIndex(secondaryLoop, currentValue);
						newVertexCount--;
					}
				} // end match
			} // end not the same
		} // end secondary loop

		barProgress++;
	} // end main loop

	// allocate new array
	ABVERTEX* newVertexArray = new ABVERTEX[newVertexCount];

	// build non duplicate indecie array//
	UINT* newIndecieArray;
	UINT numIndices;
	m_abVertexArray.GetVertexIndexArray(&newIndecieArray, numIndices);

	// Rebuild vertexArray and indecie-//
	int dupTrace = 0;
	for (UINT indecieLoop = 0; indecieLoop < m_abVertexArray.m_indecieCount; indecieLoop++) {
		// 1
		int currentValue = m_abVertexArray.GetVertexIndex(indecieLoop);
		BOOL logIt = TRUE;
		for (UINT count = 0; count < indecieLoop; count++) {
			// check to see if this has been added
			if (m_abVertexArray.GetVertexIndex(count) == currentValue) {
				// if already has been used dont log
				logIt = FALSE;
				break;
			} // end if already has been used dont log
		} // end check to see if this has been added

		if (logIt == TRUE) {
			// set vertex data
			newVertexArray[dupTrace].x = tempArray[currentValue].x;
			newVertexArray[dupTrace].y = tempArray[currentValue].y;
			newVertexArray[dupTrace].z = tempArray[currentValue].z;
			newVertexArray[dupTrace].nx = tempArray[currentValue].nx;
			newVertexArray[dupTrace].ny = tempArray[currentValue].ny;
			newVertexArray[dupTrace].nz = tempArray[currentValue].nz;
			newVertexArray[dupTrace].tx0.tu = tempArray[currentValue].tx0.tu;
			newVertexArray[dupTrace].tx0.tv = tempArray[currentValue].tx0.tv;
			newVertexArray[dupTrace].tx1.tu = tempArray[currentValue].tx1.tu;
			newVertexArray[dupTrace].tx1.tv = tempArray[currentValue].tx1.tv;
			newVertexArray[dupTrace].tx2.tu = tempArray[currentValue].tx2.tu;
			newVertexArray[dupTrace].tx2.tv = tempArray[currentValue].tx2.tv;
			newVertexArray[dupTrace].tx3.tu = tempArray[currentValue].tx3.tu;
			newVertexArray[dupTrace].tx3.tv = tempArray[currentValue].tx3.tv;
			newVertexArray[dupTrace].tx4.tu = tempArray[currentValue].tx4.tu;
			newVertexArray[dupTrace].tx4.tv = tempArray[currentValue].tx4.tv;
			newVertexArray[dupTrace].tx5.tu = tempArray[currentValue].tx5.tu;
			newVertexArray[dupTrace].tx5.tv = tempArray[currentValue].tx5.tv;
			newVertexArray[dupTrace].tx6.tu = tempArray[currentValue].tx6.tu;
			newVertexArray[dupTrace].tx6.tv = tempArray[currentValue].tx6.tv;
			newVertexArray[dupTrace].tx7.tu = tempArray[currentValue].tx7.tu;
			newVertexArray[dupTrace].tx7.tv = tempArray[currentValue].tx7.tv;

			// adjust indecies
			for (UINT indLoop = 0; indLoop < m_abVertexArray.m_indecieCount; indLoop++) {
				// check to see if this has been added
				if (m_abVertexArray.GetVertexIndex(indLoop) == currentValue) {
					// if already has been used dont log
					newIndecieArray[indLoop] = dupTrace;
				} // end if already has been used dont log
			} // end check to see if this has been added

			dupTrace++;
		}

		barProgress++;
	} // end 1

	if (tempArray) {
		delete[] tempArray;
		tempArray = 0;
	}

	m_abVertexArray.SetVertexIndexArray(newIndecieArray, numIndices, newVertexCount);

	delete[] newIndecieArray;

	m_abVertexArray.SetABVERTEXStyle(NULL, newVertexArray, newVertexCount);
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
BOOL CEXMeshObj::UpdateShadowMesh(Matrix44f& objectMatrix,
	Matrix44f& shadowDifference,
	LPDIRECT3DDEVICE9 g_pd3dDevice) {
	return TRUE;
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//

void CEXMeshObj::InitBuffer(LPDIRECT3DDEVICE9 g_pd3dDevice,
	CGlobalMaterialShader* optionalShader,
	CMaterialObject* custMaterial) {
	//if cusMaterial is set, use it. otherwise use the material in the mesh
	CMaterialObject* materialPtr = custMaterial == NULL ? m_materialObject : custMaterial;
	if (!g_pd3dDevice) {
		return;
	}

	// m_abVertexArray.m_uvCount = GetUVlayerCount();
	int uvTarget = m_abVertexArray.m_uvCount;
	if (optionalShader) {
		uvTarget = optionalShader->m_requiredUvCount;
		if (optionalShader->m_enableChroming) {
			this->m_chromeUvSet = optionalShader->m_chromeUvSet;
			this->m_chromeWrap = optionalShader->m_enableChroming;
		}

		if (optionalShader->m_useOverrideMaterial) {
			CopyMemory(&materialPtr->m_useMaterial, &optionalShader->m_overrideMaterial, sizeof(D3DMATERIAL9));
			CopyMemory(&materialPtr->m_baseMaterial, &optionalShader->m_overrideMaterial, sizeof(D3DMATERIAL9));
		}

		for (int i = 0; i < 7; i++) {
			if (optionalShader->m_blendArray[i] > -1) {
				if (i == 0) {
					materialPtr->m_texBlendMethod[9] = optionalShader->m_blendArray[i];
				} else {
					materialPtr->m_texBlendMethod[i - 1] = optionalShader->m_blendArray[i];
				}
			}

			if (optionalShader->m_textureIndexArray[i] != -1) {
				materialPtr->m_texNameHash[i] = optionalShader->m_textureIndexArray[i];
			}
		}
	}

	m_abVertexArray.InitBuffer(g_pd3dDevice, materialPtr->m_diffuseEffect, uvTarget);

	//	Create the D3DX vertex buffers for bones
	//	TODO: This function should go through all vertex buffers and create the actual
	//	D3DX object
	if (mpBoneInfo && mbIsSkinned)
		mpBoneInfo->CreateAndFillBuffer(g_pd3dDevice);

	if (mpOffsetVectsBuffer && mbIsSkinned)
		mpOffsetVectsBuffer->CreateAndFillBuffer(g_pd3dDevice);
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::ExchangeTextureSets(int level, int level2) {
	/*ABVERTEX * vertexArrayCopy = new ABVERTEX[m_abVertexArray.m_vertexCount];
	CopyMemory(vertexArrayCopy,m_abVertexArray.m_origVertexArray,(sizeof(ABVERTEX)*m_abVertexArray.m_vertexCount));

	//switch texture setting
	int texturesetstore1 = m_materialObject->m_textureSelect[level];
	CStringA textureStoredString = m_materialObject->m_storedTextureFromFile[level];
	m_materialObject->m_textureSelect[level] = m_materialObject->m_textureSelect[level2];
	m_materialObject->m_storedTextureFromFile[level] = m_materialObject->m_storedTextureFromFile[level2];
	m_materialObject->m_textureSelect[level2] = texturesetstore1;
	m_materialObject->m_storedTextureFromFile[level2] = textureStoredString;
	//switch coordinates
	for(int loop=0;loop<m_abVertexArray.m_vertexCount;loop++)
	{
	    if(level == 0 || level == 1)
			if(level2 == 0 || level2 == 1){
		       m_abVertexArray.m_origVertexArray[loop].tx1.tu = vertexArrayCopy[loop].tx0.tu;
		       m_abVertexArray.m_origVertexArray[loop].tx1.tv = vertexArrayCopy[loop].tx0.tv;
		       m_abVertexArray.m_origVertexArray[loop].tx0.tu = vertexArrayCopy[loop].tx1.tu;
		       m_abVertexArray.m_origVertexArray[loop].tx0.tv = vertexArrayCopy[loop].tx1.tv;
			}
	    if(level == 0 || level == 2)
			if(level2 == 0 || level2 == 2){
		       m_abVertexArray.m_origVertexArray[loop].tx2.tu = vertexArrayCopy[loop].tx0.tu;
		       m_abVertexArray.m_origVertexArray[loop].tx2.tv = vertexArrayCopy[loop].tx0.tv;
		       m_abVertexArray.m_origVertexArray[loop].tx0.tu = vertexArrayCopy[loop].tx2.tu;
		       m_abVertexArray.m_origVertexArray[loop].tx0.tv = vertexArrayCopy[loop].tx2.tv;
			}

	}
	//free memory
	if(vertexArrayCopy)
	   delete [] vertexArrayCopy;
	   vertexArrayCopy=0;


	*/
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::SafeDelete() {
	if (m_shadowTemp) {
		m_shadowTemp->SafeDelete();
		delete m_shadowTemp;
		m_shadowTemp = 0;
	}

	if (m_charona) {
		m_charona->SafeDelete();
		delete m_charona;
		m_charona = 0;
	}

	if (m_textureSetList) {
		// valid
		DumpTextureCoordSets();
		if (m_textureSetList) {
			delete m_textureSetList;
		}

		m_textureSetList = 0;
	} // end valid

	if (m_materialObject) {
		delete m_materialObject;
	}
	m_materialObject = NULL;
	m_abVertexArray.SafeDelete();
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//update texture coords for sphere map realtime
BOOL CEXMeshObj::ApplySphereMappingToObject(LPDIRECT3DDEVICE9 g_pd3dDevice, int& vertexType, int uvSet) {
	// Get the current world-view matrix
	Matrix44f matWorld, matView, matWV;
	g_pd3dDevice->GetTransform(D3DTS_VIEW, reinterpret_cast<D3DMATRIX*>(&matView));
	g_pd3dDevice->GetTransform(D3DTS_WORLD, reinterpret_cast<D3DMATRIX*>(&matWorld));
	matWV = matWorld * matView;
	m_abVertexArray.ApplySphereAlgorithm(matWV, vertexType, uvSet);

	return TRUE;
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
BOOL CEXMeshObj::Clone(CEXMeshObj** copy, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CEXMeshObj* copyLocal = new CEXMeshObj();

	if (m_VertexArrayValid) {
		m_abVertexArray.CloneMinimum2(&copyLocal->m_abVertexArray, NULL); // RENDERENGINE INTERFACE
	}

	copyLocal->m_chromeWrap = m_chromeWrap;
	copyLocal->m_chromeUvSet = m_chromeUvSet;

	copyLocal->m_Rendered = m_Rendered;

	if (m_charona) {
		m_charona->Clone(&copyLocal->m_charona);
	}

	if (m_materialObject) {
		//copyLocal->m_materialObject = new CMaterialObject;
		m_materialObject->Clone(&copyLocal->m_materialObject);
	} else {
		copyLocal->m_materialObject = NULL;
	}

	copyLocal->m_blendType = m_blendType; // 1 = none 1 = even opacity 2 = opacity map darker is more transparent
	copyLocal->m_worldTransform = m_worldTransform;
	copyLocal->m_translation = m_translation;
	copyLocal->m_scale = m_scale;
	copyLocal->m_rotation = m_rotation;

	copyLocal->m_byteFlagTemp = m_byteFlagTemp;

	copyLocal->m_meshCenter.x = m_meshCenter.x;
	copyLocal->m_meshCenter.y = m_meshCenter.y;
	copyLocal->m_meshCenter.z = m_meshCenter.z;
	copyLocal->m_groupReference = -1;

	copyLocal->m_boundingBox.minX = m_boundingBox.minX;
	copyLocal->m_boundingBox.maxX = m_boundingBox.maxX;
	copyLocal->m_boundingBox.minY = m_boundingBox.minY;
	copyLocal->m_boundingBox.maxY = m_boundingBox.maxY;
	copyLocal->m_boundingBox.minZ = m_boundingBox.minZ;
	copyLocal->m_boundingBox.maxZ = m_boundingBox.maxZ;

	copyLocal->m_boundingRadius = m_boundingRadius;

	copyLocal->m_meshBoundingSphereRadius = m_meshBoundingSphereRadius;
	copyLocal->m_visibilityTrack = 1;
	if (m_materialObject) { // ai doesn't have this
		copyLocal->m_materialObject->m_texFileName[0] = m_materialObject->m_texFileName[0];
		copyLocal->m_materialObject->m_texFileName[1] = m_materialObject->m_texFileName[1];
		copyLocal->m_materialObject->m_texFileName[2] = m_materialObject->m_texFileName[2];
		copyLocal->m_materialObject->m_texFileName[3] = m_materialObject->m_texFileName[3];
		copyLocal->m_materialObject->m_texFileName[4] = m_materialObject->m_texFileName[4];
		copyLocal->m_materialObject->m_texFileName[5] = m_materialObject->m_texFileName[5];
		copyLocal->m_materialObject->m_texFileName[6] = m_materialObject->m_texFileName[6];
		copyLocal->m_materialObject->m_texFileName[7] = m_materialObject->m_texFileName[7];

		CopyMemory(copyLocal->m_materialObject->m_texBlendMethod, m_materialObject->m_texBlendMethod, (sizeof(int) * 10));
		CopyMemory(copyLocal->m_materialObject->m_texNameHash, m_materialObject->m_texNameHash, (sizeof(int) * 10));
	}
	copyLocal->mbIsSkinned = mbIsSkinned;

	copyLocal->m_fileName = m_fileName;

	copyLocal->m_VertexArrayValid = m_VertexArrayValid;

	*copy = copyLocal;

	if (g_pd3dDevice) {
		copyLocal->InitBuffer(g_pd3dDevice, NULL);
	}

	return TRUE;
}

void CEXMeshObj::PreRenderFX(LPDIRECT3DDEVICE9 g_pd3dDevice) {
	if (!mpEffectShader) {
		mpEffectShader = MaterialEffect::GetMaterialEffect("skinned.fx", g_pd3dDevice);
	}
}

void CEXMeshObj::Draw(LPDIRECT3DDEVICE9 g_pd3dDevice) {
	int vertexType = 1;
	//	If we have a skinning buffer, set it.
	//	TODO: All a mesh should really have is a collection of buffers and a vertex dcl.
	//	that way, we can just set buffers to streams as we have them.
	if (mpBoneInfo && mpBoneInfo->mpBuffer) {
		g_pd3dDevice->SetStreamSource(1, mpBoneInfo->mpBuffer, 0, mpBoneInfo->mStride);
	}

	if (mpOffsetVectsBuffer && mpOffsetVectsBuffer->mpBuffer) {
		g_pd3dDevice->SetStreamSource(2, mpOffsetVectsBuffer->mpBuffer, 0, mpOffsetVectsBuffer->mStride);
	}

	if (m_abVertexArray.mVertexDeclaration->mpDeclaration) {
		g_pd3dDevice->SetVertexDeclaration(m_abVertexArray.mVertexDeclaration->mpDeclaration);
	}

	//	Render the real mesh data
	m_abVertexArray.Render(g_pd3dDevice, vertexType);
}

void CEXMeshObj::RenderFX(LPDIRECT3DDEVICE9 g_pd3dDevice, EffectRenderData* pRenderData) {
	if (!mpEffectShader)
		return;

	unsigned int numPasses = mpEffectShader->Begin();

	for (unsigned int i = 0; i < numPasses; i++) {
		mpEffectShader->BeginPass(i);
		{
			if (mpEffectShader->Prepare(&mBoneData, pRenderData)) {
				mpEffectShader->CommitChanges();
				this->Draw(g_pd3dDevice);
			}
		}
		mpEffectShader->EndPass();
	}

	mpEffectShader->End();
}
void CEXMeshObj::PostRenderFX(LPDIRECT3DDEVICE9 g_pd3dDevice) {
	g_pd3dDevice->SetStreamSource(1, NULL, 0, 0);
	g_pd3dDevice->SetStreamSource(2, NULL, 0, 0);
	g_pd3dDevice->SetVertexShader(NULL);
	g_pd3dDevice->SetPixelShader(NULL);
}

int CEXMeshObj::Render(LPDIRECT3DDEVICE9 g_pd3dDevice, int& vertexType, int uvSet) {
	// probably blade mesh will be replaced with a Node * into the object graph library
	if (m_externalSource != 0) {
		int ret = m_externalSource->Render(g_pd3dDevice, this, vertexType);
		if (ret > 0)
			return ret;
	}

	if (!blade_mesh) {
		if (m_chromeWrap) {
			ApplySphereMappingToObject(g_pd3dDevice, vertexType, uvSet);
		}

		return m_abVertexArray.Render(g_pd3dDevice, vertexType);
	} else {
		return 0;
	}
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-------------------render with vertex buffer---------------------------------//
int CEXMeshObj::RenderFast(LPDIRECT3DDEVICE9 g_pd3dDevice, int& vertexType, int& uvSet) {
	// probably blade mesh will be replaced with a Node * into the object graph library
	if (!blade_mesh) {
		if (m_chromeWrap) {
			ApplySphereMappingToObject(g_pd3dDevice, vertexType, uvSet);
		}

		if (this->m_abVertexArray.m_uvCount == 50) {
			g_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP,
				0,
				0, // UINT MinIndex,
				m_abVertexArray.m_vertexCount, // UINT NumVertices,
				0, // UINT StartIndex,
				m_abVertexArray.m_indecieCount - 2 // UINT PrimitiveCount
			);
			return (m_abVertexArray.m_indecieCount - 2) * 3;
		} else {
			g_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,
				0,
				0, // UINT MinIndex,
				m_abVertexArray.m_vertexCount, // UINT NumVertices,
				0, // UINT StartIndex,
				(m_abVertexArray.m_indecieCount / 3) // UINT PrimitiveCount
			);
			return m_abVertexArray.m_indecieCount;
		}

	} else {
		return 0;
	}
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-------------------render with vertex buffer---------------------------------//
void CEXMeshObj::DumpTextureCoordSets() {
	if (m_textureSetList) {
		// valid
		for (POSITION posLoc = m_textureSetList->GetHeadPosition(); posLoc != NULL;) {
			CTextureSetObj* delPtr = (CTextureSetObj*)m_textureSetList->GetNext(posLoc);
			if (delPtr->m_textureSet) {
				delete[] delPtr->m_textureSet;
			}

			delPtr->m_textureSet = 0;
			delete delPtr;
			delPtr = 0;
		}

		m_textureSetList->RemoveAll();
	} // end valid
}

int CEXMeshObj::GetUVlayerCount() {
	int returnVal = 0;
	for (int loop = 0; loop < 8; loop++) {
		if (ValidTextureNameHash(m_materialObject->m_texNameHash[loop]))
			returnVal++;
	}
	m_validUvCount = returnVal;
	return returnVal;
}

int CEXMeshObj::GetUVlayerCountString() {
	int returnVal = 0;
	for (int loop = 0; loop < 8; loop++) {
		if (ValidTextureFileName(m_materialObject->m_texFileName[loop])) {
			m_materialObject->m_texNameHash[loop] = 0; //DRF ? INVALID_TEXTURE_NAMEHASH;
			returnVal++;
		} else
			break;
	}
	m_validUvCount = returnVal;
	m_abVertexArray.m_uvCount = m_validUvCount;
	return returnVal;
}

void CEXMeshObj::ConvertToX(CStringA* fileString) {
	CStringA ext = CABFunctionLib::GetFileExt(*fileString);
	ext.MakeLower();
	if (ext == "3ds") {
		// begin extension conversion
		CStringA final = operator+("conv3ds -x -m ", *fileString);
		system(final);
		CABFunctionLib::RemoveFileExt(fileString);
		CABFunctionLib::AddNewFileExt(fileString, ".x");
	}
}

BOOL CEXMeshObj::SetGeometry2(const std::string& texFileName,
	ABVERTEX1L* vertexArray,
	WORD vertexCount,
	WORD* indecieArray,
	WORD indecieCount) {
	m_abVertexArray.m_uvCount = 1;
	m_abVertexArray.SetVertexIndexArray(indecieArray, indecieCount, vertexCount);
	delete[] indecieArray;

	m_abVertexArray.m_vertexArray1 = vertexArray;

	m_materialObject->m_texFileName[0] = texFileName;
	m_materialObject->m_texNameHash[0] = 0; // DRF ? INVALID_TEXTURE_NAMEHASH;

	ComputeBoundingVolumes();

	return TRUE;
}

BOOL CEXMeshObj::LoadGeometryFromPlugin(const CStringA& fileName) {
	float sightBasis = 0.0f;
	BOOL smoothShading = FALSE;
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, fileName, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	m_fileName = fileName;

	// default fileVersion == 0 for .arw files
	int fileVersion = 0;

	// default (minimum fileVersion == 1 for .kzm files, we'll determine exact number from file
	CStringA lcFileName = fileName;
	lcFileName.MakeLower();
	std::string lcFNkstr = lcFileName;
	if (StrSameFileExt(lcFNkstr, "kzm"))
		fileVersion = 1;

	BEGIN_SERIALIZE_TRY

	CArchive the_in_Archive(&fl, CArchive::load);

	if (fileVersion < 1) {
		USHORT vCount, iCount;
		the_in_Archive >> vCount >> iCount;
		m_abVertexArray.m_vertexCount = vCount;
		m_abVertexArray.m_indecieCount = iCount;
	} else {
		the_in_Archive >> fileVersion;

		the_in_Archive >> m_abVertexArray.m_vertexCount >> m_abVertexArray.m_indecieCount;
	}

	the_in_Archive >> sightBasis >> smoothShading;

	// for boundingBox Generation
	m_sightBasis = fabs(sightBasis);

	//Vertexes
	ABVERTEX* abVertex = NULL;

	if (m_abVertexArray.m_vertexCount > 0) //allocate
		abVertex = new ABVERTEX[m_abVertexArray.m_vertexCount];
	for (UINT loop = 0; loop < m_abVertexArray.m_vertexCount; loop++) { //loop
		the_in_Archive >> abVertex[loop].x >> abVertex[loop].y >> abVertex[loop].z >> abVertex[loop].nx >> abVertex[loop].ny >> abVertex[loop].nz >> abVertex[loop].tx0.tu >> abVertex[loop].tx0.tv >> abVertex[loop].tx1.tu >> abVertex[loop].tx1.tv >> abVertex[loop].tx2.tu >> abVertex[loop].tx2.tv >> abVertex[loop].tx3.tu >> abVertex[loop].tx3.tv >> abVertex[loop].tx4.tu >> abVertex[loop].tx4.tv >> abVertex[loop].tx5.tu >> abVertex[loop].tx5.tv >> abVertex[loop].tx6.tu >> abVertex[loop].tx6.tv >> abVertex[loop].tx7.tu >> abVertex[loop].tx7.tv;
	} //end loop

	//indecies
	if (fileVersion < 1 || (m_abVertexArray.m_vertexCount <= USHRT_MAX)) {
		WORD* indexArray = new WORD[m_abVertexArray.m_indecieCount];
		for (UINT loop = 0; loop < m_abVertexArray.m_indecieCount; loop++)
			the_in_Archive >> indexArray[loop];
		m_abVertexArray.SetVertexIndexArray(indexArray, m_abVertexArray.m_indecieCount, m_abVertexArray.m_vertexCount);
		delete[] indexArray;
		indexArray = 0;
	} else {
		UINT* indexArray = new UINT[m_abVertexArray.m_indecieCount];
		for (UINT loop = 0; loop < m_abVertexArray.m_indecieCount; loop++)
			the_in_Archive >> indexArray[loop];
		m_abVertexArray.SetVertexIndexArray(indexArray, m_abVertexArray.m_indecieCount, m_abVertexArray.m_vertexCount);
		delete[] indexArray;
		indexArray = 0;
	}

	//textures
	CStringA tempTexture[8];
	for (UINT loop = 0; loop < 7; loop++)
		the_in_Archive >> tempTexture[loop];

	CStringA extVersion = 0;
	CStringA reservedX = "";
	CStringA smAngleBasis = "";
	the_in_Archive >> reservedX;
	the_in_Archive >> extVersion;
	the_in_Archive >> smAngleBasis;

	m_smoothAngle = (float)atoi(smAngleBasis);

	CMaterialObject* carriedMaterial = NULL;

	int ver = atoi(extVersion);
	if (ver > 1) {
	}
	if (ver > 2) {
		the_in_Archive >> carriedMaterial;
	}

	if (carriedMaterial) {
		if (m_materialObject) {
			delete m_materialObject;
			m_materialObject = 0;
		}
		m_materialObject = carriedMaterial;
	} else if (!m_materialObject)
		m_materialObject = new CMaterialObject();

	//set the texture stages
	for (UINT loop = 0; loop < 7; loop++) {
		m_materialObject->m_texFileName[loop] = (const char*)tempTexture[loop];
		if (loop > 0 && !tempTexture[loop].IsEmpty() && tempTexture[loop].CompareNoCase("NONE") != 0) {
			// valid texture: set blend method
			m_materialObject->m_texBlendMethod[loop - 1] = 0;
		}
	}

	//clean indecies if odd numbered from 3 to avoid crash
	int expanded = m_abVertexArray.m_indecieCount / 3;
	m_abVertexArray.m_indecieCount = expanded * 3;

	GetUVlayerCountString();

	m_abVertexArray.SetABVERTEXStyleData(abVertex, m_abVertexArray.m_vertexCount);

	if (abVertex) {
		delete[] abVertex;
		abVertex = 0;
	}

	the_in_Archive.Close();
	fl.Close();

	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

BOOL CEXMeshObj::InitAfterLoad(LPDIRECT3DDEVICE9 g_pd3dDevice, bool aOptimize) {
	//GenerateNormals(0);
	//if( aOptimize )
	//OptimizeMesh(m_smoothAngle);
	//m_abVertexArray.AVGNormals(smoothAngle);
	//m_abVertexArray.UnifyNormals(smoothAngle);
	m_abVertexArray.NormalizeOnly();

	//GenerateNormals(0);
	//UnifyNormals(smoothAngle);
	//GenerateSmoothNormals();
	ComputeBoundingVolumes();
	if (m_abVertexArray.m_indicieBuffer) {
		m_abVertexArray.m_indicieBuffer->Release();
		m_abVertexArray.m_indicieBuffer = 0;
	}

	InitBuffer(g_pd3dDevice, 0);
	return TRUE;
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::UnifyNormals() {
	GenerateNormals(0);

	float tolerance = .002f;
	ABVERTEX* vArray = NULL;
	int vCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&vArray, &vCount);

	ABVERTEX* chkvArray = NULL;
	int chkvCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&chkvArray, &chkvCount);

	for (UINT pvLoop = 0; pvLoop < m_abVertexArray.m_vertexCount; pvLoop++) {
		// primary mesh loop
		for (UINT scndLoop = 0; scndLoop < m_abVertexArray.m_vertexCount; scndLoop++) {
			// secondary mesh loop
			if (pvLoop == scndLoop) {
				continue;
			}

			if (fabs(vArray[pvLoop].x - chkvArray[scndLoop].x) > tolerance) {
				continue;
			}

			if (fabs(vArray[pvLoop].y - chkvArray[scndLoop].y) > tolerance) {
				continue;
			}

			if (fabs(vArray[pvLoop].z - chkvArray[scndLoop].z) > tolerance) {
				continue;
			}
			/*float dist = m_matrixFunctions.MagnitudeSquared(vArray[pvLoop].x,
			                                               vArray[pvLoop].y,
														   vArray[pvLoop].z,
														   chkvArray[scndLoop].x,
														   chkvArray[scndLoop].y,
														   chkvArray[scndLoop].z);*/
			// if(dist < tolerance)
			// {//colapse
			chkvArray[scndLoop].nx += vArray[pvLoop].nx;
			chkvArray[scndLoop].ny += vArray[pvLoop].ny;
			chkvArray[scndLoop].nz += vArray[pvLoop].nz;

			// }//end colapse
		} // end secondary mesh loop
	} // end primary mesh loop

	for (UINT pvLoop = 0; pvLoop < m_abVertexArray.m_vertexCount; pvLoop++) {
		// primary mesh loop
		Vector3f nTemp;
		nTemp.x = vArray[pvLoop].nx;
		nTemp.y = vArray[pvLoop].ny;
		nTemp.z = vArray[pvLoop].nz;
		nTemp.Normalize();
		for (UINT scndLoop = 0; scndLoop < m_abVertexArray.m_vertexCount; scndLoop++) {
			// secondary mesh loop
			// if(pvLoop == scndLoop)
			// continue;
			float dist = Distance(
				reinterpret_cast<const Vector3f&>(vArray[pvLoop].x),
				reinterpret_cast<const Vector3f&>(chkvArray[scndLoop].x));
			if (dist < tolerance) {
				// colapse
				chkvArray[scndLoop].nx = nTemp.x;
				chkvArray[scndLoop].ny = nTemp.y;
				chkvArray[scndLoop].nz = nTemp.z;
			} // end colapse
		} // end secondary mesh loop
	} // end primary mesh loop

	if (chkvArray) {
		m_abVertexArray.SetABVERTEXStyle(NULL, chkvArray, chkvCount);
		delete[] chkvArray;
		chkvArray = 0;
	}

	if (vArray) {
		delete[] vArray;
		vArray = 0;
	}
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::UnifyNormals(float angleTolerance) {
	Vector3f tempOne, tempTwo;
	float tolerance = .002f;
	ABVERTEX* vArray = NULL;
	int vCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&vArray, &vCount);

	ABVERTEX* chkvArray = NULL;
	int chkvCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&chkvArray, &chkvCount);

	for (UINT pvLoop = 0; pvLoop < m_abVertexArray.m_vertexCount; pvLoop++) {
		// primary mesh loop
		for (UINT scndLoop = 0; scndLoop < m_abVertexArray.m_vertexCount; scndLoop++) {
			// secondary mesh loop
			if (pvLoop == scndLoop) {
				continue;
			}

			if (fabs(vArray[pvLoop].x - chkvArray[scndLoop].x) > tolerance) {
				continue;
			}

			if (fabs(vArray[pvLoop].y - chkvArray[scndLoop].y) > tolerance) {
				continue;
			}

			if (fabs(vArray[pvLoop].z - chkvArray[scndLoop].z) > tolerance) {
				continue;
			}

			/*float dist = m_matrixFunctions.MagnitudeSquared(vArray[pvLoop].x,
			                                               vArray[pvLoop].y,
														   vArray[pvLoop].z,
														   chkvArray[scndLoop].x,
														   chkvArray[scndLoop].y,
														   chkvArray[scndLoop].z);*/
			// if(dist < tolerance)
			// {
			tempOne.x = chkvArray[scndLoop].nx;
			tempOne.y = chkvArray[scndLoop].ny;
			tempOne.z = chkvArray[scndLoop].nz;
			tempTwo.x = vArray[pvLoop].nx;
			tempTwo.y = vArray[pvLoop].ny;
			tempTwo.z = vArray[pvLoop].nz;

			float angleDiff = GetAngleBetween(tempOne, tempTwo);

			if (angleDiff <= angleTolerance) {
				// colapse
				chkvArray[scndLoop].nx += vArray[pvLoop].nx;
				chkvArray[scndLoop].ny += vArray[pvLoop].ny;
				chkvArray[scndLoop].nz += vArray[pvLoop].nz;
			} // end colapse

			// }
		} // end secondary mesh loop
	} // end primary mesh loop

	for (UINT pvLoop = 0; pvLoop < m_abVertexArray.m_vertexCount; pvLoop++) {
		// primary mesh loop
		Vector3f nTemp;
		nTemp.x = vArray[pvLoop].nx;
		nTemp.y = vArray[pvLoop].ny;
		nTemp.z = vArray[pvLoop].nz;
		nTemp.Normalize();
		for (UINT scndLoop = 0; scndLoop < m_abVertexArray.m_vertexCount; scndLoop++) {
			// secondary mesh loop
			// if(pvLoop == scndLoop)
			// continue;
			float dist = Distance(
				reinterpret_cast<const Vector3f&>(vArray[pvLoop].x),
				reinterpret_cast<const Vector3f&>(chkvArray[scndLoop].x));
			if (dist < tolerance) {
				tempOne.x = chkvArray[scndLoop].nx;
				tempOne.y = chkvArray[scndLoop].ny;
				tempOne.z = chkvArray[scndLoop].nz;
				tempTwo.x = vArray[pvLoop].nx;
				tempTwo.y = vArray[pvLoop].ny;
				tempTwo.z = vArray[pvLoop].nz;

				float angleDiff = GetAngleBetween(tempOne, tempTwo);

				if (angleDiff <= angleTolerance) {
					// colapse
					chkvArray[scndLoop].nx = nTemp.x;
					chkvArray[scndLoop].ny = nTemp.y;
					chkvArray[scndLoop].nz = nTemp.z;
				} // end colapse
			}
		} // end secondary mesh loop
	} // end primary mesh loop

	if (chkvArray) {
		m_abVertexArray.SetABVERTEXStyle(NULL, chkvArray, chkvCount);
		delete[] chkvArray;
		chkvArray = 0;
	}

	if (vArray) {
		delete[] vArray;
		vArray = 0;
	}
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
BOOL CEXMeshObj::GetGeometryFromPlugin(const CStringA& fileName,
	WORD* vertexCount,
	ABVERTEX** vertexArray,
	WORD* indecieCount,
	WORD** indecieArray,
	CStringA texture[10]) {
	WORD L_vertexCount = 0;
	ABVERTEX* L_vertexArray = NULL;
	float L_activeDistance = 0;
	WORD* L_indecieArray = NULL;
	WORD L_indecieCount = 0;

	BOOL L_smoothShading = FALSE;
	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, fileName, CFile::modeRead, &exc);
	if (!res) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		return FALSE;
	}

	m_fileName = fileName;
	BEGIN_SERIALIZE_TRY

	CArchive the_in_Archive(&fl, CArchive::load);

	the_in_Archive >> L_vertexCount >> L_indecieCount >> L_activeDistance >> L_smoothShading;

	// Vertexes
	if (L_vertexCount > 0) {
		// allocate
		L_vertexArray = new ABVERTEX[L_vertexCount];
	}

	for (int loop = 0; loop < L_vertexCount; loop++) {
		// loop
		the_in_Archive >> L_vertexArray[loop].x >> L_vertexArray[loop].y >> L_vertexArray[loop].z >> L_vertexArray[loop].nx >> L_vertexArray[loop].ny >> L_vertexArray[loop].nz >> L_vertexArray[loop].tx0.tu >> L_vertexArray[loop].tx0.tv >> L_vertexArray[loop].tx1.tu >> L_vertexArray[loop].tx1.tv >> L_vertexArray[loop].tx2.tu >> L_vertexArray[loop].tx2.tv >> L_vertexArray[loop].tx3.tu >> L_vertexArray[loop].tx3.tv >> L_vertexArray[loop].tx4.tu >> L_vertexArray[loop].tx4.tv >> L_vertexArray[loop].tx5.tu >> L_vertexArray[loop].tx5.tv >> L_vertexArray[loop].tx6.tu >> L_vertexArray[loop].tx6.tv >> L_vertexArray[loop].tx7.tu >> L_vertexArray[loop].tx7.tv;
	} // end loop

	// indecies
	if (L_indecieCount > 0) {
		L_indecieArray = new WORD[L_indecieCount];
	}

	for (int loop = 0; loop < L_indecieCount; loop++) {
		// loop
		the_in_Archive >> L_indecieArray[loop];
	} // end loop

	// textures
	for (int loop = 0; loop < 10; loop++) {
		the_in_Archive >> texture[loop];
	}

	the_in_Archive.Close();
	fl.Close();

	/*m_abVertexArray.InitFromData(L_vertexCount,
							  L_indecieCount,
							  L_vertexArray,
							  L_indecieArray,
								 m_validUvCount*/

	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//move texture coords
/* void CEXMeshObj::MoveTexture(float &u,float &v,int textureSet,float &adjustfactor)
{
if(u == 0.0f &&
   v == 0.0f)
   return;

   m_abVertexArray.ApplyMoveTexture(u,v,textureSet,adjustfactor);
}*/
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::Serialize(CArchive& ar) {
	int reservedInt = 0;
	float reservedFloat = 0.0f;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		// stream the legacy vertex array
		if (m_VertexArrayValid) {
			ar << m_VertexArrayValid
			   << m_blendType;
			if (m_materialObject) {
				bool storingMaterial;
				if (m_materialObject->m_myCompressionIndex != CMATERIAL_UNCOMPRESSED) {
					storingMaterial = false;
					ar << storingMaterial;
					ar << m_materialObject->m_myCompressionIndex;
				} else {
					storingMaterial = true;
					ar << storingMaterial;
					ar << m_materialObject;
				}
			} else {
				bool storingMaterial = true;
				ar << storingMaterial;
				ar << m_materialObject;
			}
			ar << m_meshCenter.x
			   << m_meshCenter.y
			   << m_meshCenter.z
			   << m_meshBoundingSphereRadius
			   << m_chromeWrap
			   << m_chromeUvSet
			   << reservedInt
			   << reservedInt
			   << reservedFloat
			   << reservedFloat
			   << reservedFloat
			   << m_charona
			   << &m_abVertexArray
			   << m_boundingBox.maxX
			   << m_boundingBox.maxY
			   << m_boundingBox.maxZ
			   << m_boundingBox.minX
			   << m_boundingBox.minY
			   << m_boundingBox.minZ;
		} else {
			ar << m_VertexArrayValid
			   << m_blendType;
			if (m_materialObject) {
				bool storingMaterial;
				if (m_materialObject->m_myCompressionIndex != CMATERIAL_UNCOMPRESSED) {
					storingMaterial = false;
					ar << storingMaterial;
					ar << m_materialObject->m_myCompressionIndex;
				} else {
					storingMaterial = true;
					ar << storingMaterial;
					ar << m_materialObject;
				}
			} else {
				bool storingMaterial = true;
				ar << storingMaterial;
				ar << m_materialObject;
			}
			ar << m_meshCenter.x
			   << m_meshCenter.y
			   << m_meshCenter.z
			   << m_meshBoundingSphereRadius
			   << m_chromeWrap
			   << m_chromeUvSet
			   << reservedInt
			   << reservedInt
			   << reservedFloat
			   << reservedFloat
			   << reservedFloat
			   << m_charona
			   << m_boundingBox.maxX
			   << m_boundingBox.maxY
			   << m_boundingBox.maxZ
			   << m_boundingBox.minX
			   << m_boundingBox.minY
			   << m_boundingBox.minZ;
		}
		ar << m_fileName;
		ar << m_indexBuffiltered;
	} else {
		int version = ar.GetObjectSchema();
		CAdvancedVertexObj* vertexArray = NULL;

		if (version < 6) {
			ar >> m_blendType >> m_materialObject >> m_meshCenter.x >> m_meshCenter.y >> m_meshCenter.z >> m_meshBoundingSphereRadius >> m_chromeWrap >> m_chromeUvSet >> reservedInt >> reservedInt >> reservedFloat >> reservedFloat >> reservedFloat >> m_charona >> vertexArray >> m_boundingBox.maxX >> m_boundingBox.maxY >> m_boundingBox.maxZ >> m_boundingBox.minX >> m_boundingBox.minY >> m_boundingBox.minZ;
		} else {
			ar >> m_VertexArrayValid;

			if (m_VertexArrayValid) {
				ar >> m_blendType;
				if (version >= 7) {
					bool storingMaterial;
					ar >> storingMaterial;
					if (storingMaterial) {
						ar >> m_materialObject;
					} else {
						int compressionIndex;
						ar >> compressionIndex;
						MaterialCompressor* mc = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
						if (mc)
							m_materialObject = mc->getMaterial(compressionIndex);
					}
				} else {
					ar >> m_materialObject;
				}
				ar >> m_meshCenter.x >> m_meshCenter.y >> m_meshCenter.z >> m_meshBoundingSphereRadius >> m_chromeWrap >> m_chromeUvSet >> reservedInt >> reservedInt >> reservedFloat >> reservedFloat >> reservedFloat >> m_charona >> vertexArray >> m_boundingBox.maxX >> m_boundingBox.maxY >> m_boundingBox.maxZ >> m_boundingBox.minX >> m_boundingBox.minY >> m_boundingBox.minZ;
			} else {
				ar >> m_blendType;
				if (version >= 7) {
					bool storingMaterial;
					ar >> storingMaterial;
					if (storingMaterial) {
						ar >> m_materialObject;
					} else {
						int compressionIndex;
						ar >> compressionIndex;
						MaterialCompressor* mc = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
						if (mc)
							m_materialObject = mc->getMaterial(compressionIndex);
					}
				} else {
					ar >> m_materialObject;
				}
				ar >> m_meshCenter.x >> m_meshCenter.y >> m_meshCenter.z >> m_meshBoundingSphereRadius >> m_chromeWrap >> m_chromeUvSet >> reservedInt >> reservedInt >> reservedFloat >> reservedFloat >> reservedFloat >> m_charona >> m_boundingBox.maxX >> m_boundingBox.maxY >> m_boundingBox.maxZ >> m_boundingBox.minX >> m_boundingBox.minY >> m_boundingBox.minZ;
			}
		}

		if (version < 3) {
			for (int loop = 0; loop < 10; loop++) {
				CStringA stff;
				ar >> m_materialObject->m_texNameHash[loop] >> stff >> m_materialObject->m_texBlendMethod[loop];
				m_materialObject->m_texFileName[loop] = (const char*)stff;
			}
		}

		if (version >= 1) {
			ar >> m_fileName;
		}
		if (version >= 2) {
			if (version < 5) {
				ar >> m_tempSupportedMaterials;
				if (version == 2 && m_tempSupportedMaterials) {
					// This was when supported materials did not support texture.  Assume that the same texture is wanted on
					// All supported materials.
					for (POSITION posLoc = m_tempSupportedMaterials->GetHeadPosition(); posLoc != NULL;) {
						CMaterialObject* cMat = (CMaterialObject*)m_tempSupportedMaterials->GetNext(posLoc);
						for (int loop = 0; loop < 10; loop++) {
							cMat->m_texNameHash[loop] = m_materialObject->m_texNameHash[loop];
							cMat->m_texFileName[loop] = m_materialObject->m_texFileName[loop];
							cMat->m_texBlendMethod[loop] = m_materialObject->m_texBlendMethod[loop];
						}
					}
				}
			}
		}
		if (vertexArray) {
			vertexArray->CloneDataOnly(&m_abVertexArray);
			vertexArray->SafeDelete();
			delete vertexArray;
			vertexArray = 0;
		}

#ifndef _ClientOutput
		if (m_materialObject) {
			delete m_materialObject;
			m_materialObject = 0;
		}
		if (m_tempSupportedMaterials) {
			for (POSITION pos = m_tempSupportedMaterials->GetHeadPosition(); pos != NULL;) {
				CMaterialObject* obj = (CMaterialObject*)m_tempSupportedMaterials->GetNext(pos);
				delete obj;
			}
			delete m_tempSupportedMaterials;
			m_tempSupportedMaterials = 0;
		}
#endif
		m_groupReference = -1;
		m_tempDistance = 0;
		m_lastTimeStamp = 0;
		m_visibilityTrack = 1;
		m_shadowTemp = NULL;
		m_byteFlagTemp = 0;

		m_worldTransform.MakeIdentity();
		m_translation.Set(0, 0, 0);
		m_rotation.Set(0, 0, 0);
		m_scale.Set(1, 1, 1);

		if (version > 3) {
			ar >> m_indexBuffiltered;
		} else {
			m_indexBuffiltered = FALSE;
		}

		m_Rendered = TRUE;

		// get the bounding radius from the boundbox
		ABBOX& box = m_boundingBox;

		FLOAT x = box.maxX - box.minX;
		FLOAT y = box.maxY - box.minY;
		FLOAT z = box.maxZ - box.minZ;

		FLOAT r = x > y ? x : y;

		m_boundingRadius = r > z ? r * .5f : z * .5f; //@@Watch: to be replaced with SetBoundingBox once I find out there is no other side effects.
	}
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//--Get Bounding Box--//
ABBOX CEXMeshObj::ComputeBoundingVolumes(BOOL bUpdateBox /*= TRUE*/) {
	static ABBOX empty = { 0.0f, 0.0f, 0.0f, 0.01f, 0.01f, 0.01f };

	ABVERTEX* abVertex = NULL;
	int vCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);

	if (!abVertex)
		return empty;
	if (m_abVertexArray.m_vertexCount == 0)
		return empty;

	ABBOX bbox;
	bbox.maxX = bbox.minX = abVertex[0].x;
	bbox.maxY = bbox.minY = abVertex[0].y;
	bbox.maxZ = bbox.minZ = abVertex[0].z;

	for (UINT loop = 0; loop < m_abVertexArray.m_vertexCount; loop++) {
		if (abVertex[loop].x < bbox.minX)
			bbox.minX = abVertex[loop].x;
		if (abVertex[loop].x > bbox.maxX)
			bbox.maxX = abVertex[loop].x;

		if (abVertex[loop].y < bbox.minY)
			bbox.minY = abVertex[loop].y;
		if (abVertex[loop].y > bbox.maxY)
			bbox.maxY = abVertex[loop].y;

		if (abVertex[loop].z < bbox.minZ)
			bbox.minZ = abVertex[loop].z;
		if (abVertex[loop].z > bbox.maxZ)
			bbox.maxZ = abVertex[loop].z;
	}

	if (abVertex)
		delete[] abVertex;
	abVertex = 0;

	if (bUpdateBox)
		SetBoundingBox(bbox);

	return bbox;
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//--Get Bounding Box--//
BOOL CEXMeshObj::GetBoxWithMarix(const Matrix44f& positionWld, ABBOX* returnBox) const {
	ABVERTEX* abVertex = NULL;
	int vCount = 0;

	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);

	if (!abVertex)
		return FALSE;
	if (m_abVertexArray.m_vertexCount == 0)
		return FALSE;
	Vector3f posTemp;
	posTemp.x = abVertex[0].x;
	posTemp.y = abVertex[0].y;
	posTemp.z = abVertex[0].z;
	Vector3f translatedPos = TransformPoint_NonPerspective(positionWld, posTemp);

	returnBox->maxX = returnBox->minX = translatedPos.x;
	returnBox->maxY = returnBox->minY = translatedPos.y;
	returnBox->maxZ = returnBox->minZ = translatedPos.z;

	for (UINT loop = 0; loop < m_abVertexArray.m_vertexCount; loop++) {
		//update by matrix
		posTemp.x = abVertex[loop].x;
		posTemp.y = abVertex[loop].y;
		posTemp.z = abVertex[loop].z;
		translatedPos = TransformPoint_NonPerspective(positionWld, posTemp);

		//end update by marix

		if (translatedPos.x < returnBox->minX)
			returnBox->minX = translatedPos.x;
		if (translatedPos.x > returnBox->maxX)
			returnBox->maxX = translatedPos.x;

		if (translatedPos.y < returnBox->minY)
			returnBox->minY = translatedPos.y;
		if (translatedPos.y > returnBox->maxY)
			returnBox->maxY = translatedPos.y;

		if (translatedPos.z < returnBox->minZ)
			returnBox->minZ = translatedPos.z;
		if (translatedPos.z > returnBox->maxZ)
			returnBox->maxZ = translatedPos.z;

		if (returnBox->minX == returnBox->maxX)
			returnBox->maxX = returnBox->maxX + .01f;
		if (returnBox->minY == returnBox->maxY)
			returnBox->maxY = returnBox->maxY + .01f;
		if (returnBox->minZ == returnBox->maxZ)
			returnBox->maxZ = returnBox->maxZ + .01f;
	}
	if (abVertex)
		delete[] abVertex;
	abVertex = 0;

	return TRUE;
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//round texture coords
//--This Function is a component of ExactCollision()-----------//
int CEXMeshObj::Round(float x) {
	float testVal;
	testVal = fmod(x, 1.0f);
	if ((x >= 0) && (testVal >= 0.5)) {
		x = x + 1.0f;
	} else if ((x < 0) && (testVal <= -0.5f)) {
		x = x - 1.0f;
	}

	return (int)x;
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::AVGNormals(float smoothBasisAngle) {
	ABVERTEX* abVertex = NULL;
	int vCount = 0;

	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);

	for (int x = 0; x < vCount; x++) {
		for (int i = 0; i < vCount; i++) {
			if (i == x) {
				continue;
			}

			// WORD vI1 = m_abVertexArray.m_indecieArray[i];
			// WORD vI2 = m_abVertexArray.m_indecieArray[x];
			if (abVertex[i].x == abVertex[x].x && abVertex[i].y == abVertex[x].y && abVertex[i].z == abVertex[x].z) {
				// same pos
				Vector3f tempOne, tempTwo;
				tempOne.x = abVertex[i].nx;
				tempOne.y = abVertex[i].ny;
				tempOne.z = abVertex[i].nz;
				tempTwo.x = abVertex[x].nx;
				tempTwo.y = abVertex[x].ny;
				tempTwo.z = abVertex[x].nz;

				float angleDiff = GetAngleBetween(tempOne, tempTwo);
				if (angleDiff <= smoothBasisAngle) {
					// combine
					float oldIx = abVertex[i].nx;
					abVertex[i].nx = abVertex[i].nx + abVertex[x].nx;
					abVertex[x].nx = oldIx + abVertex[x].nx;

					float oldIy = abVertex[i].ny;
					abVertex[i].ny = abVertex[i].ny + abVertex[x].ny;
					abVertex[x].ny = oldIy + abVertex[x].ny;

					float oldIz = abVertex[i].nz;
					abVertex[i].nz = abVertex[i].nz + abVertex[x].nz;
					abVertex[x].nz = oldIz + abVertex[x].nz;
				} // end combine
			} // end same pos
		}
	}

	// Assign the newly computed normals back to the vertices
	Vector3f normV;
	for (UINT i = 0; i < m_abVertexArray.m_vertexCount; i++) {
		// Provide some relief to bogus normals
		if (abVertex[i].nx == 0.0f && abVertex[i].ny == 0.0f && abVertex[i].nz == 0.0f) {
			abVertex[i].nx = 0.0f; // = Vector3f( 0.0f, 0.0f, 1.0f );
			abVertex[i].ny = 0.0f;
			abVertex[i].nz = 1.0f;
		}

		normV.x = abVertex[i].nx;
		normV.y = abVertex[i].ny;
		normV.z = abVertex[i].nz;

		normV.Normalize();

		abVertex[i].nx = normV.x;
		abVertex[i].ny = normV.y;
		abVertex[i].nz = normV.z;
	}

	m_abVertexArray.SetABVERTEXStyle(NULL, abVertex, vCount);
	if (abVertex) {
		delete[] abVertex;
	}

	abVertex = 0;
}
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-------------//
//rebuild normals
//-------------//
//calculate normals
void CEXMeshObj::GenerateNormals(float smoothBasisAngle) {
	ABVERTEX* abVertex = NULL;
	int vCount = 0;

	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);

	Vector3f* pNormals = new Vector3f[m_abVertexArray.m_vertexCount];
	std::fill(pNormals, pNormals + m_abVertexArray.m_vertexCount, Vector3f::Zero());
	for (UINT i = 0; i < m_abVertexArray.m_indecieCount; i += 3) {
		WORD a = m_abVertexArray.GetVertexIndex(i + 0);
		WORD b = m_abVertexArray.GetVertexIndex(i + 1);
		WORD c = m_abVertexArray.GetVertexIndex(i + 2);

		Vector3f* v1 = (Vector3f*)&abVertex[a];
		Vector3f* v2 = (Vector3f*)&abVertex[b];
		Vector3f* v3 = (Vector3f*)&abVertex[c];

		Vector3f arg1, arg2;
		arg1.x = v2->x - v1->x;
		arg1.y = v2->y - v1->y;
		arg1.z = v2->z - v1->z;

		arg2.x = v3->x - v2->x;
		arg2.y = v3->y - v2->y;
		arg2.z = v3->z - v2->z;

		Vector3f n = Cross(arg1, arg2).GetNormalized();

		pNormals[a].x += n.x;
		pNormals[a].y += n.y;
		pNormals[a].z += n.z;

		pNormals[b].x += n.x;
		pNormals[b].y += n.y;
		pNormals[b].z += n.z;

		pNormals[c].x += n.x;
		pNormals[c].y += n.y;
		pNormals[c].z += n.z;
	}

	// Assign the newly computed normals back to the vertices
	for (UINT i = 0; i < m_abVertexArray.m_vertexCount; i++) {
		// Provide some relief to bogus normals
		if (pNormals[i].Length() < 0.1f) {
			pNormals[i].x = 0.0f; // = Vector3f( 0.0f, 0.0f, 1.0f );
			pNormals[i].y = 0.0f;
			pNormals[i].z = 1.0f;
		}

		pNormals[i].Normalize();
		abVertex[i].nx = pNormals[i].x;
		abVertex[i].ny = pNormals[i].y;
		abVertex[i].nz = pNormals[i].z;
	}

	delete[] pNormals;
	pNormals = 0;

	m_abVertexArray.SetABVERTEXStyle(NULL, abVertex, vCount);
	if (abVertex) {
		delete[] abVertex;
	}

	abVertex = 0;
}
//calculate normals
void CEXMeshObj::NormalizeOnly() {
	ABVERTEX* abVertex = NULL;
	int vCount = 0;

	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);

	Vector3f tempNormal;
	// Assign the newly computed normals back to the vertices
	for (UINT i = 0; i < m_abVertexArray.m_vertexCount; i++) {
		tempNormal.x = abVertex[i].nx;
		tempNormal.y = abVertex[i].ny;
		tempNormal.z = abVertex[i].nz;

		tempNormal.Normalize();
		abVertex[i].nx = tempNormal.x;
		abVertex[i].ny = tempNormal.y;
		abVertex[i].nz = tempNormal.z;
	}

	m_abVertexArray.SetABVERTEXStyle(NULL,
		abVertex,
		vCount);
	if (abVertex)
		delete[] abVertex;
	abVertex = 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CEXMeshObj::GenerateSmoothNormals() {
	ABVERTEX* abVertex = NULL;
	int vCount = 0;

	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);

	GenerateNormals(0);

	ABVERTEX* originalVertexArray = new ABVERTEX[m_abVertexArray.m_vertexCount];
	CopyMemory(originalVertexArray, abVertex, (m_abVertexArray.m_vertexCount * sizeof(ABVERTEX)));

	SmoothNormalsBasedOnArray(originalVertexArray);
	delete[] originalVertexArray;
	originalVertexArray = 0;

	if (abVertex) {
		delete[] abVertex;
	}

	abVertex = 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CEXMeshObj::SmoothNormalsBasedOnArray(ABVERTEX* originalVertexArray) {
	GenerateNormals(0);

	ABVERTEX* abVertex = NULL;
	int vCount = 0;

	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);

	for (UINT i = 0; i < m_abVertexArray.m_vertexCount; i++) {
		for (UINT c = 0; c < m_abVertexArray.m_vertexCount; c++) {
			// loop secondary
			// if(c != i)
			if (abVertex[i].x == originalVertexArray[c].x &&
				abVertex[i].y == originalVertexArray[c].y &&
				abVertex[i].z == originalVertexArray[c].z) {
				// match?
				abVertex[i].nx += originalVertexArray[c].nx;
				abVertex[i].ny += originalVertexArray[c].ny;
				abVertex[i].nz += originalVertexArray[c].nz;

				Vector3f conversionVect;
				conversionVect.x = abVertex[i].nx;
				conversionVect.y = abVertex[i].ny;
				conversionVect.z = abVertex[i].nz;
				conversionVect.Normalize();
				abVertex[i].nx = conversionVect.x;
				abVertex[i].ny = conversionVect.y;
				abVertex[i].nz = conversionVect.z;
			} // end match?
		} // end loop secondary
	}

	m_abVertexArray.SetABVERTEXStyle(NULL, abVertex, vCount);
	if (abVertex) {
		delete[] abVertex;
	}

	abVertex = 0;
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-------------------textuture set class---------------------//
CTextureSetObj::CTextureSetObj() {
	m_textureSet = NULL;
	m_textureCoordCount = 0;
}

void CTextureSetObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_textureCoordCount;

		for (int loop = 0; loop < m_textureCoordCount; loop++) {
			ar << m_textureSet[loop].tu
			   << m_textureSet[loop].tv;
		}
	} else {
		ar >> m_textureCoordCount;
		if (m_textureCoordCount > 0)
			m_textureSet = new TXTUV[m_textureCoordCount];
		for (int loop = 0; loop < m_textureCoordCount; loop++) {
			ar >> m_textureSet[loop].tu >> m_textureSet[loop].tv;
		}
	}
}

void CEXMeshObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CEXMeshObj* meshObj = (CEXMeshObj*)GetNext(posLoc);
		meshObj->SafeDelete();
		delete meshObj;
		meshObj = 0;
	}
	RemoveAll();
}

void CEXMeshObjList::AddAllocatedMeshOrRefresh(CEXMeshObj* mesh) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CEXMeshObj* meshObj = (CEXMeshObj*)GetNext(posLoc);
		if (mesh == meshObj) {
			mesh->m_byteFlagTemp = 1;
			return;
		}
	}

	mesh->m_byteFlagTemp = 1;
	AddTail(mesh);
}

void CEXMeshObjList::FlagAllZero() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CEXMeshObj* meshObj = (CEXMeshObj*)GetNext(posLoc);
		meshObj->m_byteFlagTemp = 0;
	}
}

int CEXMeshObjList::InvalidateZeroTemp() {
	int trace = 0;
	POSITION posLast = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CEXMeshObj* meshObj = (CEXMeshObj*)GetNext(posLoc);
		if (meshObj->m_byteFlagTemp == 0) {
			if (meshObj->m_abVertexArray.m_indicieBuffer) {
				meshObj->m_abVertexArray.m_indicieBuffer->Release();
			}

			meshObj->m_abVertexArray.m_indicieBuffer = 0;

			if (meshObj->m_abVertexArray.m_vertexBuffer) {
				meshObj->m_abVertexArray.m_vertexBuffer->Release();
			}

			meshObj->m_abVertexArray.m_vertexBuffer = 0;
			RemoveAt(posLast);
			trace++;
		}
	}

	return trace;
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CEXMeshObj::Move(const Vector3f& aTranslation, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols) {
	ABVERTEX* abVertex = NULL;
	int vCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);
	if (!abVertex) {
		return;
	}

	for (UINT loop = 0; loop < m_abVertexArray.m_vertexCount; loop++) {
		abVertex[loop].x += aTranslation.x;
		abVertex[loop].y += aTranslation.y;
		abVertex[loop].z += aTranslation.z;
	}

	m_abVertexArray.SetABVERTEXStyle(NULL, abVertex, vCount);

	delete[] abVertex;
	abVertex = 0;

	if (aRecompBoundVols) {
		ComputeBoundingVolumes();
	}

	InitBuffer(aD3D9Device, NULL);
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CEXMeshObj::Rotate(const Vector3f& aRotation, const Vector3f* const aPivotOverride, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols) {
	Vector3f pivotCenter = m_meshCenter;
	if (aPivotOverride) {
		pivotCenter = *aPivotOverride;
	}

	Matrix44f matrixApply;
	ConvertRotationZXY(aRotation, out(matrixApply));

	ABVERTEX* abVertex = NULL;
	int vCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);
	if (!abVertex) {
		return;
	}

	for (UINT loop = 0; loop < m_abVertexArray.m_vertexCount; loop++) {
		Vector3f tempVect;
		tempVect.x = abVertex[loop].x;
		tempVect.y = abVertex[loop].y;
		tempVect.z = abVertex[loop].z;
		tempVect.x -= pivotCenter.x;
		tempVect.y -= pivotCenter.y;
		tempVect.z -= pivotCenter.z;
		tempVect = TransformVector(matrixApply, tempVect);
		tempVect.x += pivotCenter.x;
		tempVect.y += pivotCenter.y;
		tempVect.z += pivotCenter.z;
		abVertex[loop].x = tempVect.x;
		abVertex[loop].y = tempVect.y;
		abVertex[loop].z = tempVect.z;

		tempVect;
		tempVect.x = abVertex[loop].nx;
		tempVect.y = abVertex[loop].ny;
		tempVect.z = abVertex[loop].nz;
		tempVect = TransformVector(matrixApply, tempVect);
		abVertex[loop].nx = tempVect.x;
		abVertex[loop].ny = tempVect.y;
		abVertex[loop].nz = tempVect.z;
	}

	m_abVertexArray.SetABVERTEXStyle(NULL, abVertex, vCount);
	delete[] abVertex;
	abVertex = 0;

	if (aRecompBoundVols) {
		ComputeBoundingVolumes();
	}

	InitBuffer(aD3D9Device, NULL);
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CEXMeshObj::Scale(const Vector3f& aScale, const Vector3f* const aPivotOverride, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols) {
	Matrix44f matrixApply;
	matrixApply.MakeScale(aScale);

	Matrix44f matrixInv, matrixInvTranspose;
	matrixInv.MakeInverseOf(matrixApply);
	MatrixARB::Transpose(matrixInvTranspose, matrixInv);

	Vector3f pivotCenter = m_meshCenter;
	if (aPivotOverride) {
		pivotCenter = *aPivotOverride;
	}

	ABVERTEX* abVertex = NULL;
	int vCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&abVertex, &vCount);
	if (!abVertex) {
		return;
	}

	for (UINT loop = 0; loop < m_abVertexArray.m_vertexCount; loop++) {
		Vector3f tempVect;
		tempVect.x = abVertex[loop].x;
		tempVect.y = abVertex[loop].y;
		tempVect.z = abVertex[loop].z;
		tempVect.x -= pivotCenter.x;
		tempVect.y -= pivotCenter.y;
		tempVect.z -= pivotCenter.z;
		tempVect = TransformVector(matrixApply, tempVect);
		tempVect.x += pivotCenter.x;
		tempVect.y += pivotCenter.y;
		tempVect.z += pivotCenter.z;
		abVertex[loop].x = tempVect.x;
		abVertex[loop].y = tempVect.y;
		abVertex[loop].z = tempVect.z;

		// normal xform
		tempVect.x = abVertex[loop].nx;
		tempVect.y = abVertex[loop].ny;
		tempVect.z = abVertex[loop].nz;
		tempVect = TransformVector(matrixInvTranspose, tempVect);
		tempVect.Normalize();
		abVertex[loop].nx = tempVect.x;
		abVertex[loop].ny = tempVect.y;
		abVertex[loop].nz = tempVect.z;
	}

	m_abVertexArray.SetABVERTEXStyle(NULL, abVertex, vCount);
	delete[] abVertex;
	abVertex = 0;

	if (aRecompBoundVols) {
		ComputeBoundingVolumes();
	}

	InitBuffer(aD3D9Device, NULL);
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
void CEXMeshObj::Transform(const Matrix44f aTransform, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols) {
	ABVERTEX* tempArray = NULL;
	int vCount = 0;
	m_abVertexArray.GetABVERTEXStyle(&tempArray, &vCount);

	if (!tempArray)
		return;

	for (UINT loop = 0; loop < m_abVertexArray.m_vertexCount; loop++) {
		Vector3f tempConv;
		tempConv.x = tempArray[loop].x;
		tempConv.y = tempArray[loop].y;
		tempConv.z = tempArray[loop].z;
		tempConv = TransformPoint_NonPerspective(aTransform, tempConv);
		tempArray[loop].x = tempConv.x;
		tempArray[loop].y = tempConv.y;
		tempArray[loop].z = tempConv.z;
	}

	m_abVertexArray.SetABVERTEXStyle(NULL, tempArray, vCount);
	delete[] tempArray;
	tempArray = 0;

	if (aRecompBoundVols)
		ComputeBoundingVolumes();

	InitBuffer(aD3D9Device, NULL);
}

// DRF - DEAD CODE? - Have yet to see where this gets called
void CEXMeshObj::TextureMovementCallback(int& curVertexType) {
	LogWarn("DEAD CODE");
	if (m_materialObject->m_enableTextureMovement == TRUE) {
		// texture Movement
		for (unsigned int uvSet = 0; uvSet < MAX_UV_SETS; uvSet++) {
			if (m_materialObject->m_enableSet[uvSet] == TRUE) {
				m_abVertexArray.ApplyMoveTexture(
					m_materialObject->m_textureMovement[uvSet].tu,
					m_materialObject->m_textureMovement[uvSet].tv,
					uvSet,
					curVertexType);
			}
		}
	} // end texture Movement
}

void CEXMeshObj::SetExternalSource(std::shared_ptr<IExternalEXMesh> externalSource) {
	m_externalSource = externalSource;
}

void CEXMeshObj::ClearExternalSource() {
	m_externalSource.reset();
}

IMPLEMENT_SERIAL_SCHEMA(CTextureSetObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CTextureSetObjList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CEXMeshObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CEXMeshObjList, CObList)

BEGIN_GETSET_IMPL(CEXMeshObj, IDS_EXMESHOBJ_NAME)
GETSET_OBJ_PTR(m_charona, GS_FLAGS_DEFAULT, 2, 9, EXMESHOBJ, CHARONA, CCharonaObj)
END_GETSET_IMPL

void CEXMeshObj::SetMaterial(CMaterialObject* mat) {
	if (m_materialObject != NULL)
		delete m_materialObject;
	m_materialObject = mat;
}

void CEXMeshObj::TransformVertices(const Matrix44f& matrix) {
	m_abVertexArray.Transform(matrix);
}

void CEXMeshObj::SetBoundingBox(const ABBOX& box) {
	m_boundingBox = box;

	// prevent box degeneration
	if (m_boundingBox.minX == m_boundingBox.maxX)
		m_boundingBox.maxX = m_boundingBox.maxX + .01f;
	if (m_boundingBox.minY == m_boundingBox.maxY)
		m_boundingBox.maxY = m_boundingBox.maxY + .01f;
	if (m_boundingBox.minZ == m_boundingBox.maxZ)
		m_boundingBox.maxZ = m_boundingBox.maxZ + .01f;

	// get the new mesh center
	m_meshCenter.x = (m_boundingBox.maxX + m_boundingBox.minX) * .5f;
	m_meshCenter.y = (m_boundingBox.maxY + m_boundingBox.minY) * .5f;
	m_meshCenter.z = (m_boundingBox.maxZ + m_boundingBox.minZ) * .5f;

	// get the bound sphere radius (squared)
	Vector3f r;
	r.x = m_meshCenter.x - m_boundingBox.maxX;
	r.y = m_meshCenter.y - m_boundingBox.maxY;
	r.z = m_meshCenter.z - m_boundingBox.maxZ;
	m_meshBoundingSphereRadius = r.x * r.x + r.y * r.y + r.z * r.z;

	// get the bounding radius from the boundbox

	FLOAT x = m_boundingBox.maxX - m_boundingBox.minX;
	FLOAT y = m_boundingBox.maxY - m_boundingBox.minY;
	FLOAT z = m_boundingBox.maxZ - m_boundingBox.minZ;

	FLOAT l = x > y ? x : y;

	m_boundingRadius = l > z ? l * .5f : z * .5f;
}

void CEXMeshObj::SetMeshCenter(const Vector3f& ctr) {
	// called all the time during distro
	// ASSERT(FALSE);	// Setting mesh center without going thru SetBoundingBox is generally not allowed
	// This is added to make sure it open happen to very few hacked occasions.

	m_meshCenter = ctr;
}

void CEXMeshObj::SetBoundingSphereRadiusSqr(float newR) {
	ASSERT(FALSE); // Setting bounding radius without going thru SetBoundingBox is generally not allowed
	// This is added to make sure it open happen to very few hacked occasions.

	m_meshBoundingSphereRadius = newR;
}

void CEXMeshObj::BakeVertices() {
	// Bake vertices
	TransformVertices(GetWorldTransform());

	// Cleanup matrix -- already baked into vertices
	ResetWorldTransform();
}

unsigned CEXMeshObj::FillPositionBuffer(unsigned char* buffer, unsigned bufferSize, unsigned strideInBytes /*= sizeof(float) * 3*/) {
	unsigned vertexCount = GetVertexCount();
	ASSERT(UINT_MAX / vertexCount >= strideInBytes);

	unsigned offset = 0;
	for (unsigned i = 0; i < vertexCount; i++, offset += strideInBytes) {
		if (offset + strideInBytes > bufferSize) {
			// Buffer too small to hold all data
			ASSERT(false);
			return i;
		}

		auto pVert = GetVertices().GetVertexPtr(i);
		float* pOutPos = (float*)(buffer + offset);
		memcpy(pOutPos, &pVert->x, sizeof(float) * 3);
	}

	return vertexCount;
}

unsigned CEXMeshObj::FillNormalBuffer(unsigned char* buffer, unsigned bufferSize, unsigned strideInBytes /*= sizeof(float) * 3*/) {
	unsigned vertexCount = GetVertexCount();
	ASSERT(UINT_MAX / vertexCount >= strideInBytes);

	unsigned offset = 0;
	for (unsigned i = 0; i < vertexCount; i++, offset += strideInBytes) {
		if (offset + strideInBytes > bufferSize) {
			// Buffer too small to hold all data
			ASSERT(false);
			return i;
		}

		auto pVert = GetVertices().GetVertexPtr(i);
		float* pOutNorm = (float*)(buffer + offset);
		memcpy(pOutNorm, &pVert->nx, sizeof(float) * 3);
	}

	return vertexCount;
}

unsigned CEXMeshObj::FillUVCoordsBuffer(unsigned char* buffer, unsigned bufferSize, unsigned strideInBytes) {
	unsigned vertexCount = GetVertexCount();
	ASSERT(UINT_MAX / vertexCount >= strideInBytes);

	unsigned uvCount = GetVertexUVCount();
	ASSERT(uvCount < MAX_UV_SETS);
	uvCount = std::min(MAX_UV_SETS, uvCount);

	unsigned offset = 0;
	for (unsigned i = 0; i < vertexCount; i++, offset += strideInBytes) {
		if (offset + strideInBytes > bufferSize) {
			// Buffer too small to hold all data
			ASSERT(false);
			return i;
		}

		auto pVert = GetVertices().GetVertexPtr(i);
		float* pOutUVs = (float*)(buffer + offset);
		memcpy(pOutUVs, &pVert->tx0, sizeof(float) * 2 * uvCount);
	}

	return vertexCount;
}

unsigned CEXMeshObj::FillIndexBufferInt(unsigned char* buffer, unsigned bufferSize, unsigned int baseIndex, unsigned strideInBytes /*= sizeof(int)*/) {
	unsigned indexCount = GetIndexCount();
	ASSERT(UINT_MAX / indexCount >= strideInBytes);

	unsigned offset = 0;
	for (unsigned i = 0; i < indexCount; i++, offset += strideInBytes) {
		if (offset + strideInBytes > bufferSize) {
			// Buffer too small to hold all data
			ASSERT(false);
			return i;
		}

		unsigned int vertexIndex = GetVertices().GetVertexIndex(i);
		unsigned* pOutIndex = (unsigned*)(buffer + offset);
		*pOutIndex = baseIndex + vertexIndex;
	}

	return indexCount;
}

unsigned CEXMeshObj::FillIndexBufferShort(unsigned char* buffer, unsigned bufferSize, unsigned short baseIndex, unsigned strideInBytes /*= sizeof(short)*/) {
	unsigned indexCount = GetIndexCount();
	ASSERT(UINT_MAX / indexCount >= strideInBytes);

	unsigned offset = 0;
	for (unsigned i = 0; i < indexCount; i++, offset += strideInBytes) {
		if (offset + strideInBytes > bufferSize) {
			// Buffer too small to hold all data
			ASSERT(false);
			return i;
		}

		unsigned int vertexIndex = GetVertices().GetVertexIndex(i);
		ASSERT(vertexIndex <= USHORT_MAX);
		unsigned short* pOutIndex = (unsigned short*)(buffer + offset);
		*pOutIndex = baseIndex + (unsigned short)vertexIndex;
	}

	return indexCount;
}

void CEXMeshObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		pMemSizeGadget->AddObject(m_abVertexArray);
		pMemSizeGadget->AddObjectSizeof(m_boundingBox);
		pMemSizeGadget->AddObject(m_fileName);
		pMemSizeGadget->AddObject(m_materialObject);
		pMemSizeGadget->AddObject(m_shadowTemp);
	}

	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	m_charona					// CCharonaObj*
	m_externalSource			// std::shared_ptr<IExternalEXMesh> 
	m_pd3dDevice				// LPDIRECT3DDEVICE9
	m_tempSupportedMaterials	// CObList*
	m_textureSetList			// CTextureSetObjList*  (CObList)
	mBoneData					// RawBoneData  
	mpBoneInfo		    		// VertexBuffer* 
	mpEffectShader				// MaterialEffect*
	mpOffsetVectsBuffer			// VertexBuffer* 
	-------------------------------------------------------------------------------*/
}

} // namespace KEP