///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CServerItemGen.h"

#if (DRF_OLD_VENDORS == 1)

#include "CServerItemClass.h"

namespace KEP {

CServerItemGenObj::CServerItemGenObj() {
	m_pSIO = NULL;
	m_existsOnServer = FALSE;
	m_spawnGenID = 0;
	m_genName = "NONE";
	m_lastCallbackUpdate = fTime::TimeMs();
}

void CServerItemGenObj::SafeDelete() {
	if (m_pSIO) {
		m_pSIO->SafeDelete();
		delete m_pSIO;
		m_pSIO = 0;
	}
}

void CServerItemGenObjList::SafeDelete() {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pSIGO = (CServerItemGenObj*)GetNext(pos);
		pSIGO->SafeDelete();
		delete pSIGO;
		pSIGO = 0;
	}
	RemoveAll();
}

void CServerItemGenObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	long deprecated;
	if (ar.IsStoring()) {
		ar << m_pSIO
		   << (long)0 //m_respawnDuration
		   << m_spawnGenID
		   << m_genName;
	} else {
		ar >> m_pSIO >> deprecated //m_respawnDuration
			>> m_spawnGenID >> m_genName;

		m_existsOnServer = FALSE;
		m_lastCallbackUpdate = fTime::TimeMs();
	}
}

IMPLEMENT_SERIAL(CServerItemGenObj, CObject, 0)
IMPLEMENT_SERIAL(CServerItemGenObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CServerItemGenObj, IDS_SERVERITEMGENOBJ_NAME)
GETSET_OBJ_PTR(m_pSIO, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMGENOBJ, SERVERITEM, CServerItemObj)
GETSET_RANGE(m_spawnGenID, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMGENOBJ, SPAWNGENID, INT_MIN, INT_MAX)
GETSET_MAX(m_genName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMGENOBJ, GENNAME, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP

#else
void CServerItemGen_4221() {}
#endif
