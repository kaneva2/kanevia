///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "StreamableDynamicObj.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CTriggerClass.h"
#include "CVertexAnimationModule.h"

#ifdef _ClientOutput
#include "ReMeshStream.h"
#include "ReMeshCreate.h"
#include "ReSkinMesh.h"
#include "IClientEngine.h"
#include "SkeletonProxy.h"
#include "SkeletonManager.h"
#endif

#include <ServerSerialize.h>
#include <direct.h>
#include "resource.h"
#include "MaterialPersistence.h"
#include "CCollisionClass.h"
#include "CGlobalInventoryClass.h"
#include "UseTypes.h"
#include "UnicodeMFCHelpers.h"

#include "config.h"
#include "LogHelper.h"
#include "Core\Util\Unicode.h"
#include "ReMeshConvertOptions.h"

static LogInstance("Instance");

#define CUR_DO_FILE_VERSION 4

namespace KEP {

#ifdef _ClientOutput
#ifdef ED7119_DYNOBJ_ASSET_FIX
static inline void gatherMeshes(StreamableDynamicObject* obj, MaterialCompressor* matComp, std::vector<ReMesh*>& collectedMeshes, std::map<uint64_t, uint64_t>& meshHashMap) {
#else
static inline void gatherMeshes(StreamableDynamicObject* obj, MaterialCompressor* matComp, std::vector<ReMesh*>& collectedMeshes) {
#endif
	std::set<uint64_t> meshHashes; // Hashes of collected meshes - for filtering duplicates

	for (size_t i = 0; i < obj->getVisualCount(); i++) {
		StreamableDynamicObjectVisual* vis = obj->Visual(i);
		matComp->addMaterial(vis->getMaterial());
		for (int j = 0; j < vis->getLodCount(); j++) {
			ReMesh* rm = MeshRM::Instance()->FindMesh(vis->getLOD(j), MeshRM::RMP_ID_DYNAMIC);
			if (rm == NULL) {
				// probably a legacy conversion
				rm = MeshRM::Instance()->FindMesh(vis->getLOD(j), MeshRM::RMP_ID_LIBRARY);
			}
			if (rm == NULL) {
				// wow this is great the lod's are in the world object mesh pool... that makes alot of sense
				rm = MeshRM::Instance()->FindMesh(vis->getLOD(j), MeshRM::RMP_ID_LEVEL);
			}
			if (rm == NULL) {
				// Lets have more fun.  Now they can be in the skeletal pool too... Just for hack for now.
				rm = MeshRM::Instance()->FindMesh(vis->getLOD(j), MeshRM::RMP_ID_PERSISTENT);
			}

			assert(rm != nullptr);
			if (rm == nullptr) {
				continue;
			}

#ifdef ED7119_DYNOBJ_ASSET_FIX
			if (ReMeshConvertOptions::getRecalcSkinHashBeforeWrite()) {
				ReSkinMesh* pSkinMesh = dynamic_cast<ReSkinMesh*>(rm);
				if (pSkinMesh != nullptr) {
					uint64_t oldHash = rm->GetHash();
					ReMesh* pNewMesh = pSkinMesh->CloneMeshWeightsSorted();
					if (pNewMesh != nullptr) {
						uint64_t newHash = pNewMesh->GetHash();
						if (newHash == oldHash) {
							// Already sorted
							delete pNewMesh;
						} else {
							ReMesh* pExistingMesh = MeshRM::Instance()->AddMesh(pNewMesh, MeshRM::RMP_ID_DYNAMIC);
							if (pExistingMesh != nullptr) {
								// Already exists
								assert(pExistingMesh->GetHash() == newHash);
								delete pNewMesh;
								rm = pExistingMesh;
							} else {
								rm = pNewMesh;
							}

							meshHashMap.insert(std::make_pair(oldHash, newHash));
						}
					}
				}
			}
#endif

			if (meshHashes.find(rm->GetHash()) == meshHashes.end()) {
				// New
				meshHashes.insert(rm->GetHash());
				collectedMeshes.push_back(rm);
			}
		}
	}
}
#endif

#ifdef _ClientOutput
bool ValidateBones(StreamableDynamicObject* pStrmDynObj, RuntimeSkeleton* pRTSkeleton) {
	for (size_t i = 0; i < pStrmDynObj->getVisualCount(); ++i) {
		StreamableDynamicObjectVisual* pVisual = pStrmDynObj->Visual(i);
		if (!pVisual)
			continue;
		for (int j = 0; j < pVisual->getLodCount(); ++j) {
			ReMesh* pReMesh = pVisual->getReMesh(j);
			if (!pReMesh)
				continue;
			if (pReMesh->GetNumBones() > pRTSkeleton->getBoneCount())
				return false;
		}
	}
	return true;
};
#endif

void StreamableDynamicObject::writeObject(CArchive& ar) {
#ifdef _ClientOutput
	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	std::vector<ReMesh*> archiveMeshes;
#ifdef ED7119_DYNOBJ_ASSET_FIX
	std::map<uint64_t, uint64_t> reMeshHashMap; // Old hash -> new
	gatherMeshes(this, matCompressor, archiveMeshes, reMeshHashMap);
#else
	gatherMeshes(this, matCompressor, archiveMeshes);
#endif

	ar << dyn_obj_archive_code
	   << CUR_DO_FILE_VERSION
	   << (long long)0
	   << CStringA(m_name.c_str());

	if (!archiveMeshes.empty()) {
		MeshRM::Instance()->StoreReMeshes(&archiveMeshes[0], archiveMeshes.size(), ar, MeshRM::RMP_ID_MIXED);
	}
	matCompressor->compress();
	matCompressor->writeToArchive(ar);

	GLID glid = m_LegacyGlid; // StreamableDynamicObject no longer keeps track of its GLID due to sharing of data between GLIDs

	long long glidLL = glid; // GLID is now 32-bit -- keep it 64-bit here just for serialization
	ar << nullptr
	   << m_particleEffectId
	   << m_particleOffset.x
	   << m_particleOffset.y
	   << m_particleOffset.z
	   << m_triggers
	   << m_attachable
	   << m_interactive
	   << glidLL
	   << m_boundBox.min.x
	   << m_boundBox.min.y
	   << m_boundBox.min.z
	   << m_boundBox.max.x
	   << m_boundBox.max.y
	   << m_boundBox.max.z;

	if (m_light && m_light->getRange() > 0.0f) {
		ar << true
		   << m_light->getRange()
		   << m_light->getAttenuation1()
		   << m_light->getAttenuation2()
		   << m_light->getAttenuation3()
		   << m_light->getLightColor().r
		   << m_light->getLightColor().g
		   << m_light->getLightColor().b
		   << m_lightPosition.x
		   << m_lightPosition.y
		   << m_lightPosition.z;
	} else {
		ar << false;
	}
	ar << getVisualCount();
	for (size_t i = 0; i < getVisualCount(); i++) {
		StreamableDynamicObjectVisual* vis = Visual(i);
		ar << vis->getName()
		   << vis->getRelativePosition().x
		   << vis->getRelativePosition().y
		   << vis->getRelativePosition().z
		   << vis->getRelativeOrientation().x
		   << vis->getRelativeOrientation().y
		   << vis->getRelativeOrientation().z
		   << vis->getVertexAnim()
		   << VisualMediaSupported(i)
		   << VisualCustomizable(i);

		if (vis->getMaterial()) {
			ar << vis->getMaterial()->m_myCompressionIndex;
		} else {
			ar << -1;
		}
		ar << vis->getLodCount();
		for (int j = 0; j < vis->getLodCount(); j++) {
			uint64_t meshHash = vis->getLOD(j);
#ifdef ED7119_DYNOBJ_ASSET_FIX
			auto itr = reMeshHashMap.find(meshHash);
			if (itr != reMeshHashMap.end()) {
				meshHash = itr->second;
			}
#endif
			ar << meshHash
			   << vis->getLODDistance(j);
		}
		ar << vis->getCanCollide();
	}

	// Instead of serialize current value of the skeletonGLID, save a boolean value to indicate
	// whether or not this DO carries a skeleton. This would allow a DOB file to stay unchanged
	// after going through a read/save iteration.
	BOOL hasSkeleton = m_skeletonGLID != 0;
	ar << hasSkeleton;

	if (hasSkeleton) {
		SkeletonProxy* proxy = SkeletonRM::Instance()->GetSkeletonProxy(m_skeletonGLID);
		RuntimeSkeleton* toSave = proxy->GetRuntimeSkeleton();
		ASSERT(toSave);
		if (toSave)
			RuntimeSkeleton::write(toSave, ar);
	}
	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
#endif
}

void StreamableDynamicObject::writeObject(BYTE*& dataBuffer, FileSize& dataLen) {
	dataLen = 0;

	CMemFile memFile;
	CArchive dyn_obj_archive(&memFile, CArchive::store);
	writeObject(dyn_obj_archive);
	dyn_obj_archive.Close();
	memFile.Flush();
	dataLen = (FileSize)memFile.GetLength();

	// make File Size evenly divisible by 128 bits for encryption
	FileSize remainder = dataLen % 16;
	if (remainder > 0) {
		char padding[16];
		memset(&padding, 0, 16 - remainder);
		memFile.Write(&padding, 16 - remainder);
	}
	dataLen = (FileSize)memFile.GetLength();
	dataBuffer = memFile.Detach();
}

StreamableDynamicObject* StreamableDynamicObject::readObject(CArchive& ar, const GLID& glidOverride, int* pAssetSchemaVersion) {
	unsigned char code;
	int version;

	ar >> code >> version;

	if (code != dyn_obj_archive_code) {
		AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj");
	}

	if (pAssetSchemaVersion != nullptr) {
		*pAssetSchemaVersion = version;
	}

	StreamableDynamicObject* obj = NULL;
	if (version == 1) {
		obj = readObjectV1(ar, glidOverride, version);
	} else if (version >= 2) {
		obj = readObjectV2(ar, glidOverride, version);
	} else {
		AfxThrowArchiveException(CArchiveException::badClass, L"dynamic obj - unsupported version");
	}

	if (version <= 3) {
		obj->updateVisualCollisionFlags();
	}

#ifdef _ClientOutput
	// Discard legacy collision to save memory if possible
	if (obj->m_pLegacyCollision != nullptr) {
		if (!IS_KANEVA_GLID(glidOverride)) { // preserve legacy collision for Kaneva items
			auto pICE = IClientEngine::Instance();
			if (pICE) {
				// Discard legacy collision data. They are no longer needed
				delete obj->m_pLegacyCollision;
				obj->m_pLegacyCollision = nullptr;
			}
		}
	}
#endif

	// Initialize collision shapes
	obj->createCollisionShapes();

	return obj;
}

StreamableDynamicObject* StreamableDynamicObject::readObjectV1(CArchive& ar, const GLID& glidOverride, int version) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return nullptr;

	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	//m_texFileName[] does not point to actual texture files if UGC. The actual texture name / URL come from content service.
	matCompressor->setTexFileNameIsConcrete(IS_UGC_GLID(glidOverride) ? false : true);

	long long deprecatedDynObjId;
	CStringA name;
	ar >> deprecatedDynObjId >> name;

	std::string utf8Name;
	if (!IsValidUtf8String(name)) {
		int wideNameLength = MultiByteToWideChar(1252, MB_PRECOMPOSED, name, -1, nullptr, 0);
		std::vector<WCHAR> wideName(wideNameLength);
		if (MultiByteToWideChar(1252, MB_PRECOMPOSED, name, -1, wideName.data(), wideName.size()) > 0)
			utf8Name = Utf16ToUtf8(wideName.data());
		else
			utf8Name = "invalidName";
	} else
		utf8Name = name;

	if (version == 1) {
		MeshRM::Instance()->LoadReMeshes(ar, pICE->GetReDeviceState(), MeshRM::RMP_ID_DYNAMIC);
	} else {
		MeshRM::Instance()->LoadReMeshes(ar, pICE->GetReDeviceState(), MeshRM::RMP_ID_MIXED);
	}
	matCompressor->readFromArchive(ar);

	CCollisionObj* collision;
	int particleEffectId;
	Vector3f partOffset;
	CTriggerObjectList* triggers;
	bool att, inter;
	long long glidLL; // GLID is now 32-bit -- keep it 64-bit here just for serialization
	BNDBOX box;

	// NOTE: the marker does not take CArchive buffer offset into account. It's just a rough estimate of current seek position.
	ULONGLONG markerBeforeCollision = ar.GetFile()->GetPosition();
	ar >> collision;
	ULONGLONG markerAfterCollision = ar.GetFile()->GetPosition();

	ar >> particleEffectId >> partOffset.x >> partOffset.y >> partOffset.z >> triggers >> att >> inter >> glidLL >> box.min.x >> box.min.y >> box.min.z >> box.max.x >> box.max.y >> box.max.z;
	GLID glid = (GLID)glidLL;

	// Create New Dynamic Object
	auto toReturn = new StreamableDynamicObject();
	toReturn->m_LegacyGlid = IS_KANEVA_GLID(glid) ? glid : INVALID_GLID;
	toReturn->setName(utf8Name.c_str());

	// Legacy collision
	toReturn->m_pLegacyCollision = collision;
	toReturn->m_hasLegacyCollision = toReturn->m_pLegacyCollision != nullptr;

	toReturn->setParticleEffectId(particleEffectId);
	toReturn->setParticleOffset(partOffset.x, partOffset.y, partOffset.z);
	toReturn->setTriggers(triggers);
	toReturn->setAttachable(att);
	toReturn->setInteractive(inter);
	toReturn->setBoundBox(box);
	markerAfterCollision;
	markerBeforeCollision;

	delete triggers;

	bool hasLight;
	ar >> hasLight;
	if (hasLight) {
		float range, att1, att2, att3, x, y, z;
		unsigned char r, g, b;
		StreamableDynamicObjectLight* lite;
		ar >> range >> att1 >> att2 >> att3 >> r >> g >> b >> x >> y >> z;
		lite = new StreamableDynamicObjectLight(r, g, b, range, att1, att2, att3);
		toReturn->setLight(lite);
		toReturn->setLightPosition(x, y, z);
	} else {
		toReturn->setLight((StreamableDynamicObjectLight*)NULL);
	}

	int visCount;
	ar >> visCount;

	// Create Dynamic Object Visuals (materials -> textures)
	for (int i = 0; i < visCount; i++) {
		StreamableDynamicObjectVisual* vis = new StreamableDynamicObjectVisual;
		CStringA visName;
		float posX, posY, posZ, orX, orY, orZ;
		CVertexAnimationModule* vAnim;
		bool mediaSupported, customizable;
		int matIndex, lodCount;

		ar >> visName >> posX >> posY >> posZ >> orX >> orY >> orZ >> vAnim >> mediaSupported >> customizable >> matIndex >> lodCount;
		CMaterialObject* mat = matCompressor->getMaterial(matIndex);
		vis->setName(visName.GetString());
		vis->setRelativePosition(posX, posY, posZ);
		vis->setRelativeOrientation(orX, orY, orZ);
		vis->setVertexAnim(vAnim);
		vis->setMaterial(mat);

		for (int j = 0; j < lodCount; j++) {
			long long hash;
			float dist;
			ar >> hash >> dist;
			vis->addLOD(hash, dist);
		}
		if (version >= 3) {
			bool canCollide;
			ar >> canCollide;
			vis->setCanCollide(canCollide);
		}

		toReturn->AddVisual(vis, customizable, mediaSupported);
		delete vAnim;
		delete mat;
	}

	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	return toReturn;
#else
	return nullptr;
#endif
}

StreamableDynamicObject* StreamableDynamicObject::readObjectV2(CArchive& ar, const GLID& glidOverride, int version) {
	// V2 Is hasSkeleton Extension Of V1
	StreamableDynamicObject* dob = readObjectV1(ar, glidOverride, version);

	BOOL hasSkeleton; // This value is a 32-bit boolean flag indicating whether or not the DO uses skeleton.
	ar >> hasSkeleton;

	// In older DO file, this value can be neither 0 or 1. It used to store the temporary
	// local GLID (which is non-zero) generated during import process. Since it doesn't make
	// sense to store a temporary value (which can vary from one import to anther), we take the
	// actual meaningful part of it and turn it into a boolean without updating schema version.
	if (hasSkeleton != FALSE) {
		hasSkeleton = TRUE;
	}

#ifdef _ClientOutput
	dob->setSkeletonGLID(0); // Default 0 - no skeleton
	if (hasSkeleton) {
		// Non-zero skeletonFlag means there is a skeleton. Read it.
		std::shared_ptr<RuntimeSkeleton> skeleton(RuntimeSkeleton::read(ar));

		if (ValidateBones(dob, skeleton.get())) {
			SkeletonRM::Instance()->AddRuntimeSkeleton(glidOverride, skeleton.get());
			dob->setSkeletonGLID(glidOverride);
			dob->m_pSkeleton = std::move(skeleton);
		} else {
			LogError("Object mesh expects more bones than are provided by skeleton. Object glid: " << glidOverride);
		}
	}
#endif
	return dob;
}

} // namespace KEP
