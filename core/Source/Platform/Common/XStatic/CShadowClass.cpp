///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include <d3d9.h>
#include <d3dx9.h>
#include "TextureDatabase.h"
#include "CShadowClass.h"
#include "CCollisionClass.h"
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "common/include/IMemSizeGadget.h"

#include "math.h"

namespace KEP {

CShadowClass::CShadowClass(float radius,
	float collRadius,
	float extentUp,
	float extentDown,
	bool scale,
	int textureIndex,
	const std::string& textureSearchName) {
	m_type = 0;
	m_lastNormal.x = 0.0f;
	m_lastNormal.y = 1.0f;
	m_lastNormal.z = 0.0f;
	m_wldPosition.Set(0, 0, 0);
	m_valid = FALSE;

	m_color.r = 1.0f;
	m_color.g = 1.0f;
	m_color.b = 1.0f;

	m_pd3dDevice8 = NULL;

	m_normals = NULL;

	m_vertices = NULL;
	m_numvertices = 0;

	m_collisionVects = NULL;
	m_numcollisionVects = 0;

	m_collisionScaleVects = NULL;
	m_numcollisionScaleVects = 0;

	m_firstTime = TRUE;

	m_scaleTexture = scale ? TRUE : FALSE;
	m_beginScaling = FALSE;

	m_originalRadius = radius;
	m_collisionRadius = collRadius;
	m_extentUp = (float)fabs(extentUp); //just to be safe...
	m_extentDown = (float)fabs(extentDown);
	m_textureSearchName = textureSearchName;
	m_textureIndex = textureIndex;

	//initialize shadow mesh object
	m_shadowMesh = NULL;
	m_shadowMesh = new CEXMeshObj();
	m_shadowMesh->m_blendType = 5;
	m_shadowMesh->m_validUvCount = 1;
	m_shadowMesh->m_materialObject->m_texNameHash[0] = textureIndex;
	m_shadowMesh->m_materialObject = new CMaterialObject();
	m_shadowMesh->m_materialObject->m_textureTileType = 2;
	m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.r = 1.0f;
	m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.g = 1.0f;
	m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.b = 1.0f;
	m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.a = 1.0f;
}

void CShadowClass::SafeDelete() {
	if (m_normals) {
		delete[] m_normals;
		m_normals = NULL;
	}

	if (m_vertices) {
		delete[] m_vertices;
		m_vertices = NULL;
	}

	if (m_collisionVects) {
		delete[] m_collisionVects;
		m_collisionVects = NULL;
	}

	if (m_collisionScaleVects) {
		delete[] m_collisionScaleVects;
		m_collisionScaleVects = NULL;
	}

	if (m_shadowMesh) {
		m_shadowMesh->m_abVertexArray.m_vertexArray1 = NULL;
		m_shadowMesh->SafeDelete();
		delete m_shadowMesh;
		m_shadowMesh = NULL;
	}
}

CShadowClass::~CShadowClass() {
}

void CShadowClass::Clone(CShadowClass** clone) {
	CShadowClass* returnClone = new CShadowClass(m_originalRadius,
		m_collisionRadius,
		m_extentUp,
		m_extentDown,
		m_scaleTexture == TRUE,
		m_textureIndex,
		m_textureSearchName);

	*clone = returnClone;
}

void CShadowClass::Calculate(const Vector3f& objectPos, CCollisionObj* collisionObject) {
	if (m_type == 1) {
		int objectHit = 0;

		Vector3f startSeg, segEnd;
		startSeg.x = objectPos.x;
		startSeg.y = objectPos.y + m_extentUp;
		startSeg.z = objectPos.z;
		segEnd.x = objectPos.x;
		segEnd.y = objectPos.y - m_extentDown;
		segEnd.z = objectPos.z;

		m_valid = collisionObject->CollisionSegmentIntersect(startSeg, segEnd, 100.0f, false, false, &m_wldPosition, &m_lastNormal, &objectHit);
	} else if (m_type == 0) {
		m_objectPos = objectPos;
		m_radius = m_originalRadius;

		if (m_collisionVects) {
			delete[] m_collisionVects;
			m_collisionVects = NULL;
		}
		MakeCollisionPoly();

		if (m_scaleTexture) {
			if (m_collisionScaleVects) {
				delete[] m_collisionScaleVects;
				m_collisionScaleVects = NULL;
			}
			MakeCollisionScalePoly();
			GetCollidedScalePolys(collisionObject);
			if (m_beginScaling)
				ScaleTexture();
		}

		if (m_normals) {
			delete[] m_normals;
			m_normals = NULL;
		}
		if (m_vertices) {
			delete[] m_vertices;
			m_vertices = NULL;
		}

		GetCollidedPolys(collisionObject);

		AddTextureCoordinates();

		if (m_firstTime)
			m_firstTime = FALSE;
		m_oldPos = m_objectPos;
	} //end matt texture shadow
}

void CShadowClass::GetCollidedScalePolys(CCollisionObj* collisionObject) {
	m_intersectY = 0;
	int CPolyCount = m_numcollisionScaleVects / 3;

	int totalPolys = 0;

	//get all polygons that collide with the collision shape
	for (int CPolyIndex = 0; CPolyIndex < CPolyCount; CPolyIndex++) {
		Vector3f intersection, normal;
		int numberPolys = 0;
		CPolyRetStruct collisionPolys[20];
		Vector3f collisionNormals[20];

		collisionObject->CollisionPolyRetrieval(&m_collisionScaleVects[CPolyIndex * 3],
			&intersection,
			&normal,
			numberPolys,
			collisionPolys,
			collisionNormals);

		totalPolys += numberPolys;
		m_intersectY += intersection.y;
	}

	if (totalPolys > 0)
		m_beginScaling = FALSE;
	else
		m_beginScaling = TRUE;

	m_intersectY /= CPolyCount;
}

void CShadowClass::GetCollidedPolys(CCollisionObj* collisionObject) {
	int cCollPolys = m_numcollisionVects / 3; //number of polys in CollisionVert structure

	CPolyRetStruct* PolyList = NULL;
	PolyList = new CPolyRetStruct[cCollPolys * 20]; //maximum number of polygons that can be returned
	int cPolyList = 0;

	Vector3f* Normals = NULL;
	Normals = new Vector3f[cCollPolys * 20]; //maximum number of normals that can be returned

	//get all polygons that collide with the collision shape
	for (int CPolyIndex = 0; CPolyIndex < cCollPolys; CPolyIndex++) {
		Vector3f intersection, normal;
		int numberPolys = 0;
		CPolyRetStruct collisionPolys[20];
		Vector3f collisionNormals[20];

		collisionObject->CollisionPolyRetrieval(&m_collisionVects[CPolyIndex * 3],
			&intersection,
			&normal,
			numberPolys,
			collisionPolys,
			collisionNormals);

		//add all polygons that are not duplicates
		for (int addpoly = 0; addpoly < numberPolys; addpoly++) {
			if (collisionNormals[addpoly].y < 0.5f && collisionNormals[addpoly].y > -0.5f)
				continue; //ignore polys that are mostly vertical

			if (!DuplicateTri(PolyList, cPolyList, collisionPolys[addpoly])) {
				PolyList[cPolyList] = collisionPolys[addpoly];
				Normals[cPolyList] = collisionNormals[addpoly];
				cPolyList++;
			}
		}
	}

	if (cPolyList <= 0)
		return;
	////////////////////////
	////////////////////////
	m_vertices = new ABVERTEX1L[3 * cPolyList];
	m_normals = new Vector3f[cPolyList];

	//copy polys and normals to class variables
	for (int poly = 0; poly < cPolyList; poly++) {
		//if(Normals[poly].y > -.01f && Normals[poly].y < .01f) continue; //ignore vertical polys
		m_vertices[(poly * 3) + 0] = ToVertex(PolyList[poly].vertices[0]);
		m_vertices[(poly * 3) + 1] = ToVertex(PolyList[poly].vertices[1]);
		m_vertices[(poly * 3) + 2] = ToVertex(PolyList[poly].vertices[2]);

		m_vertices[(poly * 3) + 0].ny = 1.0f;
		m_vertices[(poly * 3) + 1].ny = 1.0f;
		m_vertices[(poly * 3) + 2].ny = 1.0f;

		m_normals[poly] = Normals[poly];
	}

	//de-allocate memory
	if (PolyList)
		delete[] PolyList;
	if (Normals)
		delete[] Normals;

	//send out number of polygons
	m_numvertices = cPolyList * 3;
}

void CShadowClass::MakeCollisionScalePoly() {
	Vector3f pos = m_objectPos;

	//Creates a square pyramid
	float zmax = pos.z + 2.0f;
	float zmin = pos.z - 2.0f;
	float xmin = pos.x - 2.0f;
	float xmax = pos.x + 2.0f;
	float ymin = pos.y - 2.0f;
	float ymax = pos.y;

	m_collisionScaleVects = new Vector3f[6];

	m_collisionScaleVects[0] = Vector3f(pos.x, ymax, pos.z);
	m_collisionScaleVects[1] = Vector3f(xmin, ymin, zmax);
	m_collisionScaleVects[2] = Vector3f(xmax, ymin, zmin);

	m_collisionScaleVects[3] = Vector3f(pos.x, ymax, pos.z);
	m_collisionScaleVects[4] = Vector3f(xmin, ymin, zmin);
	m_collisionScaleVects[5] = Vector3f(xmax, ymin, zmax);

	m_numcollisionScaleVects = 6;
}

void CShadowClass::MakeCollisionPoly() {
	Vector3f pos = m_objectPos;

	//Creates a square pyramid
	float zmax = pos.z + m_collisionRadius;
	float zmin = pos.z - m_collisionRadius;
	float xmin = pos.x - m_collisionRadius;
	float xmax = pos.x + m_collisionRadius;
	float ymin = pos.y - m_extentDown;
	float ymax = pos.y + m_extentUp;

	m_collisionVects = new Vector3f[12];

	m_collisionVects[0] = Vector3f(pos.x, ymax, pos.z);
	m_collisionVects[1] = Vector3f(xmin, ymin, zmax);
	m_collisionVects[2] = Vector3f(xmax, ymin, zmin);

	m_collisionVects[3] = Vector3f(pos.x, ymax, pos.z);
	m_collisionVects[4] = Vector3f(xmin, ymin, zmin);
	m_collisionVects[5] = Vector3f(xmax, ymin, zmax);

	m_collisionVects[6] = Vector3f(pos.x, ymax, pos.z);
	m_collisionVects[7] = Vector3f(xmin, ymin, pos.z);
	m_collisionVects[8] = Vector3f(xmax, ymin, pos.z);

	m_collisionVects[9] = Vector3f(pos.x, ymax, pos.z);
	m_collisionVects[10] = Vector3f(pos.x, ymin, zmax);
	m_collisionVects[11] = Vector3f(pos.x, ymin, zmin);

	m_numcollisionVects = 12;
}

void CShadowClass::ScaleTexture() {
	return;
}

void CShadowClass::AddTextureCoordinates() {
	int numpolys = m_numvertices / 3;
	ABVERTEX1L* vtx;

	for (int poly = 0; poly < numpolys; poly++) {
		for (int line = 0; line < 3; line++) {
			vtx = &m_vertices[(poly * 3) + line];

			//y offset to help correct z-errors
			vtx->y += .2f;

			float d = m_radius * 2;
			//calculate texture coordinates

			if (m_objectPos.x > vtx->x)
				vtx->tx0.tu = (-m_objectPos.x + m_radius + vtx->x) / d;
			else
				vtx->tx0.tu = (-m_objectPos.x - m_radius + vtx->x) / d + 1.0f;

			if (m_objectPos.z > vtx->z)
				vtx->tx0.tv = (-m_objectPos.z + m_radius + vtx->z) / d;
			else
				vtx->tx0.tv = (-m_objectPos.z - m_radius + vtx->z) / d + 1.0f;

			/*if(Pos.x > vtx->x)//same code, but in a form that makes sense
				vtx->tu = -((m_objectPos.x-m_radius)-vtx->x)/(m_radius*2);
			else
				vtx->tu = (vtx->x-(m_objectPos.x+m_radius))/(m_radius*2)+1.0f;

			if(Pos.z > vtx->z)
				vtx->tv = -((m_objectPos.z-m_radius)-vtx->z)/(m_radius*2);
			else
				vtx->tv = (vtx->z-(m_objectPos.z+m_radius))/(m_radius*2)+1.0f;*/
		}
	}
}

//Helper functions
ABVERTEX1L CShadowClass::ToVertex(Vector3f pt) {
	ABVERTEX1L NewPt;
	ZeroMemory(&NewPt, sizeof(ABVERTEX1L));

	NewPt.x = pt.x;
	NewPt.y = pt.y;
	NewPt.z = pt.z;

	return NewPt;
}

BOOL CShadowClass::DuplicateTri(CPolyRetStruct* Polys, int cPolys, const CPolyRetStruct& TestPoly) {
	BOOL ret = FALSE;

	for (int p = 0; p < cPolys; p++) {
		if (Polys[p].vertices[0] == TestPoly.vertices[0] &&
			Polys[p].vertices[1] == TestPoly.vertices[1] &&
			Polys[p].vertices[2] == TestPoly.vertices[2]) {
			ret = TRUE;
			break;
		}
	}

	return ret;
}

void CShadowClass::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_originalRadius
		   << m_collisionRadius
		   << m_extentUp
		   << m_extentDown
		   << m_color.r
		   << m_color.g
		   << m_color.b
		   << m_scaleTexture
		   << CStringA(m_textureSearchName.c_str())
		   << m_textureIndex;
	} else {
		CStringA textureSearchName;

		ar >> m_originalRadius >> m_collisionRadius >> m_extentUp >> m_extentDown >> m_color.r >> m_color.g >> m_color.b >> m_scaleTexture >> textureSearchName >> m_textureIndex;

		m_textureSearchName = textureSearchName.GetString();
		m_type = 0;
		m_lastNormal.x = 0.0f;
		m_lastNormal.y = 1.0f;
		m_lastNormal.z = 0.0f;
		m_valid = FALSE;

		m_pd3dDevice8 = NULL;
		m_normals = NULL;

		m_vertices = NULL;
		m_numvertices = 0;

		m_collisionVects = NULL;
		m_numcollisionVects = 0;

		m_firstTime = TRUE;

		m_beginScaling = FALSE;
		m_collisionScaleVects = NULL;

		m_shadowMesh = NULL;
		m_shadowMesh = new CEXMeshObj;

		/*m_shadowMesh->m_blendType = 5;
		m_shadowMesh->m_materialObject = new CMaterialObject();
		m_shadowMesh->m_materialObject->m_textureTileType = 2;
		m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.r = 1.0f;
		m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.g = 1.0f;
		m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.b = 1.0f;
		m_shadowMesh->m_materialObject->m_baseMaterial.Emissive.a = 1.0f;*/
	}
}

void CShadowClass::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObjectSizeof(m_collisionScaleVects, m_numcollisionScaleVects);
		pMemSizeGadget->AddObjectSizeof(m_collisionVects, m_numcollisionVects);
		pMemSizeGadget->AddObject(m_shadowMesh);
		pMemSizeGadget->AddObject(m_textureSearchName);
		pMemSizeGadget->AddObjectSizeof(m_vertices, m_numvertices);
		pMemSizeGadget->AddObjectSizeof(m_normals, m_numvertices / 3);
	}
}

IMPLEMENT_SERIAL(CShadowClass, CObject, 0)

BEGIN_GETSET_IMPL(CShadowClass, IDS_SHADOWCLASS_NAME)
GETSET_RANGE(m_originalRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, ORIGINALRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_collisionRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, COLLISIONRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_extentUp, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, EXTENTUP, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_extentDown, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, EXTENTDOWN, FLOAT_MIN, FLOAT_MAX)
GETSET_RGBA(m_color.r, m_color.g, m_color.b, m_color.a, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, COLOR)
GETSET(m_scaleTexture, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, SCALETEXTURE)
GETSET_MAX(m_textureSearchName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, TEXTURESEARCHNAME, STRLEN_MAX)
GETSET_RANGE(m_textureIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, SHADOWCLASS, TEXTUREINDEX, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP
