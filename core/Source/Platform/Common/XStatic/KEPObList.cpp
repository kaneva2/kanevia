///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "..\include\CoreStatic\KEPObList.h"
#include "..\include\IMemSizeGadget.h"

namespace KEP {

void CKEPObList::GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const {
	pMemSizeGadget->AddObject(m_lastSavedPath);
}

void CKEPObList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		GetMemSizeofInternals(pMemSizeGadget);
	}
}

IMPLEMENT_SERIAL(CKEPObList, CObject, 0)

} // namespace KEP