///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Common\include\CoreStatic\CMultiplayerClass.h"

#include "common\KEPUtil\Helpers.h"
#include "Common/KEPUtil/SocketSubsystem.h"
#include "Common/KEPUtil/FileSwitch.h"

#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Tools/NetworkV2/AuthConfiguration.h"
#include "Tools/NetworkV2/KEPNet/KEPNet.h"
#include "Tools/NetworkV2/Protocols.h"

#include "resource.h"

#include "CStructuresMisl.h"
#include "CServerItemClass.h"
#include <KEPConfig.h>
#include "KEPConfigurationsXML.h"
#include <IGameState.h>
#include "CElementStatusClass.h"
#include <PassList.h>
#include "CAccountClass.h"
#include "CBountySystem.h"
#include "CClanSystemClass.h"
#include "CHousePlacementClass.h"
#include "CQuestJournal.h"
#include "CPacketObjListMsgEquip.h"
#include "CCurrencyClass.h"
#include "CInventoryClass.h"
#include "CNoterietyClass.h"
#include "CPlayerClass.h"
#include "CTitleObject.h"
#include "CStatClass.h"
#include "CWorldAvailableClass.h"
#include "FriendList.h"
#include "ScriptPlayerInfo.h"
#include "IGamestateDispatcher.h"
#include "SetThreadName.h"
#include "ISender.h"
#include "Common/include/NetworkCfg.h"
#include "UnicodeMFCHelpers.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "SerializeHelper.h"

#include <TinyXML/tinyxml.h>

#include "Common\include\LogHelper.h"

namespace KEP {

static LogInstance("Instance");

#define DEV_SERVER_MAX 30

#define PREVIOUS_NORM_CONFIG_MAX 1000 // This is what wok server runs for license limit...when it was license limited.
// We have no idea how important or unimportant it might be to set this value
// way out there.  E.g. would hate find out that it affected load balancing.

#define ALERT_LEVEL 1000

//Developer website company permissions
#define CP_ADMIN 1
#define CP_SUPER_USER 2
#define CP_ASSOCIATE 3

#ifdef _DEBUG
static const ULONG DEFAULT_UPDATE_INTERVAL = 30.0 * 1000.0;
#else
static const ULONG DEFAULT_UPDATE_INTERVAL = 5.0 * 60.0 * 1000.0;
#endif

CMultiplayerObj* CMultiplayerObj::m_pInstance = 0;

CMultiplayerObj::CMultiplayerObj(const char* name, ISender* sender) :
		m_packetQue(0),
		m_IPaddress(""),
		m_accountDatabase(0),
		m_runtimeAccountDB(0),
		m_runtimePlayersOnServer(0),
		m_protocolVersion(0),
		m_contentVersion(0),
		m_clientStatusFromServer(0),
		m_ipBlockList(),
		m_connectedAIservers(0),
		m_restrictedMode(FALSE),
		m_pMsgOutboxAttrib(nullptr),
		m_pMsgOutboxEquip(nullptr),
		m_pMsgOutboxEvent(nullptr),
		m_dispatcher(0),
		m_gameStateDispatcher(0),
		m_alwaysStartInHousingZone(false),
		m_serverNetwork(nullptr),
		m_clientNetwork(nullptr),
		m_tutorialZoneIndex(INVALID_ZONE_INDEX),
		m_tutorialZoneInstanceId(0),
		m_tutorialZoneSpawnIndex(0),
		m_disabledEdbs("3,4"), // WOK specific
		m_gameStateDb(0),
		m_mainThdId(GetCurrentThreadId()),
		m_pingTimeoutSec(10),
		m_maxPlayers(PREVIOUS_NORM_CONFIG_MAX) {
	m_pInstance = this;

	m_currentPlayersOnServer = 0;
	setIsClientEnabled(false);
	setIPAddress("");
	m_userName = "";
	m_passWord = "";
	m_accountDatabase = NULL;
	m_maxAllowedPlayers = m_maxPlayers; // until license sets it
	m_serverStarted = FALSE;
	m_runtimePlayersOnServer = NULL;
	m_currentWorldsUsed = NULL;
	m_port = 25857;
	m_clientCharacterMaxCount = 3;
	m_initialBankCash = 300;
	m_initialCash = 150;
	m_spawnPointCfgs = NULL;
	m_currentCharacterInUse = -1;
	m_cashCount = 0;
	m_bankCashCount = 0;
	m_enableAutoBackup = FALSE;
	m_maxPlayerBackupTimeMs = DEFAULT_UPDATE_INTERVAL;
	m_netTraceIdNext = 1;
	m_pServerItemObjMap = NULL;
	m_protocolVersion = 1;
	m_contentVersion = 1;
	m_clientStatusFromServer = 0;
	m_chatEnabled = FALSE;
	m_groupChatEnabled = FALSE;
	m_clanChatEnabled = FALSE;
	m_maxItemLimit_onChar = 40;
	m_maxItemLimit_inBank = 250;
	m_clanDatabase = NULL;
	m_housingDB = NULL;
	m_connectedAIservers = NULL;
	m_runtimeAccountDB = NULL;
	m_restrictedMode = FALSE;
	m_bountySystem = NULL;
	m_tryOnItemDefaultExpirationDuration = 5;

	InitializeCriticalSection(&m_criticalSection);
	InitializeCriticalSection(&m_loginInfoCS);

	m_msgMetrics.resize(255);
}

CMultiplayerObj::~CMultiplayerObj() {
	m_pInstance = NULL;
	DeleteCriticalSection(&m_criticalSection);
	DeleteCriticalSection(&m_loginInfoCS);
}

BOOL CMultiplayerObj::EnableServer(const char* key, eEngineType engineType, const char* instanceName) {
#ifndef _ClientOutput

	AuthConfiguration authConf;
	authConf.initialize(m_pathBase);

	PingerConfigurator configurator;
	configurator.initialize(m_pathBase)
		.setIPAddress(m_IPaddress)
		.setGameKey(key)
		.setInstanceName(instanceName)
		.setSchemaVersion(m_gameStateDb ? m_gameStateDb->SchemaVersion() : 0)
		.setServerVersion(Algorithms::GetModuleVersion().c_str())
		.setProtocolVersion(m_protocolVersion)
		.setTimeoutSecs(m_pingTimeoutSec)
		.setAssetVersion(m_contentVersion)
		.setEngineType(engineType)
		.setLoggerName("Pinger")
		.setPort(m_port)
		.setMaxPlayers(2000);

	m_protocolVersion = GetVersion();
	m_contentVersion = GetContentVersion();

	if (!m_spawnPointCfgs)
		m_spawnPointCfgs = new CSpawnCfgObjList();

	if (!m_currentWorldsUsed)
		m_currentWorldsUsed = new CWorldAvailableObjList();

	if (!m_clanDatabase)
		m_clanDatabase = new CClanSystemObjList();

	if (!m_housingDB)
		m_housingDB = new CHousingZoneDB();

	if (!m_bountySystem)
		m_bountySystem = new CBountyObjList();

	m_runtimePlayersOnServer = new CPlayerObjectList();
	m_pServerItemObjMap = new ServerItemObjMap();
	m_connectedAIservers = new CAccountObjectList();
	m_runtimeAccountDB = new CAccountObjectList();

	// Purge/Allocate Packet Queue
	PacketQueueAllocServer();

	if (!m_accountDatabase)
		m_accountDatabase = new CAccountObjectList();

	ULONG dwMaxPlayers = std::max(DEV_SERVER_MAX, maxPlayers()); // could be higher, but set DPlay to at least DEV_SERVER_MAX
	m_maxAllowedPlayers = dwMaxPlayers;

	if (m_serverNetwork)
		delete m_serverNetwork;

	// File switch is no longer appropriate as we get closer to release.
	// Simply use default
	m_serverNetwork = MakeServerNetwork(
		this, configurator, authConf, instanceName, "ServerNetwork");

	if (m_serverNetwork) {
		// create the logger passthru
		IServerNetworkCallback* callback = this;

		// set the parameters
		// Note that we are storing a pointer to this CMultiplayerClass
		// instance's m_maxAllowedPlayers data member.  This specifies the
		// maximum number of of concurrent users allowed by the applicable
		// license.  DPlayServer will subsequently call pingUpdate().
		// pingUpdate() receives this license info back from the ping service
		// and DPlayServer saves it to this variable, thus  making the value
		// of m_maxAllowPlayers appear to change, with no obvious calls
		// assignments to cause it.
		m_serverNetwork->setMaxConnections(m_maxAllowedPlayers);
		try {
			m_serverNetwork->Listen(callback, engineType, m_port, getIPAddress().c_str());
		} catch (const ChainedException exc) {
			m_gameStateDispatcher->SetServerNetwork(m_serverNetwork);
			LogError(exc.str());
			return FALSE;
		}
		m_gameStateDispatcher->SetServerNetwork(m_serverNetwork);
		m_serverStarted = TRUE;
	}

#else
	ASSERT(FALSE); // server only
#endif

	return TRUE;
}

void CMultiplayerObj::DisconnectClient(NETID netId, eDisconnectReason reason) {
	if (m_packetQue)
		m_packetQue->PurgeAllByID(netId);
	if (m_serverNetwork)
		m_serverNetwork->DisconnectClient(netId, reason);
}

bool CMultiplayerObj::DataFromClient(NETID netIdFrom, const BYTE* data, ULONG dataLen) {
	if (!m_packetQue) {
		LogError("Invalid packetQue!");
		return false;
	}

	int count = m_packetQue->AddPacketGetCount(dataLen, (const BYTE*)data, netIdFrom);
	if (count > 1000) {
		static Timer s_timer;
		if (s_timer.ElapsedSec() > 10.0) {
			s_timer.Reset();
			LogWarn("Packet queue count is " << count);
		}
	}

	return true;
}

// IServerNetworkCallback override
ULONG CMultiplayerObj::ClientConnected(NETID netId, const char* notUsedUserName) {
	return NET_OK;
}

// IServerNetworkCallback override
ULONG CMultiplayerObj::ClientDisconnected(NETID netId, eDisconnectReason reason) {
	IEvent* e = new DisconnectedEvent(netId, (ULONG)reason, 0);
	auto pPO = m_runtimePlayersOnServer->GetPlayerObjectByNetId(netId, false);
	if (pPO)
#ifndef REFACTOR_INSTANCEID
		e->SetFilter(pPO->m_currentZoneIndex);
#else
		e->SetFilter(pPO->m_currentZoneIndex.toLong());
#endif
	m_dispatcher->QueueEvent(e);
	return NET_OK;
}

/**
 * During the normal course of operation on an active server, the m_pendingLogins
 * is pruned when a user account is verified.  When a server is idle however
 * there is a slight leak because this pruning never occurs, even though woknettest
 * login failures cause the collection to be come populated.
 *
 * This memory should get recalled on the first login attempt as this will cause the
 * purge to occur.
 *
 * This function serves as stand alone pruning logic that can be called at will to
 * keep the pending logins clean.
 *
 * Note that this is a good application for Leases.  But for now kill it easy.
 */
void PrunePendingLogins(LogonInfoVect& m_pendingLogins) {
	TimeMs timeMs = fTime::TimeMs();
	for (auto i = m_pendingLogins.begin(); i != m_pendingLogins.end();) {
		if ((timeMs - (*i)->timestamp) > 30000) {
			// haven't seen them for 30 secs after first phase
			i = m_pendingLogins.erase(i);
		} else {
			i++;
		}
	}
}

// IServerNetworkCallback override, runs on thread
LOGON_RET CMultiplayerObj::ValidateLogon(
	NETID id,
	CONTEXT_VALIDATION* pMsg,
	const char* ipAddress,
	const char* userInfoXML) {
	int kuid = 0;
	std::string gender, birthDate, country;
	bool showMature = false;
	ULONG roleId = 0;

	LOGON_RET ret = LR_OK;

	const char* userName = pMsg->username;
	const char* password = pMsg->password;

	KEPConfig cfg;
	if (!cfg.Parse(userInfoXML, "user")) {
		LogError("Error parsing userinfo for " << userName);
		ret = LR_USER_INFO;
		goto VL_Exit;
	}
	kuid = cfg.Get("u", 0);
	gender = cfg.Get("g", "U");
	birthDate = cfg.Get("b", "1900-1-1");
	country = cfg.Get("c", "US");
	showMature = cfg.Get("m", false);
	roleId = cfg.Get("p", 0);
	if (gender.empty())
		gender = "U";
	if (birthDate.empty())
		birthDate = "1900-1-1";

	EnterCriticalSection(&m_criticalSection);

	//valid ip list
	if (m_ipBlockList.exists(ipAddress)) {
		//check to see if ip is blocked
		LeaveCriticalSection(&m_criticalSection);
		LogWarn("Blocked IP Address attempting connection: " << userName << " " << ipAddress);
		ret = LR_EMAIL; //ip is currently blocked  REJECTED
		goto VL_Exit;
	} //end check to see if ip is blocked
	LeaveCriticalSection(&m_criticalSection);

	ULONG minProtVer = pMsg->protocolVersionInfo;
	ULONG maxProtVer = pMsg->protocolVersionInfo;

	if (pMsg->protocolVersionInfo >= MIN_PROTOCOL_NEGO_VERSION) {
		// Peer supports protocol negotiation
		CONTEXT_VALIDATION_EX* pMsgEx = (CONTEXT_VALIDATION_EX*)pMsg;
		minProtVer = pMsgEx->minProtocolVersion;
	}

	ULONG sessionProtocolVersion = ProtocolCompatibility::Instance().negotiate(minProtVer, maxProtVer);
	if (sessionProtocolVersion == INVALID_PROTOCOL_VERSION) {
		//out of date protocol version
		LogError("Invalid protocol version for user " << userName << " his " << numToString(pMsg->protocolVersionInfo) << " != " << numToString(m_protocolVersion));
		ret = LR_VERSION; // disconnect them
		goto VL_Exit;
	}

	// Update protocol version in context so caller will get it as a return value
	pMsg->protocolVersionInfo = sessionProtocolVersion;

	if (m_contentVersion != 0 && pMsg->contentVersionInfo != m_contentVersion) {
		//out of date content version
		LogError("Invalid content version for " << userName << " his " << numToString(pMsg->contentVersionInfo) << " != " << numToString(m_contentVersion));
		ret = LR_VERSION; // disconnect them
		goto VL_Exit;
	}

	EnterCriticalSection(&m_loginInfoCS);
	PrunePendingLogins(m_pendingLogins);
	m_pendingLogins.push_back(LogonInfoPtr(new LogonInfo(
		userName,
		password,
		gender.at(0),
		birthDate.c_str(),
		showMature,
		country.c_str(),
		ipAddress,
		kuid,
		roleId,
		sessionProtocolVersion)));
	LeaveCriticalSection(&m_loginInfoCS);

	if (kuid == 0) {
		// local accts need to validate before continuing, otherwise if
		// local accts need to validate before continuing, otherwise if invalid, keeps going only to find out later
		// that it fails.  really probably only affects woknettest
		//
		ret = VerifyAccount(userName);
		if (ret != LR_OK)
			goto VL_Exit;
		else {
			// readd to queue for next time.
			EnterCriticalSection(&m_loginInfoCS);
			m_pendingLogins.push_back(LogonInfoPtr(new LogonInfo(
				userName,
				password,
				gender.at(0),
				birthDate.c_str(),
				showMature,
				country.c_str(),
				ipAddress,
				kuid,
				roleId,
				sessionProtocolVersion)));
			LeaveCriticalSection(&m_loginInfoCS);
		}
	}

VL_Exit:
	if (ret != LR_OK) {
		if (kuid != 0) {
			LogInfo("Cleaning up user before login complete. " << userName);
			m_serverNetwork->LogoffCleanup(kuid, DISC_FORCE);
		} else {
			// usually more informative error message logged before this
			if (m_serverNetwork->pinger().GetGameId() != 0) {
				LogError("User id not parsed.  May end up stuck on server. " << userName);
			}
		}
	}

	return ret;
}

LOGON_RET CMultiplayerObj::VerifyAccount(const char* userName) {
	LogonInfoPtr li;
	EnterCriticalSection(&m_loginInfoCS);
	TimeMs timeMs = fTime::TimeMs();
	for (LogonInfoVect::iterator i = m_pendingLogins.begin(); i != m_pendingLogins.end();) {
		if ((timeMs - (*i)->timestamp) > 30000) {
			// haven't seen them for 30 secs after first phase
			i = m_pendingLogins.erase(i);
		} else if ((*i)->userName == userName) {
			li = *i; // keep going in case > 1 (note auto_ptr will delete li if already set)
			i = m_pendingLogins.erase(i);
		} else
			i++;
	}
	LeaveCriticalSection(&m_loginInfoCS);
	if (li.get() == 0) {
		LogError("Account '" << userName << "' not found in pendingLogons");
		return LR_ACCT_NOT_FOUND;
	}

	// check at this high level if max limit has been hit
	// since if throttled down, can't ret DPlay limit < current number of users
	// only check if not ai
	// Moved here to avoid account being added to m_runtimeAccountDB when the server
	// hit its limits.
	EnterCriticalSection(&m_criticalSection);
	if (m_runtimeAccountDB && m_connectedAIservers &&
		maxPlayers() <= m_runtimeAccountDB->GetCount() - m_connectedAIservers->GetCount()) {
		LeaveCriticalSection(&m_criticalSection);
		LogError(loadStr(IDS_MAX_LIMIT_HIT) << maxPlayers());
		return LR_SERVER_MAX;
	}
	LeaveCriticalSection(&m_criticalSection);

	//Verify Account
	CAccountObject* pAO = NULL;
	int logOnStatus = m_accountDatabase->LogOn(
		li->userName.c_str(),
		li->password.c_str(),
		li->ipAddress.c_str(),
		&pAO,
		m_runtimeAccountDB,
		li->serverId);
	if (m_gameStateDb == 0 && pAO != 0) {
		if (pAO->m_administratorAccess == FALSE && m_restrictedMode != FALSE) //server non public
			logOnStatus = LOGIN_SERVER_PRIVATE;
	}

	if (pAO != 0) {
		// set the additional values on the acct
		pAO->m_gender = li->gender;
		if (!li->birthDate.empty())
			pAO->m_birthDate = li->birthDate;
		pAO->m_showMature = li->showMature;
		pAO->m_country = li->country;
		if (li->roleId > 0 && li->roleId < CP_ASSOCIATE) {
			pAO->m_canSpawn = TRUE;
			pAO->m_gm = TRUE;
		}

		if (li->roleId == CP_ADMIN)
			pAO->m_administratorAccess = TRUE;

		pAO->setCurrentProtocolVersion(li->protocolVersion);
	}

	if (logOnStatus == LOGIN_FORCE_OFF) { // auth failure
		LogError("LOGIN_FORCE_OFF - '" << userName << "'");
		//		logOnStatus = LOGIN_NOT_FOUND; // force them off
	}

	if (logOnStatus == LOGIN_OK) {
		EnterCriticalSection(&m_criticalSection);
		if (pAO && pAO->m_aiServer) {
			if (!m_connectedAIservers->GetAccountObjectByAccountNumber(pAO->m_accountNumber))
				m_connectedAIservers->AddTail(pAO);
		}
		LeaveCriticalSection(&m_criticalSection);
		return LR_OK; //allow log in
	}

	else if (logOnStatus == LOGIN_NOT_FOUND) {
		if (m_serverNetwork->pinger().GetGameId() != 0) {
			LogError("LOGIN_NOT_FOUND - '" << userName << "' ipAddress=" << li->ipAddress);
		}
		return LR_ACCT_NOT_FOUND;
	}

	else if (logOnStatus == LOGIN_SUSPENDED) {
		LogError("LOGIN_SUSPENDED - '" << userName << "' ipAddress= " << li->ipAddress);
		return LR_SUSPENDED;
	}

	else if (logOnStatus == LOGIN_ALREADY_LOGGED_ON) {
		LogError("LOGIN_ALREADY_LOGGED_ON - '" << userName << "' ipAddress=" << li->ipAddress);
		return LR_ALREADY_LOGGED_ON;
	}

	else if (logOnStatus == LOGIN_NONPAYMENT) {
		LogError("LOGIN_NONPAYMENT - '" << userName << "' ipAddress=" << li->ipAddress);
		return LR_ACCT_HOLD;
	}

	if (logOnStatus == LOGIN_SERVER_PRIVATE) {
		LogError("LOGIN_SERVER_PRIVATE - '" << userName << "' ipAddress=" << li->ipAddress);
		return LR_SVR_NOT_PUBLIC;
	}

	return logOnStatus == LOGIN_OK ? LR_OK : LR_ACCT_NOT_FOUND;
}

// IClientNetworkCallback overrride
bool CMultiplayerObj::DataFromServer(NETID netIdFrom, const BYTE* data, ULONG dataLen) {
	// DRF - Loss of Precision
	if ((dataLen == 0) || (dataLen > MAX_INT)) {
		LogError("Invalid Packet Data Length - dataLen=" << dataLen << " netIdFrom=" << netIdFrom);
	} else {
		// DRF - Potential Null Pointer Bug Fix
		if (!m_packetQue) {
			LogError("AddPacket() - Invalid packetQue!");
			return false;
		}
		m_packetQue->AddPacket((int)dataLen, (const BYTE*)data, netIdFrom);
	}

	return true;
}

// Network Layer Callback (IClientNetworkCallback override)
NETRET CMultiplayerObj::Connected(LOGON_RET logonRet, NETRET hr, ULONG sessionProtocolVersion) {
	// Convert logonRet To Error Message
	GetConnectionCode(logonRet);

	// DRF - DPNHRESULT Is Least 3 Hex Digits (see dplay8.h::MAKE_DPNHRESULT())
	HRESULT hrCut = hr & 0xfff;

	// Logon Ok ?
	bool ok = (hrCut == S_OK) && (hr == NET_OK) && (logonRet == LR_OK);
	if (ok)
		LogInfo("OK");
	else
		LogError("'" << m_connectionErrorCode << "' logonRet='" << logonRet << "' hr=0x" << std::hex << hrCut);

	// Enable Client If Ok
	setIsClientEnabled(ok);

	switch (hr) {
		case NET_OK:
			m_clientProtocolVersion = sessionProtocolVersion;
			// if ok, we'll fire logon event after LOG_STRUCT sent
			if (logonRet == LR_OK)
				return NET_OK;
			break;

		case NET_HOSTREJECTEDCONNECTION:
			m_clientStatusFromServer = 1;
			break;
	}

	// Send LogonResponseEvent -> LoginMenu.lua
	if (m_dispatcher) {
		LogonResponseEvent* pEvent = new LogonResponseEvent();
		pEvent->SetErrorCodes(m_connectionErrorCode.c_str(), hr, 0, logonRet);
		m_dispatcher->QueueEvent(pEvent);
	}

	return NET_OK;
}

// Network Layer Callback (IClientNetworkCallback override)
NETRET CMultiplayerObj::Disconnected(const std::string& reason, HRESULT hr, BOOL report, LOGON_RET logonRet, bool connecting) {
	// Convert logonRet To Error Message
	GetConnectionCode(logonRet);

	// DRF - DPNHRESULT Is Least 3 Hex Digits (see dplay8.h::MAKE_DPNHRESULT())
	HRESULT hrCut = hr & 0xfff;
	if (hrCut == S_OK)
		LogInfo("OK");
	else
		LogError("'" << m_connectionErrorCode << "' logonRet='" << logonRet << "' hr=0x" << std::hex << hrCut);

	if (report) {
		IEvent* e = new DisconnectedEvent(0, 0, 0);
		if (m_dispatcher && e) {
			(*e->OutBuffer()) << reason << (int)hr;
			m_dispatcher->QueueEvent(e);
		}
	}

	// Client No Longer Enabled
	setIsClientEnabled(false);

	//Report null client content version
	ULONG nDiscDiagCode = DISC_DIAG_NOERROR;
	if (m_contentVersion == 0) {
		std::string sVersionInfoPath = GetVersionInfoFullPath(NULL);
		if (!FileHelper::Exists(sVersionInfoPath)) {
			if (errno == ENOENT) {
				nDiscDiagCode = DISC_DIAG_VERINFO_NOTFOUND;
			} else {
				nDiscDiagCode = DISC_DIAG_VERINFO_NOACCESS;
			}
		} else {
			if (FileHelper::Size(sVersionInfoPath) == 0) {
				nDiscDiagCode = DISC_DIAG_VERINFO_EMPTY;
			} else {
				char version[100];
				version[0] = '\0';
				GetPrivateProfileStringA("CURRENT", "VERSION", "", version, 99, sVersionInfoPath.c_str());
				if (version[0] == '\0') {
					nDiscDiagCode = DISC_DIAG_VERINFO_NOVERSION;
				} else if (strspn(version, "0123456789.") != strlen(version)) {
					nDiscDiagCode = DISC_DIAG_VERINFO_BADVERSION;
				} else if (strspn(version, "0.") == strlen(version)) {
					nDiscDiagCode = DISC_DIAG_VERINFO_ZEROVERSION;
				} else {
					nDiscDiagCode = DISC_DIAG_VERINFO_OTHERISSUES;
				}
			}
		}
	}

	// Send LogonResponseEvent -> LoginMenu.lua
	if (m_dispatcher) {
		LogonResponseEvent* e = new LogonResponseEvent();
		if (e) {
			e->SetErrorCodes(m_connectionErrorCode.c_str(), hr, 0, logonRet, nDiscDiagCode);
			m_dispatcher->QueueEvent(e);
		}
	}

	return NET_OK;
}

bool CMultiplayerObj::SendServerClientExitEvent(bool reconnectingToOtherServer) {
	if (!m_dispatcher)
		return false;

	LogWarn("reconnectingToOtherServer=" << LogBool(reconnectingToOtherServer));

	// Send ClientExitEvent To Server
	IEvent* pEvent = new ClientExitEvent(reconnectingToOtherServer);
	pEvent->AddTo(SERVER_NETID);
	pEvent->SetFlags(EF_SENDIMMEDIATE);
	m_dispatcher->QueueEvent(pEvent);

	return true;
}

bool CMultiplayerObj::EnableClient() {
	// Disable Client & Tear Down Network Stack (reconnecting to other server)
	DisableClient(true);

	//retrieve version from file
	m_protocolVersion = GetVersion();
	m_contentVersion = GetContentVersion();
	GetIPDataFromDisk();

	// Purge/Allocate Packet Queue & All Message Outboxes
	PacketQueueAllocClient();

	bool disableNATTraversal = false;
	unsigned int timeoutTimeMillis = 0;

	// Default to the name of currently defined default
	std::string cfgProvider = INetwork::NetworkProviderName(INetwork::defaultNetworkProvider);

	try {
		std::string path = PathAdd(m_pathBase, "Network.cfg");
		KEPConfig cfg;
		cfg.Load(path.c_str(), NETCFG_ROOT);
		cfg.GetValue(DISABLENATTRAVERSAL_TAG, disableNATTraversal, disableNATTraversal);
		cfg.GetValue("NetworkProvider", cfgProvider, cfgProvider.c_str());
		cfg.GetValue("NetworkTimeoutTimeMillis", (int&)timeoutTimeMillis, (int)0);

	} catch (KEPException* e) {
		LogError("KEPConfig::Load(Network.cfg) FAILED");
		e->Delete();
	}

	// Create New Client Network
	INetwork::NetworkProvider provider = INetwork::NetworkProviderWithName(cfgProvider);
	LogInfo("ENABLED - " << (const char*)getIPAddress().c_str() << ":" << m_port);
	LogInfo("Creating " << INetwork::NetworkProviderName(provider) << " client");
	m_clientNetwork.reset(MakeClientNetwork(m_pathBase.c_str(), "ClientNetwork", disableNATTraversal, provider));

	// Perform Actual Network Connection
	CONTEXT_VALIDATION_EX cntxValidation;
	FillInLogonContext(cntxValidation, false, true);
	NETRET ret = m_clientNetwork->Connect(this, m_port, getIPAddress().c_str(), (BYTE*)&cntxValidation, sizeof(cntxValidation));

	// Connection Ok?
	bool ok = (ret == NET_OK || ret == NET_PENDING);
	if (!ok) {
		LogError("ClientNetwork::Connect() FAILED - '" << cntxValidation.username << " netRet='" << NetRetToString(ret) << "'");
	} else {
		LogInfo("ClientNetwork::Connect() OK - '" << cntxValidation.username << "' ipAddress=" << getIPAddress() << ":" << m_port);
	}

	if (timeoutTimeMillis > 0)
		m_clientNetwork->synchronizeTimeoutTimeMillis(timeoutTimeMillis);
	return ok;
}

bool CMultiplayerObj::DisableClient(bool reconnectingToOtherServer) {
	// Send Server Client Exit Event If logged In
	bool sendExitEvent = isClientEnabled();
	if (sendExitEvent)
		SendServerClientExitEvent(reconnectingToOtherServer);

	setIsClientEnabled(false);

	if (m_clientNetwork) {
		LogWarn("DISABLED");

		// Give Exit Event Time To Hit Server ?
		if (sendExitEvent)
			::Sleep(100);

		// Tear Down Network Stack
		m_clientNetwork.reset();
	}

	// Purge/Allocate Packet Queue & All Message Outboxes
	PacketQueueAllocClient();

	return true;
}

void CMultiplayerObj::PacketQueueAllocServer() {
	if (m_packetQue)
		m_packetQue->PurgeAll();
	else
		m_packetQue = new CSerializedPacketObjList(CPacketObjList::LEN_FMT_BYTE);
}

void CMultiplayerObj::PacketQueueAllocClient() {
	if (m_packetQue)
		m_packetQue->PurgeAll();
	else
		m_packetQue = new CSerializedPacketObjList(CPacketObjList::LEN_FMT_BYTE);

	if (m_pMsgOutboxAttrib)
		m_pMsgOutboxAttrib->PurgeAll();
	else
		m_pMsgOutboxAttrib = new CPacketObjList(CPacketObjList::LEN_FMT_NONE);

	if (m_pMsgOutboxEquip)
		m_pMsgOutboxEquip->PurgeAll();
	else
		m_pMsgOutboxEquip = new CPacketObjListMsgEquip();

	if (m_pMsgOutboxEvent)
		m_pMsgOutboxEvent->PurgeAll();
	else
		m_pMsgOutboxEvent = new CPacketObjList(CPacketObjList::LEN_FMT_ULONG);
}

void CMultiplayerObj::PacketQueueFree() {
	if (m_packetQue) {
		m_packetQue->SafeDelete();
		delete m_packetQue;
		m_packetQue = nullptr;
	}

	if (m_pMsgOutboxAttrib) {
		m_pMsgOutboxAttrib->SafeDelete();
		delete m_pMsgOutboxAttrib;
		m_pMsgOutboxAttrib = nullptr;
	}

	if (m_pMsgOutboxEquip) {
		m_pMsgOutboxEquip->SafeDelete();
		delete m_pMsgOutboxEquip;
		m_pMsgOutboxEquip = nullptr;
	}

	if (m_pMsgOutboxEvent) {
		m_pMsgOutboxEvent->SafeDelete();
		delete m_pMsgOutboxEvent;
		m_pMsgOutboxEvent = nullptr;
	}
}

void CMultiplayerObj::SafeDelete() {
#ifndef _ClientOutput
	if (m_serverNetwork) {
		delete m_serverNetwork;
		m_serverNetwork = nullptr;
	}
#endif

	if (m_clientNetwork) {
		m_clientNetwork.reset();
	}

	if (m_runtimeAccountDB) {
		m_runtimeAccountDB->RemoveAll(); //DO NOT SAFEDELETE-LIST OF REFERENCES
		delete m_runtimeAccountDB;
		m_runtimeAccountDB = nullptr;
	}

	if (m_bountySystem) {
		m_bountySystem->SafeDelete();
		delete m_bountySystem;
		m_bountySystem = nullptr;
	}

	if (m_housingDB) {
		m_housingDB->SafeDelete();
		delete m_housingDB;
		m_housingDB = nullptr;
	}

	if (m_pServerItemObjMap) {
		m_pServerItemObjMap->SafeDelete();
		delete m_pServerItemObjMap;
		m_pServerItemObjMap = nullptr;
	}

	if (m_accountDatabase) {
		m_accountDatabase->SafeDelete();
		delete m_accountDatabase;
		m_accountDatabase = nullptr;
	}

	if (m_runtimePlayersOnServer) {
		m_runtimePlayersOnServer->SafeDelete();
		delete m_runtimePlayersOnServer;
		m_runtimePlayersOnServer = nullptr;
	}

	if (m_currentWorldsUsed) {
		m_currentWorldsUsed->SafeDelete();
		delete m_currentWorldsUsed;
		m_currentWorldsUsed = nullptr;
	}

	if (m_spawnPointCfgs) {
		m_spawnPointCfgs->SafeDelete();
		delete m_spawnPointCfgs;
		m_spawnPointCfgs = nullptr;
	}

	if (m_clanDatabase) {
		m_clanDatabase->SafeDelete();
		delete m_clanDatabase;
		m_clanDatabase = nullptr;
	}

	if (m_connectedAIservers) {
		m_connectedAIservers->RemoveAll();
		delete m_connectedAIservers;
		m_connectedAIservers = nullptr;
	}

	m_pendingLogins.clear();

	PacketQueueFree();
}

// central point to call to logout a user and clean everything up
bool CMultiplayerObj::LogoutCleanup(
	CAccountObject* pAO,
	ULONG reason,
	CPlayerObject* pPO,
	POSITION* runtimeListPlayerPos,
	LONG* minutesConnected,
	IEvent* pEvent_reply) {
	if (!pAO)
		return false;

	bool ret = true;

	POSITION posLast = NULL;
	if (runtimeListPlayerPos != 0) {
		posLast = *runtimeListPlayerPos;
		if (!pPO)
			pPO = (CPlayerObject*)m_runtimePlayersOnServer->GetAt(*runtimeListPlayerPos);
	} else {
		for (POSITION posLoc = m_runtimePlayersOnServer->GetHeadPosition(); (posLast = posLoc) != NULL;) {
			auto pPO_find = (CPlayerObject*)m_runtimePlayersOnServer->GetNext(posLoc);
			if (pPO_find->m_pAccountObject == pAO) {
				pPO = pPO_find;
				break;
			}
		}
	}

	if (pPO) {
		LogInfo(pPO->ToStr() << " reason='" << reason << "'");
	} else {
		LogInfo("'" << pAO->m_accountName << "' netId=" << pAO->m_netId << " reason=" << reason);
	}

	// Send matching PlayerDepartedEvent based on the last recorded PlayerArrivedEvent
	NETID currentNetID = 0;
	ZoneIndex departZoneIndex;
	unsigned playerArriveId = 0;

	// let the script server know that the player is leaving this server
	if (pAO->FetchPendingPlayerDepartInfo(0, currentNetID, departZoneIndex, playerArriveId)) {
		if (pPO) {
			LogInfo("PlayerDepartedEvent -> " << pPO->ToStr());
		} else {
			LogInfo("PlayerDepartedEvent -> <" << pAO->m_accountName << ":" << currentNetID << ">");
		}

		assert(playerArriveId > 0);
		SendPlayerDepartedEvent(currentNetID, departZoneIndex, playerArriveId);
	}

	// clean up the account
	m_accountDatabase->LogOut(pAO, minutesConnected);
	pAO->m_currentcharInUse = -1;

	// clean up the player
	if (pPO) {
		if (reason == DISC_PLAYER_ZONED && pPO->playerOrigRef()) {
			pPO->playerOrigRef()->m_reconnecting = true; // suppress disconnect msg
		}
		m_runtimePlayersOnServer->RemoveAt(posLast);
		pPO->m_justSpawned = FALSE;
	}

	// remove all other players associated with this player's account (dead entities)
	for (POSITION posLoc = m_runtimePlayersOnServer->GetHeadPosition(); (posLast = posLoc) != NULL;) {
		auto pPO_find = (CPlayerObject*)m_runtimePlayersOnServer->GetNext(posLoc);
		if (pPO_find) {
			if (pPO_find->m_pAccountObject == pAO) {
				pPO_find->SafeDelete();
				delete pPO_find;
				pPO_find = 0;
				m_runtimePlayersOnServer->RemoveAt(posLast);
			}
		}
	}

	if (m_gameStateDb != 0 && pAO->m_serverId != 0) { // non-local account, remove from memory
		try {
			CPlayerObject* pPO_detach = pPO;

			if (pPO) {
				if (pPO->playerOrigRef())
					pPO_detach = pPO->playerOrigRef();

				// detach the player from the account object so Logoff can delete it
				POSITION delPos = NULL;
				for (POSITION pos = pAO->m_pPlayerObjectList->GetHeadPosition(); (delPos = pos) != NULL;) {
					auto pPO_find = (CPlayerObject*)pAO->m_pPlayerObjectList->GetNext(pos);
					if (pPO_find == pPO_detach) {
						pAO->m_pPlayerObjectList->RemoveAt(delPos);
						break;
					}
				}
			}

			jsAssert(reason >= DISC_NORMAL && reason <= DISC_SERVER_RESTART);
			m_serverNetwork->LogoffCleanup(pAO->m_serverId, (eDisconnectReason)reason);
			m_gameStateDb->Logoff(pAO, pPO, reason, pEvent_reply);

			m_runtimeAccountDB->RemoveAccountByAccNumWithoutDel(pAO->m_accountNumber);
			m_accountDatabase->SafeDeleteAccount(pAO);

			return true;
		} catch (KEPException* e) {
			LogError("EXCEPTION(KEPException) - " << e->m_msg);
			e->Delete();
			ret = false;
		}
	}

	// if get here, local or error
	if (pPO) {
		pPO->SafeDelete();
		delete pPO;
	}

	return ret;
}

void CMultiplayerObj::ServerTimeoutCallback() {
	if (!m_runtimePlayersOnServer)
		return;

	// Logout All Players Who Have Timed Out
	POSITION posLast;
	for (POSITION posLoc = m_runtimePlayersOnServer->GetHeadPosition(); (posLast = posLoc) != NULL;) {
		auto pPO = (CPlayerObject*)m_runtimePlayersOnServer->GetNext(posLoc);
		if (!pPO)
			continue;

		auto pAO = pPO->m_pAccountObject;
		if (!pAO)
			continue;

		if (pAO->m_sentLogoff)
			continue;

		// ED-7720 - Extend Timeout While Player Spawning
		TimeMs timeoutMs = 60.0 * MS_PER_SEC;
		if (pPO->m_justSpawned)
			timeoutMs = 10.0 * 60.0 * MS_PER_SEC;

		// Player Timeout ?
		TimeMs timeMs = pPO->m_timerMsgRx.ElapsedMs();
		if (timeMs > timeoutMs) {
			LogWarn("PLAYER TIMEOUT - Logging Out " << pPO->ToStr() << " (" << timeMs << "ms > " << timeoutMs << "ms)");
			LogoutCleanup(pAO, DISC_TIMEOUT, pPO, &posLast);
		}
	}
}

BOOL CMultiplayerObj::UpdateAllAccounts() {
	if (!m_runtimePlayersOnServer)
		return FALSE;

	EnterCriticalSection(&m_criticalSection);
	if (m_accountDatabase)
		for (POSITION posLoc = m_accountDatabase->GetHeadPosition(); posLoc != NULL;) {
			auto pAO = (CAccountObject*)m_accountDatabase->GetNext(posLoc);
			if (!pAO || !pAO->m_pPlayerObjectList)
				continue;

			if (pAO->m_currentcharInUse > -1) {
				auto pPO = m_runtimePlayersOnServer->GetPlayerObjectByNetId(pAO->m_netId, false);
				if (pPO) {
					if (!UpdateAccount(pPO, pAO))
						continue;
				}
			}
		}
	LeaveCriticalSection(&m_criticalSection);
	return TRUE;
}

BOOL CMultiplayerObj::UpdateAccount(CPlayerObject* pPO, CAccountObject* pAO) {
	if (!pAO || !pPO || (pAO->m_currentcharInUse < 0))
		return FALSE;

	POSITION chPos = pAO->m_pPlayerObjectList->FindIndex(pAO->m_currentcharInUse);
	if (!chPos) {
		LogError("playerObjectList.find() FAILED - '" << pAO->m_accountName << "' netId=" << pAO->m_netId);
		pAO->m_currentcharInUse = -1;
		return FALSE;
	}

	if (m_gameStateDb != 0) {
		try {
			m_gameStateDb->UpdatePlayer(pPO);
		} catch (KEPException* e) {
			LogError("Caught Exception - Logging Out " << pPO->ToStr() << " '" << e->ToString() << "'");
			delete e;
		}
	}

	auto pPO_cur = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(chPos);
	pPO_cur->m_previousZoneIndex = pPO->m_currentZoneIndex;
	pPO_cur->m_currentZoneIndex = pPO->m_currentZoneIndex;
	pPO_cur->m_parentZoneInstanceId = pPO->m_parentZoneInstanceId;
	pPO_cur->m_currentZonePriv = pPO->m_currentZonePriv;

	pPO_cur->m_movementInterpolator = pPO->m_movementInterpolator;

	pPO_cur->m_aiCfgOfMounted = pPO->m_aiCfgOfMounted;
	pPO_cur->m_clanHandle = pPO->m_clanHandle;
	pPO_cur->m_housingZoneIndex = pPO->m_housingZoneIndex;
	pPO_cur->m_title = pPO->m_title;
	pPO_cur->CopyDynamicallyAddedMembers(pPO);
	pPO_cur->GetSet::SetDirty(pPO->GetSet::IsDirty());

	//update inventory
	if (pPO_cur->m_inventory) {
		pPO_cur->m_inventory->SafeDelete();
		delete pPO_cur->m_inventory;
		pPO_cur->m_inventory = 0;
	}
	pPO->m_inventory->Clone(&pPO_cur->m_inventory);

	if (pPO_cur->m_reconfigInven) {
		pPO_cur->m_reconfigInven->SafeDelete();
		delete pPO_cur->m_reconfigInven;
		pPO_cur->m_reconfigInven = 0;
	}
	if (pPO->m_reconfigInven)
		pPO->m_reconfigInven->Clone(&pPO_cur->m_reconfigInven);

	if (pPO_cur->m_cash) {
		delete pPO_cur->m_cash;
		pPO_cur->m_cash = 0;
	}
	pPO->m_cash->Clone(&pPO_cur->m_cash);

	if (pPO_cur->m_giftCredits) { // 6/08 leak fix
		delete pPO_cur->m_giftCredits;
		pPO_cur->m_giftCredits = 0;
	}
	pPO->m_giftCredits->Clone(&pPO_cur->m_giftCredits);

	if (pPO_cur->m_noteriety) {
		delete pPO_cur->m_noteriety;
		pPO_cur->m_noteriety = 0;
	}
	pPO->m_noteriety->Clone(&pPO_cur->m_noteriety);

	// need to copy list of titles earned when updating the account
	if (pPO_cur->m_titleList != NULL && pPO->m_titleList != NULL) {
		delete pPO_cur->m_titleList;
		pPO_cur->m_titleList = 0;
		pPO_cur->m_titleList = new CTitleObjectList(*pPO->m_titleList);
	}

	if (pPO_cur->m_stats != NULL && pPO->m_stats != NULL) {
		delete pPO_cur->m_stats;
		pPO_cur->m_stats = 0;
		pPO->m_stats->Clone(&pPO_cur->m_stats);
	}

	if (pPO_cur->m_passList)
		DELETE_AND_ZERO(pPO_cur->m_passList);

	if (pPO->m_passList != 0)
		pPO_cur->m_passList = new PlayerPassList(*pPO->m_passList);

	// copy any guaranteed events this was added to get messages send right before a spawn, otherwise, they are lost
	CPacketObjList::MovePackets(pPO_cur->m_pMsgOutboxAttrib, pPO->m_pMsgOutboxAttrib);
	CPacketObjList::MovePackets(pPO_cur->m_pMsgOutboxEquip, pPO->m_pMsgOutboxEquip);
	CPacketObjList::MovePackets(pPO_cur->m_pMsgOutboxGeneral, pPO->m_pMsgOutboxGeneral);
	CPacketObjList::MovePackets(pPO_cur->m_pMsgOutboxEvent, pPO->m_pMsgOutboxEvent);

	// Reset Account Timer
	pPO_cur->m_updateAcctTimer.Reset();

	return TRUE;
}

void CMultiplayerObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(3);
		ASSERT(ar.GetObjectSchema() == 3);

		UpdateAllAccounts();

		CStringA ununsed;
		ar << (int)0 //m_frequencyLimit
		   << (int)0 //m_bandwidthAvailable
		   << m_currentPlayersOnServer
		   << (long)0 //m_totalBandwidthIncoming
		   << (BOOL)FALSE //m_clientSideFireing
		   << (BOOL)FALSE //m_PersistentType
		   << (BOOL)FALSE //m_messagingEnabledDplay
		   << (BOOL)FALSE //m_isHost
		   << m_userName
		   << m_passWord
		   << m_accountDatabase
		   << m_maxPlayers
		   << (int)0 // m_communicationFrequency
		   << (int)0 //m_frequencyBuffer
		   << (int)0 //m_frequencyIncrement
		   << (float)0.0 //m_serverCalculateRadius
		   << (float)0.0 //m_serverCalculateRadiusItems
		   << ununsed // m_serverKey
		   << ununsed // m_servername
		   << (BOOL)FALSE //m_passwordRequired
		   << (long)0 //m_deadEntityDissolveTime
		   << (int)0 //m_interpolationOnNetworkEntities
		   << m_currentWorldsUsed
		   << FALSE
		   << (long)0 //m_AIUpdateFrequency
		   << m_port
		   << (long)0 //m_timeOutDuration
		   << (long)0 //m_lagMSTolerance
		   << m_spawnPointCfgs
		   << (ULONG)0 //m_aiserverCycleDuration
		   << (long)0 //m_deadEntityNPCDissolveTime
		   << m_enableAutoBackup
		   << (int)0 //m_goodBehaviorDuration
		   << m_clanDatabase
		   << (int)0 //m_murderCountMax
		   << m_housingDB;

		ar << m_bountySystem;

	} else {
		int version = ar.GetObjectSchema();

		int freqLimit;
		int deprecatedInt;
		long deprecatedLong;
		ULONG deprecatedULong;
		float deprecatedFloat;
		BOOL deprecatedBool;
		CStringA deprecatedCString;
		ar >> freqLimit >> deprecatedInt //m_bandwidthAvailable
			>> m_currentPlayersOnServer >> deprecatedLong //m_totalBandwidthIncoming
			>> deprecatedBool //m_clientSideFireing
			>> deprecatedBool //m_PersistentType
			>> deprecatedBool //m_messagingEnabledDplay
			>> deprecatedBool //m_isHost
			>> m_userName >> m_passWord >> m_accountDatabase >> deprecatedInt // >> m_maxPlayers
			>> deprecatedInt //comFreq
			>> deprecatedInt //m_frequencyBuffer
			>> deprecatedInt //m_frequencyIncrement
			>> deprecatedFloat //m_serverCalculateRadius
			>> deprecatedFloat //m_serverCalculateRadiusItems
			>> deprecatedCString // m_serverKey
			>> deprecatedCString // m_servername
			>> deprecatedBool //m_passwordRequired
			>> deprecatedLong //m_deadEntityDissolveTime
			>> deprecatedInt //m_interpolationOnNetworkEntities
			>> m_currentWorldsUsed >> deprecatedBool >> deprecatedLong //m_AIUpdateFrequency
			>> m_port >> deprecatedLong //timeoutDur
			>> deprecatedLong //lagTol
			>> m_spawnPointCfgs >> deprecatedULong //m_aiserverCycleDuration
			>> deprecatedLong; //m_deadEntityNPCDissolveTime;

		if (version < 3) {
			CStringA autoBackupFile_Deprecated;
			ar >> deprecatedLong //autoBackupFrequency_Deprecated
				>> autoBackupFile_Deprecated;
		}

		ar >> m_enableAutoBackup >> deprecatedInt //m_goodBehaviorDuration
			>> m_clanDatabase >> deprecatedInt //m_murderCountMax
			>> m_housingDB;

		m_maxAllowedPlayers = m_maxPlayers; // until license sets it
		m_bountySystem = NULL;

		if (version > 1)
			ar >> m_bountySystem;

		m_runtimeAccountDB = NULL;
		m_protocolVersion = 1;
		m_contentVersion = 1;

		m_port = 25857;

		m_serverStarted = FALSE;
		setIsClientEnabled(false);
		setIPAddress("");
		m_serverStarted = FALSE;
		m_runtimePlayersOnServer = NULL;
		m_pServerItemObjMap = NULL;
		m_clientCharacterMaxCount = 3;
		m_initialBankCash = 300;
		m_initialCash = 150;
		m_currentCharacterInUse = -1;
		m_cashCount = 0;
		m_bankCashCount = 0;
		m_netTraceIdNext = 1;
		m_clientStatusFromServer = 0;
		m_chatEnabled = FALSE;
		m_groupChatEnabled = FALSE;
		m_clanChatEnabled = FALSE;

		m_maxItemLimit_onChar = 40;
		m_maxItemLimit_inBank = 250;

		m_restrictedMode = FALSE;

		m_requestProcessingErrorTimeMS = 1000;
		m_requestProcessingInfoTimeMS = 1000;

		m_tryOnItemDefaultExpirationDuration = 5;

		PacketQueueFree();
	}
}

Monitored::DataPtr CMultiplayerObj::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();

	int packetQueCount = 0;
	unsigned long packetQuePeakDepth = 0;
	if (m_packetQue) {
		packetQueCount = m_packetQue->GetCount();
		packetQuePeakDepth = m_packetQue->cummulativePeakQueueDepth();
	}
	(*p) << "<CMultiplayerObj>"
		 << "<PacketQueue "
		 << "QueueDepth=\"" << packetQueCount << "\" "
		 << "PeakQueueDepth=\"" << packetQuePeakDepth << "\"/>"
		 << "<PendingLogins count=\"" << m_pendingLogins.size() << "\"/>";
	if (!m_serverNetwork)
		(*p) << "<nonetwork/>";
	else
		(*p) << m_serverNetwork->monitor();

	(*p) << "</CMultiplayerObj>";

	Monitored::DataPtr ret(p);
	return ret;
}

// Returns next available netId
int CMultiplayerObj::GetNextFreeNetTraceId() {
	if (m_netTraceIdNext > 30000 || m_netTraceIdNext < 2)
		m_netTraceIdNext = 2;

	int netTraceId = m_netTraceIdNext;
	bool done = false;
	while (!done) {
		done = true;
		for (POSITION pos = m_runtimePlayersOnServer->GetHeadPosition(); pos;) {
			auto pPO = (CPlayerObject*)m_runtimePlayersOnServer->GetNext(pos);
			if (!pPO)
				continue;
			if (pPO->m_netTraceId == netTraceId) {
				done = false;
				break;
			}
		}

		if (done && m_pServerItemObjMap) {
			auto pSIO = m_pServerItemObjMap->GetObjectByNetTraceId(netTraceId);
			if (pSIO)
				done = false;
		}

		if (!done)
			netTraceId++;
	}
	m_netTraceIdNext = netTraceId + 1;
	return netTraceId;
}

// Broadcast message to all players in the distribution that exist on this server.
void CMultiplayerObj::BroadcastMessageToDistribution(
	const char* message,
	NETID fromNetId,
	const char* toDistribution,
	const char* fromRecipient,
	std::vector<PLAYER_HANDLE>& remotePlayerIds,
	eChatType chatType,
	bool fromRemote,
	ULONG filter) {
	RenderTextEvent* rte = new RenderTextEvent(message, chatType, toDistribution, fromRecipient);

	rte->SetFrom(SERVER_NETID);
	if (filter != 0) {
		rte->SetFilter(filter);
	}

	std::string distUnexpanded;
	std::string distExpanded;

	distUnexpanded.assign(toDistribution);

	PLAYER_HANDLE recipientId;
	std::string recipientType;
	std::string distributionName;

	ExpandChatGroups(distUnexpanded, distExpanded);

	while (GetNextPrivateChatDistributionPlayer(distExpanded, &recipientId, recipientType, distributionName)) {
		bool ret = AddToPrivateChatDistribution(recipientId, rte, message); // only adds if player is on our server
		if (!ret)
			remotePlayerIds.push_back(recipientId);
	}

	if (rte->To().size() > 0)
		m_dispatcher->QueueEvent(rte);
	else
		rte->Delete();
}

// Modifies the distribution list to expand groups to individual recipients
// Format "R23:bob,R24:scott,G88:agroup"
void CMultiplayerObj::ExpandChatGroups(std::string distIn, std::string& distOut) {
	if (distIn.size() == 0)
		return;

	distOut.clear();

	int stringStart = 0;
	int pos1 = 0;
	int pos2 = 0;

	do {
		std::string recipientType;
		std::string distributionName;

		recipientType.assign(distIn.substr(stringStart, 1)); // (R)ecipient or (G)roup

		pos1 = distIn.find_first_of(":", stringStart);
		pos2 = distIn.find_first_of(",", stringStart);

		int end = 0;
		if (pos2 == -1)
			// end of the list
			end = distIn.size();
		else
			end = pos2;

		std::string id = distIn.substr(stringStart + 1, pos1 - 1);
		int dbId = atoi(id.c_str());

		if (recipientType.compare("G") == 0) {
			// Eat the group if no db

			if (m_gameStateDb != 0) {
				// Expand the group based on its dbId and add each to the distOut

				std::vector<std::pair<int, std::string>> recipientList;

				m_gameStateDb->ExpandGroup(dbId, recipientList);

				for (std::vector<std::pair<int, std::string>>::iterator i = recipientList.begin(); i != recipientList.end(); i++) {
					char buf[128];
					int user_id = i->first;
					std::string user;
					_itoa_s(user_id, buf, _countof(buf), 10);
					user = buf;

					std::string charactername = i->second;

					std::string recipient;

					if (distOut.size() > 0) {
						recipient += ",";
					}

					recipient = "R";

					recipient += user;
					recipient += ":";
					recipient += charactername;

					distOut += recipient;
				}
			}
		} else {
			if (pos2 != -1)
				distOut += distIn.substr(stringStart, end + 1 - stringStart);
			else
				distOut += distIn.substr(stringStart, end - stringStart);
		}

		if (pos2 != -1)
			stringStart = end + 1;

	} while (pos2 != -1);
}

// Adds the player to the event if they exist on this server returns true if they are local
bool CMultiplayerObj::AddToPrivateChatDistribution(PLAYER_HANDLE player, IEvent* rte, const char* message) {
	auto pPO = (CPlayerObject*)m_runtimePlayersOnServer->GetPlayerObjectByPlayerHandle(player);
	if (!pPO)
		return false;

	bool ret = false;
	NETID currentID = pPO->m_netId;
	if (currentID == 0) {
		// allow players in spawning state to get messages
		if (pPO->m_pAccountObject && pPO->m_pAccountObject->m_netId != 0) {
			currentID = pPO->m_pAccountObject->m_netId;
		}
	}

	if (currentID != 0) {
		rte->AddTo(currentID);
		ret = true;
	}

	return ret;
}

bool CMultiplayerObj::GetNextPrivateChatDistributionPlayer(std::string& dist, PLAYER_HANDLE* recipientId, std::string& recipientType, std::string& distributionName) {
	if (dist.size() == 0)
		return false;

	recipientType.assign(dist.substr(0, 1)); // (R)ecipient or (G)roup

	int pos1 = dist.find_first_of(":");
	int pos2 = dist.find_first_of(",");

	int end = 0;
	if (pos2 == -1)
		// end of the list
		end = dist.size();
	else
		end = pos2;

	std::string id = dist.substr(1, pos1 - 1);
	*recipientId = atoi(id.c_str());

	distributionName.assign(dist.substr(pos1 + 1, end - pos1 - 1));

	if (pos2 == -1)
		dist.erase(0, end);
	else
		dist.erase(0, end + 1);

	return true;
}

// DRF - Appears To Trigger 'Press TAB to Interact' Message
void CMultiplayerObj::BroadcastMessage(
	short radius, // 0=to all
	const char* message,
	const Vector3f& position,
	const ChannelId& channel,
	NETID fromNetId,
	eChatType chatType,
	bool allInstances /*= false*/
) {
	RenderTextEvent* rte = new RenderTextEvent(message, chatType);
	BroadcastEvent(radius, rte, position, channel, fromNetId, chatType, allInstances, true);

	// now do static NPCs msgs
	if (chatType == eChatType::Talk || chatType == eChatType::Whisper) { // only send these types to NPCs
#ifndef REFACTOR_INSTANCEID
		if (m_dispatcher->HasHandler(NPCListenEvent::ClassId(), channel, 0))
#else
		if (m_dispatcher->HasHandler(NPCListenEvent::ClassId(), channel.toLong(), 0))
#endif
		{
			std::vector<int> ids;

			if (m_pServerItemObjMap) {
				ServerItemVec siVec;
				ServerItemVec* pSIV = nullptr;
				m_pServerItemObjMap->GetServerItemVec(pSIV, channel);
				if (pSIV)
					siVec = *pSIV;

				//check for permanent zone npcs that exist on all channels
				if (channel.isPermanent()) {
					pSIV = nullptr;
					m_pServerItemObjMap->GetServerItemVec(pSIV, channel.channelId());
					if (pSIV) {
						for (const auto& pSIO : *pSIV) {
							if (!pSIO)
								continue;
							if (pSIO->m_existsInAllPermInstances)
								siVec.push_back(pSIO);
						}
					}
				}

				for (const auto& pSIO : siVec) {
					if (!pSIO)
						continue;
					float n = std::max(std::max(abs(pSIO->m_position.x - position.x), abs(pSIO->m_position.y - position.y)), abs(pSIO->m_position.z - position.z));
					if (radius == 0 || n < radius && n > 0)
						ids.push_back(pSIO->m_spawnID);
				}
			}

			if (!ids.empty())
				m_dispatcher->QueueEvent(new NPCListenEvent(message, chatType, channel, radius, position.x, position.y, position.z, ids));
		}
	}
}

// code from old BroadcastMessage, now does any event
void CMultiplayerObj::BroadcastEvent(
	short radius, //0 radius = to all
	IEvent* pEvent,
	const Vector3f& position,
	const ChannelId& channel,
	NETID fromNetId,
	eChatType chatType,
	bool allInstances /*= false*/,
	bool restrictIfBlocked /*=false*/
) {
	pEvent->SetFrom(SERVER_NETID);

	CPlayerObject* pPO = nullptr;
	if (chatType == eChatType::Race || chatType == eChatType::Team || chatType == eChatType::Clan || chatType == eChatType::Group) {
		if (fromNetId != 0)
			pPO = m_runtimePlayersOnServer->GetPlayerObjectByNetId(fromNetId, false);
		if (!pPO)
			return;
	}

	if (chatType == eChatType::Clan) {
		// use existing method, which is very similar to what we do below
		SendEventToClan(pPO, pEvent);
		return;
	} else if (chatType == eChatType::Group) {
		// use existing method, which is very similar to what we do below
		SendEventToGroup(pPO, pEvent);
		return;
	}

	std::vector<ULONG>* destKanevaIds = new std::vector<ULONG>;

	std::set<NETID> spawnedNetIDs;
	for (POSITION posLoc = m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
		auto pPO_find = (CPlayerObject*)m_runtimePlayersOnServer->GetNext(posLoc);
		spawnedNetIDs.insert(pPO_find->m_netId);
	}

	for (POSITION posLoc = m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
		auto pPO_find = (CPlayerObject*)m_runtimePlayersOnServer->GetNext(posLoc);

		if (pPO_find->m_aiControlled) // TODO: Send this event to the AI Server
			continue;

		NETID currentID = pPO_find->m_netId;
		if (currentID == 0) {
			// allow players in spawning state to get messages
			if (pPO_find->m_pAccountObject && pPO_find->m_pAccountObject->m_netId != 0)
				currentID = pPO_find->m_pAccountObject->m_netId;
			else
				continue;
		}

		if (allInstances && channel.channelIdEquals(pPO_find->m_currentChannel) || !allInstances && channel == pPO_find->m_currentChannel || channel == INVALID_CHANNEL_ID) {
			if (pPO_find->m_dbCfg >= 0 && currentID != 0) {
				// bounding box check
				float n = 0;
				if (radius > 0) {
					if (pPO_find->m_gm)
						n = 0; // gm's now hear all (4/07)
					else {
						Vector3f pos = pPO_find->InterpolatedPos();
						n = std::max(std::max(
									abs(pos.x - position.x),
									abs(pos.y - position.y)),
							abs(pos.z - position.z));
					}
				}

				if (radius == 0 || n < radius) {
					pEvent->AddTo(currentID);
					if (restrictIfBlocked) {
						CAccountObject* acct = (CAccountObject*)pPO_find->m_pAccountObject;
						if (acct) {
							destKanevaIds->push_back(acct->m_serverId);
						}
					}
				}
			} //end id
		} //end same world
	}

	// Check account database for spawn-pending players
	for (POSITION posAcc = m_accountDatabase->GetHeadPosition(); posAcc != NULL;) {
		auto pAO = (CAccountObject*)m_accountDatabase->GetNext(posAcc);

		// Logged-on but not spawned
		if (pAO && pAO->m_loggedOn && pAO->m_netId != (NETID)-1 && spawnedNetIDs.find(pAO->m_netId) == spawnedNetIDs.end()) {
			const SpawnInfo& spawnInfo = pAO->GetSpawnInfo();
			int spawnZoneIndexInt = spawnInfo.getZoneIndexInt();
			ZoneIndex spawnZoneIndex(spawnZoneIndexInt);

			if (!spawnInfo.isSet() || spawnZoneIndex == INVALID_ZONE_INDEX) {
				continue;
			}

			CWorldAvailableObj* pWldAvailObj = m_currentWorldsUsed->GetByZoneIndex(spawnZoneIndex);
			if (pWldAvailObj == nullptr) {
				continue;
			}

			Vector3f spawnPt = spawnInfo.getSpawnPt();

			ChannelId currentChannel = pWldAvailObj->m_channel;
			currentChannel.setInstanceAndTypeFrom(spawnZoneIndexInt);

			if (currentChannel == channel ||
				allInstances && channel.channelIdEquals(currentChannel)) {
				// bounding box check
				float n = 0;
				if (radius > 0) {
					if (pAO->m_gm)
						n = 0; // gm's now hear all (4/07)
					else
						n = std::max(std::max(abs(spawnPt.x - position.x), abs(spawnPt.x - position.y)), abs(spawnPt.x - position.z));
				}

				if (radius == 0 || n < radius) {
					bool add = false;
					switch (chatType) {
						case eChatType::Race:
						case eChatType::Team:
							// Unable to identify race and team at this moment
							break;

						default:
							add = true;
							break;
					}
					if (add) {
						pEvent->AddTo(pAO->m_netId);
						if (restrictIfBlocked) {
							destKanevaIds->push_back(pAO->m_serverId);
						}
					}
				}
			}
		}
	}

	if (pEvent->To().size() > 0) {
		// are we supposed to restrict the event for users that block it?
		std::vector<ULONG> blockedKanevaIds;
		if (restrictIfBlocked && m_gameStateDispatcher) {
			if (!pPO)
				pPO = m_runtimePlayersOnServer->GetPlayerObjectByNetId(fromNetId, false);
			if (pPO) {
				auto pAO = pPO->m_pAccountObject;
				IBackgroundDBEvent* e = new BroadcastEventBackgroundEvent(pEvent, destKanevaIds, pAO->m_serverId);
				m_gameStateDispatcher->QueueEvent(e);
				return;
			}
		}

		if (pEvent->To().size() > 0)
			m_dispatcher->QueueEvent(pEvent);
	} else {
		pEvent->Delete();
	}

	delete destKanevaIds;
}

bool CMultiplayerObj::SendTextMessageToGroup(CPlayerObject* pPO, const char* message) {
	RenderTextEvent* pEvent = new RenderTextEvent(message, eChatType::Group);
	pEvent->SetFrom(SERVER_NETID);
	bool ok = SendEventToGroup(pPO, pEvent);
	if (!ok)
		pEvent->Delete();
	return ok;
}

bool CMultiplayerObj::SendEventToGroup(CPlayerObject* pPO, IEvent* pEvent) {
	if (!pPO->playerOrigRef()->m_friendList)
		return false;

	pEvent->AddTo(pPO->m_netId);
	bool ok = true;
	for (POSITION posLoc = pPO->playerOrigRef()->m_friendList->GetHeadPosition(); posLoc != NULL;) {
		auto pFI = (FriendInfo*)pPO->playerOrigRef()->m_friendList->GetNext(posLoc);
		auto pPO_find = (CPlayerObject*)m_runtimePlayersOnServer->GetPlayerObjectByPlayerHandle(pFI->m_handle);
		if (pPO_find == 0 || pPO_find->m_aiControlled || pPO_find->m_netId == 0)
			continue;
		pEvent->AddTo(pPO_find->m_netId);
	}
	m_dispatcher->QueueEvent(pEvent);
	return ok;
}

bool CMultiplayerObj::SendTextMessageToClan(CPlayerObject* pPO, const char* message) {
	auto pEvent = new RenderTextEvent(message, eChatType::Clan);
	pEvent->SetFrom(SERVER_NETID);
	bool ok = SendEventToClan(pPO, pEvent);
	if (!ok)
		pEvent->Delete();
	return ok;
}

bool CMultiplayerObj::SendEventToClan(CPlayerObject* pPO, IEvent* pEvent) {
	if (!pPO)
		return false;

	PLAYER_HANDLE clanHandle = pPO->m_clanHandle;
	if (clanHandle == NO_CLAN || !m_clanDatabase)
		return false;

	CClanSystemObj* pCSO = m_clanDatabase->GetClanByHandle(clanHandle);
	if (!pCSO || !pCSO->m_memberList)
		return false;

	pEvent->AddTo(pPO->m_netId);

	bool ok = false;
	for (POSITION posLoc = pCSO->m_memberList->GetHeadPosition(); posLoc != NULL;) {
		auto pCMO = (CClanMemberObj*)pCSO->m_memberList->GetNext(posLoc);
		auto pPO_find = (CPlayerObject*)m_runtimePlayersOnServer->GetPlayerObjectByPlayerHandle(pCMO->m_memberHandle);
		if (pPO_find == 0 || pPO_find->m_aiControlled || pPO_find->m_netId == 0)
			continue;
		pEvent->AddTo(pPO_find->m_netId);
	}
	if (pEvent->To().size() > 0) {
		ok = true;
		m_dispatcher->QueueEvent(pEvent);
	}

	return ok;
}

CStringA WINAPI CMultiplayerObj::GetIPAddress(NETID id) {
	return m_serverNetwork ? m_serverNetwork->GetIPAddress(id).c_str() : UNDEFINED_IP_ADDRESS;
}

BOOL CMultiplayerObj::CleanClanDatabase() {
	if (!m_clanDatabase)
		return FALSE;

	for (POSITION posLoc = m_clanDatabase->GetHeadPosition(); posLoc != NULL;) {
		auto pCSO = (CClanSystemObj*)m_clanDatabase->GetNext(posLoc);
		if (!pCSO->m_memberList)
			continue;

		POSITION memPosLast;
		for (POSITION memPos = pCSO->m_memberList->GetHeadPosition(); (memPosLast = memPos) != NULL;) {
			auto pCMO = (CClanMemberObj*)pCSO->m_memberList->GetNext(memPos);
			auto pPO_find = m_accountDatabase->GetPlayerObjectByPlayerHandle(pCMO->m_memberHandle);
			if (pPO_find) {
				pCMO->m_memberName = pPO_find->m_charName;
				if (pPO_find->m_clanHandle != pCSO->m_guildMasterHandle) {
					delete pCMO;
					pCMO = 0;
					pCSO->m_memberList->RemoveAt(memPosLast);
				}
			}
		}
	}
	return TRUE;
}

ULONG CMultiplayerObj::GetVersion() {
	return ProtocolCompatibility::Instance().getMaxVersion();
}

void CMultiplayerObj::SetContentVersion(ULONG ver) {
	// server side only
	m_contentVersion = ver;
}

ULONG CMultiplayerObj::GetContentVersion(const char* dir) {
	std::string path = GetVersionInfoFullPath(dir);

	char version[100];
	GetPrivateProfileStringA("CURRENT", "VERSION", "", version, 99, path.c_str());

	ULONG ver = 0;
	int ints[4];
	int count = sscanf_s(version, "%d.%d.%d.%d", &ints[0], &ints[1], &ints[2], &ints[3]);
	int i, j;
	for (i = 4 - count, j = count - 1; i < 4; i++, j--) {
		ver = ver + (ints[j] << (8 * i));
	}

	return ver;
}

std::string CMultiplayerObj::GetVersionInfoFullPath(const char* dir) {
	// NOTE!! GetPrivateProfileString DOES NOT USE current directory
	// if the path is not supplied it looks in the Windows directory
	// so use m_baseDir if nothing passed in
	std::string path;
	if (dir == nullptr)
		path = m_pathBase;
	else
		path = dir;
	PathAddInPlace(path, VERSIONINFO_DAT);
	return path;
}

void CMultiplayerObj::WriteDebugFile(CStringA filename, CStringA debugString) {
	CFileException exc;
	CFile fl;
	CArchive the_out_Archive(&fl, CArchive::store);
	BOOL res = OpenCFile(fl, filename, CFile::modeCreate | CFile::modeWrite, &exc);
	if (res) {
		BEGIN_SERIALIZE_TRY
		WriteString(the_out_Archive, debugString);
		WriteString(the_out_Archive, "\n");

		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY_NO_LOGGER
	} else
		LOG_EXCEPTION_NO_LOGGER(exc)
}

bool CMultiplayerObj::GetIPDataFromDisk() {
	if (!getIPAddress().empty())
		return true; // already have one

	std::ifstream f("CONDAT.VER");
	if (f.is_open()) {
		char s[255];
		ZeroMemory(s, 255);
		f.getline(s, 255);

		// trim it
		CStringA ss(s);
		ss.Trim();
		int start = 0;
		CStringA ip = ss.Tokenize(":", start);
		CStringA port = ss.Tokenize(":", start);
		if (ip != "") {
			setIPAddress((const char*)ip);
			if (port != "") {
				m_port = atol(port);
			}
			return true;
		}
	}

	return false;
}

void CMultiplayerObj::BanIPByUsername(const std::string& username) {
	auto pAO = m_accountDatabase->GetAccountObjectByUsername(username);
	if (!pAO)
		return;
	m_ipBlockList.add(pAO->m_currentIP);
	if (pAO->m_loggedOn) {
		LogError("BANNED USER - '" << username << "' netId=" << pAO->m_netId << " Disconnecting...");
		DisconnectClient(pAO->m_netId, DISC_BANNED);
	}
}

void CMultiplayerObj::UnBanIPByUsername(const std::string& username) {
	auto pAO = m_accountDatabase->GetAccountObjectByUsername(username);
	if (!pAO)
		return;
	m_ipBlockList.remove(pAO->m_currentIP);
}

void CMultiplayerObj::SerializeToXML(const std::string& fileName) {
	KEPConfig cfg;
	std::string rootXML = std::string("<") + NETWORK_DEFINITIONS_TAG + "/>";
	if (!cfg.Parse(rootXML.c_str(), NETWORK_DEFINITIONS_TAG)) {
		LogError("Error in CMultiplayerClass::SerializeToXML serializing " << fileName);
		return;
	}
	cfg.SetValue(MAX_PLAYERS_TAG, m_maxPlayers);
	cfg.SetValue(ENABLE_AUTO_BACKUP_TAG, m_enableAutoBackup);
	cfg.SetValue(ALWAYS_START_IN_HOUSING_TAG, m_alwaysStartInHousingZone);
	cfg.SetValue(MAX_PLAYER_BACKUP_TIME_TAG, m_maxPlayerBackupTimeMs);
	cfg.SetValue(MAX_PLAYERS_ON_ACCT, m_clientCharacterMaxCount);
	cfg.SetValue(INITIAL_PLAYERS_CASH, m_initialCash);
	cfg.SetValue(INITIAL_PLAYERS_BANK_CASH, m_initialBankCash);
#ifndef REFACTOR_INSTANCEID
	cfg.SetValue(INITIAL_ZONE_INDEX_TAG, m_tutorialZoneIndex);
#else
	cfg.SetValue(INITIAL_ZONE_INDEX_TAG, m_tutorialZoneIndex.toLong());
#endif
	cfg.SetValue(INITIAL_ZONE_INSTANCE_TAG, m_tutorialZoneInstanceId);
	cfg.SetValue(INITIAL_ZONE_SPAWN_INDEX_TAG, m_tutorialZoneSpawnIndex);
	cfg.SetValue(DISABLED_EDBS_TAG, m_disabledEdbs);
	cfg.SetValue(DEFAULT_PLACE_URL_TAG, m_defaultPlaceUrl.c_str());
#ifndef REFACTOR_INSTANCEID
	cfg.SetValue(INITIAL_ARENA_ZONE_INDEX_TAG, m_initialArenaZoneIndex);
#else
	cfg.SetValue(INITIAL_ARENA_ZONE_INDEX_TAG, m_initialArenaZoneIndex.toLong());
#endif

	cfg.SaveAs(fileName.c_str());
}

void CMultiplayerObj::SerializeFromXML(const std::string& fileName) {
	bool xmlExists = FileHelper::Exists(fileName);
	if (xmlExists && this) {
		KEPConfig cfg;

		cfg.Load(fileName.c_str(), NETWORK_DEFINITIONS_TAG);

		//		m_passwordRequired = cfg.Get(PASSWORD_REQUIRED_TAG, m_passwordRequired);
		m_enableAutoBackup = cfg.Get(ENABLE_AUTO_BACKUP_TAG, m_enableAutoBackup);
		//		m_maxBandwidthPerPerson = cfg.Get(LEVEL_MAX_BANDWIDTH_TAG, (int) m_maxBandwidthPerPerson);
		m_alwaysStartInHousingZone = cfg.Get(ALWAYS_START_IN_HOUSING_TAG, m_alwaysStartInHousingZone);
		m_maxPlayerBackupTimeMs = cfg.Get(MAX_PLAYER_BACKUP_TIME_TAG, m_maxPlayerBackupTimeMs);
		m_clientCharacterMaxCount = cfg.Get(MAX_PLAYERS_ON_ACCT, m_clientCharacterMaxCount);
		m_initialCash = cfg.Get(INITIAL_PLAYERS_CASH, m_initialCash);
		m_initialBankCash = cfg.Get(INITIAL_PLAYERS_BANK_CASH, m_initialBankCash);
#ifndef REFACTOR_INSTANCEID
		m_tutorialZoneIndex = cfg.Get(INITIAL_ZONE_INDEX_TAG, m_tutorialZoneIndex);
#else
		m_tutorialZoneIndex.fromLong(cfg.Get(INITIAL_ZONE_INDEX_TAG, m_tutorialZoneIndex.toLong()));
#endif
		m_tutorialZoneInstanceId = cfg.Get(INITIAL_ZONE_INSTANCE_TAG, m_tutorialZoneInstanceId);
		m_tutorialZoneSpawnIndex = cfg.Get(INITIAL_ZONE_SPAWN_INDEX_TAG, m_tutorialZoneSpawnIndex);
		m_requestProcessingErrorTimeMS = cfg.Get(REQUEST_PROCESSING_ERROR_TIME_MS_TAG, m_requestProcessingErrorTimeMS);
		m_requestProcessingInfoTimeMS = cfg.Get(REQUEST_PROCESSING_INFO_TIME_MS_TAG, m_requestProcessingInfoTimeMS);
		m_defaultPlaceUrl = cfg.Get(DEFAULT_PLACE_URL_TAG, m_defaultPlaceUrl.c_str());
#ifndef REFACTOR_INSTANCEID
		m_initialArenaZoneIndex = cfg.Get(INITIAL_ARENA_ZONE_INDEX_TAG, m_initialArenaZoneIndex);
#else
		m_initialArenaZoneIndex = cfg.Get(INITIAL_ARENA_ZONE_INDEX_TAG, m_initialArenaZoneIndex.toLong());
#endif
		m_maxItemLimit_onChar = cfg.Get(MAX_PLAYER_INVENTORY_TAG, 100000); // very large, but can still control, was 40
		m_maxItemLimit_inBank = cfg.Get(MAX_BANK_INVENTORY_TAG, 1000000); // very large, but can still control, was 250
		CInventoryObj::MAX_STACK_LIMIT = std::max(1, cfg.Get(MAX_STACK_LIMIT_TAG, 30000));
		m_disabledEdbs = cfg.Get(DISABLED_EDBS_TAG, "").c_str(); // default WAS WOK specific
		m_tryOnItemDefaultExpirationDuration = cfg.Get(TRYON_DEFAULT_EXPIRATION_DURATION, 5);
		m_maxAllowedPlayers = m_maxPlayers; // until license sets it
		m_pingTimeoutSec = std::max(5, cfg.Get(PING_TIMEOUT_SEC_TAG, 10));
	}
}

CSpawnCfgObj* CMultiplayerObj::GetSpawnPointCfg(int _zoneIndex, int spawnIdx) {
	// we'll look for all three types of zone indexes, returning the best match
	ZoneIndex zoneIndex(_zoneIndex);
	ZoneIndex uninstancedZoneIndex(zoneIndex);
	uninstancedZoneIndex.clearInstance();

	ZoneIndex justZoneIndex(uninstancedZoneIndex.zoneIndex());
	CSpawnCfgObj* uninstancedMatch = 0;
	CSpawnCfgObj* justZoneIndexMatch = 0;

	int instancedIdx = 0;
	int uninstancedIdx = 0;
	int justZoneIdx = 0;

	for (POSITION posLoc = m_spawnPointCfgs->GetHeadPosition(); posLoc != NULL;) {
		CSpawnCfgObj* spawnPtr = (CSpawnCfgObj*)m_spawnPointCfgs->GetNext(posLoc);
		if (spawnPtr->m_zoneIndex == zoneIndex) {
			if (instancedIdx == spawnIdx) {
				return spawnPtr; // this is best match, so return
			} else {
				instancedIdx++;
			}
		} else if (spawnPtr->m_zoneIndex == uninstancedZoneIndex &&
				   uninstancedMatch == 0) {
			if (uninstancedIdx == spawnIdx) {
				uninstancedMatch = spawnPtr; // keep looking since may find better match
			} else {
				uninstancedIdx++;
			}
		}
#ifndef REFACTOR_INSTANCEID
		else if (spawnPtr->m_zoneIndex.zoneIndex() == justZoneIndex &&
#else
		else if (spawnPtr->m_zoneIndex.zoneIndex() == justZoneIndex.toLong() &&
#endif
				 justZoneIndexMatch == 0) {
			if (justZoneIdx == spawnIdx) {
				justZoneIndexMatch = spawnPtr; // keep looking since may find better match
			} else {
				justZoneIdx++;
			}
		}
	}

	// return uninstanced, by typed match over just zone index match
	return (uninstancedMatch != 0) ? uninstancedMatch : justZoneIndexMatch;
}

// find a player by name across all accounts
CPlayerObject* CMultiplayerObj::findPlayerByName(const char* name) {
	for (POSITION pos = m_accountDatabase->GetHeadPosition(); pos != NULL;) {
		CAccountObject* acct = (CAccountObject*)m_accountDatabase->GetNext(pos);
		for (POSITION pos2 = acct->m_pPlayerObjectList->GetHeadPosition(); pos2 != NULL;) {
			CPlayerObject* player = (CPlayerObject*)acct->m_pPlayerObjectList->GetNext(pos2);
			if (player->m_charName.CompareNoCase(name) == 0)
				return player;
		}
	}
	return 0;
}

void CMultiplayerObj::FillInLogonContext(CONTEXT_VALIDATION_EX& msg, bool legacy, bool hashPassword) {
	// hash the password
	CStringA userName(m_userName);
	userName.MakeLower();
	lstrcpynA(msg.username, m_userName, sizeof(msg.username));

	if (hashPassword) {
		MD5_DIGEST_STR pw;
		hashString(m_passWord + userName, pw);
		lstrcpynA(msg.password, pw.s, sizeof(msg.password));

		for (int i = 0; i < m_passWord.GetLength(); i++)
			m_passWord.SetAt(i, ' '); // clear memory
		m_passWord = pw.s; // set to hash since used in SendLogFast later
	} else {
		lstrcpynA(msg.password, (const char*)m_passWord, sizeof(msg.password));
	}

	msg.protocolVersionInfo = m_protocolVersion;
	msg.contentVersionInfo = m_contentVersion;
	msg.minProtocolVersion = ProtocolCompatibility::Instance().getMinVersion();

	// Sanity check
	jsAssert(msg.protocolVersionInfo != INVALID_PROTOCOL_VERSION);
	jsAssert(msg.minProtocolVersion != INVALID_PROTOCOL_VERSION);
}

CMultiplayerObj& CMultiplayerObj::setIPAddress(const std::string& ipAddress) {
	m_IPaddress = ipAddress;
	return *this;
}

const std::string& CMultiplayerObj::getIPAddress() const {
	return m_IPaddress;
}

void CMultiplayerObj::SendPendingSpawnEventToClient(const SpawnInfo& spawnInfo, NETID netIdTo, const ZoneIndex& zoneIndex, bool initialSpawn) {
	ZoneIndex zi(zoneIndex);
	if (zi.zoneIndex() > MAX_KANEVA_SUPPORTED_WORLD) {
		assert(false); // Deprecated dev world feature
		// find the one kaneva know about so client can progressively download it
		// first find this one
		POSITION pos = m_currentWorldsUsed->GetHeadPosition();
		bool found = false;
		while (pos != 0 && !found) {
			CWorldAvailableObj* wa = (CWorldAvailableObj*)m_currentWorldsUsed->GetNext(pos);
			if (wa->m_index == zi.zoneIndex()) {
				// second, find the match based on exg name
				POSITION pos2 = m_currentWorldsUsed->GetHeadPosition();
				while (pos2 != 0) {
					CWorldAvailableObj* kwa = (CWorldAvailableObj*)m_currentWorldsUsed->GetNext(pos2);
					if (kwa->m_worldName == wa->m_worldName && kwa->m_index <= MAX_KANEVA_SUPPORTED_WORLD) {
						zi = ZoneIndex(kwa->m_index);
						found = true;
						break;
					}
				}
			}
		}
		if (!found) {
			LogError("Couldn't find kaneva zone with index of " << zi.zoneIndex());
		}
	}

	LogInfo("NETID[" << netIdTo << "]: " << zi.ToStr() << ", spawnId=" << spawnInfo.getSpawnId() << ", initialSpawn=" << LogBool(initialSpawn));

#ifndef REFACTOR_INSTANCEID
	m_dispatcher->QueueEvent(new PendingSpawnEvent(zi, spawnInfo.getSpawnId(), netIdTo, initialSpawn, spawnInfo.getCoverCharge(), zi.GetInstanceId()));
#else
	m_dispatcher->QueueEvent(new PendingSpawnEvent(zi.toLong(), spawnInfo.getSpawnId(), netIdTo, initialSpawn, spawnInfo.getCoverCharge(), zi.GetInstanceId()));
#endif
}

void CMultiplayerObj::SendPlayerArrivedEvent(NETID netID, ZoneIndex zoneIndex, NETID prevNetID, const ZoneIndex& prevZoneIndex, unsigned playerArriveId, const ScriptPlayerInfo& playerInfo, unsigned parentZoneInstanceId, CAccountObject* pAccount) {
#ifndef REFACTOR_INSTANCEID
	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerArrived, 0, zoneIndex, zoneIndex.GetInstanceId(), netID);
	(*sse->OutBuffer()) << playerArriveId << prevNetID << prevZoneIndex << prevZoneIndex.GetInstanceId() << playerInfo << parentZoneInstanceId;
#else
	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerArrived, 0, zoneIndex.toLong(), zoneIndex.GetInstanceId(), netID);
	(*sse->OutBuffer()) << playerArriveId << prevNetID << prevZoneIndex.toLong() << prevZoneIndex.GetInstanceId() << playerInfo << parentZoneInstanceId;
#endif
	sse->SetFrom(netID);
	m_dispatcher->ProcessSynchronousEvent(sse);

	// PlayerArrived is sent. Record relevant information for the future PlayerDeparted event
	pAccount->SetPendingPlayerDepartInfo(netID, zoneIndex, playerArriveId);
}

void CMultiplayerObj::SendPlayerDepartedEvent(NETID netID, const ZoneIndex& departZoneIndex, unsigned playerArriveId) {
	jsAssert(departZoneIndex.toLong() != 0 && departZoneIndex != INVALID_ZONE_INDEX);

#ifndef REFACTOR_INSTANCEID
	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerDeparted, 0, departZoneIndex, departZoneIndex.GetInstanceId(), netID);
#else
	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerDeparted, 0, departZoneIndex.toLong(), departZoneIndex.GetInstanceId(), netID);
#endif
	(*sse->OutBuffer()) << playerArriveId;

	sse->SetObjectId((OBJECT_ID)sse->From());
	m_dispatcher->ProcessSynchronousEvent(sse);
}

IMPLEMENT_SERIAL_SCHEMA(CMultiplayerObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CMultiplayerObjList, CObList)

#define GETSET_MAXPLAYERS INT_MAX

// turn on if you want to see all the values, not relevant ones
#ifdef SHOW_ALL_GETSET
#define SHOW_ALL GS_FLAG_EXPOSE
#else
#define SHOW_ALL GS_FLAG_READ_ONLY
#endif

BEGIN_GETSET_IMPL(CMultiplayerObj, IDS_MULTIPLAYEROBJ_NAME)
GETSET(m_restrictedMode, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MULTIPLAYEROBJ, SERVERMODE)
GETSET_RANGE(m_port, gsDword, GS_FLAG_READ_ONLY, 0, 0, MULTIPLAYEROBJ, PORT, DWORD_MIN, DWORD_MAX)
GETSET_OBLIST_PTR(m_accountDatabase, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, MULTIPLAYEROBJ, ACCOUNTDATABASE)
GETSET_OBLIST_PTR(m_currentWorldsUsed, SHOW_ALL, 0, 0, MULTIPLAYEROBJ, CURRENTWORLDSUSED)
GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, MULTIPLAYEROBJ, DISCONNECTALL)
GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, MULTIPLAYEROBJ, IMPORT_ACCT)
GETSET2_RANGE(m_requestProcessingInfoTimeMS, gsLong, GS_FLAGS_DEFAULT, 0, 0, IDS_MULTIPLAYEROBJ_REQUESTPROCESSINGINFOTIMEMS, IDS_MULTIPLAYEROBJ_REQUESTPROCESSINGWARNINGTIMEMS, 0, LONG_MAX)
GETSET2(m_defaultPlaceUrl, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_DEFAULT_PLACE_URL, IDS_DEFAULT_PLACE_URL_HELP)
GETSET2(m_initialArenaZoneIndex, gsLong, GS_FLAGS_DEFAULT, 0, 0, IDS_INITIAL_ARENA_ZONE_INDEX, IDS_INITIAL_ARENA_ZONE_INDEX)
GETSET2(m_alwaysStartInHousingZone, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_ALWAYS_START_IN_HOUSING, IDS_ALWAYS_START_IN_HOUSING_HELP)
GETSET2(m_disabledEdbs, gsCString, GS_FLAGS_DEFAULT, 0, 0, IDS_DISABLED_EDBS, IDS_DISABLED_EDBS_HELP)
GETSET(m_enableAutoBackup, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MULTIPLAYEROBJ, ENABLEAUTOBACKUP)
GETSET_RANGE(m_maxAllowedPlayers, gsInt, SHOW_ALL, 0, 0, MULTIPLAYEROBJ, MAXALLOWEDPLAYERS, 1, INT_MAX)
GETSET_RANGE(m_currentPlayersOnServer, gsInt, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, STATUS_GROUP, MULTIPLAYEROBJ, CURRENTPLAYERSONSERVER, INT_MIN, INT_MAX)
GETSET_RANGE(m_currentCharacterInUse, gsInt, SHOW_ALL, 0, 0, MULTIPLAYEROBJ, CURRENTCHARACTERINUSE, -1, INT_MAX)
GETSET_OBLIST_PTR(m_spawnPointCfgs, SHOW_ALL, 0, 0, MULTIPLAYEROBJ, SPAWNPOINTCFGS)
END_GETSET_IMPL

} // namespace KEP