///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include "ClientStrings.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "ServerSerialize.h"

#include <SerializeHelper.h>

#include "CMovementObj.h"
#include "ProgressStatus.h"
#include "IAssetsDatabase.h"
#include "IClientEngine.h"
#include "ContentService.h"
#include "MaterialPersistence.h"
#include "CBoneAnimationNaming.h"
#include "imodel.h"
#include "ActorDatabase.h"

#include "..\..\RenderEngine\ReSkinMesh.h"
#include "ReMeshCreate.h"
#include "EquippableSystem.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "KEPPhysics/RigidBodyStatic.h"
#include "KEPPhysics/CollisionShape.h"
#include "KEPPhysics/Vehicle.h"

#include "CTextRenderClass.h"
#include "CSkillClass.h"
#include "CItemClass.h"
#include "CCfgClass.h"
#include "CSkeletalLODClass.h"
#include "CSkeletonObject.h"
#include "SkeletonConfiguration.h"
#include "CArmedInventoryClass.h"
#include "CFrameObj.h"
#include "CPhysicsEmuClass.h"
#include "CResourceClass.h"
#include "AIclass.h"
#include "CBoneClass.h"
#include "CDeformableMesh.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "RuntimeSkeleton.h"
#include "SkeletonLabels.h"
#include "UgcConstants.h"
#include "usetypes.h"
#include "LookAt.h"
#include "CPhysicsEmuClass.h"
#include "PhysicsUserData.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/Units.h"
#include "KEPPhysics/Avatar.h"
#include "common\include\LookAt.h"
#include "common\include\IMemSizeGadget.h"

#include "Core/Util/CallbackSystem.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

bool CMovementObj::MyPassGroup(const PASS_GROUP& passGroup) {
	auto pICE = IClientEngine::Instance();
	auto pMO_me = dynamic_cast<CMovementObj*>(pICE ? pICE->GetMovementObjectMeAsGetSet() : nullptr);
	if (!pMO_me)
		return false;
	return pMO_me->hasPass(passGroup);
}

bool CMovementObj::IsItemAP(const GLID& glid) {
#ifdef _ClientOutput
	ContentMetadata md(glid, true);
	return (md.IsValid() && md.PassGroupAP());
#else
	return false;
#endif
}

// This occurs for non-players (doors, coins, cars, whatever)
CMovementObj::CMovementObj() :
		m_state(STATE_UNCONFIGURED),
		m_effectTrack(ObjectType::MOVEMENT) {
	init();
}

// This occurs for players (avatars & npcs)
CMovementObj::CMovementObj(const std::string& name, int traceNumber, int networkDefinedID) :
		m_state(STATE_UNCONFIGURED),
		m_effectTrack(ObjectType::MOVEMENT) {
	init();
	m_name = name;
	m_traceNumber = traceNumber;
	m_networkDefinedID = networkDefinedID;
	LogInfo(ToStr());
}

CMovementObj::~CMovementObj() {
	CallbackSystem::Instance()->UnregisterAllCallbacksWithOwner(this);
	LogInfo(ToStr());
}

void CMovementObj::init() {
	resetOverrideAnimType();

	m_touchTriggered = false;

	const Vector3f zeroVec(0.0f, 0.0f, 0.0f);
	const Vector3f zeroUpVec(0.0f, 1.0f, 0.0f);
	m_posForceExt = zeroVec;
	m_recentSurfaceInfo.contactLocation = zeroVec;
	m_recentSurfaceInfo.faceNormal = zeroUpVec;

	m_skills = NULL;
	m_autoCfg = NULL;
	m_currentlyArmedItems = NULL;
	m_currentLOD = -1;
	m_skeletonLODList = NULL;
	m_recentSurfaceInfo.objectType = ObjectType::NONE;
	m_recentSurfaceInfo.objectId = 0;
	m_pSkeleton = NULL;
	m_skeletonConfiguration = NULL;
	m_baseFrame = NULL;
	m_ridable = -1;
	m_controlType = eActorControlType::OPC;
	m_movementDB = NULL;
	m_name = "";
	m_displayName = "";
	m_clanName = "";
	m_title = "";
	m_traceNumber = 0;
	m_glid = GLID_INVALID;
	m_lastPosition = zeroVec;
	m_boundingRadius = 0.0f;
	m_unscaledBoundingRadius = 0.0f;
	m_idxRefModel = 0;
	m_controlConfig = 0;

	m_networkEntity = false;

	m_modelType = 0;
	m_firstPersonModel = false;

	m_possedBy_traceNumber = -1;
	m_networkDefinedID = -1;

	m_firstPersonMode = false;
	m_inventory = NULL;
	m_ridable = -1;
	m_pMO_posses = NULL;
	m_pMO_possedBy = NULL;
	m_mountType = MT_NONE;
	m_deathCfg = NULL;
	m_stats = NULL;

	m_validThisRound = TRUE;
	m_filterCollision = FALSE;
	m_distanceToCamTemp = 0.0f;
	m_iModel = NULL;

	m_currentCameraDataIndex = 0;

	m_isInFov = false;
	for (int tempi = 0; tempi < 4; tempi++) {
		m_actorGroups[tempi] = 0; // default is 0 None
	}

	m_nameColor.r = 1;
	m_nameColor.g = 1;
	m_nameColor.b = 1;
	m_nameColor.a = 1;

	m_outline = false;
	m_outlineColor = D3DXCOLOR(1, 1, 1, 1);
	m_outlineZSorted = false;

	m_visible = true;

	m_ugcActor = NULL;

	m_substituteObjType = ObjectType::NONE;
	m_substituteObjId = 0;
	m_substituteAllowed = true;

	m_lookAtInfo = NULL;
	m_render = true;

	m_passGroups = PASS_GROUP_INVALID;

	// Arm/Disarm Is disabled for avatars entering worlds upon construction in
	// ClientBaseEngine::ActivateMovementEntity() and enabled again in ClientBaseEngine::HandleMovementObjectCfgEvent().
	// The reason is that sometimes arming events come in between the two which do not get armed properly
	// but remain in the armed list preventing default clothing to be put on to cover nakedness.
	m_allowArmDisarm = true;
}

void CMovementObj::SafeDelete() {
	if (m_skills) {
		delete m_skills;
		m_skills = nullptr;
	}

	if (m_deathCfg) {
		m_deathCfg->SafeDelete();
		delete m_deathCfg;
		m_deathCfg = nullptr;
	}

	if (m_autoCfg) {
		m_autoCfg->SafeDelete();
		delete m_autoCfg;
		m_autoCfg = nullptr;
	}

	if (m_currentlyArmedItems) {
		m_currentlyArmedItems->SafeDelete();
		delete m_currentlyArmedItems;
		m_currentlyArmedItems = nullptr;
	}

	if (m_skeletonLODList) {
		m_skeletonLODList->SafeDelete();
		delete m_skeletonLODList;
		m_skeletonLODList = nullptr;
	}

	if (m_baseFrame) {
		m_baseFrame->SafeDelete();
		delete m_baseFrame;
		m_baseFrame = nullptr;
	}

	if (m_movementDB) {
		delete m_movementDB;
		m_movementDB = nullptr;
	}

	m_pSkeleton.reset();

	delete m_skeletonConfiguration;

	if (m_inventory) {
		m_inventory->SafeDelete();
		delete m_inventory;
		m_inventory = nullptr;
	}

	//posses stuff
	if (m_pMO_posses) {
		m_pMO_posses->m_pMO_possedBy = nullptr;
		m_pMO_posses = nullptr;
	}
	if (m_pMO_possedBy) {
		m_pMO_possedBy->deMount();
	}

	if (m_stats) {
		delete m_stats;
		m_stats = nullptr;
	}

	if (m_iModel) {
		m_iModel->Invalidate();
	}

	m_cameraData.clear();
	return;
}

UINT CMovementObj::queryLOD(FLOAT distance, DWORD& numLOD) {
	if (!m_skeletonLODList) {
		numLOD = 0;
		m_currentLOD = 0;
		return 0;
	}
	m_currentLOD = m_skeletonLODList->GetLODIndex(distance);
	m_pSkeleton->setCurrentLOD(m_currentLOD);
	numLOD = m_skeletonLODList->GetNumLOD();
	return m_currentLOD;
}

void CMovementObj::initPhysicsObjects() {
#ifdef _ClientOutput
	Physics::AvatarProperties avatarProps;
	avatarProps.m_CollidesWithMask = -1;
	avatarProps.m_CollisionGroupMask = -1;
	//Vector3f boxSize = m_pSkeleton->getWorldBoxHalfSize() * 2;
	float fHeightFeet, fRadiusFeet;
	m_pSkeleton->getAvatarCapsuleDimensions(out(fHeightFeet), out(fRadiusFeet));
	float fHeightMeters = FeetToMeters(fHeightFeet);
	float fRadiusMeters = FeetToMeters(fRadiusFeet);
	avatarProps.m_fHeightMeters = fHeightMeters;
	avatarProps.m_fWidthMeters = 2 * fRadiusMeters;
	avatarProps.m_fMassKG = 13 * avatarProps.m_fHeightMeters * avatarProps.m_fHeightMeters * avatarProps.m_fHeightMeters;
	m_pPhysicsAvatar = Physics::CreateAvatar(avatarProps);
	if (m_pPhysicsAvatar)
		m_pPhysicsAvatar->SetCollisionMasks(CollisionGroup::Avatar, CollisionMask::Avatar);

	Physics::CollisionShapeCapsuleProperties visibleShapeProps;
	visibleShapeProps.m_fCylinderHeightMeters = fHeightMeters - 2 * fRadiusMeters;
	visibleShapeProps.m_fRadiusMeters = FeetToMeters(fRadiusFeet);
	visibleShapeProps.m_iAxis = 1;
	std::shared_ptr<Physics::CollisionShape> pVisibleShape = Physics::CreateCollisionShape(visibleShapeProps);
	if (pVisibleShape) {
		Physics::RigidBodyStaticProperties visibleBodyProps;
		m_pVisibleBody = Physics::CreateRigidBodyStatic(visibleBodyProps, pVisibleShape.get(), Vector3f(0, 0.5f * fHeightMeters, 0), 1.0f);
		if (m_pVisibleBody) {
			m_pVisibleBody->SetCollisionMasks(CollisionGroup::Visible, static_cast<unsigned int>(0));
			std::shared_ptr<PhysicsBodyData> pPhysicsBodyData = std::make_shared<PhysicsBodyData>(ObjectType::MOVEMENT, getNetTraceId(), this);
#if defined(_DEBUG)
			pPhysicsBodyData->m_glid = m_glid;
			pPhysicsBodyData->m_pszName = m_name.c_str();
#endif
			m_pVisibleBody->SetUserData(pPhysicsBodyData);
		}
	}
#endif
}

BOOL CMovementObj::Clone(
	CMovementObj** ppMO_out,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int* pSpawnCfgOpt,
	int* pMaterialCfgOpt) {
	return Clone(ppMO_out, g_pd3dDevice, pSpawnCfgOpt, pMaterialCfgOpt, NULL, true);
}

BOOL CMovementObj::Clone(
	CMovementObj** ppMO_out,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int* pSpawnCfgOpt) {
	return Clone(ppMO_out, g_pd3dDevice, pSpawnCfgOpt, NULL, NULL, true);
}

BOOL CMovementObj::Clone(
	CMovementObj** ppMO_out,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int* pSpawnCfgOpt,
	int* pMaterialCfgOpt,
	D3DCOLORVALUE* pSpawnColorOpt) {
	return Clone(ppMO_out, g_pd3dDevice, pSpawnCfgOpt, pMaterialCfgOpt, pSpawnColorOpt, true);
}

BOOL CMovementObj::Clone(
	CMovementObj** ppMO_out,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int* pSpawnCfgOpt,
	int* pMaterialCfgOpt,
	D3DCOLORVALUE* pSpawnColorOpt,
	int traceNumber,
	int networkDefinedID) {
	return Clone(ppMO_out, g_pd3dDevice, pSpawnCfgOpt, pMaterialCfgOpt, pSpawnColorOpt, true, traceNumber, networkDefinedID);
}

BOOL CMovementObj::Clone(
	CMovementObj** ppMO_out,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int* pSpawnCfgOpt,
	int* pMaterialCfgOpt,
	D3DCOLORVALUE* pSpawnColorOpt,
	bool fullClone,
	int traceNumber,
	int networkDefinedID) {
	// Clone Trace & Net Ids ?
	if (traceNumber == -1)
		traceNumber = m_traceNumber;
	if (networkDefinedID == -1)
		networkDefinedID = m_networkDefinedID;

	// Make New Movement Object
	CMovementObj* pMO = new CMovementObj(m_name, traceNumber, networkDefinedID);

	pMO->m_state = m_state;
	pMO->m_allowArmDisarm = m_allowArmDisarm;
	pMO->m_passGroups = m_passGroups;

	if (m_autoCfg)
		m_autoCfg->Clone(&pMO->m_autoCfg);

	pMO->m_currentlyArmedItems = new CItemObjList();

	if (m_pSkeleton && m_skeletonConfiguration) {
		m_pSkeleton->Clone(&pMO->m_pSkeleton, g_pd3dDevice, pSpawnCfgOpt, pMaterialCfgOpt, pSpawnColorOpt, fullClone, m_skeletonConfiguration->m_meshSlots == NULL);
		m_skeletonConfiguration->Clone(&pMO->m_skeletonConfiguration, m_pSkeleton.get(), pSpawnCfgOpt, pMaterialCfgOpt, pSpawnColorOpt, fullClone);
	}

	if (m_skeletonLODList)
		m_skeletonLODList->Clone(&pMO->m_skeletonLODList);

	if (m_movementDB)
		m_movementDB->Clone(&pMO->m_movementDB);

	pMO->m_ridable = m_ridable;
	pMO->m_controlType = m_controlType;
	pMO->m_name = m_name;
	pMO->m_clanName = m_clanName;
	pMO->m_title = m_title;
	pMO->m_lastPosition = m_lastPosition;
	pMO->m_boundingRadius = m_boundingRadius;
	pMO->m_unscaledBoundingRadius = m_unscaledBoundingRadius;
	pMO->m_idxRefModel = m_idxRefModel;
	pMO->m_controlConfig = m_controlConfig;

	pMO->m_networkEntity = m_networkEntity;

	pMO->m_modelType = m_modelType;
	pMO->m_firstPersonModel = m_firstPersonModel;

	pMO->m_movementInterpolator = m_movementInterpolator;

	pMO->m_possedBy_traceNumber = m_possedBy_traceNumber;
	pMO->m_ridable = m_ridable;
	pMO->m_filterCollision = m_filterCollision;
	pMO->m_isInFov = m_isInFov;
	pMO->m_iModel = m_iModel;
	pMO->m_substituteObjType = m_substituteObjType;
	pMO->m_substituteObjId = m_substituteObjId;
	pMO->m_substituteAllowed = m_substituteAllowed;

	for (int tempi = 0; tempi < 4; tempi++)
		pMO->m_actorGroups[tempi] = m_actorGroups[tempi];

	// copy camera data
	for (const auto& cameraData : m_cameraData)
		pMO->m_cameraData.push_back(cameraData);

	pMO->m_currentCameraDataIndex = m_currentCameraDataIndex;

	if (pMO->m_pSkeleton) {
		const Vector3d color(pMO->m_nameColor.r, pMO->m_nameColor.g, pMO->m_nameColor.b);
		pMO->m_pSkeleton->getOverheadLabels()->addLabel(pMO->m_name, .014f, color, SkeletonLabels::SL_POS_NAME);
	}

	pMO->m_glid = m_glid;
	pMO->m_ugcActor = m_ugcActor;

	pMO->initPhysicsObjects();

	*ppMO_out = pMO;

	return TRUE;
}

void CMovementObj::characterAnimationManagement(RuntimeSkeleton* pRS, bool bPlayerSkeleton, TimeMs animBlendDurationMs) {
	if (!pRS)
		return;

	auto pMC = m_movementDB;
	if (!pMC)
		return;

	eAnimType animType = eAnimType::None;
	int animVer = pRS->getAnimVerPreference();

	auto posForce = pMC->GetPosForce();

	// Stand or Walk/Run
	bool animDirForward = (posForce.z >= 0);
	if (posForce.z == 0) {
		animType = eAnimType::Stand;
	} else {
		resetOverrideAnimType();
		animType = pMC->IsWalking() ? eAnimType::Walk : eAnimType::Run;
	}

	// Side Step
	float posForceX = posForce.x;
	if (posForce.z < 0)
		posForceX = -posForceX;
	if (posForceX > 0.0f) {
		animType = pMC->IsWalking() ? eAnimType::Walk : eAnimType::Run;
		resetOverrideAnimType();
	} else if (posForceX < 0.0f) {
		animType = pMC->IsWalking() ? eAnimType::Walk : eAnimType::Run;
		resetOverrideAnimType();
	}

	// Jump
	if (pMC->IsJumping()) {
		animType = eAnimType::Jump;
		animDirForward = true;
		resetOverrideAnimType();
	}

	// Clear Secondary Animation
	pRS->setSecondaryAnimation(GLID_INVALID);

	// Reset Override Animation On Timer Expiry
	TimeMs timeMs = fTime::TimeMs();
	if (IS_VALID_ANIM_TYPE(m_overrideAnimType) && m_overrideDurationMs && ((timeMs - m_overrideTimeMs) > m_overrideDurationMs))
		resetOverrideAnimType();

	// Animation override in effect, cancel secondary animation
	if (IS_VALID_ANIM_TYPE(m_overrideAnimType)) {
		pRS->setSecondaryAnimation(GLID_INVALID);
		animType = m_overrideAnimType;
		animDirForward = true;
	}

	if ((animType == eAnimType::None) && m_overrideAnimSettings.isValid() && bPlayerSkeleton) {
		pRS->setPrimaryAnimationSettings(m_overrideAnimSettings);
	} else {
		if (IS_UGC_GLID(m_glid) && m_ugcActor) {
			GLID glid;
			switch (animType) {
				case eAnimType::Walk:
					glid = m_ugcActor->m_walkGlid;
					break;
				case eAnimType::Run:
					glid = m_ugcActor->m_runGlid;
					break;
				case eAnimType::Jump:
					glid = m_ugcActor->m_jumpGlid;
					break;
				default:
					glid = m_ugcActor->m_standGlid;
					break;
			}

			if (!IS_VALID_GLID(glid))
				glid = m_ugcActor->m_standGlid;

			AnimSettings animSettings(glid, animDirForward, eAnimLoopCtl::Default, animBlendDurationMs);
			pRS->setPrimaryAnimationSettings(animSettings);
		} else {
			if (!pRS->setPrimaryAnimationByAnimTypeAndVer(animType, animVer, animDirForward, animBlendDurationMs))
				pRS->setPrimaryAnimationByAnimTypeAndVer(animType, ANIM_VER_ANY, animDirForward, animBlendDurationMs);
		}
	}

	if (IS_VALID_ANIM_TYPE(m_overrideAnimType)) {
		if (m_overrideAnimLock) {
			m_overrideDurationMs = 0; // Do not expire if locked
		} else {
			if (m_overrideDurationMs == 0)
				m_overrideDurationMs = pRS->getAnimMinTimeMs();
		}
	}
}

void CMovementObj::Serialize(CArchive& ar) {
	BOOL fillBool = TRUE;
	float fillFloat = 0.0f;
	CStringA fillCstring;
	Vector3f reserverdVector;
	reserverdVector.x = 0.0f;
	reserverdVector.y = 0.0f;
	reserverdVector.z = 0.0f;

	CSkeletonObject* tmpSkeletalObject = new CSkeletonObject(m_pSkeleton, m_skeletonConfiguration);

	CStringA collisionInfoFileDeprecated; // deprecated

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << CStringA(m_name.c_str())
		   << m_ridable
		   << static_cast<int>(m_controlType)
		   << m_movementDB
		   << m_traceNumber
		   << fillBool
		   << m_unscaledBoundingRadius
		   << (float)0 //m_speedBasedAnimation
		   << 0.0f // m_location.x
		   << 0.0f // m_location.y
		   << 0.0f // m_location.z;
		   << m_controlConfig
		   << (int)0 //m_AIconfig
		   << (BOOL)0 //m_exactCollision
		   << (float)0.0 //m_popoutRadius
		   << TRUE //m_poppedOut;
		   << NULL //m_lightObject
		   << collisionInfoFileDeprecated
		   << (int)0 //m_team
		   << m_filterCollision
		   << (int)0 //m_raceID
		   << fillCstring
		   << (int)0 //m_primaryPhysicsOverd
		   << (float)0 //m_hardPointOverdY
		   << fillCstring
		   << fillBool
		   << fillFloat
		   << (int)0 //m_energy
		   << (int)0 //m_maxEnergy
		   << m_modelType
		   << (BOOL)(m_firstPersonModel ? TRUE : FALSE)
		   << (int)-1 //m_fullModelRef
		   << -1 // m_camFirstPerson - no longer used
		   << -1 // m_camThirdPerson - no longer used
		   << (int)-1 // m_firstPersonRef
		   << -1 // m_deathCam
		   << (int)-1 //m_hudLink
		   << tmpSkeletalObject
		   << tmpSkeletalObject->getRuntimeSkeleton()->getArmedInventoryList() // horrible example of the runtime and persistence layers being identical.
		   << m_skeletonLODList
		   << m_stats
		   << nullptr //m_resources
		   << m_inventory
		   << m_autoCfg
		   << m_skills
		   << m_deathCfg;

		//arrays ect
		{
			// m_transformMatrix has been removed from the class.
			// Continue to serialize for compatibility.
			Matrix44f m_transformMatrix;
			m_transformMatrix.MakeIdentity();
			for (int loop = 0; loop < 4; loop++) {
				for (int loop2 = 0; loop2 < 4; loop2++) {
					ar << m_transformMatrix[loop][loop2];
					ProgressUpdater::UpdateProgress("CMovementObj::Serialize");
				}
			}
		}

		// write out camera data
		ar << (int)m_cameraData.size();
		for (const CameraData& cameraData : m_cameraData) {
			ar << cameraData.m_cameraDBIndex << cameraData.m_boneIndex;
		}
		for (int tempi = 0; tempi < 4; tempi++) {
			ar << m_actorGroups[tempi];
		}

	} else {
		int version = ar.GetObjectSchema();
		CArmedInventoryList* iHateCObjectSerialize;
		CResourceObjectList* resources = nullptr;

		BOOL deprecatedBOOL = FALSE;
		float deprecatedFloat = 0.0;
		int deprecated = 0;
		float speedBasedAnimation_Deprecated;
		BOOL exactCollision_Deprecated;
		int primaryPhysicsOverd_Deprecated;
		float hardPointOverdY_Deprecated;
		int fullModelRef_Deprecated;
		int firstPersonRef_Deprecated;
		int hudLink_Deprecated;
		Vector3f location_Deprecated;

		CStringA name;
		int controlType;
		BOOL firstPersonModel;

		ar >> name >> m_ridable >> controlType >> m_movementDB >> m_traceNumber >> fillBool >> m_unscaledBoundingRadius >> speedBasedAnimation_Deprecated >> location_Deprecated.x >> location_Deprecated.y >> location_Deprecated.z >> m_controlConfig >> deprecated //m_AIconfig
			>> exactCollision_Deprecated >> deprecatedFloat //m_popoutRadius
			>> deprecatedBOOL //m_poppedOut
			>> deprecated //m_lightObject
			>> collisionInfoFileDeprecated >> deprecated //m_team
			>> m_filterCollision >> deprecated //m_raceID
			>> fillCstring >> primaryPhysicsOverd_Deprecated >> hardPointOverdY_Deprecated >> fillCstring >> fillBool >> fillFloat >> deprecated //m_energy
			>> deprecated //m_maxEnergy
			>> m_modelType >> firstPersonModel >> fullModelRef_Deprecated >> deprecated //m_camFirstPerson
			>> deprecated //m_camThirdPerson
			>> firstPersonRef_Deprecated >> deprecated //m_deathCam
			>> hudLink_Deprecated >> tmpSkeletalObject >> iHateCObjectSerialize >> m_skeletonLODList >> m_stats >> resources >> m_inventory;

		if (resources != nullptr) {
			resources->SafeDelete();
			delete resources;
		}

		m_name = name.GetString();
		m_firstPersonModel = firstPersonModel == TRUE;
		m_controlType = static_cast<eActorControlType>(controlType);
		m_pSkeleton = tmpSkeletalObject->getRuntimeSkeletonShared();
		tmpSkeletalObject->getRuntimeSkeleton()->setArmedInventoryList(iHateCObjectSerialize);
		m_skeletonConfiguration = tmpSkeletalObject->getSkeletonConfiguration();

		if (version <= 3) {
			// Flush any items on actors that don't exist in the GIL to the GIL.
			CItemObjList* possiblyUnsavedItems;
			ar >> possiblyUnsavedItems;
			if (possiblyUnsavedItems) {
				for (POSITION posLoc = possiblyUnsavedItems->GetHeadPosition(); posLoc != NULL;) {
					CItemObj* itemPtr = (CItemObj*)possiblyUnsavedItems->GetNext(posLoc);
					CGlobalInventoryObj* glbInvOb = (CGlobalInventoryObj*)CGlobalInventoryObjList::GetInstance()->GetByGLID(itemPtr->m_globalID);
					if (!glbInvOb) {
						glbInvOb = new CGlobalInventoryObj();
						itemPtr->Clone(&(glbInvOb->m_itemData));
						CGlobalInventoryObjList::GetInstance()->AddTail(glbInvOb);
					}
				}
				possiblyUnsavedItems->SafeDelete();
				delete possiblyUnsavedItems;
				possiblyUnsavedItems = NULL;
			}
		}
		ar >> m_autoCfg >> m_skills >> m_deathCfg;

		//arrays ect
		{
			// m_transformMatrix has been removed from the class.
			// Continue to serialize for compatibility.
			Matrix44f m_transformMatrix;
			for (int loop = 0; loop < 4; loop++) {
				for (int loop2 = 0; loop2 < 4; loop2++) {
					ar >> m_transformMatrix[loop][loop2];
					ProgressUpdater::UpdateProgress("CMovementObj::Serialize");
				}
			}
		}

		if (version > 1) {
			// read in camera data
			int count = 0;
			ar >> count;
			int depInt;
			for (int i = 0; i < count; i++)
				ar >> depInt >> depInt;

			// Only Camera Is 'Orbit Camera' From ClientEngine::LoadVportCameraConfigurations()
			if (count)
				m_cameraData.push_back(CameraData(0, 0, 0));
		}

		if (version > 2) {
			for (int tempi = 0; tempi < 4; tempi++) {
				ar >> m_actorGroups[tempi]; // Added by Jonny
				if (m_actorGroups[tempi] == 0) {
					m_actorGroups[tempi] = -1; // fix so actor can never be in the none group.
				}
			}
		}

		m_pMO_posses = nullptr;
		m_pMO_possedBy = nullptr;
		m_boundingRadius = m_unscaledBoundingRadius;

		m_validThisRound = true;

		m_networkDefinedID = -1;

		const Vector3f zeroVec(0.0f, 0.0f, 0.0f);
		const Vector3f zeroUpVec(0.0f, 1.0f, 0.0f);
		m_lastPosition = zeroVec;
		setPosForceExt(zeroVec);
		m_recentSurfaceInfo.contactLocation = zeroVec;
		m_recentSurfaceInfo.faceNormal = zeroUpVec;

		m_possedBy_traceNumber = -1;

		m_baseFrame = 0;

		m_idxRefModel = 0;

		m_networkEntity = false;

		m_firstPersonMode = FALSE;
		m_currentLOD = -1;
		m_recentSurfaceInfo.objectType = ObjectType::NONE;
		m_recentSurfaceInfo.objectId = 0;

		m_overrideAnimType = eAnimType::Invalid;
		m_overrideTimeMs = 0;
		m_overrideDurationMs = 0;

		m_distanceToCamTemp = 0.0f;

		m_touchTriggered = false;

		m_clanName = "";
		m_title = "";

		m_isInFov = false;

		m_iModel = nullptr;
	}

	tmpSkeletalObject->detach();

	if (!ar.IsStoring())
		delete tmpSkeletalObject;
}

BOOL CMovementObj::mount(CMovementObj* target, int mountType /*=MT_RIDE*/) {
	target->m_possedBy_traceNumber = m_traceNumber;
	m_pMO_posses = target;
	target->m_pMO_possedBy = this;
	auto pMC = m_movementDB;
	if (pMC)
		pMC->ClearPosForce();

	m_mountType = mountType;
	if (m_mountType == MT_STUNT)
		m_pMO_posses->m_ridable = 0;

	return TRUE;
}

BOOL CMovementObj::deMount() {
	if (!m_pMO_posses)
		return FALSE;

	auto pMC = m_movementDB;
	if (pMC) {
		if (m_mountType != MT_STUNT) {
			Vector3f position;
			m_baseFrame->GetPosition(position);
			Matrix44f newMatrix;
			newMatrix.MakeIdentity();
			newMatrix(3, 0) = position.x;
			newMatrix(3, 1) = position.y;
			newMatrix(3, 2) = position.z + 4;
			m_baseFrame->AddTransformReplace(newMatrix);
			pMC->SetPosLast(newMatrix.GetTranslation());
		}

		pMC->ClearPosForce();
	}

	m_pMO_posses->m_possedBy_traceNumber = -1;
	m_pMO_posses->m_pMO_possedBy = NULL;
	m_pMO_posses = NULL;

	m_mountType = MT_NONE;

	return TRUE;
}

int CMovementObjList::getNextFreeTraceNumber() {
	return ++m_nextFreeTraceNumber; // let it wrap, hopefully by the time 64K items have been in system, re-use won't collide
}

CMovementObj* CMovementObjList::getObjByTraceNumber(int trace) const {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)GetNext(posLoc);
		if (pMO && (pMO->getTraceNumber() == trace))
			return pMO;
	}
	return nullptr;
}

CMovementObj* CMovementObjList::getObjByNetTraceId(int id) const {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)GetNext(posLoc);
		if (pMO && (pMO->getNetTraceId() == id))
			return pMO;
	}
	return nullptr;
}

CMovementObj* CMovementObjList::getObjTypeStaticByNetTraceId(int id) const {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)GetNext(posLoc);
		if (pMO && pMO->isTypeStatic() && (pMO->getNetTraceId() == id))
			return pMO;
	}
	return nullptr;
}

CMovementObj* CMovementObjList::getObjByIndex(int index) const {
	if (index < 0)
		return nullptr;
	POSITION posLoc = FindIndex(index);
	return (posLoc ? (CMovementObj*)GetAt(posLoc) : NULL);
}

CMovementObj* CMovementObjList::getObjByName(CStringA name) const {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pMO = (CMovementObj*)GetNext(posLoc);
		if (pMO && (pMO->getName() == name.GetString()))
			return pMO;
	}
	return nullptr;
}

void CMovementObjList::iterateReMeshes(std::function<void(const CDeformableMesh*)> func) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pMovObj = static_cast<const CMovementObj*>(GetNext(posLoc));
		if (pMovObj->hasSkeleton() && pMovObj->getConfigurableMeshSlots() != nullptr) {
			const CAlterUnitDBObjectList* pMeshSlots = pMovObj->getConfigurableMeshSlots();
			for (POSITION posLoc2 = pMeshSlots->GetHeadPosition(); posLoc2 != NULL;) {
				auto dbu = static_cast<const CAlterUnitDBObject*>(pMeshSlots->GetNext(posLoc2));
				if (dbu && dbu->m_configurations) {
					for (POSITION posLoc3 = dbu->m_configurations->GetHeadPosition(); posLoc3 != NULL;) {
						auto dMesh = static_cast<const CDeformableMesh*>(dbu->m_configurations->GetNext(posLoc3));
						if (dMesh != nullptr) {
							func(dMesh);
						}
					}
				}
			}
		}
	}
}

int CMovementObjList::countReMeshes() {
	int reMeshCount = 0;
	iterateReMeshes([&reMeshCount](const CDeformableMesh* pDeformableMesh) {
		reMeshCount = reMeshCount + pDeformableMesh->m_lodCount;
	});
	return reMeshCount;
}

int CMovementObjList::countCoreMeshes() {
	int reMeshCount = 0;
	iterateReMeshes([&reMeshCount](const CDeformableMesh* pDeformableMesh) {
		if (pDeformableMesh->m_coreContent) {
			reMeshCount = reMeshCount + pDeformableMesh->m_lodCount;
		}
	});
	return reMeshCount;
}

ReSkinMesh** CMovementObjList::gatherCoreMeshesWithDup() {
#ifdef _ClientOutput
	int reMeshCount = countCoreMeshes();
	ReSkinMesh** outMeshes = new ReSkinMesh*[reMeshCount];
	int meshIndex = 0;

	iterateReMeshes([&outMeshes, &meshIndex](const CDeformableMesh* pDeformableMesh) {
		if (pDeformableMesh->m_coreContent) {
			for (unsigned int i = 0; i < pDeformableMesh->m_lodCount; i++) {
				outMeshes[meshIndex] = static_cast<ReSkinMesh*>(MeshRM::Instance()->FindMesh(pDeformableMesh->m_lodHashes[i], MeshRM::RMP_ID_PERSISTENT));
				meshIndex = meshIndex + 1;
			}
		}
	});
	return outMeshes;
#else
	return 0;
#endif
}

ReSkinMesh** CMovementObjList::gatherSkinMeshesWithDup() {
#ifdef _ClientOutput
	int reMeshCount = countReMeshes();
	ReSkinMesh** outMeshes = new ReSkinMesh*[reMeshCount];
	int meshIndex = 0;

	iterateReMeshes([&outMeshes, &meshIndex](const CDeformableMesh* pDeformableMesh) {
		for (unsigned int i = 0; i < pDeformableMesh->m_lodCount; i++) {
			outMeshes[meshIndex] = static_cast<ReSkinMesh*>(MeshRM::Instance()->FindMesh(pDeformableMesh->m_lodHashes[i], MeshRM::RMP_ID_PERSISTENT));
			meshIndex = meshIndex + 1;
		}
	});
	return outMeshes;
#else
	return 0;
#endif
}

void CMovementObjList::Serialize(CArchive& ar) {
	if (::ServerSerialize) {
		CKEPObList::Serialize(ar);
	}
#ifdef _ClientOutput
	else if (ar.IsStoring()) {
		// Store native remeshes
		int reMeshCount;
		MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
		ReSkinMesh** outMeshes;
		if (!m_saveWholeEDB) {
			// just core
			reMeshCount = countCoreMeshes();
			outMeshes = gatherCoreMeshesWithDup();
		} else {
			// all meshes
			reMeshCount = countReMeshes();
			outMeshes = gatherSkinMeshesWithDup();
		}
		// store meshes in the APD archive
		MeshRM::Instance()->StoreReMeshes((ReMesh**)outMeshes, reMeshCount, ar, MeshRM::RMP_ID_PERSISTENT);
		// store loose meshes
		MeshRM::Instance()->StoreReMeshes(outMeshes, reMeshCount);

		matCompressor->addMaterials(this);
		matCompressor->compress();
		matCompressor->writeToArchive(ar); // write compressed materials

		CKEPObList::Serialize(ar);
		delete[] outMeshes;
		MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	} else {
		MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
		int version = ar.GetObjectSchema();
		if (version >= 1) {
			auto pICE = IClientEngine::Instance();
			if (!pICE)
				return;
			MeshRM::Instance()->LoadReMeshes(ar, pICE->GetReDeviceState(), MeshRM::RMP_ID_PERSISTENT);
		}
		if (version >= 2) {
			matCompressor->readFromArchive(ar);
		}
		CKEPObList::Serialize(ar);
		MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	}
#endif
}

// Related to RuntimeSkeleton function of same name.  If this returns false we are waiting for
// Assets for this movementObj to progressively load, and we should not be rendering it.
bool CMovementObj::assetsLoaded() {
#if defined(_ClientOutput)
	if (m_pSkeleton)
		return m_pSkeleton->areAllMeshesLoaded();
#endif
	return true;
}

bool CMovementObj::groupMatch(CMovementObj* pMO) {
	// Assume this Movement Object ?
	if (!pMO)
		pMO = this;

	for (size_t i = 0; i < 4; ++i)
		if ((pMO->m_actorGroups[i] == m_actorGroups[i]) && (m_actorGroups[i] != 0))
			return true;

	return false;
}

// BLOCKING - Better to call ContentMetadataCacheAsync() before calling this!
bool CMovementObj::cacheItem(const GLID& glid) {
#ifdef _ClientOutput
	if (!IS_UGC_GLID(glid))
		return true;

	if (!ContentService::Instance()->ContentMetadataIsCached(glid))
		ContentService::Instance()->ContentMetadataCacheSync(glid);

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	pICE->CacheUGCMetadata(glid);
#endif
	return true;
}

CItemObj* CMovementObj::getItem(const GLID& glid) {
	// Find Item In Global Inventory By GLID
	CGlobalInventoryObj* pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(glid);
	if (!pGIO) {
		LogError("CGlobalInventoryObjList::GetByGLID() FAILED - glid<" << glid << ">");
		return NULL;
	}
	CItemObj* pIO = pGIO->m_itemData;
	if (!pIO) {
		LogError("GlobalInventoryObj::itemData null - glid<" << glid << ">");
		return NULL;
	}

	// Verify Item Matches Actor Groups {ALL, male, female, ...}
	if (!pIO->MatchActorGroups(4, m_actorGroups)) {
		LogError("MatchActorGroup() FAILED - glid<" << glid << "> '" << m_name << "'");
		return NULL;
	}

	return pIO;
}

class DefaultClothingTraverser : public CGlobalInventoryObjList::Traverser {
public:
	DefaultClothingTraverser(CItemObj::itemVector& vec, int* actorGroups) :
			defaultItems(vec), m_actorGroups(actorGroups) {}
	virtual bool evaluate(CGlobalInventoryObj& obj) {
		CItemObj* itemPtr = obj.m_itemData;
		if (itemPtr->m_defaultArmedItem)
			if (itemPtr->MatchActorGroups(4, m_actorGroups))
				defaultItems.push_back(itemPtr);
		return false;
	}
	CItemObj::itemVector& defaultItems;
	int* m_actorGroups;
};

void CMovementObj::getDefaultClothingItems(CItemObj::itemVector& defaultItems) {
	CGlobalInventoryObjList::GetInstance()->traverse(DefaultClothingTraverser(defaultItems, m_actorGroups));
}

void CMovementObj::removeWOKHair() {
	// Avatar hair is composed of multiple skinned meshes (see CharacterCreation2.lua)
	if (IS_STD_FEMALE_AVATAR(m_idxRefModel)) {
		// female hair slots: 27 to 31
		for (int i = 27; i < 32; i++) {
			m_skeletonConfiguration->HotSwapDim(i, -1, m_pSkeleton->getSkinnedMeshCount(), false);
		}
	} else if (IS_STD_MALE_AVATAR(m_idxRefModel)) {
		// male hair slots: 25 to 31
		for (int i = 25; i < 32; i++) {
			m_skeletonConfiguration->HotSwapDim(i, -1, m_pSkeleton->getSkinnedMeshCount(), false);
		}
	}

	realizeDimConfig();

	m_pSkeleton->setMeshSortValid(false);
}

void CMovementObj::reapplyWOKHair() {
	// Avatar hair is composed of multiple skinned meshes (see CharacterCreation2.lua)
	if (IS_STD_FEMALE_AVATAR(m_idxRefModel)) {
		// female hair slots: 27 to 31
		for (int i = 27; i < 32; i++) {
			SHORT meshId = m_skeletonConfiguration->m_charConfig.getItemSlotMeshId(GOB_BASE_ITEM, i);
			USHORT matlId = m_skeletonConfiguration->m_charConfig.getItemSlotMaterialId(GOB_BASE_ITEM, i);
			m_skeletonConfiguration->HotSwapDim(i, meshId, m_pSkeleton->getSkinnedMeshCount(), false);
			m_skeletonConfiguration->HotSwapMat(i, meshId, matlId);
		}
	} else if (IS_STD_MALE_AVATAR(m_idxRefModel)) {
		// male hair slots: 25 to 31
		for (int i = 25; i < 32; i++) {
			SHORT meshId = m_skeletonConfiguration->m_charConfig.getItemSlotMeshId(GOB_BASE_ITEM, i);
			USHORT matlId = m_skeletonConfiguration->m_charConfig.getItemSlotMaterialId(GOB_BASE_ITEM, i);
			m_skeletonConfiguration->HotSwapDim(i, meshId, m_pSkeleton->getSkinnedMeshCount(), false);
			m_skeletonConfiguration->HotSwapMat(i, meshId, matlId);
		}
	}

	realizeDimConfig();

	m_pSkeleton->setMeshSortValid(false);
}

BOOL CMovementObj::fadeIn(TimeMs duration) {
	return m_pSkeleton->fadeIn(duration);
}

IMPLEMENT_SERIAL_SCHEMA(CMovementObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CMovementObjList, CKEPObList)

Vector3f CMovementObj::getHeadPosition() {
	Vector3f headPoint;
	if (m_movementDB->IsStandVerticalToSurfaceEnabled()) {
		//actor is rotated to be aligned with collision nomral
		float characterHeight = m_boundingRadius; // this is the offset from the feet
		Vector3f feetPoint;
		m_baseFrame->GetPosition(feetPoint); //get postion where actor is standing at,
			//use this as feet position

		//translate feetpostion using the collision normal
		Vector3f dirVec = m_recentSurfaceInfo.faceNormal.GetNormalized();

		//now calculate the head point based off the feet point
		headPoint.x = feetPoint.x + dirVec.x * characterHeight;
		headPoint.y = feetPoint.y + dirVec.y * characterHeight;
		headPoint.z = feetPoint.z + dirVec.z * characterHeight;
	} else {
		Matrix44f positionMatrix;
		m_baseFrame->GetTransform(NULL, &positionMatrix);

		Vector3f center = positionMatrix.GetTranslation();

		//actor is standing vertical on the screen
		headPoint.x = center.x;
		headPoint.y = center.y + m_boundingRadius; //bndBox.maxY;
		headPoint.z = center.z;
	}

	return headPoint;
}

void CMovementObj::setOverrideAnimationByType(eAnimType animType, int animVer, bool bLockAnimation) {
	m_overrideAnimType = animType;
	m_overrideDurationMs = 0;
	m_overrideAnimLock = bLockAnimation;
	m_pSkeleton->setAnimVerPreference(animVer);
	m_overrideTimeMs = fTime::TimeMs();
	if (m_pMO_posses && m_mountType == MT_STUNT)
		m_pMO_posses->setOverrideAnimationByType(animType, animVer, bLockAnimation);
}

void CMovementObj::setOverrideAnimationSettings(const AnimSettings& animSettings, bool bLockAnimation) {
	if (!animSettings.isValid()) {
		resetOverrideAnimType();
		return;
	}

	setOverrideAnimationByType(eAnimType::None, m_pSkeleton->getAnimVerPreference(), bLockAnimation);
	m_overrideAnimSettings = animSettings;
	if (m_pMO_posses && m_mountType == MT_STUNT)
		m_pMO_posses->m_overrideAnimSettings = animSettings;
}

void CMovementObj::resetOverrideAnimType() {
	m_overrideAnimType = eAnimType::Invalid;
	m_overrideAnimSettings = AnimSettings();
	m_overrideDurationMs = 0;
	m_overrideAnimLock = false;
	m_overrideTimeMs = 0;
}

bool CMovementObj::getSubstitute(ObjectType& type, int& id) const {
	type = m_substituteObjType;
	id = m_substituteObjId;
	return type != ObjectType::NONE;
}

void CMovementObj::setSubstitute(ObjectType type, int id) {
	m_substituteObjType = type;
	m_substituteObjId = id;
}

void CMovementObj::clearLookAt() {
	if (m_lookAtInfo) {
		delete m_lookAtInfo;
		m_lookAtInfo = nullptr;
	}
}

void CMovementObj::setLookAt(ObjectType targetType, int targetId, const std::string& targetBone, const Vector3f& offset, bool followTarget) {
	// Clear Previous Look At Setting
	clearLookAt();

	if (targetType == ObjectType::NONE)
		return;

	// Keep looking at the target
	m_lookAtInfo = new LookAtInfo;
	m_lookAtInfo->targetType = targetType;
	m_lookAtInfo->targetId = targetId;
	m_lookAtInfo->targetBone = targetBone;
	m_lookAtInfo->offset = offset;
	m_lookAtInfo->followTarget = followTarget;
}

bool CMovementObj::getLookAt(ObjectType& targetType, int& targetId, std::string& targetBone, Vector3f& offset, bool& followTarget) const {
	if (!m_lookAtInfo) {
		targetType = ObjectType::NONE;
		return false;
	}

	targetType = m_lookAtInfo->targetType;
	targetId = m_lookAtInfo->targetId;
	targetBone = m_lookAtInfo->targetBone;
	offset = m_lookAtInfo->offset;
	followTarget = m_lookAtInfo->followTarget;
	return true;
}

void CMovementObj::setCurrentCollision(std::vector<int> aIdNewCollision, const std::function<void(int iId, bool bNewState)>& changeCallback) {
	std::sort(aIdNewCollision.begin(), aIdNewCollision.end());
	aIdNewCollision.erase(std::unique(aIdNewCollision.begin(), aIdNewCollision.end()), aIdNewCollision.end());
	if (changeCallback) {
		std::vector<int>::const_iterator itr_old = m_aIdCurrentCollision.begin();
		std::vector<int>::const_iterator itr_new = aIdNewCollision.begin();
		while (itr_new != aIdNewCollision.end() && itr_old != m_aIdCurrentCollision.end()) {
			if (*itr_new < *itr_old) {
				changeCallback(*itr_new, true);
				++itr_new;
			} else if (*itr_new > *itr_old) {
				changeCallback(*itr_old, false);
				++itr_old;
			} else {
				++itr_new;
				++itr_old;
			}
		}
		for (; itr_new != aIdNewCollision.end(); ++itr_new)
			changeCallback(*itr_new, true);
		for (; itr_old != m_aIdCurrentCollision.end(); ++itr_old)
			changeCallback(*itr_old, false);
	}
	m_aIdCurrentCollision = std::move(aIdNewCollision);
}

void CMovementObj::saveLastPositionFromBaseFrame(bool alsoUpdateMovementCaps) {
	m_baseFrame->GetPosition(m_lastPosition);
	if (alsoUpdateMovementCaps) {
		auto pMC = m_movementDB;
		if (pMC)
			pMC->SetPosLast(m_lastPosition);
	}
}

void CMovementObj::deleteMovementCaps() {
	if (m_movementDB) {
		delete m_movementDB;
		m_movementDB = nullptr;
	}
}

const CMovementObj::CameraData& CMovementObj::getCameraData(size_t index) const {
	if (index >= m_cameraData.size()) {
		static CameraData dummy;
		return dummy;
	}

	return m_cameraData[index];
}

const CMovementObj::CameraData& CMovementObj::getCurrentCameraData() const {
	if (m_currentCameraDataIndex < 0 || m_currentCameraDataIndex >= (int)m_cameraData.size()) {
		static CameraData dummy;
		return dummy;
	}

	return m_cameraData[m_currentCameraDataIndex];
}

void CMovementObj::setRuntimeCameraId(size_t index, int runtimeCameraId) {
	assert(index < m_cameraData.size());
	if (index < m_cameraData.size()) {
		m_cameraData[index].m_runtimeCameraId = runtimeCameraId;
	}
}

const CAlterUnitDBObjectList* CMovementObj::getConfigurableMeshSlots() const {
	return m_skeletonConfiguration != nullptr ? m_skeletonConfiguration->m_meshSlots : nullptr;
}

CAlterUnitDBObjectList* CMovementObj::getConfigurableMeshSlots() {
	return m_skeletonConfiguration != nullptr ? m_skeletonConfiguration->m_meshSlots : nullptr;
}

size_t CMovementObj::getTotalConfigSlotCount() const {
	return m_skeletonConfiguration->m_charConfig.getTotalSlotCount() + m_pSkeleton->getNumPendingEquippableItems();
}

bool CMovementObj::configMeshSlot(RuntimeSkeleton* pSkeleton, SkeletonConfiguration* pSkeletonCfg, int slotId, int meshId, int matlId) {
	auto pMeshSlots = getConfigurableMeshSlots();
	if (pMeshSlots == nullptr) {
		return false;
	}

	POSITION posSlot = pMeshSlots->FindIndex(slotId);
	if (posSlot == nullptr) {
		return false;
	}

	//Ankit ----- if meshId < 0, no need to update base char config
	bool updateCharConfig = true;
	if (meshId < 0) {
		//This is added quickly to be able to set a material on the base config without having a mesh index
		meshId = pSkeletonCfg->m_newConfig.getItemSlotMeshId(GOB_BASE_ITEM, slotId);
		updateCharConfig = false;
	}

	auto pCurSlot = static_cast<CAlterUnitDBObject*>(pMeshSlots->GetAt(posSlot));
	POSITION posCfg = pCurSlot->m_configurations->FindIndex(meshId);
	if (!posCfg)
		return false;

	auto pDeformableMesh = static_cast<CDeformableMesh*>(pCurSlot->m_configurations->GetAt(posCfg));
	if (pDeformableMesh && pDeformableMesh->m_publicDim) {
		if (pSkeletonCfg->m_charConfig.getItemCount() == 0) {
			pSkeletonCfg->m_newConfig.initBaseItem(m_pSkeleton->getSkinnedMeshCount());
			pSkeletonCfg->m_charConfig.initBaseItem(m_pSkeleton->getSkinnedMeshCount());
		}

		if (!pSkeletonCfg->HotSwapDim(slotId, meshId, m_pSkeleton->getSkinnedMeshCount(), updateCharConfig))
			return false;

		if (!pSkeletonCfg->HotSwapMat(slotId, meshId, matlId, updateCharConfig))
			return false;

		pSkeletonCfg->RealizeDimConfig(pSkeleton, pMeshSlots);
	}

	return true;
}

void CMovementObj::addChildFrame(CFrameObj* child) {
	assert(m_baseFrame != nullptr);
	child->SetParent(m_baseFrame);
}

const Matrix44f& CMovementObj::getBaseFrameMatrix() const {
	assert(m_baseFrame != nullptr);
	return m_baseFrame->m_frameMatrix;
}

void CMovementObj::setBaseFrameMatrix(const Matrix44f& matrix, bool cascade) {
	assert(m_baseFrame != nullptr);
	if (cascade) {
		m_baseFrame->AddTransformReplace(matrix);
	} else {
		m_baseFrame->m_frameMatrix = matrix;
	}
	TransformChanged();
}

void CMovementObj::setBaseFramePosition(const Vector3f& pos, bool cascade) {
	assert(m_baseFrame != nullptr);
	if (cascade)
		m_baseFrame->SetPosition(pos);
	else
		m_baseFrame->m_frameMatrix.GetTranslation() = pos;
	TransformChanged();
}

void CMovementObj::setBaseFrameOrientationCascade(const Vector3f& dir, const Vector3f& up) {
	assert(m_baseFrame != nullptr);
	m_baseFrame->SetOrientation(dir, up);
	TransformChanged();
}

void CMovementObj::setBaseFramePosDirUp(const Vector3f& pos, const Vector3f& dir, const Vector3f& up) {
	m_baseFrame->SetPosDirUp(pos, dir, up);
	TransformChanged();
}

void CMovementObj::getBaseFramePosDirUp(Vector3f& pos, Vector3f& dir, Vector3f& up) const {
	m_baseFrame->GetPosDirUp(pos, dir, up);
}

void CMovementObj::transformBaseFrameCascade(const Matrix44f& matrix) {
	assert(m_baseFrame != nullptr);
	m_baseFrame->AddTransformBefore(matrix);
}

void CMovementObj::standVerticalToSurface() {
	assert(m_baseFrame != nullptr);
	//rotate the actor using the collision normal
	//Y' = N (the collision normal becomes the new Y axis)
	//and then calculate the new X and Z axis using cross product
	//X' = (Y' x Z) = Y' x { 0.0f, 0.0f, 1.0f };)
	//Z' = (X' x Y') = X' x Y'
	m_recentSurfaceInfo.faceNormal.Normalize();
	Vector3f yVector = m_recentSurfaceInfo.faceNormal;

	Vector3f dir, up;
	m_baseFrame->GetOrientation(dir, up);
	Vector3f xVector = yVector.Cross(dir).GetNormalized();
	Vector3f zVector = xVector.Cross(yVector).GetNormalized();

	Matrix44f rotationMat;
	rotationMat.MakeIdentity();

	rotationMat.SetRowX(xVector);
	rotationMat.SetRowY(yVector);
	rotationMat.SetRowZ(zVector);

	m_baseFrame->AddTransformReplaceRotationOnly(rotationMat);
	TransformChanged();
}

void CMovementObj::TransformChanged() {
#ifdef _ClientOutput
	if (m_pVisibleBody && m_baseFrame) {
		m_pVisibleBody->SetTransformMetric(FeetToMeters(m_baseFrame->m_frameMatrix));
	}
#endif
}
void CMovementObj::setNetTraceId(long val) {
	m_networkDefinedID = val;
#ifdef _ClientOutput
	if (m_pVisibleBody) {
		PhysicsBodyData* pPhysicsBodyData = static_cast<PhysicsBodyData*>(m_pVisibleBody->GetUserData().get());
		if (pPhysicsBodyData)
			pPhysicsBodyData->m_iObjectId = getNetTraceId();
	}
#endif
}

void CMovementObj::CameraData::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CMovementObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_aIdCurrentCollision); // std::vector<int>
		pMemSizeGadget->AddObject(m_autoCfg); // CCfgObjList*
		pMemSizeGadget->AddObject(m_baseFrame); // CFrameObj*
		pMemSizeGadget->AddObject(m_cameraData); // std::vector<CameraData>
		pMemSizeGadget->AddObject(m_clanName); // std::string
		pMemSizeGadget->AddObject(m_currentlyArmedItems); // CItemObjList*
		pMemSizeGadget->AddObject(m_deathCfg); // CCfgObjList*
		pMemSizeGadget->AddObject(m_displayName); // std::string
		pMemSizeGadget->AddObject(m_effectTrack); // EffectTrack
		pMemSizeGadget->AddObject(m_inventory); // CInventoryObjList*
		pMemSizeGadget->AddObject(m_lookAtInfo); // LookAtInfo*
		pMemSizeGadget->AddObject(m_movementDB); // CMovementCaps*
		pMemSizeGadget->AddObject(m_name); // std::string
		pMemSizeGadget->AddObject(m_pMO_possedBy); // CMovementObj*
		pMemSizeGadget->AddObject(m_pMO_posses); // CMovementObj*
		pMemSizeGadget->AddObject(m_pOtherPlayerVehicleRigidBody); // std::shared_ptr<Physics::RigidBodyStatic>
		pMemSizeGadget->AddObject(m_pPhysicsAvatar); // std::shared_ptr<Physics::Avatar>
		pMemSizeGadget->AddObject(m_pPhysicsVehicle); // std::shared_ptr<Physics::Vehicle>
		pMemSizeGadget->AddObject(m_pSkeleton); // std::shared_ptr<RuntimeSkeleton>
		pMemSizeGadget->AddObject(m_pVisibleBody); // std::shared_ptr<Physics::RigidBodyStatic>
		pMemSizeGadget->AddObject(m_skeletonConfiguration); // SkeletonConfiguration*
		pMemSizeGadget->AddObject(m_skeletonLODList); // CSkeletalLODObjectList*
		pMemSizeGadget->AddObject(m_skills); // CSkillObjectList*
		pMemSizeGadget->AddObject(m_stats); // CSkillObjectList*
		pMemSizeGadget->AddObject(m_title); // std::string
		pMemSizeGadget->AddObject(m_ugcActor); // UGCActor*
	}

	/*-------------------------------------------------------------------------------
	// The following data member, iModel* m_iModel appears to be unused. The only 
	// concrete class DynamicPlacementModelProxy is not instantiated or used in any 
	// context.
	// #MLB_TODO - Confirm it can be deleted and then delete
	-------------------------------------------------------------------------------*/
}

void CMovementObj::MovementInterpolatorInit() {
	MovementData md;
	m_baseFrame->GetPosDirUp(md.pos, md.dir, md.up);
	m_movementInterpolator.Update(md, true);
}

// Disable unused GETSETs. Delete them when ready to rebuild scripts because deleting changes numbers and
// causes incompatibility with existing script.
BEGIN_GETSET_IMPL(CMovementObj, IDS_MOVEMENTOBJ_NAME)
GETSET_MAX(m_name, gsString, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, PLAYERNAME, STRLEN_MAX)
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, PLAYER_LAST_NAME, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, PLAYER_CLAN_NAME, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, PLAYER_TITLE, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, RIDABLE, 0, 0) // Disabled
GETSET_RANGE(m_controlType, gsInt, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, CONTROLTYPE, INT_MIN, INT_MAX)
GETSET_OBJ_PTR(m_movementDB, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, MOVEMENTDB, CMovementCaps)
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, TRACENUMBER, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, BOUNDINGRADIUS, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, SPEEDBASEDANIMATION, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, LOCATION, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, CONTROLCONFIG, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, EXACTCOLLISION, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, COLLISIONINFOFILE, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, FILTERCOLLISION, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, PRIMARYPHYSICSOVERD, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, HARDPOINTOVERDY, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, MODELTYPE, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, FIRSTPERSONMODEL, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, FULLMODELREF, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, CAMFIRSTPERSON, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, CAMTHIRDPERSON, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, FIRSTPERSONREF, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, DEATHCAM, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, HUDLINK, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, SKELETONLODLIST, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, STATS, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, RESOURCES, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, INVENTORY, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, ITEMDB, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, AUTOCFG, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, SKILLS, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, DEATHCFG, 0, 0) // Disabled
GETSET(m_networkDefinedID, gsLong, GS_FLAG_READ_ONLY, 0, 0, MOVEMENTOBJ, NETWORKDEFINEDID)
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, CLANMEMBER, 0, 0) // Disabled
GETSET(m_idxRefModel, gsInt, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, REFMODEL)
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, CURHITCOUNTER, 0, 0) // Disabled
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, DEATHTIME, 0, 0) // Disabled
GETSET_D3DVECTOR(m_lastPosition, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, LASTPOSITION)
GETSET_OBJ_PTR(m_baseFrame, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTOBJ, BASEFRAMEOBJECT, CFrameObj)
GETSET_CUSTOM(nullptr, gsCustom, 0, 0, 0, MOVEMENTOBJ, DISPLAYNAME, 0, 0) // Disabled
END_GETSET_IMPL

} // namespace KEP
