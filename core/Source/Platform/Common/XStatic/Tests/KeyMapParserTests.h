#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/include/CoreStatic/KeyMapParser.h"
#include <sstream>

inline bool TestKeyMapParserResult(const std::string& strInput, const std::string& strExpected) {
	std::istringstream strm(strInput);

	eKeyCodeType aKeyCodeTypes[] = { eKeyCodeType::WindowsMessage, eKeyCodeType::DirectInput };
	for (eKeyCodeType keyCodeType : aKeyCodeTypes) {
		strm.clear();
		strm.seekg(std::ios::beg, 0);
		std::vector<KeyEventBindingByName> aKeyEventBindings;
		if (!ParseKeyMappings(strm, keyCodeType, out(aKeyEventBindings)))
			return false;

		std::ostringstream ostrstrm;
		if (!PrintKeyMappings(ostrstrm, keyCodeType, aKeyEventBindings))
			return false;

		std::string strOutput = ostrstrm.str();
		if (strOutput != strExpected)
			return false;
	}

	return true;
}

TEST(KeyMapParserTests, BasicTest) {
	std::string strInput =
		"A on() when(down(a)|down(b)|down(c))\n"
		"B on() when(down(a,b,c))\n"
		"C on() when(!up(a)|!up(b)|!up(c))\n"
		"D on() when(!up(a,b,c))\n"
		"E on() when(!(down(a)|down(b)|down(c)))\n"
		"F on() when(!(down(a,b,c)))\n"
		"G on() when(!(!up(a)|!up(b)|!up(c)))\n"
		"H on() when(!(!up(a,b,c)))\n";

	std::string strExpected =
		"A on() when(down(A) | down(B) | down(C))\n"
		"B on() when(down(A) | down(B) | down(C))\n"
		"C on() when(down(A) | down(B) | down(C))\n"
		"D on() when(down(A) | down(B) | down(C))\n"
		"E on() when(up(A) & up(B) & up(C))\n"
		"F on() when(up(A) & up(B) & up(C))\n"
		"G on() when(up(A) & up(B) & up(C))\n"
		"H on() when(up(A) & up(B) & up(C))\n";

	ASSERT_TRUE(TestKeyMapParserResult(strInput, strExpected));
}

TEST(KeyMapParserTests, CommentedFileTest) {
	std::string strInput =
		"# down() is the same as !up()\n"
		"# down(a,b) is satisfied whenever a OR b is down.\n"
		"# up(a,b) is satisfied when both a AND b are up.\n"
		"car_left on() when((down(a) & up(shift)) | down(leftarrow)) # Always turn left when 'a' key is down and shift is up, or when left arrow key is down.\n"
		"car_right on() when(down(d) & !down(shift)) # Always turn right when 'd' key is down and shift is not down\n"
		"car_exit on(press(x)) when(!down(shift) & !down(control) & !down(alt)) # exit car when x is pressed down\n"
		"car_forward on() when(down(w))\n"
		"car_brake on() when(down(space))\n";

	std::string strExpected =
		"car_left on() when(down(LEFTARROW) | (down(A) & up(LSHIFT) & up(RSHIFT)))\n"
		"car_right on() when(down(D) & up(LSHIFT) & up(RSHIFT))\n"
		"car_exit on( X Press) when(up(LSHIFT) & up(RSHIFT) & up(LCONTROL) & up(RCONTROL) & up(LALT) & up(RALT))\n"
		"car_forward on() when(down(W))\n"
		"car_brake on() when(down(SPACE))\n";

	ASSERT_TRUE(TestKeyMapParserResult(strInput, strExpected));
}

TEST(KeyMapParserTests, ModifierTest) {
	std::string strInput =
		"A on() when(down(a)&down(shift))\n"
		"B on() when(down(a)&up(shift))\n"
		"C on() when(down(a)&down(ctrl))\n"
		"D on() when(down(a)&up(ctrl))\n"
		"E on() when(down(a)&down(alt))\n"
		"F on() when(down(a)&up(alt))\n";

	std::string strExpected =
		"A on() when(down(A) & (down(LSHIFT) | down(RSHIFT)))\n"
		"B on() when(down(A) & up(LSHIFT) & up(RSHIFT))\n"
		"C on() when(down(A) & (down(LCONTROL) | down(RCONTROL)))\n"
		"D on() when(down(A) & up(LCONTROL) & up(RCONTROL))\n"
		"E on() when(down(A) & (down(LALT) | down(RALT)))\n"
		"F on() when(down(A) & up(LALT) & up(RALT))\n";

	ASSERT_TRUE(TestKeyMapParserResult(strInput, strExpected));
}

TEST(KeyMapParserTests, ComplexTest) {
	std::string strInput =
		"A on() when(down(a)&down(b)|down(c)^down(d)&down(e)|down(f)&down(g)^down(h))\n"
		"B on() when(((down(a)&down(b)))|(down(c)^(down(d)&down(e)))|((down(f)&down(g))^down(h)))\n"
		"C on() when(down(a)|down(b)|down(c)^down(d)^down(e)|down(f)^down(g)&down(h)&down(i)^down(j)|down(k)|down(l)|down(m)^down(n)&down(o)^down(p)|down(q)^down(r)|down(s)&down(t)&down(u)&down(v)&down(w)|down(x)&down(y)^down(z))\n"
		"D on() when((down(a)|down(b)|down(c)^down(d)^down(e))|(down(f)^(down(g)&down(h)&down(i))^down(j))|(down(k)|down(l))|(down(m)^down(n)&down(o)^down(p))|(down(q)^down(r))|((down(s)&down(t)&down(u)&down(v)&down(w)))|((down(x)&down(y))^down(z)))\n"
		"E on() when(down(a)&down(b)^down(c)&down(d)|down(e)^down(f)^down(g)^down(h)|down(i)|down(j)|down(k)&down(l)|down(m)|down(n)^down(o)&down(p)&down(q)&down(r)^down(s)&down(t)&down(u)|down(v)^down(w)&down(x)&down(y)&down(z))\n"
		"F on() when(((down(a)&down(b))^(down(c)&down(d)))|(down(e)^down(f)^down(g)^down(h))|(down(i))|(down(j))|((down(k)&down(l)))|(down(m))|(down(n)^(down(o)&down(p)&down(q)&down(r))^(down(s)&down(t)&down(u)))|(down(v)^(down(w)&down(x)&down(y)&down(z))))\n";

	// Note: Bare operands are always printed before parenthesized operand groups.
	std::string strExpected =
		"A on() when((down(A) & down(B)) | (down(C) ^ (down(D) & down(E))) | (down(H) ^ (down(F) & down(G))))\n"
		"B on() when((down(A) & down(B)) | (down(C) ^ (down(D) & down(E))) | (down(H) ^ (down(F) & down(G))))\n"
		"C on() when(down(A) | down(B) | down(K) | down(L) | (down(C) ^ down(D) ^ down(E)) | (down(F) ^ down(J) ^ (down(G) & down(H) & down(I))) | (down(M) ^ down(P) ^ (down(N) & down(O))) | (down(Q) ^ down(R)) | (down(S) & down(T) & down(U) & down(V) & down(W)) | (down(Z) ^ (down(X) & down(Y))))\n"
		"D on() when(down(A) | down(B) | down(K) | down(L) | (down(C) ^ down(D) ^ down(E)) | (down(F) ^ down(J) ^ (down(G) & down(H) & down(I))) | (down(M) ^ down(P) ^ (down(N) & down(O))) | (down(Q) ^ down(R)) | (down(S) & down(T) & down(U) & down(V) & down(W)) | (down(Z) ^ (down(X) & down(Y))))\n"
		"E on() when(down(I) | down(J) | down(M) | ((down(A) & down(B)) ^ (down(C) & down(D))) | (down(E) ^ down(F) ^ down(G) ^ down(H)) | (down(K) & down(L)) | (down(N) ^ (down(O) & down(P) & down(Q) & down(R)) ^ (down(S) & down(T) & down(U))) | (down(V) ^ (down(W) & down(X) & down(Y) & down(Z))))\n"
		"F on() when(down(I) | down(J) | down(M) | ((down(A) & down(B)) ^ (down(C) & down(D))) | (down(E) ^ down(F) ^ down(G) ^ down(H)) | (down(K) & down(L)) | (down(N) ^ (down(O) & down(P) & down(Q) & down(R)) ^ (down(S) & down(T) & down(U))) | (down(V) ^ (down(W) & down(X) & down(Y) & down(Z))))\n";

	ASSERT_TRUE(TestKeyMapParserResult(strInput, strExpected));
}

TEST(KeyMapParserTests, NotTest) {
	std::string strInput =
		"A on() when(!(down(a)&down(b)))\n"
		"B on() when(!!!(down(a)&down(b)))\n"
		"C on() when(!(!(down(a)&down(b))&down(c)))\n"
		"D on() when(!((up(a)|up(b))&down(c)))\n"
		"E on() when((down(a)&down(b))|up(c))\n"
		"F on() when(!(!(!(down(a)&down(b))&down(c))&down(d)))\n"
		"G on() when(!!(!(!(down(a)&down(b))&down(c))&down(d)))\n"
		"H on() when(!(!(!(down(a)^down(b)^down(c))&down(d))^down(e)&!(down(f)^down(g))))\n"
		"I on() when(!((!(up(a)^down(b)^down(c))|up(d))^down(e)&!(down(f)^down(g))))\n"
		"J on() when(!((!(up(a)^down(b)^down(c))|up(d))^down(e)&(up(f)^down(g))))\n"
		"K on() when(!(((down(a)^down(b)^down(c))|up(d))^down(e)&(up(f)^down(g))))\n"
		"L on() when((!((down(a)^down(b)^down(c))|up(d))^down(e)&(up(f)^down(g))))\n"
		"M on() when(!((down(a)^down(b)^down(c))|up(d))^down(e)&(up(f)^down(g)))\n"
		"N on() when((!(down(a)^down(b)^down(c))&down(d))^down(e)&(up(f)^down(g)))\n"
		"O on() when(((up(a)^down(b)^down(c))&down(d))^down(e)&(up(f)^down(g)))\n";

	// Note: Bare operands are always printed before parenthesized operand groups.
	std::string strExpected =
		"A on() when(up(A) | up(B))\n"
		"B on() when(up(A) | up(B))\n"
		"C on() when(up(C) | (down(A) & down(B)))\n"
		"D on() when(up(C) | (down(A) & down(B)))\n"
		"E on() when(up(C) | (down(A) & down(B)))\n"
		"F on() when(up(D) | (down(C) & (up(A) | up(B))))\n"
		"G on() when(down(D) & (up(C) | (down(A) & down(B))))\n"
		"H on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n"
		"I on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n"
		"J on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n"
		"K on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n"
		"L on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n"
		"M on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n"
		"N on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n"
		"O on() when((down(D) & (up(A) ^ down(B) ^ down(C))) ^ (down(E) & (up(F) ^ down(G))))\n";

	ASSERT_TRUE(TestKeyMapParserResult(strInput, strExpected));
}
