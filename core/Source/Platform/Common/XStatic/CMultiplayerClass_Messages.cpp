///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ctype.h"

#include "common\include\CoreStatic\CMultiplayerClass.h"

#include "Common\include\ISender.h"
#include "Common\include\CoreStatic\CServerItemClass.h"
#include "CAccountClass.h"
#include "CPacketQueClass.h"
#include "CPlayerClass.h"
#include "CompressHelper.h"

#include "Common\include\LogHelper.h"

namespace KEP {

static LogInstance("Instance");

bool CMultiplayerObj::s_msgGzipLog = false;
size_t CMultiplayerObj::s_msgGzipSize = 128;

COMPRESS_METRICS s_gzCompress;
COMPRESS_METRICS s_gzUncompress;

static std::string MsgStr(const BYTE* pData, size_t dataSize) {
	if (!pData)
		return "{null}";

	std::string str;
	bool isHex = false;
	for (size_t i = 0; i < dataSize; ++i) {
		if ((i % 100) == 0)
			StrAppend(str, "\n ... ");
		if (i && isprint(pData[i])) {
			if (isHex)
				StrAppend(str, " ");
			StrAppend(str, (char)pData[i]);
			isHex = false;
		} else {
			StrAppend(str, " x" << std::hex << std::uppercase << (int)pData[i]);
			isHex = true;
		}
	}
	return str;
}

static bool MsgGzipCompress(BYTE*& pData, size_t& dataSize) {
	if (!pData || (dataSize < CMultiplayerObj::s_msgGzipSize))
		return false;

	// Perform Gzip Compress
	Timer timer;
	auto gzipSize = compressBound(dataSize);
	auto pGzip = new Bytef[sizeof(MSG_GZIP_HEADER) + gzipSize];
	auto rv = compress2(pGzip + sizeof(MSG_GZIP_HEADER), &gzipSize, (Bytef*)pData, dataSize, Z_BEST_SPEED);
	if (rv < 0) {
		LogError("compress() FAILED - rv=" << rv << " dataSize=" << dataSize << " gzipSize=" << gzipSize);
		delete[] pGzip;
		return false;
	}

	// ED-7512 - Don't Message Compress Under 10% Savings
	double ratio = (double)(sizeof(MSG_GZIP_HEADER) + gzipSize) / (double)dataSize;
	if (ratio > 0.9) {
		delete[] pGzip;
		return false;
	}

	// Update Metrics
	TimeMs timeMs = timer.ElapsedMs();
	s_gzCompress.Add(dataSize, sizeof(MSG_GZIP_HEADER) + gzipSize, timeMs);

	// Log Message?
	if (CMultiplayerObj::s_msgGzipLog) {
		LogInfo(dataSize
				<< " -> "
				<< (sizeof(MSG_GZIP_HEADER) + gzipSize) << " bytes"
				<< " (" << FMT_TIME << timeMs << "ms)"
				<< " " << s_gzCompress.ToStr()
				<< MsgStr(pData, dataSize));
	}

	// Set Compressed Message Header
	auto pMsg = (MSG_GZIP_HEADER*)pGzip;
	pMsg->m_msgCat = MSG_CAT::GZIP;
	pMsg->m_uncompressedSize = dataSize;

	// Return Compressed Data (caller must free)
	pData = pGzip;
	dataSize = sizeof(MSG_GZIP_HEADER) + gzipSize;

	return true;
}

static bool MsgGzipUncompress(BYTE*& pGzip, size_t& gzipSize) {
	auto pMsg = (MSG_GZIP_HEADER*)pGzip;
	if (!pMsg || (pMsg->m_msgCat != MSG_CAT::GZIP))
		return false;

	// Perform Gzip Uncompress
	Timer timer;
	auto dataSize = (uLong)pMsg->m_uncompressedSize;
	auto pData = new Bytef[dataSize];
	auto rv = uncompress(pData, &dataSize, (Bytef*)pGzip + sizeof(MSG_GZIP_HEADER), gzipSize - sizeof(MSG_GZIP_HEADER));
	if (rv < 0) {
		LogError("uncompress() FAILED - rv=" << rv << " dataSize=" << dataSize << " gzipSize=" << gzipSize);
		delete[] pData;
		return false;
	}

	// Update Metrics
	TimeMs timeMs = timer.ElapsedMs();
	s_gzUncompress.Add(dataSize, gzipSize, timeMs);

	// Log Message ?
	if (CMultiplayerObj::s_msgGzipLog) {
		LogInfo(gzipSize
				<< " -> "
				<< dataSize << " bytes"
				<< " (" << FMT_TIME << timeMs << "ms)"
				<< " " << s_gzUncompress.ToStr()
				<< MsgStr(pData, dataSize));
	}

	// Return Uncompressed Data (caller must free)
	pGzip = pData;
	gzipSize = dataSize;

	return true;
}

bool CMultiplayerObj::MsgTx(const BYTE* pData, DWORD dataSize, NETID netIdTo) {
	if (!pData || !dataSize)
		return false;

	// Extract Message Category
	auto msgCat = (MSG_CAT)*pData;

	// Gzip Data ?
	BYTE* pTx = (BYTE*)pData;
	size_t sizeTx = dataSize;
	MsgGzipCompress(pTx, sizeTx);

	// Accumulate Message Metrics
	if (msgCat != MSG_CAT::NONE) {
		MsgMetric& mcs = m_msgMetrics[(size_t)msgCat];
		mcs.m_msgTxCount++;
		mcs.m_msgTxBytes += sizeTx;
	}

	// Actually Send The Message
	ULONG ret = NET_OK;
	if (m_clientNetwork) {
		ret = m_clientNetwork->Send(netIdTo, pTx, sizeTx);
	} else if (m_serverNetwork) {
		ret = m_serverNetwork->Send(netIdTo, pTx, sizeTx);
	}

	if (pTx != pData)
		delete[] pTx;

	// Handle Server Disconnects
	if (m_serverNetwork && FAILED(ret) && netIdTo != 0) {
		// 1/09 add sendTo check since often trying to sendTo 0.  Send() will log
		// 11/07 fire disconnected event because someone doesn't know this player is gone
		IEvent* e = new DisconnectedEvent(netIdTo, DISC_UNKNOWN_NETID, 0);
		m_dispatcher->QueueEvent(e);
		return false;
	}

	return SUCCEEDED(ret);
}

bool CMultiplayerObj::MsgRx(BYTE*& pData, size_t& dataSize, NETID& netIdFrom) {
	// Reset Return Values
	dataSize = 0;
	pData = nullptr;
	netIdFrom = 0;

	if (!m_packetQue) {
		LogError("Invalid packetQue!");
		return false;
	}

	// ED-7834 - Warn On Stalling
	Timer timer;
	TimeMs timeMs;

	CPacketObj* pPacketObj = m_packetQue->GetNextPacket();
	if (!pPacketObj) {
		// ED-7834 - Warn On Stalling
		timeMs = timer.ElapsedMs();
		if (timeMs > 100) {
			LogError("SLOW(" << FMT_TIME << timeMs << "ms) - GetNextPacket() Failed");
		}
		return false;
	}

	pData = pPacketObj->m_pData;
	dataSize = pPacketObj->m_dataSize;
	netIdFrom = pPacketObj->m_netId;

	// ED-7834 - Warn On Stalling
	timeMs = timer.ElapsedMs();
	if (timeMs > 100) {
		LogWarn("SLOW(" << FMT_TIME << timeMs << "ms) - GetNextPacket() bytes=" << dataSize);
	}
	timer.Reset();

	// Gzip Data ?
	BYTE* pRx = pData;
	size_t sizeRx = dataSize;
	MsgGzipUncompress(pRx, sizeRx);

	// ED-7834 - Warn On Stalling
	timeMs = timer.ElapsedMs();
	if (timeMs > 100) {
		LogWarn("SLOW(" << FMT_TIME << timeMs << "ms) - MsgGzipUncompress() bytes=" << sizeRx);
	}

	// Accumulate Message Metrics
	auto msgCat = (MSG_CAT)*pRx;
	if (msgCat != MSG_CAT::NONE) {
		MsgMetric& mcs = m_msgMetrics[(size_t)msgCat];
		mcs.m_msgRxCount++;
		mcs.m_msgRxBytes += dataSize;
	}

	if (pRx != pData) {
		delete[] pData;
		pData = pRx;
		dataSize = sizeRx;
	}

	delete pPacketObj;

	return true;
}

bool CMultiplayerObj::SendMsgLogToServer(const std::string& userName, const std::string& password) {
	MSG_LOG_TO_SERVER msg;
	strcpy_s(msg.logInName, userName.c_str());
	strcpy_s(msg.logInPass, password.c_str());
	msg.timeLogToServer = fTime::TimeMs();
	LogInfo("'" << userName << "' timeLogToServer=" << msg.timeLogToServer);
	return MsgTx((BYTE*)&msg, sizeof(msg), SERVER_NETID);
}

bool CMultiplayerObj::SendMsgLogToClient(
	NETID netIdTo,
	TimeMs timeLogToServer, // drf - added
	TimeMs timeProcessing // drf - added
) {
	MSG_LOG_TO_CLIENT msg;
	msg.timeLogToServer = timeLogToServer; // drf - added
	msg.timeProcessing = timeProcessing; // drf - added
	msg.timeLogToClient = fTime::TimeMs(); // drf - added
	LogInfo("timeLogToServer=" << msg.timeLogToServer
							   << " timeProcessing=" << msg.timeProcessing
							   << " timeLogToClient=" << msg.timeLogToClient);
	return MsgTx((BYTE*)&msg, sizeof(msg), netIdTo);
}

bool CMultiplayerObj::QueueMsgAttribToClient(NETID netIdTo, const ATTRIB_DATA& attribData, CPlayerObject* pPO) {
	bool storeIt = true;
	if (!pPO || (pPO && pPO->m_aiControlled))
		storeIt = false;

	if (netIdTo == SERVER_NETID) {
		if (!m_pMsgOutboxAttrib)
			return false;
		m_pMsgOutboxAttrib->AddPacket(sizeof(ATTRIB_DATA), (BYTE*)&attribData);
		return true;
	} else if (storeIt) {
		if (!pPO->m_pMsgOutboxAttrib)
			return false;
		pPO->m_pMsgOutboxAttrib->AddPacket(sizeof(ATTRIB_DATA), (BYTE*)&attribData);
		return true;
	}

	auto msgSize = sizeof(MSG_ATTRIB);
	auto pMsg = (MSG_ATTRIB*)new BYTE[msgSize];
	if (!pMsg)
		return false;

	pMsg->SetCategory(MSG_CAT::ATTRIB_TO_CLIENT);
	pMsg->SetVersion(0);
	pMsg->m_data = attribData;

	bool ok = MsgTx((BYTE*)pMsg, msgSize, netIdTo);
	delete pMsg;
	return ok;
}

bool CMultiplayerObj::QueueMsgAttribToServer(const ATTRIB_DATA& attribData) {
	if (!m_pMsgOutboxAttrib)
		return false;
	m_pMsgOutboxAttrib->AddPacket(sizeof(ATTRIB_DATA), (BYTE*)&attribData);
	return true;
}

bool CMultiplayerObj::SendMsgMoveToClient(NETID netId, CPlayerObject* pPO, bool sendForce) {
	if (!pPO || (pPO->m_dbCfg < 0))
		return false;

	// Limit Move Messages To Client
	if (!sendForce && (pPO->m_msgMoveToClientTimer.ElapsedMs() < msgMoveMinTimeMs))
		return false;
	pPO->m_msgMoveToClientTimer.Reset();

	MSG_MOVE_TO_CLIENT msgHdr;
	msgHdr.m_numMoveToClientData = 0;
	msgHdr.m_numStaticEntityData = 0;
	msgHdr.m_numSpawnStaticMOData = pPO->m_pMsgOutboxSpawnStaticMO->GetCount();

	// Accumulate All Entities Move Data For Update
	std::vector<BYTE> mtcd;
	for (POSITION pos = m_runtimePlayersOnServer->GetHeadPosition(); pos;) {
		auto pPO_srv = (CPlayerObject*)m_runtimePlayersOnServer->GetNext(pos);
		if (!pPO_srv || (pPO_srv->m_justSpawned == TRUE) || (pPO->m_netTraceId == pPO_srv->m_netTraceId) // is self
			|| (pPO->m_currentChannel != pPO_srv->m_currentChannel) // different world
			|| (pPO_srv->m_dbCfg < 0))
			continue;

		// Encode Entity Move Data (only deltas since last message)
		auto moveData = pPO_srv->InterpolatedMoveData();
		if (pPO->EncodeMoveDataToClient(moveData, mtcd))
			msgHdr.m_numMoveToClientData++;
	}

	// Accumulate All Static Movement Objects For Update (city vendors, etc.)
	ServerItemVec siVec;
	if (m_pServerItemObjMap) {
		// Get ServerItemVec For Zone
		ServerItemVec* pSIV = nullptr;
		m_pServerItemObjMap->GetServerItemVec(pSIV, pPO->m_currentZoneIndex.toLong(), pPO->m_currentChannel);
		if (pSIV) {
			for (const auto& pSIO : *pSIV) {
				if (!pSIO)
					continue;
				msgHdr.m_numStaticEntityData++;
				siVec.push_back(pSIO);
			}
		}

		if (pPO->m_currentChannel.isPermanent()) {
			pSIV = nullptr;
			m_pServerItemObjMap->GetServerItemVec(pSIV, pPO->m_currentChannel.channelId());
			if (pSIV) {
				for (const auto& pSIO : *pSIV) {
					if (!pSIO || !pSIO->m_existsInAllPermInstances)
						continue;
					msgHdr.m_numStaticEntityData++;
					siVec.push_back(pSIO);
				}
			}
		}
	}

	// Length of each data block
	size_t numBytesMoveToClientData = mtcd.size();
	size_t numBytesStaticData = sizeof(STATIC_ENTITY_DATA) * msgHdr.m_numStaticEntityData;
	size_t numBytesSpawnStaticMOData = 0;
	if (msgHdr.m_numSpawnStaticMOData > 0)
		numBytesSpawnStaticMOData = pPO->m_pMsgOutboxSpawnStaticMO->PackedSizeBytes();

	// Header size
	size_t headerSize = sizeof(MSG_MOVE_TO_CLIENT);

	// Allocate buffer
	size_t bufferSize = headerSize + numBytesMoveToClientData + numBytesStaticData + numBytesSpawnStaticMOData;
	BYTE* buffer = new BYTE[bufferSize];
	CopyMemory(buffer, &msgHdr, headerSize);

	MOVE_TO_CLIENT_DATA* pMoveToClientData = nullptr;
	STATIC_ENTITY_DATA* pStaticEntityData = nullptr;
	BYTE* pSpawnStaticMOData = nullptr;

	size_t byteOffset = headerSize;

	if (numBytesMoveToClientData) {
		pMoveToClientData = (MOVE_TO_CLIENT_DATA*)(buffer + byteOffset);
		byteOffset += numBytesMoveToClientData;
	}
	if (numBytesStaticData) {
		pStaticEntityData = (STATIC_ENTITY_DATA*)(buffer + byteOffset);
		byteOffset += numBytesStaticData;
	}
	if (numBytesSpawnStaticMOData) {
		pSpawnStaticMOData = buffer + byteOffset;
		byteOffset += numBytesSpawnStaticMOData;
	}

	// Pack Move To Client Data
	if (numBytesMoveToClientData)
		CopyMemory(pMoveToClientData, &(mtcd[0]), numBytesMoveToClientData);

	// Pack Static Entity Data
	// If client has not yet instanced one of these it will respond with an
	// AttribRequestSpawnStaticMovementObject for the full movement object info.
	size_t staticTrace = 0;
	for (const auto& pSIO : siVec) {
		if (!pSIO)
			continue;
		if (staticTrace < msgHdr.m_numStaticEntityData) {
			pStaticEntityData[staticTrace].netTraceIdSpecial = -pSIO->m_netTraceId; // <0=static
			staticTrace++;
		}
	}

	// Pack Spawn Static Movement Object Data
	if (numBytesSpawnStaticMOData)
		pPO->m_pMsgOutboxSpawnStaticMO->PackList(pSpawnStaticMOData);
	pPO->m_pMsgOutboxSpawnStaticMO->SafeDelete();

	// Send Message
	bool ok = MsgTx(buffer, bufferSize, netId);

	delete[] buffer;

	return ok;
}

bool CMultiplayerObj::QueueOrSendMsgUpdate_EventsOnly(IEvent* pEvent) {
	// Serialize Event Data To Buffer
	std::vector<unsigned char> eventBuffer;
	pEvent->SerializeToBuffer(eventBuffer);
	assert(!eventBuffer.empty());

	// Handle Client Events To Server
	bool isClient = (m_clientNetwork != nullptr);
	if (isClient) {
		m_pMsgOutboxEvent->AddPacket(eventBuffer.size(), &eventBuffer.front());
		return true;
	}

	// Handle Server Events To Client
	bool isServer = (m_serverNetwork != nullptr);
	if (isServer) {
		// for each "to", queue it to the guaranteed outbox
		for (const auto& netId : pEvent->To()) {
			auto pPO = m_runtimePlayersOnServer->GetPlayerObjectByNetId(netId, false);
			bool queued = false;
			if (pPO && pPO->m_pMsgOutboxEvent) {
				pPO->m_pMsgOutboxEvent->AddPacket(eventBuffer.size(), &eventBuffer.front(), netId);
				queued = true;
			}

			if (!queued) {
				// try sending it directly, during login, won't be in runtime list
				MsgTx(&eventBuffer.front(), eventBuffer.size(), netId);
			}
		}

		return true;
	}

	return false;
}

BOOL CMultiplayerObj::QueueOrSendRenderTextEvent(NETID sendTo, const char* message, eChatType chatType) {
	DWORD textSize = ::strlen(message);
	if (textSize == 0)
		return FALSE;

	if (textSize > 250)
		return FALSE;

	IEvent* pEvent = new RenderTextEvent(message, chatType);
	pEvent->AddTo(sendTo);
	QueueOrSendMsgUpdate_EventsOnly(pEvent); // bypass dipatcher, and just send it, a bit more efficient since ends up calling this anyway
	pEvent->Delete();

	return TRUE;
}

bool CMultiplayerObj::SetMsgSpawnToClientOnAccountObject(
	CAccountObject* pAO,
	short dbIndex,
	const ZoneIndex& zoneIndex,
	const std::string& exgFileName,
	const std::string& url,
	const Vector3f& spawnPt,
	int rotation,
	bool initialSpawn) {
	if (exgFileName.size() > _MAX_PATH) {
		LogError("Invalid world file: " << exgFileName);
		return false;
	}

	MSG_SPAWN_TO_CLIENT* pMsg = NULL;
	DWORD messageSize;
	messageSize = sizeof(MSG_SPAWN_TO_CLIENT) + exgFileName.size(); // no need to add 1 since already have one for placeholder
	messageSize += url.size() + 1; // add one for null (4/08)
	pMsg = (MSG_SPAWN_TO_CLIENT*)new BYTE[messageSize];
	pMsg->SetCategory(MSG_CAT::SPAWN_TO_CLIENT);
	pMsg->aiConfigDeprecated = 0;
	pMsg->dbIndex = dbIndex;
	pMsg->x = spawnPt.x;
	pMsg->y = spawnPt.y;
	pMsg->z = spawnPt.z;
	pMsg->zoneIndexInt = zoneIndex.toLong();
	pMsg->instanceId = zoneIndex.GetInstanceId();

	// do this so fit in char
	if (rotation < -359 || rotation > 359)
		rotation = 0;
	else if (rotation > 180)
		rotation -= 360;
	else if (rotation < -180)
		rotation += 360;

	pMsg->rotation = rotation / 2;

	lstrcpyA(pMsg->exgFileName, exgFileName.c_str());
	lstrcpyA(pMsg->exgFileName + exgFileName.size() + 1, url.c_str());

	SpawnInfo& spawnInfo = pAO->GetSpawnInfo();
	if (spawnInfo.isSet()) {
		LogError("Overlapped spawn! " << pAO->m_accountName << " attempting to spawn to " << numToString(pMsg->zoneIndexInt) << ":" << pMsg->instanceId << " initialSpawn = " << initialSpawn);
		return false;
	}

	spawnInfo.updateSpawnMsg(pMsg, messageSize);
	return true;
}

bool CMultiplayerObj::SendMsgSpawnToClient(CAccountObject* pAO, CPlayerObject* pPO, NETID netIdTo) {
	if (!pAO)
		return false;

	const SpawnInfo& spawnInfo = pAO->GetSpawnInfo();
	if (pPO) {
		// not initial spawn
		auto pPO_cur = pAO->GetCurrentPlayerObject();
		if (!pPO_cur)
			return false;

		UpdateAccount(pPO, pAO); // update account for skills
		if (m_runtimePlayersOnServer->RemoveByPtrByLiveHandle(pPO->m_handle)) {
			// dont let nobody do anything to this
			pPO = 0;
		}

		assert(spawnInfo.getZoneIndexInt() != 0);
		pPO_cur->m_currentZoneIndex.fromLong(spawnInfo.getZoneIndexInt());

		// Send PlayerDepart if player is going to a new zone, or m_forcePlayerDepartEventOnSpawn flag is true
		NETID prevNetID = 0;
		ZoneIndex departZoneIndex;
		unsigned playerArriveId = 0;
		if (pAO->FetchPendingPlayerDepartInfo(pPO_cur->m_forcePlayerDepartEventOnSpawn ? ZoneIndex() : pPO_cur->m_currentZoneIndex, prevNetID, departZoneIndex, playerArriveId)) {
			// Player has left the zone. Send PlayerDeparted event now
			LogInfo("PlayerDepartedEvent -> <" << pAO->m_accountName << ":" << prevNetID << ">");
			assert(playerArriveId > 0);
			SendPlayerDepartedEvent(prevNetID, departZoneIndex, playerArriveId);
		} else {
			// Player is rezoning back. Schedule a backup PlayerDeparted event in case the expected PlayerArrived event is *NOT* coming due to connectivity issue etc
			LogInfo("RecordPlayerDepartTime " << pPO_cur->ToStr());
			pAO->RecordPlayerDepartTime();
		}

		// Clear m_forcePlayerDepartEventOnSpawn flag
		pPO_cur->m_forcePlayerDepartEventOnSpawn = false;

		MovementData md = pPO_cur->InterpolatedMovementData();
		md.pos = spawnInfo.getSpawnPt();
		pPO_cur->MovementInterpolatorInit(md);
	}

	if (!spawnInfo.isSet()) {
		LogError("Missing spawn struct for user " << pAO->m_accountName);
		return false;
	}

	size_t dataSize = 0;
	const BYTE* pData = reinterpret_cast<const BYTE*>(spawnInfo.getSpawnMsg(dataSize));
	assert(pData != nullptr && dataSize > 0);
	return MsgTx(pData, dataSize, netIdTo);
}

bool CMultiplayerObj::SendMsgSpawnToServer(
	short dbIndex,
	const ZoneIndex& zoneIndex,
	const CStringA& worldFile,
	int traceNumber,
	short useNewPresets,
	short characterInUse,
	short aiCfgIndex) {
	if (worldFile.GetLength() > _MAX_PATH)
		return false;

	DWORD messageSize = sizeof(MSG_SPAWN_TO_SERVER) + worldFile.GetLength(); // no need to add 1 since already have one for placeholder
	auto pMsg = (MSG_SPAWN_TO_SERVER*)new BYTE[messageSize];
	pMsg->SetCategory(MSG_CAT::SPAWN_TO_SERVER);
	pMsg->currentTrace = traceNumber;
	pMsg->dbIndex = dbIndex;
	pMsg->usePresets = useNewPresets;
	pMsg->characterInUse = characterInUse;
	pMsg->aiCfgIndex = aiCfgIndex;
	pMsg->zoneIndex = zoneIndex.toLong();
	lstrcpyA(pMsg->worldName, worldFile);

	LogInfo(zoneIndex.ToStr() << " worldName='" << worldFile.GetString() << "'");

	bool ok = MsgTx((BYTE*)pMsg, messageSize);
	if (pMsg)
		delete pMsg;

	return ok;
}

std::string CMultiplayerObj::GetMsgMetrics(const std::string& runtimeId) {
	std::string str;
	size_t num = 0;
	StrAppend(str, "{\"collection\":\"msg_metrics\"");
	if (!runtimeId.empty())
		StrAppend(str, ",\"runtimeId\":\"" << runtimeId << "\"");
	StrAppend(str, ",\"messageTypes\":[");
	for (size_t i = 0; i < m_msgMetrics.size(); ++i) {
		const auto& mcs = m_msgMetrics[i];
		if (!mcs.m_msgTxCount && !mcs.m_msgRxCount)
			continue;
		if (num++)
			StrAppend(str, ",");
		StrAppend(str, "{\"msgCat\":" << i);
		if (mcs.m_msgRxCount)
			StrAppend(str, ",\"rxCount\":" << mcs.m_msgRxCount);
		if (mcs.m_msgRxBytes)
			StrAppend(str, ",\"rxBytes\":" << mcs.m_msgRxBytes);
		if (mcs.m_msgTxCount)
			StrAppend(str, ",\"txCount\":" << mcs.m_msgTxCount);
		if (mcs.m_msgTxBytes)
			StrAppend(str, ",\"txBytes\":" << mcs.m_msgTxBytes);
		StrAppend(str, "}");
	}
	StrAppend(str, "]");
	StrAppend(str, ",\"compress\":{");
	StrAppend(str, "\"num\":" << s_gzCompress.m_num);
	StrAppend(str, ",\"origBytes\":" << s_gzCompress.m_origSize);
	StrAppend(str, ",\"compBytes\":" << s_gzCompress.m_compSize);
	StrAppend(str, ",\"timeMs\":" << s_gzCompress.m_timeMs);
	StrAppend(str, "}");
	StrAppend(str, ",\"uncompress\":{");
	StrAppend(str, "\"num\":" << s_gzUncompress.m_num);
	StrAppend(str, ",\"origBytes\":" << s_gzUncompress.m_origSize);
	StrAppend(str, ",\"compBytes\":" << s_gzUncompress.m_compSize);
	StrAppend(str, ",\"timeMs\":" << s_gzUncompress.m_timeMs);
	StrAppend(str, "}");
	StrAppend(str, "}");
	return str;
}

} // namespace KEP