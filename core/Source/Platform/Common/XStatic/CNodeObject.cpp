///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include "CStructuresMisl.h"
#include <afxtempl.h>
#include "CNodeObject.h"
#include "MatrixARB.h"
#include "Core/Math/TransformUtil.h"

namespace KEP {

CNodeObj::CNodeObj() {
	m_libraryReference = 0;
	ZeroMemory(&m_clipBox, sizeof(ABBOX));

	m_translation.x = 0.0f;
	m_translation.y = 0.0f;
	m_translation.z = 0.0f;
	;

	m_scale.x = 1.0f;
	m_scale.y = 1.0f;
	m_scale.z = 1.0f;

	m_rotation.x = 0.0f;
	m_rotation.y = 0.0f;
	m_rotation.z = 0.0f;

	m_worldTransform.MakeIdentity();
} // CNodeObj::CNodeObj

void CNodeObj::SafeDelete() {
} // CNodeObj::SafeDelete

void CNodeObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(CArchiveObjectSchemaVersion<CNodeObj>);
		ar << m_libraryReference
		   << m_clipBox.minX
		   << m_clipBox.minY
		   << m_clipBox.minZ
		   << m_clipBox.maxX
		   << m_clipBox.maxY
		   << m_clipBox.maxZ;

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				ar << m_worldTransform[i][j];

		ar << m_translation.x
		   << m_translation.y
		   << m_translation.z;

		ar << m_scale.x
		   << m_scale.y
		   << m_scale.z;

		ar << m_rotation.x
		   << m_rotation.y
		   << m_rotation.z;
	} // if..ar.IsStoring()
	else {
		int version;

		version = ar.GetObjectSchema();
		ar >> m_libraryReference >> m_clipBox.minX >> m_clipBox.minY >> m_clipBox.minZ >> m_clipBox.maxX >> m_clipBox.maxY >> m_clipBox.maxZ;

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				ar >> m_worldTransform[i][j];

		if (version >= 2) {
			ar >> m_translation.x >> m_translation.y >> m_translation.z;

			ar >> m_scale.x >> m_scale.y >> m_scale.z;

			ar >> m_rotation.x >> m_rotation.y >> m_rotation.z;
		} // if..2 >= version
		else {
			DecomposeTransform();
		} // if..else 2 >= version
	} // if..else..ar.IsStoring()
} // CNodeObj::Serialize

void CNodeObj::SetScaleTranslationRotation(const Vector3f& scale, const Vector3f& translation, const Vector3f& rotation) {
	m_scale = scale;
	m_rotation = rotation;
	m_translation = translation;
	BuildTransform();
} // CEXMeshObj::SetScaleTranslationRotation

void CNodeObj::SetScale(const Vector3f& scale) {
	m_scale = scale;
	BuildTransform();
} // CNodeObj::SetScale

void CNodeObj::SetRotation(const Vector3f& rotation) {
	m_rotation = rotation;
	BuildTransform();
} // CNodeObj::SetRotation

void CNodeObj::SetTranslation(const Vector3f& translation) {
	m_translation = translation;
	BuildTransform();
} // CNodeObj::SetTranslation

void CNodeObj::SetWorldTransform(const Matrix44f& worldTransform) {
	m_worldTransform = worldTransform;
	DecomposeTransform();
} // CNodeObj::SetWorldTransform

void CNodeObj::ResetWorldTransform() {
	m_scale.Set(1, 1, 1);
	m_translation.Set(0, 0, 0);
	m_rotation.Set(0, 0, 0);

	m_worldTransform.MakeIdentity();
} // CNodeObj::ResetWorldTransform

void CNodeObj::BuildTransform() {
	Quaternionf q;
	ConvertRotationXYZ(ToRadians(m_rotation), out(q));
	ScaleRotationTranslationToMatrix(m_scale, q, m_translation, out(m_worldTransform));
} // CNodeObj::BuildTransform

void CNodeObj::DecomposeTransform() {
	Quaternionf qRot;
	MatrixToScaleRotationTranslation(m_worldTransform, out(m_scale), out(qRot), out(m_translation));
	Vector3f vAngles;
	ConvertRotationXYZ(qRot, out(vAngles));
	m_rotation = ToDegrees(vAngles);
} // CNodeObj::DecomposeTransform

void CNodeObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CNodeObj* nodePtr = (CNodeObj*)GetNext(posLoc);
		nodePtr->SafeDelete();
		delete nodePtr;
		nodePtr = 0;
	}

	RemoveAll();
} // CNodeObjList::SafeDelete

IMPLEMENT_SERIAL_SCHEMA(CNodeObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CNodeObjList, CObList)

BEGIN_GETSET_IMPL(CNodeObj, IDS_NODEOBJ_NAME)
GETSET_RANGE(m_libraryReference, gsInt, GS_FLAGS_DEFAULT, 0, 0, NODEOBJ, LIBRARYREFERENCE, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP