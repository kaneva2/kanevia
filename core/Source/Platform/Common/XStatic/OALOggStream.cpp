///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "OALOggStream.h"

#define BUFFER_SIZE (4096 * 64)

namespace KEP {

OALOggStream::OALOggStream() {
}

OALOggStream::~OALOggStream() {
	alDeleteBuffers(NUM_BUFFERS, m_buffers);
	ov_clear(&m_oggStream);
}

bool OALOggStream::OpenOggStream(const std::string& url) {
	int result;
	std::string fileName = url;
	if ((result = ov_fopen((char*)fileName.c_str(), &m_oggStream)) < 0)
		return false;

	m_vorbisInfo = ov_info(&m_oggStream, -1);
	m_vorbisComment = ov_comment(&m_oggStream, -1);

	if (m_vorbisInfo->channels == 1)
		m_format = AL_FORMAT_MONO16;
	else
		m_format = AL_FORMAT_STEREO16;

	alGenBuffers(NUM_BUFFERS, m_buffers);

	return true;
}

bool OALOggStream::Update(ALuint source) {
	int processed;
	bool active = true;
	alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed);

	while (processed--) {
		ALuint buffer;
		alSourceUnqueueBuffers(source, 1, &buffer);

		active = Stream(buffer);
		if (active) {
			alSourceQueueBuffers(source, 1, &buffer);
		} else {
			break;
		}
	}
	return active;
}

bool OALOggStream::Stream(ALuint buffer) {
	char data[BUFFER_SIZE];
	int size = 0;
	int section;
	int result;

	memset(data, 0, sizeof(data));

	while (size < BUFFER_SIZE) {
		result = ov_read(&m_oggStream, data + size, BUFFER_SIZE - size, 0, 2, 1, &section);

		if (result > 0)
			size += result;
		else {
			if (result < 0)
				return false;
			else
				break;
		}
	}

	if (size == 0)
		return false;

	alBufferData(buffer, m_format, data, size, m_vorbisInfo->rate);

	return true;
}

bool OALOggStream::Play(ALuint source) {
	if (Playing(source))
		return true;
	for (int i = 0; i < NUM_BUFFERS; i++)
		if (!Stream(m_buffers[i]))
			return false;
	alSourceQueueBuffers(source, NUM_BUFFERS, m_buffers);
	alSourcePlay(source);
	return true;
}

bool OALOggStream::EmptyQueue(ALuint source) {
	int queued;
	alGetSourcei(source, AL_BUFFERS_QUEUED, &queued);
	while (queued--) {
		ALuint buffer;
		alSourceUnqueueBuffers(source, 1, &buffer);
	}
	return true;
}

} // namespace KEP