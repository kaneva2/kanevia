///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CSkeletalLODClass.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

//-----------------------------------------------------------//
//-----class is for persisting characters over a network-----//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkeletalLODObject::CSkeletalLODObject() {
	m_distance = 0;
}

void CSkeletalLODObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_distance;

	} else {
		ar >> m_distance;
	}
}

void CSkeletalLODObject::SafeDelete() {
}

void CSkeletalLODObject::Clone(CSkeletalLODObject** copy) {
	CSkeletalLODObject* copyLocal = new CSkeletalLODObject();
	copyLocal->m_distance = m_distance;

	*copy = copyLocal;
}

void CSkeletalLODObjectList::Serialize(CArchive& ar) {
	CObList::Serialize(ar);

	if (!ar.IsStoring()) {
		m_NumLOD = 0;

		// fill out the LOD distance table
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CSkeletalLODObject* skPtr = (CSkeletalLODObject*)GetNext(posLoc);

			if (skPtr) {
				if (m_NumLOD < SKELETAL_LODS_MAX) {
					m_LODDistance[m_NumLOD++] = skPtr->m_distance;
				}
			}
		}
	}
}

void CSkeletalLODObjectList::Clone(CSkeletalLODObjectList** copy) {
	CSkeletalLODObjectList* copyLocal = new CSkeletalLODObjectList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkeletalLODObject* skPtr = (CSkeletalLODObject*)GetNext(posLoc);
		CSkeletalLODObject* newPtr = NULL;
		skPtr->Clone(&newPtr);
		copyLocal->AddTail(newPtr);
	}

	copyLocal->m_NumLOD = m_NumLOD;
	for (DWORD i = 0; i < m_NumLOD; i++) {
		copyLocal->m_LODDistance[i] = m_LODDistance[i];
	}

	*copy = copyLocal;
}

int CSkeletalLODObjectList::GetLODIndex(int currentDistance) {
	for (DWORD i = 0; i < m_NumLOD; i++) {
		if (m_LODDistance[i] > currentDistance) {
			return i;
		}
	}
	return m_NumLOD - 1;
}

void CSkeletalLODObjectList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkeletalLODObject* skPtr = (CSkeletalLODObject*)GetNext(posLoc);
		skPtr->SafeDelete();
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}

void CSkeletalLODObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CSkeletalLODObjectList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CSkeletalLODObject* pCSkeletalLODObject = static_cast<const CSkeletalLODObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCSkeletalLODObject);
		}
	}
}

IMPLEMENT_SERIAL(CSkeletalLODObject, CObject, 0)
IMPLEMENT_SERIAL(CSkeletalLODObjectList, CObList, 0)

BEGIN_GETSET_IMPL(CSkeletalLODObject, IDS_SKELETALLODOBJECT_NAME)
GETSET_RANGE(m_distance, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKELETALLODOBJECT, DISTANCE, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP