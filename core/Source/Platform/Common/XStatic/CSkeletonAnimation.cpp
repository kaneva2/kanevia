///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CSkeletonAnimation.h"
#include "CBoneAnimationClass.h"
#include "CBoneClass.h"
#include "Utils/ReUtils.h"
#include "KEPConstants.h"
#include "../include/IMemSizeGadget.h"

#define MAGIC_HEADER_SKELETONANIMATION 0x6153 // 'Sa' - Magic word to indicate the use of schema version other than legacy format with header size

namespace KEP {

void SkeletonDef::Write(CArchive& ar) {
	CStringA nameStr, parentStr;
	ar << 1; // version 1
	ar << m_bonesList.size();
	for (unsigned i = 0; i < m_bonesList.size(); ++i) {
		nameStr = m_bonesList[i].name.c_str();
		parentStr = m_bonesList[i].parent.c_str();
		ar << nameStr;
		ar << m_bonesList[i].length;
		ar << parentStr;
	}
}

void SkeletonDef::Read(CArchive& ar) {
	int version;
	ar >> version;

	unsigned numBones;
	ar >> numBones;
	for (unsigned i = 0; i < numBones; i++) {
		float length = -1.0f;
		CStringA nameStr;
		ar >> nameStr;
		std::string name = (const char*)nameStr;
		CStringA parentStr;
		if (version > 0) {
			ar >> length;
			ar >> parentStr;
		}
		std::string parent = (const char*)parentStr;
		AddBone(name, length, parent);
	}

	ASSERT(version <= 1); // Change this line if new version added
}

int SkeletonDef::GetBoneIndexByName(const std::string& name, bool bIgnoreCare, bool bConvertUnderscoreToSpace) {
	CStringA nameToMatch = name.c_str();
	nameToMatch.Replace('_', ' ');
	for (unsigned i = 0; i < m_bonesList.size(); i++) {
		CStringA boneName = m_bonesList[i].name.c_str();
		boneName.Replace('_', ' ');
		if (bIgnoreCare) {
			if (nameToMatch.CompareNoCase(boneName) == 0)
				return i;
		} else {
			if (nameToMatch == boneName)
				return i;
		}
	}

	return -1;
}

std::string SkeletonDef::GetBoneNameByIndex(UINT index, bool bConvertUnderscoreToSpace) {
	jsVerifyReturn(index < m_bonesList.size(), "");

	CStringA boneName = m_bonesList[index].name.c_str();
	if (bConvertUnderscoreToSpace)
		boneName.Replace('_', ' ');

	return (const char*)boneName;
}

void CSkeletonAnimation::init() {
	m_numBones = 0;
	m_animIndex = 0;
	m_animLooping = 0;
	m_animType = eAnimType::None;
	m_animVer = 0;
	m_animMinTimeMs = 0.0;
	m_animDurationMs = 0.0;
	m_animFrameTimeMs = 0.0;
	m_refCount = 0;
	m_hashCode = 0;
	m_headerSize = m_headerSize1 + m_headerSize2;
	m_name = "";
	m_ppBoneAnimationObjs = NULL;
	m_skeletonDef = NULL;
	m_animGlid = GLID_INVALID;
}

CSkeletonAnimation::CSkeletonAnimation() {
	init();
}

CSkeletonAnimation::~CSkeletonAnimation() {
	if (m_ppBoneAnimationObjs) {
		for (UINT i = 0; i < m_numBones; i++) {
			if (m_ppBoneAnimationObjs[i]) {
				delete m_ppBoneAnimationObjs[i];
				m_ppBoneAnimationObjs[i] = NULL;
			}
		}
		delete[] m_ppBoneAnimationObjs;
		m_ppBoneAnimationObjs = NULL;
	}
	m_numBones = 0;
}

void CSkeletonAnimation::Write(CArchive& ar) {
	unsigned short magic = MAGIC_HEADER_SKELETONANIMATION;
	unsigned short schemaVersion = 0; // version 0
	ar << magic;
	ar << schemaVersion;

	ar << m_numBones
	   << m_animIndex
	   << m_animLooping
	   << (int)m_animType
	   << (UINT)m_animVer
	   << (UINT)m_animMinTimeMs
	   << (UINT)m_animDurationMs
	   << (UINT)m_animFrameTimeMs
	   << m_refCount
	   << m_hashCode
	   << (int)m_animGlid;

	ar << m_name.length();
	ar.Write(m_name.c_str(), m_name.length());

	// do key frame reduction & data compression
	for (UINT i = 0; i < m_numBones; i++) {
		ar << m_ppBoneAnimationObjs[i];
	}

	// Write out skeleton definition
	bool skeletonExists = m_skeletonDef != NULL;
	ar << skeletonExists;
	if (skeletonExists)
		m_skeletonDef->Write(ar);
}

void CSkeletonAnimation::ReadLegacy(CArchive& ar) {
	ar.Read((void*)&m_animIndex, m_headerSize1 - sizeof(m_numBones));

	// if version 1 data present, read them
	if (m_headerSize > m_headerSize1) {
		ar.Read((void*)&m_hashCode, m_headerSize2);
	}

	// read the animation data
	for (UINT i = 0; i < m_numBones; i++) {
		ar >> m_ppBoneAnimationObjs[i];
	}

	// get the hash code when going to version 0 from 1
	if (m_headerSize == m_headerSize1) {
		Hash();
	}
}

void CSkeletonAnimation::Read(CArchive& ar) {
	unsigned short magic;
	ar >> magic;
	if (magic != MAGIC_HEADER_SKELETONANIMATION) {
		// legacy file format
		unsigned short tmp;
		ar >> tmp;
		m_numBones = (tmp << 16) + magic; // store first 4 bytes into m_numBones
		ReadLegacy(ar);
		return;
	}

	// new file format
	unsigned short schemaVersion;
	UINT animDurationMs = 0;
	UINT animFrameTimeMs = 0;
	UINT animMinTimeMs = 0;
	int animGlid = 0;
	int animType;
	UINT animVer;
	ar >> schemaVersion;
	ar >> m_numBones >> m_animIndex >> m_animLooping >> animType >> animVer >> animMinTimeMs >> animDurationMs >> animFrameTimeMs >> m_refCount >> m_hashCode >> animGlid;
	m_animGlid = (GLID)animGlid;
	m_animDurationMs = (TimeMs)animDurationMs;
	m_animFrameTimeMs = (TimeMs)animFrameTimeMs;
	m_animMinTimeMs = (TimeMs)animMinTimeMs;
	m_animType = (eAnimType)animType;
	m_animVer = (int)animVer;

	int nameLength;
	ar >> nameLength;
	if (nameLength > 0) {
		char* tmpName = new char[nameLength + 1];
		ar.Read(tmpName, nameLength);
		tmpName[nameLength] = 0;
		m_name = tmpName;
	}

	// read the animation data
	for (UINT i = 0; i < m_numBones; i++) {
		ar >> m_ppBoneAnimationObjs[i];
	}

	bool skeletonExists;
	ar >> skeletonExists;
	if (skeletonExists) {
		m_skeletonDef = new SkeletonDef;
		m_skeletonDef->Read(ar);
	}
}

void CSkeletonAnimation::Hash() {
	UINT boneDatasize = 0;
	UINT hashCode = 0;

	for (UINT i = 0; i < m_numBones; i++) {
		boneDatasize += m_ppBoneAnimationObjs[i]->m_animKeyFrames * sizeof(AnimKeyFrame);
	}

	if (boneDatasize) {
		BYTE* hashBuf = new BYTE[boneDatasize];
		BYTE* hashPtr = hashBuf;
		for (UINT i = 0; i < m_numBones; i++) {
			// This will only work properly if the sum of the sizes of the members of
			// struct AnimationContent == sizeof(AnimationContent).  The simplest way
			// to ensure that is to make sure that AnimationContent packing it set to 1
			// byte.  An alternative would be to memset hashBuf, then do a member-wise
			// copy of data from the source, which would really require an operator=()
			// to be defined on AnimationContent.
			UINT animDatasize = m_ppBoneAnimationObjs[i]->m_animKeyFrames * sizeof(AnimKeyFrame);
			memcpy((void*)hashPtr, (void*)m_ppBoneAnimationObjs[i]->m_pAnimKeyFrames, animDatasize);
			hashPtr += animDatasize;
		}

		hashCode = ReHash(hashBuf, boneDatasize, 0);
		delete[] hashBuf;
	}

	m_hashCode = (((UINT64)hashCode) << 32) | ((UINT64)hashCode);
}

void SkeletonDef::Bone::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(name);
		pMemSizeGadget->AddObject(parent);
	}
}

void SkeletonDef::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_bonesList);
	}
}

} // namespace KEP
