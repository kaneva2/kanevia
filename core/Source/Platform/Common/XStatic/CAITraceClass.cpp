///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <afxtempl.h>
#include <d3d9.h>
#include "CAITraceClass.h"

namespace KEP {

//CONSTRUCTION
CAITraceObj::CAITraceObj() {
	m_NameOfLocation = "Default";
	m_locationCenter.x = 0.0f;
	m_locationCenter.y = 0.0f;
	m_locationCenter.z = 0.0f;
	m_toleranceOfLocation = 20.0f;
}

BOOL CAITraceObj::SetData(CStringA locationName,
	Vector3f position,
	float Tolerance) {
	m_NameOfLocation = locationName;
	m_locationCenter.x = position.x;
	m_locationCenter.y = position.y;
	m_locationCenter.z = position.z;
	m_toleranceOfLocation = Tolerance;
	return TRUE;
}

void CAITraceObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_NameOfLocation
		   << m_locationCenter.x
		   << m_locationCenter.y
		   << m_locationCenter.z
		   << m_toleranceOfLocation;
	} else {
		ar >> m_NameOfLocation >> m_locationCenter.x >> m_locationCenter.y >> m_locationCenter.z >> m_toleranceOfLocation;
	}
}

IMPLEMENT_SERIAL(CAITraceObj, CObject, 0)
IMPLEMENT_SERIAL(CAITraceObjList, CObList, 0)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CAITraceObj, IDS_AITRACEOBJ_NAME)
GETSET_MAX(m_NameOfLocation, gsCString, GS_FLAGS_DEFAULT, 0, 0, AITRACEOBJ, NAMEOFLOCATION, STRLEN_MAX)
GETSET_D3DVECTOR(m_locationCenter, GS_FLAGS_DEFAULT, 0, 0, AITRACEOBJ, LOCATIONCENTER)
GETSET_RANGE(m_toleranceOfLocation, gsFloat, GS_FLAGS_DEFAULT, 0, 0, AITRACEOBJ, TOLERANCEOFLOCATION, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP