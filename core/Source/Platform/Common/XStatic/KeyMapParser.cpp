///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "KeyMapParser.h"

#include "Core/Util/EnumStringMap.h"
#include "KeyCodeMap.h"
#include "LogHelper.h"
#include "Core/Util/Unicode.h"
#include <fstream>
#include <list>

static LogInstance("Instance");

namespace KEP {

// eKeyEventType string conversion
static std::pair<eKeyEventType, const char*> s_aKeyEventTypeStrings[] = {
	{ eKeyEventType::None, "None" },
	{ eKeyEventType::Press, "Press" },
	{ eKeyEventType::Release, "Release" },
	{ eKeyEventType::Click, "Click" },
	{ eKeyEventType::DoubleClick, "DoubleClick" },
};
static EnumStringMap<eKeyEventType> s_KeyEventTypeMap(s_aKeyEventTypeStrings, eKeyEventType::None, "Unknown", size_t(eKeyEventType::Count));
template <>
const EnumStringMap<eKeyEventType>& GetEnumStringMap() {
	return s_KeyEventTypeMap;
}

// eKeyStateType string conversion
std::pair<eKeyStateType, const char*> s_aKeyStateTypeStrings[] = {
	//{ eKeyStateType::Unknown, "Unknown"},
	{ eKeyStateType::Up, "Up" },
	{ eKeyStateType::Down, "Down" },
};
static EnumStringMap<eKeyStateType> s_KeyStateTypeMap(s_aKeyStateTypeStrings, eKeyStateType::Unknown, "Unknown", 2);
template <>
const EnumStringMap<eKeyStateType>& GetEnumStringMap() {
	return s_KeyStateTypeMap;
}

// eLogicalOperator string conversion
std::pair<eLogicalOperator, const char*> s_aLogicalOperatorStrings[] = {
	{ eLogicalOperator::And, "And" },
	{ eLogicalOperator::Or, "Or" },
	{ eLogicalOperator::Xor, "Xor" },
	{ eLogicalOperator::Not, "Not" },
	{ eLogicalOperator::Identity, "Identity" },
	{ eLogicalOperator::Unknown, "Unknown" },
};
static EnumStringMap<eLogicalOperator> s_LogicalOperatorStringMap(s_aLogicalOperatorStrings, eLogicalOperator::Unknown, "Unknown");
template <>
const EnumStringMap<eLogicalOperator>& GetEnumStringMap() {
	return s_LogicalOperatorStringMap;
}

// Symbols used for parsing and output
std::pair<eLogicalOperator, const char*> s_aLogicalOperatorSymbols[] = {
	{ eLogicalOperator::And, "&" },
	{ eLogicalOperator::Or, "|" },
	{ eLogicalOperator::Xor, "^" },
	{ eLogicalOperator::Not, "!" },
};
static EnumStringMap<eLogicalOperator> s_LogicalOperatorSymbolMap(s_aLogicalOperatorSymbols, eLogicalOperator::Unknown, "");

// Object holding information about a parser error.
struct ParseError {
	ParseError(std::string&& strDescription) :
			m_strDescription(std::move(strDescription)) {}
	ParseError(const std::string& strDescription) :
			m_strDescription(strDescription) {}
	ParseError(const char* pszDescription) :
			m_strDescription(pszDescription) {}

	std::string m_strDescription;
};

// Stream wrapper used for parsing.
// Its primary purpose is to keep track of line and column numbers for sensible error messages.
class ParseStream {
public:
	ParseStream(std::istream& strm) :
			m_strm(strm), m_iLine(0), m_iColumn(0), m_bNextCharacterIsOnNewLine(false) {
		m_strm.unsetf(std::ios::skipws);
	}

	// Returns 0 if no character is available.
	char GetChar() {
		char ch;
		m_strm.read(&ch, 1);
		if (!m_strm)
			return 0;

		if (m_bNextCharacterIsOnNewLine) {
			++m_iLine;
			m_iColumn = 0;
		} else {
			++m_iColumn;
		}
		m_bNextCharacterIsOnNewLine = ch == '\n';
		return ch;
	}

	explicit operator bool() const {
		return m_strm.operator bool();
	}

	char peek() {
		return m_strm.peek();
	}

	size_t GetLine() const {
		return m_iLine;
	}
	size_t GetColumn() const {
		return m_iColumn;
	}

private:
	// 0-based line and column
	size_t m_iLine;
	size_t m_iColumn;
	bool m_bNextCharacterIsOnNewLine;
	std::istream& m_strm;
};

template <typename Predicate>
bool SkipIf(ParseStream& strm, const Predicate& pred) {
	while (strm) {
		auto next = strm.peek();
		if (next == std::char_traits<char>::eof())
			return false;
		char ch = next;
		if (!pred(ch))
			break;
		strm.GetChar(); // consume the character
	}
	return true;
}

// Skip space or tab
bool SkipBlank(ParseStream& strm) {
	return SkipIf(strm, &isblank);
}

// Skip whitespace
bool SkipSpace(ParseStream& strm) {
	return SkipIf(strm, &isspace);
}

// Read up to and including next newline character.
bool SkipToNextLine(ParseStream& strm) {
	if (!strm)
		return false;

	char ch;
	do {
		ch = strm.GetChar();
		if (!strm)
			return false;
	} while (ch != '\n');

	return true;
}

// Tests the value of the next character without consuming it. Like ParseStream::peek(), but does not require testing for end-of-file.
template <typename Predicate>
bool TestNextCharacter(ParseStream& strm, const Predicate& pred) {
	while (strm) {
		auto next = strm.peek();
		if (next == std::char_traits<char>::eof())
			return false;
		char ch = next;
		return pred(ch);
	}
	return true;
}

// Tests the value of the next character without consuming it. Like ParseStream::peek(), but does not require testing for end-of-file.
bool TestNextCharacter(ParseStream& strm, char chTest) {
	return TestNextCharacter(strm, [chTest](char ch) {
		return ch == chTest;
	});
}

// Read up to 'nToRead' characters while predicate is true and not at end of file.
// Returns the characters read in optional parameter pstrRead.
// Returns the number of characters read.
template <typename Predicate>
size_t ReadCountIf(ParseStream& strm, size_t nToRead, const Predicate& pred, std::string* pstrRead = nullptr) {
	if (!strm)
		return 0;

	size_t nRead = 0;
	while (nRead < nToRead) {
		auto next = strm.peek();
		if (next == std::char_traits<char>::eof())
			break;
		char ch = next;
		if (!pred(ch))
			break;
		ch = strm.GetChar(); // consume the character
		if (strm) { // I expect this test to be redundant if peek() succeeded.
			if (pstrRead)
				*pstrRead += ch;
			++nRead;
		}
	}
	return nRead;
}

// Read while predicate is true. Optionally returns characters read.
template <typename Predicate>
bool ReadIf(ParseStream& strm, const Predicate& pred, std::string* pstrRead = nullptr) {
	return ReadCountIf(strm, SIZE_MAX, pred, pstrRead) > 0;
}

// Reads a literal string.
// Returns the number of characters read before failure. These characters are not pushed back onto the stream.
size_t ReadLiteral(ParseStream& strm, const char* pszExpected) {
	const char* pszRemaining = pszExpected;
	size_t nToRead = strlen(pszExpected);
	return ReadCountIf(strm, nToRead, [&pszRemaining](char ch) -> bool { if (ch != *pszRemaining) return false; ++pszRemaining; return true; }) == nToRead;
}

// Reads a word (string of alpha-numeric characters or underscore) and stores it in the second argument.
// Does not skip spaces before or after.
// Returns false if no word characters are available.
bool ReadWord(ParseStream& strm, Out<std::string> word) {
	return ReadIf(
		strm, [](char ch) {
			return isalnum(ch) || ch == '_';
		},
		&word.get());
}

// Skip space or comment
bool SkipSpaceOrComment(ParseStream& strm) {
	while (strm) {
		SkipSpace(strm);
		if (ReadLiteral(strm, "#") != 1)
			break;
		SkipToNextLine(strm);
	}
	return bool(strm);
}

KeyMapParser::KeyMapParser(eKeyCodeType keyCodeType) {
	m_pKeyCodeMap = GetKeyCodeMap(keyCodeType);
}

void KeyMapParser::ParseParenthesizedKeyNameList(ParseStream& strm, Out<std::vector<int>> aKeys) {
	SkipSpaceOrComment(strm);

	if (!ReadLiteral(strm, "("))
		throw ParseError("Missing opening parenthesis");

	bool bJustParsedKeyName = false; // To make sure commas only show up between key names.
	while (true) {
		SkipSpaceOrComment(strm);

		std::string strKeyName;
		if (ReadWord(strm, out(strKeyName))) {
			int iKeyCode = m_pKeyCodeMap->KeyNameToKeyCode(strKeyName.c_str());
			if (iKeyCode != -1) {
				aKeys.get().push_back(iKeyCode);
			} else {
				// Not a valid key name. It could be a key group name (e.g. "shift" instead of both "lshift" and "rshift")
				const std::vector<const char*>* papszKeyNamesInGroup = KeyGroupNameToKeyNames(strKeyName.c_str());
				if (papszKeyNamesInGroup == nullptr)
					throw ParseError("Invalid key name: " + strKeyName);

				for (const char* pszKeyNameInGroup : *papszKeyNamesInGroup) {
					int iKeyCodeInGroup = m_pKeyCodeMap->KeyNameToKeyCode(pszKeyNameInGroup);
					if (iKeyCodeInGroup == -1)
						throw ParseError(std::string("Unknown key \"") + pszKeyNameInGroup + "\" in group \"" + strKeyName);
					aKeys.get().push_back(iKeyCodeInGroup);
				}
			}

			bJustParsedKeyName = true;
		} else {
			if (ReadLiteral(strm, ")"))
				return;
			if (!bJustParsedKeyName || !ReadLiteral(strm, ","))
				throw ParseError("Unexpected character");

			bJustParsedKeyName = false;
		}
	}

	std::string strKeyName;
	if (!ReadWord(strm, out(strKeyName)))
		throw ParseError("Unexpected end of file");
}

// Negates a KeyStateCombination by applying De Morgan's law to AND and OR combinations,
// Negating the first argument of an XOR combination, and promoting the child of Identity
// and NOT combinations and negating it as necessary.
static bool NegateKeyStateCombination(KeyStateCombination* pKeyStateCombination) {
	size_t nNegateChildren = 0;
	switch (pKeyStateCombination->m_Operator) {
		case eLogicalOperator::And:
			pKeyStateCombination->m_Operator = eLogicalOperator::Or;
			nNegateChildren = SIZE_MAX;
			break;

		case eLogicalOperator::Or:
			pKeyStateCombination->m_Operator = eLogicalOperator::And;
			nNegateChildren = SIZE_MAX;
			break;

		case eLogicalOperator::Xor:
			pKeyStateCombination->m_Operator = eLogicalOperator::Xor;
			nNegateChildren = 1;
			break;

		case eLogicalOperator::Identity:
			if (pKeyStateCombination->m_aCombinations.size() != 1)
				return false; // Expected a child.
			if (!pKeyStateCombination->m_aKeyStates.empty())
				return false; // Identity key states not allowed.
			*pKeyStateCombination = pKeyStateCombination->m_aCombinations.front();
			nNegateChildren = 1;
			break;

		case eLogicalOperator::Not:
			// Promote child.
			if (pKeyStateCombination->m_aCombinations.size() != 1)
				return false; // Expected a child.
			if (!pKeyStateCombination->m_aKeyStates.empty())
				return false; // Identity key states not allowed.
			*pKeyStateCombination = pKeyStateCombination->m_aCombinations.front();
			nNegateChildren = 0;
			break;

		default:
			return false;
	}

	for (size_t i = 0; nNegateChildren > 0 && i < pKeyStateCombination->m_aCombinations.size(); ++i, --nNegateChildren)
		NegateKeyStateCombination(&pKeyStateCombination->m_aCombinations[i]);

	for (size_t i = 0; nNegateChildren > 0 && i < pKeyStateCombination->m_aKeyStates.size(); ++i, --nNegateChildren) {
		eKeyStateType& keyStateType = pKeyStateCombination->m_aKeyStates[i].m_keyStateType;
		switch (keyStateType) {
			case eKeyStateType::Up:
				keyStateType = eKeyStateType::Down;
				break;
			case eKeyStateType::Down:
				keyStateType = eKeyStateType::Up;
				break;
			default:
				return false;
		}
	}
	return true;
}

bool KeyMapParser::ParseParenthesizedKeyCombination(ParseStream& strm, Out<KeyStateCombination> keyStateCombination) {
	size_t nNegations = 0; // Collapse sequential negations into 0 or 1.
	std::list<std::pair<eLogicalOperator, KeyStateCombination>> aCombinations;

	if (!ReadLiteral(strm, "("))
		return false;

	while (true) {
		SkipSpaceOrComment(strm);

		if (ReadLiteral(strm, ")"))
			break;

		if (ReadLiteral(strm, "!")) {
			++nNegations;
			continue;
		}

		eLogicalOperator op = eLogicalOperator::Identity;
		if (ReadLiteral(strm, "&"))
			op = eLogicalOperator::And;
		else if (ReadLiteral(strm, "^"))
			op = eLogicalOperator::Xor;
		else if (ReadLiteral(strm, "|"))
			op = eLogicalOperator::Or;

		if (op != eLogicalOperator::Identity) {
			if ((aCombinations.empty() || aCombinations.back().first != eLogicalOperator::Identity) || nNegations != 0)
				throw ParseError("Misplaced binary operator");
			aCombinations.back().first = op;
		} else if (TestNextCharacter(strm, '(')) {
			KeyStateCombination childCombination;
			if (!ParseParenthesizedKeyCombination(strm, out(childCombination)))
				return false; // Empty key combination.

			if (nNegations & 1) {
				if (!NegateKeyStateCombination(&childCombination))
					throw ParseError("Could not apply negation"); // Was a new operator introduced that is not handled by NegateKeyStateCombination?
			}

			aCombinations.emplace_back(std::make_pair(eLogicalOperator::Identity, std::move(childCombination)));

			nNegations = 0;
		} else {
			std::string strKeyState;
			if (!ReadWord(strm, out(strKeyState)))
				throw ParseError("Invalid key combination specification");

			eKeyStateType keyStateType = StringToEnum<eKeyStateType>(strKeyState.c_str());
			switch (keyStateType) {
				case eKeyStateType::Down:
					if (nNegations & 1)
						keyStateType = eKeyStateType::Up;
					break;
				case eKeyStateType::Up:
					if (nNegations & 1)
						keyStateType = eKeyStateType::Down;
					break;
				default:
					throw ParseError("Unknown key state type: " + strKeyState);
			}

			std::vector<int> aKeyIds;
			ParseParenthesizedKeyNameList(strm, out(aKeyIds));

			KeyStateCombination ksc;
			for (int keyId : aKeyIds)
				ksc.m_aKeyStates.push_back({ keyId, keyStateType });
			// For key groups, if down is specified, any key in list satisfies the condition (OR).
			// If up is specified, all keys in list must satisfy the condition (AND).
			if (keyStateType == eKeyStateType::Down)
				ksc.m_Operator = eLogicalOperator::Or;
			else
				ksc.m_Operator = eLogicalOperator::And;
			aCombinations.emplace_back(std::make_pair(eLogicalOperator::Identity, std::move(ksc)));

			nNegations = 0;
		}
	}

	if (aCombinations.empty())
		return false;

	// Merge terms into a single KeyStateCombination based on operator precedence.
	// Iterator through list -
	// If the current operator is higher precedence, we delay processing until the next operator is known.
	// If the current operator is lower (or equal) precedence, we can apply the previous operator.
	auto itr_prev = aCombinations.begin();
	auto itr_curr = std::next(itr_prev);
	while (itr_curr != aCombinations.end()) {
		if (itr_prev->first <= itr_curr->first || itr_curr->first == eLogicalOperator::Identity) {
			// Operator at itr_prev has higher precedence than the one at itr_curr. Apply it.

			// Operands can be merged with the parent operator if they use the same operator.
			// And(And(a,b),And(c,d) == And(a,b,c,d)
			// And(Op(a),And(b,c)) == And(Op(a),b,c)
			// And(Op(a),Or(b)) == And(Op(a),b)  # because Or(b) == And(b) == Xor(b)
			// bFromOnLeft is to maintain ordering of operands for easier testing. When true, mergeFrom is on the left of mergeTo
			auto TryMerge = [](eLogicalOperator op, KeyStateCombination&& mergeFrom, KeyStateCombination& mergeTo, bool bFromOnLeft) -> bool {
				if (mergeTo.m_Operator != op) {
					if (!IsBinaryOperator(mergeTo.m_Operator))
						return false;
					if (mergeTo.m_aCombinations.size() + mergeTo.m_aKeyStates.size() != 1)
						return false;

					// Only a single argument to a binary operator, so we can change the operator without affecting the result.
					mergeTo.m_Operator = op;
				}

				// If the "from" combination is the same operator as "to", or it has only a single operand, its contents can be copied directly.
				if (mergeFrom.m_Operator == op || (mergeFrom.m_aCombinations.empty() && mergeFrom.m_aKeyStates.size() == 1)) {
					auto& left = bFromOnLeft ? mergeFrom : mergeTo;
					auto& right = bFromOnLeft ? mergeTo : mergeFrom;
					for (auto& comb : right.m_aCombinations)
						left.m_aCombinations.emplace_back(std::move(comb));
					for (auto& keyState : right.m_aKeyStates)
						left.m_aKeyStates.emplace_back(std::move(keyState));
					if (bFromOnLeft) {
						mergeTo.m_aCombinations = std::move(mergeFrom.m_aCombinations);
						mergeTo.m_aKeyStates = std::move(mergeFrom.m_aKeyStates);
					}
				} else {
					// Move "mergeFrom" to be a child of "mergeTo"
					auto itr_at = bFromOnLeft ? mergeTo.m_aCombinations.begin() : mergeTo.m_aCombinations.end();
					mergeTo.m_aCombinations.emplace(itr_at, std::move(mergeFrom));
				}
				return true;
			};

			if (TryMerge(itr_prev->first, std::move(itr_curr->second), itr_prev->second, false)) {
				itr_prev->first = itr_curr->first;
				aCombinations.erase(itr_curr);
			} else if (TryMerge(itr_prev->first, std::move(itr_prev->second), itr_curr->second, true)) {
				itr_prev = aCombinations.erase(itr_prev);
			} else {
				// Operands have different operators. Need to create a new parent node.
				KeyStateCombination parent;
				parent.m_Operator = itr_prev->first;
				parent.m_aCombinations.emplace_back(std::move(itr_prev->second));
				parent.m_aCombinations.emplace_back(std::move(itr_curr->second));
				itr_prev->first = itr_curr->first;
				itr_prev->second = std::move(parent);
				aCombinations.erase(itr_curr);
			}
			if (itr_prev != aCombinations.begin())
				--itr_prev;
			itr_curr = std::next(itr_prev);
		} else {
			// Binding due to precedence can not yet be determined. Advance to next term.
			itr_prev = itr_curr;
			++itr_curr;
		}
	}

	if (aCombinations.size() != 1)
		throw(ParseError("Internal error consolidating operators"));

	keyStateCombination.set(std::move(aCombinations.front().second));
	return true;
}

void KeyMapParser::ParseParenthesizedEventList(ParseStream& strm, Out<std::vector<KeyEvent>> aKeyEvents) {
	SkipSpaceOrComment(strm);

	if (!ReadLiteral(strm, "("))
		throw ParseError("Missing opening parenthesis");

	while (true) {
		SkipSpaceOrComment(strm);

		std::string strEventType;
		if (!ReadWord(strm, out(strEventType))) {
			if (!ReadLiteral(strm, ")"))
				throw ParseError("Invalid key event name or missing closing parenthesis");
			return;
		}

		eKeyEventType keyEventType = StringToEnum<eKeyEventType>(strEventType.c_str());
		if (keyEventType == eKeyEventType::None)
			throw ParseError("Unknown key event type: " + strEventType);

		std::vector<int> aKeyList;
		ParseParenthesizedKeyNameList(strm, out(aKeyList));

		// Empty list OK
		for (int keyId : aKeyList)
			aKeyEvents.get().push_back(KeyEvent{ keyId, keyEventType });
	}
}

bool KeyMapParser::ParseKeyMapping(ParseStream& strm, Out<KeyEventBindingByName> keyEventBinding) {
	if (!SkipSpaceOrComment(strm))
		return false; // No more data.

	bool bBraceEnclosed = ReadLiteral(strm, "{") == 1;

	std::string strAction;
	if (!ReadWord(strm, out(strAction)))
		throw ParseError("Unexpected end of file");

	SkipSpaceOrComment(strm);

	std::string strCondition;
	if (!ReadWord(strm, out(strCondition)))
		throw ParseError("Unexpected end of file");

	std::vector<KeyEvent> aKeyEvents;
	if (strCondition == "on") {
		ParseParenthesizedEventList(strm, out(aKeyEvents));
		SkipSpaceOrComment(strm);

		strCondition.clear();
		ReadWord(strm, out(strCondition));
	}

	KeyStateCombination keyStateCombination;
	if (strCondition == "when") {
		ParseParenthesizedKeyCombination(strm, out(keyStateCombination));
	}

	if (bBraceEnclosed) {
		SkipSpaceOrComment(strm);
		if (!ReadLiteral(strm, "}"))
			throw ParseError("Unexpected end of file");
	} else {
		SkipIf(strm, [](char ch) {
			return isspace(ch) && ch != '\n';
		});
		char ch = strm.peek();
		if (ch == '\n' || ch == '#')
			SkipToNextLine(strm);
		else if (ch != '\0')
			throw ParseError("Trailing characters on line");
	}

	keyEventBinding.get().m_strActionName = std::move(strAction);
	keyEventBinding.get().m_BindableKeyEvent.m_KeyCombination = std::move(keyStateCombination);
	keyEventBinding.get().m_BindableKeyEvent.m_aKeyEvents = std::move(aKeyEvents);

	SkipSpaceOrComment(strm);

	return true;
}

bool KeyMapParser::Parse(std::istream& istrm, Out<std::vector<KeyEventBindingByName>> aKeyEventBindings) {
	if (m_pKeyCodeMap == nullptr)
		return false;

	if (!istrm)
		return false;

	ParseStream parseStream(istrm);
	while (true) {
		KeyEventBindingByName keyEventBinding;
		try {
			if (!ParseKeyMapping(parseStream, out(keyEventBinding)))
				break;
		} catch (ParseError& parseError) {
			LogError("Keymap parse error on line " << parseStream.GetLine() << " and column " << parseStream.GetColumn() << ": " << parseError.m_strDescription);
			SkipToNextLine(parseStream);
			if (!parseStream)
				break;
		}
		LogInfo("Applying a mapping for action " << keyEventBinding.m_strActionName);
		aKeyEventBindings.get().emplace_back(std::move(keyEventBinding));
	}

	return true;
}

bool ParseKeyMappings(std::istream& istrm, eKeyCodeType keyCodeType, Out<std::vector<KeyEventBindingByName>> aKeyEventBindings) {
	KeyMapParser kmp(keyCodeType);
	return kmp.Parse(istrm, aKeyEventBindings);
}

bool PrintKeyMappings(std::ostream& ostrm, eKeyCodeType keyCodeType, const std::vector<KeyEventBindingByName>& aKeyEventBindings) {
	struct Local {
		static void WriteKeyName(std::ostream& ostrm, const KeyCodeMap* pKeyCodeMap, int iKeyId) {
			if (pKeyCodeMap)
				ostrm << pKeyCodeMap->KeyCodeToKeyName(iKeyId);
			else
				ostrm << iKeyId;
		}
		static bool WriteCondition(std::ostream& ostrm, const KeyCodeMap* pKeyCodeMap, const KeyStateCombination& keyStateCombination) {
			eLogicalOperator op = keyStateCombination.m_Operator;
			bool bFirstOperand = true;
			auto OutputOperator = [op, &bFirstOperand, &ostrm]() -> bool {
				if (!IsBinaryOperator(op) || !bFirstOperand) {
					const char* pszOperator = s_LogicalOperatorSymbolMap.LookUp(op);
					if (!pszOperator)
						return false;
					ostrm << " " << pszOperator << " ";
				}
				bFirstOperand = false;
				return true;
			};

			for (const KeyState& keyState : keyStateCombination.m_aKeyStates) {
				if (!OutputOperator())
					return false;
				std::string strKeyState = EnumToString(keyState.m_keyStateType);
				strKeyState[0] = tolower(strKeyState[0]);
				ostrm << strKeyState << "(";
				WriteKeyName(ostrm, pKeyCodeMap, keyState.m_iKeyId);
				ostrm << ")";
			}

			for (const KeyStateCombination& comb : keyStateCombination.m_aCombinations) {
				if (!OutputOperator())
					return false;
				ostrm << "(";
				WriteCondition(ostrm, pKeyCodeMap, comb);
				ostrm << ")";
			}
			return true;
		}
	};

	const KeyCodeMap* pKeyCodeMap = GetKeyCodeMap(keyCodeType);

	for (const auto& keb : aKeyEventBindings) {
		ostrm << keb.m_strActionName;
		ostrm << " on(";
		if (keb.m_BindableKeyEvent.m_aKeyEvents.empty()) {
			//ostrm << " [None]";
		} else {
			for (const auto& keyEvent : keb.m_BindableKeyEvent.m_aKeyEvents) {
				ostrm << " ";
				Local::WriteKeyName(ostrm, pKeyCodeMap, keyEvent.m_iKeyId);
				ostrm << " " << EnumToString(keyEvent.m_KeyEventType);
			}
		}
		ostrm << ") ";
		ostrm << "when(";
		Local::WriteCondition(ostrm, pKeyCodeMap, keb.m_BindableKeyEvent.m_KeyCombination);
		ostrm << ")\n";
	}
	return bool(ostrm);
}

// Sample file format
// car_left on() when((down(a) & up(shift)) | down(leftarrow)) # Always turn left when 'a' key is down and shift is up, or when left arrow key is down.
// car_right on() when(down(d) & !down(shift)) # Always turn right when 'd' key is down and shift is not down
// car_exit on(press(x)) when(!down(shift) & !down(control) & !down(alt)) # exit car when x is pressed down
// car_forward on() when(down(w))
// car_brake on() when(down(space))

} // namespace KEP
