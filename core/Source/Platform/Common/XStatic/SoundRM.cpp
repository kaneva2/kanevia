///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "SoundRM.h"
#include "ContentService.h"
#include "DownloadManager.h"
#include "OALSoundProxy.h"
#include "IClientEngine.h"
#include "Event/Base/IDispatcher.h"
#include "LogHelper.h"

static LogInstance("Instance");

namespace KEP {

OALSoundProxy* SoundRM::getProxy(const GLID& glid, bool bCreateIfNotExist) {
	// Find Existing Sound Proxy ?
	for (const auto& pSP : m_proxies) {
		if (!pSP)
			continue;
		if (pSP->getGlid() == glid)
			return pSP;
	}

	// Create New Sound Proxy If Not Existing
	OALSoundProxy* pSP = NULL;
	if (bCreateIfNotExist) {
		pSP = new OALSoundProxy(this, glid);
		m_proxies.push_back(pSP);
	}

	return pSP;
}

OALSoundProxy* SoundRM::addUGCProxy(const GLID& localGlid, BYTE* theBuff, FileSize theSize) {
	OALSoundProxy* newProxy = new OALSoundProxy(this, localGlid, theBuff, theSize);
	m_proxies.push_back(newProxy);
	return newProxy;
}

std::string SoundRM::GetResourceLocalStore(const std::string& gameFilesDir) {
	std::string soundDir = IResource::mBaseSoundPath[0];
	FileHelper::CreateFolderDeep(soundDir);
	return soundDir;
}

std::string SoundRM::GetResourceFileName(UINT64 hash) {
	CStringA fileName;
	fileName.Format("s%010I64u.krx", hash);
	return fileName.GetString();
}

std::string SoundRM::GetResourceFilePath(UINT64 hash) {
	return PathAdd(IResource::mBaseSoundPath[0], GetResourceFileName(hash));
}

EVENT_PROC_RC SoundRM::ProcessSoundDataEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	IResource* pRes = (IResource*)e->ObjectId();
	if (pRes && pRes->GetResourceManager()) {
		auto pRM = dynamic_cast<SoundRM*>(pRes->GetResourceManager());
		if (pRM)
			pRM->HandleSoundDataEvent(lparam, disp, e);
	}

	return EVENT_PROC_RC::OK;
}

void SoundRM::HandleSoundDataEvent(ULONG lparam, IDispatcher* disp, IEvent* e) {
	IResource* pRes = (IResource*)e->ObjectId();

	// extract name and count
	std::string resultText;
	int code = 0;
	(*e->InBuffer()) >> resultText >> code;

	if (resultText.size() > 0)
		ContentService::Instance()->ContentMetadataXmlUnpackToCache(resultText);

	this->QueueResource(pRes);
}

} // namespace KEP
