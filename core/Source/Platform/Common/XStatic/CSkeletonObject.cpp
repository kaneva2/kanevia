///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "resource.h"
#include <d3d9.h>
#include <direct.h>
#include <afxtempl.h>
#include "matrixarb.h"
#include "math.h"
#include <SerializeHelper.h>
#include <stdlib.h>

#include "ServerSerialize.h"
#include "CSkeletonObject.h"
#include "CMovementObj.h"
#include "ReSkinMesh.h"
#include "CSkeletonObject.h"
#include "CBoneClass.h"
#include "CDamagePathClass.h"
#include "CDeformableMesh.h"
#include "CParticleEmitRange.h"
#include "CShadowClass.h"
#include "CSphereCollisionClass.h"
#include "CFireRangeClass.h"

#ifdef _ClientOutput
#include "CBoneAnimationClass.h"
#include "CBoneClass.h"
#include "CDamagePathClass.h"
#include "CDeformableMesh.h"
#include "CFireRangeClass.h"
#include "CParticleEmitRange.h"
#include "CShadowClass.h"
#include "CSphereCollisionClass.h"
#include "ReMeshCreate.h"
#include "SkeletalAnimationManager.h"
#include "TextureManager.h"
#include "CSkeletonAnimation.h"
#include "AnimationProxy.h"
#include "MeshProxy.h"
#include "../../RenderEngine/Utils/ReUtils.h"
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "ReMeshCache.h"
#include "DownloadPriority.h"
#include "ReMeshCreate.h"
#endif

#include "Intersect.h"
#include "AnimationProxy.h"
#include "UgcConstants.h"
#include "BoundBox.h"
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "common/include/IMemSizeGadget.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

CSkeletonObject::~CSkeletonObject() {
	delete m_pSkeletonConfiguration;
}

void CSkeletonObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		assert(m_pSkeletonConfiguration != nullptr);
		m_pRuntimeSkeleton->writeLegacySkeletonObject(ar, m_pSkeletonConfiguration);
	} else {
		m_pRuntimeSkeleton = RuntimeSkeleton::New();
		m_pSkeletonConfiguration = new SkeletonConfiguration();
		m_pRuntimeSkeleton->readLegacySkeletonObject(ar, m_pSkeletonConfiguration);
	}
}

void CSkeletonObject::Clone(CSkeletonObject** clone,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int* spawnCfgOptional,
	int* matCfgOptional,
	D3DCOLORVALUE* spawnRGB,
	bool fullClone) {
	CSkeletonObject* tmp = new CSkeletonObject();
	m_pRuntimeSkeleton->Clone(&tmp->m_pRuntimeSkeleton, g_pd3dDevice, spawnCfgOptional, matCfgOptional, spawnRGB, fullClone, m_pSkeletonConfiguration->m_meshSlots == NULL);
	m_pSkeletonConfiguration->Clone(&tmp->m_pSkeletonConfiguration, m_pRuntimeSkeleton.get(), spawnCfgOptional, matCfgOptional, spawnRGB, fullClone);
	*clone = tmp;
}

void CSkeletonObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pRuntimeSkeleton);
		pMemSizeGadget->AddObject(m_pSkeletonConfiguration);
	}
}

IMPLEMENT_SERIAL_SCHEMA(CSkeletonObject, CObject)

} // namespace KEP
