///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS

#include "resource.h"
#include <afxtempl.h>

#include "matrixarb.h"
#include "Core/Math/Intersect.h"
#include "Core/Math/Angle.h"

#include "common\KEPUtil\Helpers.h"

// DRF - Angles Bug Fix
// Legacy code always had a bug in using angles that are all 90 degrees off.  Enabling this fixes the bug
// but you must also update CommonFunctions::g_PI to be 0 for compatibility with old scripts.
#define DRF_NEW_ANGLES 0 ///< legacy MatrixArb angle fix (0=off, 1=on)

#define N_BITS 32
#define MAX_BIT ((N_BITS + 1) / 2 - 1)

namespace KEP {

Vector3f MatrixARB::RotateY_WLD(float& Ya, Vector3f& curPos) {
	Matrix44f m_tempMatGLB;
	Vector3f m_tempXYZ;
	float m_t1;
	float m_t2;
	float m_t3;

	// Generate 3D rotation matrix:

	m_t1 = ToRadians(Ya);

	m_t2 = (float)cos(m_t1); //cy
	m_t3 = (float)sin(m_t1); //sy

	// Initialize Y rotation matrix:
	m_tempMatGLB[0][0] = m_t2;
	m_tempMatGLB[0][1] = 0;
	m_tempMatGLB[0][2] = -m_t3;
	m_tempMatGLB[0][3] = 0;
	m_tempMatGLB[1][0] = 0;
	m_tempMatGLB[1][1] = 1;
	m_tempMatGLB[1][2] = 0;
	m_tempMatGLB[1][3] = 0;
	m_tempMatGLB[2][0] = m_t3;
	m_tempMatGLB[2][1] = 0;
	m_tempMatGLB[2][2] = m_t2;
	m_tempMatGLB[2][3] = 0;
	m_tempMatGLB[3][0] = 0;
	m_tempMatGLB[3][1] = 0;
	m_tempMatGLB[3][2] = 0;
	m_tempMatGLB[3][3] = 1;

	//output vertex
	m_tempXYZ.x = (m_tempMatGLB[0][0] * curPos.x) + (m_tempMatGLB[1][0] * curPos.y) + (m_tempMatGLB[2][0] * curPos.z);
	m_tempXYZ.y = (m_tempMatGLB[0][1] * curPos.x) + (m_tempMatGLB[1][1] * curPos.y) + (m_tempMatGLB[2][1] * curPos.z);
	m_tempXYZ.z = (m_tempMatGLB[0][2] * curPos.x) + (m_tempMatGLB[1][2] * curPos.y) + (m_tempMatGLB[2][2] * curPos.z);

	return m_tempXYZ;
}

Vector3f MatrixARB::GetLocalPositionFromMatrixView2(const Matrix44f& invMatrix, const Matrix44f& matrixLoc, const Vector3f& objVect) {
	Vector3f zeroedPos = objVect - matrixLoc.GetTranslation();
	return TransformVector(invMatrix, zeroedPos);
}

Vector3f MatrixARB::GetLocalPositionFromMatrixView(const Matrix44f& matrixLoc, const Vector3f& objVect) {
	Matrix44f invMatrix;
	invMatrix.MakeInverseOf(matrixLoc);
	return GetLocalPositionFromMatrixView2(invMatrix, matrixLoc, objVect);
}

void MatrixARB::FaceTowardPoint(Vector3f& targetVect, Matrix44f* inputMatrix) {
	targetVect.y += .001f;

	float opp = 0;
	float adj = 0;
	float yaw = 0;
	float pitch = 0;

	Vector3f thisVect = inputMatrix->GetTranslation();
	opp = thisVect.z - targetVect.z;
	adj = thisVect.x - targetVect.x;
	yaw = atan2(adj, opp);

	adj = thisVect.y - targetVect.y;

	opp = sqrt((targetVect.x - thisVect.x) * (targetVect.x - thisVect.x) +
			   (targetVect.z - thisVect.z) * (targetVect.z - thisVect.z));
	pitch = atan2(adj, opp);
	yaw = -yaw;
	pitch = pitch;

	float cosyaw = cos(yaw);
	float sinyaw = sin(yaw);
	float cospitch = cos(pitch);
	float sinpitch = sin(pitch);
	if (yaw && pitch) {
		(*inputMatrix)(0, 0) = cosyaw;
		(*inputMatrix)(1, 0) = sinpitch * sinyaw;
		(*inputMatrix)(2, 0) = -cospitch * sinyaw;
		(*inputMatrix)(1, 1) = cospitch;
		(*inputMatrix)(2, 1) = sinpitch;
		(*inputMatrix)(0, 2) = sinyaw;
		(*inputMatrix)(1, 2) = -sinpitch * cosyaw;
		(*inputMatrix)(2, 2) = cospitch * cosyaw;

	} else if (yaw) {
		(*inputMatrix)(0, 0) = cosyaw;
		(*inputMatrix)(0, 2) = -sinyaw;
		(*inputMatrix)(2, 0) = sinyaw;
		(*inputMatrix)(2, 2) = cosyaw;
	} else if (pitch) {
		(*inputMatrix)(1, 1) = cospitch;
		(*inputMatrix)(2, 1) = sinpitch;
		(*inputMatrix)(1, 2) = -sinpitch;
		(*inputMatrix)(2, 2) = cospitch;
	}
}

void MatrixARB::GetOrientation(const Matrix44f& input, Vector3f* dirVect, Vector3f* upVect) {
	Vector3f upVectLoc, dirVectLoc;
	upVectLoc.x = 0.0f;
	upVectLoc.y = 1.0f;
	upVectLoc.z = 0.0f;
	dirVectLoc.x = 0.0f;
	dirVectLoc.y = 0.0f;
	dirVectLoc.z = 1.0f;
	upVectLoc = TransformVector(input, upVectLoc); //get position in world space
	dirVectLoc = TransformVector(input, dirVectLoc); //get position in world space
	*upVect = upVectLoc;
	*dirVect = dirVectLoc;
}

void MatrixARB::SetOrientation(Matrix44f* matrixInOut,
	const Vector3f& dirVect,
	const Vector3f& upVect) {
	(*matrixInOut)(0, 0) = upVect.y * dirVect.z - upVect.z * dirVect.y;
	(*matrixInOut)(0, 1) = upVect.z * dirVect.x - upVect.x * dirVect.z;
	(*matrixInOut)(0, 2) = upVect.x * dirVect.y - upVect.y * dirVect.x;
	(*matrixInOut)(0, 3) = 0.f;

	(*matrixInOut)(1, 0) = upVect.x;
	(*matrixInOut)(1, 1) = upVect.y;
	(*matrixInOut)(1, 2) = upVect.z;
	(*matrixInOut)(1, 3) = 0.f;

	(*matrixInOut)(2, 0) = dirVect.x;
	(*matrixInOut)(2, 1) = dirVect.y;
	(*matrixInOut)(2, 2) = dirVect.z;
	(*matrixInOut)(2, 3) = 0.f;

	(*matrixInOut)(3, 3) = 1.0;
}

Vector3f MatrixARB::InterpolatePointsHiRes(const Vector3f& StartVec,
	const Vector3f& EndVec,
	float NumOfStages,
	float ThisStage) {
	Vector3f StageVec;
	float propfactor;
	if (NumOfStages <= 0)
		NumOfStages = 1;
	propfactor = (float)ThisStage / NumOfStages;
	StageVec.x = StartVec.x + propfactor * (EndVec.x - StartVec.x);
	StageVec.y = StartVec.y + propfactor * (EndVec.y - StartVec.y);
	StageVec.z = StartVec.z + propfactor * (EndVec.z - StartVec.z);
	return StageVec;
}

Vector3f MatrixARB::InterpolatePoints(const Vector3f& StartVec,
	const Vector3f& EndVec,
	TimeMs NumOfStages,
	TimeMs ThisStage) {
	Vector3f StageVec;
	float propfactor;
	if (NumOfStages <= 0)
		NumOfStages = 1;
	propfactor = ThisStage / NumOfStages;
	StageVec.x = StartVec.x + propfactor * (EndVec.x - StartVec.x);
	StageVec.y = StartVec.y + propfactor * (EndVec.y - StartVec.y);
	StageVec.z = StartVec.z + propfactor * (EndVec.z - StartVec.z);
	return StageVec;
}

Vector3f MatrixARB::InterpolatePoints2(
	const Vector3f& StartVec,
	const Vector3f& EndVec,
	TimeMs NumOfStages,
	TimeMs ThisStage) {
	Vector3f StageVec;
	float propfactor;
	if (NumOfStages <= 0)
		NumOfStages = 1;
	propfactor = ThisStage / NumOfStages;
	if (propfactor <= 0) {
		StageVec = EndVec;
	} else {
		StageVec.x = StartVec.x + propfactor * (EndVec.x - StartVec.x);
		StageVec.y = StartVec.y + propfactor * (EndVec.y - StartVec.y);
		StageVec.z = StartVec.z + propfactor * (EndVec.z - StartVec.z);
	}
	return StageVec;
}

BOOL MatrixARB::CollisionSphereSeg(const Vector3f& startPoint,
	const Vector3f& endPoint,
	float sphereRadius,
	const Vector3f& center,
	BOOL getAdjustInfo,
	Vector3f* adjust,
	float objectRadius,
	Vector3f* intersection) {
	Matrix44f mainMatrix;
	mainMatrix.MakeIdentity();
	Vector3f axis = endPoint - startPoint;
	if (axis.x == 0.0f && axis.y == 0.0f && axis.z == 0.0f)
		return FALSE;

	axis.Normalize();
	FaceTowardPoint(axis, &mainMatrix);
	mainMatrix.GetTranslation() = startPoint;
	Vector3f inSpacePoint;
	inSpacePoint = GetLocalPositionFromMatrixView(mainMatrix, center);
	float radiusCurrent = Distance(Vector2f(inSpacePoint.x, inSpacePoint.y), Vector2f(0, 0));
	if (radiusCurrent > (sphereRadius + objectRadius))
		return FALSE; //out of radius
	//check distance
	if (inSpacePoint.z > (sphereRadius + objectRadius))
		return FALSE; //too far behind-remember missiles fly foward in negative direction

	float distance = Distance(startPoint, endPoint);

	if ((distance + (sphereRadius + objectRadius)) < -inSpacePoint.z)
		return FALSE;

	if (intersection) {
		float distanceToTarget = Distance(startPoint, center);
		*intersection = startPoint + axis * distanceToTarget;
	}

	if (getAdjustInfo) { //find adjust movement
		Vector3f pushVect;
		float mDist = Distance(startPoint, center);
		pushVect.x = startPoint.x - center.x;
		pushVect.y = startPoint.y - center.y;
		pushVect.z = startPoint.z - center.z;
		if (pushVect.x == 0.0f &&
			pushVect.y == 0.0f &&
			pushVect.z == 0.0f) {
			adjust->x = 0.0f;
			adjust->y = 0.0f;
			adjust->z = 0.0f;
		} else {
			pushVect.Normalize();
			mDist = ((sphereRadius + objectRadius) - mDist) * 1.1f; //
			if (mDist > 0) {
				adjust->x = pushVect.x * mDist;
				adjust->y = pushVect.y * mDist;
				adjust->z = pushVect.z * mDist;
			} else {
				adjust->x = 0.0f;
				adjust->y = 0.0f;
				adjust->z = 0.0f;
			}
		}
	} //end find adjust movement

	return TRUE;
}

void MatrixARB::Transpose(float q[4][4], const float a[4][4]) {
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			q[j][i] = a[i][j];
}

void MatrixARB::Invert(float q[4][4], const float a[4][4]) {
	FLOAT fDetInv = 1.0f / (a[0][0] * (a[1][1] * a[2][2] - a[1][2] * a[2][1]) -
							   a[0][1] * (a[1][0] * a[2][2] - a[1][2] * a[2][0]) +
							   a[0][2] * (a[1][0] * a[2][1] - a[1][1] * a[2][0]));

	q[0][0] = fDetInv * (a[1][1] * a[2][2] - a[1][2] * a[2][1]);
	q[0][1] = -fDetInv * (a[0][1] * a[2][2] - a[0][2] * a[2][1]);
	q[0][2] = fDetInv * (a[0][1] * a[1][2] - a[0][2] * a[1][1]);
	q[0][3] = 0.0f;

	q[1][0] = -fDetInv * (a[1][0] * a[2][2] - a[1][2] * a[2][0]);
	q[1][1] = fDetInv * (a[0][0] * a[2][2] - a[0][2] * a[2][0]);
	q[1][2] = -fDetInv * (a[0][0] * a[1][2] - a[0][2] * a[1][0]);
	q[1][3] = 0.0f;

	q[2][0] = fDetInv * (a[1][0] * a[2][1] - a[1][1] * a[2][0]);
	q[2][1] = -fDetInv * (a[0][0] * a[2][1] - a[0][1] * a[2][0]);
	q[2][2] = fDetInv * (a[0][0] * a[1][1] - a[0][1] * a[1][0]);
	q[2][3] = 0.0f;

	q[3][0] = -(a[3][0] * q[0][0] + a[3][1] * q[1][0] + a[3][2] * q[2][0]);
	q[3][1] = -(a[3][0] * q[0][1] + a[3][1] * q[1][1] + a[3][2] * q[2][1]);
	q[3][2] = -(a[3][0] * q[0][2] + a[3][1] * q[1][2] + a[3][2] * q[2][2]);
	q[3][3] = 1.0f;
}

void MatrixARB::Invert2(Matrix44f& q, const Matrix44f& a) {
	// first transpose the rotation matrix
	q[0][0] = a[0][0];
	q[0][1] = a[1][0];
	q[0][2] = a[2][0];
	q[1][0] = a[0][1];
	q[1][1] = a[1][1];
	q[1][2] = a[2][1];
	q[2][0] = a[0][2];
	q[2][1] = a[1][2];
	q[2][2] = a[2][2];

	// fix right column
	q[0][3] = 0;
	q[1][3] = 0;
	q[2][3] = 0;
	q[3][3] = 1;

	// now get the new translation vector
	Vector3f temp;
	temp.x = a[3][0];
	temp.y = a[3][1];
	temp.z = a[3][2];

	q[3][0] = -(temp.x * a[0][0] + temp.y * a[0][1] + temp.z * a[0][2]);
	q[3][1] = -(temp.x * a[1][0] + temp.y * a[1][1] + temp.z * a[1][2]);
	q[3][2] = -(temp.x * a[2][0] + temp.y * a[2][1] + temp.z * a[2][2]);
}

void MatrixARB::Transform(float q[3], const float a[3], const float m[4][4]) {
	q[0] = a[0] * m[0][0] + a[1] * m[1][0] + a[2] * m[2][0] + m[3][0];
	q[1] = a[0] * m[0][1] + a[1] * m[1][1] + a[2] * m[2][1] + m[3][1];
	q[2] = a[0] * m[0][2] + a[1] * m[1][2] + a[2] * m[2][2] + m[3][2];
}

void MatrixARB::Transpose(Matrix44f& q, const Matrix44f& a) {
	q.MakeTransposeOf(a);
}

void MatrixARB::Invert(Matrix44f& q, const Matrix44f& a) {
	q.MakeInverseOf(a);
}

float MatrixARB::InterpolateFloats(
	float StartVec,
	float EndVec,
	int NumOfStages,
	int ThisStage) {
	float propfactor;
	if (NumOfStages <= 0)
		NumOfStages = 1;
	propfactor = (float)ThisStage / NumOfStages;
	float StageVec = StartVec + propfactor * (EndVec - StartVec);

	return StageVec;
}

float MatrixARB::InterpolateFloatsHiRes(
	float StartVec,
	float EndVec,
	float NumOfStages,
	float ThisStage) {
	float propfactor;
	if (NumOfStages <= 0)
		NumOfStages = 1;
	propfactor = ThisStage / NumOfStages;
	float StageVec = StartVec + propfactor * (EndVec - StartVec);

	return StageVec;
}

BOOL MatrixARB::BoundingSphereCheck(float radius1, float radius2, float x1, float y1, float z1,
	float x2, float y2, float z2) {
	float MinDist, dist;
	MinDist = radius1 + radius2;
	MinDist = MinDist * MinDist;
	dist = (x1 - x2) * (x1 - x2);
	if (dist <= MinDist) {
		dist = dist + (y1 - y2) * (y1 - y2);
		if (dist <= MinDist) {
			dist = dist + (z1 - z2) * (z1 - z2);
			if (dist <= MinDist)
				return TRUE;
		}
	}
	return FALSE;
}

Matrix44f MatrixARB::AxisAngleRotationV2(const Vector3f& axis, float angle) {
	float s = (float)sin(angle);
	float c = (float)cos(angle);
	float x = axis.x, y = axis.y, z = axis.z;

	Matrix44f matrixLocal;

	matrixLocal(0, 0) = x * x * (1 - c) + c;
	matrixLocal(1, 0) = x * y * (1 - c) - (z * s);
	matrixLocal(2, 0) = x * z * (1 - c) + (y * s);
	matrixLocal(3, 0) = 0;
	matrixLocal(0, 1) = y * x * (1 - c) + (z * s);
	matrixLocal(1, 1) = y * y * (1 - c) + c;
	matrixLocal(2, 1) = y * z * (1 - c) - (x * s);
	matrixLocal(3, 1) = 0;
	matrixLocal(0, 2) = z * x * (1 - c) - (y * s);
	matrixLocal(1, 2) = z * y * (1 - c) + (x * s);
	matrixLocal(2, 2) = z * z * (1 - c) + c;
	matrixLocal(3, 2) = 0;
	matrixLocal(0, 3) = 0;
	matrixLocal(1, 3) = 0;
	matrixLocal(2, 3) = 0;
	matrixLocal(3, 3) = 1;

	return matrixLocal;
}

// Replace legacy implementation with D3DX API call which is at least one order of magnitude faster.
// D3DX API also take cares of the "edge" condition (literally) where the old one fails.
BOOL MatrixARB::SegPolyCheck(const Triangle& polygon,
	const Vector3f& startPoint,
	const Vector3f& endPoint,
	Vector3f* intersectionPoint) {
	Matrix44f polyspace;
	polyspace.MakeIdentity();

	Vector3f directionalVect, directionalVect2;
	directionalVect.x = polygon[1].x - polygon[0].x;
	directionalVect.y = polygon[1].y - polygon[0].y;
	directionalVect.z = polygon[1].z - polygon[0].z;

	directionalVect2.x = polygon[2].x - polygon[0].x;
	directionalVect2.y = polygon[2].y - polygon[0].y;
	directionalVect2.z = polygon[2].z - polygon[0].z;

	Vector3f polySpaceUpVector = Cross(directionalVect, directionalVect2).GetNormalized();
	Vector3f directionalVector = directionalVect.GetNormalized();

	SetOrientation(&polyspace, directionalVector, polySpaceUpVector);

	Matrix44f invMatrix;
	polyspace.GetTranslation() = polygon[0];
	Invert2(invMatrix, polyspace);

	Vector3f localStartPoint = GetLocalPositionFromMatrixView2(invMatrix, polyspace, startPoint);
	Vector3f localEndPoint = GetLocalPositionFromMatrixView2(invMatrix, polyspace, endPoint);

	if (localStartPoint.y > 0.0f && localEndPoint.y > 0.0f)
		return FALSE;

	if (localStartPoint.y <= 0.0f && localEndPoint.y <= 0.0f)
		return FALSE;

	Vector3f localPolygon[3];
	localPolygon[0] = GetLocalPositionFromMatrixView2(invMatrix, polyspace, polygon[0]);
	localPolygon[1] = GetLocalPositionFromMatrixView2(invMatrix, polyspace, polygon[1]);
	localPolygon[2] = GetLocalPositionFromMatrixView2(invMatrix, polyspace, polygon[2]);

	float segDistance = fabs(localStartPoint.y) + fabs(localEndPoint.y);

	Vector3f intersectLocal = InterpolatePointsHiRes(localStartPoint,
		localEndPoint,
		segDistance,
		fabs(localStartPoint.y));

	*intersectionPoint = TransformPoint(polyspace, intersectLocal);

	if (PointInPolyXZ(intersectLocal, localPolygon))
		return TRUE;
	return FALSE;
}

BOOL MatrixARB::PointInPolyXZ(const Vector3f& point,
	const Triangle& polyGon) {
	int intersectionCount = 0;
	//segment 1   poly points 0 1
	for (int loop = 0; loop < 3; loop++) { //loop
		int positionOne = loop;
		int positionTwo = loop + 1;
		if (positionTwo == 3)
			positionTwo = 0;

		if (Between(point.z, polyGon[positionOne].z, polyGon[positionTwo].z)) { //possible
			if (polyGon[positionOne].x > point.x || polyGon[positionTwo].x > point.x) { //possible 2
				if (polyGon[positionOne].x > point.x && polyGon[positionTwo].x > point.x)
					intersectionCount++; //definite intersection
				else { //aquire greater value and resolve intersection
					if (polyGon[positionOne].z > polyGon[positionTwo].z) { //1
						float range = polyGon[positionOne].z - polyGon[positionTwo].z;
						float position = polyGon[positionOne].z - point.z;
						float x = InterpolateFloatsHiRes(
							polyGon[positionOne].x,
							polyGon[positionTwo].x,
							range,
							position);
						if (point.x < x)
							intersectionCount++;
					} //end 1
					else { //2
						float range = polyGon[positionTwo].z - polyGon[positionOne].z;
						float position = polyGon[positionTwo].z - point.z;
						float x = InterpolateFloatsHiRes(
							polyGon[positionTwo].x,
							polyGon[positionOne].x,
							range,
							position);
						if (point.x < x)
							intersectionCount++;
					} //end 2
				} //aquire greater value and resolve intersection
			} //end possible 2
		} //end possible
	} //end loop

	if (intersectionCount == 1)
		return TRUE;

	return FALSE;
}

BOOL MatrixARB::Between(float val, float h1, float h2) {
	if (val < h1 && val > h2)
		return TRUE;

	if (val > h1 && val < h2)
		return TRUE;

	return FALSE;
}

BOOL MatrixARB::GetSubDistance(const Triangle& polyGon,
	const Vector3f& uniNormal,
	const Vector3f& point,
	float* distanceToMoveAlongNormal) {
	Matrix44f polyspace;
	polyspace.MakeIdentity();

	Vector3f polySpaceDirVector = polyGon[1] - polyGon[0];

	Vector3f polySpaceUpVector(uniNormal);
	polySpaceUpVector.Normalize();
	polySpaceDirVector.Normalize();

	SetOrientation(&polyspace, polySpaceDirVector, polySpaceUpVector);

	Matrix44f invMatrix;
	polyspace.GetTranslation() = polyGon[0];
	Invert2(invMatrix, polyspace);

	Vector3f localPoint = GetLocalPositionFromMatrixView2(invMatrix, polyspace, point);

	*distanceToMoveAlongNormal = fabs(localPoint.y);

	if (localPoint.y >= 0.0f)
		return FALSE;

	return TRUE;
}

void MatrixARB::RotationMatrixToYRot(const Matrix44f& rotationMatrix, float& xAngle, float& yAngle, float& zAngle) {
	// Platform Does Not Support X or Z Angles
	xAngle = 0.0f;
	zAngle = 0.0f;

	// DRF - Offset By PI/2 Is A Bug That Needs Fixed Once Game Objects Recognize DRF_NEW_ANGLES!
#if (DRF_NEW_ANGLES == 1)
	yAngle = RadNorm(atan2(rotationMatrix._31, rotationMatrix._33));
#else
	yAngle = RadNorm(atan2(rotationMatrix(2, 0), rotationMatrix(2, 2)) - (M_PI / 2.0));
#endif
}

} // namespace KEP
