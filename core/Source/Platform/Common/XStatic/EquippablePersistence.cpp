///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "EquippableSystem.h"

#include "CGlobalInventoryClass.h"
#include <direct.h>
#include "config.h"
#include "MaterialPersistence.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "ResourceManagers/ReMeshCreate.h"
#include "IClientEngine.h"
#include "LogHelper.h"
#include "Core/Crypto/Crypto_KRX.h"

static LogInstance("Instance");

// Some arbitrary soft limits for performance warnings
#define NUM_BONES_LIMIT 99
#define NUM_RIGID_MESHES_LIMIT 99
#define NUM_SKINNED_MESHES_LIMIT 99

namespace KEP {

BYTE* CArmedInventoryProxy::GetSerializedData(FileSize& dataLen) {
	CItemObj* dummy = new CItemObj();
	dummy->m_globalID = GetHashKey();
	if (!dummy->m_attachmentRefList)
		dummy->m_attachmentRefList = new CAttachmentRefList();
	dummy->m_attachmentRefList->AddTail(new CAttachmentRefObj(GetHashKey()));
	BYTE* result = EquippableRM::Instance()->prepareEquippableObjectData(dummy, false, dataLen, 1.0f);
	delete dummy; // CItemObj::SafeDelete now merged with destructor
	return result;
}

BYTE* EquippableRM::prepareEquippableObjectData(CItemObj* item, bool bEncrypt, FileSize& buffLen, float scaleFactor) const {
	BYTE* rawBuff = NULL;
	jsVerifyReturn(item != NULL, NULL);

	CMemFile memFile;
	CArchive eqp_obj_archive(&memFile, CArchive::store);
	std::string fileName = EquippableRM::Instance()->GetResourceFileName((UINT64)item->m_globalID);
	eqp_obj_archive.m_strFileName = Utf8ToUtf16(fileName).c_str();

	POSITION pos = item->m_attachmentRefList->GetHeadPosition();
	int equipId = ((CAttachmentRefObj*)item->m_attachmentRefList->GetNext(pos))->m_attachmentType;
	CArmedInventoryObj* obj = EquippableRM::Instance()->GetProxy(equipId, true)->GetData();

	if (!obj) {
		LogError("Item:" << item->m_globalID << " references missind APD data index:" << equipId);
		return NULL;
	}

	EquippableRM::Instance()->WriteEquippableObject(obj, item->m_globalID, eqp_obj_archive);
	eqp_obj_archive.Close();
	memFile.Flush();
	buffLen = (FileSize)memFile.GetLength();

	// make File Size evenly divisible by 128 bits for encryption
	int remainder = buffLen % 16;
	if (remainder > 0) {
		char padding[16];
		memset(&padding, 0, 16 - remainder);
		memFile.Write(&padding, 16 - remainder);
	}
	buffLen = (FileSize)memFile.GetLength();

	rawBuff = memFile.Detach();
	if (bEncrypt) {
		BYTE* encryptBuff = KRXEncrypt(rawBuff, buffLen, KRX_SALT);
		delete rawBuff;
		return encryptBuff;
	}
	return rawBuff;
}

inline CArmedInventoryObj* readObjectV1(GLID glid, CArchive& ar) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return nullptr;

	long long glidFromArchive;
	unsigned int count;
	CArmedInventoryObj* obj = NULL;
	ar >> glidFromArchive; // ignore this
	ar >> count; // for now ignore count.  To be implemented later if necessary.

	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	//m_texFileName[] does not point to actual texture files if UGC. The actual texture name / URL come from content service.
	matCompressor->setTexFileNameIsConcrete(IS_UGC_GLID(glid) ? false : true);

	MeshRM::Instance()->LoadReMeshes(ar, pICE->GetReDeviceState(), MeshRM::RMP_ID_PERSISTENT);
	matCompressor->readFromArchive(ar);

	ar >> obj;

	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	return obj;
}

std::unique_ptr<CArmedInventoryObj> EquippableRM::LoadEquippableItemFromLooseArchive(GLID glid, CArchive& ar) {
	unsigned char version;
	unsigned int code;

	ar >> code;
	ar >> version;
	if (code != EquippableRM::equippable_code) {
		LogError("File:" << ar.m_strFileName << " is not a valid DNG file.");
		return nullptr;
	}
	try {
		std::unique_ptr<CArmedInventoryObj> pEquip;
		if (version == 1) {
			pEquip.reset(readObjectV1(glid, ar));
		} else {
			LogWarn("File:" << ar.m_strFileName << " Unsupported asset file version: " << version);
			return nullptr;
		}

		if (!pEquip) {
			LogWarn("File:" << ar.m_strFileName << " Reading object failed");
			return false;
		}

		// Verify loaded object
		auto pSkeleton = pEquip->getSkeleton();
		assert(pSkeleton != nullptr);

		if (pSkeleton == nullptr) {
			LogError("File:" << ar.m_strFileName << " Object loaded but contains NULL 3P skeleton");
		} else {
			// Soft limits to replace hard validations in CBoneObject::Serialize()
			size_t boneCount = pSkeleton->getBoneCount();
			if (boneCount > NUM_BONES_LIMIT) {
				LogWarn("[" << glid << "] contains too many bones: " << boneCount);
			}

			size_t rigidMeshCount = pSkeleton->getRigidMeshCount();
			if (rigidMeshCount > NUM_RIGID_MESHES_LIMIT) {
				LogWarn("[" << glid << "] contains too many rigid meshes: " << rigidMeshCount);
			}

			size_t skinnedMeshCount = pSkeleton->getSkinnedMeshCount();
			if (skinnedMeshCount > NUM_SKINNED_MESHES_LIMIT) {
				LogWarn("[" << glid << "] contains too many skinned meshes: " << skinnedMeshCount);
			}
		}

		return pEquip;

	} catch (CArchiveException* e) {
		LogError("File:" << ar.m_strFileName << " Corrupted or incompatible file format");
		e->Delete();
		return false;
	} catch (CMemoryException* e) {
		LogError("File:" << ar.m_strFileName << " Insufficient memory");
		e->Delete();
		return false;
	} catch (CException* e) {
		LogError("File:" << ar.m_strFileName << " Unknown error");
		e->Delete();
		return false;
	}
}

inline bool writeObjectV1(CArmedInventoryObj* obj, long long glid, CArchive& ar) {
	unsigned char version = 1;
	unsigned int count = 1;
	ar << EquippableRM::equippable_code;
	ar << version;
	ar << glid;
	ar << count;

	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	RuntimeSkeleton* pSkeleton;

	pSkeleton = obj->getSkeleton(eVisualType::ThirdPerson);
	if (pSkeleton != nullptr) {
		matCompressor->addMaterials(pSkeleton, obj->getSkeletonConfig(eVisualType::ThirdPerson));
	}

	pSkeleton = obj->getSkeleton(eVisualType::FirstPerson);
	if (pSkeleton) {
		matCompressor->addMaterials(pSkeleton, obj->getSkeletonConfig(eVisualType::FirstPerson));
	}

	int rmCount = obj->countReMeshes();
	ReSkinMesh** outMeshes;
	outMeshes = obj->gatherMeshes();
	matCompressor->compress();

	MeshRM::Instance()->StoreReMeshes((ReMesh**)outMeshes, rmCount, ar, MeshRM::RMP_ID_PERSISTENT);
	matCompressor->writeToArchive(ar);

	ar << obj;
	delete[] outMeshes;
	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	return true;
}

bool EquippableRM::WriteEquippableObject(CArmedInventoryObj* obj, int glid, CArchive& ar) {
	return writeObjectV1(obj, glid, ar);
}

} // namespace KEP
