///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "math.h"

#include "CLaserClass.h"
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "Core/Math/Rotation.h"

namespace KEP {

CLaserObj::CLaserObj() {
	m_speed = 50.0f;
	m_currentLength = -500.0f;
	m_currentWidth = 10.0f;
	m_meshObject = NULL;
	m_lockDown = TRUE;
	m_millisecondsOfFade = 200;
	m_fadeOut = FALSE;
	m_terminated = FALSE;

	m_orientationAndOrigin.MakeIdentity();
	m_fireFromLocation = m_orientationAndOrigin;
}

void CLaserObj::SafeDelete() {
	if (m_meshObject) {
		m_meshObject->SafeDelete();
		delete m_meshObject;
		m_meshObject = 0;
	}
}

void CLaserObj::Clone(CLaserObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CLaserObj* copyLocal = new CLaserObj();

	copyLocal->m_currentLength = m_currentLength;
	copyLocal->m_currentWidth = m_currentWidth;
	copyLocal->m_speed = m_speed;
	copyLocal->m_lockDown = m_lockDown;
	copyLocal->m_millisecondsOfFade = m_millisecondsOfFade;
	copyLocal->m_fadeOut = m_fadeOut;
	copyLocal->m_terminated = FALSE;

	if (m_meshObject)
		m_meshObject->Clone(&copyLocal->m_meshObject, g_pd3dDevice);

	*clone = copyLocal;
}

void CLaserObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CLaserObj* spnPtr = (CLaserObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CLaserObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_speed
		   << m_currentLength
		   << m_currentWidth
		   << m_meshObject
		   << m_lockDown
		   << m_millisecondsOfFade
		   << m_fadeOut;

	} else {
		ar >> m_speed >> m_currentLength >> m_currentWidth >> m_meshObject >> m_lockDown >> m_millisecondsOfFade >> m_fadeOut;

		m_terminated = FALSE;

		m_orientationAndOrigin.MakeIdentity();
		m_fireFromLocation = m_orientationAndOrigin;
	}
}

void CLaserObj::InitializeMesh(LPDIRECT3DDEVICE9 g_pd3dDevice) {
	BOOL previousDataExisted = FALSE;
	int texture = 0;
	std::string textureName;
	CMaterialObject* materialStored = NULL;

	if (m_meshObject) {
		m_meshObject->m_materialObject->Clone(&materialStored);
		previousDataExisted = TRUE;
		texture = m_meshObject->m_materialObject->m_texNameHash[0];
		textureName = m_meshObject->m_materialObject->m_texFileName[0];
		m_meshObject->SafeDelete();
		delete m_meshObject;
		m_meshObject = 0;
	}

	m_meshObject = new CEXMeshObj();
	if (!materialStored)
		m_meshObject->m_materialObject = new CMaterialObject();
	else
		materialStored->Clone(&m_meshObject->m_materialObject);

	if (!previousDataExisted)
		m_meshObject->m_materialObject->m_texNameHash[0] = 0;
	else {
		m_meshObject->m_materialObject->m_texNameHash[0] = texture;
		m_meshObject->m_materialObject->m_texFileName[0] = textureName;
	}

	m_meshObject->m_abVertexArray.m_uvCount = 1;

	UINT indexArray[6];
	ZeroMemory(indexArray, sizeof(indexArray));
	m_meshObject->m_abVertexArray.SetVertexIndexArray(indexArray, 6, 4);

	ABVERTEX vArray[4];
	ZeroMemory(vArray, sizeof(vArray));
	m_meshObject->m_abVertexArray.SetABVERTEXStyle(NULL, vArray, m_meshObject->m_abVertexArray.m_vertexCount);
	m_meshObject->m_materialObject->Reset();
}

BOOL CLaserObj::UpdateLazerMesh(Matrix44f cameraMatrix) {
	Vector3f cameraPositionLocal;
	cameraPositionLocal = cameraMatrix.GetTranslation();
	cameraPositionLocal = MatrixARB::GetLocalPositionFromMatrixView(m_orientationAndOrigin, cameraPositionLocal);
	cameraPositionLocal.z = 0.0f;

	float rotationOnZ = -(atan(cameraPositionLocal.x / cameraPositionLocal.y) * 57.3f);
	if (cameraPositionLocal.y < 0.0f)
		rotationOnZ = rotationOnZ + 180.0f;

	//Vertex one
	float widthComponent = (m_currentWidth * .5f);

	m_meshObject->m_abVertexArray.m_vertexArray1[0].x = -widthComponent;
	m_meshObject->m_abVertexArray.m_vertexArray1[0].y = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[0].z = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[0].tx0.tu = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[0].tx0.tv = 0.0f;
	//vertex two
	m_meshObject->m_abVertexArray.m_vertexArray1[1].x = widthComponent;
	m_meshObject->m_abVertexArray.m_vertexArray1[1].y = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[1].z = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[1].tx0.tu = 1.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[1].tx0.tv = 0.0f;
	//vertex three
	m_meshObject->m_abVertexArray.m_vertexArray1[2].x = widthComponent;
	m_meshObject->m_abVertexArray.m_vertexArray1[2].y = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[2].z = m_currentLength;
	m_meshObject->m_abVertexArray.m_vertexArray1[2].tx0.tu = 1.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[2].tx0.tv = 1.0f;
	//vertex four
	m_meshObject->m_abVertexArray.m_vertexArray1[3].x = -widthComponent;
	m_meshObject->m_abVertexArray.m_vertexArray1[3].y = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[3].z = m_currentLength;
	m_meshObject->m_abVertexArray.m_vertexArray1[3].tx0.tu = 0.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[3].tx0.tv = 1.0f;
	//default normals (doesnt matter cause of emmisive property of a laser)
	m_meshObject->m_abVertexArray.m_vertexArray1[0].nx = 1.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[1].nx = 1.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[2].nx = 1.0f;
	m_meshObject->m_abVertexArray.m_vertexArray1[3].nx = 1.0f;
	//set up indecies
	m_meshObject->m_abVertexArray.SetVertexIndex(0, 0);
	m_meshObject->m_abVertexArray.SetVertexIndex(1, 1);
	m_meshObject->m_abVertexArray.SetVertexIndex(2, 2);
	m_meshObject->m_abVertexArray.SetVertexIndex(3, 0);
	m_meshObject->m_abVertexArray.SetVertexIndex(4, 2);
	m_meshObject->m_abVertexArray.SetVertexIndex(5, 3);

	Matrix44f rotation;
	Matrix44f result;
	ConvertRotationZ(rotationOnZ, out(rotation));
	result = rotation * m_orientationAndOrigin;
	m_meshObject->SetWorldTransform(result);
	return TRUE;
}

IMPLEMENT_SERIAL(CLaserObj, CObject, 0)
IMPLEMENT_SERIAL(CLaserObjList, CObList, 0)

BEGIN_GETSET_IMPL(CLaserObj, IDS_LASEROBJ_NAME)
GETSET_RANGE(m_speed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LASEROBJ, SPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_currentLength, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LASEROBJ, CURRENTLENGTH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_currentWidth, gsFloat, GS_FLAGS_DEFAULT, 0, 0, LASEROBJ, CURRENTWIDTH, FLOAT_MIN, FLOAT_MAX)
GETSET_OBJ_PTR(m_meshObject, GS_FLAGS_DEFAULT, 0, 0, LASEROBJ, MESHOBJECT, CEXMeshObj)
GETSET(m_lockDown, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, LASEROBJ, LOCKDOWN)
GETSET_RANGE(m_millisecondsOfFade, gsLong, GS_FLAGS_DEFAULT, 0, 0, LASEROBJ, MILLISECONDSOFFADE, LONG_MIN, LONG_MAX)
GETSET(m_fadeOut, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, LASEROBJ, FADEOUT)
END_GETSET_IMPL

} // namespace KEP
