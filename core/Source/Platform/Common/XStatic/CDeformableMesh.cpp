///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS

#include "resource.h"
#include <afxtempl.h>
#include "d3d9.h"
#include "CDeformableMesh.h"
#include "VertexDeclaration.h"
#include <SerializeHelper.h>
#include "CBoneClass.h"
#include "CEXMeshClass.h"
#include "LogHelper.h"

#ifdef _ClientOutput
#include "ReMeshCreate.h"
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "ReD3DX9SkinMesh.h"
#include "ReMeshCache.h"
#include "Utils/ReUtils.h"
#include "ReMeshStream.h"
#endif

#include "CSkeletonObject.h"
#include "IClientEngine.h"
#include "MaterialPersistence.h"
#include "UnicodeMFCHelpers.h"
#include "common/include/IMemSizeGadget.h"

static LogInstance("Instance");

namespace KEP {

CDeformableVisObj::CDeformableVisObj() {
	m_mesh = NULL;
	m_blendedType = SkinVertexBlendType::Disabled;
	m_ReMesh = NULL;
}

inline BOOL CDeformableVisObj::VerticiePartialCompare(const ABVERTEX& v1, const ABVERTEX& v2, int i1, int i2) {
	if (v1.x == v2.x &&
		v1.y == v2.y &&
		v1.z == v2.z &&
		v1.tx0.tu == v2.tx0.tu &&
		v1.tx0.tv == v2.tx0.tv &&
		v1.tx1.tu == v2.tx1.tu &&
		v1.tx1.tv == v2.tx1.tv &&
		v1.tx2.tu == v2.tx2.tu &&
		v1.tx2.tv == v2.tx2.tv &&
		v1.tx3.tu == v2.tx3.tu &&
		v1.tx3.tv == v2.tx3.tv &&
		v1.tx4.tu == v2.tx4.tu &&
		v1.tx4.tv == v2.tx4.tv &&
		v1.tx5.tu == v2.tx5.tu &&
		v1.tx5.tv == v2.tx5.tv &&
		v1.tx6.tu == v2.tx6.tu &&
		v1.tx6.tv == v2.tx6.tv &&
		v1.tx7.tu == v2.tx7.tu &&
		v1.tx7.tv == v2.tx7.tv) {
		// All vertex data identical, lets verify that they have the same vertex assignments
		// We will convert from the array index to his .index member so that
		// This will work on the older assets.

		/////////////////////////////////////////////////////////////////////////////////////
		int convertedI1 = -1; // Ok convert i1 and i2 for backwards compatibility with GPB
		int convertedI2 = -1; // in the newer assets i1 == convertedI1 && i2 ==convertedI2

		// Ok first lets check to see if i1 == convertedI1 so we don't have to search for it
		switch (m_blendedType) {
			case SkinVertexBlendType::Disabled:
				break;
			case SkinVertexBlendType::Position_Normal:
				if (i1 < m_blendedVertexAssignments.size() && m_blendedVertexAssignments[i1].vertexIndex == i1) {
					convertedI1 = i1;
				}
				if (i2 < m_blendedVertexAssignments.size() && m_blendedVertexAssignments[i2].vertexIndex == i2) {
					convertedI2 = i2;
				}
				break;
			case SkinVertexBlendType::Position_Normal_Tangent:
				assert(false); // deprecated
				break;
		}
		if (convertedI1 == -1 || convertedI2 == -1) {
			// We have to search for one or both of them
			switch (m_blendedType) {
				case SkinVertexBlendType::Disabled:
					break;
				case SkinVertexBlendType::Position_Normal:
					for (int temploop = 0; temploop < m_blendedVertexAssignments.size(); temploop++) {
						// Loop through the vertex assignments looking for the vertex assignment for i1 and i2
						if (convertedI1 == -1 && m_blendedVertexAssignments[temploop].vertexIndex == i1) {
							convertedI1 = temploop;
						}
						if (convertedI2 == -1 && m_blendedVertexAssignments[temploop].vertexIndex == i2) {
							convertedI2 = temploop;
						}
					}
					if (convertedI1 == -1 || convertedI2 == -1) {
						return FALSE; // This is a big problem we couldn't find assignments for one or both of our vertexes.
					}
					break;
				case SkinVertexBlendType::Position_Normal_Tangent:
					assert(false); // deprecated
					break;
			}
		} // End search for convertedi1&2
		///////////////////////////////////////////////////////////////////

		switch (m_blendedType) {
			case SkinVertexBlendType::Disabled:
				if (i1 < m_vertexAssignments.size() && i2 < m_vertexAssignments.size()) {
					if (m_vertexAssignments[i1].vertexIndex != m_vertexAssignments[i2].vertexIndex) {
						return FALSE;
					}
				}
				break;
			case SkinVertexBlendType::Position_Normal:
				if (m_blendedVertexAssignments[convertedI1].vertexBoneWeights.size() != m_blendedVertexAssignments[convertedI2].vertexBoneWeights.size()) {
					return FALSE;
				}
				for (int xBonesLoop = 0; xBonesLoop < m_blendedVertexAssignments[convertedI1].vertexBoneWeights.size(); xBonesLoop++) {
					//vertex/bone blend calc
					const VertexBoneWeight& boneWeight1 = m_blendedVertexAssignments[convertedI1].vertexBoneWeights[xBonesLoop];
					const VertexBoneWeight& boneWeight2 = m_blendedVertexAssignments[convertedI2].vertexBoneWeights[xBonesLoop];

					if (boneWeight1.boneIndex != boneWeight2.boneIndex || boneWeight1.weight != boneWeight2.weight) {
						return FALSE;
					}
				}
				break;
			case SkinVertexBlendType::Position_Normal_Tangent:
				assert(false); // deprecated
				break;
		}
		return TRUE; //matches criteria
	} else {
		return FALSE;
	}
}

//////////////////////////////////////////////////////////////////////////////////////
// Helper function by Jonny to take care of these 3 different vertex weighting types
// without making my code too complicated
/////////////////////////////////////////////////////////////////////////////////////
void CDeformableVisObj::setVertexAssignment(int newIndex, int oldIndex, std::vector<VertexAssignment>& newVertexAssignments, std::vector<VertexAssignmentBlended>& newBlendedVertexAssignments) {
	// Same technique with the vertex compare we have to convert the old index to make it compatible with GPB

	int convertedOldIndex = -1;
	switch (m_blendedType) {
		case SkinVertexBlendType::Disabled:
			// See case 1 for code explanation and comments.
			if (m_vertexAssignments[oldIndex].vertexIndex == oldIndex) {
				convertedOldIndex = oldIndex;
			} else {
				for (int temploop = 0; temploop < m_vertexAssignments.size(); temploop++) {
					if (m_vertexAssignments[temploop].vertexIndex == oldIndex) {
						convertedOldIndex = temploop;
						break;
					}
					for (auto vtxIndex : m_vertexAssignments[temploop].verticesCarried) {
						if (vtxIndex == oldIndex) {
							convertedOldIndex = temploop;
							break;
						}
					}
				}
			}
			break;
		case SkinVertexBlendType::Position_Normal:
			// Just like before check the index to see if its unoptimized first
			if (m_blendedVertexAssignments[oldIndex].vertexIndex == oldIndex) {
				convertedOldIndex = oldIndex;
			} // All this does is check the most likely location for the assignment first to save time.
			else {
				// else we have to search for the vertex assignment
				for (int temploop = 0; temploop < m_blendedVertexAssignments.size(); temploop++) {
					// Loop through the vertex assignments looking for the converted oldIndex
					if (m_blendedVertexAssignments[temploop].vertexIndex == oldIndex) {
						convertedOldIndex = temploop;
						break; // found it we can stop now
					}
					// Also check for "carried" vertexes because this was a hack in the original to animate verts that were redundant
					// but weren't eliminated vertexesCarried and carriedCount should be completely removed once everyone converts their
					// assets to the optimized format.
					for (auto vtxIndex : m_blendedVertexAssignments[temploop].verticesCarried) {
						if (vtxIndex == oldIndex) {
							// The temploop vertex "carries" this vertex so we need its assignment.
							convertedOldIndex = temploop;
							break;
						}
					}
				}
			}
			break;
		case SkinVertexBlendType::Position_Normal_Tangent:
			assert(false); // deprecated
			break;
	}
	if (convertedOldIndex == -1) {
		convertedOldIndex = 0; // We really couldn't find this vertex
		// Give it the first assignment in the array and hope for the best.
	}
	if (!newVertexAssignments.empty() && !m_vertexAssignments.empty()) {
		assert(newIndex < newVertexAssignments.size() && convertedOldIndex < m_vertexAssignments.size());
		newVertexAssignments[newIndex].boneIndex = m_vertexAssignments[convertedOldIndex].boneIndex;
		newVertexAssignments[newIndex].verticesCarried.clear();
		newVertexAssignments[newIndex].vertexIndex = newIndex; // This fixes crash of optimized assets
		// He was storing the index as a member instead of using the location in the array.
	}
	if (!newBlendedVertexAssignments.empty() && !m_blendedVertexAssignments.empty()) {
		assert(newIndex < newBlendedVertexAssignments.size() && convertedOldIndex < m_blendedVertexAssignments.size());
		newBlendedVertexAssignments[newIndex].vertexBoneWeights = m_blendedVertexAssignments[convertedOldIndex].vertexBoneWeights;
		newBlendedVertexAssignments[newIndex].verticesCarried.clear(); // Carried count should now be 0 the optimized assets won't use
		newBlendedVertexAssignments[newIndex].vertexIndex = newIndex; // This creates backwards compatibility
	}
}

void CDeformableVisObj::Optimize() {
	// Quick Sanity checks
	assert(m_mesh != nullptr);
	if (m_mesh == nullptr) {
		return;
	}

	if (m_blendedType == SkinVertexBlendType::Position_Normal_Tangent) {
		assert(false);
		// DRF - Added Error Check & Message Box
		MessageBoxW(NULL, L"ERROR - Mesh Missing Vertex-Weight Map", L"ERROR", MB_ICONEXCLAMATION);
		LogError("FAILED - Mesh Missing Vertex-Weight Map");
		return;
	}

	ABVERTEX* tempArray = NULL;
	int vCount = 0;

	// These temporary arrays hold the vertex to bone assignments depending on the type of mesh this is
	std::vector<VertexAssignment> newVertexAssignments;
	std::vector<VertexAssignmentBlended> newBlendedVertexAssignments;

	// copy all the mesh's vertex data into tempArray
	m_mesh->m_abVertexArray.GetABVERTEXStyle(&tempArray, &vCount);

	// This is a conversion table to store what each vertex in the indecie array should be converted to in the new array.
	UINT* indexConversionTable = new UINT[(m_mesh->m_abVertexArray).m_indecieCount];

	// Number of actually unique vertices in the mesh
	int uniqueVerts = 0;

	// setup the index conversion table
	//memset( &indexConversionTable[0], -1, sizeof(UINT)*m_mesh->m_abVertexArray.m_indecieCount);
	for (UINT temp = 0; temp < m_mesh->m_abVertexArray.m_indecieCount; temp++) {
		indexConversionTable[temp] = -1;
	}

	// Set this to the origional vertex count and we will decrement it.
	UINT newVertexCount = 0; //(m_mesh->m_abVertexArray).m_vertexCount;

	//fprintf(gf, "Mappings\r\n");

	// Loop through all the origional vertices in the mesh
	for (UINT mainLoop = 0; mainLoop < m_mesh->m_abVertexArray.m_vertexCount; mainLoop++) {
		// main loop
		//fprintf( gf, "%d-%.2f:%.2f:%.2f\r\n", mainLoop, tempArray[mainLoop].x, tempArray[mainLoop].y, tempArray[mainLoop].z );
		// This inner loop is used to compare each vertex to every other vertex in the array
		for (UINT secondaryLoop = mainLoop; secondaryLoop < (m_mesh->m_abVertexArray).m_vertexCount; secondaryLoop++) {
			// secondary loop
			if (secondaryLoop != mainLoop) {
				// Don't compare vertex to itself
				if (VerticiePartialCompare(tempArray[mainLoop], tempArray[secondaryLoop], mainLoop, secondaryLoop)) {
					// The two vertices are redundant one of them needs to be ELIMINATED!
					// In the vertex indecie array set the number of the second indecie to the first.
					if (indexConversionTable[mainLoop] == -1) {
						// The old vertex number <mainLoop> will be numbered <uniqueVerts> in the new Array
						indexConversionTable[mainLoop] = uniqueVerts;
					}
					// The redundant vertex we found in the new Array should have the same number as the first vertex
					indexConversionTable[secondaryLoop] = indexConversionTable[mainLoop];
					//fprintf(gf, "mapping %d to %d\r\n", secondaryLoop, indexConversionTable[mainLoop]);
				} // end match
			} // end not the same
		}
		if (indexConversionTable[mainLoop] == -1) {
			// Lone vertex that doesn't exist elsewhere
			indexConversionTable[mainLoop] = uniqueVerts;
			//fprintf(gf, "mapping %d to %d\r\n", mainLoop, uniqueVerts);
			// This vertex should have its own unique identifier in the new Array
			uniqueVerts++;
		} else if (indexConversionTable[mainLoop] == uniqueVerts) {
			// Two or more redundant vertices were found and assigned the same identifier in the new array so increase
			// The number of unique vertices
			uniqueVerts++;
		}
		// end secondary loop
	}
	newVertexCount = uniqueVerts; // The size the vertex array should be reduced to
	assert(newVertexCount > 0);

	// end main loop

	// allocate new array
	ABVERTEX* newVertexArray = new ABVERTEX[newVertexCount];
	switch (m_blendedType) {
		case SkinVertexBlendType::Disabled:
			newVertexAssignments.resize(newVertexCount);
			break;
		case SkinVertexBlendType::Position_Normal:
			newBlendedVertexAssignments.resize(newVertexCount);
			break;
		case SkinVertexBlendType::Position_Normal_Tangent:
			assert(false); // deprecated
			break;
	}

	// build non duplicate indecie array//
	UINT* newIndecieArray;
	UINT numIndices;
	m_mesh->m_abVertexArray.GetVertexIndexArray(&newIndecieArray, numIndices);
	for (UINT indecieLoop = 0; indecieLoop < numIndices; indecieLoop++) {
	}

	// Rebuild vertexArray//
	int dupTrace = 0;
	for (UINT indecieLoop = 0; indecieLoop < m_mesh->m_abVertexArray.m_vertexCount; indecieLoop++) {
		BOOL logIt = TRUE;
		for (UINT count = 0; count < indecieLoop; count++) {
			// check to see if this has been added
			// if ( newIndecieArray[indecieLoop] == indexConversionTable[count] )
			if (VerticiePartialCompare(tempArray[indecieLoop], tempArray[count], indecieLoop, count)) {
				// if already has been used dont log
				logIt = FALSE;
				break;
			} // end if already has been used dont log
		} // end check to see if this has been added

		if (logIt == TRUE) {
			// Copying a unique vertex into the new vertex array.
			// set vertex data
			newVertexArray[dupTrace].x = tempArray[indecieLoop].x;
			newVertexArray[dupTrace].y = tempArray[indecieLoop].y;
			newVertexArray[dupTrace].z = tempArray[indecieLoop].z;
			newVertexArray[dupTrace].nx = tempArray[indecieLoop].nx;
			newVertexArray[dupTrace].ny = tempArray[indecieLoop].ny;
			newVertexArray[dupTrace].nz = tempArray[indecieLoop].nz;
			newVertexArray[dupTrace].tx0.tu = tempArray[indecieLoop].tx0.tu;
			newVertexArray[dupTrace].tx0.tv = tempArray[indecieLoop].tx0.tv;
			newVertexArray[dupTrace].tx1.tu = tempArray[indecieLoop].tx1.tu;
			newVertexArray[dupTrace].tx1.tv = tempArray[indecieLoop].tx1.tv;
			newVertexArray[dupTrace].tx2.tu = tempArray[indecieLoop].tx2.tu;
			newVertexArray[dupTrace].tx2.tv = tempArray[indecieLoop].tx2.tv;
			newVertexArray[dupTrace].tx3.tu = tempArray[indecieLoop].tx3.tu;
			newVertexArray[dupTrace].tx3.tv = tempArray[indecieLoop].tx3.tv;
			newVertexArray[dupTrace].tx4.tu = tempArray[indecieLoop].tx4.tu;
			newVertexArray[dupTrace].tx4.tv = tempArray[indecieLoop].tx4.tv;
			newVertexArray[dupTrace].tx5.tu = tempArray[indecieLoop].tx5.tu;
			newVertexArray[dupTrace].tx5.tv = tempArray[indecieLoop].tx5.tv;
			newVertexArray[dupTrace].tx6.tu = tempArray[indecieLoop].tx6.tu;
			newVertexArray[dupTrace].tx6.tv = tempArray[indecieLoop].tx6.tv;
			newVertexArray[dupTrace].tx7.tu = tempArray[indecieLoop].tx7.tu;
			newVertexArray[dupTrace].tx7.tv = tempArray[indecieLoop].tx7.tv;
			newVertexArray[dupTrace].tx = tempArray[indecieLoop].tx;
			newVertexArray[dupTrace].ty = tempArray[indecieLoop].ty;
			newVertexArray[dupTrace].tz = tempArray[indecieLoop].tz;

			//newIndecieArray[indecieLoop] = indexConversionTable[indecieLoop];
			setVertexAssignment(dupTrace, indecieLoop,
				newVertexAssignments,
				newBlendedVertexAssignments);

			dupTrace++;
		}
	}

	// Now we rebuild the index array.
	for (UINT indecieLoop = 0; indecieLoop < m_mesh->m_abVertexArray.m_indecieCount; indecieLoop++) {
		newIndecieArray[indecieLoop] = indexConversionTable[newIndecieArray[indecieLoop]];
	}

	for (UINT indecieLoop = 0; indecieLoop < m_mesh->m_abVertexArray.m_indecieCount; indecieLoop++) {
		//fprintf(gf, "%d - %d\r\n", indecieLoop, newIndecieArray[indecieLoop]);
	}

	//fclose(gf);

	// Clean up and replace values
	if (tempArray) {
		delete[] tempArray;
		tempArray = 0;
	}
	if (indexConversionTable) {
		delete[] indexConversionTable;
		indexConversionTable = 0;
	}
	// Set the vertex table which is the list of vertices
	m_mesh->m_abVertexArray.SetABVERTEXStyle(NULL, newVertexArray, newVertexCount);

	// Set the index table which is a table of which vertices compose the triangles
	m_mesh->m_abVertexArray.SetVertexIndexArray(newIndecieArray, numIndices, newVertexCount);
	switch (m_blendedType) {
		case SkinVertexBlendType::Disabled:
			m_vertexAssignments = newVertexAssignments;
			break;
		case SkinVertexBlendType::Position_Normal:
			m_blendedVertexAssignments = newBlendedVertexAssignments;
			break;
		case SkinVertexBlendType::Position_Normal_Tangent:
			assert(false); // deprecated
			break;
	}
	if (newIndecieArray) {
		delete[] newIndecieArray; //Fixed memory leak - Same as above
		newIndecieArray = 0;
	}
	if (newVertexArray) {
		delete[] newVertexArray; //Fixed memory leak - Same as above
		newVertexArray = 0;
	}

	//	All deformable objects are skinned. For now.
	//	The question is, are there any non-deformable meshes
	//	which require skinning ? Anyone know? Anyone? Beuller?
#ifdef GPU_SKINNING_ENABLED
	m_mesh->mbIsSkinned = true;
#endif

	m_mesh->InitBuffer(m_mesh->m_pd3dDevice, NULL);

	m_mesh->m_indexBuffiltered = TRUE;
}

CDeformableVisObj::~CDeformableVisObj() {
#ifdef _ClientOutput
	if (m_mesh) {
		m_mesh->SafeDelete();
		delete m_mesh;
		m_mesh = 0;
	}

	// old data format arrays aren't there anymore
	if (m_ReMesh) { // RENDERENGINE INTERFACE
		// delete the ReMesh if cloned....the cloning meshes are persistent in ReMeshCreate
		if (m_ReMesh->IsCloned()) {
			delete m_ReMesh;
		}
		m_ReMesh = NULL;
		//return;	//Fixed memory leak - why returned here? Apparently we have more clean-ups to do. [Yanfeng 10/2008]
	}

#else
	ASSERT(0);
#endif
}

void CDeformableVisObj::Clone(CDeformableVisObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CDeformableVisObj* copyLocal = new CDeformableVisObj();

	copyLocal->m_blendedType = m_blendedType;

#ifdef _ClientOutput
	if (m_mesh) {
		m_mesh->Clone(&copyLocal->m_mesh, g_pd3dDevice);
	}

	copyLocal->m_vertexAssignments = m_vertexAssignments;
	copyLocal->m_blendedVertexAssignments = m_blendedVertexAssignments;

	if (m_ReMesh) {
		//((ReSkinMesh*)m_ReMesh)->Clone((ReSkinMesh**)&copyLocal->m_ReMesh);					// RENDERENGINE INTERFACE
		m_ReMesh->Clone(&copyLocal->m_ReMesh);
	} else {
		copyLocal->m_ReMesh = NULL;
	}
#endif
	*clone = copyLocal;
}

void CDeformableVisObj::AddVertexAssignmentBlended(std::vector<VertexBoneWeight>&& vertexBoneWeights, int vertexIndex) {
	VertexAssignmentBlended assignment;
	assignment.vertexBoneWeights = std::move(vertexBoneWeights);
	assignment.vertexIndex = vertexIndex;
	m_blendedVertexAssignments.push_back(assignment);

	//NOTE: old impl also clears out verticesCarried on all assignments
}

void CDeformableVisObj::DoOldImport(CArchive& ar) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
#endif

	int version = ar.GetObjectSchema();

	// ReMesh version
	if (version >= 4) {
#ifdef _ClientOutput
		// the new ReSkinMesh
		ReSkinMesh* newMesh;
		// get an ReMeshStream object
		ReMeshStream streamer;
		// stream the ReMesh
		streamer.ReadSkin(&newMesh, ar, pICE->GetReDeviceState());
#else
		ASSERT(FALSE); // server never imports these
#endif
		// stream the CEXMeshObj
		ar >> m_mesh;

#ifdef _ClientOutput
		// add the CEXMesh textures to the data base before making the ReMaterials (ONLY in Progressive Download mode)
		if (MeshRM::Instance()->GetProgressiveLoadFlag()) {
			pICE->AddTextureToDatabase(m_mesh, FALSE);
		}
		// check for dups
		ReMesh* mesh = MeshRM::Instance()->AddMesh(newMesh, MeshRM::RMP_ID_PERSISTENT);
		// no dup found
		if (mesh == NULL) {
			SetReMesh(newMesh);
		}
		// dup found....use it and delete the new mesh
		else {
			SetReMesh(mesh);
			delete newMesh;
		}
#endif

		return;
	}

	// previous versions
	m_vertexAssignments.clear();
	m_blendedVertexAssignments.clear();

	if (version < 2) {
		//VERSION 1
		int assignmentCount = 0;
		ar >> m_mesh >> assignmentCount;

		m_blendedType = SkinVertexBlendType::Disabled;

		if (m_mesh->m_abVertexArray.m_vertexCount > 0) {
			//m_originalArrayVector = new ABVERTEX0L[m_mesh->m_abVertexArray.m_vertexCount];
			//m_mesh->m_abVertexArray.Fill0UVArrayData(m_originalArrayVector);//Correct
			for (UINT i = 0; i < m_mesh->m_abVertexArray.m_vertexCount; i++) {
				//ar >> m_originalArrayVector[i].x >> m_originalArrayVector[i].y >> m_originalArrayVector[i].z;
				float x, y, z;
				ar >> x >> y >> z;
			}
		}

		m_vertexAssignments.resize(assignmentCount);
		for (int loop = 0; loop < assignmentCount; loop++) {
			int intBoneIndex = 0, intVertexIndex = 0, carriedCount = 0;
			ar >> intBoneIndex;
			m_vertexAssignments[loop].boneIndex = intBoneIndex;
			ar >> carriedCount;
			ar >> intVertexIndex;
			m_vertexAssignments[loop].vertexIndex = intVertexIndex;

			for (int i = 0; i < carriedCount; i++) {
				int carriedVertexIndex = 0;
				ar >> carriedVertexIndex;
				m_vertexAssignments[loop].verticesCarried[i] = carriedVertexIndex;
			}
		}
	} //END VERSION 1
	else if (version == 2) {
		//VERSION 2
		int assignmentCount = 0;
		ar >> m_mesh >> assignmentCount;
		m_blendedType = SkinVertexBlendType::Disabled;

		if (m_mesh->m_abVertexArray.m_vertexCount > 0) {
			//m_originalArrayVector = new ABVERTEX0L[m_mesh->m_abVertexArray.m_vertexCount];
			//m_mesh->m_abVertexArray.Fill0UVArrayData(m_originalArrayVector);//Correct
			for (UINT i = 0; i < m_mesh->m_abVertexArray.m_vertexCount; i++) {
				//ar >> m_originalArrayVector[i].x >> m_originalArrayVector[i].y >> m_originalArrayVector[i].z;
				float x, y, z;
				ar >> x >> y >> z;
			}
		}

		m_vertexAssignments.resize(assignmentCount);
		for (int loop = 0; loop < assignmentCount; loop++) {
			ar >> m_vertexAssignments[loop].boneIndex;
			byte carriedCount = 0;
			ar >> carriedCount;
			ar >> m_vertexAssignments[loop].vertexIndex;

			m_vertexAssignments[loop].verticesCarried.resize(carriedCount);
			for (int i = 0; i < carriedCount; i++) {
				ar >> m_vertexAssignments[loop].verticesCarried[i];
			}
		}
	} //END VERSION 2
	else if (version == 3) {
		//VERSION 3
		int assignmentCount = 0, intBlendedType = 0;
		ar >> m_mesh >> assignmentCount >> intBlendedType;
		m_blendedType = (SkinVertexBlendType)intBlendedType;

		if (m_blendedType != SkinVertexBlendType::Disabled) {
			if (m_mesh->m_abVertexArray.m_vertexCount > 0) {
				//build orig vector array
				//m_originalArrayVector = new ABVERTEX0L[m_mesh->m_abVertexArray.m_vertexCount];
				//m_mesh->m_abVertexArray.Fill0UVArrayData(m_originalArrayVector);//Correct
			}
		}

		switch (m_blendedType) {
			case SkinVertexBlendType::Disabled:
				//rigid type
				m_vertexAssignments.resize(assignmentCount);
				for (int loop = 0; loop < assignmentCount; loop++) {
					byte carriedCount = 0;
					ar >> m_vertexAssignments[loop].boneIndex;
					ar >> carriedCount;
					ar >> m_vertexAssignments[loop].vertexIndex;

					m_vertexAssignments[loop].verticesCarried.resize(carriedCount);
					for (int i = 0; i < carriedCount; i++) {
						ar >> m_vertexAssignments[loop].verticesCarried[i];
					}
				}
				break;
			case SkinVertexBlendType::Position_Normal:
				//blended type
				m_blendedVertexAssignments.resize(assignmentCount);
				for (int loop = 0; loop < assignmentCount; loop++) {
					byte boneIndexesCount = 0, carriedCount = 0;
					ar >> boneIndexesCount >> carriedCount >> m_blendedVertexAssignments[loop].vertexIndex;

					m_blendedVertexAssignments[loop].vertexBoneWeights.resize(boneIndexesCount);
					for (int i = 0; i < boneIndexesCount; i++) {
						VertexBoneWeight& boneWeight = m_blendedVertexAssignments[loop].vertexBoneWeights[i];
						ar >> boneWeight.boneIndex >> boneWeight.weight >> boneWeight.positionInBoneSpace.x >> boneWeight.positionInBoneSpace.y >> boneWeight.positionInBoneSpace.z >> boneWeight.normalInBoneSpace.x >> boneWeight.normalInBoneSpace.y >> boneWeight.normalInBoneSpace.z;
					}

					m_blendedVertexAssignments[loop].verticesCarried.resize(carriedCount);
					for (int i = 0; i < carriedCount; i++) {
						ar >> m_blendedVertexAssignments[loop].verticesCarried[i];
					}
				}
			case SkinVertexBlendType::Position_Normal_Tangent:
				assert(false); // deprecated
				break;
		}
	} //END VERSION 3

#ifdef _ClientOutput
	// optimize the vertex buffers
	Optimize();
	// create a new ReMesh the legacy way....
	SetReMesh(MeshRM::Instance()->CreateReSkinMeshPersistent(this));
	// increment LOD count as LOD chain is read in (reset CDeformableMesh::Serialize)
	MeshRM::Instance()->SetCurrentLOD(MeshRM::Instance()->GetCurrentLOD() + 1);

	// flag disables VertexArray streaming in m_meshObject by default
	m_mesh->m_VertexArrayValid = FALSE;
#endif
}

void CDeformableVisObj::DoOldExport(CArchive& ar) {
#ifdef _ClientOutput
	// bump the version level from 3 to 4 for ReMesh support
	ar.SetObjectSchema(4);
	ASSERT(ar.GetObjectSchema() == 4);
	// create a ReMesh streaming object
	ReMeshStream streamer;
	// stream the ReMesh
	streamer.WriteSkin(static_cast<ReSkinMesh*>(m_ReMesh), ar);
	// flag disables VertexArray streaming in m_meshObject by default
	m_mesh->m_VertexArrayValid = FALSE;
	ar << m_mesh;
#else
	ASSERT(FALSE);
#endif
}

void CDeformableVisObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		DoOldExport(ar);
	} else {
		DoOldImport(ar);
	}
}

CDeformableVisObj* CDeformableVisList::GetByIndex(int index) {
	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;
	return (CDeformableVisObj*)GetAt(posLoc);
}

void CDeformableVisList::Clone(CDeformableVisList** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, const CBoneList* pBoneList) {
	CDeformableVisList* copyLocal = new CDeformableVisList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDeformableVisObj* dPtr = (CDeformableVisObj*)GetNext(posLoc);
		CDeformableVisObj* cloned = NULL;
		dPtr->Clone(&cloned, g_pd3dDevice);
		copyLocal->AddTail(cloned);
	}
	*clone = copyLocal;
}

void CDeformableVisList::Optimize() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDeformableVisObj* dPtr = (CDeformableVisObj*)GetNext(posLoc);
		dPtr->Optimize();
	}
}

void CDeformableVisList::AvgNormals(float angTol) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDeformableVisObj* dPtr = (CDeformableVisObj*)GetNext(posLoc);
		dPtr->m_mesh->AVGNormals(angTol);
	}
}

void CDeformableVisList::Normalize() {
	/*
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CDeformableVisObj* dPtr = (CDeformableVisObj*)GetNext(posLoc);
			//dPtr->m_mesh->GenerateSmoothNormals();
		}
	*/
}

void CDeformableVisList::UnifyNormals(float tol) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDeformableVisObj* dPtr = (CDeformableVisObj*)GetNext(posLoc);
		dPtr->m_mesh->UnifyNormals(tol);
	}
}

void CDeformableVisList::NormilizeOnly() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDeformableVisObj* dPtr = (CDeformableVisObj*)GetNext(posLoc);
		dPtr->m_mesh->NormalizeOnly();
	}
}
void CDeformableVisList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDeformableVisObj* dPtr = (CDeformableVisObj*)GetNext(posLoc);
		delete dPtr;
		dPtr = 0;
	}
	RemoveAll();
}

CDeformableMesh::CDeformableMesh() :
		m_lodChain(NULL) { // Keep m_lodChain for some special clients (Editor/Previewer/Importer/Exporter)
	m_publicDim = FALSE;
	m_dimName = "";
	m_supportedMaterials = NULL;
	m_worldSort = FALSE;
	m_coreContent = false;
	m_currentMatInUse = 0;
	m_material = NULL;
	m_lodCount = 0;
	m_lodHashes = NULL;
	m_effectIndex = 0xffffffff;
	m_supportedMaterialsOwned = true;
}

CDeformableMesh::~CDeformableMesh() {
	// Keep m_lodChain for some special clients (Editor/Previewer/Importer/Exporter)
	// For Editor/Previewer/Importer/Exporter only - others do not create LOD chains so m_lodChain would be NULL for them
	if (m_lodChain) {
		m_lodChain->SafeDelete();
		delete m_lodChain;
		m_lodChain = NULL;
	}
	m_lodChain = NULL;

	if (m_material) {
		delete m_material;
		m_material = NULL;
	}
	delete m_lodHashes;
	m_lodHashes = NULL;

	// delete the m_supportedMaterials list, but only if we own it
	// (cloned objects don't own this list, ref objects do)
	if (m_supportedMaterials && m_supportedMaterialsOwned) {
		for (POSITION pos = m_supportedMaterials->GetHeadPosition(); pos != NULL;) {
			CMaterialObject* mat = (CMaterialObject*)m_supportedMaterials->GetNext(pos);
			delete mat;
		}

		delete m_supportedMaterials;
		m_supportedMaterials = NULL;
	}
}

void CDeformableMesh::Clone(CDeformableMesh** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, const CBoneList* pBoneList) {
	CDeformableMesh* copyLocal = new CDeformableMesh();

	copyLocal->m_publicDim = m_publicDim;
	copyLocal->m_dimName = m_dimName;
	copyLocal->m_worldSort = m_worldSort;

	if (m_supportedMaterials) {
		copyLocal->m_supportedMaterials = m_supportedMaterials;
		copyLocal->m_supportedMaterialsOwned = false;
	} else {
		copyLocal->m_supportedMaterials = NULL;
		copyLocal->m_supportedMaterialsOwned = true;
		copyLocal->m_material = new CMaterialObject();
	}

	copyLocal->m_lodCount = m_lodCount;

	if (m_material)
		m_material->Clone(&(copyLocal->m_material));

	if (m_lodCount > 0) {
		copyLocal->m_lodHashes = new UINT64[m_lodCount];
		CopyMemory(copyLocal->m_lodHashes, m_lodHashes, sizeof(UINT64) * m_lodCount);
	} else {
		copyLocal->m_lodHashes = NULL;
	}

	// Keep m_lodChain for some special clients (Editor/Previewer/Importer/Exporter)
	if (m_lodChain)
		m_lodChain->Clone(&copyLocal->m_lodChain, g_pd3dDevice, pBoneList);

	copyLocal->m_effectIndex = m_effectIndex;

	*clone = copyLocal;
}

void CDeformableMesh::Serialize(CArchive& ar) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	BOOL reserved = 0;
	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		ar.SetObjectSchema(5);
		ASSERT(ar.GetObjectSchema() == 5); // version 5: m_worldSort member added

		ar << m_publicDim
		   << reserved
		   << reserved;

		ar << m_dimName;

		// replaced CMaterial::Serialize() with material compression
		int matCount;
		if (m_supportedMaterials) {
			matCount = m_supportedMaterials->GetCount();
			ar << matCount;
			for (POSITION matPos = m_supportedMaterials->GetHeadPosition(); matPos != NULL;) {
				CMaterialObject* mat = (CMaterialObject*)m_supportedMaterials->GetNext(matPos);
				ar << mat->m_myCompressionIndex;
			}
		} else {
			matCount = 0;
			ar << matCount;
		}
		// end material compression

		ar << m_worldSort;
		ar << m_coreContent;

		ar << m_lodCount;
		for (unsigned int i = 0; i < m_lodCount; i++) {
			ar << m_lodHashes[i];
		}
	} else {
		int version = ar.GetObjectSchema();
		CDeformableVisList* lodChain = NULL;
		// inform the serializer that an LOD chain is starting
		MeshRM::Instance()->SetCurrentLOD(0);
		if (version < 7) {
			// lodChain removed by Jonny in version 7 now we use remesh hashes.
			ar >> lodChain;
			// convert lodChain to remesh hashes and add remeshes to global table here.

			// Keep m_lodChain for some special clients (Editor/Previewer/Importer/Exporter)
			m_lodChain = lodChain;
		}

		m_publicDim = FALSE;

		if (version > 1) {
			ar >> m_publicDim >> reserved >> reserved;
		}

		if (version > 2) {
			ar >> m_dimName;
		}
		if (version >= 4 && version < 8) {
			ar >> m_supportedMaterials;
		} else if (version >= 8) {
			int count;
			ar >> count;
			assert(m_supportedMaterials == NULL); // this should never not be NULL
			m_supportedMaterials = new CObList();
			for (int i = 0; i < count; i++) {
				int matIndex;
				CMaterialObject* mat = 0; // DRF - potentially uninitialized variable
				ar >> matIndex;
				MaterialCompressor* mc = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
				if (mc)
					mat = mc->getMaterial(matIndex, TRUE); //Just for added protection preserve all texture layers (even if disabled) for EDB for now.
				else
					assert(false); // DRF - mat unitialized
				m_supportedMaterials->AddTail(mat);
			}
		} else {
			if (lodChain && lodChain->GetCount() > 0) {
				CDeformableVisObj* firstVis = (CDeformableVisObj*)lodChain->GetHead();
				if (firstVis->m_mesh && firstVis->m_mesh->m_tempSupportedMaterials) {
					m_supportedMaterials = firstVis->m_mesh->m_tempSupportedMaterials;
				}
			}
		}
		if (version >= 5) {
			ar >> m_worldSort;
		} else {
			m_worldSort = FALSE;
		}
		if (version >= 6) {
			ar >> m_coreContent;
		}
		if (version >= 7) {
			ar >> m_lodCount;
			if (m_lodCount > 0) {
				m_lodHashes = new UINT64[m_lodCount];
			}
			for (unsigned int i = 0; i < m_lodCount; i++) {
				ar >> m_lodHashes[i];
			}
		}

		// pool the ReMaterials
		if (m_supportedMaterials) {
			POSITION fPos = m_supportedMaterials->FindIndex(0);
			if (fPos) {
				CMaterialObject* mat = (CMaterialObject*)m_supportedMaterials->GetAt(fPos);
				if (mat) {
					mat->Clone(&m_material);
				} else {
					m_material = new CMaterialObject();
				}
			} else {
				m_material = new CMaterialObject();
			}
		} else {
			m_material = new CMaterialObject();
		}

		MeshRM::Instance()->CreateReMaterials(this);
		if (version < 7) {
			m_lodCount = lodChain->GetCount();
			if (m_lodCount > 0) {
				m_lodHashes = new UINT64[m_lodCount];
				int lcnt = 0;
				;
				for (POSITION posLoc = lodChain->GetHeadPosition(); posLoc != NULL;) {
					CDeformableVisObj* dVis = (CDeformableVisObj*)lodChain->GetNext(posLoc);
					m_lodHashes[lcnt] = dVis->GetReMesh()->GetHash();
					lcnt++;
				}
			}
		}

		if (m_material) {
			for (int texLoop = 0; texLoop < NUM_CMATERIAL_TEXTURES; texLoop++)
				m_material->m_texNameHashOriginal[texLoop] = m_material->m_texNameHash[texLoop];
		}

		if (m_supportedMaterials) {
			for (POSITION pos = m_supportedMaterials->GetHeadPosition(); pos != NULL;) {
				CMaterialObject* mat = (CMaterialObject*)m_supportedMaterials->GetNext(pos);
				for (int texLoop = 0; texLoop < NUM_CMATERIAL_TEXTURES; texLoop++)
					mat->m_texNameHashOriginal[texLoop] = mat->m_texNameHash[texLoop];
			}
		}
	}
#else
	ASSERT(0);
#endif
}

void CDeformableMeshList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDeformableMesh* dPtr = (CDeformableMesh*)GetNext(posLoc);
		delete dPtr;
		dPtr = 0;
	}
	RemoveAll();
}

void VertexAssignment::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(verticesCarried);
	}
}

void VertexBoneWeight::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	pMemSizeGadget->AddObjectSizeof(this);
}

void VertexAssignmentBlended::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(vertexBoneWeights);
		pMemSizeGadget->AddObject(verticesCarried);
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	ABVERTEX0L* 	offsetVects
	-------------------------------------------------------------------------------*/
}

void CDeformableVisObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_blendedVertexAssignments); // VertexAssignmentBlended*
		pMemSizeGadget->AddObject(m_mesh); // CEXMeshObj
#ifdef _ClientOutput
		pMemSizeGadget->AddObject(m_ReMesh); // ReMesh*
#endif
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	VertexAssignment*     m_vertexAssignments;
	-------------------------------------------------------------------------------*/
}

void CDeformableVisList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CDeformableVisObj* pCDeformableVisObj = static_cast<const CDeformableVisObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCDeformableVisObj);
		}
	}
}

void CDeformableMesh::getMemSizeSupportedMaterials(IMemSizeGadget* pMemSizeGadget) const {
	if (m_supportedMaterials) {
		for (POSITION pos = m_supportedMaterials->GetHeadPosition(); pos != NULL;) {
			const CMaterialObject* pCMaterialObject = static_cast<const CMaterialObject*>(m_supportedMaterials->GetNext(pos));
			pMemSizeGadget->AddObject(pCMaterialObject);
		}
	}
}

void CDeformableMesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_dimName);
		pMemSizeGadget->AddObject(m_lodChain);
		pMemSizeGadget->AddObjectSizeof(m_lodHashes, m_lodCount);
		pMemSizeGadget->AddObject(m_material);

		// Internal getMemSize function
		getMemSizeSupportedMaterials(pMemSizeGadget);
	}
}

void CDeformableMeshList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CDeformableMesh* pCDeformableMesh = static_cast<const CDeformableMesh*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCDeformableMesh);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CDeformableVisObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CDeformableVisList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CDeformableMesh, CObList)
IMPLEMENT_SERIAL_SCHEMA(CDeformableMeshList, CObList)

} // namespace KEP
