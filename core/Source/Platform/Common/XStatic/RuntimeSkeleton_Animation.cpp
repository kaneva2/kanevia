///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "RuntimeSkeleton.h"

#include "ResourceManagers\SkeletalAnimationManager.h"

#include "CBoneClass.h"

#include "LogHelper.h"
static LogInstance("ClientEngine");

#pragma comment(linker, "/NODEFAULTLIB:LIBCP.LIB") // this is because of a bug in D3DX and VS2005

namespace KEP {

int RuntimeSkeleton::getAnimationIndexByHash(UINT64 animHash) const {
	auto animCount = getSkeletonAnimHeaderCount();
	for (size_t animIndex = 0; animIndex < animCount; ++animIndex) {
		auto pSAH = m_apSkeletonAnimHeaders[animIndex].get();
		if (pSAH && (pSAH->m_hashCode == animHash))
			return animIndex;
	}
	return ANIM_INVALID;
}

int RuntimeSkeleton::getAnimationIndexByGlid(const GLID& glid) const {
	if (!IS_VALID_GLID(glid))
		return ANIM_INVALID;

	auto animCount = getSkeletonAnimHeaderCount();
	for (size_t animIndex = 0; animIndex < animCount; ++animIndex) {
		auto pSAH = m_apSkeletonAnimHeaders[animIndex].get();
		if (pSAH && (pSAH->m_animGlid == glid))
			return animIndex;
	}
	return ANIM_INVALID;
}

// ANIM_VER_ANY finds animVer=0 first otherwise any match will do
int RuntimeSkeleton::getAnimationIndexByAnimTypeAndVer(eAnimType animType, int animVer) const {
	if (!IS_VALID_ANIM_TYPE(animType))
		return ANIM_INVALID;

	int indexAny = ANIM_INVALID;
	auto animCount = getSkeletonAnimHeaderCount();
	for (size_t animIndex = 0; animIndex < animCount; ++animIndex) {
		auto pSAH = m_apSkeletonAnimHeaders[animIndex].get();
		if (!pSAH || (pSAH->m_animType != animType))
			continue;

		if (animVer == ANIM_VER_ANY) {
			if (pSAH->m_animVer == 0)
				return animIndex;
			indexAny = animIndex;
		} else if (animVer == pSAH->m_animVer)
			return animIndex;
	}
	return indexAny;
}

GLID RuntimeSkeleton::getAnimationGlidByAnimTypeAndVer(eAnimType animType, int animVer) const {
	auto animIndex = getAnimationIndexByAnimTypeAndVer(animType, animVer);
	return isValidAnimIndex(animIndex) ? m_apSkeletonAnimHeaders[animIndex]->m_animGlid : GLID_INVALID;
}

// Animation proxy is not available at server side - CSkeletonAnimHeader::m_animDurationMs will be used
void RuntimeSkeleton::getAnimationDurations(std::map<GLID, TimeMs>& animDurationMap) {
	for (const auto& pSAH : m_apSkeletonAnimHeaders) {
		if (!IS_VALID_GLID(pSAH->m_animGlid))
			continue;
		TimeMs durationMs = pSAH->GetAnimDurationMs();
		animDurationMap.insert(std::make_pair(pSAH->m_animGlid, durationMs));
	}
}

int RuntimeSkeleton::getOrQueueAnimationIndexByGlid(const GLID& glid) {
	const size_t animSel = ANIM_PRI;

	// Animation Already Loaded ?
	auto animIndex = getAnimationIndexByGlid(glid);
	if (isValidAnimIndex(animIndex))
		return animIndex;

	// Queue Animation For Load
	queueAnimForLoad(glid);

	// If Existing Animation Valid Continue Using It During Load
	if (m_animInUse[animSel].m_pSAH)
		return m_animInUse[animSel].m_animIndex;

	// Otherwise Use Stand Animation
	return getAnimationIndexByAnimTypeAndVer(eAnimType::Stand);
}

BOOL RuntimeSkeleton::setPrimaryAnimationSettings(const AnimSettings& animSettings) {
	// Valid Settings ?
	if (!animSettings.isValid())
		return setPrimaryAnimationByIndex(ANIM_INVALID, animSettings);

	// Get Or Queue Animation Index By Glid
	auto animIndex = getOrQueueAnimationIndexByGlid(animSettings.m_glid);

	// Set Primary Animation By Index
	return setPrimaryAnimationByIndex(animIndex, animSettings);
}

BOOL RuntimeSkeleton::setPrimaryAnimationByIndex(int animIndex, const AnimSettings& animSettings) {
	// Same Settings ?
	bool isSame = (m_setAnimIndex == animIndex) && (m_setAnimSettings == animSettings);
	if (isSame)
		return TRUE;

	// DRF - ED-6846 - Other Avatar Animation Blending
	// Set Parameters Used In updateCurrentAnimationByIndex() (now called by animationCallback())
	m_setAnimIndex = animIndex;
	m_setAnimSettings = animSettings;

	return updatePrimaryAnimationByIndex(animIndex, animSettings);
}

// DRF - ED-6846 - Other Avatar Animation Blending
// Used to be part of setPrimaryAnimationByIndex(), now called by animationCallback()
// NOTE: animSettings.m_glid may not be set depending on usage. get glid from pSAH!
BOOL RuntimeSkeleton::updatePrimaryAnimationByIndex(int animIndex, const AnimSettings& animSettings) {
	const size_t animSel = ANIM_PRI;

	bool animDirForward = animSettings.m_dirForward;
	//	eAnimLoopCtl animLoopCtl = animSettings.m_loopCtl; // todo
	TimeMs animBlendDurationMs = animSettings.m_blendDurationMs;

	auto frameTimestampMs = RenderMetrics::GetFrameTimestampMs();

	// Valid Animation ?
	bool animInUseValid = isValidAnimIndex(m_animInUse[animSel].m_animIndex);
	bool animValid = isValidAnimIndex(animIndex);
	if (!animValid) {
		if (animInUseValid) {
			m_animInUse[animSel] = AnimInUse();
			m_animInUse[animSel + 1] = AnimInUse();
			setAnimTarget(GLID_INVALID);
			m_animBlendState = eAnimBlendState::Idle;
			m_animBlendDurationMs = (TimeMs)0.0;
		}
		return FALSE;
	}

	CSkeletonAnimHeader* pSAH = m_apSkeletonAnimHeaders[animIndex].get();

	GLID animGlid = pSAH ? pSAH->m_animGlid : GLID_INVALID;

	// Current Animation Valid ?
	if (!animInUseValid || (animBlendDurationMs <= 0.0)) {
		// Current Animation Invalid
		bool animChange = (animIndex != m_animInUse[animSel].m_animIndex);
		if (animChange) {
			// Animation Changed - Make It Current Animation
			TimeMs animStartTime = pSAH ? pSAH->GetAnimStartTimeMs(animDirForward) : (TimeMs)0.0;
			m_animInUse[animSel] = AnimInUse(pSAH, animIndex, animGlid, animDirForward, animStartTime);
			m_animInUse[animSel + 1] = AnimInUse();
			setAnimTarget(animDirForward ? animGlid : -animGlid);
			m_animBlendState = eAnimBlendState::Idle;
			m_animBlendDurationMs = (TimeMs)0.0;
			m_animUpdateTimeMs = (TimeMs)0.0; // DRF - ED-7870 - DO Animation API Door Glitch
		} else if (m_animInUse[animSel].m_dirForward != animDirForward) {
			m_animInUse[animSel].m_dirForward = animDirForward;
			setAnimTarget(animDirForward ? animGlid : -animGlid);
		}
	} else if (m_animBlendState == eAnimBlendState::Idle) {
		// Current Animation Valid But Not Blending
		bool animChange = (animIndex != m_animInUse[animSel].m_animIndex);
		if (animChange) {
			// Animation Changed - Start Blending
			TimeMs animStartTime = pSAH ? pSAH->GetAnimStartTimeMs(animDirForward) : (TimeMs)0.0;
			m_animInUse[animSel + 1] = AnimInUse(pSAH, animIndex, animGlid, animDirForward, animStartTime);
			setAnimTarget(animDirForward ? animGlid : -animGlid);
			if (pSAH) {
				m_animBlendState = eAnimBlendState::Blending;
				m_animBlendDurationMs = animBlendDurationMs;
				m_animBlendStartTimeMs = frameTimestampMs;
			}
			m_animUpdateTimeMs = (TimeMs)0.0; // DRF - ED-7870 - DO Animation API Door Glitch
		} else if (m_animInUse[animSel].m_dirForward != animDirForward) {
			m_animInUse[animSel].m_dirForward = animDirForward;
			setAnimTarget(animDirForward ? animGlid : -animGlid);
		}
	} else if (m_animBlendState == eAnimBlendState::Blending) {
		// Blending - Complete When Finished Or Change Forced
		auto animBlendTimeMs = frameTimestampMs - m_animBlendStartTimeMs;
		bool animBlendComplete = (animBlendTimeMs >= m_animBlendDurationMs);
		bool animChangeForced = (animIndex != m_animInUse[animSel + 1].m_animIndex);
		if (!animBlendComplete && animChangeForced) {
			// Reverse Blend And Continue If Changing Back
			bool animChangeBack = (animIndex == m_animInUse[animSel].m_animIndex);
			if (animChangeBack && pSAH && pSAH->GetAnimLooping()) {
				std::swap(m_animInUse[animSel + 1], m_animInUse[animSel]);
				m_animInUse[animSel + 1].m_dirForward = animDirForward;
				auto blendRatio = animBlendTimeMs / m_animBlendDurationMs;
				m_animBlendDurationMs = animBlendDurationMs;
				m_animBlendStartTimeMs = frameTimestampMs - (animBlendDurationMs * (1.0 - blendRatio));
				setAnimTarget(animDirForward ? animGlid : -animGlid);
				animChangeForced = false;
			}
		}

		if (animBlendComplete || animChangeForced) {
			// Finished Blending - Update Current Animation
			m_animInUse[animSel] = m_animInUse[animSel + 1];
			m_animInUse[animSel + 1] = AnimInUse();
			m_animBlendState = eAnimBlendState::Idle;
			m_animBlendDurationMs = (TimeMs)0.0;
		}
	}

	return TRUE;
}

BOOL RuntimeSkeleton::setPrimaryAnimationByAnimTypeAndVer(eAnimType animType, int animVer, bool animDirForward, TimeMs animBlendDurationMs) {
	if (m_apSkeletonAnimHeaders.empty())
		return FALSE;

	POSITION pos = m_pBoneList->FindIndex(0);
	if (!pos)
		return FALSE;

	// Set Animation Settings
	GLID glid = getAnimationGlidByAnimTypeAndVer(animType, animVer);
	if (IS_VALID_GLID(glid)) {
		AnimSettings animSettings(glid, animDirForward, eAnimLoopCtl::Default, animBlendDurationMs);
		if (!setPrimaryAnimationSettings(animSettings))
			return FALSE;
	}

	// Stand If All Else Fails
	if (!m_animInUse[ANIM_PRI].m_pSAH && !m_apSkeletonAnimHeaders.empty()) {
		glid = getAnimationGlidByAnimTypeAndVer(eAnimType::Stand);
		if (IS_VALID_GLID(glid)) {
			AnimSettings animSettings(glid, animDirForward, eAnimLoopCtl::Default, animBlendDurationMs);
			if (!setPrimaryAnimationSettings(animSettings))
				return FALSE;
		}
	}

	return TRUE;
}

bool RuntimeSkeleton::AnimInUseTimeElapsedIsPast(TimeMs timeMs, RuntimeSkeleton* pRS, bool aUseSecondaryAnimation) {
#ifdef _ClientOutput
	if (!pRS)
		return false;

	UINT animSel = aUseSecondaryAnimation ? ANIM_SEC : ANIM_PRI;
	bool reversePlay = aUseSecondaryAnimation ? false : !pRS->isAnimInUseDirForward();
	if (reversePlay) {
		auto pSAH = pRS->m_animInUse[animSel].m_pSAH;
		if (pSAH && pSAH->LoadData()) {
			auto pBAO = pSAH->GetBoneAnimationObj(0);
			if (!pBAO)
				return false;

			if (pRS->m_animInUse[animSel].m_elapsedMs >= (pBAO->m_animDurationMs - timeMs))
				return true;
		}
	} else {
		if (pRS->m_animInUse[animSel].m_elapsedMs >= timeMs)
			return true;
	}
#endif
	return false;
}

bool RuntimeSkeleton::setAnimInUseElapsedMs(size_t animSel, TimeMs elapsedMs) {
	auto pSAH = m_animInUse[animSel].m_pSAH;
	if (!pSAH)
		return false;

	elapsedMs *= pSAH->GetAnimSpeedFactor();
	if (!isAnimInUseDirForward(animSel))
		elapsedMs = -elapsedMs;

	m_animInUse[animSel].m_elapsedMs = elapsedMs;

	return true;
}

bool RuntimeSkeleton::updateAnimInUseElapsedMs(size_t animSel, TimeMs elapsedMs) {
	auto pSAH = m_animInUse[animSel].m_pSAH;
	if (!pSAH)
		return false;

	elapsedMs *= pSAH->GetAnimSpeedFactor();
	if (!isAnimInUseDirForward(animSel))
		elapsedMs = -elapsedMs;

	// Get Animation Duration
	auto animDurationMs = pSAH->GetAnimDurationMs();

	// Adjust For Crop Start Time (0.0=disabled)
	TimeMs iAnimStart = 0;
	auto cropS = pSAH->GetAnimCropStartTimeMs();
	if (cropS > 0.0)
		iAnimStart = cropS;

	// Adjust For Crop End Time (-1.0=disabled)
	auto iAnimEnd = animDurationMs;
	auto cropE = pSAH->GetAnimCropEndTimeMs();
	if (cropE != -1.0)
		iAnimEnd = cropE;

	// Adjust For Reverse Direction
	if (iAnimEnd <= iAnimStart) {
		iAnimEnd = iAnimStart + 1;
		if (iAnimEnd >= animDurationMs) {
			iAnimEnd = animDurationMs;
			iAnimStart = iAnimEnd - 1;
		}
	}
	animDurationMs = iAnimEnd - iAnimStart;

	auto animInUseElapsedMs = m_animInUse[animSel].m_elapsedMs;
	if (!pSAH->m_animLooping) {
		if (elapsedMs >= 0) {
			if ((animInUseElapsedMs + elapsedMs) > iAnimEnd)
				animInUseElapsedMs = iAnimEnd;
			else
				animInUseElapsedMs += elapsedMs;
		} else {
			if ((animInUseElapsedMs + elapsedMs) < iAnimStart)
				animInUseElapsedMs = iAnimStart;
			else
				animInUseElapsedMs += elapsedMs;
		}
	} else {
		if (elapsedMs >= 0)
			animInUseElapsedMs = iAnimStart + fmod(animInUseElapsedMs + elapsedMs, animDurationMs);
		else
			animInUseElapsedMs = iAnimStart + animDurationMs - fmod(animDurationMs - (animInUseElapsedMs + elapsedMs), animDurationMs);
	}

	m_animInUse[animSel].m_elapsedMs = animInUseElapsedMs;

	return true;
}

bool RuntimeSkeleton::updateAnimations(TimeMs animTimeMs) {
	if (!m_pBoneList)
		return false;

	// Update Animation Blending State Machine
	// ED-7903 - Fixing DO Animation Zero Glid Broke Accessory Animations
	if (!isValidAnimIndex(m_setAnimIndex) && m_setAnimSettings.isValid())
		setPrimaryAnimationSettings(m_setAnimSettings); // updates m_setAnimIndex when finished loading
	else
		updatePrimaryAnimationByIndex(m_setAnimIndex, m_setAnimSettings);

	// Do we have a current animation?
	auto pSAH_pri = m_animInUse[ANIM_PRI].m_pSAH;
	bool bHavePri = (pSAH_pri && pSAH_pri->LoadData());
	if (!bHavePri) {
		// Render without a current animation
		// Copy animation matrices from binding matrices
		int boneIndex = 0;
		for (POSITION pos = m_pBoneList->GetHeadPosition(); pos; boneIndex++) {
			auto pBO = (CBoneObject*)m_pBoneList->GetNext(pos);
			if (!pBO)
				continue;
			m_animBoneMats[boneIndex] = pBO->m_startPosition;
		}
	} else {
		// Time For Animation Update ?
		TimeMs elapsedMs = 0.0;
		auto frameTimestampMs = RenderMetrics::GetFrameTimestampMs();
		if (m_animUpdateTimeMs > 0.0) {
			elapsedMs = frameTimestampMs - m_animUpdateTimeMs;
			if (elapsedMs < m_animUpdateDelayMs) {
				// MUST update animation for a new LOD
				if (m_lastLOD != m_currentLOD)
					m_lastLOD = m_currentLOD;
				else {
					m_animUpdated = false;
					return false;
				}
			}
		}
		m_animUpdateTimeMs = frameTimestampMs;
		m_animUpdated = true;

		auto pSAH_priBlend = m_animInUse[ANIM_PRI_BLEND].m_pSAH;
		auto pSAH_sec = m_animInUse[ANIM_SEC].m_pSAH;

		bool bHavePriBlend = (pSAH_priBlend && pSAH_priBlend->LoadData());
		bool bHaveSec = (pSAH_sec && pSAH_sec->LoadData());

		eAnimBlendState animBlendState = m_animBlendState;
		if (animBlendState != eAnimBlendState::Idle) {
			// We are being asked to blend.  Make sure that both animations
			// are available, or we'll need to wait until they are before we
			// start actually blending.
			if (!bHavePri || !bHavePriBlend) {
				// reset the blend so I get a full blend when the data is ready
				// and continue playing current animation
				animBlendState = eAnimBlendState::Idle;
				m_animBlendStartTimeMs = frameTimestampMs;
			}
		} else {
			bHavePriBlend = false; // Blending disabled.
		}

		// Freeze Frame ?
		bool freezeFrame = (animTimeMs != -1.0);
		if (freezeFrame) {
			if (bHavePri)
				setAnimInUseElapsedMs(ANIM_PRI, animTimeMs);
			if (bHavePriBlend)
				setAnimInUseElapsedMs(ANIM_PRI_BLEND, animTimeMs);
			if (bHaveSec)
				setAnimInUseElapsedMs(ANIM_PRI, animTimeMs);
		} else {
			if (bHavePri)
				updateAnimInUseElapsedMs(ANIM_PRI, elapsedMs);
			if (bHavePriBlend)
				updateAnimInUseElapsedMs(ANIM_PRI_BLEND, elapsedMs);
			if (bHaveSec)
				updateAnimInUseElapsedMs(ANIM_SEC, elapsedMs);
		}

		// Get Animation Blend Ratio
		auto animBlendRatio = ((frameTimestampMs - m_animBlendStartTimeMs) / m_animBlendDurationMs);
		if (animBlendRatio > 1.0)
			animBlendRatio = 1.0;

		// All Bones Animation Key Frame Interpolation & Blending
		Matrix44fA16 matBlend1, matBlend2;
		int boneIndex = 0;
		for (POSITION pos = m_pBoneList->GetHeadPosition(); pos; ++boneIndex) {
			auto pBO = (CBoneObject*)m_pBoneList->GetNext(pos);
			if (!pBO)
				continue;

			int animSel1 = -1;
			int animSel2 = -1;
			if (bHaveSec && pBO->m_secondaryOverridable) {
				animSel1 = ANIM_SEC;
			} else {
				if (bHavePri)
					animSel1 = ANIM_PRI;
				if ((animBlendState != eAnimBlendState::Idle) && bHavePriBlend)
					animSel2 = ANIM_PRI_BLEND;
			}

			const CBoneAnimationObject* pBAO1 = (animSel1 != -1) ? m_animInUse[animSel1].m_pSAH->GetBoneAnimationObj(boneIndex) : nullptr;
			const CBoneAnimationObject* pBAO2 = (animSel2 != -1) ? m_animInUse[animSel2].m_pSAH->GetBoneAnimationObj(boneIndex) : nullptr;

			Matrix44fA16* pMatDest = &m_animBoneMats[boneIndex];
			if (pBAO1 && pBAO2) {
				// Interpolate Animation Key Frames
				pBO->InterpolateKeyFrames(m_animInUse[animSel1].m_elapsedMs, &matBlend1, pBAO1, m_animInUse[animSel1].m_pSAH->m_animLooping);
				pBO->InterpolateKeyFrames(m_animInUse[animSel2].m_elapsedMs, &matBlend2, pBAO2, m_animInUse[animSel2].m_pSAH->m_animLooping);

				// Blend Animations
				for (size_t i = 0; i < 4; ++i) {
					for (size_t j = 0; j < 3; ++j) {
						float f1 = matBlend1(i, j);
						float f2 = matBlend2(i, j);
						(*pMatDest)(i, j) = f1 + animBlendRatio * (f2 - f1);
					}
				}

				pMatDest->GetRowX().Normalize();
				pMatDest->GetRowY().Normalize();
				pMatDest->GetRowZ().Normalize();
			} else if (pBAO1) {
				pBO->InterpolateKeyFrames(m_animInUse[animSel1].m_elapsedMs, pMatDest, pBAO1, m_animInUse[animSel1].m_pSAH->m_animLooping);
			} else if (pBAO2) {
				pBO->InterpolateKeyFrames(m_animInUse[animSel2].m_elapsedMs, pMatDest, pBAO2, m_animInUse[animSel2].m_pSAH->m_animLooping);
			}
		}
	}
	updateRootBonePosition();

	m_pBoneList->computeBoneMatrices(m_invBoneMats.get(), m_animBoneMats.get());

	return true;
}

CSkeletonAnimHeader* RuntimeSkeleton::getSkeletonAnimHeaderByGlid(const GLID& glid) {
	auto animIndex = getAnimationIndexByGlid(glid);
	return getSkeletonAnimHeaderByIndex(animIndex);
}

void RuntimeSkeleton::queueAnimForLoad(const GLID& glid) {
#ifdef _ClientOutput
	if (!IS_VALID_GLID(glid)) // || glid == -1)
		return;

	for (const auto& pSAH : m_apSkeletonAnimHeaders) {
		if (pSAH->m_animGlid == glid)
			return; // Already loaded.
	}

	if (!m_glidsQueuedAnim.insert(glid).second)
		return; // Already queued.

	auto pAnimRM = AnimationRM::Instance();
	auto pAP = pAnimRM->GetAnimationProxy(glid, true);
	pAP->StartLoad();
	if (pAP->GetAnimData())
		AnimationReadyCallback(glid);
	else
		pAnimRM->RegisterResourceLoadedCallback(glid, this->shared_from_this(), std::bind(&RuntimeSkeleton::AnimationReadyCallback, this, glid));
#endif
}

void RuntimeSkeleton::AnimationReadyCallback(const GLID& glidAnim) {
#ifdef _ClientOutput
	auto itr_found = m_glidsQueuedAnim.find(glidAnim);
	if (itr_found != m_glidsQueuedAnim.end()) {
		m_glidsQueuedAnim.erase(itr_found);
	} else {
		LogError("Notified about an unrequested animation");
		return;
	}

	if (getSkeletonAnimHeaderByGlid(glidAnim))
		return; // Already loaded

	auto pAP = AnimationRM::Instance()->GetAnimationProxy(glidAnim, true);
	if (!pAP)
		return;

	auto pSAH_new = std::make_unique<CSkeletonAnimHeader>(this);

	auto pSA = AnimationRM::Instance()->GetSkeletonAnimationByGlid(glidAnim);
	if (pSA) {
		AnimationRM::Instance()->InitializeSkeletonAnimHeaderByGlid(pSAH_new.get(), glidAnim);
		pSAH_new->SetAnimLooping(pSAH_new->GetAnimLooping());
	} else {
		// dynamic load of a UGC animation from server
		// Create a dummy header now (with GLID assigned as linkage between header and proxy)
		// We will overwrite animation header when AnimationProxy is fully loaded
		pSAH_new->m_animGlid = glidAnim;
	}
	pSAH_new->SetAnimationProxy(pAP);

	// Resolve version conflict if not UGC
	if (pSAH_new->m_animType != eAnimType::None)
		resolveSkeletonAnimHeaderVersionConflict(pSAH_new->m_animType, pSAH_new->m_animVer, glidAnim);

	pSAH_new->LoadData();

	m_apSkeletonAnimHeaders.push_back(std::move(pSAH_new));
#endif
}

//! \brief Resolve version conflict with the provided animation type and version
//! \param type Animation type for conflict check (version conflict only occurs among animations of same type).
//! \param desiredVersion Animation version desired by caller. If conflict found, this version will be updated and passed back to caller.
//! \param currAnimIndex In case we're updating an existing animation, this is the index of animation to ignore while checking conflict.
//! \return TRUE if conflict found (and resolved), FALSE if no conflict.
BOOL RuntimeSkeleton::resolveSkeletonAnimHeaderVersionConflict(eAnimType animType, int& desiredAnimVer, const GLID& glidCurrAnim) {
	int resolvedAnimVer = desiredAnimVer;
	auto anims = getSkeletonAnimHeaderCount();
	for (size_t i = 0; i < anims; i++) {
		auto pSAH = getSkeletonAnimHeaderByIndex(i);
		if (pSAH && (pSAH->m_animGlid != glidCurrAnim) && (pSAH->m_animType == animType) && (pSAH->m_animVer == resolvedAnimVer))
			++resolvedAnimVer;
	}

	// Return resolved version back to caller
	desiredAnimVer = resolvedAnimVer;

	return TRUE;
}

void RuntimeSkeleton::remapSkeletonAnimHeader(CSkeletonAnimHeader* pSAH, eAnimType newAnimType, int& newAnimVer, bool bResolveConflict) {
	if (!pSAH)
		return;

	// search existing list for type/version conflict ?
	if (bResolveConflict) {
		int maxAnimVer = ANIM_VER_ANY;
		for (const auto& pSAH_find : m_apSkeletonAnimHeaders) {
			if (pSAH_find && (pSAH_find.get() != pSAH) && (pSAH_find->m_animType == newAnimType) && (pSAH_find->m_animVer == newAnimVer) && (maxAnimVer < pSAH_find->m_animVer))
				maxAnimVer = pSAH_find->m_animVer;
		}

		if (maxAnimVer != ANIM_VER_ANY)
			newAnimVer = maxAnimVer + 1;
	}

	pSAH->m_animType = newAnimType;
	pSAH->m_animVer = newAnimVer;
}

void RuntimeSkeleton::remapSkeletonAnimHeaderByGlid(const GLID& glid, eAnimType animType, int& animVer, bool bResolveConflict) {
	if (!IS_VALID_GLID(glid))
		return;

	auto pSAH = getSkeletonAnimHeaderByGlid(glid);
	if (!pSAH) {
#ifdef _ClientOutput
		auto pAP = AnimationRM::Instance()->GetAnimationProxy(glid, true);
		if (!pAP)
			return;

		// dynamic load of a UGC animation from server
		// Create a dummy header now (with GLID assigned as linkage between header and proxy)
		// We will overwrite animation header when AnimationProxy is fully loaded
		std::unique_ptr<CSkeletonAnimHeader> pSAH_new = std::make_unique<CSkeletonAnimHeader>(this);
		pSAH_new->m_animGlid = glid;
		pSAH_new->SetAnimationProxy(pAP);
		m_apSkeletonAnimHeaders.push_back(std::move(pSAH_new));
		pSAH = m_apSkeletonAnimHeaders.back().get();
#else
		return;
#endif
	}

	remapSkeletonAnimHeader(pSAH, animType, animVer, bResolveConflict);
}

} // namespace KEP
