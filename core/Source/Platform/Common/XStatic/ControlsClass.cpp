///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "ControlsClass.h"
#include "TextCommands.h"
#include "LocaleStrings.h"

namespace KEP {

bool ControlInfoList::initializedKbMapping = false;

#define MAX_JOYSTICK_NAMES 9
char* joystickNames[MAX_JOYSTICK_NAMES] = {
	"JS_BUTTON1",
	"JS_BUTTON2",
	"JS_BUTTON3",
	"JS_BUTTON4",
	"JS_BUTTON5",
	"JS_BUTTON6",
	"JS_BUTTON7",
	"JS_BUTTON8",
	"JS_BUTTON9"
};

// DRF - TODO - Mouse Only Has 4 Buttons!!!
// Since this code was written there was a change from using DIMOUSESTATE2 (8 buttons) to DIMOUSESTATE (4 buttons)
#define MAX_MOUSE_NAMES 7
char* mouseNames[MAX_MOUSE_NAMES] = {
	"Left Mouse Button",
	"Right Mouse Button",
	"Middle Mouse Button",
	"Back Button",
	"Forward Button",
	"Mouse Wheel Up",
	"Mouse Wheel Down"
};

//typedef std::map<int,CStringA> intToStringMap;
//intToStringMap keyboardCodeToName;
struct stringCodePair {
	stringCodePair(const CStringA& s, int i) {
		string = s;
		code = i;
	}
	CStringA string;
	int code;
};
typedef std::vector<stringCodePair> stringCodeVector;
stringCodeVector keyboardNames;
typedef std::map<CStringA, int> stringToIntMap;
stringToIntMap keyboardNameToCode;

void InitKeyboardNameTables() {
	keyboardNameToCode.clear();
	keyboardNames.clear();

#define KEYBOARD_ENTRY(code, str)   \
	keyboardNameToCode[str] = code; \
	keyboardNames.push_back(stringCodePair(str, code));
	//keyboardCodeToName[code] = str;

	KEYBOARD_ENTRY(DIK_F1, "F1")
	KEYBOARD_ENTRY(DIK_F2, "F2")
	KEYBOARD_ENTRY(DIK_F3, "F3")
	KEYBOARD_ENTRY(DIK_F4, "F4")
	KEYBOARD_ENTRY(DIK_F5, "F5")
	KEYBOARD_ENTRY(DIK_F6, "F6")
	KEYBOARD_ENTRY(DIK_F7, "F7")
	KEYBOARD_ENTRY(DIK_F8, "F8")
	KEYBOARD_ENTRY(DIK_F9, "F9")
	KEYBOARD_ENTRY(DIK_F10, "F10")
	KEYBOARD_ENTRY(DIK_F11, "F11")
	KEYBOARD_ENTRY(DIK_F12, "F12")

	KEYBOARD_ENTRY(DIK_NUMPAD1, "NUMPAD 1")
	KEYBOARD_ENTRY(DIK_NUMPAD2, "NUMPAD 2")
	KEYBOARD_ENTRY(DIK_NUMPAD3, "NUMPAD 3")
	KEYBOARD_ENTRY(DIK_NUMPAD4, "NUMPAD 4")
	KEYBOARD_ENTRY(DIK_NUMPAD5, "NUMPAD 5")
	KEYBOARD_ENTRY(DIK_NUMPAD6, "NUMPAD 6")
	KEYBOARD_ENTRY(DIK_NUMPAD7, "NUMPAD 7")
	KEYBOARD_ENTRY(DIK_NUMPAD8, "NUMPAD 8")
	KEYBOARD_ENTRY(DIK_NUMPAD9, "NUMPAD 9")
	KEYBOARD_ENTRY(DIK_NUMPAD0, "NUMPAD 0")

	KEYBOARD_ENTRY(DIK_NUMLOCK, "NUMLOCK")
	KEYBOARD_ENTRY(DIK_NUMPADSLASH, "NUMPAD SLASH (\\)")
	KEYBOARD_ENTRY(DIK_NUMPADSTAR, "NUMPAD STAR (*)")
	KEYBOARD_ENTRY(DIK_NUMPADENTER, "NUMPAD ENTER")
	KEYBOARD_ENTRY(DIK_NUMPADPERIOD, "NUMPAD PERIOD (.)")

	KEYBOARD_ENTRY(DIK_1, "1")
	KEYBOARD_ENTRY(DIK_2, "2")
	KEYBOARD_ENTRY(DIK_3, "3")
	KEYBOARD_ENTRY(DIK_4, "4")
	KEYBOARD_ENTRY(DIK_5, "5")
	KEYBOARD_ENTRY(DIK_6, "6")
	KEYBOARD_ENTRY(DIK_7, "7")
	KEYBOARD_ENTRY(DIK_8, "8")
	KEYBOARD_ENTRY(DIK_9, "9")
	KEYBOARD_ENTRY(DIK_0, "0")

	KEYBOARD_ENTRY(DIK_A, "A")
	KEYBOARD_ENTRY(DIK_B, "B")
	KEYBOARD_ENTRY(DIK_C, "C")
	KEYBOARD_ENTRY(DIK_D, "D")
	KEYBOARD_ENTRY(DIK_E, "E")
	KEYBOARD_ENTRY(DIK_F, "F")
	KEYBOARD_ENTRY(DIK_G, "G")
	KEYBOARD_ENTRY(DIK_H, "H")
	KEYBOARD_ENTRY(DIK_I, "I")
	KEYBOARD_ENTRY(DIK_J, "J")
	KEYBOARD_ENTRY(DIK_K, "K")
	KEYBOARD_ENTRY(DIK_L, "L")
	KEYBOARD_ENTRY(DIK_M, "M")
	KEYBOARD_ENTRY(DIK_N, "N")
	KEYBOARD_ENTRY(DIK_O, "O")
	KEYBOARD_ENTRY(DIK_P, "P")
	KEYBOARD_ENTRY(DIK_Q, "Q")
	KEYBOARD_ENTRY(DIK_R, "R")
	KEYBOARD_ENTRY(DIK_S, "S")
	KEYBOARD_ENTRY(DIK_T, "T")
	KEYBOARD_ENTRY(DIK_U, "U")
	KEYBOARD_ENTRY(DIK_V, "V")
	KEYBOARD_ENTRY(DIK_W, "W")
	KEYBOARD_ENTRY(DIK_X, "X")
	KEYBOARD_ENTRY(DIK_Y, "Y")
	KEYBOARD_ENTRY(DIK_Z, "Z")

	KEYBOARD_ENTRY(DIK_SPACE, "SPACE BAR")
	KEYBOARD_ENTRY(DIK_TAB, "TAB")
	KEYBOARD_ENTRY(DIK_ADD, "ADD")
	KEYBOARD_ENTRY(DIK_SUBTRACT, "ZSUBTRACT")
	KEYBOARD_ENTRY(DIK_ESCAPE, "ESC")
	KEYBOARD_ENTRY(DIK_BACK, "BACKSPACE")
	KEYBOARD_ENTRY(DIK_LSHIFT, "LEFT SHIFT")
	KEYBOARD_ENTRY(DIK_RSHIFT, "RIGHT SHIFT")
	KEYBOARD_ENTRY(DIK_RCONTROL, "RIGHT CONTROL")
	KEYBOARD_ENTRY(DIK_LCONTROL, "LEFT CONTROL")
	KEYBOARD_ENTRY(DIK_LMENU, "LEFT ALT")
	KEYBOARD_ENTRY(DIK_RMENU, "RIGHT ALT")
	KEYBOARD_ENTRY(DIK_UP, "UP")
	KEYBOARD_ENTRY(DIK_DOWN, "DOWN")
	KEYBOARD_ENTRY(DIK_RIGHT, "RIGHT")
	KEYBOARD_ENTRY(DIK_LEFT, "LEFT")
	KEYBOARD_ENTRY(DIK_CAPITAL, "CAPS")
	KEYBOARD_ENTRY(DIK_ADD, "ADD")

	KEYBOARD_ENTRY(DIK_MINUS, "MINUS")
	KEYBOARD_ENTRY(DIK_EQUALS, "EQUALS")
	KEYBOARD_ENTRY(DIK_LBRACKET, "LEFT BRACKET")
	KEYBOARD_ENTRY(DIK_RBRACKET, "RIGHT BRACKET")
	KEYBOARD_ENTRY(DIK_RETURN, "RETURN")
	KEYBOARD_ENTRY(DIK_SEMICOLON, "SEMICOLON")
	KEYBOARD_ENTRY(DIK_APOSTROPHE, "APOSTROPHE")
	KEYBOARD_ENTRY(DIK_GRAVE, "GRAVE")
	KEYBOARD_ENTRY(DIK_BACKSLASH, "BACKSLASH")
	KEYBOARD_ENTRY(DIK_COMMA, "COMMA")
	KEYBOARD_ENTRY(DIK_PERIOD, "PERIOD")

	KEYBOARD_ENTRY(DIK_SLASH, "FORWARD SLASH")
	KEYBOARD_ENTRY(DIK_MULTIPLY, "MULTIPLY")
	KEYBOARD_ENTRY(DIK_SCROLL, "SCROLL")
	KEYBOARD_ENTRY(DIK_DECIMAL, "PERIOD")

	KEYBOARD_ENTRY(DIK_APPS, "APPS")

	KEYBOARD_ENTRY(DIK_INSERT, "INSERT")
	KEYBOARD_ENTRY(DIK_DELETE, "DELETE")
	KEYBOARD_ENTRY(DIK_HOME, "HOME")
	KEYBOARD_ENTRY(DIK_END, "END")
	KEYBOARD_ENTRY(DIK_PRIOR, "PAGE UP")
	KEYBOARD_ENTRY(DIK_NEXT, "PAGE DOWN")
}

//------------Main object list------------------//
ControlDBObj::ControlDBObj(
	ControlObjType const& L_controlType,
	CStringA L_controlConfigName,
	ControlInfoList* L_controlInfoList) {
	m_controlType = L_controlType;
	m_controlConfigName = L_controlConfigName;
	m_controlInfoList = L_controlInfoList;
	m_invertMouse = FALSE;
	m_mouseSensitivity = .03f;
	m_allowMultiBind = FALSE;
}

void ControlDBObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CObject::Serialize(ar);
	int controlType = (int)m_controlType;
	if (ar.IsStoring()) {
		ar << m_controlConfigName
		   << m_controlInfoList
		   << controlType
		   << m_invertMouse
		   << m_allowMultiBind
		   << reserved
		   << reserved
		   << reserved
		   << m_mouseSensitivity;

	} else {
		ar >> m_controlConfigName >> m_controlInfoList >> controlType >> m_invertMouse >> m_allowMultiBind >> reserved >> reserved >> reserved >> m_mouseSensitivity;
		m_controlType = (ControlObjType)controlType;
	}
}

IMPLEMENT_SERIAL(ControlDBObj, CObject, 0)
IMPLEMENT_SERIAL(ControlDBList, CObList, 0)

ControlInfoObj::ControlInfoObj(
	ControlObjType const& L_controlType,
	int L_buttonDown,
	eCommandAction const& cmdAction,
	CStringA L_description,
	float L_miscFloat1,
	float L_miscFloat2,
	float L_miscFloat3) {
	m_miscFloat1 = L_miscFloat1;
	m_miscFloat2 = L_miscFloat2;
	m_miscFloat3 = L_miscFloat3;
	m_description = L_description;
	m_controlType = L_controlType;
	m_buttonDown = L_buttonDown;
	m_cmdAction = cmdAction;
	m_miscString = TextCommands::DEFAULT_COMMAND_STRING;
}

CStringA ControlInfoObj::GetButtonString() {
	return ControlInfoList::GetButtonString(m_buttonDown, m_controlType);
}

void ControlInfoObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	int action = (int)m_cmdAction;
	int controlType = (int)m_controlType;
	if (ar.IsStoring()) {
		ar << controlType
		   << m_buttonDown
		   << action
		   << m_description
		   << m_miscFloat1
		   << m_miscFloat2
		   << m_miscFloat3;

	} else {
		ar >> controlType >> m_buttonDown >> action >> m_description >> m_miscFloat1 >> m_miscFloat2 >> m_miscFloat3;
		m_cmdAction = (eCommandAction)action; // drf - added
		m_controlType = (ControlObjType)controlType; // drf - added
		m_miscString = TextCommands::DEFAULT_COMMAND_STRING;
	}
}

void ControlInfoList::InitKeyboardMapping() {
	if (!initializedKbMapping) {
		InitKeyboardNameTables();
		initializedKbMapping = true;
	}
}

CStringA ControlInfoList::GetButtonString(int button, ControlObjType const& type) {
	if (type == CO_TYPE_KEYBOARD) {
		InitKeyboardMapping();
		stringCodeVector::iterator it(keyboardNames.begin());
		for (; it != keyboardNames.end(); ++it)
			if (it->code == button)
				return it->string;
	} else if (type == CO_TYPE_MOUSE) {
		return CStringA(mouseNames[button]);
	} else if (type == CO_TYPE_JOYSTICK) {
		return CStringA(joystickNames[button]);
	}

	return "Invalid";
}

int ControlInfoList::GetKeyNum(const CStringA key) {
	InitKeyboardMapping();
	stringToIntMap::iterator it;
	if ((it = keyboardNameToCode.find(key)) != keyboardNameToCode.end())
		return it->second;
	return -1;
}

int ControlInfoList::GetMouseKey(const CStringA key) {
	for (int i = 0; i < MAX_MOUSE_NAMES; i++)
		if (mouseNames[i] == key)
			return i;
	return -1;
}

int ControlInfoList::GetJSNum(CStringA key) {
	for (int i = 0; i < MAX_JOYSTICK_NAMES; i++)
		if (joystickNames[i] == key)
			return i;
	return -1;
}

eCommandAction ControlInfoList::GetCommandAction(CStringA action) {
	for (int i = 0; i < (int)eCommandAction::Num_Actions; i++) {
		std::string t;
		if (loadLocaleString(i + ACTIONS_LANG_OFFSET).compare(action) == 0)
			return (eCommandAction)i;
	}
	return eCommandAction::Invalid;
}

CStringA ControlInfoList::ReturnActionByIndexAdv(int index,
	int& miscInt,
	int& miscInt2,
	int& miscInt3,
	int& miscInt4,
	CSkillObjectList* m_skillDB) {
	CStringA action = "";
	miscInt = 0;
	miscInt2 = 0;
	miscInt3 = 0;
	miscInt4 = 0;
	action = ReturnActionByIndex(index);
	if (action != "") {
		miscInt = (int)ReturnMiscFloatByActionName(action);
		return action;
	}

	//in list skill by type (magic)
	int indexCount = 59;
	int skillTypeLoc = 12;
	CSkillObject* skillDBPtrRef = m_skillDB->GetSkillByType(skillTypeLoc);
	if (skillDBPtrRef) {
		int levelLoc = 1;
		if (skillDBPtrRef->m_levelRangeDB)
			for (POSITION lrangePos = skillDBPtrRef->m_levelRangeDB->GetHeadPosition(); lrangePos != NULL;) {
				CLevelRangeObject* levelRange = (CLevelRangeObject*)skillDBPtrRef->m_levelRangeDB->GetNext(lrangePos);
				int abilityLoc = 0;
				if (levelRange->m_abilityDB)
					for (POSITION lvlPos = levelRange->m_abilityDB->GetHeadPosition(); lvlPos != NULL;) {
						CSkillAbilityObject* lvlPtr = (CSkillAbilityObject*)levelRange->m_abilityDB->GetNext(lvlPos);
						if (index == indexCount) {
							miscInt = abilityLoc;
							miscInt2 = levelLoc;
							miscInt3 = skillTypeLoc;
							miscInt4 = 50;
							return lvlPtr->m_abilityName;
						}
						abilityLoc++;
						indexCount++;
					}
				levelLoc++;
			}
	}
	return action;
}

// DRF - These strings are located in ...\WokScripts\lang\KEPLang_ENU.xml (...\star\####\Lang during runtime)
CStringA ControlInfoList::ReturnActionByIndex(int loop) {
	return loadLocaleString(loop + ACTIONS_LANG_OFFSET).c_str();
}

CStringA ControlInfoList::ReturnKeyStringByIndex(int loop) {
	int mouseOffset = 0;
	int joystickOffset = mouseOffset + MAX_MOUSE_NAMES;
	int keyboardOffset = joystickOffset + MAX_JOYSTICK_NAMES;

	if (loop < joystickOffset)
		return mouseNames[loop - mouseOffset];
	else if (loop < keyboardOffset)
		return joystickNames[loop - joystickOffset];
	else {
		InitKeyboardMapping();

		int keypos = loop - keyboardOffset;
		ASSERT(keypos >= 0);
		if (keypos < (int)keyboardNames.size())
			return keyboardNames[keypos].string;
	}

	return "";
}

CStringA ControlInfoList::GetKeyAssignmentByActionString(CStringA actionString) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		ControlInfoObj* cPtr = (ControlInfoObj*)GetNext(posLoc);
		if (cPtr->m_description == actionString)
			return cPtr->GetButtonString();
	}
	return "Unassigned";
}

ControlInfoObj* ControlInfoList::GetByActionName(CStringA actionName) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		ControlInfoObj* cPtr = (ControlInfoObj*)GetNext(posLoc);
		if (cPtr->m_description == actionName)
			return cPtr;
	}
	return NULL;
}

ControlInfoObj* ControlInfoList::UnassignByActionName(CStringA actionName) {
	POSITION posLast = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		ControlInfoObj* cPtr = (ControlInfoObj*)GetNext(posLoc);
		if (cPtr->m_description == actionName) {
			delete cPtr;
			cPtr = 0;
			RemoveAt(posLast);
		}
	}
	return NULL;
}

ControlInfoObj* ControlInfoList::UnassignByKeynum(int KeyNum) {
	POSITION posLast = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		ControlInfoObj* cPtr = (ControlInfoObj*)GetNext(posLoc);
		if (cPtr->m_buttonDown == KeyNum) {
			delete cPtr;
			cPtr = 0;
			RemoveAt(posLast);
		}
	}
	return NULL;
}

// DRF - TODO - This functions has some serious limitations and is somewhat unpredicatable.
// This function is used when we want to detect a key press to assign this key to an action
// within the game. Assuming only one key / button is pressed it should work fine. Otherwise
// the order in which they are checked affects the value returned.
CStringA ControlInfoList::GetCurrentKeyString(const MouseState& mouseState, const KeyboardState& keyboardState) {
	// Return First Key Down As String
	for (int i = DIK_FIRST_DEFINED; i <= DIK_LAST_DEFINED; i++)
		if (keyboardState.KeyDown(i))
			return GetButtonString(i, CO_TYPE_KEYBOARD);

	// Return First Mouse Button Down As String
	// DRF - TODO - Mouse Only Has 4 Buttons!!!
	// Since this code was written there was a change from using DIMOUSESTATE2 (8 buttons) to DIMOUSESTATE (4 buttons)
	for (int j = 0; j < 3; j++)
		if (mouseState.ButtonDown(j))
			return GetButtonString(j, CO_TYPE_MOUSE);

	// mouse wheel
	int posWheel = mouseState.DeltaWheel();
	if (posWheel != 0) {
		if (posWheel > 0)
			return GetButtonString(5, CO_TYPE_MOUSE);
		else
			return GetButtonString(6, CO_TYPE_MOUSE);
	}

	return "";
}

void ControlInfoList::AddControlEvent(CStringA keyString,
	CStringA actionString,
	int glid,
	int miscFloat2,
	int miscFloat3,
	int miscInt3,
	BOOL replace,
	BOOL modifyExisting) {
	ControlObjType controlType = CO_TYPE_KEYBOARD;
	int finalKeyNum = GetKeyNum(keyString);

	if (finalKeyNum == -1) {
		finalKeyNum = GetMouseKey(keyString);
		if (finalKeyNum != -1)
			controlType = CO_TYPE_MOUSE;
	}

	eCommandAction cmdAction = eCommandAction::UseSkill;
	if (miscInt3 != (int)eCommandAction::UseSkill)
		cmdAction = GetCommandAction(actionString);

	if (replace)
		UnassignByKeynum(finalKeyNum);

	ControlInfoObj* existing = GetByActionName(actionString);
	if (existing != NULL && modifyExisting == TRUE) {
		existing->m_miscFloat1 = (float)glid;
		existing->m_miscFloat2 = (float)miscFloat2;
		existing->m_miscFloat3 = (float)miscFloat3;
		existing->m_description = actionString;
		existing->m_controlType = controlType;
		existing->m_buttonDown = finalKeyNum;
		existing->m_cmdAction = cmdAction;
	} else {
		ControlInfoObj* cPtr = new ControlInfoObj(controlType,
			finalKeyNum,
			cmdAction,
			actionString,
			(float)glid,
			(float)miscFloat2,
			(float)miscFloat3);
		AddTail(cPtr);
	}
}

float ControlInfoList::ReturnMiscFloatByActionName(CStringA actionName) {
	if (actionName.IsEmpty())
		return 0.0f;

	ControlInfoObj* cPtr = GetByActionName(actionName);
	if (cPtr) {
		return cPtr->m_miscFloat1;
	}

	return 0.0f;
}

IMPLEMENT_SERIAL(ControlInfoObj, CObject, 0)
IMPLEMENT_SERIAL(ControlInfoList, CKEPObList, 0)

BEGIN_GETSET_IMPL(ControlDBObj, IDS_CONTROLDBOBJ_NAME)
GETSET_MAX(m_controlConfigName, gsCString, GS_FLAGS_DEFAULT, 0, 0, CONTROLDBOBJ, CONTROLCONFIGNAME, 255)
GETSET_OBLIST_PTR(m_controlInfoList, GS_FLAGS_DEFAULT, 0, 0, CONTROLDBOBJ, CONTROLINFOLIST)
GETSET_RANGE(m_controlType, gsInt, GS_FLAGS_DEFAULT, 0, 0, CONTROLDBOBJ, CONTROLBASIS, INT_MIN, INT_MAX)
GETSET(m_invertMouse, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, CONTROLDBOBJ, INVERTMOUSE)
GETSET(m_allowMultiBind, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, CONTROLDBOBJ, ALLOWMULTIBIND)
GETSET_RANGE(m_mouseSensitivity, gsFloat, GS_FLAGS_DEFAULT, 0, 0, CONTROLDBOBJ, MOUSESENSITIVITY, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP