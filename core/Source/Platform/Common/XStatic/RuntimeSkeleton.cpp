///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "RuntimeSkeleton.h"

#include "resource.h"
#include <SerializeHelper.h>
#include "ReSkinMesh.h"
#include "CDamagePathClass.h"
#include "CDeformableMesh.h"
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "CShadowClass.h"
#include "CSphereCollisionClass.h"
#include "CFireRangeClass.h"
#include "CBoneAnimationClass.h"
#include "CHierarchyMesh.h"
#include "CSkeletonObject.h"
#include "CBoneClass.h"
#include "SkeletalAnimationManager.h"
#include "CSkeletonAnimation.h"
#ifdef _ClientOutput
#include "CBoneAnimationClass.h"
#include "CBoneClass.h"
#include "CDamagePathClass.h"
#include "CDeformableMesh.h"
#include "CParticleEmitRange.h"
#include "CShadowClass.h"
#include "CSphereCollisionClass.h"
#include "ReMeshCreate.h"
#include "TextureManager.h"
#include "AnimationProxy.h"
#include "MeshProxy.h"
#include "../../RenderEngine/Utils/ReUtils.h"
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "ReMeshCache.h"
#include "DownloadPriority.h"
#include "CShaderLibClass.h"
#endif

#include "Intersect.h"
#include "Core/Math/Rotation.h"

#include "AnimationProxy.h"
#include "UgcConstants.h"
#include "BoundBox.h"
#include "CArmedInventoryClass.h"
#include "EquippableSystem.h"
#include "SkeletonConfiguration.h"
#include "CItemClass.h"
#include "AIclass.h"
#include "CFrameObj.h"
#include "SkeletonLabels.h"
#include "ContentService.h"
#include "TextureProvider.h"
#include "ServerSerialize.h"

#include "CMovementObj.h" // TODO: remove this dependency
#include "StreamableDynamicObj.h" // TODO: remove this dependency
#include "ReMeshCreate.h"
#include <Core/Util/CallbackSystem.h>
#include "common/include/IMemSizeGadget.h"

#undef min
#undef max

#include "LogHelper.h"
static LogInstance("ClientEngine");

#pragma comment(linker, "/NODEFAULTLIB:LIBCP.LIB") // this is because of a bug in D3DX and VS2005

namespace KEP {

RuntimeSkeleton::RuntimeSkeleton() :
		m_skeletonName("BASE"),
		m_shadowInterface(nullptr),
		m_shadowOn(false),
		m_freeLookBone(-1),
		m_freeLookPitch(0),
		m_freeLookYaw(0),
		m_pitchAxis(0.0f, 1.0f, 0.0f),
		m_yawAxis(0.0f, 1.0f, 0.0f),
		m_animVerPreference(ANIM_VER_ANY),
		m_headAngle(0.f),
		m_waistAngle(0.f),
		m_sphereLocationalDamageList(nullptr),
		m_calculationSphere(nullptr),
		m_sphereBodyCollisionSystem(nullptr),
		m_damageSphereList(nullptr),
		m_particleEmitterList(nullptr),
		m_fireQueueList(nullptr),
		m_waistBone(-1),
		m_currentLOD(0),
		m_scaleVector(1.0f, 1.0f, 1.0f),
		m_pBoneList(nullptr),
		m_invBoneMats(nullptr),
		m_animBoneMats(nullptr),
		m_meshSortValid(false),
		m_culled(true),
		m_animBlendState(eAnimBlendState::Idle),
		m_animBlendDurationMs(0.0),
		m_animUpdated(true),
		m_animUpdateTimeMs(0.0),
		m_animUpdateDelayMs(ANIM_UPDATE_DELAY_MS_MIN),
		m_loadPriorityDelta(0),
		m_rootBoneIndex(0),
		m_armedInventoryList(nullptr),
		m_isParentScaled(false),
		m_lastLOD((DWORD)-1),
		m_bAllMeshesLoaded(true),
		m_nLODDistances(0) {
	m_boxLocal.min.Set(0, 0, 0);
	m_boxLocal.max.Set(0, 0, 0);
	m_boxBones.min.Set(0, 0, 0);
	m_boxBones.max.Set(0, 0, 0);
	m_rootBonePosition.Set(0, 0, 0);
	m_ptWorldBoxCenter.Set(0);
	m_vWorldBoxHalfSize.Set(0);
	m_worldMatrix.MakeIdentity();
	m_overheadLabels = std::make_unique<SkeletonLabels>();
}

RuntimeSkeleton::~RuntimeSkeleton() {
#ifdef _ClientOutput
	ResourceManager::UnregisterAllResourceLoadedCallbacks(this);
#endif
}

UINT RuntimeSkeleton::getBoneCount() const {
	return (m_pBoneList ? m_pBoneList->GetCount() : 0);
}

size_t RuntimeSkeleton::getRigidMeshCount() const {
	size_t count = 0;
	if (m_pBoneList != nullptr) {
		for (POSITION pos = m_pBoneList->GetHeadPosition(); pos != nullptr;) {
			const CBoneObject* pBone = static_cast<CBoneObject*>(m_pBoneList->GetNext(pos));
			count += pBone->GetRigidMeshCount();
		}
	}

	return count;
}

void RuntimeSkeleton::setSkinnedMeshCount(size_t newCount) {
	m_deformableMeshArray.resize(newCount);
}

void RuntimeSkeleton::setSkinnedMesh(size_t meshSlotId, CDeformableMesh* pNewMesh) {
#ifdef _ClientOutput
	assert(meshSlotId < m_deformableMeshArray.size());
	if (meshSlotId < m_deformableMeshArray.size()) {
		m_deformableMeshArray[meshSlotId].reset(pNewMesh);
		if (pNewMesh) {
			for (unsigned int lod = 0; lod < pNewMesh->m_lodCount; lod++) {
				uint64_t meshHash = pNewMesh->m_lodHashes[lod];
				std::unique_ptr<ReMesh> pMeshClone = MeshRM::Instance()->CloneMesh(meshHash, MeshRM::RMP_ID_DYNAMIC);

				if (!pMeshClone) {
					MeshProxy* pMeshProxy = MeshRM::Instance()->GetMeshProxy(meshHash);
					pMeshClone = pMeshProxy->GetReMeshClone(); // This will start the (down)load process, if necessary
				}

				if (!pMeshClone) {
					m_bAllMeshesLoaded = false;
					MeshRM::Instance()->RegisterResourceLoadedCallback(meshHash, shared_from_this(), std::bind(&RuntimeSkeleton::MeshReadyCallback, this, meshHash));
				} else {
					pMeshClone->CreateShader(NULL);

					// The mesh in this slot should always be NULL now
					if (lod < SKELETAL_LODS_MAX)
						m_apLodMeshes[lod][meshSlotId] = std::move(pMeshClone);
				}
			}
		} else {
			for (unsigned int lod = 0; lod < pNewMesh->m_lodCount; lod++)
				m_apLodMeshes[lod][meshSlotId].reset();
		}
	}
#endif
}

void RuntimeSkeleton::disposeSkinnedMesh(size_t meshSlotId) {
	assert(meshSlotId < m_deformableMeshArray.size());
	if (meshSlotId < m_deformableMeshArray.size())
		m_deformableMeshArray[meshSlotId].reset();
}

ReMesh* RuntimeSkeleton::getLODMesh(unsigned meshSlotId, unsigned LOD) {
	assert(m_apLodMeshes[LOD].size() == m_deformableMeshArray.size());

	if (!m_apLodMeshes[LOD].empty() && meshSlotId < m_apLodMeshes->size()) {
		assert(meshSlotId < m_apLodMeshes[LOD].size());
		if (meshSlotId < m_apLodMeshes[LOD].size()) {
			return m_apLodMeshes[LOD][meshSlotId].get();
		}
	}

	return NULL;
}

void RuntimeSkeleton::clearLODMeshes(unsigned meshSlotId) {
	for (int LOD = 0; LOD < SKELETAL_LODS_MAX; LOD++) {
		// If there is already a mesh in this slot then delete it and
		// set the slot to NULL.  This is so that AssetsLoaded()
		// will know to process this slot later on.
		m_apLodMeshes[LOD][meshSlotId].reset();
	}
}

size_t RuntimeSkeleton::getEquippableItemCount() const {
	return m_armedInventoryList != nullptr ? m_armedInventoryList->GetCount() : 0;
}

const CArmedInventoryObj* RuntimeSkeleton::getEquippableItemByGLID(GLID glid) const {
	return m_armedInventoryList != nullptr ? m_armedInventoryList->getByGLID(glid) : nullptr;
}

CArmedInventoryObj* RuntimeSkeleton::getEquippableItemByGLID(GLID glid) {
	return m_armedInventoryList != nullptr ? m_armedInventoryList->getByGLID(glid) : nullptr;
}

void RuntimeSkeleton::getAllEquippedGLIDs(std::vector<GLID>& out) {
	if (m_armedInventoryList) {
		m_armedInventoryList->getAllGLIDs(out);
	}
}

void RuntimeSkeleton::getAllEquippableItems(std::vector<CArmedInventoryObj*>& out) {
	if (m_armedInventoryList) {
		m_armedInventoryList->getAllItems(out);
	}
}

void RuntimeSkeleton::getAllEquippableItems(std::vector<const CArmedInventoryObj*>& out) const {
	if (m_armedInventoryList) {
		m_armedInventoryList->getAllItems(out);
	}
}

bool RuntimeSkeleton::updateEquippableItemBoneAttachment(GLID glid, int boneIndex, const std::string& boneName) {
	auto pEquip = getEquippableItemByGLID(glid);
	if (pEquip) {
		pEquip->setAttachedBone(boneIndex, boneName);
		return true;
	}
	return false;
}

bool RuntimeSkeleton::updateEquippableItemTransform(GLID glid, const Vector3f& position, const Vector3f& rotZXY, const Vector3f& scale, bool bIgnoreParentScale) {
	// Update accessory position if it has been loaded
	auto pEquip = getEquippableItemByGLID(glid);
	if (pEquip) {
		pEquip->setTranslation(position);
		pEquip->setRotationZXY(rotZXY);
		pEquip->setScale(scale);
		pEquip->setIgnoreParentScale(bIgnoreParentScale);
		return true;
	}

	// Not found - search in the pending list instead
	return updatePendingEquippableItemPlacement(glid, position, rotZXY, scale, bIgnoreParentScale);
}

bool RuntimeSkeleton::updateEquippableItemCustomTexture(GLID glid, const std::string& textureUrl, TextureDatabase* pTextureDatabase) {
	auto pEquip = getEquippableItemByGLID(glid);
	if (pEquip) {
		auto pSkeleton = pEquip->getSkeleton();
		if (pSkeleton != nullptr) {
			pSkeleton->setCustomTexture(std::make_shared<CustomTextureProvider>(textureUrl));
			pSkeleton->updateCustomMaterials(pTextureDatabase);
		}
		return true;
	}

	// put this info into pending equips
	if (updatePendingEquippableItemTextureUrl(glid, textureUrl)) {
		// Found and updated
		return true;
	}

	return false;
}

void RuntimeSkeleton::realizeEquippableItemDimConfig(IEquippableManager* pManager) {
	if (m_armedInventoryList != nullptr) {
		for (POSITION pos = m_armedInventoryList->GetHeadPosition(); pos != NULL;) {
			CArmedInventoryObj* pEquip = static_cast<CArmedInventoryObj*>(m_armedInventoryList->GetNext(pos));
			CArmedInventoryObj* pEquipRef = pManager->GetProxy(pEquip->getGlobalId(), true)->GetData();
			if (pEquipRef != nullptr) {
				// Asset is ready for this item
				assert(pEquip->getGlobalId() == pEquipRef->getGlobalId()); // double check

				if (pEquip->getGlobalId() == pEquipRef->getGlobalId()) {
					pEquip->realizeDimConfig(eVisualType::FirstPerson, pEquipRef);
					pEquip->realizeDimConfig(eVisualType::ThirdPerson, pEquipRef);
				}
			}
		}
	}
}

bool RuntimeSkeleton::configPendingEquippableItemMeshSlot(GLID glidOrBase, size_t meshSlotId, int meshId, int matlId) {
	for (auto& item : m_pendingEquippableItems) {
		if (item.m_glidOrBase == glidOrBase) {
			if (!item.m_pCCI)
				item.m_pCCI = new CharConfigItem;

			CharConfigItem* pConfigItem = item.m_pCCI;
			if (!pConfigItem)
				return false;

			if (meshSlotId >= pConfigItem->getSlotCount()) {
				//NOTE: old logic sets r/g/b to 0 for newly created slots if initial slot count is zero
				pConfigItem->updateSlotCount(meshSlotId + 1);
			}

			CharConfigItemSlot* pSlot = pConfigItem->getSlot(meshSlotId);
			assert(pSlot != nullptr);

			pSlot->m_meshId = meshId;
			pSlot->m_matlId = matlId;
			return true;
		}
	}

	return false;
}

bool RuntimeSkeleton::updatePendingEquippableItemAnimation(GLID glid, GLID animGLID) {
	for (auto& item : m_pendingEquippableItems) {
		if (item.m_glidOrBase == glid) {
			if (!item.m_pCCI) {
				item.m_pCCI = new CharConfigItem();
			}
			item.m_pCCI->setAnimationGlid(animGLID);
			return true;
		}
	}

	return false;
}

bool RuntimeSkeleton::updatePendingEquippableItemTextureUrl(GLID glid, const std::string& textureUrl) {
	for (auto& item : m_pendingEquippableItems) {
		if (item.m_glidOrBase == glid) {
			if (!item.m_pCCI) {
				item.m_pCCI = new CharConfigItem();
				item.m_pCCI->setGlidOrBase(glid);
			}
			item.m_pCCI->setTexUrl(textureUrl);
			return true;
		}
	}

	return false;
}

bool RuntimeSkeleton::updatePendingEquippableItemPlacement(GLID glid, const Vector3f& position, const Vector3f& rotZXY, const Vector3f& scale, bool bIgnoreParentScale) {
	// If accessory load is pending, update the pending data to update position once the accessory loads.
	for (auto& item : m_pendingEquippableItems) {
		// dealing with an accessory that is pending load
		if (item.m_glidOrBase == glid) {
			if (!item.m_pPlacementOverride) {
				item.m_pPlacementOverride = std::make_unique<PendingEquippableData::PlacementOverride>();
			}

			PendingEquippableData::PlacementOverride* pPlacementOverride = item.m_pPlacementOverride.get();
			pPlacementOverride->m_ptPosition = position;
			pPlacementOverride->m_vEulerZXY = rotZXY;
			pPlacementOverride->m_vScale = scale;
			pPlacementOverride->m_bIgnoreParentScale = bIgnoreParentScale;
			return true;
		}
	}

	// Accessory not found on this object.
	return false;
}

bool RuntimeSkeleton::getAnimatedWorldPosition(Vector3f& pos) const {
	if (!m_pBoneList || m_pBoneList->GetCount() < 4) // ???
		return false;

	Vector3f outPos;
	outPos = TransformPoint_NonPerspective(m_worldMatrix, m_animBoneMats[3].GetTranslation());
	pos.x = outPos.x;
	pos.y = m_worldMatrix(3, 1);
	pos.z = outPos.z;
	return true;
}

void RuntimeSkeleton::updateWorldBox() {
	// Note: World matrix should have scale included in it.
	Vector3f ptLocalBoxCenter = m_boxLocal.GetCenter();
	Vector3f vLocalBoxHalfSize = 0.5f * m_boxLocal.GetSize();
	//for (size_t i = 0; i < 3; ++i)
	//	m_vWorldBoxHalfSize[i] = Abs(m_worldMatrix.GetColumn<3>(i)).Dot(vLocalBoxHalfSize);
	m_vWorldBoxHalfSize =
		Abs(Simd4f(m_worldMatrix[0])) * Simd4f(vLocalBoxHalfSize[0]) + Abs(Simd4f(m_worldMatrix[1])) * Simd4f(vLocalBoxHalfSize[1]) + Abs(Simd4f(m_worldMatrix[2])) * Simd4f(vLocalBoxHalfSize[2]);

	m_ptWorldBoxCenter.Set(TransformPoint_NonPerspective(m_worldMatrix, ptLocalBoxCenter), 0);
	m_fWorldRadius = m_vWorldBoxHalfSize.Length().GetScalar();
}

void RuntimeSkeleton::updateRootBonePosition() {
	if (m_pBoneList && !m_pBoneList->IsEmpty())
		m_rootBonePosition = TransformPoint_NonPerspective(m_worldMatrix, m_animBoneMats[getRootBoneIndex()].GetTranslation());
	else
		m_rootBonePosition = m_worldMatrix.GetTranslation();
}

bool RuntimeSkeleton::scaleSystem(float aNewScaleFactorX, float aNewScaleFactorY, float aNewScaleFactorZ) {
	float percentageX = aNewScaleFactorX;
	float percentageY = aNewScaleFactorY;
	float percentageZ = aNewScaleFactorZ;

	m_scaleVector.x = aNewScaleFactorX;
	m_scaleVector.y = aNewScaleFactorY;
	m_scaleVector.z = aNewScaleFactorZ;

	//maxPercentage is the greateast among x,y,z, use it to apply to the radius
	float maxPercentage = (percentageX > percentageY) ? percentageX : percentageY;
	maxPercentage = (maxPercentage > percentageZ) ? maxPercentage : percentageZ;

	if (m_sphereLocationalDamageList) {
		for (POSITION posLoc = m_sphereLocationalDamageList->GetHeadPosition(); posLoc != NULL;) {
			auto pSCO = (CSphereCollisionObj*)m_sphereLocationalDamageList->GetNext(posLoc);
			if (pSCO) {
				pSCO->scaleRadius(maxPercentage);
			}
		}
	}

	if (m_calculationSphere) {
		m_calculationSphere->scaleRadius(maxPercentage);
	}

	if (m_sphereBodyCollisionSystem) {
		for (POSITION csysPos = m_sphereBodyCollisionSystem->GetHeadPosition(); csysPos != NULL;) {
			auto pSCO = (CSphereCollisionObj*)m_sphereBodyCollisionSystem->GetNext(csysPos);
			if (pSCO) {
				pSCO->scaleRadius(maxPercentage);
			}
		}
	}

	if (m_damageSphereList) {
		for (POSITION csysPos = m_damageSphereList->GetHeadPosition(); csysPos != NULL;) {
			auto pDPO = (CDamagePathObj*)m_damageSphereList->GetNext(csysPos);
			if (pDPO)
				pDPO->m_radius = pDPO->m_radius * maxPercentage;
		}
	}

	computeBoundingBox();
	return true;
}

Matrix44f RuntimeSkeleton::getBoneMatrixByIndex(int index) {
	Matrix44f boneMatrix;
	POSITION posLoc = m_pBoneList->FindIndex(index);
	if (!posLoc) {
		boneMatrix.MakeIdentity();
		return boneMatrix;
	}
	auto pBO = (CBoneObject*)m_pBoneList->GetAt(posLoc);
	pBO->m_boneFrame->GetTransform(NULL, &boneMatrix);
	return boneMatrix;
}

CFrameObj* RuntimeSkeleton::getBoneFrameByIndex(int index) const {
	if (index < 0)
		return NULL;

	POSITION posLoc = m_pBoneList->FindIndex(index);
	if (!posLoc)
		return NULL;
	auto pBO = (CBoneObject*)m_pBoneList->GetAt(posLoc);
	return pBO->m_boneFrame;
}

Vector3f RuntimeSkeleton::getBonePositionByIndex(int index) {
	Vector3f bonePosition;
	bonePosition.x = 0.0f;
	bonePosition.y = 0.0f;
	bonePosition.z = 0.0f;
	POSITION posLoc = m_pBoneList->FindIndex(index);
	if (!posLoc)
		return bonePosition;
	auto pBO = (CBoneObject*)m_pBoneList->GetAt(posLoc);
	pBO->m_boneFrame->GetPosition(bonePosition);
	return bonePosition;
}

std::shared_ptr<RuntimeSkeleton> RuntimeSkeleton::New() {
	auto deleter = [](RuntimeSkeleton* p) { p->SafeDelete(); };
	std::shared_ptr<RuntimeSkeleton> pSkel(new RuntimeSkeleton, deleter);
	return pSkel;
}

void RuntimeSkeleton::Clone(
	std::shared_ptr<RuntimeSkeleton>* ppRS_new,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int* spawnCfgOptional,
	int* matCfgOptional,
	D3DCOLORVALUE* spawnRGBOptional,
	bool fullClone,
	bool cloneRuntimeMeshes) {
	std::shared_ptr<RuntimeSkeleton> pRS_new = RuntimeSkeleton::New();

	pRS_new->m_skeletonName = m_skeletonName;
	pRS_new->m_shadowOn = m_shadowOn;
	pRS_new->m_boxLocal = m_boxLocal;
	pRS_new->m_boxBones = m_boxBones;
	pRS_new->m_rootBonePosition = m_rootBonePosition;
	pRS_new->m_ptWorldBoxCenter = m_ptWorldBoxCenter;
	pRS_new->m_vWorldBoxHalfSize = m_vWorldBoxHalfSize;
	pRS_new->m_fWorldRadius = m_fWorldRadius;
	pRS_new->m_freeLookBone = m_freeLookBone;
	pRS_new->m_freeLookPitch = m_freeLookPitch;
	pRS_new->m_freeLookYaw = m_freeLookYaw;
	pRS_new->m_waistBone = m_waistBone;
	pRS_new->m_freeLookBone2 = m_freeLookBone2;
	pRS_new->m_pitchAxis = m_pitchAxis;
	pRS_new->m_yawAxis = m_yawAxis;

	pRS_new->m_animUpdateTimeMs = m_animUpdateTimeMs;
	pRS_new->m_animUpdateDelayMs = m_animUpdateDelayMs;

	pRS_new->m_apSkeletonAnimHeaders.resize(m_apSkeletonAnimHeaders.size());
	for (UINT i = 0; i < m_apSkeletonAnimHeaders.size(); i++)
		pRS_new->m_apSkeletonAnimHeaders[i] = std::make_unique<CSkeletonAnimHeader>(*m_apSkeletonAnimHeaders[i]);

	pRS_new->m_headAngle = m_headAngle;
	pRS_new->m_waistAngle = m_waistAngle;

	pRS_new->m_animBlendState = m_animBlendState;
	pRS_new->m_animBlendDurationMs = m_animBlendDurationMs;
	pRS_new->m_animBlendStartTimeMs = m_animBlendStartTimeMs;

	pRS_new->m_animVerPreference = ANIM_VER_ANY;

	if (m_shadowOn)
		m_shadowInterface->Clone(&pRS_new->m_shadowInterface);

	pRS_new->m_particleEmitterList = nullptr;
	if (m_particleEmitterList)
		m_particleEmitterList->Clone(&pRS_new->m_particleEmitterList);

	pRS_new->m_calculationSphere = nullptr;
	if (m_calculationSphere)
		m_calculationSphere->Clone(&pRS_new->m_calculationSphere);

	pRS_new->m_damageSphereList = nullptr;
	if (m_damageSphereList)
		m_damageSphereList->Clone(&pRS_new->m_damageSphereList);

	pRS_new->m_sphereBodyCollisionSystem = nullptr;
	if (m_sphereBodyCollisionSystem)
		m_sphereBodyCollisionSystem->Clone(&pRS_new->m_sphereBodyCollisionSystem);

	pRS_new->m_sphereLocationalDamageList = nullptr;
	if (m_sphereLocationalDamageList)
		m_sphereLocationalDamageList->Clone(&pRS_new->m_sphereLocationalDamageList);

	if (fullClone) {
#ifdef _ClientOutput
		// start of CloneBoneMeshes()
		//
		// the following code used to be in CloneMeshes, but it's specific to
		// the runtime mesh, so is here.  CloneMeshes ended up in SkeletonConfiguration
		// because of all the other stuff it does; it's mostly about setting the
		// configuration from the DB with a clause to use this code instead if
		// m_meshSlots == 0.
		//
		for (DWORD i = 0; i < SKELETAL_LODS_MAX; i++) {
			//			assert(m_deformableMeshArray.size() == m_apLodMeshes[i].size());
			//			assert(m_meshCache[i].size() == m_apLodMeshes[i].size());
			pRS_new->m_apLodMeshes[i].resize(m_apLodMeshes[i].size());
			pRS_new->m_meshCache[i].resize(m_apLodMeshes[i].size());
			for (DWORD j = 0; j < pRS_new->m_apLodMeshes[i].size(); j++) {
				pRS_new->m_apLodMeshes[i][j] = nullptr;
				pRS_new->m_meshCache[i][j] = nullptr;
			}
		}

		if (!m_deformableMeshArray.empty()) {
			pRS_new->m_deformableMeshArray.resize(m_deformableMeshArray.size());

			for (int loop = 0; loop < m_deformableMeshArray.size(); loop++) {
				// Where do I get m_meshSlots from, or how do I do
				// the correct thing here?
				//
				// Originally, the skeleton either cloned the stuff now in SkeletonConfiguration or
				// it did this, cloning the runtime stuff, if there were no alternative configurations.
				// Maybe, for the runtime skeleton object, I should always do this, then clone the
				// SkeletonConfiguration with the runtime skeleton as an argument, so it can do whatever
				// hot swapping it needs to do.
				if (cloneRuntimeMeshes && m_deformableMeshArray[loop]) {
					// if alternate configurations don't exist (cloneRuntimeMeshes == true), do this
					CDeformableMesh* pDeformableMesh = nullptr;
					m_deformableMeshArray[loop]->Clone(&pDeformableMesh, g_pd3dDevice, pRS_new->m_pBoneList);
					pRS_new->m_deformableMeshArray[loop].reset(pDeformableMesh);
					for (unsigned int lod = 0; lod < m_deformableMeshArray[loop]->m_lodCount; lod++) {
						ReMesh* pMesh = nullptr;
						m_apLodMeshes[lod][loop]->Clone(&pMesh);
						pRS_new->m_apLodMeshes[lod][loop].reset(pMesh);
					}
				} else {
					pRS_new->m_deformableMeshArray[loop] = nullptr;
				}
			}
		}

		// mark mesh sort as dirty
		pRS_new->m_meshSortValid = false;
#endif
	} else {
		pRS_new->m_pBoneList = nullptr;
	}
	CloneBones(pRS_new.get(), g_pd3dDevice);

	pRS_new->m_scaleVector = m_scaleVector;
	pRS_new->m_isParentScaled = m_isParentScaled;

	// set a default "stand" animation
	pRS_new->setPrimaryAnimationByAnimTypeAndVer(eAnimType::Stand);

	*ppRS_new = std::move(pRS_new);
}

// Pulled out because the clone is getting bloated.
void RuntimeSkeleton::CloneBones(RuntimeSkeleton* pRS, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	if (!m_pBoneList || !pRS)
		return;

	m_pBoneList->Clone(&pRS->m_pBoneList, g_pd3dDevice);
	pRS->buildHierarchy();

	// build inverse bone matrices
	if (m_pBoneList->getBoneMatrices()) {
		// allocate 16-byte aligned block of memory for both the inverse bone matrices
		pRS->m_invBoneMats.reset(new Matrix44fA16[m_pBoneList->GetCount()]);
		for (size_t i = 0; i < m_pBoneList->GetCount(); i++)
			pRS->m_invBoneMats[i] = m_invBoneMats[i];

		pRS->m_animBoneMats.reset(new Matrix44fA16[m_pBoneList->GetCount()]);
		for (size_t i = 0; i < m_pBoneList->GetCount(); i++)
			pRS->m_animBoneMats[i] = m_animBoneMats[i];
	}
}

void RuntimeSkeleton::CloneArmedInventoryList(RuntimeSkeleton* pRS, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	if (m_armedInventoryList)
		m_armedInventoryList->Clone(&pRS->m_armedInventoryList, g_pd3dDevice);
}

BOOL RuntimeSkeleton::buildHierarchy() {
	if (!m_pBoneList)
		return TRUE;

	// build bone hierarchy
	m_bonesHierarchy.clear();
	m_bonesHierarchy.resize(m_pBoneList->GetCount());
	for (int boneIndex = 0; boneIndex < m_pBoneList->GetCount(); boneIndex++) {
		auto pBO = (CBoneObject*)m_pBoneList->getBoneByIndex(boneIndex);
		if (!pBO)
			continue;

		BoneNode& boneNode = m_bonesHierarchy[boneIndex];
		boneNode.boneIndex = boneIndex;

		// Parent
		if (!pBO->m_parentName.empty())
			boneNode.parentIndex = getBoneIndexByName(pBO->m_parentName);

		// Children and siblings
		for (int boneSrchIdx = 0; boneSrchIdx < m_pBoneList->GetCount(); boneSrchIdx++) {
			CBoneObject* pBoneSrch = (CBoneObject*)m_pBoneList->getBoneByIndex(boneSrchIdx);
			if (pBoneSrch->m_parentName == pBO->m_boneName) {
				boneNode.numChildren++;
				if (boneNode.firstChildIndex == -1)
					boneNode.firstChildIndex = boneSrchIdx;
			} else if ((boneNode.nextSiblingIndex == -1) && (pBoneSrch->m_parentName == pBO->m_parentName)) {
				boneNode.nextSiblingIndex = boneSrchIdx;
			}
		}
	}

	// build the hierarchy in the CFrameObj of the bone...store the parent bone of each bone
	for (POSITION posLoc = m_pBoneList->GetHeadPosition(); posLoc != NULL;) {
		auto pBO = (CBoneObject*)m_pBoneList->GetNext(posLoc);
		for (POSITION posLoc2 = m_pBoneList->GetHeadPosition(); posLoc2 != NULL;) {
			auto pBO_find = (CBoneObject*)m_pBoneList->GetNext(posLoc2);
			if (pBO->m_parentName == pBO_find->m_boneName) {
				pBO->m_boneFrame->SetParent(pBO_find->m_boneFrame);
				break;
			}
		}
	}

	// check for procedural animation
	CBoneObject* pBO_freelook = nullptr;
	CBoneObject* pBO_freelook2 = nullptr;
	CBoneObject* pBO_waistturn = nullptr;

	if (m_freeLookBone >= 0) {
		POSITION pos = m_pBoneList->FindIndex(m_freeLookBone);
		if (pos)
			pBO_freelook = (CBoneObject*)m_pBoneList->GetAt(pos);
	}

	if (m_freeLookBone2 >= 0) {
		POSITION pos = m_pBoneList->FindIndex(m_freeLookBone2);
		if (pos)
			pBO_freelook2 = (CBoneObject*)m_pBoneList->GetAt(pos);
	}

	if (m_waistBone >= 0) {
		POSITION pos = m_pBoneList->FindIndex(m_waistBone);
		if (pos)
			pBO_waistturn = (CBoneObject*)m_pBoneList->GetAt(pos);
	}

	if (!pBO_freelook && !pBO_freelook2 && !pBO_waistturn)
		return TRUE;

	// go through the bone list
	UINT boneNum = 0;
	for (POSITION posLoc = m_pBoneList->GetHeadPosition(); posLoc != NULL;) {
		auto pBO = (CBoneObject*)m_pBoneList->GetNext(posLoc);
		UINT boneNum1 = boneNum;
		BOOL secondPass = TRUE;
		BOOL thirdPass = TRUE;

		// init the bone to have NO procedural animation
		pBO->m_procAnimmat = -1;

		// go UP from the "childPtr" bone until the sought parent bone is (not) found
		boneNum1 = boneNum;
		while (secondPass && m_bonesHierarchy[boneNum1].parentIndex != -1) {
			CBoneObject* pBO_parent = m_pBoneList->getBoneByIndex(m_bonesHierarchy[boneNum1].parentIndex);
			if (pBO_freelook && (pBO_parent->m_boneName == pBO_freelook->m_boneName || pBO->m_boneName == pBO_freelook->m_boneName)) {
				// assign the procedural anim rotation matrix to the bone
				pBO->m_procAnimmat = 0;
				thirdPass = FALSE;
				break;
			}
			// go up the parent bones
			boneNum1 = m_bonesHierarchy[boneNum1].parentIndex;
		}

		// go UP from the "childPtr" bone until the sought parent bone is (not) found
		boneNum1 = boneNum;
		while (thirdPass && m_bonesHierarchy[boneNum1].parentIndex != -1) {
			CBoneObject* pBO_parent = m_pBoneList->getBoneByIndex(m_bonesHierarchy[boneNum1].parentIndex);
			if (pBO_waistturn && (pBO_parent->m_boneName == pBO_waistturn->m_boneName || pBO->m_boneName == pBO_waistturn->m_boneName)) {
				// assign the procedural anim rotation matrix to the bone
				pBO->m_procAnimmat = 2;
				break;
			}
			// go up the parent bones
			boneNum1 = m_bonesHierarchy[boneNum1].parentIndex;
		}

		boneNum++;
	}

	return TRUE;
}

CArmedInventoryObj* RuntimeSkeleton::getCurrentHead() {
	if (!m_armedInventoryList)
		return nullptr;

	for (POSITION pos = m_armedInventoryList->GetHeadPosition(); pos != NULL;) {
		auto pAIO = static_cast<CArmedInventoryObj*>(m_armedInventoryList->GetNext(pos));
		if (pAIO && IS_HEAD_ITEM(pAIO->getLocationOccupancy()))
			return pAIO;
	}
	return nullptr;
}

void RuntimeSkeleton::EquippableReadyCallback(GLID glid, CMovementObj* pMO) {
#ifdef _ClientOutput
	if (m_pendingEquippableItems.empty())
		return;

	size_t i = 0;
	while (i < m_pendingEquippableItems.size()) {
		auto& pendingEquip = m_pendingEquippableItems[i];

		// Get Armed Inventory Object From Resource Manager
		// This expectedly returns null until the object is actually loaded and ready to go.
		auto pAIO_Ref = EquippableRM::Instance()->GetProxy(pendingEquip.m_glidOrBase, true)->GetData();
		if (!pAIO_Ref) {
			// object is not yet ready
			++i;
		} else {
			// Arm Pending Item As Attachment
			ArmEquippableItem(std::move(pendingEquip), pMO); // pendingEquip will be removed next - safe to move

			// Note: index 'i' not being incremented.
			// Remove Pending Equippable Item
			m_pendingEquippableItems.erase(m_pendingEquippableItems.begin() + i);
		}
	}

	checkFullyLoaded();
#endif
}

void RuntimeSkeleton::MeshReadyCallback(uint64_t hash) {
#ifdef _ClientOutput
	bool bAllMeshesLoaded = true;
	for (int loop = 0; loop < m_deformableMeshArray.size(); loop++) {
		if (m_deformableMeshArray[loop]) {
			for (unsigned int lod = 0; lod < m_deformableMeshArray[loop]->m_lodCount; lod++) {
				if (m_apLodMeshes[lod][loop])
					continue; // This mesh has already been loaded, probably because we have duplicates of the same mesh.

				if (m_deformableMeshArray[loop]->m_lodHashes[lod] != hash) {
					bAllMeshesLoaded = false;
					continue; // This isn't the mesh that's ready.
				}

				auto pMP = MeshRM::Instance()->GetMeshProxy(hash);
				if (pMP->IsErrored())
					continue;

				std::unique_ptr<ReMesh> pMeshClone = pMP->GetReMeshClone();
				assert(pMeshClone);
				if (!pMeshClone) {
					LogError("Mesh that was supposed to be ready could not be created. ID: " << hash);
					bAllMeshesLoaded = false;
					continue;
				}

				pMeshClone->CreateShader(NULL);
				m_apLodMeshes[lod][loop] = std::move(pMeshClone);
			}
		}
	}
	m_bAllMeshesLoaded = bAllMeshesLoaded;
	checkFullyLoaded();
#endif
}

ReMesh* RuntimeSkeleton::getMeshCacheEntry(int lod, int mesh) {
	if (lod >= 0 && lod < SKELETAL_LODS_MAX && mesh >= 0 && mesh < (int)m_meshCache[lod].size()) {
		if (!m_meshSortValid)
			sortMeshCache();

		return m_meshCache[lod][mesh];
	} else {
		return NULL;
	}
}

// get the render-state sorted mesh cache "m_meshCache"
void RuntimeSkeleton::sortMeshCache() {
#ifdef _ClientOutput
	if (m_meshSortValid)
		return;

	m_meshSortValid = true;

	// init the mesh cache
	for (size_t i = 0; i < SKELETAL_LODS_MAX; i++) {
		for (size_t j = 0; j < m_deformableMeshArray.size(); j++) {
			m_meshCache[i][j] = NULL;
			unsigned int lodCount = 0;
			CDeformableMesh* dMeshHolder = m_deformableMeshArray[j].get();

			if (dMeshHolder) {
				lodCount = dMeshHolder->m_lodCount;
			} else if (m_apLodMeshes[0][j]) {
				lodCount = 1; // assuming this is skeleton created from DO.
			}

			if (i < lodCount) {
				ReMesh* pMesh = m_apLodMeshes[i][j].get();
				m_meshCache[i][j] = pMesh;

				// set runtime vars of ReMesh
				if (pMesh) {
					if (dMeshHolder) {
						dMeshHolder->m_material->GetReMaterial()->PreLight(FALSE);
						pMesh->SetMaterial(dMeshHolder->m_material->GetReMaterial());
						pMesh->SetBones(m_pBoneList->getBoneMatrices());
						if (dMeshHolder->m_worldSort)
							pMesh->SetSorted(TRUE);
						else
							pMesh->SetSorted(FALSE);
					}
					pMesh->SetWorldMatrix(&m_worldMatrix);
					pMesh->SetWorldViewProjMatrix(&m_worldViewProjMatrix);
					pMesh->SetWorldInvTransposeMatrix(&m_worldInvTransposeMatrix);
					pMesh->SetRadius(getWorldRadius()); // Current usage (for lighting) expects a world space (scaled) radius.
					pMesh->SetLightCache(&m_lightCache);
				}
			}
		}
	}

	for (size_t i = 0; i < SKELETAL_LODS_MAX; i++) {
		std::sort(m_meshCache[i].begin(), m_meshCache[i].end(), [](ReMesh* left, ReMesh* right) {
			// Make sure NULL meshes are at the end.
			if (!left)
				return false;
			if (!right)
				return true;

			// Sort by render state, with NULL render states at the end.
			const ReRenderState* rsLeft = getRenderState(left);
			const ReRenderState* rsRight = getRenderState(right);
			if (!rsLeft)
				return false;
			if (!rsRight)
				return true;
			return rsLeft->Compare(rsRight) < 0;
		});
	}

#else
	ASSERT(FALSE);
#endif
}

void RuntimeSkeleton::invalidateShaders() {
	for (int i = 0; i < SKELETAL_LODS_MAX; i++) {
		for (unsigned int j = 0; j < m_apLodMeshes[i].size(); j++) {
			if (m_apLodMeshes[i][j])
				m_apLodMeshes[i][j]->InvalidateShader();
		}
	}
}

const ReRenderState* RuntimeSkeleton::getRenderState(const ReMesh* mesh) {
	if (mesh == NULL)
		return NULL;

	const ReMaterial* reMat1 = mesh->GetMaterial();
	if (!reMat1)
		return NULL;

	return reMat1->GetRenderState();
}

void RuntimeSkeleton::calculateShadow(const Vector3f& objectPos, CCollisionObj* collisionObject) {
	m_shadowInterface->Calculate(objectPos, collisionObject);
}

void RuntimeSkeleton::enableShadow(
	bool enable,
	float radius,
	float collisionRadius,
	float depth,
	float height,
	int textureIndex,
	const std::string& textureSearchName) {
	if (enable == FALSE) {
		m_shadowOn = false;
		if (m_shadowInterface) {
			delete m_shadowInterface;
			m_shadowInterface = 0;
		}
	} else {
		if (m_shadowInterface) {
			delete m_shadowInterface;
			m_shadowInterface = 0;
		}
		m_shadowInterface = new CShadowClass(
			radius,
			collisionRadius,
			height,
			depth,
			true,
			textureIndex,
			textureSearchName);
		m_shadowOn = true;
	}
}

void RuntimeSkeleton::SafeDelete() {
	if (m_pBoneList) {
		m_pBoneList->SafeDelete();
		m_pBoneList = 0;
	}

	if (m_shadowInterface) {
		m_shadowInterface->SafeDelete();
		delete m_shadowInterface;
		m_shadowInterface = 0;
	}

	m_deformableMeshArray.clear();

	if (m_sphereLocationalDamageList) {
		m_sphereLocationalDamageList->SafeDelete();
		delete m_sphereLocationalDamageList;
		m_sphereLocationalDamageList = 0;
	}

	if (m_particleEmitterList) {
		m_particleEmitterList->SafeDelete();
		delete m_particleEmitterList;
		m_particleEmitterList = 0;
	}

	if (m_calculationSphere) {
		delete m_calculationSphere;
		m_calculationSphere = 0;
	}

	if (m_damageSphereList) {
		m_damageSphereList->SafeDelete();
		delete m_damageSphereList;
		m_damageSphereList = 0;
	}

	if (m_sphereBodyCollisionSystem) {
		m_sphereBodyCollisionSystem->SafeDelete();
		delete m_sphereBodyCollisionSystem;
		m_sphereBodyCollisionSystem = 0;
	}

	m_invBoneMats.reset();
	m_animBoneMats.reset();

	for (int i = 0; i < SKELETAL_LODS_MAX; i++)
		m_apLodMeshes[i].clear();

	m_apSkeletonAnimHeaders.clear();

	if (m_armedInventoryList) {
		m_armedInventoryList->SafeDelete();
		delete m_armedInventoryList;
		m_armedInventoryList = 0;
	}

	m_overheadLabels.reset();

	delete this;
}

void RuntimeSkeleton::initReMeshBuffer() {
	//Init ReMesh buffer
	for (UINT i = 0; i < SKELETAL_LODS_MAX; i++) {
		m_apLodMeshes[i].resize(m_deformableMeshArray.size());
		m_meshCache[i].resize(m_deformableMeshArray.size());
	}
}

bool RuntimeSkeleton::intersectRay(const Vector3f& origin, const Vector3f& direction, Vector3f* location) {
	if (!areAllMeshesLoaded())
		return false;

	Matrix44f matWorldInverse;
	FLOAT determinant = 0.0f;
	matWorldInverse.MakeInverseOf(m_worldMatrix, &determinant);

	// MS: Mistake? This determinant test will fail for invertible world matrices that have small scale. (e.g. .01 uniform scale)
	if (fabs(determinant) <= 1e-5)
		return false;

	// transform ray by world matrix inverse, this puts ray into object space
	Vector3f osOrigin, osDirection;

	// transform origin
	osOrigin = TransformPoint_NonPerspective(matWorldInverse, origin);

	// transform and normalize direction
	osDirection = TransformVector(matWorldInverse, direction);
	osDirection.Normalize();

	// test each individual mesh for intersection
	bool intersectFound = false;
	float closestIntersect = FLT_MAX; // distance to current closest intersection point
	for (int meshIndex = 0; meshIndex < m_deformableMeshArray.size(); meshIndex++) {
		int lodToTest = 0;

		// get the mesh in this slot
		ReMesh* reMesh = getMeshCacheEntry(lodToTest, meshIndex);

		// some meshes might not be loaded yet
		if (reMesh) {
			float dist = FLT_MAX;

			if (reMesh->IntersectRay(osOrigin, osDirection, &dist, location)) {
				intersectFound = true;
				closestIntersect = std::min(closestIntersect, dist);
			}
		}
	}

	// transform point of intersection back into world space
	if (intersectFound && location)
		*location = TransformPoint_NonPerspective(m_worldMatrix, *location);

	return intersectFound;
}

bool RuntimeSkeleton::intersectSphere(const Vector3f& sphereOrigin, float radius, const Vector3f& orientation, float maxAngle, Vector3f* location /*= NULL*/) {
	if (!areAllMeshesLoaded())
		return false;

	// Invert world matrix so we can transform sphere into object space.
	Matrix44f matWorldInverse;
	FLOAT determinant = 0.0f;
	matWorldInverse.MakeInverseOf(m_worldMatrix, &determinant);

	// MS: Mistake? This determinant test will fail for invertible world matrices that have small scale. (e.g. .01 uniform scale)
	if (fabs(determinant) <= 1e-5)
		return false;

	// Transform sphere by world matrix inverse, this puts sphere into object space
	Vector3f osOrigin, osOrientation;
	float osRadius;
	D3DXVECTOR4 tmp;

	// Transform origin
	osOrigin = TransformPoint_NonPerspective(matWorldInverse, sphereOrigin);

	// Scale the radius by the longest scaled axis.
	// MS: This algorithm is incorrect if the desire is to find a bounding sphere.
	// The radius ought to be scaled by the largest eigenvalue of m_worldMatrix,
	// which could be greater than the length of a single row.
	// WorldMatrix is unlikely to have non-uniform scale (in which case
	// len0==len1==len2) so this should usually work in practice.
	float len0 = m_worldMatrix.GetRowX().LengthSquared();
	float len1 = m_worldMatrix.GetRowY().LengthSquared();
	float len2 = m_worldMatrix.GetRowZ().LengthSquared();
	float lenToUse = std::max(std::max(len0, len1), len2);
	lenToUse = sqrt(lenToUse);
	osRadius = radius / lenToUse;

	// Transform and normalize orientation
	osOrientation = TransformVector(matWorldInverse, orientation);
	osOrientation.Normalize();

	// Test each individual mesh for intersection
	bool intersectFound = false;
	float closestIntersect = FLT_MAX;

	for (int meshIndex = 0; meshIndex < m_deformableMeshArray.size(); meshIndex++) {
		int lodToTest = 0;

		// Get the mesh in this slot
		ReMesh* reMesh = getMeshCacheEntry(lodToTest, meshIndex);

		// Some meshes might not be loaded yet
		if (reMesh) {
			float dist = FLT_MAX;
			Vector3f hitLoc;

			if (reMesh->IntersectSphere(osOrigin, osRadius, osOrientation, maxAngle, &dist, &hitLoc)) {
				intersectFound = true;
				if (dist < closestIntersect) {
					closestIntersect = dist;
					if (location)
						*location = hitLoc;
				}
			}
		}
	}

	// Transform point of intersection back into world space
	if (intersectFound && location)
		*location = TransformPoint_NonPerspective(m_worldMatrix, *location);

	return intersectFound;
}

void RuntimeSkeleton::setWorldMatrix(const Matrix44f* pWorldMatrix, bool scale) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// set the world matrix
	m_worldMatrix = *pWorldMatrix;

	if (scale) {
		// scale the matrix
		m_worldMatrix.GetRowX() *= m_scaleVector.x;
		m_worldMatrix.GetRowY() *= m_scaleVector.y;
		m_worldMatrix.GetRowZ() *= m_scaleVector.z;
	}

	ReD3DX9DeviceState* dev = pICE->GetReDeviceState();
	const D3DXMATRIXA16* pProjView = dev->GetRenderState()->GetProjView();
	const Matrix44fA16& projView = *reinterpret_cast<const Matrix44f*>(pProjView);
	m_worldViewProjMatrix = m_worldMatrix * projView;
	m_worldInvTransposeMatrix.MakeInverseOf(m_worldMatrix);

	updateRootBonePosition();
	updateWorldBox();
#endif
}

void RuntimeSkeleton::setCurrentLODFromDistance(float fDist) {
	size_t nLODs = m_nLODDistances;
	size_t iLOD;
	if (nLODs <= 1) {
		iLOD = 0;
	} else {
		iLOD = nLODs - 1;
		while (iLOD > 0 && fDist < m_afLODDistances[iLOD])
			--iLOD;
	}
	setCurrentLOD(iLOD);
}

void RuntimeSkeleton::update(const Matrix44f& worldMatrix) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// resort the meshes if required
	sortMeshCache();

	// The world matrix has already been scaled, so that's why
	// UpdateWorldMatrix isn't called, and this isn't called
	// at all from GenRenderList_Dynamic()
	m_worldMatrix = worldMatrix;

	const D3DXMATRIXA16* pProjView = pICE->GetReDeviceState()->GetRenderState()->GetProjView();
	const Matrix44fA16& projView = *reinterpret_cast<const Matrix44f*>(pProjView);
	m_worldViewProjMatrix = m_worldMatrix * projView;
	m_worldInvTransposeMatrix.MakeInverseOf(m_worldMatrix);

	// separately buffer the meshes depending on material type
	ReMesh* worldTex[64];
	ReMesh* multiTex[64];
	ReMesh* singlTex[64];

	UINT numWorldtex = 0;
	UINT numMultitex = 0;
	UINT numSingltex = 0;

	for (size_t i = 0; i < m_deformableMeshArray.size(); i++) {
		int effectiveLOD = m_currentLOD;
		ReMesh* pMesh;
		while (true) {
			pMesh = m_meshCache[effectiveLOD][i];
			if (effectiveLOD == 0)
				break;
			if (pMesh)
				break;
			--effectiveLOD;
		}
		if (!pMesh)
			continue;

		// separately cache world-sorted meshes; disable vertex buffer updates if bones havn't changed
		if (pMesh->IsSorted()) {
			if (numWorldtex < 64) // DRF - Crash Fix
				worldTex[numWorldtex++] = pMesh;
		}
		// separately cache multitextured meshes; disable vertex buffer updates if bones havn't changed
		else if (pMesh->GetNumUV() > 1) {
			if (numMultitex < 64) // DRF - Crash Fix
				multiTex[numMultitex++] = pMesh;
		}
		// singly textured meshes; disable vertex buffer updates if bones havn't changed
		else {
			if (numSingltex < 64) // DRF - Crash Fix
				singlTex[numSingltex++] = m_meshCache[effectiveLOD][i];
		}
		pMesh->SetBones(m_animUpdated ? m_pBoneList->getBoneMatrices() : NULL);
	}

	// add the alpha transparent meshes to appropriate mesh cache
	if (numWorldtex)
		MeshRM::Instance()->LoadMeshCache(&worldTex[0], numWorldtex, MeshRM::RMC_ID_2);

	// add the multitextured meshes to appropriate mesh cache
	if (numMultitex)
		MeshRM::Instance()->LoadMeshCache(&multiTex[0], numMultitex, MeshRM::RMC_ID_6);

	// add the single textured meshes to appropriate mesh cache
	if (numSingltex)
		MeshRM::Instance()->LoadMeshCache(&singlTex[0], numSingltex, MeshRM::RMC_ID_5);

#else
	ASSERT(FALSE);
#endif
}
void RuntimeSkeleton::flushLightCache() {
	m_lightCache.Flush();
	if (m_armedInventoryList) {
		for (POSITION pos = m_armedInventoryList->GetHeadPosition(); pos;) {
			auto pEquip = static_cast<CArmedInventoryObj*>(m_armedInventoryList->GetNext(pos));
			if (pEquip != nullptr) {
				RuntimeSkeleton* pEquipSkel = pEquip->getSkeleton();
				if (pEquipSkel) {
					pEquipSkel->flushLightCache();
				}
			}
		}
	}
}

// TODO - Get Rid Of This Performance Issue!
BOOL RuntimeSkeleton::fadeIn(TimeMs duration) {
	if (m_armedInventoryList) {
		for (POSITION pos = m_armedInventoryList->GetHeadPosition(); pos;) {
			auto pEquip = static_cast<CArmedInventoryObj*>(m_armedInventoryList->GetNext(pos));
			if (pEquip != nullptr) {
				RuntimeSkeleton* pSkeleton = pEquip->getSkeleton();
				if (pSkeleton != nullptr) {
					pSkeleton->fadeIn(duration);
				}
			}
		}
	}

	if (!m_deformableMeshArray.empty()) {
		if (m_pBoneList) {
			for (POSITION bnPos = m_pBoneList->GetHeadPosition(); bnPos;) {
				auto pBO = (CBoneObject*)m_pBoneList->GetNext(bnPos);
				if (!pBO)
					continue;

				for (const auto& pHM : pBO->m_rigidMeshes) {
					if (!pHM || !pHM->m_lodChain)
						continue;

					for (POSITION hPos = pHM->m_lodChain->GetHeadPosition(); hPos;) {
						auto pHVO = (CHiarcleVisObj*)pHM->m_lodChain->GetNext(hPos);
						if (!pHVO || !pHVO->m_mesh || !pHVO->m_mesh->m_materialObject)
							continue;

						pHVO->m_mesh->m_materialObject->SetFadeInAlpha(duration);
					}
				}
			}
		}
	}

	return TRUE;
}

void RuntimeSkeleton::initBones() {
	computeBoundingBox();

	// build inverse bone matrices
	if (m_pBoneList) {
		// build the run-time bone array in order to get inverse bone matrices
		m_pBoneList->BuildBoneList();
		// allocate 16-byte aligned block of memory for both the inverse bone matrices
		m_invBoneMats.reset(new Matrix44fA16[m_pBoneList->GetCount()]);
		m_animBoneMats.reset(new Matrix44fA16[m_pBoneList->GetCount()]);
		for (size_t i = 0; i < m_pBoneList->GetCount(); ++i)
			m_animBoneMats[i].MakeIdentity();
		// get the inverse bone matrices
		MatrixARB::MassInvert(m_pBoneList->GetCount(), m_pBoneList->getBoneMatrices(), m_invBoneMats.get());
	}
}

BOOL RuntimeSkeleton::addBone(
	const std::string& boneName,
	const std::string& parentName,
	Vector3f dir,
	Vector3f up,
	Vector3f pos,
	Vector3f scale,
	CBoneObject** ppBO_out) {
	CBoneObject* pBO = NULL;
	pBO = new CBoneObject();
	pBO->m_boneFrame = new CFrameObj(NULL);
	pBO->m_boneName = boneName.c_str();
	pBO->m_parentName = parentName.c_str();

	pBO->m_boneFrame->SetPosDirUp(pos, dir, up);
	pBO->m_boneFrame->GetTransform(NULL, &pBO->m_startPosition);

	pBO->m_startPositionInv.MakeInverseOf(pBO->m_startPosition);
	MatrixARB::Transpose(pBO->m_startPositionTranspose, pBO->m_startPosition);
	if (!m_pBoneList)
		m_pBoneList = new CBoneList();

	m_pBoneList->AddTail(pBO);

	*ppBO_out = pBO;

	return TRUE;
}

bool RuntimeSkeleton::getBoneByName(const std::string& boneName, CBoneObject** ppBO_out, bool convertSpaceToUnderscore) const {
	for (POSITION posLoc = m_pBoneList->GetHeadPosition(); posLoc;) {
		auto pBO = (CBoneObject*)m_pBoneList->GetNext(posLoc);
		if (!pBO)
			continue;

		// Convert space to underscore for COLLADA compatibility
		CStringA tmpName = pBO->m_boneName.c_str();
		if (convertSpaceToUnderscore)
			tmpName.Replace(' ', '_');
		std::string tmpStr = (const char*)tmpName;

		if (StrSameIgnoreCase(tmpStr, boneName)) {
			*ppBO_out = pBO;
			return TRUE;
		}
	}
	return FALSE;
}

int RuntimeSkeleton::getBoneIndexByName(const std::string& boneName, bool convertSpaceToUnderscore) const {
	if (!m_pBoneList)
		return -1;

	int trace = 0;
	for (POSITION posLoc = m_pBoneList->GetHeadPosition(); posLoc; ++trace) {
		auto pBO = (CBoneObject*)m_pBoneList->GetNext(posLoc);
		if (!pBO)
			continue;

		// Convert space to underscore for COLLADA compatibility
		CStringA tmpName = pBO->m_boneName.c_str();
		if (convertSpaceToUnderscore)
			tmpName.Replace(' ', '_');
		std::string tmpStr = (const char*)tmpName;

		if (StrSameIgnoreCase(tmpStr, boneName))
			return trace;
	}

	return -1;
}

CBoneObject* RuntimeSkeleton::getBoneByIndex(int index) const {
	int trace = 0;
	for (POSITION posLoc = m_pBoneList->GetHeadPosition(); posLoc; ++trace) {
		auto pBO = (CBoneObject*)m_pBoneList->GetNext(posLoc);
		if (!pBO)
			continue;

		if (trace == index)
			return pBO;
	}

	return NULL;
}

BOOL RuntimeSkeleton::computeBoundingBox() {
	BoundBox bonesBox;
	BoundBox fullBox;

	if (m_pBoneList) {
		for (POSITION posLoc = m_pBoneList->GetHeadPosition(); posLoc;) {
			auto pBO = (CBoneObject*)m_pBoneList->GetNext(posLoc);
			if (!pBO)
				continue;

			Vector3f bonePosition;
			pBO->m_boneFrame->GetPosition(bonePosition);

			bonesBox.merge(bonePosition);

#ifdef _ClientOutput
			for (CHiarchyMesh* pHMesh : pBO->m_rigidMeshes) {
				if (!pHMesh)
					continue;

				CHierarchyVisObj* pHVis = pHMesh->GetByIndex(0);
				if (!pHVis)
					continue;

				if (pHVis->m_reMesh)
					fullBox.merge(MeshRM::Instance()->ComputeBoundingBoxWithMatrix(pHVis->m_reMesh, &pBO->m_startPosition));
				else if (pHVis->m_mesh) {
					ABBOX meshBox;
					meshBox.minX = meshBox.minY = meshBox.minZ = meshBox.maxX = meshBox.maxY = meshBox.maxZ = 0.0f;
					pHVis->m_mesh->GetBoxWithMarix(pBO->m_startPosition, &meshBox);
					fullBox.merge(meshBox);
				}
			}
#endif
		}

		// Scale the BB
		// Rigid meshes have their scale re-set after
		// baking in the scale during the import process.
		// Skinned meshes have the scale applied at runtime,
		// because it's easy to apply a scale when the mesh
		// is animated, but tricky to apply the scale on
		// import and still consider the mesh and skeleton
		// as seperate.
		bonesBox.scale(m_scaleVector);
	} else {
#ifdef _ClientOutput
		IterateMeshes(0, [&fullBox](ReMesh* pMesh) {
			fullBox.merge(MeshRM::Instance()->ComputeBoundingBoxWithMatrix(pMesh, nullptr));
		});
#endif
	}

	if (!fullBox.isValid())
		fullBox = bonesBox;
	fullBox.validate();
	setBonesBoundingBox(bonesBox);
	setLocalBoundingBox(fullBox);

	return TRUE;
}

BOOL RuntimeSkeleton::computeBoundingBoxWithSkinnedMesh(const SkeletonConfiguration* pSkeletonCfg) {
	// Compute two bounding boxes - m_boxBones: bones only, m_boxLocal: bones + rigid meshes
	computeBoundingBox();

#ifdef _ClientOutput
	// Moved from CSkeletonObject::computeBoundingBox - it overwrites m_boxLocal with the one with skinned meshes only
	// Keep it as is for now
	BoundBox fullBox = getLocalBoundingBox();

	if (pSkeletonCfg && pSkeletonCfg->m_meshSlots) {
		for (POSITION pos = pSkeletonCfg->m_meshSlots->GetHeadPosition(); pos != NULL;) {
			auto pAltUnit = static_cast<const CAlterUnitDBObject*>(pSkeletonCfg->m_meshSlots->GetNext(pos));
			POSITION posDM = pAltUnit->m_configurations->FindIndex(0); // There should only be one configuration for UGC currently
			auto pDeformableMesh = static_cast<const CDeformableMesh*>(pAltUnit->m_configurations->GetNext(posDM));
			for (unsigned int i = 0; i < pDeformableMesh->m_lodCount; i++) {
				ReMesh* rm = MeshRM::Instance()->FindMesh(pDeformableMesh->m_lodHashes[i], MeshRM::RMP_ID_PERSISTENT);
				if (rm == NULL) {
					rm = (ReSkinMesh*)MeshRM::Instance()->FindMesh(pDeformableMesh->m_lodHashes[i], MeshRM::RMP_ID_DYNAMIC);
				}
				if (rm) {
					fullBox.merge(MeshRM::Instance()->ComputeBoundingBoxWithMatrix(rm, nullptr));
				}
			}
		}
	} else if (getSkinnedMeshCount() > 0) {
		for (int i = 0; i < getSkinnedMeshCount(); i++) {
			ReMesh* rm = getMeshCacheEntry(0, i);
			if (rm) {
				fullBox.merge(MeshRM::Instance()->ComputeBoundingBoxWithMatrix(rm, nullptr));
			}
		}
	}
	fullBox.validate();
	setLocalBoundingBox(fullBox);
#endif
	return TRUE;
}

void RuntimeSkeleton::applyFreeLookRotation(float pitch, float yaw) {
	if (m_freeLookBone == -1)
		return;
	if (!m_pBoneList)
		return;
	m_freeLookPitch = pitch;
	m_freeLookYaw = yaw;
}

float RuntimeSkeleton::getInitialBoneLength(const std::string& boneName) const {
	CBoneObject* pBO = nullptr;
	if (!getBoneByName(boneName, &pBO, false) || !pBO)
		return -1;

	Vector3f offset = pBO->m_startPosition.GetTranslation();

	CBoneObject* pBO_parent = nullptr;
	if (getBoneByName(pBO->m_parentName, &pBO_parent, false) && pBO_parent)
		offset -= pBO_parent->m_startPosition.GetTranslation();

	return offset.Length();
}

// realize custom textures -- mimic the convention in streamabledynobj -- still wonder if this is necessary
// right now this would only handle materials of rigid meshes -- will think about a way for deformable mesh either merge matt's RealizeDimConfig or separate
void RuntimeSkeleton::updateCustomMaterials(TextureDatabase* textureDb, const TextureColorMap* altColorMap) {
#ifdef _ClientOutput

	// Update ReMaterial if custom texture used or mesh scaled (hierarchically)
	TextureProviderPtr currentTexture = getCurrentTexture();
	if (currentTexture || isScaled(true)) {
		// realize custom textures for rigid meshes
		for (UINT boneLoop = 0; boneLoop < getBoneCount(); boneLoop++) {
			const CBoneObject* pBO = getBoneByIndex(boneLoop);
			if (!pBO)
				continue;

			for (UINT meshLoop = 0; meshLoop < pBO->m_rigidMeshes.size(); meshLoop++) {
				CHierarchyMesh* pHMesh = pBO->m_rigidMeshes[meshLoop];
				if (!pHMesh || !pHMesh->m_lodChain)
					continue;

				for (UINT lodLoop = 0; lodLoop < pHMesh->m_lodChain->GetCount(); lodLoop++) {
					const CHierarchyVisObj* pHVis = pHMesh->GetByIndex(lodLoop);
					if (!pHVis)
						continue;

					if (pHVis->m_mesh && pHVis->m_mesh->m_materialObject) { //&& pRefHVis->m_mesh && pRefHVis->m_mesh->m_materialObject )
						CMaterialObject* pMaterial = pHVis->m_mesh->m_materialObject;
						if (!pMaterial)
							continue;

						// Re-create ReMaterial
						pMaterial->SetCustomTexture(currentTexture, CMaterialObject::CUSTOM_ALL_TEXTURE_SLOTS);
						pMaterial->m_normalizeNormals = isScaled(true) ? TRUE : FALSE; // NORMALIZENORMALS: UGC skeleton with rigid meshes
						MeshRM::Instance()->CreateReMaterial(pMaterial, "", altColorMap);
					}
				}
			}
		}
	}
#endif
}

// A new version for dynamic object where m_apMaterialObjects is properly initialized
void RuntimeSkeleton::updateCustomMaterials2(TextureDatabase* textureDb, const TextureColorMap* pAltColorMap, int customTextureOption, const std::vector<bool>& meshCustomizableFlags) {
#ifdef _ClientOutput
	// Recreate ReMaterials
	for (size_t i = 0; i < m_apMaterialObjects.size(); i++) {
		assert(i < meshCustomizableFlags.size());
		bool customizable = i < meshCustomizableFlags.size() ? meshCustomizableFlags[i] : true;
		if (customizable) {
			CMaterialObject* pMaterial = m_apMaterialObjects[i];
			pMaterial->SetCustomTexture(getCurrentTexture(), customTextureOption);
			pMaterial->m_normalizeNormals = isScaled(true) ? TRUE : FALSE; // NORMALIZENORMALS: UGC skeleton with rigid meshes
			MeshRM::Instance()->CreateReMaterial(pMaterial, "", pAltColorMap);
		}
	}

	// Reassign new ReMaterials to ReMeshes
	applyMaterials(SKELETAL_ALL_MESHES);
#endif
}

void RuntimeSkeleton::initFromDynamicObject(StreamableDynamicObject* dob, GLID glid, const TextureColorMap* altColorMap) {
#ifdef _ClientOutput
	if (!dob) {
		LogFatal("StreamableDynamicObject is null");
		return;
	}

	if (dob->getLinkedMovObjNetTraceId() != 0) {
		// Skeleton from CMovementObj
		return;
	}

	// Get Already Cached Content Metadata (don't cache if not already cached? bug?)
	ContentMetadata md(glid);
	if (md.IsValid()) {
		setUGCItemTexture(std::make_shared<UGCItemTextureProvider>(md));
	}

	// Need to initialize the bounding box before the call to getBoundingRadius() below.
	m_boxBones.min = dob->getBoundBox().min * m_scaleVector;
	m_boxBones.max = dob->getBoundBox().max * m_scaleVector;

	// Collect the visible visuals
	std::vector<StreamableDynamicObjectVisual*> apSDOV;
	for (size_t vi = 0; vi < dob->getVisualCount(); vi++) {
		StreamableDynamicObjectVisual* pVisual = dob->Visual(vi);
		if (!pVisual)
			continue;
		if (!pVisual->getIsVisible())
			continue;
		apSDOV.push_back(pVisual);
	}

	size_t renderMeshCount = apSDOV.size();
	m_deformableMeshArray.clear();
	m_deformableMeshArray.resize(renderMeshCount);

	//Init ReMesh buffer
	for (size_t i = 0; i < SKELETAL_LODS_MAX; i++) {
		m_apLodMeshes[i].clear();
		m_apLodMeshes[i].resize(renderMeshCount);
		m_meshCache[i].clear();
		m_meshCache[i].resize(renderMeshCount);
	}

	// Calculate LOD distances
	std::vector<float> afLODDistances;
	afLODDistances.push_back(0);
	for (StreamableDynamicObjectVisual* pSDOV : apSDOV) {
		if (pSDOV->getLodCount() == 1)
			continue;

		size_t nLODs = std::min<size_t>(SKELETAL_LODS_MAX, pSDOV->getLodCount());
		for (size_t i = 0; i < nLODs; i++)
			afLODDistances.push_back(pSDOV->getLODDistance(i));
	}
	std::sort(afLODDistances.begin(), afLODDistances.end());
	afLODDistances.erase(std::unique(afLODDistances.begin(), afLODDistances.end()), afLODDistances.end());
	if (afLODDistances.size() > SKELETAL_LODS_MAX) {
		// Too many LODs. Uniformly sample, keeping the highest LOD (index 0).
		for (size_t i = 0; i < SKELETAL_LODS_MAX; ++i)
			afLODDistances[i] = afLODDistances[i * afLODDistances.size() / SKELETAL_LODS_MAX];
		afLODDistances.resize(SKELETAL_LODS_MAX);
	}
	std::copy(afLODDistances.begin(), afLODDistances.end(), m_afLODDistances);
	m_nLODDistances = afLODDistances.size();

	Vector3f vBoxSize = dob->getBoundBox().GetSize() * m_scaleVector;
	float fRadius = vBoxSize.Length() * 0.5f;

	// Populate mesh and material arrays
	m_apMaterialObjects.clear();
	for (size_t vi = 0; vi < apSDOV.size(); ++vi) {
		StreamableDynamicObjectVisual* pSDOV = apSDOV[vi];

		CMaterialObject* pMaterialObject = pSDOV->getMaterial();
		m_apMaterialObjects.push_back(pMaterialObject);

		for (size_t iLOD = 0; iLOD < m_nLODDistances; ++iLOD) {
			ReMesh* clonedReMesh = NULL;
			size_t iVisualLOD = pSDOV->getDistanceLOD(m_afLODDistances[iLOD]);
			ReMesh* srcMesh = pSDOV->getReMesh(iVisualLOD);
			if (!srcMesh)
				continue;

			srcMesh->Clone(&clonedReMesh, true);
			if (!clonedReMesh)
				continue;

			m_apLodMeshes[iLOD][vi].reset(clonedReMesh);

			clonedReMesh->CreateShader(NULL);

			// set transform state of ReMesh
			clonedReMesh->SetWorldMatrix(&m_worldMatrix);
			clonedReMesh->SetWorldViewProjMatrix(&m_worldViewProjMatrix);
			clonedReMesh->SetWorldInvTransposeMatrix(&m_worldInvTransposeMatrix);
			clonedReMesh->SetRadius(fRadius);
			clonedReMesh->SetLightCache(&m_lightCache);
			CMaterialObject* matPtr = pSDOV->getMaterial();
			if (matPtr) {
				matPtr->SetCustomTexture(getCurrentTexture(), CMaterialObject::CUSTOM_ALL_TEXTURE_SLOTS);
				MeshRM::Instance()->CreateReMaterial(matPtr, "", altColorMap);
				clonedReMesh->SetMaterial(matPtr->GetReMaterial());
			}
		}
	}

	computeBoundingBox();
#endif
}

bool RuntimeSkeleton::addParticle(
	const std::string& boneName,
	int particleSlot,
	GLID particleGlid,
	int particleDBIndex,
	const std::string& particleEffectName,
	const Vector3f& offset,
	const Vector3f& dir,
	const Vector3f& up) {
	if (particleGlid == INVALID_GLID && particleEffectName.empty() && particleDBIndex == PARTICLE_DB_INDEX_INVALID)
		return false;

	int boneIndex = -1;
	if (!boneName.empty()) {
		boneIndex = getBoneIndexByName(boneName);
		if (boneIndex == -1)
			return false;
	}

	LogInfo("glid<" << particleGlid << "> bone='" << boneName << "' slot=" << particleSlot << " name='" << particleEffectName << "'");

	if (!m_particleEmitterList) {
		m_particleEmitterList = new CParticleEmitRangeObjList;
	} else {
		//Remove existing emitter in the same slot
		removeParticle(boneName, particleSlot);
	}

	auto pPERO_new = new CParticleEmitRangeObj(boneIndex, particleSlot, particleGlid, 0);
	pPERO_new->setMatrix(offset, dir, up);
	pPERO_new->setParticleSystemName(particleEffectName);
	pPERO_new->setParticleDBIndex(particleDBIndex);

	m_particleEmitterList->AddTail(pPERO_new);

	return true;
}

bool RuntimeSkeleton::removeParticle(const std::string& boneName, int particleSlotId) {
	if (!m_particleEmitterList)
		return false;

	int boneIndex = -1;
	if (!boneName.empty()) {
		boneIndex = getBoneIndexByName(boneName);
		if (boneIndex == -1)
			return false;
	}

	POSITION posCurr = NULL;
	for (POSITION pos = posCurr = m_particleEmitterList->GetHeadPosition(); pos; posCurr = pos) {
		auto pPERO = (CParticleEmitRangeObj*)m_particleEmitterList->GetNext(pos);
		if (!pPERO)
			continue;
		if (pPERO->getBoneIndex() == boneIndex && pPERO->getParticleSlotId() == particleSlotId) {
			m_particleEmitterList->RemoveAt(posCurr);
			pPERO->SafeDelete();
			delete pPERO;
		}
	}
	return true;
}

UINT RuntimeSkeleton::getParticleRuntimeId(const std::string& boneName, int particleSlotId) {
	if (!m_particleEmitterList)
		return 0;

	int boneIndex = -1;
	if (!boneName.empty()) {
		boneIndex = getBoneIndexByName(boneName);
		if (boneIndex == -1)
			return false;
	}

	POSITION posCurr = NULL;
	for (POSITION pos = posCurr = m_particleEmitterList->GetHeadPosition(); pos; posCurr = pos) {
		auto pPERO = (CParticleEmitRangeObj*)m_particleEmitterList->GetNext(pos);
		if (!pPERO)
			continue;
		if (pPERO->getBoneIndex() == boneIndex && pPERO->getParticleSlotId() == particleSlotId)
			return pPERO->getRuntimeParticleId();
	}
	return 0;
}

void RuntimeSkeleton::writeLegacySkeletonObjectV9(CArchive& ar, const SkeletonConfiguration* pSkeletonConfig) {
	ar << CStringA(m_skeletonName.c_str())
	   << m_pBoneList
	   << (int)0 //m_frameSpeed
	   << (int)0 //m_ticksPerFrame
	   << m_deformableMeshArray.size()
	   << (isShadowEnabled() ? TRUE : FALSE)
	   << getShadow()
	   << m_freeLookBone
	   << m_sphereLocationalDamageList
	   << m_calculationSphere
	   << m_sphereBodyCollisionSystem
	   << m_damageSphereList
	   << m_particleEmitterList
	   << m_waistBone
	   << 0.0f //m_waistTurn
	   << 0.0f //m_waistTurnMax
	   << 0.0f //m_waistTurnRate
	   << m_fireQueueList;

	// Maybe this class needs to be sperated out now,
	// so that we can load things like this in a seperate
	// serialization function and then give CSkeletonObject
	// or SkeletonConfiguration the appropriate values.
	if (::ServerSerialize)
		ar << (CAlterUnitDBObjectList*)0; // server doesn't need this
	else {
		assert(pSkeletonConfig != nullptr);
		ar << pSkeletonConfig->m_meshSlots;
	}

	ar << (int)0 //m_secondaryCOMInheritBone
	   << m_freeLookBone2
	   << (BOOL)0 //reserved
	   << (BOOL)0; //reserved;

	// version 3 starts here
	ar << m_pitchAxis.x;
	ar << m_pitchAxis.y;
	ar << m_pitchAxis.z;
	ar << m_yawAxis.x;
	ar << m_yawAxis.y;
	ar << m_yawAxis.z;
	ar << (BOOL)FALSE; //m_normalsSmoothed;

	// CSkeletonAnimation manager support
	ar << m_apSkeletonAnimHeaders.size()
	   << (UINT)0; //m_animVersion;

	// write out the animation headers
	for (const auto& pSAH : m_apSkeletonAnimHeaders) {
		if (pSAH)
			pSAH->Write(ar);
	}

	//Version 9 of CSkeleonObject has same structure as version 8, except CSkeletonAnimHeader is in a new schema.
}

void RuntimeSkeleton::readLegacySkeletonObject(CArchive& ar, SkeletonConfiguration* pSkeletonConfig) {
	int version = ar.GetObjectSchema();
	assert(version >= 8);
	if (version < 8) {
		LogError("CSkeletonObject version " << version << " is no longer supported. Require version 8 or later. Archive: " << ar.GetFile()->GetFileName());
		AfxThrowArchiveException(CArchiveException::badSchema);
	}

	UINT numAnimations = 0;
	int deprecated;
	BOOL shadowOn = FALSE;
	int meshSlotCount = 0;
	CStringA skeletonName;
	float depF;
	ar >> skeletonName >> m_pBoneList >> deprecated //m_runtimeSkeleton->m_frameSpeed
		>> deprecated //m_runtimeSkeleton->m_ticksPerFrame
		>> meshSlotCount >> shadowOn >> m_shadowInterface >> m_freeLookBone >> m_sphereLocationalDamageList >> m_calculationSphere >> m_sphereBodyCollisionSystem >> m_damageSphereList >> m_particleEmitterList >> m_waistBone >> depF //m_waistTurn
		>> depF //m_waistTurnMax
		>> depF //m_waistTurnRate
		>> m_fireQueueList;

	// EmitterList holds animation-triggered particle effects.
	// We still use this system, but we shouldn't.
	// We should trigger animation particles as the animation changes, not
	// poll all emitters every frame to determine whether to activate them.
	//delete m_particleEmitterList;
	//m_particleEmitterList = nullptr;

	m_skeletonName = skeletonName.GetString();
	setShadowEnabled(shadowOn == TRUE);
	buildHierarchy();

	m_deformableMeshArray.clear();
	m_deformableMeshArray.resize(meshSlotCount);

	initBones();

	if (!::ServerSerialize) {
		// set these prior to streaming in CDeformableVisObj's
		MeshRM::Instance()->SetCurrentBoneList(m_pBoneList);
	}

	// init the mesh cache
	for (size_t i = 0; i < SKELETAL_LODS_MAX; i++) {
		m_meshCache[i].resize(meshSlotCount);
		m_apLodMeshes[i].resize(meshSlotCount);
	}

	int secondaryCOMInheritBone_Deprecated;
	BOOL reserved = 0;

	ar >> pSkeletonConfig->m_meshSlots >> secondaryCOMInheritBone_Deprecated >> m_freeLookBone2 >> reserved >> reserved >> m_pitchAxis.x >> m_pitchAxis.y >> m_pitchAxis.z >> m_yawAxis.x >> m_yawAxis.y >> m_yawAxis.z;

	pSkeletonConfig->m_charConfig.initBaseItem(meshSlotCount);

	BOOL normalsSmoothed_Deprecated = FALSE;
	ar >> normalsSmoothed_Deprecated;

	// version 8 stores the animation headers, NOT the animation indexes
	// which means we can stream the rest of the data and don't need the ANM file
	ar >> numAnimations;
	UINT animVersion_Deprecated;
	ar >> animVersion_Deprecated;

	// read in the animation headers
	for (UINT i = 0; i < numAnimations; i++) {
		std::unique_ptr<CSkeletonAnimHeader> pAnimHdr = std::make_unique<CSkeletonAnimHeader>(this);
		pAnimHdr->Read(ar);
		m_apSkeletonAnimHeaders.push_back(std::move(pAnimHdr));
	}

	// copy extra data from CSkeletonAnimation
	if (!::ServerSerialize) {
		auto anims = getSkeletonAnimHeaderCount();
		for (size_t i = 0; i < anims; i++) {
			auto pSAH = getSkeletonAnimHeaderByIndex(i);
			if (!pSAH)
				continue;

			if (version < 9) {
				auto pSA = AnimationRM::Instance()->GetSkeletonAnimationByHash(pSAH->m_hashCode);
				if (pSA) {
					pSAH->m_animGlid = pSA->m_animGlid;
					pSAH->m_animDurationMs = pSA->m_animDurationMs;
					pSAH->m_animFrameTimeMs = pSA->m_animFrameTimeMs;
					pSAH->SetAnimationProxy(AnimationRM::Instance()->GetAnimationProxy(pSAH->m_animGlid));
				}
			}

			if (IS_VALID_GLID(pSAH->m_animGlid))
				pSAH->SetAnimationProxy(AnimationRM::Instance()->GetAnimationProxy(pSAH->m_animGlid));
		}
	}

	for (int i = 0; i < 4; i++)
		m_animInUse[i] = AnimInUse();

	m_animVerPreference = ANIM_VER_ANY;

	// Convert old version CParticleEmitRangeObj if necessary
	if (m_particleEmitterList) {
		for (POSITION pos = m_particleEmitterList->GetHeadPosition(); pos;) {
			auto pPERO = dynamic_cast<CParticleEmitRangeObj*>(m_particleEmitterList->GetNext(pos));
			if (!pPERO)
				continue;

			// Type is not yet assigned ?
			if (!IS_VALID_ANIM_TYPE(pPERO->getAnimationType())) {
				auto animIndex = pPERO->getLegacyAnimationIndex();
				auto pSAH = getSkeletonAnimHeaderByIndex(animIndex);
				if (pSAH)
					pPERO->setAnimationInfo(pSAH->m_animType, pSAH->m_animVer);
			}
		}
	}

	if (m_damageSphereList) {
		for (POSITION pos = m_damageSphereList->GetHeadPosition(); pos;) {
			auto pDamagePathObj = dynamic_cast<CDamagePathObj*>(m_damageSphereList->GetNext(pos));
			if (!pDamagePathObj)
				continue;

			if (!IS_VALID_ANIM_TYPE(pDamagePathObj->m_enableOnAnimationType)) {
				auto animIndex = pDamagePathObj->GetLegacyAnimationIndex();
				auto pSAH = getSkeletonAnimHeaderByIndex(animIndex);
				if (pSAH)
					pDamagePathObj->SetAnimationInfo(pSAH->m_animType, pSAH->m_animVer);
			}
		}
	}
}

TextureProviderPtr RuntimeSkeleton::getCurrentTexture() {
	if (m_customTexture)
		return std::static_pointer_cast<TextureProvider, CustomTextureProvider>(m_customTexture);
	if (m_ugcItemTexture)
		return std::static_pointer_cast<TextureProvider, UGCItemTextureProvider>(m_ugcItemTexture);
	return TextureProviderPtr();
}

void RuntimeSkeleton::SetMaterialObject(size_t i, CMaterialObject* pMaterialObject, bool applyToMeshes, const TextureColorMap* pAltColorMap) {
	if (i >= m_apMaterialObjects.size())
		return;
	m_apMaterialObjects[i] = pMaterialObject;
	if (applyToMeshes) {
		MeshRM::Instance()->CreateReMaterial(pMaterialObject, "", pAltColorMap);
		applyMaterials(i);
	}
}

void RuntimeSkeleton::applyMaterials(size_t meshIndex) {
	// Reapply ReMaterial to ReMesh
	for (size_t iLOD = 0; iLOD < SKELETAL_LODS_MAX; iLOD++) {
		auto& apLodMeshes = m_apLodMeshes[iLOD];

		size_t indexStart = meshIndex == SKELETAL_ALL_MESHES ? 0 : meshIndex;
		size_t indexEnd = meshIndex == SKELETAL_ALL_MESHES ? apLodMeshes.size() : (meshIndex + 1);

		for (size_t i = indexStart; i < indexEnd; ++i) {
			if (apLodMeshes[i]) {
				assert(i < m_apMaterialObjects.size() && m_apMaterialObjects[i] != nullptr); // Currently assertion fails for NPCs. To be fixed.
				if (i < m_apMaterialObjects.size() && m_apMaterialObjects[i] != nullptr) {
					apLodMeshes[i]->SetMaterial(m_apMaterialObjects[i]->GetReMaterial());
				}
			}
		}
	}
}

void RuntimeSkeleton::applyTexturePanningSettings(const std::map<UINT, Vector2f>& texturePanningSettings, USHORT meshIdFilter, USHORT uvSetIdFilter) {
	std::map<size_t, CMaterialObject*> affectedMaterials;

	for (const auto& it : texturePanningSettings) {
		UINT key = it.first;
		USHORT meshId = key >> 16;
		USHORT uvSetId = key & 0xffff;

		bool ok = true && (meshIdFilter == SKELETAL_ALL_MESHES || meshIdFilter == meshId) && (uvSetIdFilter == MATERIAL_ALL_UVSETS || uvSetIdFilter == uvSetId) && (uvSetId < MAX_UV_SETS) && (meshId < m_apMaterialObjects.size());
		if (!ok)
			continue;

		CMaterialObject* pMaterial = m_apMaterialObjects[meshId];
		if (!pMaterial)
			continue;

		pMaterial->m_enableTextureMovement = TRUE;
		pMaterial->m_enableSet[uvSetId] = TRUE;
		pMaterial->m_textureMovement[uvSetId].tu = it.second.x;
		pMaterial->m_textureMovement[uvSetId].tv = it.second.y;
		affectedMaterials.insert(std::make_pair((size_t)meshId, pMaterial));
	}

	MeshRM* pMeshRM = MeshRM::Instance();
	if (!pMeshRM)
		return;

	for (const auto& itMat : affectedMaterials) {
		pMeshRM->CreateReMaterial(itMat.second);
		applyMaterials(itMat.first);
	}
}

bool RuntimeSkeleton::isFullyLoaded() const {
	if (!m_pendingEquippableItems.empty())
		return false;
	if (!areAllMeshesLoaded())
		return false;
	return true;
}

void RuntimeSkeleton::checkFullyLoaded() {
	if (!m_pendingEquippableItems.empty())
		return;
	if (!areAllMeshesLoaded())
		return;

	CallbackSystem::Instance()->CallbackReady(this, 0);
}

void RuntimeSkeleton::getAvatarCapsuleDimensions(Out<float> fHeight, Out<float> fRadius) {
	Vector3f boxSize = getBonesBoundingBox().GetSize();
	float fAdjustedHeight = boxSize.y * 1.0625;
	fHeight.set(fAdjustedHeight);
	fRadius.set(0.2f * fAdjustedHeight);
}

void PendingEquippableData::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_exclusionIds);
		pMemSizeGadget->AddObject(m_pCCI);
		pMemSizeGadget->AddObject(m_pSC);
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
	m_boneOverride				// boost::optional<std::string>
	m_pPlacementOverride 		// std::unique_ptr<PlacementOverride>
	-------------------------------------------------------------------------------*/
}

void RuntimeSkeleton::getMemSizeBoneMats(IMemSizeGadget* pMemSizeGadget) const {
	if (m_pBoneList && m_pBoneList->getBoneMatrices()) {
		pMemSizeGadget->AddObjectSizeof(m_invBoneMats.get(), m_pBoneList->GetCount()); // std::unique_ptr<Matrix44fA16[]>
		pMemSizeGadget->AddObjectSizeof(m_animBoneMats.get(), m_pBoneList->GetCount()); // std::unique_ptr<Matrix44fA16[]>
	}
}

void RuntimeSkeleton::getMemSizeLodMeshes(IMemSizeGadget* pMemSizeGadget) const {
	for (size_t i = 0; i < SKELETAL_LODS_MAX; ++i) {
		pMemSizeGadget->AddObject(m_apLodMeshes[i]); // std::vector<std::unique_ptr<ReMesh>>[SKELETAL_LODS_MAX]
	}
}

void RuntimeSkeleton::getMemSizeMeshCache(IMemSizeGadget* pMemSizeGadget) const {
	for (size_t i = 0; i < SKELETAL_LODS_MAX; ++i) {
		pMemSizeGadget->AddObject(m_meshCache[i]); // std::vector<ReMesh*>[SKELETAL_LODS_MAX]
	}
}

void RuntimeSkeleton::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_apMaterialObjects); // std::vector<CMaterialObject*>
		pMemSizeGadget->AddObject(m_apSkeletonAnimHeaders); // vector<std::unique_ptr<CSkeletonAnimHeader>>
		pMemSizeGadget->AddObject(m_armedInventoryList); // CArmedInventoryList* (CObList)
		pMemSizeGadget->AddObject(m_calculationSphere); // CSphereCollisionObj*
		pMemSizeGadget->AddObject(m_customTexture); // std::shared_ptr<CustomTextureProvider>
		pMemSizeGadget->AddObject(m_damageSphereList); // CDamagePathObjList* (CObList)
		pMemSizeGadget->AddObject(m_deformableMeshArray); // std::vector<std::unique_ptr<CDeformableMesh>>
		pMemSizeGadget->AddObject(m_fireQueueList); // CFireRangeObjectList*(CObList)
		pMemSizeGadget->AddObject(m_glidsQueuedAnim); // std::set<GLID>
		pMemSizeGadget->AddObject(m_lightCache); // ReLightCache
		pMemSizeGadget->AddObject(m_overheadLabels); // std::unique_ptr<SkeletonLabels>
		pMemSizeGadget->AddObject(m_particleEmitterList); // CParticleEmitRangeObjList* (CObList)
		pMemSizeGadget->AddObject(m_pBoneList); // BoneList* (CObList)
		pMemSizeGadget->AddObject(m_pendingEquippableItems); // std::vector<PendingEquippableData>
		pMemSizeGadget->AddObject(m_shadowInterface); // CShadowClass
		pMemSizeGadget->AddObject(m_skeletonName); // std::string
		pMemSizeGadget->AddObject(m_sphereLocationalDamageList); // CSphereCollisionObjList* (CObList)
		pMemSizeGadget->AddObject(m_sphereBodyCollisionSystem); // CSphereCollisionObjList*	(CObList)
		pMemSizeGadget->AddObject(m_ugcItemTexture); // std::shared_ptr<UGCItemTextureProvider>

		// Internal getMemSize functions
		getMemSizeBoneMats(pMemSizeGadget); // std::unique_ptr<Matrix44fA16[]>
		getMemSizeLodMeshes(pMemSizeGadget); // std::vector<std::unique_ptr<ReMesh>>[SKELETAL_LODS_MAX]
		getMemSizeMeshCache(pMemSizeGadget); // std::vector<ReMesh*>[SKELETAL_LODS_MAX]
	}
}

void BoneNode::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

} // namespace KEP
