///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CCommerceClass.h"

namespace KEP {

CStoreInventoryObj::CStoreInventoryObj() {
	m_globalInventoryID = 0;
	m_raceID = -1;
}

void CStoreInventoryObj::SafeDelete() {
}

void CStoreInventoryObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(2);
		ASSERT(ar.GetObjectSchema() == 2);

		ar << m_globalInventoryID
		   << m_raceID
		   << reserved
		   << reserved
		   << reserved
		   << reserved
		   << reserved;
	} else {
		int version = ar.GetObjectSchema();

		if (version < 2) {
			ar >> m_globalInventoryID;
			m_raceID = -1;

		} else if (version == 2) {
			ar >> m_globalInventoryID >> m_raceID >> reserved >> reserved >> reserved >> reserved >> reserved;
		}
	}
}

void CStoreInventoryObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStoreInventoryObj* invPtr = (CStoreInventoryObj*)GetNext(posLoc);
		invPtr->SafeDelete();
		delete invPtr;
		invPtr = 0;
	}
	RemoveAll();
}

CStoreInventoryObj* CStoreInventoryObjList::GetByGLID(int GLID) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStoreInventoryObj* invPtr = (CStoreInventoryObj*)GetNext(posLoc);
		if (invPtr->m_globalInventoryID == GLID)
			return invPtr;
	}
	return NULL;
}

CStoreInventoryObj* CStoreInventoryObjList::RemoveByGLID(int GLID) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		POSITION previousPosition = posLoc;
		CStoreInventoryObj* invPtr = (CStoreInventoryObj*)GetNext(posLoc);
		if (invPtr->m_globalInventoryID == GLID) {
			RemoveAt(previousPosition);
			return invPtr;
		}
	}
	return NULL;
}

BOOL CStoreInventoryObjList::VerifyInventoryGLID(int GLID) {
	return GetByGLID(GLID) != NULL;
}

//-------------------------------------------------------------//
CCommerceObj::CCommerceObj() {
	m_establishmentName = "N/A";
	m_uniqueID = 0;
	m_storeInventory = NULL;
	m_boundingBoxMax.x = 0.0f;
	m_boundingBoxMax.y = 0.0f;
	m_boundingBoxMax.z = 0.0f;
	m_boundingBoxMin.x = 0.0f;
	m_boundingBoxMin.y = 0.0f;
	m_boundingBoxMin.z = 0.0f;
	m_channel.clear();
	m_menuBind = 0;
	m_willDealWithCriminals = TRUE;
}

void CCommerceObj::SafeDelete() {
	if (m_storeInventory) {
		m_storeInventory->SafeDelete();
		delete m_storeInventory;
		m_storeInventory = 0;
	}
}

void CCommerceObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_establishmentName
		   << m_uniqueID
		   << m_storeInventory
		   << m_boundingBoxMax.x
		   << m_boundingBoxMax.y
		   << m_boundingBoxMax.z
		   << m_boundingBoxMin.x
		   << m_boundingBoxMin.y
		   << m_boundingBoxMin.z
		   << m_channel
		   << m_menuBind
		   << m_willDealWithCriminals;
	} else {
		ar >> m_establishmentName >> m_uniqueID >> m_storeInventory >> m_boundingBoxMax.x >> m_boundingBoxMax.y >> m_boundingBoxMax.z >> m_boundingBoxMin.x >> m_boundingBoxMin.y >> m_boundingBoxMin.z >> m_channel >> m_menuBind >> m_willDealWithCriminals;
	}
}

bool CCommerceObjList::RemoveEstablishmentByName(const CStringA& name) {
	for (POSITION commercePosition = GetHeadPosition(); commercePosition;) {
		CCommerceObj* pCommerceObj;
		POSITION previousPosition;

		previousPosition = commercePosition;
		pCommerceObj = dynamic_cast<CCommerceObj*>(GetNext(commercePosition));
		if (!pCommerceObj)
			continue;

		if (name == pCommerceObj->m_establishmentName) {
			RemoveAt(previousPosition);
			pCommerceObj->SafeDelete();
			delete pCommerceObj;
			return true;
		} // if..name == pCommerceObj->m_establishmentName
	} // for..commercePosition

	return false;
} // CCommerceObjList::RemoveEstablishmentByName

bool CCommerceObjList::RemoveEstablishmentByID(int id) {
	for (POSITION commerceObjPosition = GetHeadPosition(); commerceObjPosition;) {
		POSITION previousPosition;
		CCommerceObj* pCommerceObj;

		previousPosition = commerceObjPosition;
		pCommerceObj = dynamic_cast<CCommerceObj*>(GetNext(commerceObjPosition));
		if (pCommerceObj->m_uniqueID == id) {
			RemoveAt(previousPosition);
			pCommerceObj->SafeDelete();
			delete pCommerceObj;
			return true;
		} // if..pCommerceObj->m_uniqueID == id
	} // for..commerceObjPosition

	return false;
} // CCommerceObjList::RemoveEstablishmentByID

CCommerceObj*
CCommerceObjList::GetEstablishmentByID(int ID) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CCommerceObj* invPtr = dynamic_cast<CCommerceObj*>(GetNext(posLoc));
		if (invPtr->m_uniqueID == ID)
			return invPtr;
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////
//
// Method searches through the list of commerce objects for the object that has
// the given establishment name. If the object can not be found, then 'NULL' is
// returned.
//
// Parameters:
//  name    - Name of the establishment associated with the commerce object.
//
// Return:
//  A pointer to the commerce object with the given establishment name, other-
//  wise 'NULL' is returned.
//
CCommerceObj*
CCommerceObjList::GetEstablishmentByName(const CStringA& name) {
	for (POSITION commercePosition = GetHeadPosition(); commercePosition;) {
		CCommerceObj* pCommerceObj;
		pCommerceObj = reinterpret_cast<CCommerceObj*>(GetNext(commercePosition));
		if (!pCommerceObj)
			continue;

		if (name == pCommerceObj->m_establishmentName)
			return pCommerceObj;
	} // for..commercePosition

	return 0;
} // CCommerceObjList::GetEstablishmentByName

CCommerceObj*
CCommerceObjList::GetEstablishmentByPosition(const Vector3f& position, const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CCommerceObj* cmPtr = static_cast<CCommerceObj*>(GetNext(posLoc));
		if (channel.channelIdEquals(cmPtr->m_channel)) //same world
			if (BoundBoxPointCheck(position, cmPtr->m_boundingBoxMin, cmPtr->m_boundingBoxMax) == TRUE) {
				//in zone
				return cmPtr;
			} //end in zone
	}

	return NULL;
}

BOOL CCommerceObjList::BoundBoxPointCheck(const Vector3f& point, const Vector3f& min, const Vector3f& max) const {
	if (point.x >= min.x &&
		point.x <= max.x &&
		point.y >= min.y &&
		point.y <= max.y &&
		point.z >= min.z &&
		point.z <= max.z)
		return TRUE;
	return FALSE;
}

void CCommerceObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CCommerceObj* invPtr = static_cast<CCommerceObj*>(GetNext(posLoc));
		invPtr->SafeDelete();
		delete invPtr;
		invPtr = 0;
	}
	RemoveAll();
}

IMPLEMENT_SERIAL_SCHEMA(CCommerceObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CCommerceObjList, CKEPObList)
IMPLEMENT_SERIAL_SCHEMA(CStoreInventoryObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CStoreInventoryObjList, CObList)

BEGIN_GETSET_IMPL(CStoreInventoryObj, IDS_STOREINVENTORYOBJ_NAME)
GETSET_RANGE(m_globalInventoryID, gsInt, GS_FLAGS_DEFAULT, 0, 0, STOREINVENTORYOBJ, GLOBALINVENTORYID, INT_MIN, INT_MAX)
GETSET_RANGE(m_raceID, gsInt, GS_FLAGS_DEFAULT, 0, 0, STOREINVENTORYOBJ, RACEID, INT_MIN, INT_MAX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CCommerceObj, IDS_COMMERCEOBJ_NAME)
GETSET_MAX(m_establishmentName, gsCString, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, ESTABLISHMENTNAME, STRLEN_MAX)
GETSET_RANGE(m_uniqueID, gsInt, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, UNIQUEID, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_storeInventory, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, STOREINVENTORY)
GETSET(m_boundingBoxMax, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, BOUNDINGBOXMAX)
GETSET(m_boundingBoxMin, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, BOUNDINGBOXMIN)
GETSET(m_channel, gsInt, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, CHANNEL)
GETSET_RANGE(m_menuBind, gsInt, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, MENUBIND, INT_MIN, INT_MAX)
GETSET(m_willDealWithCriminals, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, COMMERCEOBJ, WILLDEALWITHCRIMINALS)
END_GETSET_IMPL

} // namespace KEP