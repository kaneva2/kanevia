///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CElementsClass.h"

namespace KEP {

CElementObj::CElementObj() {
}

void CElementObj::SafeDelete() {
}

void CElementObj::Clone(CElementObj** clone) {
}

void CElementObjList::Clone(CElementObjList** clone) {
}

void CElementObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CElementObj* elementPtr = (CElementObj*)GetNext(posLoc);
		elementPtr->SafeDelete();
		delete elementPtr;
		elementPtr = 0;
	}
	RemoveAll();
}

CElementObj* CElementObjList::GetByIndex(int index) {
	if (index < 0)
		return NULL;
	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;

	CElementObj* elementPtr = (CElementObj*)GetAt(posLoc);
	return elementPtr;
}

void CElementObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_elementName;
	} else {
		ar >> m_elementName;
	}
}

IMPLEMENT_SERIAL(CElementObj, CObject, 0)
IMPLEMENT_SERIAL(CElementObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CElementObj, IDS_ELEMENTOBJ_NAME)
GETSET_MAX(m_elementName, gsCString, GS_FLAGS_DEFAULT, 0, 0, ELEMENTOBJ, ELEMENTNAME, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP