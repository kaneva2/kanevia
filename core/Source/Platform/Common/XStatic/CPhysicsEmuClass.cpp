///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CPhysicsEmuClass.h"

#include "CABD3DFunctionLib.h"
#include "CCameraClass.h"

#include "MatrixArb.h"

#include "common\include\glAdjust.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

const TimeMs c_jumpAnimTimeMs = 480.0; // jump animation time (ms)

CMovementCaps::CMovementCaps() {
	m_maxSpeed = 0.3f;
	m_minSpeed = -0.2f;
	m_walkMaxSpeed = 0.15f;
	m_walkMinSpeed = -.10f;
	m_bIsWalking = FALSE;
	m_accellerate = 1.0f;
	m_decellerate = 0.25f;
	m_yawAccel = 0.2f;
	m_yawDecel = 0.3f;
	m_maxYaw = 3.0f;
	m_minYaw = 0.02f;
	m_jumpPower = 2.0f;
	m_strafeReduction = 1.0f;
	m_rotationalGroundDrag = 1.0f;
	m_bAutoRun = FALSE;
	m_invertMouse = FALSE;
	m_isJumping = false;
	m_doGravity = TRUE;
	m_gravity = 0.0f;
	m_curSpeed = 0.0f;
	m_curRotX = 0.0f;
	m_curRotY = 0.0f;
	m_curRotZ = 0.0f;
	m_curTransX = 0.0f;
	m_curTransY = 0.0f;
	m_curTransZ = 0.0f;
	m_standVerticalToSurface = FALSE;
	m_burstForceY = 0.0f;
	m_rotationDragConstant = 0.9f;
	m_rotationalGroundDrag = 1.0f;
	m_antiGravity = 0.0;
	m_mass = 100.0f;
	m_curRotationVect.x = 0.0f;
	m_curRotationVect.y = 0.0f;
	m_curRotationVect.z = 0.0f;
	m_curVelocityVect.x = 0.0f;
	m_curVelocityVect.y = 0.0f;
	m_curVelocityVect.z = 0.0f;
	m_lastPosition.x = 0.0f;
	m_lastPosition.y = 0.0f;
	m_lastPosition.z = 0.0f;
	m_groundDrag = 0.99f;
	m_autoDecel = TRUE;
	m_autoDecelY = TRUE;
	m_autoDecelZ = TRUE;
	m_capabilityFovMax = .91f;
	m_capabilityFovMin = .2f;
	m_capFovChangeSensitivity = .1f;
	m_targetFov = 0.0f;
	m_fovCurrentInterp = 0;
	m_lastFov = 0.0f;
	m_crossSectionalArea = 1.0f;
	m_airControl = 0.1f;
	m_isTurning = false;
	m_isMoving = false;
	m_isSideStepping = false;
	m_currentSurfaceProp = ObjectType::NONE;

	// DEPRECATED
	m_freeLookSpeed = .03f;
	m_freeLookCurPitch = 0.0f;
	m_freeLookCurMaxPitch = 60.0f;
	m_freeLookCurYaw = 0.0f;
	m_freeLookCurMaxYaw = 0.0f;
	m_freeLookYawForce = 3.0f;
	m_freeLookPitchForce = 0.0f;
	m_maxRoll = 0.0f;
	m_rollAccel = 0.0f;
	m_rollDecel = 0.0f;
	m_rollFactor = .8f;
	m_pitchAccel = 0.1f;
	m_pitchDecel = 0.1f;
	m_maxPitch = 0.0f;
	m_minPitch = 0.02f;
}

BOOL CMovementCaps::Clone(CMovementCaps** ppMC_out) {
	CMovementCaps* pMC_new = new CMovementCaps();
	Copy(pMC_new);
	*ppMC_out = pMC_new;
	return TRUE;
}

void CMovementCaps::Copy(CMovementCaps* pMC_new) {
	if (!pMC_new)
		return;
	pMC_new->m_invertMouse = m_invertMouse;
	pMC_new->m_isJumping = m_isJumping;
	pMC_new->m_jumpPower = m_jumpPower;
	pMC_new->m_doGravity = m_doGravity;
	pMC_new->m_gravity = m_gravity;
	pMC_new->m_collisionEnabled = m_collisionEnabled;
	pMC_new->m_autoDecel = m_autoDecel;
	pMC_new->m_curSpeed = m_curSpeed;
	pMC_new->m_maxSpeed = m_maxSpeed;
	pMC_new->m_minSpeed = m_minSpeed;
	pMC_new->m_walkMaxSpeed = m_walkMaxSpeed;
	pMC_new->m_walkMinSpeed = m_walkMinSpeed;
	pMC_new->m_bIsWalking = m_bIsWalking;
	pMC_new->m_accellerate = m_accellerate;
	pMC_new->m_decellerate = m_decellerate;
	pMC_new->m_yawAccel = m_yawAccel;
	pMC_new->m_yawDecel = m_yawDecel;
	pMC_new->m_curRotX = m_curRotX;
	pMC_new->m_curRotY = m_curRotY;
	pMC_new->m_curRotZ = m_curRotZ;
	pMC_new->m_curTransX = m_curTransX;
	pMC_new->m_curTransY = m_curTransY;
	pMC_new->m_curTransZ = m_curTransZ;
	pMC_new->m_minYaw = m_minYaw;
	pMC_new->m_maxYaw = m_maxYaw;
	pMC_new->m_standVerticalToSurface = m_standVerticalToSurface;
	pMC_new->m_burstForceY = m_burstForceY;
	pMC_new->m_rotationDragConstant = m_rotationDragConstant;
	pMC_new->m_rotationalGroundDrag = m_rotationalGroundDrag;
	pMC_new->m_antiGravity = m_antiGravity;
	pMC_new->m_mass = m_mass;
	pMC_new->m_curRotationVect.x = m_curRotationVect.x;
	pMC_new->m_curRotationVect.y = m_curRotationVect.y;
	pMC_new->m_curRotationVect.z = m_curRotationVect.z;
	pMC_new->m_curVelocityVect.x = m_curVelocityVect.x;
	pMC_new->m_curVelocityVect.y = m_curVelocityVect.y;
	pMC_new->m_curVelocityVect.z = m_curVelocityVect.z;
	pMC_new->m_lastPosition.x = m_lastPosition.x;
	pMC_new->m_lastPosition.y = m_lastPosition.y;
	pMC_new->m_lastPosition.z = m_lastPosition.z;
	pMC_new->m_groundDrag = m_groundDrag;
	pMC_new->m_autoDecelY = m_autoDecelY;
	pMC_new->m_autoDecelZ = m_autoDecelZ;
	pMC_new->m_capabilityFovMax = m_capabilityFovMax;
	pMC_new->m_capabilityFovMin = m_capabilityFovMin;
	pMC_new->m_capFovChangeSensitivity = m_capFovChangeSensitivity;
	pMC_new->m_targetFov = m_targetFov;
	pMC_new->m_lastFov = 0.0f;
	pMC_new->m_fovCurrentInterp = 0;
	pMC_new->m_strafeReduction = m_strafeReduction;
	pMC_new->m_crossSectionalArea = m_crossSectionalArea;
	pMC_new->m_airControl = m_airControl;
	pMC_new->m_strafeReduction = m_strafeReduction;

	// DEPRECATED
	pMC_new->m_freeLookSpeed = m_freeLookSpeed;
	pMC_new->m_freeLookCurPitch = m_freeLookCurPitch;
	pMC_new->m_freeLookCurMaxPitch = m_freeLookCurMaxPitch;
	pMC_new->m_freeLookCurYaw = m_freeLookCurYaw;
	pMC_new->m_freeLookCurMaxYaw = m_freeLookCurMaxYaw;
	pMC_new->m_freeLookYawForce = m_freeLookYawForce;
	pMC_new->m_freeLookPitchForce = m_freeLookPitchForce;
	pMC_new->m_rollAccel = m_rollAccel;
	pMC_new->m_rollDecel = m_rollDecel;
	pMC_new->m_minPitch = m_minPitch;
	pMC_new->m_maxPitch = m_maxPitch;
	pMC_new->m_maxRoll = m_maxRoll;
	pMC_new->m_rollFactor = m_rollFactor;
	pMC_new->m_pitchAccel = m_pitchAccel;
	pMC_new->m_pitchDecel = m_pitchDecel;
}

void CMovementCaps::SetUseStatesToFalse() {
	m_isTurning = false;
	m_isMoving = false;
	m_isSideStepping = false;
}

void CMovementCaps::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << 0.0f; //m_mouseRestriction;
		ar << m_curSpeed;
		ar << m_autoDecel;
		ar << m_gravity;
		ar << m_maxSpeed;
		ar << m_minSpeed;
		ar << m_accellerate;
		ar << m_decellerate;
		ar << m_pitchAccel;
		ar << m_pitchDecel;
		ar << m_yawAccel;
		ar << m_yawDecel;
		ar << m_curRotX;
		ar << m_curRotY;
		ar << m_curRotZ;
		ar << m_curTransX;
		ar << m_curTransY;
		ar << m_curTransZ;
		ar << m_rollFactor;
		ar << m_minYaw;
		ar << m_maxYaw;
		ar << m_minPitch;
		ar << m_maxPitch;
		ar << m_maxRoll;
		ar << m_rollAccel;
		ar << m_rollDecel;
		ar << (BOOL)FALSE; //m_floatWhenDead;
		ar << 0.0f; //m_letGoDecel;
		ar << m_burstForceY;
		ar << 0.0f; //m_curRotAnimY;
		ar << 0.0f; //m_autoLevelStrength;
		ar << 0.0f; //m_distancePerSecond;
		ar << (BOOL)FALSE; //m_autoLevel;
		ar << m_rotationDragConstant;
		ar << m_rotationalGroundDrag;
		ar << m_freeLookPitchForce;
		ar << 0.0f; //m_submergedHeight;
		ar << 0.0f; //m_inLiquidMotionModifier;
		ar << (BOOL)FALSE; //m_inLiquid;
		ar << (BOOL)FALSE; //m_crouched;
		ar << m_antiGravity;
		ar << (BOOL)FALSE; //m_slidingEnabled;
		ar << (BOOL)TRUE; //m_doGravity
		ar << m_jumpPower;
		ar << m_freeLookSpeed;
		ar << m_freeLookCurMaxPitch;
		ar << m_freeLookCurMaxYaw;
		ar << m_invertMouse;
		ar << (BOOL)FALSE; //m_jointMouseFocus;
		ar << 0.0f; //m_mouseSensitivity;
		ar << m_mass;
		ar << m_curRotationVect.x;
		ar << m_curRotationVect.y;
		ar << m_curRotationVect.z;
		ar << m_curVelocityVect.x;
		ar << m_curVelocityVect.y;
		ar << m_curVelocityVect.z;
		ar << 0.0f; //m_maxVelocity;
		ar << m_lastPosition.x;
		ar << m_lastPosition.y;
		ar << m_lastPosition.z;
		ar << m_groundDrag;
		ar << m_autoDecelY;
		ar << m_autoDecelZ;
		ar << m_capabilityFovMax;
		ar << m_capabilityFovMin;
		ar << m_capFovChangeSensitivity;
		ar << 0.0f; //m_frictionCoefficient;
		ar << m_freeLookYawForce;
		ar << m_strafeReduction;
		ar << m_crossSectionalArea;
		ar << 0.0f; //m_displacedVolume;
		ar << m_airControl;
		ar << (int)0; // m_jumpRecoverTime;
		ar << 0.0f; //m_zeroToSpeed;
		ar << m_standVerticalToSurface;
		ar << m_walkMaxSpeed;
		ar << m_walkMinSpeed;
	} else {
		int version = ar.GetObjectSchema();
		int depInt;
		BOOL depBool;
		float depFloat;
		ar >> depFloat //m_mouseRestriction
			>> m_curSpeed >> m_autoDecel >> m_gravity >> m_maxSpeed >> m_minSpeed >> m_accellerate >> m_decellerate >> m_pitchAccel >> m_pitchDecel >> m_yawAccel >> m_yawDecel >> m_curRotX >> m_curRotY >> m_curRotZ >> m_curTransX >> m_curTransY >> m_curTransZ >> m_rollFactor >> m_minYaw >> m_maxYaw >> m_minPitch >> m_maxPitch >> m_maxRoll >> m_rollAccel >> m_rollDecel >> depBool //m_floatWhenDead
			>> depFloat //m_letGoDecel
			>> m_burstForceY >> depFloat //m_curRotAnimY
			>> depFloat //m_autoLevelStrength
			>> depFloat //m_distancePerSecond
			>> depBool //m_autoLevel
			>> m_rotationDragConstant >> m_rotationalGroundDrag >> m_freeLookPitchForce >> depFloat //m_submergedHeight
			>> depFloat //m_inLiquidMotionModifier
			>> depBool //m_inLiquid
			>> depBool //m_crouched
			>> m_antiGravity >> depBool //m_slidingEnabled
			>> m_doGravity >> m_jumpPower >> m_freeLookSpeed >> m_freeLookCurMaxPitch >> m_freeLookCurMaxYaw >> m_invertMouse >> depBool //m_jointMouseFocus
			>> depFloat //m_mouseSensitivity
			>> m_mass >> m_curRotationVect.x >> m_curRotationVect.y >> m_curRotationVect.z >> m_curVelocityVect.x >> m_curVelocityVect.y >> m_curVelocityVect.z >> depFloat //m_maxVelocity
			>> m_lastPosition.x >> m_lastPosition.y >> m_lastPosition.z >> m_groundDrag >> m_autoDecelY >> m_autoDecelZ >> m_capabilityFovMax >> m_capabilityFovMin >> m_capFovChangeSensitivity >> depFloat //m_frictionCoefficient
			>> m_freeLookYawForce >> m_strafeReduction >> m_crossSectionalArea >> depFloat //m_displacedVolume
			>> m_airControl >> depInt // jumpRecoverTime
			>> depFloat; //m_zeroToSpeed;
		if (version > 1) {
			ar >> m_standVerticalToSurface;
		}

		if (version > 2) {
			ar >> m_walkMaxSpeed;
			ar >> m_walkMinSpeed;
		}

		m_bIsWalking = FALSE;
		m_isTurning = false;
		m_isMoving = false;
		m_isSideStepping = false;
		m_freeLookCurPitch = 0.0f;
		m_freeLookCurYaw = 0.0f;
		m_isJumping = false;
		m_targetFov = 0.0f;
		m_fovCurrentInterp = 0;
		m_lastFov = 0.0f;
		m_burstForceY = 0.0f;
		m_gravity = 0.0f;
		m_collisionEnabled = TRUE;
		m_currentSurfaceProp = ObjectType::NONE;
	}
}

IMPLEMENT_SERIAL_SCHEMA(CMovementCaps, CObject)

bool CMovementCaps::DoJump() {
	if (m_isJumping || m_doGravity || !m_collisionEnabled || (m_jumpPower <= 0) || (m_jumpTimer.ElapsedMs() < c_jumpAnimTimeMs))
		return false;
	m_burstForceY = m_jumpPower;
	m_isJumping = true;
	m_jumpTimer.Reset();
	return true;
}

void CMovementCaps::DoMoveForward() {
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	float maxSpeed = m_bIsWalking ? m_walkMaxSpeed : m_maxSpeed;
	maxSpeed *= (1.0 / deltaT);
	m_curTransZ += m_accellerate * deltaT;
	if (m_curTransZ > maxSpeed)
		m_curTransZ = maxSpeed;
	m_isMoving = true;
}

void CMovementCaps::DoMoveBackward() {
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	float minSpeed = m_bIsWalking ? m_walkMinSpeed : m_minSpeed;
	minSpeed *= (1.0 / deltaT);
	m_curTransZ -= m_accellerate * deltaT;
	if (m_curTransZ < minSpeed)
		m_curTransZ = minSpeed;
	m_isMoving = true;
}

void CMovementCaps::DoMoveDecel() {
	if (m_autoDecelZ != TRUE || m_isJumping)
		return;
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	if (m_curTransZ > (m_decellerate * deltaT))
		m_curTransZ -= (m_decellerate * deltaT);
	else if (m_curTransZ < -(m_decellerate * deltaT))
		m_curTransZ += (m_decellerate * deltaT);
	else
		m_curTransZ = 0.0f;
}

void CMovementCaps::DoTurnLeft(bool bFullSpeed) {
	if (bFullSpeed) {
		m_curRotY = m_maxYaw;
	} else {
		auto deltaT = RenderMetrics::GetFrameTimeRatio();
		m_curRotY += m_yawAccel * deltaT;
		if (m_curRotY > m_maxYaw)
			m_curRotY = m_maxYaw;
	}
	m_isTurning = true;
}

void CMovementCaps::DoTurnRight(bool bFullSpeed) {
	if (bFullSpeed) {
		m_curRotY = -m_maxYaw;
	} else {
		auto deltaT = RenderMetrics::GetFrameTimeRatio();
		m_curRotY -= m_yawAccel * deltaT;
		if (m_curRotY < -m_maxYaw)
			m_curRotY = -m_maxYaw;
	}
	m_isTurning = true;
}

void CMovementCaps::DoTurnDecel(bool bStopNow) {
	if (bStopNow) {
		m_curRotY = 0;
		return;
	}
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	if (m_curRotY > 0)
		m_curRotY -= m_yawDecel * deltaT;
	else if (m_curRotY < 0)
		m_curRotY += m_yawDecel * deltaT;
	if (m_curRotY > 0 && m_curRotY < ((m_yawDecel * deltaT) + .01f))
		m_curRotY = 0;
	if (m_curRotY < 0 && m_curRotY > -((m_yawDecel * deltaT) + .01f))
		m_curRotY = 0;
}

void CMovementCaps::DoSideStepRight() {
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	if (m_strafeReduction > 1.0)
		m_strafeReduction = 1.0f;
	float maxSpeed = m_bIsWalking ? m_walkMaxSpeed : m_maxSpeed;
	maxSpeed *= (1.0 / deltaT) * m_strafeReduction;
	m_curTransX += (m_accellerate * m_strafeReduction) * deltaT;
	if (m_curTransX > maxSpeed)
		m_curTransX = maxSpeed;
	m_isSideStepping = true;
}

void CMovementCaps::DoSideStepLeft() {
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	if (m_strafeReduction > 1.0)
		m_strafeReduction = 1.0f;
	float maxSpeed = m_bIsWalking ? m_walkMaxSpeed : m_maxSpeed;
	maxSpeed *= (1.0 / deltaT) * m_strafeReduction;
	m_curTransX -= (m_accellerate * m_strafeReduction) * deltaT;
	if (m_curTransX < -maxSpeed)
		m_curTransX = -maxSpeed;
	m_isSideStepping = true;
}

void CMovementCaps::DoSideStepDecel() {
	if (m_autoDecel != TRUE)
		return;
	auto deltaT = RenderMetrics::GetFrameTimeRatio();
	if (m_curTransX > (m_decellerate * deltaT))
		m_curTransX -= (m_decellerate * deltaT);
	else if (m_curTransX < -(m_decellerate * deltaT))
		m_curTransX += (m_decellerate * deltaT);
	else
		m_curTransX = 0.0f;
}

void CMovementCaps::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);
	}
}

BEGIN_GETSET_IMPL(CMovementCaps, IDS_MOVEMENTCAPS_NAME)
GETSET_RANGE(m_curSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET(m_autoDecel, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, AUTODECEL)
GETSET_RANGE(m_gravity, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, GRAVITY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_maxSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MAXSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_minSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MINSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_walkMaxSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, WALKMAXSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_walkMinSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, WALKMINSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_accellerate, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, ACCELLERATE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_decellerate, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, DECELLERATE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_pitchAccel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, PITCHACCEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_pitchDecel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, PITCHDECEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_yawAccel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, YAWACCEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_yawDecel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, YAWDECEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_curRotX, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURROTX, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_curRotY, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURROTY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_curRotZ, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURROTZ, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_curTransX, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURTRANSX, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_curTransY, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURTRANSY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_curTransZ, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURTRANSZ, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_rollFactor, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, ROLLFACTOR, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_minYaw, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MINYAW, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_maxYaw, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MAXYAW, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_minPitch, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MINPITCH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_maxPitch, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MAXPITCH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_maxRoll, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MAXROLL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_rollAccel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, ROLLACCEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_rollDecel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, ROLLDECEL, FLOAT_MIN, FLOAT_MAX)
GETSET(m_standVerticalToSurface, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, STAND_VERTICAL_TO_SURFACE)
GETSET_RANGE(m_burstForceY, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, BURSTFORCEY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_rotationDragConstant, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, ROTATIONDRAGCONSTANT, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_rotationalGroundDrag, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, ROTATIONALGROUNDDRAG, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_antiGravity, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, ANTIGRAVITY, FLOAT_MIN, FLOAT_MAX)
GETSET(m_doGravity, gsBOOL, GS_FLAG_EXPOSE, 0, 0, MOVEMENTCAPS, DOGRAVITY) // hardcoded to TRUE on output serialize
GETSET_RANGE(m_jumpPower, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, JUMPPOWER, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_freeLookSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, FREELOOKSPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_freeLookCurMaxPitch, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, FREELOOKCURMAXPITCH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_freeLookCurMaxYaw, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, FREELOOKCURMAXYAW, FLOAT_MIN, FLOAT_MAX)
GETSET(m_invertMouse, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, INVERTMOUSE)
GETSET_RANGE(m_mass, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, MASS, FLOAT_MIN, FLOAT_MAX)
GETSET_D3DVECTOR(m_curRotationVect, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURROTATIONVECT)
GETSET_D3DVECTOR(m_curVelocityVect, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CURVELOCITYVECT)
GETSET_D3DVECTOR(m_lastPosition, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, LASTPOSITION)
GETSET_RANGE(m_groundDrag, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, GROUNDDRAG, FLOAT_MIN, FLOAT_MAX)
GETSET(m_autoDecelY, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, AUTODECELY)
GETSET(m_autoDecelZ, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, AUTODECELZ)
GETSET_RANGE(m_capabilityFovMax, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CAPABILITYFOVMAX, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_capabilityFovMin, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CAPABILITYFOVMIN, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_capFovChangeSensitivity, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CAPFOVCHANGESENSITIVITY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_freeLookYawForce, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, FREELOOKYAWFORCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_strafeReduction, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, STRAFEREDUCTION, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_crossSectionalArea, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, CROSSSECTIONALAREA, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_airControl, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, AIRCONTROL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_jumpRecoverTime, gsDouble, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, JUMPRECOVERTIME, DOUBLE_MIN, DOUBLE_MAX)
GETSET(m_bIsWalking, gsBool, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, WALKMODE)
GETSET(m_collisionEnabled, gsBool, GS_FLAGS_DEFAULT, 0, 0, MOVEMENTCAPS, COLLISIONENABLED)
END_GETSET_IMPL

} // namespace KEP