///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VertexDeclaration.h"
#include "common/include/IMemSizeGadget.h"
#include "Core/Util/Unicode.h"
#include "RenderEngine/Utils/ReUtils.h"
#include <assert.h>

namespace KEP {

void VertexBuffer::CreateAndFillBuffer(LPDIRECT3DDEVICE9 g_pd3dDevice) {
	//	Must have set some valid data
	assert(pData);
	assert(g_pd3dDevice);

	D3DCheck(g_pd3dDevice->CreateVertexBuffer(mStride * mNumItems, D3DUSAGE_WRITEONLY, 0,
		D3DPOOL_MANAGED, &mpBuffer, NULL));

	void* ptr;

	D3DCheck(mpBuffer->Lock(0, 0, &ptr, 0));
	memcpy(ptr, pData, mStride * mNumItems);
	D3DCheck(mpBuffer->Unlock());
}

VertexDeclaration::VertexDeclaration() {
	mNumElements = 0;
	memset(mStreamSize, 0, MAX_STREAMS * sizeof(unsigned int));
	mpDeclaration = NULL;
	D3DFVF = 0;
}

VertexDeclaration::~VertexDeclaration() {
	if (mpDeclaration) {
		mpDeclaration->Release();
		mpDeclaration = NULL;
	}
}

D3DVERTEXELEMENT9 VertexDeclaration::mEndElement = D3DDECL_END();

void VertexDeclaration::AddVertexElement(D3DDECLUSAGE usage, D3DDECLTYPE type, unsigned int elementSize,
	unsigned int stream, unsigned int usageIndex) {
	//	You added too many elements. May need to increase this size.
	assert(mNumElements < MAX_VERTEX_ELEMENTS);

	mVertexElements[mNumElements].Usage = usage;
	mVertexElements[mNumElements].Offset = mStreamSize[stream];
	mVertexElements[mNumElements].Stream = stream;
	mVertexElements[mNumElements].Type = type;
	mVertexElements[mNumElements].Method = 0;
	mVertexElements[mNumElements].UsageIndex = usageIndex;

	mStreamSize[stream] += elementSize;
	mNumElements++;

	//	Add D3DDECL_END() at the end after every addition
	mVertexElements[mNumElements] = mEndElement;
}

void VertexDeclaration::CreateVertexDeclaration(LPDIRECT3DDEVICE9 pDevice) {
	//	You have not added any elements. Can't create declaration of 0 size
	if (mNumElements <= 0)
		return;

	assert(pDevice != NULL);

	D3DCheck(pDevice->CreateVertexDeclaration(mVertexElements, &mpDeclaration));

	//	TODO: Create a FVF declaration for fixed function
}

void VertexDeclaration::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

} // namespace KEP