///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "matrixarb.h"
#include "direct.h"

#include "IClientEngine.h"
#include "CParticleEmitRange.h"
#include "EnvParticleSystem.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

CParticleEmitRangeObj::CParticleEmitRangeObj(int boneIndex, int particleSlotId, GLID particleGLID, GLID animationGLID) :
		m_boneLink(boneIndex),
		m_particleSlotId(particleSlotId),
		m_particleGLID(particleGLID),
		m_animationQue(-1),
		m_startRange(0),
		m_endRange(0),
		m_particleDBIndex(0),
		m_useExactRange(FALSE),
		m_inRange(FALSE),
		m_runtimeParticleId(0),
		m_animationType(eAnimType::Invalid),
		m_animationVersion(0),
		m_animationGLID(0) {
	m_matrix.MakeIdentity();
	setAnimationByGLID(animationGLID);
}

void CParticleEmitRangeObj::SafeDelete() {
	if (m_runtimeParticleId) {
#ifdef _ClientOutput
		auto pICE = IClientEngine::Instance();
		if (pICE && pICE->GetParticleDatabase())
			pICE->GetParticleDatabase()->SetRuntimeEffectStatus(m_runtimeParticleId, -1, -1, 0);
#endif
		m_runtimeParticleId = 0;
	}
}

void CParticleEmitRangeObj::Clone(CParticleEmitRangeObj** clone) {
	CParticleEmitRangeObj* clonePtr = new CParticleEmitRangeObj();
	//base data
	clonePtr->m_animationQue = m_animationQue;
	clonePtr->m_startRange = m_startRange;
	clonePtr->m_endRange = m_endRange;
	clonePtr->m_particleDBIndex = m_particleDBIndex;
	clonePtr->m_boneLink = m_boneLink;
	clonePtr->m_inRange = FALSE;
	clonePtr->m_useExactRange = m_useExactRange;
	clonePtr->m_animationType = m_animationType;
	clonePtr->m_animationVersion = m_animationVersion;

	*clone = clonePtr;
}

void CParticleEmitRangeObj::setRanges(long start, long end) {
	m_startRange = start;
	m_endRange = end;
}

void CParticleEmitRangeObjList::Clone(CParticleEmitRangeObjList** clone) {
	CParticleEmitRangeObjList* cloneList = new CParticleEmitRangeObjList;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CParticleEmitRangeObj* ptRang = (CParticleEmitRangeObj*)GetNext(posLoc);
		CParticleEmitRangeObj* clonedObject = NULL;
		ptRang->Clone(&clonedObject);
		cloneList->AddTail(clonedObject);
	}
	*clone = cloneList;
}

void CParticleEmitRangeObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CParticleEmitRangeObj* ptRang = (CParticleEmitRangeObj*)GetNext(posLoc);
		ASSERT(ptRang); // Added in response to a user generated crash report, please contact Ken Lightner if you get this.
		if (NULL != ptRang) {
			ptRang->SafeDelete();
			delete ptRang;
			ptRang = 0;
		}
	}
	RemoveAll();
}

void CParticleEmitRangeObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_animationQue
		   << m_startRange
		   << m_endRange
		   << m_particleDBIndex
		   << m_boneLink
		   << m_useExactRange
		   << (int)m_animationType
		   << m_animationVersion;
	} else {
		UINT version = ar.GetObjectSchema();

		ar >> m_animationQue >> m_startRange >> m_endRange >> m_particleDBIndex >> m_boneLink >> m_useExactRange;

		if (version >= 1) {
			int animType;
			ar >> animType;
			m_animationType = (eAnimType)animType;
			ar >> m_animationVersion;
		}

		m_runtimeParticleId = 0;
		m_inRange = FALSE;
	}
}

void CParticleEmitRangeObj::setAnimationInfo(eAnimType animType, int animVer) {
	m_animationType = animType;
	m_animationVersion = animVer;
	m_animationQue = -1; // Invalidate legacy index as we no longer have the correct value
	m_animationGLID = GLID_INVALID;
}

void CParticleEmitRangeObj::setAnimationByGLID(GLID glid) {
	m_animationType = eAnimType::None;
	m_animationVersion = 0;
	m_animationQue = -1; // Invalidate legacy index as we no longer have the correct value
	m_animationGLID = glid;
}

void CParticleEmitRangeObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_strParticleSystemName);
	}
}

void CParticleEmitRangeObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CParticleEmitRangeObj* pCParticleEmitRangeObj = static_cast<const CParticleEmitRangeObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCParticleEmitRangeObj);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CParticleEmitRangeObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CParticleEmitRangeObjList, CObject)

BEGIN_GETSET_IMPL(CParticleEmitRangeObj, IDS_PARTICLEEMITRANGEOBJ_NAME)
GETSET_RANGE(m_animationQue, gsInt, GS_FLAGS_DEFAULT, 0, 0, PARTICLEEMITRANGEOBJ, ANIMATIONQUE, INT_MIN, INT_MAX)
GETSET_RANGE(m_startRange, gsInt, GS_FLAGS_DEFAULT, 0, 0, PARTICLEEMITRANGEOBJ, STARTRANGE, INT_MIN, INT_MAX)
GETSET_RANGE(m_endRange, gsInt, GS_FLAGS_DEFAULT, 0, 0, PARTICLEEMITRANGEOBJ, ENDRANGE, INT_MIN, INT_MAX)
GETSET_RANGE(m_particleDBIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, PARTICLEEMITRANGEOBJ, PARTICLEDBINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_boneLink, gsInt, GS_FLAGS_DEFAULT, 0, 0, PARTICLEEMITRANGEOBJ, BONELINK, INT_MIN, INT_MAX)
GETSET(m_useExactRange, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, PARTICLEEMITRANGEOBJ, USEEXACTRANGE)
END_GETSET_IMPL

} // namespace KEP
