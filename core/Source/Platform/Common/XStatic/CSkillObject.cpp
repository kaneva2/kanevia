///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "CTitleObject.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillAbilityObject::CSkillAbilityObject() {
	m_abilityName = "N/A";
	m_menuID = -1;
	m_minimumLevelToUse = 0;
	m_masterLevel = 0.0f;
	m_functionType = 0;
	m_requiredStatistics = NULL; //minimum statistics to attempt
	m_requiredItems = NULL;
	m_resultItems = NULL;
	m_miscInt = 0;
	m_miscInt2 = 0;
	m_miscInt3 = 0;
	m_effectBind = -1;
	m_miscAmount = 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillAbilityObject::Clone(CSkillAbilityObject** clone) {
	CSkillAbilityObject* copyLocal = new CSkillAbilityObject();

	copyLocal->m_functionType = m_functionType;
	copyLocal->m_abilityName = m_abilityName;
	copyLocal->m_masterLevel = m_masterLevel;
	copyLocal->m_minimumLevelToUse = m_minimumLevelToUse;
	copyLocal->m_miscInt = m_miscInt;
	copyLocal->m_miscInt2 = m_miscInt2;
	copyLocal->m_miscInt3 = m_miscInt3;
	copyLocal->m_effectBind = m_effectBind;
	copyLocal->m_miscAmount = m_miscAmount;

	if (m_requiredStatistics)
		m_requiredStatistics->Clone(&copyLocal->m_requiredStatistics);

	if (m_requiredItems)
		m_requiredItems->Clone(&copyLocal->m_requiredItems);

	if (m_resultItems)
		m_resultItems->Clone(&copyLocal->m_resultItems);

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillAbilityObject::Serialize(CArchive& ar) {
	float reservedFloat = 0.0f;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_abilityName
		   << m_minimumLevelToUse
		   << m_miscAmount
		   << m_requiredStatistics
		   << m_functionType
		   << m_miscInt
		   << m_miscInt2
		   << m_miscInt3
		   << m_masterLevel
		   << reservedFloat
		   << reservedFloat
		   << m_requiredItems
		   << m_resultItems
		   << m_effectBind;
	} else {
		ar >> m_abilityName >> m_minimumLevelToUse >> m_miscAmount >> m_requiredStatistics >> m_functionType >> m_miscInt >> m_miscInt2 >> m_miscInt3 >> m_masterLevel >> reservedFloat >> reservedFloat >> m_requiredItems >> m_resultItems >> m_effectBind;

		//m_effectBind = -1;

		//m_requiredItems = NULL;
		//m_resultItems   = NULL;
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillAbilityObject::~CSkillAbilityObject() {
	if (m_requiredItems) {
		m_requiredItems->SafeDelete();
		delete m_requiredItems;
		m_requiredItems = 0;
	}

	if (m_resultItems) {
		m_resultItems->SafeDelete();
		delete m_resultItems;
		m_resultItems = 0;
	}

	if (m_requiredStatistics) {
		delete m_requiredStatistics;
		m_requiredStatistics = 0;
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillAbilityList::Clone(CSkillAbilityList** clone) {
	CSkillAbilityList* copyLocal = new CSkillAbilityList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillAbilityObject* skPtr = (CSkillAbilityObject*)GetNext(posLoc);
		CSkillAbilityObject* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillAbilityObject* CSkillAbilityList::GetObjByIndex(int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;
	CSkillAbilityObject* ptr = (CSkillAbilityObject*)GetAt(posLoc);
	return ptr;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillAbilityList::~CSkillAbilityList() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillAbilityObject* skPtr = (CSkillAbilityObject*)GetNext(posLoc);
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRangeObject::CLevelRangeObject() {
	m_rangeStart = 0;
	m_rangeEnd = 0;
	m_levelTitle = "N/A";
	m_abilityDB = 0;
	m_menuID = -1;
	m_profficiencyBonus = 0; //
	m_perfectiveBonus = 0; //
	m_levelRewardDB = 0;
	m_levelRangeId = -1;
	m_titleId = 0;
	m_altTitleId = 0;
	m_gainMsg = "";
	m_altGainMsg = "";
	m_levelMsg = "";
	m_levelNumber = 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRangeObject::~CLevelRangeObject() {
	if (m_abilityDB) {
		delete m_abilityDB;
		m_abilityDB = 0;
	}

	if (m_levelRewardDB) {
		delete m_levelRewardDB;
		m_levelRewardDB = 0;
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CLevelRangeObject::Clone(CLevelRangeObject** clone) {
	CLevelRangeObject* lRangeObj = new CLevelRangeObject();
	lRangeObj->m_levelTitle = m_levelTitle;
	lRangeObj->m_rangeEnd = m_rangeEnd;
	lRangeObj->m_rangeStart = m_rangeStart;
	lRangeObj->m_menuID = m_menuID;
	lRangeObj->m_profficiencyBonus = m_profficiencyBonus; //
	lRangeObj->m_perfectiveBonus = m_perfectiveBonus; //
	lRangeObj->m_levelRangeId = m_levelRangeId;
	lRangeObj->m_titleId = m_titleId;
	lRangeObj->m_altTitleId = m_altTitleId;
	lRangeObj->m_gainMsg = m_gainMsg;
	lRangeObj->m_altGainMsg = m_altGainMsg;
	lRangeObj->m_levelMsg = m_levelMsg;
	lRangeObj->m_levelNumber = m_levelNumber;

	if (m_abilityDB)
		m_abilityDB->Clone(&lRangeObj->m_abilityDB);

	if (m_levelRewardDB)
		m_levelRewardDB->Clone(&lRangeObj->m_levelRewardDB);

	*clone = lRangeObj;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CLevelRangeObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		ar << m_rangeStart
		   << m_rangeEnd
		   << m_levelTitle
		   << m_abilityDB
		   << m_menuID
		   << m_profficiencyBonus
		   << m_perfectiveBonus
		   << m_levelRewardDB
		   << m_levelRangeId
		   << m_titleId
		   << m_altTitleId
		   << m_gainMsg
		   << m_altGainMsg
		   << m_levelMsg
		   << m_levelNumber;
	} else {
		int version = ar.GetObjectSchema();

		m_levelRewardDB = 0;

		if (version == 1) {
			ar >> m_rangeStart >> m_rangeEnd >> m_levelTitle >> m_abilityDB >> m_menuID >> m_profficiencyBonus >> m_perfectiveBonus >> m_levelRewardDB >> m_levelRangeId >> m_titleId >> m_gainMsg >> m_levelMsg >> m_levelNumber;
		} else if (version == 2) {
			ar >> m_rangeStart >> m_rangeEnd >> m_levelTitle >> m_abilityDB >> m_menuID >> m_profficiencyBonus >> m_perfectiveBonus >> m_levelRewardDB >> m_levelRangeId >> m_titleId >> m_altTitleId >> m_gainMsg >> m_levelMsg >> m_levelNumber;
		} else if (version == 3) {
			ar >> m_rangeStart >> m_rangeEnd >> m_levelTitle >> m_abilityDB >> m_menuID >> m_profficiencyBonus >> m_perfectiveBonus >> m_levelRewardDB >> m_levelRangeId >> m_titleId >> m_altTitleId >> m_gainMsg >> m_altGainMsg >> m_levelMsg >> m_levelNumber;
		} else {
			ar >> m_rangeStart >> m_rangeEnd >> m_levelTitle >> m_abilityDB >> m_menuID >> m_profficiencyBonus >> m_perfectiveBonus;
		}
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRangeObject* CLevelRangeList::GetObjByIndex(int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;
	CLevelRangeObject* ptr = (CLevelRangeObject*)GetAt(posLoc);
	return ptr;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
BOOL CLevelRangeList::GetExperienceValueByLevel(int level,
	long& experiencePoints) {
	int trace = 1;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CLevelRangeObject* rgPtr = (CLevelRangeObject*)GetNext(posLoc);
		if (trace == level) {
			experiencePoints = rgPtr->m_rangeStart;
			return TRUE;
		}
		trace++;
	}
	return FALSE;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CLevelRangeList::Clone(CLevelRangeList** clone) {
	CLevelRangeList* copyLocal = new CLevelRangeList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CLevelRangeObject* skPtr = (CLevelRangeObject*)GetNext(posLoc);
		CLevelRangeObject* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRangeList::~CLevelRangeList() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CLevelRangeObject* skPtr = (CLevelRangeObject*)GetNext(posLoc);
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRangeObject* CLevelRangeList::GetLevelByExp(long exp) {
	CLevelRangeObject* persist = NULL;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CLevelRangeObject* skPtr = (CLevelRangeObject*)GetNext(posLoc);
		if (exp >= skPtr->m_rangeStart)
			persist = skPtr;
	}
	return persist;
}
int CLevelRangeList::GetFreeID() {
	int trace = 1;

	BOOL done = FALSE;
	while (!done) {
		done = TRUE;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CLevelRangeObject* lrPtr = (CLevelRangeObject*)GetNext(posLoc);
			if (lrPtr->m_levelRangeId == trace) {
				done = FALSE;
				trace++;
				break;
			}
		}
	}

	return trace;
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CRacialAdvantageObject::CRacialAdvantageObject() {
	m_edbRef = 0;
	m_adjustment = 0.0f;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CRacialAdvantageList::Clone(CRacialAdvantageList** clone) {
	CRacialAdvantageList* copyLocal = new CRacialAdvantageList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CRacialAdvantageObject* skPtr = (CRacialAdvantageObject*)GetNext(posLoc);
		CRacialAdvantageObject* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CRacialAdvantageList::~CRacialAdvantageList() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CRacialAdvantageObject* skPtr = (CRacialAdvantageObject*)GetNext(posLoc);
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CRacialAdvantageObject::Clone(CRacialAdvantageObject** clone) {
	CRacialAdvantageObject* copyLocal = new CRacialAdvantageObject();

	copyLocal->m_adjustment = m_adjustment;
	copyLocal->m_edbRef = m_edbRef;

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
float CRacialAdvantageList::GetAdjustmentByEdbIndex(int edbIndex) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CRacialAdvantageObject* skPtr = (CRacialAdvantageObject*)GetNext(posLoc);
		if (skPtr->m_edbRef == edbIndex)
			return skPtr->m_adjustment;
	}
	return 1.0f;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CRacialAdvantageObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_adjustment
		   << m_edbRef;

	} else {
		ar >> m_adjustment >> m_edbRef;
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObject::CSkillObject() {
	m_skillName = "Unknown";
	m_skillType = 0;
	m_currentExperience = 0.0f;
	m_currentLevel = 0;
	m_gainIncrement = 0.1f;
	m_levelRangeDB = NULL;
	m_raceAdvantageDB = NULL;
	m_skillActionDB = NULL;
	m_basicAbilitySkill = FALSE; //means that it only has one function like hiding
	m_basicAbilityExpMastered = 0.0f; //component of ability basic skill option
	m_basicAbilityFunction = 0;
	m_skillGainsWhenFailed = 0.0f;
	m_maxMultiplier = 3.0f;
	m_minimalSave = FALSE;
	m_lastUseTime = fTime::TimeMs();
	m_useInternalTimer = FALSE;
	m_currentSkillDelay = 5000;
	m_skillId = 0;
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CSkillObject::UseSkill(long expOfTarget,
	int levelToUse,
	int ability,
	int curDBCfg,
	CInventoryObjList* currentInv,
	int* specialFunction,
	CStatObjList* currentStats,
	BOOL* updateStatsToClient,
	CLevelRangeObject** levelRaisedPtr,
	int* returnMiscInt,
	float totalCurrentExp,
	int maxItemsInInventory,
	CSkillObject* dbObjectRef,
	int* effectBind,
	ULONG curGlobalSkillDelay,
	ULONG curPlayerSkillTimeStamp) {
	//calculate speed cap
	if (totalCurrentExp == 0.0f) //prevent divide by zero
		totalCurrentExp = 1.0f;

	float speedCapMultiplier = 10000.0f / totalCurrentExp;
	if (speedCapMultiplier > 1.0f)
		speedCapMultiplier = 1.0f;
	else if (speedCapMultiplier < .1f)
		speedCapMultiplier = .1f;

	if (!dbObjectRef->m_levelRangeDB)
		return SKILLRET_NO_LEVELS;

	if (!dbObjectRef->m_useInternalTimer) {
		if (fTime::ElapsedMs(curPlayerSkillTimeStamp) < curGlobalSkillDelay) {
			//dont let him use skills too fast
			return SKILLRET_TOO_SOON;
		} //end dont let him use skills too fast
	} else {
		//skill based timer
		if (fTime::ElapsedMs(m_lastUseTime) < m_currentSkillDelay) {
			return SKILLRET_TOO_SOON_SKILL;
		}
	} //end skill based timer
	m_lastUseTime = fTime::TimeMs();

	*returnMiscInt = 0;

	CLevelRangeObject* beforeUseLevel = dbObjectRef->m_levelRangeDB->GetLevelByExp((long)m_currentExperience);

	*specialFunction = SKILL_NO_SPECIAL; //default none

	float racialAdvantageFactor = 1.0f;
	if (dbObjectRef->m_raceAdvantageDB) //see if there is a racial advantage/disadvantage
		racialAdvantageFactor = dbObjectRef->m_raceAdvantageDB->GetAdjustmentByEdbIndex(curDBCfg);

	if (ability > -1) { //if ability based

		if (levelToUse < 0)
			return SKILLRET_INVALID_LEVEL; //invalid level error

		POSITION levelPos = dbObjectRef->m_levelRangeDB->FindIndex(levelToUse);
		if (!levelPos)
			return SKILLRET_INVALID_LEVEL; //invalid level error
		CLevelRangeObject* lPtr = (CLevelRangeObject*)dbObjectRef->m_levelRangeDB->GetAt(levelPos);

		if (m_currentExperience < lPtr->m_rangeStart)
			return SKILLRET_LACK_SKILL; //you lask the skill to attempt this

		if (lPtr->m_abilityDB) {
			//valid ability DB
			POSITION posLoc = lPtr->m_abilityDB->FindIndex(ability);
			if (posLoc) {
				//valid
				CSkillAbilityObject* aPtr = (CSkillAbilityObject*)lPtr->m_abilityDB->GetAt(posLoc);

				*effectBind = aPtr->m_effectBind;
				//check required items and remove them and proceed
				if (aPtr->m_requiredStatistics) {
					//verify stat req
					if (!currentStats)
						return SKILLRET_STAT_REQ_NOT_MET; //stat req not met

					for (POSITION stPos = aPtr->m_requiredStatistics->GetHeadPosition(); stPos != NULL;) {
						//req loop
						CStatObj* statPtr = (CStatObj*)aPtr->m_requiredStatistics->GetNext(stPos);
						float curVal = currentStats->GetStatValueByID(statPtr->m_id, currentInv);
						if (curVal < statPtr->m_currentValue)
							return SKILLRET_STAT_REQ_NOT_MET; //stat req not met
					} //end req loop
				} //end verify stat req

				if (aPtr->m_miscInt2 != 0 && aPtr->m_miscInt3 == 0) {
					//required item
					if (!currentInv->VerifyInventory(aPtr->m_miscInt2))
						return SKILLRET_ITEM_NOT_PRESENT; //item not present
				} //end required item
				else if (aPtr->m_miscInt2 != 0 && aPtr->m_miscInt3 != 0) {
					//required item and required exp obj
					if (!currentInv->VerifyInventory(aPtr->m_miscInt2))
						return SKILLRET_ITEM_NOT_PRESENT; //item not present
					if (!currentInv->IsExpItemObjectPresent(aPtr->m_miscInt2, aPtr->m_miscInt3))
						return SKILLRET_LACK_THE_COMPONENT; //you lack the component
				} //end required item and required exp obj

				if (aPtr->m_requiredItems) {
					//required items valid
					int qty = 0;

					if (!currentInv)
						return SKILLRET_LACK_INVENTORY; //does not have required inventory items

					POSITION curInvPos;
					for (curInvPos = aPtr->m_requiredItems->GetHeadPosition(); curInvPos != NULL;) {
						//verify required items
						CInventoryObj* reqInvPtr = (CInventoryObj*)aPtr->m_requiredItems->GetNext(curInvPos);
						//count the items
						qty = 0;
						for (POSITION poscnt = aPtr->m_requiredItems->GetHeadPosition(); poscnt != NULL;) {
							CInventoryObj* cntInvObj = (CInventoryObj*)aPtr->m_requiredItems->GetNext(poscnt);
							if (reqInvPtr->m_itemData->m_globalID == cntInvObj->m_itemData->m_globalID)
								qty++;
						}

						if (!currentInv->VerifyInventory(reqInvPtr->m_itemData->m_globalID, qty))
							return SKILLRET_LACK_INVENTORY; //does not have required inventory items
					} //end verify required items
					for (curInvPos = aPtr->m_requiredItems->GetHeadPosition(); curInvPos != NULL;) {
						//remove required items
						CInventoryObj* reqInvPtr = (CInventoryObj*)aPtr->m_requiredItems->GetNext(curInvPos);
						currentInv->DeleteByGLID(reqInvPtr->m_itemData->m_globalID, 1);
					} //end verify required items
				} //end remove items valid

				float multiplier = 1.0f;
				float spread = aPtr->m_masterLevel - lPtr->m_rangeStart;
				float posInLevel = m_currentExperience - lPtr->m_rangeStart;
				if (posInLevel != 0)
					multiplier = spread / posInLevel;

				if (multiplier > dbObjectRef->m_maxMultiplier)
					multiplier = dbObjectRef->m_maxMultiplier;

				float resultFromThrow = 0.0f;

				if (posInLevel < spread) //otherwise its matered so leave at zero
					resultFromThrow = (float)(rand() % ((int)(spread + 1.0f)));
				if (resultFromThrow <= (posInLevel + 1.0f)) {
					//successful throw

					float applicableBenefit = ((dbObjectRef->m_gainIncrement * multiplier) * racialAdvantageFactor) * speedCapMultiplier;
					m_currentExperience += applicableBenefit; //apply exp gained

					if (aPtr->m_resultItems) //success you gain items ;D
						for (POSITION benInvPos = aPtr->m_resultItems->GetHeadPosition(); benInvPos != NULL;) {
							//remove required items
							CInventoryObj* additionalPtr = (CInventoryObj*)aPtr->m_resultItems->GetNext(benInvPos);
							if (currentInv->GetCount() < maxItemsInInventory)
								currentInv->AddInventoryItem(1, FALSE, additionalPtr->m_itemData, additionalPtr->inventoryType());
							else
								return SKILLRET_TOO_MUCH_INVENTORY;
						} //end verify required items

					*specialFunction = aPtr->m_functionType;
					*returnMiscInt = aPtr->m_miscInt;
					if (currentStats)
						if (aPtr->m_requiredStatistics) {
							//access stats and use the appropriately
							for (POSITION stPos = aPtr->m_requiredStatistics->GetHeadPosition(); stPos != NULL;) {
								//push required stats
								CStatObj* statPtr = (CStatObj*)aPtr->m_requiredStatistics->GetNext(stPos);
								if (currentStats->UseStatByID(statPtr->m_currentValue, statPtr->m_id, currentInv)) {
									//you have increased a stat
									//inform client to update stats and notify
									*updateStatsToClient = TRUE;
								} //end you have increased a stat
							} //end push required stats
						} //end access stats and use the appropriately

					CLevelRangeObject* afterSuccessfulUse = dbObjectRef->m_levelRangeDB->GetLevelByExp((long)m_currentExperience);
					if (afterSuccessfulUse)
						if (beforeUseLevel->m_rangeStart != afterSuccessfulUse->m_rangeStart)
							*levelRaisedPtr = afterSuccessfulUse;

					return SKILLRET_SUCCESS; //successful attempt
				} //end successful throw
				else {
					//unsuccessful throw
					if (dbObjectRef->m_skillGainsWhenFailed > 0.0f) {
						//skill will gain some even when failed
						if (m_currentExperience < dbObjectRef->m_skillGainsWhenFailed) {
							//can still gain on failure
							float curveMultiplier = 1.0f;
							if (m_currentExperience != 0)
								curveMultiplier = dbObjectRef->m_skillGainsWhenFailed / m_currentExperience;

							if (curveMultiplier > dbObjectRef->m_maxMultiplier)
								curveMultiplier = dbObjectRef->m_maxMultiplier;

							float applicableBenefit = ((dbObjectRef->m_gainIncrement * multiplier) * racialAdvantageFactor) * curveMultiplier;
							m_currentExperience += applicableBenefit; //apply exp gained
						} //end can still gain on failure
					} //end skill will gain some even when failed

					return SKILLRET_UNSUCCESSFUL; //unsuccessful attempt
				} //end unsuccessful throw
			} //end valid
		} //end valid ability DB
		else
			return SKILLRET_FAILED; //FAILED
	} //end if ability based
	else {
		//non ability skill based-linear stuffola
		if (m_currentExperience == 0)
			m_currentExperience = 10.0f;

		if (!dbObjectRef->m_basicAbilitySkill) {
			//derived by player skill use (contact etc)
			float multiplier = expOfTarget / m_currentExperience;
			if (multiplier > dbObjectRef->m_maxMultiplier)
				multiplier = dbObjectRef->m_maxMultiplier;
			float applicableBenefit = ((dbObjectRef->m_gainIncrement * multiplier) * racialAdvantageFactor) * speedCapMultiplier;
			m_currentExperience += applicableBenefit;
			CLevelRangeObject* afterSuccessfulUse = dbObjectRef->m_levelRangeDB->GetLevelByExp((long)m_currentExperience);
			if (afterSuccessfulUse)
				if (beforeUseLevel->m_rangeStart != afterSuccessfulUse->m_rangeStart)
					*levelRaisedPtr = afterSuccessfulUse;
		} //end derived by player skill use (contact etc)
		else {
			//basic ability skill
			float multiplier = 1.0f;
			float spread = dbObjectRef->m_basicAbilityExpMastered;
			float posInLevel = m_currentExperience;
			if (posInLevel != 0)
				multiplier = spread / posInLevel;

			if (multiplier > dbObjectRef->m_maxMultiplier)
				multiplier = dbObjectRef->m_maxMultiplier;

			float resultFromThrow = 0.0f;

			if (posInLevel < spread) //otherwise its matered so leave at zero
				resultFromThrow = (float)(rand() % ((int)spread + 1));
			if (resultFromThrow <= (posInLevel + 1.0f)) {
				//successful throw
				float applicableBenefit = ((dbObjectRef->m_gainIncrement * multiplier) * racialAdvantageFactor) * speedCapMultiplier;
				m_currentExperience += applicableBenefit; //apply exp gained
				*specialFunction = dbObjectRef->m_basicAbilityFunction;

				CLevelRangeObject* afterSuccessfulUse = dbObjectRef->m_levelRangeDB->GetLevelByExp((long)m_currentExperience);
				if (afterSuccessfulUse)
					if (beforeUseLevel->m_rangeStart != afterSuccessfulUse->m_rangeStart)
						*levelRaisedPtr = afterSuccessfulUse;

				return SKILLRET_SUCCESS; //successful attempt
			} //end successful throw
			else {
				//unsuccessful throw
				if (dbObjectRef->m_skillGainsWhenFailed > 0.0f) {
					//skill will gain some even when failed
					if (m_currentExperience < dbObjectRef->m_skillGainsWhenFailed) {
						//can still gain on failure
						float curveMultiplier = 1.0f;
						if (m_currentExperience != 0)
							curveMultiplier = dbObjectRef->m_skillGainsWhenFailed / m_currentExperience;

						if (curveMultiplier > dbObjectRef->m_maxMultiplier)
							curveMultiplier = dbObjectRef->m_maxMultiplier;

						float applicableBenefit = ((dbObjectRef->m_gainIncrement * multiplier) * racialAdvantageFactor) * curveMultiplier;
						m_currentExperience += applicableBenefit; //apply exp gained
					} //end can still gain on failure
				} //end skill will gain some even when failed
				return SKILLRET_UNSUCCESSFUL; //unsuccessful attempt
			} //end unsuccessful throw
		} //end basic ability skill
	} //end non ability skill based-and linear stufola

	return SKILLRET_SUCCESS; //ok
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CSkillObject::UseAction(int actionId, CSkillObject* skillRef, bool useAlternate, int& newLevel, std::string& levelMessage, std::string& gainMessage) {
	levelMessage = "";
	gainMessage = "";

	if (skillRef) {
		if (skillRef->m_skillActionDB) {
			CSkillActionObject* actionPtr = skillRef->m_skillActionDB->GetActionById(actionId);

			if (actionPtr) {
				// Make sure the skill has levels
				if (skillRef->m_levelRangeDB) {
					CLevelRangeObject* currentLevel = skillRef->m_levelRangeDB->GetLevelByExp((long)m_currentExperience);

					// Apply the gain amount associated with the skill action
					m_currentExperience += actionPtr->m_gainAmount;

					CLevelRangeObject* possibleNewLevel = skillRef->m_levelRangeDB->GetLevelByExp((long)m_currentExperience);

					if (possibleNewLevel) {
						// the level has changed is the old experience and the new experience evaluate to different levels
						if (currentLevel->m_rangeStart != possibleNewLevel->m_rangeStart) {
							m_currentLevel = possibleNewLevel->m_levelRangeId;
							levelMessage = possibleNewLevel->m_levelMsg;

							if (useAlternate) {
								gainMessage = possibleNewLevel->m_altGainMsg;
							} else {
								gainMessage = possibleNewLevel->m_gainMsg;
							}

							newLevel = possibleNewLevel->m_levelNumber;
						}
						return SKILLRET_SUCCESS;
					}
				}
			}
		}
	}
	return SKILLRET_FAILED;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CStringA CSkillObject::GetRewardsAtNextLevel(CSkillObject* skillRef, CTitleObjectList* titles, CGlobalInventoryObjList* items, bool useAlternate) {
	CStringA retString = "";

	if (!skillRef->m_levelRangeDB)
		return 0;

	CLevelRangeObject* finalLevel = NULL;
	bool foundCurrentLevel = false;
	for (POSITION posLoc = skillRef->m_levelRangeDB->GetHeadPosition(); posLoc != NULL;) {
		CLevelRangeObject* lrObject = (CLevelRangeObject*)skillRef->m_levelRangeDB->GetNext(posLoc);

		if (foundCurrentLevel) {
			finalLevel = lrObject;
			break;
		}

		if (m_currentExperience >= lrObject->m_rangeStart && m_currentExperience < lrObject->m_rangeEnd) {
			//in range
			foundCurrentLevel = true;
		} //end in range
	}

	//caclulate percentages
	if (finalLevel) {
		CLevelRewardObjectList* rewardList = finalLevel->m_levelRewardDB;

		for (POSITION posLoc = rewardList->GetHeadPosition(); posLoc != NULL;) {
			CLevelRewardObject* rewardPtr = (CLevelRewardObject*)rewardList->GetNext(posLoc);

			if (useAlternate) {
				if (rewardPtr->m_altRewardGLID != 0) {
					CGlobalInventoryObj* globalInvObj = items->GetByGLID(rewardPtr->m_altRewardGLID);
					if (globalInvObj) {
						retString += globalInvObj->m_itemData->m_itemName + " \n "; //NOTE: m_itemName is now empty for UGC items at server side. Only work for Kaneva items.
					}
				}

				if (rewardPtr->m_altRewardAmount > 0) {
					int decimal, sign;
					char buffer[50];
					CStringA temp;
					_fcvt_s(buffer, _countof(buffer), rewardPtr->m_altRewardAmount, 0, &decimal, &sign);
					temp = buffer;

					if (rewardPtr->m_rewardPointType == IT_GIFT) {
						retString += temp + " Rewards \n";
					} else {
						retString += temp + " Credits \n ";
					}
				}

			} else {
				if (rewardPtr->m_rewardGLID != 0) {
					CGlobalInventoryObj* globalInvObj = items->GetByGLID(rewardPtr->m_rewardGLID);
					if (globalInvObj) {
						retString += globalInvObj->m_itemData->m_itemName + " \n "; //NOTE: m_itemName is now empty for UGC items at server side. Only work for Kaneva items.
					}
				}

				if (rewardPtr->m_rewardAmount > 0) {
					int decimal, sign;
					char buffer[50];
					CStringA temp;
					_fcvt_s(buffer, _countof(buffer), rewardPtr->m_rewardAmount, 0, &decimal, &sign);
					temp = buffer;

					if (rewardPtr->m_rewardPointType == IT_GIFT) {
						retString += temp + " Rewards \n";
					} else {
						retString += temp + " Credits \n ";
					}
				}
			}
		}

		if (useAlternate) {
			if (finalLevel->m_altTitleId != 0) {
				if (titles) {
					// get the title text to put onto the player's new title object
					CTitleObject* titleRef = titles->GetObjById(finalLevel->m_altTitleId);
					if (titleRef) {
						retString += "\"";
						retString += titleRef->m_titleName.c_str();
						retString += "\" Title \n ";
					}
				}
			}
		} else {
			if (finalLevel->m_titleId != 0) {
				if (titles) {
					// get the title text to put onto the player's new title object
					CTitleObject* titleRef = titles->GetObjById(finalLevel->m_titleId);
					if (titleRef) {
						retString += "\"";
						retString += titleRef->m_titleName.c_str();
						retString += "\" Title \n ";
					}
				}
			}
		}

		return retString;
	}
	return "";
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRewardObjectList* CSkillObject::GetCurrentLevelRewards(CSkillObject* skillRef) {
	if (skillRef) {
		if (skillRef->m_levelRangeDB) {
			CLevelRangeObject* currentLevel = skillRef->m_levelRangeDB->GetLevelByExp((long)m_currentExperience);

			if (currentLevel) {
				return currentLevel->m_levelRewardDB;
			}
		}
	}

	return NULL;
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObject::~CSkillObject() {
	if (m_levelRangeDB) {
		delete m_levelRangeDB;
		m_levelRangeDB = 0;
	}

	if (m_raceAdvantageDB) {
		delete m_raceAdvantageDB;
		m_raceAdvantageDB = 0;
	}

	if (m_skillActionDB) {
		delete m_skillActionDB;
		m_skillActionDB = 0;
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillObject::Clone(CSkillObject** clone) {
	CSkillObject* sklObj = new CSkillObject();
	sklObj->m_currentExperience = m_currentExperience;
	sklObj->m_currentLevel = m_currentLevel;
	sklObj->m_gainIncrement = m_gainIncrement;
	sklObj->m_skillName = m_skillName;
	sklObj->m_skillType = m_skillType;
	sklObj->m_basicAbilitySkill = m_basicAbilitySkill;
	sklObj->m_basicAbilityExpMastered = m_basicAbilityExpMastered;
	sklObj->m_basicAbilityFunction = m_basicAbilityFunction;
	sklObj->m_skillGainsWhenFailed = m_skillGainsWhenFailed;
	sklObj->m_maxMultiplier = m_maxMultiplier;
	sklObj->m_useInternalTimer = m_useInternalTimer;
	sklObj->m_skillId = m_skillId;

	if (m_levelRangeDB)
		m_levelRangeDB->Clone(&sklObj->m_levelRangeDB);

	if (m_raceAdvantageDB)
		m_raceAdvantageDB->Clone(&sklObj->m_raceAdvantageDB);

	if (m_skillActionDB)
		m_skillActionDB->Clone(&sklObj->m_skillActionDB);

	*clone = sklObj;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillObject::CloneMinimum(CSkillObject** clone) {
	CSkillObject* sklObj = new CSkillObject();
	sklObj->m_currentExperience = m_currentExperience;
	sklObj->m_currentLevel = m_currentLevel;
	sklObj->m_skillType = m_skillType;
	sklObj->m_skillName = m_skillName;
	sklObj->m_skillId = m_skillId;

	*clone = sklObj;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
BOOL CSkillObject::EvaluateLevelAndTitle(CStringA& title, int& level, CSkillObject* skillRef) {
	if (!skillRef->m_levelRangeDB)
		return FALSE;

	int levelTrace = 1;
	for (POSITION posLoc = skillRef->m_levelRangeDB->GetHeadPosition(); posLoc != NULL;) {
		CLevelRangeObject* lrObject = (CLevelRangeObject*)skillRef->m_levelRangeDB->GetNext(posLoc);

		if (m_currentExperience >= lrObject->m_rangeStart) {
			//in range
			title = lrObject->m_levelTitle;
			level = levelTrace;
		} //end in range
		levelTrace++;
	}

	return TRUE;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
BOOL CSkillObject::EvaluateLevel(int& level, CSkillObject* skillRef) {
	if (!skillRef->m_levelRangeDB)
		return FALSE;

	int levelTrace = 1;
	for (POSITION posLoc = skillRef->m_levelRangeDB->GetHeadPosition(); posLoc != NULL;) {
		CLevelRangeObject* lrObject = (CLevelRangeObject*)skillRef->m_levelRangeDB->GetNext(posLoc);
		if (m_currentExperience >= lrObject->m_rangeStart) {
			//in range
			level = levelTrace;
		} //end in range
		levelTrace++;
	}

	return TRUE;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CSkillObject::GetPercentageToNextLevel(CSkillObject* skillPtrRef) {
	if (!skillPtrRef->m_levelRangeDB)
		return 0;

	CLevelRangeObject* finalLevel = NULL;
	for (POSITION posLoc = skillPtrRef->m_levelRangeDB->GetHeadPosition(); posLoc != NULL;) {
		CLevelRangeObject* lrObject = (CLevelRangeObject*)skillPtrRef->m_levelRangeDB->GetNext(posLoc);
		if (m_currentExperience >= lrObject->m_rangeStart) {
			//in range
			finalLevel = lrObject;
		} //end in range
	}

	//caclulate percentages
	if (finalLevel) {
		float range = (float)(finalLevel->m_rangeEnd - finalLevel->m_rangeStart);
		float levelStart = (float)finalLevel->m_rangeStart;
		float positionInLevel = m_currentExperience - levelStart;
		if (positionInLevel >= range)
			return 100;

		if (range == 0)
			return 0;

		float multiplier = positionInLevel / range;
		return ((int)(100.0f * multiplier));
	}
	return 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillObject::Serialize(CArchive& ar) {
	char version = 0;

	int intReserved = 0;
	CSkillAbilityList* abilityDB = NULL;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		if (!m_minimalSave) {
			version = 3;
			ar << version;

			ar << m_skillName
			   << m_skillGainsWhenFailed
			   << m_currentExperience
			   << m_skillType
			   << m_maxMultiplier
			   << m_currentLevel
			   << m_levelRangeDB
			   << abilityDB
			   << m_gainIncrement
			   << m_raceAdvantageDB
			   << m_basicAbilitySkill
			   << m_basicAbilityExpMastered
			   << m_basicAbilityFunction
			   << m_useInternalTimer
			   << m_filterEvalLevel
			   << intReserved
			   << intReserved
			   << m_skillActionDB;
		} else { //minimal save
			version = 1;
			ar << version;

			ar << m_currentExperience
			   << m_skillType
			   << m_currentLevel;
		} //end minimal save
		m_minimalSave = FALSE;
	} else {
		ar >> version;
		//version = 0;

		m_filterEvalLevel = 0;

		if (version == 1) {
			ar >> m_currentExperience >> m_skillType >> m_currentLevel;
		} else if (version == 0) {
			ar >> m_skillName >> m_skillGainsWhenFailed >> m_currentExperience >> m_skillType >> m_maxMultiplier >> m_currentLevel >> m_levelRangeDB >> abilityDB >> m_gainIncrement >> m_raceAdvantageDB >> m_basicAbilitySkill >> m_basicAbilityExpMastered >> m_basicAbilityFunction >> m_useInternalTimer;
		} else if (version == 2) {
			ar >> m_skillName >> m_skillGainsWhenFailed >> m_currentExperience >> m_skillType >> m_maxMultiplier >> m_currentLevel >> m_levelRangeDB >> abilityDB >> m_gainIncrement >> m_raceAdvantageDB >> m_basicAbilitySkill >> m_basicAbilityExpMastered >> m_basicAbilityFunction >> m_useInternalTimer >> m_filterEvalLevel >> intReserved >> intReserved;
		} else if (version == 3) {
			ar >> m_skillName >> m_skillGainsWhenFailed >> m_currentExperience >> m_skillType >> m_maxMultiplier >> m_currentLevel >> m_levelRangeDB >> abilityDB >> m_gainIncrement >> m_raceAdvantageDB >> m_basicAbilitySkill >> m_basicAbilityExpMastered >> m_basicAbilityFunction >> m_useInternalTimer >> m_filterEvalLevel >> intReserved >> intReserved >> m_skillActionDB;
		}

		m_minimalSave = FALSE;
		m_lastUseTime = fTime::TimeMs();
		m_currentSkillDelay = 5000;
	}
}

void CSkillObject::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_currentExperience
		   << m_skillType
		   << m_currentLevel;
	} else {
		ar >> m_currentExperience >> m_skillType >> m_currentLevel;
	}
}

void CSkillObjectList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		if (!m_titles) {
			ar << -1;
		} else {
			int titleCount = (int)m_titles->GetCount();
			ar << titleCount;
			m_titles->Write(ar);
		}

		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CSkillObject* elementPtr = (CSkillObject*)GetNext(posLoc);
			elementPtr->SerializeAlloc(ar);
		}
	} else {
		int titleCount = 0;
		ar >> titleCount;

		m_titles = new CTitleObjectList();

		if (titleCount > -1) {
			m_titles->Read(ar, titleCount);
		}

		for (int i = 0; i < loadCount; i++) {
			CSkillObject* elementPtr = new CSkillObject();
			elementPtr->SerializeAlloc(ar);
			AddTail(elementPtr);
		}
	}
}

//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void CSkillObjectList::Serialize(CArchive& ar) {
	if (ar.IsStoring()) {
		if (!m_titles) {
			ar << -1;
		} else {
			int titleCount = (int)m_titles->GetCount();
			ar << titleCount;
			m_titles->Write(ar);
		}

		CKEPObList::Serialize(ar);
	} else {
		int version = ar.GetObjectSchema();

		if (version > 0) {
			int titleCount = 0;
			ar >> titleCount;

			m_titles = new CTitleObjectList();

			if (titleCount > -1) {
				m_titles->Read(ar, titleCount);
			}

			CKEPObList::Serialize(ar);
		} else {
			m_titles = new CTitleObjectList();

			CKEPObList::Serialize(ar);
		}
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObjectList::CSkillObjectList() {
	m_totalExperience = 0;
	m_speedGainCap = 15000.0f;
	m_titles = NULL;
}

// do we have it?  if not add it
bool CSkillObjectList::CheckSkill(int type, CSkillObjectList* dbList, CSkillObject** skPtr, CSkillObject** dbSkillPtr, bool& added) {
	if (dbList == 0 || skPtr == 0 || dbSkillPtr == 0)
		return FALSE;

	//see if we have used this skill before-if not add it to our database
	*skPtr = GetSkillByType(type);
	*dbSkillPtr = dbList->GetSkillByType(type);
	added = false;

	if (!*dbSkillPtr)
		return false; //safty first

	if (!(*skPtr)) {
		//need to add this skill
		(*dbSkillPtr)->CloneMinimum(skPtr);
		AddTail(*skPtr);
		added = true;
	} //end need to add this skill

	return true;
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CSkillObjectList::UseSkillByType(int type,
	CSkillObjectList* dbList,
	long expOfTarget,
	int levelToUse,
	int ability,
	int curDBCfg,
	CInventoryObjList* currentInv,
	int* specialFunction,
	CSkillObject** newAddedSkill,
	CStatObjList* currentStats,
	BOOL* updateStatsToClient,
	CLevelRangeObject** levelRaised,
	CSkillObject** skillPtr,
	int* returnMiscInt,
	int maxItemsInInventory,
	int* effectBind,
	ULONG curGlobalSkillDelay,
	ULONG curPlayerSkillTimeStamp) {
	//see if we have used this skill before-if not add it to our database
	CSkillObject* skPtr = 0;
	CSkillObject* dbSkillPtr = 0;
	bool added = false;
	if (!CheckSkill(type, dbList, &skPtr, &dbSkillPtr, added))
		return SKILLRET_FAILED;

	if (added) {
		*newAddedSkill = skPtr; //reference this
	} //end need to add this skill

	*effectBind = -1;

	float totalCurrentExp = UpdateTotalExperienceSum();

	*skillPtr = skPtr;
	return skPtr->UseSkill(expOfTarget,
		levelToUse,
		ability,
		curDBCfg,
		currentInv,
		specialFunction,
		currentStats,
		updateStatsToClient,
		levelRaised,
		returnMiscInt,
		totalCurrentExp,
		maxItemsInInventory,
		dbSkillPtr,
		effectBind,
		curGlobalSkillDelay,
		curPlayerSkillTimeStamp);
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObject* CSkillObjectList::GetSkillByType(int type) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		if (skPtr->m_skillType == type)
			return skPtr;
	}
	return NULL;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObject* CSkillObjectList::GetSkillByName(const char* skillName) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		if (skPtr->m_skillName == skillName)
			return skPtr;
	}
	return NULL;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CSkillObjectList::GetEfficiencyByTypeAndLevel(int type,
	CSkillObjectList* charSkillList,
	int* proficiency,
	int* perfective) {
	if (!charSkillList)
		return 0;

	CSkillObject* skillPtr = GetSkillByType(type);
	if (!skillPtr)
		return 0;

	CSkillObject* charSkill = charSkillList->GetSkillByType(type);
	if (!charSkill)
		return 0;

	if (!skillPtr->m_levelRangeDB)
		return 0;

	int levelTrace = 1;
	for (POSITION posLoc = skillPtr->m_levelRangeDB->GetHeadPosition(); posLoc != NULL;) {
		CLevelRangeObject* lrObject = (CLevelRangeObject*)skillPtr->m_levelRangeDB->GetNext(posLoc);

		if (charSkill->m_currentExperience >= lrObject->m_rangeStart) {
			//in range

			*proficiency = lrObject->m_profficiencyBonus;
			*perfective = lrObject->m_perfectiveBonus;
		} //end in range
		else
			return 0;

		levelTrace++;
	}
	return 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObject* CSkillObjectList::GetSkillByIndex(int index) {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		if (trace == index)
			return skPtr;
		trace++;
	}
	return NULL;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CSkillObjectList::GetSkillIndexByType(int type) {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		if (skPtr->m_skillType == type)
			return trace;
		trace++;
	}
	return -1;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CStringA CSkillObjectList::GetTitleByLevelAndType(int type, int level) {
	CSkillObject* skillPtr = GetSkillByType(type);
	if (skillPtr)
		if (skillPtr->m_levelRangeDB) {
			CLevelRangeObject* levelPtr = skillPtr->m_levelRangeDB->GetObjByIndex(level - 1);
			if (levelPtr) {
				return levelPtr->m_levelTitle;
			}
		}
	return "";
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
/*CStringA CSkillObjectList::GetTitleByLevelAndType(int type,int level)
{
	 CSkillObject * skillPtr = GetSkillByType(type);
     if(skillPtr)
     if(skillPtr->m_levelRangeDB)
	 {
        CLevelRangeObject * levelPtr = skillPtr->m_levelRangeDB->GetObjByIndex(level-1);
		if(levelPtr)
		{
		   return levelPtr->m_levelTitle;
		}
	 }
	 return "";
}*/
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
BOOL CSkillObjectList::EvaluateRequiredSkillLevel(int type, int requiredLevel, CSkillObjectList* skillDBRef) {
	CSkillObject* skillPtr = GetSkillByType(type);
	CSkillObject* skillPtrRef = skillDBRef->GetSkillByType(type);
	if (skillPtr) {
		int curLevel = 1;
		skillPtr->EvaluateLevel(curLevel, skillPtrRef);
		if (curLevel >= requiredLevel)
			return TRUE;
	}
	return FALSE;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObject* CSkillObjectList::GetBestSkill(CSkillObjectList* skillDB) {
	if (!skillDB)
		return NULL;

	CSkillObject* highestSkill = NULL;
	int curhighestLevel = 1;
	int testLevel = 1;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);

		CSkillObject* skillPtrRef = skillDB->GetSkillByType(skPtr->m_skillType);
		if (!skillPtrRef)
			continue;
		if (!highestSkill) {
			skPtr->EvaluateLevel(curhighestLevel, skillPtrRef);
			highestSkill = skPtr;
			continue;
		}
		if (skPtr->EvaluateLevel(testLevel, skillPtrRef)) {
			if (curhighestLevel < testLevel) {
				highestSkill = skPtr;
				curhighestLevel = testLevel;
			}
		}
	}
	return highestSkill;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObject* CSkillObjectList::GetBestSkillWEval(CSkillObjectList* skillDB) {
	if (!skillDB)
		return NULL;

	CSkillObject* highestSkill = NULL;
	int curhighestLevel = 1;
	int testLevel = 1;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);

		CSkillObject* skillPtrRef = skillDB->GetSkillByType(skPtr->m_skillType);
		if (!skillPtrRef)
			continue;

		if (skillPtrRef->m_filterEvalLevel)
			continue;

		if (!highestSkill) {
			skPtr->EvaluateLevel(curhighestLevel, skillPtrRef);
			highestSkill = skPtr;
			continue;
		}
		if (skPtr->EvaluateLevel(testLevel, skillPtrRef)) {
			if (curhighestLevel < testLevel) {
				highestSkill = skPtr;
				curhighestLevel = testLevel;
			}
		}
	}
	return highestSkill;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillObjectList::Clone(CSkillObjectList** clone) const {
	CSkillObjectList* copyLocal = new CSkillObjectList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		CSkillObject* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillObjectList::CloneMinimum(CSkillObjectList** clone) const {
	CSkillObjectList* copyLocal = new CSkillObjectList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		CSkillObject* clonePtr = NULL;
		skPtr->CloneMinimum(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillObjectList::~CSkillObjectList() {
	if (m_titles) {
		delete m_titles;
		m_titles = 0;
	}

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}
//------------------------------------------------------------//
//------------------------------------------------------------//
//------------------------------------------------------------//
int CSkillObjectList::GetFreeID() {
	int trace = 1;

	BOOL done = FALSE;
	while (!done) {
		done = TRUE;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CSkillObject* spnPtr = (CSkillObject*)GetNext(posLoc);
			if (spnPtr->m_skillType == trace) {
				done = FALSE;
				trace++;
				break;
			}
		}
	}

	return trace;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
float CSkillObjectList::UpdateTotalExperienceSum() {
	m_totalExperience = 0.0f;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		m_totalExperience += skPtr->m_currentExperience;
	}
	return m_totalExperience;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillObjectList::SetOptimizedSave(BOOL optimized) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillObject* skPtr = (CSkillObject*)GetNext(posLoc);
		skPtr->m_minimalSave = optimized;
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillActionObject::CSkillActionObject() {
	m_actionID = 0;
	m_skillID = 0;
	m_gainAmount = 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillActionObject::Clone(CSkillActionObject** clone) {
	CSkillActionObject* copyLocal = new CSkillActionObject();

	copyLocal->m_actionID = m_actionID;
	copyLocal->m_skillID = m_skillID;
	copyLocal->m_gainAmount = m_gainAmount;

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillActionObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_actionID
		   << m_skillID
		   << m_gainAmount;
	} else {
		ar >> m_actionID >> m_skillID >> m_gainAmount;
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillActionObjectList::~CSkillActionObjectList() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillActionObject* skPtr = (CSkillActionObject*)GetNext(posLoc);
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CSkillActionObjectList::Clone(CSkillActionObjectList** clone) {
	CSkillActionObjectList* copyLocal = new CSkillActionObjectList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillActionObject* skPtr = (CSkillActionObject*)GetNext(posLoc);
		CSkillActionObject* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillActionObject* CSkillActionObjectList::GetObjByIndex(int index) {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSkillActionObject* skPtr = (CSkillActionObject*)GetNext(posLoc);
		if (trace == index)
			return skPtr;
		trace++;
	}
	return NULL;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CSkillActionObject* CSkillActionObjectList::GetActionById(int actionId) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CSkillActionObject* actionPtr = (CSkillActionObject*)GetNext(posLoc);
		if (actionPtr->m_actionID == actionId)
			return actionPtr;
	}
	return NULL;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CSkillActionObjectList::GetFreeID() {
	int trace = 1;
	BOOL done = FALSE;

	while (!done) {
		done = TRUE;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CSkillActionObject* titlePtr = (CSkillActionObject*)GetNext(posLoc);
			if (titlePtr->m_actionID == trace) {
				done = FALSE;
				trace++;
				break;
			}
		}
	}

	return trace;
}

//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void CSkillActionObjectList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CSkillActionObject* elementPtr = (CSkillActionObject*)GetNext(posLoc);
			elementPtr->Serialize(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			CSkillActionObject* elementPtr = new CSkillActionObject();
			elementPtr->Serialize(ar);
			AddTail(elementPtr);
		}
	}
}

//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRewardObject::CLevelRewardObject() {
	m_rewardId = 0;
	m_skillId = 0;
	m_levelId = 0;
	m_rewardGLID = 0;
	m_rewardQty = 0;
	m_rewardPointType = 0;
	m_rewardAmount = 0;
	m_altRewardGLID = 0;
	m_altRewardQty = 0;
	m_altRewardAmount = 0;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CLevelRewardObject::Clone(CLevelRewardObject** clone) {
	CLevelRewardObject* copyLocal = new CLevelRewardObject();

	copyLocal->m_rewardId = m_rewardId;
	copyLocal->m_skillId = m_skillId;
	copyLocal->m_levelId = m_levelId;
	copyLocal->m_rewardGLID = m_rewardGLID;
	copyLocal->m_rewardQty = m_rewardQty;
	copyLocal->m_rewardPointType = m_rewardPointType;
	copyLocal->m_rewardAmount = m_rewardAmount;
	copyLocal->m_altRewardGLID = m_altRewardGLID;
	copyLocal->m_altRewardQty = m_altRewardQty;
	copyLocal->m_altRewardAmount = m_altRewardAmount;

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CLevelRewardObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_rewardId
		   << m_skillId
		   << m_levelId
		   << m_rewardGLID
		   << m_rewardQty
		   << m_rewardPointType
		   << m_rewardAmount
		   << m_altRewardGLID
		   << m_altRewardQty
		   << m_altRewardAmount;
	} else {
		int version = ar.GetObjectSchema();

		if (version == 1) {
			ar >> m_rewardId >> m_skillId >> m_levelId >> m_rewardGLID >> m_rewardQty >> m_rewardPointType >> m_rewardAmount >> m_altRewardGLID >> m_altRewardQty >> m_altRewardAmount;
		} else {
			ar >> m_rewardId >> m_skillId >> m_levelId >> m_rewardGLID >> m_rewardQty >> m_rewardPointType >> m_rewardAmount;
		}
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRewardObjectList::~CLevelRewardObjectList() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CLevelRewardObject* skPtr = (CLevelRewardObject*)GetNext(posLoc);
		delete skPtr;
		skPtr = 0;
	}
	RemoveAll();
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
void CLevelRewardObjectList::Clone(CLevelRewardObjectList** clone) {
	CLevelRewardObjectList* copyLocal = new CLevelRewardObjectList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CLevelRewardObject* skPtr = (CLevelRewardObject*)GetNext(posLoc);
		CLevelRewardObject* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
CLevelRewardObject* CLevelRewardObjectList::GetObjByIndex(int index) {
	int trace = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CLevelRewardObject* skPtr = (CLevelRewardObject*)GetNext(posLoc);
		if (trace == index)
			return skPtr;
		trace++;
	}
	return NULL;
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//
int CLevelRewardObjectList::GetFreeID() {
	int trace = 1;
	BOOL done = FALSE;

	while (!done) {
		done = TRUE;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CLevelRewardObject* titlePtr = (CLevelRewardObject*)GetNext(posLoc);
			if (titlePtr->m_rewardId == trace) {
				done = FALSE;
				trace++;
				break;
			}
		}
	}

	return trace;
}

//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void CLevelRewardObjectList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CLevelRewardObject* elementPtr = (CLevelRewardObject*)GetNext(posLoc);
			elementPtr->Serialize(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			CLevelRewardObject* elementPtr = new CLevelRewardObject();
			elementPtr->Serialize(ar);
			AddTail(elementPtr);
		}
	}
}
//-----------------------------------------------------------//
//-----------------------------------------------------------//
//-----------------------------------------------------------//

void CLevelRangeObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_abilityDB);
		pMemSizeGadget->AddObject(m_altGainMsg);
		pMemSizeGadget->AddObject(m_gainMsg);
		pMemSizeGadget->AddObject(m_levelMsg);
		pMemSizeGadget->AddObject(m_levelRewardDB);
		pMemSizeGadget->AddObject(m_levelTitle);
	}
}

void CLevelRewardObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);
	}
}

void CRacialAdvantageObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CSkillAbilityObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_abilityName);
		pMemSizeGadget->AddObject(m_requiredItems);
		pMemSizeGadget->AddObject(m_requiredStatistics);
		pMemSizeGadget->AddObject(m_resultItems);
	}
}

void CSkillActionObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);
	}
}

void CSkillObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_levelRangeDB);
		pMemSizeGadget->AddObject(m_skillName);
		pMemSizeGadget->AddObject(m_skillActionDB);
	}
}

void CLevelRangeList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CLevelRangeObject* pCLevelRangeObject = static_cast<const CLevelRangeObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCLevelRangeObject);
		}
	}
}

void CLevelRewardObjectList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CLevelRewardObject* pCLevelRewardObject = static_cast<const CLevelRewardObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCLevelRewardObject);
		}
	}
}

void CRacialAdvantageList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CRacialAdvantageObject* pCRacialAdvantageObject = static_cast<const CRacialAdvantageObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCRacialAdvantageObject);
		}
	}
}

void CSkillAbilityList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CSkillAbilityObject* pCSkillAbilityObject = static_cast<const CSkillAbilityObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCSkillAbilityObject);
		}
	}
}

void CSkillActionObjectList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CSkillActionObject* pCSkillActionObject = static_cast<const CSkillActionObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCSkillActionObject);
		}
	}
}

void CSkillObjectList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CSkillObject* pCSkillObject = static_cast<const CSkillObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCSkillObject);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CSkillAbilityObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CSkillAbilityList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CSkillObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CSkillObjectList, CKEPObList)
IMPLEMENT_SERIAL_SCHEMA(CLevelRangeObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CLevelRangeList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CRacialAdvantageObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CRacialAdvantageList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CSkillActionObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CSkillActionObjectList, CKEPObList)
IMPLEMENT_SERIAL_SCHEMA(CLevelRewardObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CLevelRewardObjectList, CKEPObList)

BEGIN_GETSET_IMPL(CSkillAbilityObject, IDS_SKILLABILITYOBJECT_NAME)
GETSET_MAX(m_abilityName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, ABILITYNAME, STRLEN_MAX)
GETSET_RANGE(m_minimumLevelToUse, gsLong, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, MINIMUMLEVELTOUSE, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_miscAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, MISCAMOUNT, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_requiredStatistics, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, REQUIREDSTATISTICS)
GETSET_RANGE(m_functionType, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, FUNCTIONTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_miscInt, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, MISCINT, INT_MIN, INT_MAX)
GETSET_RANGE(m_miscInt2, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, MISCINT2, INT_MIN, INT_MAX)
GETSET_RANGE(m_miscInt3, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, MISCINT3, INT_MIN, INT_MAX)
GETSET_RANGE(m_masterLevel, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, MASTERLEVEL, FLOAT_MIN, FLOAT_MAX)
GETSET_OBLIST_PTR(m_requiredItems, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, REQUIREDITEMS)
GETSET_OBLIST_PTR(m_resultItems, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, RESULTITEMS)
GETSET_RANGE(m_effectBind, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLABILITYOBJECT, EFFECTBIND, INT_MIN, INT_MAX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CSkillObject, IDS_SKILLOBJECT_NAME)
GETSET_MAX(m_skillName, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, SKILLOBJECT, SKILLNAME, STRLEN_MAX)
GETSET_RANGE(m_skillGainsWhenFailed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, SKILLGAINSWHENFAILED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_currentExperience, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, CURRENTEXPERIENCE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_skillType, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, SKILLTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_maxMultiplier, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, MAXMULTIPLIER, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_currentLevel, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, CURRENTLEVEL, INT_MIN, INT_MAX)
GETSET_RANGE(m_gainIncrement, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, GAININCREMENT, FLOAT_MIN, FLOAT_MAX)
GETSET_OBLIST_PTR(m_raceAdvantageDB, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, RACEADVANTAGEDB)
GETSET(m_basicAbilitySkill, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, BASICABILITYSKILL)
GETSET_RANGE(m_basicAbilityExpMastered, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, BASICABILITYEXPMASTERED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_basicAbilityFunction, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, BASICABILITYFUNCTION, INT_MIN, INT_MAX)
GETSET(m_useInternalTimer, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, USEINTERNALTIMER)
GETSET(m_filterEvalLevel, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, FILTEREVALLEVEL)
GETSET_OBLIST_PTR(m_levelRangeDB, GS_FLAGS_DEFAULT, 0, 0, SKILLOBJECT, LEVELRANGEDB)
GETSET2_OBLIST_PTR(m_skillActionDB, GS_FLAGS_DEFAULT, 0, 0, IDS_SKILLOBJECT_SKILLACTIONDB, IDS_SKILLOBJECT_SKILLACTIONDB)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CSkillActionObject, IDS_SKILLACTIONOBJECT_NAME)
GETSET_RANGE(m_actionID, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLACTIONOBJECT, ACTIONID, INT_MIN, INT_MAX)
GETSET_RANGE(m_skillID, gsInt, GS_FLAGS_DEFAULT, 0, 0, SKILLACTIONOBJECT, SKILLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_gainAmount, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SKILLACTIONOBJECT, GAINAMOUNT, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CLevelRangeObject, IDS_LEVELRANGEOBJECT_NAME)
GETSET_RANGE(m_levelRangeId, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELRANGEOBJECT, LEVELRANGEID, INT_MIN, INT_MAX)
GETSET_RANGE(m_rangeStart, gsLong, GS_FLAGS_DEFAULT, 0, 0, LEVELRANGEOBJECT, RANGESTART, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_rangeEnd, gsLong, GS_FLAGS_DEFAULT, 0, 0, LEVELRANGEOBJECT, RANGEEND, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_titleId, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELRANGEOBJECT, TITLEID, INT_MIN, INT_MAX)
GETSET_RANGE(m_altTitleId, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELRANGEOBJECT, ALTTITLEID, INT_MIN, INT_MAX)
GETSET_MAX(m_gainMsg, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, LEVELRANGEOBJECT, GAINMSG, STRLEN_MAX)
GETSET_MAX(m_altGainMsg, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, LEVELRANGEOBJECT, ALTGAINMSG, STRLEN_MAX)
GETSET_MAX(m_levelMsg, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, LEVELRANGEOBJECT, LEVELMSG, STRLEN_MAX)
GETSET_RANGE(m_levelNumber, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELRANGEOBJECT, LEVELNUMBER, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_levelRewardDB, GS_FLAGS_DEFAULT, 0, 0, LEVELRANGEOBJECT, LEVELREWARDDB)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CLevelRewardObject, IDS_LEVELREWARDOBJECT_NAME)
GETSET_RANGE(m_rewardId, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, REWARDID, INT_MIN, INT_MAX)
GETSET_RANGE(m_skillId, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, SKILLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_levelId, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, LEVELID, INT_MIN, INT_MAX)
GETSET_RANGE(m_rewardGLID, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, REWARDGLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_rewardQty, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, REWARDQTY, INT_MIN, INT_MAX)
GETSET_RANGE(m_rewardPointType, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, REWARDPOINTTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_rewardAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, REWARDAMOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_altRewardGLID, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, ALTREWARDGLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_altRewardQty, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, ALTREWARDQTY, INT_MIN, INT_MAX)
GETSET_RANGE(m_altRewardAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, LEVELREWARDOBJECT, ALTREWARDAMOUNT, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP