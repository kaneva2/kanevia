///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "InputSystem.h"
#include "KeyMapParser.h"
#include "KeyCodeMap.h"
#include "Core/Math/KEPMathUtil.h"

#include "LogHelper.h"
static LogInstance("InputSystem");

namespace KEP {

InputSystem::InputSystem(eKeyCodeType keyCodeType) :
		m_KeyCodeType(keyCodeType), m_dPreviousProcessTime(-DBL_MAX) {
	m_fnGetKeyState = std::bind(&InputSystem::GetKeyState, this, std::placeholders::_1);

	std::vector<int> aKeyCodes;
	GetKeyCodeMap(m_KeyCodeType)->GetAllKeyCodes(out(aKeyCodes));
	for (int iKeyCode : aKeyCodes) {
		// Use operator[] to default-construct an entry for the key code.
		// Other methods of insertion require a copy constructor, which is not available for KeyStateData.
		m_KeyStateDataMap[iKeyCode];
	}
}

void InputSystem::EnqueueKeyStateChange(double dRealTime, int keyCode, bool bDown) {
	// Insert new element at upper_bound() to make sure events enqueued at the same time are kept in the order that they were inserted.
	auto itr = m_KeyStateChangeQueue.upper_bound(dRealTime);
	m_KeyStateChangeQueue.insert(itr, std::make_pair(dRealTime, KeyStateChangeData{ keyCode, bDown ? eKeyStateType::Down : eKeyStateType::Up, Vector2i(0, 0), false }));
}

void InputSystem::EnqueueMouseButtonStateChange(double dRealTime, int keyCode, bool bDown, Vector2i point) {
	// Insert new element at upper_bound() to make sure events enqueued at the same time are kept in the order that they were inserted.
	auto itr = m_KeyStateChangeQueue.upper_bound(dRealTime);
	m_KeyStateChangeQueue.insert(itr, std::make_pair(dRealTime, KeyStateChangeData{ keyCode, bDown ? eKeyStateType::Down : eKeyStateType::Up, point, true }));
}

void InputSystem::ClearInputQueue() {
	m_KeyStateChangeQueue.clear();
}

void InputSystem::CancelActiveHandlers(double dRealTime) {
	for (EventHandler* pEventHandler : m_ActiveHandlers) {
		pEventHandler->CancelHandlerFunction(dRealTime);
	}

	m_ActiveHandlers.clear();
}

void InputSystem::ResetKeys(double dTime) {
	std::vector<int> aKeyCodes;
	GetKeyCodeMap(m_KeyCodeType)->GetAllKeyCodes(out(aKeyCodes));
	for (int iKeyCode : aKeyCodes) {
		EnqueueKeyStateChange(dTime, iKeyCode, false);
	}
}

void InputSystem::UpdateKeyStates(const std::vector<int>& aDepressedKeys, double dCurrentTime) {
	std::vector<int> aSortedDepressedKeys(aDepressedKeys);
	std::sort(aSortedDepressedKeys.begin(), aSortedDepressedKeys.end());

	// Update keys
	std::vector<KeyBinding*> apAffectedKeyBindings;
	for (auto itr_keystate = m_KeyStateDataMap.begin(); itr_keystate != m_KeyStateDataMap.end(); ++itr_keystate) {
		eKeyStateType newKeyState = eKeyStateType::Up;
		if (std::binary_search(aSortedDepressedKeys.begin(), aSortedDepressedKeys.end(), itr_keystate->first))
			newKeyState = eKeyStateType::Down;
		int iKeyCode = itr_keystate->first;
		KeyStateChangeData keyStateChangeData{ iKeyCode, newKeyState };
		apAffectedKeyBindings.clear();
		HandleKeyStateChange(keyStateChangeData, dCurrentTime, &apAffectedKeyBindings);
		for (KeyBinding* pKeyBinding : apAffectedKeyBindings)
			pKeyBinding->KeyChanged();
	}

	// Update key bindings that have no key dependencies
	for (const std::unique_ptr<KeyBinding>& pKeyBinding : m_aKeyBindings) {
		auto itr_handler = m_EventHandlerMap.find(pKeyBinding->m_strActionName);
		if (itr_handler == m_EventHandlerMap.end())
			continue; // No handler.

		itr_handler->second.UpdateBindingState(pKeyBinding.get());
	}

	// Update list of active handlers.
	for (auto& mapEntry : m_EventHandlerMap) {
		EventHandler* pEventHandler = &mapEntry.second;
		if (pEventHandler->IsActive()) {
			m_ActiveHandlers.insert(pEventHandler).second;
		} else {
			m_ActiveHandlers.erase(pEventHandler);
		}
	}
}

void InputSystem::RegisterHandler(const std::string& strActionName, HandlerFunctionType handlerFunction) {
	EventHandler& eventHandler = m_EventHandlerMap[strActionName];
	eventHandler.SetHandlerFunction(std::move(handlerFunction)); // Replaces any existing handler.
}

size_t InputSystem::AddBindingsFromStream(std::istream& strm) {
	std::vector<KeyEventBindingByName> aKeyEventBindings;

	bool bParseSuccessful = ParseKeyMappings(strm, m_KeyCodeType, out(aKeyEventBindings));
	if (!bParseSuccessful) {
		// Parse error. Could have been partially successful, in which case aKeyEventBindings will be non-empty.
		// Error should have already been logged in call to ParseKeyMappings.
	}

	size_t nBindingsAdded = 0;
	for (const KeyEventBindingByName& keb : aKeyEventBindings)
		nBindingsAdded += AddBinding(keb.m_strActionName, keb.m_BindableKeyEvent);

	return nBindingsAdded == aKeyEventBindings.size();
}

bool InputSystem::AddBinding(std::string strActionName, const BindableKeyEvent& bindableKeyEvent) {
	// Create KeyBinding object.
	std::unique_ptr<KeyBinding> pKeyBinding = std::make_unique<KeyBinding>();
	pKeyBinding->m_KeyEvents.insert(bindableKeyEvent.m_aKeyEvents.begin(), bindableKeyEvent.m_aKeyEvents.end());
	pKeyBinding->m_strActionName = std::move(strActionName);

	// Connect key state logic.
	std::shared_ptr<BooleanLogic::LogicComponent> pKeyCombLogic;
	if (!bindableKeyEvent.m_KeyCombination.IsEmpty()) {
		std::set<int> keysUsed;
		pKeyCombLogic = BuildLogic(bindableKeyEvent.m_KeyCombination, &keysUsed);
		if (!pKeyCombLogic) {
			LogError("Error generating key logic from key combination description.");
			return false;
		}
		for (int iKey : keysUsed)
			m_KeyStateDataMap[iKey].AddDependentKeyBinding(pKeyBinding.get());
	}

	if (!pKeyBinding->m_KeyEvents.empty()) {
		std::vector<std::shared_ptr<BooleanLogic::LogicComponent>> aLogicInputs;
		for (const KeyEvent& keyEvent : pKeyBinding->m_KeyEvents) {
			aLogicInputs.emplace_back(GetKeyLogic(keyEvent.m_iKeyId, eKeyStateType::Down));
			m_KeyStateDataMap[keyEvent.m_iKeyId].AddDependentKeyBinding(pKeyBinding.get());
		}
		if (BooleanLogic::And* pAnd = dynamic_cast<BooleanLogic::And*>(pKeyCombLogic.get())) {
			pAnd->AddInputs(aLogicInputs.begin(), aLogicInputs.end());
		} else {
			std::shared_ptr<BooleanLogic::LogicComponent> pEventKeys;
			if (aLogicInputs.size() == 1)
				pEventKeys = aLogicInputs.front();
			else
				pEventKeys = std::make_shared<BooleanLogic::Or>(aLogicInputs.begin(), aLogicInputs.end());

			if (!pKeyCombLogic) {
				pKeyCombLogic = pEventKeys;
			} else {
				std::shared_ptr<BooleanLogic::LogicComponent> apAndComponents[2] = { pEventKeys, pKeyCombLogic };
				pKeyCombLogic = std::make_shared<BooleanLogic::And>(&apAndComponents[0], &apAndComponents[0] + 2);
			}
		}
	}

	if (!pKeyCombLogic)
		pKeyCombLogic = std::make_shared<BooleanLogic::Variable>(true);
	pKeyBinding->m_pKeyStateLogic = std::move(pKeyCombLogic);

	// Commit new KeyBinding
	m_aKeyBindings.push_back(std::move(pKeyBinding));
	return true;
}

std::shared_ptr<BooleanLogic::LogicComponent> InputSystem::GetKeyLogic(int iKeyCode, eKeyStateType keyState) {
	KeyStateData& keyStateData = m_KeyStateDataMap[iKeyCode];
	if (keyState == eKeyStateType::Down)
		return keyStateData.GetKeyDownLogicVariable();
	else if (keyState == eKeyStateType::Up)
		return keyStateData.GetKeyUpLogicVariable();
	else
		return nullptr;
}

std::shared_ptr<BooleanLogic::LogicComponent> InputSystem::BuildLogic(const KeyStateCombination& ksc, std::set<int>* pKeysUsed) {
	std::vector<std::shared_ptr<BooleanLogic::LogicComponent>> aLogicInputs;
	for (const KeyStateCombination& keyComb : ksc.m_aCombinations) {
		std::shared_ptr<BooleanLogic::LogicComponent> pInput = BuildLogic(keyComb, pKeysUsed);
		if (!pInput)
			return nullptr;
		aLogicInputs.emplace_back(std::move(pInput));
	}

	for (const auto& keyState : ksc.m_aKeyStates) {
		std::shared_ptr<BooleanLogic::LogicComponent> pKeyStateLogic = GetKeyLogic(keyState.m_iKeyId, keyState.m_keyStateType);
		if (!pKeyStateLogic)
			return nullptr;
		if (pKeysUsed)
			pKeysUsed->insert(keyState.m_iKeyId);
		aLogicInputs.emplace_back(std::move(pKeyStateLogic));
	}

	if (aLogicInputs.empty())
		return nullptr;

	switch (ksc.m_Operator) {
		case eLogicalOperator::And:
			return std::make_shared<BooleanLogic::And>(aLogicInputs.begin(), aLogicInputs.end());
		case eLogicalOperator::Or:
			return std::make_shared<BooleanLogic::Or>(aLogicInputs.begin(), aLogicInputs.end());
		case eLogicalOperator::Xor:
			return std::make_shared<BooleanLogic::Xor>(aLogicInputs.begin(), aLogicInputs.end());
		case eLogicalOperator::Not:
			if (aLogicInputs.size() != 1)
				return nullptr;
			return std::make_shared<BooleanLogic::Not>(aLogicInputs.front());
		case eLogicalOperator::Identity:
			if (aLogicInputs.size() != 1)
				return nullptr;
			return std::move(aLogicInputs.front());
	}

	LogError("Unknown logical operator value: " << int(ksc.m_Operator));
	return nullptr;
}

eKeyEventType InputSystem::HandleKeyStateChange(const KeyStateChangeData& kscd, double dEventTime, std::vector<KeyBinding*>* papAffectedKeyBindings) {
	auto itr_keyState = m_KeyStateDataMap.find(kscd.m_iKeyCode);
	if (itr_keyState == m_KeyStateDataMap.end())
		return eKeyEventType::None;

	KeyStateData& ksd = itr_keyState->second;
	if (ksd.GetKeyState() == kscd.m_NewKeyState && kscd.m_bIsMouseInput == false)
		return eKeyEventType::None;

	eKeyEventType keyEvent = ksd.SetKeyState(kscd, dEventTime, papAffectedKeyBindings);
	return keyEvent;
}

void InputSystem::ProcessQueuedEventsUntil(double dRealTime) {
	auto itr_keyChange = m_KeyStateChangeQueue.begin();
	while (itr_keyChange != m_KeyStateChangeQueue.end() && itr_keyChange->first <= dRealTime) {
		std::vector<KeyBinding*> aChangedBindings; // List of handlers whose state may have changed (probably, but not necessarily) and need to be processed because of a key event.

		const KeyStateChangeData& kscd = itr_keyChange->second;
		double dEventTime = Clamp(itr_keyChange->first, m_dPreviousProcessTime, dRealTime); // Clamp key event time to occur during this frame.
		//eKeyEventType keyEventType = HandleKeyStateChange(kscd, dEventTime, &aChangedHandlers);
		eKeyEventType keyEventType = HandleKeyStateChange(kscd, dEventTime, &aChangedBindings);
		KeyEvent keyEvent{ kscd.m_iKeyCode, keyEventType };
		//LogInfo("InputSystem::ProcessQueuedEventsUntil x: " << kscd.m_mouseMovement.x << ", y: " << kscd.m_mouseMovement.y);
		// Call handlers to respond to activation/deactivation
		std::vector<std::pair<EventHandler*, int>> aChangedHandlers; // List of handlers changed and whether the handler needs to be called for activation(2), deactivation(1), or both(3).

		// Update handler states. Add changed handler to list and whether the handler
		for (KeyBinding* pKeyBinding : aChangedBindings) {
			auto itr_handler = m_EventHandlerMap.find(pKeyBinding->m_strActionName);
			if (itr_handler == m_EventHandlerMap.end())
				return; // No handler.

			EventHandler& eventHandler = itr_handler->second;
			pKeyBinding->KeyChanged();
			bool bActivationChanged = eventHandler.UpdateBindingState(pKeyBinding);
			eventHandler.UpdateMouseMovement(kscd.m_mouseMovement);

			if (bActivationChanged) {
				bool bHandlesEvent = pKeyBinding->m_KeyEvents.empty() || pKeyBinding->m_KeyEvents.find(keyEvent) != pKeyBinding->m_KeyEvents.end();
				bool bActivated = pKeyBinding->IsStateSatisfied();

				// Flag is 2 for activating, 1 for deactivating, 0 if no call needed for this handler (but still need to update activation state)
				aChangedHandlers.push_back(std::make_pair(&itr_handler->second, bHandlesEvent << int(bActivated)));
			}
		}

		std::sort(aChangedHandlers.begin(), aChangedHandlers.end());

		for (size_t i = 0; i < aChangedHandlers.size();) {
			// Call action handler only if at least one key binding needs it.
			// Make sure each event handler is called no more than once even if multiple keybindings trigger it.
			EventHandler* pEventHandler = aChangedHandlers[i].first;
			int iNeedHandlerCall = aChangedHandlers[i].second;
			size_t j = i + 1;
			while (j != aChangedHandlers.size() && aChangedHandlers[j].first == pEventHandler) {
				iNeedHandlerCall |= aChangedHandlers[j].second;
				++j;
			}

			eActionHandlerEventType eventType = eActionHandlerEventType::None;

			// Update list of active handlers.
			// If no net change to handler state, handler should not be called.
			if (pEventHandler->IsActive()) {
				bool bActivated = m_ActiveHandlers.insert(pEventHandler).second;
				if (bActivated) {
					pEventHandler->Activate(dEventTime);
					if (iNeedHandlerCall & 2)
						eventType = eActionHandlerEventType::Activate;
				}
			} else {
				bool bDeactivated = m_ActiveHandlers.erase(pEventHandler) != 0;
				if (bDeactivated) {
					pEventHandler->Deactivate(dEventTime);
					if (iNeedHandlerCall & 1)
						eventType = eActionHandlerEventType::Deactivate;
				}
			}
			if (eventType != eActionHandlerEventType::None) {
				pEventHandler->CallHandlerFunction(eventType, dEventTime, keyEvent, m_fnGetKeyState);
			}

			i = j;
		}
		aChangedHandlers.clear();

		itr_keyChange = m_KeyStateChangeQueue.erase(itr_keyChange);
	}

	ProcessActiveHandlers(dRealTime);
	m_dPreviousProcessTime = dRealTime;

	if (m_fnOnQueuedEventsProcessed) {
		m_fnOnQueuedEventsProcessed();
	}
}

void InputSystem::ProcessActiveHandlers(double dRealTime) {
	// Process active state-based handlers.
	KeyEvent keyEventNone{ -1, eKeyEventType::None };
	for (auto itr_handler = m_ActiveHandlers.begin(); itr_handler != m_ActiveHandlers.end(); ++itr_handler) {
		EventHandler* pEventHandler = *itr_handler;
		if (pEventHandler->NeedsSustainCall())
			pEventHandler->CallHandlerFunction(eActionHandlerEventType::Sustain, dRealTime, keyEventNone, m_fnGetKeyState);
		pEventHandler->UpdateLastProcessTime(dRealTime);
	}
}

eKeyStateType InputSystem::GetKeyState(int iKeyCode) {
	auto itr = m_KeyStateDataMap.find(iKeyCode);
	if (itr != m_KeyStateDataMap.end())
		return itr->second.GetKeyState();
	else
		return eKeyStateType::Unknown;
}

int InputSystem::GetKeyCode(const char* pszKeyName) {
	const KeyCodeMap* pKeyCodeMap = GetKeyCodeMap(m_KeyCodeType);
	if (pKeyCodeMap)
		return pKeyCodeMap->KeyNameToKeyCode(pszKeyName);
	else
		return -1;
}

} // namespace KEP
