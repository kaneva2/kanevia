///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CBoneClass.h"
#include "CBoneAnimationClass.h"
#include "CHierarchyMesh.h"
#include "CFrameObj.h"
#include "ServerSerialize.h"
#ifdef _ClientOutput
#include "..\include\IClientEngine.h"
#include "..\include\CoreStatic\CMovementObj.h"
#endif
#include "matrixarb.h"
#include "Core/Math/Interpolate.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

CBoneObject::CBoneObject() {
	m_boneFrame = NULL;
	m_animationList = NULL;
	m_secondaryOverridable = TRUE;
	m_referencedAnimList = FALSE;

	TimeMs timeMs = fTime::TimeMs();
	m_lastToTargetTimeStamp[0] = timeMs;
	m_lastToTargetTimeStamp[1] = timeMs;
	m_lastToTargetTimeStamp[2] = timeMs;
	m_lastToTargetTimeStamp[3] = timeMs;

	m_lastPositionsAreInitialized[0] = FALSE;
	m_lastPositionsAreInitialized[1] = FALSE;
	m_lastPositionsAreInitialized[2] = FALSE;
	m_lastPositionsAreInitialized[3] = FALSE;

	m_startPosition.MakeIdentity();
	m_startPositionInv.MakeIdentity();
	m_startPositionTranspose.MakeIdentity();

	m_procAnimmat = -1;
}

void CBoneObject::SafeDelete() {
	if (m_boneFrame) {
		m_boneFrame->SafeDelete();
		delete m_boneFrame;
		m_boneFrame = 0;
	}

	for (auto it = m_rigidMeshes.begin(); it != m_rigidMeshes.end(); ++it) {
		CHiarchyMesh* pHMesh = *it;
		pHMesh->SafeDelete();
		delete pHMesh;
	}
	m_rigidMeshes.clear();

	if (!m_referencedAnimList) {
		if (m_animationList) {
			m_animationList->SafeDelete();
			m_animationList = 0;
		}
	}
}

template <typename ListPtrType, typename BonePtrType>
BonePtrType CBoneList::GetBoneObjectByName(ListPtrType list, const std::string& name, int* index, bool convertSpaceToUnderscore) {
	int trace = 0;
	for (POSITION posLoc = list->GetHeadPosition(); posLoc != NULL;) {
		BonePtrType boneObject = static_cast<BonePtrType>(list->GetNext(posLoc));
		*index = trace;
		CStringA tmpName = boneObject->m_boneName.c_str();
		if (convertSpaceToUnderscore) // Convert space to underscore for COLLADA compatibility
			tmpName.Replace(' ', '_');
		std::string tmpNameStr = (const char*)tmpName;
		if (StrSameIgnoreCase(tmpNameStr, name))
			return boneObject;
		trace++;
	}

	return nullptr;
}

CBoneObject* CBoneList::getBoneObjectByName(const std::string& name, int* index, bool convertSpaceToUnderscore) {
	return GetBoneObjectByName<CBoneList*, CBoneObject*>(this, name, index, convertSpaceToUnderscore);
}

const CBoneObject* CBoneList::getBoneObjectByName(const std::string& name, int* index, bool convertSpaceToUnderscore /*= false*/) const {
	return GetBoneObjectByName<const CBoneList*, const CBoneObject*>(this, name, index, convertSpaceToUnderscore);
}

template <typename ListPtrType, typename BonePtrType>
BonePtrType CBoneList::GetBoneByIndex(ListPtrType list, int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = list->FindIndex(index);
	if (!posLoc)
		return NULL;

	return static_cast<BonePtrType>(list->GetAt(posLoc));
}

CBoneObject* CBoneList::getBoneByIndex(int index) {
	return GetBoneByIndex<CBoneList*, CBoneObject*>(this, index);
}

const CBoneObject* CBoneList::getBoneByIndex(int index) const {
	return GetBoneByIndex<const CBoneList*, const CBoneObject*>(this, index);
}

void CBoneObject::Clone(CBoneObject** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	Clone(clone, g_pd3dDevice, true);
}

void CBoneObject::Clone(CBoneObject** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, bool cloneanimdata) {
	CBoneObject* cloneReturn = new CBoneObject();
	cloneReturn->m_boneFrame = new CFrameObj(NULL);
	cloneReturn->m_boneFrame->AddTransformReplace(m_startPosition);
	cloneReturn->m_boneFrame->GetTransform(NULL, &cloneReturn->m_startPosition);

	cloneReturn->m_parentName = m_parentName;
	cloneReturn->m_boneName = m_boneName;

	cloneReturn->m_secondaryOverridable = m_secondaryOverridable;
	cloneReturn->m_startPosition = m_startPosition;
	cloneReturn->m_startPositionInv = m_startPositionInv;
	cloneReturn->m_startPositionTranspose = m_startPositionTranspose;

	cloneReturn->m_procAnimmat = m_procAnimmat;

	//hiarch mesh
	for (auto it = m_rigidMeshes.begin(); it != m_rigidMeshes.end(); ++it) {
		CHierarchyMesh* pClonedHMesh = NULL;
		//		ASSERT(*it);
		if (*it) {
			(*it)->Clone(&pClonedHMesh, g_pd3dDevice);
			cloneReturn->m_rigidMeshes.push_back(pClonedHMesh);
		}
	}

	//animation data
	if (m_animationList) {
		if (cloneanimdata) {
			m_animationList->Clone(&cloneReturn->m_animationList);
			cloneReturn->m_referencedAnimList = FALSE;
		} else {
			cloneReturn->m_animationList = NULL;
			cloneReturn->m_referencedAnimList = FALSE;
		}
	}

	*clone = cloneReturn;
}

void CBoneObject::SetTransformDataForAnimation(Vector3f& lastPosition,
	Vector3f& lastUpVector,
	Vector3f& lastDirVector,
	Vector3f& targetPosition,
	Vector3f& targetUpVector,
	Vector3f& targetDirVector,
	Matrix44f scaleMatrix) {
#ifdef _ClientOutput
	m_lastPositionsAreInitialized[0] = TRUE;
#else
	ASSERT(FALSE);
#endif
}

void CBoneObject::InterpolateKeyFrames(TimeMs elapsedTimeMs, Matrix44fA16* bonePtr, const CBoneAnimationObject* pBAO, BOOL loop) {
#ifdef _ClientOutput
	if (!pBAO)
		return;

	pBAO->InterpolateKeyFrames(elapsedTimeMs, bonePtr, loop != FALSE);
	m_boneFrame->m_frameMatrix = *bonePtr;

	if (pBAO->m_animKeyFrames == 0)
		return; // No frames to animate.

	UINT iCurrFrame = pBAO->GetKeyFrameIndexAtTime(elapsedTimeMs);
	UINT iBeginFrame = 0;
	UINT iEndFrame = pBAO->m_animKeyFrames;
	UINT iNextFrame = iCurrFrame + 1;
	if (iCurrFrame == iEndFrame) {
		// No keyframes defined at this point.
		// Use first or last keyframe depending on whether we're looping.
		if (loop)
			iCurrFrame = iBeginFrame;
		else
			iCurrFrame = iEndFrame - 1;
		iNextFrame = iCurrFrame;
	} else {
		if (iNextFrame == iEndFrame) {
			if (loop)
				iNextFrame = iBeginFrame;
			else
				iNextFrame = iCurrFrame;
		}
	}

	const AnimKeyFrame* pCurrFrame = pBAO->m_pAnimKeyFrames + iCurrFrame;
	const AnimKeyFrame* pNextFrame = pBAO->m_pAnimKeyFrames + iNextFrame;

	// Calculate interpFactor, i.e. the relative position between frames.
	// 0.0 = pCurrFrame, 1.0 = pNextFrame, and 0.5 is midway between them.
	float interpFactor = 0.0f;
	TimeMs frameStart2 = pCurrFrame->m_timeMs;
	TimeMs frameStart1 = ((iCurrFrame == 0) ? 0.0 : (pCurrFrame - 1)->m_timeMs);
	TimeMs frameDiff = frameStart2 - frameStart1;

	if (frameDiff > 0) {
		TimeMs fFrameRemainingTime = frameStart2 - elapsedTimeMs;
		if (fFrameRemainingTime < 0)
			interpFactor = 1.0f;
		else if (fFrameRemainingTime > frameDiff)
			interpFactor = 0.0f;
		else
			interpFactor = 1.0f - fFrameRemainingTime / frameDiff;
	}

	// MJS - Note: This method of interpolating two orientations is incorrect.
	// It will (usually) generate a non-orthogonal matrix since atVector and upVector
	// are not orthogonalized after interpolation. Furthermore, their cross product
	// is used in MatrixARB::SetOrientation without first normalizing it.
	// I recommend storing and interpolating orientation by using quaternions.

	// lerp the animation vectors and form the matrix
	Vector3f posVector, upVector, atVector;
	posVector = InterpolateLinear(pCurrFrame->pos, pNextFrame->pos, interpFactor);
	upVector = InterpolateLinear(pCurrFrame->up, pNextFrame->up, interpFactor);
	atVector = InterpolateLinear(pCurrFrame->at, pNextFrame->at, interpFactor);

	// Normalize orientation vectors to get rid of scaling component introduced by linear interpolation
	// Alternatively, we might use quaternion interpolations in the future.
	atVector.Normalize();
	upVector.Normalize();

	MatrixARB::SetOrientation(bonePtr, atVector, upVector);
	bonePtr->GetTranslation() = posVector;

	m_boneFrame->m_frameMatrix = *bonePtr;
#else
	ASSERT(FALSE);
#endif
}

void CBoneObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		CStringA parentNameStr = m_parentName.c_str();
		CStringA boneNameStr = m_boneName.c_str();
		ar << parentNameStr
		   << m_secondaryOverridable
		   << boneNameStr;
		if (::ServerSerialize)
			ar << 0; // server doesn't need this (5/08)
		else {
			ar << m_rigidMeshes.size();
			for (std::vector<CHierarchyMesh*>::iterator it = m_rigidMeshes.begin(); it != m_rigidMeshes.end(); ++it)
				ar << (*it);
		}

		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar << m_startPosition(loop, loop2);

	} else {
		int version = ar.GetObjectSchema();
		CStringA parentNameStr;
		ar >> parentNameStr;
		m_parentName = (const char*)parentNameStr;

		if (version < 1) {
			ar >> m_animationList;
		}

		CStringA boneNameStr;
		ar >> m_secondaryOverridable >> boneNameStr;
		m_boneName = (const char*)boneNameStr;

		if (version <= 1) {
			CHierarchyMesh* pHMesh = NULL;
			ar >> pHMesh;
			if (pHMesh)
				m_rigidMeshes.push_back(pHMesh);
		} else {
			// version 2+, list of CHierarchyMesh
			int count = 0;
			ar >> count;
			m_rigidMeshes.clear();
			for (int i = 0; i < count; i++) {
				CHierarchyMesh* pHMesh = NULL;
				ar >> pHMesh;
				if (pHMesh)
					m_rigidMeshes.push_back(pHMesh);
			}
		}

		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar >> m_startPosition(loop, loop2);

		m_startPositionInv.MakeInverseOf(m_startPosition);
		MatrixARB::Transpose(m_startPositionTranspose, m_startPosition);

		m_boneFrame = new CFrameObj(NULL);
		m_boneFrame->AddTransformReplace(m_startPosition);

		m_lastToTargetTimeStamp[0] = 0;
		m_lastToTargetTimeStamp[1] = 0;
		m_lastPositionsAreInitialized[0] = FALSE;
		m_lastPositionsAreInitialized[1] = FALSE;

		m_procAnimmat = -1;

		m_referencedAnimList = FALSE;
	}
}

CBoneList::CBoneList() :
		m_boneMats(nullptr) {
}

void CBoneList::BuildBoneList() {
	/* allocate the bone matrices as 16-byte aligned arrays */
	m_boneMats = (Matrix44fA16*)_aligned_malloc(GetCount() * sizeof(Matrix44fA16), 16);

	int i = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL; i++) {
		CBoneObject* p = (CBoneObject*)GetNext(posLoc);

		//	This list duplicates the mFrameMatrix element inside of the
		//	bone frame. We'll keep all the animation data around for now
		//	but we'll be using this list to do math.
		//	What we should be storing, for children and such, are indices
		//	into this array (and not pointers to more bones).
		//	Normally, when you export, a bone knows
		//	which bones are its children and which bone is its parent.
		//
		//	TODO: We'll need to decode these
		//	Eventually (ideally), each skeletonInstance will have this
		//	contiguous array, and a seperate smaller structure that contains
		//	anim data, with indices into this array.
		//	-ShawnM
		m_boneMats[i] = p->m_boneFrame->m_frameMatrix;
	}
}

void CBoneList::DestroyBoneList() {
	if (m_boneMats) {
		_aligned_free(m_boneMats);
		m_boneMats = NULL;
	}
}

void CBoneList::Clone(CBoneList** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, bool cloneanimdata) const {
	CBoneList* newList = new CBoneList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CBoneObject* bnPtr = (CBoneObject*)GetNext(posLoc);
		CBoneObject* newObject = NULL;

		bnPtr->Clone(&newObject, g_pd3dDevice, cloneanimdata);
		newList->AddTail(newObject);
	}

	//	This function will decompose the CBoneObjects that exist in
	//	the list and add them to the array. Eventually, this should
	//	be the only step, and we wont have to do allocations.
	newList->BuildBoneList();

	//	Set master list.
	//	TODO: Init'ing this array should happen in serialize, and
	//	cloning should simply set a pointer, instead of deep
	//	copying all the bones every time we clone a skeleton
	//		m_boneMats		=	newList->m_boneMats;
	///		m_numBones		=	newList->m_numBones;

	*clone = newList;
}

void CBoneList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CBoneObject* bnPtr = (CBoneObject*)GetNext(posLoc);
		bnPtr->SafeDelete();
		delete bnPtr;
		bnPtr = 0;
	}

	//	Clean up bone matrix array
	DestroyBoneList();

	RemoveAll();

	delete this;
}

void CBoneList::computeBoneMatrices(const Matrix44fA16* invBoneMats, const Matrix44fA16* animBoneMats) {
	MatrixARB::MassMultiply(GetCount(), invBoneMats, animBoneMats, m_boneMats);
}

void CBoneList::Serialize(CArchive& ar) {
	CObList::Serialize(ar);
	if (ar.IsStoring()) {
		// version 3: do nothing now
	} else {
		int version = ar.GetObjectSchema();
		int animCount = 0;
		if (version == 1 || version == 2) {
			// read and discard legacy data
			ar >> animCount;
		}
		if (version == 2) {
			// read and discard legacy data
			int* animActors = new int[animCount];
			for (int i = 0; i < animCount; i++) {
				ar >> animActors[i];
			}

			delete[] animActors;
		}
	}
}

CHierarchyMesh* CBoneObject::GetRigidMeshByName(const CStringA& meshName) const {
	for (auto it = m_rigidMeshes.begin(); it != m_rigidMeshes.end(); ++it) {
		if ((*it)->m_name == meshName)
			return *it;
	}

	return NULL;
}

void CBoneObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_animationList);
		pMemSizeGadget->AddObject(m_boneFrame);
		pMemSizeGadget->AddObject(m_boneName);
		pMemSizeGadget->AddObject(m_parentName);
		pMemSizeGadget->AddObject(m_rigidMeshes);
	}
}

void CBoneList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CBoneObject* pBoneObject = static_cast<const CBoneObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pBoneObject);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CBoneObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CBoneList, CObject)

BEGIN_GETSET_IMPL(CBoneObject, IDS_BONEOBJECT_NAME)
GETSET_MAX(m_parentName, gsString, GS_FLAGS_DEFAULT, 0, 0, BONEOBJECT, PARENTNAME, STRLEN_MAX)
GETSET_OBLIST_PTR(m_animationList, GS_FLAGS_DEFAULT, 0, 0, BONEOBJECT, ANIMATIONLIST)
GETSET_RANGE(m_secondaryOverridable, gsInt, GS_FLAGS_DEFAULT, 0, 0, BONEOBJECT, SECONDARYOVERRIDABLE, INT_MIN, INT_MAX)
GETSET_MAX(m_boneName, gsString, GS_FLAGS_DEFAULT, 0, 0, BONEOBJECT, BONENAME, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP
