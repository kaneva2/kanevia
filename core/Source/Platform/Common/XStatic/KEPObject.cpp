///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KEPObject.h"

namespace KEP {

CKEPObject::CKEPObject(void) {
	m_lastSavedPath = "";
	m_dirty = FALSE;
}

CKEPObject::~CKEPObject(void) {
}

void CKEPObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	m_lastSavedPath = Utf16ToUtf8(ar.GetFile()->GetFilePath().GetString()).c_str();
	m_dirty = FALSE;
}

IMPLEMENT_SERIAL(CKEPObject, CObject, 0)

} // namespace KEP