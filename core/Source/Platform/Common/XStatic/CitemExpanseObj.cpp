///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CitemExpanseObj.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

CExpansiveObj::CExpansiveObj() {
	m_iconIndex = 0;
	m_xPos = 0.0f;
	m_yPos = 0.0f;
	m_miscInt = 0;
	m_miscInt2 = 0;
}

void CExpansiveObj::Clone(CExpansiveObj** clone) {
	CExpansiveObj* newCopy = new CExpansiveObj();

	newCopy->m_iconIndex = m_iconIndex;
	newCopy->m_xPos = m_xPos;
	newCopy->m_yPos = m_yPos;
	newCopy->m_miscInt = m_miscInt;
	newCopy->m_miscInt2 = m_miscInt2;

	*clone = newCopy;
}

void CExpansiveObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_iconIndex
		   << m_xPos
		   << m_yPos
		   << m_miscInt
		   << m_miscInt2;
	} else {
		ar >> m_iconIndex >> m_xPos >> m_yPos >> m_miscInt >> m_miscInt2;
	}
}

//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void CCExpansiveObjList::Clone(CCExpansiveObjList** clone) {
	CCExpansiveObjList* newList = new CCExpansiveObjList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CExpansiveObj* bnPtr = (CExpansiveObj*)GetNext(posLoc);
		CExpansiveObj* newObject = NULL;
		bnPtr->Clone(&newObject);
		newList->AddTail(newObject);
	}
	*clone = newList;
}
//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
CExpansiveObj* CCExpansiveObjList::GetByMisc2(int miscInt2) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CExpansiveObj* spnPtr = (CExpansiveObj*)GetNext(posLoc);
		if (spnPtr->m_miscInt2 == miscInt2) {
			return spnPtr;
		}
	}
	return NULL;
}
//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void CCExpansiveObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CExpansiveObj* spnPtr = (CExpansiveObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}
//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void CExpansiveObj::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_iconIndex
		   << m_xPos
		   << m_yPos
		   << m_miscInt
		   << m_miscInt2;
	} else {
		ar >> m_iconIndex >> m_xPos >> m_yPos >> m_miscInt >> m_miscInt2;
	}
}
//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void CCExpansiveObjList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CExpansiveObj* elementPtr = (CExpansiveObj*)GetNext(posLoc);
			elementPtr->SerializeAlloc(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			CExpansiveObj* elementPtr = new CExpansiveObj();
			elementPtr->SerializeAlloc(ar);
			AddTail(elementPtr);
		}
	}
}

void CExpansiveObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);
	}
}

void CCExpansiveObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CExpansiveObj* pCExpansiveObj = static_cast<const CExpansiveObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCExpansiveObj);
		}
	}
}

IMPLEMENT_SERIAL(CExpansiveObj, CObject, 0)
IMPLEMENT_SERIAL(CCExpansiveObjList, CObList, 0)

BEGIN_GETSET_IMPL(CExpansiveObj, IDS_EXPANSIVEOBJ_NAME)
GETSET_RANGE(m_iconIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, EXPANSIVEOBJ, ICONINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_xPos, gsFloat, GS_FLAGS_DEFAULT, 0, 0, EXPANSIVEOBJ, XPOS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_yPos, gsFloat, GS_FLAGS_DEFAULT, 0, 0, EXPANSIVEOBJ, YPOS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_miscInt, gsInt, GS_FLAGS_DEFAULT, 0, 0, EXPANSIVEOBJ, MISCINT, INT_MIN, INT_MAX)
GETSET_RANGE(m_miscInt2, gsInt, GS_FLAGS_DEFAULT, 0, 0, EXPANSIVEOBJ, MISCINT2, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP