///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS

#include "CBoneAnimationClass.h"

#include "resource.h"
#include "ClientStrings.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "d3dx9math.h"
#include "matrixarb.h"
#include "matrixarb.h"
#include "Utils/ReUtils.h"
#include <math.h>
#include <SerializeHelper.h>
#include "CSSoundRange.h"
#include "UnicodeMFCHelpers.h"
#include "Core/Math/Interpolate.h"
#include "Common/include/IMemSizeGadget.h"

namespace KEP {

CBoneAnimationObject::CBoneAnimationObject() {
	Init();
}

void CBoneAnimationObject::Init() {
	m_animKeyFrames = 0;
	m_animFrameTimeMs = 0.0;
	m_pAnimKeyFrames = nullptr;
	m_animLooping = TRUE;
	m_animMinTimeMs = 0.0;
	m_deleteKeyFrames = TRUE;
	m_animDurationMs = 0.0;
	m_compressed = FALSE;
}

void CBoneAnimationObject::SafeDelete() {
	if (m_deleteKeyFrames && m_pAnimKeyFrames) {
		delete[] m_pAnimKeyFrames;
		m_pAnimKeyFrames = nullptr;
	}
}

void CBoneAnimationObject::Clone(CBoneAnimationObject** ppBAO_out) const {
	CBoneAnimationObject* pBAO_new = new CBoneAnimationObject();
	pBAO_new->m_animKeyFrames = m_animKeyFrames;
	pBAO_new->m_animFrameTimeMs = m_animFrameTimeMs;
	pBAO_new->m_animLooping = m_animLooping;
	pBAO_new->m_animMinTimeMs = m_animMinTimeMs;
	pBAO_new->m_animDurationMs = m_animDurationMs;
	pBAO_new->m_compressed = m_compressed;
	if (m_pAnimKeyFrames) {
		pBAO_new->m_deleteKeyFrames = TRUE;
		pBAO_new->m_pAnimKeyFrames = new AnimKeyFrame[m_animKeyFrames];
		CopyMemory(pBAO_new->m_pAnimKeyFrames, m_pAnimKeyFrames, (sizeof(AnimKeyFrame) * m_animKeyFrames));
	}

	*ppBAO_out = pBAO_new;
}

bool CBoneAnimationObject::GetBoneMatrixByTime(TimeMs animTimeMs, Matrix44f& matrix) const {
	for (int frmLoop = 0; frmLoop < m_animKeyFrames; frmLoop++) {
		AnimKeyFrame& frame = m_pAnimKeyFrames[frmLoop];
		if (frame.m_timeMs >= animTimeMs) {
			matrix.SetRow(1, frame.up, 0);
			matrix.SetRow(2, frame.at, 0);
			matrix.SetRow(0, Cross(frame.up, frame.at), 0);
			matrix.SetRow(3, frame.pos, 1);
			return true;
		}
	}

	return false;
}

size_t CBoneAnimationObject::GetKeyFrameIndexAtTime(TimeMs timeMs) const {
	struct Comparator {
		bool operator()(const AnimKeyFrame& left, int right) const {
			return left.m_timeMs < right;
		}
		bool operator()(int left, const AnimKeyFrame& right) const {
			return left < right.m_timeMs;
		}
	};

	const AnimKeyFrame* pBeginFrame = m_pAnimKeyFrames;
	const AnimKeyFrame* pEndFrame = m_pAnimKeyFrames + m_animKeyFrames;

	return std::lower_bound(pBeginFrame, pEndFrame, static_cast<int>(timeMs), Comparator()) - pBeginFrame;
}

void CBoneAnimationObject::InterpolateKeyFrames(TimeMs elapsedTimeMs, Matrix44f* bonePtr, bool loopedAnimation) const {
#ifdef _ClientOutput
	if (m_animKeyFrames == 0)
		return; // No frames to animate.

	UINT iCurrFrame = GetKeyFrameIndexAtTime(elapsedTimeMs);

	UINT iBeginFrame = 0;
	UINT iEndFrame = m_animKeyFrames;
	UINT iNextFrame = iCurrFrame + 1;
	if (iCurrFrame == iEndFrame) {
		// No keyframes defined at this point.
		// Use first or last keyframe depending on whether we're looping.
		if (loopedAnimation)
			iCurrFrame = iBeginFrame;
		else
			iCurrFrame = iEndFrame - 1;
		iNextFrame = iCurrFrame;
	} else {
		if (iNextFrame == iEndFrame) {
			if (loopedAnimation)
				iNextFrame = iBeginFrame;
			else
				iNextFrame = iCurrFrame;
		}
	}

	const AnimKeyFrame* pCurrFrame = m_pAnimKeyFrames + iCurrFrame;
	const AnimKeyFrame* pNextFrame = m_pAnimKeyFrames + iNextFrame;

	// Calculate interpFactor, i.e. the relative position between frames.
	// 0.0 = pCurrFrame, 1.0 = pNextFrame, and 0.5 is midway between them.
	float interpFactor = 0;
	TimeMs frameStart2 = pCurrFrame->m_timeMs;
	TimeMs frameStart1 = ((iCurrFrame == 0) ? 0.0 : (pCurrFrame - 1)->m_timeMs);
	TimeMs frameDiff = frameStart2 - frameStart1;

	if (frameDiff > 0) {
		TimeMs fFrameRemainingTime = frameStart2 - elapsedTimeMs;
		if (fFrameRemainingTime < 0)
			interpFactor = 1.0f;
		else if (fFrameRemainingTime > frameDiff)
			interpFactor = 0.0f;
		else
			interpFactor = 1.0f - fFrameRemainingTime / frameDiff;
	}

	// MJS - Note: This method of interpolating two orientations is incorrect.
	// It will (usually) generate a non-orthogonal matrix since atVector and upVector
	// are not orthogonalized after interpolation. Furthermore, their cross product
	// is used in MatrixARB::SetOrientation without first normalizing it.
	// I recommend storing and interpolating orientation by using quaternions.

	// lerp the animation vectors and form the matrix
	Vector3f posVector, upVector, atVector;
	posVector = InterpolateLinear(pCurrFrame->pos, pNextFrame->pos, interpFactor);
	upVector = InterpolateLinear(pCurrFrame->up, pNextFrame->up, interpFactor);
	atVector = InterpolateLinear(pCurrFrame->at, pNextFrame->at, interpFactor);

	// Normalize orientation vectors to get rid of scaling component introduced by linear interpolation
	// Alternatively, we might use quaternion interpolations in the future.
	atVector.Normalize();
	upVector.Normalize();

	MatrixARB::SetOrientation(bonePtr, atVector, upVector);
	bonePtr->GetTranslation() = posVector;
#else
	ASSERT(FALSE);
#endif
}

void CBoneAnimationObject::RegenerateKeyFrames(FLOAT angTol, FLOAT posTol, UINT maxSkipped) {
	if (m_animKeyFrames <= maxSkipped)
		return;

	// temp animatiion data array to hold interpolated keyframes
	AnimKeyFrame* pKeyFrames_new = new AnimKeyFrame[m_animKeyFrames];

	// interpolated keyframe count
	size_t numKeyframes = 0;

	// prime the interpData array
	pKeyFrames_new[numKeyframes++] = m_pAnimKeyFrames[0];

	// go thru original keyframe list and interpolate as necessary by "tol"
	for (size_t i = 1; i < m_animKeyFrames; ++i) {
		Vector3f v0 = pKeyFrames_new[numKeyframes - 1].up;
		v0 -= m_pAnimKeyFrames[i].up;
		FLOAT error0 = v0.Length();
		;

		Vector3f v1 = pKeyFrames_new[numKeyframes - 1].at;
		v1 -= m_pAnimKeyFrames[i].at;
		FLOAT error1 = v1.Length();

		Vector3f v2 = pKeyFrames_new[numKeyframes - 1].pos;
		v2 -= m_pAnimKeyFrames[i].pos;
		FLOAT error2 = v2.Length();

		// end keyframe for interpolation....always in original animation data array
		if (error0 > angTol || error1 > angTol || posTol >= 0 && error2 > posTol || (i % maxSkipped) == 0 || i == m_animKeyFrames - 1) {
			// use the time of the last skipped frame
			pKeyFrames_new[numKeyframes - 1].m_timeMs = m_pAnimKeyFrames[i - 1].m_timeMs;
			// insert another keyframe
			pKeyFrames_new[numKeyframes++] = m_pAnimKeyFrames[i];
		}
	}

	// replace the original keyframe list with the interpolated keyframe list
	for (UINT i = 0; i < numKeyframes; i++)
		m_pAnimKeyFrames[i] = pKeyFrames_new[i];

	m_animKeyFrames = numKeyframes;

	delete[] pKeyFrames_new;
}

void CBoneAnimationObject::Serialize(CArchive& ar) {
	int reserved = 0;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_animKeyFrames
		   << (int)m_animFrameTimeMs
		   << m_animLooping
		   << (long)0 //m_timingCapMax
		   << (long)0 //m_timingCapMin
		   << (BOOL)FALSE //m_timingBased
		   << CStringA("") //m_animName
		   << (int)0 //m_animType
		   << (int)m_animMinTimeMs
		   << (int)0 //m_animVer
		   << (UINT)m_animDurationMs
		   << (UINT)0 //m_hashCode
		   << m_compressed;

		if (m_compressed) {
			// create temp mem block for data compression
			AnimationContentStreamedV4* memBlock = new AnimationContentStreamedV4[m_animKeyFrames];
			// compress & write the data
			for (int i = 0; i < m_animKeyFrames; i++) {
#define CLAMPED_SHORT(x) ((SHORT)(x > SHORT_MAX ? SHORT_MAX : (x < -SHORT_MAX ? -SHORT_MAX : x)));

				int temp;
				temp = m_pAnimKeyFrames[i].up.X() * SHORT_MAX + .5f;
				memBlock[i].up[0] = CLAMPED_SHORT(temp);
				temp = m_pAnimKeyFrames[i].up.Y() * SHORT_MAX + .5f;
				memBlock[i].up[1] = CLAMPED_SHORT(temp);
				temp = m_pAnimKeyFrames[i].up.Z() * SHORT_MAX + .5f;
				memBlock[i].up[2] = CLAMPED_SHORT(temp);
				temp = m_pAnimKeyFrames[i].at.X() * SHORT_MAX + .5f;
				memBlock[i].at[0] = CLAMPED_SHORT(temp);
				temp = m_pAnimKeyFrames[i].at.Y() * SHORT_MAX + .5f;
				memBlock[i].at[1] = CLAMPED_SHORT(temp);
				temp = m_pAnimKeyFrames[i].at.Z() * SHORT_MAX + .5f;
				memBlock[i].at[2] = CLAMPED_SHORT(temp);
				memBlock[i].pos[0] = m_pAnimKeyFrames[i].pos.X();
				memBlock[i].pos[1] = m_pAnimKeyFrames[i].pos.Y();
				memBlock[i].pos[2] = m_pAnimKeyFrames[i].pos.Z();
				memBlock[i].time = (USHORT)m_pAnimKeyFrames[i].m_timeMs;
			}
			// write compressed data block to archive
			ar.Write((void*)memBlock, m_animKeyFrames * sizeof(AnimationContentStreamedV4));
			delete[] memBlock;
		} else {
			// create temp mem block
			AnimationContentUStreamed* memBlock = new AnimationContentUStreamed[m_animKeyFrames];
			// write the data
			for (int i = 0; i < m_animKeyFrames; i++) {
				memBlock[i].up[0] = (FLOAT)m_pAnimKeyFrames[i].up.X();
				memBlock[i].up[1] = (FLOAT)m_pAnimKeyFrames[i].up.Y();
				memBlock[i].up[2] = (FLOAT)m_pAnimKeyFrames[i].up.Z();
				memBlock[i].at[0] = (FLOAT)m_pAnimKeyFrames[i].at.X();
				memBlock[i].at[1] = (FLOAT)m_pAnimKeyFrames[i].at.Y();
				memBlock[i].at[2] = (FLOAT)m_pAnimKeyFrames[i].at.Z();
				memBlock[i].pos[0] = (FLOAT)m_pAnimKeyFrames[i].pos.X();
				memBlock[i].pos[1] = (FLOAT)m_pAnimKeyFrames[i].pos.Y();
				memBlock[i].pos[2] = (FLOAT)m_pAnimKeyFrames[i].pos.Z();
				memBlock[i].time = (FLOAT)m_pAnimKeyFrames[i].m_timeMs;
			}
			// write data block to archive
			ar.Write((void*)memBlock, m_animKeyFrames * sizeof(AnimationContentUStreamed));
			delete[] memBlock;
		}
	} else {
		int version = ar.GetObjectSchema();

		BOOL depBOOL;
		long depLong;
		int animType;
		int animVer;
		CStringA depStr;
		if (version <= 2) {
			CSoundRangeObjectList* soundRanges_Deprecated = nullptr;

			int msPerFrame = 0;
			int minPlay = 0;
			ar >> m_animKeyFrames >> reserved >> msPerFrame >> m_animLooping >> depLong //m_timingCapMax
				>> depLong //m_timingCapMin
				>> depBOOL //m_timingBased
				>> depStr //m_animName
				>> animType >> soundRanges_Deprecated >> reserved >> reserved >> reserved >> reserved >> minPlay >> animVer; //m_animVer;

			if (soundRanges_Deprecated != nullptr) {
				soundRanges_Deprecated->SafeDelete();
				delete soundRanges_Deprecated;
			}

			m_animMinTimeMs = (TimeMs)minPlay;

			// exporter is incorrectly reporting 30fps as 80 milliseconds per frame
			m_animFrameTimeMs = (TimeMs)msPerFrame * 33.0 / 80.0;

			m_compressed = FALSE;
		} else if (version >= 3) {
			UINT animDurationMs = 0;
			int msPerFrame = 0;
			int minPlay = 0;
			UINT hashCode;
			ar >> m_animKeyFrames >> msPerFrame >> m_animLooping >> depLong //m_timingCapMax
				>> depLong //m_timingCapMin
				>> depBOOL //m_timingBased
				>> depStr //m_animName
				>> animType >> minPlay >> animVer //m_animVer
				>> animDurationMs >> hashCode //m_hashCode
				>> m_compressed;
			m_animDurationMs = (TimeMs)animDurationMs;
			m_animFrameTimeMs = (TimeMs)msPerFrame;
			m_animMinTimeMs = (TimeMs)minPlay;
		}

		if (m_animKeyFrames > 0) {
			m_pAnimKeyFrames = new AnimKeyFrame[m_animKeyFrames];
		}

		if (version < 2) {
			float temp;
			for (int loop = 0; loop < m_animKeyFrames; loop++) {
				ar >> temp;
				m_pAnimKeyFrames[loop].up.X() = temp;
				ar >> temp;
				m_pAnimKeyFrames[loop].up.Y() = temp;
				ar >> temp;
				m_pAnimKeyFrames[loop].up.Z() = temp;
				ar >> temp;
				m_pAnimKeyFrames[loop].at.X() = temp;
				ar >> temp;
				m_pAnimKeyFrames[loop].at.Y() = temp;
				ar >> temp;
				m_pAnimKeyFrames[loop].at.Z() = temp;
				ar >> m_pAnimKeyFrames[loop].pos.X();
				ar >> m_pAnimKeyFrames[loop].pos.Y();
				ar >> m_pAnimKeyFrames[loop].pos.Z();
			}
		} else if (version == 2) {
			assert(m_animKeyFrames < 262144 / 9);

			SHORT* buf = new SHORT[262144];

			assert(buf);

			SHORT tempS;
			FLOAT tempF;
			UINT count = 0;
			for (int i = 0; i < m_animKeyFrames; i++) {
				ar >> tempS;
				buf[count++] = tempS;
				m_pAnimKeyFrames[i].up.X() = tempS * .0001f;
				ar >> tempS;
				buf[count++] = tempS;
				m_pAnimKeyFrames[i].up.Y() = tempS * .0001f;
				ar >> tempS;
				buf[count++] = tempS;
				m_pAnimKeyFrames[i].up.Z() = tempS * .0001f;
				ar >> tempS;
				buf[count++] = tempS;
				m_pAnimKeyFrames[i].at.X() = tempS * .0001f;
				ar >> tempS;
				buf[count++] = tempS;
				m_pAnimKeyFrames[i].at.Y() = tempS * .0001f;
				ar >> tempS;
				buf[count++] = tempS;
				m_pAnimKeyFrames[i].at.Z() = tempS * .0001f;
				ar >> tempF;
				buf[count++] = tempF;
				m_pAnimKeyFrames[i].pos.X() = tempF;
				ar >> tempF;
				buf[count++] = tempF;
				m_pAnimKeyFrames[i].pos.Y() = tempF;
				ar >> tempF;
				buf[count++] = tempF;
				m_pAnimKeyFrames[i].pos.Z() = tempF;
			}

			delete[] buf;
		} else if (version >= 3) {
			if (m_compressed) {
				if (version == 3) {
					//v3 -- decode compressed animation data
					// create temp mem block for data decompression
					AnimationContentStreamedV3* memBlock = new AnimationContentStreamedV3[m_animKeyFrames];

					if (memBlock) {
						// read compressed data block from archive
						ar.Read((void*)memBlock, m_animKeyFrames * sizeof(AnimationContentStreamedV3));

						// compress the data
						for (int i = 0; i < m_animKeyFrames; i++) {
							m_pAnimKeyFrames[i].up.X() = memBlock[i].up[0] * .00006103515625f;
							m_pAnimKeyFrames[i].up.Y() = memBlock[i].up[1] * .00006103515625f;
							m_pAnimKeyFrames[i].up.Z() = memBlock[i].up[2] * .00006103515625f;
							m_pAnimKeyFrames[i].at.X() = memBlock[i].at[0] * .00006103515625f;
							m_pAnimKeyFrames[i].at.Y() = memBlock[i].at[1] * .00006103515625f;
							m_pAnimKeyFrames[i].at.Z() = memBlock[i].at[2] * .00006103515625f;
							m_pAnimKeyFrames[i].pos.X() = memBlock[i].pos[0] * .0009765625f;
							m_pAnimKeyFrames[i].pos.Y() = memBlock[i].pos[1] * .0009765625f;
							m_pAnimKeyFrames[i].pos.Z() = memBlock[i].pos[2] * .0009765625f;
							m_pAnimKeyFrames[i].m_timeMs = (TimeMs)memBlock[i].time;
						}

						delete[] memBlock;
					}
				} else {
					//V4 and later -- read uncompressed pos to preserve precision (up and at vectors are still compressed)
					// create temp mem block for data decompression
					AnimationContentStreamedV4* memBlock = new AnimationContentStreamedV4[m_animKeyFrames];

					if (memBlock) {
						// read compressed data block from archive
						ar.Read((void*)memBlock, m_animKeyFrames * sizeof(AnimationContentStreamedV4));

						// compress the data
						for (int i = 0; i < m_animKeyFrames; i++) {
							Vector3f temp;
							temp.X() = memBlock[i].up[0] * 1.0f / SHORT_MAX;
							temp.Y() = memBlock[i].up[1] * 1.0f / SHORT_MAX;
							temp.Z() = memBlock[i].up[2] * 1.0f / SHORT_MAX;
							m_pAnimKeyFrames[i].up = temp.GetNormalized();
							temp.X() = memBlock[i].at[0] * 1.0f / SHORT_MAX;
							temp.Y() = memBlock[i].at[1] * 1.0f / SHORT_MAX;
							temp.Z() = memBlock[i].at[2] * 1.0f / SHORT_MAX;
							m_pAnimKeyFrames[i].at = temp.GetNormalized();
							m_pAnimKeyFrames[i].pos.X() = memBlock[i].pos[0];
							m_pAnimKeyFrames[i].pos.Y() = memBlock[i].pos[1];
							m_pAnimKeyFrames[i].pos.Z() = memBlock[i].pos[2];
							m_pAnimKeyFrames[i].m_timeMs = (TimeMs)memBlock[i].time;
						}

						delete[] memBlock;
					}
				}
			} else {
				// create temp mem block
				AnimationContentUStreamed* memBlock = new AnimationContentUStreamed[m_animKeyFrames];

				if (memBlock) {
					// read data block from archive
					ar.Read((void*)memBlock, m_animKeyFrames * sizeof(AnimationContentUStreamed));

					for (int i = 0; i < m_animKeyFrames; i++) {
						m_pAnimKeyFrames[i].up.X() = memBlock[i].up[0];
						m_pAnimKeyFrames[i].up.Y() = memBlock[i].up[1];
						m_pAnimKeyFrames[i].up.Z() = memBlock[i].up[2];
						m_pAnimKeyFrames[i].at.X() = memBlock[i].at[0];
						m_pAnimKeyFrames[i].at.Y() = memBlock[i].at[1];
						m_pAnimKeyFrames[i].at.Z() = memBlock[i].at[2];
						m_pAnimKeyFrames[i].pos.X() = memBlock[i].pos[0];
						m_pAnimKeyFrames[i].pos.Y() = memBlock[i].pos[1];
						m_pAnimKeyFrames[i].pos.Z() = memBlock[i].pos[2];
						m_pAnimKeyFrames[i].m_timeMs = (TimeMs)memBlock[i].time;
					}

					delete[] memBlock;
				}
			}
		}

		m_deleteKeyFrames = TRUE;

		// upgrade to version 3 keyframes with variable time slices....use old default for version 2
		if (version <= 2) {
			TimeMs frameLength = m_animFrameTimeMs;
			TimeMs frameTime = 0.0;
			for (UINT i = 0; i < (UINT)m_animKeyFrames; i++) {
				frameTime += frameLength;
				m_pAnimKeyFrames[i].m_timeMs = frameTime;
			}
			m_animDurationMs = frameTime;
		}
	}
}

CBoneAnimationList::CBoneAnimationList() :
		m_cloneCnt(1) {}

void CBoneAnimationList::Clone(CBoneAnimationList** clone) {
	m_cloneCnt++;
	*clone = this;
}

void CBoneAnimationList::SafeDelete() {
	m_cloneCnt--;

	if (m_cloneCnt > 0)
		return;

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pBAO = (CBoneAnimationObject*)GetNext(posLoc);
		if (pBAO) {
			pBAO->SafeDelete();
			delete pBAO;
		}
		pBAO = 0;
	}
	RemoveAll();
	delete this;
}

void AnimKeyFrame::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CBoneAnimationObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_pAnimKeyFrames);
	}
}

void CBoneAnimationList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CBoneAnimationObject* pBoneAnimationObject = static_cast<const CBoneAnimationObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pBoneAnimationObject);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CBoneAnimationObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CBoneAnimationList, CObject)

} // namespace KEP
