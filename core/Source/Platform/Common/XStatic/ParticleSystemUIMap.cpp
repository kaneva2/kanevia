///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ParticleSystemUIMap.h"
#include "EnvParticleSystem.h"

namespace KEP {

ListItemMap CParticleSystemUIMap::GetParticles() {
	ListItemMap particleMap;

	if (m_particleSystem) {
		for (UINT psLoop = 0; psLoop < m_particleSystem->GetNumberOfStockItems(); psLoop++) {
			EnvParticleSystem* particle = m_particleSystem->GetStockItemByIndex(psLoop);
			particleMap.insert(ListItemMap::value_type(psLoop, particle->m_systemName.c_str()));
		}
	}
	return particleMap;
}

} // namespace KEP