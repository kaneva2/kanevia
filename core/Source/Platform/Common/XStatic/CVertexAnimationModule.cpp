///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CVertexAnimationModule.h"
#include "ReMesh.h"

#include "matrixarb.h"
#include "CStructuresMisl.h"
#include "CExMeshClass.h"
#include <log4cplus/logger.h>
#include "UnicodeMFCHelpers.h"
#include "common/include/IMemSizeGadget.h"

using namespace log4cplus;

namespace KEP {

Logger errorLogger(Logger::getInstance(L"ClientEngine"));

CVertexAnimationModule::CVertexAnimationModule() {
	m_vertexFrames = NULL;
	m_frameCount = 0;
	m_curFrame = 0;
	m_targetFrame = 1;
	m_timeBetweenFrames = 1000;
	m_looping = TRUE;
	m_currentFrameTime = 0;
	m_lastUpdatedFrameChngTime = fTime::TimeMs();
}

void CVertexAnimationModule::SafeDelete() {
	for (int i = 0; i < m_frameCount; i++) {
		if (m_vertexFrames[i].m_vertexs)
			delete[] m_vertexFrames[i].m_vertexs;
	}
	if (m_vertexFrames) {
		delete[] m_vertexFrames;
		m_vertexFrames = 0;
	}

	m_frameCount = 0;
	m_curFrame = 0;
	m_targetFrame = 1;
}

bool CVertexAnimationModule::checkNewFrameVertexCount(const std::string& newMeshName, int newVtxCount) {
	if (m_frameCount > 0 &&
		m_vertexFrames[0].m_vertexCount != newVtxCount) {
		char errorMsgTemplate[] = { "Unable to add mesh \"%s\" to animation because it has different number of vertices: %d verts. needed; %d verts. provided" };
		char errorMsg[2048];
		_snprintf_s(errorMsg, _countof(errorMsg), _TRUNCATE, errorMsgTemplate, newMeshName, m_vertexFrames[0].m_vertexCount, newVtxCount);
		AfxMessageBoxUtf8(errorMsg);
		return false;
	}

	return true;
}

bool CVertexAnimationModule::AddKeyFrame(CEXMeshObj& aMesh) {
	CAdvancedVertexObj* vertexData = &(aMesh.m_abVertexArray);

	if (!checkNewFrameVertexCount((const char*)aMesh.m_fileName, vertexData->m_vertexCount))
		return false;

	ABVERTEX0L* vtxArray = new ABVERTEX0L[vertexData->m_vertexCount];
	vertexData->Fill0UVArrayData(vtxArray);

	return AddKeyFrame(vtxArray, vertexData->m_vertexCount);
}

bool CVertexAnimationModule::AddKeyFrame(ABVERTEX0L* vertices, int vtxCount) {
	if (!checkNewFrameVertexCount("", vtxCount))
		return false;

	VertexPositionsFrame* m_newFrames = new VertexPositionsFrame[m_frameCount + 1];

	int count;
	for (count = 0; count < m_frameCount; count++) {
		m_newFrames[count].m_vertexCount = m_vertexFrames[count].m_vertexCount;
		m_newFrames[count].m_vertexs = m_vertexFrames[count].m_vertexs;
	}
	m_newFrames[count].m_vertexCount = vtxCount;
	m_newFrames[count].m_vertexs = vertices; // pointer copied directly, do not dispose memory at caller

	if (m_vertexFrames)
		delete[] m_vertexFrames;
	m_vertexFrames = m_newFrames;

	m_frameCount++;
	return true;
}

void CVertexAnimationModule::ApplyAnimation(CEXMeshObj& aMesh, LPDIRECT3DDEVICE9 pd3dDevice) {
	CAdvancedVertexObj* vertexData = &(aMesh.m_abVertexArray);

	if (m_frameCount == 0)
		return;

	if (m_vertexFrames[0].m_vertexCount != vertexData->m_vertexCount) {
		static int i = 0;
		if ((i++ % 10000) == 0) // otherwise floods the log
			LOG4CPLUS_DEBUG(errorLogger, "VertexAnimationModule: the mesh has a number of vertices different from that in the animation frames. Skipping the animation.");
		return;
	}

	TimeMs timeDiff = fTime::ElapsedMs(m_lastUpdatedFrameChngTime);
	if (timeDiff > m_timeBetweenFrames) {
		timeDiff = 0;
		m_curFrame = m_targetFrame;
		m_targetFrame++;
		m_lastUpdatedFrameChngTime = fTime::TimeMs();
		if (m_targetFrame >= m_frameCount) {
			m_targetFrame = 0;
		}
	}

	for (int vCount = 0; vCount < m_vertexFrames[m_curFrame].m_vertexCount; vCount++) {
		vertexData->SetPositionXYZNXNYNZMEM(vCount,
			MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[vCount].x, m_vertexFrames[m_targetFrame].m_vertexs[vCount].x, m_timeBetweenFrames, timeDiff),
			MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[vCount].y, m_vertexFrames[m_targetFrame].m_vertexs[vCount].y, m_timeBetweenFrames, timeDiff),
			MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[vCount].z, m_vertexFrames[m_targetFrame].m_vertexs[vCount].z, m_timeBetweenFrames, timeDiff),
			MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[vCount].nx, m_vertexFrames[m_targetFrame].m_vertexs[vCount].nx, m_timeBetweenFrames, timeDiff),
			MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[vCount].ny, m_vertexFrames[m_targetFrame].m_vertexs[vCount].ny, m_timeBetweenFrames, timeDiff),
			MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[vCount].nz, m_vertexFrames[m_targetFrame].m_vertexs[vCount].nz, m_timeBetweenFrames, timeDiff));
	}

	//vertexData->ReinitializeDxVbuffersPositionOnly(pd3dDevice);
	vertexData->ReinitializeDxVbuffersPosNormOnly(pd3dDevice);
}

void CVertexAnimationModule::ApplyAnimation(ReMesh* mesh, LPDIRECT3DDEVICE9 pd3dDevice) {
	if (m_frameCount == 0) {
		return;
	}

	ReMeshData* pData = NULL;

	// get the mesh array data
	mesh->Access(&pData);

	if (pData->NumV != m_vertexFrames[0].m_vertexCount) {
		static int i = 0;
		if ((i++ % 10000) == 0) { // otherwise floods the log
			LOG4CPLUS_DEBUG(errorLogger, "VertexAnimationModule: the mesh has a number of vertices different from that in the animation frames. Skipping the animation.");
		}
		return;
	}

	TimeMs timeDiff = fTime::ElapsedMs(m_lastUpdatedFrameChngTime);
	if (timeDiff > m_timeBetweenFrames) {
		timeDiff = 0;
		m_curFrame = m_targetFrame;
		m_targetFrame++;
		m_lastUpdatedFrameChngTime = fTime::TimeMs();

		if (m_targetFrame >= m_frameCount) {
			m_targetFrame = 0;
		}
	}

	for (int i = 0; i < m_vertexFrames[m_curFrame].m_vertexCount; i++) {
		pData->pP[i].x = MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[i].x, m_vertexFrames[m_targetFrame].m_vertexs[i].x, m_timeBetweenFrames, timeDiff);
		pData->pP[i].y = MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[i].y, m_vertexFrames[m_targetFrame].m_vertexs[i].y, m_timeBetweenFrames, timeDiff);
		pData->pP[i].z = MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[i].z, m_vertexFrames[m_targetFrame].m_vertexs[i].z, m_timeBetweenFrames, timeDiff);
		pData->pN[i].x = MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[i].nx, m_vertexFrames[m_targetFrame].m_vertexs[i].nx, m_timeBetweenFrames, timeDiff);
		pData->pN[i].y = MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[i].ny, m_vertexFrames[m_targetFrame].m_vertexs[i].ny, m_timeBetweenFrames, timeDiff);
		pData->pN[i].z = MatrixARB::InterpolateFloats(m_vertexFrames[m_curFrame].m_vertexs[i].nz, m_vertexFrames[m_targetFrame].m_vertexs[i].nz, m_timeBetweenFrames, timeDiff);
	}

	mesh->Reload();
}

void CVertexAnimationModule::Clone(CVertexAnimationModule** clone) {
	CVertexAnimationModule* copyLocal = new CVertexAnimationModule();

	copyLocal->m_frameCount = m_frameCount;
	copyLocal->m_curFrame = m_curFrame;
	copyLocal->m_targetFrame = m_targetFrame;
	copyLocal->m_timeBetweenFrames = m_timeBetweenFrames;
	copyLocal->m_looping = m_looping;

	if (m_frameCount > 0)
		copyLocal->m_vertexFrames = new VertexPositionsFrame[m_frameCount];

	for (int i = 0; i < m_frameCount; i++) {
		if (m_vertexFrames[i].m_vertexCount > 0)
			copyLocal->m_vertexFrames[i].m_vertexs = new ABVERTEX0L[m_vertexFrames[i].m_vertexCount];

		for (int vCount = 0; vCount < m_vertexFrames[i].m_vertexCount; vCount++) {
			copyLocal->m_vertexFrames[i].m_vertexs[vCount].x = m_vertexFrames[i].m_vertexs[vCount].x;
			copyLocal->m_vertexFrames[i].m_vertexs[vCount].y = m_vertexFrames[i].m_vertexs[vCount].y;
			copyLocal->m_vertexFrames[i].m_vertexs[vCount].z = m_vertexFrames[i].m_vertexs[vCount].z;
			copyLocal->m_vertexFrames[i].m_vertexs[vCount].nx = m_vertexFrames[i].m_vertexs[vCount].nx;
			copyLocal->m_vertexFrames[i].m_vertexs[vCount].ny = m_vertexFrames[i].m_vertexs[vCount].ny;
			copyLocal->m_vertexFrames[i].m_vertexs[vCount].nz = m_vertexFrames[i].m_vertexs[vCount].nz;
		}
	}
}

bool CVertexAnimationModule::Compare(CVertexAnimationModule* module1, CVertexAnimationModule* module2) {
	if (module1 == NULL || module2 == NULL) {
		if (module1 == NULL && module2 == NULL) {
			return true;
		} else {
			return false;
		}
	}
	if (module1->m_frameCount != module2->m_frameCount) {
		return false;
	}
	if (module1->m_timeBetweenFrames != module2->m_timeBetweenFrames) {
		return false;
	}
	if (module1->m_looping != module2->m_looping) {
		return false;
	}

	if (module1->m_frameCount > 0) {
		for (int i = 0; i < module1->m_frameCount; i++) {
			if (module1->m_vertexFrames[i].m_vertexCount != module2->m_vertexFrames[i].m_vertexCount) {
				return false;
			}
			for (int j = 0; j < module1->m_vertexFrames[i].m_vertexCount; j++) {
				if (module1->m_vertexFrames[i].m_vertexs[j].x != module2->m_vertexFrames[i].m_vertexs[j].x) {
					return false;
				}
				if (module1->m_vertexFrames[i].m_vertexs[j].y != module2->m_vertexFrames[i].m_vertexs[j].y) {
					return false;
				}
				if (module1->m_vertexFrames[i].m_vertexs[j].z != module2->m_vertexFrames[i].m_vertexs[j].z) {
					return false;
				}
				if (module1->m_vertexFrames[i].m_vertexs[j].nx != module2->m_vertexFrames[i].m_vertexs[j].nx) {
					return false;
				}
				if (module1->m_vertexFrames[i].m_vertexs[j].ny != module2->m_vertexFrames[i].m_vertexs[j].ny) {
					return false;
				}
				if (module1->m_vertexFrames[i].m_vertexs[j].nz != module2->m_vertexFrames[i].m_vertexs[j].nz) {
					return false;
				}
			}
		}
	}
	return true;
}

void CVertexAnimationModuleList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CVertexAnimationModule* spnPtr = (CVertexAnimationModule*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CVertexAnimationModule::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_frameCount << m_curFrame << m_targetFrame << (long)m_timeBetweenFrames << m_looping;

		for (int i = 0; i < m_frameCount; i++) {
			ar << m_vertexFrames[i].m_vertexCount;
			for (int vCount = 0; vCount < m_vertexFrames[i].m_vertexCount; vCount++) {
				ar << m_vertexFrames[i].m_vertexs[vCount].x
				   << m_vertexFrames[i].m_vertexs[vCount].y
				   << m_vertexFrames[i].m_vertexs[vCount].z
				   << m_vertexFrames[i].m_vertexs[vCount].nx
				   << m_vertexFrames[i].m_vertexs[vCount].ny
				   << m_vertexFrames[i].m_vertexs[vCount].nz;
			}
		}
	} else {
		long timeBetweenFramesLongInt;
		ar >> m_frameCount >> m_curFrame >> m_targetFrame >> timeBetweenFramesLongInt >> m_looping;
		m_timeBetweenFrames = timeBetweenFramesLongInt;

		if (m_frameCount > 0)
			m_vertexFrames = new VertexPositionsFrame[m_frameCount];

		for (int i = 0; i < m_frameCount; i++) {
			ar >> m_vertexFrames[i].m_vertexCount;
			if (m_vertexFrames[i].m_vertexCount > 0)
				m_vertexFrames[i].m_vertexs = new ABVERTEX0L[m_vertexFrames[i].m_vertexCount];

			for (int vCount = 0; vCount < m_vertexFrames[i].m_vertexCount; vCount++) {
				ar >> m_vertexFrames[i].m_vertexs[vCount].x >> m_vertexFrames[i].m_vertexs[vCount].y >> m_vertexFrames[i].m_vertexs[vCount].z >> m_vertexFrames[i].m_vertexs[vCount].nx >> m_vertexFrames[i].m_vertexs[vCount].ny >> m_vertexFrames[i].m_vertexs[vCount].nz;
			}
		}
		m_lastUpdatedFrameChngTime = fTime::TimeMs();
	}
}

bool CVertexAnimationModule::AddKeyFrameByMorphing(const CEXMeshObj& base, const CEXMeshObj& target, float weight) {
	return AddKeyFrameByMorphing(base, 1, &target, &weight);
}

bool CVertexAnimationModule::AddKeyFrameByMorphing(const CEXMeshObj& base, int numTargets, const CEXMeshObj* targets, float* weights) {
	//1. check all meshes for vertex counts
	UINT vtxCount = base.GetVertices().GetVertexCount();

	if (!checkNewFrameVertexCount("", vtxCount))
		return false;

	for (int i = 0; i < numTargets; i++) {
		ASSERT(targets[i].GetVertices().GetVertexCount() == vtxCount);
		if (targets[i].GetVertices().GetVertexCount() != vtxCount)
			return false; // to be changed to VERIFY_RETURN instead
	}

	ABVERTEX0L* vtxArray = new ABVERTEX0L[vtxCount];
	memset(vtxArray, 0, sizeof(ABVERTEX0L) * vtxCount);

	for (UINT vtxLoop = 0; vtxLoop < vtxCount; ++vtxLoop) {
		Vector3f& pos = *(Vector3f*)&vtxArray[vtxLoop].x;
		Vector3f& nml = *(Vector3f*)&vtxArray[vtxLoop].nx;

		float totalTargetWeights = 0.0f;
		for (int tgtLoop = 0; tgtLoop < numTargets; tgtLoop++) {
			const CAdvancedVertexObj& tgtVertices = targets[tgtLoop].GetVertices();
			float tgtWeight = weights[tgtLoop];

			ABVERTEX tgtVtx = tgtVertices.GetVertex(vtxLoop);
			Vector3f& tgtPos = *(Vector3f*)&tgtVtx.x;
			Vector3f& tgtNml = *(Vector3f*)&tgtVtx.nx;

			pos += tgtPos * tgtWeight;
			nml += tgtNml * tgtWeight; //@@Watch: poor normal interpolation algorithm
			totalTargetWeights += tgtWeight;
		}

		ABVERTEX baseVtx = base.GetVertices().GetVertex(vtxLoop);
		Vector3f& basePos = *(Vector3f*)&baseVtx.x;
		Vector3f& baseNml = *(Vector3f*)&baseVtx.nx;

		pos += basePos * (1.0f - totalTargetWeights);
		nml += baseNml * (1.0f - totalTargetWeights);
	}

	return AddKeyFrame(vtxArray, vtxCount);
}

void VertexPositionsFrame::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObjectSizeof(m_vertexs, m_vertexCount);
	}
}

void CVertexAnimationModule::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_vertexFrames);
	}
}

IMPLEMENT_SERIAL(CVertexAnimationModule, CObject, 0)
IMPLEMENT_SERIAL(CVertexAnimationModuleList, CObList, 0)

BEGIN_GETSET_IMPL(CVertexAnimationModule, IDS_VERTEXANIMATIONMODULE_NAME)
GETSET_RANGE(m_frameCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, VERTEXANIMATIONMODULE, FRAMECOUNT, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP