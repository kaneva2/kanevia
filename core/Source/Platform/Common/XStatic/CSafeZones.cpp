///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include "d3d9.h"
#include <afxtempl.h>

#include "CSafeZones.h"

namespace KEP {

CSafeZoneObj::CSafeZoneObj() {
	m_zoneName = "SAFE";
	m_channel.clear();
	m_boundingBoxMax.x = 60000.0f;
	m_boundingBoxMax.y = 60000.0f;
	m_boundingBoxMax.z = 60000.0f;
	m_boundingBoxMin.x = -60000.0f;
	m_boundingBoxMin.y = -60000.0f;
	m_boundingBoxMin.z = -60000.0f;
	m_rebirthableZone = FALSE;
}

void CSafeZoneObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSafeZoneObj* spnPtr = (CSafeZoneObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

BOOL CSafeZoneObjList::BoundBoxPointCheck(const Vector3f& point,
	const Vector3f& min,
	const Vector3f& max) {
	if (point.x >= min.x &&
		point.x <= max.x &&
		point.y >= min.y &&
		point.y <= max.y &&
		point.z >= min.z &&
		point.z <= max.z)
		return TRUE;
	return FALSE;
}

CSafeZoneObj* CSafeZoneObjList::GetSafeZoneByPosition(const Vector3f& position,
	const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSafeZoneObj* cmPtr = (CSafeZoneObj*)GetNext(posLoc);
		if (channel.channelIdEquals(cmPtr->m_channel)) //same world
			if (BoundBoxPointCheck(position,
					cmPtr->m_boundingBoxMin,
					cmPtr->m_boundingBoxMax) == TRUE) {
				//in zone
				return cmPtr;
			} //end in zone
	}
	return NULL;
}

void CSafeZoneObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_zoneName
		   << m_channel
		   << m_boundingBoxMax.x
		   << m_boundingBoxMax.y
		   << m_boundingBoxMax.z
		   << m_boundingBoxMin.x
		   << m_boundingBoxMin.y
		   << m_boundingBoxMin.z
		   << m_rebirthableZone;
	} else {
		ar >> m_zoneName >> m_channel >> m_boundingBoxMax.x >> m_boundingBoxMax.y >> m_boundingBoxMax.z >> m_boundingBoxMin.x >> m_boundingBoxMin.y >> m_boundingBoxMin.z >> m_rebirthableZone;
	}
}

IMPLEMENT_SERIAL(CSafeZoneObj, CObject, 0)
IMPLEMENT_SERIAL(CSafeZoneObjList, CKEPObList, 0)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CSafeZoneObj, IDS_SAFEZONEOBJ_NAME)
GETSET_MAX(m_zoneName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SAFEZONEOBJ, ZONENAME, STRLEN_MAX)
GETSET_RANGE(m_channel, gsInt, GS_FLAGS_DEFAULT, 0, 0, SAFEZONEOBJ, WORLDID, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_boundingBoxMax, GS_FLAGS_DEFAULT, 0, 0, SAFEZONEOBJ, BOUNDINGBOXMAX)
GETSET_D3DVECTOR(m_boundingBoxMin, GS_FLAGS_DEFAULT, 0, 0, SAFEZONEOBJ, BOUNDINGBOXMIN)
GETSET(m_rebirthableZone, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, SAFEZONEOBJ, REBIRTHABLEZONE)
END_GETSET_IMPL

} // namespace KEP