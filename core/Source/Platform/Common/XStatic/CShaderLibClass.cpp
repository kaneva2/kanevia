///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <d3dx9.h>
#include <afxtempl.h>

#include "CShaderLibClass.h"

namespace KEP {

CShaderObj::CShaderObj() {
	m_shaderName;
	m_shaderCode;
	m_shader = NULL;
	m_pShader = NULL;
	m_pEffect = NULL;
	m_shaderValidHandle = NULL;
	m_shaderType = 0; //shader type 0=vertex 1=pixel
	m_compiler = 1;
	m_effectParamCount = 0;
	m_shaderUsedOnce = 0;
	m_resetRenderState = 1;
}

void CShaderObj::SafeDelete() {
	m_shaderUsedOnce = 0;

	if (m_shader) {
		m_shader->Release();
		m_shader = 0;
	}

	if (m_pShader) {
		m_pShader->Release();
		m_pShader = 0;
	}

	if (m_pEffect) {
		//effect delete
		//delete possible dynamic procedurals
		int trace = 0;
		while (1) {
			//param loop
			D3DXHANDLE floatingHandle = m_pEffect->GetParameter(NULL, trace);
			if (!floatingHandle)
				break;
			trace++;

			D3DXPARAMETER_DESC localDescStruct;
			if (m_pEffect->GetParameterDesc(floatingHandle, &localDescStruct) != S_OK)
				continue;

			if (localDescStruct.Type != D3DXPT_TEXTURE)
				continue;

			if (localDescStruct.Annotations < 1)
				continue;

			int Depth = 0;
			D3DXPARAMETER_DESC AnnotDesc;
			const char* textureType;
			const char* pstrFunction;
			const char* pstrTarget;
			const char* pstrName;
			textureType = pstrFunction = pstrTarget = pstrName = NULL;
			for (UINT iAnnot = 0; iAnnot < localDescStruct.Annotations; iAnnot++) {
				D3DXHANDLE hAnnot = m_pEffect->GetAnnotation(floatingHandle, iAnnot);
				if (m_pEffect->GetParameterDesc(hAnnot, &AnnotDesc) == S_OK) {
					if (_strcmpi(AnnotDesc.Name, "name") == 0)
						m_pEffect->GetString(hAnnot, &pstrName);
					else if (_strcmpi(AnnotDesc.Name, "function") == 0)
						m_pEffect->GetString(hAnnot, &pstrFunction);
					else if (_strcmpi(AnnotDesc.Name, "depth") == 0)
						m_pEffect->GetInt(hAnnot, &Depth);
				}
			}
			if (Depth > 0) {
				//3D texture detected
				if (pstrName == NULL && pstrFunction != NULL) {
					//delete
					LPDIRECT3DBASETEXTURE9 ppTexture = NULL;
					HRESULT errHr = m_pEffect->GetTexture(floatingHandle, &ppTexture);
					if (errHr == S_OK)
						if (ppTexture) {
							m_pEffect->SetTexture(floatingHandle, NULL);
							ppTexture->Release();
							ppTexture = 0;
						}
				} //end delete
			} //end 3D texture detected
		} //end delete possible dynamic procedurals
		m_pEffect->Release();
		m_pEffect = 0;
	} //end effect delete
}

BOOL CShaderObj::Compile(LPDIRECT3DDEVICE9 pd3dDevice) {
	if (m_shader) {
		m_shader->Release();
		m_shader = 0;
	}

	if (m_pShader) {
		m_pShader->Release();
		m_pShader = 0;
	}

	if (m_pEffect) {
		m_pEffect->Release();
		m_pEffect = 0;
	}

	HRESULT hr;

	int bufSize = m_shaderCode.GetLength();

	if (m_compiler == 0) {
		//compile low level
		LPD3DXBUFFER pCode = NULL;
		hr = D3DXAssembleShader((const char*)m_shaderCode,
			bufSize, //UINT SrcDataLen,
			NULL, //CONST D3DXMACRO* pDefines,
			NULL, //LPD3DXINCLUDE pInclude,
			0, //DWORD Flags,
			&pCode, //LPD3DXBUFFER* ppShader,
			NULL);

		if (pCode) {
			if (m_shaderType == 0) {
				hr = pd3dDevice->CreateVertexShader((DWORD*)pCode->GetBufferPointer(), &m_shader);
			} else if (m_shaderType == 1) {
				hr = pd3dDevice->CreatePixelShader((DWORD*)pCode->GetBufferPointer(), &m_pShader);
			}

			pCode->Release();
			pCode = 0;
		} else
			return FALSE;
	} //end compile low level
	else if (m_compiler == 1) {
		//compile fx style

		LPD3DXBUFFER parseErrorsBuffer;
		DWORD dwFlags = D3DXSHADER_DEBUG;

		HRESULT complErr = D3DXCreateEffect(pd3dDevice,
			m_shaderCode,
			bufSize,
			NULL,
			NULL, //&includeManager,
			dwFlags,
			NULL,
			&m_pEffect,
			&parseErrorsBuffer);

		if (complErr == 0) {
			m_effectParamCount = 0;
			while (1) {
				D3DXHANDLE floatingHandle = m_pEffect->GetParameter(NULL, m_effectParamCount);
				if (floatingHandle) {
					D3DXPARAMETER_DESC localDescStruct;
					if (m_pEffect->GetParameterDesc(floatingHandle, &localDescStruct) == S_OK) {
						m_effectParamCount++;
					}
				} else
					break;
			}
		} else
			return FALSE;
	} //end compile fx style

	return TRUE;
}

void CShaderObjList::Compile(LPDIRECT3DDEVICE9 pd3dDevice) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CShaderObj* spnPtr = (CShaderObj*)GetNext(posLoc);
		spnPtr->Compile(pd3dDevice);
	}
}

void CShaderObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CShaderObj* spnPtr = (CShaderObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

BOOL CShaderObjList::DeleteByIndex(int index) {
	if (index < 0)
		return FALSE;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return FALSE;

	CShaderObj* spnPtr = (CShaderObj*)GetAt(posLoc);
	delete spnPtr;
	spnPtr = 0;
	RemoveAt(posLoc);
	return TRUE;
}

CShaderObj* CShaderObjList::GetByIndex(int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;

	CShaderObj* spnPtr = (CShaderObj*)GetAt(posLoc);
	return spnPtr;
}

CShaderObj* CShaderObjList::GetByName(CStringA shaderName, int& index) {
	index = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CShaderObj* spnPtr = (CShaderObj*)GetNext(posLoc);
		if (spnPtr->m_shaderName.CompareNoCase(shaderName) == 0)
			return spnPtr;

		index++;
	}
	return NULL;
}

BOOL CShaderObjList::SetShaderByIndex(LPDIRECT3DDEVICE9 pd3dDevice,
	int index,
	int mode,
	LPD3DXEFFECT* effectRet,
	int& correctParamCount,
	CStringA* retCode,
	CShaderObj** retPtr) {
	POSITION posLoc = 0;
	CShaderObj* spnPtr = 0;
	if ((index < 0) || (0 == (posLoc = FindIndex(index))) || (0 == (spnPtr = (CShaderObj*)GetAt(posLoc)))) {
		if (mode == 0)
			pd3dDevice->SetVertexShader(NULL);
		else if (mode == 1) {
			pd3dDevice->SetPixelShader(NULL);
			*effectRet = NULL;
		}
		return FALSE;
	}

	if (!spnPtr)
		return FALSE;

	if (retPtr)
		*retPtr = spnPtr;

	if (retCode)
		*retCode = spnPtr->m_shaderCode;

	if (spnPtr->m_compiler == 1) {
		// Using FX Shaders
		if (spnPtr->m_pEffect) {
			if ((0 == spnPtr->m_shaderValidHandle)) {
				spnPtr->m_pEffect->FindNextValidTechnique(NULL, &spnPtr->m_shaderValidHandle);
				spnPtr->m_pEffect->SetTechnique(spnPtr->m_shaderValidHandle);
			}

			if (spnPtr->m_shaderValidHandle) {
				*effectRet = spnPtr->m_pEffect;
				correctParamCount = spnPtr->m_effectParamCount;
				return TRUE;
			} else
				return FALSE;
		}
	} else {
		// using individual pixel and vertex shaders
		if ((spnPtr->m_shader && (pd3dDevice->SetVertexShader(spnPtr->m_shader) != 0))) {
			spnPtr->m_shaderType = 0;
			return FALSE;
		}

		if ((spnPtr->m_pShader) && (pd3dDevice->SetPixelShader(spnPtr->m_pShader) != 0)) {
			spnPtr->m_shaderType = 1;
			return FALSE;
		}
	}

	return TRUE;
}

int CShaderObjList::LoadShaderByStringData(CStringA dataStr, LPDIRECT3DDEVICE9 pd3dDevice, CStringA& fxNameRet) {
	//let the parse begin

	CStringA shaderName = "";
	CStringA shaderCode = "";
	BOOL fxType = TRUE;
	int dataLength = dataStr.GetLength();
	if (dataLength < 2)
		return -1;

	for (int loop = 0; loop < dataLength; loop++) {
		char curC = dataStr.GetAt(0);

		if (curC == '*') {
			dataStr.Delete(0, 1);
			break;
		}
		shaderName += curC;

		dataStr.Delete(0, 1);
	}

	shaderCode = dataStr;

	//if(!GetByName(shaderName))
	//    return FALSE;

	//compare then add
	CShaderObj* shdrPtr = new CShaderObj();
	shdrPtr->m_shaderCode = shaderCode;
	shdrPtr->m_shaderName = shaderName;
	shdrPtr->m_compiler = fxType; //flag fx
	shdrPtr->m_shaderType = 1;
	shdrPtr->m_resetRenderState = 1; //**TODO** Not sure about this

	shdrPtr->Compile(pd3dDevice);

	int index = 0;
	CShaderObj* shdrPtrDup = GetByName(shaderName, index);
	fxNameRet = shaderName;
	if (shdrPtrDup)
		if (shdrPtrDup->m_effectParamCount != shdrPtr->m_effectParamCount) {
			shdrPtr->m_shaderName + " ParamCount:" + ConvToString(shdrPtr->m_effectParamCount);
			AddTail(shdrPtr);
			return GetCount() - 1;
		} else
			return index; //found valid shader

	if (!shdrPtrDup)
		AddTail(shdrPtr);

	return GetCount() - 1; //v-fmt
} //end let the parse begin

//-----------END FUNCTION--------------------------------------//
//-------------------------------------------------------------//
//-----converts a long into string-----------------------------//
CStringA CShaderObjList::ConvToString(int number) {
	if (number == 0)
		return "0";

	int decimal, sign;
	char buffer[32];
	CStringA temp;
	_fcvt_s(buffer, _countof(buffer), number, 0, &decimal, &sign);
	temp = buffer;
	if (sign != 0)
		temp = operator+("-", temp);

	return temp;
}

void CShaderObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(2);
		ASSERT(ar.GetObjectSchema() == 2);

		ar << m_shaderName
		   << m_shaderCode;

		ar << m_shaderType
		   << m_compiler
		   << m_resetRenderState
		   << reserved
		   << reserved;
	} else {
		int version = ar.GetObjectSchema();

		ar >> m_shaderName >> m_shaderCode;

		if (version == 2) {
			ar >> m_shaderType >> m_compiler >> m_resetRenderState >> reserved >> reserved;
		}

		m_shader = NULL;
		m_pShader = NULL;
		m_pEffect = NULL;
		m_shaderUsedOnce = 0;
	}
}

void CShaderObjList::OnLostDevice() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CShaderObj* spnPtr = (CShaderObj*)GetNext(posLoc);
		if (spnPtr->m_pEffect)
			spnPtr->m_pEffect->OnLostDevice();
	}
}

void CShaderObjList::OnResetDevice() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CShaderObj* spnPtr = (CShaderObj*)GetNext(posLoc);
		if (spnPtr->m_pEffect)
			spnPtr->m_pEffect->OnResetDevice();
	}
}

IMPLEMENT_SERIAL_SCHEMA(CShaderObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CShaderObjList, CKEPObList)

BEGIN_GETSET_IMPL(CShaderObj, IDS_SHADEROBJ_NAME)
GETSET_MAX(m_shaderName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SHADEROBJ, SHADERNAME, STRLEN_MAX)
GETSET_MAX(m_shaderCode, gsCString, GS_FLAGS_DEFAULT, 0, 0, SHADEROBJ, SHADERCODE, STRLEN_MAX)
GETSET_RANGE(m_shaderType, gsInt, GS_FLAGS_DEFAULT, 0, 0, SHADEROBJ, SHADERTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_compiler, gsInt, GS_FLAGS_DEFAULT, 0, 0, SHADEROBJ, COMPILER, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP