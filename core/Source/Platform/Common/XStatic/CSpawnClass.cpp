///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CSpawnClass.h"

namespace KEP {

CSpawnObj::CSpawnObj() {
	m_spawnMatrix.MakeIdentity();
	m_spawnName = "Unnamed Spawn Point";
	m_maxOutPutCapacity = 1;
	m_currentOutPut = 0;
	m_possibilityOfSpawn = 500; //% out of 1000 once per minute evaluation
	m_spawnIndex = 0;
	m_spawnRadius = 0.0f;
	m_spawnType = 5;
	m_treeUse = -1; //-1 = none
	m_treePosition = -1; //-1 = random
	m_aiCfg = 0;
	m_channel = -1;
	m_manualStart = TRUE;
}

CSpawnObj& CSpawnObj::operator=(const CSpawnObj& src) {
	if (this != &src) {
		m_spawnMatrix = src.m_spawnMatrix;
		m_spawnName = src.m_spawnName;
		m_maxOutPutCapacity = src.m_maxOutPutCapacity;
		m_currentOutPut = src.m_currentOutPut;
		m_possibilityOfSpawn = src.m_possibilityOfSpawn; //% out of 1000
		m_spawnIndex = src.m_spawnIndex;
		m_spawnRadius = src.m_spawnRadius;
		m_spawnType = src.m_spawnType;
		m_aiCfg = src.m_aiCfg; //if used
		m_treeUse = src.m_treeUse; //-1 = none
		m_treePosition = src.m_treePosition; //-1 = random
		m_manualStart = src.m_manualStart;
		m_channel = src.m_channel;
	}
	return *this;
}

void CSpawnObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSpawnObj* spnPtr = (CSpawnObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CSpawnObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_spawnName
		   << m_maxOutPutCapacity
		   << m_aiCfg
		   << m_possibilityOfSpawn
		   << m_spawnIndex
		   << m_spawnRadius
		   << m_spawnType
		   << m_treeUse
		   << m_treePosition;

		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar << m_spawnMatrix[loop][loop2];

		ar << m_manualStart;
	} else {
		int version = ar.GetObjectSchema();
		m_manualStart = TRUE;

		ar >> m_spawnName >> m_maxOutPutCapacity >> m_aiCfg >> m_possibilityOfSpawn >> m_spawnIndex >> m_spawnRadius >> m_spawnType >> m_treeUse >> m_treePosition;

		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar >> m_spawnMatrix[loop][loop2];

		m_currentOutPut = 0;

		if (version > 1) {
			ar >> m_manualStart;
		}
	}
}

CSpawnObjList::CSpawnObjList() {
	m_recentTotalOutput = 0;
}

long CSpawnObjList::GetTotalAIOutput() {
	long total = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSpawnObj* spwnPtr = (CSpawnObj*)GetNext(posLoc);
		if (spwnPtr)
			total += spwnPtr->m_maxOutPutCapacity;
	}
	m_recentTotalOutput = total;
	return total;
}

void CSpawnObjList::UpdateChannel(LONG channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSpawnObj* spwnPtr = (CSpawnObj*)GetNext(posLoc);
		if (spwnPtr)
			spwnPtr->m_channel = channel;
	}
}

IMPLEMENT_SERIAL_SCHEMA(CSpawnObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CSpawnObjList, CKEPObList)

BEGIN_GETSET_IMPL(CSpawnObj, IDS_SPAWNOBJ_NAME)
GETSET_MAX(m_spawnName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, SPAWNNAME, STRLEN_MAX)
GETSET_RANGE(m_maxOutPutCapacity, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, MAXOUTPUTCAPACITY, INT_MIN, INT_MAX)
GETSET_RANGE(m_aiCfg, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, AICFG, INT_MIN, INT_MAX)
GETSET_RANGE(m_possibilityOfSpawn, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, POSSIBILITYOFSPAWN, INT_MIN, INT_MAX)
GETSET_RANGE(m_spawnIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, SPAWNINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_spawnRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, SPAWNRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_spawnType, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, SPAWNTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_treeUse, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, TREEUSE, INT_MIN, INT_MAX)
GETSET_RANGE(m_treePosition, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, TREEPOSITION, INT_MIN, INT_MAX)
GETSET_RANGE(m_manualStart, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, SPAWNOBJ, MANUALSTART, 0, 1)
END_GETSET_IMPL

} // namespace KEP