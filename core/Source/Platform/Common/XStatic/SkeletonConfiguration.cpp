///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SkeletonConfiguration.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CMaterialClass.h"
#include "CDeformableMesh.h"
#include "TextureManager.h"
#include "ReMeshCreate.h"
#include "MeshProxy.h"
#include "IClientEngine.h"
#include "CShaderLibClass.h"
#include "ReSkinMesh.h"
#include "CBoneClass.h"
#include "TextureProvider.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

void SkeletonConfiguration::Clone(
	SkeletonConfiguration** clone,
	RuntimeSkeleton* pRS,
	int* pMeshIdsOpt,
	int* pMatlIdsOpt,
	D3DCOLORVALUE* pColorOpt,
	bool fullClone) {
	SkeletonConfiguration* cloneReturn = new SkeletonConfiguration();
	cloneReturn->m_newConfig.initBaseItem(pRS->getSkinnedMeshCount());
	cloneReturn->m_charConfig.initBaseItem(pRS->getSkinnedMeshCount());
	if (fullClone)
		CloneBonesMeshes(cloneReturn, pRS, pMeshIdsOpt, pMatlIdsOpt, pColorOpt);
	*clone = cloneReturn;
}

void SkeletonConfiguration::CloneBonesMeshes(
	SkeletonConfiguration* cloneReturn,
	RuntimeSkeleton* pRS,
	int* pMeshIdsOpt,
	int* pMatlIdsOpt,
	D3DCOLORVALUE* pColorOpt) {
	CloneMeshes(cloneReturn, pRS, pMeshIdsOpt, pMatlIdsOpt, pColorOpt);
}

void SkeletonConfiguration::CloneMeshes(
	SkeletonConfiguration* cloneReturn,
	RuntimeSkeleton* pRS,
	int* pMeshIdsOpt,
	int* pMatlIdsOpt,
	D3DCOLORVALUE* pColorOpt) const {
#ifdef _ClientOutput
	if (!pRS || pRS->getSkinnedMeshCount() == 0)
		return;

	for (int slot = 0; slot < pRS->getSkinnedMeshCount(); slot++) {
		if (!m_meshSlots)
			continue;

		POSITION alPos = m_meshSlots->FindIndex(slot);
		if (!alPos)
			continue;

		CAlterUnitDBObject* altPtr = (CAlterUnitDBObject*)m_meshSlots->GetAt(alPos);
		POSITION curCfgPos = NULL;
		int curMat = 0;
		int curCfg = 0;
		float curR = -1.0;
		float curG = -1.0;
		float curB = -1.0;

		if (!pMeshIdsOpt)
			curCfg = m_charConfig.getItemSlotMeshId(GOB_BASE_ITEM, slot);
		else
			curCfg = pMeshIdsOpt[slot];

		if (!pMatlIdsOpt)
			curMat = m_charConfig.getItemSlotMaterialId(GOB_BASE_ITEM, slot);
		else
			curMat = pMatlIdsOpt[slot];

		if (pColorOpt) {
			curR = pColorOpt[slot].r;
			curG = pColorOpt[slot].g;
			curB = pColorOpt[slot].b;
		} else {
			m_charConfig.getItemSlotColor(GOB_BASE_ITEM, slot, curR, curG, curB);
		}
		curCfgPos = altPtr->m_configurations->FindIndex(abs(curCfg));

		// get a valid default
		if (curCfgPos == NULL)
			curCfgPos = altPtr->m_configurations->FindIndex(0);

		if (curCfgPos) {
			// We need to swap the supported material
			//
			// this is awkward... can I pull it out into RuntimeSkeleton::Clone?
			// Or maybe it should be in CSkeletonObject::Clone and called after
			// the runtime and configuration objects are cloned to configure
			// the runtime skeleton?
			//
			cloneReturn->HotSwapDim(slot, curCfg, pRS->getSkinnedMeshCount(), true);
			cloneReturn->HotSwapMat(slot, curCfg, curMat, TRUE);
			if (curR >= 0 && curG >= 0 && curB >= 0) {
				// Need to use a custom RGB.
				cloneReturn->HotSwapRGB(slot, curR, curG, curB, pRS);
			}
		}
	}

	if (pRS->getSkinnedMeshCount() > 0) {
		cloneReturn->m_charConfig.initBaseItem(pRS->getSkinnedMeshCount());
		if (!pMeshIdsOpt)
			cloneReturn->m_newConfig.initBaseItem(pRS->getSkinnedMeshCount());

		for (int baseSlot = 0; baseSlot < pRS->getSkinnedMeshCount(); baseSlot++) {
			if (pMeshIdsOpt)
				cloneReturn->m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, baseSlot, pMeshIdsOpt[baseSlot]);
			if (pMatlIdsOpt)
				cloneReturn->m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, baseSlot, pMatlIdsOpt[baseSlot]);
		}
	}
#endif
}

void SkeletonConfiguration::InitialDimConfig(const CharConfig& charConfig) {
	m_charConfig = charConfig;
	m_newConfig = charConfig;
	m_lastConfig.disposeAllItems();
}

void SkeletonConfiguration::DimSwap(RuntimeSkeleton* pRS, int baseSlot, int meshId, int matlId) const {
#ifdef _ClientOutput
	if (!pRS || baseSlot >= pRS->getSkinnedMeshCount())
		return;

	// This added to allow hair removal for UGC faces
	if (meshId < 0) {
		pRS->clearLODMeshes(static_cast<unsigned>(baseSlot));
	}

	// delete existing mesh in this slot if exists
	pRS->disposeSkinnedMesh(baseSlot);

	if (m_meshSlots) {
		POSITION alPos = m_meshSlots->FindIndex(baseSlot); // find alter unit db ptr for this slot

		if (alPos) {
			CAlterUnitDBObject* altPtr = (CAlterUnitDBObject*)m_meshSlots->GetAt(alPos);

			if (altPtr && altPtr->m_configurations) {
				POSITION curCfgPos = altPtr->m_configurations->FindIndex(meshId); // find deformable mesh object by mesh index

				if (curCfgPos) {
					CDeformableMesh* refMesh = (CDeformableMesh*)altPtr->m_configurations->GetAt(curCfgPos);

					// remove anything in the lod array
					pRS->clearLODMeshes(static_cast<unsigned>(baseSlot));

					// don't swap in a mesh if we can't also swap in its material
					if (refMesh && refMesh->m_supportedMaterials && matlId < refMesh->m_supportedMaterials->GetCount()) {
						// clone the deformable mesh
						CDeformableMesh* pClonedMesh = nullptr;
						refMesh->Clone(&pClonedMesh, NULL, pRS->getBoneList());
						pRS->setSkinnedMesh(baseSlot, pClonedMesh);

						if (matlId > -1) {
							// clone the material
							POSITION materialPos = refMesh->m_supportedMaterials->FindIndex(matlId);

							if (materialPos) {
								// Find the material to use on the first LOD
								CMaterialObject* mat = (CMaterialObject*)refMesh->m_supportedMaterials->GetAt(materialPos);

								if (mat) {
									CDeformableMesh* tarMesh = pRS->getSkinnedMesh(baseSlot);

									if (tarMesh) {
										if (tarMesh->m_material) {
											delete tarMesh->m_material;
											tarMesh->m_material = NULL;
										}

										mat->Clone(&(tarMesh->m_material));

										if (pRS->getCurrentTexture()) {
											tarMesh->m_material->SetCustomTexture(pRS->getCurrentTexture(), CMaterialObject::CUSTOM_ALL_TEXTURE_SLOTS);
										}

										tarMesh->m_currentMatInUse = matlId;

										MeshRM::Instance()->CreateReMaterial(tarMesh->m_material);
										for (unsigned int lod = 0; lod < (unsigned int)refMesh->m_lodCount; lod++) {
											auto pMesh = pRS->getLODMesh(static_cast<unsigned>(baseSlot), lod);
											if (pMesh != nullptr) {
												pMesh->SetMaterial(tarMesh->m_material->GetReMaterial());
											}
										}
									}
								}
							}
						}
					}

					// finished
					pRS->setMeshSortValid(false);
				}
			}
		}
	}
#endif // _ClientOutput
}

void SkeletonConfiguration::RealizeSlotConfig(RuntimeSkeleton* pRS, int deformableIndex, CAlterUnitDBObjectList* alternateConfigurations) {
	const CharConfigItem* pBaseConfigItem = m_newConfig.getItemByGlidOrBase(GOB_BASE_ITEM);
	if (pBaseConfigItem && deformableIndex >= 0 && deformableIndex < (int)pBaseConfigItem->getSlotCount()) {
		const CharConfigItemSlot* pSlot = pBaseConfigItem->getSlot(deformableIndex);
		assert(pSlot != nullptr);

		int meshIndex = pSlot->m_meshId;
		int matIndex = pSlot->m_matlId;
		if (meshIndex >= 0 && matIndex >= 0) {
			// For convenience i.e. not having to change DimSwap right now.
			m_meshSlots = alternateConfigurations;
			DimSwap(pRS, deformableIndex, meshIndex, matIndex);
			m_meshSlots = NULL; // set it back to avoid confusion and potential for double delete
		}
	}
}

void SkeletonConfiguration::RealizeDimConfig(RuntimeSkeleton* pRS, CAlterUnitDBObjectList* alternateConfigurations) {
	if (!pRS)
		return;

	if (alternateConfigurations == 0)
		return;

	// For convenience i.e. not having to change DimSwap right now.
	m_meshSlots = alternateConfigurations;

	if (m_lastConfig.getItemCount() == 0) {
		// Swap in everything that's in m_newConfig.at(0)
		const CharConfigItem* pConfigItem = m_newConfig.getItemByIndex(0); // Consider using getItemByGlidOrBase(GOB_BASE_ITEM)

		if (pConfigItem) {
			if (pRS->getSkinnedMeshCount() == pConfigItem->getSlotCount()) {
				for (size_t deformableIndex = 0; deformableIndex < pConfigItem->getSlotCount(); deformableIndex++) {
					const CharConfigItemSlot* pConfigSlot = pConfigItem->getSlot(deformableIndex);
					assert(pConfigSlot != nullptr);

					// check valid mesh and mtl index
					if (pConfigSlot->m_meshId != -1 && pConfigSlot->m_matlId != static_cast<USHORT>(-1))
						DimSwap(pRS, deformableIndex, pConfigSlot->m_meshId, pConfigSlot->m_matlId);
				}
			}
		}
	} else {
		// Swap in only the things that are different between m_newConfig and m_lastConfig
		if (m_lastConfig.getItemCount() > 0 && m_newConfig.getItemCount() > 0) {
			const CharConfigItem* pCurCfgItem = m_newConfig.getItemByIndex(0);
			assert(pCurCfgItem != nullptr);
			const CharConfigItem* pPreCfgItem = m_lastConfig.getItemByIndex(0);
			assert(pPreCfgItem != nullptr);

			if (pCurCfgItem->getSlotCount() == pPreCfgItem->getSlotCount()) {
				for (size_t slot = 0; slot < pCurCfgItem->getSlotCount(); slot++) {
					const CharConfigItemSlot* pCurCfgSlot = pCurCfgItem->getSlot(slot);
					assert(pCurCfgSlot != nullptr);
					const CharConfigItemSlot* pPreCfgSlot = pPreCfgItem->getSlot(slot);
					assert(pPreCfgSlot != nullptr);

					// If the material index or mesh index changed ...
					if (pCurCfgSlot->m_meshId != pPreCfgSlot->m_meshId ||
						pCurCfgSlot->m_matlId != pPreCfgSlot->m_matlId) {
						if (pCurCfgSlot->m_matlId != static_cast<USHORT>(-1))
							DimSwap(pRS, slot, pCurCfgSlot->m_meshId, pCurCfgSlot->m_matlId);
					}
				}
			}
		}
	}

	m_meshSlots = NULL; // set it back to avoid confusion and potential for double delete

	// Update m_lastConfig to hold m_newConfig
	m_lastConfig = m_newConfig;
}

bool SkeletonConfiguration::HotSwapDim(int baseSlotIndex, int meshId, int numBaseSlots, bool changeBaseCfg) {
#ifdef _ClientOutput
	if (numBaseSlots <= 0)
		return false;

	if (baseSlotIndex < 0)
		return false;

	if (changeBaseCfg) {
		if (m_charConfig.getItemCount() == 0) {
			m_charConfig.initBaseItem(numBaseSlots);
		}
	}

	m_newConfig.setItemSlotMeshId(GOB_BASE_ITEM, baseSlotIndex, meshId);
	m_newConfig.setItemSlotMaterialId(GOB_BASE_ITEM, baseSlotIndex, 0);
	if (changeBaseCfg) {
		m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, baseSlotIndex, meshId);
		m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, baseSlotIndex, 0);
		m_charConfig.setItemSlotColor(GOB_BASE_ITEM, baseSlotIndex, -1.0, -1.0, -1.0);
	}

	return true;
#else
	return false;
#endif
}

void SkeletonConfiguration::SetEquippableCharConfigBlock(const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId, int numBaseSlots) {
	if (m_charConfig.getItemCount() == 0) {
		m_charConfig.initBaseItem(numBaseSlots);
		m_newConfig.initBaseItem(numBaseSlots);
	}
	m_charConfig.setItemSlotMeshId(glidOrBase, slot, meshId);
	m_charConfig.setItemSlotMaterialId(glidOrBase, slot, matlId);
}

BOOL SkeletonConfiguration::HotSwapMat(int baseSlot, int newCfg, int matlId, BOOL changeBaseCfg) {
#ifdef _ClientOutput

	if (newCfg < 0)
		return FALSE;

	if (baseSlot < 0)
		return FALSE;

	m_newConfig.setItemSlotMaterialId(GOB_BASE_ITEM, baseSlot, matlId, false);

	if (changeBaseCfg)
		m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, baseSlot, matlId);

	return TRUE;
#else
	return FALSE;
#endif
}

BOOL SkeletonConfiguration::HotSwapMat(
	int baseSlot,
	int newCfg,
	const CMaterialObject* material,
	RuntimeSkeleton* pRS,
	CAlterUnitDBObjectList* alternateConfigurations) {
#ifdef _ClientOutput

	if (!pRS)
		return FALSE;

	RealizeSlotConfig(pRS, baseSlot, alternateConfigurations);

	if (pRS->getSkinnedMeshCount() == 0)
		return FALSE;

	if (newCfg < 0)
		return FALSE;

	if (baseSlot < 0)
		return FALSE;

	if (!alternateConfigurations)
		return FALSE;

	POSITION alPos = alternateConfigurations->FindIndex(baseSlot);
	if (!alPos)
		return FALSE;

	CAlterUnitDBObject* altPtr = (CAlterUnitDBObject*)alternateConfigurations->GetAt(alPos);
	if (!altPtr || !altPtr->m_configurations)
		return FALSE;

	POSITION curCfgPos = altPtr->m_configurations->FindIndex(newCfg);
	if (!curCfgPos)
		return FALSE;

	CDeformableMesh* mesh = pRS->getSkinnedMesh(baseSlot);
	CDeformableMesh* refMesh = static_cast<CDeformableMesh*>(altPtr->m_configurations->GetAt(curCfgPos));

	if (mesh && refMesh) {
		if (!refMesh->m_supportedMaterials) {
			return FALSE;
		}

		if (mesh->m_material) {
			delete mesh->m_material;
			mesh->m_material = NULL;
		}

		material->Clone(&mesh->m_material);

		mesh->m_currentMatInUse = -1;

		MeshRM::Instance()->CreateReMaterial(mesh->m_material);

		m_newConfig.setItemSlotMaterialId(GOB_BASE_ITEM, baseSlot, -1, false); // -1 means don't screw with this material in DimSwap()
		return TRUE;
	}

	return FALSE;
#else
	return FALSE;
#endif
}

BOOL SkeletonConfiguration::HotSwapRGB(int deformableIndex, float r, float g, float b, RuntimeSkeleton* pRS) {
	if ((r < 0 || g < 0 || b < 0 || r > 1 || g > 1 || b > 1) || (deformableIndex < 0) || !pRS || deformableIndex >= pRS->getSkinnedMeshCount()) {
		return FALSE;
	}

	CDeformableMesh* tarMesh = pRS->getSkinnedMesh(deformableIndex);
	if (tarMesh == nullptr) {
		return FALSE;
	}

	tarMesh->m_material->m_RGBOverride = true;
	tarMesh->m_material->m_useMaterial.Ambient.r = r;
	tarMesh->m_material->m_useMaterial.Ambient.g = g;
	tarMesh->m_material->m_useMaterial.Ambient.b = b;
	tarMesh->m_material->m_useMaterial.Diffuse.r = r;
	tarMesh->m_material->m_useMaterial.Diffuse.g = g;
	tarMesh->m_material->m_useMaterial.Diffuse.b = b;

	m_charConfig.setItemSlotColor(GOB_BASE_ITEM, deformableIndex, r, g, b);

	pRS->setMeshSortValid(false);

	return TRUE;
}

// It's probably worth seperating this CAlterUnitDBObject stuff into a seperate file.
CAlterUnitDBObject::CAlterUnitDBObject() {
	m_name = "";
	m_configurations = NULL;
	m_meshCount = 0;
	m_meshNames = NULL;
	m_loadedfoldername.Format("");
}

//! \brief create data for m_meshNames from CDeformableMesh:m_dimName
void CAlterUnitDBObject::GenerateMeshNames() {
	if (!m_configurations)
		return;

	if (m_meshNames != NULL)
		delete[] m_meshNames;
	m_meshNames = new CStringA[m_configurations->GetCount()];

	int meshIdx = 0;
	for (POSITION pos = m_configurations->GetHeadPosition(); pos != NULL; meshIdx++) {
		CDeformableMesh* dMesh = (CDeformableMesh*)m_configurations->GetNext(pos);
		if (dMesh) {
			m_meshNames[meshIdx].Format("%03d%s", meshIdx, dMesh->m_dimName);
			m_meshNames[meshIdx].Replace(" ", "");
			m_meshNames[meshIdx].Replace("-", "");
			m_meshNames[meshIdx].Replace("_", "");
		} else {
			m_meshNames[meshIdx].Format("NULL");
		}
	}
}

void CAlterUnitDBObject::SafeDelete() {
	if (m_configurations) {
		m_configurations->SafeDelete();
		delete m_configurations;
		m_configurations = 0;
	}
	if (m_meshNames) {
		delete[] m_meshNames;
		m_meshNames = 0;
	}
}

void CAlterUnitDBObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(4);
		ASSERT(ar.GetObjectSchema() == 4);
		ar << m_configurations;
		ar << m_name;
		ar << m_meshCount;
		for (int i = 0; i < m_meshCount; i++) {
			ar << m_meshNames[i];
		}
		ar << false;
	} else {
		int version = ar.GetObjectSchema();
		ar >> m_configurations;

		if (version > 1) {
			ar >> m_name;
		}
		if (version > 2) {
			ar >> m_meshCount;
			m_meshNames = new CStringA[m_meshCount];
			for (int i = 0; i < m_meshCount; i++) {
				ar >> m_meshNames[i];
			}
		} else {
			m_meshCount = m_configurations->GetCount();
			GenerateMeshNames();
		}
		if (version > 3) {
			bool temp;
			ar >> temp;
		}
	}
}

void CAlterUnitDBObjectList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAlterUnitDBObject* dPtr = (CAlterUnitDBObject*)GetNext(posLoc);
		dPtr->SafeDelete();
		delete dPtr;
		dPtr = 0;
	}
	RemoveAll();
}

void SkeletonConfiguration::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_charConfig);
		pMemSizeGadget->AddObject(m_lastConfig);
		pMemSizeGadget->AddObject(m_meshSlots);
		pMemSizeGadget->AddObject(m_newConfig);
	}
}

void CAlterUnitDBObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_configurations);
		pMemSizeGadget->AddObject(m_loadedfoldername);
		pMemSizeGadget->AddObject(m_meshNames);
		pMemSizeGadget->AddObject(m_name);
	}
}

void CAlterUnitDBObjectList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CAlterUnitDBObject* pCAlterUnitDBObject = static_cast<const CAlterUnitDBObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCAlterUnitDBObject);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CAlterUnitDBObjectList, CObject)
IMPLEMENT_SERIAL_SCHEMA(CAlterUnitDBObject, CObject)

} // namespace KEP
