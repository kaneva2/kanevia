///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "KEPCommon.h"

#if (DRF_DIRECT_SHOW_MESH == 1)

#include <DirectShowEXMesh.h>
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "CStateManagementClass.h"
#include "TextureDatabase.h"

#include "..\..\RenderEngine\ReD3DX9.h"
#include "..\..\RenderEngine\Utils\ReUtils.h"
#include "..\..\common\include\IMemSizeGadget.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

// static
std::shared_ptr<DirectShowEXMesh> DirectShowEXMesh::Create(
	CStateManagementObj* stateManager,
	TextureDatabase* textureDatabase,
	DownloadManager* dlMgr,
	LPDIRECT3DDEVICE9 dev,
	const MediaParams& mediaParams,
	uint32_t width, uint32_t height,
	bool loop,
	int32_t x, int32_t y) {
	// Flash Globally Enabled ?
	if (!GetEnabledGlobal()) {
		LogError("DIRECTSHOW DISABLED");
		return nullptr;
	}

	// Create New DirectShow Mesh With Media Params
	std::shared_ptr<DirectShowEXMesh> pDSXM(new DirectShowEXMesh(dlMgr, stateManager, textureDatabase), [](DirectShowEXMesh* p) { delete p; });
	if (!DirectShowMesh::Create(pDSXM.get(), dev, mediaParams, width, height, loop, x, y)) {
		LogInstance("Instance");
		LogError("CreateAndAddToMeshes() FAILED - " << mediaParams.ToStr(true));
		return nullptr;
	}

	return pDSXM;
}

bool DirectShowEXMesh::NewReMaterialPtr(ReD3DX9DeviceState* devState) {
	if (!devState)
		return false;

	ReRenderStateData data;
	data.Mat.Diffuse.x = 1.f;
	data.Mat.Diffuse.y = 1.f;
	data.Mat.Diffuse.z = 1.f;
	data.Mat.Diffuse.w = 1.f;
	data.Mat.Ambient.x = 1.f;
	data.Mat.Ambient.y = 1.f;
	data.Mat.Ambient.z = 1.f;
	data.Mat.Ambient.w = 1.f;
	data.Mat.Specular.x = .4f;
	data.Mat.Specular.y = .4f;
	data.Mat.Specular.z = .4f;
	data.Mat.Specular.w = .0f;
	data.Mat.Emissive.x = 1.f;
	data.Mat.Emissive.y = 1.f;
	data.Mat.Emissive.z = 1.f;
	data.Mat.Emissive.w = 0.f;
	data.Mat.Power = 10.f;

	data.Blend.Srcblend = 9;
	data.Blend.Dstblend = 2;

	data.ColorSrc = devState->GetRenderState()->GetPreLighting() ? ReColorSource::VERTEX : ReColorSource::MATERIAL;

	data.Mode.Alphaenable = 0;
	data.Mode.Zenable = 1;
	data.Mode.Shademode = 0;
	data.Mode.Zwriteenable = 1;
	data.Mode.Alphatestenable = 0;
	data.Mode.CullMode = D3DCULL_CCW;
	data.Mode.ZBias = 0;

	data.MatHash = ReHash((BYTE*)&data.Mat, sizeof(ReMatProps), 0);
	data.ModeHash = ReHash((BYTE*)&data.Mode, sizeof(ReModeProps), 0);
	data.BlendHash = ReHash((BYTE*)&data.Blend, sizeof(ReBlendProps), 0);

	data.BlendHash &= 0x7fffffff;

	for (UINT i = 0; i < 4; i++) {
		data.TexPtr[i] = NULL;
		data.ResPtr[i] = NULL;

		data.Tex[i].ColorOp = D3DTOP_DISABLE;
		data.Tex[i].AlphaOp = D3DTOP_DISABLE;
		data.Tex[i].ColorArg1 = D3DTA_TEXTURE;
		data.Tex[i].ColorArg2 = D3DTA_CURRENT;
	}

	data.NumTexStages = 1;
	data.Tex[0].ColorOp = D3DTOP_SELECTARG1;

	data.TexHash[0] = ReHash((BYTE*)&data.Tex[0], sizeof(ReTexProps), 0);

	m_ReMat = ReMaterialPtr(new ReD3DX9Material(devState, &data));

	SetTexture(m_ReMat);

	return true;
}

void DirectShowEXMesh::ScrapeTexture(ReD3DX9DeviceState* devState) {
	if (!devState || !GetEnabledGlobal())
		return;

	// DRF - Limit 100 Scrapes/Sec
	if (m_texTimer.ElapsedMs() < (1000.0 / 100.0))
		return;
	m_texTimer.Reset();

	// Scrape Texture & Update Mesh
	if (!m_ReMat)
		NewReMaterialPtr(devState);
	UpdateTexture();
	if (!m_ReMat)
		return;
	m_ReMat->Update(GetTextureShared(), 0);
}

int DirectShowEXMesh::Render(LPDIRECT3DDEVICE9 pd3dDevice, CEXMeshObj* mesh, int vertexType) {
	if (!GetEnabledGlobal())
		return 0;

	int ret = 0;

	if (m_renderer) {
		if (GetTexture() != 0 && m_startedPlaying) {
			if (false) { // do an extra msg loop?
				// Process Message Loop Until Empty
				FOREVER {
					// Message Waiting ?
					MSG msg;
					BOOL bRet = ::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
					if (bRet == -1) {
						LogError("PeekMessage() FAILED");
						break;
					}
					if (bRet == 0)
						break;

					// Dispatch Message
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}

			TimeMs now = fTime::TimeMs();
			static TimeMs m_lastRender = 0;
			static TimeMs m_waitMs = 10;

			// only get the image every so often
			if ((now - m_lastRender) > m_waitMs)
				m_lastRender = now;

			// copied from DrawTexturedQuad
			m_stateManager->SetMaterial(&mesh->m_materialObject->m_useMaterial);

			pd3dDevice->SetTexture(0, GetTexture()); // set texture

			m_stateManager->SetDiffuseSource(mesh->m_abVertexArray.m_advancedFixedMode);
			m_stateManager->SetVertexType(mesh->m_abVertexArray.m_uvCount);
			ret = mesh->m_abVertexArray.Render(pd3dDevice, vertexType);

			m_stateManager->SetTextureState(-1, 0, m_textureDatabase);
		}
	}

	return ret;
}

void DirectShowEXMesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
		m_stateManager			// CStateManagementObj 
		m_textureDatabase		// TextureDatabase* 
		---------------------------------------------------------------------*/
	}
}

} // namespace KEP

#endif
