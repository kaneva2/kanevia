///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CQuestJournal.h"
#include "CInventoryClass.h"
#include "CGlobalInventoryClass.h"

namespace KEP {

CQJRequiredItem::CQJRequiredItem() {
	m_glid = -1;
	m_quanity = 0;
}

void CQJRequiredItem::Clone(CQJRequiredItem** clone) {
	CQJRequiredItem* copyLocal = new CQJRequiredItem();

	copyLocal->m_glid = m_glid;
	copyLocal->m_quanity = m_quanity;

	*clone = copyLocal;
}

void CQJRequiredItem::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_glid
		   << m_quanity
		   << m_itemName;

	} else {
		ar >> m_glid >> m_quanity >> m_itemName;
	}
}

void CQJRequiredItemList::Clone(CQJRequiredItemList** clone) {
	CQJRequiredItemList* copyLocal = new CQJRequiredItemList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		// account list
		CQJRequiredItem* skPtr = (CQJRequiredItem*)GetNext(posLoc);
		CQJRequiredItem* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}

void CQJRequiredItemList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CQJRequiredItem* spnPtr = (CQJRequiredItem*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}

	RemoveAll();
}

CQJRequiredItem* CQJRequiredItemList::GetByID(int id) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CQJRequiredItem* spnPtr = (CQJRequiredItem*)GetNext(posLoc);
		if (spnPtr->m_glid == id) {
			return spnPtr;
		}
	}

	return NULL;
}

CQuestJournalObj::CQuestJournalObj() {
	m_requiredItems = NULL;
	m_currentProgression = 0;
	m_status = 0;
	m_mobToHuntID = 0;
	m_maxDropCount = 0;
	m_dropGlid = 0;
	m_dropPercentage = 0;
	m_lastSelectionIndex = 0;
	m_autoComplete = TRUE; // for hunting quests
	m_skillExpType = 0;
	m_expBonus = 0.0f;
	m_thisGainCap = 0.0f;
	m_undeletable = FALSE;
}

void CQuestJournalObj::SafeDelete() {
	if (m_requiredItems) {
		m_requiredItems->SafeDelete();
		delete m_requiredItems;
		m_requiredItems = 0;
	}
}

void CQuestJournalObj::Clone(CQuestJournalObj** clone) {
	CQuestJournalObj* copyLocal = new CQuestJournalObj();

	copyLocal->m_questDescription = m_questDescription;
	copyLocal->m_npcName = m_npcName;
	copyLocal->m_keyStatement = m_keyStatement;
	copyLocal->m_currentProgression = m_currentProgression;
	copyLocal->m_mobToHuntID = m_mobToHuntID;
	copyLocal->m_maxDropCount = m_maxDropCount;
	copyLocal->m_dropGlid = m_dropGlid;
	copyLocal->m_dropPercentage = m_dropPercentage;
	copyLocal->m_lastSelectionIndex = m_lastSelectionIndex;
	copyLocal->m_autoComplete = m_autoComplete; //for hunting quests
	copyLocal->m_skillExpType = m_skillExpType;
	copyLocal->m_expBonus = m_expBonus;
	copyLocal->m_thisGainCap = m_thisGainCap;
	copyLocal->m_undeletable = m_undeletable;

	if (m_requiredItems) {
		m_requiredItems->Clone(&copyLocal->m_requiredItems);
	}

	*clone = copyLocal;
}

void CQuestJournalObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CStringA reservedString = "";
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(3);
		ASSERT(ar.GetObjectSchema() == 3);

		ar << m_requiredItems
		   << m_questDescription
		   << m_npcName
		   << m_keyStatement
		   << m_currentProgression
		   << m_status;

		ar << m_mobToHuntID
		   << m_maxDropCount
		   << m_dropGlid
		   << m_dropPercentage
		   << m_lastSelectionIndex
		   << m_undeletable
		   << reserved
		   << reserved;

		ar << m_autoComplete
		   << m_skillExpType
		   << m_expBonus
		   << m_thisGainCap
		   << reserved
		   << reserved
		   << reserved
		   << reserved;

	} else {
		int version = ar.GetObjectSchema();

		ar >> m_requiredItems >> m_questDescription >> m_npcName >> m_keyStatement >> m_currentProgression >> m_status;

		m_autoComplete = TRUE; // for hunting quests
		m_skillExpType = 0;
		m_expBonus = 0.0f;
		m_thisGainCap = 0.0f;

		if (version == 2) {
			ar >> m_mobToHuntID >> m_maxDropCount >> m_dropGlid >> m_dropPercentage >> m_lastSelectionIndex >> reserved >> reserved >> reserved;
		} else if (version == 3) {
			ar >> m_mobToHuntID >> m_maxDropCount >> m_dropGlid >> m_dropPercentage >> m_lastSelectionIndex >> m_undeletable >> reserved >> reserved;

			ar >> m_autoComplete >> m_skillExpType >> m_expBonus >> m_thisGainCap >> reserved >> reserved >> reserved >> reserved;
		}
	}
}

BOOL CQuestJournalList::LogQuest(CStringA description,
	CInventoryObjList* requiredItems,
	CStringA m_npcName,
	CStringA m_keyStatement,
	CGlobalInventoryObjList* gllist,
	BOOL succeeded,
	int curVisibleSelection,
	int logOption,
	int mobToHuntID,
	int maxDropCount,
	int dropGlid,
	int dropPercentage,
	BOOL autoComplete,
	int skillExpType,
	float expBonus,
	float thisGainCap,
	BOOL isProgressive,
	BOOL undeletable,
	CStringA closeJournalEntryByName) {
	// existing quest...
	CQuestJournalObj* existingPtr = GetBySeer(m_npcName);

	if (!succeeded) {
		if (closeJournalEntryByName.GetLength() > 0 && closeJournalEntryByName != "Reserved") {
			DeleteEntryByNpcName(closeJournalEntryByName);
		}
	}

	if (existingPtr) {
		// existing quest
		if (succeeded) {
			// progress to next quest
			if (curVisibleSelection == existingPtr->m_currentProgression && isProgressive == TRUE) {
				// success will eman somthing because this is a undone quest
				existingPtr->m_currentProgression = existingPtr->m_currentProgression + 1;
				existingPtr->m_status = 1;
			} // end success will eman somthing because this is a undone quest
			else {
				existingPtr->m_status = 1;
			}
		} // end progress to next quest
		else {
			// if(existingPtr->m_currentProgression <= curVisibleSelection)
			// {//dont overwrite
			existingPtr->m_keyStatement = m_keyStatement;
			existingPtr->m_npcName = m_npcName;
			existingPtr->m_questDescription = description;
			existingPtr->m_mobToHuntID = mobToHuntID;
			existingPtr->m_maxDropCount = maxDropCount;
			existingPtr->m_dropGlid = dropGlid;
			existingPtr->m_dropPercentage = dropPercentage;

			existingPtr->m_autoComplete = autoComplete;
			existingPtr->m_skillExpType = skillExpType;
			existingPtr->m_expBonus = expBonus;
			existingPtr->m_thisGainCap = thisGainCap;
			existingPtr->m_undeletable = undeletable;

			if (curVisibleSelection > existingPtr->m_lastSelectionIndex) {
				existingPtr->m_lastSelectionIndex = curVisibleSelection;
			}

			if (existingPtr->m_requiredItems) {
				existingPtr->m_requiredItems->SafeDelete();
				BuildRequiredList(existingPtr, requiredItems, gllist, logOption);
			}

			existingPtr->m_status = 0; // revert status to new objective
			MakeMostRecentBySeer(m_npcName);

			// }//end done over write
		}

		return TRUE;
	} // end existing quest

	// new quest....
	if (GetCount() > 10) {
		POSITION posLast = NULL;
		for (POSITION posLoc = GetTailPosition(); (posLast = posLoc) != NULL;) {
			CQuestJournalObj* spnPtr = (CQuestJournalObj*)GetPrev(posLoc);
			if (spnPtr->m_undeletable) {
				continue;
			}

			spnPtr->SafeDelete();
			delete spnPtr;
			spnPtr = 0;
			RemoveAt(posLast);
			break;
		}
	}

	CQuestJournalObj* newPtr = new CQuestJournalObj();
	newPtr->m_requiredItems = new CQJRequiredItemList();
	newPtr->m_keyStatement = m_keyStatement;
	newPtr->m_npcName = m_npcName;
	newPtr->m_questDescription = description;
	newPtr->m_mobToHuntID = mobToHuntID;
	newPtr->m_maxDropCount = maxDropCount;
	newPtr->m_dropGlid = dropGlid;
	newPtr->m_dropPercentage = dropPercentage;
	newPtr->m_lastSelectionIndex = curVisibleSelection;
	newPtr->m_autoComplete = autoComplete;
	newPtr->m_skillExpType = skillExpType;
	newPtr->m_expBonus = expBonus;
	newPtr->m_thisGainCap = thisGainCap;
	BuildRequiredList(newPtr, requiredItems, gllist, logOption);

	if (succeeded) {
		newPtr->m_status = 1;
		newPtr->m_currentProgression = newPtr->m_currentProgression + 1;
	}

	AddHead(newPtr);
	return TRUE;
}

BOOL CQuestJournalList::BuildRequiredList(CQuestJournalObj* newPtr,
	CInventoryObjList* requiredItems,
	CGlobalInventoryObjList* gllist,
	int logOption) {
	// build required list
	if (requiredItems) {
		for (POSITION posLoc = requiredItems->GetHeadPosition(); posLoc != NULL;) {
			CInventoryObj* invPtr = (CInventoryObj*)requiredItems->GetNext(posLoc);
			if (!invPtr->m_itemData) {
				continue;
			}

			if (invPtr->m_itemData->m_permanent) {
				continue;
			}

			CQJRequiredItem* existingItem = newPtr->m_requiredItems->GetByID(invPtr->m_itemData->m_globalID);
			if (existingItem) {
				if (invPtr->getQty() == 0) {
					existingItem->m_quanity += 1;
				} else {
					existingItem->m_quanity += invPtr->getQty();
				}
			} else {
				CGlobalInventoryObj* globalPtr = gllist->GetByGLID(invPtr->m_itemData->m_globalID);
				if (globalPtr) {
					CQJRequiredItem* newItem = new CQJRequiredItem();
					newItem->m_glid = invPtr->m_itemData->m_globalID;
					newItem->m_quanity = invPtr->getQty();
					if (newItem->m_quanity == 0) {
						newItem->m_quanity = 1;
					}

					if (logOption == 4) {
						newItem->m_itemName = "?????";
					} else {
						newItem->m_itemName = globalPtr->m_itemData->m_itemName;
					}

					newPtr->m_requiredItems->AddTail(newItem);
				}
			}
		}
	}

	if (newPtr->m_requiredItems) {
		for (POSITION posLoc = newPtr->m_requiredItems->GetHeadPosition(); posLoc != NULL;) {
			CQJRequiredItem* existingItem = (CQJRequiredItem*)newPtr->m_requiredItems->GetNext(posLoc);

			std::stringstream strBuilder;
			strBuilder << "     (" << existingItem->m_quanity << ")" << existingItem->m_itemName;
			existingItem->m_itemName = strBuilder.str().c_str();
		}
	}

	return TRUE;
}

CQuestJournalObj* CQuestJournalList::GetBySeer(CStringA seerName) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CQuestJournalObj* spnPtr = (CQuestJournalObj*)GetNext(posLoc);
		if (seerName == spnPtr->m_npcName) {
			return spnPtr;
		}
	}

	return NULL;
}

CQuestJournalObj* CQuestJournalList::GetMostRecentUncompleted() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CQuestJournalObj* spnPtr = (CQuestJournalObj*)GetNext(posLoc);
		if (spnPtr->m_status == 0) {
			return spnPtr;
		}
	}

	return NULL;
}

BOOL CQuestJournalList::DeleteEntryByNpcName(CStringA name) {
	POSITION posLast = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CQuestJournalObj* spnPtr = (CQuestJournalObj*)GetNext(posLoc);
		if (spnPtr->m_npcName == name) {
			spnPtr->SafeDelete();
			delete spnPtr;
			spnPtr = 0;
			RemoveAt(posLast);
			return TRUE;
		}
	}

	return FALSE;
}

void CQuestJournalList::MakeMostRecentBySeer(CStringA seerName) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CQuestJournalObj* spnPtr = (CQuestJournalObj*)GetNext(posLoc);
		if (seerName == spnPtr->m_npcName) {
			RemoveAt(posLast);
			AddHead(spnPtr);
			return;
		}
	}
}

void CQuestJournalList::RemoveBySeer(CStringA seerName) {
	POSITION posLast = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CQuestJournalObj* spnPtr = (CQuestJournalObj*)GetNext(posLoc);
		if (seerName == spnPtr->m_npcName) {
			spnPtr->SafeDelete();
			delete spnPtr;
			spnPtr = 0;
			RemoveAt(posLast);
			return;
		}
	}
}

void CQuestJournalList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CQuestJournalObj* spnPtr = (CQuestJournalObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}

	RemoveAll();
}

void CQuestJournalList::Clone(CQuestJournalList** clone) {
	CQuestJournalList* copyLocal = new CQuestJournalList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		// account list
		CQuestJournalObj* skPtr = (CQuestJournalObj*)GetNext(posLoc);
		CQuestJournalObj* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}

CQuestJournalObj* CQuestJournalList::GetUncompletedQuestJournalEntryByTarget(int aiCfgId) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		// account list
		CQuestJournalObj* skPtr = (CQuestJournalObj*)GetNext(posLoc);
		if (skPtr->m_mobToHuntID > 0) {
			if (skPtr->m_status == 0) {
				if (aiCfgId == skPtr->m_mobToHuntID) {
					return skPtr;
				}
			}
		}
	}

	return NULL;
}

IMPLEMENT_SERIAL_SCHEMA(CQuestJournalObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CQuestJournalList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CQJRequiredItem, CObject)
IMPLEMENT_SERIAL_SCHEMA(CQJRequiredItemList, CObList)

BEGIN_GETSET_IMPL(CQJRequiredItem, IDS_QJREQUIREDITEM_NAME)
GETSET_RANGE(m_glid, gsInt, GS_FLAGS_DEFAULT, 0, 0, QJREQUIREDITEM, GLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_quanity, gsInt, GS_FLAGS_DEFAULT, 0, 0, QJREQUIREDITEM, QUANITY, INT_MIN, INT_MAX)
GETSET_MAX(m_itemName, gsCString, GS_FLAGS_DEFAULT, 0, 0, QJREQUIREDITEM, ITEMNAME, STRLEN_MAX)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(CQuestJournalObj, IDS_QUESTJOURNALOBJ_NAME)
GETSET_OBLIST_PTR(m_requiredItems, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, REQUIREDITEMS)
GETSET_MAX(m_questDescription, gsCString, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, QUESTDESCRIPTION, STRLEN_MAX)
GETSET_MAX(m_npcName, gsCString, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, NPCNAME, STRLEN_MAX)
GETSET_MAX(m_keyStatement, gsCString, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, KEYSTATEMENT, STRLEN_MAX)
GETSET_RANGE(m_currentProgression, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, CURRENTPROGRESSION, INT_MIN, INT_MAX)
GETSET_RANGE(m_status, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, STATUS, INT_MIN, INT_MAX)
GETSET_RANGE(m_mobToHuntID, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, MOBTOHUNTID, INT_MIN, INT_MAX)
GETSET_RANGE(m_maxDropCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, MAXDROPCOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_dropGlid, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, DROPGLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_dropPercentage, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, DROPPERCENTAGE, INT_MIN, INT_MAX)
GETSET_RANGE(m_lastSelectionIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, LASTSELECTIONINDEX, INT_MIN, INT_MAX)
GETSET(m_undeletable, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, UNDELETABLE)
GETSET(m_autoComplete, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, AUTOCOMPLETE)
GETSET_RANGE(m_skillExpType, gsInt, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, SKILLEXPTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_expBonus, gsFloat, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, EXPBONUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_thisGainCap, gsFloat, GS_FLAGS_DEFAULT, 0, 0, QUESTJOURNALOBJ, THISGAINCAP, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP