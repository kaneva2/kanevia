///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "KEPHelpers.h"
#include "KEPException.h"
#include "Audit.h"
#include "CSecureTradeClass.h"
#include "CPlayerClass.h"
#include "IGameState.h"
#include <PassList.h>
#include "CCurrencyClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "TransactionType.h"

namespace KEP {

CSecureTradeObj::CSecureTradeObj() {
	m_hostInvenOffer = NULL;
	m_clientInvenOffer = NULL;
	m_uniqueTradeID = 0;
	m_hostNetworkID = 0;
	m_clientNetworkID = 0;
	m_hostOfferCompleted = FALSE;
	m_clientOfferCompleted = FALSE;
	m_hostAccept = FALSE;
	m_clientAccept = FALSE;
	m_hostCashOffer = 0;
	m_clientCashOffer = 0;
	m_timeStamp = fTime::TimeMs();
}

void CSecureTradeObj::SafeDelete() {
	if (m_hostInvenOffer) {
		m_hostInvenOffer->SafeDelete();
		delete m_hostInvenOffer;
		m_hostInvenOffer = 0;
	}
	if (m_clientInvenOffer) {
		m_clientInvenOffer->SafeDelete();
		delete m_clientInvenOffer;
		m_clientInvenOffer = 0;
	}
}

void CSecureTradeObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		tradePtr->SafeDelete();
		delete tradePtr;
		tradePtr = 0;
	}
	RemoveAll();
}

int CSecureTradeObjList::GetUniqueTradeID() {
	int trace = 1;
	BOOL done = FALSE;
	while (!done) {
		done = TRUE;
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //runtime trade loop
			CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
			if (tradePtr->m_uniqueTradeID == trace) {
				done = FALSE;
				break;
			}
		} //end runtime trade loop
		if (!done) {
			trace++;
		}
	}
	return trace;
}

BOOL CSecureTradeObjList::InitiateTrade(int hostNetworkID,
	int clientNetworkID,
	CPlayerObjectList* runtimeServerPlayerDB) {
	//verify players
	CPlayerObject* hostPtr = runtimeServerPlayerDB->GetPlayerObjectByNetTraceId(hostNetworkID);
	CPlayerObject* clientPtr = runtimeServerPlayerDB->GetPlayerObjectByNetTraceId(clientNetworkID);
	if (hostPtr == NULL || clientPtr == NULL)
		return FALSE;

	CSecureTradeObj* newTradingSession = new CSecureTradeObj();
	newTradingSession->m_hostInvenOffer = new CInventoryObjList();
	newTradingSession->m_clientInvenOffer = new CInventoryObjList();
	//get unique id
	newTradingSession->m_uniqueTradeID = GetUniqueTradeID();
	newTradingSession->m_hostNetworkID = hostNetworkID;
	newTradingSession->m_clientNetworkID = clientNetworkID;
	AddTail(newTradingSession);
	return TRUE;
}

BOOL CSecureTradeObjList::IsPlayerInvolvedInTrade(int id) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //primary trade DB loop
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		if (tradePtr->m_clientNetworkID == id || tradePtr->m_hostNetworkID == id)
			return TRUE;
	}
	return FALSE;
}

BOOL CSecureTradeObjList::AddCashToOffer(int id,
	int cashAmount,
	int* fowardInfoToID) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //primary trade DB loop
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		if (tradePtr->m_clientNetworkID == id) {
			tradePtr->m_clientCashOffer = cashAmount;
			*fowardInfoToID = tradePtr->m_hostNetworkID;
			ClearTradeAccepts(id);
			return TRUE;
		}
		if (tradePtr->m_hostNetworkID == id) {
			tradePtr->m_hostCashOffer = cashAmount;
			*fowardInfoToID = tradePtr->m_clientNetworkID;
			ClearTradeAccepts(id);
			return TRUE;
		}
	}
	return FALSE;
}

void CSecureTradeObjList::AddItemsToOffer(CInventoryObjList* inv, CHAR_ITEM_DATA* itemArray, int itemCount,
	CGlobalInventoryObjList* globalInventoryBD) {
	inv->SafeDelete(); // empty existing list
	for (int itemLoop = 0; itemLoop < itemCount; itemLoop++) {
		// item compile to secure trade object
		CGlobalInventoryObj* glPtr = (CGlobalInventoryObj*)globalInventoryBD->GetByGLID(itemArray[itemLoop].m_GLID);
		if (glPtr) {
			inv->AddInventoryItem(itemArray[itemLoop].m_quanity, FALSE, glPtr->m_itemData, IT_NORMAL); // can't trade gifts
		}
	}
}

BOOL CSecureTradeObjList::AddItemsToOffer(int fromNetAssignedId, CHAR_ITEM_DATA* itemArray, int itemCount,
	CGlobalInventoryObjList* globalInventoryBD, int* fowardInfoToID) {
	//primary trade DB loop
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		if (tradePtr->m_clientNetworkID == fromNetAssignedId) {
			AddItemsToOffer(tradePtr->m_clientInvenOffer, itemArray, itemCount, globalInventoryBD);

			*fowardInfoToID = tradePtr->m_hostNetworkID;
			ClearTradeAccepts(tradePtr->m_hostNetworkID);
			return TRUE;
		}

		if (tradePtr->m_hostNetworkID == fromNetAssignedId) {
			AddItemsToOffer(tradePtr->m_hostInvenOffer, itemArray, itemCount, globalInventoryBD);

			*fowardInfoToID = tradePtr->m_clientNetworkID;
			ClearTradeAccepts(tradePtr->m_clientNetworkID);
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CSecureTradeObjList::AddItemToOffer(int id,
	int gl_id,
	int quanity,
	CGlobalInventoryObjList* globalInventoryBD,
	int* fowardInfoToID) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //primary trade DB loop
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		if (tradePtr->m_clientNetworkID == id) {
			CGlobalInventoryObj* glPtr = (CGlobalInventoryObj*)globalInventoryBD->GetByGLID(gl_id);
			if (glPtr) {
				tradePtr->m_clientInvenOffer->AddInventoryItem(quanity, FALSE, glPtr->m_itemData, IT_NORMAL); // can't trade gifts
				*fowardInfoToID = tradePtr->m_hostNetworkID;
				ClearTradeAccepts(id);
			}
			return TRUE;
		}
		if (tradePtr->m_hostNetworkID == id) {
			CGlobalInventoryObj* glPtr = (CGlobalInventoryObj*)globalInventoryBD->GetByGLID(gl_id);
			if (glPtr) {
				tradePtr->m_hostInvenOffer->AddInventoryItem(quanity, FALSE, glPtr->m_itemData, IT_NORMAL); // can't trade gifts
				*fowardInfoToID = tradePtr->m_clientNetworkID;
				ClearTradeAccepts(id);
			}
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CSecureTradeObjList::CancelTransaction(int id, int* notifyIDofCancelation) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) { //primary trade DB loop
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		if (tradePtr->m_clientNetworkID == id) {
			*notifyIDofCancelation = tradePtr->m_hostNetworkID;
			tradePtr->SafeDelete();
			delete tradePtr;
			tradePtr = 0;
			RemoveAt(posLast);

			return TRUE;
		}
		if (tradePtr->m_hostNetworkID == id) {
			*notifyIDofCancelation = tradePtr->m_clientNetworkID;
			tradePtr->SafeDelete();
			delete tradePtr;
			tradePtr = 0;
			RemoveAt(posLast);

			return TRUE;
		}
	}
	return FALSE;
}

BOOL CSecureTradeObjList::AcceptTradeConditions(int id, int* sendResponseTo) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) { //primary trade DB loop
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		if (tradePtr->m_clientNetworkID == id) {
			tradePtr->m_clientAccept = TRUE;
			if (sendResponseTo != 0) {
				*sendResponseTo = tradePtr->m_hostNetworkID;
			}

			return TRUE;
		}
		if (tradePtr->m_hostNetworkID == id) {
			tradePtr->m_hostAccept = TRUE;
			if (sendResponseTo != 0) {
				*sendResponseTo = tradePtr->m_clientNetworkID;
			}

			return TRUE;
		}
	}
	return FALSE;
}

//Called whenever a new trade offer is made or a change in trade occurs.
//Ensures every must agree on new terms.
void CSecureTradeObjList::ClearTradeAccepts(int id) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) { //primary trade DB loop
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);
		if (tradePtr->m_clientNetworkID == id || tradePtr->m_hostNetworkID == id) {
			tradePtr->m_hostAccept = FALSE;
			tradePtr->m_clientAccept = FALSE;
			break;
		}
	}
}

int CSecureTradeObjList::EvaluateAllTrades(CPlayerObjectList* runtimeServerPlayerDB,
	CPlayerObject*& hostPtr,
	CPlayerObject*& clientPtr,
	int maxItemCount) {
	int ret = TRADE_OK;
	hostPtr = 0;
	clientPtr = 0;

	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		//primary trade DB loop
		CSecureTradeObj* tradePtr = (CSecureTradeObj*)GetNext(posLoc);

		if (tradePtr->m_hostAccept && tradePtr->m_clientAccept) {
			//Both parties happy do exchange
			hostPtr = runtimeServerPlayerDB->GetPlayerObjectByNetTraceId(tradePtr->m_hostNetworkID);
			clientPtr = runtimeServerPlayerDB->GetPlayerObjectByNetTraceId(tradePtr->m_clientNetworkID);

			if (hostPtr == NULL || clientPtr == NULL)
				ret = TRADE_NONE_FOUND;
			else {
				clientPtr->m_interactionStates &= ~TRADING;
				hostPtr->m_interactionStates &= ~TRADING;
			}

			if (ret != TRADE_OK || fTime::ElapsedMs(tradePtr->m_timeStamp) > 1200000) {
				tradePtr->SafeDelete();
				delete tradePtr;
				tradePtr = 0;
				RemoveAt(posLast);
				continue; // silently continue, no notification
			}

			if (!clientPtr->m_inventory->CanTakeObjects(maxItemCount, tradePtr->m_hostInvenOffer->GetCount())) {
				ret = TRADE_CLIENT_OVERLOADED; //client of trade is overloaded
			}

			if (ret == TRADE_OK && !hostPtr->m_inventory->CanTakeObjects(maxItemCount, tradePtr->m_clientInvenOffer->GetCount())) {
				ret = TRADE_HOST_OVERLOADED; //host of trade is overloaded
			}

			//verify host inventory of trade
			for (POSITION hsPos = tradePtr->m_hostInvenOffer->GetHeadPosition(); hsPos != NULL && ret == TRADE_OK;) {
				//host inventory loop
				CInventoryObj* invPtr = (CInventoryObj*)tradePtr->m_hostInvenOffer->GetNext(hsPos);

				if (!hostPtr->m_inventory->CanTradeItem(invPtr->m_itemData->m_globalID, invPtr->getQty())) {
					//invalid inventory detected
					ret = TRADE_HOST_CANT_TRADE_ITEM; //host of trade trying to trade invalid item
					break;
				}

				// make sure client can take item
				if (!PassList::canPlayerAcceptItemTrade(clientPtr->m_passList, invPtr->m_itemData->m_passList) ||
					invPtr->m_itemData->m_defaultArmedItem) {
					ret = TRADE_CLIENT_CANT_ACCEPT_ITEM; //host of trade trying to trade invalid item
					break;
				}
			} //end host inventory loop

			//verify client inventory of trade
			for (POSITION clPos = tradePtr->m_clientInvenOffer->GetHeadPosition(); clPos != NULL && ret == TRADE_OK;) {
				//client inventory loop
				CInventoryObj* invPtr = (CInventoryObj*)tradePtr->m_clientInvenOffer->GetNext(clPos);
				if (!clientPtr->m_inventory->CanTradeItem(invPtr->m_itemData->m_globalID, invPtr->getQty())) {
					//invalid inventory detected
					ret = TRADE_CLIENT_CANT_TRADE_ITEM; //host of trade trying to trade invalid item
					break;
				}

				// make sure client can take item
				if (!PassList::canPlayerAcceptItemTrade(hostPtr->m_passList, invPtr->m_itemData->m_passList)) {
					ret = TRADE_HOST_CANT_ACCEPT_ITEM; //host of trade trying to trade invalid item
					break;
				}

			} //end client inventory loop

			// verify host cash
			if (ret != TRADE_OK ||
				(tradePtr->m_hostCashOffer > 0 &&
					hostPtr->m_cash->m_amount < tradePtr->m_hostCashOffer) ||
				(tradePtr->m_clientCashOffer > 0 &&
					clientPtr->m_cash->m_amount < tradePtr->m_clientCashOffer)) {
				//not enough funds, or other error
				tradePtr->SafeDelete();
				delete tradePtr;
				RemoveAt(posLast);
				if (ret != TRADE_OK)
					return ret;
				else
					continue; // silently cancel on insuff funds?
			}

			//make it this far and its on

			//give client host trade offer

			// do cash side first
			int clientCashChange = tradePtr->m_hostCashOffer - tradePtr->m_clientCashOffer;
			int hostCashChange = tradePtr->m_clientCashOffer - tradePtr->m_hostCashOffer;

			IGameState* state = IGameState::GetState();
			if (state != 0) {
				try {
					IGameState::EUpdateRet updateRet = state->UpdatePlayersCash(clientPtr, clientCashChange, TransactionType::TRADE, hostPtr, hostCashChange);
					if (updateRet != IGameState::UPDATE_OK) {
						Logger logger(Logger::getInstance(L"Database"));
						LOG4CPLUS_WARN(logger, "DB says not enough funds for trade between " << clientPtr->m_charName << " and " << hostPtr->m_charName << " rc = " << updateRet);
						//not enough funds
						tradePtr->SafeDelete();
						tradePtr = 0;
						RemoveAt(posLast);
						continue;
					}
				} catch (KEPException* e) {
					Logger logger(Logger::getInstance(L"Database"));
					LOG4CPLUS_WARN(logger, "DB Error in cash trade between " << clientPtr->m_charName << " and " << hostPtr->m_charName << " :" << e->m_msg);
					e->Delete();

					tradePtr->SafeDelete();
					tradePtr = 0;
					RemoveAt(posLast);
					continue;
				}
			}

			//inventory side
			for (POSITION hsPos = tradePtr->m_hostInvenOffer->GetHeadPosition(); hsPos != NULL;) {
				//host inventory loop
				CInventoryObj* invPtr = (CInventoryObj*)tradePtr->m_hostInvenOffer->GetNext(hsPos);

				CInventoryObj* referenceInvPtr = hostPtr->m_inventory->CanTradeItem(invPtr->m_itemData->m_globalID, invPtr->getQty());
				if (referenceInvPtr) {
					if (hostPtr->m_inventory->TransferInventoryObjsTo(clientPtr->m_inventory,
							invPtr->m_itemData->m_globalID,
							invPtr->getQty(), true, IT_NORMAL)) {
					}
				}
			} //end host inventory loop

			//give host client offer
			//inventory side
			for (POSITION hsPos = tradePtr->m_clientInvenOffer->GetHeadPosition(); hsPos != NULL;) {
				//client inventory loop
				CInventoryObj* invPtr = (CInventoryObj*)tradePtr->m_clientInvenOffer->GetNext(hsPos);

				CInventoryObj* referenceInvPtr = clientPtr->m_inventory->CanTradeItem(invPtr->m_itemData->m_globalID, invPtr->getQty());
				if (referenceInvPtr) {
					if (clientPtr->m_inventory->TransferInventoryObjsTo(hostPtr->m_inventory,
							invPtr->m_itemData->m_globalID, invPtr->getQty(), true, IT_NORMAL)) {
					}
				}

			} //end client inventory loop

			if (state != 0) {
				try {
					state->UpdateTradingPlayers(clientPtr, hostPtr);
				} catch (KEPException* e) {
					Logger logger(Logger::getInstance(L"Database"));
					LOG4CPLUS_WARN(logger, "DB Error in trade between " << clientPtr->m_charName << " and " << hostPtr->m_charName << " :" << e->m_msg);
					e->Delete();
					ret = TRADE_CLIENT_CANT_ACCEPT_ITEM;
				}
			}

			//inform client transaction is conplted and to close menus
			//todo - send code
			tradePtr->SafeDelete();
			delete tradePtr;
			tradePtr = 0;
			RemoveAt(posLast);
			return ret; //quit early and inform
			//end trade
		} //end Both parties happy do exchange

	} //end primary trade DB loop

	clientPtr = 0;
	hostPtr = 0;
	return TRADE_NONE_FOUND; // no completed transactions
}

} // namespace KEP
