///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CStaticMeshObject.h"
#include "CReferenceLibrary.h"
#include "IAssetsDatabase.h"
#include "AssetTree.h"
#include "MaterialPersistence.h"

#ifdef _ClientOutput
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "ReMeshStream.h"
#include "ReMeshCreate.h"
#endif

namespace KEP {

CReferenceObj::CReferenceObj() {
	m_referenceName = "N/A";
	m_lodVisualList = NULL;
	m_collisionModel = NULL;
	m_popoutBox.max.x = 0.0f;
	m_popoutBox.max.y = 0.0f;
	m_popoutBox.max.z = 0.0f;
	m_popoutBox.min.x = 0.0f;
	m_popoutBox.min.y = 0.0f;
	m_popoutBox.min.z = 0.0f;
	m_animModule = NULL;
	m_faceCamera = FALSE;
	m_animationUpdatedThisRound = FALSE;
	m_extLockPoints = NULL;
	m_lockPointCount = 0;
	m_lodRange = 1000.0;
	m_lodLevels = 10;
	m_billboardSz = 0.0f;
	m_seqLoad = 0;
	m_ReMesh = NULL;
	m_externalSource = NULL;
	m_material = NULL;
	m_boundRadius = 0.f;
}

void CReferenceObj::SafeDelete() {
	if (m_lodVisualList) {
		m_lodVisualList->SafeDelete();
		delete m_lodVisualList;
		m_lodVisualList = 0;
	}

	if (m_collisionModel) {
		m_collisionModel->SafeDelete();
		delete m_collisionModel;
		m_collisionModel = 0;
	}

	if (m_animModule) {
		m_animModule->SafeDelete();
		delete m_animModule;
		m_animModule = 0;
	}

	if (m_extLockPoints) {
		delete[] m_extLockPoints;
		m_extLockPoints = 0;
	}

	if (m_material) {
		delete m_material;
		m_material = NULL;
	}
}

void CReferenceObj::InitFromPath(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	ASSERT(FALSE);
	// Moved to CReferenceLibrary::LoadReferenceObjects
}

//! \brief Initialize collision related data based on a EX mesh.
//!
//! Call this function to intialize collision model after m_collisionModel is set to a valid CEXMeshObj
//! Refactored so that caller can have control on where and how a CEXMeshObj is obtained for collision.
//! Original import function call has been moved to the caller.
void CReferenceObj::InitCollisionModel() {
	ASSERT(m_collisionModel != NULL);
	if (m_collisionModel == NULL)
		return;

	m_collisionModel->BakeVertices();

	m_collisionModel->ComputeBoundingVolumes();

	// optimize the mesh objects
	m_collisionModel->OptimizeMesh();

	// locate the matrix by the boundbox
	const ABBOX& bbox = m_collisionModel->GetBoundingBox();
	Vector3f center((bbox.minX + bbox.maxX) * 0.5, (bbox.minY + bbox.maxY) * 0.5, (bbox.minZ + bbox.maxZ) * 0.5);
	m_collisionModel->SetTranslation(center);
} // CReferenceObj..InitCollisionModel

//! \brief Remove and dispose collision model.
void CReferenceObj::DeleteCollisionModel() {
	if (m_collisionModel == NULL)
		return;

	m_collisionModel->SafeDelete();
	delete m_collisionModel;
	m_collisionModel = 0;
}

BOOL CReferenceObj::UpdateLods(BOOL filterTextureCarry /*=FALSE*/) {
	if (!m_lodVisualList)
		return FALSE;

	for (POSITION posLoc = m_lodVisualList->GetHeadPosition(); posLoc != NULL;) {
		CMeshLODObject* meshLod = (CMeshLODObject*)m_lodVisualList->GetNext(posLoc);
		if (meshLod->m_material)
			delete meshLod->m_material;
		meshLod->m_material = 0;
		m_material->Clone(&meshLod->m_material);

		CopyMemory(meshLod->m_material->m_texBlendMethod, m_material->m_texBlendMethod, (sizeof(int) * 10));
		CopyMemory(meshLod->m_material->m_texNameHash, m_material->m_texNameHash, (sizeof(int) * 10));

		if (!filterTextureCarry) {
			for (int i = 0; i < 8; i++)
				meshLod->m_material->m_texFileName[i] = m_material->m_texFileName[i];
		}

		meshLod->m_material->Reset();
	}
	return TRUE;
}

FLOAT
CReferenceObj::GetBoundingRadius() {
	if (m_boundRadius != 0.f) {
		return m_boundRadius;
	}

	ABBOX& box = m_boundBox;

	FLOAT x = box.maxX - box.minX;
	FLOAT y = box.maxY - box.minY;
	FLOAT z = box.maxZ - box.minZ;

	FLOAT r = x > y ? x : y;

	m_boundRadius = r > z ? r * .5f : z * .5f;

	return m_boundRadius;
}

void CReferenceObj::Serialize(CArchive& ar) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	BOOL reserved = 0;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(6); //VERSION 5: m_ReMesh
		ASSERT(ar.GetObjectSchema() == 6);

		ar << m_referenceName;

		ar.Write((const void*)&m_matrix, sizeof(Matrix44f));
		ar.Write((const void*)&m_boundBox, sizeof(ABBOX));

		if (m_material) {
			assert(m_material->m_myCompressionIndex != CMATERIAL_UNCOMPRESSED);
			ar << m_material->m_myCompressionIndex;
		} else {
			int noMat = -1;
			ar << noMat;
		}

		ar << m_lodVisualList
		   << m_collisionModel
		   << m_popoutBox.max.x
		   << m_popoutBox.max.y
		   << m_popoutBox.max.z
		   << m_popoutBox.min.x
		   << m_popoutBox.min.y
		   << m_popoutBox.min.z;

		ar << m_animModule
		   << m_faceCamera
		   << m_lockPointCount
		   << m_lodRange
		   << m_lodLevels;

		ar << m_billboardSz;

		for (int li = 0; li < m_lockPointCount; li++) {
			ar << m_extLockPoints[li].x
			   << m_extLockPoints[li].y
			   << m_extLockPoints[li].z
			   << m_extLockPoints[li].diffuse;
		}

		ar << m_seqLoad
		   << reserved
		   << reserved
		   << reserved;

		// write the ReMesh
		if (m_ReMesh) {
			// get a ReMeshStream object
			ReMeshStream streamer;
			// stream out the ReMesh object
			streamer.Write(m_ReMesh, ar);
		}
	} else {
		int version = ar.GetObjectSchema();
		// temporary holder for old meshes.  Only necessary for backwards compatibility
		CEXMeshObj* legacyMesh = NULL;

		// init the bound radius as invalid
		m_boundRadius = 0.f;

		ar >> m_referenceName;

		if (version <= 5) {
			ar >> legacyMesh;
			m_material = NULL;
			m_externalSource = NULL;
			m_matrix = legacyMesh->GetWorldTransform();
			m_boundBox = legacyMesh->GetBoundingBox();
		} else {
			ar.Read((void*)&m_matrix, sizeof(Matrix44f));
			ar.Read((void*)&m_boundBox, sizeof(ABBOX));

			if (version >= 7) {
				int compressionIndex;
				ar >> compressionIndex;
				MaterialCompressor* mc = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
				if (mc)
					m_material = mc->getMaterial(compressionIndex);
			} else {
				ar >> m_material;
			}

			MeshRM::Instance()->CreateReMaterial(m_material, Utf16ToUtf8(ar.m_strFileName).c_str());
			m_externalSource = NULL;
		}

		ar >> m_lodVisualList >> m_collisionModel >> m_popoutBox.max.x >> m_popoutBox.max.y >> m_popoutBox.max.z >> m_popoutBox.min.x >> m_popoutBox.min.y >> m_popoutBox.min.z;

		m_extLockPoints = NULL;
		if (version < 2) {
			m_animModule = NULL;
		}
		if (version > 1) {
			ar >> m_animModule >> m_faceCamera >> m_lockPointCount >> m_lodRange >> m_lodLevels;
		}

		if (version > 2) {
			ar >> m_billboardSz;
			if (m_lockPointCount > 0) {
				m_extLockPoints = new Ext_lockpoints[m_lockPointCount];
				for (int li = 0; li < m_lockPointCount; li++) {
					ar >> m_extLockPoints[li].x >> m_extLockPoints[li].y >> m_extLockPoints[li].z >> m_extLockPoints[li].diffuse;
				}
			}
		}
		m_seqLoad = 0;
		if (version > 3) {
			ar >> m_seqLoad >> reserved >> reserved >> reserved;
		}

		// init the ReMesh object
		SetReMesh(NULL);

		// Stream the ReMesh if not vertex animation
		if (version >= 5) { //VERSION 5: m_ReMesh
			// the new ReSkinMesh
			ReMesh* newMesh;
			// get an ReMeshStream object
			ReMeshStream streamer;
			// stream the ReMesh
			streamer.Read(&newMesh, ar, pICE->GetReDeviceState());
			// check for dups
			ReMesh* mesh = MeshRM::Instance()->AddMesh(newMesh, MeshRM::RMP_ID_LIBRARY);
			// no dup found
			if (mesh == NULL) {
				SetReMesh(newMesh);
			}
			// dup found....use it and delete the new mesh
			else {
				SetReMesh(mesh);
				delete newMesh;
			}

			if (version == 5) {
				// pool the ReMaterials
				MeshRM::Instance()->CreateReMaterial(legacyMesh->m_materialObject, Utf16ToUtf8(ar.m_strFileName).c_str());
				legacyMesh->m_materialObject->Clone(&m_material);

				memcpy((void*)&m_boundBox, (void*)&legacyMesh->GetBoundingBox(), sizeof(ABBOX));
			}
		}
		// build the ReMesh the legacy way
		else {
			// optimize the mesh objects
			legacyMesh->m_pd3dDevice = pICE->GetReDeviceState()->GetDevice();
			// unless they have animation vertex buffers
			if (!m_animModule) {
				legacyMesh->OptimizeMesh();
			}

			// disable legacy vertex buffers
			legacyMesh->m_VertexArrayValid = FALSE;

			// create ReMesh
			if (legacyMesh) {
				// create a new ReMesh the legacy way....TODO...the LOD's
				SetReMesh(MeshRM::Instance()->CreateReStaticMesh(legacyMesh, GetPivot())); // no pivot point needed - CreateReStaticMeshLibrary
			}

			// convert LOD's meshes into ReMesh
			if (m_lodVisualList) {
				for (POSITION lodPos = m_lodVisualList->GetHeadPosition(); lodPos != NULL;) {
					// Generate ReMesh from CEXMeshObj. We are forced to use x_backward_compat_meshLodLevel here
					// because currently there is no other way to retrieve CEXMeshObj from CMeshLODObject.
					//@@Todo: let CMeshLodObject generate ReMesh by itself. No need to pass a CEXMeshObj out of CMeshLODObject.
					//@@Todo: once this is done (and also in CWldObject), we can remove x_backward_compat_meshLodLevel.

					CMeshLODObject* mLod = (CMeshLODObject*)m_lodVisualList->GetNext(lodPos);
					mLod->x_backward_compat_meshLodLevel->InitBuffer(legacyMesh->m_pd3dDevice, NULL);
					mLod->x_backward_compat_meshLodLevel->OptimizeMesh();

					// create a new ReMesh the legacy way....
					mLod->SetReMesh(MeshRM::Instance()->CreateReStaticMesh(mLod->x_backward_compat_meshLodLevel, GetPivot())); // no pivot point needed - CreateReStaticMeshLibrary

					// Cleanup temporarily member variable - no more access beyond this point
					mLod->x_backward_compat_meshLodLevel->SafeDelete();
					delete mLod->x_backward_compat_meshLodLevel;
					mLod->x_backward_compat_meshLodLevel = NULL;
				}
			}
		}
		if (legacyMesh) {
			legacyMesh->SafeDelete();
			delete legacyMesh;
		}
		m_animationUpdatedThisRound = FALSE;
	}
#else
	ASSERT(0);
#endif
}

CReferenceObj*
CReferenceObjList::GetByIndex(int index) {
	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;
	return (CReferenceObj*)GetAt(posLoc);
}

void CReferenceObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CReferenceObj* refPtr = (CReferenceObj*)GetNext(posLoc);
		if (!refPtr)
			continue;

		refPtr->SafeDelete();
		delete refPtr;
		refPtr = 0;
	}
	RemoveAll();
}

void CReferenceObjList::FlagAllForAnimUpdate() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CReferenceObj* refPtr = (CReferenceObj*)GetNext(posLoc);
		if (!refPtr)
			continue;

		refPtr->m_animationUpdatedThisRound = FALSE;
	}
}

CReferenceObj*
CReferenceObjList::GetReferenceObjByName(const CStringA& name) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CReferenceObj* refPtr = (CReferenceObj*)GetNext(posLoc);
		if (!refPtr)
			continue;

		if (refPtr->m_referenceName == name)
			return refPtr;
	}
	return NULL;
}

int CReferenceObjList::GetReferenceIndexByName(const CStringA& name) {
	int trace = -1;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CReferenceObj* refPtr = (CReferenceObj*)GetNext(posLoc);
		if (trace == -1)
			trace = 0;

		if (refPtr && refPtr->m_referenceName == name)
			return trace;
		trace++; // Keep counting even if refPtr == NULL (placeholder)
	}
	return trace;
}

POSITION
CReferenceObjList::GetReferencePositionByName(const CStringA& name) {
	POSITION referencePosition;
	POSITION previousPosition;
	for (referencePosition = GetHeadPosition(); referencePosition;) {
		CReferenceObj* pReferenceObj;
		previousPosition = referencePosition;
		pReferenceObj = reinterpret_cast<CReferenceObj*>(GetNext(referencePosition));
		if (!pReferenceObj)
			continue;

		if (pReferenceObj->m_referenceName == name)
			return previousPosition;
	} // for..referencePosition
	return 0;
} // CReferenceObjList::GetReferencePositionByName

// Not sure this function is necessary anymore -Jonny
void CReferenceObjList::ReinitAll(LPDIRECT3DDEVICE9 g_pd3dDevice) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CReferenceObj* refPtr = (CReferenceObj*)GetNext(posLoc);
		if (!refPtr)
			continue;
	}
}

void CReferenceObjList::Serialize(CArchive& ar) {
	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CReferenceObj* rPtr = (CReferenceObj*)GetNext(posLoc);
			if (!rPtr)
				continue;

			if (rPtr->m_material) {
				matCompressor->addMaterial(rPtr->m_material);
			}
		}
		matCompressor->compress();
		matCompressor->writeToArchive(ar); // write compressed materials
		CKEPObList::Serialize(ar);
	} else {
		int version = ar.GetObjectSchema();
		if (version >= 1) {
			matCompressor->readFromArchive(ar);
		}
		CKEPObList::Serialize(ar);
	}
	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
}

//! \brief Replace mesh with new data, rebuild ReMesh after replacement.
//!
//! Refactored from related UI class (CReferenceLibrary).
//! This is supposed to be part of model class (CReferenceObj).
void CReferenceObj::SetMesh(CEXMeshObj* apNewMesh) {
#ifdef _ClientOutput
	// pRefObj->m_ReMesh might still be used by other reference objects
	// and should not be disposed here. However, there should be another
	// way to notify resource manager and decrease usage reference count.
	// Otherwise, we will see memory leak. Not critical problem though
	// as it's driven by low frequency UI operation and will go away when
	// Editor is closed.

	apNewMesh->BakeVertices(); // Apply transform so that we can use world origin as pivot point (as it's happening in studio)
	apNewMesh->ComputeBoundingVolumes(); // Compute bounding volumes
	apNewMesh->OptimizeMesh();

	// create a new ReMesh the legacy way....
	SetReMesh(MeshRM::Instance()->CreateReStaticMesh(apNewMesh, GetPivot())); // no pivot point needed - CreateReStaticMeshLibrary
	apNewMesh->m_materialObject->Clone(&m_material);

	// locate the matrix by the boundbox
	const ABBOX& bbox = apNewMesh->GetBoundingBox();
	Vector3f center((bbox.minX + bbox.maxX) * 0.5, (bbox.minY + bbox.maxY) * 0.5, (bbox.minZ + bbox.maxZ) * 0.5);
	apNewMesh->SetTranslation(center);

	// init new members
	m_matrix = apNewMesh->GetWorldTransform();
	m_boundBox = apNewMesh->GetBoundingBox();

	// init the bound radius as invalid
	m_boundRadius = 0.f;

#else
	ASSERT(0);
#endif
}

const Matrix44f& CReferenceObj::GetMatrix() const {
	return m_matrix; // always identity?
}

const ABBOX& CReferenceObj::GetBoundingBox() const {
	return m_boundBox; // in local coordinate space
}

const CMaterialObject* CReferenceObj::GetMaterial() const {
	return m_material; // shared among instances
}

std::string CReferenceObj::GetName() const {
	return (const char*)m_referenceName;
}

int CReferenceObj::GetLODCount() const {
	if (m_lodVisualList == NULL)
		return 0;

	return m_lodVisualList->GetCount();
}

Vector3f CReferenceObj::GetPivot() const {
	return Vector3f(0.0f, 0.0f, 0.0f); // Use origin
}

IMPLEMENT_SERIAL_SCHEMA(CReferenceObj, CObject) //VERSION 5: m_ReMesh
IMPLEMENT_SERIAL_SCHEMA(CReferenceObjList, CKEPObList)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CReferenceObj, IDS_REFERENCEOBJ_NAME)
GETSET_MAX(m_referenceName, gsCString, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, REFERENCENAME, STRLEN_MAX)
GETSET_OBLIST_PTR(m_lodVisualList, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, LODVISUALLIST)
GETSET_OBJ_PTR(m_collisionModel, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, COLLISIONMODEL, CEXMeshObj)
GETSET_D3DVECTOR(m_popoutBox.min, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, POPOUTBOX_MIN)
GETSET_D3DVECTOR(m_popoutBox.max, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, POPOUTBOX_MAX)
GETSET_OBJ_PTR(m_animModule, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, ANIMMODULE, CVertexAnimationModule)
GETSET(m_faceCamera, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, FACECAMERA)
GETSET_RANGE(m_lockPointCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, LOCKPOINTCOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_lodRange, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, LODRANGE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_lodLevels, gsInt, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, LODLEVELS, INT_MIN, INT_MAX)
GETSET_RANGE(m_billboardSz, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEOBJ, BILLBOARDSZ, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP
