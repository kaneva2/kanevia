///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "matrixarb.h"
#include <d3dx9.h>

namespace KEP {

void MatrixARB::MassInvert(int numMatrics, const Matrix44f* matrics, Matrix44f* matricsOut) {
	float det = 0;
	for (int i = 0; i < numMatrics; i++)
		D3DXMatrixInverse((D3DXMATRIX*)matricsOut + i, &det, (D3DXMATRIX*)matrics + i);
}

void MatrixARB::MassMultiply(int numMatrics, const Matrix44f* matricsA, const Matrix44f* matricsB, Matrix44f* matricsOut) {
	for (int i = 0; i < numMatrics; i++)
		D3DXMatrixMultiply((D3DXMATRIX*)matricsOut + i, (D3DXMATRIX*)matricsA + i, (D3DXMATRIX*)matricsB + i);
}

} // namespace KEP