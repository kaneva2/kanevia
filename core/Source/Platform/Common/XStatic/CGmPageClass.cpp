///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CPlayerClass.h"
#include "CGmPageClass.h"

namespace KEP {

CGmPageObj::CGmPageObj() {
	m_handle = 0;
}

void CGmPageList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CGmPageObj* gmpPtr = (CGmPageObj*)GetNext(posLoc);
		delete gmpPtr;
		gmpPtr = 0;
	}
	RemoveAll();
}

void CGmPageList::AddPage(PLAYER_HANDLE handle) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CGmPageObj* gmpPtr = (CGmPageObj*)GetNext(posLoc);
		if (gmpPtr->m_handle == handle)
			return;
	}
	CGmPageObj* pageObject = new CGmPageObj();
	pageObject->m_handle = handle;

	AddTail(pageObject);
}

void CGmPageList::RemovePage(PLAYER_HANDLE handle) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CGmPageObj* gmpPtr = (CGmPageObj*)GetNext(posLoc);
		if (gmpPtr->m_handle == handle) {
			delete gmpPtr;
			gmpPtr = 0;
			RemoveAt(posLast);
		}
	}
}

} // namespace KEP