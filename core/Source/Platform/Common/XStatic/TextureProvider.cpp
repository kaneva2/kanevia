///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TextureProvider.h"
#include "ContentService.h"
#undef min
#include "UniqueAssetPathFormatter.h"
#include "common\include\IMemSizeGadget.h"

#include "common\include\NetworkCfg.h"

namespace KEP {

// Hard-code for now - to be replaced by wokweb call
static UniqueAssetPathFormatter s_uniqueDDSPathFormatter("{xr122}/{d}.dds.lzma");
static UniqueAssetPathFormatter s_uniqueJPGPathFormatter("{xr122}/{d}.{q}.jpg");

std::string TextureProvider::ms_uniqueTextureUrlPrefix = NET_TEXTURES;

UGCItemTextureProvider::UGCItemTextureProvider(const ContentMetadata& md) :
		m_textureUrlTemplate(md.getPrimaryTextureUrl()),
		m_JPEGUrlTemplate(md.getJPEGTextureUrl()),
		m_textureUniqueIds(md.getTextureUniqueIds()),
		m_uniqueTextureFileStore(md.getUniqueTextureFileStore()) {
}

std::string UGCItemTextureProvider::getUrl(ItemTextureOrdinal key, bool multiresJpg) {
	auto itr = m_textureUniqueIds.find(key);
	if (itr != m_textureUniqueIds.end()) {
		// Must set URL prefix (e.g. http://dev-textures.kaneva.com/)
		assert(!ms_uniqueTextureUrlPrefix.empty());

		// Unique texture
		if (multiresJpg) {
			return ms_uniqueTextureUrlPrefix + m_uniqueTextureFileStore + s_uniqueJPGPathFormatter(itr->second);
		} else {
			return ms_uniqueTextureUrlPrefix + m_uniqueTextureFileStore + s_uniqueDDSPathFormatter(itr->second);
		}
	}

	// Item texture URL template
	std::string url = multiresJpg ? m_JPEGUrlTemplate : m_textureUrlTemplate;
	STLStringSubstitute(url, TEXTURE_SELECTOR_MACRO, std::to_string(key));
	return url;
}

void UGCItemTextureProvider::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_textureUrlTemplate);
		pMemSizeGadget->AddObject(m_JPEGUrlTemplate);
		pMemSizeGadget->AddObject(m_textureUniqueIds);
		pMemSizeGadget->AddObject(m_uniqueTextureFileStore);
	}
}

CustomTextureProvider::CustomTextureProvider(const std::string& url) : m_url(url) {
	assert(!m_url.empty());
}

void CustomTextureProvider::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_url);
	}
}

} // namespace KEP
