///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <d3dx9math.h>
#include <afxtempl.h>

#ifdef _ClientOutput
#include <IClientEngine.h>
#endif

#include "CTranslucentPolyCache.h"
#include "CMaterialClass.h"
#include "CEXMeshClass.h"
#include "CStateManagementClass.h"

#include "common\include\LogHelper.h"

namespace KEP {

static LogInstance("Instance");

void CTranslucentPolyList::CacheMesh(
	CEXMeshObj* mesh,
	const Matrix44f& viewMatrix,
	const Matrix44f& wldMatrix,
	const Vector3f& meshCenter,
	CReferenceObj* refOpt,
	CMaterialObject* custMaterial) {
	InsertPolygonOrMeshRef(
		NULL,
		viewMatrix,
		wldMatrix,
		mesh,
		meshCenter,
		refOpt,
		custMaterial);
}

void CTranslucentPolyList::CacheMeshNoSort(
	CEXMeshObj* mesh,
	const Matrix44f& viewMatrix,
	const Matrix44f& wldMatrix,
	const Vector3f& meshCenter,
	CReferenceObj* refOpt,
	CMaterialObject* custMaterial) {
	InsertPolygonOrMeshRefNonTrans(
		NULL,
		viewMatrix,
		wldMatrix,
		mesh,
		meshCenter,
		refOpt,
		custMaterial);
}

CTranslucentPolyList::CTranslucentPolyList() {
	m_currentObjectCount = 0;
	m_lastPosition = 0;
	m_lastValue = 0;
	m_alphaMode = FALSE;
}

bool MatrixScaleNotOne(Matrix44f aMat) {
	float sqrVecNorm = 0.0;
	for (int i = 0; i < 3; i++) {
		sqrVecNorm = (aMat[i][0] * aMat[i][0]) + (aMat[i][1] * aMat[i][1]) + (aMat[i][2] * aMat[i][2]);
		if (abs(1.0f - sqrVecNorm) > 0.05f /* threshold to enable normalization or vertex normals */)
			return true;
	}
	return false;
}

void CTranslucentPolyObj::Render(
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	BOOL& mirrorEnable,
	Matrix44f& mirrorMatrix,
	CStateManagementObj* stateManager,
	TextureDatabase* textureDataBase,
	Vector3f& camLocation,
	Matrix44f& matrixLoc,
	float camFov,
	CTranslucentPolyObj** lastObjRendered,
	BOOL& alphaMode) {
	HRESULT hr = 0;
	if (!m_meshReference)
		return;

	if (!m_meshReference->m_visibilityTrack) //do not render this
		return;

	if (m_meshReference->mbIsSkinned) {
		mRenderData.mpMaterial = GetMaterialObject();
		mRenderData.mpWorldTransform = (D3DXMATRIX*)&m_location;
		mRenderData.mpTextureDatabase = textureDataBase;

		m_meshReference->PreRenderFX(g_pd3dDevice);
		m_meshReference->RenderFX(g_pd3dDevice, &mRenderData);
		m_meshReference->PostRenderFX(g_pd3dDevice);
		return;
	}

	// DRF - Believe this is dead code
	if (*lastObjRendered == NULL || (*lastObjRendered)->m_meshReference != m_meshReference || stateManager->m_currentHSLeffect != NULL || m_meshReference->GetExternalSource() != NULL)
	// Modified by Jonny 11/28/06 to allow flash on 2 tv's at same time was using the renderfast method
	// so second wouldn't render properly.  Added check to see if this mesh has external source.
	{
		LogWarn("DEAD CODE");
		//new mesh type
		// state that can be set when generating or rendering shadowmaps
		stateManager->SetVertexType(m_meshReference->m_abVertexArray.m_uvCount);

		stateManager->SetCullState(GetMaterialObject()->m_cullOn);
		stateManager->SetWireFillState(GetMaterialObject()->m_forceWireFrame);

		stateManager->SetZBias(GetMaterialObject()->m_zBias);
		stateManager->SetAlphaPixelTest(GetMaterialObject()->m_doAlphaDepthTest);

		if (MatrixScaleNotOne(m_location))
			stateManager->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
		else
			stateManager->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);

		if (!stateManager->m_fxShaderOverride) {
			// state that should not be set when generating or rendering shadowmaps
			stateManager->SetWorldMatrix((D3DXMATRIX*)&m_location);

			stateManager->SetPxShader(0, GetMaterialObject()->m_pixelShader);

			stateManager->SetFXShaderParameters(
				GetMaterialObject(),
				GetMaterialObject()->m_texNameHash,
				textureDataBase,
				reinterpret_cast<D3DXMATRIX&>(m_location));

			stateManager->SetMaterial(&m_materialRef);
			stateManager->SetDiffuseSource(m_meshReference->m_abVertexArray.m_advancedFixedMode);
			stateManager->SetTextureTileMode(GetMaterialObject()->m_textureTileType);
			m_meshReference->TextureMovementCallback(stateManager->m_currentVertexType);

			stateManager->SetBlendType(m_blendRef);
			stateManager->SetFogOverrideState(GetMaterialObject()->m_fogOverride);
		} else {
			if (stateManager->m_currentHSLeffect) {
				// set World and WorldIT parameters in the effect shader
				Matrix44f worldInverseTranspose;
				worldInverseTranspose.MakeInverseOf(m_location);
				worldInverseTranspose.Transpose();
				hr = stateManager->m_currentHSLeffect->SetMatrix("g_mWorldIT", reinterpret_cast<D3DXMATRIX*>(&worldInverseTranspose));
				hr = stateManager->m_currentHSLeffect->SetMatrix("g_mWorld", reinterpret_cast<D3DXMATRIX*>(&m_location));
			}
		}

		if (stateManager->m_currentHSLeffect) {
			// render using D3DX Effect framework

			stateManager->SetVertexType(m_meshReference->m_abVertexArray.m_uvCount);

			// Apply the technique contained in the effect (note: technique set in pixel state managment)
			HRESULT errHr;
			UINT cPasses = 0;
			if (stateManager->m_currentHSLeffect->Begin(&cPasses, 0) == S_OK) {
				for (UINT iPass = 0; iPass < cPasses; iPass++) {
					errHr = stateManager->m_currentHSLeffect->BeginPass(iPass);

					// The effect interface queues up the changes and performs them
					// with the CommitChanges call. You do not need to call CommitChanges if
					// you are not setting any parameters between the BeginPass and EndPass.
					// V( g_pEffect->CommitChanges() );
					m_meshReference->Render(g_pd3dDevice, stateManager->m_currentVertexType, m_meshReference->m_chromeUvSet);

					// Render the mesh with the applied technique

					errHr = stateManager->m_currentHSLeffect->EndPass();
				}
				errHr = stateManager->m_currentHSLeffect->End();
			}

			return;
		} //end snap shot render

		// Rendering not using FX shaders

		// Set textures and color ops
		// first for the first texture (deal differently with this one)
		stateManager->SetTextureState(GetMaterialObject()->m_texNameHash[0], 0, textureDataBase);
		if (GetMaterialObject()->m_texBlendMethod[9] < 0)
			stateManager->SetBlendLevel(0, 0);
		else
			stateManager->SetBlendLevel(0, GetMaterialObject()->m_texBlendMethod[9]);
		// then for the 7 other textures
		for (int loop = 1; loop < stateManager->m_multiTextureSupported; loop++) {
			stateManager->SetTextureState(GetMaterialObject()->m_texNameHash[loop], loop, textureDataBase);
			stateManager->SetBlendLevel(loop, GetMaterialObject()->m_texBlendMethod[(loop - 1)]);
			if (loop > 7)
				break;
		}
		//set multitexture color op arguments
		if (GetMaterialObject()) {
			for (int loop = 0; loop < 8; loop++) {
				stateManager->SetColorArg1Level(loop, GetMaterialObject()->m_colorArg1[loop]);
				stateManager->SetColorArg2Level(loop, GetMaterialObject()->m_colorArg2[loop]);
			}
		}

		m_meshReference->Render(g_pd3dDevice, stateManager->m_currentVertexType, m_meshReference->m_chromeUvSet);
		*lastObjRendered = this; //m_meshReference;
	} //end new mesh type
	else {
		//same mesh (so don't set the same render states again)
		if (this->GetMaterialObject() != (*lastObjRendered)->GetMaterialObject()) {
			//this is a customized dynamic object, so we need to update texture & material info
			// Set textures and color ops

			stateManager->SetTextureState(GetMaterialObject()->m_texNameHash[0], 0, textureDataBase);
			if (GetMaterialObject()->m_texBlendMethod[9] < 0)
				stateManager->SetBlendLevel(0, 0);
			else
				stateManager->SetBlendLevel(0, GetMaterialObject()->m_texBlendMethod[9]);
			// then for the 7 other textures
			for (int loop = 1; loop < stateManager->m_multiTextureSupported; loop++) {
				stateManager->SetTextureState(GetMaterialObject()->m_texNameHash[loop], loop, textureDataBase);
				stateManager->SetBlendLevel(loop, GetMaterialObject()->m_texBlendMethod[(loop - 1)]);
				if (loop > 7)
					break;
			}
			//set multitexture color op arguments
			if (GetMaterialObject()) {
				for (int loop = 0; loop < 8; loop++) {
					stateManager->SetColorArg1Level(loop, GetMaterialObject()->m_colorArg1[loop]);
					stateManager->SetColorArg2Level(loop, GetMaterialObject()->m_colorArg2[loop]);
				}
			}
		}

		stateManager->SetVertexType(m_meshReference->m_abVertexArray.m_uvCount);
		stateManager->SetWorldMatrix((D3DXMATRIX*)&m_location);
		stateManager->SetMaterial(&m_materialRef);
		m_meshReference->RenderFast(g_pd3dDevice, stateManager->m_currentVertexType, m_meshReference->m_chromeUvSet);
	} //end same mesh type
}

float CTranslucentPolyObj::MagnitudeSquared(
	float& Mx1, float& My1,
	float& Mz1, float& Mx2,
	float& My2, float& Mz2) {
	float temp_Mxyz = (((Mx1 - Mx2) * (Mx1 - Mx2)) + ((My1 - My2) * (My1 - My2)) + ((Mz1 - Mz2) * (Mz1 - Mz2)));
	return (float)sqrt(temp_Mxyz);
}

CMaterialObject* CTranslucentPolyObj::GetMaterialObject() {
	return m_customMaterial ? m_customMaterial : m_meshReference->m_materialObject;
}

void CTranslucentPolyList::Render(
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	BOOL& mirrorEnable,
	Matrix44f& mirrorMatrix,
	CStateManagementObj* stateManager,
	TextureDatabase* textureDataBase,
	BOOL frontToBack,
	Vector3f& camLocation,
	Matrix44f& matrixLoc,
	float camFov) {
#ifdef _ClientOutput
	m_lastObjRendered = NULL;

	if (frontToBack) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CTranslucentPolyObj* pObj = (CTranslucentPolyObj*)GetNext(posLoc);

			const ABBOX& bbox = pObj->m_meshReference->GetBoundingBox();
			Vector3f pos;
			pos.x = pObj->m_location(3, 0) + (bbox.minX + bbox.maxX) * 0.5;
			pos.y = pObj->m_location(3, 1) + (bbox.minY + bbox.maxY) * 0.5;
			pos.z = pObj->m_location(3, 2) + (bbox.minZ + bbox.maxZ) * 0.5;

			auto pICE = IClientEngine::Instance();
			if (!pICE)
				return;

			pICE->EnableLightForPosition(pos);

			pObj->Render(g_pd3dDevice,
				mirrorEnable,
				mirrorMatrix,
				stateManager,
				textureDataBase,
				camLocation,
				matrixLoc,
				camFov,
				&m_lastObjRendered,
				m_alphaMode);
		}
	} else {
		for (POSITION posLoc = GetTailPosition(); posLoc != NULL;) {
			CTranslucentPolyObj* pObj = (CTranslucentPolyObj*)GetPrev(posLoc);

			// enable lights for this object
			const ABBOX& bbox = pObj->m_meshReference->GetBoundingBox();
			Vector3f pos;
			pos.x = pObj->m_location(3, 0) + (bbox.minX + bbox.maxX) * 0.5;
			pos.y = pObj->m_location(3, 1) + (bbox.minY + bbox.maxY) * 0.5;
			pos.z = pObj->m_location(3, 2) + (bbox.minZ + bbox.maxZ) * 0.5;

			auto pICE = IClientEngine::Instance();
			if (!pICE)
				return;

			pICE->EnableLightForPosition(pos);

			pObj->Render(g_pd3dDevice,
				mirrorEnable,
				mirrorMatrix,
				stateManager,
				textureDataBase,
				camLocation,
				matrixLoc,
				camFov,
				&m_lastObjRendered,
				m_alphaMode);
		}
	}

	stateManager->SetAlphaPixelTest(FALSE);
#else
	ASSERT(FALSE);
#endif
}

void CTranslucentPolyList::InsertPolygonOrMeshRef(
	ABVERTEX* vertex,
	const Matrix44f& viewMatrix,
	const Matrix44f& wldMatrix,
	CEXMeshObj* meshRef,
	const Vector3f& meshCenter,
	CReferenceObj* refOpt,
	CMaterialObject* custMaterial) {
	//if cusMaterial is set, use it. otherwise use the material in the mesh
	CMaterialObject* materialPtr = custMaterial == NULL ? meshRef->m_materialObject : custMaterial;
	CTranslucentPolyObj* pObj = new CTranslucentPolyObj();

	pObj->m_referenceOption = refOpt;
	pObj->m_meshReference = NULL;
	pObj->m_customMaterial = custMaterial;

	if (meshRef) {
		pObj->m_meshReference = meshRef;
		pObj->m_location = meshRef->GetWorldTransform();
	}

	if (materialPtr) {
		CopyMemory(&pObj->m_materialRef, &materialPtr->m_useMaterial, sizeof(D3DMATERIAL9));
		pObj->m_blendRef = materialPtr->m_blendMode;
	}

	pObj->m_zValue = Distance(viewMatrix.GetTranslation(), meshCenter);

	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CTranslucentPolyObj* listObj = (CTranslucentPolyObj*)GetNext(posLoc);
		if (pObj->m_zValue < listObj->m_zValue) {
			m_lastPosition = InsertBefore(posLast, pObj);
			m_lastValue = pObj->m_zValue;
			m_currentObjectCount++;
			return;
		}
	}

	AddTail(pObj);
	m_currentObjectCount++;
}

void CTranslucentPolyList::InsertPolygonOrMeshRefNonTrans(
	ABVERTEX* vertex,
	const Matrix44f& viewMatrix,
	const Matrix44f& wldMatrix,
	CEXMeshObj* meshRef,
	const Vector3f& meshCenter,
	CReferenceObj* refOpt,
	CMaterialObject* custMaterial) {
	//if cusMaterial is set, use it. otherwise use the material in the mesh
	CMaterialObject* materialPtr = custMaterial == NULL ? meshRef->m_materialObject : custMaterial;
	CTranslucentPolyObj* pObj = new CTranslucentPolyObj();

	pObj->m_meshReference = NULL;
	pObj->m_referenceOption = refOpt;
	pObj->m_customMaterial = custMaterial;
	if (meshRef) {
		pObj->m_meshReference = meshRef;
		pObj->m_location = meshRef->GetWorldTransform();
	}

	if (materialPtr) {
		CopyMemory(&pObj->m_materialRef, &materialPtr->m_useMaterial, sizeof(D3DMATERIAL9));
		pObj->m_blendRef = materialPtr->m_blendMode;
	}

	pObj->m_zValue = Distance(viewMatrix.GetTranslation(), meshCenter);

	POSITION posLast, posLoc2, posLoc3;

	posLoc2 = NULL;
	posLoc3 = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CTranslucentPolyObj* listObj = (CTranslucentPolyObj*)GetNext(posLoc);
		if (!listObj->m_meshReference)
			continue;
		if (pObj->m_zValue < listObj->m_zValue)
			posLoc2 = posLast;

		if (materialPtr->m_texNameHash[0] == listObj->GetMaterialObject()->m_texNameHash[0]) {
			if (pObj->m_zValue < listObj->m_zValue) {
				InsertBefore(posLast, pObj);
				m_currentObjectCount++;
				return;
			}

			for (POSITION innerPos = posLoc; (posLoc3 = innerPos) != NULL;) { //
				CTranslucentPolyObj* innerObj = (CTranslucentPolyObj*)GetNext(innerPos);
				if (materialPtr->m_texNameHash[0] == innerObj->GetMaterialObject()->m_texNameHash[0]) {
					if (pObj->m_zValue < innerObj->m_zValue) {
						InsertBefore(posLoc3, pObj);
						m_currentObjectCount++;
						return;
					}

				} else {
					InsertBefore(posLoc3, pObj);
					m_currentObjectCount++;
					return;
				}
			}
			m_currentObjectCount++;
		}
	}

	if (posLoc2)
		InsertBefore(posLast, pObj);
	else
		AddTail(pObj);

	m_currentObjectCount++;
}

void CTranslucentPolyList::Flush() {
	m_currentObjectCount = 0;
}

void CTranslucentPolyList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CTranslucentPolyObj* pObj = (CTranslucentPolyObj*)GetNext(posLoc);
		if (pObj) //crash prevention
			delete pObj;
		pObj = 0;
	}

	m_currentObjectCount = 0;
	m_lastPosition = 0;
	m_lastValue = 0.0f;
	RemoveAll();
}

} // namespace KEP