///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CBackgroundclass.h"

namespace KEP {

//------------Main object list------------------//
CBackGroundObj::CBackGroundObj(IDirect3DTexture9* L_mainSurface, //main surface
	CStringA L_bitmapName, //bitmap file name
	int L_mapX, //represents the x pixel count of source image
	int L_mapY, //represents the y pixel count of source image
	RECT L_usageRect, //represents the rect of what is used of the source image
	int L_posX, //position X of offset that defines location
	int L_posY, //position Y of offset that defines location
	BOOL L_moving, //is the map movable
	int L_animX,
	int L_animY) {
	m_animX = L_animX;
	m_animY = L_animY;
	m_moving = L_moving;
	m_mainSurface = L_mainSurface;
	m_bitmapName = L_bitmapName;
	m_mapX = L_mapX;
	m_mapY = L_mapY;
	m_usageRect = L_usageRect;
	m_posX = L_posX;
	m_posY = L_posY;
}

//------------Main object list------------------//

void CBackGroundObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_posX
		   << m_posY
		   << m_mapX
		   << m_mapY
		   << m_bitmapName
		   << m_usageRect
		   << m_moving
		   << m_animX
		   << m_animY;

	} else {
		ar >> m_posX >> m_posY >> m_mapX >> m_mapY >> m_bitmapName >> m_usageRect >> m_moving >> m_animX >> m_animY;
		m_mainSurface = 0;
	}
}

IMPLEMENT_SERIAL(CBackGroundObj, CObject, 0)
IMPLEMENT_SERIAL(CBackGroundObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CBackGroundObj, IDS_BACKGROUNDOBJ_NAME)
GETSET_RANGE(m_posX, gsInt, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, POSX, INT_MIN, INT_MAX)
GETSET_RANGE(m_posY, gsInt, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, POSY, INT_MIN, INT_MAX)
GETSET_RANGE(m_mapX, gsInt, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, MAPX, INT_MIN, INT_MAX)
GETSET_RANGE(m_mapY, gsInt, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, MAPY, INT_MIN, INT_MAX)
GETSET_MAX(m_bitmapName, gsCString, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, BITMAPNAME, STRLEN_MAX)
GETSET(m_usageRect, gsRect, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, USAGERECT)
GETSET(m_moving, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, MOVING)
GETSET_RANGE(m_animX, gsInt, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, ANIMX, INT_MIN, INT_MAX)
GETSET_RANGE(m_animY, gsInt, GS_FLAGS_DEFAULT, 0, 0, BACKGROUNDOBJ, ANIMY, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP