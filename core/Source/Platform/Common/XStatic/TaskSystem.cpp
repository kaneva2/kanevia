///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TaskSystem.h"
#include "TaskObject.h"
#include "SetThreadName.h"
#include "common\include\KEPCommon.h"
#include "common\include\KEPHelpers.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

const unsigned int TaskSystem::DEFAULT_WORKER_THREADS = 4;
const unsigned int TaskSystem::WORKER_THREAD_LIMIT = 4;
const unsigned int TaskSystem::WAIT_FOR_EXIT_TIMEOUT = 5000;

void TaskSystem::Initialize(size_t threads) {
	if (IsInitialized())
		return;

	m_semaphoreHandle = CreateSemaphore(NULL, 0, LONG_MAX, NULL);
	if (!m_semaphoreHandle)
		return;

	m_exitEventHandle = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (!m_exitEventHandle) {
		CloseHandle(m_semaphoreHandle);
		m_semaphoreHandle = nullptr;
		return;
	}

	if (!threads)
		threads = DEFAULT_WORKER_THREADS;
	threads = std::min(threads, WORKER_THREAD_LIMIT);
	if (!CreateWorkerThreads(threads)) {
		LogError("CreateWorkerThreads() FAILED - threads=" << threads);
		CloseHandle(m_semaphoreHandle);
		m_semaphoreHandle = nullptr;
		CloseHandle(m_exitEventHandle);
		m_exitEventHandle = nullptr;
		return;
	}
}

void TaskSystem::Finalize() {
	if (!IsInitialized())
		return;

	SetEvent(m_exitEventHandle);

	// Wait for worker threads to exit
	WaitForMultipleObjects(m_threads, m_threadHandles, TRUE, WAIT_FOR_EXIT_TIMEOUT);

	// Close all worker thread handles
	for (int i = 0; i < m_threads; i++)
		CloseHandle(m_threadHandles[i]);
	delete[] m_threadHandles;
	m_threadHandles = nullptr;
	m_threads = 0;

	// Close the semaphore
	CloseHandle(m_semaphoreHandle);
	m_semaphoreHandle = nullptr;

	// Close exit event
	CloseHandle(m_exitEventHandle);
	m_exitEventHandle = nullptr;
}

void TaskSystem::ProcessOneTask() {
	// Pop Next Task From Tasks Queue (FIFO)
	TaskObject* pTask = PopTask();
	if (!pTask)
		return;

	// Execute Task
	pTask->ExecuteTask();

	// Task system is finished with this task.
	// If the caller didn't increment it's ref count
	// then it will get deleted here.
	if (pTask->DecRefCount() == 0)
		delete pTask;
}

void TaskSystem::ClearAllTasks() {
	// Clear All Tasks In Tasks Queue
	std::lock_guard<fast_mutex> lock(m_mutex);
	while (m_taskQueue.size() > 0) {
		TaskObject* pTask = m_taskQueue.front();
		if (pTask->DecRefCount() == 0)
			delete pTask;
		m_taskQueue.pop_front();
	}
}

void TaskSystem::QueueTask(TaskObject* pTask) {
	// Valid Task ?
	if (!pTask)
		return;

	// Push Task To Tasks Queue (FIFO)
	std::lock_guard<fast_mutex> lock(m_mutex);
	pTask->IncRefCount(); // decremented in ProcessOneTask()
	m_taskQueue.push_back(pTask); // add to tasks queue
	ReleaseSemaphore(m_semaphoreHandle, 1, NULL); // signal task available
}

TaskObject* TaskSystem::PopTask() {
	// Pop Next Task From Tasks Queue (FIFO)
	std::lock_guard<fast_mutex> lock(m_mutex);
	if (m_taskQueue.size() == 0)
		return nullptr;
	TaskObject* pTask = m_taskQueue.front();
	m_taskQueue.pop_front();
	return pTask;
}

bool TaskSystem::CreateWorkerThreads(size_t threads) {
	// Create Worker Threads
	m_threadHandles = new HANDLE[threads];
	for (int i = 0; i < threads; i++) {
		m_threadHandles[i] = CreateThread(NULL, 0, TaskSystem::WorkerThreadProcedure, this, 0, NULL);
		if (m_threadHandles[i] == NULL) {
			// Something went wrong, clean up all worker threads created so far
			for (int j = i - 1; j >= 0; j--)
				CloseHandle(m_threadHandles[j]);
			delete[] m_threadHandles;
			m_threadHandles = NULL;
			return false;
		}
	}

	// Successfully created all worker threads
	m_threads = threads;

	return true;
}

DWORD TaskSystem::WorkerThreadProcedure(LPVOID lpParam) {
	auto self = static_cast<TaskSystem*>(lpParam);
	if (!self)
		return 0;

	// Set Thread Priority
	::SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);

	// Set Thread Name
	std::string str;
	StrBuild(str, "TaskSystem::" << self->m_name);
	::SetThreadName(str.c_str());

	self->InitializeWorkerThread();

	FOREVER {
		HANDLE waitHandles[] = { self->m_exitEventHandle, self->m_semaphoreHandle };

		// Wait for the next task to be queued up, or for the exit event
		DWORD waitResult = WaitForMultipleObjects(2, waitHandles, FALSE, INFINITE);

		switch (waitResult) {
			case WAIT_OBJECT_0: {
				// Exit event is set
				return 0;
			}

			case WAIT_OBJECT_0 + 1: {
				// There's work in the task queue
				self->ProcessOneTask();
				break;
			}

			default: {
				// Unknown return code means bail out with error
				return ULONG_MAX;
			}
		}
	}
}

} // namespace KEP