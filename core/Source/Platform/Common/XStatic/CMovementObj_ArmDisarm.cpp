///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CMovementObj.h"

#include "CCfgClass.h"
#include "CItemClass.h"
#include "usetypes.h"
#include "..\..\Engine\ClientBase\ClientBaseEngine.h"
#include "IClientEngine.h" // TODO: reduce/eliminate use of IClientEngine::Instance inside XStatic
#include "RuntimeSkeleton.h"
#include "SkeletonConfiguration.h"
#include "CSkeletalLODClass.h"
#include "SkeletonLabels.h"
#include "EquippableSystem.h"
#include "ActorDatabase.h"

namespace KEP {

static LogInstance("Instance");

static bool AllowsAP() {
	auto pICE = IClientEngine::Instance();
	auto pCBE = dynamic_cast<ClientBaseEngine*>(pICE);
	if (!pCBE)
		return false;
	bool worldAP = pCBE->WorldPassGroupAP();
	bool myAP = CMovementObj::MyPassGroupAP();
	return (myAP && worldAP);
}

static CMovementObj* GetMovementObjRefModel(const CMovementObj* pMO) {
	auto pICE = IClientEngine::Instance();
	auto pCBE = dynamic_cast<ClientBaseEngine*>(pICE);
	return pCBE ? pCBE->GetMovementObjRefModel(pMO) : nullptr;
}

size_t CMovementObj::armedItems() const {
	return m_currentlyArmedItems ? m_currentlyArmedItems->GetCount() : 0;
}

std::vector<CItemObj*> CMovementObj::getArmedItems() const {
	std::vector<CItemObj*> armedItems;
	if (!m_currentlyArmedItems)
		return armedItems;
	OBJ_LIST_GUARD(m_currentlyArmedItems);
	for (POSITION pos = m_currentlyArmedItems->GetHeadPosition(); pos;) {
		auto pIO = (CItemObj*)m_currentlyArmedItems->GetNext(pos);
		if (!pIO)
			continue;
		armedItems.push_back(pIO);
	}
	return armedItems;
}

void CMovementObj::logArmed() const {
	// Log All Currently Armed Items
	auto armedItems = getArmedItems();
	LogInfo(ToStr() << " items=" << armedItems.size());
	for (const auto& pIO : armedItems) {
		if (!pIO)
			continue;
		_LogInfo(" ... " << pIO->ToStr());
	}
}

bool CMovementObj::realizeDimConfig(CMovementObj* pMORM) {
	// Assume 'this' Movement Object ?
	if (!pMORM) {
		pMORM = GetMovementObjRefModel(this);
		if (!pMORM)
			return false;
	}

	// Valid Skeleton Configurations ?
	if (!m_skeletonConfiguration || !pMORM->m_skeletonConfiguration)
		return false;

	// Realize Configuration Now (starts rendering newly armed/disarmed avatar)
	m_skeletonConfiguration->RealizeDimConfig(m_pSkeleton.get(), pMORM->m_skeletonConfiguration->m_meshSlots);

	LogInfo(ToStr(pMORM) << " OK - items=" << armedItems());

	return true;
}

const CItemObj* CMovementObj::getItemExcludesCurrentlyArmed(const CItemObj* pIO) {
	// Valid Item ?
	if (!pIO)
		return NULL;

	// DRF - Crash Fix - Be Sure To Unlock Before Return!
	if (!m_currentlyArmedItems)
		return NULL;

	// Item Excludes Currently Armed Item ?
	OBJ_LIST_GUARD(m_currentlyArmedItems);
	const CItemObj* pIOExc = NULL;
	for (POSITION pos = m_currentlyArmedItems->GetHeadPosition(); pos;) {
		auto pIOArmed = (CItemObj*)m_currentlyArmedItems->GetNext(pos);
		if (!pIOArmed)
			continue;

		// Excludes Armed Item ?
		if (pIO->Excludes(pIOArmed)) {
			pIOExc = pIOArmed;
			break;
		}
	}

	return pIOExc;
}

///////////////////////////////////////////////////////////////////////////////
// Arm Functions
///////////////////////////////////////////////////////////////////////////////

bool CMovementObj::armDefaultItemsNotExcluded() {
	// Is my PassGroup AP ?
	bool allowsAP = AllowsAP();

	// Get Default Clothing Items
	CItemObj::itemVector defaultItems;
	getDefaultClothingItems(defaultItems);

	// Default Clothing Items Must Be Either Excluded Or Armed (prevent nakedness)
	for (auto i = defaultItems.begin(); i != defaultItems.end(); i++) {
		auto pIO = (*i);
		if (!pIO)
			continue;

		// If AP Is Allowed Only 'Jeans' Are Required
		if (allowsAP && !STLContainsIgnoreCase((const char*)pIO->m_itemName, "Jeans"))
			continue;

		// If Default Item Not Excluded Arm Now (caller should do realize, don't arm defaults recursively)
		if (!getItemExcludesCurrentlyArmed(pIO)) {
			LogInfo(ToStr() << " ARMING DEFAULT - " << pIO->ToStr());
			if (!armItem(pIO, false, NULL, false)) {
				LogError(ToStr() << " armItem() FAILED - " << pIO->ToStr());
			}
		}
	}

	return true;
}

bool CMovementObj::armItem(CItemObj* pIO, bool realizeNow, bool armDefaults, CMovementObj* pMORM) {
	if (!pIO)
		return false;

	if (!pMORM) {
		pMORM = GetMovementObjRefModel(this);
		if (!pMORM)
			return false;
	}

	// Arm/Disarm Allowed ?
	if (!m_allowArmDisarm) {
		LogError(ToStr(pMORM) << " NOT_ALLOWED");
		return false;
	}

	// Match Actor Group With Item Group { ALL, male, female, none }
	if (!pIO->MatchActorGroups(4, m_actorGroups)) {
		LogError(ToStr(pMORM) << " MatchActorGroups() FAILED - actorGroups=" << m_actorGroups << " " << pIO->ToStr());
		return false;
	}

	CItemObj* pIOCopy = pIO;
	GLID glid = pIO->m_globalID;

	// DRF - Crash Fix - Be Sure To Unlock Before Return!
	if (!m_currentlyArmedItems)
		return false;

	// Item Already Armed Or Excludes Currently Armed Item ?
	OBJ_LIST_GUARD(m_currentlyArmedItems);
	std::vector<CItemObj*> vecExc;
	for (POSITION pos = m_currentlyArmedItems->GetHeadPosition(); pos;) {
		auto pIOArmed = (CItemObj*)m_currentlyArmedItems->GetNext(pos);
		if (!pIOArmed)
			continue;

		// Already Armed ?
		GLID glidArmed = pIOArmed->m_globalID;
		if (glidArmed == glid) {
			LogInfo(ToStr(pMORM) << " ALREADY ARMED - " << pIO->ToStr() << " - items=" << armedItems());
			goto ArmItemSuccess;
		}

		// Excludes Armed Item ?
		if (pIO->Excludes(pIOArmed)) {
			LogInfo(ToStr(pMORM) << " EXCLUSION - " << pIO->ToStr() << " EXCLUDES " << pIOArmed->ToStr());
			vecExc.push_back(pIOArmed);
		}
	}

	// Disarm Excluded Items Now (don't realize until end, don't arm defaults until end)
	for (size_t i = 0; i < vecExc.size(); ++i) {
		auto pIOExc = vecExc[i];
		if (!disarmItem(pIOExc, false, false, pMORM)) {
			LogError(ToStr(pMORM) << " disarmItem() FAILED - " << pIOExc->ToStr());
		}
	}

	// Add Cloned Item To Currently Armed List
	CItemObj* itemPtrClone = NULL;
	pIOCopy->Clone(&itemPtrClone);
	itemPtrClone->m_equiped = TRUE;
	m_currentlyArmedItems->AddTail(itemPtrClone);

	for (auto swapCfg = itemPtrClone->m_dimConfigurations.begin(); swapCfg != itemPtrClone->m_dimConfigurations.end(); swapCfg++) {
		if (swapCfg->dimCfgChangeTo > -1) {
			// Lets get old mat Added by Jonny 10/26/06 for preserve material
			int oldmat = 0;
			if (swapCfg->dimMatPreserve == TRUE) {
				const CharConfigItem* pConfigItem = m_skeletonConfiguration->m_newConfig.getItemByGlidOrBase(GOB_BASE_ITEM);
				const CharConfigItemSlot* oldSlot = pConfigItem != nullptr ? pConfigItem->getSlot(swapCfg->dimCfgIndex) : nullptr;
				if (oldSlot)
					oldmat = oldSlot->m_matlId;
			}

			m_skeletonConfiguration->HotSwapDim(
				(int)swapCfg->dimCfgIndex,
				(int)swapCfg->dimCfgChangeTo,
				pMORM->m_pSkeleton->getSkinnedMeshCount(),
				false);

			if (swapCfg->dimMatPreserve == TRUE) {
				// 'preserve material' is true, we need to swap back in the material
				// index we were using previously because HotSwapDim sets it to 0.
				m_skeletonConfiguration->HotSwapMat(
					(int)swapCfg->dimCfgIndex,
					(int)swapCfg->dimCfgChangeTo,
					oldmat);
			}
			else {
				if (pIOCopy->m_materialOverride != NULL) {
					// custom material
					// this version of HotSwapMat still applies the
					// configuration to the argument skeletal object.
					m_skeletonConfiguration->HotSwapMat(
						(int)swapCfg->dimCfgIndex,
						(int)swapCfg->dimCfgChangeTo,
						pIOCopy->m_materialOverride,
						m_pSkeleton.get(),
						pMORM->m_skeletonConfiguration->m_meshSlots);
				}
				else if (swapCfg->dimMaterial > -1) {
					// EDB material
					m_skeletonConfiguration->HotSwapMat(
						(int)swapCfg->dimCfgIndex,
						(int)swapCfg->dimCfgChangeTo,
						(int)swapCfg->dimMaterial);
				}
			}
		}
	}

	if (pIOCopy->m_attachmentRefList && pIOCopy->m_attachmentRefList->GetCount() > 0) {
		// Obtain item exclusion group
		std::set<int> exclusionGroupIDs;
		exclusionGroupIDs.insert(pIOCopy->m_exclusionGroupIDs.begin(), pIOCopy->m_exclusionGroupIDs.end());

		// change this to now use the GLID.
		// Also remain backward compatible with the editor
		GLID attachmentType = pIOCopy->m_globalID;
		RuntimeSkeleton::eEquipArmState armState;
		armState = m_pSkeleton->ArmEquippableItem(attachmentType, exclusionGroupIDs, this, m_skeletonConfiguration);

#ifdef _ClientOutput
		if (armState == RuntimeSkeleton::eEquipArmState::Success) {
			CArmedInventoryObj* armedPos = EquippableRM::Instance()->GetProxy(attachmentType, true)->GetData();
			if (IS_HAIR_ITEM(armedPos->getLocationOccupancy()))
				removeWOKHair();
		}
#else
		(void)armState; // unused variable.
#endif
		}

#ifdef _ClientOutput
	// If possible maybe this should be changed to a UGC avatar use type?
	if (pIOCopy->m_useType == USE_TYPE_ADD_DYN_OBJ && IS_UGC_GLID(pIOCopy->m_globalID)) {
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return false;

		pICE->LoadUGCActor(pIOCopy->m_globalID);
		m_pSkeleton.reset();
		if (m_skeletonLODList) {
			m_skeletonLODList->SafeDelete();
			delete m_skeletonLODList;
			m_skeletonLODList = NULL;
		}
		m_glid = pIOCopy->m_globalID;
	}
#endif

	LogInfo(ToStr(pMORM) << " OK - " << pIO->ToStr() << " - items=" << armedItems());

	// Arm Default Items That Are Not Excluded (prevent nakedness)
	if (armDefaults)
		armDefaultItemsNotExcluded();

	// Realize Now ?
	if (realizeNow)
		realizeDimConfig(pMORM);

ArmItemSuccess:

	return true;
	}

bool CMovementObj::armItem(const GLID& glid, bool realizeNow, bool armDefaults, CMovementObj* pMORM) {
	if (!IS_VALID_GLID(glid))
		return false;

	if (!pMORM) {
		pMORM = GetMovementObjRefModel(this);
		if (!pMORM)
			return false;
	}

	// Cache Item If Not Already (gets UGC glid metadata synchronously)
	if (!cacheItem(glid)) {
		LogWarn(ToStr(pMORM) << " cacheItem() FAILED - glid<" << glid << "> - Ignoring...");
	}

	// Get Item Object For GLID
	CItemObj* pIO = getItem(glid);
	if (!pIO) {
		// UGC Failures Get Ignored (they will arm later asynchronously)
		if (IS_UGC_GLID(glid)) {
			LogWarn(ToStr(pMORM) << " getItem() FAILED UGC GLID - glid<" << glid << "> - Ignoring...");
			return true;
		}

		LogError(ToStr(pMORM) << " getItem() FAILED - glid<" << glid << ">");
		return false;
	}

	// Don't Arm AP Items If My PassGroup Is Not AP !
	bool allowsAP = AllowsAP();
	bool dontArm = (!allowsAP && CMovementObj::IsItemAP(glid));
	if (dontArm) {
		LogError(ToStr(pMORM) << " AP CONFLICT - " << pIO->ToStr());
		return false;
	}

	// Arm Item Object
	if (!armItem(pIO, realizeNow, armDefaults, pMORM)) {
		LogError(ToStr(pMORM) << " ArmItem() FAILED - " << pIO->ToStr());
		return false;
	}

	return true;
}

// DRF - TODO - This Looks To Never Actually Get Called
bool CMovementObj::armItems(const CCfgObjList* pCOL, bool realizeNow, bool armDefaults, CMovementObj* pMORM) {
	if (!pCOL)
		return false;

	if (!pMORM) {
		pMORM = GetMovementObjRefModel(this);
		if (!pMORM)
			return false;
	}

	if (!m_skeletonConfiguration || !pMORM->m_skeletonConfiguration)
		return false;

	// Initialize Configuration
	m_skeletonConfiguration->InitialDimConfig(pMORM->m_skeletonConfiguration->m_charConfig);

	// Arm Configured Object List Items (don't realize until end, don't arm defaults until end)
	for (POSITION pos = pCOL->GetHeadPosition(); pos;) {
		auto pCO = static_cast<const CCfgObj*>(pCOL->GetNext(pos));
		if (pCO->m_armed == TRUE && pCO->m_quanity > 0) {
			GLID glid = pCO->m_globalCFG;
			if (!armItem(glid, false, false, pMORM)) {
				LogError(ToStr(pMORM) << " ArmItem() FAILED - glid<" << glid << ">");
			}
		}
	}

	LogInfo(ToStr(pMORM) << " OK - items=" << armedItems());

	// Arm Default Items That Are Not Excluded (prevent nakedness) ?
	if (armDefaults)
		armDefaultItemsNotExcluded();

	// Realize Now ?
	if (realizeNow)
		realizeDimConfig(pMORM);

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Disarm Functions
///////////////////////////////////////////////////////////////////////////////

bool CMovementObj::disarmItem(CItemObj* pIO, bool realizeNow, bool armDefaults, CMovementObj* pMORM, bool logOk) {
	if (!pIO)
		return false;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;
	auto pAD = pICE->GetActorDatabase();
	if (!pAD)
		return false;
	auto pMOL = pAD->GetEntObjList();
	if (!pMOL)
		return false;

	if (!pMORM) {
		pMORM = GetMovementObjRefModel(this);
		if (!pMORM)
			return false;
	}

	// Arm/Disarm Allowed ?
	if (!m_allowArmDisarm) {
		LogError(ToStr(pMORM) << " NOT_ALLOWED");
		return false;
	}

	if (m_skeletonConfiguration != nullptr) {
		// Swap skinned mesh based on DIM configuration defined in the item
		// For body mesh only (CC_ITEM_BASE or GOB_BASE_ITEM)
		for (const auto& swapCfg : pIO->m_dimConfigurations) {
			if (swapCfg.dimCfgChangeTo > -1) {
				int oldMaterialIndex = 0;
				if (swapCfg.dimMatPreserve == TRUE) {
					oldMaterialIndex = m_skeletonConfiguration->m_newConfig.getItemSlotMaterialId(GOB_BASE_ITEM, swapCfg.dimCfgIndex);
				}

				const CharConfigItem* pBaseConfigItem = m_skeletonConfiguration->m_charConfig.getItemByGlidOrBase(GOB_BASE_ITEM);
				assert(pBaseConfigItem != nullptr);
				if (pBaseConfigItem == nullptr || pBaseConfigItem->getSlotCount() == 0) {
					continue;
				}

				const CharConfigItemSlot* pSlot = pBaseConfigItem->getSlot(swapCfg.dimCfgIndex);
				assert(pSlot != nullptr);
				if (pSlot == nullptr) {
					continue;
				}

				// Swap in new mesh
				assert(pMORM->m_pSkeleton != nullptr);
				if (pMORM->m_pSkeleton != nullptr) {
					m_skeletonConfiguration->HotSwapDim((int)swapCfg.dimCfgIndex, pSlot->m_meshId, pMORM->m_pSkeleton->getSkinnedMeshCount(), false);
				}

				// Swap in new material
				if (swapCfg.dimMatPreserve == TRUE) {
					// swap back in the material we were just using
					m_skeletonConfiguration->HotSwapMat((int)swapCfg.dimCfgIndex, pSlot->m_meshId, oldMaterialIndex);
				}
				else if (swapCfg.dimMaterial > -1) {
					// This was the old method.  It recalled what was set at character creation. -Jonny
					m_skeletonConfiguration->HotSwapMat((int)swapCfg.dimCfgIndex, pSlot->m_meshId, pSlot->m_matlId);
				}
			}
		}
	}

	if (m_pSkeleton && m_skeletonConfiguration && pIO->m_attachmentRefList && (pIO->m_attachmentRefList->GetCount() > 0)) {
		bool bReapplyKanevaHair = false;
		bool bReapplyKanevaHead = false;

		// Equippable heads the project that just keeps on hacking
		int kanevaHeadGlid = 0;
		for (size_t i = 0; i < m_skeletonConfiguration->m_charConfig.getItemCount(); i++) {
			auto pConfigItem = m_skeletonConfiguration->m_charConfig.getItemByIndex(i);
			if (!pConfigItem)
				continue;
			if (pConfigItem->getGlidOrBase() != GOB_BASE_ITEM) {
				kanevaHeadGlid = pConfigItem->getGlidOrBase();
				break;
			}
		}

		if (!m_pSkeleton->getArmedInventoryList())
			m_pSkeleton->setArmedInventoryList(new CArmedInventoryList());

		// If we are removing a head
		const CArmedInventoryObj* toDisarm = m_pSkeleton->getEquippableItemByGLID(pIO->m_globalID);
		if (toDisarm) {
			if (IS_HEAD_ITEM(toDisarm->getLocationOccupancy())) {
				// if this is not the head you are disarming
				if (kanevaHeadGlid != pIO->m_globalID)
					bReapplyKanevaHead = true;
			}

			if (IS_HAIR_ITEM(toDisarm->getLocationOccupancy()))
				bReapplyKanevaHair = true;
		}

		// Disarm Attachment
		m_pSkeleton->DisarmEquippableItem(pIO->m_globalID);

		// Stick your character creation head back on
		if (bReapplyKanevaHead) {
			//auto pMORef = pMOL->getObjByIndex(static_cast<int>(m_idxRefModel));
			auto pCCI = m_skeletonConfiguration->m_charConfig.getItemByGlidOrBase(kanevaHeadGlid);
			pICE->ConfigPlayerEquippableItem(this, pCCI);
		}

		if (bReapplyKanevaHair)
			reapplyWOKHair();
	}

	if (m_pSkeleton && (pIO->m_animAccessValue != 0))
		m_pSkeleton->setAnimVerPreference(0);

#ifdef _ClientOutput
	if (m_skeletonConfiguration && (pIO->m_useType == USE_TYPE_ADD_DYN_OBJ) && IS_UGC_GLID(pIO->m_globalID)) {
		CMovementObj* pMO_ref = pMOL->getObjByIndex(static_cast<int>(m_idxRefModel));

		m_pSkeleton.reset();

		if (m_skeletonLODList) {
			m_skeletonLODList->SafeDelete();
			delete m_skeletonLODList;
			m_skeletonLODList = NULL;
		}

		if (pMO_ref && pMO_ref->m_pSkeleton) {
			// Clone Reference Model Movement Object Skeleton
			pMO_ref->m_pSkeleton->Clone(&m_pSkeleton, NULL, NULL, NULL, NULL, true, false);
			if (pMO_ref->m_skeletonLODList)
				pMO_ref->m_skeletonLODList->Clone(&m_skeletonLODList);

			// Set Overhead Labels
			auto pCBE = dynamic_cast<ClientBaseEngine*>(pICE);
			if (pCBE)
				pCBE->setOverheadLabels(this);
		}

		m_glid = GLID_INVALID;

		m_skeletonConfiguration->m_lastConfig.disposeAllItems();

		// Config the equippable items.
		for (size_t i = CC_ITEM_FIRST_EQUIP; i < m_skeletonConfiguration->m_charConfig.getItemCount(); i++)
			pICE->ConfigPlayerEquippableItem(this, m_skeletonConfiguration->m_charConfig.getItemByIndex(i));
	}
#endif

	// Lock Currently Armed Items List
	if (!m_currentlyArmedItems)
		return false;

	// Remove Item From List Of Currently Armed Items
	OBJ_LIST_GUARD(m_currentlyArmedItems);
	POSITION posLast;
	for (POSITION pos = m_currentlyArmedItems->GetHeadPosition(); (posLast = pos) != NULL;) {
		auto pIOList = (CItemObj*)m_currentlyArmedItems->GetNext(pos);
		if (pIO == pIOList) {
			m_currentlyArmedItems->RemoveAt(posLast);
			break;
		}
	}

	if (logOk) {
		LogInfo(ToStr(pMORM) << " OK - " << pIO->ToStr() << " - items=" << armedItems());
	}

	// Delete Item
	delete pIO;

	// Arm Default Items That Are Not Excluded (prevent nakedness) ?
	if (armDefaults)
		armDefaultItemsNotExcluded();

	// Realize Now ?
	if (realizeNow)
		realizeDimConfig(pMORM);

	return true;
}

bool CMovementObj::disarmItem(const GLID& glid, bool realizeNow, bool armDefaults, CMovementObj* pMORM) {
	if (!IS_VALID_GLID(glid))
		return false;

	if (!pMORM) {
		pMORM = GetMovementObjRefModel(this);
		if (!pMORM)
			return false;
	}

	// Nothing Currently Armed ?
	if (!armedItems()) {
		LogError(ToStr(pMORM) << " NOTHING ARMED");
		return false;
	}

	// DRF - Crash Fix - Be Sure To Unlock Before Return!
	if (!m_currentlyArmedItems)
		return false;

	// Find Matching Item In Armed Items List And Disarm
	OBJ_LIST_GUARD(m_currentlyArmedItems);
	bool found = false;
	CItemObj* pIO = NULL;
	for (POSITION pos = m_currentlyArmedItems->GetHeadPosition(); pos;) {
		pIO = (CItemObj*)m_currentlyArmedItems->GetNext(pos);
		if (!pIO)
			continue;

		// Found Matching Item ?
		if (pIO->m_globalID == glid) {
			found = true;
			break;
		}
	}

	// Not Found ?
	if (!found) {
		LogError(ToStr(pMORM) << " NOT_ARMED - glid<" << glid << ">");
		return false;
	}

	// Disarm Item
	if (!disarmItem(pIO, realizeNow, armDefaults, pMORM)) {
		LogError(ToStr(pMORM) << " disarmItem() FAILED - " << pIO->ToStr());
		return false;
	}

	return true;
}

bool CMovementObj::disarmAllItems(bool realizeNow, bool armDefaults, CMovementObj* pMORM) {
	if (!pMORM) {
		pMORM = GetMovementObjRefModel(this);
		if (!pMORM)
			return false;
	}

	// Nothing Currently Armed ?
	LogInfo(ToStr(pMORM) << " OK - items=" << armedItems()); //...");
	if (!armedItems())
		return true;

	// DRF - Crash Fix - Be Sure To Unlock Before Return!
	if (!m_currentlyArmedItems)
		return false;

	// Disarm All Currently Armed Items
	OBJ_LIST_GUARD(m_currentlyArmedItems);
	for (POSITION pos = m_currentlyArmedItems->GetHeadPosition(); pos;) {
		auto pIO = (CItemObj*)m_currentlyArmedItems->GetNext(pos);
		if (!pIO)
			continue;

		// Disarm Item (don't realize until end, don't arm defaults until end)
		if (!disarmItem(pIO, false, false, pMORM, false)) {
			LogError(ToStr(pMORM) << " disarmItem() FAILED - " << pIO->ToStr());
			// just do our best anyway
		}
	}

	//	LogInfo(ToStr(pMORM) << " OK - items=" << armedItems());

	// Arm Default Items That Are Not Excluded (prevent nakedness) ?
	if (armDefaults)
		armDefaultItemsNotExcluded();

	// Realize Now ?
	if (realizeNow)
		realizeDimConfig(pMORM);

	return true;
}

} // namespace KEP