///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CFireRangeClass.h"
#include "common/include/IMemSizeGadget.h"

#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

namespace KEP {

CFireRangeObject::CFireRangeObject() {
	m_animationIndex = -1; // Legacy animation index
	m_animationType = -1;
	m_animationVersion = 0;
	m_animationFrameRangeStart = 0;
	m_animationFrameRangeEnd = 3;
	m_boneIndex = 1;
	m_missileIndex = 0;
	m_inRangeCurrently = FALSE;
	m_hardPointAdjust.x = 0.0f;
	m_hardPointAdjust.y = 0.0f;
	m_hardPointAdjust.z = 0.0f;
	m_accuracyRating = 0.0f;
}

void CFireRangeObject::Clone(CFireRangeObject** clone) {
	CFireRangeObject* copyLocal = new CFireRangeObject();
	copyLocal->m_animationIndex = m_animationIndex;
	copyLocal->m_animationType = m_animationType;
	copyLocal->m_animationVersion = m_animationVersion;
	copyLocal->m_animationFrameRangeStart = m_animationFrameRangeStart;
	copyLocal->m_animationFrameRangeEnd = m_animationFrameRangeEnd;
	copyLocal->m_boneIndex = m_boneIndex;
	copyLocal->m_missileIndex = m_missileIndex;
	copyLocal->m_inRangeCurrently = FALSE;
	copyLocal->m_accuracyRating = m_accuracyRating;
	copyLocal->m_timerLastFired = m_timerLastFired;

	copyLocal->m_hardPointAdjust = m_hardPointAdjust;

	*clone = copyLocal;
}

void CFireRangeObjectList::Clone(CFireRangeObjectList** clone) {
	CFireRangeObjectList* copyLocal = new CFireRangeObjectList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CFireRangeObject* spnPtr = (CFireRangeObject*)GetNext(posLoc);
		CFireRangeObject* clonedPtr = NULL;
		spnPtr->Clone(&clonedPtr);
		copyLocal->AddTail(clonedPtr);
	}

	*clone = copyLocal;
}

void CFireRangeObjectList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CFireRangeObject* spnPtr = (CFireRangeObject*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CFireRangeObject::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		ar << m_animationIndex
		   << m_animationFrameRangeStart
		   << m_animationFrameRangeEnd
		   << m_boneIndex
		   << m_missileIndex
		   << m_hardPointAdjust.x
		   << m_hardPointAdjust.y
		   << m_hardPointAdjust.z;

		ar << m_animationType
		   << m_animationVersion
		   << m_accuracyRating;
	} else {
		int version = ar.GetObjectSchema();

		ar >> m_animationIndex >> m_animationFrameRangeStart >> m_animationFrameRangeEnd >> m_boneIndex >> m_missileIndex >> m_hardPointAdjust.x >> m_hardPointAdjust.y >> m_hardPointAdjust.z;

		m_accuracyRating = 0.0f;

		if (version == 2) {
			ar >> reserved >> reserved >> m_accuracyRating;
		} else if (version >= 3) {
			ar >> m_animationType >> m_animationVersion >> m_accuracyRating;
		}

		m_inRangeCurrently = FALSE;
	}
}

void CFireRangeObject::SetAnimationInfo(int type, int ver) {
	m_animationType = type;
	m_animationVersion = ver;
	m_animationIndex = -1; // Invalidate legacy animation index
}

void CFireRangeObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);
	}
}

void CFireRangeObjectList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CFireRangeObject* pCFireRangeObject = static_cast<const CFireRangeObject*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCFireRangeObject);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CFireRangeObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CFireRangeObjectList, CObList)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CFireRangeObject, IDS_FIRERANGEOBJECT_NAME)
GETSET_RANGE(m_animationFrameRangeStart, gsInt, GS_FLAGS_DEFAULT, 0, 0, FIRERANGEOBJECT, ANIMATIONFRAMERANGESTART, INT_MIN, INT_MAX)
GETSET_RANGE(m_animationFrameRangeEnd, gsInt, GS_FLAGS_DEFAULT, 0, 0, FIRERANGEOBJECT, ANIMATIONFRAMERANGEEND, INT_MIN, INT_MAX)
GETSET_RANGE(m_boneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, FIRERANGEOBJECT, BONEINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_missileIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, FIRERANGEOBJECT, MISSILEINDEX, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_hardPointAdjust, GS_FLAGS_DEFAULT, 0, 0, FIRERANGEOBJECT, HARDPOINTADJUST)
GETSET_RANGE(m_accuracyRating, gsFloat, GS_FLAGS_DEFAULT, 0, 0, FIRERANGEOBJECT, ACCURACYRATING, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP
