///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CPersistClass.h"

namespace KEP {

CPersistObj::CPersistObj(int entDBIndex, const Matrix44f& entMatrix, eActorControlType controlType) {
	m_entControlType = controlType;
	m_entDBIndex = entDBIndex;
	m_entMatrix = entMatrix;
}

void CPersistObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_entDBIndex
		   << static_cast<int>(m_entControlType);
		//arrays ect
		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar << m_entMatrix[loop][loop2];

	} else {
		int controlType;
		ar >> m_entDBIndex >> controlType;

		m_entControlType = static_cast<eActorControlType>(controlType);

		//arrays ect
		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar >> m_entMatrix[loop][loop2];
	}
}

IMPLEMENT_SERIAL(CPersistObj, CObject, 0)
IMPLEMENT_SERIAL(CPersistObjList, CObList, 0)

BEGIN_GETSET_IMPL(CPersistObj, IDS_PERSISTOBJ_NAME)
GETSET_RANGE(m_entDBIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, PERSISTOBJ, ENTDBINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_entControlType, gsInt, GS_FLAGS_DEFAULT, 0, 0, PERSISTOBJ, ENTCONTROLTYPE, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP