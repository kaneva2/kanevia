///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include <direct.h>
#include "CAccountClass.h"
#include <KEPException.h>
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CPlayerClass.h"
#include "CSkillClass.h"
#include <TinyXML/tinyxml.h>
#include <KEPHelpers.h>
#include <SerializeHelper.h>
#include "UnicodeMFCHelpers.h"
#include "Core/Crypto/Crypto_MD5.h"

namespace KEP {

const TimeMs CAccountObject::BAD_EL_TIME = -1e10;

#include "Audit.h"

std::atomic<USHORT> SpawnInfo::s_nextSpawnId = 1; // 0 is invalid one

CAccountObject::CAccountObject(const CStringA& name, const CStringA& password) :
		m_serverId(0), m_scriptServerId(0), m_scriptServerIdPrev(0), m_showMature(false), m_age(0), m_gender('U'), m_logonTimer(Timer::State::Paused) {
	m_localClientContentVersion = 0;
	m_accountName = name;
	m_accountPassword = password;
	m_loggedOn = FALSE; //online status
	m_sentLogoff = FALSE;
	m_accountStatus = 0; //0=ok 1=suspended for payment reasons 2=suspended for violation
	m_suspentionNotes = "None"; //description of suspension reasons
	m_netId = 0; //
	m_currentProtocolVersion = INVALID_PROTOCOL_VERSION;
	m_registeredVersion = FALSE;
	m_timePlayedMinutes = 0;
	m_tempPlayedTimeMilliseconds = 0;
	m_pPlayerObjectList = NULL;
	m_currentcharInUse = -1;
	m_accountNumber = 0;
	m_aiServer = FALSE;
	m_securityIndexer = 0;
	m_administratorAccess = FALSE;

	m_realName = "Unknown"; //as apears on credit card
	m_email = "unknown@unknown.com";
	m_yearOfLastLogon = 0;
	m_monthOfLastLogon = 0;
	m_dayOfLastLogon = 0;
	m_hourOfLastLogon = 0;
	m_minuteOfLastLogon = 0;
	m_refreshPaymentYear = 0;
	m_refreshPaymentMonth = 0;
	m_refreshPaymentDay = 0;
	m_paidMonths = 0;
	m_initializedHousingStates = FALSE;
	m_gm = FALSE;
	m_canSpawn = FALSE;
	m_arenaLiscense = 0;

	StampLogOn();
}

CAccountObject::~CAccountObject() {}

void CAccountObject::SerializeAlloc(CArchive& ar) {
	int schema = CArchiveObjectSchemaVersion<CAccountObject>; // since not using Serialize to construct it, must hack this version in

	if (ar.IsStoring()) {
		ar << m_accountName
		   << m_accountPassword
		   << m_accountStatus
		   << m_suspentionNotes
		   << m_registeredVersion
		   << m_timePlayedMinutes
		   << schema;

		if (!m_pPlayerObjectList) {
			ar << -1;
		} else {
			ar << (int)m_pPlayerObjectList->GetCount();
			m_pPlayerObjectList->SerializeAlloc(ar, 0);
		}
		//save char List here

		ar << m_accountNumber
		   << m_aiServer
		   << (int)0 // m_housingBind
		   << m_gm
		   << m_canSpawn
		   << m_administratorAccess
		   << m_realName
		   << CStringA(m_currentIP.c_str())
		   << m_yearOfLastLogon
		   << m_monthOfLastLogon
		   << m_dayOfLastLogon
		   << m_hourOfLastLogon
		   << m_minuteOfLastLogon
		   << m_refreshPaymentYear
		   << m_refreshPaymentMonth
		   << m_refreshPaymentDay
		   << m_paidMonths;

		// version 2 stuff
		ar << m_serverId;

	} else {
		ar >> m_accountName >> m_accountPassword >> m_accountStatus >> m_suspentionNotes >> m_registeredVersion >> m_timePlayedMinutes >> schema;

		int charListCount = 0;
		ar >> charListCount;
		if (charListCount == -1)
			m_pPlayerObjectList = NULL;
		else { //allocate and fill in need be
			m_pPlayerObjectList = new CPlayerObjectList();
			m_pPlayerObjectList->SerializeAlloc(ar, charListCount);
		} //end allocate and fill in need be
		//restore list

		int oldHousingBind;
		CStringA ip;
		ar >> m_accountNumber >> m_aiServer >> oldHousingBind >> m_gm >> m_canSpawn >> m_administratorAccess >> m_realName >> ip // m_currentIP
			>> m_yearOfLastLogon >> m_monthOfLastLogon >> m_dayOfLastLogon >> m_hourOfLastLogon >> m_minuteOfLastLogon >> m_refreshPaymentYear >> m_refreshPaymentMonth >> m_refreshPaymentDay >> m_paidMonths;

		m_currentIP = ip.GetBuffer();

		if (schema >= 2) {
			ar >> m_serverId;
		} else {
			m_serverId = 0;
		}

		m_loggedOn = FALSE;
		m_netId = 0;
		m_currentProtocolVersion = INVALID_PROTOCOL_VERSION;
		m_tempPlayedTimeMilliseconds = 0;
		m_currentcharInUse = -1;
		m_securityIndexer = 0;
		m_initializedHousingStates = FALSE;
		m_arenaLiscense = 0;
		m_localClientContentVersion = 0;
	}
}

void CAccountObject::SafeDelete() {
	if (m_pPlayerObjectList) {
		m_pPlayerObjectList->SafeDelete();
		delete m_pPlayerObjectList;
		m_pPlayerObjectList = 0;
	}
}

void CAccountObject::SetOptimizedSave(BOOL optimize) {
	if (m_pPlayerObjectList)
		for (POSITION cPos = m_pPlayerObjectList->GetHeadPosition(); cPos != NULL;) {
			CPlayerObject* plPtr = (CPlayerObject*)m_pPlayerObjectList->GetNext(cPos);
			if (plPtr->m_inventory)
				plPtr->m_inventory->SetOptimizedSave(optimize);
			if (plPtr->m_bankInventory)
				plPtr->m_bankInventory->SetOptimizedSave(optimize);
			if (plPtr->m_skills)
				plPtr->m_skills->SetOptimizedSave(optimize);
			if (plPtr->m_reconfigInven)
				plPtr->m_reconfigInven->SetOptimizedSave(optimize);
		}
}

void CAccountObject::RestoreAccountFromOptimized(CSkillObjectList* skillDB,
	CGlobalInventoryObjList* globalInven) {
	if (m_pPlayerObjectList)
		for (POSITION cPos = m_pPlayerObjectList->GetHeadPosition(); cPos != NULL;) {
			//char loop
			CPlayerObject* plPtr = (CPlayerObject*)m_pPlayerObjectList->GetNext(cPos);
			POSITION posLast;
			if (plPtr->m_inventory) //inventory
				for (POSITION posLoc = plPtr->m_inventory->GetHeadPosition(); (posLast = posLoc) != NULL;) {
					//inven loop
					CInventoryObj* invObj = (CInventoryObj*)plPtr->m_inventory->GetNext(posLoc);
					CGlobalInventoryObj* globalPtr = NULL;
					if (invObj->m_itemData)
						globalPtr = globalInven->GetByGLID(invObj->m_itemData->m_globalID);

					if (globalPtr) {
						//update
						invObj->m_itemData = 0;
						invObj->m_itemData = globalPtr->m_itemData;
					} //end update
					else { //remove
						invObj->SafeDelete();
						delete invObj;
						invObj = 0;
						plPtr->m_inventory->RemoveAt(posLast);
					} //end remove
				} //end inven loop
			if (plPtr->m_bankInventory) //bank inventory
				for (POSITION posLoc = plPtr->m_bankInventory->GetHeadPosition(); (posLast = posLoc) != NULL;) {
					CInventoryObj* invObj = (CInventoryObj*)plPtr->m_bankInventory->GetNext(posLoc);
					//WriteDebugFile(invObj->m_itemData->m_itemName + ".TXT",invObj->m_itemData->m_itemName);//REMOVE
					CGlobalInventoryObj* globalPtr = NULL;
					if (invObj->m_itemData)
						globalPtr = globalInven->GetByGLID(invObj->m_itemData->m_globalID);
					if (globalPtr) {
						//update
						invObj->m_itemData = 0;
						invObj->m_itemData = globalPtr->m_itemData;
					} //end update
					else { //remove
						invObj->SafeDelete();
						delete invObj;
						invObj = 0;
						plPtr->m_bankInventory->RemoveAt(posLast);
					} //end remove
				}
			if (plPtr->m_reconfigInven) //reconfig inventory
				for (POSITION posLoc = plPtr->m_reconfigInven->GetHeadPosition(); (posLast = posLoc) != NULL;) {
					CInventoryObj* invObj = (CInventoryObj*)plPtr->m_reconfigInven->GetNext(posLoc);
					//WriteDebugFile(invObj->m_itemData->m_itemName + ".TXT",invObj->m_itemData->m_itemName);//REMOVE
					CGlobalInventoryObj* globalPtr = NULL;
					if (invObj->m_itemData)
						globalPtr = globalInven->GetByGLID(invObj->m_itemData->m_globalID);
					if (globalPtr) {
						//update
						invObj->m_itemData = 0;
						invObj->m_itemData = globalPtr->m_itemData;
					} //end update
					else { //remove
						invObj->SafeDelete();
						delete invObj;
						invObj = 0;
						plPtr->m_reconfigInven->RemoveAt(posLast);
					} //end remove
				}
		} //end char loop
}

void CAccountObject::WriteDebugFile(CStringA filename, CStringA debugString) {
	BEGIN_SERIALIZE_TRY

	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(Utf8ToUtf16(filename).c_str(), CFile::modeCreate | CFile::modeWrite, &exc);
	if (res) {
		CArchive the_out_Archive(&fl, CArchive::store);
		WriteString(the_out_Archive, debugString);
		the_out_Archive.Close();
		fl.Close();
	} else {
		LOG_EXCEPTION_NO_LOGGER(exc)
	}

	END_SERIALIZE_TRY_NO_LOGGER
}

CStringA CAccountObject::ConvLongToString(DWORD number) {
	CStringA temp;
	char buffer[34];
	_ultoa_s(number, buffer, _countof(buffer), 10);
	temp = buffer;
	return temp;
}

CPlayerObject* CAccountObject::GetPlayerObjectByIndex(int index) {
	if (index < 0)
		return nullptr;
	POSITION posLoc = m_pPlayerObjectList->FindIndex(index);
	return posLoc ? (CPlayerObject*)m_pPlayerObjectList->GetAt(posLoc) : nullptr;
}

CPlayerObject* CAccountObject::GetCurrentPlayerObject() {
	return GetPlayerObjectByIndex(m_currentcharInUse);
}

CPlayerObject* CAccountObject::GetCurrentPlayerObjectOrDefault() {
	auto pPO = GetCurrentPlayerObject();
	return pPO ? pPO : GetPlayerObjectByIndex(0);
}

void CAccountObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_accountName
		   << m_accountPassword
		   << m_accountStatus
		   << m_suspentionNotes
		   << m_registeredVersion
		   << m_timePlayedMinutes
		   << m_pPlayerObjectList
		   << m_accountNumber
		   << m_aiServer
		   << (int)0 // m_housingBind
		   << m_gm
		   << m_canSpawn
		   << m_administratorAccess
		   << m_realName
		   << CStringA(m_currentIP.c_str())
		   << m_yearOfLastLogon
		   << m_monthOfLastLogon
		   << m_dayOfLastLogon
		   << m_hourOfLastLogon
		   << m_minuteOfLastLogon
		   << m_refreshPaymentYear
		   << m_refreshPaymentMonth
		   << m_refreshPaymentDay
		   << m_paidMonths;

		// version 2 stuff
		ar << m_serverId;
	} else {
		int oldHousingBind;
		int schema = ar.GetObjectSchema();
		CStringA ip;
		ar >> m_accountName >> m_accountPassword >> m_accountStatus >> m_suspentionNotes >> m_registeredVersion >> m_timePlayedMinutes >> m_pPlayerObjectList >> m_accountNumber >> m_aiServer >> oldHousingBind >> m_gm >> m_canSpawn >> m_administratorAccess >> m_realName >> ip // m_currentIP
			>> m_yearOfLastLogon >> m_monthOfLastLogon >> m_dayOfLastLogon >> m_hourOfLastLogon >> m_minuteOfLastLogon >> m_refreshPaymentYear >> m_refreshPaymentMonth >> m_refreshPaymentDay >> m_paidMonths;

		m_currentIP = ip.GetBuffer();

		if (schema >= 2) {
			ar >> m_serverId;
		} else {
			m_serverId = 0;
		}

		m_loggedOn = FALSE;
		m_netId = 0;
		m_currentProtocolVersion = INVALID_PROTOCOL_VERSION;
		m_tempPlayedTimeMilliseconds = 0;
		m_currentcharInUse = -1;
		m_securityIndexer = 0;
		m_initializedHousingStates = FALSE;
		m_localClientContentVersion = 0;
	}
}

void CAccountObject::StampLogOn() {
	CTime t = CTime::GetCurrentTime();

	m_yearOfLastLogon = t.GetYear();
	m_monthOfLastLogon = t.GetMonth();
	m_dayOfLastLogon = t.GetDay();
	m_hourOfLastLogon = t.GetHour();
	m_minuteOfLastLogon = t.GetMinute();
}

void CAccountObject::MakePayment(int monthsPaid) {
	CTime t = CTime::GetCurrentTime();

	m_refreshPaymentYear = t.GetYear();
	m_refreshPaymentMonth = t.GetMonth();
	m_refreshPaymentDay = t.GetDay();
	m_paidMonths = monthsPaid;
}

BOOL CAccountObject::IsAccountUpToDateOnPayments(int buffer) {
	CTime t1 = CTime::GetCurrentTime();
	CTime t2(m_refreshPaymentYear, m_refreshPaymentMonth, m_refreshPaymentDay, 1, 1, 0);
	CTimeSpan ts = t1 - t2;

	long paidDayCount = m_paidMonths * 31;
	long daysSincePayment = (long)ts.GetDays();

	if (daysSincePayment > (paidDayCount + buffer))
		return FALSE;

	return TRUE;
}

void CAccountObjectList::MakePaymentByUsernameAndPass(CStringA username,
	CStringA password,
	int monthsPaid) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);
		if (pAO->m_accountName == username && pAO->m_accountPassword == password)
			pAO->MakePayment(monthsPaid);
	} //account list
}

void CAccountObjectList::GenerateLogonRecordBasedOnTime(int minutes,
	int hours,
	int days,
	int months,
	int years) {
	CTime t = CTime::GetCurrentTime();
	int y = t.GetYear();
	int m = t.GetMonth();
	int d = t.GetDay();
	int h = t.GetHour();
	int mi = t.GetMinute();

	minutes = minutes + (hours * 60);
	int daysTotal = days + (months * 30) + (years * 356);

	CTime t1(y, m, d, h, mi, 0); // 10:15PM March 19, 1999

	//int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec,
	BEGIN_SERIALIZE_TRY

	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(L"Logon_Report.txt", CFile::modeCreate | CFile::modeWrite, &exc);
	if (res) {
		CArchive the_out_Archive(&fl, CArchive::store);

		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			auto pAO = (CAccountObject*)GetNext(posLoc);

			CTime t2(pAO->m_yearOfLastLogon, pAO->m_monthOfLastLogon, pAO->m_dayOfLastLogon, pAO->m_hourOfLastLogon, pAO->m_minuteOfLastLogon, 0); // 10:15PM March 20, 1999
			CTimeSpan ts = t1 - t2;

			if (pAO->m_timePlayedMinutes == 0)
				continue;

			long daysPast = (long)ts.GetDays();
			if (daysTotal < daysPast)
				continue;

			if (daysTotal == 0) {
				long minutePast = (long)ts.GetTotalMinutes();
				if (minutes < minutePast)
					continue;
			}

			CStringA build = pAO->m_accountName;
			build = operator+(build, "-Total minutes played:");
			build = operator+(build, ConvLongToString(pAO->m_timePlayedMinutes));
			build = operator+(build, "-Last Log On date:");

			CStringW lastLoggedOnformat = t2.Format(L"%A, %B %d, %Y");
			build = operator+(build, Utf16ToUtf8(lastLoggedOnformat).c_str());

			WriteString(the_out_Archive, build);
			WriteString(the_out_Archive, "\n");
		}
		the_out_Archive.Close();
		fl.Close();
	} else {
		LOG_EXCEPTION_NO_LOGGER(exc)
	}

	END_SERIALIZE_TRY_NO_LOGGER

	GenerateAccountDataFile();
}

void CAccountObjectList::GenerateAccountDataFile() {
	BEGIN_SERIALIZE_TRY

	//int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec,
	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(L"AccountData.txt", CFile::modeCreate | CFile::modeWrite, &exc);
	if (res) {
		CArchive the_out_Archive(&fl, CArchive::store);

		//Values('XXXX','ssss@sss.com','password', 50)
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
			auto pAO = (CAccountObject*)GetNext(posLoc);

			//Insert into tblContacts(UserName, Password, Minutes) Values('testuser', '12345', 43231) UPDATE tblContacts SET Minutes=43231 where UserName='testuser' and password='12345'
			CStringA build = "Insert Into tblUsers (UserName, password, minutes) Values('";
			build = operator+(build, pAO->m_accountName);
			build = operator+(build, "','");
			build = operator+(build, pAO->m_accountPassword);
			build = operator+(build, "',");
			build = operator+(build, ConvLongToString(pAO->m_timePlayedMinutes));
			build = operator+(build, ") Update tblUsers SET Minutes=");
			build = operator+(build, ConvLongToString(pAO->m_timePlayedMinutes));
			build = operator+(build, " where UserName='");
			build = operator+(build, pAO->m_accountName);
			build = operator+(build, "' and password='");
			build = operator+(build, pAO->m_accountPassword);
			build = operator+(build, "'\n");

			WriteString(the_out_Archive, build);
		}
		the_out_Archive.Close();
		fl.Close();
	} else {
		LOG_EXCEPTION_NO_LOGGER(exc)
	}

	END_SERIALIZE_TRY_NO_LOGGER
}

void CAccountObjectList::RemoveAccountBasedOnTime(int days, BOOL zeroMinOnly) {
	CTime t = CTime::GetCurrentTime();
	int y = t.GetYear();
	int m = t.GetMonth();
	int d = t.GetDay();
	int h = t.GetHour();
	int mi = t.GetMinute();

	CTime t1(y, m, d, h, mi, 0); // 10:15PM March 19, 1999

	POSITION posLast = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) { //account list
		auto pAO = (CAccountObject*)GetNext(posLoc);

		CTime t2(pAO->m_yearOfLastLogon, pAO->m_monthOfLastLogon, pAO->m_dayOfLastLogon, pAO->m_hourOfLastLogon, pAO->m_minuteOfLastLogon, 0); // 10:15PM March 20, 1999
		CTimeSpan ts = t1 - t2;

		long daysPast = (long)ts.GetDays();

		if (daysPast < days)
			continue;

		if (pAO->m_timePlayedMinutes != 0 && zeroMinOnly == TRUE)
			continue;

		pAO->SafeDelete();
		delete pAO;
		pAO = 0;
		RemoveAt(posLast);
	}
}

CStringA CAccountObjectList::ConvLongToString(DWORD number) {
	CStringA temp;
	char buffer[34];
	_ultoa_s(number, buffer, _countof(buffer), 10);
	temp = buffer;
	return temp;
}

void CAccountObjectList::ExpandFromOptimized(CSkillObjectList* skillDB, CGlobalInventoryObjList* pGIOL) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);
		pAO->RestoreAccountFromOptimized(skillDB, pGIOL);
	}
}

void CAccountObjectList::FlagAllForOptimizedSave() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);
		pAO->SetOptimizedSave(TRUE);
	}
}

void CAccountObjectList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);
		pAO->SafeDelete();
		delete pAO;
		pAO = 0;
	}
	RemoveAll();
}

PLAYER_HANDLE CAccountObjectList::GetNewLocalPlayerHandleID() {
	USHORT trace = 1;

	// changed to ensure <= USHORT_MAX
	std::vector<USHORT> usedHandles;

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);

		for (POSITION cPos = pAO->m_pPlayerObjectList->GetHeadPosition(); cPos != NULL;) {
			auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetNext(cPos);
			if (pPO->m_handle < MAX_LOCAL_PLAYER_HANDLE) // anything above this can't be local
				usedHandles.push_back(pPO->m_handle);
		}
	}

	trace = 0;
	if (usedHandles.empty())
		trace = 1;
	else {
		// find empty slot, this should be fast, but could sort/binary search if expect big list
		for (trace = 1; trace <= MAX_LOCAL_PLAYER_HANDLE; trace++) {
			std::vector<USHORT>::iterator i;
			for (i = usedHandles.begin(); i != usedHandles.end(); i++) {
				if (trace == *i)
					break; // it is used, continue
			}
			if (i == usedHandles.end())
				break; // found empty slot
		}
	}
	if (trace >= MAX_LOCAL_PLAYER_HANDLE)
		trace = 0; // can't have > USHORT_MAX local

	ASSERT(trace != 0); //

	return trace;
}

void CAccountObjectList::SetOptimizedSave(BOOL optimize) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		auto pAO = (CAccountObject*)GetNext(posLoc);
		pAO->SetOptimizedSave(optimize);
	}
}

void CAccountObjectList::RemoveAccountByAccNumWithoutDel(int number) {
	POSITION posLast = NULL;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) { //account list
		auto pAO = (CAccountObject*)GetNext(posLoc);
		if (pAO->m_accountNumber == number)
			RemoveAt(posLast);
	}
}

int CAccountObjectList::LogOn(
	const char* userName,
	const char* password,
	const char* currentIP,
	CAccountObject** ppAO_out,
	CAccountObjectList* pAOL,
	ULONG usersServerId) {
	static LogInstance("Instance");

	int ret = LOGIN_NOT_FOUND;
	bool validatedViaWS = usersServerId != 0; // if non-zero usersServerId, they authenticated to Kaneva

	// first check to see if we have the account in memory already
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		// account list
		CAccountObject* pAO = (CAccountObject*)GetNext(posLoc);

		if (pAO->m_accountName.CompareNoCase(userName) == 0) {
			// found account
			if (pAO->m_loggedOn == TRUE) {
				//They are alreaady logged on so dont add them to any account lists
				if (validatedViaWS || pAO->m_accountPassword == password)
					return LOGIN_OK;
			}

			if (validatedViaWS || pAO->m_accountPassword == password) { // pw already ok, or matched local pw
				// if have a usersServerId, must match
				if (usersServerId != 0 && usersServerId != pAO->m_serverId) {
					LogWarn(loadStr(IDS_ACCOUNT_COLLISION) << userName << ":" << usersServerId);
					return LOGIN_SUSPENDED;
				}

				if (pAO->m_accountStatus == ACCTSTAT_SUSPENDED_NON_PAYMENT)
					return LOGIN_SUSPENDED;

				if (pAO->m_accountStatus == ACCTSTAT_SUSPENDED_VIOLATION)
					return LOGIN_NONPAYMENT;

				*ppAO_out = pAO;

				ret = LOGIN_OK;
			}

			break;
		} // found account
	}

	// no account found in svr, but validated on server, add them on the fly
	if (ret == LOGIN_NOT_FOUND && validatedViaWS && usersServerId != 0) {
		ASSERT(ppAO_out);
		*ppAO_out = new CAccountObject(userName, ""); // empty pw
		(*ppAO_out)->m_serverId = usersServerId;
		(*ppAO_out)->m_registeredVersion = TRUE;
		(*ppAO_out)->m_pPlayerObjectList = new CPlayerObjectList();
		if (usersServerId != 0 && m_gameStateDb != 0)
			(*ppAO_out)->m_accountNumber = 0;
		else
			(*ppAO_out)->m_accountNumber = GetUntakenAccountNumber();

		//
		// already in CS that protects this so, no need to lock (sectionLock
		// that's passed in)
		//
		AddTail(*ppAO_out);

		ret = LOGIN_OK;
	}

	// at this point everything is ok, we have the account
	if (ret == LOGIN_OK) {
		(*ppAO_out)->m_sentLogoff = FALSE; // in case re-using pointer
		(*ppAO_out)->StampLogOn();
		(*ppAO_out)->m_initializedHousingStates = FALSE;
		(*ppAO_out)->m_currentIP = currentIP;
		(*ppAO_out)->m_securityIndexer = 0; // reset indxer
		(*ppAO_out)->m_logonTimer.Reset().Start();
		pAOL->AddTail(*ppAO_out);
	}

	return ret;
}

int CAccountObjectList::EstablishLinkToAccount(
	CStringA userName,
	CStringA password,
	NETID netIdCur,
	CAccountObject** ppAO_out) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);
		if (pAO->m_accountName.CompareNoCase(userName) == 0 && (pAO->m_accountPassword == password || pAO->m_serverId != 0)) {
			*ppAO_out = pAO;

			if (pAO->m_loggedOn == FALSE) {
				pAO->m_netId = netIdCur;
				pAO->m_loggedOn = TRUE;
				return ELA_LOGON;
			} else {
				return ELA_RECONNECT;
			}
		}
	}
	return ELA_FAIL;
}

int CAccountObjectList::LogOut(CAccountObject* pAO, LONG* minutes) {
	int ret = 0; //ok
	if (minutes)
		*minutes = 0;

	fTime::Ms logDuration = 0;
	if (pAO && pAO->m_loggedOn == TRUE) { // 8/15/05 break up if so always remove
		pAO->m_loggedOn = FALSE;
		pAO->m_arenaLiscense = 0;
		logDuration = pAO->m_logonTimer.ElapsedMs();
		if (minutes != 0)
			*minutes = (long)(unsigned long)logDuration;
		pAO->m_timePlayedMinutes += (long)(unsigned long)logDuration;
	}

	return ret;
}

void CAccountObjectList::ReinitMemory() {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pAO = (CAccountObject*)GetNext(pos);
		if (pAO && pAO->m_pPlayerObjectList)
			pAO->m_pPlayerObjectList->ReinitMemory();
	}
}

int CAccountObjectList::GetUntakenAccountNumber() {
	int tracer = 1;

	int accountTotal = (int)GetCount();
	if (tracer == 0)
		return tracer;
	int accountBounds = accountTotal + 1;
	int* intArray = new int[accountBounds];
	ZeroMemory(intArray, (accountBounds * 4));
	accountBounds = accountTotal + 1;

	POSITION posLoc;
	for (posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);
		if (pAO->m_accountNumber < accountBounds)
			intArray[pAO->m_accountNumber] = 1;
	}

	for (int i = 1; i < accountBounds; i++) {
		if (intArray[i] == 0) {
			delete[] intArray;
			intArray = 0;
			return i;
		}
	}
	delete[] intArray;
	intArray = 0;

	for (posLoc = GetHeadPosition(); posLoc != NULL;) {
		auto pAO = (CAccountObject*)GetNext(posLoc);
		if (pAO->m_accountNumber >= tracer)
			tracer = pAO->m_accountNumber + 1;
	}

	return tracer;
}

bool CAccountObjectList::SafeDeleteAccount(CAccountObject* pAO) {
	if (!pAO)
		return false;

	POSITION delPos;
	for (POSITION posLoc = GetHeadPosition(); (delPos = posLoc) != NULL;) {
		auto pAO_find = (CAccountObject*)GetNext(posLoc);
		if (pAO_find == pAO) {
			RemoveAt(delPos);
			pAO->SafeDelete();
			delete pAO;
			return true;
		}
	}

	return false;
}

CAccountObject* CAccountObjectList::GetAccountObjectByNetId(const NETID& netId) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pAO = (CAccountObject*)GetNext(pos);
		if (pAO && pAO->m_netId == netId)
			return pAO;
	}
	return nullptr;
}

CAccountObject* CAccountObjectList::GetAccountObjectByUsername(const std::string& username) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pAO = (CAccountObject*)GetNext(pos);
		if (pAO && pAO->m_accountName.GetString() == username)
			return pAO;
	}
	return nullptr;
}

CAccountObject* CAccountObjectList::GetAccountObjectByAccountNumber(int number) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pAO = (CAccountObject*)GetNext(pos);
		if (pAO && pAO->m_accountNumber == number)
			return pAO;
	}
	return nullptr;
}

CAccountObject* CAccountObjectList::GetAccountObjectByServerId(ULONG serverId) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pAO = (CAccountObject*)GetNext(pos);
		if (pAO && pAO->m_serverId == serverId)
			return pAO;
	}
	return nullptr;
}

CAccountObject* CAccountObjectList::GetAccountObjectByPlayerHandle(const PLAYER_HANDLE& handle) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pAO = (CAccountObject*)GetNext(pos);
		if (!pAO || !pAO->m_pPlayerObjectList)
			continue;

		for (POSITION pos2 = pAO->m_pPlayerObjectList->GetHeadPosition(); pos2;) {
			auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetNext(pos2);
			if (pPO && pPO->m_handle == handle)
				return pAO;
		}
	}
	return nullptr;
}

CPlayerObject* CAccountObjectList::GetPlayerObjectByPlayerHandle(const PLAYER_HANDLE& handle) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pAO = (CAccountObject*)GetNext(pos);
		if (!pAO || !pAO->m_pPlayerObjectList)
			continue;

		for (POSITION pos2 = pAO->m_pPlayerObjectList->GetHeadPosition(); pos2;) {
			auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetNext(pos2);
			if (pPO && pPO->m_handle == handle)
				return pPO;
		}
	}
	return nullptr;
}

void CAccountObject::getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s) {
	// create the command for our parent to delete us
	gsMetaDatum& md = (*m_gsMetaData)[i];

	// do our custom one
	if (md.GetDescId() == IDS_ACCOUNTOBJECT_DISCONNECT ||
		md.GetDescId() == IDS_ACCOUNTOBJECT_EXPORT ||
		md.GetDescId() == IDS_ACCOUNTOBJECT_IMPORT_CHAR) {
		// figure out the parent object id since it is the parent that processes this
		std::string parentId;
		const char* lastDelim = strrchr(ourObjectId, '_');
		if (lastDelim)
			parentId.assign(ourObjectId, lastDelim - ourObjectId);

		_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
			"<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\" objectId=\"%s\"",
			loadStr(md.GetDescId()).c_str(),
			md.GetType(),
			md.GetFlags(),
			md.GetDescId(),
			parentId.c_str());
		if (filter == 0 || filter->allAttributes())
			_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
				"%s help=\"%s\" minValue=\"%f\" maxValue=\"%f\" indent=\"%d\" groupId=\"%d\"",
				s,
				loadStr(md.GetHelpId()).c_str(),
				md.GetMinValue(), md.GetMaxValue(),
				md.GetIndent(),
				md.GetGroupTitleId());

		if (md.GetDescId() == IDS_ACCOUNTOBJECT_DISCONNECT)
			_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
				"%s><Enabled>%d</Enabled",
				s,
				m_loggedOn);

		strncat_s(s, TEMP_STR_SIZE, "></GetSetRec>\n", _TRUNCATE);
		xml += s;

	} else if (md.GetDescId() == IDS_ACCOUNTOBJECT_YEAROFLASTLOGON) {
		// convert to string
		_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
			"<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\">%d-%d-%d %d:%02d</GetSetRec>\n",
			loadStr(md.GetDescId()).c_str(),
			gsString,
			md.GetFlags(),
			md.GetDescId(),
			m_yearOfLastLogon,
			m_monthOfLastLogon,
			m_dayOfLastLogon,
			m_hourOfLastLogon,
			m_minuteOfLastLogon);
		xml += s;
	} else if (md.GetDescId() == IDS_ACCOUNTOBJECT_REFRESHPAYMENTYEAR) {
		// convert to string
		_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
			"<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\">%d-%d-%d</GetSetRec>\n",
			loadStr(md.GetDescId()).c_str(),
			gsString,
			md.GetFlags(),
			md.GetDescId(),
			m_refreshPaymentYear,
			m_refreshPaymentMonth,
			m_refreshPaymentDay);
		xml += s;
	} else {
		GetSet::getItemXml(i, ourObjectId, depth, filter, myMd, xml, s);
	}
}

IMPLEMENT_SERIAL_SCHEMA(CAccountObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CAccountObjectList, CObList)

BEGIN_GETSET_IMPL(CAccountObject, IDS_ACCOUNTOBJECT_NAME)
GETSET_MAX(m_accountName, gsCString, GS_FLAGS_DEFAULT | GS_FLAG_INSTANCE_NAME | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, ACCOUNTNAME, STRLEN_MAX)
GETSET_MAX(m_accountPassword, gsCString, GS_FLAGS_DEFAULT | GS_FLAG_PASSWORD | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, ACCOUNTPASSWORD, STRLEN_MAX)
GETSET_MAX(m_realName, gsCString, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, REALNAME, STRLEN_MAX)
GETSET_RANGE(m_accountStatus, gsBOOL, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, ACCOUNTSTATUS, INT_MIN, INT_MAX)
GETSET_MAX(m_suspentionNotes, gsCString, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, SUSPENTIONNOTES, STRLEN_MAX)
GETSET(m_gm, gsBOOL, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, GM)
GETSET(m_canSpawn, gsBOOL, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, CANSPAWN)
GETSET(m_administratorAccess, gsBOOL, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, ACCOUNTOBJECT, ADMINISTRATORACCESS)
GETSET_RANGE(m_serverId, gsUlong, GS_FLAGS_DEFAULT, 0, 0, ACCOUNTOBJECT, SERVERID, 0, ULONG_MAX);
GETSET_OBLIST_PTR(m_pPlayerObjectList, GS_FLAGS_DEFAULT, 0, 0, ACCOUNTOBJECT, CHARACTERSONACCOUNT)
GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, ACCOUNTOBJECT, DISCONNECT)
GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, ACCOUNTOBJECT, EXPORT)
GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, ACCOUNTOBJECT, IMPORT_CHAR)
GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, ADMIN, DELETE)
GETSET(m_loggedOn, gsBOOL, GS_FLAG_EXPOSE, 0, STATUS_GROUP, ACCOUNTOBJECT, LOGGEDON)
GETSET(m_registeredVersion, gsBOOL, GS_FLAG_EXPOSE | GS_FLAG_CAN_ADD, 0, STATUS_GROUP, ACCOUNTOBJECT, REGISTEREDVERSION)
GETSET_RANGE(m_timePlayedMinutes, gsLong, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, STATUS_GROUP, ACCOUNTOBJECT, TIMEPLAYEDMINUTES, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_accountNumber, gsInt, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, STATUS_GROUP, ACCOUNTOBJECT, ACCOUNTNUMBER, INT_MIN, INT_MAX)
GETSET(m_aiServer, gsBOOL, GS_FLAG_ADDONLY | GS_FLAGS_DEFAULT, 0, STATUS_GROUP, ACCOUNTOBJECT, AISERVER)
GETSET_MAX(m_currentIP, gsCString, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, STATUS_GROUP, ACCOUNTOBJECT, CURRENTIP, STRLEN_MAX)
GETSET_CUSTOM(&m_yearOfLastLogon, gsString, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, STATUS_GROUP, ACCOUNTOBJECT, YEAROFLASTLOGON, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP