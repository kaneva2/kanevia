///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include "ClientStrings.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CReferenceImporterExporter.h"

#include "afxwin.h"
#include "afxext.h"
#include "UnicodeMFCHelpers.h"
#include "SerializeHelper.h"

namespace KEP {

CReferenceImpExpObj::CReferenceImpExpObj(CStringA setName, Matrix44f matrix) {
	m_idName = setName;
	m_matrix = matrix;
	m_type = 0;
	//DPD Spotlight addition
	m_lightType = D3DLIGHT_POINT; //default to Omni/point
	m_theta = m_phi = m_falloff = 0.0f;
	m_direction.x = 0;
	m_direction.y = 0;
	m_direction.z = 1;
	m_atten0 = m_atten1 = m_atten2 = 0.0f;
}

CReferenceImpExpObj::CReferenceImpExpObj(CStringA setName, int type, Matrix44f matrix, float redValue, float greenValue, float blueValue, float startAttenuation, float endAttenuation, float intensity, int lightType, Vector3f direction, float theta, float phi, float falloff) {
	m_idName = setName;
	m_type = type;
	m_matrix = matrix;
	m_redValue = redValue;
	m_greenValue = greenValue;
	m_blueValue = blueValue;
	m_intensity = intensity;
	m_startAttenuation = startAttenuation;
	m_endAttenuation = endAttenuation;
	//We convert from Max to D3D types
	//D3D {Point = 1, Spot = 2, Dir = 3}
	//Max {Omni = 0, Spot = 1, Dir = 2)
	m_lightType = lightType + 1;

	m_direction = direction;
	m_theta = theta;
	m_phi = phi;
	m_falloff = falloff;

	//directX attenuation values
	//this formula is for nearly smooth fade
	m_atten0 = 1.0f;
	m_atten1 = 1.0f / endAttenuation;
	m_atten2 = 1.0f / endAttenuation / endAttenuation;
}

void CReferenceImpExpList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CReferenceImpExpObj* spnPtr = (CReferenceImpExpObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CReferenceImpExpObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(3);
		ASSERT(ar.GetObjectSchema() == 3);

		ar << m_idName;

		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar << m_matrix[loop][loop2];

		ar << m_type
		   << m_redValue
		   << m_greenValue
		   << m_blueValue
		   << m_intensity
		   << m_startAttenuation
		   << m_endAttenuation;

		ar << m_lightType
		   << m_direction.x
		   << m_direction.y
		   << m_direction.z
		   << m_theta
		   << m_phi
		   << m_falloff
		   << m_atten0
		   << m_atten1
		   << m_atten2;
	} else {
		int version = ar.GetObjectSchema();

		ar >> m_idName;

		for (int loop = 0; loop < 4; loop++)
			for (int loop2 = 0; loop2 < 4; loop2++)
				ar >> m_matrix[loop][loop2];

		m_endAttenuation = m_startAttenuation = m_intensity = m_redValue = m_greenValue = m_blueValue = 1.0;
		m_lightType = m_type = 0;
		m_direction.x = m_direction.y = m_direction.z = m_theta = m_phi = m_falloff = m_atten0 = m_atten1 = m_atten2 = 0.0;

		if (version > 1) {
			ar >> m_type >> m_redValue >> m_greenValue >> m_blueValue >> m_intensity >> m_startAttenuation >> m_endAttenuation;
		}
		if (version > 2) {
			ar >> m_lightType >> m_direction.x >> m_direction.y >> m_direction.z >> m_theta >> m_phi >> m_falloff >> m_atten0 >> m_atten1 >> m_atten2;
		}
	}
}

BOOL CReferenceImpExpList::AddNewReference(CStringA name, Matrix44f matrix) {
	CReferenceImpExpObj* newPtr = new CReferenceImpExpObj(name, matrix);

	AddTail(newPtr);

	return TRUE;
}

BOOL CReferenceImpExpList::AddNewReference(CStringA name, int type, Matrix44f matrix, float redValue, float greenValue, float blueValue, float startAttenuation, float endAttenuation, float intensity, int lightType, Vector3f direction, float theta, float phi, float falloff) {
	CReferenceImpExpObj* newPtr = new CReferenceImpExpObj(name, type, matrix, redValue, greenValue, blueValue, startAttenuation, endAttenuation, intensity, lightType, direction, theta, phi, falloff);

	AddTail(newPtr);

	return TRUE;
}

BOOL CReferenceImpExpList::SaveToFile(CStringA fileNameUtf8) {
	CStringW fileNameW = Utf8ToUtf16(fileNameUtf8).c_str();
	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(fileNameW, CFile::modeCreate | CFile::modeWrite, &exc);

	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		CStringW s;
		s.LoadString(IDS_ERROR_CANNOTCREATE);
		AfxMessageBox(s + fileNameW);
		return FALSE;
	}
	CArchive the_out_Archive(&fl, CArchive::store);
	BEGIN_SERIALIZE_TRY

	the_out_Archive << this;
	the_out_Archive.Close();
	fl.Close();
	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

BOOL CReferenceImpExpList::InitializeFromFile(CStringA fileNameUtf8) {
	CStringW fileNameW = Utf8ToUtf16(fileNameUtf8).c_str();
	CFileException exc;
	CFile fl;
	BOOL res = fl.Open(fileNameW, CFile::modeRead, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc)
		CStringW s;
		s.LoadString(IDS_ERROR_CANNOTOPEN);
		AfxMessageBox(s + fileNameW);
		return FALSE;
	}
	CArchive the_in_Archive(&fl, CArchive::load);

	CReferenceImpExpList* newImportedList = NULL;

	BEGIN_SERIALIZE_TRY

	the_in_Archive >> newImportedList;
	the_in_Archive.Close();
	fl.Close();

	if (!newImportedList) {
		CStringW s;
		s.LoadString(IDS_ERROR_INVALIDFORMAT);
		AfxMessageBox(s + fileNameW);
		return FALSE;
	}

	for (POSITION posLoc = newImportedList->GetHeadPosition(); posLoc != NULL;) {
		CReferenceImpExpObj* oPtr = (CReferenceImpExpObj*)newImportedList->GetNext(posLoc);
		this->AddTail(oPtr);
	}
	newImportedList->RemoveAll();
	delete newImportedList;
	newImportedList = 0;

	END_SERIALIZE_TRY_NO_LOGGER

	return serializeOk ? TRUE : FALSE;
}

IMPLEMENT_SERIAL_SCHEMA(CReferenceImpExpObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CReferenceImpExpList, CObList)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CReferenceImpExpObj, IDS_REFERENCEIMPEXPOBJ_NAME)
GETSET_MAX(m_idName, gsCString, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, IDNAME, STRLEN_MAX)
GETSET_RANGE(m_type, gsInt, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, TYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_redValue, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, REDVALUE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_greenValue, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, GREENVALUE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_blueValue, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, BLUEVALUE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_intensity, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, INTENSITY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_startAttenuation, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, STARTATTENUATION, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_endAttenuation, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, ENDATTENUATION, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_lightType, gsInt, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, LIGHTTYPE, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_direction, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, DIRECTION)
GETSET_RANGE(m_theta, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, THETA, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_phi, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, PHI, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_falloff, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, FALLOFF, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_atten0, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, ATTEN0, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_atten1, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, ATTEN1, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_atten2, gsFloat, GS_FLAGS_DEFAULT, 0, 0, REFERENCEIMPEXPOBJ, ATTEN2, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP