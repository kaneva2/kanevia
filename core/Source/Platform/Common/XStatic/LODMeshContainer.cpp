#include "stdafx.h"
#include "LODMeshContainer.h"

LODMeshContainer LODMeshContainer::MeshCombinations[MAX_MESH_COMBINATIONS];
int LODMeshContainer::mNumMeshCombinations = 0;

int LODMeshContainer::AddCombination(__int64* MeshHashes, int numHashes) {
	if (numHashes <= 0)
		return -1;

	MeshCombinations[mNumMeshCombinations].mNumHashes = numHashes;

	for (int i = 0; i < numHashes; i++)
		MeshCombinations[mNumMeshCombinations].MeshHashes[i] = MeshHashes[i];

	return mNumMeshCombinations++;
}

int LODMeshContainer::GetCombination(__int64** MeshHashes, int CombinationIndex) {
	if (CombinationIndex < 0 || CombinationIndex >= mNumMeshCombinations)
		return 0;

	LODMeshContainer* p = &MeshCombinations[CombinationIndex];

	__int64* pNewHashArray = new __int64[p->mNumHashes];

	memcpy(pNewHashArray, p->MeshHashes, p->mNumHashes);

	(*MeshHashes) = pNewHashArray;

	return p->mNumHashes;
}

void LODMeshContainer::SaveMeshCombinationsToFile(const char* path) {
	if (mNumMeshCombinations <= 0)
		return;

	FILE* fp;

	fp = fopen(path, "wb");

	if (fp) {
		//	Write out the number of combinations
		fwrite(&mNumMeshCombinations, sizeof(int), 1, fp);

		//	Write out the combinations
		fwrite(MeshCombinations, sizeof(LODMeshContainer) * mNumMeshCombinations, 1, fp);

		fclose(fp);
	}
}

void LODMeshContainer::LoadMeshCombinationsFromFile(const char* path) {
	FILE* fp;

	fp = fopen(path, "rb");

	if (fp) {
		//	Read the number of combinations
		fread(&mNumMeshCombinations, sizeof(int), 1, fp);

		//	Read the combinations
		fread(MeshCombinations, sizeof(LODMeshContainer) * mNumMeshCombinations, 1, fp);

		fclose(fp);
	}
}