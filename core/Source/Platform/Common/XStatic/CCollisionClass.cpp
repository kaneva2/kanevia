///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CCollisionClass.h"
#include "CMaterialClass.h"
#include "CWldObject.h"
#include "CNodeObject.h"
#include "CReferenceLibrary.h"
#include "ReD3DX9StaticMesh.h"
#include <unordered_map>
#include <boost/functional/hash/hash.hpp>
#include <Core/Math/Intersect.h>

#include "LogHelper.h"
static LogInstance("Instance");

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace KEP {

// Serialization Helpers
template <size_t N, typename T>
CArchive& operator<<(CArchive& ar, const Vector<N, T>& v) {
	for (size_t i = 0; i < N; i++) {
		ar << v[i];
	}
	return ar;
}

template <size_t N, typename T>
CArchive& operator>>(CArchive& ar, Vector<N, T>& v) {
	for (size_t i = 0; i < N; i++) {
		ar >> v[i];
	}
	return ar;
}

// CCollisionObj
CCollisionObj::CCollisionObj(size_t maxLevels /*= COL_DEF_LEVELSDEEP*/, size_t maxFacesPerBox /*= COL_DEF_FACESPERBOX*/) :
		m_numberOfLevels(maxLevels), m_maxFacesPerBox(maxFacesPerBox), m_baseBox(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f), m_divResX(NULL), m_divResY(NULL), m_divResZ(NULL), m_boxSizeX(NULL), m_boxSizeY(NULL), m_boxSizeZ(NULL), m_wldObjList(NULL), m_boxCollection(NULL), m_useObjectRef(false), m_nextFaceIndexCacheKey(0), m_compressed(true) {
}

CCollisionObj::~CCollisionObj() {
	SafeDelete();
}

void CCollisionObj::BuildObjectReferenceArray(CWldObjectList* wList) {
	m_wldObjectReferenceArray.clear();

	if (wList->GetCount()) {
		m_wldObjectReferenceArray.resize(wList->GetCount());

		int trace = 0;
		for (POSITION posLoc = wList->GetHeadPosition(); posLoc != NULL; ++trace) {
			m_wldObjectReferenceArray[trace] = (CWldObject*)wList->GetNext(posLoc);
		}
	}
}

BOOL CCollisionObj::CollisionSegmentSIValidPath(
	const Vector3f& collisionPathStart,
	const Vector3f& collisionPathEnd,
	int tolerance,
	float floorTolerance,
	float wallMinimumHeight,
	float currentYpos,
	Vector3f origin,
	BOOL* wallInWayRet,
	BOOL* floorExistRet,
	Vector3f* wallNormal) {
	BOOL wallInWay = FALSE;
	BOOL floorExist = FALSE;

	if (!m_boxCollection)
		return FALSE;

	BoundBox cZone = GetSegmentBoundingBox(collisionPathStart, collisionPathEnd);

	float distanceTraveled = Distance(collisionPathStart, collisionPathEnd);
	long possibilities = (long)ceil(distanceTraveled / tolerance);
	REFBOX* goodSubBox = NULL;

	for (int prog = 0; prog < possibilities; prog++) {
		//mainloop
		Vector3f currentPosition = MatrixARB::InterpolatePoints(collisionPathStart, collisionPathEnd, possibilities, prog);
		if (BaseBoxContains(currentPosition)) {
			//if in bounds
			REFBOX* curSubBox = FindLeafBoxAtPosition(currentPosition);
			if (curSubBox == goodSubBox)
				continue;

			for (const auto& faceIndex : GetFaceIndexArray(curSubBox->m_faceIndexCacheKey)) {
				if (faceIndex < 0 || faceIndex >= m_faceDataBank.size()) {
					jsAssert(false);
					continue;
				}

				const CFaceStruct& faceData = m_faceDataBank[faceIndex];
				if (BoundingBoxToBoxCheck(cZone, faceData.m_box)) {
					//bounding collision check
					Vector3f intersectLocalTest;
					if (MatrixARB::SegPolyCheck(faceData.m_vertices, collisionPathStart, collisionPathEnd, &intersectLocalTest) == TRUE) {
						//Has collided
						if (faceData.m_normal.y > floorTolerance)
							floorExist = TRUE;
						else if (wallInWay == FALSE) {
							wallInWay = TRUE;
							*wallNormal = faceData.m_normal;
						}
					}
				}
			}

			goodSubBox = curSubBox;
		}
	}

	*wallInWayRet = wallInWay;
	*floorExistRet = floorExist;

	if (wallInWay == TRUE)
		return FALSE;
	if (floorExist == FALSE)
		return FALSE;

	return TRUE;
}

BOOL CCollisionObj::CollisionSegmentIntersectAdvanced(
	const Vector3f& collisionPathStart,
	const Vector3f& collisionPathEnd,
	float tolerance,
	float normalYHeightFilter,
	float normalYHeightFilterDW,
	Vector3f* intersection,
	Vector3f* normal,
	int* indexOfObjectHit,
	float* highestYcontactPoint,
	Vector3f* adjustVector) const {
	if (!m_boxCollection)
		return FALSE;

	*highestYcontactPoint = -100000.0f;

	BoundBox cZone = GetSegmentBoundingBox(collisionPathStart, collisionPathEnd);
	bool everCollided = false;
	float deepestIntersection = 100000.0f;

	float distanceTraveled = Distance(collisionPathStart, collisionPathEnd);
	long possibilities = (long)ceil(distanceTraveled / tolerance);
	REFBOX* goodSubBox = NULL;

	for (int prog = 0; prog < possibilities; prog++) {
		Vector3f currentPosition = MatrixARB::InterpolatePoints(collisionPathStart, collisionPathEnd, possibilities, prog);
		if (BaseBoxContains(currentPosition)) {
			REFBOX* curSubBox = FindLeafBoxAtPosition(currentPosition);
			if (curSubBox == goodSubBox)
				continue;

			for (const auto& faceIndex : GetFaceIndexArray(curSubBox->m_faceIndexCacheKey)) {
				if (faceIndex < 0 || faceIndex >= m_faceDataBank.size())
					continue;

				const CFaceStruct& faceData = m_faceDataBank[faceIndex];
				if (BoundingBoxToBoxCheck(cZone, faceData.m_box)) {
					Vector3f intersectLocalTest;
					if (faceData.m_normal.y >= normalYHeightFilter - 1e-6 &&
						faceData.m_normal.y <= normalYHeightFilterDW + 1e-6) {
						if (MatrixARB::SegPolyCheck(faceData.m_vertices, collisionPathStart, collisionPathEnd, &intersectLocalTest) == TRUE) {
							//Has collided
							float distance = 0.0f;
							if (MatrixARB::GetSubDistance(faceData.m_vertices, faceData.m_normal, collisionPathEnd, &distance)) {
								if (distance < deepestIntersection) {
									deepestIntersection = distance;
									*adjustVector = faceData.m_normal * distance;
									*intersection = intersectLocalTest;
									*normal = faceData.m_normal;
									if (*highestYcontactPoint < intersectLocalTest.y)
										*highestYcontactPoint = intersectLocalTest.y;

									*indexOfObjectHit = faceData.m_objectIndexRef;
									everCollided = true;
								} else {
									if (*highestYcontactPoint < intersectLocalTest.y)
										*highestYcontactPoint = intersectLocalTest.y;
								}
							}
						}
					}
				}
			}

			goodSubBox = curSubBox;
		}
	}

	return everCollided ? TRUE : FALSE;
}

BOOL CCollisionObj::CollisionSegmentIntersect(
	const Vector3f& collisionPathStart,
	const Vector3f& collisionPathEnd,
	float tolerance,
	bool returnAfterFirstContact,
	bool filterCorona,
	Vector3f* intersection,
	Vector3f* normal,
	int* indexOfObjectHit) {
	if (!m_boxCollection)
		return FALSE;

	bool everCollided = false;
	BoundBox cZone = GetSegmentBoundingBox(collisionPathStart, collisionPathEnd);
	float closestIntersection = 100000.0f;

	float distanceTraveled = Distance(collisionPathStart, collisionPathEnd);
	long possibilities = (long)ceil(distanceTraveled / tolerance);
	REFBOX* goodSubBox = NULL;

	for (int prog = 0; prog < possibilities; prog++) {
		//mainloop
		Vector3f currentPosition = MatrixARB::InterpolatePoints(collisionPathStart, collisionPathEnd, possibilities, prog);
		if (BaseBoxContains(currentPosition)) {
			//if in bounds
			REFBOX* curSubBox = FindLeafBoxAtPosition(currentPosition);
			if (curSubBox == goodSubBox)
				continue;

			for (const auto& faceIndex : GetFaceIndexArray(curSubBox->m_faceIndexCacheKey)) {
				if (faceIndex < 0 || faceIndex >= m_faceDataBank.size()) {
					jsAssert(false);
					continue;
				}

				const CFaceStruct& faceData = m_faceDataBank[faceIndex];
				if (BoundingBoxToBoxCheck(cZone, faceData.m_box)) {
					if (filterCorona) {
						CWldObject* wldObj = GetWorldObjectReference(faceData.m_objectIndexRef);
						CEXMeshObj* mesh = wldObj ? wldObj->m_meshObject : NULL;
						if (mesh && (mesh->m_charona || mesh->m_materialObject && mesh->m_materialObject->m_blendMode > -1)) {
							continue;
						}
					}

					Vector3f intersectLocalTest;
					if (MatrixARB::SegPolyCheck(faceData.m_vertices, collisionPathStart, collisionPathEnd, &intersectLocalTest) == TRUE) {
						//Has collided
						float curdistance = Distance(collisionPathStart, intersectLocalTest);
						if (!everCollided || curdistance < closestIntersection) {
							closestIntersection = curdistance;
							if (intersection) {
								*intersection = intersectLocalTest;
							}
							if (normal) {
								*normal = faceData.m_normal;
							}
							if (indexOfObjectHit) {
								*indexOfObjectHit = faceData.m_objectIndexRef;
							}
						}

						everCollided = true;
					}

					if (everCollided && returnAfterFirstContact)
						return TRUE;
				}
			}

			goodSubBox = curSubBox;
		}
	}

	return everCollided ? TRUE : FALSE;
}

BOOL CCollisionObj::CollisionSegmentIntersectBasic(
	const Vector3f& collisionPathStart,
	const Vector3f& collisionPathEnd,
	float tolerance,
	bool filterCorona) {
	Vector3f intersect, normal;
	int indexOfObjectHit;
	return CollisionSegmentIntersect(collisionPathStart, collisionPathEnd, tolerance, true, filterCorona, &intersect, &normal, &indexOfObjectHit);
}

BOOL CCollisionObj::CollisionPolyRetrieval(
	Vector3f polyLoc[3],
	Vector3f* intersection,
	Vector3f* latestNormal,
	int& numberPolys,
	CPolyRetStruct polyRet[20],
	Vector3f storedNormals[20]) {
	if (!m_boxCollection)
		return FALSE;

	bool everCollided = false;

	REFBOX* goodSubBox = NULL;

	BoundBox polyPass = GetPolyBoundingBox(polyLoc);
	for (int loop = 0; loop < 3; loop++) {
		//mainloop
		const Vector3f& currentPosition = polyLoc[loop];
		if (BaseBoxContains(currentPosition)) {
			//if in bounds
			REFBOX* curSubBox = FindLeafBoxAtPosition(currentPosition);

			jsAssert(false);
			// ??? Conditional check below is probably wrong based on other similar functions: all && should be || instead, I think ???
			//if (Xpos != oldXpos && Ypos != oldYpos && Zpos != oldZpos && CurSubLevel != oldCurSubLevel
			//    && minX != oldMinX && minY != oldMinY && minZ != oldMinZ) {
			//	//corner check

			if (curSubBox == goodSubBox)
				continue;

			for (auto faceIndex : GetFaceIndexArray(curSubBox->m_faceIndexCacheKey)) {
				if (numberPolys < 10) {
					if (faceIndex < 0 || faceIndex >= m_faceDataBank.size()) {
						jsAssert(false);
						continue;
					}

					const CFaceStruct& faceData = m_faceDataBank[faceIndex];
					if (BoundingBoxToBoxCheck(polyPass, faceData.m_box)) {
						//bounding collision check
						if (TestTrianglesIntersection(faceData.m_vertices, polyLoc) == TRUE) {
							//Has collided
							*latestNormal = faceData.m_normal;

							//output polys for shadowing
							polyRet[numberPolys].vertices[0] = faceData.m_vertices[0];
							polyRet[numberPolys].vertices[1] = faceData.m_vertices[1];
							polyRet[numberPolys].vertices[2] = faceData.m_vertices[2];
							storedNormals[numberPolys] = faceData.m_normal;
							numberPolys++;

							everCollided = true;
						} //end Has collided
					} // BoundingBoxToBoxCheck
				} // end howmanynormals
			} // for loop

			goodSubBox = curSubBox;
		} //end if in bounds
		//tally final info then output final resolutions
	} //end main loop

	return everCollided ? TRUE : FALSE;
}

void CCollisionObj::SafeDelete() {
	m_wldObjectReferenceArray.clear();
	m_faceDataBank.clear();
	m_useObjectRef = false;

	if (!m_divResX)
		return;

	SubLevelDelete(m_boxCollection);
	m_boxCollection = 0;

	if (m_divResX) {
		delete[] m_divResX;
		m_divResX = 0;
	}

	if (m_divResY) {
		delete[] m_divResY;
		m_divResY = 0;
	}

	if (m_divResZ) {
		delete[] m_divResZ;
		m_divResZ = 0;
	}

	if (m_boxSizeX) {
		delete[] m_boxSizeX;
		m_boxSizeX = 0;
	}

	if (m_boxSizeY) {
		delete[] m_boxSizeY;
		m_boxSizeY = 0;
	}

	if (m_boxSizeZ) {
		delete[] m_boxSizeZ;
		m_boxSizeZ = 0;
	}

	m_faceIndexArrayCache.clear();
	m_nextFaceIndexCacheKey = 0;
	m_compressed = true;
}

void CCollisionObj::SubLevelDelete(REFBOX*** CurrentSubBox) {
	for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
		for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
			for (int zPos = 0; zPos < m_divResZ[0]; zPos++) {
				//loop through all boxes inside list
				if (CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection != nullptr) {
					SubLevelDelete(CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection);
				}
			} //end loop through all boxes inside list
		}
	}

	for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
		for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
			delete[] CurrentSubBox[xPos][yPos];
			CurrentSubBox[xPos][yPos] = 0;
		}

		delete[] CurrentSubBox[xPos];
		CurrentSubBox[xPos] = 0;
	}

	delete[] CurrentSubBox;
}

BOOL CCollisionObj::GenerateSubTable(REFBOX* curSubBox, const BoundBox& curBounds, const std::vector<HWND>& hwndProgressBars) {
	int SubLevel = curSubBox->m_subTableLevel + 1;
	m_boxSizeX[SubLevel] = (curBounds.maxX - curBounds.minX) / m_divResX[SubLevel];
	m_boxSizeY[SubLevel] = (curBounds.maxY - curBounds.minY) / m_divResY[SubLevel];
	m_boxSizeZ[SubLevel] = (curBounds.maxZ - curBounds.minZ) / m_divResZ[SubLevel];

	curSubBox->m_subBoxCollection = new REFBOX**[m_divResX[SubLevel]];
	for (int m = 0; m < m_divResX[SubLevel]; m++) {
		curSubBox->m_subBoxCollection[m] = new REFBOX*[m_divResY[SubLevel]];
		for (int n = 0; n < m_divResY[SubLevel]; n++) {
			curSubBox->m_subBoxCollection[m][n] = new REFBOX[m_divResZ[SubLevel]];
		}
	}

	if (hwndProgressBars[SubLevel]) {
		int posCntrolTotal = m_divResX[SubLevel] * m_divResY[SubLevel] * m_divResZ[SubLevel];
		::SendMessage(hwndProgressBars[SubLevel], PBM_SETRANGE, 0, MAKELPARAM(0, posCntrolTotal));
	}

	int progPos = 0;
	for (int xPtr = 0; xPtr < m_divResX[SubLevel]; xPtr++) {
		for (int yPtr = 0; yPtr < m_divResY[SubLevel]; yPtr++) {
			for (int zPtr = 0; zPtr < m_divResZ[SubLevel]; zPtr++, progPos++) {
				if (hwndProgressBars[SubLevel]) {
					::SendMessage(hwndProgressBars[SubLevel], PBM_SETPOS, progPos, 0);
				}

				REFBOX& newBox = curSubBox->m_subBoxCollection[xPtr][yPtr][zPtr];

				//allocate link list
				newBox.m_objectReferenceList.clear();
				newBox.m_faceIndexCacheKey = REFBOX::INVALID_FACE_INDEX_CACHE_KEY;
				newBox.m_subTableLevel = SubLevel;
				newBox.m_subBoxCollection = NULL;

				BoundBox newBounds;
				newBounds.maxX = (xPtr + 1) * m_boxSizeX[SubLevel] + curBounds.minX;
				newBounds.minX = (xPtr)*m_boxSizeX[SubLevel] + curBounds.minX;
				newBounds.maxY = (yPtr + 1) * m_boxSizeY[SubLevel] + curBounds.minY;
				newBounds.minY = (yPtr)*m_boxSizeY[SubLevel] + curBounds.minY;
				newBounds.maxZ = (zPtr + 1) * m_boxSizeZ[SubLevel] + curBounds.minZ;
				newBounds.minZ = (zPtr)*m_boxSizeZ[SubLevel] + curBounds.minZ;

				for (auto objIndex : curSubBox->m_objectReferenceList) {
					CWldObject* wldObj = GetWorldObjectReference(objIndex);
					jsAssert(wldObj != NULL);
					if (wldObj && (BoundingBoxToBoxCheck(wldObj->m_popoutBox, newBounds) == TRUE || wldObj->m_environmentAttached == TRUE)) {
						newBox.m_objectReferenceList.push_back(objIndex);
					}
				}

				float boundaryTolerance = SubLevel == 0 ? 0 : 2;
				BoundBox colBounds;
				colBounds.minX = newBounds.minX - boundaryTolerance;
				colBounds.minY = newBounds.minY - boundaryTolerance;
				colBounds.minZ = newBounds.minZ - boundaryTolerance;
				colBounds.maxX = newBounds.maxX + boundaryTolerance;
				colBounds.maxY = newBounds.maxY + boundaryTolerance;
				colBounds.maxZ = newBounds.maxZ + boundaryTolerance;

				const std::vector<int>& curFaceIndexArray = GetFaceIndexArray(curSubBox->m_faceIndexCacheKey);

				std::vector<int> newFaceIndexArray;
				for (auto faceIndex : curFaceIndexArray) {
					if (faceIndex < 0 || faceIndex >= m_faceDataBank.size()) {
						jsAssert(false);
						continue;
					}
					if (BoundingBoxToBoxCheck(m_faceDataBank[faceIndex].m_box, colBounds) == TRUE) {
						newFaceIndexArray.push_back(faceIndex);
					}
				}

				size_t subBoxFaceCount = 0;
				if (newFaceIndexArray.size() > COMPRESSION_FACE_COUNT_THRESHOLD && newFaceIndexArray.size() * 1.0 / curFaceIndexArray.size() > COMPRESSION_FACE_SUBSET_THRESHOLD) {
					// Reuse parent box's face array
					newBox.m_faceIndexCacheKey = curSubBox->m_faceIndexCacheKey;
					subBoxFaceCount = curFaceIndexArray.size();
				} else {
					// Insert new array into cache
					newBox.m_faceIndexCacheKey = m_nextFaceIndexCacheKey;
					m_faceIndexArrayCache[m_nextFaceIndexCacheKey] = newFaceIndexArray;
					m_nextFaceIndexCacheKey++;
					subBoxFaceCount = newFaceIndexArray.size();
				}

				if (subBoxFaceCount > m_maxFacesPerBox &&
					newBox.m_subTableLevel < (int)m_numberOfLevels - 1) {
					//If the number of faces assigned to the box is greater than m_maxFacesPerBox
					//then create a subset of boxes.
					if (GenerateSubTable(&curSubBox->m_subBoxCollection[xPtr][yPtr][zPtr], newBounds, hwndProgressBars) == FALSE) {
						return FALSE;
					}
				}
			}
		}
	}

	return TRUE;
}

REFBOX* CCollisionObj::ConvertPositionToSubBox(const Vector3f& pos, REFBOX* curBox, const Vector3f& curBoxMin, Vector3f& subBoxMin) const {
	auto boxCol = m_boxCollection;
	size_t curLevel = 0;
	if (curBox) {
		boxCol = curBox->m_subBoxCollection;
		curLevel = curBox->m_subTableLevel + 1;
	}

	jsVerifyReturn(boxCol != NULL, NULL);
	jsVerifyReturn(m_boxSizeX != NULL && m_boxSizeY != NULL && m_boxSizeZ != NULL && curLevel < m_numberOfLevels, NULL);

	float boxSizeX = m_boxSizeX[curLevel];
	float boxSizeY = m_boxSizeY[curLevel];
	float boxSizeZ = m_boxSizeZ[curLevel];

	int resX = 0, resY = 0, resZ = 0;
	GetResolution(curLevel, resX, resY, resZ);

	int Xpos = boxSizeX == 0 ? 0 : std::max<int>(0, std::min<int>(resX - 1, (int)((pos.x - curBoxMin.x) / boxSizeX)));
	int Ypos = boxSizeY == 0 ? 0 : std::max<int>(0, std::min<int>(resY - 1, (int)((pos.y - curBoxMin.y) / boxSizeY)));
	int Zpos = boxSizeZ == 0 ? 0 : std::max<int>(0, std::min<int>(resZ - 1, (int)((pos.z - curBoxMin.z) / boxSizeZ)));

	subBoxMin.x = curBoxMin.x + Xpos * boxSizeX;
	subBoxMin.y = curBoxMin.y + Ypos * boxSizeY;
	subBoxMin.z = curBoxMin.z + Zpos * boxSizeZ;
	return &boxCol[Xpos][Ypos][Zpos];
}

REFBOX* CCollisionObj::FindLeafBoxAtPosition(const Vector3f& pos) const {
	Vector3f curBoxMin(m_baseBox.minX, m_baseBox.minY, m_baseBox.minZ), subBoxMin;
	REFBOX* curSubBox = ConvertPositionToSubBox(pos, NULL, curBoxMin, subBoxMin);
	jsAssert(curSubBox != NULL && curSubBox->m_subTableLevel == 0);

	while (curSubBox->m_subBoxCollection) {
		curBoxMin = subBoxMin;
		auto newSubBox = ConvertPositionToSubBox(pos, curSubBox, curBoxMin, subBoxMin);
		jsAssert(newSubBox != NULL && newSubBox->m_subTableLevel == curSubBox->m_subTableLevel + 1);
		curSubBox = newSubBox;
	}

	return curSubBox;
}

BOOL CCollisionObj::CalculateWorld(CWldObjectList* worldList, size_t maxLevels, size_t maxFacesPerBox, int resX, int resY, int resZ, bool genObjList, CReferenceObjList* reference, HWND hwndProgTotal, HWND hwndProgTask) {
	jsAssert(maxLevels > 0);

	if (m_boxCollection) {
		SafeDelete();
	}

	m_baseBox.invalidate();
	m_numberOfLevels = maxLevels;
	m_maxFacesPerBox = maxFacesPerBox;
	m_divResX = new int[m_numberOfLevels];
	m_divResY = new int[m_numberOfLevels];
	m_divResZ = new int[m_numberOfLevels];
	m_boxSizeX = new float[m_numberOfLevels];
	m_boxSizeY = new float[m_numberOfLevels];
	m_boxSizeZ = new float[m_numberOfLevels];
	for (size_t cntr = 0; cntr < m_numberOfLevels; cntr++) {
		m_divResX[cntr] = resX;
		m_divResY[cntr] = resY;
		m_divResZ[cntr] = resZ;
	}
	m_useObjectRef = genObjList;

	Vector3f* triList = new Vector3f[0xffff];

	int globalObjectIndex = 0;

	for (POSITION posLoc = worldList->GetHeadPosition(); posLoc != NULL;) {
		// For each world object / dynamic object mesh
		CWldObject* wldObj = (CWldObject*)worldList->GetNext(posLoc);
		CEXMeshObj* meshReference = NULL;
		ReMesh* reMesh = NULL;

		Matrix44f worldMat;

		if (wldObj->m_meshObject) {
			// Unique world mesh or dynamic object mesh
			meshReference = wldObj->m_meshObject;
			reMesh = wldObj->GetReMesh();
			worldMat = wldObj->m_meshObject->GetWorldTransform();
		} else if (wldObj->m_meshObject == NULL && wldObj->m_node != NULL) {
			// Instanced mesh from reference library
			CReferenceObj* libraryref = reference->GetByIndex(wldObj->m_node->m_libraryReference);
			if (libraryref) {
				if (libraryref->m_collisionModel) {
					meshReference = libraryref->m_collisionModel;
					worldMat = wldObj->m_node->GetWorldTransform();
				} else {
					globalObjectIndex++;
					continue;
				}
			} //end valid referenece
			else {
				//AfxMessageBox(IDS_ERROR_REFERENCEINVALID);
				continue;
			}
		} else {
			globalObjectIndex++;
			continue;
		}

		if (wldObj->m_environmentAttached == FALSE && wldObj->m_collisionInfoFilter == FALSE) {
			// calculate collision for this mesh
			// convert the tristrip to a trilist using a temp buffer
			if (reMesh) {
				// access the ReMesh platform-independent data
				DWORD numVerts = 0;
				ReMeshData* pData;
				reMesh->Access(&pData);

				// convert from tristrip to trilist
				assert(pData->PrimType == ReMeshPrimType::TRISTRIP || pData->PrimType == ReMeshPrimType::TRILIST);
				if (pData->PrimType == ReMeshPrimType::TRISTRIP) {
					// prime the tristrip
					Vector3f lastV1 = pData->pP[pData->pI[0]];
					Vector3f lastV2 = pData->pP[pData->pI[1]];

					//process the tristrip
					for (DWORD i = 0; i < pData->NumI - 2; i++) {
						if (numVerts >= 0xffff - 3) {
							break;
						}

						Vector3f v1, v2, v3;

						// keep clockwise ordering of verts in tri (hardware does this also)
						if ((i & 1) == 0) {
							v1 = lastV1;
							v2 = lastV2;
						} else {
							v1 = lastV2;
							v2 = lastV1;
						}

						triList[numVerts++] = v1;
						triList[numVerts++] = v2;

						v3 = pData->pP[pData->pI[i + 2]];

						triList[numVerts++] = v3;

						// tristrip means reusing the last two verts for next tri
						lastV1 = lastV2;
						lastV2 = v3;
					}
				}
				// just move trilist data over from ReMesh
				else {
					//process the tristrip
					for (DWORD i = 0; i < pData->NumI; i++) {
						triList[numVerts++] = pData->pP[pData->pI[i]];
					}
				}

				for (int faceLoop = 0; faceLoop < numVerts / 3; faceLoop++) {
					//face loop
					m_faceDataBank.push_back(CFaceStruct());
					CFaceStruct& newFaceData = m_faceDataBank.back();

					for (int vi = 0; vi < 3; ++vi) {
						// Transform current vertex into world space
						newFaceData.m_vertices[vi] = TransformPoint(worldMat, triList[faceLoop * 3 + vi]);
					}

					newFaceData.m_objectIndexRef = genObjList ? globalObjectIndex : 0;
					newFaceData.m_box = GetPolyBoundingBox(newFaceData.m_vertices);

					m_baseBox.merge(newFaceData.m_box);

					//create perp normal for collision not rendering ;)
					Vector3f edgeAB = newFaceData.m_vertices[1] - newFaceData.m_vertices[0];
					Vector3f edgeAC = newFaceData.m_vertices[2] - newFaceData.m_vertices[0];
					newFaceData.m_normal = edgeAB.Cross(edgeAC).GetNormalized();
				} //end face loop
			} //has no animations
			// NO ReMesh...use default code
			else {
				for (int faceLoop = 0; faceLoop < meshReference->m_abVertexArray.m_indecieCount / 3; faceLoop++) {
					//face loop
					m_faceDataBank.push_back(CFaceStruct());
					CFaceStruct& newFaceData = m_faceDataBank.back();

					for (int vi = 0; vi < 3; ++vi) {
						const ABVERTEX* vertex = meshReference->m_abVertexArray.GetVertexPtr(faceLoop * 3 + vi);
						jsAssert(vertex != NULL);
						newFaceData.m_vertices[vi] = TransformPoint(worldMat, vertex ? Vector3f(vertex->x, vertex->y, vertex->z) : Vector3f());
					}

					newFaceData.m_objectIndexRef = genObjList ? globalObjectIndex : 0;
					newFaceData.m_box = GetPolyBoundingBox(newFaceData.m_vertices);

					m_baseBox.merge(newFaceData.m_box);

					//create perp normal for collision not rendering ;)
					Vector3f edgeAB = newFaceData.m_vertices[1] - newFaceData.m_vertices[0];
					Vector3f edgeAC = newFaceData.m_vertices[2] - newFaceData.m_vertices[0];
					newFaceData.m_normal = edgeAB.Cross(edgeAC).GetNormalized();
				} //end face loop
			} //has no animations

			globalObjectIndex++;
		} //end world object loop
	}

	delete[] triList;

	// Validate base box in case it's empty
	m_baseBox.validate();

	// Progress UI handle by levels
	std::vector<HWND> hwndProgressBars;
	hwndProgressBars.resize(std::max<size_t>(m_numberOfLevels, 2));
	if (hwndProgTotal) {
		hwndProgressBars[0] = hwndProgTotal;
		::SendMessage(hwndProgTotal, PBM_SETBARCOLOR, 0, RGB(0, 100, 0));
	}
	if (hwndProgTask) {
		hwndProgressBars[1] = hwndProgTask;
		::SendMessage(hwndProgTask, PBM_SETBARCOLOR, 0, RGB(255, 0, 255));
	}

	// Construct a temporary, world-level REFBOX
	REFBOX curSubBox;

	// Include all faces
	std::vector<int> faceIndexArray;
	faceIndexArray.resize(m_faceDataBank.size());
	for (int i = 0; i < m_faceDataBank.size(); ++i) {
		faceIndexArray[i] = i;
	}
	m_faceIndexArrayCache[m_nextFaceIndexCacheKey] = faceIndexArray;
	curSubBox.m_faceIndexCacheKey = m_nextFaceIndexCacheKey;
	m_nextFaceIndexCacheKey++;

	if (genObjList) {
		// Convert world object list into an array for fast lookup
		BuildObjectReferenceArray(worldList);
		// Include all objects
		curSubBox.m_objectReferenceList.resize(m_wldObjectReferenceArray.size());
		for (int i = 0; i < m_wldObjectReferenceArray.size(); ++i) {
			curSubBox.m_objectReferenceList[i] = i;
		}
	}

	curSubBox.m_subTableLevel = -1;
	curSubBox.m_subBoxCollection = NULL;

	//Generate table
	BOOL res = GenerateSubTable(&curSubBox, m_baseBox, hwndProgressBars);
	m_boxCollection = curSubBox.m_subBoxCollection;
	m_compressed = true;
	return res;
}

void CCollisionObj::SubLevelSave(REFBOX*** CurrentSubBox, CArchive& ar, std::set<unsigned>& cacheKeys) {
	for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
		for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
			for (int zPos = 0; zPos < m_divResZ[0]; zPos++) {
				if (CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection == NULL) {
					//if bottom level save data
					ar << 1;
					jsAssert(CurrentSubBox[xPos][yPos][zPos].m_faceIndexCacheKey != REFBOX::INVALID_FACE_INDEX_CACHE_KEY);
					ar << CurrentSubBox[xPos][yPos][zPos].m_faceIndexCacheKey;
					cacheKeys.insert(CurrentSubBox[xPos][yPos][zPos].m_faceIndexCacheKey);
					ar << CurrentSubBox[xPos][yPos][zPos].m_subTableLevel;
					ar << (WORD)CurrentSubBox[xPos][yPos][zPos].m_objectReferenceList.size();
					for (auto objIndex : CurrentSubBox[xPos][yPos][zPos].m_objectReferenceList)
						ar << objIndex;
				} else {
					//not bottom level
					ar << 0;
					SubLevelSave(CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection, ar, cacheKeys);
				}
			}
		}
	}
}

void CCollisionObj::SubLevelLoad(REFBOX*** CurrentSubBox, int curLevel, CArchive& ar, UINT schemaVersion) {
	for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
		for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
			for (int zPos = 0; zPos < m_divResZ[0]; zPos++) {
				int isLeafBox = 1;
				ar >> isLeafBox;

				if (isLeafBox == 1) {
					size_t faceCount = 0;
					if (schemaVersion == 0) {
						// Ver 0: face index array size
						ar >> faceCount;
					} else {
						// Ver 1 and above: use face index array cache
						ar >> CurrentSubBox[xPos][yPos][zPos].m_faceIndexCacheKey;
					}

					ar >> CurrentSubBox[xPos][yPos][zPos].m_subTableLevel;
					jsAssert(CurrentSubBox[xPos][yPos][zPos].m_subTableLevel == curLevel);
					WORD objectCount = 0;
					ar >> objectCount;

					if (schemaVersion == 0) {
						// Convert ver 0 face array into cache
						CurrentSubBox[xPos][yPos][zPos].m_faceIndexCacheKey = m_nextFaceIndexCacheKey;
						std::vector<int>& faceIndexArray = m_faceIndexArrayCache[m_nextFaceIndexCacheKey];
						m_nextFaceIndexCacheKey++;

						if (faceCount > 0) {
							faceIndexArray.resize(faceCount);
							for (int i = 0; i < faceCount; ++i) {
								ar >> faceIndexArray[i];
							}
						}
					}

					CurrentSubBox[xPos][yPos][zPos].m_objectReferenceList.clear();
					if (objectCount > 0) {
						CurrentSubBox[xPos][yPos][zPos].m_objectReferenceList.resize(objectCount);
						for (int i = 0; i < objectCount; ++i) {
							ar >> CurrentSubBox[xPos][yPos][zPos].m_objectReferenceList[i];
						}
					}

					CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection = NULL;
				} else {
					CurrentSubBox[xPos][yPos][zPos].m_faceIndexCacheKey = REFBOX::INVALID_FACE_INDEX_CACHE_KEY;
					CurrentSubBox[xPos][yPos][zPos].m_objectReferenceList.clear();
					CurrentSubBox[xPos][yPos][zPos].m_subTableLevel = curLevel;
					CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection = new REFBOX**[m_divResX[0]];
					for (int m = 0; m < m_divResX[0]; m++) {
						CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection[m] = new REFBOX*[m_divResY[0]];
						for (int n = 0; n < m_divResY[0]; n++) {
							CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection[m][n] = new REFBOX[m_divResZ[0]];
						}
					}
					SubLevelLoad(CurrentSubBox[xPos][yPos][zPos].m_subBoxCollection, curLevel + 1, ar, schemaVersion);
				}
			}
		}
	}
}

void CCollisionObj::Serialize(CArchive& ar) {
	//============================================================
	// CCollisionObj serialization schema
	//
	// Ver 0 - original
	// Ver 1 - YC 06/2015
	//  * Added a global flag indicating whether object reference
	//    is stored (not for dynamic objects)
	//  * Move per-box face index array into a global cache for
	//    compression
	//============================================================

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		int hasContent = 0;
		if (m_divResX)
			hasContent = 1;

		ar << m_useObjectRef
		   << m_faceDataBank.size()
		   << m_numberOfLevels
		   << m_maxFacesPerBox
		   << m_baseBox.maxX
		   << m_baseBox.maxY
		   << m_baseBox.maxZ
		   << m_baseBox.minX
		   << m_baseBox.minY
		   << m_baseBox.minZ
		   << hasContent;

		//save poly array
		for (auto faceData : m_faceDataBank) {
			ar << faceData.m_normal
			   << faceData.m_vertices[0]
			   << faceData.m_vertices[1]
			   << faceData.m_vertices[2];
			if (m_useObjectRef) {
				ar << faceData.m_objectIndexRef;
			}
		}

		if (hasContent == 1) {
			for (size_t aIndex = 0; aIndex < m_numberOfLevels; aIndex++) {
				ar << m_divResX[aIndex]
				   << m_divResY[aIndex]
				   << m_divResZ[aIndex]
				   << m_boxSizeX[aIndex]
				   << m_boxSizeY[aIndex]
				   << m_boxSizeZ[aIndex];
			}

			std::set<unsigned> cacheKeys;
			SubLevelSave(m_boxCollection, ar, cacheKeys);

			// Save shared face index array cache
			ar << cacheKeys.size(); // Number of cache items
			for (auto cacheKey : cacheKeys) {
				ar << cacheKey; // Key
				const std::vector<int>& faceIndexArray = GetFaceIndexArray(cacheKey);
				ar << faceIndexArray.size(); // Face index array size
				for (auto faceIndex : faceIndexArray) {
					ar << faceIndex; // Face index
				}
			}
		}

	} else {
		//loading
		UINT schemaVersion = ar.GetObjectSchema();
		if (schemaVersion == 0) {
			m_useObjectRef = true; // Always contains object references
			m_compressed = false;
		} else {
			ar >> m_useObjectRef;
			m_compressed = true;
		}

		// Make sure data have not been loaded
		jsAssert(m_nextFaceIndexCacheKey == 0 && m_faceIndexArrayCache.empty());

		m_numberOfLevels = DEF_COL_LEVELSDEEP;
		m_maxFacesPerBox = DEF_COL_FACESPERBOX;
		m_divResX = NULL;
		m_divResY = NULL;
		m_divResZ = NULL;
		m_boxSizeX = NULL;
		m_boxSizeY = NULL;
		m_boxSizeZ = NULL;
		m_wldObjList = NULL;
		m_boxCollection = NULL;

		int hasContent = 0;
		int totalFaceCount = 0;
		ar >> totalFaceCount >> m_numberOfLevels >> m_maxFacesPerBox >> m_baseBox.maxX >> m_baseBox.maxY >> m_baseBox.maxZ >> m_baseBox.minX >> m_baseBox.minY >> m_baseBox.minZ >> hasContent;

		//load poly array
		m_faceDataBank.clear();
		if (totalFaceCount > 0) {
			m_faceDataBank.resize(totalFaceCount);
			for (int i = 0; i < totalFaceCount; i++) {
				ar >> m_faceDataBank[i].m_normal >> m_faceDataBank[i].m_vertices[0] >> m_faceDataBank[i].m_vertices[1] >> m_faceDataBank[i].m_vertices[2];

				if (m_useObjectRef) {
					ar >> m_faceDataBank[i].m_objectIndexRef;
				} else {
					m_faceDataBank[i].m_objectIndexRef = 0;
				}

				m_faceDataBank[i].m_box = GetPolyBoundingBox(m_faceDataBank[i].m_vertices);
			}
		}

		if (hasContent == 1) {
			//has content
			m_divResX = new int[m_numberOfLevels];
			m_divResY = new int[m_numberOfLevels];
			m_divResZ = new int[m_numberOfLevels];
			m_boxSizeX = new float[m_numberOfLevels];
			m_boxSizeY = new float[m_numberOfLevels];
			m_boxSizeZ = new float[m_numberOfLevels];
			for (size_t aIndex = 0; aIndex < m_numberOfLevels; aIndex++) {
				ar >> m_divResX[aIndex] >> m_divResY[aIndex] >> m_divResZ[aIndex] >> m_boxSizeX[aIndex] >> m_boxSizeY[aIndex] >> m_boxSizeZ[aIndex];

				// Check if all of them are the same
				// Notify YC if any of the following assertions failed
				jsAssert(m_divResX[aIndex] == m_divResX[0]);
				jsAssert(m_divResY[aIndex] == m_divResY[0]);
				jsAssert(m_divResZ[aIndex] == m_divResZ[0]);
			}

			//allocate for root boxes
			m_boxCollection = new REFBOX**[m_divResX[0]];
			for (int m = 0; m < m_divResX[0]; m++) {
				m_boxCollection[m] = new REFBOX*[m_divResY[0]];
				for (int n = 0; n < m_divResY[0]; n++) {
					m_boxCollection[m][n] = new REFBOX[m_divResZ[0]];
				}
			}

			SubLevelLoad(m_boxCollection, 0, ar, schemaVersion);

			// Load shared face index array cache
			if (schemaVersion > 0) {
				size_t cacheEntryCount;
				ar >> cacheEntryCount;
				for (auto ci = 0; ci < cacheEntryCount; ++ci) {
					unsigned cacheKey = 0;
					ar >> cacheKey;
					jsAssert(cacheKey != REFBOX::INVALID_FACE_INDEX_CACHE_KEY);

					std::vector<int>& faceIndexArray = m_faceIndexArrayCache[cacheKey];
					jsAssert(faceIndexArray.empty());

					size_t arraySize = 0;
					ar >> arraySize;
					for (auto ai = 0; ai < arraySize; ++ai) {
						int faceIndex = 0;
						ar >> faceIndex;
						faceIndexArray.push_back(faceIndex);
					}
				}
			}
		} //end has content

		jsAssert(m_wldObjectReferenceArray.empty());
		m_wldObjectReferenceArray.clear();
	} //end loading
}

// Added by jonny 11/17/08 because we need a CCollisionObj clone to get rid of memory leaks.
// This is very expensive, and is only used in the editor.
void CCollisionObj::Clone(CCollisionObj** clone) {
	CCollisionObj* cloneReturn = new CCollisionObj();
	cloneReturn->m_divResX = NULL;
	cloneReturn->m_divResY = NULL;
	cloneReturn->m_divResZ = NULL;
	cloneReturn->m_boxSizeX = NULL;
	cloneReturn->m_boxSizeY = NULL;
	cloneReturn->m_boxSizeZ = NULL;
	cloneReturn->m_wldObjList = NULL;
	cloneReturn->m_boxCollection = NULL;

	cloneReturn->m_numberOfLevels = m_numberOfLevels;
	cloneReturn->m_maxFacesPerBox = m_maxFacesPerBox;
	cloneReturn->m_baseBox = m_baseBox;

	cloneReturn->m_faceDataBank = m_faceDataBank;

	if (m_divResX) {
		cloneReturn->m_divResX = new int[cloneReturn->m_numberOfLevels];
		cloneReturn->m_divResY = new int[cloneReturn->m_numberOfLevels];
		cloneReturn->m_divResZ = new int[cloneReturn->m_numberOfLevels];
		cloneReturn->m_boxSizeX = new float[cloneReturn->m_numberOfLevels];
		cloneReturn->m_boxSizeY = new float[cloneReturn->m_numberOfLevels];
		cloneReturn->m_boxSizeZ = new float[cloneReturn->m_numberOfLevels];

		for (size_t aIndex = 0; aIndex < cloneReturn->m_numberOfLevels; aIndex++) {
			cloneReturn->m_divResX[aIndex] = m_divResX[aIndex];
			cloneReturn->m_divResY[aIndex] = m_divResY[aIndex];
			cloneReturn->m_divResZ[aIndex] = m_divResZ[aIndex];
			cloneReturn->m_boxSizeX[aIndex] = m_boxSizeX[aIndex];
			cloneReturn->m_boxSizeY[aIndex] = m_boxSizeY[aIndex];
			cloneReturn->m_boxSizeZ[aIndex] = m_boxSizeZ[aIndex];
		}

		//allocate for root boxes
		cloneReturn->m_boxCollection = new REFBOX**[cloneReturn->m_divResX[0]];
		for (int m = 0; m < cloneReturn->m_divResX[0]; m++) {
			cloneReturn->m_boxCollection[m] = new REFBOX*[cloneReturn->m_divResY[0]];
			for (int n = 0; n < cloneReturn->m_divResY[0]; n++) {
				cloneReturn->m_boxCollection[m][n] = new REFBOX[cloneReturn->m_divResZ[0]];
			}
		}

		CloneSubBox(cloneReturn->m_boxCollection, m_boxCollection);
	}
	*clone = cloneReturn;
}

void CCollisionObj::CloneSubBox(REFBOX*** cloneBox, REFBOX*** sourceBox) {
	for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
		for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
			for (int zPos = 0; zPos < m_divResZ[0]; zPos++) {
				if (sourceBox[xPos][yPos][zPos].m_subBoxCollection == NULL) {
					cloneBox[xPos][yPos][zPos].m_subTableLevel = sourceBox[xPos][yPos][zPos].m_subTableLevel;
					cloneBox[xPos][yPos][zPos].m_faceIndexCacheKey = sourceBox[xPos][yPos][zPos].m_faceIndexCacheKey;
					cloneBox[xPos][yPos][zPos].m_objectReferenceList = sourceBox[xPos][yPos][zPos].m_objectReferenceList;
					cloneBox[xPos][yPos][zPos].m_subBoxCollection = NULL;
				} else {
					cloneBox[xPos][yPos][zPos].m_faceIndexCacheKey = REFBOX::INVALID_FACE_INDEX_CACHE_KEY;
					cloneBox[xPos][yPos][zPos].m_objectReferenceList.clear();
					cloneBox[xPos][yPos][zPos].m_subBoxCollection = new REFBOX**[m_divResX[0]];
					for (int m = 0; m < m_divResX[0]; m++) {
						cloneBox[xPos][yPos][zPos].m_subBoxCollection[m] = new REFBOX*[m_divResY[0]];
						for (int n = 0; n < m_divResY[0]; n++) {
							cloneBox[xPos][yPos][zPos].m_subBoxCollection[m][n] = new REFBOX[m_divResZ[0]];
						}
					}
					// recursion is fun
					CloneSubBox(cloneBox[xPos][yPos][zPos].m_subBoxCollection, sourceBox[xPos][yPos][zPos].m_subBoxCollection);
				}
			}
		}
	}
}

bool CCollisionObj::Compare(CCollisionObj* col1, CCollisionObj* col2) {
	if (col1 == NULL && col2 == NULL) {
		return true;
	}
	if (col1 == NULL || col2 == NULL) {
		return false;
	}
	if (col1->m_faceDataBank.size() != col2->m_faceDataBank.size()) {
		return false;
	}
	if (col1->m_numberOfLevels != col2->m_numberOfLevels) {
		return false;
	}
	if (col1->m_maxFacesPerBox != col2->m_maxFacesPerBox) {
		return false;
	}
	if (col1->m_baseBox != col2->m_baseBox) {
		return false;
	}
	for (int i = 0; i < col1->m_faceDataBank.size(); i++) {
		if (col1->m_faceDataBank[i].m_normal.x != col2->m_faceDataBank[i].m_normal.x) {
			// I hate whoever designed this.  NAN appears to be a valid value in this data structure
			if (!(_isnan(col1->m_faceDataBank[i].m_normal.x) || _isnan(col2->m_faceDataBank[i].m_normal.x))) {
				return false;
			}
		}
		if (col1->m_faceDataBank[i].m_normal.y != col2->m_faceDataBank[i].m_normal.y) {
			// I hate whoever designed this.  NAN appears to be a valid value in this data structure
			if (!(_isnan(col1->m_faceDataBank[i].m_normal.y) || _isnan(col2->m_faceDataBank[i].m_normal.y))) {
				return false;
			}
		}
		if (col1->m_faceDataBank[i].m_normal.z != col2->m_faceDataBank[i].m_normal.z) {
			// I hate whoever designed this.  NAN appears to be a valid value in this data structure
			if (!(_isnan(col1->m_faceDataBank[i].m_normal.z) || _isnan(col2->m_faceDataBank[i].m_normal.z))) {
				return false;
			}
		}
		for (size_t vi = 0; vi < 3; vi++) {
			if (col1->m_faceDataBank[i].m_vertices[vi] != col2->m_faceDataBank[i].m_vertices[vi]) {
				return false;
			}
		}
		if (col1->m_faceDataBank[i].m_objectIndexRef != col2->m_faceDataBank[i].m_objectIndexRef) {
			return false;
		}
		if (col1->m_faceDataBank[i].m_box != col2->m_faceDataBank[i].m_box) {
			return false;
		}
	}

	if (col1->m_divResX && col2->m_divResX) {
		for (size_t aIndex = 0; aIndex < col1->m_numberOfLevels; aIndex++) {
			if (col1->m_divResX[aIndex] != col2->m_divResX[aIndex]) {
				return false;
			}
			if (col1->m_divResY[aIndex] != col2->m_divResY[aIndex]) {
				return false;
			}
			if (col1->m_divResZ[aIndex] != col2->m_divResZ[aIndex]) {
				return false;
			}
			if (col1->m_boxSizeX[aIndex] != col2->m_boxSizeX[aIndex]) {
				// I hate whoever designed this.  NAN appears to be a valid value in this data structure
				if (!(_isnan(col1->m_boxSizeX[aIndex]) || _isnan(col2->m_boxSizeX[aIndex]))) {
					return false;
				}
			}
			if (col1->m_boxSizeY[aIndex] != col2->m_boxSizeY[aIndex]) {
				// I hate whoever designed this.  NAN appears to be a valid value in this data structure
				if (!(_isnan(col1->m_boxSizeY[aIndex]) || _isnan(col2->m_boxSizeY[aIndex]))) {
					return false;
				}
			}
			if (col1->m_boxSizeZ[aIndex] != col2->m_boxSizeZ[aIndex]) {
				// I hate whoever designed this.  NAN appears to be a valid value in this data structure
				if (!(_isnan(col1->m_boxSizeZ[aIndex]) || _isnan(col2->m_boxSizeZ[aIndex]))) {
					return false;
				}
			}
		}

		if (col1->CompareSubBox(col1->m_boxCollection, col2->m_boxCollection) == false) {
			return false;
		}
	}
	return true;
}

bool CCollisionObj::CompareSubBox(REFBOX*** box1, REFBOX*** box2) {
	for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
		for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
			for (int zPos = 0; zPos < m_divResZ[0]; zPos++) {
				if (box1[xPos][yPos][zPos].m_subBoxCollection == NULL && box2[xPos][yPos][zPos].m_subBoxCollection == NULL) {
					if (box1[xPos][yPos][zPos].m_subTableLevel != box2[xPos][yPos][zPos].m_subTableLevel) {
						return false;
					}
					if (box1[xPos][yPos][zPos].m_faceIndexCacheKey != box2[xPos][yPos][zPos].m_faceIndexCacheKey) {
						return false;
					}
					if (box1[xPos][yPos][zPos].m_objectReferenceList != box2[xPos][yPos][zPos].m_objectReferenceList) {
						return false;
					}
				} else {
					if (box1[xPos][yPos][zPos].m_subBoxCollection == NULL || box2[xPos][yPos][zPos].m_subBoxCollection == NULL) {
						return false;
					}
					// recursion is fun
					if (CompareSubBox(box1[xPos][yPos][zPos].m_subBoxCollection, box2[xPos][yPos][zPos].m_subBoxCollection) == false) {
						return false;
					}
				}
			}
		}
	}
	return true;
}

BoundBox CCollisionObj::GetPolyBoundingBox(const Vector3f poly[3]) {
	BoundBox box;
	for (UINT i = 0; i < 3; i++)
		box.merge(poly[i]);

	box.validate(false, true);

	const float c_tolerance = 0.000001f;
	if (fabs(box.minX - box.maxX) < c_tolerance)
		box.maxX += c_tolerance;
	if (fabs(box.minY - box.maxY) < c_tolerance)
		box.maxY += c_tolerance;
	if (fabs(box.minZ - box.maxZ) < c_tolerance)
		box.maxZ += c_tolerance;

	return box;
}

BoundBox CCollisionObj::GetSegmentBoundingBox(const Vector3f& segmentStart, const Vector3f& segmentEnd) const {
	BoundBox box(segmentStart.x, segmentStart.y, segmentStart.z, segmentEnd.x, segmentEnd.y, segmentEnd.z);
	box.validate(true, true);
	return box;
}

// Return signed distance value between a point and a plane
float CCollisionObj::GetPointToPlaneDistance(const Vector3f triangle[3], const Vector3f& ptloc) {
	Vector3f a = triangle[1] - triangle[0];
	Vector3f b = triangle[2] - triangle[0];
	Vector3f u = ptloc - triangle[0];
	return a.Cross(b).GetNormalized().Dot(u); // INF (div-by-zero) if the triangle is degenerated
}

void CCollisionObj::GetLinePlaneIntersection(const Vector3f triangle[3], const Vector3f& pt1, const Vector3f& pt2, Vector3f& intersection) {
	Vector3f normal = (triangle[1] - triangle[0]).Cross(triangle[2] - triangle[0]); // not a unit vector
	Vector3f u = pt1 - pt2;
	Vector3f v = pt1 - triangle[0];
	intersection = pt1 - u * normal.Dot(v) / normal.Dot(u); // INF (div-by-zero) if line is perpendicular to normal (or parallel to plane)
}

BOOL CCollisionObj::SegOverlap(const Vector3f& pt1, const Vector3f& pt2, const Vector3f& pt3, const Vector3f& pt4) {
	float t1, t2;

	if ((pt2.x - pt1.x) != 0.0f) {
		t1 = (pt3.x - pt1.x) / (pt2.x - pt1.x);
		t2 = (pt4.x - pt1.x) / (pt2.x - pt1.x);
	} else if ((pt2.y - pt1.y) != 0.0f) {
		t1 = (pt3.y - pt1.y) / (pt2.y - pt1.y);
		t2 = (pt4.y - pt1.y) / (pt2.y - pt1.y);
	} else if ((pt2.z - pt1.z) != 0.0f) {
		t1 = (pt3.z - pt1.z) / (pt2.z - pt1.z);
		t2 = (pt4.z - pt1.z) / (pt2.z - pt1.z);
	} else
		return FALSE;

	if (((t1 < 0) && (t2 < 0)) || ((t1 > 1) && (t2 > 1)))
		return FALSE;

	return TRUE;
}

//-----This function determines Exact collision between two----//
//-----Polygons - also will get exact location of intersection-//
//-----so you could have something happen there----------------//
BOOL CCollisionObj::TestTrianglesIntersection(const Vector3f triangle1[3], const Vector3f triangle2[3]) {
	Vector3f interPtLoc1, interPtLoc2, interPtLoc3, interPtLoc4;

	// Triangle 2: D-E-F against plane 1
	float relativePtLoc1 = GetPointToPlaneDistance(triangle1, triangle2[0]);
	float relativePtLoc2 = GetPointToPlaneDistance(triangle1, triangle2[1]);
	float relativePtLoc3 = GetPointToPlaneDistance(triangle1, triangle2[2]);

	// 1) Co-planar
	if (relativePtLoc1 == 0.0f && relativePtLoc2 == 0.0f && relativePtLoc3 == 0.0f)
		return FALSE;

	// 2) One or two vertices of triangle 2 falls on plane 1: considered as no collision
	if (relativePtLoc1 == 0.0f || relativePtLoc2 == 0.0f || relativePtLoc3 == 0.0f)
		return FALSE;

	// 3) All three vertices of triangle 2 are at the same side of plane 1: no collision
	if (((relativePtLoc1 > 0.0f) && (relativePtLoc2 > 0.0f) && (relativePtLoc3 > 0.0f)) ||
		((relativePtLoc1 < 0.0f) && (relativePtLoc2 < 0.0f) && (relativePtLoc3 < 0.0f)))
		return FALSE;

	// Triangle 1: A-B-C against plane 2
	float relativePtLoc4 = GetPointToPlaneDistance(triangle2, triangle1[0]);
	float relativePtLoc5 = GetPointToPlaneDistance(triangle2, triangle1[1]);
	float relativePtLoc6 = GetPointToPlaneDistance(triangle2, triangle1[2]);

	// 4) All three vertices of triangle 1 are at the same side of plane 2: no collision
	if (((relativePtLoc4 > 0.0f) && (relativePtLoc5 > 0.0f) && (relativePtLoc6 > 0.0f)) ||
		((relativePtLoc4 < 0.0f) && (relativePtLoc5 < 0.0f) && (relativePtLoc6 < 0.0f)))
		return FALSE;

	// 5) Plane 1 intersects plane 2

	// 5.a) Plane 1 intersects ploygon 2: 1 vertex at one side, 2 at the opposite side: collision possible
	if (relativePtLoc1 > 0.0f && relativePtLoc2 > 0.0f || relativePtLoc1 < 0.0 && relativePtLoc2 < 0.0f) {
		// 5.a.1) DE vs F: intersect DF, EF with plane 1
		GetLinePlaneIntersection(triangle1, triangle2[2], triangle2[0], interPtLoc1);
		GetLinePlaneIntersection(triangle1, triangle2[2], triangle2[1], interPtLoc2);
	} else if (relativePtLoc1 > 0.0f && relativePtLoc3 > 0.0f || relativePtLoc1 < 0.0f && relativePtLoc3 < 0.0f) {
		// 5.a.2) DF vs E: intersect DE, FE with plane 1
		GetLinePlaneIntersection(triangle1, triangle2[1], triangle2[0], interPtLoc1);
		GetLinePlaneIntersection(triangle1, triangle2[1], triangle2[2], interPtLoc2);
	} else {
		// 5.a.3) EF vs D: intersect ED, FD with plane 1
		GetLinePlaneIntersection(triangle1, triangle2[0], triangle2[1], interPtLoc1);
		GetLinePlaneIntersection(triangle1, triangle2[0], triangle2[2], interPtLoc2);
	}

	// 5.b) Plane 2 intersects ploygon 1: 1 vertex at one side, 2 at the opposite side: collision possible
	if (relativePtLoc4 > 0.0f && relativePtLoc5 > 0.0f || relativePtLoc4 < 0.0f && relativePtLoc5 < 0.0f) {
		// 5.b.1) AB vs C: intersect AB, AC with plane 2
		GetLinePlaneIntersection(triangle2, triangle1[2], triangle1[0], interPtLoc3);
		GetLinePlaneIntersection(triangle2, triangle1[2], triangle1[1], interPtLoc4);
	} else if (relativePtLoc4 > 0.0f && relativePtLoc6 > 0.0f || relativePtLoc4 < 0.0f && relativePtLoc6 < 0.0f) {
		// 5.b.2) AC vs B: intersect AB, CB with plane 2
		GetLinePlaneIntersection(triangle2, triangle1[1], triangle1[0], interPtLoc3);
		GetLinePlaneIntersection(triangle2, triangle1[1], triangle1[2], interPtLoc4);
	} else {
		// 5.b.3) BC vs A: intersect BA, CA with plane 2
		GetLinePlaneIntersection(triangle2, triangle1[0], triangle1[1], interPtLoc3);
		GetLinePlaneIntersection(triangle2, triangle1[0], triangle1[2], interPtLoc4);
	}

	// Both P1-P2 and P3-P4 lay on the intersection between plane 1 and plane 2
	// If P1-P2 and P3-P4 overlaps: collision occurred
	return SegOverlap(interPtLoc1, interPtLoc2, interPtLoc3, interPtLoc4);
}

bool CCollisionObj::BoundingBoxToBoxCheck(const BoundBox& box1, const BoundBox& box2) const {
	return box1.overlaps(box2);
}

CCollisionObj* CCollisionObjList::GetByIndex(int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;

	CCollisionObj* cPtr = (CCollisionObj*)GetAt(posLoc);
	return cPtr;
}

void CCollisionObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CCollisionObj* delPtr = (CCollisionObj*)GetNext(posLoc);
		delPtr->SafeDelete();
		delete delPtr;
		delPtr = 0;
	}
	RemoveAll();
}

void CCollisionObj::Compress(REFBOX* curBox) {
	if (m_compressed) {
		ASSERT(false);
		return;
	}

	REFBOX*** boxCollection;
	if (curBox == nullptr) {
		boxCollection = m_boxCollection;
	} else {
		boxCollection = curBox->m_subBoxCollection;
	}

	// Pass 1 : scan bottom up to regenerate face list for intermediate nodes
	std::set<int> faceIndexCombinedSet;
	for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
		for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
			for (int zPos = 0; zPos < m_divResZ[0]; zPos++) {
				REFBOX* childBox = &boxCollection[xPos][yPos][zPos];
				if (childBox->m_subBoxCollection != NULL) {
					// Intermediate node - collect all faces from all descendants
					jsAssert(childBox->m_faceIndexCacheKey == REFBOX::INVALID_FACE_INDEX_CACHE_KEY);
					jsAssert(m_faceIndexArrayCache.find(m_nextFaceIndexCacheKey) == m_faceIndexArrayCache.end());

					std::vector<int>& childFaceIndexArray = m_faceIndexArrayCache[m_nextFaceIndexCacheKey];
					childBox->m_faceIndexCacheKey = m_nextFaceIndexCacheKey;
					m_nextFaceIndexCacheKey++;

					Compress(childBox);
					std::copy(childFaceIndexArray.begin(), childFaceIndexArray.end(), std::inserter(faceIndexCombinedSet, faceIndexCombinedSet.end()));
				} else {
					// Leaf node - get array directly
					const std::vector<int>& childBoxFaceIndexArray = GetFaceIndexArray(childBox->m_faceIndexCacheKey);
					std::copy(childBoxFaceIndexArray.begin(), childBoxFaceIndexArray.end(), std::inserter(faceIndexCombinedSet, faceIndexCombinedSet.end()));
				}
			}
		}
	}

	if (curBox) {
		jsAssert(curBox->m_faceIndexCacheKey != REFBOX::INVALID_FACE_INDEX_CACHE_KEY);
		auto itr = m_faceIndexArrayCache.find(curBox->m_faceIndexCacheKey);
		jsAssert(itr != m_faceIndexArrayCache.end());
		if (itr != m_faceIndexArrayCache.end()) {
			std::vector<int>& curFaceIndexArray = itr->second;

			// Save combined array to current box
			jsAssert(curFaceIndexArray.empty());
			curFaceIndexArray.clear();
			std::copy(faceIndexCombinedSet.begin(), faceIndexCombinedSet.end(), std::back_inserter(curFaceIndexArray));

			// Pass 2: scan all child nodes for compression possibilities
			for (int xPos = 0; xPos < m_divResX[0]; xPos++) {
				for (int yPos = 0; yPos < m_divResY[0]; yPos++) {
					for (int zPos = 0; zPos < m_divResZ[0]; zPos++) {
						REFBOX* childBox = &boxCollection[xPos][yPos][zPos];
						jsAssert(childBox->m_faceIndexCacheKey != REFBOX::INVALID_FACE_INDEX_CACHE_KEY);
						if (childBox->m_faceIndexCacheKey != REFBOX::INVALID_FACE_INDEX_CACHE_KEY) {
							const std::vector<int>& childFaceIndexArray = GetFaceIndexArray(childBox->m_faceIndexCacheKey);
							if (childFaceIndexArray.size() > COMPRESSION_FACE_COUNT_THRESHOLD && childFaceIndexArray.size() * 1.0 / curFaceIndexArray.size() > COMPRESSION_FACE_SUBSET_THRESHOLD) {
								// Reuse parent box's face array
								childBox->m_faceIndexCacheKey = curBox->m_faceIndexCacheKey;
							}
						}
					}
				}
			}
		}
	}

	if (curBox == nullptr) {
		// completed: set flag
		m_compressed = true;
	}
}

std::vector<Vector3f> CCollisionObj::GetTriangles() const {
	std::vector<Vector3f> aTriangles;
	for (const CFaceStruct& face : m_faceDataBank)
		aTriangles.insert(aTriangles.end(), face.m_vertices, face.m_vertices + 3);
	return aTriangles;
}

namespace {
bool InTolerance(const Vector3f& pt1, const Vector3f& pt2, float fTolerance) {
	for (size_t i = 0; i < 3; ++i) {
		if (std::abs(pt1[i] - pt2[i]) > fTolerance)
			return false;
	}
	return true;
}
} // namespace

bool CCollisionObj::ContainsTriangle(const Vector3f& pt1, const Vector3f& pt2, const Vector3f& pt3, float fTolerance, size_t* piTriangleIndex) {
	if (!m_boxCollection)
		return false;
	if (!m_boxSizeX || !m_boxSizeY || !m_boxSizeZ)
		return false;

	auto TestBoxFunction = [pt1, pt2, pt3, this, piTriangleIndex, fTolerance](const BoxVisitCallbackArg& arg) -> bool {
		unsigned int key = arg.m_pBox->m_faceIndexCacheKey;
		auto itr = this->m_faceIndexArrayCache.find(key);
		if (itr != this->m_faceIndexArrayCache.end()) {
			// Test all triangles in this box for equality with our test triangle.
			for (int iFaceIndex : itr->second) {
				// Compare each triangle, testing all permutations of triangle vertices.
				const CFaceStruct& faceStruct = this->m_faceDataBank[iFaceIndex];
				for (size_t i = 0; i < 3; ++i) {
					if (InTolerance(faceStruct.m_vertices[i], pt1, fTolerance)) {
						if (InTolerance(faceStruct.m_vertices[(i + 1) % 3], pt2, fTolerance) && InTolerance(faceStruct.m_vertices[(i + 2) % 3], pt3, fTolerance)) {
							if (piTriangleIndex)
								*piTriangleIndex = iFaceIndex;
							return false;
						} else if (InTolerance(faceStruct.m_vertices[(i + 1) % 3], pt3, fTolerance) && InTolerance(faceStruct.m_vertices[(i + 2) % 3], pt2, fTolerance)) {
							if (piTriangleIndex)
								*piTriangleIndex = iFaceIndex;
							return false;
						}
					}
				}
			}
		}
		return true;
	};
	Vector3f vTolerance(fTolerance);
	Vector3f ptCenter = (pt1 + pt2 + pt3) * (1.0f / 3.0f);
	Vector3f ptTestMin = ptCenter - vTolerance;
	Vector3f ptTestMax = ptCenter + vTolerance;
	bool bNotFound = VisitBoxesInRange(ptTestMin, ptTestMax, TestBoxFunction);
	return !bNotFound;
}

float CCollisionObj::ClosestTriangle(const Vector3f& pt0, const Vector3f& pt1, const Vector3f& pt2, Vector3f* pvErrorDirection, Vector3f (*paptsClosestTriangle)[3], size_t* pClosestTriangleIndex) const {
	static const size_t permutations[6][3] = { { 0, 1, 2 }, { 0, 2, 1 }, { 1, 0, 2 }, { 1, 2, 0 }, { 2, 0, 1 }, { 2, 1, 0 } };

	const Vector3f* papts[3] = { &pt0, &pt1, &pt2 };
	size_t iClosestFace = SIZE_MAX;
	float fThisTriangleError = FLT_MAX;
	Vector3f vDirectionOfThisTriangleError(0, 0, 0);
	for (size_t iFace = 0; iFace < m_faceDataBank.size(); ++iFace) {
		const CFaceStruct& face = m_faceDataBank[iFace];
		for (size_t j = 0; j < 6; ++j) {
			float fThisPermutationError = -1;
			Vector3f vDirectionOfThisPermutationError;
			for (size_t k = 0; k < 3; ++k) {
				Vector3f vThisPointDirectionError = *papts[permutations[j][k]] - face.m_vertices[k];
				float fThisPointError = vThisPointDirectionError.Length();
				if (fThisPointError > fThisPermutationError) {
					fThisPermutationError = fThisPointError;
					vDirectionOfThisPermutationError = vThisPointDirectionError;
				}
			}
			if (fThisPermutationError < fThisTriangleError) {
				fThisTriangleError = fThisPermutationError;
				vDirectionOfThisTriangleError = vDirectionOfThisPermutationError;
				iClosestFace = iFace;
			}
		}
	}

	if (pClosestTriangleIndex)
		*pClosestTriangleIndex = iClosestFace;
	if (pvErrorDirection)
		*pvErrorDirection = vDirectionOfThisTriangleError;
	if (paptsClosestTriangle && iClosestFace < m_faceDataBank.size())
		std::copy(m_faceDataBank[iClosestFace].m_vertices, m_faceDataBank[iClosestFace].m_vertices + 3, *paptsClosestTriangle);
	return fThisTriangleError;
}

bool CCollisionObj::CreateReMeshFromCollisionData(ReD3DX9DeviceState* dev, bool bUseTriStrip, std::vector<std::unique_ptr<ReMesh>>* paMeshes) {
	struct HashVector {
		size_t operator()(const Vector3f& v) const {
			return boost::hash_range(v.begin(), v.end());
		}
	};

	struct VertexData {
		size_t m_iVertexIndex;
		size_t m_iTriangleGroup;
		size_t m_iInListForTriangleGroup;
	};
	std::unordered_map<Vector3f, VertexData, HashVector, std::equal_to<Vector3f>> vertexMap;

	for (const CFaceStruct& face : m_faceDataBank) {
		for (size_t i = 0; i < 3; ++i)
			vertexMap.insert(std::make_pair(face.m_vertices[i], VertexData{ SIZE_MAX, SIZE_MAX, SIZE_MAX }));
	}

	size_t iNextTriIndex = 0;

	// Meshes are limited in size. Create as many as necessary.
	std::vector<std::unique_ptr<ReMesh>> aMeshes;
	while (iNextTriIndex < m_faceDataBank.size()) {
		size_t iCurrentTriangleGroup = aMeshes.size();

		// Count the maximum number of triangles we can put in a mesh.
		size_t iFirstTriIndexInBatch = iNextTriIndex;
		size_t iNextVertexIndex = 0;
		size_t iLastTriangle = std::min<size_t>(iFirstTriIndexInBatch + 0xffff / 3, m_faceDataBank.size());
		for (; iNextTriIndex < iLastTriangle && iNextVertexIndex <= 0xffff - 3; ++iNextTriIndex) {
			const CFaceStruct& face = m_faceDataBank[iNextTriIndex];
			for (size_t i = 0; i < 3; ++i) {
				VertexData& vd = vertexMap[face.m_vertices[i]];
				if (vd.m_iTriangleGroup != iCurrentTriangleGroup) {
					vd.m_iTriangleGroup = iCurrentTriangleGroup;
					vd.m_iVertexIndex = iNextVertexIndex++;
				}
			}
		}

		// Allocate the mesh
		std::unique_ptr<ReD3DX9StaticMesh> pNewMesh = std::make_unique<ReD3DX9StaticMesh>(dev);

		ReMeshData* pMeshData = nullptr;
		pNewMesh->Access(&pMeshData);

		pMeshData->Version = 0;
		pMeshData->NumV = 0;
		pMeshData->NumI = 0;
		pMeshData->NumUV = 0;

		pMeshData->pMem = nullptr;
		pMeshData->pP = nullptr;
		pMeshData->pN = nullptr;
		pMeshData->pUv = nullptr;
		pMeshData->pI = nullptr;

		pMeshData->PrimType = ReMeshPrimType::TRILIST;

		pMeshData->NumV = iNextVertexIndex;
		pMeshData->NumI = 3 * (iNextTriIndex - iFirstTriIndexInBatch);
		if (!pNewMesh->Allocate())
			return false;

		// Populate the mesh data.
		size_t nextIndex = 0; // Index in triangle index list.
		for (size_t iTriIndex = iFirstTriIndexInBatch; iTriIndex < iNextTriIndex; ++iTriIndex) {
			const CFaceStruct& face = m_faceDataBank[iTriIndex];
			for (size_t i = 0; i < 3; ++i) {
				VertexData& vd = vertexMap[face.m_vertices[i]];
				pMeshData->pI[nextIndex] = static_cast<unsigned short>(vd.m_iVertexIndex);
				if (vd.m_iInListForTriangleGroup != iCurrentTriangleGroup) {
					vd.m_iInListForTriangleGroup = iCurrentTriangleGroup;
					pMeshData->pP[vd.m_iVertexIndex] = face.m_vertices[i];
				}
				++nextIndex;
			}
		}

		pNewMesh->Build(bUseTriStrip);
		aMeshes.push_back(std::move(pNewMesh));
	}

	if (paMeshes)
		*paMeshes = std::move(aMeshes);

	return true;
}

bool CCollisionObj::VisitBoxesInRange(const Vector3f& ptMin, const Vector3f& ptMax, std::function<bool(const BoxVisitCallbackArg&)> visitBoxCallback) {
	struct BoxVisitAlgorithm {
	private:
		std::function<bool(const BoxVisitCallbackArg&)> m_VisitBoxCallback; // Return false to abort iteration.
		CCollisionObj* m_pCollisionObj;
		Vector3f m_ptMin;
		Vector3f m_ptMax;

	public:
		BoxVisitAlgorithm(CCollisionObj* pCollisionObj, const Vector3f& ptMin, const Vector3f& ptMax) :
				m_pCollisionObj(pCollisionObj), m_ptMin(ptMin), m_ptMax(ptMax) {
		}

		// Returns false if search aborted.
		bool VisitBoxes(std::function<bool(const BoxVisitCallbackArg&)> visitBoxCallback) {
			m_VisitBoxCallback = std::move(visitBoxCallback);
			Vector3f vBoxCollectionMin(m_pCollisionObj->m_baseBox.minX, m_pCollisionObj->m_baseBox.minY, m_pCollisionObj->m_baseBox.minZ);
			return VisitSubBoxes(0, m_pCollisionObj->m_boxCollection, vBoxCollectionMin);
		}

	private:
		// Returns false if search aborted.
		bool VisitSubBoxes(size_t curLevel, REFBOX*** boxCollection, const Vector3f& vBoxCollectionMin) {
			if (!boxCollection)
				return true;

			int aRes[3] = { 0, 0, 0 };
			m_pCollisionObj->GetResolution(curLevel, aRes[0], aRes[1], aRes[2]);
			if (aRes[0] <= 0 || aRes[1] <= 0 || aRes[2] <= 0)
				return true;

			Vector3f vBoxDim(
				m_pCollisionObj->m_boxSizeX[curLevel],
				m_pCollisionObj->m_boxSizeY[curLevel],
				m_pCollisionObj->m_boxSizeZ[curLevel]);

			int aiBeginIndices[3];
			int aiEndIndices[3];
			for (size_t i = 0; i < 3; ++i) {
				if (vBoxDim[i] <= 0) {
					aiBeginIndices[i] = 0;
					aiEndIndices[i] = 0;
				} else {
					float fMinOffset = std::max<float>(0.0f, m_ptMin[i] - vBoxCollectionMin[i]);
					if (fMinOffset > vBoxDim[i] * aRes[i])
						return true;
					float fMaxOffset = std::min<float>(aRes[i] * vBoxDim[i], m_ptMax[i] - vBoxCollectionMin[i]);
					if (fMaxOffset < 0)
						return true;

					float fMinRel = fMinOffset / vBoxDim[i];
					aiBeginIndices[i] = static_cast<int>(fMinRel);
					float fMaxRel = fMaxOffset / vBoxDim[i];
					aiEndIndices[i] = static_cast<int>(fMaxRel) + 1; // Add one to include the box that contains the max
					if (aiEndIndices[i] > aRes[i])
						aiEndIndices[i] = aRes[i];
				}
			}

			// Look at all sub boxes intersecting our bounding box.
			for (int x = aiBeginIndices[0]; x < aiEndIndices[0]; ++x) {
				for (int y = aiBeginIndices[1]; y < aiEndIndices[1]; ++y) {
					for (int z = aiBeginIndices[2]; z < aiEndIndices[2]; ++z) {
						Vector3f vSubBoxMin(x * vBoxDim[0], y * vBoxDim[1], z * vBoxDim[2]);

						REFBOX& subBox = boxCollection[x][y][z];
						Vector3f ptSubBoxMin = vBoxCollectionMin + vSubBoxMin;
						Vector3f ptSubBoxMax = ptSubBoxMin + vBoxDim;
						BoxVisitCallbackArg arg = { &subBox, ptSubBoxMin, ptSubBoxMax };
						if (!m_VisitBoxCallback(arg))
							return false;

						// Recursive call to check subboxes of this box.
						if (!VisitSubBoxes(curLevel + 1, subBox.m_subBoxCollection, ptSubBoxMin))
							return false;
					}
				}
			}
			return true;
		}
	};

	BoxVisitAlgorithm bva(this, ptMin, ptMax);
	return bva.VisitBoxes(visitBoxCallback);
}

int CCollisionObj::ValidateBoxes() {
	enum {
		ErrorType_InefficientTriangleInWrongBox = 1,
		ErrorType_BoxMissingTriangle = 2,
		ErrorType_TriangleNotInBoundingBox = 4,
		ErrorType_TriangleMissingFromRoundOffError = 8,
	};
	int iErrorType = 0;
	size_t nInBoxCount = 0;
	const CFaceStruct* pCurrentFace;
	size_t iCurrentFace;
	Vector3f vMaxBoxCoord(
		std::max<float>(std::abs(m_baseBox.minX), std::abs(this->m_baseBox.maxX)),
		std::max<float>(std::abs(m_baseBox.minY), std::abs(this->m_baseBox.maxY)),
		std::max<float>(std::abs(m_baseBox.minZ), std::abs(this->m_baseBox.maxZ)));
	Vector3f vTolerance = vMaxBoxCoord * (FLT_EPSILON * 16);
	auto ProcessBox = [this, &nInBoxCount, &pCurrentFace, &iCurrentFace, vTolerance, &iErrorType](const BoxVisitCallbackArg& arg) -> bool {
		bool bInBox = false;
		int key = arg.m_pBox->m_faceIndexCacheKey;
		auto itr = this->m_faceIndexArrayCache.find(key);
		if (itr != this->m_faceIndexArrayCache.end()) {
			if (std::binary_search(itr->second.begin(), itr->second.end(), iCurrentFace)) {
				bInBox = true;
				++nInBoxCount;
			}
		}
		// Apply tolerance to make correct result more likely.
		Vector3f ptMin = bInBox ? arg.m_ptMin - vTolerance : arg.m_ptMin + vTolerance;
		Vector3f ptMax = bInBox ? arg.m_ptMax + vTolerance : arg.m_ptMax - vTolerance;
		bool bIntersectsBox = IntersectTriangleBox(pCurrentFace->m_vertices[0], pCurrentFace->m_vertices[1], pCurrentFace->m_vertices[2], ptMin, ptMax);

		if (bIntersectsBox) {
			if (arg.m_pBox->m_subBoxCollection == nullptr && !bInBox) {
				// Leaf box does not contain triangle that it is supposed to.
				iErrorType |= ErrorType_BoxMissingTriangle;
			}
		} else {
			if (bInBox) {
				// Box contains triangle that doesn't belong. Will be tested needlessly.
				iErrorType |= ErrorType_InefficientTriangleInWrongBox;
			}
		}
		return true;
	};

	for (size_t iFace = 0; iFace < m_faceDataBank.size(); ++iFace) {
		const CFaceStruct& face = m_faceDataBank[iFace];
		pCurrentFace = &face;
		iCurrentFace = iFace;

		nInBoxCount = 0;
		VisitAllBoxes(ProcessBox);
		if (nInBoxCount == 0) {
			// Triangle not in any boxes. Roundoff error, or triangle outside of overall bounding box.
			if (IntersectTriangleBox(face.m_vertices[0], face.m_vertices[1], face.m_vertices[2],
					Vector3f(this->m_baseBox.minX, this->m_baseBox.minY, this->m_baseBox.minZ) - vTolerance,
					Vector3f(this->m_baseBox.maxX, this->m_baseBox.maxY, this->m_baseBox.maxZ) + vTolerance))
				iErrorType |= ErrorType_TriangleMissingFromRoundOffError; // Roundoff error.
			else
				iErrorType |= ErrorType_TriangleNotInBoundingBox; // Triangle not within bounding box.
		}
	}

	return iErrorType;
}

void CCollisionObj::RebuildBoxes() {
	size_t nOriginalFaceCount = m_faceDataBank.size();
	size_t nOriginalFacesInLists = 0;
	for (auto& mapEntry : m_faceIndexArrayCache)
		nOriginalFacesInLists += mapEntry.second.size();

	// Remove degenerate triangles.
	auto IsDegenerate = [](const CFaceStruct& face) -> bool {
		return face.m_vertices[0] == face.m_vertices[1] || face.m_vertices[0] == face.m_vertices[2] || face.m_vertices[2] == face.m_vertices[1];
	};
	auto itr_removed = std::remove_if(m_faceDataBank.begin(), m_faceDataBank.end(), IsDegenerate);
	m_faceDataBank.erase(itr_removed, m_faceDataBank.end());

	// Sort points in triangles so we can compare faces.
	for (CFaceStruct& face : m_faceDataBank)
		std::sort(face.m_vertices, face.m_vertices + 3, LexicographicallyLess<Vector3f>());

	// Sort faces
	auto CompareFacesLess = [](const CFaceStruct& left, const CFaceStruct& right) -> bool {
		for (size_t i = 0; i < 3; ++i) {
			for (size_t j = 0; j < 3; ++j) {
				float L = left.m_vertices[i][j];
				float R = right.m_vertices[i][j];
				if (L != R)
					return L < R;
			}
		}
		return false;
	};
	std::sort(m_faceDataBank.begin(), m_faceDataBank.end(), CompareFacesLess);

	// Remove duplicate faces
	auto itr_first_unique = m_faceDataBank.begin();
	auto itr_write = itr_first_unique;
	while (itr_first_unique != m_faceDataBank.end()) {
		*itr_write++ = *itr_first_unique;
		auto itr_end_unique = std::next(itr_first_unique);
		while (itr_end_unique != m_faceDataBank.end() && !CompareFacesLess(*itr_first_unique, *itr_end_unique))
			++itr_end_unique;
		itr_first_unique = itr_end_unique;
	}
	m_faceDataBank.erase(itr_write, m_faceDataBank.end());
	//auto itr_uniqued = std::unique(m_faceDataBank.begin(), m_faceDataBank.end(), CompareFacesEqual);
	//m_faceDataBank.erase(itr_uniqued, m_faceDataBank.end());

	// Initialize boxes to have face index key of zero.
	auto InitBox = [](const BoxVisitCallbackArg& arg) -> bool { arg.m_pBox->m_faceIndexCacheKey = 0; return true; };
	VisitAllBoxes(InitBox);

	// Construct new face lists.
	std::map<REFBOX*, std::vector<int>> mapNewFaceLists;
	CFaceStruct* pCurrentFace;
	size_t iCurrentFace;
	Vector3f vMaxBoxCoord(
		std::max<float>(std::abs(m_baseBox.minX), std::abs(this->m_baseBox.maxX)),
		std::max<float>(std::abs(m_baseBox.minY), std::abs(this->m_baseBox.maxY)),
		std::max<float>(std::abs(m_baseBox.minZ), std::abs(this->m_baseBox.maxZ)));
	Vector3f vTolerance = vMaxBoxCoord * (FLT_EPSILON * 8);
	auto ProcessBox = [&mapNewFaceLists, &pCurrentFace, &iCurrentFace, &vTolerance](const BoxVisitCallbackArg& arg) -> bool {
		if (!arg.m_pBox->m_subBoxCollection) {
			// Only process leaf boxes.
			bool bIntersect = IntersectTriangleBox(pCurrentFace->m_vertices[0], pCurrentFace->m_vertices[1], pCurrentFace->m_vertices[2], arg.m_ptMin - vTolerance, arg.m_ptMax + vTolerance);
			if (bIntersect)
				mapNewFaceLists[arg.m_pBox].push_back(iCurrentFace);
		}

		return true;
	};

	for (size_t iFace = 0; iFace < m_faceDataBank.size(); ++iFace) {
		CFaceStruct& face = m_faceDataBank[iFace];
		iCurrentFace = iFace;
		pCurrentFace = &face;

		Vector3f ptMin, ptMax;
		CalculateBoundingBox(&face.m_vertices[0], &face.m_vertices[0] + 3, out(ptMin), out(ptMax));
		ptMin -= vTolerance;
		ptMax += vTolerance;
		VisitBoxesInRange(ptMin, ptMax, ProcessBox);
	}

	// Create new face list IDs.
	struct CompareFaceList {
		bool operator()(const std::vector<int>& left, const std::vector<int>& right) const {
			size_t minSize = std::min<size_t>(left.size(), right.size());
			for (size_t i = 0; i < minSize; ++i) {
				if (left[i] != right[i])
					return left[i] < right[i];
			}
			if (left.size() < right.size())
				return true;
			return false;
		}
	};
	std::map<std::vector<int>, size_t, CompareFaceList> mapFaceListToKey;
	mapFaceListToKey[std::vector<int>()] = 0; // Initialize key value zero to empty vector.
	for (const auto& mapEntry : mapNewFaceLists) {
		auto itr = mapFaceListToKey.lower_bound(mapEntry.second);
		if (itr == mapFaceListToKey.end() || CompareFaceList()(mapEntry.second, itr->first)) {
			itr = mapFaceListToKey.insert(itr, std::make_pair(mapEntry.second, mapFaceListToKey.size()));
		}
		mapEntry.first->m_faceIndexCacheKey = itr->second;
	}

	// Update m_faceIndexArrayCache
	m_faceIndexArrayCache.clear();
	for (auto& mapEntry : mapFaceListToKey) {
		m_faceIndexArrayCache[mapEntry.second] = std::move(mapEntry.first);
	}
	m_nextFaceIndexCacheKey = mapFaceListToKey.size();

	size_t nNewFaceCount = m_faceDataBank.size();
	size_t nNewFacesInLists = 0;
	for (auto& mapEntry : m_faceIndexArrayCache)
		nNewFacesInLists += mapEntry.second.size();
	LogInfo("Original faces: " << nOriginalFaceCount << " Original cumulative face list size: " << nOriginalFacesInLists
							   << "  New faces: " << nNewFaceCount << " New cumulative face list size: " << nNewFacesInLists);
}

void CCollisionObj::DumpTrianglesToLog() const {
	for (const CFaceStruct& face : m_faceDataBank) {
		Vector3f apts[3] = { face.m_vertices[0], face.m_vertices[1], face.m_vertices[2] };
		std::sort(apts, apts + 3, [](const Vector3f& left, const Vector3f& right) -> bool {
			for (int i = 0; i < 3; ++i) {
				if (left[i] != right[i])
					return left[i] < right[i];
			}
			return false;
		});
		std::ostringstream strm;
		if (apts[0] == apts[1] || apts[2] == apts[1] || apts[0] == apts[2])
			strm << "Degenerate triangle: ";
		for (size_t j = 0; j < 3; ++j) {
			const Vector3f& pt = apts[j];
			strm << "(" << pt[0] << "," << pt[1] << "," << pt[2] << ") ";
		}
		LogInfo(strm.str());
	}
}

IMPLEMENT_SERIAL_SCHEMA(CCollisionObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CCollisionObjList, CKEPObList)

} // namespace KEP
