///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include <TinyXML/tinyxml.h>
#include <SerializeHelper.h>
#include <GetSetMFCSerialize.h>
#include "CPlayerClass.h"
#include <PassList.h>
#include "KEPConstants.h"
#include "common/KEPUtil/LocationRegistry.h"
#include "UnicodeMFCHelpers.h"
#include "CPacketQueClass.h"
#include "CPacketObjListMsgEquip.h"
#include "CAccountClass.h"
#include "CInventoryClass.h"
#include "CNoterietyClass.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "CCurrencyClass.h"
#include "CQuestJournal.h"
#include "FriendList.h"
#include "CTitleObject.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

CPlayerObject::CPlayerObject() :
		m_currentChannel(INVALID_CHANNEL_ID),
		m_currentZoneIndex(INVALID_ZONE_INDEX),
		m_parentZoneInstanceId(0),
		m_preArenaInventory(0),
		m_housingZoneIndex(INVALID_ZONE_INDEX),
		m_reconnecting(false),
		m_dbDirtyFlags(0),
		m_currentZonePriv(ZP_NONE),
		m_previousZoneIndex(INVALID_ZONE_INDEX),
		m_passList(0),
		m_packetSizeViolationHardLimitCount(0),
		m_packetSizeViolationSoftLimitCount(0),
		m_timeDbUpdate(TIME_UNDEFINED),
		m_pMsgOutboxSpawnStaticMO(nullptr),
		m_pMsgOutboxGeneral(nullptr),
		m_pMsgOutboxAttrib(nullptr),
		m_pMsgOutboxEquip(nullptr),
		m_pMsgOutboxEvent(nullptr),

		m_netId(0),
		m_animationState(0),
		m_currentWorldMap("Unknown"),
		m_inventory(nullptr),
		m_scratchInventory(nullptr),
		m_bankInventory(nullptr),
		m_dbCfg(-1), // -1 = none
		m_animGLID(GLID_INVALID),
		m_animGLID2nd(GLID_INVALID),
		m_inGame(FALSE),
		m_netTraceId(-1), // -1 = none
		m_justSpawned(TRUE),
		m_mountedOnPlayerID(-1), // -1 = not riding anything
		m_mountedByPlayerID(-1), // -1 = not mounted
		m_accountNumberRef(-1),
		m_bornSpawnCfgIndex(0),
		m_aiControlled(FALSE),
		m_charName("Unknown"),
		m_displayName("Unknown"),
		m_baseMeshes(0),
		m_skills(nullptr),
		m_cash(nullptr),
		m_giftCredits(nullptr),
		m_dynamicRadius(0.0f),
		m_bankCash(nullptr),
		m_stats(nullptr),
		m_respawnPosition(0.0f, 0.0f, 0.0f),
		m_handle(0),
		m_friendList(nullptr),
		m_playerOrigRef(nullptr),
		m_positionLock(0.0f, 0.0f, 0.0f),
		m_altCfgIndex(0),
		m_curEDBIndex(0),
		m_originalEDBCfg(0),
		m_reconfigInven(nullptr),
		m_skillUseTimeStamp(0),
		m_aiCfgOfMounted(-1),
		m_admin(FALSE),
		m_pAccountObject(nullptr),
		m_noteriety(nullptr),
		m_clanHandle(NO_CLAN),
		m_currentRequestToJoinClanHandle(NO_CLAN),
		m_skillDBisReferenced(FALSE),
		m_gm(FALSE),
		m_spawnAbility(FALSE),
		m_questJournal(nullptr),
		m_scaleFactor(1.0f, 1.0f, 1.0f),
		m_scratchScaleFactor(1.0f, 1.0f, 1.0f),
		m_titleList(nullptr),
		m_interactionStates(AVAILABLE),
		m_nameColor(255, 255, 255) {
	m_respawnZoneIndex.clear();
}

void CPlayerObject::Clone(CPlayerObject** ppPO_out) {
	auto pPO_new = new CPlayerObject();

	pPO_new->m_movementInterpolator = m_movementInterpolator;

	pPO_new->m_scaleFactor = m_scaleFactor;
	pPO_new->m_scratchScaleFactor = m_scratchScaleFactor;

	pPO_new->m_respawnPosition = m_respawnPosition;
	pPO_new->m_animationState = m_animationState;
	pPO_new->m_currentWorldMap = m_currentWorldMap;
	pPO_new->m_dbCfg = m_dbCfg;
	pPO_new->m_animGLID = m_animGLID;
	pPO_new->m_inGame = m_inGame;
	pPO_new->m_currentChannel = m_currentChannel;
	pPO_new->m_currentZoneIndex = m_currentZoneIndex;
	pPO_new->m_parentZoneInstanceId = m_parentZoneInstanceId;
	pPO_new->m_currentZonePriv = m_currentZonePriv;
	pPO_new->m_previousZoneIndex = m_previousZoneIndex;
	pPO_new->m_netTraceId = m_netTraceId;
	pPO_new->m_justSpawned = m_justSpawned;
	pPO_new->m_handle = m_handle;
	pPO_new->m_mountedByPlayerID = m_mountedByPlayerID;
	pPO_new->m_bornSpawnCfgIndex = m_bornSpawnCfgIndex;
	pPO_new->m_charName = m_charName;
	pPO_new->m_displayName = m_displayName;
	pPO_new->m_lastName = m_lastName;
	pPO_new->m_clanName = m_clanName;
	pPO_new->m_title = m_title;
	pPO_new->m_displayTitle = m_displayTitle;
	pPO_new->m_baseMeshes = m_baseMeshes;
	pPO_new->m_gm = m_gm;
	pPO_new->m_respawnZoneIndex = m_respawnZoneIndex;
	pPO_new->m_altCfgIndex = m_altCfgIndex;
	pPO_new->m_curEDBIndex = m_curEDBIndex;
	pPO_new->m_originalEDBCfg = m_originalEDBCfg;
	pPO_new->m_clanHandle = m_clanHandle;
	pPO_new->m_spawnAbility = m_spawnAbility;

	if (m_cash)
		m_cash->Clone(&pPO_new->m_cash);

	if (m_giftCredits)
		m_giftCredits->Clone(&pPO_new->m_giftCredits);

	if (m_bankCash)
		m_bankCash->Clone(&pPO_new->m_bankCash);

	if (m_reconfigInven)
		m_reconfigInven->Clone(&pPO_new->m_reconfigInven);

	if (m_inventory)
		m_inventory->Clone(&pPO_new->m_inventory);

	if (m_scratchInventory)
		m_scratchInventory->Clone(&pPO_new->m_scratchInventory);

	if (m_preArenaInventory)
		m_preArenaInventory->Clone(&pPO_new->m_preArenaInventory);

	if (m_bankInventory)
		m_bankInventory->Clone(&pPO_new->m_bankInventory);

	if (m_stats)
		m_stats->Clone(&pPO_new->m_stats);

	if (m_noteriety)
		m_noteriety->Clone(&pPO_new->m_noteriety);

	if (m_skills)
		m_skills->Clone(&pPO_new->m_skills);

	if (m_friendList)
		m_friendList->Clone(&pPO_new->m_friendList);

	if (m_questJournal)
		m_questJournal->Clone(&pPO_new->m_questJournal);

	if (m_baseMeshes > 0)
		pPO_new->m_charConfig = m_charConfig;

	pPO_new->m_currentProtocolVersion = m_currentProtocolVersion;

	*ppPO_out = pPO_new;
}

void CPlayerObject::SafeDelete() {
	if (m_passList)
		DELETE_AND_ZERO(m_passList);

	if (m_titleList) {
		delete m_titleList;
		m_titleList = nullptr;
	}

	if (m_inventory) {
		m_inventory->SafeDelete();
		delete m_inventory;
		m_inventory = nullptr;
	}

	if (m_scratchInventory) {
		m_scratchInventory->SafeDelete();
		delete m_scratchInventory;
		m_scratchInventory = nullptr;
	}

	if (m_preArenaInventory) {
		m_preArenaInventory->SafeDelete();
		delete m_preArenaInventory;
		m_preArenaInventory = nullptr;
	}

	m_charConfig.disposeAllItems();

	if (!m_skillDBisReferenced && m_skills)
		delete m_skills;
	m_skills = nullptr;

	if (m_cash) {
		delete m_cash;
		m_cash = nullptr;
	}

	if (m_giftCredits) {
		delete m_giftCredits;
		m_giftCredits = nullptr;
	}

	if (m_bankCash) {
		delete m_bankCash;
		m_bankCash = nullptr;
	}

	if (m_bankInventory) {
		m_bankInventory->SafeDelete();
		delete m_bankInventory;
		m_bankInventory = nullptr;
	}

	if (m_stats) {
		delete m_stats;
		m_stats = nullptr;
	}

	if (m_friendList) {
		m_friendList->SafeDelete();
		delete m_friendList;
		m_friendList = nullptr;
	}

	if (m_reconfigInven) {
		m_reconfigInven->SafeDelete();
		delete m_reconfigInven;
		m_reconfigInven = nullptr;
	}

	if (m_pMsgOutboxSpawnStaticMO) {
		m_pMsgOutboxSpawnStaticMO->SafeDelete();
		delete m_pMsgOutboxSpawnStaticMO;
		m_pMsgOutboxSpawnStaticMO = nullptr;
	}

	if (m_pMsgOutboxAttrib) {
		m_pMsgOutboxAttrib->SafeDelete();
		delete m_pMsgOutboxAttrib;
		m_pMsgOutboxAttrib = nullptr;
	}

	if (m_pMsgOutboxEquip) {
		m_pMsgOutboxEquip->SafeDelete();
		delete m_pMsgOutboxEquip;
		m_pMsgOutboxEquip = nullptr;
	}

	if (m_pMsgOutboxGeneral) {
		m_pMsgOutboxGeneral->SafeDelete();
		delete m_pMsgOutboxGeneral;
		m_pMsgOutboxGeneral = nullptr;
	}

	if (m_pMsgOutboxEvent) {
		m_pMsgOutboxEvent->SafeDelete();
		delete m_pMsgOutboxEvent;
		m_pMsgOutboxEvent = nullptr;
	}

	if (m_noteriety) {
		m_noteriety->SafeDelete();
		delete m_noteriety;
		m_noteriety = nullptr;
	}

	if (m_questJournal) {
		m_questJournal->SafeDelete();
		delete m_questJournal;
		m_questJournal = nullptr;
	}
}

void CPlayerObject::SerializeAlloc(CArchive& ar) {
	ASSERT(m_currentChannel == INVALID_CHANNEL_ID); // only Account players get serialized, not runtime

	int version = 7;

	if (ar.IsStoring()) {
		ar
			<< (float)0.0 //m_upVector.x
			<< (float)0.0 //m_upVector.y
			<< (float)0.0 //m_upVector.z
			<< (float)0.0 //m_dirVector.x
			<< (float)0.0 //m_dirVector.y
			<< (float)0.0 //m_dirVector.z
			<< (float)0.0 //m_3dPosition.x
			<< (float)0.0 //m_3dPosition.y
			<< (float)0.0 //m_3dPosition.z
			<< (float)0.0 // m_freeLookYaw
			<< (float)0.0 // m_freeLookPitch
			<< (float)0.0 //m_LastUpVector.x
			<< (float)0.0 //m_LastUpVector.y
			<< (float)0.0 //m_LastUpVector.z
			<< (float)0.0 //m_LastDirVector.x
			<< (float)0.0 //m_LastDirVector.y
			<< (float)0.0 //m_LastDirVector.z
			<< (float)0.0 //m_Last3dPosition.x
			<< (float)0.0 //m_Last3dPosition.y
			<< (float)0.0 //m_Last3dPosition.z
			<< (float)0.0 //m_TargetUpVector.x
			<< (float)0.0 //m_TargetUpVector.y
			<< (float)0.0 //m_TargetUpVector.z
			<< (float)0.0 //m_TargetDirVector.x
			<< (float)0.0 //m_TargetDirVector.y
			<< (float)0.0 //m_TargetDirVector.z
			<< (float)0.0 //m_Target3dPosition.x
			<< (float)0.0 //m_Target3dPosition.y
			<< (float)0.0 //m_Target3dPosition.z
			<< m_animationState
			<< m_currentWorldMap
			<< version;

		if (!m_inventory)
			ar << -1;
		else {
			ar << (int)m_inventory->GetCount();
			m_inventory->SerializeAlloc(ar, 0);
		}

		ar << m_dbCfg
		   << m_animGLID
		   << m_inGame
		   << m_currentZoneIndex
		   << (int)0 //m_energy
		   << (int)0 //m_maxEnergy
		   << m_netTraceId
		   << (int)0 //m_timeDied
		   << m_justSpawned
		   << (int)0 //m_timerMsgRx
		   << m_handle
		   << m_mountedByPlayerID
		   << m_bornSpawnCfgIndex
		   << m_charName
		   << m_lastName
		   << m_clanName
		   << m_title
		   << m_baseMeshes;

		if (!m_skills)
			ar << -1;
		else {
			ar << (int)m_skills->GetCount();
			m_skills->SerializeAlloc(ar, 0);
		}

		if (!m_cash)
			ar << -1;
		else {
			ar << 1;
			m_cash->SerializeAlloc(ar);
		}

		if (!m_bankCash)
			ar << -1;
		else {
			ar << 1;
			m_bankCash->SerializeAlloc(ar);
		}

		if (!m_bankInventory)
			ar << -1;
		else {
			ar << (int)m_bankInventory->GetCount();
			m_bankInventory->SerializeAlloc(ar, 0);
		}

		if (!m_stats)
			ar << -1;
		else {
			ar << (int)m_stats->GetCount();
			m_stats->SerializeAlloc(ar, 0);
		}

		ar << m_respawnPosition.x
		   << m_respawnPosition.y
		   << m_respawnPosition.z
		   << m_respawnZoneIndex;

		if (!m_friendList)
			ar << -1;
		else {
			ar << (int)m_friendList->GetCount();
			m_friendList->SerializeAlloc(ar, 0);
		}

		ar << (int)0 //m_arenaTotalKills
		   << (int)0 //m_arenaBasePoints
		   << (int)0 //m_stasis
		   << m_altCfgIndex
		   << m_curEDBIndex
		   << m_originalEDBCfg;

		if (!m_reconfigInven)
			ar << -1;
		else {
			ar << (int)m_reconfigInven->GetCount();
			m_reconfigInven->SerializeAlloc(ar, 0);
		}

		ar << (int)0 //m_countDownToMurderPardon
		   << m_clanHandle;

		if (!m_noteriety)
			ar << -1;
		else {
			ar << 1;
			m_noteriety->SerializeAlloc(ar);
		}

		for (int slot = 0; slot < m_baseMeshes; slot++) {
			ar << m_charConfig.getItemSlotMeshId(GOB_BASE_ITEM, slot);
		}

		ar << m_questJournal;
		ar << (int)0; //m_currentBounty;
		ar << (int)0; //m_origTeamID;  // version 4
		ar << m_preArenaInventory; // version 5
		ar << (int)0; //m_preArenaTeamID; // version 6
		SerializeGetSetValues(this, ar); // version 7
		ar << m_housingZoneIndex; // version 9
		ar << m_scaleFactor.x; // version 10
		ar << m_scaleFactor.y;
		ar << m_scaleFactor.z;

		for (int slot = 0; slot < m_baseMeshes; slot++) {
			ar << m_charConfig.getItemSlotMaterialId(GOB_BASE_ITEM, slot);
			float r = -1.0f, g = -1.0f, b = -1.0f;
			m_charConfig.getItemSlotColor(GOB_BASE_ITEM, slot, r, g, b);
			ar << r << g << b;
		}

	} else {
		int tempLoadCount = 0;
		float deprecatedFloat;
		ar >> deprecatedFloat //m_upVector.x
			>> deprecatedFloat //m_upVector.y
			>> deprecatedFloat //m_upVector.z
			>> deprecatedFloat //m_dirVector.x
			>> deprecatedFloat //m_dirVector.y
			>> deprecatedFloat //m_dirVector.z
			>> deprecatedFloat //m_3dPosition.x
			>> deprecatedFloat //m_3dPosition.y
			>> deprecatedFloat //m_3dPosition.z
			>> deprecatedFloat //m_freeLookYaw
			>> deprecatedFloat //m_freeLookPitch
			>> deprecatedFloat //m_LastUpVector.x
			>> deprecatedFloat //m_LastUpVector.y
			>> deprecatedFloat //m_LastUpVector.z
			>> deprecatedFloat //m_LastDirVector.x
			>> deprecatedFloat //m_LastDirVector.y
			>> deprecatedFloat //m_LastDirVector.z
			>> deprecatedFloat //m_Last3dPosition.x
			>> deprecatedFloat //m_Last3dPosition.y
			>> deprecatedFloat //m_Last3dPosition.z
			>> deprecatedFloat //m_TargetUpVector.x
			>> deprecatedFloat //m_TargetUpVector.y
			>> deprecatedFloat //m_TargetUpVector.z
			>> deprecatedFloat //m_TargetDirVector.x
			>> deprecatedFloat //m_TargetDirVector.y
			>> deprecatedFloat //m_TargetDirVector.z
			>> deprecatedFloat //m_Target3dPosition.x
			>> deprecatedFloat //m_Target3dPosition.y
			>> deprecatedFloat //m_Target3dPosition.z
			>> m_animationState >> m_currentWorldMap >> version;

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_inventory = new CInventoryObjList();
			m_inventory->SerializeAlloc(ar, tempLoadCount);
		}

		int deprecated;
		ar >> m_dbCfg >> m_animGLID >> m_inGame >> m_currentZoneIndex >> deprecated //m_energy
			>> deprecated //m_maxEnergy
			>> m_netTraceId >> deprecated //m_timeDied
			>> m_justSpawned >> deprecated //tlu
			>> m_handle >> m_mountedByPlayerID >> m_bornSpawnCfgIndex >> m_charName;
		m_displayName = m_charName;

		if (version > 6) {
			ar >> m_lastName >> m_clanName >> m_title;
			m_displayTitle = m_title;
		}

		ar >> m_baseMeshes;

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_skills = new CSkillObjectList();
			m_skills->SerializeAlloc(ar, tempLoadCount);
		}

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_cash = new CCurrencyObj();
			m_cash->SerializeAlloc(ar);
			m_cash->m_currencyName.LoadString(IDS_PLAYEROBJECT_CASH);
		}

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_bankCash = new CCurrencyObj();
			m_bankCash->SerializeAlloc(ar);
			m_bankCash->m_currencyName.LoadString(IDS_PLAYEROBJECT_BANKCASH);
		}

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_bankInventory = new CInventoryObjList();
			m_bankInventory->SerializeAlloc(ar, tempLoadCount);
		}

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_stats = new CStatObjList();
			m_stats->SerializeAlloc(ar, tempLoadCount);
		}

		ar >> m_respawnPosition.x >> m_respawnPosition.y >> m_respawnPosition.z >> m_respawnZoneIndex;

		ar >> tempLoadCount;
		{
			m_friendList = new FriendList();
			m_friendList->SerializeAlloc(ar, tempLoadCount);
		}

		ar >> deprecated //m_arenaTotalKills
			>> deprecated //m_arenaBasePoints
			>> deprecated //m_stasis
			>> m_altCfgIndex >> m_curEDBIndex >> m_originalEDBCfg;

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_reconfigInven = new CInventoryObjList();
			m_reconfigInven->SerializeAlloc(ar, tempLoadCount);
		}

		ar >> deprecated //m_countDownToMurderPardon
			>> m_clanHandle;

		ar >> tempLoadCount;
		if (tempLoadCount > -1) {
			m_noteriety = new CNoterietyObj();
			m_noteriety->SerializeAlloc(ar);
		}

		if (m_baseMeshes > 0) {
			m_charConfig.initBaseItem(m_baseMeshes);
		}

		for (int baseSlot = 0; baseSlot < m_baseMeshes; baseSlot++) {
			int meshId;
			ar >> meshId;
			m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, baseSlot, meshId);
		}

		m_questJournal = NULL;
		if (version > 1)
			ar >> m_questJournal;

		if (version > 2)
			ar >> deprecated; //m_currentBounty;

		if (version > 3)
			ar >> deprecated; //m_teamID;

		if (version > 4)
			ar >> m_preArenaInventory;

		if (version > 5)
			ar >> deprecated; //m_preArenaTeamID;

		if (version > 7)
			SerializeGetSetValues(this, ar);

		if (version > 8)
			ar >> m_housingZoneIndex;

		if (version > 9) {
			ar >> m_scaleFactor.x >> m_scaleFactor.y >> m_scaleFactor.z;
			m_scratchScaleFactor = m_scaleFactor;
		}

		if (version > 10) {
			for (int loop = 0; loop < m_baseMeshes; loop++) {
				int tempMat;
				float tempr, tempg, tempb;
				ar >> tempMat;
				ar >> tempr;
				ar >> tempg;
				ar >> tempb;
				m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, loop, tempMat);
				m_charConfig.setItemSlotColor(GOB_BASE_ITEM, loop, tempr, tempg, tempb);
			}
		}

		m_currentRequestToJoinClanHandle = NO_CLAN;

		m_admin = FALSE;
		m_gm = FALSE;
		m_spawnAbility = FALSE;
		m_aiCfgOfMounted = -1;

		m_skillUseTimeStamp = 0;
		m_mountedOnPlayerID = -1;
		m_netId = 0;
		m_accountNumberRef = -1;
		m_aiControlled = FALSE;
		m_dynamicRadius = 0.0f;
		m_inGame = FALSE;
		m_animGLID = GLID_INVALID;
		m_animGLID2nd = GLID_INVALID;

		m_timerMsgRx.Reset();

		m_positionLock.x = 0.0f;
		m_positionLock.y = 0.0f;
		m_positionLock.z = 0.0f;

		m_playerOrigRef = nullptr;
		m_pAccountObject = nullptr;

		m_pMsgOutboxSpawnStaticMO = nullptr;
		m_pMsgOutboxGeneral = nullptr;
		m_pMsgOutboxAttrib = nullptr;
		m_pMsgOutboxEquip = nullptr;
		m_pMsgOutboxEvent = nullptr;

		m_skillDBisReferenced = FALSE;
		m_updateAcctTimer.Reset();
	}
}

void CPlayerObject::Serialize(CArchive& ar) {
	int deprecated;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(7);
		ASSERT(ar.GetObjectSchema() == 7);

		ar
			<< (float)0.0 //m_upVector.x
			<< (float)0.0 //m_upVector.y
			<< (float)0.0 //m_upVector.z
			<< (float)0.0 //m_dirVector.x
			<< (float)0.0 //m_dirVector.y
			<< (float)0.0 //m_dirVector.z
			<< (float)0.0 //m_3dPosition.x
			<< (float)0.0 //m_3dPosition.y
			<< (float)0.0 //m_3dPosition.z
			<< (float)0.0 // m_freeLookYaw
			<< (float)0.0 // m_freeLookPitch
			<< (float)0.0 //m_LastUpVector.x
			<< (float)0.0 //m_LastUpVector.y
			<< (float)0.0 //m_LastUpVector.z
			<< (float)0.0 //m_LastDirVector.x
			<< (float)0.0 //m_LastDirVector.y
			<< (float)0.0 //m_LastDirVector.z
			<< (float)0.0 //m_Last3dPosition.x
			<< (float)0.0 //m_Last3dPosition.y
			<< (float)0.0 //m_Last3dPosition.z
			<< (float)0.0 //m_TargetUpVector.x
			<< (float)0.0 //m_TargetUpVector.y
			<< (float)0.0 //m_TargetUpVector.z
			<< (float)0.0 //m_TargetDirVector.x
			<< (float)0.0 //m_TargetDirVector.y
			<< (float)0.0 //m_TargetDirVector.z
			<< (float)0.0 //m_Target3dPosition.x
			<< (float)0.0 //m_Target3dPosition.y
			<< (float)0.0 //m_Target3dPosition.z
			;

		ar << m_animationState;
		ar << m_currentWorldMap;
		ar << m_inventory;
		ar << m_dbCfg;
		ar << m_animGLID;
		ar << m_inGame;
		ar << m_currentZoneIndex;
		ar << (int)0; //m_energy;
		ar << (int)0; //m_maxEnergy ;
		ar << m_netTraceId;
		ar << (int)0; //m_timeDied ;
		ar << m_justSpawned;
		ar << (int)0; //m_timerMsgRx  ;
		ar << m_handle;
		ar << m_mountedByPlayerID;
		ar << m_bornSpawnCfgIndex;
		ar << m_charName;
		ar << m_lastName;
		ar << m_clanName;
		ar << m_title;
		ar << m_baseMeshes;
		ar << m_skills;
		ar << m_cash;
		ar << m_bankCash;
		ar << m_bankInventory;
		ar << m_stats;
		ar << m_respawnPosition.x;
		ar << m_respawnPosition.y;
		ar << m_respawnPosition.z;
		ar << m_respawnZoneIndex;
		ar << m_friendList;
		ar << (int)0; //m_arenaTotalKills;
		ar << (int)0; //m_arenaBasePoints;
		ar << (int)0; //m_stasis;
		ar << m_altCfgIndex;
		ar << m_curEDBIndex;
		ar << m_originalEDBCfg;
		ar << m_reconfigInven;
		ar << (int)0; //m_countDownToMurderPardon;
		ar << m_clanHandle; // version 11 changed from USHORT to PLAYER_HANDLE (int)
		ar << m_noteriety;
		;

		for (int slot = 0; slot < m_baseMeshes; slot++)
			ar << m_charConfig.getItemSlotMeshId(GOB_BASE_ITEM, slot);

		ar << m_questJournal;

		ar << (int)0; //m_currentBounty;
		ar << (int)0; //m_origTeamID;  //version 4
		ar << m_preArenaInventory; // version 5
		ar << (int)0; //m_preArenaTeamID; // version 6
		SerializeGetSetValues(this, ar); // version 7
		ar << m_housingZoneIndex; // version 9
		ar << m_scaleFactor.x; //version 10
		ar << m_scaleFactor.y;
		ar << m_scaleFactor.z;
		for (int slot = 0; slot < m_baseMeshes; slot++) {
			ar << m_charConfig.getItemSlotMaterialId(GOB_BASE_ITEM, slot);
			float r = -1.0f, g = -1.0f, b = -1.0f;
			m_charConfig.getItemSlotColor(GOB_BASE_ITEM, slot, r, g, b);
			ar << r << g << b;
		}

	} else {
		int version = ar.GetObjectSchema();
		float deprecatedFloat;

		ar >> deprecatedFloat //m_upVector.x
			>> deprecatedFloat //m_upVector.y
			>> deprecatedFloat //m_upVector.z
			>> deprecatedFloat //m_dirVector.x
			>> deprecatedFloat //m_dirVector.y
			>> deprecatedFloat //m_dirVector.z
			>> deprecatedFloat //m_3dPosition.x
			>> deprecatedFloat //m_3dPosition.y
			>> deprecatedFloat //m_3dPosition.z
			>> deprecatedFloat //m_freeLookYaw
			>> deprecatedFloat //m_freeLookPitch
			>> deprecatedFloat //m_LastUpVector.x
			>> deprecatedFloat //m_LastUpVector.y
			>> deprecatedFloat //m_LastUpVector.z
			>> deprecatedFloat //m_LastDirVector.x
			>> deprecatedFloat //m_LastDirVector.y
			>> deprecatedFloat //m_LastDirVector.z
			>> deprecatedFloat //m_Last3dPosition.x
			>> deprecatedFloat //m_Last3dPosition.y
			>> deprecatedFloat //m_Last3dPosition.z
			>> deprecatedFloat //m_TargetUpVector.x
			>> deprecatedFloat //m_TargetUpVector.y
			>> deprecatedFloat //m_TargetUpVector.z
			>> deprecatedFloat //m_TargetDirVector.x
			>> deprecatedFloat //m_TargetDirVector.y
			>> deprecatedFloat //m_TargetDirVector.z
			>> deprecatedFloat //m_Target3dPosition.x
			>> deprecatedFloat //m_Target3dPosition.y
			>> deprecatedFloat //m_Target3dPosition.z
			;

		ar >> m_animationState;
		ar >> m_currentWorldMap;
		ar >> m_inventory;
		ar >> m_dbCfg;
		ar >> m_animGLID;
		ar >> m_inGame;
		ar >> m_currentZoneIndex;
		ar >> deprecated; //m_energy;
		ar >> deprecated; //m_maxEnergy ;
		ar >> m_netTraceId;
		ar >> deprecated; // m_timeDied;
		ar >> m_justSpawned;
		ar >> deprecated; //m_timerMsgRx  ;
		ar >> m_handle;
		ar >> m_mountedByPlayerID;
		ar >> m_bornSpawnCfgIndex;
		ar >> m_charName;
		m_displayName = m_charName;
		if (version > 6) {
			ar >> m_lastName;
			ar >> m_clanName;
			ar >> m_title;
			m_displayTitle = m_title;
		}

		ar >> m_baseMeshes;
		ar >> m_skills;
		ar >> m_cash;
		ar >> m_bankCash;
		ar >> m_bankInventory;
		ar >> m_stats;
		ar >> m_respawnPosition.x;
		ar >> m_respawnPosition.y;
		ar >> m_respawnPosition.z;
		ar >> m_respawnZoneIndex;
		ar >> m_friendList;
		ar >> deprecated; //m_arenaTotalKills;
		ar >> deprecated; //m_arenaBasePoints;
		ar >> deprecated; //m_stasis;
		ar >> m_altCfgIndex;
		ar >> m_curEDBIndex;
		ar >> m_originalEDBCfg;
		ar >> m_reconfigInven;
		ar >> deprecated; //m_countDownToMurderPardon;
		ar >> m_clanHandle;
		ar >> m_noteriety;

		if (m_cash->m_currencyName == "CREDITS") { // is it the hard-coded value?
			m_cash->m_currencyName.LoadString(IDS_PLAYEROBJECT_CASH);
			m_bankCash->m_currencyName.LoadString(IDS_PLAYEROBJECT_BANKCASH);
		}

		if (m_baseMeshes > 0) {
			m_charConfig.initBaseItem(m_baseMeshes);
		}

		for (int baseSlot = 0; baseSlot < m_baseMeshes; baseSlot++) {
			int temp;
			ar >> temp;
			m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, baseSlot, temp);
		}

		m_questJournal = NULL;
		if (version > 1)
			ar >> m_questJournal;

		if (version > 2)
			ar >> deprecated; //m_currentBounty;

		if (version > 3)
			ar >> deprecated; //m_teamID;  //version 4

		if (version > 4)
			ar >> m_preArenaInventory;

		if (version > 5)
			ar >> deprecated; //m_preArenaTeamID;

		if (version > 7)
			SerializeGetSetValues(this, ar);

		if (version > 8)
			ar >> m_housingZoneIndex;

		if (version > 9) {
			ar >> m_scaleFactor.x >> m_scaleFactor.y >> m_scaleFactor.z;
			m_scratchScaleFactor = m_scaleFactor;
		}

		if (version > 10) {
			for (int loop = 0; loop < m_baseMeshes; loop++) {
				int tempMat;
				float tempr, tempg, tempb;
				ar >> tempMat;
				ar >> tempr;
				ar >> tempg;
				ar >> tempb;
				m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, loop, tempMat);
				m_charConfig.setItemSlotColor(GOB_BASE_ITEM, loop, tempr, tempg, tempb);
			}
		}

		m_currentRequestToJoinClanHandle = NO_CLAN;

		m_admin = FALSE;
		m_gm = FALSE;
		m_spawnAbility = FALSE;
		m_aiCfgOfMounted = -1;

		m_skillUseTimeStamp = 0;
		m_mountedOnPlayerID = -1;
		m_netId = 0;
		m_accountNumberRef = -1;
		m_aiControlled = FALSE;
		m_dynamicRadius = 0.0f;
		m_inGame = FALSE;
		m_animGLID = GLID_INVALID;
		m_animGLID2nd = GLID_INVALID;

		m_playerOrigRef = NULL;
		m_positionLock.x = 0.0f;
		m_positionLock.y = 0.0f;
		m_positionLock.z = 0.0f;

		m_pAccountObject = nullptr;

		m_pMsgOutboxSpawnStaticMO = nullptr;
		m_pMsgOutboxAttrib = nullptr;
		m_pMsgOutboxEquip = nullptr;
		m_pMsgOutboxGeneral = nullptr;
		m_pMsgOutboxEvent = nullptr;

		m_timerMsgRx.Reset();

		m_skillDBisReferenced = FALSE;
	}
}

CPlayerObject* CPlayerObjectList::GetPlayerObjectByName(const std::string& userName) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPO = (CPlayerObject*)GetNext(pos);
		if (pPO && pPO->m_charName.CompareNoCase(userName.c_str()) == 0)
			return pPO;
	}
	return nullptr;
}

CPlayerObject* CPlayerObjectList::GetPlayerObjectByNetTraceId(int netTraceId) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPO = (CPlayerObject*)GetNext(pos);
		if (pPO && pPO->m_netTraceId == netTraceId)
			return pPO;
	}
	return nullptr;
}

CPlayerObject* CPlayerObjectList::GetPlayerObjectByNetId(const NETID& netId, bool includeSpawning) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPO = (CPlayerObject*)GetNext(pos);
		if (pPO && ((pPO->m_netId == netId) || (includeSpawning && pPO->m_pAccountObject && (pPO->m_pAccountObject->m_netId == netId))))
			return pPO;
	}
	return nullptr;
}

CPlayerObject* CPlayerObjectList::GetPlayerObjectByPlayerHandle(const PLAYER_HANDLE& handle) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPO = (CPlayerObject*)GetNext(pos);
		if (pPO && pPO->m_handle == handle)
			return pPO;
	}
	return nullptr;
}

CPlayerObject* CPlayerObjectList::GetPlayerObjectByAccountNumber(int accNum) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPO = (CPlayerObject*)GetNext(pos);
		if (pPO && pPO->m_accountNumberRef == accNum)
			return pPO;
	}
	return nullptr;
}

BOOL CPlayerObjectList::RemoveByPtrByLiveHandle(PLAYER_HANDLE handle) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) { //account list
		auto pPO = (CPlayerObject*)GetNext(posLoc);
		if (pPO && pPO->m_handle == handle) {
			LocationRegistry::singleton().unregister(pPO->m_netId);
			pPO->SafeDelete();
			delete pPO;
			pPO = nullptr;
			RemoveAt(posLast);
			return TRUE;
		}
	}
	return FALSE;
}

void CPlayerObjectList::ReinitMemory() {
	for (POSITION posLoc = GetHeadPosition(); posLoc;) {
		auto pPO = (CPlayerObject*)GetNext(posLoc);
		if (!pPO || !pPO->m_inventory)
			continue;
		CInventoryObjList* pIOL = NULL;
		pPO->m_inventory->Clone(&pIOL);
		delete pPO->m_inventory;
		pPO->m_inventory = nullptr;
		pPO->m_inventory = pIOL;
	}
}

void CPlayerObjectList::SafeDelete() {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPO = (CPlayerObject*)GetNext(pos);
		if (!pPO)
			continue;
		pPO->SafeDelete();
		delete pPO;
	}
	RemoveAll();
}

void CPlayerObjectList::Clone(CPlayerObjectList** ppPOL_out) {
	auto pPOL = new CPlayerObjectList();
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPO = (CPlayerObject*)GetNext(pos);
		if (!pPO)
			continue;
		CPlayerObject* pPO_new = nullptr;
		pPO->Clone(&pPO_new);
		pPOL->AddTail(pPO_new);
	}
	*ppPOL_out = pPOL;
}

void CPlayerObjectList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			CPlayerObject* elementPtr = (CPlayerObject*)GetNext(pos);
			elementPtr->SerializeAlloc(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			CPlayerObject* elementPtr = new CPlayerObject();
			elementPtr->SerializeAlloc(ar);
			AddTail(elementPtr);
		}
	}
}

// figure out what instance of the zone we should be using
ZoneIndex CPlayerObject::createInstanceId(const ZoneIndex& zoneIndex) {
	ZoneIndex ret = zoneIndex;

	switch (zoneIndex.GetType()) {
		case CI_GUILD:
			// set the id to our guild, if we have one
			if (m_clanHandle != NO_CLAN) {
				ret.setAsGuild(m_clanHandle);
			} else {
				// can't zone if not in a guild
				ret = INVALID_ZONE_INDEX;
			}
			break;

		case CI_HOUSING:
			// for now make it just for us
			ASSERT(m_handle > 0);
			ret.setAsHousing(m_handle);
			break;

		case CI_GROUP:
		case CI_CHANNEL:
		case CI_NOT_INSTANCED:
		case CI_PERMANENT:
		case CI_ARENA:
			break; // do nothing since these have instance Ids generated elsewhere

		default:
			ASSERT(FALSE); // should never get here, send back copy
			break;
	}

	return ret;
}

void CPlayerObject::AllocatePacketLists() {
	// MSG_MOVE_TO_CLIENT Payload Data
	m_pMsgOutboxSpawnStaticMO = new CPacketObjList(CPacketObjList::LEN_FMT_USHORT);

	// MSG_UPDATE_TO_CLIENT Payload Data
	m_pMsgOutboxAttrib = new CPacketObjList(CPacketObjList::LEN_FMT_NONE);
	m_pMsgOutboxEquip = new CPacketObjListMsgEquip();
	m_pMsgOutboxGeneral = new CPacketObjList(CPacketObjList::LEN_FMT_NONE);
	m_pMsgOutboxEvent = new CPacketObjList(CPacketObjList::LEN_FMT_ULONG);
}

bool CPlayerObject::VerifyStatRequirement(CStatBonusList* pSBL) {
	bool ret = true;
	if (m_stats && pSBL) { //checked for valid requirements
		for (POSITION pos = pSBL->GetHeadPosition(); pos;) {
			auto pSBO = (CStatBonusObj*)pSBL->GetNext(pos);
			if (!pSBO)
				continue;
			ret = m_stats->VerifyStatRequirement(pSBO->m_statBonus, pSBO->m_statID, m_inventory) != FALSE;
			if (!ret)
				break;
		}
	}
	return ret; // no stats, or no requirements
}

void CPlayerObject::getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s) {
	// create the command for our parent to delete us
	gsMetaDatum& md = (*m_gsMetaData)[i];

	// do our custom one
	if (md.GetDescId() == IDS_PLAYEROBJECT_MOVE ||
		md.GetDescId() == IDS_ACCOUNTOBJECT_EXPORT) {
		std::string parentId;
		const char* lastDelim = strrchr(ourObjectId, '_');
		if (md.GetDescId() == IDS_ACCOUNTOBJECT_EXPORT) {
			// just parent for this one
			if (lastDelim)
				parentId.assign(ourObjectId, lastDelim - ourObjectId);
		} else { // move
			// figure out the grandparent object id since that is who processes this
			if (lastDelim) {
				do {
					lastDelim--;
				} while (lastDelim > ourObjectId && *lastDelim != '_');
				parentId.assign(ourObjectId, lastDelim - ourObjectId);
			}
		}

		_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
			"<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\" objectId=\"%s\"",
			loadStr(md.GetDescId()).c_str(),
			md.GetType(),
			md.GetFlags(),
			md.GetDescId(),
			parentId.c_str());
		if (filter == 0 || filter->allAttributes())
			_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
				"%s help=\"%s\" minValue=\"%f\" maxValue=\"%f\" indent=\"%d\" groupId=\"%d\"",
				s,
				loadStr(md.GetHelpId()).c_str(),
				md.GetMinValue(), md.GetMaxValue(),
				md.GetIndent(),
				md.GetGroupTitleId());

		strncat_s(s, TEMP_STR_SIZE, "></GetSetRec>\n", _TRUNCATE);

		xml += s;

	} else {
		GetSet::getItemXml(i, ourObjectId, depth, filter, myMd, xml, s);
	}
}

void CPlayerObject::recordPacketSizeViolation(
	TimeMs hardLimitPeriod,
	DWORD hardLimit,
	TimeMs softLimitPeriod,
	DWORD softLimit,
	bool& hardLimitExceeded,
	bool& softLimitExceeded) {
	// Thread-safety notes:
	// This function is currently only called by DP threads (CMultiplayerObj::DataFromClient) and protected by CMultiplayerObj::m_criticalSection.
	// Additional mutex will be needed if engine threads or other threads need to access this function or the data it manipulates.

	// reset if elapsed
	if (m_timerPacketSizeViolationHardLimit.ElapsedMs() > hardLimitPeriod) {
		m_timerPacketSizeViolationHardLimit.Reset();
		m_packetSizeViolationHardLimitCount = 0;
	}

	if (m_timerPacketSizeViolationSoftLimit.ElapsedMs() > softLimitPeriod) {
		m_timerPacketSizeViolationSoftLimit.Reset();
		m_packetSizeViolationSoftLimitCount = 0;
	}

	// increment counters
	m_packetSizeViolationHardLimitCount++;
	m_packetSizeViolationSoftLimitCount++;

	// out
	softLimitExceeded = m_packetSizeViolationSoftLimitCount > softLimit;
	hardLimitExceeded = m_packetSizeViolationHardLimitCount > hardLimit;
}

bool CPlayerObject::EncodeMoveDataToClient(const MOVE_DATA& moveData, std::vector<BYTE>& msg) {
	// Get Move Data Player
	auto netTraceId = moveData.netTraceId;
	if (netTraceId <= 0)
		return false;

	// Get Last Move Data To Client For This Player
	MOVE_DATA moveData_find;
	auto itr = m_moveDataToClientMap.find(netTraceId);
	if (itr != m_moveDataToClientMap.end())
		moveData_find = itr->second;

	// Encode Move Data To Client (deltas since last time)
	moveData.Encode(moveData_find, msg);

	// Update Move Data For Next Time
	m_moveDataToClientMap[netTraceId] = moveData;

	return true;
}

void CPlayerObject::HandleMsgMoveToServer(BYTE* pData) {
	if (!pData)
		return;

	// Skip Message Header
	pData += sizeof(MSG_HEADER);

	// Decode Message Move To Server (update move data received)
	m_moveDataToServer.Decode(pData);

	// Update Movement Interpolator
	MovementData md(m_moveDataToServer.pos, m_moveDataToServer.dir, m_moveDataToServer.up);
	if (m_justSpawned) {
		m_justSpawned = FALSE;
		MovementInterpolatorInit(md);
		LogInfo("MSG_MOVE_RECEIVED - " << ToStr());
	} else {
		MovementInterpolatorUpdate(md);
	}

	// Update Animations
	m_animGLID = m_moveDataToServer.animGlid1;
	m_animGLID2nd = m_moveDataToServer.animGlid2;

	m_inGame = TRUE;
}

IMPLEMENT_SERIAL_SCHEMA(CPlayerObject, CObject)
IMPLEMENT_SERIAL_SCHEMA(CPlayerObjectList, CObList)

// turn on if you want to see all the values, not relevant ones
#ifdef SHOW_ALL_GETSET
#define SHOW_ALL GS_FLAG_EXPOSE
#else
#define SHOW_ALL GS_FLAG_READ_ONLY
#endif

BEGIN_GETSET_IMPL(CPlayerObject, IDS_PLAYEROBJECT_NAME)
// USED BY PYTHON AND LUA
GETSET(m_currentChannel, gsUlong, GS_FLAG_READ_WRITE, 0, 0, PLAYEROBJECT, CHANNEL)
GETSET_MAX(m_charName, gsCString, GS_FLAGS_DEFAULT | GS_FLAG_INSTANCE_NAME | GS_FLAG_CAN_ADD, 0, 0, PLAYEROBJECT, CHARNAME, STRLEN_MAX)
//GETSET_D3DVECTOR(m_3dPosition, GS_FLAGS_DEFAULT, 1, 0, PLAYEROBJECT, 3DPOSITION)
// USED BY PYTHON ONLY
GETSET_RANGE(m_currentZoneIndex, gsInt, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, PLAYEROBJECT, CURRENTWORLDID, INT_MIN, INT_MAX)
GETSET_RANGE(m_handle, gsInt, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, PLAYEROBJECT, HANDLE, INT_MIN, INT_MAX)
GETSET(m_gm, gsUlong, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, ACCOUNTOBJECT, GM)
GETSET(m_admin, gsUlong, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, ACCOUNTOBJECT, ADMINISTRATORACCESS)
GETSET_RANGE(m_curEDBIndex, gsInt, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, CUREDBINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_netTraceId, gsInt, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, NETWORKASSIGNEDID, INT_MIN, INT_MAX)
GETSET(m_justSpawned, gsBOOL, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, JUSTSPAWNED)
GETSET_RANGE(m_aiControlled, gsBOOL, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, AICONTROLLED, -1, 1)
GETSET_OBLIST_PTR(m_inventory, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, PLAYEROBJECT, CURRENTINVENTORY)
GETSET_RANGE(m_housingZoneIndex, gsInt, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, PLAYEROBJECT, HOUSINGZONEINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_animGLID, gsInt, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, CURANIM, INT_MIN, INT_MAX)
GETSET_MAX(m_title, gsCString, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, TITLE, STRLEN_MAX)
GETSET_RANGE(m_originalEDBCfg, gsInt, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, PLAYEROBJECT, ORIGINALEDBCFG, INT_MIN, INT_MAX)
// USED BY DDR_S.CPP
GETSET(m_netId, gsUlong, SHOW_ALL, 0, 0, PLAYEROBJECT, DPNID)
GETSET_RANGE(m_interactionStates, gsInt, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, INTERACTIONSTATES, INT_MIN, INT_MAX)

//GETSET_MAX( m_lastName, gsCString, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, LAST_NAME, STRLEN_MAX )
//GETSET_MAX( m_clanName, gsCString, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, CLAN_NAME, STRLEN_MAX )
//GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, PLAYEROBJECT, MOVE )
//GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, PLAYEROBJECT, EXPORT )
//GETSET_ACTION(this, GS_FLAGS_ACTION, 0, 0, ADMIN, DELETE )
//GETSET_OBLIST_PTR( m_bankInventory, GS_FLAGS_DEFAULT | GS_FLAG_CAN_ADD, 0, 0, PLAYEROBJECT, BANKINVENTORY )
//GETSET_OBLIST_PTR( m_skills, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, SKILLS )
//GETSET_OBLIST_PTR( m_stats, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, STATS )
//GETSET_OBJ_PTR( m_noteriety, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, NOTERIETY, CNoterietyObj )
//GETSET_OBJ_PTR( m_cash, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, CASH, CCurrencyObj )
//GETSET_OBJ_PTR( m_bankCash, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, BANKCASH, CCurrencyObj )
//GETSET_RANGE( m_respawnZoneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, RESPAWNWORLD, INT_MIN, INT_MAX )
//GETSET_D3DVECTOR( m_respawnPosition, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, RESPAWNPOSITION )
//GETSET_RANGE( m_dbCfg, gsInt, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, DBCFG, INT_MIN, INT_MAX )
//GETSET( m_inGame, gsBOOL, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, PLAYEROBJECT, INGAME )
//GETSET_RANGE( m_timeLastUpdated, gsLong, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, TIMELASTUPDATED, LONG_MIN, LONG_MAX )
//GETSET_RANGE( m_clanHandle, gsInt, GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE, 0, 0, PLAYEROBJECT, CLANHANDLE, INT_MIN, INT_MAX )
//GETSET_RANGE( m_mountedByPlayerID, gsInt, GS_FLAG_READ_WRITE | SHOW_ALL, 0, 0, PLAYEROBJECT, MOUNTEDBYPLAYERID, INT_MIN, INT_MAX )
//GETSET_RANGE( m_bornSpawnCfgIndex, gsInt, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, BORNSPAWNCFGINDEX, INT_MIN, INT_MAX )
//GETSET_RANGE( m_baseMeshes, gsInt, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, DEFORMABLEMESHCOUNT, INT_MIN, INT_MAX )
//GETSET_RANGE( m_altCfgIndex, gsInt, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, PLAYEROBJECT, ALTCFGINDEX, INT_MIN, INT_MAX )
//GETSET_D3DVECTOR( m_upVector, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, UPVECTOR )
//GETSET_D3DVECTOR( m_dirVector, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, DIRVECTOR )
//GETSET_D3DVECTOR( m_LastUpVector, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, LASTUPVECTOR )
//GETSET_D3DVECTOR( m_LastDirVector, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, LASTDIRVECTOR )
//GETSET_D3DVECTOR( m_Last3dPosition, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, LAST3DPOSITION )
//GETSET_D3DVECTOR( m_TargetUpVector, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, TARGETUPVECTOR )
//GETSET_D3DVECTOR( m_TargetDirVector, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, TARGETDIRVECTOR )
//GETSET_D3DVECTOR( m_Target3dPosition, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, TARGET3DPOSITION )
//GETSET_RANGE( m_animationState, gsInt, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, ANIMATIONSTATE, INT_MIN, INT_MAX )
//GETSET_MAX( m_currentWorldMap, gsCString, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, CURRENTWORLDMAP, STRLEN_MAX )
//GETSET_RANGE( m_mountedOnPlayerID, gsInt, GS_FLAG_READ_WRITE | SHOW_ALL, 0, 0, PLAYEROBJECT, MOUNTEDONPLAYERID, INT_MIN, INT_MAX )
//GETSET( m_spawnAbility , gsUlong, GS_FLAG_READ_ONLY | SHOW_ALL, 0, 0, ACCOUNTOBJECT, CANSPAWN )
//GETSET_OBJ_PTR( m_giftCredits, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, GIFTCREDITS, CCurrencyObj )
//GETSET_MAX( m_displayName, gsCString, GS_FLAGS_DEFAULT | GS_FLAG_INSTANCE_NAME | GS_FLAG_CAN_ADD, 0, 0, PLAYEROBJECT, DISPLAYNAME, STRLEN_MAX )
//GETSET_MAX( m_displayTitle, gsCString, GS_FLAGS_DEFAULT, 0, 0, PLAYEROBJECT, DISPLAYTITLE, STRLEN_MAX )
END_GETSET_IMPL

} // namespace KEP
