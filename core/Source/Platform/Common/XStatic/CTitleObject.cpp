///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CTitleObject.h"

namespace KEP {

void CTitleObject::Write(CArchive& ar) const {
	ar << CStringA(m_titleName.c_str()) << m_titleID;
}

void CTitleObject::Read(CArchive& ar) {
	CStringA titleName;
	ar >> titleName >> m_titleID;
	m_titleName = (const char*)titleName;
}

} // namespace KEP