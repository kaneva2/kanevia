///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CHousePlacementClass.h"
#include "CHousingClass.h"
#include "CInventoryClass.h"

namespace KEP {

CHPlacementObj::CHPlacementObj() {
	m_type = 0; //DB reference
	m_playerHandle = 0; // 6/06 updated to be player handle
	m_wldPosition.x = 0.0f;
	m_wldPosition.y = 0.0f;
	m_wldPosition.z = 0.0f;
	m_id = 0;
	m_open = TRUE;
	m_vaultInstalled = FALSE;
	m_vaultItemCapacity = 0;
	m_vaultCurrentCash = 0;
	m_vaultInventory = NULL;
	m_storeItemCapacity = 0;
	m_storeCurrentCash = 0;
	m_storeInventory = NULL;
}

void CHPlacementObj::SafeDelete() {
	if (m_storeInventory) {
		m_storeInventory->SafeDelete();
		delete m_storeInventory;
		m_storeInventory = 0;
	}
	if (m_vaultInventory) {
		m_vaultInventory->SafeDelete();
		delete m_vaultInventory;
		m_vaultInventory = 0;
	}
}

void CHPlacementObj::Clone(CHPlacementObj** clone) {
	CHPlacementObj* copyLocal = new CHPlacementObj();
	copyLocal->m_type = m_type; //DB reference
	copyLocal->m_playerHandle = m_playerHandle; //one placement per accout allowed
	copyLocal->m_wldPosition = m_wldPosition;
	copyLocal->m_id = m_id;
	copyLocal->m_open = m_open;
	copyLocal->m_vaultInstalled = m_vaultInstalled;
	copyLocal->m_vaultItemCapacity = m_vaultItemCapacity;
	copyLocal->m_vaultCurrentCash = m_vaultCurrentCash;

	if (m_vaultInventory)
		m_vaultInventory->Clone(&copyLocal->m_vaultInventory);

	copyLocal->m_storeItemCapacity = m_storeItemCapacity;
	copyLocal->m_storeCurrentCash = m_storeCurrentCash;

	if (m_storeInventory)
		m_storeInventory->Clone(&copyLocal->m_storeInventory);

	*clone = copyLocal;
}

void CHPlacementObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHPlacementObj* spnPtr = (CHPlacementObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

CHPlacementObj* CHPlacementObjList::GetByPlayerHandle(PLAYER_HANDLE playerHandle) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHPlacementObj* spnPtr = (CHPlacementObj*)GetNext(posLoc);
		if (spnPtr->m_playerHandle == playerHandle)
			return spnPtr;
	}
	return NULL;
}

CHPlacementObj* CHPlacementObjList::GetHouseByBox(Vector3f position) {
	BNDBOX clipBoxTranslated;

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHPlacementObj* spnPtr = (CHPlacementObj*)GetNext(posLoc);
		clipBoxTranslated.max.x = 150.0f + spnPtr->m_wldPosition.x;
		clipBoxTranslated.max.y = 150.0f + spnPtr->m_wldPosition.y;
		clipBoxTranslated.max.z = 150.0f + spnPtr->m_wldPosition.z;
		clipBoxTranslated.min.x = spnPtr->m_wldPosition.x - 150.0f;
		clipBoxTranslated.min.y = spnPtr->m_wldPosition.y - 150.0f;
		clipBoxTranslated.min.z = spnPtr->m_wldPosition.z - 150.0f;
		if (BoundBoxPointCheck(position, clipBoxTranslated.min, clipBoxTranslated.max)) {
			return spnPtr;
		}
	}
	return NULL;
}

BOOL CHPlacementObjList::BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max) {
	if (point.x >= min.x &&
		point.x <= max.x &&
		point.y >= min.y &&
		point.y <= max.y &&
		point.z >= min.z &&
		point.z <= max.z)
		return TRUE;
	return FALSE;
}

BOOL CHPlacementObjList::RemoveByPlayerHandle(PLAYER_HANDLE playerHandle) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CHPlacementObj* spnPtr = (CHPlacementObj*)GetNext(posLoc);
		if (spnPtr->m_playerHandle == playerHandle) {
			delete spnPtr;
			spnPtr = 0;
			RemoveAt(posLast);
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CHPlacementObjList::AddPlacement(int type,
	PLAYER_HANDLE playerHandle,
	Vector3f position,
	int id) {
	if (GetByPlayerHandle(playerHandle))
		return FALSE;

	CHPlacementObj* m_placementObj = new CHPlacementObj();

	m_placementObj->m_id = id;
	m_placementObj->m_type = type;
	m_placementObj->m_playerHandle = playerHandle;
	m_placementObj->m_wldPosition = position;

	AddTail(m_placementObj);
	return TRUE;
}

void CHPlacementObjList::Clone(CHPlacementObjList** clone) {
	CHPlacementObjList* copyLocal = new CHPlacementObjList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHPlacementObj* skPtr = (CHPlacementObj*)GetNext(posLoc);
		CHPlacementObj* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}

void CHPlacementObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_type
		   << m_playerHandle
		   << m_wldPosition.x
		   << m_wldPosition.y
		   << m_wldPosition.z
		   << m_open
		   << m_id
		   << m_vaultInstalled
		   << m_vaultItemCapacity
		   << m_vaultCurrentCash
		   << m_vaultInventory
		   << m_storeItemCapacity
		   << m_storeCurrentCash
		   << m_storeInventory;

	} else {
		ar >> m_type >> m_playerHandle >> m_wldPosition.x >> m_wldPosition.y >> m_wldPosition.z >> m_open >> m_id >> m_vaultInstalled >> m_vaultItemCapacity >> m_vaultCurrentCash >> m_vaultInventory >> m_storeItemCapacity >> m_storeCurrentCash >> m_storeInventory;

		/*m_vaultInstalled      = FALSE;
		m_vaultItemCapacity   = 0;
		m_vaultCurrentCash    = 0;
		m_vaultInventory      = NULL;*/
		/*m_storeItemCapacity   = 0;
		m_storeCurrentCash    = 0;
		m_storeInventory      = NULL;*/
	}
}
//--------------------------
//--------------------------
CHousingZone::CHousingZone() {
	ZeroMemory(&m_visibleBox, sizeof(BNDBOX));
	ZeroMemory(&m_zoneBox, sizeof(BNDBOX));
	m_subDivisionX = 2;
	m_subDivisionY = 2;
	m_subDivisionZ = 2;
	m_channel.clear();
	m_densityAllowance = 300.0f;

	m_hsTable = NULL;
	m_houseDatabase = NULL;
	m_optimizedSave = FALSE;
}

void CHousingZone::Clone(CHousingZone** clone) {
	CHousingZone* copyLocal = new CHousingZone();

	CopyMemory(&copyLocal->m_visibleBox, &m_visibleBox, sizeof(BNDBOX));
	CopyMemory(&copyLocal->m_zoneBox, &m_zoneBox, sizeof(BNDBOX));
	copyLocal->m_subDivisionX = m_subDivisionX;
	copyLocal->m_subDivisionY = m_subDivisionY;
	copyLocal->m_subDivisionZ = m_subDivisionZ;
	copyLocal->m_channel = m_channel;
	copyLocal->m_densityAllowance = m_densityAllowance;

	if (m_hsTable) {
		copyLocal->m_hsTable = new Placement**[(int)m_subDivisionX];
		for (int m = 0; m < m_subDivisionX; m++) {
			copyLocal->m_hsTable[m] = new Placement*[(int)m_subDivisionY];
			for (int n = 0; n < m_subDivisionY; n++) {
				copyLocal->m_hsTable[m][n] = new Placement[(int)m_subDivisionZ];
			}
		}

		for (int xPos = 0; xPos < m_subDivisionX; xPos++) {
			for (int yPos = 0; yPos < m_subDivisionY; yPos++) {
				for (int zPos = 0; zPos < m_subDivisionZ; zPos++) {
					m_hsTable[xPos][yPos][zPos].m_placementList->Clone(&copyLocal->m_hsTable[xPos][yPos][zPos].m_placementList);
				}
			}
		}
	}

	if (!m_optimizedSave)
		if (m_houseDatabase)
			m_houseDatabase->Clone(&copyLocal->m_houseDatabase);

	m_optimizedSave = FALSE;

	*clone = copyLocal;
}

void CHousingZone::SafeDelete() {
	if (m_hsTable) {
		for (int xPos = 0; xPos < m_subDivisionX; xPos++) {
			for (int yPos = 0; yPos < m_subDivisionY; yPos++) {
				for (int zPos = 0; zPos < m_subDivisionZ; zPos++) {
					m_hsTable[xPos][yPos][zPos].m_placementList->SafeDelete();
					delete m_hsTable[xPos][yPos][zPos].m_placementList;
					m_hsTable[xPos][yPos][zPos].m_placementList = 0;
				}
				delete[] m_hsTable[xPos][yPos];
				m_hsTable[xPos][yPos] = 0;
			}
			delete[] m_hsTable[xPos];
			m_hsTable[xPos] = 0;
		}
		delete[] m_hsTable;
		m_hsTable = 0;
	}

	if (m_houseDatabase) {
		m_houseDatabase->SafeDelete();
		delete m_houseDatabase;
		m_houseDatabase = 0;
	}
}

void CHousingZoneDB::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

BOOL CHousingZone::RemoveByPlayerHandle(PLAYER_HANDLE playerHandle) {
	if (!m_houseDatabase)
		return FALSE;

	m_houseDatabase->RemoveByPlayerHandle(playerHandle);

	if (m_hsTable) {
		for (int xPos = 0; xPos < m_subDivisionX; xPos++) {
			for (int yPos = 0; yPos < m_subDivisionY; yPos++) {
				for (int zPos = 0; zPos < m_subDivisionZ; zPos++) {
					m_hsTable[xPos][yPos][zPos].m_placementList->RemoveByPlayerHandle(playerHandle);
				}
			}
		}
	}

	return TRUE;
}

void CHousingZone::CreateNewHousingZone(CStringA zoneName,
	float subX,
	float subY,
	float subZ,
	const ChannelId& channel,
	BNDBOX box,
	BNDBOX visibleBox) {
	SafeDelete();

	m_zoneLabel = zoneName;
	m_subDivisionX = subX;
	m_subDivisionY = subY;
	m_subDivisionZ = subZ;
	m_channel = channel;

	CopyMemory(&m_zoneBox, &box, sizeof(BNDBOX));
	CopyMemory(&m_visibleBox, &visibleBox, sizeof(BNDBOX));

	m_hsTable = new Placement**[(int)m_subDivisionX];
	for (int m = 0; m < m_subDivisionX; m++) {
		m_hsTable[m] = new Placement*[(int)m_subDivisionY];
		for (int n = 0; n < m_subDivisionY; n++) {
			m_hsTable[m][n] = new Placement[(int)m_subDivisionZ];
			for (int o = 0; o < m_subDivisionZ; o++) {
				m_hsTable[m][n][o].m_placementList = new CHPlacementObjList();
			}
		}
	}
}

BOOL CHousingZone::BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max) {
	if (point.x >= min.x &&
		point.x <= max.x &&
		point.y >= min.y &&
		point.y <= max.y &&
		point.z >= min.z &&
		point.z <= max.z)
		return TRUE;
	return FALSE;
}

float CHousingZone::MagnitudeSquared(float& Mx1, float& My1,
	float& Mz1, float& Mx2,
	float& My2, float& Mz2) {
	float temp_Mxyz = (((Mx1 - Mx2) * (Mx1 - Mx2)) + ((My1 - My2) * (My1 - My2)) + ((Mz1 - Mz2) * (Mz1 - Mz2)));

	return (float)sqrt(temp_Mxyz);
}

CHPlacementObjList* CHousingZone::GetVisibleList(Vector3f wldPosition, float range) {
	if (!m_houseDatabase)
		return NULL;

	CHPlacementObjList* visibleList = new CHPlacementObjList();

	for (POSITION posLoc = m_houseDatabase->GetHeadPosition(); posLoc != NULL;) {
		CHPlacementObj* houseOnTable = (CHPlacementObj*)m_houseDatabase->GetNext(posLoc);
		if (houseOnTable) {
			float dist = MagnitudeSquared(wldPosition.x, wldPosition.y, wldPosition.z, houseOnTable->m_wldPosition.x, houseOnTable->m_wldPosition.y, houseOnTable->m_wldPosition.z);
			if (dist < range)
				visibleList->AddTail(houseOnTable);
		}
	}

	if (visibleList->GetCount() == 0) { //none found
		delete visibleList;
		visibleList = 0;

		return visibleList;
	} else
		return visibleList;
}

CHPlacementObjList* CHousingZone::GetContainerByWldPosition(Vector3f position) {
	if (!BoundBoxPointCheck(position, m_zoneBox.min, m_zoneBox.max))
		return NULL;
	//current position              //Container Length
	int positionXtable = (int)((m_zoneBox.max.x - position.x) / ((m_zoneBox.max.x - m_zoneBox.min.x) / m_subDivisionX));
	int positionYtable = (int)((m_zoneBox.max.y - position.y) / ((m_zoneBox.max.y - m_zoneBox.min.y) / m_subDivisionY));
	int positionZtable = (int)((m_zoneBox.max.z - position.z) / ((m_zoneBox.max.z - m_zoneBox.min.z) / m_subDivisionZ));

	if (positionXtable >= m_subDivisionX || positionXtable < 0)
		return NULL;
	if (positionYtable >= m_subDivisionY || positionYtable < 0)
		return NULL;
	if (positionZtable >= m_subDivisionZ || positionZtable < 0)
		return NULL;

	//int count = m_hsTable[positionXtable][positionYtable][positionZtable].m_placementList->GetCount();

	return m_hsTable[positionXtable][positionYtable][positionZtable].m_placementList;
}

BOOL CHousingZone::AnyHousesWithinArea(Vector3f position, float radius) {
	if (!m_houseDatabase)
		return FALSE;

	for (POSITION posLoc = m_houseDatabase->GetHeadPosition(); posLoc != NULL;) {
		CHPlacementObj* house = (CHPlacementObj*)m_houseDatabase->GetNext(posLoc);

		float dist = MagnitudeSquared(house->m_wldPosition.x, house->m_wldPosition.y,
			house->m_wldPosition.z, position.x,
			position.y, position.z);

		if (dist < radius)
			return TRUE;
	}
	return FALSE;
}

int CHousingZone::AddPlacementObjToTable(CHPlacementObj* placement, BNDBOX& clipBox) {
	int ret = PrepareToAddPlacementObjToTable(placement->m_type,
		placement->m_playerHandle,
		clipBox,
		placement->m_wldPosition,
		placement->m_id);
	if (ret == PLACEMENT_OK) {
		if (m_houseDatabase->GetByPlayerHandle(placement->m_playerHandle))
			ret = PLACEMENT_FAILED;
		else
			m_houseDatabase->AddTail(placement);
	}
	return ret;
}

int CHousingZone::PrepareToAddPlacementObjToTable(int type,
	PLAYER_HANDLE playerHandle,
	BNDBOX& clipBoxOfObjectPlaced,
	Vector3f& wldPosition,
	int id) {
	if (!BoundBoxPointCheck(wldPosition, m_zoneBox.min, m_zoneBox.max))
		return PLACEMENT_FAILED; //out of housing zone

	if (AnyHousesWithinArea(wldPosition, m_densityAllowance))
		return PLACEMENT_TOO_CLOSE; //too close to another house

	BNDBOX clipBoxTranslated;
	clipBoxTranslated.max.x = clipBoxOfObjectPlaced.max.x + wldPosition.x;
	clipBoxTranslated.max.y = clipBoxOfObjectPlaced.max.y + wldPosition.y;
	clipBoxTranslated.max.z = clipBoxOfObjectPlaced.max.z + wldPosition.z;
	clipBoxTranslated.min.x = clipBoxOfObjectPlaced.min.x + wldPosition.x;
	clipBoxTranslated.min.y = clipBoxOfObjectPlaced.min.y + wldPosition.y;
	clipBoxTranslated.min.z = clipBoxOfObjectPlaced.min.z + wldPosition.z;

	Vector3f point1, point2, point3, point4, point5, point6, point7, point8;
	//point 1
	point1.x = clipBoxTranslated.min.x;
	point1.y = clipBoxTranslated.max.y;
	point1.z = clipBoxTranslated.max.z;
	//point 2
	point2.x = clipBoxTranslated.max.x;
	point2.y = clipBoxTranslated.max.y;
	point2.z = clipBoxTranslated.max.z;
	//point 3
	point3.x = clipBoxTranslated.max.x;
	point3.y = clipBoxTranslated.max.y;
	point3.z = clipBoxTranslated.min.z;
	//point 4
	point4.x = clipBoxTranslated.min.x;
	point4.y = clipBoxTranslated.max.y;
	point4.z = clipBoxTranslated.min.z;
	//point 5
	point5.x = clipBoxTranslated.min.x;
	point5.y = clipBoxTranslated.min.y;
	point5.z = clipBoxTranslated.max.z;
	//point 6
	point6.x = clipBoxTranslated.max.x;
	point6.y = clipBoxTranslated.min.y;
	point6.z = clipBoxTranslated.max.z;
	//point 7
	point7.x = clipBoxTranslated.max.x;
	point7.y = clipBoxTranslated.min.y;
	point7.z = clipBoxTranslated.min.z;
	//point 8
	point8.x = clipBoxTranslated.min.x;
	point8.y = clipBoxTranslated.min.y;
	point8.z = clipBoxTranslated.min.z;

	CHPlacementObjList* container1 = GetContainerByWldPosition(point1);
	if (!container1)
		return PLACEMENT_FAILED;
	CHPlacementObjList* container2 = GetContainerByWldPosition(point2);
	if (!container2)
		return PLACEMENT_FAILED;
	CHPlacementObjList* container3 = GetContainerByWldPosition(point3);
	if (!container3)
		return PLACEMENT_FAILED;
	CHPlacementObjList* container4 = GetContainerByWldPosition(point4);
	if (!container4)
		return PLACEMENT_FAILED;
	CHPlacementObjList* container5 = GetContainerByWldPosition(point5);
	if (!container5)
		return PLACEMENT_FAILED;
	CHPlacementObjList* container6 = GetContainerByWldPosition(point6);
	if (!container6)
		return PLACEMENT_FAILED;
	CHPlacementObjList* container7 = GetContainerByWldPosition(point7);
	if (!container7)
		return PLACEMENT_FAILED;
	CHPlacementObjList* container8 = GetContainerByWldPosition(point8);
	if (!container8)
		return PLACEMENT_FAILED;

	container1->AddPlacement(type, playerHandle, wldPosition, id);
	container2->AddPlacement(type, playerHandle, wldPosition, id);
	container3->AddPlacement(type, playerHandle, wldPosition, id);
	container4->AddPlacement(type, playerHandle, wldPosition, id);
	container5->AddPlacement(type, playerHandle, wldPosition, id);
	container6->AddPlacement(type, playerHandle, wldPosition, id);
	container7->AddPlacement(type, playerHandle, wldPosition, id);
	container8->AddPlacement(type, playerHandle, wldPosition, id);

	if (!m_houseDatabase)
		m_houseDatabase = new CHPlacementObjList();

	return PLACEMENT_OK;
}

int CHousingZone::AddPlacementObjToTable(int type,
	PLAYER_HANDLE playerHandle,
	BNDBOX& clipBoxOfObjectPlaced,
	Vector3f& wldPosition,
	int id) {
	int ret = PrepareToAddPlacementObjToTable(type,
		playerHandle,
		clipBoxOfObjectPlaced,
		wldPosition,
		id);

	if (ret != PLACEMENT_OK)
		return ret;
	//Tracking database
	return m_houseDatabase->AddPlacement(type, playerHandle, wldPosition, id) ? PLACEMENT_OK : PLACEMENT_FAILED;
}

void CHousingZone::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_zoneLabel
		   << m_subDivisionX
		   << m_subDivisionY
		   << m_subDivisionZ
		   << m_channel
		   << m_visibleBox.max.x
		   << m_visibleBox.max.y
		   << m_visibleBox.max.z
		   << m_visibleBox.min.x
		   << m_visibleBox.min.y
		   << m_visibleBox.min.z
		   << m_zoneBox.max.x
		   << m_zoneBox.max.y
		   << m_zoneBox.max.z
		   << m_zoneBox.min.x
		   << m_zoneBox.min.y
		   << m_zoneBox.min.z
		   << m_densityAllowance;
		if (!m_optimizedSave)
			ar << m_houseDatabase;
		else {
			CHPlacementObjList* temp = NULL;
			ar << temp;
		}

		m_optimizedSave = FALSE;

		if (m_hsTable) {
			for (int xPos = 0; xPos < m_subDivisionX; xPos++) {
				for (int yPos = 0; yPos < m_subDivisionY; yPos++) {
					for (int zPos = 0; zPos < m_subDivisionZ; zPos++) {
						ar << m_hsTable[xPos][yPos][zPos].m_placementList;
					}
				}
			}
		}
	} else {
		ar >> m_zoneLabel >> m_subDivisionX >> m_subDivisionY >> m_subDivisionZ >> m_channel >> m_visibleBox.max.x >> m_visibleBox.max.y >> m_visibleBox.max.z >> m_visibleBox.min.x >> m_visibleBox.min.y >> m_visibleBox.min.z >> m_zoneBox.max.x >> m_zoneBox.max.y >> m_zoneBox.max.z >> m_zoneBox.min.x >> m_zoneBox.min.y >> m_zoneBox.min.z >> m_densityAllowance >> m_houseDatabase;

		m_optimizedSave = FALSE;

		//m_densityAllowance = 300.0f;

		m_hsTable = new Placement**[(int)m_subDivisionX];
		for (int m = 0; m < m_subDivisionX; m++) {
			m_hsTable[m] = new Placement*[(int)m_subDivisionY];
			for (int n = 0; n < m_subDivisionY; n++) {
				m_hsTable[m][n] = new Placement[(int)m_subDivisionZ];
			}
		}

		if (m_hsTable) {
			for (int xPos = 0; xPos < m_subDivisionX; xPos++) {
				for (int yPos = 0; yPos < m_subDivisionY; yPos++) {
					for (int zPos = 0; zPos < m_subDivisionZ; zPos++) {
						ar >> m_hsTable[xPos][yPos][zPos].m_placementList;
					}
				}
			}
		}
	}
}

CHousingZoneDB::CHousingZoneDB() {
	m_currentIDCount = 1;
}

// move the placements from "from" into this
// DB.  Any ones left behind need to be cleaned up
void CHousingZoneDB::MergePlacementsFrom(CHousingZoneDB* from, CHousingObjList* housingDB) {
	// for each from Housing DB, get the housing zone
	for (POSITION posLoc = from->GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)from->GetNext(posLoc);

		if (spnPtr->m_channel != INVALID_CHANNEL_ID && spnPtr->m_houseDatabase != 0) {
			// check against ours
			for (POSITION myPosLoc = GetHeadPosition(); myPosLoc != NULL;) {
				CHousingZone* mySpnPtr = (CHousingZone*)GetNext(myPosLoc);

				if (!spnPtr->m_channel.channelIdEquals(mySpnPtr->m_channel))
					continue; // not same channel

				// try to add all the placements into this zone
				POSITION delPos;
				for (POSITION placementLoc = spnPtr->m_houseDatabase->GetHeadPosition(); (delPos = placementLoc) != NULL;) {
					CHPlacementObj* placement = (CHPlacementObj*)spnPtr->m_houseDatabase->GetNext(placementLoc);
					CHousingObj* houseBasePtr = housingDB->GetByIndex(placement->m_type);
					if (houseBasePtr) {
						// valid database base
						BNDBOX clipBoxCast;
						clipBoxCast.max.x = houseBasePtr->m_clipBox.maxX;
						clipBoxCast.max.y = houseBasePtr->m_clipBox.maxY;
						clipBoxCast.max.z = houseBasePtr->m_clipBox.maxZ;
						clipBoxCast.min.x = houseBasePtr->m_clipBox.minX;
						clipBoxCast.min.y = houseBasePtr->m_clipBox.minY;
						clipBoxCast.min.z = houseBasePtr->m_clipBox.minZ;

						int result = mySpnPtr->AddPlacementObjToTable(placement, clipBoxCast);

						if (result == PLACEMENT_OK) {
							// remove placement since now moved to this db
							spnPtr->m_houseDatabase->RemoveAt(delPos);
						}
						// else too close, or not in this db
					}
				}
			}
		}
	}
}

int CHousingZoneDB::AddPlacementObjToTable(int type,
	PLAYER_HANDLE playerHandle,
	BNDBOX& clipBoxOfObjectPlaced,
	Vector3f& wldPosition,
	int* idReturn,
	const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);

		if (channel != INVALID_CHANNEL_ID) {
			if (!channel.channelIdEquals(spnPtr->m_channel)) //wid
				continue;

			int result = spnPtr->AddPlacementObjToTable(type, playerHandle, clipBoxOfObjectPlaced, wldPosition, m_currentIDCount);
			if (result != PLACEMENT_FAILED) {
				if (result == PLACEMENT_TOO_CLOSE)
					return PLACEMENT_TOO_CLOSE;

				*idReturn = m_currentIDCount;
				m_currentIDCount++;
				return PLACEMENT_OK;
			}
		}
	}

	return PLACEMENT_FAILED;
}

BOOL CHousingZoneDB::BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max) {
	if (point.x >= min.x &&
		point.x <= max.x &&
		point.y >= min.y &&
		point.y <= max.y &&
		point.z >= min.z &&
		point.z <= max.z)
		return TRUE;
	return FALSE;
}

CHPlacementObjList* CHousingZoneDB::GetVisibleList(Vector3f wldPosition, float range, const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (!spnPtr->m_channel.channelIdEquals(channel))
			continue;

		if (BoundBoxPointCheck(wldPosition, spnPtr->m_visibleBox.min, spnPtr->m_visibleBox.max)) {
			//in this zones visible range
			return spnPtr->GetVisibleList(wldPosition, range);
		} //end in this zones visible range
	}
	return NULL;
}

CHPlacementObjList* CHousingZoneDB::GetContainer(Vector3f wldPosition) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (BoundBoxPointCheck(wldPosition, spnPtr->m_zoneBox.min, spnPtr->m_zoneBox.max)) {
			//in this zones visible range
			return spnPtr->GetContainerByWldPosition(wldPosition);
		} //end in this zones visible range
	}
	return NULL;
}

CHPlacementObjList* CHousingZoneDB::GetContainerAndZone(Vector3f wldPosition, CHousingZone** zone) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (BoundBoxPointCheck(wldPosition, spnPtr->m_zoneBox.min, spnPtr->m_zoneBox.max)) {
			//in this zones visible range
			*zone = spnPtr;
			return spnPtr->GetContainerByWldPosition(wldPosition);
		} //end in this zones visible range
	}
	return NULL;
}

CHPlacementObj* CHousingZoneDB::GetHousePlacementByPosPlayerSpecial(Vector3f wldPosition, PLAYER_HANDLE playerHandle, const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (spnPtr->m_channel.channelIdEquals(channel))
			if (spnPtr->m_houseDatabase)
				for (POSITION posLoc2 = spnPtr->m_houseDatabase->GetHeadPosition(); posLoc2 != NULL;) {
					CHPlacementObj* houseOnTable = (CHPlacementObj*)spnPtr->m_houseDatabase->GetNext(posLoc2);
					if (houseOnTable)
						if (houseOnTable->m_playerHandle == playerHandle) {
							Vector3f max, min;
							max.x = houseOnTable->m_wldPosition.x + 200.0f;
							max.y = houseOnTable->m_wldPosition.y + 200.0f;
							max.z = houseOnTable->m_wldPosition.z + 200.0f;
							min.x = houseOnTable->m_wldPosition.x - 200.0f;
							min.y = houseOnTable->m_wldPosition.y - 200.0f;
							min.z = houseOnTable->m_wldPosition.z - 200.0f;
							if (BoundBoxPointCheck(wldPosition, min, max)) {
								//in this zones visible range
								return houseOnTable;
							} //end in this zones visible range
						}
				}
	}
	return NULL;
}

CHPlacementObj* CHousingZoneDB::GetHousePlacementByPosPlayer(Vector3f wldPosition, PLAYER_HANDLE playerHandle) {
	CHPlacementObj* tempRet = NULL;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (BoundBoxPointCheck(wldPosition, spnPtr->m_visibleBox.min, spnPtr->m_visibleBox.max)) {
			//in this zones visible range
			if (spnPtr->m_houseDatabase)
				tempRet = spnPtr->m_houseDatabase->GetByPlayerHandle(playerHandle);

			if (tempRet)
				return tempRet;
		} //end in this zones visible range
	}
	return NULL;
}

CHPlacementObj* CHousingZoneDB::GetHousePlacementByPosPlayer(Vector3f wldPosition, PLAYER_HANDLE playerHandle, ChannelId* retChannel) {
	CHPlacementObj* tempRet = NULL;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (BoundBoxPointCheck(wldPosition, spnPtr->m_visibleBox.min, spnPtr->m_visibleBox.max)) {
			//in this zones visible range
			if (spnPtr->m_houseDatabase) {
				tempRet = spnPtr->m_houseDatabase->GetByPlayerHandle(playerHandle);
				*retChannel = spnPtr->m_channel;
			}

			if (tempRet)
				return tempRet;
		} //end in this zones visible range
	}
	return NULL;
}

CHPlacementObj* CHousingZoneDB::GetHousePlacementByPos_area(Vector3f wldPosition) {
	//CHPlacementObjList * containerList = GetContainer(wldPosition);
	CHousingZone* zone = NULL;
	CHPlacementObjList* containerList = GetContainerAndZone(wldPosition, &zone);
	if (containerList) {
		CHPlacementObj* hsPtr = containerList->GetHouseByBox(wldPosition);
		if (hsPtr) {
			if (zone->m_houseDatabase)
				return zone->m_houseDatabase->GetByPlayerHandle(hsPtr->m_playerHandle);
		}
	}

	return NULL;
}

void CHousingZoneDB::RemoveHouseByPlayerHandle(PLAYER_HANDLE playerHandle) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		spnPtr->RemoveByPlayerHandle(playerHandle);
	}
}

CHPlacementObj* CHousingZoneDB::GetHouseByPlayerHandle(PLAYER_HANDLE playerHandle) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (spnPtr->m_houseDatabase) {
			CHPlacementObj* housePlPtr = spnPtr->m_houseDatabase->GetByPlayerHandle(playerHandle);
			if (housePlPtr)
				return housePlPtr;
		}
	}

	return NULL;
}

int CHousingZoneDB::RemoveHouseByPosition(Vector3f wldPosition, const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (spnPtr->m_channel.channelIdEquals(channel))
			if (BoundBoxPointCheck(wldPosition, spnPtr->m_visibleBox.min, spnPtr->m_visibleBox.max)) {
				//in this zones visible range
				if (spnPtr->m_houseDatabase)
					for (POSITION posLoc2 = spnPtr->m_houseDatabase->GetHeadPosition(); posLoc2 != NULL;) {
						CHPlacementObj* houseOnTable = (CHPlacementObj*)spnPtr->m_houseDatabase->GetNext(posLoc2);
						if (houseOnTable) {
							Vector3f max, min;
							max.x = houseOnTable->m_wldPosition.x + 200.0f;
							max.y = houseOnTable->m_wldPosition.y + 200.0f;
							max.z = houseOnTable->m_wldPosition.z + 200.0f;
							min.x = houseOnTable->m_wldPosition.x - 200.0f;
							min.y = houseOnTable->m_wldPosition.y - 200.0f;
							min.z = houseOnTable->m_wldPosition.z - 200.0f;
							if (BoundBoxPointCheck(wldPosition, min, max)) {
								//in this zones visible range
								int ret = houseOnTable->m_playerHandle;
								spnPtr->RemoveByPlayerHandle(houseOnTable->m_playerHandle);
								return ret;
							} //end in this zones visible range
						}
					}
			} //end in this zones visible range
	}
	return -1;
}

CHPlacementObj* CHousingZoneDB::GetHouseByPositionAndWldID(Vector3f wldPosition, const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* spnPtr = (CHousingZone*)GetNext(posLoc);
		if (spnPtr->m_channel.channelIdEquals(channel))
			if (BoundBoxPointCheck(wldPosition, spnPtr->m_visibleBox.min, spnPtr->m_visibleBox.max)) {
				//in this zones visible range
				if (spnPtr->m_houseDatabase)
					for (POSITION posLoc2 = spnPtr->m_houseDatabase->GetHeadPosition(); posLoc2 != NULL;) {
						CHPlacementObj* houseOnTable = (CHPlacementObj*)spnPtr->m_houseDatabase->GetNext(posLoc2);
						if (houseOnTable) {
							Vector3f max, min;
							max.x = houseOnTable->m_wldPosition.x + 200.0f;
							max.y = houseOnTable->m_wldPosition.y + 200.0f;
							max.z = houseOnTable->m_wldPosition.z + 200.0f;
							min.x = houseOnTable->m_wldPosition.x - 200.0f;
							min.y = houseOnTable->m_wldPosition.y - 200.0f;
							min.z = houseOnTable->m_wldPosition.z - 200.0f;
							if (BoundBoxPointCheck(wldPosition, min, max)) {
								//in this zones visible range
								return houseOnTable;
							} //end in this zones visible range
						}
					}
			} //end in this zones visible range
	}
	return NULL;
}

void CHousingZoneDB::Clone(CHousingZoneDB** clone) {
	CHousingZoneDB* copyLocal = new CHousingZoneDB();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CHousingZone* skPtr = (CHousingZone*)GetNext(posLoc);
		CHousingZone* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}

IMPLEMENT_SERIAL(CHousingZone, CObject, 0)
IMPLEMENT_SERIAL(CHousingZoneDB, CKEPObList, 0)
IMPLEMENT_SERIAL(CHPlacementObj, CObject, 0)
IMPLEMENT_SERIAL(CHPlacementObjList, CObList, 0)

BEGIN_GETSET_IMPL(CHPlacementObj, IDS_HPLACEMENTOBJ_NAME)
GETSET_RANGE(m_type, gsInt, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, TYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_playerHandle, gsInt, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, ACCOUNTBIND, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_wldPosition, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, WLDPOSITION)
GETSET(m_open, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, OPEN)
GETSET_RANGE(m_id, gsInt, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, ID, INT_MIN, INT_MAX)
GETSET(m_vaultInstalled, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, VAULTINSTALLED)
GETSET_RANGE(m_vaultItemCapacity, gsInt, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, VAULTITEMCAPACITY, INT_MIN, INT_MAX)
GETSET_RANGE(m_vaultCurrentCash, gsInt, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, VAULTCURRENTCASH, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_vaultInventory, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, VAULTINVENTORY)
GETSET_RANGE(m_storeItemCapacity, gsInt, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, STOREITEMCAPACITY, INT_MIN, INT_MAX)
GETSET_RANGE(m_storeCurrentCash, gsInt, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, STORECURRENTCASH, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_storeInventory, GS_FLAGS_DEFAULT, 0, 0, HPLACEMENTOBJ, STOREINVENTORY)
END_GETSET_IMPL

} // namespace KEP