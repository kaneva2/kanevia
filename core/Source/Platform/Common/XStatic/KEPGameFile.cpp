///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "kepgamefile.h"
#include "UnicodeMFCHelpers.h"

#include <log4cplus/logger.h>
using namespace log4cplus;

namespace KEP {

Logger logger(Logger::getInstance(L"EditorState"));

CKEPGameFile::CKEPGameFile(void) {
}
CKEPGameFile::CKEPGameFile(CKEPObList** ptr) {
	m_objectsSetPtr = new CKEPObListVector;
	m_objectsSetPtr->push_back(ptr);
}

CKEPGameFile::~CKEPGameFile(void) {
	delete m_objectsSetPtr;
}

BOOL CKEPGameFile::IsDirty() {
	BOOL retVal = FALSE;
	CKEPObListIterator itSetData;
	for (itSetData = m_objectsSetPtr->begin(); itSetData != m_objectsSetPtr->end(); itSetData++) {
		try {
			CKEPObList* listPtr = **(itSetData);
			if (listPtr && listPtr->IsDirty()) {
				retVal = TRUE;
				break;
			}
		} catch (...) {
			LOG4CPLUS_ERROR(logger, "invalid game object");
		}
	}
	return retVal;
}

std::string CKEPGameFile::GetFileName() {
	CKEPObListIterator itSetData;
	for (itSetData = m_objectsSetPtr->begin(); itSetData != m_objectsSetPtr->end(); itSetData++) {
		CKEPObList* listPtr = **(itSetData);
		if (listPtr->GetLastSavedPath() == "")
			continue;
		return (const char*)listPtr->GetLastSavedPath();
	}
	return std::string();
}

BOOL CKEPGameFile::SaveFile(const std::string& fname) {
	std::string fileName(fname);
	if (fileName == "") {
		//use current file name
		fileName = GetFileName();
	}
	CStringW CurDir = Utf8ToUtf16(PathAdd(FileHelper::GetCurrentDirectory(), "GameFiles")).c_str();

	CFileException exc;
	CFile fl;
	BOOL res = OpenCFile(fl, fileName.c_str(), CFile::modeCreate | CFile::modeWrite, &exc);
	if (res == FALSE) {
		LOG_EXCEPTION_NO_LOGGER(exc);
		return FALSE;
	}
	if (fl) {
		BEGIN_SERIALIZE_TRY
		CArchive the_out_Archive(&fl, CArchive::store);
		CKEPObListIterator itSetData;
		for (itSetData = m_objectsSetPtr->begin(); itSetData != m_objectsSetPtr->end(); itSetData++) {
			CKEPObList* listPtr = **(itSetData);
			the_out_Archive << listPtr;
		}
		the_out_Archive.Close();
		fl.Close();
		END_SERIALIZE_TRY_NO_LOGGER
	}
	//m_fileName = fileName;
	return TRUE;
}

} // namespace KEP