///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common/include/kepcommon.h"
#include "common/include/IMemSizeGadget.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CAdvancedVertexClass.h"
#include "CItemExpanseObj.h"
#include "CInventoryClass.h"
#include "CElementStatusClass.h"
#include "CGlobalInventoryClass.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

int CInventoryObj::MAX_STACK_LIMIT = 30000;

CInventoryObj::CInventoryObj() :
		m_inventoryType(IT_NORMAL) {
	m_dirty = INV_NEW;
	m_quantity = 0;
	m_armed = FALSE;
	m_itemData = NULL;
	m_optimizedSave = FALSE;
	m_chargeCurrent = 0;
}

CInventoryObj::~CInventoryObj() {
}

CInventoryObj::CInventoryObj(const CInventoryObj& rhs) :
		m_dirty(rhs.m_dirty), m_inventoryType(rhs.m_inventoryType), m_quantity(rhs.m_quantity), m_armed(rhs.m_armed), m_itemData(rhs.m_itemData), m_optimizedSave(rhs.m_optimizedSave), m_chargeCurrent(rhs.m_chargeCurrent) {
}

CInventoryObj& CInventoryObj::operator=(const CInventoryObj& rhs) {
	m_dirty = rhs.m_dirty;
	m_inventoryType = rhs.m_inventoryType;
	m_quantity = rhs.m_quantity;
	m_armed = rhs.m_armed;
	m_itemData = rhs.m_itemData;
	m_optimizedSave = rhs.m_optimizedSave;
	m_chargeCurrent = rhs.m_chargeCurrent;
	return *this;
}

std::string CInventoryObjList::toString() {
	std::stringstream ret;
	ret << "[" << GetSize() << "] elements:";
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		ret << "[" << invObj->toString() << "]" << std::endl;
	}
	return ret.str();
}

std::string CInventoryObj::toString() {
	std::stringstream ret;
	ret
		<< "dirty=" << m_dirty
		<< ";quantity:" << m_quantity
		<< ";armed=" << m_armed
		<< ";invtype=" << m_inventoryType
		<< ";chargecurrent=" << m_chargeCurrent
		<< ";item=[" << m_itemData << "][" << m_itemData->toString() << "]";
	return ret.str();
}

void CInventoryObj::SafeDelete() {
	// Points to global inventory object. Don't delete, just clear
	// our reference.
	m_itemData = NULL;
}

void CInventoryObj::Clone(CInventoryObj** copy) {
	CInventoryObj* copyLocal = new CInventoryObj();
	if (m_itemData)
		copyLocal->m_itemData = m_itemData;
	copyLocal->m_quantity = m_quantity;
	copyLocal->m_armed = m_armed;
	copyLocal->setCharge(m_chargeCurrent);
	copyLocal->setInventoryType(m_inventoryType);
	copyLocal->m_dirty = m_dirty;
	*copy = copyLocal;
}

// clears all dirty flags and empties the deletedItems list
void CInventoryObjList::ClearDirty() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		invObj->clearDirty();
	}

	if (m_deletedItems != 0) {
		m_deletedItems->SafeDelete();
		DELETE_AND_ZERO(m_deletedItems);
	}
	m_commitCount++;
}

bool CInventoryObjList::IsDirty() const {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->dirtyFlags() != 0)
			return true;
	}

	if (m_deletedItems != 0 && !m_deletedItems->IsEmpty())
		return true;

	return false;
}

void CInventoryObjList::SafeDelete() {
#ifdef DEPLOY_CLEANER
	CGlobalInventoryObjList* plist = CGlobalInventoryObjList::GetInstance();
	GILCleaner* pclean = plist ? plist->cleaner() : 0;
#else
#endif
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
#ifdef DEPLOY_CLEANER
		if (pclean)
			pclean->itemRemoved(invObj->m_itemData->m_globalID);
#endif
		invObj->SafeDelete();
		delete invObj;
		invObj = 0;
	}
	RemoveAll();
	if (m_deletedItems != 0) {
		m_deletedItems->SafeDelete();
		DELETE_AND_ZERO(m_deletedItems);
	}
}

CInventoryObj* CInventoryObjList::GetByGLID(int glid, short invTypes) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (glid == invObj->m_itemData->m_globalID && (invObj->inventoryType() & invTypes) != 0)
			return invObj;
	}
	return NULL;
}

BOOL CInventoryObjList::IsItemOfValuePresent() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_permanent)
			continue;
		return TRUE;
	}
	return FALSE;
}

int CInventoryObjList::GetInvenCountFilterUnseeable() {
	int count = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_permanent)
			continue;
		count++;
	}
	return count;
}

bool CInventoryObjList::CanTakeObjects(int maxCount, int itemCountToAdd) {
	int currentCount = GetInvenCountFilterUnseeable();
	bool canTake = ((currentCount + itemCountToAdd) <= maxCount);
	LogInfo(LogBool(canTake));
	return canTake;
}

BOOL CInventoryObjList::IsExpItemObjectPresent(int glid, int expID) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_globalID == glid) {
			if (invObj->m_itemData->m_expansiveCollection) {
				if (invObj->m_itemData->m_expansiveCollection->GetByMisc2(expID))
					return TRUE;
			}
		}
	}
	return FALSE;
}

void CInventoryObj::SerializeAlloc(CArchive& ar) {
	CGlobalInventoryObjList* globalDBRef = CGlobalInventoryObjList::GetInstance();

	if (globalDBRef != 0) {
		if (ar.IsStoring()) {
			ar << m_itemData->m_globalID << m_chargeCurrent << m_quantity << m_armed;
			// Throw away expansive collection. Retired.
			ar << -1;
		} else {
			int globalID;

			ar >> globalID >> m_chargeCurrent >> m_quantity >> m_armed;

			int expCount = 0;
			ar >> expCount;

			// Read but throw away expansive collection
			if (expCount > -1) {
				CCExpansiveObjList* local_expansiveCollection = new CCExpansiveObjList();
				local_expansiveCollection->SerializeAlloc(ar, expCount);
				delete local_expansiveCollection;
			}

			CGlobalInventoryObj* itemPtr = (CGlobalInventoryObj*)globalDBRef->GetByGLID(globalID);
			if (itemPtr != 0)
				m_itemData = itemPtr->m_itemData;
		}

		clearDirty(); // in or out, not dirty
	}
}

void CInventoryObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	char version = 0;
	CObject::Serialize(ar);

	CGlobalInventoryObjList* globalDBRef = CGlobalInventoryObjList::GetInstance();

	if (globalDBRef != 0) {
		if (ar.IsStoring()) {
			// now always optimized
			version = 1;
			ar << version << m_itemData->m_globalID << m_chargeCurrent << m_quantity << m_armed;
			// Throw away expansive collection. Retired.
			CCExpansiveObjList* local_expansiveCollection = 0;
			ar << local_expansiveCollection;
			m_optimizedSave = FALSE; //reset
		} else {
			int globalID;

			ar >> version;
			if (version == 0) {
				CItemObj* local_itemData = 0;
				ar >> local_itemData >> m_quantity >> m_armed >> reserved >> reserved;
				globalID = local_itemData->m_globalID;
				delete local_itemData; // CItemObj::SafeDelete now merged with destructor
			} else {
				//expand
				CCExpansiveObjList* local_expansiveCollection = 0;
				ar >> globalID >> m_chargeCurrent >> m_quantity >> m_armed >> local_expansiveCollection;
				// Throw away expansive collection. Retired.
				delete local_expansiveCollection;
			}

			CGlobalInventoryObj* itemPtr = (CGlobalInventoryObj*)globalDBRef->GetByGLID(globalID);
			if (itemPtr != 0) {
				m_itemData = itemPtr->m_itemData;
			} else {
				LogWarn("Inventory object with bad glid not found in GIL " << globalID);
			}

			m_optimizedSave = FALSE; //reset
		}

		clearDirty(); // in or out, not dirty
	}
}

void CInventoryObjList::Serialize(CArchive& ar) {
	CObList::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_totalWieght
		   << m_itemCountLimit
		   << m_wieghtLimit
		   << m_itemTotal;
	} else {
		ar >> m_totalWieght >> m_itemCountLimit >> m_wieghtLimit >> m_itemTotal;

		m_lastArmedWeapon = -1;

		// remove any nulls that may have been serialized in
		POSITION posLast;
		for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
			CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
			if (invObj->m_itemData == 0) {
				RemoveAt(posLast);
				delete invObj;
			}
		}
	}
}

CInventoryObjList::CInventoryObjList() :
		m_commitCount(0), m_pendingRefresh(false) {
	m_deletedItems = 0;
	m_totalWieght = 0;
	m_itemCountLimit = 0;
	m_wieghtLimit = 0;
	m_itemTotal = 0;
	m_lastArmedWeapon = -1;
}

void CInventoryObjList::Clone(CInventoryObjList** copy) {
	CInventoryObjList* copyLocal = new CInventoryObjList();
	copyLocal->m_totalWieght = m_totalWieght;
	copyLocal->m_itemCountLimit = m_itemCountLimit;
	copyLocal->m_wieghtLimit = m_wieghtLimit;
	copyLocal->m_itemTotal = m_itemTotal;
	copyLocal->m_lastArmedWeapon = m_lastArmedWeapon;

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		CInventoryObj* cpyPtr = NULL;
		invObj->Clone(&cpyPtr);
		copyLocal->AddTail(cpyPtr);
#ifdef DEPLOY_CLEANER
		CGlobalInventoryObjList::GetInstance()->cleaner()->itemAdded(cpyPtr->m_itemData->m_globalID);
#else
#endif
	}

	if (m_deletedItems != 0)
		m_deletedItems->Clone(&copyLocal->m_deletedItems);

	// leak fix if pass in prev list
	if (*copy) {
		(*copy)->SafeDelete();
		delete *copy;
	}
	*copy = copyLocal;
}

void CInventoryObjList::CloneMinimum(CInventoryObjList** copy) {
	CInventoryObjList* copyLocal = new CInventoryObjList();
	copyLocal->m_totalWieght = m_totalWieght;
	copyLocal->m_itemCountLimit = m_itemCountLimit;
	copyLocal->m_wieghtLimit = m_wieghtLimit;
	copyLocal->m_itemTotal = m_itemTotal;
	copyLocal->m_lastArmedWeapon = m_lastArmedWeapon;

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		CInventoryObj* cpyPtr = NULL;
		invObj->Clone(&cpyPtr);
		copyLocal->AddTail(cpyPtr);
	}

	if (m_deletedItems != 0)
		m_deletedItems->Clone(&copyLocal->m_deletedItems);

	*copy = copyLocal;
}

int CInventoryObjList::AddInventoryItem(int quantity, bool armed, const CItemObj* itemData, short inventoryType, bool markDirty) {
	CInventoryObj* existingSimilar = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* pCurrEntry = (CInventoryObj*)GetNext(posLoc);
		if (pCurrEntry->m_itemData->m_globalID == itemData->m_globalID && pCurrEntry->inventoryType() == inventoryType)
			existingSimilar = pCurrEntry;
	}

	if (existingSimilar) {
		//  existing similar object found
		int wasDirty = existingSimilar->dirtyFlags();
		if (existingSimilar->m_itemData->m_stackable) {
			// stackable
			// NOTE if gifting stackable item, only first gift marked so
			existingSimilar->incQty(quantity);
			if (!markDirty && (wasDirty & INV_QTY_UPDATED) == 0)
				existingSimilar->clearDirty();
		} else {
			// not stackable
			for (int j = 0; j < quantity; j++) {
				CInventoryObj* invObj = new CInventoryObj();
				// since already have one, not really NEW
				invObj->clearDirty(); // clear dirty, let setQty set dirty flag
				invObj->m_itemData = itemData;
				invObj->setQty(1);
				invObj->setArmed(armed);
				invObj->setInventoryType(inventoryType);
				if (!markDirty)
					invObj->clearDirty();
				AddTail(invObj);
			}
		}
	} else {
		// add new one
		if (itemData->m_stackable) {
			// stackable
			// dirty flag will be set to INV_NEW
			CInventoryObj* invObj = new CInventoryObj();
			invObj->m_itemData = itemData;
			invObj->setQty(quantity);
			invObj->setArmed(armed);
			invObj->setInventoryType(inventoryType);
			if (!markDirty)
				invObj->clearDirty();
			AddTail(invObj);
		} else {
			// not stackable
			for (int j = 0; j < quantity; j++) {
				// dirty flag will be set to INV_NEW
				CInventoryObj* invObj = new CInventoryObj();
				invObj->m_itemData = itemData;
				invObj->setQty(1);
				invObj->setArmed(armed);
				invObj->setInventoryType(inventoryType);
				if (!markDirty)
					invObj->clearDirty();
				AddTail(invObj);
			}
		}
	}
	return 1; // added item
}

//  It is incumbent upon client CInventoryObj to mark the object as dirty
CInventoryObj* CInventoryObjList::AddOneInventoryItem(CInventoryObj& inventoryObj) {
	// If the caller passed us an item that is non-stackable (i.e. cannot be aggregated) and a quantity
	// greater than 1...then go no further.
	const CItemObj* itemData = inventoryObj.m_itemData;
	if (!itemData->m_stackable && (inventoryObj.getQty() > 1)) {
		// TODO: Throw and exception here.  Thus forcing callers of this
		// method to implement their own non-stacking logic.  In the
		// meantime, log an error and return an invalid value.
		LogError("Attempt to to add non-stackable inventory item with quantity > 1");
		return 0;
	}

	CInventoryObj* existingSimilar = 0;
	CInventoryObj* ret = 0;

	// Find existing item like this on the players inventory list
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* candidate = (CInventoryObj*)GetNext(posLoc);
		if (candidate->m_itemData->m_globalID == itemData->m_globalID && candidate->inventoryType() == inventoryObj.inventoryType())
			existingSimilar = candidate;
	}

	if (existingSimilar) {
		if (!existingSimilar->m_itemData->m_stackable && (inventoryObj.getQty() > 1)) {
			// TODO: Throw and exception here.  Thus forcing callers of this
			// method to implement their own non-stacking logic.  In the
			// meantime, log an error and return an invalid value.
			LogError("Attempt to to add non-stackable inventory item with quantity > 1");
			return 0;
		}

		existingSimilar->incQty(inventoryObj.getQty());
		ret = existingSimilar;

	} else {
		AddTail(&inventoryObj);
#ifdef DEPLOY_CLEANER
		CGlobalInventoryObjList::GetInstance()->cleaner()->itemAdded(inventoryObj.m_itemData->m_globalID);
#endif
		ret = &inventoryObj;
	}

	return ret;
}

CInventoryObj* CInventoryObjList::AddOneInventoryItem(int quantity, bool armed, const CItemObj* itemData, short inventoryType, bool markDirty) {
	// If the caller passed us an item that is non-stackable (i.e. cannot be aggregated) and a quantity
	// greater than 1...then go no further.

	if (!itemData->m_stackable && (quantity > 1)) {
		// TODO: Throw and exception here.  Thus forcing callers of this
		// method to implement their own non-stacking logic.  In the
		// meantime, log an error and return an invalid value.
		LogError("Attempt to to add non-stackable inventory item with quantity > 1");
		return 0;
	}

	CInventoryObj* existingSimilar = 0;
	CInventoryObj* ret = 0;

	// Find existing item like this on the players inventory list
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_globalID == itemData->m_globalID && invObj->inventoryType() == inventoryType)
			existingSimilar = invObj;
	}

	if (existingSimilar) {
		if (!existingSimilar->m_itemData->m_stackable && (quantity > 1)) {
			// TODO: Throw and exception here.  Thus forcing callers of this
			// method to implement their own non-stacking logic.  In the
			// meantime, log an error and return an invalid value.
			LogError("Attempt to to add non-stackable inventory item with quantity > 1");
			return 0;
		}

		// We're about to update the quantity which will
		// cause the item's quantity attribute to be marked as dirty.
		int wasDirty = existingSimilar->dirtyFlags();
		existingSimilar->incQty(quantity);
		if (!markDirty && (wasDirty & INV_QTY_UPDATED) == 0)
			existingSimilar->clearDirty();
		ret = existingSimilar;

	} else {
		// add new stackable one
		// dirty flag will be set to INV_NEW
		CInventoryObj* invObj = new CInventoryObj();
		invObj->m_itemData = itemData;
		invObj->setQty(quantity);
		invObj->setArmed(armed);
		invObj->setInventoryType(inventoryType);
		if (!markDirty)
			invObj->clearDirty();
		AddTail(invObj);
		ret = invObj;
	}
	return ret;
}

BOOL CInventoryObjList::IsAnythingInInventoryValuable() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_permanent)
			continue;
		if (invObj->m_itemData->m_nonDrop)
			continue;
		if (invObj->m_itemData->m_marketCost > 0)
			return TRUE;
	}
	return FALSE; //no valuable
}

int CInventoryObjList::GetTotalValue() {
	int total = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_permanent)
			continue;
		if (invObj->m_itemData->m_nonDrop)
			continue;
		if (invObj->m_itemData->m_marketCost > 0)
			total += (invObj->m_itemData->m_marketCost * invObj->getQty());
	}
	return total; //no valuable
}

CInventoryObj* CInventoryObjList::VerifyInventory(int glid) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_globalID == glid)
			return invObj;
	}
	return NULL;
}

CInventoryObj* CInventoryObjList::CanTradeItem(int glid, int quantity) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (!invObj->isArmed() && invObj->m_itemData->m_globalID == glid && invObj->getQty() >= quantity && invObj->inventoryType() == IT_NORMAL)
			return invObj;
	}
	return NULL;
}

CInventoryObj* CInventoryObjList::VerifyInventory(int glid, int quantity) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_globalID == glid) {
			if (invObj->getQty() >= quantity)
				return invObj;
		}
	}
	return NULL;
}

BOOL CInventoryObjList::DoesArmedLocationIDExist(int id) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->isArmed()) {
			for (auto exID = invObj->m_itemData->m_exclusionGroupIDs.begin(); exID != invObj->m_itemData->m_exclusionGroupIDs.end(); exID++) {
				if (*exID == id)
					return TRUE;
			}
		}
	}
	return FALSE;
}

void CInventoryObjList::DeleteInvItem(CInventoryObj* deletedItem, int qtyDeleted) {
#ifdef DELETE_INV_ITEMS
	deletedItem->SafeDelete();
	delete deletedItem;
#else
	// if not stackable, are there more in the list?
	if (!deletedItem->m_itemData->m_stackable) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CInventoryObj* existingItem = (CInventoryObj*)GetNext(posLoc);
			if (deletedItem->m_itemData->m_globalID == existingItem->m_itemData->m_globalID && deletedItem->inventoryType() == existingItem->inventoryType()) {
				existingItem->incQty(0); // sets dirty
				deletedItem->SafeDelete();
				delete deletedItem;
				return; // don't put in deleted list
			}
		}
	}

	if (m_deletedItems == 0) {
		m_deletedItems = new CInventoryObjList();
		deletedItem->setQty(qtyDeleted);
		m_deletedItems->AddTail(deletedItem);
	} else {
		// only add if don't have one like this in deleted list
		for (POSITION posLoc = m_deletedItems->GetHeadPosition(); posLoc != NULL;) {
			CInventoryObj* existingItem = (CInventoryObj*)m_deletedItems->GetNext(posLoc);
			if (deletedItem->m_itemData->m_globalID == existingItem->m_itemData->m_globalID && deletedItem->inventoryType() == existingItem->inventoryType()) {
				existingItem->incQty(qtyDeleted);
				deletedItem->SafeDelete();
				delete deletedItem;
				return; // don't put in deleted list
			}
		}
		m_deletedItems->AddTail(deletedItem);
	}
#endif
}

int CInventoryObjList::CombineToExpObj(int glid_combineItem, int glid_targetObject) {
	// not implemented
	jsAssert(false);
	return 0;
}

void CInventoryObjList::SetOptimizedSave(BOOL optimized) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		invObj->m_optimizedSave = optimized;
	}
}

int CInventoryObjList::GetArmourRating(int damageChannel, BOOL forVisualOnly, int attackSpecified) {
	int armourRating = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (!invObj->m_itemData)
			continue;

		if (forVisualOnly == TRUE) {
			if (invObj->m_itemData->m_defensesSpecific != -1)
				continue;
		}

		if (invObj->m_itemData->m_defensesSpecific != -1) {
			if (attackSpecified != invObj->m_itemData->m_defensesSpecific)
				continue;
		}

		if (invObj->m_itemData->m_elementDefenses) {
			if (invObj->isArmed()) {
				for (POSITION defPos = invObj->m_itemData->m_elementDefenses->GetHeadPosition(); defPos != NULL;) {
					CElementStatusObj* elementStatusPtr = (CElementStatusObj*)invObj->m_itemData->m_elementDefenses->GetNext(defPos);
					if (damageChannel == elementStatusPtr->m_elementIndex)
						armourRating += elementStatusPtr->m_currentValue;
				}
			}
		}
	}

	if (armourRating > 90)
		armourRating = 90;

	return armourRating;
}

CInventoryObj* CInventoryObjList::GetInvenItemByLocationID(int id) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		for (auto exID = invObj->m_itemData->m_exclusionGroupIDs.begin(); exID != invObj->m_itemData->m_exclusionGroupIDs.end(); exID++) {
			if (*exID == id)
				return invObj;
		}
	}
	return NULL;
}

CInventoryObj* CInventoryObjList::GetInvenItemByLocationIDArmable(int id, CStatObjList& playersStats) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		for (auto exID = invObj->m_itemData->m_exclusionGroupIDs.begin(); exID != invObj->m_itemData->m_exclusionGroupIDs.end(); exID++) {
			if (*exID == id) {
				if (invObj->m_itemData->m_requiredStats && invObj->m_itemData->m_requiredStats->MeetsRequirements(playersStats))
					return invObj;
			}
		}
	}
	return NULL;
}

CInventoryObj* CInventoryObjList::GetArmedInvenItemByLocationID(int id) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->isArmed()) {
			for (auto exID = invObj->m_itemData->m_exclusionGroupIDs.begin(); exID != invObj->m_itemData->m_exclusionGroupIDs.end(); exID++) {
				if (*exID == id)
					return invObj;
			}
		}
	}
	return NULL;
}

BOOL CInventoryObjList::UseIfKeyInInventory(int keyID) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);
		if (invObj->m_itemData->m_keyID == keyID) {
			if (invObj->m_itemData->m_destroyOnUse) {
				invObj->decQty();
				if (invObj->getQty() < 1) {
					RemoveAt(posLast);
					DeleteInvItem(invObj, 1);
					invObj = 0;
				}
			}
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CInventoryObjList::RemoveRecentAddByGlid(int glid, int quantity, bool mustBeUnarmed, short invTypes) {
	if (quantity == 0)
		return TRUE;

	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);

		if (glid == invObj->m_itemData->m_globalID && (!mustBeUnarmed || !invObj->isArmed()) && (invObj->inventoryType() & invTypes) != 0) {
			// trying to take more than we have
			if (quantity > invObj->getQty())
				return FALSE;

			// <0 = removing all
			if (quantity < 0)
				quantity = invObj->getQty();

			invObj->decQty(quantity);

			if (invObj->getQty() < 1) {
				RemoveAt(posLast);
				invObj->SafeDelete();
				delete invObj;
				invObj = 0;
			}

			return TRUE;
		}
	}

	return FALSE;
}

// if qty < 0 removed all
BOOL CInventoryObjList::DeleteByGLID(int glid, int quantity, bool mustBeUnarmed, short invTypes, bool markDirty) {
	return TransferInventoryObjsTo(NULL, glid, quantity, mustBeUnarmed, invTypes, markDirty);
}

BOOL CInventoryObjList::TransferInventoryObjsTo(CInventoryObjList* otherList, int glid, int quantity, bool mustBeUnarmed, short invTypes, bool markDirty) {
	if (quantity == 0)
		return TRUE;

	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CInventoryObj* invObj = (CInventoryObj*)GetNext(posLoc);

		if (glid == invObj->m_itemData->m_globalID && (!mustBeUnarmed || !invObj->isArmed()) && (invObj->inventoryType() & invTypes) != 0) {
			// trying to take more than we have
			if (quantity > invObj->getQty())
				return FALSE;

			int wasDirty = invObj->dirtyFlags();

			// <0 = removing all
			if (quantity < 0)
				quantity = invObj->getQty();

			invObj->decQty(quantity);
			if (otherList)
				otherList->AddInventoryItem(quantity, FALSE, invObj->m_itemData, invObj->inventoryType(), markDirty);

			if (invObj->getQty() < 1) {
				RemoveAt(posLast);
				if (markDirty) {
					DeleteInvItem(invObj, quantity);
				} else {
					invObj->SafeDelete();
					delete invObj;
				}
				invObj = 0;
			} else if (!markDirty && (wasDirty & INV_QTY_UPDATED) == 0) {
				invObj->clearDirty();
			}
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CInventoryObjList::ReorderInventory(int objectToMoveIndex, int targetIndex, int filterType) {
	if (objectToMoveIndex == targetIndex)
		return FALSE;

	if (objectToMoveIndex < 0 || targetIndex < 0)
		return FALSE;

	POSITION posObjectToMove = NULL;
	int trace = 0;
	POSITION posLoc;
	for (posLoc = GetHeadPosition(); (posObjectToMove = posLoc) != NULL;) {
		CInventoryObj* invPtrTemp = (CInventoryObj*)GetNext(posLoc);
		if (filterType == 1) {
			if (invPtrTemp->m_itemData) {
				if (invPtrTemp->m_itemData->m_permanent == TRUE)
					continue;
			}
		}

		if (trace == objectToMoveIndex)
			break;
		trace++;
	}

	if (posObjectToMove == NULL)
		return FALSE;

	CInventoryObj* invObj = (CInventoryObj*)GetAt(posObjectToMove);
	RemoveAt(posObjectToMove);

	if (targetIndex == 0) {
		AddHead(invObj);
		return TRUE;
	} else {
		trace = 0;
		for (posLoc = GetHeadPosition(); posLoc != NULL;) {
			CInventoryObj* invPtrTemp = (CInventoryObj*)GetAt(posLoc);
			if (filterType == 1) {
				if (invPtrTemp->m_itemData) {
					if (invPtrTemp->m_itemData->m_permanent == TRUE) {
						GetNext(posLoc);
						continue;
					}
				}
			}
			if (trace == targetIndex) {
				InsertBefore(posLoc, invObj);
				return TRUE;
			}
			GetNext(posLoc);
			trace++;
		}
	}
	AddTail(invObj);
	return TRUE;
}

void CInventoryObjList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CInventoryObj* temoObj = (CInventoryObj*)GetNext(posLoc);
			temoObj->SerializeAlloc(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			CInventoryObj* temoObj = new CInventoryObj();
			temoObj->SerializeAlloc(ar);
			AddTail(temoObj);
		}
	}
}

BOOL CInventoryObjList::GenerateMsgBlockItemToClient(BOOL includeArmed, MSG_BLOCK_ITEM_TO_CLIENT** dataGLIDBlock, int& itemCount, int& msgSize, BOOL filterNonTransferable, int useTypeId) {
	itemCount = 0;
	POSITION posLoc;
	for (posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* pIO = (CInventoryObj*)GetNext(posLoc);
		if (!pIO || (useTypeId > 0 && pIO->m_itemData->m_useType != (USE_TYPE)useTypeId) || pIO->m_itemData->m_permanent || (filterNonTransferable && pIO->m_itemData->m_nonTransferable) || (pIO->m_itemData->m_disarmable == FALSE && pIO->isArmed()) || (!includeArmed && pIO->isArmed()))
			continue;

		itemCount++;
	}

	if (itemCount == 0)
		return FALSE;

	msgSize = MSG_BLOCK_ITEM_TO_CLIENT::computeSize(itemCount);
	MSG_BLOCK_ITEM_TO_CLIENT* pGLIDBlock = (MSG_BLOCK_ITEM_TO_CLIENT*)(new BYTE[msgSize]);

	pGLIDBlock->m_header.SetCategory(MSG_CAT::BLOCK_ITEM_TO_CLIENT);
	pGLIDBlock->m_header.m_miscInt = 0;

	int itemIndex = 0;
	for (posLoc = GetHeadPosition(); posLoc != NULL;) {
		CInventoryObj* pIO = (CInventoryObj*)GetNext(posLoc);
		if (!pIO || (useTypeId > 0 && pIO->m_itemData->m_useType != (USE_TYPE)useTypeId) || pIO->m_itemData->m_permanent || (filterNonTransferable && pIO->m_itemData->m_nonTransferable) || (pIO->m_itemData->m_disarmable == FALSE && pIO->isArmed()) || (!includeArmed && pIO->isArmed()))
			continue;

		pGLIDBlock->m_data[itemIndex].m_GLID = pIO->m_itemData->m_globalID;
		pGLIDBlock->m_data[itemIndex].m_quanity = pIO->getQty();

		// hacky way to send down two bools, but otherwise ITEMDATA_STRUCT
		// must change which is used all over the place
		if (pIO->isArmed())
			pGLIDBlock->m_data[itemIndex].m_quanity = -pGLIDBlock->m_data[itemIndex].m_quanity;

		if (pIO->inventoryType() == IT_GIFT)
			pGLIDBlock->m_data[itemIndex].m_GLID = -pGLIDBlock->m_data[itemIndex].m_GLID;

		itemIndex++;
	}

	*dataGLIDBlock = pGLIDBlock;
	return TRUE;
}

void CInventoryObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CInventoryObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CInventoryObj* pCInventoryObj = static_cast<const CInventoryObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCInventoryObj);
		}
	}
}

IMPLEMENT_SERIAL(CInventoryObj, CObject, 0)
IMPLEMENT_SERIAL(CInventoryObjList, CObList, 0)

bool CInventoryObj::getInnerXml(std::string& xml, gsMetaDatum& md, void* value, char* workingString) {
	bool ret = false;
	if (md.GetDescId() == IDS_INVENTORYOBJ_ITEMDATA) {
		std::string itemName(this->m_itemData->m_itemName);
		escapeXml(itemName);
		xml += itemName;
		ret = true;
	}
	return ret;
}

void CInventoryObj::getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s) {
	gsMetaDatum& md = (*m_gsMetaData)[i];

	// do our custom one
	if (md.GetDescId() == IDS_INVENTORYOBJ_ITEMDATA) {
		_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
			"<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\"",
			loadStr(md.GetDescId()).c_str(),
			md.GetType(),
			md.GetFlags(),
			md.GetDescId());
		if (filter == 0 || filter->allAttributes())
			_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
				"%s help=\"%s\" minValue=\"%f\" maxValue=\"%f\" indent=\"%d\" groupId=\"%d\"",
				s,
				loadStr(md.GetHelpId()).c_str(),
				md.GetMinValue(), md.GetMaxValue(),
				md.GetIndent(),
				md.GetGroupTitleId());
		strncat_s(s, TEMP_STR_SIZE, ">", TEMP_STR_SIZE);
		xml += s;

		if (m_itemData) {
			std::string itemName(this->m_itemData->m_itemName);
			escapeXml(itemName);
			xml += itemName;
		}
		xml += "</GetSetRec>\n";
	} else {
		GetSet::getItemXml(i, ourObjectId, depth, filter, myMd, xml, s);
	}
}

BEGIN_GETSET_IMPL(CInventoryObj, IDS_INVENTORYOBJ_NAME)
GETSET_CUSTOM(0, gsListOfValues, GS_FLAGS_DEFAULT | GS_FLAG_ADDONLY, 0, 0, INVENTORYOBJ, ITEMDATALIST, 0, 0);
GETSET(m_armed, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, INVENTORYOBJ, ARMED)
GETSET_RANGE(m_quantity, gsInt, GS_FLAG_EXPOSE, 0, 0, INVENTORYOBJ, QUANITY, INT_MIN, INT_MAX)
GETSET_OBJ_PTR_CONST(m_itemData, 0, 0, INVENTORYOBJ, ITEMDATA, CItemObj)
GETSET_ACTION(this, GS_FLAGS_DEFAULT, 0, 0, ADMIN, DELETE)
END_GETSET_IMPL

} // namespace KEP