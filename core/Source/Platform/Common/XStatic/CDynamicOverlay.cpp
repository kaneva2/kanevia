///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CDynamicOverlay.h"

namespace KEP {

CDynamicOverlayObj::CDynamicOverlayObj() {
	m_screenPos.x = 0.0f;
	m_screenPos.y = 0.0f;
	m_screenPos.z = 0.0f;
	m_multiplier = 1.0f;
}

void CDynamicOverlayList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDynamicOverlayObj* spnPtr = (CDynamicOverlayObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

} // namespace KEP