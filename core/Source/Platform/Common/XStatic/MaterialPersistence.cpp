///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MaterialPersistence.h"
#include "CMovementObj.h"
#include "CSkeletonObject.h"
#include "SkeletonConfiguration.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "CBoneClass.h"
#include "RuntimeSkeleton.h"
#include "CEXMeshClass.h"
#include "TextureDatabase.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

// Overloaded equal operators to compare if two data structures equal. The 'compare' function is reserved for returning 3-way results (>, <, =).
static bool operator==(const FULLMATERIALCOLOR& mat1, const FULLMATERIALCOLOR& mat2);
static bool operator==(const FULLMATERIALFLAGS& flags1, const FULLMATERIALFLAGS& flags2);
static bool operator==(const FULLTEXTUREASSIGNMENT& texAssign1, const FULLTEXTUREASSIGNMENT& texAssign2);
static bool operator==(const MATERIALENTRY& entry1, const MATERIALENTRY& entry2);

MaterialCompressor::CompressorMap MaterialCompressor::m_compressorMap;

MaterialCompressor::MaterialCompressor() :
		m_compressed(false), m_v1Compatible(false), m_texFileNameIsConcrete(true) {
	ZeroMemory(&m_fileHeader, sizeof(COMPRESSIONHEADER));
}

MaterialCompressor::~MaterialCompressor() {
	for (unsigned int i = 0; i < m_fileNameTable.size(); i++) {
		delete m_fileNameTable[i];
	}
	for (unsigned int i = 0; i < m_materialNameTable.size(); i++) {
		delete m_materialNameTable[i];
	}
	for (unsigned int i = 0; i < m_fxBindStringTable.size(); i++) {
		delete m_fxBindStringTable[i];
	}
	// Note: legacyMat is shared memory and should not be freed in compressor destructor
}

// static function
MaterialCompressor* MaterialCompressor::getMaterialCompressor(const std::string& name) {
	MaterialCompressor* matCompressor = NULL;
	EnterCriticalSection(&m_compressorMap.m_cs);
	CompressorMap::MatCompressorMap::iterator itr = m_compressorMap.m_matCompMap.find(name);
	if (itr != m_compressorMap.m_matCompMap.end()) {
		matCompressor = (*itr).second;
	} else {
		matCompressor = new MaterialCompressor();
		m_compressorMap.m_matCompMap.insert(CompressorMap::StringCompressorPair(name, matCompressor));
	}
	LeaveCriticalSection(&m_compressorMap.m_cs);
	return matCompressor;
}

// static function
void MaterialCompressor::deleteMaterialCompressor(const std::string& name) {
	MaterialCompressor* matCompressor = NULL;
	EnterCriticalSection(&m_compressorMap.m_cs);
	CompressorMap::MatCompressorMap::iterator itr = m_compressorMap.m_matCompMap.find(name);
	if (itr != m_compressorMap.m_matCompMap.end()) {
		matCompressor = (*itr).second;
		m_compressorMap.m_matCompMap.erase(itr);
	}
	LeaveCriticalSection(&m_compressorMap.m_cs);
	delete matCompressor;
}

bool MaterialCompressor::addMaterial(CMaterialObject* legacyMat) {
	if (m_compressed) {
		// You cannot add a material to an already compressed lib.
		return false;
	}
	if (!legacyMat) {
		// This is a valid case in many instances material is NULL.  Handle by calling '-1' the
		// index for NULL materials.
		return true;
	} else {
		FULLMATERIALCOLOR baseColor;
		FULLMATERIALCOLOR modColor;
		FULLMATERIALFLAGS flags;
		FULLTEXTUREASSIGNMENT texAssign[NUM_CMATERIAL_TEXTURES];

		getBaseColor(legacyMat, &baseColor);
		getModColor(legacyMat, &modColor);
		getFlags(legacyMat, &flags);
		addColorNoDupes(&baseColor);
		addColorNoDupes(&modColor);
		addFlagsNoDupes(&flags);
		for (int i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
			getTextureAssignment(i, legacyMat, &(texAssign[i]));
			if (texAssign[i].filenameIndex != NO_TEXTURE_ASSIGNMENT) {
				addTexAssignNoDupes(&texAssign[i]);
			}
		}
		legacyMat->m_myCompressor = this;
		legacyMat->m_myCompressionIndex = CMATERIAL_UNCOMPRESSED;
		m_legacyMaterials.push_back(legacyMat);
	}
	return true;
}

bool MaterialCompressor::addMaterials(CMovementObjList* movList) {
	if (m_compressed) {
		return false;
	}
	for (POSITION movPos = movList->GetHeadPosition(); movPos != NULL;) {
		CMovementObj* movOb = (CMovementObj*)movList->GetNext(movPos);
		addMaterials(movOb->getSkeleton(), movOb->getSkeletonConfig());
	}
	return true;
}

inline BOOL getEnable(const CMaterialObject* mat, int index) {
	if (index >= 0 && (unsigned int)index < MAX_UV_SETS) {
		return mat->m_enableSet[index];
	}

	return false;
}

bool MaterialCompressor::addMaterials(const RuntimeSkeleton* skeleton, const SkeletonConfiguration* cfg) {
	if (m_compressed) {
		return false;
	}
	if (skeleton == NULL || cfg == NULL) {
		// There may be valid instances of "NULL" skeleton.
		return true;
	}

	if (cfg->m_meshSlots) {
		for (POSITION alPos = cfg->m_meshSlots->GetHeadPosition(); alPos != NULL;) {
			CAlterUnitDBObject* altPtr = (CAlterUnitDBObject*)cfg->m_meshSlots->GetNext(alPos);
			for (POSITION cfgPos = altPtr->m_configurations->GetHeadPosition(); cfgPos != NULL;) {
				CDeformableMesh* dMesh = (CDeformableMesh*)altPtr->m_configurations->GetNext(cfgPos);
				if (dMesh->m_supportedMaterials) {
					for (POSITION matPos = dMesh->m_supportedMaterials->GetHeadPosition(); matPos != NULL;) {
						CMaterialObject* mat = (CMaterialObject*)dMesh->m_supportedMaterials->GetNext(matPos);
						addMaterial(mat);
					}
				}
			}
		}
	}
	for (POSITION bnPos = skeleton->getBoneList()->GetHeadPosition(); bnPos != NULL;) {
		auto pBone = static_cast<const CBoneObject*>(skeleton->getBoneList()->GetNext(bnPos));
		for (auto itMesh = pBone->m_rigidMeshes.begin(); itMesh != pBone->m_rigidMeshes.end(); ++itMesh) {
			for (POSITION visPos = (*itMesh)->m_lodChain->GetHeadPosition(); visPos != NULL;) {
				auto visObj = static_cast<const CHierarchyVisObj*>((*itMesh)->m_lodChain->GetNext(visPos));
				if (visObj->m_mesh) {
					addMaterial(visObj->m_mesh->m_materialObject);
				}
			}
		}
	}
	return true;
}

inline int addStringNoDupes(const CStringA* fileName, std::vector<CStringA*>& fileNames) {
	for (unsigned int i = 0; i < fileNames.size(); i++) {
		if (fileNames[i]->CompareNoCase(*fileName) == 0) {
			return i;
		}
	}
	CStringA* toAdd = new CStringA(*fileName);
	fileNames.push_back(toAdd);
	return fileNames.size() - 1;
}

void MaterialCompressor::compress() {
	m_compressed = true;
	// we do lots of complex sorting to minimize the amount of data that must be written to disk.
	// in general its largest size to smallest.  The values in the header tell us how many of each
	// type will go into the archive.
	colorSort();
	flagSort();
	texAssignSort();
	for (unsigned int i = 0; i < m_legacyMaterials.size(); i++) {
		FULLMATERIALCOLOR matColor, modColor;
		FULLMATERIALFLAGS flags;
		FULLTEXTUREASSIGNMENT assign[NUM_CMATERIAL_TEXTURES];
		MATERIALENTRY indexedEntry;

		getBaseColor(m_legacyMaterials[i], &matColor);
		getModColor(m_legacyMaterials[i], &modColor);
		getFlags(m_legacyMaterials[i], &flags);
		for (int j = 0; j < NUM_CMATERIAL_TEXTURES; j++) {
			CMaterialObject* mat = m_legacyMaterials[i];
			getTextureAssignment(j, mat, &(assign[j]));
			indexedEntry.textureAssignments[j] = addTexAssignNoDupes(&(assign[j]));
		}
		indexedEntry.colorTableIndexUse = addColorNoDupes(&matColor);
		indexedEntry.colorTableIndexMod = addColorNoDupes(&modColor);
		indexedEntry.flagsIndex = addFlagsNoDupes(&flags);
		m_legacyMaterials[i]->m_compressionColorTableIndexUse = indexedEntry.colorTableIndexUse;
		m_legacyMaterials[i]->m_compressionColorTableIndexMod = indexedEntry.colorTableIndexMod;
		m_legacyMaterials[i]->m_compressionFlagsIndex = indexedEntry.flagsIndex;
		for (int j = 0; j < NUM_CMATERIAL_TEXTURES; j++) {
			m_legacyMaterials[i]->m_compressionTextureAssignments[j] = indexedEntry.textureAssignments[j];
		}
		addIndexedEntryNoDupes(&indexedEntry, false);
	}
	matEntrySort();
	for (unsigned int i = 0; i < m_legacyMaterials.size(); i++) {
		MATERIALENTRY indexedEntry;
		indexedEntry.colorTableIndexMod = m_legacyMaterials[i]->m_compressionColorTableIndexMod;
		indexedEntry.colorTableIndexUse = m_legacyMaterials[i]->m_compressionColorTableIndexUse;
		indexedEntry.flagsIndex = m_legacyMaterials[i]->m_compressionFlagsIndex;
		for (int j = 0; j < NUM_CMATERIAL_TEXTURES; j++) {
			indexedEntry.textureAssignments[j] = m_legacyMaterials[i]->m_compressionTextureAssignments[j];
		}
		m_legacyMaterials[i]->m_myCompressionIndex = addIndexedEntryNoDupes(&indexedEntry, true);
	}
	//assert(verifyCompression()); // testing only very slow.
}

void MaterialCompressor::writeToArchive(CArchive& ar) {
	unsigned char code = material_archive_code;
	ar << code;
	materialCompressionWriteV2(ar);
}

// V2 uses same format as V1, only data content fix. So basically this is the same function as V1
void MaterialCompressor::materialCompressionWriteV2(CArchive& ar) {
	unsigned char version = 2;
	assert(m_compressed);
	m_fileHeader.fileNameCount = m_fileNameTable.size();
	m_fileHeader.materialNameCount = m_materialNameTable.size();
	m_fileHeader.bindStringCount = m_fxBindStringTable.size();
	ar << version; // version 2
	ar.Write(&m_fileHeader, sizeof(COMPRESSIONHEADER));
	for (int i = 0; i < m_fileHeader.fileNameCount; i++) {
		ar << *(m_fileNameTable[i]);
	}
	for (int i = 0; i < m_fileHeader.materialNameCount; i++) {
		ar << *(m_materialNameTable[i]);
	}
	for (int i = 0; i < m_fileHeader.bindStringCount; i++) {
		ar << *(m_fxBindStringTable[i]);
	}
	int start = 0;
	for (int i = 0; i < m_fileHeader.fullColorCount; i++) {
		ar.Write(&(m_colorTable[i]), sizeof(FULLMATERIALCOLOR));
	}
	start = m_fileHeader.fullColorCount;
	for (int i = start; i < start + m_fileHeader.specularColorCount; i++) {
		SPECULARMATERIALCOLOR spec;
		convertColorDown(&m_colorTable[i], &spec);
		ar.Write(&spec, sizeof(SPECULARMATERIALCOLOR));
	}
	start = m_fileHeader.fullColorCount + m_fileHeader.specularColorCount;
	for (int i = start; i < start + m_fileHeader.emissiveColorCount; i++) {
		EMISSIVEMATERIALCOLOR emis;
		convertColorDown(&m_colorTable[i], &emis);
		ar.Write(&emis, sizeof(EMISSIVEMATERIALCOLOR));
	}
	start = m_fileHeader.fullColorCount + m_fileHeader.specularColorCount + m_fileHeader.emissiveColorCount;
	for (int i = start; i < start + m_fileHeader.plainColorCount; i++) {
		PLAINMATERIALCOLOR plain;
		convertColorDown(&m_colorTable[i], &plain);
		ar.Write(&plain, sizeof(PLAINMATERIALCOLOR));
	}
	start = m_fileHeader.fullColorCount + m_fileHeader.specularColorCount + m_fileHeader.emissiveColorCount + m_fileHeader.plainColorCount;
	for (int i = start; i < start + m_fileHeader.bareColorCount; i++) {
		BAREMATERIALCOLOR bare;
		convertColorDown(&m_colorTable[i], &bare);
		ar.Write(&bare, sizeof(BAREMATERIALCOLOR));
	}
	for (int i = 0; i < m_fileHeader.fullAssignCount; i++) {
		ar.Write(&(m_assignmentTable[i]), sizeof(FULLTEXTUREASSIGNMENT));
	}
	start = m_fileHeader.fullAssignCount;
	for (int i = start; i < start + m_fileHeader.blendedAssignCount; i++) {
		BLENDEDTEXTUREASSIGNMENT assign;
		convertAssignmentDown(&m_assignmentTable[i], &assign);
		ar.Write(&assign, sizeof(BLENDEDTEXTUREASSIGNMENT));
	}
	start = m_fileHeader.fullAssignCount + m_fileHeader.blendedAssignCount;
	for (int i = start; i < start + m_fileHeader.bareAssignCount; i++) {
		BARETEXTUREASSIGNMENT assign;
		convertAssignmentDown(&m_assignmentTable[i], &assign);
		ar.Write(&assign, sizeof(BARETEXTUREASSIGNMENT));
	}
	for (int i = 0; i < m_fileHeader.flagsCount; i++) {
		ar.Write(&(m_flagsTable[i]), sizeof(FULLMATERIALFLAGS));
	}
	start = 0;
	// ok this is tricky
	// i is the number of texture assignments for the material starting with '0' for no assignments used
	// and NUM_CMATERIAL_TEXTURES for all assignments used
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES + 1; i++) {
		// j is the index into compressedmaterials.  start is the first material of that size
		for (int j = start; j < start + m_fileHeader.m_compressionTextureCounts[i]; j++) {
			ar << m_compressedMaterials[j].colorTableIndexMod;
			ar << m_compressedMaterials[j].colorTableIndexUse;
			ar << m_compressedMaterials[j].flagsIndex;
			// i is the num assignments this mat has.  k loops through the i assignments
			for (int k = 0; k < i; k++) {
				ar << m_compressedMaterials[j].textureAssignments[k];
			}
		}
		// set 'start' to the last material we saved then increase i.
		start = start + m_fileHeader.m_compressionTextureCounts[i];
	}
}

// V2 uses same format as V1, only data content fix. So basically this is the same function as V1
void MaterialCompressor::materialCompressionReadV2(CArchive& ar) {
	ar.Read(&m_fileHeader, sizeof(COMPRESSIONHEADER));
	for (int i = 0; i < m_fileHeader.fileNameCount; i++) {
		CStringA* fileName = new CStringA();
		ar >> *fileName;
		m_fileNameTable.push_back(fileName);
	}
	for (int i = 0; i < m_fileHeader.materialNameCount; i++) {
		CStringA* matName = new CStringA();
		ar >> *matName;
		m_materialNameTable.push_back(matName);
	}
	for (int i = 0; i < m_fileHeader.bindStringCount; i++) {
		CStringA* bindString = new CStringA();
		ar >> *bindString;
		m_fxBindStringTable.push_back(bindString);
	}
	for (int i = 0; i < m_fileHeader.fullColorCount; i++) {
		FULLMATERIALCOLOR fullColor;
		ar.Read(&fullColor, sizeof(FULLMATERIALCOLOR));
		m_colorTable.push_back(fullColor);
	}
	for (int i = 0; i < m_fileHeader.specularColorCount; i++) {
		SPECULARMATERIALCOLOR specColor;
		FULLMATERIALCOLOR fullColor;
		ar.Read(&specColor, sizeof(SPECULARMATERIALCOLOR));
		convertColorUp(&specColor, &fullColor);
		m_colorTable.push_back(fullColor);
	}
	for (int i = 0; i < m_fileHeader.emissiveColorCount; i++) {
		EMISSIVEMATERIALCOLOR emis;
		FULLMATERIALCOLOR fullColor;
		ar.Read(&emis, sizeof(EMISSIVEMATERIALCOLOR));
		convertColorUp(&emis, &fullColor);
		m_colorTable.push_back(fullColor);
	}
	for (int i = 0; i < m_fileHeader.plainColorCount; i++) {
		PLAINMATERIALCOLOR plain;
		FULLMATERIALCOLOR fullColor;
		ar.Read(&plain, sizeof(PLAINMATERIALCOLOR));
		convertColorUp(&plain, &fullColor);
		m_colorTable.push_back(fullColor);
	}
	for (int i = 0; i < m_fileHeader.bareColorCount; i++) {
		BAREMATERIALCOLOR bare;
		FULLMATERIALCOLOR fullColor;
		ar.Read(&bare, sizeof(BAREMATERIALCOLOR));
		convertColorUp(&bare, &fullColor);
		m_colorTable.push_back(fullColor);
	}
	for (int i = 0; i < m_fileHeader.fullAssignCount; i++) {
		FULLTEXTUREASSIGNMENT fullAssign;
		ar.Read(&fullAssign, sizeof(FULLTEXTUREASSIGNMENT));
		m_assignmentTable.push_back(fullAssign);
	}
	for (int i = 0; i < m_fileHeader.blendedAssignCount; i++) {
		BLENDEDTEXTUREASSIGNMENT blendAssign;
		FULLTEXTUREASSIGNMENT fullAssign;
		ar.Read(&blendAssign, sizeof(BLENDEDTEXTUREASSIGNMENT));
		convertAssignmentUp(&blendAssign, &fullAssign);
		m_assignmentTable.push_back(fullAssign);
	}
	for (int i = 0; i < m_fileHeader.bareAssignCount; i++) {
		BARETEXTUREASSIGNMENT bareAssign;
		FULLTEXTUREASSIGNMENT fullAssign;
		ar.Read(&bareAssign, sizeof(BARETEXTUREASSIGNMENT));
		convertAssignmentUp(&bareAssign, &fullAssign);
		m_assignmentTable.push_back(fullAssign);
	}
	for (int i = 0; i < m_fileHeader.flagsCount; i++) {
		FULLMATERIALFLAGS fl;
		ar.Read(&fl, sizeof(FULLMATERIALFLAGS));
		m_flagsTable.push_back(fl);
	}
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES + 1; i++) {
		for (int j = 0; j < m_fileHeader.m_compressionTextureCounts[i]; j++) {
			MATERIALENTRY entry;
			for (int k = 0; k < NUM_CMATERIAL_TEXTURES; k++) {
				entry.textureAssignments[k] = -1;
			}
			ar >> entry.colorTableIndexMod;
			ar >> entry.colorTableIndexUse;
			ar >> entry.flagsIndex;
			for (int k = 0; k < i; k++) {
				ar >> entry.textureAssignments[k];
			}
			m_compressedMaterials.push_back(entry);
		}
	}
}

// For V1, we read data as V2 but fix m_BlendMethod problem once loaded.
void MaterialCompressor::materialCompressionReadV1(CArchive& ar) {
	m_v1Compatible = true; // one and only place where m_v1Compatible should be set to true
	materialCompressionReadV2(ar);
}

void MaterialCompressor::readFromArchive(CArchive& ar) {
	unsigned char code;
	unsigned char version;
	ar >> code;
	if (code != material_archive_code) {
		AfxThrowArchiveException(CArchiveException::badClass, L"Material Compression system");
	}
	ar >> version;
	switch (version) {
		case 1:
			materialCompressionReadV1(ar);
			break;
		case 2:
			materialCompressionReadV2(ar);
			break;
		default:
			AfxThrowArchiveException(CArchiveException::badClass, L"Material Compression system: unknown version");
			break;
	}
	m_compressed = true;
}

CMaterialObject* MaterialCompressor::getMaterial(int materialTableIndex, BOOL bPreserveDisabledTextureLayers /*= FALSE*/) {
	assert(m_compressed);
	if (materialTableIndex == -1) {
		return NULL;
	}

	CMaterialObject* answer = new CMaterialObject();

	//Stop bad material index from crashing the client. -YC20091211
	ASSERT(materialTableIndex >= 0 && materialTableIndex < (int)m_compressedMaterials.size());
	if (materialTableIndex < 0 || materialTableIndex >= (int)m_compressedMaterials.size()) {
		return answer;
	}

	int useIndex = m_compressedMaterials[materialTableIndex].colorTableIndexUse;
	int modIndex = m_compressedMaterials[materialTableIndex].colorTableIndexMod;
	int flagsIndex = m_compressedMaterials[materialTableIndex].flagsIndex;

	answer->m_baseMaterial.Diffuse.r = m_colorTable[useIndex].diffuse.r;
	answer->m_baseMaterial.Diffuse.g = m_colorTable[useIndex].diffuse.g;
	answer->m_baseMaterial.Diffuse.b = m_colorTable[useIndex].diffuse.b;
	answer->m_baseMaterial.Diffuse.a = m_colorTable[useIndex].diffuse.a;
	answer->m_baseMaterial.Ambient.r = m_colorTable[useIndex].ambient.r;
	answer->m_baseMaterial.Ambient.g = m_colorTable[useIndex].ambient.g;
	answer->m_baseMaterial.Ambient.b = m_colorTable[useIndex].ambient.b;
	answer->m_baseMaterial.Ambient.a = m_colorTable[useIndex].ambient.a;
	answer->m_baseMaterial.Specular.r = m_colorTable[useIndex].specular.r;
	answer->m_baseMaterial.Specular.g = m_colorTable[useIndex].specular.g;
	answer->m_baseMaterial.Specular.b = m_colorTable[useIndex].specular.b;
	answer->m_baseMaterial.Specular.a = m_colorTable[useIndex].specular.a;
	answer->m_baseMaterial.Emissive.r = m_colorTable[useIndex].emissive.r;
	answer->m_baseMaterial.Emissive.g = m_colorTable[useIndex].emissive.g;
	answer->m_baseMaterial.Emissive.b = m_colorTable[useIndex].emissive.b;
	answer->m_baseMaterial.Emissive.a = m_colorTable[useIndex].emissive.a;
	answer->m_baseMaterial.Power = m_colorTable[useIndex].specularPower;

	memcpy(&answer->m_useMaterial, &answer->m_baseMaterial, sizeof(D3DMATERIAL9));

	answer->m_targetModMaterial.Diffuse.r = m_colorTable[modIndex].diffuse.r;
	answer->m_targetModMaterial.Diffuse.g = m_colorTable[modIndex].diffuse.g;
	answer->m_targetModMaterial.Diffuse.b = m_colorTable[modIndex].diffuse.b;
	answer->m_targetModMaterial.Diffuse.a = m_colorTable[modIndex].diffuse.a;
	answer->m_targetModMaterial.Ambient.r = m_colorTable[modIndex].ambient.r;
	answer->m_targetModMaterial.Ambient.g = m_colorTable[modIndex].ambient.g;
	answer->m_targetModMaterial.Ambient.b = m_colorTable[modIndex].ambient.b;
	answer->m_targetModMaterial.Ambient.a = m_colorTable[modIndex].ambient.a;
	answer->m_targetModMaterial.Specular.r = m_colorTable[modIndex].specular.r;
	answer->m_targetModMaterial.Specular.g = m_colorTable[modIndex].specular.g;
	answer->m_targetModMaterial.Specular.b = m_colorTable[modIndex].specular.b;
	answer->m_targetModMaterial.Specular.a = m_colorTable[modIndex].specular.a;
	answer->m_targetModMaterial.Emissive.r = m_colorTable[modIndex].emissive.r;
	answer->m_targetModMaterial.Emissive.g = m_colorTable[modIndex].emissive.g;
	answer->m_targetModMaterial.Emissive.b = m_colorTable[modIndex].emissive.b;
	answer->m_targetModMaterial.Emissive.a = m_colorTable[modIndex].emissive.a;
	answer->m_targetModMaterial.Power = m_colorTable[modIndex].specularPower;

	answer->SetTransition(m_flagsTable[flagsIndex].transition);
	answer->m_cycleTime = m_flagsTable[flagsIndex].cycleTime;
	answer->m_textureTileType = m_flagsTable[flagsIndex].textureTileType;
	answer->m_cullOn = m_flagsTable[flagsIndex].cullOn;
	answer->m_forceWireFrame = m_flagsTable[flagsIndex].forceWireFrame;
	answer->m_mirrorEnabled = m_flagsTable[flagsIndex].mirrorEnabled;
	answer->m_fogOverride = m_flagsTable[flagsIndex].fogOverride;
	answer->m_optionalShaderIndex = m_flagsTable[flagsIndex].shaderIndex;
	answer->m_diffuseEffect = m_flagsTable[flagsIndex].diffuseEffect;
	answer->m_blendMode = m_flagsTable[flagsIndex].blendMode;
	answer->SetFadeMaxDistance(m_flagsTable[flagsIndex].fadeMaxDist);
	answer->SetFadeVisDistance(m_flagsTable[flagsIndex].fadeVisDist);
	answer->SetFadeByDistance(m_flagsTable[flagsIndex].fadeByDistance);
	answer->m_fogFilter = m_flagsTable[flagsIndex].fogFilter;
	answer->m_zBias = m_flagsTable[flagsIndex].zBias;
	answer->m_doAlphaDepthTest = m_flagsTable[flagsIndex].alphaDepthTest;
	answer->m_pixelShader = m_flagsTable[flagsIndex].pixelShader;
	answer->m_vertexShader = m_flagsTable[flagsIndex].vertexShader;
	if (m_flagsTable[flagsIndex].fxBindStringIndex >= 0) {
		answer->m_fxBindString = *(m_fxBindStringTable[m_flagsTable[flagsIndex].fxBindStringIndex]);
	} else {
		answer->m_fxBindString = "";
	}
	if (m_flagsTable[flagsIndex].materialNameIndex >= 0) {
		answer->m_materialName = *(m_materialNameTable[m_flagsTable[flagsIndex].materialNameIndex]);
	} else {
		answer->m_materialName = "Default";
	}
	answer->m_sortOrder = m_flagsTable[flagsIndex].sortOrder;
	answer->m_enableTextureMovement = false;

	answer->m_texFileNameIsConcrete = m_texFileNameIsConcrete;
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
		if (m_compressedMaterials[materialTableIndex].textureAssignments[i] != -1) {
			int assignIndex = m_compressedMaterials[materialTableIndex].textureAssignments[i];
			answer->m_colorArg1[i] = m_assignmentTable[assignIndex].colorArg1;
			answer->m_colorArg2[i] = m_assignmentTable[assignIndex].colorArg2;
			answer->m_texFileName[i] = (const char*)*(m_fileNameTable[m_assignmentTable[assignIndex].filenameIndex]);
			if (m_v1Compatible) { // for v1 data only
				answer->m_texBlendMethod[i] = m_assignmentTable[assignIndex].blendMethod;
			} else { // normal data
				if (i == 0)
					answer->m_texBlendMethod[9] = m_assignmentTable[assignIndex].blendMethod;
				else
					answer->m_texBlendMethod[i - 1] = m_assignmentTable[assignIndex].blendMethod;
			}

			///////////////////////////////////////////////////////////////////////
			// Start duplicated texture layer fix
			//
			if (!bPreserveDisabledTextureLayers && i > 0) { // Don't discard first layer
				// First get the correct blend method (apply to both V1 and V2+ data)
				int blendMethod = answer->m_texBlendMethod[i - 1]; // Blend method of layer i (i>0) is in m_BlendMethod[i-1]

				// Now discard any texture whose blend method is set to "disabled" (OKed by studio--will not cause any data loss)
				if (blendMethod == -1) {
					//					answer->m_storedTextureFromFile[i].Empty();
					answer->m_texFileName[i] = "";
					continue;
				}
			}

			// End duplicated texture layer fix
			///////////////////////////////////////////////////////////////////////

			answer->m_texColorKey[i] = m_assignmentTable[assignIndex].textureColorKey;
			if (m_assignmentTable[assignIndex].textureMovementU != 0.0f || m_assignmentTable[assignIndex].textureMovementV != 0.0f) {
				answer->m_enableTextureMovement = true;
				//??? -- need add'l overhaul: why enable set 0 when id is out-of-bound?
				int uvSetId = i;
				if (uvSetId < 0 || (unsigned int)uvSetId >= MAX_UV_SETS) {
					uvSetId = 0;
				}
				answer->m_enableSet[uvSetId] = true;
				answer->m_textureMovement[i].tu = m_assignmentTable[assignIndex].textureMovementU;
				answer->m_textureMovement[i].tv = m_assignmentTable[assignIndex].textureMovementV;
			}
		}
	}

	//Compute texNameHash for UGC item textures
	answer->ComputeTextureNameHashes(true);
	return answer;
}

void MaterialCompressor::getBaseColor(const CMaterialObject* legacyMat, FULLMATERIALCOLOR* data) {
	assert(legacyMat);
	assert(data);
	data->ambient.r = legacyMat->m_baseMaterial.Ambient.r;
	data->ambient.g = legacyMat->m_baseMaterial.Ambient.g;
	data->ambient.b = legacyMat->m_baseMaterial.Ambient.b;
	data->ambient.a = legacyMat->m_baseMaterial.Ambient.a;
	data->diffuse.r = legacyMat->m_baseMaterial.Diffuse.r;
	data->diffuse.g = legacyMat->m_baseMaterial.Diffuse.g;
	data->diffuse.b = legacyMat->m_baseMaterial.Diffuse.b;
	data->diffuse.a = legacyMat->m_baseMaterial.Diffuse.a;
	data->specular.r = legacyMat->m_baseMaterial.Specular.r;
	data->specular.g = legacyMat->m_baseMaterial.Specular.g;
	data->specular.b = legacyMat->m_baseMaterial.Specular.b;
	data->specular.a = legacyMat->m_baseMaterial.Specular.a;
	data->specularPower = legacyMat->m_baseMaterial.Power;
	data->emissive.r = legacyMat->m_baseMaterial.Emissive.r;
	data->emissive.g = legacyMat->m_baseMaterial.Emissive.g;
	data->emissive.b = legacyMat->m_baseMaterial.Emissive.b;
	data->emissive.a = legacyMat->m_baseMaterial.Emissive.a;
}

void MaterialCompressor::getModColor(const CMaterialObject* legacyMat, FULLMATERIALCOLOR* data) {
	assert(legacyMat);
	assert(data);
	data->ambient.r = legacyMat->m_targetModMaterial.Ambient.r;
	data->ambient.g = legacyMat->m_targetModMaterial.Ambient.g;
	data->ambient.b = legacyMat->m_targetModMaterial.Ambient.b;
	data->ambient.a = legacyMat->m_targetModMaterial.Ambient.a;
	data->diffuse.r = legacyMat->m_targetModMaterial.Diffuse.r;
	data->diffuse.g = legacyMat->m_targetModMaterial.Diffuse.g;
	data->diffuse.b = legacyMat->m_targetModMaterial.Diffuse.b;
	data->diffuse.a = legacyMat->m_targetModMaterial.Diffuse.a;
	data->specular.r = legacyMat->m_targetModMaterial.Specular.r;
	data->specular.g = legacyMat->m_targetModMaterial.Specular.g;
	data->specular.b = legacyMat->m_targetModMaterial.Specular.b;
	data->specular.a = legacyMat->m_targetModMaterial.Specular.a;
	data->specularPower = legacyMat->m_targetModMaterial.Power;
	data->emissive.r = legacyMat->m_targetModMaterial.Emissive.r;
	data->emissive.g = legacyMat->m_targetModMaterial.Emissive.g;
	data->emissive.b = legacyMat->m_targetModMaterial.Emissive.b;
	data->emissive.a = legacyMat->m_targetModMaterial.Emissive.a;
}

void MaterialCompressor::getFlags(const CMaterialObject* legacyMat, FULLMATERIALFLAGS* data) {
	assert(legacyMat);
	assert(data);
	data->blendMode = legacyMat->m_blendMode;
	data->zBias = legacyMat->m_zBias;
	data->alphaDepthTest = legacyMat->m_doAlphaDepthTest;
	data->sortOrder = legacyMat->m_sortOrder;
	data->transition = legacyMat->GetTransition();
	data->cycleTime = legacyMat->m_cycleTime;
	data->fogOverride = legacyMat->m_fogOverride;
	data->fogFilter = legacyMat->m_fogFilter;
	data->fadeMaxDist = legacyMat->GetFadeMaxDistance();
	data->fadeVisDist = legacyMat->GetFadeVisDistance();
	data->fadeByDistance = legacyMat->GetFadeByDistance();
	data->cullOn = legacyMat->m_cullOn;
	data->forceWireFrame = legacyMat->m_forceWireFrame;
	data->mirrorEnabled = legacyMat->m_mirrorEnabled;
	data->diffuseEffect = legacyMat->m_diffuseEffect;
	data->shaderIndex = legacyMat->m_optionalShaderIndex;
	data->pixelShader = legacyMat->m_pixelShader;
	data->vertexShader = legacyMat->m_vertexShader;
	data->materialNameIndex = addStringNoDupes(&(legacyMat->m_materialName), m_materialNameTable);
	data->fxBindStringIndex = addStringNoDupes(&(legacyMat->m_fxBindString), m_fxBindStringTable);
	data->textureTileType = legacyMat->m_textureTileType;
}

void MaterialCompressor::getTextureAssignment(int assignmentIndex, const CMaterialObject* legacyMat, FULLTEXTUREASSIGNMENT* data) {
	assert(legacyMat);
	assert(data);
	assert(assignmentIndex >= 0 && assignmentIndex < NUM_CMATERIAL_TEXTURES);
	// check for NONE
	if (!ValidTextureFileName(legacyMat->m_texFileName[assignmentIndex])) {
		data->filenameIndex = NO_TEXTURE_ASSIGNMENT;
		data->blendMethod = -1;
		data->colorArg1 = 2;
		if (assignmentIndex == 0) { // DRF - assignment in conditional bona fide bug
			data->colorArg2 = 0;
		} else {
			data->colorArg2 = 1;
		}
		data->textureColorKey = 0;
		data->textureMovementU = 0.0f;
		data->textureMovementV = 0.0f;
	} else {
		CStringA stff = legacyMat->m_texFileName[assignmentIndex].c_str();
		data->filenameIndex = addStringNoDupes(&stff, m_fileNameTable);
		if (assignmentIndex == 0)
			data->blendMethod = (char)legacyMat->m_texBlendMethod[9];
		else
			data->blendMethod = (char)legacyMat->m_texBlendMethod[assignmentIndex - 1];
		data->colorArg1 = legacyMat->m_colorArg1[assignmentIndex];
		data->colorArg2 = legacyMat->m_colorArg2[assignmentIndex];
		data->textureColorKey = legacyMat->m_texColorKey[assignmentIndex];
		BOOL enable = getEnable(legacyMat, assignmentIndex);
		// Due to hard coding and a bug in CMaterial it is impossible for texture assignments 8 and 9
		// to have scrolling uvs.  It will arrayindex out of bounds.
		if (legacyMat->m_enableTextureMovement && enable && assignmentIndex < 8) {
			data->textureMovementU = legacyMat->m_textureMovement[assignmentIndex].tu;
			data->textureMovementV = legacyMat->m_textureMovement[assignmentIndex].tv;
		} else {
			data->textureMovementU = 0.0f;
			data->textureMovementV = 0.0f;
		}
	}
}

static bool operator==(const FULLMATERIALCOLOR& mat1, const FULLMATERIALCOLOR& mat2) {
	return (memcmp(&mat1, &mat2, sizeof(FULLMATERIALCOLOR)) == 0);
}

static bool operator==(const FULLMATERIALFLAGS& flags1, const FULLMATERIALFLAGS& flags2) {
	return (memcmp(&flags1, &flags2, sizeof(FULLMATERIALFLAGS)) == 0);
}

static bool operator==(const FULLTEXTUREASSIGNMENT& texAssign1, const FULLTEXTUREASSIGNMENT& texAssign2) {
	return (memcmp(&texAssign1, &texAssign2, sizeof(FULLTEXTUREASSIGNMENT)) == 0);
}

static bool operator==(const MATERIALENTRY& entry1, const MATERIALENTRY& entry2) {
	return (memcmp(&entry1, &entry2, sizeof(MATERIALENTRY)) == 0);
}

#define BEGIN_COMPARE int res;
#define END_COMPARE return 0;
#define COMPARE_FIELD(fld)               \
	res = compare(lhs.##fld, rhs.##fld); \
	if (res != 0)                        \
		return res;
#define COMPARE_FIELD_ARRAY(fld, min, max)         \
	res = compare(lhs.##fld, rhs.##fld, min, max); \
	if (res != 0)                                  \
		return res;

int MaterialCompressor::compare(const D3DCOLORVALUE& lhs, const D3DCOLORVALUE& rhs) {
	BEGIN_COMPARE;
	COMPARE_FIELD(r);
	COMPARE_FIELD(g);
	COMPARE_FIELD(b);
	COMPARE_FIELD(a);
	END_COMPARE;
}

int MaterialCompressor::compare(const D3DMATERIAL9& lhs, const D3DMATERIAL9& rhs) {
	BEGIN_COMPARE;
	COMPARE_FIELD(Diffuse);
	COMPARE_FIELD(Ambient);
	COMPARE_FIELD(Specular);
	COMPARE_FIELD(Emissive);
	COMPARE_FIELD(Power);
	END_COMPARE;
}

int MaterialCompressor::compare(const TXTUV& lhs, const TXTUV& rhs) {
	BEGIN_COMPARE;
	COMPARE_FIELD(tu);
	COMPARE_FIELD(tv);
	END_COMPARE;
}

int MaterialCompressor::compare(const CMaterialObject& lhs, const CMaterialObject& rhs) {
	//assert(mat1);
	//assert(mat2);

	BEGIN_COMPARE;
	COMPARE_FIELD(m_baseMaterial);
	COMPARE_FIELD(m_targetModMaterial);
	COMPARE_FIELD(GetTransition());
	COMPARE_FIELD(m_cycleTime);
	COMPARE_FIELD(m_textureTileType);
	COMPARE_FIELD(m_cullOn);
	COMPARE_FIELD(m_forceWireFrame);
	COMPARE_FIELD(m_mirrorEnabled);
	COMPARE_FIELD(m_fogOverride);
	COMPARE_FIELD(m_enableTextureMovement);
	COMPARE_FIELD(m_optionalShaderIndex);
	COMPARE_FIELD(m_diffuseEffect);
	COMPARE_FIELD(m_blendMode);
	COMPARE_FIELD(GetFadeMaxDistance());
	COMPARE_FIELD(GetFadeVisDistance());
	COMPARE_FIELD(GetFadeByDistance());
	COMPARE_FIELD(m_fogFilter);
	COMPARE_FIELD(m_zBias);
	COMPARE_FIELD(m_doAlphaDepthTest);
	COMPARE_FIELD(m_pixelShader);
	COMPARE_FIELD(m_vertexShader);
	COMPARE_FIELD(m_fxBindString);
	COMPARE_FIELD(m_sortOrder);
	COMPARE_FIELD_ARRAY(m_textureMovement, 0, MAX_UV_SETS);
	COMPARE_FIELD_ARRAY(m_enableSet, 0, MAX_UV_SETS);
	COMPARE_FIELD_ARRAY(m_texBlendMethod, 0, NUM_CMATERIAL_TEXTURES);
	COMPARE_FIELD_ARRAY(m_texColorKey, 0, NUM_CMATERIAL_TEXTURES);
	COMPARE_FIELD_ARRAY(m_colorArg1, 0, NUM_CMATERIAL_TEXTURES);
	COMPARE_FIELD_ARRAY(m_colorArg2, 0, NUM_CMATERIAL_TEXTURES);
	COMPARE_FIELD_ARRAY(m_texFileName, 0, NUM_CMATERIAL_TEXTURES);
	// Do case-insensitive but strict one-on-one comparison
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
		std::string lstr = lhs.m_texFileName[i];
		std::string rstr = rhs.m_texFileName[i];
		STLToUpper(lstr);
		STLToUpper(rstr);
		if (lstr.empty())
			lstr = "NONE";
		if (rstr.empty())
			rstr = "NONE";
		int compareResult = lstr.compare(rstr);
		if (compareResult != 0) {
			return compareResult;
		}
	}
	END_COMPARE;
}

void MaterialCompressor::convertColorDown(const FULLMATERIALCOLOR* col1, SPECULARMATERIALCOLOR* col2) {
	assert(getCompressionType(col1) == SpecularMaterialColor);
	col2->ambient.r = col1->ambient.r;
	col2->ambient.g = col1->ambient.g;
	col2->ambient.b = col1->ambient.b;
	col2->ambient.a = col1->ambient.a;
	col2->diffuse.r = col1->diffuse.r;
	col2->diffuse.g = col1->diffuse.g;
	col2->diffuse.b = col1->diffuse.b;
	col2->diffuse.a = col1->diffuse.a;
	col2->specular.r = col1->specular.r;
	col2->specular.g = col1->specular.g;
	col2->specular.b = col1->specular.b;
	col2->specular.a = col1->specular.a;
	col2->specularPower = col1->specularPower;
}

void MaterialCompressor::convertColorDown(const FULLMATERIALCOLOR* col1, EMISSIVEMATERIALCOLOR* col2) {
	assert(getCompressionType(col1) == EmissiveMaterialColor);
	col2->ambient.r = col1->ambient.r;
	col2->ambient.g = col1->ambient.g;
	col2->ambient.b = col1->ambient.b;
	col2->ambient.a = col1->ambient.a;
	col2->diffuse.r = col1->diffuse.r;
	col2->diffuse.g = col1->diffuse.g;
	col2->diffuse.b = col1->diffuse.b;
	col2->diffuse.a = col1->diffuse.a;
	col2->emissive.r = col1->emissive.r;
	col2->emissive.g = col1->emissive.g;
	col2->emissive.b = col1->emissive.b;
	col2->emissive.a = col1->emissive.a;
}

void MaterialCompressor::convertColorDown(const FULLMATERIALCOLOR* col1, PLAINMATERIALCOLOR* col2) {
	assert(getCompressionType(col1) == PlainMaterialColor);
	col2->ambient.r = col1->ambient.r;
	col2->ambient.g = col1->ambient.g;
	col2->ambient.b = col1->ambient.b;
	col2->ambient.a = col1->ambient.a;
	col2->diffuse.r = col1->diffuse.r;
	col2->diffuse.g = col1->diffuse.g;
	col2->diffuse.b = col1->diffuse.b;
	col2->diffuse.a = col1->diffuse.a;
}

void MaterialCompressor::convertColorUp(const SPECULARMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2) {
	col2->ambient.r = col1->ambient.r;
	col2->ambient.g = col1->ambient.g;
	col2->ambient.b = col1->ambient.b;
	col2->ambient.a = col1->ambient.a;
	col2->diffuse.r = col1->diffuse.r;
	col2->diffuse.g = col1->diffuse.g;
	col2->diffuse.b = col1->diffuse.b;
	col2->diffuse.a = col1->diffuse.a;
	col2->emissive.r = 0.0f;
	col2->emissive.g = 0.0f;
	col2->emissive.b = 0.0f;
	col2->emissive.a = 0.0f;
	col2->specular.r = col1->specular.r;
	col2->specular.g = col1->specular.g;
	col2->specular.b = col1->specular.b;
	col2->specular.a = col1->specular.a;
	col2->specularPower = col1->specularPower;
	assert(getCompressionType(col2) == SpecularMaterialColor);
}

void MaterialCompressor::convertColorUp(const EMISSIVEMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2) {
	col2->ambient.r = col1->ambient.r;
	col2->ambient.g = col1->ambient.g;
	col2->ambient.b = col1->ambient.b;
	col2->ambient.a = col1->ambient.a;
	col2->diffuse.r = col1->diffuse.r;
	col2->diffuse.g = col1->diffuse.g;
	col2->diffuse.b = col1->diffuse.b;
	col2->diffuse.a = col1->diffuse.a;
	col2->emissive.r = col1->emissive.r;
	col2->emissive.g = col1->emissive.g;
	col2->emissive.b = col1->emissive.b;
	col2->emissive.a = col1->emissive.a;
	col2->specular.r = 0.0f;
	col2->specular.g = 0.0f;
	col2->specular.b = 0.0f;
	col2->specular.a = 0.0f;
	col2->specularPower = 0.0f;
	assert(getCompressionType(col2) == EmissiveMaterialColor);
}

void MaterialCompressor::convertColorUp(const PLAINMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2) {
	col2->ambient.r = col1->ambient.r;
	col2->ambient.g = col1->ambient.g;
	col2->ambient.b = col1->ambient.b;
	col2->ambient.a = col1->ambient.a;
	col2->diffuse.r = col1->diffuse.r;
	col2->diffuse.g = col1->diffuse.g;
	col2->diffuse.b = col1->diffuse.b;
	col2->diffuse.a = col1->diffuse.a;
	col2->emissive.r = 0.0f;
	col2->emissive.g = 0.0f;
	col2->emissive.b = 0.0f;
	col2->emissive.a = 0.0f;
	col2->specular.r = 0.0f;
	col2->specular.g = 0.0f;
	col2->specular.b = 0.0f;
	col2->specular.a = 0.0f;
	col2->specularPower = 0.0f;
	assert(getCompressionType(col2) == PlainMaterialColor);
}

void MaterialCompressor::convertColorUp(const BAREMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2) {
	col2->ambient.r = col1->dambient.r;
	col2->ambient.g = col1->dambient.g;
	col2->ambient.b = col1->dambient.b;
	col2->ambient.a = col1->dambient.a;
	col2->diffuse.r = col1->dambient.r;
	col2->diffuse.g = col1->dambient.g;
	col2->diffuse.b = col1->dambient.b;
	col2->diffuse.a = col1->dambient.a;
	col2->emissive.r = 0.0f;
	col2->emissive.g = 0.0f;
	col2->emissive.b = 0.0f;
	col2->emissive.a = 0.0f;
	col2->specular.r = 0.0f;
	col2->specular.g = 0.0f;
	col2->specular.b = 0.0f;
	col2->specular.a = 0.0f;
	col2->specularPower = 0.0f;
	assert(getCompressionType(col2) == BareMaterialColor);
}

void MaterialCompressor::convertColorDown(const FULLMATERIALCOLOR* col1, BAREMATERIALCOLOR* col2) {
	assert(getCompressionType(col1) == BareMaterialColor);
	col2->dambient.r = col1->ambient.r;
	col2->dambient.g = col1->ambient.g;
	col2->dambient.b = col1->ambient.b;
	col2->dambient.a = col1->ambient.a;
}

void MaterialCompressor::convertAssignmentDown(const FULLTEXTUREASSIGNMENT* assign1, BLENDEDTEXTUREASSIGNMENT* assign2) {
	assert(getCompressionType(assign1) == BlendedTextureAssignment);
	assign2->blendMethod = assign1->blendMethod;
	assign2->colorArg1 = assign1->colorArg1;
	assign2->colorArg2 = assign1->colorArg2;
	assign2->filenameIndex = assign1->filenameIndex;
}

void MaterialCompressor::convertAssignmentDown(const FULLTEXTUREASSIGNMENT* assign1, BARETEXTUREASSIGNMENT* assign2) {
	assert(getCompressionType(assign1) == BareTextureAssignment);
	assign2->filenameIndex = assign1->filenameIndex;
}

void MaterialCompressor::convertAssignmentUp(const BLENDEDTEXTUREASSIGNMENT* assign1, FULLTEXTUREASSIGNMENT* assign2) {
	assign2->blendMethod = assign1->blendMethod;
	assign2->colorArg1 = assign1->colorArg1;
	assign2->colorArg2 = assign1->colorArg2;
	assign2->filenameIndex = assign1->filenameIndex;
	assign2->textureColorKey = 0;
	assign2->textureMovementU = 0.0f;
	assign2->textureMovementV = 0.0f;
}

void MaterialCompressor::convertAssignmentUp(const BARETEXTUREASSIGNMENT* assign1, FULLTEXTUREASSIGNMENT* assign2) {
	assign2->blendMethod = -1;
	assign2->colorArg1 = 2;
	assign2->colorArg2 = 2;
	assign2->filenameIndex = assign1->filenameIndex;
	assign2->textureColorKey = 0;
	assign2->textureMovementU = 0.0f;
	assign2->textureMovementV = 0.0f;
}

int MaterialCompressor::addColorNoDupes(FULLMATERIALCOLOR* color) {
	assert(color);
	for (unsigned int i = 0; i < m_colorTable.size(); i++) {
		if (*color == m_colorTable[i]) {
			return i;
		}
	}
	assert(m_compressed == false);
	m_colorTable.push_back(*color);
	return m_colorTable.size() - 1;
}

int MaterialCompressor::addFlagsNoDupes(FULLMATERIALFLAGS* flags) {
	assert(flags);
	for (unsigned int i = 0; i < m_flagsTable.size(); i++) {
		if (*flags == m_flagsTable[i]) {
			return i;
		}
	}
	assert(m_compressed == false);
	m_flagsTable.push_back(*flags);
	return m_flagsTable.size() - 1;
}

int MaterialCompressor::addTexAssignNoDupes(FULLTEXTUREASSIGNMENT* texAssign) {
	assert(texAssign);
	if (texAssign->filenameIndex == NO_TEXTURE_ASSIGNMENT) {
		return NO_TEXTURE_ASSIGNMENT;
	}
	for (unsigned int i = 0; i < m_assignmentTable.size(); i++) {
		if (*texAssign == m_assignmentTable[i]) {
			return i;
		}
	}
	assert(m_compressed == false);
	if (m_compressed == false) {
		m_assignmentTable.push_back(*texAssign);
		return m_assignmentTable.size() - 1;
	} else {
		return -1;
	}
}

int MaterialCompressor::addIndexedEntryNoDupes(MATERIALENTRY* matEntry, bool locked) {
	assert(matEntry);
	assert(m_compressed == true);
	alignAndCountCompressedMat(matEntry);
	for (unsigned int i = 0; i < m_compressedMaterials.size(); i++) {
		alignAndCountCompressedMat(&m_compressedMaterials[i]);
		if (*matEntry == m_compressedMaterials[i]) {
			return i;
		}
	}
	assert(locked == false);
	if (!locked) {
		m_compressedMaterials.push_back(*matEntry);
		return m_compressedMaterials.size() - 1;
	} else {
		return -1;
	}
}

inline bool isColorBlack(RGBMatColor c) {
	if (c.r != 0.0f || c.g != 0.0f || c.b != 0.0f || c.a != 0.0f) {
		return false;
	} else {
		return true;
	}
}

inline bool colorEqual(RGBMatColor c1, RGBMatColor c2) {
	if (c1.r == c2.r && c1.g == c2.g && c1.b == c2.b && c1.a == c2.a) {
		return true;
	} else {
		return false;
	}
}

MaterialColorType MaterialCompressor::getCompressionType(const FULLMATERIALCOLOR* color) {
	if (!isColorBlack(color->emissive) && !isColorBlack(color->specular) && color->specularPower != 0.0f) {
		// the emissive is not black and the specular is not black and the specular power is not 0
		return FullMaterialColor;
	}
	if (!isColorBlack(color->emissive)) {
		return EmissiveMaterialColor;
	}
	if (!isColorBlack(color->specular) && color->specularPower != 0.0f) {
		return SpecularMaterialColor;
	}
	if (colorEqual(color->ambient, color->diffuse)) {
		return BareMaterialColor;
	} else {
		return PlainMaterialColor;
	}
}

TextureAssignmentType MaterialCompressor::getCompressionType(const FULLTEXTUREASSIGNMENT* texAssign) {
	if (texAssign->textureMovementU != 0.0f || texAssign->textureMovementV != 0.0f || texAssign->textureColorKey != 0) {
		return FullTextureAssignment;
	} else if (texAssign->blendMethod != -1 || texAssign->colorArg1 != 2 || texAssign->colorArg2 != 2) {
		return BlendedTextureAssignment;
	} else {
		return BareTextureAssignment;
	}
}

void MaterialCompressor::colorSort() {
	std::vector<FULLMATERIALCOLOR> fullMats;
	std::vector<FULLMATERIALCOLOR> specMats;
	std::vector<FULLMATERIALCOLOR> emisMats;
	std::vector<FULLMATERIALCOLOR> plainMats;
	std::vector<FULLMATERIALCOLOR> bareMats;

	for (unsigned int i = 0; i < m_colorTable.size(); i++) {
		switch (getCompressionType(&(m_colorTable[i]))) {
			case SpecularMaterialColor:
				specMats.push_back(m_colorTable[i]);
				break;
			case EmissiveMaterialColor:
				emisMats.push_back(m_colorTable[i]);
				break;
			case PlainMaterialColor:
				plainMats.push_back(m_colorTable[i]);
				break;
			case BareMaterialColor:
				bareMats.push_back(m_colorTable[i]);
				break;
			default:
			case FullMaterialColor:
				fullMats.push_back(m_colorTable[i]);
				break;
		}
	}
	m_colorTable.clear();
	m_fileHeader.fullColorCount = fullMats.size();
	m_fileHeader.specularColorCount = specMats.size();
	m_fileHeader.emissiveColorCount = emisMats.size();
	m_fileHeader.plainColorCount = plainMats.size();
	m_fileHeader.bareColorCount = bareMats.size();
	for (unsigned int i = 0; i < fullMats.size(); i++) {
		m_colorTable.push_back(fullMats[i]);
	}
	for (unsigned int i = 0; i < specMats.size(); i++) {
		m_colorTable.push_back(specMats[i]);
	}
	for (unsigned int i = 0; i < emisMats.size(); i++) {
		m_colorTable.push_back(emisMats[i]);
	}
	for (unsigned int i = 0; i < plainMats.size(); i++) {
		m_colorTable.push_back(plainMats[i]);
	}
	for (unsigned int i = 0; i < bareMats.size(); i++) {
		m_colorTable.push_back(bareMats[i]);
	}
}

void MaterialCompressor::flagSort() {
	// not necessary currently there are no abbreviated structs for flags.
	m_fileHeader.flagsCount = m_flagsTable.size();
}

void MaterialCompressor::texAssignSort() {
	std::vector<FULLTEXTUREASSIGNMENT> fullAssign;
	std::vector<FULLTEXTUREASSIGNMENT> blendedAssign;
	std::vector<FULLTEXTUREASSIGNMENT> bareAssign;

	for (unsigned int i = 0; i < m_assignmentTable.size(); i++) {
		switch (getCompressionType(&(m_assignmentTable[i]))) {
			case BlendedTextureAssignment:
				blendedAssign.push_back(m_assignmentTable[i]);
				break;
			case BareTextureAssignment:
				bareAssign.push_back(m_assignmentTable[i]);
				break;
			default:
			case FullTextureAssignment:
				fullAssign.push_back(m_assignmentTable[i]);
				break;
		}
	}
	m_assignmentTable.clear();
	m_fileHeader.fullAssignCount = fullAssign.size();
	m_fileHeader.blendedAssignCount = blendedAssign.size();
	m_fileHeader.bareAssignCount = bareAssign.size();
	for (unsigned int i = 0; i < fullAssign.size(); i++) {
		m_assignmentTable.push_back(fullAssign[i]);
	}
	for (unsigned int i = 0; i < blendedAssign.size(); i++) {
		m_assignmentTable.push_back(blendedAssign[i]);
	}
	for (unsigned int i = 0; i < bareAssign.size(); i++) {
		m_assignmentTable.push_back(bareAssign[i]);
	}
}

int MaterialCompressor::alignAndCountCompressedMat(MATERIALENTRY* ent) {
	int shiftedAssignments[NUM_CMATERIAL_TEXTURES];
	int usedAssignmentCount = 0;
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
		if (ent->textureAssignments[i] != NO_TEXTURE_ASSIGNMENT) {
			shiftedAssignments[usedAssignmentCount] = ent->textureAssignments[i];
			usedAssignmentCount = usedAssignmentCount + 1;
		}
	}
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
		if (i < usedAssignmentCount) {
			ent->textureAssignments[i] = shiftedAssignments[i];
		} else {
			ent->textureAssignments[i] = -1;
		}
	}
	return usedAssignmentCount;
}

void MaterialCompressor::matEntrySort() {
	std::vector<MATERIALENTRY> entries[NUM_CMATERIAL_TEXTURES + 1];

	for (unsigned int i = 0; i < m_compressedMaterials.size(); i++) {
		int assignmentCount = alignAndCountCompressedMat(&(m_compressedMaterials[i]));
		entries[assignmentCount].push_back(m_compressedMaterials[i]);
	}
	m_compressedMaterials.clear();
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES + 1; i++) {
		m_fileHeader.m_compressionTextureCounts[i] = entries[i].size();
		for (unsigned int j = 0; j < entries[i].size(); j++) {
			m_compressedMaterials.push_back(entries[i][j]);
		}
	}
}

// Unit test function. Not used by engine.
bool MaterialCompressor::verifyCompression() {
	bool compareResult = true;
	for (unsigned int i = 0; i < m_legacyMaterials.size(); i++) {
		CMaterialObject* compressedVer = getMaterial(m_legacyMaterials[i]->m_myCompressionIndex);
		if (compare(*m_legacyMaterials[i], *compressedVer) != 0) {
			// Different
			compareResult = false;
		}
		delete compressedVer;
	}
	return compareResult;
}

void MaterialCompressor::CompressorMap::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObjectSizeof(m_cs);
		pMemSizeGadget->AddObject(m_matCompMap);
	}
}

void FULLMATERIALFLAGS::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void FULLMATERIALCOLOR::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void MaterialCompressor::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_colorTable);
		pMemSizeGadget->AddObjectSizeof(m_fileHeader);
		pMemSizeGadget->AddObject(m_fileNameTable);
		pMemSizeGadget->AddObject(m_flagsTable);
		pMemSizeGadget->AddObject(m_fxBindStringTable);
		pMemSizeGadget->AddObject(m_legacyMaterials);
		pMemSizeGadget->AddObject(m_materialNameTable);

		// static data member
		pMemSizeGadget->AddObject(m_compressorMap);
	}
}

} // namespace KEP
