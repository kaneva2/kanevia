///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "math.h"

#include "CEnvironmentClass.h"

#include "CCharonaClass.h"
#include "CDayStateClass.h"
#include "CEXMeshClass.h"
#include "CStateManagementClass.h"
#include "CStormClass.h"
#include "CTimeSystem.h"
#include "matrixArb.h"
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "Core/Math/Rotation.h"

namespace KEP {

static void D3DUtil_InitLight(
	D3DLIGHT9& light,
	D3DLIGHTTYPE ltType,
	FLOAT x, FLOAT y, FLOAT z) {
	ZeroMemory(&light, sizeof(D3DLIGHT9));
	light.Type = ltType;
	light.Diffuse.r = 1.0f;
	light.Diffuse.g = 1.0f;
	light.Diffuse.b = 1.0f;
	light.Specular = light.Diffuse;
	light.Position.x = light.Direction.x = x;
	light.Position.y = light.Direction.y = y;
	light.Position.z = light.Direction.z = z;
	light.Attenuation0 = 1.0f;
	light.Range = sqrt(FLT_MAX);
}

CEnvironmentObj::CEnvironmentObj() :
		m_environmentTimeSystem(nullptr),
		m_enableTimeSystem(FALSE),
		m_fogOn(FALSE),
		m_fogRange(0.0f),
		m_fogStart(0.0f),
		m_fogRed(0.0f),
		m_fogGreen(0.0f),
		m_fogBlue(0.0f),
		m_backRed(0.0f),
		m_backGreen(0.0f),
		m_backBlue(0.0f),
		m_sunLightOn(TRUE),
		m_sunPosition(0.0f, 1000.0f, 1000.0f),
		m_sunRed(1.0f),
		m_sunGreen(1.0f),
		m_sunBlue(1.0f),
		m_ambientLightOn(TRUE),
		m_ambientLightRedDay(.2f),
		m_ambientLightGreenDay(.2f),
		m_ambientLightBlueDay(.2f),
		m_specularLightOn(FALSE),
		m_specularLightRed(1.0f),
		m_specularLightGreen(1.0f),
		m_specularLightBlue(1.0f),
		m_gravity(-.1f),
		m_atmosphereDensity(FALSE),
		m_seaLevel(0.0f),
		m_seaViscusDrag(.97f),
		m_lockBackColor(false),
		m_fogCanCull(false) {}

void CEnvironmentObj::SafeDelete() {
	DeleteEnvironmentTimeSystem();
}

void CEnvironmentObj::DeleteEnvironmentTimeSystem() {
	if (m_environmentTimeSystem) {
		m_environmentTimeSystem->SafeDelete();
		delete m_environmentTimeSystem;
		m_environmentTimeSystem = nullptr;
	}
}

BOOL CEnvironmentObj::SetWorldCurrentTime(
	long currentSystemDay,
	long currentSystemYear,
	long currentSystemMonth,
	long currentDayTime) {
	if (!m_environmentTimeSystem)
		return FALSE;
	m_environmentTimeSystem->m_timeStamp = fTime::TimeMs();
	m_environmentTimeSystem->m_currentSystemDay = currentSystemDay;
	m_environmentTimeSystem->m_currentSystemMonth = currentSystemMonth;
	m_environmentTimeSystem->m_currentSystemYear = currentSystemYear;
	m_environmentTimeSystem->m_currentDayTime = currentDayTime;
	return TRUE;
}

BOOL CEnvironmentObj::InitAll(
	LPDIRECT3DDEVICE9 pD3D,
	D3DCOLOR* BackgroundColorGlobal,
	CStateManagementObj* pSMO) {
	if (!pD3D || !pSMO)
		return FALSE;

	if (m_environmentTimeSystem) {
		m_environmentTimeSystem->StartSystem();
		if (m_environmentTimeSystem->m_visual) {
			m_environmentTimeSystem->m_visual->InitBuffer(pD3D, NULL);
			if (m_environmentTimeSystem->m_visual->m_charona) {
				m_environmentTimeSystem->m_visual->m_charona->InitializeVis();
				m_environmentTimeSystem->m_visual->m_charona->m_visual->InitBuffer(pD3D, m_environmentTimeSystem->m_visual->m_charona->m_visual->m_uvCount);
			}
		}
	}

	D3DCOLOR colorLoc = D3DCOLOR_XRGB((int)(m_fogRed * 255.0f),
		(int)(m_fogGreen * 255.0f),
		(int)(m_fogBlue * 255.0f));

	*BackgroundColorGlobal = D3DCOLOR_XRGB((int)(m_backRed * 255.0f),
		(int)(m_backGreen * 255.0f),
		(int)(m_backBlue * 255.0f));

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return FALSE;

	auto HWCaps = pICE->GetReDeviceState()->GetRenderState()->GetHardwareCaps();

	// Set Fog
	bool fogEnabled = (m_fogOn != FALSE);
	if (!fogEnabled)
		m_fogStart = m_fogRange = 0.f;
	pSMO->SetFogEnabled(fogEnabled);
	auto fogRange = D3DXVECTOR2(m_fogStart, m_fogRange);
	pSMO->SetFogRange(fogRange); // drf - added
	Vector3f fogColor(m_fogRed, m_fogGreen, m_fogBlue);
	pSMO->SetFogColor(*reinterpret_cast<D3DVECTOR*>(&fogColor));
	pSMO->SetFogCanCull(m_fogCanCull); // drf - added

	// manage fog state here using D3D...NOT using vertex shader fogging
	if (HWCaps < ReD3DX9MINVERTEXSHADERSUPPORT) {
		if (fogEnabled) {
			FLOAT fFogEnd = m_fogRange;
			FLOAT fFogStart = m_fogStart;
			pD3D->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_LINEAR);
			pD3D->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
			pD3D->SetRenderState(D3DRS_FOGCOLOR, colorLoc);
			pD3D->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&fFogStart));
			pD3D->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&fFogEnd));
			pSMO->SetFogConstant(fFogStart, fFogEnd);
		}
	}

	D3DLIGHT9 light;
	if (m_sunLightOn == TRUE) {
		D3DUtil_InitLight(light, D3DLIGHT_DIRECTIONAL, m_sunRed, m_sunGreen, m_sunBlue);
		light.Diffuse.r = m_sunRed;
		light.Diffuse.g = m_sunGreen;
		light.Diffuse.b = m_sunBlue;
	} else {
		D3DUtil_InitLight(light, D3DLIGHT_DIRECTIONAL, 0.f, 0.f, 0.f);
		light.Diffuse.r = 0.f;
		light.Diffuse.g = 0.f;
		light.Diffuse.b = 0.f;
	}

	Vector3f temp = m_sunPosition;
	temp.Normalize();

	light.Position = reinterpret_cast<D3DVECTOR&>(m_sunPosition);
	light.Direction.x = -temp.x;
	light.Direction.y = -temp.y;
	light.Direction.z = -temp.z;

	if (m_specularLightOn) {
		light.Specular.r = m_specularLightRed;
		light.Specular.g = m_specularLightGreen;
		light.Specular.b = m_specularLightBlue;
		light.Specular.a = 0.f;
	} else {
		light.Specular.r = 0.f;
		light.Specular.g = 0.f;
		light.Specular.b = 0.f;
		light.Specular.a = 0.f;
	}

	pD3D->SetLight(0, &light);
	pD3D->LightEnable(0, TRUE);
	pSMO->SetSunLight(light);
	pSMO->SetDirLight(light.Position, light.Diffuse.r, light.Diffuse.g, light.Diffuse.b);

	if (m_ambientLightOn == TRUE) {
		Vector3f ambient(m_ambientLightRedDay, m_ambientLightGreenDay, m_ambientLightBlueDay);
		pSMO->SetAmbLight(*reinterpret_cast<D3DVECTOR*>(&ambient));
	} else {
		Vector3f ambient(0, 0, 0);
		pSMO->SetAmbLight(*reinterpret_cast<D3DVECTOR*>(&ambient));
	}

	if (TRUE == m_specularLightOn) {
		Vector3f specular(m_specularLightRed, m_specularLightGreen, m_specularLightBlue);
		pSMO->SetSpcLight(*reinterpret_cast<D3DVECTOR*>(&specular));
	} else {
		Vector3f specular(0.0f, 0.0f, 0.0f);
		pSMO->SetSpcLight(*reinterpret_cast<D3DVECTOR*>(&specular));
	}

	if (m_environmentTimeSystem) {
		if (m_environmentTimeSystem->m_pDSO) {
			m_environmentTimeSystem->m_pDSO->SafeDelete();
			delete m_environmentTimeSystem->m_pDSO;
			m_environmentTimeSystem->m_pDSO = 0;
		}

		if (m_environmentTimeSystem->m_stormPtr) {
			m_environmentTimeSystem->m_stormPtr->SafeDelete();
			delete m_environmentTimeSystem->m_stormPtr;
			m_environmentTimeSystem->m_stormPtr = 0;
		}
	}

	return TRUE;
}

BOOL CEnvironmentObj::CallBack(
	LPDIRECT3DDEVICE9 pD3D,
	D3DCOLOR* pBackgroundColorGlobal,
	CLightningObjList* lightningEffectSystem,
	Matrix44f soundCenter,
	CSoundBankList* soundManager,
	BOOL& renderEnvironment,
	CStateManagementObj* pSMO,
	CEXMeshObjList* charonaList) {
	if (!pBackgroundColorGlobal)
		return FALSE;

	D3DCOLOR backgroundColor = D3DCOLOR_XRGB(int(255 * m_backRed), int(255 * m_backGreen), int(255 * m_backBlue));
	*pBackgroundColorGlobal = backgroundColor;

	if (!m_enableTimeSystem || !m_environmentTimeSystem)
		return TRUE;

	m_environmentTimeSystem->UpdateSystem(
		lightningEffectSystem,
		soundCenter,
		soundManager);

	auto pDSO = m_environmentTimeSystem->m_pDSO;
	if (!pDSO || !pSMO)
		return TRUE;

	if (renderEnvironment) {
		bool fogEnabled = (m_fogOn != FALSE);
		if (fogEnabled) {
			D3DCOLOR currentfogColor = D3DCOLOR_XRGB(
				(int)(pDSO->m_fogColorRed * 255.0f),
				(int)(pDSO->m_fogColorGreen * 255.0f),
				(int)(pDSO->m_fogColorBlue * 255.0f));

			if (!m_lockBackColor)
				backgroundColor = currentfogColor;

			Vector3f fogColor(
				pDSO->m_fogColorRed,
				pDSO->m_fogColorGreen,
				pDSO->m_fogColorBlue);
			pSMO->SetFogColor(*reinterpret_cast<D3DVECTOR*>(&fogColor));
			pSMO->SetFogRange(D3DXVECTOR2(pDSO->m_fogStartRange, pDSO->m_fogEndRange));
		} else {
			m_fogStart = m_fogRange = 0.0f;
			pSMO->SetFogRange(D3DXVECTOR2(m_fogStart, m_fogRange));
		}
		pSMO->SetFogEnabled(fogEnabled); // drf - added
		pSMO->SetFogCanCull(m_fogCanCull); // drf - added

		D3DLIGHT9 light;
		D3DUtil_InitLight(
			light,
			D3DLIGHT_DIRECTIONAL,
			pDSO->m_sunColorRed,
			pDSO->m_sunColorGreen,
			pDSO->m_sunColorBlue);

		//build directional vector
		Vector3f directionalBuild;
		directionalBuild.x = 0.0f;
		directionalBuild.y = 0.0f;
		directionalBuild.z = -1.0f;

		Matrix44f matMod;

		ConvertRotationX(ToRadians(pDSO->m_sunRotationX), out(matMod));
		directionalBuild = TransformPoint_NonPerspective(matMod, directionalBuild);

		ConvertRotationY(ToRadians(pDSO->m_sunRotationY), out(matMod));
		directionalBuild = TransformPoint_NonPerspective(matMod, directionalBuild);

		light.Direction.x = directionalBuild.x;
		light.Direction.y = directionalBuild.y;
		light.Direction.z = directionalBuild.z;
		light.Diffuse.r = pDSO->m_sunColorRed;
		light.Diffuse.g = pDSO->m_sunColorGreen;
		light.Diffuse.b = pDSO->m_sunColorBlue;
		light.Specular.r = pDSO->m_sunColorRed;
		light.Specular.g = pDSO->m_sunColorGreen;
		light.Specular.b = pDSO->m_sunColorBlue;
		light.Specular.a = 1.0f;

		m_environmentTimeSystem->m_currentSunPosition.x = -directionalBuild.x;
		m_environmentTimeSystem->m_currentSunPosition.y = -directionalBuild.y;
		m_environmentTimeSystem->m_currentSunPosition.z = -directionalBuild.z;
		m_environmentTimeSystem->m_currentSunPosition.Normalize();
		m_environmentTimeSystem->m_currentSunPosition.x = m_environmentTimeSystem->m_currentSunPosition.x * pDSO->m_sunDistance;
		m_environmentTimeSystem->m_currentSunPosition.y = m_environmentTimeSystem->m_currentSunPosition.y * pDSO->m_sunDistance;
		m_environmentTimeSystem->m_currentSunPosition.z = m_environmentTimeSystem->m_currentSunPosition.z * pDSO->m_sunDistance;

		Vector3f temp;
		temp.x = m_environmentTimeSystem->m_currentSunPosition.x;
		temp.y = m_environmentTimeSystem->m_currentSunPosition.y;
		temp.z = m_environmentTimeSystem->m_currentSunPosition.z;
		temp.Normalize();

		pD3D->SetLight(0, &light);
		pD3D->LightEnable(0, TRUE);
		pSMO->SetSunLight(light);
		pSMO->SetDirLight(
			reinterpret_cast<D3DVECTOR&>(directionalBuild),
			pDSO->m_sunColorRed,
			pDSO->m_sunColorGreen,
			pDSO->m_sunColorBlue);

		Vector3f ambient(
			pDSO->m_ambientColorRed,
			pDSO->m_ambientColorGreen,
			pDSO->m_ambientColorBlue);
		pSMO->SetAmbLight(*reinterpret_cast<D3DVECTOR*>(&ambient));
	} else {
		pD3D->LightEnable(0, FALSE);
		pD3D->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(0, 0, 0));
		pSMO->SetFogEnabled(false);
		pSMO->SetFogRange(D3DXVECTOR2(0.0f, 0.0f));
	}

	*pBackgroundColorGlobal = backgroundColor;

	return TRUE;
}

void CEnvironmentObj::Serialize(CArchive& ar) {
	BOOL reservedBOOL = FALSE;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(CArchiveObjectSchemaVersion<CEnvironmentObj>);

		ar << m_fogOn
		   << (float)0.0 //m_fogDensity
		   << m_fogRange
		   << m_fogStart
		   << m_fogRed
		   << m_fogGreen
		   << m_fogBlue
		   << m_backRed
		   << m_backGreen
		   << m_backBlue
		   << (float)0.0 //m_emisiveRed
		   << (float)0.0 //m_emisiveGreen
		   << (float)0.0 //m_emisiveBlue
		   << m_sunPosition.x
		   << m_sunPosition.y
		   << m_sunPosition.z
		   << m_sunLightOn
		   << m_sunRed
		   << m_sunGreen
		   << m_sunBlue
		   << m_enableTimeSystem
		   << m_ambientLightRedDay
		   << m_ambientLightGreenDay
		   << m_ambientLightBlueDay
		   << m_ambientLightOn
		   << (float)0.0 //m_windDirectionX
		   << (float)0.0 //m_windDirectionY
		   << (float)0.0 //m_windDirectionZ
		   << (BOOL)FALSE //m_enableWindRandomizer
		   << (float)0.0 //m_maxWindStrength
		   << (float)0.0 //m_windChangeFrequency
		   << m_gravity
		   << m_atmosphereDensity
		   << m_seaLevel
		   << m_seaViscusDrag
		   << m_environmentTimeSystem;

		ar << m_lockBackColor
		   << reservedBOOL
		   << reservedBOOL
		   << reservedBOOL;

		ar << m_specularLightOn
		   << m_specularLightRed
		   << m_specularLightGreen
		   << m_specularLightBlue;

	} else {
		int version = ar.GetObjectSchema();

		float depFloat;
		BOOL depBool;
		ar >> m_fogOn >> depFloat // m_fogDensity
			>> m_fogRange >> m_fogStart >> m_fogRed >> m_fogGreen >> m_fogBlue >> m_backRed >> m_backGreen >> m_backBlue >> depFloat //m_emisiveRed
			>> depFloat //m_emisiveGreen
			>> depFloat //m_emisiveBlue
			>> m_sunPosition.x >> m_sunPosition.y >> m_sunPosition.z >> m_sunLightOn >> m_sunRed >> m_sunGreen >> m_sunBlue >> m_enableTimeSystem >> m_ambientLightRedDay >> m_ambientLightGreenDay >> m_ambientLightBlueDay >> m_ambientLightOn >> depFloat //m_windDirectionX
			>> depFloat //m_windDirectionY
			>> depFloat //m_windDirectionZ
			>> depBool //m_enableWindRandomizer
			>> depFloat //m_maxWindStrength
			>> depFloat //m_windChangeFrequency
			>> m_gravity >> m_atmosphereDensity >> m_seaLevel >> m_seaViscusDrag >> m_environmentTimeSystem;

		m_lockBackColor = false;

		if (version > 1) {
			ar >> m_lockBackColor >> reservedBOOL >> reservedBOOL >> reservedBOOL;
		}

		if (version > 2) {
			ar >> m_specularLightOn >> m_specularLightRed >> m_specularLightGreen >> m_specularLightBlue;
		}

		// ED-7251 - Camera Cull To Fog
		// ED-7771 - Disable Fog Culling For LoginMenu, Beach House & Island Deeds
		std::string fileName = StrFileNameExt(Utf16ToUtf8(ar.m_strFileName.GetString()));
		m_fogCanCull = true && !StrSameIgnoreCase(fileName, "LoginMenu.exg") && !StrSameIgnoreCase(fileName, "Beach_House_Deed.exg") && !StrSameIgnoreCase(fileName, "Island.exg") && !StrSameIgnoreCase(fileName, "Island_Premium.exg");
	}
}

#include "gsD3dvectorValue.h"

IMPLEMENT_SERIAL_SCHEMA(CEnvironmentObj, CKEPObject)

BEGIN_GETSET_IMPL(CEnvironmentObj, IDS_ENVIRONMENTOBJ_NAME)
GETSET_RGB(m_ambientLightRedDay, m_ambientLightGreenDay, m_ambientLightBlueDay, GS_FLAGS_DEFAULT, 1, 1, ENVIRONMENTOBJ, AMBIENTLIGHTCOLORDAY)
GETSET(m_ambientLightOn, gsBOOL, GS_FLAGS_DEFAULT, 1, 1, ENVIRONMENTOBJ, AMBIENTLIGHTON)
GETSET_RGB(m_sunRed, m_sunGreen, m_sunBlue, GS_FLAGS_DEFAULT, 1, 1, ENVIRONMENTOBJ, SUNCOLOR)
GETSET(m_sunLightOn, gsBOOL, GS_FLAGS_DEFAULT, 1, 1, ENVIRONMENTOBJ, SUNLIGHTON)
GETSET_D3DVECTOR(m_sunPosition, GS_FLAGS_DEFAULT, 2, 1, ENVIRONMENTOBJ, SUNPOSITION)
GETSET_RGB(m_fogRed, m_fogGreen, m_fogBlue, GS_FLAGS_DEFAULT, 1, 3, ENVIRONMENTOBJ, FOGCOLOR)
GETSET(m_fogOn, gsBOOL, GS_FLAGS_DEFAULT, 1, 3, ENVIRONMENTOBJ, FOGON)
GETSET_RGB(m_backRed, m_backGreen, m_backBlue, GS_FLAGS_DEFAULT, 1, 3, ENVIRONMENTOBJ, BACKCOLOR)
GETSET(m_lockBackColor, gsBool, GS_FLAGS_DEFAULT, 1, 3, ENVIRONMENTOBJ, LOCKBACKCOLOR)
GETSET_RANGE(m_fogStart, gsFloat, GS_FLAGS_DEFAULT, 1, 3, ENVIRONMENTOBJ, FOGSTART, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_fogRange, gsFloat, GS_FLAGS_DEFAULT, 1, 3, ENVIRONMENTOBJ, FOGRANGE, FLOAT_MIN, FLOAT_MAX)
GETSET(m_enableTimeSystem, gsBOOL, GS_FLAGS_DEFAULT, 1, 4, ENVIRONMENTOBJ, ENABLETIMESYSTEM)
GETSET_OBJ_PTR(m_environmentTimeSystem, GS_FLAGS_DEFAULT, 1, 4, ENVIRONMENTOBJ, ENVIRONMENTTIMESYSTEM, CTimeSystemObj)
GETSET_RANGE(m_gravity, gsFloat, GS_FLAGS_DEFAULT, 1, 8, ENVIRONMENTOBJ, GRAVITY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_seaLevel, gsFloat, GS_FLAGS_DEFAULT, 1, 8, ENVIRONMENTOBJ, SEALEVEL, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_seaViscusDrag, gsFloat, GS_FLAGS_DEFAULT, 1, 8, ENVIRONMENTOBJ, SEAVISCUSDRAG, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP