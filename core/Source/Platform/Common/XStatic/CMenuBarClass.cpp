/******************************************************************************
 CMenuBarClass.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CMenuBarClass.h"

CMenuBarObj::CMenuBarObj() {
	m_verticleStyle = FALSE;
	m_flip = FALSE;
	m_max = 1.0f;
	m_min = 0.0f;
	m_barLength = 0.4f;
	m_barWidth = 0.2f;
	m_xRoot = 0.0f;
	m_yRoot = 0.0f;
	m_cur = 0.5f;
	m_barDynamic = NULL;
	m_barStatic = NULL;
	m_visualReference = 0;
	m_barUseType = 0;
}

void CMenuBarObj::SafeDelete() {
	if (m_barDynamic) {
		m_barDynamic->SafeDelete();
		delete m_barDynamic;
		m_barDynamic = 0;
	}

	if (m_barStatic) {
		m_barStatic->SafeDelete();
		delete m_barStatic;
		m_barStatic = 0;
	}
}

void CMenuBarObj::SetStateByTwoAmounts(float cur, float max) {
	if (max == 0.0f)
		return;

	m_cur = cur / max;
}

void CMenuBarObj::SetStateByDirectValue(float cur) {
	m_cur = cur;
}

void CMenuBarObj::Clone(CMenuBarObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CMenuBarObj* copyLocal = new CMenuBarObj();

	copyLocal->m_verticleStyle = m_verticleStyle;
	copyLocal->m_flip = m_flip;
	copyLocal->m_max = m_max;
	copyLocal->m_min = m_min;
	copyLocal->m_barLength = m_barLength;
	copyLocal->m_barWidth = m_barWidth;
	copyLocal->m_xRoot = m_xRoot;
	copyLocal->m_yRoot = m_yRoot;
	copyLocal->m_visualReference = m_visualReference;
	copyLocal->m_barUseType = m_barUseType;

	if (m_barStatic)
		m_barStatic->Clone(&copyLocal->m_barStatic, g_pd3dDevice);

	if (m_barDynamic)
		m_barDynamic->Clone(&copyLocal->m_barDynamic, g_pd3dDevice);

	*clone = copyLocal;
}

void CMenuBarObj::Initialize(LPDIRECT3DDEVICE9 g_pd3dDevice) {
	if (!m_barStatic) {
		m_barStatic = new CEXMeshObj();
		m_barStatic->m_materialObject = new CMaterialObject();
		m_barStatic->m_materialObject->m_cullOn = FALSE;

		m_barStatic->m_materialObject->m_useMaterial.Emissive.r = 1.0f;
		m_barStatic->m_materialObject->m_useMaterial.Emissive.g = 1.0f;
		m_barStatic->m_materialObject->m_useMaterial.Emissive.b = 1.0f;
		m_barStatic->m_materialObject->m_useMaterial.Emissive.a = 1.0f;

		ABVERTEX7L* verts = new ABVERTEX7L[4];
		ZeroMemory(verts, sizeof(ABVERTEX7L) * 4);
		UINT indecies[6];
		ZeroMemory(indecies, sizeof(UINT) * 6);
		m_barStatic->m_abVertexArray.m_uvCount = 7;
		m_barStatic->m_validUvCount = 7;

		//indecie
		indecies[0] = 0;
		indecies[1] = 1;
		indecies[2] = 3;
		indecies[3] = 3;
		indecies[4] = 1;
		indecies[5] = 2;

		//N
		verts[0].nx = 0.0f;
		verts[0].ny = 0.0f;
		verts[0].nz = 3.0f;
		verts[1].nx = 0.0f;
		verts[1].ny = 0.0f;
		verts[1].nz = 3.0f;
		verts[2].nx = 0.0f;
		verts[2].ny = 0.0f;
		verts[2].nz = 3.0f;
		verts[3].nx = 0.0f;
		verts[3].ny = 0.0f;
		verts[3].nz = 3.0f;
		//UVSET1
		verts[0].tx0.tu = 0.0f;
		verts[0].tx0.tv = 0.0f;
		verts[1].tx0.tu = 1.0f;
		verts[1].tx0.tv = 0.0f;
		verts[2].tx0.tu = 1.0f;
		verts[2].tx0.tv = 1.0f;
		verts[3].tx0.tu = 0.0f;
		verts[3].tx0.tv = 1.0f;
		//UVSET2
		verts[0].tx1.tu = 0.0f;
		verts[0].tx1.tv = 0.0f;
		verts[1].tx1.tu = 1.0f;
		verts[1].tx1.tv = 0.0f;
		verts[2].tx1.tu = 1.0f;
		verts[2].tx1.tv = 1.0f;
		verts[3].tx1.tu = 0.0f;
		verts[3].tx1.tv = 1.0f;
		//UVSET3
		verts[0].tx2.tu = 0.0f;
		verts[0].tx2.tv = 0.0f;
		verts[1].tx2.tu = 1.0f;
		verts[1].tx2.tv = 0.0f;
		verts[2].tx2.tu = 1.0f;
		verts[2].tx2.tv = 1.0f;
		verts[3].tx2.tu = 0.0f;
		verts[3].tx2.tv = 1.0f;
		//UVSET4
		verts[0].tx3.tu = 0.0f;
		verts[0].tx3.tv = 0.0f;
		verts[1].tx3.tu = 1.0f;
		verts[1].tx3.tv = 0.0f;
		verts[2].tx3.tu = 1.0f;
		verts[2].tx3.tv = 1.0f;
		verts[3].tx3.tu = 0.0f;
		verts[3].tx3.tv = 1.0f;
		//UVSET5
		verts[0].tx4.tu = 0.0f;
		verts[0].tx4.tv = 0.0f;
		verts[1].tx4.tu = 1.0f;
		verts[1].tx4.tv = 0.0f;
		verts[2].tx4.tu = 1.0f;
		verts[2].tx4.tv = 1.0f;
		verts[3].tx4.tu = 0.0f;
		verts[3].tx4.tv = 1.0f;
		//UVSET6
		verts[0].tx5.tu = 0.0f;
		verts[0].tx5.tv = 0.0f;
		verts[1].tx5.tu = 1.0f;
		verts[1].tx5.tv = 0.0f;
		verts[2].tx5.tu = 1.0f;
		verts[2].tx5.tv = 1.0f;
		verts[3].tx5.tu = 0.0f;
		verts[3].tx5.tv = 1.0f;
		//UVSET7
		verts[0].tx6.tu = 0.0f;
		verts[0].tx6.tv = 0.0f;
		verts[1].tx6.tu = 1.0f;
		verts[1].tx6.tv = 0.0f;
		verts[2].tx6.tu = 1.0f;
		verts[2].tx6.tv = 1.0f;
		verts[3].tx6.tu = 0.0f;
		verts[3].tx6.tv = 1.0f;

		m_barStatic->m_abVertexArray.SetVertexIndexArray(indecies, 6, 4);
		m_barStatic->m_abVertexArray.m_vertexArray7 = verts;

		m_barStatic->InitBuffer(g_pd3dDevice, NULL);
	}

	if (!m_barDynamic) {
		m_barDynamic = new CEXMeshObj();
		m_barDynamic->m_materialObject = new CMaterialObject();
		m_barDynamic->m_materialObject->m_cullOn = FALSE;

		m_barDynamic->m_materialObject->m_useMaterial.Emissive.r = 1.0f;
		m_barDynamic->m_materialObject->m_useMaterial.Emissive.g = 1.0f;
		m_barDynamic->m_materialObject->m_useMaterial.Emissive.b = 1.0f;
		m_barDynamic->m_materialObject->m_useMaterial.Emissive.a = 1.0f;

		ABVERTEX7L* verts = new ABVERTEX7L[4];
		ZeroMemory(verts, sizeof(ABVERTEX7L) * 4);
		UINT indecies[6];
		ZeroMemory(indecies, sizeof(UINT) * 6);
		m_barDynamic->m_abVertexArray.m_uvCount = 7;
		m_barDynamic->m_validUvCount = 7;

		//indecie
		indecies[0] = 0;
		indecies[1] = 1;
		indecies[2] = 3;
		indecies[3] = 3;
		indecies[4] = 1;
		indecies[5] = 2;

		//N
		verts[0].nx = 0.0f;
		verts[0].ny = 0.0f;
		verts[0].nz = 1.0f;
		verts[1].nx = 0.0f;
		verts[1].ny = 0.0f;
		verts[1].nz = 1.0f;
		verts[2].nx = 0.0f;
		verts[2].ny = 0.0f;
		verts[2].nz = 1.0f;
		verts[3].nx = 0.0f;
		verts[3].ny = 0.0f;
		verts[3].nz = 1.0f;
		//UVSET1
		verts[0].tx0.tu = 0.0f;
		verts[0].tx0.tv = 0.0f;
		verts[1].tx0.tu = 1.0f;
		verts[1].tx0.tv = 0.0f;
		verts[2].tx0.tu = 1.0f;
		verts[2].tx0.tv = 1.0f;
		verts[3].tx0.tu = 0.0f;
		verts[3].tx0.tv = 1.0f;
		//UVSET2
		verts[0].tx1.tu = 0.0f;
		verts[0].tx1.tv = 0.0f;
		verts[1].tx1.tu = 1.0f;
		verts[1].tx1.tv = 0.0f;
		verts[2].tx1.tu = 1.0f;
		verts[2].tx1.tv = 1.0f;
		verts[3].tx1.tu = 0.0f;
		verts[3].tx1.tv = 1.0f;
		//UVSET3
		verts[0].tx2.tu = 0.0f;
		verts[0].tx2.tv = 0.0f;
		verts[1].tx2.tu = 1.0f;
		verts[1].tx2.tv = 0.0f;
		verts[2].tx2.tu = 1.0f;
		verts[2].tx2.tv = 1.0f;
		verts[3].tx2.tu = 0.0f;
		verts[3].tx2.tv = 1.0f;
		//UVSET4
		verts[0].tx3.tu = 0.0f;
		verts[0].tx3.tv = 0.0f;
		verts[1].tx3.tu = 1.0f;
		verts[1].tx3.tv = 0.0f;
		verts[2].tx3.tu = 1.0f;
		verts[2].tx3.tv = 1.0f;
		verts[3].tx3.tu = 0.0f;
		verts[3].tx3.tv = 1.0f;
		//UVSET5
		verts[0].tx4.tu = 0.0f;
		verts[0].tx4.tv = 0.0f;
		verts[1].tx4.tu = 1.0f;
		verts[1].tx4.tv = 0.0f;
		verts[2].tx4.tu = 1.0f;
		verts[2].tx4.tv = 1.0f;
		verts[3].tx4.tu = 0.0f;
		verts[3].tx4.tv = 1.0f;
		//UVSET6
		verts[0].tx5.tu = 0.0f;
		verts[0].tx5.tv = 0.0f;
		verts[1].tx5.tu = 1.0f;
		verts[1].tx5.tv = 0.0f;
		verts[2].tx5.tu = 1.0f;
		verts[2].tx5.tv = 1.0f;
		verts[3].tx5.tu = 0.0f;
		verts[3].tx5.tv = 1.0f;
		//UVSET7
		verts[0].tx6.tu = 0.0f;
		verts[0].tx6.tv = 0.0f;
		verts[1].tx6.tu = 1.0f;
		verts[1].tx6.tv = 0.0f;
		verts[2].tx6.tu = 1.0f;
		verts[2].tx6.tv = 1.0f;
		verts[3].tx6.tu = 0.0f;
		verts[3].tx6.tv = 1.0f;

		m_barDynamic->m_abVertexArray.SetVertexIndexArray(indecies, 6, 4);
		m_barDynamic->m_abVertexArray.m_vertexArray7 = verts;
		if (g_pd3dDevice)
			m_barDynamic->InitBuffer(g_pd3dDevice, NULL);
	}

	//update static
	int loop = 0;
	float curX = m_xRoot;
	float curY = m_yRoot;
	float inset = 3.0f;

	curY += (m_barWidth * .5f);
	m_barStatic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	loop++;
	curX += m_barLength;
	m_barStatic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	loop++;
	curY -= m_barWidth;
	m_barStatic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	loop++;
	curX -= m_barLength;
	m_barStatic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	if (g_pd3dDevice)
		m_barStatic->m_abVertexArray.ReinitializeDxVbuffersPositionOnly(g_pd3dDevice);
	//update dynamic
	loop = 0;
	curX = m_xRoot;
	curY = m_yRoot;

	curY += (m_barWidth * .5f);
	m_barDynamic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	loop++;
	curX += (m_barLength * m_cur);
	m_barDynamic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	loop++;
	curY -= m_barWidth;
	m_barDynamic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	loop++;
	curX -= (m_barLength * m_cur);
	m_barDynamic->m_abVertexArray.SetPositionXYZ(loop, curX, curY, inset);
	if (g_pd3dDevice)
		m_barDynamic->m_abVertexArray.ReinitializeDxVbuffersPositionOnly(g_pd3dDevice);
}

void CMenuBarObj::Render(LPDIRECT3DDEVICE9 g_pd3dDevice,
	CStateManagementObj* m_stateManager,
	TextureDatabase* TextureDataBase,
	float gl_adjust) {
	if (m_max == 0)
		return;

	Initialize(g_pd3dDevice);

	m_barStatic->m_materialObject->Callback();
	m_stateManager->SetBlendType(m_barStatic->m_materialObject->m_blendMode);
	m_stateManager->SetMaterial(&m_barStatic->m_materialObject->m_useMaterial);
	m_stateManager->SetDiffuseSource(m_barStatic->m_abVertexArray.m_advancedFixedMode);
	m_stateManager->SetVertexType(m_barStatic->m_abVertexArray.m_uvCount);

	m_stateManager->SetTextureState(m_barStatic->m_materialObject->m_texNameHash[0], 0, TextureDataBase);
	int loop;
	for (loop = 1; loop < 7; loop++) {
		m_stateManager->SetTextureState(m_barStatic->m_materialObject->m_texNameHash[loop], loop, TextureDataBase);
		m_stateManager->SetBlendLevel(loop, m_barStatic->m_materialObject->m_texBlendMethod[(loop - 1)]);
	}

	for (loop = 0; loop < 8; loop++) {
		m_stateManager->SetColorArg1Level(loop, m_barStatic->m_materialObject->m_colorArg1[loop]);
		m_stateManager->SetColorArg2Level(loop, m_barStatic->m_materialObject->m_colorArg2[loop]);
	}

	if (m_barStatic->m_materialObject->m_enableTextureMovement == TRUE) { //texture Movement
		for (int uvSet = 0; uvSet < MAX_UV_SETS; uvSet++) {
			if (m_barStatic->m_materialObject->m_enableSet[uvSet] == TRUE) {
				m_barStatic->m_abVertexArray.ApplyMoveTexture(
					m_barStatic->m_materialObject->m_textureMovement[uvSet].tu,
					m_barStatic->m_materialObject->m_textureMovement[uvSet].tv,
					uvSet,
					m_stateManager->m_currentVertexType);
			}
		}
	} //end texture Movement

	m_barStatic->Render(g_pd3dDevice, m_stateManager->m_currentVertexType, m_barStatic->m_chromeUvSet);

	m_barDynamic->m_materialObject->Callback();
	m_stateManager->SetBlendType(m_barDynamic->m_materialObject->m_blendMode);
	m_stateManager->SetMaterial(&m_barDynamic->m_materialObject->m_useMaterial);
	m_stateManager->SetDiffuseSource(m_barDynamic->m_abVertexArray.m_advancedFixedMode);
	m_stateManager->SetVertexType(m_barDynamic->m_abVertexArray.m_uvCount);

	m_stateManager->SetTextureState(m_barDynamic->m_materialObject->m_texNameHash[0], 0, TextureDataBase);
	for (loop = 1; loop < 7; loop++) {
		m_stateManager->SetTextureState(m_barDynamic->m_materialObject->m_texNameHash[loop], loop, TextureDataBase);
		m_stateManager->SetBlendLevel(loop, m_barDynamic->m_materialObject->m_texBlendMethod[(loop - 1)]);
	}

	for (loop = 0; loop < 8; loop++) {
		m_stateManager->SetColorArg1Level(loop, m_barDynamic->m_materialObject->m_colorArg1[loop]);
		m_stateManager->SetColorArg2Level(loop, m_barDynamic->m_materialObject->m_colorArg2[loop]);
	}

	if (m_barDynamic->m_materialObject->m_enableTextureMovement == TRUE) { //texture Movement
		for (int uvSet = 0; uvSet < MAX_UV_SETS; uvSet++) {
			if (m_barDynamic->m_materialObject->m_enableSet[uvSet] == TRUE) {
				m_barDynamic->m_abVertexArray.ApplyMoveTexture(
					m_barDynamic->m_materialObject->m_textureMovement[uvSet].tu,
					m_barDynamic->m_materialObject->m_textureMovement[uvSet].tv,
					uvSet,
					m_stateManager->m_currentVertexType);
			}
		}
	} //end texture Movement

	m_barDynamic->Render(g_pd3dDevice, m_stateManager->m_currentVertexType, m_barDynamic->m_chromeUvSet);
}

void CMenuBarObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CMenuBarObj* spnPtr = (CMenuBarObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CMenuBarObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_verticleStyle
		   << m_flip
		   << m_max
		   << m_min
		   << m_barLength
		   << m_barWidth
		   << m_xRoot
		   << m_yRoot
		   << m_barDynamic
		   << m_barStatic
		   << m_cur
		   << m_visualReference
		   << m_barUseType;
	} else {
		ar >> m_verticleStyle >> m_flip >> m_max >> m_min >> m_barLength >> m_barWidth >> m_xRoot >> m_yRoot >> m_barDynamic >> m_barStatic >> m_cur >> m_visualReference >> m_barUseType;
	}
}

IMPLEMENT_SERIAL(CMenuBarObj, CObject, 0)
IMPLEMENT_SERIAL(CMenuBarObjList, CObList, 0)

BEGIN_GETSET_IMPL(CMenuBarObj, IDS_MENUBAROBJ_NAME)
GETSET(m_verticleStyle, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, VERTICLESTYLE)
GETSET(m_flip, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, FLIP)
GETSET_RANGE(m_max, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, MAX, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_min, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, MIN, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_barLength, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, BARLENGTH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_barWidth, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, BARWIDTH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_xRoot, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, XROOT, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_yRoot, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, YROOT, FLOAT_MIN, FLOAT_MAX)
GETSET_OBJ_PTR(m_barDynamic, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, BARDYNAMIC, CEXMeshObj)
GETSET_OBJ_PTR(m_barStatic, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, BARSTATIC, CEXMeshObj)
GETSET_RANGE(m_cur, gsFloat, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, CUR, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_visualReference, gsInt, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, VISUALREFERENCE, INT_MIN, INT_MAX)
GETSET_RANGE(m_barUseType, gsInt, GS_FLAGS_DEFAULT, 0, 0, MENUBAROBJ, BARUSETYPE, INT_MIN, INT_MAX)
END_GETSET_IMPL
