///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CPlayerClass.h"
#include "CNoterietyClass.h"

namespace KEP {

CNoterietyObj::CNoterietyObj() {
	m_currentMurders = 0;
	m_currentMinuteCountSinceLastMurder = 0;
	m_currentAgressors = NULL;
}

void CNoterietyObj::SafeDelete() {
	if (m_currentAgressors) {
		m_currentAgressors->SafeDelete();
		delete m_currentAgressors;
		m_currentAgressors = 0;
	}
}

BOOL CNoterietyObj::AddAgressor(PLAYER_HANDLE handle,
	long agressorTimeout,
	const ChannelId& channel) {
	if (!m_currentAgressors)
		m_currentAgressors = new CAggressorObjList();

	if (m_currentAgressors->GetCount() > 10) {
		//max agressors
		m_currentAgressors->CacheTimedOutAgressors(agressorTimeout);
		return FALSE;
	}

	if (!m_currentAgressors->DoesHandleExist_IfSoRefresh(handle, agressorTimeout)) {
		//dont add twice
		CAggressorObj* newAgressor = new CAggressorObj();
		newAgressor->m_handle = handle;
		newAgressor->m_timeStamp = fTime::TimeMs();
		newAgressor->m_channel = channel;
		m_currentAgressors->AddTail(newAgressor);
	} //end dont add twice

	return TRUE;
}

BOOL CNoterietyObj::DoesHandleExistInAgressorDB(PLAYER_HANDLE handle, long timeout) {
	if (!m_currentAgressors)
		return FALSE;

	return m_currentAgressors->DoesHandleExist(handle, timeout);
}

void CNoterietyObj::Clone(CNoterietyObj** clone) {
	CNoterietyObj* copyLocal = new CNoterietyObj();
	copyLocal->m_currentMurders = m_currentMurders;
	copyLocal->m_currentMinuteCountSinceLastMurder = m_currentMinuteCountSinceLastMurder;

	if (m_currentAgressors)
		m_currentAgressors->Clone(&copyLocal->m_currentAgressors);

	*clone = copyLocal;
}

void CNoterietyObj::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_currentMurders
		   << m_currentMinuteCountSinceLastMurder;
	} else {
		ar >> m_currentMurders >> m_currentMinuteCountSinceLastMurder;
	}
}

void CNoterietyObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_currentMurders
		   << m_currentMinuteCountSinceLastMurder;
	} else {
		ar >> m_currentMurders >> m_currentMinuteCountSinceLastMurder;
	}
}

void CAggressorObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAggressorObj* aPtr = (CAggressorObj*)GetNext(posLoc);
		delete aPtr;
		aPtr = 0;
	}
	RemoveAll();
}

BOOL CAggressorObjList::DoesHandleExist_IfSoRefresh(PLAYER_HANDLE& handle, TimeMs timeout) {
	TimeMs currentTime = fTime::TimeMs();

	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CAggressorObj* aPtr = (CAggressorObj*)GetNext(posLoc);
		if (aPtr->m_handle == handle) {
			aPtr->m_timeStamp = currentTime;
			return TRUE;
		} else if ((currentTime - aPtr->m_timeStamp) > timeout) {
			//remove agressor
			delete aPtr;
			aPtr = 0;
			RemoveAt(posLast);
		} //end removeagressor
	}
	return FALSE;
}

BOOL CAggressorObjList::DoesHandleExist(PLAYER_HANDLE handle, TimeMs timeout) {
	TimeMs currentTime = fTime::TimeMs();
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CAggressorObj* aPtr = (CAggressorObj*)GetNext(posLoc);
		if ((currentTime - aPtr->m_timeStamp) > timeout) {
			//remove agressor
			delete aPtr;
			aPtr = 0;
			RemoveAt(posLast);
		} //end removeagressor
		else if (aPtr->m_handle == handle) {
			return TRUE;
		}
	}
	return FALSE;
}

void CAggressorObjList::CacheTimedOutAgressors(TimeMs timeout) {
	TimeMs currentTime = fTime::TimeMs();

	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CAggressorObj* aPtr = (CAggressorObj*)GetNext(posLoc);
		if ((currentTime - aPtr->m_timeStamp) > timeout) {
			//remove agressor
			delete aPtr;
			aPtr = 0;
			RemoveAt(posLast);
		} //end removeagressor
	}
}

void CAggressorObjList::FilterByChannelID(const ChannelId& id) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CAggressorObj* aPtr = (CAggressorObj*)GetNext(posLoc);
		if (aPtr->m_channel == id) {
			//remove agressor
			delete aPtr;
			aPtr = 0;
			RemoveAt(posLast);
		} //end removeagressor
	}
}

CAggressorObj::CAggressorObj() {
	m_handle = 0;
	m_timeStamp = 0;
	m_channel.clear();
}

void CAggressorObj::Clone(CAggressorObj** clone) {
	CAggressorObj* copyLocal = new CAggressorObj();
	copyLocal->m_handle = m_handle;
	copyLocal->m_timeStamp = m_timeStamp;
	copyLocal->m_channel = m_channel;

	*clone = copyLocal;
}

void CAggressorObjList::Clone(CAggressorObjList** clone) {
	CAggressorObjList* newList = new CAggressorObjList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CAggressorObj* bnPtr = (CAggressorObj*)GetNext(posLoc);
		CAggressorObj* newObject = NULL;
		bnPtr->Clone(&newObject);
		newList->AddTail(newObject);
	}
	*clone = newList;
}

IMPLEMENT_SERIAL(CNoterietyObj, CObject, 0)

BEGIN_GETSET_IMPL(CNoterietyObj, IDS_NOTERIETYOBJ_NAME)
GETSET_RANGE(m_currentMurders, gsInt, GS_FLAGS_DEFAULT, 0, 0, NOTERIETYOBJ, CURRENTMURDERS, INT_MIN, INT_MAX)
GETSET_RANGE(m_currentMinuteCountSinceLastMurder, gsLong, GS_FLAG_EXPOSE, 0, 0, NOTERIETYOBJ, CURRENTMINUTECOUNTSINCELASTMURDER, LONG_MIN, LONG_MAX)
END_GETSET_IMPL

} // namespace KEP