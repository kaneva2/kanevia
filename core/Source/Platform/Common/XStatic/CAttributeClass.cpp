///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <afxtempl.h>
#include "CAttributeClass.h"

namespace KEP {

CAttributeObj::CAttributeObj(
	int L_selfAttributeType,
	int L_selfAttributeValue,
	int L_otherAttributeType,
	int L_otherAttributeValue,
	int L_otherObjectSelect,
	BOOL L_vicinityBased,
	BOOL L_autoRetract,
	BOOL L_inUse,
	int L_retractDelay,
	int L_retractDelayVarial) {
	m_retractDelay = L_retractDelay;
	m_retractDelayVarial = L_retractDelayVarial;
	m_autoRetract = L_autoRetract;
	m_inUse = L_inUse;
	m_vicinityBased = L_vicinityBased;
	m_selfAttributeType = L_selfAttributeType;
	m_selfAttributeValue = L_selfAttributeValue;
	m_otherAttributeType = L_otherAttributeType;
	m_otherAttributeValue = L_otherAttributeValue;
	m_otherObjectSelect = L_otherObjectSelect;
}

void CAttributeObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_selfAttributeType
		   << m_selfAttributeValue
		   << m_otherAttributeType
		   << m_otherAttributeValue
		   << m_otherObjectSelect
		   << m_vicinityBased
		   << m_autoRetract
		   << m_retractDelay;
	} else {
		ar >> m_selfAttributeType >> m_selfAttributeValue >> m_otherAttributeType >> m_otherAttributeValue >> m_otherObjectSelect >> m_vicinityBased >> m_autoRetract >> m_retractDelay;

		m_retractDelayVarial = 0;
		m_inUse = FALSE;
	}
}

IMPLEMENT_SERIAL(CAttributeObj, CObject, 0)
IMPLEMENT_SERIAL(CAttributeObjList, CObList, 0)

BEGIN_GETSET_IMPL(CAttributeObj, IDS_ATTRIBUTEOBJ_NAME)
GETSET_RANGE(m_selfAttributeType, gsInt, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, SELFATTRIBUTETYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_selfAttributeValue, gsInt, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, SELFATTRIBUTEVALUE, INT_MIN, INT_MAX)
GETSET_RANGE(m_otherAttributeType, gsInt, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, OTHERATTRIBUTETYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_otherAttributeValue, gsInt, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, OTHERATTRIBUTEVALUE, INT_MIN, INT_MAX)
GETSET_RANGE(m_otherObjectSelect, gsInt, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, OTHEROBJECTSELECT, INT_MIN, INT_MAX)
GETSET(m_vicinityBased, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, VICINITYBASED)
GETSET(m_autoRetract, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, AUTORETRACT)
GETSET_RANGE(m_retractDelay, gsInt, GS_FLAGS_DEFAULT, 0, 0, ATTRIBUTEOBJ, RETRACTDELAY, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP