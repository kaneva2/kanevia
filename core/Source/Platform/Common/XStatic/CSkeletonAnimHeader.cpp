///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CSkeletonAnimHeader.h"

#include "common\include\CoreStatic\RuntimeSkeleton.h"
#include "common\include\CoreStatic\CSkeletonAnimation.h"
#include "common\include\CoreStatic\CBoneClass.h"
#include "common\include\IMemSizeGadget.h"

#include "common\include\LogHelper.h"
static LogInstance("ClientEngine");

namespace KEP {

CSkeletonAnimHeader::CSkeletonAnimHeader(RuntimeSkeleton* pRS) {
	m_animType = eAnimType::None;
	m_animVer = 0;
	m_animLooping = 1; // default is looping
	m_animMinTimeMs = (TimeMs)0.0;
	m_hashCode = 0;
	m_name = "";
	m_pAnimProxy = NULL;
	m_animGlid = GLID_INVALID;
	m_pRS = pRS;
	m_boneIndexMap = NULL;
	m_animSpeedFactor = 1.0f;
	m_animCropStartTimeMs = (TimeMs)0.0; // 0.0=disabled
	m_animCropEndTimeMs = (TimeMs)-1.0; // -1.0=disabled
	m_updated = false;
}

CSkeletonAnimHeader::CSkeletonAnimHeader(const CSkeletonAnimHeader& rhs) {
	m_animType = rhs.m_animType;
	m_animVer = rhs.m_animVer;
	m_animLooping = rhs.m_animLooping;
	m_animMinTimeMs = rhs.m_animMinTimeMs;
	m_hashCode = rhs.m_hashCode;
	m_name = rhs.m_name;
	m_pAnimProxy = rhs.m_pAnimProxy;
	m_animGlid = rhs.m_animGlid;
	m_pRS = rhs.m_pRS;
	m_boneIndexMap = NULL;
	m_animSpeedFactor = rhs.m_animSpeedFactor;
	m_animCropStartTimeMs = rhs.m_animCropStartTimeMs;
	m_animCropEndTimeMs = rhs.m_animCropEndTimeMs;
	m_updated = rhs.m_updated;
	m_animDurationMs = rhs.m_animDurationMs;
	m_animFrameTimeMs = rhs.m_animFrameTimeMs;
	if (rhs.m_boneIndexMap != NULL) {
		m_boneIndexMap = new UINT[m_pRS->getBoneCount()];
		for (UINT i = 0; i < m_pRS->getBoneCount(); i++) {
			m_boneIndexMap[i] = rhs.m_boneIndexMap[i];
		}
	}
}

CSkeletonAnimHeader::~CSkeletonAnimHeader() {
	// Don't delete anim; it's managed by the animation manager.
	m_pAnimProxy = NULL;
	if (m_boneIndexMap)
		delete[] m_boneIndexMap;
	m_boneIndexMap = NULL;
}

void CSkeletonAnimHeader::Read_v0(CArchive& ar) {
	int animType;
	ar.Read(&m_animVer, sizeof(m_animVer));
	ar.Read(&animType, sizeof(animType));
	m_animType = (eAnimType)animType;
	ar.Read(&m_animLooping, sizeof(m_animLooping));
	UINT minPlay = 0;
	ar.Read(&minPlay, sizeof(minPlay));
	m_animMinTimeMs = (TimeMs)minPlay;
	std::string::size_type nameLen;
	ar >> nameLen;
	ar.Read(&m_hashCode, sizeof(m_hashCode));

	char* tmpName = new char[nameLen + 1];
	memset(tmpName, 0, nameLen + 1);
	ar.Read(tmpName, nameLen);
	m_name = tmpName;
	delete[] tmpName;
}

void CSkeletonAnimHeader::Read_v1(CArchive& ar) {
	int glidInt;
	Read_v0(ar);
	UINT animDurationMs = 0;
	ar >> animDurationMs;
	m_animDurationMs = (TimeMs)animDurationMs;
	UINT msPerFrame = 0;
	ar >> msPerFrame;
	m_animFrameTimeMs = (TimeMs)msPerFrame;
	ar >> glidInt;
	m_animGlid = (GLID)glidInt;
}

void CSkeletonAnimHeader::Read(CArchive& ar) {
	UINT dataVersion;
	ar.Read(&dataVersion, sizeof(dataVersion));
	switch (dataVersion) {
		case 0:
			Read_v0(ar);
			break;

		case 1:
			Read_v1(ar);
			break;
	}
}

void CSkeletonAnimHeader::Write(CArchive& ar) {
	ar.Write(&m_schemaVersion, sizeof(m_schemaVersion));
	ar.Write(&m_animVer, sizeof(m_animVer));
	ar.Write(&m_animType, sizeof(m_animType));
	ar.Write(&m_animLooping, sizeof(m_animLooping));
	UINT minPlay = (UINT)m_animMinTimeMs;
	ar.Write(&minPlay, sizeof(minPlay));
	ar << m_name.size();
	ar.Write(&m_hashCode, sizeof(m_hashCode));
	ar.Write(m_name.c_str(), m_name.size());

	// v1 addition
	ar << (UINT)m_animDurationMs;
	ar << (UINT)m_animFrameTimeMs;
	ar << (int)m_animGlid;
}

CBoneAnimationObject* CSkeletonAnimHeader::GetBoneAnimationObj(UINT boneIndex) {
#ifdef _ClientOutput
	if (!m_pAnimProxy)
		return nullptr;

	auto pAnimData = m_pAnimProxy->GetAnimData();
	if (!pAnimData)
		return nullptr;

	// This Happens All The Time In Game Worlds
	if (boneIndex >= m_pRS->getBoneCount())
		return nullptr;

	if (!pAnimData->m_ppBoneAnimationObjs)
		return nullptr;

	// MSciabica - Create bone index map at first time use of AnimationProxy
	if (!m_boneIndexMap) {
		if (pAnimData->m_skeletonDef && !pAnimData->m_skeletonDef->IsEmpty()) {
			m_boneIndexMap = new UINT[m_pRS->getBoneCount()];
			bool boneMismatch = false;
			for (UINT i = 0; i < m_pRS->getBoneCount(); i++) {
				CBoneObject* pBO = m_pRS->getBoneByIndex(i);
				if (pBO) {
					m_boneIndexMap[i] = pAnimData->m_skeletonDef->GetBoneIndexByName(pBO->m_boneName, true, true);
				} else {
					boneMismatch = true;
					m_boneIndexMap[i] = -1;
				}
			}
		}
	}

	// Map bone index directly if mapping info not is available, else use mapped bone index.
	UINT mappedBoneIndex = m_boneIndexMap ? m_boneIndexMap[boneIndex] : boneIndex;
	if (mappedBoneIndex >= pAnimData->m_numBones)
		return nullptr;

	return pAnimData->m_ppBoneAnimationObjs[mappedBoneIndex];
#else
	return nullptr;
#endif
}

TimeMs CSkeletonAnimHeader::GetAnimDurationMs() const {
	// Value for animation length stored in bones is more reliable than that stored in the the AnimHeader,
	// so use that if available.
	if (!m_pAnimProxy)
		return m_animDurationMs;

	// The first bone sometimes has a different animation length than the others, so get the value from the
	// last bone.
	auto numBones = GetNumBones();
	if (numBones == 0)
		return m_animDurationMs;
	auto pAnimData = m_pAnimProxy->GetAnimData();
	return pAnimData ? pAnimData->m_ppBoneAnimationObjs[numBones - 1]->m_animDurationMs : m_animDurationMs;
}

UINT CSkeletonAnimHeader::GetNumBones() const {
#ifdef _ClientOutput
	if (!m_pAnimProxy)
		return 0;
	auto pAnimData = m_pAnimProxy->GetAnimData();
	if (!pAnimData)
		return 0;
	return pAnimData->m_numBones;
#else
	return 0;
#endif
}

bool CSkeletonAnimHeader::IsDataReady() const {
#ifdef _ClientOutput
	return m_pAnimProxy != NULL && m_pAnimProxy->HasAnimData();
#else
	return true;
#endif
}

bool CSkeletonAnimHeader::HasLoadError() const {
	return !m_pAnimProxy || (m_pAnimProxy->GetState() == IResource::State::ERRORED);
}

bool CSkeletonAnimHeader::LoadData() {
#ifdef _ClientOutput
	if (!m_pAnimProxy)
		return false;

	const AnimationProxy::AnimationData* animData = m_pAnimProxy->GetAnimData();
	if (!animData)
		return false;

	if (!m_updated) {
		m_animDurationMs = m_pAnimProxy->GetAnimDurationMs();
		if (animData->m_animLooping == (UINT)-1) { // v0 AP data does not carry looping flag (-1 in that case)
		} else {
			// Get looping flag from AnimationProxy
			m_animLooping = m_pAnimProxy->GetAnimLooping();
		}

		UpdateAnimMinTimeMs();
		m_updated = true;
	}

	return true;

#else
	return true;
#endif
}

bool CSkeletonAnimHeader::UpdateAnimMinTimeMs() {
	// Update Non-Kaneva & Local Animations Only
	if (IS_KANEVA_GLID(m_animGlid))
		return false;

	// if minPlay not set and not-looping, try determine from attributes
	if (!m_animLooping) {
		// Set to full length
		auto animDurationMs = GetAnimDurationMs();
		m_animMinTimeMs = animDurationMs;

		// Adjust For Crop Start Time (0.0=disabled)
		TimeMs cropS = GetAnimCropStartTimeMs();
		if (cropS > 0.0)
			m_animMinTimeMs -= cropS;

		// Adjust For Crop End Time (-1.0=disabled)
		TimeMs cropE = GetAnimCropEndTimeMs();
		if (cropE != -1.0)
			m_animMinTimeMs -= animDurationMs - cropE;

		// Scaling
		m_animMinTimeMs /= m_animSpeedFactor;

		// at least 1 ms ( 0 = forever )
		if (m_animMinTimeMs <= 0.0)
			m_animMinTimeMs = 1.0;
	} else {
		m_animMinTimeMs = 0.0;
	}

	return true;
}

void CSkeletonAnimHeader::GetMemSizeBoneIndexMap(IMemSizeGadget* pMemSizeGadget) const {
	if (m_pRS) {
		pMemSizeGadget->AddObjectSizeof(m_boneIndexMap, m_pRS->getBoneCount());
	}
}

void CSkeletonAnimHeader::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pAnimProxy);
		pMemSizeGadget->AddObject(m_pRS);
		pMemSizeGadget->AddObject(m_name);
	}

	// Internal getMemSize function
	GetMemSizeBoneIndexMap(pMemSizeGadget);
}

} // namespace KEP
