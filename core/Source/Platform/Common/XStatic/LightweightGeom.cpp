///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LightweightGeom.h"
#include "BoundBox.h"
#include "ObjectType.h"
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "ReMesh.h"
#include "ReD3DX9StaticMesh.h"
#include "ReMaterial.h"
#include "RenderEngine/Utils/ReUtils.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/Primitive.h"
#include <map>

namespace KEP {

namespace LWG {

class Geom : public IGeom {
public:
	Geom() :
			m_uniqueId(++s_nextGeomId),
			m_referenceType(ObjectType::NONE), // init as undefined (no pre-existing ref matrix)
			m_referenceObjId(0),
			m_drawMode(F_DRAW_WIREFRAME),
			m_visible(false),
			m_reMesh(NULL),
			m_material(s_defaultMaterial) {
		m_matrix.MakeIdentity();
		m_reWorldXForm.MakeIdentity();
		m_reWorldXFormInvTrn.MakeIdentity();
		m_reViewProjXForm.MakeIdentity();
	}

	~Geom() {
		if (m_reMesh)
			delete m_reMesh;
		m_reMesh = NULL;
	}

	virtual int getUniqueId() const override { return m_uniqueId; }

	virtual void setReferenceObj(ObjectType refType, int refObjId) override {
		m_referenceType = refType;
		m_referenceObjId = refObjId;
	}

	virtual void getReferenceObj(ObjectType& refType, int& refObjId) const override {
		refType = m_referenceType;
		refObjId = m_referenceObjId;
	}

	Matrix44f getReferenceTransform() const {
		Matrix44f out;
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return out;
		if (m_referenceType == ObjectType::NONE || !pICE->ObjectGetWorldTransformation(static_cast<ObjectType>(m_referenceType), m_referenceObjId, out))
			out.MakeIdentity();
		return out;
	}

	virtual void setPosition(const Vector3f& pos) override { m_matrix.SetRowW(pos); }
	virtual Vector3f getPosition() const override { return m_matrix.GetRowW<3>(); }

	virtual void setRotationAngles(const Vector3f& rotAngles) override {
		Matrix44f rot;
		ConvertRotationXYZ(rotAngles, out(rot));

		for (size_t i = 0; i < 3; i++) {
			m_matrix.GetRow<3>(i) = rot.GetRow<3>(i) * m_matrix.GetRow<3>(i).Length();
		}
	}

	virtual void setScale(const Vector3f& scale) override {
		for (size_t i = 0; i < 3; i++) {
			m_matrix.GetRow<3>(i) *= scale[i] / m_matrix.GetRow<3>(i).Length();
		}
	}

	virtual bool isVisible() const override { return m_visible; }
	virtual void setVisible(bool val) override { m_visible = val; }

	virtual void setMaterial(const Material& material) override { m_material = material; }
	virtual const Material& getMaterial() const override { return m_material; }

	virtual void setDrawMode(int mode) override { m_drawMode = mode; }
	virtual int getDrawMode() const override { return m_drawMode; }

	void createReMesh() {
		ReD3DX9DeviceState* dev = NULL;

		auto pICE = IClientEngine::Instance();
		if (pICE)
			dev = pICE->GetReDeviceState();
		else
			dev = ReD3DX9DeviceState::Instance();

		m_reMesh = new ReD3DX9StaticMesh(dev);

		// create the ReMaterial
		ReRenderStateData data;

		// ReRenderStateData::Mat
		data.Mat = m_material;
		// ReRenderStateData::Mode
		data.Mode.Alphaenable = (m_drawMode & F_ALPHA_BLENDING) != 0 ? TRUE : FALSE;
		data.Mode.Alphatestenable = (m_drawMode & F_ALPHA_TESTING) != 0 ? TRUE : FALSE;
		data.Mode.CullMode = (m_drawMode & F_CULL_BACK) != 0 ? D3DCULL_CCW : (m_drawMode & F_CULL_FRONT) != 0 ? D3DCULL_CW : D3DCULL_NONE;
		data.Mode.FogFilter = FALSE; // not used
		data.Mode.Shademode = 0; // not used
		data.Mode.ZBias = 0;
		data.Mode.Zenable = (m_drawMode & F_ALWAY_ON_TOP) != 0 ? FALSE : TRUE;
		data.Mode.Zwriteenable = data.Mode.Zenable;
		data.Mode.DrawWireFrame = (m_drawMode & F_DRAW_WIREFRAME) != 0;
		// ReRenderStateData::Blend
		if (data.Mode.Alphaenable) {
			data.Blend.Srcblend = D3DBLEND_SRCALPHA;
			data.Blend.Dstblend = D3DBLEND_INVSRCALPHA;
			data.SortOrder = 1 << 4;
			data.Mode.Zwriteenable = FALSE;
		} else {
			data.Blend.Srcblend = D3DBLEND_DESTCOLOR;
			data.Blend.Dstblend = D3DBLEND_ONE;
			data.SortOrder = 0;
		}
		// ReRenderStateData::ColorSrc
		data.ColorSrc = ReColorSource::MATERIAL;
		// ReRenderStateData::TexPtr, ResPtr, Tex, NumTexStages
		// Everything initialized except we need least one texture state (mandatory)
		data.NumTexStages = 1;
		data.Tex[0].ColorOp = D3DTOP_SELECTARG2;
		data.Tex[0].AlphaOp = D3DTOP_SELECTARG2;
		data.Tex[0].ColorArg1 = D3DTA_DIFFUSE;
		data.Tex[0].ColorArg2 = D3DTA_CURRENT;

		//get the hash value of the tex state
		data.TexHash[0] = ReHash((BYTE*)&data.Tex[0], sizeof(ReTexProps), 0);
		// get the hash value of the material state
		int intMat[4][4] = { // get an int version of material props for hash (eliminates floating point error)
			{ lrint(data.Mat.Diffuse.x * 100), lrint(data.Mat.Diffuse.y * 100), lrint(data.Mat.Diffuse.z * 100), lrint(data.Mat.Diffuse.w * 100) },
			{ lrint(data.Mat.Ambient.x * 100), lrint(data.Mat.Ambient.y * 100), lrint(data.Mat.Ambient.z * 100), lrint(data.Mat.Ambient.w * 100) },
			{ lrint(data.Mat.Specular.x * 100), lrint(data.Mat.Specular.y * 100), lrint(data.Mat.Specular.z * 100), lrint(data.Mat.Specular.w * 100) },
			{ lrint(data.Mat.Emissive.x * 100), lrint(data.Mat.Emissive.y * 100), lrint(data.Mat.Emissive.z * 100), lrint(data.Mat.Emissive.w * 100) }
		};
		data.MatHash = ReHash((BYTE*)&intMat[0][0], 16 * sizeof(int), 0);
		// get the hash value of the blend state
		data.BlendHash = ReHash((BYTE*)&data.Blend, sizeof(ReBlendProps), 0);
		// get the hash value of the mode state
		data.ModeHash = ReHash((BYTE*)&data.Mode, sizeof(ReModeProps), 0);

		ReMaterialPtr reMat(new ReD3DX9Material(dev, &data));
		m_reMesh->SetMaterial(reMat);

		if (!buildMeshData(m_reMesh)) {
			delete m_reMesh;
			m_reMesh = NULL;
			return;
		}

		ReMeshData* pData = NULL;
		m_reMesh->Access(&pData);

		if (pData->NumV > 0) {
			// fill in the UV and compute bounding box
			for (DWORD i = 0; i < pData->NumV; i++) {
				pData->pUv[i].Set(0, 0); // dummy value
				m_boundBox.merge(pData->pP[i]);
			}
		}

		// build the internal data
		m_reMesh->Build();

		// Call CreateShader to Obtain m_VertexType according to Charles.
		// Otherwise newly imported mesh won't render until zone reloaded. [Yanfeng 11/2008]
		m_reMesh->CreateShader(NULL);

		m_reWorldXForm.MakeIdentity();
		m_reWorldXFormInvTrn.MakeIdentity();
		m_reViewProjXForm.MakeIdentity();

		m_reMesh->SetWorldMatrix(&m_reWorldXForm);
		m_reMesh->SetWorldInvTransposeMatrix(&m_reWorldXFormInvTrn);
		m_reMesh->SetWorldViewProjMatrix(&m_reViewProjXForm);
		m_reMesh->SetLightCache(NULL);
	}

	virtual bool buildMeshData(ReMesh* pReMesh) = 0;

	virtual bool render(std::vector<ReMesh*>& meshes) override {
		if (!m_visible) {
			return false;
		}

		if (m_reMesh == NULL)
			createReMesh(); // First-time render function call, create remesh now

		if (m_reMesh == NULL)
			return false;

		// update matrices
		ASSERT(m_reMesh->GetWorldMatrix());

		m_reWorldXForm = m_matrix * getReferenceTransform();
		Matrix44f tmp;
		tmp.MakeInverseOf(m_reWorldXForm);
		m_reWorldXFormInvTrn.MakeTransposeOf(tmp);
		m_reMesh->SetRadius(std::max(std::max(m_boundBox.getSizeX(), m_boundBox.getSizeY()), m_boundBox.getSizeZ()) / 2);

		meshes.push_back(m_reMesh);
		return true;
	}

private:
	int m_uniqueId;
	BoundBox m_boundBox; // pre-xform bound box
	ObjectType m_referenceType;
	int m_referenceObjId; // reference object where local transformation is based on
	Matrix44f m_matrix;
	Material m_material;
	int m_drawMode;
	bool m_visible;

	// Rendering states
	ReMesh* m_reMesh; // Remesh for rendering
	Matrix44f m_reWorldXForm, m_reWorldXFormInvTrn, m_reViewProjXForm; // Transformation matrix for rendering (need to be updated every frame)

private:
	static std::atomic<int> s_nextGeomId;
	static Material s_defaultMaterial;
};

std::atomic<int> Geom::s_nextGeomId = 0;
Material Geom::s_defaultMaterial(
	TColor<float>(1.0f, 1.0f, 1.0f, 1.0f), // Diffuse
	TColor<float>(1.0f, 1.0f, 1.0f, 1.0f), // Ambient
	TColor<float>(0.0f, 0.0f, 0.0f, 1.0f), // Specular
	TColor<float>(1.0f, 1.0f, 1.0f, 1.0f), // Emissive
	1.0f);

class StandardGeom : public Geom {
public:
	StandardGeom(const PrimitiveGeomArgs<float>& args) :
			m_pShape(std::shared_ptr<Primitive<float>>(args.createPrimitive())) {}

	virtual bool buildMeshData(ReMesh* pReMesh) override {
		assert(m_pShape);

		ReMeshData* pData = NULL;
		pReMesh->Access(&pData);

		// obtain mesh
		Primitive<float>::TriList triangles;
		m_pShape->GenerateVisualMesh(triangles);

		unsigned int numVerts = triangles.positions.size();
		assert(numVerts % 3 == 0);

		// fill in mesh array data
		pData->NumV = numVerts;
		pData->NumI = numVerts;
		pData->PrimType = ReMeshPrimType::TRILIST;
		pData->NumUV = 1; // at least 1 otherwise won't render

		// allocate the internal data arrays
		if (!pReMesh->Allocate()) {
			assert(false);
			return false;
		}

		// fill in the index array
		for (DWORD i = 0; i < pData->NumI; i++) {
			pData->pI[i] = i;
		}

		if (numVerts > 0) {
			// bake vertices
			Matrix44f primMatrix;
			m_pShape->GetVisualMeshMatrix(primMatrix);

			TransformPoints(primMatrix, &triangles.positions[0], sizeof(Vector3f), pData->pP, sizeof(Vector3f), numVerts);
			TransformVectors(primMatrix, &triangles.normals[0], sizeof(Vector3f), pData->pN, sizeof(Vector3f), numVerts);
		}

		return true;
	}

private:
	std::shared_ptr<Primitive<float>> m_pShape;
};

class CustomGeom : public Geom {
public:
	CustomGeom(ReMeshPrimType primType, const std::vector<float>& positions, const std::vector<float>& normals) :
			m_primType(primType) {
		assert(!positions.empty());
		assert(positions.size() % 3 == 0);
		assert(positions.size() == normals.size() || normals.empty());

		m_positions.resize(positions.size() / 3);
		const Vector3f* pPositions = reinterpret_cast<const Vector3f*>(&positions[0]);
		std::copy(pPositions, pPositions + m_positions.size(), m_positions.begin());

		if (!normals.empty()) {
			m_normals.resize(normals.size() / 3);
			const Vector3f* pNormals = reinterpret_cast<const Vector3f*>(&normals[0]);
			std::copy(pNormals, pNormals + m_normals.size(), m_normals.begin());
		}
	}

	virtual bool buildMeshData(ReMesh* pReMesh) override {
		ReMeshData* pData = NULL;
		pReMesh->Access(&pData);

		unsigned int numVerts = m_positions.size();

		// fill in mesh array data
		pData->NumV = numVerts;
		pData->NumI = numVerts;
		pData->PrimType = m_primType;
		pData->NumUV = 1; // at least 1 otherwise won't render

		// allocate the internal data arrays
		if (!pReMesh->Allocate()) {
			assert(false);
			return false;
		}

		// fill in the index array
		for (DWORD i = 0; i < pData->NumI; i++) {
			pData->pI[i] = i;
		}

		if (numVerts > 0) {
			Vector3f* pPositions = reinterpret_cast<Vector3f*>(pData->pP);
			std::copy(m_positions.cbegin(), m_positions.cend(), pPositions);

			Vector3f* pNormals = reinterpret_cast<Vector3f*>(pData->pN);
			if (!m_normals.empty()) {
				assert(m_normals.size() == m_positions.size());
				std::copy(m_normals.cbegin(), m_normals.cend(), pNormals);
			} else {
				// Normals not provided
				std::fill(pNormals, pNormals + m_positions.size(), Vector3f(0.0f, 1.0f, 0.0f));
			}
		}

		return true;
	}

private:
	ReMeshPrimType m_primType;
	std::vector<Vector3f> m_positions;
	std::vector<Vector3f> m_normals;
};

class GeomFactory : public IGeomFactory {
public:
	GeomFactory() {
		registerSupportedTypes();
	}

	virtual IGeom* createStandardGeometry(const PrimitiveGeomArgs<float>& args) {
		if (m_supportedStandardGeomTypes.find(args.type) != m_supportedStandardGeomTypes.end()) {
			return new StandardGeom(args);
		}
		return nullptr;
	}

	virtual IGeom* createCustomGeometry(int geomType, const std::vector<float>& positions, const std::vector<float>& normals) {
		// Vertex count must match or no normals
		assert(positions.size() == normals.size() || normals.size() == 0);
		if (!normals.empty() && positions.size() != normals.size()) {
			return nullptr;
		}

		// Positions and normals are list of vectors (3 floats per vector)
		assert(positions.size() % 3 == 0);
		if (positions.size() % 3 != 0) {
			return nullptr;
		}

		// At least one vertex
		assert(!positions.empty());
		if (positions.empty()) {
			return nullptr;
		}

		auto itr = m_supportedCustomGeomTypes.find(geomType);
		if (itr != m_supportedCustomGeomTypes.end()) {
			return itr->second(geomType, positions, normals);
		}

		return nullptr;
	}

protected:
	virtual void registerSupportedTypes() {
		for (size_t i = GT_STANDARD_BASE; i < GT_STANDARD_BASE + PrimitiveType::NUM_TYPES; i++) {
			m_supportedStandardGeomTypes.insert(i);
		}

		m_supportedCustomGeomTypes.insert(std::make_pair(GT_TRIANGLES, &buildTriListMesh));
		m_supportedCustomGeomTypes.insert(std::make_pair(GT_LINES, &buildLineList));
		m_supportedCustomGeomTypes.insert(std::make_pair(GT_LINESTRIP, &buildLineStrip));
	}

	static IGeom* buildTriListMesh(int, const std::vector<float>& positions, const std::vector<float>& normals) {
		size_t numVerts = positions.size() / 3;
		assert(numVerts % 3 == 0);
		if (numVerts % 3 == 0) {
			//size_t numPrims = numVerts / 3;
			return new CustomGeom(ReMeshPrimType::TRILIST, positions, normals);
		}
		return nullptr;
	}

	static IGeom* buildLineList(int, const std::vector<float>& positions, const std::vector<float>& normals) {
		size_t numVerts = positions.size() / 3;
		assert(numVerts % 2 == 0);
		if (numVerts % 2 == 0) {
			//size_t numPrims = numVerts / 2;
			return new CustomGeom(ReMeshPrimType::LINELIST, positions, normals);
		}
		return nullptr;
	}

	static IGeom* buildLineStrip(int, const std::vector<float>& positions, const std::vector<float>& normals) {
		size_t numVerts = positions.size() / 3;
		assert(numVerts > 1);
		if (numVerts > 1) {
			//size_t numPrims = numVerts - 1;
			return new CustomGeom(ReMeshPrimType::LINESTRIP, positions, normals);
		}
		return nullptr;
	}

protected:
	std::set<int> m_supportedStandardGeomTypes;
	std::map<int, std::function<IGeom*(int, const std::vector<float>&, const std::vector<float>&)>> m_supportedCustomGeomTypes;
};

static IGeomFactory* geomFactory = NULL;

void InitModule() {
	geomFactory = new GeomFactory();
}

void CleanupModule() {
	if (geomFactory)
		delete geomFactory;

	geomFactory = NULL;
}

} // namespace LWG

} // namespace KEP