///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include <afxtempl.h>
#include "CGFONTCLASS.h"

namespace KEP {

CGFontObj::CGFontObj() {
	m_fontTableIndex = 0;
	m_fontTableName = "NONE";
	m_fontName = "n/a";
}

void CGFontObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_fontTableIndex
		   << m_fontTableName
		   << m_fontName;
	} else {
		ar >> m_fontTableIndex >> m_fontTableName >> m_fontName;
	}
}

void CGFontObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) { //account list
		CGFontObj* cgPtr = (CGFontObj*)GetNext(posLoc);
		delete cgPtr;
		cgPtr = 0;
	}
	RemoveAll();
}

IMPLEMENT_SERIAL(CGFontObj, CObject, 0)
IMPLEMENT_SERIAL(CGFontObjList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CGFontObj, IDS_GFONTOBJ_NAME)
GETSET_RANGE(m_fontTableIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, GFONTOBJ, FONTTABLEINDEX, INT_MIN, INT_MAX)
GETSET_MAX(m_fontTableName, gsCString, GS_FLAGS_DEFAULT, 0, 0, GFONTOBJ, FONTTABLENAME, STRLEN_MAX)
GETSET_MAX(m_fontName, gsCString, GS_FLAGS_DEFAULT, 0, 0, GFONTOBJ, FONTNAME, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP