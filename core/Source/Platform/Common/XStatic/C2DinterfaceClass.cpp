/******************************************************************************
 C2DinterfaceClass.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "resource.h"
#include <d3d8.h>
#include <afxtempl.h>
#include "C2DinterfaceClass.h"

//------------visual list------------------//
C2DvisObj::C2DvisObj(LPDIRECTDRAWSURFACE7 L_visSurface,
	LPDIRECTDRAWSURFACE7 L_baseSurface,
	LPDIRECTDRAWSURFACE7 L_upSurface,
	LPDIRECTDRAWSURFACE7 L_downSurface,
	IDXSurface* L_forground,
	IDXSurface* L_background,
	IDXSurface* L_onSurfModify,
	IDXSurface* L_outSurface,
	IDXSurfaceModifier* L_surfaceModifier,
	BOOL L_upState,
	BOOL L_fadeIn,
	BOOL L_initialTranslucentOnly,
	float L_translucency,
	int L_redKeySrc,
	int L_greenKeySrc,
	int L_blueKeySrc,
	int L_attribute,
	RECT L_positionSize,
	CString L_fileNameUp,
	CString L_fileNameDown,
	RECT L_percentageRect,
	BOOL L_actuated,
	float L_fadeVarial,
	float L_fadeSpeed,
	int L_attribMiscInt,
	CString L_attribMiscTxt) {
	m_attribMiscInt = L_attribMiscInt;
	m_attribMiscTxt = L_attribMiscTxt;
	m_fadeSpeed = L_fadeSpeed;
	m_fadeVarial = L_fadeVarial;
	m_actuated = L_actuated;
	m_percentageRect = L_percentageRect;
	m_visSurface = L_visSurface;
	m_baseSurface = L_baseSurface;
	m_upSurface = L_upSurface;
	m_downSurface = L_downSurface;
	m_forground = L_forground;
	m_background = L_background;
	m_onSurfModify = L_onSurfModify;
	m_outSurface = L_outSurface;
	m_surfaceModifier = L_surfaceModifier;
	m_upState = L_upState;
	m_fadeIn = L_fadeIn;
	m_initialTranslucentOnly = L_initialTranslucentOnly;
	m_translucency = L_translucency;
	m_redKeySrc = L_redKeySrc;
	m_greenKeySrc = L_greenKeySrc;
	m_blueKeySrc = L_blueKeySrc;
	m_attribute = L_attribute;
	m_positionSize = L_positionSize;
	m_fileNameUp = L_fileNameUp;
	m_fileNameDown = L_fileNameDown;
}

void C2DvisObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_upState
		   << m_fadeIn
		   << m_initialTranslucentOnly
		   << m_translucency
		   << m_redKeySrc
		   << m_greenKeySrc
		   << m_blueKeySrc
		   << m_attribute
		   << m_positionSize
		   << m_fileNameUp
		   << m_fileNameDown
		   << m_percentageRect
		   << m_fadeSpeed
		   << m_attribMiscInt
		   << m_attribMiscTxt;
	} else {
		ar >> m_upState >> m_fadeIn >> m_initialTranslucentOnly >> m_translucency >> m_redKeySrc >> m_greenKeySrc >> m_blueKeySrc >> m_attribute >> m_positionSize >> m_fileNameUp >> m_fileNameDown >> m_percentageRect >> m_fadeSpeed >> m_attribMiscInt >> m_attribMiscTxt;

		m_visSurface = 0;
		m_baseSurface = 0;
		m_upSurface = 0;
		m_downSurface = 0;
		m_forground = 0;
		m_background = 0;
		m_onSurfModify = 0;
		m_outSurface = 0;
		m_surfaceModifier = 0;
		m_fadeVarial = 0.0f;
		m_actuated = FALSE;
	}
}

IMPLEMENT_SERIAL(C2DvisObj, CObject, 0)
IMPLEMENT_SERIAL(C2DvisObjList, CObList, 0)

//-----------main list---------------------//
C2DinterfaceObj::C2DinterfaceObj(CString L_displayName,
	BOOL L_inUse,
	C2DvisObjList* L_displayCollection,
	int L_displType,
	int L_inheritMiscInt) {
	m_inheritMiscInt = L_inheritMiscInt;
	m_displType = L_displType;
	m_displayName = L_displayName;
	m_inUse = L_inUse;
	m_displayCollection = L_displayCollection;
}

void C2DinterfaceObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_inUse
		   << m_displayCollection
		   << m_displayName
		   << m_displType;
	} else {
		ar >> m_inUse >> m_displayCollection >> m_displayName >> m_displType;

		m_inheritMiscInt = -1;
		m_inUse = FALSE;
	}
}

IMPLEMENT_SERIAL(C2DinterfaceObj, CObject, 0)
IMPLEMENT_SERIAL(C2DinterfaceObjList, CObList, 0)

// begin serialize.pl generated code
BEGIN_GETSET_IMPL(C2DvisObj, IDS_C2DVISOBJ_NAME)
GETSET(m_upState, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, , C2DVISOBJ, UPSTATE)
GETSET(m_fadeIn, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, , C2DVISOBJ, FADEIN)
GETSET(m_initialTranslucentOnly, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, INITIALTRANSLUCENTONLY)
GETSET_RANGE(m_translucency, gsFloat, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, TRANSLUCENCY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_redKeySrc, gsInt, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, REDKEYSRC, INT_MIN, INT_MAX)
GETSET_RANGE(m_greenKeySrc, gsInt, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, GREENKEYSRC, INT_MIN, INT_MAX)
GETSET_RANGE(m_blueKeySrc, gsInt, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, BLUEKEYSRC, INT_MIN, INT_MAX)
GETSET_RANGE(m_attribute, gsInt, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, ATTRIBUTE, INT_MIN, INT_MAX)
GETSET(m_positionSize, gsRect, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, POSITIONSIZE)
GETSET_MAX(m_fileNameUp, gsCString, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, FILENAMEUP, STRLEN_MAX)
GETSET_MAX(m_fileNameDown, gsCString, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, FILENAMEDOWN, STRLEN_MAX)
GETSET(m_percentageRect, gsRect, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, PERCENTAGERECT)
GETSET_RANGE(m_fadeSpeed, gsFloat, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, FADESPEED, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_attribMiscInt, gsInt, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, ATTRIBMISCINT, INT_MIN, INT_MAX)
GETSET_MAX(m_attribMiscTxt, gsCString, GS_FLAGS_DEFAULT, 0, 0, C2DVISOBJ, ATTRIBMISCTXT, STRLEN_MAX)
END_GETSET_IMPL
// end of generated code
