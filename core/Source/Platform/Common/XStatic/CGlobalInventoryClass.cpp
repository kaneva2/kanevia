///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <io.h>
#include <direct.h>

#define D3D_OVERLOADS
#include "resource.h"
#include "ClientStrings.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CGlobalInventoryClass.h"
#include "KEPConstants.h"
#ifdef DEPLOY_CLEANER
#include <Shlwapi.h>
#else
#endif

#include "Core/Util/ObjectRegistry.h"

// Note: the pool allocator pulls in boost/thread which requires a workaround when using
// it in a DLL along with MFC.
// Essentially, the DLL must have this line in one of its source files:
// #include "boost/thread/win32/mfc_thread_init.hpp"
#include "boost/pool/pool_alloc.hpp"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include "common\KEPUtil\Helpers.h"
#include "UnicodeMFCHelpers.h"

namespace KEP {

static LogInstance("GIL");

class MockGlobalInventoryList
		: public GlobalInventoryList,
		  public Deletable<CGlobalInventoryObj>,
		  public Selectable<CGlobalInventoryObj>,
		  public Traversable<CGlobalInventoryObj> {
public:
	virtual GlobalInventoryList& append(CGlobalInventoryObj& invObj) {
		return *this;
	}
	virtual CGlobalInventoryObj* at(INT_PTR nIndex) {
		return 0;
	}
	virtual void CheckForDuplicates() {}
	virtual bool DeleteByGLID(const GLID& glid) {
		return false;
	}
	virtual CGlobalInventoryObj* GetByGLID(const GLID& glid) {
		return 0;
	}
	virtual ListItemMap GetItems() {
		return ListItemMap();
	}
	virtual ListItemMap GetItemsById() {
		return ListItemMap();
	}
	virtual GLID GetGLIDByLegacyAttachmentType(int type) {
		return 0;
	}
	virtual size_t GetSize() const {
		return 0;
	}
	virtual size_t GetCount() const {
		return 0;
	}
	virtual int indexOfGLID(const GLID& glid) {
		return 0;
	}
	virtual CGlobalInventoryObj* removeItemAt(int index) {
		return 0;
	}
	virtual CGlobalInventoryObj* removeItemWithGLID(const GLID& glid) {
		return 0;
	}
	virtual void SafeDelete() {}
	virtual CGlobalInventoryObj* select(Selector<CGlobalInventoryObj>& selector) {
		return 0;
	}
	virtual Traversable<CGlobalInventoryObj>& traverse(Traverser<CGlobalInventoryObj>& traverser) {
		return *this;
	}
	virtual CGlobalInventoryObj* remove(Deleter<CGlobalInventoryObj>& deleter) {
		return 0;
	}
};

class LegacyGIL : public GlobalInventoryList, public Deletable<CGlobalInventoryObj>, public Selectable<CGlobalInventoryObj>, public Traversable<CGlobalInventoryObj> {
public:
	typedef KGenericObjList<CGlobalInventoryObj> gil_impl;

	LegacyGIL() :
			m_impl() {
	}

	size_t GetSize() const {
		return m_impl.size();
	}

	size_t GetCount() const {
		return m_impl.size();
	}

	Traversable<CGlobalInventoryObj>& traverse(gil_impl::TraverserT& traverser) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		m_impl.traverse(traverser);
		return *this;
	}

	CGlobalInventoryObj* select(Selector<CGlobalInventoryObj>& selector) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		return m_impl.select(selector);
	}

	CGlobalInventoryObj* remove(Deleter<CGlobalInventoryObj>& deleter) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		return m_impl.remove(deleter);
	}

	void CheckForDuplicates() {
		//AfxMessageBox(IDS_MSG_COMPLETE);
	}

	int indexOfGLID(const GLID& glid) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		GlobalInventoryListHelpers::GLIDSelector selector(glid);
		if (m_impl.select(selector))
			return selector.m_index;
		return -1;
	}

	CGlobalInventoryObj* GetByGLID(const GLID& glid) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		return GlobalInventoryListHelpers::GLIDSelector(glid).select(m_impl);
	}

	bool DeleteByGLID(const GLID& glid) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		return (m_impl.remove(GlobalInventoryListHelpers::GLIDSelector(glid)) > 0);
	}

	GlobalInventoryList& append(CGlobalInventoryObj& invObj) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		m_impl.append(invObj);
		return *this;
	}

	CGlobalInventoryObj* at(INT_PTR nIndex) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		return m_impl.at(nIndex);
	}

	ListItemMap GetItems() {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		GlobalInventoryListHelpers::ByIndexCollector collector;
		m_impl.traverse(collector);
		return collector.itemMap;
	}

	void SafeDelete() {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		m_impl.traverse(GlobalInventoryListHelpers::GILDeleter());
		m_impl.clear();
	}

	ListItemMap GetItemsById() {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		GlobalInventoryListHelpers::ByIdCollector collector;
		m_impl.traverse(collector);
		return collector.itemMap;
	}

	GLID GetGLIDByLegacyAttachmentType(int type) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		GlobalInventoryListHelpers::LegacyAttachmentTypeSelector selector(type);
		if (m_impl.select(selector))
			return selector.glid;
		return type;
	}

	CGlobalInventoryObj* removeItemWithGLID(const GLID& glid) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		return m_impl.remove(GlobalInventoryListHelpers::GLIDSelector(glid));
	}

	CGlobalInventoryObj* removeItemAt(int index) {
		std::lock_guard<fast_recursive_mutex> lock(m_sync);
		return m_impl.removeAt(index);
	}

private:
	fast_recursive_mutex m_sync;
	gil_impl m_impl;
};

using namespace boost;
using namespace boost::multi_index;
class wrapper;
class MultiIndexGIL : public GlobalInventoryList, public Deletable<CGlobalInventoryObj>, public Selectable<CGlobalInventoryObj>, public Traversable<CGlobalInventoryObj> {
private:
	// Objects contained in MultiIndex are immutable.  This is required to insure
	// consistency of key data.  MultiIndex provides methods for updating the object
	// and updating keys.  But this collection is used in so many places that detecting
	// all of the update points (locations where CGlobalInventoryObj is updated, requiring
	// explicit calls to MultiIndex update) that enforcing this explicit update would be
	// problematic.  Furthermore, immutability increases use of copy logic (copy ctor, assignement
	// operators, etc.  Insuring safe deep copies will be problematic in this system, what with
	// the presence of all these SafeDelete Clone calls.
	//
	// In our case the keys don't really change and other pieces do, so we can create
	// a placeholder class, that contains all of the constant key data along with a
	// pointer to our mutable CGlobalInventoryObj instance.
	class wrapper {
	public:
		wrapper(CGlobalInventoryObj& obj, int dif = 0) :
				pMutable(&obj), m_glid(obj.m_itemData->m_globalID) {
		}

		wrapper(const wrapper& rhs) :
				m_glid(rhs.m_glid), pMutable(rhs.pMutable) {
		}

		wrapper& operator=(const wrapper& rhs) {
			m_glid = rhs.m_glid;
			pMutable = rhs.pMutable;
			return *this;
		}

		virtual ~wrapper() {
			// Note that we don't delete the mutable which is on the heap.
			// Memory management for this resource is handled external to
			// this class.
		}

		bool operator==(const wrapper& rhs) {
			return (m_glid == rhs.m_glid) && (pMutable == pMutable);
		}

		GLID glid() const {
			return m_glid;
		}

		CGlobalInventoryObj* getMutable() const {
			return pMutable;
		}

		std::string toString() {
			std::stringstream ss;
			ss << "glid=" << m_glid << ";";
			return ss.str();
		}

	private:
		GLID m_glid;
		CGlobalInventoryObj* pMutable;
	};

	// The following typedefs symply for the purpose of tagging the indexes
	// in the MultiIndex.  These will allow us to address the individual
	// indexes by a logical name rather than an index.
	struct tag_glid_index {}; // Far and away the most common method to access elements in this collection
	struct tag_name_index {};
	struct tag_usetype_index {};
	struct tag_vectored_index {}; // Support random access to the collection via index support (e.g., [], at())
	struct tag_sequenced_index {}; // For emulating prior, non-optimized behavior

	// Now create typedef for our glid index.
	// define a tagged, hashed, unique index on glid
	//
	typedef hashed_unique<tag<tag_glid_index>, BOOST_MULTI_INDEX_CONST_MEM_FUN(wrapper, int, glid)> mii_glid_index;

	typedef sequenced<tag<tag_sequenced_index>> mii_sequenced_index;
	typedef random_access<tag<tag_vectored_index>> mii_vectored_index;
	typedef multi_index_container<wrapper, indexed_by<mii_glid_index // primary index
											   ,
											   mii_vectored_index, mii_sequenced_index>,
		fast_pool_allocator<wrapper>>
		implementation_type;

	typedef implementation_type::index<tag_glid_index>::type glid_index_type;
	typedef implementation_type::index<tag_vectored_index>::type vectored_index_type;
	typedef implementation_type::index<tag_sequenced_index>::type sequenced_index_type;

public:
	MultiIndexGIL() :
			m_impl() {
	}

	virtual ~MultiIndexGIL() {
	}

	size_t GetSize() const {
		return m_impl.size();
	}

	size_t GetCount() const {
		return m_impl.size();
	}

	Traversable<CGlobalInventoryObj>& traverse(Traverser<CGlobalInventoryObj>& traverser);
	Traversable<CGlobalInventoryObj>& traverse(Traverser<CGlobalInventoryObj>&& traverser) {
		return traverse(traverser); // Allow calling traverse with a temporary.
	}
	CGlobalInventoryObj* select(Selector<CGlobalInventoryObj>& selector);
	CGlobalInventoryObj* select(Selector<CGlobalInventoryObj>&& selector) {
		return select(selector); // Allow calling select with a temporary.
	}
	CGlobalInventoryObj* remove(Deleter<CGlobalInventoryObj>& deleter);
	CGlobalInventoryObj* remove(Deleter<CGlobalInventoryObj>&& deleter) {
		return remove(deleter); // Allow calling remove with a temporary.
	}

	void CheckForDuplicates() {} // This is unnecessary, because the primary index of glid will preclude duplicates

	/**
	 * No matter how you slice it.  This is a potentially expensive operation
	 * MultiIndex allows us to quickly find hashed values and also allows
	 * random access (e.g. operator[], at()), however, there is no trully
	 * safe way of optimizing this.
	 *
	 * One possible solution would be to find the glid using the glid index
	 * then somehow translate that to the index in the random_access index.
	 * But as far I can tell, there is no method to do this (i.e.
	 * ra_index.index_of(glid_index.find())  But there is not mechanism for
	 * making this jump.
	 *
	 * [MS] There is a way to do this if study the documentation of MultiIndex
	 * long enough. Use m_impl.iterator_to(*glid_index.find()) or even
	 * better m_impl.project<tag_vectored_index>(glid_index.find()).
	 *
	 * Another option that comes to mind is store index into the wrapper.
	 * e.g.
	 *       wrapper w = new wrapper( wrapper, gil.size() );	// get size prior to insert
	 *       gil.insert(w)										// and now the wrapper knows index
	 *
	 * This however carries the risk that, when elements are removed from the GIL,
	 * the index value stored in the wrapper is potentially incorrect, if not invalid.
	 *
	 * So, in this implementation, we resign ourselves to a sequential scan.
	 */
	int indexOfGLID(const GLID& glid);
	CGlobalInventoryObj* GetByGLID(const GLID& glid);
	bool DeleteByGLID(const GLID& glid);

	GlobalInventoryList& append(CGlobalInventoryObj& invObj);
	CGlobalInventoryObj* at(INT_PTR nIndex);
	CGlobalInventoryObj* removeItemAt(int index);
	ListItemMap GetItems();
	void SafeDelete();
	ListItemMap GetItemsById();
	GLID GetGLIDByLegacyAttachmentType(int type);

	CGlobalInventoryObj* removeItemWithGLID(const GLID& glid);

private:
	glid_index_type& glidIndex() {
		return m_impl.get<tag_glid_index>();
	}

	vectored_index_type& vectoredIndex() {
		return m_impl.get<tag_vectored_index>();
	}

	sequenced_index_type& sequencedIndex() {
		return m_impl.get<tag_sequenced_index>();
	}

	fast_recursive_mutex m_sync;
	implementation_type m_impl;
};

GlobalInventoryList& MultiIndexGIL::append(CGlobalInventoryObj& invObj) {
#ifdef DEPLOY_CLEANER
	LogTrace("[" << this << "]");
#else
#endif
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	wrapper* pw = new wrapper(invObj, 1); // We are putting this wrapper on the stack

	m_impl.insert(*pw); // This safe because multiindex will invoke

	// create a copy of this wrapper when we insert wrapper.
	delete pw;

	return *this;
}

CGlobalInventoryObj* MultiIndexGIL::at(INT_PTR nIndex) {
	if (nIndex < 0)
		return 0;
	if ((UINT_PTR)nIndex >= m_impl.size())
		return 0;
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return vectoredIndex()[nIndex].getMutable();
}

bool MultiIndexGIL::DeleteByGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	m_impl.erase(m_impl.find(glid));
	return true;
}

CGlobalInventoryObj* MultiIndexGIL::GetByGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	glid_index_type::iterator i = m_impl.find(glid);
	if (i == m_impl.end())
		return 0;
	return i->getMutable();
}

GLID MultiIndexGIL::GetGLIDByLegacyAttachmentType(int type) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	GlobalInventoryListHelpers::LegacyAttachmentTypeSelector selector(type);
	if (select(selector))
		return selector.glid;
	return type;
}

ListItemMap MultiIndexGIL::GetItems() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	GlobalInventoryListHelpers::ByIndexCollector collector;
	traverse(collector);
	return collector.itemMap;
}

ListItemMap MultiIndexGIL::GetItemsById() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	GlobalInventoryListHelpers::ByIdCollector collector;
	traverse(collector);
	return collector.itemMap;
}

int MultiIndexGIL::indexOfGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	GlobalInventoryListHelpers::GLIDSelector selector(glid);
	if (select(selector))
		return selector.m_index;
	return -1;
}

CGlobalInventoryObj* MultiIndexGIL::remove(Deleter<CGlobalInventoryObj>& selector) {
	glid_index_type& index = glidIndex();
	for (auto iter = index.begin(); iter != index.end(); iter++) {
		if (selector.evaluate(*(iter->getMutable()))) {
			CGlobalInventoryObj* ret = iter->getMutable();
			index.erase(iter);
			return ret;
		}
	}
	return 0;
}

CGlobalInventoryObj* MultiIndexGIL::removeItemWithGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	glid_index_type& index = glidIndex();
	glid_index_type::iterator iter = index.find(glid);
	if (iter == index.end())
		return 0;
	CGlobalInventoryObj* ret = iter->getMutable();
	m_impl.erase(index.find(glid));
	return ret;
}

CGlobalInventoryObj* MultiIndexGIL::removeItemAt(int index) {
	if (index < 0)
		return 0;
	if ((unsigned int)index >= m_impl.size())
		return 0;
	std::lock_guard<fast_recursive_mutex> lock(m_sync); // Lock the resource.
	vectored_index_type& v = vectoredIndex(); // get short hand for the random access (vectored) index
	const wrapper& w = v[index]; // Get a reference to the element at said index.
	v.erase(v.iterator_to(w)); // Erase element from the index (and the rest of the multi index)
	return w.getMutable(); // Return a pointer to the contained mutable instance
}

// Call the effective destructor SafeDelete() on every gio in the collection
void MultiIndexGIL::SafeDelete() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	traverse(GlobalInventoryListHelpers::GILDeleter());
	m_impl.clear();
}

CGlobalInventoryObj* MultiIndexGIL::select(Selector<CGlobalInventoryObj>& selector) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// Note that depending on the what the traverser is attempting to do,
	// Iterating over some indexes might be better than others.
	//
	// It would be cool allow the traverser to select index.  This enable
	// optimization of search-traverse operations.
	// e.g.
	//       select index
	//       perform find() to jump to first instance to be traversed
	//       traverse until traverser stops
	//
	// For now, let's just use the primary index
	sequenced_index_type& index = sequencedIndex();
	for (sequenced_index_type::iterator iter = index.begin(); iter != index.end(); iter++) {
		// verify handling evaluate return value
		if (selector.evaluate(*(iter->getMutable())))
			return iter->getMutable();
	}
	return 0;
}

Traversable<CGlobalInventoryObj>& MultiIndexGIL::traverse(Traverser<CGlobalInventoryObj>& traverser) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// Note that depending on the what the traverser is attempting to do,
	// Iterating over some indexes might be better than others.
	//
	// It would be cool allow the traverser to select index.  This enable
	// optimization of search-traverse operations.
	// e.g.
	//       select index
	//       perform find() to jump to first instance to be traversed
	//       traverse until traverser stops
	//
	// For now, let's just use the primary index
	sequenced_index_type& index = sequencedIndex();
	int cc = 0;
	for (sequenced_index_type::iterator iter = index.begin(); iter != index.end(); iter++) {
		cc++;
		// verify handling evaluate return value
		if (traverser.evaluate(*(iter->getMutable())))
			break;
	}

	return *this;
}

CGlobalInventoryObjList* CGlobalInventoryObjList::m_instance = 0;
Logger CGlobalInventoryObjList::m_logger(Logger::getInstance(L"GIL"));
fast_recursive_mutex CGlobalInventoryObjList::m_initSync;

CGlobalInventoryObj::CGlobalInventoryObj() {
	m_visual = NULL;
	m_itemData = NULL;
}

void CGlobalInventoryObj::SafeDelete() {
	if (m_visual) {
		m_visual->SafeDelete();
		delete m_visual;
		m_visual = 0;
	}

	if (m_itemData) {
		delete m_itemData; // CItemObj::SafeDelete now merged with destructor
		m_itemData = 0;
	}
}

void CGlobalInventoryObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << reserved
		   << m_visual
		   << m_itemData;
	} else {
		ar >> reserved >> m_visual >> m_itemData;
	}
}

// In the past the GIL was a derivative of CObList and made use of MFC serialization
// framework.
//
// In the interest of backward compatibility, the logic below implements serialization
// logic for the GIL.
//
GILSerializer::GILSerializer() :
		m_store(new CGlobalInventoryObjList()), m_manageCleanup(1) {}

GILSerializer::GILSerializer(CGlobalInventoryObjList& store) :
		m_store(&store), m_manageCleanup(0) {}

GILSerializer::~GILSerializer() {
	if (m_store && m_manageCleanup) {
		m_store->SafeDelete();
		delete m_store;
		m_store = 0;
	}
}

GILSerializer& GILSerializer::setStore(CGlobalInventoryObjList& store) {
	m_store = &store;
	return *this;
}

CObject* PASCAL GILSerializer::CreateObject() {
	return new GILSerializer;
}

extern AFX_CLASSINIT _init_GILSerializer;

CRuntimeClass* PASCAL GILSerializer::_GetBaseClass() {
	return CKEPObList::GetThisClass();
}

// Here's the tricky bit.  We implemented all of this just so
// we could hijack the name in the RuntimeClass object of the
// serializer.  Now, when CArchive encounters a CGlobalInventoryObjList
// in the input stream, it will call this method...
//
AFX_COMDAT CRuntimeClass GILSerializer::classGILSerializer = {
	"CGlobalInventoryObjList" // "GILSerializer"
	,
	sizeof(class GILSerializer), VERSIONABLE_SCHEMA | 1, &GILSerializer::CreateObject, &GILSerializer::_GetBaseClass, NULL, &_init_GILSerializer
};

CRuntimeClass* PASCAL GILSerializer::GetThisClass() {
	return (CRuntimeClass*)(&GILSerializer::classGILSerializer);
}

CRuntimeClass* GILSerializer::GetRuntimeClass() const {
	return ((CRuntimeClass*)(&GILSerializer::classGILSerializer));
}

AFX_CLASSINIT _init_GILSerializer(GILSerializer::GetThisClass());

CArchive& AFXAPI operator>>(CArchive& ar, GILSerializer*& pOb) {
	pOb = (GILSerializer*)ar.ReadObject(GILSerializer::GetThisClass());
	return ar;
}

class ItemWriter : public CGlobalInventoryObjList::TraverserT {
public:
	ItemWriter(GILSerializer& serializer) :
			target(serializer) {
	}

	virtual bool evaluate(CGlobalInventoryObj& obj) {
		target.AddTail(&obj);
		return false;
	}

	GILSerializer& target;
};

void GILSerializer::Serialize(CArchive& ar) {
	if (ar.IsStoring()) {
		// First copy the elements into the serialization source.  Note the
		// semantics still assume MFC POSITION/Iteration access which will need
		// to be replaced when we serialize access to the GIL.
		m_store->traverse(ItemWriter(*this));
		CKEPObList::Serialize(ar);
	} else {
		CKEPObList::Serialize(ar);

		// Now copy the elements into the store.
		//int version = ar.GetObjectSchema();
		for (POSITION bPos = GetHeadPosition(); bPos != NULL;) {
			CGlobalInventoryObj* m_invObj = (CGlobalInventoryObj*)GetNext(bPos); //)->m_itemData;
#ifdef DEPLOY_CLEANER
			m_store->append(*m_invObj);
#else
			m_store->AddTail(m_invObj);
#endif
			if (m_invObj->m_itemData->m_globalID >= m_store->m_nextGLID)
				m_store->m_nextGLID = m_invObj->m_itemData->m_globalID + 1;
		}
	}
}

CGlobalInventoryObjList::~CGlobalInventoryObjList() {
	if (m_instance == this) {
		m_instance = 0;
		ObjectRegistry::instance().removeObject("GIL");
		SafeDelete();
	}
#ifdef DEPLOY_CLEANER
	if (_cleaner) {
		delete _cleaner;
		_cleaner = 0;
	}
#else
#endif
	delete m_impl;
}

CGlobalInventoryObjList::CGlobalInventoryObjList() :
		m_impl(new MockGlobalInventoryList()), m_lastSavedPath(""), m_dirty(FALSE), m_sync(), m_reportingThrottle(1000)
#ifdef DEPLOY_CLEANER
		,
		_cleaner(0)
#else
#endif
		,
		_gameFile(*this) {
#ifdef DEPLOY_CLEANER
	LogTrace("[" << this << "]");
#else
#endif
	wchar_t acBufW[128];
	GetCurrentDirectoryW(128, acBufW);

	std::stringstream ss;
	ss << Utf16ToUtf8(acBufW) << "\\gil.ini";
#ifdef DEPLOY_CLEANER
	LogInfo("[" << this << "] Attempting to load GIL config from [" << ss.str().c_str() << "] ");
#else
#endif

	// To be deprecated.  As soon as we have full faith in the multiindex....disable the ability to configure the
	// implementation.  But leave the abstraction in place.
	//
	char acBuf[128];
	GetPrivateProfileStringA("GIL", "implementation", "1", acBuf, 128, ss.str().c_str());

	switch (acBuf[0]) {
		case '1':
			delete m_impl;
			m_impl = new MultiIndexGIL();
#ifdef DEPLOY_CLEANER
			LogInfo("[" << this << "] Loaded MultiIndex GIL Implementation");
#else
#endif
			break;
		default:
			delete m_impl;
			m_impl = new LegacyGIL();
#ifdef DEPLOY_CLEANER
			LogInfo("Loaded Legacy GIL Implementation");
#else
#endif
			break;
	};

	m_nextGLID = 1;
	m_lastAllocatedGLID = 0;
};

// N.B.: This implementation is full of concurrency holes, but it should suffice for verifying the fix.
//
CGlobalInventoryObjList* CGlobalInventoryObjList::GetInstance() {
	// If we've already acquired our local reference...then simply return it.
	if (m_instance)
		return m_instance;

	// else, we've got some work to do because for all we know CGlobalInventoryObjList from one of the other
	// dlls is coming into this call as well....in fact there really isn't a good way to do it, beyond a full
	// blown lease implementation.  One option except to force the issue by calling GetInstance early in the
	// run when concurrency isn't an issue...which while we're in proof of concept...that's the tack
	// that will be taken.  Once the fix is validated, then evaluate a more robust initialization method that
	// leverages the fact that we know the ObjectRegistry is a bonafide singleton.
	//
	// First acquire to our initialization sync object
	//
	std::lock_guard<fast_recursive_mutex> lock(m_initSync);

	// now that we've acquired the lock, reevaluate to make sure that nobody
	// constructed our singleton instance during lock acquisition
	//
	if (m_instance)
		return m_instance;

	// It's possible, as this class is statically linked into multiple dll's, that our pointer
	// to the single instance is null, but that an instance already exists.  Let's look in
	// the ObjectRegistry to find out.
	//
	ObjectRegistry& registry = ObjectRegistry::instance();
#ifdef DEPLOY_CLEANER
	LogDebug("Acquired ObjectRegistry[" << &registry << "][" << registry.toString() << "]");
#else
#endif

	m_instance = (CGlobalInventoryObjList*)registry.query("GIL");
	if (m_instance)
		// Yep, our static instance indicated that an instance had not been instantiated yet...
		// but another dll had already created one.
		return m_instance;

		// Okay, this is truly the first attempt to get our singleton instance.
		// Create the instance
#ifdef DEPLOY_CLEANER
	LogDebug("failed[" << registry.toString() << "] Initializing singleton!");
#else
#endif
	m_instance = new CGlobalInventoryObjList();
#ifdef DEPLOY_CLEANER
	LogDebug("Construct GIL[" << &m_instance << "]");

	// Check run time switch
	// if cleaner is disabled use a mock cleaner, else use concrete

	// should be at <pathApp>\devflag_usegilcleaner
	std::string pathDevFlag = PathAdd(PathApp(), "devflag_usegilcleaner");

	LogInfo("Looking for [" << pathDevFlag << "]");
	bool useCleaner = (_access(pathDevFlag.c_str(), 0) == 0) ? true : false;
	LogInfo("GILCleaner [" << (useCleaner ? "enabled" : "disabled") << "]");
	GILCleaner* cleaner = useCleaner ? ((GILCleaner*)new ConcreteGILCleaner()) : ((GILCleaner*)new MockGILCleaner());
	m_instance->setCleaner(*cleaner);
#else
#endif
	// Register the instance in the ObjectRegistry
	//
	registry.registerObject("GIL", (void*)m_instance);
#ifdef DEPLOY_CLEANER
	LogDebug("Registered GIL instance[" << m_instance << "]");
#else
#endif
	// And hand the instance off to the user.
	//
	return m_instance;
}

CGlobalInventoryObjList* GILSerializer::retrieveList() {
	CGlobalInventoryObjList* pRet = m_store;
	m_store = 0;
	return pRet;
}

#ifdef DEPLOY_CLEANER
std::string CGlobalInventoryObj::toXML() const {
	std::stringstream ret;
	ret << "<CGlobalInventoryObj>"
		<< m_itemData->toXML()
		<< "</CGlobalInventoryObj>";
	return ret.str();
}
#else
#endif

std::string CGlobalInventoryObj::toString() const {
	std::stringstream ret;
	ret << "[" << m_itemData << "]" << m_itemData->toString();
	return ret.str();
}

GLID CGlobalInventoryObjList::AllocateGLID() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	m_lastAllocatedGLID = m_nextGLID++;
	return m_lastAllocatedGLID;
}

//! \brief Reserve external GLID.
//!
//! Some modules (e.g. Skeletal Animation) might use GLIDs without matching inventory items.
//! Such GLIDs are used for identification purposes and also reserved for future item creation
//! for the associated assets. This ReserveGLID function prevents external GLIDs from being
//! used by regular items.
//!
//! NOTE: CGlobalInventoryObjList class does not memorize those special GLIDs persistently,
//! other modules are responsible for resubmitting them every time Editor starts.
void CGlobalInventoryObjList::ReserveExternalGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	if (glid >= m_nextGLID) {
		m_nextGLID = glid + 1;
		m_lastAllocatedGLID = 0;
	}
}

namespace gil_internals {

class ToStringTraverser : public CGlobalInventoryObjList::TraverserT {
public:
	virtual bool evaluate(CGlobalInventoryObj& obj) {
		ret << obj.toString() << std::endl;
		return false;
	}

	std::stringstream ret;
};

#ifdef DEPLOY_CLEANER
class ToXMLTraverser : public CGlobalInventoryObjList::TraverserT {
public:
	virtual bool evaluate(CGlobalInventoryObj& obj) {
		ret << obj.toXML() << std::endl;
		return false;
	}

	std::stringstream ret;
};
#else
#endif

class GILMigrator : public CGlobalInventoryObjList::TraverserT {
public:
	CGlobalInventoryObjList& _target;
	GILMigrator(CGlobalInventoryObjList& target) :
			_target(target) {}

	virtual bool evaluate(CGlobalInventoryObj& obj) {
		_target.append(obj);
		// Cause the newly added data into the cleaner with an incremented reference count
		// this should keep items that have been serialized into the gil from ever disappearing.
		//
#ifdef DEPLOY_CLEANER
		_target.cleaner()->itemAdded(obj.m_itemData->m_globalID);
#else
#endif
		return false;
	}
};

class SecondaryDupChecker : public Traverser<CGlobalInventoryObj> {
public:
	SecondaryDupChecker(CGlobalInventoryObj& vprimaryObject, int vprimaryIndex) :
			primaryObject(vprimaryObject), primaryIndex(vprimaryIndex), secondaryIndex(0) {
	}

	virtual bool evaluate(CGlobalInventoryObj& obj) {
		// It was bound to happen...we found "self"
		//
		if (primaryIndex == secondaryIndex)
			if (primaryObject.m_itemData->m_globalID == obj.m_itemData->m_globalID) {
				AfxMessageBox(IDS_ERROR_CONFLICTFOUND);
				AfxMessageBox(Utf8ToUtf16(operator+(operator+(primaryObject.m_itemData->m_itemName, " "), obj.m_itemData->m_itemName)).c_str());
			}
		secondaryIndex++;
		return false;
	}

	CGlobalInventoryObj& primaryObject;
	int primaryIndex;
	int secondaryIndex;
};

class PrimaryDupChecker : public Traverser<CGlobalInventoryObj> {
public:
	PrimaryDupChecker(Traversable<CGlobalInventoryObj>& SearchTarget) :
			searchTarget(SearchTarget), primaryIndex(0) {}

	virtual bool evaluate(CGlobalInventoryObj& obj) {
		SecondaryDupChecker secDubChkr(obj, primaryIndex);
		searchTarget.traverse(secDubChkr);
		primaryIndex++;
		return false;
	}

	Traversable<CGlobalInventoryObj>& searchTarget;
	int primaryIndex;
};

} // namespace gil_internals

CGlobalInventoryObjList& CGlobalInventoryObjList::copyFrom(CGlobalInventoryObjList& source) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	source.traverse(gil_internals::GILMigrator(*m_instance));
#ifdef DEPLOY_CLEANER
#else
	m_lastAllocatedGLID = source.m_lastAllocatedGLID;
	m_nextGLID = source.m_nextGLID;
#endif
	return *this;
}

void CGlobalInventoryObjList::CheckForDuplicates() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	impl().CheckForDuplicates();
}

int CGlobalInventoryObjList::indexOfGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().indexOfGLID(glid);
}

CGlobalInventoryObj* CGlobalInventoryObjList::GetByGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().GetByGLID(glid);
}

void CGlobalInventoryObjList::SafeDelete() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	impl().SafeDelete();
}

bool CGlobalInventoryObjList::DeleteByGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().DeleteByGLID(glid);
}

CGlobalInventoryObjList& CGlobalInventoryObjList::AddTail(CGlobalInventoryObj* invObj) {
	return append(*invObj);
}

CGlobalInventoryObjList& CGlobalInventoryObjList::append(CGlobalInventoryObj& invObj) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	if (m_reportingThrottle.isCleared()) {
		LOG4CPLUS_INFO(this->m_logger, this->toString());
	}
	impl().append(invObj);
	return *this;
}

CGlobalInventoryObj* CGlobalInventoryObjList::at(INT_PTR nIndex) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().at(nIndex);
}

ListItemMap CGlobalInventoryObjList::GetItems() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().GetItems();
}

ListItemMap CGlobalInventoryObjList::GetItemsById() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().GetItemsById();
}

void CGlobalInventoryObjList::serializeToFileWithName(const char* fileName) {
	CFile file;
	OpenCFile(file, fileName, CFile::modeWrite | CFile::modeCreate);
	CArchive the_in_Archive(&file, CArchive::store);
	GILSerializer slizer(*this); // = &(serializer());
	the_in_Archive << &(slizer);
	the_in_Archive.Close();
	file.Close();
}

CGlobalInventoryObjList& CGlobalInventoryObjList::traverse(TraverserT& traverser) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	impl().traverse(traverser);
	return *this;
}

CGlobalInventoryObj* CGlobalInventoryObjList::select(SelectorT& selector) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().select(selector);
}

GLID CGlobalInventoryObjList::GetGLIDByLegacyAttachmentType(int type) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().GetGLIDByLegacyAttachmentType(type);
}

std::string CGlobalInventoryObjList::toString() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	std::stringstream ret;
#ifdef DEPLOY_CLEANER
	ret << "<CGlobalInventoryObjList"
		<< " address=\"" << this << "\""
		<< " size=\"" << GetSize() << "\"/>";
#else
	ret << "CGlobalInventoryObjList[size=" << GetSize() << ";]";
#endif
	return ret.str();
}

#ifdef DEPLOY_CLEANER
std::string CGlobalInventoryObjList::toXML() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	std::stringstream ret;
	ret << "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		<< "<CGlobalInventoryObjList"
		<< " address=\"" << this << "\""
		<< " size=\"" << GetSize() << "\">";
	gil_internals::ToXMLTraverser traverser;
	m_impl->traverse(traverser);
	ret << traverser.ret.str()
		<< "</CGlobalInventoryObjList>";
	return ret.str();
}
#else
#endif

CGlobalInventoryObj* CGlobalInventoryObjList::removeItemWithGLID(const GLID& glid) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().removeItemWithGLID(glid);
}

CGlobalInventoryObj* CGlobalInventoryObjList::removeItemAt(int index) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	return impl().removeItemAt(index);
}

IMPLEMENT_SERIAL(CGlobalInventoryObj, CObject, 0)

BEGIN_GETSET_IMPL(CGlobalInventoryObj, IDS_GLOBALINVENTORYOBJ_NAME)
GETSET_OBJ_PTR(m_visual, GS_FLAGS_DEFAULT, 0, 0, GLOBALINVENTORYOBJ, VISUAL, CEXMeshObj)
GETSET_OBJ_PTR(m_itemData, GS_FLAGS_DEFAULT, 0, 0, GLOBALINVENTORYOBJ, ITEMDATA, CItemObj)
END_GETSET_IMPL

} // namespace KEP