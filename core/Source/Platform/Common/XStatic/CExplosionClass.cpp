///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CExplosionClass.h"
#include "IAssetsDatabase.h"
#include "AssetTree.h"
#include "MaterialPersistence.h"
#include "SkeletonConfiguration.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CFrameObj.h"
#include "CRTLightClass.h"
#include "CEXMeshClass.h"
#include "CSoundManagerClass.h"

namespace KEP {

CExplosionObj::CExplosionObj() :
		m_pRuntimeSkeleton(nullptr) {
	m_meshObject = NULL;
	m_frame = NULL;
	m_keyFrames = NULL;
	m_duration = 1000;
	m_baseScalerX = 1.0f;
	m_baseScalerY = 1.0f;
	m_baseScalerZ = 1.0f;
	m_damageRadius = 0.0f;
	m_radiusDamage = 0;
	m_beginTimeStamp = 0;
	m_orientation = 0;
	m_startFadeAt = 0;
	m_currentKeyPosition = NULL;
	m_currentKeyDuration = 0;
	m_currentKeyTimeStamp = 0;
	m_lastScalerX = 1.0f;
	m_lastScalerY = 1.0f;
	m_lastScalerZ = 1.0f;
	m_rotationX = 0.0f;
	m_rotationY = 0.0f;
	m_rotationZ = 0.0f;
	m_lastRotationX = 0.0f;
	m_lastRotationY = 0.0f;
	m_lastRotationZ = 0.0f;
	m_explosionName = "NONE";
	m_enableSound = FALSE;
	m_distanceFactor = .03f;
	m_needInitPlay = TRUE;
	m_clipRadius = 50.0f;
	m_snID = 0;
	m_soundHandle = NULL;
	m_ownerTrace = 0;
	m_splashDamageFinsh = FALSE;
	m_pushPower = 5.0f;
	m_light = NULL;
	m_dbIndexRef = 0;
	m_damageChannel = 0;
	m_teamRef = -1;
	m_damageSpecific = -1;
	m_traceFollow = -1;
	m_criteriaForSkillUse = 0;
}

void CExplosionObj::SafeDelete() {
	if (m_meshObject) {
		m_meshObject->SafeDelete();
		delete m_meshObject;
		m_meshObject = 0;
	}
	if (m_frame) {
		m_frame->SafeDelete();
		delete m_frame;
		m_frame = 0;
	}
	if (m_keyFrames) {
		m_keyFrames->SafeDelete();
		delete m_keyFrames;
		m_keyFrames = 0;
	}
	if (m_soundHandle) {
		m_soundHandle->SetToDelete();
		m_soundHandle = NULL;
	}
	if (m_light) {
		delete m_light;
		m_light = 0;
	}

	m_pRuntimeSkeleton.reset();
}

BOOL CExplosionObj::Clone(CExplosionObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CExplosionObj* copyLocal = new CExplosionObj();
	copyLocal->m_duration = m_duration;
	copyLocal->m_baseScalerX = m_baseScalerX;
	copyLocal->m_baseScalerY = m_baseScalerY;
	copyLocal->m_baseScalerZ = m_baseScalerZ;
	copyLocal->m_damageRadius = m_damageRadius;
	copyLocal->m_radiusDamage = m_radiusDamage;
	copyLocal->m_orientation = m_orientation;
	copyLocal->m_startFadeAt = m_startFadeAt;
	copyLocal->m_enableSound = m_enableSound;
	copyLocal->m_distanceFactor = m_distanceFactor;
	copyLocal->m_snID = m_snID;
	copyLocal->m_pushPower = m_pushPower;
	copyLocal->m_damageChannel = m_damageChannel;
	copyLocal->m_damageSpecific = m_damageSpecific;
	copyLocal->m_traceFollow = m_traceFollow;
	copyLocal->m_criteriaForSkillUse = m_criteriaForSkillUse;

	if (m_light)
		m_light->Clone(&copyLocal->m_light);

	if (m_meshObject)
		m_meshObject->Clone(&copyLocal->m_meshObject, g_pd3dDevice);

	if (m_keyFrames)
		m_keyFrames->Clone(&copyLocal->m_keyFrames);

	if (m_pRuntimeSkeleton)
		m_pRuntimeSkeleton->Clone(&copyLocal->m_pRuntimeSkeleton, g_pd3dDevice, NULL);

	*clone = copyLocal;
	return TRUE;
}

void CExplosionObj::AddKeyFrame(Vector3f scale, Vector3f rotation, int duration) {
	if (!m_keyFrames)
		m_keyFrames = new CExpKeyFrameObjList();

	CExpKeyFrameObj* newKeyFrame = new CExpKeyFrameObj();
	newKeyFrame->m_scalerX = scale.x;
	newKeyFrame->m_scalerY = scale.y;
	newKeyFrame->m_scalerZ = scale.z;
	newKeyFrame->m_rotationX = rotation.x;
	newKeyFrame->m_rotationY = rotation.y;
	newKeyFrame->m_rotationZ = rotation.z;
	newKeyFrame->m_duration = duration;

	m_keyFrames->AddTail(newKeyFrame);
}

void CExplosionObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;

	CSkeletonObject* pTmpSkeleton = nullptr;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		if (m_pRuntimeSkeleton != nullptr) {
			// Wrap runtime skeleton into a temporary skeleton object
			pTmpSkeleton = new CSkeletonObject(m_pRuntimeSkeleton, nullptr);
		}

		ar << m_meshObject
		   << m_duration
		   << m_baseScalerX
		   << m_baseScalerY
		   << m_baseScalerZ
		   << m_damageRadius
		   << m_radiusDamage
		   << m_startFadeAt
		   << m_explosionName
		   << m_keyFrames
		   << m_orientation
		   << m_enableSound
		   << m_distanceFactor
		   << m_clipRadius
		   << m_snID
		   << m_pushPower
		   << m_light
		   << pTmpSkeleton
		   << m_damageChannel
		   << m_criteriaForSkillUse
		   << reserved
		   << m_damageSpecific; //migrated

		if (pTmpSkeleton) {
			pTmpSkeleton->detach();
			delete pTmpSkeleton;
			pTmpSkeleton = nullptr;
		}

	} else {
		ar >> m_meshObject >> m_duration >> m_baseScalerX >> m_baseScalerY >> m_baseScalerZ >> m_damageRadius >> m_radiusDamage >> m_startFadeAt >> m_explosionName >> m_keyFrames >> m_orientation >> m_enableSound >> m_distanceFactor >> m_clipRadius >> m_snID >> m_pushPower >> m_light >> pTmpSkeleton >> m_damageChannel >> m_criteriaForSkillUse >> reserved >> m_damageSpecific; //migrated

		if (pTmpSkeleton) {
			// Extract runtime skeleton and dispose the temporary object
			m_pRuntimeSkeleton = pTmpSkeleton->getRuntimeSkeletonShared();
			auto pSkeletonCfg = pTmpSkeleton->getSkeletonConfiguration();
			if (pSkeletonCfg) {
				delete pSkeletonCfg;
			}
			pTmpSkeleton->detach();
			delete pTmpSkeleton;
			pTmpSkeleton = nullptr;
		}

		m_needInitPlay = TRUE;
		m_soundHandle = NULL;
		m_ownerTrace = 0;
		m_splashDamageFinsh = FALSE;
		m_dbIndexRef = 0;
		m_teamRef = -1;
		m_traceFollow = -1;
	}
}

void CExplosionObj::SetFade(TimeMs timeMs) {
	if (!m_meshObject)
		return;

	if (m_meshObject->m_materialObject->GetTransition() == 4)
		return;
	m_meshObject->m_materialObject->m_cycleTime = m_duration - (timeMs - m_beginTimeStamp);
	m_meshObject->m_materialObject->SetTransition(4); //fade out
	m_meshObject->m_materialObject->m_timeStamp = fTime::TimeMs();
}

BOOL CExplosionObj::LoadBasicMesh(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	if (m_meshObject) {
		m_meshObject->SafeDelete();
		delete m_meshObject;
		m_meshObject = 0;
	}

	if (0 == (m_meshObject = IAssetsDatabase::Instance()->ImportStaticMesh(fileName)))
		return FALSE;

	m_meshObject->InitAfterLoad(g_pd3dDevice);
	m_meshObject->m_materialObject = new CMaterialObject();
	m_meshObject->m_materialObject->m_blendMode = 2;
	m_meshObject->m_materialObject->m_baseMaterial.Emissive.r = 1.0f;
	m_meshObject->m_materialObject->m_baseMaterial.Emissive.g = 1.0f;
	m_meshObject->m_materialObject->m_baseMaterial.Emissive.b = 1.0f;
	m_meshObject->m_materialObject->Reset();

	return TRUE;
}

BOOL CExplosionObjList::AddExplosionToDB(CStringA fileName,
	int duration,
	float damageRadius,
	int orientation,
	int radiusDamage,
	LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CExplosionObj* newExp = new CExplosionObj();
	newExp->m_damageRadius = damageRadius;
	newExp->m_orientation = orientation;
	newExp->m_radiusDamage = radiusDamage;
	newExp->m_duration = duration;

	if (!newExp->LoadBasicMesh(fileName, g_pd3dDevice)) {
		delete newExp;
		newExp = 0;
		return FALSE;
	}

	AddTail(newExp);
	return TRUE;
}

BOOL CExplosionObj::IndexKeyframe() {
	if (!m_keyFrames)
		return FALSE;
	if (!m_currentKeyPosition)
		return FALSE;
	if (m_keyFrames) {
		CExpKeyFrameObj* keyframe = (CExpKeyFrameObj*)m_keyFrames->GetNext(m_currentKeyPosition);

		m_lastScalerX = m_baseScalerX;
		m_lastScalerY = m_baseScalerY;
		m_lastScalerZ = m_baseScalerZ;

		m_baseScalerX = keyframe->m_scalerX;
		m_baseScalerY = keyframe->m_scalerY;
		m_baseScalerZ = keyframe->m_scalerZ;

		m_lastRotationX = m_rotationX;
		m_lastRotationY = m_rotationY;
		m_lastRotationZ = m_rotationZ;

		m_rotationX = keyframe->m_rotationX;
		m_rotationY = keyframe->m_rotationY;
		m_rotationZ = keyframe->m_rotationZ;
		m_currentKeyDuration = keyframe->m_duration;
		m_currentKeyTimeStamp = fTime::TimeMs();
	}
	return TRUE;
}

BOOL CExplosionObjList::CreateExplosion(int dbIndex,
	Vector3f wldPosition,
	CExplosionObjList* runtimeRenderList,
	Vector3f lookatPosition,
	int ownerTrace,
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	int teamRef,
	int traceFollow) {
	POSITION dbPos = FindIndex(dbIndex);
	if (!dbPos)
		return FALSE;
	CExplosionObj* expObj = (CExplosionObj*)GetAt(dbPos);
	CExplosionObj* newExp = NULL;
	expObj->Clone(&newExp, g_pd3dDevice);
	newExp->m_ownerTrace = ownerTrace;
	newExp->m_dbIndexRef = dbIndex;
	newExp->m_teamRef = teamRef;
	newExp->m_traceFollow = traceFollow;

	if (newExp->getRuntimeSkeleton())
		newExp->getRuntimeSkeleton()->setPrimaryAnimationByIndex(0, AnimSettings());

	newExp->m_frame = new CFrameObj(NULL);
	newExp->m_frame->SetPosition(wldPosition);
	if (newExp->m_keyFrames) {
		POSITION keyPosition = newExp->m_keyFrames->FindIndex(0);
		if (keyPosition) {
			CExpKeyFrameObj* keyframe = (CExpKeyFrameObj*)newExp->m_keyFrames->GetNext(keyPosition);
			newExp->m_baseScalerX = keyframe->m_scalerX;
			newExp->m_baseScalerY = keyframe->m_scalerY;
			newExp->m_baseScalerZ = keyframe->m_scalerZ;
			newExp->m_rotationX = keyframe->m_rotationX;
			newExp->m_rotationY = keyframe->m_rotationY;
			newExp->m_rotationZ = keyframe->m_rotationZ;
			newExp->m_currentKeyDuration = keyframe->m_duration;
			newExp->m_currentKeyTimeStamp = fTime::TimeMs();
			newExp->m_currentKeyPosition = keyPosition;
		}
	}
	if (newExp->m_orientation == 2) //Ground Orientate
		newExp->m_frame->FaceTowardPoint(lookatPosition);

	newExp->m_beginTimeStamp = fTime::TimeMs();
	runtimeRenderList->AddTail(newExp);
	return TRUE;
}

void CExplosionObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CExplosionObj* expObj = (CExplosionObj*)GetNext(posLoc);
		expObj->SafeDelete();
		delete expObj;
		expObj = 0;
	}
	RemoveAll();
}

CExpKeyFrameObj::CExpKeyFrameObj() {
	m_scalerX = 0.0f;
	m_scalerY = 0.0f;
	m_scalerZ = 0.0f;
	m_rotationX = 0.0f;
	m_rotationY = 0.0f;
	m_rotationZ = 0.0f;
	m_duration = 300;
}

void CExpKeyFrameObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_scalerX
		   << m_scalerY
		   << m_scalerZ
		   << m_duration
		   << m_rotationX
		   << m_rotationY
		   << m_rotationZ;
	} else {
		ar >> m_scalerX >> m_scalerY >> m_scalerZ >> m_duration >> m_rotationX >> m_rotationY >> m_rotationZ;
	}
}

void CExpKeyFrameObj::Clone(CExpKeyFrameObj** copy) {
	CExpKeyFrameObj* copyLocal = new CExpKeyFrameObj();
	copyLocal->m_scalerX = m_scalerX;
	copyLocal->m_scalerY = m_scalerY;
	copyLocal->m_scalerZ = m_scalerZ;
	copyLocal->m_rotationX = m_rotationX;
	copyLocal->m_rotationY = m_rotationY;
	copyLocal->m_rotationZ = m_rotationZ;
	copyLocal->m_duration = m_duration;

	*copy = copyLocal;
}

void CExpKeyFrameObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CExpKeyFrameObj* kObj = (CExpKeyFrameObj*)GetNext(posLoc);
		delete kObj;
		kObj = 0;
	}
	RemoveAll();
}

void CExpKeyFrameObjList::Clone(CExpKeyFrameObjList** copy) {
	CExpKeyFrameObjList* copyLocal = new CExpKeyFrameObjList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CExpKeyFrameObj* kObj = (CExpKeyFrameObj*)GetNext(posLoc);
		CExpKeyFrameObj* clonedObj = NULL;
		kObj->Clone(&clonedObj);
		copyLocal->AddTail(clonedObj);
	}
	*copy = copyLocal;
}

void CExplosionObjList::Serialize(CArchive& ar) {
	MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CExplosionObj* ePtr = (CExplosionObj*)GetNext(posLoc);
			if (ePtr->m_meshObject && ePtr->m_meshObject->m_materialObject) {
				matCompressor->addMaterial(ePtr->m_meshObject->m_materialObject);
			}
			if (ePtr->m_light && ePtr->m_light->m_material) {
				matCompressor->addMaterial(ePtr->m_light->m_material.get());
			}
		}
		matCompressor->compress();
		matCompressor->writeToArchive(ar); // write compressed materials

		CKEPObList::Serialize(ar);
	} else {
		int version = ar.GetObjectSchema();
		if (version >= 1) {
			matCompressor->readFromArchive(ar);
		}
		CKEPObList::Serialize(ar);
	}
	MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
}

IMPLEMENT_SERIAL_SCHEMA(CExpKeyFrameObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CExpKeyFrameObjList, CObList)
IMPLEMENT_SERIAL_SCHEMA(CExplosionObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CExplosionObjList, CKEPObList)

} // namespace KEP