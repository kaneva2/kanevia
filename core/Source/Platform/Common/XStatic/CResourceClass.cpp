///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CResourceClass.h"

namespace KEP {

CResourceObject::CResourceObject() {
	m_type = 0;
	m_amount = 0;
	m_resourceName = "NAME";
	m_enableMaxAmountVar = FALSE;
	m_maxAmount = 0;
	m_regenRatePerSecond = 10;
	m_regenEnable = FALSE;
	m_lastUpdatedTime = 0;
}

void CResourceObject::Clone(CResourceObject** copy) {
	CResourceObject* copyLocal = new CResourceObject();
	copyLocal->m_type = m_type;
	copyLocal->m_amount = m_amount;
	copyLocal->m_resourceName = m_resourceName;
	*copy = copyLocal;
}

void CResourceObject::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_type
		   << m_amount
		   << m_resourceName
		   << m_enableMaxAmountVar
		   << m_maxAmount
		   << m_regenRatePerSecond
		   << m_regenEnable;
	} else {
		ar >> m_type >> m_amount >> m_resourceName >> m_enableMaxAmountVar >> m_maxAmount >> m_regenRatePerSecond >> m_regenEnable;

		m_lastUpdatedTime = 0;
	}
}

void CResourceObjectList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CResourceObject* resPtr = (CResourceObject*)GetNext(posLoc);
		delete resPtr;
		resPtr = 0;
	}
	RemoveAll();
}

void CResourceObjectList::Clone(CResourceObjectList** copy) {
	CResourceObjectList* copyLocal = new CResourceObjectList;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CResourceObject* resPtr = (CResourceObject*)GetNext(posLoc);
		CResourceObject* clonePtr = NULL;
		resPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}
	*copy = copyLocal;
}

void CResourceObjectList::RegenerativeCallback(TimeMs curTimeMs) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CResourceObject* resPtr = (CResourceObject*)GetNext(posLoc);
		if (resPtr->m_regenEnable == TRUE) {
			if ((curTimeMs - resPtr->m_lastUpdatedTime) >= 1000) {
				resPtr->m_amount += resPtr->m_regenRatePerSecond;
				if (resPtr->m_amount > resPtr->m_maxAmount)
					resPtr->m_amount = resPtr->m_maxAmount;

				resPtr->m_lastUpdatedTime = curTimeMs;
			}
		}
	}
}

CResourceObject* CResourceObjectList::GetByType(int type) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CResourceObject* resPtr = (CResourceObject*)GetNext(posLoc);
		if (resPtr->m_type == type) { //type match
			return resPtr;
		} //end match
	}
	return NULL;
}

int CResourceObjectList::AddResource(CStringA resName,
	int type,
	int amount,
	BOOL enableMaxVar,
	int maxAmount,
	BOOL regenEnable,
	int regenRatePersecond) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CResourceObject* resPtr = (CResourceObject*)GetNext(posLoc);
		if (resPtr->m_type == type) {
			if (resPtr->m_enableMaxAmountVar)
				if ((resPtr->m_amount + amount) > maxAmount)
					return 2; //no room

			resPtr->m_amount += amount;
			return 1; //success
		}
	}
	//if we made it this far it doesnt exist
	CResourceObject* resPtr = new CResourceObject();
	resPtr->m_amount = amount;
	resPtr->m_resourceName = resName;
	resPtr->m_type = type;
	resPtr->m_enableMaxAmountVar = enableMaxVar;
	resPtr->m_maxAmount = maxAmount;
	resPtr->m_regenEnable = regenEnable;
	resPtr->m_regenRatePerSecond = regenRatePersecond;
	AddTail(resPtr);
	return 1;
}

BOOL CResourceObjectList::UseResource(int type, int amount) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CResourceObject* resPtr = (CResourceObject*)GetNext(posLoc);
		if (resPtr->m_type == type && amount <= resPtr->m_amount) {
			resPtr->m_amount -= amount;
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CResourceObjectList::VerifyResource(int type, int amount) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CResourceObject* resPtr = (CResourceObject*)GetNext(posLoc);
		if (resPtr->m_type == type && amount <= resPtr->m_amount) {
			return TRUE;
		}
	}
	return FALSE;
}

IMPLEMENT_SERIAL(CResourceObject, CObject, 0)
IMPLEMENT_SERIAL(CResourceObjectList, CObList, 0)

BEGIN_GETSET_IMPL(CResourceObject, IDS_RESOURCEOBJECT_NAME)
GETSET_RANGE(m_type, gsInt, GS_FLAGS_DEFAULT, 0, 0, RESOURCEOBJECT, TYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_amount, gsInt, GS_FLAGS_DEFAULT, 0, 0, RESOURCEOBJECT, AMOUNT, INT_MIN, INT_MAX)
GETSET_MAX(m_resourceName, gsCString, GS_FLAGS_DEFAULT, 0, 0, RESOURCEOBJECT, RESOURCENAME, STRLEN_MAX)
GETSET(m_enableMaxAmountVar, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, RESOURCEOBJECT, ENABLEMAXAMOUNTVAR)
GETSET_RANGE(m_maxAmount, gsInt, GS_FLAGS_DEFAULT, 0, 0, RESOURCEOBJECT, MAXAMOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_regenRatePerSecond, gsInt, GS_FLAGS_DEFAULT, 0, 0, RESOURCEOBJECT, REGENRATEPERSECOND, INT_MIN, INT_MAX)
GETSET(m_regenEnable, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, RESOURCEOBJECT, REGENENABLE)
END_GETSET_IMPL

} // namespace KEP