///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CDayStateClass.h"
#include "CEnvironmentClass.h"

namespace KEP {

CDayStateObj::CDayStateObj(CEnvironmentObj* _baseEnvironment /* = NULL */) {
	m_fogStartRange = 800;
	m_fogEndRange = 3000;
	m_fogColorRed = 0.0f;
	m_fogColorGreen = 0.0f;
	m_fogColorBlue = 0.0f;
	m_sunRotationX = 0.0f;
	m_sunRotationY = 0.0f;
	m_sunDistance = 0;
	m_sunColorRed = 0.0f;
	m_sunColorGreen = 0.0f;
	m_sunColorBlue = 0.0f;
	m_materialModifier = NULL;
	m_ambientColorRed = 0.0f;
	m_ambientColorGreen = 0.0f;
	m_ambientColorBlue = 0.0f;
	m_duration = 0; //in milliseconds

	if (_baseEnvironment) {
		m_sunColorRed = _baseEnvironment->m_sunRed;
		m_sunColorGreen = _baseEnvironment->m_sunGreen;
		m_sunColorBlue = _baseEnvironment->m_sunBlue;

		m_ambientColorRed = _baseEnvironment->m_ambientLightRedDay;
		m_ambientColorGreen = _baseEnvironment->m_ambientLightGreenDay;
		m_ambientColorBlue = _baseEnvironment->m_ambientLightBlueDay;

		Vector3f origin = Vector3f(0.f, 0.f, 0.f);
		Vector3f sunLookAtOrigin = (Vector3f)_baseEnvironment->m_sunPosition - origin;

		m_sunDistance = sunLookAtOrigin.Length();

		float yaw = atan2(sunLookAtOrigin.x, sunLookAtOrigin.z);
		float pitch = -atan2(sunLookAtOrigin.y, sqrt((sunLookAtOrigin.x * sunLookAtOrigin.x) + (sunLookAtOrigin.z * sunLookAtOrigin.z)));

		m_sunRotationX = pitch * 180.f / M_PI;
		m_sunRotationY = yaw * 180.f / M_PI;
	}
}

void CDayStateObj::SafeDelete() {
	if (m_materialModifier)
		delete m_materialModifier;
	m_materialModifier = 0;
}

BOOL CDayStateObjList::GetCurrentDayState(
	TimeMs currentDayTime,
	CDayStateObj** currentState,
	CDayStateObj** targetState,
	float* currentDuration,
	float* currentTimePassed) {
	long timePosition = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDayStateObj* spnPtr = (CDayStateObj*)GetNext(posLoc);
		if (currentDayTime < (spnPtr->m_duration + timePosition)) {
			*currentState = spnPtr;

			if (posLoc)
				*targetState = (CDayStateObj*)GetAt(posLoc);
			else
				*targetState = (CDayStateObj*)GetHead();

			*currentDuration = (float)spnPtr->m_duration;
			*currentTimePassed = (float)(spnPtr->m_duration - ((spnPtr->m_duration + timePosition) - currentDayTime));

			return TRUE;
		}
		timePosition += spnPtr->m_duration;
	}
	return FALSE;
}

CDayStateObjList::CDayStateObjList() {
	m_lastDayState = NULL;
}

void CDayStateObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDayStateObj* spnPtr = (CDayStateObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

long CDayStateObjList::GetCompleteCycleLength() {
	long total = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDayStateObj* spnPtr = (CDayStateObj*)GetNext(posLoc);
		total += spnPtr->m_duration;
	}
	return total;
}

void CDayStateObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_fogStartRange
		   << m_fogEndRange
		   << m_fogColorRed
		   << m_fogColorGreen
		   << m_fogColorBlue
		   << m_materialModifier
		   << m_sunRotationX
		   << m_sunRotationY
		   << m_sunDistance
		   << m_sunColorRed
		   << m_sunColorGreen
		   << m_sunColorBlue
		   << m_ambientColorRed
		   << m_ambientColorGreen
		   << m_ambientColorBlue
		   << m_duration;
	} else {
		ar >> m_fogStartRange >> m_fogEndRange >> m_fogColorRed >> m_fogColorGreen >> m_fogColorBlue >> m_materialModifier >> m_sunRotationX >> m_sunRotationY >> m_sunDistance >> m_sunColorRed >> m_sunColorGreen >> m_sunColorBlue >> m_ambientColorRed >> m_ambientColorGreen >> m_ambientColorBlue >> m_duration;
	}
}

IMPLEMENT_SERIAL(CDayStateObj, CObject, 0)
IMPLEMENT_SERIAL(CDayStateObjList, CObList, 0)

BEGIN_GETSET_IMPL(CDayStateObj, IDS_DAYSTATEOBJ_NAME)
GETSET_RANGE(m_duration, gsLong, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, DURATION, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_fogStartRange, gsFloat, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, FOGSTARTRANGE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_fogEndRange, gsFloat, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, FOGENDRANGE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_sunRotationX, gsFloat, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, SUNROTATIONX, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_sunRotationY, gsFloat, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, SUNROTATIONY, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_sunDistance, gsLong, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, SUNDISTANCE, LONG_MIN, LONG_MAX)
GETSET_RGB(m_fogColorRed, m_fogColorGreen, m_fogColorBlue, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, FOGCOLOR)
GETSET_RGB(m_sunColorRed, m_sunColorGreen, m_sunColorBlue, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, SUNCOLOR)
GETSET_RGB(m_ambientColorRed, m_ambientColorGreen, m_ambientColorBlue, GS_FLAGS_DEFAULT, 3, 5, DAYSTATEOBJ, AMBIENTCOLOR)
END_GETSET_IMPL

} // namespace KEP