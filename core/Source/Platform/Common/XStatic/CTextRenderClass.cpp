///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS

#include "resource.h"
#include "ClientStrings.h"
#include "d3d9.h"
#include <afxtempl.h>

#include "IClientEngine.h"
#include "CTextRenderClass.h"
#include "common/include/IMemSizeGadget.h"
#include "CAdvancedVertexClass.h"
#include "CMaterialClass.h"
#include "CStateManagementClass.h"

namespace KEP {

// DRF - Added - Singleton Instance
CSymbolMapperObjList* CSymbolMapperObjList::pInstance = nullptr;

CTextRenderObj::CTextRenderObj(
	const std::string& text,
	int mode,
	double startPosX,
	double startPosY,
	double fontSizeX,
	double fontSizeY,
	bool tempObj,
	double red,
	double green,
	double blue) :
		m_vertexObject(nullptr),
		m_selfDeleteIt(tempObj), m_blendMode(mode), m_startPosX(startPosX), m_startPosY(startPosY), m_fontSizeX(fontSizeX), m_fontSizeY(fontSizeY), m_measuredDistanceRef(0), m_lastStartPosX(-99), m_lastStartPosY(-99), m_unitLength(0), m_nextSymbol(nullptr), m_nextPosLoc(nullptr), m_initialized(false), m_constChangeTicks(0) {
	m_worldMatrix.MakeIdentity();

	m_materialObj = new CMaterialObject();
	m_materialObj->m_baseBlendMode = mode;
	m_materialObj->m_baseMaterial.Ambient.r = 0.0f;
	m_materialObj->m_baseMaterial.Ambient.g = 0.0f;
	m_materialObj->m_baseMaterial.Ambient.b = 0.0f;
	m_materialObj->m_baseMaterial.Ambient.a = 0.0f;
	m_materialObj->m_baseMaterial.Diffuse.r = 0.0f;
	m_materialObj->m_baseMaterial.Diffuse.g = 0.0f;
	m_materialObj->m_baseMaterial.Diffuse.b = 0.0f;
	m_materialObj->m_baseMaterial.Diffuse.a = 0.0f;
	m_materialObj->m_baseMaterial.Emissive.r = red;
	m_materialObj->m_baseMaterial.Emissive.g = green;
	m_materialObj->m_baseMaterial.Emissive.b = blue;
	m_materialObj->m_baseMaterial.Emissive.a = 1.0f;
	m_materialObj->Reset();

	// DRF - Added - Also calls Setup to calculate unit length
	SetText(text);
}

void CTextRenderObj::SafeDelete() {
	if (m_materialObj) {
		delete m_materialObj;
		m_materialObj = nullptr;
	}

	if (m_vertexObject) {
		m_vertexObject->SafeDelete();
		delete m_vertexObject;
		m_vertexObject = nullptr;
	}
}

void CTextRenderObj::Setup(CSymbolMapperObjList* pSMOL) {
	// DRF - Added - Use Singleton Mapper ?
	if (!pSMOL)
		pSMOL = CSymbolMapperObjList::Instance();
	if (!pSMOL)
		return;

	m_textObsList.RemoveAll();

	// Calculate Text Unit Length (x size)
	double length = 0.0;
	size_t count = m_text.size();
	for (size_t i = 0; i < count; ++i) {
		CStringA id(m_text[i]);
		auto pSMO = (CSymbolMapperObj*)pSMOL->GetByID(id);
		if (!pSMO)
			continue;
		m_textObsList.AddCharacter(pSMO);
		length += pSMO->m_percentageX;
	}
	m_unitLength = length;
	m_nextSymbol = nullptr;
	m_nextPosLoc = nullptr;

	m_constChangeTicks = 0;
}

void CTextRenderObj::UpdateGeometry(
	LPDIRECT3DDEVICE9 g_pd3dDevice,
	const std::string& textStr,
	bool constantlyChanging,
	CSymbolMapperObjList* pSMOL) {
	// DRF - Added - Use Singleton Mapper ?
	if (!pSMOL)
		pSMOL = CSymbolMapperObjList::Instance();
	if (!pSMOL)
		return;

	m_materialObj->Callback();

	// DRF - Label Size Not Constantly Changing Bug Fix
	// Refresh On Text Change Or Constantly Changing Time
	bool textChange = (textStr != m_text);
	bool constChange = (constantlyChanging && (m_constChangeTicks == 0));
	bool change = (textChange || constChange);
	if (m_vertexObject && change) {
		m_vertexObject->SafeDelete();
		delete m_vertexObject;
		m_vertexObject = nullptr;
		m_initialized = false;
	}

	// DRF - Label Size Not Constantly Changing Bug Fix
	// Update Constant Change Ticks (2 times per second assuming 60hz frame rate)
	++m_constChangeTicks;
	if (m_constChangeTicks > 30)
		m_constChangeTicks = 0;

	// Already Initialized And Not Changing?
	if (!change && m_initialized)
		return;
	m_initialized = true;

	if (!m_vertexObject)
		m_vertexObject = new CAdvancedVertexObj();

	// Set Text String (calls Setup on text change to calculate new unit length)
	SetText(textStr);

	size_t textLength = textStr.size();

	m_vertexObject->m_uvCount = 1;

	m_lastStartPosX = m_startPosX;
	m_lastStartPosY = m_startPosY;

	double m_41 = m_startPosX;
	double m_42 = m_startPosY;
	double characterSpacing = m_fontSizeX * 2.0f;
	double zDist = 0.0f;

	m_vertexObject->m_vertexCount = textLength * 4;
	m_vertexObject->m_indecieCount = textLength * 6;
	if (!m_vertexObject->m_vertexArray1)
		m_vertexObject->m_vertexArray1 = new ABVERTEX1L[m_vertexObject->m_vertexCount];

	ZeroMemory(m_vertexObject->m_vertexArray1, sizeof(ABVERTEX1L) * m_vertexObject->m_vertexCount);

	// Prepare Mesh
	UINT* indexArray = new UINT[m_vertexObject->m_indecieCount];
	int vertexTrace = 0;
	int indecieTrace = 0;
	bool startingChar = true;
	double lastSpacing = characterSpacing;
	int skipFunction = 0;
	bool initIt = false;
	double currentMultiplier = 1.0f;

	// Build Mesh
	CStringA value, oldValue;
	for (size_t loop = 0; loop < textLength; loop++) {
		value = m_text[loop];
		oldValue = value;
		bool foundSymbol = false;
		if (value == "~") {
			if ((textLength - loop) > 1) {
				value = "";
				for (size_t buildX = loop; buildX < textLength; buildX++) {
					char xtemp = m_text[buildX];
					value += xtemp;
					if (((int)xtemp) >= '0' && ((int)xtemp) <= '9') {
						buildX++;
						if (buildX < textLength) {
							char xtemp2 = m_text[buildX];
							if (((int)xtemp2) >= '0' && ((int)xtemp2) <= '9')
								value += xtemp2;
						}

						if (pSMOL->GetByID(value)) {
							foundSymbol = true;
							skipFunction = value.GetLength();
							initIt = true;
						}
						initIt = true;
						break;
					}
				}

				if (!foundSymbol)
					value = "~";
			}
		}

		if (skipFunction > 0) {
			skipFunction--;
			if (!initIt)
				currentMultiplier = 0.0f;
			else
				currentMultiplier = 1.0f;
			initIt = false;
		} else {
			currentMultiplier = 1.0f;
		}

		// Symbol Found ?
		CSymbolMapperObj* pSMO = GetSymbol(value);
		if (pSMO) {
			double currentSpacing = (characterSpacing * pSMO->m_percentageX);
			if (!startingChar) {
				if (pSMO->m_percentageX > 1.0)
					lastSpacing = (characterSpacing * (double)pSMO->m_percentageX);
				m_41 += (((currentSpacing * .5f) + (lastSpacing * .5f)) * currentMultiplier);
			}
			lastSpacing = currentSpacing;

			startingChar = false;

			m_vertexObject->m_vertexArray1[vertexTrace].tx0.tu = pSMO->m_uvIndexLeft * currentMultiplier;
			m_vertexObject->m_vertexArray1[vertexTrace].tx0.tv = pSMO->m_uvIndexBottom * currentMultiplier;
			m_vertexObject->m_vertexArray1[vertexTrace + 1].tx0.tu = pSMO->m_uvIndexLeft * currentMultiplier;
			m_vertexObject->m_vertexArray1[vertexTrace + 1].tx0.tv = pSMO->m_uvIndexTop * currentMultiplier;
			m_vertexObject->m_vertexArray1[vertexTrace + 2].tx0.tu = pSMO->m_uvIndexRight * currentMultiplier;
			m_vertexObject->m_vertexArray1[vertexTrace + 2].tx0.tv = pSMO->m_uvIndexTop * currentMultiplier;
			m_vertexObject->m_vertexArray1[vertexTrace + 3].tx0.tu = pSMO->m_uvIndexRight * currentMultiplier;
			m_vertexObject->m_vertexArray1[vertexTrace + 3].tx0.tv = pSMO->m_uvIndexBottom * currentMultiplier;

			//verticies
			m_vertexObject->m_vertexArray1[vertexTrace].x = (-m_fontSizeX * currentMultiplier) + m_41;
			m_vertexObject->m_vertexArray1[vertexTrace].y = -m_fontSizeY + m_42;
			m_vertexObject->m_vertexArray1[vertexTrace + 1].x = (-m_fontSizeX * currentMultiplier) + m_41;
			m_vertexObject->m_vertexArray1[vertexTrace + 1].y = m_fontSizeY + m_42;
			m_vertexObject->m_vertexArray1[vertexTrace + 2].x = (m_fontSizeX * currentMultiplier) + m_41;
			m_vertexObject->m_vertexArray1[vertexTrace + 2].y = m_fontSizeY + m_42;
			m_vertexObject->m_vertexArray1[vertexTrace + 3].x = (m_fontSizeX * currentMultiplier) + m_41;
			m_vertexObject->m_vertexArray1[vertexTrace + 3].y = -m_fontSizeY + m_42;
		}

		m_vertexObject->m_vertexArray1[vertexTrace].z = zDist;
		m_vertexObject->m_vertexArray1[vertexTrace].nx = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace].ny = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace].nz = -1.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 1].z = zDist;
		m_vertexObject->m_vertexArray1[vertexTrace + 1].nx = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 1].ny = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 1].nz = -1.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 2].z = zDist;
		m_vertexObject->m_vertexArray1[vertexTrace + 2].nx = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 2].ny = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 2].nz = -1.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 3].z = zDist;
		m_vertexObject->m_vertexArray1[vertexTrace + 3].nx = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 3].ny = 0.0f;
		m_vertexObject->m_vertexArray1[vertexTrace + 3].nz = -1.0f;

		indexArray[indecieTrace] = vertexTrace;
		indexArray[indecieTrace + 1] = vertexTrace + 1;
		indexArray[indecieTrace + 2] = vertexTrace + 2;
		indexArray[indecieTrace + 3] = vertexTrace + 2;
		indexArray[indecieTrace + 4] = vertexTrace + 3;
		indexArray[indecieTrace + 5] = vertexTrace;

		vertexTrace += 4;
		indecieTrace += 6;
	}

	m_vertexObject->SetVertexIndexArray(indexArray, m_vertexObject->m_indecieCount, m_vertexObject->m_vertexCount);
	delete[] indexArray;

	m_vertexObject->InitBuffer(g_pd3dDevice, m_vertexObject->m_uvCount);
}

void CTextRenderObj::Render(LPDIRECT3DDEVICE9 g_pd3dDevice, CStateManagementObj* stateManager) {
	if (!stateManager || !m_vertexObject)
		return;
	stateManager->SetDiffuseSource(m_vertexObject->m_advancedFixedMode);
	stateManager->SetVertexType(m_vertexObject->m_uvCount);
	m_vertexObject->Render(g_pd3dDevice, stateManager->m_currentVertexType);
}

void CTextRenderObjList::Flush() {
	RemoveAll();
}

void CTextRenderObjList::DeleteFinishedTextDisplays() {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != nullptr;) {
		auto pTRO = (CTextRenderObj*)GetNext(posLoc);
		if (pTRO->m_selfDeleteIt == TRUE) {
			pTRO->SafeDelete();
			delete pTRO;
			pTRO = nullptr;
			RemoveAt(posLast);
		}
	}
}

void CTextRenderObjList::SafeDelete() {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pTRO = (CTextRenderObj*)GetNext(pos);
		pTRO->SafeDelete();
		delete pTRO;
		pTRO = nullptr;
	}
	RemoveAll();
}

void CTextRenderObjList::AddTextObject(
	const std::string& text,
	int mode,
	double startPosX,
	double startPosY,
	double fontSizeX,
	double fontSizeY,
	bool tempObj) {
	auto pTRO = new CTextRenderObj(
		text,
		mode,
		startPosX,
		startPosY,
		fontSizeX,
		fontSizeY,
		tempObj,
		1.0f,
		1.0f,
		1.0f);
	if (pTRO)
		AddTail(pTRO);
}

CSymbolMapperObj::CSymbolMapperObj() :
		m_percentageX(1.0f), m_uvIndexLeft(0.0f), m_uvIndexRight(0.0f), m_uvIndexTop(0.0f), m_uvIndexBottom(0.0f), m_gridDensity(16), m_origGridIndexHoriz(0.0f), m_origGridIndexVert(0.0f) {
}

void CSymbolMapperObj::Assign(
	int gridDensity,
	double horizOffset,
	double vertOffset,
	const CStringA& id,
	double xPercentage) {
	m_uniqueID = id;
	m_origGridIndexHoriz = (float)horizOffset;
	m_origGridIndexVert = (float)vertOffset;
	m_gridDensity = gridDensity;
	m_percentageX = (float)xPercentage;

	double spacing = 1.0f / ((double)gridDensity);
	m_uvIndexLeft = (float)(horizOffset * spacing);
	m_uvIndexRight = (float)((horizOffset + 1) * spacing);
	m_uvIndexTop = (float)(vertOffset * spacing);
	m_uvIndexBottom = (float)((vertOffset + 1) * spacing);
}

void CSymbolMapperObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(2);
		ASSERT(ar.GetObjectSchema() == 2);
		ar << m_percentageX
		   << m_uniqueID
		   << m_uvIndexLeft
		   << m_uvIndexRight
		   << m_uvIndexTop
		   << m_uvIndexBottom
		   << m_gridDensity
		   << m_origGridIndexHoriz
		   << m_origGridIndexVert
		   << reserved
		   << reserved
		   << m_group
		   << m_extendedOption;
	} else {
		int version = ar.GetObjectSchema();

		if (version < 2) {
			unsigned int tempLoc;
			ar >> m_percentageX >> tempLoc >> m_uvIndexLeft >> m_uvIndexRight >> m_uvIndexTop >> m_uvIndexBottom >> m_gridDensity >> m_origGridIndexHoriz >> m_origGridIndexVert;
			char temploc2[2];
			temploc2[0] = (char)tempLoc;
			temploc2[1] = 0;
			m_uniqueID = temploc2;
			m_group = "";
			m_extendedOption = "";
		} else {
			ar >> m_percentageX >> m_uniqueID >> m_uvIndexLeft >> m_uvIndexRight >> m_uvIndexTop >> m_uvIndexBottom >> m_gridDensity >> m_origGridIndexHoriz >> m_origGridIndexVert >> reserved >> reserved >> m_group >> m_extendedOption;
		}
	}
}

void CSymbolMapperObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != nullptr;) {
		CSymbolMapperObj* spnPtr = (CSymbolMapperObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = nullptr;
	}
	RemoveAll();
}

CSymbolMapperObj* CSymbolMapperObjList::GetByID(const CStringA& id) const {
	for (POSITION pos = GetHeadPosition(); pos;) {
		CSymbolMapperObj* spnPtr = (CSymbolMapperObj*)GetNext(pos);
		if (spnPtr->m_uniqueID == id)
			return spnPtr;
	}
	return nullptr;
}

CSymbolMapperObj* CTextRenderObj::GetSymbol(const CStringA& id) {
	// Quick hack because this is usually sequential first check the pos next to last one queried
	CSymbolMapperObj* answer;
	if (m_nextSymbol) {
		answer = m_nextSymbol;
		if (answer->m_uniqueID == id) {
			if (!m_nextPosLoc)
				m_nextPosLoc = m_textObsList.GetHeadPosition();
			m_nextSymbol = (CSymbolMapperObj*)m_textObsList.GetNext(m_nextPosLoc);
			return answer;
		}
	}

	// else search the whole list.
	for (m_nextPosLoc = m_textObsList.GetHeadPosition(); m_nextPosLoc != nullptr;) {
		answer = (CSymbolMapperObj*)m_textObsList.GetNext(m_nextPosLoc);
		if (answer->m_uniqueID == id) {
			if (m_nextPosLoc == nullptr) {
				m_nextSymbol = nullptr;
				return answer;
			}
			m_nextSymbol = (CSymbolMapperObj*)m_textObsList.GetNext(m_nextPosLoc);
			return answer;
		}
	}

	return nullptr;
}

void CTempRenderStringObList::SafeDelete() {
	RemoveAll();
}

void CTempRenderStringObList::AddCharacter(CSymbolMapperObj* newSymbol) {
	AddTail(newSymbol);
}

void CSymbolMapperObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_extendedOption);
		pMemSizeGadget->AddObject(m_group);
		pMemSizeGadget->AddObject(m_uniqueID);
	}
}

void CTempRenderStringObList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CSymbolMapperObj* pCSymbolMapperObject = static_cast<const CSymbolMapperObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCSymbolMapperObject);
		}
	}
}

void CTextRenderObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_nextSymbol);
		pMemSizeGadget->AddObject(m_materialObj);
		pMemSizeGadget->AddObject(m_text);
		pMemSizeGadget->AddObject(m_textObsList);
		pMemSizeGadget->AddObject(m_vertexObject);
	}
}

void CSymbolMapperObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CSymbolMapperObj* pCSymbolMapperObj = static_cast<const CSymbolMapperObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCSymbolMapperObj);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CSymbolMapperObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CSymbolMapperObjList, CKEPObList)

BEGIN_GETSET_IMPL(CSymbolMapperObj, IDS_SYMBOLMAPPEROBJ_NAME)
GETSET_RANGE(m_percentageX, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, PERCENTAGEX, FLOAT_MIN, FLOAT_MAX)
GETSET_MAX(m_uniqueID, gsCString, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, UNIQUEID, STRLEN_MAX)
GETSET_RANGE(m_uvIndexLeft, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, UVINDEXLEFT, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_uvIndexRight, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, UVINDEXRIGHT, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_uvIndexTop, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, UVINDEXTOP, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_uvIndexBottom, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, UVINDEXBOTTOM, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_gridDensity, gsInt, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, GRIDDENSITY, INT_MIN, INT_MAX)
GETSET_RANGE(m_origGridIndexHoriz, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, ORIGGRIDINDEXHORIZ, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_origGridIndexVert, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, ORIGGRIDINDEXVERT, FLOAT_MIN, FLOAT_MAX)
GETSET_MAX(m_group, gsCString, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, GROUP, STRLEN_MAX)
GETSET_MAX(m_extendedOption, gsCString, GS_FLAGS_DEFAULT, 0, 0, SYMBOLMAPPEROBJ, EXTENDEDOPTION, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP