///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CPlayerClass.h"
#include "FriendList.h"

namespace KEP {

FriendInfo::FriendInfo() {
	m_handle = 0;
}

void FriendInfo::Clone(FriendInfo** clone) {
	FriendInfo* copyLocal = new FriendInfo();

	copyLocal->m_handle = m_handle;
	copyLocal->m_name = m_name;

	*clone = copyLocal;
}

void FriendList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		FriendInfo* spnPtr = (FriendInfo*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void FriendList::Clone(FriendList** clone) {
	FriendList* copyLocal = new FriendList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		FriendInfo* groupPtr = (FriendInfo*)GetNext(posLoc);
		FriendInfo* copyPtr = NULL;
		groupPtr->Clone(&copyPtr);
		copyLocal->AddTail(copyPtr);
	}
	*clone = copyLocal;
}

void FriendInfo::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_name << m_handle;
	} else {
		ar >> m_name >> m_handle;
	}
}

void FriendInfo::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_name << m_handle;
	} else {
		ar >> m_name >> m_handle;
	}
}

//------------------------------------------------------------------//
//------------------------------------------------------------------//
//------------------------------------------------------------------//
void FriendList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			FriendInfo* elementPtr = (FriendInfo*)GetNext(posLoc);
			elementPtr->SerializeAlloc(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			FriendInfo* elementPtr = new FriendInfo();
			elementPtr->SerializeAlloc(ar);
			AddTail(elementPtr);
		}
	}
}

IMPLEMENT_SERIAL(FriendInfo, CObject, 0)
IMPLEMENT_SERIAL(FriendList, CObList, 0)

BEGIN_GETSET_IMPL(FriendInfo, IDS_COMMGROUPOBJ_NAME)
GETSET_MAX(m_name, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, COMMGROUPOBJ, NAME, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP