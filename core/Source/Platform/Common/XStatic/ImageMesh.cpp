/******************************************************************************
 ImageMesh.cpp
 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifdef XSTATIC_BUILD
#include ".\stdafx.h"
#else
#include "..\..\engine\clientblades\kepmenu\dxstdafx.h"
#endif

#include "..\include\corestatic\ImageMesh.h"
#include "..\..\Tools\zlib\zlib.h"

#include "KEPConfig.h"
#include "KEPException.h"

#include <d3d9.h>
#include <d3dx9tex.h>

#include <sys/stat.h>

ULONG ImageMesh::m_waitMs = 50;
#define IDB_TEST 9999

ImageMesh::ImageMesh(IDispatcher* disp) :
		m_lastRender(0),
		m_imageWidth(400),
		m_imageHeight(300),
		m_colorDepth(3),
		m_pDIRECT3DDEVICE9(0),
		m_logger(Logger::getInstance("ImageMesh")) {
}

bool ImageMesh::Initialize(IN const char* baseDir, IN HWND parent, IN const char* url,
	IN DWORD width /*= 0*/, IN DWORD height /*= 0*/,
	IN DWORD x /*= ULONG_MAX*/, IN DWORD y /*= ULONG_MAX*/) {
	jsVerifyReturn(url != 0 && baseDir != 0, false);

	kstring msg;

	m_baseDir = baseDir;

	// override the cfg file with parameters
	if (width != 0 && width < 5000)
		m_imageWidth = width;

	if (height != 0 && height < 5000)
		m_imageHeight = height;

	m_url = url;

	// load the url
	bool ret = false;

	// TODO: any initialization for the URL

	// load the test bitmap
	m_bitmap.LoadBitmap(IDB_TEST);
	CSize sz = m_bitmap.GetBitmapDimension();
	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	m_imageHeight = bm.bmHeight;
	m_imageWidth = bm.bmWidth;
	m_colorDepth = 3; // 4 breaks it  see usage (bm.bmBitsPixel + 7) / 8 ;  // round up to nearest byte
	BYTE bits[500];
	m_bitmap.GetBitmapBits(500, bits); // why alpha zero??

	return ret;
}

bool ImageMesh::SendInputMessage(UINT msg, WPARAM wp, LPARAM lp) {
	// don't take input, not in menus
	return false;
}

LPDIRECT3DTEXTURE9 ImageMesh::CreateTextureFromWindow() {
	DWORD now = TickCount();
	// only get the image from Image every so often
	if (TickDiff(now, m_lastRender) > m_waitMs) {
		m_lastRender = now;
		if (NULL != m_pTexture)
			m_pTexture->Release();
		m_pTexture = Image_Render(m_pDIRECT3DDEVICE9);
	}

	return m_pTexture;
}

int ImageMesh::UpdateTexture(IN LPDIRECT3DDEVICE9 dev) {
	m_pDIRECT3DDEVICE9 = dev;

	int ret = 1;

	CreateTextureFromWindow();

	return ret;
}

bool ImageMesh::UpdateRect(INT x, INT width, INT y, INT height) {
	bool ret = true;

	m_imageWidth = width;
	m_imageHeight = height;
	return ret;
}

ImageMesh::~ImageMesh() {
	// TODO: free resources
}

LPDIRECT3DTEXTURE9 ImageMesh::Image_Render(LPDIRECT3DDEVICE9 device3d) {
	LPDIRECT3DTEXTURE9 pTexture = NULL;
	HRESULT hr = S_OK;

	D3DXCreateTexture(device3d,
		m_imageWidth,
		m_imageHeight,
		1,
		0,
		m_colorDepth == 3 ? D3DFMT_R8G8B8 : D3DFMT_A8R8G8B8,
		D3DPOOL_MANAGED,
		&pTexture);

	if (NULL == pTexture)
		return pTexture;

	D3DSURFACE_DESC d3dsd;
	pTexture->GetLevelDesc(0, &d3dsd);

	// Size
	SIZE size = { d3dsd.Width, d3dsd.Height };

	IDirect3DSurface9* surface;
	hr = pTexture->GetSurfaceLevel(0, &surface);
	if (D3D_OK == hr) {
		HBITMAP hBitmap = m_bitmap;
		VERIFY(hBitmap != 0);

		// TODO: better way to copy bits?
		HDC hDc = GetDC(AfxGetMainWnd()->m_hWnd);

		HDC memDC;
		memDC = CreateCompatibleDC(hDc);

		HGDIOBJ oldbm = SelectObject(memDC, hBitmap);

		HDC dc;
		HRESULT hr = surface->GetDC(&dc);
		VERIFY(D3D_OK == hr);
		if (!BitBlt(dc, 0, 0, d3dsd.Width, d3dsd.Height, memDC, 0, 0, SRCCOPY)) {
			LOG4CPLUS_ERROR(m_logger, "BitBlt failed.  Err=" << GetLastError());
			ASSERT(false);
		}
		surface->ReleaseDC(dc);
		surface->Release();

		DeleteObject(hBitmap);
		DeleteDC(memDC);
		SelectObject(memDC, oldbm);

		return pTexture;
	} else {
		ASSERT(FALSE);
		LOG4CPLUS_ERROR(m_logger, "GetSurfaceLevel failed.  HR=" << hr);
	}

	return 0;
}
