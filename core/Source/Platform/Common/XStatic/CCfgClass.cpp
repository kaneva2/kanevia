///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CCfgClass.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

CCfgObj::CCfgObj() {
	m_globalCFG = -1;
	m_armed = FALSE;
	m_quanity = 1;
}

void CCfgObj::Clone(CCfgObj** clone) {
	CCfgObj* copyLocal = new CCfgObj();

	copyLocal->m_globalCFG = m_globalCFG;
	copyLocal->m_armed = m_armed;
	copyLocal->m_quanity = m_quanity;

	*clone = copyLocal;
}

void CCfgObjList::Clone(CCfgObjList** clone) {
	CCfgObjList* copyLocal = new CCfgObjList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CCfgObj* bnPtr = (CCfgObj*)GetNext(posLoc);
		CCfgObj* newObject = NULL;
		bnPtr->Clone(&newObject);
		copyLocal->AddTail(newObject);
	}
	*clone = copyLocal;
}

void CCfgObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CCfgObj* spnPtr = (CCfgObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CCfgObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_globalCFG
		   << m_armed
		   << m_quanity;

	} else {
		ar >> m_globalCFG >> m_armed >> m_quanity;
	}
}

void CCfgObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CCfgObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CCfgObj* pCCfgObj = static_cast<const CCfgObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCCfgObj);
		}
	}
}

IMPLEMENT_SERIAL(CCfgObj, CObject, 0)
IMPLEMENT_SERIAL(CCfgObjList, CObList, 0)

BEGIN_GETSET_IMPL(CCfgObj, IDS_CFGOBJ_NAME)
GETSET_RANGE(m_globalCFG, gsInt, GS_FLAGS_DEFAULT, 0, 0, CFGOBJ, GLOBALCFG, INT_MIN, INT_MAX)
GETSET_RANGE(m_armed, gsInt, GS_FLAGS_DEFAULT, 0, 0, CFGOBJ, ARMED, INT_MIN, INT_MAX)
GETSET_RANGE(m_quanity, gsInt, GS_FLAGS_DEFAULT, 0, 0, CFGOBJ, QUANITY, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP