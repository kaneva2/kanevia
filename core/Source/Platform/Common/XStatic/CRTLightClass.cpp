///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <d3dx9.h>
#include "CRTLightClass.h"
#include <algorithm>
#include "CSkeletonObject.h"
#include "MaterialPersistence.h"
#ifdef _ClientOutput
#include "IClientEngine.h"
#include "ReD3DX9.h"
#include "ReMaterial.h"
#endif
#include "Core/Math/Interpolate.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

CRTLightObj::CRTLightObj() :
		m_range(0.0f),
		m_attenuation0(1.0f),
		m_attenuation1(0.0f),
		m_attenuation2(0.0f),
		m_type(D3DLIGHT_POINT),
		m_direction(0.0f, 0.0f, 1.0f),
		m_theta(0.0f),
		m_phi(0.0f),
		m_falloff(0.0f),
		m_distFromChar(0.0f),
		m_fadeOn(false),
		m_blinkOn(false),
		m_blinkDur(0),
		m_randomOn(false),
		m_wldspacePos(0.0f, 0.0f, 0.0f),
		m_render(true) {
}

void CRTLightObj::Clone(CRTLightObj** clone) {
	CRTLightObj* copyLocal = new CRTLightObj();

	if (m_material) {
		CMaterialObject* pMaterial = nullptr;
		m_material->Clone(&pMaterial);
		copyLocal->m_material.reset(pMaterial);
	}

	copyLocal->m_range = m_range;
	copyLocal->m_attenuation0 = m_attenuation0;
	copyLocal->m_attenuation1 = m_attenuation1;
	copyLocal->m_attenuation2 = m_attenuation2;

	copyLocal->m_render = m_render;

	//DPD
	copyLocal->m_type = m_type;
	copyLocal->m_direction = m_direction;
	copyLocal->m_theta = m_theta;
	copyLocal->m_phi = m_phi;
	copyLocal->m_falloff = m_falloff;

	//DPD2
	copyLocal->m_distFromChar = m_distFromChar;
	copyLocal->m_fadeOn = m_fadeOn;

	//DPD4
	copyLocal->m_blinkOn = m_blinkOn;
	copyLocal->m_blinkDur = m_blinkDur;
	copyLocal->m_randomOn = m_randomOn;

	*clone = copyLocal;
}

void CRTLightObjList::FlushSys(LPDIRECT3DDEVICE9 g_pd3dDevice, CStateManagementObj* stateManager) {
	if (g_pd3dDevice == 0 || stateManager == 0)
		return;

	// Disable all lights (except the sun which is index 0)
	for (UINT index = 1; index < stateManager->m_maxActiveLights; index++) {
		g_pd3dDevice->LightEnable(index, FALSE);
		stateManager->SetSTLight(index, D3DVECTOR{ 0, 0, 0 }, 0.0f, 0.0f, 0.0f, 0.0f);
	}

	m_apLights.clear();
}

void CRTLightObjList::AddLight(CRTLightObj* light,
	const Vector3f& position) {
	// make sure this light isn't already in our list
	if (Contains(light))
		return;

	if (!light->m_material)
		return;

	//DPD4 Blinking
	if (light->m_blinkOn) {
		long blinkd;

		if (light->m_randomOn) {
			blinkd = (rand()) / (RAND_MAX + 1) * light->m_blinkDur;
		}
		blinkd = light->m_blinkDur;
		light->m_material->SetFadeIn(blinkd, TRUE);
	}

	//set position
	light->m_wldspacePos = position;

	m_apLights.push_back(light);
}

void CRTLightObjList::AddLight(CRTLightObj* light,
	const Vector3f& position,
	const Vector3f& charPosition) {
	// make sure this light isn't already in our list
	if (Contains(light))
		return;

	if (!light->m_material)
		return;

	//DPD4 Blinking
	if (light->m_blinkOn) {
		long blinkd;

		if (light->m_randomOn) {
			blinkd = (rand()) / (RAND_MAX + 1) * light->m_blinkDur;
		} else {
			blinkd = light->m_blinkDur;
		}

		light->m_material->SetFadeIn(blinkd, TRUE);
	}

	//set position
	light->m_wldspacePos.x = position.x;
	light->m_wldspacePos.y = position.y;
	light->m_wldspacePos.z = position.z;

	//DPD2 set light distance from char
	light->m_distFromChar = Distance(charPosition, position);

	m_apLights.push_back(light);
}

class LightSortPredicate {
public:
	LightSortPredicate(Vector3f& pos) {
		m_position = pos;
	}

	bool operator()(CRTLightObj* first, CRTLightObj* second) {
		float dx1 = first->m_wldspacePos.x - m_position.x;
		float dy1 = first->m_wldspacePos.y - m_position.y;
		float dz1 = first->m_wldspacePos.z - m_position.z;

		float sqrdist1 = (dx1 * dx1 + dy1 * dy1 + dz1 * dz1);

		float dx2 = second->m_wldspacePos.x - m_position.x;
		float dy2 = second->m_wldspacePos.y - m_position.y;
		float dz2 = second->m_wldspacePos.z - m_position.z;

		float sqrdist2 = (dx2 * dx2 + dy2 * dy2 + dz2 * dz2);

		// if only one light is in range return whether it's this one
		bool inRange1(false), inRange2(false);
		if ((inRange1 = (sqrdist1 < (first->m_range * first->m_range))) ^ (inRange2 = (sqrdist2 < (second->m_range * second->m_range)))) {
			if (inRange1)
				return true;
			else
				return false;
		}

		// if both are in range or both are out of range return whether the first is closer than the 2nd
		return sqrdist1 < sqrdist2;
	}

	Vector3f m_position;
};

void CRTLightObjList::EnableLightsForPosition(LPDIRECT3DDEVICE9 device, CStateManagementObj* state_manager,
	Vector3f& position) {
	std::vector<CRTLightObj*> found_lights;

	for (CRTLightObj* light : m_apLights)
		found_lights.push_back(light);

	std::sort(found_lights.begin(), found_lights.end(), LightSortPredicate(position));

	unsigned int i, d3dLightIndex;
	D3DLIGHT9 lightImStruct;

	// we skip the first light for the sun
	for (i = 0, d3dLightIndex = 1; (i < (unsigned int)state_manager->m_maxActiveLights); i++, d3dLightIndex++) {
		// If we are here and gone over the number of available lights we disable the remaining lights
		if (i >= found_lights.size()) {
			state_manager->SetSTLight(d3dLightIndex, D3DVECTOR{ 0, 0, 0 }, 0, 0, 0, 0);
			device->LightEnable(d3dLightIndex, FALSE);
			continue;
		}

		// reset light structure
		ZeroMemory(&lightImStruct, sizeof(D3DLIGHT9));

		CRTLightObj* light = found_lights[i];

		lightImStruct.Type = D3DLIGHTTYPE(light->m_type);
		lightImStruct.Theta = light->m_theta;
		lightImStruct.Phi = light->m_phi;
		lightImStruct.Falloff = light->m_falloff;
		lightImStruct.Direction = reinterpret_cast<D3DVECTOR&>(light->m_direction);

		//DPD2
		//Interpolate color so we get a fade effect
		if (light->m_fadeOn) {
			if (light->m_range >= light->m_distFromChar) {
				lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
				lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
				lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
			} else {
				lightImStruct.Diffuse.r = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.r, lightImStruct.Diffuse.r, light->m_range, -light->m_range + light->m_distFromChar);
				lightImStruct.Diffuse.g = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.g, lightImStruct.Diffuse.g, light->m_range, -light->m_range + light->m_distFromChar);
				lightImStruct.Diffuse.b = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.b, lightImStruct.Diffuse.b, light->m_range, -light->m_range + light->m_distFromChar);
			}
		} else {
			//set diffuse
			lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
			lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
			lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
			lightImStruct.Diffuse.a = light->m_material->m_useMaterial.Diffuse.a;
		}

		if (light->m_randomOn) { //random intensity
			float blinkd;
			int range;
			range = light->m_intMax - light->m_intMin;

			// illamas: I'm guessing the min and max are supposed to be in between 1 and 100
			if (range)
				blinkd = (float)(light->m_intMin + (rand() % range)) / 100.0f;
			else
				blinkd = 1.0f;

			lightImStruct.Diffuse.r *= blinkd;
			lightImStruct.Diffuse.g *= blinkd;
			lightImStruct.Diffuse.b *= blinkd;
			lightImStruct.Diffuse.a *= blinkd;
		}

		//set ambient
		lightImStruct.Ambient.r = light->m_material->m_useMaterial.Ambient.r;
		lightImStruct.Ambient.g = light->m_material->m_useMaterial.Ambient.g;
		lightImStruct.Ambient.b = light->m_material->m_useMaterial.Ambient.b;
		lightImStruct.Ambient.a = light->m_material->m_useMaterial.Ambient.a;

		//set specular
		lightImStruct.Specular.r = light->m_material->m_useMaterial.Specular.r;
		lightImStruct.Specular.g = light->m_material->m_useMaterial.Specular.g;
		lightImStruct.Specular.b = light->m_material->m_useMaterial.Specular.b;
		lightImStruct.Specular.a = light->m_material->m_useMaterial.Specular.a;

		//set range
		lightImStruct.Range = light->m_range;

		//set position
		lightImStruct.Position.x = light->m_wldspacePos.x;
		lightImStruct.Position.y = light->m_wldspacePos.y;
		lightImStruct.Position.z = light->m_wldspacePos.z;

		//set attenuation
		lightImStruct.Attenuation0 = light->m_attenuation0;
		lightImStruct.Attenuation1 = light->m_attenuation1;
		lightImStruct.Attenuation2 = light->m_attenuation2;

		device->SetLight(d3dLightIndex, &lightImStruct);
		device->LightEnable(d3dLightIndex, TRUE);

		state_manager->SetSTLight(
			d3dLightIndex,
			lightImStruct.Position,
			light->m_material->m_useMaterial.Diffuse.r,
			light->m_material->m_useMaterial.Diffuse.g,
			light->m_material->m_useMaterial.Diffuse.b,
			light->m_range);
	}
}

// Called per-frame after level has loaded lights (in "GenRenderList_WorldObjects").
void CRTLightObjList::GetLightsRenderEngine(ReD3DX9DeviceState* devState) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	auto pSMO = pICE->GetRenderStateManager();
	if (!pSMO)
		return;

	devState->GetRenderState()->FlushLightCache();
	const D3DLIGHT9* sunLight = pSMO->GetSunLight();
	devState->GetRenderState()->LoadLight(sunLight);

	const D3DVECTOR* globalAmb = pSMO->GetAmbLight();
	D3DXVECTOR4 amb(globalAmb->x, globalAmb->y, globalAmb->z, 0.f);
	devState->GetRenderState()->SetGlobalAmbient(&amb);

	const D3DVECTOR* fogColor = pSMO->GetFogColor();
	D3DXVECTOR4 fog(fogColor->x, fogColor->y, fogColor->z, 0.f);
	devState->GetRenderState()->SetFogColor(&fog);

	const D3DXVECTOR2* fogRange = pSMO->GetFogRange();
	D3DXVECTOR4 range(fogRange->x, fogRange->y, 0.f, 0.f);
	devState->GetRenderState()->SetFogRange(&range);

	// go thru the light list
	for (auto light : m_apLights) {
		// reject point lights with no range
		if (D3DLIGHTTYPE(light->m_type) == D3DLIGHT_POINT && light->m_range <= 0.f)
			continue;

		// reset light structure
		D3DLIGHT9 lightImStruct;
		ZeroMemory(&lightImStruct, sizeof(D3DLIGHT9));

		lightImStruct.Type = D3DLIGHTTYPE(light->m_type);
		lightImStruct.Theta = light->m_theta;
		lightImStruct.Phi = light->m_phi;
		lightImStruct.Falloff = light->m_falloff;
		lightImStruct.Direction = reinterpret_cast<D3DVECTOR&>(light->m_direction);

		//DPD2
		//Interpolate color so we get a fade effect
		if (light->m_fadeOn) {
			if (light->m_range >= light->m_distFromChar) {
				lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
				lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
				lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
			} else {
				lightImStruct.Diffuse.r = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.r, lightImStruct.Diffuse.r, light->m_range, -light->m_range + light->m_distFromChar);
				lightImStruct.Diffuse.g = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.g, lightImStruct.Diffuse.g, light->m_range, -light->m_range + light->m_distFromChar);
				lightImStruct.Diffuse.b = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.b, lightImStruct.Diffuse.b, light->m_range, -light->m_range + light->m_distFromChar);
			}
		} else {
			//set diffuse
			lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
			lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
			lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
			lightImStruct.Diffuse.a = light->m_material->m_useMaterial.Diffuse.a;
		}

		if (light->m_randomOn) { //random intensity
			float blinkd;
			int intensityRange;
			intensityRange = light->m_intMax - light->m_intMin;

			// illamas: I'm guessing the min and max are supposed to be in between 1 and 100
			if (intensityRange)
				blinkd = (float)(light->m_intMin + (rand() % intensityRange)) / 100.0f;
			else
				blinkd = 1.0f;

			lightImStruct.Diffuse.r *= blinkd;
			lightImStruct.Diffuse.g *= blinkd;
			lightImStruct.Diffuse.b *= blinkd;
			lightImStruct.Diffuse.a *= blinkd;
		}

		//set ambient
		lightImStruct.Ambient.r = light->m_material->m_useMaterial.Ambient.r;
		lightImStruct.Ambient.g = light->m_material->m_useMaterial.Ambient.g;
		lightImStruct.Ambient.b = light->m_material->m_useMaterial.Ambient.b;
		lightImStruct.Ambient.a = light->m_material->m_useMaterial.Ambient.a;

		//set specular
		lightImStruct.Specular.r = light->m_material->m_useMaterial.Specular.r;
		lightImStruct.Specular.g = light->m_material->m_useMaterial.Specular.g;
		lightImStruct.Specular.b = light->m_material->m_useMaterial.Specular.b;
		lightImStruct.Specular.a = light->m_material->m_useMaterial.Specular.a;

		// set attenuation factors
		lightImStruct.Attenuation0 = light->m_attenuation0;
		lightImStruct.Attenuation1 = light->m_attenuation1;
		lightImStruct.Attenuation2 = light->m_attenuation2;

		//set range
		lightImStruct.Range = light->m_range;

		//set position
		lightImStruct.Position.x = light->m_wldspacePos.x;
		lightImStruct.Position.y = light->m_wldspacePos.y;
		lightImStruct.Position.z = light->m_wldspacePos.z;

		//don't load the sunlite twice
		if (memcmp((void*)&lightImStruct, (void*)sunLight, sizeof(D3DLIGHT9)) != 0) {
			devState->GetRenderState()->LoadLight(&lightImStruct);
		}
	}
#else
	ASSERT(FALSE);
#endif
}

void CRTLightObj::LoadLight(ReD3DX9DeviceState* devState) {
#ifdef _ClientOutput
	CRTLightObj* light = this;

	// reset light structure
	D3DLIGHT9 lightImStruct;
	ZeroMemory(&lightImStruct, sizeof(D3DLIGHT9));

	lightImStruct.Type = D3DLIGHTTYPE(light->m_type);
	lightImStruct.Theta = light->m_theta;
	lightImStruct.Phi = light->m_phi;
	lightImStruct.Falloff = light->m_falloff;
	lightImStruct.Direction = reinterpret_cast<D3DVECTOR&>(light->m_direction);

	//DPD2
	//Interpolate color so we get a fade effect
	if (light->m_fadeOn) {
		if (light->m_range >= light->m_distFromChar) {
			lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
			lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
			lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
		} else {
			float fInterpFactor = light->m_range == 0 ? 0 : (-light->m_range + light->m_distFromChar) / light->m_range;
			lightImStruct.Diffuse.r = InterpolateLinear(light->m_material->m_useMaterial.Diffuse.r, lightImStruct.Diffuse.r, fInterpFactor);
			lightImStruct.Diffuse.g = InterpolateLinear(light->m_material->m_useMaterial.Diffuse.g, lightImStruct.Diffuse.g, fInterpFactor);
			lightImStruct.Diffuse.b = InterpolateLinear(light->m_material->m_useMaterial.Diffuse.b, lightImStruct.Diffuse.b, fInterpFactor);
		}
	} else {
		//set diffuse
		lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
		lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
		lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
		lightImStruct.Diffuse.a = light->m_material->m_useMaterial.Diffuse.a;
	}

	if (light->m_randomOn) { //random intensity
		float blinkd;
		int range;
		range = light->m_intMax - light->m_intMin;

		// illamas: I'm guessing the min and max are supposed to be in between 1 and 100
		if (range)
			blinkd = (float)(light->m_intMin + (rand() % range)) / 100.0f;
		else
			blinkd = 1.0f;

		lightImStruct.Diffuse.r *= blinkd;
		lightImStruct.Diffuse.g *= blinkd;
		lightImStruct.Diffuse.b *= blinkd;
		lightImStruct.Diffuse.a *= blinkd;
	}

	//set ambient
	lightImStruct.Ambient.r = light->m_material->m_useMaterial.Ambient.r;
	lightImStruct.Ambient.g = light->m_material->m_useMaterial.Ambient.g;
	lightImStruct.Ambient.b = light->m_material->m_useMaterial.Ambient.b;
	lightImStruct.Ambient.a = light->m_material->m_useMaterial.Ambient.a;

	//set specular
	lightImStruct.Specular.r = light->m_material->m_useMaterial.Specular.r;
	lightImStruct.Specular.g = light->m_material->m_useMaterial.Specular.g;
	lightImStruct.Specular.b = light->m_material->m_useMaterial.Specular.b;
	lightImStruct.Specular.a = light->m_material->m_useMaterial.Specular.a;

	// set attenuation factors
	lightImStruct.Attenuation0 = light->m_attenuation0;
	lightImStruct.Attenuation1 = light->m_attenuation1;
	lightImStruct.Attenuation2 = light->m_attenuation2;

	//set range
	lightImStruct.Range = light->m_range;

	//set position
	lightImStruct.Position.x = light->m_wldspacePos.x;
	lightImStruct.Position.y = light->m_wldspacePos.y;
	lightImStruct.Position.z = light->m_wldspacePos.z;

	// load the light
	devState->GetRenderState()->LoadLight(&lightImStruct);
#else
	ASSERT(FALSE);
#endif
}

void CRTLightObjList::EnableAll(LPDIRECT3DDEVICE9 g_pd3dDevice,
	CStateManagementObj* stateManager) {
	UINT index = 1;
	D3DLIGHT9 lightImStruct;
	ZeroMemory(&lightImStruct, sizeof(D3DLIGHT9));

	for (CRTLightObj* light : m_apLights) {
		switch (light->m_type) {
			case D3DLIGHT_POINT:
				lightImStruct.Type = D3DLIGHT_POINT;
				break;

			case D3DLIGHT_SPOT:
				lightImStruct.Type = D3DLIGHT_SPOT;
				lightImStruct.Theta = light->m_theta;
				lightImStruct.Phi = light->m_phi;
				lightImStruct.Falloff = light->m_falloff;
				lightImStruct.Direction = reinterpret_cast<D3DVECTOR&>(light->m_direction);
				break;
		}

		//DPD2
		//Interpolate color so we get a fade effect
		if (light->m_fadeOn) {
			if (light->m_range >= light->m_distFromChar) {
				lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
				lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
				lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
			} else {
				lightImStruct.Diffuse.r = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.r, lightImStruct.Diffuse.r, light->m_range, -light->m_range + light->m_distFromChar);
				lightImStruct.Diffuse.g = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.g, lightImStruct.Diffuse.g, light->m_range, -light->m_range + light->m_distFromChar);
				lightImStruct.Diffuse.b = MatrixARB::InterpolateFloatsHiRes(light->m_material->m_useMaterial.Diffuse.b, lightImStruct.Diffuse.b, light->m_range, -light->m_range + light->m_distFromChar);
			}
		} else {
			//set diffuse
			lightImStruct.Diffuse.r = light->m_material->m_useMaterial.Diffuse.r;
			lightImStruct.Diffuse.g = light->m_material->m_useMaterial.Diffuse.g;
			lightImStruct.Diffuse.b = light->m_material->m_useMaterial.Diffuse.b;
			lightImStruct.Diffuse.a = light->m_material->m_useMaterial.Diffuse.a;
		}

		if (light->m_randomOn) { //random intensity
			float blinkd;
			int range;
			range = light->m_intMax - light->m_intMin;

			if (range)
				blinkd = (float)(light->m_intMin + (rand() % range)) / 100.0f;
			else
				blinkd = 1.0f;

			lightImStruct.Diffuse.r *= blinkd;
			lightImStruct.Diffuse.g *= blinkd;
			lightImStruct.Diffuse.b *= blinkd;
			lightImStruct.Diffuse.a *= blinkd;
		}

		//set ambient
		lightImStruct.Ambient.r = light->m_material->m_useMaterial.Ambient.r;
		lightImStruct.Ambient.g = light->m_material->m_useMaterial.Ambient.g;
		lightImStruct.Ambient.b = light->m_material->m_useMaterial.Ambient.b;
		lightImStruct.Ambient.a = light->m_material->m_useMaterial.Ambient.a;

		//set specular
		lightImStruct.Specular.r = light->m_material->m_useMaterial.Specular.r;
		lightImStruct.Specular.g = light->m_material->m_useMaterial.Specular.g;
		lightImStruct.Specular.b = light->m_material->m_useMaterial.Specular.b;
		lightImStruct.Specular.a = light->m_material->m_useMaterial.Specular.a;

		//set range
		lightImStruct.Range = light->m_range;

		//set position
		lightImStruct.Position.x = light->m_wldspacePos.x;
		lightImStruct.Position.y = light->m_wldspacePos.y;
		lightImStruct.Position.z = light->m_wldspacePos.z;

		//set attenuation
		lightImStruct.Attenuation0 = light->m_attenuation0;
		lightImStruct.Attenuation1 = light->m_attenuation1;
		lightImStruct.Attenuation2 = light->m_attenuation2;

		g_pd3dDevice->SetLight(index, &lightImStruct);
		g_pd3dDevice->LightEnable(index, TRUE);

		stateManager->SetSTLight(
			index,
			lightImStruct.Position,
			light->m_material->m_useMaterial.Diffuse.r,
			light->m_material->m_useMaterial.Diffuse.g,
			light->m_material->m_useMaterial.Diffuse.b,
			light->m_range);

		index++;
	}
}

void CRTLightObj::Serialize(CArchive& ar) {
#ifdef _ClientOutput
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
#endif

	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		ar.SetObjectSchema(CArchiveObjectSchemaVersion<CRTLightObj>);

		if (m_material) {
			bool storingMaterial;
			if (m_material->m_myCompressionIndex != CMATERIAL_UNCOMPRESSED) {
				storingMaterial = false;
				ar << storingMaterial;
				ar << m_material->m_myCompressionIndex;
			} else {
				storingMaterial = true;
				ar << storingMaterial;
				ar << m_material.get();
			}
		} else {
			bool storingMaterial = true;
			ar << storingMaterial;
			ar << m_material.get();
		}

		ar << m_range
		   << m_attenuation0
		   << m_attenuation1
		   << m_attenuation2;

		ar << m_wldspacePos.x
		   << m_wldspacePos.y
		   << m_wldspacePos.z;

		ar << m_type
		   << m_direction.x
		   << m_direction.y
		   << m_direction.z
		   << m_theta
		   << m_phi
		   << m_falloff;

		ar << m_fadeOn;

		ar << m_blinkOn
		   << m_blinkDur
		   << m_randomOn;

		ar << m_render;
	} else {
		// despite the fact that we moved to d3dxvector3 instead of d3dvector, this
		//  code will work fine since it loads in each component seperately
		int version = ar.GetObjectSchema();

		if (version >= 9) {
			bool storingMaterial;
			ar >> storingMaterial;
			if (storingMaterial) {
				CMaterialObject* pMaterial = nullptr;
				ar >> pMaterial;
				m_material.reset(pMaterial);
			} else {
				int compressionIndex;
				ar >> compressionIndex;
				MaterialCompressor* mc = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
				if (mc)
					m_material.reset(mc->getMaterial(compressionIndex));
			}
		} else {
			CMaterialObject* pMaterial = nullptr;
			ar >> pMaterial;
			m_material.reset(pMaterial);
		}

		ar >> m_range >> m_attenuation0 >> m_attenuation1 >> m_attenuation2;

		m_wldspacePos.Set(0, 0, 0);

		if (version > 1) {
			ar >> m_wldspacePos.x >> m_wldspacePos.y >> m_wldspacePos.z;
		}

		//DPD for spotlight
		if (version > 2) {
			ar >> m_type >> m_direction.x >> m_direction.y >> m_direction.z >> m_theta >> m_phi >> m_falloff;
		}

		if (version >= 6) { // version 7 for ReMesh
			ar >> m_fadeOn;
			ar >> m_blinkOn >> m_blinkDur >> m_randomOn;

#ifdef _ClientOutput
			if (m_material) {
				// Get the ReMaterial
				ReD3DX9DeviceState* dev = pICE->GetReDeviceState();
				// get the ReRenderState dataof this material
				ReRenderStateData data;
				m_material->GetReRenderState(&data);
				ReMaterialPtr reMat(new ReD3DX9Material(dev, &data));
				// set the new material
				m_material->SetReMaterial(reMat);
			}
#else
			// server gets here if explosion has light, just ignore now ASSERT( FALSE );
#endif
			if (version > 7) {
				ar >> m_render;
			} // if..version > 7
		} else {
			// changed BOOL values to bool
			//  also changed Vector3f to Vector3f

			//DPD2 Fading
			if (version > 3) {
				BOOL fade_on;
				ar >> fade_on;

				m_fadeOn = (fade_on == TRUE);
			}

			//DPD4 Blinking
			if (version > 4) {
				BOOL blink_on;
				ar >> blink_on;

				m_blinkOn = (blink_on == TRUE);

				ar >> m_blinkDur;

				BOOL random_on;
				ar >> random_on;

				m_randomOn = (random_on == TRUE);
			}
		}
	}
}

void CRTLightObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_material);
	}
}

IMPLEMENT_SERIAL_SCHEMA(CRTLightObj, CObject)

} // namespace KEP