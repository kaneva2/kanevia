///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "TextureDatabase.h"

#include "ProgressStatus.h"
#include "RenderEngine/Utils/ReUtils.h"
#include "TextureManager.h"
#include "IClientEngine.h"
#include "GenUtils.h"
#include "TextureManager.h"
#include "CompressHelper.h"
#include "LogHelper.h"

static LogInstance("Instance");

namespace KEP {

TextureDatabase::TextureDatabase() :
		m_nextDynamicTextureId(INVALID_DYNAMIC_TEXTURE_ID + 1) {
	ZeroMemory(&m_d3d_caps, sizeof(D3DCAPS9));
}

TextureDatabase::~TextureDatabase() {
	TextureMapUnloadAll();
	TextureMapDelAll();
}

void TextureDatabase::Initialize() {
	G_DEVICE();
	gDevice->GetDeviceCaps(&m_d3d_caps);
}

DWORD TextureDatabase::TextureNameHash(CTextureEX* pTex) {
	if (!pTex)
		return INVALID_TEXTURE_NAMEHASH;
	return TextureNameHash(pTex->GetFileName(), pTex->GetColorKey());
}

DWORD TextureDatabase::TextureNameHash(const std::string& texName, const D3DCOLOR& colorKey) {
	unsigned char r = (unsigned char)((colorKey >> 16) & 0xff);
	unsigned char g = (unsigned char)((colorKey >> 8) & 0xff);
	unsigned char b = (unsigned char)((colorKey >> 0) & 0xff);
	unsigned char a = (unsigned char)((colorKey >> 24) & 0xff);
	return TextureNameHash(texName, r, g, b, a);
}

DWORD TextureDatabase::TextureNameHash(const std::string& texName, unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	// Hash Short Name
	std::string shortName = StrStripFilePath(texName);
	std::vector<BYTE> hashBytes;
	hashBytes.assign(reinterpret_cast<BYTE*>(const_cast<char*>(shortName.c_str())), reinterpret_cast<BYTE*>(const_cast<char*>(shortName.c_str())) + shortName.size());
	hashBytes.push_back(r);
	hashBytes.push_back(g);
	hashBytes.push_back(b);
	hashBytes.push_back(a);
	DWORD nameHash = ReHash(&(hashBytes[0]), hashBytes.size(), 0);
	if (!ValidTextureNameHash(nameHash)) {
		LogError("ReHash() FAILED - '" << texName << "'");
	}
	return nameHash;
}

// DRF - ED-7035 - Investigate texture memory impact
void TextureDatabase::TextureMapLog() {
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);
	size_t texLoaded = 0;
	size_t texLoadedBytes = 0;
	size_t texErrored = 0;
	struct memStat {
		size_t num;
		size_t bytes;
		memStat() :
				num(0), bytes(0) {}
		memStat(size_t num, size_t bytes) :
				num(num), bytes(bytes) {}
	};
	std::map<std::string, memStat> memMap;
	size_t texMapSize = m_texMap.size();
	size_t texMapBytes = texMapSize * sizeof(HashedTexPair);
	LogInfo("texMap.size=" << texMapSize << " sizeof(texMap)=" << texMapBytes << " sizeof(CTextureEX)=" << texMapSize * sizeof(CTextureEX));
	for (const auto& it : m_texMap) {
		CTextureEX* pTex = it.second;
		if (!pTex)
			continue;
		if (pTex->StateIsLoadedAndCreated()) {
			auto texBytes = pTex->GetTextureMemoryUsage();
			texLoaded++;
			texLoadedBytes += texBytes;
			auto texCat = pTex->GetTextureCategory();
			auto itr = memMap.find(texCat);
			if (itr != memMap.end()) {
				itr->second.num++;
				itr->second.bytes += texBytes;
			} else {
				memMap[texCat] = memStat(1, texBytes);
			}
			_LogInfo(" ... " << pTex->ToStr());
		} else if (pTex->IsErrored()) {
			texErrored++;
			_LogWarn(" ... " << pTex->ToStr());
		}
	}
	LogInfo("texLoaded=" << texLoaded
						 << " texErrored=" << texErrored
						 << FMT_FLT << " (" << (texLoadedBytes / BYTES_PER_MB) << "mb)");
	for (const auto& it : memMap) {
		_LogInfo(" ... '" << it.first << "' texLoaded=" << it.second.num << FMT_FLT << " (" << (it.second.bytes / BYTES_PER_MB) << "mb)");
	}
}

// ED-8437 - Clear Errored Resources On Rezone
bool TextureDatabase::TextureMapClearErrored() {
	for (const auto& it : m_texMap) {
		CTextureEX* pTex = it.second;
		if (!pTex || !pTex->IsErrored())
			continue;
		pTex->ClearErrored();
	}
	return true;
}

bool TextureDatabase::TextureMapUnloadAll() {
	// Unload texture to save memory.  Will be re-loaded on call to CreateResource()
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);
	for (const auto& it : m_texMap) {
		CTextureEX* pTex = it.second;
		if (!pTex)
			continue;
		pTex->UnloadTexture();
	}

	// ED-7732 - Texture Scaling
	// Clear Texture memory Usage (should already be 0 otherwise investigate a texture memory leak)
	//	auto texUsage = CTextureEX::GetAllTextureMemoryUsage();
	//	if (texUsage != 0) {
	//		LogError("TEXTURE MEMORY LEAK - texUsage=" << texUsage << "bytes");
	//	}
	CTextureEX::ClearAllTextureMemoryUsage();

	return true;
}

bool TextureDatabase::TextureMapDelAll() {
	// Remove All Textures
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);
	m_texMap.clear();
	return true;
}

bool TextureDatabase::TextureMapDel(CTextureEX* pTex) {
	if (!pTex)
		return false;

	// Get Texture Name Hash
	auto nameHash = pTex->GetNameHash();
	if (!nameHash)
		return false;

	// Remove Texture From Texture Map
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);
	m_texMap.erase(nameHash);
	return true;
}

bool TextureDatabase::TextureMapAdd(const HashedTexPair& texPair) {
	if (!texPair.second)
		return false;

	// Insert Texture Into Texture Map
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);
	m_texMap.insert(texPair);
	return true;
}

CTextureEX* TextureDatabase::TextureMapGet(const DWORD& nameHash) {
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);
	auto i = m_texMap.find(nameHash);
	return (i == m_texMap.end()) ? NULL : (i->second);
}

CTextureEX* TextureDatabase::TextureMapGet(const std::string& texFileName) {
	DWORD nameHash = ReHash((BYTE*)texFileName.c_str(), texFileName.length(), 0);
	return TextureMapGet(nameHash);
}

DWORD TextureDatabase::TextureMapGetNameHash(const std::string& texFileName) {
	DWORD nameHash = ReHash((BYTE*)texFileName.c_str(), texFileName.length(), 0);
	return TextureMapContains(nameHash) ? nameHash : INVALID_TEXTURE_NAMEHASH;
}

bool TextureDatabase::TextureMapContains(const DWORD& nameHash) {
	return (TextureMapGet(nameHash) != NULL);
}

const CTextureEX* TextureDatabase::TextureGet(const std::string& texName) {
	DWORD texHash = TextureNameHash(texName, 0, 0, 0, 0);
	return TextureGet(texHash);
}

CTextureEX* TextureDatabase::TextureGet(DWORD& nameHash, const std::string& texName, const std::string& subDir, D3DCOLOR colorKey) {
	// Valid Name Hash ?
	if (!ValidTextureNameHash(nameHash)) {
		LogError("INVALID TEXTURE NAMEHASH");
		return NULL;
	}

	// DRF - Bug Fix - Must Lock Here Otherwise We May End Up Adding Twice
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);

	// Is Texture Already In Database ?
	CTextureEX* pTex = TextureMapGet(nameHash);
	if (pTex)
		return pTex;

	// Do nothing if texture name is not provided (query by hash only)
	if (texName.empty()) {
		return NULL;
	}

	// Texture Not Found - Add It To Database By Name
	TextureAdd(nameHash, texName, subDir, "", colorKey);
	if (!ValidTextureNameHash(nameHash)) {
		if (!texName.empty())
			LogError("AddTextureToDatabase() FAILED - '" << texName << "'");
		return NULL;
	}

	// Texture Should Be In Database Now
	pTex = TextureMapGet(nameHash);
	return pTex;
}

DWORD TextureDatabase::TextureAdd(DWORD& nameHash, const std::string& texName, const std::string& subDir, const std::string& url, D3DCOLOR colorKey, eTexturePath texPath) {
	// Reset Name Hash
	nameHash = INVALID_TEXTURE_NAMEHASH;

	// Strip Compression Extension
	std::string localFileName = texName;
#ifdef _ClientOutput
	localFileName = ::CompressTypeExtDel(localFileName);
#endif

	// Get Short Name
	std::string shortName;
	std::string shortSubDir("");
	GetShortNames(&localFileName, subDir.c_str(), &shortName, &shortSubDir);

	// Valid Texture Name ?
	if (!ValidTextureFileName(shortName)) {
		if (!shortName.empty())
			LogError("INVALID TEXTURE NAME - '" << shortName << "'");
		return INVALID_TEXTURE_NAMEHASH;
	}

	// Calculate Texture Name Hash
	nameHash = TextureNameHash(shortName, colorKey);
	if (!ValidTextureNameHash(nameHash)) {
		LogError("INVALID TEXTURE NAMEHASH - '" << shortName << "'");
		return INVALID_TEXTURE_NAMEHASH;
	}

	// DRF - Bug Fix - Must Lock Here Otherwise We May End Up Adding Twice
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);

	// Is Texture Already In Database ?
	bool isTextureInDB = TextureMapContains(nameHash);
	if (isTextureInDB)
		return nameHash;

	// Not In Database - Load Texture From Texture Paths
	// NOTE: For now we use texture search paths to find the texture, though it's usually under gamefiles directory
	std::string pathOverride;
	std::string tmpSubDir;
	if (!subDir.empty()) {
		tmpSubDir = subDir;
		if (STLContains(tmpSubDir, "CustomTemplate")) {
			tmpSubDir = tmpSubDir + "\\";
			pathOverride = tmpSubDir;
		}
	}

	// Create New Texture
	CTextureEX* pTex = new CTextureEX(shortName, shortSubDir, texPath, pathOverride, url, colorKey);

	// Encrypted ?
	bool isKRX = StrSameFileExt(shortName, "krx");
	pTex->SetKRXEncrypted(isKRX);
	pTex->SetNameHash(nameHash);
	pTex->SetHashKey(nameHash);
	pTex->SetState(IResource::State::INITIALIZED_NOT_LOADED);
	pTex->SetError(IResource::Error::ERROR_NONE);

	// Add To Texture Map
	TextureMapAdd(HashedTexPair(nameHash, pTex));

	return nameHash;
}

// Only called by CWldObject::UpdateCustomMaterials()
DWORD TextureDatabase::CustomTextureAdd(DWORD& nameHash, const std::string& texName, const std::string& subDir, D3DCOLOR colorKey, eTexturePath texPath) {
	// Reset Name Hash
	nameHash = INVALID_TEXTURE_NAMEHASH;

	// Get Short Name (without path)
	std::string shortName = StrStripFilePath(texName);
	std::string shortSubDir;
	if (!subDir.empty()) {
		shortSubDir = subDir;
		shortSubDir.append("\\");
	}

	// Valid Texture Name ?
	if (!ValidTextureFileName(shortName)) {
		LogError("INVALID TEXTURE NAME - '" << shortName << "'");
		return INVALID_TEXTURE_NAMEHASH;
	}

	// Calculate Texture Name Hash
	nameHash = TextureNameHash(shortName, colorKey);
	if (!ValidTextureNameHash(nameHash)) {
		LogError("INVALID TEXTURE NAMEHASH - '" << shortName << "'");
		return INVALID_TEXTURE_NAMEHASH;
	}

	// DRF - Bug Fix - Must Lock Here Otherwise We May End Up Adding Twice
	std::lock_guard<fast_recursive_mutex> lock(m_texMutex);

	// Is Texture Already In Database ?
	bool isTextureInDB = TextureMapContains(nameHash);
	if (isTextureInDB)
		return nameHash;

	// Not In Database - Load Texture From Texture Paths
	// NOTE: For now we use texture search paths to find the texture, though it's usually under gamefiles directory
	CTextureEX* pTex = new CTextureEX(shortName, shortSubDir, texPath, "", "", colorKey);
	pTex->SetKRXEncrypted(false); // Custom textures are always unencrypted
	pTex->SetNameHash(nameHash);
	pTex->SetHashKey(nameHash);
	pTex->SetState(IResource::State::INITIALIZED_NOT_LOADED);
	pTex->SetError(IResource::Error::ERROR_NONE);

	// Add To Texture Map
	TextureMapAdd(HashedTexPair(nameHash, pTex));

	return nameHash;
}

class DynamicTexture {
public:
	DynamicTexture(unsigned width, unsigned height) :
			m_pd3dTexture(nullptr),
			m_width(width),
			m_height(height),
			m_errorLogged(false) {}

	virtual ~DynamicTexture() {
		destroyTexture();
	}

	static unsigned getPrevPowerOf2(unsigned num) {
		// Edge case
		if (num == 0) {
			return 0;
		}
		// Mark all lower (than the highest 1) bits to 1
		num = num | (num >> 1);
		num = num | (num >> 2);
		num = num | (num >> 4);
		num = num | (num >> 8);
		num = num | (num >> 16);
		return (num >> 1) + 1; // getNextPowerOf2: return num + 1
	}

	IDirect3DTexture9* getD3DTexture() {
		G_DEVICE(nullptr);
		if (m_pd3dTexture == nullptr) {
			HRESULT hr = gDevice->CreateTexture(m_width, m_height, 0, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &m_pd3dTexture, nullptr);
			if (hr == D3DERR_INVALIDCALL) {
				// Try X8R8G8B8
				LogDebug("Fallback with D3DFMT_X8R8G8B8");
				hr = gDevice->CreateTexture(m_width, m_height, 0, 0, D3DFMT_X8R8G8B8, D3DPOOL_MANAGED, &m_pd3dTexture, nullptr);
				if (hr == D3DERR_INVALIDCALL) {
					// Try power of two
					LogDebug("Fallback with POT");
					auto w = getPrevPowerOf2(m_width);
					auto h = getPrevPowerOf2(m_height);
					hr = gDevice->CreateTexture(w, h, 0, 0, D3DFMT_X8R8G8B8, D3DPOOL_MANAGED, &m_pd3dTexture, nullptr);
				}
			}
			if (hr != D3D_OK && !m_errorLogged) {
				LogWarn("Error creating dynamic texture: width = " << m_width << ", height = " << m_height << ", hr = " << std::hex << std::setw(8) << std::setfill('0') << hr);
				m_errorLogged = true; // Suppress additional error messages
			}
		}
		return m_pd3dTexture;
	}

	void destroyTexture() {
		if (m_pd3dTexture != nullptr) {
			m_pd3dTexture->Release();
			m_pd3dTexture = nullptr;
		}
	}

private:
	IDirect3DTexture9* m_pd3dTexture;
	unsigned m_width, m_height;
	bool m_errorLogged;
};

unsigned TextureDatabase::DynamicTextureAdd(unsigned width, unsigned height) {
	auto pDynTexture = new DynamicTexture(width, height);

	std::lock_guard<std::mutex> lock(m_dynamicTextureMutex);
	unsigned dynTextureId = m_nextDynamicTextureId++;
	m_dynamicTextures.insert(std::make_pair(dynTextureId, pDynTexture));
	return dynTextureId;
}

void TextureDatabase::DynamicTextureDestroy(unsigned dynTextureId) {
	std::lock_guard<std::mutex> lock(m_dynamicTextureMutex);
	auto itr = m_dynamicTextures.find(dynTextureId);
	if (itr != m_dynamicTextures.end()) {
		auto pDynTexture = itr->second;
		delete pDynTexture;
		m_dynamicTextures.erase(itr);
	}
}

void TextureDatabase::DynamicTextureDestroyAll() {
	std::lock_guard<std::mutex> lock(m_dynamicTextureMutex);
	for (const auto& pr : m_dynamicTextures)
		delete pr.second;
	m_dynamicTextures.clear();
}

IDirect3DTexture9* TextureDatabase::DynamicTextureGetHandle(unsigned dynTextureId) {
	if (dynTextureId == INVALID_DYNAMIC_TEXTURE_ID)
		return nullptr;

	std::lock_guard<std::mutex> lock(m_dynamicTextureMutex);
	auto itr = m_dynamicTextures.find(dynTextureId);
	if (itr != m_dynamicTextures.end()) {
		assert(itr->second != nullptr);
		return itr->second->getD3DTexture();
	}

	return nullptr;
}

} // namespace KEP
