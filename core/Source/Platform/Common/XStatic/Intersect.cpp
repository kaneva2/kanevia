///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Intersect.h"

namespace KEP {

/*
    Fast Ray-Box Intersection
	by Andrew Woo
	from "Graphics Gems", Academic Press, 1990
	http://www.acm.org/tog/GraphicsGems/gems/RayBox.c

	char HitBoundingBox(minB,maxB, origin, dir,coord)
	float minB[3], maxB[3];		//box
	float origin[3], dir[3];	//ray
	float coord[3];				// hit point
*/
bool Intersect::RayAABBCheck(
	const Vector3f& minB,
	const Vector3f& maxB,
	const Vector3f& origin,
	const Vector3f& dir,
	Vector3f& coord,
	Vector3f& normal,
	BOOL inIsOut) {
	const int RIGHT = 0;
	const int LEFT = 1;
	const int MIDDLE = 2;
	char inside = TRUE;
	char quadrant[3];
	register int i;
	int whichPlane;
	float maxT[3];
	float candidatePlane[3];

	/* Find candidate planes; this loop can be avoided if
	rays cast all from the eye(assume perpsective view) */
	for (i = 0; i < 3; i++)
		if (origin[i] < minB[i]) {
			quadrant[i] = LEFT;
			candidatePlane[i] = minB[i];
			inside = FALSE;
		} else if (origin[i] > maxB[i]) {
			quadrant[i] = RIGHT;
			candidatePlane[i] = maxB[i];
			inside = FALSE;
		} else {
			quadrant[i] = MIDDLE;
		}

	/* Ray origin inside bounding box */
	if (inside) {
		if (inIsOut) {
			return FALSE;
		}
		coord = origin;
		return TRUE;
	}

	/* Calculate T distances to candidate planes */
	for (i = 0; i < 3; i++)
		if (quadrant[i] != MIDDLE && dir[i] != 0.)
			maxT[i] = (candidatePlane[i] - origin[i]) / dir[i];
		else
			maxT[i] = -1.;

	/* Get largest of the maxT's for final choice of intersection */
	whichPlane = 0;
	for (i = 1; i < 3; i++)
		if (maxT[whichPlane] < maxT[i])
			whichPlane = i;

	/* Check final candidate actually inside box */
	if (maxT[whichPlane] < 0.)
		return (FALSE);
	for (i = 0; i < 3; i++)
		if (whichPlane != i) {
			coord[i] = origin[i] + maxT[whichPlane] * dir[i];
			if (coord[i] < minB[i] || coord[i] > maxB[i])
				return (FALSE);
		} else {
			coord[i] = candidatePlane[i];
		}

	/* get hit normal */
	normal.x = normal.y = normal.z = 0.f;

	if (coord[0] == minB.x) {
		normal.x = -1.f;
	} else if (coord[0] == maxB.x) {
		normal.x = 1.f;
	}

	if (coord[1] == minB.y) {
		normal.y = -1.f;
	} else if (coord[1] == maxB.y) {
		normal.y = 1.f;
	}

	if (coord[2] == minB.z) {
		normal.z = -1.f;
	} else if (coord[2] == maxB.z) {
		normal.z = 1.f;
	}

	return true; /* ray hits box */
}

} // namespace KEP