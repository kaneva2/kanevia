///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CServerItemClass.h"
#include "CInventoryClass.h"
#include "CAIICollectionQuest.h"
#include "Tools/json11/json11.hpp"
#include "matrixarb.h"

static LogInstance("Instance");

namespace KEP {

CServerItemObj::CServerItemObj() :
		m_dbRef(0), m_scale(1.0f, 1.0f, 1.0f), m_position(0.0f, 0.0f, 0.0f), m_glidAnimSpecial(GLID_INVALID), m_glid(GLID_INVALID), m_spawnID(-1) //-1 = non generator spawned
		,
		m_netTraceId(0),
		m_placementId(0),
		m_requiredKey(0) //0=none
		,
		m_contents(NULL),
		m_type(SI_TYPE_LOOTABLE_CHEST),
		m_rotationAdjust(0),
		m_collectionQuestDB(NULL),
		m_aiIndex(-1) // none
		,
		m_noResponseMsg("..."),
		m_existsInAllPermInstances(TRUE),
		m_zoneIndex(0) {
	m_channel.clear();
}

void CServerItemObj::SafeDelete() {
	if (m_contents) {
		m_contents->SafeDelete();
		delete m_contents;
		m_contents = 0;
	}

	// remove cleanup of mats
	m_charConfig.disposeAllItems();

	if (m_collectionQuestDB) {
		m_collectionQuestDB->SafeDelete();
		delete m_collectionQuestDB;
		m_collectionQuestDB = 0;
	}
}

void CServerItemObj::operator=(const CServerItemObj& rhs) {
	m_glidAnimSpecial = rhs.m_glidAnimSpecial;
	m_dbRef = rhs.m_dbRef;
	m_position = rhs.m_position;
	m_glid = rhs.m_glid;
	m_spawnID = rhs.m_spawnID; //-1 = non generator spawned
	m_netTraceId = rhs.m_netTraceId;
	m_requiredKey = rhs.m_requiredKey;
	m_channel = rhs.m_channel;
	m_zoneIndex = rhs.m_zoneIndex;
	m_displayedName = rhs.m_displayedName;
	m_type = rhs.m_type;
	m_rotationAdjust = rhs.m_rotationAdjust;
	m_aiIndex = rhs.m_aiIndex; //-1 = none
	m_noResponseMsg = rhs.m_noResponseMsg;
	m_existsInAllPermInstances = rhs.m_existsInAllPermInstances;
	m_charConfig = rhs.m_charConfig;
	m_glidsArmed = rhs.m_glidsArmed; // drf - added

	if (rhs.m_contents)
		rhs.m_contents->Clone(&m_contents);

	if (rhs.m_collectionQuestDB)
		rhs.m_collectionQuestDB->Clone(&m_collectionQuestDB);
}

void CServerItemObj::Clone(CServerItemObj** ppSIO_out) {
	CServerItemObj* pSIO_new = new CServerItemObj();
	*pSIO_new = *this;
	*ppSIO_out = pSIO_new;
}

void CServerItemObj::Serialize(CArchive& ar) {
	BOOL reserved = 0;
	CStringA reservedString = " ";
	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		ar << m_channel
		   << m_dbRef
		   << m_position.x
		   << m_position.y
		   << m_position.z
		   << m_glidAnimSpecial
		   << m_glid
		   << m_spawnID
		   << m_contents
		   << m_requiredKey
		   << (int)0 // old m_spawnCfgCount
		   << m_displayedName
		   << (int)m_type
		   << m_rotationAdjust
		   << 0 //m_chatStringCount
		   << m_collectionQuestDB;

		ar << -1 //m_randomAnimation
		   << -1 //m_randomExpEffect
		   << m_aiIndex
		   << 1000.0 //m_randomInterval
		   << 0 //m_organizeGroupID
		   << 1000.0 //m_sightRange
		   << reserved
		   << reserved
		   << reserved;

		ar << reservedString
		   << reservedString
		   << reservedString
		   << m_noResponseMsg;

		// converted spawn info to m_charConfig
		m_charConfig.Serialize(ar, 1);

		ar << m_existsInAllPermInstances;

	} else {
		int version = ar.GetObjectSchema();
		int oldDimConfigCount = 0;
		int itemType = 0;
		int deprecated;
		ar >> m_channel >> m_dbRef >> m_position.x >> m_position.y >> m_position.z >> m_glidAnimSpecial >> m_glid >> m_spawnID >> m_contents >> m_requiredKey >> oldDimConfigCount >> m_displayedName >> itemType >> m_rotationAdjust >> deprecated //m_chatStringCount
			>> m_collectionQuestDB;
		m_type = (SERVER_ITEM_TYPE)itemType;

		if (version < 2) {
			m_aiIndex = -1; //-1 = none
			m_noResponseMsg = "....";

		} else if (version == 2) {
			ar >> deprecated //m_randomAnimation
				>> deprecated //m_randomExpEffect
				>> m_aiIndex >> deprecated //m_randomInterval
				>> deprecated //m_organizeGroupID
				>> reserved >> reserved >> reserved >> reserved;
			m_noResponseMsg = "....";
		} else if (version >= 3) {
			ar >> deprecated //m_randomAnimation
				>> deprecated //m_randomExpEffect
				>> m_aiIndex >> deprecated //m_randomInterval
				>> deprecated //m_organizeGroupID
				>> deprecated //m_sightRange
				>> reserved >> reserved >> reserved;

			ar >> reservedString >> reservedString >> reservedString >> m_noResponseMsg;
		}

		if (version > 5 && version < 7) {
			m_charConfig.Serialize(ar, 0);
		} else if (version >= 7) {
			m_charConfig.Serialize(ar, 1);
		} else if (version >= 8) {
			m_charConfig.Serialize(ar, 2);
		} else {
			// convert old stuff
			m_charConfig.initBaseItem(oldDimConfigCount);
			int tempInt;

			for (int i = 0; i < oldDimConfigCount; i++) {
				ar >> tempInt;
				m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, i, tempInt);
			}
			if (version > 3) {
				for (int slot = 0; slot < oldDimConfigCount; slot++) {
					ar >> tempInt;
					m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, slot, tempInt);
				}
			}
		}

		if (version > 4)
			ar >> m_existsInAllPermInstances;

		m_netTraceId = 0;
	}

	if (m_type == 1 || m_type == 2 || m_type > 4)
		LogFatal("DEAD_CODE - staticItemType=" << m_type);
}

void ServerItemObjMap::AddItem(CServerItemObj* pSIO, bool replaceIfFound) {
	if (!pSIO)
		return;

	if (replaceIfFound) {
		auto itrPidServerItemObjMap = m_pidServerItemObjMap.find(pSIO->m_placementId);
		if (itrPidServerItemObjMap != m_pidServerItemObjMap.end()) {
			// Already exists - replace content
			*itrPidServerItemObjMap->second = *pSIO;
			pSIO->SafeDelete();
			delete pSIO;
			return;
		}
	}

	// Add To Key -> vector<pSIO> Map
	auto mapKey = pSIO->MapKey();
	auto mapItr = m_keyServerItemVecMap.find(mapKey);
	if (mapItr != m_keyServerItemVecMap.end()) {
		mapItr->second.push_back(pSIO);
	} else {
		ServerItemVec newVec;
		newVec.push_back(pSIO);
		m_keyServerItemVecMap.insert(std::pair<LONG, ServerItemVec>(mapKey, newVec));
	}

	// Add To PID -> pSIO Map
	if (pSIO->m_placementId)
		m_pidServerItemObjMap[pSIO->m_placementId] = pSIO;
}

// DRF - Added - Erase From PID -> pSIO Map
bool ServerItemObjMap::ErasePlacementId(int placementId) {
	if (!placementId)
		return false;
	auto it = m_pidServerItemObjMap.find(placementId);
	if (it == m_pidServerItemObjMap.end())
		return false;
	m_pidServerItemObjMap.erase(it);
	return true;
}

bool ServerItemObjMap::DeleteByPlacementId(const ZoneIndex& zoneIndex, int placementId) {
	// Erase Placement Id
	if (!ErasePlacementId(placementId))
		return false;

	// Erase From Key -> vector<pSIO> Map
	long mapKey = (long)zoneIndex;
	auto mapItr = m_keyServerItemVecMap.find(mapKey);
	if (mapItr == m_keyServerItemVecMap.end())
		return false;
	auto& siVec = mapItr->second;
	for (auto siVecIter = siVec.begin(); siVecIter != siVec.end(); siVecIter++) {
		if ((*siVecIter)->m_placementId == placementId) {
			(*siVecIter)->SafeDelete();
			delete (*siVecIter);
			(*siVecIter) = nullptr;
			siVec.erase(siVecIter);
			LogInfo("OK - " << zoneIndex.ToStr() << " objId=" << placementId);
			return true;
		}
	}
	return false;
}

bool ServerItemObjMap::DeleteByZoneIndex(const ZoneIndex& zoneIndex) {
	long mapKey = (long)zoneIndex;
	auto mapItr = m_keyServerItemVecMap.find(mapKey);
	if (mapItr == m_keyServerItemVecMap.end())
		return false;
	auto& siVec = mapItr->second;
	for (auto siVecIter = siVec.begin(); siVecIter != siVec.end(); siVecIter++) {
		auto placementId = (*siVecIter)->m_placementId;
		LogInfo(" ... " << zoneIndex.ToStr() << " objId=" << placementId);
		ErasePlacementId(placementId);
		(*siVecIter)->SafeDelete();
		delete (*siVecIter);
		(*siVecIter) = nullptr;
	}
	siVec.clear();
	return true;
}

BOOL ServerItemObjMap::DeleteBySpawnIDAndChannel(int spawnId, ChannelId channel) {
	auto mapItr = m_keyServerItemVecMap.find(channel.toLong());
	if (mapItr != m_keyServerItemVecMap.end()) {
		for (auto siVecIter = (*mapItr).second.begin(); siVecIter != (*mapItr).second.end(); siVecIter++) {
			if ((*siVecIter)->m_spawnID == spawnId) {
				auto placementId = (*siVecIter)->m_placementId;
				ErasePlacementId(placementId);
				(*siVecIter)->SafeDelete();
				delete (*siVecIter);
				(*siVecIter) = 0;
				(*mapItr).second.erase(siVecIter);
				if ((*mapItr).second.empty())
					m_keyServerItemVecMap.erase(mapItr);
				return TRUE;
			}
		}
	}

	if (channel.isPermanent()) {
		mapItr = m_keyServerItemVecMap.find(channel.channelId());
		if (mapItr != m_keyServerItemVecMap.end()) {
			for (auto siVecIter = (*mapItr).second.begin(); siVecIter != (*mapItr).second.end(); siVecIter++) {
				if ((*siVecIter)->m_spawnID == spawnId) {
					auto placementId = (*siVecIter)->m_placementId;
					ErasePlacementId(placementId);
					(*siVecIter)->SafeDelete();
					delete (*siVecIter);
					(*siVecIter) = 0;
					(*mapItr).second.erase(siVecIter);
					if ((*mapItr).second.empty())
						m_keyServerItemVecMap.erase(mapItr);
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL ServerItemObjMap::DeleteByNetTraceId(int netTraceId) {
	for (auto SIMiter = m_keyServerItemVecMap.begin(); SIMiter != m_keyServerItemVecMap.end(); SIMiter++) {
		for (auto siVecIter = (*SIMiter).second.begin(); siVecIter != (*SIMiter).second.end(); siVecIter++) {
			if ((*siVecIter)->m_netTraceId == netTraceId) {
				auto placementId = (*siVecIter)->m_placementId;
				ErasePlacementId(placementId);
				(*siVecIter)->SafeDelete();
				delete (*siVecIter);
				(*siVecIter) = 0;
				(*SIMiter).second.erase(siVecIter);
				if ((*SIMiter).second.empty())
					m_keyServerItemVecMap.erase(SIMiter);
				return TRUE;
			}
		}
	}
	return FALSE;
}

void ServerItemObjMap::SafeDelete() {
	for (auto SIMiter = m_keyServerItemVecMap.begin(); SIMiter != m_keyServerItemVecMap.end(); SIMiter++) {
		for (auto& pSIO : (*SIMiter).second) {
			if (!pSIO)
				continue;
			pSIO->SafeDelete();
			delete pSIO;
			pSIO = nullptr;
		}
	}
	m_keyServerItemVecMap.clear();
	m_pidServerItemObjMap.clear();
}

void ServerItemObjMap::GetServerItemVec(ServerItemVec*& pSIV, long key1, long key2) {
	auto mapItr = m_keyServerItemVecMap.find(key1);
	if (mapItr == m_keyServerItemVecMap.end())
		mapItr = m_keyServerItemVecMap.find(key2);
	pSIV = (mapItr != m_keyServerItemVecMap.end()) ? &(mapItr->second) : nullptr;
}

CServerItemObj* ServerItemObjMap::GetObjectBySpawnID(int spawnId) {
	for (const auto& mapItr : m_keyServerItemVecMap) {
		for (const auto& pSIO : mapItr.second) {
			if (!pSIO)
				continue;
			if (pSIO->m_spawnID == spawnId)
				return pSIO;
		}
	}
	return nullptr;
}

CServerItemObj* ServerItemObjMap::GetObjectByNetTraceId(int netTraceId) {
	for (const auto& mapItr : m_keyServerItemVecMap) {
		for (const auto& pSIO : mapItr.second) {
			if (!pSIO)
				continue;
			if (pSIO->m_netTraceId == netTraceId)
				return pSIO;
		}
	}
	return nullptr;
}

CServerItemObj* ServerItemObjMap::GetItemInRadius(Vector3f entPos, float radius, const ChannelId& channel) {
	auto mapResult = m_keyServerItemVecMap.find(channel.toLong());
	if (mapResult != m_keyServerItemVecMap.end()) {
		for (const auto& pSIO : (*mapResult).second) {
			if (!pSIO)
				continue;

			BOOL inRange = MatrixARB::BoundingSphereCheck(
				radius,
				0.0f,
				entPos.x,
				entPos.y,
				entPos.z,
				pSIO->m_position.x,
				pSIO->m_position.y,
				pSIO->m_position.z);
			if (inRange)
				return pSIO;
		}
	}

	if (channel.isPermanent()) {
		mapResult = m_keyServerItemVecMap.find(channel.channelId());
		if (mapResult != m_keyServerItemVecMap.end()) {
			for (const auto& pSIO : (*mapResult).second) {
				if (!pSIO)
					continue;

				BOOL inRange = MatrixARB::BoundingSphereCheck(
					radius,
					0.0f,
					entPos.x,
					entPos.y,
					entPos.z,
					pSIO->m_position.x,
					pSIO->m_position.y,
					pSIO->m_position.z);
				if (inRange)
					return pSIO;
			}
		}
	}

	return nullptr;
}

enum class eRefModel { Isabella,
	Sofia,
	Eva,
	Jimmy,
	Hondo,
	Meaty };
enum class eGender { None,
	Male,
	Female };
enum class eCharConfigOpt {
	// Mesh swap
	STYLE_MIN,
	HairStyle,
	EyebrowStyle,
	BeardStyle,
	STYLE_MAX,
	// Material swap
	COLOR_MIN,
	SkinColor,
	HairColor,
	FacialHairColor,
	EyeColor,
	MakeupColor,
	COLOR_MAX,
};
enum class eBaseSlot { None,
	Neck,
	Torso,
	Crotch,
	Arms,
	Legs,
	Hands,
	Feet,
	Shirt,
	Pants,
	Boots,
	Hair,
	MaleHair_F = 25,
	MaleHair_G,
	Hair_A,
	Hair_B,
	Hair_C,
	Hair_D,
	Hair_E,
}; // Enumerate values match reference model asset
enum class eHeadSlot { EyeRight,
	_HeadSlot1,
	EyeLeft,
	_HeadSlot3,
	_HeadSlot4,
	_HeadSlot5,
	Face,
	Eyebrow,
	Beard,
	Makeup = Beard,
	_HeadSlot9 }; // Enumerate values match reference model asset

struct RefModelDef {
	eGender m_gender;
	eRefModel m_model;
	std::map<eBaseSlot, int> m_meshSlotRemap; // Optional, if this model has a mesh slot ID does not match the default eMenuSlot underlying value
};

// Model
const static std::map<unsigned, RefModelDef> SUPPORTED_REF_MODELS{
	{ 10, { eGender::Female, eRefModel::Isabella } },
	{ 11, { eGender::Female, eRefModel::Sofia } },
	{ 12, { eGender::Female, eRefModel::Eva } },
	{ 13, { eGender::Male, eRefModel::Jimmy } },
	{ 14, { eGender::Male, eRefModel::Hondo } },
	{ 15, { eGender::Male, eRefModel::Meaty } },
};
const static size_t DEFAULT_BASE_SLOT_COUNT = 32;

// Head
const static std::map<eGender, std::vector<GLID>> HEAD_GLID_BY_GENDER{
	{ eGender::Male, { 2988, 2989, 2990, 3720, 3746 } },
	{ eGender::Female, { 2991, 2992, 2993, 3744, 3745 } },
};
const static std::map<eGender, size_t> DEFAULT_HEAD_SLOT_COUNT{
	{ eGender::Male, 9 },
	{ eGender::Female, 10 },
};

// Mesh slot configurations
const static std::map<eGender, std::map<eCharConfigOpt, std::pair<std::vector<eBaseSlot>, std::vector<eHeadSlot>>>> MESH_SLOTS_BY_GENDER{
	{ eGender::Male, {
						 { eCharConfigOpt::SkinColor, { { eBaseSlot::Neck, eBaseSlot::Torso, eBaseSlot::Hands, eBaseSlot::Crotch, eBaseSlot::Legs, eBaseSlot::Feet, eBaseSlot::Arms }, { eHeadSlot::Face } } },
						 { eCharConfigOpt::HairColor, { { eBaseSlot::Hair_A, eBaseSlot::Hair_B, eBaseSlot::Hair_C, eBaseSlot::Hair_D, eBaseSlot::Hair_E, eBaseSlot::MaleHair_F, eBaseSlot::MaleHair_G }, {} } },
						 { eCharConfigOpt::HairStyle, { { eBaseSlot::Hair_A, eBaseSlot::Hair_B, eBaseSlot::Hair_C, eBaseSlot::Hair_D, eBaseSlot::Hair_E, eBaseSlot::MaleHair_F, eBaseSlot::MaleHair_G }, {} } },
						 { eCharConfigOpt::FacialHairColor, { {}, { eHeadSlot::Eyebrow, eHeadSlot::Beard } } },
						 { eCharConfigOpt::EyebrowStyle, { {}, { eHeadSlot::Eyebrow } } },
						 { eCharConfigOpt::BeardStyle, { {}, { eHeadSlot::Beard } } },
						 { eCharConfigOpt::EyeColor, { {}, { eHeadSlot::EyeLeft, eHeadSlot::EyeRight } } },
					 } },
	{ eGender::Female, {
						   { eCharConfigOpt::SkinColor, { { eBaseSlot::Neck, eBaseSlot::Torso, eBaseSlot::Hands, eBaseSlot::Crotch, eBaseSlot::Legs, eBaseSlot::Feet, eBaseSlot::Arms }, { eHeadSlot::Face } } },
						   { eCharConfigOpt::HairColor, { { eBaseSlot::Hair_A, eBaseSlot::Hair_B, eBaseSlot::Hair_C, eBaseSlot::Hair_D, eBaseSlot::Hair_E }, {} } },
						   { eCharConfigOpt::HairStyle, { { eBaseSlot::Hair_A, eBaseSlot::Hair_B, eBaseSlot::Hair_C, eBaseSlot::Hair_D, eBaseSlot::Hair_E }, {} } },
						   { eCharConfigOpt::FacialHairColor, { {}, { eHeadSlot::Eyebrow } } },
						   { eCharConfigOpt::EyebrowStyle, { {}, { eHeadSlot::Eyebrow } } },
						   { eCharConfigOpt::EyeColor, { {}, { eHeadSlot::EyeLeft, eHeadSlot::EyeRight } } },
						   { eCharConfigOpt::MakeupColor, { {}, { eHeadSlot::Makeup } } },
					   } },
};

bool CServerItemObj::parseJSON(const std::string& jsonStr, std::string& err) {
	auto json = json11::Json::parse(jsonStr, err);
	if (!err.empty()) {
		return false;
	}

	/*
		{
		   "scaleY":90,
		   "scaleXZ":90,
		   "dbIndex":12,
		   "faceIndex":4,
		   "skinColor":0,
		   "faceCover":5,
		   "beardColor":2,
		   "hairIndex":1,
		   "hairColor":24,
		   "eyebrowIndex":0,
		   "eyeColor":0
		   "GLIDS":[
			  1247,
			  1248,
			  1249
		   ],
		}
	*/

	// Parse JSON
	float scaleY = json["scaleY"].is_null() ? 1.0f : (json["scaleY"].int_value() / 100.0f);
	float scaleXZ = json["scaleXZ"].is_null() ? 1.0f : (json["scaleXZ"].int_value() / 100.0f);
	int dbIndex = json["dbIndex"].is_null() ? m_dbRef : json["dbIndex"].int_value();
	int faceIndex = json["faceIndex"].is_null() ? -1 : json["faceIndex"].int_value();
	std::map<eCharConfigOpt, int> charConfigOptions = {
		{ eCharConfigOpt::SkinColor, json["skinColor"].int_value() },
		{ eCharConfigOpt::HairColor, json["hairColor"].int_value() },
		{ eCharConfigOpt::HairStyle, json["hairIndex"].int_value() },
		{ eCharConfigOpt::FacialHairColor, json["beardColor"].int_value() }, // "beardColor" controls eyebrow color + male beard color
		{ eCharConfigOpt::EyebrowStyle, json["eyebrowIndex"].int_value() },
		{ eCharConfigOpt::BeardStyle, json["faceCover"].int_value() }, // "faceCover" is used for both female makeup color and male beard style
		{ eCharConfigOpt::EyeColor, json["eyeColor"].int_value() },
		{ eCharConfigOpt::MakeupColor, json["faceCover"].int_value() },
	};

	// Parse JSON GLIDs array
	m_glidsArmed.clear();
	if (!json["GLIDS"].is_null()) {
		if (!json["GLIDS"].is_array()) {
			err = "Invalid value for `GLIDs' parameter - must be an array of item IDs";
			return false;
		}
		for (const auto& jsonGLID : json["GLIDS"].array_items()) {
			m_glidsArmed.push_back(jsonGLID.int_value());
		}
	}

	// Additional validations
	auto itrRefModel = SUPPORTED_REF_MODELS.find(dbIndex);
	if (itrRefModel == SUPPORTED_REF_MODELS.end()) {
		err = "Invalid value for `dbIndex' parameter: " + std::to_string(dbIndex);
		return false;
	}

	// Use primary actor group for gender -- most avatar models have up to 1 actor group
	eGender gender = itrRefModel->second.m_gender;

	// Determine face/head item
	GLID headGLID = 0;
	size_t headSlotCount = 0;
	if (faceIndex >= 0) {
		auto itrFace = HEAD_GLID_BY_GENDER.find(gender);
		if (itrFace == HEAD_GLID_BY_GENDER.end()) {
			// Gender is unknown -- unable to mount face item
			err = "Avatar model specified by `dbIndex' (" + std::to_string(dbIndex) + ") does not support parameter `faceIndex'";
			return false;
		}

		const auto& headGLIDs = itrFace->second;
		if (faceIndex >= headGLIDs.size()) {
			err = "Invalid value for `faceIndex' parameter: " + std::to_string(faceIndex) + " with avatar model specified by `dbIndex' (" + std::to_string(dbIndex) + ")";
			return false;
		}
		headGLID = headGLIDs[faceIndex];

		auto itrSlotCount = DEFAULT_HEAD_SLOT_COUNT.find(gender);
		assert(itrSlotCount != DEFAULT_HEAD_SLOT_COUNT.end());
		headSlotCount = itrSlotCount->second;
	}

	// Assignment
	m_scale.Set(scaleXZ, scaleY, scaleXZ);
	m_dbRef = dbIndex;
	m_charConfig.disposeAllItems();
	m_charConfig.initBaseItem(DEFAULT_BASE_SLOT_COUNT);
	if (headGLID != 0) {
		m_charConfig.addItem(headGLID, headSlotCount);
	}

	auto itrMeshSlotConfigs = MESH_SLOTS_BY_GENDER.find(gender);
	assert(itrMeshSlotConfigs != MESH_SLOTS_BY_GENDER.end());

	for (const auto& pr : itrMeshSlotConfigs->second) {
		eCharConfigOpt opt = pr.first;
		bool changeMesh = opt > eCharConfigOpt::STYLE_MIN && opt < eCharConfigOpt::STYLE_MAX;
		bool changeMaterial = opt > eCharConfigOpt::COLOR_MIN && opt < eCharConfigOpt::COLOR_MAX;
		const std::vector<eBaseSlot>& baseSlots = pr.second.first;
		const std::vector<eHeadSlot>& headSlots = pr.second.second;

		assert(charConfigOptions.find(opt) != charConfigOptions.end());
		int value = charConfigOptions[opt];

		for (eBaseSlot slot : baseSlots) {
			int slotIndex = static_cast<int>(slot);
			if (changeMesh) {
				m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, slotIndex, static_cast<SHORT>(value));
			} else if (changeMaterial) {
				m_charConfig.setItemSlotMaterialId(GOB_BASE_ITEM, slotIndex, static_cast<USHORT>(value));
			} else {
				assert(false);
			}
		}

		for (eHeadSlot slot : headSlots) {
			int slotIndex = static_cast<int>(slot);
			if (changeMesh) {
				m_charConfig.setItemSlotMeshId(headGLID, slotIndex, static_cast<SHORT>(value));
			} else if (changeMaterial) {
				m_charConfig.setItemSlotMaterialId(headGLID, slotIndex, static_cast<USHORT>(value));
			} else {
				assert(false);
			}
		}
	}

	return true;
}

IMPLEMENT_SERIAL_SCHEMA(CServerItemObj, CObject)

BEGIN_GETSET_IMPL(CServerItemObj, IDS_SERVERITEMOBJ_NAME)
GETSET_RANGE(m_channel, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, WORLD, INT_MIN, INT_MAX)
GETSET_RANGE(m_dbRef, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, DBREF, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_position, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, POSITION)
GETSET_RANGE(m_glidAnimSpecial, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, CURANIM, INT_MIN, INT_MAX)
GETSET_RANGE(m_glid, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, GLID, INT_MIN, INT_MAX)
GETSET_RANGE(m_spawnID, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, SPAWNID, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_contents, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, CONTENTS)
GETSET_RANGE(m_requiredKey, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, REQUIREDKEY, INT_MIN, INT_MAX)
GETSET_MAX(m_displayedName, gsCString, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, DISPLAYEDNAME, STRLEN_MAX)
GETSET_RANGE(m_type, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, STATICITEMTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_rotationAdjust, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, ROTATIONADJUST, INT_MIN, INT_MAX)
GETSET_OBLIST_PTR(m_collectionQuestDB, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, COLLECTIONQUESTDB)
GETSET_RANGE(m_aiIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, MOUNTOPTION, INT_MIN, INT_MAX)
GETSET_MAX(m_noResponseMsg, gsCString, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, NORESPONSEMSG, STRLEN_MAX)
GETSET_RANGE(m_netTraceId, gsInt, GS_FLAGS_DEFAULT, 0, 0, SERVERITEMOBJ, NETWORKASSIGNEDID, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP
