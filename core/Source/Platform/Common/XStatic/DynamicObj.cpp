///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include <math.h>
#include "matrixarb.h"
#include "IClientEngine.h"
#include "dynamicobj.h"
#include "CDeformableMesh.h"
#include "ReMaterial.h"
#include "ReMeshCreate.h"
#include "TextureObj.h"
#ifdef _ClientOutput
#include "ReD3DX9.h"
#include "ReMeshCache.h"
#include "DynamicObPlacementModelProxy.h"
#endif
#include "EnvParticleSystem.h"
#include "RuntimeSkeleton.h"
#include "CArmedInventoryClass.h"
#include "SkeletonManager.h"
#include "SkeletonProxy.h"
#include "SkeletonConfiguration.h"
#include "UgcConstants.h"
#include "CBoneClass.h"
#include "SkeletonLabels.h"
#include "LookAt.h"
#include "CTriggerClass.h"
#include "EffectTrack.h"
#include "PhysicsUserData.h"
#include "LogHelper.h"
#include "KepWidgets.h"
#include "MeshStrings.h"
#include "Event/Base/IDispatcher.h"
#include "Event/Events/DynamicObjectEvents.h"
#include "Core/Math/Interpolate.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/TransformUtil.h"
#include "Core/Math/Units.h"
#include "Core/Math/Sphere.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "KEPPhysics/RigidBodyStatic.h"
#include "Common/KEPUtil/Helpers.h"
#include "ContentService.h"
#include "CMovementObj.h"
#include "TextureProvider.h"
#include "DynamicObjectManager.h"
#include <Core/Util/CallbackSystem.h>
#include <Common/include/CoreStatic/CGlobalInventoryClass.h>
#include "Common/include/CoreStatic/ExternalEXMesh.h"
#include <KEPPhysics/CollisionShape.h>
#include "common/include/IMemSizeGadget.h"
#include "common/include/MemSizeSpecializations.h"

#define VISUAL_SCHEMA_VERSION 3
#define DYNAMIC_OBJ_SCHEMA_VERSION 4

static LogInstance("Instance");

namespace KEP {

const D3DXCOLOR BOUNDBOX_COLOR(1.00f, 1.00f, 1.00f, 0.99f);
const D3DXCOLOR MEDIA_VIS_IDLE_COLOR(0.70f, 0.70f, 0.70f, 0.99f);
const D3DXCOLOR MEDIA_VIS_PLAY_COLOR(0.70f, 0.70f, 0.70f, 0.99f);
//const D3DXCOLOR MEDIA_VIS_IDLE_COLOR(1.00f, 0.00f, 0.00f, 0.99f);
//const D3DXCOLOR MEDIA_VIS_PLAY_COLOR(0.00f, 1.00f, 0.00f, 0.99f);

const float BOUNDBOX_EMISSIVE = 0.80f; // Fraction of diffuse color that non-lit areas receive
const float MEDIA_VIDRANGE_IDLE_EMISSIVE = 0.60f;
const float MEDIA_VIDRANGE_PLAY_EMISSIVE = 0.80f;
const float MEDIA_AUDRANGE_IDLE_EMISSIVE = 0.50f;
const float MEDIA_AUDRANGE_PLAY_EMISSIVE = 0.80f;

const D3DMATERIAL9 BOUNDBOX_D3D_MATERIAL = {
	BOUNDBOX_COLOR,
	{ 0.1f, 0.1f, 0.1f, 1.0f },
	{ 0.0f, 0.0f, 0.0f, 1.0f },
	BOUNDBOX_COLOR* BOUNDBOX_EMISSIVE,
	0.0f,
};

const D3DMATERIAL9 MEDIA_VIDRANGE_IDLE_D3D_MATERIAL = {
	MEDIA_VIS_IDLE_COLOR,
	{ 0.1f, 0.1f, 0.1f, 1.0f },
	{ 0.0f, 0.0f, 0.0f, 1.0f },
	MEDIA_VIS_IDLE_COLOR* MEDIA_VIDRANGE_IDLE_EMISSIVE,
	0.0f,
};

const D3DMATERIAL9 MEDIA_VIDRANGE_PLAY_D3D_MATERIAL = {
	MEDIA_VIS_PLAY_COLOR,
	{ 0.1f, 0.1f, 0.1f, 1.0f },
	{ 0.0f, 0.0f, 0.0f, 1.0f },
	MEDIA_VIS_PLAY_COLOR* MEDIA_VIDRANGE_PLAY_EMISSIVE,
	0.0f,
};

const D3DMATERIAL9 MEDIA_AUDRANGE_IDLE_D3D_MATERIAL = {
	MEDIA_VIS_IDLE_COLOR,
	{ 0.1f, 0.1f, 0.1f, 1.0f },
	{ 0.0f, 0.0f, 0.0f, 1.0f },
	MEDIA_VIS_IDLE_COLOR* MEDIA_AUDRANGE_IDLE_EMISSIVE,
	0.0f,
};

const D3DMATERIAL9 MEDIA_AUDRANGE_PLAY_D3D_MATERIAL = {
	MEDIA_VIS_PLAY_COLOR,
	{ 0.1f, 0.1f, 0.1f, 1.0f },
	{ 0.0f, 0.0f, 0.0f, 1.0f },
	MEDIA_VIS_PLAY_COLOR* MEDIA_AUDRANGE_PLAY_EMISSIVE,
	0.0f,
};

CMaterialObject CDynamicPlacementObj::ms_boxMaterial(BOUNDBOX_D3D_MATERIAL);
CMaterialObject CDynamicPlacementObj::ms_mediaVideoRangeMaterialIdle(MEDIA_VIDRANGE_IDLE_D3D_MATERIAL);
CMaterialObject CDynamicPlacementObj::ms_mediaVideoRangeMaterialPlay(MEDIA_VIDRANGE_PLAY_D3D_MATERIAL);
CMaterialObject CDynamicPlacementObj::ms_mediaAudioRangeMaterialIdle(MEDIA_AUDRANGE_IDLE_D3D_MATERIAL);
CMaterialObject CDynamicPlacementObj::ms_mediaAudioRangeMaterialPlay(MEDIA_AUDRANGE_PLAY_D3D_MATERIAL);

bool CDynamicPlacementObj::PivotTransitionPosition::interpolatePosition(Out<Vector3f> position) {
	Vector3f* outPos = &position.get();

	// find a new value for m_location, which is somewhere between
	// the first and second keyframe, until we can initialise the
	// keyframe array with more data.

	if (m_currentInterpolationTime >= m_keyframes[1].m_timePos) {
		*outPos = m_keyframes[1].m_position;
		m_bMoving = false;
		return false;
	}

	// In here, we know that time <= the end time (keyframe[1]).
	// Calculate the lerp factor.
	float frameTime = m_keyframes[1].m_timePos - m_keyframes[0].m_timePos;
	float elapsedTime = m_currentInterpolationTime - m_keyframes[0].m_timePos;
	elapsedTime = (std::max)(0.0f, elapsedTime);
	float factor = 0;
	if (m_accDur > elapsedTime) {
		factor = (m_accDistFrac * elapsedTime * elapsedTime) / (m_accDur * m_accDur);
	} else if ((m_accDur + m_constVelDur) > elapsedTime) {
		factor = m_accDistFrac + (m_constVelDistFrac * (elapsedTime - m_accDur) / m_constVelDur);
	} else {
		float timeRemaining = frameTime - elapsedTime;
		float timeNormalizingFactor = frameTime - m_constVelDur - m_accDur;
		float remDistFactor = 1 - m_accDistFrac - m_constVelDistFrac;
		factor = 1 - ((remDistFactor * timeRemaining * timeRemaining) / (timeNormalizingFactor * timeNormalizingFactor));
	}

	// Interpolate position
	*outPos = InterpolateLinear(m_keyframes[0].m_position, m_keyframes[1].m_position, factor);
	return true;
}

bool CDynamicPlacementObj::PivotTransitionRotation::interpolateOrientation(Out<Vector3f> vX, Out<Vector3f> vY, Out<Vector3f> vZ) {
	// find a new value for m_location, which is somewhere between
	// the first and second keyframe, until we can initialise the
	// keyframe array with more data.

	// For code readablity
	Vector3f* outFdVect = &vX.get(); // Forward
	Vector3f* outUpVect = &vY.get(); // Up
	Vector3f* outSdVect = &vZ.get(); // Side

	if (m_currentInterpolationTime >= m_keyframes[1].m_timeRot) {
		*outFdVect = m_keyframes[1].m_fdVect;
		*outUpVect = m_keyframes[1].m_upVect;
		*outSdVect = m_keyframes[1].m_sdVect;
		m_bRotating = false;
		return false;
	}

	// In here, we know that time <= the end time (keyframe[1]).
	// Calculate the lerp factor.
	float frameTime = m_keyframes[1].m_timeRot - m_keyframes[0].m_timeRot;
	float elapsedTime = m_currentInterpolationTime - m_keyframes[0].m_timeRot;
	elapsedTime = (std::max)(0.0f, elapsedTime);
	float factor = elapsedTime / frameTime;

	// Interpolate orientation
	//
	// interpolate either [0] or [1] and re-calculate the other
	// using the cross product of the lerped one and [2]

	// Assume up vector never changes (for now)
	*outUpVect = m_keyframes[0].m_upVect;

	// Find the new forward direction
	Vector3f newDir;
	if (m_useBisectVect) {
		// Use a bi-secting vector as an extra key-frame for wide angle rotations
		if (factor < 0.5f) {
			// [0, 0.5) - interpolate between V0 and Vb
			newDir = InterpolateLinear(m_keyframes[0].m_fdVect, m_bisectDirVect, factor * 2.0f);
		} else {
			// (0.5, 1] - interpolate between Vb and V1
			newDir = InterpolateLinear(m_bisectDirVect, m_keyframes[1].m_fdVect, factor * 2.0f - 1.0f);
		}
	} else {
		// Interpolate between V0 and V1
		newDir = InterpolateLinear(m_keyframes[0].m_fdVect, m_keyframes[1].m_fdVect, factor);
	}

	*outFdVect = newDir.GetNormalized();
	*outSdVect = outFdVect->Cross(*outUpVect);
	outSdVect->Normalize();

	return true;
}

void CDynamicPlacementObj::PivotTransitionPosition::startPivotToPosition(TimeMs startTime, DWORD duration, DWORD accDur, DWORD constVelDur, double accDistFrac, double constVelDistFrac, const Matrix44f& beginningMatrix, const Matrix44f& endingMatrix) {
	m_bMoving = true;
	m_keyframes[0].m_position = beginningMatrix.GetTranslation();
	m_keyframes[0].m_timePos = startTime;
	m_keyframes[1].m_position = endingMatrix.GetTranslation();
	m_keyframes[1].m_timePos = m_keyframes[0].m_timePos + duration;
	m_accDur = accDur;
	m_constVelDur = constVelDur;
	m_accDistFrac = accDistFrac;
	m_constVelDistFrac = constVelDistFrac;
}

void CDynamicPlacementObj::PivotTransitionRotation::startPivotToOrientation(TimeMs startTime, DWORD duration, const Matrix44f& beginningMatrix, const Matrix44f& endingMatrix) {
	// Scaling components are discarded. It will be reapplied after interpolation.
	m_bRotating = true;
	m_keyframes[0].m_fdVect = beginningMatrix.GetRowX().GetNormalized();
	m_keyframes[0].m_upVect = beginningMatrix.GetRowY().GetNormalized();
	m_keyframes[0].m_sdVect = beginningMatrix.GetRowZ().GetNormalized();
	m_keyframes[0].m_timeRot = startTime;
	m_keyframes[1].m_fdVect = endingMatrix.GetRowX().GetNormalized();
	m_keyframes[1].m_upVect = endingMatrix.GetRowY().GetNormalized();
	m_keyframes[1].m_sdVect = endingMatrix.GetRowZ().GetNormalized();
	m_keyframes[1].m_timeRot = m_keyframes[0].m_timeRot + duration;
	m_useBisectVect = false;

	/////////////////////////////////////////////////////
	// Special handling for rotation over wide angles for better approximating angle interpolation

	// Get normalize direction vector in case there is scaling
	float dotProd = m_keyframes[0].m_fdVect.Dot(m_keyframes[1].m_fdVect);

	// Add a bi-secting vector as an extra keyframe if the angle belongs to [120, 180] U [-180, -120]
	if (dotProd <= -0.5f) {
		// Get the bi-secting direction vector
		if (false /*&& dotProd>=-1.0f+Math::default_tolerance<float>()*/) {
			// If the angle belongs to [120, 180) U (-180, 120], Vb = normalize((V0+V1)/2)
			Vector3f midPointVect = 0.5f * (m_keyframes[0].m_fdVect + m_keyframes[1].m_fdVect);
			m_bisectDirVect = midPointVect.GetNormalized();
		} else {
			// If the angle is nearly +/- 180 deg, use the initial side vector, which is perpendicular to the initial forward direction
			m_bisectDirVect = m_keyframes[0].m_sdVect;
		}

		m_useBisectVect = true;
	}
}

Vector3f CDynamicPlacementObj::GetLastLocation() const {
	return m_pSkeleton ? m_pSkeleton->getWorldMatrix().GetTranslation() : m_position;
}

bool CDynamicPlacementObj::NeedsUpdateCallback() const {
	if (!NeedsUpdate())
		return false;
	if (m_pEffectTrack)
		return true;
	if (m_pPivotTransitionPosition)
		return true;
	if (m_pPivotTransitionRotation)
		return true;
	if (m_pLookAtInfo)
		return true;
	return false;
}

void CDynamicPlacementObj::UpdateShouldBeInSublist(DynObjSubListId sublist) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
	pICE->UpdateDynamicObjectInSubList(this, sublist);
}

template <typename T>
void CDynamicPlacementObj::SendNotificationEvent() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
	IDispatcher* pDispatcher = pICE->GetDispatcher();
	std::unique_ptr<T> pEvent = std::make_unique<T>(this->m_placementId);
	pDispatcher->QueueEvent(pEvent.release());
}

void CDynamicPlacementObj::SetEffectTrack(std::unique_ptr<EffectTrack> pEffectTrack) {
	m_pEffectTrack = std::move(pEffectTrack);
	UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
}

void CDynamicPlacementObj::UpdateCallback(TimeMs timeMs) {
#ifdef _ClientOutput
	// update the DO's effects
	if (m_pEffectTrack) {
		m_pEffectTrack->update(timeMs);
		if (m_pEffectTrack->empty()) {
			m_pEffectTrack.reset();
			UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
		}
	}
#endif

	if (!m_pSkeleton)
		return;

	Matrix44f mtxOrig = m_pSkeleton->getWorldMatrix();

	Matrix44f matrix;
	bool bUpdatedMatrix = false;

	if (HandlePivotTransition(timeMs, inout(matrix)))
		bUpdatedMatrix = true;

	if (m_pLookAtInfo) {
		if (!bUpdatedMatrix)
			matrix = m_pSkeleton->getWorldMatrix();
		if (HandleLookAt(inout(matrix)))
			bUpdatedMatrix = true;
	}

	if (bUpdatedMatrix) {
		// Always reapply scaling factor either after interpolation or with the objectLookAt adjustment. We have now removed scaling component in keyframes for ease of calculation.
		m_pSkeleton->setWorldMatrix(&matrix, true);
		m_pSkeleton->invalidateShaders();
		UpdateRigidBodyPosition();
		auto pICE = IClientEngine::Instance();
		if (pICE)
			pICE->MoveDynamicObjectTriggers(m_placementId, matrix.GetTranslation(), matrix.GetRowZ().GetNormalized());

		// Re-calculate the bounding box position.
		FlushBoundBox();
	}
}

bool CDynamicPlacementObj::HandlePivotTransition(TimeMs timeMs, InOut<Matrix44f> matrix) {
	if (!m_pPivotTransitionPosition && !m_pPivotTransitionRotation)
		return false;

	Vector3f vX, vY, vZ;
	if (m_pPivotTransitionRotation) {
		m_pPivotTransitionRotation->SetCurrentTime(timeMs);
		if (!m_pPivotTransitionRotation->interpolateOrientation(out(vX), out(vY), out(vZ))) {
			m_pPivotTransitionRotation.reset(); // No more interpolation needed.
			UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
		}
	} else {
		// Entire matrix still needs to be initialized even if rotation is not being interpolated.
		vX = m_slide;
		vZ = m_direction;
	}

	Vector3f position;
	if (m_pPivotTransitionPosition) {
		m_pPivotTransitionPosition->SetCurrentTime(timeMs);
		if (!m_pPivotTransitionPosition->interpolatePosition(out(position))) {
			m_pPivotTransitionPosition.reset(); // No more interpolation needed.
			UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
		}
	} else {
		position = m_pSkeleton->getWorldMatrix().GetTranslation();
	}

	if (!ToCoordSysPosXZ(position, vX, vZ, out(matrix.get())))
		matrix.get().MakeTranslation(position);

	return true;
}

bool CDynamicPlacementObj::HandleLookAt(InOut<Matrix44f> matrix) {
	// Update orientation
	ObjectType targetType = ObjectType::NONE;
	int targetId = 0;
	std::string targetBone;
	Vector3f offset;
	Matrix44f* pForwardDirectionMatrix;
	bool followTarget = false, allowXZRotation = false;

	if (!GetLookAt(targetType, targetId, targetBone, offset, followTarget, allowXZRotation, pForwardDirectionMatrix) && targetType != ObjectType::NONE)
		return false;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	Matrix44f targetMatrix;
	if (pICE->ObjectGetWorldTransformation(targetType, targetId, targetMatrix)) {
		if (!targetBone.empty()) {
			Matrix44f boneRelMatrix;
			pICE->ObjectGetBoneRelativeTransformation(targetType, targetId, targetBone, boneRelMatrix);
			targetMatrix = boneRelMatrix * targetMatrix;
		}

		Vector3f targetPos = TransformPoint_NonPerspective(targetMatrix, offset);

		Vector3f entPos = matrix.get().GetTranslation();

		Vector3f dirToTarget = entPos - targetPos;
		dirToTarget.y = 0.0f;
		if (dirToTarget.Normalize()) {
			if (!allowXZRotation) {
				matrix.get().GetRowRef(2) = dirToTarget;
				matrix.get().GetRowRef(0) = Vector3f(dirToTarget.z, 0, -dirToTarget.x);
				matrix.get().GetRowRef(1) = Vector3f(0, 1, 0);
				// account for scaling, since matrix is supposed to be scaled.

				if (pForwardDirectionMatrix) {
					// handle forward direction override
					matrix.get() = *pForwardDirectionMatrix * matrix.get();
				}
			} else {
				ASSERT(false);
				// Not supported yet
			}
		}
	}

	if (!followTarget) {
		//Remove target
		SetLookAt(ObjectType::NONE, 0, "", Vector3f(0, 0, 0), false, false, Vector3f(0, 0, 0));
	}

	return true;
}

// Returns a matrix constructed from m_slide, m_direction, m_position, and m_pRS->m_scaleVector.
Matrix44f CDynamicPlacementObj::updateNextFrameMatrix() const {
	Matrix44f newLocation;
	if (!ToCoordSysPosXZ(m_position, m_slide, m_direction, out(newLocation)))
		newLocation.MakeIdentity();

	if (m_pSkeleton) {
		// This needs to be scale the same as the DO's world matrix.
		for (size_t i = 0; i < 3; ++i)
			newLocation[i].SubVector<3>() *= m_pSkeleton->getScaleVector()[i];
	}
	return newLocation;
}

void CDynamicPlacementObj::Reconfig() {
	if (m_pSkeleton) {
		Vector3f position;
		if (m_pPivotTransitionPosition)
			m_pPivotTransitionPosition->interpolatePosition(out(position));
		else
			position = m_position;
		Vector3f vX, vY, vZ;
		if (m_pPivotTransitionRotation)
			m_pPivotTransitionRotation->interpolateOrientation(out(vX), out(vY), out(vZ));
		else {
			vX = m_slide;
			vZ = m_direction;
		}
		Matrix44f mtx;
		if (!ToCoordSysPosXZ(position, vX, vZ, out(mtx)))
			mtx.MakeTranslation(position);
		m_pSkeleton->setWorldMatrix(&mtx);

		// Invalidate lighting.
		m_pSkeleton->invalidateShaders();
		m_pSkeleton->flushLightCache();
	}
	FlushBoundBox();
	UpdateRigidBodyPosition();
	UpdateRigidBodyCollidableState();

	auto pICE = IClientEngine::Instance();
	if (pICE && HasLight())
		pICE->UpdateDynamicObjectLight(this);
}

void CDynamicPlacementObj::UpdateRigidBodyPosition() {
	if (m_pRigidBody || m_pRigidBodyVisual) {
		Matrix44f mtx = m_pSkeleton ? FeetToMeters(m_pSkeleton->getWorldMatrix()) : Matrix44f::GetIdentity();
		if (m_pRigidBody)
			m_pRigidBody->SetTransformMetric(mtx);
		if (m_pRigidBodyVisual)
			m_pRigidBodyVisual->SetTransformMetric(mtx);
	}
}

void CDynamicPlacementObj::UpdateRigidBodyCollidableState() {
	if (m_pRigidBody) {
		bool bCollidable = IsCollisionEnabled();
		m_pRigidBody->SetIsCollidable(bCollidable);
	}
}

void CDynamicPlacementObj::SetScale(float sx, float sy, float sz) {
	//Record object-level scale here
	m_objectScale.x = sx;
	m_objectScale.y = sy;
	m_objectScale.z = sz;

	//Apply it to skeleton, if available
	if (m_pSkeleton)
		m_pSkeleton->scaleSystem(m_objectScale.x * m_skeletonScaleBase.x, m_objectScale.y * m_skeletonScaleBase.y, m_objectScale.z * m_skeletonScaleBase.z);

	Reconfig();
}

void CDynamicPlacementObj::SetRotationEulerAngleZXY(double x, double y, double z) {
	// DRF - Bug Fix
	// Platform Only Supports Y Rotation!
	x = z = 0.0;
	y = RadNorm(y);

	// Order of rotation matters!  This ordering results in the same rotation
	// matrix that MatrixARB::RotationMatrixToEulerAnglesXYZ() is given that
	// results in these rotation angles, which is an important property when
	// using object-space rotations in build mode.
	Matrix44f rotMat;
	ConvertRotationZXY(Vector3f(x, y, z), out(rotMat));

	m_slide = rotMat.GetRowRef(0);
	m_slide.Normalize();

	m_direction = rotMat.GetRowRef(2);
	m_direction.Normalize();

	// update the world matrix too
	Reconfig();
}

void CDynamicPlacementObj::GetYAngleFromPoint(Vector3f point, double* angle, double* magnitude) {
	float adjacent = point.x - m_position.x;
	float opposite = point.z - m_position.z;
	float anglef;
	if (adjacent == 0.0f) {
		anglef = 0.0f;
	} else if (adjacent > 0) {
		// correction for I and IV quadrant
		anglef = D3DX_PI - atan(opposite / adjacent);
	} else {
		// correction for II and III quadrant
		anglef = -1 * atan(opposite / adjacent);
	}

	// make result between 0 and 2 PI
	if (anglef < 0) {
		anglef = anglef + (2 * D3DX_PI);
	}

	*angle = (double)anglef;
	*magnitude = (double)sqrt((opposite * opposite) + (adjacent * adjacent));
}

void CDynamicPlacementObj::FlushBoundBox() {
	m_snapLocationsDirty = true;

	StreamableDynamicObject* pBaseObj = GetBaseObj();
	if (pBaseObj) {
		BNDBOX box;

		// DRF - Validate Pointers
		if (!pBaseObj || !m_pSkeleton)
			return;

		BNDBOX baseBox = pBaseObj->getBoundBox();
		Vector3f boxCenter = baseBox.GetCenter();
		Vector3f boxHalfSize = 0.5f * baseBox.GetSize();
		Matrix44f mtx = m_pSkeleton->getWorldMatrix();

		Vector3f transformedBoxHalfSize = boxHalfSize[0] * Abs(mtx.GetRowX()) + boxHalfSize[1] * Abs(mtx.GetRowY()) + boxHalfSize[2] * Abs(mtx.GetRowZ());
		Vector3f transformedBoxCenter = TransformPoint_NonPerspective(mtx, boxCenter);

		m_min = transformedBoxCenter - transformedBoxHalfSize;
		m_max = transformedBoxCenter + transformedBoxHalfSize;

		// Update object space bounding box
		m_minObjectSpace = TransformPoint_NonPerspective(m_pSkeleton->getWorldInvTransposeMatrix(), m_min); // m_WorldInvTransposeMatrix is actually only inverted but not transposed
		m_maxObjectSpace = TransformPoint_NonPerspective(m_pSkeleton->getWorldInvTransposeMatrix(), m_max);

		m_boundRadius = Sqrt_Close(transformedBoxHalfSize.LengthSquared()) * 2.01f; // Add a little extra to account for approximation of square root.
		m_boundSphereCenter = transformedBoxCenter;

		m_pSkeleton->setLocalBoundingBox(pBaseObj->getBoundBox());
	} else {
		//
		// If the base obj isn't ready, we don't have a skeleton.  Construct a transformation
		// matrix from the position.
		//
		m_min = m_minObjectSpace + m_position;
		m_max = m_maxObjectSpace + m_position;

		m_boundRadius = 0.5f * Distance(m_minObjectSpace, m_maxObjectSpace);
		m_boundSphereCenter = 0.5f * (m_min + m_max);
	}
}

const Vector3f* CDynamicPlacementObj::GetSnapLocations(int* snapLocationsCount) const {
	if (!m_pSkeleton) {
		*snapLocationsCount = 0;
		return NULL;
	}

	*snapLocationsCount = 8;

	if (m_snapLocationsDirty) {
		Vector3f min = m_boxMinLocal, max = m_boxMaxLocal;

		// Form corners of box in object space
		m_snapLocations[0] = Vector3f(min.x, min.y, min.z);
		m_snapLocations[1] = Vector3f(min.x, min.y, max.z);
		m_snapLocations[2] = Vector3f(min.x, max.y, min.z);
		m_snapLocations[3] = Vector3f(min.x, max.y, max.z);
		m_snapLocations[4] = Vector3f(max.x, min.y, min.z);
		m_snapLocations[5] = Vector3f(max.x, min.y, max.z);
		m_snapLocations[6] = Vector3f(max.x, max.y, min.z);
		m_snapLocations[7] = Vector3f(max.x, max.y, max.z);

		// Transform all corners into world space
		for (size_t i = 0; i < 8; i++) {
			m_snapLocations[i] = TransformPoint_NonPerspective(m_pSkeleton->getWorldMatrix(), m_snapLocations[i]);
		}

		m_snapLocationsDirty = false;
	}

	return m_snapLocations;
}

void CDynamicPlacementObj::MoveForward(double d) {
	MoveInDirection(d, m_direction);
}

void CDynamicPlacementObj::Slide(double d) {
	MoveInDirection(d, m_slide);
}

void CDynamicPlacementObj::Lift(double d) {
	//up vector is always (0,1,0)
	SetPosition(m_position + Vector3f(0, d, 0));
}

void CDynamicPlacementObj::SetPosition(const Vector3f& position) {
	m_position = position;
	Reconfig();
}

void CDynamicPlacementObj::SetWorldMatrix(const Matrix44f& mtx) {
	m_position = mtx.GetTranslation();
	m_slide = mtx.GetRowX();
	m_direction = mtx.GetRowZ();

	Reconfig();
}

void CDynamicPlacementObj::MoveInDirection(double d, Vector3f dir) {
	SetPosition(m_position + dir * d);
}

void CDynamicPlacementObj::MoveInDirectionRounding(double d, Vector3f dir, int numdirs) {
	dir.y = 0; // Ignore up vector this is for x and z movement only
	dir.Normalize(); // Now normalize this vector
	int degrees = asinf(dir.z) * (180 / D3DX_PI); // convert to degrees to make math easier
	float radians = degrees * (D3DX_PI / 180); // convert back to radians
	if (radians < 0) {
		radians = radians * -1; // use only first geometric quadrant
	}
	float newx = cosf(radians);
	float newz = sinf(radians);
	if (dir.x < 0) {
		newx = newx * -1; // restore origional x direction
	}
	if (dir.z < 0) {
		newz = newz * -1; // restore origional y direction
	}
	dir.x = newx;
	dir.z = newz;
	this->MoveInDirection(d, dir);
}

void CDynamicPlacementObj::SaveChanges() {
	m_positionOrig = m_position;
	m_directionOrig = m_direction;
	m_slideOrig = m_slide;
	FlushBoundBox();
}

void CDynamicPlacementObj::CancelChanges() {
	//restore original pos and orientation, cancel any changes made
	m_position = m_positionOrig;
	m_direction = m_directionOrig;
	m_slide = m_slideOrig;
	Reconfig();
}

CDynamicPlacementObj::CDynamicPlacementObj(GLID globalId, int placementId, bool fromMovementObj) :
		m_inited(false),
		m_loadErrorReported(false),
		m_baseObj(NULL),
		m_objType(DYN_OBJ_TYPE::INVALID),
		m_globalId(globalId),
		m_placementId(placementId),
		m_fromMovementObj(fromMovementObj),
		m_pResource(nullptr),
		m_pMovementObjSkeleton(nullptr),
		m_isDerivable(false),
		m_textureSet(false),
		m_pSkeleton(NULL),
		m_pLookAtInfo(),
		m_translucent(false),
		m_minDrawDistanceOverride(-1.f),
		m_gameItemId(INVALID_GAMEITEM),
		m_renderBoundingBox(false),
		m_boxMesh(NULL),
		m_boxMatrix(NULL),
		m_mediaMute(false), // drf - added
		m_mediaVideoRangeMesh(nullptr),
		m_mediaAudioRangeMesh(nullptr),
		m_mediaAudioRangeMeshRadius(0),
		m_iCollisionSuspended(0),
		m_bCollisionEnabled(true),
		m_waitForCustomTextAndApply(false),
		m_playerId(-1),
		m_assetId(0),
		m_friendId(-1),
		m_min(Vector4f(0.0f, 0.0f, 0.0f, 0.0f)),
		m_max(Vector4f(0.0f, 0.0f, 0.0f, 0.0f)),
		m_boundRadius(0.f),
		m_boundSphereCenter(Vector3f(0.0f, 0.0f, 0.0f)),
		m_customizable(true),
		m_culled(false),
		m_numVisobjs(0),
		m_outline(false),
		m_outlineColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
		m_outlineZSorted(false),
		m_highlight(false),
		m_highlightColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f)),
		m_snapLocationsDirty(true),
		m_frictionEnabled(false),
		m_skeletonLabels(new SkeletonLabels()),
		m_visibility(eVisibility::VISIBLE),
		m_objectScale(Vector3f(1.0f, 1.0f, 1.0f)),
		m_skeletonScaleBase(Vector3f(1.0f, 1.0f, 1.0f)),
		m_position(Vector3f::Zero()),
		m_direction(Vector3f::Basis<2>()),
		m_slide(Vector3f::Basis<0>()),
		m_positionOrig(m_position),
		m_directionOrig(m_direction),
		m_slideOrig(m_slide),
		m_needsUpdate(false) {
	if (IS_UGC_GLID(m_globalId) && !ContentService::Instance()->ContentMetadataCacheGetPtr(m_globalId)) {
		ContentService::Instance()->ContentMetadataCacheAsync(m_globalId, std::bind(&CDynamicPlacementObj::ContentMetadataReady, this), this);
	} else if (fromMovementObj) {
		// Wait for skeleton to be assigned by npc system.
	} else {
		ContentMetadataReady();
	}
}

CDynamicPlacementObj::~CDynamicPlacementObj() {
	CallbackSystem::Instance()->UnregisterAllCallbacksWithOwner(this);

	if (m_skeletonLabels)
		delete m_skeletonLabels;

	if (m_boxMesh)
		delete m_boxMesh;

	if (m_boxMatrix)
		delete m_boxMatrix;

	if (m_mediaVideoRangeMesh)
		delete m_mediaVideoRangeMesh;

	if (m_mediaAudioRangeMesh)
		delete m_mediaAudioRangeMesh;
}

void CDynamicPlacementObj::MovementObjectReady() {
	assert(m_fromMovementObj);
	if (!m_fromMovementObj)
		return;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	CMovementObj* pMO;
	CMovementObj* pMORef;
	if (!pICE->GetMovementObjectByPlacementId(m_placementId, pMO, pMORef)) {
		LoadError();
		return;
	}

	if (!pMO->getSkeleton()) {
		LoadError();
		return;
	}
	if (pMO->getSkeleton()->isFullyLoaded()) {
		ContentMetadataReady();
		return;
	}
	CallbackSystem::Instance()->RegisterCallback(pMO->getSkeleton(), 0, this, std::weak_ptr<void>(), [placementId = m_placementId]() {
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;
		CDynamicPlacementObj* pDPO = pICE->GetDynamicObject(placementId);
		if (pDPO)
			pDPO->ContentMetadataReady();
	});
}

void CDynamicPlacementObj::InvalidateBaseObj() {
	if (m_fromMovementObj) {
		m_pMovementObjSkeleton = nullptr; // Reference only
		m_pSkeleton.reset(); // Shared ptr to cloned skeleton

		// Destroy resource because each paper doll placement has a unique resource at the mean time. Update this block if the logic is changed.
		assert(m_pResource != nullptr);
		if (m_pResource != nullptr) {
			assert(DynamicObjectRM::Instance() != nullptr);
			DynamicObjectRM::Instance()->RemoveResource(m_pResource->GetHashKey());
		}

		m_pResource = nullptr;
		m_baseObj = nullptr; // Destroyed along with m_pResource
		m_inited = false; // Allow another initialization
	} else {
		// Regular StreamableDynamicObject placement
		// Unsupported at this moment
		assert(false);
	}
}

void CDynamicPlacementObj::BaseObjectReady() {
	//LogInfo("BaseObjectReady: " << m_globalId);
	InitBaseObj();
}

void CDynamicPlacementObj::ContentMetadataReady() {
	if (m_pResource)
		BaseObjectReady();
	else
		InitDynamicObjectResource();
}

void CDynamicPlacementObj::InitBaseObj() {
	if (m_inited)
		return;

	StreamableDynamicObject* ob = nullptr;
	if (m_pResource) {
		assert(m_baseObj == nullptr);
		ob = m_pResource->GetAsset();
		if (ob) {
			RuntimeSkeleton* skeleton = m_pMovementObjSkeleton;
			if (!skeleton) {
				auto skeletonProxy = SkeletonRM::Instance()->GetSkeletonProxy(ob->getSkeletonGLID());
				skeleton = skeletonProxy->GetRuntimeSkeleton();
			}

			if (skeleton) {
				auto pICE = IClientEngine::Instance();
				if (!pICE)
					return;

				// Instance the skeleton
				skeleton->Clone(&m_pSkeleton, nullptr, nullptr);
				skeleton->CloneArmedInventoryList(m_pSkeleton.get(), pICE->GetD3DDevice());

				m_baseObj = ob;
				InitPlacement();
				if (m_animSettings.isValid())
					setPrimaryAnimationSettings(m_animSettings);
			}
		}
	}
	if (!ob)
		LoadError();
}

void CDynamicPlacementObj::LoadError() {
	if (!m_loadErrorReported) {
		// Resource erred - send notification that this placement is failed permanently
		SendNotificationEvent<DynamicObjectLoadingFailedEvent>();
		m_loadErrorReported = true;
	}
}

void CDynamicPlacementObj::InitReMeshes() {
#ifdef _ClientOutput
	// not yet initialized
	if (m_numVisobjs == 0) {
		if (m_baseObj)
			m_numVisobjs = m_baseObj->getVisualCount();
	}

	if (m_pSkeleton)
		m_pSkeleton->initFromDynamicObject(m_baseObj, m_globalId, &m_textureColors);
#endif
}

void CDynamicPlacementObj::InitRigidBodies() {
	std::shared_ptr<Physics::CollisionShape> pCollisionShape;
	Vector3f vCollisionShapeOffset(0, 0, 0);

	if (m_fromMovementObj) {
		Physics::CollisionShapeCapsuleProperties props;
		float fHeight, fRadius;
		m_pSkeleton->getAvatarCapsuleDimensions(out(fHeight), out(fRadius));
		props.m_fCylinderHeightMeters = FeetToMeters(fHeight - 2 * fRadius); // Capsule dimensions include the endcaps, so subtract them out.
		props.m_fRadiusMeters = FeetToMeters(fRadius);
		props.m_iAxis = 1;
		pCollisionShape = Physics::CreateCollisionShape(props);

		vCollisionShapeOffset.Y() = 0.5f * props.m_fCylinderHeightMeters + props.m_fRadiusMeters;
	} else {
		pCollisionShape = m_baseObj->getCollisionShapeShared();
		vCollisionShapeOffset = FeetToMeters(m_baseObj->getCollisionShapeOffset());
	}

	auto InitRigidBody = [this](Physics::RigidBodyStatic* pRigidBody, size_t iRigidBodyIndex, uint16_t iCollisionGroup, uint16_t iCollidesWithMask) {
		if (pRigidBody) {
			pRigidBody->SetCollisionMasks(iCollisionGroup, iCollidesWithMask);
			std::shared_ptr<PhysicsBodyData> pPhysicsBodyData = std::make_shared<PhysicsBodyData>(ObjectType::DYNAMIC, m_placementId, this, iRigidBodyIndex);
#if defined(_DEBUG)
			pPhysicsBodyData->m_glid = this->m_globalId;
			pPhysicsBodyData->m_pszName = m_baseObj->getName().c_str();
			pPhysicsBodyData->m_pDynObj = this;
#endif
			pRigidBody->SetUserData(std::move(pPhysicsBodyData));
		}
	};

	Physics::RigidBodyStaticProperties rbprops;
	m_pRigidBody = Physics::CreateRigidBodyStatic(rbprops, pCollisionShape.get(), vCollisionShapeOffset, 1.0f);
	InitRigidBody(m_pRigidBody.get(), 0, CollisionGroup::Object | CollisionGroup::Visible, CollisionMask::Object);
	m_pRigidBodyVisual = Physics::CreateRigidBodyStatic(rbprops, m_baseObj->getVisualShapeShared().get(), FeetToMeters(m_baseObj->getVisualShapeOffset()), 1.0f);
	InitRigidBody(m_pRigidBodyVisual.get(), 1, CollisionGroup::Visible, CollisionGroup::None);

	UpdateRigidBodyPosition();
}

void CDynamicPlacementObj::InitMedia() {
	// Media Supported ?
	if (!MediaSupported())
		return;

	// Set Flash Games Media Radius (1' audio 10' video)
	if (IsInteractive())
		SetMediaRadius(MediaRadius(1.0, 10.0));

	// Add To Media Placements (begins updates)
	auto pICE = IClientEngine::Instance();
	if (pICE)
		pICE->UpdateDynamicObjectInSubList(this, DynObjSubListId_HasMedia);
}

void CDynamicPlacementObj::SetMaterial(int index, CMaterialObject* matToCopy) {
	assert(m_baseObj != nullptr && m_pSkeleton != nullptr);
	assert(matToCopy != nullptr);
	assert(index < m_baseObj->getVisualCount());
	if (m_baseObj != nullptr && m_pSkeleton != nullptr && matToCopy != nullptr && index < m_baseObj->getVisualCount()) {
		if (m_customMaterials.empty()) {
			m_customMaterials.resize(m_baseObj->getVisualCount());
		}
		CMaterialObject* pMaterial = nullptr;
		matToCopy->Clone(&pMaterial);
		m_pSkeleton->SetMaterialObject(index, pMaterial, true, &m_textureColors);
		m_customMaterials[index] = std::unique_ptr<CMaterialObject>(pMaterial);
	}
}

CMaterialObject* CDynamicPlacementObj::GetMaterial(int id) {
	return m_pSkeleton ? m_pSkeleton->GetMaterialObject(id) : nullptr;
}

std::string CDynamicPlacementObj::GetCustomTextureUrl() const {
	return m_customTexture ? m_customTexture->getUrl(0, false) : std::string();
}

void CDynamicPlacementObj::ResetCustomTexture() {
	m_customTexture.reset();

	// Reset materials
	if (m_baseObj && m_pSkeleton) {
		m_pSkeleton->resetCustomTexture();

		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return;
		TextureDatabase* textureDb = pICE->GetTextureDataBase();
		m_pSkeleton->updateCustomMaterials2(textureDb, &m_textureColors, CMaterialObject::NO_CUSTOM_TEXTURE, m_baseObj->GetVisualCustomizableFlags());
	}
}

void CDynamicPlacementObj::UpdateCustomMaterials(bool chainLoad) {
#ifdef _ClientOutput
	// Texture download event arrived before InitPlacement is called.
	// GetBaseObj will trigger InitPlacement and subsequently calls updateCustomMaterials
	// again (if the object is ready). Return now and wait until InitPlacement to be
	// called in other places. It will prevent this function from getting executed twice.
	// We should probably try not to download textures until objects are ready.
	if (!m_inited || !m_baseObj || !m_pSkeleton)
		return;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	auto pTexDB = pICE->GetTextureDataBase();
	if (!pTexDB)
		return;

	// forward cached texture overrides to skeleton
	if (m_customTexture)
		m_pSkeleton->setCustomTexture(m_customTexture);

	if (m_ugcItemTexture)
		m_pSkeleton->setUGCItemTexture(m_ugcItemTexture);

	int customTextureOption = IS_UGC_GLID(m_globalId) ? CMaterialObject::CUSTOM_ALL_TEXTURE_SLOTS : CMaterialObject::CUSTOM_FIRST_TEXTURE_SLOT;
	m_pSkeleton->updateCustomMaterials2(pTexDB, &m_textureColors, customTextureOption, m_baseObj->GetVisualCustomizableFlags());

	m_textureHashes.clear();
	m_pSkeleton->IterateMaterials([this](CMaterialObject* pMaterial) {
		// Scan all materials to gather textures
		for (size_t texIdx = 0; texIdx < NUM_CMATERIAL_TEXTURES; texIdx++) {
			DWORD nameHash = pMaterial->m_texNameHash[texIdx];
			if (nameHash != 0 && nameHash != (DWORD)-1) {
				m_textureHashes.insert(nameHash);
			}
		}
	});

	// Load Texture Outside Frustum ?
	if (IsVisible()) {
		// Trigger all textures for loading - this overrides the old behavior where only textures
		// within view frustum would be queued for loading, when requested by render engine.
		double camDist = m_pResource ? m_pResource->GetCamDistance() : 0.0;
		for (DWORD nameHash : m_textureHashes) {
			CTextureEX* pTex = pTexDB->TextureGet(nameHash);
			if (!pTex)
				continue;

			// Initial resource distance to same value as its owner
			pTex->InitCamDistance(camDist); // Texture specific - InitCamDistance() will only execute once to prevent texture's cam distance from being reset back to default

			// Trigger texture loading / downloading
			// ED-7733 - Load all DO textures before you look at them
			// NOTE - Only loads all textures if chainLoad is true which only occurs when progress menu is closed from ClientEngine::OnZoneLoadingUIActiveChange(false)
			pTex->getTexturePtr(chainLoad);
		}
	}
#endif //_ClientOutput
}

void CDynamicPlacementObj::StartPivot(TimeMs moveDuration, TimeMs orientDuration, TimeMs accDur, TimeMs constVelDur, double accDistFrac, double constVelDistFrac) {
	if (!m_pSkeleton)
		return;

	TimeMs startTime = fTime::TimeMs();
	Matrix44f mtxTarget = updateNextFrameMatrix();
	if (moveDuration > 0) {
		if (!m_pPivotTransitionPosition) {
			m_pPivotTransitionPosition = std::make_unique<PivotTransitionPosition>();
			UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
		}
		m_pPivotTransitionPosition->startPivotToPosition(startTime, moveDuration, accDur, constVelDur, accDistFrac, constVelDistFrac, m_pSkeleton->getWorldMatrix(), mtxTarget);
	} else if (moveDuration == 0) {
		m_pPivotTransitionPosition.reset();
	} else {
		// no change requested to position pivot.
	}

	if (orientDuration > 0) {
		if (!m_pPivotTransitionRotation) {
			m_pPivotTransitionRotation = std::make_unique<PivotTransitionRotation>();
			UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
		}
		m_pPivotTransitionRotation->startPivotToOrientation(startTime, orientDuration, m_pSkeleton->getWorldMatrix(), mtxTarget);
	} else if (orientDuration == 0) {
		m_pPivotTransitionRotation.reset();
	} else {
		// no change requested to orientation pivot.
	}
}

void CDynamicPlacementObj::Relocate(const Vector3f& position, DWORD duration, DWORD accDur, DWORD constVelDur, double accDistFrac, double constVelDistFrac) {
	m_position = position;

	StartPivot(duration, -1, accDur, constVelDur, accDistFrac, constVelDistFrac);

	Reconfig();

	//backup position so the changes made by user can be cancelled
	m_positionOrig = m_position;
}

void CDynamicPlacementObj::Relocate(const Vector3f& direction, const Vector3f& slide, DWORD duration) {
	m_direction = direction.GetNormalized();
	m_slide = slide.GetNormalized();
	StartPivot(-1, duration);

	Reconfig();

	// backup orientation, so the changes made by user can be cancelled
	m_directionOrig = m_direction;
	m_slideOrig = m_slide;
}

void CDynamicPlacementObj::Relocate(const Vector3f& position, DWORD moveDuration, const Vector3f& direction, const Vector3f& slide, DWORD orientDuration) {
	m_position = position;
	m_direction = direction.GetNormalized();
	m_slide = slide.GetNormalized();
	StartPivot(moveDuration, orientDuration, 0, moveDuration, 0, 1);

	Reconfig();

	//backup position and orientation, so the changes made by user can be cancelled
	m_positionOrig = m_position;
	m_directionOrig = m_direction;
	m_slideOrig = m_slide;
}

void CDynamicPlacementObj::GetBoundingBox(Vector4f& min, Vector4f& max) {
	min.SubVector<3>() = m_min;
	max.SubVector<3>() = m_max;
}

void CDynamicPlacementObj::GetBoundingBoxAnimAdjust(Vector4f& min, Vector4f& max) {
	if (m_animSettings.isValid() && m_pSkeleton && m_pSkeleton->getBoneCount() > 0 && m_pSkeleton->getBoneList()->getBoneMatrices() != nullptr) {
		// Use bounding box center for pivot reference
		Vector3f pivotOffset((m_minObjectSpace + m_maxObjectSpace) / 2);
		// Transform reference point by animation
		pivotOffset = TransformPoint_NonPerspective(m_pSkeleton->getBoneList()->getBoneMatrices()[0], pivotOffset);
		pivotOffset = TransformPoint_NonPerspective(m_pSkeleton->getWorldMatrix(), pivotOffset);
		// Calculate offset from original pivot location
		pivotOffset -= (m_min + m_max) / 2;

		// Offset skin bounding box by pivot offset
		min.SubVector<3>() = m_min + pivotOffset;
		max.SubVector<3>() = m_max + pivotOffset;
		return;
	}

	// if no skeleton or bones return default bound box.  This shouldn't happen unless skeleton is streaming
	// or something weird.
	min.SubVector<3>() = m_min;
	max.SubVector<3>() = m_max;
}

bool CDynamicPlacementObj::IntersectRay(const Vector3f& origin, const Vector3f& direction, float* distance, Vector3f* location, Vector3f* normal, bool testAlpha) {
	if (!GetBaseObj())
		return false;

	bool intersectFound = false;
	float closestDist = FLT_MAX;
	for (size_t i = 0; i < GetBaseObj()->getVisualCount(); i++) {
		StreamableDynamicObjectVisual* visual = GetBaseObj()->Visual(i);
		if (visual) {
			/* don't test alpha-blended geometry unless specifically asked to */
			CMaterialObject* cmat = visual->getMaterial();

			if (testAlpha || !cmat || cmat->m_blendMode == CMaterialObject::BlendMode::BLEND_DISABLED) {
				// use the reference library version of the mesh ...
				// the ReLODMesh array isn't always safe to use because
				// the meshes can be pointing to freed materials sometimes
				UINT lod = m_pSkeleton->getCurrentLOD();
				ReMesh* reMesh = visual->getReMesh(lod);
				// This is what I'd like to do, but we're not
				// serializing the 'mesh to use in collisions'
				// and the getCollisionReMesh function doesn't take
				// a LOD.
				if (reMesh) {
					/* compute world matrix for visual */
					Matrix44f matOffset = visual->getRelativeTransform(), matVisual, matVisualInv;

					// DRF - Validate Pointers
					if (!m_pSkeleton)
						continue;

					matVisual = m_pSkeleton->getWorldMatrix() * matOffset;

					/* invert visual matrix to get from world space to visual space */
					bool bValid = matVisualInv.MakeInverseOf(matVisual);

					if (bValid) {
						Vector3f osOrigin, osDirection;
						Vector4f tmp;

						/* transform ray origin */
						osOrigin = TransformPoint_NonPerspective(matVisualInv, origin);

						/* transform and normalize ray direction */
						osDirection = TransformVector(matVisualInv, direction);
						osDirection.Normalize();

						/* check for intersection */
						float dist = FLT_MAX;

						// The values that need to be scaled come from here.  Rather than try
						// to dig into charles land, I'll see if I can adjust the output.
						if (reMesh->IntersectRay(osOrigin, osDirection, &dist, location)) {
							intersectFound = true;

							if (dist < closestDist)
								closestDist = dist;
						}
					}
				}
			}
		}
	}

	if (GetBaseObj()->getVisualCount() == 0) {
		if (m_pSkeleton && m_pSkeleton->intersectRay(origin, direction, location)) {
			closestDist = sqrt(pow(origin.x - location->x, 2) +
							   pow(origin.y - location->y, 2) +
							   pow(origin.z - location->z, 2));
			intersectFound = true;
		}
	}

	if (intersectFound && m_pSkeleton) {
		if (distance) {
			// Correct for scaling.  ASSUME UNIFORM OBJECT SCALING
			float scaleModifier = (m_pSkeleton->getScaleVector().x +
									  m_pSkeleton->getScaleVector().y +
									  m_pSkeleton->getScaleVector().z) /
								  3.0f;
			*distance = closestDist * scaleModifier;
		}

		if (location && distance) {
			*location = origin + *distance * direction;
		}
	}

	return intersectFound;
}

bool CDynamicPlacementObj::IntersectSphere(const Vector3f& origin, float radius, const Vector3f& orientation, float maxAngle, float* distance, Vector3f* location, Vector3f* normal, bool testAlpha) {
	if (!GetBaseObj())
		return false;

	bool intersectFound = false;
	float closestDist = FLT_MAX;
	for (size_t i = 0; i < GetBaseObj()->getVisualCount(); i++) {
		StreamableDynamicObjectVisual* visual = GetBaseObj()->Visual(i);
		if (visual) {
			// don't test alpha-blended geometry unless specifically asked to
			CMaterialObject* cmat = visual->getMaterial();

			if (testAlpha || !cmat || cmat->m_blendMode == -1) {
				// use the reference library version of the mesh ...
				// the ReLODMesh array isn't always safe to use because
				// the meshes can be pointing to freed materials sometimes
				UINT lod = m_pSkeleton->getCurrentLOD();
				ReMesh* reMesh = visual->getReMesh(lod);

				// This is what I'd like to do, but we're not
				// serializing the 'mesh to use in collisions'
				// and the getCollisionReMesh function doesn't take
				// a LOD.
				//ReMesh *reMesh = visual->getCollisionReMesh(lod);
				if (reMesh) {
					// Compute world matrix for visual
					Matrix44f matOffset = visual->getRelativeTransform(), matVisual, matVisualInv;

					// DRF - Validate Pointers
					if (!m_pSkeleton)
						continue;

					matVisual = m_pSkeleton->getWorldMatrix() * matOffset;

					/* invert visual matrix to get from world space to visual space */
					bool bValid = matVisualInv.MakeInverseOf(matVisual);

					if (bValid) {
						Vector3f osOrigin, osOrientation;
						float osRadius;
						Vector4f tmp;

						// transform sphere origin
						osOrigin = TransformPoint_NonPerspective(matVisualInv, origin);

						// Scale the radius by the longest scaled axis.
						float len0 = matVisual[0].SubVector<3>().LengthSquared();
						float len1 = matVisual[1].SubVector<3>().LengthSquared();
						float len2 = matVisual[2].SubVector<3>().LengthSquared();
						float lenToUse = std::max(std::max(len0, len1), len2);
						lenToUse = sqrt(lenToUse);
						osRadius = radius / lenToUse;

						// transform and normalize orientation
						osOrientation = TransformVector(matVisualInv, orientation);
						osOrientation.Normalize();

						// check for intersection
						float dist = FLT_MAX;
						Vector3f hitLoc;

						// The values that need to be scaled come from here.  Rather than try
						// to dig into charles land, I'll see if I can adjust the output.
						if (reMesh->IntersectSphere(osOrigin, osRadius, osOrientation, maxAngle, &dist, &hitLoc)) {
							intersectFound = true;

							// Transform the distance back into original sphere-space.
							dist *= lenToUse;

							if (dist < closestDist) {
								closestDist = dist;

								if (distance)
									*distance = dist;

								if (location) {
									*location = TransformPoint_NonPerspective(matVisual, hitLoc);
								}
							}
						}
					}
				}
			}
		}
	}

	return intersectFound;
}

void CDynamicPlacementObj::SetCollisionEnabled(bool bEnabled) {
	if (m_bCollisionEnabled == bEnabled)
		return;

	m_bCollisionEnabled = bEnabled;
	UpdateRigidBodyCollidableState();
}

ReMesh* CDynamicPlacementObj::GetReLODMesh(UINT visIndex, UINT lod) {
	// This breaks DO LODs
	return (m_pSkeleton && lod < m_maxLODs && visIndex < m_numVisobjs) ? m_pSkeleton->getLODMesh(visIndex, lod) : nullptr;
}

void CDynamicPlacementObj::InitPlacement() {
	if (!m_baseObj || !m_pSkeleton)
		return;

	m_inited = true;

	AssignObjType();

	ContentMetadata* pMetaData = ContentService::Instance()->ContentMetadataCacheGetPtr(m_globalId);
	if (pMetaData)
		m_textureColors = pMetaData->getTextureColors();

	// DRF - Only Used For Dance Floors!
	CTriggerObjectList* pTriggerList = m_baseObj->getTriggers();
	if (pTriggerList && !pTriggerList->IsEmpty()) {
		m_baseObj->getTriggers()->ForEachTrigger([this](CTriggerObject* pTriggerObject) {
			PrimitiveGeomArgs<float> volumeArgs;
			volumeArgs.type = PrimitiveType::Sphere;
			volumeArgs.sphere.radius = pTriggerObject->GetSphereRadius();
			auto pICE = IClientEngine::Instance();
			if (!pICE)
				return;
			auto pTO = pICE->AddDynamicObjectTrigger(
				this,
				1,
				pTriggerObject->GetTriggerAction(),
				volumeArgs,
				pTriggerObject->GetIntParam(),
				pTriggerObject->GetStrParam());
			if (!pTO)
				return;
			pTO->SetAttachGlid(m_globalId);
			pTO->SetInTrigger(true);
			pTO->SetPosition(m_position);
		});
	}

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// copy data from base object, must delete
	if (m_baseObj->getLight()) {
		m_pLight = m_baseObj->createLegacyLight();
		pICE->UpdateDynamicObjectLight(this);
	}

	// Save skeleton base scale factor
	m_pSkeleton->setScaleVector(m_pSkeleton->getScaleVector() * m_baseObj->getSkeletonAdditionalScale()); // Apply additional scale (component-wise multiply)
	m_skeletonScaleBase = m_pSkeleton->getScaleVector();

	// Initialize world matrix for skeleton
	m_pSkeleton->scaleSystem(m_objectScale.x * m_skeletonScaleBase.x, m_objectScale.y * m_skeletonScaleBase.y, m_objectScale.z * m_skeletonScaleBase.z);
	Matrix44f mtx = updateNextFrameMatrix();
	m_pSkeleton->setWorldMatrix(&mtx, false);

	InitReMeshes();

	InitRigidBodies();

	InitMedia();

	// We should probably merge this with InitReMeshes, as well as similar logic in UpdateDynamicObjectUGCEventHandler
	if (m_customTexture || m_ugcItemTexture)
		UpdateCustomMaterials();

	// Apply texture panning settings recorded before InitPlacement()
	ApplyTexturePanningSettings();

	if (m_baseObj) {
		BNDBOX bboxLocal = m_baseObj->computeBoundingBox();
		m_boxMinLocal = bboxLocal.min;
		m_boxMaxLocal = bboxLocal.max;
	} else {
		m_boxMinLocal = Vector3f(0, 0, 0);
		m_boxMaxLocal = Vector3f(0, 0, 0);
	}

	FlushBoundBox();

	// Apply per-placement labels to skeleton
	m_pSkeleton->getOverheadLabels()->addLabels(*m_skeletonLabels);

	// Apply base object particle
	int stockParticleId = m_baseObj->getParticleEffectId();
	if (stockParticleId != -1)
		m_pSkeleton->addParticleByStockIndex(std::string(), 0, stockParticleId);

	// Apply pending particles
	for (const auto& ppa : m_pendingParticleAdds)
		m_pSkeleton->addParticle(ppa.boneName, ppa.particleSlotId, ppa.particleGLID, -1, ppa.particleEffectName, ppa.offset, ppa.dir, ppa.up);
	m_pendingParticleAdds.clear();

	if (m_translucent)
		MakeTranslucent();

	ApplyCurrentHighlight();

	Reconfig();

	SendNotificationEvent<DynamicObjectInitializedEvent>();
}

bool CDynamicPlacementObj::IsBaseObjReady() {
	if (!IsResourceReady()) { // If from an IResource, check resource loading state
		return false;
	}
	return (GetBaseObj() != NULL);
}

TResource<StreamableDynamicObject>* CDynamicPlacementObj::GetDynamicObjectResource(GLID globalId) {
	UINT64 resKey = (UINT64)globalId; // Non-UGC items use global ID for resource key

	bool bLocalResource = IS_LOCAL_GLID(globalId);
	if (!bLocalResource && IS_UGC_GLID(globalId)) {
		// Pre-screen metadata readiness for UGC items
		IClientEngine* pICE = IClientEngine::Instance();
		if (!pICE)
			return nullptr;

		ContentMetadata md;
		if (!pICE->GetCachedUGCMetadata(globalId, md)) {
			// Wait until metadata are available
			return nullptr;
		}

		// Metadata ready: ok to create IResource.
		// ResourceManager::QueueResource will pick up rest of the metadata later.

		// Determine resource key
		std::string _assetUrl, _fileHash;
		FileSize _fileSize = 0;
		unsigned uniqueId = 0;
		md.getPrimaryAsset(_assetUrl, _fileSize, _fileHash, uniqueId);

		if (uniqueId != 0) {
			resKey = ResourceManager::GetResourceKeyByUniqueId(uniqueId); // If it has a unique ID, use it
		} else if (md.getBaseGlobalId() != 0) {
			resKey = (UINT64)md.getBaseGlobalId(); // Unique not available - use base global ID for resource key
		}
	}

	TResource<StreamableDynamicObject>* pResource = AddResourceToManager<TResource<StreamableDynamicObject>>(resKey, globalId, bLocalResource);

	return pResource;
}

TResource<StreamableDynamicObject>* CDynamicPlacementObj::GetDynamicObjectResourceFromMovementObj(GLID globalId, int placementId, RuntimeSkeleton*& pSkeleton) {
	assert(IS_LOCAL_GLID(globalId));
	TResource<StreamableDynamicObject>* pResource = GetDynamicObjectResource(globalId);
	assert(pResource != nullptr);

	if (pResource->StateIsLoadedAndCreated() || pResource->StateIsErrored()) {
		// Ready or erred
		return pResource;
	}

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return nullptr;

	CMovementObj *pMO, *pMORM;
	pICE->GetMovementObjectByPlacementId(placementId, pMO, pMORM);
	if (!pMO) {
		// Not spawned yet
		return nullptr;
	}

	if (!pMO->hasSkeleton()) {
		LogError("Movement object does not have a skeleton: " << pMO->ToStr());
		pResource->SetState(IResource::ERRORED);
		return nullptr;
	}

	// We need both the main skeleton and the head to be ready
	if (!pMO->getSkeleton()->areAllMeshesLoaded() || pMO->getSkeleton()->getNumPendingEquippableItems() > 0) {
		// Assets not ready
		return nullptr;
	}

	// Now clone the skeleton into dynamic object
	if (!pMORM) {
		LogError("GetMovementObjByPlacementId failed: " << pMO->ToStr());
		pResource->SetState(IResource::ERRORED);
		return nullptr;
	}

	assert(pMO->getSkeletonConfig() != nullptr);
	auto pCharConfig = pMO->getSkeletonConfig() ? &pMO->getSkeletonConfig()->m_charConfig : nullptr;

	assert(pMORM->hasSkeleton());
	if (!pMORM->hasSkeleton()) {
		LogError("Reference movement object does not have a skeleton: " << pMORM->ToStr());
		pResource->SetState(IResource::ERRORED);
		return nullptr;
	}

	GLID skeletonGLID = ResourceManager::AllocateLocalGLID();

	pSkeleton = pMO->getSkeleton();
	pSkeleton->sortMeshCache();

	// Hide CMovementObj instance of the NPC
	pMO->setVisible(false);

	// Create new local dynamic object
	auto pDynObj = std::make_unique<StreamableDynamicObject>();
	pDynObj->setName("Actor based obj");
	pDynObj->setLinkedMovObjNetTraceId(pMO->getNetTraceId());

	for (int i = 0; i < pMORM->getSkeleton()->getSkinnedMeshCount(); i++) {
		POSITION dbPos = pMORM->getSkeletonConfig()->m_meshSlots->FindIndex(i);
		CAlterUnitDBObject* db = (CAlterUnitDBObject*)pMORM->getSkeletonConfig()->m_meshSlots->GetAt(dbPos);
		if (db) {
			int meshIndex = 0;
			int matIndex = 0;
			if (pCharConfig) {
				CharConfigItem* pConfigItem = pCharConfig->getItemByGlidOrBase(GOB_BASE_ITEM);
				assert(pConfigItem != nullptr);
				CharConfigItemSlot* pConfigSlot = pConfigItem->getSlot(i);
				assert(pConfigSlot != nullptr);
				meshIndex = pConfigSlot->m_meshId;
				matIndex = pConfigSlot->m_matlId;
			}

			POSITION cPos = db->m_configurations->FindIndex(meshIndex);
			CDeformableMesh* config = nullptr;
			if (cPos != nullptr) {
				config = (CDeformableMesh*)db->m_configurations->GetAt(cPos);
				assert(config != nullptr);
			}

			StreamableDynamicObjectVisual* vis = new StreamableDynamicObjectVisual();
			vis->setName(db->m_name);
			if (config != nullptr && config->m_lodHashes != nullptr) {
				vis->addLOD(config->m_lodHashes[0]); // TODO: DO should use all LODs from movement obj
			}
			vis->setUsePersistentMeshPool(true);

			if (config != nullptr && config->m_supportedMaterials != nullptr) {
				POSITION mPos = config->m_supportedMaterials->FindIndex(matIndex);
				if (mPos != nullptr) {
					CMaterialObject* mat = (CMaterialObject*)config->m_supportedMaterials->GetAt(mPos);
					vis->setMaterial(mat);

					// Turn off pre-lighting
					assert(vis->getMaterial() && vis->getMaterial()->GetReMaterial());
					if (vis->getMaterial() && vis->getMaterial()->GetReMaterial()) {
						vis->getMaterial()->GetReMaterial()->PreLight(FALSE);
					}
				}
			}
			pDynObj->AddVisual(vis, false, false);
		}
	}

	pDynObj->setSkeletonGLID(skeletonGLID);
	pDynObj->setBoundBox(pDynObj->computeBoundingBox());

	pResource->SetAsset(pDynObj.release());
	return pResource;
}

void CDynamicPlacementObj::InitDynamicObjectResource() {
	if (m_pResource) {
		BaseObjectReady();
		return;
	}

	m_pResource = m_fromMovementObj ? GetDynamicObjectResourceFromMovementObj(m_globalId, m_placementId, m_pMovementObjSkeleton) : GetDynamicObjectResource(m_globalId);
	if (!m_pResource || m_pResource->StateIsErrored()) {
		LoadError();
		return;
	}

	if (m_pResource->StateIsLoadedAndCreated()) {
		BaseObjectReady();
		return;
	}

	DynamicObjectRM::Instance()->QueueResource(m_pResource, m_globalId);

	if (!m_pResource->LoadStarted())
		return;

	// Since CDynamicPlacementObj is not a shared pointer, we need to create a substitute callback owner
	// and verify that this CDynamicPlacementObj still exists before modifying it.
	std::shared_ptr<CDynamicPlacementObj> pCallbackOwner(this, [](void*) {});
	int iPlacementId = this->GetPlacementId();
	DynamicObjectRM::Instance()->RegisterResourceLoadedCallback(
		m_pResource->GetHashKey(),
		pCallbackOwner,
		[this, pCallbackOwner, iPlacementId]() {
			auto pICE = IClientEngine::Instance();
			if (!pICE)
				return;
			CDynamicPlacementObj* pDPO = pICE->GetDynamicObject(iPlacementId);
			if (pDPO == this) // Make sure placement has not been deleted.
				this->BaseObjectReady();
		});
}

void CDynamicPlacementObj::getInUseTextures(std::map<DWORD, CStringA>& outTextures) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	for (size_t i = 0; i < GetBaseObj()->getVisualCount(); i++) {
		StreamableDynamicObjectVisual* visualPtr = (StreamableDynamicObjectVisual*)GetBaseObj()->Visual(i);
		if (visualPtr) {
			CMaterialObject* matPtr = GetMaterial(i);
			if (matPtr) {
				// multitexturing support
				for (size_t j = 0; j < NUM_CMATERIAL_TEXTURES; j++) {
					if (matPtr->m_texNameHash[j] != -1 && matPtr->m_texNameHash[j] != 0) {
						bool newTex = true;
						for (size_t k = 0; k < outTextures.size(); k++) {
							if (outTextures.find(matPtr->m_texNameHash[j]) != outTextures.end()) {
								newTex = false;
							}
						}
						if (newTex) {
							CTextureEX* tempTex = const_cast<CTextureEX*>(pICE->GetTextureDataBase()->TextureGet(matPtr->m_texNameHash[j]));
							if (tempTex && !tempTex->IsErrored()) {
								outTextures.insert(std::make_pair(matPtr->m_texNameHashOriginal[j], tempTex->GetFileName().c_str()));
							}
						}
					}
				}
			}
		}
	}
}

bool CDynamicPlacementObj::SaveTemplateTextures(CStringA folder) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	std::map<DWORD, CStringA> textureFileNames;
	std::vector<CTextureEX*> textureExs;

	if (!GetBaseObj())
		return false; // fail if the dynamic object is not currently loaded

	if (GetBaseObj()->getVisualCount() < 1)
		return false; // fail if dynamic object has no visuals

	// can't save templates for local or things that are not customizable
	if (!IsDerivable())
		return false; // must not be local and must be customizable

	getInUseTextures(textureFileNames);

	for (auto it = textureFileNames.begin(); it != textureFileNames.end(); ++it) {
		CTextureEX* tempTex = const_cast<CTextureEX*>(pICE->GetTextureDataBase()->TextureGet(it->second.GetString()));
		if (!tempTex || !tempTex->StateIsLoadedAndCreated()) {
			// I don't understand why I have to call getData this should already be loaded... Steve?
			if (tempTex->GetD3DTextureIfLoadedOrQueueToLoad(true) == nullptr) {
				return false; // fail if any of those textures are not currently loaded
			}
		} else {
			textureExs.push_back(tempTex);
		}
	}

	int i = 0;
	for (auto it = textureFileNames.begin(); it != textureFileNames.end(); ++it, ++i) {
		it->second.Replace(".dds", ".jpg");
		it->second.Replace(".DDS", ".jpg");
		it->second.Replace(".tga", ".jpg");
		it->second.Replace(".TGA", ".jpg");
		it->second.Replace(".dat", ".jpg");
		if (D3DXSaveTextureToFileW(Utf8ToUtf16(folder + "\\" + it->second).c_str(), D3DXIFF_JPG, textureExs[i]->GetD3DTextureIfLoadedOrQueueToLoad(), NULL) != D3D_OK) {
			return false;
		}
	}
	return true;
}

// DRF - DEAD CODE ?
bool CDynamicPlacementObj::AssignObjectToTemplate(CStringA folder) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	auto pTDB = pICE->GetTextureDataBase();
	if (!pTDB)
		return false;

	if (!GetBaseObj())
		return false;

	for (size_t i = 0; i < GetBaseObj()->getVisualCount(); i++) {
		StreamableDynamicObjectVisual* visualPtr = (StreamableDynamicObjectVisual*)GetBaseObj()->Visual(i);
		if (visualPtr) {
			CMaterialObject* newMat = NULL;
			CMaterialObject* oldMat = GetMaterial(i);
			if (!oldMat)
				continue;

			oldMat->Clone(&newMat);
			if (newMat) {
				// Add All Material Textures To Texture Database
				for (size_t j = 0; j < NUM_CMATERIAL_TEXTURES; j++) {
					// Skip Empty Texture Slots (nameHash=0)
					DWORD nameHash = newMat->m_texNameHash[j];
					if (nameHash == 0 || !ValidTextureNameHash(nameHash))
						continue;

					TextureDatabase* texDb = pICE->GetTextureDataBase();
					if (!texDb)
						continue;

					// get active texture
					CTextureEX* pTex = texDb->TextureGet(nameHash);
					if (!pTex)
						continue;

					// Replace Extension With '.jpg' (assuming we don't use .jpg in patch data or custom texture cache)
					std::string texFileName = pTex->GetFileName();
					if (StrSameFileExt(texFileName, "dat") || StrSameFileExt(texFileName, "dds") || StrSameFileExt(texFileName, "tga")) {
						texFileName = StrStripFileExt(texFileName) + ".jpg";
					}
					std::string subDir = (const char*)folder;

					// Add Texture To Texture Database
					nameHash = pTDB->TextureAdd(newMat->m_texNameHashStored[j], texFileName, subDir);
					if (!ValidTextureNameHash(nameHash)) {
						LogError("AddTextureToDatabase() FAILED - texName='" << texFileName << "'");
					} else
						newMat->m_texNameHash[j] = nameHash;
				}
				SetMaterial(i, newMat);
				delete newMat;
			}
		}
	}
	return true;
}

void CDynamicPlacementObj::setPrimaryAnimationSettings(const AnimSettings& animSettings) {
	m_animSettings = animSettings;

	if (!m_pSkeleton || !m_pSkeleton->getBoneList())
		return;

	// Set Animation Settings
	m_pSkeleton->setSecondaryAnimation(GLID_INVALID);
	m_pSkeleton->setPrimaryAnimationSettings(animSettings);
}

bool CDynamicPlacementObj::AddSkeletonLabel(const std::string& text, double fontSize, double r, double g, double b) {
	const Vector3d color(r, g, b);
	if (m_skeletonLabels)
		m_skeletonLabels->addLabel(text, fontSize, color);

	if (m_pSkeleton && m_pSkeleton->getOverheadLabels())
		m_pSkeleton->getOverheadLabels()->addLabel(text, fontSize, color);

	return true;
}

bool CDynamicPlacementObj::AddSkeletonLabels(const SkeletonLabels& labels) {
	if (m_skeletonLabels)
		m_skeletonLabels->addLabels(labels);

	if (m_pSkeleton && m_pSkeleton->getOverheadLabels())
		m_pSkeleton->getOverheadLabels()->addLabels(labels);

	return true;
}

bool CDynamicPlacementObj::ResetSkeletonLabels() {
	if (m_skeletonLabels)
		m_skeletonLabels->reset();

	if (m_pSkeleton && m_pSkeleton->getOverheadLabels())
		m_pSkeleton->getOverheadLabels()->reset();

	return true;
}

void CDynamicPlacementObj::SetSkeletonConfiguration(std::unique_ptr<SkeletonConfiguration> pSkelCfg) {
	m_pSkeletonConfiguration = std::move(pSkelCfg);
}

void CDynamicPlacementObj::SetTexturePanning(USHORT meshId, USHORT uvSetId, float uIncr, float vIncr) {
	if (uvSetId >= MAX_UV_SETS)
		return;

	UINT key = (meshId << 16) + uvSetId;

	m_texturePanningSettings[key].x = uIncr;
	m_texturePanningSettings[key].y = vIncr;

	if (m_inited)
		ApplyTexturePanningSettings(meshId, uvSetId);
}

void CDynamicPlacementObj::ApplyTexturePanningSettings(USHORT meshIdFilter, USHORT uvSetIdFilter) {
	if (m_pSkeleton)
		m_pSkeleton->applyTexturePanningSettings(m_texturePanningSettings, meshIdFilter, uvSetIdFilter);
}

bool CDynamicPlacementObj::SetVisibility(eVisibility visibility) {
	if (m_visibility == visibility)
		return true;
	m_visibility = visibility;
	UpdateRigidBodyCollidableState();
	UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
	return true;
}

bool CDynamicPlacementObj::AddParticle(const std::string& boneName, int particleSlotId, UINT particleGLID, const std::string& particleEffectName, const Vector3f& offset, const Vector3f& dir, const Vector3f& up) {
	if (m_inited) {
		if (!m_pSkeleton)
			return false;
		return m_pSkeleton->addParticle(boneName, particleSlotId, particleGLID, -1, particleEffectName, offset, dir, up);
	} else {
		//Not fully loaded, record in pending particle list
		m_pendingParticleAdds.push_back(PendingParticleAdd(boneName, particleSlotId, particleGLID, particleEffectName, offset, dir, up));
		return true;
	}
}

void CDynamicPlacementObj::RemoveParticle(const std::string& boneName, int particleSlotId) {
	if (m_inited) {
		if (!m_pSkeleton)
			return;
		m_pSkeleton->removeParticle(boneName, particleSlotId);
	} else {
		for (auto it = m_pendingParticleAdds.begin(); it != m_pendingParticleAdds.end(); ++it) {
			if (it->boneName == boneName && it->particleSlotId == particleSlotId) {
				m_pendingParticleAdds.erase(it);
				break;
			}
		}
	}
}

bool CDynamicPlacementObj::GetParticleGlidAndName(const std::string& boneName, int particleSlotId, GLID& particleGlid, std::string& name) {
	particleGlid = 0;
	name.clear();

	if (m_inited) {
		if (!m_pSkeleton)
			return false;
		auto pICE = IClientEngine::Instance();
		if (!pICE)
			return false;
		ParticleRM* pParticleRM = pICE->GetParticleDatabase();
		if (!pParticleRM)
			return false;
		UINT particleId = m_pSkeleton->getParticleRuntimeId(boneName, particleSlotId);
		if (particleId == 0)
			return false;
		pParticleRM->GetRuntimeEffect(particleId, particleGlid, name);
		return true;
	} else {
		for (auto it = m_pendingParticleAdds.begin(); it != m_pendingParticleAdds.end(); ++it) {
			if (it->boneName == boneName && it->particleSlotId == particleSlotId) {
				particleGlid = it->particleGLID;
				// No name stored in m_pendingParticleAdds
				return true;
			}
		}
		return false;
	}
}

void CDynamicPlacementObj::SetLookAt(ObjectType targetType, int targetId, const std::string& targetBone, const Vector3f& offset, bool followTarget, bool allowXZRotation, const Vector3f& forwardDirection) {
	// Cancel previous look-at settings
	m_pLookAtInfo.reset();

	if (targetType != ObjectType::NONE) {
		// Keep looking at the target
		m_pLookAtInfo = std::make_unique<LookAtInfo>();
		m_pLookAtInfo->targetType = targetType;
		m_pLookAtInfo->targetId = targetId;
		m_pLookAtInfo->targetBone = targetBone;
		m_pLookAtInfo->offset = offset;
		m_pLookAtInfo->followTarget = followTarget;
		m_pLookAtInfo->allowXZRotation = allowXZRotation;

		if (forwardDirection.z < 1 - 1E-6) {
			Vector3f tmp(forwardDirection), fwdVector;

			if (!allowXZRotation)
				tmp.y = 0;

			fwdVector = tmp.GetNormalized();

			m_pLookAtInfo->pForwardDirectionMatrix = new Matrix44f();
			m_pLookAtInfo->pForwardDirectionMatrix->MakeIdentity();
			m_pLookAtInfo->pForwardDirectionMatrix->SetRowZ(fwdVector);
			m_pLookAtInfo->pForwardDirectionMatrix->SetRowX(Vector3f(fwdVector.z, 0, -fwdVector.x));
			m_pLookAtInfo->pForwardDirectionMatrix->SetRowY(Vector3f(0, 1, 0));
		}
	}

	UpdateShouldBeInSublist(DynObjSubListId_NeedsUpdate);
}

bool CDynamicPlacementObj::GetLookAt(ObjectType& targetType, int& targetId, std::string& targetBone, Vector3f& offset, bool& followTarget, bool& allowXZRotation, Matrix44f*& pForwardDirectionMatrix) const {
	if (!m_pLookAtInfo) {
		targetType = ObjectType::NONE;
		return false;
	}

	targetType = m_pLookAtInfo->targetType;
	targetId = m_pLookAtInfo->targetId;
	targetBone = m_pLookAtInfo->targetBone;
	offset = m_pLookAtInfo->offset;
	followTarget = m_pLookAtInfo->followTarget;
	allowXZRotation = m_pLookAtInfo->allowXZRotation;
	pForwardDirectionMatrix = m_pLookAtInfo->pForwardDirectionMatrix;
	return true;
}

void CDynamicPlacementObj::MakeTranslucent() {
	m_translucent = true;
	if (m_baseObj && m_pSkeleton) {
		for (size_t i = 0; i < m_baseObj->getVisualCount(); i++) {
			// Make a copy of current material (possibly still shared from SDO visual)
			CMaterialObject* pSourceMaterial = GetMaterial(i);
			if (!pSourceMaterial)
				continue; // No material. This is probably a collision-only visual.
			SetMaterial(i, pSourceMaterial);
			CMaterialObject* pMaterial = GetMaterial(i); // cloned one
			pMaterial->m_useMaterial.Ambient.a = pMaterial->m_baseMaterial.Ambient.a / 2.0f;
			pMaterial->m_blendMode = 0;
		}
	}
}

ReMesh* CDynamicPlacementObj::GetBoundingBoxMesh() {
#ifdef _ClientOutput
	if (!m_boxMesh)
		m_boxMesh = KEPWidgetMeshes::loadReMeshOBJ(MeshStrings::BOX_MESH);

	if (!ms_boxMaterial.GetReMaterial()) {
		ms_boxMaterial.m_blendMode = 0; // Enable transparency
		MeshRM::Instance()->CreateReMaterial(&ms_boxMaterial);
		ms_boxMaterial.GetReMaterial()->PreLight(FALSE);
	}

	m_boxMesh->SetMaterial(ms_boxMaterial.GetReMaterial());
	return m_boxMesh;
#else
	return nullptr;
#endif
}

Matrix44f* CDynamicPlacementObj::GetBoundingBoxMatrix() {
	if (!m_boxMatrix)
		m_boxMatrix = new Matrix44f;

	Vector4f min4, max4;
	GetBoundingBoxAnimAdjust(min4, max4);
	const Vector3f& min = min4.SubVector<3>();
	const Vector3f& max = max4.SubVector<3>();

	Vector3f ctr, size;
	ctr = (min + max) / 2;
	size = max - min;

	// Render the box slightly bigger than it should be to avoid flickering
	m_boxMatrix->MakeScale(abs(size.x + 1E-2));
	m_boxMatrix->GetTranslation() = ctr;

	return m_boxMatrix;
}

bool CDynamicPlacementObj::GetWorldMatrix(Matrix44f& matrix) const {
	if (!m_pSkeleton)
		return false;

	matrix = m_pSkeleton->getWorldMatrix();
	return true;
}

ReMesh* CDynamicPlacementObj::GetMediaVideoRangeMesh() {
#ifdef _ClientOutput
	if (!IsMediaLocal())
		return nullptr;

	auto radiusVideo = GetMediaRadius().GetVideo();
	if (m_mediaVideoRangeMesh == nullptr) {
		m_mediaVideoRangeMesh = KEPWidgetMeshes::loadReMeshOBJ(MeshStrings::SOUND_MESH);
		m_mediaVideoRangeMesh->SetWorldMatrix(&m_mediaVideoRangeMatrix);
	}

	m_mediaVideoRangeMatrix.MakeScale_Homogeneous((float)radiusVideo);
	m_mediaVideoRangeMatrix.GetTranslation() = m_position;
	m_mediaVideoRangeMesh->SetMaterial(MediaIsPlaying() ? ms_mediaVideoRangeMaterialPlay.GetReMaterial() : ms_mediaVideoRangeMaterialIdle.GetReMaterial());
	return m_mediaVideoRangeMesh;
#else
	return NULL;
#endif
}

ReMesh* CDynamicPlacementObj::GetMediaAudioRangeMesh() {
#ifdef _ClientOutput
	if (!IsMediaLocal())
		return nullptr;

	auto radiusAudio = GetMediaRadius().GetAudio();
	if (m_mediaAudioRangeMesh && fabs(radiusAudio - m_mediaAudioRangeMeshRadius) > 1E-2) {
		delete m_mediaAudioRangeMesh;
		m_mediaAudioRangeMesh = nullptr;
	}

	if (m_mediaAudioRangeMesh == nullptr) {
		m_mediaAudioRangeMeshRadius = radiusAudio;
		m_mediaAudioRangeMesh = KEPWidgetMeshes::getRingMesh(2.0 * M_PI, m_mediaAudioRangeMeshRadius);
		m_mediaAudioRangeMatrix.MakeIdentity();
		m_mediaAudioRangeMesh->SetWorldMatrix(&m_mediaAudioRangeMatrix);
	}

	m_mediaAudioRangeMatrix.GetTranslation() = m_position;
	m_mediaAudioRangeMesh->SetMaterial(MediaIsPlaying() && !GetMediaMute() ? ms_mediaAudioRangeMaterialPlay.GetReMaterial() : ms_mediaAudioRangeMaterialIdle.GetReMaterial());
	return m_mediaAudioRangeMesh;
#else
	return NULL;
#endif
}

void CDynamicPlacementObj::UpdateMediaVisMaterials() {
	if (!ms_mediaVideoRangeMaterialIdle.GetReMaterial()) {
		ms_mediaVideoRangeMaterialIdle.m_blendMode = 0; // Enable transparency
		MeshRM::Instance()->CreateReMaterial(&ms_mediaVideoRangeMaterialIdle);
		ms_mediaVideoRangeMaterialIdle.GetReMaterial()->PreLight(FALSE);
	}

	if (!ms_mediaVideoRangeMaterialPlay.GetReMaterial()) {
		ms_mediaVideoRangeMaterialPlay.m_blendMode = 0; // Enable transparency
		MeshRM::Instance()->CreateReMaterial(&ms_mediaVideoRangeMaterialPlay);
		ms_mediaVideoRangeMaterialPlay.GetReMaterial()->PreLight(FALSE);
	}

	if (!ms_mediaAudioRangeMaterialIdle.GetReMaterial()) {
		ms_mediaAudioRangeMaterialIdle.m_blendMode = 0; // Enable transparency
		MeshRM::Instance()->CreateReMaterial(&ms_mediaAudioRangeMaterialIdle);
		ms_mediaAudioRangeMaterialIdle.GetReMaterial()->PreLight(FALSE);
	}

	if (!ms_mediaAudioRangeMaterialPlay.GetReMaterial()) {
		ms_mediaAudioRangeMaterialPlay.m_blendMode = 0; // Enable transparency
		MeshRM::Instance()->CreateReMaterial(&ms_mediaAudioRangeMaterialPlay);
		ms_mediaAudioRangeMaterialPlay.GetReMaterial()->PreLight(FALSE);
	}
}

std::string CDynamicPlacementObj::ToStr(bool verbose) const {
	std::string str;
	StrAppend(str, ObjTypeStr() << "<" << GetPlacementId());
	if (verbose) {
		StrAppend(str, ":" << GetGlobalId() << " '" << GetBaseObjName() << "'");
	}
	if (MediaSupported() && ValidMediaUrlAndParams()) {
		StrAppend(str, " " << m_mediaParams.ToStr(verbose));
	}
	if (!IsVisible()) {
		StrAppend(str, (IsHiddenForMe() ? " HIDDEN_FOR_ME" : " HIDDEN_FOR_ALL"));
	}
	StrAppend(str, ">");
	return str;
}

void CDynamicPlacementObj::AssignObjType() {
	auto pSDO = GetBaseObj();
	if (!pSDO) {
		m_objType = DYN_OBJ_TYPE::INVALID;
	} else {
		const std::set<GLID> mediaFrameGlids = { 1770, 1771, 1772, 2999 };
		GLID glid = m_globalId;
		if (mediaFrameGlids.find(glid) != mediaFrameGlids.end()) {
			m_objType = DYN_OBJ_TYPE::MEDIA_FRAME;
		} else if (MediaSupported()) {
			m_objType = (IsInteractive() ? DYN_OBJ_TYPE::MEDIA_GAME : DYN_OBJ_TYPE::MEDIA_PLAYER);
		} else {
			m_objType = DYN_OBJ_TYPE::NORMAL;
		}
	}
}

void CDynamicPlacementObj::SetHighlight(bool highlight, const D3DXCOLOR& color) {
	if (m_highlight == highlight && (!highlight || color != m_highlightColor))
		return; // no change.

	m_highlight = highlight;
	m_highlightColor = highlight ? color : D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);

	ApplyCurrentHighlight();
}

void CDynamicPlacementObj::ApplyCurrentHighlight() {
	if (!m_pSkeleton)
		return;

	size_t nRequiredMaterials = m_pSkeleton->getSkinnedMeshCount();
	if (nRequiredMaterials > m_customMaterials.size())
		m_customMaterials.resize(nRequiredMaterials);

	for (size_t i = 0; i < m_customMaterials.size(); ++i) {
		// Create a custom material if we're highlighting and we don't already have a custom material.
		if (m_highlight && !m_customMaterials[i]) {
			StreamableDynamicObjectVisual* pVisual = m_baseObj->Visual(i);
			if (pVisual) {
				CMaterialObject* pMaterial = pVisual->getMaterial();
				if (pMaterial) {
					CMaterialObject* pClonedMat = nullptr;
					pMaterial->Clone(&pClonedMat);
					m_customMaterials[i].reset(pClonedMat);
					m_pSkeleton->SetMaterialObject(i, pClonedMat, true, &m_textureColors);
				}
			}
		}

		if (m_customMaterials[i])
			m_customMaterials[i]->ApplyHighlight(m_highlight, m_highlightColor);
	}
}

void CDynamicPlacementObj::SetBoundingBoxRenderEnabled(bool enable) {
	if (m_renderBoundingBox == enable)
		return;

	m_renderBoundingBox = enable;

	auto pICE = IClientEngine::Instance();
	if (pICE)
		pICE->UpdateDynamicObjectInSubList(this, DynObjSubListId_BoundingBoxRender);
}

void CDynamicPlacementObj::SetExternalMesh(std::shared_ptr<IExternalEXMesh> pExternalMesh) {
	if (pExternalMesh.get() == m_pExternalMesh.get())
		return;

	m_pExternalMesh = std::move(pExternalMesh);
}

void CDynamicPlacementObj::SetMediaParams(const MediaParams& mediaParams) {
	m_mediaParams.Set(mediaParams);
	auto pICE = IClientEngine::Instance();
	if (pICE)
		pICE->UpdateDynamicObjectInSubList(this, DynObjSubListId_HasMedia);
}

void CDynamicPlacementObj::SetMediaURLandParams(const std::string& strURL, const std::string& strParams) {
	m_mediaParams.SetUrl(strURL);
	m_mediaParams.SetParams(strParams);
	auto pICE = IClientEngine::Instance();
	if (pICE)
		pICE->UpdateDynamicObjectInSubList(this, DynObjSubListId_HasMedia);
}

void CDynamicPlacementObj::SetMediaRadius(const MediaRadius& radius) {
	bool bWasGlobal = m_mediaParams.GetRadius().IsGlobal();
	bool bNowGlobal = radius.IsGlobal();
	m_mediaParams.SetRadius(radius);
	if (bWasGlobal != bNowGlobal) {
		auto pICE = IClientEngine::Instance();
		if (pICE)
			pICE->UpdateDynamicObjectInSubList(this, DynObjSubListId_HasMedia);
	}
}

bool CDynamicPlacementObj::ShouldBeInSublist(DynObjSubListId iSubListId) const {
	switch (iSubListId) {
		case DynObjSubListId_NeedsUpdate:
			return NeedsUpdateCallback();

		case DynObjSubListId_BoundingBoxRender:
			return IsBoundingBoxRenderEnabled();

		case DynObjSubListId_HasMedia:
			return HasMedia();

		case DynObjSubListId_HasLight:
			return HasLight();
	}

	return false;
}

void CDynamicPlacementObj::ClientEngineState::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
}

void CDynamicPlacementObj::PivotTransitionPosition::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CDynamicPlacementObj::PivotTransitionRotation::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CDynamicPlacementObj::PendingParticleAdd::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(boneName);
		pMemSizeGadget->AddObject(particleEffectName);
	}
}

void CDynamicPlacementObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	// Attempt to add the base object size of CDynamicPlacementObj. If it hasn't
	// already been added, AddObjectSizeof()->true, then add all of this objects child
	// data members that are allocated dynamically or also have child data members
	// that are dynamically allocated.
	//
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_baseObj); // StreamableDynamicObject*
		pMemSizeGadget->AddObject(m_boxMatrix); // Matrix44f*
		pMemSizeGadget->AddObject(m_boxMesh); // ReMesh*
		pMemSizeGadget->AddObject(m_ClientEngineState); // CDynamicPlacementObj::ClientEngineState
		pMemSizeGadget->AddObject(m_customMaterials); // std::vector<std::unique_ptr<CMaterialObject>
		pMemSizeGadget->AddObject(m_customTexture); // std::shared_ptr<CustomTextureProvider>
		pMemSizeGadget->AddObject(m_mediaAudioRangeMesh); // ReMesh*
		pMemSizeGadget->AddObject(m_mediaParams); // MediaParams
		pMemSizeGadget->AddObject(m_mediaVideoRangeMesh); // ReMesh*
		pMemSizeGadget->AddObject(m_pendingParticleAdds); // vector<PendingParticleAdd>
		pMemSizeGadget->AddObject(m_pEffectTrack); // std::unique_ptr<EffectTrack>
		pMemSizeGadget->AddObject(m_pExternalMesh); // std::shared_ptr<IExternalEXMesh>
		pMemSizeGadget->AddObject(m_pLight); // std::unique_ptr<CRTLightObj
		pMemSizeGadget->AddObject(m_pLookAtInfo); // std::unique_ptr<LookAtInfo>
		pMemSizeGadget->AddObject(m_pMovementObjSkeleton); // RuntimeSkeleton*
		pMemSizeGadget->AddObject(m_pPivotTransitionPosition); // std::unique_ptr<PivotTransitionPosition>
		pMemSizeGadget->AddObject(m_pPivotTransitionRotation); // std::unique_ptr<PivotTransitionPosition>
		pMemSizeGadget->AddObject(m_pResource); // TResource<StreambleDynamicObject>*
		pMemSizeGadget->AddObject(m_pRigidBody); // std::shared_ptr<Physics::RigidBodyStatic>
		pMemSizeGadget->AddObject(m_pRigidBodyVisual); // std::shared_ptr<Physics::RigidBodyStatic>
		pMemSizeGadget->AddObject(m_pSkeleton); // std::shared_ptr<RuntimeSkeleton>
		pMemSizeGadget->AddObject(m_pSkeletonConfiguration); // std::unique_ptr<SkeletonConfiguration>
		pMemSizeGadget->AddObject(m_skeletonLabels); // SkeletonLabels*
		pMemSizeGadget->AddObject(m_textureColors); // std::map<unsigend, unsigned>
		pMemSizeGadget->AddObject(m_textureHashes); // std::set<DWORD>
		pMemSizeGadget->AddObject(m_texturePanningSettings); // std::map<UINT, Vector2f>
		pMemSizeGadget->AddObject(m_textureUrl); // std::string
		pMemSizeGadget->AddObject(m_ugcItemTexture); // std::shared_ptr<UGCItemTextureProvider>

		// static data members
		pMemSizeGadget->AddObject(ms_boxMaterial); // CMaterialObject
		pMemSizeGadget->AddObject(ms_mediaAudioRangeMaterialIdle); // CMaterialObject
		pMemSizeGadget->AddObject(ms_mediaAudioRangeMaterialPlay); // CMaterialObject
		pMemSizeGadget->AddObject(ms_mediaVideoRangeMaterialIdle); // CMaterialObject
		pMemSizeGadget->AddObject(ms_mediaVideoRangeMaterialPlay); // CMaterialObject
	}
}

} // namespace KEP
