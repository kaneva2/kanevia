///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DropTargetProxy_Win32.h"
#include "IKEPDropTarget.h"
#include "Core/Util/Unicode.h"

#define MAX_DROP_TEXT_LEN 1024

namespace KEP {

HRESULT STDMETHODCALLTYPE DropTargetProxy::QueryInterface(REFIID riid, void __RPC_FAR* __RPC_FAR* ppvObject) {
	if (riid == IID_IUnknown) {
		*ppvObject = (IUnknown*)this;
		AddRef();
		return S_OK;
	}

	else if (riid == IID_IDropTarget) {
		*ppvObject = (IDropTarget*)this;
		AddRef();
		return S_OK;
	}

	*ppvObject = NULL;
	return E_NOINTERFACE;
}

ULONG STDMETHODCALLTYPE DropTargetProxy::AddRef(void) {
	refcnt++;
	return refcnt;
}

ULONG STDMETHODCALLTYPE DropTargetProxy::Release(void) {
	refcnt--;
	// Do not dispose -- leave it to main application
	return refcnt;
}

HRESULT STDMETHODCALLTYPE DropTargetProxy::DragEnter(IDataObject* pDataObj, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect) {
	bAllowed = ProcessDragDrop(FALSE, pDataObj, grfKeyState, pt);
	*pdwEffect = bAllowed ? DROPEFFECT_COPY : DROPEFFECT_NONE;
	return S_OK;
}

HRESULT STDMETHODCALLTYPE DropTargetProxy::DragOver(DWORD grfKeyState, POINTL pt, DWORD* pdwEffect) {
	if (!bAllowed) {
		pdwEffect = DROPEFFECT_NONE;
		return S_OK;
	}

	target.DragOver(pt.x, pt.y, grfKeyState, 0);
	return S_OK;
}

HRESULT STDMETHODCALLTYPE DropTargetProxy::DragLeave(void) {
	target.DragLeave();

	// Reset bAllowed flag
	bAllowed = false;
	return S_OK;
}

HRESULT STDMETHODCALLTYPE DropTargetProxy::Drop(IDataObject* pDataObj, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect) {
	bool res = ProcessDragDrop(TRUE, pDataObj, grfKeyState, pt);
	*pdwEffect = res ? DROPEFFECT_COPY : DROPEFFECT_NONE;

	// Reset bAllowed flag
	bAllowed = false;
	return S_OK;
}

bool DropTargetProxy::ProcessDragDrop(BOOL bDrop, IDataObject* pDataObj, DWORD grfKeyState, const POINTL& pt) {
	FORMATETC fmtetcFile = { CF_HDROP, 0, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
	FORMATETC fmtetcText = { CF_UNICODETEXT, 0, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
	STGMEDIUM medium;

	/*
		Available FORMATETCs for file drop from Windows Explorer/Vista
			0x0000C07C, 			DVASPECT_CONTENT, 	-1, 	TYMED_HGLOBAL
			0x0000C0C2, 			DVASPECT_CONTENT, 	-1, 	TYMED_HGLOBAL
			0x0000C2A5, 			DVASPECT_CONTENT, 	-1, 	TYMED_HGLOBAL
			0x0000C0D0, 			DVASPECT_CONTENT, 	-1, 	TYMED_ISTREAM
			0x0000C0CF, 			DVASPECT_CONTENT, 	-1, 	TYMED_HGLOBAL
			CF_HDROP (15), 			DVASPECT_CONTENT, 	-1, 	TYMED_HGLOBAL
			0x0000C006, 			DVASPECT_CONTENT, 	-1, 	TYMED_HGLOBAL
			0x0000C007, 			DVASPECT_CONTENT, 	-1, 	TYMED_HGLOBAL

		Available FORMATETCs for URL drop from Firefox 3.0
			0x0000C2A1,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C0C5,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C0C6,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C0C4,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C0CE,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C0D6,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			CF_UNICODETEXT (13),	DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			CF_TEXT (1),			DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C13B,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C0CA,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C2A5,				DVASPECT_CONTENT,	-1, 	TYMED_HGLOBAL
			0x0000C0D0,				DVASPECT_CONTENT,	-1, 	TYMED_ISTREAM

		Available FORMATETCs for URL drop from IE7/Vista
			0x0000C0C4, 			DVASPECT_ICON,		0,		TYMED_HGLOBAL
			0x0000C0C6, 			DVASPECT_ICON, 		-1,		TYMED_HGLOBAL
			0x0000C0C5, 			DVASPECT_ICON,		-1,		TYMED_HGLOBAL
			0x0000C0C4, 			DVASPECT_CONTENT | DVASPECT_THUMBNAIL,	0,		TYMED_ISTREAM
			0x0000C0C6, 			DVASPECT_CONTENT | DVASPECT_THUMBNAIL,	-1,		TYMED_HGLOBAL
			0x0000C0C5, 			DVASPECT_CONTENT | DVASPECT_THUMBNAIL,	-1, 	TYMED_HGLOBAL
			0x0000C0C4, 			DVASPECT_CONTENT,	0,	TYMED_HGLOBAL
			0x0000C0C6, 			DVASPECT_CONTENT,	-1,	TYMED_HGLOBAL
			0x0000C0C5, 			DVASPECT_CONTENT,	-1,	TYMED_HGLOBAL
			0x0000C0D6, 			DVASPECT_CONTENT,	-1,	TYMED_HGLOBAL
			CF_UNICODETEXT (13),	DVASPECT_CONTENT,	-1,	TYMED_HGLOBAL
			0x0000C0CE, 			DVASPECT_CONTENT,	-1,	TYMED_HGLOBAL
			CF_TEXT (1), 			DVASPECT_CONTENT,	-1,	TYMED_HGLOBAL
	*/

	//Temp-Debug
#ifdef _DEBUG
	if (bDrop) {
		IEnumFORMATETC* pFormatEtcs;
		FORMATETC fmtetc;
		pDataObj->EnumFormatEtc(DATADIR_GET, &pFormatEtcs);
		ULONG numFetched;
		while (pFormatEtcs->Next(1, &fmtetc, &numFetched) == S_OK) {
			CStringA msg;
			msg.Format("0x%08X, \t\t\t%d, \t%d, \t%d\n", fmtetc.cfFormat, fmtetc.dwAspect, fmtetc.lindex, fmtetc.tymed);
			TRACE(msg);
		}
	}
#endif

	bool res = false;

	if (pDataObj->QueryGetData(&fmtetcFile) == S_OK) {
		if (pDataObj->GetData(&fmtetcFile, &medium) == S_OK && medium.tymed == TYMED_HGLOBAL) {
			HDROP hDrop = (HDROP)medium.hGlobal;

			UINT fileCount = DragQueryFile(hDrop, 0xFFFFFFFF, NULL, 0);
			if (fileCount == 1) {
				UINT filePathLen = DragQueryFile(hDrop, 0, NULL, 0);
				if (filePathLen > 0) {
					wchar_t* tmpBuf = new wchar_t[filePathLen + 1];
					if (tmpBuf) {
						if (DragQueryFile(hDrop, 0, tmpBuf, filePathLen + 1) == filePathLen) {
							std::string fullPath(Utf16ToUtf8(tmpBuf).c_str());
							if (bDrop)
								res = target.Drop(DROP_FILE, fullPath, pt.x, pt.y, grfKeyState, 0);
							else
								res = target.DragEnter(DROP_FILE, fullPath, pt.x, pt.y, grfKeyState, 0);
						}

						delete tmpBuf;
					}
				}
			}
		} else {
			// Something strange happened -- check FORMATETC enumeration for detail
			ASSERT(FALSE);
		}
	} else if (pDataObj->QueryGetData(&fmtetcText) == S_OK) {
		// Text dropped
		if (pDataObj->GetData(&fmtetcText, &medium) == S_OK && medium.tymed == TYMED_HGLOBAL) {
			wchar_t* pwszText = (wchar_t*)GlobalLock(medium.hGlobal);
			size_t stringLen = pwszText == NULL ? 0 : wcslen(pwszText);

			if (stringLen <= MAX_DROP_TEXT_LEN) {
				std::string fullPath = Utf16ToUtf8(pwszText);

				if (bDrop)
					res = target.Drop(DROP_TEXT, fullPath, pt.x, pt.y, grfKeyState, 0);
				else
					res = target.DragEnter(DROP_TEXT, fullPath, pt.x, pt.y, grfKeyState, 0);
			}

			GlobalUnlock(medium.hGlobal);
		} else {
			// Something strange happened -- check FORMATETC enumeration for detail
			ASSERT(FALSE);
		}
	}

	return res;
}

} // namespace KEP