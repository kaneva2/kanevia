///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EffectTrack.h"
#include "common\include\IMemSizeGadget.h"
#include <algorithm>

namespace KEP {

struct EffectEventPredicate {
	bool operator()(const IEffectEventPtr& e1, const IEffectEventPtr& e2) {
		return e1->m_offset < e2->m_offset;
	}
};

void EffectTrack::addEffect(IEffectEventPtr effect) {
	effect->m_objType = m_type;
	m_effectTrack.push_back(effect);
	EffectEventPredicate pred;
	std::sort(m_effectTrack.begin(), m_effectTrack.end(), pred);
}

void EffectTrack::update(TimeMs currentTime) {
	while (m_effectTrack.size()) {
		TimeMs effectTime = 0;
		IEffectEventPtr effect = m_effectTrack.front();
		effectTime = effect->m_offset + m_startTime;
		if (effectTime <= currentTime) {
			m_effectTrack.pop_front();
			effect->execute();
		} else {
			// pending effects, but nothing that's ready to play
			return;
		}
	}
}

void EffectTrack::clearTrack() {
	m_effectTrack.clear();
}

void IEffectEvent::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void EffectTrack::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_effectTrack);
	}
}

} // namespace KEP