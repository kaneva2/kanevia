///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CLightTrackerClass.h"

namespace KEP {

//------------Main object list------------------//
CLightTrackerObj::CLightTrackerObj() {
	m_hardwareIndex = 0;
}

void CLightTrackerObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CLightTrackerObj* objPtr = (CLightTrackerObj*)GetNext(posLoc);
		delete objPtr;
		objPtr = 0;
	}
	RemoveAll();
}

int CLightTrackerObjList::AddLight() {
	int indexFind = 2;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CLightTrackerObj* objPtr = (CLightTrackerObj*)GetNext(posLoc);
		if (objPtr->m_hardwareIndex != indexFind) { //found open hardware int
			CLightTrackerObj* newObjPtr = new CLightTrackerObj();
			newObjPtr->m_hardwareIndex = indexFind;
			AddTail(newObjPtr);
			return indexFind;
		} //end found open hardware int
		indexFind++;
	}
	return indexFind;
}

void CLightTrackerObjList::LightHasBeenRemoved(int hardwareIndex) {
	POSITION posLast;
	for (POSITION posLoc = GetHeadPosition(); (posLast = posLoc) != NULL;) {
		CLightTrackerObj* objPtr = (CLightTrackerObj*)GetNext(posLoc);
		if (objPtr->m_hardwareIndex == hardwareIndex) { //
			CLightTrackerObj* delPtr = (CLightTrackerObj*)GetAt(posLast);
			delete delPtr;
			delPtr = 0;
			RemoveAt(posLast);
			return;
		} //
	}
}

} // namespace KEP