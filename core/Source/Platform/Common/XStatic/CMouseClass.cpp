/******************************************************************************
 CMouseClass.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"
#include "resource.h"
#include <d3d8.h>
#include <afxtempl.h>
#include "CMouseClass.h"
//------------visual list------------------//
CMouseVisualObj::CMouseVisualObj(CStringA L_fileName,
	LPDIRECTDRAWSURFACE7 L_surface) {
	m_fileName = L_fileName;
	m_surface = L_surface;
}

void CMouseVisualObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_fileName;
	} else {
		ar >> m_fileName;
		m_surface = 0;
	}
}

IMPLEMENT_SERIAL(CMouseVisualObj, CObject, 0)
IMPLEMENT_SERIAL(CMouseVisualObjList, CObList, 0)

//-----------main list---------------------//
CMouseObj::CMouseObj(CStringA L_mouseCfgName,
	int L_sizeX,
	int L_sizeY,
	BOOL L_inUse,
	CMouseVisualObjList* L_visuals,
	int L_visualCount,
	int L_currentVisual,
	int L_redSrc,
	int L_greenSrc,
	int L_blueSrc) {
	m_redSrc = L_redSrc;
	m_greenSrc = L_greenSrc;
	m_blueSrc = L_blueSrc;
	m_visualCount = L_visualCount;
	m_currentVisual = L_currentVisual;
	m_mouseCfgName = L_mouseCfgName;
	m_sizeX = L_sizeX;
	m_sizeY = L_sizeY;
	m_inUse = L_inUse;
	m_visuals = L_visuals;
}

void CMouseObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_visuals << m_inUse << m_sizeY << m_sizeX << m_mouseCfgName
		   << m_visualCount << m_currentVisual
		   << m_redSrc << m_greenSrc << m_blueSrc;
	} else {
		ar >> m_visuals >> m_inUse >> m_sizeY >> m_sizeX >> m_mouseCfgName >> m_visualCount >> m_currentVisual >> m_redSrc >> m_greenSrc >> m_blueSrc;
		m_inUse = FALSE;
	}
}

IMPLEMENT_SERIAL(CMouseObj, CObject, 0)
IMPLEMENT_SERIAL(CMouseObjList, CObList, 0)

// begin serialize.pl generated code
BEGIN_GETSET_IMPL(CMouseVisualObj, IDS_MOUSEVISUALOBJ_NAME)
GETSET_MAX(m_fileName, gsCString, GS_FLAGS_DEFAULT, 0, 0, MOUSEVISUALOBJ, FILENAME, STRLEN_MAX)
END_GETSET_IMPL
// end of generated code
