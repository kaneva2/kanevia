///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <afxtempl.h>
#include "d3d9.h"
#include "CBankClass.h"

namespace KEP {

CBankObj::CBankObj() {
	m_bankName = "VAULT";
	m_channel.clear();
	m_boundingBoxMax.x = 60000.0f;
	m_boundingBoxMax.y = 60000.0f;
	m_boundingBoxMax.z = 60000.0f;
	m_boundingBoxMin.x = -60000.0f;
	m_boundingBoxMin.y = -60000.0f;
	m_boundingBoxMin.z = -60000.0f;
	m_menuBind = 0;
	m_willDealWithCriminals = TRUE;
}

void CBankObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CBankObj* spnPtr = (CBankObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

BOOL CBankObjList::BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max) {
	if (point.x >= min.x &&
		point.x <= max.x &&
		point.y >= min.y &&
		point.y <= max.y &&
		point.z >= min.z &&
		point.z <= max.z)
		return TRUE;
	return FALSE;
}

CBankObj* CBankObjList::GetBankByPosition(Vector3f position,
	const ChannelId& channel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CBankObj* cmPtr = (CBankObj*)GetNext(posLoc);
		if (channel.channelIdEquals(cmPtr->m_channel)) //same world
			if (BoundBoxPointCheck(position,
					cmPtr->m_boundingBoxMin,
					cmPtr->m_boundingBoxMax) == TRUE) {
				//in zone
				return cmPtr;
			} //end in zone
	}
	return NULL;
}

void CBankObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_bankName
		   << m_channel
		   << m_boundingBoxMax.x
		   << m_boundingBoxMax.y
		   << m_boundingBoxMax.z
		   << m_boundingBoxMin.x
		   << m_boundingBoxMin.y
		   << m_boundingBoxMin.z
		   << m_menuBind
		   << m_willDealWithCriminals;
	} else {
		ar >> m_bankName >> m_channel >> m_boundingBoxMax.x >> m_boundingBoxMax.y >> m_boundingBoxMax.z >> m_boundingBoxMin.x >> m_boundingBoxMin.y >> m_boundingBoxMin.z >> m_menuBind >> m_willDealWithCriminals;
	}
}

IMPLEMENT_SERIAL(CBankObj, CObject, 0)
IMPLEMENT_SERIAL(CBankObjList, CKEPObList, 0)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CBankObj, IDS_BANKOBJ_NAME)
GETSET_MAX(m_bankName, gsCString, GS_FLAGS_DEFAULT, 0, 0, BANKOBJ, BANKNAME, STRLEN_MAX)
GETSET_RANGE(m_channel, gsInt, GS_FLAGS_DEFAULT, 0, 0, BANKOBJ, WORLDID, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_boundingBoxMax, GS_FLAGS_DEFAULT, 0, 0, BANKOBJ, BOUNDINGBOXMAX)
GETSET_D3DVECTOR(m_boundingBoxMin, GS_FLAGS_DEFAULT, 0, 0, BANKOBJ, BOUNDINGBOXMIN)
GETSET_RANGE(m_menuBind, gsInt, GS_FLAGS_DEFAULT, 0, 0, BANKOBJ, MENUBIND, INT_MIN, INT_MAX)
GETSET(m_willDealWithCriminals, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, BANKOBJ, WILLDEALWITHCRIMINALS)
END_GETSET_IMPL

} // namespace KEP