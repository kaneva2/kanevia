///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CElementStatusClass.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

CElementStatusObj::CElementStatusObj() {
	m_elementIndex = 0; //corilates to index zero based
	m_currentValue = 0; //current value
}

void CElementStatusObj::SafeDelete() {
}

void CElementStatusObj::Clone(CElementStatusObj** clone) {
	CElementStatusObj* elmPtr = new CElementStatusObj();
	elmPtr->m_currentValue = m_currentValue;
	elmPtr->m_elementIndex = m_elementIndex;

	*clone = elmPtr;
}

int CElementStatusList::GetDefenseForSpecifiedChannel(int damageChannel) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CElementStatusObj* invObj = (CElementStatusObj*)GetNext(posLoc);
		if (invObj->m_elementIndex == damageChannel)
			return invObj->m_currentValue;
	}
	return 0;
}

void CElementStatusList::Clone(CElementStatusList** clone) {
	CElementStatusList* copyLocal = new CElementStatusList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CElementStatusObj* invObj = (CElementStatusObj*)GetNext(posLoc);
		CElementStatusObj* cpyPtr = NULL;
		invObj->Clone(&cpyPtr);
		copyLocal->AddTail(cpyPtr);
	}
	*clone = copyLocal;
}

void CElementStatusList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CElementStatusObj* elementPtr = (CElementStatusObj*)GetNext(posLoc);
		elementPtr->SafeDelete();
		delete elementPtr;
		elementPtr = 0;
	}
	RemoveAll();
}

void CElementStatusObj::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_currentValue
		   << m_elementIndex;
	} else {
		ar >> m_currentValue >> m_elementIndex;
	}
}

void CElementStatusList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CElementStatusObj* elementPtr = (CElementStatusObj*)GetNext(posLoc);
			elementPtr->SerializeAlloc(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			CElementStatusObj* elementPtr = new CElementStatusObj();
			elementPtr->SerializeAlloc(ar);
		}
	}
}

void CElementStatusObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_currentValue
		   << m_elementIndex;
	} else {
		ar >> m_currentValue >> m_elementIndex;
	}
}

void CElementStatusObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);
	}
}

void CElementStatusList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CElementStatusObj* pCElementStatusObj = static_cast<const CElementStatusObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCElementStatusObj);
		}
	}
}

IMPLEMENT_SERIAL(CElementStatusObj, CObject, 0)
IMPLEMENT_SERIAL(CElementStatusList, CObList, 0)

BEGIN_GETSET_IMPL(CElementStatusObj, IDS_ELEMENTSTATUSOBJ_NAME)
GETSET_RANGE(m_currentValue, gsShort, GS_FLAGS_DEFAULT, 0, 0, ELEMENTSTATUSOBJ, CURRENTVALUE, SHRT_MIN, SHRT_MAX)
GETSET_RANGE(m_elementIndex, gsShort, GS_FLAGS_DEFAULT, 0, 0, ELEMENTSTATUSOBJ, ELEMENTINDEX, SHRT_MIN, SHRT_MAX)
END_GETSET_IMPL

} // namespace KEP