///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CharConfigClass.h"
#include "Event\Base\IEvent.h"
#include "CGlobalInventoryClass.h"
#include "common\include\IMemSizeGadget.h"
#include "common\KEPUtil\Helpers.h"
static LogInstance("Instance");

namespace KEP {

std::string CharConfig::ToStr(bool verbose) const {
	std::string str;
	if (!verbose) {
		StrAppend(str, "CC<");
		for (size_t i = 0; i < getItemCount(); ++i) {
			StrAppend(str, (i ? " " : "") << getItemByIndex(i)->getGlidOrBase());
		}
		StrAppend(str, ">");
	} else {
		StrAppend(str, "CC<" << this << "> items=" << getItemCount());
		for (size_t i = 0; i < getItemCount(); ++i) {
			const CharConfigItem* pItem = getItemByIndex(i);
			StrAppend(str, "\n ... item=" << i
										  << " baseGlid=" << (int)pItem->getGlidOrBase()
										  << " animGlid=" << (int)pItem->getAnimationGlid()
										  << " texUrl='" << pItem->getTexUrl() << "'"
										  << " slots=" << pItem->getSlotCount());
			for (size_t j = 0; j < pItem->getSlotCount(); j++) {
				const CharConfigItemSlot* pSlot = pItem->getSlot(j);
				if (!pSlot)
					continue;
				StrAppend(str, "\n ...    slot=" << j << " {"
												 << (int)pSlot->m_meshId
												 << ", " << (int)pSlot->m_matlId
												 << ", " << (int)pSlot->m_r
												 << ", " << (int)pSlot->m_g
												 << ", " << (int)pSlot->m_b
												 << "}");
			}
		}
	}
	return str;
}

void CharConfig::Log(bool verbose) const {
	LogInfo(ToStr(verbose));
}

#ifdef _AFX
CArchive& CharConfig::Serialize(CArchive& ar, int version) {
	// This is only called in CServerItemObj.  To increment version up the version of CServerItemObj
	// then pass in the version as a param.  This is necessary because this class doesn't derive from
	// CObject, and I can't change that without messing up the old file versions.
	// version 0 - origional
	// version 1 - type changed from BYTE to const BASE_OR_GLID& for equippable heads.
	// version 2 - added missing animGlid and new CustomTexture
	if (ar.IsStoring()) {
		size_t numItems = getItemCount();
		BYTE itemCountByte = (BYTE)numItems;
		ar << itemCountByte;

		for (size_t i = 0; i < numItems; ++i) {
			CharConfigItem* pItem = getItemByIndex(i);
			assert(pItem != nullptr);

			GLID_OR_BASE_SERIAL bogs = (GLID_OR_BASE_SERIAL)pItem->getGlidOrBase();
			ar << bogs; // drf - legacy type (UINT64)

			size_t numSlots = pItem->getSlotCount();
			ar << (BYTE)numSlots;
			ar << pItem->getAnimationGlid();
			ar << CStringA(pItem->getTexUrl().c_str());

			for (size_t s = 0; s < numSlots; ++s) {
				CharConfigItemSlot* pSlot = pItem->getSlot(s);
				assert(pSlot != nullptr);
				ar << pSlot->m_meshId
				   << pSlot->m_matlId
				   << pSlot->m_r
				   << pSlot->m_g
				   << pSlot->m_b;
			}
		}
	} else {
		BYTE itemCountByte = 0;
		ar >> itemCountByte;
		size_t numItems = (size_t)itemCountByte;

		for (size_t i = 0; i < numItems; ++i) {
			GLID_OR_BASE glidOrBase = GOB_BASE_ITEM;
			if (version == 0) {
				// Original version used byte for apd index this has been replaced by BASE_OR_GLID glids
				BYTE apdType;
				ar >> apdType;

				glidOrBase = (GLID_OR_BASE)apdType;
				ASSERT(CGlobalInventoryObjList::GetInstance()); // If hit, fix your autoexec.xml to allow GIL loaded before IGN,
				if (CGlobalInventoryObjList::GetInstance()) {
					// Translate legacy APD type to GLID if any
					glidOrBase = (GLID_OR_BASE)CGlobalInventoryObjList::GetInstance()->GetGLIDByLegacyAttachmentType(apdType);
				}
			} else {
				GLID_OR_BASE_SERIAL bogs = (GLID_OR_BASE_SERIAL)GOB_BASE_ITEM;
				ar >> bogs; // drf - legacy type (UINT64)
				glidOrBase = bogs;
			}

			BYTE slotCountByte = 0;
			ar >> slotCountByte;
			size_t numSlots = (size_t)slotCountByte;

			// Add new item
			CharConfigItem* pItem = addItem(glidOrBase, numSlots);
			assert(pItem != nullptr);

			if (version >= 2) {
				GLID glidAnim;
				ar >> glidAnim;
				pItem->setAnimationGlid(glidAnim);

				CStringA textureUrlStr;
				ar >> textureUrlStr;
				pItem->setTexUrl((const char*)textureUrlStr);
			}

			for (size_t s = 0; s < numSlots; ++s) {
				CharConfigItemSlot* pSlot = pItem->getSlot(s);
				assert(pSlot != nullptr);
				ar >> pSlot->m_meshId >> pSlot->m_matlId >> pSlot->m_r >> pSlot->m_g >> pSlot->m_b;
			}
		}

		assert(getItemCount() == numItems);
	}
	return ar;
}
#endif

bool CharConfig::extractFromEvent(IEvent* ece) {
	int numItems = 0;
	int customRGBCount = 0;
	int customTextureCount = 0;

	(*ece->InBuffer()) >> numItems;
	(*ece->InBuffer()) >> customRGBCount;
	(*ece->InBuffer()) >> customTextureCount;

	for (int i = 0; i < numItems; i++) {
		int glid;
		int numSlots;
		(*ece->InBuffer()) >> glid >> numSlots;

		// BEGIN HEAD CONVERSION TO GLID HACK
		// REMOVE THIS CODE WHEN DATABASE HEAD CONVERSION SCRIPTS RUN
		// THIS IS HACKY AND HARMLESS TO WOK BUT MIGHT AFFECT STAR
		if (glid == 79) {
			glid = 3744; // F_Head_004
		} else if (glid == 80) {
			glid = 3745; // F_Head_005
		} else if (glid == 44) {
			glid = 2993; // F_Head_African
		} else if (glid == 43) {
			glid = 2992; // F_Head_Asian
		} else if (glid == 42) {
			glid = 2991; // F_Head_Caucasian
		} else if (glid == 77) {
			glid = 3720; // M_Head_004
		} else if (glid == 78) {
			glid = 3746; // M_Head_005
		} else if (glid == 47) {
			glid = 2990; // M_Head_Africa
		} else if (glid == 46) {
			glid = 2989; // M_Head_Asian
		} else if (glid == 45) {
			glid = 2988; // M_Head_Caucasian
		}
		// END HEAD GLID HACK

		CharConfigItem* pItem = addItem(glid, numSlots);
		assert(pItem != nullptr);

		if (pItem != nullptr) {
			for (int s = 0; s < numSlots; s++) {
				CharConfigItemSlot* pSlot = pItem->getSlot(s);
				assert(pSlot != nullptr);
				(*ece->InBuffer()) >> pSlot->m_meshId;
				(*ece->InBuffer()) >> pSlot->m_matlId;
			}
		}
	}

	if (customRGBCount > 0) {
		for (int i = 0; i < customRGBCount; i++) {
			int glid, slotIndex;
			(*ece->InBuffer()) >> glid >> slotIndex;
			CharConfigItem* pItem = getItemByGlidOrBase(glid);
			assert(pItem != nullptr);
			if (!pItem || (slotIndex >= (int)pItem->getSlotCount()) || (slotIndex < 0))
				continue;

			CharConfigItemSlot* pSlot = pItem->getSlot(slotIndex);
			assert(pSlot != nullptr);
			(*ece->InBuffer()) >> pSlot->m_r >> pSlot->m_g >> pSlot->m_b;
		}
	}

	if (customTextureCount > 0) {
		for (int i = 0; i < customTextureCount; i++) {
			int glid;
			std::string url;
			(*ece->InBuffer()) >> glid >> url;

			CharConfigItem* pItem = getItemByGlidOrBase(glid);
			assert(pItem != nullptr);
			if (pItem != nullptr) {
				pItem->setTexUrl(url);
			}
		}
	}

	return true;
}

void CharConfig::insertIntoEvent(IEvent* e) const {
	size_t customColorCount = 0;
	size_t customTextureCount = 0;

	// count of equip items
	size_t numItems = getItemCount();
	(*e->OutBuffer()) << (BYTE)numItems;

	for (size_t i = 0; i < numItems; i++) {
		const CharConfigItem* pItem = getItemByIndex(i);
		assert(pItem != nullptr);

		for (size_t s = 0; s < pItem->getSlotCount(); s++) {
			const CharConfigItemSlot* pSlot = pItem->getSlot(s);
			assert(pSlot != nullptr);
			if (pSlot->m_r >= 0 || pSlot->m_g >= 0 || pSlot->m_b >= 0) {
				customColorCount++;
			}
		}
		if (!pItem->getTexUrl().empty()) {
			customTextureCount++;
		}
	}

	(*e->OutBuffer()) << (int)customColorCount;
	(*e->OutBuffer()) << (int)customTextureCount;

	if (numItems <= 0)
		return;

	for (size_t i = 0; i < numItems; i++) {
		const CharConfigItem* pItem = getItemByIndex(i);
		assert(pItem != nullptr);

		int baseOrGlidInt = (int)pItem->getGlidOrBase(); // drf - legacy type (int)
		(*e->OutBuffer()) << baseOrGlidInt;
		auto numSlots = pItem->getSlotCount();
		(*e->OutBuffer()) << (BYTE)numSlots;

		for (size_t s = 0; s < numSlots; s++) {
			const CharConfigItemSlot* pSlot = pItem->getSlot(s);
			assert(pSlot != nullptr);
			(*e->OutBuffer()) << pSlot->m_meshId;
			(*e->OutBuffer()) << pSlot->m_matlId;
		}
	}

	for (size_t i = 0; i < numItems; i++) {
		const CharConfigItem* pItem = getItemByIndex(i);
		assert(pItem != nullptr);

		for (size_t s = 0; s < pItem->getSlotCount(); s++) {
			const CharConfigItemSlot* pSlot = pItem->getSlot(s);
			assert(pSlot != nullptr);
			if (pSlot->m_r >= 0 || pSlot->m_g >= 0 || pSlot->m_b >= 0) {
				(*e->OutBuffer()) << (GLID_OR_BASE_SERIAL)pItem->getGlidOrBase(); // drf - legacy type (UINT64)
				(*e->OutBuffer()) << (BYTE)s;
				(*e->OutBuffer()) << pSlot->m_r;
				(*e->OutBuffer()) << pSlot->m_g;
				(*e->OutBuffer()) << pSlot->m_b;
			}
		}
	}

	for (size_t i = 0; i < numItems; i++) {
		const CharConfigItem* pItem = getItemByIndex(i);
		assert(pItem != nullptr);
		if (!pItem->getTexUrl().empty()) {
			(*e->OutBuffer()) << (GLID_OR_BASE_SERIAL)pItem->getGlidOrBase(); // drf - legacy type (UINT64)
			(*e->OutBuffer()) << CStringA(pItem->getTexUrl().c_str());
		}
	}
}

void CharConfigItemSlot::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CharConfigItem::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_slots);
		pMemSizeGadget->AddObject(m_texUrl);
	}
}

void CharConfig::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_items);
	}
}

} // namespace KEP
