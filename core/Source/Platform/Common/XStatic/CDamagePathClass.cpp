///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#define D3D_OVERLOADS
#include <afxtempl.h>
#include "d3d9.h"
#include "CDamagePathClass.h"
#include "common\include\IMemSizeGadget.h"

namespace KEP {

CDamagePathObj::CDamagePathObj() {
	m_boneIndex = 0;
	m_lastPosition.x = 0.0f;
	m_lastPosition.y = 0.0f;
	m_lastPosition.z = 0.0f;
	m_radius = 0.0f;
	m_enableOnAnimationIndex = -1;
	m_damageValue = 0;
	m_contactMade = FALSE;
	m_damageDelay = 200;
	m_damageDelayStamp = 0;
	m_damageChannel = 0;
	m_criteriaForSkillUse = -1;
	m_damageSpecific = -1;
	m_enableOnAnimationType = eAnimType::Invalid;
	m_enableOnAnimationVersion = 0;
}

void CDamagePathObj::Clone(CDamagePathObj** copy) {
	CDamagePathObj* copyLocal = new CDamagePathObj();

	copyLocal->m_boneIndex = m_boneIndex;
	copyLocal->m_lastPosition.x = m_lastPosition.x;
	copyLocal->m_lastPosition.y = m_lastPosition.y;
	copyLocal->m_lastPosition.z = m_lastPosition.z;
	copyLocal->m_radius = m_radius;
	copyLocal->m_enableOnAnimationIndex = m_enableOnAnimationIndex;
	copyLocal->m_damageValue = m_damageValue;
	copyLocal->m_contactMade = m_contactMade;
	copyLocal->m_damageDelay = m_damageDelay;
	copyLocal->m_damageChannel = m_damageChannel;
	copyLocal->m_criteriaForSkillUse = m_criteriaForSkillUse;
	copyLocal->m_damageSpecific = m_damageSpecific;
	copyLocal->m_enableOnAnimationType = m_enableOnAnimationType;
	copyLocal->m_enableOnAnimationVersion = m_enableOnAnimationVersion;

	*copy = copyLocal;
}

void CDamagePathObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_boneIndex
		   << m_lastPosition.x
		   << m_lastPosition.y
		   << m_lastPosition.z
		   << m_radius
		   << m_enableOnAnimationIndex
		   << m_damageValue
		   << m_contactMade
		   << m_damageChannel
		   << m_criteriaForSkillUse
		   << m_damageSpecific; //migrated

		ar << (int)m_enableOnAnimationType
		   << m_enableOnAnimationVersion;
	} else {
		UINT version = ar.GetObjectSchema();

		ar >> m_boneIndex >> m_lastPosition.x >> m_lastPosition.y >> m_lastPosition.z >> m_radius >> m_enableOnAnimationIndex >> m_damageValue >> m_contactMade >> m_damageChannel >> m_criteriaForSkillUse >> m_damageSpecific; //migrated

		//m_damageSpecific    = -1;
		m_damageDelay = 200;
		m_damageDelayStamp = 0;
		//m_criteriaForSkillUse = -1;

		if (version >= 1) {
			int animType;
			ar >> animType >> m_enableOnAnimationVersion;
			m_enableOnAnimationType = (eAnimType)animType;
		}
	}
}

void CDamagePathObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDamagePathObj* csObj = (CDamagePathObj*)GetNext(posLoc);
		delete csObj;
		csObj = 0;
	}
	RemoveAll();
}

void CDamagePathObjList::Clone(CDamagePathObjList** copy) {
	CDamagePathObjList* copyLocal = new CDamagePathObjList;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CDamagePathObj* csObj = (CDamagePathObj*)GetNext(posLoc);
		CDamagePathObj* clone = NULL;
		csObj->Clone(&clone);
		copyLocal->AddTail(clone);
	}
	*copy = copyLocal;
}

void CDamagePathObj::SetAnimationInfo(eAnimType animType, int animVer) {
	m_enableOnAnimationType = animType;
	m_enableOnAnimationVersion = animVer;
	m_enableOnAnimationIndex = -1; // Invalidate legacy animation index
}

void CDamagePathObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);
	}
}

void CDamagePathObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CDamagePathObj* pCDamagePathObj = static_cast<const CDamagePathObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCDamagePathObj);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CDamagePathObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CDamagePathObjList, CObList)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CDamagePathObj, IDS_DAMAGEPATHOBJ_NAME)
GETSET_RANGE(m_boneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, BONEINDEX, INT_MIN, INT_MAX)
GETSET_D3DVECTOR(m_lastPosition, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, LASTPOSITION)
GETSET_RANGE(m_radius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, RADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_enableOnAnimationIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, ENABLEONANIMATIONINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_damageValue, gsInt, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, DAMAGEVALUE, INT_MIN, INT_MAX)
GETSET(m_contactMade, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, CONTACTMADE)
GETSET_RANGE(m_damageChannel, gsInt, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, DAMAGECHANNEL, INT_MIN, INT_MAX)
GETSET_RANGE(m_criteriaForSkillUse, gsInt, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, CRITERIAFORSKILLUSE, INT_MIN, INT_MAX)
GETSET_RANGE(m_damageSpecific, gsInt, GS_FLAGS_DEFAULT, 0, 0, DAMAGEPATHOBJ, DAMAGESPECIFIC, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP
