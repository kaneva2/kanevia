///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GenUtils.h"

using namespace std;

namespace KEP {

void GetShortNames(const string* pathSource, const char* subDirSource,
	string* pathDest, string* subDirDest) {
	// mphears THINGS TO FIX THIS CODE
	//   1) change find_last_of checks to compare against npos, not 0
	//   2) change find_last_of to use "\\/" for correct forward slash handling
	// Doing either of these requires fixing code in TextureManager that relies on these weirdnesses.

	if (!pathDest || !pathSource)
		return;

	int i = pathSource->find_last_of('\\');

	if (i == 0)
		i = pathSource->find_last_of('/');

	if (i != 0) {
		i++;
		(*pathDest) = pathSource->substr(i, pathSource->size() - i);
	} else {
		(*pathDest) = (*pathSource);
	}

	if (!subDirDest || !subDirSource)
		return;

	string subDir(subDirSource);

	i = subDir.find_last_of('\\');

	if (i == 0)
		i = subDir.find_last_of('/');

	string finalSubString;

	if (i != 0) {
		i++;
		finalSubString = subDir.substr(i, subDir.size() - i);

		i = finalSubString.find_last_of('.');

		string ext = finalSubString.substr(i + 1, finalSubString.size() - 1);

		(*subDirDest) = finalSubString.substr(0, i);

		//	If the file has an RLB extension, then we should add the RLB tag to the directory to search through
		if (ext == "RLB")
			(*subDirDest) += ext;

		//	Add final slash
		(*subDirDest) += '\\';
	} else {
		(*subDirDest) = subDirSource;
		(*subDirDest) += '\\';
	}

	CStringA stringReady((*subDirDest).c_str());
	stringReady.Replace("---", "\\");

	(*subDirDest) = stringReady.GetString();
}

// Once GetShortNames can be fixed, this "corrected version" can be discarded with calls redirected.
void GetShortNames_CorrectVersion(const string* pathSource, const char* subDirSource,
	string* pathDest, string* subDirDest) {
	if (!pathDest || !pathSource)
		return;

	int i = pathSource->find_last_of('\\');

	if (i == string::npos)
		i = pathSource->find_last_of('/');

	if (i != string::npos) {
		i++;
		(*pathDest) = pathSource->substr(i, pathSource->size() - i);
	} else {
		(*pathDest) = (*pathSource);
	}

	if (!subDirDest || !subDirSource)
		return;

	string subDir(subDirSource);

	i = subDir.find_last_of('\\');

	if (i == string::npos)
		i = subDir.find_last_of('/');

	string finalSubString;

	if (i != string::npos) {
		i++;
		finalSubString = subDir.substr(i, subDir.size() - i);

		i = finalSubString.find_last_of('.');

		string ext = finalSubString.substr(i + 1, finalSubString.size() - 1);

		(*subDirDest) = finalSubString.substr(0, i);

		//	If the file has an RLB extension, then we should add the RLB tag to the directory to search through
		if (ext == "RLB")
			(*subDirDest) += ext;

		//	Add final slash
		(*subDirDest) += '\\';
	} else {
		(*subDirDest) = subDirSource;
		(*subDirDest) += '\\';
	}

	CStringA stringReady((*subDirDest).c_str());
	stringReady.Replace("---", "\\");

	(*subDirDest) = stringReady.GetString();
}

} // namespace KEP