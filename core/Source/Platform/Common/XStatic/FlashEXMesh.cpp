///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <FlashEXMesh.h>
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "CStateManagementClass.h"
#include "TextureDatabase.h"
#include "ReMesh.h"
#include "ReMaterial.h"
#include "../../Common/include/IMemSizeGadget.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

// static
std::shared_ptr<FlashEXMesh> FlashEXMesh::Create(
	IFlashSystem* pFlashSystem,
	CStateManagementObj* stateManager,
	TextureDatabase* textureDatabase,
	HWND parent,
	const MediaParams& mediaParams,
	uint32_t width, uint32_t height,
	bool loop, bool wantInput,
	int32_t x, int32_t y) {
	// Flash Globally Enabled ?
	if (!GetEnabledGlobal()) {
		LogError("FLASH DISABLED");
		return nullptr;
	}

	// Create New Flash Mesh With Media Params
	std::shared_ptr<FlashEXMesh> pFXM(new FlashEXMesh(stateManager, textureDatabase), [](FlashEXMesh* p) { delete p; });
	if (!pFXM->Initialize(pFlashSystem, parent, mediaParams, wantInput, x, y, width, height)) {
		LogError("FlashEXMesh::Initialize() FAILED");
		return nullptr;
	}

	return pFXM;
}

void FlashEXMesh::ScrapeTexture(ReD3DX9DeviceState* devState) {
	if (!devState || !GetEnabledGlobal())
		return;

	// Scrape Texture & Update Mesh if texture is ready.
	UpdateD3DTextureWithAvailableData(devState);

	// DRF - Limit 100 requested Scrapes/Sec
	if (m_texTimer.ElapsedMs() < (1000.0 / 100.0))
		return;
	m_texTimer.Reset();

	UpdateBitmapAsync();
}

int FlashEXMesh::Render(LPDIRECT3DDEVICE9 pd3dDevice, CEXMeshObj* mesh, int vertexType) {
	if (!pd3dDevice || !mesh || !m_stateManager || !m_textureDatabase || !GetEnabledGlobal() || !GetTexture())
		return 0;

	// Render Texture
	m_stateManager->SetMaterial(&mesh->m_materialObject->m_useMaterial);
	pd3dDevice->SetTexture(0, GetTexture());
	m_stateManager->SetDiffuseSource(mesh->m_abVertexArray.m_advancedFixedMode);
	m_stateManager->SetVertexType(mesh->m_abVertexArray.m_uvCount);
	int ret = mesh->m_abVertexArray.Render(pd3dDevice, vertexType);
	m_stateManager->SetTextureState(-1, 0, m_textureDatabase);
	return ret;
}

void FlashEXMesh::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
		m_stateManager			// CStateManagementObj
		m_textureDatabase		// TextureDatabase*
		---------------------------------------------------------------------*/
	}
}

} // namespace KEP
