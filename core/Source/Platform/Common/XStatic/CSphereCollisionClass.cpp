///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#define D3D_OVERLOADS
#include <afxtempl.h>
#include "d3d9.h"
#include "CSphereCollisionClass.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

CSphereCollisionObj::CSphereCollisionObj() {
	m_boneIndex = 0;
	m_sphereRadius = 0.0f;
	m_damageMultiplier = 1.0f;
	m_unscaledSphereRadius = 0.0f;
}

void CSphereCollisionObj::Clone(CSphereCollisionObj** copy) {
	CSphereCollisionObj* copyLocal = new CSphereCollisionObj();
	copyLocal->m_boneIndex = m_boneIndex;
	copyLocal->m_damageMultiplier = m_damageMultiplier;
	copyLocal->m_sphereRadius = m_sphereRadius;
	copyLocal->m_unscaledSphereRadius = m_unscaledSphereRadius;
	copyLocal->m_comment = m_comment;

	*copy = copyLocal;
}

void CSphereCollisionObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_boneIndex
		   << m_unscaledSphereRadius
		   << m_damageMultiplier
		   << m_comment
		   << m_sphereRadius;
	} // if..ar.IsStoring
	else {
		int version;

		version = ar.GetObjectSchema();
		ar >> m_boneIndex >> m_unscaledSphereRadius >> m_damageMultiplier >> m_comment;

		if (version >= 1) {
			ar >> m_sphereRadius;
		} // if..version
		else {
			m_sphereRadius = m_unscaledSphereRadius;
		} // if..else..version
	} // if..else..ar.IsStoring
}

void CSphereCollisionObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_comment);
	}
}

void CSphereCollisionObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSphereCollisionObj* csObj = (CSphereCollisionObj*)GetNext(posLoc);
		delete csObj;
		csObj = 0;
	}
	RemoveAll();
}

void CSphereCollisionObjList::Clone(CSphereCollisionObjList** copy) {
	CSphereCollisionObjList* copyLocal = new CSphereCollisionObjList;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CSphereCollisionObj* csObj = (CSphereCollisionObj*)GetNext(posLoc);
		CSphereCollisionObj* clone = NULL;
		csObj->Clone(&clone);
		copyLocal->AddTail(clone);
	}
	*copy = copyLocal;
}

void CSphereCollisionObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CSphereCollisionObj* pSphereCollisionObj = static_cast<const CSphereCollisionObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pSphereCollisionObj);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CSphereCollisionObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CSphereCollisionObjList, CObList)

BEGIN_GETSET_IMPL(CSphereCollisionObj, IDS_SPHERECOLLISIONOBJ_NAME)
GETSET_RANGE(m_boneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, SPHERECOLLISIONOBJ, BONEINDEX, INT_MIN, INT_MAX)
GETSET_RANGE(m_sphereRadius, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SPHERECOLLISIONOBJ, SPHERERADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_damageMultiplier, gsFloat, GS_FLAGS_DEFAULT, 0, 0, SPHERECOLLISIONOBJ, DAMAGEMULTIPLIER, FLOAT_MIN, FLOAT_MAX)
GETSET_MAX(m_comment, gsCString, GS_FLAGS_DEFAULT, 0, 0, SPHERECOLLISIONOBJ, COMMENT, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP
