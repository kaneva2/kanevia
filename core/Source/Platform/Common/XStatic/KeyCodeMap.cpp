///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "KeyCodeMap.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/key_extractors.hpp>

#define DIRECTINPUT_VERSION 0x0800 // This is the latest version our header currently supports. If we don't specify, we get a warning.
#include <dinput.h> // For DirectX key codes

namespace KEP {

namespace {
struct StringCompareIgnoreCase {
	bool operator()(const char* left, const char* right) const {
		return stricmp(left, right) < 0;
	}
};
} // namespace

struct KeyCodeMap::MapType : public boost::multi_index_container<
								 KeyDefinition,
								 boost::multi_index::indexed_by<
									 boost::multi_index::ordered_unique<
										 boost::multi_index::member<KeyDefinition, const char*, &KeyDefinition::m_pszName>, StringCompareIgnoreCase>,
									 boost::multi_index::ordered_non_unique<
										 boost::multi_index::member<KeyDefinition, int, &KeyDefinition::m_iKeyCode>>>> {
};

static std::map<const char*, std::vector<const char*>, StringCompareIgnoreCase> s_KeyGroupMap = {
	{ "shift", { "lshift", "rshift" } },
	{ "control", { "lcontrol", "rcontrol" } },
	{ "ctrl", { "lcontrol", "rcontrol" } },
	{ "ctl", { "lcontrol", "rcontrol" } },
	{ "alt", { "lalt", "ralt" } },
};

const std::vector<const char*>* KeyGroupNameToKeyNames(const char* pszKeyGroupName) {
	auto itr = s_KeyGroupMap.find(pszKeyGroupName);
	if (itr != s_KeyGroupMap.end())
		return &itr->second;
	return nullptr;
}

// Returns -1 for invalid key name.
int KeyCodeMap::KeyNameToKeyCode(const char* pszKeyName) const {
	int retVal = -1;
	KeyNameToKeyCode(pszKeyName, out(retVal));
	return retVal;
}

bool KeyCodeMap::KeyNameToKeyCode(const char* pszKeyName, Out<int> keyCode) const {
	auto itr = m_pMap->get<0>().find(pszKeyName);
	if (itr == m_pMap->get<0>().end())
		return false;
	keyCode.set(itr->m_iKeyCode);
	return true;
}

const char* KeyCodeMap::KeyCodeToKeyName(int iKeyCode, const char* pszResultForInvalidKeyCode) const {
	auto itr = m_pMap->get<1>().find(iKeyCode);
	if (itr == m_pMap->get<1>().end())
		return pszResultForInvalidKeyCode;
	return itr->m_pszName;
}

void KeyCodeMap::GetAllKeyCodes(Out<std::vector<int>> aKeyCodes) const {
	std::vector<int>& aKeys = aKeyCodes.get();
	for (auto& mapEntry : *m_pMap)
		aKeys.push_back(mapEntry.m_iKeyCode);
	std::sort(aKeys.begin(), aKeys.end());
	aKeys.erase(std::unique(aKeys.begin(), aKeys.end()), aKeys.end());
}

void KeyCodeMap::GetAllKeyNames(Out<std::vector<const char*>> aKeyNames) const {
	std::vector<const char*>& aKeys = aKeyNames.get();
	for (auto& mapEntry : *m_pMap)
		aKeys.push_back(mapEntry.m_pszName);
}

void KeyCodeMap::Init(const KeyDefinition* pKeyDefinitions, size_t nKeyDefinitions) {
	m_pMap = std::make_unique<MapType>();

	for (size_t i = 0; i < nKeyDefinitions; ++i) {
		const KeyDefinition& keyDef = pKeyDefinitions[i];
		if (keyDef.m_iKeyCode == -1)
			continue;
		auto ib = m_pMap->insert(keyDef);
		if (!ib.second)
			ASSERT(false); // Multiple keys with same name. (or redundant entry)
	}
}

// DirectInput key codes
KeyDefinition s_aKeyCodeMappingDirectInput[] = {
	{
		"ESCAPE",
		DIK_ESCAPE,
	},
	{
		"1",
		DIK_1,
	},
	{
		"2",
		DIK_2,
	},
	{
		"3",
		DIK_3,
	},
	{
		"4",
		DIK_4,
	},
	{
		"5",
		DIK_5,
	},
	{
		"6",
		DIK_6,
	},
	{
		"7",
		DIK_7,
	},
	{
		"8",
		DIK_8,
	},
	{
		"9",
		DIK_9,
	},
	{
		"0",
		DIK_0,
	},
	{
		"MINUS",
		DIK_MINUS,
	}, // - on main keyboard
	{
		"EQUALS",
		DIK_EQUALS,
	},
	{
		"BACKSPACE",
		DIK_BACK,
	}, // backspace
	{
		"TAB",
		DIK_TAB,
	},
	{
		"Q",
		DIK_Q,
	},
	{
		"W",
		DIK_W,
	},
	{
		"E",
		DIK_E,
	},
	{
		"R",
		DIK_R,
	},
	{
		"T",
		DIK_T,
	},
	{
		"Y",
		DIK_Y,
	},
	{
		"U",
		DIK_U,
	},
	{
		"I",
		DIK_I,
	},
	{
		"O",
		DIK_O,
	},
	{
		"P",
		DIK_P,
	},
	{
		"LBRACKET",
		DIK_LBRACKET,
	},
	{
		"RBRACKET",
		DIK_RBRACKET,
	},
	{
		"RETURN",
		DIK_RETURN,
	}, // Enter on main keyboard
	{
		"ENTER",
		DIK_RETURN,
	}, // Enter on main keyboard
	{
		"LCONTROL",
		DIK_LCONTROL,
	},
	{
		"LCTRL",
		DIK_LCONTROL,
	},
	{
		"LCTL",
		DIK_LCONTROL,
	},
	{
		"A",
		DIK_A,
	},
	{
		"S",
		DIK_S,
	},
	{
		"D",
		DIK_D,
	},
	{
		"F",
		DIK_F,
	},
	{
		"G",
		DIK_G,
	},
	{
		"H",
		DIK_H,
	},
	{
		"J",
		DIK_J,
	},
	{
		"K",
		DIK_K,
	},
	{
		"L",
		DIK_L,
	},
	{
		"SEMICOLON",
		DIK_SEMICOLON,
	},
	{
		"APOSTROPHE",
		DIK_APOSTROPHE,
	},
	{
		"GRAVE",
		DIK_GRAVE,
	}, // accent grave
	{
		"LSHIFT",
		DIK_LSHIFT,
	},
	{
		"BACKSLASH",
		DIK_BACKSLASH,
	},
	{
		"Z",
		DIK_Z,
	},
	{
		"X",
		DIK_X,
	},
	{
		"C",
		DIK_C,
	},
	{
		"V",
		DIK_V,
	},
	{
		"B",
		DIK_B,
	},
	{
		"N",
		DIK_N,
	},
	{
		"M",
		DIK_M,
	},
	{
		"COMMA",
		DIK_COMMA,
	},
	{
		"PERIOD",
		DIK_PERIOD,
	}, // . on main keyboard
	{
		"SLASH",
		DIK_SLASH,
	}, // / on main keyboard
	{
		"RSHIFT",
		DIK_RSHIFT,
	},
	{
		"MULTIPLY",
		DIK_MULTIPLY,
	}, // * on numeric keypad
	//{ "LMENU",           DIK_LMENU,           }, // left Alt
	{
		"LALT",
		DIK_LMENU,
	}, // left Alt
	{
		"SPACE",
		DIK_SPACE,
	},
	{
		"CAPSLOCK",
		DIK_CAPITAL,
	},
	{
		"F1",
		DIK_F1,
	},
	{
		"F2",
		DIK_F2,
	},
	{
		"F3",
		DIK_F3,
	},
	{
		"F4",
		DIK_F4,
	},
	{
		"F5",
		DIK_F5,
	},
	{
		"F6",
		DIK_F6,
	},
	{
		"F7",
		DIK_F7,
	},
	{
		"F8",
		DIK_F8,
	},
	{
		"F9",
		DIK_F9,
	},
	{
		"F10",
		DIK_F10,
	},
	{
		"NUMLOCK",
		DIK_NUMLOCK,
	},
	{
		"SCROLL",
		DIK_SCROLL,
	}, // Scroll Lock
	{
		"NUMPAD7",
		DIK_NUMPAD7,
	},
	{
		"NUMPAD8",
		DIK_NUMPAD8,
	},
	{
		"NUMPAD9",
		DIK_NUMPAD9,
	},
	{
		"NUMPADSUBTRACT",
		DIK_SUBTRACT,
	}, // - on numeric keypad
	{
		"NUMPADMINUS",
		DIK_SUBTRACT,
	}, // - on numeric keypad
	{
		"NUMPAD4",
		DIK_NUMPAD4,
	},
	{
		"NUMPAD5",
		DIK_NUMPAD5,
	},
	{
		"NUMPAD6",
		DIK_NUMPAD6,
	},
	{
		"NUMPADADD",
		DIK_ADD,
	}, // + on numeric keypad
	{
		"NUMPADPLUS",
		DIK_ADD,
	}, // + on numeric keypad
	{
		"NUMPAD1",
		DIK_NUMPAD1,
	},
	{
		"NUMPAD2",
		DIK_NUMPAD2,
	},
	{
		"NUMPAD3",
		DIK_NUMPAD3,
	},
	{
		"NUMPAD0",
		DIK_NUMPAD0,
	},
	{
		"DECIMAL",
		DIK_DECIMAL,
	}, // . on numeric keypad
	{
		"OEM_102",
		DIK_OEM_102,
	}, // <> or \| on RT 102-key keyboard (Non-U.S.)
	{
		"F11",
		DIK_F11,
	},
	{
		"F12",
		DIK_F12,
	},
	{
		"F13",
		DIK_F13,
	}, //                     (NEC PC98)
	{
		"F14",
		DIK_F14,
	}, //                     (NEC PC98)
	{
		"F15",
		DIK_F15,
	}, //                     (NEC PC98)
	{
		"KANA",
		DIK_KANA,
	}, // (Japanese keyboard)
	{
		"ABNT_C1",
		DIK_ABNT_C1,
	}, // /? on Brazilian keyboard
	{
		"CONVERT",
		DIK_CONVERT,
	}, // (Japanese keyboard)
	{
		"NOCONVERT",
		DIK_NOCONVERT,
	}, // (Japanese keyboard)
	{
		"YEN",
		DIK_YEN,
	}, // (Japanese keyboard)
	{
		"ABNT_C2",
		DIK_ABNT_C2,
	}, // Numpad . on Brazilian keyboard
	{
		"NUMPADEQUALS",
		DIK_NUMPADEQUALS,
	}, // = on numeric keypad (NEC PC98)
	{
		"PREVTRACK",
		DIK_PREVTRACK,
	}, // Previous Track (DIK_CIRCUMFLEX on Japanese keyboard)
	{
		"AT",
		DIK_AT,
	}, //                     (NEC PC98)
	{
		"COLON",
		DIK_COLON,
	}, //                     (NEC PC98)
	{
		"UNDERLINE",
		DIK_UNDERLINE,
	}, //                     (NEC PC98)
	{
		"KANJI",
		DIK_KANJI,
	}, // (Japanese keyboard)
	{
		"STOP",
		DIK_STOP,
	}, //                     (NEC PC98)
	{
		"AX",
		DIK_AX,
	}, //                     (Japan AX)
	{
		"UNLABELED",
		DIK_UNLABELED,
	}, //                        (J3100)
	{
		"NEXTTRACK",
		DIK_NEXTTRACK,
	}, // Next Track
	{
		"NUMPADENTER",
		DIK_NUMPADENTER,
	}, // Enter on numeric keypad
	{
		"NUMPADRETURN",
		DIK_NUMPADENTER,
	}, // Enter on numeric keypad
	{
		"RCONTROL",
		DIK_RCONTROL,
	},
	{
		"RCTRL",
		DIK_RCONTROL,
	},
	{
		"RCTL",
		DIK_RCONTROL,
	},
	{
		"MUTE",
		DIK_MUTE,
	}, // Mute
	{
		"CALCULATOR",
		DIK_CALCULATOR,
	}, // Calculator
	{
		"PLAYPAUSE",
		DIK_PLAYPAUSE,
	}, // Play / Pause
	{
		"MEDIASTOP",
		DIK_MEDIASTOP,
	}, // Media Stop
	{
		"VOLUMEDOWN",
		DIK_VOLUMEDOWN,
	}, // Volume -
	{
		"VOLUMEUP",
		DIK_VOLUMEUP,
	}, // Volume +
	{
		"WEBHOME",
		DIK_WEBHOME,
	}, // Web home
	{
		"NUMPADCOMMA",
		DIK_NUMPADCOMMA,
	}, // , on numeric keypad (NEC PC98)
	{
		"DIVIDE",
		DIK_DIVIDE,
	}, // / on numeric keypad
	{
		"SYSRQ",
		DIK_SYSRQ,
	},
	//{ "RMENU",           DIK_RMENU,           }, // right Alt
	{
		"RALT",
		DIK_RMENU,
	}, // right Alt
	{
		"PAUSE",
		DIK_PAUSE,
	}, // Pause
	{
		"HOME",
		DIK_HOME,
	}, // Home on arrow keypad
	//{ "UP",              DIK_UP,              }, // UpArrow on arrow keypad
	{
		"UPARROW",
		DIK_UP,
	}, // UpArrow on arrow keypad
	//{ "PRIOR",           DIK_PRIOR,           }, // PgUp on arrow keypad
	{
		"PAGEUP",
		DIK_PRIOR,
	}, // PgUp on arrow keypad
	//{ "LEFT",            DIK_LEFT,            }, // LeftArrow on arrow keypad
	{
		"LEFTARROW",
		DIK_LEFT,
	}, // LeftArrow on arrow keypad
	//{ "RIGHT",           DIK_RIGHT,           }, // RightArrow on arrow keypad
	{
		"RIGHTARROW",
		DIK_RIGHT,
	}, // RightArrow on arrow keypad
	{
		"END",
		DIK_END,
	}, // End on arrow keypad
	//{ "DOWN",            DIK_DOWN,            }, // DownArrow on arrow keypad
	{
		"DOWNARROW",
		DIK_DOWN,
	}, // DownArrow on arrow keypad
	//{ "NEXT",            DIK_NEXT,            }, // PgDn on arrow keypad
	{
		"PAGEDOWN",
		DIK_NEXT,
	}, // PgDn on arrow keypad
	{
		"INSERT",
		DIK_INSERT,
	}, // Insert on arrow keypad
	{
		"DELETE",
		DIK_DELETE,
	}, // Delete on arrow keypad
	{
		"LWIN",
		DIK_LWIN,
	}, // Left Windows key
	{
		"RWIN",
		DIK_RWIN,
	}, // Right Windows key
	{
		"APPS",
		DIK_APPS,
	}, // AppMenu key
	{
		"POWER",
		DIK_POWER,
	}, // System Power
	{
		"SLEEP",
		DIK_SLEEP,
	}, // System Sleep
	{
		"WAKE",
		DIK_WAKE,
	}, // System Wake
	{
		"WEBSEARCH",
		DIK_WEBSEARCH,
	}, // Web Search
	{
		"WEBFAVORITES",
		DIK_WEBFAVORITES,
	}, // Web Favorites
	{
		"WEBREFRESH",
		DIK_WEBREFRESH,
	}, // Web Refresh
	{
		"WEBSTOP",
		DIK_WEBSTOP,
	}, // Web Stop
	{
		"WEBFORWARD",
		DIK_WEBFORWARD,
	}, // Web Forward
	{
		"WEBBACK",
		DIK_WEBBACK,
	}, // Web Back
	{
		"MYCOMPUTER",
		DIK_MYCOMPUTER,
	}, // My Computer
	{
		"MAIL",
		DIK_MAIL,
	}, // Mail
	{
		"MEDIASELECT",
		DIK_MEDIASELECT,
	}, // Media Select
};

// Windows message key codes
KeyDefinition s_aKeyCodeMappingWindowsMessage[] = {
	{
		"MOUSE1",
		VK_LBUTTON,
	},
	{
		"LMOUSE",
		VK_LBUTTON,
	},
	{
		"MOUSE2",
		VK_RBUTTON,
	},
	{
		"RMOUSE",
		VK_RBUTTON,
	},
	{
		"CANCEL",
		VK_CANCEL,
	},
	{
		"MOUSE3",
		VK_MBUTTON,
	},
	{
		"MMOUSE",
		VK_MBUTTON,
	},
	{
		"MOUSE4",
		VK_XBUTTON1,
	},
	{
		"MOUSE5",
		VK_XBUTTON2,
	},
	{
		"BACKSPACE",
		VK_BACK,
	},
	{
		"TAB",
		VK_TAB,
	},
	{
		"CLEAR",
		VK_CLEAR,
	},
	{
		"ENTER",
		VK_RETURN,
	},
	{
		"RETURN",
		VK_RETURN,
	},
	//{ "SHIFT",                   VK_SHIFT,                },
	//{ "CONTROL",                 VK_CONTROL,              },
	//{ "ALT",                     VK_MENU,                 },
	{
		"PAUSE",
		VK_PAUSE,
	},
	{
		"CAPSLOCK",
		VK_CAPITAL,
	},
	{
		"KANA",
		VK_KANA,
	},
	//{ "HANGUEL",                 VK_HANGUEL,              },
	{
		"HANGUL",
		VK_HANGUL,
	},
	{
		"JUNJA",
		VK_JUNJA,
	},
	{
		"FINAL",
		VK_FINAL,
	},
	{
		"HANJA",
		VK_HANJA,
	},
	{
		"KANJI",
		VK_KANJI,
	},
	{
		"ESCAPE",
		VK_ESCAPE,
	},
	{
		"CONVERT",
		VK_CONVERT,
	},
	{
		"NOCONVERT",
		VK_NONCONVERT,
	},
	{
		"ACCEPT",
		VK_ACCEPT,
	},
	{
		"MODECHANGE",
		VK_MODECHANGE,
	},
	{
		"SPACE",
		VK_SPACE,
	},
	{
		"PAGEUP",
		VK_PRIOR,
	},
	{
		"PAGEDOWN",
		VK_NEXT,
	},
	{
		"END",
		VK_END,
	},
	{
		"HOME",
		VK_HOME,
	},
	{
		"LEFTARROW",
		VK_LEFT,
	}, // arrows
	{
		"UPARROW",
		VK_UP,
	},
	{
		"RIGHTARROW",
		VK_RIGHT,
	},
	{
		"DOWNARROW",
		VK_DOWN,
	},
	{
		"SELECT",
		VK_SELECT,
	},
	{
		"PRINT",
		VK_PRINT,
	},
	{
		"EXECUTE",
		VK_EXECUTE,
	},
	{
		"SNAPSHOT",
		VK_SNAPSHOT,
	},
	{
		"INSERT",
		VK_INSERT,
	},
	{
		"DELETE",
		VK_DELETE,
	},
	{
		"HELP",
		VK_HELP,
	},
	{
		"0",
		'0',
	},
	{
		"1",
		'1',
	},
	{
		"2",
		'2',
	},
	{
		"3",
		'3',
	},
	{
		"4",
		'4',
	},
	{
		"5",
		'5',
	},
	{
		"6",
		'6',
	},
	{
		"7",
		'7',
	},
	{
		"8",
		'8',
	},
	{
		"9",
		'9',
	},
	{
		"A",
		'A',
	},
	{
		"B",
		'B',
	},
	{
		"C",
		'C',
	},
	{
		"D",
		'D',
	},
	{
		"E",
		'E',
	},
	{
		"F",
		'F',
	},
	{
		"G",
		'G',
	},
	{
		"H",
		'H',
	},
	{
		"I",
		'I',
	},
	{
		"J",
		'J',
	},
	{
		"K",
		'K',
	},
	{
		"L",
		'L',
	},
	{
		"M",
		'M',
	},
	{
		"N",
		'N',
	},
	{
		"O",
		'O',
	},
	{
		"P",
		'P',
	},
	{
		"Q",
		'Q',
	},
	{
		"R",
		'R',
	},
	{
		"S",
		'S',
	},
	{
		"T",
		'T',
	},
	{
		"U",
		'U',
	},
	{
		"V",
		'V',
	},
	{
		"W",
		'W',
	},
	{
		"X",
		'X',
	},
	{
		"Y",
		'Y',
	},
	{
		"Z",
		'Z',
	},
	{
		"LWIN",
		VK_LWIN,
	},
	{
		"RWIN",
		VK_RWIN,
	},
	{
		"APPS",
		VK_APPS,
	},
	{
		"SLEEP",
		VK_SLEEP,
	},
	{
		"NUMPAD0",
		VK_NUMPAD0,
	},
	{
		"NUMPAD1",
		VK_NUMPAD1,
	},
	{
		"NUMPAD2",
		VK_NUMPAD2,
	},
	{
		"NUMPAD3",
		VK_NUMPAD3,
	},
	{
		"NUMPAD4",
		VK_NUMPAD4,
	},
	{
		"NUMPAD5",
		VK_NUMPAD5,
	},
	{
		"NUMPAD6",
		VK_NUMPAD6,
	},
	{
		"NUMPAD7",
		VK_NUMPAD7,
	},
	{
		"NUMPAD8",
		VK_NUMPAD8,
	},
	{
		"NUMPAD9",
		VK_NUMPAD9,
	},
	{
		"MULTIPLY",
		VK_MULTIPLY,
	},
	{
		"NUMPADADD",
		VK_ADD,
	}, // =/+ key on number pad
	{
		"NUMPADPLUS",
		VK_ADD,
	}, // =/+ key on number pad
	{
		"SEPARATOR",
		VK_SEPARATOR,
	},
	{
		"NUMPADSUBTRACT",
		VK_SUBTRACT,
	},
	{
		"NUMPADMINUS",
		VK_SUBTRACT,
	},
	{
		"DECIMAL",
		VK_DECIMAL,
	},
	{
		"DIVIDE",
		VK_DIVIDE,
	},
	{
		"F1",
		VK_F1,
	},
	{
		"F2",
		VK_F2,
	},
	{
		"F3",
		VK_F3,
	},
	{
		"F4",
		VK_F4,
	},
	{
		"F5",
		VK_F5,
	},
	{
		"F6",
		VK_F6,
	},
	{
		"F7",
		VK_F7,
	},
	{
		"F8",
		VK_F8,
	},
	{
		"F9",
		VK_F9,
	},
	{
		"F10",
		VK_F10,
	},
	{
		"F11",
		VK_F11,
	},
	{
		"F12",
		VK_F12,
	},
	{
		"F13",
		VK_F13,
	},
	{
		"F14",
		VK_F14,
	},
	{
		"F15",
		VK_F15,
	},
	{
		"F16",
		VK_F16,
	},
	{
		"F17",
		VK_F17,
	},
	{
		"F18",
		VK_F18,
	},
	{
		"F19",
		VK_F19,
	},
	{
		"F20",
		VK_F20,
	},
	{
		"F21",
		VK_F21,
	},
	{
		"F22",
		VK_F22,
	},
	{
		"F23",
		VK_F23,
	},
	{
		"F24",
		VK_F24,
	},
	{
		"NUMLOCK",
		VK_NUMLOCK,
	},
	{
		"SCROLLLOCK",
		VK_SCROLL,
	},
	{
		"LSHIFT",
		VK_LSHIFT,
	},
	{
		"RSHIFT",
		VK_RSHIFT,
	},
	{
		"LCONTROL",
		VK_LCONTROL,
	},
	{
		"LCTRL",
		VK_LCONTROL,
	},
	{
		"LCTL",
		VK_LCONTROL,
	},
	{
		"RCONTROL",
		VK_RCONTROL,
	},
	{
		"RCTROL",
		VK_RCONTROL,
	},
	{
		"RCTRL",
		VK_RCONTROL,
	},
	{
		"LALT",
		VK_LMENU,
	},
	{
		"RALT",
		VK_RMENU,
	},
	{
		"BROWSER_BACK",
		VK_BROWSER_BACK,
	},
	{
		"BROWSER_FORWARD",
		VK_BROWSER_FORWARD,
	},
	{
		"BROWSER_REFRESH",
		VK_BROWSER_REFRESH,
	},
	{
		"BROWSER_STOP",
		VK_BROWSER_STOP,
	},
	{
		"BROWSER_SEARCH",
		VK_BROWSER_SEARCH,
	},
	{
		"BROWSER_FAVORITES",
		VK_BROWSER_FAVORITES,
	},
	{
		"BROWSER_HOME",
		VK_BROWSER_HOME,
	},
	{
		"VOLUME_MUTE",
		VK_VOLUME_MUTE,
	},
	{
		"VOLUME_DOWN",
		VK_VOLUME_DOWN,
	},
	{
		"VOLUME_UP",
		VK_VOLUME_UP,
	},
	{
		"MEDIA_NEXT_TRACK",
		VK_MEDIA_NEXT_TRACK,
	},
	{
		"MEDIA_PREV_TRACK",
		VK_MEDIA_PREV_TRACK,
	},
	{
		"MEDIA_STOP",
		VK_MEDIA_STOP,
	},
	{
		"MEDIA_PLAY_PAUSE",
		VK_MEDIA_PLAY_PAUSE,
	},
	{
		"LAUNCH_MAIL",
		VK_LAUNCH_MAIL,
	},
	{
		"LAUNCH_MEDIA_SELECT",
		VK_LAUNCH_MEDIA_SELECT,
	},
	{
		"LAUNCH_APP1",
		VK_LAUNCH_APP1,
	},
	{
		"LAUNCH_APP2",
		VK_LAUNCH_APP2,
	},
	{
		"OEM_1",
		VK_OEM_1,
	},
	{
		"EQUALS",
		VK_OEM_PLUS,
	}, // Equal sign on main keyboard
	{
		"OEM_COMMA",
		VK_OEM_COMMA,
	},
	{
		"MINUS",
		VK_OEM_MINUS,
	}, // Minus sign on main keyboard
	{
		"SUBTRACT",
		VK_OEM_MINUS,
	}, // Minus sign on main keyboard
	{
		"OEM_PERIOD",
		VK_OEM_PERIOD,
	},
	{
		"OEM_2",
		VK_OEM_2,
	},
	{
		"OEM_3",
		VK_OEM_3,
	},
	{
		"OEM_4",
		VK_OEM_4,
	},
	{
		"OEM_5",
		VK_OEM_5,
	},
	{
		"OEM_6",
		VK_OEM_6,
	},
	{
		"OEM_7",
		VK_OEM_7,
	},
	{
		"OEM_8",
		VK_OEM_8,
	},
	{
		"OEM_102",
		VK_OEM_102,
	},
	{
		"PROCESSKEY",
		VK_PROCESSKEY,
	},
	{
		"PACKET",
		VK_PACKET,
	},
	{
		"ATTN",
		VK_ATTN,
	},
	{
		"CRSEL",
		VK_CRSEL,
	},
	{
		"EXSEL",
		VK_EXSEL,
	},
	{
		"EREOF",
		VK_EREOF,
	},
	{
		"PLAY",
		VK_PLAY,
	},
	{
		"ZOOM",
		VK_ZOOM,
	},
	{
		"NONAME",
		VK_NONAME,
	},
	{
		"PA1",
		VK_PA1,
	},

	{
		"NUMPADENTER",
		KEP_VK_NUMPADRETURN,
	}, // Enter on numeric keypad. This is
	{
		"NUMPADRETURN",
		KEP_VK_NUMPADRETURN,
	}, // Enter on numeric keypad
};

const KeyCodeMap s_KeyCodeMapWindowsMessage(s_aKeyCodeMappingWindowsMessage);
const KeyCodeMap s_KeyCodeMapDirectInput(s_aKeyCodeMappingWindowsMessage);

const KeyCodeMap* GetKeyCodeMap(eKeyCodeType keyCodeType) {
	switch (keyCodeType) {
		case eKeyCodeType::DirectInput:
			return &s_KeyCodeMapDirectInput;
		case eKeyCodeType::WindowsMessage:
			return &s_KeyCodeMapWindowsMessage;
		default:
			return nullptr;
	}
}

} // namespace KEP
