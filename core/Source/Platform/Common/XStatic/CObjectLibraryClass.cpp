///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CEXMeshClass.h"
#include "CObjectLibraryClass.h"
#include "IAssetsDatabase.h"
#include "AssetTree.h"
#include "CTriggerClass.h"
#include "CWldObject.h"

namespace KEP {

CObjectLibraryObj::CObjectLibraryObj() {
	m_wldObject = NULL;
}

void CObjectLibraryObj::SafeDelete() {
	if (m_wldObject) {
		m_wldObject->SafeDelete();
		delete m_wldObject;
		m_wldObject = 0;
	}
}

CObjectLibraryObjList::CObjectLibraryObjList() {
	m_altCollisionModel = NULL;
}

BOOL CObjectLibraryObjList::AddObject(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CObjectLibraryObj* newPtr = new CObjectLibraryObj();
	BOOL result = newPtr->InitFromFile(fileName, g_pd3dDevice);
	if (!result) {
		delete newPtr;
		return FALSE;
	}

	AddTail(newPtr);
	return TRUE;
}

BOOL CObjectLibraryObjList::AddCollisionObject(CStringA filename, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	if (0 == IAssetsDatabase::Instance())
		return FALSE;

	CEXMeshObj* tmpMeshObj;
	if (0 == (tmpMeshObj = IAssetsDatabase::Instance()->ImportStaticMesh(filename)))
		return FALSE;

	if (m_altCollisionModel) {
		m_altCollisionModel->SafeDelete();
		delete m_altCollisionModel;
	}
	m_altCollisionModel = new CWldObject(0);
	m_altCollisionModel->m_meshObject = tmpMeshObj;
	m_altCollisionModel->m_meshObject->InitAfterLoad(g_pd3dDevice);
	m_altCollisionModel->m_fileNameRef = filename;
	m_altCollisionModel->m_triggerList = new CTriggerObjectList;
	m_altCollisionModel->CalibrateCenteredBoundingBox((int)m_altCollisionModel->m_meshObject->m_sightBasis);
	return TRUE;
}

void CObjectLibraryObjList::RemoveCollisionObject() {
	if (m_altCollisionModel)
		delete m_altCollisionModel;
	m_altCollisionModel = 0;
}

void CObjectLibraryObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CObjectLibraryObj* spnPtr = (CObjectLibraryObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CObjectLibraryObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_wldObject
		   << m_description;
	} else {
		ar >> m_wldObject >> m_description;
	}
}

void CObjectLibraryObjList::Serialize(CArchive& ar) {
	CObList::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_altCollisionModel
		   << m_description;
	} else {
		ar >> m_altCollisionModel >> m_description;
	}
}

CObjectCollection::CObjectCollection() {
	m_collection = NULL;
}

void CObjectCollection::SafeDelete() {
	if (m_collection) {
		m_collection->SafeDelete();
		delete m_collection;
		m_collection = 0;
	}
}

void CObjectCollectionList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CObjectCollection* spnPtr = (CObjectCollection*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

BOOL CObjectLibraryObj::InitFromFile(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	if (0 == IAssetsDatabase::Instance())
		return FALSE;

	CEXMeshObj* tmpMeshObj;
	if (0 == (tmpMeshObj = IAssetsDatabase::Instance()->ImportStaticMesh(fileName)))
		return FALSE;

	if (m_wldObject) {
		m_wldObject->SafeDelete();
		delete m_wldObject;
	}
	m_wldObject = new CWldObject(0);
	m_wldObject->m_meshObject = tmpMeshObj;
	m_wldObject->m_meshObject->InitAfterLoad(g_pd3dDevice);
	m_wldObject->m_fileNameRef = fileName;
	m_wldObject->m_triggerList = new CTriggerObjectList;
	m_wldObject->CalibrateCenteredBoundingBox(m_wldObject->m_meshObject->m_sightBasis);
	return TRUE;
}

void CObjectCollection::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_collection;
	} else {
		ar >> m_collection;
	}
}

IMPLEMENT_SERIAL(CObjectLibraryObj, CObject, 0)
IMPLEMENT_SERIAL(CObjectLibraryObjList, CObList, 0)
IMPLEMENT_SERIAL(CObjectCollection, CObList, 0)
IMPLEMENT_SERIAL(CObjectCollectionList, CObList, 0)

BEGIN_GETSET_IMPL(CObjectLibraryObj, IDS_OBJECTLIBRARYOBJ_NAME)
GETSET_OBJ_PTR(m_wldObject, GS_FLAGS_DEFAULT, 0, 0, OBJECTLIBRARYOBJ, WLDOBJECT, CWldObject)
GETSET_MAX(m_description, gsCString, GS_FLAGS_DEFAULT, 0, 0, OBJECTLIBRARYOBJ, DESCRIPTION, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP