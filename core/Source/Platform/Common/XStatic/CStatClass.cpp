///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CStatClass.h"
#include "CInventoryClass.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

CStatObj::CStatObj() :
		m_bonusBasis(1.0) {
	m_statName = "UNKNOWN";
	m_capValue = 250.0f;
	m_currentValue = 50.0f;
	m_id = 0;
	m_gainBasis = .1f;
	m_benefitType = 0;
}

void CStatObj::Clone(CStatObj** clone) {
	CStatObj* copyLocal = new CStatObj();
	copyLocal->m_statName = m_statName;
	copyLocal->m_capValue = m_capValue;
	copyLocal->m_currentValue = m_currentValue;
	copyLocal->m_gainBasis = m_gainBasis;
	copyLocal->m_id = m_id;
	copyLocal->m_benefitType = m_benefitType;
	copyLocal->m_bonusBasis = m_bonusBasis;

	*clone = copyLocal;
}

float CStatObj::GetValueIncludingBonusItems(CInventoryObjList* inventory) {
	float returnValue = m_currentValue;

	if (inventory) {
		for (POSITION posLoc = inventory->GetHeadPosition(); posLoc != NULL;) {
			CInventoryObj* invenPtr = (CInventoryObj*)inventory->GetNext(posLoc);
			if (invenPtr->isArmed() &&
				invenPtr->m_itemData && // valid item data
				invenPtr->m_itemData->m_statBonuses) {
				returnValue += ((float)invenPtr->m_itemData->m_statBonuses->GetBonusByID(m_id));
			}
		}
	}

	return returnValue * m_bonusBasis;
}

BOOL CStatObj::UseStat(float useLevel, CInventoryObjList* inventory) {
	float virtualCurrentValue = GetValueIncludingBonusItems(inventory);
	if (virtualCurrentValue > m_capValue) {
		return FALSE;
	}

	if (virtualCurrentValue < useLevel) {
		return FALSE;
	}

	// if(m_currentValue < useLevel)
	// return FALSE;
	int floor = (int)m_currentValue;

	if (m_currentValue == 0.0f) {
		m_currentValue = 1.0f; // increment zeroed stats
	}

	if (m_currentValue > m_capValue) {
		return FALSE;
	}

	float degrationOfMax = 1.0f - (virtualCurrentValue / m_capValue);

	m_currentValue += (degrationOfMax * (useLevel / (virtualCurrentValue * 6) * m_gainBasis));

	int floorCmp = (int)m_currentValue;

	if (floorCmp != floor) {
		// indicate change
		return TRUE;
	} else {
		return FALSE;
	}
}

void CStatObjList::Clone(CStatObjList** clone) {
	CStatObjList* copyLocal = new CStatObjList();

	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		// account list
		CStatObj* skPtr = (CStatObj*)GetNext(posLoc);
		CStatObj* clonePtr = NULL;
		skPtr->Clone(&clonePtr);
		copyLocal->AddTail(clonePtr);
	}

	*clone = copyLocal;
}

CStatObjList::~CStatObjList() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatObj* spnPtr = (CStatObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}

	RemoveAll();
}

float CStatObjList::GetStatValueByID(int id, CInventoryObjList* inventory) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatObj* spnPtr = (CStatObj*)GetNext(posLoc);
		if (spnPtr->m_id == id) {
			return spnPtr->GetValueIncludingBonusItems(inventory);
		}
	}

	return -1;
}

int CStatObjList::GetBenefitBonus(int benefit, CInventoryObjList* inventory) {
	int tally = 0;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatObj* spnPtr = (CStatObj*)GetNext(posLoc);
		if (spnPtr->m_benefitType == benefit) {
			tally += (int)spnPtr->GetValueIncludingBonusItems(inventory);
		}
	}

	return tally;
}

BOOL CStatObjList::UseStatByID(float useLevel, int id, CInventoryObjList* inventory) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatObj* spnPtr = (CStatObj*)GetNext(posLoc);
		if (spnPtr->m_id == id) {
			return spnPtr->UseStat(useLevel, inventory);
		}
	}

	return FALSE;
}

CStatObj* CStatObjList::GetStatByID(int id) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatObj* spnPtr = (CStatObj*)GetNext(posLoc);
		if (spnPtr->m_id == id) {
			return spnPtr;
		}
	}

	return NULL;
}

BOOL CStatObjList::VerifyStatRequirement(float minimumValue, int id, CInventoryObjList* inventory) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatObj* statPtr = (CStatObj*)GetNext(posLoc);
		if (statPtr->m_id == id) {
			if (statPtr->GetValueIncludingBonusItems(inventory) >= minimumValue) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	return FALSE; // changed to FALSE since if you don't have the min stat, you aren't verifyied
}

void CStatObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_statName
		   << m_capValue
		   << m_currentValue
		   << m_id
		   << m_gainBasis
		   << m_benefitType
		   << m_bonusBasis;
	} else {
		int version = ar.GetObjectSchema();
		m_bonusBasis = 1.0;

		ar >> m_statName >> m_capValue >> m_currentValue >> m_id >> m_gainBasis >> m_benefitType;

		if (version > 1) {
			ar >> m_bonusBasis;
		}
	}
}

void CStatObj::SerializeAlloc(CArchive& ar) {
	if (ar.IsStoring()) {
		ar << m_statName
		   << m_capValue
		   << m_currentValue
		   << m_id
		   << m_gainBasis
		   << m_benefitType;
	} else {
		m_bonusBasis = 1.0;
		int version = 2;

		ar >> m_statName >> m_capValue >> m_currentValue >> m_id >> m_gainBasis >> m_benefitType >> version; // DANGER DANGER!! This wasn't originally versioned, so this will blow up unless version 2 was serialized!!

		if (version > 1) {
			ar >> m_bonusBasis;
		}
	}
}

void CStatObjList::SerializeAlloc(CArchive& ar, int loadCount) {
	if (ar.IsStoring()) {
		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CStatObj* elementPtr = (CStatObj*)GetNext(posLoc);
			elementPtr->SerializeAlloc(ar);
		}
	} else {
		for (int i = 0; i < loadCount; i++) {
			CStatObj* elementPtr = new CStatObj();
			elementPtr->SerializeAlloc(ar);
			AddTail(elementPtr);
		}
	}
}

int CStatObjList::GetUniqueID() {
	int id = -1;
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStatObj* elementPtr = (CStatObj*)GetNext(posLoc);
		if (elementPtr->m_id > id)
			id = elementPtr->m_id;
	}

	return id + 1;
}

void CStatObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_statName);
	}
}

void CStatObjList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CStatObj* pCStatObj = static_cast<const CStatObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pCStatObj);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CStatObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CStatObjList, CKEPObList)

BEGIN_GETSET_IMPL(CStatObj, IDS_STATOBJ_NAME)
GETSET_MAX(m_statName, gsCString, GS_FLAG_EXPOSE | GS_FLAG_INSTANCE_NAME, 0, 0, STATOBJ, STATNAME, STRLEN_MAX)
GETSET_RANGE(m_currentValue, gsFloat, GS_FLAGS_DEFAULT, 0, 0, STATOBJ, CURRENTVALUE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_capValue, gsFloat, GS_FLAG_EXPOSE, 0, 0, STATOBJ, CAPVALUE, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_id, gsInt, GS_FLAG_EXPOSE, 0, 0, STATOBJ, ID, INT_MIN, INT_MAX)
GETSET_RANGE(m_gainBasis, gsFloat, GS_FLAG_EXPOSE, 0, 0, STATOBJ, GAINBASIS, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_benefitType, gsInt, GS_FLAG_EXPOSE, 0, 0, STATOBJ, BENEFITTYPE, INT_MIN, INT_MAX)
GETSET_RANGE(m_bonusBasis, gsFloat, GS_FLAGS_DEFAULT, 0, 0, STATOBJ, BONUSBASIS, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP