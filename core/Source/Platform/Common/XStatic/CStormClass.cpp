///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS

#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>
#include "CStormClass.h"
#include "IClientEngine.h"
#include "EnvParticleSystem.h"

namespace KEP {

CStormObj::CStormObj() :
		m_strikeVariationGetSet(&m_strikeVariationArray, &m_VariationLightningCount) {
	m_stormName = "NA";
	m_runtimeParticleId = 0;
	m_particleSystemRef = -1;
	m_duration = 20000;
	m_stormTransition = 1000;
	m_frequency = 10;
	m_fogRedMod = .5f;
	m_fogGreenMod = .5f;
	m_fogBlueMod = .5f;
	m_sunColorRedMod = .5f;
	m_sunColorGreenMod = .5f;
	m_sunColorBlueMod = .5f;
	m_VariationLightningCount = 0;
	m_totalStrikeCountPerMinute = 50;
	m_lightningStamp = 0;
	m_strikeDuration = 500;
	m_strikeVariationArray = NULL;
	m_strikePanelWidth = 300;
	m_strikeSpawnHeight = 2000;
	m_strikeDepth = 2500;
	m_stormRadius = 3000.0;
}

BOOL CStormObj::RemoveLightningStrike(int index) {
	if (!m_strikeVariationArray)
		return FALSE;

	if (m_VariationLightningCount <= index)
		return FALSE;

	StrikeVariation* strikeVariationTemp = 0;

	if ((m_VariationLightningCount - 1) > 0) {
		strikeVariationTemp = new StrikeVariation[m_VariationLightningCount - 1];
		//transfer array
		int tmpIdx = 0;
		for (int loop = 0; loop < m_VariationLightningCount; loop++) {
			if (index != loop) {
				strikeVariationTemp[tmpIdx].textureByIndex = m_strikeVariationArray[loop].textureByIndex;
				lstrcpyA(strikeVariationTemp[tmpIdx].textureName, m_strikeVariationArray[loop].textureName);
				strikeVariationTemp[tmpIdx].m_sndID = m_strikeVariationArray[loop].m_sndID;
				strikeVariationTemp[tmpIdx].m_soundFactor = m_strikeVariationArray[loop].m_soundFactor;
				tmpIdx++;
			}
		}
	}
	if (m_strikeVariationArray) { //delete existing
		delete[] m_strikeVariationArray;
		m_strikeVariationArray = 0;
	} //end delete existing
	m_VariationLightningCount--;
	m_strikeVariationArray = strikeVariationTemp;
	return TRUE;
}

void CStormObj::AddLightningStrike(char textureName[256],
	int sndID,
	float soundFactor) {
	StrikeVariation* strikeVariationTemp = new StrikeVariation[m_VariationLightningCount + 1];
	int loop;
	for (loop = 0; loop < m_VariationLightningCount; loop++) {
		strikeVariationTemp[loop].textureByIndex = m_strikeVariationArray[loop].textureByIndex;
		lstrcpyA(strikeVariationTemp[loop].textureName, m_strikeVariationArray[loop].textureName);
		strikeVariationTemp[loop].m_sndID = m_strikeVariationArray[loop].m_sndID;
		strikeVariationTemp[loop].m_soundFactor = m_strikeVariationArray[loop].m_soundFactor;
	}
	lstrcpyA(strikeVariationTemp[m_VariationLightningCount].textureName, textureName);
	strikeVariationTemp[loop].m_sndID = sndID;
	strikeVariationTemp[loop].m_soundFactor = soundFactor;
	strikeVariationTemp[m_VariationLightningCount].textureByIndex = 0;
	if (m_strikeVariationArray) { //delete existing
		delete[] m_strikeVariationArray;
		m_strikeVariationArray = 0;
	} //end delete existing
	m_VariationLightningCount++;

	m_strikeVariationArray = strikeVariationTemp;
}

void CStormObj::Clone(CStormObj** clone) {
	CStormObj* copyLocal = new CStormObj();
	copyLocal->m_stormName = m_stormName;
	copyLocal->m_particleSystemRef = m_particleSystemRef;
	copyLocal->m_duration = m_duration;
	copyLocal->m_stormTransition = m_stormTransition;
	copyLocal->m_frequency = m_frequency;
	copyLocal->m_fogRedMod = m_fogRedMod;
	copyLocal->m_fogGreenMod = m_fogGreenMod;
	copyLocal->m_fogBlueMod = m_fogBlueMod;
	copyLocal->m_sunColorRedMod = m_sunColorRedMod;
	copyLocal->m_sunColorGreenMod = m_sunColorGreenMod;
	copyLocal->m_sunColorBlueMod = m_sunColorBlueMod;
	copyLocal->m_VariationLightningCount = m_VariationLightningCount;
	copyLocal->m_totalStrikeCountPerMinute = m_totalStrikeCountPerMinute;
	copyLocal->m_lightningStamp = m_lightningStamp;
	copyLocal->m_strikeDuration = m_strikeDuration;
	copyLocal->m_strikePanelWidth = m_strikePanelWidth;
	copyLocal->m_strikeSpawnHeight = m_strikeSpawnHeight;
	copyLocal->m_strikeDepth = m_strikeDepth;
	copyLocal->m_stormRadius = m_stormRadius;

	if (m_VariationLightningCount > 0) {
		copyLocal->m_strikeVariationArray = new StrikeVariation[m_VariationLightningCount];
		for (int loop = 0; loop < m_VariationLightningCount; loop++) {
			copyLocal->m_strikeVariationArray[loop].textureByIndex = m_strikeVariationArray[loop].textureByIndex;
			lstrcpyA(copyLocal->m_strikeVariationArray[loop].textureName, m_strikeVariationArray[loop].textureName);
			copyLocal->m_strikeVariationArray[loop].m_sndID = m_strikeVariationArray[loop].m_sndID;
			copyLocal->m_strikeVariationArray[loop].m_soundFactor = m_strikeVariationArray[loop].m_soundFactor;
		}
	}
	*clone = copyLocal;
}

void CStormObj::SafeDelete() {
	if (m_strikeVariationArray) {
		delete[] m_strikeVariationArray;
		m_strikeVariationArray = 0;
	}
	if (m_runtimeParticleId) {
#ifdef _ClientOutput
		auto pICE = IClientEngine::Instance();
		if (pICE && pICE->GetParticleDatabase())
			pICE->GetParticleDatabase()->SetRuntimeEffectStatus(m_runtimeParticleId, -1, -1, 0);
#endif
		m_runtimeParticleId = 0;
	}
}

void CStormObjList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CStormObj* spnPtr = (CStormObj*)GetNext(posLoc);
		spnPtr->SafeDelete();
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CStormObj::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_stormName
		   << m_particleSystemRef
		   << m_duration
		   << m_stormTransition
		   << m_frequency
		   << m_fogRedMod
		   << m_fogGreenMod
		   << m_fogBlueMod
		   << m_sunColorRedMod
		   << m_sunColorGreenMod
		   << m_sunColorBlueMod
		   << m_VariationLightningCount
		   << m_totalStrikeCountPerMinute
		   << m_lightningStamp
		   << m_strikeDuration
		   << m_strikePanelWidth
		   << m_strikeSpawnHeight
		   << m_strikeDepth
		   << m_stormRadius;

		for (int loop = 0; loop < m_VariationLightningCount; loop++) {
			CStringA textureName;
			textureName = m_strikeVariationArray[loop].textureName;
			ar << m_strikeVariationArray[loop].textureByIndex
			   << m_strikeVariationArray[loop].m_sndID
			   << m_strikeVariationArray[loop].m_soundFactor
			   << textureName;
		}
	} else {
		ar >> m_stormName >> m_particleSystemRef >> m_duration >> m_stormTransition >> m_frequency >> m_fogRedMod >> m_fogGreenMod >> m_fogBlueMod >> m_sunColorRedMod >> m_sunColorGreenMod >> m_sunColorBlueMod >> m_VariationLightningCount >> m_totalStrikeCountPerMinute >> m_lightningStamp >> m_strikeDuration >> m_strikePanelWidth >> m_strikeSpawnHeight >> m_strikeDepth >> m_stormRadius;

		if (m_VariationLightningCount > 0)
			m_strikeVariationArray = new StrikeVariation[m_VariationLightningCount];
		for (int loop = 0; loop < m_VariationLightningCount; loop++) {
			CStringA textureName;
			ar >> m_strikeVariationArray[loop].textureByIndex >> m_strikeVariationArray[loop].m_sndID >> m_strikeVariationArray[loop].m_soundFactor >> textureName;

			lstrcpyA(m_strikeVariationArray[loop].textureName, textureName);
		}

		m_runtimeParticleId = 0;
	}
}

IMPLEMENT_SERIAL(CStormObj, CObject, 0)
IMPLEMENT_SERIAL(CStormObjList, CObList, 0)

BEGIN_GETSET_IMPL(CStormObj, IDS_STORMOBJ_NAME)
GETSET_MAX(m_stormName, gsCString, GS_FLAGS_DEFAULT | GS_FLAG_INSTANCE_NAME, 3, 6, STORMOBJ, STORMNAME, STRLEN_MAX)
GETSET_RANGE(m_duration, gsLong, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, DURATION, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_stormTransition, gsLong, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, STORMTRANSITION, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_frequency, gsLong, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, FREQUENCY, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_particleSystemRef, gsInt, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, PARTICLESYSTEMREF, INT_MIN, INT_MAX)
GETSET_RANGE(m_strikeDuration, gsLong, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, STRIKEDURATION, LONG_MIN, LONG_MAX)
GETSET_RANGE(m_totalStrikeCountPerMinute, gsInt, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, TOTALSTRIKECOUNTPERMINUTE, INT_MIN, INT_MAX)
GETSET_RANGE(m_strikePanelWidth, gsFloat, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, STRIKEPANELWIDTH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_strikeSpawnHeight, gsFloat, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, STRIKESPAWNHEIGHT, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_strikeDepth, gsFloat, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, STRIKEDEPTH, FLOAT_MIN, FLOAT_MAX)
GETSET_RANGE(m_stormRadius, gsFloat, GS_FLAGS_DEFAULT, 3, 6, STORMOBJ, STORMRADIUS, FLOAT_MIN, FLOAT_MAX)
GETSET_VECTOR(m_strikeVariationGetSet, GS_FLAG_READ_WRITE, 4, 7, STORMOBJ, STRIKEVARIATIONARRAY)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(StrikeClassGetSet, IDS_STRIKEVARIATION_NAME)
GETSET_RANGE(m_strikeVariation ? &m_strikeVariation->m_sndID : 0, gsInt, GS_FLAGS_DEFAULT, 5, 7, STRIKEVARIATION, SNDID, 0, INT_MAX)
GETSET_RANGE(m_strikeVariation ? &m_strikeVariation->m_soundFactor : 0, gsFloat, GS_FLAGS_DEFAULT, 5, 7, STRIKEVARIATION, SOUNDFACTOR, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP