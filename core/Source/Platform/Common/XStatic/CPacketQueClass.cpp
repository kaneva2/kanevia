///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CPacketQueClass.h"
#include "SerializeHelper.h"
#include "Tools/NetworkV2/Protocols.h"
#include "UnicodeMFCHelpers.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

void CPacketObjList::AddPacket(size_t sizeData, const BYTE* pData, NETID netId) {
	auto pPacketObj = new CPacketObj(sizeData, pData, netId);
	m_sizeData += sizeData;
	if (m_lenFmt == LEN_FMT_BYTE)
		m_sizeData += sizeof(BYTE);
	else if (m_lenFmt == LEN_FMT_USHORT)
		m_sizeData += sizeof(USHORT);
	else if (m_lenFmt == LEN_FMT_ULONG)
		m_sizeData += sizeof(ULONG);
	m_list.AddTail(pPacketObj);
}

void CPacketObjList::PurgeAll() {
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPacketObj = (CPacketObj*)GetNext(pos);
		pPacketObj->SafeDelete();
		delete pPacketObj;
	}
	m_list.RemoveAll();
	m_sizeData = 0;
}

void CPacketObjList::PurgeAllByID(NETID netId) {
	POSITION posLast;
	for (POSITION pos = GetHeadPosition(); (posLast = pos) != NULL;) {
		auto pPacketObj = (CPacketObj*)GetNext(pos);
		if (pPacketObj->m_netId == netId) {
			m_sizeData -= pPacketObj->m_dataSize;
			if (m_lenFmt == LEN_FMT_BYTE)
				m_sizeData -= sizeof(BYTE);
			else if (m_lenFmt == LEN_FMT_USHORT)
				m_sizeData -= sizeof(USHORT);
			else if (m_lenFmt == LEN_FMT_ULONG)
				m_sizeData -= sizeof(ULONG);
			pPacketObj->SafeDelete();
			delete pPacketObj;
			m_list.RemoveAt(posLast);
		}
	}
}

CPacketObj* CPacketObjList::GetNextPacket() {
	POSITION posLoc = m_list.FindIndex(0);
	if (!posLoc)
		return 0;
	auto pPacketObj = (CPacketObj*)m_list.GetAt(posLoc);
	m_sizeData -= pPacketObj->m_dataSize;
	if (m_lenFmt == LEN_FMT_BYTE)
		m_sizeData -= sizeof(BYTE);
	else if (m_lenFmt == LEN_FMT_USHORT)
		m_sizeData -= sizeof(USHORT);
	else if (m_lenFmt == LEN_FMT_ULONG)
		m_sizeData -= sizeof(ULONG);
	m_list.RemoveAt(posLoc);
	return pPacketObj;
}

size_t CPacketObjList::PackList(BYTE* pData) {
	// Pack All Packets Into Given Buffer
	size_t dataSize = 0;
	for (POSITION pos = GetHeadPosition(); pos;) {
		auto pPacketObj = (CPacketObj*)GetNext(pos);

		// Pack Message Length (bytes)
		if (m_lenFmt == LEN_FMT_BYTE) {
			if (pPacketObj->m_dataSize > (size_t)BYTE_MAX) {
				LogError("Data Too Large - sizeData=" << pPacketObj->m_dataSize << " > " << (size_t)BYTE_MAX);
				continue;
			}
			pData[dataSize] = (BYTE)pPacketObj->m_dataSize;
			dataSize += sizeof(BYTE);
		} else if (m_lenFmt == LEN_FMT_USHORT) {
			if (pPacketObj->m_dataSize > (size_t)USHORT_MAX) {
				LogError("Data Too Large - sizeData=" << pPacketObj->m_dataSize << " > " << (size_t)USHORT_MAX);
				continue;
			}
			*(USHORT*)&pData[dataSize] = (USHORT)pPacketObj->m_dataSize;
			dataSize += sizeof(USHORT);
		} else if (m_lenFmt == LEN_FMT_ULONG) {
			*(ULONG*)&pData[dataSize] = (ULONG)pPacketObj->m_dataSize;
			dataSize += sizeof(ULONG);
		}

		// Pack Message Data Bytes
		for (size_t i = 0; i < pPacketObj->m_dataSize; ++i) {
			pData[dataSize] = pPacketObj->m_pData[i];
			dataSize += sizeof(BYTE);
		}
	}

	return dataSize;
}

void CPacketObjList::EncodeMsgUpdate(
	BYTE*& pData,
	size_t& dataSize,
	MSG_CAT msgCat,
	CPacketObjList* pMsgOutboxAttrib,
	CPacketObjList* pMsgOutboxEquip,
	CPacketObjList* pMsgOutboxGeneral,
	CPacketObjList* pMsgOutboxEvent,
	bool forceSend) {
	// Reset Return Values
	pData = nullptr;
	dataSize = 0;

	// DRF - Added
	bool equipSend = pMsgOutboxEquip ? pMsgOutboxEquip->CanSend() : false;

	MSG_UPDATE msgHdr;
	msgHdr.SetCategory(msgCat);
	msgHdr.m_attribMsgs = pMsgOutboxAttrib ? pMsgOutboxAttrib->GetCount() : 0;
	msgHdr.m_equipMsgs = equipSend ? pMsgOutboxEquip->GetCount() : 0;
	msgHdr.m_generalMsgs = pMsgOutboxGeneral ? pMsgOutboxGeneral->GetCount() : 0;
	msgHdr.m_eventMsgs = pMsgOutboxEvent ? pMsgOutboxEvent->GetCount() : 0;

	// Something To Send ?
	if (!forceSend && !msgHdr.m_attribMsgs && !msgHdr.m_equipMsgs && !msgHdr.m_generalMsgs && !msgHdr.m_eventMsgs)
		return;

	// Header size
	auto headerSize = sizeof(MSG_UPDATE);

	// Allocate buffer
	dataSize = headerSize + (msgHdr.m_attribMsgs ? pMsgOutboxAttrib->PackedSizeBytes() : 0) + (msgHdr.m_equipMsgs ? pMsgOutboxEquip->PackedSizeBytes() : 0) + (msgHdr.m_generalMsgs ? pMsgOutboxGeneral->PackedSizeBytes() : 0) + (msgHdr.m_eventMsgs ? pMsgOutboxEvent->PackedSizeBytes() : 0);

	// Create Message
	pData = new BYTE[dataSize];
	CopyMemory(pData, &msgHdr, headerSize);

	size_t byteOffset = headerSize;

	if (msgHdr.m_attribMsgs) {
		byteOffset += pMsgOutboxAttrib->PackList(&pData[byteOffset]);
		pMsgOutboxAttrib->SafeDelete();
	}

	if (msgHdr.m_equipMsgs) {
		byteOffset += pMsgOutboxEquip->PackList(&pData[byteOffset]);
		pMsgOutboxEquip->SafeDelete();
	}

	if (msgHdr.m_generalMsgs) {
		byteOffset += pMsgOutboxGeneral->PackList(&pData[byteOffset]);
		pMsgOutboxGeneral->SafeDelete();
	}

	if (msgHdr.m_eventMsgs) {
		byteOffset += pMsgOutboxEvent->PackList(&pData[byteOffset]);
		pMsgOutboxEvent->SafeDelete();
	}
}

void CPacketObjList::MovePackets(CPacketObjList*& left, CPacketObjList*& right) {
	if (right == 0)
		return;
	if (left != 0) {
		left->SafeDelete();
		delete left;
	}
	left = right;
	right = 0;
}

} // namespace KEP