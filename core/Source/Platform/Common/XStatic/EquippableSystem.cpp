///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EquippableSystem.h"

#include "ResourceManagers/DownloadManager.h"
#include "IClientEngine.h"

#include "common\include\CompressHelper.h"
#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

TypedResourceManager<CArmedInventoryProxy>* TypedResourceManager<CArmedInventoryProxy>::ms_pInstance = nullptr;

EquippableRM* EquippableRM::Instance() {
	return static_cast<EquippableRM*>(TypedResourceManager<CArmedInventoryProxy>::Instance());
}

void EquippableRM::LoadFromAPD(CArmedInventoryList* apd) {
	if (!apd) {
		return;
	}

	RemoveAllResources(); // remove any existing APD items

	for (POSITION posLoc = apd->GetHeadPosition(); posLoc != NULL;) {
		CArmedInventoryObj* armedPtr = (CArmedInventoryObj*)apd->GetNext(posLoc);
		//Add(armedPtr, armedPtr->getGlobalId());
		GLID glid = armedPtr->getGlobalId();
		CArmedInventoryProxy* prox = AddResource(glid, IS_LOCAL_GLID(glid));
		prox->ReplaceEquippableObj(std::unique_ptr<CArmedInventoryObj>(armedPtr));
		///////// TODO memory management should not share and leak mem //////////////
	}
}

bool EquippableRM::AddNew(std::unique_ptr<CArmedInventoryObj> obj, int glid) {
	if (!obj)
		return false;

	if (FindResource(glid))
		return false;

	CArmedInventoryProxy* prox = GetProxy(glid, true);
	prox->ReplaceEquippableObj(std::move(obj));
	prox->SetState(IResource::State::LOADED_AND_CREATED);
	return true;
}

CArmedInventoryList* EquippableRM::GetLegacyAPD() {
	CArmedInventoryList* legacy = new CArmedInventoryList();
	if (GetResourceCount() == 0) {
		return NULL;
	}

	std::vector<UINT64> allKeys;
	GetAllResourceKeys(allKeys);

	for (auto it = allKeys.begin(); it != allKeys.end(); ++it) {
		CArmedInventoryProxy* proxy = dynamic_cast<CArmedInventoryProxy*>(FindResource(*it));
		ASSERT(proxy != NULL);

		if (proxy && proxy->GetInventoryObj())
			legacy->AddTail(proxy->GetInventoryObj());
	}

	return legacy;
}

CArmedInventoryProxy* EquippableRM::GetProxy(int glid, bool bCreateIfNotExist) {
	CArmedInventoryProxy* proxy = dynamic_cast<CArmedInventoryProxy*>(FindResource((UINT64)glid));
	if (proxy)
		return proxy;

	if (bCreateIfNotExist) {
		if (glid >= m_glidMax) {
			m_glidMax = glid + 1;
		}

		UINT64 resKey = (UINT64)glid;
		AddResource(resKey, IS_LOCAL_GLID(glid));
		return dynamic_cast<CArmedInventoryProxy*>(FindResource((UINT64)glid));
	}
	return NULL;
}

std::string EquippableRM::GetResourceFileName(UINT64 hash) {
	CStringA fileName;
	fileName.Format("e%010I64u.dat", hash);
	return fileName.GetString();
}

std::string EquippableRM::GetResourceFilePath(UINT64 hash) {
	return PathAdd(IResource::mBaseEqpPath[0], GetResourceFileName(hash));
}

std::string EquippableRM::GetResourceURL(UINT64 hash) {
	GLID glid = (GLID)hash;
	if (IsUGCItem(glid))
		return ""; // UGC Asset URL will be determined before downloading

	std::string url;
	StrBuild(url, GetPatchAssetLocation() << "GameFiles_EquippableItems/" << GetResourceFileName(hash));
	url = ::CompressTypeExtAdd(url);
	return url;
}

std::string EquippableRM::GetResourceLocalStore(const std::string& gameFilesDir) {
	std::string dngDir;
	if (gameFilesDir == "~") // If parent folder is not provided, use game default
		dngDir = IResource::mBaseEqpPath[0];
	else
		dngDir = PathAdd(gameFilesDir, "EquippableItems");

	// Create directory if not already exists
	FileHelper::CreateFolderDeep(dngDir);

	return dngDir;
}

void EquippableRM::loadLooseFileInfo() {
	std::string filePath = PathAdd(IResource::mBaseEqpPath[0], "textureinfo.dat");
	ResourceManager::loadLooseFileInfo(filePath);
}

CArmedInventoryProxy::CArmedInventoryProxy(ResourceManager* library, UINT64 resKey, bool isLocal) :
		IResource(library, resKey, isLocal),
		m_inventoryObj(nullptr) {
	SetState(State::INITIALIZED_NOT_LOADED);
}

CArmedInventoryProxy::~CArmedInventoryProxy() {
}

CArmedInventoryObj* CArmedInventoryProxy::GetData(bool bImmediate) {
	// Already Created ?
	if (m_inventoryObj) {
		SetState(State::LOADED_AND_CREATED);
		return m_inventoryObj.get();
	}

	// Ready To Read ?
	if (!StateIsInitializedNotLoaded())
		return NULL;

	// Read Immediately Or Queue Asynchromously ?
	if (bImmediate) {
		// Read Resource File (immediate only no async download)
		if (EquippableRM::Instance()->QueueFileForRead(this, true, false) && StateIsLoadedAndCreated())
			return m_inventoryObj.get();
	} else
		EquippableRM::Instance()->QueueResource(this);

	return NULL;
}

bool CArmedInventoryProxy::CreateResource() {
	bool ret = false;
	ASSERT(GetRawBuffer() && GetRawSize());

	CMemFile file;
	file.Attach(GetRawBuffer(), GetRawSize(), 0);
	CArchive dngArchive(&file, CArchive::load);

	dngArchive.m_strFileName = Utf8ToUtf16(GetFileName()).c_str();

	// This reads a loose dol file
	std::unique_ptr<CArmedInventoryObj> pEquip = EquippableRM::Instance()->LoadEquippableItemFromLooseArchive(GetGlid(), dngArchive);
	if (pEquip) {
		ReplaceEquippableObj(std::move(pEquip));
		SetState(LOADED_AND_CREATED);
		ret = true;
	}

	// clean up
	dngArchive.Close();
	file.Detach();
	FreeRawBuffer();

	return ret;
}

void CArmedInventoryProxy::ReplaceEquippableObj(std::unique_ptr<CArmedInventoryObj> obj) {
	if (obj) {
		obj->setGlobalId(GetGlid());
		m_inventoryObj = std::move(obj);
	}
}

CArmedInventoryObj* CArmedInventoryProxy::GetInventoryObj() const {
	return m_inventoryObj.get();
}

} // namespace KEP