///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#define D3D_OVERLOADS
#include <d3d9.h>
#include <afxtempl.h>
#include "CArmedInventoryClass.h"
#include "ServerSerialize.h"
#include "SkeletonConfiguration.h"
#include "RuntimeSkeleton.h"
#include "CSkeletonObject.h"
#include "CDeformableMesh.h"

#ifdef _ClientOutput
#include "ReMeshCreate.h"
#include "IClientEngine.h"
#include "MaterialPersistence.h"
#endif

#include "Core/Math/Rotation.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

const float CArmedInventoryObj::EI_SMALL_SIZE = 3.0f;
const float CArmedInventoryObj::EI_MED_SIZE = 6.0f;

CArmedInventoryObj::CArmedInventoryObj(const std::string& name) {
	init();
	m_name = name;
	m_equippableSize = EI_Size_Undefined;
}

void CArmedInventoryObj::Clone(CArmedInventoryObj** copy, LPDIRECT3DDEVICE9 g_pd3dDevice) {
	CArmedInventoryObj* copyLocal = new CArmedInventoryObj(m_name);

	if (m_firstPersonSystem)
		m_firstPersonSystem->Clone(&copyLocal->m_firstPersonSystem, g_pd3dDevice, NULL);
	if (m_thirdPersonSystem)
		m_thirdPersonSystem->Clone(&copyLocal->m_thirdPersonSystem, g_pd3dDevice, NULL);
	copyLocal->m_attachedBoneIndex = m_attachedBoneIndex;
	copyLocal->m_locationOccupancy = m_locationOccupancy;
	copyLocal->m_abilityTypeUse = m_abilityTypeUse;
	copyLocal->m_globalId = m_globalId;
	copyLocal->m_attachedBoneName = m_attachedBoneName;

	copyLocal->m_position = m_position;
	copyLocal->m_rotation = m_rotation;
	copyLocal->m_scale = m_scale;
	copyLocal->m_bMatrixDirty = m_bMatrixDirty;
	copyLocal->m_equippableSize = m_equippableSize;
	if (m_matrixCache != NULL)
		copyLocal->m_matrixCache = new Matrix44f(*m_matrixCache);
	else
		copyLocal->m_matrixCache = NULL;

	*copy = copyLocal;
}

void CArmedInventoryObj::calculateSize() {
	if (m_thirdPersonSystem) {
		Vector3f boxSize = m_thirdPersonSystem->getRuntimeSkeleton()->getLocalBoundingBox().GetSize();
		float xsiz, ysiz, zsiz;
		xsiz = abs(boxSize.x);
		ysiz = abs(boxSize.y);
		zsiz = abs(boxSize.z);
		if (xsiz + ysiz + zsiz < CArmedInventoryObj::EI_SMALL_SIZE) {
			m_equippableSize = EI_Size_Small;
		} else if (xsiz + ysiz + zsiz < CArmedInventoryObj::EI_MED_SIZE) {
			m_equippableSize = EI_Size_Medium;
		} else {
			m_equippableSize = EI_Size_Large;
		}
	} else {
		m_equippableSize = EI_Size_Small;
	}
}

void CArmedInventoryObj::Serialize(CArchive& ar) {
	BOOL reserved = FALSE;
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << CStringA(m_name.c_str())
		   << m_firstPersonSystem
		   << m_thirdPersonSystem
		   << m_attachedBoneIndex
		   << (int)0 // m_functionalityAttribute
		   << (int)0 // m_miscInt
		   << (int)0 // m_miscInt2
		   << (float)0.0f // m_miscPosition.x
		   << (float)0.0f // m_miscPosition.y
		   << (float)0.0f // m_miscPosition.z
		   << m_locationOccupancy
		   << (int)-1 // m_skillUse
		   << 0 // m_reloadDelay
		   << (int)0 // m_animAccessVersion
		   << (int)0 // m_miscInt3
		   << (int)m_globalId;

		CStringA boneMappingStr = m_attachedBoneName.c_str();
		ar << boneMappingStr
		   << reserved
		   << reserved;

		ar << m_position.x
		   << m_position.y
		   << m_position.z
		   << m_rotation.x
		   << m_rotation.y
		   << m_rotation.z
		   << m_scale.x
		   << m_scale.y
		   << m_scale.z;
	} else {
		int version = ar.GetObjectSchema();

		CStringA name;
		int functionalityAttribute_Deprecated, miscInt_Deprecated, miscInt2_Deprecated, miscInt3_Deprecated, skillUse_Deprecated, animAccessVersion_Deprecated, reloadDelay_Deprecated;
		Vector3f miscPosition_Deprecated;
		int globalId;
		ar >> name >> m_firstPersonSystem >> m_thirdPersonSystem >> m_attachedBoneIndex >> functionalityAttribute_Deprecated >> miscInt_Deprecated >> miscInt2_Deprecated >> miscPosition_Deprecated.x >> miscPosition_Deprecated.y >> miscPosition_Deprecated.z >> m_locationOccupancy >> skillUse_Deprecated >> reloadDelay_Deprecated >> animAccessVersion_Deprecated >> miscInt3_Deprecated >> globalId;

		m_name = name.GetString();
		m_globalId = static_cast<GLID>(globalId);

		//
		// Need to get the configuration from the
		// 'legacy' CSkeletonObject
		//

		CStringA boneMappingStr;
		if (version > 2) {
			ar >> boneMappingStr >> reserved >> reserved;
		}
		m_attachedBoneName = (const char*)boneMappingStr;

#ifdef _ClientOutput
		if (version == 4 && !::ServerSerialize) {
			auto pICE = IClientEngine::Instance();
			if (!pICE)
				return;
			MeshRM::Instance()->LoadReMeshes(ar, pICE->GetReDeviceState(), MeshRM::RMP_ID_PERSISTENT);
			//load those remeshes
		}
#endif
		if (version >= 6) {
			// offset, rotation and scaling
			ar >> m_position.x >> m_position.y >> m_position.z >> m_rotation.x >> m_rotation.y >> m_rotation.z >> m_scale.x >> m_scale.y >> m_scale.z;

			// Force skeleton to do normal vector normalization if m_scale is defined
			if (isScaled()) {
				if (m_firstPersonSystem) {
					m_firstPersonSystem->getRuntimeSkeleton()->setParentScaled(true);
				}

				if (m_thirdPersonSystem) {
					m_thirdPersonSystem->getRuntimeSkeleton()->setParentScaled(true);
				}
			}

			m_bMatrixDirty = true;
		}
		calculateSize();
	}
}

BOOL CArmedInventoryObj::isConfigurable() {
	CSkeletonObject* equipVisuals[2] = { m_firstPersonSystem, m_thirdPersonSystem };
	for (int i = 0; i < 2; i++) {
		if (equipVisuals[i] && equipVisuals[i]->getSkeletonConfiguration() && equipVisuals[i]->getSkeletonConfiguration()->m_meshSlots) {
			SkeletonConfiguration* config = equipVisuals[i]->getSkeletonConfiguration();
			for (POSITION dbPos = config->m_meshSlots->GetHeadPosition(); dbPos != NULL;) {
				CAlterUnitDBObject* dbu = (CAlterUnitDBObject*)config->m_meshSlots->GetNext(dbPos);
				if (dbu && dbu->m_configurations) {
					if (dbu->m_configurations->GetCount() > 1) {
						return TRUE; // more than one mesh choice need a configuration
					}
					POSITION cfgPos = dbu->m_configurations->FindIndex(0);
					CDeformableMesh* dMesh = (CDeformableMesh*)dbu->m_configurations->GetNext(cfgPos);
					if (dMesh && dMesh->m_supportedMaterials) {
						if (dMesh->m_supportedMaterials->GetCount() > 1) {
							return TRUE; // more than one material choice need a configuration
						}
					}
				}
			}
		}
	}

	return FALSE;
}

int CArmedInventoryObj::countReMeshes() const {
	int meshcount = 0;
	if (m_firstPersonSystem && m_firstPersonSystem->getSkeletonConfiguration() && m_firstPersonSystem->getSkeletonConfiguration()->m_meshSlots) {
		for (POSITION posLoc2 = m_firstPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetHeadPosition(); posLoc2 != NULL;) {
			CAlterUnitDBObject* dbu = (CAlterUnitDBObject*)m_firstPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetNext(posLoc2);
			if (dbu && dbu->m_configurations) {
				for (POSITION posLoc3 = dbu->m_configurations->GetHeadPosition(); posLoc3 != NULL;) {
					CDeformableMesh* dMesh = (CDeformableMesh*)dbu->m_configurations->GetNext(posLoc3);
					if (dMesh) {
						meshcount = meshcount + dMesh->m_lodCount;
					}
				}
			}
		}
	}
	if (m_thirdPersonSystem && m_thirdPersonSystem->getSkeletonConfiguration() && m_thirdPersonSystem->getSkeletonConfiguration()->m_meshSlots) {
		for (POSITION posLoc2 = m_thirdPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetHeadPosition(); posLoc2 != NULL;) {
			CAlterUnitDBObject* dbu = (CAlterUnitDBObject*)m_thirdPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetNext(posLoc2);
			if (dbu && dbu->m_configurations) {
				for (POSITION posLoc3 = dbu->m_configurations->GetHeadPosition(); posLoc3 != NULL;) {
					CDeformableMesh* dMesh = (CDeformableMesh*)dbu->m_configurations->GetNext(posLoc3);
					if (dMesh) {
						meshcount = meshcount + dMesh->m_lodCount;
					}
				}
			}
		}
	}
	return meshcount;
}

int CArmedInventoryList::CountReMeshes() {
	int meshcount = 0;
	for (POSITION armedPos = GetHeadPosition(); armedPos != NULL;) {
		CArmedInventoryObj* armedObj = (CArmedInventoryObj*)GetNext(armedPos);
		meshcount = meshcount + armedObj->countReMeshes();
	}
	return meshcount;
}

ReSkinMesh** CArmedInventoryObj::gatherMeshes() {
#ifdef _ClientOutput
	int reMeshCount = countReMeshes();
	ReSkinMesh** outMeshes = new ReSkinMesh*[reMeshCount];
	int meshindex = 0;

	if (m_firstPersonSystem && m_firstPersonSystem->getSkeletonConfiguration() && m_firstPersonSystem->getSkeletonConfiguration()->m_meshSlots) {
		for (POSITION posLoc2 = m_firstPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetHeadPosition(); posLoc2 != NULL;) {
			CAlterUnitDBObject* dbu = (CAlterUnitDBObject*)m_firstPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetNext(posLoc2);
			if (dbu && dbu->m_configurations) {
				for (POSITION posLoc3 = dbu->m_configurations->GetHeadPosition(); posLoc3 != NULL;) {
					CDeformableMesh* dMesh = (CDeformableMesh*)dbu->m_configurations->GetNext(posLoc3);
					if (dMesh) {
						for (unsigned int i = 0; i < dMesh->m_lodCount; i++) {
							outMeshes[meshindex] = (ReSkinMesh*)MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[i], MeshRM::RMP_ID_PERSISTENT);
							meshindex = meshindex + 1;
						}
					}
				}
			}
		}
	}
	if (m_thirdPersonSystem && m_thirdPersonSystem->getSkeletonConfiguration() && m_thirdPersonSystem->getSkeletonConfiguration()->m_meshSlots) {
		for (POSITION posLoc2 = m_thirdPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetHeadPosition(); posLoc2 != NULL;) {
			CAlterUnitDBObject* dbu = (CAlterUnitDBObject*)m_thirdPersonSystem->getSkeletonConfiguration()->m_meshSlots->GetNext(posLoc2);
			if (dbu && dbu->m_configurations) {
				for (POSITION posLoc3 = dbu->m_configurations->GetHeadPosition(); posLoc3 != NULL;) {
					CDeformableMesh* dMesh = (CDeformableMesh*)dbu->m_configurations->GetNext(posLoc3);
					if (dMesh) {
						for (unsigned int i = 0; i < dMesh->m_lodCount; i++) {
							outMeshes[meshindex] = (ReSkinMesh*)MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[i], MeshRM::RMP_ID_PERSISTENT);
							if (outMeshes[meshindex] == NULL) {
								outMeshes[meshindex] = (ReSkinMesh*)MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[i], MeshRM::RMP_ID_DYNAMIC);
							}
							meshindex = meshindex + 1;
						}
					}
				}
			}
		}
	}
	return outMeshes;
#else
	ASSERT(0);
	return 0;
#endif
}

#ifdef _ClientOutput
inline void gatherMeshesOnSkeleton(ReSkinMesh**& outMeshPtr, CArmedInventoryObj* pEquip, eVisualType type) {
	auto pSkeleton = pEquip->getSkeleton(type);
	auto pSkeletonConfig = pEquip->getSkeletonConfig(type);
	if (pSkeleton != nullptr && pSkeletonConfig != nullptr && pSkeletonConfig->m_meshSlots != nullptr) {
		auto altCfgs = pSkeletonConfig->m_meshSlots;
		for (POSITION posCfgs = altCfgs->GetHeadPosition(); posCfgs != nullptr;) {
			CAlterUnitDBObject* dbu = (CAlterUnitDBObject*)altCfgs->GetNext(posCfgs);
			if (dbu && dbu->m_configurations) {
				for (POSITION posDIM = dbu->m_configurations->GetHeadPosition(); posDIM != NULL;) {
					CDeformableMesh* dMesh = (CDeformableMesh*)dbu->m_configurations->GetNext(posDIM);
					if (dMesh) {
						for (unsigned int i = 0; i < dMesh->m_lodCount; i++) {
							*outMeshPtr = (ReSkinMesh*)MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[i], MeshRM::RMP_ID_PERSISTENT);
							outMeshPtr++;
						}
					}
				}
			}
		}
	}
}
#endif

ReSkinMesh** CArmedInventoryList::GatherMeshes() {
#ifdef _ClientOutput
	int reMeshCount = CountReMeshes();
	ReSkinMesh** outMeshes = new ReSkinMesh*[reMeshCount];
	ReSkinMesh** outMeshPtr = outMeshes;
	for (POSITION pos = GetHeadPosition(); pos != NULL;) {
		CArmedInventoryObj* pEquip = (CArmedInventoryObj*)GetNext(pos);
		gatherMeshesOnSkeleton(outMeshPtr, pEquip, eVisualType::FirstPerson);
		gatherMeshesOnSkeleton(outMeshPtr, pEquip, eVisualType::ThirdPerson);
	}
	return outMeshes;
#else
	ASSERT(0);
	return 0;
#endif
}

template <typename ListPtrType, typename ObjPtrType>
ObjPtrType CArmedInventoryList::GetByGLID(ListPtrType list, const GLID& glid) {
	for (POSITION pos = list->GetHeadPosition(); pos != nullptr;) {
		auto pEquip = static_cast<ObjPtrType>(list->GetNext(pos));
		if (pEquip->getGlobalId() == glid)
			return pEquip;
	}
	return nullptr;
}

CArmedInventoryObj* CArmedInventoryList::getByGLID(const GLID& glid) {
	return GetByGLID<CArmedInventoryList*, CArmedInventoryObj*>(this, glid);
}

const CArmedInventoryObj* CArmedInventoryList::getByGLID(const GLID& glid) const {
	return GetByGLID<const CArmedInventoryList*, const CArmedInventoryObj*>(this, glid);
}

void CArmedInventoryList::Clone(CArmedInventoryList** copy, LPDIRECT3DDEVICE9 g_pd3dDevice) const {
	CArmedInventoryList* copyLocal = new CArmedInventoryList();
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CArmedInventoryObj* visObj = (CArmedInventoryObj*)GetNext(posLoc);
		CArmedInventoryObj* clonedPtr = NULL;
		visObj->Clone(&clonedPtr, g_pd3dDevice);
		copyLocal->AddTail(clonedPtr);
	}
	*copy = copyLocal;
}

void CArmedInventoryList::getAllGLIDs(std::vector<GLID>& out) const {
	for (POSITION pos = GetHeadPosition(); pos != NULL;) {
		auto pEquip = static_cast<const CArmedInventoryObj*>(GetNext(pos));
		out.push_back(pEquip->getGlobalId());
	}
}

template <typename ListPtrType, typename ObjPtrType>
void CArmedInventoryList::GetAllItems(ListPtrType list, std::vector<ObjPtrType>& out) {
	for (POSITION pos = list->GetHeadPosition(); pos != NULL;) {
		auto pEquip = static_cast<ObjPtrType>(list->GetNext(pos));
		out.push_back(pEquip);
	}
}

void CArmedInventoryList::getAllItems(std::vector<const CArmedInventoryObj*>& out) const {
	GetAllItems(this, out);
}

void CArmedInventoryList::getAllItems(std::vector<CArmedInventoryObj*>& out) {
	GetAllItems(this, out);
}

size_t CArmedInventoryList::deleteAllWithGLID(const GLID& glid) {
	size_t nDeleted = 0;
	POSITION posLast = nullptr;
	for (POSITION pos = GetHeadPosition(); pos != nullptr;) {
		posLast = pos;
		auto pEquip = static_cast<CArmedInventoryObj*>(GetNext(pos));
		if (pEquip->getGlobalId() == glid) {
			RemoveAt(posLast);
			delete pEquip;
			++nDeleted;
		}
	}
	return nDeleted;
}

CArmedInventoryObj* CArmedInventoryList::removeByGLID(const GLID& glid) {
	POSITION posLast = nullptr;
	for (POSITION pos = GetHeadPosition(); pos != nullptr;) {
		posLast = pos;
		auto pEquip = static_cast<CArmedInventoryObj*>(GetNext(pos));
		if (pEquip->getGlobalId() == glid) {
			RemoveAt(posLast);
			return pEquip;
		}
	}
	return nullptr;
}

void CArmedInventoryList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CArmedInventoryObj* armedObj = (CArmedInventoryObj*)GetNext(posLoc);
		delete armedObj;
		armedObj = 0;
	}
	RemoveAll();
}

void CArmedInventoryList::Serialize(CArchive& ar) {
	if (::ServerSerialize) {
		CKEPObList::Serialize(ar);
	}
#ifdef _ClientOutput
	else {
		// APD Material compression
		MaterialCompressor* matCompressor = MaterialCompressor::getMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
		if (ar.IsStoring()) {
			for (POSITION pos = GetHeadPosition(); pos != NULL;) {
				CArmedInventoryObj* pEquip = (CArmedInventoryObj*)GetNext(pos);
				RuntimeSkeleton* pSkeleton;

				pSkeleton = pEquip->getSkeleton(eVisualType::ThirdPerson);
				if (pSkeleton != nullptr) {
					matCompressor->addMaterials(pSkeleton, pEquip->getSkeletonConfig(eVisualType::ThirdPerson));
				}

				pSkeleton = pEquip->getSkeleton(eVisualType::FirstPerson);
				if (pSkeleton != nullptr) {
					matCompressor->addMaterials(pSkeleton, pEquip->getSkeletonConfig(eVisualType::FirstPerson));
				}
			}
			int rmCount = CountReMeshes();
			ReSkinMesh** outMeshes;
			outMeshes = GatherMeshes();
			matCompressor->compress();
			// Save the meshes to the APD
			MeshRM::Instance()->StoreReMeshes((ReMesh**)outMeshes, rmCount, ar, MeshRM::RMP_ID_PERSISTENT);
			// now save loose meshes.
			MeshRM::Instance()->StoreReMeshes(outMeshes, rmCount);
			matCompressor->writeToArchive(ar);
			CKEPObList::Serialize(ar);
			delete[] outMeshes;
		} else {
			int version = ar.GetObjectSchema();
			if (version >= 1) {
				auto pICE = IClientEngine::Instance();
				if (!pICE)
					return;
				MeshRM::Instance()->LoadReMeshes(ar, pICE->GetReDeviceState(), MeshRM::RMP_ID_PERSISTENT);
				matCompressor->readFromArchive(ar);
			}
			CKEPObList::Serialize(ar);
		}
		MaterialCompressor::deleteMaterialCompressor(Utf16ToUtf8(ar.m_strFileName));
	}
#endif
}

CArmedInventoryObj::~CArmedInventoryObj() {
	if (m_matrixCache)
		delete m_matrixCache;
	m_matrixCache = NULL;
}

void CArmedInventoryObj::init() {
	m_firstPersonSystem = NULL;
	m_thirdPersonSystem = NULL;
	m_attachedBoneIndex = 0;
	m_locationOccupancy = 0;
	m_abilityTypeUse = 0;
	m_globalId = -1;

	m_matrixCache = NULL;
	m_bMatrixDirty = false;
	m_position.x = m_position.y = m_position.z = 0.0f;
	m_scale.x = m_scale.y = m_scale.z = 1.0f;
	m_rotation.x = m_rotation.y = m_rotation.z = 0.0f;
	m_bIgnoreParentScale = false;
}

bool CArmedInventoryObj::getPrimaryAnimationSettings(AnimSettings& animSettings) const {
	// Reset Return Values
	animSettings = AnimSettings();

	if (!m_thirdPersonSystem || !m_thirdPersonSystem->getRuntimeSkeleton())
		return false;

	RuntimeSkeleton* pRS = m_thirdPersonSystem->getRuntimeSkeleton();
	if (!pRS)
		return false;

	// TODO - Add loopCtl, etc
	auto animGlid = pRS->getAnimInUseSkeletonGlid();
	auto animDirForward = pRS->isAnimInUseDirForward();
	animSettings = AnimSettings(animGlid, animDirForward);
	return true;
}

void CArmedInventoryObj::setPrimaryAnimationSettings(const AnimSettings& animSettings) {
	if (!m_thirdPersonSystem || !m_thirdPersonSystem->getRuntimeSkeleton())
		return;

	RuntimeSkeleton* pRS = m_thirdPersonSystem->getRuntimeSkeleton();
	if (!pRS)
		return;

	// Set Animation Settings
	pRS->setSecondaryAnimation(GLID_INVALID);
	pRS->setPrimaryAnimationSettings(animSettings);
}

Matrix44f* CArmedInventoryObj::getRelativeMatrix() {
	if (m_bMatrixDirty) {
		if (m_matrixCache == NULL)
			m_matrixCache = new Matrix44f();

		ConvertRotationZXY(m_rotation, out(*m_matrixCache));
		m_matrixCache->GetRow(0).SubVector<3>() *= m_scale[0];
		m_matrixCache->GetRow(1).SubVector<3>() *= m_scale[1];
		m_matrixCache->GetRow(2).SubVector<3>() *= m_scale[2];
		m_matrixCache->GetTranslation() = m_position;

		m_bMatrixDirty = false;
	}

	return m_matrixCache;
}

void CArmedInventoryObj::realizeDimConfig(eVisualType type, CArmedInventoryObj* pRefObj) {
	auto pSkeleton = getSkeleton(type);
	auto pSkeletonConfig = getSkeletonConfig(type);
	if (pSkeleton != nullptr && pSkeletonConfig != nullptr) {
		auto pSkeletonConfigRef = pRefObj->getSkeletonConfig(type);
		assert(pSkeletonConfigRef != nullptr);
		if (pSkeletonConfigRef != nullptr) {
			pSkeletonConfig->RealizeDimConfig(pSkeleton, pSkeletonConfigRef->m_meshSlots);
		}
	}
}

template <typename ClassPtr, typename SkelPtr>
SkelPtr CArmedInventoryObj::GetSkeleton(ClassPtr self, eVisualType type) {
	return type == eVisualType::FirstPerson ? self->m_firstPersonSystem ? self->m_firstPersonSystem->getRuntimeSkeleton() : nullptr : self->m_thirdPersonSystem ? self->m_thirdPersonSystem->getRuntimeSkeleton() : nullptr;
}

RuntimeSkeleton* CArmedInventoryObj::getSkeleton(eVisualType type) {
	return GetSkeleton<CArmedInventoryObj*, RuntimeSkeleton*>(this, type);
}

const RuntimeSkeleton* CArmedInventoryObj::getSkeleton(eVisualType type) const {
	return GetSkeleton<const CArmedInventoryObj*, const RuntimeSkeleton*>(this, type);
}

template <typename ClassPtr, typename ConfPtr>
ConfPtr CArmedInventoryObj::GetSkeletonConfig(ClassPtr self, eVisualType type) {
	return type == eVisualType::FirstPerson ? self->m_firstPersonSystem ? self->m_firstPersonSystem->getSkeletonConfiguration() : nullptr : self->m_thirdPersonSystem ? self->m_thirdPersonSystem->getSkeletonConfiguration() : nullptr;
}

SkeletonConfiguration* CArmedInventoryObj::getSkeletonConfig(eVisualType type) {
	return GetSkeletonConfig<CArmedInventoryObj*, SkeletonConfiguration*>(this, type);
}

const SkeletonConfiguration* CArmedInventoryObj::getSkeletonConfig(eVisualType type) const {
	return GetSkeletonConfig<const CArmedInventoryObj*, const SkeletonConfiguration*>(this, type);
}

void CArmedInventoryObj::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_attachedBoneName);
		pMemSizeGadget->AddObject(m_firstPersonSystem);
		pMemSizeGadget->AddObjectSizeof(m_matrixCache);
		pMemSizeGadget->AddObject(m_name);
		pMemSizeGadget->AddObject(m_thirdPersonSystem);
	}
}

void CArmedInventoryList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		CKEPObList::GetMemSizeofInternals(pMemSizeGadget);

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			const CArmedInventoryObj* pArmedInventoryObj = static_cast<const CArmedInventoryObj*>(GetNext(pos));
			pMemSizeGadget->AddObject(pArmedInventoryObj);
		}
	}
}

IMPLEMENT_SERIAL_SCHEMA(CArmedInventoryObj, CObject)
IMPLEMENT_SERIAL_SCHEMA(CArmedInventoryList, CKEPObList)

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CArmedInventoryObj, IDS_ARMEDINVENTORYOBJ_NAME)
GETSET_MAX(m_name, gsString, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, NAME, STRLEN_MAX)
GETSET_OBJ_PTR(m_firstPersonSystem, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, FIRSTPERSONSYSTEM, CSkeletonObject)
GETSET_OBJ_PTR(m_thirdPersonSystem, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, THIRDPERSONSYSTEM, CSkeletonObject)
GETSET_RANGE(m_attachedBoneIndex, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, BONEATTACHED, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, FUNCTIONALITYATTRIBUTE, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, MISCINT, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, MISCINT2, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, MISCPOSITION, 0, 0)
GETSET_RANGE(m_locationOccupancy, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, LOCATIONOCCUPANCY, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, SKILLUSE, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, RELOADDELAY, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, ANIMACCESSVERSION, INT_MIN, INT_MAX)
GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, MISCINT3, INT_MIN, INT_MAX)
GETSET_RANGE(m_globalId, gsInt, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, TYPE, INT_MIN, INT_MAX)
GETSET_MAX(m_attachedBoneName, gsString, GS_FLAGS_DEFAULT, 0, 0, ARMEDINVENTORYOBJ, BONEMAPPING, STRLEN_MAX)
END_GETSET_IMPL

} // namespace KEP
