///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CWorldChannelRules.h"

namespace KEP {

CWordlChannelRuleObj::CWordlChannelRuleObj() {
	m_ruleName = "NAME";
	m_rule = NO_HEAL;
}

void CWorldChannelRuleList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWordlChannelRuleObj* spnPtr = (CWordlChannelRuleObj*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

void CWordlChannelRuleObj::Serialize(CArchive& ar) {
	int temp = m_rule;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_ruleName
		   << temp
		   << m_channel;
	} else {
		ar >> m_ruleName >> temp >> m_channel;
		m_rule = (EChannelRule)temp;
		ASSERT(temp >= NO_HEAL && temp <= NO_HEAL_WEAPONS);
	}
}

BOOL CWorldChannelRuleList::DoesThisRuleExistInThisChannel(const ChannelId& channel, CWordlChannelRuleObj::EChannelRule searchRule) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CWordlChannelRuleObj* spnPtr = (CWordlChannelRuleObj*)GetNext(posLoc);
		if (spnPtr->m_channel.channelIdEquals(channel))
			if (spnPtr->m_rule == searchRule)
				return TRUE;
	}
	return FALSE;
}

IMPLEMENT_SERIAL(CWordlChannelRuleObj, CObject, 0)
IMPLEMENT_SERIAL(CWorldChannelRuleList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CWordlChannelRuleObj, IDS_WORDLCHANNELRULEOBJ_NAME)
GETSET_MAX(m_ruleName, gsCString, GS_FLAGS_DEFAULT, 0, 0, WORDLCHANNELRULEOBJ, RULENAME, STRLEN_MAX)
GETSET_RANGE(m_rule, gsInt, GS_FLAGS_DEFAULT, 0, 0, WORDLCHANNELRULEOBJ, RULE, INT_MIN, INT_MAX)
GETSET_RANGE(m_channel, gsInt, GS_FLAGS_DEFAULT, 0, 0, WORDLCHANNELRULEOBJ, WLDID, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP