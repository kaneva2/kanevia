///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "SoundPlacement.h"
#include "OALSoundProxy.h"
#include "MeshStrings.h"
#include "IClientEngine.h"
#include "KepWidgets.h"
#include "CEXMeshClass.h"
#include "CMaterialClass.h"
#include "OALOggSound.h"
#include "Event/Base/IEvent.h"
#include "Event/Glue/SoundPlacementIds.h"
#include "ContentService.h"
#include "matrixarb.h"
#include "Common/KEPUtil/Helpers.h"
#include <KEPPhysics/CollisionShape.h>
#include <KEPPhysics/RigidBodyStatic.h>
#include <KEPPhysics/PhysicsFactory.h>
#include <PhysicsUserData.h>

#ifdef _ClientOutput
#include "ReMeshCreate.h"
#include "ReMesh.h"
#include "DownloadManager.h"
#endif

static LogInstance("Instance");

namespace KEP {

static const bool SOUND_RELATIVE = false;
static const bool SOUND_LOOPING = false;
static const TimeMs SOUND_LOOPING_DELAY = 0;
static const double SOUND_GAIN = 1.0; // default gain
static const double SOUND_PITCH = 1.0; // default pitch
static const double SOUND_ROLLOFF = 1.0; // default rolloff
static const double SOUND_OUTER_GAIN = 0.0; // default outer gain
static const double SOUND_REF_DIST = 1.0; // default reference distance (distance falloff)
static const double SOUND_MAX_DIST = 25.0; // default max distance (range dome)
static const double SOUND_CONE_ANGLE_OMNI = 2.0 * M_PI; // omni angle radians (360 degrees)

static const float SOUND_EMISSIVE = 0.70f; // Fraction of diffuse color that non-lit areas receive
static const float SOUND_ROLLOVER_EMISSIVE = 0.40f; // Fraction of diffuse color used in brightening rollovers

static const D3DXCOLOR SND_DEFAULT_COLOR(0.50f, 1.00f, 0.25f, 0.99f);
static const D3DXCOLOR SND_RANGE_COLOR(0.50f, 0.25f, 1.00f, 0.99f);

// DRF - Added
static const size_t numSourcesMax = 256;
static size_t numSources = 0;

Vector3f g_posListener; // last updated position of my avatar

#define LogQuiet(txt)  \
	if (!GetQuiet()) { \
		LogInfo(txt);  \
	}

bool SoundPlacement::InitOpenAL() {
	//get default device
	ALCdevice* p_alcDevice = alcOpenDevice(NULL);
	if (!p_alcDevice)
		return false;

	ALCcontext* p_alcContext = alcCreateContext(p_alcDevice, NULL);
	alcMakeContextCurrent(p_alcContext);
	alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
	alGetError();

	ALuint bufferID, sourceID;
	int error;
	const int PIMP_DURATION = 16;
	char* pimpSilence = new char[PIMP_DURATION];
	for (int i = 0; i < PIMP_DURATION; i++)
		pimpSilence[i] = (char)0;

	alGenBuffers(1, &bufferID);
	alGenSources(1, &sourceID);
	alBufferData(bufferID, AL_FORMAT_MONO16, (ALvoid*)pimpSilence, (ALsizei)PIMP_DURATION, 22050);
	alSource3f(sourceID, AL_POSITION, 0.0f, 0.0f, 0.0f);
	alSource3f(sourceID, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSource3f(sourceID, AL_DIRECTION, 0.0f, 0.0f, 1.0f);
	alSourcef(sourceID, AL_REFERENCE_DISTANCE, 1.0f);
	alSourcef(sourceID, AL_MAX_DISTANCE, 25.0f);
	alSourcei(sourceID, AL_SOURCE_RELATIVE, AL_FALSE);
	alListener3f(AL_POSITION, 1.0f, 0.0f, 5.0f);
	ALfloat listenerOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
	alListenerfv(AL_ORIENTATION, listenerOri);
	alListener3f(AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSourcei(sourceID, AL_BUFFER, bufferID);
	error = alGetError();
	if (error == AL_INVALID_OPERATION) {
		// restart with DirectSound device
		alDeleteBuffers(1, &bufferID);
		alDeleteSources(1, &sourceID);
		alcDestroyContext(p_alcContext);
		alcCloseDevice(p_alcDevice);
		const ALCchar* DeviceName = "DirectSound";
		p_alcDevice = alcOpenDevice(DeviceName);
		p_alcContext = alcCreateContext(p_alcDevice, NULL);
		alcMakeContextCurrent(p_alcContext);
		alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
		error = alGetError();
	} else {
		alDeleteBuffers(1, &bufferID);
		alDeleteSources(1, &sourceID);
	}

	delete[] pimpSilence;

	return true;
}

bool SoundPlacement::UpdateListener(const Vector3f& pos, const Vector3f& rot, const Vector3f& vel) {
	// Update OpenAL Listener Position
	ALfloat listenerPos[] = { pos[0], pos[1], pos[2] };
	alListener3f(AL_POSITION, listenerPos[0], listenerPos[1], listenerPos[2]);

	// Update OpenAL Listener Orientation
	ALfloat listenerOri[] = { rot[0], rot[1], rot[2], 0.0, 1.0, 0.0 };
	alListenerfv(AL_ORIENTATION, listenerOri);

	// Update OpenAL Listener Velocity
	ALfloat listenerVel[] = { vel[0], vel[1], vel[2] };
	alListenerfv(AL_VELOCITY, listenerVel);

	return true;
}

void SoundPlacement::SoundAttr::Reset() {
	relative = SOUND_RELATIVE;
	looping = SOUND_LOOPING;
	gain = SOUND_GAIN;
	pitch = SOUND_PITCH;
	refDist = SOUND_REF_DIST;
	maxDist = SOUND_MAX_DIST;
	rolloff = SOUND_ROLLOFF;
	outerGain = SOUND_OUTER_GAIN;
	coneOuterAngle = SOUND_CONE_ANGLE_OMNI;
	coneInnerAngle = SOUND_CONE_ANGLE_OMNI;
	pos.x = pos.y = pos.z = 0.0;
	dir.x = dir.y = dir.z = 0.0;
}

SoundPlacement::SoundPlacement(int pid, const Vector3f& pos) :
		m_id(pid), m_glid(GLID_INVALID), m_inited(true), m_playbackMode(PM_TRIGGERED_ON), m_playing(false), m_scriptPlay(false), m_coneMesh(NULL), m_soundMesh(NULL), m_rangeMesh(NULL), m_innerAngleRing(NULL), m_outerAngleRing(NULL), m_material(NULL), m_rangeMaterial(NULL), m_outline(false), m_outlineZSorted(false), m_source(0), m_proxy(NULL) {
	// Initialize Materials & Meshes
	MaterialAndMeshInit();

	PhysicsInit();

	// Set Position
	SetPosition(pos);
}

SoundPlacement::~SoundPlacement() {
	// Stop Sound (forced)
	Stop(true);

	// Delete Visuals
	if (m_soundMesh)
		delete m_soundMesh;
	if (m_rangeMesh)
		delete m_rangeMesh;
	if (m_coneMesh)
		delete m_coneMesh;
	if (m_innerAngleRing)
		delete m_innerAngleRing;
	if (m_outerAngleRing)
		delete m_outerAngleRing;
	if (m_material)
		delete m_material;
	if (m_rangeMaterial)
		delete m_rangeMaterial;

	// Delete Source & Proxy
	SourceDel(true);
}

void SoundPlacement::PhysicsInit() {
	Physics::CollisionShapeSphereProperties visibleShapeProps;
	float fRadiusFeet = 0;
	if (m_soundMesh) {
		m_soundMesh->GetMeshData()->CalculateDiameter();
		fRadiusFeet = 0.5f * m_soundMesh->GetMeshData()->Diameter() * M_SQRT1_2;
	}
	if (fRadiusFeet == 0)
		fRadiusFeet = 1;

	visibleShapeProps.m_fRadiusMeters = FeetToMeters(fRadiusFeet);
	std::shared_ptr<Physics::CollisionShape> pVisibleShape = Physics::CreateCollisionShape(visibleShapeProps);
	if (pVisibleShape) {
		Physics::RigidBodyStaticProperties visibleBodyProps;
		m_pVisibleBody = Physics::CreateRigidBodyStatic(visibleBodyProps, pVisibleShape.get(), 1.0f);
		if (m_pVisibleBody) {
			m_pVisibleBody->SetCollisionMasks(CollisionGroup::Visible, 0);
			std::shared_ptr<PhysicsBodyData> pPhysicsBodyData = std::make_shared<PhysicsBodyData>(ObjectType::SOUND, m_id, this);
#if defined(_DEBUG)
			pPhysicsBodyData->m_glid = m_glid;
			pPhysicsBodyData->m_pszName = m_name.c_str();
#endif
			m_pVisibleBody->SetUserData(pPhysicsBodyData);
		}
	}

	VisibilityChanged();
}

void SoundPlacement::VisibilityChanged() {
	if (!m_pVisibleBody)
		return;

	bool bVisible = this->ShowSoundPlacement();

	// Alternatively, add/remove from physics universe
	m_pVisibleBody->SetCollisionMasks(bVisible ? CollisionGroup::Visible : 0, 0);
}

bool SoundPlacement::MaterialAndMeshInit() {
	// Initialize Position And Range Matrix
	D3DXMatrixIdentity((D3DXMATRIX*)&m_locationMat);
	D3DXMatrixIdentity((D3DXMATRIX*)&m_rangeMat);

	m_soundMesh = KEPWidgetMeshes::loadReMeshOBJ(MeshStrings::SOUND_MESH, &m_locationMat);
	m_rangeMesh = KEPWidgetMeshes::loadReMeshOBJ(MeshStrings::SOUND_MESH, &m_rangeMat);

	m_material = new CMaterialObject();

	m_material->m_useMaterial.Diffuse = SND_DEFAULT_COLOR;
	m_material->m_useMaterial.Emissive = SND_DEFAULT_COLOR * SOUND_EMISSIVE;
	m_material->m_useMaterial.Ambient = D3DXCOLOR(.1f, .1f, .1f, 1.0f);

	m_material->m_baseMaterial.Emissive = m_material->m_useMaterial.Emissive;
	m_material->m_baseMaterial.Diffuse = m_material->m_useMaterial.Diffuse;
	m_material->m_baseMaterial.Ambient = m_material->m_useMaterial.Ambient;
	m_material->m_blendMode = 0;

	m_rangeMaterial = new CMaterialObject();

	m_rangeMaterial->m_useMaterial.Diffuse = SND_RANGE_COLOR;
	m_rangeMaterial->m_useMaterial.Emissive = SND_RANGE_COLOR * SOUND_EMISSIVE;
	m_rangeMaterial->m_useMaterial.Ambient = D3DXCOLOR(.1f, .1f, .1f, 1.0f);

	m_rangeMaterial->m_baseMaterial.Emissive = m_rangeMaterial->m_useMaterial.Emissive;
	m_rangeMaterial->m_baseMaterial.Diffuse = m_rangeMaterial->m_useMaterial.Diffuse;
	m_rangeMaterial->m_baseMaterial.Ambient = m_rangeMaterial->m_useMaterial.Ambient;
	m_rangeMaterial->m_blendMode = 0;

	m_coneMaterial = new CMaterialObject();
	m_outerMaterial = new CMaterialObject();
	m_innerMaterial = new CMaterialObject();

	m_coneMaterial->m_useMaterial.Diffuse = m_innerMaterial->m_useMaterial.Diffuse = m_outerMaterial->m_useMaterial.Diffuse = SND_DEFAULT_COLOR;
	m_coneMaterial->m_useMaterial.Emissive = m_innerMaterial->m_useMaterial.Emissive = m_outerMaterial->m_useMaterial.Emissive = SND_DEFAULT_COLOR * SOUND_EMISSIVE;
	m_coneMaterial->m_useMaterial.Ambient = m_innerMaterial->m_useMaterial.Ambient = m_outerMaterial->m_useMaterial.Ambient = D3DXCOLOR(.1f, .1f, .1f, 1.0f);

	m_coneMaterial->m_baseMaterial.Emissive = m_innerMaterial->m_baseMaterial.Emissive = m_outerMaterial->m_baseMaterial.Emissive = m_rangeMaterial->m_useMaterial.Emissive;
	m_coneMaterial->m_baseMaterial.Diffuse = m_innerMaterial->m_baseMaterial.Diffuse = m_outerMaterial->m_baseMaterial.Diffuse = m_rangeMaterial->m_useMaterial.Diffuse;
	m_coneMaterial->m_baseMaterial.Ambient = m_innerMaterial->m_baseMaterial.Ambient = m_outerMaterial->m_baseMaterial.Ambient = m_rangeMaterial->m_useMaterial.Ambient;
	m_coneMaterial->m_blendMode = m_innerMaterial->m_blendMode = m_outerMaterial->m_blendMode = 1;

	GenConeMesh();

	MeshRM::Instance()->CreateReMaterial(m_innerMaterial);
	MeshRM::Instance()->CreateReMaterial(m_outerMaterial);
	m_innerMaterial->GetReMaterial()->PreLight(FALSE);
	m_outerMaterial->GetReMaterial()->PreLight(FALSE);

	UpdateConeAngleRingMeshes();

	m_outlineColor = D3DXCOLOR(1, 1, 1, 1);

	MeshRM::Instance()->CreateReMaterial(m_material);
	m_material->GetReMaterial()->PreLight(FALSE);
	m_soundMesh->SetMaterial(m_material->GetReMaterial());

	MeshRM::Instance()->CreateReMaterial(m_rangeMaterial);
	m_rangeMaterial->GetReMaterial()->PreLight(FALSE);
	m_rangeMesh->SetMaterial(m_rangeMaterial->GetReMaterial());

	return true;
}

bool SoundPlacement::SourceInit() {
	// Source Already Ok ?
	bool sourceNew = false;
	if (!SourceOk()) {
		// Generate New Source
		if (!SourceNew()) {
			LogError(ToStr() << " SourceNew() FAILED");
			return false;
		}
		sourceNew = true;
	}

	// Source Set All Attributes (forcefully if new source)
	return SourceSetAll(sourceNew);
}

bool SoundPlacement::SourceNew() {
	// Source Already Ok ?
	if (SourceOk())
		return true;

	// Generate New Source
	alGenSources(1, &m_source);
	if (!SourceOk()) {
		auto err = alGetError();
		bool maxSources = (numSources >= numSourcesMax);
		LogError(ToStr() << " aiGenSource() FAILED - numSources=" << numSources << (maxSources ? " MAX_SOURCES" : "") << " err=0x" << std::hex << err);
		return false;
	}
	++numSources;

	LogInfo(ToStr() << " numSources=" << numSources);
	return true;
}

bool SoundPlacement::SourceDel(bool delProxy) {
	// Stop Source & Delete
	if (SourceOk()) {
		alSourceStop(m_source);
		alDeleteSources(1, &m_source);
		m_source = 0;
		--numSources;
	}

	// Delete Sound Proxy ?
	auto proxySound = ProxySound();
	if (proxySound && delProxy) {
		proxySound->EmptyQueue(m_source);
		m_proxy = NULL;
	}

	LogInfo(ToStr() << " numSources=" << numSources << " delProxy=" << LogBool(delProxy));
	return true;
}

bool SoundPlacement::SourceOk() const {
	return (m_source && alIsSource(m_source));
}

bool SoundPlacement::SourceSet(ALenum attr, bool val) const {
	if (!SourceOk())
		return false;

	// Set Source Boolean Attribute
	alSourcei(m_source, attr, val ? AL_TRUE : AL_FALSE);
	auto err = alGetError();
	if (err != AL_NO_ERROR) {
		//		LogError(ToStr() << "aiSourcei() FAILED - attr=0x" << hex << attr << " val=" << LogBool(val) << " err=0x" << hex << err);
		return false;
	}
	return true;
}

bool SoundPlacement::SourceSet(ALenum attr, double val) const {
	if (!SourceOk())
		return false;

	// Set Source Float Attribute
	alSourcef(m_source, attr, val);
	auto err = alGetError();
	if (err != AL_NO_ERROR) {
		//		LogError(ToStr() << "aiSourcef() FAILED - attr=0x" << hex << attr << " val=" << val << " err=0x" << hex << err);
		return false;
	}
	return true;
}

bool SoundPlacement::SourceSet(ALenum attr, const Vector3f& val) const {
	if (!SourceOk())
		return false;

	// Set Source Vector Attribute
	alSource3f(m_source, attr, val.x, val.y, val.z);
	auto err = alGetError();
	if (err != AL_NO_ERROR) {
		//		LogError(ToStr() << "aiSource3f() FAILED - attr=0x" << hex << attr << " val=" << val.ToStr() << " err=0x" << hex << err);
		return false;
	}
	return true;
}

bool SoundPlacement::SourceSetAll(bool force) {
	if (!SourceOk())
		return false;

	// Source Set All Sound Attributes
	if (force || (GetRelative() != m_soundAttrSource.relative))
		SourceSet(AL_SOURCE_RELATIVE, GetRelative());
	if (force || (GetGain() != m_soundAttrSource.gain))
		SourceSet(AL_GAIN, GetGain());
	if (force || (GetPitch() != m_soundAttrSource.pitch))
		SourceSet(AL_PITCH, GetPitch());
	if (force || (GetRefDist() != m_soundAttrSource.refDist))
		SourceSet(AL_REFERENCE_DISTANCE, GetRefDist());
	if (force || (GetMaxDist() != m_soundAttrSource.maxDist))
		SourceSet(AL_MAX_DISTANCE, GetMaxDist());
	if (force || (GetRolloff() != m_soundAttrSource.rolloff))
		SourceSet(AL_ROLLOFF_FACTOR, GetRolloff());
	if (force || (GetConeOuterAngleRad() != m_soundAttrSource.coneOuterAngle))
		SourceSet(AL_CONE_OUTER_ANGLE, GetConeOuterAngleDeg() / 2.0);
	if (force || (GetConeInnerAngleRad() != m_soundAttrSource.coneInnerAngle))
		SourceSet(AL_CONE_INNER_ANGLE, GetConeInnerAngleDeg() / 2.0);
	if (force || (DistSqPos(m_soundAttrSource.pos) > 0.00001))
		SourceSet(AL_POSITION, GetPosition());
	if (force || (DistSqDir(m_soundAttrSource.dir) > 0.00001))
		SourceSet(AL_DIRECTION, GetDirection());

	// Update Sourced Sound Attributes For Next Set Comparison
	m_soundAttrSource = m_soundAttr;

	return true;
}

void SoundPlacement::UpdateRangeMatrix() {
	double maxDist = GetMaxDist();
	D3DXMatrixScaling((D3DXMATRIX*)&m_rangeMat, maxDist, maxDist, maxDist);
	m_rangeMat.GetTranslation() = GetPosition();
}

void SoundPlacement::UpdateConeAngleRingMeshes() {
	double maxDist = GetMaxDist();
	if (m_outerAngleRing)
		delete m_outerAngleRing;
	m_outerAngleRing = getRingMesh(GetConeOuterAngleRad(), maxDist / 2, m_outerMaterial);
	if (m_innerAngleRing)
		delete m_innerAngleRing;
	m_innerAngleRing = getRingMesh(GetConeInnerAngleRad(), maxDist / 4, m_innerMaterial);
}

bool SoundPlacement::SetRelative(bool rel) {
	m_soundAttr.relative = rel;
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetLooping(bool loop) {
	m_soundAttr.looping = loop;
	LoopingDelayReset();
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetLoopingDelay(TimeMs timeMs) {
	LoopingDelaySet(timeMs);
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetGain(double gain) {
	if (gain < 0.0)
		gain = 0.0;
	m_soundAttr.gain = gain;
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetPitch(double pitch) {
	if (pitch < 0.0)
		pitch = 0.0;
	m_soundAttr.pitch = pitch;
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetOuterGain(double gain) {
	if (gain < 0.0)
		gain = 0.0;
	m_soundAttr.outerGain = gain;
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetMaxDist(double dist) {
	m_soundAttr.maxDist = dist;
	UpdateRangeMatrix();
	UpdateConeAngleRingMeshes();
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetRefDist(double dist) {
	m_soundAttr.refDist = dist;
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetRolloff(double rolloff) {
	m_soundAttr.rolloff = rolloff;
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetConeOuterAngleRad(double radians) {
	m_soundAttr.coneOuterAngle = radians;
	UpdateConeAngleRingMeshes();
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetConeInnerAngleRad(double radians) {
	m_soundAttr.coneInnerAngle = radians;
	UpdateConeAngleRingMeshes();
	SourceSetAll();
	return true;
}

bool SoundPlacement::SetPosition(const Vector3f& pos) {
	if (m_soundAttr.pos == pos)
		return true;

	m_soundAttr.pos = pos;

	// Set Position Vector
	m_locationMat.GetTranslation() = pos;

	m_pVisibleBody->SetTransformMetric(FeetToMeters(m_locationMat), true);

	// Update Range Matrix & Ring Meshes
	UpdateRangeMatrix();

	SourceSetAll();
	return true;
}

bool SoundPlacement::SetDirection(const Vector3f& dir) {
	m_soundAttr.dir = dir;

	// Set Direction Vector (x=adjacent, y=0.0, z=opposite)
	m_locationMat.SetRowZ(dir);
	m_locationMat.SetRowY(Vector3f(0, 1, 0));

	// calc x as cross of y and z
	Vector3f vUp = m_locationMat.GetRowY();
	Vector3f vDir = m_locationMat.GetRowZ();
	Vector3f vCross = vUp.Cross(vDir);
	m_locationMat.SetRowX(vCross);

	SourceSetAll();
	return true;
}

double SoundPlacement::GetSoundAttribute(ULONG attr) {
	switch (attr) {
		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_INITED:
			return m_inited ? 1.0 : 0.0;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_RELATIVE:
			return GetRelative() ? 1.0 : 0.0;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP:
			return GetLooping() ? 1.0 : 0.0;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP_DELAY:
			return (double)GetLoopingDelay();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_PITCH:
			return GetPitch();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_GAIN:
			return GetGain();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_MAX_DISTANCE:
			return GetMaxDist();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR:
			return GetRolloff();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_POS_X:
			return GetPosition().x;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_POS_Y:
			return GetPosition().y;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_POS_Z:
			return GetPosition().z;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN:
			return GetOuterGain();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE:
			return GetConeInnerAngleDeg();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE:
			return GetConeOuterAngleDeg();

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_DIR_X:
			return GetDirection().x;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_DIR_Y:
			return GetDirection().y;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_DIR_Z:
			return GetDirection().z;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_ROT:
			return GetRotationDeg();
	}

	return GetSet::GetNumber(attr);
}

bool SoundPlacement::SetSoundAttribute(ULONG attr, double value) {
	switch (attr) {
		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_PLAYBACKCTRL: {
			int controlId = (int)value;
			if (controlId == SOUND_START) {
				//				LogInfo(ToStr() << " SCRIPT PLAY");
				if (Playing())
					Rewind();
				m_scriptPlay = true;
				return true;
			} else if (controlId == SOUND_STOP) {
				//				LogInfo(ToStr() << " SCRIPT STOP");
				//if (Playing())
				//	Stop();
				Stop(true); // force it to stop
				m_scriptPlay = false;
				return true;
			}
		} break;

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_RELATIVE:
			return SetRelative(value > 0.1);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP:
			return SetLooping(value > 0.1);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP_DELAY:
			return SetLoopingDelay((TimeMs)value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_PITCH:
			return SetPitch(value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_GAIN:
			return SetGain(value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_MAX_DISTANCE:
			return SetMaxDist(value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR:
			return SetRolloff(value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_POS_X:
			return SetPosition(Vector3f(value, GetPosition().y, GetPosition().z));

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_POS_Y:
			return SetPosition(Vector3f(GetPosition().x, value, GetPosition().z));

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_POS_Z:
			return SetPosition(Vector3f(GetPosition().x, GetPosition().y, value));

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN:
			return SetOuterGain(value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE:
			return SetConeInnerAngleDeg(value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE:
			return SetConeOuterAngleDeg(value);

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_DIR_X:
			return SetDirection(Vector3f(value, GetDirection().y, GetDirection().z));

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_DIR_Y:
			return SetDirection(Vector3f(GetDirection().x, value, GetDirection().z));

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_DIR_Z:
			return SetDirection(Vector3f(GetDirection().x, GetDirection().y, value));

		case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_ROT:
			return SetRotationDeg(value);
	}

	GetSet::SetNumber(attr, value);
	return true;
}

void SoundPlacement::SetHover(bool hover) {
#ifdef _ClientOutput
	if (m_soundMesh && m_soundMesh->GetMaterial() && m_soundMesh->GetMaterial()->GetRenderState() && m_soundMesh->GetMaterial()->GetRenderState()->GetData()) {
		if (hover) {
			m_material->m_useMaterial.Emissive = (SOUND_ROLLOVER_EMISSIVE + SOUND_EMISSIVE) * SND_DEFAULT_COLOR;
			m_material->m_baseMaterial.Emissive = m_material->m_useMaterial.Emissive;
			// annoying can't modify sorted materials have to go thru CMaterial

			MeshRM::Instance()->CreateReMaterial(m_material);
			m_material->GetReMaterial()->PreLight(FALSE);
			m_soundMesh->SetMaterial(m_material->GetReMaterial());
		} else {
			// annoying can't modify sorted materials have to go thru CMaterial
			m_material->m_useMaterial.Emissive = SND_DEFAULT_COLOR * SOUND_EMISSIVE;
			m_material->m_baseMaterial.Emissive = m_material->m_useMaterial.Emissive;
			MeshRM::Instance()->CreateReMaterial(m_material);
			m_material->GetReMaterial()->PreLight(FALSE);
			m_soundMesh->SetMaterial(m_material->GetReMaterial());
		}
	}
#endif
}

bool SoundPlacement::IntersectRay(const Vector3f& origin, const Vector3f& direction, float* distance, Vector3f* location, Vector3f* normal) const {
	bool intersectFound = false;

#ifdef _ClientOutput
	float closestDist = FLT_MAX;
	ReMesh* reMesh = GetSoundMesh();
	if (reMesh) {
		/* compute world matrix for visual */
		/* invert visual matrix to get from world space to visual space */
		FLOAT determinant = 0.0f;
		Matrix44f matVisualInv;
		matVisualInv.MakeInverseOf(m_locationMat, &determinant);

		if (determinant > 1e-5) {
			Vector3f osOrigin, osDirection;
			D3DXVECTOR4 tmp;

			/* transform ray origin */
			osOrigin = TransformPoint_NonPerspective(matVisualInv, origin);

			/* transform and normalize ray direction */
			osDirection = TransformVector(matVisualInv, direction);
			osDirection.Normalize();

			/* check for intersection */
			float dist = FLT_MAX;

			if (reMesh->IntersectRay(osOrigin, osDirection, &dist, location)) {
				intersectFound = true;

				if (dist < closestDist)
					closestDist = dist;
			}
		}
	}

	if (intersectFound) {
		if (distance)
			*distance = closestDist;
		if (location)
			*location = origin + closestDist * direction;
	}
#endif

	return intersectFound;
}

bool SoundPlacement::Rewind() {
	// Stop Sound
	Stop();

	// Rewind Proxy Sound
	return ProxySoundRewind();
}

bool SoundPlacement::Stop(bool force) {
	// Already Stopped ?
	if (!Playing())
		return true;

	// Set Not Currently Playing
	SetPlaying(false);

	// Stop Proxy Sound
	return ProxySoundStop(force);
}

bool SoundPlacement::Play() {
	// Sound Proxy Sound Not Ready ?
	if (!ProxySoundReady())
		return false;

	// Initialize Source
	if (!SourceInit())
		return false;

	// Set Currently Playing
	SetPlaying(true);

	// Reset Looping Delay
	LoopingDelayReset();

	// Play Proxy Sound
	return ProxySoundPlay();
}

bool SoundPlacement::Update(const Vector3f& posListener, const Vector3f* posNew) {
	// Update Sound Proxy
	if (ProxySound())
		ProxySound()->Update(m_source);

	// Save My Avatar Position (globally used for range checks)
	g_posListener = posListener;

	// Update Sound Placement Position
	if (posNew)
		SetPosition(*posNew);

	// Sound In Range Of My Avatar?
	bool inRange = InRange(g_posListener);
	double dist = sqrt(DistSqPos(g_posListener));

	// Proxy Sound Not Playing ? (finished single play loop)
	bool mute = false; //(m_initTimer.elapsed() < initGainTimeMs);
	if (!ProxySoundPlaying() && !mute) {
		// ED-794 - DRF - OpenAL Limit Is 256 Sounds
		// Delete Source While Out Of Range (Play() adds source)
		if (Playing() && (PlaybackMode() != PM_ALWAYS_ON)) {
			if (!inRange) {
				LogQuiet(ToStr() << " RANGE STOP - dist=" << dist);
				Stop();
				if (SourceOk())
					SourceDel(false);
			}
		}

		// Start Looping Delay Timer (if not already started)
		LoopingDelayStart();

		// Attempt Play If Proxy Sound Ready And Not Playing Or Loop Elapsed
		bool playing = Playing();
		bool looping = GetLooping();
		bool loopingElapsed = playing && looping && LoopingDelayElapsed();
		bool doPlay = ProxySoundReady() && (!playing || loopingElapsed);
		if (doPlay) {
			// Start Playing Depending On Playback Mode
			switch (PlaybackMode()) {
				case PM_ALWAYS_ON: {
					LogQuiet(ToStr() << " ALWAYS PLAY");
					Play();
				} break;

				case PM_TRIGGERED_ON:
				case PM_TRIGGERED_ONOFF: {
					if (!inRange)
						break;
					LogQuiet(ToStr() << " RANGE PLAY -" << (looping ? " LOOPING" : "") << " dist=" << dist);
					Play();
				} break;

				case PM_SCRIPT: {
					if (!inRange || !m_scriptPlay) {
						if (!looping)
							m_scriptPlay = false;
						break;
					}
					if (!looping)
						m_scriptPlay = false;
					LogQuiet(ToStr() << " RANGE SCRIPT PLAY -" << (looping ? " LOOPING" : "") << " dist=" << dist);
					Play();
				} break;
			}
		}
	}

	return true;
}

bool SoundPlacement::LoadSoundByGlid(const GLID& glid) {
	LogInfo(ToStr() << " glid<" << glid << ">");

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	// Stop Incase Already Playing
	Stop();

	// Delete Source & Proxy
	if (SourceOk() || m_proxy)
		SourceDel(true);

	// Get Sound Proxy From Resource Manager
	SoundRM* soundRM = pICE->GetSoundRM();
	if (!soundRM)
		return false;
	m_proxy = soundRM->getProxy(glid, true);
	if (!m_proxy) {
		if (IS_VALID_GLID(glid)) {
			LogError("getProxy() FAILED - glid<" << glid << ">");
			return false;
		} else
			return true;
	}

	m_glid = glid;
	m_proxy->SetHashKey((UINT64)glid);
	m_proxy->GetSoundBuffer();

	return true;
}

void SoundPlacement::GenConeMesh() {
	if (m_coneMesh)
		delete m_coneMesh;

	const int rimCount = 16;
	double length = GetMaxDist();
	double radius = sin(GetConeOuterAngleRad()) * length;

	ReMesh* rm = KEPWidgetMeshes::getConeMesh(length, radius, rimCount);

#ifdef _ClientOutput
	MeshRM::Instance()->CreateReMaterial(m_coneMaterial);
	m_coneMaterial->GetReMaterial()->PreLight(FALSE);
	rm->SetMaterial(m_coneMaterial->GetReMaterial());
#endif

	m_coneMesh = rm;
}

ReMesh* SoundPlacement::getRingMesh(double angle, double radius, CMaterialObject* mat) {
	auto mesh = KEPWidgetMeshes::getRingMesh(angle, radius);
	if (mesh) {
		mesh->SetMaterial(mat->GetReMaterial());
	}

	return mesh;
}

double SoundPlacement::GetRotationRad() const {
#if (DRF_NEW_ANGLE == 1)
	return RadNorm(atan2(GetDirection().x, GetDirection().z));
#else
	return RadNorm(atan2(GetDirection().x, GetDirection().z) - (M_PI / 2.0));
#endif
}

OALSoundBuffer* SoundPlacement::ProxySound() const {
	return (m_proxy ? m_proxy->GetSoundBuffer() : NULL);
}

bool SoundPlacement::ProxySoundPlaying() const {
	auto proxySound = ProxySound();
	return proxySound ? proxySound->Playing(m_source) : false;
}

bool SoundPlacement::ProxySoundPlay() const {
	auto proxySound = ProxySound();
	return proxySound ? proxySound->Play(m_source) : false;
}

bool SoundPlacement::ProxySoundStop(bool force /*= false*/) const {
	auto proxySound = ProxySound();
	return proxySound ? proxySound->Stop(m_source, force) : false;
}

bool SoundPlacement::ProxySoundRewind() const {
	auto proxySound = ProxySound();
	return proxySound ? proxySound->Rewind(m_source) : false;
}

BEGIN_GETSET_IMPL(SoundPlacement, IDS_SOUNDPLACEMENT_NAME)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_PITCH, IDS_SOUNDPLACEMENT_PITCH, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_GAIN, IDS_SOUNDPLACEMENT_GAIN, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_MAX_DISTANCE, IDS_SOUNDPLACEMENT_MAX_DISTANCE, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR, IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_LOOP, IDS_SOUNDPLACEMENT_LOOP, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsUlong, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_LOOP_DELAY, IDS_SOUNDPLACEMENT_LOOP_DELAY, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_POS_X, IDS_SOUNDPLACEMENT_POS_X, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_POS_Y, IDS_SOUNDPLACEMENT_POS_Y, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_POS_Z, IDS_SOUNDPLACEMENT_POS_Z, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN, IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE, IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE, IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_DIR_X, IDS_SOUNDPLACEMENT_DIR_X, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_DIR_Y, IDS_SOUNDPLACEMENT_DIR_Y, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_DIR_Z, IDS_SOUNDPLACEMENT_DIR_Z, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_INITED, IDS_SOUNDPLACEMENT_INITED, INT_MIN, INT_MAX)
GETSET2_CUSTOM(NULL, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_PLAYBACKCTRL, IDS_SOUNDPLACEMENT_PLAYBACKCTRL, INT_MIN, INT_MAX)
GETSET2_RANGE(m_playbackMode, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_PLAYBACKMODE, IDS_SOUNDPLACEMENT_PLAYBACKMODE, 0, PLAYBACK_MODES - 1)
GETSET2_CUSTOM(NULL, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_RELATIVE, IDS_SOUNDPLACEMENT_RELATIVE, INT_MIN, INT_MAX);
GETSET2_CUSTOM(NULL, gsFloat, GS_FLAGS_DEFAULT, 0, 0, IDS_SOUNDPLACEMENT_ROT, IDS_SOUNDPLACEMENT_ROT, INT_MIN, INT_MAX);
END_GETSET_IMPL

} // namespace KEP
