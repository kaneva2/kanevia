///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS

#include <d3d9.h>
#include <afxtempl.h>
#include "CMaterialClass.h"
#include "TextureDatabase.h"
#ifdef _ClientOutput
#include "IClientEngine.h"
#endif
#include "ReD3DX9.h"
#include "ReMaterial.h"
#include "Utils/ReUtils.h"
#include "MaterialPersistence.h"

#include "TextureManager.h"
#include "Common/Include/TextureProvider.h"
#include "Common/Include/IMemSizeGadget.h"

namespace KEP {

CMaterialObject::CMaterialObject() {
	init();
}

CMaterialObject::~CMaterialObject() {
	DeleteParamMemoryAndZeroIt();
}

void CMaterialObject::init() {
	m_optionalShaderIndex = 0;
	m_baseMaterial.Diffuse.r = 1.0f;
	m_baseMaterial.Diffuse.g = 1.0f;
	m_baseMaterial.Diffuse.b = 1.0f;
	m_baseMaterial.Diffuse.a = 1.0f;
	m_baseMaterial.Ambient.r = 1.0f;
	m_baseMaterial.Ambient.g = 1.0f;
	m_baseMaterial.Ambient.b = 1.0f;
	m_baseMaterial.Ambient.a = 1.0f;
	m_baseMaterial.Specular.r = 0.0f;
	m_baseMaterial.Specular.g = 0.0f;
	m_baseMaterial.Specular.b = 0.0f;
	m_baseMaterial.Specular.a = 0.0f;
	m_baseMaterial.Emissive.r = 0.0f;
	m_baseMaterial.Emissive.g = 0.0f;
	m_baseMaterial.Emissive.b = 0.0f;
	m_baseMaterial.Emissive.a = 0.0f;
	m_baseMaterial.Power = 0.0f;
	CopyMaterial(m_baseMaterial, &m_useMaterial);
	CopyMaterial(m_baseMaterial, &m_targetModMaterial);
	m_cycleTime = 500; //milliseconds
	m_switch = FALSE;
	m_timeStamp = -1;
	m_transition = 0;
	m_textureTileType = 0;
	m_cullOn = TRUE;
	m_forceWireFrame = FALSE;
	m_blendMode = -1;
	m_baseBlendMode = m_blendMode;
	m_mirrorEnabled = FALSE;
	m_curDistFromCam = 0.0f;
	m_fadeMaxDist = 2700;
	m_fadeVisDist = 2400;
	m_fadeByDistance = FALSE;
	m_materialName = "";
	m_RGBOverride = FALSE;
	m_forceFullRes = false;

	m_enableTextureMovement = FALSE;
	for (unsigned int i = 0; i < MAX_UV_SETS; i++) {
		m_enableSet[i] = FALSE;
		m_textureMovement[i].tu = 0.0f;
		m_textureMovement[i].tv = 0.0f;
	}

	m_diffuseEffect = 0;
	m_fogFilter = FALSE;
	m_zBias = 0;
	m_sortOrder = ATTRIB_DEFAULT_SORT_ORDER;
	m_fogOverride = FALSE;
	m_transitionBlend = -2;

	for (size_t i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
		m_colorArg1[i] = 0;
		m_colorArg2[i] = 0;
	}

	m_colorArg1[0] = 2;
	m_colorArg1[1] = 2;
	m_colorArg1[2] = 2;
	m_colorArg1[3] = 2;
	m_colorArg1[4] = 2;
	m_colorArg1[5] = 2;
	m_colorArg1[6] = 2;
	m_colorArg1[7] = 2;
	m_colorArg1[8] = 2;

	m_colorArg2[0] = 0;
	m_colorArg2[1] = 1;
	m_colorArg2[2] = 1;
	m_colorArg2[3] = 1;
	m_colorArg2[4] = 1;
	m_colorArg2[5] = 1;
	m_colorArg2[6] = 1;
	m_colorArg2[7] = 1;
	m_colorArg2[8] = 1;

	m_pixelShader = -1;
	m_vertexShader = -1;

	m_shadowObject = 0;
	m_doAlphaDepthTest = FALSE;

	m_paramBlock = NULL;
	m_paramCount = 0;

	m_texFileNameIsConcrete = true;
	for (size_t i = 0; i < NUM_CMATERIAL_TEXTURES; ++i) {
		m_texNameHashStored[i] = 0; //INVALID_TEXTURE_NAMEHASH;
		m_texNameHash[i] = 0; //INVALID_TEXTURE_NAMEHASH;
		m_texNameHashOriginal[i] = 0; //INVALID_TEXTURE_NAMEHASH;
		m_texColorKey[i] = 0;
		m_texBlendMethod[i] = -1;
	}

	m_myCompressor = NULL;
	m_myCompressionIndex = CMATERIAL_UNCOMPRESSED;
	for (int j = 0; j < NUM_CMATERIAL_TEXTURES; j++) {
		m_compressionTextureAssignments[j] = -1;
	}
	m_compressionColorTableIndexUse = -1;
	m_compressionColorTableIndexMod = -1;
	m_compressionFlagsIndex = -1;
	m_customTextureOption = NO_CUSTOM_TEXTURE;
	m_normalizeNormals = FALSE;
}

void CMaterialObject::SetReMaterial(ReMaterialPtr mat) {
	m_ReMat = mat;
}

void CMaterialObject::GetReRenderState(ReRenderStateData* state, const std::string& subDir) {
#ifdef _ClientOutput
	// copy over the material state
	memcpy((void*)&state->Mat, (void*)&m_useMaterial, sizeof(ReMatProps));

	// get an int version of material props for hash (eliminates floating point error)
	int intMat[4][4] = { { lrint(state->Mat.Diffuse.x * 100), lrint(state->Mat.Diffuse.y * 100),
							 lrint(state->Mat.Diffuse.z * 100), lrint(state->Mat.Diffuse.w * 100) },
		{ lrint(state->Mat.Ambient.x * 100), lrint(state->Mat.Ambient.y * 100),
			lrint(state->Mat.Ambient.z * 100), lrint(state->Mat.Ambient.w * 100) },
		{ lrint(state->Mat.Specular.x * 100), lrint(state->Mat.Specular.y * 100),
			lrint(state->Mat.Specular.z * 100), lrint(state->Mat.Specular.w * 100) },
		{ lrint(state->Mat.Emissive.x * 100), lrint(state->Mat.Emissive.y * 100),
			lrint(state->Mat.Emissive.z * 100), lrint(state->Mat.Emissive.w * 100) } };

	// get the hash value of the material state
	state->MatHash = ReHash((BYTE*)&intMat[0][0], 16 * sizeof(int), 0);

	// alpha test enable
	state->Mode.Alphatestenable = m_doAlphaDepthTest;

	// set the fog filter
	state->Mode.FogFilter = m_fogFilter;

	// culling mode
	switch (m_cullOn) {
		case 0:
			state->Mode.CullMode = D3DCULL_NONE;
			break;

		case 1:
			state->Mode.CullMode = D3DCULL_CCW;
			break;

		case 2:
			state->Mode.CullMode = D3DCULL_CW;
			break;

		default:
			state->Mode.CullMode = D3DCULL_NONE;
			break;
	}

	ReD3DX9DeviceState* dev = NULL;
	auto pICE = IClientEngine::Instance();
	if (pICE)
		dev = pICE->GetReDeviceState(); // Try client version if exists
	else
		dev = ReD3DX9DeviceState::Instance(); // Otherwise use singleton -- this is to allow use of ReD3DX9DeviceState without a client engine

	// set default prelighting
	// Don't pre-light if the colors are animated.
	if (pICE && pICE->GetReDeviceState()->GetRenderState()->GetPreLighting() && m_transition == 0)
		state->ColorSrc = ReColorSource::VERTEX;
	else
		state->ColorSrc = ReColorSource::MATERIAL;

	// blending state
	switch (m_blendMode) {
		case BLEND_DISABLED:
			state->Mode.Zwriteenable = TRUE;
			state->Mode.Alphaenable = FALSE;
			state->Blend.Srcblend = D3DBLEND_DESTCOLOR;
			state->Blend.Dstblend = D3DBLEND_ONE;
			break;

		case BLEND_MULTIPLY_ADD:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_DESTCOLOR;
			state->Blend.Dstblend = D3DBLEND_ONE;
			break;

		case BLEND_ALPHA:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_SRCALPHA;
			state->Blend.Dstblend = D3DBLEND_INVSRCALPHA;
			break;

		case BLEND_ADDITION:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_ONE;
			state->Blend.Dstblend = D3DBLEND_ONE;
			break;

		case BLEND_SQUARE_ADD:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_SRCCOLOR;
			state->Blend.Dstblend = D3DBLEND_DESTCOLOR;
			break;

		case BLEND_INV_MULTIPLY:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_ZERO;
			state->Blend.Dstblend = D3DBLEND_INVSRCCOLOR;
			break;

		case BLEND_INV_MULTIPLY_ALT:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_ZERO;
			state->Blend.Dstblend = D3DBLEND_INVSRCCOLOR;
			break;

		case BLEND_MULTIPLY:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_ZERO;
			state->Blend.Dstblend = D3DBLEND_SRCCOLOR;
			break;

		case BLEND_ALPHA_ALT:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_SRCALPHA;
			state->Blend.Dstblend = D3DBLEND_INVSRCALPHA;
			break;

		default:
			state->Mode.Zwriteenable = FALSE;
			state->Mode.Alphaenable = TRUE;
			state->Blend.Srcblend = D3DBLEND_DESTCOLOR;
			state->Blend.Dstblend = D3DBLEND_ONE;
			break;
	}

	// NormalizeNormals flag
	state->Mode.NormalizeNormals = m_normalizeNormals;

	// get the hash value of the mode state
	state->ModeHash = ReHash((BYTE*)&state->Mode, sizeof(ReModeProps), 0);
	// get the hash value of the blend state
	state->BlendHash = ReHash((BYTE*)&state->Blend, sizeof(ReBlendProps), 0);

	// initialize the sort order
	state->SortOrder = 0;

	// renderer sorts by the blend state hash code...
	switch (m_blendMode) {
		// opaque renderstate
		case BLEND_DISABLED:
			// if zbiasing is enabled.... we are emulating zbiasing by sorting
			if (m_zBias != 0) {
				if (m_zBias >= 0 && m_zBias < 16) {
					state->SortOrder = m_zBias;
				}
			}
			break;

			// transparent renderstate: set MSB higher than 0... higher hash will render-sort last
		default:
			// sort order disabled....render before sort-order meshes but after opaque meshes
			if (m_sortOrder == -1) {
				state->SortOrder = 1 << 4;
			}
			// sort order enabled...ranges from 2 to 15 sort levels
			else {
				if (m_sortOrder >= 0 && m_sortOrder < 16) {
					int sortOrder = m_sortOrder + 2;
					sortOrder = sortOrder > 15 ? 15 : sortOrder;
					state->SortOrder = sortOrder << 4;
				}
			}
			break;
	}

	// init the texture stages
	state->NumTexStages = 0;

	TextureDatabase* pTextureDB = NULL;
	if (pICE)
		pTextureDB = pICE->GetTextureDataBase(); // Use client version if IClientEngine has an instance
	if (!pTextureDB)
		return;

	for (DWORD i = 0; i < ReDEVICEMAXTEXTURESTAGES; i++) {
		long blendType;

		// Skip Empty Texture Slots (nameHash=0)
		std::string texFileName = m_texFileName[i];
		DWORD texNameHash = m_texNameHash[i];
		if (!ValidTextureFileName(texFileName) && (texNameHash == 0))
			break;

		if (i == 0) {
			blendType = m_texBlendMethod[9] < 0 ? 0 : m_texBlendMethod[9];
		} else {
			blendType = m_texBlendMethod[i - 1];

			// if texture stage is disabled, break
			if (blendType == -1)
				break;
		}

		state->TexPtr[i] = NULL;
		state->ResPtr[i] = NULL;

		CTextureEX* pTex = NULL;
		DWORD nameHash = m_texNameHash[i];
		if (pTextureDB && ValidTextureNameHash(nameHash)) {
			pTex = pTextureDB->TextureGet(m_texNameHash[i], m_texFileNameIsConcrete ? m_texFileName[i] : "", subDir, m_texColorKey[i]);
		}

		if (pTex) {
			pTex->SetForceFullRes(m_forceFullRes);
			state->ResPtr[i] = pTex;

			switch (m_colorArg1[i]) {
				case 0:
					state->Tex[i].ColorArg1 = D3DTA_DIFFUSE;
					break;
				case 1:
					state->Tex[i].ColorArg1 = D3DTA_CURRENT;
					break;
				case 2:
					state->Tex[i].ColorArg1 = D3DTA_TEXTURE;
					break;
				case 3:
					state->Tex[i].ColorArg1 = D3DTA_TFACTOR;
					break;
				case 4:
					state->Tex[i].ColorArg1 = D3DTA_SPECULAR;
					break;
				case 5:
					state->Tex[i].ColorArg1 = D3DTA_SELECTMASK;
					break;
				case 6:
					state->Tex[i].ColorArg1 = D3DTA_DIFFUSE | D3DTA_COMPLEMENT;
					break;
				case 7:
					state->Tex[i].ColorArg1 = D3DTA_CURRENT | D3DTA_COMPLEMENT;
					break;
				case 8:
					state->Tex[i].ColorArg1 = D3DTA_TEMP | D3DTA_COMPLEMENT;
					break;
				case 9:
					state->Tex[i].ColorArg1 = D3DTA_TEXTURE | D3DTA_COMPLEMENT;
					break;
				case 10:
					state->Tex[i].ColorArg1 = D3DTA_TEXTURE | D3DTA_ALPHAREPLICATE;
					break;
				default:;
					state->Tex[i].ColorArg1 = D3DTA_DIFFUSE;
					break;
			}

			switch (m_colorArg2[i]) {
				case 0:
					state->Tex[i].ColorArg2 = D3DTA_DIFFUSE;
					break;
				case 1:
					state->Tex[i].ColorArg2 = D3DTA_CURRENT;
					break;
				case 2:
					state->Tex[i].ColorArg2 = D3DTA_TEXTURE;
					break;
				case 3:
					state->Tex[i].ColorArg2 = D3DTA_TFACTOR;
					break;
				case 4:
					state->Tex[i].ColorArg2 = D3DTA_SPECULAR;
					break;
				case 5:
					state->Tex[i].ColorArg2 = D3DTA_SELECTMASK;
					break;
				case 6:
					state->Tex[i].ColorArg2 = D3DTA_DIFFUSE | D3DTA_COMPLEMENT;
					break;
				case 7:
					state->Tex[i].ColorArg2 = D3DTA_CURRENT | D3DTA_COMPLEMENT;
					break;
				case 8:
					state->Tex[i].ColorArg2 = D3DTA_TEMP | D3DTA_COMPLEMENT;
					break;
				case 9:
					state->Tex[i].ColorArg2 = D3DTA_TEXTURE | D3DTA_COMPLEMENT;
					break;
				case 10:
					state->Tex[i].ColorArg2 = D3DTA_TEXTURE | D3DTA_ALPHAREPLICATE;
					break;
				default:;
					state->Tex[i].ColorArg2 = D3DTA_DIFFUSE;
					break;
			}

			switch (blendType) {
				case TOP_DISABLE:
					state->Tex[i].ColorOp = D3DTOP_DISABLE;
					break;
				case TOP_MODULATE:
					state->Tex[i].ColorOp = D3DTOP_MODULATE;
					break;
				case TOP_ADD:
					state->Tex[i].ColorOp = D3DTOP_ADD;
					break;
				case TOP_SUBTRACT:
					state->Tex[i].ColorOp = D3DTOP_SUBTRACT;
					break;
				case TOP_ADDSMOOTH:
					state->Tex[i].ColorOp = D3DTOP_ADDSMOOTH;
					break;
				case TOP_ADDSIGNED:
					state->Tex[i].ColorOp = D3DTOP_ADDSIGNED;
					break;
				case TOP_MODULATE2X:
					state->Tex[i].ColorOp = D3DTOP_MODULATE2X;
					break;
				case TOP_TYPE6:
					state->Tex[i].ColorOp = D3DTOP_MODULATE;
					break;
				case TOP_TYPE7:
					state->Tex[i].ColorOp = D3DTOP_MODULATE;
					break;
				case TOP_SELECTARG1:
					state->Tex[i].ColorOp = D3DTOP_SELECTARG1;
					break;
				case TOP_SELECTARG2:
					state->Tex[i].ColorOp = D3DTOP_SELECTARG2;
					break;
				case TOP_MODULATE4X:
					state->Tex[i].ColorOp = D3DTOP_MODULATE4X;
					break;
				case TOP_ADDSIGNED2X:
					state->Tex[i].ColorOp = D3DTOP_ADDSIGNED2X;
					break;
				case TOP_PREMODULATE:
					state->Tex[i].ColorOp = D3DTOP_PREMODULATE;
					break;
				case TOP_MULTIPLYADD:
					state->Tex[i].ColorOp = D3DTOP_MULTIPLYADD;
					break;
				case TOP_BLENDCURRENTALPHA:
					state->Tex[i].ColorOp = D3DTOP_BLENDCURRENTALPHA;
					if (i > 0) {
						state->Tex[i - 1].AlphaOp = D3DTOP_SELECTARG1;
					}
					break;
				default:
					state->Tex[i].ColorOp = D3DTOP_DISABLE;
					break;
			}

			if (blendType != TOP_BLENDCURRENTALPHA) {
				if (i == 0) {
					state->Tex[i].AlphaOp = D3DTOP_SELECTARG1;
				} else {
					state->Tex[i].AlphaOp = D3DTOP_DISABLE;
				}
			}

			state->NumTexStages += 1;

		} else {
			state->ResPtr[i] = NULL;
		}
	}

	// always at least 1 tex stage
	if (state->NumTexStages == 0) {
		state->NumTexStages = 1;
		state->Tex[0].ColorOp = D3DTOP_MODULATE;
		state->Tex[0].AlphaOp = D3DTOP_SELECTARG1;

		state->Tex[0].ColorArg1 = D3DTA_DIFFUSE;
		state->Tex[0].ColorArg2 = D3DTA_CURRENT;
	}

	// get the hash code of the texture states
	for (DWORD i = 0; i < state->NumTexStages; i++)
		state->TexHash[i] = ReHash((BYTE*)&state->Tex[i], sizeof(ReTexProps), 0);
#else
	ASSERT(FALSE);
#endif
}

void CMaterialObject::ComputeTextureNameHashes(bool saveAsOriginal) {
	for (DWORD i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
		if (ValidTextureFileName(m_texFileName[i]) && ValidTextureNameHash(m_texNameHash[i])) { // != 0xffffffff
			assert(m_texNameHash[i] == 0);
			m_texNameHash[i] = TextureDatabase::TextureNameHash(m_texFileName[i], m_texColorKey[i]);
		}

		if (saveAsOriginal) {
			assert(m_texFileNameOriginal[i].empty());
			m_texFileNameOriginal[i] = m_texFileName[i];
			assert(m_texNameHashOriginal[i] == 0);
			m_texNameHashOriginal[i] = m_texNameHash[i];
		}
	}
}

void CMaterialObject::DeleteParamMemoryAndZeroIt() {
	if (m_paramBlock) {
		for (int loop = 0; loop < m_paramCount; loop++) {
			if (&m_paramBlock[loop] && m_paramBlock[loop].dataSize > 0 && m_paramBlock[loop].data) {
				delete[] m_paramBlock[loop].data;
				m_paramBlock[loop].data = 0;
			}
		}

		delete[] m_paramBlock;
		m_paramBlock = 0;
	}
}

CMaterialObject::CMaterialObject(D3DMATERIAL9 materialInit) {
	init();

	m_baseMaterial.Diffuse.r = materialInit.Diffuse.r;
	m_baseMaterial.Diffuse.g = materialInit.Diffuse.g;
	m_baseMaterial.Diffuse.b = materialInit.Diffuse.b;
	m_baseMaterial.Diffuse.a = materialInit.Diffuse.a;
	m_baseMaterial.Ambient.r = materialInit.Ambient.r;
	m_baseMaterial.Ambient.g = materialInit.Ambient.g;
	m_baseMaterial.Ambient.b = materialInit.Ambient.b;
	m_baseMaterial.Ambient.a = materialInit.Ambient.a;
	m_baseMaterial.Specular.r = materialInit.Specular.r;
	m_baseMaterial.Specular.g = materialInit.Specular.g;
	m_baseMaterial.Specular.b = materialInit.Specular.b;
	m_baseMaterial.Specular.a = materialInit.Specular.a;
	m_baseMaterial.Emissive.r = materialInit.Emissive.r;
	m_baseMaterial.Emissive.g = materialInit.Emissive.g;
	m_baseMaterial.Emissive.b = materialInit.Emissive.b;
	m_baseMaterial.Emissive.a = materialInit.Emissive.a;
	m_baseMaterial.Power = materialInit.Power;
	CopyMaterial(m_baseMaterial, &m_useMaterial);

	//milliseconds
	m_cycleTime = 500;

	m_switch = FALSE;
	m_timeStamp = -1;
	m_transition = 0;
	m_paramBlock = NULL;
	m_paramCount = 0;
}

void CMaterialObject::Clone(CMaterialObject** clone) const {
	CMaterialObject* interm = new CMaterialObject(m_baseMaterial);
	interm->m_cycleTime = m_cycleTime;
	interm->m_timeStamp = m_timeStamp;
	interm->m_switch = m_switch;
	interm->m_transition = m_transition;
	interm->m_timeStamp = -1;
	interm->m_textureTileType = m_textureTileType;
	interm->m_blendMode = m_blendMode;
	interm->m_baseBlendMode = m_blendMode;
	interm->m_mirrorEnabled = m_mirrorEnabled;
	interm->m_fogFilter = m_fogFilter;
	interm->m_zBias = m_zBias;
	interm->m_sortOrder = m_sortOrder;
	interm->m_fogOverride = m_fogOverride;
	interm->m_diffuseEffect = m_diffuseEffect;
	interm->m_optionalShaderIndex = m_optionalShaderIndex;
	interm->m_shadowObject = m_shadowObject;

	interm->m_enableTextureMovement = m_enableTextureMovement;
	for (unsigned int i = 0; i < MAX_UV_SETS; i++)
		interm->m_enableSet[i] = m_enableSet[i];

	interm->m_textureMovement[0].tu = m_textureMovement[0].tu;
	interm->m_textureMovement[0].tv = m_textureMovement[0].tv;
	interm->m_textureMovement[1].tu = m_textureMovement[1].tu;
	interm->m_textureMovement[1].tv = m_textureMovement[1].tv;
	interm->m_textureMovement[2].tu = m_textureMovement[2].tu;
	interm->m_textureMovement[2].tv = m_textureMovement[2].tv;
	interm->m_textureMovement[3].tu = m_textureMovement[3].tu;
	interm->m_textureMovement[3].tv = m_textureMovement[3].tv;
	interm->m_textureMovement[4].tu = m_textureMovement[4].tu;
	interm->m_textureMovement[4].tv = m_textureMovement[4].tv;
	interm->m_textureMovement[5].tu = m_textureMovement[5].tu;
	interm->m_textureMovement[5].tv = m_textureMovement[5].tv;
	interm->m_textureMovement[6].tu = m_textureMovement[6].tu;
	interm->m_textureMovement[6].tv = m_textureMovement[6].tv;
	interm->m_textureMovement[7].tu = m_textureMovement[7].tu;
	interm->m_textureMovement[7].tv = m_textureMovement[7].tv;
	interm->m_forceWireFrame = m_forceWireFrame;
	interm->m_fadeMaxDist = m_fadeMaxDist;
	interm->m_fadeVisDist = m_fadeVisDist;
	interm->m_fadeByDistance = m_fadeByDistance;
	interm->m_pixelShader = m_pixelShader;
	interm->m_vertexShader = m_vertexShader;
	interm->m_doAlphaDepthTest = m_doAlphaDepthTest;
	interm->m_fxBindString = m_fxBindString;
	interm->m_baseMaterial = m_baseMaterial;
	interm->m_useMaterial = m_useMaterial;
	interm->m_customTextureProvider = m_customTextureProvider;

	if (m_ReMat)
		interm->m_ReMat = m_ReMat->Clone(); //RENDERENGINE INTERFACE

	interm->m_materialName = m_materialName;

	CopyMaterial(m_targetModMaterial, &interm->m_targetModMaterial);

	CopyMemory(interm->m_colorArg1, m_colorArg1, sizeof(m_colorArg1));
	CopyMemory(interm->m_colorArg2, m_colorArg2, sizeof(m_colorArg2));

	//param block
	interm->m_paramCount = m_paramCount;
	if (m_paramCount > 0) {
		// param block clone
		interm->m_paramBlock = new SLARBparameterBlock[m_paramCount];

		ZeroMemory(interm->m_paramBlock, sizeof(SLARBparameterBlock) * m_paramCount);
		for (int pmL = 0; pmL < m_paramCount; pmL++) {
			CopyMemory(&interm->m_paramBlock[pmL], &m_paramBlock[pmL], sizeof(SLARBparameterBlock));

			if (interm->m_paramBlock[pmL].dataSize > 0)
				if (m_paramBlock[pmL].data) {
					interm->m_paramBlock[pmL].data = new byte[interm->m_paramBlock[pmL].dataSize];
					CopyMemory(interm->m_paramBlock[pmL].data, m_paramBlock[pmL].data, interm->m_paramBlock[pmL].dataSize);
				}
		}
	}

	interm->m_texFileNameIsConcrete = m_texFileNameIsConcrete;
	for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
		interm->m_texNameHash[loop] = m_texNameHash[loop];
		interm->m_texNameHashOriginal[loop] = m_texNameHashOriginal[loop];
		interm->m_texFileName[loop] = m_texFileName[loop];
		interm->m_texFileNameOriginal[loop] = m_texFileNameOriginal[loop];
		interm->m_texBlendMethod[loop] = m_texBlendMethod[loop];
		interm->m_texColorKey[loop] = m_texColorKey[loop];
	}

	interm->m_cullOn = m_cullOn;
	*clone = interm;
}

int CMaterialObject::GetParameterCount(LPD3DXEFFECT effect) {
	int trace = 0;
	while (1) {
		D3DXHANDLE floatingHandle = effect->GetParameter(NULL, trace);
		if (floatingHandle) {
			D3DXPARAMETER_DESC localDescStruct;
			if (effect->GetParameterDesc(floatingHandle, &localDescStruct) == S_OK) {
				trace++;
			}
		} else
			break;
	}
	return trace;
}

DWORD GetUsageIDFromSemantic(const char* semantic) {
	// these are for non-texture constants.  only the following are currently supported:
	/*
	--#define ARBP_UKNOWN					 (0)
	--#define ARBP_USEDEFAULT				 (1)
	--#define ARBP_WorldInverseTranspose (2)
	--#define ARBP_WorldViewProjection	 (3)
	--#define ARBP_World				 (4)
	--#define ARBP_ViewInverse			 (5)
	--#define ARBP_Time					 (6)
	--#define ARBP_WorldView			 (7)
	--#define ARBP_Projection			 (8)
	#define ARBP_Texture				(9)
	#define ARBP_Material				(12)
	#define ARBP_DirLight				(13)
	--#define ARBP_Diffuse				(14)
	--#define ARBP_Specular				(15)
	--#define ARBP_Emmisive				(16)
	--#define ARBP_Ambient				(17)
	--#define ARBP_SpecPower			(18)
	--#define ARBP_WorldInverse			(20)
	#define ARBP_VolumeTexturePrcdrl	(21)
	*/

	std::string stl_semantic(semantic);

	if (stl_semantic == "Ambient") {
		return ARBP_Ambient;
	} else if (stl_semantic == "Attenuation") {
	} else if (stl_semantic == "BoundingBoxMax") {
	} else if (stl_semantic == "BoundingBoxMin") {
	} else if (stl_semantic == "BoundingBoxSize") {
	} else if (stl_semantic == "BoundingBoxSize") {
	} else if (stl_semantic == "BoundingCenter") {
	} else if (stl_semantic == "BoundingSphereSize") {
	} else if (stl_semantic == "BoundingSphereMin") {
	} else if (stl_semantic == "BoundingSphereMax") {
	} else if (stl_semantic == "Diffuse") {
		return ARBP_Diffuse;
	} else if (stl_semantic == "ElapsedTime") {
		return ARBP_Time;
	} else if (stl_semantic == "Emissive") {
		return ARBP_Emmisive;
	} else if (stl_semantic == "EnvironmentNormal") {
	} else if (stl_semantic == "Height") {
	} else if (stl_semantic == "Joint") {
	} else if (stl_semantic == "JointWorld") {
	} else if (stl_semantic == "JointWorldInverse") {
	} else if (stl_semantic == "JointWorldInverseTranspose") {
	} else if (stl_semantic == "JointWorldView") {
	} else if (stl_semantic == "JointWorldViewInverse") {
	} else if (stl_semantic == "JointWorldViewInverseTranspose") {
	} else if (stl_semantic == "JointWorldViewProjection") {
	} else if (stl_semantic == "JointWorldViewProjectionInverse") {
	} else if (stl_semantic == "JointWorldViewProjectionInverseTranspose") {
	} else if (stl_semantic == "LastTime") {
	} else if (stl_semantic == "Normal") {
	} else if (stl_semantic == "Opacity") {
	} else if (stl_semantic == "Position") {
	} else if (stl_semantic == "Projection") {
		return ARBP_Projection;
	} else if (stl_semantic == "ProjectionInverse") {
	} else if (stl_semantic == "ProjectionInverseTranspose") {
	} else if (stl_semantic == "Random") {
	} else if (stl_semantic == "Refraction") {
	} else if (stl_semantic == "RenderColorTarget") {
	} else if (stl_semantic == "RenderDepthStencilTarget") {
	} else if (stl_semantic == "RenderTargetClipping") {
	} else if (stl_semantic == "RenderTargetDimensions") {
	} else if (stl_semantic == "Specular") {
		return ARBP_Specular;
	} else if (stl_semantic == "SpecularPower") {
		return ARBP_SpecPower;
	} else if (stl_semantic == "StandardsGlobal") {
	} else if (stl_semantic == "TextureMatrix") {
	} else if (stl_semantic == "Time") {
		return ARBP_Time;
	} else if (stl_semantic == "UnitsScale") {
	} else if (stl_semantic == "View") {
	} else if (stl_semantic == "ViewInverse") {
		return ARBP_ViewInverse;
	} else if (stl_semantic == "ViewInverseTranspose") {
	} else if (stl_semantic == "ViewProjection") {
	} else if (stl_semantic == "ViewProjectionInverse") {
	} else if (stl_semantic == "ViewProjectionInverseTranspose") {
	} else if (stl_semantic == "World") {
		return ARBP_World;
	} else if (stl_semantic == "WorldInverse") {
		return ARBP_WorldInverse;
	} else if (stl_semantic == "WorldInverseTranspose") {
		return ARBP_WorldInverseTranspose;
	} else if (stl_semantic == "WorldView") {
		return ARBP_WorldView;
	} else if (stl_semantic == "WorldViewInverse") {
	} else if (stl_semantic == "WorldViewInverseTranspose") {
	} else if (stl_semantic == "WorldViewProjection") {
		return ARBP_WorldViewProjection;
	} else if (stl_semantic == "WorldViewProjectionInverse") {
	} else if (stl_semantic == "WorldViewProjectionInverseTranspose") {
	}

	return ARBP_UKNOWN;

	/*
	--	Ambient float4 Ambient color.
	--	Attenuation float3 or float4 Attenuation value, whose value can be one of the following:
	--			Distance uses 4 components.
	--			Spot uses 3 components.
	--			BoxX uses 3 components.
	--			BoxY uses 3 components.
	--	BoundingBoxMax float3 Bounding box maximum for x, y, z.
	--	BoundingBoxMin float3 Bounding box minimum for x, y, z.
	--	BoundingBoxSize float3 Bounding box size for x, y, z.
	--	BoundingCenter vector Bounding box center.
	--	BoundingSphereSize float1 Median bounding sphere radius.
	--	BoundingSphereMin float1 Minimum bounding sphere radius.
	--	BoundingSphereMax float1 Maximum bounding sphere radius.
	--	Diffuse float4 Diffuse color.
	--	ElapsedTime float1 Elapsed time. Useful when LastTime may not be accurate due to looping simulations.
	--	Emissive float4 Emissive color.
	--	EnvironmentNormal texture Environment normal map.
	--	Height 1 component numeric or a texture Height value in a bump map.
	--	Joint float4 Object space to Frame-Joint space
	--	JointWorld float4x4 Joint world matrix.
	--	JointWorldInverse float4x4 Joint inverse world matrix.
	--	JointWorldInverseTranspose float4x4 Joint transposed, inverse world matrix.
	--	JointWorldView float4x4 Joint world-view matrix.
	--	JointWorldViewInverse float4x4 Joint inverse world-view matrix.
	--	JointWorldViewInverseTranspose float4x4 Joint transposed, inverse world-view matrix.
	--	JointWorldViewProjection float4x4 Joint world-view-projection matrix.
	--	JointWorldViewProjectionInverse float4x4 Joint inverse world-view-projection matrix.
	--	JointWorldViewProjectionInverseTranspose float4x4 Joint transposed, inverse world-view-projection matrix.
	--	LastTime float1 Last simulation time.
	--	Normal 3 or 4 component numeric or texture Surface normal vector.
	--	Opacity float1, float2, float3, float4, or texture Object opacity. Can be a single value or a per-component value.
	--	Position float4 XYZW position.
	--	Projection float4x4 Projection matrix.
	--	ProjectionInverse float4x4 Inverse projection matrix.
	--	ProjectionInverseTranspose float4x4 Transposed, inverse projection matrix.
	--	Random numeric Random numbers.
	--	Refraction float1, float2, float3, float4, or texture Refraction value for an environment map. Can be a single value or a per-component value.
	--	RenderColorTarget texture Rendertarget surface.
	--	RenderDepthStencilTarget texture Depth/stencil surface.
	--	RenderTargetClipping 4 components Clipping parameters which include: near distance, far distance, width angle (radians), and height angle(radians).
	--	RenderTargetDimensions 2 component Rendertarget width and height in pixels.
	--	Specular float4 Specular color.
	--	SpecularPower float1 to float4  Specular power exponent. Can be a single value or a per-component value.
	--	StandardsGlobal float1 Standard annotation version number. Default value is 0.80. This may appear only once in a script.
	--	TextureMatrix float4x4 Texture coordinate transform matrix.
	--	Time float Simulation time.
	--	UnitsScale float Model scale value.
	--	View float4x4 View matrix.
	--	ViewInverse float4x4 Inverse view matrix.
	--	ViewInverseTranspose float4x4 Transposed, inverse view matrix.
	--	ViewProjection float4x4 View-projection matrix.
	--	ViewProjectionInverse float4x4 Inverse view-projection matrix.
	--	ViewProjectionInverseTranspose float4x4 Transposed, inverse view-projection matrix.
	--	World float4x4 World matrix.
	--	WorldInverse float4x4 Inverse world matrix.
	--	WorldInverseTranspose float4x4 Transposed, inverse world matrix.
	--	WorldView float4x4 Transposed, inverse world-view matrix.
	--	WorldViewInverse float4x4 Inverse world-view matrix.
	--	WorldViewInverseTranspose float4x4 Transposed, inverse world-view matrix.
	--	WorldViewProjection float4x4 World-view-projection matrix.
	--	WorldViewProjectionInverse float4x4 Inverse world-view-projection matrix.
	--	WorldViewProjectionInverseTranspose float4x4 Transposed, inverse world-view-projection matrix.
	*/
}

BOOL CMaterialObject::InitParamBlockFXShadersHSL(LPD3DXEFFECT effect) {
	if (m_paramBlock)
		DeleteParamMemoryAndZeroIt();

	m_paramCount = GetParameterCount(effect);

	if (m_paramCount < 1)
		return FALSE;

	m_paramBlock = new SLARBparameterBlock[m_paramCount];
	ZeroMemory(m_paramBlock, sizeof(SLARBparameterBlock) * m_paramCount);

	CStringA tempString = "";
	int trace = 0;
	while (1) {
		D3DXHANDLE floatingHandle = effect->GetParameter(NULL, trace);
		if (floatingHandle) {
			D3DXPARAMETER_DESC localDescStruct;
			if (effect->GetParameterDesc(floatingHandle, &localDescStruct) == S_OK) {
				if (localDescStruct.Type == D3DXPT_TEXTURE) {
					//texture
					m_paramBlock[trace].type = ARBP_Texture;

					if (localDescStruct.Annotations > 0) {
						//D3DXHANDLE hAnnot;
						D3DXPARAMETER_DESC AnnotDesc;
						int Width = 0;
						int Height = 0;
						int Depth = 0;
						const char* textureType;
						const char* pstrFunction;
						const char* pstrTarget;
						const char* pstrName;

						textureType = pstrFunction = pstrTarget = pstrName = NULL;
						for (UINT iAnnot = 0; iAnnot < localDescStruct.Annotations; iAnnot++) {
							D3DXHANDLE hAnnot = effect->GetAnnotation(floatingHandle, iAnnot);
							if (effect->GetParameterDesc(hAnnot, &AnnotDesc) == S_OK) {
								if (_strcmpi(AnnotDesc.Name, "name") == 0)
									effect->GetString(hAnnot, &pstrName);
								else if (_stricmp(AnnotDesc.Name, "function") == 0)
									effect->GetString(hAnnot, &pstrFunction);
								else if (_stricmp(AnnotDesc.Name, "target") == 0)
									effect->GetString(hAnnot, &pstrTarget);
								else if (_stricmp(AnnotDesc.Name, "width") == 0)
									effect->GetInt(hAnnot, &Width);
								else if (_stricmp(AnnotDesc.Name, "height") == 0)
									effect->GetInt(hAnnot, &Height);
								else if (_stricmp(AnnotDesc.Name, "depth") == 0)
									effect->GetInt(hAnnot, &Depth);
								else if (_stricmp(AnnotDesc.Name, "type") == 0)
									effect->GetString(hAnnot, &textureType);
							} else {
								//capture error
							}
						}

						if (Depth > 0) {
							//3D texture detected
							//dynamic prceedural texture
							if (pstrName == NULL && pstrFunction != NULL)
								m_paramBlock[trace].type = ARBP_VolumeTexturePrcdrl;
						}
					}
				} else {
					//Check the name first
					tempString = localDescStruct.Name;
					tempString.MakeUpper();

					if (tempString == "TIME" || tempString == "TICKS" || tempString == "TIMER") {
						m_paramBlock[trace].type = ARBP_Time;
					} else if (tempString == "WORLDVIEW" || tempString == "WORLDVIEWMATRIX") {
						m_paramBlock[trace].type = ARBP_WorldView;
					} else if (tempString == "WORLDINVERSETRANSPOSE" || tempString == "WORLDIT") {
						m_paramBlock[trace].type = ARBP_WorldInverseTranspose;
					} else if (tempString == "WORLDVIEWPROJECTION" || tempString == "WVP" || tempString == "WORLDVIEWPROJ") {
						m_paramBlock[trace].type = ARBP_WorldViewProjection;
					} else if (tempString == "WORLD" || tempString == "WORLDMATRIX") {
						m_paramBlock[trace].type = ARBP_World;
					} else if (tempString == "VIEWINVTRANS" || tempString == "VIEWINVERSE" || tempString == "VIEWI" || tempString == "VIEWINV" || tempString == "VIEWINVERSEMATRIX") {
						m_paramBlock[trace].type = ARBP_ViewInverse;
					} else if (tempString == "PROJECTION") {
						m_paramBlock[trace].type = ARBP_Projection;
					} else if (tempString == "DIFFUSE") {
						m_paramBlock[trace].type = ARBP_Diffuse;
					} else if (tempString == "SPECULAR" || tempString == "SPECCOLOR") {
						m_paramBlock[trace].type = ARBP_Specular;
					} else if (tempString == "SPECPOWER") {
						m_paramBlock[trace].type = ARBP_SpecPower;
					} else if (tempString == "WORLDI") {
						m_paramBlock[trace].type = ARBP_WorldInverse;
					} else if (tempString == "LIGHTPOS1") {
						m_paramBlock[trace].type = ARBP_DirLight;
					} else {
						m_paramBlock[trace].type = ARBP_UKNOWN;
						m_paramBlock[trace].svData = 0;
					}

					// Check the semantics to see if we need an update here
					if (m_paramBlock[trace].type == ARBP_UKNOWN) {
						tempString = localDescStruct.Semantic;
						if (tempString.GetLength())
							m_paramBlock[trace].type = (short)GetUsageIDFromSemantic(tempString);
					}

					m_paramBlock[trace].svData = 0;
					m_paramBlock[trace].dataSize = localDescStruct.Bytes;
				}
#ifdef _DEBUG
				if (m_paramBlock[trace].type == ARBP_UKNOWN) {
					TRACE2("Unknown Semantic or Name in Shader (%s / %s)\r\n", localDescStruct.Name, localDescStruct.Semantic);
				}
#endif

				m_paramBlock[trace].data = NULL;
				trace++;
			}
		} else
			break;
	}

	return TRUE;
}

// TODO - Get Rid Of This Performance Issue!
void CMaterialObject::SetFadeInAlpha(TimeMs duration) {
	if (m_transition == 8)
		return;
	m_transitionBlend = m_blendMode;
	m_blendMode = 1;
	m_transition = 8; //fade out
	m_cycleTime = duration;
	m_timeStamp = fTime::TimeMs();
}

void CMaterialObject::SetFadeOut(TimeMs duration) {
	if (m_transition == 4)
		return;
	m_blendMode = 2;
	m_transition = 4; //fade out
	m_cycleTime = duration;
	m_timeStamp = fTime::TimeMs();
}

void CMaterialObject::SetFadeIn(TimeMs duration, BOOL reset) {
	if (m_transition == 5)
		return;

	m_blendMode = 2;
	m_transition = 5;
	m_cycleTime = duration;
	m_timeStamp = fTime::TimeMs();

	if (reset) {
		m_useMaterial.Emissive.r = 0.0f;
		m_useMaterial.Emissive.g = 0.0f;
		m_useMaterial.Emissive.b = 0.0f;
		m_useMaterial.Diffuse.r = 0.0f;
		m_useMaterial.Diffuse.g = 0.0f;
		m_useMaterial.Diffuse.b = 0.0f;
		m_useMaterial.Ambient.r = 0.0f;
		m_useMaterial.Ambient.g = 0.0f;
		m_useMaterial.Ambient.b = 0.0f;
	}
}

void CMaterialObject::SetFadeOutAlpha(TimeMs duration) {
	if (m_transition == 9)
		return;

	m_blendMode = 1;
	m_transition = 9;
	m_cycleTime = duration;
	m_timeStamp = fTime::TimeMs();
}

void CMaterialObject::ApplyDistanceFade() {
	if (!m_fadeByDistance)
		return;

	m_blendMode = 1;
	m_useMaterial.Diffuse.a = m_baseMaterial.Diffuse.a;
	if (m_fadeMaxDist >= 0) {
		//normal distance fade
		if (m_curDistFromCam >= m_fadeMaxDist)
			m_useMaterial.Diffuse.a = 0.0f;
		else if (m_curDistFromCam <= m_fadeVisDist) {
			m_useMaterial.Diffuse.a = m_baseMaterial.Diffuse.a;
			m_blendMode = m_baseBlendMode;
		} else {
			long range = m_fadeMaxDist - m_fadeVisDist;
			m_useMaterial.Diffuse.a = InterpolateFloats(m_baseMaterial.Diffuse.a, 0.0f, range, (int)(m_curDistFromCam - (float)m_fadeVisDist));
		}
	} //end normal distance fade
	else {
		// inverse fade
		if (m_curDistFromCam >= abs(m_fadeMaxDist))
			m_useMaterial.Diffuse.a = m_baseMaterial.Diffuse.a;
		else if (m_curDistFromCam <= abs(m_fadeVisDist)) {
			m_useMaterial.Diffuse.a = 0.0f;
			m_blendMode = m_baseBlendMode;
		} else {
			long range = abs(m_fadeMaxDist) - abs(m_fadeVisDist);
			m_useMaterial.Diffuse.a = InterpolateFloats(0.0f, m_baseMaterial.Diffuse.a, range, abs((long)m_curDistFromCam - abs(m_fadeVisDist)));
		}
	} //end inverse fade
}

void CMaterialObject::ApplyHighlight(bool highlight, D3DCOLOR highlightColor) {
	if (!highlight && !m_bLastHighlighted)
		return; // Staying in an unhighlighted state.

	m_bLastHighlighted = highlight;

	if (m_ReMat) {
		ReRenderStateData* pRSData = m_ReMat->GetRenderStateDataMutable();
		if (pRSData) {
			// Texture highlight
			highlightColor = highlight ? highlightColor : 0;
			pRSData->Highlight = highlight ? TRUE : FALSE;
			pRSData->Constant = highlightColor | 0xff000000;
			pRSData->TextureFactor = highlightColor & 0xff000000;
		}
	}
}

// TODO - Get Rid Of This Performance Issue!
void CMaterialObject::ApplyColorTransition() {
	if (m_transition == 0) //material reconfig
		return;

	if (m_timeStamp == -1)
		m_timeStamp = fTime::TimeMs();

	if (m_transition == 1) { //oscillate emmissive
		TimeMs passedTime = fTime::ElapsedMs(m_timeStamp);
		if (passedTime > m_cycleTime) {
			if (m_switch == TRUE)
				m_switch = FALSE;
			else
				m_switch = TRUE;
			m_timeStamp = fTime::TimeMs();
			passedTime = 0;
		}
		if (m_switch == TRUE) {
			m_useMaterial.Emissive.r = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.g = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.b = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
		} else {
			m_useMaterial.Emissive.r = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.g = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.b = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
		}
	} else if (m_transition == 2) { //oscillate all color
		TimeMs passedTime = fTime::ElapsedMs(m_timeStamp);
		if (passedTime > m_cycleTime) {
			if (m_switch == TRUE)
				m_switch = FALSE;
			else
				m_switch = TRUE;
			m_timeStamp = fTime::TimeMs();
			passedTime = 0;
		}
		if (m_switch == TRUE) {
			m_useMaterial.Emissive.r = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.g = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.b = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.r = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.g = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.b = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Ambient.r = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Ambient.g = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
			m_useMaterial.Ambient.b = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
		} else {
			m_useMaterial.Emissive.r = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.g = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Emissive.b = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.r = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.g = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.b = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Ambient.r = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Ambient.g = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
			m_useMaterial.Ambient.b = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
		}
	} else if (m_transition == 3) { //custom
		TimeMs passedTime = fTime::ElapsedMs(m_timeStamp);
		if (passedTime > m_cycleTime) {
			if (m_switch == TRUE)
				m_switch = FALSE;
			else
				m_switch = TRUE;
			m_timeStamp = fTime::TimeMs();
			passedTime = 0;
		}

		if (m_switch == TRUE) {
			m_useMaterial.Emissive.r = InterpolateFloats(m_baseMaterial.Emissive.r, m_targetModMaterial.Emissive.r, m_cycleTime, passedTime);
			m_useMaterial.Emissive.g = InterpolateFloats(m_baseMaterial.Emissive.g, m_targetModMaterial.Emissive.g, m_cycleTime, passedTime);
			m_useMaterial.Emissive.b = InterpolateFloats(m_baseMaterial.Emissive.b, m_targetModMaterial.Emissive.b, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.r = InterpolateFloats(m_baseMaterial.Diffuse.r, m_targetModMaterial.Diffuse.r, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.g = InterpolateFloats(m_baseMaterial.Diffuse.g, m_targetModMaterial.Diffuse.g, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.b = InterpolateFloats(m_baseMaterial.Diffuse.b, m_targetModMaterial.Diffuse.b, m_cycleTime, passedTime);
			m_useMaterial.Ambient.r = InterpolateFloats(m_baseMaterial.Ambient.r, m_targetModMaterial.Ambient.r, m_cycleTime, passedTime);
			m_useMaterial.Ambient.g = InterpolateFloats(m_baseMaterial.Ambient.g, m_targetModMaterial.Ambient.g, m_cycleTime, passedTime);
			m_useMaterial.Ambient.b = InterpolateFloats(m_baseMaterial.Ambient.b, m_targetModMaterial.Ambient.b, m_cycleTime, passedTime);
		} else {
			m_useMaterial.Emissive.r = InterpolateFloats(m_targetModMaterial.Emissive.r, m_baseMaterial.Emissive.r, m_cycleTime, passedTime);
			m_useMaterial.Emissive.g = InterpolateFloats(m_targetModMaterial.Emissive.g, m_baseMaterial.Emissive.g, m_cycleTime, passedTime);
			m_useMaterial.Emissive.b = InterpolateFloats(m_targetModMaterial.Emissive.b, m_baseMaterial.Emissive.b, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.r = InterpolateFloats(m_targetModMaterial.Diffuse.r, m_baseMaterial.Diffuse.r, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.g = InterpolateFloats(m_targetModMaterial.Diffuse.g, m_baseMaterial.Diffuse.g, m_cycleTime, passedTime);
			m_useMaterial.Diffuse.b = InterpolateFloats(m_targetModMaterial.Diffuse.b, m_baseMaterial.Diffuse.b, m_cycleTime, passedTime);
			m_useMaterial.Ambient.r = InterpolateFloats(m_targetModMaterial.Ambient.r, m_baseMaterial.Ambient.r, m_cycleTime, passedTime);
			m_useMaterial.Ambient.g = InterpolateFloats(m_targetModMaterial.Ambient.g, m_baseMaterial.Ambient.g, m_cycleTime, passedTime);
			m_useMaterial.Ambient.b = InterpolateFloats(m_targetModMaterial.Ambient.b, m_baseMaterial.Ambient.b, m_cycleTime, passedTime);
		}
	} else if (m_transition == 4) { //fade out

		TimeMs passedTime = fTime::ElapsedMs(m_timeStamp);
		if (passedTime > m_cycleTime)
			passedTime = m_cycleTime;
		m_useMaterial.Emissive.r = InterpolateFloats(m_baseMaterial.Emissive.r, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Emissive.g = InterpolateFloats(m_baseMaterial.Emissive.g, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Emissive.b = InterpolateFloats(m_baseMaterial.Emissive.b, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Diffuse.r = InterpolateFloats(m_baseMaterial.Diffuse.r, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Diffuse.g = InterpolateFloats(m_baseMaterial.Diffuse.g, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Diffuse.b = InterpolateFloats(m_baseMaterial.Diffuse.b, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Diffuse.a = InterpolateFloats(m_baseMaterial.Diffuse.a, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Ambient.r = InterpolateFloats(m_baseMaterial.Ambient.r, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Ambient.g = InterpolateFloats(m_baseMaterial.Ambient.g, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Ambient.b = InterpolateFloats(m_baseMaterial.Ambient.b, 0.0f, m_cycleTime, passedTime);
	} else if (m_transition == 5) { //fade in
		TimeMs passedTime = fTime::ElapsedMs(m_timeStamp);
		if (passedTime > m_cycleTime)
			passedTime = m_cycleTime;
		m_useMaterial.Emissive.r = InterpolateFloats(m_useMaterial.Emissive.r, m_baseMaterial.Emissive.r, m_cycleTime, passedTime);
		m_useMaterial.Emissive.g = InterpolateFloats(m_useMaterial.Emissive.g, m_baseMaterial.Emissive.g, m_cycleTime, passedTime);
		m_useMaterial.Emissive.b = InterpolateFloats(m_useMaterial.Emissive.b, m_baseMaterial.Emissive.b, m_cycleTime, passedTime);
		m_useMaterial.Diffuse.r = InterpolateFloats(m_useMaterial.Diffuse.r, m_baseMaterial.Diffuse.r, m_cycleTime, passedTime);
		m_useMaterial.Diffuse.g = InterpolateFloats(m_useMaterial.Diffuse.g, m_baseMaterial.Diffuse.g, m_cycleTime, passedTime);
		m_useMaterial.Diffuse.b = InterpolateFloats(m_useMaterial.Diffuse.b, m_baseMaterial.Diffuse.b, m_cycleTime, passedTime);
		m_useMaterial.Ambient.r = InterpolateFloats(m_useMaterial.Ambient.r, m_baseMaterial.Ambient.r, m_cycleTime, passedTime);
		m_useMaterial.Ambient.g = InterpolateFloats(m_useMaterial.Ambient.g, m_baseMaterial.Ambient.g, m_cycleTime, passedTime);
		m_useMaterial.Ambient.b = InterpolateFloats(m_useMaterial.Ambient.b, m_baseMaterial.Ambient.b, m_cycleTime, passedTime);
		if (passedTime >= m_cycleTime) {
			m_blendMode = -1;
			m_transition = 0;
			Reset();
		}
	} //end fade in
	else if (m_transition == 8) { //fade alpha in
		TimeMs passedTime = fTime::ElapsedMs(m_timeStamp);
		if (passedTime > m_cycleTime)
			passedTime = m_cycleTime;

		m_useMaterial.Emissive.a = 0.0f;
		m_useMaterial.Diffuse.a = InterpolateFloats(0.0f, 1.0f, m_cycleTime, passedTime);
		m_useMaterial.Ambient.a = 0.0f;
		if (passedTime >= m_cycleTime) {
			m_blendMode = m_transitionBlend;
			m_transition = 0;
			Reset();
		}
	} else if (m_transition == 9) { //fade alpha out
		TimeMs passedTime = fTime::ElapsedMs(m_timeStamp);
		if (passedTime > m_cycleTime)
			passedTime = m_cycleTime;

		m_useMaterial.Emissive.a = 1.0f;
		m_useMaterial.Diffuse.a = InterpolateFloats(1.0f, 0.0f, m_cycleTime, passedTime);
		m_useMaterial.Ambient.a = 1.0f;
		if (passedTime >= m_cycleTime) {
			m_blendMode = m_transitionBlend;
			m_transition = 0;
			Reset();
		}
	}

	// set the ReMaterial material props
	if (m_ReMat) {
		// adjust the material properties
		ReRenderStateData* data = m_ReMat->GetRenderStateDataMutable();
		// copy over the material state
		memcpy((void*)&data->Mat, (void*)&m_useMaterial, sizeof(ReMatProps));
		// make sure hash code changes with each frame to assure rendering
		data->MatHash = m_timeStamp;

		// doing alpha distance blending...set the ReMaterial alpha blend props
		if (m_transition >= 8) {
			switch (m_blendMode) {
				case BLEND_DISABLED:
					data->Mode.Zwriteenable = TRUE;
					data->Mode.Alphaenable = FALSE;
					data->Blend.Srcblend = D3DBLEND_DESTCOLOR;
					data->Blend.Dstblend = D3DBLEND_ONE;
					break;

				case BLEND_MULTIPLY_ADD:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_DESTCOLOR;
					data->Blend.Dstblend = D3DBLEND_ONE;
					break;

				case BLEND_ALPHA:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_SRCALPHA;
					data->Blend.Dstblend = D3DBLEND_INVSRCALPHA;
					break;

				case BLEND_ADDITION:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_ONE;
					data->Blend.Dstblend = D3DBLEND_ONE;
					break;

				case BLEND_SQUARE_ADD:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_SRCCOLOR;
					data->Blend.Dstblend = D3DBLEND_DESTCOLOR;
					break;

				case BLEND_INV_MULTIPLY:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_ZERO;
					data->Blend.Dstblend = D3DBLEND_INVSRCCOLOR;
					break;

				case BLEND_INV_MULTIPLY_ALT:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_ZERO;
					data->Blend.Dstblend = D3DBLEND_INVSRCCOLOR;
					break;

				case BLEND_MULTIPLY:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_ZERO;
					data->Blend.Dstblend = D3DBLEND_SRCCOLOR;
					break;

				case BLEND_ALPHA_ALT:
					data->Mode.Zwriteenable = FALSE;
					data->Mode.Alphaenable = TRUE;
					data->Blend.Srcblend = D3DBLEND_SRCALPHA;
					data->Blend.Dstblend = D3DBLEND_INVSRCALPHA;
					break;

				default:
					data->Mode.Zwriteenable = TRUE;
					data->Mode.Alphaenable = FALSE;
					data->Blend.Srcblend = D3DBLEND_DESTCOLOR;
					data->Blend.Dstblend = D3DBLEND_ONE;
					break;
			}

			// renderer sorts by the blend data hash code...
			switch (m_blendMode) {
				// non-transparent renderstate: set MSB to 0
				case BLEND_DISABLED:
					data->BlendHash &= 0x7fffffff;
					break;
					// transparent renderstate: set MSB to 1... higher hash will render-sort first first
				case BLEND_ALPHA:
					data->BlendHash |= 0x80000000;
					break;
					// non-transparent renderstate: set MSB to 0
				default:
					data->BlendHash &= 0x7fffffff;
					break;
			}
		}
	}
}

void CMaterialObject::SetCallbackMethod(int method) {
	if (m_transition != method) {
		m_transition = 3;
		m_timeStamp = fTime::TimeMs();
	}
}

void CMaterialObject::CopyMaterial(D3DMATERIAL9 source, D3DMATERIAL9* dest) const {
	dest->Diffuse.r = source.Diffuse.r;
	dest->Diffuse.g = source.Diffuse.g;
	dest->Diffuse.b = source.Diffuse.b;
	dest->Diffuse.a = source.Diffuse.a;
	dest->Ambient.r = source.Ambient.r;
	dest->Ambient.g = source.Ambient.g;
	dest->Ambient.b = source.Ambient.b;
	dest->Ambient.a = source.Ambient.a;
	dest->Specular.r = source.Specular.r;
	dest->Specular.g = source.Specular.g;
	dest->Specular.b = source.Specular.b;
	dest->Specular.a = source.Specular.a;
	dest->Emissive.r = source.Emissive.r;
	dest->Emissive.g = source.Emissive.g;
	dest->Emissive.b = source.Emissive.b;
	dest->Emissive.a = source.Emissive.a;
	dest->Power = source.Power;
}

void CMaterialObject::Reset() {
	CopyMaterial(m_baseMaterial, &m_useMaterial);
	m_blendMode = m_baseBlendMode;
	m_timeStamp = fTime::TimeMs();
}

float CMaterialObject::InterpolateFloats(
	float StartVec,
	float EndVec,
	int NumOfStages,
	int ThisStage) {
	float propfactor;
	if (NumOfStages <= 0)
		NumOfStages = 1;
	propfactor = (float)ThisStage / NumOfStages;
	float StageVec = StartVec + propfactor * (EndVec - StartVec);

	return StageVec;
}

void CMaterialObject::Serialize(CArchive& ar) {
	BOOL reservedBool = FALSE;

	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar.SetObjectSchema(CArchiveObjectSchemaVersion<CMaterialObject>);

		ar << m_baseMaterial.Diffuse.r
		   << m_baseMaterial.Diffuse.g
		   << m_baseMaterial.Diffuse.b
		   << m_baseMaterial.Diffuse.a
		   << m_baseMaterial.Ambient.r
		   << m_baseMaterial.Ambient.g
		   << m_baseMaterial.Ambient.b
		   << m_baseMaterial.Ambient.a
		   << m_baseMaterial.Specular.r
		   << m_baseMaterial.Specular.g
		   << m_baseMaterial.Specular.b
		   << m_baseMaterial.Specular.a
		   << m_baseMaterial.Emissive.r
		   << m_baseMaterial.Emissive.g
		   << m_baseMaterial.Emissive.b
		   << m_baseMaterial.Emissive.a
		   << m_baseMaterial.Power
		   << m_transition
		   << (int)m_cycleTime
		   << m_textureTileType
		   << m_cullOn
		   << m_forceWireFrame
		   << m_mirrorEnabled
		   << m_fogOverride
		   << m_enableTextureMovement
		   << m_enableSet[0]
		   << m_enableSet[1]
		   << m_enableSet[2]
		   << m_enableSet[3]
		   << m_enableSet[4]
		   << m_enableSet[5]
		   << m_optionalShaderIndex
		   << m_diffuseEffect
		   << m_targetModMaterial.Diffuse.r
		   << m_targetModMaterial.Diffuse.g
		   << m_targetModMaterial.Diffuse.b
		   << m_targetModMaterial.Diffuse.a
		   << m_targetModMaterial.Ambient.r
		   << m_targetModMaterial.Ambient.g
		   << m_targetModMaterial.Ambient.b
		   << m_targetModMaterial.Ambient.a
		   << m_targetModMaterial.Specular.r
		   << m_targetModMaterial.Specular.g
		   << m_targetModMaterial.Specular.b
		   << m_targetModMaterial.Specular.a
		   << m_targetModMaterial.Emissive.r
		   << m_targetModMaterial.Emissive.g
		   << m_targetModMaterial.Emissive.b
		   << m_targetModMaterial.Emissive.a
		   << m_blendMode
		   << m_fadeMaxDist
		   << m_fadeVisDist
		   << m_fadeByDistance
		   << m_fogFilter
		   << m_zBias;

		for (int loop = 0; loop < 8; loop++) {
			ar << m_textureMovement[loop].tu
			   << m_textureMovement[loop].tv;
		}

		for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
			ar << m_colorArg1[loop];
			ar << m_colorArg2[loop];
		}

		ar << m_doAlphaDepthTest
		   << m_paramCount
		   << reservedBool
		   << reservedBool
		   << m_pixelShader
		   << m_vertexShader;

		//parameter extention
		if (m_paramBlock) {
			for (int loop = 0; loop < m_paramCount; loop++) {
				ar << m_paramBlock[loop].type
				   << m_paramBlock[loop].dataSize
				   << m_paramBlock[loop].svData;

				if (m_paramBlock[loop].svData == 1)
					if (m_paramBlock[loop].dataSize > 0)
						if (m_paramBlock[loop].data) {
							for (int bL = 0; bL < m_paramBlock[loop].dataSize; bL++)
								ar << m_paramBlock[loop].data[bL];
						}
			}
		}
		//+VS3
		ar << m_fxBindString;
		// By Jonny support for the naming of materials, call it "default" if unnamed.
		if (m_materialName.Compare("")) {
			ar << m_materialName;
		} else {
			ar << (CStringA) "Default Material";
		}

		for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
			CStringA stff = (CStringA)m_texFileName[loop].c_str();
			ar << m_texNameHash[loop]
			   << stff
			   << m_texBlendMethod[loop];
		}

		for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
			ar << m_texColorKey[loop];
		} // for..loop

		ar << m_sortOrder;
	} else {
		int version = ar.GetObjectSchema();
		int cycleTime;
		ar >> m_baseMaterial.Diffuse.r >> m_baseMaterial.Diffuse.g >> m_baseMaterial.Diffuse.b >> m_baseMaterial.Diffuse.a >> m_baseMaterial.Ambient.r >> m_baseMaterial.Ambient.g >> m_baseMaterial.Ambient.b >> m_baseMaterial.Ambient.a >> m_baseMaterial.Specular.r >> m_baseMaterial.Specular.g >> m_baseMaterial.Specular.b >> m_baseMaterial.Specular.a >> m_baseMaterial.Emissive.r >> m_baseMaterial.Emissive.g >> m_baseMaterial.Emissive.b >> m_baseMaterial.Emissive.a >> m_baseMaterial.Power >> m_transition >> cycleTime >> m_textureTileType >> m_cullOn >> m_forceWireFrame >> m_mirrorEnabled >> m_fogOverride >> m_enableTextureMovement >> m_enableSet[0] >> m_enableSet[1] >> m_enableSet[2] >> m_enableSet[3] >> m_enableSet[4] >> m_enableSet[5] >> m_optionalShaderIndex >> m_diffuseEffect >> m_targetModMaterial.Diffuse.r >> m_targetModMaterial.Diffuse.g >> m_targetModMaterial.Diffuse.b >> m_targetModMaterial.Diffuse.a >> m_targetModMaterial.Ambient.r >> m_targetModMaterial.Ambient.g >> m_targetModMaterial.Ambient.b >> m_targetModMaterial.Ambient.a >> m_targetModMaterial.Specular.r >> m_targetModMaterial.Specular.g >> m_targetModMaterial.Specular.b >> m_targetModMaterial.Specular.a >> m_targetModMaterial.Emissive.r >> m_targetModMaterial.Emissive.g >> m_targetModMaterial.Emissive.b >> m_targetModMaterial.Emissive.a >> m_blendMode >> m_fadeMaxDist >> m_fadeVisDist >> m_fadeByDistance >> m_fogFilter >> m_zBias;
		m_cycleTime = cycleTime;
		for (int loop = 0; loop < 8; loop++) {
			ar >> m_textureMovement[loop].tu >> m_textureMovement[loop].tv;
		}

		m_colorArg1[0] = 2;
		m_colorArg1[1] = 2;
		m_colorArg1[2] = 2;
		m_colorArg1[3] = 2;
		m_colorArg1[4] = 2;
		m_colorArg1[5] = 2;
		m_colorArg1[6] = 2;
		m_colorArg1[7] = 2;
		m_colorArg1[8] = 2;
		m_colorArg1[9] = 2;

		m_colorArg2[0] = 0;
		m_colorArg2[1] = 1;
		m_colorArg2[2] = 1;
		m_colorArg2[3] = 1;
		m_colorArg2[4] = 1;
		m_colorArg2[5] = 1;
		m_colorArg2[6] = 1;
		m_colorArg2[7] = 1;
		m_colorArg2[8] = 1;
		m_colorArg2[9] = 1;

		m_pixelShader = -1;
		m_vertexShader = -1;
		m_paramCount = 0;
		m_paramBlock = NULL;

		if (version >= 2) {
			for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
				ar >> m_colorArg1[loop];
				ar >> m_colorArg2[loop];
			}
		}

		if (version >= 3) {
			ar >> m_doAlphaDepthTest >> m_paramCount >> reservedBool >> reservedBool >> m_pixelShader >> m_vertexShader;
		}
		if (version >= 4) {
			if (m_paramCount > 0) {
				m_paramBlock = new SLARBparameterBlock[m_paramCount];
				ZeroMemory(m_paramBlock, sizeof(SLARBparameterBlock) * m_paramCount);

				for (int loop = 0; loop < m_paramCount; loop++) {
					ar >> m_paramBlock[loop].type >> m_paramBlock[loop].dataSize >> m_paramBlock[loop].svData;

					if (m_paramBlock[loop].svData == 1)
						if (m_paramBlock[loop].dataSize > 0) {
							m_paramBlock[loop].data = new byte[m_paramBlock[loop].dataSize];
							for (int bL = 0; bL < m_paramBlock[loop].dataSize; bL++)
								ar >> m_paramBlock[loop].data[bL];
						} else {
							m_paramBlock[loop].dataSize = 0;
							m_paramBlock[loop].data = NULL;
						}
				}
			}

			ar >> m_fxBindString;
		}
		if (version >= 5) {
			ar >> m_materialName;
		}

		CopyMaterial(m_baseMaterial, &m_useMaterial);
		m_switch = FALSE;
		m_timeStamp = -1;
		m_transitionBlend = -2;
		m_baseBlendMode = m_blendMode;
		m_curDistFromCam = 0.0f;
		m_enableSet[6] = FALSE;
		m_enableSet[7] = FALSE;
		m_shadowObject = 0;

		if (version >= 6) {
			for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
				CStringA stff;
				ar >> m_texNameHash[loop] >> stff >> m_texBlendMethod[loop];
				m_texFileName[loop] = (const char*)stff;

				m_texNameHashOriginal[loop] = m_texNameHash[loop];
				m_texFileNameOriginal[loop] = m_texFileName[loop];
			}
		}

		if (version >= 7) {
			for (int loop = 0; loop < NUM_CMATERIAL_TEXTURES; loop++) {
				ar >> m_texColorKey[loop];
			}
		}

		if (version >= 8) {
			ar >> m_sortOrder;
		} // if..version > 7
	}
}

void SLARBparameterBlock::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void CMaterialObject::GetMemSizeTexFileNames(IMemSizeGadget* pMemSizeGadget) const {
	for (int i = 0; i < NUM_CMATERIAL_TEXTURES; ++i) {
		pMemSizeGadget->AddObject(m_texFileName[i]);
		pMemSizeGadget->AddObject(m_texFileNameOriginal[i]);
	}
}

void CMaterialObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_customTextureProvider);
		pMemSizeGadget->AddObject(m_fxBindString);
		pMemSizeGadget->AddObject(m_materialName);
		pMemSizeGadget->AddObject(m_myCompressor);
		pMemSizeGadget->AddObject(m_paramBlock);
		pMemSizeGadget->AddObject(m_ReMat);

		// Internal getMemSize function
		GetMemSizeTexFileNames(pMemSizeGadget);
	}
}

IMPLEMENT_SERIAL_SCHEMA(CMaterialObject, CObject)

} // namespace KEP
