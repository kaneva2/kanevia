///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#define D3D_OVERLOADS
#include "resource.h"
#include <d3d9.h>
#include <afxtempl.h>

#include "CGlobalMaterialShaderLib.h"

namespace KEP {

CGlobalMaterialShader::CGlobalMaterialShader() {
	m_requiredUvCount = 0;
	m_chromeUvSet = 0;
	m_enableChroming = 0;

	for (int i = 0; i < 10; i++)
		m_textureIndexArray[i] = -1;

	ZeroMemory(m_blendArray, sizeof(int) * 10); //

	m_useOverrideMaterial = 0;
	ZeroMemory(&m_overrideMaterial, sizeof(D3DMATERIAL9));
}

void CGlobalMaterialShader::ScaleForRuntimeCompatibility(BOOL& bumpSupport) {
	if (!bumpSupport) //reconfig
		for (int i = 0; i < 10; i++) {
			//m_textureIndexArray[i] = -1;

			if (m_blendArray[i] == 7) { //incompatible blend found
				m_blendArray[i] = m_blendArray[i + 1];
				m_textureIndexArray[i] = m_textureIndexArray[i + 1];

				m_textureStringArray[i] = m_textureStringArray[i + 1];

				if (m_chromeUvSet > 0)
					m_chromeUvSet--;

				m_textureIndexArray[i + 1] = 1;
				m_blendArray[i + 1] = -1;

				return;
			}
		}
}

void CGlobalMaterialShaderList::ScaleForRuntimeCompatibility(BOOL& bumpSupport) {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CGlobalMaterialShader* spnPtr = (CGlobalMaterialShader*)GetNext(posLoc);

		spnPtr->ScaleForRuntimeCompatibility(bumpSupport);
	}
}

void CGlobalMaterialShaderList::SafeDelete() {
	for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
		CGlobalMaterialShader* spnPtr = (CGlobalMaterialShader*)GetNext(posLoc);
		delete spnPtr;
		spnPtr = 0;
	}
	RemoveAll();
}

CGlobalMaterialShader* CGlobalMaterialShaderList::GetByIndex(int index) {
	if (index < 0)
		return NULL;

	POSITION posLoc = FindIndex(index);
	if (!posLoc)
		return NULL;
	CGlobalMaterialShader* spnPtr = (CGlobalMaterialShader*)GetAt(posLoc);
	return spnPtr;
}

void CGlobalMaterialShader::Serialize(CArchive& ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) {
		ar << m_requiredUvCount
		   << m_chromeUvSet
		   << m_enableChroming
		   << m_useOverrideMaterial
		   << m_shaderName
		   << m_overrideMaterial.Diffuse.r
		   << m_overrideMaterial.Diffuse.g
		   << m_overrideMaterial.Diffuse.b
		   << m_overrideMaterial.Diffuse.a
		   << m_overrideMaterial.Ambient.r
		   << m_overrideMaterial.Ambient.g
		   << m_overrideMaterial.Ambient.b
		   << m_overrideMaterial.Ambient.a
		   << m_overrideMaterial.Specular.r
		   << m_overrideMaterial.Specular.g
		   << m_overrideMaterial.Specular.b
		   << m_overrideMaterial.Specular.a
		   << m_overrideMaterial.Emissive.r
		   << m_overrideMaterial.Emissive.g
		   << m_overrideMaterial.Emissive.b
		   << m_overrideMaterial.Emissive.a;

		for (int i = 0; i < 10; i++) {
			ar << m_textureIndexArray[i] << m_textureStringArray[i] << m_blendArray[i];
		}

	} else {
		ar >> m_requiredUvCount >> m_chromeUvSet >> m_enableChroming >> m_useOverrideMaterial >> m_shaderName >> m_overrideMaterial.Diffuse.r >> m_overrideMaterial.Diffuse.g >> m_overrideMaterial.Diffuse.b >> m_overrideMaterial.Diffuse.a >> m_overrideMaterial.Ambient.r >> m_overrideMaterial.Ambient.g >> m_overrideMaterial.Ambient.b >> m_overrideMaterial.Ambient.a >> m_overrideMaterial.Specular.r >> m_overrideMaterial.Specular.g >> m_overrideMaterial.Specular.b >> m_overrideMaterial.Specular.a >> m_overrideMaterial.Emissive.r >> m_overrideMaterial.Emissive.g >> m_overrideMaterial.Emissive.b >> m_overrideMaterial.Emissive.a;

		for (int i = 0; i < 10; i++) {
			ar >> m_textureIndexArray[i] >> m_textureStringArray[i] >> m_blendArray[i];
		}
	}
}

IMPLEMENT_SERIAL(CGlobalMaterialShader, CObject, 0)
IMPLEMENT_SERIAL(CGlobalMaterialShaderList, CKEPObList, 0)

BEGIN_GETSET_IMPL(CGlobalMaterialShader, IDS_GLOBALMATERIALSHADER_NAME)
GETSET_RANGE(m_requiredUvCount, gsInt, GS_FLAGS_DEFAULT, 0, 0, GLOBALMATERIALSHADER, REQUIREDUVCOUNT, INT_MIN, INT_MAX)
GETSET_RANGE(m_chromeUvSet, gsInt, GS_FLAGS_DEFAULT, 0, 0, GLOBALMATERIALSHADER, CHROMEUVSET, INT_MIN, INT_MAX)
GETSET_RANGE(m_enableChroming, gsInt, GS_FLAGS_DEFAULT, 0, 0, GLOBALMATERIALSHADER, ENABLECHROMING, INT_MIN, INT_MAX)
GETSET(m_useOverrideMaterial, gsBOOL, GS_FLAGS_DEFAULT, 0, 0, GLOBALMATERIALSHADER, USEOVERRIDEMATERIAL)
GETSET_MAX(m_shaderName, gsCString, GS_FLAGS_DEFAULT, 0, 0, GLOBALMATERIALSHADER, SHADERNAME, STRLEN_MAX)
GETSET_D3DMATERIAL9(m_overrideMaterial, GS_FLAGS_DEFAULT, 0, 0, GLOBALMATERIALSHADER, OVERRIDEMATERIAL)
GETSET_RANGE(m_textureIndexArray, gsInt, GS_FLAGS_DEFAULT, 0, 0, GLOBALMATERIALSHADER, TEXTUREINDEXARRAY, INT_MIN, INT_MAX)
END_GETSET_IMPL

} // namespace KEP