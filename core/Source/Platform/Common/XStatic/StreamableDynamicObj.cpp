///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StreamableDynamicObj.h"
#include "CCollisionClass.h"
#include "CWldObject.h"
#include "CReferenceLibrary.h"
#include "CHierarchyMesh.h"
#include "CSkeletonObject.h"
#include "RuntimeSkeleton.h"
#include "CRTLightClass.h"
#ifdef _ClientOutput
#include "ReMeshStream.h"
#include "ReMeshCreate.h"
#include "ReD3DX9StaticMesh.h"
#include "IClientEngine.h"
#include "DownloadManager.h"
#include "FlashMesh.h"
#include "EquippableSystem.h"
#include "CArmedInventoryClass.h"
#include "SkeletonConfiguration.h"
#include "CDeformableMesh.h"
#include "ResourceManagers\ReMeshCreate.h"
#include "CBoneClass.h"
#endif
#include "ReD3DX9.h"
#include "Event/Base/IEvent.h"
#include <ServerSerialize.h>
#include "resource.h"
#include "MaterialPersistence.h"
#include "ReMesh.h"
#include "BoundBox.h"
#include "DownloadManager.h"
#include "IClientEngine.h"
#include "UgcConstants.h"
#include "ReMeshCreate.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/Units.h"
#include "KEPPhysics/PhysicsFactory.h"
#include "KEPPhysics/CollisionShape.h"
#include "PhysicsUserData.h"
#include "SkeletonManager.h"
#include "common\include\IMemSizeGadget.h"
#include "common\include\MemSizeSpecializations.h"
#include "CTriggerClass.h"
#include "LogHelper.h"

static LogInstance("Instance");

namespace KEP {

StreamableDynamicObjectLight::StreamableDynamicObjectLight() {
	m_lightColor.r = 255;
	m_lightColor.g = 255;
	m_lightColor.b = 255;
	m_range = 100.0f;
	m_attenuation1 = 1.0f;
	m_attenuation2 = 2.0f;
	m_attenuation3 = 3.0f;
}

/*!
 * \brief
 * Constructor.
 *
 * \param r
 * red component of the light color int(0-255)
 *
 * \param g
 * green component of the light color int(0-255)
 *
 * \param b
 * blue component of the light color int(0-255)
 *
 * \param range
 * the distance at which the light is first visible a float value
 *
 * \param att1
 * attenuation param 1.  I honestly would have to look up what this does.
 *
 * \param att2
 * attenuation param 2.  I honestly would have to look up what this does.
 *
 * \param att3
 * attenuation param 3.  I honestly would have to look up what this does.
 *
 * \throws <exception class>
 * Description of criteria for throwing this exception.
 *
 * Write detailed description for StreamableDynamicObjectLight here.
 *
 * \remarks
 * Write remarks for StreamableDynamicObjectLight here.
 *
 * \see
 * Separate items with the '|' character.
 */
StreamableDynamicObjectLight::StreamableDynamicObjectLight(int r, int g, int b, float range, float att1, float att2, float att3) {
	m_lightColor.r = r;
	m_lightColor.g = g;
	m_lightColor.b = b;
	m_range = range;
	m_attenuation1 = att1;
	m_attenuation2 = att2;
	m_attenuation3 = att2;
}

StreamableDynamicObjectLight::~StreamableDynamicObjectLight() {
}

StreamableDynamicObjectVisual::StreamableDynamicObjectVisual() :
		m_LODCollisionSource(-1), // No collision by default
		m_altCollisionSource(NULL),
		m_canCollide(TRUE),
		m_relativePosition(0.0f, 0.0f, 0.0f),
		m_relativeOrientation(0.0f, 0.0f, 0.0f),
		m_animModule(nullptr),
		m_material(nullptr),
		m_name("visual name"),
		m_index(0xffffffff),
		m_usePersistentMeshPool(false) {
}

StreamableDynamicObjectVisual::~StreamableDynamicObjectVisual() {
	if (m_animModule) {
		delete m_animModule;
		m_animModule = NULL;
	}
	delete m_material;
}

UINT64 StreamableDynamicObjectVisual::getLOD(unsigned int lodLevel) const {
	if (lodLevel < m_lodHashes.size()) {
		return m_lodHashes[lodLevel].first;
	} else {
		return 0;
	}
}

float StreamableDynamicObjectVisual::getLODDistance(unsigned int lodLevel) const {
	if (lodLevel < m_lodHashes.size()) {
		return m_lodHashes[lodLevel].second;
	} else {
		return 0.0f;
	}
}

UINT StreamableDynamicObjectVisual::getDistanceLOD(FLOAT distance) const {
	for (INT i = m_lodHashes.size() - 1; i >= 0; i--) {
		if (distance >= m_lodHashes[i].second) {
			return i;
		}
	}
	return 0;
}

bool StreamableDynamicObjectVisual::replaceLOD(unsigned int lodLevel, UINT64 hash, float distance) {
	assert(lodLevel < m_lodHashes.size());
	if (lodLevel < m_lodHashes.size()) {
		m_lodHashes[lodLevel] = std::make_pair(hash, distance);
		return true;
	} else {
		return false;
	}
}

void StreamableDynamicObjectVisual::addLOD(UINT64 hash, float distance) {
	m_lodHashes.push_back(std::make_pair(hash, distance));
}

void StreamableDynamicObjectVisual::removeLOD(int index) {
	m_lodHashes.erase(m_lodHashes.begin() + index);
}

void StreamableDynamicObjectVisual::setMaterial(CMaterialObject* mat, const char* subDir /*=DYNAMIC_TEXTURE_SUBDIR*/) {
	// Destroy Existing Material
	if (m_material) {
		delete m_material;
		m_material = NULL;
	}

#ifdef _ClientOutput
	// Valid New Material ?
	if (!mat)
		return;

	// Copy New Material
	mat->Clone(&m_material);
	MeshRM::Instance()->CreateReMaterial(m_material, subDir ? subDir : "");
#else
	ASSERT(0);
#endif
}

void StreamableDynamicObjectVisual::setRelativePosition(float x, float y, float z) {
	m_relativePosition.x = x;
	m_relativePosition.y = y;
	m_relativePosition.z = z;
}

void StreamableDynamicObjectVisual::setRelativeOrientation(float x, float y, float z) {
	m_relativeOrientation.x = x;
	m_relativeOrientation.y = y;
	m_relativeOrientation.z = z;
}

void StreamableDynamicObjectVisual::setVertexAnim(CVertexAnimationModule* anim) {
	if (m_animModule) {
		delete m_animModule;
		m_animModule = NULL;
	}
	if (anim) {
		anim->Clone(&m_animModule);
	} else {
		m_animModule = NULL;
	}
}

/*!
 * \brief
 * Provides direct access to the remesh lod chain for this visual
 *
 * \param lod
 * an int for the lod level of the mesh requested.  0 is the highest poly count.
 *
 * \returns
 * the requested remesh
 *
 * \throws <exception class>
 * Description of criteria for throwing this exception.
 *
 * Write detailed description for getReMesh here.
 *
 * \remarks
 * Write remarks for getReMesh here.
 *
 * \see
 * ReMesh | StreamableDynamicObjectSystem
 */
ReMesh* StreamableDynamicObjectVisual::getReMesh(int lod) {
#ifdef _ClientOutput
	// passive initialization, no point in allocating or finding meshes if client never asks for
	// them.  Only a small percentage of dyn objs will ever be in use at once.
	if (m_reMeshDirectAccess.size() != m_lodHashes.size()) {
		m_reMeshDirectAccess.clear(); // mem managed by Charles's remesh pools
		for (unsigned int i = 0; i < m_lodHashes.size(); i++)
			m_reMeshDirectAccess.push_back(NULL);
	}

	if (lod >= (int)m_lodHashes.size())
		return NULL;

	if (m_reMeshDirectAccess[lod] == nullptr)
		m_reMeshDirectAccess[lod] = MeshRM::Instance()->FindMesh(m_lodHashes[lod].first, MeshRM::RMP_ID_DYNAMIC);

	if (m_reMeshDirectAccess[lod] == nullptr && m_usePersistentMeshPool) {
		m_reMeshDirectAccess[lod] = MeshRM::Instance()->FindMesh(m_lodHashes[lod].first, MeshRM::RMP_ID_PERSISTENT);
	}

	return m_reMeshDirectAccess[lod];
#else
	return 0;
#endif
}

StreamableDynamicObject::StreamableDynamicObject() :
		m_name("Default"),
		m_pLegacyCollision(nullptr),
		m_hasLegacyCollision(false),
		m_LegacyGlid(GLID_INVALID),
		m_triggers(nullptr),
		m_light(nullptr),
		m_attachable(false),
		m_particleEffectId(-1),
		m_particleOffset(0.0f, 0.0f, 0.0f),
		m_lightPosition(0.0f, 0.0f, 0.0f),
		m_interactive(false),
		m_skeletonGLID(GLID_INVALID),
		m_skeletonAdditionalScale(1.0f, 1.0f, 1.0f),
		m_linkedMovObjNetTraceId(0) {
	m_boundBox.min.x = 0.0f;
	m_boundBox.min.y = 0.0f;
	m_boundBox.min.z = 0.0f;
	m_boundBox.max.x = 0.0f;
	m_boundBox.max.y = 0.0f;
	m_boundBox.max.z = 0.0f;
}

StreamableDynamicObject::~StreamableDynamicObject() {
	if (m_pLegacyCollision) {
		m_pLegacyCollision->SafeDelete();
		delete m_pLegacyCollision;
		m_pLegacyCollision = NULL;
	}

	while (m_visuals.size())
		DelVisual(0);

	if (m_light) {
		delete m_light;
		m_light = NULL;
	}

	if (m_triggers) {
		delete m_triggers;
		m_triggers = NULL;
	}

	if (m_skeletonGLID != GLID_INVALID) {
		SkeletonRM::Instance()->DestroyRuntimeSkeleton(m_skeletonGLID);
	}
}

void StreamableDynamicObject::populateObjectHasCollision() {
	m_objectHasCollision = (m_pLegacyCollision != NULL);
}

void StreamableDynamicObject::AddVisual(StreamableDynamicObjectVisual* vis, bool customizable, bool mediaSupported) {
	if (!vis)
		return;

	// Make sure all visible visuals are first so we have a contiguous list of visible visuals.
	size_t idxInsert = m_visuals.size();
	if (vis->getIsVisible()) {
		while (idxInsert > 0 && !m_visuals[idxInsert - 1]->getIsVisible())
			--idxInsert;
	}
	m_visuals.insert(m_visuals.begin() + idxInsert, vis);
	for (size_t i = idxInsert; i < m_visuals.size(); ++i)
		m_visuals[i]->setIndex(i);

	m_visualCustomizable.insert(m_visualCustomizable.begin() + idxInsert, customizable || !IS_KANEVA_GLID(m_LegacyGlid));
	m_visualMediaSupported.insert(m_visualMediaSupported.begin() + idxInsert, mediaSupported);
	populateMediaSupported();
}

bool StreamableDynamicObject::DelVisual(int index) {
	if ((index < 0) || (index >= (int)m_visuals.size()))
		return false;
	delete m_visuals[index];
	m_visuals.erase(m_visuals.begin() + index);
	m_visualCustomizable.erase(m_visualCustomizable.begin() + index);
	m_visualMediaSupported.erase(m_visualMediaSupported.begin() + index);
	populateMediaSupported();
	return true;
}

bool StreamableDynamicObject::SetVisualMediaSupported(unsigned int visNum, bool mediaSupported) {
	if (visNum >= m_visuals.size()) {
		LogError("Visual Out Of Range - visual=" << visNum << " visuals=" << m_visuals.size());
		return false;
	}
	m_visualMediaSupported[visNum] = mediaSupported;
	populateMediaSupported();
	return true;
}

bool StreamableDynamicObject::VisualMediaSupported(unsigned int visNum) const {
	return (visNum < m_visuals.size()) ? m_visualMediaSupported[visNum] : false;
}

void StreamableDynamicObject::populateMediaSupported() {
	m_canObjectPlayFlash = false;
	for (size_t i = 0; i < m_visuals.size(); i++) {
		if (VisualMediaSupported(i)) {
			m_canObjectPlayFlash = true;
			return;
		}
	}
}

bool StreamableDynamicObject::VisualCustomizable(unsigned int visNum) const {
	return (visNum < m_visuals.size()) ? (!IS_KANEVA_GLID(this->m_LegacyGlid) ? true : m_visualCustomizable[visNum]) : false;
}

bool StreamableDynamicObject::SetVisualCustomizable(unsigned int visNum, bool customizable) {
	if (visNum >= m_visuals.size())
		return false;
	m_visualCustomizable[visNum] = !IS_KANEVA_GLID(this->m_LegacyGlid) || customizable;
	return true;
}

float MeshCollisionError(const CCollisionObj& collObj, ReMesh* pMesh, Vector3f* pvErrorDirection, size_t* piFarthestTriIndex) {
	ReMeshData* pMeshData;
	pMesh->Access(&pMeshData);
	assert(pMeshData->PrimType == ReMeshPrimType::TRISTRIP || pMeshData->PrimType == ReMeshPrimType::TRILIST);
	size_t iStep = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? 1 : 3;
	size_t nIndices = pMeshData->NumI;
	size_t iEnd = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? std::max<size_t>(2, nIndices) - 2 : nIndices - nIndices % 3;
	Vector3f* pFirstPoint = pMeshData->pP;
	uint16_t* pFirstIndex = pMeshData->pI;

	size_t iFarthestTriIndex = SIZE_MAX;
	float fMaxTriangleError = 0;
	Vector3f vDirectionOfMaxTriangleError(0, 0, 0);
	for (size_t i = 0; i < iEnd; i += iStep) {
		Vector3f vDirectionOfThisTriangleError;
		size_t iTriIndex = SIZE_MAX;
		float fThisTriangleError = collObj.ClosestTriangle(pFirstPoint[pFirstIndex[i]], pFirstPoint[pFirstIndex[i + 1]], pFirstPoint[pFirstIndex[i + 2]], &vDirectionOfMaxTriangleError, nullptr, &iTriIndex);
		if (fThisTriangleError > fMaxTriangleError) {
			fMaxTriangleError = fThisTriangleError;
			vDirectionOfMaxTriangleError = vDirectionOfThisTriangleError;
			iFarthestTriIndex = iTriIndex;
		}
	}
	if (pvErrorDirection)
		*pvErrorDirection = vDirectionOfMaxTriangleError;
	if (piFarthestTriIndex)
		*piFarthestTriIndex = iFarthestTriIndex;
	return fMaxTriangleError;
}

float VisualCollisionError(const CCollisionObj& collObj, StreamableDynamicObjectVisual* pVis, Vector3f* pvErrorDirection, size_t* piFarthestTriIndex) {
	size_t iFarthestTriIndex = SIZE_MAX;
	float fMinLODError = HUGE_VAL;
	Vector3f vMinErrorDirection(0, 0, 0);
	for (size_t i = 0; i < pVis->getLodCount(); ++i) {
		ReMesh* pMesh = pVis->getReMesh(i);
		if (!pMesh)
			continue;
		Vector3f vErrorDirection(0, 0, 0);
		size_t iTriIndex = SIZE_MAX;
		float fError = MeshCollisionError(collObj, pMesh, &vErrorDirection, &iTriIndex);
		if (fError < fMinLODError) {
			fMinLODError = fError;
			vMinErrorDirection = vErrorDirection;
			iFarthestTriIndex = iTriIndex;
		}
	}

	if (pvErrorDirection)
		*pvErrorDirection = vMinErrorDirection;
	if (piFarthestTriIndex)
		*piFarthestTriIndex = iFarthestTriIndex;
	return fMinLODError;
}

int StreamableDynamicObject::updateVisualCollisionFlags() {
	// UGC objects with m_pLegacyCollision == NULL have been converted and should have their collision flags set properly.
	// Kaneva objects with m_pLegacyCollision == NULL need to have their collision disabled.
	if (!m_pLegacyCollision) {
		if (IS_KANEVA_GLID(m_LegacyGlid)) {
			for (size_t iVisual = 0; iVisual < m_visuals.size(); ++iVisual) {
				StreamableDynamicObjectVisual* pVis = m_visuals[iVisual];
				pVis->setCanCollide(false);
			}
		}
		return 1;
	}

	// Collision bug in old system: If collision geometry was 2-dimensional, no collision occurred.
	// To maintain compatibility in this case, we will delete the collision geometry and mark
	// all visuals as not collidable.
	if (m_pLegacyCollision) {
		const BoundBox& bb = m_pLegacyCollision->GetBoundingBox();
		static const float kfTolerance = 1e-4;
		if (bb.getSizeX() < kfTolerance || bb.getSizeY() < kfTolerance || bb.getSizeZ() < kfTolerance) {
			m_pLegacyCollision->SafeDelete();
			delete m_pLegacyCollision;
			m_pLegacyCollision = NULL;

			for (StreamableDynamicObjectVisual* pVis : m_visuals)
				pVis->setCanCollide(false);
		}
	}

	if (!IS_KANEVA_GLID(m_LegacyGlid))
		return 1; // Assume all "recent" objects have collision flags set properly.

	const bool bCreateCollisionVisualWhenNeeded = true;
	const bool bOptimizeCollisionObjects = true;
	std::vector<Vector3f> aCollisionTrianglesVerts;
	std::vector<bool> abCollisionTrianglesMatched;

	if (m_pLegacyCollision) {
		aCollisionTrianglesVerts = m_pLegacyCollision->GetTriangles();
		abCollisionTrianglesMatched.resize(m_pLegacyCollision->GetTriangleCount(), false);
		if (bOptimizeCollisionObjects) {
			//int iValidate1 = m_pLegacyCollision->ValidateBoxes();
			m_pLegacyCollision->RebuildBoxes();
			//int iValidate2 = m_pLegacyCollision->ValidateBoxes();
			//LogInfo("Validate results for " << m_name << " (glid=" << m_LegacyGlid << "): Old: " << iValidate1 << " New: " << iValidate2);
		}
	}

	size_t nOldCollisionTriangles = aCollisionTrianglesVerts.size() / 3;

#if defined(DEBUG)
	static bool bDumpTriangles = false;
	if (bDumpTriangles) {
		if (m_pLegacyCollision)
			m_pLegacyCollision->DumpTrianglesToLog();
		size_t iVisual = 0;
		for (StreamableDynamicObjectVisual* pVis : m_visuals) {
			LogInfo("Visual " << iVisual << " \"" << pVis->getName() << "\"");
			++iVisual;
			pVis->getReMesh(0)->DumpTrianglesToLog();
		}
	}
#endif

	bool bCollisionFlagChanged = false;
	size_t nCollisionVisuals = 0;
	size_t nNewCollisionTriangles = 0;
	size_t nMeshesNotInCollision = 0;
	size_t nMeshesNotInCollisionAndShouldBe = 0;
	size_t nTotalTriangles = 0;
	for (size_t iVisual = 0; iVisual < m_visuals.size(); ++iVisual) {
		StreamableDynamicObjectVisual* pVis = m_visuals[iVisual];
		bool bNewCollisionFlag = false;
		bool bOldCollisionFlag = pVis->getCanCollide();
		int nLODs = pVis->getLodCount();
		for (int i = 0; i < nLODs; ++i) {
			ReMesh* pCollisionMesh = pVis->getReMesh(i);
			if (pCollisionMesh) {
				if (i == 0) {
					ReMeshData* pMeshData;
					pCollisionMesh->Access(&pMeshData);
					assert(pMeshData->PrimType == ReMeshPrimType::TRISTRIP || pMeshData->PrimType == ReMeshPrimType::TRILIST);
					nTotalTriangles += pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? std::max<size_t>(2, pMeshData->NumI) - 2 : pMeshData->NumI / 3;
				}

				std::vector<bool> abMeshCollisionMatches;
				if (isMeshInCollision(pCollisionMesh, &abMeshCollisionMatches)) {
					for (size_t j = 0; j < abMeshCollisionMatches.size(); ++j)
						abCollisionTrianglesMatched[j] = abCollisionTrianglesMatched[j] | abMeshCollisionMatches[j];
					bNewCollisionFlag = true;

					ReMeshData* pMeshData;
					pCollisionMesh->Access(&pMeshData);
					assert(pMeshData->PrimType == ReMeshPrimType::TRISTRIP || pMeshData->PrimType == ReMeshPrimType::TRILIST);
					size_t nMeshTriangles = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? std::max<size_t>(2, pMeshData->NumI) - 2 : pMeshData->NumI / 3;
					nNewCollisionTriangles += nMeshTriangles;
					break;
				} else {
					nMeshesNotInCollision += 1;
					nMeshesNotInCollisionAndShouldBe += bOldCollisionFlag;
				}
			}
		}

#if defined(DEBUG)
		if (0 && !bNewCollisionFlag && bOldCollisionFlag && m_pLegacyCollision) {
			size_t iTriIndex = SIZE_MAX;
			Vector3f vErrorDirection(0, 0, 0);
			float fError = VisualCollisionError(*m_pLegacyCollision, pVis, &vErrorDirection, &iTriIndex);
			LogWarn("object " << m_name << " (glid=" << m_LegacyGlid << ") Visual " << iVisual << " \"" << m_name << "\" has error " << fError << " in direction " << vErrorDirection.x << "," << vErrorDirection.y << "," << vErrorDirection.z << " Farthest triangle = " << iTriIndex);
			std::vector<Vector3f> aAdjustedCollision = aCollisionTrianglesVerts;
			for (Vector3f& pt : aAdjustedCollision)
				pt += vErrorDirection;
			Vector3f vNewErrorDirection;
			float fNewError = VisualCollisionError(*m_pLegacyCollision, pVis, &vNewErrorDirection, &iTriIndex);
			LogInfo("Error after adjustment: " << fNewError << " in direction " << vNewErrorDirection.x << "," << vNewErrorDirection.y << "," << vNewErrorDirection.z);
		}
#endif

		nCollisionVisuals += bNewCollisionFlag;
		if (bNewCollisionFlag != pVis->getCanCollide()) {
			bCollisionFlagChanged = true;
			pVis->setCanCollide(bNewCollisionFlag);
		}
	}
	//LogInfo("Total mesh triangles = " << nTotalTriangles << " Mesh count = " << m_visuals.size() << " for object " << m_name << " (glid=" << m_LegacyGlid << ")");

	size_t iUnmatchedTriangle = std::find(abCollisionTrianglesMatched.begin(), abCollisionTrianglesMatched.end(), false) - abCollisionTrianglesMatched.begin();
	bool bUnmatchedCollisionTriangle = iUnmatchedTriangle < abCollisionTrianglesMatched.size();

	int iConfidence = -1; // -1: Good chance of being wrong. 0: We have a good guess. +1: Fairly certain.
	if (nMeshesNotInCollisionAndShouldBe == 0 && !bUnmatchedCollisionTriangle) {
		// All meshes that should be in collision are, and all triangles in collision mesh are accounted for.
		iConfidence = 1;
	} else if (nCollisionVisuals == m_visuals.size()) {
		// Every visual participates in collision. Assume there isn't any collision where object isn't visible.
		iConfidence = 0;
	} else if (!bCollisionFlagChanged) {
		// Every visual that canCollide() is in the collision mesh, and none that can't are in it.
		iConfidence = 0;
	} else if (m_visuals.size() == 1 && nOldCollisionTriangles > 0) {
		// Only one visual and there is collision geometry.
		// Assume the geometry is a simplified representation of the visual.
		m_visuals.front()->setCanCollide(true);
		iConfidence = 0;
	}

	if (iConfidence != 1) {
		if (iConfidence == 0) {
			LogWarn("Collision flags for object " << m_name << " (glid=" << m_LegacyGlid << ") may not be correct");
		} else {
			LogWarn("Collision flags for object " << m_name << " (glid=" << m_LegacyGlid << ") are probably incorrect and new collision will be created");
		}
	} else {
		//LogInfo("Collision flags for object " << m_name << " (glid=" << m_LegacyGlid << ") are probably correct");
	}

	if (bCreateCollisionVisualWhenNeeded && iConfidence == -1) {
		for (StreamableDynamicObjectVisual* pVisual : m_visuals)
			pVisual->setCanCollide(false);
		if (!createVisualFromCollisionData())
			LogError("Failed to create collision visual for object " << m_name << " (glid=" << m_LegacyGlid << ")");
	}

	return iConfidence;
}

// Returns false for empty mesh.
bool StreamableDynamicObject::isMeshInCollision(ReMesh* pCollisionMesh, std::vector<bool>* pabCollisionTrianglesMatched) {
	if (m_pLegacyCollision == nullptr)
		return false;

	if (!pCollisionMesh)
		return false;

	ReMeshData* pMeshData = nullptr;
	pCollisionMesh->Access(&pMeshData);
	if (!pMeshData)
		return true;

	if (pMeshData->NumV == 0)
		return true;

	Vector3f ptMin = pMeshData->pP[0];
	Vector3f ptMax = pMeshData->pP[0];
	for (size_t i = 1; i < pMeshData->NumV; ++i) {
		const Vector3f& pt = pMeshData->pP[i];
		for (size_t j = 0; j < 3; ++j) {
			if (pt[j] < ptMin[j])
				ptMin[j] = pt[j];
			else if (pt[j] > ptMax[j])
				ptMax[j] = pt[j];
		}
	}

#if 0
	float fScale = 1.0f;// / 12.0f;
	for ( size_t i = 0; i < 3; ++i )
		fScale = std::max<float>(fScale, ptMax[i] - ptMin[i]);

	// Is there a bug in the compression code? I'm getting an object of size <.1 with a ScaleSize of 2.
	// The object is found in the collision mesh if I treat it as ScaleSize of 1.
	//float fRelativePrecision = 1.0f / ((1 << (8*1))-1);
	//float fRoundoffFactor = 1<<3;
	//float fTolerance = fScale * fRelativePrecision * fRoundoffFactor;
#endif
	float fTolerance = 1.0f / 24.0f; // Half an inch seems like a reliable value in tests.

	assert(pMeshData->PrimType == ReMeshPrimType::TRISTRIP || pMeshData->PrimType == ReMeshPrimType::TRILIST);
	size_t iStep = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? 1 : 3;
	size_t nIndices = pMeshData->NumI;
	size_t iEnd = pMeshData->PrimType == ReMeshPrimType::TRISTRIP ? std::max<size_t>(2, nIndices) - 2 : nIndices - nIndices % 3;
	Vector3f* pFirstPoint = pMeshData->pP;
	uint16_t* pFirstIndex = pMeshData->pI;
	for (size_t i = 0; i < iEnd; i += iStep) {
		size_t idx0 = pFirstIndex[i + 0];
		size_t idx1 = pFirstIndex[i + 1];
		size_t idx2 = pFirstIndex[i + 2];
		if (idx0 == idx1 || idx1 == idx2 || idx2 == idx0)
			continue;
		const Vector3f& pt0 = pFirstPoint[idx0];
		const Vector3f& pt1 = pFirstPoint[idx1];
		const Vector3f& pt2 = pFirstPoint[idx2];
		size_t iTriIndex = SIZE_MAX;
		if (m_pLegacyCollision->ContainsTriangle(pt0, pt1, pt2, fTolerance, &iTriIndex)) {
			if (pabCollisionTrianglesMatched) {
				if (pabCollisionTrianglesMatched->size() <= iTriIndex)
					pabCollisionTrianglesMatched->resize(iTriIndex + 1);
				(*pabCollisionTrianglesMatched)[iTriIndex] = true;
			}
		} else {
			if (pMeshData->PrimType == ReMeshPrimType::TRISTRIP && i * 3 >= 65535)
				return true; // Collision meshes don't store more than 65535 vertices per mesh. We don't expect to find them here.
			if (pt0 != pt1 && pt1 != pt2 && pt2 != pt0) {
				// Only an error if triangle is not degenerate.
#if defined(DEBUG)
				static bool bFindClosest = false;
				if (bFindClosest) {
					Vector3f vErrorDirection;
					Vector3f aptsClosestTriangle[3];
					size_t iClosestTriangle = SIZE_MAX;
					float fError = m_pLegacyCollision->ClosestTriangle(pt0, pt1, pt2, &vErrorDirection, &aptsClosestTriangle, &iClosestTriangle);
					(void)fError;
					if (iClosestTriangle != SIZE_MAX) {
						Vector3f ptCenter = (aptsClosestTriangle[0] + aptsClosestTriangle[1] + aptsClosestTriangle[2]) * (1.0f / 3.0f);
						REFBOX* pRefBox = m_pLegacyCollision->FindLeafBoxAtPosition(ptCenter);
						(void)pRefBox;
					}
				}
#endif
				return false;
			}
		}
	}
	return true;
}

bool StreamableDynamicObject::isVisualInCollision(StreamableDynamicObjectVisual* pVisual, ReMesh** ppCollisionMesh) {
	int nLODs = pVisual->getLodCount();
	for (int i = 0; i < nLODs; ++i) {
		ReMesh* pVisualMesh = pVisual->getReMesh(i);
		bool bEmptyMesh = false;
		if (pVisualMesh == nullptr) {
			bEmptyMesh = true;
		} else {
			ReMeshData* pMeshData = nullptr;
			pVisualMesh->Access(&pMeshData);
			if (!pMeshData || pMeshData->NumI == 0)
				bEmptyMesh = true;
		}

		if (bEmptyMesh)
			continue;

		if (isMeshInCollision(pVisualMesh, nullptr)) {
			if (ppCollisionMesh)
				*ppCollisionMesh = pVisualMesh;
			return true;
		}
	}
	return false;
}

bool StreamableDynamicObject::createVisualFromCollisionData() {
	if (!m_pLegacyCollision)
		return true;

	ReD3DX9DeviceState* dev = nullptr;
	auto pICE = IClientEngine::Instance();
	if (pICE)
		dev = pICE->GetReDeviceState();
	else
		dev = ReD3DX9DeviceState::Instance();

	std::vector<std::unique_ptr<ReMesh>> apNewMeshes;
	bool bCreateMeshResult = m_pLegacyCollision->CreateReMeshFromCollisionData(dev, false, &apNewMeshes);
	if (!bCreateMeshResult)
		return false;

	if (apNewMeshes.empty())
		return true;

	for (size_t i = 0; i < apNewMeshes.size(); ++i) {
		std::unique_ptr<ReMesh>& pNewMesh = apNewMeshes[i];
		UINT64 meshHash = pNewMesh->GetHash();
		MeshRM::Instance()->AddMesh(pNewMesh.release(), MeshRM::RMP_ID_DYNAMIC);

		std::unique_ptr<StreamableDynamicObjectVisual> pVisual = std::make_unique<StreamableDynamicObjectVisual>();
		std::ostringstream strm;
		strm << "CCollisionObj " << i + 1;
		pVisual->setName(strm.str().c_str());
		pVisual->setCanCollide(true);
		pVisual->addLOD(meshHash);
		this->AddVisual(pVisual.release(), false, false);
	}

	return true;
}

StreamableDynamicObjectVisual* StreamableDynamicObject::GetVisualFromCollisionData(size_t iRigidBody, int iPart) {
	bool bCollisionRigidBody = iRigidBody == 0;
	int iVisual = 0;
	for (StreamableDynamicObjectVisual* pVis : m_visuals) {
		if (bCollisionRigidBody == pVis->getCanCollide()) {
			if (iVisual == iPart)
				return pVis;
			++iVisual;
		}
	}
	return nullptr;
}

void StreamableDynamicObject::createCollisionShapes() {
	std::vector<Physics::CollisionShapeMeshProperties> aCollisionMeshProps;
	std::vector<Physics::CollisionShapeMeshProperties> aVisibleMeshProps;

	// Animated skeletons are treated as their bounding boxes for collision purposes.
	static bool bUseBoundingBoxForAnimatedSkeletons = false;
	if (bUseBoundingBoxForAnimatedSkeletons && m_pSkeleton && m_pSkeleton->getBoneCount() > 0) {
		Physics::CollisionShapeBoxProperties boxProps;
		BNDBOX bbox = getBoundBox();
		boxProps.m_vDimensionsMeters = FeetToMeters(bbox.GetSize());
		std::shared_ptr<Physics::CollisionShape> pBoxShape = Physics::CreateCollisionShape(boxProps);
		bool bCollisionEnabled = false;
		for (StreamableDynamicObjectVisual* pVis : m_visuals)
			bCollisionEnabled |= pVis->getCanCollide();
		Vector3f vOffset = bbox.GetCenter() * m_skeletonAdditionalScale;
		if (bCollisionEnabled) {
			m_pCollisionShape = pBoxShape;
			m_vCollisionShapeOffset = vOffset;
		} else {
			m_pVisualShape = pBoxShape;
			m_vVisualShapeOffset = vOffset;
		}
		return;
	}

	for (StreamableDynamicObjectVisual* pVis : m_visuals) {
		if (pVis->getCanCollide()) {
			ReMesh* pCollisionMesh = pVis->getCollisionReMesh();
			if (!pCollisionMesh)
				continue;

			aCollisionMeshProps.emplace_back(pCollisionMesh->GetPhysicsMeshProps());
		} else {
			ReMesh* pVisibleMesh = pVis->getReMesh(0);
			aVisibleMeshProps.emplace_back(pVisibleMesh->GetPhysicsMeshProps());
		}
	}

	if (!aCollisionMeshProps.empty()) {
		m_pCollisionShape = Physics::CreateCollisionShape(aCollisionMeshProps, Vector3f(FeetToMeters(1.0f)));
		if (m_pCollisionShape)
			m_pCollisionShape->SetUserData(std::make_shared<PhysicsCollisionUserData>(this));
	}
	if (!aVisibleMeshProps.empty()) {
		m_pVisualShape = Physics::CreateCollisionShape(aVisibleMeshProps, Vector3f(FeetToMeters(1.0f)));
		if (m_pVisualShape)
			m_pVisualShape->SetUserData(std::make_shared<PhysicsCollisionUserData>(this));
	}
}

void StreamableDynamicObject::setLight(CRTLightObj* legacyLight) {
	// no assert NULL is a valid light
	if (m_light) {
		delete m_light;
		m_light = NULL;
	}
	if (legacyLight && legacyLight->m_material) {
		int r, g, b;
		float range, att1, att2, att3;
		r = (int)(legacyLight->m_material->m_baseMaterial.Diffuse.r * 255.0f);
		g = (int)(legacyLight->m_material->m_baseMaterial.Diffuse.g * 255.0f);
		b = (int)(legacyLight->m_material->m_baseMaterial.Diffuse.b * 255.0f);
		range = legacyLight->m_range;
		att1 = legacyLight->m_attenuation0;
		att2 = legacyLight->m_attenuation1;
		att3 = legacyLight->m_attenuation2;
		m_light = new StreamableDynamicObjectLight(r, g, b, range, att1, att2, att3);
	} else {
		m_light = NULL;
	}
}

std::unique_ptr<CRTLightObj> StreamableDynamicObject::createLegacyLight() {
	if (m_light) {
		std::unique_ptr<CRTLightObj> legacyLight = std::make_unique<CRTLightObj>();
		legacyLight->m_material = std::make_unique<CMaterialObject>();
		legacyLight->m_material->m_baseMaterial.Diffuse.r = ((float)m_light->getLightColor().r / 255.0f);
		legacyLight->m_material->m_baseMaterial.Diffuse.g = ((float)m_light->getLightColor().g / 255.0f);
		legacyLight->m_material->m_baseMaterial.Diffuse.b = ((float)m_light->getLightColor().b / 255.0f);
		legacyLight->m_material->m_useMaterial.Diffuse.r = ((float)m_light->getLightColor().r / 255.0f);
		legacyLight->m_material->m_useMaterial.Diffuse.g = ((float)m_light->getLightColor().g / 255.0f);
		legacyLight->m_material->m_useMaterial.Diffuse.b = ((float)m_light->getLightColor().b / 255.0f);

		legacyLight->m_material->m_baseMaterial.Ambient.r = (float)m_light->getLightColor().r / 255.0f;
		legacyLight->m_material->m_baseMaterial.Ambient.g = (float)m_light->getLightColor().g / 255.0f;
		legacyLight->m_material->m_baseMaterial.Ambient.b = (float)m_light->getLightColor().b / 255.0f;
		legacyLight->m_material->m_useMaterial.Ambient.r = (float)m_light->getLightColor().r / 255.0f;
		legacyLight->m_material->m_useMaterial.Ambient.g = (float)m_light->getLightColor().g / 255.0f;
		legacyLight->m_material->m_useMaterial.Ambient.b = (float)m_light->getLightColor().b / 255.0f;

		legacyLight->m_range = m_light->getRange();
		legacyLight->m_attenuation0 = m_light->getAttenuation1();
		legacyLight->m_attenuation1 = m_light->getAttenuation2();
		legacyLight->m_attenuation2 = m_light->getAttenuation3();

		return legacyLight;
	} else {
		return nullptr;
	}
}

void StreamableDynamicObject::setParticleOffset(float x, float y, float z) {
	m_particleOffset.x = x;
	m_particleOffset.y = y;
	m_particleOffset.z = z;
}

void StreamableDynamicObject::setTriggers(CTriggerObjectList* trg) {
	if (trg != NULL) {
		trg->Clone(&m_triggers);
	} else {
		m_triggers = NULL;
	}
}

void StreamableDynamicObject::setLightPosition(float x, float y, float z) {
	m_lightPosition.x = x;
	m_lightPosition.y = y;
	m_lightPosition.z = z;
}

StreamableDynamicObject* StreamableDynamicObject::convertFromAccessory(CArmedInventoryObj* accessory, const GLID& glid) {
	StreamableDynamicObject* result = NULL;
#ifdef _ClientOutput
	if (accessory == NULL) {
		return NULL;
	}
	result = new StreamableDynamicObject();
	result->setName(accessory->getName().c_str());

	// skinned meshes (dynamic object uses 3rd person visual only)
	auto pSkeleton = accessory->getSkeleton();
	auto pSkeletonConfig = accessory->getSkeletonConfig();

	if (pSkeleton != nullptr && pSkeletonConfig != nullptr) {
		CAlterUnitDBObjectList* configs = pSkeletonConfig->m_meshSlots;
		if (configs) {
			for (POSITION posLoc = configs->GetHeadPosition(); posLoc != NULL;) {
				CAlterUnitDBObject* aPtr = static_cast<CAlterUnitDBObject*>(configs->GetNext(posLoc));
				POSITION cfgPos = aPtr->m_configurations->FindIndex(0);
				CDeformableMesh* dMesh = static_cast<CDeformableMesh*>(aPtr->m_configurations->GetAt(cfgPos));
				if (dMesh->m_lodCount > 0) {
					ReMesh* skin = MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[0], MeshRM::RMP_ID_PERSISTENT);
					if (skin == NULL) {
						skin = MeshRM::Instance()->FindMesh(dMesh->m_lodHashes[0], MeshRM::RMP_ID_DYNAMIC);
					}
					if (skin != NULL) {
						StreamableDynamicObjectVisual* vis = new StreamableDynamicObjectVisual();
						vis->addLOD(dMesh->m_lodHashes[0]);
						vis->setMaterial(dMesh->m_material);
						result->AddVisual(vis, true, false);
					}
				}
			}
		}
	}

	//rigid meshes
	if (pSkeleton->getBoneList() != nullptr) {
		for (POSITION posLoc = pSkeleton->getBoneList()->GetHeadPosition(); posLoc != NULL;) {
			auto pBone = static_cast<const CBoneObject*>(pSkeleton->getBoneList()->GetNext(posLoc));

			for (auto itMesh = pBone->m_rigidMeshes.cbegin(); itMesh != pBone->m_rigidMeshes.cend(); ++itMesh) {
				const CHierarchyVisObj* visObj = (*itMesh)->GetByIndex(0);
				if (visObj->m_reMesh) {
					ReMesh* meshCopy;
					visObj->m_reMesh->Clone(&meshCopy, true);
					MeshRM::Instance()->AddMesh(meshCopy, MeshRM::RMP_ID_DYNAMIC);

					StreamableDynamicObjectVisual* vis = new StreamableDynamicObjectVisual();
					vis->addLOD(visObj->m_reMesh->GetHash());
					vis->setMaterial(visObj->m_mesh->m_materialObject);
					result->AddVisual(vis, true, false);
				}
			}
		}
	}

	result->setSkeletonAdditionalScale(accessory->getScale());
	result->setBoundBox(result->computeBoundingBox());
#endif
	return result;
}

BEGIN_GETSET_IMPL(StreamableDynamicObject, IDS_DYNAMICOBJ_NAME)
GETSET2_MAX(m_name, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_OBJNAME, IDS_DYNAMICOBJ_OBJNAME_HELP, STRLEN_MAX)
GETSET2(m_attachable, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_ISATTACHABLE, IDS_DYNAMICOBJ_ISATTACHABLE_HELP)
GETSET2(m_interactive, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_ISINTERACTIVE, IDS_DYNAMICOBJ_ISINTERACTIVE_HELP)
GETSET2_RANGE(m_particleEffectId, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_PARTICLEEFFECTID, IDS_DYNAMICOBJ_PARTICLEEFFECTID_HELP, -1, INT_MAX)
GETSET2(m_particleOffset, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_PARTICLEEFFECTOFFSET, IDS_DYNAMICOBJ_PARTICLEEFFECTOFFSET_HELP)
GETSET2(m_canObjectPlayFlash, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_CANPLAYMOVIE, IDS_DYNAMICOBJ_CANPLAYMOVIE_HELP)
GETSET2(m_boundBox.min, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_BOUNDBOXMIN, IDS_DYNAMICOBJ_BOUNDBOXMIN_HELP)
GETSET2(m_boundBox.max, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_BOUNDBOXMAX, IDS_DYNAMICOBJ_BOUNDBOXMAX_HELP)
GETSET2(m_objectHasCollision, gsBool, GS_FLAGS_DEFAULT, 0, 0, IDS_DYNAMICOBJ_HASCOLLISION, IDS_DYNAMICOBJ_HASCOLLISION_HELP)
END_GETSET_IMPL

Matrix44f StreamableDynamicObjectVisual::getRelativeTransform() const {
	// Refactored from dynamicObPlacementModelProxy [Yanfeng 03/2009]
	Matrix44f matrix;
	matrix.MakeIdentity();

	//calc world space transformation matrix = object * world
	Matrix44f rotation;
	ConvertRotationZXY(ToRadians(getRelativeOrientation()), out(rotation));

	Matrix44f translation;
	translation.MakeTranslation(getRelativePosition());

	matrix = rotation * translation;
	return matrix;
}

ReMesh* StreamableDynamicObjectVisual::getCollisionReMesh() {
	if (m_LODCollisionSource != -1)
		return getReMesh(m_LODCollisionSource);
	if (m_altCollisionSource)
		return m_altCollisionSource;
	if (getLodCount() > 0)
		return getReMesh(0);
	return nullptr;
}

void StreamableDynamicObjectVisual::setCollisionSource(ReMesh* apReMesh, BOOL abClone /*= FALSE */) {
	clearCollisionSource();

	if (apReMesh == NULL) {
		m_altCollisionSource = NULL;
		return;
	}

	if (abClone) {
		apReMesh->Clone(&m_altCollisionSource);
		m_bDisposeAltCollision = TRUE;
	}
}

void StreamableDynamicObjectVisual::setCollisionSource(CEXMeshObj* apExMesh) {
#ifdef _ClientOutput
	ReMesh* pAltCol = NULL;

	if (apExMesh != NULL) {
		CEXMeshObj* pTmpMesh = NULL;
		apExMesh->Clone(&pTmpMesh, NULL);

		pTmpMesh->BakeVertices();
		pTmpMesh->ComputeBoundingVolumes();

		pAltCol = MeshRM::Instance()->CreateReStaticMesh(pTmpMesh);

		pTmpMesh->SafeDelete();
		delete pTmpMesh;
	}

	setCollisionSource(pAltCol);
#else
	ASSERT(FALSE);
#endif
}

void StreamableDynamicObjectVisual::setCollisionSource(int lodNo) {
	clearCollisionSource();

	ASSERT(lodNo >= 0 && lodNo < getLodCount());
	m_LODCollisionSource = lodNo;
}

void StreamableDynamicObjectVisual::clearCollisionSource() {
	m_LODCollisionSource = -1;
	if (m_altCollisionSource && m_bDisposeAltCollision) {
		delete m_altCollisionSource;
		m_altCollisionSource = NULL;
	}
	m_bDisposeAltCollision = FALSE;
}

void StreamableDynamicObject::setCollisionSourceByLOD(int lodLevel) {
	for (auto pVis : m_visuals) {
		if (!pVis)
			continue;
		pVis->setCollisionSource(lodLevel);
	}
}

float StreamableDynamicObject::getBoundingRadius() const {
	if (getVisualCount() > 0) {
		Vector3f d(Vector3f(m_boundBox.max) - Vector3f(m_boundBox.min));
		return 0.5f * d.Length();
	} else {
		return 3.0f;
	}
}

BNDBOX StreamableDynamicObject::computeBoundingBox(const Matrix44f* world) const {
	BoundBox bbox;

#ifdef _ClientOutput
	if (m_linkedMovObjNetTraceId == 0 && getVisualCount() > 0) {
		for (size_t visLoop = 0; visLoop < getVisualCount(); ++visLoop) {
			StreamableDynamicObjectVisual* pVis = Visual(visLoop);

			for (int lodLoop = 0; lodLoop < pVis->getLodCount(); ++lodLoop) {
				UINT64 hash = pVis->getLOD(lodLoop);
				ReMesh* rm = MeshRM::Instance()->FindMesh(hash, MeshRM::RMP_ID_DYNAMIC);
				//			ASSERT( rm != NULL );
				if (rm == NULL) {
					continue; // We can't find a mesh for the listed hash, either archive corrupt
					// or some UGC error has allowed a visual with no mesh to be loaded.
				}

				ReMeshData* rmData = NULL;
				rm->Access(&rmData);
				ASSERT(rmData != NULL);
				ABBOX bboxLOD;
				bboxLOD = MeshRM::Instance()->ComputeBoundingBoxWithMatrix(rm, world); // NULL world simply does not tranform the points.

				bbox.merge(bboxLOD);
			}
		}
	} else {
		// need a bound box for actor based objs that have no visuals.
		float x, y, z;
		if (world) {
			x = (*world)(3, 0);
			y = (*world)(3, 1);
			z = (*world)(3, 2);
		} else {
			x = 0.0f;
			y = 0.0f;
			z = 0.0f;
		}
		bbox.minX = x - 3.0f;
		bbox.minY = y - 6.0f;
		bbox.minZ = z - 3.0f;
		bbox.maxX = x + 3.0f;
		bbox.maxY = y + 6.0f;
		bbox.maxZ = z + 3.0f;
	}
#else
	ASSERT(FALSE);
#endif

	bbox.validate();
	return bbox;
}

void StreamableDynamicObjectLight::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

void StreamableDynamicObjectVisual::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_altCollisionSource);
		pMemSizeGadget->AddObject(m_animModule);
		pMemSizeGadget->AddObject(m_lodHashes);
		pMemSizeGadget->AddObject(m_material);
		pMemSizeGadget->AddObject(m_name);
		pMemSizeGadget->AddObject(m_reMeshDirectAccess);
	}
}

void StreamableDynamicObject::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	// Attempt to add the base object size of StreamableDynamicObject. If it hasn't
	// already been added, AddObjectSizeof()->true, then add all of this objects child
	// data members that are allocated dynamically or also have child data members
	// that are dynamically allocated.
	//
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_aCollisionTriangleVerts); // std::vector<Vector3f>
		pMemSizeGadget->AddObject(m_light); // StreamableDynamicObjectLight*
		pMemSizeGadget->AddObject(m_name); // std::string
		pMemSizeGadget->AddObject(m_pCollisionShape); // std::shared_ptr<Physics::CollisionShape>
		pMemSizeGadget->AddObject(m_pVisualShape); // std::shared_ptr<Physics::CollisionShape>
		pMemSizeGadget->AddObject(m_triggers); // CTriggerObjectList*
		pMemSizeGadget->AddObject(m_visualCustomizable); // vector<bool>
		pMemSizeGadget->AddObject(m_visualMediaSupported); // vector<bool>
		pMemSizeGadget->AddObject(m_visuals); // vector<StreamableDynamicObjectVisual*>

		/*----#MLB_TODO_IMemSizeGadget In Progress ----------------
		m_pLegacyCollision  // 	CCollisionObj*  (NOTE: Should not be used, but need to double check)
		/*----------------------------------------------------------------------------*/
	}
}

} // namespace KEP
