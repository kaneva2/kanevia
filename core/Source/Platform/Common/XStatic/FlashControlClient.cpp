///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "FlashControlProxy.h"
#include "FlashControlFactoryProxy.h"
#include "FlashClientServerAPI.h"

#include "LogHelper.h"
static LogInstance("Instance");
#include "Core/Util/Parameter.h"

namespace KEP {
namespace FlashControl {

class FlashControlClient : public IFlashControlProxy {
public:
	FlashControlClient(IFlashControlFactoryProxy* pFlashControlFactoryProxy);
	~FlashControlClient();

	virtual uint32_t GetMeshId() const override final {
		return m_CurrentState.m_CreateSettings.GetValue().m_iMeshId;
	}

	virtual bool Initialize(const InitializeArgs& args) override final;

	virtual bool StopProcessing() override final;
	virtual std::string GetIdString(bool bVerbose = false) const override final {
		return BuildFlashMeshIdString("", GetMeshId(), bVerbose);
	}
	virtual bool GetAvailableBitmap(void** ppBitmapData, uint32_t* bitmapWidth, uint32_t* bitmapHeight) override final;

	virtual bool Rewind() override final {
		++m_CurrentState.m_iRewindRequestId;
		return SendStateUpdateRequest();
	}
	virtual bool SetMediaParams(const std::string& strParams, const std::string& strURL, FlashScriptAccessMode eAllowScriptAccess) override final {
		FlashStateData::MediaSettings& mediaSettings = m_CurrentState.m_MediaSettings.GetRefToUpdate();
		mediaSettings.m_bufferFlashVars.Set(strParams);
		mediaSettings.m_bufferURL.Set(strURL);
		mediaSettings.m_eAllowScriptAccess = eAllowScriptAccess;
		return SendStateUpdateRequest();
	}
	virtual bool UpdateRect(int32_t x, int32_t y, uint32_t width, uint32_t height, uint32_t iCropTop, uint32_t iCropLeft, uint32_t iCropBottom, uint32_t iCropRight) override final {
		FlashStateData::RectSettings newRectSettings;
		newRectSettings.m_iPositionX = x;
		newRectSettings.m_iPositionY = y;
		newRectSettings.m_iWidth = width;
		newRectSettings.m_iHeight = height;
		newRectSettings.m_iCropLeft = iCropLeft;
		newRectSettings.m_iCropTop = iCropTop;
		newRectSettings.m_iCropBottom = iCropBottom;
		newRectSettings.m_iCropRight = iCropRight;

		bool bBitmapSizeChanged = m_CurrentState.m_RectSettings.GetValue().GetBitmapWidth() != newRectSettings.GetBitmapWidth() || m_CurrentState.m_RectSettings.GetValue().GetBitmapHeight() != newRectSettings.GetBitmapHeight();
		m_CurrentState.m_RectSettings.UpdateValue(newRectSettings);
		if (bBitmapSizeChanged)
			m_CurrentState.m_iBitmapSharedMemoryId = CreateSharedMemoryBitmapsForCurrentSize();

		return SendStateUpdateRequest();
	}
	virtual bool UpdateBitmap() override final {
		++m_CurrentState.m_iGenerateBitmapId;
		return SendStateUpdateRequest();
	}
	virtual bool SetLocalVolume(float fLocalVolume) override final {
		m_CurrentState.m_LocalVolume.UpdateValue(fLocalVolume);
		return SendStateUpdateRequest();
	}
	virtual bool SetFocus() override final {
		++m_CurrentState.m_iSetFocusId;
		return SendStateUpdateRequest();
	}

private:
	bool CloseStateSharedMemory();
	bool CloseBitmapSharedMemory();
	bool CreateSharedMemoryMappingForState();
	uint32_t CreateSharedMemoryBitmapsForCurrentSize();
	bool SendStateUpdateRequest();

private:
	IFlashControlFactoryProxy* m_pFlashControlFactoryProxy = nullptr;
	// Per-mesh data.
	bool m_bInitialized = false;

	FlashStateData m_CurrentState;

	// Per-connection data.
	EventHandleWrapper m_RequestAvailableEvent; // To signal us that shared memory has changed and contains a new request.
	SharedMemory m_SharedMemory;
	MappedFileView m_SharedMemoryView;
	FlashSharedMemoryBlock* m_pSharedMemory = nullptr;
	TripleBufferWriterStateMgr m_ClientStateMgr;
	TripleBufferReaderStateMgr m_ServerStateMgr;
	uint32_t m_iNextBitmapSharedMemoryId = 1;
	uint32_t m_iPreviousAvailableBitmap = 0; // The m_iGenerateBitmapId that we most recently returned from GetAvailableBitmap.
	SharedMemory m_BitmapSharedMemory;
	WindowsBitmapWrapper m_ahBitmaps[3]; // Handles to bitmaps whose data is stored in shared memory.
	void* m_apBitmapData[3] = { 0, 0, 0 }; // Direct access to bitmap data.
};

FlashControlClient::FlashControlClient(IFlashControlFactoryProxy* pFlashControlFactoryProxy) {
	// Assign Mesh Id
	static uint32_t s_iMeshIds = 0; // meshes since launch
	++s_iMeshIds;
	m_CurrentState.m_CreateSettings.GetRefToUpdate().m_iMeshId = s_iMeshIds;
	HANDLE hRequestAvailableEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	m_RequestAvailableEvent.Set(hRequestAvailableEvent);

	// Every mesh is sharing a single static process right now.
	m_pFlashControlFactoryProxy = pFlashControlFactoryProxy;
}

bool FlashControlClient::CloseStateSharedMemory() {
	m_SharedMemory.Close();
	m_SharedMemoryView.Close();
	m_pSharedMemory = nullptr;
	return true;
}

bool FlashControlClient::CloseBitmapSharedMemory() {
	for (WindowsBitmapWrapper& h : m_ahBitmaps)
		h.Close();
	for (void*& p : m_apBitmapData)
		p = nullptr;

	m_BitmapSharedMemory.Close();
	return true;
}

bool FlashControlClient::CreateSharedMemoryMappingForState() {
	CloseStateSharedMemory();

	size_t iSharedMemorySize = sizeof(FlashSharedMemoryBlock);
	std::wstring wstrSharedMemoryName = ServerSharedMemoryName(GetCurrentProcessId(), GetMeshId());
	HANDLE hSharedMemory = CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, iSharedMemorySize, wstrSharedMemoryName.c_str());
	//LogInfo("Creating file mapping " << Utf16ToUtf8(wstrSharedMemoryName));
	if (!hSharedMemory) {
		LogError("Could not create shared memory.");
		return false;
	}
	m_SharedMemory.Set(hSharedMemory);

	// Assumes that MapViewOfFile returns a pointer to a cache-line aligned address.
	// If it doesn't, we should allocate extra space and adjust this pointer.
	m_SharedMemoryView = m_SharedMemory.Map(0, iSharedMemorySize, true);
	if (!m_SharedMemoryView) {
		LogError("Could not map shared memory.");
		return false;
	}
	m_pSharedMemory = new (m_SharedMemoryView.GetPointer()) FlashSharedMemoryBlock; // Placement new
	m_ClientStateMgr.SetSynchronizer(&m_pSharedMemory->m_ClientRequestSynchronizer);
	m_ServerStateMgr.SetSynchronizer(&m_pSharedMemory->m_ServerAcknowledgmentSynchronizer);
	return true;
}

uint32_t FlashControlClient::CreateSharedMemoryBitmapsForCurrentSize() {
	CloseBitmapSharedMemory();

	uint32_t iBitmapWidth = m_CurrentState.m_RectSettings.GetValue().GetBitmapWidth();
	uint32_t iBitmapHeight = m_CurrentState.m_RectSettings.GetValue().GetBitmapHeight();
	size_t iSharedMemorySize = 3 * CalculateSingleBitmapAllocSize(iBitmapWidth, iBitmapHeight);
	if (iSharedMemorySize == 0)
		return 0; // No bitmaps needed.

	uint32_t iBitmapSharedMemoryId = m_iNextBitmapSharedMemoryId++;
	std::wstring strSharedMemoryName = BitmapSharedMemoryName(GetCurrentProcessId(), GetMeshId(), iBitmapSharedMemoryId);
	const wchar_t* pszSharedMemoryName = strSharedMemoryName.c_str();
	HANDLE hSharedMemory = CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, iSharedMemorySize, pszSharedMemoryName);
	if (!hSharedMemory) {
		LogError("Could not create shared memory.");
		return 0;
	}
	m_BitmapSharedMemory.Set(hSharedMemory);

	CreateBitmapsInSharedMemory(m_BitmapSharedMemory.GetHandle(), m_ahBitmaps, m_apBitmapData, 0, iBitmapWidth, iBitmapHeight);

	return iBitmapSharedMemoryId;
}

FlashControlClient::~FlashControlClient() {
	m_pFlashControlFactoryProxy->StopFlashControl(GetMeshId());
	CloseBitmapSharedMemory();
	CloseStateSharedMemory();
}

bool FlashControlClient::SendStateUpdateRequest() {
	if (!m_pSharedMemory)
		return false;

	size_t idx = m_ClientStateMgr.GetWorkAreaIndex();
	m_pSharedMemory->m_aClientRequests[idx].m_FlashState.UpdateFrom(m_CurrentState);
	m_ClientStateMgr.CompletedWork();
	m_pFlashControlFactoryProxy->SignalRequestAvailable(GetMeshId());

	return true;
}

bool FlashControlClient::Initialize(const InitializeArgs& args) {
	bool bSucceeded = false;
	if (m_bInitialized)
		return false;

	if (!m_pFlashControlFactoryProxy)
		return false;

	if (!CreateSharedMemoryMappingForState())
		return false;

	if (!m_pFlashControlFactoryProxy->StartFlashControl(GetMeshId()))
		return false;

	uintptr_t iParentWindowHandle = reinterpret_cast<uintptr_t>(args.m_hwndParent);
	FlashStateData::CreateSettings& createSettings = m_CurrentState.m_CreateSettings.GetRefToUpdate();
	createSettings.m_bWantInput = args.m_bWantInput;
	//createSettings.m_iMeshId = m_iMeshId;
	createSettings.m_iParentWindowHandle = iParentWindowHandle;

	FlashStateData::MediaSettings& mediaSettings = m_CurrentState.m_MediaSettings.GetRefToUpdate();
	mediaSettings.m_bufferFlashVars.Set(args.m_strParams);
	mediaSettings.m_bufferURL.Set(args.m_strURL);
	mediaSettings.m_eAllowScriptAccess = args.m_eAllowScriptAccess;

	m_CurrentState.m_LocalVolume.UpdateValue(args.m_fLocalVolume);
	m_CurrentState.m_iGenerateBitmapId = 0; // No initial bitmap

	FlashStateData::RectSettings& rectSettings = m_CurrentState.m_RectSettings.GetRefToUpdate();
	rectSettings.m_iCropTop = args.m_iCropTop;
	rectSettings.m_iCropLeft = args.m_iCropLeft;
	rectSettings.m_iCropBottom = args.m_iCropBottom;
	rectSettings.m_iCropRight = args.m_iCropRight;
	rectSettings.m_iPositionX = args.m_iPositionX;
	rectSettings.m_iPositionY = args.m_iPositionY;
	rectSettings.m_iWidth = args.m_iWidth;
	rectSettings.m_iHeight = args.m_iHeight;
	m_CurrentState.m_iBitmapSharedMemoryId = CreateSharedMemoryBitmapsForCurrentSize();
	if (SendStateUpdateRequest()) {
		m_bInitialized = true;
		bSucceeded = true;
	}

	return true;
}

bool FlashControlClient::StopProcessing() {
	bool bSucceeded = true;
	m_CurrentState.m_bQuit = true;
	bSucceeded &= SendStateUpdateRequest();
	bSucceeded &= m_pFlashControlFactoryProxy->StopFlashControl(GetMeshId());
	return true;
}

bool FlashControlClient::GetAvailableBitmap(void** ppBitmapData, uint32_t* bitmapWidth, uint32_t* bitmapHeight) {
	if (!m_pSharedMemory)
		return false;

	if (!m_ServerStateMgr.ClaimBuffer())
		return false;

	uint32_t idx = m_ServerStateMgr.GetBufferIndex();

	FlashSharedMemoryBlock::ServerAcknowledgement* pAcknowledgement = &m_pSharedMemory->m_aServerAcknowledgements[idx];

	const GeneratedBitmapStateData* pBitmapState = &pAcknowledgement->m_BitmapState;
	size_t idxBitmap = pBitmapState->m_idxBitmapBuffer;
	if (idxBitmap >= 3)
		return false; // Bitmap generation failed, or no bitmaps have been requested yet.

	uint32_t iGenerateBitmapId = pBitmapState->m_iGenerateBitmapId;
	if (iGenerateBitmapId == m_iPreviousAvailableBitmap)
		return false; // Bitmap hasn't changed.

	m_iPreviousAvailableBitmap = iGenerateBitmapId;

	*ppBitmapData = m_apBitmapData[idxBitmap];
	*bitmapWidth = m_CurrentState.m_RectSettings.GetValue().GetBitmapWidth();
	*bitmapHeight = m_CurrentState.m_RectSettings.GetValue().GetBitmapHeight();
	return *ppBitmapData != nullptr;
}

// Intermediate layer between Client and IFlashControlFactoryProxy so that we can optionally handle multiple processes for Flash.
class FlashSystem : public IFlashSystem {
public:
	FlashSystem(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback = nullptr);
	~FlashSystem();

	// IFlashSystem interface
public:
	virtual bool IsFlashInstalled() override;
	virtual std::string GetFlashVersion() override;
	virtual void SetGlobalVolume(float fVolume, bool bMute) override;
	virtual std::shared_ptr<IFlashControlProxy> CreateFlashControlProxy() override;
	virtual size_t GetFlashProcessCrashCount() override;
	virtual size_t GetPeakFlashInstanceCount() override;
	virtual void CrashFlashProcess() override;

private:
	std::function<void(float fNewVolume, bool bNewMute)> m_volumeChangeCallback;
	std::unique_ptr<IFlashControlFactoryProxy> m_pFlashControlFactoryProxy;
};

FlashSystem::FlashSystem(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback) {
	m_volumeChangeCallback = volumeChangeCallback;
	m_pFlashControlFactoryProxy = IFlashControlFactoryProxy::Create([this](float fNewVolume, bool bNewMute) {
		if (m_volumeChangeCallback)
			m_volumeChangeCallback(fNewVolume, bNewMute);
	});
	m_pFlashControlFactoryProxy->Start();
}

FlashSystem::~FlashSystem() {
	m_pFlashControlFactoryProxy->Stop();
}

std::shared_ptr<IFlashControlProxy> FlashSystem::CreateFlashControlProxy() {
	return std::make_shared<FlashControlClient>(m_pFlashControlFactoryProxy.get());
}

bool FlashSystem::IsFlashInstalled() {
	return m_pFlashControlFactoryProxy->IsFlashInstalled();
}

std::string FlashSystem::GetFlashVersion() {
	return m_pFlashControlFactoryProxy->GetVersion();
}

void FlashSystem::SetGlobalVolume(float fVolume, bool bMute) {
	m_pFlashControlFactoryProxy->SetGlobalVolume(fVolume, bMute);
}

size_t FlashSystem::GetFlashProcessCrashCount() {
	return m_pFlashControlFactoryProxy->GetFlashProcessCrashCount();
}

size_t FlashSystem::GetPeakFlashInstanceCount() {
	return m_pFlashControlFactoryProxy->GetPeakFlashInstanceCount();
}

void FlashSystem::CrashFlashProcess() {
	return m_pFlashControlFactoryProxy->CrashFlashProcess();
}

} // namespace FlashControl

// static
std::shared_ptr<IFlashSystem> IFlashSystem::New(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback) {
	return std::make_shared<FlashControl::FlashSystem>(volumeChangeCallback);
}

} // namespace KEP
