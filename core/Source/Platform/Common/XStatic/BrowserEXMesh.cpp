/******************************************************************************
 BrowserEXMesh.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"

#include <BrowserEXMesh.h>

#if (DRF_BROWSER_MESH == 1)
#include "CEXMeshClass.h"
#include "CStateManagementClass.h"
#include "TextureDatabase.h"

using namespace KEP;

int BrowserEXMesh::Render(LPDIRECT3DDEVICE9 pd3dDevice, IN CEXMeshObj* mesh, int vertexType) {
	int ret = 0;

	//this will need to have some management added into it
	m_pDIRECT3DDEVICE9 = pd3dDevice;

	GetTexture() = CreateTextureFromWindow();

	// copied from DrawTexturedQuad
	m_stateManager->SetMaterial(pd3dDevice, &mesh->m_materialObject->m_useMaterial);

	pd3dDevice->SetTexture(0, GetTexture()); // set texture

	m_stateManager->SetDiffuseSource(mesh->m_abVertexArray.m_advancedFixedMode, pd3dDevice);
	m_stateManager->SetVertexType(mesh->m_abVertexArray.m_uvCount, pd3dDevice);
	ret = mesh->m_abVertexArray.Render(pd3dDevice, vertexType);

	m_stateManager->SetTextureState(-1, 0, pd3dDevice, m_textureDatabase);

	return ret;
}

#endif