///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "OALOggSound.h"
#include "SoundPlacement.h"
#include "OALSoundProxy.h"
#include "IClientEngine.h"
#include "LogHelper.h"

static LogInstance("Instance");

namespace KEP {

typedef unsigned short U16;
typedef signed short S16;
typedef signed int S32;
typedef unsigned int U32;

#define BUFFER_SIZE 32768 // 32K

typedef struct _OggMemoryFile {
	unsigned char* dataPtr;
	long dataSize;
	long dataRead;
} OGG_MEMORYFILE;

static size_t vorbis_read(void* data_ptr, size_t byteSize, size_t sizeToRead, void* data_src);
static int vorbis_seek(void* data_src, ogg_int64_t offset, int origin);
static int vorbis_close(void* data_src);
static int vorbis_test_close(void* data_src);
static long vorbis_tell(void* data_src);

OALOggSound::OALOggSound() :
		m_format(AL_FORMAT_MONO16),
		m_freq(4400) {
	alGenBuffers(1, &m_bufferID);
}

OALOggSound::~OALOggSound() {
	alDeleteBuffers(1, &m_bufferID);
	ov_clear(&m_oggStream);
}

// This blocks and reads the whole file needs to be threaded off.
// Opens an ogg based on a proxy
bool OALOggSound::OpenOgg(OALSoundProxy* proxy, TimeMs& duration) {
	return OpenOgg(proxy->GetRawBuffer(), proxy->GetRawSize(), duration);
}

// Opens an ogg based on a BYTE array.
bool OALOggSound::OpenOgg(BYTE* rawData, FileSize dataSize, TimeMs& duration) {
	int result, bitStream;
	char data[BUFFER_SIZE];
	long bytes;
	ov_callbacks oggCallbacks;

	if (rawData == NULL)
		return false;

	OGG_MEMORYFILE* oggMemFileStruct = new OGG_MEMORYFILE;
	oggMemFileStruct->dataRead = 0;
	oggMemFileStruct->dataSize = (long)dataSize;
	oggMemFileStruct->dataPtr = rawData;

	oggCallbacks.read_func = vorbis_read;
	oggCallbacks.close_func = vorbis_test_close;
	oggCallbacks.seek_func = vorbis_seek;
	oggCallbacks.tell_func = vorbis_tell;

	// Test Open the ogg file just to get the real size
	if (ov_open_callbacks(oggMemFileStruct, &m_oggStream, NULL, 0, oggCallbacks) < 0)
		return false;

	ogg_int64_t real_size = ov_raw_total(&m_oggStream, -1);
	ov_clear(&m_oggStream); // ov_clear apparently calls delete on the pointer.

	// reopen it with the encryption padding removed
	oggMemFileStruct = new OGG_MEMORYFILE;
	oggMemFileStruct->dataRead = 0;
	oggMemFileStruct->dataSize = (long)real_size;
	oggMemFileStruct->dataPtr = rawData;

	oggCallbacks.close_func = vorbis_close;

	if ((result = ov_open_callbacks(oggMemFileStruct, &m_oggStream, NULL, 0, oggCallbacks)) < 0)
		return false;

	m_vorbisInfo = ov_info(&m_oggStream, -1);
	m_vorbisComment = ov_comment(&m_oggStream, -1);

	do {
		bytes = ov_read(&m_oggStream, data, BUFFER_SIZE, 0, 2, 1, &bitStream);
		m_bufferData.insert(m_bufferData.end(), data, data + bytes);
	} while (bytes > 0);
	size_t bufSize = m_bufferData.size();

	bool mono = (m_vorbisInfo->channels == 1);
	m_freq = m_vorbisInfo->rate;
	duration = bufSize / 2.0f / m_vorbisInfo->channels / (m_freq / 1000.0f);
	m_format = mono ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;
	LogInfo((mono ? "MONO" : "STEREO") << " " << m_freq << "hz " << FMT_TIME << duration << "ms");

	// DRF - Convert Sound Placement Stereo -> Mono
	if (m_format == AL_FORMAT_STEREO16) {
		LogWarn("STEREO -> MONO");
		std::vector<char> buf;
		size_t dither = 0;
		for (size_t i = 0; i < bufSize; i += 4) {
			buf.push_back(m_bufferData[i + dither]);
			buf.push_back(m_bufferData[i + dither + 1]);
			dither = 2 - dither;
		}
		m_bufferData = buf;
		m_vorbisInfo->channels = 1;
		m_format = AL_FORMAT_MONO16;
	}

	alGenBuffers(1, &m_bufferID);

	// Calculate Sound Duration (ms)
	size_t samples = m_bufferData.size() / 2.0f;
	size_t samplesPerChannel = samples / m_vorbisInfo->channels;
	duration = (TimeMs)(samplesPerChannel / (m_freq / 1000.0f));

	return true;
}

bool OALOggSound::Play(ALuint source) {
	if (Playing(source))
		return true;
	alBufferData(m_bufferID, m_format, &m_bufferData[0], static_cast<ALsizei>(m_bufferData.size()), m_freq);
	alSourcei(source, AL_BUFFER, m_bufferID);
	alSourcePlay(source);
	return true;
}

/////////////////////////////////////// Ogg Vorbis IO Callbacks ///////////////////////////////////////
size_t vorbis_read(void* data_ptr, size_t byteSize, size_t sizeToRead, void* data_src) {
	OGG_MEMORYFILE* vorbisData = reinterpret_cast<OGG_MEMORYFILE*>(data_src);
	if (NULL == vorbisData) {
		return -1;
	}
	size_t actualSizeToRead, spaceToEOF;
	spaceToEOF = vorbisData->dataSize - vorbisData->dataRead;
	if ((sizeToRead * byteSize) < spaceToEOF) {
		actualSizeToRead = (sizeToRead * byteSize);
	} else {
		actualSizeToRead = spaceToEOF;
	}

	if (actualSizeToRead) {
		memcpy(data_ptr, (char*)vorbisData->dataPtr + vorbisData->dataRead, actualSizeToRead);
		vorbisData->dataRead += actualSizeToRead;
	}
	return actualSizeToRead;
}

int vorbis_seek(void* data_src, ogg_int64_t offset, int origin) {
	OGG_MEMORYFILE* vorbisData = reinterpret_cast<OGG_MEMORYFILE*>(data_src);
	if (NULL == vorbisData) {
		return -1;
	}

	switch (origin) {
		case SEEK_SET: {
			ogg_int64_t actualOffset;
			if (vorbisData->dataSize >= offset) {
				actualOffset = offset;
			} else {
				actualOffset = vorbisData->dataSize;
			}
			vorbisData->dataRead = (long)actualOffset;
			break;
		}
		case SEEK_CUR: {
			size_t spaceToEOF = vorbisData->dataSize - vorbisData->dataRead;
			ogg_int64_t actualOffset;
			if (offset < spaceToEOF) {
				actualOffset = offset;
			} else {
				actualOffset = spaceToEOF;
			}
			vorbisData->dataRead += static_cast<LONG>(actualOffset);
			break;
		}
		case SEEK_END: {
			vorbisData->dataRead = vorbisData->dataSize + 1;
			break;
		}
		default: {
			_ASSERT(false && "The 'origin' argument must be one of the following constants, defined in STDIO.H!\n");
			break;
		}
	};
	return 0;
}

int vorbis_close(void* data_src) {
	OGG_MEMORYFILE* oggStream = reinterpret_cast<OGG_MEMORYFILE*>(data_src);
	if (NULL != oggStream) {
		if (NULL != oggStream->dataPtr) {
			delete[] oggStream->dataPtr;
			oggStream->dataPtr = NULL;
		}

		delete data_src;
		return 0;
	}
	_ASSERT(false && "The 'data_src' argument (set by ov_open_callbacks) was NULL so memory was not cleaned up!\n");
	return EOF;
}

int vorbis_test_close(void* data_src) {
	OGG_MEMORYFILE* oggStream = reinterpret_cast<OGG_MEMORYFILE*>(data_src);
	if (NULL != oggStream) {
		if (NULL != oggStream->dataPtr) {
			oggStream->dataPtr = NULL;
		}
		delete data_src;
		return 0;
	}
	_ASSERT(false && "The 'data_src' argument (set by ov_open_callbacks) was NULL so memory was not cleaned up!\n");
	return EOF;
}

long vorbis_tell(void* data_src) {
	OGG_MEMORYFILE* oggStream = reinterpret_cast<OGG_MEMORYFILE*>(data_src);
	if (NULL == oggStream) {
		return -1L;
	}
	return oggStream->dataRead;
}

} // namespace KEP
