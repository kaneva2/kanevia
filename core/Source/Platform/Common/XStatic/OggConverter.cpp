///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "OggConverter.h"
#include "IClientEngine.h"
#include <Vfw.h>

#define WAV_CHUNK_SIZE 1024

// Apparently it has to be closed twice.
#define ANNOYING_CLOSE(x)                \
	{                                    \
		if (x) {                         \
			int cnt = AVIFileRelease(x); \
			if (cnt != 0) {              \
				AVIFileRelease(x);       \
			}                            \
			AVIFileExit();               \
		}                                \
	}

// This function converts wav files being imported into vorbis/ogg files in memory for playback.
// Original wav file will actually get uploaded.
BYTE* OggConverter::convert(const std::string& filePathWav, int& retBufLen, int& errorCode) {
	CMemFile out;

	short wavChannels;
	int wavSampleRate;
	short wavBitsPerSample;
	long wavDataSize;
	long curSample;

	// READ WAV FILE HERE
	IAVIFile* wavReader = NULL;
	AVIFileInit();
	HRESULT hr = AVIFileOpenW(&wavReader, KEP::Utf8ToUtf16(filePathWav).c_str(), OF_READ, NULL);
	if (hr != S_OK) {
		errorCode = OggConverter::UnableToOpenSourceFile;
		ANNOYING_CLOSE(wavReader);
		return NULL;
	}

	IAVIStream* pstream;
	if (AVIFileGetStream(wavReader, &pstream, streamtypeAUDIO, 0) != 0) {
		errorCode = OggConverter::UnrecognizedFileType;
		ANNOYING_CLOSE(wavReader);
		return NULL;
	}

	// Get Stream Number Of Samples
	wavDataSize = AVIStreamEnd(pstream);

	// Set Stream To First Sample
	curSample = AVIStreamStart(pstream);

	long formatSize;
	WAVEFORMATEX* formatHeader;
	hr = AVIStreamFormatSize(pstream, 0, &formatSize);
	if (hr != S_OK) {
		errorCode = OggConverter::UnrecognizedFileType;
		ANNOYING_CLOSE(wavReader);
		return NULL;
	}

	// header can be bigger (some wierd format with extra data) but not smaller
	if (formatSize < sizeof(WAVEFORMATEX))
		formatSize = sizeof(WAVEFORMATEX);

	formatHeader = (WAVEFORMATEX*)new BYTE[formatSize];
	if (!formatHeader) {
		errorCode = OggConverter::MemoryError;
		ANNOYING_CLOSE(wavReader);
		return NULL; // out mem
	}

	hr = AVIStreamReadFormat(pstream, 0, formatHeader, &formatSize);
	if (FAILED(hr)) {
		errorCode = OggConverter::UnrecognizedFileType;
		delete[] formatHeader;
		ANNOYING_CLOSE(wavReader);
		return NULL;
	}

	wavSampleRate = formatHeader->nSamplesPerSec;
	wavBitsPerSample = formatHeader->wBitsPerSample;
	wavChannels = formatHeader->nChannels;

	delete[] formatHeader;

	if (wavBitsPerSample == 0) {
		// Probably mp3 or variable sample size.  I can't handle this
		errorCode = OggConverter::UnSupportedCodec;
		ANNOYING_CLOSE(wavReader);
		return NULL;
	}

	// Nothing Wider Then Stereo
	if (wavChannels > 2) {
		errorCode = OggConverter::UnSupportedChannels; // force them to convert it to mono.
		ANNOYING_CLOSE(wavReader);
		return NULL;
	}

	// READ WAV FILE HERE
	ogg_stream_state os; /* take physical pages, weld into a logical
		                      stream of packets */
	ogg_page og; /* one Ogg bitstream page.  Vorbis packets are inside */
	ogg_packet op; /* one raw packet of data for decode */
	vorbis_info vi; /* struct that stores all the static vorbis bitstream
                          settings */
	vorbis_comment vc; /* struct that stores all the user comments */
	vorbis_dsp_state vd; /* central working state for the packet->PCM decoder */
	vorbis_block vb; /* local working space for packet->PCM decode */

	// Initialize Vorbis
	vorbis_info_init(&vi);

	// Encode At Highest quality
	int ret = vorbis_encode_init_vbr(&vi, wavChannels, wavSampleRate, 1.0f);
	if (ret) {
		// unsupported vorbis output mode
		errorCode = OggConverter::VorbisEncodeError;
		ANNOYING_CLOSE(wavReader);
		return NULL;
	}

	/* add a comment */
	vorbis_comment_init(&vc);
	vorbis_comment_add_tag(&vc, "ENCODER", "Kaneva Wav to Ogg Converter");

	/* set up the analysis state and auxiliary encoding storage */
	vorbis_analysis_init(&vd, &vi);
	vorbis_block_init(&vd, &vb);

	/* set up our packet->stream encoder */
	/* pick a random serial number; that way we can more likely build
	  chained streams just by concatenation */
	srand(time(NULL));
	ogg_stream_init(&os, rand());

	/* Vorbis streams begin with three headers; the initial header (with
	   most of the codec setup parameters) which is mandated by the Ogg
	   bitstream spec.  The second header holds any comment fields.  The
	   third header holds the bitstream codebook.  We merely need to
	   make the headers, then pass them to libvorbis one at a time;
	   libvorbis handles the additional Ogg bitstream constraints */
	ogg_packet header;
	ogg_packet header_comm;
	ogg_packet header_code;

	vorbis_analysis_headerout(&vd, &vc, &header, &header_comm, &header_code);
	ogg_stream_packetin(&os, &header); /* automatically placed in its own page */
	ogg_stream_packetin(&os, &header_comm);
	ogg_stream_packetin(&os, &header_code);

	/* This ensures the actual audio data will start on a new page, as per spec */
	int flushResult = ogg_stream_flush(&os, &og);
	while (flushResult != 0) {
		out.Write((void*)og.header, og.header_len);
		out.Write((void*)og.body, og.body_len);
		flushResult = ogg_stream_flush(&os, &og);
	}

	// Allocate Read Buffer
	int bytesPerSample = wavBitsPerSample / 8.0f;
	auto readbuffer = new signed char[WAV_CHUNK_SIZE * wavChannels * bytesPerSample + 1];

	// For Entire File
	int eos = 0;
	while (!eos) {
		// Read Sound Samples
		long bytesRead = 0;
		long samplesRead = 0;
		AVIStreamRead(
			pstream,
			curSample,
			WAV_CHUNK_SIZE * wavChannels,
			readbuffer,
			WAV_CHUNK_SIZE * wavChannels * bytesPerSample + 1,
			&bytesRead,
			&samplesRead);
		curSample = curSample + samplesRead;

		// Feed Vorbis Encoder With Floating Point Samples
		if (bytesRead > 0) {
			// Allocate Sample Buffers buffer[channels][samples]
			float** buffer = vorbis_analysis_buffer(&vd, WAV_CHUNK_SIZE);

			int outindex = 0;
			int samples = (int)samplesRead;
			for (int i = 0; i < samples; i++) {
				for (int j = 0; j < wavChannels; j++) {
					float val = 0.0;
					switch (bytesPerSample) {
						case 1: {
							unsigned char sample = readbuffer[(i * wavChannels) + j];
							val = ((float)sample - 128.0f) / 128.0f;
						} break;

						case 2: {
							val = ((readbuffer[2 * ((i * wavChannels) + j) + 1] << 8) | (0x00ff & (int)readbuffer[2 * ((i * wavChannels) + j)])) / 32768.f;
						} break;

						case 4: {
							memcpy(&val, &(readbuffer[4 * i * wavChannels]), 4); // 32 bit float
						} break;
					}
					buffer[j][outindex] = val;
				}
				outindex++;
			}

			/* tell the library how much we actually submitted */
			vorbis_analysis_wrote(&vd, samples);
		} else {
			/* end of file.  this can be done implicitly in the mainline,
			but it's easier to see here in non-clever fashion.
			Tell the library we're at end of stream so that it can handle
			the last frame and mark end of stream in the output properly */
			vorbis_analysis_wrote(&vd, 0);
		}

		/* vorbis does some data preanalysis, then divvies up blocks for
		    more involved (potentially parallel) processing.  Get a single
		    block for encoding now */
		while (vorbis_analysis_blockout(&vd, &vb) == 1) {
			/* analysis, assume we want to use bitrate management */
			vorbis_analysis(&vb, NULL);
			vorbis_bitrate_addblock(&vb);

			while (vorbis_bitrate_flushpacket(&vd, &op)) {
				/* weld the packet into the bitstream */
				ogg_stream_packetin(&os, &op);

				/* write out pages (if any) */
				while (!eos) {
					int result = ogg_stream_pageout(&os, &og);
					if (result == 0)
						break;
					out.Write((void*)og.header, og.header_len);
					out.Write((void*)og.body, og.body_len);
					if (ogg_page_eos(&og))
						eos = 1;
				}
			}
		}
	}

	// Free Read Buffer
	delete[] readbuffer;

	/* clean up and exit.  vorbis_info_clear() must be called last */
	ogg_stream_clear(&os);
	vorbis_block_clear(&vb);
	vorbis_dsp_clear(&vd);
	vorbis_comment_clear(&vc);
	vorbis_info_clear(&vi);

	/* ogg_page and ogg_packet structs always point to storage in
	 libvorbis.  They're never freed or manipulated directly */
	BYTE* wavMem = new BYTE[out.GetLength()];
	out.Seek(0, CFile::begin);
	out.Read(wavMem, out.GetLength());

	retBufLen = out.GetLength();
	out.Close();
	ANNOYING_CLOSE(wavReader);
	errorCode = 0;

	return wavMem;
}
