///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Benchmark.h"
#include <PreciseTime.h>
#include <Core/Util/Memory.h>
#include <Core/Util/WindowsAPIWrappers.h>
#include <LogHelper.h>
#include <lzma1505/LzmaLib.h>
#include <functional>
#include <vector>
#include <algorithm>
static LogInstance("LogInstance");
using namespace KEP;

float BenchmarkMemory(size_t nRuns) {
	if (nRuns == 0)
		return -1;

	struct Test {
		Test(std::string strName, std::function<void()> fn, size_t nOpsPerIteration) :
				m_strName(std::move(strName)), m_fnTest(std::move(fn)), m_nOpsPerIteration(nOpsPerIteration) {}
		std::string m_strName;
		std::function<void()> m_fnTest;
		size_t m_nOpsPerIteration;
		double m_dMinTime = DBL_MAX;
		double m_dMemSpeed = 0;
	};
	//static const size_t kArrayLength = 1 * 1024; // Number of elements in each array.
	static const size_t kRepeats = 1; // If testing small memory sizes, need to repeat multiple times because our clock tolerance is too small.
	static const size_t kArrayLength = 8 * 1024 * 1024; // Number of elements in each array.
	static const double dScalar = 3.14159265358979323846;
	std::unique_ptr<double[], AlignedDeleter<double>> pMemory(static_cast<double*>(_aligned_malloc((3 * kArrayLength + 4096) * sizeof(double), 4096)));
	double* a = pMemory.get();
	double* b = a + kArrayLength + 16; // + 4096 / (sizeof(double) * 4); // + 1/4 page
	double* c = b + kArrayLength + 16; // + 4096 / (sizeof(double) * 4);

	Test aTests[] = {
		Test(
			"Copy",
			std::function<void()>([a, c]() {
				for (size_t j = 0; j < kArrayLength; ++j)
					c[j] = a[j];
			}),
			2),
		/*
		Test{
			"Multiply",
			[b,c](){
				for( size_t j = 0; j < kArrayLength; ++j )
					c[j] = dScalar * b[j];
			},
			2,
		},
		Test{
			"Add",
			[a,b,c](){
				for( size_t j = 0; j < kArrayLength; ++j )
					c[j] = a[j] + b[j];
			},
			3,
		},
		Test{
			"Multiply-Add",
			[a,b,c](){
				for( size_t j = 0; j < kArrayLength; ++j )
					c[j] = b[j] + dScalar * a[j];
			},
			3,
		},
		*/
	};

	for (size_t i = 0; i < kArrayLength; ++i) {
		a[i] = 1.0;
		b[i] = 2.0;
		c[i] = 0.0;
	}

	for (auto& test : aTests) {
		for (size_t i = 0; i < nRuns; ++i) {
			double timeStart = GetCurrentTimeInSeconds();
			for (size_t j = 0; j < kRepeats; ++j)
				test.m_fnTest();
			double timeFinish = GetCurrentTimeInSeconds();
			bool bSkipFirst = nRuns > 1;
			if (i != 0 || !bSkipFirst) {
				test.m_dMinTime = (std::min)(test.m_dMinTime, timeFinish - timeStart);
			}
		}
	}

	for (auto& test : aTests) {
		test.m_dMemSpeed = test.m_nOpsPerIteration * kArrayLength * kRepeats * sizeof(double) / (1024 * 1024 * test.m_dMinTime);
		LogInfo("Test " << test.m_strName << ": min = " << test.m_dMinTime << " Memory bandwidth: " << test.m_dMemSpeed << " MB/s");
	}

	return aTests[0].m_dMemSpeed;
}

float BenchmarkFloatingPoint(size_t nRuns) {
	if (nRuns == 0)
		return -1;

	static const size_t kArrayLength = 2 * 1024 * 1024;
	//std::unique_ptr<float[], AlignedDeleter<float>> pMemory(static_cast<float*>(_aligned_malloc(kArrayLength* sizeof(float), 4096)));
	//float* pData = pMemory.get();
	std::vector<float> aMemory(kArrayLength);
	float* pData = aMemory.data();

	for (size_t i = 0; i < kArrayLength; ++i)
		pData[i] = i % 256;

	static const size_t knAccumulators = 16;
	float aAccumulators[knAccumulators] = { 0 };
	double timeMin = DBL_MAX;
	for (size_t iRun = 0; iRun < nRuns; ++iRun) {
		double timeStart, timeFinish;
		timeStart = GetCurrentTimeInSeconds();
		for (size_t i = 0; i < kArrayLength - knAccumulators; i += knAccumulators) {
			float m = 0.5f;
			size_t iAccumulator;
			iAccumulator = 0;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 1;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 2;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 3;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 4;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 5;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 6;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 7;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 8;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 9;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 10;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 11;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 12;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 13;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 14;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
			iAccumulator = 15;
			aAccumulators[iAccumulator] = aAccumulators[iAccumulator] * m + pData[i + iAccumulator];
		}
		timeFinish = GetCurrentTimeInSeconds();
		timeMin = (std::min)(timeMin, timeFinish - timeStart);
	}

	// Log calculated value to prevent compiler from optimizing it away.
	float value = 0;
	for (size_t i = 0; i < knAccumulators; ++i)
		value += aAccumulators[i];

	size_t nFloatOps = 2 * kArrayLength;
	float fMFLOPS = nFloatOps * 1e-6 / timeMin;
	LogInfo("FPU benchmark: (value=" << value << ") " << fMFLOPS << " MFLOPS");
	return fMFLOPS;
}

float BenchmarkCompress(size_t nRuns) {
	if (nRuns == 0)
		return -1;

	//const wchar_t* pwzFile = L"lzmalib.dll";
	//const wchar_t* pwzFile = L"libcollada14dom21.dll";
	const wchar_t* pwzFile = L"libxml2-2.dll";
	FileHandleWrapper hFile(CreateFile(pwzFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL));
	if (!hFile)
		return -1;
	DWORD nFileBytes = GetFileSize(hFile.GetHandle(), nullptr);
	std::vector<unsigned char> aFileBytes;
	aFileBytes.resize(nFileBytes);
	DWORD nBytesRead = 0;
	if (!ReadFile(hFile.GetHandle(), aFileBytes.data(), aFileBytes.size(), &nBytesRead, nullptr))
		return -1;
	unsigned char lzmaProps[LZMA_PROPS_SIZE];
	size_t propsSize = sizeof(lzmaProps);
	std::vector<unsigned char> aCompressedBytes;
	aCompressedBytes.resize(nFileBytes);
	size_t nCompressedBytes = aCompressedBytes.size();
	int compressResult = LzmaCompress(aCompressedBytes.data(), &nCompressedBytes, aFileBytes.data(), aFileBytes.size(), lzmaProps, &propsSize, -1, 0, -1, -1, -1, -1, -1);
	if (compressResult != SZ_OK)
		return -1;

	double timeMin = DBL_MAX;
	std::vector<unsigned char> aDecompressedBytes;
	aDecompressedBytes.resize(nFileBytes);
	size_t nDecompressedBytes = aDecompressedBytes.size();
	for (size_t i = 0; i < nRuns; ++i) {
		double timeStart, timeFinish;
		timeStart = GetCurrentTimeInSeconds();
		int uncompressResult = LzmaUncompress(aDecompressedBytes.data(), &nDecompressedBytes, aCompressedBytes.data(), &nCompressedBytes, lzmaProps, propsSize);
		if (uncompressResult != SZ_OK)
			return -1;
		timeFinish = GetCurrentTimeInSeconds();
		timeMin = (std::min)(timeMin, timeFinish - timeStart);
	}

	LogInfo("LZMA decompress time: " << timeMin);
	return timeMin * 1000.0f;
}
