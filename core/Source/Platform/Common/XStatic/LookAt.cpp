///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "common/include/LookAt.h"
#include "common/include/IMemSizeGadget.h"
#include "Common/include/MemSizeSpecializations.h"

namespace KEP {

void LookAtInfo::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(targetBone);
		pMemSizeGadget->AddObject(pForwardDirectionMatrix);
	}
}

} // namespace KEP