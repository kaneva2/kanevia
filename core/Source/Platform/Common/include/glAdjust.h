///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common\include\KEPConstants.h"
#include "Core/Util/HighPrecisionTime.h"

class RenderMetrics {
public:
	static void Log();

	static void FrameUpdate();

	static uint_fast32_t GetFrameCount();

	static TimeMs GetFrameTimestampMs();

	static TimeMs GetFrameTimeMs();

	static double GetFrameTimeRatio();

	static double GetFramesPerSec(bool filter);
};