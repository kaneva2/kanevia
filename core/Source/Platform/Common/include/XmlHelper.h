///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "tinyxml/tinyxml.h"

#include <string>

inline const char* XmlGetAttr(const TiXmlElement* root, const std::string& tag) {
	if (!root)
		return NULL;
	return root->Attribute(tag.c_str());
}

inline int XmlGetAttrAsInt(const TiXmlElement* root, const std::string& tag, int valDefault = 0) {
	auto pText = XmlGetAttr(root, tag);
	return pText ? atoi(pText) : valDefault;
}

inline std::string XmlGetAttrAsStr(const TiXmlElement* root, const std::string& tag, const std::string& valDefault = "") {
	auto pText = XmlGetAttr(root, tag);
	return pText ? pText : valDefault;
}

inline const TiXmlElement* XmlGetFirstChildElem(const TiXmlElement* root, const std::string& tag) {
	if (!root)
		return NULL;
	return root->FirstChildElement(tag.c_str());
}

inline const TiXmlText* XmlGetFirstChildText(const TiXmlElement* root, const std::string& tag) {
	auto pElem = XmlGetFirstChildElem(root, tag);
	if (!pElem)
		return NULL;
	auto pChild = pElem->FirstChild();
	if (!pChild)
		return NULL;
	return pChild->ToText();
}

inline int XmlGetFirstChildTextAsInt(const TiXmlElement* root, const std::string& tag, int valDefault = 0) {
	auto pText = XmlGetFirstChildText(root, tag);
	return pText ? atoi(pText->Value()) : valDefault;
}

inline std::string XmlGetFirstChildTextAsStr(const TiXmlElement* root, const std::string& tag, const std::string& valDefault = "") {
	auto pText = XmlGetFirstChildText(root, tag);
	return pText ? pText->Value() : valDefault;
}

inline int XmlGetFirstChildTextAsInt(const std::string& xml, const std::string& tag, int valDefault = 0) {
	if (xml.empty())
		return valDefault;

	// Parse Content Metadata Xml
	TiXmlDocument doc;
	doc.Parse(xml.c_str());
	if (doc.Error())
		return valDefault;

	// Get Root Element
	const TiXmlElement* root = doc.RootElement();
	if (!root)
		return valDefault;

	// Get First Child Text As Int
	return XmlGetFirstChildTextAsInt(root, tag, valDefault);
}

inline std::string XmlGetFirstChildTextAsStr(const std::string& xml, const std::string& tag, const std::string& valDefault = "") {
	if (xml.empty())
		return valDefault;

	// Parse Content Metadata Xml
	TiXmlDocument doc;
	doc.Parse(xml.c_str());
	if (doc.Error())
		return valDefault;

	// Get Root Element
	const TiXmlElement* root = doc.RootElement();
	if (!root)
		return valDefault;

	// Get First Child Text As String
	return XmlGetFirstChildTextAsStr(root, tag, valDefault);
}
