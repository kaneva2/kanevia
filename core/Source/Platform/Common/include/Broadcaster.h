///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Pointer.h"
#include "Reference.h"
#include <set>

namespace KEP {

class Broadcaster;
class IListener;

class Broadcaster {
public:

	Broadcaster();
	virtual ~Broadcaster();

	bool Attach(IListener* pListener);
	bool Detach(IListener* pListener);

	virtual void Broadcast();

protected:
	typedef std::set<Pointer<IListener>> ListenerContainer;

	ListenerContainer m_listeners;
}; // class..Broadcaster

class IListener : public IReference {
public:

	IListener();
	virtual ~IListener() = 0;

	virtual void Update(Broadcaster* pBroadcaster) = 0;
}; // class..IListener
} // namespace KEP
