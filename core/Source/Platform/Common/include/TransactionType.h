#pragma once

namespace KEP {

// these are are to pass to SQL, use these so can compare with ==
// generated by
/*
select concat( "const unsigned short CASH_", trans_text, " = ", transaction_type, ";" )
from `kaneva`.`transaction_type`
order by transaction_type;
*/
enum class TransactionType {
	BOUGHT_CREDITS = 1,
	TRADE = 2,
	BOUGHT_ITEM = 3,
	SOLD_ITEM = 4,
	LOOT = 5,
	MISC = 6,
	REFUND = 7,
	OPENING_BALANCE = 8,
	BOUGHT_HOUSE_ITEM = 9,
	QUEST = 10,
	BANKING = 11,
	HOUSE_BANKING = 12,
	BOUGHT_GIFT = 13,
	ROYALITY = 14,
	SPECIAL = 15,
	COVER_CHARGE = 16,
	COMMISSION = 17,
	ITEM_PICKUP = 18,
	LEVEL_REWARD = 19,
};

} // namespace KEP