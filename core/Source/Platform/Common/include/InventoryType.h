/******************************************************************************
 InventoryType.h

 Copyright (c) 2004-2007 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <KEPMacros.h>

namespace KEP {

// inventory types, all use high byte of short
// each type is a separate stack in inv, if stackable

const static short IT_NORMAL = 0x0100; // bought w/ money
const static short IT_GIFT = 0x0200; // given as gift
const static short IT_UNLIMITED = 0x0400; // inventory item with no quantity limit (smart object)
const static short IT_INFINITE = 0x0800; // inventory item with no quantity limit

const static short IT_ALL = IT_NORMAL | IT_GIFT | IT_UNLIMITED | IT_INFINITE;

} // namespace KEP

