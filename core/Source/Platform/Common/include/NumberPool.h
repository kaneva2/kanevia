/******************************************************************************
 NumberPool.h

 Copyright (c) 2004-2012 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include <map>
#include <set>
#include <vector>
#include "common/include/kepexception.h"

/**
 * Generalized Pool.  Pool encompasses the paradigm of checking an object/value out
 * of the pool, such that the value will not be available for checkout until it
 * has been checked back into the pool.
 */
template <typename PooledT>
class Pool {
public:
	Pool() {}
	virtual ~Pool() {}

	/**
	 *	Checkout an instance from the pool.
	 */
	virtual PooledT checkout() = 0;

	/**
	 * Checkin an instance from the pool.  Note instance must have previously
	 * checked out from the pool.
	 */
	virtual void checkin(PooledT value) = 0;
};

/**
 * Construct for pooling a range of values.  Instances of this class warrantee
 * that any value checked out from the pool will not be available for checkout until
 * it has been checked back into the pool.  Instances of this class make no warrantee 
 * of order or contiguity of values.
 */
template <typename OrdinalT>
class RangedPool : public Pool<OrdinalT> {
public:
	/**
	 * Construct RangedPool with floor and ceiling values that define
	 * the range of possible values
	 */
	RangedPool(OrdinalT rangeFloor, OrdinalT rangeCeiling) :
			_floor(rangeFloor), _next(rangeFloor), _ceiling(rangeCeiling) {}

	/**
	 * Checkout a value from the pool
	 */
	OrdinalT checkout() {
		OrdinalT ret = 0;

		// NOTE: Depending on the order of the next two code blocks changes the
		// behavior of the pool as follows:
		//  1) Looking to allocate a new value makes the pool tend to it's maximum
		//     size, while decreasing the frequency of any single value checked out
		//     from the pool.
		//	2) Looking to reuse an existing value first, causes the pool to
		//     tend toward smaller size
		//
		// It may be that we want to make this configurable as both behaviours
		// have value.
		//
		// Attempt allocate a new value first.
		//
		if (_next <= _ceiling) {
			ret = _next++;
			_inUse.insert(ret);
			return ret;
		}

		// No room to allocate more.  Reuse.
		//
		if (_available.size() > 0) {
			ret = _available.back();
			_available.pop_back();
			_inUse.insert(ret);
			return ret;
		}

		// throw an exception.  There are no values unused at this point
		// and we cannot allocate any more.
		//
		throw new RangeException("", 0);
	}

	/**
	 * Checkin a value previously checked out from the pool
	 * @return void
	 */
	void checkin(OrdinalT ordinal) {
		_inUse.erase(_inUse.find(ordinal));
		_available.push_back(ordinal);
	}

	virtual ~RangedPool() {}

private:
	typedef vector<unsigned short> PortVecT;
	typedef set<unsigned short> PortMapT;
	unsigned short _next;
	unsigned short _ceiling;
	unsigned short _floor;
	PortVecT _available;
	PortMapT _inUse;
};

/**
 * Same as RangedPool, but thread safe.
 */
template <typename OrdinalT>
class ConcurrentNumberPool : public Pool<OrdinalT> {
public:
	ConcurrentNumberPool(OrdinalT floor, OrdinalT ceiling) :
			_pool(floor, ceiling) {}

	virtual ~ConcurrentNumberPool() {}

	OrdinalT checkout() {
		jsFastLock lock(_self);
		return _pool.checkout();
	}

	void checkin(OrdinalT val) {
		jsFastLock lock(_self);
		_pool.checkin(val);
	}

private:
	jsFastSync _self;
	RangedPool<OrdinalT> _pool;
};
