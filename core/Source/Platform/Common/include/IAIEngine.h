/******************************************************************************
 IAIEngine.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "IGame.h"

namespace KEP {

class IAIEngine : public IGame {
public:
	virtual IGetSet* GetEnvironment() = 0;

protected:
	IAIEngine() :
			IGame(ENGINE_AI) {}
};

} // namespace KEP
