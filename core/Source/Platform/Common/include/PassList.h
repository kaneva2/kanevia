///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "PassGroups.h"

#include <vector>

namespace KEP {

typedef int PassId;

class PlayerPassList;
class IMemSizeGadget;

/// \brief a class to track the global pass list for a game
class PassLists {
public:
	/// \brief flag that indicates no zone checking for using these items
	static const UINT UseAnyWhereFlag = 1;

	typedef std::pair<PassId, UINT> PassListVectItem;
	typedef std::vector<PassListVectItem> PassListsVect;

	/// \brief init lists
	///
	/// this will create a TLS entry for passlist, optionally
	/// setting it to the value copied in.
	///
	/// \param copy if null will allocate one, otherwise, sets it to this one
	static void Initialize(PassLists *copy = 0);

	/// \brief free allocated memory
	///
	/// DANGER this must be called only on one thread, if you call it
	/// on a thread that used the copy param on Init, it will delete it twice.
	/// If you don't mind a bit of leaking, don't call this.
	static void DeInitialize();

	/// \brief add or update as pass list item
	///
	/// \param id the pass list id from db
	/// \param flags the flags about it.  see fns below
	static void UpdatePassList(PassId id, UINT flags);

	/// \brief use this pass id in any zone
	///
	/// \return true if UseAnywhereFlag is set for this id
	static bool UseAnywhere(PassId id);
};

/*
	class to track Passes on items, zones, and players
*/
class PassList : public GetSet {
public:
	typedef std::vector<PassId> PassIds;

	PassList() {}
	PassList(const PassList &src) { operator=(src); }

	std::string toString() const {
		std::stringstream ret;
		char delim = ' ';
		ret << "[";

		for (PassIds::size_type i = 0; i < m_ids.size(); i++) {
			ret << delim << m_ids[i];
			delim = ',';
		}
		ret << "]";
		return ret.str();
	}

	PassList &operator=(const PassList &src) {
		if (this != &src) {
			m_ids.clear();
			m_ids.assign(src.m_ids.begin(), src.m_ids.end());
		}
		return *this;
	}

	inline bool hasPass(IN PassId id) const;
	inline void addPass(IN PassId id);
	inline bool playerAccessAllowed(IN const PlayerPassList *accessor) const;
	inline bool empty() const;
	inline bool removePass(IN PassId id);

	PassIds &ids() { return m_ids; }

	// GetSet overrides to access our items
	// index 0 = count, then items in array
	virtual ULONG &GetULONG(IN ULONG index);
	virtual double GetNumber(IN ULONG index) { return (double)GetULONG(index); }

	// allow it to be serialize to MFC
	void SerializeOut(CArchive &ar);
	// allocates if not empty, otherwise returns null
	inline static PassList *SerializeIn(CArchive &ar);

	// HELPERS
	static bool canUseItemInZone(IN PassList *item, IN PassList *zone) {
		if (item == 0 || item->empty())
			return true; // item has no restrictions so it can be used anywhere, allow it

		// 6-09 changed to require all passes, and support UseAnywhere flag
		for (PassIds::size_type i = 0; i < item->m_ids.size(); i++) {
			if (PassLists::UseAnywhere(item->m_ids[i]))
				continue;

			if (zone != 0 && !zone->empty()) {
				if (!zone->hasPass(item->m_ids[i]))
					return false;
			} else {
				return false; // item has restrictions, zone has none, disallow
			}
		}
		return true; // must have matched all
	}

	static bool canPlayerUseItem(IN PlayerPassList *player, IN PassList *item) {
		return playerAccessAllowed(player, item);
	}

	static bool canPlayerGotoZone(IN PlayerPassList *player, IN PassList *zone) {
		return playerAccessAllowed(player, zone);
	}

	static bool canPlayerSetPassOnZone(IN PlayerPassList *player, IN PassList *zone) {
		return playerAccessAllowed(player, zone);
	}

	static bool canPlayerBuyItem(IN PlayerPassList *player, IN PassList *item) {
		return playerAccessAllowed(player, item);
	}

	static bool canPlayerAcceptItemTrade(IN PlayerPassList *player, IN PassList *item) {
		return playerAccessAllowed(player, item);
	}

	DECLARE_GETSET

public:
	// returns true if allowed
	inline static bool playerAccessAllowed(IN const PlayerPassList *accessor, IN const PassList *accessee);

	void GetMemSize(IMemSizeGadget *pMemSizeGadget) const;

	PassIds m_ids;
};

/// PassList class that has expiration for players
class PlayerPassList : public PassList {
public:
	typedef std::vector<time_t> Expirations;

	PlayerPassList() {}
	PlayerPassList(const PlayerPassList &src) { operator=(src); }

	PlayerPassList &operator=(const PlayerPassList &src) {
		if (this != &src) {
			m_ids.clear();
			m_ids.assign(src.m_ids.begin(), src.m_ids.end());
			m_expirations.clear();
			m_expirations.assign(src.m_expirations.begin(), src.m_expirations.end());
		}
		return *this;
	}

	inline void addPass(IN PassId id, IN time_t expirationDate);
	inline bool removePass(IN PassId id);

	inline bool hasActivePass(IN PassId id) const;
	const Expirations &expirations() const { return m_expirations; }

	void addPass(IN PassId id); // not virtual, but same, fn w/o impl to get compiler warn if try to use it

protected:
	Expirations m_expirations;
};

inline bool PassList::empty() const {
	return m_ids.empty();
}

inline bool PassList::playerAccessAllowed(IN const PlayerPassList *accessor, IN const PassList *accessee) {
	if (accessee == 0 || accessee->empty())
		return true; // no restrictions on access

	return accessee->playerAccessAllowed(accessor);
}

inline bool PassList::hasPass(IN PassId id) const {
	for (PassIds::size_type i = 0; i < m_ids.size(); i++) {
		if (m_ids[i] == id)
			return true;
	}

	return false;
}

inline bool PlayerPassList::hasActivePass(IN PassId id) const {
	jsAssert(m_expirations.size() == m_ids.size());

	for (PassIds::size_type i = 0; i < m_ids.size(); i++) {
		if (m_ids[i] == id) {
			return m_expirations[i] >= time(0);
		}
	}

	return false;
}

inline bool PassList::removePass(IN PassId id) {
	for (PassIds::iterator i = m_ids.begin(); i != m_ids.end(); i++) {
		if (*i == id) {
			m_ids.erase(i);
			return true;
		}
	}

	return false;
}

inline bool PlayerPassList::removePass(IN PassId id) {
	jsAssert(m_expirations.size() == m_ids.size());
	Expirations::iterator j = m_expirations.begin();
	PassIds::iterator i = m_ids.begin();
	for (; i != m_ids.end(); i++, j++) {
		if (*i == id) {
			m_expirations.erase(j);
			m_ids.erase(i);
			return true;
		}
	}

	return false;
}

inline void PassList::addPass(IN PassId id) {
	m_ids.push_back(id);
}

inline void PlayerPassList::addPass(IN PassId id, IN time_t expirationDate) {
	m_ids.push_back(id);
	m_expirations.push_back(expirationDate);
}

inline bool PassList::playerAccessAllowed(IN const PlayerPassList *accessor) const {
	if (empty())
		return true; // we have no restrictions

	if (accessor != 0 && !accessor->empty()) {
		for (PassIds::size_type i = 0; i < m_ids.size(); i++) {
			// player must have *all* the passes of item/zone
			if (!accessor->hasActivePass(m_ids[i]))
				return false;
		}
		return true;
	} else {
		return false; // we have passes, and accessor doesn't
	}
}

inline void PassList::SerializeOut(CArchive &ar) {
	if (ar.IsStoring()) {
		ar << m_ids.size();
		for (PassIds::iterator i = m_ids.begin(); i != m_ids.end(); i++) {
			ar << *i;
		}
	} else {
		jsAssert(false);
	}
}

inline PassList *PassList::SerializeIn(CArchive &ar) {
	if (ar.IsStoring()) {
		jsAssert(false);
	} else {
		PassIds::size_type count;
		PassId id;
		ar >> count;
		if (count > 0) {
			PassList *ret = new PassList;
			for (PassIds::size_type i = 0; i < count; i++) {
				ar >> id;
				ret->addPass(id);
			}
			return ret;
		}
	}
	return 0;
}

} // namespace KEP
