///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include "KEPCommon.h"
#include "KEP/Util/KEPUtil.h"

namespace KEP {
class PreciseTimeNative {
public:
	PreciseTimeNative() {}
	PreciseTimeNative operator-(const PreciseTimeNative& other) const {
		PreciseTimeNative ptn;
		ptn.m_time = m_time - other.m_time;
		return ptn;
	}
	PreciseTimeNative& operator-=(const PreciseTimeNative& other) {
		m_time -= other.m_time;
		return *this;
	}
	PreciseTimeNative operator+(const PreciseTimeNative& other) const {
		PreciseTimeNative ptn;
		ptn.m_time = m_time + other.m_time;
		return ptn;
	}
	PreciseTimeNative& operator+=(const PreciseTimeNative& other) {
		m_time += other.m_time;
		return *this;
	}

	double AsSeconds() const { return m_time * s_dInverseFrequencySeconds; }

	static PreciseTimeNative FromSeconds(double dSeconds) { return PreciseTimeNative(dSeconds * s_iFrequencySeconds); }
	static PreciseTimeNative FromCurrentTime() {
		PreciseTimeNative ptn;
		QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&ptn.m_time));
		return ptn;
	}

private:
	explicit PreciseTimeNative(int64_t time) :
			m_time(time) {}

private:
	static KEPUTIL_EXPORT int64_t s_iFrequencySeconds;
	static KEPUTIL_EXPORT double s_dInverseFrequencySeconds;

	int64_t m_time;
};

inline PreciseTimeNative GetCurrentTimeNative() {
	return PreciseTimeNative::FromCurrentTime();
}

inline double GetCurrentTimeInSeconds() {
	return GetCurrentTimeNative().AsSeconds();
}

inline double GetThreadTimeInSeconds() {
	FILETIME timeCreation, timeExit, timeKernel, timeUser;
	if (!GetThreadTimes(GetCurrentThread(), &timeCreation, &timeExit, &timeKernel, &timeUser))
		return 0;
	int64_t iTimeKernel = (int64_t(timeKernel.dwHighDateTime) << 32) + timeKernel.dwLowDateTime;
	int64_t iTimeUser = (int64_t(timeUser.dwHighDateTime) << 32) + timeUser.dwLowDateTime;
	int64_t iTimeCombined = iTimeUser + iTimeKernel;
	double dTimeCombined = 1e-7 * iTimeCombined;
	return dTimeCombined;
}

class PreciseTimer {
	PreciseTimer() { Start(); }
	PreciseTimer(bool bStartImmediately) {
		if (bStartImmediately)
			Start();
	}

	void Start() {
		m_Time = GetCurrentTimeNative();
	}

	double GetElapsedTimeSeconds() const {
		return (GetCurrentTimeNative() - m_Time).AsSeconds();
	}

	PreciseTimeNative GetElapsedTimeNative() const {
		return (GetCurrentTimeNative() - m_Time);
	}

private:
	PreciseTimeNative m_Time;
};

} // namespace KEP
