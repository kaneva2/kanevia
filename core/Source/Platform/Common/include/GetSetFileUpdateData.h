/******************************************************************************
 GetSetFileUpdateData.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "GetSetUpdateData.h"
#include <afxdlgs.h> // MFC-specific!

namespace KEP {

/***********************************************************
CLASS

	GetSetFileUpdateData
	
	helper for GetSets that are gsFile

DESCRIPTION

***********************************************************/
class KEP_EXPORT GetSetFileUpdateData : public GetSetUpdateData {
public:
	// see help for CFileDialog for explanation of all but first parameter
	GetSetFileUpdateData(GetSet* item, const char* lpszDefExt = NULL,
		const char* lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		const char* lpszFilter = NULL,
		CWnd* pParentWnd = NULL,
		DWORD dwSize = 0) :
			GetSetUpdateData(item),

			m_DefExt(lpszDefExt ? lpszDefExt : ""),
			m_FileName(lpszFileName ? lpszFileName : ""),
			m_dwFlags(dwFlags),
			m_Filter(lpszFilter ? lpszFilter : ""),
			m_pParentWnd(pParentWnd),
			m_dwSize(dwSize) {}

	///---------------------------------------------------------
	// vfn to allow custom editing of the item
	//
	// [Returns]
	// 		TRUE if the object was saved
	inline virtual BOOL CustomEdit();

	const string& selectedFile() const { return m_selectedFile; }

protected:
	string m_selectedFile;
	string m_DefExt;
	string m_FileName;
	DWORD m_dwFlags;
	string m_Filter;
	CWnd* m_pParentWnd;
	DWORD m_dwSize;
};

inline BOOL GetSetFileUpdateData::CustomEdit() {
	CFileDialog dlg(TRUE, m_DefExt.c_str(), m_FileName.c_str(), m_dwFlags, m_Filter.c_str(), m_pParentWnd, m_dwSize);
	if (dlg.DoModal() != IDOK)
		return FALSE;
	else {
		m_selectedFile = dlg.GetPathName();
		return TRUE;
	}
}

} // namespace KEP

