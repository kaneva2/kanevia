#pragma once

#include "IKEPObject.h"
#include "EngineTypes.h"

namespace KEP {

class IGame : public IKEPObject {
public:
	eEngineType EngineType() { return m_engineType; }
	virtual ~IGame() {}

protected:
	IGame(eEngineType type) :
			m_engineType(type) {}

	eEngineType m_engineType;
};

} // namespace KEP
