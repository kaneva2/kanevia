///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <d3d9.h>
#include <d3dX9.h>
#include <d3dx9math.h>
#include "matrixarb.h"
#include "GetSet.h"
#include "Glids.h"

#include "common\include\CoreStatic\CBoneAnimationNaming.h"
#include "common\include\CoreStatic\AnimSettings.h"

namespace KEP {

class CArmedInventoryProxy;
class CDynamicPlacementObj;
class CMovementObj;
class CArmedInventoryObj;

class CMenuGameObject : public GetSet {
public:
	CMenuGameObject();
	CMenuGameObject(CMovementObj *game_obj);
	CMenuGameObject(int game_object_index, int animation_index);
	CMenuGameObject(std::unique_ptr<CDynamicPlacementObj> dyn_obj);
	CMenuGameObject(CArmedInventoryProxy *equip_obj);
	virtual ~CMenuGameObject();

	void Init();

	CMovementObj *GetMovementObject() { return m_pMO; }
	CDynamicPlacementObj *GetDynamicObject() { return m_pDPO.get(); }
	CArmedInventoryObj *GetEquippableObject() { return m_pAIO; }

	Vector3f GetPosition() const { return m_position; }

	Vector3f GetRotation() const { return m_rotation; } // in degrees

	Vector3f GetScaling() const { return m_scaling; }
	Vector3f GetViewportPosition() const { return m_vp_position; } // only x,y are meaningful
	Vector3f GetViewportDimensions() const { return m_vp_dimensions; } // only x,y are meaningful
	RECT GetViewportRect() const {
		auto pos = GetViewportPosition();
		auto dim = GetViewportDimensions();
		RECT rect;
		rect.left = pos.x;
		rect.right = pos.x + dim.x;
		rect.top = pos.y;
		rect.bottom = pos.y + dim.y;
		return rect;
	}
	D3DCOLOR GetViewportBackgroundColor() const { return m_vp_bg_color; }
	D3DCOLORVALUE GetViewportLightColor() const { return m_vp_light_color; }
	D3DXVECTOR4 GetViewportLightAmbient() const { return m_vp_light_ambient; }
	Vector3f GetViewportLightDirection() const { return m_vp_light_dir; }
	bool GetOnTop() const { return m_alwaysOnTop; }
	float GetDynamicObjZoomFactor() const { return m_dynObjZoomFactor; }

	void SetPosition(Vector3f position) { m_position = position; }
	void SetRotation(Vector3f rotation) { m_rotation = rotation; }
	void SetScaling(Vector3f scaling) { m_scaling = scaling; }
	void SetViewportPosition(Vector3f position) { m_vp_position = position; }
	void SetViewportDimensions(Vector3f dimensions) { m_vp_dimensions = dimensions; }
	void SetViewportBackgroundColor(unsigned char r, unsigned char g, unsigned char b) { m_vp_bg_color = D3DCOLOR_ARGB(255, r, g, b); }
	void SetViewportLightColor(unsigned char r, unsigned char g, unsigned char b) { m_vp_light_color = D3DXCOLOR((r / 255.0f), (g / 255.0f), (b / 255.0f), 1.0f); }
	void SetViewportLightAmbient(unsigned char r, unsigned char g, unsigned char b) { m_vp_light_ambient = D3DXVECTOR4((r / 255.0f), (g / 255.0f), (b / 255.0f), 1.0f); }
	void SetViewportLightDirection(float x, float y, float z) { m_vp_light_dir = Vector3f(x, y, z); };
	void SetOnTop(bool onTop) { m_alwaysOnTop = onTop; }
	void SetDynamicObjZoomFactor(float zoom) { m_dynObjZoomFactor = zoom; }

	void setCurrentAnimationByType(eAnimType animType, int animVer = ANIM_VER_ANY);

	bool setPrimaryAnimationSettings(const AnimSettings &animSettings);

	// By setting a time the menu game object's animation will freeze frame at that time.
	// Setting this to -1 disables this freeze frame and plays the animation as normal.
	void SetAnimTimeMs(TimeMs timeMs) { m_animTimeMs = timeMs; }
	TimeMs GetAnimTimeMs() { return m_animTimeMs; }

	void Render(Matrix44f world_matrix, bool onTop);

protected:
	CMovementObj *m_pMO;
	std::unique_ptr<CDynamicPlacementObj> m_pDPO;
	CArmedInventoryObj *m_pAIO;

	Vector3f m_position;
	Vector3f m_rotation; // in degrees
	Vector3f m_scaling;
	Vector3f m_vp_position;
	Vector3f m_vp_dimensions;
	D3DCOLOR m_vp_bg_color;
	D3DCOLORVALUE m_vp_light_color;
	D3DXVECTOR4 m_vp_light_ambient;
	Vector3f m_vp_light_dir;
	bool m_alwaysOnTop;
	float m_dynObjZoomFactor;

	TimeMs m_animTimeMs;

	AnimSettings m_animSettings;

	void LoadReMesh(CMovementObj *pMovementObj, double aspect, double fovy, Matrix44f &newViewMatrix, Matrix44f &newProjMatrix, Matrix44f &camWorld);
	void LoadReMesh(CDynamicPlacementObj *pDynamicObjPlacement, double aspect, double fovy, Matrix44f &newViewMatrix, Matrix44f &newProjMatrix, Matrix44f &camWorld);
	void LoadReMesh(CArmedInventoryObj *pEquipObj, double aspect, double fovy, Matrix44f &newViewMatrix, Matrix44f &newProjMatrix, Matrix44f &camWorld);

public:
	IGetSet *Values() { return this; }
	Matrix44f m_dynObMatrix;

	DECLARE_GETSET
};

class CMenuGameObjects {
protected:
	CMenuGameObjects() {}

public:
	static CMenuGameObjects *GetInstance();

	virtual ~CMenuGameObjects();

	void Render(Matrix44f world_matrix, bool onTop);
	void Add(CMenuGameObject *game_obj);
	void Remove(CMenuGameObject *game_obj);
	void RemoveAll();

	void GetAllMovementObjects(std::set<CMovementObj *> &movementObjs);

protected:
	typedef std::vector<CMenuGameObject *> MenuGameObjVector;
	typedef MenuGameObjVector::iterator MenuGameObjIter;
	MenuGameObjVector m_game_objects;

private:
	static CMenuGameObjects *m_pInstance;
};

} // namespace KEP