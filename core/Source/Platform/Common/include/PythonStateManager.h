/******************************************************************************
 PythonStateManager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#ifdef PYTHONEVENTHANDLER_EXPORTS
#define PSM_EXPORT __declspec(dllexport)
#else
#define PSM_EXPORT __declspec(dllimport)
#endif
#include <log4cplus/logger.h>

extern "C" {

//----------------------------------------------------------------------------
// call before first use of Python for your high-level activity such as
// an engine thread or the main thread
//
// first time called, Python is initalized
// each time this is called a count is incremented to help
// determine when to de-init Python
//
// [in] useGlobalState if true will reuse the global thread state, if
// there is not already a thread state
PSM_EXPORT bool PSM_Initialize(bool useGlobalState);

//----------------------------------------------------------------------------
// call when finished with your activity, This must be
// matched to a PSM_Initialize call.  See above
PSM_EXPORT void PSM_Uninitialize();

//----------------------------------------------------------------------------
// call before making Python calls to set the thread state properly
// Initialization is performed once
//
// [in] reInit true if you are reinitializing Python
PSM_EXPORT bool PSM_Begin(IN bool reInit);

//----------------------------------------------------------------------------
// call when finished with making Python calls, This must be
// matched to a PSM_Begin call.  See above
PSM_EXPORT void PSM_End();

// common helpers

// log the Python error, gets call stack
PSM_EXPORT void logPythonErr(IN log4cplus::Logger& logger, IN const char* msg, IN bool warn);
}

// helper classes to call the DLL entry points, calling
// the symmetrical function on destroy
namespace PSM {

class Manager {
public:
	Manager(bool useGlobalState = false) {
		ok = PSM_Initialize(useGlobalState);
	}
	~Manager() {
		PSM_Uninitialize();
	}
	bool ok;
};

class State {
public:
	State(IN bool reInit = false) {
		ok = PSM_Begin(reInit);
	}
	~State() {
		PSM_End();
	}
	bool ok;
};

} // namespace PSM

