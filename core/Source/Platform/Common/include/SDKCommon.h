/******************************************************************************
 SDKCommon.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEPMacros.h"
#include "KEPCommon.h"
#include <js.h>
#include <assert.h>
#include <string>

///---------------------------------------------------------
// Unpack the version numbers from a ULONG
//
// [in] ver
// [out] v1
// [out] v2
// [out] v3
// [out] v4
//
inline void UnpackVersion(IN ULONG ver, OUT BYTE &v1, OUT BYTE &v2, OUT BYTE &v3, OUT BYTE &v4) {
	v1 = (BYTE)(ver >> 24);
	v2 = (BYTE)((ver & 0x00ff0000) >> 16);
	v3 = (BYTE)((ver & 0x0000ff00) >> 8);
	v4 = (BYTE)((ver & 0x000000ff));
}

///---------------------------------------------------------
// Pack four version numbers into a ULONG
//
// [in] v1
// [in] v2
// [in] v3
// [in] v4
//
// [Returns]
//    the packed version
inline ULONG PackVersion(IN BYTE v1, IN BYTE v2, IN BYTE v3, IN BYTE v4) {
	return (v1 << 24) | (v2 << 16) | (v3 << 8) | v4;
}

inline const char *VersionToString(IN ULONG v, OUT std::string &s) {
	static const int BUF_SIZE = 255;
	char buffer[BUF_SIZE];
	BYTE v1, v2, v3, v4;
	UnpackVersion(v, v1, v2, v3, v4);
	_snprintf_s(buffer, _countof(buffer), _TRUNCATE, "%d.%d.%d.%d", v1, v2, v3, v4);

	s = buffer;
	return s.c_str();
}

inline bool VersionEqual(IN ULONG vLeft, IN ULONG vRight, IN int levels = 1) {
	if (levels == 1)
		return (vLeft & 0xff000000) == (vRight & 0xff000000);
	else if (levels == 2)
		return (vLeft & 0xffff0000) == (vRight & 0xffff0000);
	else if (levels == 3)
		return (vLeft & 0xffffff00) == (vRight & 0xffffff00);
	else
		return vLeft == vRight;
}

inline bool VersionLess(IN ULONG vLeft, IN ULONG vRight, IN int levels = 1) {
	if (levels == 1)
		return (vLeft & 0xff000000) < (vRight & 0xff000000);
	else if (levels == 2)
		return (vLeft & 0xffff0000) < (vRight & 0xffff0000);
	else if (levels == 3)
		return (vLeft & 0xffffff00) < (vRight & 0xffffff00);
	else
		return vLeft < vRight;
}

