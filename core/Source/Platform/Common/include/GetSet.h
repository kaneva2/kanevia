///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IGetSet.h"

#include <GetSetStrings.h>

#include "d3d9.h"
#include "Core/Math/KEPMath.h"

#include <KEPHelpers.h>
#include "GetSetUpdateData.h"

class TiXmlElement;

#include <string>
#include <vector>
#include <memory>

#define STATUS_GROUP -1

// delimeter for object ids
const char OID_DELIM = '_';

class jsBEREncoder;
class jsBERDecoder;

namespace KEP {

// temp buffer size
const int TEMP_STR_SIZE = 1024;

inline bool gsIsSimple(int type) {
	return type > 0 && type < 100;
}
inline bool gsIsArray(int type) {
	return (type & gsSimpleArray) != 0;
}
inline int gsArrayType(int type) {
	return type & gsSimpleArray;
}
inline bool gsIsGraphic(int type) {
	return type >= 100 && type < 200;
}

#ifndef DWORD_MAX
#define DWORD_MAX ULONG_MAX
#endif

#ifndef DWORD_MIN
#define DWORD_MIN 0
#endif

#ifndef WORD_MAX
#define WORD_MAX USHRT_MAX
#endif

#ifndef WORD_MIN
#define WORD_MIN 0
#endif

#ifndef FLOAT_MAX
#define FLOAT_MAX FLT_MAX
#endif

#ifndef FLOAT_MIN
#define FLOAT_MIN FLT_MIN
#endif

#ifndef DOUBLE_MAX
#define DOUBLE_MAX DBL_MAX
#endif

#ifndef DOUBLE_MIN
#define DOUBLE_MIN DBL_MIN
#endif

// default max string length
#define STRLEN_MAX 80
class GetSet;
class GetSetUpdateData;
class IMemSizeGadget;

typedef GetSet* (*GETSET_FROM_PTR_FN)(void* o);

template <class T>
GetSet* GetSetFromPtr(void* o) {
	return dynamic_cast<GetSet*>(*((T**)o));
}

class KEPUTIL_EXPORT gsMetaDatum {
public:
	gsMetaDatum(EGetSetType _type, ULONG _flags, WORD _indent, UINT _groupTitleId,
		UINT _descId, UINT _helpId, double _min, double _max, GETSET_FROM_PTR_FN _getSetFromObj) :
			type(_type),
			flags(_flags), indent(_indent), groupTitleId(_groupTitleId), descId(_descId), helpId(_helpId), minValue(_min), maxValue(_max), getSetFromPtr(_getSetFromObj), dynamicDefaultValue(0), dynamicName(0) {
		jsAssert(!IsFlagSet(GS_FLAG_DYNAMIC_ADD));
		flags &= ~GS_FLAG_DYNAMIC_ADD;
	}

	// dynamically added one via type-safe constructors
	gsMetaDatum(ULONG _flags, const char* _name, int defaultValue) {
		int* i = (int*)malloc(sizeof(int));
		*i = defaultValue;
		initDynamic(gsInt, _flags, _name, i);
	}
	gsMetaDatum(ULONG _flags, const char* _name, const char* defaultValue) {
		initDynamic(gsString, _flags, _name, _strdup(NULL_CK(defaultValue)));
	}
	gsMetaDatum(ULONG _flags, const char* _name, long defaultValue) {
		long* l = (long*)malloc(sizeof(long));
		*l = defaultValue;
		initDynamic(gsLong, _flags, _name, l);
	}
	gsMetaDatum(ULONG _flags, const char* _name, double defaultValue) {
		double* d = (double*)malloc(sizeof(double));
		*d = defaultValue;
		initDynamic(gsDouble, _flags, _name, d);
	}
	gsMetaDatum(ULONG _flags, const char* _name, bool defaultValue) {
		bool* b = (bool*)malloc(sizeof(bool));
		*b = defaultValue;
		initDynamic(gsDouble, _flags, _name, b);
	}
	// more dangerous since no type on default value, in this case
	// GetSet owns the value and will free() it
	gsMetaDatum(EGetSetType _type, ULONG _flags, const char* _name, void* defaultValue) {
		initDynamic(_type, _flags, _name, defaultValue);
	}

	gsMetaDatum(const gsMetaDatum& src) :
			flags(0), dynamicName(0), dynamicDefaultValue(0) {
		operator=(src);
	}

	gsMetaDatum& operator=(const gsMetaDatum& src);

	virtual ~gsMetaDatum() {
		if (IsFlagSet(GS_FLAG_DYNAMIC_ADD)) {
			free((void*)dynamicName);
			free(dynamicDefaultValue);
		}
	}

	EGetSetType GetType() const {
		return type;
	}

	bool IsFlagSet(ULONG flagToTest) const {
		return (flags & flagToTest) != 0;
	}

	ULONG GetFlags() const {
		return flags; // see above for GS_FLAG_*
	}

	WORD GetIndent() const {
		return indent; // user-defined indent
	}

	UINT GetGroupTitleId() const {
		return groupTitleId; // stringId for group
	}

	UINT GetDescId() const {
		return descId; // string id for the descripition in the UI
	}

	UINT GetHelpId() const {
		return helpId; // help id for the UI
	}

	const char* GetDynamicName() const {
		return dynamicName;
	}

	const void* GetDynamicDefaultValue() const {
		return dynamicDefaultValue;
	}

	double GetMinValue() const {
		return minValue; // min/max for validating ranges on set
	}

	double GetMaxValue() const {
		return maxValue;
	}

	GETSET_FROM_PTR_FN GetGetSetFromObj() const {
		return getSetFromPtr;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	void GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const;

private:
	// constructor helper for dynamic values
	void initDynamic(EGetSetType _type, ULONG _flags, const char* _name, void* defaultValue) {
		type = _type;
		flags = _flags | GS_FLAG_DYNAMIC_ADD | GS_FLAG_DELETE;
		indent = 0;
		groupTitleId = 0;
		dynamicName = _strdup(_name);
		helpId = 0;
		descId = 0;
		minValue = 0;
		maxValue = 0;
		getSetFromPtr = 0;
		dynamicDefaultValue = defaultValue;
		jsAssert(defaultValue != 0);
	}

	EGetSetType type; // its type
	ULONG flags; // see above for GS_FLAG_*
	WORD indent; // user-defined indent number
	UINT groupTitleId; // stringId for group
	UINT descId; // string id for the descripition in the UI
	UINT helpId; // help id for the UI
	double minValue; // min/max for validating ranges on set
	double maxValue;
	GETSET_FROM_PTR_FN getSetFromPtr;

	// only valid for dynamically added ones
	const char* dynamicName; // if dynamically added, this is a string, not id
	void* dynamicDefaultValue; // if don't have it, the default
};

class gsFilter {
public:
	gsFilter(bool allAttributes = true) :
			m_allAttributes(allAttributes) {}

	virtual ~gsFilter() {}

	virtual bool include(gsMetaDatum& /*md*/) const {
		return true;
	}

	bool allAttributes() const {
		return m_allAttributes;
	}

private:
	bool m_allAttributes;
};

class GetSetVector : public std::vector<GetSet*> {
public:
	GetSetVector(bool deleteItemsOnDelete = true) :
			m_deleteItemsOnDelete(deleteItemsOnDelete) {}

	virtual inline ~GetSetVector();

	virtual void fillArray() {}

protected:
	bool m_deleteItemsOnDelete;
};

typedef std::vector<gsMetaDatum> gsMetaData;
typedef std::vector<void*> gsValues;

class KEPUTIL_EXPORT GetSet : public IGetSet {
public:
	GetSet() :
			m_gsNameId(0), m_gsMetaData(0), m_dirty(false) {
		m_objectId = (ULONG)(GetSet*)this;
	}

	virtual ~GetSet();

	// get the name id of this item, should be unique
	virtual UINT GetNameId();

	// set a single value on the object
	virtual bool SetValue(ULONG index, const char* value);

	// set a single value on the object
	virtual bool SetValueFromBER(ULONG index, jsBERDecoder& dec);

	// get the name.  The DECLARE_GETSET macro creates this
	virtual std::string GetGsName() const {
		return loadStr(m_gsNameId);
	}

	// get the id of this object.  The BEGIN_GETSET_IMPL macro
	// sets this as the string id
	UINT GetGsId() const {
		return m_gsNameId;
	}

	///---------------------------------------------------------
	// get the array of gsRecs for this object.  The array is
	// terminated with a rec of type gsEndOfArray.  The position
	// of items in the array is used in other method calls
	// that take an index parameter.  The DECLARE_GETSET
	// macro creates this in a class
	//
	// [out] values array of the values
	// [out] md matching array of metadata about the values
	virtual void GetValues(gsValues*& values, gsMetaData*& md) {
		initGetSet();
		values = m_gsValues.get();
		md = m_gsMetaData;
	}

	///---------------------------------------------------------
	// Helper to get a pointer to a CObList since the m_values
	// value is a CObList **
	//
	// [in] index index to ge
	//
	// [Returns]
	//    the CObList *, or null of wrong index, not right type,
	// or it is null
	inline CObList* ToObListPtr(gsValues::size_type index);

	///---------------------------------------------------------
	// Helper to get count of CObList
	//
	// [in] index index of item
	//
	// [Returns]
	//    count, or 0 if object is null
	inline INT_PTR GetObListCount(gsValues::size_type index);

	///---------------------------------------------------------
	// Helper for enumerating a type that is a CObList
	//
	// [in] index index of item
	//
	// [Returns]
	//    the CObList POSITION, null if the object is null
	inline POSITION GetObListHeadPosition(gsValues::size_type index);

	///---------------------------------------------------------
	// Helper for enumerating a type that is a CObList
	//
	// [in] index index of item
	// [inout] pos the position from GetObListHeadPosition
	//
	// [Returns]
	//    null if at end, or object is null
	inline GetSet* GetObListNext(gsValues::size_type index, POSITION& pos);

	///---------------------------------------------------------
	// Helper to get a pointer to a CObList since the m_values
	// value is a CObject **
	//
	// [in] index index to get
	//
	// [Returns]
	//    the GetSet *, or null of wrong index, not right type,
	// or it is null
	inline GetSet* ToGetSetPtr(gsValues::size_type index);

	///---------------------------------------------------------
	// Helper to get a point to a vector of GetSet*.  It
	// is important to use this since delegated arrays init
	// when this is called
	//
	// [in] index index to get
	//
	// [Returns]
	//    the GetSetVector *, or null of wrong index, not right type,
	// or it is null
	inline GetSetVector* ToGetSetVector(gsValues::size_type index);

	///---------------------------------------------------------
	// get an XML representation of this object's values
	//
	// [in] objectId this object's id
	// [in] depth how far to get, INT_MAX means all items
	// [in] filter class to test each object to see if it
	//	    should be included in the XML
	// [in] md metadata about this object, used in recursion
	//
	// [Returns]
	//     the XML
	virtual std::string GetXml(const char* objectId = "", int depth = INT_MAX, gsFilter* filter = 0, gsMetaData* md = 0);

	///---------------------------------------------------------
	// set values from XML.
	//
	// [in] getSetDataNode XML node of data
	//
	virtual void SetFromXml(TiXmlElement* getSetDataNode);

	///---------------------------------------------------------
	// get an BER representation of this object's values
	//
	// [inout] enc the encode where to encode the object
	// [in] depth how far to get, INT_MAX means all items
	// [in] filter class to test each object to see if it
	//	    should be included in the XML
	// [in] md metadata about this object, used in recursion
	//
	// [Returns]
	//     the XML
	virtual void EncodeToBER(jsBEREncoder& enc, int depth = -1, gsFilter* filter = 0, gsMetaData* myMd = 0);

	///---------------------------------------------------------
	// set values from BER buffer.
	//
	// [in] dec the decoder to extra the data from
	//
	virtual void DecodeFromBER(jsBERDecoder& dec);

	///---------------------------------------------------------
	// used by the macros to populate the vectors
	//
	// [in] v
	// [in] type
	// [in] flags
	// [in] indent
	// [in] groupTitleId
	// [in] descId
	// [in] helpId
	// [in] min
	// [in] max
	// [in] extra
	//
	inline void AddGetSetRec(void* v,
		EGetSetType type,
		ULONG flags,
		WORD indent,
		UINT groupTitleId,
		UINT descId,
		UINT helpId,
		double min,
		double max,
		GETSET_FROM_PTR_FN extra = 0);

	inline std::string GetInstanceName() const;

	inline void SetUpdater(GetSetUpdateDataPtr& updater) {
		m_updater = updater;
	}
	inline GetSetUpdateDataPtr GetUpdater() {
		return m_updater;
	}

	// IGetSet overrides
	virtual Vector3f& GetVector3f(ULONG index);
	virtual ULONG& GetULONG(ULONG index);
	virtual INT& GetINT(ULONG index);
	virtual BOOL& GetBOOL(ULONG index);
	virtual bool& Getbool(ULONG index);
	virtual std::string& Getstring(ULONG index);
	virtual IGetSet* GetObjectPtr(ULONG index);
	virtual void GetColor(ULONG index, float* r, float* g, float* b, float* a = 0);

	// return a copy of the string, if it was string or CStringA
	virtual std::string GetString(ULONG index);
	// return a copy of the number as double
	virtual double GetNumber(ULONG index);

	virtual ULONG AddNewIntMember(const char* name, int defaultValue, ULONG flags = amfTransient);
	virtual ULONG AddNewDoubleMember(const char* name, double defaultValue, ULONG flags = amfTransient);
	virtual ULONG AddNewStringMember(const char* name, const char* defaultValue, ULONG flags = amfTransient);
	virtual ULONG FindIdByName(const char* name, EGetSetType* type = 0);
	ULONG AddNewMember(const char* name, EGetSetType type, ULONG flags, void* defaultValue);
	void CopyDynamicallyAddedMembers(GetSet* from);

	// sets
	virtual void SetNumber(ULONG index, LONG number);
	virtual void SetNumber(ULONG index, ULONG number);
	virtual void SetNumber(ULONG index, double number);
	virtual void SetNumber(ULONG index, BOOL value);
	virtual void SetNumber(ULONG index, bool value);
	virtual void SetString(ULONG index, const char* s);
	virtual void SetVector3f(ULONG index, Vector3f v);
	virtual void SetColor(ULONG index, float r, float g, float b, float a = 1.0);

	// array methods
	virtual ULONG GetArrayCount(ULONG indexOfArray);
	virtual IGetSet* GetObjectInArray(ULONG indexOfArray, ULONG indexInArray);

	void SerializeMetaDataFromXML(const std::string& fileName);
	void SerializeMetaDataToXML(const std::string& fileName);

	void SetDirty(bool b = true) {
		m_dirty = b;
	}
	bool IsDirty() const {
		return m_dirty;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	// defined in the BEGIN_GETSET_IMPL macro
	virtual void initGetSet() = 0;

	///---------------------------------------------------------
	// called when getting the inner XML for an item
	//
	// [inout] xml the XML you append to
	// [in] md the metaData about the objectx
	// [in] value the object
	// [inout] workingString a temp string, if you need it
	// [in] parent parent's metadatum object
	//
	// [Returns]
	//     true if you handle it
	virtual bool getInnerXml(std::string& /*xml*/, gsMetaDatum& /*md*/, void* /*value*/, char* /*workingString*/) {
		return false;
	}

	///---------------------------------------------------------
	// get the Xml for a single item, called by GetXml and
	// can be overriden for custom types
	//
	// [in] i index of item in md and value arrays being processed
	// [in] objectId this object's id
	// [in] depth how far to get, INT_MAX means all items
	// [in] filter class to test each object to see if it
	//	    should be included in the XML
	// [in] myMd metadata about this (parent) object, used in recursion
	// [inout] xml xml string to append to
	// [in] s temporary string of TMP_STR_SIZE to avoid extra mallocs
	//
	virtual void getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s);

	///---------------------------------------------------------
	// get the BER for a single item, called by EncodeToBER and
	// can be overriden for custom types
	//
	// [in] i index of item in md and value arrays being processed
	// [in] objectId this object's id
	// [in] depth how far to get, INT_MAX means all items
	// [in] filter class to test each object to see if it
	//	    should be included in the XML
	// [in] myMd metadata about this (parent) object, used in recursion
	// [inout] enc encoder
	//
	virtual void encodeItemToBER(int i, int depth, gsFilter* filter, gsMetaData* myMd, jsBEREncoder& enc);

	// helper for creating parameter Xml for actions
	void addActionParamXml(std::string& xml, UINT descId, UINT helpId, EGetSetType type, char* s, ULONG flags = 0, double min = 0.0, double max = 0.0);

	// see if anything dynamically added since we're constructed
	void checkValues();

protected:
	void GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const;

	UINT m_gsNameId;
	std::unique_ptr<gsValues> m_gsValues;
	gsMetaData* m_gsMetaData;
	GetSetUpdateDataPtr m_updater;

	ULONG m_objectId; // usually "this", well known objects can reset (e.g. root is 0)
	bool m_dirty; // track if any Set* fns called
};

//---------------------------------------------------------
inline CObList* GetSet::ToObListPtr(gsValues::size_type index) {
	initGetSet();
	if (index < m_gsValues->size() && (*m_gsValues)[index]) {
		ASSERT(m_gsMetaData->at(index).GetType() == gsObListPtr);
		if (m_gsMetaData->at(index).GetType() == gsObListPtr)
			return *(CObList**)(*m_gsValues)[index];
	}
	return 0;
}

//---------------------------------------------------------
inline GetSetVector* GetSet::ToGetSetVector(gsValues::size_type index) {
	initGetSet();
	if (index < m_gsValues->size() && (*m_gsValues)[index]) {
		ASSERT(m_gsMetaData->at(index).GetType() == gsGetSetVector);
		if (m_gsMetaData->at(index).GetType() == gsGetSetVector) {
			GetSetVector* v = (GetSetVector*)(*m_gsValues)[index];
			if (v)
				v->fillArray();

			return v;
		}
	}
	return 0;
}

//---------------------------------------------------------
inline GetSet* GetSet::ToGetSetPtr(gsValues::size_type index) {
	initGetSet();
	if (index < m_gsValues->size() && (*m_gsValues)[index]) {
		ASSERT(m_gsMetaData->at(index).GetType() == gsObjectPtr);
		if (m_gsMetaData->at(index).GetType() == gsObjectPtr)
			return m_gsMetaData->at(index).GetGetSetFromObj()(m_gsValues->at(index));
	}
	return 0;
}

//---------------------------------------------------------
inline INT_PTR GetSet::GetObListCount(gsValues::size_type index) {
	CObList* obj = ToObListPtr(index);
	if (obj) {
		return obj->GetCount();
	}
	return 0;
}

//---------------------------------------------------------
inline POSITION GetSet::GetObListHeadPosition(gsValues::size_type index) {
	CObList* obj = ToObListPtr(index);
	if (obj) {
		return obj->GetHeadPosition();
	}
	return NULL;
}

//---------------------------------------------------------
inline GetSet* GetSet::GetObListNext(gsValues::size_type index, POSITION& pos) {
	CObList* obj = ToObListPtr(index);
	if (obj) {
		return dynamic_cast<GetSet*>(obj->GetNext(pos));
	}
	return NULL;
}

//---------------------------------------------------------
inline GetSetVector::~GetSetVector() {
	if (m_deleteItemsOnDelete) {
		for (GetSetVector::iterator i = begin(); i != end(); i++) {
			if (*i)
				delete (*i);
		}
	}
}

//---------------------------------------------------------
inline void GetSet::AddGetSetRec(void* v,
	EGetSetType type,
	ULONG flags,
	WORD indent,
	UINT groupTitleId,
	UINT descId,
	UINT helpId,
	double min,
	double max,
	GETSET_FROM_PTR_FN extra /*= 0*/) {
#ifdef _DEBUG
	std::string s = loadStr(descId);
	ASSERT(::strstr(s.c_str(), "not found in resource file") == 0);
	s = loadStr(helpId);
	ASSERT(::strstr(s.c_str(), "not found in resource file") == 0);
#endif
	// always add to this instance's
	m_gsValues->push_back(v);
	// if static vector is smaller, we need to add it
	if (m_gsMetaData->size() < m_gsValues->size())
		m_gsMetaData->push_back(gsMetaDatum(type, flags, indent, groupTitleId, descId, helpId, min, max, extra));
}

//---------------------------------------------------------
inline std::string GetSet::GetInstanceName() const {
	std::string ret;
	if (m_gsMetaData != 0 && m_gsValues.get() != 0) {
		for (UINT i = 0; i < m_gsMetaData->size(); i++) {
			if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_INSTANCE_NAME)) {
				ASSERT(ret.empty()); // should only be one!
				if (m_gsMetaData->at(i).GetType() == gsString && m_gsValues->at(i)) {
					ret = *(std::string*)m_gsValues->at(i);
				} else if (m_gsMetaData->at(i).GetType() == gsCString && m_gsValues->at(i)) {
					ret = *(CStringA*)m_gsValues->at(i);
				} else {
					ASSERT(FALSE);
				}
#ifndef _DEBUG
				break; // for debug, we don't break to assert if > 1
#endif
			}
		}
	}
	return ret;
}

} // namespace KEP

// All the impl is done via macros to allow the option of
// completely disabling GetSet functionality, if needed

#define DECLARE_GETSET         \
protected:                     \
	virtual void initGetSet(); \
                               \
protected:                     \
	static gsMetaData m_myGsMetaData;

//===========================================
// for the CPP one of these...
#define BEGIN_GETSET_IMPL(className, nameId) \
	gsMetaData className## ::m_myGsMetaData; \
	void className## ::initGetSet() {        \
		m_gsNameId = nameId;                 \
		m_gsMetaData = &m_myGsMetaData;      \
		if (m_gsValues.get() == 0) {         \
			m_gsValues.reset(new gsValues());

#define BEGIN_GETSET_DERIVED_IMPL(className, nameId, baseClassName) \
	void className## ::initGetSet() {                               \
		m_gsNameId = nameId;                                        \
		bool needInit = m_gsValues.get() == 0;                      \
		baseClassName## ::initGetSet();                             \
		jsAssert(m_gsValues.get() != 0);                            \
		if (needInit) {
//===========================================
// one or more of each of these....
#define GETSET_RANGE(var, type, flags, indent, groupId, class, name, min, max) \
	AddGetSetRec(&var, type, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, min, max);

#define GETSET_MAX(var, type, flags, indent, groupId, class, name, max) \
	AddGetSetRec(&var, type, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, max);

#define GETSET(var, type, flags, indent, groupId, class, name) \
	AddGetSetRec(&var, type, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

// changed 7/05 to allow null pointers, otherwise if set to non-null later, it was still zero
// check to see if ptr non-null and it's the right type on these two
#define GETSET_OBJ_PTR(var, flags, indent, groupId, class, name, className) \
	AddGetSetRec(var == 0 || dynamic_cast<GetSet*>(var) ? &var : 0, gsObjectPtr, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0, GetSetFromPtr<className>);

#define GETSET_OBJ_PTR_CONST(var, indent, groupId, class, name, className) \
	AddGetSetRec(var == 0 || const_cast<GetSet*>(dynamic_cast<const GetSet*>(var)) ? &var : 0, gsObjectPtr, GS_FLAG_EXPOSE, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0, GetSetFromPtr<className>);

#define GETSET_OBLIST_PTR(var, flags, indent, groupId, listClass, name) \
	AddGetSetRec(var == 0 || dynamic_cast<CObList*>(var) ? &var : 0, gsObListPtr, flags, indent, groupId, IDS_##listClass##_##name, IDS_##listClass##_##name##_HELP, 0.0, 0.0);

#define GETSET_RGB(varR, varG, varB, flags, indent, groupId, class, name) \
	AddGetSetRec(new gsRgbaValue(&varR, &varG, &varB, 0), gsRgbaColor, flags | GS_FLAG_DELETE, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_RGBA(varR, varG, varB, varA, flags, indent, groupId, class, name) \
	AddGetSetRec(new gsRgbaValue(&varR, &varG, &varB, &varA), gsRgbaColor, flags | GS_FLAG_DELETE, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_D3DVECTOR(var, flags, indent, groupId, class, name) \
	AddGetSetRec(&var, gsD3dvector, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_D3DMATERIAL9(var, flags, indent, groupId, class, name) \
	AddGetSetRec(dynamic_cast<GetSet*>(new gsD3dmaterial9Value(&var, indent, groupId)), gsGetSetPtr, flags | GS_FLAG_DELETE, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_PTR(var, flags, indent, groupId, class, name) \
	AddGetSetRec(dynamic_cast<GetSet*>(var), gsGetSetPtr, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

// check to see if ptr non-null and it's the right type on these two
#define GETSET_VECTOR(var, flags, indent, groupId, class, name) \
	AddGetSetRec(static_cast<GetSetVector*>(&var), gsGetSetVector, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_ACTION(var, flags, indent, groupId, class, name) \
	AddGetSetRec(dynamic_cast<GetSet*>(var), gsAction, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_VALUE_LIST(var, flags, indent, groupId, class, name) \
	AddGetSetRec(dynamic_cast<gsListItems*>(&var), gsListOfValues, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_SIMPLE_ARRAY(count, type, gsType, items, flags, indent, groupId, class, name) \
	AddGetSetRec(new TGetSetSimpleArray<type>((ULONG&)count, &items), gsSimpleArray | gsType, flags | GS_FLAG_DELETE, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, 0.0, 0.0);

#define GETSET_CUSTOM(ptr, type, flags, indent, groupId, class, name, min, max) \
	AddGetSetRec(ptr, type, flags, indent, groupId, IDS_##class##_##name, IDS_##class##_##name##_HELP, min, max);

// 1/08 added these that specifiy strings explicitly to allow reuse
#define GETSET2_RANGE(var, type, flags, indent, groupId, nameStrId, helpStrId, min, max) \
	AddGetSetRec(&var, type, flags, indent, groupId, nameStrId, helpStrId, min, max);

#define GETSET2_MAX(var, type, flags, indent, groupId, nameStrId, helpStrId, max) \
	AddGetSetRec(&var, type, flags, indent, groupId, nameStrId, helpStrId, 0.0, max);

#define GETSET2(var, type, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(&var, type, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

// changed 7/05 to allow null pointers, otherwise if set to non-null later, it was still zero
// check to see if ptr non-null and it's the right type on these two
#define GETSET2_OBJ_PTR(var, flags, indent, groupId, nameStrId, helpStrId, className) \
	AddGetSetRec(var == 0 || dynamic_cast<GetSet*>(var) ? &var : 0, gsObjectPtr, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0, GetSetFromPtr<className>);

#define GETSET2_OBLIST_PTR(var, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(var == 0 || dynamic_cast<CObList*>(var) ? &var : 0, gsObListPtr, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_RGB(varR, varG, varB, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(new gsRgbaValue(&varR, &varG, &varB, 0), gsRgbaColor, flags | GS_FLAG_DELETE, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_RGBA(varR, varG, varB, varA, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(new gsRgbaValue(&varR, &varG, &varB, &varA), gsRgbaColor, flags | GS_FLAG_DELETE, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_D3DVECTOR(var, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(&var, gsD3dvector, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_D3DMATERIAL9(var, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(dynamic_cast<GetSet*>(new gsD3dmaterial9Value(&var, indent, groupId)), gsGetSetPtr, flags | GS_FLAG_DELETE, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_PTR(var, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(dynamic_cast<GetSet*>(var), gsGetSetPtr, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

// check to see if ptr non-null and it's the right type on these two
#define GETSET2_VECTOR(var, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(static_cast<GetSetVector*>(&var), gsGetSetVector, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_ACTION(var, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(dynamic_cast<GetSet*>(var), gsAction, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_VALUE_LIST(var, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(dynamic_cast<gsListItems*>(&var), gsListOfValues, flags, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_SIMPLE_ARRAY(count, type, gsType, items, flags, indent, groupId, nameStrId, helpStrId) \
	AddGetSetRec(new TGetSetSimpleArray<type>((ULONG&)count, &items), gsSimpleArray | gsType, flags | GS_FLAG_DELETE, indent, groupId, nameStrId, helpStrId, 0.0, 0.0);

#define GETSET2_CUSTOM(ptr, type, flags, indent, groupId, nameStrId, helpStrId, min, max) \
	AddGetSetRec(ptr, type, flags, indent, groupId, nameStrId, helpStrId, min, max);

//===========================================
// and one to end it
#define END_GETSET_IMPL \
	}                   \
	checkValues();      \
	}

// other helpers used by getset
#include "gsRgbaValue.h"
#include "gsD3dmaterial9Value.h"

#include "GetSetList.h"
#include "GetSetUpdateData.h"
#include "GetSetSimpleArray.h"
