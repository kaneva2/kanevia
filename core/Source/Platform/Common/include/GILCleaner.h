///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <map>
#include "Core/Util/fast_mutex.h"

namespace KEP {

class CGlobalInventoryObjList;

/**
 * This class provides a stop gap solution for keeping the GIL cleaned of any
 * items that are not in use.  An instance is bound to the GIL at startup.
 * CInventoryObjList acquires a reference to this instance.  When items are
 * added or removed from the CInventoryObjList, this classes itemAdded() and
 * and itemRemoved() methods are called respectively.  When items have a
 * "count" of zero (i.e. CInventoryObjLists reference the item), then the
 * cleaner removes the item from the GIL.
 */
class GILCleaner {
public:
	/**
	 * Default constructor
	 */
	GILCleaner();

	/**
	 * dtor
	 */
	virtual ~GILCleaner();

	/**
	 * Called by CInventoryObjList when an item is added to the list.
	 * Note that CInventoryObjList constructor acquires the 
	 * CGlobalInventoryObjList::singleton.  From this it acquires this
	 * GILCleanerInstance.
	 * @param glid   The global id of the item that was added to a CInventoryObjList
	 */
	virtual GILCleaner& itemAdded(unsigned long glid) = 0;

	/**
	 * Called by CInventoryObjList when an item is removed from the list.
	 * Note that CInventoryObjList constructor acquires the 
	 * CGlobalInventoryObjList::singleton.  From this it acquires this
	 * GILCleanerInstance.
	 * @param glid   The global id of the item that was added to a CInventoryObjList
	 */
	virtual GILCleaner& itemRemoved(unsigned long glid) = 0;

	/**
	 * Acquire an xml representation of the cleaner.
	 */
	virtual std::string toXML() const = 0;

	/**
	 * Acquire a summary in xml format
	 */
	virtual std::string toXMLSummary() const = 0;
};

/**
 * This class provides a stop gap solution for keeping the GIL cleaned of any
 * items that are not in use.  An instance is bound to the GIL at startup.
 * CInventoryObjList acquires a reference to this instance.  When items are
 * added or removed from the CInventoryObjList, this classes itemAdded() and
 * and itemRemoved() methods are called respectively.  When items have a
 * "count" of zero (i.e. CInventoryObjLists reference the item), then the
 * cleaner removes the item from the GIL.
 * 
 * @see GILCleaner
 */
class ConcreteGILCleaner : public GILCleaner {
public:
	/**
	 * Default constructor
	 */
	ConcreteGILCleaner();

	/**
	 * dtor
	 */
	virtual ~ConcreteGILCleaner();

	/**
	 * Called by CInventoryObjList when an item is added to the list.
	 * Note that CInventoryObjList constructor acquires the 
	 * CGlobalInventoryObjList::singleton.  From this it acquires this
	 * GILCleanerInstance.
	 * @param glid   The global id of the item that was added to a CInventoryObjList
	 */
	GILCleaner& itemAdded(unsigned long glid);

	/**
	 * Called by CInventoryObjList when an item is removed from the list.
	 * Note that CInventoryObjList constructor acquires the 
	 * CGlobalInventoryObjList::singleton.  From this it acquires this
	 * GILCleanerInstance.
	 * @param glid   The global id of the item that was added to a CInventoryObjList
	 */
	GILCleaner& itemRemoved(unsigned long glid);

	/**
	 * Acquire an xml representation of the cleaner.
	 */
	std::string toXML() const;
	std::string toXMLSummary() const;

private:
	typedef std::pair<unsigned long, unsigned long> GLIDMapEntryT;
	typedef std::map<unsigned long, unsigned long> GLIDMapT;

	CGlobalInventoryObjList* _pGIL;
	GLIDMapT _glidMap;
	mutable fast_recursive_mutex _resource;
};

/**
 * A null implementation of GILCleaner.
 * @see GILCleaner
 */
class MockGILCleaner : public GILCleaner {
public:
	/**
	 * Default constructor
	 */
	MockGILCleaner() :
			GILCleaner() {}

	/**
	 * dtor
	 */
	virtual ~MockGILCleaner() {}

	/**
	 * Called by CInventoryObjList when an item is added to the list.
	 * Note that CInventoryObjList constructor acquires the 
	 * CGlobalInventoryObjList::singleton.  From this it acquires this
	 * GILCleanerInstance.
	 *
	 * This implementation does performs no operation.
	 *
	 * @param glid   The global id of the item that was added to a CInventoryObjList
	 */
	GILCleaner& itemAdded(unsigned long /*glid*/) { return *this; }

	/**
	 * Called by CInventoryObjList when an item is removed from the list.
	 * Note that CInventoryObjList constructor acquires the 
	 * CGlobalInventoryObjList::singleton.  From this it acquires this
	 * GILCleanerInstance.
	 *
	 * This implementation does performs no operation.
	 *
	 * @param glid   The global id of the item that was added to a CInventoryObjList
	 */
	GILCleaner& itemRemoved(unsigned long /*glid*/) { return *this; }

	/**
	 * Acquire an xml representation of the cleaner.
	 */
	std::string toXML() const;
	std::string toXMLSummary() const;
};

} // namespace KEP