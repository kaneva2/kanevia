///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPHelpers.h"
#include "FlashScriptAccessMode.h"

#define FMT_DIST std::fixed << std::setprecision(1)

#define MEDIA_VOL_PCT 100.0 // full volume
#define MEDIA_RAD_AUD 0.0 // global presence
#define MEDIA_RAD_VID 0.0 // global presence

#define MEDIA_SET_PLAYLIST(val, id) (val = (id < 0) ? val : id)
#define MEDIA_SET_VOLPCT(val, volPct) (val = (volPct < 0.0) ? val : ((volPct <= 100.0) ? volPct : 100.0))
#define MEDIA_SET_RADIUS(val, radius) (val = (radius < 0.0) ? val : radius)

namespace KEP {
class IMemSizeGadget;

class MediaRadius {
public:
	MediaRadius(double audio = MEDIA_RAD_AUD, double video = MEDIA_RAD_VID) :
			m_audio(audio), m_video(video) {}

	std::string ToStr() const {
		std::string str;
		if (GetAudio() >= 0) {
			StrAppend(str, "radAud=" << GetAudio() << " ");
		}
		if (GetVideo() >= 0) {
			StrAppend(str, "radVid=" << GetVideo() << " ");
		}
		return str;
	}

	// Audio Trigger Radius (-1=no change)
	double GetAudio() const { return m_audio; }
	bool SetAudio(double radius = -1) {
		MEDIA_SET_RADIUS(m_audio, radius);
		return true;
	}

	// Video Trigger Radius (-1=no change)
	double GetVideo() const { return m_video; }
	bool SetVideo(double radius = -1) {
		MEDIA_SET_RADIUS(m_video, radius);
		return true;
	}

	// Global Radius
	bool IsGlobalAudio() const { return (GetAudio() == 0.0); }
	bool IsGlobalVideo() const { return (GetVideo() == 0.0); }
	bool IsGlobal() const { return (IsGlobalAudio() || IsGlobalVideo()); }

	// Local Radius
	bool IsLocalAudio() const { return !IsGlobalAudio(); }
	bool IsLocalVideo() const { return !IsGlobalVideo(); }
	bool IsLocal() const { return (IsLocalAudio() && IsLocalVideo()); }

private:
	double m_audio; // audio radius
	double m_video; // video radius
};

class MediaRange {
public:
	MediaRange() :
			m_distance(0.0), m_inRangeAudio(false), m_inRangeVideo(false) {}

	std::string ToStr() const {
		std::string str;
		StrBuild(str, "dist=" << FMT_DIST << GetDistance()
							  << " " << GetRadius().ToStr()
							  << (InRangeAudio() ? "AUD " : "")
							  << (InRangeVideo() ? "VID " : ""));
		return str;
	}

	// Range Radius
	MediaRadius GetRadius() const { return m_radius; }
	bool SetRadius(const MediaRadius& radius) {
		m_radius = radius;
		return InRangeUpdate();
	}

	// Range Distance
	double GetDistance() const { return m_distance; }
	bool SetDistance(double distance) {
		m_distance = distance;
		return InRangeUpdate();
	}

	// In Range Of Audio
	bool InRangeAudio() const { return m_inRangeAudio; }
	bool SetInRangeAudio(bool inRange) {
		m_inRangeAudio = inRange;
		return true;
	}

	// In Range Of Video
	bool InRangeVideo() const { return m_inRangeVideo; }
	bool SetInRangeVideo(bool inRange) {
		m_inRangeVideo = inRange;
		return true;
	}

	// In Range Update (audio & video)
	bool InRangeUpdate() {
		SetInRangeAudio(m_radius.IsGlobalAudio() || (m_distance <= m_radius.GetAudio()));
		SetInRangeVideo(m_radius.IsGlobalVideo() || (m_distance <= m_radius.GetVideo()));
		return true;
	}

private:
	MediaRadius m_radius; // range radius
	double m_distance; // range distance
	bool m_inRangeAudio; // audio range trigger
	bool m_inRangeVideo; // video range trigger
};

class MediaParams {
public:
	// Media States
	enum STATE {
		STATE_CHANGED,
		STATE_STOPPED,
		STATE_PLAYING,
	};

	// Media Modes
	enum MODE {
		MODE_STOP,
		MODE_PLAY
	};

	MediaParams(const std::string& url = "", const std::string& params = "", double volPct = MEDIA_VOL_PCT, double radAud = MEDIA_RAD_AUD, double radVid = MEDIA_RAD_VID, int playlistId = 0) :
			m_mode(MODE_PLAY), m_state(STATE_STOPPED), m_url(url), m_params(params), m_volPct(volPct), m_radius(radAud, radVid), m_playlistId(playlistId), m_allowScriptAccess(FlashScriptAccessMode::Never) {
		UpdateParamsValid();
	}

	std::string ToStr(bool verbose = false) const {
		std::string str;
		if (IsRadiusGlobal()) {
			StrAppend(str, "MP_G<");
		} else {
			StrAppend(str, "MP_L<" << m_radius.ToStr());
		}
		if (GetVolume() >= 0) {
			StrAppend(str, "volPct=" << GetVolume() << " ");
		}
		if (GetPlaylistId() > 0) {
			StrAppend(str, "playlistId=" << GetPlaylistId() << " ");
		}
		if (!IsUrlAndParamsValid()) {
			StrAppend(str, "''");
		} else if (IsUrlKgpPlaylist()) {
			StrAppend(str, "'KGPPlaylist.swf + " << GetParams() << "'");
		} else {
			StrAppend(str, "'" << GetUrl() << " + " << GetParams() << "'");
		}
		StrAppend(str, ">");
		return str;
	}

	// Set All Params (applying no-change rules)
	bool Set(const MediaParams& mediaParams) {
		SetUrl(mediaParams.GetUrl());
		SetParams(mediaParams.GetParams());
		SetVolume(mediaParams.GetVolume());
		SetRadius(mediaParams.GetRadius());
		SetPlaylistId(mediaParams.GetPlaylistId());
		SetAllowScriptAccess(mediaParams.GetAllowScriptAccess());
		return true;
	}

	// Media Mode
	MODE GetMode() const { return m_mode; }
	void SetMode(const MODE& mode) { m_mode = mode; }

	// Media State
	STATE GetState() const { return m_state; }
	bool SetState(const STATE& state) {
		m_state = state;
		return true;
	}
	bool IsStopped() const { return (m_state == STATE_STOPPED); }
	bool IsPlaying() const { return (m_state == STATE_PLAYING); }
	bool IsChanged() const { return (m_state == STATE_CHANGED); }

	// Media Url
	const std::string& GetUrl() const { return m_url; }
	bool SetUrl(const std::string& url, bool changed = true) {
		m_url = StrReplace(url, "autoPlay=no", "autoPlay=yes");
		if (changed)
			SetState(STATE_CHANGED);
		return true;
	}
	bool IsUrlSame(const std::string& url) const { return StrSame(url, m_url); }
	bool IsUrlValid() const { return !GetUrl().empty(); }
	bool IsUrlLocal() const { return !STLContainsIgnoreCase(GetUrl(), "://"); }
	bool IsUrlFlash() const { return (IsUrlSwf() || IsUrlYouTube()); }
	bool IsUrlSwf() const { return STLContainsIgnoreCase(GetUrl(), ".swf"); }
	bool IsUrlKgpPlaylist() const { return STLContainsIgnoreCase(GetUrl(), "KGPPlaylist.swf"); }
	bool IsUrlYouTube() const { return IsUrlYouTube(GetUrl()); }
	static bool IsUrlYouTube(const std::string& str) { return STLContainsIgnoreCase(str, "YouTube.com"); }

	// Media Params
	const std::string& GetParams() const { return m_params; }
	bool SetParams(const std::string& params, bool changed = true) {
		m_params = params;
		UpdateParamsValid();
		if (changed)
			SetState(STATE_CHANGED);
		return true;
	}
	bool IsParamsSame(const std::string& params) const { return StrSame(params, m_params); }
	bool IsParamsValid() const { return m_bParamsValid; } // && !IsParamsCancelBroadcast(); }
	bool IsParamsPlaylistNone() const { return STLContainsIgnoreCase(GetParams(), "playlistUrl=None"); }
	bool IsParamsCancelBroadcast() const { return STLContainsIgnoreCase(GetParams(), "cancel_broadcast"); }

	bool IsUrlAndParamsSame(const MediaParams& mediaParams) const {
		return IsUrlSame(mediaParams.GetUrl()) && IsParamsSame(mediaParams.GetParams());
	}
	bool IsUrlAndParamsValid() const {
		return IsUrlValid() && IsParamsValid();
	}

	// Media Playlist (-1=no change, 0=none)
	int GetPlaylistId() const { return m_playlistId; }
	bool SetPlaylistId(int id = -1) {
		MEDIA_SET_PLAYLIST(m_playlistId, id);
		return true;
	}
	bool SetPlaylistId(const std::string& params) {
		auto it = params.find("playlistId=");
		return SetPlaylistId((it != std::string::npos) ? atoi(&params[it + 11]) : 0);
	}
	bool HasPlaylistId() const { return (GetPlaylistId() > 0); }
	bool IsPlaylistSame(const MediaParams& mediaParams) const {
		return HasPlaylistId() && (GetPlaylistId() == mediaParams.GetPlaylistId());
	}

	// Media Volume (-1=no change, 0=mute)
	double GetVolume() const { return m_volPct; }
	bool SetVolume(double volPct = -1) {
		MEDIA_SET_VOLPCT(m_volPct, volPct);
		return true;
	}

	// Media Radius (-1=no change, 0=global)
	MediaRadius GetRadius() const { return m_radius; }
	bool SetRadius(const MediaRadius& radius) {
		SetRadiusAudio(radius.GetAudio());
		SetRadiusVideo(radius.GetVideo());
		return true;
	}
	double GetRadiusAudio() const { return m_radius.GetAudio(); }
	bool SetRadiusAudio(double radius = -1) { return m_radius.SetAudio(radius); }
	double GetRadiusVideo() const { return m_radius.GetVideo(); }
	bool SetRadiusVideo(double radius = -1) { return m_radius.SetVideo(radius); }
	bool IsRadiusGlobal() const { return m_radius.IsGlobal(); }

	void SetAllowScriptAccess(FlashScriptAccessMode mode) { m_allowScriptAccess = mode; }
	FlashScriptAccessMode GetAllowScriptAccess() const { return m_allowScriptAccess; }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	void UpdateParamsValid() { m_bParamsValid = !IsParamsPlaylistNone(); }

private:
	MODE m_mode; // media mode
	STATE m_state; // media state
	std::string m_url; // media url
	bool m_bParamsValid; // Whether media params are valid. Calculated when params changed to avoid performance impact measured when calculated on demand.
	std::string m_params; // media params
	double m_volPct; // media volume percent
	MediaRadius m_radius; // media radius
	int m_playlistId; // media playlist identifier (0=none)
	FlashScriptAccessMode m_allowScriptAccess;
};

} // namespace KEP