///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptArgManager.h"
#include <string>
#include <map>
#include <vector>

// Generic type to hold a value sent to or received from script.
class ScriptValue {
public:
	typedef std::string StringType;
	typedef std::map<std::string, ScriptValue> TableType;
	typedef std::vector<ScriptValue> ArrayType;
	using eValueType = IScriptArgManager::eValueType;

	ScriptValue() :
			m_Type(eValueType::None), m_Value{ false } { /*Construct(eValueType::None);*/
	}
	ScriptValue(const ScriptValue& sv) :
			ScriptValue() { operator=(sv); }
	ScriptValue(ScriptValue&& sv) :
			ScriptValue() { operator=(std::move(sv)); }
	~ScriptValue() { Destruct(); }

	// Move assignment
	ScriptValue& operator=(ScriptValue&& sv) {
		switch (sv.GetType()) {
			default:
			case eValueType::None: Clear(); break;
			case eValueType::Double: GetValueRef<double>() = sv.GetValueRef<double>(); break;
			case eValueType::Int: GetValueRef<int>() = sv.GetValueRef<int>(); break;
			case eValueType::Bool: GetValueRef<bool>() = sv.GetValueRef<bool>(); break;
			case eValueType::String: GetValueRef<StringType>() = std::move(sv.GetValueRef<StringType>()); break;
			case eValueType::Table: GetValueRef<TableType>() = std::move(sv.GetValueRef<TableType>()); break;
			case eValueType::Array: GetValueRef<ArrayType>() = std::move(sv.GetValueRef<ArrayType>()); break;
			case eValueType::Pointer: GetValueRef<void*>() = std::move(sv.GetValueRef<void*>()); break;
		}
		sv.Clear();
		return *this;
	}
	// Copy assignment
	ScriptValue& operator=(const ScriptValue& sv) {
		switch (sv.GetType()) {
			default:
			case eValueType::None: Clear(); break;
			case eValueType::Double: GetValueRef<double>() = sv.GetValueRef<double>(); break;
			case eValueType::Int: GetValueRef<int>() = sv.GetValueRef<int>(); break;
			case eValueType::Bool: GetValueRef<bool>() = sv.GetValueRef<bool>(); break;
			case eValueType::String: GetValueRef<StringType>() = sv.GetValueRef<StringType>(); break;
			case eValueType::Table: GetValueRef<TableType>() = sv.GetValueRef<TableType>(); break;
			case eValueType::Array: GetValueRef<ArrayType>() = sv.GetValueRef<ArrayType>(); break;
			case eValueType::Pointer: GetValueRef<void*>() = sv.GetValueRef<void*>(); break;
		}
		return *this;
	}

	// Destroys existing value and sets stored type to eValueType::None.
	void Clear() { Destruct(); }

	// Returns the type of the stored value.
	eValueType GetType() const { return m_Type; }

	// Returns nullptr if the stored value's type does not match the template parameter
	// otherwise returns a pointer to the value.
	template <typename T>
	const T* GetValuePointer() const {
		if (TypeTraits<T>::s_type != m_Type;)
			return nullptr;
		return &TypeTraits<T>::Get(*this);
	}

	// If the type requested is not the type stored, the stored value is destroy and a new
	// value of the requested type is default-constructed before returning a reference to it.
	template <typename T>
	const T& GetValueRef() const {
		return const_cast<ScriptValue*>(this)->GetValueRef<T>();
	}
	template <typename T>
	T& GetValueRef() {
		if (TypeTraits<T>::s_type != m_Type) {
			Destruct();
			Construct(TypeTraits<T>::s_type);
		}
		T& t = TypeTraits<T>::Get(*this);
		return t;
	}

	// Returns true if the stored type matches the template argument to the function.
	template <typename T>
	bool IsType() const {
		return TypeTraits<T>::s_type == m_Type;
	}

private:
	void Construct(eValueType type) {
		switch (type) {
			case eValueType::None: break;
			case eValueType::Double: break;
			case eValueType::Int: break;
			case eValueType::Bool: break;
			case eValueType::String: new (&m_Value.m_String) std::string; break;
			case eValueType::Table: new (&m_Value.m_Table) TableType(); break;
			case eValueType::Array: new (&m_Value.m_Array) ArrayType(); break;
			case eValueType::Pointer: break;
		}
		m_Type = type;
	}
	void Destruct() {
		switch (m_Type) {
			case eValueType::None: break;
			case eValueType::Double: break;
			case eValueType::Int: break;
			case eValueType::Bool: break;
			case eValueType::String: m_Value.m_String.~basic_string(); break;
			case eValueType::Table: m_Value.m_Table.~TableType(); break;
			case eValueType::Array: m_Value.m_Array.~ArrayType(); break;
			case eValueType::Pointer: break;
		}
		m_Type = eValueType::None;
	}
	union ValueUnion {
		~ValueUnion() {}
		double m_Double;
		int m_Int;
		bool m_Bool;
		void* m_Pointer;
		std::string m_String;
		TableType m_Table;
		ArrayType m_Array;
	};
	template <typename T>
	struct TypeTraits;
	template <>
	struct TypeTraits<bool> {
		static const eValueType s_type = eValueType::Bool;
		static bool& Get(ScriptValue& sv) { return sv.m_Value.m_Bool; }
	};
	template <>
	struct TypeTraits<int> {
		static const eValueType s_type = eValueType::Int;
		static int& Get(ScriptValue& sv) { return sv.m_Value.m_Int; }
	};
	template <>
	struct TypeTraits<double> {
		static const eValueType s_type = eValueType::Double;
		static double& Get(ScriptValue& sv) { return sv.m_Value.m_Double; }
	};
	template <>
	struct TypeTraits<std::string> {
		static const eValueType s_type = eValueType::String;
		static std::string& Get(ScriptValue& sv) { return sv.m_Value.m_String; }
	};
	template <>
	struct TypeTraits<TableType> {
		static const eValueType s_type = eValueType::Table;
		static TableType& Get(ScriptValue& sv) { return sv.m_Value.m_Table; }
	};
	template <>
	struct TypeTraits<ArrayType> {
		static const eValueType s_type = eValueType::Array;
		static ArrayType& Get(ScriptValue& sv) { return sv.m_Value.m_Array; }
	};
	template <>
	struct TypeTraits<void*> {
		static const eValueType s_type = eValueType::Pointer;
		static void*& Get(ScriptValue& sv) { return sv.m_Value.m_Pointer; }
	};

	eValueType m_Type;
	ValueUnion m_Value;
};
