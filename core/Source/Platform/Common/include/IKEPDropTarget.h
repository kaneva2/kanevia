///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

enum {
	DROP_NONE = 0,
	DROP_FILE = 1,
	DROP_TEXT = 2,
};

class IKEPDropTarget {
public:
	//! \brief Dragging cursor entering target
	//! \return true if willing to accept, false otherwise
	virtual bool DragEnter(int type,
		const std::string& fullPath,
		int mouseX,
		int mouseY,
		unsigned int keyState,
		unsigned int flags) = 0;

	//! \brief Dragging cursor moving over target
	virtual void DragOver(int mouseX,
		int mouseY,
		unsigned int keyState,
		unsigned int flags) = 0;

	//! \brief Dragging cursor leaving target
	virtual void DragLeave(void) = 0;

	//! \brief Dropped over target
	//! \return true if accepted, false if rejected
	virtual bool Drop(int type,
		const std::string& fullPath,
		int mouseX,
		int mouseY,
		unsigned int keyState,
		unsigned int flags) = 0;
};

} // namespace KEP