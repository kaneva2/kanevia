///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

// helper callback for sending data
class ISender {
public:
	//	static LONG MAX_OUTBOX_SIZE;
	virtual bool SendMsgUpdateToClient(CPlayerObject* pPO, bool sendForce) = 0;
	virtual bool SendMsgMoveToClient(CPlayerObject* pPO, bool sendForce) = 0;
};

} // namespace KEP