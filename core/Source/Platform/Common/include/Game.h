///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include <ISerializableObj.h>
#include <KEPConfig.h>
#include <string>
#include "..\KEPUtil\Algorithms.h"

namespace KEP {

class Game : public CObject, public GetSet, ISerializableObj {
public:
	Game(void);

	bool Load(const char* baseDir, const char* fname, bool svrCheck = true);
	void New(const char* pathName, const char* gameName);
	void Copy(const char* gameName);
	bool CanSave() const;

	// [in] fname if not null, do save as
	bool Save(const char* pathName = 0);

	~Game(void);

	BOOL IsDirty() const { return m_dirty || m_config.IsDirty(); }
	void SetDirty(BOOL dirty = TRUE) { m_dirty = dirty; }
	FileName GetLastSavedPath() const { return m_fileName; }

	CStringA FileNameCStr() const { return CStringA(m_fileName.str().c_str()); }
	CStringA Name() { return m_name; }
	CStringA Desc() { return m_desc; }
	CStringA Author() { return m_author; }
	CStringA Date() { return m_date; }
	CStringA Manifest() { return m_manifest; }
	CStringA SVR() { return m_svr; }
	CStringA Zone() { return m_loadedZone; }
	CStringA GameId() { return m_gameId; }
	CStringA ParentId() const { return m_parentId; }
	bool GetGameUserPass(CStringA& user, CStringA& password); // returns false if decrypt fails
	bool GetEncryptionKey(CStringA& key); // returns false if decrypt fails
	bool GetServerKey(CStringA& key); // returns false if decrypt fails
	CStringA GetLastDistroFolder() { return m_lastDistroFolder; }
	CStringA Version() const { return m_version; }
	bool GetLicenseDate(__time64_t& licenseDate);
	bool GetLicenseSecondsRemaining(int& secondsRemaining);
	bool IsSubGame() const { return atol(m_parentId) != 0; }

	const CStringA PathBase() const { return m_pathBase; }

	void SetName(CStringA s) { m_name = s; }
	void SetDesc(CStringA s) { m_desc = s; }
	void SetAuthor(CStringA s) { m_author = s; }
	void SetDate(CStringA s) { m_date = s; }
	void SetManifest(CStringA s) { m_manifest = s; }
	void SetSVR(CStringA s) { m_svr = s; }
	void SetPathBase(CStringA s) { m_pathBase = s; }
	void SetGameId(CStringA s) { m_gameId = s; }
	void SetParentId(CStringA s) { m_parentId = s; }
	void SetVersion(const char* v) { m_version = v; }
	bool SetGameUserPass(CStringA user, CStringA password); // returns false if encrypt fails
	bool SetEncryptionKey(CStringA key); // returns false if decrypt fails
	bool SetServerKey(CStringA key); // returns false if decrypt fails
	void SetLastDistroFolder(CStringA destDir) { m_lastDistroFolder = destDir; }
	bool SetLicenseDate(__time64_t date);
	bool SetLicenseSecondsRemaining(int seconds);

	void SetLoadedZone(CStringA s) { m_loadedZone = s; }

	// override to get/set encrypted values (only for editor)
	virtual string GetString(ULONG index);

	virtual void SetString(ULONG index, const char* s);

private:
	FileName m_fileName; // name of file loaded

	// variables loaded from file
	CStringA m_name;
	CStringA m_desc;
	CStringA m_author;
	CStringA m_date;
	CStringA m_manifest;
	CStringA m_svr;
	CStringA m_gameId;
	CStringA m_parentId;

	CStringA m_version;

	CStringA m_loadedZone;

	CStringA m_pathBase;

	BOOL m_dirty;

	KEPConfig m_config;

	CStringA m_user;
	CStringA m_password;
	CStringA m_encryptionKey;
	CStringA m_serverKey;
	CStringA m_lastDistroFolder;
	CStringA m_licenseDate;
	CStringA m_licenseSecondsRemaining;

	DECLARE_GETSET
};

} // namespace KEP
