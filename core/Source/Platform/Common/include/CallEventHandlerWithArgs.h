///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptEventArgs.h"
#include "Core/Util/FunctionTraits.h"
#include "Event/PlayListMgr/PLMMediaData.h"

// Standard event decoder which decodes next value from IReadableBuffer directly
template <typename EventType, typename ValueType>
void DecodeEvent(EventType* pEvent, KEP::IReadableBuffer* pReadBuffer, ValueType& val) {
	*pReadBuffer >> val;
}

// Call an function with arguments read from an IReadableBuffer. Arguments are decoded based on function signature.
template <typename EventType, size_t iArg, size_t nArgs, typename FunctionType, typename... Args>
std::enable_if_t<(iArg == nArgs), int> CallEventHandlerWithArgs(EventType* pEvent, KEP::IReadableBuffer* pReadBuffer, const FunctionType& func, const Args&... args) {
	int returnValue = 0;
	CallPossiblyVoidFunction(&returnValue, func, args...);
	return returnValue;
}

template <typename EventType, size_t iArg, size_t nArgs, typename FunctionType, typename... Args>
std::enable_if_t<(iArg < nArgs), int> CallEventHandlerWithArgs(EventType* pEvent, KEP::IReadableBuffer* pReadBuffer, const FunctionType& func, const Args&... args) {
	std::decay_t<NthArgTypeOfFunction_t<iArg, FunctionType>> arg;
	DecodeEvent(pEvent, pReadBuffer, arg);
	return CallEventHandlerWithArgs<EventType, iArg + 1, nArgs>(pEvent, pReadBuffer, func, args..., arg);
}

// Overloaded for calling directly with EventType* argument. Also allow additional non-event arguments passed in explicitly
template <typename EventType, typename FunctionType, typename... Args>
int CallEventHandlerWithArgs(EventType* pEvent, const FunctionType& func, const Args&... args) {
	constexpr size_t nBaseArgs = sizeof...(Args);
	constexpr size_t nArgs = NumberOfFunctionArguments<FunctionType>::value;
	return CallEventHandlerWithArgs<EventType, nBaseArgs, nArgs>(pEvent, pEvent->InBuffer(), func, args...);
}

//========================================================================
// Specialized for decoding certain values from a ScriptServerEvent
template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, KEP::ScriptServerEvent*& val) {
	val = pEvent;
}

template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, KEP::ScriptServerEvent::EventType& val) {
	val = static_cast<KEP::ScriptServerEvent::EventType>(pEvent->Filter());
}

template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, KEP::ScriptPIDType& val) {
	assert(pEvent->isDecoded());
	val.value = pEvent->getObjPlacementId();
}

template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, KEP::ScriptNETIDType& val) {
	assert(pEvent->isDecoded());
	val.value = pEvent->From();
}

template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, KEP::ZoneIndex& zi) {
	assert(pEvent->isDecoded());
	int zoneId = pEvent->getZoneId();
	int instanceId = pEvent->getInstanceId();
	zi.fromLong(zoneId);
	if (!zi.isInstanced() && instanceId != 0) {
		zi.setInstanceId(instanceId);
	}
}

template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, KEP::ScriptPlaylistID& playlistId) {
	playlistId.value = 0;

	KEP::PlaylistAssets* pPlaylistAssets = pEvent->GetPlaylistAssets();
	if (pPlaylistAssets == nullptr) {
		return;
	}

	playlistId.value = pPlaylistAssets->getPlaylistId();
}

template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, KEP::PlaylistAsset*& pla) {
	pla = nullptr;

	KEP::PlaylistAssets* pPlaylistAssets = pEvent->GetPlaylistAssets();
	if (pPlaylistAssets == nullptr) {
		return;
	}

	pla = pPlaylistAssets->getAssetList()[pPlaylistAssets->getNextMediaIndex()];
}

template <>
void DecodeEvent(KEP::ScriptServerEvent* pEvent, KEP::IReadableBuffer* pReadBuffer, std::vector<KEP::PlaylistAsset*>& playlistAssetVector) {
	KEP::PlaylistAssets* pPlaylistAssets = pEvent->GetPlaylistAssets();
	if (pPlaylistAssets == nullptr) {
		return;
	}

	for (const auto& pAsset : pPlaylistAssets->getAssetList()) {
		playlistAssetVector.push_back(pAsset);
	}
}
