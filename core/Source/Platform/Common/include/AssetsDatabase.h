///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IAssetsDatabase.h"

namespace KEP {

struct NamingSyntax;
class CBoneObject;

class CSimpleImporterFactory : public IAssetImporterFactory {
protected:
	// table of importers
	struct IAssetImporter_set : std::set<IAssetImporter*> {}; // Use struct instead of typedef to avoid C4503 warning
	typedef std::map<std::string, IAssetImporter_set> ExtensionToImporters_map;
	typedef std::map<KEP_ASSET_TYPE, ExtensionToImporters_map> AssetTypeToExts_map;
	typedef std::map<KEP_ASSET_TYPE, std::string> AssetTypeToString_map;
	typedef std::map<std::string, IAssetImporter*> NameToImporter_map;
	AssetTypeToExts_map importerTable;
	AssetTypeToString_map filterTable;
	IAssetImporter_set allImportersSet;
	NameToImporter_map allImporters;
	//@}

	// table of importer impl's
	typedef std::multimap<IAssetImporter*, IAssetImporterImpl*> AssetImplMap;
	AssetImplMap allImpls;

public:
	CSimpleImporterFactory() {}
	virtual ~CSimpleImporterFactory() {}

	/** @name AssetImporter Registration */
	//@{
	//! Register the asset importer with the AssetsDatabase, for a particular extension and asset type.
	//! Call this function as many times as needed for each extension and asset type supported
	void RegisterImporter(IAssetImporter* aAssetImporter, KEP_ASSET_TYPE aAssetType, const std::string& aExtension) override;

	//! Unregister the asset importer with the AssetsDatabase, for a particular extension and asset type.
	//! Call this function as many times as needed to discontinue support of a particular format
	void UnregisterImporter(IAssetImporter* aAssetImporter, KEP_ASSET_TYPE aAssetType, const std::string& aExtension) override;

	//! Unregister the asset importer with the AssetsDatabase.
	//! Removes all the entries that refer to this importer
	void UnregisterImporter(IAssetImporter* aAssetImporter) override;
	//@}

	//! Return, for a particular asset type, a filter to be used in a file open dialog
	void GetOpenDialogFilter(KEP_ASSET_TYPE aAssetType, std::string& aExtensionsList) override;
	void GetOpenDialogFilter(std::set<KEP_ASSET_TYPE> aAssetTypes, std::string& aExtensionsList) override;

	//! Return an appropriate for the assets specified by the URI, return NULL if no matches.
	IAssetImporter* GetImporter(KEP_ASSET_TYPE aAssetType, const std::string& aURI, int aFlags = 0) override;

	void RegisterImporterImpl(IAssetImporterImpl* aImpl, const std::string& aImporterName) override;
	void UnregisterImporterImpl(IAssetImporterImpl* aImpl, const std::string& aImporterName) override;
	void UnregisterImporterImpl(IAssetImporterImpl* aImpl) override;
	std::set<IAssetImporterImpl*> GetImporterImpl(IAssetImporter* aImporter) override;

	// Make abstract class a friend so that its CreateInstance member function can instantiate this class.
	friend class IAssetImporterFactory;
};

class CAssetsDatabase : public IAssetsDatabase {
protected:
	NamingSyntax* namingSyntaxs;

protected:
	CSkeletonObject* ImportSkeletalObjectFromLegacyImporter(const std::string& aURI, int aFlags, CSkeletonObject* pDstObj, IAssetImporter* pImporter);
	//BOOL PadAnimationDataForImmobileBones(RuntimeSkeleton& aSkeletalObject, UINT64 aAnimHash);
	int DoCompareSkeletons(const RuntimeSkeleton& first, const CBoneObject& firstRoot, const RuntimeSkeleton& second, const CBoneObject& secondRoot, int& numBonesCompared);

public:
	//! \brief Compare two skeletons
	//! \return 1 if first skeleton contains second, -1 if error found, 0 otherwise.
	int CompareSkeletons(RuntimeSkeleton& first, RuntimeSkeleton& second) override;

	//! \brief Merge two skeletons
	BOOL MergeSkeletalObjects(CSkeletonObject& toObject, CSkeletonObject& fromObject, int aSectionIndex, int aConfigIndex, unsigned int aLodIndex, int aFlags) override;

	//! \brief Remap bone indices in vertex assignments from old skeleton to new skeleton.
	//! \param dmesh Deformable mesh to be converted
	//! \param sourceSkeleton Source skeleton where the deformable mesh is part of.
	//! \param targetSkeleton Target skeleton where the deformable mesh is converted into.
	//! \param unmatchedBones Returns a list of unmatched bones.
	//! \return FALSE if there is unmatched bones. TRUE otherwise.
	BOOL RemapDeformableMeshBones(CDeformableMesh& dmesh, RuntimeSkeleton& sourceSkeleton, RuntimeSkeleton& targetSkeleton, std::set<std::string>& unmatchedBones) override;

public:
	//	static IGameDirProvider* pGDP;	//@@Watch: temporarily solution to call GameBaseDir(), will be replaced later with a more versitile way.

	CAssetsDatabase();
	virtual ~CAssetsDatabase();

	/** @name Asset Importing
	*	These functions attempt the import of an asset from the specified file
	*	If there is a library to store this particular type of asset,
	*	  the function will check if an asset with the same file name already exists
	*	  if found it will only replace it if the "replace" parameter is true
	*	They return a valid pointer upon success, 0 otherwise
	*/
	//@{
	//! Try importing a KEP_STATIC_MESH
	AssetData ImportStaticMesh(
		const std::string& aFileName, int aFlags = IMP_STATIC_LEGACY_FLAGS,
		IAssetImporter* aImporter = NULL) override;

	//! Try importing a list of KEP_STATIC_MESH'es
	AssetTree* ImportStaticMeshes(
		const std::string& aFilename, int aFlags,
		IClassifyAssetNode* classifyFunc = NULL, IAssetImporter* aImporter = NULL) override;

	//! Try importing a list of KEP_STATIC_MESH'es with the help of an asset tree (from a previous preview operation)
	bool ImportStaticMeshesByAssetTree(
		const std::string& aFilename, int aFlags,
		AssetTree* assetTree, IAssetImporter* apImporter = NULL) override;

	//! Try importing one single KEP_STATIC_MESH using asset definition obtained previously
	bool ImportStaticMeshByAssetDefinition(
		const std::string& aFilename, int aFlags,
		AssetData& aDef, int subID = 0, IAssetImporter* apImporter = NULL) override;

	//! Try importing a KEP_SKA_ANIM_OBJECT
	CSkeletonObject* ImportSkeletalObject(
		const std::string& aFileName, int aFlags = IMP_SKELETAL_LEGACY_FLAGS,
		CSkeletonObject* pDstObj = NULL, int dstSection = -1, int dstConfig = -1, int dstLOD = -1,
		IClassifyAssetNode* classifyFunc = NULL, IAssetImporter* aImporter = NULL) override;

	//! Try importing a skeletal object with the help of an asset tree (from a previous preview operation)
	CSkeletonObject* ImportSkeletalObjectByAssetTree(
		const std::string& aFileName, int aFlags,
		AssetTree* assetTree, IAssetImporter* apImporter = NULL) override;

	//! Try importing a KEP_TEXTURE
	TextureDef ImportTexture(const std::string& uri, int flags = 0) override;

	//! Query metadata for imported texture
	bool GetTextureImportInfo(TextureDef& textureInfo, const std::string& importedFileName) override;

	/** @name Additional skeletal animation import functions.
	* These functions try to import skeletal animation data of different types
	*  to an existing skeletal animation object which is already initialized
	* The functions return true upon success and false otherwise
	*/

	//! Imports a deformable mesh in the file into the given position
	//! If any *Index is < 0 or not found appending occurs at the appropriate list
	bool ImportSkinnedMesh(
		CSkeletonObject& aSkeletalObject, const std::string& aFileName,
		int aSectionIndex, int aConfigIndex, int aLodIndex,
		int aFlags = SKELETAL_APPENDTO_SECTION | SKELETAL_APPENDTO_CONFIG | SKELETAL_REPLACE_LOD,
		IAssetImporter* aImporter = NULL) override;

	//! Imports a rigid mesh in the file and attaches it to the bone at the given index as a
	//! rigid mesh at the given LOD. No loading occurs if the bone is not found.
	//! If aLodexIndex < 0 or not found the mesh is appended to the LOD list.
	bool ImportRigidMesh(
		RuntimeSkeleton& aSkeletalObject,
		const std::string& aFileName, int aBoneIndex, int aLodIndex, int aFlags = SKELETAL_REPLACE_LOD,
		IAssetImporter* aImporter = NULL) override;

	//! Imports animation data into the given skeletal animated object
	//! Data will only be added if all the bones in the file match (by name) those in the given object
	//! If data is missing for some of the bones it will be assumed to be equal to the default
	//!  transformation for that bone
	//! If aAnimationIndex < 0 or not found the animation data is appended to the list of animations.
	bool ImportAnimation(
		CSkeletonObject& aSkeletalObject,
		const std::string& aURI, int& aAnimGLID, int aFlags = SKELETAL_REPLACE_ANIMATION,
		const AnimImportOptions* apOptions = NULL, IAssetImporter* aImporter = NULL) override;

	//! Try importing one single animation using asset definition obtained previously
	bool ImportAnimationByAssetDefinition(
		CSkeletonObject& aSkeletalObject,
		const std::string& aURI, int& aAnimGLID, int aFlags,
		AssetData& aDef, const AnimImportOptions* apOptions = NULL, IAssetImporter* apImporter = NULL) override;

	CVertexAnimationModule* ImportVertexAnimation(
		CEXMeshObj& aMesh,
		const std::string& aFileName, int aFlags,
		IClassifyAssetNode* classifyFunc = NULL, IAssetImporter* aImporter = NULL) override;

	BOOL ParseAssetName(KEP_ASSET_TYPE assetType, const std::string& name, char& type, std::string& stem, std::string& slotOrPart, std::string& dimVar, int& lod) override;
	BOOL ParseAnimationName(const std::string& animID, std::string& animName, int& firstFrame, int& lastFrame, int& fps) override;
	std::string MakeAnimationName(const std::string& animName, int firstFrame = 0, int lastFrame = -1, int fps = -1) override;

	AssetTree* OpenAndPreviewURI(const std::string& aFileName, KEP_ASSET_TYPE aAssetType, int aFlags, IClassifyAssetNode* classifyFunc) override;
	void CloseURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags) override;
};

} // namespace KEP
