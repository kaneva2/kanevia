///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

//	From the following MSDN resource
// 	https://msdn.microsoft.com/en-us/library/xcb2z8hs.aspx

// Usage: SetThreadName ("MainThread");

const DWORD MS_VC_EXCEPTION = 0x406D1388;
typedef struct tagTHREADNAME_INFO {
	DWORD dwType; // must be 0x1000
	const char* szName; // pointer to name (in user addr space)
	DWORD dwThreadID; // thread ID (-1=caller thread)
	DWORD dwFlags; // reserved for future use, must be zero
} THREADNAME_INFO;

inline void SetThreadName(const char* szThreadName, DWORD dwThreadID = -1) {
#ifdef _DEBUG

	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = szThreadName;
	info.dwThreadID = dwThreadID;
	info.dwFlags = 0;

	__try {
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(DWORD), (DWORD*)&info);
	} __except (EXCEPTION_CONTINUE_EXECUTION) {
	}

	szThreadName; // DRF - unreferenced formal parameters
	dwThreadID;

#endif // NDEBUG
}
