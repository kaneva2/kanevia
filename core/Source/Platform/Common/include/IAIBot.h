/******************************************************************************
 IAIBot.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "ChatType.h"
#include "KEPNetwork.h"
#include "IGetSet.h"
#include "Core/Util/HighPrecisionTime.h"
#include <vector>
#include <utility> // for pair

namespace KEP {

class IPlayer;

typedef std::pair<float, IGetSet *> AITarget; // distance, object
typedef std::vector<AITarget> AITargetVector;

/***********************************************************
CLASS

	IAIBot 
	
	interface object for AI bots

DESCRIPTION
	This wraps some things in the CMovementObj and CAIObj
	that it contained within the CMovementObj

***********************************************************/
class IAIBot : public IGetSet {
public:
	// version of this class interface
	static BYTE Version() { return 1; }

	//  tells the server to set the AI to decay
	virtual void SetDissipateFlag() = 0;

	//  sets the decay minutes for the corpse GetSet � The entire Bot should be GetSet, so that it can be changed in real time on the server
	// now in CMP, not per bot virtual void SetDissipateTime( IN USHORT minutes)  = 0;

	// return dpnid, netid, name, distance, highest skill, team, race, clan, edb, ai_controlled, etc..
	virtual void GetPossibleTargets(OUT AITargetVector &targets) = 0;

	// set to anything to help them track state on AI
	// getset AIObj m_desiredGoal virtual void SetDesiredGoal( IN USHORT goal ) = 0;

	//  AI looks at a position
	virtual void LookAt(IN double x, IN double y, IN double z) = 0;

	//  sets the current target for an AI
	// GetSet virtual void SetCurrentTarget( IN NETID netid, IN EAIAction action )  = 0;

	//  clears all actions and erases all targets
	// GetSet virtual void ReleaseCurrentTarget()  = 0;

	//  checks the collision to see if they can see this target
	virtual bool IsMyTargetInSight() = 0;

	// set my target
	virtual void SetTarget(IN IGetSet *target, IN bool insight) = 0;

	// clear my target
	virtual void ClearTarget() = 0;

	//  sets the AI in motion to a location
	virtual void MoveToLocation(IN double x, IN double y, IN double z) = 0;

	//  sets the AI in motion to a target
	virtual void MoveToCurrentTarget() = 0;

	//  sets the AI in motion to retreat from a target
	virtual void RetreatFromCurrentTarget() = 0;

	//  returns the x,y,z of a AI tree node, -1,-1,-1 if not found
	virtual void GetTreeNodePosition(IN LONG nodeIndex, OUT double &x, OUT double &y, OUT double &z) = 0;

	//  moves the AI to a desired tree location
	virtual bool GotoTreeNode(IN LONG nodeIndex) = 0;

	//  does a magnitudesquared on input to get a distance.
	virtual double CalculateDistance(IN double x1, IN double y1, IN double z1, IN double x2, IN double y2, IN double z2) = 0;

	//  strafes and zig zags to a location
	virtual void MoveToLocationDefensively(IN double x, IN double y, IN double z) = 0;

	//  changes the animation for an AI
	// GetSet virtual void SetCurrentAnimation( IN ULONG animationIndex )  = 0;

	//  Makes the AI seem to talk
	// future virtual void Speak( IN const char* message, IN chatType )  = 0;

	// added to avoid tons of script coding
	virtual void Transform() = 0;
	virtual void SetUseStatesToFalse() = 0;
	virtual bool VerifyTarget() = 0;
	virtual bool OnWall() = 0;
	virtual void Attack(TimeMs currentTime) = 0;
	virtual void Retreat() = 0;
	virtual void TravelLookBasis(TimeMs currentTime) = 0;
	virtual void TreeWebUse() = 0;
	virtual void HandleScript() = 0;
	virtual void EvaluateAIVisionToTargetDest(TimeMs currentTime) = 0;
	virtual void ClearRoute() = 0;
};

} // namespace KEP
