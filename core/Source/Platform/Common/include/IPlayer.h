#pragma once

#include "IKEPObject.h"
#include <vector>
#include <utility> // for pair
#include <string>
#include "Core/Math/Vector.h"

namespace KEP {

class IPlayer;

const int AVAILABLE = 0x00000000;
const int MISC_BUSY = 0x00000001;
const int TRADING = 0x00000002;
const int DANCE_PARTY = 0x00000004;
const int KACHING = 0x00000008;
const int P2PANIM = 0x00000010;

class IPlayer : public IKEPObject {
public:
	typedef std::pair<long, long> StatBonus; // ID, bonus
	typedef std::vector<StatBonus> StatBonusVector;

	virtual std::string ToStr(bool verbose = true) const = 0;

	virtual int GetNetTraceId() = 0;
	virtual Vector3f getLocation() const = 0;
};

} // namespace KEP
