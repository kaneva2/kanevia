///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include <string>

enum MenuCategory;

namespace KEP {

//
// Get rid of this?  KepMenu can map IMenuDialog to filename.
//
class CMenuList {
protected:
	CMenuList() {}

public:
	static CMenuList* GetInstance();

	virtual ~CMenuList();

	void Load(int gameId, bool isChildGame, bool interactive);

	// Enumerate menus and scripts
	int GetNumMenus(int categoryFilter) const { return CountAssets(m_menus, categoryFilter); }
	std::string GetMenuNameByIndex(int categoryFilter, int idx) const { return GetAssetName(m_menus, categoryFilter, idx); }
	int GetNumScripts(int categoryFilter) const { return CountAssets(m_scripts, categoryFilter); }
	std::string GetScriptNameByIndex(int categoryFilter, int idx) const { return GetAssetName(m_scripts, categoryFilter, idx); }

	std::string GetFilenameByID(int menuID); // Used by ClientEngine to lookup dialog by <ID> tag

private:
	struct AssetDef {
		MenuCategory category;
		std::string name;
		int ID; // for menus only

		AssetDef(MenuCategory cat, std::string name, int ID = 0) :
				category(cat), name(name), ID(ID) {}
	};

	typedef std::vector<AssetDef>::const_iterator AssetIterator;
	std::vector<AssetDef> m_menus, m_scripts;

	static CMenuList* m_pInstance;

	void RemoveAll() {
		m_menus.clear();
		m_scripts.clear();
	}
	void LoadFromFolder(MenuCategory category, const std::string& file_mask, bool interactive);
	void LoadScriptsFromFolder(MenuCategory category, const std::string& file_mask);

	int CountAssets(const std::vector<AssetDef>& assets, int categoryFilter) const;
	std::string GetAssetName(const std::vector<AssetDef>& assets, int categoryFilter, int index) const;

	// retruns 0 if ok, conflicting menu otherwise
	std::string Add(MenuCategory category, std::string filename, int menuID = 0);
};

} // namespace KEP