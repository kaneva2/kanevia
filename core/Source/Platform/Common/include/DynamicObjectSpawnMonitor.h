///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "Core/Math/Vector.h"
#include "Glids.h"

#define SPAWN_WAIT 30000
#define CHECK_RADIUS 25

namespace KEP {

class CDynamicPlacementObj;

class DynamicObjectSpawnMonitor {
public:
	DynamicObjectSpawnMonitor();
	~DynamicObjectSpawnMonitor();

	bool IsSpawning() const { return m_isSpawning; };
	void SetSpawning();
	void CustomizationComplete(int dynamicObjects);
	void CheckAddDynObjToMonitor(CDynamicPlacementObj* dynObj, Vector3f playerPos);
	void CheckDynObjLoadComplete(const GLID& glid);

private:
	bool AABB_Sphere_Test(Vector3f aabbMin, Vector3f aabbMax, Vector3f center, float radius);

	std::set<GLID> m_DOSet;
	bool m_isSpawning;
	bool m_customizationComplete;
	TimeMs m_spawnStart;

	void completeSpawn();
};

} // namespace KEP
