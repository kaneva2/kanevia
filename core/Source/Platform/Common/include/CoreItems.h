// Core item GLIDs (see CoreItems.lua)
#pragma once
#define GLID_EI_POSE_MALE 1 // Male avatar pose for accessory import
#define GLID_EI_POSE_FEMALE 2 // Female avatar pose for accessory import
#define GLID_RESTING_POSE_MALE 3 // Male avatar pose for accessory import - person space box check
#define GLID_RESTING_POSE_FEMALE 4 // Female avatar pose for accessory import - person space box check
#define GLID_NULL_OBJECT 5 // A tiny object
#define GLID_PAPERDOLL_NPC 6 // Placeholder for "placed" paper doll NPC
#define GLID_MALE_AVATAR 7 // Used in ZoneLoader.lua - avatar substitute
#define GLID_FEMALE_AVATAR 8 // Used in ZoneLoader.lua - avatar substitute
#define GLID_GEM_INDICATOR 9 // Gem indicator for the old smart objects
