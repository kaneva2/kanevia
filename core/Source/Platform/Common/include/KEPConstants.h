///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "limits.h"

#include "glids.h"
#include "ObjectType.h"

#include "common\include\CoreStatic\CBoneAnimationNaming.h"

// Data Limits
#define MAX_UINT UINT_MAX //(~(unsigned int)0)
#define MAX_INT INT_MAX //(MAX_UINT>>1)
#define MAX_ULONG ULONG_MAX //(~(unsigned long)0)
#define MAX_LONG LONG_MAX //(MAX_ULONG>>1)

const float c_frameTimeTargetMs = 16.0f; // 62.5 fps (slightly more than 60 to avoid roundoff issues relative to vsync)

// Skeletal Entity Constants
enum {
	MT_NONE,
	MT_RIDE,
	MT_STUNT
}; // mount type

// Object Visibility
enum class eVisibility {
	VISIBLE,
	HIDDEN_FOR_ALL,
	HIDDEN_FOR_ME
};

// max value the editor will export into supported worlds.
const ULONG MAX_KANEVA_SUPPORTED_WORLD = 1000;

// ControlDynamicObjectEvent Filters - Also Update KEP.lua!
enum CONTROL_DYNAMIC_OBJECT_EVENT_FILTER {
	ANIM_START = 0x0001,
	ANIM_STOP = 0x0002,
	FRICTION_ENABLE = 0x0003,
	FRICTION_DISABLE = 0x0004,
	OBJECT_SHOW = 0x0008,
	OBJECT_HIDE = 0x0009,
	SOUND_CONFIG = 0x0010,
	SOUND_START = 0x0011,
	SOUND_STOP = 0x0012,
	PARTICLE_START = 0x0013,
	PARTICLE_STOP = 0x0014
};

// Dynamic placement types
enum {
	Placement_DynObj,
	Placement_Frame,
	Placement_Sound
};

const UINT c_numLocomotionAnimTypes = 4;
const eAnimType c_locomotionAnimTypes[] = {
	eAnimType::Stand,
	eAnimType::Run,
	eAnimType::Walk,
	eAnimType::Jump
};

/////////////////////////////////////////////////////////////////
// Available path finder types
enum PathFinderType {
	PF_SIMPLE = 0,
};

/////////////////////////////////////////////////////////////////
// EDB avatar index (matching entities.edb and CharacterCreation2.lua)
enum class EDBIndex {
	MALE_NPC = 3,
	FEMALE_NPC = 4,
	MALE_THIN = 13, // "Jimmy"
	MALE_MED = 14, // "Hondo"
	MALE_BIG = 15, // "Meaty"
	FEMALE_THIN = 10, // "Isabella"
	FEMALE_MED = 11, // "Sofia"
	FEMALE_BIG = 12, // "Eva"
};

#define IS_STD_MALE_AVATAR(modelIdx) ((modelIdx) == (int)EDBIndex::MALE_THIN || (modelIdx) == (int)EDBIndex::MALE_MED || (modelIdx) == (int)EDBIndex::MALE_BIG)
#define IS_STD_FEMALE_AVATAR(modelIdx) ((modelIdx) == (int)EDBIndex::FEMALE_THIN || (modelIdx) == (int)EDBIndex::FEMALE_MED || (modelIdx) == (int)EDBIndex::FEMALE_BIG)

/////////////////////////////////////////////////////////////////
// Some important EI sockets (aka location occupancy, exclusion group, etc)
enum {
	EI_HEAD_ONLY = 11,
	EI_HAIR = 27,
	EI_HEAD_W_HAIR = 28,
	EI_WEAPON = 12,
};

#define IS_HEAD_ITEM(s) ((s) == EI_HEAD_ONLY || (s) == EI_HEAD_W_HAIR)
#define IS_HAIR_ITEM(s) ((s) == EI_HAIR || (s) == EI_HEAD_W_HAIR)

enum ABILITY_TYPES {
	AT_WEAPON = 12,
};

enum DISCONNECT_DIAG_CODE {
	DISC_DIAG_NOERROR = 0,
	DISC_DIAG_VERINFO_NOTFOUND = 100,
	DISC_DIAG_VERINFO_NOACCESS,
	DISC_DIAG_VERINFO_EMPTY,
	DISC_DIAG_VERINFO_NOVERSION,
	DISC_DIAG_VERINFO_ZEROVERSION,
	DISC_DIAG_VERINFO_BADVERSION,
	DISC_DIAG_VERINFO_OTHERISSUES,
};

enum MenuCategory {
	MENU_ALL = 0, // Everything
	MENU_WOK_PATCH_ONLY = 1, // Published WOK menus
	MENU_WOK_LOCALDEV_ONLY = 16, // LocalDev WOK Menus
	MENU_WORLD_TEMPLATE_ONLY = 2, // Template menus for current world
	MENU_WORLD_CUSTOM_ONLY = 4, // Published custom menus for current world
	MENU_WORLD_LOCALDEV_ONLY = 8, // Unpublished custom menus for current world

	MENU_WOK = MENU_WOK_PATCH_ONLY | MENU_WOK_LOCALDEV_ONLY, // All WOK Menus
	MENU_WORLD = MENU_WORLD_TEMPLATE_ONLY | MENU_WORLD_CUSTOM_ONLY | MENU_WORLD_LOCALDEV_ONLY, // All menus for current world
};

/// DRF - Cursor Mode Enumeration (also update in CommonFunctions.lua)
enum CURSOR_MODE {
	CURSOR_MODE_NONE, // no cursor
	CURSOR_MODE_MENU, // cursor is over menu (priority 1)
	CURSOR_MODE_WIDGET, // widget is visible (priority 2)
	CURSOR_MODE_OBJECT, // cursor is over object (priority 3)
	CURSOR_MODE_WORLD, // everything else (priority 4)
	CURSOR_MODES
};

inline const char* CursorModeStr(const CURSOR_MODE& cursorMode) {
	const char* cursorModeStr[CURSOR_MODES] = {
		"MODE_NONE",
		"MODE_MENU",
		"MODE_WIDGET",
		"MODE_OBJECT",
		"MODE_WORLD"
	};
	if ((int)cursorMode >= CURSOR_MODES)
		return "MODE_ERROR";
	return cursorModeStr[(int)cursorMode];
}

/// DRF - Cursor Type Enumeration (also update in CommonFunctions.lua)
enum CURSOR_TYPE {
	CURSOR_TYPE_NONE,
	CURSOR_TYPE_DEFAULT,
	CURSOR_TYPE_CLICKABLE,
	CURSOR_TYPE_DRAGGABLE,
	CURSOR_TYPE_ROTATABLE,
	CURSOR_TYPE_TRANSLATABLE,
	CURSOR_TYPE_ATTACKABLE,
	CURSOR_TYPE_ATTACKABLE2,
	CURSOR_TYPE_HEALABLE,
	CURSOR_TYPES
};

inline const char* CursorTypeStr(const CURSOR_TYPE& cursorType) {
	const char* cursorTypeStr[CURSOR_TYPES] = {
		"TYPE_NONE",
		"TYPE_DEFAULT",
		"TYPE_CLICKABLE",
		"TYPE_DRAGGABLE",
		"TYPE_ROTATABLE",
		"TYPE_TRANSLATABLE",
		"TYPE_ATTACKABLE",
		"TYPE_ATTACKABLE2",
		"TYPE_HEALABLE",
	};
	if ((int)cursorType >= CURSOR_TYPES)
		return "TYPE_ERROR";
	return cursorTypeStr[(int)cursorType];
}

inline const char* CursorFile(const CURSOR_TYPE& cursorType) {
	const char* cursorFiles[CURSOR_TYPES] = {
		"", // TYPE_NONE
		"Cursor.dds", // TYPE_DEFAULT
		"ClickableCursor.dds", // TYPE_CLICKABLE
		"DraggableCursor.dds", // TYPE_DRAGGABLE
		"rotatecursor_32x32_DXT1BA.dds", // TYPE_ROTATABLE
		"movecursor_32x32_DXT1BA.dds", // TYPE_TRANSLATABLE
		"attackNormalCursor.dds", // TYPE_ATTACKABLE
		"attackCanAttackCursor.dds", // TYPE_ATTACKABLE2
		"HealCanHealCursor.dds", // TYPE_HEALABLE
	};
	if ((int)cursorType >= CURSOR_TYPES)
		return cursorFiles[CURSOR_TYPE_DEFAULT];
	return cursorFiles[(int)cursorType];
}

#ifdef _WINDOWS_
static const POINT CURSOR_HOTSPOT[CURSOR_TYPES] = {
	{ 0, 0 }, // CURSOR_TYPE_NONE
	{ 12, 6 }, // CURSOR_TYPE_DEFAULT
	{ 9, 6 }, // CURSOR_TYPE_CLICKABLE
	{ 13, 12 }, // CURSOR_TYPE_DRAGGABLE
	{ 12, 6 }, // CURSOR_TYPE_ROTATABLE - drf - same cursor file as CURSOR_TYPE_DEFAULT
	{ 12, 6 }, // CURSOR_TYPE_TRANSLATABLE - drf - same cursor file as CURSOR_TYPE_DEFAULT
	{ 15, 15 }, // CURSOR_TYPE_ATTACKABLE
	{ 15, 15 }, // CURSOR_TYPE_ATTACKABLE 2
	{ 15, 15 } // CURSOR_TYPE_HEALABLE
};
#endif

/// DRF - CreateImmediateModeDevice() Error Codes
enum D3D_ERROR {
	D3D_SUCCESS = 0,
	D3D_ERROR_NULL = 1,
	D3D_ERROR_NO_COMPATIBLE_DEVICE = 2,
	D3D_ERROR_FAILED_DISPLAY_MODE = 3,
	D3D_ERROR_NO_LOGGER = 4,
	D3D_ERROR_CREATE_DEVICE = 5,
	D3D_ERROR_CREATE_DRAW = 6
};

// Game Item Stuff
#define INVALID_GAMEITEM 0

// zone privileges, lower number more priv, these numbers are from
// the Kaneva web site
const BYTE ZP_UNKNOWN = 0xff; // not a valid value
const BYTE ZP_NONE = 3; // subscriber in db
const BYTE ZP_MODERATOR = 2;
const BYTE ZP_OWNER = 1;
const BYTE ZP_GM = 0; // not in DB, but allows for <= ZP_OWNER check

// invalid protocol version
#define INVALID_PROTOCOL_VERSION 0
#define MIN_PROTOCOL_NEGO_VERSION 0x8027 // Minimum protocol version supporting version negotiation

// Effect types for effect track
enum class EffectCode {
	EFX_NULL = -1,
	EFX_ANIMATION = 0,
	EFX_SOUND = 1,
	EFX_PARTICLE = 2,
	EFX_SOUND_EXISTING = 3, // EFFECT_CODES.SOUND_OBJECT
	EFX_OBJECT_MOVE = 4,
	EFX_OBJECT_ROTATE = 5,
};

// Effect arguments
enum EffectArgIndex {
	EFX_CODE,
	EFX_STARTTIME,
	EFX_NUM_REQUIRED_ARGS,
	EFX_STOPTIME = EFX_STARTTIME + 1,

	EFX_ANM_v0_GLID = EFX_STARTTIME + 1,
	EFX_ANM_v0_NUM_ARGS, // Deprecated
	EFX_SND_v0_GLID = EFX_STARTTIME + 1,
	EFX_SND_v0_NUM_ARGS, // Deprecated
	EFX_SNX_v0_PID = EFX_STARTTIME + 1,
	EFX_SNX_v0_NUM_ARGS, // Deprecated

	EFX_ANM_GLID = EFX_STOPTIME + 1,
	EFX_ANM_NUM_ARGS, // New animation effect arguments
	EFX_SND_GLID = EFX_STOPTIME + 1,
	EFX_SND_NUM_ARGS, // New sound effect arguments
	EFX_SNX_PID = EFX_STOPTIME + 1,
	EFX_SNX_NUM_ARGS, // New existing sound effect arguments

	EFX_PTC_BONE = EFX_STOPTIME + 1,
	EFX_PTC_SLOT,
	EFX_PTC_GLID,
	EFX_PTC_NUM_ARGS_BASE, // Base particle effect
	EFX_PTC_OFV_X = EFX_PTC_NUM_ARGS_BASE,
	EFX_PTC_OFV_Y,
	EFX_PTC_OFV_Z,
	EFX_PTC_NUM_ARGS_WOFV, // Optional arguments for offset vector from bone (or root)
	EFX_PTC_FWD_X = EFX_PTC_NUM_ARGS_WOFV,
	EFX_PTC_FWD_Y,
	EFX_PTC_FWD_Z,
	EFX_PTC_NUM_ARGS_WFWD, // Optional arguments for forward direction
	EFX_PTC_UPD_X = EFX_PTC_NUM_ARGS_WFWD,
	EFX_PTC_UPD_Y,
	EFX_PTC_UPD_Z,
	EFX_PTC_NUM_ARGS, // Optional arguments for up direction

	EFX_MOVROT_DURATION = EFX_STARTTIME + 1,
	EFX_MOVROT_X,
	EFX_MOVROT_Y,
	EFX_MOVROT_Z,
	EFX_MOVROT_NUM_ARGS,

	EFX_MAX_ARGS = 99,
	EFX_MAX_EFFECTS = 99, // For sanity check
};

// Server visibility (see GameServer.cs)
enum class ServerVisibility {
	Unknown = 0,
	Public = 1,
	Private = 2,
};

// DRF - Time Conversions
#define MS_PER_SEC (TimeMs)1000
#define SEC_PER_MIN (TimeSec)60
#define MIN_PER_HOUR 60

// DRF - Memory Conversions
#define BYTES_PER_KB 1024.0
#define KB_PER_MB 1024.0
#define MB_PER_GB 1024.0
#define BYTES_PER_MB (BYTES_PER_KB * KB_PER_MB)
#define BYTES_PER_GB (BYTES_PER_MB * MB_PER_GB)

#define SKELETAL_LODS_MAX 6
#define SKELETAL_ALL_MESHES 0xffff
#define MATERIAL_ALL_UVSETS 0xffff

#define WIN_WIDTH_DEFAULT 1024
#define WIN_HEIGHT_DEFAULT 700
#define WIN_WIDTH_MIN WIN_WIDTH_DEFAULT
#define WIN_HEIGHT_MIN WIN_HEIGHT_DEFAULT
