///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <utility>
#include <vector>
#include <string>

namespace KEP {

typedef std::pair<std::string, ULONG> gsListItem;

class gsListItems : public std::vector<gsListItem> {
public:
	void SetCurSel(size_type i) {
		if (i < size() && i >= 0)
			m_curSel = i;
		else
			assert(false);
	}
	size_type GetCurSel() const { return m_curSel; }

private:
	size_type m_curSel;
};

/***********************************************************
CLASS

	GetSetList
	
	class for making a list of items for a combo or listbox

DESCRIPTION

***********************************************************/
//	class GetSetList
//	{
//	public:
//
//		///---------------------------------------------------------
//		// vfn to populate the list of itmes
//		//
//		virtual void gsPopulateList() = 0;
//
//
//		///---------------------------------------------------------
//		// [Returns]
//		//    the item list
//		const gsListItems gsList() const { return m_gsList; }
//
//	protected:
//		gsListItems		m_gsList;
//
//	};

} // namespace KEP

//#define GETSET_LIST public GetSetList
//#define DECLARE_GETSET_LIST virtual gsPopulateList();
//#define GETSET_LIST_POPULATE_FROM_OBLIST( class,

