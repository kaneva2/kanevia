///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/Vector.h"

namespace KEP {

template <typename T>
struct TColor : Vector4<T> {
	TColor() { MakeZero(); }
	TColor(T r, T g, T b, T a) { Set(r, g, b, a); }
};

template <typename T>
struct TMaterial {
	TColor<T> Diffuse;
	TColor<T> Ambient;
	TColor<T> Specular;
	TColor<T> Emissive;
	T Power;

	TMaterial() :
			Power(0) {}
	TMaterial(const TColor<T>& d, const TColor<T>& a, const TColor<T>& s, const TColor<T>& e, T p) :
			Diffuse(d), Ambient(a), Specular(s), Emissive(s), Power(p) {}
};

} // namespace KEP
