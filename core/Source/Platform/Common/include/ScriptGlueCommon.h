///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptArgManager.h"
#include "ScriptValue.h"
#include "ScriptAPITypes.h"
#include "ScriptEventArgs.h"
#include "LogHelper.h"
#include "../KEPUtil/RefPtr.h"
#include "Core/Math/Vector.h"
#include "Material.h"
#include "Core/Util/FunctionTraits.h"
#include <boost/optional.hpp>
#include <functional>

// Use the REGISTER_SCRIPT_FUNCTION() macro to automatically handle translating script types to C++ types.
// REGISTER_SCRIPT_FUNCTION takes two parameters: the name of the function that script sees, and the C++ function to call to implement that function.
// The function called can be a global function, a class member function (IClientEngine, IGetSet, and IDispatcher are supported), a
// std::function, or a lambda function.
// Support for other class types can be added by implementing GetGlueGlobal<Type>() for that type.
//
// Supported types:
// nullptr, bool, std::string, float, double, std::vector, std::array, KEP::Vector3
//
// Tables are supported via the MakeScriptTable() call.
// Multiple return types are supported by returning a std::tuple<>
//
//
// Usage examples:
//
// Member functions:
//	REGISTER_SCRIPT_FUNCTION("KEP_SetFullScreen", &IClientEngine::SetFullScreen)
//	REGISTER_SCRIPT_FUNCTION("KEP_IsFullScreen", &IClientEngine::IsFullScreen)
//
// std::functions:
//	REGISTER_SCRIPT_FUNCTION("KEP_F_v", std::function<void()>(std::bind(F_d, 3.3)))
//	REGISTER_SCRIPT_FUNCTION("KEP_F_i_i", std::function<int(int)>(std::bind(F_ii, 3, std::placeholders::_1)))
//
// lambda functions:
//	REGISTER_SCRIPT_FUNCTION("KEP_L_v", []() { LogInfo("Inside L_v"); return nullptr; })
//	REGISTER_SCRIPT_FUNCTION("KEP_L_i", [](int i1) { LogInfo("Inside L_i. arg = " << i1); return i1; })
//	REGISTER_SCRIPT_FUNCTION("KEP_L_ii", [](int i1, int i2) { LogInfo("Inside L_i. arg = " << i1 << " " << i2); return i1+i2; })
//	REGISTER_SCRIPT_FUNCTION("KEP_L_v3", [](Vector3f v) { LogInfo("Inside L_v3. arg = " << v); return v; })
//
//	// Array parameter, return array.
//	REGISTER_SCRIPT_FUNCTION("KEP_L_ai", [](std::vector<int> ai) {
//		std::stringstream strm;
//		for( size_t i = 0; i < ai.size(); ++i ) {
//			if( i != 0 )
//				strm << " ";
//			strm << ai[i];
//		}
//		LogInfo("Inside L_v3. arg = " << strm.str());
//		return ai;
//	})
//
//	// int parameter, return table.
//	REGISTER_SCRIPT_FUNCTION("KEP_L_i_vv", [](int iId) { LogInfo("KEP_L_i_vv. arg = " << iId); return MakeScriptTable("min",MakeScriptTable("x",3,"y",6,"z",8),"max",MakeScriptTable("x",9,"y",12,"z",18)); })
//
//	// Functions can do their own argument handling by taking a IScriptArgManager* parameter.
//	// This is the only way to take table arguments without creating a new specialization of ScriptValueHandler.
//	// This example accepts and returns a nested table
//	REGISTER_SCRIPT_FUNCTION("KEP_L_S", ([](IScriptArgManager* pScriptArgManager){
//		Vector3f min{1.1,1.2,1.3};
//		Vector3f max{2.1,2.2,2.3};
//		GetTupleAsTable(pScriptArgManager,
//			MakeScriptTable("min", MakeScriptTable("x", &min.x, "y", &min.y, "z", &min.z),
//							"max", MakeScriptTable("x", &max.x, "y", &max.y, "z", &max.z)));
//		LogInfo("min = " << min << "    max = " << max);
//		PushTupleAsTable(pScriptArgManager, MakeScriptTable("min", MakeScriptTable("x", min.x, "y", min.y, "z", min.z), "max", MakeScriptTable("x", max.x, "y", max.y, "z", max.z)));
//	}))
//
//	// Multiple values can be returned by using a std::tuple.
//	REGISTER_SCRIPT_FUNCTION("KEP_GetDynamicObjectBoundingBox2", [](int iPlacementId){
//		Vector3f vMin;
//		Vector3f vMax;
//		bool b = g_pICE->GetDynamicObjectBoundingBox(iPlacementId, &vMin.x, &vMin.y, &vMin.z, &vMax.x, &vMax.y, &vMax.z);
//		return std::make_tuple(b,vMin,vMax);
//	})
//
//
//	REGISTER_SCRIPT_FUNCTION("KEP_GetGeneric", [](const ScriptValue& value){
//		ScriptValue ret = value;
//		return ret;
//	})

inline void AppendToStream(std::ostream& strm) {}

template <typename NextArg, typename... Args>
inline void AppendToStream(std::ostream& strm, NextArg&& nextArg, Args&&... args) {
	strm << nextArg;
	AppendToStream(strm, std::forward<Args>(args)...);
}

template <typename... Args>
inline std::string ToString(Args&&... args) {
	std::ostringstream strm;
	AppendToStream(strm, std::forward<Args>(args)...);
	return strm.str();
}

struct BadScriptArgException : public std::exception {
	BadScriptArgException() = default;
	explicit BadScriptArgException(const char* psz) :
			std::exception(psz) {}
	explicit BadScriptArgException(const std::string& str) :
			std::exception(str.c_str()) {}
	template <typename Arg1, typename... Args>
	explicit BadScriptArgException(Arg1&& arg1, Args&&... args) :
			std::exception(ToString(std::forward<Arg1>(arg1), std::forward<Args>(args)...).c_str()) {}
};

template <typename T>
bool GetScriptValue(IScriptArgManager* pScriptArgManager, T&& pValue);
template <typename T>
void PushScriptValue(IScriptArgManager* pScriptArgManager, const T& value);

// -------------------------------------------- Arrays ------------------------------------------------
inline bool GetScriptArray(IScriptArgManager* pScriptArgManager) {
	return true;
}
template <typename OutputIterator>
inline bool GetScriptArrayElements(IScriptArgManager* pScriptArgManager, OutputIterator itr, size_t count) {
	for (size_t i = 0; i < count; ++i, ++itr) {
		if (!pScriptArgManager->PushArrayElem(i))
			return false;

		bool bGetSucceeded = GetScriptValue(pScriptArgManager, &*itr);
		pScriptArgManager->Pop();
		if (!bGetSucceeded)
			return false;
	}
	return true;
}
template <typename T>
inline bool GetScriptArray(IScriptArgManager* pScriptArgManager, std::vector<T>* pValue) {
	pValue->clear();
	size_t size = 0;
	if (!pScriptArgManager->GetArraySize(&size))
		return false; //throw BadScriptArgException("Expected an array");

	pValue->resize(size);
	if (GetScriptArrayElements(pScriptArgManager, pValue->begin(), size))
		return true;

	pValue->clear();
	return false;
}
template <typename T, size_t N>
inline bool GetScriptArray(IScriptArgManager* pScriptArgManager, std::array<T, N>* pValue) {
	size_t size = 0;
	if (!pScriptArgManager->GetArraySize(&size))
		return false; //throw BadScriptArgException("Expected an array");
	if (size != N)
		return false; //throw BadScriptArgException("Invalid array size: ", size, " expected: ", N);

	if (GetScriptArrayElements(pScriptArgManager, pValue->begin(), N))
		return true;

	return false;
}

template <typename Iterator>
inline void PushRangeAsArray(IScriptArgManager* pScriptArgManager, Iterator begin, Iterator end) {
	size_t size = end - begin;
	pScriptArgManager->PushNewArray(size);
	for (Iterator itr = begin; itr != end; ++itr) {
		PushScriptValue(pScriptArgManager, *itr);
		size_t iElem = itr - begin;
		pScriptArgManager->SetArrayElem(iElem);
	}
}

// -------------------------------------------- Tables ------------------------------------------------
// Table entries are std::pair<const char*,value_type>
struct ScriptTableTag {}; // This type is used to tag a std::tuple as a Table.
inline bool GetScriptTableValues(IScriptArgManager* pScriptArgManager, std::string* pstrFailedKey = nullptr) {
	return true;
}
template <typename NextTableValue, typename... Args>
inline bool GetScriptTableValues(IScriptArgManager* pScriptArgManager, std::string* pstrFailedKey, const char* pszKey, NextTableValue&& value, Args&&... args) {
	if (!pScriptArgManager->PushTableElem(pszKey)) {
		if (pstrFailedKey)
			*pstrFailedKey = pszKey;
		return false; //throw BadScriptArgException("Not a table");
	}
	bool ret = GetScriptValue(pScriptArgManager, std::forward<NextTableValue>(value));
	pScriptArgManager->Pop();
	if (!ret) {
		if (pstrFailedKey)
			*pstrFailedKey = pszKey;
		return false; //throw BadScriptArgException("Table does not have key: ", pszKey);
	}
	return GetScriptTableValues(pScriptArgManager, pstrFailedKey, std::forward<Args>(args)...);
}
// Ignore the key causing the error.
template <typename... Args>
inline bool GetScriptTableValues(IScriptArgManager* pScriptArgManager, Args&&... args) {
	std::string* pstrKeyCausingError = nullptr;
	return GetScriptTableValues(pScriptArgManager, pstrKeyCausingError, std::forward<Args>(args)...);
}

// GetTupleAsTable. Contents of tuple should be std::pair<const char*,value_type*>
template <size_t NumRemainingArgs, typename... Args>
inline std::enable_if_t<NumRemainingArgs == 0, bool> GetTupleAsTableHelper(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) {
	return true;
}
template <size_t NumRemainingArgs, typename... Args>
inline std::enable_if_t<(NumRemainingArgs > 0), bool> GetTupleAsTableHelper(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) {
	auto& pr = std::get<sizeof...(Args) - NumRemainingArgs + 1>(value);
	if (!pScriptArgManager->PushTableElem(pr.first))
		return false; //throw BadScriptArgException("Not a table");
	bool bRet = GetScriptValue(pScriptArgManager, pr.second);
	pScriptArgManager->Pop();
	if (!bRet)
		return false; //throw BadScriptArgException("Table does not have key: ", pr.first);
	return GetTupleAsTableHelper<NumRemainingArgs - 1, Args...>(pScriptArgManager, value);
}
template <typename... Args>
inline bool GetTupleAsTable(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) {
	return GetTupleAsTableHelper<sizeof...(Args)>(pScriptArgManager, value);
}

inline bool AddScriptTableElems(IScriptArgManager* pScriptArgManager) {
	return true;
}
template <typename NextTableValue, typename... Args>
inline bool AddScriptTableElems(IScriptArgManager* pScriptArgManager, const char* pszKey, const NextTableValue& value, Args... args) {
	pScriptArgManager->PushKey(pszKey);
	PushScriptValue(pScriptArgManager, value);
	pScriptArgManager->AddTableElem();
	return AddScriptTableElems(pScriptArgManager, std::forward<Args>(args)...);
}
template <typename... Args>
inline void PushScriptTable(IScriptArgManager* pScriptArgManager, Args... args) {
	pScriptArgManager->PushNewTable(sizeof...(Args) / 2);
	AddScriptTableElems(pScriptArgManager, std::forward<Args>(args)...);
}

// MakeScriptTable() takes arbitrary number of key/value pairs
inline auto MakeScriptTable() {
	return std::tuple<ScriptTableTag>(ScriptTableTag());
}
template <typename Key, typename Value, typename... Args>
inline auto MakeScriptTable(const std::pair<Key, Value>& firstEntry, Args... args) {
	return std::make_tuple(ScriptTableTag(), firstEntry, std::forward<Args>(args)...); // If first arg is a pair, assume all are.
}
template <typename Key, typename Value, typename... Args>
inline auto MakeScriptTable(Key key, Value value, Args... args) {
	return MakeScriptTable(std::forward<Args>(args)..., std::make_pair(key, value)); // Convert all key/value args to std::pairs and put at end of arg list.
}

template <typename Iterator>
inline void PushRangeAsTable(IScriptArgManager* pScriptArgManager, Iterator begin, Iterator end) {
	size_t size = end - begin;
	pScriptArgManager->PushNewTable(size);
	for (Iterator itr = begin; itr != end; ++itr) {
		pScriptArgManager->PushKey(itr->first);
		PushScriptValue(pScriptArgManager, &itr->second);
		pScriptArgManager->AddTableElem();
	}
}

// PushTupleAsTable
template <size_t NumRemainingArgs, typename... Args>
inline std::enable_if_t<NumRemainingArgs == 0, void> PushTupleAsTableHelper(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) {
}
template <size_t NumRemainingArgs, typename... Args>
inline std::enable_if_t<(NumRemainingArgs > 0), void> PushTupleAsTableHelper(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) {
	auto& pr = std::get<sizeof...(Args) - NumRemainingArgs + 1>(value);
	pScriptArgManager->PushKey(pr.first);
	PushScriptValue(pScriptArgManager, pr.second);
	pScriptArgManager->AddTableElem();
	PushTupleAsTableHelper<NumRemainingArgs - 1, Args...>(pScriptArgManager, value);
}
template <typename... Args>
inline void PushTupleAsTable(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) {
	pScriptArgManager->PushNewTable(sizeof...(Args));
	PushTupleAsTableHelper<sizeof...(Args)>(pScriptArgManager, value);
}

// -------------------------------------------- Tuple return values ------------------------------------------------
template <size_t NumRemainingArgs, typename... Args>
inline std::enable_if_t<NumRemainingArgs == 0, void> PushTupleHelper(IScriptArgManager* pScriptArgManager, const std::tuple<Args...>& value) {
}
template <size_t NumRemainingArgs, typename... Args>
inline std::enable_if_t<(NumRemainingArgs > 0), void> PushTupleHelper(IScriptArgManager* pScriptArgManager, const std::tuple<Args...>& value) {
	auto& subValue = std::get<sizeof...(Args) - NumRemainingArgs>(value);
	PushScriptValue(pScriptArgManager, subValue);
	PushTupleHelper<NumRemainingArgs - 1, Args...>(pScriptArgManager, value);
}
template <typename... Args>
inline void PushTuple(IScriptArgManager* pScriptArgManager, const std::tuple<Args...>& value) {
	PushTupleHelper<sizeof...(Args)>(pScriptArgManager, value);
}

// -------------------------------------------- Push var-args ------------------------------------------------
template <typename Arg0, typename... Args>
inline void PushVarArgs(IScriptArgManager* pScriptArgManager, const Arg0& arg0, const Args&... args) {
	PushScriptValue(pScriptArgManager, arg0);
	PushVarArgs(pScriptArgManager, args...);
}
inline void PushVarArgs(IScriptArgManager*) {}

// -------------------------------------------- ScriptOptional ------------------------------------------------
template <typename T>
struct TScriptOptArgRef {
	TScriptOptArgRef(T* pValue) :
			m_pValue(pValue) {}
	T* m_pValue = nullptr;
};

template <typename T>
struct TScriptOptArg {
	T m_Value;
	bool m_bValid = false;
};

// Template function constructor in the style of std::make_shared
template <typename T>
inline TScriptOptArgRef<T> ScriptOptArg(T* t) {
	return TScriptOptArgRef<T>(t);
}

// -------------------------------------------- ScriptPointer ------------------------------------------------
// Pointers passed to script.
template <typename T, bool bCheckValid = true>
struct ScriptPointer {
	//ScriptPointer() : m_pObject(nullptr) {}
	ScriptPointer(T* p = nullptr) :
			m_pObject(p) {}
	void set(T* p) { m_pObject = p; }
	T* get() const { return m_pObject; }
	operator T*() const { return m_pObject; }

private:
	T* m_pObject;
};

// -------------------------------------------- ScriptKeyValue (SCRIPT_NAMED_PARAM) ------------------------------------------------
template <typename T, bool bIsOptional, char... stringChars>
class ScriptKeyValue;

template <char... stringChars>
static const char* TemplateArgsAsString() {
	static const char s_achString[sizeof...(stringChars) + 1] = { stringChars..., 0 };
	return s_achString;
}

template <typename T, char... stringChars>
class ScriptKeyValue<T, false, stringChars...> {
public:
	ScriptKeyValue() {}
	ScriptKeyValue(ScriptKeyValue&& other) :
			m_Value(std::move(other.m_Value)) {}
	static const char* GetKey() { return TemplateArgsAsString<stringChars...>(); }
	template <typename U>
	void Set(U&& value) { m_Value = std::forward<U>(value); }
	const T& Get() const { return m_Value; }

private:
	T m_Value;
};
template <typename T, char... stringChars>
class ScriptKeyValue<T, true, stringChars...> {
public:
	ScriptKeyValue() { m_bIsSet = false; }
	ScriptKeyValue(ScriptKeyValue&& other) :
			m_bIsSet(other.m_bIsSet), m_Value(std::move(other.m_Value)) {}
	static bool IsOptional() { return bIsOptional; }
	static const char* GetKey() { return TemplateArgsAsString<stringChars...>(); }

	template <typename U>
	void Set(U&& value) {
		m_bIsSet = true;
		m_Value = std::forward<U>(value);
	}
	template <typename U>
	void SetOptional(U&& value) {
		if (!m_bIsSet)
			Set(std::forward<U>(value));
	}
	const T& GetOptional(const T& defaultValue) const { return IsSet() ? m_Value : defaultValue; }
	bool IsSet() const { return m_bIsSet; }

private:
	void VerifyIsSet() const {
		if (!IsSet())
			LogError("Using unset parameter.");
	}

private:
	bool m_bIsSet;
	T m_Value;
};

// Macros to convert a string literal to a list of characters.
// For example,. converts "Hello world" to 'H','e','l','l','o',' ','w','o','r','l','d',0,0,0,0,....
#define GET_CHAR_FROM_STRING_LITERAL(str, idx) ((idx) < sizeof(str) ? str[(idx)] : 0)
#define GET_CHAR_LIST_AT_2(str, idx) GET_CHAR_FROM_STRING_LITERAL(str, idx), GET_CHAR_FROM_STRING_LITERAL(str, idx + 1)
#define GET_CHAR_LIST_AT_4(str, idx) GET_CHAR_LIST_AT_2(str, idx), GET_CHAR_LIST_AT_2(str, idx + 2)
#define GET_CHAR_LIST_AT_8(str, idx) GET_CHAR_LIST_AT_4(str, idx), GET_CHAR_LIST_AT_4(str, idx + 4)
#define GET_CHAR_LIST_AT_16(str, idx) GET_CHAR_LIST_AT_8(str, idx), GET_CHAR_LIST_AT_8(str, idx + 8)
#define GET_CHAR_LIST_AT_32(str, idx) GET_CHAR_LIST_AT_16(str, idx), GET_CHAR_LIST_AT_16(str, idx + 16)
#define GET_CHAR_LIST_AT_64(str, idx) GET_CHAR_LIST_AT_32(str, idx), GET_CHAR_LIST_AT_32(str, idx + 32)

// Limit of 24 characters. This can be increased if necessary, but longer names make error messages and debug info harder to read.
#define GET_SCRIPT_KEY_CHAR_LIST(str) GET_CHAR_LIST_AT_16(str, 0), GET_CHAR_LIST_AT_8(str, 16)
#define SCRIPT_NAMED_PARAM_TYPE(type, isoptional, name) ScriptKeyValue<type, isoptional, GET_SCRIPT_KEY_CHAR_LIST(#name)>
#define SCRIPT_NAMED_PARAM_4(type, isoptional, name, var_name) SCRIPT_NAMED_PARAM_TYPE(type, isoptional, name) && var_name
#define GET_MACRO_ARG_3(ARG1, ARG2, ARG3, ...) ARG3
#define SCRIPT_NAMED_PARAM(type, name, ...) SCRIPT_NAMED_PARAM_4(type, false, name, GET_MACRO_ARG_3(type, name, __VA_ARGS__, name))
#define SCRIPT_NAMED_PARAM_OPT(type, name, ...) SCRIPT_NAMED_PARAM_4(type, true, name, GET_MACRO_ARG_3(type, name, __VA_ARGS__, name))

// -------------------------------------------- ScriptValueHandler ------------------------------------------------
// New types are supported by creating specializations of ScriptValueHandler.
template <typename T, typename Enable = void>
struct ScriptValueHandler;

struct ScriptArgTraitsDefault {
	static constexpr size_t NumberOfArgumentsConsumed = 1;
	static constexpr bool ArgumentIsImplicit = false; // When true, argument is not read from the stack.
	static constexpr bool ArgumentIsOptional = false; // When true and no argument is on the stack, null is pushed onto stack in its place.
};

struct ScriptArgTraitsImplicit : ScriptArgTraitsDefault {
	static constexpr size_t NumberOfArgumentsConsumed = 0;
	static constexpr bool ArgumentIsImplicit = true;
};

struct ScriptArgTraitsNotConsumed : ScriptArgTraitsDefault {
	static constexpr size_t NumberOfArgumentsConsumed = 0;
};

struct ScriptArgTraitsOptional : ScriptArgTraitsDefault {
	static constexpr bool ArgumentIsOptional = true;
};

// Specialize this to set function parameters without consuming the arguments passed from script.
template <typename T>
struct ScriptArgTraits : ScriptArgTraitsDefault {};

template <typename T>
struct DirectlySupportedScriptValueHandler {
	static bool Get(IScriptArgManager* pScriptArgManager, T* pValue) { return pScriptArgManager->GetValue(pValue); }
	static void Push(IScriptArgManager* pScriptArgManager, const T& value) { pScriptArgManager->PushValue(value); }
};
template <>
struct ScriptValueHandler<nullptr_t> : public DirectlySupportedScriptValueHandler<nullptr_t> {};
template <>
struct ScriptValueHandler<bool> : public DirectlySupportedScriptValueHandler<bool> {};
template <>
struct ScriptValueHandler<double> : public DirectlySupportedScriptValueHandler<double> {};
template <>
struct ScriptValueHandler<std::string> : public DirectlySupportedScriptValueHandler<std::string> {};
template <>
struct ScriptValueHandler<ScriptValue> : DirectlySupportedScriptValueHandler<ScriptValue> {};

// Numeric conversions.
template <typename InputType, typename ConvertedType, bool AreSame = std::is_same<InputType, ConvertedType>::value>
struct ConvertedTypeScriptValueHandler;

// No conversion required
template <typename InputType, typename ConvertedType>
struct ConvertedTypeScriptValueHandler<InputType, ConvertedType, true> : DirectlySupportedScriptValueHandler<InputType> {};

template <typename InputType, typename ConvertedType>
struct ConvertedTypeScriptValueHandler<InputType, ConvertedType, false> {
	static bool Get(IScriptArgManager* pScriptArgManager, InputType* pValue) {
		ConvertedType value;
		bool ret = ScriptValueHandler<ConvertedType>::Get(pScriptArgManager, &value);
		*pValue = InputType(value);
		return ret;
	}
	static void Push(IScriptArgManager* pScriptArgManager, const InputType& value) { ScriptValueHandler<ConvertedType>::Push(pScriptArgManager, ConvertedType(value)); }
};

template <>
struct ScriptValueHandler<float> : public ConvertedTypeScriptValueHandler<float, double> {};
template <>
struct ScriptValueHandler<unsigned long> : public ConvertedTypeScriptValueHandler<unsigned long, double> {};
template <>
struct ScriptValueHandler<long> : public ConvertedTypeScriptValueHandler<long, int32_t> {};
template <>
struct ScriptValueHandler<unsigned int> : public ConvertedTypeScriptValueHandler<unsigned int, double> {};
template <>
struct ScriptValueHandler<int> : public ConvertedTypeScriptValueHandler<int, int32_t> {};
template <>
struct ScriptValueHandler<enum class ObjectType> : public ConvertedTypeScriptValueHandler<enum class ObjectType, int> {};

template <size_t N, typename T>
struct ScriptValueHandler<KEP::Vector<N, T>> {
	static_assert(N == 2 || N == 3 || N == 4, "Unsupported Vector length");
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::Vector<N, T>* pValue) {
		bool bSucceeded = GetScriptTableValues(pScriptArgManager, "x", &(*pValue)[0], "y", &(*pValue)[1]);
		if (N > 2 && bSucceeded)
			bSucceeded &= GetScriptTableValues(pScriptArgManager, "z", &(*pValue)[2]);
		if (N > 3 && bSucceeded)
			bSucceeded &= GetScriptTableValues(pScriptArgManager, "w", &(*pValue)[3]);
		return bSucceeded;
	}
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::Vector<N, T>& value) {
		pScriptArgManager->PushNewTable(N);
		AddScriptTableElems(pScriptArgManager, "x", value[0], "y", value[1]);
		if (N > 2)
			AddScriptTableElems(pScriptArgManager, "z", value[2]);
		if (N > 3)
			AddScriptTableElems(pScriptArgManager, "w", value[3]);
	}
};
template <typename T>
struct ScriptValueHandler<KEP::TColor<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::TColor<T>* pValue) {
		bool bSucceeded = GetScriptTableValues(pScriptArgManager, "r", &(*pValue)[0], "g", &(*pValue)[1], "b", &(*pValue)[2]);
		(*pValue)[3] = 1.0f; // Alpha is optional
		GetScriptTableValues(pScriptArgManager, "a", &(*pValue)[3]);
		return bSucceeded;
	}
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::TColor<T>& value) {
		pScriptArgManager->PushNewTable(4);
		AddScriptTableElems(pScriptArgManager, "r", value[0], "g", value[1], "b", value[2], "a", value[3]);
	}
};
template <typename T>
struct ScriptValueHandler<KEP::TMaterial<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::TMaterial<T>* pValue) {
		// Diffuse is required
		bool bSucceeded = GetScriptTableValues(pScriptArgManager, "diffuse", &pValue->Diffuse);
		if (bSucceeded) {
			// Optional
			pValue->Ambient.MakeZero();
			pValue->Specular.MakeZero();
			pValue->Emissive.MakeZero();
			pValue->Power = 0;
			GetScriptTableValues(pScriptArgManager, "ambient", &pValue->Ambient);
			GetScriptTableValues(pScriptArgManager, "specular", &pValue->Specular);
			GetScriptTableValues(pScriptArgManager, "emissive", &pValue->Emissive);
			GetScriptTableValues(pScriptArgManager, "power", &pValue->Power);
		}
		return bSucceeded;
	}
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::TMaterial<T>& value) {
		pScriptArgManager->PushNewTable(5);
		AddScriptTableElems(pScriptArgManager, "diffuse", value.Diffuse, "ambient", value.Ambient, "specular", value.Specular, "emissive", value.Emissive, "power", value.Power);
	}
};

template struct ScriptValueHandler<KEP::TMaterial<float>>;
template struct ScriptValueHandler<KEP::TMaterial<double>>;

template <typename T>
struct ScriptValueHandler<std::vector<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, std::vector<T>* pValue) { return GetScriptArray(pScriptArgManager, pValue); }
	static void Push(IScriptArgManager* pScriptArgManager, const std::vector<T>& value) { PushRangeAsArray(pScriptArgManager, value.begin(), value.end()); }
};
template <typename T, size_t N>
struct ScriptValueHandler<std::array<T, N>> {
	static bool Get(IScriptArgManager* pScriptArgManager, std::array<T, N>* pValue) { return GetScriptArray(pScriptArgManager, pValue); }
	static void Push(IScriptArgManager* pScriptArgManager, const std::array<T, N>& value) { PushRangeAsArray(pScriptArgManager, value.begin(), value.end()); }
};
template <typename... Args>
struct ScriptValueHandler<std::tuple<ScriptTableTag, Args...>> {
	static bool Get(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) { return GetTupleAsTable(pScriptArgManager, value); }
	static void Push(IScriptArgManager* pScriptArgManager, const std::tuple<ScriptTableTag, Args...>& value) { PushTupleAsTable(pScriptArgManager, value); }
};
template <typename... Args>
struct ScriptValueHandler<std::tuple<Args...>> {
	//static bool Get(IScriptArgManager* pScriptArgManager, const std::tuple<Args...>& value)
	//{ return GetTuple(pScriptArgManager, value); }
	static void Push(IScriptArgManager* pScriptArgManager, const std::tuple<Args...>& value) { PushTuple(pScriptArgManager, value); }
};
template <>
struct ScriptValueHandler<const char*> {
	static void Push(IScriptArgManager* pScriptArgManager, const char* value) {
		std::string str(value);
		ScriptValueHandler<std::string>::Push(pScriptArgManager, str);
	}
	// Can't Get() into a const char*
};
template <typename... Args>
struct ScriptValueHandler<KEP::TScriptAPIRetType<Args...>> {
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::TScriptAPIRetType<Args...>& value) {} // Do nothing
	// Can't Get() into a ScriptAPIRetType
};
template <typename T>
struct ScriptValueHandler<T, typename T::script_wrapped_void> {
	static bool Get(IScriptArgManager* pScriptArgManager, T* pValue) { return GetScriptValue(pScriptArgManager, &pValue->value); }
	static void Push(IScriptArgManager* pScriptArgManager, const T& value) { PushScriptValue(pScriptArgManager, value.value); }
};
template <typename T>
struct ScriptValueHandler<TScriptOptArgRef<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, const TScriptOptArgRef<T>& value) {
		ScriptValueHandler<std::decay_t<T>>::Get(pScriptArgManager, value.m_pValue);
		return true;
	}
	static bool Get(IScriptArgManager* pScriptArgManager, TScriptOptArg<T>* pValue) {
		bSucceeded = ScriptValueHandler<std::decay_t<T>>::Get(pScriptArgManager, value.m_pValue);
		value.m_bValid = bSucceeded;
		return bSucceeded;
	}
	// Can't Push() a TScriptOptArgRef
};
template <typename T>
struct ScriptValueHandler<boost::optional<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, boost::optional<T>* pValue) {
		std::decay_t<T> t;
		bool bSucceeded = ScriptValueHandler<std::decay_t<T>>::Get(pScriptArgManager, &t);
		if (bSucceeded) {
			*pValue = std::move(t);
		} else {
			*pValue = boost::none;
			if (pScriptArgManager->GetValueType() == IScriptArgManager::eValueType::None)
				bSucceeded = true;
		}
		return bSucceeded;
	}
	static void Push(IScriptArgManager* pScriptArgManager, boost::optional<T>& value) {
		if (value)
			ScriptValueHandler<std::decay<T>>::Push(pScriptArgManager, value.value());
		else
			pScriptArgManager->Push(nullptr);
	}
};

template <typename T>
struct ScriptArgTraits<boost::optional<T>> : ScriptArgTraitsOptional {};

template <>
struct ScriptValueHandler<IScriptArgManager*> {
	static bool Get(IScriptArgManager* pScriptArgManager, IScriptArgManager** pValue) {
		*pValue = pScriptArgManager;
		return true;
	}
};
// IScriptArgManager is not passed in from script. It is an implicit argument.
template <>
struct ScriptArgTraits<IScriptArgManager*> : ScriptArgTraitsImplicit {};

template <typename T, bool bCheckValid>
struct ScriptValueHandler<ScriptPointer<T, bCheckValid>> {
	static bool Get(IScriptArgManager* pScriptArgManager, ScriptPointer<T, bCheckValid>* pValue) {
		void* p = nullptr;
		if (!pScriptArgManager->GetPointer(&p))
			return false;
		if (bCheckValid && !RefPtrValid(__FUNCTION__, typeid(T).name(), p))
			return false;
		pValue->set(static_cast<T*>(p));
		return true;
	}
	static void Push(IScriptArgManager* pScriptArgManager, const ScriptPointer<T, bCheckValid>& value) {
		RefPtrAdd(typeid(T).name(), value.get());
		pScriptArgManager->PushPointer(value.get());
	}
};
template <typename T, bool bIsOptional, char... stringChars>
struct ScriptValueHandler<ScriptKeyValue<T, bIsOptional, stringChars...>> {
	static bool Get(IScriptArgManager* pScriptArgManager, ScriptKeyValue<T, bIsOptional, stringChars...>* pValue) {
		T value;
		bool bHaveKey = pScriptArgManager->PushTableElem(pValue->GetKey());
		if (!bHaveKey) {
			if (bIsOptional)
				return true;
			LogError("Key \"" << pValue->GetKey() << "\" of type " << typeid(T).name() << " does not exist.");
			return false;
		}
		bool bSucceeded = GetScriptValue(pScriptArgManager, &value);
		pScriptArgManager->Pop();
		if (bSucceeded)
			pValue->Set(value);
		return bSucceeded;
	}
};

// ScriptKeyValue does not consume its script argument. The next function parameter uses the same script argument.
template <typename T, bool bIsOptional, char... stringChars>
struct ScriptArgTraits<ScriptKeyValue<T, bIsOptional, stringChars...>> : ScriptArgTraitsNotConsumed {};

template <typename T>
inline bool GetScriptValue(IScriptArgManager* pScriptArgManager, T&& pValue) {
	return ScriptValueHandler<std::remove_pointer_t<std::decay_t<T>>>::Get(pScriptArgManager, pValue);
}
template <typename T>
inline void PushScriptValue(IScriptArgManager* pScriptArgManager, const T& value) {
	ScriptValueHandler<T>::Push(pScriptArgManager, value);
}

// -------------------------------------------- Argument handling framework ------------------------------------------------
// Script argument management
template <typename T>
inline bool GetScriptValueAt(IScriptArgManager* pScriptArgManager, size_t idx, T* pValue) {
	if (!ScriptArgTraits<T>::ArgumentIsImplicit) {
		if (!pScriptArgManager->PushArgAtIndex(idx)) {
			if (ScriptArgTraits<T>::ArgumentIsOptional)
				pScriptArgManager->PushValue(nullptr);
			else
				return false;
		}
	}

	bool ret;
	try {
		ret = GetScriptValue(pScriptArgManager, pValue);
	} catch (BadScriptArgException& except) {
		LogError(except.what());
		ret = false;
	}

	if (!ScriptArgTraits<T>::ArgumentIsImplicit)
		pScriptArgManager->Pop();
	return ret;
}

template <typename FunctionType, typename... Args>
inline ReturnTypeOfFunction_t<FunctionType> CallGlueFunctionWithArgs(FunctionType f, Args... args) {
	return f(std::forward<Args>(args)...);
}

// Glue globals are used to support handlers that are defined with member functions
// IsGlueGlobalInitialized()
// return true if function is not a member of a glue global or if the necessary glue global is non-null.
template <typename T>
inline T* GetGlueGlobal() {
	static T* p = nullptr;
	return p;
}
template <typename T>
bool IsGlueGlobalInitialized(const T& func) {
	return true;
}
template <typename ClassType, typename ReturnType, typename... Args>
bool IsGlueGlobalInitialized(ReturnType (ClassType::*)(Args...)) {
	return GetGlueGlobal<ClassType>() != nullptr;
}
template <typename ClassType, typename ReturnType, typename... Args>
bool IsGlueGlobalInitialized(ReturnType (ClassType::*)(Args...) const) {
	return GetGlueGlobal<ClassType>() != nullptr;
}

// ScriptArgUnwrapper. Takes arguments from script, calls registered function with those arguments, and sends the return value back to script.
template <size_t iScriptArg, size_t iRemainingArgs, typename ReturnType, typename FunctionType, typename... Args>
struct ScriptArgUnwrapper {
	static void Call(IScriptArgManager* pScriptArgManager, const FunctionType& f, Args... args) {
		static_assert(iRemainingArgs > 0, "Compiler should have used one of the specializations");

		static const size_t iArg = sizeof...(Args);
		typedef std::decay_t<NthArgTypeOfFunction_t<iArg, FunctionType>> NextArgType;
		NextArgType next_arg;
		if (!GetScriptValueAt(pScriptArgManager, iScriptArg, &next_arg)) {
			throw BadScriptArgException("Invalid argument at index ", iArg, " of expected type ", typeid(NextArgType).name(), ".");
		}
		const size_t nArgsConsumed = ScriptArgTraits<NextArgType>::NumberOfArgumentsConsumed;
		ScriptArgUnwrapper<iScriptArg + nArgsConsumed, iRemainingArgs - 1, ReturnType, FunctionType, Args..., NextArgType>::Call(pScriptArgManager, f, std::forward<Args>(args)..., std::move(next_arg));
	}
};

template <size_t iScriptArg, typename ReturnType, typename FunctionType, typename... Args>
struct ScriptArgUnwrapper<iScriptArg, 0, ReturnType, FunctionType, Args...> {
	static void Call(IScriptArgManager* pScriptArgManager, const FunctionType& f, Args... args) {
		ReturnType returnValue = CallGlueFunctionWithArgs(f, std::forward<Args>(args)...);
		PushScriptValue(pScriptArgManager, returnValue);
	}
};
template <size_t iScriptArg, typename FunctionType, typename... Args>
struct ScriptArgUnwrapper<iScriptArg, 0, void, FunctionType, Args...> {
	static void Call(IScriptArgManager* pScriptArgManager, const FunctionType& f, Args... args) {
		CallGlueFunctionWithArgs(f, std::forward<Args>(args)...);
	}
};
template <typename FunctionType>
bool CallScriptFunctionWithArgs(IScriptArgManager* pScriptArgManager, const FunctionType& f) {
	try {
		if (!IsGlueGlobalInitialized(f)) {
			throw BadScriptArgException("Script global for function type ", typeid(FunctionType).name(), " was not initialized.");
		}
		ScriptArgUnwrapper<0, NumberOfFunctionArguments<FunctionType>::value, ReturnTypeOfFunction_t<FunctionType>, FunctionType>::Call(pScriptArgManager, f);
		return true;
	} catch (std::exception& e) {
		LogError("Error calling function from script: " << e.what() << " Context: " << pScriptArgManager->GetCallInfo());
		return false;
	}
}
