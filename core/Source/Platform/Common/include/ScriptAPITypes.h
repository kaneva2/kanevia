///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <type_traits>

namespace KEP {

// API type enum
struct ScriptAPIType {
	struct SYNC {}; // Synchronous API that returns quickly with results
	struct BLOC {}; // Blocking API that calls lua_yield on VM and expects a future result event to resume the VM.
	// We currently enforce a timeout mechanism that will place VM in zombie state if the result
	// doesn't come back within a preset duration
	struct ASYN {}; // Asynchronous API that returns immediately and expects a future result event to be delivered
	// to the requesting VM through kgp_result callback. See kgp.scriptCallAsync
};

// Script interface wrapper
template <typename Script, typename Event, typename Task>
struct TScriptAPIScript {
	int LuaYield(int unblockEventType, const std::string& reason, Event* e, Task* task);
};

// Generic API return type
template <typename Type, typename Script, typename Event, typename Task>
struct TScriptAPIRetType {};

// Specialization for synchronous APIs (*NOT* currently used)
template <typename Script, typename Event, typename Task>
struct TScriptAPIRetType<ScriptAPIType::SYNC, typename Script, typename Event, typename Task> {
	using _MyT = TScriptAPIRetType<ScriptAPIType::SYNC, Script, Event, Task>;

	_MyT(int numRetVals) :
			m_numRetVals(numRetVals) {}
	int getLuaRC() { return m_numRetVals; } // Number of return values, or 0 if API failed

	static auto Failed() { return _MyT(0); }

private:
	int m_numRetVals;
};

// Specialization for blocking APIs
template <typename Script, typename Event, typename Task>
struct TScriptAPIRetType<ScriptAPIType::BLOC, typename Script, typename Event, typename Task> {
	using _MyT = TScriptAPIRetType<ScriptAPIType::BLOC, Script, Event, Task>;
	using _MyScript = TScriptAPIScript<Script, Event, Task>;

	_MyT(_MyScript&& ss, int unblockEventType, const std::string& reason, Event* e, Task* task = nullptr) {
		m_luaRC = std::move(ss).LuaYield(unblockEventType, reason, e, task);
	}

	int getLuaRC() { return m_luaRC; } // LUA_YIELD or other return values from lua_yield, or 0 if API failed

	static auto Failed() { return _MyT(); }

private:
	_MyT() {
		// Private constructor used by Failed() only
		m_luaRC = 0;
	}

	int m_luaRC;
};

// Specialization for asynchronous APIs
template <typename Script, typename Event, typename Task>
struct TScriptAPIRetType<ScriptAPIType::ASYN, typename Script, typename Event, typename Task> {
	using _MyT = TScriptAPIRetType<ScriptAPIType::ASYN, Script, Event, Task>;

	int getLuaRC() { return 0; } // No return values

	static auto Failed() { return _MyT(); }
};

// Type trait helper - check if a type (T) is a specialization of ScriptAPIRetType<Type, Script, Event, Task> with Type == APIType
// e.g. isScriptAPIRetType<ScriptAPIRetType::BLOC, ScriptAPIType::BLOC>::value == true
template <typename T, typename APIType>
struct isScriptAPIRetType : std::false_type {};

template <typename APIType, typename Script, typename Event, typename Task>
struct isScriptAPIRetType<TScriptAPIRetType<APIType, Script, Event, Task>, APIType> : std::true_type {};

} // namespace KEP
