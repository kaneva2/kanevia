///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

typedef int DownloadPriority;

// DRF - Download Queue Relative Priorities (NOT THREAD!)
enum {
	DL_PRIO_SYNC = 1000000000, // Download now
	DL_PRIO_HIGH = 3000,
	DL_PRIO_MEDIUM = 2000,
	DL_PRIO_LOW = 1000,
	DL_PRIO_NO = 0, // drf - added - don't download
	DL_PRIO_DYN = -1, // Dynamic
};
