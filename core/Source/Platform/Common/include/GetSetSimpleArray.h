/******************************************************************************
 GetSetSimpleArray.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

class GetSetSimpleArray {
public:
	GetSetSimpleArray(ULONG &arraySize, void *array) :
			m_arraySize(arraySize), m_array(array) {
		ASSERT(array != 0);
	}

	ULONG size() { return m_arraySize; }

protected:
	ULONG &m_arraySize; // ptr to long in the object
	void *m_array; // e.g. int** or CString **
};

template <class T>
class TGetSetSimpleArray : public GetSetSimpleArray {
public:
	TGetSetSimpleArray(ULONG &item, void *array) :
			GetSetSimpleArray(item, array) {}
	ULONG add(T x) {
		T *temp;

		ULONG newCount = m_arraySize + 1;
		temp = new T[m_arraySize + 1];

		for (ULONG i = 0; i < m_arraySize; i++) {
			// copy the existing ones
			temp[i] = (array())[i];
		}
		temp[i] = x;
		eraseAll();

		*(T **)m_array = temp;
		m_arraySize = newCount;

		return m_arraySize;
	}

	bool update(ULONG index, T x) {
		if (index >= m_arraySize)
			return false;
		(array())[index] = x;
		return true;
	}

	bool erase(ULONG index) {
		if (index >= m_arraySize)
			return false;

		T *temp;
		ULONG newCount = m_arraySize - 1;
		temp = new T[m_arraySize - 1];

		ULONG j = 0;
		for (ULONG i = 0; i < m_arraySize; i++) {
			// copy the existing ones
			if (i != index)
				temp[j++] = (array())[i];
		}
		eraseAll();
		*(T **)m_array = temp;
		m_arraySize = newCount;

		return true;
	}
	void eraseAll() {
		m_arraySize = 0;
		delete[](array());
		*(T **)m_array = 0;
	}

	T *array() { return *(T **)m_array; }

	// LONG versions of above calls
	bool update(LONG index, T x) { return update((ULONG)index, x); }
	bool erase(LONG index) { return erase((ULONG)index); }
};

} // namespace KEP

