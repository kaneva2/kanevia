///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CommandActions.h"
#include "Event\Base\IEvent.h"

#include <SDKCommon.h>
#include <string>

#include "ControlsClass.h"

namespace KEP {

class CMovementObj;

class ActionEvent : public IEvent {
public:
	ActionEvent(ControlObjActionState const &act_state, CMovementObj *movement_obj, ControlInfoObj *control_info_obj, TimeMs cur_time);

	void ExtractInfo(ControlObjActionState &act_state, CMovementObj **movement_obj, ControlInfoObj **control_info_obj, TimeMs &cur_time);

	DEFINE_CLASS_INFO(ActionEvent, PackVersion(1, 0, 0, 0), EP_NORMAL)

private:
	CMovementObj *m_movement_obj;
	ControlInfoObj *m_control_info_obj;
};

} // namespace KEP
