///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include "IMemSizeGadget.h"

namespace KEP {

using ObjectSet = std::map<size_t, size_t>;

class CMemSizeGadget : public IMemSizeGadget {
public:
	virtual size_t GetTotalSizeInBytes() {
		return m_TotalSizeInBytes;
	}

	virtual size_t GetTotalObjects() {
		// Subtract one for the sentinel object {0,0}
		// id/address of 0 and size of 0
		//
		return m_ObjectSet.size() - 1;
	}

	virtual void Release() {
		delete this;
	}

	virtual void Reset() {
		m_TotalSizeInBytes = 0;
		m_ObjectSet.clear();
		m_ObjectSet.emplace(0, 0);
	}

	// For MFC classes
	virtual void AddObject(const CStringA* pCString) {
		if (pCString) {
			AddObjectSizeof(*pCString);
			AddObject(*pCString);
		}
	}

	virtual void AddObject(const CStringA& aCString) {
		if (!aCString.IsEmpty()) {
			AddObjectSizeof(aCString.GetAt(0), aCString.GetAllocLength());
		}
	}

	virtual void AddObject(const std::vector<CStringA*>& rVector) {
		for (auto it = rVector.begin(); it != rVector.end(); ++it) {
			AddObject(*it);
		}
	}

protected:
	virtual bool AddMemSize(size_t objectId, size_t sizeInBytes) {
		auto result = m_ObjectSet.emplace(objectId, sizeInBytes);
		if (result.second) {
			m_TotalSizeInBytes += sizeInBytes;
			return true;
		}
		return false;
	}

private:
	ObjectSet m_ObjectSet = { { 0, 0 } };
	size_t m_TotalSizeInBytes = { 0 };
};

} // namespace KEP
