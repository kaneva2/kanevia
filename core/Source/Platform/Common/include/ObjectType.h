///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <ostream>

//*********************************************************
//	Global ObjectType definition used by
//	CPP modules and LUA script files
//
//	Objective is for Game script and Platform C++ code
//	to use the same base definition.
//
//	Reference the file ObjectType.lua in the
//	game code project.
//
//*********************************************************
enum class ObjectType : int {
	NONE = 0x0000,
	WORLD = 0x0001, // 1 << 0
	_OLD_ZONE_ = 0x0002, // 1 << 1 // used to be zone
	DYNAMIC = 0x0004, // 1 << 2
	MOVEMENT = 0x0008, // 1 << 3
	EQUIPPABLE = 0x0010, // 1 << 4
	BONE = 0x0020, // 1 << 5
	CAMERA = 0x0040, // 1 << 6
	SOUND = 0x0080, // 1 << 7
	ANY = (WORLD | DYNAMIC | MOVEMENT | EQUIPPABLE | BONE | CAMERA | SOUND)
};

inline std::string ObjTypeToStr(const ObjectType& objType) {
	const static std::string objTypeStr[] = {
		"WORLD",
		"_OLD_ZONE_",
		"DYNAMIC",
		"MOVEMENT",
		"EQUIPPABLE",
		"BONE",
		"CAMERA",
		"SOUND"
	};

	if (objType == ObjectType::NONE)
		return "NONE";

	std::string str;
	size_t types = 0;
	for (size_t i = 0, ot = static_cast<int>(objType); (i < _countof(objTypeStr)) && ot; ++i, ot >>= 1) {
		if (ot & 1) {
			if (types++)
				str = str + "|";
			str = str + objTypeStr[i];
		}
	}
	return str;
}

inline std::ostream& operator<<(std::ostream& outs, const ObjectType& objectType) {
	return outs << static_cast<const int>(objectType);
}

inline ObjectType operator&(const ObjectType& lhs, const ObjectType& rhs) {
	return static_cast<ObjectType>(static_cast<int>(lhs) & static_cast<int>(rhs));
}

inline ObjectType operator|(const ObjectType& lhs, const ObjectType& rhs) {
	return static_cast<ObjectType>(static_cast<int>(lhs) | static_cast<int>(rhs));
}

inline int ObjTypeToInt(const ObjectType& objectType) {
	return static_cast<int>(objectType);
}

inline double ObjTypeToDouble(const ObjectType& objectType) {
	return static_cast<double>(objectType);
}

inline ObjectType DoubleToObjType(const double& aDouble) {
	return static_cast<ObjectType>(static_cast<int>(aDouble));
}
