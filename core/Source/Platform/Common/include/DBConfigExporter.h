#pragma once
#include "Common/include/KEPCommon.h"
#ifndef DECOUPLE_DBCONFIG
#else
#include "Common/Include/DBConfigTags.h"
#include "Common/include/ksoftkeys.h"
#include "common/include/KEPMacros.h"
#include "common/include/IKEPConfig.h"

namespace KEP {

class DBConfigExporter {
public:
	// Export settings to an IKEPConfig instance
	static bool Export(INOUT IKEPConfig& cfg, IN const char* dbServer, IN int dbPort, IN const char* dbName, IN const char* dbUser, IN const char* dbPassword, IN bool dontShowAgain) {
		return Export(cfg, dbServer, dbPort, dbName, dbUser, dbPassword, dontShowAgain, false, false);
	}

	// Export settings to an IKEPConfig instance, including subGame value
	static bool Export(INOUT IKEPConfig& cfg, IN const char* dbServer, IN int dbPort, IN const char* dbName, IN const char* dbUser, IN const char* dbPassword, IN bool dontShowAgain, IN bool subGame, IN bool saveSubGame = true) {
		cfg.SetValue(DB_SERVER, dbServer);
		cfg.SetValue(DB_PORT, dbPort);
		cfg.SetValue(DB_NAME, dbName);
		cfg.SetValue(DB_USER, dbUser);
		if (saveSubGame)
			cfg.SetValue(DB_PASSWORD_CLEAR, dbPassword); // so Python can use it

		// encrypt the password
		string encryptedPassword;
		BOOL encryptResult = EncryptAndEncodeString(dbPassword, encryptedPassword, DB_KEY);

		_ASSERT(encryptResult);
		if (!encryptResult) {
			return false;
		}
		cfg.SetValue(DB_PASSWORD, encryptedPassword.c_str());
		cfg.SetValue(DB_DONTSHOWAGAIN, dontShowAgain);
		cfg.SetValue(DB_DISABLED, 0); // put in XML so they can easily disable it
		if (saveSubGame)
			cfg.SetValue(DB_LOCAL_PLAYERS, !subGame);

		return true;
	}
};

} // namespace KEP

#endif
