///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/PrimitiveType.h"
#include "Core/Math/Vector.h"

namespace KEP {

template <typename T>
class Primitive;
class IReadableBuffer;
class IWriteableBuffer;

template <typename T>
struct PrimitiveGeomArgs {
	int type;
	size_t axis;
	union {
		struct {
			T radius;
		} sphere;
		struct {
			Vector3<T> min;
			Vector3<T> max;
		} cuboid;
		struct {
			T radius;
			T height;
			T base;
		} cylinder;
		struct {
			T fov;
			T nearPlane;
			T farPlane;
			T aspect;
		} rectangularFrustum;
		struct {
			T fov;
			T nearPlane;
			T farPlane;
		} circularFrustum;
	};

	PrimitiveGeomArgs() {
		memset(this, 0, sizeof(PrimitiveGeomArgs));
	}

	Primitive<T>* createPrimitive() const;

	static PrimitiveGeomArgs getSphere(T radius) {
		PrimitiveGeomArgs prim;
		prim.type = PrimitiveType::Sphere;
		prim.sphere.radius = radius;
		return prim;
	}
};

template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, PrimitiveGeomArgs<T>& val);

template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const PrimitiveGeomArgs<T>& val);

} // namespace KEP
