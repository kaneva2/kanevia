#pragma once

#include <vector>

// New KEP object class behaviors
template <typename T>
class DefKEPObjBehaviors {
public:
	T* Clone(const T& obj) { return new T(obj); }
	void SafeDelete(T&) {}
	void Deserialize(T& obj, CArchive& ar) { obj.Read(ar); }
	void Serialize(T& obj, CArchive& ar) { obj.Write(ar); }
};

// Legacy KEP object class behaviors
template <typename T>
class LegacyKEPObjBehaviors {
public:
	T* Clone(const T& obj) {
		T* cloned = new T();
		obj.Clone(cloned);
		return cloned;
	}
	void SafeDelete(T& obj) { obj.SafeDelete(); }
	void Deserialize(T& obj, CArchive& ar) { obj.Serialize(ar); }
	void Serialize(T& obj, CArchive& ar) { obj.Serialize(ar); }
};

template <typename ItemType, typename ItemBehaviors = DefKEPObjBehaviors<ItemType>, typename IDType = int>
class KEPObVector : public std::vector<ItemType*> {
public:
	KEPObVector() :
			m_dirty(false) {}

	KEPObVector(const KEPObVector& rhs) { *this = rhs; }

	KEPObVector& operator=(const KEPObVector& rhs) {
		for_each(rhs.begin(), rhs.end(), [&](ItemType* obj) {
			ItemType* copied = ItemBehaviors().Clone(*obj);
			push_back(copied);
		});

		SetDirty(rhs.IsDirty());
		return *this;
	}

	~KEPObVector() {
		for_each(begin(), end(), [&](ItemType* obj) {
			ItemBehaviors().SafeDelete(*obj);
			delete obj;
		});
		clear();
	}

	ItemType* GetObjByIndex(int index) {
		if (index < 0 || (size_t)index >= size()) {
			return NULL;
		}

		return at((size_t)index);
	}

	ItemType* GetObjById(IDType id) const {
		auto itr = find_if(begin(), end(), [id](ItemType* obj) { return obj->getID() == id; });
		if (itr != end())
			return *itr;
		return NULL;
	}

	void Write(CArchive& ar) {
		for_each(begin(), end(), [&](ItemType* obj) {
			ItemBehaviors().Serialize(*obj, ar);
		});

		SetDirty(false);
	}

	void Read(CArchive& ar, int count) {
		for (int i = 0; i < count; i++) {
			ItemType* obj = new ItemType();
			ItemBehaviors().Deserialize(*obj, ar);
			push_back(obj);
		}

		SetDirty(false);
	}

	int GetFreeID() const {
		for (int trace = 1;; ++trace) {
			if (all_of(begin(), end(), [&](const ItemType* obj) { return obj->getID() != trace; })) {
				return trace;
			}
		}
	}

	void AddTail(ItemType* obj) { push_back(obj); }

	size_t GetCount() const { return size(); }

	void RemoveAt(size_t index) {
		if (index < size()) {
			erase(begin() + index);
		}
	}

	bool IsDirty() const {
		return m_dirty;
	}
	void SetDirty(bool dirty = true) {
		m_dirty = dirty;
	}

private:
	bool m_dirty;
};
