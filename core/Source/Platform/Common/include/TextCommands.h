#pragma once

#include <string>
#include <map>
#include <tchar.h>

namespace KEP {

typedef std::map<std::string, ULONG> CommandMap;

/** DRF
* This class manages mapping text commands to enumerated command ids.
*
* NOTE - Client reads the command mapping from ...\star\####\GameFiles\Command.xml but none of the 
* commands are actually implimented in ClientEngine_UserInterface::ProcessCommandLocally() so really 
* all of this is dead code.
*/
class TextCommands {
public:
	static const char* DEFAULT_COMMAND_STRING;

	// DRF - ':' is redefined to be '/' for client in star\####\GameFiles\Command.xml
	TextCommands() :
			m_commandChar(_T(':')), m_defaultCommand(2009) {} // loot for our default

	bool Init(const std::string& pathCommandsXml);

	// returns false if not plain text or a valid command
	bool GetCommandId(const char* msg, ULONG& commandId, const char*& remainder);

private:
	ULONG mapCommand(const std::string& cmd);

	CommandMap m_map;
	char m_commandChar; // character indicating the next word is a command
	ULONG m_defaultCommand; // if DEFAULT_COMMAND_STRING the text, then use this command id
};

} // namespace KEP
