/******************************************************************************
 IKEPObject.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

class IGetSet;

class IKEPObject {
public:
	virtual IGetSet *Values() = 0;
	virtual ~IKEPObject() {}
};

} // namespace KEP

