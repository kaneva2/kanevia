#pragma once

namespace KEP {

class DownloadHandler {
public:
	virtual bool BytesDownloaded(char *bytes, ULONG len) = 0;
	virtual void DownloadComplete() {}
	virtual void DownloadCanceled() = 0;
	virtual ~DownloadHandler() {}
};

} // namespace KEP
