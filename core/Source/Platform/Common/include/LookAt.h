///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "ObjectType.h"
#include "Core/Math/Vector.h"
#include "Core/Math/Matrix.h"

namespace KEP {
class IMemSizeGadget;

struct LookAtInfo {
	ObjectType targetType;
	int targetId;
	std::string targetBone;
	Vector3f offset;
	bool followTarget;
	bool allowXZRotation;
	Matrix44f* pForwardDirectionMatrix;

	LookAtInfo() :
			targetType(ObjectType::NONE), targetId(0), offset(0.0f, 0.0f, 0.0f), followTarget(false), allowXZRotation(false), pForwardDirectionMatrix(NULL) {
	}

	~LookAtInfo() {
		if (pForwardDirectionMatrix) {
			delete pForwardDirectionMatrix;
			pForwardDirectionMatrix = NULL;
		}
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

} // namespace KEP
