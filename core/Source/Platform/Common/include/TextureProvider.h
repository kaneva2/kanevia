///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// A place to hold all variations of texture override logics such as textures applied through pattern browser,
// UGC dynamic object / accessory / particle textures, UGC clothing/furniture textures and etc.

#include "ItemTextureInfo.h"
#include "KEPFileNames.h"
#include <string>
#include <map>
#include <memory>

namespace KEP {

class IMemSizeGadget;
class ContentMetadata;

class TextureProvider {
public:
	~TextureProvider() {}
	virtual std::string getUrl(ItemTextureOrdinal key, bool multiresJpg) { return ""; }

	static void SetUniqueTextureUrlPrefix(const std::string& prefix) { ms_uniqueTextureUrlPrefix = prefix; }

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const {}

protected:
	static std::string ms_uniqueTextureUrlPrefix;
};

typedef std::shared_ptr<TextureProvider> TextureProviderPtr;

// Used for assets with single texture: e.g. custom texture applied through pattern browser to dynamic objects
class CustomTextureProvider : public TextureProvider {
public:
	CustomTextureProvider(const std::string& url);
	virtual std::string getUrl(ItemTextureOrdinal key, bool multiresJpg) override { return multiresJpg ? "" : m_url; }

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	std::string m_url;
};

// Per-item textures for UGC items from shop
class UGCItemTextureProvider : public TextureProvider {
public:
	UGCItemTextureProvider(const ContentMetadata& md);
	virtual std::string getUrl(ItemTextureOrdinal key, bool multiresJpg) override;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	std::string m_textureUrlTemplate;
	std::string m_JPEGUrlTemplate;
	TextureUniqueIdMap m_textureUniqueIds;
	std::string m_uniqueTextureFileStore;
};

} // namespace KEP
