///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core\Math\Vector.h"
#include "Core\Math\Matrix.h"
#include "Core\Math\Primitive.h"

namespace KEP {

class IMemSizeGadget;

void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const Matrix44f& param);
void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const Vector3f& param);
void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const Vector2f& param);
void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const PrimitiveF& param);

} // namespace KEP
