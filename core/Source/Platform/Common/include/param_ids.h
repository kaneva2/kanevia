#pragma once

/* generated by 
select concat( 'const int PARAM_', RPAD(UPPER(REPLACE(name,' ','_')),30,' '),' = ',param_type_id, '; // ', description ) 
from parameters;
*/

const int PARAM_HEALING_AMT = 1; // for use_type_healing, amount of healing
const int PARAM_AMMO = 2; // placeholder
const int PARAM_SKILL = 3; // placeholder
const int PARAM_MENU_ID = 4; // for use_type_menu
const int PARAM_COMBINE = 5; // placeholder
const int PARAM_HOUSING_ID = 6; // for use_type_place_house
const int PARAM_INSTALL_HOUSE_ID = 7; // for use_type_install_house
const int PARAM_SUMMON_AI_CFG_ID = 8; // for use_type_summon_creature
const int PARAM_OLD_SCRIPT_PARAM = 9; // for old use_type script, passed to event
const int PARAM_DYN_OBJ_ID = 10; // for use_type_add*obj
const int PARAM_ATTACH_DYN_OBJ_ID = 11; // for use_type_add*obj
const int PARAM_MOUNT_AI_CFG_ID = 12; // for mounting ai
const int PARAM_TEXTURE_ASSET_ID = 14; // FK to kaneva assets
const int PARAM_TEXTURE_URL = 15; // from kaneva asset or personal community
const int PARAM_ASSET_GROUP_ID = 16; // FK to kaneva.asset_groups, basically playlist id
const int PARAM_SWF_NAME = 17; // Local or URL
const int PARAM_SWF_PARAMETER = 18; // optional playlist for object.
const int PARAM_SWF_ASSET_ID = 19; // asset id of swf
const int PARAM_FRIEND_ID = 20; // friend id, used to show friend's picture and activate related actions
const int PARAM_SCRIPT_FILE_PATH = 29; //
const int PARAM_SCRIPT_CREATED_DATE = 30; //
const int PARAM_EXCLUSION_GROUPS = 31; //
const int PARAM_ANIMATION_GLID = 32; // FK to items table associating an animation with a dynamic object placement id.
const int PARAM_GAME_ITEM_ID = 33; // New game item ID.
const int PARAM_TYPE_SCRIPT_GAME_ITEM_BUNDLE_GLID = 34;
const int PARAM_TYPE_PREMIUM_ITEM_CREDIT_DONATION = 35; // Used by only 4 premium items - 4511392/4511393/4511394/4511395
const int PARAM_TYPE_EFFECT_DURATION = 36; // Duration for animation, sound and particle

const int PARAM_ANIMATION = 200; // event
const int PARAM_P2P_ANIMATION = 201; // event
const int PARAM_NPC = 202; // event
const int PARAM_DEED = 203; // event
const int PARAM_QUEST = 204; // event
const int PARAM_PARTICLE_GLID = 206;
const int PARAM_SOUND = 207; // sound effect
const int PARAM_DEFAULT_GLID = 208; // action item default object GLID
