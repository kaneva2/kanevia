/******************************************************************************
 AdminRequestProcessor.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <log4cplus/logger.h>
using namespace log4cplus;

#include "TinyXML/tinyxml.h"
#include "Core/Util/fast_mutex.h"

#include <string>
#include <map>
typedef map<string, time_t> CookieMap;

namespace KEP {

class IDaemonManager;

/***********************************************************
CLASS

	AdminRequestProcessor
	
	base class for processing requests for the controller
	and admin

DESCRIPTION

***********************************************************/
class AdminRequestProcessor {
public:
	AdminRequestProcessor() :
			m_pLogger(0), m_port(0), m_shutdown(false), m_adminTimeoutSec(600), m_socketTimeoutMs(3000) {}
	virtual ~AdminRequestProcessor() {}

	virtual bool Init(Logger &logger) {
		m_pLogger = &logger;
		return true;
	}
	bool ProcessRequests();
	void StopProcessing();

protected:
	virtual void newCookie(string &cookie);
	virtual bool processRequest(const char *objectId, const char *cookie, TiXmlElement *node, string &resultXml) = 0;
	// called when doing something like checking password, this will refresh settings from disk
	virtual bool reloadConfig() = 0;

	Logger *m_pLogger;
	Logger &logger() {
		jsAssert(m_pLogger != 0);
		return *m_pLogger;
	}

	// must be set in derived class's Init()
	string m_debugAdminFile;
	string m_adminUser;
	string m_adminUserHash;
	UINT m_adminTimeoutSec; // timeout for cookies from admin i/f
	UINT m_socketTimeoutMs;
	int m_port;

private:
	bool m_shutdown;
	fast_recursive_mutex m_sync;
	CookieMap m_cookies;
	void processXml(const char *actionXml, string &resultXml);
	void actionResultError(string &resultXml, LONG errNo, const string &errMsg);
	UINT checkLogin(TiXmlElement *root, string &cookie);
};

} // namespace KEP

