///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <d3d9.h>

namespace KEP {

class ITextureProxy {
public:
	virtual ~ITextureProxy() {}

	virtual IDirect3DTexture9* getTexturePtr(bool chainLoad = false) = 0;

	virtual bool ready() = 0;
};

} // namespace KEP
