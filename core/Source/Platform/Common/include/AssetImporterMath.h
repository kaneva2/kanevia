///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <math.h>
#include "Core/Math/Vector.h"

#define AIM_FLOAT_MIN FLT_MIN * 10

namespace KEP {

struct ImpFloatVector3;
struct ImpFloatVector4;
class ImpFloatMatrix4;

struct ImpFloatVector3 {
	union {
		struct {
			float x, y, z;
		};
		float val[3];
	};

	ImpFloatVector3() { clear(); }

	ImpFloatVector3(float ax, float ay, float az) {
		x = ax;
		y = ay;
		z = az;
	}

	ImpFloatVector3(const ImpFloatVector4& v); // use inline after definition of ImpFloatVector4

	void clear() {
		memset(val, 0, sizeof(val));
	}

	float& operator[](int index) { return val[index]; }
	float operator[](int index) const { return val[index]; }

	ImpFloatVector3& operator+=(const ImpFloatVector3& v) {
		val[0] += v[0];
		val[1] += v[1];
		val[2] += v[2];
		return *this;
	}

	ImpFloatVector3& operator-=(const ImpFloatVector3& v) // Redundant, but faster
	{
		val[0] -= v[0];
		val[1] -= v[1];
		val[2] -= v[2];
		return *this;
	}

	ImpFloatVector3& operator*=(float coeff) {
		val[0] *= coeff;
		val[1] *= coeff;
		val[2] *= coeff;
		return *this;
	}

	float getNorm() const {
		return sqrtf(x * x + y * y + z * z);
	}

	float getNormSquare() const {
		return x * x + y * y + z * z;
	}

	void normalize() {
		float norm = getNorm();
		if (norm <= AIM_FLOAT_MIN) {
			x = 1.0f;
			y = 0.0f;
			z = 0.0f;
		} else {
			x /= norm;
			y /= norm;
			z /= norm;
		}
	}

	operator Vector3f();

	friend ImpFloatVector3 operator+(const ImpFloatVector3& v1, const ImpFloatVector3& v2) {
		ImpFloatVector3 ret(v1);
		ret[0] += v2[0];
		ret[1] += v2[1];
		ret[2] += v2[2];
		return ret;
	}

	friend ImpFloatVector3 operator-(const ImpFloatVector3& v1, const ImpFloatVector3& v2) // Redundant, but faster
	{
		ImpFloatVector3 ret(v1);
		ret[0] -= v2[0];
		ret[1] -= v2[1];
		ret[2] -= v2[2];
		return ret;
	}

	friend ImpFloatVector3 operator*(const ImpFloatVector3& v, float coeff) {
		ImpFloatVector3 ret(v);
		ret[0] *= coeff;
		ret[1] *= coeff;
		ret[2] *= coeff;
		return ret;
	}

	friend ImpFloatVector3 operator-(const ImpFloatVector3& v) {
		ImpFloatVector3 ret;
		ret[0] = -v[0];
		ret[1] = -v[1];
		ret[2] = -v[2];
		return ret;
	}
};

struct ImpFloatVector4 {
	union {
		struct {
			float x, y, z, w;
		};
		float val[4];
	};

	ImpFloatVector4() { clear(); }

	ImpFloatVector4(float ax, float ay, float az, float aw = 1) {
		x = ax;
		y = ay;
		z = az;
		w = aw;
	}

	ImpFloatVector4(const ImpFloatVector3 v) {
		memcpy(val, v.val, sizeof(float) * 3);
		val[3] = 1;
	}

	void clear() {
		memset(val, 0, sizeof(val));
		val[3] = 1;
	}

	float& operator[](int index) { return val[index]; }
	float operator[](int index) const { return val[index]; }

	ImpFloatVector4& operator+=(const ImpFloatVector4& v) {
		float w1 = operator[](3);
		float w2 = v[3];

		if (w1 == w2) // Most common case - use less comparison and fast algorithm to improve efficiency
		{
			val[0] += v[0];
			val[1] += v[1];
			val[2] += v[2];
		}

		if (w1 == 0)
			*this = v;
		else if (w2 != 0) {
			//[x1:y1:z1:w1] + [x2:y2:z2:w2]
			val[0] = operator[](0) * w2 + v[0] * w1;
			val[1] = operator[](1) * w2 + v[1] * w1;
			val[2] = operator[](2) * w2 + v[2] * w1;
			val[3] = w1 * w2;
		}

		return *this;
	}

	ImpFloatVector4& operator-=(const ImpFloatVector4& v) {
		return (*this) += -v;
	}

	ImpFloatVector4& operator*=(float coeff) {
		val[0] *= coeff;
		val[1] *= coeff;
		val[2] *= coeff;
		return *this;
	}

	float getNorm() const {
		return sqrtf(x * x + y * y + z * z);
	}

	float getNormSquare() const {
		return x * x + y * y + z * z;
	}

	void normalize() //@@Watch: normalization of a homogeneous vector
	{
		float norm = getNorm();
		if (norm < AIM_FLOAT_MIN) {
			x = 1.0f;
			y = 0.0f;
			z = 0.0f;
		} else {
			x /= norm;
			y /= norm;
			z /= norm;
		}
	}

	void homogenize() {
		if (fabs(w) < AIM_FLOAT_MIN)
			return; //Everything is the same at infinite distance

		x /= w;
		y /= w;
		z /= w;
		w = 1;
	}

	friend ImpFloatVector4 operator+(const ImpFloatVector4& v1, const ImpFloatVector4& v2) {
		ImpFloatVector4 ret(v1);
		ret += v2;
		return ret;
	}

	friend ImpFloatVector4 operator-(const ImpFloatVector4& v1, const ImpFloatVector4& v2) {
		ImpFloatVector4 ret(v1);
		ret -= v2;
		return ret;
	}

	friend ImpFloatVector4 operator*(const ImpFloatVector4& v, float coeff) {
		ImpFloatVector4 ret(v);
		ret[0] *= coeff;
		ret[1] *= coeff;
		ret[2] *= coeff;
		return ret;
	}

	friend ImpFloatVector4 operator-(const ImpFloatVector4& v) {
		ImpFloatVector4 ret;
		ret[0] = -v[0];
		ret[1] = -v[1];
		ret[2] = -v[2];
		return ret;
	}
};

inline ImpFloatVector3::ImpFloatVector3(const ImpFloatVector4& v) {
	float coeff = v.val[3];
	jsAssert(coeff == 0 || coeff == 1);

	//if (coeff==0) { clear(); return; }

	memcpy(val, v.val, sizeof(float) * 3);

	if (coeff != 1 && fabs(coeff) >= AIM_FLOAT_MIN) {
		val[0] /= coeff;
		val[1] /= coeff;
		val[2] /= coeff;
	}
};

class ImpFloatMatrix4 {
private:
	union {
		float m[4][4]; // column-major vector
		struct {
			float
				_m11,
				_m21, _m31, _m41,
				_m12, _m22, _m32, _m42,
				_m13, _m23, _m33, _m43,
				_m14, _m24, _m34, _m44;
		};
	};

public:
	ImpFloatMatrix4(float val = 1) {
		Identity();
		(*this) *= val;
	}

	void clear() { // Initialized as identity matrix
		Identity();
	}

	float valueAt(int col, int row) const {
		return m[col][row];
	}

	float* operator[](int index) {
		return m[index];
	}

	//! return MatrixA + MatrixB
	friend ImpFloatMatrix4 operator+(const ImpFloatMatrix4& matA, const ImpFloatMatrix4& matB) {
		ImpFloatMatrix4 result;
		for (int row = 0; row < 4; row++)
			for (int col = 0; col < 4; col++)
				result[col][row] = matA.valueAt(col, row) + matB.valueAt(col, row);

		return result;
	}

	//! return -Matrix
	friend ImpFloatMatrix4 operator-(const ImpFloatMatrix4& mat) {
		ImpFloatMatrix4 result;
		for (int row = 0; row < 4; row++)
			for (int col = 0; col < 4; col++)
				result[col][row] = -mat.valueAt(col, row);

		return result;
	}

	//! return MatrixA - MatrixB
	friend ImpFloatMatrix4 operator-(const ImpFloatMatrix4& matA, const ImpFloatMatrix4& matB) {
		ImpFloatMatrix4 result;
		for (int row = 0; row < 4; row++)
			for (int col = 0; col < 4; col++)
				result[col][row] = matA.valueAt(col, row) - matB.valueAt(col, row);

		return result;
	}

	//! return Matrix * coefficient
	friend ImpFloatMatrix4 operator*(const ImpFloatMatrix4& mat, float coeff) {
		ImpFloatMatrix4 result(mat);
		result *= coeff;
		return result;
	}

	//! return MatrixA * MatrixB
	friend ImpFloatMatrix4 operator*(const ImpFloatMatrix4& matA, const ImpFloatMatrix4& matB) {
		// Compose curr matrix with matrix passed in
		ImpFloatMatrix4 ret;
		for (int row = 0; row < 4; row++) {
			for (int col = 0; col < 4; col++) {
				ret[col][row] = 0;

				for (int loop = 0; loop < 4; loop++)
					ret[col][row] += matA.valueAt(loop, row) * matB.valueAt(col, loop);
			}
		}

		return ret;
	}

	//! return Vector * Matrix
	friend ImpFloatVector4 operator*(const ImpFloatVector4& v, const ImpFloatMatrix4& m) {
		ImpFloatVector4 ret;

		for (int col = 0; col < 4; col++) {
			ret[col] = 0;
			for (int loop = 0; loop < 4; loop++)
				ret[col] += v[loop] * m.valueAt(col, loop);
		}

		return ret;
	}

	//! return Vector3 * Matrix
	friend ImpFloatVector3 operator*(const ImpFloatVector3& v, const ImpFloatMatrix4& m) {
		return ImpFloatVector4(v) * m;
	}

	// MatrixA += MatrixB
	ImpFloatMatrix4& operator+=(const ImpFloatMatrix4& mat) {
		for (int col = 0; col < 4; col++)
			for (int row = 0; row < 4; row++) (*this)[col][row] += mat.valueAt(col, row);
		return *this;
	}

	// MatrixA -= MatrixB
	ImpFloatMatrix4& operator-=(const ImpFloatMatrix4& mat) {
		for (int col = 0; col < 4; col++)
			for (int row = 0; row < 4; row++) (*this)[col][row] -= mat.valueAt(col, row);
		return *this;
	}

	// MatrixA *= MatrixB
	ImpFloatMatrix4& operator*=(const ImpFloatMatrix4& mat) {
		(*this) = (*this) * mat;
		return *this;
	}

	// MatrixA *= coefficient
	ImpFloatMatrix4& operator*=(float coeff) {
		for (int col = 0; col < 4; col++)
			for (int row = 0; row < 4; row++) (*this)[col][row] *= coeff;

		return *this;
	}

	// Matrix(T)
	void Transpose() {
		for (int col = 1; col < 4; col++) {
			for (int row = 0; row <= col; row++) {
				float tmp = m[col][row];
				m[col][row] = m[row][col];
				m[row][col] = tmp;
			}
		}
	}

	void scaleColumn(int col, float scalar) {
		for (int row = 0; row < 4; row++)
			m[col][row] *= scalar;
	}

	void scaleRow(int row, float scalar) {
		for (int col = 0; col < 4; col++)
			m[col][row] *= scalar;
	}

	void scaleAndAddColumns(int dstCol, int srcCol, float scalar) {
		for (int row = 0; row < 4; row++)
			m[dstCol][row] += m[srcCol][row] * scalar;
	}

	void scaleAndAddRows(int dstRow, int srcRow, float scalar) {
		for (int col = 0; col < 4; col++)
			m[col][dstRow] += m[col][srcRow] * scalar;
	}

	void swapColumns(int colA, int colB) {
		for (int row = 0; row < 4; row++) {
			float tmp = m[colA][row];
			m[colA][row] = m[colB][row];
			m[colB][row] = tmp;
		}
	}

	void swapRows(int rowA, int rowB) {
		for (int col = 0; col < 4; col++) {
			float tmp = m[col][rowA];
			m[col][rowA] = m[col][rowB];
			m[col][rowB] = tmp;
		}
	}

	// Matrix(-1)
	BOOL Inverse() {
		ImpFloatMatrix4 helper = *this;
		Identity();

		for (int col = 0; col < 4; col++) {
			if (helper.m[col][col] == 0) {
				int maxCol = -1;
				float max = 0;
				for (int testCol = col + 1; testCol < 4; testCol++) {
					if (fabs(helper[testCol][col]) > max) {
						maxCol = testCol;
						max = fabs(helper[testCol][col]);
					}
				}

				if (maxCol == -1) {
					jsAssert(FALSE); // no inversion available
					return FALSE;
				}

				// Swap column
				helper.swapColumns(col, maxCol);
				this->swapColumns(col, maxCol);
			}

			float scalar = 1.0f / helper.m[col][col];
			helper.scaleColumn(col, scalar);
			this->scaleColumn(col, scalar);

			for (int elimCol = col + 1; elimCol < 4; elimCol++) {
				scalar = -helper[elimCol][col];
				if (scalar == 0)
					continue; // already zero

				// Elimination
				helper.scaleAndAddColumns(elimCol, col, scalar);
				this->scaleAndAddColumns(elimCol, col, scalar);
			}
		}

		// Now we have the triangle matrix
		for (int col = 3; col >= 0; col--) {
			for (int elimCol = col - 1; elimCol >= 0; elimCol--) {
				float scalar = -helper.m[elimCol][col];
				if (scalar == 0)
					continue; // already zero

				// Elimination
				helper.scaleAndAddColumns(elimCol, col, scalar);
				this->scaleAndAddColumns(elimCol, col, scalar);
			}
		}

		return TRUE;
	}

	// return transformed vector
	ImpFloatVector3 Transform(const ImpFloatVector3& v) const {
		return v * (*this);
	}

	// return transformed vector
	ImpFloatVector4 Transform(const ImpFloatVector4& v) const {
		return v * (*this);
	}

	// set identity matrix
	// | 1      0      0      0 |
	// | 0      1      0      0 |
	// | 0      0      1      0 |
	// | 0      0      0      1 |
	void Identity() {
		memset(m, 0, sizeof(m));
		for (int i = 0; i < 4; i++)
			m[i][i] = 1;
	}

	// set translational matrix (column-order)
	// | 1      0      0      0 |
	// | 0      1      0      0 |
	// | 0      0      1      0 |
	// | Tx     Ty     Tz     1 |
	void Translate(float x, float y, float z) {
		Identity();
		m[0][3] = x;
		m[1][3] = y;
		m[2][3] = z;
	}

	void Scale(float sx, float sy, float sz) {
		Identity();
		m[0][0] = sx;
		m[1][1] = sy;
		m[2][2] = sz;
	}

	// Set rotational matrix (column-order)
	// | 1      0      0      0 |
	// | 0      cos   -sin    0 |
	// | 0      sin    cos    0 |
	// | 0      0      0      1 |
	void RotateX(float rad) {
		Identity();
		m[1][1] = m[2][2] = cosf(rad);
		m[1][2] = sinf(rad);
		m[2][1] = -m[1][2];
	}

	// Set rotational matrix (column-order)
	// | cos    0    sin    0 |
	// | 0      1    0      0 |
	// |-sin    0    cos    0 |
	// | 0      0    0      1 |
	void RotateY(float rad) {
		Identity();
		m[0][0] = m[2][2] = cosf(rad);
		m[0][2] = -sinf(rad);
		m[2][0] = -m[0][2];
	}

	// Set rotational matrix (column-order)
	// | cos   -sin    0     0 |
	// | sin    cos    0     0 |
	// | 0      0      1     0 |
	// | 0      0      0     1 |
	void RotateZ(float rad) {
		Identity();
		m[0][0] = m[1][1] = cosf(rad);
		m[0][1] = sinf(rad);
		m[1][0] = -m[0][1];
	}

	void Decompose(ImpFloatVector3& dirVector, ImpFloatVector3& upVector, ImpFloatVector3& position, ImpFloatVector3& scales) const {
		ImpFloatVector3 tmpDir(0.0f, 0.0f, 1.0f);
		dirVector = Transform(tmpDir);
		dirVector.normalize();

		ImpFloatVector3 tmpUp(0.0f, 1.0f, 0.0f);
		upVector = Transform(tmpUp);
		upVector.normalize();

		position.x = valueAt(0, 3);
		position.y = valueAt(1, 3);
		position.z = valueAt(2, 3);

		scales.x = scales.y = scales.z = 1.0f;
	}

#ifdef TRACE
	void debug(int precision = 3) const {
		char fmt[40];
		sprintf_s(fmt, "%%%d.%df  ", 5 + precision, precision);

		for (int row = 0; row < 4; row++) {
			TRACE("[ ");
			for (int col = 0; col < 4; col++) {
				char buf[100];
				sprintf_s(buf, fmt, m[col][row]);
				TRACE(buf);
			}

			TRACE(" ]\n");
		}
	}
#endif
};
} // namespace KEP