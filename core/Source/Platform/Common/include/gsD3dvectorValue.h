#pragma once

#if DRF_ADMINISTERED
#include "KEP/Util/KEPUtil.h"
#include "KEPHelpers.h"
#include <d3d9.h>

#include <Administered.h>

namespace KEP {

class KEPUTIL_EXPORT gsD3dvectorValue : public Administered {
public:
	gsD3dvectorValue(D3DVECTOR *v, WORD parentIndent, WORD parentGroupTitleId) :
			m_d3dVector(v),
			m_indent(parentIndent),
			m_groupTitleId(parentGroupTitleId) {}

	D3DVECTOR *vector() { return m_d3dVector; }
	DECLARE_GETSET
private:
	D3DVECTOR *m_d3dVector;
	WORD m_indent;
	UINT m_groupTitleId;
};

} // namespace KEP

#endif
