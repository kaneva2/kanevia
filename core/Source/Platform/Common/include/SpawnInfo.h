///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "CStructuresMisl.h"
#include "KEPConstants.h"

namespace KEP {

class SpawnInfo {
public:
	SpawnInfo() :
			m_spawnId(0), m_pMsgSpawnToClient(nullptr), m_messageSize(0), m_coverCharge(0), m_zoneOwnerId(0), m_zonePermissions(ZP_UNKNOWN) {}

	void updateSpawnMsg(MSG_SPAWN_TO_CLIENT* spawnMsg, ULONG msgSize) {
		assert(spawnMsg != nullptr && msgSize != 0);
		if (m_pMsgSpawnToClient) {
			delete m_pMsgSpawnToClient;
			m_pMsgSpawnToClient = nullptr;
		}
		m_pMsgSpawnToClient = spawnMsg;
		m_messageSize = msgSize;
		m_spawnId = s_nextSpawnId++;
	}

	void reset() {
		if (m_pMsgSpawnToClient) {
			delete m_pMsgSpawnToClient;
			m_pMsgSpawnToClient = nullptr;
		}
		m_messageSize = 0;
		m_spawnId = 0;
		m_coverCharge = 0;
		m_zoneOwnerId = 0;
		m_zonePermissions = ZP_UNKNOWN;
	}

	~SpawnInfo() {
		delete m_pMsgSpawnToClient;
	}

	bool isSet() const { return m_spawnId > 0; }

	unsigned short getSpawnId() const { return m_spawnId; }
	int getZoneIndexInt() const {
		assert(m_pMsgSpawnToClient);
		return m_pMsgSpawnToClient ? m_pMsgSpawnToClient->zoneIndexInt : 0;
	}
	int getZoneInstanceId() const {
		assert(m_pMsgSpawnToClient);
		return m_pMsgSpawnToClient ? m_pMsgSpawnToClient->instanceId : 0;
	}
	Vector3f getSpawnPt() const {
		assert(m_pMsgSpawnToClient);
		return m_pMsgSpawnToClient ? Vector3f(m_pMsgSpawnToClient->x, m_pMsgSpawnToClient->y, m_pMsgSpawnToClient->z) : Vector3f(0, 0, 0);
	}
	const MSG_SPAWN_TO_CLIENT* getSpawnMsg(size_t& size) const {
		size = m_messageSize;
		return m_pMsgSpawnToClient;
	}

	BYTE getZonePermissions() const { return m_zonePermissions; }
	int getCoverCharge() const { return m_coverCharge; }
	unsigned getZoneOwnerId() const { return m_zoneOwnerId; }
	unsigned getParentZoneInstanceId() const { return m_parentZoneInstanceId; }

	void clearZoneInfo() {
		m_zonePermissions = ZP_UNKNOWN;
		m_parentZoneInstanceId = 0;
	}
	void setZonePermissions(BYTE perm) { m_zonePermissions = perm; }
	void setCoverCharge(int coverCharge, int ownerId) {
		m_coverCharge = coverCharge;
		m_zoneOwnerId = ownerId;
	}
	void setParentZoneInstanceId(unsigned instanceId) { m_parentZoneInstanceId = instanceId; }

private:
	unsigned short m_spawnId; // simple id echoed back from client
	MSG_SPAWN_TO_CLIENT* m_pMsgSpawnToClient;
	size_t m_messageSize;
	int m_coverCharge;
	unsigned m_zoneOwnerId;
	BYTE m_zonePermissions;
	unsigned m_parentZoneInstanceId = 0;

	static std::atomic<USHORT> s_nextSpawnId;
};

} // namespace KEP