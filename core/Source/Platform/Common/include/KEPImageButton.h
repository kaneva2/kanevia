/******************************************************************************
 KEPImageButton.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_KEPIMAGEBUTTON_H__16C6D980_BD45_11D3_BDA3_00104B133581__INCLUDED_)
#define AFX_KEPIMAGEBUTTON_H__16C6D980_BD45_11D3_BDA3_00104B133581__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// KEPImageButton.h : header file
//

// Warning in Windows SDK header when building with VS2015
#pragma warning(push)
#pragma warning(disable : 4458) // declaration of '' hides class member
#include <gdiplus.h>
#pragma warning(pop)

class CKEPImageButton : public CBitmapButton {
	DECLARE_DYNAMIC(CKEPImageButton);

public:
	CKEPImageButton();
	CKEPImageButton(Gdiplus::Color &fontColor, WCHAR *fontName, UINT fontSize, Gdiplus::FontStyle fontStyle);
	virtual ~CKEPImageButton();

	void SetToolTipText(CStringW *spText, BOOL bActivate = TRUE);
	void SetToolTipText(int nId, BOOL bActivate = TRUE);
	void SetFontStyle(Gdiplus::FontStyle fontStyle);
	void SetFontSize(Gdiplus::REAL fontSize);
	void SetShowCaption(BOOL bShowCaption) { m_bShowCaption = bShowCaption; }
	void SetUseHover(BOOL bUseHover) { m_bUseHover = bUseHover; }

	BOOL LoadImages(CStringA pathNormal, CStringA pathHover = CStringA(""), CStringA pathSelected = CStringA(""), CStringA pathDisabled = CStringA(""));
	BOOL LoadImages(const CStringA &pathNormal, const Gdiplus::Color &colorKeyLow, const Gdiplus::Color &colorKeyHigh, const CStringA &pathHover = CStringA(""), const CStringA &pathSelected = CStringA(""), const CStringA &pathDisabled = CStringA(""));

protected:
	// Generated message map functions

	//{{AFX_MSG(CKEPImageButton)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnMouseHover(WPARAM wparam, LPARAM lparam);
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKEPImageButton)
protected:
	virtual BOOL PreTranslateMessage(MSG *pMsg);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

	void InitToolTip();
	void ActivateTooltip(BOOL bActivate = TRUE);

protected:
	BOOL m_bHover;
	BOOL m_bTracking;
	BOOL m_bTransparency;
	BOOL m_bShowCaption;
	BOOL m_bUseHover;

	Gdiplus::SolidBrush m_fontBrush; // used for the font color
	Gdiplus::Font *m_font;
	Gdiplus::Color m_colorKeyLow; // low RGB mask value
	Gdiplus::Color m_colorKeyHigh; // high RGB mask value

	Gdiplus::Image *m_imgNormal;
	Gdiplus::Image *m_imgHover;
	Gdiplus::Image *m_imgSelected;
	Gdiplus::Image *m_imgDisabled;

	CToolTipCtrl m_toolTip;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KEPIMAGEBUTTON_H__16C6D980_BD45_11D3_BDA3_00104B133581__INCLUDED_)
