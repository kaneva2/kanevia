#pragma once

#include <KEPHelpers.h>
//#include <log4cplus/Logger.h>

namespace KEP {

class IDispatcher;
class CTriggerObjectList;
class CWldObjectList;

class TriggerFile {
public:
	static bool RegisterTriggerEvents(const char *fname, IDispatcher &dispatcher, CTriggerObjectList *list);
	static void RegisterAllTriggerEvents(const char *gameDir, IDispatcher &dispatcher, CTriggerObjectList *list);
};

} // namespace KEP
