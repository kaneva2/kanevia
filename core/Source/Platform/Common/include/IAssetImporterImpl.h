///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <set>
#include <map>
#include "AssetImporterMath.h"

namespace KEP {

class CBoneObject;
class CDeformableVisObj;
class CEXMeshObj;
class CHiarcleVisObj;
typedef CHiarcleVisObj CHierarchyVisObj;
class CMaterialObject;
class CSkeletonAnimation;
class CSkeletonObject;
class CVertexAnimationModule;
class ColladaSkeleton;
enum enumUPAXIS;

class IAssetImporterImpl {
protected:
	std::string rootURI;

public:
	IAssetImporterImpl() {}
	IAssetImporterImpl(const std::string& aRootURI) {
		rootURI = aRootURI;
	}

	virtual ~IAssetImporterImpl() {}

	const std::string& GetRootURI() {
		return rootURI;
	}

	virtual void Register() = 0;

	virtual void Unregister() = 0;

	virtual IAssetImporterImpl* spawn(const std::string& aRootURI, int aFlags) = 0;

	// For all Load* functions: return -1 = error found, 0 = asset not found, >0 = success

	virtual int EnumAssetMeshes(std::map<enum KEP_ASSET_TYPE, std::vector<std::string>>& allMeshURIs, IClassifyAssetNode* classifyFunc) = 0;

	virtual int LoadAssetMeshByName(const std::string& meshUri, CEXMeshObj& mesh, int aFlags) = 0;

	virtual void PrepareMeshForKEP(CEXMeshObj& mesh) = 0;

	virtual int EnumAssetMeshesWithSkeletons(std::map<enum KEP_ASSET_TYPE, std::vector<std::string>>& allMeshURIs, ColladaSkeleton** allSkeletons, IClassifyAssetNode* classifyFunc) = 0;
	virtual int LoadAssetSkeleton(const std::string& rootBone, CSkeletonObject& skeletonObject, int aFlags) = 0;
	virtual int LoadAssetRigidMeshByName(const std::string& meshUri, CHierarchyVisObj& hvis, std::string& joint, int aFlags) = 0;
	virtual int LoadAssetSkinnedMeshByName(const std::string& meshUri, CDeformableVisObj& dvis, const CSkeletonObject& aSkeletonObject, int aFlags) = 0;
	virtual void PrepareSkeletonForKEP(CSkeletonObject& skeleton) = 0;
	virtual void PrepareRigidMeshForKEP(CHierarchyVisObj& hvis) = 0;
	virtual void PrepareSkinnedMeshForKEP(CDeformableVisObj& dvis) = 0;

	virtual int EnumSkeletalAnimations(std::vector<std::string>& allAnimationURIs, IClassifyAssetNode* classifyFunc) = 0;
	virtual int LoadSkeletalAnimation(const std::string& animUri, CSkeletonObject* apRefSkeleton, CSkeletonAnimation& aNewAnimation, int aFlags, const AnimImportOptions* apOptions = NULL) = 0;
	virtual void PrepareSkeletalAnimationForKEP(CSkeletonAnimation& aNewAnimation) = 0; // if aAnimationIndex==-1, prepare all animations

	virtual int EnumVertexAnimations(std::vector<std::string>& allAnimationURIs, IClassifyAssetNode* classifyFunc) = 0;
	virtual int LoadVertexAnimation(const std::string& animUri, CEXMeshObj& baseMesh, CVertexAnimationModule& targets) = 0;
	virtual void PrepareVertexAnimationForKEP(CEXMeshObj& baseMesh, CVertexAnimationModule& targets) = 0;

	virtual int LoadMaterialByName(const std::string& name, CMaterialObject* mat) = 0;
	virtual void PrepareMaterialForKEP(CMaterialObject* mat) = 0;

	// Asset specs with multi-revisions (KZM v2/v3, Collada 1.3/1.4, etc)
	virtual BOOL SupportsFormat(const std::string& extension, float ver) = 0;
	virtual BOOL SupportsURI(const std::string& aURI) = 0;
	virtual float GetDocumentVersion(const std::string& aURI) = 0;

	virtual ImpFloatMatrix4 GetMatrixByURI(const std::string& meshUri) const = 0;
	virtual enumUPAXIS GetUpAxisByURI(const std::string& meshUri) const = 0;

	virtual void GetSupportedOptions(std::set<std::string>& optionNames) = 0;
	virtual bool SetOption(const std::string& optionName, int intValue) = 0;
	virtual bool GetOption(const std::string& optionName, int& intValue) = 0;
};

} // namespace KEP
