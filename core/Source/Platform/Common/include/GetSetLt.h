#pragma once

#include "IGetSet.h"

//#define GETSET_ASSERTION

// Lightweight GetSet helper that uses no in-class metadata or MFC dependencies
// Classes that use GetSetLt should declare GETSET_LT macros with resource constants defined in GetSetStrings.h
//
// Example:
//
//	class CTitleObject: GETSET_LT_BASE
// 	{
// 	public:
// 		CTitleObject() : m_titleID(-1) {}
//
// 		string	m_titleName;
// 		int     m_titleID;
//
// 		void Write(CArchive & ar) const;
// 		void Read(CArchive & ar);
//
// 		#pragma region GetSet(CTitleObject, IDS_TITLEOBJECT_NAME)
// 		BEGIN_GETSET_LT(string)
// 			GETSET_LT(m_titleName, IDS_TITLEOBJECT_TITLENAME)
// 		END_GETSET_LT
// 		BEGIN_GETSET_LT(int)
// 			GETSET_LT(m_titleID, IDS_TITLEOBJECT_TITLEID)
// 		END_GETSET_LT
// 		#pragma endregion
// 	};
//
// NOTES:
// Currently only strings and numbers are supported. They should be grouped separately
// with section macros like BEGIN_GSETSET_LT_STRINGS and BEGIN_GSETSET_LT_NUMBERS.
// Region pragma is required and used by perl parser. The region name syntax must be
// followed closely (GetSet(ClassName, IDS_STR)) as it provides hints for the parser.
//

namespace KEP {

// Adapter class with dummy implementation for all IGetSet abstract functions
class GetSetLt : public IGetSet {
	///////////////////////////////////////////////////////////////
	// dummy overrides
	virtual UINT GetNameId() { return 0; }

	///////////////////////////////////////////////////////////////
	// No longer needed
	virtual ULONG AddNewIntMember(IN const char* /*name*/, IN int /*defaultValue*/, IN ULONG /*flags*/ = amfTransient) {
		assert(false);
		return 0;
	}
	virtual ULONG AddNewDoubleMember(IN const char* /*name*/, IN double /*defaultValue*/, IN ULONG /*flags*/ = amfTransient) {
		assert(false);
		return 0;
	}
	virtual ULONG AddNewStringMember(IN const char* /*name*/, IN const char* /*defaultValue*/, IN ULONG /*flags*/ = amfTransient) {
		assert(false);
		return 0;
	}
	virtual ULONG FindIdByName(IN const char* /*name*/, OUT EGetSetType* /*type*/ = 0) {
		assert(false);
		return 0;
	}

	//////////////////////////////////////////////////////////////
	// Get functions
	virtual ULONG& GetULONG(IN ULONG index) {
#if UINT_MAX == ULONG_MAX
		unsigned* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *(ULONG*)ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		static ULONG dummy = 0;
		return dummy;
#else
#error TODO
#endif
	}

	virtual INT& GetINT(IN ULONG index) {
		int* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		static int dummy = 0;
		return dummy;
	}

	virtual BOOL& GetBOOL(IN ULONG index) {
		int* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		static BOOL dummy = 0;
		return dummy;
	}

	virtual bool& Getbool(IN ULONG index) {
		bool* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		static bool dummy = 0;
		return dummy;
	}

	virtual double GetNumber(IN ULONG index) {
		int* i = nullptr;
		if (_GetSet(index, false, i))
			return (double)*i;

		unsigned* u = nullptr;
		if (_GetSet(index, false, u))
			return (double)*u;

		float* flt = nullptr;
		if (_GetSet(index, false, flt))
			return (double)*flt;

		double* dbl = nullptr;
		if (_GetSet(index, false, dbl))
			return *dbl;

		bool* b = nullptr;
		if (_GetSet(index, false, b))
			return *b ? 1 : 0;

		unsigned char* uc = nullptr;
		if (_GetSet(index, false, uc))
			return (double)*uc;

		short* si = nullptr;
		if (_GetSet(index, false, si))
			return (double)*si;

		unsigned short* usi = nullptr;
		if (_GetSet(index, false, usi))
			return (double)*usi;

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		return 0;
	}

	virtual std::string GetString(IN ULONG index) {
		std::string* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		return std::string();
	}

	virtual std::string& Getstring(IN ULONG index) {
		std::string* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		static std::string dummy;
		return dummy;
	}

	virtual Vector3f& GetVector3f(IN ULONG index) {
		Vector3f* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		static Vector3f dummy;
		return dummy;
	}

	virtual void GetColor(IN ULONG /*index*/, OUT float* /*r*/, OUT float* /*g*/, OUT float* /*b*/, OUT float* /*a*/ = 0) { assert(false); }

	virtual IGetSet* GetObjectPtr(IN ULONG index) {
		IGetSet** ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			return *ptr;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
		static IGetSet* dummy = nullptr;
		return dummy;
	}

#define INVALID_ARRAY_INDEX ((ULONG)-1)

	virtual ULONG GetArrayCount(IN ULONG indexOfArray) {
		IGetSet* dummy;
		size_t arrayCount = 0;
		bool res = _GetSetVector(indexOfArray, INVALID_ARRAY_INDEX, dummy, arrayCount);

#ifdef GETSET_ASSERTION
		assert(res);
#else
		res;
#endif
		return arrayCount;
	}

	virtual IGetSet* GetObjectInArray(IN ULONG indexOfArray, IN ULONG indexInArray) {
		IGetSet* ptr = nullptr;
		size_t dummy = 0;
		bool res = _GetSetVector(indexOfArray, indexInArray, ptr, dummy);

#ifdef GETSET_ASSERTION
		assert(res);
#else
		res;
#endif
		assert(ptr != nullptr); // Out-of-bound array index assertion
		return ptr;
	}

	//////////////////////////////////////////////////////////////
	// Set functions
	virtual void SetNumber(IN ULONG index, IN LONG number) {
		int* i = nullptr;
		if (_GetSet(index, true, i)) {
			*i = number;
			return;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
	}

	virtual void SetNumber(IN ULONG index, IN ULONG number) {
		unsigned* u = nullptr;
		if (_GetSet(index, true, u)) {
			*u = number;
			return;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
	}

	virtual void SetNumber(IN ULONG index, IN BOOL value) {
		int* B = nullptr;
		if (_GetSet(index, true, B)) {
			*B = value;
			return;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
	}

	virtual void SetNumber(IN ULONG index, IN bool value) {
		bool* b = nullptr;
		if (_GetSet(index, true, b)) {
			*b = value;
			return;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
	}

	virtual void SetNumber(IN ULONG index, IN double number) {
		int* i = nullptr;
		if (_GetSet(index, true, i)) {
			*i = (int)number;
			return;
		}

		unsigned* u = nullptr;
		if (_GetSet(index, true, u)) {
			*u = (unsigned)number;
			return;
		}

		float* flt = nullptr;
		if (_GetSet(index, true, flt)) {
			*flt = (float)number;
			return;
		}

		double* dbl = nullptr;
		if (_GetSet(index, true, dbl)) {
			*dbl = number;
			return;
		}

		bool* b = nullptr;
		if (_GetSet(index, true, b)) {
			*b = number != 0;
			return;
		}

		unsigned char* uc = nullptr;
		if (_GetSet(index, true, uc)) {
			*uc = (unsigned char)number;
			return;
		}

		short* si = nullptr;
		if (_GetSet(index, true, si)) {
			*si = (short)number;
			return;
		}

		unsigned short* usi = nullptr;
		if (_GetSet(index, true, usi)) {
			*usi = (unsigned short)number;
			return;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
	}

	virtual void SetString(IN ULONG index, IN const char* s) {
		std::string* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			*ptr = s ? s : "";
			return;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
	}

	virtual void SetVector3f(IN ULONG index, IN Vector3f v) {
		Vector3f* ptr = nullptr;
		if (_GetSet(index, false, ptr)) {
			*ptr = v;
			return;
		}

#ifdef GETSET_ASSERTION
		assert(false);
#endif
	}

	virtual void SetColor(IN ULONG /*index*/, IN float /*r*/, IN float /*g*/, IN float /*b*/, IN float /*a*/ = 0) { assert(false); }

	///////////////////////////////////////////////////////////////
	// Override to link index to member variables

	// String
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT std::string*& /*s*/) { return false; }

	// Number
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT int*& /*i*/) { return false; }
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT unsigned*& /*u*/) { return false; }
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT double*& /*dbl*/) { return false; }
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT float*& /*flt*/) { return false; }
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT bool*& /*b*/) { return false; }
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT unsigned char*& /*uc*/) { return false; }
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT short*& /*si*/) { return false; }
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT unsigned short*& /*usi*/) { return false; }

	// Complex types
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT Vector3f*& /*v*/) { return false; }

	// Pointer
	virtual bool _GetSet(IN ULONG /*index*/, IN bool /*set*/, OUT IGetSet**& /*v*/) { return false; }

	// Vectors of IGetSet
	virtual bool _GetSetVector(IN ULONG /*index*/, IN ULONG /*indexInArray*/, OUT IGetSet*& /*gs*/, OUT size_t& /*arrSize*/) { return false; }
};

#define GETSET_LT_BASE \
public                 \
	GetSetLt

// Option 1: inline syntax
#define BEGIN_GETSET_LT(Type)                                            \
	bool _GetSet(IN ULONG index, IN bool set, OUT Type*& ptr) override { \
		Type* target =
#define END_GETSET_LT \
	NULL;             \
	if (!target)      \
		return false; \
	ptr = target;     \
	return true;      \
	}

#define BEGIN_GETSET_VECTOR_LT()                                                                                 \
	bool _GetSetVector(IN ULONG index, IN ULONG indexInArray, OUT IGetSet*& ptr, OUT size_t& arrSize) override { \
		std::vector<IGetSet*>* target =
#define END_GETSET_VECTOR_LT                           \
	NULL;                                              \
	if (!target)                                       \
		return false;                                  \
	arrSize = target->size();                          \
	if (indexInArray >= 0 && indexInArray < arrSize) { \
		ptr = (*target)[indexInArray];                 \
	}                                                  \
	return true;                                       \
	}

// Option 2: separate declaration and implementations
#define DECLARE_GETSET_LT(Type) \
	bool _GetSet(IN ULONG index, IN bool set, OUT Type*& ptr) override;
#define BEGIN_GETSET_LT_IMPL(Type, Class)                              \
	bool Class::_GetSet(IN ULONG index, IN bool set, OUT Type*& ptr) { \
		Type* target =

#define DECLARE_GETSET_VECTOR_LT() \
	bool _GetSetVector(IN ULONG index, IN ULONG indexInArray, OUT IGetSet*& ptr, OUT size_t& arrSize) override;
#define BEGIN_GETSET_VECTOR_LT_IMPL(Class)                                                                     \
	bool Class::_GetSetVector(IN ULONG index, IN ULONG indexInArray, OUT IGetSet*& ptr, OUT size_t& arrSize) { \
		std::vector<IGetSet*>* target =

#define GETSET_LT(VAR, IDS) (index == (IDS)) ? &(VAR):
#define GETSET_LT_RO(VAR, IDS) (!set && index == (IDS)) ? &(VAR):
#define GETSET_LT_CAST(VAR, IDS, Type) (index == (IDS)) ? (Type*)&(VAR):
#define GETSET_LT_CAST_RO(VAR, IDS, Type) (!set && index == (IDS)) ? (Type*)&(VAR):

} // namespace KEP