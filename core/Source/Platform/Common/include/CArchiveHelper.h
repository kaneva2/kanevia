///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

//-----------------------------------------------------------------------------
// CArchive STL wrapper

#define MAX_PERSISTENT_STRLEN 65535

struct WriteValueFunc {
	template <typename ValueType>
	void operator()(CArchive& ar, const ValueType& val) {
		ar.Write(&val, sizeof(val));
	}
};

struct ReadValueFunc {
	template <typename ValueType>
	void operator()(CArchive& ar, ValueType& val) {
		ar.Read(&val, sizeof(val));
	}
};

static WriteValueFunc WriteValue;
static ReadValueFunc ReadValue;

struct WriteSTLStringFunc {
	void operator()(CArchive& ar, const std::string& str) {
		size_t len = std::min(str.size(), (size_t) MAX_PERSISTENT_STRLEN);
		WriteValue(ar, len);
		if (!str.empty()) {
			ar.Write(str.c_str(), len);
		}
	}
};

struct ReadSTLStringFunc {
	void operator()(CArchive& ar, std::string& str) {
		size_t len;
		ReadValue(ar, len);

		if (len > 0 && len <= MAX_PERSISTENT_STRLEN) {
			char* tmp = new char[len + 1];
			ar.Read(tmp, len);
			tmp[len] = 0;

			str = tmp;
		} else {
			str.clear();
		}
	}
};

static WriteSTLStringFunc WriteSTLString;
static ReadSTLStringFunc ReadSTLString;

template <typename ValType, typename _WriteVal = WriteValueFunc>
struct WriteSTLVectorFunc {
	void operator()(CArchive& ar, const std::vector<ValType>& data) {
		unsigned int tmp = data.size();
		WriteValue(ar, tmp);

		_WriteVal WriteVal;
		for (std::vector<ValType>::const_iterator it = data.begin(); it != data.end(); ++it) {
			ValType value = *it;
			WriteVal(ar, value);
		}
	}
};

template <typename ValType, typename _ReadVal = ReadValueFunc>
struct ReadSTLVectorFunc {
	void operator()(CArchive& ar, std::vector<ValType>& data) {
		data.clear();

		unsigned int tmp;
		ReadValue(ar, tmp);

		_ReadVal ReadVal;
		for (unsigned int i = 0; i < tmp; i++) {
			ValType value;
			ReadVal(ar, value);
			data.push_back(value);
		}
	}
};

template <typename KeyType, typename ValType, typename _WriteKey = WriteValueFunc, typename _WriteVal = WriteValueFunc>
struct WriteSTLMapFunc {
	void operator()(CArchive& ar, const std::map<KeyType, ValType>& data) {
		unsigned int tmp = data.size();
		WriteValue(ar, tmp);

		_WriteKey WriteKey;
		_WriteVal WriteVal;
		for (std::map<KeyType, ValType>::const_iterator it = data.begin(); it != data.end(); ++it) {
			KeyType key = it->first;
			ValType value = it->second;
			WriteKey(ar, key);
			WriteVal(ar, value);
		}
	}
};

template <typename KeyType, typename ValType, typename _ReadKey = ReadValueFunc, typename _ReadVal = ReadValueFunc>
struct ReadSTLMapFunc {
	void operator()(CArchive& ar, std::map<KeyType, ValType>& data) {
		data.clear();

		unsigned int tmp;
		ReadValue(ar, tmp);

		_ReadKey ReadKey;
		_ReadVal ReadVal;
		for (unsigned int i = 0; i < tmp; i++) {
			KeyType key;
			ValType value;
			ReadKey(ar, key);
			ReadVal(ar, value);
			data.insert(std::make_pair(key, value));
		}
	}
};
