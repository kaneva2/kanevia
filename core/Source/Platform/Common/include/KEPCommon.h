///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define K_NAME "Kanevia"
#define K_NAME_REGISTRY L"Kanevia"

#define K_INSTALLER_NAME "KInstaller"
#define K_INSTALLER_EXE K_INSTALLER_NAME ".exe"

#define K_LAUNCHER_NAME "KLauncher"
#ifdef _DEBUG
#define K_LAUNCHER_EXE K_LAUNCHER_NAME "d.exe"
#else
#define K_LAUNCHER_EXE K_LAUNCHER_NAME ".exe"
#endif
#define K_LAUNCHER_LOG_CFG K_LAUNCHER_NAME "LogCfg.cfg"
#define K_LAUNCHER_SEMAPHORE_NAME L"KLauncher"
#define K_PATCHER_SEMAPHORE_NAME L"KPatcher"

#define K_CLIENT_NAME "KClient"
#ifdef _DEBUG
#define K_CLIENT_EXE K_CLIENT_NAME "d.exe"
#else
#define K_CLIENT_EXE K_CLIENT_NAME ".exe"
#endif
#define K_CLIENT_LOG_CFG K_CLIENT_NAME "LogCfg.cfg"
#define K_CLIENT_WINDOW_NAME L"KClient"
#define K_CLIENT_SEMAPHORE_NAME L"KClient"

#define K_CLIENT_MEDIA_NAME "KClientMedia"
#ifdef _DEBUG
#define K_CLIENT_MEDIA_EXE K_CLIENT_MEDIA_NAME "d.exe"
#else
#define K_CLIENT_MEDIA_EXE K_CLIENT_MEDIA_NAME ".exe"
#endif
#define K_CLIENT_MEDIA_LOG_CFG K_CLIENT_MEDIA_NAME "LogCfg.cfg"

#define K_SERVER_NAME "KServer"
#ifdef _DEBUG
#define K_SERVER_EXE K_SERVER_NAME "d.exe"
#else
#define K_SERVER_EXE K_SERVER_NAME ".exe"
#endif
#define K_SERVER_LOG_CFG K_SERVER_NAME "LogCfg.cfg"

#define FIX_MTQUEUE // Demarks code that, depending upon the configuration, \
		// bypasses the player serialization mechanism in MTQueue            \
		// which is used by the gamestatedispatcher.

#define DEPLOY_CLEANER

//#define DECOUPLE_DBCONFIG           // Enables modifications that decouple DBConfig from KEPConfig
//                                    // CryptDecrypt, and others.
//#define DECOUPLE_SERVER_FROM_F_IN_BOX   Pending.  Placed here as reminder.  Note KEPMenu is renamed to zmenu as a post
// build step.  f_in_box.lib is pulled in via a pragma comment(lib...) in flashmesh.obj

#define FOREVER for (;;)

// DRF - Active Test Groups (bitmask selection)
enum {
	DRF_TEST_GROUP_OFF = 0x00, ///< set to this to turn off a test
	DRF_TEST_GROUP_ON = 0x01, ///< set to this to turn on a test
	DRF_TEST_GROUP_SLEEP_ON_CRASH = 0x02, ///< enables 30sec sleep on crash
	DRF_TEST_GROUP_VM_FIX = 0x04, ///< fixes issues running under parallels vm
	DRF_TEST_GROUP_DRF_PREFS = 0x08, ///< enables drf preferences
};
#define DRF_TEST_GROUP_FLASH_MESH DRF_TEST_GROUP_OFF ///< flash mesh test group
#define DRF_TEST_GROUP_KEP_MENU DRF_TEST_GROUP_OFF ///< kep menu test group
#define DRF_TEST_GROUP_EVENT_MAP DRF_TEST_GROUP_DRF_PREFS ///< event map test group

// DRF - Feature Selectors
#define DRF_RUNTIME_REPORTER_SERVER 3600 ///< KServer runtime report selection (0=off >0=reportIntervalSec)
#define DRF_RUNTIME_REPORTER_SEND_REPORT 0 ///< runtime reporter sends report via webcall (0=off, 1=on)
#define DRF_RIPPER 1 ///< client ripper detection (0=off, 1=report, 2=disallow)
#define DRF_TEXTURE_IMPORT_OPT 0 ///< client optimizes import textures (0=off, 1=on)
#define DRF_DIRECT_SHOW_MESH 1 ///< client supports direct show media mesh (0=off, 1=on)
#define DRF_OLD_VENDORS 0 ///< supports old vendors including EA (0=off 1=on)
#define DRF_OLD_PORTALS 0 ///< supports old portals (0=off 1=on)
#define DRF_WORLD_ENVIRONMENT_LIST 0 ///< something i have never seen actually used (0=off 1=on)
#define DRF_WORLD_MIRROR_LIST 0 ///< something i have never seen actually used (0=off 1=on)

#define SCRIPT_CHECKSUM_MD5 1
#define SCRIPT_CHECKSUM_SHA256 2
#define SCRIPT_CHECKSUM_METHOD SCRIPT_CHECKSUM_MD5

#define SUPPORT_LUAC_INVERTED_VERSION_AND_FORMAT // Detect and correct if an input LUAC file has all bits inverted for version and format field

#define FMT_FLT std::setprecision(2) << std::fixed
