///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Unicode.h"
#include "KEPCommon.h"
#include "KEP/Util/KEPUtil.h"
#include <string>
#include <sstream>
#include <log4cplus/logger.h>
using namespace log4cplus;

// Forward declaration
KEPUTIL_EXPORT std::string PathApp(bool log);
__declspec(dllimport) void __stdcall Sleep(unsigned long dwMilliseconds);

// Allow specification of logger by narrow or wide character pointers, strings, and by reference to a logger.
inline log4cplus::Logger LogHelper_GetLogger(const std::wstring& strName) {
	return log4cplus::Logger::getInstance(strName);
}
inline log4cplus::Logger LogHelper_GetLogger(const wchar_t* pwzName) {
	return log4cplus::Logger::getInstance(pwzName);
}
inline log4cplus::Logger LogHelper_GetLogger(const std::string& strName) {
	return log4cplus::Logger::getInstance(KEP::Utf8ToUtf16(strName));
}
inline log4cplus::Logger LogHelper_GetLogger(const char* pszName) {
	return log4cplus::Logger::getInstance(KEP::Utf8ToUtf16(pszName));
}
inline const log4cplus::Logger& LogHelper_GetLogger(const log4cplus::Logger& logger) {
	return logger;
}

// Private Log Macros
//typedef log4cplus::Logger Logger;
#define _LogInfo(txt) LOG4CPLUS_INFO(LogHelper_GetLogger(m_logger), txt)
#define _LogTrace(txt) LOG4CPLUS_TRACE(LogHelper_GetLogger(m_logger), txt)
#define _LogDebug(txt) LOG4CPLUS_DEBUG(LogHelper_GetLogger(m_logger), txt)
#define _LogWarn(txt) LOG4CPLUS_WARN(LogHelper_GetLogger(m_logger), txt)
#define _LogError(txt) LOG4CPLUS_ERROR(LogHelper_GetLogger(m_logger), txt)
#define _LogFatal(txt) LOG4CPLUS_FATAL(LogHelper_GetLogger(m_logger), txt)
#define LogHelper_LogTxt(txt) __FUNCTION__ << ": " << txt

// Public Log Macros
#define LoggerGetInstance(a) LogHelper_GetLogger(a)
#define LogInstanceAs(logger, a) Logger logger(LogHelper_GetLogger(a))
#define LogInstance(a) LogInstanceAs(m_logger, a)
#define LogInfo(txt) _LogInfo(LogHelper_LogTxt(txt))
#define LogTrace(txt) _LogTrace(LogHelper_LogTxt(txt))
#define LogDebug(txt) _LogDebug(LogHelper_LogTxt(txt))
#define LogWarn(txt) _LogWarn(LogHelper_LogTxt(txt))
#define LogError(txt) _LogError(LogHelper_LogTxt(txt))
#define LogFatal(txt) _LogFatal(LogHelper_LogTxt(txt))
#define LogInfoTo(logger, txt) LOG4CPLUS_INFO(LogHelper_GetLogger(logger), LogHelper_LogTxt(txt))
#define LogTraceTo(logger, txt) LOG4CPLUS_TRACE(LogHelper_GetLogger(logger), LogHelper_LogTxt(txt))
#define LogDebugTo(logger, txt) LOG4CPLUS_DEBUG(LogHelper_GetLogger(logger), LogHelper_LogTxt(txt))
#define LogWarnTo(logger, txt) LOG4CPLUS_WARN(LogHelper_GetLogger(logger), LogHelper_LogTxt(txt))
#define LogErrorTo(logger, txt) LOG4CPLUS_ERROR(LogHelper_GetLogger(logger), LogHelper_LogTxt(txt))
#define LogFatalTo(logger, txt) LOG4CPLUS_FATAL(LogHelper_GetLogger(logger), LogHelper_LogTxt(txt))
#define LogBool(b) ((b) ? "YES" : "NO")
#define LogEnum(e) ((UINT)(e))
#define LogPath(p) StrStripFilePath(p, PathApp(false))

// Initializes Logger On Own Thread
#define LogInit(pathLogCfg) _LogInit(__FUNCTION__, pathLogCfg)
inline bool _LogInit(const std::string& func, const std::string& pathLogCfg, unsigned int timeRefreshSec = 10) {
	// Spin Up New Logging Thread
	new log4cplus::ConfigureAndWatchThread(KEP::Utf8ToUtf16(pathLogCfg).c_str(), timeRefreshSec * 1000);
	::Sleep(10); // 10ms to spin up

	// Start Logging
	static LogInstance("Instance");
	_LogInfo(func << "::LogInit: OK");

	return true;
}

// Debug Macros
#ifdef _DEBUG
#define DBGOUT(x)                                 \
	{                                             \
		std::stringstream dbgOut;                 \
		dbgOut << x << std::endl;                 \
		OutputDebugStringA(dbgOut.str().c_str()); \
	}
#else
#define DBGOUT(x) \
	{}
#endif
