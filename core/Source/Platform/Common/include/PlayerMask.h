#pragma once

namespace KEP {

namespace PlayerMask {
	// change mask values.  Used to indicate what changed on the player
	// to optimize updates
	const unsigned long ALL_DATA = 0xffffffff;
	const unsigned long USE_DIRTY_FLAGS = 0x00000000; // passed into UpdatePlayer to indicate to use player->m_dbDirtyFlags
	const unsigned long STATS = 0x00000001;
	const unsigned long SKILLS = 0x00000002;
	const unsigned long QUESTS = 0x00000004;
	const unsigned long CLAN = 0x00000008;
	const unsigned long HOUSING = 0x00000010;
	const unsigned long PLAYER = 0x00000020; // base player object
	const unsigned long INVENTORY = 0x00000040;
	const unsigned long BANK_INVENTORY = 0x00000080;
	const unsigned long GETSETS = 0x00000100; // dynamically added GetSet values
	const unsigned long NOTERIETIES = 0x00000200;
	const unsigned long DEFCONFIG = 0x00000400;
	const unsigned long POSITION = 0x00000800;
	const unsigned long RESPAWN_POINT = 0x00001000;
	const unsigned long SPAWN_INFO = 0x00002000;
	const unsigned long PLAYER_HOUSE_INVENTORY = 0x00004000;
	const unsigned long CLAN_HOUSE_INVENTORY = 0x00008000;
	const unsigned long PLAYER_EDB = 0x00010000; // changing base player info
	const unsigned long PASS_LIST = 0x00020000;
	const unsigned long TITLES = 0x00040000;
	const unsigned long CASH = 0x00080000;
	const unsigned long BANK_CASH = 0x00100000;
}

} // namespace KEP