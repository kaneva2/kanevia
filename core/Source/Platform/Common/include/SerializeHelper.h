/******************************************************************************
 SerializeHelper.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <log4cplus/Logger.h>
using namespace log4cplus;
#include <GetSetStrings.h>
#include <KEPHelpers.h>

// helpers to avoid a bunch of typing, or change of impl for serialize errors
// Disable C4456: "declaration of 'serializeOk' hides previous local declaration"
// because this construct is used in nested contexts in some places.
#define BEGIN_SERIALIZE_TRY                                    \
	__pragma(warning(disable : 4456)) bool serializeOk = true; \
	try {
inline void Log_Exception(CException& e, const char* file, int line, Logger* logger) {
	wchar_t buff[1024];
	e.GetErrorMessage(buff, 1024);
	TRACE("SerializeException in %s at %d: %s", file, line, buff);
	if (logger)
		LOG4CPLUS_WARN((*logger), loadStrPrintf(IDS_SERIALIZE_EXECEPTION_LOG, file, line, buff));
}

#define LOG_EXCEPTION(e, logger) Log_Exception(e, __FILE__, __LINE__, &logger);

#define LOG_EXCEPTION_NO_LOGGER(e)                          \
	{                                                       \
		Logger myLogger(Logger::getInstance(L"Exception")); \
		Log_Exception(e, __FILE__, __LINE__, &myLogger);    \
	}

#define END_SERIALIZE_TRY_NO_LOGGER                         \
	}                                                       \
	catch (CException * e) {                                \
		ASSERT(FALSE);                                      \
		Logger myLogger(Logger::getInstance(L"Exception")); \
		Log_Exception(*e, __FILE__, __LINE__, &myLogger);   \
		e->Delete();                                        \
		serializeOk = false;                                \
	}

#define END_SERIALIZE_TRY(logger)                       \
	}                                                   \
	catch (CException * e) {                            \
		ASSERT(FALSE);                                  \
		Log_Exception(*e, __FILE__, __LINE__, &logger); \
		e->Delete();                                    \
		serializeOk = false;                            \
	}

inline BOOL TryDeserialize(CFile* pFile, CObject** pObjToLoad, CRuntimeClass* pClassType) {
	//try to deserilize a file to a object, return true only if serialization succeeded and the
	//type of deserialized object match the type passed in
	BOOL retVal = FALSE;
	BEGIN_SERIALIZE_TRY
	CArchive the_in_Archive(pFile, CArchive::load);
	the_in_Archive >> *pObjToLoad;
	the_in_Archive.Close();
	pFile->Close();
	if ((*pObjToLoad)->IsKindOf(pClassType)) {
		retVal = TRUE;
	} else {
		retVal = FALSE;
	}
	END_SERIALIZE_TRY_NO_LOGGER

	return retVal;
}

inline BOOL TryDeserializeAndLog(CFile* pFile, CObject** pObjToLoad, CRuntimeClass* pClassType, Logger* pLogger) {
	//try to deserilize a file to a object, return true only if serialization succeeded and the
	//type of deserialized object match the type passed in
	BOOL retVal = FALSE;
	BEGIN_SERIALIZE_TRY
	CArchive the_in_Archive(pFile, CArchive::load);
	the_in_Archive >> *pObjToLoad;
	the_in_Archive.Close();
	pFile->Close();
	if ((*pObjToLoad)->IsKindOf(pClassType)) {
		retVal = TRUE;
	} else {
		retVal = FALSE;
	}
	END_SERIALIZE_TRY_NO_LOGGER

	return retVal;
}

