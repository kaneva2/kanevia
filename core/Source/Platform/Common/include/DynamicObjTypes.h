///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

enum DYN_OBJ_TYPE {
	INVALID, ///< invalid object
	NORMAL, ///< normal object
	MEDIA_PLAYER, ///< media player object
	MEDIA_FRAME, ///< media frame object
	MEDIA_GAME, ///< media game object
	DYN_OBJ_TYPES
};

inline std::string DynObjTypeStr(const DYN_OBJ_TYPE& doType) {
	const std::string typeStr[] = {
		"DO_INVALID",
		"DO",
		"DO_MP",
		"DO_MF",
		"DO_MG"
	};
	return (doType < DYN_OBJ_TYPES) ? typeStr[doType] : "DO_TYPE_INVALID";
}
