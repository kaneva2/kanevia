///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "Common/include/KEPMacros.h"
#include <string>

namespace KEP {

class IKEPConfig {
public:
	
	// Abstract Interface
	virtual bool Print() = 0;
	virtual bool New(const char* rootNodeName, const char* rootAttribute = 0, const char* rootAttributeValue = 0) = 0;
	virtual bool Parse(const char* buffer, const char* rootNodeName, const char* rootAttribute = 0, const char* rootAttributeValue = 0) = 0;
	virtual bool Load(const std::string& fname, const char* rootNodeName, const char* rootAttribute = 0, const char* rootAttributeValue = 0) = 0;
	virtual bool Save() = 0;
	virtual bool SaveAs(const std::string& fname) = 0;
	virtual bool IsDirty() const = 0;
	virtual bool SetDirty(bool d) = 0;
	virtual bool IsLoaded() const = 0;
	virtual bool ValueExists(const char* valueName) const = 0;
	virtual bool GetValue(const char* valueName, int& ret, int defaultValue = 0) const = 0;
	virtual bool GetValue(const char* valueName, std::string& ret, const char* defaultValue = "") const = 0;
	virtual bool GetValue(const char* valueName, bool& ret, bool defaultValue = false) const = 0;
	virtual bool GetValue(const char* valueName, double& ret, double defaultValue = 0.0) const = 0;
	virtual bool SetValue(const char* valueName, int value, bool append = false) = 0;
	virtual bool SetValue(const char* valueName, const char* value, bool append = false) = 0;
	virtual bool SetValue(const char* valueName, bool value, bool append = false) = 0;
	virtual bool SetValue(const char* valueName, double value, bool append = false) = 0;

	int Get(const char* valueName, int defaultValue = 0) const {
		int ret;
		GetValue(valueName, ret, defaultValue);
		return ret;
	}

	std::string Get(const char* valueName, const char* defaultValue = "") const {
		std::string ret;
		GetValue(valueName, ret, defaultValue);
		return ret;
	}
	
	bool Get(const char* valueName, bool defaultValue = false) const {
		bool ret;
		GetValue(valueName, ret, defaultValue);
		return ret;
	}
	
	double Get(const char* valueName, double defaultValue = 0.0) const {
		double ret;
		GetValue(valueName, ret, defaultValue);
		return ret;
	}

	virtual ~IKEPConfig(){};
};

} // namespace KEP

