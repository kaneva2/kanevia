/******************************************************************************
 tinyxmlUtil.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

#include <TinyXML/tinyxml.h>

inline bool
GetIntFromXMLNode(TiXmlNode *node, int &val) {
	if (node) {
		TiXmlNode *first_child = node->FirstChild();
		if (first_child) {
			std::string text = first_child->Value();
			val = atoi(text.c_str());
			return true;
		}
	}

	return false;
}

inline bool
PutIntToXMLNode(TiXmlNode *node, const int val) {
	if (node) {
		std::string text = std::to_string(val);

		TiXmlNode *child = node->InsertEndChild(TiXmlText(text.c_str()));
		if (child)
			return true;
	}

	return false;
}

inline bool
GetLongFromXMLNode(TiXmlNode *node, long &val) {
	if (node) {
		TiXmlNode *first_child = node->FirstChild();
		if (first_child) {
			std::string text = first_child->Value();
			val = strtol(text.c_str(), nullptr, 10);
			return true;
		}
	}

	return false;
}

inline bool
PutLongToXMLNode(TiXmlNode *node, const long val) {
	if (node) {
		std::string text = std::to_string(val);

		TiXmlNode *child = node->InsertEndChild(TiXmlText(text.c_str()));
		if (child)
			return true;
	}

	return false;
}

inline bool
GetBoolFromXMLNode(TiXmlNode *node, bool &val) {
	if (node) {
		TiXmlNode *first_child = node->FirstChild();
		if (first_child) {
			const char *pText = first_child->Value();
			val = (!_stricmp(pText, "true") | !_stricmp(pText, "1"));
			return true;
		}
	}

	return false;
}

inline bool
PutBoolToXMLNode(TiXmlNode *node, const bool val) {
	if (node) {
		TiXmlNode *child = node->InsertEndChild(TiXmlText(val ? "true" : "false"));
		if (child)
			return true;
	}

	return false;
}

inline bool
GetFloatFromXMLNode(TiXmlNode *node, float &val) {
	if (node) {
		TiXmlNode *first_child = node->FirstChild();
		if (first_child) {
			std::string text = first_child->Value();
			val = strtof(text.c_str(), nullptr);
			return true;
		}
	}

	return false;
}

inline bool
PutFloatToXMLNode(TiXmlNode *node, const float val) {
	if (node) {
		std::string text = std::to_string(val);

		TiXmlNode *child = node->InsertEndChild(TiXmlText(text.c_str()));
		if (child)
			return true;
	}

	return false;
}

inline bool
GetStringFromXMLNode(TiXmlNode *node, std::string &val) {
	if (node) {
		TiXmlNode *first_child = node->FirstChild();
		if (first_child) {
			val = first_child->Value();
			return true;
		}
	}

	return false;
}

inline bool
PutStringToXMLNode(TiXmlNode *node, const std::string val) {
	if (node) {
		TiXmlNode *child = node->InsertEndChild(TiXmlText(val.c_str()));
		if (child)
			return true;
	}

	return false;
}

inline bool
GetDWORDFromXMLNode(TiXmlNode *node, DWORD &val) {
	if (node) {
		TiXmlNode *first_child = node->FirstChild();
		if (first_child) {
			std::string text = first_child->Value();

			char *stopstring;
			val = strtoul(text.c_str(), &stopstring, 16);
			return true;
		}
	}

	return false;
}

inline bool
PutDWORDToXMLNode(TiXmlNode *node, const DWORD val) {
	if (node) {
		char str[20];
		sprintf_s(str, _countof(str), "0x%08x", val);
		TiXmlNode *child = node->InsertEndChild(TiXmlText(str));
		if (child)
			return true;
	}

	return false;
}

} // namespace KEP

