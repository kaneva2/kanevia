///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

//***********************************************************************
//
//      Based on Dr. Dobbs article "Counting Objects in C++" April 01, 1998 by Scott Meyers
//      http://www.drdobbs.com/cpp/counting-objects-in-c/184403484
//
//***********************************************************************
#pragma once
#include <atomic>

namespace KEP {

template <typename T>
class ObjectCounter {
public:
	ObjectCounter() { ++count; }
	ObjectCounter(const ObjectCounter&) { ++count; }
	~ObjectCounter() { --count; }

	static size_t howMany() { return count.load(); }
	static size_t highWaterMark() { return highWater; }
	static void Update() {
		if (count.load() > highWater)
			++highWater;
	}

private:
	static std::atomic<size_t> count;
	static size_t highWater;
};

template <typename T>
std::atomic<size_t>
	ObjectCounter<T>::count = 0;

template <typename T>
size_t ObjectCounter<T>::highWater = 0;
} // namespace KEP

//***********************************************************************
//
//      Based on Dr. Dobbs article "Counting Objects in C++" April 01, 1998 by Scott Meyers
//      http://www.drdobbs.com/cpp/counting-objects-in-c/184403484
//
//      Example Usage:
//
//      class Widget: private Counter<Widget> {
//      public:
//          // make howMany public
//          using Counter<Widget>::howMany;
//
//          ..... // rest of Widget is unchanged
//      };
//
//      class ABCD: private Counter<ABCD> {
//      public:
//          // make howMany public
//          using Counter<ABCD>::howMany;
//
//          ..... // rest of ABCD is unchanged
//      };
//
//      class SpecialWidget: public Widget,
//          private Counter<SpecialWidget> {
//      public:
//          .....
//          static size_t howMany()
//          { return Counter<SpecialWidget>::howMany(); }
//          .....
//      };
//
//***********************************************************************
