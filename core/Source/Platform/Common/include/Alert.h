/******************************************************************************
 Alert.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <log4cplus/logger.h>
#include <log4cplus/ndc.h>
using namespace log4cplus;

extern Logger alert_logger;

