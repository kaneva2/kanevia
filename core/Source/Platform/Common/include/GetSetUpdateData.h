#pragma once

#include "KEP/Util/KEPUtil.h"
#include "KEPHelpers.h"
#include <memory>

class CWnd;

namespace KEP {
class GetSet;
class gsMetaDatum;

class KEPUTIL_EXPORT GetSetUpdateData {
public:
	enum EContextRet {
		crNoAction,
		crItemAdded,
		crItemDeleted,
		crItemUpdated
	};

	virtual BOOL UpdateData(GetSet * /*item*/, BOOL /*saveAndValidate*/ = true) {
		return TRUE;
	}

	virtual BOOL CustomEdit(GetSet * /*item*/) {
		return FALSE;
	}

	virtual bool SetValue(GetSet * /*item*/, gsMetaDatum & /*md*/, const char * /*value*/, OUT bool &ret) {
		ret = false;
		return false;
	}

	virtual enum EContextRet ContextMenu(GetSet *item, int screenX, int screenY, CWnd *parent);

	virtual ~GetSetUpdateData() {}

protected:
	GetSetUpdateData(GetSet *baseItem) :
			m_item(baseItem) {}

	GetSet *m_item; // the base item we're attached to
};

typedef std::shared_ptr<GetSetUpdateData> GetSetUpdateDataPtr;

} // namespace KEP
