/******************************************************************************
 ListItemMap.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_LIST_ITEM_MAP_INCLUDED_)
#define _LIST_ITEM_MAP_INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListItemMap.h : header file
//

#include <map>

typedef std::map<int, std::string> ListItemMap;
typedef ListItemMap::iterator ListItemMapIterator;

#endif // !defined(_LIST_ITEM_MAP_INCLUDED_)
