/******************************************************************************
 IKEPFile.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEPHelpers.h"

namespace KEP {

class IKEPFile {
public:
	// version of this class interface
	static BYTE Version() { return 1; }

	virtual bool ReadLine(OUT std::string &line, OUT std::string &errMsg) = 0;
	virtual bool WriteLine(IN const char *line, OUT std::string &errMsg) = 0;
	virtual void CloseAndDelete() = 0;
};

} // namespace KEP
