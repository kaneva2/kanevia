/******************************************************************************
 gsRgbaValue.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEP/Util/KEPUtil.h"
namespace KEP {

/***********************************************************
CLASS

	gsRgbaValue
	
	helper class for three or four floats that make a color

DESCRIPTION

***********************************************************/
class KEPUTIL_EXPORT gsRgbaValue {
public:
	gsRgbaValue(float *_r, float *_g, float *_b, float *_a) :
			r(_r), g(_g), b(_b), a(_a) {}

	float *r;
	float *g;
	float *b;
	float *a;

	COLORREF getColorRef() const { return D3DCOLOR_COLORVALUE(r ? *r : 0, g ? *g : 0, b ? *b : 0, a ? *a : 0); }
	void setColorRef(COLORREF c) {
		if (r)
			*r = GetRValue(c) / 255.0f;
		if (g)
			*g = GetGValue(c) / 255.0f;
		if (b)
			*b = GetBValue(c) / 255.0f;
		if (a)
			*a = (c >> 24) / 255.0f; // DX puts it here, but no RGB macro to get it
	}
};

} // namespace KEP

