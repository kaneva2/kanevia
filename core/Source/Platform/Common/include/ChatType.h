#pragma once

namespace KEP {

#define SAY_TEXT " says > "
#define TELL_TEXT " tells you > "

// types of chat, in this order for backward compatability
enum class eChatType : int {
	Talk, // 0
	Group, // 1
	Clan, // 2
	Private, // just to you from one other, direct tell message
	Whisper, // talk w/ limited radius
	Shout, // talk w/ large radius
	GM, // from GameMaster
	System, // from System Admin, status, system going down etc.
	External, // from an external source, IM, e-mail, etc.
	ArenaMgr, // from arena status, etc
	Team, // restrict to same team
	Race, // restrict to same race
	Battle, // battle related, etc. damage
	Heal, // healing messages
	Friend, // friend messages
	S2S, // server to server message
	Emote, //16, emote message type for players
	ScriptObject //from object controlled by a script
};

} // namespace KEP
