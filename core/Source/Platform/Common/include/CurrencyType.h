#pragma once

namespace KEP {

enum class CurrencyType {
	// these map enum in kaneva database, except Bank, which isn't supported in WOK
	GIFT_CREDITS_ON_PLAYER = 1,
	CREDITS_ON_PLAYER = 2,
	GIFT_CREDITS_IN_BANK = 5,
	CREDITS_IN_BANK = 6,
};

} // namespace KEP