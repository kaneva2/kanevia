///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define NETCFG_CFG "Network.cfg"
#define NETCFG_ROOT "NetworkConfig"

static const char* const GETPLAYERURL_TAG = "GetPlayerUrl";
static const char* const GETITEMSURL_TAG = "GetItemsUrl";
static const char* const GETPLAYLISTURL_TAG = "GetPlaylistUrl";
static const char* const SPACEIMPORTURL_TAG = "SpaceImportUrl";
static const char* const SERVERCONFIGURL_TAG = "ConfigUrl";
static const char* const AUTHSERVERURL_TAG = "AuthServerUrl";
static const char* const PINGSERVERURL_TAG = "PingServerUrl";
static const char* const PINGINTERVAL_TAG = "PingIntervalMin";
static const char* const DISABLENATTRAVERSAL_TAG = "DisableNATTraversal";
static const char* const INVENTORYUPDATER_TAG = "InventoryUpdaterUrl";
static const char* const MEMCACHEAPI_TAG = "MemCacheAPIUrl";
static const char* const WORLDEXPERIMENT_TAG = "WorldExperimentUrl";

static const char* const CLIENTDEFAULTLOGINURL_TAG = "LoginUrl";
static const char* const CLIENTDEFAULT_RUNTIME_REPORT_URL_TAG = "RuntimeReportUrl";
static const char* const CLIENTDEFAULTSTARTAPPURL_TAG = "StartAppUrl";
static const char* const CLIENTDEFAULTAUTHURL_TAG = "AuthUrl";
static const char* const ALLOWACCESSURL_TAG = "AllowAccessUrl";
static const char* const CLIENTDEFAULTIMAGEURL_TAG = "ImageUrl";
static const char* const CLIENTDEFAULTIMAGETHUMBURL_TAG = "ImageThumbUrl";
static const char* const CLIENTDEFAULTFRIENDURL_TAG = "FriendUrl";
static const char* const CLIENTDEFAULTFRIENDTHUMBURL_TAG = "FriendThumbUrl";
static const char* const CLIENTDEFAULTGIFTURL_TAG = "GiftUrl";
static const char* const CLIENTDEFAULTGIFTTHUMBURL_TAG = "GiftThumbUrl";
static const char* const WOKURL_TAG = "WOKUrl";
static const char* const SECUREWOKURL_TAG = "SecureWOKUrl";
static const char* const KANEVAURL_TAG = "KanevaUrl";
static const char* const SHOPURL_TAG = "ShopUrl";
static const char* const CLIENTDEFAULTIMAGESERVER_TAG = "ImageServerUrl";
static const char* const UNIQUETEXTURESERVERURL_TAG = "UniqueTextureServerUrl";
static const char* const ITEM_ANIMATIONS_URL_TAG = "ItemAnimationsUrl";
static const char* const CLIENTDEFAULTINVENTORYSERVER_TAG = "InventoryServerUrl";
static const char* const ZONECUSTOM_TAG = "ZoneCustomizationUrl";
static const char* const ERROR_URL_TAG = "ErrorReportUrl";
static const char* const PRODSERVERLISTURL_TAG = "ProdServerListUrl";
static const char* const SERVERLISTURL_TAG = "ServerListUrl";
static const char* const CONTENTMETADATAURL_TAG = "ContentMetadataUrl";
static const char* const GAMEINFOURL_TAG = "GameInfoUrl";
static const char* const CHILDGAMEWEBCALLS_URL_TAG = "ChildGameWebCallValues";
static const char* const FEEDBACKDIAGURL_TAG = "DxDiagUrl";
static const char* const SCRIPTURL_TAG = "ScriptUrl";

static const char* const DOWNLOADTHREAD_TAG = "DownloadThreads";
static const char* const DOWNLOADTHREADPRIO_TAG = "DownloadPriority";
static const char* const PING_LOCATION_TAG = "PingLocation";

static const char* const PUBLISH_LOGIN_TAG = "PubLoginUrl";
static const char* const PUBLISH_GETGAMES_TAG = "PubGetGamesUrl";
static const char* const LOST_PASSWORD_TAG = "LostPassword";
static const char* const CREATEACCT_TAG = "CreateAcctUrl";
static const char* const COUNTRY_URL_TAG = "CountryUrl";
static const char* const TERMS_TAG = "TermsAgreement";
static const char* const PRIVACY_TAG = "PrivacyAgreement";

// Configuration Enumeration
#define NET_CFG_OLD_K 0
#define NET_CFG_NEW_K 1
#define NET_CFG NET_CFG_NEW_K

#if (NET_CFG == NET_CFG_OLD_K)
	#define NET_DOMAIN "kaneva.com"
	#define NET_DOMAIN_IP 0x4221d5cc // Kaneva Original - 66.33.213.204
	#define NET_PATCH_URL "http://patch.kaneva.com.s3.amazonaws.com/patchdata/"
	#define NET_PATCH_VERSION "16.9.0.71_a"
#elif (NET_CFG == NET_CFG_NEW_K)
	#define NET_DOMAIN "kanevia.com"
	#define NET_DOMAIN_IP 0x28587c99 // Daryl's Azure - 40.88.124.153
	#define NET_PATCH_URL "https://wokzpatch.blob.core.windows.net/patchdata/"
	#define NET_PATCH_VERSION "alpha-0.0.1a"
	#define NET_WOK_NO_SECURITY 0 // HACK - Disable https for wok calls
	#define NET_KGP_NO_SECURITY 0 // HACK - Disable https for kgp calls
	#define NET_AUTH_WEBCALL_DISABLED 0 // HACK - Disable auth webcall (validateUser)
#endif

#define NET_ENV_PRO ""
#define NET_ENV_PRE "pv-"
#define NET_ENV_DEV "dev-"

#define NET_STAR_PRO "3296"
#define NET_STAR_PRE "3298"
#define NET_STAR_DEV "5316"

#define NET_ENV NET_ENV_PRO

#define NET_SUB(sub) sub "." NET_DOMAIN

#define NET_HTTP(sub) "http://" NET_SUB(sub) "/"
#define NET_HTTPS(sub) "https://" NET_SUB(sub) "/"

#if (NET_CFG == NET_CFG_OLD_K)

#define NET_WOK_ENV(env) NET_HTTP(env "wok")
#define NET_WOK_SECURE_ENV(env) NET_HTTPS(env "wok")
#define NET_KGP_ENV(env) \
	NET_WOK_ENV(env)     \
	"kgp/"
#define NET_KGP_SECURE_ENV(env) \
	NET_WOK_SECURE_ENV(env)     \
	"kgp/"
#define NET_AUTH_ENV(env) \
	NET_HTTPS(env "auth") \
	"kepauth/"
#define NET_PING_ENV(env) \
	NET_HTTPS(env "ping") \
	"kepping/"
#define NET_PATCH_ENV(env) \
	NET_HTTP(env "patch")  \
	"patchdata/"
#define NET_IMAGES_ENV(env) NET_HTTP(env "images")
#define NET_TEXTURES_ENV(env) NET_HTTP(env "textures")
#define NET_FORUM_ENV(env) NET_HTTP(env "forums")
#define NET_SUPPORT_ENV(env) NET_HTTP(env "support")
#define NET_2D_ENV(env) NET_HTTP(env "www")
#define NET_2D_SECURE_ENV(env) NET_HTTPS(env "www")
#define NET_SHOP_ENV(env) NET_HTTP(env "shop")
#define NET_SCRIPTS_ENV(env) NET_HTTP(env "scripts")
#define NET_PUBLISH_ENV(env) \
	NET_HTTP(env "publish")  \
	"publishingServer/"
#define NET_PUBLISH_SECURE_ENV(env) \
	NET_HTTPS(env "publish")        \
	"publishingServer/"
#define NET_STREAMING_ENV(env) NET_HTTP(env "streaming")
#define NET_MQ_ENV(env) NET_SUB(env "mq")
#define NET_CRASH_ENV(env) \
	NET_HTTP(env "basher") \
	"crashfix/index.php/crashReport/uploadExternal"
#define NET_RUNTIME_ENV(env) \
	NET_KGP_ENV(env)         \
	"runtimeReport.aspx?"

#elif (NET_CFG == NET_CFG_NEW_K)
	
#define NET_WOK_ENV(env) \
	NET_HTTP("wok")
#define NET_WOK_SECURE_ENV(env) \
	NET_HTTPS("wok")         
#define NET_KGP_ENV(env) \
	NET_WOK_ENV(env)     \
	"kgp/"
#define NET_KGP_SECURE_ENV(env) \
	NET_WOK_SECURE_ENV(env)     \
	"kgp/"
#define NET_AUTH_ENV(env)   \
	NET_WOK_SECURE_ENV(env) \
	"auth/"
#define NET_PING_ENV(env)   \
	NET_WOK_SECURE_ENV(env) \
	"ping/"
#define NET_PATCH_ENV(env) \
	NET_WOK_ENV(env)       \
	"patch/"
#define NET_IMAGES_ENV(env) \
	NET_WOK_ENV(env)        \
	"images/"
#define NET_TEXTURES_ENV(env) \
	NET_WOK_ENV(env)          \
	"textures/"
#define NET_FORUM_ENV(env) \
	NET_WOK_ENV(env)       \
	"forums/"
#define NET_SUPPORT_ENV(env) \
	NET_WOK_ENV(env)         \
	"support/"
#define NET_2D_ENV(env) \
	NET_WOK_ENV(env)    \
	"2d/"
#define NET_2D_SECURE_ENV(env) \
	NET_WOK_SECURE_ENV(env)    \
	"2d/"
#define NET_SHOP_ENV(env) \
	NET_WOK_ENV(env)      \
	"shop/"
#define NET_SCRIPTS_ENV(env) \
	NET_WOK_ENV(env)         \
	"scripts/"
#define NET_PUBLISH_ENV(env) \
	NET_WOK_ENV(env)         \
	"publish/"
#define NET_PUBLISH_SECURE_ENV(env) \
	NET_WOK_SECURE_ENV(env)         \
	"publish/"
#define NET_STREAMING_ENV(env) \
	NET_WOK_ENV(env)           \
	"streaming/"
#define NET_MQ_ENV(env) \
	NET_WOK_ENV(env)    \
	"mq/"
#define NET_CRASH_ENV(env) \
	NET_WOK_ENV(env)       \
	"crashfix/index.php/crashReport/uploadExternal"
#define NET_RUNTIME_ENV(env) \
	NET_WOK_ENV(env)         \
	"runtimeReport.aspx?"

#endif

// Default Environment
#define NET_WOK NET_WOK_ENV(NET_ENV)
#if (NET_WOK_NO_SECURITY == 1)
#define NET_WOK_SECURE NET_WOK_ENV(NET_ENV)
#else
#define NET_WOK_SECURE NET_WOK_SECURE_ENV(NET_ENV)
#endif
#define NET_KGP NET_KGP_ENV(NET_ENV)
#if (NET_KGP_NO_SECURITY == 1)
#define NET_KGP_SECURE NET_KGP_ENV(NET_ENV)
#else
#define NET_KGP_SECURE NET_KGP_SECURE_ENV(NET_ENV)
#endif
#define NET_AUTH NET_AUTH_ENV(NET_ENV)
#define NET_PING NET_PING_ENV(NET_ENV)
#define NET_PATCH NET_PATCH_ENV(NET_ENV)
#define NET_IMAGES NET_IMAGES_ENV(NET_ENV)
#define NET_TEXTURES NET_TEXTURES_ENV(NET_ENV)
#define NET_FORUM NET_FORUM_ENV(NET_ENV)
#define NET_SUPPORT NET_SUPPORT_ENV(NET_ENV)
#define NET_2D NET_2D_ENV(NET_ENV)
#define NET_2D_SECURE NET_2D_SECURE_ENV(NET_ENV)
#define NET_SHOP NET_SHOP_ENV(NET_ENV)
#define NET_SCRIPTS NET_SCRIPTS_ENV(NET_ENV)
#define NET_PUBLISH NET_PUBLISH_ENV(NET_ENV)
#define NET_PUBLISH_SECURE NET_PUBLISH_SECURE_ENV(NET_ENV)
#define NET_STREAMING NET_STREAMING_ENV(NET_ENV)
#define NET_MQ NET_MQ_ENV(NET_ENV)
#define NET_CRASH NET_CRASH_ENV(NET_ENV)
#define NET_RUNTIME NET_RUNTIME_ENV(NET_ENV)
#define NET_PATCH_VERSION_URL NET_PATCH_URL NET_PATCH_VERSION "/"

// Server
static const char* const NET_DEFAULT_GET_PLAYER_URL = NET_KGP "getplayerinfo.aspx?gzip=true&userId={0}&flags={1}&gid={2}";
static const char* const NET_DEFAULT_GET_ITEMS_URL = NET_KGP "getItems.aspx?gzip=true&id={0}";
static const char* const NET_DEFAULT_GET_PLAYLIST_URL = NET_KGP "playList.aspx?gzip=true&game=T&type={0}&pid={1}&playerId={2}&zoneIndex={3}&zoneInstanceId={4}";
static const char* const NET_DEFAULT_SPACEIMPORT_URL = NET_KGP "getSpaceForImport.aspx?gzip=true&game=T&zoneIndex={0}&instanceId={1}";
static const char* const NET_DEFAULT_MEMCACHAPI_URL = NET_KGP "MemCache.aspx";
static const char* const NET_DEFAULT_INVENTORYUPDATER_URL = NET_KGP "inventoryUpdate.aspx";
static const char* const NET_DEFAULT_WORLDEXPERIMENT_URL = NET_KGP "server/worldExperiment.aspx?action=getActiveExperimentGroups&assignAutomatically=Y";

static const char* const DEFAULT_AUTHSERVER_URL = NET_AUTH "kepauthv6.aspx";
static const char* const DEFAULT_PINGSERVER_URL = NET_PING "keppingv7.aspx";

static const char* const NET_DEFAULT_ASSET_DETAILS_URL = NET_2D "asset/assetDetails.aspx?assetId={0}";

// Dispatcher Client
static const char* const DEFAULT_SERVERCONFIG_URL = NET_KGP "getserverconfig.aspx?gzip=true&type={0}";

// Client
static const char* const DEFAULT_WOK_URL = NET_WOK;
static const char* const DEFAULT_SECURE_WOK_URL = NET_WOK_SECURE;
static const char* const DEFAULT_KANEVA_URL = NET_2D;
static const char* const DEFAULT_SHOP_URL = NET_SHOP;
static const char* const DEFAULT_SCRIPT_URL = NET_SCRIPTS;

static const char* const DEFAULT_IMAGESERVER_URL = NET_IMAGES;
static const char* const DEFAULT_UNIQUE_TEXTURE_SERVER_URL = NET_TEXTURES;

static const char* const DEFAULT_LOGIN_URL = NET_KGP_SECURE "gamelogin.aspx?gzip=true&user={0}&format=xml";
static const char* const DEFAULT_RUNTIME_REPORT_URL = NET_KGP "runtimeReport.aspx?gzip=true&userId={0}&runtime={1}&runs={2}&crashes={3}&clientVersion={4}&testGroup={5}";
static const char* const DEFAULT_START_APP_URL = NET_KGP_SECURE "appHosting.aspx?gzip=true&action=GoToApp&name={0}";
static const char* const DEFAULT_AUTH_URL = NET_KGP_SECURE "validateuser.aspx?gzip=true&user={0}&pw={1}&gid={2}";
static const char* const DEFAULT_ALLOW_ACCESS_URL = NET_KGP "allowAccess.aspx?gzip=true&gid={0}";

static const char* const DEFAULT_IMAGE_URL = NET_KGP "getImageUrl.aspx?gzip=true&assetId={0}";
static const char* const DEFAULT_IMAGE_THUMB_URL = NET_KGP "getImageUrl.aspx?gzip=true&assetId={0}&thumb=1";
static const char* const DEFAULT_FRIEND_URL = NET_KGP "getImageUrl.aspx?gzip=true&userId={0}";
static const char* const DEFAULT_FRIEND_THUMB_URL = NET_KGP "getImageUrl.aspx?gzip=true&userId={0}&thumb=1";

static const char* const DEFAULT_ITEM_ANIMATIONS_URL = NET_KGP "getitemanimations.aspx?gzip=true&id={0}";
static const char* const DEFAULT_INVENTORYSERVER_URL = NET_KGP "inventory.aspx?gzip=true&sort={0}";
static const char* const DEFAULT_ZONECUSTOM_URL = NET_KGP "zoneCustomizations.aspx?gzip=true&zoneIndex={0}&instanceId={1}&v=2&x={2}&y={3}&z={4}";

static const char* const DEFAULT_ERROR_URL = NET_KGP "errorReport.aspx?gzip=true&errorType={0}&description={1}&time={2}&link={3}&errorCode={4}";

static const char* const DEFAULT_GIFT_URL = NET_IMAGES "gifts/{0}.jpg";
static const char* const DEFAULT_GIFT_THUMB_URL = NET_IMAGES "gifts/{0}.jpg";

// this is for prod only and will look up the patch urls for ver=N
// so it can't be used anywhere else since all others WOKs and 3DApps only have patch
// urls for ver=1
#define SERVERLIST_V "&v=2"
#define SERVERLIST_VER "&ver=12"
static const char* const DEFAULT_PROD_SERVERLIST_URL = NET_KGP "serverlist.aspx?gzip=true&id={0}" SERVERLIST_V SERVERLIST_VER;
// this one is for all other games/apps
static const char* const DEFAULT_SERVERLIST_URL = NET_KGP "serverlist.aspx?gzip=true&id={0}" SERVERLIST_V;

static const char* const DEFAULT_CONTENTMETADATA_URL = NET_KGP "contentMetadata.aspx?gzip=true&glid={0}";
static const char* const DEFAULT_GAMEINFO_URL = NET_KGP "gameInfo.aspx?gzip=true&id={0}";
static const char* const DEFAULT_CHILDGAMEWEBCALLS_URL = NET_KGP "childgamewebcallvalues.aspx?gzip=true";

static const char* const DEFAULT_FEEDBACK_DIAG_URL = NET_WOK "kaneva/FeedbackReportServer/ReportUserHardwareV3.asmx";

static const char* const DEFAULT_PING_LOCATION = "wok." NET_DOMAIN "";

// Editor and publishing
static const char* const DEFAULT_PUBLISHING_SERVER_LOGIN_URL = NET_PUBLISH_SECURE "login.aspx";
static const char* const DEFAULT_PUBLISHING_SERVER_GETGAMES_URL = NET_PUBLISH "getGames.aspx";
static const char* const DEFAULT_LOST_PASSWORD = NET_2D "lostPassword.aspx";
static const char* const DEFAULT_TERMS_URL = NET_2D "overview/TermsAndConditions.aspx";
static const char* const DEFAULT_PRIVACY_URL = NET_2D "overview/privacy.aspx";
static const char* const DEFAULT_CREATE_ACCT_URL = NET_2D_SECURE "register/kaneva/RegisterService.aspx";
static const char* const DEFAULT_COUNTRY_URL = NET_2D "register/kaneva/registerHelperService.aspx?action=GetCountries";

// Publishing URLs to be prefixed with WOKUrl or SecureWOKUrl
static const char* const NEW_GAME_URL = "kgp/gameAdd.aspx?gzip=true&name={0}&desc={1}&type={2}&user={3}&pw={4}";
static const char* const NEW_SERVER_URL = "kgp/gameAddServer.aspx?gzip=true&version=2&gameId={0}&hostName={1}&patchUrl={2}&user={3}&pw={4}";

// Publishing game Type
static const char* const FULL_STAR = "2";
static const char* const GAME_STAR = "1";
