///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "KEPConstants.h"

namespace KEP {

struct ScriptPlayerInfo {
	// Selected 'quasi-static' player information from CAccountObject and CPlayerObject
	std::string m_name;
	int m_playerId;
	char m_gender; // M/F/U(nknown)
	int m_age;
	bool m_showMature; // if > 18, do they want to see mature stuff
	int m_permissions; // GM = 0, OWNER = 1, MODERATOR = 2, MEMBER = 3 (i.e. just a normal user)
	std::string m_title;

	ScriptPlayerInfo() :
			m_playerId(0), m_gender(0), m_age(0), m_showMature(false), m_permissions(ZP_NONE) {}

	ScriptPlayerInfo(const std::string& name,
		int playerId,
		char gender,
		int age,
		bool showMature,
		int permissions,
		const std::string& title) :
			m_name(name),
			m_playerId(playerId),
			m_gender(gender),
			m_age(age),
			m_showMature(showMature),
			m_permissions(permissions),
			m_title(title) {}
};

} // namespace KEP
