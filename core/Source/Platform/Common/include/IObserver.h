/******************************************************************************
 IObserver.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

class IObserver {
public:
	virtual void DoUpdate() = 0;
	virtual ~IObserver() {}
};

class ISubject {
public:
	virtual void AddObserver(IObserver *pObserver) = 0;
};

} // namespace KEP