///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

/// Control Type Enumeration
enum class eActorControlType {
	OPC = -1, // other player controlled (other player's avatar)
	PC = 0, // my player controlled (my avatar)
	NPC = 5, // not player controlled (not used)
	VEHICLE = 10, // new physics vehicle (msciabica)
	STATIC = 35 // not player controlled (city vendors, paper dolls, ...)
};
