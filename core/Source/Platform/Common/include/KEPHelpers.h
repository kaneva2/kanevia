///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>
#include <tchar.h>
#include <stdint.h>
#include <sys/types.h>

// STL Includes
#include <set>
#include <map>
#include <list>
#include <vector>
#include <string>
#include <Log4CPlus/Logger.h>
#include "Core/Util/StackTrace.h"
#include "KEP/Util/KEPUtil.h"
#include "KEPCommon.h"
#include "SDKCommon.h"
#include "Core/Util/Unicode.h"

#include "js.h"
#undef min
#undef max

// easier to compile this in each file
extern HINSTANCE StringResource;

///---------------------------------------------------------
// load a string from the resource file
//
// [in] id id of string to load
// [out] loaded return value
// [in] hinst optional instance where to load it from
//
// [Returns]
//    the string, or "String X not found in resource file"
inline std::string loadStr(UINT id, bool* loaded = 0, HINSTANCE hinst = 0) {
	// TODO: avoid big chunk on stack, use TLS?
	const int BUF_SIZE = 256;
	wchar_t buffer[BUF_SIZE];

	if (hinst == 0) {
		if (StringResource == 0) {
#ifdef _AFX
			CWinApp* papp = ::AfxGetApp();
			if (papp != 0)
				::InterlockedExchangePointer((void**)&StringResource, (void*)papp->m_hInstance);
			else
				::InterlockedExchangePointer((void**)&StringResource, GetModuleHandle(0));
#else
			hinst = ::GetModuleHandle(0);
			if (hinst == 0) {
				if (loaded != 0)
					*loaded = false;
				StackTrace st;
				LOG4CPLUS_ERROR(log4cplus::Logger::getInstance(L"KEPHelpers"), "loadStr() failed!");
				LOG4CPLUS_ERROR(log4cplus::Logger::getInstance(L"KEPHelpers"), st.str().c_str());
				return std::string("???");
			}
#endif
		} else {
			hinst = StringResource;
		}
	}

	if (hinst == 0) {
		if (loaded != 0)
			*loaded = false;
		return std::string("???");
	}

	if (::LoadStringW(hinst, id, buffer, BUF_SIZE) == 0) {
		if (loaded)
			*loaded = false;
		::_snwprintf_s(buffer, _countof(buffer), _TRUNCATE, L"String %d not found in resource file", id);
	} else if (loaded) {
		*loaded = true;
	}

	return KEP::Utf16ToUtf8(buffer);
}

// DRF - typecasted loadStr()
inline std::string loadStdStr(UINT id) {
	return loadStr(id, 0, 0);
}

///---------------------------------------------------------
// helper for logging to convert a number to a string, if
// you need it in a different format
//
// [in] l the number
// [in] format the printf format to print, must not exceed 33 bytes
//
// [Returns]
//		the formatted string
template <class T>
std::string numToString(T l, const char* format = "0x%x") {
	char s[34];
	s[33] = _T('\0');
	_snprintf_s(s, _countof(s), _TRUNCATE, format, l);
	return s;
}

///---------------------------------------------------------
// convert an error number to a meaningful string for
// OS-type error messages
//
// [in] errNo the error number
// [out] msg the resulting message or "Error number 0x%x"
//
// [Returns]
//		msg.c_str(), for convenience
const wchar_t* errorNumToString(ULONG errNo, std::wstring& msg);
const char* errorNumToString(ULONG errNo, std::string& msg);

// DRF typecast HRESULT -> ULONG
#define HRerrorNumToString(errNo, msg) errorNumToString((ULONG)errNo, msg)

///---------------------------------------------------------
// load a string from the resource file
//
// [in] id id of string to load
// [in] ... extra stuff to sprintf into it
//
// [Returns]
//    the string, or "String X not found in resource file"
inline std::string loadStrPrintf(UINT id, ...) {
	// TODO: avoid big chunk on stack, use TLS?
	static const int BUF_SIZE = 4096;
	char buffer[BUF_SIZE];
	buffer[BUF_SIZE - 1] = _T('\0');

	std::string s = loadStr(id);
	va_list args;
	va_start(args, id);

	_vsnprintf_s(buffer, _countof(buffer), _TRUNCATE, s.c_str(), args);

	va_end(args);

	return std::string(buffer);
}

// little helper to avoid assigning null, w/o evaluating t twice in macro
inline const char* NULL_CK(const char* t) {
	if (t == 0)
		return "";
	else
		return t;
}

inline void STLTokenize(const std::string& str,
	std::vector<std::string>& tokens,
	const std::string& delimiters = " ") {
	// Skip delimiters at beginning.
	auto lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	auto pos = str.find_first_of(delimiters, lastPos);

	while (std::string::npos != pos || std::string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}
}

#include <locale>
inline void STLToLower(std::string& cmd) {
	std::locale loc = std::locale::classic();
	// tolower
	for (size_t j = 0; j < cmd.length(); j++) {
		cmd[j] = tolower(cmd[j], loc);
	}
}

inline void STLToUpper(std::string& cmd) {
	std::locale loc = std::locale::classic();
	// tolower
	for (size_t j = 0; j < cmd.length(); j++) {
		cmd[j] = toupper(cmd[j], loc);
	}
}

inline int STLCompareIgnoreCase(const char* _l, const char* _r) {
	std::string l = NULL_CK(_l);
	std::string r = NULL_CK(_r);
	STLToLower(l);
	STLToLower(r);
	return l.compare(r);
}

inline int STLCompareIgnoreCase(const std::string& _l, const std::string& _r) {
	return STLCompareIgnoreCase(_l.c_str(), _r.c_str());
}

inline bool STLContains(const char* _l, const char* _r) {
	std::string l = NULL_CK(_l);
	std::string r = NULL_CK(_r);
	return (l.find(r) != std::string::npos);
}

inline bool STLContains(const std::string& _l, const std::string& _r) {
	return STLContains(_l.c_str(), _r.c_str());
}

inline bool STLContainsIgnoreCase(const char* _l, const char* _r) {
	std::string l = NULL_CK(_l);
	std::string r = NULL_CK(_r);
	STLToLower(l);
	STLToLower(r);
	return (l.find(r) != std::string::npos);
}

inline bool STLContainsIgnoreCase(const std::string& _l, const std::string& _r) {
	return STLContainsIgnoreCase(_l.c_str(), _r.c_str());
}

inline bool STLBeginWith(const std::string& aString, const std::string& aToken) {
	size_t tokenLen = aToken.length();
	size_t strLen = aString.length();
	if (strLen < tokenLen)
		return false;

	//	return aString.substr(0, tokenLen).compare(aToken)==0;
	return (0 == aString.compare(0, tokenLen, aToken));
}

inline bool STLEndWith(const std::string& aString, const std::string& aToken) {
	size_t tokenLen = aToken.length();
	size_t strLen = aString.length();
	if (strLen < tokenLen)
		return false;

	//	return aString.substr(strLen-tokenLen, tokenLen).compare(aToken)==0;
	return (0 == aString.compare(strLen - tokenLen, tokenLen, aToken));
}

inline bool STLBeginWithIgnoreCase(const std::string& aString, const std::string& aToken) {
	size_t tokenLen = aToken.length();
	if (aString.length() < tokenLen)
		return false;

	return STLCompareIgnoreCase(aString.substr(0, tokenLen), aToken) == 0;
}

inline bool STLEndWithIgnoreCase(const std::string& aString, const std::string& aToken) {
	size_t tokenLen = aToken.length();
	size_t strLen = aString.length();
	if (strLen < tokenLen)
		return false;

	return STLCompareIgnoreCase(aString.substr(strLen - tokenLen, tokenLen), aToken) == 0;
}

// checks if a aFilename has aExtension at the end
// assumes aExtension is already in lowercase
inline bool STLHasExtension(const std::string& aFileName, const std::string& aExtension) {
	std::string fileName = aFileName;
	STLToLower(fileName);
	size_t extbegin = 0;
	if (((extbegin = fileName.rfind(std::string(".") + aExtension)) != std::string::npos) && ((fileName.size() - extbegin) == (aExtension.size() + 1)))
		return true;
	return false;
}

inline void STLTrimLeft(std::string& str, const std::string& chars = " \t") {
	size_t pos = str.find_first_not_of(chars);
	str.erase(0, pos);
}

inline void STLTrimRight(std::string& str, const std::string& chars = " \t") {
	size_t pos = str.find_last_not_of(chars);
	if (pos != std::string::npos) {
		str.erase(pos + 1);
	}
}

inline void STLTrim(std::string& str, const std::string& chars = " \t") {
	STLTrimLeft(str, chars);
	STLTrimRight(str, chars);
}

#define CharIsSlash(c) ((c) == '/' || (c) == '\\')
#define StrBeginsWithSlash(str) (!str.empty() && CharIsSlash(*str.begin()))
#define StrEndsWithSlash(str) (!str.empty() && CharIsSlash(*str.rbegin()))
#define StrBeginsWith(str, c) (!str.empty() && (c) == *str.begin())
#define StrEndsWith(str, c) (!str.empty() && (c) == *str.rbegin())

/** DRF - DEPRECATED - Use PathAdd() Instead
 * This function appends path2 to path1 in-place of path1, adding a slash 
 * separator if necessary.  Legacy behavior dictates the special case that 
 * given an empty path2 the slash is still appended to path1.  This behavior
 * is inconsistent with the theme of appending paths and should eventually 
 * be removed. A pointer to path1's appended c-style string is returned.
 */
inline const char* PathAddInPlace(std::string& path1, const char* path2) {
	std::string path2Copy = path2 ? path2 : "";
	if (!path1.empty()) {
		const char* slash = (STLContains(path1, "/") || STLContains(path2Copy, "/")) ? "/" : "\\";
		if (path2Copy.empty()) {
			if (!StrEndsWithSlash(path1))
				path1 += slash; // TODO remove special case
			return path1.c_str();
		}
		if (StrBeginsWithSlash(path2Copy))
			path2Copy.erase(0, 1);
		if (!StrEndsWithSlash(path1))
			path1 += slash;
	}
	path1 += path2Copy;
	return path1.c_str();
}

/** DRF
 * This function appends path2 to path1 and returns the appended path.
 */
inline std::string PathAdd(const char* path1, const char* path2) {
	std::string path1Copy = path1 ? path1 : "";
	return PathAddInPlace(path1Copy, path2);
}

/** DRF
 * This function appends path2 to path1 and returns the appended path.
 */
inline std::string PathAdd(const std::string& path1, const std::string& path2) {
	std::string path1Copy = path1;
	return PathAddInPlace(path1Copy, path2.c_str());
}

inline std::wstring PathAddW(const std::wstring& path1, const std::wstring& path2) {
	return KEP::Utf8ToUtf16(PathAdd(KEP::Utf16ToUtf8(path1), KEP::Utf16ToUtf8(path2)));
}

inline std::wstring PathAddW(const wchar_t* path1, const wchar_t* path2) {
	return KEP::Utf8ToUtf16(PathAdd(KEP::Utf16ToUtf8(path1), KEP::Utf16ToUtf8(path2)));
}

#ifdef _AFX
inline CStringW GetExceptionErrorMessage(CException* pException) {
	CStringW strMessage;
	try {
		wchar_t buf[256];
		pException->GetErrorMessage(buf, _countof(buf));
		strMessage = buf;
	} catch (...) {
		// If the exception was due to out of memory, constructing the message may throw another exception.
		// Return an empty message in this case.
	}
	return strMessage;
}
#endif

typedef struct {
	const char* replacement;
	int replacementLen;
	char charToMap;
} XmlMapping;

inline void unEscapeXml(std::string& s) {
	static XmlMapping xmlMapping[] = {
		{ "&amp;", 5, '&' },
		{ "&lt;", 4, '<' },
		{ "&gt;", 4, '>' },
		{ "&quot;", 6, '\"' },
		{ "&apos;", 6, '\'' }
	};
	static int xmlMappingCnt = sizeof(xmlMapping) / sizeof XmlMapping;

	for (int i = 0; i < xmlMappingCnt; i++) {
		std::string::size_type j = 0;
		std::string::size_type found = std::string::npos;
		while ((found = s.find(xmlMapping[i].replacement, j)) != std::string::npos) {
			char ss[2] = { xmlMapping[i].charToMap, '\0' };
			s.replace(found, strlen(xmlMapping[i].replacement), ss);
			j = found + 1;
		}
	}
}

// convert non-XML chars, per table
inline void escapeXml(std::string& s) {
	static XmlMapping xmlMapping[] = {
		{ "&amp;", 5, '&' },
		{ "&lt;", 4, '<' },
		{ "&gt;", 4, '>' },
		{ "&quot;", 6, '\"' },
		{ "&apos;", 6, '\'' }
	};
	static int xmlMappingCnt = sizeof(xmlMapping) / sizeof XmlMapping;

	for (int i = 0; i < xmlMappingCnt; i++) {
		std::string::size_type j = 0;
		std::string::size_type found = std::string::npos;
		while ((found = s.find(xmlMapping[i].charToMap, j)) != std::string::npos) {
			s.replace(found, 1, xmlMapping[i].replacement);
			j = found + 1;
		}
	}
}

// DRF - Return if two templated lists are equal or not.
template <class T>
inline bool StlIsEqual(std::list<T> const& l1, std::list<T> const& l2) {
	return (l1.size() == l2.size()) ? equal(l1.begin(), l1.end(), l2.begin()) : false;
}

//! \brief String substitution. Return true if at least one substitution done.
template <typename Char>
inline bool STLStringSubstituteT(std::basic_string<Char>& str, const std::basic_string<Char>& searchToken, const std::basic_string<Char>& substituteString, bool replaceAll = true) {
	int replaceCount = 0;
	size_t startPos = 0;

	do {
		size_t pos = str.find(searchToken, startPos);
		if (pos != std::string::npos) {
			str.erase(pos, searchToken.size());
			str.insert(pos, substituteString);
			replaceCount++;

			startPos = pos + substituteString.size(); // skip substitute string to avoid recursive
		} else {
			break;
		}
	} while (replaceAll);

	return replaceCount > 0;
}

inline bool STLStringSubstitute(std::string& str, const std::string& searchToken, const std::string& substituteString, bool replaceAll = true) {
	return STLStringSubstituteT(str, searchToken, substituteString, replaceAll);
}

inline bool STLStringSubstitute(std::wstring& str, const std::wstring& searchToken, const std::wstring& substituteString, bool replaceAll = true) {
	return STLStringSubstituteT(str, searchToken, substituteString, replaceAll);
}

/*
* Some helper functions to get system time and convert between readable date format and epoch time
*/
#include <sstream>
double GetEpochTime(SYSTEMTIME systemTime);

double GetCurrentEpochTime();

double GetCurrentLocalEpochTime();

std::string GetCurrentTimeStamp();

std::string EpochTimeToTimeStamp(double epochTime);

double DateToEpochTime(WORD year, WORD month, WORD day, WORD hour, WORD minute, WORD second, WORD millisecond);

// Get environment variable in STL string
inline std::string GetEnv(const char* name) {
	std::string res;

	std::wstring nameW = KEP::Utf8ToUtf16(name);
	size_t buflen;
	_wgetenv_s(&buflen, NULL, 0, nameW.c_str());

	if (buflen > 0) {
		std::vector<wchar_t> buffer(buflen, 0);
		if (0 == _wgetenv_s(&buflen, buffer.data(), buflen, nameW.c_str())) {
			if (!buffer.empty())
				res = KEP::Utf16ToUtf8(buffer.data()).c_str();
		}
	}

	return res;
}

// Check if a file name is valid
inline bool IsValidFileName(const std::string& fileName) {
	return strcspn(fileName.c_str(), "\\/:*?\"<>|") == fileName.size() && // Must be valid file name without path
		   fileName.find("..") == std::string::npos; // Must not contain ..
}

// Check if a path is absolute
inline bool IsAbsolutePath(const std::string& path) {
	// fully qualified UNC or drive?
	return path.length() > 1 && (path.substr(0, 2) == "\\\\" || isalpha(path[0]) && path[1] == ':' || path[0] == '\\');
}

// Return a absolute path. To obtain full path of baseDir only, use GetAbsolutePath( ".", baseDir )
// If path is empty, return empty.
// If path is already absolute, return path.
// If baseDir is empty, return path.
inline std::string GetAbsolutePath(const std::string& path, const std::string& baseDir) {
	if (path.empty() || baseDir.empty() || IsAbsolutePath(path)) {
		return path;
	}

	// DRF - Debugging ZMenu::GetAbsolutePath()->PathAdd() Crash Fix
	std::string retStr = "";
	wchar_t fullPath[MAX_PATH + 1] = L"";
	std::string fileName = PathAdd(baseDir, path);
	if (GetFullPathNameW(KEP::Utf8ToUtf16(fileName).c_str(), MAX_PATH, fullPath, NULL))
		retStr = KEP::Utf16ToUtf8(fullPath);

	return retStr;
}

#define IF_STR_EMPTY(x, y) ((x).empty() ? (y) : (x))
#define IF_NULL(x, y) ((x) ? (x) : (y))

///---------------------------------------------------------
// DRF - String Helpers
///---------------------------------------------------------
#include <algorithm>
#include <string>
#include <sstream>
#include <istream>
#include <iterator>

inline std::vector<std::string> StrSplit(std::string str) {
	std::istringstream iss(str);
	std::vector<std::string> strVec((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());
	return strVec;
}

// DRF - Returns the given string split at first of any of the given characters.
inline void StrSplitFirst(const std::string& str, const std::string& x, std::string& left, std::string& right) {
	unsigned found = str.find_first_of(x.c_str());
	if (found == std::string::npos) {
		left = str;
		right = "";
	} else {
		left = str.substr(0, found);
		right = str.substr(found + 1);
	}
}

// DRF - Returns the given string split at last of any of the given characters.
inline void StrSplitLast(const std::string& str, const std::string& x, std::string& left, std::string& right) {
	unsigned found = str.find_last_of(x.c_str());
	if (found == std::string::npos) {
		left = str;
		right = "";
	} else {
		left = str.substr(0, found);
		right = str.substr(found + 1);
	}
}

// DRF - Returns the given string to the right of the first of any of the given characters.
inline std::string StrStripLeftFirst(const std::string& str, const std::string& x) {
	unsigned found = str.find_first_of(x.c_str());
	return (found == std::string::npos) ? str : str.substr(found + 1);
}

// DRF - Returns the given string to the left of the first of any of the given characters.
inline std::string StrStripRightFirst(const std::string& str, const char* x) {
	unsigned found = str.find_first_of(x);
	return (found == std::string::npos) ? str : str.substr(0, found);
}

inline std::string StrStripRightFirst(const std::string& str, const std::string& x) {
	return StrStripRightFirst(str, x.c_str());
}

// DRF - Returns the given string to the right of the last of any of the given characters.
inline std::string StrStripLeftLast(const std::string& str, const std::string& x) {
	unsigned found = str.find_last_of(x.c_str());
	return (found == std::string::npos) ? str : str.substr(found + 1);
}

// DRF - Returns the given string to the left of the last of any of the given characters.
inline std::string StrStripRightLast(const std::string& str, const std::string& x) {
	unsigned found = str.find_last_of(x.c_str());
	return (found == std::string::npos) ? str : str.substr(0, found);
}

// DRF - Returns string in all uppercase.
inline std::string StrToUpper(const std::string& str) {
	std::string _str = str;
	transform(_str.begin(), _str.end(), _str.begin(), ::toupper);
	return _str;
}

// DRF - Returns string in all lowercase.
inline std::string StrToLower(const std::string& str) {
	std::string _str = str;
	transform(_str.begin(), _str.end(), _str.begin(), ::tolower);
	return _str;
}

// DRF - Returns string with all characters 'x' replaced by 'y'.
inline std::string StrReplace(const std::string& str, char x, char y) {
	std::string _str = str;
	replace(_str.begin(), _str.end(), x, y);
	return _str;
}

// DRF - Returns string with first instance of 'from' replaced by 'to'.
inline std::string StrReplace(const std::string& str, const std::string& from, const std::string& to) {
	size_t pos = str.find(from);
	if (pos == std::string::npos)
		return str;
	std::string _str = str;
	_str.replace(pos, from.length(), to);
	return _str;
}

// YC - Returns string with all '${ENV_VAR}' replaced by environment variables.
inline std::string StrReplaceEnvVar(const std::string& str) {
	// Replace All Environment Variables
	std::string strOut;
	size_t scanOfs = 0;
	FOREVER {
		// Find Left '${' In Input String
		size_t ofsLB = str.find("${", scanOfs);
		if (ofsLB == std::string::npos) {
			strOut.append(str.substr(scanOfs));
			break;
		}

		// Append Preceding Chars to Output String
		if (ofsLB > scanOfs)
			strOut.append(str.substr(scanOfs, ofsLB - scanOfs));

		// Find Right '}' In Input String
		size_t ofsRB = str.find("}", ofsLB + 1);
		if (ofsRB == std::string::npos)
			break;

		// Append Environment Variable to Output String
		std::string varName = str.substr(ofsLB + 2, ofsRB - ofsLB - 2);
		if (!varName.empty())
			strOut.append(GetEnv(varName.c_str()));

		// Move forward
		scanOfs = ofsRB + 1;
	}

	return strOut;
}

// DRF - Returns true if the strings are the same (case-sensitive)
inline bool StrSame(const std::string& str1, const std::string& str2) {
	return (str1 == str2);
}

inline char ToLower(char ch) {
	static const char achLower[256] = {
		'\x00',
		'\x01',
		'\x02',
		'\x03',
		'\x04',
		'\x05',
		'\x06',
		'\x07',
		'\x08',
		'\x09',
		'\x0a',
		'\x0b',
		'\x0c',
		'\x0d',
		'\x0e',
		'\x0f',
		'\x10',
		'\x11',
		'\x12',
		'\x13',
		'\x14',
		'\x15',
		'\x16',
		'\x17',
		'\x18',
		'\x19',
		'\x1a',
		'\x1b',
		'\x1c',
		'\x1d',
		'\x1e',
		'\x1f',
		'\x20',
		'\x21',
		'\x22',
		'\x23',
		'\x24',
		'\x25',
		'\x26',
		'\x27',
		'\x28',
		'\x29',
		'\x2a',
		'\x2b',
		'\x2c',
		'\x2d',
		'\x2e',
		'\x2f',
		'\x30',
		'\x31',
		'\x32',
		'\x33',
		'\x34',
		'\x35',
		'\x36',
		'\x37',
		'\x38',
		'\x39',
		'\x3a',
		'\x3b',
		'\x3c',
		'\x3d',
		'\x3e',
		'\x3f',
		'\x40',
		'\x61',
		'\x62',
		'\x63',
		'\x64',
		'\x65',
		'\x66',
		'\x67',
		'\x68',
		'\x69',
		'\x6a',
		'\x6b',
		'\x6c',
		'\x6d',
		'\x6e',
		'\x6f',
		'\x70',
		'\x71',
		'\x72',
		'\x73',
		'\x74',
		'\x75',
		'\x76',
		'\x77',
		'\x78',
		'\x79',
		'\x7a',
		'\x5b',
		'\x5c',
		'\x5d',
		'\x5e',
		'\x5f',
		'\x60',
		'\x61',
		'\x62',
		'\x63',
		'\x64',
		'\x65',
		'\x66',
		'\x67',
		'\x68',
		'\x69',
		'\x6a',
		'\x6b',
		'\x6c',
		'\x6d',
		'\x6e',
		'\x6f',
		'\x70',
		'\x71',
		'\x72',
		'\x73',
		'\x74',
		'\x75',
		'\x76',
		'\x77',
		'\x78',
		'\x79',
		'\x7a',
		'\x7b',
		'\x7c',
		'\x7d',
		'\x7e',
		'\x7f',
		'\x80',
		'\x81',
		'\x82',
		'\x83',
		'\x84',
		'\x85',
		'\x86',
		'\x87',
		'\x88',
		'\x89',
		'\x8a',
		'\x8b',
		'\x8c',
		'\x8d',
		'\x8e',
		'\x8f',
		'\x90',
		'\x91',
		'\x92',
		'\x93',
		'\x94',
		'\x95',
		'\x96',
		'\x97',
		'\x98',
		'\x99',
		'\x9a',
		'\x9b',
		'\x9c',
		'\x9d',
		'\x9e',
		'\x9f',
		'\xa0',
		'\xa1',
		'\xa2',
		'\xa3',
		'\xa4',
		'\xa5',
		'\xa6',
		'\xa7',
		'\xa8',
		'\xa9',
		'\xaa',
		'\xab',
		'\xac',
		'\xad',
		'\xae',
		'\xaf',
		'\xb0',
		'\xb1',
		'\xb2',
		'\xb3',
		'\xb4',
		'\xb5',
		'\xb6',
		'\xb7',
		'\xb8',
		'\xb9',
		'\xba',
		'\xbb',
		'\xbc',
		'\xbd',
		'\xbe',
		'\xbf',
		'\xc0',
		'\xc1',
		'\xc2',
		'\xc3',
		'\xc4',
		'\xc5',
		'\xc6',
		'\xc7',
		'\xc8',
		'\xc9',
		'\xca',
		'\xcb',
		'\xcc',
		'\xcd',
		'\xce',
		'\xcf',
		'\xd0',
		'\xd1',
		'\xd2',
		'\xd3',
		'\xd4',
		'\xd5',
		'\xd6',
		'\xd7',
		'\xd8',
		'\xd9',
		'\xda',
		'\xdb',
		'\xdc',
		'\xdd',
		'\xde',
		'\xdf',
		'\xe0',
		'\xe1',
		'\xe2',
		'\xe3',
		'\xe4',
		'\xe5',
		'\xe6',
		'\xe7',
		'\xe8',
		'\xe9',
		'\xea',
		'\xeb',
		'\xec',
		'\xed',
		'\xee',
		'\xef',
		'\xf0',
		'\xf1',
		'\xf2',
		'\xf3',
		'\xf4',
		'\xf5',
		'\xf6',
		'\xf7',
		'\xf8',
		'\xf9',
		'\xfa',
		'\xfb',
		'\xfc',
		'\xfd',
		'\xfe',
		'\xff',
	};
	return achLower[static_cast<unsigned char>(ch)];
}

// DRF - Returns true if the strings are the same (case-insensitive)
inline bool StrSameIgnoreCase(const std::string& str1, const std::string& str2) {
	size_t len1 = str1.length();
	size_t len2 = str2.length();
	if (len1 != len2)
		return false;

	const char* psz1 = str1.c_str();
	const char* psz2 = str2.c_str();
	for (size_t i = 0; i < len1; ++i) {
		if (ToLower(psz1[i]) != ToLower(psz2[i]))
			return false;
	}
	return true;
}

// DRF - Returns the string to the right of the given folder to strip (...\<filePathStrip>\path\name.ext -> path\name.ext)
inline std::string StrStripFilePath(const std::string& filePath, const std::string& filePathStrip) {
	auto it = StrToLower(filePath).find(StrToLower(filePathStrip) + "\\");
	return (it == std::string::npos) ? filePath : filePath.substr(it + filePathStrip.size() + 1);
}

// DRF - Returns the string to the right of the file path (.\path\filename.ext -> filename.ext)
inline std::string StrStripFilePath(const std::string& filePath) {
	return StrStripLeftLast(filePath, "/\\");
}

// DRF - Returns the string to the left of the file name and extension (.\path\filename.ext -> .\path)
inline std::string StrStripFileNameExt(const std::string& filePath) {
	return StrStripRightLast(filePath, "/\\");
}

// DRF - Returns the string to the right of the file name (.\path\filename.ext -> ext)
inline std::string StrStripFileName(const std::string& filePath) {
	return StrStripLeftLast(filePath, ".");
}

// DRF - Returns the string to the left of the file extension (.\path\filename.ext -> .\path\filename)
inline std::string StrStripFileExt(const std::string& filePath) {
	return StrStripRightLast(filePath, ".");
}

// DRF - Returns the string to the left of the file '_ab#' or the file extension (filename_ab#.ext -> filename)
inline std::string StrStripFileExtAB(const std::string& filePath) {
	return STLContains(filePath, "_ab") ? StrStripRightLast(filePath, "_") : StrStripFileExt(filePath);
}

// DRF - Returns the file name and extension (.\path\filename.ext -> filename.ext)
inline std::string StrFileNameExt(const std::string& filePath) {
	return StrStripFilePath(filePath);
}

// DRF - Returns the string file name (.\path\filename.ext -> filename)
inline std::string StrFileName(const std::string& filePath) {
	return StrStripFileExt(StrFileNameExt(filePath));
}

// DRF - Returns the string file name without '_ab#' or extension (.\path\filename_ab12.ext -> filename)
inline std::string StrFileNameNoAB(const std::string& filePath) {
	return StrStripFileExtAB(StrFileName(filePath));
}

// DRF - Returns the string file extension (.\path\filename.ext -> ext)
inline std::string StrFileExt(const std::string& filePath) {
	return StrStripFileName(StrFileNameExt(filePath));
}

// DRF - Returns the normalized file path (.\Path\FileName.EXT -> ./path/filename.ext)
inline std::string StrNormalizeFilePath(const std::string& filePath) {
	return StrReplace(StrToLower(filePath), '\\', '/');
}

// DRF - Returns true if the file paths are the same (normalized)
inline bool StrSameFilePath(const std::string& filePath1, const std::string& filePath2) {
	return (StrNormalizeFilePath(filePath1) == StrNormalizeFilePath(filePath2));
}

// DRF - Returns true if subPath is a sub path of filePath (normalized)
inline bool StrSameFileSubPath(const std::string& filePath, const std::string& subPath) {
	size_t subPathLen = subPath.length();
	return (filePath.length() < subPathLen) ? false : StrSameFilePath(filePath.substr(0, subPathLen), subPath);
}

// DRF - Returns true if the file extensions are the same (normalized)
inline bool StrSameFileExt(const std::string& filePath1, const std::string& filePath2) {
	return (StrNormalizeFilePath(StrFileExt(filePath1)) == StrNormalizeFilePath(StrFileExt(filePath2)));
}

// Return a number in hexadecimal string
inline std::string IntToHexStr(int num, bool prefix0x = true, bool lowercase = true) {
	char hexStr[20];
	sprintf_s(hexStr, lowercase ? "%s%08x" : "%s%08X", prefix0x ? "0x" : "", num);
	return hexStr;
}

// Url Encoding/Decoding (http://en.wikipedia.org/wiki/Uniform_resource_locator)
// Encodes a string for use as a Url. If isUrl true it performs a weak encoding
// assuming the string is already a full url with all parameters, otherwise it
// does a strong encoding assuming the string is a parameter that will then be
// inserted into a url.
#include <cctype>
#include <iomanip>
#include <sstream>
inline std::string UrlEncode(const std::string& str, bool isUrl) {
	// Reset Output Url (ints as hex)
	std::ostringstream url;
	url.fill('0');
	url << std::hex;

	// Encode Urls Following First '?' & Non Urls Entirely
	bool encode = !isUrl;
	for (auto i = str.begin(), n = str.end(); i != n; ++i) {
		auto c = (*i);
		bool unreserved = (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~');
		bool reserved = (c == '=' || c == '&' || c == '%' || c == ',' || c == '+');
		if (!encode || unreserved || (isUrl && reserved)) {
			encode |= (c == '?');
			url << c;
		} else if (c == ' ') {
			url << '+';
		} else {
			url << '%' << std::setw(2) << ((int)c) << std::setw(0);
		}
	}

	return url.str();
}

inline std::string JSONEncodeString(const std::string& str, bool bAddQuotes = false) {
	std::string strJSON;
	strJSON.reserve(str.size() + 2 * bAddQuotes);
	if (bAddQuotes)
		strJSON += '"';
	for (auto ch : str) {
		if (ch == '\\' || ch == '"') {
			strJSON += '\\';
			strJSON += ch;
		} else if (!std::iscntrl(ch)) {
			strJSON += ch;
		} else {
			const char* pszHexDigits = "0123456789abcdef";
			strJSON += "\\u00";
			unsigned int iChar = ch;
			strJSON += pszHexDigits[(iChar >> 4) & 0xf];
			strJSON += pszHexDigits[iChar & 0xf];
		}
	}
	if (bAddQuotes)
		strJSON += '"';
	return strJSON;
}

// String Stream Helpers
#include <sstream>
#define StrBuild(s, ss)        \
	{                          \
		std::stringstream _ss; \
		_ss << ss;             \
		s = _ss.str();         \
	}
#define StrBuildW(s, ss)        \
	{                           \
		std::wstringstream _ss; \
		_ss << ss;              \
		s = _ss.str();          \
	}
#define StrAppend(s, ss) StrBuild(s, s << ss)
#define StrAppendW(s, ss) StrBuildW(s, s << ss)
#define UrlBuild(u, ss)                 \
	{                                   \
		std::stringstream _ss;          \
		_ss << ss;                      \
		u = UrlEncode(_ss.str(), true); \
	}
#define UrlAppend(u, ss) UrlBuild(u, u << ss)

/** DRF
 * This function returns a random integer from 0 -> RAND_MAX.
 */
inline int GetRandomInt() {
	static time_t seedTime = 0;
	if (seedTime == 0) {
		seedTime = time(NULL);
		srand((unsigned int)seedTime);
	}
	return rand(); // integer 0 -> RAND_MAX
}

/** DRF
 * This function returns a random double from 0.0 -> 1.0.
 */
inline double GetRandomNumber() {
	return (double)GetRandomInt() / (double)RAND_MAX;
}

/** DRF
 * Returns a random character from d33 '!' -> d126 '~' excepting 
 * xml special characters ' ', '&', '<', '>' to avoid encoding issues.
 */
inline char GetRandomChar() {
	char c = (char)(93.0 * GetRandomNumber() + 33.5);
	return ((c == '&') || (c == '<') || (c == '>')) ? '!' : c;
}

/** DRF
* Rotates characters between 'A' and 'Z' and between 'a' and 'z'.
*/
inline char ROT13(char c) {
	if ((c >= 'A' && c <= 'M') || (c >= 'a' && c <= 'm'))
		c += 13;
	else if ((c >= 'N' && c <= 'Z') || (c >= 'n' && c <= 'z'))
		c -= 13;
	return c;
}

/** DRF
* Encrypts string later decrypted using KEP_Decrypt().
*/
inline std::string KEP_Encrypt(const std::string& s) {
	if (s.empty())
		return "";
	std::vector<char> x;
	x.resize(s.size() * 8);
	for (size_t i = 0; i < x.size(); ++i)
		x[i] = GetRandomChar();
	for (size_t i = 0; i < s.size(); ++i)
		x[8 * i + ((i + 4) % 8)] = ROT13(s[i]);
	return std::string(x.begin(), x.end());
}

/** DRF
* Decrypts string previously encrypted using KEP_Encrypt().
*/
inline std::string KEP_Decrypt(const std::string& x) {
	if (x.empty())
		return "";
	std::vector<char> s;
	s.resize(x.size() / 8);
	for (size_t i = 0; i < s.size(); ++i)
		s[i] = ROT13(x[8 * i + ((i + 4) % 8)]);
	return std::string(s.begin(), s.end());
}

/** DRF
 * This function converts a given MsgProc message code (WM_xxx) to string.
 */
void GetMsgProcMsgStr(UINT msgCode, std::string& msgStr);

///---------------------------------------------------------
// DRF - File Helper RAII
///---------------------------------------------------------
#include "Shlwapi.h"
#include "Shellapi.h"
typedef uint64_t FileSize;
typedef uint64_t FilePos;
struct FileHelper {
	HANDLE m_handle; // file handle
	FilePos m_pos; // file position (bytes)

	FileHelper() :
			m_handle(INVALID_HANDLE_VALUE),
			m_pos(0) {}

	FileHelper(const std::string& path, DWORD dwMode = (GENERIC_READ | GENERIC_WRITE), DWORD dwCreate = OPEN_ALWAYS) :
			m_handle(INVALID_HANDLE_VALUE),
			m_pos(0) {
		Open(path, dwMode, dwCreate);
	}

	~FileHelper() {
		Close();
	}

	static bool Exists(const std::string& path) {
		if (path.empty())
			return false;
		return (::PathFileExistsW(KEP::Utf8ToUtf16(path).c_str()) != FALSE);
	}

	// Using both GetFileAttributesEx() and _stat64() because GetFileAttributesEx()
	// returns 0 for symlinks but _stat64() returns 0 on XP.  Together we get more
	// or less everything we need.
	static FileSize Size(const std::string& path) {
		if (path.empty())
			return 0;
		FileSize fileSize = 0;
		struct _stat64 fileStat;
		if (_wstat64(KEP::Utf8ToUtf16(path).c_str(), &fileStat) == 0)
			fileSize = (FileSize)fileStat.st_size;
		if (fileSize == 0) {
			WIN32_FILE_ATTRIBUTE_DATA fileInfo = { 0 };
			if (::GetFileAttributesExW(KEP::Utf8ToUtf16(path).c_str(), GetFileExInfoStandard, &fileInfo) != FALSE)
				fileSize = (FileSize)((((uint64_t)fileInfo.nFileSizeHigh) << 32) | (uint64_t)fileInfo.nFileSizeLow);
		}
		return fileSize;
	}

	static std::string GetCurrentDirectory(bool normalize = false) {
		wchar_t path[MAX_PATH] = L"";
		::GetCurrentDirectoryW(_countof(path), path);
		std::string strPath = KEP::Utf16ToUtf8(path);
		return normalize ? StrNormalizeFilePath(strPath) : strPath;
	}

	static bool SetCurrentDirectory(const std::string& path) {
		if (path.empty())
			return false;
		return (::SetCurrentDirectoryW(KEP::Utf8ToUtf16(path).c_str()) != FALSE);
	}

	static bool SetAttr(const std::string& path, int attr) {
		if (path.empty())
			return false;
		return (::SetFileAttributesW(KEP::Utf8ToUtf16(path).c_str(), attr) != FALSE);
	}

	static bool CreateFolder(const std::string& path) {
		if (path.empty())
			return false;
		if (Exists(path))
			return true;
		return (::CreateDirectoryW(KEP::Utf8ToUtf16(path).c_str(), NULL) == FALSE) ? (GetLastError() == ERROR_ALREADY_EXISTS) : Exists(path);
	}

	static bool CreateFolderDeep(const std::string& path) {
		if (path.empty())
			return false;
		if (CreateFolder(path))
			return true;
		for (size_t i = 0; i < path.size(); ++i)
			if (path[i] == '\\')
				CreateFolder(path.substr(0, i));
		return CreateFolder(path);
	}

	static bool Delete(const std::string& path) {
		if (path.empty())
			return false;
		SetAttr(path, FILE_ATTRIBUTE_NORMAL);
		return (::DeleteFileW(KEP::Utf8ToUtf16(path).c_str()) == FALSE) ? (GetLastError() == ERROR_FILE_NOT_FOUND) : !Exists(path);
	}

	// Recursively deletes given folder and all contents!
	static bool DeleteFolder(const std::string& path) {
		if (path.empty())
			return false;
		// SHFileOperation Requires 2 terminating nulls !
		char pathStr[MAX_PATH] = "";
		strcpy_s(pathStr, path.c_str());
		size_t pathSize = path.size();
		pathStr[pathSize] = 0;
		pathStr[pathSize + 1] = 0;
		SHFILEOPSTRUCTA file_op = {
			NULL,
			FO_DELETE,
			pathStr,
			NULL,
			FOF_NO_UI,
			false,
			0,
			NULL
		};
		int err = ::SHFileOperationA(&file_op);
		return (err != 0) ? (err == ERROR_FILE_NOT_FOUND) : !Exists(path);
	}

	static bool Copy(const std::string& pathFrom, const std::string& pathTo) {
		if (pathFrom.empty() || pathTo.empty())
			return false;
		if (StrSameFilePath(pathFrom, pathTo))
			return true;
		Delete(pathTo);
		return (::CopyFileW(KEP::Utf8ToUtf16(pathFrom).c_str(), KEP::Utf8ToUtf16(pathTo).c_str(), FALSE) != FALSE);
	}

	static bool Move(const std::string& pathFrom, const std::string& pathTo) {
		if (pathFrom.empty() || pathTo.empty())
			return false;
		if (StrSameFilePath(pathFrom, pathTo))
			return true;
		Delete(pathTo);
		return (::MoveFileW(KEP::Utf8ToUtf16(pathFrom).c_str(), KEP::Utf8ToUtf16(pathTo).c_str()) != FALSE);
	}

	bool IsOpen() const {
		return (m_handle != INVALID_HANDLE_VALUE);
	}

	bool Open(const std::string& path, DWORD dwMode = (GENERIC_READ | GENERIC_WRITE), DWORD dwCreate = OPEN_ALWAYS) {
		if (path.empty())
			return false;
		if (IsOpen())
			Close();
		if (dwCreate == CREATE_ALWAYS)
			Delete(path);
		DWORD dwShareMode = (dwMode == GENERIC_READ) ? FILE_SHARE_READ : 0;
		m_handle = ::CreateFileW(KEP::Utf8ToUtf16(path).c_str(), dwMode, dwShareMode, NULL, dwCreate, FILE_ATTRIBUTE_NORMAL, NULL);
		return IsOpen();
	}

	bool Close() {
		if ((m_handle != INVALID_HANDLE_VALUE) && ::CloseHandle(m_handle))
			m_handle = INVALID_HANDLE_VALUE;
		m_pos = 0;
		return (m_handle == INVALID_HANDLE_VALUE);
	}

	FilePos Pos() const {
		return m_pos;
	}

	DWORD Rx(void* data, DWORD bytes) {
		DWORD bytesRx = bytes;
		bytesRx = (IsOpen() && ::ReadFile(m_handle, data, bytes, &bytesRx, NULL)) ? bytesRx : 0;
		m_pos += (FilePos)bytesRx;
		return bytesRx;
	}

	DWORD Tx(const void* data, DWORD bytes) {
		DWORD bytesTx = bytes;
		bytesTx = (IsOpen() && ::WriteFile(m_handle, data, bytes, &bytesTx, NULL)) ? bytesTx : 0;
		m_pos += (FilePos)bytesTx;
		return bytesTx;
	}
};

// Changes current directory. Changes it back to original when it goes out of scope.
class ScopedDirectoryChanger {
public:
	ScopedDirectoryChanger(const std::string& newDirectory) {
		m_strOldDirectory = FileHelper::GetCurrentDirectory();
		FileHelper::SetCurrentDirectory(newDirectory);
	};

	~ScopedDirectoryChanger() {
		ChangeBackToOldDirectory();
	}

	void ChangeBackToOldDirectory() {
		if (!m_strOldDirectory.empty())
			FileHelper::SetCurrentDirectory(m_strOldDirectory);
		m_strOldDirectory.clear();
	}

private:
	std::string m_strOldDirectory;
};

inline std::string GetSubDirectoryOfCurrent(const char* subDir) {
	return PathAdd(FileHelper::GetCurrentDirectory(), subDir);
}

inline std::wstring GetSubDirectoryOfCurrentW(const wchar_t* subDir) {
	return KEP::Utf8ToUtf16(GetSubDirectoryOfCurrent(KEP::Utf16ToUtf8(subDir).c_str()));
}

// -------------------------------------------------------------
// DP engine message string encoding/decoding helper
// -------------------------------------------------------------
// 1) Encode a length-prefixed string (max string length 255). Caller should guarantee that the buffer is big enough for the string.
inline void encodeLengthPrefixedString(LPVOID lpMessage, int& byteOffset, const char* szString) {
	jsVerifyReturnVoid(szString != NULL && lpMessage != NULL && byteOffset >= 0);

	BYTE* buffer = (BYTE*)lpMessage;

	size_t strLen = std::min(strlen(szString), (size_t)BYTE_MAX); // Truncate at 255 characters if too long
	buffer[byteOffset] = (BYTE)strLen;
	byteOffset++;

	if (strLen > 0) {
		memcpy(buffer + byteOffset, szString, strLen);
		byteOffset += strLen;
	}
}

// 2) Decode a length-prefixed string (max string length 255)
inline bool decodeLengthPrefixedString(LPVOID lpMessage, size_t& byteOffset, size_t msgSize, std::string& outStr) {
	jsVerifyReturn(lpMessage != NULL && byteOffset + 1 <= msgSize, false);

	BYTE* buffer = (BYTE*)lpMessage;

	size_t strLen = buffer[byteOffset];
	byteOffset++;
	jsVerifyReturn(byteOffset + (int)strLen <= msgSize, false);

	if (strLen == 0) {
		outStr.clear();
	} else {
		outStr.assign((char*)(buffer + byteOffset), strLen);
		byteOffset += strLen;
	}
	return true;
}
