///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

/**
 * This file defines constants that define the lower boundaries of glid value ranges
 *
 * GLIDs are unsigned 4byte numeric values.  GLID values can fall into one of two
 * sets: KANEVA GLIDS and UGC GLIDS.  
 *
 *  1                                                                    0xffffffff
 *  |          KANEVA GLIDS        |              UGC GLIDS				 |
 *
 * Furthermore, each of these two super categories are broken into sub categories as follows:
 *
 *  1                                                                    0xffffffff
 *  | STUDIO GLIDS   | SGI GLIDS   |  BASIC UGC GLIDS | DYNOBJ UGC GLIDS | 
 * 
 * This header declares the minimum constants needed to define these ranges along with helper
 * macros such that developers should rarely need to reference these constants let alone
 * actually know the value of these constants.
 */
typedef int GLID; // drf - local glids are negative (we get 31-bits of glid)

const GLID GLID_VALID_MIN = (GLID)-1000000; // min valid glid

const GLID GLID_LOCAL_MIN = (GLID)-1000000; // min local import glid
const GLID GLID_LOCAL_MAX = (GLID)-1; // max local import glid
const GLID GLID_LOCAL_BASE = (GLID)-1; // base local import glid

const GLID GLID_INVALID = (GLID)0; // invalid glid

const GLID STUDIO_BASE_GLID = (GLID)1; // Studio created item glids	(1    <= n < SGI_BASE_GLID)
const GLID GLID_CORE_MIN = (GLID)1; // min internal client engine reserved glid
const GLID GLID_CORE_MAX = (GLID)99; // max internal client engine reserved glid
const GLID SGI_BASE_GLID = (GLID)1000000; // Script Game Item glids		(1mil <= n < UGC_BASE_GLID)
const GLID UGC_BASE_GLID = (GLID)3000000; // UGC Item glids				(3mil <= n )
const GLID UGC_DO_BASE_GLID = (GLID)1000000000; // Dynamic object mesh glids	(1bil <= n )
const GLID UGC_DOB_FILE_GLID = UGC_DO_BASE_GLID; // A dummy GLID used in UGC DO data file

const GLID GLID_VALID_MAX = (GLID)INT_MAX; // max valid glid (31-bits 2147483647)

inline bool IS_VALID_GLID(const GLID& glid) {
	return (glid != GLID_INVALID) && (glid >= GLID_VALID_MIN) && (glid <= GLID_VALID_MAX);
	//	return (glid != GLID_INVALID);
}

inline bool IS_LOCAL_GLID(const GLID& glid) {
	return (glid >= GLID_LOCAL_MIN) && (glid <= GLID_LOCAL_MAX);
}

inline bool IS_CORE_GLID(const GLID& glid) {
	return (glid >= GLID_CORE_MIN) && (glid <= GLID_CORE_MAX);
}

#define IS_DYNOBJ_GLID(n) (IS_VALID_GLID(n) && (UGC_DO_BASE_GLID <= (n)))
#define IS_UGC_GLID(n) (IS_VALID_GLID(n) && (UGC_BASE_GLID <= (n)))
#define IS_SGI_GLID(n) (IS_VALID_GLID(n) && ((SGI_BASE_GLID <= (n)) && ((n) < UGC_BASE_GLID)))
#define IS_STUDIO_GLID(n) (IS_VALID_GLID(n) && ((STUDIO_BASE_GLID <= (n)) && ((n) < SGI_BASE_GLID)))
#define IS_KANEVA_GLID(n) (IS_VALID_GLID(n) && ((STUDIO_BASE_GLID <= (n)) && ((n) < UGC_BASE_GLID)))

#define IS_VALID_NON_UGC_GLID(n) (IS_VALID_GLID(n) && !IS_UGC_GLID(n))
#define IS_VALID_NON_SGI_GLID(n) (IS_VALID_GLID(n) && !IS_SGI_GLID(n))

#define IS_VALID_CONTENTMETADATA_GLID(n) (IS_VALID_NON_SGI_GLID(n) && !IS_LOCAL_GLID(n))

// DRF - Added - Used By CharConfigClass
typedef GLID GLID_OR_BASE; // drf - GOB_BASE_ITEM(0) means CharConfig base item instead of glid
typedef unsigned __int64 GLID_OR_BASE_SERIAL; // drf - legacy serialized type (UINT64)
const GLID_OR_BASE GOB_BASE_ITEM = GLID_INVALID; // drf - CharConfig base item instead of glid
