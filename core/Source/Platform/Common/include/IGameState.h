#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "KEPMacros.h"
#include "InstanceId.h"
#include "Core/Math/Vector.h"
#include "Glids.h"
#include "PlayerMask.h"
#include "TransactionType.h"
#include "CurrencyType.h"
#include <string>
#include <map>

namespace KEP {

class CAccountObject;
class CGlobalInventoryObj;
class CInventoryObjList;
class CItemObj;
class CPlayerObject;
class MediaParams;
class PassList;
class IEvent;
class KGP_Connection;
class UrlParser;
typedef ULONG NETID;
enum EServerState;
enum class BlockType;

class IGameState {
public:
	enum EUpdateRet {
		UPDATE_OK,
		UPDATE_PLAYER1_INSUFFICIENT,
		UPDATE_PLAYER2_INSUFFICIENT,
		UPDATE_ERROR
	};

	static IGameState *GetState();

	virtual ~IGameState();

	// attempt to connect to the database
	virtual void Initialize(const char *gameDir);

	// initialize any event/eventhandlers
	virtual void InitEvents() = 0;

	// called when the server engine does save all
	virtual void SaveAll() = 0;

	virtual void Logoff(CAccountObject *acct, CPlayerObject *player, ULONG reason, IEvent *replyEvent) = 0;

	// called when the player is selected by the user after logging in
	// the inbound object will have just the name, on return, the
	// player will have everything
	virtual void GetPlayer(CPlayerObject *player) = 0;

	// called when the user creates a new player object
	virtual void CreatePlayer(CAccountObject *acct, CPlayerObject *player) = 0;

	virtual void DeletePlayer(ULONG accountId, CPlayerObject *player) = 0;


	// called to update the player in the database
	virtual void UpdatePlayer(CPlayerObject *player, ULONG changeMask = PlayerMask::USE_DIRTY_FLAGS) = 0;
	virtual bool ReloadPlayer(CPlayerObject *player, ULONG changeMask) = 0;

	virtual void RefreshInventory(CPlayerObject *player, bool = false) = 0;

	virtual void updateInventory3DApp(int playerId, CInventoryObjList *inventory, ULONG changeMask) = 0;

	virtual bool getItemData(int glid, CItemObj *item) = 0;
	virtual CGlobalInventoryObj *addCustomItem(int globalId) = 0; // create an custom item (currently UGC furniture or UGC mesh) from wok.items table

	virtual long GetPlayersCash(CPlayerObject *player, CurrencyType currencyType = CurrencyType::CREDITS_ON_PLAYER) = 0;
	// called to change cash amt , which is from db
	// amountChanging is the amount to add, or remove (if negative)
	// returns true if sufficient funds, throws otherwise
	virtual EUpdateRet UpdatePlayersCash(CPlayerObject *player1, long player1AmtChanging,
		TransactionType transactionType,
		CPlayerObject *player2 = 0, long player2AmtChanging = 0,
		CurrencyType currencyType = CurrencyType::CREDITS_ON_PLAYER,
		int glid = 0, int qty = 0) = 0;

	virtual EUpdateRet PayCoverCharge(CAccountObject *acct, CPlayerObject *player) = 0;

	// called to update both players' inventories, etc. for trade
	virtual void UpdateTradingPlayers(CPlayerObject *player1, CPlayerObject *player2, ULONG changeMask = PlayerMask::INVENTORY | PlayerMask::PLAYER) = 0;

	virtual void ExpandGroup(int groupId, std::vector<std::pair<int, std::string>> &recipientList) = 0;

	virtual PassList *GetZonePassList(const ZoneIndex &zi) = 0;

	virtual bool IsUserBlocked(CAccountObject *acctWantingAccess, LONG destKanevaId, LONG instanceId, BlockType blockType, const char *blockingUserName) = 0;

	virtual bool DoesItemSupportAnimation(long itemGLID, long animGLID) = 0;

	virtual bool CreateClan(CPlayerObject *player, const char *clanName, ULONG &clanId) = 0;
	virtual bool DeleteClan(int ownerId) = 0;
	virtual bool AddMemberToClan(ULONG clanId, ULONG playerId) = 0;
	virtual bool RemoveMemberFromClan(ULONG clanId, ULONG playerId) = 0;

	virtual bool SetPlayerBanStatus(const char *playerName, const char *reason, CPlayerObject *banner, bool banned) = 0;

	virtual long SchemaVersion() const = 0;

	virtual bool CheckForNewP2pAnimation(int p2pIndex) = 0;

	virtual bool MapServerId(LONG serverId, std::string &server, ULONG &port) = 0;

	static const char *MapDisconnectReason(ULONG disconnectReason);

	virtual void LoadScriptServerItemAvailability() = 0;

	virtual bool IsAvatarTemplateApplied(CPlayerObject *player) = 0;
	virtual bool ApplyAvatarSelection(int avatarIndex, CPlayerObject *player) = 0;

	virtual std::string GetDefaultPlaceUrl() const = 0;
	virtual void SetDefaultPlaceUrl(const std::string &url) = 0;
	virtual ZoneIndex GetInitialArenaZoneIndex() const = 0;
	virtual void SetInitialArenaZoneIndex(ZoneIndex zi) = 0;

	virtual void SavePlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex, float x, float y, float z, float rx, float ry, float rz) = 0;
	virtual void DeletePlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex) = 0;
	virtual bool GetPlayerSpawnPoint(int playerId, const ZoneIndex &zoneIndex, float &x, float &y, float &z, float &rx, float &ry, float &rz) = 0;

	virtual void updateMediaPlayer(const ZoneIndex &zoneIndex, NETID netId, const CPlayerObject *player, int objPlacementId, const MediaParams &mediaParams,
		bool usePlaylistId, int assetId = 0, int assetType = 0, const char *itemUrl = NULL, const char *mature = NULL, int stopPlay = 0) = 0;
	virtual void updateMediaPlayerVolumeAndRadius(const ZoneIndex &zoneIndex, NETID netId, const CPlayerObject *player, int objPlacementId, const MediaParams &mediaParams) = 0;

	virtual void UpdateGameInventorySync(int kanevaUserId, int gameId, ULONG globalId, int inventoryCount) = 0;
	virtual ULONG AddGameItemPlacement(const ZoneIndex &zoneIndex, ULONG doGlid, const Vector3f &pos, const Vector3f &ori, int ownerId, int gameItemId) = 0;
	virtual void ClearZoneDOMemCache(ZoneIndex const &zi) = 0;
	virtual bool GetParentZoneInfo(KGP_Connection *conn, unsigned zoneInstanceId, unsigned zoneType, unsigned &parentZoneInstanceId, unsigned &parentZoneIndex, unsigned &parentZoneType, unsigned &parentCommunityId) = 0;
	virtual TimeMs GetEffectDurationMs(KGP_Connection *conn, GLID globalId) = 0;
	virtual void AddEffectDurationMs(const std::map<GLID, TimeMs> &effectDurationMap) = 0;

	virtual bool GetYouTubeSWFUrls(std::string &lastKnownGoodUrl, std::string &overrideUrl) = 0;
};

} // namespace KEP
