///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "js.h"
#include "jsThread.h"
#include "log4cplus\logger.h"
#include "Core/Util/fast_mutex.h"
#include <vector>

using namespace log4cplus;

namespace KEP {

/// \brief class to monitor thread, and process CPU
///
/// This class will monitor the process's CPU usage
/// and any thread's CPU usage over a given interval
///
class CpuMonitor : public jsThread {
	typedef fast_recursive_mutex fast_recursive_mutex;

public:
	/// \brief constructor
	///
	/// \param logger the logging object from parent
	CpuMonitor(Logger &logger);

	/// \brief initialize the CpuMonitor and start the background thread
	///
	/// \param interval the time in ms between checking CPU
	/// \return true if ok, false if failed to get processor count.  Will log
	bool Initialize(TimeMs interval = 60000);

	/// \brief terminate the CpuMonitor background thread
	void Terminate();

	/// \brief call to monitor the calling thread
	///
	/// \return your thread id, or zero if could not get thread handle
	DWORD MonitorMe();

	/// \brief return the last value of the process's CPU
	///
	/// \return the value in percent. Note that this may be greater than 100
	int LastProcessCpu() const {
		return m_processLastValue;
	}

	/// \brief return the last value of the thread's CPU
	///
	/// \param the thread Id of the thread in question
	/// \return the value in percent. Note that this may be greater than 100 due
	/// to Windows quirkiness
	int LastThreadCpu(DWORD threadId) {
		std::lock_guard<fast_recursive_mutex> lock(getSync());
		ThreadList::iterator i = m_thds.begin();
		for (; i != m_thds.end(); i++) {
			if (i->id == threadId)
				return i->lastValue;
		}
		return 0;
	}

	/// /return the interval used for checking CPU
	TimeMs interval() const { return m_interval; }

protected:
	/// \brief helper class for tracking thread details
	class ThreadInfo {
	public:
		ThreadInfo(DWORD _id, HANDLE _h) :
				id(_id), handle(_h), lastValue(0), hadStart(false) {}

		ThreadInfo(const ThreadInfo &src) { operator=(src); }
		ThreadInfo &operator=(const ThreadInfo &src) {
			if (this != &src) {
				id = src.id;
				handle = src.handle;
				lastValue = src.lastValue;
			}
			return *this;
		}
		DWORD id;
		HANDLE handle;
		int lastValue;
		bool hadStart;

		// these are temp for threadInfo so don't copy or init
		FILETIME ignoreCreate, ignoreExit;
		FILETIME beginKernelTime, beginUserTime;
		FILETIME endKernelTime, endUserTime;
	};

	/// \brief override from jsThread
	///
	/// loops, monitoring all the threads and the main process
	ULONG _doWork();

	typedef std::vector<ThreadInfo> ThreadList;

	ThreadList m_thds;
	TimeMs m_interval;
	fast_recursive_mutex m_sync;
	int m_processLastValue;
	fast_recursive_mutex &getSync() { return m_sync; }
	Logger m_logger;
	int m_processorCount;
};

} // namespace KEP