#pragma once

#ifndef INOUT
#define INOUT
#endif

#include <string>

// helper for buidling and parsing stp urls
class StpUrl {
public:
// NOTE the only other place these suffixes exist are in *vw_url* Stored Procs and StpUrl.cs
// If changes are made, they must be made in both places
#define HOME_SUFFIX ".home"
#define CHANNEL_SUFFIX ".channel"
#define ZONE_SUFFIX ".zone"
#define PEOPLE_SUFFIX ".people"
#define URL_PREFIX "kaneva://"
#define PARAMETER_IDENTIFIER "?"

	// will be xxxx://<gamename>/ (currently xxxx is kaneva, but we won't look for that
	static inline void ExtractGameName(IN const std::string &url, OUT std::string &gameName) {
		gameName.clear();

		std::string::size_type start = url.find("://");
		if (start != std::string::npos) {
			start += 3; // skip ://
			std::string::size_type end = url.find("/", start);
			if (end != std::string::npos) {
				gameName = url.substr(start, end - start);
			} else {
				gameName = url.substr(start);
			}
		}
	}

	static inline void ExtractPlaceNameAndInstanceId(IN const std::string &url, OUT std::string &placeName, OUT LONG &instanceId) {
		instanceId = 0;
		placeName.clear();

		std::string::size_type start = url.find("://");
		if (start != std::string::npos) {
			start += 3; // skip ://
			std::string::size_type end = url.find("/", start);
			if (end != std::string::npos) {
				if (url.size() > end) {
					// usually something like mall.zone
					placeName = url.substr(end + 1, url.size() - end - 1);

					// see if instanced (trailing .n where n is instanceid e.g. mall.zone.1)
					std::string::size_type dot = placeName.find(".");
					if (dot != std::string::npos && placeName.size() > dot) {
						std::string::size_type secondDot = placeName.find(".", dot + 1);
						if (secondDot != std::string::npos && placeName.size() > secondDot) {
							std::string temp = placeName;
							placeName = temp.substr(0, secondDot);
							instanceId = atol(temp.substr(secondDot + 1).c_str());
						}
					}
				}
				// else just <prefix>://gamename/
			}
			// else just <prefix>://gamename
		}
	}

	static inline void ExtractPlaceName(IN const std::string &url, OUT std::string &placeName) {
		LONG ignored = 0;
		ExtractPlaceNameAndInstanceId(url, placeName, ignored);
	}

	static inline void ExtractInstanceId(IN const std::string &url, OUT LONG &instanceId) {
		std::string ignored;
		ExtractPlaceNameAndInstanceId(url, ignored, instanceId);
	}

	static inline void ExtractParameters(IN const std::string &url, OUT std::string &params) {
		params.clear();

		std::string::size_type start = url.find("://");
		if (start != std::string::npos) {
			start += 3; // skip ://
			std::string::size_type end = url.find(PARAMETER_IDENTIFIER, start);
			if (end != std::string::npos) {
				if (url.size() > end) {
					// should be something like ?param1=testvalue&param2=testvalue2
					params = url.substr(end + 1, url.size() - end - 1);
				}
				// else no params specified
			}
			// else no params specified
		}
	}

	// make sure the url has <prefix>  if not fix it
	// return true if was ok, false if wasn't and the string was fixed
	static inline bool CheckPrefix(INOUT std::string &url) {
		std::string::size_type start = url.find("://");
		if (start != std::string::npos) {
			std::string scheme = url.substr(0, start + 3);
			if (scheme == URL_PREFIX) {
				return true;
			} else {
				url.replace(0, start + 3, URL_PREFIX);
			}
		}
		return false;
	}

	static inline std::string MakeUrlPrefix(IN const char *gameName) {
		std::string ret(URL_PREFIX);
		ret += gameName;

		return ret;
	}

	static inline std::string MakeUrlPrefix(IN LONG gameId) {
		std::string ret(URL_PREFIX);
		char s[30];
		_ltoa_s(gameId, s, _countof(s), 10);
		ret += s;

		return ret;
	}

	static inline std::string MakePersonUrlPath(IN const char *userName) {
		std::string ret(userName);
		ret += PEOPLE_SUFFIX;

		return ret;
	}

	static inline std::string MakeAptUrlPath(IN const char *userName) {
		std::string ret(userName);
		ret += HOME_SUFFIX;

		return ret;
	}

	static inline std::string MakeZoneUrlPath(IN const char *zoneName) {
		std::string ret(zoneName);
		ret += ZONE_SUFFIX;

		return ret;
	}

	static inline std::string MakeCommunityUrlPath(IN const char *communityName) {
		std::string ret(communityName);
		ret += CHANNEL_SUFFIX;

		return ret;
	}

	template <class T>
	static inline std::string MakePersonUrl(IN T gameName, IN const char *userName) {
		std::string ret(MakeUrlPrefix(gameName));
		ret += "/";
		ret += userName;
		ret += PEOPLE_SUFFIX;

		return ret;
	}

	template <class T>
	static inline std::string MakeAptUrl(IN T gameName, IN const char *userName) {
		std::string ret(MakeUrlPrefix(gameName));
		ret += "/";
		ret += userName;
		ret += HOME_SUFFIX;

		return ret;
	}

	template <class T>
	static inline std::string MakeZoneUrl(IN T gameName, IN const char *zoneName) {
		std::string ret(MakeUrlPrefix(gameName));
		ret += "/";
		ret += zoneName;
		ret += ZONE_SUFFIX;

		return ret;
	}

	template <class T>
	static inline std::string MakeCommunityUrl(IN T gameName, IN const char *communityName) {
		std::string ret(MakeUrlPrefix(gameName));
		ret += "/";
		ret += communityName;
		ret += CHANNEL_SUFFIX;

		return ret;
	}
};

