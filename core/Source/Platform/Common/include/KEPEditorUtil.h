/******************************************************************************
 KEPEditorUtil.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <vector>
#include "SDKCommon.h"

// forward declarations
class CWnd;

namespace KEP {

/** Opens a CFileDialog dialog and gathers the names of multiple files to be opened or processed
  * @param aMakeAbsolute Wether to generate absolute paths or return only the file names in aFileNames
  */
bool GetMultipleFileNamesToOpen(OUT std::vector<string>& aFileNames, OUT string& aFolderPath, IN const string& aFilter, IN CWnd* pParentWnd, IN bool aMakeAbsolute);
} // namespace KEP

