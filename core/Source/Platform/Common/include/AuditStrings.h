///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDS_AUDIT_LOGON_NO_ACCESS 7000
#define IDS_AUDIT_LOGON_ERR 7001
#define IDS_AUDIT_SUSPENED 7002
#define IDS_AUDIT_NON_PAYMENT 7003
#define IDS_AUDIT_ALREADY_LOGGED_ON 7004
#define IDS_AUDIT_LOGIN_OK 7005
#define IDS_AUDIT_LOGOUT 7006
#define IDS_AUDIT_TRADE 7007
#define IDS_AUDIT_SHUTDOWN 7008
#define IDS_AUDIT_STARTALL 7009
#define IDS_AUDIT_SAVEALL 7010
#define IDS_AUDIT_TRANSACTION_BUY 7011
#define IDS_AUDIT_NEW_LEVEL 7012
#define IDS_AUDIT_DEATH 7013
#define IDS_AUDIT_VUP 7014
#define IDS_AUDIT_VDOWN 7015
#define IDS_AUDIT_REMOVE_HOUSE 7016
#define IDS_AUDIT_ZERO_MURDERS 7017
#define IDS_AUDIT_BLOCKIP 7018
#define IDS_AUDIT_BLOCKBYNAME 7019
#define IDS_AUDIT_UNBLOCKBYNAME 7020
#define IDS_AUDIT_UNBLOCK 7021
#define IDS_AUDIT_SPAWNGEN 7022
#define IDS_AUDIT_BROADCAST 7023
#define IDS_AUDIT_GENERATE 7024
#define IDS_AUDIT_CHAT 7025
#define IDS_AUDIT_STR_UP 7026
#define IDS_AUDIT_DEX_UP 7027
#define IDS_AUDIT_WISDOM_UP 7028
#define IDS_AUDIT_CLANCHAT 7029
#define IDS_AUDIT_ADMIN_ACTION 7030
#define IDS_AUDIT_STAT_UP 7031
#define IDS_ACCOUNT_COUNT 7032
#define IDS_AUDIT_PLAYER_ZONE 7033

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE 102
#define _APS_NEXT_COMMAND_VALUE 40001
#define _APS_NEXT_CONTROL_VALUE 1001
#define _APS_NEXT_SYMED_VALUE 101
#endif
#endif
