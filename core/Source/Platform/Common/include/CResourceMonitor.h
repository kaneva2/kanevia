#pragma once

class CResourceMonitor {
public:
	CResourceMonitor() {
	}

	CResourceMonitor(const CResourceMonitor&) = delete;

	~CResourceMonitor() {
	}
};
