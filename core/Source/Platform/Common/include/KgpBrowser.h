#pragma once

// ============ callbacks see below for list of types and lparam
typedef void (*BROWSER_CALLBACK_FN)(DWORD userParam, SHORT type, const char* strParam, DWORD wparam);

// one place to define the generic event name
#define BROWSER_EVENT_NAME "BrowserCallbackEvent"
#define BROWSER_REPLY_EVENT_NAME "BrowserReplyEvent"
#define BROWSER_REQUEST_EVENT_NAME "BrowserRequestEvent"
#define INVENTORY_UPDATE_EVENT "InventoryUpdateEvent"

// types
const SHORT BROWSER_KGP_LINK = 1; // sp = sz string that has everything after scheme: (scheme passed into create), wp = 0
const SHORT BROWSER_NAVIGATE_BEGIN = 2; // sp = "",  wp = 0, can use to start throbber
const SHORT BROWSER_NAVIGATE_COMPLETE = 3; // sp = "", wp = 0, can use to stop throbber
const SHORT BROWSER_UPDATE_PROGRESS = 4; // sp = "", wp = % progress
const SHORT BROWSER_STATUSTEXT_CHANGE = 5; // sp = sz string for text, wp = 0
const SHORT BROWSER_LOCATION_CHANGE = 6; // sp = sz string or redirected URL, wp = 0

// types for the BROWSER_NAVIGATE type, add 100, just to avoid any confusion
const SHORT BROWSER_FORWARD = 100;
const SHORT BROWSER_BACK = 101;
const SHORT BROWSER_STOP = 102;
const SHORT BROWSER_RELOAD = 103;

// reply event actions from server
const LONG REPLY_REDIRECT = 1;
const LONG REPLY_CLOSE_WINDOW = 2;

//cancel parameter for playlist manager (py)
const char* const CANCEL_BROADCAST = "params=cancel_broadcast";

