#pragma once
#include "log4cplus/Logger.h"
//#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/LoggingTimer.h"
#include "KEP/Util/KEPUtil.h"

#undef LOG_TIMINGS
#ifdef LOG_TIMINGS
#define START_TIMING(timingLogger, msg) LoggingTimer _lgt_(timingLogger, msg);
#else
#define START_TIMING(timingLogger, msg) Timer _lgt_
#endif
