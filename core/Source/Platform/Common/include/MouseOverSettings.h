///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\KEPUtil\Helpers.h"

#include "common\include\ObjectType.h"

namespace KEP {

// Mouse Over Settings
struct MOS {
	// DRF - MouseOverSetting Mode Bitmap (also update CommonFunctions.lua)
	enum MODE {
		MODE_NONE = 0,
		MODE_LIGHTEN = 1,
		MODE_OUTLINE = 2,
		MODE_CURSOR = 4
	};

	// DRF - MouseOverSetting Category Bitmap
	enum CATEGORY {
		CATEGORY_NONE = 0,
		CATEGORY_ALL = 0xffffffff
	};

	int objId;
	ObjectType objType;
	CURSOR_TYPE cursorType;
	std::string toolTipText;
	MODE mode;
	CATEGORY category;

	MOS() :
			objId(0), objType(ObjectType::NONE), cursorType(CURSOR_TYPE_NONE), mode(MODE_NONE), category(CATEGORY_NONE) {}

	std::string ToStr() const {
		std::string str;
		StrBuild(str, "MOS<"
						  << ObjTypeToStr(objType) << ":" << objId
						  << " cat=x" << std::hex << category
						  << " mode=x" << std::hex << mode
						  << " tooltip='" << toolTipText << "'"
						  << " cursor=" << (int)cursorType
						  << ">");
		return str;
	}

	bool objValid() const {
		return (objType != ObjectType::NONE);
	}

	bool modeLighten() const {
		return (mode & MODE_LIGHTEN) != 0;
	}

	bool modeOutline() const {
		return (mode & MODE_OUTLINE) != 0;
	}

	bool modeCursor() const {
		return (mode & MODE_CURSOR) != 0;
	}
};

} // namespace KEP
