///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "Common/include/IKEPConfig.h"
#include "Common/include/KEPCommon.h"

class TiXmlHandle;
class TiXmlDocument;
class TiXmlElement;

namespace KEP {

/*
	Wrapper over an XML config-type file
	Get and Set fns are used for getting and setting values in the XML
	valueName may have nested element separated by /, e.g. A/B/C which 
	translates to 
	<rootNode>
	  <A>
	    <B>
	      <C>

	Most Get/Set fns do one element.  Use append to add more nodes of the
	same name, instead of replacing them.  Use GetFirst/Next to get multiples
*/
class __declspec(dllexport) KEPConfig : public IKEPConfig {
public:
	KEPConfig();

	// IKEPConfig Overrides
	virtual bool Print() override;
	virtual bool New(const char* rootNodeName, const char* rootAttribute = 0, const char* rootAttributeValue = 0) override;
	virtual bool Parse(const char* buffer, const char* rootNodeName, const char* rootAttribute = 0, const char* rootAttributeValue = 0) override;
	virtual bool Load(const std::string& fname, const char* rootNodeName, const char* rootAttribute = 0, const char* rootAttributeValue = 0) override;
	virtual bool Save() override;
	virtual bool SaveAs(const std::string& fname) override;
	virtual bool IsDirty() const override { return m_dirty; }
	virtual bool SetDirty(bool d) override { m_dirty = d; return true; }
	virtual bool IsLoaded() const override { return m_loaded; }
	virtual bool ValueExists(const char* valueName) const override;
	virtual bool GetValue(const char* valueName, int& ret, int defaultValue = 0) const override;
	virtual bool GetValue(const char* valueName, std::string& ret, const char* defaultValue = "") const override;
	virtual bool GetValue(const char* valueName, bool& ret, bool defaultValue = false) const override;
	virtual bool GetValue(const char* valueName, double& ret, double defaultValue = 0.0) const override;
	virtual bool SetValue(const char* valueName, int value, bool append = false) override;
	virtual bool SetValue(const char* valueName, const char* value, bool append = false) override;
	virtual bool SetValue(const char* valueName, bool value, bool append = false) override;
	virtual bool SetValue(const char* valueName, double value, bool append = false) override;

	bool RemoveValue(const char* valueName);

	TiXmlHandle GetElement(const char* valueName) const;

	bool GetFirstValue(TiXmlHandle& h, const char* valueName, std::string& ret, const char* defaultValue = "");
	bool GetNextValue(TiXmlHandle& h, std::string& ret);

	bool GetFirstValue(TiXmlHandle& h, const char* valueName, int& ret, int defaultValue = 0);
	bool GetNextValue(TiXmlHandle& h, int& ret);

	bool GetFirstValue(TiXmlHandle& h, const char* valueName, double& ret, double defaultValue = 0.0);
	bool GetNextValue(TiXmlHandle& h, double& ret);

	bool GetFirstValue(TiXmlHandle& h, const char* valueName, bool& ret, bool defaultValue = false);
	bool GetNextValue(TiXmlHandle& h, bool& ret);

	virtual ~KEPConfig();

protected:
	TiXmlDocument& impl() { return *m_dom; }
	friend class TIXMLConfig;

private:
	bool m_loaded;
	bool m_dirty;
	TiXmlElement* m_root;
	TiXmlDocument* m_dom;

	bool ParsePrivate(const char* rootNodeName, const char* rootAttribute, const char* rootAttributeValue);
};

inline bool KEPConfig::GetFirstValue(TiXmlHandle& h, const char* valueName, int& ret, int defaultValue) {
	std::string s;
	bool bRet = GetFirstValue(h, valueName, s, "");
	if (!bRet)
		ret = atol(s.c_str());
	else
		ret = defaultValue;
	return bRet;
}

inline bool KEPConfig::GetNextValue(TiXmlHandle& h, int& ret) {
	std::string s;
	bool bRet = GetNextValue(h, s);
	if (bRet)
		ret = atol(s.c_str());
	return bRet;
}

inline bool KEPConfig::GetFirstValue(TiXmlHandle& h, const char* valueName, double& ret, double defaultValue) {
	std::string s;
	bool bRet = GetFirstValue(h, valueName, s, "");
	if (!bRet)
		ret = atof(s.c_str());
	else
		ret = defaultValue;
	return bRet;
}

inline bool KEPConfig::GetNextValue(TiXmlHandle& h, double& ret) {
	std::string s;
	bool bRet = GetNextValue(h, s);
	if (bRet)
		ret = atof(s.c_str());
	return bRet;
}

inline bool KEPConfig::GetFirstValue(TiXmlHandle& h, const char* valueName, bool& ret, bool defaultValue) {
	std::string s;
	bool bRet = GetFirstValue(h, valueName, s, "");
	if (!bRet)
		ret = s == "1";
	else
		ret = defaultValue;
	return bRet;
}

inline bool KEPConfig::GetNextValue(TiXmlHandle& h, bool& ret) {
	std::string s;
	bool bRet = GetNextValue(h, s);
	if (bRet)
		ret = s == "1";
	return bRet;
}

} // namespace KEP
