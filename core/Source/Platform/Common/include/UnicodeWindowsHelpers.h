///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Unicode.h"

#include "LogHelper.h"

namespace KEP {

inline int MessageBoxUtf8(HWND hwnd, const char* pszText, const char* pszCaption, UINT type) {
	return MessageBoxW(hwnd, Utf8ToUtf16(pszText).c_str(), Utf8ToUtf16(pszCaption).c_str(), type);
}

///////////////////////////////////////////////////////////////////////////////
// CreateProcess()
///////////////////////////////////////////////////////////////////////////////

inline bool CreateProcess(
	_In_opt_ LPCWSTR lpApplicationName,
	_In_opt_ LPCWSTR lpCommandLine, // drf - modified const
	_In_opt_ LPSECURITY_ATTRIBUTES lpProcessAttributes,
	_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
	_In_ BOOL bInheritHandles,
	_In_ DWORD dwCreationFlags,
	_In_opt_ LPVOID lpEnvironment,
	_In_opt_ LPCWSTR lpCurrentDirectory,
	_In_ LPSTARTUPINFOW lpStartupInfo,
	_Out_ LPPROCESS_INFORMATION lpProcessInformation) {
	std::wstring commandLineNonConst = SafeStr(lpCommandLine); // win32 modifies this!
	LPWSTR lpCommandLineNonConst = const_cast<LPWSTR>(lpCommandLine ? commandLineNonConst.c_str() : NULL);
	if (!::CreateProcessW(
			lpApplicationName,
			lpCommandLineNonConst, // win32 modifies this!
			lpProcessAttributes,
			lpThreadAttributes,
			bInheritHandles,
			dwCreationFlags,
			lpEnvironment,
			lpCurrentDirectory,
			lpStartupInfo,
			lpProcessInformation)) {
		LogInstance("Exception");
		LogError("::CreateProcess() FAILED - errCode=" << GetLastError()
													   << " '" << Utf16ToUtf8(SafeStr(lpApplicationName)) << Utf16ToUtf8(SafeStr(lpCommandLine)) << "'");
		return false;
	}
	return true;
}

inline bool CreateProcess(
	_In_opt_ LPCSTR lpApplicationName,
	_In_opt_ LPCSTR lpCommandLine, // drf - modified const
	_In_opt_ LPSECURITY_ATTRIBUTES lpProcessAttributes,
	_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
	_In_ BOOL bInheritHandles,
	_In_ DWORD dwCreationFlags,
	_In_opt_ LPVOID lpEnvironment,
	_In_opt_ LPCSTR lpCurrentDirectory,
	_In_ LPSTARTUPINFOW lpStartupInfo,
	_Out_ LPPROCESS_INFORMATION lpProcessInformation) {
	auto appNameW = Utf8ToUtf16(lpApplicationName);
	auto cmdLineW = Utf8ToUtf16(lpCommandLine);
	auto curDirW = Utf8ToUtf16(lpCurrentDirectory);
	return ::KEP::CreateProcess(
		static_cast<LPCWSTR>(lpApplicationName ? appNameW.c_str() : NULL),
		static_cast<LPCWSTR>(lpCommandLine ? cmdLineW.c_str() : NULL),
		lpProcessAttributes,
		lpThreadAttributes,
		bInheritHandles,
		dwCreationFlags,
		lpEnvironment,
		static_cast<LPCWSTR>(lpCurrentDirectory ? curDirW.c_str() : NULL),
		lpStartupInfo,
		lpProcessInformation);
}

inline bool CreateProcess(const std::wstring& cmdLine, int timeoutMs = 5000) {
	// Create Process System Call Wrapper
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	bool ok = (TRUE == ::KEP::CreateProcess(
						   NULL,
						   static_cast<LPCWSTR>(cmdLine.c_str()),
						   NULL,
						   NULL,
						   FALSE,
						   HIGH_PRIORITY_CLASS,
						   NULL,
						   NULL,
						   &si,
						   &pi));
	if (!ok) {
		LogInstance("Exception");
		LogError("KEP::CreateProcess() FAILED - '" << Utf16ToUtf8(cmdLine) << "'");
	} else {
		WaitForSingleObject(pi.hProcess, timeoutMs);
	}
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	return ok;
}

inline bool CreateProcess(const std::string& cmdLine, int timeoutMs = 5000) {
	return ::KEP::CreateProcess(Utf8ToUtf16(cmdLine), timeoutMs);
}

} // namespace KEP
