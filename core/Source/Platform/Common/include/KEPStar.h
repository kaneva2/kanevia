///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "Common/include/NetworkCfg.h"
#include "Common/KEPUtil/Helpers.h"

/// Star Structure
struct StarStruct {
	std::string id; ///< star identifier eg. '3296'
	std::string label; ///< star label postfix
	std::string wokUrl; ///< star wok url
	std::string patchUrl; ///< star path url
	std::string patchVersion; ///< star version
};

/// Star Enumeration
enum STAR {
	STAR_PRO, ///< star production
	STAR_PRE, ///< star preview
	STAR_DEV, ///< star developer
	STARS
};

/// Star Constants (patch version is only used while debugging instead of -k option)
static const StarStruct g_stars[STARS] = {
	{ /// STAR_PRO
		NET_STAR_PRO,
		"",
		NET_KGP_ENV(NET_ENV_PRO),
		NET_PATCH_URL,
		NET_PATCH_VERSION },
	{ /// STAR_PRE
		NET_STAR_PRE,
		" (Pre)",
		NET_KGP_ENV(NET_ENV_PRE),
		NET_PATCH_URL,
		NET_PATCH_VERSION },
	{ /// STAR_DEV
		NET_STAR_DEV,
		" (Dev)",
		NET_KGP_ENV(NET_ENV_DEV),
		NET_PATCH_URL,
		NET_PATCH_VERSION }
};

inline STAR Star(const std::string& starId) {
	for (auto i = 0; i < STARS; ++i)
		if (starId == g_stars[i].id)
			return (STAR)i;
	return STAR_PRO;
}

inline bool StarIsDev(const std::string& starId) {
	return (Star(starId) == STAR_DEV);
}

inline bool StarIsPreview(const std::string& starId) {
	return (Star(starId) == STAR_PRE);
}

inline bool StarIsProduction(const std::string& starId) {
	return (Star(starId) == STAR_PRO);
}

inline std::string StarLabel(const std::string& starId) {
	return g_stars[Star(starId)].label;
}

inline std::string StarVersion(const std::string& starId) {
#ifdef _DEBUG
	return g_stars[Star(starId)].patchVersion;
#else
	std::string starVer;
	starVer.append(VersionApp(false));
	starVer.append("_a");
	return starVer;
#endif
}

inline std::string StarWokUrl(const std::string& starId) {
	return g_stars[Star(starId)].wokUrl;
}

inline std::string StarPatchUrl(const std::string& starId) {
	return g_stars[Star(starId)].patchUrl;
}
