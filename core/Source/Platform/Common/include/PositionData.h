/******************************************************************************
 PositionData.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
#include "d3d9.h"

class PositionData {
public:
	PositionData() { cb = sizeof(PositionData); }
	static const BYTE AI_CONTROLLED = 1;
	static const BYTE IN_GAME = 2;

	ULONG cb;
	BYTE flags; // in game, ai
	D3DVECTOR pos;
	ULONG worldId;
	char name[50];
	char world[50];
	LONG energy;
};

class PositionHeader {
public:
	static const ULONG FILE_VER = 1;
	PositionHeader() {
		ZeroMemory(this, sizeof(PositionHeader));
		version = FILE_VER;
	}

	ULONG version;
	char timestamp[15]; // yyyymmddhhmmss
	char serverName[50];
	ULONG count;
};
