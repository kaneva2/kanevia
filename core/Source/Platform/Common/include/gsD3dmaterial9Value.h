#pragma once
#include "KEP/Util/KEPUtil.h"
#include "GetSet.h"
#include "KEPHelpers.h"
#include <d3d9.h>

namespace KEP {

class KEPUTIL_EXPORT gsD3dmaterial9Value : public GetSet {
public:
	gsD3dmaterial9Value(D3DMATERIAL9 *v, WORD parentIndent, WORD parentGroupTitleId) :
			m_material(v),
			m_indent(parentIndent + 1),
			m_groupTitleId(parentGroupTitleId) {}

	D3DMATERIAL9 *material() { return m_material; }

	DECLARE_GETSET
private:
	D3DMATERIAL9 *m_material;
	WORD m_indent;
	UINT m_groupTitleId;
};

} // namespace KEP
