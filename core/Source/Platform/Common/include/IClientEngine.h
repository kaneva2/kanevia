///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"
#include "KEPConstants.h"

#include <boost/optional.hpp>
#include <string>
#include <map>
#include <vector>

#include "IGame.h"

#include "chattype.h"
#include "inventorytype.h"
#include "Core/Math/KEPMath.h"
#include "DynamicObjTypes.h"
#include "Core/Util/HighPrecisionTime.h"
#include "MouseOverSettings.h"
#include "DownloadPriority.h"
#include "KEPPhysics/Declarations.h"
#include "Material.h"
#include "PrimitiveGeomArgs.h"

#include "Common\include\CoreStatic\AnimSettings.h"
#include "Common\include\CoreStatic\DynamicObjectSubListId.h"
#include "Common\include\CoreStatic\TriggerEnums.h"

#include "event\Events\AttribEvents.h"

#include "client\ClientMetrics.h"

typedef ULONG KEP_NETID;

#define CLIENTENGINE_WEBCALLER_ID "ClientEngine"

struct IDirect3DDevice9;
struct IDirect3DTexture9;
struct D3DXCOLOR;

typedef std::vector<std::pair<std::string, std::string>> ValueList;

// Old IKEPClientEngine

namespace KEP {
typedef int EffectHandle;
class IClientBladeFactory;
class LegacyModelProxy;
class IAssetFactory;
class IEvent;
class IScriptVM;
class IDispatcher;
class IExternalMesh;
class IGetSet;
class IKEPFile;
class IKEPConfig;
class Grid;
class ReDynamicGeom;
class ReD3DX9DeviceState;
class IUGCManager;
struct TextureDef;
class IResource;
class DownloadHandler;
enum class ResourceType;
enum class WorldRenderStyle;
enum class WorldRenderOptions;
class FrameTimeProfiler;
class DynamicObjectRM;
class EquippableRM;
class SoundRM;
class StreamableDynamicObject;
class CCameraObj;
class CCameraObjList;
class CDynamicPlacementObj;
class CEXMeshObj;
class CMovementObj;
class CMovementObjList;
class ActorDatabase;
class TextureDatabase;
class CharConfig;
class CharConfigItem;
class RuntimeSkeleton;
class CTextureEX;
class SoundPlacement;
class CExplosionObjList;
class CMissileObjList;
class ITexture;
class CFrameObj;
class CStateManagementObj;
class CReferenceObjList;
class CShaderObjList;
class CSkillObjectList;
class CSoundBankList;
class CTextRenderObj;
class CTriggerObject;
class CViewportObj;
class ContentService;
class ContentMetadata;
class ControlDBList;
class MediaParams;
class MediaRadius;
class ParticleRM;
class RuntimeCameras;
enum class CameraAction;

// DRF - Enumerated Developer Mode
enum DEV_MODE {
	DEV_MODE_OFF = 0, // normal user runtime mode
	DEV_MODE_ON = 1, // developer playground runtime mode
	DEV_MODES
};

// There are a number of places in the code where it would be nice to have access to the
//  ClientEngine class, but have no other way of getting to it.  Instead of having to
//  #include "ClientEngine.h" and the gazillion header files that goes along with it
//  and all their dependencies, this is a mostly pure virutal interface to it with
//  a simple singleton pattern.  It just saves the last ClientEngine pointer that was
//  instantiated, and you can access it by #including "IClientEngine.h" and using
//  IClientEngine::Instance()->
//  If you need further access to the engine's functiaonlity or data, just put
//  functions in here and implement them in ClientEngine.cpp (or similiar).

class IClientEngine : public IGame {
public:
	enum MovementRestriction {
		RestrictMovement_None = 0x00000000,
		RestrictMovement_MouseLook = 0x01,
		RestrictMovement_Forward = 0x02,
		RestrictMovement_Backward = 0x04,
		RestrictMovement_Side = 0x08,
		RestrictMovement_Turn = 0x10,
		RestrictMovement_Jump = 0x20,
		RestrictMovement_All = 0xffffffff
	};

	enum MovementRestrictionSource {
		MR_SRC_DEATH,
		MR_SRC_MENU,
		MR_SRC_PATHFINDER,
		MR_SRC_LOOKAT,
		MR_SRC_DDR,
		MR_SRCS
	};

	enum {
		eTextureDatabase_Static,
		eTextureDatabase_Dynamic,
		eTextureDatabase_All
	};

	static IClientEngine* Instance(IClientEngine* pICE = nullptr) {
		static IClientEngine* s_pICE = nullptr;
		if (pICE)
			s_pICE = pICE;
		return s_pICE;
	}

	//---------------------------------------------------------------------
	// DRF - App API (implimented in ClientEngineApp)
	//---------------------------------------------------------------------
	virtual std::string AppVersion() const = 0;
	virtual std::string AppStarPath() const = 0;
	virtual std::string AppStarId() const = 0;
	virtual std::string AppWokUrl() const = 0;
	virtual std::string AppTTLang() const = 0;
	virtual bool AppIsProduction() const = 0;
	virtual DEV_MODE AppDevMode() const = 0;
	virtual int AppTestGroup() const = 0;

	//---------------------------------------------------------------------
	// DRF - Log API
	//---------------------------------------------------------------------
	virtual void LogMemory() const = 0;
	virtual void LogWebCalls() const = 0;
	virtual void LogResources(int resources) = 0;
	virtual void LogMetrics() = 0;

	//---------------------------------------------------------------------
	// DRF - WebCall API
	// Provides both asynchronous and synchronous single thread low priority
	// webcalls using the WebCaller given by id.  If no WebCaller is found
	// with matching id a new one is created.
	//---------------------------------------------------------------------
	virtual bool WebCallDisabled(const std::string& id = CLIENTENGINE_WEBCALLER_ID) = 0;
	virtual bool WebCall(const std::string& url, const std::string& id = CLIENTENGINE_WEBCALLER_ID) = 0;
	virtual bool WebCallAndWaitOk(const std::string& url, const std::string& id = CLIENTENGINE_WEBCALLER_ID) = 0;

	//---------------------------------------------------------------------
	// DRF - ClientMetrics API
	// Provides basic metrics accounting for the client.
	// The ProgressMenu disables metrics accounting while open.
	//---------------------------------------------------------------------
	virtual void MetricsEnable(bool enable) = 0;
	virtual void MetricsLog() = 0;
	virtual bool MetricsFps(ClientMetrics::FPS_ID fpsId, double& fps) const = 0;

	virtual float RenderMetricsFps(bool filter) const = 0;

	// DRF - Added
	virtual bool SendSystemServiceEvent(const std::string& userId) = 0;

	// DRF - Added
	virtual std::string Encrypt(const std::string& s) const = 0;
	virtual std::string Decrypt(const std::string& s) const = 0;

	virtual void FlagWindowRefresh() = 0;

	virtual size_t MouseClicks() const = 0;

	virtual int GetGameId() const = 0;
	virtual int GetParentGameId() const = 0;
	virtual int GetZoneInstanceId() const = 0;
	virtual ULONG GetZoneIndexAsULong() const = 0;
	virtual ULONG GetZoneIndexType() const = 0;
	virtual std::string GetZoneFileName() = 0;

	virtual bool WorldPassGroupAP() = 0;

	virtual bool MyPassGroupAP() = 0;
	virtual bool MyPassGroupVIP() = 0;
	virtual bool MyPassGroupGM() = 0;
	virtual bool MyPassGroupOWN() = 0;
	virtual bool MyPassGroupMOD() = 0;

	virtual bool IsShutdown() = 0;

	virtual void GetViewMatrix(Matrix44f* out) const = 0;
	virtual void GetProjectionMatrix(Matrix44f* out) const = 0;

	virtual IDirect3DDevice9* GetD3DDevice() = 0;

	virtual IDirect3DTexture9* GetD3DTextureIfLoadedOrQueueToLoad(const std::string& texFileName) = 0;

	virtual IDirect3DTexture9* GetD3DTextureIfLoadedOrQueueToLoad(DWORD textureKey) = 0;
	virtual ITexture* GetTextureIfLoadedOrQueueToLoad(DWORD texKey) = 0;

	virtual bool IsFullScreen() = 0;
	virtual void SetFullScreen(bool bFullScreen) = 0;
	virtual BOOL WindowMinimized() = 0;

	virtual TextureDatabase* GetTextureDataBase() const = 0;

	virtual IAssetFactory* GetAssetFactory() = 0;

	virtual void KeyCapturedTriggered(bool trigger) = 0;

	virtual void MenuEventTriggered(bool trigger) = 0;
	virtual void WidgetEventTriggered(bool trigger) = 0;

	virtual IClientBladeFactory* GetBladeFactory() = 0;
	virtual const IClientBladeFactory* GetBladeFactory() const = 0;
	virtual bool DownloadPatchFile(const char* exgFileName, IEvent* e, FileSize fileSize) = 0;
	virtual bool DownloadPatchFile(const char* exgFileName, const char* folder, IEvent* e, FileSize fileSize) = 0;
	virtual bool TestPatchFileForUpdate(const char* filename) = 0;
	virtual bool ArmPreLoadProgress(IEvent* e) = 0;

public:
	//////////////////////////////////////////////////////////////////////
	// DRF - Arm/Disarm Functions
	// MovementObj=NULL Assumes Local Avatar
	// GetSet Variants Are For Script Glue Pointer Conversion Only
	///////////////////////////////////////////////////////////////////////

	virtual std::vector<GLID> GetPlayerArmedItems(CMovementObj* pMO = NULL) = 0;
	virtual std::vector<GLID> GetPlayerArmedItemsGS(IGetSet* pGS_MO = NULL) = 0;

	// Arm Functions
	virtual bool ArmInventoryItem_MO(const GLID& glid, CMovementObj* pMO = NULL) = 0;
	virtual bool ArmInventoryItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL) = 0;
	virtual bool ArmEquippableItem_MO(const GLID& glid, CMovementObj* pMO = NULL, int material = 0) = 0;
	virtual bool ArmEquippableItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL, int material = 0) = 0;
	virtual bool ArmEquippableItem_GS_MGO(const GLID& glid, IGetSet* pGS_MGO = NULL, int material = 0) = 0;
	virtual bool ArmEquippableItem_DPO(const GLID& glid, CDynamicPlacementObj* pDPO, const boost::optional<std::string>& strBoneOverride) = 0;

	// Disarm Functions
	virtual bool DisarmInventoryItem_MO(const GLID& glid, CMovementObj* pMO = NULL) = 0;
	virtual bool DisarmInventoryItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL) = 0;
	virtual bool DisarmEquippableItem_MO(const GLID& glid, CMovementObj* pMO = NULL) = 0;
	virtual bool DisarmEquippableItem_GS_MO(const GLID& glid, IGetSet* pGS_MO = NULL) = 0;
	virtual bool DisarmEquippableItem_DPO(const GLID& glid, CDynamicPlacementObj* pDPO) = 0;

public:
	// To clarify nomenclature, a mesh is stateless
	//  A model is an instance of a mesh with it's own transform
	virtual bool HitBoundingBox(const Vector4f& minB, const Vector4f& maxB, const Vector3f& origin, const Vector3f& dir, Vector3f& coord, Vector3f& normal, BOOL inOut = 0) = 0; // Woo's method ray bs AABB intersector
	virtual std::tuple<bool, Vector3f> DynamicObjectCollisionTest(int iPlacementId, const Vector3f& direction, const boost::optional<Vector3f>& startPos) = 0; // How far the placement can move until it collides with something. -1 on no collision.

	virtual bool IsWorldObjectCustomizableByIdentity(int identity) = 0;

	virtual bool IsDynamicObjectCustomizable(int placementId) = 0;

	virtual void SetDynamicObjectRM(DynamicObjectRM* sys) = 0;
	virtual DynamicObjectRM* GetDynamicObjectRM() = 0;
	virtual SoundRM* GetSoundRM() = 0;
	virtual EquippableRM* GetEquippableRM() = 0;

	virtual CDynamicPlacementObj* GetDynamicObject(int placementId) = 0;
	virtual const CDynamicPlacementObj* GetDynamicObject(int placementId) const = 0;

	virtual int PlaceLocalDynamicObject(int globalId, const Vector3f* positionOverride = NULL, const Vector3f* directionOverride = NULL, const Vector3f* slideOverride = NULL, float scaleFactor = 1.0f) = 0; // Return placement ID
	virtual ActorDatabase* GetActorDatabase() = 0;

	virtual bool CanDynamicObjectSupportAnimation(int placementId) = 0;

	virtual void SetAnimationOnDynamicObject(int placementId, const AnimSettings& animSettings) = 0;
	virtual bool GetDynamicObjectAnimation(int placementId, AnimSettings& animSettings, std::string& name) = 0;

	virtual bool isDynamicObjectInited(int placementId) = 0;

	virtual bool GetDynamicObjectInfo(
		int placementId,
		std::string& name,
		std::string& desc,
		bool& mediaSupported,
		bool& isAttachable,
		bool& isInteractive,
		bool& isLocal,
		bool& isDerivable,
		int& assetId,
		int& friendId,
		std::string& customTexture,
		int& globalId,
		int& invSubType,
		std::string& textureUrl,
		int& gameItemId,
		int& playerId,
		DYN_OBJ_TYPE& objType) = 0;

	virtual void UpdateDynamicObjectInSubList(CDynamicPlacementObj* pDO, DynObjSubListId sublistId) = 0; // Object belongs in a particular sublist.
	virtual void UpdateDynamicObjectLight(CDynamicPlacementObj* pDO) = 0; // Object light ownership status has changed.

	virtual bool GetDynamicObjectBoundingBox(int placementId, float* x1, float* y1, float* z1, float* x2, float* y2, float* z2) = 0;
	virtual bool DynamicObjectMakeTranslucent(int placementId) = 0;
	virtual bool EnableDynamicObjectBoundingBoxRender(int placementId, bool enable) = 0;

	virtual bool CanAutoLogin() const = 0;

	virtual int GetNumPlayers() = 0; //number of players

	virtual bool GetMyPosition(float& x, float& y, float& z, float& rotDeg) = 0;

	virtual bool SetMyLookAt(float x, float y, float z) = 0;

	virtual void EnableLightForPosition(Vector3f& position) = 0;

	virtual unsigned short GetControlsUpdatedEventID() const = 0;
	virtual unsigned short GetDownloadZoneEventId() const = 0; // Can't think of any other way to get that member var to glue. --Jonny
	virtual unsigned short GetDownloadLooseTextureEventId() const = 0;
	virtual unsigned short GetRequestSoundItemDataId() const = 0;
	virtual unsigned short GetDialogEventId() const = 0;

	virtual HWND GetWindowHandle() = 0;

	virtual bool RegisterVMsEvents(IScriptVM* vm, const char* filename) = 0;
	virtual bool UnRegisterVMsEvents(IScriptVM* vm) = 0;

	virtual IDispatcher* GetDispatcher() = 0;

	virtual unsigned int getMovementRestriction() const = 0;
	virtual void setMovementRestriction(const MovementRestrictionSource& source, const MovementRestriction& restriction, bool enable) = 0;

	//set whether or not a menu is open that should supress selection
	virtual void SetMenuSelectMode(BOOL mode = FALSE) = 0;

	virtual BOOL AddTextureToDatabase(CEXMeshObj* meshObj, BOOL remesh = TRUE, DWORD maxTex = 8) = 0;
	virtual CTextureEX* GetTextureFromDatabase(DWORD texNameHash) = 0;

	virtual void OnTextureDestroy(CTextureEX* pTex) = 0;

	// DRF - ED-7035 - Investigate texture memory impact
	virtual void OnTextureUnload(CTextureEX* pTex) = 0;

	virtual bool DepositItemToBank(int glID, int nQuantity, short invType) = 0;
	virtual bool WithdrawItemFromBank(int glID, int nQuantity, short invType) = 0;

	virtual BOOL BuyItemFromStore(int glID, int nQuantity, int nCurrencyType) = 0;
	virtual BOOL SellItemToStore(int glID, int nQuantity) = 0;

	virtual bool SendChatMessage(eChatType chatType, const std::string& msg, bool fromIMClient = false) = 0;
	virtual bool SendChatMessageBroadcast(eChatType chatType, const std::string& msg, const char* toDistribution, const char* fromRecipient) = 0;

	virtual BOOL WorldToScreen(Vector3f wldPosition, Vector3f* scrPosition) = 0;

	virtual bool PlayBySoundID(int id, BOOL looping, double x, double y, double z, float soundFactor, float clipRadius) = 0;
	virtual void PlayBySoundID(int id, BOOL looping) = 0;

	virtual void GetMyInfo(std::vector<std::string>& pInfo) = 0;

	virtual IGetSet* GetMovementObjectMeAsGetSet() = 0;
	virtual IGetSet* GetMovementObjectByNetIdAsGetSet(int netId) = 0;
	virtual IGetSet* GetMovementObjectByNameAsGetSet(const std::string& name) = 0;

	virtual std::string GetMyTitle() = 0;

	virtual bool Login(const std::string& email, const std::string& password, bool promptAllowAccess = true) = 0;

	virtual void Logout() = 0;

	virtual bool ChildGameWebCallAllowed(const std::string& url) = 0;
	virtual void Quit() = 0;
	virtual void ClearChatFocus() = 0;

	virtual bool PlayCharacter(int index, const char* startUrl, bool reconnect = false) = 0;

	virtual bool DeleteCharacter(int index) = 0;

	virtual bool CreateCharacterOnServerFromMenuGameObject(
		IGetSet* pGS_MGO,
		int spawnIndex,
		const std::string& name,
		int nTeam,
		int scaleX, int scaleY, int scaleZ) = 0;

	virtual bool CreateCharacterOnServerFromPlayer(
		IGetSet* pGS_MO,
		int spawnIndex,
		const std::string& name,
		int nTeam,
		int scaleX, int scaleY, int scaleZ) = 0;

	// DRF - Added
	virtual IGetSet* SpawnNPC(
		int siType // SERVER_ITEM_TYPE
		,
		const std::string& name, CharConfig* pCharConfig, const std::vector<GLID>& glidsArmed, int dbIndex, KEP_NETID netId, const Vector3f& pos, double rotDeg, const GLID& glidAnimSpecial, bool networkEntity) = 0;
	virtual bool DeSpawnNPC(KEP_NETID netTraceId) = 0;

	virtual void SpawnToRebirthPoint() = 0;
	virtual void RequestPlayerRespawn() = 0; // #MLB_ED_7947 Define interface for new event RequestPlayerRespawnEvent

	virtual std::string ScreenshotCapture(const RECT& rect, std::string destFile = "", char* folderOverride = nullptr) = 0;
	virtual bool ScreenshotIsCapturing() const = 0;

	virtual IGetSet* AddMenuGameObjectAsGetSet(int game_object_index, int animation_index) = 0;
	virtual IGetSet* AddMenuGameObjectByEquippableGlidAsGetSet(long long glid) = 0;
	virtual IGetSet* AddMenuDynamicObjectByPlacementAsGetSet(int placementId) = 0;

	virtual void RemoveMenuGameObjectAsGetSet(IGetSet* pGS_MGO) = 0;
	virtual std::string MenuGameObjectScreenshotCaptureAsGetSet(IGetSet* pGS_MGO, const std::string& fileName) = 0;
	virtual void MenuGameObjectMoveToTopAsGetSet(IGetSet* pGS_MGO) = 0;
	virtual void MenuGameObjectMoveToBottomAsGetSet(IGetSet* pGS_MGO) = 0;
	virtual void MenuGameObjectSetDOZoomFactorAsGetSet(IGetSet* pGS_MGO, float zoomFactor) = 0;
	virtual void MenuGameObjectSetViewportBackgroundColorAsGetSet(IGetSet* pGS_MGO, int r, int g, int b) = 0;
	virtual void MenuGameObjectSetAnimationTimeAsGetSet(IGetSet* pGS_MGO, TimeMs animTimeMs) = 0;
	virtual void MenuGameObjectSetAnimationByTypeAsGetSet(IGetSet* pGS_MGO, eAnimType animType, int animVer) = 0;
	virtual bool MenuGameObjectSetAnimationAsGetSet(IGetSet* pGS_MGO, const AnimSettings& animSettings) = 0;

	virtual bool SubmitLocalDynamicObjectByPlacementId(long placementId, IEvent* submissionFeedbackEvent) = 0;

	virtual bool UseInventoryItem(const GLID& glid, short invType = IT_NORMAL) = 0;
	virtual bool UseInventoryItemOnTarget(const GLID& glid, int objType, int objId, short invType = IT_NORMAL) = 0;

	virtual bool RemoveInventoryItem(const GLID& glid, int qty = -1, short invType = IT_NORMAL) = 0;
	virtual bool RemoveBankItem(const GLID& glid, int qty = -1, short invType = IT_NORMAL) = 0;

	virtual bool SetMenuGameObjectEquippableAltCharConfig(IGetSet* pGS_MGO, const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId) = 0;
	virtual bool SetPlayerEquippableAltCharConfig(IGetSet* pGS_MO, const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId) = 0;
	virtual bool SetPlayerEquippableAnimation(IGetSet* pGS_MO, const GLID& accessoryGLID, const GLID& animationGLID, bool updateServer) = 0;
	virtual bool SetPlayerCustomTextureOnAccessoryLocal(RuntimeSkeleton* pRuntimeSkeleton, int glid, const std::string& textureUrl) = 0;
	virtual bool SetPlayerCustomTextureOnAccessoryLocal(IGetSet* pGS_MO, int glid, const std::string& textureUrl) = 0;

	virtual int GetCharacterMaxCount() const = 0;

	virtual bool GetMyPlayerCharConfigBaseMeshIds(std::vector<std::string>& meshIds) = 0;
	virtual bool GetMyPlayerCharConfigBaseMatlIds(std::vector<std::string>& matlIds) = 0;

	virtual bool SetMenuGameObjectAltCharConfig(IGetSet* pGS_MGO, int baseSlot, int meshId, int matlId = 0) = 0;
	virtual bool SetPlayerAltCharConfig(IGetSet* pGS_MO, int baseSlot, int meshId, int matlId = 0) = 0;
	virtual bool SetPlayerActorType(IGetSet* pGS_MO, int actorIndex) = 0;
	virtual bool ScaleRuntimePlayer(IGetSet* pGS_MO, float xFactor, float yFactor, float zFactor) = 0;
	virtual Vector3f GetRuntimePlayerScale(IGetSet* pGS_MO) = 0;
	virtual int GetPlayerCharConfigMeshId(IGetSet* pGS_MO, int configGlid, int configSlot) = 0;
	virtual int GetPlayerCharConfigMatlId(IGetSet* pGS_MO, int configGlid, int configSlot) = 0;
	virtual bool GetPlayerAttachmentTypes(IGetSet* pGS_MO, std::vector<std::string>& attachments) = 0;

	virtual bool RequestCharacterInventory(eAttribEventType invType) = 0;

	virtual bool RequestCharacterInventoryAttachableObjects() = 0;

	virtual bool RequestPictureFrames() = 0;

	virtual bool GetControlConfiguration(std::vector<std::string>& pConfig) = 0;
	virtual bool PendingControlAssignment(int index) = 0;
	virtual bool RestoreDefaultControlConfiguration() = 0;
	virtual bool UnassignControlByIndex(int index) = 0;
	virtual bool SaveControlConfiguration() = 0;

	virtual bool GetItemInfo(const GLID& glid, std::string& texture_name, RECT& texture_coords) = 0;

	virtual void AddUGCTexture(const GLID& glid, const std::string& element_name) = 0;

	// Menus
	virtual std::string GetMenuDir() const = 0;
	virtual std::string GetMenuScriptDir() const = 0;
	virtual std::string GetTemplateMenuDir() = 0;
	virtual std::string GetTemplateScriptDir() = 0;
	virtual std::string GetAppMenuDir() = 0;
	virtual std::string GetAppScriptDir() = 0;
	virtual int GetNumMenus(int categoryMask) = 0;
	virtual std::string GetMenuByIndex(int categoryMask, int index) = 0;
	virtual int GetNumMenuScripts(int categoryMask) = 0;
	virtual std::string GetMenuScriptByIndex(int categoryMask, int index) = 0;
	virtual void ReloadMenus() = 0;

	// added for exposing engine to events
	virtual void SendInteractCommand() = 0;

	// file i/o
	virtual IKEPFile* OpenFile(const char* fname, const char* access, std::string& errMsg, const char* dir) = 0;
	virtual IKEPConfig* OpenConfig(const char* _fname, const char* rootNodeName, std::string& errMsg) = 0;

	virtual bool DeleteAsset(int type, std::string const& fileName) = 0;

	virtual bool GetBrowserPage(const std::string& url, int filter, int startIndex = 0, int maxItems = 0) = 0;

	virtual bool GetBrowserPageAsync(const std::string& url, IEvent* completionEvent = NULL, int filter = 0, int startIndex = 0, int maxItems = 0, int retries = 1, bool doAuth = true) = 0;

	virtual bool _GetBrowserPage(const std::string& url, int startIndex, int maxItems, std::string& resultText, DWORD* httpStatusCode, std::string& httpStatusDescription) = 0;

	virtual bool _GetBrowserLogin(const std::string& url, std::string& resultText, DWORD* httpStatusCode, std::string& httpStatusDescription, const std::string& gotoUrlOverride) = 0;

	virtual bool LaunchBrowser(const char* url) = 0;
	virtual HINSTANCE shellExec(const char* url, const char* verb, const char* args) = 0;

	virtual std::string GetURL(const char* urlTag, bool bReturnCompleted) = 0;

	virtual IGetSet* GetEnvironment() = 0;

	// Global Master Volume 0..100
	virtual bool SetMasterVolume(double volPct) = 0;
	virtual double GetMasterVolume() const = 0;
	virtual bool SetMute(bool bMute) = 0;
	virtual bool GetMute() const = 0;

	// Global Media Enabled
	virtual bool SetMediaEnabled(bool enabled) = 0;
	virtual bool GetMediaEnabled() const = 0;

	virtual bool StartMediaOnWorldObjectMesh(int traceId, const MediaParams& mediaParams) = 0;
	virtual bool StopMediaOnWorldObjectMesh(int traceId) = 0;

	virtual bool StartMediaOnWorldObjectMesh(const std::string& name, const MediaParams& mediaParams) = 0;
	virtual bool StopMediaOnWorldObjectMesh(const std::string& name) = 0;

	// Dynamic Object Media Params
	virtual bool SetMediaParamsOnDynamicObject(int placementId, const MediaParams& mediaParams) = 0;
	virtual bool GetMediaParamsOnDynamicObject(int placementId, MediaParams& mediaParams) = 0;

	// Dynamic Object Media Volume (-1=no change, 0=mute)
	virtual bool SetMediaVolumeOnDynamicObject(int placementId, double volPct = -1) = 0;
	virtual bool GetMediaVolumeOnDynamicObject(int placementId, double& volPct) = 0;

	// Dynamic Object Media Radius (-1=no change, 0=global)
	virtual bool SetMediaRadiusOnDynamicObject(int placementId, const MediaRadius& radius) = 0;
	virtual bool GetMediaRadiusOnDynamicObject(int placementId, MediaRadius& radius) = 0;

	// Render media trigger volume
	virtual void EnableMediaTriggerRender(bool enable) = 0;

	// Media Placements
	virtual void MediaPlacementsLog(bool verbose) const = 0;
	virtual bool MediaPlacementsDel(int placementId, bool andWait) = 0;
	virtual bool MediaPlacementsDelAll(bool andWait) = 0;

	// Flash Specific Functions
	virtual IExternalMesh* CreateFlashMesh(
		HWND parent,
		const MediaParams& mediaParams,
		uint32_t width = 0, uint32_t height = 0,
		bool loop = false,
		bool wantInput = false,
		int32_t x = 0, int32_t y = 0) = 0;
	virtual bool GetFlashMovieSize(const std::string& filePath, uint32_t& width, uint32_t& height) = 0;

	virtual bool EntityAnimationManagementCallbackForMe() = 0;

	// Assign current animation overrides the animation currently in place.
	// Set current animation uses the animation systems' override
	virtual TimeSec AssignCurrentAnimation(eAnimType animType, int animVer = ANIM_VER_ANY, bool repeat = true) = 0;

	virtual bool setMyOverrideAnimationByType(eAnimType animType, int animVer) = 0;

	virtual bool setMyOverrideAnimationSettings(const AnimSettings& animSettings) = 0;

	virtual bool getMyOverrideAnimationSettings(AnimSettings& animSettings) = 0;

	virtual bool getMyPrimaryAnimationSettings(AnimSettings& animSettings) = 0;

	virtual bool LoadAnimationOnPlayer(IGetSet* player, const GLID& animGLID) = 0;

	virtual bool GetPostureMatrices(const GLID& animGLID, TimeMs animTimeMs, std::map<std::string, Matrix44f>& posture) = 0;

	virtual void RotatePointAroundPoint(Vector3f& outPos, const Vector3f& point, const Vector3f& pivot, const Vector3f& angle) = 0;

	virtual size_t GetDynamicObjectCount() const = 0;

	virtual int GetDynamicObjectPlacementIdByPos(int pos) = 0;

	virtual std::string GetDynamicObjectName(int placementId) = 0;

	virtual int GetDynamicObjectGlid(int placementId) = 0;

	virtual bool SetDynamicObjectPosition(int placementId, const Vector3f& pos) = 0;
	virtual bool GetDynamicObjectPosition(int placementId, Vector3f& pos) = 0;
	virtual bool SetDynamicObjectRotation(int placementId, const Vector3f& rot) = 0;
	virtual bool GetDynamicObjectRotation(int placementId, Vector3f& rot) = 0;
	virtual bool GetDynamicObjectVisibility(int placementId, eVisibility& vis) = 0;
	virtual bool SetDynamicObjectVisibility(int placementId, const eVisibility& vis) = 0;
	virtual bool GetDynamicObjectAnimPosRot(int placementId, float& posx, float& posY, float& posZ, float& RotY) = 0;

	virtual bool SendRemoveDynamicObjectEvent(int placementId) = 0;

	virtual bool AddDynamicObjectPlacement(int objId, const char* type) = 0;

	virtual bool MoveDynamicObjectPlacement(int placementId, double dir, double slide, double lift) = 0;
	virtual bool MoveDynamicObjectPlacementInDirection(int placementId, double x, double y, double z, double distance) = 0;

	virtual bool DynamicObjectCollisionFilter(int placementId, bool filter) = 0;

	// Sound Placements
	virtual void SoundPlacementsLog(bool verbose) const = 0;
	virtual bool SoundPlacementsGetIds(std::vector<int>& soundPlacementIdList) = 0;
	virtual bool SoundPlacementsEnableRender(bool toRender) = 0;

	virtual SoundPlacement* SoundPlacementGet(int soundPlacementId) const = 0;

	virtual bool SoundPlacementValid(int soundPlacementId) const = 0;

	virtual bool SoundPlacementAdd(int soundPlacementId) = 0;
	virtual bool SoundPlacementDel(int soundPlacementId) = 0;

	virtual bool SoundPlacementGetInfo(int soundPlacementId, std::string& name, GLID& glid) const = 0;
	virtual bool SoundPlacementSetPosition(int soundPlacementId, const Vector3f& pos) = 0;
	virtual bool SoundPlacementGetPosition(int soundPlacementId, Vector3f& pos) const = 0;
	virtual bool SoundPlacementSetRotationRad(int soundPlacementId, float radians) = 0;
	virtual bool SoundPlacementGetRotationRad(int soundPlacementId, float& radians) const = 0;
	virtual bool SoundPlacementSetDirection(int soundPlacementId, const Vector3f& dir) = 0;
	virtual bool SoundPlacementPlaySound(int soundPlacementId, const GLID& glid) = 0;

	virtual int PlaySoundOnDynamicObject(int placementId, const GLID& glid) = 0;
	virtual bool RemoveSoundOnDynamicObject(int placementId, const GLID& glid) = 0;
	virtual int PlaySoundOnPlayer(int netId, const GLID& glid) = 0;
	virtual bool RemoveSoundOnPlayer(int netId, const GLID& glid) = 0;

	virtual bool SoundPlacementAddModifier(int soundPlacementId, int input, int output, double gain, double offset) = 0;

	virtual bool GetTransformRelativeToBone(int boneId, double x, double y, double z, double* outX, double* outY, double* outZ) = 0;

	virtual float GetMouseNormalizedX() const = 0;
	virtual float GetMouseNormalizedY() const = 0;

	virtual bool IsKeyDown(size_t keyCode) const = 0;

	virtual CCameraObjList* GetCameraList() = 0;
	virtual RuntimeCameras* GetRuntimeCameras() const = 0;

	virtual CCameraObj* GetActiveCameraForMe() const = 0;
	virtual Vector3f GetActiveCameraPositionForMe() const = 0;
	virtual Vector3f GetActiveCameraOrientationForMe() const = 0;
	virtual Vector3f GetActiveCameraUpVectorForMe() const = 0;
	virtual bool SetActiveCameraAutoAzimuthForMe(float radians) = 0;
	virtual float GetActiveCameraAutoAzimuthForMe() const = 0;
	virtual bool SetActiveCameraAutoElevationForMe(float radians) = 0;
	virtual float GetActiveCameraAutoElevationForMe() const = 0;

	virtual void SetCameraCollision(bool collision) = 0;
	virtual void SetCameraLock(bool lock) = 0;
	virtual bool GetCameraLock() const = 0;
	virtual void SetCameraMoveSpeed(double speed) = 0;
	virtual double GetCameraMoveSpeed() const = 0;

	virtual void SendCameraEvent(CCameraObj* pCO, CameraAction action, int runtime_camera_id, int runtime_viewport_index = 0) = 0;

	// Camera Script GetSet Interface
	virtual IGetSet* GetActiveCameraForMeAsGetSet() const = 0;
	virtual bool SetActiveCameraForActorByIndexOffset(IGetSet* pGS_mo, int cameraIndexOffset, bool forceChange) = 0;
	virtual int GetActiveCameraIndexForActor(IGetSet* pGS_mo) const = 0;

	// DRF - Added
	virtual bool AttachActiveViewportCameraToObject(ObjectType objType, int objId) = 0;

	virtual bool SaveDynamicObjectPlacementChanges(int placementId) = 0; //save changes made via move and rotate
	virtual bool CancelDynamicObjectPlacementChanges(int placementId) = 0; //cancel changes made via move and rotate

	virtual bool ApplyCustomTexture(int objId, int assetId, bool thumbnail = false, const char* textureURL = "") = 0; //apply a custom texture from kaneva to a dynamic object
	virtual bool ApplyFriendPicture(int objId, int friendId, bool thumbnail = false) = 0; //apply a friend's picture from kaneva to a dynamic object
	virtual bool ApplyGiftPicture(int objId, int friendId, bool thumbnail = false) = 0; //apply a gfit picture from kaneva to a dynamic object
	virtual bool ApplyCustomTextureWorldObject(int objId, int assetId, bool thumbnail = false) = 0; //apply a custom texture from kaneva to a world object
	virtual bool GetWorldObjectInfo(int objId, int* assetId, std::string* customTexture, bool* canHangPictures, bool* customizableTexture, std::string* textureUrl) = 0;
	virtual bool ApplyCustomTextureLocal(int objId, const std::string& fileName) = 0;
	virtual bool ApplyCustomTextureWorldObjectLocal(int objId, const std::string& fileName) = 0;
	virtual void SaveWorldObjectChanges(int objId, const char* textureURL = "") = 0; //save changes (right now just texture)
	virtual void CancelWorldObjectChanges(int objId) = 0; //cancel changes (right now just texture)

	virtual std::string DownloadTexture(int type, int assetId, const char* texUrl, bool thumbnail = false) = 0;
	virtual std::string DownloadTextureAsync(int type, int assetId, const char* texUrl, bool thumbnail = false) = 0;
	virtual std::string DownloadTextureAsync(const std::string& texFileName, const std::string& texUrl, const std::string& ctPath = "") = 0;

	virtual void CancelAllDownloads() = 0;
	virtual BOOL CancelPatch() = 0;

	virtual void RenderLabelsOnTop(bool onTop) = 0;

	virtual void SetAntiAlias(int aaTypeMax) = 0;
	virtual int GetAntiAlias() const = 0;

	virtual void SetLodBias_MovementObjects(int bias, bool biasAuto) = 0;
	virtual void GetLodBias_MovementObjects(int& bias, bool& biasAuto) const = 0;

	virtual void SetCullBias_DynamicObjects(int bias, bool biasAuto) = 0;
	virtual void GetCullBias_DynamicObjects(int& bias, bool& biasAuto) const = 0;

	virtual void SetParticleBias(int bias, bool biasAuto) = 0;
	virtual void GetParticleBias(int& bias, bool& biasAuto) const = 0;

	virtual void SetAnimationBias_MovementObjects(int bias, bool biasAuto) = 0;
	virtual void GetAnimationBias_MovementObjects(int& bias, bool& biasAuto) const = 0;

	virtual void SetAnimationBias_DynamicObjects(int bias, bool biasAuto) = 0;
	virtual void GetAnimationBias_DynamicObjects(int& bias, bool& biasAuto) const = 0;

	virtual void SetAvatarNames(int names, bool namesAuto) = 0;
	virtual void GetAvatarNames(int& names, bool& namesAuto) const = 0;

	virtual void SetShadows(bool enabled) = 0;
	virtual bool GetShadows() const = 0;

	virtual void SetInvertY(bool enabled) = 0;
	virtual bool GetInvertY() const = 0;

	virtual void SetMLookSwap(bool enabled) = 0;
	virtual bool GetMLookSwap() const = 0;

	virtual void WriteConfigSettings() = 0;

	virtual void SetControlConfig(int controlConfig) = 0;

	virtual std::string PatchUrl() const = 0;

	virtual ReD3DX9DeviceState* GetReDeviceState() = 0;

	virtual int GetKanevaUserId() const = 0;

	// Preloading methods
	virtual bool PreloadAnimationByHash(const std::string& animHash, int prioDelta) = 0;
	virtual bool PreloadAnimation(GLID animGLID, int prioDelta) = 0;
	virtual bool PreloadMesh(const std::string& animHash, int prioDelta) = 0;

	virtual bool MenuCloseAllExcept(const std::string& menuName = "") = 0;
	virtual bool MenuCloseAll() { return MenuCloseAllExcept(""); }

	virtual bool MenuExitAll() = 0;

	virtual FLOAT GetMyDistanceToMovementObject(int netTraceId) = 0;

	virtual IUGCManager* GetUGCManager() const = 0;

	virtual BOOL GetDragDropModeOn() const = 0;

	virtual bool UploadFileAsync(const char* uploadFile, const char* url, int prio, ValueList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0) = 0;
	virtual bool UploadFileAsync(const char* uploadName, char* dataBuf, DWORD dataBufLen, const char* url, int prio, ValueList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0) = 0;
	virtual bool UploadMultiFilesAsync(const std::vector<std::string>& uploadFiles, const char* url, int prio, ValueList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0) = 0;

	virtual void TurnOffWidget() = 0;
	virtual void widgetWorldSpace() = 0;
	virtual void widgetObjectSpace() = 0;
	virtual void widgetShowWidget(bool show) = 0;
	virtual void widgetAdvancedMovement(bool adv) = 0;
	virtual void getWidgetPosition(float* x, float* y, float* z) = 0;
	virtual void getWidgetRotation(float* x, float* y, float* z) = 0;
	virtual void setWidgetRotation(float x, float y, float z) = 0;
	virtual void setWidgetPosition(float x, float y, float z) = 0;
	virtual void ObjectDisplayTranslationWidget(const ObjectType& objType) = 0;
	virtual void ObjectDisplayRotationWidget(const ObjectType& objType) = 0;

	// Procedural/simple geometries support for scripts
	virtual int CreateStandardGeometry(const PrimitiveGeomArgs<float>& args) = 0;
	virtual int CreateCustomGeometry(int geomType, const std::vector<float>& positions, const std::vector<float>& normals) = 0;
	virtual bool DestroyGeometry(int geomId) = 0;
	virtual void DestroyAllGeometries() = 0;
	virtual bool SetGeometryVisible(int geomId, bool visible) = 0;
	virtual bool MoveGeometry(int geomId, float x, float y, float z, bool bRelative) = 0;
	virtual bool RotateGeometry(int geomId, float radx, float rady, float radz) = 0;
	virtual bool ScaleGeometry(int geomId, float sx, float sy, float sz) = 0;
	virtual bool SetGeometryRefObj(int geomId, ObjectType refType, int refObjId) = 0;
	virtual bool SetGeometryDrawMode(int geomId, int drawMode) = 0;
	virtual bool SetGeometryMaterial(int geomId, const TMaterial<float>& material) = 0;

	// DRF - Used For Accessory & Animation Imports
	virtual void Mount(int aiBotId, int mountType, bool bLocal) = 0;
	virtual void Unmount() = 0;

	virtual bool ObjectExists(const ObjectType& objType, int objId) = 0;
	virtual bool ObjectIsMe(const ObjectType& objType, int objId) = 0;
	virtual RuntimeSkeleton* ObjectGetRuntimeSkeleton(const ObjectType& objType, int objId, bool posses = false) = 0;
	virtual CFrameObj* ObjectGetBaseFrame(const ObjectType& objType, int objId, bool posses = false) = 0;
	virtual bool ObjectSetPosition(const ObjectType& objType, int objId, const Vector3f& pos) = 0;
	virtual bool ObjectGetPosition(const ObjectType& objType, int objId, Vector3f& pos) = 0;
	virtual bool ObjectSetRotation(const ObjectType& objType, int objId, const Vector3f& rot) = 0;
	virtual bool ObjectGetRotation(const ObjectType& objType, int objId, Vector3f& rot) = 0;
	virtual bool ObjectSetVisibility(const ObjectType& objType, int objId, bool visible) = 0;
	virtual bool ObjectSetNeedsUpdate(const ObjectType& objType, int objId, bool update) = 0;
	virtual void ObjectSetOutlineState(const ObjectType& objType, int objId, bool outline) = 0;
	virtual void ObjectSetOutlineColor(const ObjectType& objType, int objId, const D3DXCOLOR& color) = 0;
	virtual void ObjectSetOutlineZSorted(const ObjectType& objType, int objId, bool sort) = 0;
	virtual void ObjectSetRolloverMode(const ObjectType& objType, bool show) = 0;
	virtual bool ObjectGetWorldTransformation(ObjectType objType, int objId, Matrix44f& matrix) = 0;
	virtual bool ObjectGetBoneRelativeTransformation(ObjectType objType, int objId, const std::string& boneName, Matrix44f& matrix) = 0;

	virtual bool ObjectBakeAnimation(const ObjectType& objType, int objId, const GLID& animGlid) = 0;
	virtual bool ObjectSetAnimSpeedFactor(const ObjectType& objType, int objId, const GLID& animGlid, float animSpeedScale) = 0;
	virtual bool ObjectSetAnimLooping(const ObjectType& objType, int objId, const GLID& animGlid, bool animLooping) = 0;
	virtual bool ObjectSetAnimCropTimes(const ObjectType& objType, int objId, const GLID& animGlid, TimeMs animCropStartTimeMs, TimeMs animCropEndTimeMs) = 0;
	virtual bool ObjectGetAnimInfo(const ObjectType& objType, int objId, const GLID& animGlid, TimeMs& animDurationMs, TimeMs& animMinTimeMs, bool& animLooping, float& animSpeedScale, TimeMs& animCropStartTimeMs, TimeMs& animCropEndTimeMs) = 0;

	virtual void ObjectAddToSelection(const ObjectType& objType, int objId, bool reCalcPivot) = 0;
	virtual void ObjectLockSelection(BOOL lock) = 0;
	virtual void ObjectRemoveFromSelection(const ObjectType& objType, int objId, bool reCalcPivot) = 0;
	virtual void ObjectToggleSelected(const ObjectType& objType, int objId, bool reCalcPivot) = 0;
	virtual void ObjectReplaceSelection(const ObjectType& objType, int objId) = 0;
	virtual void ObjectClearSelection() = 0;
	virtual bool ObjectIsSelected(const ObjectType& objType, int objId) const = 0;
	virtual int ObjectsSelected() const = 0;
	virtual void ObjectGetSelection(ObjectType* objectType, int* objectID, unsigned int index) const = 0;
	virtual void ObjectSnapSelection(float tolerance) = 0;
	virtual void ObjectGetSelectionPivotOffset(float* x, float* y, float* z, int index) const = 0;

	virtual bool BakeAnimation_MovementObj(IGetSet* player, const GLID& glid) = 0;

	// Translate bone name into index (-1 if not found)
	virtual int GetBoneIndexByName(IGetSet* pGS_MO, const std::string& boneName) = 0;
	virtual int SendScriptServerMenuEvent(std::vector<std::string> params) = 0;
	virtual bool UploadAssetTo3DApp(const std::string& filename, const std::string& fullPath, int assetType, unsigned long seekPos, unsigned long objPlacementId) = 0;
	virtual int countDevToolAssets(int assetType) = 0;
	virtual bool getDevToolAssetInfo(int assetType, int index, std::string& name, std::string& fullPath, bool& modified) = 0;
	virtual std::string getDevToolAssetPath(int assetType) = 0;
	virtual bool Import3DAppAsset(int assetType, const std::string& path) = 0;
	virtual bool Import3DAppAssetFromString(int assetType, const std::string& baseName, const std::string& content, std::string& outFullPath) = 0;
	virtual void NotifyServerOfAssetUpload(int uploadId, std::string const& name, int assetType) = 0;

	// UGC asset URL
	virtual ContentMetadata* GetCachedUGCMetadataPtr(const GLID& glid) { return NULL; }
	virtual bool GetCachedUGCMetadata(const GLID& glid, ContentMetadata& md) = 0;
	virtual void CacheUGCMetadata(const GLID& glid) = 0;

	// Trigger manipulations
	virtual void EnableTriggerRender(bool renderDynamicTriggers, bool renderWorldTriggers) = 0;

	virtual CTriggerObject* AddDynamicObjectTrigger(CDynamicPlacementObj* pDPO, int triggerId, eTriggerAction triggerAction, const PrimitiveGeomArgs<float>& volumeArgs, int intParam, const std::string& strParam) = 0;

	// Item thumbnail texture request
	virtual std::string AddItemThumbnailToDatabase(const GLID& glid) = 0;

	// Handles Overhead Labels
	virtual bool DisplayTextBasedOn3dPosition(
		const Vector3f& wldSpcPos,
		const Matrix44f& cameraMatrix,
		const std::string& textStr,
		double maxDistance, // drf - at this distance in feet the object will not be visible anymore
		double minDistance,
		double fontSize,
		CTextRenderObj*& pTRO,
		const Vector3d& color,
		double yoffset) = 0;

	// Create UGC particle system and return local GLID
	virtual int CreateNewUGCParticleSystem() = 0;

	// Particle system access
	virtual IGetSet* GetParticleSystemByGlid(GLID glid) = 0;
	virtual IGetSet* GetParticleSystemByName(const std::string& effectName) = 0;

	virtual bool ObjectAddParticleAsync(
		const ObjectType& objType,
		int objId,
		const std::string& boneName,
		int particleSlot,
		const GLID& particleGlid,
		const Vector3f& offset,
		const Vector3f& dir,
		const Vector3f& up) = 0;

	virtual bool ObjectRemoveParticle(const ObjectType& objType, int objId, const std::string& boneName, int particleSlot) = 0;

	virtual bool AddParticleSystemToDynamicObject(int placementId, const std::string& effectName) = 0;
	virtual void RemoveParticleSystemFromDynamicObject(int placementId) = 0;

	virtual bool GetDynamicObjectParticleSystem(int placementId, GLID& glid, std::string& effectName) = 0;

	virtual bool ConfigPlayerEquippableItem(IGetSet* pGS_MO, const CharConfigItem* pCCI) = 0;

	virtual void RequestItemAnimDataAsync(int DOGlid) = 0;

	virtual void LoadUGCActor(const GLID& actorGLID) = 0;
	virtual void addAnimationUGCActor(const GLID& actorGlid, const GLID& animGlid, const std::string& name) = 0;

	virtual SIZE getClientRectSize() = 0; // Return client window width and height

	virtual void configMovementObjectSubstitute(int edbIdx, ObjectType type, int globalId) = 0;

	virtual unsigned short GetItemsMetadataResultEventId() const = 0;

	virtual void setDynamicObjectTexturePanning(int objPlacementId, int meshId, int uvSetId, float uIncr, float vIncr) = 0;

	virtual size_t GetNumHumanPlayersInRuntimeList() const = 0;

	virtual BOOL PlayParticleEffectOnPlayer(int playerAssId, const std::string& boneName, int particleSlotId, UINT particleGLID) = 0;

	virtual bool requestSourceAssetFromAppServer(int type, std::string name, int seekPos) = 0;

	virtual void MoveDynamicObjectTriggers(int placementId, const Vector3f& origin, const Vector3f& dir) = 0;
	virtual bool GlueRayTrace(const Vector3f& rayOrigin, const Vector3f& rayDirection, ObjectType mask, ObjectType& typeResult, int& idResult, Vector3f& intersection, float& distance) = 0;
	virtual bool SphereTestObject(const ObjectType& objType, int objId, Vector3f& sphereOrigin, float radius, Vector3f& intersection, float& distance, const Vector3f& orientation, float maxAngle) = 0;
	virtual void GetPickRay(Vector3f* rayOrigin, Vector3f* rayDir, int x, int y) = 0;

	virtual std::vector<int> getAllMovementObjectNetTraceIds() = 0;

	// DRF - MouseOverSettings
	virtual bool MOS_Clear(const MOS::CATEGORY& mosCategory = MOS::CATEGORY_ALL) = 0;
	virtual bool MOS_Add(const ObjectType& objType, int objId, const MOS::MODE& mosMode, const std::string& toolTipText = "", const CURSOR_TYPE& cursorType = CURSOR_TYPE_CLICKABLE, const MOS::CATEGORY& mosCategory = MOS::CATEGORY_NONE) = 0;

	virtual bool SetMyPosition(float x, float y, float z) = 0;
	virtual bool SetMyOrientation(float rx, float ry, float rz) = 0;

	virtual void presetMenuMessage(const std::string& menuName, const int placementId, const std::string& messageType, const std::string& data1, const std::string& data2, const std::string& identifier) = 0;
	virtual void sendPlaceDynamicObjectAtPositionEvent(const long long& glid, const short& invenType, const float& posX, const float& posY, const float& posZ, const float& rotX, const float& rotY, const float& rotZ) = 0;

	virtual bool PlaceDynamicObjectByGlid(const GLID& glid, int invenType) = 0;
	virtual void adjustBuildPlaceHeight(float adjustment) = 0;

	virtual void setGridSpacing(float spacing) = 0;
	virtual float getGridSpacing() = 0;

public:
	virtual bool ResetCursorModesTypes() = 0;
	virtual bool SetCursorModeType(const CURSOR_MODE& cursorMode, const CURSOR_TYPE& cursorType) = 0;
	virtual bool SetCursorMode(const CURSOR_MODE& cursorMode) = 0;

	virtual std::string GetCursorFileName(const CURSOR_TYPE& type) = 0;
	virtual void SetCursorFileName(const CURSOR_TYPE& type, const std::string& fileName) = 0;

public:
	virtual void SetZoneAllowsLabels(bool _allow) = 0;

public:
	// Directories
	virtual std::string PathBase(void) const = 0;
	virtual std::string LocalDevBaseDir(void) const = 0;
	virtual std::string LocalDevAppDir(void) const = 0;

	virtual CStateManagementObj* GetRenderStateManager(void) const = 0;

	virtual CReferenceObjList* GetReferenceLibrary(void) = 0;
	virtual CMovementObjList* GetMovementObjectRefModelList() = 0;
	virtual CMovementObjList* GetMovementObjectList() = 0;
	virtual CMovementObjList* GetMovementObjectShadowList() = 0;
	virtual int GetMyRefModelIndex() = 0;
	virtual EffectHandle CreateParticleSystem(const char* effectName, Vector3f position, Quaternionf orientation) = 0;
	virtual ParticleRM* GetParticleDatabase() = 0;
	virtual ControlDBList* GetControlsList() = 0;
	virtual CSkillObjectList* GetSkillDatabase() = 0;

	// Projectiles
	virtual CMissileObjList* GetProjectiles() = 0;

	// Explosions
	virtual CExplosionObjList* GetExplosions() = 0;

	// DDR Sounds Only
	virtual CSoundBankList* GetSoundManager() = 0;

	//	dynamic geom renderer
	virtual ReDynamicGeom* GetDynamicGeom() = 0;

	virtual bool GetMediaScrapeEnabled() const = 0;
	virtual void SetMediaScrapeEnabled(bool enabled) = 0;

	virtual bool GetAnimationsEnabled() const = 0;
	virtual void SetAnimationsEnabled(bool enabled) = 0;

	virtual bool GetParticlesEnabled() const = 0;
	virtual void SetParticlesEnabled(bool enabled) = 0;

	virtual bool GetLimitFPS() const = 0;
	virtual void SetLimitFPS(bool enabled) = 0;

	virtual TimeMs GetOnRenderTimeMs() const = 0;
	virtual void SetOnRenderTimeMs(TimeMs timeMs) = 0;

	// Physics
	// The current state (at the end of the function call) is returned.
	virtual int ShowPhysicsWindow(int mode) = 0;

	virtual void EngagePhysicsVehicle(int iDynamicObjectPlacementId, const Vector3f& offsetMeters, const Quaternionf& orientation, const Physics::VehicleProperties& props) = 0;
	virtual void DisengagePhysicsVehicle() = 0;
	virtual double GetMyVehicleSpeedKPH() const = 0;

	// Texture quality settings
	virtual void ResetTextureQualityLevels() = 0;
	virtual bool EnableTextureQualityLevel(unsigned quality) = 0; // return false if quality level is unrecognized / unavailable
	virtual void ForceOnePixelTexture(bool force) = 0;
	virtual bool IsForcingOnePixelTexture() const = 0;

	// Query metadata of imported texture. Return true if found.
	virtual bool GetTextureImportInfo(const std::string& fileName, TextureDef& textureInfo) = 0;

	// Notify client engine that zone loading progress menu is open
	virtual void SetZoneLoadingUIActive(bool active) = 0;
	virtual void SetZoneLoadingProgressPercent(double percent) = 0;

	// Set per-frame event processing time parameters
	virtual void ConfigEventDispatchTime(unsigned dispatchTimeInGame, unsigned dispatchTimeLoading, bool legacyMode) = 0;

	//Ankit ----- APIs for the SATFramework
	virtual void PostKeyboardMouseMsg(bool isKeybdInput, int isPressed, int code, int dx, int dy) = 0;
	virtual int GETGetSetInt(std::string varName) = 0;
	virtual void SETGetSetInt(std::string varName, int value) = 0;
	virtual std::string GETGetSetString(std::string varName) = 0;
	virtual void SETGetSetString(std::string varName, std::string value) = 0;
	virtual void LogMsgForSAT(std::string msg) = 0;
	virtual void SetScrollBarInMenuToContainIndex(std::string lstName, std::string menuName, int index) = 0;
	virtual void SetTextForEditBoxInMenu(std::string text, std::string edtBox, std::string menuName) = 0;
	virtual void ComboBoxSetSelectedByDataInMenu(int data, std::string cmbBox, std::string menuName) = 0;
	virtual std::string GetInfoBoxMessage() = 0;
	virtual bool IsButtonInMenuEnabled(std::string btnName, std::string menuName) = 0;
	virtual void UseButtonInsideMenu(std::string btnName, std::string menuName) = 0;
	virtual void UseRadioButtonInsideMenu(std::string radioBtnName, std::string menuName) = 0;
	virtual bool CheckIfMenuLoaded(std::string menuName) = 0;
	virtual bool CheckIfScreenshotImageSaved() = 0;
	virtual void CheckIfPacketRedeemedForUser(int packetId, int userId) = 0;
	virtual bool CheckIfParticleEffectPlayingOnPlayer(int particleGlid, int playerNetID = 0) = 0;
	virtual std::vector<int> GetItemsArmedByPlayer(int particleGlid, int playerNetID = 0) = 0;
	virtual bool IsPlayerAP() = 0;
	virtual bool IsPlayerVIP() = 0;
	virtual bool IsDynamicObjectWithPIDValid(int pid) = 0;
	virtual void CreateTestUser(std::string username, std::string password) = 0;
	virtual void CloseAllBrowsers() = 0;
	virtual bool IsBrowserOpenWithTitleAndURL(std::string windowClassName, std::string titleOfWebpage, std::string URL) = 0;
	virtual bool IsChatWindowInForeground() = 0;
	virtual void BringKanevaClientToForeground() = 0;
	virtual void FinishTestingAndCloseClient(bool successfulCompletion) = 0;
	virtual bool IsSATRunning() = 0;

	virtual void ExecuteFunction(std::string funcName) = 0;
	virtual void GetDevOptionsDefinedInEngine(std::string& devOptionsCombinedString) = 0;

	// Download URL to disk, with optional completion event
	virtual bool DownloadFileAsync(ResourceType jobType, const std::string& url, DownloadPriority prio, const std::string& destFile, FileSize expectedFileSize, bool compressed, IEvent* event, bool preload) = 0;

	// Download URL with data handler callback
	virtual bool DownloadFileAsyncWithHandler(ResourceType jobType, const std::string& url, DownloadPriority prio, DownloadHandler* handler, bool preload) = 0;

	// Download resource, with optional completion event
	virtual bool DownloadResourceAsync(IResource* res, bool compressed, IEvent* event, bool preload) = 0;

	// Enable download priority visualization
	virtual void EnableDownloadPriorityVisualization(bool enable) = 0;
	virtual bool IsDownloadPriorityVisualizationEnabled() const = 0;

	// Experimental engine options
	virtual void SetExperimentalEngineOption(int option, int value) = 0;
	virtual int GetExperimentalEngineOption(int option) const = 0;

	// Object texture highlight
	virtual bool SetDynamicObjectHighlight(int placementId, bool highlight, float r, float g, float b, float a) = 0;

	// Local, procedurally generated dynamic texture A8R8G8B8. Usage: world map, paintable surface, etc.
	virtual unsigned CreateDynamicTexture(unsigned width, unsigned height) = 0;
	virtual void DestroyDynamicTexture(unsigned dynamicTextureId) = 0;
	virtual IDirect3DTexture9* GetDynamicTexture(unsigned dynamicTextureId) = 0;

	// World map generation support
	virtual void GetWorldMapBoundary(WorldRenderOptions options, double minRadiusFilter, double& minX, double& minY, double& minZ, double& maxX, double& maxY, double& maxZ) = 0;
	virtual bool GenerateWorldMap(unsigned dynTextureId, WorldRenderStyle style, WorldRenderOptions options, double minRadiusFilter, double minX, double minY, double minZ, double maxX, double maxY, double maxZ) = 0;

	// Suspend/Resume server connection
	virtual void DisableClient() = 0;
	virtual void EnableClient() = 0;
	virtual bool IsClientEnabled() const = 0;

	// Enable/Disable MSG_MOVE_TO_SERVER
	virtual void SetMsgMoveEnabled(bool enable) = 0;
	virtual bool IsMsgMoveEnabled() = 0;

	// Set/Get MSG_GZIP Minimum Size Requirement
	virtual void SetMsgGzipSize(size_t size) = 0;
	virtual size_t GetMsgGzipSize() const = 0;

	// Enable/Disable MSG_GZIP Logging
	virtual void SetMsgGzipLogEnabled(bool enable) = 0;
	virtual bool IsMsgGzipLogEnabled() const = 0;

	// Frame Time Profiler
	virtual void EnableFrameTimeProfiler(bool enable) = 0;
	virtual bool IsFrameTimeProfilerEnabled() const = 0;
	virtual FrameTimeProfiler* GetFrameTimeProfiler() const = 0;

	virtual bool GetMovementObjectByPlacementId(int placement, CMovementObj*& pMovObj, CMovementObj*& pMovObjRefModel) = 0;

	// #MLB_BulletSensor - IClientEngine LandClaim
	virtual bool IsObjectInLandClaim(int objectPid, int landClaimPid) = 0;
	virtual bool RegisterLandClaim(int pid) = 0;
	virtual bool UnRegisterLandClaim(int pid) = 0;

protected:
	IClientEngine() :
			IGame(eEngineType::Client) {
		Instance(this);
	}

	// DRF - Never Called!
	virtual ~IClientEngine() {}
};

}; // namespace KEP
