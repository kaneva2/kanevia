///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include "ListItemMap.h"

namespace KEP {

class CCameraObj;

class RuntimeCameras {
public:
	RuntimeCameras() :
			m_cameraId(0) {}

	virtual ~RuntimeCameras() {
		DelAllExceptDefault();
	}

	std::string GetNewCameraName(const std::string& base_name);

	uint32_t GetNewCameraId() {
		return ++m_cameraId;
	}

	CCameraObj* AddDefaultCamera();

	uint32_t Add(uint32_t db_camera_index);

	bool Del(CCameraObj* pCO);

	void DelAllExceptDefault();

	CCameraObj* GetCamera(uint32_t camera_id);

	void Update();

private:
	typedef std::map<int, CCameraObj*> CameraMap; // cameraId -> pCO
	CameraMap m_cameras;

	uint32_t m_cameraId;
};

} // namespace KEP