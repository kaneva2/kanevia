///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

const char* const GAMEFILES = "GameFiles";
const char* const MAPSMODELS = "MapsModels";
const char* const SOUNDS = "Sounds";
#ifndef _DEBUG
const char* const CLIENTBLADES = "Clientblades";
#else
const char* const CLIENTBLADES = "Clientbladesd";
#endif
const char* const TEMPLATES = "TEMPLATES";

const char* const STARTCFG_AUT = "STARTCFG.AUT";

// for zone files
const char* const EXG_EXT = "exg";

// for game files
const char* const KEP_EXT = "kep";

// for patching
const char* const VERSIONINFO_DAT = "versioninfo.dat";
const char* const ZONEINFO_DAT = "zoneinfo.dat";
const char* const PATCH_TYPE_TAG = "VIFILE_TYPE";
const char* const PATCH_TYPE_TEMPLATE = "TEMPLATE";
const char* const PATCH_TYPE_WORLD = "WORLD";
const char* const PATCH_TYPE_WOK = "WOK";

const char* const IMPORTED_TEXTURE = "imported";
const char* const DYNOBJ_TEXTURE = "DynamicObject";

// for publishing and script server
const char* const SCRIPTSERVERDIR = "ScriptServerScripts";
const char* const SAVEDSCRIPTDIR = "SavedScripts";
const char* const ACTIONSCRIPTDIR = "ActionItems";
const char* const ACTIONINSTANCEDIR = "ActionInstances";
const char* const CLIENTSCRIPTSDIR = "ClientScripts";
const char* const MENUSDIR = "Menus";
const char* const MENUSCRIPTSDIR = "MenuScripts";
const char* const SCRIPTDATADIR = "ScriptData";
const char* const SCRIPTDIR = "Scripts";
const char* const CONFIGDIR = "KanevaConfig";
//const char* const CUSTOMTEXTUREDIR = "CustomTexture";		// <BaseDir>\CustomTexture

#define FRAMEWORK_BASE_DIR "Framework"
const char* const FRAMEWORKDIR = FRAMEWORK_BASE_DIR;
const char* const FRAMEWORKINCLUDEDIR = FRAMEWORK_BASE_DIR "\\Includes";
const char* const FRAMEWORKBEHAVIORDIR = FRAMEWORK_BASE_DIR "\\Behaviors";
const char* const FRAMEWORKCLASSDIR = FRAMEWORK_BASE_DIR "\\Classes";
const char* const FRAMEWORKTESTDIR = FRAMEWORK_BASE_DIR "\\Test";

const char* const GLOBAL_JSON = "global.json";

const char* const TEXTURE_QUALITY_SUFFIX_FORMAT = "%03d";
const char* const TEXTURE_QUALITY_SUFFIX_MACRO = "{q}";
const char* const TEXTURE_SELECTOR_MACRO = "{subid}";

const char* const DEV_AUTH_FILE = "developer.dat";
