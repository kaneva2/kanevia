///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Crypto/CryptDecrypt.h"
#include "common/include/kepcommon.h"
#include "common/include/config.h"
#ifndef DECOUPLE_DBCONFIG
const double MIN_DB_VERSION = 49.0;
// schema version history
// 49	11/19/08	add p2p table, make version decimal to allow sub versions, change SP signature
// 48	10/ /08		view insulation, bug fixes, etc.
// 47	8/14/08		dispatcher cleanup, fixes
// 46   7/  /08		added cols for shopping, etc. no code change req'd
// 45   4/08/08		add rotation to spawn points, revamp canSpawn* SPs
// 43   3/12/08		more changes for STAR
// 43   1/22/08		changes for STAR
// 42	1/03/08		remove all "kaneva." references from C++, replaced w/SPs
// 36   8/14/07     add pass_groups
// 35   7/17/07	    unique key on inventories
// 34	7/01/07		add active column to inventories
// 33	6/18/07		dyn obj changes
// 32 	6/05/07		add create_date to game_users and players
// 31   5/22/07		playlist id instead of URL
// 30   5/10/07		server_id phase 2 complete, denormalize kaneva_user_id
// 29   4/30/07		GPOINT switch
// 28   4/19/07		Add SP for tran details
// 27	4/12/07		Add SP for setting balances, remove balances from user tables
// 26   3/29/07		Add pending_gifts
// 25   3/06/07  	IP to game_user, stats, swf to dyn objs
// 23   2/14/07		3D Gifting changes
// 22   2/01/07		SP signature changes for occupancy
// 20 	1/19/07 	added occupancy columns, first version check

const char* const DB_CFG_NAME = "db.cfg";
const char* const DB_CFG_ROOT_NODE = "database";

const char* const DB_SERVER = "DB_Server";
const char* const DB_PORT = "DB_Port";
const char* const DB_NAME = "DB_Name";
const char* const DB_USER = "DB_User";
const char* const DB_PASSWORD = "DB_Password";
const char* const DB_PASSWORD_CLEAR = "DB_PasswordClear";
const char* const DB_DONTSHOWAGAIN = "DB_DontShowEdit";
const char* const DB_DISABLED = "DB_Disabled";
const char* const DB_UPDATE_INTERVAL = "DB_UpdateInterval";
const char* const DB_KPOINT = "DB_KPointName";
const char* const DB_GPOINT = "DB_GPointName"; // gift credits
const char* const DB_PLAYLISTSWF = "PlaylistSwf";
const char* const DB_PLAYLISTPARAM = "PlaylistParam";
const char* const DB_BACKUP_SLEEP_TIME = "DB_BACKUP_SLEEP_MS";
const char* const DB_MAX_PLAYERS_IN_BACKUP = "DB_MaxPlayersInBackup";
const char* const DB_NEVERSWITCHSERVERS = "NeverSwitchServers";
const char* const DB_LOCALPING = "LocalPing";
const char* const DB_PINGINTERVAL = "PingIntervalMs";
const char* const DB_BACKGROUNDINTERVAL = "BackgroundIntervalMs";
const char* const DB_BACKGROUNDTHREADS = "BackgroundThreadCount";
const char* const DB_LOCAL_PLAYERS = "LocalPlayers";
const char* const DB_COMMON = "DB_Common"; // Shared DB schema. E.g. kgp_common. If not set, use DB_Name.
const char* const DB_FULL_INVENTORY = "FullInventory";
#else
#include "Common/include/DBConfigTags.h"
#include "Common/include/config.h"
#endif

#include "KEPConfig.h"
#include "KEPException.h"
#ifndef DECOUPLE_DBCONFIG
enum EshowValue {
	showDialog, // always show it
	useConfigToShow,
	onlyShowIfNeeded,
	showDialogHadError,
};
#else
#endif

namespace KEP {

// wrapper over KEPConfig for the DBConfig file
class DBConfig {
public:
	DBConfig(const IKEPConfig& cfg) :
			m_cfg(cfg) {}

	// load the values from the file
	bool Get(std::string& dbServer, int& dbPort, std::string& dbName,
		std::string& dbUser, std::string& dbPassword,
		bool& dontShowDlg) const {
		if (!m_cfg.IsLoaded()) {
			return false;
		}

		std::string dbClearPassword;

		// set defaults
		dbPort = 3306;
		dbServer = "localhost";
		dbUser = "root";
		dbName = "kgp";

		if (m_cfg.GetValue(DB_SERVER, dbServer) &&
			m_cfg.GetValue(DB_PORT, dbPort) &&
			m_cfg.GetValue(DB_NAME, dbName) &&
			m_cfg.GetValue(DB_USER, dbUser) &&
			m_cfg.GetValue(DB_PASSWORD, dbPassword) &&
			m_cfg.GetValue(DB_DONTSHOWAGAIN, dontShowDlg)) {
			if (m_cfg.GetValue(DB_PASSWORD_CLEAR, dbClearPassword)) {
				dbPassword = dbClearPassword;
			} else {
				// decrypt password
				std::string szPassword;
				if (SetupCryptoClient() && DecodeAndDecryptString(dbPassword.c_str(), szPassword, DB_KEY)) {
					dbPassword = szPassword;
				}
			}
		}
		return true;
	}
#ifndef DECOUPLE_DBCONFIG
	// Export settings to an IKEPConfig instance
	static bool Export(IKEPConfig& cfg, const char* dbServer, int dbPort, const char* dbName,
		const char* dbUser, const char* dbPassword,
		bool dontShowAgain) {
		return Export(cfg, dbServer, dbPort, dbName,
			dbUser, dbPassword,
			dontShowAgain,
			false, false);
	}

	// Export settings to an IKEPConfig instance, including subGame value
	static bool Export(IKEPConfig& cfg, const char* dbServer, int dbPort, const char* dbName,
		const char* dbUser, const char* dbPassword,
		bool dontShowAgain,
		bool subGame,
		bool saveSubGame = true) {
		cfg.SetValue(DB_SERVER, dbServer);
		cfg.SetValue(DB_PORT, dbPort);
		cfg.SetValue(DB_NAME, dbName);
		cfg.SetValue(DB_USER, dbUser);
		if (saveSubGame)
			cfg.SetValue(DB_PASSWORD_CLEAR, dbPassword); // so Python can use it

		// encrypt the password
		std::string encryptedPassword;
		BOOL encryptResult = EncryptAndEncodeString(dbPassword, encryptedPassword, DB_KEY);

		_ASSERT(encryptResult);
		if (!encryptResult) {
			return false;
		}
		cfg.SetValue(DB_PASSWORD, encryptedPassword.c_str());
		cfg.SetValue(DB_DONTSHOWAGAIN, dontShowAgain);
		cfg.SetValue(DB_DISABLED, 0); // put in XML so they can easily disable it
		if (saveSubGame)
			cfg.SetValue(DB_LOCAL_PLAYERS, !subGame);

		return true;
	}
#endif
	const IKEPConfig& config() const { return m_cfg; }

private:
	const IKEPConfig& m_cfg;
};

} // namespace KEP