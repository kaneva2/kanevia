# generate the header from the XML file

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;

open( LUA, ">..\\..\\..\\..\\Deploy\\GameContentDbg\\templates\\GameFiles\\scripts\\CMD.lua" ) || die ( "Cannot open ..\\..\\..\\..\\Deploy\\GameContentDbg\\templates\\GameFiles\\scripts\\CMD.lua $!" );
open( PYTHON, ">..\\..\\..\\..\\Deploy\\GameContentDbg\\templates\\GameFiles\\scripts\\CMD.py" ) || die ( "Cannot open ..\\..\\..\\..\\Deploy\\GameContentDbg\\templates\\GameFiles\\scripts\\CMD.py $!" );
open( CPP, ">CommandHeader.h" ) || die ( "Cannot open CommandHeader.h $!" );
open( INPUT, " ..\\..\\..\\..\\Deploy\\GameContentDbg\\templates\\GameFiles\\commands.xml" ) || die ( "Cannot open  ..\\..\\..\\..\\Deploy\\GameContentDbg\\templates\\GameFiles\\commands.xml\n\t$!" );

print STDERR "Reading ..\\..\\..\\..\\Deploy\\GameContentDbg\\templates\\GameFiles\\commands.xml\n";
print STDERR "Writing to CMD.lua, CMD.py, and CommandHeader.h...\n";

printf LUA "-- CommandHeader.pl generated file %d-%02d-%d %d:%02d\n", $year+1900, $mon+1, $mday, $hour, $min;
printf PYTHON "# CommandHeader.pl generated file %d-%02d-%d %d:%02d\n", $year+1900, $mon+1, $mday, $hour, $min;
printf CPP "// CommandHeader.pl generated file %d-%02d-%d %d:%02d\n", $year+1900, $mon+1, $mday, $hour, $min;

print LUA "CMD = {}\n";
print CPP "#ifndef _COMMANDHEADER_H_\n";
print CPP "#define _COMMANDHEADER_H_\n\n";

# we use the name in comment to allow the actual command to change
my $nameInComment = "";

my $lastItem = "";

while (<INPUT>)
{
	if ( /<!--\s+([A-Z]+)/ )
	{
		$nameInComment = $1;
	}
	
	#       <Command id="1">BLOCKIP</Command>
	if ( /<Command id="(\d*)\">(.*)</i )
	{
		if ( $nameInComment ne $lastItem )
		{
			print LUA "CMD.$nameInComment = $1\n";
			print PYTHON "$nameInComment = $1\n";
			print CPP "#define CMD_$nameInComment\t$1\n";
			print "$nameInComment $1\n";
			$lastItem = $nameInComment;
		}
	}
}

print CPP "\n\n#endif\n";

print STDERR "Complete, copy the script files to the appropriate folders\n";
