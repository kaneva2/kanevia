/******************************************************************************
 KEPAuthConstants.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// here are constants used by the authorization calls
const ULONG KEPAUTH_SYSTEM = 0x0001;
const ULONG KEPAUTH_VISITOR = 0x0002;
const ULONG KEPAUTH_USER = 0x0004;
const ULONG KEPAUTH_ADMIN = 0x0008; // this bit must be set to do admin calls
const ULONG KEPAUTH_CSR = 0x0010;
const ULONG KEPAUTH_MASTER = 0x0020;
const ULONG KEPAUTH_LOGICAL = 0x0040;

const ULONG NO_GAME_ID = 0; // when logging in for admin, etc., no game id

// errors from web service
const ULONG KEPAUTH_FAILED = 0; // 0 not authenticated
const ULONG KEPAUTH_SUCCESS = 1; // 1 successuful authentication
const ULONG KEPAUTH_USER_NOT_FOUND = 2; // 2 user not retrieved from database
const ULONG KEPAUTH_PASSWORD_INVALID = 3; // 3 password invalid
const ULONG KEPAUTH_NO_GAME_ACCESS = 4; // 4 user was authenticated; but not to game supplied
const ULONG KEPAUTH_NOT_VALIDATED = 5; // 5 user has not validated the account
const ULONG KEPAUTH_ACCOUNT_DELETED = 6; // 6 user account was deleted
const ULONG KEPAUTH_ACCOUNT_LOCKED = 7; // 7 user account was locked
const ULONG KEPAUTH_ALREADY_LOGGED_IN = 8; // 8 user alreay logged in; performed by DS web service
const ULONG KEPAUTH_GAME_FULL = 9; // 9 Max user limit hit for the game
const ULONG KEPAUTH_NOT_AUTHORIZED = 10; // 10 mature, age, access pass, etc.
const ULONG KEPAUTH_WRONG_SERVER = 11; // 11 mismatched server info serverId doesn't match serverId for actualIp

// errors passed back
#define ERR_SERVER_NOT_STARTED -1
#define ERR_LOG_INIT -2

inline bool KEPAUTH_IsAuthorized(IN ULONG mask, IN ULONG bit) {
	return (mask & bit) != 0;
}
