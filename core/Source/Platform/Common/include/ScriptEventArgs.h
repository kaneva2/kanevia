///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Tools\NetworkV2\NetworkTypes.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Glids.h"
#include "Core/Util/StaticLoop.h"

namespace KEP {

class IReadableBuffer;
class IWriteableBuffer;

struct ScriptWrappedType {
	typedef void script_wrapped_void; // replace std::enable_if<std::is_base_of>> as a workaround for VS2015 update 2
};

// Wrapper type for PID
struct ScriptPIDType : ScriptWrappedType {
	int value;
};

// Wrapper type for NETID
struct ScriptNETIDType : ScriptWrappedType {
	NETID value;
};

// Wrapper type for timestamp
struct ScriptTimestamp : ScriptWrappedType {
	TimeMs value;
};

// Wrapper type for playlist ID
struct ScriptPlaylistID : ScriptWrappedType {
	int value;
};

template <typename T>
struct ScriptPos {
	T x, y, z;
};

template <typename T>
struct ScriptPosRot {
	T x, y, z, rx, ry, rz;
};

// Wrapper type for legacy size-prefixed string array
template <typename T>
struct ScriptLegacyArray : ScriptWrappedType {
	std::vector<T> value;
};

// Wrapper type for key modifier table
struct KeyModifierTable {
	bool shift;
	bool ctrl;
	bool alt;
};

struct ScriptZoneInfo {
	int zoneId;
	std::string appName;
	ScriptLegacyArray<int> instanceIdArray;
};

struct ScriptZoneInstanceInfo {
	int instanceId;
	int locked;
	ScriptLegacyArray<int> players;
};

struct ScriptItemInfo {
	int succ;
	GLID globalId;
	int useType;
	int actorGroup;
};

// Do nothing for integral constants
template <typename T, T v>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, std::integral_constant<T, v>& val) {
	return inBuffer;
}
template <typename T, T v>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const std::integral_constant<T, v>& val) {
	return outBuffer;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPIDType& val);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPIDType& val);

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptNETIDType& val);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptNETIDType& val);

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptTimestamp& val);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptTimestamp& val);

template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPos<T>& val);
template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPos<T>& val);
template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPosRot<T>& val);
template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPosRot<T>& val);

template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptLegacyArray<T>& val);
template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptLegacyArray<T>& val);

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, KeyModifierTable& val);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const KeyModifierTable& val);

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptZoneInfo& val);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptZoneInfo& val);

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptZoneInstanceInfo& val);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptZoneInstanceInfo& val);

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptItemInfo& val);
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptItemInfo& val);

// Tuple
template <typename... Args>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, std::tuple<Args...>& val) {
	ForEachInRange<size_t, 0, std::tuple<Args...>::_Mysize>([&](auto index) {
		inBuffer >> std::get<decltype(index)::value>(val);
	});
	return inBuffer;
}

template <typename... Args>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const std::tuple<Args...>& val) {
	ForEachInRange<size_t, 0, std::tuple<Args...>::_Mysize>([&](auto index) {
		outBuffer << std::get<decltype(index)::value>(val);
	});
	return outBuffer;
}

} // namespace KEP
