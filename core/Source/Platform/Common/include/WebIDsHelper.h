#pragma once

#include <string>
#include <map>

//Utility function to generate WebIDs.lua
bool GenerateWebIDsXml(const std::string& webIDsPath, int gameId, const std::string& oauthKey, const std::string& oauthSecret,
	const std::map<std::string, int>& achievementIds, const std::map<std::string, int>& premiumItemIds);
