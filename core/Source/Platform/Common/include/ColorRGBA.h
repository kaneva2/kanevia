///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <assert.h>

namespace KEP {

template <typename TComponent>
class ColorRGBA {
public:

	ColorRGBA();
	ColorRGBA(const ColorRGBA<TComponent>& color);
	ColorRGBA(const TComponent& red, const TComponent& green, const TComponent& blue, const TComponent& alpha);
	ColorRGBA(const TComponent& value);
	ColorRGBA(const TComponent* pColor);
	~ColorRGBA();

	void SetRed(const TComponent& red);
	void SetGreen(const TComponent& green);
	void SetBlue(const TComponent& blue);
	void SetAlpha(const TComponent& alpha);

	const TComponent& GetRed() const;
	const TComponent& GetGreen() const;
	const TComponent& GetBlue() const;
	const TComponent& GetAlpha() const;

	ColorRGBA<TComponent>& operator=(const ColorRGBA<TComponent>& color);

	bool operator==(const ColorRGBA<TComponent>& color) const;
	bool operator!=(const ColorRGBA<TComponent>& color) const;
	bool operator!() const;
	operator bool() const;

	operator TComponent*();
	operator const TComponent*() const;

	ColorRGBA<TComponent> operator+(const ColorRGBA<TComponent>& color) const;
	ColorRGBA<TComponent> operator-(const ColorRGBA<TComponent>& color) const;
	ColorRGBA<TComponent> operator*(const ColorRGBA<TComponent>& color) const;
	ColorRGBA<TComponent> operator/(const ColorRGBA<TComponent>& color) const;

private:

	TComponent m_components[4];
}; // class..ColorRGBA

template <typename TComponent>
ColorRGBA<TComponent>::ColorRGBA() {
} // ColorRGBA< TComponent >::ColorRGBA

template <typename TComponent>
ColorRGBA<TComponent>::ColorRGBA(const ColorRGBA<TComponent>& color) {
	m_components[0] = color.m_components[0];
	m_components[1] = color.m_components[1];
	m_components[2] = color.m_components[2];
	m_components[3] = color.m_components[3];
} // ColorRGBA< TComponent >::ColorRGBA

template <typename TComponent>
ColorRGBA<TComponent>::ColorRGBA(const TComponent& red, const TComponent& green, const TComponent& blue, const TComponent& alpha) {
	m_components[0] = red;
	m_components[1] = green;
	m_components[2] = blue;
	m_components[3] = alpha;
} // ColorRGBA< TComponent >::ColorRGBA

template <typename TComponent>
ColorRGBA<TComponent>::ColorRGBA(const TComponent& value) {
	m_components[0] = value;
	m_components[1] = value;
	m_components[2] = value;
	m_components[3] = value;
} // ColorRGBA< TComponent >::ColorRGBA

template <typename TComponent>
ColorRGBA<TComponent>::ColorRGBA(const TComponent* pColors) {
	m_components[0] = pColors[0];
	m_components[1] = pColors[1];
	m_components[2] = pColors[2];
	m_components[3] = pColors[3];
} // ColorRGBA< TComponent >::ColorRGBA

template <typename TComponent>
ColorRGBA<TComponent>::~ColorRGBA() {
} // ColorRGBA< TComponent >::~ColorRGBA

template <typename TComponent>
void ColorRGBA<TComponent>::SetRed(const TComponent& red) {
	m_components[0] = red;
} // ColorRGBA< TComponent >::SetRed

template <typename TComponent>
void ColorRGBA<TComponent>::SetGreen(const TComponent& green) {
	m_components[1] = green;
} // ColorRGBA< TComponent >::SetGreen

template <typename TComponent>
void ColorRGBA<TComponent>::SetBlue(const TComponent& blue) {
	m_components[2] = blue;
} // ColorRGBA< TComponent >::SetBlue

template <typename TComponent>
void ColorRGBA<TComponent>::SetAlpha(const TComponent& alpha) {
	m_components[3] = alpha;
} // ColorRGBA< TComponent >::SetAlpha

template <typename TComponent>
const TComponent&
ColorRGBA<TComponent>::GetRed() const {
	return m_components[0];
} // ColorRGBA< TComponent >::GetRed

template <typename TComponent>
const TComponent&
ColorRGBA<TComponent>::GetGreen() const {
	return m_components[1];
} // ColorRGBA< TComponent >::GetGreen

template <typename TComponent>
const TComponent&
ColorRGBA<TComponent>::GetBlue() const {
	return m_components[2];
} // ColorRGBA< TComponent >::GetBlue

template <typename TComponent>
const TComponent&
ColorRGBA<TComponent>::GetAlpha() const {
	return m_components[3];
} // ColorRGBA< TComponent >::GetAlpha

template <typename TComponent>
ColorRGBA<TComponent>&
ColorRGBA<TComponent>::operator=(const ColorRGBA<TComponent>& color) {
	m_components[0] = color.m_components[0];
	m_components[1] = color.m_components[1];
	m_components[2] = color.m_components[2];
	m_components[3] = color.m_components[3];

	return *this;
} // ColorRGBA< TComponent >::operator=

template <typename TComponent>
bool ColorRGBA<TComponent>::operator==(const ColorRGBA<TComponent>& color) const {
	if ((color.m_components[0] == m_components[0]) &&
		(color.m_components[1] == m_components[1]) &&
		(color.m_components[2] == m_components[2]) &&
		(color.m_components[3] == m_components[3]))
		return true;
	return false;
} // ColorRGBA< TComponent >::operator==

template <typename TComponent>
bool ColorRGBA<TComponent>::operator!=(const ColorRGBA<TComponent>& color) const {
	return !(*this == color);
} // ColorRGBA< TComponent >::operator!=

template <typename TComponent>
bool ColorRGBA<TComponent>::operator!() const {
	if ((color.m_components[0] == reinterpret_cast<TComponent>(0.0)) &&
		(color.m_components[1] == reinterpret_cast<TComponent>(0.0)) &&
		(color.m_components[2] == reinterpret_cast<TComponent>(0.0)) &&
		(color.m_components[3] == reinterpret_cast<TComponent>(0.0)))
		return true;
	return false;
} // ColorRGBA< TComponent >::operator!

template <typename TComponent>
ColorRGBA<TComponent>::operator bool() const {
	return !(!(*this));
} // ColorRGBA< TComponent >::operator bool

template <typename TComponent>
ColorRGBA<TComponent>::operator TComponent*() {
	return m_components;
} // ColorRGBA< TComponent >::operator TComponent*

template <typename TComponent>
ColorRGBA<TComponent>::operator const TComponent*() const {
	return m_components;
} // ColorRGBA< TComponent >::operator const TComponent*

template <typename TComponent>
ColorRGBA<TComponent>
ColorRGBA<TComponent>::operator+(const ColorRGBA<TComponent>& color) const {
	ColorRGBA<TComponent> resultColor;

	resultColor.m_components[0] = m_components[0] + color.m_components[0];
	resultColor.m_components[1] = m_components[1] + color.m_components[1];
	resultColor.m_components[2] = m_components[2] + color.m_components[2];
	resultColor.m_components[3] = m_components[3] + color.m_components[3];

	return resultColor;
} // ColorRGBA< TComponent >::operator+

template <typename TComponent>
ColorRGBA<TComponent>
ColorRGBA<TComponent>::operator-(const ColorRGBA<TComponent>& color) const {
	ColorRGBA<TComponent> resultColor;

	resultColor.m_components[0] = m_components[0] - color.m_components[0];
	resultColor.m_components[1] = m_components[1] - color.m_components[1];
	resultColor.m_components[2] = m_components[2] - color.m_components[2];
	resultColor.m_components[3] = m_components[3] - color.m_components[3];

	return resultColor;
} // ColorRGBA< TComponent >::operator-

template <typename TComponent>
ColorRGBA<TComponent>
	ColorRGBA<TComponent>::operator*(const ColorRGBA<TComponent>& color) const {
	ColorRGBA<TComponent> resultColor;

	resultColor.m_components[0] = m_components[0] * color.m_components[0];
	resultColor.m_components[1] = m_components[1] * color.m_components[1];
	resultColor.m_components[2] = m_components[2] * color.m_components[2];
	resultColor.m_components[3] = m_components[3] * color.m_components[3];

	return resultColor;
} // ColorRGBA< TComponent >::operator*

template <typename TComponent>
ColorRGBA<TComponent>
ColorRGBA<TComponent>::operator/(const ColorRGBA<TComponent>& color) const {
	ColorRGBA<TComponent> resultColor;

	resultColor.m_components[0] = m_components[0] / color.m_components[0];
	resultColor.m_components[1] = m_components[1] / color.m_components[1];
	resultColor.m_components[2] = m_components[2] / color.m_components[2];
	resultColor.m_components[3] = m_components[3] / color.m_components[3];

	return resultColor;
} // ColorRGBA< TComponent >::operator/
}; // namespace KEP
