/******************************************************************************
 CollapsibleGroup.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_COLLAPSIBLEGROUP_H__D52CACDF_E803_4410_8E69_96902E57B045__INCLUDED_)
#define AFX_COLLAPSIBLEGROUP_H__D52CACDF_E803_4410_8E69_96902E57B045__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CollapsibleGroup.h : header file
//
#include <afxtempl.h>
#include "KEPImageToggleButton.h"

/////////////////////////////////////////////////////////////////////////////
// CCollapsibleGroup window

class CCollapsibleGroup : public CKEPImageToggleButton {
	// Construction
public:
	CCollapsibleGroup();
	virtual ~CCollapsibleGroup();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCollapsibleGroup)
protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Implementation
public:
	void SetCheck(int nCheck);
	void SetFrame(CWnd* pFrame);
	void SetEnableResize(BOOL bEnableResize) { m_bEnableResize = bEnableResize; }
	BOOL GetEnableResize() const { return m_bEnableResize; }

	// Generated message map functions
protected:
	BOOL m_bPrevState;
	static BOOL CALLBACK WndEnumProc(HWND hWnd, LPARAM lParam);
	void Collapse();
	void Expand();
	CWnd* m_pFrame;
	CUIntArray m_uaIDs;
	CRect m_rcMaxSize;
	CRect m_rcMinSize;
	CArray<HWND, HWND> m_haInsideWindows;
	CArray<HWND, HWND> m_haOutsideWindows;
	BOOL m_bEnableResize;
	//{{AFX_MSG(CCollapsibleGroup)
	afx_msg BOOL OnClicked();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	void RecalcLayout();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLLAPSIBLEGROUP_H__D52CACDF_E803_4410_8E69_96902E57B045__INCLUDED_
