///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#ifndef IMAGE_SELECTION
#define IMAGE_SELECTION DRF_TEXTURE_IMPORT_OPT // default image version selection (0=none, 1=v3.17.0)
#endif

#if (IMAGE_SELECTION == 1)
#include "FreeImage-3.17.0\x32\FreeImage.h"
enum IMAGE_FMT {
	IMAGE_FMT_UNKNOWN = FIF_UNKNOWN,
	IMAGE_FMT_BMP = FIF_BMP,
	IMAGE_FMT_JPEG = FIF_JPEG,
	IMAGE_FMT_TIFF = FIF_TIFF,
	IMAGE_FMT_GIF = FIF_GIF,
	IMAGE_FMT_PNG = FIF_PNG,
	IMAGE_FMT_DDS = FIF_DDS,
	IMAGE_FMT_TARGA = FIF_TARGA
};
#else
enum IMAGE_FMT {
	IMAGE_FMT_UNKNOWN,
	IMAGE_FMT_BMP,
	IMAGE_FMT_JPEG,
	IMAGE_FMT_TIFF,
	IMAGE_FMT_GIF,
	IMAGE_FMT_PNG,
	IMAGE_FMT_DDS,
	IMAGE_FMT_TARGA
};
#endif

// Returns installed image version.
std::string ImageVersion();
