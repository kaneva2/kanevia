///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Legacy Skin Vertex-Bone Assignments

#include "Core/Math/Vector.h"

namespace KEP {

class IMemSizeGadget;

enum class SkinVertexBlendType {
	Disabled = 0, // rigid
	Position_Normal = 1, // blend position and normal
	Position_Normal_Tangent = 2 // deprecated
};

struct VertexAssignment {
	byte boneIndex;
	short vertexIndex;
	std::vector<short> verticesCarried; // optimizing vertex movement	// Array of vertex IDs sharing the same position as current vertex?

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

struct VertexBoneWeight {
	byte boneIndex; // Bone index
	float weight; // Weight
	Vector3f positionInBoneSpace; // Current vertex's position/normal in the coordinate systems of each influencing bone
	Vector3f normalInBoneSpace;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

struct VertexAssignmentBlended {
	std::vector<VertexBoneWeight> vertexBoneWeights;
	short vertexIndex; // Current vertex being influenced by the bones listed above
	std::vector<short> verticesCarried; // optimizing vertex movement	// Array of vertex IDs sharing the same position as current vertex?

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

} // namespace KEP
