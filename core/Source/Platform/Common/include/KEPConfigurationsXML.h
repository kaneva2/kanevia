#pragma once
/******************************************************************************
 KEPConfigurationsXML.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

static const char* const SUPPORTED_WORLDS_CONFIG_FILE = "SupportedWorldsConfiguration.xml";
// <SupportedWorlds>
//		<World>
//			<Name></Name>
//			<Id></Id>
//		</World>
// </SupportedWorlds>
static const char* const WORLDS_TAG = "SupportedWorlds";
static const char* const WORLD_TAG = "World";
static const char* const WORLD_NAME_TAG = "Name";
static const char* const WORLD_DISPNAME_TAG = "DisplayName";
static const char* const ID_TAG = "Id";
static const char* const MAX_OCCUPANCY_TAG = "MaxOccupancy";
static const char* const ZONE_ID_TAG = "ZoneIndex";
static const char* const VISIBILITY_TAG = "Visibility";

static const char* const SPAWN_POINTS_CONFIG_FILE = "SpawnPointsConfiguration.xml";
// <SpawnPoints>
//		<SpawnPoint>
//			<POSZ></POSZ>
//			<POSY></POSY>
//			<POSX></POSX>
//			<RefId></RefId>
//			<Name></Name>
//  		<Rotation></Rotation>
//		</SpawnPoint>
// </SpawnPoints>
static const char* const SPAWNS_TAG = "SpawnPoints";
static const char* const SPAWN_TAG = "SpawnPoint";
static const char* const SPAWN_NAME_TAG = "Name";
static const char* const REFID_TAG = "RefId";
static const char* const ROTATION_TAG = "Rotation";
static const char* const POS_X = "POSX";
static const char* const POS_Y = "POSY";
static const char* const POS_Z = "POSZ";

static const char* const HOUSING_ZONE_DB_FILE = "HousingZoneDB.dat";

static const char* const NETWORK_DEFINITION_ZONE_DB_FILE = "NetworkDefinitions.xml";

static const char* const NETWORK_DEFINITIONS_TAG = "NetworkDefinitions";
static const char* const FREQUENCY_LIMIT_TAG = "FrequencyLimit";
static const char* const CLIENTSIDE_FIRING_TAG = "ClientSideFiring";
static const char* const PERSISTENT_TYPE_TAG = "PersistentType";
static const char* const MAX_PLAYERS_TAG = "MaxPlayers";
static const char* const FREQUENCY_BUFFER_TAG = "FrequencyBuffer";
static const char* const FREQUENCY_INCREMENT_TAG = "FrequencyIncrement";
static const char* const CALCULATE_RADIUS_TAG = "CalculateRadius";
static const char* const CALCULATE_RADIUS_ITEMS_TAG = "CalculateRadiusItems";
static const char* const PASSWORD_REQUIRED_TAG = "PasswordRequired";
static const char* const DEAD_ENTITY_DISSOLVE_TIME_TAG = "DeadEntityDissolveTime";
static const char* const AI_SERVER_TAG = "AIServer";
static const char* const AI_SERVER_CYCLE_DURATION_TAG = "AIServerCycleDuration";
static const char* const DEAD_ENTITY_NPC_DISSOLVE_TIME_TAG = "DeadEntityNPCDissolveTime";
static const char* const ENABLE_AUTO_BACKUP_TAG = "EnableAutoBackup";
static const char* const GOOD_BEHAVIOR_DURATION_TAG = "GoodBehaviourDuration";
static const char* const MURDER_COUNT_MAX_TAG = "MurderCountMax";
static const char* const LEVEL_RESTRICTION_TAG = "LevelRestriction";
static const char* const LEVEL_MAX_BANDWIDTH_TAG = "MaxBandwidthPerPerson";
static const char* const LIMIT_SLEEP_MS = "LimitSleepMs";
static const char* const MAX_NON_ADMIN_PACKET_TAG = "NonAdminPacketSize";
static const char* const PACKET_SIZE_VIOLATION_HARD_LIMIT_PERIOD_TAG = "PacketSizeViolationHardLimitPeriod"; // In milliseconds
static const char* const PACKET_SIZE_VIOLATION_HARD_LIMIT_TAG = "PacketSizeViolationHardLimit"; // Hard limit of oversized packets allowed per period: disconnect session once exceeded.
static const char* const PACKET_SIZE_VIOLATION_SOFT_LIMIT_PERIOD_TAG = "PacketSizeViolationSoftLimitPeriod"; // In milliseconds
static const char* const PACKET_SIZE_VIOLATION_SOFT_LIMIT_TAG = "PacketSizeViolationSoftLimit"; // Soft limit of oversized packets allowed per period: additional oversized packets will be dropped
static const char* const HOUSE_SIGHT_RANGE_TAG = "HousingSightRange";
static const char* const SQL_TABLE_NAME_TAG = "SQLTableName";
static const char* const SQL_TABLE_CATALOG_TAG = "SQLTableCatalog";
static const char* const SQL_SERVER_ID_TAG = "SQLServerId";
static const char* const SQL_SERVER_PASSWORD_TAG = "SQLServerPassword";
static const char* const SQL_SERVER_USERNAME_TAG = "SQLServerUsername";
static const char* const ALWAYS_START_IN_HOUSING_TAG = "AlwaysStartInHousing";
static const char* const CLIENT_DISCONNECT_TIMEOUT_TAG = "ClientDisconnectTimeout";
static const char* const CLIENT_EXIT_TIMEOUT_TAG = "ClientExitTimeout";
static const char* const MAX_PLAYER_BACKUP_TIME_TAG = "MaxPlayerBackupTime";
static const char* const MAX_PLAYERS_ON_ACCT = "MaxPlayersOnAcct";
static const char* const INITIAL_PLAYERS_CASH = "InitialPlayerCash";
static const char* const INITIAL_PLAYERS_BANK_CASH = "InitialPlayerBankCash";
static const char* const ALLOWBANKANYWHERE_TAG = "AllowCashBankingAnywhere";
static const char* const MAX_PLAYER_INVENTORY_TAG = "MaxPlayerInventory";
static const char* const MAX_BANK_INVENTORY_TAG = "MaxBankInventory";
static const char* const MAX_STACK_LIMIT_TAG = "MaxStackInventoryLimit";
static const char* const MAX_MOVEMENT_DISTANCE_TAG = "MaxMovementDistancePerSecond";
static const char* const VIOLATION_TO_DISC_TAG = "ViolationsToDisconnect";
static const char* const ISEC_VIOLATION_TO_DISC_TAG = "ISecViolationsToDisconnect";
static const char* const DISABLED_EDBS_TAG = "DisabledEdbs";
static const char* const TRYON_DEFAULT_EXPIRATION_DURATION = "TryOnItemExpireDuration";
static const char* const REQUEST_PROCESSING_ERROR_TIME_MS_TAG = "RequestProcessingErrorTimeMs";
static const char* const REQUEST_PROCESSING_INFO_TIME_MS_TAG = "RequestProcessingInfoTimeMs";
static const char* const NEWBIE_LEVEL_TAG = "NewbieLevel";
static const char* const INITIAL_ZONE_INDEX_TAG = "InitialZoneIndex";
static const char* const INITIAL_ZONE_INSTANCE_TAG = "InitialZoneInstanceId";
static const char* const INITIAL_ZONE_SPAWN_INDEX_TAG = "InitialZoneSpawnIndex";
static const char* const LOGGING_ENABLED_TAG = "EnableNetworkLogging";
static const char* const PING_TIMEOUT_SEC_TAG = "PingTimeoutSec";
static const char* const DEFAULT_PLACE_URL_TAG = "DefaultPlaceUrl";
static const char* const INITIAL_ARENA_ZONE_INDEX_TAG = "InitialArenaZoneIndex";
static const char* const ALERT_FREQUENCY_TAG = "AlertFrequency";

static const int DFLT_ALERT_FREQUENCY = 20;
