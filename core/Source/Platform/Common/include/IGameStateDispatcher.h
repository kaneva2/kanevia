#pragma once

namespace KEP {
class IBackgroundDBEvent;
class IServerNetwork;

/// class to allow CMP to get this class w/o sucking
/// db stuff in to client
class IGameStateDispatcher {
public:
	/// queue an event to the background
	virtual void QueueEvent(IBackgroundDBEvent *e) = 0;
	virtual void SetServerNetwork(IServerNetwork *s) = 0;
};

} // namespace KEP