///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/Unicode.h"
#include <afxcmn.h>
#include <afxdlgs.h>

namespace KEP {

inline BOOL OpenCFile(CFile& cfile, const char* pszFileNameUtf8, UINT nOpenFlags, CFileException* pError = NULL) {
	return cfile.Open(Utf8ToUtf16(pszFileNameUtf8).c_str(), nOpenFlags, pError);
}

inline BOOL ReadString(CArchive& ar, CStringA& str) {
	try {
		while (true) {
			char ch;
			ar >> ch;
			if (ch == '\r' || ch == '\n') {
				if (ch == '\r')
					ar >> ch; // Should be '\n'
				break; // Don't add newline to string.
			}
			str += ch;
		}
	} catch (CArchiveException* e) {
		if (e && e->m_cause == CArchiveException::endOfFile)
			e->Delete(); // Treat end-of-file as end-of-line.
		else
			throw;
	}
	return TRUE;
}

inline void WriteString(CArchive& ar, const char* psz) {
	ar.Write(psz, strlen(psz));
}

typedef void(AFXAPI* DDX_CSTRING_FUNC)(CDataExchange* pDX, int nIDC, CStringW& value);

template <DDX_CSTRING_FUNC ddx_func>
inline void DDX_Utf8(CDataExchange* pDX, int nIDC, CStringA& value) {
	CStringW valueW;

	if (!pDX->m_bSaveAndValidate)
		valueW = Utf8ToUtf16(value).c_str();

	ddx_func(pDX, nIDC, valueW);

	if (pDX->m_bSaveAndValidate)
		value = Utf16ToUtf8(valueW.GetString()).c_str();
}

inline void DDX_CBStringExact(CDataExchange* pDX, int nIDC, CStringA& value) {
	DDX_Utf8<::DDX_CBStringExact>(pDX, nIDC, value);
}

inline void DDX_Text(CDataExchange* pDX, int nIDC, CStringA& value) {
	DDX_Utf8<::DDX_Text>(pDX, nIDC, value);
}

inline void DDV_MaxChars(CDataExchange* pDX, const CStringA& value, int nChars) {
	if (value.GetLength() > nChars)
		pDX->Fail();
}

inline void DDX_CBString(CDataExchange* pDX, int nIDC, CStringA& value) {
	DDX_Utf8<::DDX_CBString>(pDX, nIDC, value);
}

inline int AddString(CComboBox& comboBox, const char* pszStringUtf8) {
	return comboBox.AddString(Utf8ToUtf16(pszStringUtf8).c_str());
}

inline int AddString(CListBox& listBox, const char* pszStringUtf8) {
	return listBox.AddString(Utf8ToUtf16(pszStringUtf8).c_str());
}

inline void GetText(const CListBox& lb, int nIndex, CStringA& rString) {
	CStringW rStringW;
	lb.GetText(nIndex, rStringW);
	rString = Utf16ToUtf8(rStringW).c_str();
}

inline void GetLBText(const CComboBox& cb, int nIndex, CStringA& rString) {
	CStringW rStringW;
	cb.GetLBText(nIndex, rStringW);
	rString = Utf16ToUtf8(rStringW).c_str();
}

inline CStringA GetItemText(const CListCtrl& lc, int nItem, int nSubItem) {
	CStringW ret = lc.GetItemText(nItem, nSubItem);
	return Utf16ToUtf8(ret).c_str();
}

inline void SetItemText(CListCtrl& lc, int nItem, int nSubItem, const char* psz) {
	lc.SetItemText(nItem, nSubItem, Utf8ToUtf16(psz).c_str());
}

inline int InsertItem(CListCtrl& lc, int nItem, const char* pszItem) {
	return lc.InsertItem(nItem, Utf8ToUtf16(pszItem).c_str());
}

inline int InsertString(CListBox& lb, int nIndex, const char* pszItem) {
	return lb.InsertString(nIndex, Utf8ToUtf16(pszItem).c_str());
}

inline int GetDlgItemTextUtf8(const CWnd* pWnd, int nID, CStringA& rString) {
	CStringW strW;
	int ret = pWnd->GetDlgItemTextW(nID, strW);
	rString = Utf16ToUtf8(strW).c_str();
	return ret;
}

inline void SetDlgItemTextUtf8(CWnd* pWnd, int nID, const char* psz) {
	pWnd->SetDlgItemTextW(nID, Utf8ToUtf16(psz).c_str());
}

inline void GetWindowTextUtf8(const CWnd& wnd, CStringA& rString) {
	CStringW rStringW;
	wnd.GetWindowTextW(rStringW);
	rString = Utf16ToUtf8(rStringW).c_str();
}

inline void SetWindowTextUtf8(CWnd& wnd, const CStringA& string) {
	CStringW stringW = Utf8ToUtf16(string).c_str();
	wnd.SetWindowTextW(stringW);
}

inline CStringA GetPathName(const CFileDialog& fd) {
	return Utf16ToUtf8(fd.GetPathName()).c_str();
}

inline CStringA GetFileName(const CFileDialog& fd) {
	return Utf16ToUtf8(fd.GetFileName()).c_str();
}

inline int AfxMessageBoxUtf8(const char* pszText, UINT nType = MB_OK, UINT nIDHelp = 0) {
	return AfxMessageBox(Utf8ToUtf16(pszText).c_str(), nType, nIDHelp);
}

} // namespace KEP
