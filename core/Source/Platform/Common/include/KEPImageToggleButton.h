/******************************************************************************
 KEPImageToggleButton.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_KEPIMAGETOGGLEBUTTON_H__16C6D980_BD45_11D3_BDA3_00104B133581__INCLUDED_)
#define AFX_KEPIMAGETOGGLEBUTTON_H__16C6D980_BD45_11D3_BDA3_00104B133581__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// KEPImageToggleButton.h : header file
//

#include "KEPImageButton.h"

class CKEPImageToggleButton : public CKEPImageButton {
	DECLARE_DYNAMIC(CKEPImageToggleButton);

public:
	CKEPImageToggleButton();
	CKEPImageToggleButton(Gdiplus::Color &fontColor, WCHAR *fontName, UINT fontSize, Gdiplus::FontStyle fontStyle);
	virtual ~CKEPImageToggleButton();

	BOOL LoadImages(CStringA pathNormal, CStringA pathSelected, CStringA pathHover = CStringA(""), CStringA pathDisabled = CStringA(""));

	void SetSelected(BOOL bSelected);
	void SetShowDepress(BOOL bShowDepress) { m_bShowDepress = bShowDepress; }

	BOOL GetSelected() const { return m_bSelected; }
	BOOL GetShowDepress() const { return m_bShowDepress; }

protected:
	// Generated message map functions

	//{{AFX_MSG(CKEPImageToggleButton)
	afx_msg BOOL OnClicked();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKEPImageToggleButton)
protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

protected:
	BOOL m_bSelected;
	BOOL m_bShowDepress;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KEPIMAGETOGGLEBUTTON_H__16C6D980_BD45_11D3_BDA3_00104B133581__INCLUDED_)
