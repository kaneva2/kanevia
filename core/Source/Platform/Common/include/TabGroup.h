/******************************************************************************
 TabGroup.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_TABGROUP_H__D52CACDF_E803_4410_8E69_96902E57B045__INCLUDED_)
#define AFX_TABGROUP_H__D52CACDF_E803_4410_8E69_96902E57B045__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TabGroup.h : header file
//
#include <afxtempl.h>
#include "KEPImageToggleButton.h"
#include "IObserver.h"

/////////////////////////////////////////////////////////////////////////////
// CTabGroup window

class CTabGroup : public CKEPImageToggleButton, public IObserver, public ISubject {
	// Construction
public:
	CTabGroup();
	virtual ~CTabGroup();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabGroup)
protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Implementation
public:
	virtual void AddObserver(IObserver* pObserver);
	virtual void DoUpdate();

	void SetCheck(int nCheck);
	void SetFrame(CWnd* pFrame);
	void SetParentFrame(CWnd* pParentFrame);
	void SetEnableResize(BOOL bEnableResize) { m_bEnableResize = bEnableResize; }
	BOOL GetEnableResize() const { return m_bEnableResize; }

	// Generated message map functions
protected:
	static BOOL CALLBACK WndEnumProc(HWND hWnd, LPARAM lParam);
	void Collapse();
	void Expand();

	//{{AFX_MSG(CTabGroup)
	afx_msg BOOL OnClicked();
	//}}AFX_MSG

	BOOL m_bPrevState;
	CWnd* m_pFrame;
	CWnd* m_pParentFrame;
	CRect m_rcMaxSize;
	CRect m_rcMinSize;
	CArray<HWND, HWND> m_haInsideWindows;
	BOOL m_bEnableResize;

	typedef std::vector<IObserver*> ObserverVector;
	typedef ObserverVector::iterator ObserverIterator;

	ObserverVector m_observers;

	DECLARE_MESSAGE_MAP()

private:
	void RecalcLayout();
	void PositionChildWindows();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABGROUP_H__D52CACDF_E803_4410_8E69_96902E57B045__INCLUDED_
