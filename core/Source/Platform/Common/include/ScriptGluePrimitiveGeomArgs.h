///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptGlueCommon.h"
#include "PrimitiveGeomArgs.h"

template <typename T>
struct ScriptValueHandler<KEP::PrimitiveGeomArgs<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::PrimitiveGeomArgs<T>* pValue) {
		memset(pValue, 0, sizeof(KEP::PrimitiveGeomArgs<T>));

		bool bSucceeded = GetScriptTableValues(pScriptArgManager, "type", &pValue->type);
		if (!bSucceeded) {
			return false;
		}

		switch (pValue->type) {
			case KEP::PrimitiveType::Sphere:
				return GetScriptTableValues(pScriptArgManager, "radius", &(pValue->sphere.radius));
			case KEP::PrimitiveType::Cuboid:
				return GetScriptTableValues(pScriptArgManager, "min", &(pValue->cuboid.min)) &&
					   GetScriptTableValues(pScriptArgManager, "max", &(pValue->cuboid.max));
			case KEP::PrimitiveType::Cylinder:
				pValue->axis = 1; // Default axis: Y
				GetScriptTableValues(pScriptArgManager, "axis", &(pValue->axis));
				GetScriptTableValues(pScriptArgManager, "base", &(pValue->cylinder.base));
				return GetScriptTableValues(pScriptArgManager, "radius", &(pValue->cylinder.radius)) &&
					   GetScriptTableValues(pScriptArgManager, "height", &(pValue->cylinder.height));
			case KEP::PrimitiveType::RectangularFrustum:
				pValue->axis = 2; // Default axis: Z
				GetScriptTableValues(pScriptArgManager, "axis", &(pValue->axis));
				return GetScriptTableValues(pScriptArgManager, "fov", &(pValue->rectangularFrustum.fov)) &&
					   GetScriptTableValues(pScriptArgManager, "aspect", &(pValue->rectangularFrustum.aspect)) &&
					   GetScriptTableValues(pScriptArgManager, "near", &(pValue->rectangularFrustum.nearPlane)) &&
					   GetScriptTableValues(pScriptArgManager, "far", &(pValue->rectangularFrustum.farPlane));
			case KEP::PrimitiveType::CircularFrustum:
				pValue->axis = 2; // Default axis: Z
				GetScriptTableValues(pScriptArgManager, "axis", &(pValue->axis));
				return GetScriptTableValues(pScriptArgManager, "fov", &(pValue->circularFrustum.fov)) &&
					   GetScriptTableValues(pScriptArgManager, "near", &(pValue->circularFrustum.nearPlane)) &&
					   GetScriptTableValues(pScriptArgManager, "far", &(pValue->circularFrustum.farPlane));
			default:
				assert(false);
				return false;
		}
	}
	// "Push" is not supported
};
