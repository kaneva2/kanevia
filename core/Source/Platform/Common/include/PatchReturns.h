#pragma once

// return codes from CheckAndPatchGameUrl

// an error occurred
const LONG UP_ERROR = 0;

// the URL specifies the same game
// it will not patch
const LONG UP_SAME_GAME = 1;

// the URL specifies a different game with the same parent,
// it will patch
const LONG UP_SIBLING_GAME = 2;

// the URL specifies the parent game
// it will not patch
const LONG UP_PARENT_GAME = 3;

// the URL specifies a totally different game
// it will patch
const LONG UP_DIFFERENT_GAME = 4;

// User-canceled
const LONG UP_CANCELED = 5;

