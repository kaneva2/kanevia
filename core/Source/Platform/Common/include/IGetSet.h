///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "SDKCommon.h"

#include "Common\KEPUtil\RefPtr.h"
#include "Core/Math/KEPMath.h"

namespace KEP {

// enumeration of all the possible types for a GetSet item
typedef int EGetSetType;

// flags and types pushed up from GetSet as a subset so as not to expose too much
const EGetSetType amBool = 1;
const EGetSetType amInt = 8;
const EGetSetType amLong = 10;
const EGetSetType amDouble = 14;
const EGetSetType amString = 15;

// add member flags
const ULONG amfTransient = 0x02000000; // this item is not serialized
	// dynamcially added always set     = 0x04000000;
const ULONG amfReplicate = 0x08000000; // transfer between server and client, for dynamic
const ULONG amfSaveAcrossSpawn = 0x10000000; // save across a spawn for dynamic player data
const ULONG amfMask = 0x1e000000;

// DRF - GetSet Index Error Enumeration
const ULONG ADD_MEMBER_DUP_WRONG_TYPE = 0xfffffffe;
const ULONG ADD_MEMBER_BAD_TYPE = 0xfffffffd;
const ULONG FIND_ID_NOT_FOUND = 0xffffffff;

// DRF - GetSet Index Ok Validation
#define GS_OK(rv) ((rv != ADD_MEMBER_DUP_WRONG_TYPE) && (rv != ADD_MEMBER_BAD_TYPE) && (rv != FIND_ID_NOT_FOUND))

// flags for the GetSet flags
const ULONG GS_FLAG_READ_ONLY = 0x00000000; // just for fun
const ULONG GS_FLAG_READ_WRITE = 0x00010000;
const ULONG GS_FLAG_EXPOSE = 0x00020000;
const ULONG GS_FLAG_CAN_ADD = 0x00040000; // for array, can we add new items to it and delete from it
const ULONG GS_FLAG_INSTANCE_NAME = 0x00080000; // for arrays, this item is the name, only one item should have this
const ULONG GS_FLAG_DELETE = 0x00100000; // indicate that this GetSet object should be deleted
const ULONG GS_FLAG_HIDDENLIST = 0x00200000; // in this list node hidden, but children shown?
const ULONG GS_FLAG_ADDONLY = 0x00400000; // can only edit this item in Add, can't edit it after add
const ULONG GS_FLAG_OPENTREENODE = 0x00800000; // by default, open this tree node
const ULONG GS_FLAG_HIDDENCHILDREN = 0x01000000; // children of this list are hidden in tree
const ULONG GS_FLAG_TRANSIENT = 0x02000000; // this item is not serialized, mainly for dynamic
const ULONG GS_FLAG_DYNAMIC_ADD = 0x04000000; // this was added on-the-fly
const ULONG GS_FLAG_REPLICATE = 0x08000000; // transfer between server and client, for dynamic
const ULONG GS_FLAG_SAVE_SPAWN = 0x10000000; // save across a spawn for dynamic player data

const ULONG GS_DYNAMIC_FLAG_MASK = 0x1e000000;

const ULONG GS_FLAG_PASSWORD = 0x00008000; // password-type string

// flags for list types
const ULONG GS_FLAG_LIST_MULTISEL = 0x00001000;
const ULONG GS_FLAG_LIST_COMBO = 0x00002000;

const ULONG GS_FLAGS_DEFAULT = GS_FLAG_READ_WRITE | GS_FLAG_EXPOSE;
const ULONG GS_FLAGS_ACTION = GS_FLAG_READ_ONLY | GS_FLAG_EXPOSE;

// array bit for simple-type arrays of types below
const int gsSimpleArray = 0x10000000;

// simple types
const int gsBool = 1;
const int gsBOOL = 2;
const int gsChar = 3;
const int gsByte = 4;
const int gsShort = 5;
const int gsUshort = 6;
const int gsWord = 7;
const int gsInt = 8;
const int gsUint = 9;
const int gsLong = 10;
const int gsUlong = 11;
const int gsDword = 12;
const int gsFloat = 13;
const int gsDouble = 14;
const int gsString = 15; // STL string
const int gsCString = 16; // MFC CStringA
const int gsCharArray = gsChar | gsSimpleArray; // fixed size array of char

// graphic
const int gsPoint = 115;
const int gsRect = 116;
const int gsRgbaColor = 117;
const int gsD3dvector = 118;

// special items
const int gsDate = 200; // "YYYYMMDD"
const int gsDatetime = 201; // "YYYYMMDD HH:MM:SS.xxx"
const int gsIpAddress = 202; // "a.b.c.d"
const int gsListOfValues = 203; // for combo boxes or list boxes, see extra flags for these above
const int gsCustom = 204; // derived class should do something for these
const int gsAction = 205; // for admin

// nested GetSet object w/i this one
const int gsObjectPtr = 300; // pointer to GetSet from CObject
const int gsObListPtr = 301; // CObList that has GetSet-derived classes in it

// get sets
const int gsGetSetPtr = 400; // pointer to a GetSet
const int gsGetSetVector = 401; // vector of GetSet pointers

// base interface to all getset objects
class IGetSet {
public:
	IGetSet() {}

	virtual ~IGetSet() {
		// DRF - Invalidate Reference Pointer (as IGetSet*)
		REF_PTR_DEL(this);
	}

	// version of this class interface
	static BYTE Version() {
		return 1;
	}

	virtual UINT GetNameId() = 0;

	virtual Vector3f& GetVector3f(ULONG index) = 0;
	virtual ULONG& GetULONG(ULONG index) = 0;
	virtual INT& GetINT(ULONG index) = 0;
	virtual int& GetBOOL(ULONG index) = 0;
	virtual bool& Getbool(ULONG index) = 0;
	virtual std::string& Getstring(ULONG index) = 0;
	virtual IGetSet* GetObjectPtr(ULONG index) = 0;
	virtual void GetColor(ULONG index, float* r, float* g, float* b, float* a = 0) = 0;

	virtual std::string GetString(ULONG index) = 0;

	virtual double GetNumber(ULONG index) = 0;

	virtual void SetNumber(ULONG index, LONG number) = 0;
	virtual void SetNumber(ULONG index, ULONG number) = 0;
	virtual void SetNumber(ULONG index, double number) = 0;
	virtual void SetNumber(ULONG index, int value) = 0;
	virtual void SetNumber(ULONG index, bool value) = 0;
	virtual void SetString(ULONG index, const char* s) = 0;
	virtual void SetVector3f(ULONG index, Vector3f v) = 0;
	virtual void SetColor(ULONG index, float r, float g, float b, float a = 0) = 0;

	virtual ULONG GetArrayCount(ULONG indexOfArray) = 0;
	virtual IGetSet* GetObjectInArray(ULONG indexOfArray, ULONG indexInArray) = 0;

	virtual ULONG AddNewIntMember(const char* name, int defaultValue, ULONG flags = amfTransient) = 0;
	virtual ULONG AddNewDoubleMember(const char* name, double defaultValue, ULONG flags = amfTransient) = 0;
	virtual ULONG AddNewStringMember(const char* name, const char* defaultValue, ULONG flags = amfTransient) = 0;
	virtual ULONG FindIdByName(const char* name, EGetSetType* type = 0) = 0;
};

} // namespace KEP
