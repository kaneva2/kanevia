/******************************************************************************
 SafeZonePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SAFE_ZONEPROPERTIESCTRL_H_)
#define _SAFE_ZONEPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SafeZonePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "CEXMeshClass.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CSafeZonePropertiesCtrl window

class CSafeZonePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSafeZonePropertiesCtrl();
	virtual ~CSafeZonePropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetSafeZoneName() const {
		return Utf16ToUtf8(m_safeZoneName).c_str();
	}
	int GetWorldChannelID() const {
		return m_worldChannelID;
	}
	BOOL GetAllowRebirth() const {
		return m_allowRebirth;
	}

	// modifiers
	void SetSafeZoneName(CStringA safeZoneName) {
		m_safeZoneName = Utf8ToUtf16(safeZoneName).c_str();
	}
	void SetWorldChannelID(int worldChannelID) {
		m_worldChannelID = worldChannelID;
	}
	void SetAllowRebirth(BOOL allowRebirth) {
		m_allowRebirth = allowRebirth;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSafeZonePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSafeZonePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	enum { INDEX_ROOT = 1,
		INDEX_SAFE_ZONE_NAME,
		INDEX_WORLD_CHANNEL_ID,
		INDEX_ALLOW_REBIRTH };

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	void InitChannels();

private:
	CStringW m_safeZoneName;
	int m_worldChannelID;
	BOOL m_allowRebirth;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_SAFE_ZONEPROPERTIESCTRL_H_)
