///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class IMemSizeGadget;

class CCfgObj : public CObject, public GetSet {
	DECLARE_SERIAL(CCfgObj);

public:
	CCfgObj();

	int m_globalCFG; //tracer to global inven for inventory selling buying ect
	int m_armed; //see if need to attach
	int m_quanity;

	void Clone(CCfgObj** clone);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CCfgObjList : public CObList {
public:
	CCfgObjList(){};
	void SafeDelete();
	void Clone(CCfgObjList** clone);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CCfgObjList);
};

} // namespace KEP