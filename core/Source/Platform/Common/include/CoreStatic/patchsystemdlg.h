///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CABFunctionLib.h"
#include "CFilePatchClass.h"

class CPatchSystemDlg : public CDialog {
	// Construction
public:
	CPatchSystemDlg(CWnd* pParent = NULL); // standard constructor
	CFilePatchObjectList* m_patchDatabaseRef;
	// Dialog Data
	//{{AFX_DATA(CPatchSystemDlg)
	enum { IDD = IDD_DIALOG_PATCHSYSTEM };
	CListBox m_patchHistory;
	CStringA m_oldFileName;
	CStringA m_browseNewVersion;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPatchSystemDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListPatchDatabase(int sel);
	// Generated message map functions
	//{{AFX_MSG(CPatchSystemDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonBrowse();
	afx_msg void OnButtonBrowsenewversion();
	afx_msg void OnButtonCreatepatch();
	afx_msg void OnButtonDelete();
	afx_msg void OnBUTTONSaveDatabase();
	afx_msg void OnButtonTestpatch();
	afx_msg void OnBUTTONLoadDatabase();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
