///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ELEMENTSDLG_H__7739C9CC_69B9_41DC_B86C_F0F156ACB849__INCLUDED_)
#define AFX_ELEMENTSDLG_H__7739C9CC_69B9_41DC_B86C_F0F156ACB849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ElementsDlg.h : header file
//
#include "CElementsClass.h"
#include "KEPImageButton.h"
#include "KEPEditDialog.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "ElementsPropertiesCtrl.h"
#include "EditorState.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"

namespace KEP {

class Game;


/////////////////////////////////////////////////////////////////////////////
// CElementsDlg dialog

class CElementsDlg : public CKEPEditDialog {
	DECLARE_DYNAMIC(CElementsDlg)

public:
	CElementsDlg(CElementObjList*& elementDB,
		Game* game = NULL, CWnd* pParent = NULL); // standard constructor
	virtual ~CElementsDlg();

	CElementObjList*& m_elementDBREF;
	// Dialog Data
	//{{AFX_DATA(CElementsDlg)
	enum { IDD = IDD_DIALOG_ELEMENTS };
	CListBox m_elementsList;
	//}}AFX_DATA

	CKEPImageButton m_btnAdd;
	CKEPImageButton m_btnDelete;

	Game* m_game;

	CCollapsibleGroup m_colElements;
	CButton m_grpElements;
	CElementsPropertiesCtrl m_propElements;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CElementsDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	CElementObj* m_selectedElementObj;
	void InListElementals(int sel);
	void LoadElementProperties(int sel);
	CElementObj* GetSelectedElementObj(int sel);
	virtual void OnOK();
	// Generated message map functions
	//{{AFX_MSG(CElementsDlg)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonSaveAs();
	afx_msg void OnButtonLoad();
	afx_msg void OnSelchangeLISTElementsDB();
	afx_msg LRESULT OnPropertyCtrlChanged(WPARAM wp, LPARAM lp);
	afx_msg BOOL DestroyWindow();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	BOOL SaveGameFile(CStringA path);
	BOOL SaveDialog(CStringA path);
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

} // namespace KEP

#endif // !defined(AFX_ELEMENTSDLG_H__7739C9CC_69B9_41DC_B86C_F0F156ACB849__INCLUDED_)
