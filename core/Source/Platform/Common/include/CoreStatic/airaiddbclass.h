///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AIRaidDbClass.h : header file
//
#include "CAIRaidClass.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"

#include "resource.h"
#include "CollapsibleGroup.h"
#include "AITriggeredSpawnsPropertiesCtrl.h"
#include "AITriggeredSpawnPointsPropertiesCtrl.h"
#include "AIConfigsPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CAIRaidDbClass dialog

class CAIRaidDbClass : public CKEPEditDialog {
	// Construction
public:
	CAIRaidDbClass(CAIRaidObjList*& aiRaidRefList,
		CWnd* pParent = NULL); // standard constructor

	CAIRaidObjList*& m_aiRaidRefList;
	// Dialog Data
	//{{AFX_DATA(CAIRaidDbClass)
	enum { IDD = IDD_DIALOG_AIRAIDDLG };
	CListBox m_raidList;
	CListBox m_spawnPointList;
	CListBox m_aiCfgList;
	int m_aiCfg;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAIRaidDbClass)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListRaids(int sel);
	void InListSpawnPoints(int sel);
	void InListAiCfOptions(int sel);

	void SetTriggeredSpawns(int triggeredSpawnIndex);
	void SetSpawnPoint(int spawnPointIndex);
	void SetConfigPerSpawnPoint(int configIndex);

	// Generated message map functions
	//{{AFX_MSG(CAIRaidDbClass)
	virtual BOOL OnInitDialog();
	afx_msg void OnBUTTONAddRaid();
	afx_msg void OnBUTTONAddSpawnPoint();
	afx_msg void OnBUTTONDeleteRaid();
	afx_msg void OnBTNSave();
	afx_msg void OnBTNLoadDB();
	afx_msg void OnBUTTONAddCfgOption();
	afx_msg void OnSelchangeLISTRaidDB();
	afx_msg void OnSelchangeLISTSpawnPointDB();
	afx_msg void OnSelchangeLISTAIcfgDatabase();
	afx_msg void OnBUTTONReplaceRaid();
	afx_msg void OnBUTTONDeleteSpawnPoint();
	afx_msg void OnBUTTONReplaceSpawnPoint();
	afx_msg void OnBUTTONDelCfgOption();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceRaid(int raidIndex);
	void ReplaceSpawnPoint(int raidIndex, int spawnPointIndex);

public:
	int m_raidPercent;
	afx_msg void OnBnClickedButtonLoadunitai();
	afx_msg void OnBnClickedButtonSaveunitai();

	// accessors
	float GetPositionX() const {
		return m_propSpawnPoints.GetPositionX();
	}
	float GetPositionY() const {
		return m_propSpawnPoints.GetPositionY();
	}
	float GetPositionZ() const {
		return m_propSpawnPoints.GetPositionZ();
	}

	// modifiers
	void SetPositionX(float positionX) {
		m_propSpawnPoints.SetPositionX(positionX);
	}
	void SetPositionY(float positionY) {
		m_propSpawnPoints.SetPositionY(positionY);
	}
	void SetPositionZ(float positionZ) {
		m_propSpawnPoints.SetPositionZ(positionZ);
	}

private:
	CAIRaidObj* m_pTriggeredSpawn;
	CAISpawnPoint* m_pSpawnPoint;
	CAICfgOption* m_pConfigPerSpawnPoint;

	CCollapsibleGroup m_colAITriggeredSpawns;
	CButton m_grpAITriggeredSpawns;
	CAITriggeredSpawnsPropertiesCtrl m_propAITriggeredSpawns;

	CCollapsibleGroup m_colSpawnPoints;
	CButton m_grpSpawnPoints;
	CAITriggeredSpawnPointsPropertiesCtrl m_propSpawnPoints;

	CCollapsibleGroup m_colAIConfigs;
	CButton m_grpAIConfigs;
	CAIConfigsPropertiesCtrl m_propAIConfigs;

	CKEPImageButton m_btnAddRAID;
	CKEPImageButton m_btnDeleteRAID;
	CKEPImageButton m_btnReplaceRAID;
	CKEPImageButton m_btnSaveRAIDUnit;
	CKEPImageButton m_btnLoadRAIDUnit;

	CKEPImageButton m_btnAddSpawnPoint;
	CKEPImageButton m_btnDeleteSpawnPoint;
	CKEPImageButton m_btnReplaceSpawnPoint;

	CKEPImageButton m_btnAddConfig;
	CKEPImageButton m_btnDeleteConfig;

	//last selected index in the list box
	int m_previousSelRaid;
	int m_previousSelSpawnPoint;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};
