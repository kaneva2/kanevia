/******************************************************************************
 SelectTextureDialog.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "resource.h"

// CSelectTextureDialog dialog

class CSelectTextureDialog : public CDialog {
	DECLARE_DYNAMIC(CSelectTextureDialog)

public:
	CSelectTextureDialog(CWnd* pParent = NULL); // standard constructor
	virtual ~CSelectTextureDialog();

	// Dialog Data
	enum { IDD = IDD_SELECT_TEXTURE };

	// accessors
	std::string GetBaseDir() const {
		return m_base_dir;
	}
	CStringA GetTextureName() const {
		return m_sTextureName;
	}
	bool GetApplyToAll() const {
		return m_b_apply_to_all == TRUE;
	}

	// modifiers
	void SetBaseDir(std::string base_dir) {
		m_base_dir = base_dir;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	virtual void OnOK();
	virtual BOOL OnInitDialog();

	void InitTextures();

private:
	std::string m_base_dir;
	BOOL m_b_apply_to_all;

	CListBox m_textures;

	CStringA m_sTextureName;
};
