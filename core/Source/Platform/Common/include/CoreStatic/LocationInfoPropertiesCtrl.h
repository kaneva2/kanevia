/******************************************************************************
 LocationInfoPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_LOCATION_INFOPROPERTIESCTRL_H_)
#define _LOCATION_INFOPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LocationInfoPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "CEXMeshClass.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CLocationInfoPropertiesCtrl window

class CLocationInfoPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CLocationInfoPropertiesCtrl();
	virtual ~CLocationInfoPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetZoneId() const {
		return m_zoneId;
	}
	int GetZoneInstanceID() const {
		return m_zoneInstanceID;
	}
	int GetZoneInstanceTypeID() const {
		return m_zoneInstanceTypeID;
	}
	float GetX() const {
		return m_x;
	}
	float GetY() const {
		return m_y;
	}
	float GetZ() const {
		return m_z;
	}
	int GetRotation() const {
		return m_rotation;
	}

	// modifiers
	void SetZoneId(int zoneId) {
		m_zoneId = zoneId;
	}
	void SetZoneInstanceID(int zoneInstanceID) {
		m_zoneInstanceID = zoneInstanceID;
	}
	void SetZoneInstanceTypeID(int zoneInstanceTypeID);
	void SetX(float x);
	void SetY(float y);
	void SetZ(float z);
	void SetRotation(int r);

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLocationInfoPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CLocationInfoPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_ZONE_ID,
		INDEX_ZONE_INSTANCE_TYPE,
		INDEX_ZONE_INSTANCE_ID,
		INDEX_POSITION,
		INDEX_X,
		INDEX_Y,
		INDEX_Z,
		INDEX_ROTATION
	};

	const static int X_MIN = 0;
	const static int X_MAX = 0;
	const static int Y_MIN = 0;
	const static int Y_MAX = 0;
	const static int Z_MIN = 0;
	const static int Z_MAX = 0;

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	void InitZones();
	void InitInstanceTypes();

private:
	int m_zoneId;
	int m_zoneInstanceID;
	int m_zoneInstanceTypeID;
	float m_x;
	float m_y;
	float m_z;
	int m_rotation;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_LOCATION_INFOPROPERTIESCTRL_H_)
