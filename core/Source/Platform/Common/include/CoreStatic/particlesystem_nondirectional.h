///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ParticleSystem_NonDirectional : public CPropertyPage {
	DECLARE_DYNCREATE(ParticleSystem_NonDirectional)

	// Construction
public:
	ParticleSystem_NonDirectional();
	~ParticleSystem_NonDirectional();

	// Dialog Data
	//{{AFX_DATA(ParticleSystem_NonDirectional)
	enum { IDD = IDD_DIALOG_PARTICLESYSTEM_NONDIRECTIONAL };
	int m_scaleEnd;
	int m_scaleStart;
	int m_fadeEnd;
	int m_fadeStart;
	int m_fieldRadius;
	float m_xSpeed;
	float m_ySpeed;
	float m_zSpeed;
	int m_startHeight;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(ParticleSystem_NonDirectional)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(ParticleSystem_NonDirectional)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
