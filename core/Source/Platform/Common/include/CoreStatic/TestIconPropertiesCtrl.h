/******************************************************************************
 TestIconPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_TEST_ICON_PROPERTIESCTRL_H_)
#define _TEST_ICON_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestIconPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "CIconSystemClass.h"

/////////////////////////////////////////////////////////////////////////////
// CTestIconPropertiesCtrl window

class CTestIconPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CTestIconPropertiesCtrl();
	virtual ~CTestIconPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	int GetIconID() const {
		return m_iconID;
	}
	float GetPositionX() const {
		return m_positionX;
	}
	float GetPositionY() const {
		return m_positionY;
	}

	// modifiers
	void SetIconID(int iconID) {
		m_iconID = iconID;
	}
	void SetPositionX(float positionX);
	void SetPositionY(float positionY);

	// methods
	void SetIconList(CIconSysList* iconList);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestIconPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CTestIconPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_ICON,
		INDEX_POSITION,
		INDEX_X,
		INDEX_Y,
		INDEX_TEST_ICON
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	void TestIcon();
	void InitIconList();

private:
	int m_iconID;
	float m_positionX;
	float m_positionY;

	CIconSysList* m_iconList;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_TEST_ICON_PROPERTIESCTRL_H_)
