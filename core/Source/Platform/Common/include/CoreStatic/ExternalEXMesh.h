///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../../RenderEngine/ReMaterial.h"
#include "common\include\MediaParams.h"
#include "Common/KEPUtil/RefPtr.h"

namespace KEP {
class ReMesh;
class ReD3DX9DeviceState;
class CEXMeshObj;

class IExternalEXMesh {
public:
	IExternalEXMesh() {
		// DRF - Add Reference Pointer (as IExternalEXMesh*)
		REF_PTR_ADD(this);
	}

	virtual ~IExternalEXMesh() {
		// DRF - Invalidate Reference Pointer (as IExternalEXMesh*)
		REF_PTR_DEL(this);
	}

	virtual ReMaterialPtr GetReMaterial() {
		return ReMaterialPtr();
	}

	virtual void ScrapeTexture(ReD3DX9DeviceState* devState) = 0;

	virtual int Render(LPDIRECT3DDEVICE9 g_pd3dDevice, CEXMeshObj* mesh, int vertexType) = 0;

public: // IExternalMesh Interface
	virtual std::string ToStr(bool verbose = false) const = 0;

	virtual bool SetMediaParams(const MediaParams& mediaParams, bool allowRewind = true) = 0;
	virtual MediaParams& GetMediaParams() = 0;

	virtual bool SetVolume(double volPct = -1) = 0;
	virtual double GetVolume() const = 0;

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;

	//virtual void Delete() = 0;
};

} // namespace KEP