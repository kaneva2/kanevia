///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_HOUSEPLACEMENTZONEDLG_H__6A502B18_4C1F_42B1_BE70_C54B5822CCDC__INCLUDED_)
#define AFX_HOUSEPLACEMENTZONEDLG_H__6A502B18_4C1F_42B1_BE70_C54B5822CCDC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HousePlacementZoneDlg.h : header file
//
#include "CHousePlacementClass.h"
#include "afxwin.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "PositionPropertiesCtrl.h"
#include "HousingSectorsPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CHousePlacementZoneDlg dialog

class CHousePlacementZoneDlg : public CKEPEditDialog {
	// Construction
public:
	CHousePlacementZoneDlg(CHousingZoneDB*& housingZones,
		CWnd* pParent = NULL); // standard constructor
	virtual ~CHousePlacementZoneDlg();

	CHousingZoneDB*& m_housingZonesDB;
	LPDIRECT3DDEVICE9 g_pd3dDeviceRef;

	// Dialog Data
	//{{AFX_DATA(CHousePlacementZoneDlg)
	enum { IDD = IDD_DIALOG_PLACEMENTZONEDLG };
	CListBox m_zonesDB;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHousePlacementZoneDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InList(int sel);
	// Generated message map functions
	//{{AFX_MSG(CHousePlacementZoneDlg)
	afx_msg void OnBTNAddZone();
	afx_msg void OnBTNDeleteZone();
	afx_msg void OnBTNReplaceZone();
	virtual BOOL OnInitDialog();
	afx_msg void OnBTNZNSave();
	afx_msg void OnBTNZNLoad();
	afx_msg void OnBTNLoadVisibleBox();
	afx_msg void OnBUTTONLoadZoneBox();
	afx_msg void OnSelchangeLISTZonesDB();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CListBox m_housePlacementsList;
	CListBox m_houseVaultInv;
	CListBox m_storeInv;
	afx_msg void OnLbnSelchangeListHouseplacements();

protected:
	void SetHousingZone(int housingZoneIndex);
	void InListHousePlacements(int housePlacementIndex);
	void InListVaultInventory(int housePlacementIndex);
	void InListStoreInventory(int housePlacementIndex);
	void ReplaceHousingZone(int selection);

private:
	CCollapsibleGroup m_colHousingSectors;
	CButton m_grpHousingSectors;
	CHousingSectorsPropertiesCtrl m_propHousingSectors;
	CPositionPropertiesCtrl* m_propSubdivision;

	CKEPImageButton m_btnAddHouseSector;
	CKEPImageButton m_btnDeleteHouseSector;
	CKEPImageButton m_btnReplaceHouseSector;

	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOUSEPLACEMENTZONEDLG_H__6A502B18_4C1F_42B1_BE70_C54B5822CCDC__INCLUDED_)
