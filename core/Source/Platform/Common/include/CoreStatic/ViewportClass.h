///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#include <GetSet.h>
#include "KEPObList.h"

namespace KEP {

class CCameraObj;

class CViewportObj : public CObject, public GetSet {
private:
	std::string m_name;

	int m_id; // unique id (used during runtime to identify)

	CCameraObj* m_pCO_default; // default (backup) camera - created and removed by me

	CCameraObj* m_pCO; // a camera object attached to this viewport (if present, used instead of default)

	void GenerateMatrices(float aViewportAspectRatio);

public:
	static HRESULT SetProjectionMatrix(Matrix44f& aMatrix, float aFOV, float aAspect, float aNearPlane, float aFarPlane, float aHeightAspect = 1.0f);

	CViewportObj() = delete;

	CViewportObj(const std::string& name, int x1 = 0, int y1 = 0, int x2 = 100, int y2 = 100);

	virtual ~CViewportObj();

	void Activate(LPDIRECT3DDEVICE9 aD3D9Device, int aSurfaceWidth, int aSurfaceHeight);

	void DrawOutline(IDirect3DDevice9* pd3dDevice, int thickness = 2, D3DCOLOR color = D3DCOLOR_ARGB(255, 255, 255, 0));

	std::string GetName() const {
		return m_name;
	}

	void SetID(int id) {
		m_id = id;
	}
	int GetID() const {
		return m_id;
	}

	void SetCamera(CCameraObj* pCO) {
		m_pCO = pCO;
	}
	CCameraObj* GetCamera() const {
		return m_pCO ? m_pCO : m_pCO_default;
	}

	float GetCameraFovY() const;

	float GetCameraFovX() const {
		auto camFovY = GetCameraFovY();
		auto aspectRatio = GetAspectRatio();
		return (aspectRatio > 0.0) ? (camFovY / aspectRatio) : camFovY;
	}

	// Called by CRuntimeViewportList::RemoveCamera()
	void DelCameraCallback(CCameraObj* pCO);

	/// percentages of total screen width and height (0-100 values)
	int m_X1;
	int m_Y1;
	int m_X2;
	int m_Y2;

	bool m_activated; // whether this viewport is currently active

	D3DVIEWPORT9 m_imViewPort;

	void SetPos(Vector2f pos) {
		m_imViewPort.X = (DWORD)pos.x;
		m_imViewPort.Y = (DWORD)pos.y;
	}
	Vector2f GetPos() const {
		return Vector2f((float)m_imViewPort.X, (float)m_imViewPort.Y);
	}

	void SetHeight(float height) {
		m_imViewPort.Height = (DWORD)height;
	}
	float GetHeight() const {
		return (float)m_imViewPort.Height;
	}

	void SetWidth(float width) {
		m_imViewPort.Width = (DWORD)width;
	}
	float GetWidth() const {
		return (float)m_imViewPort.Width;
	}

	float GetAspectRatio() const {
		auto height = GetHeight();
		auto width = GetWidth();
		return (width > 0.0) ? height / width : height;
	}

	// transforms derived from the camera and viewport kept here for convenience
	Matrix44f m_camWorldMatrix; // camera world position and orientation
	Vector3f m_camWorldPosition; // camera world position only
	Matrix44f m_viewMatrix; // inverse of the camera world transform
	Matrix44f m_projMatrix; // derived from fov, near, far and the current viewport aspect ratio

	DECLARE_GETSET
};

class CViewportList : public CKEPObList {
public:
	CViewportList() {}
};

class CRuntimeViewportList : public CViewportList {
public:
	CRuntimeViewportList() :
			CViewportList(), m_curr_id(0), m_active_viewport_id(0) {}

	CViewportObj* GetViewportById(int id) const;
	CViewportObj* GetActiveViewport() const;
	int AddViewport(CViewportObj* viewport);
	bool RemoveViewport(int id);
	void DelCameraCallback(CCameraObj* pCO);
	void SelectViewportForInput(int x, int y); // absolute mouse coords

	std::string GetNewViewportName(const std::string& base_name);

private:
	int GetUniqueId() {
		m_curr_id++;
		return m_curr_id;
	}

	int m_curr_id; // used to assign unique ids
	int m_active_viewport_id; // runtime_id of the active viewport
};

} // namespace KEP