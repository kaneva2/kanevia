/******************************************************************************
 ShaderPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SHADERPROPERTIESCTRL_H_)
#define _SHADERPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShaderPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CShaderPropertiesCtrl window

class CShaderPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CShaderPropertiesCtrl();
	virtual ~CShaderPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetShaderName() const {
		return Utf16ToUtf8(m_shaderName).c_str();
	}
	BOOL GetFxCode() const {
		return m_fxCode;
	}
	BOOL GetResetRender() const {
		return m_resetRenderState;
	}

	// modifiers
	void SetShaderName(CStringA shaderName) {
		m_shaderName = Utf8ToUtf16(shaderName).c_str();
	}
	void SetFxCode(BOOL fxCode) {
		m_fxCode = fxCode;
	}
	void SetResetRender(BOOL resetRender) {
		m_resetRenderState = resetRender;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShaderPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CShaderPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	enum { INDEX_ROOT = 1,
		INDEX_SHADER_NAME,
		INDEX_FX_CODE,
		INDEX_RESET_RENDER };

	CStringW m_shaderName;
	BOOL m_fxCode;
	BOOL m_resetRenderState;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_SHADERPROPERTIESCTRL_H_)
