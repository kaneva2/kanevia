///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CURRENTWORLDSDLG_H__2D910461_1D52_11D5_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_CURRENTWORLDSDLG_H__2D910461_1D52_11D5_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CurrentWorldsDlg.h : header file
//
#include "CMultiplayerClass.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "CurrentWorldPropertiesCtrl.h"
#include "SpawnPointCfgPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CCurrentWorldsDlg dialog

class CCurrentWorldsDlg : public CKEPEditDialog {
	// Construction
public:
	CCurrentWorldsDlg(CWnd* pParent = NULL); // standard constructor
	CMultiplayerObj* GL_multiplayerObject;
	// Dialog Data
	//{{AFX_DATA(CCurrentWorldsDlg)
	enum { IDD = IDD_DIALOG_CURRENTWORLDS };
	CListBox m_spawnPointCfgList;
	CListBox m_worldList;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCurrentWorldsDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListCurrentWorlds(int sel);
	void InListCurrentSpawnCfg(int sel);
	CStringA ConvToString(int number);

	void SetWorldList();
	void SetWorld(int worldIndex);

	void SetCurrentSpawnCfgList();
	void SetCurrentSpawnCfg(int spawnPointIndex);

	// Generated message map functions
	//{{AFX_MSG(CCurrentWorldsDlg)
	virtual BOOL OnInitDialog();
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);
	afx_msg void OnButtonDelete();
	afx_msg void OnBUTTONReplace();
	afx_msg void OnBUTTONAdd();
	afx_msg void OnBUTTONAddSpawnPointCfg();
	afx_msg void OnBUTTONDeleteSpawnPointCfg();
	afx_msg void OnBUTTONReplaceSpawnPointCfg();
	afx_msg void OnSelchangeLISTSpawnPointCfgList();
	afx_msg void OnSelchangeLISTWorldList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceCurrentWorld(int worldIndex);
	void ReplaceSpawnPointCfg(int spawnPointIndex);

private:
	CCollapsibleGroup m_colWorld;
	CButton m_grpWorld;
	CCurrentWorldPropertiesCtrl m_propWorld;

	CCollapsibleGroup m_colSpawnPointCfg;
	CButton m_grpSpawnPointCfg;
	CSpawnPointCfgPropertiesCtrl m_propSpawnPointCfg;

	CKEPImageButton m_btnAddWorld;
	CKEPImageButton m_btnDeleteWorld;
	CKEPImageButton m_btnReplaceWorld;

	CKEPImageButton m_btnAddSpawnPointCfg;
	CKEPImageButton m_btnDeleteSpawnPointCfg;
	CKEPImageButton m_btnReplaceSpawnPointCfg;

	//last selected index in the list box
	int m_previousSelCurrentWorld;
	int m_previousSelSpawnPointCfg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CURRENTWORLDSDLG_H__2D910461_1D52_11D5_B1EE_0000B4BD56DD__INCLUDED_)
