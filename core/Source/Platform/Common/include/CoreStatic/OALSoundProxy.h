#pragma once

#include "IResource.h"

#include "OALSoundBuffer.h"

namespace KEP {

class SoundRM;

class OALSoundProxy : public IResource {
	std::unique_ptr<OALSoundBuffer> m_buffer;

	TimeMs m_durationMS;

public:
	OALSoundProxy(SoundRM* library, const GLID& glid);

	OALSoundProxy(SoundRM* library, const GLID& localGlid, BYTE* theBuff, FileSize sizeBytes);

	virtual ~OALSoundProxy() {}

	virtual std::string ToStr() const override {
		std::string str;
		StrBuild(str, "OALSP<" << this << ">");
		return str;
	}

	virtual bool CreateResource() override;

	bool IsSerializedDataCompressible() const {
		return false;
	}

	OALSoundBuffer* GetSoundBuffer();

	long long getGlid() const {
		return GetHashKey();
	}

	int GetDuration() const {
		return m_buffer ? (int)m_durationMS : -1;
	}
};

} // namespace KEP
