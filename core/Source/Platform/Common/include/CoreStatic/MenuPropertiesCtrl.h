/******************************************************************************
 MenuPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_MENU_PROPERTIESCTRL_H_)
#define _MENU_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MenuPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include <d3d9types.h>

#define WM_KEP_PROPERTY_CHANGED WM_USER + 1

/////////////////////////////////////////////////////////////////////////////
// CMenuPropertiesCtrl window

class CMenuPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CMenuPropertiesCtrl(CWnd* pParent);
	virtual ~CMenuPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual bool OnPropertyButtonClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	std::string GetBaseDir() const {
		return m_base_dir;
	}
	int GetID() const {
		return m_ID;
	}
	CStringA GetName() const {
		return Utf16ToUtf8(m_sName).c_str();
	}
	void GetPosition(POINT& pt) {
		pt = m_position;
	}
	int GetWidth() const {
		return m_width;
	}
	int GetHeight() const {
		return m_height;
	}
	unsigned int GetPositionAlignment() const {
		return m_positionHorizAlign | m_positionVertAlign;
	}
	CStringA GetScript() const {
		return Utf16ToUtf8(m_sScript).c_str();
	}

	bool GetMovable() const {
		return m_bMovable;
	}

	bool GetModal() const {
		return m_bModal;
	}
	bool GetSnapToEdges() const {
		return m_bSnapToEdges;
	}
	bool GetEnableMouse() const {
		return m_bEnableMouse;
	}
	bool GetEnableKeyboard() const {
		return m_bEnableKeyboard;
	}

	void SetCloseOnESC(bool bEnable) {
		m_bCloseOnESC = bEnable;
	}
	bool GetCloseOnESC() const {
		return m_bCloseOnESC;
	}

	CStringA GetTitlebarText() const {
		return Utf16ToUtf8(m_sTitleBarText).c_str();
	}
	bool GetTitlebarDisabled() const {
		return m_bTitleBarDisabled;
	}
	int GetTitlebarHeight() const {
		return m_titleBarHeight;
	}

	CStringA GetFontFaceName();
	long GetFontWeight();
	long GetFontHeight();
	bool GetFontItalic();
	bool GetFontUnderline();

	unsigned int GetAlignment() const {
		return m_horizAlign | m_vertAlign;
	}
	D3DCOLOR GetFontColor();
	D3DCOLOR GetTextureColor();
	D3DCOLOR GetTopLeftColor();
	D3DCOLOR GetTopRightColor();
	D3DCOLOR GetBottomLeftColor();
	D3DCOLOR GetBottomRightColor();
	unsigned int GetState() const {
		return m_state;
	}
	RECT GetTextureCoords() const {
		return m_textureCoords;
	}
	CStringA GetTextureName() const {
		return Utf16ToUtf8(m_sTextureName).c_str();
	}

	// modifiers
	void SetBaseDir(std::string base_dir) {
		m_base_dir = base_dir;
	}
	void SetID(int ID) {
		m_ID = ID;
	}
	void SetName(CStringA sName) {
		m_sName = Utf8ToUtf16(sName).c_str();
	}
	void SetPosition(POINT pt);
	void SetPositionAlignment(unsigned int alignment);
	void SetWidth(int width);
	void SetHeight(int height);
	void SetScript(CStringA sScript) {
		m_sScript = Utf8ToUtf16(sScript).c_str();
	}

	void SetMovable(bool bMovable) {
		m_bMovable = bMovable;
	}

	void SetModal(bool bModal) {
		m_bModal = bModal;
	}
	void SetSnapToEdges(bool bSnapToEdges) {
		m_bSnapToEdges = bSnapToEdges;
	}
	void SetEnableMouse(bool bEnableMouse) {
		m_bEnableMouse = bEnableMouse;
	}
	void SetEnableKeyboard(bool bEnableKeyboard) {
		m_bEnableKeyboard = bEnableKeyboard;
	}
	void SetTitlebarText(CStringA text) {
		m_sTitleBarText = Utf8ToUtf16(text).c_str();
	}
	void SetTitlebarDisabled(bool bDisabled) {
		m_bTitleBarDisabled = bDisabled;
	}
	void SetTitlebarHeight(int height) {
		m_titleBarHeight = height;
	}

	void SetFont(CStringA sFaceName, long fontHeight, long fontWeight);
	void SetAlignment(unsigned int alignment);
	void SetFontColor(D3DCOLOR color);
	void SetTextureColor(D3DCOLOR color);
	void SetTopLeftColor(D3DCOLOR color);
	void SetTopRightColor(D3DCOLOR color);
	void SetBottomLeftColor(D3DCOLOR color);
	void SetBottomRightColor(D3DCOLOR color);
	void SetState(unsigned int state) {
		m_state = state;
	}
	void SetTextureCoords(RECT coords);
	void SetTextureName(CStringA sTextureName) {
		m_sTextureName = Utf8ToUtf16(sTextureName).c_str();
	}

	// methods
	void Reset();

	enum { INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_ID,

		INDEX_APPEARANCE_ROOT,
		INDEX_COLOR_TOP_LEFT,
		INDEX_ALPHA_TOP_LEFT,
		INDEX_COLOR_TOP_RIGHT,
		INDEX_ALPHA_TOP_RIGHT,
		INDEX_COLOR_BOTTOM_LEFT,
		INDEX_ALPHA_BOTTOM_LEFT,
		INDEX_COLOR_BOTTOM_RIGHT,
		INDEX_ALPHA_BOTTOM_RIGHT,

		INDEX_BEHAVIOR_ROOT,
		INDEX_MOVABLE,
		INDEX_MODAL,
		INDEX_SNAP_TO_EDGES,
		INDEX_ENABLE_MOUSE,
		INDEX_ENABLE_KEYBOARD,

		INDEX_SCRIPT_ROOT,
		INDEX_SCRIPT,

		INDEX_TITLEBAR_ROOT,
		INDEX_TITLEBAR_TEXT,
		INDEX_TITLEBAR_DISABLED,
		INDEX_TITLEBAR_HEIGHT,
		/////
		INDEX_TITLEBAR_FONT,
		INDEX_TITLEBAR_HORIZONTAL_ALIGNMENT,
		INDEX_TITLEBAR_VERTICAL_ALIGNMENT,
		INDEX_TITLEBAR_TEXTURE_NAME,
		INDEX_TITLEBAR_TEXTURE_COORDS,
		INDEX_TITLEBAR_TEXTURE_COORD_LEFT,
		INDEX_TITLEBAR_TEXTURE_COORD_TOP,
		INDEX_TITLEBAR_TEXTURE_COORD_RIGHT,
		INDEX_TITLEBAR_TEXTURE_COORD_BOTTOM,

		INDEX_TITLEBAR_STATES_SUB_CAT,
		INDEX_TITLEBAR_STATE,

		INDEX_TITLEBAR_FONT_COLOR,
		INDEX_TITLEBAR_FONT_ALPHA,
		INDEX_TITLEBAR_TEXTURE_COLOR,
		INDEX_TITLEBAR_TEXTURE_ALPHA,

		INDEX_POSITION_ROOT,
		INDEX_POSITION,
		INDEX_X_POSITION,
		INDEX_Y_POSITION,
		INDEX_POSITION_HORIZ_ALIGN,
		INDEX_POSITION_VERT_ALIGN,
		INDEX_SIZE,
		INDEX_WIDTH,
		INDEX_HEIGHT,

		INDEX_CLOSE_ON_ESC, // drf - added

		NUM_ITEMS
	};

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMenuPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CMenuPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	static const int ALPHA_MIN = 0;
	static const int ALPHA_MAX = 255;

	void LoadHorizontalAlignments();
	void LoadVerticalAlignments();
	void LoadPositionHorizontalAlignments();
	void LoadPositionVerticalAlignments();
	void LoadStates();
	void SetColor(D3DCOLOR color, COLORREF& color_ref, unsigned int& alpha);

private:
	CWnd* m_pParent;

	int m_ID;
	CStringW m_sName;
	CStringW m_sScript;

	bool m_bMovable;

	bool m_bModal;
	bool m_bSnapToEdges;
	bool m_bEnableMouse;
	bool m_bEnableKeyboard;

	bool m_bCloseOnESC;

	COLORREF m_topLeftColor;
	COLORREF m_topRightColor;
	COLORREF m_bottomLeftColor;
	COLORREF m_bottomRightColor;
	unsigned int m_topLeftAlpha;
	unsigned int m_topRightAlpha;
	unsigned int m_bottomLeftAlpha;
	unsigned int m_bottomRightAlpha;

	CStringW m_sTitleBarText;
	bool m_bTitleBarDisabled;
	int m_titleBarHeight;

	// titlebar element related
	CFont m_font;
	unsigned int m_horizAlign;
	unsigned int m_vertAlign;
	unsigned int m_state;
	COLORREF m_fontColor;
	COLORREF m_textureColor;
	unsigned int m_fontAlpha;
	unsigned int m_textureAlpha;
	CStringW m_sTextureName;
	RECT m_textureCoords;

	POINT m_position;
	unsigned int m_positionHorizAlign;
	unsigned int m_positionVertAlign;
	int m_width;
	int m_height;

	std::string m_base_dir;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_MENU_PROPERTIESCTRL_H_)
