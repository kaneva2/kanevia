///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "matrixarb.h"

namespace KEP {
class CEXMeshObj;
class CStateManagementObj;
class TextureDatabase;

class CLightningObj : public CObject {
public:
	CLightningObj();

	CEXMeshObj* m_lightningMesh;

	Vector3f m_wldPos;
	float m_width;
	float m_depth;
	long m_duration;
	TimeMs m_timeStamp;
	BOOL m_enviormentMode;

	void SafeDelete();
};

class CLightningObjList : public CObList {
public:
	CLightningObjList(){};

	void AddLightningObject(Vector3f& spawnPosition,
		float width,
		float depth,
		int textureIndex,
		long duration,
		BOOL enviormentModeOn);

	void RenderSystem(Vector3f& cameraPosition,
		LPDIRECT3DDEVICE9 g_pd3dDevice7,
		CStateManagementObj* stateSystem,
		TextureDatabase* textureDatabase);

	void SafeDelete();
};

} // namespace KEP