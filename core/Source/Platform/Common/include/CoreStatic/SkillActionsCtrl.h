/******************************************************************************
 SkillActionsCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_SKILLACTIONSCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
#define AFX_SKILLACTIONSCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SkillActionsCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CSkillActionsCtrl window

class CSkillActionsCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSkillActionsCtrl();
	virtual ~CSkillActionsCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetActionId() const {
		return m_actionId;
	}
	int GetSkillId() const {
		return m_skillId;
	}
	float GetGainAmount() const {
		return m_gainAmount;
	}

	// modifiers
	void SetActionId(int actionId) {
		m_actionId = actionId;
	}
	void SetSkillId(int skillId) {
		m_skillId = skillId;
	}
	void SetGainAmount(float gainAmt) {
		m_gainAmount = gainAmt;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkillActionsCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSkillActionsCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;
	CFont m_font;

	int m_actionId;
	int m_skillId;
	float m_gainAmount;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKILLSPROPERTIESCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
