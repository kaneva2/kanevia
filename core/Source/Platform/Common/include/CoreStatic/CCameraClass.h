///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"
#include "KEPConstants.h"

#include "GetSet.h"
#include "CFrameObj.h"
#include "matrixarb.h"
#include "KEPObList.h"
#include "Core/Util/CArchiveObjectSchema.h"

#include "common\include\ObjectType.h"

#include "CMovementObj.h"
#include "DynamicObj.h"

namespace KEP {

enum class CameraAction : int {
	Update = 1,
	Bind = 2
};

class CCameraObj : public CObject, public GetSet {
protected:
	DECLARE_SERIAL_SCHEMA(CCameraObj, VERSIONABLE_SCHEMA | 9)

	bool m_attachAuto;
	ObjectType m_attachObjType;
	int m_attachObjId; // netTraceId / placementId

	CCameraObj() {}

public:
	std::string m_cameraName;

	CCameraObj(
		const std::string& cameraName,
		float farPlaneDist = 10000.0f,
		float fovY = 0.8f,
		Vector3f initFloatingTarget = Vector3f(0.0f, 0.0f, 70.0f));

	CCameraObj& operator=(const CCameraObj& rhs);

	~CCameraObj() {
		SafeDelete();
	}

	void SafeDelete();

	std::string ToStr() {
		std::string str;
		StrBuild(str, "CAM<" << this << ":" << m_cameraName << " " << ObjTypeToStr(m_attachObjType) << " objId=" << m_attachObjId << ">");
		return str;
	}

	void Serialize(CArchive& ar);

	void EngineCameraCallback();

	bool AutoFollow();

	bool ObjectAttach(const ObjectType& objType, int objId);

	void ObjectAttachGet(ObjectType& objType, int& objId) const;

	bool ObjectAttachToMe();

	bool ObjectAttachIsMe() const;

	bool ObjectAttachUpdate();

	CFrameObj* ObjectAttachGetBaseFrame();

	bool ObjectAttachSetVisibility(bool visible);

	// ED-8445 - Force Object Updates If Camera Attached
	bool ObjectAttachSetNeedsUpdate(bool update);

	// increments azimuth by angle (in radians)
	void AdjustAzimuth(float rad);

	// increments elevation by angle (in radians)
	void AdjustElevation(float rad);

	// entityPtr is the entity to adjust, rotateY allows us to specify
	// a y-axis rotation to offset by
	void OrientToViewDir(CMovementObj* pMO, float rotateY);

	// Moves camera closer(step<0) or further(step>0) to it's target object.
	void AdjustDist(float step);

	void SetDist(float dist);
	float GetDist() const {
		return m_orbitDist;
	}

	// called by the camera callback, basically just so
	// that I don't have to keep typing camPtr->something
	// in the orbit camera callback
	void UpdatePosition();

	virtual const Matrix44f* GetCustomProjectionMatrix() {
		return nullptr;
	}

	/// camera transformation split into 4 3D vectors for more clarity (used to derive the view matrix)
	Vector3f m_camWorldVectorX;
	Vector3f m_camWorldVectorY;
	Vector3f m_camWorldVectorZ;
	Vector3f m_camWorldPosition;

	bool m_inCameraRotMode;

	Vector3f m_RotAxisForTarget;

	// camera variables used to derive the projection matrix
	float m_nearPlaneDist;
	float m_farPlaneDist;

	float m_heightBaseAspect; // fraction of the vertical field of view to use (this makes aspect ratio not be preserved)

	// FOV (radians)
	float m_fovY; // vertical field of view (radians)
	float GetFovY() const {
		return m_fovY;
	}

	void SetFarPlaneDist(float dist) {
		m_farPlaneDist = dist;
	}
	float GetFarPlaneDist() const {
		return m_farPlaneDist;
	}

	CFrameObj* m_baseFrame;

	float m_pitchRotation; // an extra pitch rotation
	BOOL m_useCollision; // controls whether or not the camera position is affected by collisions with environment

	// state for floating camera
	Vector3f m_initialPos;
	CFrameObj* m_floatingFocusFrame;
	CFrameObj* m_floatingCamParentFrame;
	Vector3f m_floatingCamTarget;

	// orbit camera settings
	float m_orbitAzimuth, m_orbitElevation; // these are both in radians
	float m_orbitMinDist, m_orbitMaxDist; // inner and outer orbit radii
	float m_orbitDist; // distance the camera is currently at
	float m_orbitStartDist; // distance the camera starts at
	float m_orbitDistStep; // smallest amount the orbit distance changes by
	Vector3f m_orbitFocus; // position of center of the orbit, relative to point between character's feet
	float m_orbitAziInit, m_orbitEleInit; // initial values for azimuth and elevation
	bool m_orbitAutoFollow; // whether or not camera returns to starting offset while moving character

	float m_orbitAutoAzimuth; // angle for auto azimuth (radians)
	bool SetOrbitAutoAzimuth(float radians) {
		m_orbitAutoAzimuth = radians;
		return true;
	}
	float GetOrbitAutoAzimuth() const {
		return m_orbitAutoAzimuth;
	}

	float m_orbitAutoElevation; // angle for auto elevation (radians)
	bool SetOrbitAutoElevation(float radians) {
		m_orbitAutoElevation = radians;
		return true;
	}
	float GetOrbitAutoElevation() const {
		return m_orbitAutoElevation;
	}

	Vector3f m_cameraScale;

	DECLARE_GETSET
};

class CCameraObjList : public CKEPObList {
public:
	CCameraObjList() {
		m_pInstance = this;
	}

	~CCameraObjList() {
		m_pInstance = nullptr;
	}

	static CCameraObjList* GetInstance() {
		return m_pInstance;
	}

	DECLARE_SERIAL(CCameraObjList)

private:
	static CCameraObjList* m_pInstance;
};

} // namespace KEP