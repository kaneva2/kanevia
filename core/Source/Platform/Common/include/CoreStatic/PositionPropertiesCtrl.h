/******************************************************************************
 PositionPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_POSITION_PROPERTIESCTRL_H_)
#define _POSITION_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PositionPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CPositionPropertiesCtrl window

class CPositionPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CPositionPropertiesCtrl();
	CPositionPropertiesCtrl(CStringA sRootLabel, CStringA sPositionLabel);
	virtual ~CPositionPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetPositionX() const {
		return m_positionX;
	}
	float GetPositionY() const {
		return m_positionY;
	}
	float GetPositionZ() const {
		return m_positionZ;
	}

	// modifiers
	void SetPositionX(float positionX);
	void SetPositionY(float positionY);
	void SetPositionZ(float positionZ);
	void SetRootLabel(CStringA sRootLabel) {
		m_sRootLabel = sRootLabel;
	}
	void SetPositionLabel(CStringA sPositionLabel) {
		m_sPositionLabel = sPositionLabel;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPositionPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CPositionPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_POSITION,
		INDEX_POSITION_X,
		INDEX_POSITION_Y,
		INDEX_POSITION_Z
	};

	void Init();

private:
	float m_positionX;
	float m_positionY;
	float m_positionZ;

	CStringA m_sRootLabel;
	CStringA m_sPositionLabel;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_POSITION_PROPERTIESCTRL_H_)
