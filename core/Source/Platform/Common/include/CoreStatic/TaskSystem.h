///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>
#include <string>
#include <deque>

#include "Core/Util/fast_mutex.h"

namespace KEP {

class TaskObject;

class TaskSystem {
public:
	static const unsigned int DEFAULT_WORKER_THREADS;
	static const unsigned int WORKER_THREAD_LIMIT;
	static const unsigned int WAIT_FOR_EXIT_TIMEOUT;

public:
	TaskSystem(const std::string& name) :
			m_name(name),
			m_semaphoreHandle(nullptr),
			m_exitEventHandle(nullptr),
			m_threadHandles(nullptr),
			m_threads(0) {}

	virtual ~TaskSystem() {
		Finalize();
		ClearAllTasks();
	}

	// Starts processing tasks on worker threads.  Calling Initialize() again
	// before calling Finalize() will have no effect.
	void Initialize(size_t threads);

	// Tells worker threads to stop processing tasks and exit.
	void Finalize();

	// Adds a task to the queue.  If you wish to keep this task
	// around after processing finishes, then you must increment
	// its reference count before adding it to the queue.
	void QueueTask(TaskObject* task);

private:
	bool IsInitialized() const {
		return m_semaphoreHandle != NULL;
	}

	// Clears All Tasks From Tasks Queue
	void ClearAllTasks();

	// Pop Task From Tasks Queue
	TaskObject* PopTask();

	// Calls ExecuteTask() on the next task in the queue, if any.
	void ProcessOneTask();

	bool CreateWorkerThreads(size_t threads);

	// Allow derived class to do per worker thread initializations
	virtual void InitializeWorkerThread() {} // Do nothing by default

	// Entry point for all worker threads used by task system.
	static DWORD WINAPI WorkerThreadProcedure(LPVOID lpParam);

private:
	std::string m_name;

	HANDLE m_semaphoreHandle;
	HANDLE m_exitEventHandle;

	HANDLE* m_threadHandles;
	size_t m_threads;

	typedef std::deque<TaskObject*> TaskQueue;
	TaskQueue m_taskQueue;

	fast_mutex m_mutex;
};

} // namespace KEP