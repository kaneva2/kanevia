/******************************************************************************
 MoviesDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_MOVIES_DLG_H_)
#define _MOVIES_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MoviesDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"

////////////////////////////////////////////
// CMoviesDialog control

class CMoviesDialog : public CDialogBar {
	DECLARE_DYNAMIC(CMoviesDialog)

public:
	CMoviesDialog();
	virtual ~CMoviesDialog();

	// Dialog Data
	//{{AFX_DATA(CMoviesDialog)
	enum { IDD = IDD_DIALOG_MOVIES };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CMoviesDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colMovies;
	CButton m_grpMovies;

	CKEPImageButton m_btnAddMovie;
	CKEPImageButton m_btnDeleteMovie;
	CKEPImageButton m_btnMovieProperties;
	CKEPImageButton m_btnPlayMovie;
	CKEPImageButton m_btnStopMovie;
};

#endif !defined(_MOVIES_DLG_H_)