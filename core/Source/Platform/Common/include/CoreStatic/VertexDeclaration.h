///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "d3d9.h"
#include "dxerr9.h"

//	Max number of vertex elements a declaration can have
#define MAX_VERTEX_ELEMENTS 12
#define MAX_STREAMS 4

//	Usages and types are DX9 Oct2004, included here for reference
/*
USAGE									TYPE
typedef enum _D3DDECLUSAGE {			typedef enum _D3DDECLTYPE {
D3DDECLUSAGE_POSITION = 0,				D3DDECLTYPE_FLOAT1 = 0,
D3DDECLUSAGE_BLENDWEIGHT = 1,			D3DDECLTYPE_FLOAT2 = 1,
D3DDECLUSAGE_BLENDINDICES = 2,			D3DDECLTYPE_FLOAT3 = 2,
D3DDECLUSAGE_NORMAL = 3,				D3DDECLTYPE_FLOAT4 = 3,
D3DDECLUSAGE_PSIZE = 4,					D3DDECLTYPE_D3DCOLOR = 4,
D3DDECLUSAGE_TEXCOORD = 5,				D3DDECLTYPE_UBYTE4 = 5,
D3DDECLUSAGE_TANGENT = 6,				D3DDECLTYPE_SHORT2 = 6,
D3DDECLUSAGE_BINORMAL = 7,				D3DDECLTYPE_SHORT4 = 7,
D3DDECLUSAGE_TESSFACTOR = 8,			D3DDECLTYPE_UBYTE4N = 8,
D3DDECLUSAGE_POSITIONT = 9,				D3DDECLTYPE_SHORT2N = 9,
D3DDECLUSAGE_COLOR = 10,				D3DDECLTYPE_SHORT4N = 10,
D3DDECLUSAGE_FOG = 11,					D3DDECLTYPE_USHORT2N = 11,
D3DDECLUSAGE_DEPTH = 12,				D3DDECLTYPE_USHORT4N = 12,
D3DDECLUSAGE_SAMPLE = 13				D3DDECLTYPE_UDEC3 = 13,
} D3DDECLUSAGE;							D3DDECLTYPE_DEC3N = 14,
										D3DDECLTYPE_FLOAT16_2 = 15,
										D3DDECLTYPE_FLOAT16_4 = 16,
										D3DDECLTYPE_UNUSED = 17
										} D3DDECLTYPE;
*/

namespace KEP {

class IMemSizeGadget;

class VertexBuffer {
public:
	unsigned int mStride;
	unsigned int mNumItems;
	void* pData;

	LPDIRECT3DVERTEXBUFFER9 mpBuffer;

	VertexBuffer() {
		mpBuffer = NULL;
		pData = NULL;
	}
	~VertexBuffer() {
		if (pData) {
			delete pData;
			pData = NULL;
		}

		if (mpBuffer) {
			mpBuffer->Release();
			mpBuffer = NULL;
		}
	}

	void CreateAndFillBuffer(LPDIRECT3DDEVICE9 g_pd3dDevice);
};

class VertexDeclaration {
public:
	//	1 gets added here for the D3DDECL_END() that's required
	D3DVERTEXELEMENT9 mVertexElements[MAX_VERTEX_ELEMENTS + 1];

	unsigned int mNumElements;
	unsigned int mStreamSize[MAX_STREAMS];

	//	D3D9 structs
	LPDIRECT3DDEVICE9 mpDevice;
	IDirect3DVertexDeclaration9* mpDeclaration;
	unsigned int D3DFVF;

	//	This needs to be the last element in every declaration.
	static D3DVERTEXELEMENT9 mEndElement;

	VertexDeclaration();
	~VertexDeclaration();

	//	Adds an element to the declaration. Note that
	void AddVertexElement(D3DDECLUSAGE usage, D3DDECLTYPE type, unsigned int elementSize,
		unsigned int stream = 0, unsigned int usageIndex = 0);

	//	Creates the IDirect3DVertexDeclaration9 structure
	void CreateVertexDeclaration(LPDIRECT3DDEVICE9 pDevice);
	void CreateVertexFVF();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

} // namespace KEP