#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <memory>
#include <stdint.h>

struct SFPCFlashCallInfoStructW;

namespace KEP {
namespace FlashControl {

bool IsFlashInstalled();
std::string GetFlashVersion();

class IFlashControlServer {
public:
	virtual bool StartServer(uintptr_t iClientId, uint32_t iMeshId, HANDLE hSharedMemory, HANDLE hRequestAvailableEvent, bool bStartInThread = false) = 0;
	virtual bool StopProcessing() = 0;
	virtual bool ProcessNextRequest() = 0;
	virtual void SetGlobalVolume(float fGlobalVolume) = 0;
	virtual std::string GetIdString() = 0;
	virtual bool OwnsWindow(HWND hWnd, bool* pbTopControlWindow) = 0;
	virtual void HandleFlashCall(SFPCFlashCallInfoStructW* pInfo) = 0;

	static std::shared_ptr<IFlashControlServer> Create();
	static void SetOwnerWindow(HWND hwndOwner) { s_hwndOwner = hwndOwner; }
	static HWND GetOwnerWindow() { return s_hwndOwner; }

private:
	static HWND s_hwndOwner; // handle to the owner window of the flash window (if not CHILD of the main client window) - init once
};

int FlashProcessMain(int argc, wchar_t* argv[]);

} // namespace FlashControl
} // namespace KEP
