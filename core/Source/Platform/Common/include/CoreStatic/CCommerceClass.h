///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// added by serialize.pl
#include <GetSet.h>
#include "InstanceId.h"
#include "KEPObList.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CStoreInventoryObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CStoreInventoryObj, VERSIONABLE_SCHEMA | 2);

public:
	CStoreInventoryObj();

	void SafeDelete();
	void Serialize(CArchive& ar);

	int m_globalInventoryID;
	int m_raceID;

	DECLARE_GETSET
};

class CStoreInventoryObjList : public CObList {
	DECLARE_SERIAL(CStoreInventoryObjList);

public:
	CStoreInventoryObjList(){};

	void SafeDelete();

	CStoreInventoryObj* GetByGLID(int GLID);
	CStoreInventoryObj* RemoveByGLID(int GLID);
	BOOL VerifyInventoryGLID(int GLID);
};

class CCommerceObj : public CObject, public GetSet {
	DECLARE_SERIAL(CCommerceObj);

public:
	CCommerceObj();

	void SafeDelete();

	void Serialize(CArchive& ar);

	CStringA m_establishmentName;
	int m_uniqueID;
	CStoreInventoryObjList* m_storeInventory;
	ChannelId m_channel;
	Vector3f m_boundingBoxMax;
	Vector3f m_boundingBoxMin;
	int m_menuBind;
	BOOL m_willDealWithCriminals;

	DECLARE_GETSET
};

class CCommerceObjList : public CKEPObList {
	DECLARE_SERIAL(CCommerceObjList);

public:
	CCommerceObjList(){};

	void SafeDelete();
	bool RemoveEstablishmentByName(const CStringA& name);
	bool RemoveEstablishmentByID(int id);

	CCommerceObj* GetEstablishmentByID(int ID);
	CCommerceObj* GetEstablishmentByName(const CStringA& name);
	CCommerceObj* GetEstablishmentByPosition(const Vector3f& position, const ChannelId& channel);

	BOOL BoundBoxPointCheck(const Vector3f& point, const Vector3f& min, const Vector3f& max) const;
};

} // namespace KEP