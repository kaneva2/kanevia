///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include <vector>

#include "matrixarb.h"
#include "CAdvancedVertexClass.h"
#include "CabFunctionLib.h"
#include "CStructuresMisl.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {
class IExternalEXMesh;
class IMemSizeGadget;
class IMesh;
class CBoneList;
class CCharonaObj;
class CGlobalMaterialShader;
class CMaterialObject;
class EffectRenderData;
class MaterialEffect;
class ReD3DX9DeviceState;
class TextureDatabase;
class VertexBuffer;

enum BLENDTYPE {
	RIGID = 0,
	BLENDED,
	BLENDEDTANGENT,
};

struct BoneInfo {
	float BoneWeight[3];
	D3DCOLOR boneIndices;
};

struct OffsetInfo {
	Vector3f offsets[4];
};

struct RawBoneData {
	const Matrix44f* mBoneMatrices;
	unsigned int mNumBones;
};

//TextureSetList
class CTextureSetObj : public CObject {
	DECLARE_SERIAL(CTextureSetObj)

public:
	CTextureSetObj();

	TXTUV* m_textureSet;
	int m_textureCoordCount;

	void Serialize(CArchive& ar);
};

class CTextureSetObjList : public CObList {
public:
	CTextureSetObjList(){};

	DECLARE_SERIAL(CTextureSetObjList)
};

class CEXMeshObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CEXMeshObj, VERSIONABLE_SCHEMA | 7) // version 6: stream "m_VertexArrayValid"

public:
	CEXMeshObj();
	~CEXMeshObj();

	/////////////////////////////////////////////////////////////////////////////
	// Start Encapsulations to regulate "user" behavior (Yanfeng 09/2008)
	// For existing users, we gradually phase out bad behaviors. Member variables
	// will stay public until all users are tamed.

	//! \brief Return a pointer to CAdvancedVertexObj
	CAdvancedVertexObj& GetVertices() {
		return m_abVertexArray;
	}

	//! \brief Return a pointer to CAdvancedVertexObj (const version)
	const CAdvancedVertexObj& GetVertices() const {
		return m_abVertexArray;
	}

	//! \brief Return a pointer to CMaterialObject
	CMaterialObject& GetMaterial() {
		return *m_materialObject;
	}

	//! \brief Set material, dispose old if available
	void SetMaterial(CMaterialObject* mat);

	//! \brief Transform all vertices with matrix provided
	void TransformVertices(const Matrix44f& matrix);

	//! \brief Transform all vertices with current world transform.
	void BakeVertices();

	// End Encapsulations
	///////////////////////////////////////////////////////////////

	void ClearExternalSource();
	void SetExternalSource(std::shared_ptr<IExternalEXMesh> externalSource);
	IExternalEXMesh* GetExternalSource() { return m_externalSource.get(); }
	const std::shared_ptr<IExternalEXMesh>& GetExternalSourceShared() { return m_externalSource; }

	/////////////////////
	// Mutator Methods //
	/////////////////////

	void SetScaleTranslationRotation(const Vector3f& scale, const Vector3f& translation, const Vector3f& rotation);
	void SetScale(const Vector3f& scale);
	void SetRotation(const Vector3f& rotation);
	void SetTranslation(const Vector3f& translation);
	void SetWorldTransform(const Matrix44f& worldTransform);
	void ResetWorldTransform();

	//////////////////////
	// Accessor Methods //
	//////////////////////

	const Vector3f& GetScale() const {
		return m_scale;
	}
	const Vector3f& GetRotation() const {
		return m_rotation;
	}
	const Vector3f& GetTranslation() const {
		return m_translation;
	}
	const Matrix44f& GetWorldTransform() const {
		return m_worldTransform;
	}

private:
	std::shared_ptr<IExternalEXMesh> m_externalSource; // an external source to render

public:
	IMesh* blade_mesh;

	CTextureSetObjList* m_textureSetList; //additional texture sets
	int m_blendType; //-1 = none 1 = even opacity 2 = opacity map darker is more transparent

	const ABBOX& GetBoundingBox() const {
		return m_boundingBox;
	}
	void SetBoundingBox(const ABBOX& box);

	float GetBoundingRadius() const { return m_boundingRadius; }

	CMaterialObject* m_materialObject;
	CObList* m_tempSupportedMaterials;
	int m_visibilityTrack;
	float m_tempDistance;
	const Vector3f& GetMeshCenter() {
		return m_meshCenter;
	}
	void SetMeshCenter(const Vector3f& ctr);
	float GetBoundingSphereRadiusSqr() {
		return m_meshBoundingSphereRadius;
	}
	void SetBoundingSphereRadiusSqr(float newR);
	CAdvancedVertexObj m_abVertexArray;
	float m_sightBasis;
	int m_smoothAngle;

	//	Is this mesh skinned?
	//	TODO: This should be in a material
	//	TODO: Develop a material system? Heh.
	bool mbIsSkinned;
	MaterialEffect* mpEffectShader;

	//	This buffer holds bones indices and weights
	VertexBuffer* mpBoneInfo;

	//	This buffer holds the offsetVects
	VertexBuffer* mpOffsetVectsBuffer;

	//	This is structure to hold bone matrix array info for this skeleton object.
	//	We need to send this data to the shader to skin
	RawBoneData mBoneData;
	/////////////////////////////////////////////////////////

	//Mesh effects
	BOOL m_chromeWrap;
	int m_chromeUvSet;

	//time hooks
	long m_lastTimeStamp;

	//effects extra
	CCharonaObj* m_charona;
	int m_groupReference;

	CEXMeshObj* m_shadowTemp;

	int m_validUvCount;

	char m_byteFlagTemp;

	//int                        m_textureTileType;
	LPDIRECT3DDEVICE9 m_pd3dDevice; // Added by Jonny because the mesh needs to be able to reset its index and vertex
	// Buffers on the card when there is a change to the structure.
	CStringA m_fileName;

	BOOL m_indexBuffiltered;

	BOOL m_Rendered;

	BOOL m_VertexArrayValid; // RENDERENGINE INTERFACE

	int GetUVlayerCountString();
	BOOL UpdateShadowMesh(Matrix44f& objectMatrix,
		Matrix44f& shadowDifference,
		LPDIRECT3DDEVICE9 g_pd3dDevice);
	BOOL GetGeometryFromPlugin(const CStringA& fileName,
		WORD* vertexCount,
		ABVERTEX** vertexArray,
		WORD* indecieCount,
		WORD** indecieArray,
		CStringA texture[10]);

	void Move(const Vector3f& aTranslation, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols);
	void Rotate(const Vector3f& aRotation, const Vector3f* const aPivotOverride, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols);
	void Scale(const Vector3f& aScale, const Vector3f* const aPivotOverride, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols);

	void Transform(const Matrix44f aTransform, LPDIRECT3DDEVICE9 aD3D9Device /* optional */, bool aRecompBoundVols);

	ABBOX ComputeBoundingVolumes(BOOL bUpdateBox = TRUE);
	BOOL GetBoxWithMarix(const Matrix44f& positionWld, ABBOX* returnBox) const;

	void InitBaseMaterialTextures(TextureDatabase* pDataBase, const char* subDir);

	void SafeDelete();
	static void ConvertToX(CStringA* fileString);

	int Render(LPDIRECT3DDEVICE9 g_pd3dDevice, int& vertexType, int uvSet);
	int RenderFast(LPDIRECT3DDEVICE9 g_pd3dDevice, int& vertexType, int& uvSet);
	BOOL Clone(CEXMeshObj** copy, LPDIRECT3DDEVICE9 g_pd3dDevice);

	BOOL SetGeometry2(const std::string& texFileName,
		ABVERTEX1L* vertexArray,
		WORD vertexCount,
		WORD* indecieArray,
		WORD indecieCount);

	void GenerateSmoothNormals();
	void SmoothNormalsBasedOnArray(ABVERTEX* originalVertexArray);
	BOOL ApplySphereMappingToObject(LPDIRECT3DDEVICE9 g_pd3dDevice, int& vertexType, int uvSet);
	void DumpTextureCoordSets();
	// void MoveTexture(float &u,float &v,int textureSet,float &adjustfactor);
	void GenerateNormals(float smoothBasisAngle);
	void scaleMat(float Xt, float Yt, float Zt, float TMatrix[4][4]);
	void Serialize(CArchive& ar);
	BOOL LoadGeometryFromPlugin(const CStringA& fileName);
	BOOL InitAfterLoad(LPDIRECT3DDEVICE9 g_pd3dDevice, bool aOptimize = true);
	static int Round(float x);

	void OptimizeMesh();
	void OptimizeMesh(float smoothShadeNormalReqAngle);
	void NormalizeOnly();
	void UnifyNormals();
	void UnifyNormals(float angleTolerance);
	void AVGNormals(float smoothBasisAngle);

	void Draw(LPDIRECT3DDEVICE9 g_pd3dDevice);
	void PreRenderFX(LPDIRECT3DDEVICE9 g_pd3dDevice);
	void RenderFX(LPDIRECT3DDEVICE9 g_pd3dDevice, EffectRenderData* pRenderData);
	void PostRenderFX(LPDIRECT3DDEVICE9 g_pd3dDevice);

	void ExchangeTextureSets(int level, int level2);
	//BOOL UpdateVB();
	void InitBuffer(LPDIRECT3DDEVICE9 g_pd3dDevice, CGlobalMaterialShader* optionalShader,
		CMaterialObject* custMaterial = NULL);
	int GetUVlayerCount();
	void TextureMovementCallback(int& curVertexType);

	unsigned GetVertexCount() const {
		return m_abVertexArray.GetVertexCount();
	}
	unsigned GetIndexCount() const {
		return m_abVertexArray.GetVertexIndexCount();
	}
	unsigned GetVertexUVCount() const {
		return m_abVertexArray.GetUVCount();
	}
	bool IsTriStrip() const {
		return m_abVertexArray.m_triStripped == TRUE;
	}

	// Fill position vector buffer with mesh vertex data. Return number of position vectors filled
	unsigned FillPositionBuffer(unsigned char* buffer, unsigned bufferSize, unsigned strideInBytes);

	// Fill normal vector buffer with mesh vertex data. Return number of normal vectors filled
	unsigned FillNormalBuffer(unsigned char* buffer, unsigned bufferSize, unsigned strideInBytes);

	// Fill UV coordinates buffer with mesh vertex data. Return number of UV pairs filled
	unsigned FillUVCoordsBuffer(unsigned char* buffer, unsigned bufferSize, unsigned strideInBytes);

	// Fill 32-bit index buffer with mesh index data. Return number of indices filled
	unsigned FillIndexBufferInt(unsigned char* buffer, unsigned bufferSize, unsigned int baseIndex, unsigned strideInBytes = sizeof(int));

	// Fill 16-bit index buffer with mesh index data. Return number of indices filled
	unsigned FillIndexBufferShort(unsigned char* buffer, unsigned bufferSize, unsigned short baseIndex, unsigned strideInBytes = sizeof(short));

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	////////////////////
	// Helper Methods //
	////////////////////

	void BuildTransform();
	void DecomposeTransform();

	Matrix44f m_worldTransform;
	Vector3f m_scale;
	Vector3f m_rotation;
	Vector3f m_translation;

	ABBOX m_boundingBox;
	Vector3f m_meshCenter;
	float m_meshBoundingSphereRadius;
	float m_boundingRadius;

	DECLARE_GETSET
};

class CEXMeshObjList : public CObList {
public:
	CEXMeshObjList(){};

	int InvalidateZeroTemp();
	void FlagAllZero();
	void AddAllocatedMeshOrRefresh(CEXMeshObj* mesh);
	void SafeDelete();

	DECLARE_SERIAL(CEXMeshObjList)
};

} // namespace KEP