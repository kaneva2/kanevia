/******************************************************************************
 AdvancedPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ADVANCEDPROPERTIESCTRL_H_)
#define _ADVANCEDPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AdvancedPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CAdvancedPropertiesCtrl window

class CAdvancedPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CAdvancedPropertiesCtrl();
	virtual ~CAdvancedPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetGlobalShaderID() const {
		return m_globalShaderID - 1; // subtract 1 for our 'None' entry
	}
	bool GetContactSelection() const {
		return m_bContactSelection;
	}
	bool GetCalcEntityCollision() const {
		return m_bCalcEntityCollision;
	}
	bool GetWBuffer() const {
		return m_bWBuffer;
	}
	int GetTargetFPS() const {
		return m_targetFPS;
	}
	int GetInterpolationGlobal() const {
		return m_interpolationGlobal;
	}
	int GetResolutionHeight() const {
		return m_resolutionHeight;
	}
	int GetResolutionWidth() const {
		return m_resolutionWidth;
	}

	// modifiers
	void SetGlobalShaderID(int globalShaderID) {
		m_globalShaderID = globalShaderID + 1;
	}
	void SetContactSelection(bool bContactSelection) {
		m_bContactSelection = bContactSelection;
	}
	void SetCalcEntityCollision(bool bCalcEntityCollision) {
		m_bCalcEntityCollision = bCalcEntityCollision;
	}
	void SetWBuffer(bool bWBuffer) {
		m_bWBuffer = bWBuffer;
	}
	void SetTargetFPS(int targetFPS) {
		m_targetFPS = targetFPS;
	}
	void SetInterpolationGlobal(int interpolationGlobal) {
		m_interpolationGlobal = interpolationGlobal;
	}
	void SetResolutionHeight(int resolutionHeight);
	void SetResolutionWidth(int resolutionWidth);

	// methods
	void AddGlobalShaderItem(CStringW* sItem);
	void ClearGlobalShaderItems();

	static const int TARGET_FPS_MIN = 30;
	static const int TARGET_FPS_MAX = 200;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdvancedPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CAdvancedPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;

	bool m_bContactSelection;
	bool m_bCalcEntityCollision;
	bool m_bWBuffer;

	int m_targetFPS;
	int m_interpolationGlobal; // shadow type
	int m_resolutionHeight;
	int m_resolutionWidth;

	int m_globalShaderID; // current world shader
	CStringW m_sGlobalShaderEmpty; // string 'None'
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ADVANCEDPROPERTIESCTRL_H_)
