/******************************************************************************
 BackgroundDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_BACKGROUND_DLG_H_)
#define _BACKGROUND_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BackgroundDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"

////////////////////////////////////////////
// CBackgroundDialog control

class CBackgroundDialog : public CDialogBar {
	DECLARE_DYNAMIC(CBackgroundDialog)

public:
	CBackgroundDialog();
	virtual ~CBackgroundDialog();

	// Dialog Data
	//{{AFX_DATA(CBackgroundDialog)
	enum { IDD = IDD_DIALOG_BACKGROUND };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CBackgroundDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colBackground;
	CButton m_grpBackground;

	CKEPImageButton m_btnAdd;
	CKEPImageButton m_btnDelete;
};

#endif !defined(_BACKGROUND_DLG_H_)