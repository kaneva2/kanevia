///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class IMemSizeGadget;

class CExpansiveObj : public CObject, public GetSet {
	DECLARE_SERIAL(CExpansiveObj);

public:
	CExpansiveObj();

	int m_iconIndex;
	float m_xPos;
	float m_yPos;
	int m_miscInt;
	int m_miscInt2;

	void Clone(CExpansiveObj** clone);
	void SerializeAlloc(CArchive& ar);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CCExpansiveObjList : public CObList {
public:
	CCExpansiveObjList(){};
	void SafeDelete();
	void Clone(CCExpansiveObjList** clone);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CCExpansiveObjList);

	CExpansiveObj* GetByMisc2(int miscInt2);
	void SerializeAlloc(CArchive& ar, int loadCount);
};

} // namespace KEP