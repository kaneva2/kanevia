/******************************************************************************
 GameDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEPEditDialog.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "EditorState.h"

#include "GetSetPropertiesCtrl.h"
#include "../../Editor/StartCfg.h"
#include "Common\UIStatic\BasicLayoutMFC.h"

namespace KEP {

class Game;

class CGameDlg : public CKEPEditDialog {
public:
	CGameDlg(StartCfg* startCfg, Game* game, CWnd* pParent = NULL); // standard constructor
	virtual ~CGameDlg();

	// Dialog Data
	enum { IDD = IDD_DIALOG_GAME_GENERAL };
	bool ApplyChanges();

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CGameDlg)
	afx_msg LRESULT OnPropertyCtrlChanged(WPARAM wp, LPARAM lp);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	Game* m_game;
	StartCfg* m_startCfg;

	CCollapsibleGroup m_colGeneral;
	CButton m_grpGeneral;
	CGetSetPropertiesCtrl m_propGeneral;
	CGetSetPropertiesCtrl m_propStartCfg;
	BasicLayoutMFC m_layout;
};

} // namespace KEP