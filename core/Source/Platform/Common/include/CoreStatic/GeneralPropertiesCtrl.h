/******************************************************************************
 GeneralPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_GENERALPROPERTIESCTRL_H_)
#define _GENERALPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GeneralPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CGeneralPropertiesCtrl window

class CGeneralPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CGeneralPropertiesCtrl();
	virtual ~CGeneralPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual void OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CString GetName() const {
		return m_sName;
	}
	CString GetDesc() const {
		return m_sDesc;
	}
	CString GetAuthor() const {
		return m_sAuthor;
	}
	CString GetSVR() const {
		return m_sSVR;
	}
	CString GetDate() const {
		return m_sDate;
	}
	CString GetVersion() const {
		return m_sVersion;
	}

	// modifiers
	void SetName(CString sName) {
		m_sName = sName;
	}
	void SetDesc(CString sDesc) {
		m_sDesc = sDesc;
	}
	void SetAuthor(CString sAuthor) {
		m_sAuthor = sAuthor;
	}
	void SetSVR(CString sSVR) {
		m_sSVR = sSVR;
	}
	void SetDate(CString sDate) {
		m_sDate = sDate;
	}
	void SetVersion(CString version) {
		m_sVersion = version;
	}
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGeneralPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CGeneralPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_DESCRIPTION,
		INDEX_AUTHOR,
		INDEX_VERSION,
		INDEX_SVR,
		INDEX_SAVE_DATE
	};

private:
	CString m_sName;
	CString m_sDesc;
	CString m_sAuthor;
	CString m_sSVR;
	CString m_sDate;
	CString m_sVersion;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_GENERALPROPERTIESCTRL_H_)
