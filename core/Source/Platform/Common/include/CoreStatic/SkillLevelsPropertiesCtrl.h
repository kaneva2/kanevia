/******************************************************************************
 SkillLevelsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SKILLLEVELSPROPERTIESCTRL_H_)
#define _SKILLLEVELSPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SkillLevelsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CSkillLevelsPropertiesCtrl window

class CSkillLevelsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSkillLevelsPropertiesCtrl();
	virtual ~CSkillLevelsPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetLevelTitle() const {
		return Utf16ToUtf8(m_sLevelTitle).c_str();
	}
	float GetExpStartRange() const {
		return m_expStartRange;
	}
	float GetExpEndRange() const {
		return m_expEndRange;
	}
	int GetMenuID() const {
		return m_menuID;
	}
	int GetProficiency() const {
		return m_proficiency;
	}
	int GetPerfective() const {
		return m_perfective;
	}
	int GetTitleId() const {
		return m_titleId;
	}
	int GetAltTitleId() const {
		return m_altTitleId;
	}
	CStringA GetLevelMessage() const {
		return Utf16ToUtf8(m_levelMsg).c_str();
	}
	CStringA GetGainMessage() const {
		return Utf16ToUtf8(m_gainMsg).c_str();
	}
	CStringA GetAltGainMessage() const {
		return Utf16ToUtf8(m_altGainMsg).c_str();
	}
	int GetLevelNumber() const {
		return m_levelNumber;
	}

	// modifiers
	void SetLevelTitle(CStringA sLevelTitle) {
		m_sLevelTitle = Utf8ToUtf16(sLevelTitle).c_str();
	}
	void SetExpStartRange(float expStartRange) {
		m_expStartRange = expStartRange;
	}
	void SetExpEndRange(float expEndRange) {
		m_expEndRange = expEndRange;
	}
	void SetMenuID(int menuID) {
		m_menuID = menuID;
	}
	void SetProficiency(int proficiency) {
		m_proficiency = proficiency;
	}
	void SetPerfective(int perfective) {
		m_perfective = perfective;
	}
	void SetTitleId(int titleId) {
		m_titleId = titleId;
	}
	void SetAltTitleId(int altTitleId) {
		m_altTitleId = altTitleId;
	}
	void SetLevelMessage(CStringA sLevelMessage) {
		m_levelMsg = Utf8ToUtf16(sLevelMessage).c_str();
	}
	void SetGainMessage(CStringA sGainMessage) {
		m_gainMsg = Utf8ToUtf16(sGainMessage).c_str();
	}
	void SetAltGainMessage(CStringA sAltGainMessage) {
		m_altGainMsg = Utf8ToUtf16(sAltGainMessage).c_str();
	}
	void SetLevelNumber(int levelNumber) {
		m_levelNumber = levelNumber;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkillLevelsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSkillLevelsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitMenus();

	enum {
		INDEX_ROOT = 1,
		INDEX_LEVEL_TITLE,
		INDEX_START_EXP_RANGE,
		INDEX_END_EXP_RANGE,
		INDEX_MENU_ID,
		INDEX_PROFICIENCY_BONUS,
		INDEX_PERFECTIVE_BONUS,
		INDEX_TITLE_ID,
		INDEX_ALTTITLE_ID,
		INDEX_GAIN_MESSAGE,
		INDEX_ALTGAIN_MESSAGE,
		INDEX_LEVEL_MESSAGE,
		INDEX_LEVEL_NUMBER
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 10;

	const static int BONUS_MIN = -32767;
	const static int BONUS_MAX = 32767;

private:
	CStringW m_sLevelTitle;
	float m_expStartRange;
	float m_expEndRange;
	int m_menuID;
	int m_proficiency; // Adds to your effective roll.
	int m_perfective; // Decreases the starting point of the proficiency range to reduce low rolls
	int m_titleId;
	int m_altTitleId;
	CStringW m_gainMsg;
	CStringW m_altGainMsg;
	CStringW m_levelMsg;
	int m_levelNumber;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_SKILLLEVELSPROPERTIESCTRL_H_)
