///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

struct ABBOX;
class ReMesh;
class CEXMeshObj;
class CMaterialObject;

// The D3D types were forward-declared, so forward-declare their replacements.
template <size_t, size_t, typename>
class Matrix;
typedef Matrix<4, 4, float> Matrix44f;
template <size_t, typename>
class Vector;
typedef Vector<3, float> Vector3f;

// Basic class for world objects and reference objects [Yanfeng Dec 2008]
// Introduced to make communication between LOD and base object easier because both of them uses CMeshLODObject.
class CStaticMeshObj : public CObject {
public:
	virtual ReMesh* GetReMesh() const = 0; // Return ReMesh used for render
	virtual const Matrix44f& GetMatrix() const = 0; // Transformation matrix from local to world space
	virtual const ABBOX& GetBoundingBox() const = 0; // Bounding box in local or world space, depending on the behavior of subclass
	virtual const CMaterialObject* GetMaterial() const = 0; // Material object

	virtual std::string GetName() const = 0; // Name of this object (Editor)

	//@@Todo:
	//	virtual void SetReMesh(ReMesh* pReMesh) = 0;			// Update ReMesh reference (usually update pointer only, no need to dispose old or clone new)
	//	virtual void SetMesh(CEXMeshObj* pExMesh) = 0;			// Update ReMesh with CEXMeshObj provided: a new ReMesh is created if mesh is not cached otherwise use existing
	//	virtual void SetMatrix(Matrix44f& matrix) = 0;			// Update matrix (copy data)
	//	virtual void SetBoundingBox(const ABBOX& box);			// Update bounding box (copy data)
	//	virtual void SetMaterial(const CMaterialObject* mat) = 0;	// Update material: dispose old if exists and use cloned copy of new material.
	// Note: destructor can call SetMaterial(NULL) to dispose it.

	virtual Vector3f GetPivot() const = 0;
};

//! Parse name and return true if provided ZoneObjectName follow the naming convention of a submesh. Return false if not.
//! Moved from KepEditView_UserInterface.cpp
inline BOOL ParseSubMeshName(const CStringA& ZoneObjectName, CStringA& meshName, int& subID) {
	if (ZoneObjectName.Right(1) == "]") {
		int posLeftBracket = ZoneObjectName.ReverseFind('[');
		if (posLeftBracket != -1) {
			meshName = ZoneObjectName.Left(posLeftBracket);
			CStringA subIDStr = ZoneObjectName.Mid(posLeftBracket + 1, ZoneObjectName.GetLength() - posLeftBracket - 2);
			subID = atoi(subIDStr);
			return TRUE;
		}
	}

	meshName = ZoneObjectName;
	subID = -1;
	return FALSE;
}

//! Compose a submesh name as "mesh[subid]"
inline CStringA MakeSubMeshName(const CStringA& meshName, int subID) {
	if (subID == -1)
		return meshName; // Do not add sub ID if -1

	CStringA fullName;
	fullName.Format("%s[%d]", meshName, subID);
	return fullName;
}

} // namespace KEP