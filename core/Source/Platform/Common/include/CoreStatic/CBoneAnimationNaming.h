///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPHelpers.h"

#define ANIM_VER_ANY -1

enum class eAnimType : int {
	Invalid = -1,
	None = 0,
	Stand = 1,
	Run = 2,
	Walk = 3,
	Dance4 = 4,
	Jump = 5,
	SideStepLeft = 6,
	SideStepRight = 7,
	Die = 8,
	Dance5 = 9,
	Dance6 = 10,
	Dance7 = 11,
	Fire = 12,
	Dance8 = 13,
	SwimIdle = 14,
	SwimAction = 15,
	JetPack = 16,
	Pain = 17,
	Taunt1 = 18,
	Taunt2 = 19,
	Taunt3 = 20,
	Dance1 = 21,
	Dance2 = 22,
	Dance3 = 23,
	Wave = 24,
	Salute = 25,
	Bow = 26,
	Laugh = 27,
	Sit = 28,
	Crouch = 29,
	USER = 30,
	MegaRave = 48
};

#define IS_VALID_ANIM_TYPE(a) (a != eAnimType::Invalid)

namespace KEP {

class CBoneAnimationNaming {
public:
	static eAnimType AnimNameToType(const char* animname) {
		eAnimType animType = eAnimType::Invalid;

		if (StrSameIgnoreCase(animname, "STAND"))
			animType = eAnimType::Stand;
		else if (StrSameIgnoreCase(animname, "RUN"))
			animType = eAnimType::Run;
		else if (StrSameIgnoreCase(animname, "WALK"))
			animType = eAnimType::Walk;
		else if (StrSameIgnoreCase(animname, "DANCE4"))
			animType = eAnimType::Dance4;
		else if (StrSameIgnoreCase(animname, "JUMP"))
			animType = eAnimType::Jump;
		else if (StrSameIgnoreCase(animname, "SIDESTEP LEFT"))
			animType = eAnimType::SideStepLeft;
		else if (StrSameIgnoreCase(animname, "SIDESTEP_RIGHT"))
			animType = eAnimType::SideStepRight;
		else if (StrSameIgnoreCase(animname, "DIE"))
			animType = eAnimType::Die;
		else if (StrSameIgnoreCase(animname, "DANCE5"))
			animType = eAnimType::Dance5;
		else if (StrSameIgnoreCase(animname, "DANCE6"))
			animType = eAnimType::Dance6;
		else if (StrSameIgnoreCase(animname, "DANCE7"))
			animType = eAnimType::Dance7;
		else if (StrSameIgnoreCase(animname, "FIRE"))
			animType = eAnimType::Fire;
		else if (StrSameIgnoreCase(animname, "DANCE8"))
			animType = eAnimType::Dance8;
		else if (StrSameIgnoreCase(animname, "SWIM IDLE"))
			animType = eAnimType::SwimIdle;
		else if (StrSameIgnoreCase(animname, "SWIM ACTION"))
			animType = eAnimType::SwimAction;
		else if (StrSameIgnoreCase(animname, "JET PACK"))
			animType = eAnimType::JetPack;
		else if (StrSameIgnoreCase(animname, "PAIN"))
			animType = eAnimType::Pain;
		else if (StrSameIgnoreCase(animname, "TAUNT 1"))
			animType = eAnimType::Taunt1;
		else if (StrSameIgnoreCase(animname, "TAUNT 2"))
			animType = eAnimType::Taunt2;
		else if (StrSameIgnoreCase(animname, "TAUNT 3"))
			animType = eAnimType::Taunt3;
		else if (StrSameIgnoreCase(animname, "DANCE 1"))
			animType = eAnimType::Dance1;
		else if (StrSameIgnoreCase(animname, "DANCE 2"))
			animType = eAnimType::Dance2;
		else if (StrSameIgnoreCase(animname, "DANCE 3"))
			animType = eAnimType::Dance3;
		else if (StrSameIgnoreCase(animname, "WAVE"))
			animType = eAnimType::Wave;
		else if (StrSameIgnoreCase(animname, "SALUTE"))
			animType = eAnimType::Salute;
		else if (StrSameIgnoreCase(animname, "BOW"))
			animType = eAnimType::Bow;
		else if (StrSameIgnoreCase(animname, "LAUGH"))
			animType = eAnimType::Laugh;
		else if (StrSameIgnoreCase(animname, "SIT"))
			animType = eAnimType::Sit;
		else if (StrSameIgnoreCase(animname, "CROUCH"))
			animType = eAnimType::Crouch;
		else if (STLContainsIgnoreCase(animname, "USER"))
			animType = (eAnimType)(atoi(&animname[4]) + (int)eAnimType::USER + 1);

		return animType;
	}
};

} // namespace KEP
