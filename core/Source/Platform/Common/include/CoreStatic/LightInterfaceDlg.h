///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIGHTINTERFACEDLG_H__97BBED25_3147_4E07_BC2F_AA904CC3B42F__INCLUDED_)
#define AFX_LIGHTINTERFACEDLG_H__97BBED25_3147_4E07_BC2F_AA904CC3B42F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LightInterfaceDlg.h : header file
//
#include "CMaterialClass.h"
#include "CRTLightClass.h"
#include "TextureDatabase.h"
/////////////////////////////////////////////////////////////////////////////
// CLightInterfaceDlg dialog

class CLightInterfaceDlg : public CDialog {
	// Construction
public:
	CLightInterfaceDlg(CWnd* pParent = NULL); // standard constructor

	CRTLightObj* m_lightREF;
	TextureDatabase* GL_textureDataBase;
	// Dialog Data
	//{{AFX_DATA(CLightInterfaceDlg)
	enum { IDD = IDD_DIALOG_LIGHTINTERFACEDLG };
	float m_attenuation0;
	float m_attenuation1;
	float m_attenuation2;
	float m_range;
	BOOL m_render;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLightInterfaceDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CLightInterfaceDlg)
	afx_msg void OnBUTTONMaterialAccess();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIGHTINTERFACEDLG_H__97BBED25_3147_4E07_BC2F_AA904CC3B42F__INCLUDED_)
