#pragma once
#include "StatLink.h"

#include "KepPropertyPage.h"
#include "resource.h"
#include "NewGameLoginDlg.h"
#include <string>

class CGameData;

// CNewGameSelect dialog

class CNewGameSelectDlg : public CKEPPropertyPage {
	DECLARE_DYNAMIC(CNewGameSelectDlg)

public:
	CNewGameSelectDlg(CNewGameLoginDlg& dlg, const char* baseDir, const char* editorBaseDir, bool skipExtraEditorDlgs, bool& defaultDbSettingsOk, Logger& logger, bool showStandAloneGames = false);
	virtual ~CNewGameSelectDlg();

	// Dialog Data
	enum { IDD = IDD_NEW_GAME_SELECT };

	bool GetGameList(const char* defaultName = NULL, const char* consumerKey = NULL, const char* consumerSecret = NULL);
	bool CreateNewGame(CStringA email, CStringA password, CStringA gameName, CStringA gameDescription, bool standAlone, string& consumerKey, string& consumerSecret, CStringA& error);

	// overrides
	BOOL OnSetActive();
	LRESULT OnWizardNext();
	BOOL OnWizardFinish();
	LRESULT OnWizardBack();

	// DEPRECATED - Needs KEPCommon::DRF_ADSIIS_DLL Used Only By KepEditView!
	bool AddGameServer(const char* gameDir, string& distroFolder, const char* platformInstallLocation, int& webAssignedPort);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedNewGame();
	afx_msg void OnListDblClick() {
		((CPropertySheet*)GetParent())->PressButton(PSBTN_NEXT);
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	bool IsHTTPServerServingPatch();

	CStringA m_baseDir;

public:
	CListBox m_startList;
	CNewGameLoginDlg& m_loginDlg;
	CGameData* m_selectedGame;
	Logger m_logger;
	bool m_showStandAloneGames; // added 5/10 to not show "STARs" by default pass in true on constructor to show them
	CStringA m_destDir;
	CStringA m_editorBaseDir;
	bool m_skipExtraEditorDlgs;
	bool* m_defaultDbSettingsOk;
};
