///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "common/include/KEPCommon.h"

#include "Glids.h"
#include "TriggerEnums.h"

#include "common/include/ObjectCounter.h"
#include "Core/Math/Primitive.h"
#include "Core/Util/FunctionTraits.h"
#include "RenderEngine\ReMesh.h"
#include "common\include\CoreStatic\CMaterialClass.h"
#include "include\core\math\PrimitiveType.h"

namespace KEP {

template <typename T>
struct PrimitiveGeomArgs;
class IMemSizeGadget;

class CTriggerObject : private ObjectCounter<CTriggerObject>, public CObject {
	DECLARE_SERIAL(CTriggerObject)

	CTriggerObject() :
			m_inTrigger(false),
			m_triggerAction(eTriggerAction::None),
			m_intParam(0),
			m_oneTimeOnly(false),
			m_everActuated(false),
			m_attachObjId(0),
			m_triggerId(0),
			m_attachGlid(GLID_INVALID),
			m_vectorParam(0.0f, 0.0f, 0.0f),
			m_pRM(NULL),
			m_pMtx(NULL),
			m_pMaterial(NULL),
			m_pendingDelete(false) {}

public:
	CTriggerObject(int triggerId, eTriggerAction triggerAction, int intParam, PrimitiveF_Ptr&& pPrimitive, const std::string& strParam) :
			m_triggerId(triggerId),
			m_triggerAction(triggerAction),
			m_attachObjId(0),
			m_attachGlid(GLID_INVALID),
			m_inTrigger(false),
			m_strParam(strParam),
			m_intParam(intParam),
			m_oneTimeOnly(false),
			m_pPrimitive(std::move(pPrimitive)), // always move - caller should always construct a new movable instance
			m_everActuated(false),
			m_vectorParam(0.0f, 0.0f, 0.0f),
			m_pRM(nullptr),
			m_pMtx(nullptr),
			m_pMaterial(nullptr),
			m_pendingDelete(false) {}

	CTriggerObject(const CTriggerObject& rhs, PrimitiveF_Ptr&& pPrimitive) :
			m_triggerAction(rhs.m_triggerAction),
			m_intParam(rhs.m_intParam),
			m_strParam(rhs.m_strParam),
			m_vectorParam(rhs.m_vectorParam),
			m_oneTimeOnly(rhs.m_oneTimeOnly),
			m_attachGlid(rhs.m_attachGlid),
			m_pPrimitive(std::move(pPrimitive)), // always move - caller should always construct a new movable instance
			m_attachObjId(rhs.m_attachObjId),
			m_triggerId(rhs.m_triggerId),
			m_pendingDelete(rhs.m_pendingDelete),
			m_everActuated(false),
			m_inTrigger(false),
			m_pRM(nullptr),
			m_pMtx(nullptr),
			m_pMaterial(nullptr) {}

	CTriggerObject(const CTriggerObject& rhs) = delete;
	CTriggerObject(CTriggerObject&& rhs) = delete;
	CTriggerObject& operator=(const CTriggerObject& rhs) = delete;
	CTriggerObject& operator=(CTriggerObject&& rhs) = delete;
	bool operator==(const CTriggerObject& trg) = delete;
	bool operator!=(const CTriggerObject& trg) = delete;

	virtual ~CTriggerObject() {
		if (m_pRM)
			delete m_pRM;
		m_pRM = nullptr;
		if (m_pMtx)
			delete m_pMtx;
		m_pMtx = nullptr;
		if (m_pMaterial)
			delete m_pMaterial;
		m_pMaterial = nullptr;
	}

	std::string ToStr() {
		std::string str;
		const char* trigActStr = TriggerActionStr(m_triggerAction);
		const char* primTypeStr = m_pPrimitive ? PrimitiveTypeStr(m_pPrimitive->GetType()) : "";
		auto pos = GetPosition();
		StrBuild(str, "TO<" << m_attachObjId << ":" << m_triggerId
							<< " " << trigActStr
							<< " " << primTypeStr
							<< " pos=[" << FMT_FLT << pos.x << " " << pos.y << " " << pos.z << "]"
							<< " rad=" << GetSphereRadius()
							<< (IsPendingDelete() ? " DELETING" : "")
							<< ">");
		return str;
	}

	void Serialize(CArchive& ar);

	void Clone(CTriggerObject** ppTO_out);

	void SetTriggerId(int triggerId) {
		m_triggerId = triggerId;
	}
	int GetTriggerId() const {
		return m_triggerId;
	}

	eTriggerState EvaluatePosition(const Vector3f& pos);

	eTriggerState ExitTrigger();

	eTriggerAction GetTriggerAction() const {
		return m_triggerAction;
	}
	void SetTriggerAction(eTriggerAction triggerAction) {
		m_triggerAction = triggerAction;
	}

	int GetIntParam() const {
		return m_intParam;
	}
	void SetIntParam(int intParam) {
		m_intParam = intParam;
	}

	std::string GetStrParam() const {
		return m_strParam;
	}
	void SetStrParam(const std::string& strParam) {
		m_strParam = strParam;
	}

#if (DRF_OLD_PORTALS == 1)
	const Vector3f& GetVector() const {
		return m_vectorParam;
	}
#endif
	void SetVector(const Vector3f& vec) {
		m_vectorParam = vec;
	}

	void SetOneTimeOnly(bool val) {
		m_oneTimeOnly = val;
	}

	int GetAttachObjId() const {
		return m_attachObjId;
	}
	void SetAttachObjId(int objId) {
		m_attachObjId = objId;
	}

	int GetAttachGlid() const {
		return m_attachGlid;
	}
	void SetAttachGlid(GLID glid) {
		m_attachGlid = glid;
	}

	bool IsInTrigger() const {
		return m_inTrigger;
	}
	void SetInTrigger(bool val) {
		m_inTrigger = val;
	}

	bool IsPendingDelete() const {
		return m_pendingDelete;
	}
	void SetPendingDelete() {
		m_pendingDelete = true;
	}

	Vector3f GetPosition() const {
		return m_pPrimitive ? m_pPrimitive->GetOrigin() : Vector3f(0.0, 0.0, 0.0);
	}
	void SetPosition(const Vector3f& pos) {
		static Vector3f dir = Vector3f::Basis(2); // Z+
		SetTransform(pos, dir);
	}

	void SetTransform(const Vector3f& pos, const Vector3f& dir) {
		static Vector3f up = Vector3f::Basis(1); // Y+
		if (!m_pPrimitive)
			return;
		m_pPrimitive->SetTransform(pos, dir, up);
	}

	ReMesh* GetMesh();
	Matrix44f* GetMatrix();

	// always move - caller should always construct a new movable instance
	void SetVolume(PrimitiveF_Ptr&& volume) {
		m_pPrimitive = std::move(volume);
	}

	float GetSphereRadius() const;

	Vector3f GetHalfExtents() const;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	int m_triggerId; // drf - added

	eTriggerAction m_triggerAction;

	bool m_pendingDelete;

	int m_intParam; // numeric parameter for miscellaneous purposes. For TA_OLD_SCRIPTING, it is used to store custom event ID
	std::string m_strParam; // string parameter for miscellaneous purposes. For TA_OLD_SCRIPTING, it is used to store custom event name
	Vector3f m_vectorParam; // 3D position parameter for TA_ACTIVATE_PORTAL and TA_ACTIVATE_FORCE_PAD
	bool m_oneTimeOnly; // TRUE if one-time trigger

	PrimitiveF_Ptr m_pPrimitive; // trigger shape primitive

	bool m_everActuated; // runtime state: TRUE if trigger has actuated before
	bool m_inTrigger; // runtime state: true if currently inside trigger

	GLID m_attachGlid; // global ID of the dynamic object the trigger is attached to. 0 if world object trigger.
	int m_attachObjId; // placement ID of the dynamic object the trigger is attached to. 0 if world object trigger.

	// Visualization data
	ReMesh* m_pRM; // mesh for visualizing triggers
	Matrix44f* m_pMtx; // transformation matrix for visualizing triggers
	CMaterialObject* m_pMaterial; // material for visualizing trigger
};

class CTriggerObjectList : public CObject {
public:
	CTriggerObjectList() {}

	virtual ~CTriggerObjectList() {}

	void Log(const std::string& name);

	void Clone(CTriggerObjectList** cloneList);

	CTriggerObject* AddTrigger(
		int triggerId,
		eTriggerAction triggerAction,
		int intParam,
		const PrimitiveGeomArgs<float>& volumeArgs,
		const std::string& strParam,
		const Vector3f& vectorParam,
		const Vector3f& origin,
		const Vector3f& dir,
		bool oneTimeOnly,
		int placementId);

	CTriggerObject* AddTrigger(std::shared_ptr<CTriggerObject> pTO);

	void AddTriggers(const CTriggerObjectList& list) {
		for (const auto& pTO : list.m_pTOs) {
			if (!pTO)
				continue;
			AddTrigger(pTO);
		}
	}

	CTriggerObject* GetTriggerByPlacementId(int placementId);

	void CleanupPendingDeletions();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	void Serialize(CArchive& ar);

	DECLARE_SERIAL(CTriggerObjectList)

	void Clear() {
		m_pTOs.clear();
	}

	bool IsEmpty() const {
		return m_pTOs.empty();
	}

	size_t GetTriggerCount() const {
		return m_pTOs.size();
	}

	template <typename Callable>
	void ForEachTrigger(Callable&& callable) {
		for (const auto& pTrigger : m_pTOs) {
			bool bContinue = true;
			CallPossiblyVoidFunction(&bContinue, std::forward<Callable>(callable), pTrigger.get());
			if (!bContinue)
				break;
		}
	}

private:
	std::vector<std::shared_ptr<CTriggerObject>> m_pTOs;
};

} // namespace KEP