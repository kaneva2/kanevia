///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class CLightTrackerObj : public CObject {
public:
	CLightTrackerObj();

	int m_hardwareIndex;
};

class CLightTrackerObjList : public CObList {
public:
	CLightTrackerObjList(){};

	int AddLight();
	void LightHasBeenRemoved(int hardwareIndex);
	void SafeDelete();
};

} // namespace KEP