///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADDSCRIPTEVENTDLG_H__6C56CA61_574A_11D3_AFDD_0000B45FDB0F__INCLUDED_)
#define AFX_ADDSCRIPTEVENTDLG_H__6C56CA61_574A_11D3_AFDD_0000B45FDB0F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddScriptEventDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAddScriptEventDlg dialog

class CAddScriptEventDlg : public CDialog {
	// Construction
public:
	CAddScriptEventDlg(int type, CWnd* pParent = NULL); // standard constructor

	// Dialog Data
	//{{AFX_DATA(CAddScriptEventDlg)
	enum { IDD = IDD_DIALOG_ADDSCRIPTEVENT };
	CStringA m_event;
	int m_miscInt;
	CStringA m_miscText;
	//}}AFX_DATA
	int m_type;
	int m_eventId;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddScriptEventDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAddScriptEventDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonBrowse();
	virtual void OnOK();
	afx_msg void OnCbnSelchangeComboEvent();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	static const char* GetEventNameFromNum(int num);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDSCRIPTEVENTDLG_H__6C56CA61_574A_11D3_AFDD_0000B45FDB0F__INCLUDED_)
