#pragma once

#include <vorbis/vorbisenc.h>

#include <string>

class OggConverter {
public:
	static const int NoConversionError = 0;
	static const int UnableToOpenSourceFile = -1;
	static const int UnrecognizedFileType = -2;
	static const int MemoryError = -3;
	static const int UnSupportedCodec = -4;
	static const int VorbisEncodeError = -5;
	static const int UnSupportedChannels = -6;

	static BYTE* convert(const std::string& filePathWav, int& retBufLen, int& errorCode);
};
