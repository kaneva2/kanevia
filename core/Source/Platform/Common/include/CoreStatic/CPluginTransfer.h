///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class CPluginTransfer {
public:
	CPluginTransfer();
	~CPluginTransfer(){};

	BYTE* m_miscBlock;
	DWORD m_miscSize;
	UINT m_linkMSG;
	//FUNCTIONS

	BOOL ReceiveMeshExport();
	BOOL BroadCastData(byte* memBlock, DWORD sizeMemBlock);
	BOOL ReceiveMemory(byte** dataReturn, DWORD* memSize);
	BOOL ClearBlock();
	BOOL AppendToBlock(BYTE* newData, DWORD size);
	BOOL AppendStringSegToBlock(CStringA stringLoc, short extData);
	BOOL BroadCastData();

	void GetMesh(
		BOOL* smoothShading,
		float* sightRange,
		CStringA& fileName,
		ABVERTEX** vArray,
		WORD** iArray,
		WORD* vCount,
		WORD* iCount,
		CStringA& texture1,
		CStringA& texture2,
		CStringA& texture3,
		CStringA& texture4,
		CStringA& texture5,
		CStringA& texture6,
		CStringA& texture7,
		CStringA& texture8);

	BOOL MeshInQueue();

	//VARIABLES

	//UINT m_linkMSG;

	DWORD m_meshesLeft;
	DWORD m_memBlockSize;

	BYTE* m_dataPtr;
	DWORD m_dataOffset;
};

} // namespace KEP