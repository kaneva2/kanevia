///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// CommerceDlg.h : header file
//
#include "CCommerceClass.h"
#include "CGlobalInventoryClass.h"
/////////////////////////////////////////////////////////////////////////////
// CCommerceDlg dialog
#include "CEXMeshClass.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "StorePropertiesCtrl.h"
#include "AreaOfEffectPropertiesCtrl.h"
#include "RaceFilterPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

class CCommerceDlg : public CKEPEditDialog {
public:
	/////////////////////////////
	// Constructors/Destructor //
	/////////////////////////////

	CCommerceDlg(CCommerceObjList*& commerceObjDB, CWnd* pParent = NULL); // standard constructor

	///////////////////////
	// Selection Methods //
	///////////////////////

	CGlobalInventoryObj* GetSelectedGlobalInventoryObj();
	CCommerceObj* GetSelectedCommerceObj();
	CCommerceObj* GetCommerceObj(int index);
	CStoreInventoryObj* GetSelectedStoreInventoryObj();

	/////////////////////////////
	// Public Member Variables //
	/////////////////////////////

	enum { IDD = IDD_DIALOG_COMMERCE };

	CListBox m_storeInventory;
	CListBox m_globalInventory;
	CListBox m_commerceDB;
	CCommerceObjList*& m_commerceObjDBRef;

protected:
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommerceDlg)
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	////////////////////////////
	// Initialization Methods //
	////////////////////////////

	void InitializeCommerceDBList(int selection);
	void InitializeStoreInventoryList(int selection);
	void InitializeGlobalInventoryList(int selection);

	/////////////////////////////////////
	// Windows Message Handler Methods //
	/////////////////////////////////////

	// Generated message map functions
	//{{AFX_MSG(CCommerceDlg)
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBUTTONAddItem();
	afx_msg void OnBUTTONAddStore();
	afx_msg void OnBUTTONDeleteItem();
	afx_msg void OnBUTTONDeleteStore();
	afx_msg void OnBUTTONReplaceStore();
	afx_msg void OnSelchangeLISTCommerceDB();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonSave();
	afx_msg void OnLbnSelchangeListStoreinventory();
	afx_msg void OnBnClickedButtonReplaceitem();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	//////////////////////
	// Accessor Methods //
	//////////////////////

	void SetStoreInventory(int GLID);
	void ReplaceStore(int selection);
	void AddInventoryItem();
	void ReplaceInventoryItem(int storeSelection, int inventorySelection);

private:
	CCommerceObj* m_selectedCommerceObj;

	CCollapsibleGroup m_colStores;
	CStorePropertiesCtrl m_propStore;
	CAreaOfEffectPropertiesCtrl m_propAreaOfEffect;
	CButton m_grpStores;

	CCollapsibleGroup m_colInventory;
	CRaceFilterPropertiesCtrl m_propRaceFilter;
	CButton m_grpInventory;

	CKEPImageButton m_btnAddStore;
	CKEPImageButton m_btnDeleteStore;
	CKEPImageButton m_btnReplaceStore;

	CKEPImageButton m_btnAddItem;
	CKEPImageButton m_btnDeleteGlobalInventoryItem;
	CKEPImageButton m_btnReplaceGlobalInventoryItem;

	int m_previousSelStore;
	int m_previousSelInventory;

	///////////////////////////////
	// Properties Helper Methods //
	///////////////////////////////

	void RedrawAllPropertiesCtrls();
	void LoadStoreProperties();

protected:
	afx_msg BOOL DestroyWindow();

	///////////////////////
	// Load/Save Methods //
	///////////////////////

	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");

	///////////////////////
	// Scripting Methods //
	///////////////////////

	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
