/******************************************************************************
 ItemContentsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ITEM_CONTENTSPROPERTIESCTRL_H_)
#define _ITEM_CONTENTSPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ItemContentsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

class CSpawnGenDlg;
class CSkeletonObject;

/////////////////////////////////////////////////////////////////////////////
// CItemContentsPropertiesCtrl window

class CItemContentsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CItemContentsPropertiesCtrl(CSpawnGenDlg* pSpawnGenDlg);
	virtual ~CItemContentsPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetGlobalInventoryItemID() const {
		return m_globalInventoryItemID;
	}
	bool GetArmed() const {
		return m_bArmed;
	}
	int GetQuantity() const {
		return m_quantity;
	}

	// modifiers
	void SetGlobalInventoryItemID(int globalInventoryItemID) {
		m_globalInventoryItemID = globalInventoryItemID;
	}
	void SetArmed(bool bArmed) {
		m_bArmed = bArmed;
	}
	void SetQuantity(int quantity) {
		m_quantity = quantity;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CItemContentsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CItemContentsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_GLOBAL_INVENTORY_ITEM,
		INDEX_ARMED,
		INDEX_QUANTITY,
		INDEX_EQUIP_ARMEDITEM_REF,
		INDEX_EQUIP_MESH_SLOT,
		INDEX_EQUIP_MESH_CONFIG,
		INDEX_EQUIP_MATERIAL_CONFIG
	};

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	void InitGlobalInventoryItems();
	void PopulateArmedItemRef();
	void PopulateMeshSlot();
	void PopulateMeshConfigurations();
	void PopulateMaterialConfigurations();
	void ApplyConfigurations();

private:
	CSpawnGenDlg* m_pSpawnGenDlg;

	int m_globalInventoryItemID;
	bool m_bArmed;
	int m_quantity;
	int m_equipArmedItemRefID;
	int m_equipMeshSlotID;
	int m_equipMeshConfigID;
	int m_equipMaterialConfigID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ITEM_CONTENTSPROPERTIESCTRL_H_)
