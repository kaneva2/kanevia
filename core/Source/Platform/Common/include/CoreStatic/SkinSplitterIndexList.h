///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "afxext.h"

#pragma once

#define SKINSPLITTERINVALIDINDEX 0xffffffff

typedef struct {
	unsigned int* indexes;
	unsigned char* usedMap;
	unsigned int maxSize;
	unsigned int maxValue;
	unsigned int currentSize;
} SkinSplitterIndexList;

SkinSplitterIndexList* IndexListCreate(const unsigned int maxSize, const unsigned int maxValue);
void IndexListDestroy(SkinSplitterIndexList* list);
void IndexListReset(SkinSplitterIndexList* list);
void IndexListDeleteIndexes(SkinSplitterIndexList* list, unsigned int n);
void IndexListInvalidateIndex(SkinSplitterIndexList* list, unsigned int i);
void IndexListRevalidateIndex(SkinSplitterIndexList* list, unsigned int i);
void IndexListRevalidateIndexes(SkinSplitterIndexList* list);
BOOL IndexListFull(SkinSplitterIndexList* list);
unsigned int* IndexListGetList(SkinSplitterIndexList* list);
unsigned int IndexListGetSize(const SkinSplitterIndexList* list);
BOOL IndexListIndexUsed(const SkinSplitterIndexList* list, const unsigned int i);
BOOL IndexListInvalidIndex(const SkinSplitterIndexList* list, const unsigned int i);
unsigned int IndexListGetIndex(const SkinSplitterIndexList* list, const unsigned int i);
unsigned int IndexListSetIndex(const SkinSplitterIndexList* list, const unsigned int i, const unsigned int val);
BOOL IndexListAddIndex(SkinSplitterIndexList* list, const unsigned int index);
BOOL IndexListAddIndexes(SkinSplitterIndexList* list, const unsigned int* indexes, const unsigned int n);
BOOL IndexListMerge(SkinSplitterIndexList* targetList, const SkinSplitterIndexList* list);
unsigned int IndexListGetFirstUnused(const SkinSplitterIndexList* list, const unsigned int ix);
unsigned int IndexListContainsIndex(const SkinSplitterIndexList* list, const unsigned int index);
unsigned int IndexListSort(SkinSplitterIndexList* list, const BOOL increasingOrder);
unsigned int IndexListIndexSort(SkinSplitterIndexList* list, const BOOL increasingOrder);

