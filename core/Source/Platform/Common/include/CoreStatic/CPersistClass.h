///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

#include "CMovementObj.h"

namespace KEP {

class CPersistObj : public CObject, public GetSet {
protected:
	CPersistObj() {}

	DECLARE_SERIAL(CPersistObj);

public:
	CPersistObj(int entDBIndex, const Matrix44f& entMatrix, eActorControlType controlType);

	eActorControlType m_entControlType;
	int m_entDBIndex;
	Matrix44f m_entMatrix;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CPersistObjList : public CObList {
public:
	CPersistObjList(){};

	DECLARE_SERIAL(CPersistObjList);
};

} // namespace KEP