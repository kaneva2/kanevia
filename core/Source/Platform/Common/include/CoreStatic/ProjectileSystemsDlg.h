/******************************************************************************
 ProjectileSystemsDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_PROJECTILE_SYSTEMS_DLG_H_)
#define _PROJECTILE_SYSTEMS_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProjectileSystemsDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "WeaponPropertiesCtrl.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "CMissileClass.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

////////////////////////////////////////////
// CProjectileSystemsDialog control

class CProjectileSystemsDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CProjectileSystemsDialog)

public:
	CProjectileSystemsDialog();
	virtual ~CProjectileSystemsDialog();

	//getter
	CMissileObj* GetMissileObj() {
		return m_misPtr;
	}
	int GetPreviousSelection() {
		return m_previousSel;
	}
	//setter
	void SetSkillDatabase(CSkillObjectList* skillDatabase) {
		m_skillDatabase = skillDatabase;
	}
	void SetElementDatabase(CElementObjList* elementDB) {
		m_elementDB = elementDB;
	}
	void SetMissileObj(CMissileObj* misPtr) {
		m_misPtr = misPtr;
	}
	void SetPreviousSelection(int selection) {
		m_previousSel = selection;
	}

	void LoadOptions();
	void LoadMissleObj();
	void SaveMissleObj();

	void RedrawPropertiesCtrls();

	// Dialog Data
	//{{AFX_DATA(CProjectileSystemsDialog)
	enum { IDD = IDD_DIALOG_MISSILE_INTERFACE };
	afx_msg void OnBTNMISReplaceValues();
	afx_msg void OnSelchangeLISTIconDatabase();
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CProjectileSystemsDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

	void ReplaceProjectileSystem(int selection);

private:
	CSkillObjectList* m_skillDatabase;
	CElementObjList* m_elementDB;

	CMissileObj* m_misPtr;

	CCollapsibleGroup m_colWeapons;
	CWeaponPropertiesCtrl m_propWeapon;
	CButton m_grpWeapons;

	CKEPImageButton m_btnDeleteProjectileSystem;
	CKEPImageButton m_btnReplaceProjectileSystem;
	CKEPImageButton m_btnSaveProjectileSystemUnit;
	CKEPImageButton m_btnLoadProjectileSystemUnit;

	CKEPImageButton m_btnAddStaticMeshProjectile;
	CKEPImageButton m_btnAddBillboardMeshProjectile;
	CKEPImageButton m_btnAddSkeletalMeshProjectile;

	CKEPImageButton m_btnAddLight;
	CKEPImageButton m_btnDeleteLight;
	CKEPImageButton m_btnSound;

	CKEPImageButton m_btnAddLaser;
	CKEPImageButton m_btnVisualProperties;
	CKEPImageButton m_btnDynamics;
	//last selected index in the list box
	int m_previousSel;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_PROJECTILE_SYSTEMS_DLG_H_)