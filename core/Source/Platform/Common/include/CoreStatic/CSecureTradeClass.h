///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

struct CHAR_ITEM_DATA;
class CInventoryObjList;
class CGlobalInventoryObjList;
class CPlayerObject;
class CPlayerObjectList;

class CSecureTradeObj : public CObject {
public:
	CSecureTradeObj();
	void SafeDelete();

	int m_uniqueTradeID;
	int m_hostNetworkID;
	int m_clientNetworkID;
	BOOL m_hostOfferCompleted;
	BOOL m_clientOfferCompleted;
	BOOL m_hostAccept;
	BOOL m_clientAccept;
	CInventoryObjList* m_hostInvenOffer;
	int m_hostCashOffer;
	CInventoryObjList* m_clientInvenOffer;
	int m_clientCashOffer;
	TimeMs m_timeStamp;
};

// returns from EvaluateAllTrades
const int TRADE_NONE_FOUND = 0;
const int TRADE_OK = 1;
const int TRADE_CLIENT_OVERLOADED = 2;
const int TRADE_HOST_OVERLOADED = 3;
const int TRADE_HOST_CANT_TRADE_ITEM = 4;
const int TRADE_CLIENT_CANT_ACCEPT_ITEM = 5;
const int TRADE_CLIENT_CANT_TRADE_ITEM = 6;
const int TRADE_HOST_CANT_ACCEPT_ITEM = 7;

class CSecureTradeObjList : public CObList {
public:
	CSecureTradeObjList(){};
	void SafeDelete();
	int EvaluateAllTrades(CPlayerObjectList* runtimeServerPlayerDB,
		CPlayerObject*& hostPtr,
		CPlayerObject*& clientPtr,
		int maxItemCount);
	BOOL InitiateTrade(int hostNetworkID, int clientNetworkID, CPlayerObjectList* runtimeServerPlayerDB);
	int GetUniqueTradeID();
	BOOL IsPlayerInvolvedInTrade(int id);
	BOOL AddCashToOffer(int id, int cashAmount, int* fowardInfoToID);
	BOOL CancelTransaction(int id, int* notifyIDofCancelation);
	BOOL AddItemsToOffer(int fromNetAssignedId, CHAR_ITEM_DATA* itemArray, int itemCount,
		CGlobalInventoryObjList* globalInventoryBD, int* fowardInfoToID);

	BOOL AddItemToOffer(int id,
		int gl_id,
		int quanity,
		CGlobalInventoryObjList* globalInventoryBD,
		int* fowardInfoToID);
	BOOL AcceptTradeConditions(int id, int* sendResponseTo);

private:
	void AddItemsToOffer(CInventoryObjList* inv, CHAR_ITEM_DATA* itemArray, int itemCount,
		CGlobalInventoryObjList* globalInventoryBD);
	void ClearTradeAccepts(int id);
};

} // namespace KEP