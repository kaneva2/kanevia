///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

#include "ListItemMap.h"
#include "KEPObList.h"

#include <jsThread.h>

namespace KEP {

class CAdvancedVertexObj;
class CMaterialObject;
class CStateManagementObj;
class TextureDatabase;

class CIconSysObj : public CObject, public GetSet {
	DECLARE_SERIAL(CIconSysObj);

public:
	CIconSysObj();

	CStringA m_iconName;
	CAdvancedVertexObj* m_vertexObject;
	CMaterialObject* m_materialObj;
	float m_gridDensity;
	float m_xIndex;
	float m_yIndex;
	float m_size;
	DWORD m_textureIndex;
	CStringA m_textureName;
	int m_controlIDRef;
	int m_controlContentIDRef;
	Vector3f m_refPosition;
	BOOL m_refreshed;
	int m_indexRef;

	void Serialize(CArchive& ar);
	void SafeDelete();
	void Initialize(Vector3f position, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Render(LPDIRECT3DDEVICE9 g_pd3dDevice,
		CStateManagementObj* stateManager,
		TextureDatabase* textureDatabase);
	void Clone(CIconSysObj** clone);

	DECLARE_GETSET
};

class CIconSysList : public CKEPObList {
public:
	static CIconSysList* GetInstance() {
		if (m_tlsId == 0) {
			m_tlsId = jsThread::createThreadData();
		}

		CIconSysList* ret = (CIconSysList*)jsThread::getThreadData(m_tlsId);
		if (ret == 0) {
			ret = new CIconSysList(); // this will set the tls
		}
		return ret;
	}

	virtual ~CIconSysList() {
		if (m_tlsId != 0) {
			CIconSysList* gil = (CIconSysList*)jsThread::getThreadData(m_tlsId);
			jsAssert(gil == 0 || gil == this);
			if (gil == this)
				jsThread::setThreadData(m_tlsId, 0);
		}
	}

	void SafeDelete();

	DECLARE_SERIAL(CIconSysList);

	CIconSysObj* GetByIndex(int index);
	BOOL AddIconFromDBtoThisDB(CIconSysList* primaryDB,
		int index,
		Vector3f screenPos,
		LPDIRECT3DDEVICE9 g_pd3dDevice);
	void RenderAll(LPDIRECT3DDEVICE9 g_pd3dDevice,
		CStateManagementObj* stateManager,
		TextureDatabase* textureDatabase);
	void Initialize(Vector3f position, LPDIRECT3DDEVICE9 g_pd3dDevice);
	BOOL AddIconFromDBtoThisDBWspecs(CIconSysList* primaryDB,
		int index,
		Vector3f screenPos,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int controlIDRef,
		int controlContentIDRef,
		float scale);
	BOOL TagAllRefreshed(LPDIRECT3DDEVICE9 g_pd3dDevice,
		int controlIDRef,
		BOOL refreshState);
	BOOL RemoveAllNonRefreshed(LPDIRECT3DDEVICE9 g_pd3dDevice,
		int controlIDRef);
	CIconSysObj* GetIconByCriteria(LPDIRECT3DDEVICE9 g_pd3dDevice,
		int controlIDRef,
		int controlContentIDRef,
		Vector3f position,
		int indexRef);

	ListItemMap GetIcons();

protected:
	CIconSysList() {
		if (m_tlsId == 0) {
			m_tlsId = jsThread::createThreadData();
		}
		jsThread::setThreadData(m_tlsId, (ULONG)this);
	};

private:
	static int m_tlsId;
};

} // namespace KEP