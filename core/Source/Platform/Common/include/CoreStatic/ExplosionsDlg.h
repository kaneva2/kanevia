/******************************************************************************
 ExplosionsDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_EXPLOSIONS_DLG_H_)
#define _EXPLOSIONS_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExplosionsDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

////////////////////////////////////////////
// CExplosionsDialog control

class CExplosionsDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CExplosionsDialog)

public:
	CExplosionsDialog();
	virtual ~CExplosionsDialog();

	// Dialog Data
	//{{AFX_DATA(CExplosionsDialog)
	enum { IDD = IDD_DIALOG_EXPLOSIONS };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CExplosionsDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colExplosions;
	CButton m_grpExplosions;

	CKEPImageButton m_btnAddExplosion;
	CKEPImageButton m_btnDeleteExplosion;
	CKEPImageButton m_btnExplosionProperties;
	CKEPImageButton m_btnSaveExplosionUnit;
	CKEPImageButton m_btnLoadExplosionUnit;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_EXPLOSIONS_DLG_H_)