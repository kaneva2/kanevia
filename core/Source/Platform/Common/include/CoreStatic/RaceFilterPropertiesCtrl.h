/******************************************************************************
 RaceFilterPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_RACE_FILTERPROPERTIESCTRL_H_)
#define _RACE_FILTERPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RaceFilterPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CRaceFilterPropertiesCtrl window

class CRaceFilterPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CRaceFilterPropertiesCtrl();
	virtual ~CRaceFilterPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetActorID() const {
		return m_actorID;
	}

	// modifiers
	void SetActorID(int actorID) {
		m_actorID = actorID;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRaceFilterPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CRaceFilterPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_ACTOR_ID };

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	void InitActors();

private:
	int m_actorID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_RACE_FILTERPROPERTIESCTRL_H_)
