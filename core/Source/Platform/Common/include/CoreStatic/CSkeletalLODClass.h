///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPConstants.h"

namespace KEP {

class IMemSizeGadget;

class CSkeletalLODObject : public CObject, public GetSet {
	DECLARE_SERIAL(CSkeletalLODObject)
public:
	CSkeletalLODObject();
	float m_distance; //greater than activate it

	void Clone(CSkeletalLODObject** copy);
	void SafeDelete();
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CSkeletalLODObjectList : public CObList {
public:
	CSkeletalLODObjectList() {
		m_NumLOD = 0;
	}
	void Serialize(CArchive& ar);
	void SafeDelete();
	void Clone(CSkeletalLODObjectList** copy);
	int GetLODIndex(int currentDistance);
	int GetNumLOD() {
		return m_NumLOD;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	FLOAT m_LODDistance[SKELETAL_LODS_MAX];
	DWORD m_NumLOD;

	DECLARE_SERIAL(CSkeletalLODObjectList)
};

} // namespace KEP