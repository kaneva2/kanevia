/******************************************************************************
 BankZonePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_BANK_ZONEPROPERTIESCTRL_H_)
#define _BANK_ZONEPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BankZonePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "CEXMeshClass.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CBankZonePropertiesCtrl window

class CBankZonePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CBankZonePropertiesCtrl();
	virtual ~CBankZonePropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetBankName() const {
		return Utf16ToUtf8(m_bankName).c_str();
	}
	int GetChannelID() const {
		return m_channelID;
	}
	int GetMenuBind() const {
		return m_menuBind;
	}
	BOOL GetDealWithMurderers() const {
		return m_dealWithMurderers;
	}
	float GetXMin() const {
		return m_xMin;
	}
	float GetXMax() const {
		return m_xMax;
	}
	float GetYMin() const {
		return m_yMin;
	}
	float GetYMax() const {
		return m_yMax;
	}
	float GetZMin() const {
		return m_zMin;
	}
	float GetZMax() const {
		return m_zMax;
	}

	// modifiers
	void SetBankName(CStringA bankName) {
		m_bankName = Utf8ToUtf16(bankName).c_str();
	}
	void SetChannelID(int channelID) {
		m_channelID = channelID;
	}
	void SetMenuBind(int menuBind) {
		m_menuBind = menuBind;
	}
	void SetDealWithMurderers(BOOL dealWithMurderers) {
		m_dealWithMurderers = dealWithMurderers;
	}
	void SetXMin(float xMin) {
		m_xMin = xMin;
	}
	void SetXMax(float xMax) {
		m_xMax = xMax;
	}
	void SetYMin(float yMin) {
		m_yMin = yMin;
	}
	void SetYMax(float yMax) {
		m_yMax = yMax;
	}
	void SetZMin(float zMin) {
		m_zMin = zMin;
	}
	void SetZMax(float zMax) {
		m_zMax = zMax;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBankZonePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CBankZonePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_BANK_NAME,
		INDEX_CHANNEL_ID,
		INDEX_MENU_BIND,
		INDEX_DEAL_MURDERERS,
		INDEX_AREA_OF_EFFECT,
		INDEX_X_MIN,
		INDEX_X_MAX,
		INDEX_Y_MIN,
		INDEX_Y_MAX,
		INDEX_Z_MIN,
		INDEX_Z_MAX,
		INDEX_LOAD_BOX
	};

	const static int X_MIN_MIN = 0;
	const static int X_MIN_MAX = 0;
	const static int X_MAX_MIN = 0;
	const static int X_MAX_MAX = 0;
	const static int Y_MIN_MIN = 0;
	const static int Y_MIN_MAX = 0;
	const static int Y_MAX_MIN = 0;
	const static int Y_MAX_MAX = 0;
	const static int Z_MIN_MIN = 0;
	const static int Z_MIN_MAX = 0;
	const static int Z_MAX_MIN = 0;
	const static int Z_MAX_MAX = 0;

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	void InitChannels();
	void InitMenus();

private:
	CStringW m_bankName;
	int m_channelID;
	int m_menuBind;
	BOOL m_dealWithMurderers;
	float m_xMin;
	float m_xMax;
	float m_yMin;
	float m_yMax;
	float m_zMin;
	float m_zMax;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_BANK_ZONEPROPERTIESCTRL_H_)
