///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPObList.h"
#include "BoundBox.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include <vector>
#include <map>

namespace KEP {
class ReMesh;
class ReD3DX9DeviceState;
class CReferenceObjList;
} // namespace KEP

// Sub-box face index array compression settings
#define COMPRESSION_FACE_COUNT_THRESHOLD 10 // A sub-box with more than 10 faces will be considered
#define COMPRESSION_FACE_SUBSET_THRESHOLD 0.8 // A sub-box that owns 80% of the faces from its parent will be ignored

#define DEF_COL_RES_X 2
#define DEF_COL_RES_Y 2
#define DEF_COL_RES_Z 2
#define DEF_COL_LEVELSDEEP 10
#define DEF_COL_FACESPERBOX 100

namespace KEP {

class CWldObject;
class CWldObjectList;

struct CPolyRetStruct {
	Vector3f vertices[3];
};

struct REFBOX {
	std::vector<WORD> m_objectReferenceList; // Objects associated with current box, for CWldObject rendering (popout box) and trigger calculations
	unsigned m_faceIndexCacheKey; // Key into CCollisionObj::m_faceIndexArrayCache - faces associated with current box, for collision calculations
	int m_subTableLevel; // Current sub-box level (-1: entire world, 0: first level, 1: second level, etc.)
	REFBOX*** m_subBoxCollection; // Child nodes

	static const unsigned INVALID_FACE_INDEX_CACHE_KEY = 0xffffffff;
};

class CCollisionObj : public CObject {
	DECLARE_SERIAL_SCHEMA(CCollisionObj, VERSIONABLE_SCHEMA | 1);

public:
	CCollisionObj(size_t maxLevels = DEF_COL_LEVELSDEEP, size_t maxFacesPerBox = DEF_COL_FACESPERBOX);
	~CCollisionObj();

	void SafeDelete();
	void Serialize(CArchive& ar);
	void Clone(CCollisionObj** clone);

	static bool Compare(CCollisionObj* col1, CCollisionObj* col2);

	// Reduce collision data size for uncompressed schema v0 data
	void Compress(REFBOX* curBox = nullptr);

private:
	// Supporting functions for Serialization, Cloning and Comparison
	void SubLevelSave(REFBOX*** CurrentSubBox, CArchive& ar, std::set<unsigned>& cacheKeys);
	void SubLevelLoad(REFBOX*** CurrentSubBox, int curLevel, CArchive& ar, UINT schemaVersion);
	void SubLevelDelete(REFBOX*** CurrentSubBox);
	void CloneSubBox(REFBOX*** cloneBox, REFBOX*** sourceBox);
	bool CompareSubBox(REFBOX*** box1, REFBOX*** box2);

public:
	//! \brief Calculate collision data from all meshes provided through CWldObjectList.
	//! \param worldList Meshes for collision computation.
	//!
	//! Calculate collision data from all meshes provided through CWldObjectList.
	//! Note: Current implementation only requires caller to provide CWldObject::m_meshObject/m_ReMesh.
	//! If m_ReMesh exists, only m_meshObject->GetWorldTransform/m_popoutBox is needed. This assumption
	//! is current as of 03/2009. If future implementation would require more data, developer must check
	//! all caller to make sure they are compatible.
	BOOL CalculateWorld(CWldObjectList* worldList, size_t maxLevels, size_t maxFacesPerBox, int resX, int resY, int resZ, bool genObjList, CReferenceObjList* reference, HWND hwndProgTotal, HWND hwndProgTask);

private:
	// Supporting functions for CalculateWorld()
	BOOL GenerateSubTable(REFBOX* curSubBox, const BoundBox& curBounds, const std::vector<HWND>& hwndProgressBars);

public:
	// Real-time Collision Functions

	// Return TRUE if segment intersects with collision and return closest intersection, normal, object ID, highest Y and "adjust vector" (for collision force calculation).
	BOOL CollisionSegmentIntersectAdvanced(
		const Vector3f& collisionPathStart,
		const Vector3f& collisionPathEnd,
		float tolerance,
		float normalYHeightFilter,
		float normalYHeightFilterDW,
		Vector3f* intersection,
		Vector3f* normal,
		int* indexOfObjectHit,
		float* highestYcontactPoint,
		Vector3f* adjustVector) const;

	// Return TRUE if segment intersects with collision. Find closest intersection, normal and object ID if requested.
	BOOL CollisionSegmentIntersect(
		const Vector3f& collisionPathStart,
		const Vector3f& collisionPathEnd,
		float tolerance,
		bool returnAfterFirstContact,
		bool filterCorona,
		Vector3f* intersection, // returns intersection point between collision path and the geometry
		Vector3f* normal, // returns normal vector at intersection point
		int* indexOfObjectHit // returns colliding object ID
	);

	// Return TRUE if segment intersects with any collision polygon
	BOOL CollisionSegmentIntersectBasic(
		const Vector3f& collisionPathStart,
		const Vector3f& collisionPathEnd,
		float tolerance,
		bool filterCorona = false);

	// Used by AI only
	BOOL CollisionSegmentSIValidPath(
		const Vector3f& collisionPathStart,
		const Vector3f& collisionPathEnd,
		int tolerance,
		float floorTolerance,
		float wallMinimumHeight,
		float currentYpos,
		Vector3f origin,
		BOOL* wallInWayRet,
		BOOL* floorExistRet,
		Vector3f* wallNormal);

	// Used by CShadowClass only
	BOOL CollisionPolyRetrieval(
		Vector3f polyLoc[3],
		Vector3f* intersection,
		Vector3f* latestNormal,
		int& numberPolys,
		CPolyRetStruct polyRet[20],
		Vector3f storedNormals[20]);

	void BuildObjectReferenceArray(CWldObjectList* wList);
	void ClearObjectReferenceArray() {
		m_wldObjectReferenceArray.clear();
	}

	REFBOX* FindLeafBoxAtPosition(const Vector3f& pos) const;
	bool CreateReMeshFromCollisionData(ReD3DX9DeviceState* dev, bool bUseTriStrip, std::vector<std::unique_ptr<ReMesh>>* paMeshes);
	void DumpTrianglesToLog() const;
	int ValidateBoxes(); // Returns 0 for valid boxes, 1 for boxes with unnecessary triangles, >1 for invalid collision.
	void RebuildBoxes();

private:
	// Supporting functions for real-time collision
	float GetPointToPlaneDistance(const Vector3f triangle[3], const Vector3f& ptloc);
	void GetLinePlaneIntersection(const Vector3f triangle[3], const Vector3f& pt1, const Vector3f& pt2, Vector3f& intersection);
	BOOL SegOverlap(const Vector3f& pt1, const Vector3f& pt2, const Vector3f& pt3, const Vector3f& pt4);
	BOOL TestTrianglesIntersection(const Vector3f triangle1[3], const Vector3f triangle2[3]);

	BoundBox GetSegmentBoundingBox(const Vector3f& segmentStart, const Vector3f& segmentEnd) const;
	BoundBox GetPolyBoundingBox(const Vector3f poly[3]);
	bool BoundingBoxToBoxCheck(const BoundBox& box1, const BoundBox& box2) const;
	REFBOX* ConvertPositionToSubBox(const Vector3f& pos, REFBOX* curBox, const Vector3f& curBoxMin, Vector3f& subBoxMin) const;

	const std::vector<int>& GetFaceIndexArray(unsigned cacheKey) const {
		static std::vector<int> emptyArray;
		if (cacheKey == REFBOX::INVALID_FACE_INDEX_CACHE_KEY) {
			jsAssert(false);
			return emptyArray;
		}

		auto itr = m_faceIndexArrayCache.find(cacheKey);
		if (itr == m_faceIndexArrayCache.end()) {
			jsAssert(false);
			return emptyArray;
		}

		return itr->second;
	}

	struct BoxVisitCallbackArg {
		REFBOX* m_pBox;
		Vector3f m_ptMin;
		Vector3f m_ptMax;
	};
	// Return false from callback to abort iteration. Function returns false if callback does.
	bool VisitBoxesInRange(const Vector3f& ptMin, const Vector3f& ptMax, std::function<bool(const BoxVisitCallbackArg&)> visitBoxCallback);
	bool VisitAllBoxes(std::function<bool(const BoxVisitCallbackArg&)> visitBoxCallback) {
		return VisitBoxesInRange(Vector3f(-FLT_MAX, -FLT_MAX, -FLT_MAX), Vector3f(FLT_MAX, FLT_MAX, FLT_MAX), visitBoxCallback);
	}

public:
	// Accessors
	bool IsSubBoxesValid() const {
		return m_boxCollection != NULL;
	}
	bool BaseBoxContains(const Vector3f& pos) const {
		return m_baseBox.contains(pos);
	}
	const BoundBox& GetBoundingBox() const {
		return m_baseBox;
	}

	size_t GetNumberOfLevels() const {
		return m_numberOfLevels;
	}
	size_t GetMaxFacesPerBox() const {
		return m_maxFacesPerBox;
	}

	bool GetResolution(size_t level, int& resX, int& resY, int& resZ) const {
		jsVerifyReturn(m_divResX != NULL && m_divResY != NULL && m_divResZ != NULL && level < m_numberOfLevels, false);
		resX = m_divResX[level];
		resY = m_divResY[level];
		resZ = m_divResZ[level];
		return true;
	}

	bool HasWorldObjectReferenceArray() const {
		return !m_wldObjectReferenceArray.empty();
	}

	size_t GetWorldObjectCount() const {
		return m_wldObjectReferenceArray.size();
	}

	CWldObject* GetWorldObjectReference(size_t index) {
		jsVerifyReturn(index < m_wldObjectReferenceArray.size(), NULL);
		return m_wldObjectReferenceArray[index];
	}

	std::vector<Vector3f> GetTriangles() const;
	size_t GetTriangleCount() const {
		return m_faceDataBank.size();
	}
	bool ContainsTriangle(const Vector3f& pt1, const Vector3f& pt2, const Vector3f& pt3, float fTolerance, size_t* piTriangleIndex);
	float ClosestTriangle(const Vector3f& pt0, const Vector3f& pt1, const Vector3f& pt2, Vector3f* pvErrorDirection, Vector3f (*aptsClosestTriangle)[3], size_t* pClosestTriangleIndex) const;

	CWldObjectList* m_wldObjList;

private:
	struct CFaceStruct {
		Vector3f m_vertices[3];
		Vector3f m_normal;
		BoundBox m_box;
		int m_objectIndexRef;
	};

	std::vector<CWldObject*> m_wldObjectReferenceArray;

	BoundBox m_baseBox;
	REFBOX*** m_boxCollection;
	std::vector<CFaceStruct> m_faceDataBank;
	size_t m_numberOfLevels;
	size_t m_maxFacesPerBox;
	float* m_boxSizeX;
	float* m_boxSizeY;
	float* m_boxSizeZ;
	int* m_divResX; // Space subdivision resolution along X axis
	int* m_divResY; // Space subdivision resolution along Y axis
	int* m_divResZ; // Space subdivision resolution along Z axis
	bool m_useObjectRef; // true if collision map contains object references (for EXG), false otherwise (for dynamic object).
	bool m_compressed; // true if collision data is compressed (schema v1 and later), false otherwise (schema v0).

	// Global cache for all face index sets. KEY: auto-increment integer value from 0, VALUE: vector of face indices
	std::map<unsigned, std::vector<int>> m_faceIndexArrayCache;
	unsigned m_nextFaceIndexCacheKey;
};

class CCollisionObjList : public CKEPObList {
public:
	CCollisionObjList(){};

	DECLARE_SERIAL(CCollisionObjList)

	CCollisionObj* GetByIndex(int index);
	void SafeDelete();
};

} // namespace KEP
