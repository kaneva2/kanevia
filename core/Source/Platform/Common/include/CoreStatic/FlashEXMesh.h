///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "FlashMesh.h"
#include "ReMaterial.h"
#include "ExternalEXMesh.h"

namespace KEP {
class TextureDatabase;
class IMemSizeGadget;
class CEXMeshObj;
class CStateManagementObj;
class ReMaterial;

class FlashEXMesh : public FlashMesh, public IExternalEXMesh {
public:
	static std::shared_ptr<FlashEXMesh> Create(
		IFlashSystem* pFlashSystem,
		CStateManagementObj* stateManager,
		TextureDatabase* textureDatabase,
		HWND parent,
		const MediaParams& mediaParams,
		uint32_t width = 0, uint32_t height = 0,
		bool loop = false, bool wantInput = false,
		int32_t x = 0, int32_t y = 0);

	CStateManagementObj* StateMgr() {
		return m_stateManager;
	}
	TextureDatabase* TextureDB() {
		return m_textureDatabase;
	}

	ReMaterialPtr GetReMaterial() {
		return FlashMesh::GetReMaterial();
	}

	void ScrapeTexture(ReD3DX9DeviceState* devState);

	int Render(LPDIRECT3DDEVICE9 g_pd3dDevice, CEXMeshObj* mesh, int vertexType);

private:
	FlashEXMesh(CStateManagementObj* stateManager, TextureDatabase* textureDatabase) :
			m_stateManager(stateManager),
			m_textureDatabase(textureDatabase) {
	}

	virtual ~FlashEXMesh() {}

	CStateManagementObj* m_stateManager;
	TextureDatabase* m_textureDatabase;

public: // FlashMesh::IExternalEXMesh Interface
	virtual bool SetMediaParams(const MediaParams& mediaParams, bool allowRewind = true) override {
		return FlashMesh::SetMediaParams(mediaParams, allowRewind);
	}

	virtual std::string ToStr(bool verbose = false) const override {
		return FlashMesh::StrThis(verbose);
	}

	virtual MediaParams& GetMediaParams() override {
		return FlashMesh::GetMediaParams();
	}

	virtual bool SetVolume(double volPct = -1) override {
		return FlashMesh::SetVolume(volPct);
	}
	virtual double GetVolume() const override {
		return FlashMesh::GetVolume();
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	//virtual void Delete() override {
	//	FlashMesh::Delete();
	//}
};

} // namespace KEP
