/******************************************************************************
 HousesPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_HOUSES_PROPERTIESCTRL_H_)
#define _HOUSES_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HousesPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

#define WM_KEP_PROPERTY_CHANGED WM_USER + 1

/////////////////////////////////////////////////////////////////////////////
// CHousesPropertiesCtrl window

class CHousesPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CHousesPropertiesCtrl(CWnd* pParent);
	virtual ~CHousesPropertiesCtrl();

public:
	enum { INDEX_ROOT = 1,
		INDEX_HOUSE_NAME,
		INDEX_SPAWN_HOUSE };

	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetHouseName() const {
		return Utf16ToUtf8(m_sHouseName).c_str();
	}
	bool GetSpawnHouseOnClose() const {
		return m_bSpawnHouseOnClose;
	}

	// modifiers
	void SetHouseName(CStringA sHouseName) {
		m_sHouseName = Utf8ToUtf16(sHouseName).c_str();
	}
	void SetSpawnHouseOnClose(bool bSpawnHouseOnClose) {
		m_bSpawnHouseOnClose = bSpawnHouseOnClose;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHousesPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CHousesPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CWnd* m_pParent;

	CStringW m_sHouseName;
	bool m_bSpawnHouseOnClose;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_HOUSES_PROPERTIESCTRL_H_)
