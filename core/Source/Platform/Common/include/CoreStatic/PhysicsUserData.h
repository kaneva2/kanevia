///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "KEPConstants.h"
#include "KEPPhysics/UserData.h"

namespace KEP {

class StreamableDynamicObject;

#if defined(_DEBUG)
class CDynamicPlacementObj;
#endif

struct PhysicsUserData : public Physics::UserData {
	virtual ~PhysicsUserData() {}
};

struct PhysicsCollisionUserData : public PhysicsUserData {
	PhysicsCollisionUserData(StreamableDynamicObject* pObject) :
			m_pObject(pObject) {
	}

	StreamableDynamicObject* m_pObject;
};

// Used as the UserData for physics rigid bodies.
struct PhysicsBodyData : public PhysicsUserData {
	PhysicsBodyData(ObjectType objectType = ObjectType::NONE, int iObjectId = -1, void* pObject = nullptr, size_t iRigidBodyIndex = 0) :
			m_ObjectType(objectType), m_iObjectId(iObjectId), m_pObject(pObject), m_iRigidBodyIndex(iRigidBodyIndex) {
	}

	ObjectType m_ObjectType;
	int m_iObjectId;
	void* m_pObject;
	size_t m_iRigidBodyIndex = 0; // For objects with multiple rigid bodies.
#if defined(_DEBUG)
	const char* m_pszName = nullptr;
	unsigned int m_glid = 0;
	CDynamicPlacementObj* m_pDynObj = nullptr;
#endif
};

namespace CollisionGroup {
const uint16_t None = 0x0;
const uint16_t Vehicle = 0x1; // btRaycastVehicle is hardcoded to use the "default" group (== 1) for its wheel tests.
const uint16_t Avatar = 0x2;
const uint16_t Object = 0x4; // For dyanamic objects and world objects.
const uint16_t OtherPlayerVehicle = 0x8; // Vehicles controlled by other client machines
const uint16_t VehicleDynamicObject = 0x10; // Original shape for vehicle.
const uint16_t Visible = 0x20; // For objects that can be picked or block camera view
const uint16_t SensorTrigger = 0x40; // For objects used to track or trigger on overlapping objects
const uint16_t All = 0xffff;
} // namespace CollisionGroup

namespace CollisionMask {
const uint16_t Vehicle = CollisionGroup::Object;
const uint16_t Avatar = CollisionGroup::Object | CollisionGroup::VehicleDynamicObject;
const uint16_t Object = CollisionGroup::Avatar | CollisionGroup::Vehicle;
const uint16_t OtherPlayerVehicle = 0xffff;
const uint16_t VehicleDynamicObject = 0xffff;
const uint16_t All = 0xffff;
} // namespace CollisionMask

} // namespace KEP