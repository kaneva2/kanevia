///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CFrameObj.h"
#include "KEPConstants.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class IMemSizeGadget;

class CMovementCaps : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CMovementCaps, VERSIONABLE_SCHEMA | 3);

public:
	CMovementCaps();

	void Serialize(CArchive& ar);

	BOOL Clone(CMovementCaps** clone);

	void Copy(CMovementCaps* pMC_new);

	void SetUseStatesToFalse();

	void SetAccel(float acc) {
		m_accellerate = acc;
	}

	void SetDecel(float dec) {
		m_decellerate = dec;
	}

	bool DoJump();

	void SetJumping(bool v) {
		m_isJumping = v;
	}
	bool IsJumping() const {
		return m_isJumping;
	}

	void DoMoveForward();
	void DoMoveBackward();
	void DoMoveDecel();

	void SetMoving(bool v) {
		m_isMoving = v;
	}
	bool IsMoving() const {
		return m_isMoving;
	}

	void DoTurnLeft(bool bFullSpeed);
	void DoTurnRight(bool bFullSpeed);
	void DoTurnDecel(bool bStopNow);

	void SetTurning(bool v) {
		m_isTurning = v;
	}
	bool IsTurning() const {
		return m_isTurning;
	}

	void DoSideStepRight();
	void DoSideStepLeft();
	void DoSideStepDecel();

	void SetSideStepping(bool v) {
		m_isSideStepping = v;
	}
	bool IsSideStepping() const {
		return m_isSideStepping;
	}

	bool IsAutoRun() const {
		return (m_bAutoRun != FALSE);
	}
	void SetAutoRun(BOOL v) {
		m_bAutoRun = v;
	}
	void ToggleAutoRun() {
		m_bAutoRun = m_bAutoRun ? FALSE : TRUE;
	}

	bool IsWalking() const {
		return m_bIsWalking;
	}
	void SetWalking(bool v) {
		m_bIsWalking = v;
	}
	void ToggleWalking() {
		m_bIsWalking = !m_bIsWalking;
	}

	bool IsCollisionEnabled() const {
		return m_collisionEnabled;
	}

	void SetGravityEnabled(BOOL v) {
		m_doGravity = v;
	}
	bool IsGravityEnabled() const {
		return (m_doGravity != FALSE);
	}

	void SetSurfaceProp(ObjectType objType) {
		m_currentSurfaceProp = objType;
	}
	ObjectType GetSurfaceProp() const {
		return m_currentSurfaceProp;
	}

	void SetPosLast(const Vector3f& pos) {
		m_lastPosition = pos;
	}
	Vector3f GetPosLast() const {
		return m_lastPosition;
	}

	void SetPosVel(const Vector3f& vel) {
		m_curVelocityVect = vel;
	}
	Vector3f GetPosVel() const {
		return m_curVelocityVect;
	}

	void SetRot(const Vector3f& rot) {
		m_curRotationVect = rot;
	}
	Vector3f GetRot() const {
		return m_curRotationVect;
	}

	void ClearPosForce() {
		m_curTransX = 0.0f;
		m_curTransY = 0.0f;
		m_curTransZ = 0.0f;
	}
	void SetPosForce(const Vector3f& pos) {
		m_curTransX = pos.x;
		m_curTransY = pos.y;
		m_curTransZ = pos.z;
	}
	Vector3f GetPosForce() const {
		return Vector3f(m_curTransX, m_curTransY, m_curTransZ);
	}

	void SetRotForce(const Vector3f& rot) {
		m_curRotX = rot.x;
		m_curRotY = rot.y;
		m_curRotZ = rot.z;
	}
	Vector3f GetRotForce() const {
		return Vector3f(m_curRotX, m_curRotY, m_curRotZ);
	}

	void SetFov(float targetFov) {
		m_targetFov = targetFov;
	}
	float GetFov() const {
		return m_targetFov;
	}

	void SetFovLast(float targetFov) {
		m_lastFov = targetFov;
	}
	float GetFovLast() const {
		return m_lastFov;
	}

	float GetPosDrag() const {
		return m_crossSectionalArea;
	}

	float GetPosGroundDrag() const {
		return m_groundDrag;
	}

	float GetRotDrag() const {
		return m_rotationDragConstant;
	}

	float GetRotGroundDrag() const {
		return m_rotationalGroundDrag;
	}

	void SetJumpForce(float f) {
		m_burstForceY = f;
	}
	float GetJumpForce() const {
		return m_burstForceY;
	}

	float GetPosForceAirGain() const {
		return m_airControl;
	}

	void SetMinSpeed(float speed) {
		m_minSpeed = speed;
	}

	void SetMaxSpeed(float speed) {
		m_maxSpeed = speed;
	}
	float GetMaxSpeed() const {
		return m_maxSpeed;
	}

	float GetWalkMaxSpeed() const {
		return m_walkMaxSpeed;
	}

	float GetMaxYaw() const {
		return m_maxYaw;
	}

	float GetAntiGravity() const {
		return m_antiGravity;
	}

	float GetFovChangeSensitivity() const {
		return m_capFovChangeSensitivity;
	}

	bool IsStandVerticalToSurfaceEnabled() const {
		return (m_standVerticalToSurface != FALSE);
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET

private:
	float m_capabilityFovMax;
	float m_capabilityFovMin;
	float m_capFovChangeSensitivity;
	float m_targetFov;
	float m_lastFov;
	int m_fovCurrentInterp;

	Vector3f m_curRotationVect;
	Vector3f m_curVelocityVect;
	Vector3f m_lastPosition;
	float m_groundDrag;

	float m_strafeReduction;
	float m_crossSectionalArea;
	float m_airControl;

	BOOL m_autoDecel; // affects DecelSideStep
	BOOL m_autoDecelY; // does nothing
	BOOL m_autoDecelZ; // affects DecelSpeed, which affects forward/backward movement
	BOOL m_invertMouse;

	bool m_isJumping;
	float m_jumpPower;
	TimeMs m_jumpRecoverTime; // does nothing - only here for old get set
	Timer m_jumpTimer;

	BOOL m_doGravity;
	float m_gravity;
	bool m_collisionEnabled;
	float m_curSpeed;
	float m_maxSpeed;
	float m_minSpeed;
	float m_walkMaxSpeed;
	float m_walkMinSpeed;
	bool m_bIsWalking;
	float m_accellerate;
	float m_decellerate;
	float m_yawAccel;
	float m_yawDecel;
	float m_curRotX;
	float m_curRotY;
	float m_curRotZ;
	float m_curTransX;
	float m_curTransY;
	float m_curTransZ;
	float m_minYaw;
	float m_maxYaw;

	float m_burstForceY;

	float m_rotationDragConstant;
	float m_rotationalGroundDrag;

	float m_antiGravity;

	bool m_isTurning;
	bool m_isMoving;
	bool m_isSideStepping;

	BOOL m_standVerticalToSurface;

	ObjectType m_currentSurfaceProp;

	BOOL m_bAutoRun;

	// DEPRECATED ---------------------------------
	float m_mass;

	float m_freeLookSpeed;
	float m_freeLookCurPitch;
	float m_freeLookCurMaxPitch;
	float m_freeLookCurYaw;
	float m_freeLookCurMaxYaw;
	float m_freeLookPitchForce;
	float m_freeLookYawForce;

	float m_minPitch;
	float m_maxPitch;
	float m_pitchAccel;
	float m_pitchDecel;

	float m_rollFactor;
	float m_maxRoll;
	float m_rollAccel;
	float m_rollDecel;
};

} // namespace KEP