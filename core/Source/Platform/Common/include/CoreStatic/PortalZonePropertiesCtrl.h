/******************************************************************************
 PortalZonePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_PORTAL_ZONEPROPERTIESCTRL_H_)
#define _PORTAL_ZONEPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PortalZonePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "CEXMeshClass.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CPortalZonePropertiesCtrl window

class CPortalZonePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CPortalZonePropertiesCtrl();
	virtual ~CPortalZonePropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetPortalZoneName() const {
		return Utf16ToUtf8(m_portalZoneName).c_str();
	}
	int GetWorldChannelID() const {
		return m_worldChannelID;
	}
	int GetRequiredKeyId() const {
		return m_requiredKeyId;
	}

	// modifiers
	void SetPortalZoneName(CStringA portalZoneName) {
		m_portalZoneName = Utf8ToUtf16(portalZoneName).c_str();
	}
	void SetWorldChannelID(int worldChannelID) {
		m_worldChannelID = worldChannelID;
	}
	void SetRequiredKeyId(int requiredKeyId) {
		m_requiredKeyId = requiredKeyId;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPortalZonePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CPortalZonePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitChannels();

private:
	enum { INDEX_ROOT = 1,
		INDEX_PORTAL_ZONE_NAME,
		INDEX_PORTAL_WORLD_CHANNEL_ID,
		INDEX_REQUIRED_KEY_ID };

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	CStringW m_portalZoneName;
	int m_worldChannelID;
	int m_requiredKeyId;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_PORTAL_ZONEPROPERTIESCTRL_H_)
