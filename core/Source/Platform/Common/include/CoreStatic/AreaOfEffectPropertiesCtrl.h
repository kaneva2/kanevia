/******************************************************************************
 AreaOfEffectPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_AREA_OF_EFFECTPROPERTIESCTRL_H_)
#define _AREA_OF_EFFECTPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AreaOfEffectPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "CEXMeshClass.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CAreaOfEffectPropertiesCtrl window

class CAreaOfEffectPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CAreaOfEffectPropertiesCtrl();
	virtual ~CAreaOfEffectPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetXMin() const {
		return m_xMin;
	}
	float GetXMax() const {
		return m_xMax;
	}
	float GetYMin() const {
		return m_yMin;
	}
	float GetYMax() const {
		return m_yMax;
	}
	float GetZMin() const {
		return m_zMin;
	}
	float GetZMax() const {
		return m_zMax;
	}

	// modifiers
	void SetXMin(float xMin) {
		m_xMin = xMin;
	}
	void SetXMax(float xMax) {
		m_xMax = xMax;
	}
	void SetYMin(float yMin) {
		m_yMin = yMin;
	}
	void SetYMax(float yMax) {
		m_yMax = yMax;
	}
	void SetZMin(float zMin) {
		m_zMin = zMin;
	}
	void SetZMax(float zMax) {
		m_zMax = zMax;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAreaOfEffectPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CAreaOfEffectPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_X_MIN,
		INDEX_X_MAX,
		INDEX_Y_MIN,
		INDEX_Y_MAX,
		INDEX_Z_MIN,
		INDEX_Z_MAX,
		INDEX_LOAD_BOX
	};

	const static int X_MIN_MIN = 0;
	const static int X_MIN_MAX = 0;
	const static int X_MAX_MIN = 0;
	const static int X_MAX_MAX = 0;
	const static int Y_MIN_MIN = 0;
	const static int Y_MIN_MAX = 0;
	const static int Y_MAX_MIN = 0;
	const static int Y_MAX_MAX = 0;
	const static int Z_MIN_MIN = 0;
	const static int Z_MIN_MAX = 0;
	const static int Z_MAX_MIN = 0;
	const static int Z_MAX_MAX = 0;

private:
	float m_xMin;
	float m_xMax;
	float m_yMin;
	float m_yMax;
	float m_zMin;
	float m_zMax;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_AREA_OF_EFFECTPROPERTIESCTRL_H_)
