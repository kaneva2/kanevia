///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Glids.h"

#include "Core/Util/HighPrecisionTime.h"

#include "common\include\CoreStatic\CBoneAnimationNaming.h"
#include "common\include\CoreStatic\CBoneAnimationClass.h"

#include "ResourceManagers\AnimationProxy.h"

namespace KEP {

class RuntimeSkeleton;
class IMemSizeGadget;

class CSkeletonAnimHeader {
public:
	CSkeletonAnimHeader(RuntimeSkeleton* pRS);

	CSkeletonAnimHeader(const CSkeletonAnimHeader& rhs);

	~CSkeletonAnimHeader();

	void Write(CArchive& ar);
	void Read(CArchive& ar);

	void SetAnimationProxy(AnimationProxy* proxy) {
		m_pAnimProxy = proxy;
	}

	CBoneAnimationObject* GetBoneAnimationObj(UINT boneIndex);

	UINT GetNumBones() const;

	bool HasLoadError() const;

	// return true if animationproxy is downloaded and opened/loaded
	bool IsDataReady() const;

	// Trigger data download/read, return true if already available
	bool LoadData();

	float GetAnimSpeedFactor() const {
		return m_animSpeedFactor;
	}

	bool SetAnimSpeedFactor(float factor) {
		m_animSpeedFactor = factor;
		UpdateAnimMinTimeMs();
		return true;
	}

	TimeMs GetAnimCropStartTimeMs() const {
		return m_animCropStartTimeMs;
	}

	TimeMs GetAnimCropEndTimeMs() const {
		return m_animCropEndTimeMs;
	}

	bool SetAnimCropTimes(TimeMs cropStartTimeMs, TimeMs cropEndTimeMs) {
		m_animCropStartTimeMs = cropStartTimeMs;
		m_animCropEndTimeMs = cropEndTimeMs;
		UpdateAnimMinTimeMs();
		return true;
	}

	BOOL GetAnimLooping() const {
		return (m_animLooping != 0);
	}

	bool SetAnimLooping(BOOL looping) {
		m_animLooping = looping;
		UpdateAnimMinTimeMs();
		return true;
	}

	TimeMs GetAnimMinTimeMs() const {
		return m_animMinTimeMs;
	}

	TimeMs GetAnimDurationMs() const;

	bool UpdateAnimMinTimeMs();

	// Returns time offset that will create a smoother transition from standing
	TimeMs GetAnimStartTimeMs(bool /*animDirForward*/) const {
		switch (m_animType) {
			case eAnimType::Run: // animTimeMs = 800.0
				return 145.0; //animDirForward ? 130.0 : 160.0;
			case eAnimType::Walk: // animTimeMs = 1280.0
				return 235.0; //animDirForward ? 220.0 : 250.0;
		}
		return 0.0;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

public:
	std::string m_name;

	GLID m_animGlid;

	eAnimType m_animType;

	UINT64 m_hashCode;

	UINT m_animLooping;

	int m_animVer;

	TimeMs m_animMinTimeMs;
	TimeMs m_animDurationMs;
	TimeMs m_animFrameTimeMs;

private:
	void GetMemSizeBoneIndexMap(IMemSizeGadget* pMemSizeGadget) const;

	const static UINT m_schemaVersion = 1;

	void Read_v0(CArchive& ar);
	void Read_v1(CArchive& ar);

	UINT* m_boneIndexMap; // for remapping bone indexes by name -- AnimationProxy now carriess bone animation data in a different order than defined in RuntimeSkeleton
	RuntimeSkeleton* m_pRS; // link to parent skeleton
	AnimationProxy* m_pAnimProxy;

	// runtime animation settings (to allow user to customize animation)
	float m_animSpeedFactor;
	TimeMs m_animCropStartTimeMs;
	TimeMs m_animCropEndTimeMs;
	bool m_updated;
};

} // namespace KEP
