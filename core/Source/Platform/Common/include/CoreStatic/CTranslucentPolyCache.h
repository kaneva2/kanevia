///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CStructuresMisl.h"
#include "EffectRenderData.h"

namespace KEP {

class CEXMeshObj;
class CMaterialObject;
class CReferenceObj;
class CRTLightObjList;
class CStateManagementObj;

class CTranslucentPolyObj : public CObject {
public:
	CTranslucentPolyObj() :
			m_customMaterial(0){};

	float m_zValue;
	Matrix44f m_location;
	CEXMeshObj* m_meshReference;
	D3DMATERIAL9 m_materialRef;
	int m_blendRef;
	CReferenceObj* m_referenceOption;
	CMaterialObject* m_customMaterial; //use this instead of m_meshReference->m_materialObject when it is set

	//	Render data an effect needs. World xform, material, etc.
	EffectRenderData mRenderData;

	void Render(
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		BOOL& mirrorEnable,
		Matrix44f& mirrorMatrix,
		CStateManagementObj* stateManager,
		TextureDatabase* textureDataBase,
		Vector3f& camLocation,
		Matrix44f& matrixLoc,
		float camFov,
		CTranslucentPolyObj** lastObjRendered,
		BOOL& alphaMode);

	float MagnitudeSquared(
		float& Mx1, float& My1,
		float& Mz1, float& Mx2,
		float& My2, float& Mz2);

	//returns the custom material when it present, otherwise use the material object in the mesh as usual
	CMaterialObject* GetMaterialObject();
};

class CTranslucentPolyList : public CObList {
public:
	CTranslucentPolyList();

	CTranslucentPolyObj* m_lastObjRendered; //last translucentpolyobject rendered
	BOOL m_alphaMode;

	int m_currentObjectCount;
	POSITION m_lastPosition;
	float m_lastValue;

	void SafeDelete();

	void CacheMesh(
		CEXMeshObj* mesh,
		const Matrix44f& viewMatrix,
		const Matrix44f& wldMatrix,
		const Vector3f& meshCenter,
		CReferenceObj* refOpt,
		CMaterialObject* custMaterial = NULL);

	void CacheMeshNoSort(
		CEXMeshObj* mesh,
		const Matrix44f& viewMatrix,
		const Matrix44f& wldMatrix,
		const Vector3f& meshCenter,
		CReferenceObj* refOpt,
		CMaterialObject* custMaterial);

	void InsertPolygonOrMeshRefNonTrans(
		ABVERTEX* vertex,
		const Matrix44f& viewMatrix,
		const Matrix44f& wldMatrix,
		CEXMeshObj* meshRef,
		const Vector3f& meshCenter,
		CReferenceObj* refOpt,
		CMaterialObject* custMaterial);

	void InsertPolygonOrMeshRef(
		ABVERTEX* vertex,
		const Matrix44f& viewMatrix,
		const Matrix44f& wldMatrix,
		CEXMeshObj* meshRef,
		const Vector3f& meshCenter,
		CReferenceObj* refOpt,
		CMaterialObject* custMaterial);

	void Render(
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		BOOL& mirrorEnable,
		Matrix44f& mirrorMatrix,
		CStateManagementObj* stateManager,
		TextureDatabase* textureDataBase,
		BOOL frontToBack,
		Vector3f& camLocation,
		Matrix44f& matrixLoc,
		float camFov);

	void Flush();
};

} // namespace KEP