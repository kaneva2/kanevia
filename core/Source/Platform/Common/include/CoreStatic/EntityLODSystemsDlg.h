///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENTITYLODSYSTEMSDLG_H__B18C12A1_60D3_11D4_B1ED_0000B4BD56DD__INCLUDED_)
#define AFX_ENTITYLODSYSTEMSDLG_H__B18C12A1_60D3_11D4_B1ED_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EntityLODSystemsDlg.h : header file
//

#include "CMovementObj.h"
#include "ctextureDBClass.h"

/////////////////////////////////////////////////////////////////////////////
// CEntityLODSystemsDlg dialog

class CEntityLODSystemsDlg : public CDialog {
	// Construction
public:
	CEntityLODSystemsDlg(CWnd* pParent = NULL); // standard constructor
	CMovementObj* entityReference;
	IDirectSound8* m_dirSndRef;
	TextureDatabase* GL_textureDataBase;
	bool m_delOnly;
	void delOnlyMode();
	// Dialog Data
	//{{AFX_DATA(CEntityLODSystemsDlg)
	enum { IDD = IDD_DIALOG_ENTITYLODSYS };
	CListBox m_listLODSystems;
	CButton m_addButton;
	CButton m_replaceButton;
	CButton m_editButton;
	CStatic m_static1;
	CEdit m_distanceText;
	float m_distance;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEntityLODSystemsDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListLODs(int sel);
	// Generated message map functions
	//{{AFX_MSG(CEntityLODSystemsDlg)
	afx_msg void OnButtonAddLodsys();
	afx_msg void OnBUTTONDeleteSystem();
	afx_msg void OnBUTTONReplaceValues();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListLodsystems();
	afx_msg void OnButtonEditLodsys();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENTITYLODSYSTEMSDLG_H__B18C12A1_60D3_11D4_B1ED_0000B4BD56DD__INCLUDED_)
