///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SKILLSYSTEMDLG_H__CDD3601C_F24B_41CD_BC0C_269890E30ABB__INCLUDED_)
#define AFX_SKILLSYSTEMDLG_H__CDD3601C_F24B_41CD_BC0C_269890E30ABB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SkillSystemDlg.h : header file
//
#include "CSkillClass.h"
#include "CMovementObj.h"
#include "CStatClass.h"
#include "CGlobalInventoryClass.h"
#include "afxwin.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "SkillsPropertiesCtrl.h"
#include "SkillsTitlesCtrl.h"
#include "SkillActionsCtrl.h"
#include "LevelRewardsCtrl.h"
#include "RacialAdvantagePropertiesCtrl.h"
#include "SkillLevelsPropertiesCtrl.h"
#include "AbilitiesPropertiesCtrl.h"
#include "RequiredStatsPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CSkillSystemDlg dialog

class CSkillSystemDlg : public CKEPEditDialog {
	// Construction
public:
	CSkillSystemDlg(CSkillObjectList*& skillDatabase,
		CWnd* pParent = NULL); // standard constructor

	CSkillObjectList*& m_skillDatabaseRef;
	CMovementObjList* m_edbDatabaseRef;
	CStatObjList* m_statDatabaseRef;
	CGlobalInventoryObjList* m_globalInventoryDBREF;
	// Dialog Data
	//{{AFX_DATA(CSkillSystemDlg)
	enum { IDD = IDD_DIALOG_SKILLSYSTEMDLG };
	CComboBox m_globalInventory;
	CComboBox m_basicAbilityFunction;
	CButton m_basicAbilitySkillEnable;
	CListBox m_abilitiesList;
	CListBox m_abilityRequiredItemsList;
	CListBox m_abilityResultItemsList;
	CListBox m_racialAdvantageList;
	CListBox m_abilityRequiredStatsList;
	CListBox m_skillLevelsList;
	CListBox m_skillsList;
	CListBox m_skillsTitlesList;
	CListBox m_skillActionsList;
	CListBox m_levelRewardsList;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkillSystemDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListSkills(int sel);
	void InListRacialAdvantages(int sel);
	void InListLevels(int sel);
	void InListAbilities(int sel);
	void InListStatReq(int sel);
	void InListRequiredItems(int sel);
	void InListResultItems(int sel);
	void InListSkillTitles(int sel);
	void InListSkillActions(int sel);
	void InListLevelRewards(int sel);

	void SetSkill(int skillIndex);
	void SetRacialAdvantage(int racialAdvantageIndex);
	void SetSkillLevel(int skillLevelIndex);
	void SetAbility(int abilityIndex);
	void SetRequiredStat(int requiredStatIndex);
	void SetSkillTitle(int skillTitleIndex);
	void SetSkillAction(int skillActionIndex);
	void SetLevelReward(int levelRewardIndex);

	// Generated message map functions
	//{{AFX_MSG(CSkillSystemDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeLISTSkillTitlesDB();
	afx_msg void OnBUTTONAddSkillTitle();
	afx_msg void OnBUTTONDeleteSkillTitle();
	afx_msg void OnBUTTONReplaceSkillTitle();

	afx_msg void OnSelchangeLISTSkillActionsDB();
	afx_msg void OnBUTTONAddSkillAction();
	afx_msg void OnBUTTONDeleteSkillAction();
	afx_msg void OnBUTTONReplaceSkillAction();

	afx_msg void OnSelchangeLISTLevelRewardsDB();
	afx_msg void OnBUTTONAddLevelReward();
	afx_msg void OnBUTTONDeleteLevelReward();
	afx_msg void OnBUTTONReplaceLevelReward();

	afx_msg void OnSelchangeLISTSkillsDB();
	afx_msg void OnBUTTONAddSkill();
	afx_msg void OnBUTTONDeleteSkill();
	afx_msg void OnBUTTONReplaceSkill();
	afx_msg void OnBUTTONAddRacialAdv();
	afx_msg void OnBUTTONDeleteRacialAdv();
	afx_msg void OnSelchangeLISTRacialAdvDB();
	afx_msg void OnBUTTONAddLevel();
	afx_msg void OnBUTTONReplaceLevel();
	afx_msg void OnBUTTONDeleteLevel();
	afx_msg void OnSelchangeLISTSkillLevelsDB();
	afx_msg void OnBUTTONADDAbility();
	afx_msg void OnBUTTONDeleteAbility();
	afx_msg void OnBUTTONReplaceAbility();
	afx_msg void OnSelchangeLISTAbilityDB();
	afx_msg void OnBUTTONAddStatReq();
	afx_msg void OnBUTTONDeleteStatReq();
	afx_msg void OnBUTTONAddRequiredItem();
	afx_msg void OnBUTTONAddResultItem();
	afx_msg void OnBUTTONDeleteRequiredItem();
	afx_msg void OnBUTTONDeleteResultItem();
	afx_msg void OnBUTTONLoadSys();
	afx_msg void OnBUTTONSaveSkillSys();
	afx_msg void OnSelchangeLISTrequiredStats();
	afx_msg void OnBUTTONReplaceStatReq();
	afx_msg void OnBUTTONInsertLevel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceSkillLevelReward(int rewardSelection);
	void ReplaceSkillAction(int actionSelection);
	void ReplaceSkillTitleSystem(int titleSelection);
	void ReplaceSkillSystem(int skillSystemSelection);
	void ReplaceSkillLevel(int skillSystemSelection, int skillLevelSelection);
	void ReplaceAbility(int skillSystemSelection, int skillLevelSelection, int abilitySelection);
	void ReplaceReqStat(int skillSystemSelection, int skillLevelSelection, int abilitySelection, int reqStatSelection);

public:
	CButton m_enableInternalTimer;

private:
	CSkillObject* m_pCurSkill;
	CRacialAdvantageObject* m_pCurRacialAdvantage;
	CLevelRangeObject* m_pCurSkillLevel;
	CSkillAbilityObject* m_pCurAbility;
	CTitleObject* m_pCurSkillTitle;
	CSkillActionObject* m_pCurSkillAction;
	CLevelRewardObject* m_pCurLevelReward;

	CCollapsibleGroup m_colSkillTitleSystem;
	CButton m_grpSkillTitleSystem;
	CSkillsTitlesCtrl m_propSkillTitles;

	CCollapsibleGroup m_colSkillSystem;
	CButton m_grpSkillSystem;
	CSkillsPropertiesCtrl m_propSkills;

	CCollapsibleGroup m_colSkillActions;
	CButton m_grpSkillActions;
	CSkillActionsCtrl m_propSkillActions;

	CCollapsibleGroup m_colLevelRewards;
	CButton m_grpLevelRewards;
	CLevelRewardsCtrl m_propLevelRewards;

	CCollapsibleGroup m_colRacialAdvantage;
	CButton m_grpRacialAdvantage;
	CRacialAdvantagePropertiesCtrl m_propRacialAdvantage;

	CCollapsibleGroup m_colSkillLevels;
	CButton m_grpSkillLevels;
	CSkillLevelsPropertiesCtrl m_propSkillLevels;

	CCollapsibleGroup m_colAbilities;
	CButton m_grpAbilities;
	CAbilitiesPropertiesCtrl m_propAbilities;

	CCollapsibleGroup m_colRequiredStats;
	CButton m_grpRequiredStats;
	CRequiredStatsPropertiesCtrl m_propRequiredStats;

	CCollapsibleGroup m_colGenerationAbilities;
	CButton m_grpGenerationAbilities;

	CKEPImageButton m_btnAddSkill;
	CKEPImageButton m_btnDeleteSkill;
	CKEPImageButton m_btnReplaceSkill;

	CKEPImageButton m_btnAddSkillTitle;
	CKEPImageButton m_btnDeleteSkillTitle;
	CKEPImageButton m_btnReplaceSkillTitle;

	CKEPImageButton m_btnAddSkillAction;
	CKEPImageButton m_btnDeleteSkillAction;
	CKEPImageButton m_btnReplaceSkillAction;

	CKEPImageButton m_btnAddLevelReward;
	CKEPImageButton m_btnDeleteLevelReward;
	CKEPImageButton m_btnReplaceLevelReward;

	CKEPImageButton m_btnAddRacialAdvantage;
	CKEPImageButton m_btnDeleteRacialAdvantage;

	CKEPImageButton m_btnAddLevel;
	CKEPImageButton m_btnDeleteLevel;
	CKEPImageButton m_btnReplaceLevel;
	CKEPImageButton m_btnInsertLevel;

	CKEPImageButton m_btnAddAbility;
	CKEPImageButton m_btnDeleteAbility;
	CKEPImageButton m_btnReplaceAbility;

	CKEPImageButton m_btnAddStats;
	CKEPImageButton m_btnDeleteStats;
	CKEPImageButton m_btnReplaceStats;

	CKEPImageButton m_btnAddRequiredAbility;
	CKEPImageButton m_btnDeleteRequiredAbility;

	CKEPImageButton m_btnAddResultAbility;
	CKEPImageButton m_btnDeleteResultAbility;

	//last selected index in the list box
	int m_previousSelSkillTitle;
	int m_previousSelSkillSystem;
	int m_previousSelSkillLevel;
	int m_previousSelAbility;
	int m_previousSelReqStat;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKILLSYSTEMDLG_H__CDD3601C_F24B_41CD_BC0C_269890E30ABB__INCLUDED_)
