///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOUNDSETTINGSDLG_H__E4E5B901_34BD_11D3_AFDD_A9BC27470325__INCLUDED_)
#define AFX_SOUNDSETTINGSDLG_H__E4E5B901_34BD_11D3_AFDD_A9BC27470325__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SoundSettingsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSoundSettingsDlg dialog

class CSoundSettingsDlg : public CDialog {
	// Construction
public:
	CSoundSettingsDlg(CWnd* pParent = NULL); // standard constructor
	BOOL enabled;
	// Dialog Data
	//{{AFX_DATA(CSoundSettingsDlg)
	enum { IDD = IDD_DIALOG_SOUND };
	CComboBox m_soundList;
	CButton m_checkEnabled;
	float m_distanceFactor;
	//}}AFX_DATA

	int GetID() const {
		return m_id;
	}
	CStringA GetFilename() const {
		return m_filename;
	}

	void SetID(int id) {
		m_id = id;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSoundSettingsDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListSoundFiles(int sel);

	int m_id;
	CStringA m_filename;

	// Generated message map functions
	//{{AFX_MSG(CSoundSettingsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckEnabled();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOUNDSETTINGSDLG_H__E4E5B901_34BD_11D3_AFDD_A9BC27470325__INCLUDED_)
