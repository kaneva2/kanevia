/******************************************************************************
 SpawnPointCfgPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_DYNAMIC_OBJECT_VISUAL_PROPERTIESCTRL_H_)
#define _DYNAMIC_OBJECT_VISUAL_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpawnPositionCfgPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

class CReferenceObjList;

/////////////////////////////////////////////////////////////////////////////
// CDynamicObjectVisualPropertiesCtrl window

class CDynamicObjectVisualPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CDynamicObjectVisualPropertiesCtrl();
	virtual ~CDynamicObjectVisualPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetPositionX() const {
		return m_positionX;
	}
	float GetPositionY() const {
		return m_positionY;
	}
	float GetPositionZ() const {
		return m_positionZ;
	}

	float GetOrientationX() const {
		return m_orientationX;
	}
	float GetOrientationY() const {
		return m_orientationY;
	}
	float GetOrientationZ() const {
		return m_orientationZ;
	}

	CStringA GetVisualName() const {
		return Utf16ToUtf8(m_sVisualName).c_str();
	}
	bool GetCustomizable() const {
		return m_customizable;
	}

	bool MediaSupported() const {
		return m_mediaSupported;
	}
	void SetMediaSupported(bool b) {
		m_mediaSupported = b;
	}

	float GetPopOutMinX() const {
		return m_popout_min_x;
	}
	float GetPopOutMinY() const {
		return m_popout_min_y;
	}
	float GetPopOutMinZ() const {
		return m_popout_min_z;
	}
	float GetPopOutMaxX() const {
		return m_popout_max_x;
	}
	float GetPopOutMaxY() const {
		return m_popout_max_y;
	}
	float GetPopOutMaxZ() const {
		return m_popout_max_z;
	}

	// modifiers
	void SetPositionX(float positionX);
	void SetPositionY(float positionY);
	void SetPositionZ(float positionZ);

	void SetOrientationX(float orientationX) {
		m_orientationX = orientationX;
	}
	void SetOrientationY(float orientationY) {
		m_orientationY = orientationY;
	}
	void SetOrientationZ(float orientationZ) {
		m_orientationZ = orientationZ;
	}

	void SetPopOutMinX(float valF);
	void SetPopOutMinY(float valF);
	void SetPopOutMinZ(float valF);
	void SetPopOutMaxX(float valF);
	void SetPopOutMaxY(float valF);
	void SetPopOutMaxZ(float valF);

	void SetVisualName(CStringA name) {
		m_sVisualName = Utf8ToUtf16(name).c_str();
	}
	void SetCustomizable(bool b) {
		m_customizable = b;
	}

	void InitRefObjects();

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynamicObjectVisualPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CDynamicObjectVisualPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_VISUAL_NAME,
		INDEX_POSITION,
		INDEX_POSITION_X,
		INDEX_POSITION_Y,
		INDEX_POSITION_Z,
		INDEX_GET_CURRENT_POSITION,
		INDEX_ORIENTATION,
		INDEX_ORIENTATION_X,
		INDEX_ORIENTATION_Y,
		INDEX_ORIENTATION_Z,
		INDEX_CUSTOMIZABLE,
		INDEX_CANPLAYMOVIE,
		INDEX_POPOUT_MIN,
		INDEX_POPOUT_MIN_X,
		INDEX_POPOUT_MIN_Y,
		INDEX_POPOUT_MIN_Z,
		INDEX_POPOUT_MAX,
		INDEX_POPOUT_MAX_X,
		INDEX_POPOUT_MAX_Y,
		INDEX_POPOUT_MAX_Z
	};

	void GetCurrentPosition();

private:
	float m_positionX;
	float m_positionY;
	float m_positionZ;
	float m_orientationX;
	float m_orientationY;
	float m_orientationZ;
	CStringW m_sVisualName;
	bool m_customizable;
	bool m_mediaSupported;
	float m_popout_min_x;
	float m_popout_min_y;
	float m_popout_min_z;
	float m_popout_max_x;
	float m_popout_max_y;
	float m_popout_max_z;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_DYNAMIC_OBJECT_VISUAL_PROPERTIESCTRL_H_)
