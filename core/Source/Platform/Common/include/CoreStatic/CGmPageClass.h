///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class CGmPageObj : public CObject {
public:
	CGmPageObj();

	PLAYER_HANDLE m_handle;
};

class CGmPageList : public CObList {
public:
	CGmPageList(){};
	void SafeDelete();
	void RemovePage(PLAYER_HANDLE handle);
	void AddPage(PLAYER_HANDLE handle);
};

} // namespace KEP