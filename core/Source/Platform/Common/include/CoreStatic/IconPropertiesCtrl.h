/******************************************************************************
 IconPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ICONPROPERTIESCTRL_H_)
#define _ICONPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IconPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CIconPropertiesCtrl window

class CIconPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CIconPropertiesCtrl();
	virtual ~CIconPropertiesCtrl();

public:
	enum { PROP_INDEX_ROOT = 1,
		PROP_INDEX_NAME,
		PROP_INDEX_SIZE,
		PROP_INDEX_X_INDEX,
		PROP_INDEX_Y_INDEX };

	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetName() const {
		return Utf16ToUtf8(m_sName).c_str();
	}
	float GetSize() const {
		return m_size;
	}
	int GetXIndex() const {
		return m_xIndex;
	}
	int GetXIndexMax() const {
		return m_xIndexMax;
	}
	int GetYIndex() const {
		return m_yIndex;
	}

	// modifiers
	void SetName(CStringA sName) {
		m_sName = Utf8ToUtf16(sName).c_str();
	}
	void SetSize(float size) {
		m_size = size;
	}
	void SetXIndex(int xIndex) {
		m_xIndex = xIndex;
	}
	void SetXIndexMax(int xIndexMax) {
		m_xIndexMax = xIndexMax;
	}
	void SetYIndex(int yIndex) {
		m_yIndex = yIndex;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIconPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CIconPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;
	CStringW m_sName;
	float m_size;
	int m_xIndex;
	int m_xIndexMax;
	int m_yIndex;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ICONPROPERTIESCTRL_H_)
