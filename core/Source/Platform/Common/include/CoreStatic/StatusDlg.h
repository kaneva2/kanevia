/******************************************************************************
 StatusDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEPEditDialog.h"
#include "resource.h"

// CStatusDlg dialog

class CStatusDlg : public CKEPEditDialog {
	DECLARE_DYNAMIC(CStatusDlg)

public:
	CStatusDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CStatusDlg();

	void UpdateStatus(const char* msg);
	// Dialog Data
	enum { IDD = IDD_DIALOG_STATUS };

protected:
	BOOL OnInitDialog();

	CRichEditCtrl m_richText;

	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
};
