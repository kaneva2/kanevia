#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <map>
#include <string>

#include <Core/Math/Vector.h>

/*
	-----------------------------------------------------------

	Per-Particle settings
	* Static
		- Gravity coefficient
		- Damping coefficient
		- Damping model
		- Textures
		- Texture selection
		- Texture alpha blending mode
		- Particle lighting (UNSUPPORTED)
		- Collision detection (UNSUPPORTED)
		- Motion blur (UNSUPPORTED)
	* Randomized (mean with variation)
		- Texture image mirroring /rotation
	* Animated (over particle lifetime)
		- Color - R/G/B/A
		- Size - one dimension
		- Rotation angle - one dimension

	-----------------------------------------------------------

	Per-Emitter settings
	* Static
		- Emitter shape / dimension
		- Light (UNSUPPORTED)
	* Animated+Randomized (over emitter lifetime)
		- Emitting rate (p/s)
		- Particle initial velocity X/Y/Z (f/s)
		- Particle lifetime (s)
		- Particle size coefficient
		- Particle rotation angle coefficient

	-----------------------------------------------------------

	Per-Effect settings
	* Static
		- Emitter Definition
		- Particle Definition
		- Offset
	* Randomized (mean with variation)
		- Emitter lifetime
		- Loop emitter
*/

enum DampingModel {
	LINEAR,
	QUADRATIC,
};

enum EmitterShape {
	BOX,
	SPHERE,
};

typedef std::map<unsigned int, float> AnimatedFloatValue;

struct ParticleDef {
	struct _Textures {
		std::vector<std::string> fileNames;
		std::vector<std::string> handles;
		int customFrameRate; // f/s, 0 = spread all images over particle lifetime
		bool additiveAlpha;
		bool randomInitFrame;
		bool randomXMirroring;
		bool randomYMirroring;
	} textures;

	struct _ColorFunc {
		AnimatedFloatValue r;
		AnimatedFloatValue g;
		AnimatedFloatValue b;
		AnimatedFloatValue a;
	} colorFunc;

	AnimatedFloatValue sizeFunc;

	struct _Rottaion {
		bool randomDirection;
		int initAngle;
		int initAngleVar;
		AnimatedFloatValue angleFunc;
	} rotation;

	float gravityCoeff, dampingCoeff;
	DampingModel dampingByVelocity, dampingBySize;

	ParticleDef() {
		textures.customFrameRate = 0;
		textures.additiveAlpha = false;
		textures.randomInitFrame = false;
		textures.randomXMirroring = false;
		textures.randomYMirroring = false;
		rotation.randomDirection = false;
		rotation.initAngle = 0;
		rotation.initAngleVar = 0;
		gravityCoeff = 0.0f;
		dampingCoeff = 0.0f;
		dampingBySize = LINEAR;
		dampingByVelocity = LINEAR;
	}
};

struct EmitterDef {
	EmitterShape shape;

	KEP::Vector3f m_center;

	KEP::Vector3f m_size;

	AnimatedFloatValue birthRateFunc;
	AnimatedFloatValue birthRateVarFunc;
	AnimatedFloatValue particleLifeFunc;
	AnimatedFloatValue particleLifeVarFunc;

	struct _InitialVelocityFunc {
		AnimatedFloatValue x;
		AnimatedFloatValue y;
		AnimatedFloatValue z;
	} initVelFunc;

	struct _InitialVelocityVariationFunc {
		AnimatedFloatValue x;
		AnimatedFloatValue y;
		AnimatedFloatValue z;
	} initVelVarFunc;

	AnimatedFloatValue ptcSizeCoeffFunc;
	AnimatedFloatValue ptcSizeCoeffVarFunc;
	AnimatedFloatValue ptcRotCoeffFunc;
	AnimatedFloatValue ptcRotCoeffVarFunc;

	EmitterDef() {
		shape = BOX;
		m_center.Set(0, 0, 0);
		m_size.Set(1, 1, 1);
	}
};

struct EffectDef {
	struct EffectOffset {
		float x, y, z;
	} offset;
	EmitterDef emitter;
	ParticleDef particle;
	float emitterLife;
	float emitterLifeVar;
	bool loopEmitter;
	bool emitIntoObjectSpace;

	EffectDef() :
			emitterLife(0.0f), emitterLifeVar(0.0f), loopEmitter(false), emitIntoObjectSpace(false) {
		offset.x = offset.y = offset.z = 0.0f;
	}
};
