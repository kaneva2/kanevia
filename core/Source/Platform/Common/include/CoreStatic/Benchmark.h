///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Benchmarks return negative number on error.
float BenchmarkMemory(size_t nRuns); // Returns MB/S
float BenchmarkFloatingPoint(size_t nRuns); // Returns MFLOPS
float BenchmarkCompress(size_t nRuns); // Returns time in milliseconds to decompress sample file
