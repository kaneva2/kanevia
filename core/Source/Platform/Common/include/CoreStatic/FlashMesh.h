///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "ExternalMesh.h"
#include <condition_variable>
#include <memory>

namespace KEP {
class ReMaterial;
typedef std::shared_ptr<ReMaterial> ReMaterialPtr;
class ReD3DX9DeviceState;
class IFlashControlProxy; // Class that does all of the work in a separate thread or process.
class IFlashSystem;

class FlashMesh : public IExternalMesh {
public: // IExternalMesh Interface
	virtual std::string StrThis(bool verbose = false) const override;
	virtual bool SetVolume(double volPct = -1) override;
	virtual int UpdateTexture() override {
		return 1;
	}
	virtual bool UpdateRect(int32_t x, uint32_t width, int32_t y, uint32_t height) override;
	virtual void Delete() override;
	virtual void SetFocus() override;

public: // Static Functions
	static FlashMesh* Create(
		IFlashSystem* pFlashSystem,
		HWND parent,
		const MediaParams& mediaParams,
		uint32_t width = 0, uint32_t height = 0,
		bool loop = false,
		bool wantInput = false,
		int32_t x = 0, int32_t y = 0);

	static bool GetMovieSize(const std::string& filePath, uint32_t& width, uint32_t& height);
	static bool SetEnabledGlobal(bool enable) {
		s_enabledGlobal = enable;
		return true;
	}
	static bool GetEnabledGlobal() {
		return s_enabledGlobal;
	}

	static size_t FlashMeshesNum();
	static void FlashMeshesLog();

private:
	static bool FlashMeshesAdd(IFlashControlProxy* pFC);
	static bool FlashMeshesDel(IFlashControlProxy* pFC);

	static ReMaterialPtr CreateReMaterial(ReD3DX9DeviceState* dev);

protected: // Interface for derived class (FlashExMesh)
	FlashMesh();
	virtual ~FlashMesh();
	const ReMaterialPtr& GetReMaterial() {
		return m_ReMat;
	}
	void UpdateD3DTextureWithAvailableData(ReD3DX9DeviceState* dev);
	void UpdateBitmapAsync();
	bool SetMediaParams(const MediaParams& mediaParams, bool allowRewind = true);

	bool Initialize(
		IFlashSystem* pFlashSystem,
		HWND parent,
		const MediaParams& mediaParams,
		bool wantInput = false,
		int32_t x = 0, int32_t y = 0,
		uint32_t width = 0, uint32_t height = 0);

private:
	unsigned int GetMeshId() const;
	void UpdateD3DTextureToSize(ReD3DX9DeviceState* dev, uint32_t width, uint32_t height);

private:
	std::shared_ptr<IFlashControlProxy> m_pFlashControlProxy;
	ReMaterialPtr m_ReMat;

	static double s_volPctGlobal;
	static bool s_enabledGlobal;

	static std::set<IFlashControlProxy*> s_FlashControlProxys;
	static std::mutex s_mtxFlashControlProxys;
};

} // namespace KEP