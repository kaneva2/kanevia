///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MATERIALDLG_H__F29260E0_E458_11D2_9FE7_E0A74CC10000__INCLUDED_)
#define AFX_MATERIALDLG_H__F29260E0_E458_11D2_9FE7_E0A74CC10000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MaterialDlg.h : header file
//
#include "CGlobalMaterialShaderLib.h"
#include "CEXMeshClass.h"
#include "TextureDatabase.h"
#include "CShaderLibClass.h"
#include "afxwin.h"
#include "TabGroup.h"

class CWldObject;
class CWldObjectList;

/////////////////////////////////////////////////////////////////////////////
// CMaterialDlg dialog

class CMaterialDlg : public CDialog {
	// Construction
public:
	CMaterialDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CMaterialDlg();

	BOOL decalOn;
	TextureDatabase* GL_textureDataBase;
	BOOL forceWireFrame;
	BOOL makeMirror;
	BOOL doubleSidedRendering;
	CGlobalMaterialShaderList* m_globalShaderRef;
	BOOL m_updateLODmatrialChain;

	BOOL enableTextureMovement;
	BOOL enableUVSet0, enableUVSet1;

	CStringA m_zoneName;
	CGlobalMaterialShaderList* m_pShaderList;
	CShaderObjList* m_pxShaderSystemRef;
	CShaderObjList* m_vxShaderSystemRef;
	CMaterialObject* m_materialReference; // Added by Jonny 07/06 because materials can now be modified independant of mesh.

	BOOL applyMaterialToGroup;
	int wrapType;

	float m_ambientRed;
	float m_ambientGreen;
	float m_ambientBlue;

	float m_specPower;

	// removed by Jonny 03/05/08 doesn't do anything.
	//	CEXMeshObj * meshRef;
	// Dialog Data
	//{{AFX_DATA(CMaterialDlg)

	enum { IDD = IDD_DIALOG_MATERIALMODIFY };
	CButton m_fogOverride;
	CButton m_checkFogFilter;
	CButton m_fadeByDistance;

	CComboBox m_textureBlend3;
	CComboBox m_textureBlend2;
	CComboBox m_textureBlend1;
	CComboBox m_textureBlend0;

	CComboBox m_translucencyMode;
	CComboBox m_modulationType;
	CButton m_checkChromeEnable;
	CButton m_checkDoubleSidedRendering;
	CButton m_enableUVSet1;
	CButton m_enableUVSet0;
	CButton m_textureMovementEnable;
	CButton m_makeMirror;
	CButton m_forceWireFrame;
	CStringA m_wrapType;
	float m_uset0;
	float m_uset1;
	float m_vset0;
	float m_vset1;
	int m_modCycleTime;
	float m_fadeVisibleDistance;
	float m_fadeMaxDistance;
	int m_zBias;
	int m_sortOrder;
	CStringA m_matname;
	//}}AFX_DATA

	CComboBox m_colorArg1_L0;
	CComboBox m_colorArg1_L1;
	CComboBox m_colorArg1_L2;
	CComboBox m_colorArg1_L3;

	CComboBox m_colorArg2_L0;
	CComboBox m_colorArg2_L1;
	CComboBox m_colorArg2_L2;
	CComboBox m_colorArg2_L3;

	CButton mFileOpen1Button;
	CButton mFileOpen2Button;
	CButton mFileOpen3Button;
	CButton mFileOpen4Button;

	///////////////////////////////
	// Texture Color Key Members //
	///////////////////////////////

	CButton m_colorKeyButton[4];
	unsigned char m_colorKeyRed[4];
	unsigned char m_colorKeyGreen[4];
	unsigned char m_colorKeyBlue[4];
	unsigned char m_colorKeyAlpha[4];

	CStringA mFileNames[4];

	CComboBox m_pixelShaderList;
	CComboBox m_vertexShaderList;
	CButton m_enablePerPixelAlphaTest;

	int m_chromeUvSet;
	CComboBox m_optionalShaders;

	virtual INT_PTR DoModal();

	//////////////////////
	// Accessor Methods //
	//////////////////////

	const CWldObject* GetWorldObject() const;
	void SetWorldObject(CWldObject* pWldObject);
	const CWldObjectList* GetWorldObjectList() const;
	void SetWorldObjectList(CWldObjectList* pWldObjectList);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaterialDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CMaterialDlg)
	afx_msg BOOL OnInitDialog();
	afx_msg void OnCancel();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void PostNcDestroy();

	//	Set tex names
	afx_msg void OnTextureFileOpen1();
	afx_msg void OnTextureFileOpen2();
	afx_msg void OnTextureFileOpen3();
	afx_msg void OnTextureFileOpen4();

	//	Clear tex names
	afx_msg void OnTextureClear1();
	afx_msg void OnTextureClear2();
	afx_msg void OnTextureClear3();
	afx_msg void OnTextureClear4();

	// Set Color Key
	afx_msg void OnTextureColorKey1();
	afx_msg void OnTextureColorKey2();
	afx_msg void OnTextureColorKey3();
	afx_msg void OnTextureColorKey4();

	//	Mat Saving
	afx_msg void OnMaterialSave();
	afx_msg void OnMaterialLoad();

	afx_msg void OnBTNDecalRGB();
	afx_msg void OnBTNEmisiveRGB();
	afx_msg void OnBTNDefaultRGB();
	afx_msg void OnCHECKDecalOn();
	afx_msg void OnBTNmatSpecularRGB();
	afx_msg void OnBTNColorRGB();
	afx_msg void OnCHECKForceWireframe();
	afx_msg void OnOK();
	afx_msg void OnCHECKMATMakeMirror();
	afx_msg void OnBTNAmbientRGB();
	afx_msg void OnCHECKTextureMovementEnable();
	afx_msg void OnCHECKUVEnableSet0();
	afx_msg void OnCHECKUVEnableSet1();
	afx_msg void OnCHECKMATDoubleSidedRendering();
	afx_msg void OnCHECKChromeEnable();
	afx_msg void OnEditchangeCOMBOModulationType();
	afx_msg void OnChangeEDITModCycleTime();
	afx_msg void OnBUTTONModAmbientTarget();
	afx_msg void OnBUTTONModDiffuseTarget();
	afx_msg void OnBUTTONModEmmisiveTarget();
	afx_msg void OnCHECKapplyMateriaToGroup();
	afx_msg void OnCHECKFadeByDistance();
	afx_msg void OnCHECKFogFilter();
	afx_msg void OnCHECKfogOverride();
	afx_msg void OnBnClickedCheckenableperpixelalphatest();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ProcessTextureLevelChange(int textureIndex);
	void ChangeDialogSize();
	void ApplyToGroup();

protected:
	bool m_isModal;

private:
	CWldObject* m_pWldObject;
	CWldObjectList* m_pWldObjectList;

	CButton m_grpContainer;

	CTabGroup m_tabTexture;
	CButton m_grpTexture;

	CTabGroup m_tabMaterial;
	CButton m_grpMaterial;

	CTabGroup m_tabAdvanced;
	CButton m_grpAdvanced;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MATERIALDLG_H__F29260E0_E458_11D2_9FE7_E0A74CC10000__INCLUDED_)
