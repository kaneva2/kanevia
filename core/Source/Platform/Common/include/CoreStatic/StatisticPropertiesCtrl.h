/******************************************************************************
 StatisticPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_STATISTICPROPERTIESCTRL_H_)
#define _STATISTICPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StatisticPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "CStatClass.h"

/////////////////////////////////////////////////////////////////////////////
// CStatisticPropertiesCtrl window

class CStatisticPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CStatisticPropertiesCtrl();
	virtual ~CStatisticPropertiesCtrl();

public:
	enum { INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_ID,
		INDEX_CAP,
		INDEX_START_VALUE,
		INDEX_GAIN_BASIS,
		INDEX_TYPE };

	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();
	// accessors
	int GetId() const {
		return m_id;
	}
	CStringA GetName() const {
		return Utf16ToUtf8(m_name).c_str();
	}
	float GetCap() const {
		return m_cap;
	}
	float GetStartValue() const {
		return m_startValue;
	}
	float GetGainBasis() const {
		return m_gainBasis;
	}
	int GetBenefitTypeId() const {
		return m_benefitTypeId;
	}

	// modifiers
	void SetId(int id) {
		m_id = id;
	}
	void SetName(CStringA name) {
		m_name = Utf8ToUtf16(name).c_str();
	}
	void SetCap(float cap) {
		m_cap = cap;
	}
	void SetStartValue(float startValue) {
		m_startValue = startValue;
	}
	void SetGainBasis(float gainBasis) {
		m_gainBasis = gainBasis;
	}
	void SetBenefitTypeId(int benefitTypeId) {
		m_benefitTypeId = benefitTypeId;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStatisticPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CStatisticPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void LoadBenefitTypes();

private:
	int m_id;
	CStringW m_name;
	float m_cap;
	float m_startValue;
	float m_gainBasis;
	int m_benefitTypeId;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_STATISTICPROPERTIESCTRL_H_)
