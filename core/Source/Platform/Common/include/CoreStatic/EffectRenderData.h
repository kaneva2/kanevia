///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

struct D3DXMATRIX;

namespace KEP {

class CMaterialObject;
class TextureDatabase;

class EffectRenderData {
public:
	CMaterialObject* mpMaterial;
	D3DXMATRIX* mpWorldTransform;
	TextureDatabase* mpTextureDatabase;

	EffectRenderData() {
		mpMaterial = NULL;
		mpWorldTransform = NULL;
	}
	~EffectRenderData() {}
};

} // namespace KEP

