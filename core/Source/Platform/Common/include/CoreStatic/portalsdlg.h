///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PORTALSDLG_H__9CC8E8AA_F81A_48FE_8E31_F0F524B050A1__INCLUDED_)
#define AFX_PORTALSDLG_H__9CC8E8AA_F81A_48FE_8E31_F0F524B050A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PortalsDlg.h : header file
//
#include "CPortalSysClass.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "PortalZonePropertiesCtrl.h"
#include "AreaOfEffectPropertiesCtrl.h"
#include "LocationInfoPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CPortalsDlg dialog

class CPortalsDlg : public CKEPEditDialog {
	// Construction
public:
	CPortalsDlg(CPortalSysList*& portalsDB,
		CWnd* pParent = NULL); // standard constructor

	CPortalSysList*& m_portalsDBRef;
	LPDIRECT3DDEVICE9 g_pd3dDeviceRef;
	// Dialog Data
	//{{AFX_DATA(CPortalsDlg)
	enum { IDD = IDD_DIALOG_PORTALSDLG };
	CListBox m_portalsDB;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPortalsDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InList(int sel);
	// Generated message map functions
	//{{AFX_MSG(CPortalsDlg)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	afx_msg void OnBUTTONReplace();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeLISTPortalsDB();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonSave();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplacePortalZone(int selection);

private:
	CPortalSysObj* m_selectedPortalZoneObj;

	CCollapsibleGroup m_colPortalZones;
	CPortalZonePropertiesCtrl m_propPortalZone;
	CAreaOfEffectPropertiesCtrl m_propAreaOfEffect;
	CLocationInfoPropertiesCtrl m_propLocationInfo;
	CButton m_grpPortalZones;

	CKEPImageButton m_btnAddPortalSector;
	CKEPImageButton m_btnDeletePortalSector;
	CKEPImageButton m_btnReplacePortalSector;

	void RedrawAllPropertiesCtrls();
	void LoadPortalZoneProperties();
	CPortalSysObj* GetSelectedPortalZoneObj();
	CPortalSysObj* GetPortalZoneObj(int index);

	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PORTALSDLG_H__9CC8E8AA_F81A_48FE_8E31_F0F524B050A1__INCLUDED_)
