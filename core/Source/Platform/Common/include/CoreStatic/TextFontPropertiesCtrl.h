/******************************************************************************
 TextFontPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_TEXT_FONT_PROPERTIESCTRL_H_)
#define _TEXT_FONT_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextFontPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

class CTextFontDialog;

/////////////////////////////////////////////////////////////////////////////
// CTextFontPropertiesCtrl window

class CTextFontPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CTextFontPropertiesCtrl(CTextFontDialog* textFontDlg);
	virtual ~CTextFontPropertiesCtrl();

	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetFontName() const {
		return Utf16ToUtf8(m_sFontName).c_str();
	}
	int GetFontTableTextureID() const {
		return m_fontTableTextureID;
	}

	// modifiers
	void SetFontName(CStringA sFontName) {
		m_sFontName = Utf8ToUtf16(sFontName).c_str();
	}
	void SetFontTableTextureID(int fontTableTextureID) {
		m_fontTableTextureID = fontTableTextureID;
	}

	// methods
	void AddFontTableTexture(CStringA sTexture);
	void ClearFontTableTextures();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextFontPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CTextFontPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void DeleteFontTableTextures();

private:
	int m_id;

	CTextFontDialog* m_textFontDlg;

	CStringW m_sFontName;
	int m_fontTableTextureID;

	std::vector<CStringW*> m_fontTableTextures;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_TEXT_FONT_PROPERTIESCTRL_H_)
