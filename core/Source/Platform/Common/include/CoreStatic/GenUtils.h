///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

namespace KEP {

void GetShortNames(const std::string* pathSource, const char* subDirSource, std::string* pathDest, std::string* subDirDest);

} // namespace KEP