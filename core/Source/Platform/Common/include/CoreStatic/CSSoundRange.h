///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

namespace KEP {

class CSoundRangeObject : public CObject, public GetSet {
	DECLARE_SERIAL(CSoundRangeObject)

public:
	CSoundRangeObject();
	long m_startRange;
	long m_endRange;
	BOOL m_inRange;
	int m_id;
	CStringA m_fileName;
	float m_soundFactor;
	float m_radiusClipping;

	void SafeDelete();
	void Clone(CSoundRangeObject** clone);
	void Serialize(CArchive& ar);
	void SetRanges(long start, long end);

	DECLARE_GETSET
};

class CSoundRangeObjectList : public CObList {
public:
	void SafeDelete();
	void Clone(CSoundRangeObjectList** clone);
	CSoundRangeObjectList(){};

	DECLARE_SERIAL(CSoundRangeObjectList)
};

} // namespace KEP