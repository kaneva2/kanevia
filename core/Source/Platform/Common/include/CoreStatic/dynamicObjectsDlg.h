/******************************************************************************
 dynamicobjectsdlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_DYNAMICOBJECTSDLG_H__2D910461_1D52_11D5_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_DYNAMICOBJECTSDLG_H__2D910461_1D52_11D5_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynamicObjectsDlg.h : header file
//
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "GetSetPropertiesCtrl.h"
#include "DynamicObjectVisualPropertiesCtrl.h"
#include "Common\UIStatic\BasicLayoutMFC.h"

/////////////////////////////////////////////////////////////////////////////
// CDynamicObjectsDlg dialog

class CReferenceObjList;
class StreamableDynamicObject;
class DynamicObjectRM;
class TextureDatabase;

class CDynamicObjectsDlg : public CKEPEditDialog {
	// Construction
public:
	CDynamicObjectsDlg(DynamicObjectRM* dynamicObjDb, CWnd* pParent = NULL); // standard constructor

	// Dialog Data
	//{{AFX_DATA(CDynamicObjectsDlg)
	enum { IDD = IDD_DIALOG_DYNAMIC_OBJECTS };
	CListBox m_dynamicObjectList;
	CListBox m_visualList;
	//CDynamicObjList*&   m_dynamicObjDb;
	DynamicObjectRM* m_dynamicObjSystem;
	TextureDatabase* m_textureDataBase;
	//}}AFX_DATA
protected:
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynamicObjectsDlg)
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

	void InListDynamicObjects(int sel);
	void InListVisuals(int sel);

	StreamableDynamicObject* GetSelectedDynamicObj(int selectionIndex);

	void SetDynamicObjectList();
	void SetCurrentDynamicObject(int objIndex);

	void SetVisualList();
	void SetCurrentVisual(int visualIndex);

	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CDynamicObjectsDlg)
	virtual BOOL OnInitDialog();
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonAddCollision();
	afx_msg void OnBUTTONReplace();
	afx_msg void OnBUTTONAdd();
	afx_msg void OnBUTTONAddVisual();
	afx_msg void OnBUTTONMaterial();
	afx_msg void OnButtonMeshLOD();
	afx_msg void OnButtonLegacyConversion();
	afx_msg void OnBUTTONDeleteVisual();
	afx_msg void OnBUTTONReplaceVisual();
	afx_msg void OnSelchangeLISTVisualList();
	afx_msg void OnSelchangeLISTDynamicObjectList();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonSaveAs();
	afx_msg void OnButtonLoad();
	afx_msg void OnBtnAddLight();
	afx_msg void OnBtnEditLight();
	afx_msg void OnBtnDeleteLight();
	afx_msg LRESULT OnPropertyCtrlChanged(WPARAM wp, LPARAM lp);
	afx_msg BOOL DestroyWindow();
	afx_msg void OnButtonTriggers();
	afx_msg void OnButtonPlaceObj();
	afx_msg void OnButtonClearPlacements();
	afx_msg void OnButtonBuildCollision();
	afx_msg void OnButtonClearCollision();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceDynamicObject(int worldIndex, int visualIndex, bool implicit = false);
	void ReplaceVisual(int objIndex, int visualIndex, bool implicit = false);
	BOOL SaveGameFile(CStringA& path);
	BOOL SaveDialog(CStringA path);
	void UpdateScript();
	void EditLight();

private:
	CCollapsibleGroup m_colDynamicObject;
	CButton m_grpDynamicObject;
	CGetSetPropertiesCtrl m_propDynamicObject;

	CCollapsibleGroup m_colVisual;
	CButton m_grpVisual;
	CDynamicObjectVisualPropertiesCtrl m_propVisual;

	CKEPImageButton m_btnAddDynamicObject;
	CKEPImageButton m_btnDeleteDynamicObject;
	CKEPImageButton m_btnReplaceDynamicObject;

	CKEPImageButton m_btnAddVisual;
	CKEPImageButton m_btnMaterial;
	CKEPImageButton m_btnMeshLOD;
	CKEPImageButton m_btnConvert;
	CKEPImageButton m_btnDeleteVisual;
	CKEPImageButton m_btnReplaceVisual;

	CKEPImageButton m_btnAddLight;
	CKEPImageButton m_btnDeleteLight;
	CKEPImageButton m_btnEditLight;
	CKEPImageButton m_btnPlaceObj;
	CKEPImageButton m_btnClearPlacements;

	//last selected index in the list box
	int m_previousSelDynamicObject;
	int m_previousSelVisual;

	BasicLayoutMFC m_layout;
	CStringA m_lastSavedPath;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNAMICOBJECTSDLG_H__2D910461_1D52_11D5_B1EE_0000B4BD56DD__INCLUDED_)
