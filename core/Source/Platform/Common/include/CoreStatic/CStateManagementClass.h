///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <D3DX9.h>
#include <d3d9.h>
#include "CShaderLibClass.h"
#include "TextureDatabase.h"
#include "matrixarb.h"

#include <list>
#include <vector>
#include <memory>

#define CLIP_MATRIX 0
#define WORLD_MATRIX 4
#define VIEW_MATRIX 8
#define EYE_VECTOR 12
#define DIFFUSE_COLOR 15
#define AMBIENT_COLOR 16
#define EMMISIVE_COLOR 17
#define MAX_POWER 18
#define POINT_LIGHT1 19
#define POINTLIGHT1_COLOR 20
#define POINT_LIGHT2 21
#define POINTLIGHT2_COLOR 22
#define POINT_LIGHT3 23
#define POINTLIGHT3_COLOR 24
#define POINT_LIGHT4 25
#define POINTLIGHT4_COLOR 26
#define POINT_LIGHT5 27
#define POINTLIGHT5_COLOR 28
#define POINT_LIGHT6 29
#define POINTLIGHT6_COLOR 30
#define POINT_LIGHT7 31
#define POINTLIGHT7_COLOR 32
#define DIR_LIGHTVECT 33
#define DIR_LIGHTCOLOR 34
#define AMBIENT_LIGHT 35
#define FOG_RANGE 36
#define DEPTH_DIMINISH 37
#define VAL_ONE 38
#define VAL_ZERO 39
#define VAL_HALF 40
#define VAL_CAM 41
#define VAL_MISC1 50
#define VAL_MISC2 51
#define VAL_MISC3 52
#define VAL_MISC4 53
#define VAL_MISC5 54
#define VAL_WIND 55
#define VAL_CAMROTMATRIX 64

#define MAX_LIGHTS 8

// forward declarations
namespace KEP {
class CMaterialObject;

class CStateManagementObj : public CObject {
public:
	CStateManagementObj(const char* baseDir, CShaderObjList*& vertexShaderList, CShaderObjList*& fxShaderList);

	void SafeDelete();

	CStringA m_baseDir;
	int m_currentDiffuseSource;
	char m_tangentExists;
	int m_currentVertexType;
	int m_globalBlend;
	int GlobalTextureIndex[10];
	int GlobalBlendLevels[10];
	char GlobalColorArg1Levels[10];
	char GlobalColorArg2Levels[10];
	int m_globalZBias;
	BOOL m_globalFilterFog;
	BOOL m_globalEnvFogOn;
	int m_multiTextureSupported;
	int m_stateChangesPerPass;
	int GL_cullOn;
	BOOL GL_wireFillState;
	int GL_lastTextureWrapMode;

	BOOL m_fogOverride;
	D3DCOLOR m_oldFogColor;

	BOOL m_dotProductEnabled;
	int m_shaderMode;

	D3DXMATRIX m_viewMatrix;
	D3DXMATRIX m_projMatrix;
	D3DXMATRIX m_viewMatrixUTP; //untransposed
	D3DXMATRIX m_projMatrixUTP; //untransposed
	D3DXMATRIX m_invViewMatrix;
	D3DXMATRIX m_worldViewMatrixRegister;
	D3DXMATRIX m_worldViewProjectionMatrixRegister;
	D3DXMATRIX m_worldInverseTransposeRegister;
	D3DVECTOR m_camWorldPosition;

	BOOL m_supportBUMPENVMAP;
	BOOL m_stencilMode;
	int m_shadeMode;
	BOOL m_currentZWritableState;
	BOOL m_currentAlphaPixelTestState;

	BOOL m_cameraSpaceEnabled;

	D3DMATERIAL9 m_lastMaterial;

	D3DCAPS9 m_pCaps;
	BOOL m_stencilBufferAble;

	D3DXMATRIX m_tempMatrix;
	D3DXMATRIX m_matrixInit;

	//maintained light states
	BOOL m_lightingMode;
	UINT m_maxActiveLights;
	D3DVECTOR m_lastDirLight[MAX_LIGHTS];
	D3DVECTOR m_lastColorLight[MAX_LIGHTS];

	// vertex, pixel and FX shaders
	CShaderObjList* m_shderSystemRef;
	CShaderObjList* m_pxShderSystemRef;
	CShaderObjList* m_shderSystemPtr;
	CShaderObjList* m_pxShderSystemPtr;
	int m_currentShader;
	int m_currentPxShader;
	CShaderObj* m_currentPxFxShader;
	LPD3DXEFFECT m_currentHSLeffect;
	int m_currentEffectParamCountCRC;
	CStringA m_currentHSLcode;
	bool m_fxShaderOverride;

	BOOL m_specularEnable;

	float m_hightColorRangeStart;
	float m_hightColorRangeEnd;

	D3DVECTOR m_miscConst1, m_miscConst2, m_miscConst3, m_miscConst4, m_miscConst5;

	D3DLIGHT9 m_sunLight;
	void SetSunLight(const D3DLIGHT9& light) { m_sunLight = light; }
	const D3DLIGHT9* GetSunLight() const { return &m_sunLight; }

	D3DVECTOR m_ambLight;
	void SetAmbLight(const D3DVECTOR& ambient) { m_ambLight = ambient; }
	const D3DVECTOR* GetAmbLight() const { return &m_ambLight; }

	D3DVECTOR m_spcLight;
	void SetSpcLight(const D3DVECTOR& specular) { m_spcLight = specular; }
	const D3DVECTOR* GetSpcLight() const { return &m_spcLight; }

	bool m_fogEnabled; // drf - added
	void SetFogEnabled(bool enabled) { m_fogEnabled = enabled; }
	bool GetFogEnabled() const { return m_fogEnabled; }

	bool m_fogCanCull; // drf - added
	void SetFogCanCull(bool enabled) { m_fogCanCull = enabled; }
	bool GetFogCanCull() const { return m_fogCanCull; }

	D3DVECTOR m_fogColor;
	void SetFogColor(const D3DVECTOR& fogColor) { m_fogColor = fogColor; }
	const D3DVECTOR* GetFogColor() const { return &m_fogColor; }

	D3DXVECTOR2 m_fogRange;
	void SetFogRange(const D3DXVECTOR2& fogRange) { m_fogRange = fogRange; }
	const D3DXVECTOR2* GetFogRange() const { return &m_fogRange; }

	void SetDiffuseSource(int type);
	void SetVertexType(int type);
	BOOL SetBlendType(int type);
	BOOL SetZBias(int bias);
	void SetTextureState(DWORD textureIndex, int texturePass, TextureDatabase* textureDataBase);
	void SetBlendLevel(int level, int type);
	BOOL SetCullState(int onType);
	BOOL SetWireFillState(BOOL on);
	BOOL SetTextureTileMode(int mode);
	BOOL SetFogOverrideState(BOOL on);

	BOOL InitShaderGlobals();

	void InitCaps();

	void SetWorldMatrix(const D3DXMATRIX* pWorld);
	void SetViewMatrix(const D3DXMATRIX* pView);
	void SetProjectionMatrix(const D3DXMATRIX* pProj);
	void SetCamMatrix(const D3DXMATRIX* pCam, LPDIRECT3DDEVICE9 pD3dDevice);
	void SetWorldMatrix(const Matrix44f* pWorld) {
		return SetWorldMatrix(reinterpret_cast<const D3DXMATRIX*>(pWorld));
	}
	void SetViewMatrix(const Matrix44f* pView) {
		return SetViewMatrix(reinterpret_cast<const D3DXMATRIX*>(pView));
	}
	void SetProjectionMatrix(const Matrix44f* pProj) {
		return SetProjectionMatrix(reinterpret_cast<const D3DXMATRIX*>(pProj));
	}
	void SetCamMatrix(const Matrix44f* pCam, LPDIRECT3DDEVICE9 pD3dDevice) {
		return SetCamMatrix(reinterpret_cast<const D3DXMATRIX*>(pCam), pD3dDevice);
	}

	void SetMaterial(const D3DMATERIAL9* material);
	void SetSpecular(BOOL state);
	void SetDirLight(D3DVECTOR dirVect, float r, float g, float b);
	void SetSTLight(int index, D3DVECTOR position, float r, float g, float b, float rg);
	void SetFogConstant(float startPoint, float endPoint);
	void SetStencilMode(BOOL state);
	void SetLightingMode(BOOL state);
	void SetShadeMode(int state);
	void SetColorArg2Level(int level, char type);
	void SetColorArg1Level(int level, char type);
	void SetZWritableState(BOOL state);
	void SetAlphaPixelTest(BOOL state);
	void SetWindGlobal(D3DVECTOR curPush);

	void SetVertexShader(CShaderObjList* shaderSystem, int ShaderIndexID);
	void SetPxShader(CShaderObjList* shaderSystem, int ShaderIndexID);
	void SetFXShaderParameters(CMaterialObject* aMaterial, const DWORD* aTextureIdxList, TextureDatabase* aTextureDB, const D3DXMATRIX& aWorldMatrix);
	void OverrideFXShader(LPD3DXEFFECT aFXShader);

protected:
	struct RenderState;

public:
	enum RS_RESULT {
		RS_FAILURE = 0,
		RS_SUCCESS = 1
	};

	typedef std::shared_ptr<RenderState> RenderStatePtr;
	typedef std::list<RenderStatePtr> RenderStateList;
	typedef RenderStateList::iterator RenderStateHandle;

	void SetD3dDevice(LPDIRECT3DDEVICE9 pD3dDevice) {
		m_pD3dDevice = pD3dDevice;
		m_currentState->SetD3dDevice(pD3dDevice);
	}

	RS_RESULT ResetRenderState(); //<! Reset to some reasonable defaults

	CStateManagementObj::RS_RESULT CStateManagementObj::SetRenderState(D3DRENDERSTATETYPE aState, DWORD aValue) {
		return m_currentState->SetState(aState, aValue);
	}

	CStateManagementObj::RS_RESULT CStateManagementObj::GetRenderState(D3DRENDERSTATETYPE aState, DWORD* aValue) {
		return m_currentState->GetState(aState, aValue);
	}

protected:
	LPDIRECT3DDEVICE9 m_pD3dDevice;

	struct RenderStateParameter {
		enum RSPARAM_FLAGS {
			RSPARAM_ISFLOAT = 0x1,
		};

		int paramEnum; // store a LPDIRECT3DDEVICE9 value, negative if no value exists
		int paramFlags;
		std::string paramDesc;
	};

	struct RenderStateValue {
		enum RSVALUE_FLAGS {
			RSVALUE_EVERSET = 0x1,
			RSVALUE_DIFFERENT = 0x2
		};

		union {
			ULONG value_ulong;
			float value_float;
		};

		int valueFlags;
	};

	struct RenderState {
		RenderState() :
				m_pD3dDevice(nullptr) {
			RenderState::ResetParams();
			ResetValues();
		}

		RenderState(const RenderState& aRenderState) = delete;
		RenderState& operator=(const RenderState& aRenderState) = delete;

		void SetD3dDevice(LPDIRECT3DDEVICE9 pD3dDevice) {
			m_pD3dDevice = pD3dDevice;
		}

		RS_RESULT SetState(D3DRENDERSTATETYPE aState, DWORD aValue);
		RS_RESULT GetState(D3DRENDERSTATETYPE aState, DWORD* aValue);

	protected:
		LPDIRECT3DDEVICE9 m_pD3dDevice;

		/* this size is based on the maximum value of D3DRENDERSTATETYPE in DirectX 9.0 */
		static const UINT MAX_RSPARAMS = 210;
		static RenderStateParameter c_params[MAX_RSPARAMS];
		static bool c_paramsInitialized;

		static void ResetParams();

		RenderStateValue m_values[MAX_RSPARAMS];

		void ResetValues() {
			for (UINT idx = 0; idx < MAX_RSPARAMS; idx++) {
				m_values[idx].value_ulong = 0;
				m_values[idx].valueFlags = 0;
			}
		}
	};

	RenderStatePtr m_currentState;

	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration0UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration1UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration2UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration3UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration4UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration5UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration6UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration7UV;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration8UV;

	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration0UVD;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration1UVD;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration2UVD;

	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration0UVT;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration1UVT;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration2UVT;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration3UVT;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration4UVT;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration5UVT;
	LPDIRECT3DVERTEXDECLARATION9 m_pVertexDeclaration6UVT;

	LPDIRECT3DVERTEXDECLARATION9 m_pVertexLeafUVx;
};

} // namespace KEP