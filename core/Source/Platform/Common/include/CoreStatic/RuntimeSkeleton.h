///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "..\..\RenderEngine\ReMaterial.h"
#include "CStructuresMisl.h"
#include "ActorControlType.h"
#include "KEPConstants.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/fast_mutex.h"
#include "Core/Util/FunctionTraits.h"
#include <Core/Util/Memory.h>
#include <Core/Math/Simd.h>

#include "CParticleEmitRange.h"

#include "common\include\CoreStatic\CBoneAnimationNaming.h"
#include "common\include\CoreStatic\CSkeletonAnimHeader.h"
#include "common\include\CoreStatic\AnimSettings.h"
#include "ItemTextureInfo.h"

#include "glAdjust.h"

#include <boost/optional.hpp>

namespace KEP {

class AnimationProxy;
class ReMesh;
class MeshProxy;
class CShadowClass;
class TextureProvider;
typedef std::shared_ptr<TextureProvider> TextureProviderPtr;
class CustomTextureProvider;
class UGCItemTextureProvider;
class CDeformableMesh;
class SkeletonConfiguration;
class CMaterialObject;
class CMovementObj;
class CBoneAnimationObject;
class CBoneObject;
class CBoneList;
class CFrameObj;
class TextureDatabase;
class CDamagePathObjList;
class CFireRangeObjectList;
class SkeletonLabels;
class CSphereCollisionObj;
class CSphereCollisionObjList;
class CArmedInventoryObj;
class CArmedInventoryList;
class CharConfigItem;
class IEquippableManager;
class ReRenderState;
class StreamableDynamicObject;
class CCollisionObj;
class IMemSizeGadget;

struct PendingEquippableData {
	GLID_OR_BASE m_glidOrBase;
	std::set<int> m_exclusionIds;
	CharConfigItem* m_pCCI;
	SkeletonConfiguration* m_pSC;
	boost::optional<std::string> m_boneOverride;

	struct PlacementOverride {
		Vector3f m_ptPosition;
		Vector3f m_vEulerZXY;
		Vector3f m_vScale;
		bool m_bIgnoreParentScale;
	};
	std::unique_ptr<PlacementOverride> m_pPlacementOverride;

	PendingEquippableData() :
			m_glidOrBase(GOB_BASE_ITEM),
			m_pCCI(NULL),
			m_pSC(NULL) {}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

struct BoneNode {
	UINT boneIndex; // index as in m_bonesList
	UINT numChildren; // number of children
	int parentIndex; // Tree node for parent bone
	int nextSiblingIndex; // Tree node for next sibling bone
	int firstChildIndex; // Tree node for first child

	BoneNode() :
			boneIndex(UINT(-1)),
			numChildren(0),
			parentIndex(-1),
			nextSiblingIndex(-1),
			firstChildIndex(-1) {}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

enum class eAnimBlendState : int {
	Idle,
	Blending
};

class RuntimeSkeleton : public std::enable_shared_from_this<RuntimeSkeleton>, private AlignedOperatorNewBase<16> {
protected:
	RuntimeSkeleton();

	virtual ~RuntimeSkeleton();

	void SafeDelete();

public:
	static std::shared_ptr<RuntimeSkeleton> New();

	void Clone(
		std::shared_ptr<RuntimeSkeleton>* clone,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int* spawnCfgOptional,
		int* matCfgOptional = NULL,
		D3DCOLORVALUE* spawnRGB = NULL,
		bool fullClone = true,
		bool cloneRuntimeMeshes = true);

	void CloneBones(RuntimeSkeleton* cloneReturn, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void CloneArmedInventoryList(RuntimeSkeleton* cloneReturn, LPDIRECT3DDEVICE9 g_pd3dDevice);

	void initReMeshBuffer();

	enum class eEquipArmState {
		Failed,
		Success,
		Pending
	};

	void update(const Matrix44f& worldMatrix);

	void flushLightCache();
	ReMesh* getMeshCacheEntry(int lod, int mesh);
	void sortMeshCache();
	static const ReRenderState* getRenderState(const ReMesh* mesh);

	///////////////////////////////////////////////////////////////////////
	// Arm/Disarm Functions - RuntimeSkeleton_ArmDisarm.cpp
	// This is where the meat of actual arming and disarming to and from
	// the avatar skeleton occurs.  After these calls the avatar should
	// look different. All arm/disarm functions eventually call these.
	///////////////////////////////////////////////////////////////////////

	// Called By:
	// - RuntimeSkeleton:: assetsLoaded()
	// - ClientBaseEngine::ConfigEquippableItem()
	// - ClientEngine::ArmEquippableItem()
	// - CMovementObj::ArmItem()
	eEquipArmState ArmEquippableItem(
		const GLID& glid,
		const std::set<int>& exclusionIDs,
		CMovementObj* pMO,
		SkeletonConfiguration* config = NULL,
		const CharConfigItem* pConfigItem = NULL,
		const boost::optional<std::string>& optstrBoneOverride = boost::none);

	eEquipArmState ArmEquippableItem(PendingEquippableData&& pendingEquip, CMovementObj* pMO);

	// Called By:
	// - RuntimeSkeleton:: assetsLoaded()
	// - ClientBaseEngine::ConfigEquippableItem()
	// - ClientEngine::ArmEquippableItem()
	// - CMovementObj::ArmItem()
	bool DisarmEquippableItem(const GLID& glid);

	/*!
	* \brief
	* Invalidates the shaders, which is useful if we're using vertex pre-lighting
	* and the object has just moved.
	*/
	void invalidateShaders();

	bool areAllMeshesLoaded() const {
		return m_bAllMeshesLoaded;
	}
	bool isFullyLoaded() const;

	void setLoadPriorityDelta(int val) {
		m_loadPriorityDelta = val;
	}

	// Checks for an intersection between the ray and this skeleton object.
	// If an intersection, is found, returns true and stores point of intersection
	// in intersectionPoint, if not NULL.
	bool intersectRay(const Vector3f& rayOrigin, const Vector3f& rayDirection, Vector3f* location = NULL);

	bool intersectSphere(const Vector3f& sphereOrigin, float radius, const Vector3f& orientation, float maxAngle, Vector3f* location = NULL);

	BOOL addBone(
		const std::string& boneName,
		const std::string& parentName,
		Vector3f dirVect,
		Vector3f upVect,
		Vector3f position,
		Vector3f scale,
		CBoneObject** boneNodeReturn);

	void initBones();

	void applyFreeLookRotation(float pitch, float yaw);

	// Resource callbacks. Called from the main thread
	void AnimationReadyCallback(const GLID& glidAnim);
	void EquippableReadyCallback(GLID glid, CMovementObj* pMO);
	void MeshReadyCallback(uint64_t hash);

	void calculateShadow(const Vector3f& objectPos, CCollisionObj* collisionObject);
	void enableShadow(bool enable,
		float radius,
		float collisionRadius,
		float depth,
		float height,
		int textureIndex,
		const std::string& textureSearchName);

	static bool AnimInUseTimeElapsedIsPast(TimeMs timeMs, RuntimeSkeleton* pRS, bool aUseSecondaryAnimation);

	BOOL computeBoundingBox();
	BOOL computeBoundingBoxWithSkinnedMesh(const SkeletonConfiguration* pSkeletonCfg); // From CSkeletonObject::computeBoundingBox

	BOOL fadeIn(TimeMs duration);

	bool scaleSystem(float aNewScaleFactorX, float aNewScaleFactorY, float aNewScaleFactorZ);

	BOOL buildHierarchy();

	BOOL resolveSkeletonAnimHeaderVersionConflict(eAnimType animType, int& desiredAnimVer, const GLID& glidCurrAnim);

	// realize custom textures -- mimic the convention in streamabledynobj -- still wonder if this is necessary
	// right now this would only handle materials of rigid meshes -- will think about a way for deformable mesh either merge matt's RealizeDimConfig or separate
	void updateCustomMaterials(TextureDatabase* textureDb, const TextureColorMap* altColorMap = nullptr);

	// New version for DO whose m_apMaterialObject array is properly initialized. TODO: consolidate with updateCustomMaterials()
	void updateCustomMaterials2(TextureDatabase* textureDb, const TextureColorMap* altColorMap, int customTextureOption, const std::vector<bool>& meshCustomizableFlags);

	void initFromDynamicObject(StreamableDynamicObject* dob, GLID glid, const TextureColorMap* altColorMap = nullptr);

	bool addParticle(
		const std::string& boneName,
		int particleSlotId,
		GLID particleGlid,
		int particleDBIndex,
		const std::string& particleEffectName,
		const Vector3f& offset = Vector3f(0, 0, 0),
		const Vector3f& dir = Vector3f(0, 0, 1),
		const Vector3f& up = Vector3f(0, 1, 0));

	bool addParticleByGlid(const std::string& boneName, int particleSlotId, GLID particleGlid, const Vector3f& offset = Vector3f(0, 0, 0), const Vector3f& dir = Vector3f(0, 0, 1), const Vector3f& up = Vector3f(0, 1, 0)) {
		return addParticle(boneName, particleSlotId, particleGlid, PARTICLE_DB_INDEX_INVALID, std::string(), offset, dir, up);
	}

	bool addParticleByStockIndex(const std::string& boneName, int particleSlotId, int particleDBIndex, const Vector3f& offset = Vector3f(0, 0, 0), const Vector3f& dir = Vector3f(0, 0, 1), const Vector3f& up = Vector3f(0, 1, 0)) {
		return addParticle(boneName, particleSlotId, GLID_INVALID, particleDBIndex, std::string(), offset, dir, up);
	}

	bool addParticleByEffectName(const std::string& boneName, int particleSlotId, const std::string& particleEffectName, const Vector3f& offset = Vector3f(0, 0, 0), const Vector3f& dir = Vector3f(0, 0, 1), const Vector3f& up = Vector3f(0, 1, 0)) {
		return addParticle(boneName, particleSlotId, GLID_INVALID, PARTICLE_DB_INDEX_INVALID, particleEffectName, offset, dir, up);
	}

	bool removeParticle(const std::string& boneName, int particleSlotId);

	UINT getParticleRuntimeId(const std::string& boneName, int particleSlotId);

	//-------------------------------------------
	// Current serialization functions
	//-------------------------------------------
	static bool write(RuntimeSkeleton* toSave, CArchive& ar);
	static std::shared_ptr<RuntimeSkeleton> read(CArchive& ar);

	//-------------------------------------------
	// Legacy EDB/APD serialization support
	//-------------------------------------------
	void writeLegacySkeletonObject(CArchive& ar, const SkeletonConfiguration* pSkeletonConfig) {
		return writeLegacySkeletonObjectV9(ar, pSkeletonConfig);
	}
	void readLegacySkeletonObject(CArchive& ar, SkeletonConfiguration* pSkeletonConfig);

	std::string getSkeletonName() const {
		return m_skeletonName;
	}
	void setSkeletonName(const std::string& name) {
		m_skeletonName = name;
	}

	SkeletonLabels* getOverheadLabels() const {
		return m_overheadLabels.get();
	}

	bool isCulled() const {
		return m_culled;
	}
	void setCulled(bool culled) {
		m_culled = culled;
	}

	bool isMeshSortValid() const {
		return m_meshSortValid;
	}
	void setMeshSortValid(bool val) {
		m_meshSortValid = val;
	}

	bool isShadowEnabled() const {
		return m_shadowOn;
	}
	void setShadowEnabled(bool val) {
		m_shadowOn = val;
	} // Used by Serialization function only. For normal use cases, call enableShadow.

	const CShadowClass* getShadow() const {
		return m_shadowInterface;
	}

	void setCurrentLOD(int val) {
		m_currentLOD = val;
	}
	void setCurrentLODFromDistance(float fDist);
	int getCurrentLOD() const {
		return m_currentLOD;
	}

	UINT getBoneCount() const;

	const CBoneList* getBoneList() const {
		return m_pBoneList;
	}
	void setBoneList(CBoneList* newList) {
		assert(m_pBoneList == nullptr);
		m_pBoneList = newList;
	}

	size_t getRigidMeshCount() const;
	size_t getSkinnedMeshCount() const {
		return m_deformableMeshArray.size();
	}

	// Called by importer only -- low level no integration, use with care
	void setSkinnedMeshCount(size_t newCount);

	CDeformableMesh* getSkinnedMesh(size_t meshSlotId) {
		return meshSlotId < m_deformableMeshArray.size() ? m_deformableMeshArray[meshSlotId].get() : nullptr;
	}
	void setSkinnedMesh(size_t meshSlotId, CDeformableMesh* pNewMesh);
	void disposeSkinnedMesh(size_t meshSlotId);

	ReMesh* getLODMesh(unsigned meshSlotId, unsigned LOD);
	void clearLODMeshes(unsigned meshSlotId);

	const CArmedInventoryList* getArmedInventoryList() const {
		return m_armedInventoryList;
	}
	void setArmedInventoryList(CArmedInventoryList* pNewList) {
		assert(m_armedInventoryList == nullptr);
		m_armedInventoryList = pNewList;
	}

	size_t getEquippableItemCount() const;

	const CArmedInventoryObj* getEquippableItemByGLID(GLID glid) const;
	CArmedInventoryObj* getEquippableItemByGLID(GLID glid);

	void getAllEquippedGLIDs(std::vector<GLID>& out);

	void getAllEquippableItems(std::vector<CArmedInventoryObj*>& out);
	void getAllEquippableItems(std::vector<const CArmedInventoryObj*>& out) const;

	bool updateEquippableItemBoneAttachment(GLID glid, int boneIndex, const std::string& boneName);
	bool updateEquippableItemTransform(GLID glid, const Vector3f& position, const Vector3f& rotZXY, const Vector3f& scale, bool bIgnoreParentScale);
	bool updateEquippableItemCustomTexture(GLID glid, const std::string& textureUrl, TextureDatabase* pTextureDatabase);
	void realizeEquippableItemDimConfig(IEquippableManager* pManager);

	size_t getNumPendingEquippableItems() const {
		return m_pendingEquippableItems.size();
	}
	const PendingEquippableData& getPendingEquippableItem(size_t index) const {
		return m_pendingEquippableItems[index];
	}

	bool configPendingEquippableItemMeshSlot(GLID glid, size_t meshSlotId, int meshId, int matlId);
	bool updatePendingEquippableItemAnimation(GLID glid, GLID animGLID);

	// This one is hard-coded for avatars, by the looks of it.
	bool getAnimatedWorldPosition(Vector3f& pos) const;

	// This one should work for ADO, DO and anything else that has
	// the root bone at index 0 or no bones at all.
	const Vector3f& getRootBonePosition() const {
		return m_rootBonePosition;
	}

	const Matrix44f& getWorldMatrix() const {
		return m_worldMatrix;
	}
	void setWorldMatrix(const Matrix44f* pWorldMatrix, bool scale = true); // Set world matrix, scaled plus the other two derived matrices

	// Set world matrix only -- not sure why we have this use case -- keep it for now
	void setWorldMatrixOnly(const Matrix44f& worldMatrix) {
		m_worldMatrix = worldMatrix;
	}

	const Matrix44f& getWorldInvTransposeMatrix() const {
		return m_worldInvTransposeMatrix;
	}

	const Vector3f& getScaleVector() const {
		return m_scaleVector;
	}

	void setScaleVector(const Vector3f& scaleVector) {
		m_scaleVector = scaleVector;
	}

	Matrix44f getBoneMatrixByIndex(int index);

	bool getBoneByName(const std::string& boneName, CBoneObject** boneObject, bool convertSpaceToUnderscore = false) const;

	int getBoneIndexByName(const std::string& boneName, bool convertSpaceToUnderscore = false) const;

	float getWorldRadius() const {
		return m_fWorldRadius;
	}

	const Vector3f& getWorldBoxCenter() const {
		return m_ptWorldBoxCenter.AsVector4f().SubVector<3>();
	}
	const Simd4f& getWorldBoxCenterSimd() const {
		return m_ptWorldBoxCenter;
	}

	const Vector3f& getWorldBoxHalfSize() const {
		return m_vWorldBoxHalfSize.AsVector4f().SubVector<3>();
	}
	const Simd4f& getWorldBoxHalfSizeSimd() const {
		return m_vWorldBoxHalfSize;
	}

	const BNDBOX& getBonesBoundingBox() const {
		return m_boxBones;
	}

	const BNDBOX& getLocalBoundingBox() const {
		return m_boxLocal;
	}

	void setBonesBoundingBox(const BNDBOX& box) {
		m_boxBones = box;
	}
	void setLocalBoundingBox(const BNDBOX& box) {
		m_boxLocal = box;
		updateWorldBox();
	}

	BOOL setPrimaryAnimationSettings(const AnimSettings& animSettings);

	BOOL setPrimaryAnimationByIndex(int animIndex, const AnimSettings& animSettings);

	BOOL updatePrimaryAnimationByIndex(int animIndex, const AnimSettings& animSettings);

	BOOL setPrimaryAnimationByAnimTypeAndVer(eAnimType animType, int animVer = ANIM_VER_ANY, bool animDirForward = true, TimeMs animBlendDurationMs = ANIM_BLEND_DURATION_MS);

	int getAnimVerPreference() const {
		return m_animVerPreference;
	}
	void setAnimVerPreference(int animVer) {
		m_animVerPreference = animVer;
	}

	void setAnimTarget(const GLID& glid) {
		m_animTargetGlid = glid;
		m_animTargetChanged = true;
	}

	void getAnimTarget(GLID& glid, bool& changed) {
		glid = m_animTargetGlid;
		changed = m_animTargetChanged;
		m_animTargetChanged = false;
	}

	GLID getAnimInUseSkeletonGlid() const {
		return (m_animInUse[ANIM_PRI].m_pSAH ? m_animInUse[ANIM_PRI].m_pSAH->m_animGlid : GLID_INVALID);
	}

	GLID getAnimInUseGlid(int animSel = ANIM_PRI) const {
		return m_animInUse[animSel].m_glid;
	}

	bool isAnimInUseValid(int animSel = ANIM_PRI) const {
		return IS_VALID_GLID(m_animInUse[animSel].m_glid);
	}

	bool isAnimInUseDirForward(int animSel = ANIM_PRI) const {
		return m_animInUse[animSel].m_dirForward;
	}

	bool setAnimInUseElapsedMs(size_t animSel, TimeMs elapsedMs);
	bool updateAnimInUseElapsedMs(size_t animSel, TimeMs elapsedMs);

	TimeMs getAnimMinTimeMs() const {
		auto pSAH = m_animInUse[ANIM_PRI].m_pSAH;
		return pSAH ? pSAH->m_animMinTimeMs : (TimeMs)0.0;
	}

	bool isAnimUpdated() const {
		return m_animUpdated;
	}
	void setAnimUpdated(bool val) {
		m_animUpdated = val;
	}

	TimeMs getAnimUpdateDelayMs() const {
		return m_animUpdateDelayMs;
	}
	void setAnimUpdateDelayMs(TimeMs timeMs) {
		m_animUpdateDelayMs = timeMs;
	}

	// By setting a time the menu game object's animation will freeze frame at that time.
	// Setting this to -1 disables this freeze frame and plays the animation as normal.
	bool updateAnimations(TimeMs animTimeMs = -1.0);

	int getOrQueueAnimationIndexByGlid(const GLID& glid);

	void queueAnimForLoad(const GLID& glid);

	size_t getSkeletonAnimHeaderCount() const {
		return m_apSkeletonAnimHeaders.size();
	}

	bool isValidAnimIndex(int animIndex) const {
		return (animIndex >= 0) && (animIndex < (int)getSkeletonAnimHeaderCount());
	}

	CSkeletonAnimHeader* getSkeletonAnimHeaderByGlid(const GLID& glid);

	CSkeletonAnimHeader* getSkeletonAnimHeaderByIndex(int animIndex) const {
		return isValidAnimIndex(animIndex) ? m_apSkeletonAnimHeaders[animIndex].get() : nullptr;
	}

	int getAnimationIndexByHash(UINT64 animHash) const;

	int getAnimationIndexByGlid(const GLID& glid) const;

	int getAnimationIndexByAnimTypeAndVer(eAnimType animType, int animVer = ANIM_VER_ANY) const;

	GLID getAnimationGlidByAnimTypeAndVer(eAnimType animType, int animVer = ANIM_VER_ANY) const;

	void getAnimationDurations(std::map<GLID, TimeMs>& animDurationMap);

	void setSecondaryAnimation(const GLID& glid) {
		m_animInUse[ANIM_SEC].m_glid = glid;
		auto animIndex = getAnimationIndexByGlid(glid);
		m_animInUse[ANIM_SEC].m_animIndex = animIndex;
		m_animInUse[ANIM_SEC].m_pSAH = getSkeletonAnimHeaderByIndex(animIndex);
	}

	void remapSkeletonAnimHeaderByGlid(const GLID& glid, eAnimType animType, int& animVer, bool bResolveConflict);

	bool isAnimBoneMatricesInited() const {
		return m_animBoneMats != nullptr;
	}

	const Matrix44fA16& getAnimBoneMatrix(int index) const {
		return m_animBoneMats[index];
	}

	Vector3f getBonePositionByIndex(int index);

	CFrameObj* getBoneFrameByIndex(int index) const;

	CBoneObject* getBoneByIndex(int index) const;

	float getInitialBoneLength(const std::string& boneName) const;

	int getRootBoneIndex() const {
		return m_rootBoneIndex;
	}

	int getParentBoneIndex(int boneIndex) const {
		ASSERT(boneIndex >= 0 && (boneIndex < (int)m_bonesHierarchy.size()));
		return m_bonesHierarchy[boneIndex].parentIndex;
	}

	int getFirstChildBoneIndex(int boneIndex) const {
		ASSERT(boneIndex >= 0 && (boneIndex < (int)m_bonesHierarchy.size()));
		return m_bonesHierarchy[boneIndex].firstChildIndex;
	}

	int getNextSiblingBoneIndex(int boneIndex) const {
		ASSERT(boneIndex >= 0 && (boneIndex < (int)m_bonesHierarchy.size()));
		return m_bonesHierarchy[boneIndex].nextSiblingIndex;
	}

	int getNumChildrenBones(int boneIndex) const {
		ASSERT(boneIndex >= 0 && (boneIndex < (int)m_bonesHierarchy.size()));
		return m_bonesHierarchy[boneIndex].numChildren;
	}

	bool isScaled(bool checkParentXForm) const {
		return checkParentXForm && m_isParentScaled || abs(m_scaleVector.X() - 1.0f) >= 1E-6 || abs(m_scaleVector.Y() - 1.0f) >= 1E-6 || abs(m_scaleVector.Z() - 1.0f) >= 1E-6;
	}

	void setParentScaled(bool val) {
		m_isParentScaled = val;
	}

	const CSphereCollisionObj* getCalculationSphere() const {
		return m_calculationSphere;
	}
	const CSphereCollisionObjList* getSphereBodyCollisionSystem() const {
		return m_sphereBodyCollisionSystem;
	}

	bool hasParticles() const {
		return m_particleEmitterList != nullptr;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	// Callable is usually a lambda, takes an argument of type CParticleEmitRangeObj* and should return void or bool. If it returns false, the iteration is aborted.
	template <typename Callable>
	void iterateParticleEmitters(Callable&& callable) {
		if (m_particleEmitterList) {
			for (POSITION emPos = m_particleEmitterList->GetHeadPosition(); emPos != NULL;) {
				bool bContinue = true;
				CallPossiblyVoidFunction(&bContinue, std::forward<Callable>(callable), static_cast<CParticleEmitRangeObj*>(m_particleEmitterList->GetNext(emPos)));
				if (!bContinue)
					break;
			}
		}
	}

	// set customtexture function call for UGC-EI
	// right now this would only handle materials of rigid meshes -- will think about a way for deformable mesh either merge matt's DimHotSwap or separate
	template <typename T>
	void setCustomTexture(T&& customTexturePtr) {
		m_customTexture = std::forward<T>(customTexturePtr);
	}

	template <typename T>
	void setUGCItemTexture(T&& ugcItemTexturePtr) {
		m_ugcItemTexture = std::forward<T>(ugcItemTexturePtr);
	}

	TextureProviderPtr getCurrentTexture();

	void resetCustomTexture() {
		m_customTexture.reset();
	}

	void applyMaterials(size_t meshIndex = SKELETAL_ALL_MESHES);

	void applyTexturePanningSettings(const std::map<UINT, Vector2f>& texturePanningSettings, USHORT meshIdFilter, USHORT uvSetIdFilter);

	void SetMaterialObject(size_t i, CMaterialObject* pMaterialObject, bool applyToMeshes, const TextureColorMap* pAltColorMap);

	CMaterialObject* GetMaterialObject(size_t i) {
		if (i < m_apMaterialObjects.size()) {
			return m_apMaterialObjects[i];
		}
		return nullptr;
	}

	void getAvatarCapsuleDimensions(Out<float> fHalfHeight, Out<float> fRadius);

	template <typename Callable>
	__forceinline void IterateMeshes(size_t iLOD, Callable&& callable) {
		auto& apLodMeshes = m_apLodMeshes[iLOD];
		for (const std::unique_ptr<ReMesh>& pReMesh : apLodMeshes)
			callable(pReMesh.get());
	}

	template <typename Callable>
	__forceinline void IterateMaterials(Callable&& callable) {
		for (CMaterialObject* pMaterialObject : m_apMaterialObjects)
			callable(pMaterialObject);
	}

	template <typename Callable>
	__forceinline void IterateMeshesAndMaterials(size_t iLOD, Callable&& callable) {
		auto& apLodMeshes = m_apLodMeshes[iLOD];
		for (size_t i = 0; i < apLodMeshes.size(); ++i) {
			ReMesh* pReMesh = apLodMeshes[i].get();
			CMaterialObject* pMaterialObject = m_apMaterialObjects[i];
			callable(pReMesh, pMaterialObject);
		}
	}

private:
	void writeLegacySkeletonObjectV9(CArchive& ar, const SkeletonConfiguration* pSkeletonConfig);

	static const unsigned char skeleton_archive_code = (unsigned char)'s';

	static bool writeV1(RuntimeSkeleton* toSave, CArchive& ar);

	static std::shared_ptr<RuntimeSkeleton> readV1(CArchive& ar);

	void remapSkeletonAnimHeader(CSkeletonAnimHeader* pAnimHdr, eAnimType newAnimType, int& newAnimVersion, bool bResolveConflict);

	CArmedInventoryObj* getCurrentHead();

	bool updatePendingEquippableItemPlacement(GLID glid, const Vector3f& position, const Vector3f& rotZXY, const Vector3f& scale, bool bIgnoreParentScale);
	bool updatePendingEquippableItemTextureUrl(GLID_OR_BASE glidOrBase, const std::string& textureUrl);

	void updateRootBonePosition();
	void updateWorldBox();

	void checkFullyLoaded();

	void getMemSizeBoneMats(IMemSizeGadget* pMemSizeGadget) const;
	void getMemSizeLodMeshes(IMemSizeGadget* pMemSizeGadget) const;
	void getMemSizeMeshCache(IMemSizeGadget* pMemSizeGadget) const;

private:
	// Member variables used in render loop put together to improve cache hit rate.
	CArmedInventoryList* m_armedInventoryList;
	CBoneList* m_pBoneList;
	float m_fWorldRadius;
	Simd4f m_ptWorldBoxCenter;
	Simd4f m_vWorldBoxHalfSize;
	Matrix44f m_worldMatrix;
	std::unique_ptr<SkeletonLabels> m_overheadLabels;

	std::shared_ptr<CustomTextureProvider> m_customTexture;
	std::shared_ptr<UGCItemTextureProvider> m_ugcItemTexture; // Separate item texture specified by designer from custom texture set by a creator or player
	bool m_bAllMeshesLoaded;
	bool m_culled;
	bool m_meshSortValid;
	int m_loadPriorityDelta;
	int m_currentLOD;
	int m_lastLOD;

	// List of CDeformableMesh classes in use (which includes MeshProxy and Materials)
	// Might be able to get rid of this, if we have somewhere else to put the materials.
	std::vector<std::unique_ptr<CDeformableMesh>> m_deformableMeshArray;

	BNDBOX m_boxLocal;
	BNDBOX m_boxBones;

	std::string m_skeletonName;

	std::unique_ptr<Matrix44fA16[]> m_invBoneMats; // bind pose
	std::vector<BoneNode> m_bonesHierarchy; // bone hierarchy for fast lookup
	UINT m_rootBoneIndex;

	// Pending equippable items with pending configurations
	std::vector<PendingEquippableData> m_pendingEquippableItems;

	// Pending animation queues for dynamic animation loadings (for new animations without existing definitions in m_animHeaders)
	std::set<GLID> m_glidsQueuedAnim;

	CParticleEmitRangeObjList* m_particleEmitterList; // Particle emitter list assignments

	Matrix44f m_worldViewProjMatrix;
	Matrix44f m_worldInvTransposeMatrix;

	bool m_isParentScaled; // Flag to force normal vector normalization even if m_scaleVector is (1,1,1)
	Vector3f m_scaleVector;

	// These are the loaded (from disk or server) meshes that have been copied from MeshProxies in m_deformableMeshArray.
	// It might not be necessary to copy them though.  I could just reference the meshes stored in m_deformableMeshArray,
	// but I'd need to guard against dangling pointer bugs that have been a problem in the past.
	size_t m_nLODDistances;
	float m_afLODDistances[SKELETAL_LODS_MAX];
	std::vector<std::unique_ptr<ReMesh>> m_apLodMeshes[SKELETAL_LODS_MAX];
	std::vector<CMaterialObject*> m_apMaterialObjects;

	// Meshes that will be rendered this frame.  This is a sorted list from m_apLodMeshes
	std::vector<ReMesh*> m_meshCache[SKELETAL_LODS_MAX];

	// [0] for primary animation
	// [1] for primary blend animation
	// [2] for secondary animation
	// [3] for secondary blend animation
	struct AnimInUse {
		CSkeletonAnimHeader* m_pSAH;
		int m_animIndex;
		GLID m_glid;
		bool m_dirForward;
		TimeMs m_elapsedMs;

		AnimInUse(
			CSkeletonAnimHeader* pSAH = nullptr,
			int index = ANIM_INVALID,
			GLID glid = GLID_INVALID,
			bool dirForward = true,
			TimeMs elapsedMs = 0.0) :
				m_pSAH(pSAH),
				m_animIndex(index),
				m_glid(glid),
				m_dirForward(dirForward),
				m_elapsedMs(elapsedMs) {}
	} m_animInUse[4];

	// Target animation once blending is finished
	GLID m_animTargetGlid = GLID_INVALID;
	bool m_animTargetChanged = false;

	// DRF - ED-6846 - Other Avatar Animation Blending
	int m_setAnimIndex = ANIM_INVALID;
	AnimSettings m_setAnimSettings;

	eAnimBlendState m_animBlendState;
	TimeMs m_animBlendDurationMs;
	TimeMs m_animBlendStartTimeMs;

	bool m_animUpdated;
	TimeMs m_animUpdateTimeMs; // 0=update without delay
	TimeMs m_animUpdateDelayMs; // delay between animation updates (ms)

	int m_animVerPreference;

	std::vector<std::unique_ptr<CSkeletonAnimHeader>> m_apSkeletonAnimHeaders;

	std::unique_ptr<Matrix44fA16[]> m_animBoneMats;

	Vector3f m_rootBonePosition; // Position of root bone for quick access in render loop.

	ReLightCache m_lightCache;

	float m_headAngle;
	float m_waistAngle;

	int m_waistBone;

	float m_waistTurnRate;
	int m_freeLookBone;
	int m_freeLookBone2;

	float m_freeLookPitch;
	float m_freeLookYaw;

	Vector3f m_pitchAxis;
	Vector3f m_yawAxis;

	CFireRangeObjectList* m_fireQueueList;
	CDamagePathObjList* m_damageSphereList; // collision path spheres

	bool m_shadowOn;
	CShadowClass* m_shadowInterface;

	// special collision locational spheres
	CSphereCollisionObjList* m_sphereLocationalDamageList;
	CSphereCollisionObj* m_calculationSphere;
	CSphereCollisionObjList* m_sphereBodyCollisionSystem;

	friend class ARBImporter;
};

} // namespace KEP
