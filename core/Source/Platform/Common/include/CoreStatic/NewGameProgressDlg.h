#pragma once
#include "afxwin.h"

// #include "KepPropertyPage.h"
#include "resource.h"

// CNewGameProgress dialog

class CNewGameProgressDlg : public CDialog {
	DECLARE_DYNCREATE(CNewGameProgressDlg)

public:
	CNewGameProgressDlg(); // standard constructor
	virtual ~CNewGameProgressDlg();

	// set the next status to Running... and current one to Complete
	void SetNextToRunning();
	void Create(CWnd* parent) {
		CDialog::Create(CNewGameProgressDlg::IDD, parent);
	}
	void Destroy() {
		DestroyWindow();
	}

	// Dialog Data
	enum { IDD = IDD_NEW_GAME_PROGRESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

	void OnEnUpdateStatus();

	int m_currentStatusId;
	CStringA m_complete;
	CStringA m_running;

public:
};
