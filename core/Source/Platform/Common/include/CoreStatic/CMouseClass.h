///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

class CMouseVisualObj : public CObject, public GetSet {
	CMouseVisualObj() {}

	DECLARE_SERIAL(CMouseVisualObj)

public:
	CMouseVisualObj(CStringA L_fileName,
		LPDIRECTDRAWSURFACE7 L_surface);
	CStringA m_fileName;
	LPDIRECTDRAWSURFACE7 m_surface;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CMouseVisualObjList : public CObList {
public:
	CMouseVisualObjList(){};

	DECLARE_SERIAL(CMouseVisualObjList)
};

//mouse main object
class CMouseObj : public CObject {
	CMouseObj() {}

	DECLARE_SERIAL(CMouseObj)

public:
	CMouseObj(CStringA L_mouseCfgName,
		int L_sizeX,
		int L_sizeY,
		BOOL L_inUse,
		CMouseVisualObjList* L_visuals,
		int L_visualCount,
		int L_currentVisual,
		int L_redSrc,
		int L_greenSrc,
		int L_blueSrc);
	int m_redSrc;
	int m_greenSrc;
	int m_blueSrc;
	int m_currentVisual;
	int m_visualCount;
	CStringA m_mouseCfgName;
	int m_sizeX;
	int m_sizeY;
	BOOL m_inUse;
	CMouseVisualObjList* m_visuals;

	void Serialize(CArchive& ar);
};

class CMouseObjList : public CObList {
public:
	CMouseObjList(){};

	DECLARE_SERIAL(CMouseObjList)
};
