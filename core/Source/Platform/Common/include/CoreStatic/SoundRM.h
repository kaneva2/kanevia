#pragma once

#include "Event\Base\IEvent.h"
#include "ResourceManagers\ResourceManager.h"
#include "ResourceType.h"

#include "jsLock.h"

namespace KEP {

class OALSoundProxy;

/// Sound Resource Manager
class SoundRM : public ResourceManager {
	std::vector<OALSoundProxy*> m_proxies;
	jsFastSync m_SyncProxies;

public:
	SoundRM(const std::string& id) :
			ResourceManager(id, ResourceType::SOUND) {}

	~SoundRM() {}

	OALSoundProxy* getProxy(const GLID& glid, bool bCreateIfNotExist);

	OALSoundProxy* addUGCProxy(const GLID& localGlid, BYTE* theBuff, FileSize theSize);

	void HandleSoundDataEvent(IN ULONG lparam, IN IDispatcher* disp, IN IEvent* e);

	static EVENT_PROC_RC ProcessSoundDataEvent(IN ULONG lparam, IN IDispatcher* disp, IN IEvent* e);

protected:
	//virtual void QueueFileForAsyncDownload(IResource* pRes) override;
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) override;
	virtual std::string GetResourceFileName(UINT64 hash) override;
	virtual std::string GetResourceFilePath(UINT64 hash) override;
	virtual std::string GetResourceURL(UINT64 hash) override {
		return ""; // UGC Asset URL will be determined before downloading
	}

	virtual bool IsUGCItem(UINT64 /*hash*/) const override {
		return true;
	}
};

} // namespace KEP
