/******************************************************************************
 ActorsDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ACTORS_DLG_H_)
#define _ACTORS_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ActorsDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

////////////////////////////////////////////
// CActorsDialog control

class CActorsDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CActorsDialog)

public:
	CActorsDialog();
	virtual ~CActorsDialog();

	// Dialog Data
	//{{AFX_DATA(CActorsDialog)
	enum { IDD = IDD_DIALOG_ENTITY };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CActorsDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colActors;
	CButton m_grpActors;

	CCollapsibleGroup m_colRuntime;
	CButton m_grpRuntime;

	CCollapsibleGroup m_colInventory;
	CButton m_grpInventory;

	CCollapsibleGroup m_colAppendages;
	CButton m_grpAppendages;

	CKEPImageButton m_btnAddActor;
	CKEPImageButton m_btnDeleteActor;
	CKEPImageButton m_btnSaveSingleActor;
	CKEPImageButton m_btnLoadSingleActor;
	CKEPImageButton m_btnSIEIndexReplace;
	CKEPImageButton m_btnDynamics;
	CKEPImageButton m_btnActorProperties;
	CKEPImageButton m_btnPlaceActor;

	CKEPImageButton m_btnUpdateActor;
	CKEPImageButton m_btnDeactivateActor;
	CKEPImageButton m_btnViewRuntimeStats;
	//	CKEPImageButton m_btnViewRuntimeResources;

	CKEPImageButton m_btnAddItem;
	CKEPImageButton m_btnDeleteItem;
	CKEPImageButton m_btnItemProperties;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_ACTORS_DLG_H_)