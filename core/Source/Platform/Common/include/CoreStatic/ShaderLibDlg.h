///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CShaderLibClass.h"
#include "afxwin.h"
// CShaderLibDlg dialog
#include "KEPEditDialog.h"
#include "KEPImageButton.h"

#include "resource.h"
#include "CollapsibleGroup.h"
#include "ShaderPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

class CShaderLibDlg : public CKEPEditDialog {
	DECLARE_DYNAMIC(CShaderLibDlg)

public:
	CShaderLibDlg(CShaderObjList*& shaderLib,
		CWnd* pParent = NULL); // standard constructor
	virtual ~CShaderLibDlg();

	CShaderObjList*& m_shaderLibRef;
	LPDIRECT3DDEVICE9 m_pd3dDevice;
	BOOL m_shaderTypeMode; //0=vertex 1=pixel
	CStringA m_shaderModeLabel;
	CButton m_fxCode;
	BOOL m_resetRenderState;

	// Dialog Data
	enum { IDD = IDD_DIALOG_SHADERLIBEDITOR };

	// Code can function on two different
	// shader types
	enum SHADER_DIALOG_TYPES { VERTEX = 1,
		PIXEL };
	enum SHADER_DIALOG_MODES { VERTEX_MODE = 0,
		PIXEL_MODE };

	BOOL SetType(UINT dlgType);
	UINT GetType();

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	void ReplaceShader(int selection);

public:
	void InList(int sel);

	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonUpdate();
	afx_msg void OnBnClickedButtonDelete();
	afx_msg void OnBnClickedButtonSaveshaderlib();
	afx_msg void OnBnClickedBtnTestcompile();
	CStringA m_shaderCode;
	CListBox m_shaderList;
	afx_msg void OnLbnSelchangeListshaderlist();
	afx_msg void OnBnClickedButtonloadshaderlib();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCheckFxcheck();
	afx_msg void OnBnClickedEditShader();

private:
	CShaderObj* m_selectedShaderObj;

	CCollapsibleGroup m_colShaders;
	CShaderPropertiesCtrl m_propShader;
	CButton m_grpShaders;

	CCollapsibleGroup m_colShaderCode;
	CButton m_grpShaderCode;

	UINT m_dlgType;

	CKEPImageButton m_btnAddShader;
	CKEPImageButton m_btnDeleteShader;
	CKEPImageButton m_btnReplaceShader;

	void RedrawAllPropertiesCtrls();
	void LoadShaderProperties();
	CShaderObj* GetSelectedShaderObj();
	CShaderObj* GetShaderObj(int index);

private:
	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};
