///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

//object attributes for animation managment
class CAttributeObj : public CObject, public GetSet {
protected:
	CAttributeObj() {}

	DECLARE_SERIAL(CAttributeObj)

public:
	CAttributeObj(
		int L_selfAttributeType,
		int L_selfAttributeValue,
		int L_otherAttributeType,
		int L_otherAttributeValue,
		int L_otherObjectSelect,
		BOOL L_vicinityBased,
		BOOL L_autoRetract,
		BOOL L_inUse,
		int L_retractDelay,
		int L_retractDelayVarial);

	int m_retractDelay;
	int m_retractDelayVarial;
	BOOL m_autoRetract;
	BOOL m_inUse;
	BOOL m_vicinityBased;
	int m_selfAttributeType;
	int m_selfAttributeValue;
	int m_otherAttributeType;
	int m_otherAttributeValue;
	int m_otherObjectSelect;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CAttributeObjList : public CObList {
public:
	CAttributeObjList(){};

	DECLARE_SERIAL(CAttributeObjList)
};

} // namespace KEP