/******************************************************************************
 InformationDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "afxdhtml.h"
#include "resource.h"

// CInformationDlg dialog

class CInformationDlg : public CDHtmlDialog {
	DECLARE_DYNCREATE(CInformationDlg)

public:
	CInformationDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CInformationDlg();
	// Overrides

	// Dialog Data
	enum { IDD = IDD_DIALOG_INFORMATION,
		IDH = IDR_HTML_INFORMATIONDLG };

	void LoadHelp(UINT dlgId);

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	DECLARE_DHTML_EVENT_MAP()
};
