///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "../../RenderEngine/ReMaterial.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class ReMesh;
class IMemSizeGadget;
class CEXMeshObj;

// Note this class is now hardcoded to Look in CharacterTextures/Extended.
class CHiarcleVisObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CHiarcleVisObj, VERSIONABLE_SCHEMA | 1);

public:
	CHiarcleVisObj();

	CEXMeshObj* m_mesh;
	ReMesh* m_reMesh;
	BOOL m_isCloned;
	ReLightCache m_lightCache;
	Matrix44f m_mat;

	// GPU shader matrices
	Matrix44fA16 m_WorldViewProjMatrix;
	Matrix44fA16 m_WorldInvTransposeMatrix;

	void Optimize();
	void Clone(CHiarcleVisObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void SafeDelete();
	void Serialize(CArchive& ar);

	void SetReMesh(ReMesh* reMesh) {
		m_reMesh = reMesh;
	}
	ReMesh* GetReMesh() {
		return m_reMesh;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CHiarcleVisList : public CObList {
public:
	CHiarcleVisList(){};
	DECLARE_SERIAL(CHiarcleVisList);

	CHiarcleVisObj* GetByIndex(int index);
	void Clone(CHiarcleVisList** clone, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Optimize();
	void SafeDelete();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CHiarchyMesh : public CObject, public GetSet {
public:
	CHiarchyMesh();
	DECLARE_SERIAL(CHiarchyMesh);

	CHiarcleVisList* m_lodChain;
	CStringA m_name;

	CHiarcleVisObj* GetByIndex(int index);
	void Load(CStringA filename);
	void Save(CStringA filename);
	BOOL SetLevel(int index, CStringA filename);
	BOOL SaveLevel(int index, CStringA filename);
	void SafeDelete();
	void Clone(CHiarchyMesh** clone, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void SetFadeIn(TimeMs duration);
	void SetFadeOut(TimeMs duration);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

typedef CHiarchyMesh CHierarchyMesh;
typedef CHiarcleVisObj CHierarchyVisObj;

} // namespace KEP
