///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "OALSoundBuffer.h"

// Ogg/Vorbis Includes
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

#define NUM_BUFFERS 2

namespace KEP {

class OALOggStream : public OALSoundBuffer {
public:
	OALOggStream();
	~OALOggStream();

	bool OpenOggStream(const std::string& url);

	bool Stream(ALuint buffer);

	bool Update(ALuint source);

	bool Play(ALuint source);

	bool EmptyQueue(ALuint source);

protected:
	bool OggStream(ALuint buffer); //reload buffer

private:
	OggVorbis_File m_oggStream;
	vorbis_info* m_vorbisInfo;
	vorbis_comment* m_vorbisComment;

	ALuint m_buffers[NUM_BUFFERS]; //front and back buffers
	ALenum m_format; //internal format
};

} // namespace KEP