///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPCommon.h"
#include "Core/Util/CArchiveObjectSchema.h"

#if (DRF_OLD_PORTALS == 1)

#include <GetSet.h>
#include "InstanceId.h"
#include "KEPObList.h"
#include "CStructuresMisl.h"

namespace KEP {

class CPortalSysObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CPortalSysObj, VERSIONABLE_SCHEMA | 3);

public:
	CPortalSysObj();

	CStringA m_portalName;
	int m_keyRequired; //0=no key required

	//location
	ZoneIndex m_targetZoneIndex;
	Vector3f m_wldPosition;
	int m_rotation;

	//area
	ABBOX m_portalZone;
	ChannelId m_fromChannel;

	BOOL BoundBoxPointCheck(Vector3f& point, ABBOX& box);
	BOOL InZone(Vector3f currentLocation, const ChannelId& channel);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CPortalSysList : public CKEPObList {
public:
	CPortalSysList(){};
	void SafeDelete();

	DECLARE_SERIAL(CPortalSysList);

	CPortalSysObj* ReturnPortal(Vector3f currentPosition, const ChannelId& channel);
};

} // namespace KEP

#else
void CPortalSysClass_4221();
#endif