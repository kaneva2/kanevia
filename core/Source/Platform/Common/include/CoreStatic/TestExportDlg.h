/******************************************************************************
 TestExportDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// CTestExportDlg dialog

class CTestExportDlg : public CDialog {
	DECLARE_DYNAMIC(CTestExportDlg)

public:
	CTestExportDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CTestExportDlg();

	// Dialog Data
	enum { IDD = IDD_DIALOG_TESTEXPORT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
