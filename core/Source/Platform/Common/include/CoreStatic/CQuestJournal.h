///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CABFunctionLib.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CGlobalInventoryObjList;
class CInventoryObjList;

class CQJRequiredItem : public CObject, public GetSet {
	DECLARE_SERIAL(CQJRequiredItem);

public:
	CQJRequiredItem();

	CStringA m_itemName;
	int m_glid;
	int m_quanity;

	void Clone(CQJRequiredItem** clone);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CQJRequiredItemList : public CObList {
public:
	CQJRequiredItemList(){};

	void Clone(CQJRequiredItemList** clone);
	CQJRequiredItem* GetByID(int id);
	void SafeDelete();
	DECLARE_SERIAL(CQJRequiredItemList);
};

class CQuestJournalObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CQuestJournalObj, VERSIONABLE_SCHEMA | 3);

public:
	CQuestJournalObj();

	CStringA m_questDescription;
	CStringA m_npcName;
	CStringA m_keyStatement;
	int m_currentProgression;
	int m_status;
	int m_mobToHuntID;
	int m_maxDropCount;
	int m_dropGlid;
	int m_dropPercentage;
	int m_lastSelectionIndex;
	BOOL m_undeletable;

	BOOL m_autoComplete; //for hunting quests
	int m_skillExpType;
	float m_expBonus;
	float m_thisGainCap;

	CQJRequiredItemList* m_requiredItems;

	void Clone(CQuestJournalObj** clone);
	void SafeDelete();
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CQuestJournalList : public CObList {
public:
	CQuestJournalList(){};
	void Clone(CQuestJournalList** clone);
	BOOL DeleteEntryByNpcName(CStringA name);
	BOOL LogQuest(CStringA description,
		CInventoryObjList* requiredItems,
		CStringA m_npcName,
		CStringA m_keyStatement,
		CGlobalInventoryObjList* gllist,
		BOOL succeeded,
		int curVisibleSelection,
		int logOption,
		int mobToHuntID,
		int maxDropCount,
		int dropGlid,
		int dropPercentage,
		BOOL autoComplete,
		int skillExpType,
		float expBonus,
		float thisGainCap,
		BOOL isProgressive,
		BOOL undeletable,
		CStringA closeJournalEntryByName);
	void MakeMostRecentBySeer(CStringA seerName);
	CQuestJournalObj* GetMostRecentUncompleted();
	void RemoveBySeer(CStringA seerName);
	CQuestJournalObj* GetBySeer(CStringA seerName);
	BOOL BuildRequiredList(CQuestJournalObj* newPtr, CInventoryObjList* requiredItems, CGlobalInventoryObjList* gllist, int logOption);
	CQuestJournalObj* GetUncompletedQuestJournalEntryByTarget(int aiCfgId);
	void SafeDelete();

	DECLARE_SERIAL(CQuestJournalList);
};

} // namespace KEP