/******************************************************************************
 AITriggeredSpawnsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_AI_TRIGGERED_SPAWNS_PROPERTIESCTRL_H_)
#define _AI_TRIGGERED_SPAWNS_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AITriggeredSpawnsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CAITriggeredSpawnsPropertiesCtrl window

class CAITriggeredSpawnsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CAITriggeredSpawnsPropertiesCtrl();
	virtual ~CAITriggeredSpawnsPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetName() const {
		return Utf16ToUtf8(m_sName).c_str();
	}
	long GetDuration() const {
		return m_duration;
	}
	long GetMandatoryWait() const {
		return m_mandatoryWait;
	}
	float GetBonusPoints() const {
		return m_bonusPoints;
	}
	float GetPercentSpawn() const {
		return (float)(m_percentSpawn * PERCENT_SPAWN_SCALE_FACTOR);
	}
	float GetNotificationRadius() const {
		return m_notificationRadius;
	}
	float GetNotificationPosX() const {
		return m_notificationPosX;
	}
	float GetNotificationPosY() const {
		return m_notificationPosY;
	}
	float GetNotificationPosZ() const {
		return m_notificationPosZ;
	}
	int GetNotificationChannelID() const {
		return m_notificationChannelID;
	}
	CStringA GetWarningMessage() const {
		return Utf16ToUtf8(m_sWarningMessage).c_str();
	}
	BOOL GetManualStart() const {
		return m_manualStart;
	}

	// modifiers
	void SetName(CStringA sName) {
		m_sName = Utf8ToUtf16(sName).c_str();
	}
	void SetDuration(long duration) {
		m_duration = duration;
	}
	void SetMandatoryWait(long mandatoryWait) {
		m_mandatoryWait = mandatoryWait;
	}
	void SetBonusPoints(float bonusPoints) {
		m_bonusPoints = bonusPoints;
	}
	void SetPercentSpawn(float percentSpawn) {
		m_percentSpawn = (int)(percentSpawn / PERCENT_SPAWN_SCALE_FACTOR);
	}
	void SetNotificationRadius(float notificationRadius) {
		m_notificationRadius = notificationRadius;
	}
	void SetNotificationPosX(float notificationPosX);
	void SetNotificationPosY(float notificationPosY);
	void SetNotificationPosZ(float notificationPosZ);
	void SetNotificationChannelID(int notificationChannelID) {
		m_notificationChannelID = notificationChannelID;
	}
	void SetWarningMessage(CStringA sWarningMessage) {
		m_sWarningMessage = Utf8ToUtf16(sWarningMessage).c_str();
	}
	void SetManualStart(BOOL b) {
		m_manualStart = b;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAITriggeredSpawnsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CAITriggeredSpawnsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_DURATION,
		INDEX_MANDATORY_WAIT,
		INDEX_BONUS_POINTS,
		INDEX_PERCENT_SPAWN,
		INDEX_NOTIFICATION,
		INDEX_RADIUS,
		INDEX_POSITION,
		INDEX_X,
		INDEX_Y,
		INDEX_Z,
		INDEX_CHANNEL_ID,
		INDEX_WARNING_MESSAGE,
		INDEX_MANUAL_START
	};

	const static int PERCENT_SPAWN_SCALE_FACTOR = 10;
	const static int PERCENT_SPAWN_MIN = 0;
	const static int PERCENT_SPAWN_MAX = 100;

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	void InitChannels();

private:
	CStringW m_sName;
	long m_duration;
	long m_mandatoryWait;
	float m_bonusPoints;
	int m_percentSpawn;

	float m_notificationRadius;
	float m_notificationPosX;
	float m_notificationPosY;
	float m_notificationPosZ;
	int m_notificationChannelID;
	CStringW m_sWarningMessage;
	BOOL m_manualStart;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_AI_TRIGGERED_SPAWNS_PROPERTIESCTRL_H_)
