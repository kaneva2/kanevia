///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CMaterialClass.h"

namespace KEP {

class CEnvironmentObj;

class CDayStateObj : public CObject, public GetSet {
	DECLARE_SERIAL(CDayStateObj);

public:
	CDayStateObj(CEnvironmentObj* _baseEnvironment = NULL);

	//fog
	float m_fogStartRange;
	float m_fogEndRange;
	float m_fogColorRed;
	float m_fogColorGreen;
	float m_fogColorBlue;

	//length of day section
	long m_duration; //in milliseconds

	//lighting sun ect
	float m_sunRotationX; //enviormental center
	float m_sunRotationY; //enviormental center
	long m_sunDistance; //vector distance from center
	float m_sunColorRed; //0 is basically sun off no color
	float m_sunColorGreen; //0 is basically sun off no color
	float m_sunColorBlue; //0 is basically sun off no color
	float m_ambientColorRed;
	float m_ambientColorGreen;
	float m_ambientColorBlue;

	//modifiers
	CMaterialObject* m_materialModifier;

	void Serialize(CArchive& ar);
	void SafeDelete();

	DECLARE_GETSET
};

class CDayStateObjList : public CObList {
public:
	CDayStateObjList();
	void SafeDelete();

	DECLARE_SERIAL(CDayStateObjList);

	CDayStateObj* m_lastDayState;

	long GetCompleteCycleLength();
	CDayStateObj* GetCurrentDayState(TimeMs currentDayTime);
	BOOL GetCurrentDayState(TimeMs currentDayTime,
		CDayStateObj** currentState,
		CDayStateObj** targetState,
		float* currentDuration,
		float* currentTimePassed);
};

} // namespace KEP