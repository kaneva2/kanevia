///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common\KEPUtil\Helpers.h"

#include "GetSet.h"

#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include "UseTypes.h"
#include "PassGroups.h"
#include "KEPObList.h"
#include "KEPConstants.h"
#include <string>

namespace KEP {

class IMemSizeGadget;
class CCExpansiveObjList;
class CElementStatusList;
class CMaterialObject;
class CStatObjList;
class PassList;

class CStatBonusObj : public CObject {
	DECLARE_SERIAL(CStatBonusObj);

public:
	CStatBonusObj(int id = 0, int bonus = 0);

	int m_statID;
	int m_statBonus;

	void Serialize(CArchive& ar);
	void Clone(CStatBonusObj** clone);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CStatBonusList : public CKEPObList {
public:
	CStatBonusList(){};
	void SafeDelete();

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CStatBonusList);

	// compare passed in requirements to this bonus list to make sure
	// the requirements meet the min requirements of *this* list
	bool MeetsRequirements(CStatObjList& requirements);

	int GetBonusByID(int id);
	void Clone(CStatBonusList** clone);
};

class CAttachmentRefObj : public CObject, public GetSet {
	DECLARE_SERIAL(CAttachmentRefObj);

public:
	CAttachmentRefObj(UINT64 type = 0);

	/**
	 * Acquire a string representation of this CItemObj suitable for
	 * reporting.
	 */
	std::string toString() const;

	UINT m_attachmentType;

	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CAttachmentRefList : public CKEPObList {
public:
	CAttachmentRefList(){};
	void SafeDelete();

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CAttachmentRefList);

	/**
	 * Acquire a string representation of this CItemObj suitable for
	 * reporting.
	 */
	std::string toString() const;
#ifdef DEPLOY_CLEANER
	std::string toXML() const;
#else
#endif
	void Clone(CAttachmentRefList** clone);
};

#define INVALID_GLID GLID_INVALID

inline bool CheckMountSlotConflict(int slot1, int slot2) {
	return ((slot1 == slot2) || (IS_HEAD_ITEM(slot1) && IS_HEAD_ITEM(slot2)) || (IS_HAIR_ITEM(slot1) && IS_HAIR_ITEM(slot2)));
}

class CItemObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CItemObj, VERSIONABLE_SCHEMA | 12);

public:
	CItemObj();

	/**
	 * Copy constructor
	 */
	CItemObj(const CItemObj& toCopy);

	~CItemObj();

	/**
	 * Acquire a string representation of this CItemObj suitable for
	 * reporting.
	 */
	std::string toString() const;
#ifdef DEPLOY_CLEANER
	std::string toXML() const;
#else
#endif

	// DRF - Added - Shorter then tostring() for logging.
	std::string ToStr() const {
		std::string str;
		StrBuild(str,
			"glid<" << m_globalID
					<< (PassGroupAP() ? " AP" : "")
					<< (PassGroupVIP() ? " VIP" : "")
					<< "> '" << m_itemName << "'");
		return str;
	}

	/**
	 * Overload of the assignment operator.
	 */
	CItemObj& operator=(const CItemObj& obj);

	/**
	 * Give a base item, derive appropriate properties
	 * from the derivative item.
	 */
	CItemObj& CItemObj::derivate(const CItemObj& newItem);

	/**
	 * Given a derived item, inherit appropriate properties
	 * from the base item.
	 */
	CItemObj& CItemObj::inheritFrom(const CItemObj& baseItem);

	CStringA m_itemName;
	CAttachmentRefList* m_attachmentRefList;
	int m_marketCost;
	int m_sellingPrice;
	BOOL m_equiped;
	BOOL m_disarmable;
	GLID m_globalID;

	USE_TYPE m_useType;

	int m_miscUseValue;

	// DRF - Added
	int m_passGroup; // bitmapped PASS_GROUP (1=AP, 2=VIP)
	bool PassGroup(int passGroup) const {
		return ((m_passGroup & passGroup) != 0);
	}
	bool PassGroupAP() const {
		return PassGroup(PASS_GROUP_AP);
	}
	bool PassGroupVIP() const {
		return PassGroup(PASS_GROUP_VIP);
	}

	//----------charges
	int m_chargeType; //specify the missile index
	int m_chargeCurrent; //current charges
	int m_maxCharges; //maximum charges on item
	//----------
	int m_skillUseRef;
	int m_minimumLevel;
	int m_masteredLevel;

	int m_animAccessValue;

	BOOL m_permanent;
	BOOL m_raceID; //any race can arm it(race id == 0) other must match

	int m_dbSrcIndex;
	BOOL m_nonDrop;
	BOOL m_nonTransferable;
	BOOL m_destroyOnUse;

	int m_keyID;
	BOOL m_stackable;

	int m_defensesSpecific; //reference to missile DB

	//skills additional
	int m_requiredSkillType;
	int m_requiredSkillLevel;

	TimeMs m_creationTimeStamp;
	int m_timoutValue;

	int m_visualRef;
	float m_yJetforce;
	float m_zJetforce;

	//"infinite" dim configs
	struct dimConfig {
		short dimCfgIndex;
		short dimCfgChangeTo;
		short dimCfgChangeBack;
		short dimMaterial;
		BOOL dimMatPreserve;

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	};

	typedef std::vector<dimConfig> DimVector;

	DimVector m_dimConfigurations;

	std::vector<int> m_exclusionGroupIDs;

	// DRF - Returns true if exclusion conflicts with given item or false if no conflict.
	bool Excludes(const CItemObj* pIO) const {
		if (!pIO)
			return false;
		for (auto it1 = m_exclusionGroupIDs.begin(); it1 != m_exclusionGroupIDs.end(); ++it1)
			for (auto it2 = pIO->m_exclusionGroupIDs.begin(); it2 != pIO->m_exclusionGroupIDs.end(); ++it2)
				if (CheckMountSlotConflict(*it1, *it2))
					return true;
		return false;
	}

	typedef std::vector<CItemObj*> itemVector;

	int m_reloadTime;
	int m_actorGroup;

	CStringA m_customTexture; // mphears: don't think this does anything now
	CMaterialObject* m_materialOverride; // not serialized; overrides all material dim flips
	int m_expiredDuration; // not serialized.
	GLID m_baseGlid; // not serialized.

	CStatBonusList* m_statBonuses;
	CStatBonusList* m_requiredStats;
	CElementStatusList* m_elementDefenses;
	CCExpansiveObjList* m_expansiveCollection;

	PassList* m_passList; // list of passes this item holds
	bool m_defaultArmedItem; // this is *the* default item for the slot
	bool m_useAnywhere; // this item has security, but can be used anywhere

	CStringA m_itemDescription;
	USHORT m_itemType; // currently normal (0x100) or gift (0x200)

	bool m_derivable; // derivable flag - dont' serialize this yet, only get it from DB
	std::vector<int> m_animations; // If this item is animatable, it might have some valid animations

	void SerializeAlloc(CArchive& ar);
	void Clone(CItemObj** clone);
	void CloneFromBaseGLID(CItemObj** clone, CItemObj* newItemObj, int global_id);
	void Serialize(CArchive& ar);
	inline void SerializeDimCfgsOut(CArchive& ar);
	inline void SerializeDimCfgsIn(CArchive& ar);

	bool MatchActorGroup(int actorGroup) const {
		return m_actorGroup == 0 || m_actorGroup == actorGroup;
	}

	bool MatchActorGroups(int numGroups, const int* actorGroups) const {
		for (int i = 0; i < numGroups; i++)
			if (MatchActorGroup(actorGroups[i]))
				return true;
		return false;
	}

	bool ParseExclusionGroupsCSV(const std::string& sExclusionGroups);

	/**
	 * Set the creation time stamp of this CInventoryObj.
	 * Encapsulates access to ceateTimeStamp.
	 */
	CItemObj& stampTime() {
		m_creationTimeStamp = fTime::TimeMs();
		return *this;
	}

	static Logger m_logger;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CItemObjList : public CKEPObList {
public:
	CItemObjList() {}
	virtual ~CItemObjList() {}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CItemObjList);

	void SafeDelete();
	CItemObj* GetByGLID(const GLID& glid);
	void Clone(CItemObjList** clone);
};

} // namespace KEP