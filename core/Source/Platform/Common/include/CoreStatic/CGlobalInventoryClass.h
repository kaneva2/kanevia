///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

// added by serialize.pl
#include <GetSet.h>
#include "CEXMeshClass.h"
#include "CABFunctionLib.h"
#include "CItemClass.h"
#include "ListItemMap.h"
#include "KEPObList.h"

#include "..\..\keputil\KGenericObjList.h"
#include "common/keputil/Throttle.h"

#include <jsThread.h>

#include "common\keputil\traversable.h"
#ifdef DEPLOY_CLEANER
#include "common/include/GILCleaner.h"
#else
#endif
#include "common/include/CoreStatic/IKEPGameFile.h"

namespace KEP {

class CGlobalInventoryObj : public CObject, public GetSet {
	DECLARE_SERIAL(CGlobalInventoryObj);

public:
	CGlobalInventoryObj();

	GLID glid() const {
		return m_itemData->m_globalID;
	}

	CEXMeshObj* m_visual;
	CItemObj* m_itemData;

	void SafeDelete();
	void Serialize(CArchive& ar);
	std::string toString() const;
#ifdef DEPLOY_CLEANER
	std::string toXML() const;
#else
#endif
	DECLARE_GETSET
};

class CGlobalInventoryObjList; // forward reference

/**
 * Instances of CGlobalInventoryObjList are not part of the CObject inheritance
 * chain, however, they can be serialized/deserialized.  This class provides this
 * ability.  It is marked as serializable by virtue of the use of the DECLARE_SERIAL
 * macro.  However, for backward compatibility, instances of this class must be
 * invoked when deserializing GILs.  To this end, the implementation of this class does
 * not use the IMPLEMENT_SERIAL macro, but rather, this functionality is defined explicitly
 * (See CGlobalInventoryClass.cpp)
 */
class GILSerializer : public CKEPObList {
public:
	GILSerializer();
	GILSerializer(CGlobalInventoryObjList& store);
	virtual ~GILSerializer();
	void Serialize(CArchive& ar);
	DECLARE_SERIAL(GILSerializer);
	GILSerializer& setStore(CGlobalInventoryObjList& store);

	/**
	 * One of artifacts of the deserialization process is that CArchive allocates memory for the new
	 * objects on the stack.  To insure that memory is cleaned up, GILSerializer will free the
	 * the allocated CGlobalInventoryObjList that it allocated if this call has not been made.
	 * Once it has been made, it is incumbent on the call to free the list (or not).
	 */
	CGlobalInventoryObjList* retrieveList();

private:
	int m_manageCleanup;
	CGlobalInventoryObjList* m_store;
};

class GlobalInventoryListHelpers {
public:
	class LegacyAttachmentTypeSelector : public Selector<CGlobalInventoryObj> {
	public:
		LegacyAttachmentTypeSelector(int vtype) :
				type(vtype) {}

		virtual bool evaluate(CGlobalInventoryObj& obj) {
			if (!obj.m_itemData)
				return false;
			if (!obj.m_itemData->m_attachmentRefList)
				return false;

			for (POSITION posAtt = obj.m_itemData->m_attachmentRefList->GetHeadPosition(); posAtt != NULL;) {
				CAttachmentRefObj* pRef = (CAttachmentRefObj*)obj.m_itemData->m_attachmentRefList->GetNext(posAtt);
				if (!pRef)
					continue;
				if (pRef->m_attachmentType != type)
					continue;
				glid = obj.m_itemData->m_globalID;
				return true;
			}

			return false;
		}

		UINT type;
		GLID glid;
	};

	/**
	 * This traverser is used to collect all gio in a collection
	 * and place them into a map keyed by glid.
	 */
	class ByIdCollector : public Traverser<CGlobalInventoryObj> {
	public:
		virtual bool evaluate(CGlobalInventoryObj& obj) {
			itemMap.insert(ListItemMap::value_type(obj.m_itemData->m_globalID, (const char*)obj.m_itemData->m_itemName));
			return false;
		}

		ListItemMap itemMap;
	};

	/**
	 * This traverser is used to collect all gio in a collection
	 * and place them into a map keyed by their position in the
	 * collection.
	 */
	class ByIndexCollector : public Traverser<CGlobalInventoryObj> {
	public:
		ByIndexCollector() :
				index(0) {}
		virtual bool evaluate(CGlobalInventoryObj& obj) {
			itemMap.insert(ListItemMap::value_type(index++, (const char*)obj.m_itemData->m_itemName));
			return false;
		}
		int index;
		ListItemMap itemMap;
	};

	/**
	 * This traverser is used to free the resources held by each item in the collection.
	 */
	class GILDeleter : public Traverser<CGlobalInventoryObj> {
	public:
		virtual bool evaluate(CGlobalInventoryObj& obj) {
			obj.SafeDelete(); // The delete operator on gio does not actually clean up allocations.  This method does.
			delete &obj; // then actuall delete the gio
			return false; // We want to iterate over every item in the collection, so return false.
		}
	};

	/**
	 * Selects a GLID based gil and returns the index.  This method may be the first that would require
	 * use to allow specification of the index to be used.
	 */
	class GLIDSelector : public Selector<CGlobalInventoryObj>, public Deleter<CGlobalInventoryObj> {
	public:
		GLIDSelector(const GLID& glid) :
				m_glid(glid), m_index(-1) {}
		virtual bool evaluate(CGlobalInventoryObj& candidate) {
			m_index++;
			return (candidate.m_itemData->m_globalID == m_glid);
		}

		GLID m_glid;
		int m_index;
	};
};

class GlobalInventoryList {
public:
	virtual ~GlobalInventoryList() {}
	virtual GlobalInventoryList& append(CGlobalInventoryObj& invObj) = 0;
	virtual CGlobalInventoryObj* at(INT_PTR nIndex) = 0;
	virtual void CheckForDuplicates() = 0;
	virtual bool DeleteByGLID(const GLID& glid) = 0;
	virtual CGlobalInventoryObj* GetByGLID(const GLID& glid) = 0;
	virtual ListItemMap GetItems() = 0;
	virtual ListItemMap GetItemsById() = 0;
	virtual GLID GetGLIDByLegacyAttachmentType(int type) = 0;
	virtual size_t GetSize() const = 0;
	virtual size_t GetCount() const = 0;
	virtual int indexOfGLID(const GLID& glid) = 0;
	virtual CGlobalInventoryObj* removeItemAt(int index) = 0;
	virtual CGlobalInventoryObj* removeItemWithGLID(const GLID& glid) = 0;
	virtual void SafeDelete() = 0;
	virtual CGlobalInventoryObj* select(Selector<CGlobalInventoryObj>& selector) = 0;
	virtual Traversable<CGlobalInventoryObj>& traverse(Traverser<CGlobalInventoryObj>& traverser) = 0;
};

/**
 * Singleton, global accessible collection of CGlobalInventoryObj instances.
 */
class CGlobalInventoryObjList {
public:
	typedef Deleter<CGlobalInventoryObj> DeleterT;
	typedef Selector<CGlobalInventoryObj> SelectorT;
	typedef Traverser<CGlobalInventoryObj> TraverserT;

	// The following typedefs are for backward compatibility
	// and are deprecated use T postfixed typedefs above.
	//
	/**
	 * @deprecated use DeleterT
	 */
	typedef Deleter<CGlobalInventoryObj> Deleter;

	/**
	 * @deprecated use SelectorT
	 */
	typedef Selector<CGlobalInventoryObj> Selector;

	/**
	 * @deprecated use TraverserT
	 */
	typedef Traverser<CGlobalInventoryObj> Traverser;

	CGlobalInventoryObjList& traverse(TraverserT& traverser);
	CGlobalInventoryObjList& traverse(TraverserT&& traverser) {
		return traverse(traverser); // Allow calling traverse with a temporary.
	}
	CGlobalInventoryObj* select(SelectorT& selector);

	class GLIDSelector : public Selectable<CGlobalInventoryObj>::SelectorT, public Deletable<CGlobalInventoryObj>::DeleterT {
	public:
		GLIDSelector(const GLID& glid) :
				m_glid(glid), m_index(-1) {}
		virtual bool evaluate(CGlobalInventoryObj& candidate) {
			m_index++;
			return (candidate.m_itemData->m_globalID == m_glid);
		}

		GLID m_glid;
		int m_index;
	};

	CGlobalInventoryObjList& CGlobalInventoryObjList::copyFrom(CGlobalInventoryObjList& source);

	static CGlobalInventoryObjList* GetInstance();
#ifdef DEPLOY_CLEANER
	GILCleaner* _cleaner;
	GILCleaner* cleaner() {
		return _cleaner;
	}
	CGlobalInventoryObjList& setCleaner(GILCleaner& cleaner) {
		_cleaner = &cleaner;
		return *this;
	}
#else
#endif
	void SafeDelete();

	virtual ~CGlobalInventoryObjList();

	/**
	 * The path of the file this object was lasted serialized to or deserialized from
	 */
	CStringA GetLastSavedPath() const {
		return m_lastSavedPath;
	}

	void SetLastSavedPath(const CStringA& path) {
		m_lastSavedPath = path;
	}

	/**
	 * Whether the object's state has changed and requires saving
	 */
	BOOL IsDirty() const {
		return m_dirty;
	}

	/**
	 * It will be better if this state is updated automatically, but having public member variables
	 * in classes derive from CKEPObList makes it impossible.
	 * So, to make it work, SetDirty() needs to be called manually whenever the object is changed
	 */
	void SetDirty(BOOL dirty = TRUE) {
		m_dirty = dirty;
	}

	int indexOfGLID(const GLID& glid);

	CGlobalInventoryObj* GetByGLID(const GLID& glid);

	// Remove from implementation?  This function will not be of value for some implementations (e.g. hashmap)
	// ...but.  The collection should know about that so in the meantime, leave this at the implementation
	// level.
	// Synchronize
	void CheckForDuplicates();

	ListItemMap GetItemsById();

	ListItemMap GetItems();

	bool DeleteByGLID(const GLID& glid);

	// Allocate and return a new GLID
	GLID AllocateGLID();

	// REMOVE FROM IMPLEMENTATION LAYER.
	// Reserve a GLID without creating an item (other module should call AllocateGLID first to obtain the GLID then remember it
	//  and call reservation every time Client/Editor starts). Reservation is currently not saved by CGlobalInventoryObjList
	void ReserveExternalGLID(const GLID& glid);

	// Convert legacy APD type to GLID
	GLID GetGLIDByLegacyAttachmentType(int type);

	void stringToFileWithName(const std::string& fname) {
		CStdioFile file;
		file.Open(Utf8ToUtf16(fname).c_str(), CFile::modeWrite | CFile::modeCreate);
		std::string s1 = toString();
		file.Write(s1.c_str(), s1.size());
		file.Flush();
		file.Close();
	}

	std::string toString();
#ifdef DEPLOY_CLEANER
	std::string toXML();
#else
#endif
	// Added in support of implementing CObList interface
	//
	/**
	 * @deprecated.  Use append()
	 */
	CGlobalInventoryObjList& AddTail(CGlobalInventoryObj* invObj);

	CGlobalInventoryObjList& append(CGlobalInventoryObj& invObj);

	INT_PTR GetSize() const {
		return impl().GetSize();
	}

	INT_PTR GetCount() const {
		return impl().GetSize();
	}

	CGlobalInventoryObj* at(INT_PTR nIndex);

	CGlobalInventoryObj* removeItemWithGLID(const GLID& glid);

	CGlobalInventoryObj* removeItemAt(int index);

	/**
	 * This method was introduced for testing/debugging purposes but clearly can be used
	 * in production.
	 * @param fileName The name of the file to which the gil will be saved.  Note this file,
	 *                 if it already exists will be overwritten.
	 */
	void serializeToFileWithName(const char* fileName);

	IKEPGameFile& gameFileAdapter() {
		return _gameFile;
	}

private:
	/**
	 * An implementation of IKEPGameFile that adapts CGlobalObjList
	 */
	class GILGameFileAdapter : public IKEPGameFile {
	public:
		GILGameFileAdapter(CGlobalInventoryObjList& content) :
				_content(content) {}
		virtual ~GILGameFileAdapter() {}

		std::string GetFileName() {
			return "";
		}
		BOOL IsDirty() {
			return _content.IsDirty();
		}
		BOOL SaveFile(const std::string& fileName) {
			_content.serializeToFileWithName(fileName.c_str());
			return true;
		}

	private:
		CGlobalInventoryObjList& _content;
	};

	CGlobalInventoryObjList();
	static CGlobalInventoryObjList* m_instance;
	static Logger m_logger;

	// It's not enough to simply synchronize access to the instance,
	// We also need to insure that RAII implementation GetInstance()
	// is synchronized. This sync object will help with that.
	//
	static fast_recursive_mutex m_initSync;
	CStringA m_lastSavedPath;
	BOOL m_dirty;
	GLID m_nextGLID;
	GLID m_lastAllocatedGLID;
	fast_recursive_mutex m_sync;

	inline GlobalInventoryList& impl() const {
		return *m_impl;
	}
	GlobalInventoryList* m_impl;
	CountThrottle m_reportingThrottle;
	GILGameFileAdapter _gameFile;
	friend class GILSerializer;
};

} // namespace KEP