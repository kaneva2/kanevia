/******************************************************************************
 WeaponPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_WEAPONPROPERTIESCTRL_H_)
#define _WEAPONPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WeaponPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "GWCPropertyMultiCheckboxLook.h"
#include "resource.h"
#include "CElementsClass.h"
#include "CSkillClass.h"

/////////////////////////////////////////////////////////////////////////////
// CWeaponPropertiesCtrl window

class CWeaponPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CWeaponPropertiesCtrl();
	virtual ~CWeaponPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	void LoadCriteriaOfSkillOptions(CSkillObjectList* skillDatabase);
	void LoadDamageChannelOptions(CElementObjList* elementDB);
	void InitExplosions();
	void InitParticles();

	// accessors
	CStringA GetMissileName() const {
		return Utf16ToUtf8(m_missileName).c_str();
	}
	TimeMs GetLifeTime() const {
		return m_lifeTime;
	}
	int GetDamageValue() const {
		return m_damageValue;
	}
	int GetParticleLink() const {
		return m_particleLink;
	}
	int GetExplosionLink() const {
		return m_explosionLink;
	}
	int GetAbilityType() const {
		return m_abilityType;
	}
	int GetSkillType() const {
		return m_skillType;
	}
	int GetDamageSpecific() const {
		return m_damageSpecific;
	}
	int GetBaseKickPower() const {
		return m_baseKickPower;
	}
	float GetCollisionRadius() const {
		return m_collisionRadius;
	}
	float GetHeatSeekingPower() const {
		return m_heatSeekingPower;
	}
	float GetPushPower() const {
		return m_pushPower;
	}

	int GetCriteriaOfSkillId() const {
		return m_criteriaOfSkillId;
	}
	int GetDamageChannelId() const {
		return m_damageChannelId;
	}

	BOOL GetForceExplodeAtEnd() const {
		return m_forceExplodeAtEnd;
	}
	BOOL GetOrientateToCamera() const {
		return m_orientateToCamera;
	}
	BOOL GetUseElastics() const {
		return m_useElastics;
	}
	BOOL GetUseFullPhysics() const {
		return m_useFullPhysics;
	}
	BOOL GetBurstForceType() const {
		return m_burstForceType;
	}
	BOOL GetFilterCollision() const {
		return m_filterCollision;
	}

	// modifiers

	void SetMissileName(CStringA missileName) {
		m_missileName = Utf8ToUtf16(missileName).c_str();
	}
	void SetLifeTime(TimeMs lifeTime) {
		m_lifeTime = lifeTime;
	}
	void SetDamageValue(int damageValue) {
		m_damageValue = damageValue;
	}
	void SetParticleLink(int particleLink) {
		m_particleLink = particleLink;
	}
	void SetExplosionLink(int explosionLink) {
		m_explosionLink = explosionLink;
	}
	void SetAbilityType(int abilityType) {
		m_abilityType = abilityType;
	}
	void SetSkillType(int skillType) {
		m_skillType = skillType;
	}
	void SetDamageSpecific(int damageSpecific) {
		m_damageSpecific = damageSpecific;
	}
	void SetBaseKickPower(int baseKickPower) {
		m_baseKickPower = baseKickPower;
	}
	void SetCollisionRadius(float collisionRadius) {
		m_collisionRadius = collisionRadius;
	}
	void SetHeatSeekingPower(float heatSeekingPower) {
		m_heatSeekingPower = heatSeekingPower;
	}
	void SetPushPower(float pushPower) {
		m_pushPower = pushPower;
	}

	void SetCriteriaOfSkillId(int criteriaOfSkillId) {
		m_criteriaOfSkillId = criteriaOfSkillId;
	}
	void SetDamageChannelId(int damageChannelId) {
		m_damageChannelId = damageChannelId;
	}

	void SetForceExplodeAtEnd(BOOL forceExplodeAtEnd) {
		m_forceExplodeAtEnd = forceExplodeAtEnd;
	}
	void SetOrientateToCamera(BOOL orientateToCamera) {
		m_orientateToCamera = orientateToCamera;
	}
	void SetUseElastics(BOOL useElastics) {
		m_useElastics = useElastics;
	}
	void SetUseFullPhysics(BOOL useFullPhysics) {
		m_useFullPhysics = useFullPhysics;
	}
	void SetBurstForceType(BOOL burstForceType) {
		m_burstForceType = burstForceType;
	}
	void SetFilterCollision(BOOL filterCollision) {
		m_filterCollision = filterCollision;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWeaponPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CWeaponPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_PARTICLE_LINK,
		INDEX_DAMAGE,
		INDEX_HEAT_SEEKING_POWER,
		INDEX_EXPLOSION_LINK,
		INDEX_COLLISION_RADIUS,
		INDEX_LIFE_TIME,
		//		INDEX_SKILL_USE_TYPE,	// NO LONGER USED
		//	    INDEX_ABILITY_USE_TYPE,	// NO LONGER USED
		INDEX_PUSH_POWER,
		//		INDEX_DAMAGE_SPECIFIC,  // NO LONGER USED
		INDEX_KICK_BASE_FORCE,
		INDEX_CRITERIA_OF_SKILL,
		INDEX_DAMAGE_CHANNEL,
		INDEX_OPTIONS,
		INDEX_FILTER_COLLISION,
		INDEX_USE_ELASTICS,
		INDEX_USE_FULL_PHYSICS,
		INDEX_BURST_FORCE_TYPE,
		INDEX_REORIENT_TO_CAMERA,
		INDEX_FORCE_EXPLODE_AT_END
	};

private:
	CStringW m_missileName;
	TimeMs m_lifeTime;
	int m_damageValue;
	int m_particleLink;
	int m_explosionLink;
	int m_abilityType;
	int m_skillType;
	int m_damageSpecific;
	int m_baseKickPower;
	float m_collisionRadius;
	float m_heatSeekingPower;
	float m_pushPower;

	int m_criteriaOfSkillId;
	int m_damageChannelId;

	BOOL m_forceExplodeAtEnd;
	BOOL m_orientateToCamera;
	BOOL m_useElastics;
	BOOL m_useFullPhysics;
	BOOL m_burstForceType;
	BOOL m_filterCollision;

	void Init();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_WEAPONPROPERTIESCTRL_H_)
