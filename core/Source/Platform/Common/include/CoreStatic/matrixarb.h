///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "Core/Math/Matrix.h"

namespace KEP {

class MatrixARB {
public:
	typedef Vector3f Triangle[3];

	MatrixARB() {}
	static void RotationMatrixToYRot(const Matrix44f& rotationMatrix, float& xAngle, float& yAngle, float& zAngle);
	static Vector3f RotateY_WLD(float& Ya, Vector3f& curPos);
	static Vector3f GetLocalPositionFromMatrixView(const Matrix44f& matrixLoc, const Vector3f& objVect);
	static void FaceTowardPoint(Vector3f& targetVect, Matrix44f* inputMatrix);
	static void GetOrientation(const Matrix44f& input, Vector3f* dirVect, Vector3f* upVect);
	static void SetOrientation(Matrix44f* matrixInOut, const Vector3f& dirVect, const Vector3f& upVect);
	static void Transform(float q[3], const float a[3], const float m[4][4]);
	static void Transpose(float q[4][4], const float a[4][4]);
	static void Invert(float q[4][4], const float a[4][4]);
	static void Invert2(float q[4][4], const float a[4][4]); // Fast inverse if rotation sub-matrix is orthogonal
	static void Transpose(Matrix44f& q, const Matrix44f& a);
	static void Invert(Matrix44f& q, const Matrix44f& a);
	static void Invert2(Matrix44f& q, const Matrix44f& a); // Fast inverse if rotation sub-matrix is orthogonal
	static void MassInvert(int numMatrics, const Matrix44f* matrics, Matrix44f* matricsOut);
	static void MassMultiply(int numMatrics, const Matrix44f* matricsA, const Matrix44f* matricsB, Matrix44f* matricsOut);
	static Vector3f InterpolatePoints(const Vector3f& StartVec,
		const Vector3f& EndVec,
		TimeMs NumOfStages,
		TimeMs ThisStage);
	static Vector3f InterpolatePoints2(const Vector3f& StartVec,
		const Vector3f& EndVec,
		TimeMs NumOfStages,
		TimeMs ThisStage);

	static BOOL CollisionSphereSeg(const Vector3f& startPoint,
		const Vector3f& endPoint,
		float sphereRadius,
		const Vector3f& center,
		BOOL getAdjustInfo,
		Vector3f* adjust,
		float objectRadius,
		Vector3f* intersection);
	static float InterpolateFloats(
		float StartVec,
		float EndVec,
		int NumOfStages,
		int ThisStage);
	static BOOL BoundingSphereCheck(float radius1, float radius2, float x1, float y1, float z1, float x2, float y2, float z2);
	static Matrix44f AxisAngleRotationV2(const Vector3f& axis, float angle);
	static Vector3f InterpolatePointsHiRes(const Vector3f& StartVec,
		const Vector3f& EndVec,
		float NumOfStages,
		float ThisStage);
	static BOOL SegPolyCheck(const Triangle& polygon,
		const Vector3f& startPoint,
		const Vector3f& endPoint,
		Vector3f* intersectionPoint);
	static float InterpolateFloatsHiRes(
		float StartVec,
		float EndVec,
		float NumOfStages,
		float ThisStage);
	static BOOL PointInPolyXZ(const Vector3f& point, const Triangle& polyGon);
	static BOOL Between(float val, float h1, float h2);
	static Vector3f GetLocalPositionFromMatrixView2(const Matrix44f& invMatrix, const Matrix44f& matrixLoc, const Vector3f& objVect);
	static BOOL GetSubDistance(const Triangle& polyGon,
		const Vector3f& uniNormal,
		const Vector3f& point,
		float* distanceToMoveAlongNormal);
};

} // namespace KEP