/******************************************************************************
 WorldChannelPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_WORLDCHANNELPROPERTIESCTRL_H_)
#define _WORLDCHANNELPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WorldChannelPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CWorldChannelPropertiesCtrl window

class CWorldChannelPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CWorldChannelPropertiesCtrl();
	virtual ~CWorldChannelPropertiesCtrl();

public:
	enum { INDEX_ROOT = 1,
		INDEX_RULE,
		INDEX_WORLD };

	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetWorldId() const {
		return m_worldId;
	}
	int GetRuleId() const {
		return m_ruleId;
	}
	CStringA GetRuleName();

	// modifiers
	void SetWorldId(int worldId) {
		m_worldId = worldId;
	}
	void SetRuleId(int ruleId) {
		m_ruleId = ruleId;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorldChannelPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CWorldChannelPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void LoadRules();
	void InitChannels();

private:
	int m_worldId;
	int m_ruleId;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_WORLDCHANNELPROPERTIESCTRL_H_)
