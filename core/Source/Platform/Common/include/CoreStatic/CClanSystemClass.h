///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CAccountObjectList;

class CClanMemberObj : public CObject, public GetSet {
	DECLARE_SERIAL(CClanMemberObj);

public:
	CClanMemberObj();

	PLAYER_HANDLE m_memberHandle;
	CStringA m_memberName;

	void Serialize(CArchive& ar);
	void Clone(CClanMemberObj** clone);

	DECLARE_GETSET
};

class CClanMemberList : public CObList {
public:
	CClanMemberList(){};
	CClanMemberObj* GetByHandle(PLAYER_HANDLE handle);
	BOOL RemoveMemberByName(CStringA membername, PLAYER_HANDLE* playerId);
	CClanMemberObj* GetByName(CStringA& name);
	void Clone(CClanMemberList** clone);
	void SafeDelete();

	DECLARE_SERIAL(CClanMemberList);
};

class CClanSystemObj : public CObject {
	DECLARE_SERIAL_SCHEMA(CClanSystemObj, VERSIONABLE_SCHEMA | 2);

public:
	CClanSystemObj();

	CStringA m_clanName;
	CStringA m_abbrev;
	PLAYER_HANDLE m_guildMasterHandle;
	CClanMemberList* m_memberList; //member database
	int m_houseLink;
	int m_warsWon; //keeps track of wars that are won
	PLAYER_HANDLE* m_clanWarList;
	int m_clanWarCount;
	int m_clanArenaPoints;
	ULONG m_clanId; // database id

	void AddMemberByHandle(PLAYER_HANDLE handle, CStringA memberName, bool checkForDupe = true);
	void Clone(CClanSystemObj** clone);
	PLAYER_HANDLE RemoveMemberByHandle(PLAYER_HANDLE handle);
	void SafeDelete();
	BOOL VerifyClanOnWarList(PLAYER_HANDLE clanHandle);
	BOOL AddClanToWarList(PLAYER_HANDLE clanHandle);
	BOOL RemoveClanFromWarList(PLAYER_HANDLE clanHandle);
	void Serialize(CArchive& ar);
};

class CClanSystemObjList : public CObList {
public:
	CClanSystemObjList(){};
	void SafeDelete();
	void Clone(CClanSystemObjList** clone);
	// rc: 0=you already have clan, 1 = ok, 2 = already exists, 3 = name too long
	int CreateClan(CStringA m_clanFullName,
		CStringA m_clanAbbrev,
		PLAYER_HANDLE m_guildMasterHandle, CClanSystemObj** newClan = 0, bool checkForDupe = true);
	CClanSystemObj* GetClanByHandle(PLAYER_HANDLE m_guildMasterHandle);
	CClanSystemObj* GetClanByClanId(LONG clanId);
	CClanSystemObj* GetClanByPreFix(CStringA& prefix);
	BOOL DeleteClanByHandle(PLAYER_HANDLE m_guildMasterHandle);
	void DumpClanDataToFile(CStringA filename, CAccountObjectList* accountDB);
	CStringA ConvToString(int number);
	BOOL AreTheseClansAtWar(PLAYER_HANDLE clanHandle_1, PLAYER_HANDLE clanHandle_2);
	BOOL TheseClansHaveAgreedToWar(PLAYER_HANDLE clanHandle_1, PLAYER_HANDLE clanHandle_2);
	BOOL TheseClansHaveStoppedTheWar(PLAYER_HANDLE clanHandle_1, PLAYER_HANDLE clanHandle_2);
	BOOL GetPlayersClan(PLAYER_HANDLE playerId, PLAYER_HANDLE& clanHandle, CStringA& clanName);

	DECLARE_SERIAL(CClanSystemObjList);
};

} // namespace KEP