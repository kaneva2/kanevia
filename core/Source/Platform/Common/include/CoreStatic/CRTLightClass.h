///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

#include "matrixarb.h"
#include "CMaterialClass.h"
#include "CStateManagementClass.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class IMemSizeGadget;
class ReD3DX9DeviceState;

class CRTLightObj : public CObject {
	DECLARE_SERIAL_SCHEMA(CRTLightObj, VERSIONABLE_SCHEMA | 9); // version 7 for ReMesh

public:
	CRTLightObj();

	// material used for this light - kind of overkill, isn't it?
	std::unique_ptr<CMaterialObject> m_material;

	// range of the light
	float m_range;

	// three attenuation factors used for a light.
	//  the attenuation of a light is calcualted by the following equation:
	// A = 1 / (A + Bd + Cd^2)
	//  where A == m_attenuation0, B == m_attenuation1, C == m_attenuation2
	float m_attenuation0;
	float m_attenuation1;
	float m_attenuation2;

	// Position of the light
	Vector3f m_wldspacePos;

	//light type {1 = point, 2 = spot, 3 = directional}
	enum RTLightType { RTL_POINT = 1,
		RTL_SPOT = 2,
		RTL_DIRECTIONAL = 3,
		RTL_FLICKERING = 4 };

	int m_type;

	// for spotlights
	Vector3f m_direction;
	float m_theta;
	float m_phi;
	float m_falloff;

	float m_distFromChar;
	bool m_fadeOn;
	bool m_blinkOn;
	long m_blinkDur;
	bool m_randomOn;
	int m_intMin;
	int m_intMax;

	// 'true' if the object is to be rendered, otherwise 'false' will
	// cause the object not to render.
	bool m_render;

	void Serialize(CArchive& ar);
	void Clone(CRTLightObj** clone);

	void LoadLight(ReD3DX9DeviceState* devState); // RENDERENGINE INTERFACE

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CRTLightObjList /*: public CObList*/ {
public:
	CRTLightObjList(){};

	// called once per frame
	void FlushSys(LPDIRECT3DDEVICE9 g_pd3dDevice, CStateManagementObj* stateManager);

	void AddLight(CRTLightObj* light, const Vector3f& position);
	void AddLight(CRTLightObj* light, const Vector3f& position, const Vector3f& charPosition);

	void EnableAll(LPDIRECT3DDEVICE9 g_pd3dDevice, CStateManagementObj* stateManager);

	void EnableLightsForPosition(LPDIRECT3DDEVICE9 device, CStateManagementObj* state_manager, Vector3f& position);

	void GetLightsRenderEngine(ReD3DX9DeviceState* devState);

private:
	bool Contains(CRTLightObj* light) {
		return std::find(m_apLights.begin(), m_apLights.end(), light) != m_apLights.end();
	}

private:
	std::vector<CRTLightObj*> m_apLights;
};

} // namespace KEP
