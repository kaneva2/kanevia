///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPCommon.h"
#include "KEPConstants.h"
#include "CStructuresMisl.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {
class RuntimeSkeleton;
class ReMesh;
class CShadowClass;
class AnimationProxy;
class MeshProxy;
class CDeformableMesh;
class CDeformableMeshList;
class CAlterUnitDBObjectList;
class SkeletonConfiguration;
class CMaterialObject;
class CBoneAnimationObject;
class CBoneObject;
class CFrameObj;
class CParticleEmitRangeObjList;
class TextureDatabase;
class CSkeletonAnimHeader;
class IMemSizeGadget;

class CSkeletonObject : public CObject {
public:
	CSkeletonObject() :
			m_pRuntimeSkeleton(nullptr), m_pSkeletonConfiguration(nullptr) {}

	CSkeletonObject(std::shared_ptr<RuntimeSkeleton> pRS, SkeletonConfiguration* pSC) :
			m_pRuntimeSkeleton(std::move(pRS)), m_pSkeletonConfiguration(pSC) {}

	virtual ~CSkeletonObject();

	void Clone(
		CSkeletonObject** ppSO_out,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int* spawnCfgOptional = NULL,
		int* matCfgOptional = NULL,
		D3DCOLORVALUE* spawnRGB = NULL,
		bool fullClone = true);

	void Serialize(CArchive& ar);

	void attach(std::shared_ptr<RuntimeSkeleton> pRS, SkeletonConfiguration* pSC) {
		m_pRuntimeSkeleton = std::move(pRS);
		m_pSkeletonConfiguration = pSC;
	}
	void detach() {
		m_pRuntimeSkeleton = nullptr;
		m_pSkeletonConfiguration = nullptr;
	}

	std::shared_ptr<RuntimeSkeleton>& getRuntimeSkeletonShared() { return m_pRuntimeSkeleton; }
	const std::shared_ptr<RuntimeSkeleton>& getRuntimeSkeletonShared() const { return m_pRuntimeSkeleton; }
	RuntimeSkeleton* getRuntimeSkeleton() { return m_pRuntimeSkeleton.get(); }
	const RuntimeSkeleton* getRuntimeSkeleton() const { return m_pRuntimeSkeleton.get(); }
	SkeletonConfiguration* getSkeletonConfiguration() { return m_pSkeletonConfiguration; }
	const SkeletonConfiguration* getSkeletonConfiguration() const { return m_pSkeletonConfiguration; }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	// Runtime skeleton used by movement objects and dynamic objects.
	std::shared_ptr<RuntimeSkeleton> m_pRuntimeSkeleton;

	// Configuration helper class used by movement objects.
	// i.e. Code that uses the DimSwap configuration mechanism.
	SkeletonConfiguration* m_pSkeletonConfiguration;

	DECLARE_SERIAL_SCHEMA(CSkeletonObject, VERSIONABLE_SCHEMA | 9);
};

} // namespace KEP
