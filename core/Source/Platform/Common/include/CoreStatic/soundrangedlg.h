///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOUNDRANGEDLG_H__C9F7C501_312B_11D4_B1E9_0000B4BD56DD__INCLUDED_)
#define AFX_SOUNDRANGEDLG_H__C9F7C501_312B_11D4_B1E9_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SoundRangeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSoundRangeDlg dialog

class CSoundRangeDlg : public CDialog {
	// Construction
public:
	CSoundRangeDlg(CWnd* pParent = NULL); // standard constructor

	// Dialog Data
	//{{AFX_DATA(CSoundRangeDlg)
	enum { IDD = IDD_DIALOG_SOUNDRANGE };
	CComboBox m_soundList;
	int m_endRange;
	int m_startRange;
	float m_soundFactor;
	float m_clipRadius;
	//}}AFX_DATA

	int GetID() const {
		return m_id;
	}
	CStringA GetFilename() const {
		return m_filename;
	}

	void SetID(int id) {
		m_id = id;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSoundRangeDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListSoundFiles(int sel);

	int m_id;
	CStringA m_filename;

	// Generated message map functions
	//{{AFX_MSG(CSoundRangeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOUNDRANGEDLG_H__C9F7C501_312B_11D4_B1E9_0000B4BD56DD__INCLUDED_)
