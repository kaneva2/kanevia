///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common\include\KEPCommon.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include "GetSet.h"

namespace KEP {

class IMemSizeGadget;

class CFireRangeObject : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CFireRangeObject, VERSIONABLE_SCHEMA | 3)

private:
	int m_animationIndex;

public:
	CFireRangeObject();

	int m_animationType;
	int m_animationVersion;
	int m_animationFrameRangeStart;
	int m_animationFrameRangeEnd;
	int m_boneIndex;
	int m_missileIndex;
	BOOL m_inRangeCurrently;
	Vector3f m_hardPointAdjust;
	float m_accuracyRating;
	Timer m_timerLastFired;

	void Clone(CFireRangeObject** clone);

	void Serialize(CArchive& ar);

	int GetLegacyAnimationIndex() const {
		return m_animationIndex;
	}
	void SetAnimationInfo(int type, int ver);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CFireRangeObjectList : public CObList {
public:
	void SafeDelete();
	void Clone(CFireRangeObjectList** clone);
	CFireRangeObjectList(){};

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CFireRangeObjectList)
};

} // namespace KEP
