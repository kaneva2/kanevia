#pragma once
#if 0
#include "Reference.h"
#include "Pointer.h"
#include "ColorRGBA.h"
#include "Core/Math/Vector.h"
#include <D3DX9Math.h>
#include <string>
#include <set>

namespace KEP {

class Grid : public IReference {
public:

	typedef Pointer< Grid > Ptr;

	class Axis : public IReference {
	public:

		typedef Pointer< Axis > Ptr;

		struct Vertex {
			D3DCOLOR    color;
			float       x, y, z, w;
		};

		enum {
			VERTEXFVF = D3DFVF_XYZRHW | D3DFVF_DIFFUSE
		};

		Axis();
		virtual ~Axis();

		void                        SetMajorColor(const ColorRGBA< float >& color);
		const ColorRGBA< float >&   GetMajorColor() const;

		void                        SetMinorColor(const ColorRGBA< float >& color);
		const ColorRGBA< float >&   GetMinorColor() const;

		void                        SetMinorCountPerMajor(unsigned int count);
		unsigned int                GetMinorCountPerMajor() const;

		void                        Enable(bool enable);
		bool                        IsEnabled() const;

		void                        SetSpacingDistance(float distance);
		float                       GetSpacingDistance() const;

		void                        SetDirection(const Vector3f& direction);
		const Vector3f&          GetDirection() const;

		void                        SetOrigin(const Vector3f& origin);
		const Vector3f&          GetOrigin() const;

		void                        SetName(const std::string& name);
		const std::string&          GetName() const;

		void                        SetNumberOfLines(unsigned int number);
		unsigned int                GetNumberOfLines() const;

		void                        Render() const;

	protected:

		void                    AllocateVertices();
		void                    DeleteVertices();
		void                    InitializeVertices();

		ColorRGBA< float >      m_originColor;
		ColorRGBA< float >      m_majorColor;
		ColorRGBA< float >      m_minorColor;

		bool                    m_enable;
		unsigned int            m_minorCountPerMajor;
		float                   m_spacingDistance;
		float                   m_width;
		float                   m_height;
		std::string             m_name;
		Vertex*                 m_pVertices;
		unsigned int            m_numberOfVertices;

		Vector3f             m_origin;
		Vector3f             m_direction;
	};

	Grid();
	virtual ~Grid();

	void                    AddAxis(Grid::Axis::Ptr pAxis);
	void                    RemoveAxis(Grid::Axis::Ptr pAxis);
	const Grid::Axis::Ptr   GetAxis(size_t index) const;
	Grid::Axis::Ptr         GetAxis(size_t index);

	void                    Render() const;

protected:

	typedef std::set< Axis::Ptr >   AxesContainer;

	AxesContainer   m_axes;
}; // class..Grid

}; // namespace..KEP
#endif
