///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define DIRECTINPUT_VERSION 0x0800

#include <mmsystem.h>
#include "mmstream.h"
#include "amstream.h"
#include "ddstream.h"
#include "dsound.h"
#include "dinput.h"
#include <KEPNetwork.h>
#include "CABFunctionLib.h"

// DRF - Added
typedef IDirectInput8 DInput;
typedef LPDIRECTINPUTDEVICE8 DInputDevice;

class CABD3DFunctionLib {
public:
	CABD3DFunctionLib();

	//---------members for display modes-----------//
	int currentVideoMode;
	static int totalDisplayModes;
	static int videoMDWidth[50];
	static int videoMDHeight[50];
	static int videoMDBpp[50];

	//---------members for display drivers---------//
	static int NumDrivers;
	static int curdriver;
	static BOOL driverHardware[10];
	static BOOL driverRGB[10];
	static BOOL driverAntialias[10];
	static BOOL driverBiLinearTexture[10];
	static BOOL driverAlphaBlending[10];
	static BOOL driverFogging[10];
	static BOOL driverDither[10];
	static BOOL driverZbuffer[10];
	static BOOL driverMemType[10];
	static GUID DriverGUID[10]; /* GUIDs of the available D3D drivers */
	static char DriverName[10][50]; /* names of the available D3D drivers */

	//-----members for direct input----------------//
	static int HowManyJoysticks;
	static LPDIRECTINPUTDEVICE2 deviceFound[4]; //allow for four devices
	IDirectInputEffect* deviceEffect[4];
	static BOOL deviceIsForceFeedback[4];

	DInput* directInput;
	DInputDevice directKeyboard;
	DInputDevice directMouse;

	RECT m_curModeRect; // NOTE - this is always (0, 0, width, height)

	LPDIRECTDRAWSURFACE7 PrimarySurfaceRef;

	//DIRECT PLAY
	static int avgPacketSize;
	float globalPlayerMatrix[4][4];
	int serverPopoutCount;
	int clientMsgQue;
	int clientMsgQueAttribute;

	CStringA ChatStringBuild;
	BOOL ChatModeOn;
	BOOL TakingKeyStrokes;
	CStringA LastKey;

public:
	static BOOL FAR PASCAL EnumFFJoysticksCallback(LPCDIDEVICEINSTANCE pInst, LPVOID lpvContext);
	static BOOL FAR PASCAL InitJoystickInput(LPCDIDEVICEINSTANCE pdinst, LPVOID pvRef);
	static void AddInputDevice(LPDIRECTINPUTDEVICE pdev, LPCDIDEVICEINSTANCE pdi);
	static HRESULT SetDIDwordProperty(LPDIRECTINPUTDEVICE pdev, REFGUID guidProperty, DWORD dwObject, DWORD dwHow, DWORD dwValue);

	bool InitializeDirectInput(HWND hwndLoc, HINSTANCE instanceHandleLoc);

	bool AcquireDirectInput(bool acquire);

	void PlayMovieInWindow(LPSTR szFile);
	BOOL GetMovieInterfaces(const char* pszFileName,
		LPDIRECTDRAW7 pDD,
		IDirectDrawSurface7** pSurface4Loc,
		IDirectDrawStreamSample** pSampleLoc,
		IAMMultiMediaStream** pSStreamLoc,
		IAMMultiMediaStream** pAMStreamLoc,
		IMediaStream** pPrimaryVidStreamLoc,
		IDirectDrawMediaStream** pDDStreamLoc,
		RECT* rectMov);

	//message function thats compatible with fullscreen as well as windowed mode
	HRESULT RenderStreamToSurface(const char* pszFileName, LPDIRECTDRAW7 pDD, LPDIRECTDRAWSURFACE7 pPrimary, RECT rect);
	void PostMessageD3D(LPDIRECTDRAW7 ddrawLoc, CStringA textLoc, BOOL fullscreenOn);

	//Blacks out a specified surface
	void BlackOut(LPDIRECTDRAWSURFACE7 surfaceLoc);

	//play movie Fullscreen
	void PlayMovie(const char* pszFileName, LPDIRECTDRAWSURFACE7 pPrimary, BOOL fullscreenLoc, LPDIRECTDRAW7 pDD);

	//sound
	BOOL MakeDirectSound(HWND hwnd, IDirectSound8** lpds);

	IDirectSoundBuffer* DSLoad3DSoundBuffer(IDirectSound* pDS, const char* lpName);
	BOOL DSGetWaveResource(HMODULE hModule, const char* lpName, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize);
	BOOL DSFillSoundBuffer(IDirectSoundBuffer* pDSB, BYTE* pbWaveData, DWORD cbWaveSize);
	BOOL DSGetWaveFile(HMODULE hModule, const char* lpName, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize, void** ppvBase);
	BOOL DSParseWaveResource(void* pvRes, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize);
};
