///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include "Glids.h"
#include "CStructuresMath.h"

namespace KEP {

enum MOVE_FIELDS : BYTE {
	NONE = 0x00,
	POS = 0x01,
	ROT = 0x02,
	ANIM1 = 0x04,
	ANIM2 = 0x08,
	ALL = (POS | ROT | ANIM1 | ANIM2)
};

// MOVE_DATA <--Encode/Decode--> MSG_MOVE_TO_SERVER/CLIENT
struct MOVE_DATA {
	short netTraceId;
	Vector3f pos;
	Vector3f dir;
	Vector3f up;
	GLID animGlid1;
	GLID animGlid2;

	MOVE_DATA() :
			netTraceId(0),
			pos(Vector3f(0.0f, 0.0f, 0.0f)),
			dir(Vector3f(0.0f, 0.0f, 0.0f)),
			up(Vector3f(0.0f, 0.0f, 0.0f)),
			animGlid1(GLID_INVALID),
			animGlid2(GLID_INVALID) {}

	MOVE_DATA(short _netTraceId, Vector3f _pos, Vector3f _dir, Vector3f _up, GLID _animGlid1, GLID _animGlid2) :
			netTraceId(_netTraceId),
			pos(_pos),
			dir(_dir),
			up(_up),
			animGlid1(_animGlid1),
			animGlid2(_animGlid2) {}

	MOVE_FIELDS FieldsDelta(const MOVE_DATA& rhs) const;

	template <class T>
	void Encode(std::vector<BYTE>& msg, T data) const;
	void Encode(const MOVE_DATA& moveData, std::vector<BYTE>& msg) const;

	template <class T>
	void Decode(BYTE*& pData, T& data);
	void Decode(BYTE*& pData);
};

} // namespace KEP