#pragma once

class CKEPPropertyPage : public CPropertyPage {
public:
	CKEPPropertyPage(UINT id) :
			CPropertyPage(id), m_pageLoc(middlePage), m_setActiveCnt(0) {}

	enum EPageLoc {
		firstPage,
		middlePage,
		lastPage
	};

	BOOL OnSetActive() {
		CPropertySheet* parent = (CPropertySheet*)GetParent();
		if (m_pageLoc == lastPage) {
			parent->SetWizardButtons(PSWIZB_BACK | PSWIZB_FINISH);
			//parent->SetFinishText(_T("Done"));
		} else if (m_pageLoc == firstPage) {
			// if launched from installer, goes behind all windows, bring to front
			::SetWindowPos(parent->m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
			parent->SetWizardButtons(PSWIZB_NEXT); // only next
		} else { // middle
			parent->SetWizardButtons(PSWIZB_BACK | PSWIZB_NEXT);
		}

		m_setActiveCnt++;
		return CPropertyPage::OnSetActive();
	}

	LRESULT OnWizardNext() {
		UpdateData(TRUE);
		if (m_pageLoc != lastPage) {
			// return -1 if shouldn't go on
		}
		return CPropertyPage::OnWizardNext();
	}

	EPageLoc m_pageLoc;
	int m_setActiveCnt;
};

