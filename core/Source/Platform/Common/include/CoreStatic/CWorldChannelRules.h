///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "InstanceId.h"
#include "KEPObList.h"

namespace KEP {

class CWordlChannelRuleObj : public CObject, public GetSet {
	DECLARE_SERIAL(CWordlChannelRuleObj);

public:
	CWordlChannelRuleObj();

	// IF CHANGE LAST ONE, UPDATE Serialize since it range checks
	enum EChannelRule { NO_HEAL = 0,
		NO_HIDE,
		NO_DROP,
		NO_MURDER_COUNTER,
		NO_LEVEL_RESTRICTION,
		ARENA_AREA,
		NO_HEAL_WEAPONS };

	CStringA m_ruleName;
	EChannelRule m_rule; //0=no heal 1=no hide 2=no drop 3=no murdercounter 4=no level restriction 5=Arena Area
	ChannelId m_channel;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CWorldChannelRuleList : public CKEPObList {
public:
	CWorldChannelRuleList(){};

	DECLARE_SERIAL(CWorldChannelRuleList);

	void SafeDelete();
	BOOL DoesThisRuleExistInThisChannel(const ChannelId& channel, CWordlChannelRuleObj::EChannelRule searchRule);
};

} // namespace KEP