///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CStructuresMisl.h"
#include "KEPObList.h"

namespace KEP {

class CWaterZoneObj : public CObject, public GetSet {
	DECLARE_SERIAL(CWaterZoneObj);

public:
	CWaterZoneObj();

	CStringA m_waterZoneName;
	ABBOX m_waterArea;
	BOOL m_isExclusionZone; //if false is a water zone

	BOOL InitFromARW(CStringA filename, LPDIRECT3DDEVICE9 g_pd3dDevice);
	BOOL BoundBoxPointCheck(Vector3f& point, ABBOX box) const;
	BOOL IsPositionInZone(Vector3f position) const;
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CWaterZoneObjList : public CKEPObList {
public:
	CWaterZoneObjList(){};

	DECLARE_SERIAL(CWaterZoneObjList);

	void SafeDelete();
	BOOL IsPositionInWater(Vector3f position, float* submergedValue) const;
};

} // namespace KEP