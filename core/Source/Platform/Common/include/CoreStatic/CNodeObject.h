///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CNodeObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CNodeObj, VERSIONABLE_SCHEMA | 2);

public:
	CNodeObj();

	void SetScaleTranslationRotation(const Vector3f& scale, const Vector3f& translation, const Vector3f& rotation);
	void SetScale(const Vector3f& scale);
	void SetRotation(const Vector3f& rotation);
	void SetTranslation(const Vector3f& translation);
	void SetWorldTransform(const Matrix44f& worldTransform);
	void ResetWorldTransform();

	const Vector3f& GetScale() const {
		return m_scale;
	}
	const Vector3f& GetRotation() const {
		return m_rotation;
	}
	const Vector3f& GetTranslation() const {
		return m_translation;
	}
	const Matrix44f& GetWorldTransform() const {
		return m_worldTransform;
	}

	void SafeDelete();

	void Serialize(CArchive& ar);

	int m_libraryReference; //index into a visual library
	ABBOX m_clipBox;

protected:
	void BuildTransform();
	void DecomposeTransform();

	Matrix44f m_worldTransform;
	Vector3f m_scale;
	Vector3f m_rotation;
	Vector3f m_translation;

	DECLARE_GETSET
};

class CNodeObjList : public CObList {
public:
	CNodeObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CNodeObjList);
};

} // namespace KEP