/******************************************************************************
 GetSetPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEPPropertiesCtrl.h"
#include <GetSet.h>

// helper class to edit a GetSet object
class CGetSetPropertiesCtrl : public CKEPPropertiesCtrl {
public:
	// contructor, showComments will put the help
	// string for each item under the control
	CGetSetPropertiesCtrl(bool showComments = true);
	virtual ~CGetSetPropertiesCtrl();

	// setup the control with values from the object
	// returns the number of exposed items *not* added to
	// the control, since probably not supported
	// e.g. 0 = 100% ok
	int SetValues(GetSet* object);

	// validates the value per the GetSet validation routines
	// for range, length.
	// returns 0 if all valid, otherwise the first offending
	// item
	int ValidateValues();

	// extract the values from ctrl if user confirmed ok
	// returns true if SetValues was called
	// if alsoFreeValues is passed in the values are freed
	// and it will not update anything.
	bool GetValues(GetSet* object, bool alsoFreeValues = false);

	// free the internal values should call after GetValues
	// if don't pass in true, called in destructor
	void FreeValues();

	// get a GetSet object containing a preview of the values
	GetSet* PreviewValues() {
		return m_previewObject;
	}

	// overrides
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual void PreSubclassWindow();

	// default heading color
	static COLORREF DEFAULT_COLOR;

	// strings for booleans, defaults to True/False
	static CStringW FALSE_VALUE;
	static CStringW TRUE_VALUE;

private:
	bool m_showComments;

	GetSet* m_previewObject;
	gsMetaData* m_metaData;

	// Note: stores UTF-16 encoded CStringW objects for gsCString types, which in other uses are UTF-8 encoded CStringA objects.
	gsValues m_values;
};
