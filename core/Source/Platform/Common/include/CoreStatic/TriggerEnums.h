///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

enum class eTriggerState : int {
	TriggerOnce, // one-time only triggered
	Outside,
	MovedOutside, // <= this, outside
	Inside, // >= this, inside
	MovedInside,
	Deleted
};

// Trigger Actions (serialized)
enum class eTriggerAction : int {
	None = 0,
	Deprecated_1 = 1, // TA_ACTIVATE_ANIMATION
#if (DRF_OLD_PORTALS == 1)
	ActivatePortal = 2,
	ActivateExternalPortal = 3,
#else
	Deprecated_2 = 2, // TA_ACTIVATE_PORTAL
	Deprecated_3 = 3, // TA_ACTIVATE_EXTERNAL_PORTAL
#endif
	Deprecated_4 = 4, // TA_RUN_MOVIE
	Deprecated_5 = 5, // TA_ACTIVATE_FORCE_PAD
	Deprecated_6 = 6, // TA_CHANGE_SOUNDTRACK
	Deprecated_7 = 7, // TA_TEST_MSG
	Deprecated_8 = 8, // TA_OLD_SCRIPTING
	ClientScript = 9,
	ServerScript = 10
};

enum class TriggerVersion {
	Basic,
	Advanced,
};

enum class eTriggerEvent : int {
	Enter = 1,
	Exit = 2
};

inline const char* TriggerActionStr(eTriggerAction triggerAction) {
	static const char* s_str[] = {
		"None",
		"DEP_ActivateAnimation",
		"ActivatePortal",
		"ActivateExternalPortal",
		"DEP_RunMovie",
		"DEP_ActivateForcePad",
		"DEP_ChangeSoundTrack",
		"DEP_TestMsg",
		"DEP_OldScripting",
		"ClientScript",
		"ServerScript"
	};
	return s_str[(int)triggerAction];
}