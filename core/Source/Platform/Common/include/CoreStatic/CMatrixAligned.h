/******************************************************************************
 cmatrixaligned.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

/* Intel IPP matrix lib includes */
#include "ippcore.h"
#include "ippm.h"

#include "d3dx9.h"

struct CVECTORUNALIGNED {
public:
	CVECTORUNALIGNED(){};
	CVECTORUNALIGNED(CONST Ipp32f*);
	CVECTORUNALIGNED(CONST D3DVECTOR& xyz, Ipp32f w);
	CVECTORUNALIGNED(Ipp32f x, Ipp32f y, Ipp32f z, Ipp32f w);

	// casting
	operator Ipp32f*();
	operator CONST Ipp32f*() const;

	// assignment operators
	CVECTORUNALIGNED& operator+=(CONST CVECTORUNALIGNED&);
	CVECTORUNALIGNED& operator-=(CONST CVECTORUNALIGNED&);
	CVECTORUNALIGNED& operator*=(Ipp32f);
	CVECTORUNALIGNED& operator/=(Ipp32f);

	// unary operators
	CVECTORUNALIGNED operator+() const;
	CVECTORUNALIGNED operator-() const;

	// binary operators
	CVECTORUNALIGNED operator+(CONST CVECTORUNALIGNED&) const;
	CVECTORUNALIGNED operator-(CONST CVECTORUNALIGNED&) const;
	CVECTORUNALIGNED operator*(Ipp32f) const;
	CVECTORUNALIGNED operator/(Ipp32f) const;

	friend CVECTORUNALIGNED operator*(Ipp32f, CONST CVECTORUNALIGNED&);

	BOOL operator==(CONST CVECTORUNALIGNED&) const;
	BOOL operator!=(CONST CVECTORUNALIGNED&) const;

public:
	Ipp32f x, y, z, w;
};

//--------------------------
// 4D Vector
//--------------------------
__forceinline CVECTORUNALIGNED::CVECTORUNALIGNED(CONST Ipp32f* pf) {
	x = pf[0];
	y = pf[1];
	z = pf[2];
	w = pf[3];
}

__forceinline CVECTORUNALIGNED::CVECTORUNALIGNED(CONST D3DVECTOR& v, Ipp32f f) {
	x = v.x;
	y = v.y;
	z = v.z;
	w = f;
}

__forceinline CVECTORUNALIGNED::CVECTORUNALIGNED(Ipp32f fx, Ipp32f fy, Ipp32f fz, Ipp32f fw) {
	x = fx;
	y = fy;
	z = fz;
	w = fw;
}

__forceinline CVECTORUNALIGNED::operator Ipp32f*() {
	return (Ipp32f*)&x;
}

__forceinline CVECTORUNALIGNED::operator CONST Ipp32f*() const {
	return (CONST Ipp32f*)&x;
}

__forceinline CVECTORUNALIGNED&
CVECTORUNALIGNED::operator+=(CONST CVECTORUNALIGNED& v) {
	x += v.x;
	y += v.y;
	z += v.z;
	w += v.w;
	return *this;
}

__forceinline CVECTORUNALIGNED&
CVECTORUNALIGNED::operator-=(CONST CVECTORUNALIGNED& v) {
	x -= v.x;
	y -= v.y;
	z -= v.z;
	w -= v.w;
	return *this;
}

__forceinline CVECTORUNALIGNED&
CVECTORUNALIGNED::operator*=(Ipp32f f) {
	x *= f;
	y *= f;
	z *= f;
	w *= f;
	return *this;
}

__forceinline CVECTORUNALIGNED&
CVECTORUNALIGNED::operator/=(Ipp32f f) {
	Ipp32f fInv = 1.0f / f;
	x *= fInv;
	y *= fInv;
	z *= fInv;
	w *= fInv;
	return *this;
}

__forceinline CVECTORUNALIGNED
CVECTORUNALIGNED::operator+() const {
	return *this;
}

__forceinline CVECTORUNALIGNED
CVECTORUNALIGNED::operator-() const {
	return CVECTORUNALIGNED(-x, -y, -z, -w);
}

__forceinline CVECTORUNALIGNED
CVECTORUNALIGNED::operator+(CONST CVECTORUNALIGNED& v) const {
	return CVECTORUNALIGNED(x + v.x, y + v.y, z + v.z, w + v.w);
}

__forceinline CVECTORUNALIGNED
CVECTORUNALIGNED::operator-(CONST CVECTORUNALIGNED& v) const {
	return CVECTORUNALIGNED(x - v.x, y - v.y, z - v.z, w - v.w);
}

__forceinline CVECTORUNALIGNED
	CVECTORUNALIGNED::operator*(Ipp32f f) const {
	return CVECTORUNALIGNED(x * f, y * f, z * f, w * f);
}

__forceinline CVECTORUNALIGNED
CVECTORUNALIGNED::operator/(Ipp32f f) const {
	Ipp32f fInv = 1.0f / f;
	return CVECTORUNALIGNED(x * fInv, y * fInv, z * fInv, w * fInv);
}

__forceinline CVECTORUNALIGNED
operator*(Ipp32f f, CONST CVECTORUNALIGNED& v) {
	return CVECTORUNALIGNED(f * v.x, f * v.y, f * v.z, f * v.w);
}

__forceinline BOOL
CVECTORUNALIGNED::operator==(CONST CVECTORUNALIGNED& v) const {
	return x == v.x && y == v.y && z == v.z && w == v.w;
}

__forceinline BOOL
CVECTORUNALIGNED::operator!=(CONST CVECTORUNALIGNED& v) const {
	return x != v.x || y != v.y || z != v.z || w != v.w;
}

typedef __declspec(align(32)) struct CVECTORUNALIGNED CVECTORALIGNED;

struct CMATRIXUNALIGNED {
public:
	CMATRIXUNALIGNED(){};
	CMATRIXUNALIGNED(CONST D3DMATRIX&);
	CMATRIXUNALIGNED(Ipp32f _11, Ipp32f _12, Ipp32f _13, Ipp32f _14,
		Ipp32f _21, Ipp32f _22, Ipp32f _23, Ipp32f _24,
		Ipp32f _31, Ipp32f _32, Ipp32f _33, Ipp32f _34,
		Ipp32f _41, Ipp32f _42, Ipp32f _43, Ipp32f _44);

	Ipp32f& operator()(UINT Row, UINT Col);
	Ipp32f operator()(UINT Row, UINT Col) const;

	operator Ipp32f*();
	operator CONST Ipp32f*() const;

	CMATRIXUNALIGNED& operator*=(CONST CMATRIXUNALIGNED&);
	CMATRIXUNALIGNED& operator+=(CONST CMATRIXUNALIGNED&);
	CMATRIXUNALIGNED& operator-=(CONST CMATRIXUNALIGNED&);
	CMATRIXUNALIGNED& operator*=(Ipp32f);
	CMATRIXUNALIGNED& operator/=(Ipp32f);

	CMATRIXUNALIGNED operator+() const;
	CMATRIXUNALIGNED operator-() const;

	CMATRIXUNALIGNED operator*(CONST CMATRIXUNALIGNED&)const;
	CMATRIXUNALIGNED operator+(CONST CMATRIXUNALIGNED&) const;
	CMATRIXUNALIGNED operator-(CONST CMATRIXUNALIGNED&) const;
	CMATRIXUNALIGNED operator*(Ipp32f) const;
	CMATRIXUNALIGNED operator/(Ipp32f) const;

	friend CMATRIXUNALIGNED operator*(Ipp32f, CONST CMATRIXUNALIGNED&);

	BOOL operator==(CONST CMATRIXUNALIGNED&) const;
	BOOL operator!=(CONST CMATRIXUNALIGNED&) const;

public:
	union {
		struct {
			Ipp32f _11, _12, _13, _14;
			Ipp32f _21, _22, _23, _24;
			Ipp32f _31, _32, _33, _34;
			Ipp32f _41, _42, _43, _44;
		};
		Ipp32f m[4][4];
	};
};

//--------------------------
// Matrix
//--------------------------

__forceinline CMATRIXUNALIGNED::CMATRIXUNALIGNED(CONST D3DMATRIX& mat) {
	memcpy(&_11, &mat, sizeof(CMATRIXUNALIGNED));
}

__forceinline CMATRIXUNALIGNED::CMATRIXUNALIGNED(Ipp32f f11, Ipp32f f12, Ipp32f f13, Ipp32f f14,
	Ipp32f f21, Ipp32f f22, Ipp32f f23, Ipp32f f24,
	Ipp32f f31, Ipp32f f32, Ipp32f f33, Ipp32f f34,
	Ipp32f f41, Ipp32f f42, Ipp32f f43, Ipp32f f44) {
	_11 = f11;
	_12 = f12;
	_13 = f13;
	_14 = f14;
	_21 = f21;
	_22 = f22;
	_23 = f23;
	_24 = f24;
	_31 = f31;
	_32 = f32;
	_33 = f33;
	_34 = f34;
	_41 = f41;
	_42 = f42;
	_43 = f43;
	_44 = f44;
}

__forceinline Ipp32f&
CMATRIXUNALIGNED::operator()(UINT iRow, UINT iCol) {
	return m[iRow][iCol];
}

__forceinline Ipp32f
CMATRIXUNALIGNED::operator()(UINT iRow, UINT iCol) const {
	return m[iRow][iCol];
}

__forceinline CMATRIXUNALIGNED::operator Ipp32f*() {
	return (Ipp32f*)&_11;
}

__forceinline CMATRIXUNALIGNED::operator CONST Ipp32f*() const {
	return (CONST Ipp32f*)&_11;
}

__forceinline CMATRIXUNALIGNED&
CMATRIXUNALIGNED::operator*=(CONST CMATRIXUNALIGNED& mat) {
	//    D3DXMatrixMultiply(this, this, &mat);
	return *this;
}

__forceinline CMATRIXUNALIGNED&
CMATRIXUNALIGNED::operator+=(CONST CMATRIXUNALIGNED& mat) {
	_11 += mat._11;
	_12 += mat._12;
	_13 += mat._13;
	_14 += mat._14;
	_21 += mat._21;
	_22 += mat._22;
	_23 += mat._23;
	_24 += mat._24;
	_31 += mat._31;
	_32 += mat._32;
	_33 += mat._33;
	_34 += mat._34;
	_41 += mat._41;
	_42 += mat._42;
	_43 += mat._43;
	_44 += mat._44;
	return *this;
}

__forceinline CMATRIXUNALIGNED&
CMATRIXUNALIGNED::operator-=(CONST CMATRIXUNALIGNED& mat) {
	_11 -= mat._11;
	_12 -= mat._12;
	_13 -= mat._13;
	_14 -= mat._14;
	_21 -= mat._21;
	_22 -= mat._22;
	_23 -= mat._23;
	_24 -= mat._24;
	_31 -= mat._31;
	_32 -= mat._32;
	_33 -= mat._33;
	_34 -= mat._34;
	_41 -= mat._41;
	_42 -= mat._42;
	_43 -= mat._43;
	_44 -= mat._44;
	return *this;
}

__forceinline CMATRIXUNALIGNED&
CMATRIXUNALIGNED::operator*=(Ipp32f f) {
	_11 *= f;
	_12 *= f;
	_13 *= f;
	_14 *= f;
	_21 *= f;
	_22 *= f;
	_23 *= f;
	_24 *= f;
	_31 *= f;
	_32 *= f;
	_33 *= f;
	_34 *= f;
	_41 *= f;
	_42 *= f;
	_43 *= f;
	_44 *= f;
	return *this;
}

__forceinline CMATRIXUNALIGNED&
CMATRIXUNALIGNED::operator/=(Ipp32f f) {
	Ipp32f fInv = 1.0f / f;
	_11 *= fInv;
	_12 *= fInv;
	_13 *= fInv;
	_14 *= fInv;
	_21 *= fInv;
	_22 *= fInv;
	_23 *= fInv;
	_24 *= fInv;
	_31 *= fInv;
	_32 *= fInv;
	_33 *= fInv;
	_34 *= fInv;
	_41 *= fInv;
	_42 *= fInv;
	_43 *= fInv;
	_44 *= fInv;
	return *this;
}

__forceinline CMATRIXUNALIGNED
CMATRIXUNALIGNED::operator+() const {
	return *this;
}

__forceinline CMATRIXUNALIGNED
CMATRIXUNALIGNED::operator-() const {
	return CMATRIXUNALIGNED(-_11, -_12, -_13, -_14,
		-_21, -_22, -_23, -_24,
		-_31, -_32, -_33, -_34,
		-_41, -_42, -_43, -_44);
}

__forceinline CMATRIXUNALIGNED
	CMATRIXUNALIGNED::operator*(CONST CMATRIXUNALIGNED& mat) const {
	CMATRIXUNALIGNED matT;
	//   D3DXMatrixMultiply(&matT, this, &mat);
	return matT;
}

__forceinline CMATRIXUNALIGNED
CMATRIXUNALIGNED::operator+(CONST CMATRIXUNALIGNED& mat) const {
	return CMATRIXUNALIGNED(_11 + mat._11, _12 + mat._12, _13 + mat._13, _14 + mat._14,
		_21 + mat._21, _22 + mat._22, _23 + mat._23, _24 + mat._24,
		_31 + mat._31, _32 + mat._32, _33 + mat._33, _34 + mat._34,
		_41 + mat._41, _42 + mat._42, _43 + mat._43, _44 + mat._44);
}

__forceinline CMATRIXUNALIGNED
CMATRIXUNALIGNED::operator-(CONST CMATRIXUNALIGNED& mat) const {
	return CMATRIXUNALIGNED(_11 - mat._11, _12 - mat._12, _13 - mat._13, _14 - mat._14,
		_21 - mat._21, _22 - mat._22, _23 - mat._23, _24 - mat._24,
		_31 - mat._31, _32 - mat._32, _33 - mat._33, _34 - mat._34,
		_41 - mat._41, _42 - mat._42, _43 - mat._43, _44 - mat._44);
}

__forceinline CMATRIXUNALIGNED
	CMATRIXUNALIGNED::operator*(Ipp32f f) const {
	return CMATRIXUNALIGNED(_11 * f, _12 * f, _13 * f, _14 * f,
		_21 * f, _22 * f, _23 * f, _24 * f,
		_31 * f, _32 * f, _33 * f, _34 * f,
		_41 * f, _42 * f, _43 * f, _44 * f);
}

__forceinline CMATRIXUNALIGNED
CMATRIXUNALIGNED::operator/(Ipp32f f) const {
	Ipp32f fInv = 1.0f / f;
	return CMATRIXUNALIGNED(_11 * fInv, _12 * fInv, _13 * fInv, _14 * fInv,
		_21 * fInv, _22 * fInv, _23 * fInv, _24 * fInv,
		_31 * fInv, _32 * fInv, _33 * fInv, _34 * fInv,
		_41 * fInv, _42 * fInv, _43 * fInv, _44 * fInv);
}

__forceinline CMATRIXUNALIGNED
operator*(Ipp32f f, CONST CMATRIXUNALIGNED& mat) {
	return CMATRIXUNALIGNED(f * mat._11, f * mat._12, f * mat._13, f * mat._14,
		f * mat._21, f * mat._22, f * mat._23, f * mat._24,
		f * mat._31, f * mat._32, f * mat._33, f * mat._34,
		f * mat._41, f * mat._42, f * mat._43, f * mat._44);
}

__forceinline BOOL
CMATRIXUNALIGNED::operator==(CONST CMATRIXUNALIGNED& mat) const {
	return 0 == memcmp(this, &mat, sizeof(CMATRIXUNALIGNED));
}

__forceinline BOOL
CMATRIXUNALIGNED::operator!=(CONST CMATRIXUNALIGNED& mat) const {
	return 0 != memcmp(this, &mat, sizeof(CMATRIXUNALIGNED));
}

typedef __declspec(align(32)) struct CMATRIXUNALIGNED CMATRIXALIGNED;

