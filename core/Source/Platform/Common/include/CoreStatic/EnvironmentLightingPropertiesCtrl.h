/******************************************************************************
 EnvironmentLightingPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ENVIRONMENT_LIGHTINGPROPERTIESCTRL_H_)
#define _ENVIRONMENT_LIGHTINGPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EnvironmentLightingPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "colordlg.h"
#include "GWCPropertyIconLook.h"

/////////////////////////////////////////////////////////////////////////////
// CEnvironmentLightingPropertiesCtrl window

class CEnvironmentLightingPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CEnvironmentLightingPropertiesCtrl();
	virtual ~CEnvironmentLightingPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual bool OnPropertyButtonClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetAmbientLightColorRed() const {
		return m_ambientLightColorRed;
	}
	float GetAmbientLightColorGreen() const {
		return m_ambientLightColorGreen;
	}
	float GetAmbientLightColorBlue() const {
		return m_ambientLightColorBlue;
	}

	float GetSunColorRed() const {
		return m_sunColorRed;
	}
	float GetSunColorGreen() const {
		return m_sunColorGreen;
	}
	float GetSunColorBlue() const {
		return m_sunColorBlue;
	}

	float GetSpecularLightColorRed() const {
		return m_specularLightColorRed;
	}
	float GetSpecularLightColorGreen() const {
		return m_specularLightColorGreen;
	}
	float GetSpecularLightColorBlue() const {
		return m_specularLightColorBlue;
	}

	BOOL GetUseAmbientLight() const {
		return m_useAmbientLight;
	}
	BOOL GetUseSun() const {
		return m_useSun;
	}
	BOOL GetUseSpecularLight() const {
		return m_useSpecularLight;
	}

	// modifiers
	void SetAmbientLightColorRed(float ambientLightColorRed) {
		m_ambientLightColorRed = ambientLightColorRed;
	}
	void SetAmbientLightColorGreen(float ambientLightColorGreen) {
		m_ambientLightColorGreen = ambientLightColorGreen;
	}
	void SetAmbientLightColorBlue(float ambientLightColorBlue) {
		m_ambientLightColorBlue = ambientLightColorBlue;
	}
	void SetUseAmbientLight(BOOL useAmbientLight) {
		m_useAmbientLight = useAmbientLight;
	}

	void SetSunColorRed(float sunColorRed) {
		m_sunColorRed = sunColorRed;
	}
	void SetSunColorGreen(float sunColorGreen) {
		m_sunColorGreen = sunColorGreen;
	}
	void SetSunColorBlue(float sunColorBlue) {
		m_sunColorBlue = sunColorBlue;
	}
	void SetUseSun(BOOL useSun) {
		m_useSun = useSun;
	}

	void SetSpecularLightColorRed(float specularLightColorRed) {
		m_specularLightColorRed = specularLightColorRed;
	}
	void SetSpecularLightColorGreen(float specularLightColorGreen) {
		m_specularLightColorGreen = specularLightColorGreen;
	}
	void SetSpecularLightColorBlue(float specularLightColorBlue) {
		m_specularLightColorBlue = specularLightColorBlue;
	}
	void SetUseSpecularLight(BOOL useSpecularLight) {
		m_useSpecularLight = useSpecularLight;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEnvironmentLightingPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CEnvironmentLightingPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_USE_AMBIENT_LIGHT,
		INDEX_AMBIENT_LIGHT_COLOR,
		INDEX_USE_SUN,
		INDEX_SUN_COLOR,
		INDEX_USE_SPECULAR_LIGHT,
		INDEX_SPECULAR_LIGHT_COLOR
	};

	void OpenAmbientLightColorDlg();
	void OpenSunColorDlg();
	void OpenSpecularLightColorDlg();

private:
	int m_id;

	float m_ambientLightColorRed;
	float m_ambientLightColorGreen;
	float m_ambientLightColorBlue;
	BOOL m_useAmbientLight;

	float m_specularLightColorRed;
	float m_specularLightColorGreen;
	float m_specularLightColorBlue;
	BOOL m_useSpecularLight;

	float m_sunColorRed;
	float m_sunColorGreen;
	float m_sunColorBlue;
	BOOL m_useSun;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ENVIRONMENT_LIGHTINGPROPERTIESCTRL_H_)
