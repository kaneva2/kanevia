///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// added by serialize.pl
#include <GetSet.h>
#include "InstanceId.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class CInventoryObjList;
class CSkillObjectList;

class CAICollectionQuestObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CAICollectionQuestObj, VERSIONABLE_SCHEMA | 8);

public:
	CAICollectionQuestObj();

	// general text strings
	CStringA m_title;
	CStringA m_questDesription;
	CStringA m_questCompletionMessage;

	int m_completedKeyEnabled; //logs quest to journal 0=none 1=store quest description 2=store quest completed 3=store both, 4=store ??? for req'ments

	// requirements
	CInventoryObjList* m_requiredItems;
	int m_requiredSkillType;
	int m_requiredSkillLevel;

	// rewards
	CInventoryObjList* m_reward;
	BOOL m_randomRewardType; // if True, only one, random item in m_reward given out
	int m_cashReward;
	int m_skillTypeBonus; // id of skill to give bonus on success
	float m_expBonus; // skill points to give
	float m_maxExpLimit; // max limit on player

	// this controls what which of the two next values are used, if true user spanwn
	// otherwise AI spawns
	BOOL m_spawnToLocationOverride;
	Vector3f m_spawnPosition; // position for player OR AI spawn
	int m_rotation;

	// for player spawn...
	ZoneIndex m_respawnZoneIndex; // zone to spawn user to, may be instanced
	BOOL m_makeTargetRebirthPoint; // if m_spawnToLocationOverride true, will make that point the players rebirth point
	int m_portalLimitID; // if client's localClientContentVersion < this, then can't spawn to the portal
	CStringA m_portalLimitMsg; // Message to send if can't portal

	// for ai spawn...
	int m_spawnAiCfg; // -1 =  none
	int m_spawnOnFail; // TRUE, spawn on fail, FALSE spawn on success
	ChannelId m_channel; // channel for AI spawn
	long m_spawnedAiLifeline; // lifetime in ms
	TimeMs m_spawnStamp; // prevent AI spawn > 5 sec

	// MOB hunting
	int m_mobToHuntID; // mob id to kill to complete quest
	int m_maxDropCount; // number of items you must get from dead mobs
	int m_dropGlid; // GLID of item dead mob will drop
	int m_dropPercentage; // % change dead mob will drop an item
	BOOL m_autocomplete; // if true and quest is complete when kill mob, quest it marked as complete

	// menu
	BOOL m_openDialogWindow; // if TRUE, open the menu
	int m_menuIdToOpen; // id of menu to open

	// misc
	int m_miscAction; // 0 = none, 1 = send store/house, 2 = send bank
	BOOL m_progessBasedByQuestJournal;
	BOOL m_listProgressionOptions;

	CStringA m_optionalIntruction;
	BOOL m_undeletable; // if have > 10 (hardcoded) quests, will remove one that is note marked as this
	int m_raceSpecificAccess; // -1 = whatever race

	CStringA m_closeJournalEntryByName; // if not empty or "Reserved" will delete the journal entry using this NPC name if fail

	CStringA m_optionalCommand; // NOT USED
	int m_type; // 0 = collection quest NOT USED, always 0

	BOOL EvaluateQuest(CInventoryObjList* playersInventory,
		CSkillObjectList* skillDatabase,
		CSkillObjectList* skillMainDatabase,
		float* expGained);
	void SafeDelete();
	void Clone(CAICollectionQuestObj** clone);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CAICollectionQuestList : public CObList {
public:
	CAICollectionQuestList(){};
	void SafeDelete();

	DECLARE_SERIAL(CAICollectionQuestList);

	CAICollectionQuestObj* GetByAvailIndex(int baseSelection, int raceID, int* retTraceVis);
	void Clone(CAICollectionQuestList** clone);
	CAICollectionQuestObj* GetByIndex(int selection);
};

} // namespace KEP