///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <functional>
#include <memory>

namespace KEP {
namespace FlashControl {

// Object to Interfaces with Flash Control factory in separate process to generate FlashControlServers in that process
// to connect to FlashControlProxies in this one.
class IFlashControlFactoryProxy {
public:
	virtual ~IFlashControlFactoryProxy() {}
	virtual bool IsFlashInstalled() = 0;
	virtual std::string GetVersion() = 0;
	virtual void SetGlobalVolume(float fVolume, bool bMute) = 0;

	static const int kiDefaultTimeoutSeconds = 10;
	virtual bool WaitForServerToInitialize(double dTimeOutSeconds = kiDefaultTimeoutSeconds) = 0;
	virtual bool Start() = 0;
	virtual bool Stop() = 0;
	virtual bool IsStopped() const = 0;
	virtual void CrashFlashProcess() = 0;

	virtual bool StartFlashControl(uint32_t iMeshId) = 0;
	virtual bool StopFlashControl(uint32_t iMeshId) = 0;
	virtual void SignalRequestAvailable(uint32_t iMeshId) = 0;

	virtual size_t GetFlashProcessCrashCount() = 0;
	virtual size_t GetPeakFlashInstanceCount() = 0;

	static std::unique_ptr<IFlashControlFactoryProxy> Create(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback);
};

} // namespace FlashControl
} // namespace KEP
