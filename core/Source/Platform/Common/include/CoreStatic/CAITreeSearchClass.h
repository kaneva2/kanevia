///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "matrixarb.h"

class CAIAlternateRoutes : public CObList {
public:
	CAIAlternateRoutes(){};

	//Functions
	void SafeDelete();
};
//-------------------------------
//Base class
//Primary root node
//-------------------------------
class CAITreeNode : public CObject {
public:
	CAITreeNode();

	int m_currentNode;
	CAIAlternateRoutes* m_alternateRoutes;

	//Functions
	void SafeDelete();
};
//-------------------------------
//Base class
//Primary root list
//-------------------------------
class CAITree : public CObList {
public:
	CAITree();

	int m_currentNodeCount;

	//Functions
	BOOL AddNode(int currentIndex);
	void SafeDelete();
};
//-------------------------------
//Route
//Holds a complete buildable tree
//-------------------------------
class CAIRoute : public CObject {
public:
	CAIRoute();

	CAITree* m_tree;
	BOOL m_routeIsValid;
	BOOL m_growingStill; //there are still options

	//Functions
	void SafeDelete();
};
