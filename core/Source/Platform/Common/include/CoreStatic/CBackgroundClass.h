///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPObList.h"

namespace KEP {

class CBackGroundObj : public CObject, public GetSet {
protected:
	CBackGroundObj() {}

	DECLARE_SERIAL(CBackGroundObj)

public:
	CBackGroundObj(IDirect3DTexture9* L_mainSurface, //main surface
		CStringA L_bitmapName, //bitmap file name
		int L_mapX, //represents the x pixel count of source image
		int L_mapY, //represents the y pixel count of source image
		RECT L_usageRect, //represents the rect of what is used of the source image
		int L_posX, //position X of offset that defines location
		int L_posY, //position Y of offset that defines location
		BOOL L_moving, //says if the map is going to be movable
		int L_animX,
		int L_animY);

	int m_animX;
	int m_animY;
	BOOL m_moving; //is it a moving texture
	IDirect3DTexture9* m_mainSurface;
	CStringA m_bitmapName; //bitmap file name
	int m_mapX; //represents the x pixel count of source image
	int m_mapY; //represents the y pixel count of source image
	RECT m_usageRect; //represents the rect of what is used of the source image
	int m_posX; //position X of offset that defines location
	int m_posY; //position Y of offset that defines location

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CBackGroundObjList : public CKEPObList {
public:
	CBackGroundObjList(){};

	DECLARE_SERIAL(CBackGroundObjList)
};

} // namespace KEP