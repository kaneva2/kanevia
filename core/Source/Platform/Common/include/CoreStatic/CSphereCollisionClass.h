///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class IMemSizeGadget;

class CSphereCollisionObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CSphereCollisionObj, VERSIONABLE_SCHEMA | 1)

public:
	CSphereCollisionObj();

	void Clone(CSphereCollisionObj** copy);
	void Serialize(CArchive& ar);

	int getBoneIndex() const { return m_boneIndex; }
	float getRadius() const { return m_sphereRadius; }

	void scaleRadius(float factor) { m_sphereRadius = m_unscaledSphereRadius * factor; }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	int m_boneIndex;
	float m_sphereRadius;
	float m_unscaledSphereRadius;
	float m_damageMultiplier;
	CStringA m_comment;
	DECLARE_GETSET
};

class CSphereCollisionObjList : public CObList {
public:
	CSphereCollisionObjList(){};
	void SafeDelete();
	void Clone(CSphereCollisionObjList** copy);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CSphereCollisionObjList)
};

} // namespace KEP
