///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPObList.h"

namespace KEP {

class CElementObj : public CObject, public GetSet {
	DECLARE_SERIAL(CElementObj);

public:
	CElementObj();

	CStringA m_elementName;

	void SafeDelete();
	void Clone(CElementObj** clone);

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CElementObjList : public CKEPObList {
public:
	CElementObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CElementObjList);

	CElementObj* GetByIndex(int index);
	void Clone(CElementObjList** clone);
};

} // namespace KEP