///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BANKZONESDLG_H__E30DC9E1_578B_11D5_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_BANKZONESDLG_H__E30DC9E1_578B_11D5_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BankZonesDlg.h : header file
//
#include "CBankClass.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "BankZonePropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CBankZonesDlg dialog

class CBankZonesDlg : public CKEPEditDialog {
	// Construction
public:
	CBankZonesDlg(CBankObjList*& bankDB,
		CWnd* pParent = NULL); // standard constructor

	CBankObjList*& m_bankDBRef;

	// Dialog Data
	//{{AFX_DATA(CBankZonesDlg)
	enum { IDD = IDD_DIALOG_BANKZONES };
	CListBox m_bankDBList;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBankZonesDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListBankZones(int sel);
	// Generated message map functions
	//{{AFX_MSG(CBankZonesDlg)
	afx_msg void OnBUTTONAddBank();
	afx_msg void OnBUTTONDeleteBank();
	afx_msg void OnBUTTONReplaceBank();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonLoad();
	afx_msg void OnSelchangeLISTBankDB();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceBank(int selection);

public:
	afx_msg void OnBnClicked();

private:
	CBankObj* m_selectedBankZoneObj;

	CCollapsibleGroup m_colBankZones;
	CButton m_grpBankZones;
	CBankZonePropertiesCtrl m_propBankZone;

	CKEPImageButton m_btnAddBankSector;
	CKEPImageButton m_btnDeleteBankSector;
	CKEPImageButton m_btnReplaceBankSector;

	void RedrawPropertiesCtrl();
	void LoadBankZoneProperties();
	CBankObj* GetSelectedBankClass();
	CBankObj* GetBankClass(int index);

	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BANKZONESDLG_H__E30DC9E1_578B_11D5_B1EE_0000B4BD56DD__INCLUDED_)
