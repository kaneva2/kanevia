/******************************************************************************
 NewMenuDialog.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "resource.h"

// CNewMenuDialog dialog

class CNewMenuDialog : public CDialog {
	DECLARE_DYNAMIC(CNewMenuDialog)

public:
	CNewMenuDialog(CWnd* pParent = NULL); // standard constructor
	virtual ~CNewMenuDialog();

	// Dialog Data
	enum { IDD = IDD_ADD_MENU };

	CStringA GetMenuName() const {
		return m_sMenuName;
	}
	CStringA GetScriptName() const {
		return m_sScriptName;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChangeMenuName();
	afx_msg void OnChangeScriptName();
	afx_msg void OnSetFocusScriptName();

private:
	CStringA m_sMenuName;
	CStringA m_sScriptName;
	CStringA m_sEnterName; // default text for the menu name
	bool m_bEditScriptName; // set to true once the edit field has gained focus

	CButton m_ok_button;
	CEdit m_menuName;
	CEdit m_scriptName;
};
