///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CStructuresMisl.h"
#include "GetSet.h"
#include "ResourceType.h"
#include "KEPFileNames.h"
#include <KEPPhysics/Declarations.h>
#include <memory>
#include <string>
#include <vector>

// Default collision creation parameters for dynamic objects
#define DO_COL_RES_X 4
#define DO_COL_RES_Y 4
#define DO_COL_RES_Z 4
#define DO_COL_LEVELSDEEP 2
#define DO_COL_FACESPERBOX 2000

namespace KEP {

class IMemSizeGadget;
class IResource;
class CArmedInventoryObj;
class CCollisionObj;
class CEXMeshObj;
class CMaterialObject;
class CRTLightObj;
class CTriggerObjectList;
class CVertexAnimationModule;
class ReMesh;
class RuntimeSkeleton;

struct RGBColor {
	unsigned char r, g, b;
};

/*!
 * \brief
 * Light weight class to represent a dynamic light obj.
 *
 * Streamlined version of CRTLightObj
 *
 * \remarks
 * This is a cool light
 *
 * \see
 * CRTLightObj | StreamableDynamicObject
 */
class StreamableDynamicObjectLight {
public:
	StreamableDynamicObjectLight();
	StreamableDynamicObjectLight(int r, int g, int b, float range, float att1, float att2, float att3);
	~StreamableDynamicObjectLight();

private:
	RGBColor m_lightColor;
	float m_range;
	float m_attenuation1;
	float m_attenuation2;
	float m_attenuation3;

public:
	RGBColor getLightColor() {
		return m_lightColor;
	}
	void setLightColor(int r, int g, int b) {
		m_lightColor.r = (unsigned char)r;
		m_lightColor.g = (unsigned char)g;
		m_lightColor.b = (unsigned char)b;
	}

	float getRange() {
		return m_range;
	}
	void setRange(float range) {
		m_range = range;
	}

	float getAttenuation1() {
		return m_attenuation1;
	}
	void setAttenuation1(float att1) {
		m_attenuation1 = att1;
	}

	float getAttenuation2() {
		return m_attenuation2;
	}
	void setAttenuation2(float att2) {
		m_attenuation2 = att2;
	}

	float getAttenuation3() {
		return m_attenuation3;
	}
	void setAttenuation3(float att3) {
		m_attenuation3 = att3;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

/*!
 * \brief
 * Light weight class designed to replace CDynamicObjVisual.
 *
 * This visual class incorporates some of the elements from the reference library
 * to make a self-contained representation of a dynamic object visual.  Each
 * dynamic object contains one or more visuals, which contain the meshes and
 * materials necessary to render them properly.
 *
 * \remarks
 * I'd rather not be writing doxygen comments
 *
 * \see
 * StreamableDynamicObject | StreamableDynamicObjectSystem
 */
class StreamableDynamicObjectVisual {
public:
	StreamableDynamicObjectVisual();
	~StreamableDynamicObjectVisual();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	std::vector<std::pair<UINT64, float>> m_lodHashes; // Note: lod[0] is full detail.
	CMaterialObject* m_material; // Material is NULL for visuals that participate in collision, but are not rendered.
	Vector3f m_relativePosition;
	Vector3f m_relativeOrientation;
	CVertexAnimationModule* m_animModule; // legacy for vertex animations
	CStringA m_name;
	std::vector<ReMesh*> m_reMeshDirectAccess; // runtime pointer to remesh
	bool m_usePersistentMeshPool; // search persistent mesh pool if mesh does not exist in dynamic pool
	UINT m_index; // index of visual in dynamic object

	ReMesh* m_altCollisionSource; // mesh for collision computation -- set at initiation phase only, do not serialize
	BOOL m_bDisposeAltCollision; // TRUE if m_altCollisionSource must be disposed at destruction. FALSE otherwise.
	int m_LODCollisionSource; // LOD# for collision computation -- set at initiation phase only, do not serialize
	bool m_canCollide; // collision filter mesh flag

public:
	size_t getLodCount() const {
		return m_lodHashes.size();
	}

	UINT64 getLOD(unsigned int lodLevel) const; // accessor for lod hash data
	float getLODDistance(unsigned int lodLevel) const;
	UINT getDistanceLOD(FLOAT distance) const;
	bool replaceLOD(unsigned int lodLevel, UINT64 hash, float distance); // modify existing lod hash
	void addLOD(UINT64 hash, float distance = 0.0f); // add additional lod
	void removeLOD(int index);

	CMaterialObject* getMaterial() {
		return m_material;
	}
	void setMaterial(CMaterialObject* mat, const char* subDir = DYNOBJ_TEXTURE);

	void setName(CStringA name) {
		m_name = name;
	}
	CStringA getName() const {
		return m_name;
	}

	Vector3f getRelativePosition() const {
		return m_relativePosition;
	}
	void setRelativePosition(float x, float y, float z);

	Vector3f getRelativeOrientation() const {
		return m_relativeOrientation;
	}
	void setRelativeOrientation(float x, float y, float z);

	CVertexAnimationModule* getVertexAnim() {
		return m_animModule;
	}
	void setVertexAnim(CVertexAnimationModule* anim);

	ReMesh* getReMesh(int lod);

	UINT getIndex() const {
		return m_index;
	}
	void setIndex(UINT index) {
		m_index = index;
	}

	//! \brief Return combined transformation matrix from relative position and orientation.
	Matrix44f getRelativeTransform() const;

	//! \brief Return ReMesh for collision computation.
	ReMesh* getCollisionReMesh();

	//! \brief Set mesh for collision computation.
	void setCollisionSource(ReMesh* apReMesh, BOOL abClone = FALSE);

	//! \brief Set mesh for collision computation.
	void setCollisionSource(CEXMeshObj* apExMesh);

	//! \brief Set mesh for collision computation.
	void setCollisionSource(int lodNo);

	bool getCanCollide() {
		return m_canCollide;
	}
	void setCanCollide(bool val) {
		m_canCollide = val;
	}

	bool getIsVisible() {
		return m_material != nullptr; // Visuals without a material cannot be rendered and exist only for collision.
	}

	void setUsePersistentMeshPool(bool val) { m_usePersistentMeshPool = val; }

protected:
	void clearCollisionSource();
};

/*!
 * \brief
 * Light weight dynamic object class
 *
 * This class is designed to replace CDynamicObj.  It contains collision, lights, and meta-data
 * necessary for placement of dynamic objects.  It integrates with the rest of the system via
 * CDynamicPlacementObj->m_baseObject.  Each dynamic object contains one or more visuals.
 *
 * \remarks
 * Write remarks for StreamableDynamicObject here.
 *
 * \see
 * StreamableDynamicObjectVisual | StreamableDynamicObjectSystem | CDynamicObjectPlacement
 */
class StreamableDynamicObject : public GetSet {
public:
	// Settings for TypedResourceManager
	const static ResourceType _ResType = ResourceType::DYNAMIC_OBJECT;

	static const unsigned char dyn_obj_archive_code = 8;

	StreamableDynamicObject();
	~StreamableDynamicObject();

private: // DRF - Put in order to see during debug
	std::string m_name;
	GLID m_LegacyGlid; // Glid for kaneva objects.
	CCollisionObj* m_pLegacyCollision; // legacy collision
	bool m_hasLegacyCollision; // true if legacy collision is not null when loaded from data file
	std::shared_ptr<Physics::CollisionShape> m_pCollisionShape;
	std::shared_ptr<Physics::CollisionShape> m_pVisualShape; // Shape for visual tests such as mouse picking and camera, not collision.
	Vector3f m_vCollisionShapeOffset = Vector3f(0, 0, 0);
	Vector3f m_vVisualShapeOffset = Vector3f(0, 0, 0);

	// Copy of collision triangle verts. These are redundant and wasteful of memory but we can't use the CollisionObj verts directly
	// because they do not use a fixed stride.
	// Only needed if bUseCollisionObject is true in StreamableDynamicObject::createCollisionShape().
	// The plan is to fix existing DOs so that we can discard the CollisionObj, at which time this object can be removed.
	std::vector<Vector3f> m_aCollisionTriangleVerts;

	StreamableDynamicObjectLight* m_light;
	bool m_attachable;
	int m_particleEffectId;
	Vector3f m_particleOffset;
	CTriggerObjectList* m_triggers;

	std::vector<StreamableDynamicObjectVisual*> m_visuals;
	std::vector<bool> m_visualCustomizable;
	std::vector<bool> m_visualMediaSupported;

	bool m_interactive;
	Vector3f m_lightPosition;
	BNDBOX m_boundBox;
	bool m_canObjectPlayFlash; // used for GETSET; derived canplayflash from all visuals
	bool m_objectHasCollision; // used for GETSET
	GLID m_skeletonGLID;
	std::shared_ptr<RuntimeSkeleton> m_pSkeleton;
	Vector3f m_skeletonAdditionalScale; // Additional scale on the skeleton. Should be multipled into RuntimeSkeleton::m_scaleVector when the skeleton is instantiated.
	int m_linkedMovObjNetTraceId;

public:
	void createCollisionShapes(); // Public for access by importer

	GLID getSkeletonGLID() const {
		return m_skeletonGLID;
	}
	void setSkeletonGLID(const GLID& glid) {
		m_skeletonGLID = glid;
	}

	void setSkeletonAdditionalScale(const Vector3f& scaleVec) {
		m_skeletonAdditionalScale = scaleVec;
	}
	Vector3f getSkeletonAdditionalScale() const {
		return m_skeletonAdditionalScale;
	}

	size_t getVisualCount() const {
		return m_visuals.size();
	}

	const std::string& getName() const {
		return m_name;
	}
	void setName(const char* name) {
		if (name)
			m_name = name;
	}

	Physics::CollisionShape* getCollisionShape() {
		return getCollisionShapeShared().get();
	}

	const std::shared_ptr<Physics::CollisionShape>& getCollisionShapeShared() {
		return m_pCollisionShape;
	}

	const std::shared_ptr<Physics::CollisionShape>& getVisualShapeShared() {
		return m_pVisualShape;
	}

	//void setCollisionShape(std::shared_ptr<Physics::CollisionShape> pCollisionShape) {
	//	m_pCollisionShape = std::move(pCollisionShape);
	//}

	const Vector3f& getCollisionShapeOffset() const {
		return m_vCollisionShapeOffset;
	}

	const Vector3f& getVisualShapeOffset() const {
		return m_vVisualShapeOffset;
	}

	//void setCollisionShapeOffset(const Vector3f& vCollisionShapeOffset) {
	//	m_vCollisionShapeOffset = vCollisionShapeOffset;
	//}

	StreamableDynamicObjectVisual* GetVisualFromCollisionData(size_t iRigidBody, int iPart);

	StreamableDynamicObjectLight* getLight() const {
		return m_light;
	}
	void setLight(StreamableDynamicObjectLight* light) {
		m_light = light;
	}
	void setLight(CRTLightObj* legacyLight);

	StreamableDynamicObjectVisual* Visual(unsigned int visNum) const {
		return (visNum < m_visuals.size()) ? m_visuals[visNum] : NULL;
	}

	void AddVisual(StreamableDynamicObjectVisual* vis, bool customizable, bool mediaSupported);
	bool DelVisual(int index);

	bool SetVisualCustomizable(unsigned int visNum, bool customizable);
	bool VisualCustomizable(unsigned int visNum) const;
	const std::vector<bool>& GetVisualCustomizableFlags() const { return m_visualCustomizable; }

	bool SetVisualMediaSupported(unsigned int visNum, bool mediaSupported);
	bool VisualMediaSupported(unsigned int visNum) const;

	void populateMediaSupported();
	bool MediaSupported() const {
		return m_canObjectPlayFlash;
	}

	void populateObjectHasCollision(); // sets m_objectHasCollision for GETSET

	bool getAttachable() const {
		return m_attachable;
	}
	void setAttachable(bool att) {
		m_attachable = att;
	}

	bool getInteractive() const {
		return m_interactive;
	}
	void setInteractive(bool inter) {
		m_interactive = inter;
	}

	int getParticleEffectId() const {
		return m_particleEffectId;
	}
	void setParticleEffectId(int effectId) {
		m_particleEffectId = effectId;
	}

	float getBoundingRadius() const;
	BNDBOX getBoundBox() const {
		return m_boundBox;
	}
	void setBoundBox(BNDBOX inBox) {
		m_boundBox.min = inBox.min;
		m_boundBox.max = inBox.max;
	}

	Vector3f getLightPosition() const {
		return m_lightPosition;
	}
	void setLightPosition(float x, float y, float z);

	Vector3f getParticleOffset() const {
		return m_particleOffset;
	}
	void setParticleOffset(float x, float y, float z);

	CTriggerObjectList* getTriggers() {
		return m_triggers;
	}
	void setTriggers(CTriggerObjectList* trg);

	std::unique_ptr<CRTLightObj> createLegacyLight();

	void setLinkedMovObjNetTraceId(int val) { m_linkedMovObjNetTraceId = val; }
	int getLinkedMovObjNetTraceId() const { return m_linkedMovObjNetTraceId; }

	static StreamableDynamicObject* readObject(CArchive& ar, const GLID& glidOverride, int* pAssetSchemaVersion = nullptr);
	static StreamableDynamicObject* readObjectV1(CArchive& ar, const GLID& glidOverride, int version);
	static StreamableDynamicObject* readObjectV2(CArchive& ar, const GLID& glidOverride, int version);

	void writeObject(CArchive& ar);
	void writeObject(BYTE*& dataBuffer, FileSize& dataLen);

	//! \brief Define collision source for all DynamicObjectVisuals
	void setCollisionSourceByLOD(int lodLevel);

	//! \brief Compute bounding box in local coordinate system from all visuals
	BNDBOX computeBoundingBox(const Matrix44f* world = NULL) const;

	bool isMeshInCollision(ReMesh* pCollisionMesh, std::vector<bool>* pabCollisionTrianglesMatched = nullptr);
	bool isVisualInCollision(StreamableDynamicObjectVisual* pVisual, ReMesh** ppCollisionMesh);
	int updateVisualCollisionFlags();
	bool createVisualFromCollisionData();

	static StreamableDynamicObject* convertFromAccessory(CArmedInventoryObj* accessory, const GLID& glid);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

} // namespace KEP
