/******************************************************************************
 MenusDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_MENUS_DLG_H_)
#define _MENUS_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MenusDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "TestIconPropertiesCtrl.h"
#include "CIconSystemClass.h"

////////////////////////////////////////////
// CMenusDialog control

class CMenusDialog : public CDialogBar {
	DECLARE_DYNAMIC(CMenusDialog)

public:
	CMenusDialog();
	virtual ~CMenusDialog();

	// Dialog Data
	//{{AFX_DATA(CMenusDialog)
	enum { IDD = IDD_DIALOG_MENUSYSTEM };
	//}}AFX_DATA

	// accessors
	int GetIconID() const {
		return m_propTestIcon.GetIconID();
	}
	float GetPositionX() const {
		return m_propTestIcon.GetPositionX();
	}
	float GetPositionY() const {
		return m_propTestIcon.GetPositionY();
	}

	// modifiers
	void SetIconID(int iconID) {
		m_propTestIcon.SetIconID(iconID);
	}
	void SetPositionX(float positionX) {
		m_propTestIcon.SetPositionX(positionX);
	}
	void SetPositionY(float positionY) {
		m_propTestIcon.SetPositionY(positionY);
	}

	// methods
	void SetIconList(CIconSysList* iconList) {
		m_propTestIcon.SetIconList(iconList);
	}

protected:
	// Generated message map functions
	//{{AFX_MSG(CMenusDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colMenu;
	CButton m_grpMenu;

	CTestIconPropertiesCtrl m_propTestIcon;
	CCollapsibleGroup m_colIcon;
	CButton m_grpIcon;

	CKEPImageButton m_btnAddMenu;
	CKEPImageButton m_btnEditMenu;
	CKEPImageButton m_btnDeleteMenu;
	CKEPImageButton m_btnSaveMenu;
	CKEPImageButton m_btnImportMenu;
	CKEPImageButton m_btnTestMenu;

	CKEPImageButton m_btnMenuCursor;
	CKEPImageButton m_btnScaleCursor;
	CKEPImageButton m_btnDeleteCursor;
	CKEPImageButton m_btnGetCursor;

	CKEPImageButton m_btnAddVisual;
	CKEPImageButton m_btnEditVisual;
	CKEPImageButton m_btnDeleteVisual;
	CKEPImageButton m_btnExportVisual;
	CKEPImageButton m_btnImportVisual;

	CKEPImageButton m_btnAddZone;
	CKEPImageButton m_btnEditZone;
	CKEPImageButton m_btnDeleteZone;
};

#endif !defined(_MENUS_DLG_H_)