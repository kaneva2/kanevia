///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class CStatisticObject : public CObject, public GetSet {
	DECLARE_SERIAL(CStatisticObject)

public:
	CStatisticObject();
	int m_type;
	CStringA m_statName;
	float m_currentAmount;
	float m_statCap;

	void Clone(CStatisticObject** copy);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CStatisticObjectList : public CObList {
public:
	CStatisticObjectList(){};
	void Clone(CStatisticObjectList** copy);
	void SafeDelete();
	CStatisticObject* GetStatObjectByType(int type);

	DECLARE_SERIAL(CStatisticObjectList)
};

class CAffectedByStatObject : public CObject {
	DECLARE_SERIAL(CAffectedByStatObject)

public:
	CAffectedByStatObject();
	float m_statisticalImprovement;
	float m_statisticalImprovementCap;
	int m_statType;
	float m_improvementOfSuccessVariable;

	void Clone(CAffectedByStatObject** copy);
	void Serialize(CArchive& ar);
};

class CAffectedByStatObjectList : public CObList {
public:
	CAffectedByStatObjectList(){};
	void SafeDelete();
	void Clone(CAffectedByStatObjectList** copy);

	DECLARE_SERIAL(CAffectedByStatObjectList)
};

class CSkillCostObject : public CObject {
	DECLARE_SERIAL(CSkillCostObject)

public:
	CSkillCostObject();
	int m_type;
	int m_costAmount;

	void Clone(CSkillCostObject** copy);
	void Serialize(CArchive& ar);
};

class CSkillCostObjectList : public CObList {
public:
	CSkillCostObjectList(){};
	void Clone(CSkillCostObjectList** copy);
	void SafeDelete();

	DECLARE_SERIAL(CSkillCostObjectList)
};

class CAbilityObject : public CObject {
	DECLARE_SERIAL(CAbilityObject)

public:
	CAbilityObject();
	CStringA m_name;
	int m_type;
	int m_miscInt;
	float m_minimumRequiredSkill; //must have at least this value in the skill to use ability
	float m_masterLimit; //this is when you basically never mess up
	CSkillCostObjectList* m_costOfAbilityList;
	float m_amountOfGainOnSuccess;
	//actions implemented by success
	int m_actionAttribute; //definable action upon success
	//0=none //1=generate resource //2=create item
	//3=use missile database //4=use blend mode on self
	int m_actionAttributeInt; //misc count or index or type or mode ect
	CStringA m_comment;
	int m_abilityDuration;
	BOOL m_abilityInUse;

	void SafeDelete();
	void Clone(CAbilityObject** copy);
	void Serialize(CArchive& ar);
};

class CAbilityObjectList : public CObList {
public:
	CAbilityObjectList(){};
	void SafeDelete();
	void Clone(CAbilityObjectList** copy);
	CAbilityObject* GetAbilityByType(int type);

	DECLARE_SERIAL(CAbilityObjectList)
};

class CBaseSkillObject : public CObject {
	DECLARE_SERIAL(CBaseSkillObject)

public:
	CBaseSkillObject();
	CStringA m_skillName;
	int m_skillType;
	CAffectedByStatObjectList* m_affectedByStatList;
	CAbilityObjectList* m_skillAbilities;
	float m_currentSkillValue;
	float m_skillCap;

	void SafeDelete();
	void Clone(CBaseSkillObject** clone);
	void Serialize(CArchive& ar);
};

class CBaseSkillObjectList : public CObList {
public:
	CBaseSkillObjectList(){};
	void SafeDelete();
	CBaseSkillObject* GetSkillByType(int type);
	void Clone(CBaseSkillObjectList** copy);

	DECLARE_SERIAL(CBaseSkillObjectList)
};

class CharacterDefObject : public CObject {
	DECLARE_SERIAL(CharacterDefObject)

public:
	CharacterDefObject();
	CStatisticObjectList* m_statistics;
	CBaseSkillObjectList* m_skills;
	int m_skillInUse; //-1 = none
	int m_abilityInUse; //-1 = none
	int m_inUseTimeStamp;
	int m_durationInUse;
	int m_misInput1;

	int AddStatistic(int type,
		CStringA name,
		float startAmount,
		float statCap);
	int UseSkill(int skillType,
		int abilityType,
		int* actionRet,
		int* actionIndexMiscRet,
		CStringA* commentRet);

	int AddAbility(int skillType,
		int type,
		int miscInt,
		float minimumRequiredSkill,
		float masterLimit,
		float gainBasis,
		CStringA name,
		CStringA comment,
		int duration);

	int AddSkill(CStringA skillName,
		int skillType,
		float initialSkillValue,
		float skillCap);

	void Clone(CharacterDefObject** copy);
	void SafeDelete();
	void Serialize(CArchive& ar);
};

class CharacterDefObjectList : public CObList {
public:
	CharacterDefObjectList(){};
	void SafeDelete();
	void Clone(CharacterDefObjectList** copy);

	DECLARE_SERIAL(CharacterDefObjectList)
};

} // namespace KEP