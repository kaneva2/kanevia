///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// added by serialize.pl
#include <GetSet.h>
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CSkeletonAnimation;
class CHiarchyMesh;
typedef CHiarchyMesh CHierarchyMesh;
class CBoneAnimationObject;
class CBoneAnimationList;
class CFrameObj;
class IMemSizeGadget;

class CBoneObject : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CBoneObject, VERSIONABLE_SCHEMA | 2)

private:
	CBoneAnimationList* m_animationList;

public:
	CBoneObject();

	CFrameObj* m_boneFrame;

	std::string m_parentName;
	std::string m_boneName;

	int m_procAnimmat;
	Matrix44f m_startPosition;
	Matrix44f m_startPositionInv;
	Matrix44f m_startPositionTranspose;
	std::vector<CHiarchyMesh*> m_rigidMeshes;
	int m_secondaryOverridable;

	BOOL m_referencedAnimList;

	// lastPositions used for interpolation
	BOOL m_lastPositionsAreInitialized[4];

	TimeMs m_lastToTargetTimeStamp[4];

	void Clone(CBoneObject** clone, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Clone(CBoneObject** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, bool cloneanimdata);
	void SafeDelete();

	void SetTransformDataForAnimation(Vector3f& lastPosition,
		Vector3f& lastUpVector,
		Vector3f& lastDirVector,
		Vector3f& targetPosition,
		Vector3f& targetUpVector,
		Vector3f& targetDirVector,
		Matrix44f scaleMatrix);

	void InterpolateKeyFrames(TimeMs elapsedTimeMs, Matrix44fA16* bonePtr, const CBoneAnimationObject* animBase, BOOL loop = TRUE);

	void Serialize(CArchive& ar);

	CHierarchyMesh* GetRigidMeshByName(const CStringA& meshName) const;
	size_t GetRigidMeshCount() const {
		return m_rigidMeshes.size();
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET

	friend class ARBImporter;
	friend class CSkeletonAnimation;
};

class CBoneList : public CObList {
public:
	CBoneList();
	void Clone(CBoneList** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, bool cloneanimdata = true) const;

	// Convert space to underscore for COLLADA compatibility
	CBoneObject* getBoneObjectByName(const std::string& name, int* index, bool convertSpaceToUnderscore = false);
	const CBoneObject* getBoneObjectByName(const std::string& name, int* index, bool convertSpaceToUnderscore = false) const;

	void SafeDelete();
	void BuildBoneList();
	void DestroyBoneList();

	CBoneObject* getBoneByIndex(int index);
	const CBoneObject* getBoneByIndex(int index) const;
	const Matrix44fA16* getBoneMatrices() const { return m_boneMats; }

	void computeBoneMatrices(const Matrix44fA16* invBoneMats, const Matrix44fA16* animBoneMats);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL_SCHEMA(CBoneList, VERSIONABLE_SCHEMA | 3)
	void Serialize(CArchive& ar);

protected:
	~CBoneList() {}

	template <typename ListPtrType, typename BonePtrType>
	static BonePtrType GetBoneObjectByName(ListPtrType list, const std::string& name, int* index, bool convertSpaceToUnderscore);

	template <typename ListPtrType, typename BonePtrType>
	static BonePtrType GetBoneByIndex(ListPtrType list, int index);

	//	Store bones as an array of matrices
	//	Added 1/9/2007, by SMM.
	//	Step 1 of the new direct memory access bone structure
	//	We'll fill this array at the same time as when we fill
	//	this list.
	//	TODO: Get rid of matrix ARB.
	//	TODO: Decide if we're going to stay with serializing.
	//	TODO: Uh. Lots.
	Matrix44fA16* m_boneMats;
};

} // namespace KEP
