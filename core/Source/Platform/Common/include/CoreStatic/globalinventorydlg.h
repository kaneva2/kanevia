///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "CMovementObj.h"
#include "CGlobalInventoryClass.h"
#include "CABFunctionLib.h"

#if !defined(AFX_GLOBALINVENTORYDLG_H__1B7AB481_4D70_11D5_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_GLOBALINVENTORYDLG_H__1B7AB481_4D70_11D5_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GlobalInventoryDlg.h : header file
//
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CGlobalInventoryDlg dialog

class CGlobalInventoryDlg : public CKEPEditDialog {
	// Construction
public:
	CGlobalInventoryDlg(CGlobalInventoryObjList*& globalInventoryDB, CWnd* pParent = NULL); // standard constructor

	CMovementObjList* m_entityDBRef;
	CGlobalInventoryObjList*& m_globalInventoryDBREF;

	// Dialog Data
	//{{AFX_DATA(CGlobalInventoryDlg)
	enum { IDD = IDD_DIALOG_GLOBALINVENTORYSYS };

	CListBox m_globalInventoryDB;
	int m_globalIDoutput;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGlobalInventoryDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListGlobalInventory(int sel);

	// Generated message map functions
	//{{AFX_MSG(CGlobalInventoryDlg)
	afx_msg BOOL OnInitDialog();
	afx_msg void OnButtonDelete();
	afx_msg void OnBUTTONGenerateFromEntityDB();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonSave();
	afx_msg void OnBUTTONcheckForconflicts();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	int m_glidEdit;
	afx_msg void OnBnClickedButtonSearchbyglid();

private:
	CCollapsibleGroup m_colGlobalInventory;
	CButton m_grpGlobalInventory;
	CKEPImageButton m_btnDeleteInventory;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLOBALINVENTORYDLG_H__1B7AB481_4D70_11D5_B1EE_0000B4BD56DD__INCLUDED_)
