///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "CABFunctionLib.h"
#include "CSoundManagerClass.h"
#if !defined(AFX_SOUNDDATABASEDLG_H__B7FDADE1_4491_11D5_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_SOUNDDATABASEDLG_H__B7FDADE1_4491_11D5_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SoundDatabaseDlg.h : header file
//
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "resource.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CSoundDatabaseDlg dialog

class CSoundDatabaseDlg : public CKEPEditDialog {
	// Construction
public:
	CSoundDatabaseDlg(CSoundBankList*& m_soundManager,
		CWnd* pParent = NULL); // standard constructor
	CSoundBankList*& m_soundManagerRef;
	IDirectSound8* directSoundRef;
	CStringA m_ConfigPath;

	// Dialog Data
	//{{AFX_DATA(CSoundDatabaseDlg)
	enum { IDD = IDD_DIALOG_SOUNDDATABASE };
	CListBox m_soundBank;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSoundDatabaseDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListSoundFiles(int sel);
	// Generated message map functions
	//{{AFX_MSG(CSoundDatabaseDlg)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonLoad();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CCollapsibleGroup m_colSounds;
	CButton m_grpSounds;

	CKEPImageButton m_btnAddSound;
	CKEPImageButton m_btnDeleteSound;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOUNDDATABASEDLG_H__B7FDADE1_4491_11D5_B1EE_0000B4BD56DD__INCLUDED_)
