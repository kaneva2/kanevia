///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class CResourceObject : public CObject, public GetSet {
	DECLARE_SERIAL(CResourceObject);

public:
	CResourceObject();

	int m_type;
	int m_amount;
	CStringA m_resourceName;
	BOOL m_enableMaxAmountVar;
	int m_maxAmount;
	int m_regenRatePerSecond;
	BOOL m_regenEnable;
	long m_lastUpdatedTime;

	void Clone(CResourceObject** copy);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CResourceObjectList : public CObList {
public:
	CResourceObjectList(){};
	void Clone(CResourceObjectList** copy);
	int AddResource(CStringA resName,
		int type,
		int amount,
		BOOL enableMaxVar,
		int maxAmount,
		BOOL regenEnable,
		int regenRatePersecond);
	CResourceObject* GetByType(int type);
	void RegenerativeCallback(TimeMs curTimeMs);
	BOOL UseResource(int type, int amount);
	BOOL VerifyResource(int type, int amount);
	void SafeDelete();

	DECLARE_SERIAL(CResourceObjectList);
};

} // namespace KEP