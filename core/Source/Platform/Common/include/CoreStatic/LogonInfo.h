///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <string>
#include <memory>

#include "Core/Util/HighPrecisionTime.h"

class LogonInfo {
public:
	LogonInfo(
		const char* _userName,
		const char* _password,
		char _gender,
		const char* _birthDate,
		bool _showMature,
		const char* _country,
		const char* _ipAddress,
		ULONG _serverId,
		ULONG _roleId,
		ULONG _protocolVersion) :
			userName(_userName),
			password(_password),
			gender(_gender),
			birthDate(_birthDate),
			showMature(_showMature),
			country(_country),
			ipAddress(_ipAddress),
			timestamp(fTime::TimeMs()),
			serverId(_serverId),
			roleId(_roleId),
			protocolVersion(_protocolVersion) {}

	LogonInfo(const LogonInfo& src) {
		operator=(src);
	}

	LogonInfo& operator=(const LogonInfo& src) {
		if (this != &src) {
			userName = src.userName;
			password = src.password;
			gender = src.gender;
			birthDate = src.birthDate;
			showMature = src.showMature;
			country = src.country;
			ipAddress = src.ipAddress;
			timestamp = src.timestamp;
			serverId = src.serverId;
			roleId = src.roleId;
			protocolVersion = src.protocolVersion;
		}
		return *this;
	}

	std::string userName;
	std::string password;
	char gender;
	std::string birthDate; //< db format
	bool showMature;
	std::string country;
	std::string ipAddress;
	TimeMs timestamp;
	ULONG serverId;
	ULONG roleId;
	ULONG protocolVersion;
};

typedef std::shared_ptr<LogonInfo> LogonInfoPtr;
typedef std::vector<LogonInfoPtr> LogonInfoVect; //< vector of users who have completed first pass at login sequence on DPThread
