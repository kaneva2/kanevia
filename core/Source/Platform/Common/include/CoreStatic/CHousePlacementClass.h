///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "InstanceId.h"
#include "CStructuresMisl.h"
#include "KEPObList.h"

namespace KEP {

const int PLACEMENT_FAILED = 0;
const int PLACEMENT_OK = 1;
const int PLACEMENT_TOO_CLOSE = 5;

class CHousingObjList;
class CInventoryObjList;

class CHPlacementObj : public CObject, public GetSet {
	DECLARE_SERIAL(CHPlacementObj);

public:
	CHPlacementObj();

	int m_type; //DB reference
	PLAYER_HANDLE m_playerHandle; // 6/06 update to be player handle
	Vector3f m_wldPosition;
	int m_id;
	BOOL m_open;
	BOOL m_vaultInstalled;
	int m_vaultItemCapacity;
	int m_vaultCurrentCash;
	CInventoryObjList* m_vaultInventory;
	int m_storeItemCapacity;
	int m_storeCurrentCash;
	CInventoryObjList* m_storeInventory;

	void SafeDelete();
	void Serialize(CArchive& ar);
	void Clone(CHPlacementObj** clone);

	DECLARE_GETSET
};

class CHPlacementObjList : public CObList {
public:
	CHPlacementObjList(){};
	CHPlacementObj* GetByPlayerHandle(PLAYER_HANDLE playerHandle);
	BOOL AddPlacement(int type, PLAYER_HANDLE playerHandle, Vector3f position, int id);
	BOOL RemoveByPlayerHandle(PLAYER_HANDLE playerHandle);
	CHPlacementObj* GetHouseByBox(Vector3f position);
	BOOL BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max);
	void Clone(CHPlacementObjList** clone);

	void SafeDelete();

	DECLARE_SERIAL(CHPlacementObjList);
};

struct Placement {
	CHPlacementObjList* m_placementList;
};

class CHousingZone : public CObject {
	DECLARE_SERIAL(CHousingZone);

public:
	CHousingZone();
	void SafeDelete();

	CStringA m_zoneLabel;
	BNDBOX m_visibleBox;
	BNDBOX m_zoneBox;
	float m_subDivisionX;
	float m_subDivisionY;
	float m_subDivisionZ;
	ChannelId m_channel;
	float m_densityAllowance;

	BOOL m_optimizedSave;

	Placement*** m_hsTable;

	CHPlacementObjList* m_houseDatabase;

	void Serialize(CArchive& ar);
	void Clone(CHousingZone** clone);
	void CreateNewHousingZone(CStringA zoneName,
		float subX,
		float subY,
		float subZ,
		const ChannelId& channel,
		BNDBOX box,
		BNDBOX visibleBox);
	BOOL BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max);

	// returns PLACEMENT_*
	int AddPlacementObjToTable(int type,
		PLAYER_HANDLE playerHandle,
		BNDBOX& m_clipBoxOfObjectPlaced,
		Vector3f& wldPosition,
		int id);
	int AddPlacementObjToTable(CHPlacementObj* placement, BNDBOX& clipBox);
	int PrepareToAddPlacementObjToTable(int type,
		PLAYER_HANDLE playerHandle,
		BNDBOX& clipBoxOfObjectPlaced,
		Vector3f& wldPosition,
		int id);
	CHPlacementObjList* GetContainerByWldPosition(Vector3f position);
	BOOL RemoveByPlayerHandle(PLAYER_HANDLE playerHandle);
	CHPlacementObjList* GetVisibleList(Vector3f wldPosition, float range);
	float MagnitudeSquared(float& Mx1, float& My1,
		float& Mz1, float& Mx2,
		float& My2, float& Mz2);
	BOOL AnyHousesWithinArea(Vector3f position, float radius);
};

class CHousingZoneDB : public CKEPObList {
public:
	CHousingZoneDB();
	void SafeDelete();

	DECLARE_SERIAL(CHousingZoneDB);

	int m_currentIDCount;

	// move the placements from "from" into this
	// DB.  Any ones left behind need to be cleaned up
	void MergePlacementsFrom(CHousingZoneDB* from, CHousingObjList* housingDB);

	int AddPlacementObjToTable(int type,
		PLAYER_HANDLE playerHandle,
		BNDBOX& clipBoxOfObjectPlaced,
		Vector3f& wldPosition,
		int* idReturn,
		const ChannelId& channel);
	CHPlacementObjList* GetVisibleList(Vector3f wldPosition, float range, const ChannelId& channel);
	BOOL BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max);
	CHPlacementObjList* GetContainer(Vector3f wldPosition);
	CHPlacementObjList* GetContainerAndZone(Vector3f wldPosition, CHousingZone** zone);
	CHPlacementObj* GetHousePlacementByPosPlayerSpecial(Vector3f wldPosition, PLAYER_HANDLE playerHandle, const ChannelId& channel);
	CHPlacementObj* GetHousePlacementByPosPlayer(Vector3f wldPosition, PLAYER_HANDLE playerHandle);
	void RemoveHouseByPlayerHandle(PLAYER_HANDLE playerHandle);
	int RemoveHouseByPosition(Vector3f wldPosition, const ChannelId& channel);
	CHPlacementObj* GetHousePlacementByPos_area(Vector3f wldPosition);
	void Clone(CHousingZoneDB** clone);
	CHPlacementObj* GetHouseByPlayerHandle(PLAYER_HANDLE playerHandle);
	CHPlacementObj* GetHouseByPositionAndWldID(Vector3f wldPosition, const ChannelId& channel);
	CHPlacementObj* GetHousePlacementByPosPlayer(Vector3f wldPosition, PLAYER_HANDLE playerHandle, ChannelId* retChannel);
};

} // namespace KEP