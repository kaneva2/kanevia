///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\KEPUtil\Helpers.h"

#include "GetSet.h"

#include <KEPNetwork.h>
#include "KEPConstants.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include "InstanceId.h"
#include "SpawnInfo.h"

#define ELA_FAIL 0
#define ELA_LOGON 1
#define ELA_RECONNECT 2

namespace KEP {

class CGlobalInventoryObjList;
class CPlayerObject;
class CPlayerObjectList;
class CSkillObjectList;
class SpawnInfo;

// Pending/future PlayerDeparted event for a player
struct PendingPlayerDepartInfo {
	NETID netID; // NETID to be used in a future PlayerDeparted event (recorded when a PlayerArrived event is sent)
	ZoneIndex zoneIndex; // Zone index to be used in a future PlayerDeparted event (recorded when a PlayerArrived event is sent)
	TimeMs departTimestamp; // Time when player leaves the zone (if set, will cause a backup PlayerDeparted event to be sent upon timeout)
	unsigned playerArriveId;
	PendingPlayerDepartInfo() :
			netID(0), departTimestamp(0), playerArriveId(0) {}
};

class IGameState;

// ACCTSTAT constants are probably no longer needed (see m_accountStatus)
const int ACCTSTAT_OK = 0;
const int ACCTSTAT_SUSPENDED_NON_PAYMENT = 1;
const int ACCTSTAT_SUSPENDED_VIOLATION = 2;

class CAccountObject : public CObject, public GetSet {
protected:
	CAccountObject() :
			m_serverId(0), m_scriptServerId(0), m_scriptServerIdPrev(0), m_showMature(false), m_age(0), m_gender('U') {}

	DECLARE_SERIAL_SCHEMA(CAccountObject, VERSIONABLE_SCHEMA | 2) // version 2 added server (Kaneva) id

public:
	static const TimeMs BAD_EL_TIME;

	CAccountObject(const CStringA& name, const CStringA& password);
	~CAccountObject();

	SpawnInfo& GetSpawnInfo() { return m_pendingSpawnInfo; }
	const SpawnInfo& GetSpawnInfo() const { return m_pendingSpawnInfo; }

	void SetPendingPlayerDepartInfo(NETID netID, ZoneIndex zoneIndex, unsigned playerArriveId) {
		m_pendingPlayerDepartInfo.netID = netID;
		m_pendingPlayerDepartInfo.zoneIndex = zoneIndex;
		m_pendingPlayerDepartInfo.departTimestamp = 0; // Not yet depart
		m_pendingPlayerDepartInfo.playerArriveId = playerArriveId;
	}

	void RecordPlayerDepartTime() {
		if (m_pendingPlayerDepartInfo.departTimestamp == 0)
			m_pendingPlayerDepartInfo.departTimestamp = fTime::TimeMs();
	}

	// Read and clear PendingPlayerDepartInfo if zone index is different. Pass 0 for newZoneIndex if player is leaving the server.
	bool FetchPendingPlayerDepartInfo(const ZoneIndex& newZoneIndex, NETID& netID, ZoneIndex& departZoneIndex, unsigned& playerArriveId) {
		if (m_pendingPlayerDepartInfo.netID != 0 && // Player was here
			m_pendingPlayerDepartInfo.zoneIndex != newZoneIndex) // Player is going to a different zone
		{
			// Fetch
			netID = m_pendingPlayerDepartInfo.netID;
			departZoneIndex = m_pendingPlayerDepartInfo.zoneIndex;
			playerArriveId = m_pendingPlayerDepartInfo.playerArriveId;
			// And clear
			ClearPendingPlayerDepartInfo();
			return true;
		}

		return false;
	}

	// Read and clear PendingPlayerDepartInfo if expired
	bool FetchExpiredPendingPlayerDepartInfo(TimeMs duration, NETID& netID, ZoneIndex& departZoneIndex, unsigned& playerArriveId) {
		if (m_pendingPlayerDepartInfo.departTimestamp != 0 && // Player had left (pending re-spawn at the same server)
			fTime::ElapsedMs(m_pendingPlayerDepartInfo.departTimestamp) >= duration) // Player had left for too long and still hadn't re-spawned
		{
			static ZoneIndex ziZero;
			return FetchPendingPlayerDepartInfo(ziZero, netID, departZoneIndex, playerArriveId);
		}

		return false;
	}

	void ClearPendingPlayerDepartInfo() {
		m_pendingPlayerDepartInfo.netID = 0;
		m_pendingPlayerDepartInfo.zoneIndex.fromLong(0);
		m_pendingPlayerDepartInfo.departTimestamp = 0;
		m_pendingPlayerDepartInfo.playerArriveId = 0;
	}

	CStringA m_accountName; //plaers name
	CStringA m_accountPassword;
	BOOL m_loggedOn; //online status
	BOOL m_sentLogoff; // have we let others know we've logged off?
	int m_accountStatus; //0=ok 1=suspended for payment reasons 2=suspended for violation -- PROBABLY legacy (in-memory only status, does not come from DB)
	CStringA m_suspentionNotes; //description of suspension reasons
	NETID m_netId; //
private:
	ULONG m_currentProtocolVersion; // Negotiated protocol version for current session
public:
	BOOL m_registeredVersion;
	long m_timePlayedMinutes;
	long m_tempPlayedTimeMilliseconds;
	std::string m_currentIP;

	Timer m_logonTimer;

	int m_currentcharInUse; //-1 = none
	int m_accountNumber; //

	BOOL m_aiServer;

	CPlayerObjectList* m_pPlayerObjectList;

	short m_securityIndexer;

	BOOL m_administratorAccess;
	BOOL m_gm;
	BOOL m_canSpawn;

	BOOL m_initializedHousingStates;
	int m_arenaLiscense;

	//extra vars
	int m_localClientContentVersion;

	//status info
	CStringA m_realName; //as appears on credit card
	CStringA m_email;
	int m_yearOfLastLogon;
	int m_monthOfLastLogon;
	int m_dayOfLastLogon;
	int m_hourOfLastLogon;
	int m_minuteOfLastLogon;

	//validity-to play
	int m_refreshPaymentYear;
	int m_refreshPaymentMonth;
	int m_refreshPaymentDay;
	int m_paidMonths;

	ULONG m_serverId; // KEP server id used when pushing accounts down from server
	ULONG m_scriptServerId;
	ULONG m_scriptServerIdPrev;

	// data related to age and restrictions
	bool m_showMature; // if > 18, do they want to see mature stuff
	int m_age;
	char m_gender; // M/F/U(nknown)
	std::string m_country; //< 2 char country code
	std::string m_birthDate; // in db format

	void StampLogOn();
	BOOL IsAccountUpToDateOnPayments(int buffer);
	void SafeDelete();

	CPlayerObject* GetPlayerObjectByIndex(int index);
	CPlayerObject* GetCurrentPlayerObject();
	CPlayerObject* GetCurrentPlayerObjectOrDefault();

	void SetOptimizedSave(BOOL optimize);
	void MakePayment(int monthsPaid);
	void RestoreAccountFromOptimized(CSkillObjectList* skillDB, CGlobalInventoryObjList* globalInven);
	void WriteDebugFile(CStringA filename, CStringA debugString);
	CStringA ConvLongToString(DWORD number);
	void SerializeAlloc(CArchive& ar);
	void Serialize(CArchive& ar);

	void getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s);

	ULONG getCurrentProtocolVersion() const {
		return m_currentProtocolVersion;
	}
	void setCurrentProtocolVersion(ULONG version) {
		m_currentProtocolVersion = version;
	}

private:
	SpawnInfo m_pendingSpawnInfo;

	//
	// NOTE: we don't explicitly issue departure event if the player is zoning within same server. Instead, the engine will issue a
	// player arrival event upon receiving ZoneCustomizationDLCompleteEvent from client, which will subsequently call departure event
	// handler. It resulted in an issue that if the new ZoneCustDLCompleteEvent is never received for whatever reason (e.g. player
	// disconnects, player rezones before zoneCustDL completion, etc.), the old zone will never receive departure event. It has caused
	// lua VM leak in the scripting engine.
	//
	// To address this issue, I'm recording additional data in m_pendingPlayerDepartInfo. It will be reset if ZoneCustomDLCompleteEvent is
	// eventually received. If not, ServerEngine::Callback will fire the departure event after timeout. -YC082014
	PendingPlayerDepartInfo m_pendingPlayerDepartInfo;

	DECLARE_GETSET
};

class CAccountObjectList : public CObList {
public:
	CAccountObjectList() :
			m_gameStateDb(0) {
	}

	void SafeDelete();
	PLAYER_HANDLE GetNewLocalPlayerHandleID();

	bool SafeDeleteAccount(CAccountObject* pAO);

	int LogOn(
		const char* userName,
		const char* password,
		const char* currentIP,
		CAccountObject** returnPtr,
		CAccountObjectList* referenceAccountList,
		ULONG serverId);

	int LogOut(CAccountObject* account, long* minutesConnected);

	void RemoveAccountByAccNumWithoutDel(int number);
	int EstablishLinkToAccount(CStringA userName, CStringA password, NETID currentDPNID, CAccountObject** cAccntPtr);
	void SetOptimizedSave(BOOL optimize);
	void ExpandFromOptimized(CSkillObjectList* skillDB, CGlobalInventoryObjList* globalInven);
	void FlagAllForOptimizedSave();
	void GenerateLogonRecordBasedOnTime(int minutes, int hours, int days, int months, int years);
	CStringA ConvLongToString(DWORD number);
	void MakePaymentByUsernameAndPass(CStringA username, CStringA password, int monthsPaid);
	void RemoveAccountBasedOnTime(int days, BOOL zeroMinOnly);
	void ReinitMemory();
	void GenerateAccountDataFile();
	int GetUntakenAccountNumber();

	CAccountObject* GetAccountObjectByNetId(const NETID& netId) const;
	CAccountObject* GetAccountObjectByAccountNumber(int number) const;
	CAccountObject* GetAccountObjectByPlayerHandle(const PLAYER_HANDLE& handle) const;
	CAccountObject* GetAccountObjectByUsername(const std::string& username) const;
	CAccountObject* GetAccountObjectByServerId(ULONG serverId) const;

	CPlayerObject* GetPlayerObjectByPlayerHandle(const PLAYER_HANDLE& handle) const;

	DECLARE_SERIAL(CAccountObjectList)

	IGameState* m_gameStateDb;
};

} // namespace KEP