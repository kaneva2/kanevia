///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CAdvancedVertexClass.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class CContainmentObject : public CObject, public GetSet {
	DECLARE_SERIAL(CContainmentObject)

public:
	CContainmentObject();

	int m_containmentBasis;
	int m_miscType;
	long m_respawnTime;
	TimeMs m_lastRespawnTime;
	int m_currentAmount;
	int m_maxAmount;
	int m_dispersalAmount;

	int UseIt();
	void Clone(CContainmentObject** copy);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CContainmentObjectList : public CObList {
public:
	CContainmentObjectList(){};
	void SafeDelete();

	DECLARE_SERIAL(CContainmentObjectList)
};

} // namespace KEP