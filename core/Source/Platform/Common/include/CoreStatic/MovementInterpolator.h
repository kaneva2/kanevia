#pragma once

#include <algorithm>
#include "include\core\math\Vector.h"
#include "Core/Util/HighPrecisionTime.h"
#include <mutex>

namespace KEP {

typedef std::recursive_mutex miMutex;
typedef std::lock_guard<miMutex> miMutexLock;

struct MovementData {
	Vector3f pos;
	Vector3f dir;
	Vector3f up;

	MovementData(
		const Vector3f& pos = Vector3f(0.0, 0.0, 0.0),
		const Vector3f& dir = Vector3f(0.0, 0.0, 0.0),
		const Vector3f& up = Vector3f(0.0, 0.0, 0.0)) :
			pos(pos),
			dir(dir), up(up) {}

	MovementData operator+(const MovementData& rhs) const {
		return MovementData(pos + rhs.pos, dir + rhs.dir, up + rhs.up);
	}

	MovementData operator-(const MovementData& rhs) const {
		return MovementData(pos - rhs.pos, dir - rhs.dir, up - rhs.up);
	}

	MovementData operator*(double rhs) const {
		return MovementData(pos * rhs, dir * rhs, up * rhs);
	}

	MovementData operator/(double rhs) const {
		return MovementData(pos / rhs, dir / rhs, up / rhs);
	}
};

class MovementInterpolator {
	mutable miMutex m_mutex;

	bool m_initialized = false;

	Timer m_updateTimer; // update timer
	TimeMs m_updateTimeExpMs = 210.0; // expected update interval (ms)
	TimeMs m_updateTimeMaxMs = 300.0; // maximum update interval (ms)
	TimeMs m_updateTimeInitMs = 1000.0; // re-init update interval (ms)

	float m_posSpeedMax = 0.3f; // max speed (feet per ms, ~204 mph)

	MovementData m_mdS; // movement data start
	MovementData m_mdE; // movement data end
	MovementData m_mdV; // movement data velocity (feet per ms)

public:
	MovementInterpolator& operator=(const MovementInterpolator& rhs) {
		miMutexLock lock(rhs.m_mutex);
		m_initialized = rhs.m_initialized;
		m_updateTimer = rhs.m_updateTimer;
		m_updateTimeExpMs = rhs.m_updateTimeExpMs;
		m_updateTimeMaxMs = rhs.m_updateTimeMaxMs;
		m_updateTimeInitMs = rhs.m_updateTimeInitMs;
		m_posSpeedMax = rhs.m_posSpeedMax;
		m_mdS = rhs.m_mdS;
		m_mdE = rhs.m_mdE;
		m_mdV = rhs.m_mdV;
		return *this;
	}

	void Update(const MovementData& md, bool init = false) {
		miMutexLock lock(m_mutex);
		bool doInit = (!m_initialized || init || (m_updateTimer.ElapsedMs() > m_updateTimeInitMs));
		m_mdS = doInit ? md : InterpolatedMovementData();
		m_mdE = md;
		m_mdV = (m_mdE - m_mdS) / m_updateTimeExpMs;
		if (m_mdV.pos.Length() > m_posSpeedMax) {
			m_mdS = md;
			m_mdV = MovementData();
		}
		m_updateTimer.Reset();
		m_initialized = true;
	}

	MovementData InterpolatedMovementData() const {
		MovementData md;
		{
			miMutexLock lock(m_mutex);
			TimeMs updateTimeMs = std::min<TimeMs>(m_updateTimer.ElapsedMs(), m_updateTimeMaxMs);
			md = m_mdV * updateTimeMs + m_mdS;
		}
		md.dir.Normalize();
		md.up.Normalize();
		return md;
	}

	Vector3f InterpolatedPos() const {
		return InterpolatedMovementData().pos;
	}

	Vector3f InterpolatedDir() const {
		return InterpolatedMovementData().dir;
	}

	Vector3f InterpolatedUp() const {
		return InterpolatedMovementData().up;
	}

	float InterpolatedPosSpeed() const {
		miMutexLock lock(m_mutex);
		return m_mdV.pos.Length();
	}

	float InterpolatedRotDeg() const {
		Vector3f dir = InterpolatedDir();
		float PI = 3.14159265f;
		float rotDeg = atan2(dir.x, dir.z) * 180.0f / PI;
		if (rotDeg < 0.0f)
			rotDeg += 360.0f;
		return rotDeg;
	}
};

} // namespace KEP