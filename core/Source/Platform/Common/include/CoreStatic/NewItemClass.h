///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#pragma once

class NewItemObject {
	typedef struct _meshSlotInfo {
		int slotindex;
		int lodCount;
		unsigned __int64 meshLODHash[6]; // right now charles's mesh hashes are unsigned __int64, Jim wants to shorten these
	} meshSlotInfo;

	typedef struct _materialInfo {
		int materialId; // referencing complex material with advanced settings
		char texFile1[255];
		char texFile2[255]; // support for multitexturing
		char red;
		char green;
		char blue;
		char alpha;
	} materialInfo;

	typedef struct _charMeshInfo {
		meshSlotInfo meshSlots[4]; // 4 mesh flips per item currently
		materialInfo materials[4]; // each flip has independant material
		// reserve ability to expand this to handle costumes.
	} charMeshInfo;

	typedef struct _dynamicObjMeshInfo {
		meshSlotInfo meshInfo[2]; // dynamic obj's can have LODs
		materialInfo material[2]; // same type of material
		// These have 2 slots because of TV's one is possible a screen.
	} dynamicObjMeshInfo;

	typedef struct _itemData {
		charMeshInfo cmeshinfo; // If its a normal "DIM" based item it has meshinfo
		dynamicObjMeshInfo omeshinfo; // If this is a dynamic object
		int equippableItemId; // If its an "APD" equippable item it has an id referring to the "skeleton" that makes up the item
		int GLID; // global unique id for this item
		char itemName[256]; // "short name"
		char itemDescription[1024]; // long description
		char author[128]; // person who created this item default "kaneva"

		// Other Itemey type stuff
		int price; // Should this be stored here?
		char creditType; // credits only?
		char passes; // access pass?
		// Server specific stuff?

	} itemData;

	NewItemObject();
	~NewItemObject();
};

