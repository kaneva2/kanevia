/******************************************************************************
 TextFontDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_TEXTFONT_DLG_H_)
#define _TEXTFONT_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextFontDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "TextFontPropertiesCtrl.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

////////////////////////////////////////////
// CTextFontDialog control

class CTextFontDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CTextFontDialog)

public:
	CTextFontDialog();
	virtual ~CTextFontDialog();

	// Dialog Data
	//{{AFX_DATA(CTextFontDialog)
	enum { IDD = IDD_DIALOG_TEXTINTERFACE };
	//}}AFX_DATA

	// accessors
	CStringA GetFontName() const {
		return m_propTextFont.GetFontName();
	}
	int GetFontTableTextureID() const {
		return m_propTextFont.GetFontTableTextureID();
	}

	// modifiers
	void SetFontName(CStringA sFontName) {
		m_propTextFont.SetFontName(sFontName);
	}
	void SetFontTableTextureID(int fontTableTextureID) {
		m_propTextFont.SetFontTableTextureID(fontTableTextureID);
	}

	// methods
	void AddFontTableTexture(CStringA sTexture) {
		m_propTextFont.AddFontTableTexture(sTexture);
	}
	void ClearFontTableTextures() {
		m_propTextFont.ClearFontTableTextures();
	}

protected:
	// Generated message map functions
	//{{AFX_MSG(CTextFontDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colTextFont;
	CButton m_grpTextFont;
	CTextFontPropertiesCtrl m_propTextFont;

	CKEPImageButton m_btnAdd;
	CKEPImageButton m_btnDelete;
	CKEPImageButton m_btnLoad;
	CKEPImageButton m_btnSave;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_TEXTFONT_DLG_H_)