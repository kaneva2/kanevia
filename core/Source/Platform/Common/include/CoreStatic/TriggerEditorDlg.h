///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRIGGEREDITORDLG_H__4FB4D621_FFF8_11D3_B1E8_0000B4BD56DD__INCLUDED_)
#define AFX_TRIGGEREDITORDLG_H__4FB4D621_FFF8_11D3_B1E8_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TriggerEditorDlg.h : header file
//
#include "CTriggerClass.h"
#include "afxwin.h"
/////////////////////////////////////////////////////////////////////////////
// CTriggerEditorDlg dialog

class CTriggerEditorDlg : public CDialog {
	// Construction
public:
	CTriggerEditorDlg(CWnd* pParent = NULL); // standard constructor
	CTriggerObjectList* m_triggerListRef;

	// Dialog Data
	//{{AFX_DATA(CTriggerEditorDlg)
	enum { IDD = IDD_DIALOG_TRIGGER };
	CListBox m_triggerDatabase;
	float m_dataRadius;
	int m_miscInt;
	CStringA m_miscTest;
	float m_miscVectX;
	float m_miscVectY;
	float m_miscVectZ;
	float m_originX;
	float m_originY;
	float m_originZ;
	CComboBox m_combo;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTriggerEditorDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	CStringA ConvertLongToString(int number);
	void InListTriggers();
	// Generated message map functions
	//{{AFX_MSG(CTriggerEditorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAddtrigger();
	afx_msg void OnBUTTONDeleteTRIGGER();
	afx_msg void OnBUTTONReplaceTRIGGER();
	afx_msg void OnBUTTONBROWSEfile();
	afx_msg void OnSelchangeLISTTriggerList();
	afx_msg void OnSelchangeCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CButton m_doOnlyOnce;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRIGGEREDITORDLG_H__4FB4D621_FFF8_11D3_B1E8_0000B4BD56DD__INCLUDED_)
