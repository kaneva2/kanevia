/******************************************************************************
 ScriptDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SCRIPT_DLG_H_)
#define _SCRIPT_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScriptDlg.h : header file
//

namespace KEP {

class IKEPDialogProcessor;

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "Common\UIStatic\BasicLayoutMFC.h"

////////////////////////////////////////////
// CScriptDialog control

class CScriptDialog : public CDialogBar {
	DECLARE_DYNAMIC(CScriptDialog)

public:
	CScriptDialog();
	virtual ~CScriptDialog();

	BOOL SetType(UINT dlgType);
	UINT GetType();

	void SetFileName(CStringA fileName) {
		m_fileName = fileName;
	}
	CStringA GetFileName() {
		return m_fileName;
	}

	// Dialog Data
	//{{AFX_DATA(CScriptDialog)
	enum { IDD = IDD_DIALOG_SCRIPTING };
	//}}AFX_DATA

	// Code can function on three different
	// script types, made to be bit flags for use in dialog
	enum SCRIPT_DIALOG_TYPES { GAME = 1,
		ZONE = 2,
		AI = 4,
		CLIENT = 8 };

protected:
	// Generated message map functions
	//{{AFX_MSG(CScriptDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	UINT m_dlgType;
	CStringA m_fileName;

	CCollapsibleGroup m_colScript;
	CButton m_grpScript;

	CKEPImageButton m_btnAddEvent;
	CKEPImageButton m_btnDeleteEvent;
	CKEPImageButton m_btnBuildExec;
	CKEPImageButton m_btnEditEvent;
	CKEPImageButton m_btnLoadFromScene;
	CKEPImageButton m_btnClearScene;

	BasicLayoutMFC m_layout;
};

} // namespace KEP

#endif !defined(_SCRIPT_DLG_H_)
