///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Glids.h"
#include "Core/Util/HighPrecisionTime.h"

#define ANIM_UPDATE_DELAY_MS_MIN (TimeMs)15.0 // approx 60hz
#define ANIM_BLEND_DURATION_MS 200.0 // same as expected movement update time (ms)

const int ANIM_GLID_UGC = GLID_LOCAL_BASE; // to be removed
const int ANIM_INVALID = -1; // invalid animation attrib (not glid)
#define IS_VALID_ANIM(a) (a != ANIM_INVALID)

const int ANIM_PRI = 0;
const int ANIM_PRI_BLEND = 1;
const int ANIM_SEC = 2;
const int ANIM_SEC_BLEND = 3;

enum class eAnimLoopCtl : int {
	Default = 0, // default looping (defined in SkeletonAnimHeader)
	LoopOff = 1, // disable looping
	LoopOn = 2 // enable looping
};

struct AnimSettings {
	GLID m_glid;
	bool m_dirForward;
	eAnimLoopCtl m_loopCtl;
	TimeMs m_blendDurationMs;

	AnimSettings(
		GLID glid = GLID_INVALID,
		bool dirForward = true,
		eAnimLoopCtl loopCtl = eAnimLoopCtl::Default,
		TimeMs blendDurationMs = ANIM_BLEND_DURATION_MS) :
			m_glid(glid),
			m_dirForward(dirForward),
			m_loopCtl(loopCtl),
			m_blendDurationMs(blendDurationMs) {}

	bool operator==(const AnimSettings& rhs) const {
		return (m_glid == rhs.m_glid) && (m_dirForward == rhs.m_dirForward) && (m_loopCtl == rhs.m_loopCtl) && (m_blendDurationMs == rhs.m_blendDurationMs);
	}

	bool isValid() const {
		return IS_VALID_GLID(m_glid);
	}

	// glidSpecial < 0 = reverse animation
	void setGlidSpecial(GLID glidSpecial) {
		m_glid = abs(glidSpecial);
		m_dirForward = (glidSpecial >= 0);
	}
	GLID getGlidSpecial() const {
		return m_dirForward ? m_glid : -m_glid;
	}

	void setLoopCtl(eAnimLoopCtl loopCtl) {
		m_loopCtl = loopCtl;
	}
	eAnimLoopCtl getLoopCtl() const {
		return m_loopCtl;
	}
};
