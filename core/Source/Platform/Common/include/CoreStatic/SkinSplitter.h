///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "afxext.h"

#pragma once

namespace KEP {

#define SKINSPLITTER_MAXBONES_PER_MESH 28
#define SKINSPLITTER_MAXNUMBONESPERVERTEX 4
#define SKINSPLITTER_MAXNUMBONES 256
#define SKINSPLITTER_MAXNUMMATERIALS 256
#define SKINSPLITTER_MAXDISTANCE 100.f
#define SKINSPLITTER_MINDISTANCE .001f
#define SKINSPLITTER_USEFALSECOLORS FALSE
#define SKINSPLITTER_BATCHBYMATERIAL TRUE
#define SKINSPLITTER_MAXTEXTURECOORDS 8
#define SKINSPLITTER_MAXNUMSKINCOLORS 8

struct SkinSplitterMatrixWeights {
	float w[4];
};

struct SkinBoneData {
	int numBones;
	Matrix44f* invBoneToSkinMat;
};

struct SkinVertexMaps {
	int* matrixIndices;
	SkinSplitterMatrixWeights* matrixWeights;
};

struct SkinSplitterSkin {
	SkinBoneData boneData;
	SkinVertexMaps vertexMaps;
	void* unaligned;
};

struct SkinSplitterRGBA {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	unsigned char alpha;
};

struct SkinSplitterMorphTarget {
	Vector3f* verts;
	Vector3f* normals;
};

struct SkinSplitterTriangle {
	USHORT vertIndex[3];
	USHORT matIndex;
};

struct SkinSplitterMaterialList {
	CMaterialObject** materials;
	int numMaterials;
	int space;
};

struct SkinSplitterTexCoords {
	float u;
	float v;
};

struct SkinSplitterGeometry {
	int numTriangles;
	int numVertices;
	int numTexCoordSets;

	SkinSplitterSkin* skin;

	SkinSplitterMaterialList matList;

	SkinSplitterTriangle* triangles;

	SkinSplitterTexCoords* texCoords[SKINSPLITTER_MAXTEXTURECOORDS];

	SkinSplitterMorphTarget* morphTarget;
};

/* skin data API */
SkinSplitterSkin*
SkinSplitterSkinCreate(const CEXMeshObj* mesh, const VertexAssignmentBlended* pVertexDataSource);

void SkinSplitterSkinDestroy(SkinSplitterSkin* skin);

int SkinSplitterSkinGetNumBones(SkinSplitterSkin* skin) {
	return skin->boneData.numBones;
}

SkinSplitterMatrixWeights*
SkinSplitterSkinGetVertexBoneWeights(SkinSplitterSkin* skin) {
	return skin->vertexMaps.matrixWeights;
}

int* SkinSplitterSkinGetVertexBoneIndices(SkinSplitterSkin* skin) {
	return skin->vertexMaps.matrixIndices;
}

Matrix44f*
SkinSplitterSkinGetSkinToBoneMatrices(SkinSplitterSkin* skin) {
	return skin->boneData.invBoneToSkinMat;
}

/* morph target (vertex data) API */
SkinSplitterMorphTarget*
SkinSplitterMorphTargetCreate(const CEXMeshObj* mesh, const VertexAssignmentBlended* pVertexDataSource);

void SkinSplitterMorphTargetDestroy(SkinSplitterMorphTarget* morphTarget);

/* skinned geometry data API */
SkinSplitterGeometry*
SkinSplitterGeometryCreate(const CEXMeshObj* mesh, const VertexAssignmentBlended* pVertexDataSource);

void SkinSplitterGeometryDestroy(SkinSplitterGeometry* geom);

} // namespace KEP