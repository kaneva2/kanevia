/******************************************************************************
 TimeStormPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_TIMESTORMPROPERTIESCTRL_H_)
#define _TIMESTORMPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimeStormPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CTimeStormPropertiesCtrl window

class CTimeStormPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CTimeStormPropertiesCtrl();
	virtual ~CTimeStormPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	BOOL GetUseTimeSystem() const {
		return m_useTimeSystem;
	}
	BOOL GetUseWindRandomizer() const {
		return m_useWindRandomizer;
	}
	float GetWindDirectionX();
	float GetWindDirectionZ();
	float GetMaxWindStrength();
	float GetGravity();
	float GetSeaLevel();
	float GetSeaDrag();

	// modifiers
	void SetUseTimeSystem(BOOL useTimeSystem) {
		m_useTimeSystem = useTimeSystem;
	}
	void SetUseWindRandomizer(BOOL useWindRandomizer) {
		m_useWindRandomizer = useWindRandomizer;
	}
	void SetWindDirectionX(float windDirectionX);
	void SetWindDirectionZ(float windDirectionZ);
	void SetMaxWindStrength(float maxWindStrength);
	void SetGravity(float gravity);
	void SetSeaLevel(float seaLevel);
	void SetSeaDrag(float seaDrag);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeStormPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CTimeStormPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_TIME_SYSTEM,
		INDEX_WIND_RANDOMIZER,
		INDEX_WIND_DIRECTION_X,
		INDEX_WIND_DIRECTION_Z,
		INDEX_MAX_WIND_STRENGTH,
		INDEX_GRAVITY,
		INDEX_SEA_LEVEL,
		INDEX_SEA_DRAG
	};

	const static int WIND_DIRECTION_X_MIN = 0;
	const static int WIND_DIRECTION_X_MAX = 1000;
	const static int WIND_DIRECTION_X_SCALE = 1000;
	const static int WIND_DIRECTION_Z_MIN = 0;
	const static int WIND_DIRECTION_Z_MAX = 1000;
	const static int WIND_DIRECTION_Z_SCALE = 1000;
	const static int GRAVITY_MIN = 1;
	const static int GRAVITY_MAX = 100;
	const static int GRAVITY_SCALE = -1000;

private:
	BOOL m_useTimeSystem;
	BOOL m_useWindRandomizer;
	int m_windDirectionX;
	int m_windDirectionZ;
	int m_maxWindStrength;
	int m_gravity;
	int m_seaLevel;
	int m_seaDrag;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_TIMESTORMPROPERTIESCTRL_H_)
