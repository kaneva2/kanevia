///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class CMovieRuntimeObj : public CObject, public GetSet {
protected:
	CMovieRuntimeObj() {}

	DECLARE_SERIAL(CMovieRuntimeObj);

public:
	CMovieRuntimeObj(BOOL L_playing,
		BOOL L_looping,
		CStringA L_fileName,
		IDirectDrawSurface7* L_pSurface4Loc,
		IDirectDrawStreamSample* L_pSampleLoc,
		IAMMultiMediaStream* L_pSStreamLoc,
		IAMMultiMediaStream* L_pAMStreamLoc,
		IMediaStream* L_pPrimaryVidStreamLoc,
		IDirectDrawMediaStream* L_pDDStreamLoc,
		RECT L_rectMov,
		RECT L_changingRect);

	RECT m_changingRect;
	BOOL m_playing;
	BOOL m_looping;
	CStringA m_fileName;
	IDirectDrawSurface7* m_pSurface4Loc;
	IDirectDrawStreamSample* m_pSampleLoc;
	IAMMultiMediaStream* m_pSStreamLoc;
	IAMMultiMediaStream* m_pAMStreamLoc;
	IMediaStream* m_pPrimaryVidStreamLoc;
	IDirectDrawMediaStream* m_pDDStreamLoc;
	RECT m_rectMov;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CMovieRuntimeObjList : public CObList {
public:
	CMovieRuntimeObjList(){};

	DECLARE_SERIAL(CMovieRuntimeObjList);
};

} // namespace KEP