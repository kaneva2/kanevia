#pragma once
#include "resource.h"

#include <vector>

#include "KepPropertyPage.h"
#include "StatLink.h"
class CGameData;

class TemplateAchievement {
public:
	TemplateAchievement(string name = "", string description = "", string points = "", string assetId = "") :
			m_name(name), m_description(description), m_points(points), m_assetId(assetId) {}

	string m_name, m_description, m_points, m_assetId;
};

class TemplatePremiumItem {
public:
	TemplatePremiumItem(string name = "", string description = "", string price = "", string assetId = "") :
			m_name(name), m_description(description), m_price(price), m_assetId(assetId) {}

	string m_name, m_description, m_price, m_assetId;
};

class TemplateLeaderBoard {
public:
	TemplateLeaderBoard(string name = "", string label = "", string type = "", string sort = "") :
			m_name(name), m_label(label), m_type(type), m_sort(sort) {}

	string m_name, m_label, m_type, m_sort;
};

class TemplateInfo {
public:
	TemplateInfo(const char* name = "", const char* desc = "", const char* webLink = "", const char* folder = "", bool childGame = false) :
			m_name(name), m_desc(desc), m_webLink(webLink), m_folder(folder), m_childGame(childGame), m_leaderBoard(NULL) {}

	string m_name,
		m_desc,
		m_webLink,
		m_folder;
	bool m_childGame; // GStar-type requiring a parent game
	vector<TemplateAchievement*> m_achievements;
	vector<TemplatePremiumItem*> m_premiumItems;
	TemplateLeaderBoard* m_leaderBoard;
};

// CNewGameTemplateSel dialog

class CNewGameTemplateSelDlg : public CKEPPropertyPage {
	DECLARE_DYNAMIC(CNewGameTemplateSelDlg)

public:
	CNewGameTemplateSelDlg(CGameData*& selectedGame, const char* editorBaseDir, Logger& logger);
	virtual ~CNewGameTemplateSelDlg();

	bool LoadTemplates(const char* templateDir);
	bool SelectTemplateByName(CStringA templateName);
	BOOL OnSetActive();
	LRESULT OnWizardNext();

	// Dialog Data
	enum { IDD = IDD_NEW_GAME_TEMPL_SEL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CGameData*& m_selectedGame; // game selected in prev wizard so we can tell if GStar or full STAR
	CComboBox m_comboTemplate;
	CStatic m_desc;
	CStaticLink m_infoLink;
	afx_msg void OnCbnSelchangeTemplateList();
	vector<TemplateInfo*> m_templates;
	TemplateInfo* m_selectedTemplate;

	Logger m_logger;

	CStringA m_editorBaseDir;
};
