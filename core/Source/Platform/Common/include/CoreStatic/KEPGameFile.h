///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "common/include/KEPCommon.h"
#include "IKEPGameFile.h"
#include <direct.h>
#include <SerializeHelper.h>

#ifndef KEPGAMEFILE_H
#define KEPGAMEFILE_H

namespace KEP {

class CKEPGameFile : public IKEPGameFile {
public:
	typedef std::vector<CKEPObList**> CKEPObListVector;
	typedef CKEPObListVector::iterator CKEPObListIterator;

public:
	CKEPGameFile(void);
	//CKEPGameFile(CKEPObList ** kepObList, ...);
	CKEPGameFile(CKEPObList** ptr);
	~CKEPGameFile(void);

	std::string GetFileName();

	BOOL IsDirty();

	BOOL SaveFile(const std::string& fileName = "");

	void SetObjects(CKEPObListVector* objectList) {
		m_objectsSetPtr = objectList;
	}
	CKEPObListVector* GetObjects() const {
		return m_objectsSetPtr;
	}

	void UpdateFileName();

private:
	CKEPObListVector* m_objectsSetPtr;
};

/**
 * A simple wrapper class that forwards GameFile calls to supplied
 * implementation.
 *
 * This exists primarily for the purpose supporting a GameFile implementation that
 * can be placed on the stack and deleted without attempting to destruct the
 * referenced implementation class.
 *
 * Specifically it is used to insure that when KEPGameSet is destroyed, and it's
 * constituent GameFile pointers are destructed, that the GIL's GameFile facet
 * is not deleted.
 */
class KEPGameFileReference : public IKEPGameFile {
public:
	KEPGameFileReference(IKEPGameFile& impl) :
			_impl(impl) {}
	virtual ~KEPGameFileReference() {}
	std::string GetFileName() {
		return _impl.GetFileName();
	}
	BOOL IsDirty() {
		return _impl.IsDirty();
	}
	BOOL SaveFile(const std::string& fileName = "") {
		return _impl.SaveFile(fileName);
	}

private:
	IKEPGameFile& _impl;
};
#endif

} // namespace KEP