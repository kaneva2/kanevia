/******************************************************************************
 SurfacePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SURFACEPROPERTIESCTRL_H_)
#define _SURFACEPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SurfacePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "TextureDatabase.h"

/////////////////////////////////////////////////////////////////////////////
// CSurfacePropertiesCtrl window

class CSurfacePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSurfacePropertiesCtrl();
	virtual ~CSurfacePropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	float GetParameter() const {
		return m_parameter;
	}
	int GetMips() const {
		return m_mips;
	}
	int GetBump() const {
		return m_bump;
	}
	int GetBumpType() const {
		return m_bumpType;
	}
	BOOL GetUseHighQualityMipMaps() const {
		return m_useHighQualityMipMaps;
	}

	// modifiers
	void SetParameter(float parameter) {
		m_parameter = parameter;
	}
	void SetMips(int mips) {
		m_mips = mips;
	}
	void SetBump(int bump) {
		m_bump = bump;
	}
	void SetBumpType(int bumpType) {
		m_bumpType = bumpType;
	}
	void SetUseHighQualityMipMaps(BOOL b) {
		m_useHighQualityMipMaps = b;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSurfacePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSurfacePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void LoadBumpTypes();

private:
	enum { INDEX_ROOT = 1,
		INDEX_MIPS,
		INDEX_BUMP,
		INDEX_BUMP_TYPE,
		INDEX_PARAMETER,
		INDEX_USE_HIGH_QUALITY_MIP_MAPS };

	const static int MIPS_MIN = 1;
	const static int MIPS_MAX = 20;
	const static int BUMP_MIN = 0;
	const static int BUMP_MAX = 10;

	float m_parameter;
	int m_mips;
	int m_bump;
	int m_bumpType;
	BOOL m_useHighQualityMipMaps;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_SURFACEPROPERTIESCTRL_H_)
