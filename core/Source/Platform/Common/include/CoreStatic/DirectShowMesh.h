#pragma once

#include "KEPCommon.h"

#if (DRF_DIRECT_SHOW_MESH == 1)

#include "ExternalMesh.h"
#include <list>
#include <string>
#include <KEPMacros.h>

#include "..\..\RenderEngine\ReMaterial.h"

class DirectShowTexture;
class WMTexture;
class jsThread;

namespace KEP {

class DownloadManager;
typedef std::list<std::string> WmvPlaylist;

class DirectShowMesh : public IExternalMesh {
protected:
	size_t m_meshId; // flash mesh id (mesh count since launch)

	bool m_destruct;

	DirectShowMesh(DownloadManager* dlMgr);

	virtual ~DirectShowMesh();

	bool playVideo(const std::string& url);

	bool loadPlaylist();

	DownloadManager* m_dlMgr;
	WMTexture* m_renderer;
	LPDIRECT3DDEVICE9 m_3dDev;
	WmvPlaylist* m_playlist;
	bool m_startedPlaying;
	jsThread* m_fileDownloader;
	ReMaterialPtr m_ReMat;

public:
	// to reset on same object
	bool DirectShowPlayerCreate(
		LPDIRECT3DDEVICE9 dev,
		const MediaParams& mediaParams,
		uint32_t width = 0, uint32_t height = 0,
		bool loop = false,
		int32_t x = 0, int32_t y = 0);

	bool DirectShowPlayerClose();

public: // IExternalMesh Interface
	std::string StrThis(bool verbose = false) const;

	bool SetMediaParams(const MediaParams& mediaParams, bool allowRewind = true);

	bool SetVolume(double volPct = -1);

	int UpdateTexture();

	void Delete();

public: // Static Functions
	static DirectShowMesh* Create(
		DirectShowMesh* fm,
		LPDIRECT3DDEVICE9 dev,
		const MediaParams& mediaParams,
		uint32_t width = 0, uint32_t height = 0,
		bool loop = false,
		int32_t x = 0, int32_t y = 0);

	static bool s_enabledGlobal;
	static bool SetEnabledGlobal(bool enable) {
		s_enabledGlobal = enable;
		return true;
	}
	static bool GetEnabledGlobal() {
		return s_enabledGlobal;
	}

	static double s_volPctGlobal;
	static bool SetVolumeGlobal(double volPct);
	static double GetVolumeGlobal() {
		return s_volPctGlobal;
	}
};

} // namespace KEP

#endif
