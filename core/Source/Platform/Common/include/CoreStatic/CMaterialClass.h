///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// added by serialize.pl
#include <GetSet.h>
#include "CStructuresMisl.h"
#include "../../../RenderEngine/ReMaterial.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

struct ReRenderStateData;
struct ReMaterialConstBlock;
class TextureProvider;
typedef std::shared_ptr<TextureProvider> TextureProviderPtr;
class MaterialCompressor;
class IMemSizeGadget;

#define NUM_CMATERIAL_TEXTURES 10 // DO NOT CHANGE THIS CONSTANT WILL BREAK FILE FORMAT - Jonny

#define MAX_UV_SETS 8U
#define CMATERIAL_UNCOMPRESSED -2

//define know constants
#define ARBP_UKNOWN (0)
#define ARBP_USEDEFAULT (1)
#define ARBP_WorldInverseTranspose (2)
#define ARBP_WorldViewProjection (3)
#define ARBP_World (4)
#define ARBP_ViewInverse (5)
#define ARBP_Time (6)
#define ARBP_WorldView (7)
#define ARBP_Projection (8)
#define ARBP_Texture (9)
#define ARBP_Material (12)
#define ARBP_DirLight (13)
#define ARBP_Diffuse (14)
#define ARBP_Specular (15)
#define ARBP_Emmisive (16)
#define ARBP_Ambient (17)
#define ARBP_SpecPower (18)
#define ARBP_WorldInverse (20)
#define ARBP_VolumeTexturePrcdrl (21)

struct SLARBparameterBlock {
	//determins what dataset is initialized IEFFECT CLONE TYPES
	short type;
	short dataSize;
	byte* data;

	//1=sv data 0=skip save data
	byte svData;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CMaterialObject : public CObject {
	DECLARE_SERIAL_SCHEMA(CMaterialObject, VERSIONABLE_SCHEMA | 8);

public:
	enum {
		ATTRIB_SORT_ORDER_OVERRIDE_DISABLE = -1,
		ATTRIB_DEFAULT_SORT_ORDER = ATTRIB_SORT_ORDER_OVERRIDE_DISABLE
	};

	// Material blend modes (m_blendMode)
	enum BlendMode {
		BLEND_DISABLED = -1, // No blending
		BLEND_MULTIPLY_ADD = 0, // Src * Dst + Dst
		BLEND_ALPHA = 1, // Src * SrcAlpha + Dst * (1-SrcAlpha) (assuming DstAlpha is 1)
		BLEND_ADDITION = 2, // Src + Dst
		BLEND_SQUARE_ADD = 3, // Src * Src + Dst * Dst
		BLEND_INV_MULTIPLY = 4, // (1 - Src) * Dst
		BLEND_INV_MULTIPLY_ALT = 5, // (1 - Src) * Dst (same as 4?)
		BLEND_MULTIPLY = 6, // Src * Dst (modulation)
		BLEND_ALPHA_ALT = 7, // Src * SrcAlpha + Dst * (1-SrcAlpha) (same as 1?)
	};

	// Texture blend op (m_texBlendMethod, map to D3D texture ops)
	enum TextureOp {
		TOP_DISABLE = -1, // D3DTOP_DISABLE
		TOP_MODULATE = 0, // D3DTOP_MODULATE
		TOP_ADD = 1, // D3DTOP_ADD
		TOP_SUBTRACT = 2, // D3DTOP_SUBTRACT
		TOP_ADDSMOOTH = 3, // D3DTOP_ADDSMOOTH
		TOP_ADDSIGNED = 4, // D3DTOP_ADDSIGNED
		TOP_MODULATE2X = 5, // D3DTOP_MODULATE2X
		TOP_TYPE6 = 6, // 6 and 7 seems to be special
		TOP_TYPE7 = 7, //
		TOP_SELECTARG1 = 8, // D3DTOP_SELECTARG1
		TOP_SELECTARG2 = 9, // D3DTOP_SELECTARG2
		TOP_MODULATE4X = 10, // D3DTOP_MODULATE4X
		TOP_ADDSIGNED2X = 11, // D3DTOP_ADDSIGNED2X
		TOP_PREMODULATE = 12, // D3DTOP_PREMODULATE
		TOP_MULTIPLYADD = 13, // D3DTOP_MULTIPLYADD
		TOP_BLENDCURRENTALPHA = 14, // D3DTOP_BLENDCURRENTALPHA
	};

	CMaterialObject();

	~CMaterialObject();

	CMaterialObject(D3DMATERIAL9 materialInit);

	D3DMATERIAL9 m_useMaterial; //main material used to render
	D3DMATERIAL9 m_baseMaterial; //original colors set before any mod functions
	TimeMs m_cycleTime;
	int m_textureTileType;
	BOOL m_switch;
	TimeMs m_timeStamp;
	BOOL m_cullOn;
	BOOL m_forceWireFrame;
	int m_blendMode; //0=modulated transparency 2=amplified transparency
	int m_optionalShaderIndex;
	CStringA m_materialName; // Added by Jonny to allow naming of materials.
	BOOL m_RGBOverride;

	BOOL m_enableTextureMovement;
	BOOL m_enableSet[MAX_UV_SETS];
	TXTUV m_textureMovement[MAX_UV_SETS];
	D3DMATERIAL9 m_targetModMaterial; //for transition

	BOOL m_mirrorEnabled;
	int m_baseBlendMode;

	BOOL m_fogFilter;
	int m_zBias;
	int m_sortOrder;
	BOOL m_fogOverride;
	char m_shadowObject;

	int m_diffuseEffect;
	char m_colorArg1[NUM_CMATERIAL_TEXTURES];
	char m_colorArg2[NUM_CMATERIAL_TEXTURES];
	short m_pixelShader;
	short m_vertexShader;
	BOOL m_doAlphaDepthTest;

	SLARBparameterBlock* m_paramBlock;
	int m_paramCount;
	CStringA m_fxBindString;

	// DRF - Material Texture Attributes (should be an array of struct Texture {})
	bool m_texFileNameIsConcrete; // false for UGC dynamic object
	std::string m_texFileName[NUM_CMATERIAL_TEXTURES];
	std::string m_texFileNameOriginal[NUM_CMATERIAL_TEXTURES];
	DWORD m_texNameHash[NUM_CMATERIAL_TEXTURES];
	DWORD m_texNameHashOriginal[NUM_CMATERIAL_TEXTURES]; // possibly a dup to m_texNameHashStored but the latter is used irregularly, this one will save the original m_texNameHash as deserialized
	DWORD m_texNameHashStored[NUM_CMATERIAL_TEXTURES];
	D3DCOLOR m_texColorKey[NUM_CMATERIAL_TEXTURES];
	int m_texBlendMethod[NUM_CMATERIAL_TEXTURES];

	bool m_forceFullRes; // force the client to use the full texture resolution for this material

	BOOL m_normalizeNormals; // NORMALIZENORMAL flag, set at runtime, no serialization at this time (YC Apr 2010)

	// Custom Texture Configurations (serialized - do not change numbers)
	enum {
		NO_CUSTOM_TEXTURE = 0, // material has no custom textures
		CUSTOM_FIRST_TEXTURE_SLOT = 1, // material has one custom texture (1st slot)
		CUSTOM_ALL_TEXTURE_SLOTS = 2 // material has up to 10 custom textures (all slots)
	};

	void SetReMaterial(ReMaterialPtr mat);
	ReMaterialPtr GetReMaterial() { return m_ReMat; }

	void GetReRenderState(ReRenderStateData* state, const std::string& subDir = "");

	void ComputeTextureNameHashes(bool saveAsOriginal);

	// Callback not necessary for dynamic objects since they don't do fade or transition.
	// If dynamic objects start needing this feature, we should create a more efficient mechanism
	// for it that only calls Callback() for those objects that need it.
	void Callback();

	void SetCurDistFromCam(float curDistFromCam) { m_curDistFromCam = curDistFromCam; }
	float GetCurDistFromCam() const { return m_curDistFromCam; }
	void SetFadeMaxDistance(long fadeMaxDist) { m_fadeMaxDist = fadeMaxDist; }
	long GetFadeMaxDistance() const { return m_fadeMaxDist; }
	void SetFadeVisDistance(long fadeVisDist) { m_fadeVisDist = fadeVisDist; }
	long GetFadeVisDistance() const { return m_fadeVisDist; }
	void SetFadeByDistance(BOOL fadeByDistance) { m_fadeByDistance = fadeByDistance; }
	BOOL GetFadeByDistance() const { return m_fadeByDistance; }
	void SetTransition(int transition) { m_transition = transition; }
	int GetTransition() const { return m_transition; }

	void SetTransitionBlend(int transitionBlend) { m_transitionBlend = transitionBlend; }

	void SetFadeOutAlpha(TimeMs duration);
	void SetFadeInAlpha(TimeMs duration);
	void SetFadeOut(TimeMs duration);
	void SetFadeIn(TimeMs duration, BOOL reset);
	void CopyMaterial(D3DMATERIAL9 source, D3DMATERIAL9* dest) const;
	void ApplyHighlight(bool highlight, D3DCOLOR highlightColor);
	void Clone(CMaterialObject** clone) const;
	float InterpolateFloats(float StartVec, float EndVec, int NumOfStages, int ThisStage);
	void Reset();
	void SetCallbackMethod(int method);
	int GetParameterCount(LPD3DXEFFECT effect);
	void DeleteParamMemoryAndZeroIt();
	BOOL InitParamBlockFXShadersHSL(LPD3DXEFFECT effect);
	void Serialize(CArchive& ar);

	MaterialCompressor* m_myCompressor;
	int m_myCompressionIndex;
	int m_compressionColorTableIndexUse;
	int m_compressionColorTableIndexMod;
	int m_compressionFlagsIndex;
	int m_compressionTextureAssignments[NUM_CMATERIAL_TEXTURES];

	void SetCustomTexture(const TextureProviderPtr& provider, int option) {
		if (provider) {
			m_customTextureProvider = provider;
			m_customTextureOption = option;
		} else {
			ResetCustomTexture();
		}
	}
	void ResetCustomTexture() {
		m_customTextureProvider.reset();
		m_customTextureOption = NO_CUSTOM_TEXTURE;
	}
	const TextureProviderPtr& GetCustomTextureProvider() const { return m_customTextureProvider; }
	int GetCustomTextureOption() const { return m_customTextureOption; }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	void init();
	void ApplyDistanceFade();
	void ApplyColorTransition();

	void GetMemSizeTexFileNames(IMemSizeGadget* pMemSizeGadget) const;

private:
	float m_curDistFromCam;
	long m_fadeMaxDist;
	long m_fadeVisDist;
	BOOL m_fadeByDistance;

	int m_transition; //0=none
	int m_transitionBlend;

	ReMaterialPtr m_ReMat;
	TextureProviderPtr m_customTextureProvider;
	int m_customTextureOption;
	bool m_bLastHighlighted = false;
};

__forceinline void CMaterialObject::Callback() {
	if (m_fadeByDistance)
		ApplyDistanceFade();

	if (m_transition != 0)
		ApplyColorTransition();
}

} // namespace KEP
