/******************************************************************************
 ContextTree.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
// CContextTree

#include <vector>

class CContextHandler {
public:
	virtual void ContextClick(HTREEITEM htItem, DWORD screenX, DWORD screenY) = 0;

	// helper to show the menus
	static void DoMenu(int screenX, int screenY, UINT menuId, CWnd* parent, int subMenu = -1, vector<UINT>* disabledItems = 0);
};

class CContextTree : public CTreeCtrl {
	DECLARE_DYNAMIC(CContextTree)

public:
	CContextTree();
	void SetParent(CContextHandler* parent) {
		m_parent = parent;
	}
	virtual ~CContextTree();

protected:
	CContextHandler* m_parent;

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnRClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint ptMousePos);
};
