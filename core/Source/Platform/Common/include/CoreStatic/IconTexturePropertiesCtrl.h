/******************************************************************************
 IconTexturePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ICONTEXTUREPROPERTIESCTRL_H_)
#define _ICONTEXTUREPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IconTexturePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CIconTexturePropertiesCtrl window

class CIconTexturePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CIconTexturePropertiesCtrl();
	virtual ~CIconTexturePropertiesCtrl();
	enum { PROP_INDEX_ROOT = 1,
		PROP_INDEX_DENSITY };

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	float GetDensity() const {
		return m_density;
	}

	// modifiers
	void SetDensity(float density) {
		m_density = density;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIconTexturePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CIconTexturePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;
	float m_density;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ICONTEXTUREPROPERTIESCTRL_H_)
