/******************************************************************************
 LevelRewardsCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_LEVELREWARDSCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
#define AFX_LEVELREWARDSCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LevelRewardsCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CLevelRewardsCtrl window

class CLevelRewardsCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CLevelRewardsCtrl();
	virtual ~CLevelRewardsCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetRewardId() const {
		return m_rewardId;
	}
	int GetSkillId() const {
		return m_skillId;
	}
	int GetLevelId() const {
		return m_levelId;
	}
	int GetRewardGLID() const {
		return m_rewardGLID;
	}
	int GetRewardQty() const {
		return m_rewardQty;
	}
	int GetRewardPointType() const {
		return m_rewardPointType;
	}
	int GetRewardAmount() const {
		return m_rewardAmount;
	}
	int GetAltRewardGLID() const {
		return m_altRewardGLID;
	}
	int GetAltRewardQty() const {
		return m_altRewardQty;
	}
	int GetAltRewardAmount() const {
		return m_altRewardAmount;
	}

	// modifiers
	void SetRewardId(int rewardId) {
		m_rewardId = rewardId;
	}
	void SetSkillId(int skillId) {
		m_skillId = skillId;
	}
	void SetLevelId(int levelId) {
		m_levelId = levelId;
	}
	void SetRewardGLID(int rewardGLID) {
		m_rewardGLID = rewardGLID;
	}
	void SetRewardQty(int rewardQty) {
		m_rewardQty = rewardQty;
	}
	void SetRewardPointType(int rewardPointType) {
		m_rewardPointType = rewardPointType;
	}
	void SetRewardAmount(int rewardAmount) {
		m_rewardAmount = rewardAmount;
	}
	void SetAltRewardGLID(int rewardGLID) {
		m_altRewardGLID = rewardGLID;
	}
	void SetAltRewardQty(int rewardQty) {
		m_altRewardQty = rewardQty;
	}
	void SetAltRewardAmount(int rewardAmount) {
		m_altRewardAmount = rewardAmount;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLevelRewardsCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CLevelRewardsCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;
	CFont m_font;

	int m_rewardId;
	int m_skillId;
	int m_levelId;
	int m_rewardGLID;
	int m_rewardQty;
	int m_rewardPointType;
	int m_rewardAmount;
	int m_altRewardGLID;
	int m_altRewardQty;
	int m_altRewardAmount;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEVELREWARDSCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
