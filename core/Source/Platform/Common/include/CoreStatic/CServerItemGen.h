///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPCommon.h"

#if (DRF_OLD_VENDORS == 1)

#include "Core/Util/HighPrecisionTime.h"
#include <GetSet.h>
#include "KEPObList.h"

namespace KEP {
class CServerItemObj;

class CServerItemGenObj : public CObject, public GetSet {
	DECLARE_SERIAL(CServerItemGenObj);

public:
	CServerItemGenObj();

	CServerItemObj* m_pSIO;
	BOOL m_existsOnServer;

	int m_spawnGenID;
	CStringA m_genName;
	TimeMs m_lastCallbackUpdate;

	void Serialize(CArchive& ar);
	void SafeDelete();
	void Clone(CServerItemGenObj** clone);

	DECLARE_GETSET
};

class CServerItemGenObjList : public CKEPObList {
public:
	CServerItemGenObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CServerItemGenObjList);
};

} // namespace KEP

#else
void CServerItemGen_4221();
#endif
