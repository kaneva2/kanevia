/******************************************************************************
 ViewportsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_VIEWPORTS_PROPERTIESCTRL_H_)
#define _VIEWPORTS_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ViewportsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CViewportsPropertiesCtrl window

class CViewportsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CViewportsPropertiesCtrl();
	virtual ~CViewportsPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetName() const {
		return Utf16ToUtf8(m_name).c_str();
	}
	int GetPercentTop() const {
		return m_percentTop;
	}
	int GetPercentBottom() const {
		return m_percentBottom;
	}
	int GetPercentLeft() const {
		return m_percentLeft;
	}
	int GetPercentRight() const {
		return m_percentRight;
	}

	// modifiers
	void SetName(CStringA name) {
		m_name = Utf8ToUtf16(name).c_str();
	}
	void SetPercentTop(int percentTop) {
		m_percentTop = percentTop;
	}
	void SetPercentBottom(int percentBottom) {
		m_percentBottom = percentBottom;
	}
	void SetPercentLeft(int percentLeft) {
		m_percentLeft = percentLeft;
	}
	void SetPercentRight(int percentRight) {
		m_percentRight = percentRight;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CViewportsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CViewportsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	enum { INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_PERCENT_TOP,
		INDEX_PERCENT_LEFT,
		INDEX_PERCENT_BOTTOM,
		INDEX_PERCENT_RIGHT
	};

	CStringW m_name;
	int m_percentTop;
	int m_percentBottom;
	int m_percentLeft;
	int m_percentRight;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_VIEWPORTS_PROPERTIESCTRL_H_)
