/******************************************************************************
CMaterialEffect.h

Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>

#include "d3dx9math.h"
#include "d3dx9effect.h"
#include "Core/Math/Vector.h"
#include "Core/Math/Matrix.h"

#define EFFECT_MAP std::map<std::string, MaterialEffect*>

namespace KEP {

struct RawBoneData;
class CMaterialObject;
class EffectRenderData;

class MaterialEffect {
public:
	MaterialEffect();
	virtual ~MaterialEffect();

	virtual bool Create(const char* filename, LPDIRECT3DDEVICE9 pDevice);

	static MaterialEffect* GetMaterialEffect(std::string effect, LPDIRECT3DDEVICE9 pDevice);

	virtual int Begin();
	virtual void BeginPass(unsigned int pass);
	virtual void EndPass();
	virtual void End();
	virtual void CommitChanges();
	void SetTextures(EffectRenderData* pRenderData);
	bool Prepare(RawBoneData* pBoneData, EffectRenderData* pData);

	bool SetMatrix(const char* parameter, const Matrix44f& m);
	bool SetVector(const char* parameter, const Vector4f& v);
	bool SetVector(const char* parameter, const Vector3f& v);
	bool SetFloat(const char* parameter, float f);
	bool SetInt(const char* parameter, unsigned int dw);
	bool SetTexture(const char* parameter, LPDIRECT3DTEXTURE9 tex);
	bool SetColor(const char* parameter, const D3DCOLOR& color);

	void OnLostDevice();
	void OnResetDevice();

	std::string mEffectName;

	//	The cache of stored effects
	static EFFECT_MAP gEffectMap;

	RawBoneData* mPrevBoneData;

	LPDIRECT3DDEVICE9 mpDevice;

	ID3DXEffect* m_effect;
	D3DXHANDLE m_technique;
};

} // namespace KEP

