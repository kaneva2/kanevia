/******************************************************************************
 AnimatedTexturesDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ANIMATED_TEXTURES_DLG_H_)
#define _ANIMATED_TEXTURES_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AnimatedTexturesDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"

////////////////////////////////////////////
// CAnimatedTexturesDialog control

class CAnimatedTexturesDialog : public CDialogBar {
	DECLARE_DYNAMIC(CAnimatedTexturesDialog)

public:
	CAnimatedTexturesDialog();
	virtual ~CAnimatedTexturesDialog();

	// Dialog Data
	//{{AFX_DATA(CAnimatedTexturesDialog)
	enum { IDD = IDD_DIALOG_ANIMTEXTURES };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CAnimatedTexturesDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colAnimatedTexture;
	CButton m_grpAnimatedTexture;

	CKEPImageButton m_btnAddAnimatedTextures;
	CKEPImageButton m_btnDeleteAnimatedTextures;
};

#endif !defined(_ANIMATED_TEXTURES_DLG_H_)