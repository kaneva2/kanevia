///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

#include "ISkillDB.h"
#include "KEPObList.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class IMemSizeGadget;
class CGlobalInventoryObjList;
class CInventoryObjList;
class CStatObjList;
class CTitleObjectList;

class CRacialAdvantageObject : public CObject {
	DECLARE_SERIAL(CRacialAdvantageObject);

public:
	CRacialAdvantageObject();

	int m_edbRef;
	float m_adjustment;

	void Clone(CRacialAdvantageObject** clone);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CRacialAdvantageList : public CObList {
public:
	CRacialAdvantageList() {}
	~CRacialAdvantageList();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CRacialAdvantageList);

	float GetAdjustmentByEdbIndex(int edbIndex);
	void Clone(CRacialAdvantageList** clone);
};

class CSkillAbilityObject : public CObject, public GetSet {
	DECLARE_SERIAL(CSkillAbilityObject);

public:
	CSkillAbilityObject();
	~CSkillAbilityObject();

	CStringA m_abilityName;
	int m_menuID; //less than zero means no menu
	long m_minimumLevelToUse;
	float m_masterLevel;
	int m_functionType; //0=none 1=create item 2=shapeshift 3=hiding
	int m_miscInt; //can be a glid, shapeshift db index, or duration of hiding
	int m_miscInt2;
	int m_miscInt3;
	int m_effectBind;
	int m_miscAmount;
	CInventoryObjList* m_requiredItems; //if null nothing is required
	CInventoryObjList* m_resultItems; //if successful this is what you will recieve
	CStatObjList* m_requiredStatistics; //minimum statistics to attempt

	void Clone(CSkillAbilityObject** clone);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CSkillAbilityList : public CObList {
public:
	CSkillAbilityList() {}
	~CSkillAbilityList();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CSkillAbilityList);

	CSkillAbilityObject* GetObjByIndex(int index);
	void Clone(CSkillAbilityList** clone);
};

class CLevelRewardObject : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CLevelRewardObject, VERSIONABLE_SCHEMA | 1);

public:
	CLevelRewardObject();

	int m_rewardId;
	int m_skillId;
	int m_levelId;
	int m_rewardGLID;
	int m_rewardQty;
	int m_rewardPointType;
	int m_rewardAmount;
	int m_altRewardGLID;
	int m_altRewardQty;
	int m_altRewardAmount;

	void Clone(CLevelRewardObject** clone);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CLevelRewardObjectList : public CKEPObList {
public:
	CLevelRewardObjectList() {}
	~CLevelRewardObjectList();

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CLevelRewardObjectList);

	CLevelRewardObject* GetObjByIndex(int index);
	void Clone(CLevelRewardObjectList** clone);
	int GetFreeID();
	void SerializeAlloc(CArchive& ar, int loadCount);
};

class CLevelRangeObject : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CLevelRangeObject, VERSIONABLE_SCHEMA | 3);

public:
	CLevelRangeObject();
	~CLevelRangeObject();

	int m_levelRangeId;
	long m_rangeStart;
	long m_rangeEnd;
	CStringA m_levelTitle;
	CSkillAbilityList* m_abilityDB;
	int m_menuID; //-1 = no menu
	int m_profficiencyBonus; //
	int m_perfectiveBonus; //
	int m_titleId;
	int m_altTitleId;
	CStringA m_gainMsg;
	CStringA m_altGainMsg;
	CStringA m_levelMsg;
	int m_levelNumber;

	CLevelRewardObjectList* m_levelRewardDB;

	void Clone(CLevelRangeObject** clone);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

//database
class CLevelRangeList : public CObList {
public:
	CLevelRangeList() {}
	~CLevelRangeList();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CLevelRangeList);

	void Clone(CLevelRangeList** clone);
	BOOL GetExperienceValueByLevel(int level, long& experiencePoints);
	CLevelRangeObject* GetLevelByExp(long exp);
	CLevelRangeObject* GetObjByIndex(int index);
	int GetFreeID();
};

class CSkillActionObject : public CObject, public GetSet {
	DECLARE_SERIAL(CSkillActionObject);

public:
	CSkillActionObject();

	int m_actionID;
	int m_skillID;
	float m_gainAmount;

	void Clone(CSkillActionObject** clone);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CSkillActionObjectList : public CKEPObList {
public:
	CSkillActionObjectList() {}
	~CSkillActionObjectList();

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CSkillActionObjectList);

	CSkillActionObject* GetObjByIndex(int index);
	CSkillActionObject* GetActionById(int actionId);
	void Clone(CSkillActionObjectList** clone);
	int GetFreeID();
	void SerializeAlloc(CArchive& ar, int loadCount);
};

const int SKILLRET_SCRIPT_HANDLED = -2;
const int SKILLRET_INVALID = -1;
const int SKILLRET_FAILED = 0;
const int SKILLRET_SUCCESS = 1;
const int SKILLRET_INVALID_LEVEL = 2;
const int SKILLRET_LACK_INVENTORY = 8;
const int SKILLRET_UNSUCCESSFUL = 10;
const int SKILLRET_STAT_REQ_NOT_MET = 14;
const int SKILLRET_NO_LEVELS = 19;
const int SKILLRET_LACK_SKILL = 21;
const int SKILLRET_ITEM_NOT_PRESENT = 38;
const int SKILLRET_LACK_THE_COMPONENT = 39;
const int SKILLRET_TOO_MUCH_INVENTORY = 40;
const int SKILLRET_TOO_SOON = 300;
const int SKILLRET_TOO_SOON_SKILL = 301;

// special instructions on use skill
const int SKILL_NO_SPECIAL = -1;
const int SKILL_SHAPE_SHIFT = 2;
const int SKILL_HIDE = 3;
const int SKILL_MOUNT = 4;
const int SKILL_REVEAL = 5;
const int SKILL_HEAL_RADIUS = 6;
const int SKILL_EMIT_PROJECTILE = 7;
const int SKILL_HEAL_SELF = 8;

class CSkillObject : public CObject, public GetSet {
	DECLARE_SERIAL(CSkillObject);

public:
	CSkillObject();
	~CSkillObject();

	CStringA m_skillName;
	float m_currentExperience; //depends
	int m_currentLevel; //depends
	int m_skillType; //individual number like an id
	float m_gainIncrement; //basis for gain (how fast a skill can be built)
	BOOL m_basicAbilitySkill; //means that it only has one function like hiding
	float m_basicAbilityExpMastered; //component of ability basic skill option
	int m_basicAbilityFunction; //component of ability basic-function resulting from successful throw
	float m_skillGainsWhenFailed; //0.0 is no gain when failed if not 0.0 then it will gain till that exp point then not gain from trying anymore
	float m_maxMultiplier; //
	BOOL m_minimalSave;
	TimeMs m_lastUseTime;
	BOOL m_useInternalTimer;
	ULONG m_currentSkillDelay;
	BOOL m_filterEvalLevel;
	int m_skillId;

	CLevelRangeList* m_levelRangeDB; //defines what level you are and yout title in that skill
	CRacialAdvantageList* m_raceAdvantageDB; //defines who will gain faster is specific skills
	CSkillActionObjectList* m_skillActionDB;

	void Clone(CSkillObject** clone);
	BOOL EvaluateLevelAndTitle(CStringA& title, int& level, CSkillObject* skillRef);
	int UseSkill(long expOfTarget,
		int levelToUse,
		int ability,
		int curDBCfg,
		CInventoryObjList* currentInv,
		int* specialFunction,
		CStatObjList* currentStats,
		BOOL* updateStatsToClient,
		CLevelRangeObject** levelRaisedPtr,
		int* returnMiscInt,
		float totalCurrentExp,
		int maxItemsInInventory,
		CSkillObject* dbObjectRef,
		int* effectBind,
		ULONG curGlobalSkillDelay,
		ULONG curPlayerSkillTimeStamp);
	int UseAction(int actionId, CSkillObject* skillRef, bool useAlternate, int& newLevel, std::string& levelMessage, std::string& gainMessage);
	int GetPercentageToNextLevel(CSkillObject* skillPtrRef);
	BOOL EvaluateLevel(int& level, CSkillObject* skillRef);
	CLevelRewardObjectList* GetCurrentLevelRewards(CSkillObject* skillRef);
	CStringA GetRewardsAtNextLevel(CSkillObject* skillRef, CTitleObjectList* titles, CGlobalInventoryObjList* items, bool useAlternate);
	void CloneMinimum(CSkillObject** clone);
	void Serialize(CArchive& ar);
	void SerializeAlloc(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CSkillObjectList : public CKEPObList, public ISkillDB {
	DECLARE_SERIAL_SCHEMA(CSkillObjectList, VERSIONABLE_SCHEMA | 1);

public:
	CSkillObjectList();
	~CSkillObjectList();

	float m_totalExperience;
	float UpdateTotalExperienceSum();
	float m_speedGainCap;

	CTitleObjectList* m_titles;

	void Clone(CSkillObjectList** clone) const;

	CSkillObject* GetBestSkill(CSkillObjectList* skillDB);
	CSkillObject* GetBestSkillWEval(CSkillObjectList* skillDB);
	long GetHighestSkillExp();
	int UseSkillByType(int type,
		CSkillObjectList* dbList,
		long expOfTarget,
		int levelToUse,
		int ability,
		int curDBCfg,
		CInventoryObjList* currentInv,
		int* specialFunction,
		CSkillObject** newAddedSkill,
		CStatObjList* currentStats,
		BOOL* updateStatsToClient,
		CLevelRangeObject** levelRaisedPtr,
		CSkillObject** skillPtr,
		int* returnMiscInt,
		int maxItemsInInventory,
		int* effectBind,
		ULONG curGlobalSkillDelay,
		ULONG curPlayerSkillTimeStamp);
	CSkillObject* GetSkillByType(int type);
	CSkillObject* GetSkillByIndex(int index);
	CSkillObject* GetSkillByName(const char* skillName);
	int GetSkillIndexByType(int type);
	CStringA GetTitleByLevelAndType(int type, int level);
	BOOL EvaluateRequiredSkillLevel(int type, int requiredLevel, CSkillObjectList* skillDBRef);
	int GetEfficiencyByTypeAndLevel(int type, CSkillObjectList* charSkillList, int* proficiency, int* perfective);
	int GetFreeID();
	void SetOptimizedSave(BOOL optimized);
	void CloneMinimum(CSkillObjectList** clone) const;
	void Serialize(CArchive& ar);
	void SerializeAlloc(CArchive& ar, int loadCount);

	// do we have it?  if not add it
	bool CheckSkill(int type, CSkillObjectList* dbList, CSkillObject** skPtr, CSkillObject** dbSkillPtr, bool& added);

	//2 ISkillDB overrides
	IGetSet* SkillByType(int type) {
		return GetSkillByType(type);
	}
	IGetSet* SkillByName(const char* skillName) {
		return GetSkillByName(skillName);
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

} // namespace KEP