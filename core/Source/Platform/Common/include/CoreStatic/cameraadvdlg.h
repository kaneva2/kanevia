///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAMERAADVDLG_H__C81A8961_FE65_11D2_BDB1_908ED5928F44__INCLUDED_)
#define AFX_CAMERAADVDLG_H__C81A8961_FE65_11D2_BDB1_908ED5928F44__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CameraAdvDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCameraAdvDlg dialog

class CCameraAdvDlg : public CDialog {
	// Construction
public:
	CCameraAdvDlg(CWnd* pParent = NULL); // standard constructor
	int cameraTypeInt;

	BOOL m_filterFirstPersonModeRef;

	// Dialog Data
	//{{AFX_DATA(CCameraAdvDlg)
	enum { IDD = IDD_DIALOG_CAMERADVANCEDSETTINGS };

	float m_floatingMovePwr;
	float m_fov;
	float m_clipPlane;

	float m_InitialPosX;
	float m_InitialPosY;
	float m_InitialPosZ;
	float m_floatTargetPosX;
	float m_floatTargetPosY;
	float m_floatTargetPosZ;
	BOOL m_useCollision;

	float m_baseHeightAspect;
	float m_pitchRotation;

	float m_orbitFocusX, m_orbitFocusY, m_orbitFocusZ;
	float m_orbitDefAzi, m_orbitDefEle;
	float m_orbitMinDist, m_orbitMaxDist, m_orbitStartDist, m_orbitDistStep;
	BOOL m_orbitAutoFollow;

	CStringA m_script_type;
	CStringA m_camera_name;

	int m_camera_mode;

	//}}AFX_DATA
	int m_miscVal1;
	CComboBox m_camera_type;
	CEdit m_edit_script_type;
	CComboBox m_camera_modes;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCameraAdvDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	void OnCbnSelchangeComboCameratype();

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCameraAdvDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitCameraModes();
	void InitCameraTypes();

public:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAMERAADVDLG_H__C81A8961_FE65_11D2_BDB1_908ED5928F44__INCLUDED_)
