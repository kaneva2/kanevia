///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// CMaterialShaderLibDlg dialog
#include "CGlobalMaterialShaderLib.h"
#include "CEXMeshClass.h"
#include "TextureDatabase.h"
#include "afxwin.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "MaterialShadersPropertiesCtrl.h"

class CMaterialShaderLibDlg : public CKEPEditDialog {
	DECLARE_DYNAMIC(CMaterialShaderLibDlg)

public:
	CMaterialShaderLibDlg(CGlobalMaterialShaderList*& globalShader,
		CWnd* pParent = NULL); // standard constructor
	virtual ~CMaterialShaderLibDlg();

	TextureDatabase* GL_textureDataBase;
	CGlobalMaterialShaderList*& m_globalShaderRef;

	// Dialog Data
	enum { IDD = IDD_DIALOG_MATERIALSHADERLIB };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	void InListShaders(int sel);
	virtual BOOL OnInitDialog();

protected:
	virtual void OnOK();

public:
	CListBox m_shaderList;
	afx_msg void OnBnClickedButtonAddnewshader();
	afx_msg void OnBnClickedButtonReplaceshader();
	afx_msg void OnBnClickedButtonDeleteshader();
	afx_msg void OnBnClickedCheckEnablematerialovverride();
	afx_msg void OnBnClickedCheckEnableenvmapping();
	afx_msg void OnBnClickedButtonAmbientaccess();
	afx_msg void OnLbnSelchangeListshaderlist();
	afx_msg void OnBnClickedButtonDiffuseaccess();
	afx_msg void OnBnClickedButtonEmmisiveaccess();
	afx_msg void OnBnClickedButtonSpecularaccess();
	afx_msg void OnBnClickedButtonLoadshader();
	afx_msg void OnBnClickedButtonSave();

private:
	CCollapsibleGroup m_colShaders;
	CButton m_grpShaders;
	CMaterialShadersPropertiesCtrl m_propShaders;

	CKEPImageButton m_btnAddShader;
	CKEPImageButton m_btnDeleteShader;
	CKEPImageButton m_btnReplaceShader;
};
