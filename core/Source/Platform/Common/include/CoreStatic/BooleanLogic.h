///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <vector>

namespace KEP {
namespace BooleanLogic {

// Disable foreward calculated logic which is difficult to use to detect state changes because the same input can
// be used in multiple branches, potentially causing conflicting changes to the result.
#if 0
// Forward calculated logic components.
// Modifying an input propagates the change to the outputs.
// Calls a function when input changes.
// If the same input occurs multiple times in a logic component's inputs, this function may be called multiple times.
// This can be fixed by making two passes - one to calculate new values (which may modify the new value multiple times)
// and another to commit the new value.
namespace Fwd {

class LogicComponent {
  public:
	explicit LogicComponent(bool bInitialState = false) : m_bCurrentState(bInitialState) {
	}

	bool GetCurrentState() const {
		return m_bCurrentState;
	}

	void ConnectOutput(LogicComponent* pLogicComponent) {
		m_apOutputs.emplace_back(std::move(pLogicComponent));
	}

	virtual void InputChanged(LogicComponent* pInput, bool bIsTrue, void* pStateChangeData) = 0;
	virtual void RecalculateState() = 0;
  protected:
	// Derived classes should calculate current state then call this function.
	void SetState(bool bNewState, void* pStateChangeData) {
		if ( bNewState == m_bCurrentState )
			return;
		m_bCurrentState = bNewState;
		PropagateToOutputs(m_bCurrentState, pStateChangeData);
	}

	// Reculculating state does not propagate to outputs.
	void SetRecalculatedState(bool bNewState) {
		m_bCurrentState = bNewState;
	}

  private:
	virtual void PropagateToOutputs(bool bCurrentState, void* pStateChangeData) {
		for ( auto& pLogicComponent : m_apOutputs )
			pLogicComponent->InputChanged(this, bCurrentState, pStateChangeData);
	}

  private:
	bool m_bCurrentState;
	std::vector<LogicComponent*> m_apOutputs;
};

class LogicVariable : public LogicComponent {
  public:
	explicit LogicVariable(bool bInitialState = false) : LogicComponent(bInitialState) {
	}

	virtual void InputChanged(LogicComponent* pInput, bool bIsTrue, void* pStateChangeData) {} // No inputs.
	virtual void RecalculateState() {} // No recalculation needed.
	void SetState(bool bNewState, void* pStateChangeData) {
		LogicComponent::SetState(bNewState, pStateChangeData);
	}
};

// Shared implementation of And, Or, and Xor LogicComponents
class CountOfInputs : public LogicComponent {
	virtual void RecalculateState() {
		size_t nCountOfTrueValues = 0;
		for ( auto itr = m_Inputs.begin(); itr != m_Inputs.end(); ++itr ) {
			itr->first->RecalculateState();
			if ( itr->first->GetCurrentState() )
				nCountOfTrueValues += itr->second.m_DuplicationCount;
		}
		m_nCountOfTrueValues = nCountOfTrueValues;
	}

	virtual void InputChanged(LogicComponent* pInput, bool bIsTrue, void* pStateChangeData) {
		UpdateCountOfTrue(pInput, bIsTrue);
		SetState(CalculateState(), pStateChangeData);
	}
  protected:
	template<typename Iterator>
	CountOfInputs(const Iterator& itr_begin, const Iterator& itr_end) {
		m_nCountOfInputs = 0;
		m_nCountOfTrueValues = 0;
		for ( Iterator itr = itr_begin; itr != itr_end; ++itr ) {
			if ( !*itr )
				continue;

			++m_nCountOfInputs;
			const std::shared_ptr<LogicComponent>& pInput = *itr;
			auto itr_where = m_Inputs.find(pInput.get());
			if ( itr_where == m_Inputs.end() )
				itr_where = m_Inputs.insert(itr_where, std::make_pair(pInput.get(), MapData {pInput, uint16_t(1), pInput->GetCurrentState()}));
			else
				++itr_where->second.m_DuplicationCount;
			m_nCountOfTrueValues += itr_where->second.m_bValue;
		}
	}

	virtual bool CalculateState() = 0;

	void UpdateCountOfTrue(LogicComponent* pInput, bool bIsTrue) {
		auto itr = m_Inputs.find(pInput);
		if ( itr == m_Inputs.end() ) {
			ASSERT(false);
			return;
		}
		if ( itr->second.m_bValue == bIsTrue )
			return; // No change.

		// This input changed state.
		auto nCopiesOfInput = itr->second.m_DuplicationCount;
		if ( bIsTrue )
			m_nCountOfTrueValues += nCopiesOfInput;
		else
			m_nCountOfTrueValues -= nCopiesOfInput;
	}
	size_t GetCountOfTrueValues() const {
		return m_nCountOfTrueValues;
	}
	size_t GetNumberOfInputs() const {
		return m_nCountOfInputs;
	}

  private:
	size_t m_nCountOfTrueValues;
	size_t m_nCountOfInputs;

	struct MapData {
		std::shared_ptr<LogicComponent> m_pLogicComponent;
		uint16_t m_DuplicationCount;
		bool m_bValue;
	};
	std::map<LogicComponent*, MapData> m_Inputs;
};

class Or : public CountOfInputs {
  public:
	template<typename Iterator>
	Or(const Iterator& itr_begin, const Iterator& itr_end)
		: CountOfInputs(itr_begin, itr_end) {
	}

	virtual bool CalculateState() {
		return GetCountOfTrueValues() > 0;
	}
};

class And : public CountOfInputs {
  public:
	template<typename Iterator>
	And(const Iterator& itr_begin, const Iterator& itr_end)
		: CountOfInputs(itr_begin, itr_end) {
	}

	virtual bool CalculateState() {
		return GetCountOfTrueValues() == GetNumberOfInputs();
	}
};

class Xor : public CountOfInputs {
  public:
	template<typename Iterator>
	Xor(const Iterator& itr_begin, const Iterator& itr_end)
		: CountOfInputs(itr_begin, itr_end) {
	}

	virtual bool CalculateState() {
		return (GetCountOfTrueValues() & 0x1) != 0;
	}
};

class Not : public LogicComponent {
  public:
	Not(std::shared_ptr<LogicComponent> pInput = nullptr)
		: m_pInput(std::move(pInput)) {
	}

	virtual void InputChanged(LogicComponent* pInput, bool bIsTrue, void* pStateChangeData) {
		if ( pInput != m_pInput.get() ) {
			ASSERT(false);
			return;
		}
		SetState(!bIsTrue, pStateChangeData);
	}

	virtual void RecalculateState() {
		if ( m_pInput ) {
			m_pInput->RecalculateState();
			SetRecalculatedState(!m_pInput->GetCurrentState());
		}
	}

  private:
	std::shared_ptr<LogicComponent> m_pInput;
};

class Identity : public LogicComponent {
  public:
	Identity(std::shared_ptr<LogicComponent> pInput = nullptr)
		: m_pInput(std::move(pInput)) {
	}

	virtual void InputChanged(LogicComponent* pInput, bool bIsTrue, void* pStateChangeData) {
		if ( pInput != m_pInput.get() ) {
			ASSERT(false);
			return;
		}
		SetState(bIsTrue, pStateChangeData);
	}

	virtual void RecalculateState() {
		if ( m_pInput ) {
			m_pInput->RecalculateState();
			SetRecalculatedState(m_pInput->GetCurrentState());
		}
	}
  private:
	std::shared_ptr<LogicComponent> m_pInput;
};

// Calls a function when a its input logic result changes.
class LogicResultChangedCallback : public Identity {
  public:
	typedef std::function<void(LogicResultChangedCallback*, void*)> CallbackFunctionType;
	explicit LogicResultChangedCallback(std::shared_ptr<LogicComponent> pInput,  CallbackFunctionType callbackFunction = nullptr)
		: m_CallbackFunction(callbackFunction)
		, m_pInput(std::move(pInput)) {
	}
	void SetCallbackFunction(CallbackFunctionType callbackFunction) {
		m_CallbackFunction = callbackFunction;
	}
	virtual void InputChanged(LogicComponent* pInput, bool bIsTrue, void* pStateChangeData) {
		Identity::InputChanged(pInput, bIsTrue, pStateChangeData);
		if ( m_CallbackFunction )
			m_CallbackFunction(this, pStateChangeData);
	}
  private:
	std::shared_ptr<LogicComponent> m_pInput;
	CallbackFunctionType m_CallbackFunction;
};
} // namespace Fwd
#endif

// Modifying an input requires recalculation of logic from the top down (i.e. starting from each output variable)
class LogicComponent {
public:
	explicit LogicComponent(bool bInitialState = false) :
			m_bLastCalculatedState(bInitialState) {
	}

	bool GetLastCalculatedState() const {
		return m_bLastCalculatedState;
	}

	bool CalculateState() {
		m_bLastCalculatedState = DoCalculateState();
		return m_bLastCalculatedState;
	}

protected:
	virtual bool DoCalculateState() = 0;

private:
	bool m_bLastCalculatedState;
};

class Variable : public LogicComponent {
public:
	explicit Variable(bool bInitialState = false) :
			LogicComponent(bInitialState), m_bAssignedState(bInitialState) {
	}

	void SetState(bool bNewState) {
		m_bAssignedState = bNewState;
	}

protected:
	virtual bool DoCalculateState() {
		return m_bAssignedState;
	}

public:
	bool m_bAssignedState;
};

// Shared implementation of And, Or, and Xor LogicComponents
class CountOfInputs : public LogicComponent {
public:
	template <typename Iterator>
	void AddInputs(const Iterator& itr_begin, const Iterator& itr_end) {
		for (Iterator itr = itr_begin; itr != itr_end; ++itr) {
			if (!*itr)
				continue;

			++m_nCountOfInputs;
			const std::shared_ptr<LogicComponent>& pInput = *itr;
			auto itr_where = m_Inputs.find(pInput.get());
			if (itr_where == m_Inputs.end())
				itr_where = m_Inputs.insert(itr_where, std::make_pair(pInput.get(), MapData{ pInput, uint16_t(1), pInput->GetLastCalculatedState() }));
			else
				++itr_where->second.m_DuplicationCount;
			m_nCountOfTrueValues += itr_where->second.m_bValue;
		}
	}

protected:
	virtual bool DoCalculateState() {
		size_t nCountOfTrueValues = 0;
		for (auto itr = m_Inputs.begin(); itr != m_Inputs.end(); ++itr) {
			if (itr->first->CalculateState())
				nCountOfTrueValues += itr->second.m_DuplicationCount;
		}
		m_nCountOfTrueValues = nCountOfTrueValues;
		return CalculateStateFromTrueCount();
	}

protected:
	template <typename Iterator>
	CountOfInputs(const Iterator& itr_begin, const Iterator& itr_end) {
		m_nCountOfInputs = 0;
		m_nCountOfTrueValues = 0;
		AddInputs(itr_begin, itr_end);
	}

	virtual bool CalculateStateFromTrueCount() = 0;

	size_t GetCountOfTrueValues() const {
		return m_nCountOfTrueValues;
	}
	size_t GetNumberOfInputs() const {
		return m_nCountOfInputs;
	}

private:
	size_t m_nCountOfTrueValues;
	size_t m_nCountOfInputs;

	struct MapData {
		std::shared_ptr<LogicComponent> m_pLogicComponent;
		uint16_t m_DuplicationCount;
		bool m_bValue;
	};
	std::map<LogicComponent*, MapData> m_Inputs;
};

class Or : public CountOfInputs {
public:
	template <typename Iterator>
	Or(const Iterator& itr_begin, const Iterator& itr_end) :
			CountOfInputs(itr_begin, itr_end) {
	}

private:
	virtual bool CalculateStateFromTrueCount() {
		return GetCountOfTrueValues() > 0;
	}
};

class And : public CountOfInputs {
public:
	template <typename Iterator>
	And(const Iterator& itr_begin, const Iterator& itr_end) :
			CountOfInputs(itr_begin, itr_end) {
	}

private:
	virtual bool CalculateStateFromTrueCount() {
		return GetCountOfTrueValues() == GetNumberOfInputs();
	}
};

class Xor : public CountOfInputs {
public:
	template <typename Iterator>
	Xor(const Iterator& itr_begin, const Iterator& itr_end) :
			CountOfInputs(itr_begin, itr_end) {
	}

private:
	virtual bool CalculateStateFromTrueCount() {
		return (GetCountOfTrueValues() & 0x1) != 0;
	}
};

class Not : public LogicComponent {
public:
	Not(std::shared_ptr<LogicComponent> pInput = nullptr) :
			m_pInput(std::move(pInput)) {
	}

protected:
	virtual bool DoCalculateState() {
		if (m_pInput) {
			return m_pInput->CalculateState();
		} else {
			return true;
		}
	}

private:
	std::shared_ptr<LogicComponent> m_pInput;
};

class Identity : public LogicComponent {
public:
	Identity(std::shared_ptr<LogicComponent> pInput = nullptr) :
			m_pInput(std::move(pInput)) {
	}

protected:
	virtual bool DoCalculateState() {
		if (m_pInput) {
			m_pInput->CalculateState();
			return !m_pInput->CalculateState();
		} else {
			return false;
		}
	}

private:
	std::shared_ptr<LogicComponent> m_pInput;
};

} // namespace BooleanLogic
} // namespace KEP
