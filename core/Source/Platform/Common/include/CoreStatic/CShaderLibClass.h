///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "KEPObList.h"
#include "Core/Util/CArchiveObjectSchema.h"
#include <d3dx9.h>

namespace KEP {

class CShaderObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CShaderObj, VERSIONABLE_SCHEMA | 2);

public:
	CShaderObj();
	CStringA m_shaderName;
	CStringA m_shaderCode;
	LPDIRECT3DVERTEXSHADER9 m_shader;
	LPDIRECT3DPIXELSHADER9 m_pShader;
	LPD3DXEFFECT m_pEffect;
	D3DXHANDLE m_shaderValidHandle;
	short m_effectParamCount;
	int m_shaderType;
	int m_compiler; //0=assemly 1=fx compiler
	int m_resetRenderState;
	byte m_shaderUsedOnce;

	BOOL Compile(LPDIRECT3DDEVICE9 pd3dDevice);
	void SafeDelete();
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CShaderObjList : public CKEPObList {
public:
	CShaderObjList(){};

	DECLARE_SERIAL(CShaderObjList);

	void SafeDelete();
	void Compile(LPDIRECT3DDEVICE9 pd3dDevice);
	//void AddShader(CStringA shaderName, CStringA code, int shaderType, int resetRender);
	CStringA ConvToString(int number);
	BOOL DeleteByIndex(int index);
	CShaderObj* GetByIndex(int index);
	CShaderObj* GetByName(CStringA shaderName, int& index);
	int LoadShaderByStringData(CStringA dataStr, LPDIRECT3DDEVICE9 pd3dDevice, CStringA& fxNameRet);
	BOOL SetShaderByIndex(LPDIRECT3DDEVICE9 pd3dDevice,
		int index,
		int mode,
		LPD3DXEFFECT* effectRet,
		int& correctParamCount,
		CStringA* retCode,
		CShaderObj** retPtr);

	void OnLostDevice();
	void OnResetDevice();
};

} // namespace KEP