///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "InstanceId.h"
#include "Event/Base/IArena.h"
#include "KEPNetwork.h"
#include "KEPObList.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

#define BT_LAST_MAN_STANDING 0
#define BT_TEAM_ARENA 1
#define BT_RACE_ARENA 2

// return codes from ReadyToStart
typedef enum {
	ArenaReady,
	ArenaWaiting,
	ArenaTimedOut
} EArenaReady;

class BattleSignup {
public:
	USHORT playerHandle;
	USHORT playerClan;
	int playerRace;
	BattleSignup() :
			playerClan(0), playerHandle(0), playerRace(0) {}
	BattleSignup(const BattleSignup& src) {
		operator=(src);
	}

	BattleSignup& operator=(const BattleSignup& src) {
		if (this != &src) {
			playerClan = src.playerClan;
			playerHandle = src.playerHandle;
			playerRace = src.playerRace;
		}
		return *this;
	}
};

// arena
class CBattleSchedulerObj : public CObject, public IArena, public GetSet {
	DECLARE_SERIAL_SCHEMA(CBattleSchedulerObj, VERSIONABLE_SCHEMA | 3); // version 2 added m_maxSignupTimeMs, m_minSignupTimeMs, 3 added script name, max score, team limits

public:
	CBattleSchedulerObj();
	~CBattleSchedulerObj() {
		SafeDelete();
	}

	//general vars
	CStringA m_name;
	int m_type; //0=last man standing, see defines above
	int m_battleMinimum; //will cancel match is not enough play (notify players)
	int m_battleMaximum;
	TimeMs m_battleDuration;
	//restriction
	int m_levelRangeMax;
	int m_levelRangeMin;
	//signupList
	int m_currentSignupCount;
	BattleSignup* m_signups;
	//portal vars
	ZoneIndex m_zoneIndex; // file to load
	ChannelId m_channel; // channel to run on
	int m_spawnPointCount;
	Vector3f* m_spawnPositions;
	ULONG m_maxSignupTimeMs; // ms that signup is open before timeout
	ULONG m_minSignupTimeMs; // ms that signup must be open
	CStringA m_scriptName;
	ULONG m_maxScore; // max score to end arena, 0 = n/a
	ULONG m_minTeams; // for teamed arena, min/max teams
	ULONG m_maxTeams;

	void AddSpawnPoint(Vector3f position);
	void ReplaceSpawnPoint(int index, Vector3f position);
	void DeleteSpawnPoint(int index);
	Vector3f GetSpawnPointData(int index);
	Vector3f GetNextSpawnPoint();
	BOOL AddSignup(NETID playerHandle, std::string& msg);
	EArenaReady ReadyToStart();
	void RemoveSignup(NETID playerHandle);
	void ResetBattle();
	void Serialize(CArchive& ar);
	void SafeDelete();

	// runtime only data, not serialized
	TimeMs m_battleStartTime;
	BOOL m_battleIsActive;
	int m_lastSelectedPoint; // for round-robin spawn pt
	TimeMs m_signupStartTime; // when did we start waiting for signups
	bool m_haveEnoughTeams; // enough teams or races to start?

	// IArena overrides
	virtual int GetSpawnPointCount() {
		return m_spawnPointCount;
	}
	virtual Vector3f GetSpawnPoint(int index) {
		return GetSpawnPointData(index);
	}

	DECLARE_GETSET
	IGetSet* Values() {
		return this;
	}
};

class CBattleSchedulerList : public CKEPObList {
public:
	CBattleSchedulerList();

	DECLARE_SERIAL(CBattleSchedulerList);

	void SafeDelete();
	static void FreeData();
	CBattleSchedulerObj* GetByIndex(int index);

	ChannelId GetNextInstanceId(const ChannelId& channel);

	BOOL AddSignup(CBattleSchedulerObj* arenaObj, NETID playerHandle, std::string& msg);
	void RemoveSignup(NETID playerHandle);

	// get the battle obj that the player is signedup for, or null if not signed up
	CBattleSchedulerObj* GetSignupForPlayer(NETID playerNetId);

private:
	long m_nextInstanceId;
};

} // namespace KEP