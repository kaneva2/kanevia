/******************************************************************************
 StorePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_STOREPROPERTIESCTRL_H_)
#define _STOREPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StorePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "CEXMeshClass.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CStorePropertiesCtrl window

class CStorePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CStorePropertiesCtrl();
	virtual ~CStorePropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetStoreName() const {
		return Utf16ToUtf8(m_storeName).c_str();
	}
	int GetStoreId() const {
		return m_storeId;
	}
	int GetWorldID() const {
		return m_worldID;
	}
	int GetMenuBind() const {
		return m_menuBind;
	}
	BOOL GetDealWithMurderers() const {
		return m_dealWithMurderers;
	}

	// modifiers
	void SetStoreName(CStringA storeName) {
		m_storeName = Utf8ToUtf16(storeName).c_str();
	}
	void SetStoreId(int storeId) {
		m_storeId = storeId;
	}
	void SetWorldID(int worldID) {
		m_worldID = worldID;
	}
	void SetMenuBind(int menuBind) {
		m_menuBind = menuBind;
	}
	void SetDealWithMurderers(BOOL dealWithMurderers) {
		m_dealWithMurderers = dealWithMurderers;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStorePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CStorePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitChannels();
	void InitMenus();

private:
	enum { INDEX_ROOT = 1,
		INDEX_STORE_NAME,
		INDEX_STORE_ID,
		INDEX_WORLD_ID,
		INDEX_MENU_BIND,
		INDEX_DEAL_MURDERERS
	};

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	CStringW m_storeName;
	int m_storeId;
	int m_worldID;
	int m_menuBind;
	BOOL m_dealWithMurderers;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_STOREPROPERTIESCTRL_H_)
