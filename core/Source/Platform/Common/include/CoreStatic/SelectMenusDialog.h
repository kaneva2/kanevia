/******************************************************************************
 SelectMenusDialog.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "resource.h"
#include <vector>

// CSelectMenusDialog dialog

class CSelectMenusDialog : public CDialog {
	DECLARE_DYNAMIC(CSelectMenusDialog)

public:
	CSelectMenusDialog(CWnd* pParent = NULL); // standard constructor
	virtual ~CSelectMenusDialog();

	// Dialog Data
	enum { IDD = IDD_SELECT_MENUS };

	typedef std::vector<std::string> MenusVector;
	typedef MenusVector::iterator MenusIter;

	// accessors
	void GetSelectedMenus(MenusVector& included_menus) {
		included_menus = m_included_menus;
	}

	void AddExcludedMenu(std::string exclude_filename) {
		m_exclude_menus.push_back(exclude_filename);
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	virtual void OnOK();
	virtual BOOL OnInitDialog();

	void InitMenus();

private:
	CListBox m_menus;

	MenusVector m_included_menus; // result after selection
	MenusVector m_exclude_menus; // menus to exclude from appearing in list
};
