#pragma once

#include "CPacketQueClass.h"
#include "Common\include\CoreStatic\CStructuresMisl.h"
#include "LogHelper.h"

namespace KEP {

class CPacketObjListMsgEquip : public CPacketObjList {
public:
	Timer m_timerPacket;

	virtual bool CanSend() const override {
#if _KGP_SERVER
		return m_canSend && (m_timerPacket.ElapsedMs() > 100.0);
#else
		return m_canSend;
#endif
	}

	virtual void AddPacket(size_t dataSize, const BYTE* pData, NETID netId) override {
		auto pMsg = (EQUIP_DATA*)pData;
		if (!pMsg || !dataSize)
			return;

		// Reset Packet Timer
		m_timerPacket.Reset();

		// Merge Packets ?
		for (POSITION pkPos = GetHeadPosition(); pkPos != NULL;) {
			auto pPacketObj = (CPacketObj*)GetNext(pkPos);
			if (!pPacketObj)
				continue;

			// Merge Packets Matching Message NetId
			BYTE* pDataOld = pPacketObj->m_pData;
			auto pMsgOld = (EQUIP_DATA*)pDataOld;
			if (!pMsgOld || (pMsgOld->netTraceId != pMsg->netTraceId))
				continue;

			// Determine Merged Message Size
			size_t glids = pMsg->glids;
			size_t glidsOld = pMsgOld->glids;
			size_t glidsNew = (glidsOld + glids);
			if (glidsNew > 255) {
				static LogInstance("Instance");
				LogError("Too Many Glids - glids=" << glidsNew);
				return;
			}

			// Create New Larger Message Copied From Old
			DWORD sizeOld = sizeof(EQUIP_DATA) + (glidsOld * sizeof(GLID));
			DWORD sizeNew = sizeof(EQUIP_DATA) + (glidsNew * sizeof(GLID));
			BYTE* pDataNew = new BYTE[sizeNew];
			CopyMemory(pDataNew, pDataOld, sizeOld);
			glidsNew = glidsOld;

			// Add New Special Glids (disarm all purges all previous glids)
			auto pMsgNew = (EQUIP_DATA*)pDataNew;
			GLID* pGlids = (GLID*)(pData + sizeof(EQUIP_DATA));
			GLID* pGlidsNew = (GLID*)(pDataNew + sizeof(EQUIP_DATA));
			for (size_t i = 0; i < glids; ++i) {
				GLID glidSpecial = pGlids[i];
				if (glidSpecial == 0) // disarm all
					glidsNew = 0;
				pGlidsNew[glidsNew++] = pGlids[i];
			}
			pMsgNew->glids = (unsigned char)glidsNew;
			sizeNew = sizeof(EQUIP_DATA) + (glidsNew * sizeof(GLID));

			// Replace Old Message With New
			delete[] pPacketObj->m_pData;
			pPacketObj->m_pData = pDataNew;
			pPacketObj->m_dataSize = sizeNew;
			m_sizeData += ((glidsNew - glidsOld) * sizeof(GLID));
			return;
		}

		// Nothing Merged - Add New Packet
		__super::AddPacket(dataSize, pData, netId);
	}
};

} // namespace KEP