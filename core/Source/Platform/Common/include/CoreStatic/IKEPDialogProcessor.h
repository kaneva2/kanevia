/******************************************************************************
 IKEPDialogProcessor.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// class for using dialog in the KEP editor and handling init and on ok
// ClientEngine will implement this
class IKEPDialogProcessor {
public:
	virtual CWnd* Preprocess(CWnd* parent, UINT dlgId, UINT dlgType = 0) = 0;
	virtual BOOL PostProcess(CWnd* dlg, UINT dlgId) = 0;
};