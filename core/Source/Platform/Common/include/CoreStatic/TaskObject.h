///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class TaskObject {
public:
	TaskObject() :
			m_refs(0) {}

	virtual ~TaskObject() {}

	size_t IncRefCount() {
		return ++m_refs;
	}

	size_t DecRefCount() {
		return m_refs ? --m_refs : 0;
	}

	virtual void ExecuteTask() = 0;

private:
	size_t m_refs;
};

} // namespace KEP