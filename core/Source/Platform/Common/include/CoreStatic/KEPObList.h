///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "afxcoll.h"
#include "Core/Util/Mutex.h"
#include "Core/Util/Unicode.h"

/// Mutex Macros
#define OBJ_LIST_GUARD(ol) MutexGuardLock lock(ol->m_listMutex)
#define OBJ_LIST_GUARD_INTERNAL // OBJ_LIST_GUARD(this)

namespace KEP {
class IMemSizeGadget;

class CKEPObList : public CObject {
	DECLARE_SERIAL(CKEPObList);

public:
	CKEPObList() :
			m_dirty(false) {}

	~CKEPObList() {}

	/**
	*The path of the file this object was lasted serialized to or deserialized from
	**/
	CStringA GetLastSavedPath() const {
		return m_lastSavedPath.c_str();
	}
	void SetLastSavedPath(const CStringA& path) {
		m_lastSavedPath = (const char*)path;
	}

	/**
	*Whether the object's state has changed and requires saving
	**/
	bool IsDirty() const {
		return m_dirty;
	}
	void SetDirty(bool dirty = true) {
		m_dirty = dirty;
	}

	void Serialize(CArchive& ar) {
		OBJ_LIST_GUARD_INTERNAL;
		m_list.Serialize(ar);
		SetLastSavedPath(Utf16ToUtf8(ar.GetFile()->GetFilePath().GetString()).c_str());
		SetDirty(false);
	}

	CObject* RemoveHead() {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		return m_list.RemoveHead();
	}

	CObject* RemoveTail() {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		return m_list.RemoveTail();
	}

	POSITION AddHead(CObject* newElement) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		return m_list.AddHead(newElement);
	}

	POSITION AddTail(CObject* newElement) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		return m_list.AddTail(newElement);
	}

	void AddHead(CObList* pNewList) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		m_list.AddHead(pNewList);
	}

	void AddTail(CObList* pNewList) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		m_list.AddTail(pNewList);
	}

	void RemoveAll() {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		m_list.RemoveAll();
	}

	void SetAt(POSITION pos, CObject* newElement) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		m_list.SetAt(pos, newElement);
	}

	void RemoveAt(POSITION position) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		m_list.RemoveAt(position);
	}

	POSITION InsertBefore(POSITION position, CObject* newElement) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		return m_list.InsertBefore(position, newElement);
	}

	POSITION InsertAfter(POSITION position, CObject* newElement) {
		OBJ_LIST_GUARD_INTERNAL;
		SetDirty(true);
		return m_list.InsertAfter(position, newElement);
	}

	///////////////////////////////////////////////////////////////////////////
	// DRF - Added
	///////////////////////////////////////////////////////////////////////////

	BOOL IsEmpty() const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.IsEmpty();
	}

	INT_PTR GetCount() const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetCount();
	}

	INT_PTR GetSize() const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetSize();
	}

	CObject*& GetHead() {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetHead();
	}

	const CObject* GetHead() const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetHead();
	}

	POSITION GetHeadPosition() const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetHeadPosition();
	}

	POSITION GetTailPosition() const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetTailPosition();
	}

	CObject*& GetNext(POSITION& rPosition) {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetNext(rPosition);
	}

	const CObject* GetNext(POSITION& rPosition) const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetNext(rPosition);
	}

	CObject*& GetPrev(POSITION& rPosition) {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetPrev(rPosition);
	}

	const CObject* GetPrev(POSITION& rPosition) const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetPrev(rPosition);
	}

	POSITION Find(CObject* searchValue, POSITION startAfter = NULL) const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.Find(searchValue, startAfter);
	}

	POSITION FindIndex(INT_PTR nIndex) const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.FindIndex(nIndex);
	}

	CObject*& GetAt(POSITION position) {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetAt(position);
	}

	const CObject* GetAt(POSITION position) const {
		OBJ_LIST_GUARD_INTERNAL;
		return m_list.GetAt(position);
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	std::string m_lastSavedPath;
	bool m_dirty;

	CObList m_list;

	void GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const;

public:
	mutable Mutex m_listMutex;
};

} // namespace KEP