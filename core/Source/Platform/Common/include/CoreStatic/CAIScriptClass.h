///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPObList.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class CAIScriptEvent : public CObject, public GetSet {
	DECLARE_SERIAL(CAIScriptEvent)

public:
	CAIScriptEvent();

	int m_event; //0=goTo location on tree
	//1=delay
	//2=play anim by version
	//3=goTo Random location on tree
	//4=loop to begginig of script
	int m_miscInt;
	void Serialize(CArchive& ar);
	void Clone(CAIScriptEvent** clone);

	DECLARE_GETSET
};

class CAIScriptEventList : public CObList {
public:
	CAIScriptEventList(){};

	DECLARE_SERIAL(CAIScriptEventList)

	void SafeDelete();
	CAIScriptEvent* GetEvent(int index);
	void AddEvent(int event, int miscInt);
	//functions
	void Clone(CAIScriptEventList** clone);
};

class CAIScriptObj : public CObject {
	DECLARE_SERIAL(CAIScriptObj)

public:
	CAIScriptObj();

	CStringA scriptName;
	CStringA scriptDescription;

	CAIScriptEventList* m_events;
	int m_currentEvent;
	TimeMs m_delayDuration;
	TimeMs m_delayStamp;

	CAIScriptEvent* GetEvent(int index);
	void Serialize(CArchive& ar);
	void SafeDelete();
	void Clone(CAIScriptObj** clone);
};

class CAIScriptObjList : public CKEPObList {
public:
	CAIScriptObjList(){};

	DECLARE_SERIAL(CAIScriptObjList)

	CAIScriptObj* GetScriptObject(int index);
	void SafeDelete();
};

} // namespace KEP