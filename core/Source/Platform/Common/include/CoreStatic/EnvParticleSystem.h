///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "engine\clientblades\iclientparticleblade.h"
#include "IResource.h"
#include "ResourceManagers\ResourceManager.h"
#include "ResourceType.h"
#include "ParticleDefinition.h"
#include "GetSet.h"
#include "Core/Util/fast_mutex.h"

#include "engine\ClientBlades\clientbladefactory.h"

#define ERR_PARTICLE_NOMEMORY (-1)
#define PARTICLE_OK (0)

namespace KEP {

class ParticleRM;

class EnvParticleSystem : public IResource, public GetSet {
public:
	// Settings for TypedResourceManager
	const static ResourceType _ResType = ResourceType::PARTICLE;

public:
	std::string m_systemName;
	std::string storedFileName;

	EnvParticleSystem(ResourceManager* library, UINT64 resKey, const std::string& name);

	EnvParticleSystem(const EnvParticleSystem& ps);

	virtual ~EnvParticleSystem();

	virtual std::string ToStr() const override {
		std::string str;
		StrBuild(str, "EPS<" << this << ">");
		return str;
	}

	virtual bool CreateResource() override;

	virtual BYTE* GetSerializedData(FileSize& dataLen);

	// GetSet Override
	virtual void SetNumber(ULONG index, double number);

	// Serialization
	void Write(CArchive& ar);
	void Read(CArchive& ar);

	const EffectDef* GetDefinition() const {
		return m_def;
	}

public:
	static std::string mResourceStore;

protected:
	EffectDef* m_def;

	static unsigned short ms_magicHeader;

	bool IsSerializedDataCompressible() const {
		return false; // too small to compress
	}

	DECLARE_GETSET
};

struct RuntimeParticleEffect {
	GLID m_glid;
	std::string m_name;
	bool m_systemOn;
	bool m_regenerate;
	bool m_on;
	Matrix44f m_passedInMatrix;
	EffectHandle m_blade_effect_handle;

	RuntimeParticleEffect(GLID glid, const std::string& name);
};

class ParticleRM : public TypedResourceManager<EnvParticleSystem> {
public:
	ParticleRM(const std::string& id) :
			TypedResourceManager<EnvParticleSystem>(id),
			m_blade_provider("Builtin"),
			m_currRuntimeEffectId(0) {
	}

	~ParticleRM();

	// the blade provider can change if different databases are loaded, so this shouldn't be serialized
	std::string GetBladeProvider() const {
		return m_blade_provider;
	}
	void SetBladeProvider(const std::string& provider) {
		m_blade_provider = provider;
	}

	IClientParticleBlade* GetParticleBlade() {
		return ClientBladeFactory::Instance()->GetClientParticleBlade();
	}

	void AddStockItem(const std::string& name, GLID glid);
	UINT GetNumberOfStockItems() const {
		return m_stockItems.size();
	}
	EnvParticleSystem* GetStockItemByIndex(UINT index);
	EnvParticleSystem* GetByName(const std::string& name);

	EnvParticleSystem* GetByGLID(const GLID& glid, bool bCreateIfNotExist);

	int AddRuntimeEffect(const std::string& name);
	bool RemoveRuntimeEffect(UINT runtimeId, bool killExistingParticles = false);
	void ClearRuntimeEffects();
	void FreeTextures();
	void GetAllRuntimeEffectsIds(std::vector<UINT>& idList) const;
	UINT GetNumberOfRuntimeEffects() const {
		return m_runtimeEffects.size();
	}
	bool ActivateRuntimeEffect(UINT runtimeId, const Vector3f& pos, const Quaternionf& ori);
	bool DeactivateRuntimeEffect(UINT runtimeId);
	bool MoveRuntimeEffect(UINT runtimeId, const Vector3f& pos, const Quaternionf& ori);

	bool SetRuntimeEffectStatus(UINT runtimeId, int bSysOn, int bOn, int bRegen);
	bool GetRuntimeEffectStatus(UINT runtimeId, int* bSysOn, int* bOn, int* bRegen);

	bool SetRuntimeEffectMatrix(UINT runtimeId, const Matrix44f& matrix);
	bool GetRuntimeEffectMatrix(UINT runtimeId, Matrix44f& matrix) const;
	EffectHandle GetRuntimeEffectHandle(UINT runtimeId) const;
	bool GetRuntimeEffect(UINT runtimeId, GLID& glid, std::string& effectName);

	bool UpdateEffectDefinition(const char* effectName, EffectDef* pDef);

protected:
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) override;
	virtual std::string GetResourceFileName(UINT64 hash) override;
	virtual std::string GetResourceFilePath(UINT64 hash) override;
	virtual std::string GetResourceURL(UINT64 hash) override {
		return ""; // UGC Asset URL will be determined before downloading
	}

	virtual bool IsUGCItem(UINT64 hash) const override {
		return IS_UGC_GLID((GLID)hash);
	}

	UINT AllocateNextRuntimeEffectId() {
		return ++m_currRuntimeEffectId;
	}

protected:
	std::string m_blade_provider;

	std::vector<UINT64> m_stockItems;
	std::map<UINT, RuntimeParticleEffect> m_runtimeEffects;
	UINT m_currRuntimeEffectId;
	fast_recursive_mutex m_particleBladeSync;

	friend class EnvParticleSystem;
};

} // namespace KEP