///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "common\include\chattype.h"
#include "Event/Base/EventHeader.h"

namespace KEP {

class CStateManagementObj;
class CSymbolMapperObjList;
class CTextRenderObj;
class Dispatcher;

class CStringObj : public CObject, public GetSet {
	DECLARE_SERIAL(CStringObj)

public:
	CStringObj();
	CStringA m_text;
	CTextRenderObj* m_pTRO;
	float m_red;
	float m_green;
	float m_blue;

	void SafeDelete();
	void Clone(CStringObj** copy);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CStringObjList : public CObList {
public:
	CStringObjList(){};
	void Clone(CStringObjList** copy);
	void SafeDelete();

	DECLARE_SERIAL(CStringObjList)
};

class CtextReadoutObj : public CObject {
	DECLARE_SERIAL(CtextReadoutObj)

public:
	CtextReadoutObj();

	Dispatcher* m_dispatcher;
	EVENT_ID m_attribEventID;

	int m_vertSpacing;
	int m_historyCount;
	int m_maxLineLength;
	int m_verticleLineVistCount;
	int m_timeOption;
	int m_locationX;
	int m_locationY;
	float m_cgLocationX;
	float m_cgLocationY;
	float m_cgVertSpacing;
	int m_cgBlendMode;
	float m_cgFontSizeX;
	float m_cgFontSizeY;
	int m_currentOffset;
	TimeMs m_scrollStamp;
	float m_maxMeasuredLength;

	CStringObjList* m_textLines;

	void SetDispatcher(Dispatcher* d);

	void AddLine(const std::string& text, eChatType chatType);

	void RenderText(HDC hdc);
	void RenderTextCG(CStateManagementObj* statemanager, LPDIRECT3DDEVICE9 g_pd3dDevice);
	CStringObj* GetHeadVisual();
	void ReinitVisualSection(LPDIRECT3DDEVICE9 g_pd3dDevice, CSymbolMapperObjList* symbolMapSys, int speechMode);
	void SafeDelete();
	void Clone(CtextReadoutObj** copy);
	CStringA FloatToStingCnv(float number);

	void ScrollDown(LPDIRECT3DDEVICE9 g_pd3dDevice, CSymbolMapperObjList* symbolMapSys, int speechMode);
	void ScrollUp(LPDIRECT3DDEVICE9 g_pd3dDevice, CSymbolMapperObjList* symbolMapSys, int speechMode);

	void Serialize(CArchive& ar);
};

class CtextReadoutObjList : public CObList {
public:
	CtextReadoutObjList(){};
	void Clone(CtextReadoutObjList** copy);
	void SafeDelete();

	DECLARE_SERIAL(CtextReadoutObjList)
};

} // namespace KEP