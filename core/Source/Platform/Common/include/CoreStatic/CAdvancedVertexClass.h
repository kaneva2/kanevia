///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CStructuresMisl.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

struct edge_struct {
	WORD v1, v2;
};

class IMemSizeGadget;

//	Forward declare so I can change my headers without recompiling
class VertexDeclaration;

class CAdvancedVertexObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CAdvancedVertexObj, VERSIONABLE_SCHEMA | 3) // 1_15_06 cpitzer....added m_triStripped member

public:
	CAdvancedVertexObj();

	/////////////////////////////////////////////////////////////////////////////
	// Start Encapsulations to regulate "user" behavior (Yanfeng 09/2008)
	// For existing users, we gradually phase out bad behaviors. Member variables
	// will stay public until all users are tamed.

	// Following implementations are temporarily and subject to change (Interface will stay).
	// Use inline style so that I can use pre-built library without recompile.

	~CAdvancedVertexObj();

	//! \brief Set number of texture channels per vertex
	void SetUVCount(int uvCount) {
		m_uvCount = uvCount;
	}

	//! \brief Return number of texture channels
	int GetUVCount() const {
		return m_uvCount;
	}

	UINT GetVertexCount() const {
		return m_vertexCount;
	}
	ABVERTEX GetVertex(UINT id) const;

	//! \brief Return vertex size in bytes based on m_uvCount
	UINT GetVertexSizeInBytes();

	//! \brief Return a pointer to entire vertex array buffer.
	//!
	//! Return vertex array. Be careful as actual vertex size varies (not ABVERTEX).
	//! Use in accompany with GetVertexSizeInBytes to access individual vertices.
	ABVERTEX* GetVertexArrayPtr();

	//! \brief Return pointer to a vertex inside vertex buffer
	//!
	//! pointer to a vertex inside vertex buffer. Be careful as actual vertex size varies (not ABVERTEX).
	//! Use in accompany with GetVertexSizeInBytes and GetUVCount to access valid members.
	//! Recommend only accessing position and normal data just to be safe.
	ABVERTEX* GetVertexPtr(UINT id);

	//! \brief Change data for a vertex
	void SetVertex(UINT id, ABVERTEX& vtx) {
		SetABVERTEXStyleVertex(&vtx, id);
	}

	//! \brief Copy entire vertex buffer and return copied version.
	void GetVertexArray(ABVERTEX*& aVertexArray, int& aNumVertices) {
		GetABVERTEXStyle(&aVertexArray, &aNumVertices);
	}

	//! \brief Copy entire vertex buffer and return copied version in STL vector.
	std::vector<ABVERTEX>* GetVertexArray();

	//! \brief Allocate and copy in entire vertex buffer, dispose old if exists.
	void SetVertexArray(const ABVERTEX* aVertexArray, UINT aNumVertices) {
		SetABVERTEXStyleData(aVertexArray, aNumVertices);
	}

	//! \brief Allocate and copy in entire vertex buffer, dispose old if exists. (STL vector version)
	void SetVertexArray(const std::vector<ABVERTEX>& vertices);

	//! \brief Reallocate vertex buffer and append vertices
	void AppendVertexArray(const ABVERTEX* aVertexArray, UINT aNumVertices);

	//! \brief Reallocate vertex buffer and append vertices (STL vector version)
	void AppendVertexArray(const std::vector<ABVERTEX>& vertices);

	//! \brief Return indices count
	UINT GetVertexIndexCount() const {
		return m_indecieCount;
	}
	// Call GetVertexIndex to obtain individual index
	// Call SetVertexIndex to set individual index

	//! \brief Copy entire index buffer and return copied version.
	void GetVertexIndexArray(UINT*& aIndexArray, UINT& aNumIndices) {
		GetVertexIndexArray(&aIndexArray, aNumIndices); // Standardized Signature
	}

	//! \brief Copy entire index buffer and return copied version in STL vector.
	std::vector<UINT>* GetVertexIndexArray();

	//! \brief Allocate and copy in entire index buffer. Dispose old if exists.
	void SetVertexIndexArray(const UINT* aIndexArray, UINT aNumIndices) {
		SetVertexIndexArray(aIndexArray, aNumIndices, GetVertexCount());
	}

	//! \brief Allocate and copy in entire index buffer. Dispose old if exists. (STL vector version)
	void SetVertexIndexArray(const std::vector<UINT>& indices);

	//! \brief Reallocate index buffer and append indices
	void AppendVertexIndexArray(const UINT* aIndexArray, UINT aNumIndices);

	//! \brief Reallocate index buffer and append indices (STL vector version)
	void AppendVertexIndexArray(const std::vector<UINT>& indices);

	//! \brief Transform all vertices (including positions and normals) by provided matrix
	void Transform(const Matrix44f& matrix);

	// End Encapsulations
	///////////////////////////////////////////////////////////////

	int m_uvCount;
	DWORD m_lockFlags;
	int m_advancedFixedMode;
	char m_tangentExists;

	//original data
	ABVERTEX0L* m_vertexArray0;
	ABVERTEX1L* m_vertexArray1;
	ABVERTEX2L* m_vertexArray2;
	ABVERTEX3L* m_vertexArray3;
	ABVERTEX4L* m_vertexArray4;
	ABVERTEX5L* m_vertexArray5;
	ABVERTEX6L* m_vertexArray6;
	ABVERTEX7L* m_vertexArray7;
	ABVERTEX8L* m_vertexArray8;

	ABVERTEX0Lb* m_vertexArray0t;
	ABVERTEX1Lb* m_vertexArray1t;
	ABVERTEX2Lb* m_vertexArray2t;
	ABVERTEX3Lb* m_vertexArray3t;
	ABVERTEX4Lb* m_vertexArray4t;
	ABVERTEX5Lb* m_vertexArray5t;
	ABVERTEX6Lb* m_vertexArray6t;

	ABVERTEX1Lx* m_vertexArray1x;

	//buffers
	LPDIRECT3DVERTEXBUFFER9 m_vertexBuffer;
	IDirect3DIndexBuffer9* m_indicieBuffer;
	//fixed data
	UINT m_indecieCount;
	UINT m_vertexCount;

	//	Vertex declaration to replace all the bullcrap with vertex formats
	VertexDeclaration* mVertexDeclaration;
	void BuildVertexDeclaration(LPDIRECT3DDEVICE9 g_pd3dDevice);

protected:
	// wrapper to allow having 16-bit or 32-bit indices based on the number of vertices
	typedef UINT (CAdvancedVertexObj::*GetVertexFunctPtr)(UINT aIndex);
	typedef void (CAdvancedVertexObj::*SetVertexFunctPtr)(UINT aIndex, UINT aVertexIndex);
	GetVertexFunctPtr m_GetVertexIndexPtr;
	SetVertexFunctPtr m_SetVertexIndexPtr;

	// Pre-requesite to AllocIndexArray: m_indecieCount and m_vertexCount are set to desired values
	void AllocIndexArray(void);
	UINT GetVertexIndex_USHORT(UINT aIndex);
	UINT GetVertexIndex_UINT(UINT aIndex);
	void SetVertexIndex_USHORT(UINT aIndex, UINT aVertexIndex);
	void SetVertexIndex_UINT(UINT aIndex, UINT aVertexIndex);

private:
	// No functional impact, used only to determine size in bytes
	// of allocated memory, if any
	size_t m_lastVertexBufferSize = { 0 };

public:
	USHORT* m_ushort_indexArray;
	UINT* m_uint_indexArray;
	BOOL m_triStripped;

public:
	inline BOOL IsIndexBufferInitialized() {
		return m_ushort_indexArray != NULL || m_uint_indexArray != NULL;
	}
	void GetVertexIndexArray(UINT** aIndexArray, UINT& aNumIndices);
	void SetVertexIndexArray(const UINT* aIndexArray, UINT aNumIndices, UINT aNumVertices);
	void SetVertexIndexArray(const USHORT* aIndexArray, UINT aNumIndices, UINT aNumVertices);
	inline UINT GetVertexIndex(UINT aIndex) {
		return (this->*m_GetVertexIndexPtr)(aIndex);
	}
	inline void SetVertexIndex(UINT aIndex, UINT aVertexIndex) {
		(this->*m_SetVertexIndexPtr)(aIndex, aVertexIndex);
	}

	void Serialize(CArchive& ar);
	void SafeDelete();
	void Invalidate();
	void CacheUnimediateData();
	int Render(LPDIRECT3DDEVICE9 g_pd3dDevice, int& vertexType);
	void InitBuffer(LPDIRECT3DDEVICE9 g_pd3dDevice, int requiredUVCount);
	void InitBuffer(LPDIRECT3DDEVICE9 g_pd3dDevice, int effect, int requiredUVCount, bool skinned = false);
	void ApplyMoveTexture(
		float& u,
		float& v,
		int textureSet,
		int& vertexType);
	void ApplySphereAlgorithm(Matrix44f matWV, int& vertexType, int uvSet);
	void CloneMinimum2(CAdvancedVertexObj* clone2, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void CloneDataOnly(CAdvancedVertexObj* clone2);
	void CloneMinimum(CAdvancedVertexObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice);

	void SetPositionXYZ(int& vIndex, float& x, float& y, float& z);
	void MoveTextureReset(float& u,
		float& v,
		TXTUV& uvStruct,
		TXTUV& origUvStruct,
		BOOL& resetU,
		BOOL& resetV);

	void SphereMapL1(Matrix44f matWV, int uvSetIndex);
	void SphereMapL2(Matrix44f matWV, int uvSetIndex);
	void SphereMapL3(Matrix44f matWV, int uvSetIndex);
	void SphereMapL4(Matrix44f matWV, int uvSetIndex);
	void SphereMapL5(Matrix44f matWV, int uvSetIndex);
	void SphereMapL6(Matrix44f matWV, int uvSetIndex);
	void SphereMapL7(Matrix44f matWV, int uvSetIndex);
	void SphereMapL8(Matrix44f matWV, int uvSetIndex);
	void SphereMapL1a(Matrix44f matWV, int uvSetIndex);
	void SphereMapL2a(Matrix44f matWV, int uvSetIndex);
	void SphereMapL3a(Matrix44f matWV, int uvSetIndex);

	void MoveTextureL1(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL2(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL3(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL4(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL5(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL6(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL7(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL8(float& u,
		float& v,
		int& textureSet,
		float& deltaT);
	void MoveTextureL1a(float& u,
		float& v,
		int& textureSet,
		float& deltaT);

	void MoveTextureL2a(float& u,
		float& v,
		int& textureSet,
		float& deltaT);

	void CAdvancedVertexObj::ReinitializeDxIndexBuffer(IDirect3DDevice9* pd3dDevice); // Added by Jonny to make Optimize work
	void InitializeIndexBuffer(IDirect3DDevice9* pd3dDevice, D3DPOOL memManage, DWORD bufUsage);
	void ReinitializeDxVbuffers(IDirect3DDevice9* pd3dDevice, D3DPOOL memManage,
		DWORD vUsage, int targetUVcount);
	void ReinitializeDxVbuffersAdvanced(IDirect3DDevice9* pd3dDevice, D3DPOOL memManage,
		DWORD vUsage, int effect);
	void ReinitializeDxVbuffersPositionOnly(IDirect3DDevice9* pd3dDevice);
	void ReinitializeDxVbuffersPosNormOnly(IDirect3DDevice9* pd3dDevice);

	void SetABVERTEXStyle(IDirect3DDevice9* pd3dDevice, const ABVERTEX* vArray, int vertexCount);
	void SetABVERTEXStyleData(const ABVERTEX* vArray, int vertexCount);
	void GetABVERTEXStyle(ABVERTEX** vArray, int* vertexCount) const;
	void GetABVERTEXStyleVertex(ABVERTEX* vtxBuffer, int id) const; // Get single vertex (separated from GetABVERTEXStyle to provide access to individual vertices)
	void SetABVERTEXStyleVertex(const ABVERTEX* vtxBuffer, int id); // Set single vertex (modified from Get function--I'll leave this ugly code style live for another month until I have time. -- Yanfeng 09/2008
	void SetPositionXYZNXNYNZ(int& vIndex,
		float& x, float& y, float& z,
		float& nx, float& ny, float& nz);
	void SetPositionXYZNXNYNZMEM(int vIndex,
		float x, float y, float z,
		float nx, float ny, float nz);
	void Fill0UVArrayData(ABVERTEX0L* array);
	void AddEdge(WORD* pEdges, DWORD& dwNumEdges, WORD v0, WORD v1);
	void AddEdge2(edge_struct* pEdges, DWORD& dwNumEdges, WORD& v0, WORD& v1);
	void FillD3DVECTORArrayData(Vector3f* array);
	D3DCOLOR DiffuseGradiant(float& curY, float& startRed, float& startGreen, float& startBlue,
		float& endRed, float& endGreen, float& endBlue,
		float& constantEffectStart, float& constantEffectEnd);
	float GetVertexX(int i);
	float GetVertexY(int i);
	float GetVertexZ(int i);
	float GetVertexNX(int i);
	float GetVertexNY(int i);
	float GetVertexNZ(int i);
	Vector3f CalculateTangentVector(D3DVERTEX& vector2, D3DVERTEX& vector3);
	void GenerateTangents(LPDIRECT3DDEVICE9 g_pd3dDevice);
	BOOL IntegrityCheck();
	void SetPositionXYZNXNYNZTGT(int& vIndex,
		float& x, float& y, float& z,
		float& nx, float& ny, float& nz,
		float& tx, float& ty, float& tz);
	void AVGTangents(float smoothBasisAngle);
	Vector3f GetTangentVector(int vIndex);
	BOOL GenerateTangentsPerfect(LPDIRECT3DDEVICE9 g_pd3dDevice);
	void AVGNormals(float smoothBasisAngle);
	void UnifyNormals(float angleTolerance);
	void NormalizeOnly();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

} // namespace KEP