///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "KEPConstants.h"

//#include "KEPConstants.h"
#include <deque>

namespace KEP {
class IMemSizeGadget;

/*
 * This is used to synchronize sounds, animations and particle effects right now.
 * Other things can be added by adding appropriate derived classes.
 */
class IEffectEvent {
public:
	IEffectEvent(TimeMs offset) :
			m_offset(offset), m_objType(ObjectType::NONE) {}
	virtual ~IEffectEvent() {}

	/// Run the effect.  The derived class knows how to do this for the effect it supports.
	virtual void execute() = 0;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	const TimeMs m_offset; // ms offset from the start of the track that's the time this effect event should be triggered
	ObjectType m_objType;

private:
	IEffectEvent(const IEffectEvent&);
};

typedef std::shared_ptr<IEffectEvent> IEffectEventPtr;

/*
 * Handle sorting and timing issues for effect tracks.
 */
class EffectTrack {
public:
	EffectTrack(ObjectType type) :
			m_startTime(0), m_type(type) {
	}
	~EffectTrack() {}

	ObjectType type() const {
		return m_type;
	}
	void setStartTime(TimeMs time) {
		m_startTime = time;
	}
	void addEffect(IEffectEventPtr); // add an effect and ensure the effect track remains sorted
	void update(TimeMs currentTime); // called every frame to advance the time and possibly execute pending effects
	void clearTrack();
	bool empty() const {
		return m_effectTrack.empty();
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	EffectTrack operator=(const EffectTrack&);
	EffectTrack(const EffectTrack&);

	TimeMs m_startTime;
	ObjectType m_type;
	std::deque<IEffectEventPtr> m_effectTrack;
};

} // namespace KEP
