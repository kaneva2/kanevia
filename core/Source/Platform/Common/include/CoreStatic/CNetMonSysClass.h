#pragma once

#if DEPRECATED

#include <set>

#include "Core/Util/fast_mutex.h"

#include <KEPNetwork.h>

class CNetMonSystem {
public:
	size_t GetCount() {
		std::lock_guard<fast_recursive_mutex> lock(m_mutex);
		return m_list.size();
	}

	size_t AddByID(NETID netId) {
		std::lock_guard<fast_recursive_mutex> lock(m_mutex);
		m_list.insert(netId);
		return m_list.size();
	}

	size_t DeleteByID(NETID netId) {
		std::lock_guard<fast_recursive_mutex> lock(m_mutex);
		m_list.erase(netId);
		return m_list.size();
	}

	bool IsOnline(NETID netId) {
		std::lock_guard<fast_recursive_mutex> lock(m_mutex);
		return (m_list.find(netId) != m_list.end());
	}

private:
	fast_recursive_mutex m_mutex;
	std::set<NETID> m_list;
};

#endif