///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "ListItemMap.h"
#include "KEPObList.h"

namespace KEP {

class CWldGroupObj : public CObject, public GetSet {
	DECLARE_SERIAL(CWldGroupObj)

public:
	CWldGroupObj() {}
	CWldGroupObj(const CStringA& groupName, int groupInt);

	void Serialize(CArchive& ar);

	int m_groupInt;
	CStringA m_groupName;

	DECLARE_GETSET
};

class CWldGroupObjList : public CKEPObList {
	DECLARE_SERIAL(CWldGroupObjList)

public:
	CWldGroupObjList() {
		m_pInstance = this;
	}
	virtual ~CWldGroupObjList() {
		m_pInstance = 0;
	}

	static CWldGroupObjList* GetInstance() {
		return m_pInstance;
	}
	ListItemMap GetGroups();

	CWldGroupObj* FindByID(int groupID);
	CWldGroupObj* FindByName(const CStringA& name);

private:
	static CWldGroupObjList* m_pInstance;
};

} // namespace KEP