///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include <d3d9.h>
#include <string>
#include <vector>
#include "common\include\MediaParams.h"

namespace KEP {

class IExternalMeshCleanUp {
public:
	virtual ~IExternalMeshCleanUp() {}
};

// External Mesh Base Class (derived -> FlashMesh, DirectShowMesh)
class IExternalMesh {
	std::shared_ptr<IDirect3DTexture9> m_texPtr;

public:
	Timer m_texTimer;
	uint32_t m_texWidth;
	uint32_t m_texHeight;
	IDirect3DTexture9* GetTexture() {
		return m_texPtr.get();
	}
	std::shared_ptr<IDirect3DTexture9> GetTextureShared() {
		return m_texPtr;
	}
	void SetTexture(std::shared_ptr<IDirect3DTexture9> p) {
		m_texPtr = std::move(p);
	}
	void SetTexture(IDirect3DTexture9* p) {
		std::shared_ptr<IDirect3DTexture9> pShared;
		if (p) {
			p->AddRef();
			pShared.reset(p, [](IDirect3DTexture9* p) { p->Release(); });
		}
		SetTexture(std::move(pShared));
	}

public: // Abstract Interface
	virtual std::string StrThis(bool verbose = false) const = 0;

	// DRF - Media Params
	MediaParams m_mediaParams;
	virtual MediaParams& GetMediaParams() {
		return m_mediaParams;
	}
	virtual bool SetMediaParams(const MediaParams& mediaParams) {
		return m_mediaParams.Set(mediaParams);
	}

	// DRF - Media Volume
	virtual bool SetVolume(double volPct = -1) {
		return m_mediaParams.SetVolume(volPct);
	}
	virtual double GetVolume() const {
		return m_mediaParams.GetVolume();
	}

	virtual int UpdateTexture() = 0;

	virtual void Delete() = 0;
	virtual void SetFocus() {} // Needed for FlashMesh when used as an interactive game.

	// optional
	virtual bool UpdateRect(int32_t x, uint32_t width, int32_t y, uint32_t height) {
		return false;
	}
	virtual bool SendInputMessage(UINT msg, WPARAM wp, LPARAM lp) {
		return false;
	}

protected:
	IExternalMesh() :
			m_texPtr(NULL), m_texWidth(0), m_texHeight(0) {
	}

	virtual ~IExternalMesh() {
	}
};

} // namespace KEP