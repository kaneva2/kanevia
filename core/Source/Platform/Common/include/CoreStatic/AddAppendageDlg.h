#pragma once

// AddAppendageDlg dialog

class AddAppendageDlg : public CDialog {
	DECLARE_DYNAMIC(AddAppendageDlg)

public:
	AddAppendageDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~AddAppendageDlg();

	// Dialog Data
	enum { IDD = IDD_ADD_APPENDAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int m_type;
	int m_slotCount;
};
