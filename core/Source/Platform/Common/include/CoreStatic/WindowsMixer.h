#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Core/Util/Parameter.h"
#include <functional>
#include <memory>

namespace KEP {

class IWindowsMixerProxy {
public:
	virtual ~IWindowsMixerProxy() {}
	virtual bool Initialize() = 0;
	virtual bool SetLabel(const char* pszLabel) = 0;
	virtual bool SetIconPath(const char* pszIconPath) = 0;
	virtual bool SetVolume(float fVolumeFraction) = 0;
	virtual bool GetVolume(Out<float> fVolumeFraction) = 0;
	virtual bool SetVolumeChangeCallback(std::function<void(float fNewVolume, bool bNewMute)> callback) = 0;
	virtual bool SetMute(bool bMute) = 0;
	virtual bool GetMute(Out<bool> bMute) = 0;

	static std::shared_ptr<IWindowsMixerProxy> New();
};

} // namespace KEP
