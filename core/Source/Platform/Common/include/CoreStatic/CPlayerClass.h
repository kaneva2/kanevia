///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

#include "InstanceId.h"
#include "IPlayer.h"
#include "CStructuresMisl.h"
#include <KEPNetwork.h>
#include "CharConfigClass.h"
#include "ItemAnimation.h"
#include "common\include\PassList.h"
#include "MovementInterpolator.h"
#include "Core/Util/CArchiveObjectSchema.h"

#include <string>

// anything over 10,000 is a database player
#define MAX_LOCAL_PLAYER_HANDLE 9999
#define TIME_UNDEFINED -1.0
#define NO_CLAN 0

//WOK specific genders for edb index
#define GENDER_UNKNOWN -1
#define GENDER_FEMALE_SMALL 10
#define GENDER_FEMALE_MED 11
#define GENDER_FEMALE_LARGE 12
#define GENDER_MALE_SMALL 13
#define GENDER_MALE_MED 14
#define GENDER_MALE_LARGE 15

namespace KEP {

class CAccountObject;
class CCommGroupList;
#define FriendList CCommGroupList
class CCurrencyObj;
class CInventoryObjList;
class CNoterietyObj;
class CPacketObjList;
class CQuestJournalList;
class CSkillObjectList;
class CStatObjList;
class CStatBonusList;
class CTitleObjectList;


// DRF - Added
struct PlayerOutfit {
	std::vector<GLID> glidList; // outfit glids list
};

class CPlayerObject : public CObject, public IPlayer, public GetSet {
	DECLARE_SERIAL_SCHEMA(CPlayerObject, VERSIONABLE_SCHEMA | 11)

public:
	CPlayerObject();

	// name related fields
	CStringA m_charName;
	std::string PlayerName() const {
		return m_charName.GetString();
	}

	CStringA m_displayName;
	CStringA m_lastName;
	CStringA m_clanName;
	CStringA m_title;
	CStringA m_displayTitle;

	CTitleObjectList* m_titleList;

	// DRF - Added
	std::map<std::string, PlayerOutfit> m_savedOutfits;

	MovementInterpolator m_movementInterpolator;

	void MovementInterpolatorInit(const MovementData& md) {
		m_movementInterpolator.Update(md, true);
	}

	void MovementInterpolatorUpdate(const MovementData& md) {
		m_movementInterpolator.Update(md);
	}

	MovementData InterpolatedMovementData() {
		return m_movementInterpolator.InterpolatedMovementData();
	}

	Vector3f InterpolatedPos() const {
		return m_movementInterpolator.InterpolatedPos();
	}

	Vector3f InterpolatedDir() const {
		return m_movementInterpolator.InterpolatedDir();
	}

	Vector3f InterpolatedUp() const {
		return m_movementInterpolator.InterpolatedUp();
	}

	float InterpolatedRotDeg() const {
		return m_movementInterpolator.InterpolatedRotDeg();
	}

	float InterpolatedPosSpeed() const {
		return m_movementInterpolator.InterpolatedPosSpeed();
	}

	MOVE_DATA InterpolatedMoveData() {
		MOVE_DATA moveData;
		auto md = InterpolatedMovementData();
		moveData.netTraceId = m_netTraceId;
		moveData.pos = md.pos;
		moveData.dir = md.dir;
		moveData.up = md.up;
		moveData.animGlid1 = m_animGLID;
		moveData.animGlid2 = m_animGLID2nd;
		return moveData;
	}

	// Last Move Data Sent From Client To Server For This Player
	MOVE_DATA m_moveDataToServer;

	// Last Move Data Sent From Server To Client For Other Players
	std::map<short, MOVE_DATA> m_moveDataToClientMap; // netTraceId -> MOVE_DATA
	void MoveDataToClientMapClear() {
		m_moveDataToClientMap.clear();
	}

	void MoveDataReset() {
		m_moveDataToServer = MOVE_DATA();
		MoveDataToClientMapClear();
	}

	// Encodes Move Data Delta Since Last Sent For Other Players -> MSG_ENTITY_POS_TO_CLIENT
	bool EncodeMoveDataToClient(const MOVE_DATA& moveData, std::vector<BYTE>& mtcd);

	void HandleMsgMoveToServer(BYTE* pData);

	Vector3f m_scaleFactor;
	Vector3f m_scratchScaleFactor;

	CStringA m_currentWorldMap;

	// if in runtime list this is users
	ChannelId m_currentChannel; //dynamically assigned
	// otherwise this is used
	ZoneIndex m_currentZoneIndex;
	unsigned m_parentZoneInstanceId;
	ZoneIndex m_previousZoneIndex;
	ZoneIndex m_housingZoneIndex; // WOK added to send back to housing

	int m_accountNumberRef;

	NETID m_netId; // directplay assigned random id (new netId)
	int m_netTraceId; // network assigned incrementing trace id (old netId)

	bool m_forcePlayerDepartEventOnSpawn = false; // Set to true if we want PlayerDepart to be sent upon spawning - otherwise it will be sent upon PlayerArrive event if player respawn in the same zone

private:
	ULONG m_currentProtocolVersion;

public:
	CQuestJournalList* m_questJournal;

	//Animation state
	int m_animationState;

	GLID m_animGLID; //- is reverse
	GLID m_animGLID2nd;

	//Current accessory animations
	std::vector<ItemAnimation> m_itemAnimations;

	BOOL m_inGame;
	BOOL m_justSpawned;
	Timer m_timerMsgRx;

	//Vehicle/PET mounts
	int m_mountedOnPlayerID; //-1 not riding anything
	int m_mountedByPlayerID; //-1 == not mounted
	//if mounted will be moved to appropriate position

	CInventoryObjList* m_inventory; // user client script affected inventory
	CInventoryObjList* m_scratchInventory; // server script affected inventory
	CInventoryObjList* m_bankInventory; //secure banking
	CInventoryObjList* m_preArenaInventory;

	CCurrencyObj* m_cash;
	CCurrencyObj* m_giftCredits;
	CCurrencyObj* m_bankCash;

	int m_dbCfg;

	//Initial Spawn when began, also death spawn
	int m_bornSpawnCfgIndex;

	//tag AI if spawned by an AI manager
	BOOL m_aiControlled;

	//deformable cfg (initialy done)
	size_t m_baseMeshes;
	CharConfig m_charConfig;

	float m_dynamicRadius;

	//skills/experience points
	CSkillObjectList* m_skills;
	CStatObjList* m_stats;

	//respawn config
	Vector3f m_respawnPosition;
	ZoneIndex m_respawnZoneIndex;

	//handle for chat and clans
	PLAYER_HANDLE m_handle; // changed from USHORT to get > 64K ids, use int since already serialized as int

	FriendList* m_friendList;

	PlayerPassList* m_passList; // list of passes this user holds, not serialized, only in db

	CPlayerObject* playerOrigRef() const {
		return m_playerOrigRef;
	}
	CPlayerObject& setPlayerOrigRef(CPlayerObject* player) {
		m_playerOrigRef = player;
		return *this;
	}

private:
	CPlayerObject* m_playerOrigRef;

public:
	Vector3f m_positionLock; //multi used (hiding)
	int m_altCfgIndex; //shapeshifted alternate cfg
	int m_curEDBIndex;
	int m_originalEDBCfg;
	CInventoryObjList* m_reconfigInven; //use when unshifting

	TimeMs m_skillUseTimeStamp; //time last used skill

	int m_aiCfgOfMounted; //-1 = none
	BOOL m_admin;
	BOOL m_gm;
	BOOL m_spawnAbility;

	CAccountObject* m_pAccountObject; // <CAccountObject*>

	int m_currentZonePriv; // see ZP_* above

	BOOL m_skillDBisReferenced;
	bool m_reconnecting;

	int m_interactionStates;

	struct NameColor {
		unsigned char r;
		unsigned char g;
		unsigned char b;
		NameColor(unsigned char r = 255, unsigned char g = 255, unsigned char b = 255) :
				r(r), g(g), b(b) {}
	} m_nameColor;

	// MSG_CAT::MOVE_TO_CLIENT Payload
	CPacketObjList* m_pMsgOutboxSpawnStaticMO;

	// MSG_CAT::UPDATE_TO_CLIENT Payload
	CPacketObjList* m_pMsgOutboxAttrib;
	CPacketObjList* m_pMsgOutboxEquip;
	CPacketObjList* m_pMsgOutboxGeneral;
	CPacketObjList* m_pMsgOutboxEvent;

	Timer m_msgMoveToClientTimer; // timer since last message move to client
	Timer m_msgUpdateToClientTimer; // timer since last message update to client

	TimeMs m_timeDbUpdate;

	ULONG m_dbDirtyFlags; // see IGameState for options

	Timer m_updateAcctTimer; // for handling respawns

	CNoterietyObj* m_noteriety;

	//clan binds
	PLAYER_HANDLE m_clanHandle;
	PLAYER_HANDLE m_currentRequestToJoinClanHandle;

private:
	Timer m_timerPacketSizeViolationSoftLimit;
	UINT m_packetSizeViolationSoftLimitCount;
	Timer m_timerPacketSizeViolationHardLimit;
	UINT m_packetSizeViolationHardLimitCount;

public:
	std::string ToStr(bool verbose = true) const override {
		std::string str;
		if (verbose) {
			StrBuild(str, "PO<" << PlayerName() << ":" << m_netId << "." << m_netTraceId << " " << m_currentZoneIndex.ToStr() << ">");
		} else {
			StrBuild(str, "PO<" << PlayerName() << ":" << m_netId << ">");
		}
		return str;
	}

	void Clone(CPlayerObject** ppPO_out);

	void SafeDelete();

	void Serialize(CArchive& ar);
	void SerializeAlloc(CArchive& ar);

	// figure out what instance of the zone we should be using
	ZoneIndex createInstanceId(const ZoneIndex& zoneIndex);

	void AllocatePacketLists();

	bool VerifyStatRequirement(CStatBonusList* requirements);

	virtual int GetNetTraceId() override {
		return m_netTraceId;
	}

	// GetSet overrides
	void getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s);

	IGetSet* Values() {
		return this;
	}

	ULONG getCurrentProtocolVersion() const {
		return m_currentProtocolVersion;
	}
	void setCurrentProtocolVersion(ULONG version) {
		m_currentProtocolVersion = version;
	}

	void recordPacketSizeViolation(TimeMs hardLimitPeriod, DWORD hardLimit, TimeMs softLimitPeriod, DWORD softLimit, bool& hardLimitHit, bool& softLimitHit);

	unsigned getMasterZoneInstanceId() const { // Return parent if child zone. Otherwise return self.
		return m_parentZoneInstanceId == 0 ? m_currentZoneIndex.GetInstanceId() : m_parentZoneInstanceId;
	}

	bool isPeerInSameOrLinkedWorld(const CPlayerObject* pPeer) const {
		return pPeer->m_currentZoneIndex == m_currentZoneIndex ||
			   (m_currentZoneIndex.isChannel() || m_currentZoneIndex.isHousing()) && getMasterZoneInstanceId() == pPeer->getMasterZoneInstanceId();
	}

	virtual Vector3f getLocation() const override { return InterpolatedPos(); }

	DECLARE_GETSET
};

class CPlayerObjectList : public CObList {
public:
	CPlayerObjectList(){};

	void SafeDelete();

	DECLARE_SERIAL(CPlayerObjectList)

	void Clone(CPlayerObjectList** ppPOL_out);

	CPlayerObject* GetPlayerObjectByName(const std::string& userName) const;
	CPlayerObject* GetPlayerObjectByNetId(const NETID& netId, bool includeSpawning) const;
	CPlayerObject* GetPlayerObjectByNetTraceId(int netTraceId) const;
	CPlayerObject* GetPlayerObjectByPlayerHandle(const PLAYER_HANDLE& handle) const;
	CPlayerObject* GetPlayerObjectByAccountNumber(int accNum) const;

	BOOL RemoveByPtrByLiveHandle(PLAYER_HANDLE handle);

	void SerializeAlloc(CArchive& ar, int loadCount);

	void ReinitMemory();
};

} // namespace KEP
