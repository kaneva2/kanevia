///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GROUPMODIFIERDLG_H__75BE5941_EA18_11D3_B1E8_0000B4BD56DD__INCLUDED_)
#define AFX_GROUPMODIFIERDLG_H__75BE5941_EA18_11D3_B1E8_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupModifierDlg.h : header file
//
#include "ctextureDBClass.h"
#include "CRTLightClass.h"
#include "CCharonaClass.h"
/////////////////////////////////////////////////////////////////////////////
// CGroupModifierDlg dialog

class CGroupModifierDlg : public CDialog {
	// Construction
public:
	CGroupModifierDlg(CWnd* pParent = NULL); // standard constructor
	float diffuseRed;
	float diffuseGreen;
	float diffuseBlue;
	float emmisiveRed;
	float emmisiveGreen;
	float emmisiveBlue;
	float specularRed;
	float specularGreen;
	float specularBlue;
	int blendMode;
	BOOL m_diffuseEffectEnable;
	BOOL m_applyGlobal;
	BOOL normalModEnable;
	BOOL multipassModEnable;
	BOOL materialModEnable;
	BOOL inipendentNormalType;
	BOOL boundingBoxModifier;
	BOOL hiarchyModifier;
	BOOL wrapModifier;
	BOOL boundingBoxModifierMirror;
	BOOL meshOptimizer;
	BOOL textureDataSwapEnable;
	BOOL filterCollisionModEnable;
	BOOL filterCollisionOn;
	BOOL bRenderFilter;
	BOOL bRenderFilterEnable;
	BOOL UnifyVertsByTolerance;
	BOOL particleEmmiterEnable;
	BOOL boxAbsolute;
	int wrapType;
	BOOL m_groupIdModifier;
	LPDIRECT3DDEVICE9 g_pd3dDeviceRef;

	CRTLightObj* m_lightObject;
	CCharonaObj* m_charonaObject;
	BOOL m_enableLightOption;
	BOOL m_enableCharonaOption;

	TextureDatabase* GL_textureDataBase;

	// Dialog Data
	//{{AFX_DATA(CGroupModifierDlg)
	enum { IDD = IDD_DIALOG_GROUPMODIFIER };
	CButton m_enableParticleEmmitter;
	CButton m_enableUnifyVerts;
	CButton m_enableGroupIDchange;
	CButton m_checkTextureDataSwap;
	CButton m_checkMeshOptimizer;
	CButton m_enableBoundingBoxMirror;
	CButton m_checkWrapModEnable;
	CButton m_enableHiarcyMod;
	CButton m_enableBoundingBoxMod;
	CButton m_checkMultiTextureModEnable;
	CButton m_checkIndipendentNormalize;
	CButton m_checkGroupNormalize;
	CButton m_checkGlobalMaterialModEnable;
	CButton m_checkEnableNormalMod;
	CButton m_checkGroupBoxAbsolute;
	CStringA m_multitextureMethod;
	CComboBox m_comboGroups;
	CComboBox m_comboParticles;
	float m_groupMultiplier;
	int m_groupHiarchy;
	CStringA m_comboWrapType;
	float m_bndBoxMaxX;
	float m_bndBoxMinX;
	float m_bndBoxMaxY;
	float m_bndBoxMinY;
	float m_bndBoxMaxZ;
	float m_bndBoxMinZ;
	int m_texturedataSwapIndex1;
	int m_texturedataSwapIndex2;
	int m_modGroupID;
	float m_tolerance;
	int m_particleDBref;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupModifierDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGroupModifierDlg)
	afx_msg void OnBUTTONDiffuseMat();
	afx_msg void OnBUTTONEmmisiveMat();
	afx_msg void OnBUTTONSpecularMat();
	afx_msg void OnCHECKEnableNormalsMod();
	afx_msg void OnCHECKGlobalMaterialModEnable();
	afx_msg void OnCHECKGroupNormalize();
	afx_msg void OnCHECKIndipendentNormalize();
	afx_msg void OnCHECKMultiTextureModEnable();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnCHECKEnableBoxModifier();
	afx_msg void OnCHECKEnableDependencyMod();
	afx_msg void OnCHECKWrapModEnable();
	afx_msg void OnCHECKEnableBoundingBox();
	afx_msg void OnCHECKMeshOptimizer();
	afx_msg void OnCHECKTextureDataSwap();
	afx_msg void OnCHECKCollisionFilterOn();
	afx_msg void OnCHECKRenderFilter();
	afx_msg void OnCHECKEnableCollisionFilterMod();
	afx_msg void OnCHECKEnableRenderFilter();
	afx_msg void OnCHECKEnableGroupIDchange();
	afx_msg void OnCHECKEnableUnifyVerts();
	afx_msg void OnBUTTONLoadBox();
	afx_msg void OnCHECKenableParticleEmmitter();
	afx_msg void OnBUTTONinitializeLightObject();
	afx_msg void OnBUTTONinitializeCharonaObject();
	afx_msg void OnCHECKenableCharonaObject();
	afx_msg void OnCHECKenableLightObject();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitGroupList();
	void InitParticleList();

public:
	int m_diffuseVxEffect;
	afx_msg void OnBnClickedCheckenablediffuseeffect();
	afx_msg void OnBnClickedCheckAffectglobal();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPMODIFIERDLG_H__75BE5941_EA18_11D3_B1E8_0000B4BD56DD__INCLUDED_)
