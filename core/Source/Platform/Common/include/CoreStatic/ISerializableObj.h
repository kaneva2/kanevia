///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "..\KEPUtil\Algorithms.h"

namespace KEP {

class ISerializableObj {
public:
	ISerializableObj(void);
	~ISerializableObj(void);

	/**
	*The path of the file this object was lasted serialized to or deserialized from
	**/
	virtual FileName GetLastSavedPath() const = 0;

	/**
	*Whether the object's state has changed and requires saving
	**/
	virtual BOOL IsDirty() const = 0;

	/**
	*It will be better if this state is updated automatically, but having public member variables
	*in classes derive from CKEPObject makes it impossible.
	*So, to make it work, SetDirty() needs to be called manually whenever the object is changed
	**/
	virtual void SetDirty(BOOL dirty = TRUE) = 0;
};

} // namespace KEP