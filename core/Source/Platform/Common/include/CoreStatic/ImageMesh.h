/******************************************************************************
 ImageMesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#error DEAD CODE?

#include "ExternalMesh.h"
#include <string>
#include <log4cplus/Logger.h>

#include <js.h>
#include <jsLibraryFunction.h>

using namespace log4cplus;

namespace KEP {

class IDispatcher;

class ImageMesh : public IExternalMesh {
public:
	ImageMesh(IDispatcher* disp);

	bool Initialize(IN const char* baseDir,
		IN HWND parent, IN const char* url,
		IN DWORD width = 0, IN DWORD height = 0,
		IN DWORD x = ULONG_MAX, IN DWORD y = ULONG_MAX);

	virtual int UpdateTexture(IN LPDIRECT3DDEVICE9 dev);
	virtual bool SendInputMessage(UINT msg, WPARAM wp, LPARAM lp);

	virtual bool UpdateRect(IN INT x, IN INT width, IN INT y, IN INT height);

	void Navigate(int type);

	~ImageMesh();

protected:
	string m_baseDir;

	ULONG m_imageWidth; // size of window rendered
	ULONG m_imageHeight; // size of window rendered
	int m_colorDepth;
	LPDIRECT3DDEVICE9 m_pDIRECT3DDEVICE9;

	string m_url; // name of swf to play the movie

	// performance tweaking
	static ULONG m_waitMs; // how long to wait between scrapes
	DWORD m_lastRender; // last render time
	Logger m_logger;

	//1 ImageWindow stuff ==================================
	LPDIRECT3DTEXTURE9 CreateTextureFromWindow();
	bool InitImageWindow(IN HWND parent);
	int UpdateImageWindowTexture();
	LPDIRECT3DTEXTURE9 Image_Render(LPDIRECT3DDEVICE9 device3d);

	CBitmap m_bitmap;
};

} // namespace KEP

