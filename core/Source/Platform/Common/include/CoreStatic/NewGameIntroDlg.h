#pragma once

#include "KEPPropertyPage.h"
#include "resource.h"

// CNewGameIntroDlg dialog

class CNewGameIntroDlg : public CKEPPropertyPage {
	DECLARE_DYNAMIC(CNewGameIntroDlg)

public:
	CNewGameIntroDlg();
	virtual ~CNewGameIntroDlg();

	// Dialog Data
	enum { IDD = IDD_NEW_GAME_INTRO };

	virtual LRESULT OnWizardNext();

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	CString m_destDir;
	bool m_skipNextPage;
};
