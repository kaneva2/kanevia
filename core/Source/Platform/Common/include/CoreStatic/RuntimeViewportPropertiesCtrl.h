/******************************************************************************
 RuntimeViewportPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_RUNTIME_VIEWPORT_PROPERTIES_CTRL_H_)
#define _RUNTIME_VIEWPORT_PROPERTIES_CTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RuntimeViewportPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CViewportsPropertiesCtrl window

class CRuntimeViewportPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CRuntimeViewportPropertiesCtrl();
	virtual ~CRuntimeViewportPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	void Populate();

	// accessors
	int GetRuntimeCameraID() const {
		return m_runtime_camera_id;
	}

	// modifiers
	void SetRuntimeCameraID(int runtime_camera_id) {
		m_runtime_camera_id = runtime_camera_id;
	}

	// methods
	void Reset();

	void UpdateRuntimeCameras();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CViewportsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CViewportsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitRuntimeCameras();

private:
	enum { INDEX_ROOT = 1,
		INDEX_RUNTIME_CAMERA
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	int m_runtime_camera_id;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_RUNTIME_VIEWPORT_PROPERTIES_CTRL_H_)
