///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "MatrixARB.h"

namespace KEP {

class CABFunctionLib {
public:
	//-----file extension manipulation -----------------------------//
	static CStringA GetFileExt(CStringA currentName);
	static void RemoveFileExt(CStringA* currentName);
	static void AddNewFileExt(CStringA* currentName, CStringA newExt);
	static Vector3f GetRandomPointInRadius(float radius, const Vector3f& origin, float minimum);
};

} // namespace KEP