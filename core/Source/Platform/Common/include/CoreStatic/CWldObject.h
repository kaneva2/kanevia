///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CStructuresMisl.h"
#include <mmsystem.h>
#include "dsound.h"
#include "KEPObList.h"
#include "IResource.h"
#include "CStaticMeshObject.h"
#include "..\..\..\RenderEngine\ReMaterial.h"
#include <KEPPhysics/Declarations.h>
#include "common/include/ObjectCounter.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {
class IExternalEXMesh;
class CRTLightObj;
class CAttributeObj;
class CContainmentObject;
class CMeshLODObjectList;
class CNodeObj;
class CReferenceObj;
class CReferenceObjList;
class CSoundRangeObject;
class CTriggerObjectList;
class CVertexAnimationModule;
class ReMesh;
class TextureDatabase;

// See worldobjectpropertiesdlg.cpp
enum class MeshEnvEffect {
	Disabled,
	LockEmissiveModulation,
	LockEmissive,
	MultiPassModulate2X,
};

class CWldObject : private ObjectCounter<CWldObject>, public CStaticMeshObj {
protected:
	CWldObject() :
			m_customMaterial(NULL),
			m_customTextureOrig(""),
			m_outline(false),
			m_outlineColor(1, 1, 1, 1),
			m_outlineZSorted(false) {
		mDistToCam = 0.0f;
	}

	DECLARE_SERIAL_SCHEMA(CWldObject, VERSIONABLE_SCHEMA | 11) // version 11: particle direction fix

public:
	CWldObject(int GlobalTraceNumber);
	~CWldObject();

	const static UINT m_maxLODs = 8;

	BNDBOX m_popoutBox;
	BOOL m_removeEnvironment;
	int m_usedAsSpawnPoint;
	int m_miscInt;
	float m_miscFloat;
	int m_environmentAttached;
	IDirectSoundBuffer* m_baseSoundBuffer;
	IDirectSound3DBuffer* m_buffer3DControl;
	CStringA m_soundFileName;
	float mDistToCam;
	BOOL m_useSound;
	float m_soundFactor;
	BOOL m_isPlaying;
	float m_validRange;
	CAttributeObj* m_attribute;
	int m_collisionType;
	float m_popoutRadius;
#if (DRF_WORLD_ENVIRONMENT_LIST == 1)
	BOOL m_poppedOut;
#endif
	int m_identity;
	int m_groupNumber;
	BOOL m_showBox;
	CTriggerObjectList* m_triggerList;
	CContainmentObject* m_containmentObject;
	CMeshLODObjectList* m_lodVisualList;
	CEXMeshObj* m_meshObject;
	BOOL m_renderInMirrorTag;
	Vector3f m_particleDirection;
	UINT m_runtimeParticleId;
	int m_haloLink;
	int m_lensFlareType;
	int m_particleLink;
	CStringA m_fileNameRef;
	BOOL m_collisionInfoFilter;
	BOOL m_renderFilter;
	CSoundRangeObject* m_soundRange;
	int m_filterGroup; //-1 = none
	CNodeObj* m_node;
	CRTLightObj* m_lightObject;
	CVertexAnimationModule* m_animModule;
	int m_envReactionAttrib;
	CStringA m_description;
	CStringA m_name;
	BOOL m_customizableTexture;
	int m_assetId;
	BOOL m_canHangPictures;
	GUID m_uniqueId;
	BOOL m_culled;

	ReMesh* m_ReMesh;
	ReMesh* m_ReLODMesh[m_maxLODs];
	void SetReMesh(ReMesh* mesh) {
		m_ReMesh = mesh;
	}
	ReMesh* GetReMesh() const {
		return m_ReMesh;
	}
	ReMesh* GetReLODMesh(UINT index) {
		return index < m_maxLODs ? m_ReLODMesh[index] : NULL;
	}
	void InitReMeshes();

	// GPU shader matrices
	Matrix44fA16 m_WorldViewProjMatrix;
	Matrix44fA16 m_WorldInvTransposeMatrix;

	// runtime light cache
	ReLightCache m_lightCache;

	IExternalEXMesh* m_externalSource; // video mesh support
	CMaterialObject* m_material;

	BOOL UpdateLods(); // Synchronize LOD data: copy material and texture settings down to individual LODs
	void Serialize(CArchive& ar);
	void SafeDelete();
	float GetDistToCam(Vector3f* camPosition);

	void Transform(const Vector3f& aScale, const Vector3f& aRotate, const Vector3f& aTranslate, const Vector3f* aPivotOverride = NULL);

	//! \brief Replace mesh with new data, rebuild ReMesh (refactored from KepEditView)
	//! \param newMesh CEXMeshObj containing mesh data.
	//! \param bUpdateMatrix Set to TRUE if matrix needs to be copied from CEXMeshObj as well.
	void SetMesh(CEXMeshObj* newMesh, BOOL bUpdateMatrix = TRUE);

	//! \brief Replace material, rebuild ReMaterial (refactored from KepEditView)
	//! \param newMaterial CMaterialObject containing material data.
	//! \param zoneName Name of current zone. (used for creating ReMaterial)
	void SetMaterial(CMaterialObject* newMaterial, const char* zoneName);

	// return true if valid boundbox exists
	bool GetBoundingBox(Vector4f& min, Vector4f& max);

	ABBOX CacheBoundingBox();

	FLOAT GetBoundingRadius();

	bool IntersectRay(const Vector3f& origin, const Vector3f& direction, float* distance, Vector3f* location, Vector3f* normal);

	void CalibrateCenteredBoundingBox(int sightRange);

	void SetCustomMaterial(CMaterialObject* mat);
	void ResetCustomMaterial();
	CMaterialObject* GetCustomMaterial() const;
	void UpdateCustomMaterials();

	void SetCustomTexture(const std::string& textureFileName, eTexturePath texPath = eTexturePath::CustomTexture, const char* subDir = NULL);
	std::string GetCustomTexture() const {
		return m_customTexture;
	}

	void SetCustomTextureUrl(const std::string& url) {
		m_customTextureUrl = url;
	}
	std::string GetCustomTextureUrl() const {
		return m_customTextureUrl;
	}

	void SaveChanges(); //save changes made to texture
	void CancelChanges(); //cancel changes made to texture

	using ObjectCounter<CWldObject>::howMany;

	static size_t howMany() {
		return ObjectCounter<CWldObject>::howMany();
	}

	const Vector3f& GetScale() const;
	const Vector3f& GetRotation() const;
	const Vector3f& GetTranslation() const;
	const Matrix44f& GetWorldTransform() const;

	void SetScaleTranslationRotation(const Vector3f& scale, const Vector3f& translation, const Vector3f& rotation);
	void SetScale(const Vector3f& scale);
	void SetRotation(const Vector3f& rotation);
	void SetTranslation(const Vector3f& translation);

	const Matrix44f& GetMatrix() const; // OVERRIDE CStaticMeshObj
	const ABBOX& GetBoundingBox() const; // OVERRIDE CStaticMeshObj
	const CMaterialObject* GetMaterial() const; // OVERRIDE CStaticMeshObj
	std::string GetName() const; // OVERRIDE CStaticMeshObj

	void SetBoundingBox(const ABBOX& box);

	CReferenceObj* GetReferenceObj() const;

	//! \brief TRUE if current world object is a reference
	BOOL IsReference() const {
		return m_node != NULL;
	}

	//! \brief Pivot point.
	//
	// As of 3/5/09, it's the center of the untransformed base mesh at initial import.
	// But if game artist replaced a mesh into an existing world obj, the old pivot point
	// will be used. It might not necessarily be the center of the new mesh then.
	Vector3f GetPivot() const; // OVERRIDE CStaticMeshObj

	bool GetOutlineState() const {
		return m_outline;
	}
	void SetOutlineState(bool outline) {
		m_outline = outline;
	}

	void SetOutlineColor(const D3DXCOLOR& color) {
		m_outlineColor = color;
	}
	D3DXCOLOR GetOutlineColor() const {
		return m_outlineColor;
	}

	void SetOutlineZSorted(bool sort) {
		m_outlineZSorted = sort;
	}
	bool IsOutlineZSorted() const {
		return m_outlineZSorted;
	}

	bool IsInteractable() const {
		return m_customizableTexture || m_canHangPictures;
	}

private:
	std::string m_customTextureUrl; //where the custom texture is downloaded from
	std::string m_customTexture; //where the custom texture is
	CMaterialObject* m_customMaterial;
	std::string m_customTextureOrig; //backup of orginal setting, m_customTexture is the current setting that may not have been saved
	eTexturePath m_customTexturePath;
	std::string m_customTextureSubDir;

	Vector4f m_min;
	Vector4f m_max;
	bool m_cached_box;

	Vector3f m_pivot;

	//! \brief Flag indicating whether base mesh has been set since the creation of this wldobject.
	bool m_isMeshAssigned;

	bool m_outline;
	D3DXCOLOR m_outlineColor;
	bool m_outlineZSorted; // true if the glowing outline is depth tested
};

struct DirectAccessStructWLD {
	CWldObject* object;
};

class CWldObjectList : public CKEPObList {
public:
	CWldObjectList() {
		directAccess = NULL;
		directAccessObjectCount = 0;
	};

	void CacheUnimediateData();
	DirectAccessStructWLD* directAccess;
	int directAccessObjectCount;

	void IntegrityCheck();
	void CompileDirectAccess();
	void Serialize(CArchive& ar);

	DECLARE_SERIAL_SCHEMA(CWldObjectList, VERSIONABLE_SCHEMA | 1)
};

} // namespace KEP