///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\KEPUtil\Helpers.h"
#include <string>
#include "d3d9.h"
#include "d3dx9.h"
#include "dxerr9.h"

namespace KEP {

class DxDevice {
	static DxDevice* Instance; // singleton instance

	DxDevice() {}

public:
	virtual ~DxDevice() {}

	static DxDevice* Ptr(); // singleton instance

	static ULONGLONG GetTextureMemoryUsage(const D3DSURFACE_DESC& texSurfaceDesc) {
		double bpp = 4;
		auto format = texSurfaceDesc.Format;
		if (format == D3DFMT_DXT1)
			bpp = .5;
		else if (format == D3DFMT_DXT2 || format == D3DFMT_DXT3 || format == D3DFMT_DXT4 || format == D3DFMT_DXT5)
			bpp = 1;
		return texSurfaceDesc.Width * texSurfaceDesc.Height * bpp;
	}

	// DRF - Sets Helpers::GlobalD3DDevice() used by everyone everywhere always.
	void SetDevice(LPDIRECT3DDEVICE9 pDevice);

	// DRF - Texture Memory Statistics
	struct TextureMemoryStats {
		ULONGLONG m_num; // number of texture memory checks
		ULONGLONG m_avg; // average texture memory available (bytes)
		ULONGLONG m_min; // minimum texture memory available (bytes)
		ULONGLONG m_max; // maximum texture memory available (bytes)
		ULONGLONG m_avail; // current texture memory available (bytes)

		ULONGLONG m_expVid; // expected video memory usage (bytes)
		ULONGLONG m_expSys; // expected system memory usage (bytes)

		ULONGLONG m_usedVid; // used video memory (bytes)

		TextureMemoryStats() :
				m_expVid(0),
				m_expSys(0) {
			Reset();
		}

		void Reset() {
			m_num = 0;
			m_avg = 0;
			m_min = (ULONGLONG)-1;
			m_max = 0;
			m_avail = 0;
			//m_expVid = 0;
			//m_expSys = 0;
			m_usedVid = 0;
		}

		void Log();
	};

	// Updates and returns the texture memory statistics with optional reset.
	TextureMemoryStats GetTextureMemoryStats(bool resetStats = false);

	// ED-7732 - Texture Scaling
	void SetExpectedTextureMemoryUsage(ULONGLONG vidBytes, ULONGLONG sysBytes) {
		std::lock_guard<fast_mutex> lock(m_mtxTms);
		m_tms.m_expVid = vidBytes;
		m_tms.m_expSys = sysBytes;
	}

	// ED-7732 - Texture Scaling
	void ClearAllTextureMemoryUsage() {
		std::lock_guard<fast_mutex> lock(m_mtxTms);
		m_tms.m_usedVid = 0;
	}
	ULONGLONG GetAllTextureMemoryUsage() {
		std::lock_guard<fast_mutex> lock(m_mtxTms);
		return m_tms.m_usedVid;
	}

	// Returns if the client window is currently in view.
	bool IsInView();

	/** DRF
	* Most DirectX functions fail while the graphics device is out of view or minimized.
	* This function checks if the last call failed due to this and if so sleeps until the
	* device is once again in view and ready to retry the last function call as recommended
	* by Microsoft.  This function should be called in a loop like:
	*    do { hr=DirectXFunc(); } while(HResultFailedOutOfView(hr));
	*/
	enum class eCreateTextureResult {
		SUCCESS,
		FAIL,
		RETRY,
	};
	eCreateTextureResult HResultFailedOutOfView(HRESULT hr, const std::string& msg, bool sleepOutOfView);

	bool GetSurfaceDesc(const LPDIRECT3DTEXTURE9& pTex, D3DSURFACE_DESC& texSurfaceDesc_out);

	eCreateTextureResult CreateTextureFromFileInMemoryEx(
		const std::string& name,
		LPCVOID pSrcData,
		UINT srcDataSize,
		LPDIRECT3DTEXTURE9* ppTex_out,
		D3DSURFACE_DESC& texSurfaceDesc_out, // drf - added
		D3DRESOURCETYPE resourcetype,
		bool useTexLod,
		UINT width,
		UINT height,
		D3DCOLOR colorKey,
		bool forceFullRes,
		bool transcodeJPG, // uncompress JPG texture and re-encode it into DXT1
		bool retryOutOfView);

	HRESULT DownsizeTexture(const LPDIRECT3DTEXTURE9& pTex_in, LPDIRECT3DTEXTURE9* ppTex_out);

	bool UnloadTexture(LPDIRECT3DTEXTURE9 pTex, D3DSURFACE_DESC& texSurfaceDesc);

private:
	fast_mutex m_mtxTms;
	TextureMemoryStats m_tms; // current texture memory statistics.
};

} // namespace KEP