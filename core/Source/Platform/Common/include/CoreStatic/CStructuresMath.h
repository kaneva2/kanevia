#pragma once

#include "d3d9.h"
#include <D3DX9.h>

#include "Core/Math/KEPMath.h"

namespace KEP {

typedef struct _A3DMATRIX {
	float _11, _12, _13, _14;
	float _21, _22, _23, _24;
	float _31, _32, _33, _34;
	float _41, _42, _43, _44;

	float m[4][4];
	_A3DMATRIX() {}
	_A3DMATRIX(float _m00, float _m01, float _m02, float _m03,
		float _m10, float _m11, float _m12, float _m13,
		float _m20, float _m21, float _m22, float _m23,
		float _m30, float _m31, float _m32, float _m33) {
		m[0][0] = _m00;
		m[0][1] = _m01;
		m[0][2] = _m02;
		m[0][3] = _m03;
		m[1][0] = _m10;
		m[1][1] = _m11;
		m[1][2] = _m12;
		m[1][3] = _m13;
		m[2][0] = _m20;
		m[2][1] = _m21;
		m[2][2] = _m22;
		m[2][3] = _m23;
		m[3][0] = _m30;
		m[3][1] = _m31;
		m[3][2] = _m32;
		m[3][3] = _m33;
	}
} A3DMATRIX, *LPA3DMATRIX;

struct TXTUV {
	float tu, tv;
};

struct SHADOWVERTEX {
	D3DXVECTOR4 p;
	D3DCOLOR color;
};

struct ABVERTEX {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	TXTUV tx4;
	TXTUV tx5;
	TXTUV tx6;
	TXTUV tx7;
	float tx, ty, tz;
};

struct D3DVERTEX {
	float x, y, z;
	float nx, ny, nz;
	float tu, tv;
};

struct ABVERTEX0L {
	float x, y, z;
	float nx, ny, nz;
};

struct ABVERTEX0La {
	float x, y, z;
	float nx, ny, nz;
	D3DCOLOR diffuse;
};

struct ABVERTEX0Lb {
	float x, y, z;
	float nx, ny, nz;
	float tX, tY, tZ;
};

struct ABVERTEX1L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
};

struct ABVERTEX1LNT {
	float x, y, z;
	TXTUV tx0;
};

struct ABVERTEX1La {
	float x, y, z;
	float nx, ny, nz;
	D3DCOLOR diffuse;
	TXTUV tx0;
};

struct ABVERTEX1Lb {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	float tX, tY, tZ;
};

struct ABVERTEX2L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
};

struct ABVERTEX2La {
	float x, y, z;
	float nx, ny, nz;
	D3DCOLOR diffuse;
	TXTUV tx0;
	TXTUV tx1;
};

struct ABVERTEX1Lx {
	float x, y, z;
	float ox, oy, oz;
	D3DCOLOR diffuse;
	TXTUV tx0;
	TXTUV tx1;
};

struct ABVERTEX2Lb {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	float tX, tY, tZ;
};

struct ABVERTEX3L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
};

struct ABVERTEX3La {
	float x, y, z;
	float nx, ny, nz;
	D3DCOLOR diffuse;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
};

struct ABVERTEX3Lb {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	float tX, tY, tZ;
};

struct ABVERTEX4L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
};

struct ABVERTEX4La {
	float x, y, z;
	float nx, ny, nz;
	D3DCOLOR diffuse;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
};

struct ABVERTEX4Lb {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	float tX, tY, tZ;
};

struct ABVERTEX5L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	TXTUV tx4;
};

struct ABVERTEX5Lb {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	TXTUV tx4;
	float tX, tY, tZ;
};

struct ABVERTEX6L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	TXTUV tx4;
	TXTUV tx5;
};

struct ABVERTEX6Lb {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	TXTUV tx4;
	TXTUV tx5;
	float tX, tY, tZ;
};

struct ABVERTEX7L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	TXTUV tx4;
	TXTUV tx5;
	TXTUV tx6;
};

struct ABVERTEX8L {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
	TXTUV tx1;
	TXTUV tx2;
	TXTUV tx3;
	TXTUV tx4;
	TXTUV tx5;
	TXTUV tx6;
	TXTUV tx7;
};

struct ABVERTEXONE {
	float x, y, z;
	float nx, ny, nz;
	TXTUV tx0;
};

struct ABBOX {
	float minX;
	float maxX;
	float minY;
	float maxY;
	float minZ;
	float maxZ;

	Vector3f GetMin() const { return Vector3f(minX, minY, minZ); }
	Vector3f GetMax() const { return Vector3f(maxX, maxY, maxZ); }
	Vector3f GetCenter() const { return 0.5f * (GetMin() + GetMax()); }
	Vector3f GetSize() const { return GetMax() - GetMin(); }
};

struct ABSPECFACE {
	float x;
	float y;
	float z;
	char sw;
};

typedef struct _BNDBOX {
	Vector3f min, max;

	const Vector3f& GetMin() const { return min; }
	const Vector3f& GetMax() const { return max; }
	Vector3f GetCenter() const { return 0.5f * (GetMin() + GetMax()); }
	Vector3f GetSize() const { return GetMax() - GetMin(); }
} BNDBOX;
typedef BNDBOX* LPBNDBOX;

} // namespace KEP