///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

#include "CStructuresMisl.h"
#include "ActorControlType.h"
#include "PassGroups.h"
#include "EffectTrack.h"
#include "../../KEPPhysics/Declarations.h"

#include <KEPNetwork.h>
#include "ListItemMap.h"
#include "KEPObList.h"
#include <d3d9types.h>
#include "MovementInterpolator.h"
#include "Core/Util/CArchiveObjectSchema.h"

#include "common\include\CoreStatic\RuntimeSkeleton.h"
#include "common\include\CoreStatic\AnimSettings.h"

namespace KEP {

class IMemSizeGadget;
class IModel;
class CAIObj;
class CAlterUnitDBObjectList;
class CArmedInventoryList;
class CCfgObjList;
class CDeformableMesh;
class CFrameObj;
class CInventoryObjList;
class CItemObj;
class CItemObjList;
class CMaterialObject;
class CMovementObjList;
class CMovementCaps;
class CSkillObjectList;
class CSkeletalLODObjectList;
class CStatObjList;
class ReSkinMesh;
class SkeletonConfiguration;
class UGCActor;
struct LookAtInfo;
enum SERVER_ITEM_TYPE;

class CMovementObj : public CObject, public GetSet {
protected:
	CMovementObj();
	CMovementObj(const std::string& EntityName, int traceNumber, int networkDefinedID = -1);

	DECLARE_SERIAL_SCHEMA(CMovementObj, VERSIONABLE_SCHEMA | 4)

public:
	virtual ~CMovementObj();

	void Serialize(CArchive& ar);
	void SafeDelete();

	void init();

	BOOL Clone(
		CMovementObj** ppMO_out,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int* pSpawnCfgOpt,
		int* pMaterialCfgOpt,
		D3DCOLORVALUE* pSpawnColorOpt,
		bool fullClone,
		int traceNumber = -1,
		int netId = -1);

	BOOL CMovementObj::Clone(
		CMovementObj** ppMO_out,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int* pSpawnCfgOpt,
		int* pMaterialCfgOpt,
		D3DCOLORVALUE* pSpawnColorOpt,
		int traceNumber,
		int netId);

	BOOL Clone(
		CMovementObj** ppMO_out,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int* pSpawnCfgOpt,
		int* pMaterialCfgOpt,
		D3DCOLORVALUE* pSpawnColorOpt);

	BOOL Clone(
		CMovementObj** ppMO_out,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int* pSpawnCfgOpt,
		int* pMaterialCfgOpt);

	BOOL Clone(
		CMovementObj** ppMO_out,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int* pSpawnCfgOpt);

	// Return string representation of this for logging with optional reference model.
	std::string ToStr(CMovementObj* pMORM = NULL) const {
		std::string mormStr;
		if (pMORM) {
			StrBuild(mormStr, ":" << pMORM->m_name);
		}
		std::string str;
		StrBuild(str,
			"MO<" << m_networkDefinedID
				  << ":" << m_traceNumber
				  << mormStr
				  << ":" << m_name
				  << PassGroupStr()
				  << (m_allowArmDisarm ? "" : " ARM_DISARM_NOT_ALLOWED")
				  << ">");
		return str;
	}

	/// Movement Object State Enumeration
	enum STATE {
		STATE_UNCONFIGURED = 0, // unconfigured, cannot be armed or will become naked!
		STATE_CONFIGURED = 1, // configured via HandleMovementObjectCfgEvent(), can be armed safely!
		STATES
	};

	// Caches item glid metadata and inserts UGC items into global inventory.
	bool cacheItem(const GLID& glid);

	// Gets item from global inventory matching glid and actor group. [ALL, Male, Female, None]
	CItemObj* getItem(const GLID& glid);

	// This always returns 3 default clothing items. [Starter Shirt, Starter Pants, Starter Shoes]
	void getDefaultClothingItems(std::vector<CItemObj*>& defaultItems);

	///////////////////////////////////////////////////////////////////////
	// Arm/Disarm Functions  - CMovementObj_ArmDisarm.cpp
	// These functions require a Movement Object Reference Model which can
	// be gotten by GetMovementObjRefModel(pMO). If pMORM=NULL they assume
	// GetMovementObjRefModel(this) which is really all you ever want. If
	// this proves to be the case this parameter will be deprecated.
	///////////////////////////////////////////////////////////////////////

	// Returns number of armed items.
	size_t armedItems() const;

	// Returns vector of armed items.
	std::vector<CItemObj*> getArmedItems() const;

	// Logs all armed items.
	void logArmed() const;

	// Realizes all Arm/Disarm configuration changes to begin rendering new avatar.
	bool realizeDimConfig(CMovementObj* pMORM = NULL);

	// Return currently armed item excluded by given item or NULL if no exclusion.
	const CItemObj* getItemExcludesCurrentlyArmed(const CItemObj* pIO);

	// Arm Functions
	bool armDefaultItemsNotExcluded();
	bool armItem(CItemObj* pIO, bool realizeNow = true, bool armDefaults = true, CMovementObj* pMORM = NULL);
	bool armItem(const GLID& glid, bool realizeNow = true, bool armDefaults = true, CMovementObj* pMORM = NULL);
	bool armItems(const CCfgObjList* cfg, bool realizeNow = true, bool armDefaults = true, CMovementObj* pMORM = NULL);

	// Disarm Functions
	bool disarmItem(CItemObj* pIO, bool realizeNow = true, bool armDefaults = true, CMovementObj* pMORM = NULL, bool logOk = true);
	bool disarmItem(const GLID& glid, bool realizeNow = true, bool armDefaults = true, CMovementObj* pMORM = NULL);
	bool disarmAllItems(bool realizeNow = true, bool armDefaults = true, CMovementObj* pMORM = NULL);

	void removeWOKHair();
	void reapplyWOKHair();

	Vector3f getHeadPosition();

	// Skeletal mesh configurations
	bool configMeshSlot(RuntimeSkeleton* pSkeleton, SkeletonConfiguration* pSkeletonCfg, int slotId, int meshId, int matlId);
	BOOL fadeIn(TimeMs duration);
	BOOL mount(CMovementObj* target, int mountType = MT_RIDE);
	BOOL deMount();

	// ED-7832 - Player can stand when no longer moving or has been standing long enough (possible moving platform)
	Timer m_standTimer;
	bool canStand(bool isStand) {
		if (!isStand) {
			m_standTimer.Pause().Reset();
			return false;
		}

		if (InterpolatedPosSpeed() < 0.005)
			return true;

		if (m_standTimer.IsPaused()) {
			m_standTimer.Start();
			return false;
		}
		return (m_standTimer.ElapsedMs() > 300.0);
	}

	// ED-8586 - Slide Fixes
	bool m_animStandPending = false;
	void setAnimStandPending(bool enable) {
		m_animStandPending = enable;
	}
	bool getAnimStandPending() const {
		return m_animStandPending;
	}

	void characterAnimationManagement(RuntimeSkeleton* pRS, bool bPlayerSkeleton, TimeMs animBlendDurationMs = ANIM_BLEND_DURATION_MS);

	UINT queryLOD(FLOAT distance, DWORD& numLOD);
	void initPhysicsObjects();
	bool assetsLoaded();
	bool groupMatch(CMovementObj* pMO = NULL);

	void setControlType(eActorControlType val) {
		m_controlType = val;
	}
	eActorControlType getControlType() const {
		return m_controlType;
	}
	bool isTypeOPC() const {
		return (m_controlType == eActorControlType::OPC);
	}
	bool isTypePC() const {
		return (m_controlType == eActorControlType::PC);
	}
	bool isTypeNPC() const {
		return (m_controlType == eActorControlType::NPC);
	}
	bool isTypeStatic() const {
		return (m_controlType == eActorControlType::STATIC);
	}

	// Returns true for players (avatars & npcs).
	bool isTypePlayer() const {
		return (isTypeOPC() || isTypePC() || isTypeNPC());
	}

	// Player's Passes (OWN | MOD | GM | VIP | AP)
	bool hasPass(const PASS_GROUP& passGroup) const {
		return ((m_passGroups & (int)passGroup) != 0);
	}
	bool hasOwnerPass() const {
		return hasPass(PASS_GROUP_OWNER);
	}
	bool hasModeratorPass() const {
		return hasPass(PASS_GROUP_MODERATOR);
	}
	bool hasGMPass() const {
		return hasPass(PASS_GROUP_GM);
	}
	bool hasAccessPass() const {
		return hasPass(PASS_GROUP_AP);
	}
	bool hasVIPPass() const {
		return hasPass(PASS_GROUP_VIP);
	}

	void setPassGroups(int val) {
		m_passGroups = val;
	}

	std::string PassGroupStr() const {
		std::string passStr;
		StrBuild(passStr, ""
							  << (hasOwnerPass() ? " OWN" : "")
							  << (hasModeratorPass() ? " MOD" : "")
							  << (hasGMPass() ? " GM" : "")
							  << (hasAccessPass() ? " AP" : "")
							  << (hasVIPPass() ? " VIP" : ""));
		return passStr;
	}

	// My Player's Passes (OWN | MOD | GM | VIP | AP)
	static bool MyPassGroup(const PASS_GROUP& passGroup);
	static bool MyPassGroupOwner() {
		return MyPassGroup(PASS_GROUP_OWNER);
	}
	static bool MyPassGroupModerator() {
		return MyPassGroup(PASS_GROUP_MODERATOR);
	}
	static bool MyPassGroupGM() {
		return MyPassGroup(PASS_GROUP_GM);
	}
	static bool MyPassGroupAP() {
		return MyPassGroup(PASS_GROUP_AP);
	}
	static bool MyPassGroupVIP() {
		return MyPassGroup(PASS_GROUP_VIP);
	}

	// Returns if item is AP or not.
	static bool IsItemAP(const GLID& glid);

	void getMoveDataToClient(MOVE_DATA& moveData) const {
		moveData = m_moveDataToClient;
	}
	void setMoveDataToClient(const MOVE_DATA& moveData) {
		m_moveDataToClient = moveData;
	}

	void setCurrentCollision(std::vector<int> aIdNewCollision, const std::function<void(int iId, bool bNewState)>& changeCallback = nullptr);

	void setOutlineState(bool outline) {
		m_outline = outline;
	}
	bool getOutlineState() const {
		return m_outline;
	}
	void setOutlineColor(const D3DCOLORVALUE& color) {
		m_outlineColor = color;
	}
	D3DCOLORVALUE getOutlineColor() const {
		return m_outlineColor;
	}
	void setOutlineZSorted(bool sort) {
		m_outlineZSorted = sort;
	}
	bool isOutlineZSorted() const {
		return m_outlineZSorted;
	}

	void setOverrideAnimationByType(eAnimType animType, int animVer = ANIM_VER_ANY, bool bLockAnimation = false);

	void setOverrideAnimationSettings(const AnimSettings& animSettings, bool bLockAnimation = false);
	AnimSettings getOverrideAnimationSettings() const {
		return m_overrideAnimSettings;
	}

	void resetOverrideAnimType();

	bool hasSubstitute() const {
		return m_substituteObjType != ObjectType::NONE;
	}
	bool getSubstitute(ObjectType& type, int& id) const;
	void setSubstitute(ObjectType type, int id);
	bool isSubstituteAllowed() const {
		return m_substituteAllowed;
	}
	void setSubstituteAllowed(bool bAllowed) {
		m_substituteAllowed = bAllowed;
	}

	bool isVisible() const {
		return m_visible;
	}
	void setVisible(bool bVisible) {
		m_visible = bVisible;
	}

	void clearLookAt();
	void setLookAt(ObjectType targetType, int targetId, const std::string& targetBone, const Vector3f& offset, bool followTarget);
	bool getLookAt(ObjectType& targetType, int& targetId, std::string& targetBone, Vector3f& offset, bool& followTarget) const;
	bool isLookingAtTarget() const {
		return m_lookAtInfo != nullptr;
	}

	struct CameraData {
		int m_cameraDBIndex;
		unsigned m_runtimeCameraId;
		int m_boneIndex;
		CameraData(int dbIndex = 0, unsigned cameraId = 0, int boneId = 0) :
				m_cameraDBIndex(dbIndex),
				m_runtimeCameraId(cameraId),
				m_boneIndex(boneId) {}

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	};

	struct SurfaceInfo {
		ObjectType objectType;
		int objectId;
		Vector3f contactLocation;
		Vector3f faceNormal;
		Matrix44f transform; // Transform of surface. This should make contactLocation obsolete.
		Matrix44f relativeTransform; // Relative position of avatar on surface.
	};
	SurfaceInfo m_recentSurfaceInfo;

	void standVerticalToSurface();

	STATE getState() const {
		return m_state;
	}
	void setState(STATE val) {
		m_state = val;
	}

	const std::string& getName() const {
		return m_name;
	}
	void setName(const std::string& name) {
		m_name = name;
	}

	const std::string& getDisplayName() const {
		return m_displayName;
	}
	void setDisplayName(const std::string& val) {
		m_displayName = val;
	}

	const std::string& getClanName() const {
		return m_clanName;
	}
	void setClanName(const std::string& val) {
		m_clanName = val;
	}

	const std::string& getTitle() const {
		return m_title;
	}
	void setTitle(const std::string& val) {
		m_title = val;
	}

	const int* getActorGroups() const {
		return m_actorGroups;
	}
	int getActorGroupByIndex(size_t index) {
		return m_actorGroups[index];
	}

	bool hasSkeleton() const {
		return m_pSkeleton != nullptr;
	}
	const RuntimeSkeleton* getSkeleton() const {
		return m_pSkeleton.get();
	}
	RuntimeSkeleton* getSkeleton() {
		return m_pSkeleton.get();
	}
	void setSkeleton(std::shared_ptr<RuntimeSkeleton> pSkeleton) {
		m_pSkeleton = pSkeleton;
	}

	const SkeletonConfiguration* getSkeletonConfig() const {
		return m_skeletonConfiguration;
	}
	SkeletonConfiguration* getSkeletonConfig() {
		return m_skeletonConfiguration;
	}

	const CAlterUnitDBObjectList* getConfigurableMeshSlots() const;
	CAlterUnitDBObjectList* getConfigurableMeshSlots();

	size_t getTotalConfigSlotCount() const; // Total number of configurable slots - number of avatar (base) configurable slots + number of armed items + number of pending armed items

	SERVER_ITEM_TYPE getServerItemType() const {
		return m_serverItemType;
	}
	void setServerItemType(SERVER_ITEM_TYPE val) {
		m_serverItemType = val;
	}
	GLID getUGCActorGlobalId() const {
		return m_glid;
	}
	bool getAllowArmDisarm() const {
		return m_allowArmDisarm;
	}
	void setAllowArmDisarm(bool val) {
		m_allowArmDisarm = val;
	}

	long getNetTraceId() const {
		return m_networkDefinedID;
	}
	void setNetTraceId(long val);

	bool isNetworkEntity() const {
		return m_networkEntity;
	}
	void setNetworkEntity(bool val) {
		m_networkEntity = val;
	}

	MovementInterpolator m_movementInterpolator;

	void MovementInterpolatorInit();

	void MovementInterpolatorUpdate(const MovementData& md) {
		m_movementInterpolator.Update(md);
	}

	MovementData InterpolatedMovementData() {
		return m_movementInterpolator.InterpolatedMovementData();
	}

	Vector3f InterpolatedPos() const {
		return m_movementInterpolator.InterpolatedPos();
	}

	Vector3f InterpolatedDir() const {
		return m_movementInterpolator.InterpolatedDir();
	}

	Vector3f InterpolatedUp() const {
		return m_movementInterpolator.InterpolatedUp();
	}

	float InterpolatedRotDeg() const {
		return m_movementInterpolator.InterpolatedRotDeg();
	}

	float InterpolatedPosSpeed() const {
		return m_movementInterpolator.InterpolatedPosSpeed();
	}

	CFrameObj* getBaseFrame() const {
		return m_baseFrame;
	}
	void setBaseFrame(CFrameObj* pFO) {
		m_baseFrame = pFO;
	}

	void addChildFrame(CFrameObj* child);

	const Matrix44f& getBaseFrameMatrix() const;
	void setBaseFrameMatrix(const Matrix44f& matrix, bool cascade);

	void setBaseFramePosition(const Vector3f& pos, bool cascade);
	const Vector3f& getBaseFramePosition() const {
		return getBaseFrameMatrix().GetTranslation();
	}

	void setBaseFrameOrientationCascade(const Vector3f& dir, const Vector3f& up);

	void setBaseFramePosDirUp(const Vector3f& pos, const Vector3f& dir, const Vector3f& up);
	void getBaseFramePosDirUp(Vector3f& pos, Vector3f& dir, Vector3f& up) const;

	void transformBaseFrameCascade(const Matrix44f& matrix);

	const Vector3f& getLastPosition() const {
		return m_lastPosition;
	}
	void setLastPosition(const Vector3f& val) {
		m_lastPosition = val;
	}
	void saveLastPositionFromBaseFrame(bool alsoUpdateMovementCaps = true);

	const CMovementCaps* getMovementCaps() const {
		return m_movementDB;
	}
	CMovementCaps* getMovementCaps() {
		return m_movementDB;
	}
	void setMovementCaps(CMovementCaps* pMC) {
		m_movementDB = pMC;
	}
	void deleteMovementCaps();

	bool isFirstPersonModel() const {
		return m_firstPersonModel;
	}

	int getModelType() const {
		return m_modelType;
	}
	void setModelType(int val) {
		m_modelType = val;
	}

	int getControlConfig() const {
		return m_controlConfig;
	}
	void setControlConfig(int val) {
		m_controlConfig = val;
	}

	int getIdxRefModel() const {
		return m_idxRefModel;
	}
	void setIdxRefModel(int val) {
		m_idxRefModel = val;
	}

	float getBoundingRadius() const {
		return m_boundingRadius;
	}
	void setBoundingRadius(float val) {
		m_boundingRadius = val;
	}

	float getUnscaledBoundingRadius() const {
		return m_unscaledBoundingRadius;
	}
	void setUnscaledBoundingRadius(float val) {
		m_unscaledBoundingRadius = val;
	}

	int getTraceNumber() const {
		return m_traceNumber;
	}
	void setTraceNumber(int val) {
		m_traceNumber = val;
	}

	const CSkeletalLODObjectList* getSkeletonLODList() const {
		return m_skeletonLODList;
	}

	const std::shared_ptr<Physics::RigidBodyStatic>& getVisiblePhysicsBody() const {
		return m_pVisibleBody;
	}
	const std::shared_ptr<Physics::Avatar>& getPhysicsAvatar() const {
		return m_pPhysicsAvatar;
	}
	const std::shared_ptr<Physics::Vehicle>& getPhysicsVehicle() const {
		return m_pPhysicsVehicle;
	}
	void setPhysicsVehicle(const std::shared_ptr<Physics::Vehicle>& val) {
		m_pPhysicsVehicle = val;
	}
	void resetPhysicsVehicle() {
		m_pPhysicsVehicle.reset();
	}
	const std::shared_ptr<Physics::RigidBodyStatic>& getVehicleRigidBody() const {
		return m_pOtherPlayerVehicleRigidBody;
	}
	void setVehicleRigidBody(const std::shared_ptr<Physics::RigidBodyStatic>& val) {
		m_pOtherPlayerVehicleRigidBody = val;
	}
	void resetVehicleRigidBody() {
		m_pOtherPlayerVehicleRigidBody.reset();
	}

	int getVehiclePlacementId() const {
		return m_iVehiclePlacementId;
	}
	void setVehiclePlacementId(int val) {
		m_iVehiclePlacementId = val;
	}

	const Matrix44f& getMtxDynamicObjectToMovementObject() const {
		return m_mtxDynamicObjectToMovementObject;
	}
	void setMtxDynamicObjectToMovementObject(const Matrix44f& val) {
		m_mtxDynamicObjectToMovementObject = val;
	}

	const Matrix44f& getMtxPhysicsVehicleToDynamicObject() const {
		return m_mtxPhysicsVehicleToDynamicObject;
	}
	void setMtxPhysicsVehicleToDynamicObject(const Matrix44f& val) {
		m_mtxPhysicsVehicleToDynamicObject = val;
	}

	// DRF - DEAD CODE - TriggerObject::TA_ACTIVATE_FORCE_PAD
	const Vector3f& getPosForceExt() const {
		return m_posForceExt;
	}
	void setPosForceExt(const Vector3f& val) {
		m_posForceExt = val;
	}

	bool isFirstPersonMode() const {
		return m_firstPersonMode;
	}
	bool setFirstPersonMode(bool val) {
		m_firstPersonMode = val;
		return true;
	}

	int getRidableBoneIndex() const {
		return m_ridable;
	}

	const CMovementObj* getPosses() const {
		return m_pMO_posses;
	}
	CMovementObj* getPosses() {
		return m_pMO_posses;
	}

	const CMovementObj* getPossedBy() const {
		return m_pMO_possedBy;
	}
	CMovementObj* getPossedBy() {
		return m_pMO_possedBy;
	}

	const CItemObjList* getCurrentlyArmedItems() const {
		return m_currentlyArmedItems;
	}

	const CCfgObjList* getAutoCfg() const {
		return m_autoCfg;
	}

	const CSkillObjectList* getSkills() const {
		return m_skills;
	}

	bool isValidThisRound() const {
		return m_validThisRound;
	}
	void setValidThisRound(bool val) {
		m_validThisRound = val;
	}

	const EffectTrack& getEffectTrack() const {
		return m_effectTrack;
	}
	EffectTrack& getEffectTrack() {
		return m_effectTrack;
	}
	void updateEffectTrack(TimeMs time) {
		m_effectTrack.update(time);
	}

	eAnimType getOverrideAnimType() const {
		return m_overrideAnimType;
	}
	void setOverrideAnimType(eAnimType val) {
		m_overrideAnimType = val;
	}

	void setOverrideAnimSettings(const AnimSettings& animSettings) {
		m_overrideAnimSettings = animSettings;
	}

	TimeMs getOverrideTimeMs() const {
		return m_overrideTimeMs;
	}
	void setOverrideTimeMs(TimeMs val) {
		m_overrideTimeMs = val;
	}

	TimeMs getOverrideDuration() const {
		return m_overrideDurationMs;
	}
	void setOverrideDuration(TimeMs val) {
		m_overrideDurationMs = val;
	}

	BOOL canFilterCollision() const {
		return m_filterCollision;
	}

	float getDistanceToCamTemp() const {
		return m_distanceToCamTemp;
	}
	void setDistanceToCamTemp(float val) {
		m_distanceToCamTemp = val;
	}

	size_t getCameraCount() const {
		return m_cameraData.size();
	}

	const CameraData& getCameraData(size_t index) const;
	void setRuntimeCameraId(size_t index, int runtimeCameraId);
	const CameraData& getCurrentCameraData() const;
	int getCurrentCameraDataIndex() const {
		return m_currentCameraDataIndex;
	}
	void setCurrentCameraDataIndex(int val) {
		m_currentCameraDataIndex = val;
	}

	bool isInFov() const {
		return m_isInFov;
	}
	void setInFov(bool val) {
		m_isInFov = val;
	}

	IModel* getModel() const {
		return m_iModel;
	}
	void setModel(IModel* val) {
		m_iModel = val;
	}

	D3DCOLORVALUE getNameColor() const {
		return m_nameColor;
	}
	void setNameColor(float r, float g, float b) {
		m_nameColor.r = r;
		m_nameColor.g = g;
		m_nameColor.b = b;
	}

	int getMountType() const {
		return m_mountType;
	}
	void setMountType(int val) {
		m_mountType = val;
	}

	bool getRender() const {
		return m_render;
	}
	void setRender(bool val) {
		m_render = val;
	}

	const UGCActor* getUgcActor() const {
		return m_ugcActor;
	}
	void setUgcActor(UGCActor* val) {
		m_ugcActor = val;
	}

	bool getTouchTriggered() const {
		return m_touchTriggered;
	}
	void setTouchTriggered(bool val) {
		m_touchTriggered = val;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	void TransformChanged();

	long m_networkDefinedID;

	int m_traceNumber;

	GLID m_glid;

	STATE m_state;

	bool m_visible;
	bool m_render;

	std::string m_name;
	std::string m_displayName;
	std::string m_clanName;
	std::string m_title;

	D3DCOLORVALUE m_nameColor;

	int m_actorGroups[4];

	CMovementObj* m_pMO_posses;
	CMovementObj* m_pMO_possedBy;
	int m_possedBy_traceNumber;
	int m_mountType; // MT_NONE, MT_RIDE: std mount, MT_STUNT: temp actor replacement
	int m_ridable;

	std::shared_ptr<RuntimeSkeleton> m_pSkeleton;

	SkeletonConfiguration* m_skeletonConfiguration;

	CFrameObj* m_baseFrame;

	CMovementCaps* m_movementDB;

	SERVER_ITEM_TYPE m_serverItemType; // only set for TYPE_STATIC

	eActorControlType m_controlType;

	bool m_allowArmDisarm;

	int m_passGroups; // bitmapped PASS_GROUP

	MOVE_DATA m_moveDataToClient; // DRF - Move Data To Client From Server

	int m_currentLOD;
	CSkeletalLODObjectList* m_skeletonLODList;

	// networkEntities must receive movement updates (ENTITY_POS_TO_CLIENT_CAT)
	// otherwise they are immediately deleted!
	bool m_networkEntity;
	bool m_validThisRound;

	IModel* m_iModel;

	int m_modelType;

	int m_idxRefModel; // movObjRefModelList Index

	int m_controlConfig;

	float m_boundingRadius;
	float m_unscaledBoundingRadius;

	Vector3f m_lastPosition;

	LookAtInfo* m_lookAtInfo;

	std::shared_ptr<Physics::Avatar> m_pPhysicsAvatar;
	std::shared_ptr<Physics::Vehicle> m_pPhysicsVehicle; // Set only for player controlled vehicles.
	std::shared_ptr<Physics::RigidBodyStatic> m_pOtherPlayerVehicleRigidBody; // Set only for vehicles controlled by other players
	std::shared_ptr<Physics::RigidBodyStatic> m_pVisibleBody; // Used for picking and camera collision.
	int m_iVehiclePlacementId;
	Matrix44f m_mtxDynamicObjectToMovementObject;
	Matrix44f m_mtxPhysicsVehicleToDynamicObject;

	std::vector<int> m_aIdCurrentCollision;

	Vector3f m_posForceExt; // drf - dead code - TriggerObject::TA_ACTIVATE_FORCE_PAD

	bool m_firstPersonMode;
	bool m_firstPersonModel;

	CInventoryObjList* m_inventory;

	CItemObjList* m_currentlyArmedItems;
	CCfgObjList* m_autoCfg;
	CCfgObjList* m_deathCfg;
	CSkillObjectList* m_skills;
	CStatObjList* m_stats;
	UGCActor* m_ugcActor;

	EffectTrack m_effectTrack;

	eAnimType m_overrideAnimType;
	AnimSettings m_overrideAnimSettings;
	TimeMs m_overrideTimeMs;
	TimeMs m_overrideDurationMs;
	bool m_overrideAnimLock;

	std::vector<CameraData> m_cameraData;
	int m_currentCameraDataIndex; // set at runtime (initially -1)

	BOOL m_filterCollision;
	float m_distanceToCamTemp;
	bool m_isInFov; // true if model is visiable and in current fov, used for targeting

	bool m_outline; // whether or not to draw glowy outline
	D3DCOLORVALUE m_outlineColor;
	bool m_outlineZSorted; // true if the glowing outline is depth tested

	ObjectType m_substituteObjType; // If !assetsLoaded and a substitute is available, the type (e.g. DYNAMIC)
	int m_substituteObjId; // and object ID (e.g. placementId) of the substitute
	bool m_substituteAllowed; // true if do not use substitute

	bool m_touchTriggered; // my avatar is touching this avatar

	DECLARE_GETSET
};

class CMovementObjList : public CKEPObList {
public:
	CMovementObjList() :
			m_nextFreeTraceNumber(0), m_saveWholeEDB(true){};

	int getNextFreeTraceNumber();

	CMovementObj* getObjByTraceNumber(int trace) const;
	CMovementObj* getObjByNetTraceId(int id) const;
	CMovementObj* getObjByIndex(int index) const;
	CMovementObj* getObjTypeStaticByNetTraceId(int id) const;
	CMovementObj* getObjByName(CStringA name) const;

	void Serialize(CArchive& ar);

private:
	void iterateReMeshes(std::function<void(const CDeformableMesh*)> func);
	int countReMeshes();
	int countCoreMeshes();
	ReSkinMesh** gatherSkinMeshesWithDup();
	ReSkinMesh** gatherCoreMeshesWithDup();

private:
	bool m_saveWholeEDB;
	short m_nextFreeTraceNumber; // short since once place that passed to client as short in ATTRIB_STRUCT

	DECLARE_SERIAL_SCHEMA(CMovementObjList, VERSIONABLE_SCHEMA | 2) // 1 uses StoreRemeshes instead of LodChain --Jonny
};

} // namespace KEP