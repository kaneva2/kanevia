///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENTITYITEMTESTDLG_H__BE799E61_E684_11D4_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_ENTITYITEMTESTDLG_H__BE799E61_E684_11D4_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EntityItemTestDlg.h : header file
//
#include "CItemClass.h"
#include "CMovementObj.h"
#include "Common\UIStatic\BasicLayoutMFC.h"
/////////////////////////////////////////////////////////////////////////////
// CEntityItemTestDlg dialog

class CEntityItemTestDlg : public CDialog {
public:
	//////////////////
	// Constructors //
	//////////////////

	CEntityItemTestDlg(UINT OKbtnTextStringId = 0, UINT closeBtnTextStringId = 0, CWnd* pParent = NULL); // standard constructor

	// Dialog Data
	//{{AFX_DATA(CEntityItemTestDlg)
	enum { IDD = IDD_DIALOG_ITEMTEST };
	CListBox m_itemList;
	//}}AFX_DATA

	/////////////////////////////
	// Public Member Variables //
	/////////////////////////////

	UINT m_OKBtnTextStringId, m_closeBtnTextStringId;
	CItemObjList* m_currentlyArmedListRef;
	CMovementObj* m_actor;
	CMovementObjList* m_entities; // This now needs access to all entities to equip items for other actors.
	int m_selGLID;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEntityItemTestDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//////////////////////////
	// Message Map Handlers //
	//////////////////////////

	// Generated message map functions
	//{{AFX_MSG(CEntityItemTestDlg)
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* pMMI);
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	////////////////////////////////
	// Protected Member Variables //
	////////////////////////////////

	bool m_needInit;
	CRect m_clientRect;
	int m_minClientX;
	int m_minClientY;

	BasicLayoutMFC m_layout;
}; // class..CEntityItemTestDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENTITYITEMTESTDLG_H__BE799E61_E684_11D4_B1EE_0000B4BD56DD__INCLUDED_)
