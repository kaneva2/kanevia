/******************************************************************************
 ControlsDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_CONTROLS_DLG_H_)
#define _CONTROLS_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ControlsDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "EffectSettingsPropertiesCtrl.h"

////////////////////////////////////////////
// CControlsDialog control

class CControlsDialog : public CDialogBar {
	DECLARE_DYNAMIC(CControlsDialog)

public:
	CControlsDialog();
	virtual ~CControlsDialog();

	// Dialog Data
	//{{AFX_DATA(CControlsDialog)
	enum { IDD = IDD_DIALOG_CONTROLERS };
	//}}AFX_DATA

	// accessors
	int GetForceDirectionY() const {
		return m_propEffectSettings.GetForceDirectionX();
	}
	int GetForceDirectionX() const {
		return m_propEffectSettings.GetForceDirectionY();
	}

	// modifiers
	void SetForceDirectionX(int forceDirectionX) {
		m_propEffectSettings.SetForceDirectionX(forceDirectionX);
	}
	void SetForceDirectionY(int forceDirectionY) {
		m_propEffectSettings.SetForceDirectionY(forceDirectionY);
	}

protected:
	// Generated message map functions
	//{{AFX_MSG(CControlsDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colAvailableJoysticks;
	CButton m_grpAvailableJoysticks;

	CCollapsibleGroup m_colEffectSettings;
	CButton m_grpEffectSettings;

	CCollapsibleGroup m_colControlObjects;
	CButton m_grpControlObjects;

	CCollapsibleGroup m_colControlEvents;
	CButton m_grpControlEvents;

	CEffectSettingsPropertiesCtrl m_propEffectSettings;

	CKEPImageButton m_btnTestEffect;
	CKEPImageButton m_btnStopEffect;

	CKEPImageButton m_btnAddObject;
	CKEPImageButton m_btnDeleteObject;
	CKEPImageButton m_btnEditObject;

	CKEPImageButton m_btnAddEvent;
	CKEPImageButton m_btnDeleteEvent;
	CKEPImageButton m_btnEditEvent;
};

#endif !defined(_CONTROLS_DLG_H_)