/******************************************************************************
 FogPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_FOGPROPERTIESCTRL_H_)
#define _FOGPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FogPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "colordlg.h"
#include "GWCPropertyIconLook.h"

/////////////////////////////////////////////////////////////////////////////
// CFogPropertiesCtrl window

class CFogPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CFogPropertiesCtrl();
	virtual ~CFogPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual bool OnPropertyButtonClicked(GWCDeepIterator iter);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetStartRange();
	float GetEndRange();
	float GetDensity();
	float GetFogColorRed() const {
		return m_fogColorRed;
	}
	float GetFogColorGreen() const {
		return m_fogColorGreen;
	}
	float GetFogColorBlue() const {
		return m_fogColorBlue;
	}
	BOOL GetUseFog() const {
		return m_useFog;
	}
	float GetBackgroundColorRed() const {
		return m_backgroundColorRed;
	}
	float GetBackgroundColorGreen() const {
		return m_backgroundColorGreen;
	}
	float GetBackgroundColorBlue() const {
		return m_backgroundColorBlue;
	}

	// modifiers
	void SetStartRange(float startRange);
	void SetEndRange(float endRange);
	void SetDensity(float density);
	void SetFogColorRed(float fogColorRed) {
		m_fogColorRed = fogColorRed;
	}
	void SetFogColorGreen(float fogColorGreen) {
		m_fogColorGreen = fogColorGreen;
	}
	void SetFogColorBlue(float fogColorBlue) {
		m_fogColorBlue = fogColorBlue;
	}
	void SetUseFog(int useFog) {
		m_useFog = useFog;
	}
	void SetBackgroundColorRed(float backgroundColorRed) {
		m_backgroundColorRed = backgroundColorRed;
	}
	void SetBackgroundColorGreen(float backgroundColorGreen) {
		m_backgroundColorGreen = backgroundColorGreen;
	}
	void SetBackgroundColorBlue(float backgroundColorBlue) {
		m_backgroundColorBlue = backgroundColorBlue;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFogPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CFogPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	//indices
	enum { INDEX_ROOT = 1,
		INDEX_USE_FOG,
		INDEX_FOG_COLOR,
		INDEX_BACK_COLOR,
		INDEX_MATCH_BACK_COLOR,
		INDEX_START_RANGE,
		INDEX_END_RANGE,
		INDEX_DENSITY
	};

	const static int START_RANGE_MIN = 0;
	const static int START_RANGE_MAX = 30000;
	const static int END_RANGE_MIN = 0;
	const static int END_RANGE_MAX = 30000;

	void OpenFogColorDlg();
	void OpenBackgroundColorDlg();

private:
	float m_fogColorRed;
	float m_fogColorGreen;
	float m_fogColorBlue;
	BOOL m_useFog;

	float m_backgroundColorRed;
	float m_backgroundColorGreen;
	float m_backgroundColorBlue;

	int m_startRange;
	int m_endRange;
	int m_density;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_FOGPROPERTIESCTRL_H_)
