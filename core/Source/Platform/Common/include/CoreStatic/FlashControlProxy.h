///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <functional>
#include <memory>

namespace KEP {

class IFlashControlProxy;
enum class FlashScriptAccessMode;

// Holds the bitmap returned from the flash control as well as some related metadata.
// Ensures proper destruction of the contained bitmap object.
class FlashWindowBitmap {
public:
	FlashWindowBitmap() :
			m_hbm(0), m_bOwnBitmap(false) {}
	FlashWindowBitmap(FlashWindowBitmap&& other) :
			m_hbm(0) {
		*this = std::move(other);
	}
	~FlashWindowBitmap() {
		Clear();
	}
	void Clear() {
		if (m_hbm) {
			if (m_bOwnBitmap)
				::DeleteObject(m_hbm);
			m_hbm = NULL;
		}
	}
	HBITMAP GetHBitmap() const {
		return m_hbm;
	}
	int GetWidth() const {
		return m_iWidth;
	}
	int GetHeight() const {
		return m_iHeight;
	}
	void SetBitmap(HBITMAP hbm, bool bTakeOwnership) {
		Clear();
		m_hbm = hbm;
		if (m_hbm) {
			BITMAP bitmap;
			GetObject(m_hbm, sizeof(bitmap), &bitmap);
			m_iWidth = (uint32_t)bitmap.bmWidth;
			m_iHeight = (uint32_t)bitmap.bmHeight;
			m_bOwnBitmap = bTakeOwnership;
		} else {
			m_iWidth = 0;
			m_iHeight = 0;
			m_bOwnBitmap = false;
		}
	}
	FlashWindowBitmap& operator=(FlashWindowBitmap&& other) {
		Clear();
		m_hbm = other.m_hbm;
		m_iWidth = other.m_iWidth;
		m_iHeight = other.m_iHeight;
		m_bOwnBitmap = other.m_bOwnBitmap;
		other.m_hbm = NULL;
		return *this;
	}

private:
	bool m_bOwnBitmap;
	HBITMAP m_hbm;
	uint32_t m_iWidth;
	uint32_t m_iHeight;
};

class IFlashSystem {
public:
	virtual bool IsFlashInstalled() = 0;
	virtual std::string GetFlashVersion() = 0;
	virtual void SetGlobalVolume(float fVolume, bool bMute) = 0;
	virtual std::shared_ptr<IFlashControlProxy> CreateFlashControlProxy() = 0;
	virtual size_t GetFlashProcessCrashCount() = 0;
	virtual size_t GetPeakFlashInstanceCount() = 0;
	virtual void CrashFlashProcess() = 0;

	static std::shared_ptr<IFlashSystem> New(const std::function<void(float fNewVolume, bool bNewMute)>& volumeChangeCallback = nullptr);
};

class IFlashControlProxy {
public:
	virtual size_t GetMeshId() const = 0;

	struct InitializeArgs {
		HWND m_hwndParent = NULL;
		bool m_bWantInput = false;
		std::string m_strParams;
		std::string m_strURL;
		FlashScriptAccessMode m_eAllowScriptAccess;
		float m_fLocalVolume = 0;
		int32_t m_iPositionX = 0;
		int32_t m_iPositionY = 0;
		uint32_t m_iWidth = 0;
		uint32_t m_iHeight = 0;
		uint32_t m_iCropTop = 0;
		uint32_t m_iCropLeft = 0;
		uint32_t m_iCropBottom = 0;
		uint32_t m_iCropRight = 0;
	};
	virtual bool Initialize(const InitializeArgs& args) = 0;

	virtual bool StopProcessing() = 0;
	virtual std::string GetIdString(bool bVerbose = false) const = 0;
	virtual bool GetAvailableBitmap(void** ppBitmapData, uint32_t* bitmapWidth, uint32_t* bitmapHeight) = 0;

	virtual bool Rewind() = 0;
	virtual bool SetMediaParams(const std::string& strParams, const std::string& strURL, FlashScriptAccessMode eAllowScriptAccess) = 0;
	virtual bool UpdateRect(int32_t x, int32_t y, uint32_t width, uint32_t height, uint32_t iCropTop, uint32_t iCropLeft, uint32_t iCropBottom, uint32_t iCropRight) = 0;
	virtual bool UpdateBitmap() = 0;
	virtual bool SetLocalVolume(float fLocalVolume) = 0;
	virtual bool SetFocus() = 0;
};

} // namespace KEP