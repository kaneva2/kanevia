/******************************************************************************
 SpawnGeneratorDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SPAWN_GENERATOR_DLG_H_)
#define _SPAWN_GENERATOR_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpawnGeneratorDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
//#include "SpawnGeneratorPropertiesCtrl.h"

////////////////////////////////////////////
// CSpawnGeneratorDialog control

class CSpawnGeneratorDialog : public CDialogBar {
	DECLARE_DYNAMIC(CSpawnGeneratorDialog)

public:
	CSpawnGeneratorDialog();
	virtual ~CSpawnGeneratorDialog();

	// Dialog Data
	//{{AFX_DATA(CSpawnGeneratorDialog)
	enum { IDD = IDD_DIALOG_SPAWNGENERATORUI };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CSpawnGeneratorDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colSpawnGenerator;
	CButton m_grpSpawnGenerator;
	//	CSpawnGeneratorPropertiesCtrl	m_propSpawnGenerator;

	CKEPImageButton m_btnAddSpawnGenerator;
	CKEPImageButton m_btnDeleteSpawnGenerator;
	CKEPImageButton m_btnEditSpawnGenerator;
};

#endif !defined(_SPAWN_GENERATOR_DLG_H_)