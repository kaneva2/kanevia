///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"

#include "GetSet.h"
#include "SoundRM.h"
#include "KEPConstants.h"
#include <d3dx9math.h>
#include <al.h>
#include <KEPPhysics/Declarations.h>

namespace KEP {

class ReMesh;
class OALSoundBuffer;
class CMaterialObject;

enum PLAYBACK_MODE {
	PM_SCRIPT, // Controlled by script
	PM_ALWAYS_ON, // Start playback once sound is placed and sound data are available (incl. placement during zone customization)
	PM_TRIGGERED_ON, // Start playback once player enters the sphere
	PM_TRIGGERED_ONOFF, // Start playback once player enters the sphere, and stop at exit
	PLAYBACK_MODES
};

class SoundAttachment {
	ObjectType m_targetType; // enumerated target type
	long m_targetId; // target identifier (netId, objId, ...)
	bool m_targetFollow; // sound follows target

public:
	SoundAttachment() :
			m_targetType(ObjectType::NONE),
			m_targetId(0),
			m_targetFollow(false) {
	}

	ObjectType AttachedTargetType() const {
		return m_targetType;
	}

	bool IsAttached() const {
		return (AttachedTargetType() != ObjectType::NONE);
	}
	bool IsAttachedToPlayer() const {
		return (AttachedTargetType() == ObjectType::MOVEMENT);
	}
	bool IsAttachedToDynObj() const {
		return (AttachedTargetType() == ObjectType::DYNAMIC);
	}

	bool IsAttachedToPlayer(long playerId) const {
		return IsAttachedToPlayer() && (playerId == AttachedTargetId());
	}
	bool IsAttachedToDynObj(long placementId) const {
		return IsAttachedToDynObj() && (placementId == AttachedTargetId());
	}

	bool AttachToPlayer(long netId, bool follow) {
		if (netId <= 0)
			return false;
		m_targetType = ObjectType::MOVEMENT;
		m_targetId = netId;
		m_targetFollow = follow;
		VisibilityChanged();
		return true;
	}

	bool AttachToDynObj(long objId, bool follow) {
		if (objId <= 0)
			return false;
		m_targetType = ObjectType::DYNAMIC;
		m_targetId = objId;
		m_targetFollow = follow;
		VisibilityChanged();
		return true;
	}

	long AttachedTargetId() const {
		return m_targetId;
	}
	bool SetAttachedTargetId(long targetId) {
		m_targetId = targetId;
		return true;
	}

	bool AttachedTargetFollow() const {
		return m_targetFollow;
	}
	bool SetAttachedTargetFollow(bool follow) {
		m_targetFollow = follow;
		return true;
	}

	virtual void VisibilityChanged() = 0;
};

class SoundPlacement : public GetSet, public SoundAttachment {
	int m_id; // attached placement id

	GLID m_glid;

	std::string m_name;

	bool m_quiet = false;

	bool m_inited;

	bool m_playing; // sound is currently playing
	bool Playing() const {
		return m_playing;
	}
	bool SetPlaying(bool playing) {
		m_playing = playing;
		return true;
	}

	PLAYBACK_MODE m_playbackMode;

	bool m_scriptPlay; // script commanded play

	ALuint m_source; // OpenAL source identifier

	/// Sound Placement Attributes (set to OpenAL)
	struct SoundAttr {
		bool relative; // sound relative to listener
		bool looping; // sound loops
		TimeDelay loopDelay; // sound loop delay (timeMs)
		double gain; // sound volume (0.0 -> 1.0)
		double pitch; // sound pitch (0.0 -> 1.0)
		double refDist; // sound reference distance
		double maxDist; // sound max audible distance
		double rolloff; // sound rolloff (0.0 -> 1.0)
		double outerGain; // sound outer gain (0.0 -> 1.0)
		double coneOuterAngle; // sound emitter outer angle (radians)
		double coneInnerAngle; // sound emitter inner angle (radians)
		Vector3f pos; // sound position
		Vector3f dir; // sound direction

		SoundAttr() {
			Reset();
		}
		void Reset();
	};
	SoundAttr m_soundAttr; // sound attributes
	SoundAttr m_soundAttrSource; // sound attributes last set on source

	Matrix44f m_locationMat;
	Matrix44f m_rangeMat;

	bool m_outline;
	D3DXCOLOR m_outlineColor;
	bool m_outlineZSorted; // true if the glowing outline is depth tested

	OALSoundProxy* m_proxy;

	ReMesh* m_soundMesh;
	ReMesh* m_rangeMesh;
	ReMesh* m_coneMesh;
	ReMesh* m_innerAngleRing;
	ReMesh* m_outerAngleRing;

	CMaterialObject* m_material;
	CMaterialObject* m_rangeMaterial;
	CMaterialObject* m_coneMaterial;
	CMaterialObject* m_outerMaterial;
	CMaterialObject* m_innerMaterial;

	std::shared_ptr<Physics::RigidBodyStatic> m_pVisibleBody;

	// DRF - Recalculates audible range matrix.
	// NOTE: Must be called whenever Position() or MaxDist() changes!
	void UpdateRangeMatrix();

	// DRF - Recalculates cone angle ring meshes.
	// NOTE: Must be called whenever ConeInnerAngle() or ConeOuterAngle() or MaxDist() changes!
	void UpdateConeAngleRingMeshes();

	void PhysicsInit();
	virtual void VisibilityChanged() override;

public:
	static bool InitOpenAL();

	static bool UpdateListener(const Vector3f& pos, const Vector3f& rot, const Vector3f& vel);

	SoundPlacement(int pid, const Vector3f& pos);

	~SoundPlacement();

	std::string ToStr() const {
		std::string str;
		std::string attach;
		if (IsAttachedToPlayer()) {
			StrBuild(attach, " ATTACH_PLAYER<" << AttachedTargetId() << ">");
		} else if (IsAttachedToDynObj()) {
			StrBuild(attach, " ATTACH_DO<" << AttachedTargetId() << ">");
		}
		StrBuild(str, "SP<" << GetId() << ":" << GetGlid() << " '" << GetName() << "'" << attach << ">");
		return str;
	}

	bool SetQuiet(bool quiet) {
		m_quiet = quiet;
		return true;
	}
	bool GetQuiet() const {
		return m_quiet;
	}

	int GetId() const {
		return m_id;
	}

	GLID GetGlid() const {
		return m_glid;
	}

	void SetName(const std::string& name) {
		m_name = name;
	}
	std::string GetName() const {
		return m_name;
	}

	bool MaterialAndMeshInit();

	bool SourceInit();
	bool SourceNew();
	bool SourceDel(bool delProxy);
	bool SourceOk() const;

	bool SourceSet(ALenum attr, bool val) const;
	bool SourceSet(ALenum attr, double val) const;
	bool SourceSet(ALenum attr, const Vector3f& val) const;

	bool SourceSetAll(bool force = false);

	PLAYBACK_MODE PlaybackMode() const {
		return m_playbackMode;
	}
	bool SetPlaybackMode(const PLAYBACK_MODE& pm) {
		m_playbackMode = pm;
		return true;
	}

	bool SetRelative(bool rel);
	bool GetRelative() const {
		return m_soundAttr.relative;
	}

	bool SetLooping(bool looping);
	bool GetLooping() const {
		return m_soundAttr.looping;
	}

	bool SetLoopingDelay(TimeMs timeMs);
	TimeMs GetLoopingDelay() const {
		return m_soundAttr.loopDelay.delayTime();
	}
	void LoopingDelaySet(TimeMs timeMs) {
		LoopingDelayReset();
		m_soundAttr.loopDelay.setDelayTime(timeMs);
	}
	void LoopingDelayReset() {
		m_soundAttr.loopDelay.timer().Pause().Reset();
	}
	void LoopingDelayStart() {
		m_soundAttr.loopDelay.timer().Start();
	}
	bool LoopingDelayElapsed() const {
		return GetLooping() && m_soundAttr.loopDelay.elapsed();
	}

	bool SetGain(double gain);
	double GetGain() const {
		return m_soundAttr.gain;
	}

	bool SetPitch(double pitch);
	double GetPitch() const {
		return m_soundAttr.pitch;
	}

	bool SetOuterGain(double gain);
	double GetOuterGain() const {
		return m_soundAttr.outerGain;
	}

	bool SetMaxDist(double dist);
	double GetMaxDist() const {
		return m_soundAttr.maxDist;
	}

	bool SetRefDist(double dist);
	double GetRefDist() const {
		return m_soundAttr.refDist;
	}

	bool SetRolloff(double rolloff);
	double GetRolloff() const {
		return m_soundAttr.rolloff;
	}

	bool SetConeOuterAngleRad(double radians);
	bool SetConeOuterAngleDeg(double degrees) {
		return SetConeOuterAngleRad(ToRadians(degrees));
	}
	double GetConeOuterAngleRad() const {
		return m_soundAttr.coneOuterAngle;
	}
	double GetConeOuterAngleDeg() const {
		return ToDegrees(GetConeOuterAngleRad());
	}

	bool SetConeInnerAngleRad(double radians);
	bool SetConeInnerAngleDeg(double degrees) {
		return SetConeInnerAngleRad(ToRadians(degrees));
	}
	double GetConeInnerAngleRad() const {
		return m_soundAttr.coneInnerAngle;
	}
	double GetConeInnerAngleDeg() const {
		return ToDegrees(GetConeInnerAngleRad());
	}

	bool SetPosition(const Vector3f& pos);
	Vector3f GetPosition() const {
		return m_locationMat.GetTranslation();
	}

	bool SetDirection(const Vector3f& dir);
	Vector3f GetDirection() const {
		return m_locationMat.GetRowZ();
	}

	bool SetRotationRad(float radians) {
		return SetDirection(Vector3f(sin(radians), 0.0, cos(radians)));
	}
	bool SetRotationDeg(float degrees) {
		return SetRotationRad(ToRadians(degrees));
	}
	double GetRotationRad() const;
	double GetRotationDeg() const {
		return ToDegrees(GetRotationRad());
	}

	/** DRF
	* Get/Set Sound Attributes
	* NOTE: Interface Angles Are In Degrees! (internal are in radians) !!!
	*/
	bool SetSoundAttribute(ULONG attr, double value);
	double GetSoundAttribute(ULONG attr);

	void SetHover(bool hover);

	void SetOutlineColor(const D3DXCOLOR& color) {
		m_outlineColor = color;
	}

	// DRF - Never Show Sound Placements Local Or Attached To Players!
	bool ShowSoundPlacement() const {
		return !IsAttachedToPlayer();
	}

	void SetOutlineState(bool outline) {
		m_outline = (outline && ShowSoundPlacement());
	}
	bool GetOutlineState() const {
		return (m_outline && ShowSoundPlacement());
	}

	void SetOutlineZSorted(bool sort) {
		m_outlineZSorted = sort;
	}
	bool IsOutlineZSorted() const {
		return m_outlineZSorted;
	}

	bool IntersectRay(const Vector3f& origin, const Vector3f& direction, float* distance, Vector3f* location, Vector3f* normal) const;

	bool Stop(bool force = false);
	bool Rewind();
	bool Play();

	/** DRF
	* Returns the proxy responsible for actually playing the sound.
	*/
	OALSoundBuffer* ProxySound() const;

	/** DRF
	* Returns true if the proxy responsible for actually playing the sound
	* is ready, meaning the sound is fully loaded and available to be played.
	*/
	bool ProxySoundReady() const {
		return (ProxySound() != NULL);
	}

	/** DRF
	* Returns true if proxy is actually playing sound.  Inbetween sound
	* looping this will return to false during the time no sound is heard but
	* m_playing will still be true.
	*/
	bool ProxySoundPlaying() const;

	/** DRF
	* Plays proxy sound and returns true on success or false on failure.
	*/
	bool ProxySoundPlay() const;

	/** DRF
	* Rewinds proxy sound and returns true on success or false on failure.
	*/
	bool ProxySoundRewind() const;

	/** DRF
	* Stops proxy sound and returns true on success or false on failure.
	*/
	bool ProxySoundStop(bool force = false) const;

	// DRF - Returns distance squared between the given position and the sound current position.
	double DistSqPos(const Vector3f& pos) const {
		return DistanceSquared(pos, GetPosition());
	}

	// DRF - Returns distance squared between the given direction and the sound current direction.
	double DistSqDir(const Vector3f& rot) const {
		return DistanceSquared(rot, GetDirection());
	}

	// Returns true if position is within range of this sound.
	bool InRange(const Vector3f& pos) const {
		return DistSqPos(pos) < Square(GetMaxDist());
	}

	bool Update(const Vector3f& posListener, const Vector3f* posAttach = NULL);

	bool LoadSoundByGlid(const GLID& glid);

	ReMesh* GetSoundMesh() const {
		return m_soundMesh;
	}

	ReMesh* GetRangeMesh() const {
		return m_rangeMesh;
	}

	ReMesh* GetConeMesh() const {
		return m_coneMesh;
	}

	ReMesh* GetInnerMesh() const {
		return m_innerAngleRing;
	}

	ReMesh* GetOuterMesh() const {
		return m_outerAngleRing;
	}

	void GenConeMesh();

	ReMesh* getRingMesh(double angle, double radius, CMaterialObject* mat);

	D3DXCOLOR GetOutlineColor() const {
		return m_outlineColor;
	}

	const Matrix44f* GetLocationMat() const {
		return &m_locationMat;
	}

	const Matrix44f* GetRangeMat() const {
		return &m_rangeMat;
	}

	virtual double GetNumber(ULONG id) {
		return GetSoundAttribute(id);
	}
	virtual void SetNumber(ULONG id, double val) {
		SetSoundAttribute(id, val);
	}

public:
	// DRF - ED-2803 - Vehicle Sounds - Sound Modifiers
	struct Modifier {
		enum Input {
			INPUT_NONE,
			INPUT_SPEED,
			INPUT_ACCEL,
			INPUT_THROTTLE,
			INPUT_SKID,
			INPUTS
		} m_input = INPUT_NONE;

		enum Output {
			OUTPUT_NONE,
			OUTPUT_PLAY,
			OUTPUT_PITCH,
			OUTPUT_GAIN,
			OUTPUTS
		} m_output = OUTPUT_NONE;

		double m_vGain;
		double m_vOffset;
		bool m_playing;

		Modifier(int input, int output, double gain, double offset) :
				m_input((Modifier::Input)input),
				m_output((Modifier::Output)output),
				m_vGain(gain),
				m_vOffset(offset),
				m_playing(true) {
		}

		Modifier(const Input& input, const Output& output, double gain, double offset) :
				m_input(input),
				m_output(output),
				m_vGain(gain),
				m_vOffset(offset),
				m_playing(true) {
		}
	};
	std::vector<Modifier> m_modifiers;
	std::vector<Modifier>& Modifiers() {
		return m_modifiers;
	}

	bool AddModifier(const Modifier& mod) {
		Modifier m(mod);
		SetLooping(true); // modified sounds always loop
		m_modifiers.push_back(m);
		return true;
	}

	bool SetModifierOutputValue(Modifier& mod, double vInput) {
		switch (mod.m_output) {
			case Modifier::Output::OUTPUT_PLAY: {
				bool playing = ((vInput * mod.m_vGain) > mod.m_vOffset);
				if (playing != mod.m_playing) {
					SetLooping(false);
					mod.m_playing = playing;
					playing ? Play() : Stop();
				}
				return true;
			}

			case Modifier::Output::OUTPUT_PITCH:
				return SetPitch(vInput * mod.m_vGain + mod.m_vOffset);

			case Modifier::Output::OUTPUT_GAIN:
				return SetGain(vInput * mod.m_vGain + mod.m_vOffset);
		}
		return false;
	}

	std::shared_ptr<Physics::RigidBodyStatic> GetVisibleBody() { return m_pVisibleBody; }

private:
	DECLARE_GETSET
};

} // namespace KEP
