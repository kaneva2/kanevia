///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CEXMeshClass.h"
#include "CStateManagementClass.h"

namespace KEP {

class CScreenFlashObj : public CObject {
public:
	CScreenFlashObj();

	CEXMeshObj* m_visual;
	BOOL m_on;
	TimeMs m_duration;
	TimeMs m_stamp;

	void SafeDelete();
	void DoIt(TimeMs duration, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Callback(TimeMs currentTime,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		CStateManagementObj* m_stateManager,
		TextureDatabase* TextureDataBase);
	void Render(LPDIRECT3DDEVICE9 g_pd3dDevice,
		CStateManagementObj* m_stateManager,
		TextureDatabase* TextureDataBase);
};

} // namespace KEP