///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
// SpawnGenDlg.h : header file
//
#include "CServerItemGen.h"
#include "CGlobalInventoryClass.h"
#include "CWorldAvailableClass.h"
#include "CMovementObj.h"
#include "CGlobalInventoryClass.h"
#include "afxcmn.h"
#include "CSkillClass.h"
#include "KEPEditDialog.h"
#include "AIClass.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "NPCConfigPropertiesCtrl.h"
#include "ItemContentsPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CSpawnGenDlg dialog

class CSpawnGenDlg : public CKEPEditDialog {
	// Construction
public:
	CSpawnGenDlg(CServerItemGenObjList*& serverItemGen,
		CWnd* pParent = NULL); // standard constructor

	CServerItemGenObjList*& m_serverItemGenRef;
	CGlobalInventoryObjList* m_globalInventoryList;
	CWorldAvailableObjList* m_worldsDB;
	CMovementObjList* m_edbRef;
	CAIObjList* m_aiListRef;
	CMovementObj* m_focusEntity;
	CSkillObjectList* m_skillDatabaseRef;
	BOOL m_focusEntityExists;
	int m_currentSelTreeID;

	// Dialog Data
	//{{AFX_DATA(CSpawnGenDlg)
	enum { IDD = IDD_DIALOG_SPAWNGENDLG };
	CListBox m_contentsList;
	CListBox m_generators;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSpawnGenDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListTreeGenerators(int sel);
	void InListGenerators(int sel);
	void InListItemContents(int sel);
	void InListConfigurations(CServerItemGenObj* spnGenPtr, int sel);
	//void		InListMaterials(CServerItemGenObj * spnGenPtr, int sel);
	CStringA ConvToString(int number);
	void SetAllItemData(const CItemObj* itemData, int glid);
	HTREEITEM InsertRoot(CStringA name, int idIndex, BOOL selected);
	HTREEITEM AddTreeItem(HTREEITEM root, CStringA name, int idIndex, BOOL selected);
	CServerItemGenObj* GetCurrentItem(bool msgBox = true);
	bool GetCurrentDim(int& type, BYTE& slot);
	void RepaintPropWindow(CKEPPropertiesCtrl& prop);

	// Generated message map functions
	//{{AFX_MSG(CSpawnGenDlg)
	afx_msg void OnBUTTONAddNew();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBUTTONEdit();
	afx_msg void OnButtonDelete();
	afx_msg void OnSelchangeLISTGeneratorList();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonSave();
	afx_msg void OnBUTTONAddItemToContents();
	afx_msg void OnBUTTONDeleteItemFromContents();
	afx_msg void OnSelchangeLISTContentsList();
	afx_msg void OnBUTTONTypeCfg();
	afx_msg void OnBUTTONSaveUnit();
	afx_msg void OnBUTTONLoadUnit();
	afx_msg void OnBUTTONIntegrityCheck();
	afx_msg void OnBUTTONrebuild();
	afx_msg void OnBUTTONsearch();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CTreeCtrl m_generatorTree;

	afx_msg void OnTvnSelchangedTreegeneratorslist(NMHDR* pNMHDR, LRESULT* pResult);
	void GetCurrentPosition();
	void RebuildDim();
	void SetItemContentArmed();
	void SetDeformableMeshIndex();
	void SetDeformableMatIndex();
	void UpdateDeformableMeshIndex();
	int GetNumMeshIndicesForDeformableMesh();
	int GetNumMatIndicesForDeformableMesh();
	void AddAppendage();

private:
	CStringA m_sSearchDisplayName;

	CCollapsibleGroup m_colNPCConfig;
	CButton m_grpNPCConfig;
	CNPCConfigPropertiesCtrl m_propNPCConfig;

	CCollapsibleGroup m_colItemContents;
	CButton m_grpItemContents;
	CItemContentsPropertiesCtrl m_propItemContents;

	CKEPImageButton m_btnAddNPC;
	CKEPImageButton m_btnDeleteNPC;
	CKEPImageButton m_btnReplaceNPC;
	CKEPImageButton m_btnSaveNPCUnit;
	CKEPImageButton m_btnLoadNPCUnit;
	CKEPImageButton m_btnNPCAIProperties;

	CKEPImageButton m_btnAddItemToContents;
	CKEPImageButton m_btnDeleteItemFromContents;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};
