/******************************************************************************
 CameraDBToListItemMap.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CameraDBToListItemMap.h : header file
//
#include "ListItemMap.h"

class CCameraObjList;

class CCameraDBToListItemMap {
public:
	CCameraDBToListItemMap() :
			m_list(0) {}
	CCameraDBToListItemMap(CCameraObjList* list) :
			m_list(list) {}
	~CCameraDBToListItemMap() {}

	ListItemMap GetCameras();

private:
	CCameraObjList* m_list;
};