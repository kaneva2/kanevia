/******************************************************************************
 AngleRounding.h


 Copyright (c) 2004-2010 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

class AngleRounding {
public:
	static float getYRotAngle(float xDir, float zDir) {
		float len = sqrt((xDir * xDir) + (zDir * zDir));
		xDir = xDir / len;
		zDir = zDir / len;

		float newval = atan2(zDir, xDir) * (180 / D3DX_PI);
		if (newval < 0) {
			newval = newval + 360.0f;
		}
		return newval;
	}

	static float roundNearest(float value, float step) {
		float t = floor(value / step + 0.5f);
		return t * step;
	}

	static void getVecFromYRotAngle(Vector3f& outVec, float yRotAngle, Vector3f origVector) {
		if (yRotAngle != 0) {
			float radians = yRotAngle * (D3DX_PI / 180); // convert back to radians
			if (radians < 0) {
				radians = radians * -1; // use only first geometric quadrant
			}
			float newx = cosf(radians);
			float newz = sinf(radians);
			//if(origVector.x <= 0)
			//{
			//	newx = newx * -1;									// restore origional x direction
			//}
			//if(origVector.z < 0)
			//{
			//	newz = newz * -1;									// restore origional y direction
			//}
			outVec.x = newx;
			outVec.z = newz;
		} else {
			if (origVector.z < -.9) {
				outVec.x = 0;
				outVec.z = -1;
			} else if (origVector.x < -.9) {
				outVec.x = -1;
				outVec.z = 0;
			} else if (origVector.z > .9) {
				outVec.z = 1;
				outVec.x = 0;
			} else if (origVector.x > .9) {
				outVec.x = 1;
				outVec.z = 0;
			}
		}
		outVec.y = 0.0f;
	}

	static void roundYRotation(Vector3f invec, Vector3f& outvec, float step) {
		float yRotAngle = AngleRounding::getYRotAngle(invec.x, invec.z);
		yRotAngle = AngleRounding::roundNearest(yRotAngle, step);
		AngleRounding::getVecFromYRotAngle(outvec, yRotAngle, invec);
	}
};

} // namespace KEP