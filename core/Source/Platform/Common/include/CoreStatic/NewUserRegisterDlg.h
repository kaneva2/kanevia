#pragma once

#include "KepPropertyPage.h"
#include "resource.h"
#include "StatLink.h"
#include "NewGameLoginDlg.h"

#include <map>

typedef std::map<std::string, std::string> CountryCodeMap;

// CNewGameLoginDlg dialog
class CDistributionGameSelectionDlg;
class CNewUserRegisterDlg : public CKEPPropertyPage {
	DECLARE_DYNAMIC(CNewUserRegisterDlg)

public:
	CNewUserRegisterDlg(CNewGameLoginDlg& dlg, const char* gameDir, const char* editorBaseDir, bool skipExtraEditorDlgs, Logger& logger);

	virtual ~CNewUserRegisterDlg();

	// Dialog Data
	enum { IDD = IDD_REGISTER_USER };

	BOOL OnSetActive();
	LRESULT OnWizardNext();
	bool CreateUserAccount();
	BOOL OnInitDialog();

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAlreadyMember();

	CStringA m_gameDir;
	CStaticLink m_termsLink;
	CStaticLink m_privacyLink;
	CNewGameLoginDlg& m_loginDlg;
	CStringA m_email;
	CStringA m_password;
	CStringA m_username;
	CStringA m_firstname;
	CStringA m_lastname;
	CStringA m_gender;
	CComboBox m_country;
	CDateTimeCtrl m_birthdate;
	CStringA m_zipcode;
	CStringA m_createURL;
	CStringA m_countryURL;
	Logger m_logger;

	CStringA m_editorBaseDir;

	CountryCodeMap m_countryCodes;
	CStringA m_countryCodeSel;

	CFont m_createAccountHeaderFont;

	bool m_skipNextPage;
	bool m_skipExtraEditorDlgs;
};
