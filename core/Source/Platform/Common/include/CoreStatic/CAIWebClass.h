///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "d3d.h"
#include "matrixArb.h"
#include "CAITreeSearchClass.h"
#include "KEPObList.h"

namespace KEP {

class CAIRouteToObj : public CObject, public GetSet {
	DECLARE_SERIAL(CAIRouteToObj);

public:
	//Order of objects mimics web list itself
	CAIRouteToObj();
	int m_mapNodeCount; //
	int* m_map; //list of route positions in necessary order

	void SafeDelete();
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CAIRouteToObjList : public CObList {
public:
	CAIRouteToObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CAIRouteToObjList);
};

//----------------------------------------------------//
//----------------------------------------------------//
//----------------------------------------------------//
class CAIWebNode : public CObject {
	//CAIWebObj(){}

	DECLARE_SERIAL(CAIWebNode);

public:
	CAIWebNode();
	CStringA m_locationName; //=}
	CAIRouteToObjList* m_routeToAllFromLocationList;
	int* m_directLinks;
	int m_directLinkCount;
	Vector3f m_position;
	float m_radius;

	void AddDirectLink(int newInt);
	int* GetRoute(int index, int* mapNodeCount);
	void SafeDelete();
	void Serialize(CArchive& ar);
};

class CAIWebNodeList : public CObList {
public:
	CAIWebNodeList(){};

	DECLARE_SERIAL(CAIWebNodeList);

	//Functions
	int GetNearestNode(Vector3f location);
	float GetRadius(int index);
	Vector3f GetPosition(int index);
	BOOL VerifyMoreInList(CAIWebNode* wbPtr);
	int* AddIntegerIntoArray(int* intArray, int* arrayCount, int newInt);
	int GetIndexFromPointer(CAIWebNode* wbPtr);
	void SafeDelete();
	void CompileNode(CAIWebNode* wMainObj,
		TimeMs querryDuration,
		CAIWebNode* destObj,
		CAIRouteToObj* routePtr);
	BOOL CompileAll(int querryDuration); //generates connections and routes
	BOOL GetShortestRoute(int current, int target);
	BOOL GenerateNodeInfo(int& backPath,
		int& currentIndex,
		CAITree* m_currentTree);
	CAIWebNode* GetByIndex(int index);
	BOOL BeenThereAlready(CAITree* m_baseTree, int destination);
	BOOL UntraveledPathExists(int* currentLinksTraveled,
		int& currentLinkTraveledCount,
		int& directLinkCount,
		int* pathDirectLinks,
		int& randomPathSelection);
};
//----------------------------------------------------//
//----------------------------------------------------//
//----------------------------------------------------//
class CAIWebObj : public CObject {
	//CAIWebObj(){}

	DECLARE_SERIAL(CAIWebObj);

public:
	CAIWebObj();

	CStringA m_name;
	CAIWebNodeList* m_web;

	int* GetRoute(int currentPos, int dest, int* nodeCount);
	void SafeDelete();
	void Serialize(CArchive& ar);
	BOOL IsFinalized();
	void AddNode(CAIWebNode* wNode);
	void RemoveNode(POSITION posLoc);
	void CompileAllNodes(int duration);

private:
	BOOL m_compiled;
};

class CAIWebObjList : public CKEPObList {
public:
	CAIWebObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CAIWebObjList);
};

} // namespace KEP