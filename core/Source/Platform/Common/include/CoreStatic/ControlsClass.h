///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// added by serialize.pl
#include <GetSet.h>
#include "CSkillClass.h"
#include "KEPObList.h"
#include "..\CoreStatic\DXInputSystem.h"
#define ACTIONS_LANG_OFFSET 1000
#include "CommandActions.h"

namespace KEP {

enum ControlObjType {
	CO_TYPE_KEYBOARD,
	CO_TYPE_MOUSE,
	CO_TYPE_JOYSTICK,
	CO_TYPES
};

enum ControlObjActionState {
	CO_ACTION_STATE_UP,
	CO_ACTION_STATE_PRESSED,
	CO_ACTION_STATE_RELEASED,
	CO_ACTION_STATE_DOWN,
	CO_ACTION_STATES
};

//this class holds a list of valid keys assigned to actions/controlers/keyboatrds ect
class ControlInfoObj : public CObject {
protected:
	ControlInfoObj() {
	}

	DECLARE_SERIAL(ControlInfoObj);

public:
	ControlInfoObj(
		ControlObjType const& L_controlType,
		int L_buttonDown,
		eCommandAction const& cmdAction,
		CStringA L_description,
		float L_miscFloat1,
		float L_miscFloat2,
		float L_miscFloat3);

	CStringA GetButtonString();

	float m_miscFloat1;
	float m_miscFloat2;
	float m_miscFloat3;
	CStringA m_miscString;
	CStringA m_description;

	ControlObjType m_controlType; // keyboard, mouse, joystick...

	int m_buttonDown;

	eCommandAction m_cmdAction;

	void Serialize(CArchive& ar);
};

class ControlInfoList : public CObList {
public:
	ControlInfoList(){};

	DECLARE_SERIAL(ControlInfoList);

	void AddControlEvent(CStringA keyString, CStringA actionString, int glid,
		int miscFloat2, int miscFloat3, int miscInt3,
		BOOL replace, BOOL modifyExisting);

	float ReturnMiscFloatByActionName(CStringA action);

	CStringA GetKeyAssignmentByActionString(CStringA actionString);
	ControlInfoObj* GetByActionName(CStringA actionName);
	ControlInfoObj* UnassignByActionName(CStringA actionName);
	ControlInfoObj* UnassignByKeynum(int KeyNum);
	CStringA ReturnActionByIndexAdv(int index,
		int& miscInt,
		int& miscInt2,
		int& miscInt3,
		int& miscInt4,
		CSkillObjectList* m_skillDB);

	// string to integer mapping
	static int GetMouseKey(const CStringA key);
	static int GetKeyNum(const CStringA key);
	static int GetJSNum(const CStringA key);

	// integer to string mapping
	static CStringA GetButtonString(int button, ControlObjType const& controlType);

	static CStringA ReturnKeyStringByIndex(int loop);

	// return a single key currently pressed (to detect and then map it to an action
	static CStringA GetCurrentKeyString(const MouseState& mouseState, const KeyboardState& keyboardState);

	// Action / String conversion
	static eCommandAction GetCommandAction(CStringA action);

	static CStringA ReturnActionByIndex(int loop);

private:
	static void InitKeyboardMapping();
	static bool initializedKbMapping;
};

class ControlDBObj : public CObject, public GetSet {
protected:
	ControlDBObj() {}

	DECLARE_SERIAL(ControlDBObj);

public:
	ControlDBObj(ControlObjType const& L_controlBasis, CStringA L_controlConfigName, ControlInfoList* L_controlInfoList);

	ControlObjType m_controlType;

	float m_mouseSensitivity;
	BOOL m_invertMouse;
	CStringA m_controlConfigName;
	ControlInfoList* m_controlInfoList;
	BOOL m_allowMultiBind;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class ControlDBList : public CKEPObList {
public:
	ControlDBList(){};

	DECLARE_SERIAL(ControlDBList);
};

} // namespace KEP