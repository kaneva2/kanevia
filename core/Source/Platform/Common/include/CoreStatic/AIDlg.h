/******************************************************************************
 AIDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_AI_DLG_H_)
#define _AI_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AIDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

////////////////////////////////////////////
// CAIDialog control

class CAIDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CAIDialog)

public:
	CAIDialog();
	virtual ~CAIDialog();

	// Dialog Data
	//{{AFX_DATA(CAIDialog)
	enum { IDD = IDD_DIALOG_AI_BOTS };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CAIDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colWorldMapSearchTrees;
	CButton m_grpWorldMapSearchTrees;

	CCollapsibleGroup m_colCollisionSystems;
	CButton m_grpCollisionSystems;

	CCollapsibleGroup m_colAIBots;
	CButton m_grpAIBots;

	CCollapsibleGroup m_colAIScripts;
	CButton m_grpAIScripts;

	CCollapsibleGroup m_colScriptEvents;
	CButton m_grpScriptEvents;

	CKEPImageButton m_btnAddTree;
	CKEPImageButton m_btnDeleteTree;
	CKEPImageButton m_btnEditTree;
	CKEPImageButton m_btnLoadTree;

	CKEPImageButton m_btnDeleteCollisionSys;
	CKEPImageButton m_btnImportCollisionSys;
	CKEPImageButton m_btnExportCollisionSys;
	CKEPImageButton m_btnSaveCollisionSys;
	CKEPImageButton m_btnLoadCollisionSys;

	CKEPImageButton m_btnAddBot;
	CKEPImageButton m_btnDeleteBot;
	CKEPImageButton m_btnEditBot;
	CKEPImageButton m_btnSaveBot;
	CKEPImageButton m_btnLoadBot;

	CKEPImageButton m_btnAddScript;
	CKEPImageButton m_btnDeleteScript;
	CKEPImageButton m_btnEditScript;

	CKEPImageButton m_btnAddScriptEvent;
	CKEPImageButton m_btnDeleteScriptEvent;
	CKEPImageButton m_btnEditScriptEvent;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_AI_DLG_H_)