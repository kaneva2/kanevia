///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#include <map>

#include "KEPObList.h"
#include "GetSet.h"
#include "CRTLightClass.h"
#include "CCollisionClass.h"
#include "StreamableDynamicObj.h"
#include "RenderEngine\ReD3DX9.h"
#include "InventoryType.h"
#include "KEPConstants.h"
#include "IResource.h"
#include <map>
#include "CParticleEmitRange.h"
#include <KEPPhysics/Declarations.h>
#include "Common\include\MediaParams.h"
#include "Common\include\DynamicObjTypes.h"
#include "ResourceManagers/SkeletonManager.h"
#include "ResourceManagers/SkeletonProxy.h"
#include "common\include\LookAt.h"
#include "common\include\ObjectCounter.h"

#include "Common\include\CoreStatic\DynamicObjectSubListId.h"
#include "Common\include\CoreStatic\RuntimeSkeleton.h"
#include "Common\include\CoreStatic\CFrameObj.h"

#include "Common\include\TextureProvider.h"
#include "ItemTextureInfo.h"

namespace KEP {

class IExternalEXMesh;
class CTriggerObject;
class CTriggerObjectList;
class EffectTrack;
class SkeletonProxy;
class SkeletonConfiguration;
class StreamableDynamicObject;
class SkeletonLabels;
class UGCItemTextureProvider;
class IMemSizeGadget;

class CDynamicPlacementObj : private ObjectCounter<CDynamicPlacementObj>,
							 public CObject {
public:
	CDynamicPlacementObj(GLID globalId, int placementId, bool fromMovementObj);
	~CDynamicPlacementObj();

	std::string ToStr(bool verbose = true) const;

	bool IsBaseObjInited() const {
		return m_inited;
	}
	StreamableDynamicObject* GetBaseObj() {
		return m_baseObj;
	}
	std::string GetBaseObjName() const {
		return m_baseObj ? m_baseObj->getName() : "<null>";
	}
	int GetPlacementId() const {
		return m_placementId;
	}

	void SetWorldMatrix(const Matrix44f& mtx);
	bool GetWorldMatrix(Matrix44f& matrix) const;

	Matrix44f updateNextFrameMatrix() const;

	// DRF - ED-7924 - As a developer i can attach the camera to any object
	CFrameObj m_baseFrame;
	CFrameObj* getBaseFrame() {
		Matrix44f mtx;
		GetWorldMatrix(mtx);
		m_baseFrame.AddTransformReplace(mtx);
		return &m_baseFrame;
	}

	void AssignObjType();
	DYN_OBJ_TYPE ObjType() const {
		return m_objType;
	}
	std::string ObjTypeStr() const {
		return DynObjTypeStr(ObjType());
	}

	Vector3f GetPosition() const {
		return m_position;
	}
	void SetPosition(const Vector3f& position);

	void SetRotationEulerAngleZXY(double x, double y, double z); // WS rotation

	GLID GetGlobalId() const {
		return m_globalId;
	}

	bool IsLocal() const {
		return m_placementId < 0;
	}

	void SetIsDerivable(bool enable) {
		m_isDerivable = enable;
	}
	bool IsDerivable() const {
		return !IsLocal() && m_isDerivable;
	}

	void SetIsAttachable(bool enable) {
		m_isAttachable = enable;
	}
	bool IsAttachable() const {
		return (m_baseObj ? m_baseObj->getAttachable() : m_isAttachable);
	}

	void SetIsInteractive(bool enable) {
		m_isInteractive = enable;
	}
	bool IsInteractive() const {
		return (m_baseObj ? m_baseObj->getInteractive() : m_isInteractive);
	}

	void SetMediaSupported(bool enable) {
		m_mediaSupported = enable;
	}
	bool MediaSupported() const {
		return (m_baseObj ? m_baseObj->MediaSupported() : m_mediaSupported);
	}

	void SetCollisionEnabled(bool bEnabled);
	bool IsCollisionEnabled() const {
		return m_bCollisionEnabled && (m_iCollisionSuspended == 0) && IsVisible();
	}

	void SuspendCollision(bool bSuspend) {
		m_iCollisionSuspended += 2 * bSuspend - 1;
		UpdateRigidBodyCollidableState();
	}

	bool IsCustomizable() const {
		return m_customizable;
	}
	void SetCustomizable(bool allow) {
		m_customizable = allow;
	}

	void SetEffectTrack(std::unique_ptr<EffectTrack> pEffectTrack);

	ReMesh* GetReLODMesh(UINT visIndex, UINT lod);

	void setPrimaryAnimationSettings(const AnimSettings& animSettings);
	void getPrimaryAnimationSettings(AnimSettings& animSettings) const {
		animSettings = isPrimaryAnimationSettingsValid() ? m_animSettings : AnimSettings();
	}
	bool isPrimaryAnimationSettingsValid() const {
		return m_pSkeleton && m_pSkeleton->getBoneList() && m_animSettings.isValid();
	}

	void InitReMeshes();

	void InitRigidBodies();

	template <typename Callable>
	void ForEachRigidBody(Callable&& callable) {
		if (m_pRigidBody)
			callable(m_pRigidBody.get());
		if (m_pRigidBodyVisual)
			callable(m_pRigidBodyVisual.get());
	}
	const std::shared_ptr<Physics::RigidBodyStatic>& GetRigidBody() {
		return m_pRigidBody;
	}

	// true if the dynamic object is interpolating between positions this frame
	bool IsMoving() const {
		return m_pPivotTransitionPosition ? true : false;
	}

	// true if the dynamic object is interpolating between orientations this frame
	bool IsRotating() const {
		return m_pPivotTransitionRotation ? true : false;
	}

	void MoveForward(double d); //move along the direction vector
	void Slide(double d); //move along the slide vector
	void Lift(double d); //move along the up vector
	void MoveInDirection(double d, Vector3f dir); // Move object in a generic direction
	void MoveInDirectionRounding(double d, Vector3f dir, int numdirs);
	void SaveChanges(); //save changes made to transformation matrix
	void CancelChanges(); //cancel changes made to transformation matrix
	void SetScale(float sx, float sy, float sz);
	void GetYAngleFromPoint(Vector3f point, double* angle, double* magnitude); // Just looks at the object's position and works in 2D.
	void GetBoundingBox(Vector4f& min, Vector4f& max);
	void GetBoundingBoxAnimAdjust(Vector4f& min, Vector4f& max);
	float GetBoundingRadius() { return m_boundRadius; }

	bool IntersectRay(const Vector3f& origin, const Vector3f& direction, float* distance, Vector3f* location, Vector3f* normal, bool testAlpha);
	bool IntersectSphere(const Vector3f& origin, float radius, const Vector3f& orientation, float maxAngle, float* distance, Vector3f* location, Vector3f* normal, bool testAlpha);

	void Reconfig();
	void UpdateCallback(TimeMs timeMs);
	void FlushBoundBox();

	template <typename T>
	void SetCustomTexture(bool force, T&& customTexturePtr) {
		if (!customTexturePtr) {
			ResetCustomTexture();
			return;
		}

		//Ankit ----- modifying it to so that base initaial custom texture is applied even if it is downloaded after the default texture got applied
		if (!m_textureSet || force || m_waitForCustomTextAndApply) {
			if (force) {
				m_waitForCustomTextAndApply = false;
			}
			m_customTexture = std::forward<T>(customTexturePtr);
			m_textureSet = true;
		}
	}

	template <typename T>
	void SetUGCItemTexture(T&& ugcItemTexturePtr) {
		m_ugcItemTexture = std::forward<T>(ugcItemTexturePtr);
	}

	void ResetCustomTexture();
	std::string GetCustomTextureUrl() const;
	void SetMaterial(int index, CMaterialObject* mat);
	CMaterialObject* GetMaterial(int id);

	void UpdateCustomMaterials(bool chainLoad = false);

	void SetAssetId(int assetId) {
		m_assetId = assetId;
	}
	int GetAssetId() const {
		return m_assetId;
	}

	void SetInventorySubType(int invSubType) {
		m_inventorySubType = invSubType;
	}
	int GetInventorySubType() const {
		return m_inventorySubType;
	}

	void SetTextureUrl(std::string url) {
		m_textureUrl = url;
	}
	std::string GetTextureUrl() const {
		return m_textureUrl;
	}

	void SetFriendId(int friendId) {
		m_friendId = friendId;
	}
	int GetFriendId() const {
		return m_friendId;
	}

	void Relocate(const Vector3f& direction, const Vector3f& slide, DWORD duration = 0);
	void Relocate(const Vector3f& position, DWORD moveDuration, const Vector3f& direction, const Vector3f& slide, DWORD orientDuration = 0);
	void Relocate(const Vector3f& position, DWORD duration = 0, DWORD accDur = 0, DWORD constVelDur = 0, double accDistFrac = 0, double constVelDistFrac = 0);

	bool SaveTemplateTextures(CStringA folder);
	bool AssignObjectToTemplate(CStringA folder);

	void MakeTranslucent();

	float GetMinDrawDistanceOverride() const {
		return m_minDrawDistanceOverride;
	}
	void SetMinDrawDistanceOverride(float _override) {
		m_minDrawDistanceOverride = _override;
	}

	void SetGameItemId(int gameItemId) {
		m_gameItemId = gameItemId;
	}
	int GetGameItemId() const {
		return m_gameItemId;
	}

	//! \brief Return TRUE if m_baseObj is ready to use (either loaded thru IResource or created in the memory)
	bool IsBaseObjReady();

	TResource<StreamableDynamicObject>* GetResource() { return m_pResource; }

	bool IsResourceReady() { return m_pResource ? m_pResource->StateIsLoadedAndCreated() : false; }
	bool IsResourceError() { return m_pResource ? m_pResource->IsErrored() : false; }

	FileSize GetResourceExpectedSize() {
		assert(m_pResource != nullptr);
		return m_pResource ? m_pResource->GetExpectedSize() : 0;
	}

	static TResource<StreamableDynamicObject>* GetDynamicObjectResource(GLID globalId);
	static TResource<StreamableDynamicObject>* GetDynamicObjectResourceFromMovementObj(GLID globalId, int placementId, RuntimeSkeleton*& pSkeleton);

	//! \brief Return all active textures
	void getInUseTextures(std::map<DWORD, CStringA>& outTextures);

	//! \brief Return skeleton label list
	SkeletonLabels* GetSkeletonLabels() const {
		return m_skeletonLabels;
	}

	//! \brief Add skeleton labels
	bool AddSkeletonLabel(const std::string& text, double fontSize, double r, double g, double b);
	bool AddSkeletonLabels(const SkeletonLabels& labels);

	//! \brief Reset skeleton labels
	bool ResetSkeletonLabels();

	void SetSkeletonConfiguration(std::unique_ptr<SkeletonConfiguration> pSkelCfg);
	SkeletonConfiguration* GetSkeletonConfiguration() { return m_pSkeletonConfiguration.get(); }

	//! \brief Set texture panning parameters
	void SetTexturePanning(USHORT meshId, USHORT uvSetId, float uIncr, float vIncr);
	void ApplyTexturePanningSettings(USHORT meshIdFilter = SKELETAL_ALL_MESHES, USHORT uvSetIdFilter = MATERIAL_ALL_UVSETS);

	bool SetVisibility(eVisibility visibility);
	eVisibility GetVisibility() const {
		return m_visibility;
	}
	bool IsVisible() const {
		return (m_visibility == eVisibility::VISIBLE);
	}
	bool IsHiddenForMe() const {
		return (m_visibility == eVisibility::HIDDEN_FOR_ME);
	}
	bool IsHiddenForAll() const {
		return (m_visibility == eVisibility::HIDDEN_FOR_ALL);
	}

	// ED-8445 - Force Object Updates If Camera Attached
	bool m_needsUpdate;
	bool SetNeedsUpdate(bool enable) {
		m_needsUpdate = enable;
		return true;
	}

	// ED-8445 - Force Object Updates If Camera Attached
	// If object is hidden specifically for this client, consider it an invisible system object (spawners etc). Stop updating it to save frame time.
	bool NeedsUpdate() const {
		return m_needsUpdate || !IsHiddenForMe();
	}
	bool NeedsUpdateCallback() const;

	bool AddParticle(const std::string& boneName, int particleSlotId, UINT particleGLID, const std::string& particleEffectName, const Vector3f& offset, const Vector3f& dir, const Vector3f& up);
	void RemoveParticle(const std::string& boneName, int particleSlotId);
	bool GetParticleGlidAndName(const std::string& boneName, int particleSlotId, GLID& particleGlid, std::string& name);

	void SetLookAt(ObjectType targetType, int targetId, const std::string& targetBone, const Vector3f& offset, bool followTarget, bool allowXZRotation, const Vector3f& forwardDirection);
	bool GetLookAt(ObjectType& targetType, int& targetId, std::string& targetBone, Vector3f& offset, bool& followTarget, bool& allowXZRotation, Matrix44f*& pForwardDirectionMatrix) const;

	const Vector3f* GetSnapLocations(int* snapLocationsCount) const;

	void SetOutlineState(bool outline) {
		m_outline = outline;
	}
	bool GetOutlineState() const {
		return m_outline;
	}

	void SetOutlineColor(const D3DXCOLOR& color) {
		m_outlineColor = color;
	}
	D3DXCOLOR GetOutlineColor() const {
		return m_outlineColor;
	}

	void SetOutlineZSorted(bool sort) {
		m_outlineZSorted = sort;
	}
	bool IsOutlineZSorted() const {
		return m_outlineZSorted;
	}

	void SetHighlight(bool highlight, const D3DXCOLOR& color);
	bool GetHighlightState() const {
		return m_highlight;
	}

	D3DXCOLOR GetHighlightColor() const {
		return m_highlightColor;
	}

	bool IsBoundingBoxRenderEnabled() const {
		return m_renderBoundingBox;
	}
	void SetBoundingBoxRenderEnabled(bool enable);

	ReMesh* GetBoundingBoxMesh();

	Matrix44f* GetBoundingBoxMatrix();

	ReMesh* GetMediaVideoRangeMesh();
	ReMesh* GetMediaAudioRangeMesh();

	static void UpdateMediaVisMaterials();

	void SetTextureColors(const TextureColorMap& textureColors) {
		m_textureColors = textureColors;
	}

	// Media Initialization
	void InitMedia();

	// Media Parameters
	const MediaParams& GetMediaParams() const {
		return m_mediaParams;
	}
	void SetMediaParams(const MediaParams& mediaParams);
	bool ValidMediaUrlAndParams() const {
		return m_mediaParams.IsUrlAndParamsValid();
	}
	void SetMediaURLandParams(const std::string& strURL, const std::string& strParams);

	// Media State
	MediaParams::STATE GetMediaState() const {
		return m_mediaParams.GetState();
	}
	bool SetMediaState(const MediaParams::STATE& state) {
		return m_mediaParams.SetState(state);
	}
	bool MediaIsStopped() const {
		return m_mediaParams.IsStopped();
	}
	bool MediaIsPlaying() const {
		return m_mediaParams.IsPlaying();
	}
	bool MediaIsChanged() const {
		return m_mediaParams.IsChanged();
	}

	// Media Volume (-1=no change, 0=mute)
	double GetMediaVolume() const {
		return m_mediaMute ? 0.0 : m_mediaParams.GetVolume();
	}
	bool SetMediaVolume(double volPct = -1) {
		return m_mediaParams.SetVolume(volPct);
	}

	// Media Radius (-1=no change, 0=global)
	MediaRadius GetMediaRadius() const {
		return m_mediaParams.GetRadius();
	}
	void SetMediaRadius(const MediaRadius& radius);

	// Media Types (global/local)
	bool IsMediaGlobal() const {
		return MediaSupported() && m_mediaParams.GetRadius().IsGlobal();
	}
	bool IsMediaLocal() const {
		return MediaSupported() && m_mediaParams.GetRadius().IsLocal();
	}

	// Media Mute
	bool GetMediaMute() const {
		return m_mediaMute;
	}
	bool SetMediaMute(bool mute) {
		m_mediaMute = mute;
		return true;
	}

	//Ankit ----- getters and setters for m_waitForCustomTextAndApply
	bool getWaitForCustomTextAndApply() {
		return m_waitForCustomTextAndApply;
	}
	void setWaitForCustomTextAndApply(bool missingCustomTextureBeingDownload) {
		m_waitForCustomTextAndApply = missingCustomTextureBeingDownload;
	}

	bool HasMedia() const { return ValidMediaUrlAndParams() || (IsMediaGlobal() && ObjType() == DYN_OBJ_TYPE::MEDIA_PLAYER); }
	IExternalEXMesh* GetExternalMesh() const { return m_pExternalMesh.get(); }
	const std::shared_ptr<IExternalEXMesh>& GetExternalMeshShared() const { return m_pExternalMesh; }
	void SetExternalMesh(std::shared_ptr<IExternalEXMesh> pExternalMesh);

	const std::set<DWORD>& getTextureHashes() const {
		return m_textureHashes;
	}

	CRTLightObj* GetLight() const { return m_pLight.get(); }
	bool HasLight() const { return m_pLight.get() != nullptr; }

	Vector3f GetLastLocation() const;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	using ObjectCounter<CDynamicPlacementObj>::howMany;

	static size_t howMany() {
		return ObjectCounter<CDynamicPlacementObj>::howMany();
	}

	bool ShouldBeInSublist(DynObjSubListId iSubListId) const;

	void MovementObjectReady();
	void InvalidateBaseObj();

	bool hasSkeleton() const {
		return m_pSkeleton != nullptr;
	}
	const RuntimeSkeleton* getSkeleton() const {
		return m_pSkeleton.get();
	}
	RuntimeSkeleton* getSkeleton() {
		return m_pSkeleton.get();
	}
	void setSkeleton(std::shared_ptr<RuntimeSkeleton> pSkeleton) {
		m_pSkeleton = pSkeleton;
	}

private:
	void InitBaseObj();
	void InitDynamicObjectResource();

	void BaseObjectReady();
	void ContentMetadataReady();
	void InitPlacement();
	void LoadError();

	void UpdateRigidBodyPosition();
	void UpdateRigidBodyCollidableState();
	void ApplyCurrentHighlight();

	bool isLowResTextureOpaque() const {
		return true; // Currently low res textures are in JPG format which is always opaque
	}

	void StartPivot(TimeMs moveDuration, TimeMs orientDuration, TimeMs accDur = 0, TimeMs constVelDur = 0, double accDistFrac = 0, double constVelDistFrac = 0);
	bool HandlePivotTransition(TimeMs timeMs, InOut<Matrix44f> matrix);
	bool HandleLookAt(InOut<Matrix44f> matrix);
	void UpdateShouldBeInSublist(DynObjSubListId sublist);

	template <typename T>
	void SendNotificationEvent();

	struct PendingParticleAdd {
		std::string boneName;
		int particleSlotId;
		GLID particleGLID;
		std::string particleEffectName;
		Vector3f offset, dir, up;

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

		PendingParticleAdd(const std::string& bone, int slot, GLID glid, const std::string& effName, const Vector3f& ofs, const Vector3f& d, const Vector3f& u) :
				boneName(bone), particleSlotId(slot), particleGLID(glid), particleEffectName(effName), offset(ofs), dir(d), up(u) {
		}
	};

	class PivotTransitionPosition {
	private:
		struct {
			Vector3f m_position; // Position
			TimeMs m_timePos = 0; // When the dynamic object should be at the final location.
		} m_keyframes[2]; // simple system for now, with just the start and finishing keyframes.
		bool m_bMoving = false;
		TimeMs m_currentInterpolationTime = 0;
		TimeMs m_accDur = 0;
		TimeMs m_constVelDur = 0;
		double m_accDistFrac = 0;
		double m_constVelDistFrac = 0;

	public:
		PivotTransitionPosition() {}
		bool isMoving() const {
			return m_bMoving;
		}

		void startPivotToPosition(TimeMs startTime, DWORD duration, DWORD accDur, DWORD constVelDur, double accDistFrac, double constVelDistFrac, const Matrix44f& beginningMatrix, const Matrix44f& endingMatrix);

		void SetCurrentTime(TimeMs time) {
			m_currentInterpolationTime = time;
		}

		bool interpolatePosition(Out<Vector3f> position);

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	};

	class PivotTransitionRotation {
	private:
		struct {
			Vector3f m_fdVect; // Forward direction. X row of matrix
			Vector3f m_upVect; // Up direction. Y row of matrix
			Vector3f m_sdVect; // Side direction. Z row of matrix
			TimeMs m_timeRot; // When the dynamic object should be at the final orientation. Set to zero when not rotating.
		} m_keyframes[2]; // simple system for now, with just the start and finishing keyframes.
		Vector3f m_bisectDirVect; // Mid-point direction vector helping with 180 degree rotation
		bool m_useBisectVect = false; // true if m_midDirVect is used
		bool m_bRotating = false;
		TimeMs m_currentInterpolationTime = 0;

	public:
		PivotTransitionRotation() {}
		bool isRotating() const {
			return m_bRotating;
		}
		void startPivotToOrientation(TimeMs startTime, DWORD duration, const Matrix44f& beginningMatrix, const Matrix44f& endingMatrix);
		void SetCurrentTime(TimeMs time) { m_currentInterpolationTime = time; }
		bool interpolateOrientation(Out<Vector3f> vX, Out<Vector3f> vY, Out<Vector3f> vZ);

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	};

	std::shared_ptr<RuntimeSkeleton> m_pSkeleton;

	StreamableDynamicObject* m_baseObj;

	// The minimum draw distance for this object. This changes when the object will stop rendering based on distance.
	// The larger this number, the longer the object will render. <= 0 means just use the client settings.
	float m_minDrawDistanceOverride;
	std::unique_ptr<CRTLightObj> m_pLight; // light for this object, created from m_baseObj

	TResource<StreamableDynamicObject>* m_pResource;
	RuntimeSkeleton* m_pMovementObjSkeleton;
	bool m_fromMovementObj;

	bool m_inited;
	bool m_loadErrorReported;
	bool m_isDerivable;
	bool m_isAttachable;
	bool m_isInteractive;
	bool m_mediaSupported;
	bool m_customizable; // whether or not the user should be allowed to change the pattern on this item
	bool m_bCollisionEnabled;
	bool m_textureSet;
	bool m_translucent;
	int m_placementId;
	GLID m_globalId;
	unsigned int m_iCollisionSuspended; // When >0, collision is disabled for this object.
	DYN_OBJ_TYPE m_objType;
	UINT m_numVisobjs;
	Vector3f m_objectScale; // object level scaling - to be combined with skeleton scaling
	Vector3f m_skeletonScaleBase; // base scaling factor when skeleton is first loaded from asset file

	AnimSettings m_animSettings;

	mutable Vector3f m_snapLocations[8];
	mutable bool m_snapLocationsDirty;
	bool m_outline; // Put an glowing outline around the object
	D3DXCOLOR m_outlineColor;
	bool m_outlineZSorted; // true if the glowing outline is depth tested
	bool m_highlight; // Highlight the object with texture effect
	D3DXCOLOR m_highlightColor; // RGB components are the highlight color, Alpha is used to blend current color
	std::unique_ptr<EffectTrack> m_pEffectTrack;
	std::unique_ptr<PivotTransitionPosition> m_pPivotTransitionPosition; // Pivot point transition keyframe data
	std::unique_ptr<PivotTransitionRotation> m_pPivotTransitionRotation; // Pivot point transition keyframe data
	std::shared_ptr<CustomTextureProvider> m_customTexture;
	std::shared_ptr<UGCItemTextureProvider> m_ugcItemTexture; // Separate item texture specified by designer from custom texture set by a creator or player
	std::vector<std::unique_ptr<CMaterialObject>> m_customMaterials; // stores customized material created by SetMaterial() calls
	TextureColorMap m_textureColors; // store texture average color by sub ID
	int m_assetId; // id of custom texture
	std::string m_textureUrl; // url of the custom texture if applicable
	int m_inventorySubType; // is item a gift 256 means normal, 512 is gift
	int m_friendId; // friend id if this is a picture frame
	Vector3f m_boxMinLocal; // world space oriented bbox min
	Vector3f m_boxMaxLocal; // world space oriented bbox max
	SkeletonLabels* m_skeletonLabels; // Skeleton labels -- to be applied to skeleton during InitPlacement
	std::unique_ptr<SkeletonConfiguration> m_pSkeletonConfiguration;
	bool m_renderBoundingBox;
	ReMesh* m_boxMesh;
	Matrix44f* m_boxMatrix;
	ReMesh* m_mediaVideoRangeMesh;
	Matrix44f m_mediaVideoRangeMatrix;
	ReMesh* m_mediaAudioRangeMesh;
	Matrix44f m_mediaAudioRangeMatrix;
	double m_mediaAudioRangeMeshRadius;
	int m_gameItemId;
	std::map<UINT, Vector2f> m_texturePanningSettings;
	eVisibility m_visibility;
	std::vector<PendingParticleAdd> m_pendingParticleAdds;
	std::unique_ptr<LookAtInfo> m_pLookAtInfo;
	std::shared_ptr<Physics::RigidBodyStatic> m_pRigidBody;
	std::shared_ptr<Physics::RigidBodyStatic> m_pRigidBodyVisual; // Non-collidable, for picking purposes
	MediaParams m_mediaParams;
	bool m_mediaMute;
	bool m_waitForCustomTextAndApply; //Ankit ----- adding a bool var to check for if custom texture is to be applied and so don't apply default texture from metadata
	std::set<DWORD> m_textureHashes; // List of hashes to textures initially assigned at loading time (future texture updates do not affect the list at this moment)

	std::shared_ptr<IExternalEXMesh> m_pExternalMesh;

	const static UINT m_maxLODs = 8;
	static CMaterialObject ms_boxMaterial;
	static CMaterialObject ms_mediaVideoRangeMaterialPlay;
	static CMaterialObject ms_mediaVideoRangeMaterialIdle;
	static CMaterialObject ms_mediaAudioRangeMaterialPlay;
	static CMaterialObject ms_mediaAudioRangeMaterialIdle;

public:
	Vector3f m_position; // position of the base object
	Vector3f m_direction; // direction where obj is facing
	Vector3f m_slide; // slide direction
	Vector3f m_positionOrig; // a copy of m_position before any changes are made through MoveForward() or Slide()
	Vector3f m_directionOrig; // a copy of m_direction before any changes are made through Rotate()
	Vector3f m_slideOrig; // a copy of m_direction before any changes are made through Rotate()
	Vector3f m_min; // AABB min point
	Vector3f m_max; // AABB max point
	float m_boundRadius;
	Vector3f m_boundSphereCenter;
	Vector3f m_minObjectSpace; // These come from server
	Vector3f m_maxObjectSpace; // These come from server
	BOOL m_culled; // Whether this object was culled during last render.

	bool m_frictionEnabled;
	int m_playerId; // the owner

	// Data that is only accessible by DynamicPlacementManager.
	class PlacementManagerState {
	public:
		PlacementManagerState() {
			for (size_t& i : m_SubListIndex) i = SIZE_MAX;
		}

	private:
		friend class DynamicPlacementManager;
		size_t m_iDynObjIndex = SIZE_MAX; // Index into dynamic object array.
		size_t m_SubListIndex[NumberOfDynObjSubListIds]; // Indices into dynamic object sub-array.
	} m_PlacementManagerState;

	// Data that is only accessible by ClientEngine.
	class ClientEngineState {
	public:
		ClientEngineState() {}

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

		const std::map<int, CTriggerObject*> GetTriggers() const { return m_triggers; }

		CTriggerObject* GetTrigger(int triggerId) {
			auto itr = m_triggers.find(triggerId);
			if (itr == m_triggers.end())
				return nullptr;

			return itr->second;
		}

		void SetTrigger(int triggerId, CTriggerObject* trigger) { m_triggers[triggerId] = trigger; }

	private:
		std::map<int, CTriggerObject*> m_triggers;

	} m_ClientEngineState;
};

} // namespace KEP
