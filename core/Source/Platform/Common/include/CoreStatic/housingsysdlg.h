///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_HOUSINGSYSDLG_H__1A7412E5_0C54_4A06_AB6C_C6C1DB9F5860__INCLUDED_)
#define AFX_HOUSINGSYSDLG_H__1A7412E5_0C54_4A06_AB6C_C6C1DB9F5860__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HousingSysDlg.h : header file
//
#include "CHousingClass.h"
#include "TextureDatabase.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "HousesPropertiesCtrl.h"
#include "PositionPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CHousingSysDlg dialog

class CHousingSysDlg : public CKEPEditDialog {
	// Construction
public:
	CHousingSysDlg(CHousingObjList*& houseDB,
		CWnd* pParent = NULL); // standard constructor
	virtual ~CHousingSysDlg();

	// accessors
	CStringA GetHouseName() const {
		return m_propHouses.GetHouseName();
	}
	bool GetSpawnHouseOnClose() const {
		return m_propHouses.GetSpawnHouseOnClose();
	}

	// modifiers
	void SetHouseName(CStringA sHouseName) {
		m_propHouses.SetHouseName(sHouseName);
	}
	void SetSpawnHouseOnClose(bool bSpawnHouseOnClose) {
		m_propHouses.SetSpawnHouseOnClose(bSpawnHouseOnClose);
	}

	TextureDatabase* GL_textureDataBase;
	CHousingObjList*& m_houseDBREF;
	LPDIRECT3DDEVICE9 g_pd3dDeviceRef;
	int m_houseSelection;
	// Dialog Data
	//{{AFX_DATA(CHousingSysDlg)
	enum { IDD = IDD_DIALOG_HOUSESYSTEM };
	CListBox m_houseDBList;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHousingSysDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListHouses(int sel);
	// Generated message map functions
	//{{AFX_MSG(CHousingSysDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBTNload();
	afx_msg void OnBTNSave();
	afx_msg void OnBUTTONaddCloseStateCollision();
	afx_msg void OnBUTTONAddExternalVisualsL1();
	afx_msg void OnBUTTONAddOpenStateCollision();
	afx_msg void OnBUTTONCreateNew();
	afx_msg void OnBUTTONPlaceHouse();
	//virtual void OnOK();
	afx_msg void OnBUTTONLoadInternalVisualSet();
	afx_msg void OnBUTTONLoadClipBox();
	afx_msg void OnBUTTONLoadInternalAreaBox();
	afx_msg void OnBUTTONAddExternalVisualsL2();
	afx_msg void OnBUTTONAddExternalVisualsL3();
	afx_msg void OnBUTTONLoadLodArea();
	afx_msg void OnBUTTONLoadLod2Area();
	afx_msg void OnBUTTONLoadLod3Area();
	afx_msg void OnBUTTONLoadDoorVisual();
	afx_msg void OnBUTTONAddLight();
	afx_msg void OnBUTTONDeleteLight();
	afx_msg void OnBUTTONReplaceSettings();
	afx_msg void OnSelchangeLISTHouseDatabase();
	afx_msg LRESULT OnPropertyChanged(WPARAM wParam, LPARAM lParam);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CHousingObj* GetSelectedHouse();
	CHousingObj* GetHouse(int index);
	void LoadHouseProperties();
	void ReplaceHouse(int selection);

	CHousingObj* m_currentHousingObj;

private:
	CCollapsibleGroup m_colHouses;
	CButton m_grpHouses;
	CHousesPropertiesCtrl m_propHouses;

	CPositionPropertiesCtrl* m_propLights;

	CKEPImageButton m_btnAddHouse;
	CKEPImageButton m_btnReplaceHouse;
	CKEPImageButton m_btnAddLight;
	CKEPImageButton m_btnDeleteLight;

	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOUSINGSYSDLG_H__1A7412E5_0C54_4A06_AB6C_C6C1DB9F5860__INCLUDED_)
