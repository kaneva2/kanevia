/******************************************************************************
 ImageEXMesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "ImageMesh.h"

class CEXMeshObj;
class CStateManagementObj;

#include "ExternalEXMesh.h"

namespace KEP {

class TextureDatabase;

// identical to BrowserEXMesh
class ImageEXMesh : public ImageMesh, public IExternalEXMesh {
public:
	ImageEXMesh(IN CStateManagementObj* stateManager, IN TextureDatabase* textureDatabase, IN IDispatcher* disp) :
			ImageMesh(disp), m_stateManager(stateManager), m_textureDatabase(textureDatabase) {}

	int Render(IN LPDIRECT3DDEVICE9 g_pd3dDevice, IN CEXMeshObj* mesh, int vertexType);

	CStateManagementObj* StateMgr() {
		return m_stateManager;
	}
	TextureDatabase* TextureDB() {
		return m_textureDatabase;
	}

private:
	CStateManagementObj* m_stateManager;
	TextureDatabase* m_textureDatabase;
};

} // namespace KEP

