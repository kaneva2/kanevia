/******************************************************************************
 RacialAdvantagePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_RACIALADVANTAGEPROPERTIESCTRL_H_)
#define _RACIALADVANTAGEPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RacialAdvantagePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CRacialAdvantagePropertiesCtrl window

class CRacialAdvantagePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CRacialAdvantagePropertiesCtrl();
	virtual ~CRacialAdvantagePropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetRacialAdvantageFactor() const {
		return m_racialAdvantageFactor;
	}
	int GetEntityID() const {
		return m_entityID;
	}

	// modifiers
	void SetRacialAdvantageFactor(float racialAdvantageFactor) {
		m_racialAdvantageFactor = racialAdvantageFactor;
	}
	void SetEntityID(int entityID) {
		m_entityID = entityID;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRacialAdvantagePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CRacialAdvantagePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitActors();

	enum {
		INDEX_ROOT = 1,
		INDEX_ACTOR,
		INDEX_ADVANTAGE_FACTOR
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	const static int BONUS_MIN = 0;
	const static int BONUS_MAX = 1000;

private:
	float m_racialAdvantageFactor;
	int m_entityID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_RACIALADVANTAGEPROPERTIESCTRL_H_)
