/******************************************************************************
 MenuControlPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_MENU_CONTROL_PROPERTIESCTRL_H_)
#define _MENU_CONTROL_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MenuControlPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include <vector>
#include <d3d9types.h>

#define WM_KEP_CONTROL_PROPERTY_CHANGED WM_USER + 2

/////////////////////////////////////////////////////////////////////////////
// CMenuControlPropertiesCtrl window

class CMenuControlPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CMenuControlPropertiesCtrl(CWnd* pParent);
	virtual ~CMenuControlPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual bool OnPropertyButtonClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	std::string GetBaseDir() const {
		return m_base_dir;
	}
	CStringA GetName() const {
		return Utf16ToUtf8(m_sName).c_str();
	}
	bool GetIsDefault() const {
		return m_bIsDefault;
	}
	bool GetDisabled() const {
		return m_bDisabled;
	}
	bool GetVisible() const {
		return m_bVisible;
	}
	unsigned int GetHotkey() const {
		return m_hotkey;
	}
	void GetPosition(POINT& pt) {
		pt = m_position;
	}
	int GetWidth() const {
		return m_width;
	}
	int GetHeight() const {
		return m_height;
	}
	int GetElementIndex() const {
		return m_elementIndex;
	}

	CStringA GetFontFaceName();
	long GetFontWeight();
	long GetFontHeight();
	bool GetFontItalic();
	bool GetFontUnderline();

	unsigned int GetAlignment() const {
		return m_horizAlign | m_vertAlign;
	}
	bool GetWordWrap() const {
		return m_bWordWrap;
	}
	D3DCOLOR GetFontColor();
	D3DCOLOR GetTextureColor();
	unsigned int GetState() const {
		return m_state;
	}
	RECT GetTextureCoords() const {
		return m_textureCoords;
	}
	CStringA GetTextureName() const {
		return Utf16ToUtf8(m_sTextureName).c_str();
	}

	CStringA GetText() const {
		return Utf16ToUtf8(m_sText).c_str();
	}
	unsigned int GetButtonGroup() const {
		return m_buttonGroup;
	}
	bool GetPassword() const {
		return m_bPassword;
	}
	int GetBorderWidth() const {
		return m_borderWidth;
	}
	int GetSpacing() const {
		return m_spacing;
	}
	D3DCOLOR GetTextColor();
	D3DCOLOR GetSelectedTextColor();
	D3DCOLOR GetSelectedBackColor();
	D3DCOLOR GetCaretColor();
	int GetDropHeight() const {
		return m_dropHeight;
	}
	int GetScrollBarWidth() const {
		return m_scrollBarWidth;
	}
	bool GetMultiSelection() const {
		return m_bMultiSelection;
	}
	bool GetDragSelection() const {
		return m_bDragSelection;
	}
	int GetLeftMargin() const {
		return m_leftMargin;
	}
	int GetRightMargin() const {
		return m_rightMargin;
	}
	int GetRowHeight() const {
		return m_rowHeight;
	}
	bool GetApplyTextureNameToAll() const {
		return m_apply_texture_name_to_all;
	}
	bool GetUseHTML() const {
		return m_bUseHTML;
	}

	// flash
	bool GetWantInput() const {
		return m_bWantInput;
	}
	bool GetSizeToFit() const {
		return m_bSizeToFit;
	}
	bool GetLoop() const {
		return m_bLoop;
	}
	CStringA GetParameters() const {
		return m_sParameters;
	}

	// modifiers
	void SetBaseDir(std::string base_dir) {
		m_base_dir = base_dir;
	}
	void SetName(CStringA sName) {
		m_sName = Utf8ToUtf16(sName).c_str();
		m_sOrigName = m_sName;
	}
	void SetIsDefault(bool bIsDefault) {
		m_bIsDefault = bIsDefault;
	}
	void SetDisabled(bool bDisabled) {
		m_bDisabled = bDisabled;
	}
	void SetVisible(bool bVisible) {
		m_bVisible = bVisible;
	}
	void SetHotkey(unsigned int hotkey) {
		m_hotkey = hotkey;
	}
	void SetPosition(POINT pt);
	void SetWidth(int width);
	void SetHeight(int height);
	void SetElementIndex(int index) {
		m_elementIndex = index;
	}
	void SetFont(CStringA sFaceName, long fontHeight, long fontWeight);
	void SetAlignment(unsigned int alignment);
	void SetWordWrap(bool bWordWrap) {
		m_bWordWrap = bWordWrap;
	}
	void SetFontColor(D3DCOLOR color);
	void SetTextureColor(D3DCOLOR color);
	void SetState(unsigned int state) {
		m_state = state;
	}
	void SetTextureCoords(RECT coords);
	void SetTextureName(CStringA sTextureName) {
		m_sTextureName = Utf8ToUtf16(sTextureName).c_str();
	}

	void SetText(CStringA sText) {
		m_sText = Utf8ToUtf16(sText).c_str();
	}
	void SetButtonGroup(unsigned int buttonGroup) {
		m_buttonGroup = buttonGroup;
	}
	void SetPassword(bool bPassword) {
		m_bPassword = bPassword;
	}
	void SetBorderWidth(int width) {
		m_borderWidth = width;
	}
	void SetSpacing(int spacing) {
		m_spacing = spacing;
	}
	void SetTextColor(D3DCOLOR color);
	void SetSelectedTextColor(D3DCOLOR color);
	void SetSelectedBackColor(D3DCOLOR color);
	void SetCaretColor(D3DCOLOR color);
	void SetDropHeight(int height) {
		m_dropHeight = height;
	}
	void SetScrollBarWidth(int width) {
		m_scrollBarWidth = width;
	}
	void SetMultiSelection(bool bMultiSelection) {
		m_bMultiSelection = bMultiSelection;
	}
	void SetDragSelection(bool bDragSelection) {
		m_bDragSelection = bDragSelection;
	}
	void SetLeftMargin(int margin);
	void SetRightMargin(int margin);
	void SetRowHeight(int height) {
		m_rowHeight = height;
	}
	void SetUseHTML(bool bUseHTML) {
		m_bUseHTML = bUseHTML;
	}

	// flash
	void SetWantInput(bool b) {
		m_bWantInput = b;
	}
	void SetSizeToFit(bool b) {
		m_bSizeToFit = b;
	}
	void SetLoop(bool b) {
		m_bLoop = b;
	}
	void SetParameters(const char* s) {
		m_sParameters = s;
	}

	// methods
	void Reset();
	void ShowItem(int index, bool bShow);
	void AddElement(const char* element, int index);
	void ClearElements();

	enum { INDEX_ROOT = 1,
		INDEX_NAME,

		INDEX_APPEARANCE_ROOT,
		INDEX_TEXT, // static
		INDEX_BORDER_WIDTH, // edit box, listbox
		INDEX_SPACING, // edit box
		INDEX_TEXT_COLOR, // edit box
		INDEX_TEXT_ALPHA, // edit box
		INDEX_SELECTED_TEXT_COLOR, // edit box
		INDEX_SELECTED_TEXT_ALPHA, // edit box
		INDEX_SELECTED_BACK_COLOR, // edit box
		INDEX_SELECTED_BACK_ALPHA, // edit box
		INDEX_CARET_COLOR, // edit box
		INDEX_CARET_ALPHA, // edit box
		INDEX_DROP_HEIGHT, // combo box
		INDEX_SCROLLBAR_WIDTH, // combo box, listbox
		INDEX_MARGINS, // listbox
		INDEX_LEFT_MARGIN, // listbox
		INDEX_RIGHT_MARGIN, // listbox
		INDEX_ROW_HEIGHT, // listbox
		INDEX_USE_HTML, // static

		INDEX_ELEMENTS_ROOT,
		INDEX_ELEMENTS,
		INDEX_FONT,
		INDEX_HORIZONTAL_ALIGNMENT,
		INDEX_VERTICAL_ALIGNMENT,
		INDEX_WORD_WRAP,
		INDEX_TEXTURE_NAME,
		INDEX_TEXTURE_COORDS,
		INDEX_TEXTURE_COORD_LEFT,
		INDEX_TEXTURE_COORD_TOP,
		INDEX_TEXTURE_COORD_RIGHT,
		INDEX_TEXTURE_COORD_BOTTOM,

		INDEX_STATES_SUB_CAT,
		INDEX_STATE,

		INDEX_FONT_COLOR,
		INDEX_FONT_ALPHA,
		INDEX_TEXTURE_COLOR,
		INDEX_TEXTURE_ALPHA,

		INDEX_BEHAVIOR_ROOT,
		INDEX_DEFAULT,
		INDEX_DISABLED,
		INDEX_VISIBLE,
		INDEX_HOTKEY,

		INDEX_POSITION_ROOT,
		INDEX_POSITION,
		INDEX_X_POSITION,
		INDEX_Y_POSITION,
		INDEX_SIZE,
		INDEX_WIDTH,
		INDEX_HEIGHT,

		INDEX_MISC_ROOT,
		INDEX_GROUP, // radio button
		INDEX_PASSWORD, // edit box
		INDEX_MULTI_SELECTION, // listbox
		INDEX_DRAG_SELECTION, // listbox

		// flash options
		INDEX_WANT_INPUT,
		INDEX_SIZE_TO_FIT,
		INDEX_LOOP,
		INDEX_PARAMETERS,

		NUM_ITEMS
	};

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMenuControlPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CMenuControlPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	static const int ALPHA_MIN = 0;
	static const int ALPHA_MAX = 255;

	void LoadHorizontalAlignments();
	void LoadVerticalAlignments();
	void LoadStates();
	void SetColor(D3DCOLOR color, COLORREF& color_ref, unsigned int& alpha);

private:
	CWnd* m_pParent;

	CStringW m_sName;
	CStringW m_sOrigName; // since name is the identifier, we need the original in order to update the name

	bool m_bIsDefault;
	bool m_bDisabled;
	bool m_bVisible;

	unsigned int m_hotkey;

	POINT m_position;
	int m_width;
	int m_height;

	CStringW m_sText;
	unsigned int m_buttonGroup;
	int m_borderWidth;
	int m_spacing;
	COLORREF m_textColor;
	COLORREF m_selectedTextColor;
	COLORREF m_selectedBackColor;
	COLORREF m_caretColor;
	unsigned int m_textAlpha;
	unsigned int m_selectedTextAlpha;
	unsigned int m_selectedBackAlpha;
	unsigned int m_caretAlpha;
	int m_dropHeight;
	int m_scrollBarWidth;
	int m_leftMargin;
	int m_rightMargin;
	int m_rowHeight;
	bool m_bUseHTML;

	CFont m_font;
	unsigned int m_horizAlign;
	unsigned int m_vertAlign;
	bool m_bWordWrap;
	unsigned int m_state;
	COLORREF m_fontColor;
	COLORREF m_textureColor;
	unsigned int m_fontAlpha;
	unsigned int m_textureAlpha;
	CStringW m_sTextureName;
	RECT m_textureCoords;
	bool m_apply_texture_name_to_all; // not a property, but set when selecting a texture name

	int m_elementIndex;

	bool m_bPassword;
	bool m_bMultiSelection;
	bool m_bDragSelection;

	// flash added
	bool m_bWantInput,
		m_bSizeToFit,
		m_bLoop;
	CStringA m_sParameters;

	typedef std::vector<std::string> ElementTagVector;
	typedef ElementTagVector::iterator ElementTagIter;
	ElementTagVector m_elementTags;

	std::string m_base_dir;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_MENU_CONTROL_PROPERTIESCTRL_H_)
