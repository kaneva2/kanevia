///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "Core/Util/HighPrecisionTime.h"
#include "common\include\CoreStatic\CBoneAnimationNaming.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class IMemSizeGadget;

class CDamagePathObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CDamagePathObj, VERSIONABLE_SCHEMA | 1)

private:
	int m_enableOnAnimationIndex;

public:
	CDamagePathObj();

	//functions
	int m_boneIndex;
	Vector3f m_lastPosition;
	float m_radius;
	int m_damageValue;
	BOOL m_contactMade;
	TimeMs m_damageDelay; //minimum wait on redamaging in swing
	TimeMs m_damageDelayStamp; //last hit
	int m_damageChannel;
	int m_criteriaForSkillUse; //skill type
	int m_damageSpecific;

	// Enable on type+version instead of animation index -- this is for stock assets.
	// To make this UGCable in the future, an additional member/interface would be needed to allow invoking animation by GLID.
	eAnimType m_enableOnAnimationType;
	int m_enableOnAnimationVersion;

	void Clone(CDamagePathObj** clone);
	void Serialize(CArchive& ar);

	int GetLegacyAnimationIndex() const {
		return m_enableOnAnimationIndex;
	}
	void SetAnimationInfo(eAnimType animType, int animVer);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CDamagePathObjList : public CObList {
public:
	CDamagePathObjList(){};
	void Clone(CDamagePathObjList** clone);
	void SafeDelete();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CDamagePathObjList)
};

} // namespace KEP
