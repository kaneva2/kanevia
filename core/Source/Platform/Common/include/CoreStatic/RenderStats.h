///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>

struct RenderStats {
	enum {
		OBJTYPE_ENTITY,
		OBJTYPE_WORLD,
		OBJTYPE_DYNAMIC
	};

	enum {
		STALL_TIME_MSEC = 150, /* Anything over this long is a stall */
		FREEZE_TIME_MSEC = 1000, /* Anything over this long is a freeze */
		NUM_FRAMES_COLLECT = 300 /* Number of frames to spend collecting stats */
	};

	LARGE_INTEGER beginTime; /* QueryPerformanceCounter() when we started recording */
	unsigned long duration, /* Computed after collection completes */
		numFrames, /* Number of frames spent collecting data */
		swapTimeUSEC, /* How long we spent waiting for buffer swaps (in microseconds) */
		renderTimeUSEC,
		frameTimeUSEC,
		objectsPresent[3], /* Number of objects in zone */
		objectsRendered[3],
		trisRendered[3], /* Tris rendered for [entity, world, dynamic] */
		batchesRendered[3],
		numStalls, /* Frames taking > STALL_TIME to render */
		numFreezes, /* Frames taking > FREEZE_TIME to render (a freeze counts as a stall as well) */
		numDeviceLoss, /* How many times we lost the device -- so we can ignore these stats basically */
		numDeviceReset;

	RenderStats() {
		Reset();
	}

	unsigned long TotalTrisRendered() const {
		return trisRendered[0] + trisRendered[1] + trisRendered[2];
	}

	unsigned long TotalObjectsPresent() const {
		return objectsPresent[0] + objectsPresent[1] + objectsPresent[2];
	}

	unsigned long TotalObjectsRendered() const {
		return objectsRendered[0] + objectsRendered[1] + objectsRendered[2];
	}

	unsigned long TotalBatchesRendered() const {
		return batchesRendered[0] + batchesRendered[1] + batchesRendered[2];
	}

	void Reset() {
		beginTime.QuadPart = 0;

		duration = 0;
		numFrames = 0;

		objectsPresent[0] = 0;
		objectsPresent[1] = 0;
		objectsPresent[2] = 0;

		objectsRendered[0] = 0;
		objectsRendered[1] = 0;
		objectsRendered[2] = 0;

		trisRendered[0] = 0;
		trisRendered[1] = 0;
		trisRendered[2] = 0;

		batchesRendered[0] = 0;
		batchesRendered[1] = 0;
		batchesRendered[2] = 0;

		numStalls = 0;
		numFreezes = 0;

		swapTimeUSEC = 0;
		frameTimeUSEC = 0;
		renderTimeUSEC = 0;

		numDeviceLoss = 0;
		numDeviceReset = 0;
	}
};

// Helper functions for dealing with values returned from QueryPerformanceCounter

inline unsigned long QPCDiffUSEC(LARGE_INTEGER t1, LARGE_INTEGER t0, LARGE_INTEGER freq) {
	return (unsigned long)((t1.QuadPart - t0.QuadPart) * 1000000 / (freq.QuadPart));
}

inline unsigned long QPCDiffMSEC(LARGE_INTEGER t1, LARGE_INTEGER t0, LARGE_INTEGER freq) {
	return (unsigned long)((t1.QuadPart - t0.QuadPart) * 1000 / (freq.QuadPart));
}
