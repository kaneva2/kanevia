///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "InstanceId.h"
#include "matrixarb.h"
#include "KEPObList.h"

namespace KEP {

class CSafeZoneObj : public CObject, public GetSet {
	DECLARE_SERIAL(CSafeZoneObj);

public:
	CSafeZoneObj();

	CStringA m_zoneName;
	ChannelId m_channel;
	Vector3f m_boundingBoxMax;
	Vector3f m_boundingBoxMin;
	BOOL m_rebirthableZone;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CSafeZoneObjList : public CKEPObList {
public:
	CSafeZoneObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CSafeZoneObjList);

	CSafeZoneObj* GetSafeZoneByPosition(const Vector3f& position, const ChannelId& channel);
	BOOL BoundBoxPointCheck(const Vector3f& point, const Vector3f& min, const Vector3f& max);
};

} // namespace KEP