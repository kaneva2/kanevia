///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#pragma once

#include "Core/Math/Vector.h"

namespace KEP {

class Intersect {
public:
	static bool RayAABBCheck(
		const Vector3f& boxMin,
		const Vector3f& boxMax,
		const Vector3f& rayOrigin,
		const Vector3f& rayDirNorm,
		Vector3f& coord,
		Vector3f& normal,
		BOOL inIsOut);
};

} // namespace KEP