///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class CPendingCommandObj : public CObject {
public:
	CPendingCommandObj();

	int m_command;
	int m_done;
	TimeMs m_timeCreated;
	TimeMs m_timeToExec;
	int m_register0;
	int m_register1;
	CStringA m_specialString;
};

class CPendingCommandObjList : public CObList {
public:
	CPendingCommandObjList(){};
	CPendingCommandObj* CallBack();
	BOOL AddCommand(int command,
		int register0,
		int register1,
		long duration,
		CStringA specialString);
	void SafeDelete();
};

} // namespace KEP