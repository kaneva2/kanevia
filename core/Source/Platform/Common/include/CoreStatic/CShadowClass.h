///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CStructuresMisl.h"

namespace KEP {

class CCollisionObj;
class CEXMeshObj;
struct CPolyRetStruct;
class IMemSizeGadget;

class CShadowClass : public CObject, public GetSet {
protected:
	CShadowClass() {}

	DECLARE_SERIAL(CShadowClass);

public:
	int m_type; //0=cut model shadows    1=mat texture shadow
	Vector3f m_lastNormal;
	BOOL m_valid;
	Vector3f m_wldPosition;

public:
	~CShadowClass();

	CShadowClass(float radius,
		float collRadius,
		float extentUp,
		float extentDown,
		bool scale,
		int textureIndex,
		const std::string& textureSearchName);
	void Serialize(CArchive& ar);
	void Clone(CShadowClass** clone);

	void SafeDelete();

	void Calculate(const Vector3f& objectPos, CCollisionObj* collisionObject);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	void MakeCollisionPoly();
	void GetCollidedPolys(CCollisionObj* collisionObject);
	void MakeCollisionScalePoly();
	void GetCollidedScalePolys(CCollisionObj* collisionObject);
	void ScaleTexture();
	void AddTextureCoordinates();

	ABVERTEX1L ToVertex(Vector3f pt);
	BOOL DuplicateTri(CPolyRetStruct* Polys, int cPolys, const CPolyRetStruct& TestPoly);

public: //VARIABLES
	std::string m_textureSearchName;
	DWORD m_textureIndex;

	LPDIRECT3DDEVICE9 m_pd3dDevice8; //Direct 3D Device 8.0
	Vector3f m_offset; //render offset

	Vector3f m_objectPos; //caster position
	Vector3f m_oldPos; //caster's old position
	float m_radius; //shadow radius
	float m_collisionRadius; //collision radius
	float m_originalRadius; //shadow radius
	float m_extentUp; //collision extention upwards
	float m_extentDown; //collision extention  downwards

	BOOL m_scaleTexture;
	BOOL m_beginScaling;
	float m_intersectY;

	D3DCOLORVALUE m_color;

	Vector3f* m_normals; //normals for shadow polys

	ABVERTEX1L* m_vertices; //vertices for shadow
	int m_numvertices; //number of shadow vertices

	Vector3f* m_collisionVects; //"vertices" for collision object
	int m_numcollisionVects; // number of collision "vertices"

	Vector3f* m_collisionScaleVects; //"vertices" for collision object
	int m_numcollisionScaleVects; // number of collision "vertices"

	CEXMeshObj* m_shadowMesh;

	BOOL m_firstTime;

	DECLARE_GETSET
};

} // namespace KEP