///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "CSkeletonObject.h"
#include "ctextureDBClass.h"
#include "CElementsClass.h"
#include "CSkillClass.h"
#include "CStateManagementClass.h"

#include "TabGroup.h"
#include "InPlaceEditing.h"

#pragma once

class CHiarchyMesh;

class CGlobalMaterialShaderList;

class CSkeletalAnimationDlg : public CDialog {
	// Construction
public:
	CSkeletalAnimationDlg(CWnd* pParent = NULL); // standard constructor
	CSkeletonObject* m_skeletonReferencePtr;
	IDirectSound8* directSoundRef;
	BOOL m_loopableState;
	TextureDatabase* GL_textureDataBase;
	CElementObjList* m_elementDBREF;
	CSkillObjectList* m_skillDatabaseREF;
	CGlobalMaterialShaderList* m_globalShaderRef;
	CStateManagementObj* m_renderStateObjectRef;
	LPDIRECT3DDEVICE9 g_pd3dDevice;
	// Dialog Data
	//{{AFX_DATA(CSkeletalAnimationDlg)
	enum { IDD = IDD_DIALOG_SKELETALANIMATIONEDITOR };
	CComboBox m_dmpSkillTypeUse;
	CComboBox m_elementsAvailable;
	CButton m_secondaryOverridable;
	CListBox m_fireQueDB;
	CListBox m_deformableCfgs;
	CButton m_emitPtUseRange;
	CListBox m_listParticleEvents;
	CListBox m_dPathList;
	CListBox m_listBDCSpheres;
	CComboBox m_csBoneSel;
	CButton m_checkEnableSphereCalc;
	CListBox m_listDamageSpheres;
	CListBox m_listSoundEvents;
	CListBox m_listHiarchlMeshs;
	CListBox m_listDeformableMeshs;
	CListBox m_listBones;
	CListBox m_listDeformableCfgMats;
	CListBox m_animationsList;
	CComboBox m_comboParticles;
	CStringA m_animationType;
	CComboBox m_comboAnimations;
	CStringA m_fileName;
	int m_requiredDuration;
	int m_animVersion;
	float m_damageSphereRadius;
	float m_damageShphereAmplification;
	float m_sphereCalcRadius;
	float m_bdSphereRadius;
	CStringA m_dsphereComment;
	float m_dPathRadius;
	int m_dPathValue;
	int m_dPathAnimationTrace;
	int m_ptStartRange;
	int m_ptEndRange;
	int m_curCfgDefSpawn;
	int m_fireQueStartRange;
	int m_fireQueEndRange;
	int m_fireQueMissileIndex;
	float m_fireQueZlocalAdjust;
	float m_fireQueXpAdjust;
	float m_fireQueYpAdjust;
	int m_fps;
	float m_bonePosX;
	float m_bonePosY;
	float m_bonePosZ;
	int m_damageSpecific;
	float m_lookAxisX;
	float m_lookAxisY;
	float m_lookAxisZ;
	float m_sideAxisX;
	float m_sideAxisY;
	float m_sideAxisZ;
	int m_animGLID;
	UINT64 m_animHash;
	//}}AFX_DATA
	CButton m_checkPublicCfg;
	CButton m_checkExpanded;
	CComboBox m_projectileList;
	CEdit m_renameBox;

	float m_baseAccuracy;

public:
	virtual BOOL Create(const char* lpszClassName, const char* lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	void AnimationInlist(int sel);
	void InlistBones();
	void InListSoundEvents(int sel);
	CStringA ConvertLongToString(int number);
	void InListDeformableMesh(int sel = -1);
	void InListHiarchleMesh();
	void InListDamageSpheres(int sel);
	CStringA FloatToStingCnv(float number);
	void InListBDCSpheres(int sel);
	void InListDPathList(int sel);
	void InListParticleEventsList(int sel);
	void InListCfgs(int sel);
	void InListMeshMat(int sel);
	void InListFireQues(int sel);
	void InitParticleList();
	void InListProjectiles(int sel);
	CStringA AnimToName(int id);
	int AnimToNumber(const char* animname);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnOptimizeAll();
	afx_msg void SpareButton();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	afx_msg void OnCheckLoopable();
	afx_msg void OnSelchangeListAnimations();
	afx_msg void OnButtonBrowse();
	afx_msg void OnButtonSetSoundRange();
	afx_msg void OnBUTTONDeleteSoundEvent();
	afx_msg void OnButtonSetboneFocus1();
	afx_msg void OnBUTTONDeformableMaterial();
	afx_msg void OnBUTTONHiarchleMaterial();
	afx_msg void OnBUTTONReplaceValues();
	afx_msg void OnBUTTONAddLSphere();
	afx_msg void OnBUTTONDeleteLSphere();
	afx_msg void OnBUTTONReplaceLCSphere();
	afx_msg void OnSelchangeLISTLocationSpheres();
	afx_msg void OnChangeEDITsphereCalcRadius();
	afx_msg void OnCHECKEnableSphereCollisionSys();
	afx_msg void OnSelchangeCOMBOCSBoneSel();
	afx_msg void OnBUTTONReplaceBDSphere();
	afx_msg void OnBUTTONDeleteBDSphere();
	afx_msg void OnBUTTONAddBDSphere();
	afx_msg void OnSelchangeLISTBodyColLocationSpheres();
	afx_msg void OnBUTTONdPathAdd();
	afx_msg void OnBUTTONdPathDelete();
	afx_msg void OnBUTTONdPathReplace();
	afx_msg void OnSelchangeLISTDamagePathTrackList();
	afx_msg void OnBUTTONaddParticleEvent();
	afx_msg void OnBUTTONdeleteParticleEvent();
	afx_msg void OnBUTTONreplaceParticleEvent();
	afx_msg void OnSelchangeLISTParticleEvents();
	afx_msg void OnCHECKUseRange();
	afx_msg void OnBUTTONunsetBoneLook();
	afx_msg void OnBUTTONunsetSideStep();
	afx_msg void OnButtonSetPitchAxis();
	afx_msg void OnButtonSetYawAxis();
	afx_msg void OnBUTTONSETBONESetSideStep();
	afx_msg void OnBUTTONcharona();
	afx_msg void OnBUTTONset();
	afx_msg void OnBUTTONlodHiarchy();
	afx_msg void OnBUTTONaddDeformableCfg();
	afx_msg void OnBUTTONsetLodDefCfg();
	afx_msg void OnBUTTONAddKDM();
	afx_msg void OnBUTTONDelKDM();
	afx_msg void OnBUTTONCore();
	afx_msg void OnBUTTONRenameKDM();
	afx_msg void OnBUTTONRenameConfig();
	afx_msg void OnBUTTONImportSections();
	afx_msg void OnBUTTONUpKDM();
	afx_msg void OnBUTTONDownKDM();
	afx_msg void OnBUTTONUpDim();
	afx_msg void OnBUTTONDownDim();
	afx_msg void OnBUTTONDeformableCfgMaterial();
	afx_msg void OnSelchangeListDeformablemeshlist();
	afx_msg void OnDeltaposSPINdefSpawnCfg(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBUTTONdeleteDeformableCfg();
	afx_msg void OnBUTTONaddDeformableMaterial();
	afx_msg void OnBUTTONdeleteDeformableMaterial();
	afx_msg void OnCHECKsecondaryOverridable();
	afx_msg void OnSelchangeLISTBones();
	afx_msg void OnBUTTONSetCenterOfMassLink();
	afx_msg void OnBUTTONUnsetCenterOfMassLink();
	afx_msg void OnBUTTONEditSoundEvent();
	afx_msg void OnBUTTONReplace();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBUTTONDeleteAllAnim();
	afx_msg void OnRenameBoxLoseFocus();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckPubliccfg();
	afx_msg void OnBnClickedCheckExpandcfg();
	afx_msg void OnLbnSelchangeListDeformablecfgs();
	afx_msg void OnBnClickedButtonsetlodset();

private:
	void ChangeDialogSize();

	void GetSelectedRigidMesh(CBoneObject** bone, CHiarchyMesh** pHMesh);

	CButton m_grpContainer;

	CTabGroup m_tabAnimation;
	CButton m_grpAnimation;

	CTabGroup m_tabMesh;
	CButton m_grpMesh;

	CTabGroup m_tabSkeleton;
	CButton m_grpSkeleton;

	CTabGroup m_tabCollision;
	CButton m_grpCollision;

	CTabGroup m_tabEvents;
	CButton m_grpEvents;

	InPlaceEditing m_ipEditing;
};
