///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

#include "mmstream.h"
#include "amstream.h"
#include "ddstream.h"
#include "dsound.h"
#include <mmsystem.h>
#include "matrixarb.h"
#include "KEPObList.h"

namespace KEP {

class CSoundSampleObj : public CObject, public GetSet {
	DECLARE_SERIAL(CSoundSampleObj);

public:
	CSoundSampleObj();
	void Clone(CSoundSampleObj** clone);

	IDirectSoundBuffer* m_soundBuffer;
	IDirectSound3DBuffer* m_3dBuffer;
	CStringA m_fileName;
	BOOL m_loopable;
	float m_soundFactor;
	BOOL m_selfDeleteing;

	void Clone(CSoundSampleObj** clone, IDirectSound8* directSoundPtr);
	BOOL Initialize(IDirectSound8* directSoundPtr);
	BOOL InitFromFile(IDirectSound8* directSoundPtr, CStringA fileName);
	IDirectSoundBuffer* DSLoad3DSoundBuffer(IDirectSound8* pDS, const char* lpName);
	BOOL DSGetWaveFile(HMODULE hModule, const char* lpName, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize, void** ppvBase);
	BOOL DSFillSoundBuffer(IDirectSoundBuffer* pDSB, BYTE* pbWaveData, DWORD cbWaveSize);
	BOOL DSParseWaveResource(void* pvRes, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize);
	BOOL DSGetWaveResource(HMODULE hModule, const char* lpName, WAVEFORMATEX** ppWaveHeader, BYTE** ppbWaveData, DWORD* pcbWaveSize);
	void Play(BOOL looping);
	BOOL CloneSoundObjects(IDirectSoundBuffer* baseSoundBuffer,
		IDirectSound3DBuffer* buffer3DControl,
		IDirectSoundBuffer** newSndBuf,
		IDirectSound3DBuffer** New3DSndBuf,
		IDirectSound8* DirSnd);
	BOOL UpdatePosition(Vector3f& positionalUpdate);
	BOOL SetToDelete();
	void Serialize(CArchive& ar);
	void SafeDelete();

	DECLARE_GETSET
};

class CSoundSampleObjList : public CObList {
public:
	CSoundSampleObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CSoundSampleObjList);

	void CallBack();
};

class CSoundBankObj : public CObject {
	DECLARE_SERIAL(CSoundBankObj);

public:
	CSoundBankObj();

	CSoundSampleObj* m_primary;
	CSoundSampleObjList* m_soundChain;
	int m_id;

	CSoundSampleObj* Play(BOOL looping, IDirectSound8* directSoundPtr);

	CSoundSampleObj* Play(BOOL looping,
		IDirectSound8* directSoundPtr,
		Vector3f& initialPosition,
		float& soundFactor,
		float clipRadius);
	BOOL InitPrimary(CStringA fileName, IDirectSound8* directSoundPtr);
	void Serialize(CArchive& ar);
	void SafeDelete();
};

class CSoundBankList : public CKEPObList {
public:
	CSoundBankList(){};

	DECLARE_SERIAL(CSoundBankList);

	void SafeDelete();

	IDirectSound8* m_directSoundPtr;

	void Save(CStringA fileName);
	void Initialize(IDirectSound8* directSoundPtr);

	CSoundSampleObj* PlayByID(int id, BOOL looping);

	CSoundSampleObj* PlayByID(int id,
		BOOL looping,
		Vector3f& initialPosition,
		float& soundFactor,
		float clipRadius);
	BOOL CreateSoundBank(CStringA fileName);
	int GetCurrentRuntimeSoundCount();
	void CallBack();

	int GetID(CStringA filename);

protected:
	int GetUniqueID();
};

} // namespace KEP