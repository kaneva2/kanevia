///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcoll.h"
#include "ISerializableObj.h"

namespace KEP {

class CKEPObject : public CObject, public ISerializableObj {
	DECLARE_SERIAL(CKEPObject);

public:
	CKEPObject(void);
	~CKEPObject(void);

	void Serialize(CArchive& ar);
	/**
	*The path of the file this object was lasted serialized to or deserialized from
	**/
	FileName GetLastSavedPath() const {
		return m_lastSavedPath;
	}

	/**
	*Whether the object's state has changed and requires saving
	**/
	BOOL IsDirty() const {
		return m_dirty;
	}

	/**
	*It will be better if this state is updated automatically, but having public member variables
	*in classes derive from CKEPObject makes it impossible.
	*So, to make it work, SetDirty() needs to be called manually whenever the object is changed
	**/
	void SetDirty(BOOL dirty = TRUE) {
		m_dirty = dirty;
	}

private:
	FileName m_lastSavedPath;
	BOOL m_dirty;
};

} // namespace KEP