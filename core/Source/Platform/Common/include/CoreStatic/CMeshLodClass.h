///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CEXMeshClass.h"
#include "CStructuresMisl.h"
#include "matrixarb.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {
class ReMesh; // RENDERENGINE INTERFACE
class CStaticMeshObj;

class CMeshLODObject : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CMeshLODObject, VERSIONABLE_SCHEMA | 2) // Version 1: ReMesh; 2: m_meshLodLevel not streamed

private:
	Matrix44f m_matrix;
	ABBOX m_boundBox;

public:
	CMeshLODObject();
	CMeshLODObject(CEXMeshObj* apExMesh, const Vector3f& aPivot,
		const CMaterialObject* apMaterial,
		const Matrix44f& aMatrix, const ABBOX& aBoundingBox,
		float aActivationDist, const ABBOX* apActivationBox,
		BOOL aBakeVertices = TRUE,
		BOOL aOptimizeMesh = TRUE);

	void init();

	float m_distance; //greater than activate it
	ABBOX m_boxOption;

	//@@Todo: remove x_backward_compat_meshLodLevel completely [Yanfeng 11/2008]
	CEXMeshObj* x_backward_compat_meshLodLevel; // For backward compatibility only, will be removed
	// once it's phased out from CReferenceObject/CWldObject,
	// new code should not refer to this member
	CMaterialObject* m_material;

	ReMesh* m_ReMesh;
	void SetReMesh(ReMesh* mesh) {
		m_ReMesh = mesh;
	}
	ReMesh* GetReMesh() {
		return m_ReMesh;
	}

	void Clone(CMeshLODObject** copy, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void SafeDelete();
	void Serialize(CArchive& ar);

	void SetMesh(CEXMeshObj* newMesh, const Vector3f& pivot, BOOL aBakeVertices = TRUE, BOOL aOptimizeMesh = TRUE);
	void SetMaterial(const CMaterialObject* newMaterial);
	void SetActivationDistance(float aDist) {
		m_distance = aDist;
	}
	void SetActivationBox(const ABBOX& box) {
		m_boxOption = box;
	}
	void ClearActivationBox() {
		memset(&m_boxOption, 0, sizeof(m_boxOption));
	}
	void SetMatrix(const Matrix44f& matrix) {
		m_matrix = matrix;
	}
	void SetBoundingBox(const ABBOX& box) {
		m_boundBox = box;
	}
	void ClearBoundingBox() {
		memset(&m_boundBox, 0, sizeof(m_boundBox));
	}

	CMaterialObject* GetMaterial() {
		return m_material;
	}
	float GetActivationDistance() const {
		return m_distance;
	}
	const ABBOX& GetActivationBox() const {
		return m_boxOption;
	}
	const Matrix44f& GetMatrix() const {
		return m_matrix;
	}
	const ABBOX& GetBoundingBox() const {
		return m_boundBox;
	}

	DECLARE_GETSET
};

class CMeshLODObjectList : public CObList {
public:
	CMeshLODObjectList();

	int m_lodType; //0=distance from center 1=box, 2=distance overlap fade
	float m_optionalOverlapDist;

	void SafeDelete();

	void Clone(CMeshLODObjectList** copy, LPDIRECT3DDEVICE9 g_pd3dDevice);

	//	BOOL AddLOD(int type,
	//	   const ABBOX * box,
	//	   float distance,
	//	   CEXMeshObj* pMesh,	// we will pass in mesh directly instead of file name [Yanfeng 09/2008]
	//	   CMaterialObject * materialObject,
	//	   BOOL referenceObject = FALSE);

	CMeshLODObject* GetLOD(int lodLevel);
	CMeshLODObject* GetLastLOD();

	//! \brief Extend current LOD chain to specified LOD level. Copy mesh from original max LOD level while extending.
	//! \param lodLevel LOD level to extend to
	//! \param distanceIncrement Activation distance increment. For distance-based, add increment to distance of last LOD. For box-based, expand box by increment.
	//! \param pBaseMesh Base mesh as defined in LOD0.
	//! \param pBaseMaterial Base material as defined in LOD0
	//! \param baseMatrix Transformation matrix as defined in LOD0.
	//! \param baseBoundingBox Bounding box as defined in LOD0.
	BOOL ExtendLODChain(int lodLevel, float distanceIncrement, ReMesh* pBaseMesh, const CMaterialObject* pBaseMaterial, const Matrix44f& baseMatrix, const ABBOX& baseBoundingBox);

	//! \brief Insert a new LOD into LOD chain at specified level.
	//! \param desiredLodLevel Desired LOD level to insert into. -1=append.
	//! \param lodObj LOD data.
	//! \return Actual LOD level of the newly inserted LOD.
	//!
	//! Insert a new LOD into LOD chain at specified level. Existing LODs at or above specified level will be moved up for one level.
	//! If desiredLodLevel is -1 or beyond current highest, append one to the end regardless the number specified.
	int InsertLOD(int desiredLodLevel, CMeshLODObject* lodObj);

	//! \brief Delete an existing LOD from LOD chain.
	//! \param lodLevel LOD level to be deleted.
	//!
	//! Delete an existing LOD from LOD chain. Existing LODs above specified level will be moved down for one level.
	void DeleteLOD(int lodLevel);

	//! \brief Set mesh data for an existing LOD. Extend LOD chain if needed.
	//! \param lodLevel LOD level to be replaced.
	void SetLODMesh(int lodLevel, CEXMeshObj* newMesh, BOOL bExtendLODChain, CStaticMeshObj* pParent = NULL, BOOL aBakeVertices = TRUE, BOOL aOptimizeMesh = TRUE);

	//! \brief Update LOD settings including activation type, activation distance and/or activation box.
	//! \param lodLevel LOD level to be updated.
	void UpdateLODSettings(int lodLevel, float activationDist, ABBOX* activationBox);

	//! \brief Change LOD type for the entire lod chain
	void UpdateLODType(int lodType) {
		m_lodType = lodType;
	}

	CStringA GetFileExt(CStringA currentName);

	BOOL BoundBoxPointCheck(Vector3f point, ABBOX box);

	//void RemoveLod(int index);

	CMeshLODObject* GetLodVisualExt(const Vector3f& viewPoint,
		const Vector3f& objPoint,
		float currentFov,
		CMeshLODObject** refObj2,
		float* obj1AlpaOpt,
		float* obj2AlpaOpt,
		ReMesh** reMesh);

	CMeshLODObject* GetLodVisual(const Vector3f& viewPoint,
		const Vector3f& objPoint,
		float currentFov,
		ReMesh** reMesh);

	CMeshLODObject* GetLodVisualExt(const Vector3f& viewPoint,
		const Vector3f& objPoint,
		float currentFov,
		CMeshLODObject** refObj2,
		float* obj1AlpaOpt,
		float* obj2AlpaOpt,
		UINT& index);

	CMeshLODObject* GetLodVisual(const Vector3f& viewPoint,
		const Vector3f& objPoint,
		float currentFov,
		UINT& index);

	void SetMatrixForAllLODs(const Matrix44f& matrix);
	void SetBoundingBoxForAllLODs(const ABBOX& bbox);
	void Serialize(CArchive& ar);

	DECLARE_SERIAL_SCHEMA(CMeshLODObjectList, VERSIONABLE_SCHEMA | 0)
};

} // namespace KEP
