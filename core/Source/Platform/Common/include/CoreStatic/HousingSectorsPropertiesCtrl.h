/******************************************************************************
 HousingSectorsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_HOUSING_SECTORS_PROPERTIESCTRL_H_)
#define _HOUSING_SECTORS_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HousingSectorsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CHousingSectorsPropertiesCtrl window

class CHousingSectorsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CHousingSectorsPropertiesCtrl();
	virtual ~CHousingSectorsPropertiesCtrl();

public:
	enum { INDEX_ROOT = 1,
		INDEX_HOUSING_SECTOR_NAME,
		INDEX_WORLD_CHANNEL_ID,
		INDEX_MIN_DISTANCE
	};

	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetHousingSectorName() const {
		return Utf16ToUtf8(m_sHousingSectorName).c_str();
	}
	int GetWorldChannelID() const {
		return m_worldChannelID;
	}
	float GetMinDistance() const {
		return m_minDistance;
	}

	// modifiers
	void SetHousingSectorName(CStringA sHousingSectorName) {
		m_sHousingSectorName = Utf8ToUtf16(sHousingSectorName).c_str();
	}
	void SetWorldChannelID(int worldChannelID) {
		m_worldChannelID = worldChannelID;
	}
	void SetMinDistance(float minDistance) {
		m_minDistance = minDistance;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHousingSectorsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CHousingSectorsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	void InitChannels();

private:
	CStringW m_sHousingSectorName;
	int m_worldChannelID;
	float m_minDistance;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_HOUSING_SECTORS_PROPERTIESCTRL_H_)
