///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "InputSystemTypes.h"
#include "Core/Util/Parameter.h"
#include "Core/Util/EnumStringMap.h"

namespace KEP {

// Windows does not define a separate virtual key code for this key.
// We can detect when it is pressed in WM_KEYDOWN messages, but we can not determine its current state in GetKeyboardState() calls.
// If necessary, we can use DirectInput to detect this key's state.
const int KEP_VK_NUMPADRETURN = 0x100;

struct KeyDefinition {
	const char* m_pszName;
	int m_iKeyCode;
};

class KeyCodeMap {
public:
	template <size_t N>
	KeyCodeMap(const KeyDefinition (&aKeyDefs)[N]) {
		Init(aKeyDefs, N);
	}

	int KeyNameToKeyCode(const char* pszKeyName) const; // Returns -1 for invalid key name.
	bool KeyNameToKeyCode(const char* pszKeyName, Out<int> keyCode) const;
	const char* KeyCodeToKeyName(int iKeyCode, const char* pszResultForInvalidKeyCode = nullptr) const;
	void GetAllKeyCodes(Out<std::vector<int>> aKeyCodes) const;
	void GetAllKeyNames(Out<std::vector<const char*>> aKeyNames) const;

private:
	void Init(const KeyDefinition* pKeyDefinitions, size_t nKeyDefinitions);

private:
	struct StringCompareIgnoreCase {
		bool operator()(const char* left, const char* right) const {
			return stricmp(left, right) < 0;
		}
	};

	/*
	typedef
	boost::multi_index_container<
		KeyDefinition,
		boost::multi_index::indexed_by<
			boost::multi_index::ordered_unique<
				boost::multi_index::member<KeyDefinition, const char*, &KeyDefinition::m_pszName>, StringCompareIgnoreCase>,
			boost::multi_index::ordered_non_unique<
				boost::multi_index::member<KeyDefinition, int, &KeyDefinition::m_iKeyCode>
			>>>
		MapType;
	MapType m_Map;
	*/
	struct MapType;
	std::unique_ptr<MapType> m_pMap;
};

const KeyCodeMap* GetKeyCodeMap(eKeyCodeType keyCodeType);
const std::vector<const char*>* KeyGroupNameToKeyNames(const char* pszKeyGroupName);

} // namespace KEP
