/******************************************************************************
 SpawnGeneratorPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SPAWN_GENERATORPROPERTIESCTRL_H_)
#define _SPAWN_GENERATORPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpawnGeneratorPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CSpawnGeneratorPropertiesCtrl window

class CSpawnGeneratorPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSpawnGeneratorPropertiesCtrl(CDialogBar* cDialogBar);
	virtual ~CSpawnGeneratorPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	BOOL GetEnableSystem() const {
		return m_enableSystem;
	}

	// modifiers
	void SetEnableSystem(BOOL enableSystem) {
		m_enableSystem = enableSystem;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSpawnGeneratorPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSpawnGeneratorPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void OnEnableSystemChanged();

private:
	enum { INDEX_ROOT = 1,
		INDEX_ENABLE_SYSTEM };

	CDialogBar* m_cDialogBar;

	BOOL m_enableSystem;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_SPAWN_GENERATORPROPERTIESCTRL_H_)
