///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class CCurrencyObj : public CObject, public GetSet {
	DECLARE_SERIAL(CCurrencyObj);

public:
	CCurrencyObj();

	CStringA m_currencyName;
	long m_amount;

	void Clone(CCurrencyObj** clone);
	void Serialize(CArchive& ar);
	void SerializeAlloc(CArchive& ar);

	DECLARE_GETSET
};

} // namespace KEP