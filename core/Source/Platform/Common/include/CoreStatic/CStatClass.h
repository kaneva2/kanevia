///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPObList.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class IMemSizeGadget;
class CInventoryObjList;

class CStatObj : public CObject, public GetSet {
	/*
	Version History
	2	8/02/05	added m_bonusBasis
*/
	DECLARE_SERIAL_SCHEMA(CStatObj, VERSIONABLE_SCHEMA | 2);

public:
	CStatObj();

	enum { NO_BENEFIT = 0,
		HEALTH_BENEFIT,
		SPEED_BENEFIT };

	CStringA m_statName;
	float m_capValue;
	float m_currentValue;
	int m_id;
	float m_gainBasis;
	int m_benefitType; //0 = no benefit  1=health benefit  2=speed benefit
	float m_bonusBasis; // 8/05 added multiplier per GKF

	float GetValueIncludingBonusItems(CInventoryObjList* inventory);
	BOOL UseStat(float useLevel, CInventoryObjList* inventory);
	void SerializeAlloc(CArchive& ar);
	void Clone(CStatObj** clone);

	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CStatObjList : public CKEPObList {
public:
	CStatObjList(){};
	~CStatObjList();

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CStatObjList);

	float GetStatValueByID(int id, CInventoryObjList* inventory);
	BOOL UseStatByID(float useLevel, int id, CInventoryObjList* inventory);
	void Clone(CStatObjList** clone);
	int GetBenefitBonus(int benefit, CInventoryObjList* inventory);
	BOOL VerifyStatRequirement(float minimumValue, int id, CInventoryObjList* inventory);
	int GetBonusByID(int id);
	CStatObj* GetStatByID(int id);
	void SerializeAlloc(CArchive& ar, int loadCount);
	int GetUniqueID();
};

} // namespace KEP