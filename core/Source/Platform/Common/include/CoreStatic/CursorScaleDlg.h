///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CURSORSCALEDLG_H__CFBAC1A1_ABAA_11D4_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_CURSORSCALEDLG_H__CFBAC1A1_ABAA_11D4_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CursorScaleDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCursorScaleDlg dialog

class CCursorScaleDlg : public CDialog {
	// Construction
public:
	CCursorScaleDlg(CWnd* pParent = NULL); // standard constructor

	// Dialog Data
	//{{AFX_DATA(CCursorScaleDlg)
	enum { IDD = IDD_DIALOG_CURSORSCALE };
	float m_scale;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCursorScaleDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCursorScaleDlg)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CURSORSCALEDLG_H__CFBAC1A1_ABAA_11D4_B1EE_0000B4BD56DD__INCLUDED_)
