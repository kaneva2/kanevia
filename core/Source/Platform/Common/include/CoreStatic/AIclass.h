///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "matrixarb.h"
#include "KEPObList.h"

namespace KEP {

class CAIScriptObj;
class CCollisionObj;
class CFrameObj;
class CGlobalInventoryObjList;
class CInventoryObjList;
class CMovementCaps;
class CtextReadoutObj;

struct FeelerSystem {
	Vector3f m_startPoint;
	Vector3f m_endPoint;
	BOOL m_status;

	Vector3f m_wallFeelerStartPoint;
	Vector3f m_wallFeelerEndPoint;
	int m_wallDirection; //0=right 1=left
	Vector3f m_mostRecentWallNormal;

	TimeMs m_cycleTime;
	TimeMs m_stamp;
	BOOL m_onWallCurrently;

	float m_incrementOnWall;
};

#define UNINITIALIZED_ENERGY -9999

class CAIObj : public CObject, public GetSet {
	//CAIObj(){}

	DECLARE_SERIAL(CAIObj)

public:
	CAIObj();

	int m_team; //what allies does the bot have
	int m_energyToRetreat; //at what energy does the Bot start to retreat at
	BOOL m_blitzAttack;
	BOOL m_takeCover;
	float m_closeEnoughDist;
	float m_turnTolerance;
	CFrameObj* m_targetFrame;
	int m_sightType;
	int m_sightRandVariable;
	float m_maxSightRadius;
	int m_difficulty;
	BOOL m_walkingType;
	int m_startAttackZone;
	int m_startAttackDist;
	int m_randChillTime;
	int m_pathfindingMethod;
	int m_randKamakazi;
	float m_AreaToRoam;
	int m_condition; //0=nothin  1=under attack
	int m_desiredGoal; //0=not sure  1=roam 2=defense 3=retreat 4=kill 5=get nearest point on map
	int m_lastKnownEnergy;
	float m_currentMissileSpeed;
	float m_normalSpeed;
	float m_hussleSpeed;
	float m_turnSpeed;
	int m_routePointCount;
	int* m_currentRoute;
	int m_routeCurPos;
	float m_curRadius;
	Vector3f m_wldPos;
	int m_mapCurLocation;
	int m_curMap;
	CStringA m_aiCfgName;
	CAIScriptObj* m_curScript;
	int m_spawnScript; //spawn with script  -1 = none
	FeelerSystem* m_feelerSys;
	BOOL m_leftBlocked;
	BOOL m_rightBlocked;
	float m_floorTolerance;
	float m_wallMinimumHeight;
	float m_feelerRange;
	float m_feelerDepth;
	TimeMs m_feelerCycleTime;
	float m_feelerWidth;
	float m_feelerHeight;
	BOOL m_feelerEnable;
	Vector3f m_lookAt;
	BOOL m_useOverridePhysics;
	float m_safeDistance; //at this distance-it has escaped or lost chase
	float m_minAttackDistance;
	int m_percentageAttack; //out of 100%

	BOOL m_exponentTurning;
	BOOL m_controlled;
	float m_travelingDesiredDist;
	BOOL m_needToDefend; //if trapped fight to death
	long m_delayOfAttacks;
	float m_engageRadius;
	float m_currentSpeedOverride;
	long m_attackDelayStamp;

	float m_followMaxDistance;
	float m_followMinDistance;
	float m_attackMaxDistance;
	float m_attackMinDistance;

	long m_attackDurationMax; //suto holding trigger down ;)
	long m_attackDurationMin; //suto holding trigger down ;)
	long m_attackDurationCurrent; //current round decision
	long m_attackTimeBetweenMax; //suto time before holding trigger down again
	long m_attackTimeBetweenMin; //suto time before holding trigger down again
	long m_attackTimeBetweenCurrent; //current round decision
	BOOL m_attackingCurrently; //currently holding trigger down
	TimeMs m_attackStamp;

	long m_distanceQuerryDuration;
	long m_distanceQuerryStamp;
	float m_eyeLevel;

	long m_sightCheckDuration;
	long m_sightCheckStamp;
	BOOL m_targetInSight;
	BOOL m_enableVision;

	TimeMs m_timeStampWhenTargetNotInSight;
	TimeMs m_durationTillTargetLost;

	BOOL m_enableJumpAbility;
	int m_accuracy; //out of 100%
	float m_accuracyErrorMax;
	float m_accuracyErrorMin;

	float m_prefernceAboveTarget;
	int m_agressive;

	//loot gen
	BOOL m_enableCashApplied;
	int m_maxCash;
	int m_minCash;

	int m_routeCurTarget;
	//
	BOOL m_neverAttacks;
	//radius system
	BOOL m_radiusSysEnabled;
	BOOL m_radiusSysInUse;
	Vector3f m_radiusSysCenter;
	BOOL m_radiusSysAttackOnExtents;
	BOOL m_radiusSysManditoryReturn;
	int m_radiusSysLastRoutePosition;
	float m_radiusSysRange;
	Vector3f m_radiusSysPriorDest;
	BOOL m_radiusSysIgnoreY;
	int m_dbIndex;

	//CUSTOMIZATION
	CStringA m_nameOvveride;
	CInventoryObjList* m_inventoryCfg;

	int* m_pMeshIds; // CharConfigItem::meshIds
	size_t m_baseMeshes;

	int m_entityCfgDBindexRef;
	CMovementCaps* m_physicsModuleOverride;
	int m_percentageOfDropping;
	float m_curExperienceLevel;

	//
	int m_overriderMaxEnergy;

	int m_collisionDBOverride;
	TimeMs m_retreatStamp;
	BOOL m_retreating;

	BOOL CheckVision(Vector3f startPoint, Vector3f endPoint, CCollisionObj* m_collisionBase);

	BOOL UpdateFeelerStatus(CCollisionObj* collisionSys,
		Matrix44f positionMatrix,
		float floorTolerance,
		float wallMinHeight,
		float currentYpos,
		CtextReadoutObj* debug,
		BOOL m_reachDestination,
		Vector3f currentAIDestinationRef);

	void UpdatePolygonTransform(Matrix44f posMatrix, Vector3f polygonLoc[3], Vector3f outPut[3]);

	void FeelerSystemContruction(float distance,
		float depth,
		TimeMs cycleTime,
		float width,
		float height);

	BOOL CheckVisionIsClear(Vector3f startPoint,
		Vector3f endPoint,
		CCollisionObj* m_collisionBase);

	BOOL EnableRadiusSystem(Vector3f& currentPosition,
		int curRouteDestNode,
		Vector3f& currentDestination);

	BOOL RadiusSysAllowMovement(Vector3f wldSpcDestination,
		Vector3f& newDestination,
		int& targetTraceRef);

	void Serialize(CArchive& ar);
	void SafeDelete();
	void Clone(CAIObj** clone);

	DECLARE_GETSET
};

class CAIObjList : public CKEPObList {
public:
	DECLARE_SERIAL(CAIObjList)

	CAIObjList() {
		m_pInstance = this;
	}
	~CAIObjList() {
		m_pInstance = 0;
	}

	static CAIObjList* GetInstance() {
		return m_pInstance;
	}

	void RefreshData(CGlobalInventoryObjList* globalInvList);
	void ResetIndexReferences();
	CAIObj* GetByIndex(int index);
	void SafeDelete();

	std::map<int, std::string> GetBots();

private:
	static CAIObjList* m_pInstance;
};

} // namespace KEP