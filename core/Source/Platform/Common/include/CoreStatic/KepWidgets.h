///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <d3d9types.h>
#include "Core/Math/Matrix.h"

#ifdef _ClientOutput
#include "wkgdebug.h"
#include "wkgconst.h"
#include "wkgtypes.h"
//#include "wkgmesh/selectwidget.h"
#endif

namespace KEP {
class ReMesh;
class CMaterialObject;

#define KEP_WIDGET_NO_MOUSE_STATE 3
#define KEP_WIDGET_L_MOUSE_DOWN 4
#define KEP_WIDGET_L_MOUSE_UP 5
#define KEP_WIDGET_L_MOUSE_DRAG 6

#define KEP_WIDGET_MESH_CACHE MeshRM::RMC_ID_8

enum WidgetMode {
	Unselected = 4,
	Selected_Single_None = 5,
	Selected_Single_Translation = 6,
	Selected_Single_Rotation = 7,
	Selected_Single_Scale = 8, // Probably editor world obj only
	Selected_Multi_None = 9,
	Selected_Multi_Translation = 10,
	Selected_Multi_Rotation = 11,
	Selected_Multi_Scale = 12
};

enum Axis {
	NoAxis = 0,
	XAxis = 1,
	YAxis = 2,
	ZAxis = 3,
	XYPlane = 4,
	XZPlane = 5,
	YZPlane = 6,
	OnObject = 7
};

class KEPWidgetMeshes {
public:
	static ReMesh* getXTransWidget();
	static ReMesh* getYTransWidget();
	static ReMesh* getZTransWidget();

	static ReMesh* getRotationWidget(Axis axis);
	static ReMesh* getPlaneWidget(Axis axis1, Axis axis2);

	static ReMesh* loadReMeshOBJ(const char*, const Matrix44f* matrix = NULL);
	static ReMesh* getConeMesh(double length, double radius, int rimCount);
	static ReMesh* getRingMesh(double angle, double radius, int angleCount = 36, double height = 0.5, double width = 0.5);

	static CMaterialObject* getXMaterial();
	static CMaterialObject* getYMaterial();
	static CMaterialObject* getZMaterial();
	static CMaterialObject* getXYPlaneMaterial();
	static CMaterialObject* getXZPlaneMaterial();
	static CMaterialObject* getYZPlaneMaterial();

	static const D3DXCOLOR X_COLOR, Y_COLOR, Z_COLOR, PLANE_COLOR;

private:
	static ReMesh* getTransWidget(int axis);
	static CMaterialObject* ms_xMaterial;
	static CMaterialObject* ms_yMaterial;
	static CMaterialObject* ms_zMaterial;
	static CMaterialObject* ms_xyplaneMaterial;
	static CMaterialObject* ms_xzplaneMaterial;
	static CMaterialObject* ms_yzplaneMaterial;
};

class KEP3DObjectWidget {
	enum SelectionType {
		NoSelection = 2,
		Player = 3,
		DynamicObject = 4,
		WorldObject = 5
	};

	enum AxisOrientation {
		ObjectSpace = 0,
		WorldSpace = 1
	};

public:
	KEP3DObjectWidget();
	~KEP3DObjectWidget();

	void DeleteMeshesAndPlanes();

	void switchTranslation();
	void switchRotation();
	void switchWorldSpace();
	void switchObjectSpace();
	void showWidget(bool show = true); // allows the widget to become visible (so-called "advanced mode"), or not
	void advancedMovement(bool adv = true); // makes the object drag behaviour the same as the 'old' version that used the widget's horizontal plane
	void ModeTranslate(float pivotX, float pivotY, float pivotZ);
	void ModeRotate(float pivotX, float pivotY, float pivotZ);
	void ModeOff();
	void render();
	bool handleMouse(int clickState, int axis, const Vector3f& intersection, const Vector3f& origin, const Vector3f& direction, const Vector3f& cameraPos);
	void restoreCursorPos();
	void saveCursorPos(const Vector3f& intersection);

	Vector3f getWidgetPosition();

	WidgetMode getWidgetMode() const {
		return m_currentSelectionMode;
	}

	bool isWidgetModeTranslation() const {
		auto mode = getWidgetMode();
		return (mode == Selected_Single_Translation) || (mode == Selected_Multi_Translation);
	}
	bool isWidgetModeRotation() const {
		auto mode = getWidgetMode();
		return (mode == Selected_Single_Rotation) || (mode == Selected_Multi_Rotation);
	}
	bool visible() const {
		auto mode = getWidgetMode();
		return (mode != Unselected) && (mode != Selected_Single_None) && (mode != Selected_Multi_None);
	}

	const Vector3f& getRotationAngle() const {
		return m_rotationAngle;
	}
	void setRotation(float x, float y, float z);
	void setPosition(float x, float y, float z);

	int mouseOverTest(const Vector3f& rayOrigin, const Vector3f& rayDirection, Vector3f* location) const;

	void updateHoverState(int axis);

	bool getWidgetMatrixInverse(Matrix44f* matrix, float epsilon = 1e-5);
	bool getAdvancedMovement() const {
		return m_advancedMovement;
	}

private:
	WidgetMode m_currentSelectionMode;
	bool m_showWidget; // allow the widget to become visible and handle axis & plane drags
	bool m_visible; // currently visible/active widget
	bool m_advancedMovement; // new or old click-and-drag behaviour
	Axis m_currentlyDragging;
	ReMesh* m_xMesh;
	ReMesh* m_yMesh;
	ReMesh* m_zMesh;
	ReMesh* m_xzPlane;
	ReMesh* m_xyPlane;
	ReMesh* m_yzPlane;
	Matrix44f m_locationMat;
	Vector3f m_cursorPos; // widget-space cursor position (set when the cursor is hidden)
	Matrix44f m_rotationMat[3]; // used when rotating in world space to orient the rotation models
	Vector2f m_dragStartPos; // x,y in screen space
	float m_dragStartAngle;
	float m_planeDist;
	Vector3f m_rotationAngle;
	AxisOrientation m_axisOrientation;
	Vector3f m_widgetOffset;
	float m_dragMultiplier;

	bool handleTranslationAxisDragging(int axis, Vector3f osDirection, Vector3f osOrigin);
	bool handleTranslationPlaneDragging(int axis, Vector3f osDirection, Vector3f osOrigin);
	bool handleRotationAxisDragging(Axis axis, Vector3f osDirection, Vector3f osOrigin);

	void getDragPlane(Vector3f& normal, float& distance, int axis);
	bool rayIntersectPlane(Vector3f& intersection, const Vector3f& rayOrigin, const Vector3f& rayDirection, const Vector3f& planeNormal, float planeDistance, float epsilon = 1e-5);

	void setHover(Axis axis, bool);
};

} // namespace KEP
