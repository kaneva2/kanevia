///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

struct IDirect3DTexture9;

namespace KEP {

class ITexture {
public:
	virtual IDirect3DTexture9* GetOrQueueTexture() = 0;
};

} // namespace KEP
