///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#pragma once

namespace KEP {

class HashUtil {
public:
	HashUtil(unsigned long initval = 0);

	unsigned long HashCode() {
		return _hash;
	}

	HashUtil& operator<<(int i);
	HashUtil& operator<<(unsigned long ul);
	HashUtil& operator<<(bool b);
	HashUtil& operator<<(float f);
	HashUtil& operator<<(const char*);
	HashUtil& operator<<(const class HashBuffer&);

private:
	unsigned long _hash;
};

/* this class allows for syntax like 'hash << HashBuffer(data, len)'
   for hashing arbitrary blocks of data */
class HashBuffer {
	friend class HashUtil;

public:
	HashBuffer(const void* data, int len) :
			_data(data), _len(len) {
	}

private:
	const void* _data;
	int _len;
};

} // namespace KEP
