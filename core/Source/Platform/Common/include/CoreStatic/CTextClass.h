///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPObList.h"

namespace KEP {

class CFontObj : public CObject {
protected:
	CFontObj() {}

	DECLARE_SERIAL(CFontObj)

public:
	CFontObj(HFONT L_fontObject,
		CStringA L_fontName,
		int L_sizeX,
		int L_sizeY);

	HFONT m_fontObject;
	CStringA m_fontName;
	int m_sizeX;
	int m_sizeY;

	void Serialize(CArchive& ar);
};

class CFontObjList : public CKEPObList {
public:
	CFontObjList(){};

	DECLARE_SERIAL(CFontObjList)
};

//text and font stuff
class CTextEntry : public CObject, public GetSet {
protected:
	CTextEntry() {}

	DECLARE_SERIAL(CTextEntry)

public:
	CTextEntry(CStringA L_text,
		int L_percX,
		int L_percY);

	CStringA m_text;
	int m_percX;
	int m_percY;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CTextEntryList : public CObList {
public:
	CTextEntryList(){};

	DECLARE_SERIAL(CTextEntryList)
};
//main class  list of objects defines objects i scene
//each object can have multiple animations and harcy
class CTextCollection : public CObject {
protected:
	CTextCollection() {}

	DECLARE_SERIAL(CTextCollection)

public:
	CTextCollection(CStringA L_collectionName,
		CTextEntryList* L_textEntryList,
		int L_timeVarial,
		int L_timeLength,
		int L_fontSel,
		BOOL L_displayIsOn,
		COLORREF L_textColor);

	COLORREF m_textColor;
	BOOL m_displayIsOn;
	CStringA m_collectionName;
	CTextEntryList* m_textEntryList;
	int m_timeVarial;
	int m_timeLength;
	int m_fontSel;

	void Serialize(CArchive& ar);
};

class CTextCollectionList : public CKEPObList {
public:
	CTextCollectionList(){};

	DECLARE_SERIAL(CTextCollectionList)
};

} // namespace KEP