///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/fast_mutex.h"
#include <d3d9caps.h>
#include "IResource.h"

struct IDirect3DTexture9;

#define INVALID_TEXTURE_NAMEHASH 0xFFFFFFFF

namespace KEP {

class CTextureEX;
typedef std::map<DWORD, CTextureEX*> HashedTexMap;
typedef std::pair<DWORD, CTextureEX*> HashedTexPair;
class DynamicTexture;

class TextureDatabase {
public:
	static const unsigned INVALID_DYNAMIC_TEXTURE_ID = 0;

	TextureDatabase();
	virtual ~TextureDatabase();

	void Initialize();

	static DWORD TextureNameHash(CTextureEX* pTex);
	static DWORD TextureNameHash(const std::string& texName, const D3DCOLOR& colorKey = 0);
	static DWORD TextureNameHash(const std::string& texName, unsigned char r, unsigned char g, unsigned char b, unsigned char a);

	DWORD TextureAdd(DWORD& nameHash, const std::string& texName, const std::string& subDir = "", const std::string& url = "", D3DCOLOR colorKey = 0, eTexturePath texPath = eTexturePath::MapsModels);

	DWORD CustomTextureAdd(DWORD& nameHash, const std::string& texName, const std::string& subDir = "", D3DCOLOR colorKey = 0, eTexturePath texPath = eTexturePath::CustomTexture);

	const CTextureEX* TextureGet(const std::string& name);
	CTextureEX* TextureGet(DWORD& nameHash, const std::string& texName = "", const std::string& subDir = "", D3DCOLOR colorKey = 0);

	// Texture Map Functions
	void TextureMapLog();
	bool TextureMapClearErrored();
	bool TextureMapUnloadAll();
	bool TextureMapDelAll();
	bool TextureMapDel(CTextureEX* pTex);
	bool TextureMapAdd(const HashedTexPair& texPair);
	CTextureEX* TextureMapGet(const DWORD& nameHash);
	CTextureEX* TextureMapGet(const std::string& texFileName);
	DWORD TextureMapGetNameHash(const std::string& texFileName);
	bool TextureMapContains(const DWORD& nameHash);

	// Dynamic textures (procedurally generated)
	unsigned DynamicTextureAdd(unsigned width, unsigned height);
	void DynamicTextureDestroy(unsigned dynTextureId);
	void DynamicTextureDestroyAll();
	IDirect3DTexture9* DynamicTextureGetHandle(unsigned dynTextureId);

protected:
	D3DCAPS9 m_d3d_caps;
	HashedTexMap m_texMap; // nameHash -> pTexture
	fast_recursive_mutex m_texMutex;

	// Container for procedurally generated textures
	std::mutex m_dynamicTextureMutex;
	unsigned m_nextDynamicTextureId;
	std::map<unsigned, DynamicTexture*> m_dynamicTextures;
};

// DRF - Added - Valid texture names are not empty nor 'none'
inline bool ValidTextureFileName(const std::string& texFileName) {
	return !texFileName.empty() && !StrSameIgnoreCase(texFileName, "none");
}

// DRF - Added - Valid icon texture names are valid texture names of the form 'icon<glid>.jpg'
inline bool ValidIconTextureFileName(const std::string& texFileName) {
	return ValidTextureFileName(texFileName) && STLContains(texFileName, "icon") && STLContains(texFileName, ".jpg");
}

// DRF - Added - Valid texture name hashes are not INVALID_TEXTURE_NAME_HASH
inline bool ValidTextureNameHash(const DWORD& texNameHash) {
	return (texNameHash != INVALID_TEXTURE_NAMEHASH);
}

} // namespace KEP
