/******************************************************************************
 SpawnPointCfgPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_SPAWN_POINT_CFG_PROPERTIESCTRL_H_)
#define _SPAWN_POINT_CFG_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpawnPositionCfgPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CSpawnPointCfgPropertiesCtrl window

class CSpawnPointCfgPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSpawnPointCfgPropertiesCtrl();
	virtual ~CSpawnPointCfgPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetSpawnPointCfgName() const {
		return Utf16ToUtf8(m_sSpawnPointCfgName).c_str();
	}
	float GetPositionX() const {
		return m_positionX;
	}
	float GetPositionY() const {
		return m_positionY;
	}
	float GetPositionZ() const {
		return m_positionZ;
	}
	int GetZoneIndex() const {
		return m_zoneIndex;
	}
	int GetZoneInstanceTypeID() const {
		return m_zoneInstanceTypeID;
	}
	int GetRotation() const {
		return m_rotation;
	}

	// modifiers
	void SetSpawnPointCfgName(CStringA sSpawnPointCfgName) {
		m_sSpawnPointCfgName = Utf8ToUtf16(sSpawnPointCfgName).c_str();
	}
	void SetPositionX(float positionX);
	void SetPositionY(float positionY);
	void SetPositionZ(float positionZ);
	void SetZoneIndex(int zoneIndex) {
		m_zoneIndex = zoneIndex;
	}
	void SetZoneInstanceTypeID(int zoneInstanceTypeID) {
		m_zoneInstanceTypeID = zoneInstanceTypeID;
	}
	void SetRotation(int rotation);

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSpawnPointCfgPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSpawnPointCfgPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_ZONE,
		INDEX_ZONE_INSTANCE_TYPE,
		INDEX_POSITION,
		INDEX_X,
		INDEX_Y,
		INDEX_Z,
		ROTATION,
		INDEX_GET_CURRENT_POSITION
	};

	void GetCurrentPosition();
	void InitZones();
	void InitInstanceTypes();

private:
	CStringW m_sSpawnPointCfgName;
	float m_positionX;
	float m_positionY;
	float m_positionZ;
	int m_zoneIndex;
	int m_rotation;
	int m_zoneInstanceTypeID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_SPAWN_POINT_CFG_PROPERTIESCTRL_H_)
