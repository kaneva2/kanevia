/******************************************************************************
 RequiredStatsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_REQUIREDSTATSPROPERTIESCTRL_H_)
#define _REQUIREDSTATSPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RequiredStatsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CRequiredStatsPropertiesCtrl window

class CRequiredStatsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CRequiredStatsPropertiesCtrl();
	virtual ~CRequiredStatsPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetStatID() const {
		return m_statID;
	}
	float GetStatAmount() const {
		return m_statAmount;
	}

	// modifiers
	void SetStatID(int statID) {
		m_statID = statID;
	}
	void SetStatAmount(float statAmount) {
		m_statAmount = statAmount;
	}

	// methods
	void AddStat(CStringA sStat, int statID);
	void ClearStats();
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRequiredStatsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CRequiredStatsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;

	int m_statID;
	float m_statAmount;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_REQUIREDSTATSPROPERTIESCTRL_H_)
