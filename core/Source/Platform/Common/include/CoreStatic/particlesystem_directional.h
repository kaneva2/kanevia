///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ParticleSystem_Directional : public CPropertyPage {
	DECLARE_DYNCREATE(ParticleSystem_Directional)

	// Construction
public:
	ParticleSystem_Directional();
	~ParticleSystem_Directional();

	int OnQuerySiblings();

	// Dialog Data
	//{{AFX_DATA(ParticleSystem_Directional)
	enum { IDD = IDD_DIALOG_PARTICLESYSTEM_DIRECTIONAL };
	float m_burstSpeed;
	float m_burstSpeedDeath;
	float m_burstSpread;
	float m_burstSpreadDeath;
	float m_particleSpeed;
	float m_particleSpread;
	int m_scaleEnd;
	int m_scaleStart;
	int m_fadeEnd;
	int m_fadeStart;
	int m_fieldRadius;
	int m_fieldHeight;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(ParticleSystem_Directional)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};
