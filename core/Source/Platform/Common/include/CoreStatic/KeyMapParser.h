///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/include/CoreStatic/InputSystemTypes.h"
#include "Core/Util/Parameter.h"
#include "KEPCommon.h"
#include <vector>

namespace KEP {

class KeyCodeMap;
class ParseStream;

class KeyMapParser {
public:
	KeyMapParser(eKeyCodeType keyCodeType);
	bool Parse(std::istream& istrm, Out<std::vector<KeyEventBindingByName>> aKeyEventBindings);

private:
	void ParseParenthesizedEventList(ParseStream& strm, Out<std::vector<KeyEvent>> aKeyEvents);
	bool ParseKeyMapping(ParseStream& strm, Out<KeyEventBindingByName> keyEventBinding);
	bool ParseParenthesizedKeyCombination(ParseStream& strm, Out<KeyStateCombination> keyStateCombination);
	void ParseParenthesizedKeyNameList(ParseStream& strm, Out<std::vector<int>> aKeys);

	const KeyCodeMap* m_pKeyCodeMap;
};

bool ParseKeyMappings(std::istream& istrm, eKeyCodeType keyCodeType, Out<std::vector<KeyEventBindingByName>> aKeyEventBindings);
bool PrintKeyMappings(std::ostream& ostrm, eKeyCodeType keyCodeType, const std::vector<KeyEventBindingByName>& aKeyEventBindings);

} // namespace KEP
