///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// added by serialize.pl
#include <GetSet.h>
#include "matrixarb.h"

namespace KEP {

struct StrikeVariation {
	char textureName[256];
	int textureByIndex;
	int m_sndID;
	float m_soundFactor;
};

template <class arrayClass, class arrayItemClass, class arraySizeType>
void gsFillArray(GetSetVector& v, arrayClass** a, arraySizeType* count) {
	v.clear(); // empty the array first

	if (a && *a && count && *count > 0) {
		for (int i = 0; i < *count; i++) {
			v.push_back(new arrayItemClass(&(*a)[i]));
		}
	}
}

class StrikeClassGetSet : public GetSet {
public:
	StrikeClassGetSet(StrikeVariation* v) :
			m_strikeVariation(v) {}

	StrikeVariation* m_strikeVariation;

	DECLARE_GETSET
};

class StrikeClassGetSetArray : public GetSetVector {
public:
	StrikeClassGetSetArray(StrikeVariation** v, int* count) :
			GetSetVector(true), // true = delete items on delete
			m_array(v),
			m_count(count) {
		gsFillArray<StrikeVariation, StrikeClassGetSet, int>(*this, m_array, m_count);
	}
	virtual void fillArray() {
		gsFillArray<StrikeVariation, StrikeClassGetSet, int>(*this, m_array, m_count);
	}

	StrikeVariation** m_array;
	int* m_count;
};

class CStormObj : public CObject, public GetSet {
	DECLARE_SERIAL(CStormObj);

public:
	CStormObj();
	void Clone(CStormObj** clone);

	CStringA m_stormName;
	UINT m_runtimeParticleId;
	int m_particleSystemRef;
	long m_duration;
	long m_stormTransition;
	long m_frequency; //percentage possibility Of storming out of 100%

	float m_fogRedMod;
	float m_fogGreenMod;
	float m_fogBlueMod;

	float m_sunColorRedMod;
	float m_sunColorGreenMod;
	float m_sunColorBlueMod;

	StrikeVariation* m_strikeVariationArray;
	int m_VariationLightningCount;
	int m_totalStrikeCountPerMinute;
	long m_lightningStamp;
	long m_strikeDuration;

	float m_strikePanelWidth;
	float m_strikeSpawnHeight;
	float m_strikeDepth;
	float m_stormRadius;

	void SafeDelete();
	BOOL RemoveLightningStrike(int index);
	void AddLightningStrike(char textureName[256],
		int sndID,
		float soundFactor);
	void Serialize(CArchive& ar);

	StrikeClassGetSetArray m_strikeVariationGetSet;

	DECLARE_GETSET
};

class CStormObjList : public CObList {
public:
	CStormObjList(){};

	void SafeDelete();

	DECLARE_SERIAL(CStormObjList);
};

} // namespace KEP