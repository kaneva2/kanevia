#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "KEPPropertiesCtrl.h"

class CSpawnGenDlg;

class CNetworkPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CNetworkPropertiesCtrl();
	virtual ~CNetworkPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	bool GetClientSideFiring() const {
		return m_bClientSideFiring;
	}
	bool GetRecordNetworkData() const {
		return m_bRecordNetworkData;
	}
	bool GetPersistentServer() const {
		return m_bPersistentServer;
	}
	int GetMaxLoggedOnPlayers() const {
		return m_maxLoggedOnPlayers;
	}
	int GetAvailableBandwidth() const {
		return m_availableBandwidth;
	}
	int GetFrequencyAdjustBuffer() const {
		return m_frequencyAdjustBuffer;
	}
	int GetFrequencyIncrement() const {
		return m_frequencyIncrement;
	}
	TimeMs GetFrequencyLimit() const {
		return m_frequencyLimit;
	}
	float GetCalcEntityRadius() const {
		return m_calcEntityRadius;
	}
	float GetCalcItemRadius() const {
		return m_calcItemRadius;
	}
	long GetDeadEntityDissolveTime() const {
		return m_deadEntityDissolveTime;
	}
	long GetDeadEntityNPCDissolveTime() const {
		return m_deadEntityNPCDissolveTime;
	}
	long GetAiServerCycle() const {
		return m_aiServerCycle;
	}
	int GetVersionNumber() const {
		return m_versionNumber;
	}
	int GetMurdererLevel() const {
		return m_murdererLevel;
	}
	int GetMurderWorkOffRate() const {
		return m_murderWorkOffRate;
	}
	int GetLevelRestriction() const {
		return m_levelRestriction;
	}
	int GetMaxPacketIntake() const {
		return m_maxPacketIntake;
	}
	int GetMaxPacketOutput() const {
		return m_maxPacketOutput;
	}
	int GetHouseVisRange() const {
		return m_houseVisRange;
	}
	CStringA GetServerKey() const {
		return Utf16ToUtf8(m_sServerKey).c_str();
	}
	bool GetServerKeyRequired() const {
		return m_bServerKeyRequired;
	}
	bool GetAlwaysStartInHousingZone() const {
		return m_alwaysStartInHousingZone;
	}
	CStringA GetDisabledEdbs() const {
		return Utf16ToUtf8(m_disabledEdbs).c_str();
	}
	int GetISecViolationCount() const {
		return m_isecViolationCount;
	}
	int GetViolationCount() const {
		return m_violationCount;
	}
	int GetNewbieLevel() const {
		return m_newbieLevel;
	}
	int GetInitialZoneSpawnIndex() const {
		return m_initialZoneSpawnIndex;
	}
	int GetInitialZoneInstanceId() const {
		return m_initialZoneInstanceId;
	}
	int GetInitialZoneIndexPlain() const {
		return m_initialZoneIndexPlain;
	}
	int GetInitialZoneType() const {
		return m_initialZoneType;
	}

	// modifiers
	void SetClientSideFiring(bool bClientSideFiring) {
		m_bClientSideFiring = bClientSideFiring;
	}
	void SetRecordNetworkData(bool bRecordNetworkData) {
		m_bRecordNetworkData = bRecordNetworkData;
	}
	void SetPersistentServer(bool bPersistentServer) {
		m_bPersistentServer = bPersistentServer;
	}
	void SetMaxLoggedOnPlayers(int maxLoggedOnPlayers) {
		m_maxLoggedOnPlayers = maxLoggedOnPlayers;
	}
	void SetAvailableBandwidth(int availableBandwidth) {
		m_availableBandwidth = availableBandwidth;
	}
	void SetFrequencyAdjustBuffer(int frequencyAdjustBuffer) {
		m_frequencyAdjustBuffer = frequencyAdjustBuffer;
	}
	void SetFrequencyIncrement(int frequencyIncrement) {
		m_frequencyIncrement = frequencyIncrement;
	}
	void SetFrequencyLimit(TimeMs frequencyLimit) {
		m_frequencyLimit = frequencyLimit;
	}
	void SetCalcEntityRadius(float calcEntityRadius) {
		m_calcEntityRadius = calcEntityRadius;
	}
	void SetCalcItemRadius(float calcItemRadius) {
		m_calcItemRadius = calcItemRadius;
	}
	void SetDeadEntityDissolveTime(long deadEntityDissolveTime) {
		m_deadEntityDissolveTime = deadEntityDissolveTime;
	}
	void SetDeadEntityNPCDissolveTime(long deadEntityNPCDissolveTime) {
		m_deadEntityNPCDissolveTime = deadEntityNPCDissolveTime;
	}
	void SetAiServerCycle(long aiServerCycle) {
		m_aiServerCycle = aiServerCycle;
	}
	void SetVersionNumber(int versionNumber) {
		m_versionNumber = versionNumber;
	}
	void SetMurdererLevel(int murdererLevel) {
		m_murdererLevel = murdererLevel;
	}
	void SetMurderWorkOffRate(int murderWorkOffRate) {
		m_murderWorkOffRate = murderWorkOffRate;
	}
	void SetLevelRestriction(int levelRestriction) {
		m_levelRestriction = levelRestriction;
	}
	void SetMaxPacketIntake(int maxPacketIntake) {
		m_maxPacketIntake = maxPacketIntake;
	}
	void SetMaxPacketOutput(int maxPacketOutput) {
		m_maxPacketOutput = maxPacketOutput;
	}
	void SetHouseVisRange(int houseVisRange) {
		m_houseVisRange = houseVisRange;
	}
	void SetServerKey(CStringA sServerKey) {
		m_sServerKey = Utf8ToUtf16(sServerKey).c_str();
	}
	void SetServerKeyRequired(bool bServerKeyRequired) {
		m_bServerKeyRequired = bServerKeyRequired;
	}
	void SetAlwaysStartInHousingZone(bool alwaysStartInHousingZone) {
		m_alwaysStartInHousingZone = alwaysStartInHousingZone;
	}
	void SetDisabledEdbs(CStringA edbs) {
		m_disabledEdbs = Utf8ToUtf16(edbs).c_str();
	}
	void SetISecViolationCount(int i) {
		m_isecViolationCount = i;
	}
	void SetViolationCount(int i) {
		m_violationCount = i;
	}
	void SetNewbieLevel(int i) {
		m_newbieLevel = i;
	}
	void SetInitialZoneSpawnIndex(int initZoneSpawnIndex) {
		m_initialZoneSpawnIndex = initZoneSpawnIndex;
	}
	void SetInitialZoneInstanceId(int initZoneInstanceId) {
		m_initialZoneInstanceId = initZoneInstanceId;
	}
	void SetInitialZoneIndexPlain(int initZoneIndexPlain) {
		m_initialZoneIndexPlain = initZoneIndexPlain;
	}
	void SetInitialZoneType(int initZoneType) {
		m_initialZoneType = initZoneType;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetworkPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CNetworkPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void InitZones();
	void InitInstanceTypes();

private:
	int m_id;

	bool m_bClientSideFiring;
	bool m_bRecordNetworkData;
	bool m_bPersistentServer;
	int m_maxLoggedOnPlayers;
	int m_availableBandwidth;
	int m_frequencyAdjustBuffer;
	int m_frequencyIncrement;
	TimeMs m_frequencyLimit;
	float m_calcEntityRadius;
	float m_calcItemRadius;
	long m_deadEntityDissolveTime;
	long m_deadEntityNPCDissolveTime;
	long m_aiServerCycle;
	int m_versionNumber;
	int m_murdererLevel;
	int m_murderWorkOffRate;
	int m_levelRestriction;
	int m_maxPacketIntake;
	int m_maxPacketOutput;
	bool m_alwaysStartInHousingZone;
	int m_houseVisRange;
	CStringW m_disabledEdbs;
	int m_isecViolationCount;
	int m_violationCount;
	int m_newbieLevel;
	int m_initialZoneSpawnIndex;
	int m_initialZoneInstanceId;
	int m_initialZoneIndexPlain;
	int m_initialZoneType;

	CStringW m_sServerKey;
	bool m_bServerKeyRequired;
};
