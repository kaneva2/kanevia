///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class CGroupObj : public CObject, public GetSet {
protected:
	CGroupObj() {}
	DECLARE_SERIAL(CGroupObj);

public:
	CGroupObj(
		float L_groupRadius,
		Vector3f L_groupCenter,
		BOOL L_valid);

	float m_groupRadius;
	Vector3f m_groupCenter;
	BOOL m_valid;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CGroupList : public CObList {
public:
	CGroupList(){};
	DECLARE_SERIAL(CGroupList)
};

} // namespace KEP