/******************************************************************************
 MaterialShadersPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_MATERIAL_SHADERS_PROPERTIESCTRL_H_)
#define _MATERIAL_SHADERS_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MaterialShadersPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CMaterialShadersPropertiesCtrl window

class CMaterialShadersPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CMaterialShadersPropertiesCtrl();
	virtual ~CMaterialShadersPropertiesCtrl();

	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual bool OnPropertyButtonClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetShaderName() const {
		return Utf16ToUtf8(m_sShaderName).c_str();
	}
	bool GetMaterialOverride() const {
		return m_bMaterialOverride;
	}
	bool GetEnvironmentMapping() const {
		return m_bEnvironmentMapping;
	}
	int GetEnvironmentUVSet() const {
		return m_environmentUVSet;
	}
	int GetTextureID(int textureIndex);
	int GetBlendID(int blendIndex);

	// modifiers
	void SetShaderName(CStringA sShaderName) {
		m_sShaderName = Utf8ToUtf16(sShaderName).c_str();
	}
	void SetMaterialOverride(bool bMaterialOverride) {
		m_bMaterialOverride = bMaterialOverride;
	}
	void SetEnvironmentMapping(bool bEnvironmentMapping) {
		m_bEnvironmentMapping = bEnvironmentMapping;
	}
	void SetEnvironmentUVSet(int environmentUVSet) {
		m_environmentUVSet = environmentUVSet;
	}
	void SetTextureID(int textureIndex, int textureID);
	void SetBlendID(int blendIndex, int blendID);

	// methods
	void AddTextureItem(CStringA sItem);
	void ClearTextureItems();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaterialShadersPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CMaterialShadersPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_SHADER_NAME,
		INDEX_MATERIAL_OVERRIDE_ROOT,
		INDEX_MATERIAL_OVERRIDE,
		INDEX_AMBIENT_ACCESS,
		INDEX_DIFFUSE_ACCESS,
		INDEX_EMMISSIVE_ACCESS,
		INDEX_SPECULAR_ACCESS,
		INDEX_ENVIRONMENT_MAPPING_ROOT,
		INDEX_ENABLE_ENVIRONMENT,
		INDEX_UV_SET,
		INDEX_TEXTURE_SETTINGS_ROOT,
		INDEX_TEXTURE1,
		INDEX_BLEND_LEVEL1,
		INDEX_TEXTURE2,
		INDEX_BLEND_LEVEL2,
		INDEX_TEXTURE3,
		INDEX_BLEND_LEVEL3,
		INDEX_TEXTURE4,
		INDEX_BLEND_LEVEL4,
		INDEX_TEXTURE5,
		INDEX_BLEND_LEVEL5
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	void DeleteTextureItems();

private:
	enum NumTextures {
		NumTextures = 5
	};

	CStringW m_sShaderName;
	bool m_bMaterialOverride;
	bool m_bEnvironmentMapping;
	int m_environmentUVSet;
	int m_textureID[NumTextures];
	int m_blendID[NumTextures];

	std::vector<CStringW*> m_textureNames;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_MATERIAL_SHADERS_PROPERTIESCTRL_H_)
