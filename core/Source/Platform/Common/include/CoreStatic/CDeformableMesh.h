///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/CArchiveObjectSchema.h"
#include "SkinVertex.h"
#include <assert.h>

namespace KEP {

class IMemSizeGadget;
struct ABVERTEX0L;
struct ABVERTEX;
class CMaterialObject;
class CBoneList;
class CEXMeshObj;
class ReMesh;

class CDeformableVisObj : public CObject {
	DECLARE_SERIAL_SCHEMA(CDeformableVisObj, VERSIONABLE_SCHEMA | 4); // 03_23_07 CPITZER...ReMesh format

public:
	CDeformableVisObj();
	virtual ~CDeformableVisObj();

	CEXMeshObj* m_mesh;
	std::vector<VertexAssignment> m_vertexAssignments;
	std::vector<VertexAssignmentBlended> m_blendedVertexAssignments;
	SkinVertexBlendType m_blendedType;
	ReMesh* m_ReMesh;

	size_t GetAssignmentCount() const {
		switch (m_blendedType) {
			case SkinVertexBlendType::Disabled:
				return m_vertexAssignments.size();
			case SkinVertexBlendType::Position_Normal:
				return m_blendedVertexAssignments.size();
		}
		assert(false);
		return 0;
	}

	void Optimize();

	void SetReMesh(ReMesh* mesh) { m_ReMesh = mesh; }
	ReMesh* GetReMesh() { return m_ReMesh; }

	BOOL VerticiePartialCompare(const ABVERTEX& v1, const ABVERTEX& v2, int i1, int i2);
	void AddVertexAssignmentBlended(std::vector<VertexBoneWeight>&& vertexBoneWeights, int vertexIndex);
	void Clone(CDeformableVisObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Serialize(CArchive& ar);
	void DoOldImport(CArchive& ar);
	void DoOldExport(CArchive& ar);

	void setVertexAssignment(int newIndex, int oldIndex, std::vector<VertexAssignment>& newVertexAssignments, std::vector<VertexAssignmentBlended>& newBlendedVertexAssignments);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CDeformableVisList : public CObList {
public:
	CDeformableVisList(){};

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CDeformableVisList);

	CDeformableVisObj* GetByIndex(int index);
	void Clone(CDeformableVisList** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, const CBoneList* pBoneList);
	void Optimize();
	void AvgNormals(float angTol);
	void UnifyNormals(float tol);
	void NormilizeOnly();
	void Normalize();
	void SafeDelete();
};

class CDeformableMesh : public CObject {
	DECLARE_SERIAL_SCHEMA(CDeformableMesh, VERSIONABLE_SCHEMA | 8); // version 4 supported materials; version 5 m_worldSort member; version 6 core content flag; version 7 remesh intead of deformable mesh list
public:
	CDeformableMesh();
	virtual ~CDeformableMesh();

	BOOL m_publicDim;
	CStringA m_dimName;
	CObList* m_supportedMaterials;
	bool m_supportedMaterialsOwned;
	BOOL m_worldSort;
	bool m_coreContent;
	int m_currentMatInUse;
	CMaterialObject* m_material;
	uint64_t* m_lodHashes;
	unsigned int m_lodCount;
	UINT m_effectIndex;

	void Clone(CDeformableMesh** clone, LPDIRECT3DDEVICE9 g_pd3dDevice, const CBoneList* boneList);
	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	//////////////////////////////////////////////////////////////////
	// Keep m_lodChain for some special clients (Editor/Previewer/Importer/Exporter)
	// Using preprocessor to block-out unauthorized access
private:
	CDeformableVisList* m_lodChain; // temporarily revived for internal use by Importer and Previewer only
	//-- can be removed if Previewer can render ReMesh, currently Previewer only deals with CEXMesh

	void getMemSizeSupportedMaterials(IMemSizeGadget* pMemSizeGadget) const;

#if defined(ASSET_IMPORTER) || defined(ASSETS_DATABASE) // Visible to Previewer and Asset Importers only
public:
	CDeformableVisList* GetDeformableVisList() {
		return m_lodChain;
	}
	void SetDeformableVisList(CDeformableVisList* lodChain) {
		m_lodChain = lodChain;
	}
#endif
};

class CDeformableMeshList : public CObList {
public:
	CDeformableMeshList(){};
	DECLARE_SERIAL(CDeformableMeshList);
	void SafeDelete();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

} // namespace KEP
