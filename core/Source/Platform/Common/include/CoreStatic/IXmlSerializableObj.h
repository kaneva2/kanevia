///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "iserializableobj.h"

#include "..\KEPUtil\Algorithms.h"

namespace KEP {

class IXmlSerializableObj : public ISerializableObj {
public:
	IXmlSerializableObj(void);
	~IXmlSerializableObj(void);

	virtual void SerializeToXML(const FileName& fileName) = 0;
	virtual void SerializeFromXML(const FileName& fileName) = 0;
};

} // namespace KEP