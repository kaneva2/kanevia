///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLOBALADVANCEDDLG_H__2FCBC761_18A9_11D3_BDB1_FC946FAFAD40__INCLUDED_)
#define AFX_GLOBALADVANCEDDLG_H__2FCBC761_18A9_11D3_BDB1_FC946FAFAD40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GlobalAdvancedDlg.h : header file
//
#include "CShaderLibClass.h"
#include "afxwin.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "AdvancedPropertiesCtrl.h"
#include "EditorState.h"

/////////////////////////////////////////////////////////////////////////////
// CGlobalAdvancedDlg dialog

class CGlobalAdvancedDlg : public CKEPEditDialog {
	// Construction
public:
	CGlobalAdvancedDlg(CWnd* pParent = NULL); // standard constructor
	CShaderObjList* m_shaderAvailable;
	std::deque<CStringW> m_shaderNamesW;
	// Dialog Data
	//{{AFX_DATA(CGlobalAdvancedDlg)
	enum { IDD = IDD_DIALOG_GLOBAL_ADVANCED };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGlobalAdvancedDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGlobalAdvancedDlg)
	virtual BOOL OnInitDialog();
	afx_msg LRESULT OnPropertyCtrlChanged(WPARAM wp, LPARAM lp);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();

public:
	// accessors
	int GetGlobalShaderID() const {
		return m_propAdvanced.GetGlobalShaderID();
	}
	bool GetContactSelection() const {
		return m_propAdvanced.GetContactSelection();
	}
	bool GetCalcEntityCollision() const {
		return m_propAdvanced.GetCalcEntityCollision();
	}
	bool GetWBuffer() const {
		return m_propAdvanced.GetWBuffer();
	}
	int GetTargetFPS() const {
		return m_propAdvanced.GetTargetFPS();
	}
	int GetInterpolationGlobal() const {
		return m_propAdvanced.GetInterpolationGlobal();
	}
	int GetResolutionHeight() const {
		return m_propAdvanced.GetResolutionHeight();
	}
	int GetResolutionWidth() const {
		return m_propAdvanced.GetResolutionWidth();
	}

	// modifiers
	void SetGlobalShaderID(int globalShaderID) {
		m_propAdvanced.SetGlobalShaderID(globalShaderID);
	}
	void SetContactSelection(bool bContactSelection) {
		m_propAdvanced.SetContactSelection(bContactSelection);
	}
	void SetCalcEntityCollision(bool bCalcEntityCollision) {
		m_propAdvanced.SetCalcEntityCollision(bCalcEntityCollision);
	}
	void SetWBuffer(bool bWBuffer) {
		m_propAdvanced.SetWBuffer(bWBuffer);
	}
	void SetTargetFPS(int targetFPS) {
		m_propAdvanced.SetTargetFPS(targetFPS);
	}
	void SetInterpolationGlobal(int interpolationGlobal) {
		m_propAdvanced.SetInterpolationGlobal(interpolationGlobal);
	}
	void SetResolutionHeight(int resolutionHeight) {
		m_propAdvanced.SetResolutionHeight(resolutionHeight);
	}
	void SetResolutionWidth(int resolutionWidth) {
		m_propAdvanced.SetResolutionWidth(resolutionWidth);
	}

private:
	CCollapsibleGroup m_colAdvanced;
	CButton m_grpAdvanced;
	CAdvancedPropertiesCtrl m_propAdvanced;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLOBALADVANCEDDLG_H__2FCBC761_18A9_11D3_BDB1_FC946FAFAD40__INCLUDED_)
