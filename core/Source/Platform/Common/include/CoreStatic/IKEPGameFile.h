///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "common/include/KEPCommon.h"
#include "KEPObList.h"
#include "afxcoll.h"
#include <vector>

#ifndef IKEPGAMEFILE_H
#define IKEPGAMEFILE_H

namespace KEP {

/**
* IKEPGameFile object serve as a wrapper that map game objects to a game file and
* provides funtion to save a file and determine whether associated objects are
* dirty and need to be saved
**/

class IKEPGameFile {
public:
public:
	IKEPGameFile(void);
	virtual ~IKEPGameFile(void);

public:
	/**
	*get current loaded file name
	**/
	virtual std::string GetFileName() = 0;

	/**
	*true if there are changes that are not yet saved
	**/
	virtual BOOL IsDirty() = 0;

	/**
	*serialize objects to file, default saves to current file
	**/
	virtual BOOL SaveFile(const std::string& fileName = "") = 0;
};

#endif

} // namespace KEP