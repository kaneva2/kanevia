///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CEXMeshClass.h"
#include "CMeshLodClass.h"
#include "CVertexAnimationModule.h"
#include "KEPObList.h"
#include "ExternalEXMesh.h"
#include "CStaticMeshObject.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {
class ReMesh;
class CMaterialObject;
class CStaticMeshObj;
class CMeshLODObjectList;

struct Ext_lockpoints {
	float x, y, z;
	D3DCOLOR diffuse;
};

class CReferenceObj : public CStaticMeshObj, public GetSet {
	DECLARE_SERIAL_SCHEMA(CReferenceObj, VERSIONABLE_SCHEMA | 7);

public:
	CReferenceObj();

	CStringA m_referenceName;
	//CEXMeshObj           * m_meshObject;
	CMeshLODObjectList* m_lodVisualList;
	CEXMeshObj* m_collisionModel; //NULL is none
	BNDBOX m_popoutBox;
	CVertexAnimationModule* m_animModule;
	BOOL m_animationUpdatedThisRound;
	BOOL m_faceCamera;
	int m_seqLoad;

public:
	Ext_lockpoints* m_extLockPoints; //if valid creates a batch parse
	int m_lockPointCount;
	float m_lodRange;
	int m_lodLevels;
	float m_billboardSz;

	ReMesh* m_ReMesh;
	void SetReMesh(ReMesh* mesh) {
		m_ReMesh = mesh;
	}
	ReMesh* GetReMesh() const {
		return m_ReMesh; // OVERRIDE CStaticMeshObj
	}
	Matrix44f m_matrix;
	std::shared_ptr<IExternalEXMesh> m_externalSource; // video mesh support
	CMaterialObject* m_material;
	ABBOX m_boundBox;
	FLOAT m_boundRadius;

	void SetExternalSource(std::shared_ptr<IExternalEXMesh> externalSource) {
		m_externalSource = std::move(externalSource);
	}
	IExternalEXMesh* GetExternalSource() {
		return m_externalSource.get();
	}

	void ClearExternalSource() {
		m_externalSource.reset();
		//if (REF_PTR_VALID(m_externalSource)) {
		//m_externalSource->Delete();
		//m_externalSource = 0;
		//}
	}

	FLOAT GetBoundingRadius();

	void Serialize(CArchive& ar);
	void InitFromPath(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void InitCollisionModel();
	void DeleteCollisionModel();
	BOOL hasCollision() {
		return m_collisionModel != NULL;
	}
	void SafeDelete();
	BOOL UpdateLods(BOOL filterTextureCarry = FALSE); // Merged with UpdateLods()
	void GenerateLockPointMesh(Matrix44f& matrix, Matrix44f objMatrix, IDirect3DDevice9* pd3dDevice, float distToCam);
	float InterpolateFloatsHiRes(
		float StartVec,
		float EndVec,
		float NumOfStages,
		float ThisStage);
	void GenerateLockPointMeshGPU(Matrix44f& matrix,
		Matrix44f objMatrix,
		IDirect3DDevice9* pd3dDevice,
		float distToCam,
		Vector3f& constOne,
		Vector3f& constTwo,
		Vector3f& constThree,
		Vector3f& constFour,
		int ovrdLOD);
	void GenerateGPUContants(Matrix44f& matrix,
		IDirect3DDevice9* pd3dDevice,
		float& distToCam,
		Vector3f& constOne,
		Vector3f& constTwo,
		Vector3f& constThree,
		Vector3f& constFour,
		Vector3f& constNorm,
		float& maxX,
		float& maxY,
		WORD& vCount);

	//! \brief Replace mesh with new data, rebuild ReMesh after replacement.
	//!
	//! Refactored from related UI class (CReferenceLibrary).
	//! This is supposed to be part of model class (CReferenceObj).
	void SetMesh(CEXMeshObj* newMesh);

	const Matrix44f& GetMatrix() const; // OVERRIDE CStaticMeshObj
	const ABBOX& GetBoundingBox() const; // OVERRIDE CStaticMeshObj
	const CMaterialObject* GetMaterial() const; // OVERRIDE CStaticMeshObj
	std::string GetName() const; // OVERRIDE CStaticMeshObj

	int GetLODCount() const;

	Vector3f GetPivot() const; // OVERRIDE CStaticMeshObj

	DECLARE_GETSET
};

class CReferenceObjList : public CKEPObList {
	DECLARE_SERIAL_SCHEMA(CReferenceObjList, VERSIONABLE_SCHEMA | 1);

public:
	CReferenceObjList() {}
	void SafeDelete();

	void ReinitAll(LPDIRECT3DDEVICE9 g_pd3dDevice);

	CReferenceObj* GetByIndex(int index);
	int GetReferenceIndexByName(const CStringA& name);
	POSITION GetReferencePositionByName(const CStringA& name);
	CReferenceObj* GetReferenceObjByName(const CStringA& name);

	void FlagAllForAnimUpdate();
	void Serialize(CArchive& ar);
};

} // namespace KEP