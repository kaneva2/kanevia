///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/WindowsAPIWrappers.h"
#include <string>
#include <stdlib.h>
#include <sstream>
#include <atomic>

#define CACHE_LINE_SIZE 64 // Bytes in a single cache line. Must be a literal to be used in declspec statements.
const size_t kiCacheLineSize = CACHE_LINE_SIZE;
//#define CACHE_ALIGN alignas(CACHE_LINE_SIZE)
#define CACHE_ALIGN __declspec(align(CACHE_LINE_SIZE))

namespace KEP {

enum class FlashScriptAccessMode;

#pragma pack(push, 16) // We don't need tight packing, but we need to ensure consistent packing across translation units.

class TripleBufferSynchronizer {
private:
	friend class TripleBufferWriterStateMgr;
	friend class TripleBufferReaderStateMgr;
	// Clients can only access the buffer index stored in m_idxReadyForClient.
	// Before access, the index must have the kiClientClaimBit set on it.
	// When the client is done accessing the buffer, the client sets the kiClientCompleted bit, but only if m_idxReadyForClient
	// hasn't changed since the client claimed it (kiClientClaimBit should still be set).
	static const uint32_t kiClientClaimBit = 0x40000000; // Set by the client to lock a buffer. Server won't modify buffers with this bit set unless the client release bit is also set.
	static const uint32_t kiClientReleasedBit = 0x80000000; // Set by the client to release a locked buffer.
	static const uint32_t kiInvalidStateBit = 0x20000000; // Set to indicate buffer is unused by both client and server.
	static const uint32_t kiStateMask = kiInvalidStateBit | kiClientClaimBit | kiClientReleasedBit;
	static bool IsLockedByClient(uint32_t value) {
		return (value & kiStateMask) == kiClientClaimBit; // Must be claimed, not released, and not invalid.
	}
	static bool IsReleasedByClient(uint32_t value) {
		return (value & kiStateMask) == (kiClientClaimBit | kiClientReleasedBit); // Must be claimed and released and not invalid.
	}

	// Index of buffer that is ready for the client to read.
	// If kiClientClaimBit is set, the client owns the buffer and there is no new data for the client.
	CACHE_ALIGN std::atomic<uint32_t> m_idxReadyForClient = kiInvalidStateBit;
};

class TripleBufferReaderStateMgr {
	static const uint32_t kiClientClaimBit = TripleBufferSynchronizer::kiClientClaimBit;
	static const uint32_t kiClientReleasedBit = TripleBufferSynchronizer::kiClientReleasedBit;
	static const uint32_t kiStateMask = TripleBufferSynchronizer::kiStateMask;
	static const uint32_t kiInvalidStateBit = TripleBufferSynchronizer::kiInvalidStateBit;

public:
	TripleBufferReaderStateMgr(TripleBufferSynchronizer* pSynchronizer = nullptr) :
			m_pSynchronizer(nullptr) {
		SetSynchronizer(pSynchronizer);
	}

	// If the client is currently using one of the buffers when this is called, it needs to be passed in as the second argument.
	void SetSynchronizer(TripleBufferSynchronizer* pSynchronizer) {
		m_pSynchronizer = pSynchronizer;
		m_idxClientOwned = 4;
	}

	// Releases current buffer and attempts to claim a new one.
	// Returns true if a new buffer was claimed.
	bool ClaimBuffer() {
		uint32_t idx = m_pSynchronizer->m_idxReadyForClient.fetch_or(kiClientClaimBit, std::memory_order_acq_rel);
		// We successfully claimed the buffer if it was not already claimed and it was not invalid.
		if (idx & (kiClientClaimBit | kiInvalidStateBit))
			return false;
		m_idxClientOwned = idx & 3;
		return true;
	}

	// Explicit release of buffer. This is not required, but can be used to provide lower latency replies to server.
	// Successfully claiming a new buffer implicitly releases the old. (Unsuccessful claims do not release the old buffer.)
	bool ReleaseBuffer() {
		uint32_t iOldValue = m_idxClientOwned | kiClientClaimBit; // This was the value it had when we claimed it.
		uint32_t iNewValue = iOldValue | kiClientReleasedBit; // Add the release bit to it.

		// memory_order_release only since no reading will be done after releasing buffer.
		bool bExchanged = m_pSynchronizer->m_idxReadyForClient.compare_exchange_strong(iOldValue, iNewValue, std::memory_order_release, std::memory_order_relaxed);
		return bExchanged;
	}
	uint32_t GetBufferIndex() const {
		return m_idxClientOwned;
	}

private:
	uint32_t m_idxClientOwned = 4; // Index of buffer that the client can read or write.
	TripleBufferSynchronizer* m_pSynchronizer = nullptr;
};

// Keeps track of state of TripleBufferSynchronizer so that server knows which buffer it is using, which buffer the client is (or may be) using, and which buffer the server last completed.
class TripleBufferWriterStateMgr {
	static const uint32_t kiClientClaimBit = TripleBufferSynchronizer::kiClientClaimBit;
	static const uint32_t kiClientReleasedBit = TripleBufferSynchronizer::kiClientReleasedBit;
	static const uint32_t kiStateMask = TripleBufferSynchronizer::kiStateMask;
	static const uint32_t kiInvalidStateBit = TripleBufferSynchronizer::kiInvalidStateBit;

public:
	TripleBufferWriterStateMgr(TripleBufferSynchronizer* pSynchronizer = nullptr) :
			m_pSynchronizer(nullptr) {
		SetSynchronizer(pSynchronizer);
	}

	// If the client is currently using one of the buffers when this is called, it needs to be passed in as the second argument.
	void SetSynchronizer(TripleBufferSynchronizer* pSynchronizer) {
		m_pSynchronizer = pSynchronizer;
		m_idxClientLastClaimed = 4;
		m_idxReadyForClient = 4;
		// Initial buffer is assigned to the client, but set kiClientClaimBit to indicate it does not have new data.
		if (m_pSynchronizer) {
			// No memory order requirements when initializing since client should not have accessed data before this, and
			// shouldn't access data until after CompletedWork() is called.
			m_pSynchronizer->m_idxReadyForClient.store(TripleBufferSynchronizer::kiInvalidStateBit, std::memory_order::memory_order_relaxed);
			std::atomic_thread_fence(std::memory_order_release); // To ensure that the above write is synchronized before whatever operation exposes this object to the other thread.
		}
		m_idxWorkArea = 1;
	}

	// This is the buffer the server can write to.
	uint32_t GetWorkAreaIndex() const {
		return m_idxWorkArea;
	}

	// Tells client that the current buffer is ready for it.
	// Calculates a new buffer to use for future work.
	bool CompletedWork() {
		if (!m_pSynchronizer)
			return false; // Error. Not initialized.

		uint32_t iPreviousReady = m_pSynchronizer->m_idxReadyForClient.exchange(m_idxWorkArea, std::memory_order_acq_rel);
		if (iPreviousReady & TripleBufferSynchronizer::kiClientClaimBit)
			m_idxClientLastClaimed = iPreviousReady & 3; // Client is now using iPrevious

		// Update our local m_iLatestWritten variable.
		m_idxReadyForClient = m_idxWorkArea;

		// Choose a new work area.
		// m_idxClientLastClaimed may be an invalid value (4). It will be treated as 0 in this formula.
		// m_idxWorkArea must not be initialized to 0 for this formula to work.
		m_idxWorkArea = 3 - (m_idxReadyForClient + (m_idxClientLastClaimed & 3));
		return true;
	}
	// Optional functionality. Clients are not required to explicitly release use of a buffer.
	// Claiming a new buffer implicitly releases the previous buffer use.
	bool IsCurrentBufferClaimedByClient() {
		uint32_t idx = m_pSynchronizer->m_idxReadyForClient.load(std::memory_order_acquire);
		return (idx & kiStateMask) == kiClientClaimBit;
	}

private:
	uint32_t m_idxReadyForClient = 4; // Index of buffer that is ready for client to use.
	uint32_t m_idxClientLastClaimed = 4; // Index of last buffer that client has claimed.
	uint32_t m_idxWorkArea = 1; // Index of buffer that is safe to modify.
	TripleBufferSynchronizer* m_pSynchronizer = nullptr;
};

namespace FlashControl {

// Used for logging.
inline std::string BuildFlashMeshIdString(const char* pszMediaParamDesc, uint32_t iMeshId, bool bVerbose) {
	std::stringstream strm;
	strm << "FM<" << iMeshId;
	if (bVerbose)
		strm << " " << pszMediaParamDesc;
	strm << ">";
	return strm.str();
}

template <typename T>
inline T RoundUpToMultipleOf(T value, std::common_type_t<T> roundToMultiple) {
	T t = value + (roundToMultiple - 1);
	t -= t % roundToMultiple;
	return t;
}

inline uint32_t CalculateSingleBitmapAllocSize(uint32_t iWidth, uint32_t iHeight) {
	size_t nBytesPerRow = RoundUpToMultipleOf(3 * iWidth, 4);
	size_t iSingleBitmapAllocSize = RoundUpToMultipleOf(nBytesPerRow * iHeight, kiCacheLineSize);
	return iSingleBitmapAllocSize;
}

// Name for shared memory for communicating bitmap data from a flash control server..
inline std::wstring BitmapSharedMemoryName(uint32_t iClientId, uint32_t iMeshId, uint32_t iBitmapId) {
	std::wostringstream strm;
	strm << L"Kaneva.Flash.BitmapMem." << std::hex << iClientId << L"." << std::hex << iMeshId << L"." << std::hex << iBitmapId;
	return strm.str();
}

// Name for shared memory for communicating to a server managing a single flash control.
inline std::wstring ServerSharedMemoryName(uint32_t iClientId, uint32_t iMeshId) {
	std::wostringstream strm;
	strm << L"Kaneva.Flash.ServerMem." << std::hex << iClientId << L"." << std::hex << iMeshId;
	return strm.str();
}

// Name for shared memory for communicating to Flash factory.
inline std::wstring FlashFactorySharedMemoryName(uint32_t iClientProcessId, uint32_t iFactoryId) {
	std::wostringstream strm;
	strm << L"Kaneva.Flash.FactoryMem." << std::hex << iClientProcessId << L"." << std::hex << iFactoryId;
	return strm.str();
}

// Name for event to notify client that the Flash factory has updated shared memory.
inline std::wstring FlashFactoryNotifyClientEventName(uint32_t iClientProcessId, uint32_t iFactoryId) {
	std::wostringstream strm;
	strm << L"Kaneva.Flash.FactoryNotifyClient." << std::hex << iClientProcessId << L"." << std::hex << iFactoryId;
	return strm.str();
}

// Name for event to notify factory (and servers that the factory controls) that the client has updated shared memory.
inline std::wstring FlashFactoryNotifyServerEventName(uint32_t iClientProcessId, uint32_t iFactoryId) {
	std::wostringstream strm;
	strm << L"Kaneva.Flash.FactoryNotifyServer." << std::hex << iClientProcessId << L"." << std::hex << iFactoryId;
	return strm.str();
}

inline bool CreateBitmapsInSharedMemory(HANDLE hSharedMemory, WindowsBitmapWrapper (&ahBitmaps)[3], void* (&apBitmapBits)[3], size_t iFirstBitmapDataOffset, uint32_t iBitmapWidth, uint32_t iBitmapHeight) {
	size_t iSingleBitmapAllocSize = CalculateSingleBitmapAllocSize(iBitmapWidth, iBitmapHeight);
	struct FLASHBITMAPINFO {
		BITMAPINFOHEADER bmi;
		DWORD bitfields[3];
	};
	FLASHBITMAPINFO fbmi;
	fbmi.bmi.biSize = sizeof(fbmi);
	fbmi.bmi.biWidth = iBitmapWidth;
	fbmi.bmi.biHeight = iBitmapHeight;
	fbmi.bmi.biPlanes = 1;
	fbmi.bmi.biBitCount = 16;
	fbmi.bmi.biCompression = BI_BITFIELDS;
	fbmi.bmi.biSizeImage = 0;
	fbmi.bmi.biXPelsPerMeter = 0;
	fbmi.bmi.biYPelsPerMeter = 0;
	fbmi.bmi.biClrUsed = 0;
	fbmi.bmi.biClrImportant = 0;
	fbmi.bitfields[0] = 0b1111100000000000; //5 MSB set for Red
	fbmi.bitfields[1] = 0b0000011111100000; //Next 6 for Green
	fbmi.bitfields[2] = 0b0000000000011111; //Next 5 For Blue
	for (unsigned int i = 0; i < 3; ++i) {
		void* pBits = nullptr;
		// Note: It looks like CreateDIBSection creates a new mapping of the shared memory.
		ahBitmaps[i].Set(CreateDIBSection(NULL, reinterpret_cast<const BITMAPINFO*>(&fbmi), DIB_RGB_COLORS, &pBits, hSharedMemory, iFirstBitmapDataOffset + i * iSingleBitmapAllocSize));
		apBitmapBits[i] = pBits;
		if (!ahBitmaps[i])
			return false;
	}
	return true;
}

template <typename T>
class VersionedVariable {
public:
	VersionedVariable() {}
	VersionedVariable(const VersionedVariable&) = delete;
	void operator=(const VersionedVariable&) = delete;

	void UpdateValue(const T& t) {
		++m_iVersion;
		m_Value = t;
	}
	T& GetRefToUpdate() {
		++m_iVersion;
		return m_Value;
	}
	const T& GetValue() const {
		return m_Value;
	}
	uint32_t GetVersion() const {
		return m_iVersion;
	}
	bool IsNewerThan(const VersionedVariable& other) const {
		return m_iVersion > other.m_iVersion;
	}
	bool UpdateFrom(const VersionedVariable& other) {
		if (!other.IsNewerThan(*this))
			return false;
		m_Value = other.m_Value;
		m_iVersion = other.m_iVersion;
		return true;
	}

private:
	uint32_t m_iVersion = 0;
	T m_Value;
};

#if 1 // VS2013 compiler bug workaround. Try code in #else branch after upgrade.
template <typename ClassType, typename T>
struct WorkAroundCompilerBugVS2013 {
	static void UpdateMember(const ClassType& from, ClassType& to, T ClassType::*pMember) {
		to.*pMember = from.*pMember;
	}
};
template <typename ClassType, typename T>
struct WorkAroundCompilerBugVS2013<ClassType, VersionedVariable<T>> {
	static void UpdateMember(const ClassType& from, ClassType& to, VersionedVariable<T> ClassType::*pMember) {
		(to.*pMember).UpdateFrom(from.*pMember);
	}
};
template <typename ClassType, typename T>
void UpdateMember(const ClassType& from, ClassType& to, T ClassType::*pMember) {
	WorkAroundCompilerBugVS2013<ClassType, T>::UpdateMember(from, to, pMember);
}
#else
template <typename ClassType, typename T>
void UpdateMember(const ClassType& from, ClassType& to, T ClassType::*pMember) {
	to.*pMember = from.*pMember;
}
template <typename ClassType, typename T>
void UpdateMember(const ClassType& from, ClassType& to, VersionedVariable<T> ClassType::*pMember) {
	(to.*pMember).UpdateFrom(from.*pMember);
}
#endif

template <typename ClassType>
void UpdateMembers(const ClassType& from, ClassType& to) {
}

template <typename ClassType, typename T, typename... Args>
void UpdateMembers(const ClassType& from, ClassType& to, T ClassType::*pNextMember, Args... remainingArgs) {
	UpdateMember(from, to, pNextMember);
	UpdateMembers(from, to, remainingArgs...);
}

template <size_t N>
class StringBuffer {
public:
	StringBuffer() {
		m_aCharacters[0] = 0;
	}
	StringBuffer(const StringBuffer& other) {
		*this = other;
	}
	const StringBuffer& operator=(const StringBuffer& other) {
		strcpy(m_aCharacters, other.m_aCharacters);
		return *this;
	}
	bool Set(const std::string& str) {
		if (str.size() >= N + 1)
			return false;
		std::copy(str.c_str(), str.c_str() + str.size() + 1, m_aCharacters);
		return true;
	}
	std::string GetString() const {
		return m_aCharacters;
	}

private:
	char m_aCharacters[N];
};

// Holds an array of size MaxSize with a count of valid elements.
// For simplicity, does not construct/destruct elements as they are added and removed.
// Instead they are all constructed at initialization.
template <typename T, size_t MaxSize>
class ArrayBuffer {
public:
	typedef T value_type;
	typedef T* iterator;
	typedef T* const_iterator;

	ArrayBuffer() :
			m_nSize(0) {}
	ArrayBuffer(const ArrayBuffer& other) :
			m_nSize(0) {
		operator=(other);
	}
	const ArrayBuffer& operator=(const ArrayBuffer& other) {
		if (this != &other) {
			for (size_t i = 0; i < other.m_aData[i]; ++i)
				m_aData[i] = other.m_aData[i];
			m_nSize = other.m_nSize;
		}
		return *this;
	}
	void pop_back() {
		--m_nSize;
	}
	bool push_back(const T& t) {
		if (m_nSize >= MaxSize)
			return false;
		Data()[m_nSize] = t;
		++m_nSize;
		return true;
	}
	void erase(const_iterator itr) {
		erase(itr, itr + 1);
	}
	void erase(const_iterator itr1_, const_iterator itr2_) {
		iterator itr1 = const_cast<iterator>(itr1_);
		iterator itr2 = const_cast<iterator>(itr2_);
		size_t nToMove = end() - itr2;
		for (size_t i = 0; i < nToMove; ++i)
			itr1[i] = std::move(itr2[i]);
		m_nSize -= itr2 - itr1;
	}
	template <typename ForwardIterator>
	bool insert(const_iterator where_, ForwardIterator first, ForwardIterator last) {
		iterator where = const_cast<iterator>(where_);
		size_t nToAdd = std::distance(first, last);
		size_t iNewSize = nToAdd + m_nSize;
		if (iNewSize > MaxSize)
			return false;
		size_t nToMove = end() - where;
		for (size_t i = 0; i < nToMove; ++i)
			Data()[iNewSize - (i + 1)] = std::move(Data()[m_nSize - (i + 1)]);
		InputIterator itr = first;
		for (size_t i = 0; i < nToAdd; ++i)
			where[i] = *itr++;
		m_nSize = iNewSize;
		return true;
	}
	template <typename ForwardIterator>
	bool assign(ForwardIterator first, ForwardIterator last) {
		size_t iNewSize = std::distance(first, last);
		bool bAssignedAll = true;
		if (iNewSize > MaxSize) {
			bAssignedAll = false;
			iNewSize = MaxSize;
		}
		ForwardIterator itr = first;
		for (size_t i = 0; i < iNewSize; ++i)
			Data()[i] = *itr++;
		m_nSize = iNewSize;
		return bAssignedAll;
	}

	size_t size() const {
		return m_nSize;
	}
	bool empty() const {
		return m_nSize == 0;
	}
	T& operator[](size_t i) {
		return Data()[i];
	}
	const T& operator[](size_t i) const {
		return Data()[i];
	}

	T* begin() {
		return Data();
	}
	T* end() {
		return Data() + size();
	}
	const T* begin() const {
		return cbegin();
	}
	const T* end() const {
		return cend();
	}
	const T* cbegin() const {
		return Data();
	}
	const T* cend() const {
		return Data() + size();
	}

private:
	T* Data() {
		return reinterpret_cast<T*>(&m_aData[0]);
	}
	const T* Data() const {
		return const_cast<ArrayBuffer*>(this)->Data();
	}

private:
	size_t m_nSize;
	T m_aData[MaxSize];
};

struct FlashStateData {
	FlashStateData() {}
	FlashStateData(const FlashStateData&) = delete;
	void operator=(const FlashStateData&) = delete;

	void UpdateFrom(const FlashStateData& other) {
		UpdateMembers(other, *this,
			&FlashStateData::m_CreateSettings,
			&FlashStateData::m_MediaSettings,
			&FlashStateData::m_RectSettings,
			&FlashStateData::m_LocalVolume,
			&FlashStateData::m_iGenerateBitmapId,
			&FlashStateData::m_iSetFocusId,
			&FlashStateData::m_iBitmapSharedMemoryId,
			&FlashStateData::m_iRewindRequestId,
			&FlashStateData::m_bQuit);
	}

	// This never changes.
	struct CreateSettings {
		uint32_t m_iMeshId = 0;
		uintptr_t /*HWND*/ m_iParentWindowHandle = 0;
		bool m_bWantInput = false;
	};
	struct RectSettings {
		uint32_t GetBitmapWidth() const {
			return m_iWidth - (m_iCropLeft + m_iCropRight);
		}
		uint32_t GetBitmapHeight() const {
			return m_iHeight - (m_iCropTop + m_iCropBottom);
		}

		int32_t m_iPositionX = 0;
		int32_t m_iPositionY = 0;
		uint32_t m_iWidth = 0;
		uint32_t m_iHeight = 0;

		uint32_t m_iCropTop = 0;
		uint32_t m_iCropLeft = 0;
		uint32_t m_iCropBottom = 0;
		uint32_t m_iCropRight = 0;
	};
	struct MediaSettings {
		StringBuffer<4096> m_bufferURL;
		StringBuffer<4096> m_bufferFlashVars;
		FlashScriptAccessMode m_eAllowScriptAccess;
	};

	VersionedVariable<CreateSettings> m_CreateSettings;
	VersionedVariable<RectSettings> m_RectSettings;
	VersionedVariable<MediaSettings> m_MediaSettings;

	VersionedVariable<float> m_LocalVolume; // Volume for this flash object.

	uint32_t m_iGenerateBitmapId = 0; // Id of bitmap to generate. If this is greater than the last bitmap generated, a new one should be generated.
	uint32_t m_iSetFocusId = 0; // Id of setfocus request. If zero, don't set focus. If greater than previous value, set the focus.
	uint32_t m_iRewindRequestId = 0; // Id of rewind request. If this number increases, rewind.
	bool m_bQuit = false;

	// Incremented when new bitmap shared memory is needed.
	// Used to identify named shared memory that should hold the bitmap data.
	// Handle not passed to simplify resource deallocation.
	uint32_t m_iBitmapSharedMemoryId = 0;
};

struct GeneratedBitmapStateData {
	uint32_t m_iGenerateBitmapId = 0; // Value of m_iGenerateBitmapId that m_idxBitmapBuffer is a response to.

	// Which of the three buffers contains the generated bitmap. This should match the index flash state request.
	// Set to 3 if no bitmap was generated. (e.g. for volume change requests.)
	uint32_t m_idxBitmapBuffer = 0;
};

struct VolumeSetting {
	VolumeSetting(float fVolume = 0, bool bMute = false) :
			m_fVolume(fVolume), m_bMute(bMute) {}

	float m_fVolume;
	bool m_bMute;
};

struct FlashSharedMemoryBlock {
	struct ClientRequest {
		FlashStateData m_FlashState;
	};
	struct ServerAcknowledgement {
		GeneratedBitmapStateData m_BitmapState;
	};

	FlashSharedMemoryBlock() {
	}

	//TripleBufferSynchronizer m_BitmapSynchronizer; // To be replaced by m_ClientRequestSynchronizer

	TripleBufferSynchronizer m_ClientRequestSynchronizer;
	TripleBufferSynchronizer m_ServerAcknowledgmentSynchronizer;

	CACHE_ALIGN ClientRequest m_aClientRequests[3];
	CACHE_ALIGN ServerAcknowledgement m_aServerAcknowledgements[3];

	//CACHE_ALIGN std::atomic<uint32_t> m_iLatestBitmapRequest = 0; // Each request gets a unique increasing ID. Requests less than this are ignored because they are out of date.
};

// Data written by server.
struct FactoryServerStateData {
private:
	// No implicit copies.
	FactoryServerStateData(const FactoryServerStateData&) = delete;
	FactoryServerStateData& operator=(const FactoryServerStateData&) = delete;

public:
	FactoryServerStateData() {}

	void UpdateFrom(const FactoryServerStateData& other) {
		UpdateMembers(other, *this,
			&FactoryServerStateData::m_InitData,
			&FactoryServerStateData::m_Volume);
	}

	struct InitData {
		bool m_bFlashIsInstalled = false;
		StringBuffer<256 - 8> m_strFlashVersion;
	};

	VersionedVariable<InitData> m_InitData;
	VersionedVariable<VolumeSetting> m_Volume;
};

// Data written by client.
struct FactoryClientStateData {
private:
	// No implicit copies.
	FactoryClientStateData& operator=(const FactoryClientStateData&) = delete;
	FactoryClientStateData(const FactoryClientStateData&) = delete;

public:
	FactoryClientStateData() {}
	void UpdateFrom(const FactoryClientStateData& other) {
		UpdateMembers(other, *this,
			&FactoryClientStateData::m_Quit,
			&FactoryClientStateData::m_aProviderIds,
			&FactoryClientStateData::m_Volume,
			&FactoryClientStateData::m_iIndividualRequestId);
	}

	enum class eQuit {
		DONT_QUIT,
		QUIT_NORMAL,
		QUIT_CRASH
	};
	eQuit m_Quit = eQuit::DONT_QUIT;

	// Each provider interfaces with a single instance of a Flash control.
	static const size_t knMaxProviders = 1000;
	VersionedVariable<ArrayBuffer<uint32_t, knMaxProviders>> m_aProviderIds;

	VersionedVariable<VolumeSetting> m_Volume;

	// Incremented by client to indicate one of the single control servers has data available.
	uint32_t m_iIndividualRequestId = 0;
};

struct FlashFactorySharedMemoryBlock {
	FlashFactorySharedMemoryBlock() {}

	TripleBufferSynchronizer m_ClientDataSynchronizer;
	TripleBufferSynchronizer m_ServerDataSynchronizer;

	// To ensure each element of array is cache aligned, we wrap client and server data
	// in an anonymous cache-aligned struct.
	CACHE_ALIGN struct AlignedServerStateData : FactoryServerStateData {};
	CACHE_ALIGN struct AlignedClientStateData : FactoryClientStateData {};

	AlignedServerStateData m_aServerData[3];
	AlignedClientStateData m_aClientData[3];
};
#pragma pack(pop)

} // namespace FlashControl
} // namespace KEP
