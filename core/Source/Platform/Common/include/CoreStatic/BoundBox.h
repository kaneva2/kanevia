///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CStructuresMisl.h"
#include <math.h>
#include <utility>

#undef min
#undef max

namespace KEP {

class BoundBox : public ABBOX {
public:
	BoundBox() {
		invalidate();
	}
	BoundBox(const ABBOX& abBox) {
		set(abBox);
	}
	BoundBox(const BNDBOX& bndBox) {
		set(bndBox);
	}
	BoundBox(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
		set(minX, minY, minZ, maxX, maxY, maxZ);
	}

	bool operator==(const BoundBox& aBox) const {
		return minX == aBox.minX && minY == aBox.minY && minZ == aBox.minZ &&
			   maxX == aBox.maxX && maxY == aBox.maxY && maxZ == aBox.maxZ;
	}

	bool operator!=(const BoundBox& aBox) const {
		return !(*this == aBox);
	}

	// true if box contains something
	bool isValid() const {
		return minX != FLT_MAX && maxX != -FLT_MAX && minY != FLT_MAX && maxY != -FLT_MAX && minZ != FLT_MAX && maxZ != -FLT_MAX;
	}

	// true if box contains exactly one point
	bool isDegenerated() const {
		return minX == maxX && minY == maxY && minZ == maxZ;
	}

	void invalidate() {
		minX = FLT_MAX;
		maxX = -FLT_MAX;
		minY = FLT_MAX;
		maxY = -FLT_MAX;
		minZ = FLT_MAX;
		maxZ = -FLT_MAX;
	}

	void validate(bool bPreventDegeneration = true, bool bSort = false) {
		if (minX >= FLT_MAX || maxX <= -FLT_MAX)
			minX = maxX = 0.0f;

		if (minY >= FLT_MAX || maxY <= -FLT_MAX)
			minY = maxY = 0.0f;

		if (minZ >= FLT_MAX || maxZ <= -FLT_MAX)
			minZ = maxZ = 0.0f;

		if (bSort) {
			// Sort coordinates to make sure min strictly <= max
			if (minX > maxX)
				std::swap(minX, maxX);
			if (minY > maxY)
				std::swap(minY, maxY);
			if (minZ > maxZ)
				std::swap(minZ, maxZ);
		}

		if (bPreventDegeneration) {
			if (minX == maxX)
				maxX = minX + 0.01f;

			if (minY == maxY)
				maxY = minY + 0.01f;

			if (minZ == maxZ)
				maxZ = minZ + 0.01f;
		}
	}

	void set(const ABBOX& aBox) {
		minX = aBox.minX;
		minY = aBox.minY;
		minZ = aBox.minZ;
		maxX = aBox.maxX;
		maxY = aBox.maxY;
		maxZ = aBox.maxZ;
	}

	void set(const BNDBOX& aBox) {
		minX = aBox.min.X();
		minY = aBox.min.Y();
		minZ = aBox.min.Z();
		maxX = aBox.max.X();
		maxY = aBox.max.Y();
		maxZ = aBox.max.Z();
	}

	void set(float minX_, float minY_, float minZ_, float maxX_, float maxY_, float maxZ_) {
		this->minX = minX_;
		this->minY = minY_;
		this->minZ = minZ_;
		this->maxX = maxX_;
		this->maxY = maxY_;
		this->maxZ = maxZ_;
	}

	BoundBox merge(const BoundBox& aBox) {
		if (minX > aBox.minX)
			minX = aBox.minX;

		if (maxX < aBox.maxX)
			maxX = aBox.maxX;

		if (minY > aBox.minY)
			minY = aBox.minY;

		if (maxY < aBox.maxY)
			maxY = aBox.maxY;

		if (minZ > aBox.minZ)
			minZ = aBox.minZ;

		if (maxZ < aBox.maxZ)
			maxZ = aBox.maxZ;

		return *this;
	}

	BoundBox merge(float x, float y, float z) {
		return merge(BoundBox(x, y, z, x, y, z));
	}

	bool overlaps(const BoundBox& aBox) const {
		return intersectHelper(aBox, NULL);
	}

	BoundBox intersect(const BoundBox& aBox) const {
		BoundBox res;
		intersectHelper(aBox, &res);
		return res;
	}

	BoundBox operator+(const BoundBox& aBox) const {
		BoundBox tmp = *this;
		return tmp.merge(aBox);
	}

	BoundBox operator*(float scaleFactor) const {
		float x, y, z;
		getCenter(x, y, z);
		BoundBox norm = getNormalizedBox();

		return BoundBox(x + norm.minX * scaleFactor, y + norm.minY * scaleFactor, z + norm.minZ * scaleFactor,
			x + norm.maxX * scaleFactor, y + norm.maxY * scaleFactor, z + norm.maxZ * scaleFactor);
	}

	operator BNDBOX() const {
		BNDBOX retBox;
		retBox.min.X() = minX;
		retBox.min.Y() = minY;
		retBox.min.Z() = minZ;
		retBox.max.X() = maxX;
		retBox.max.Y() = maxY;
		retBox.max.Z() = maxZ;
		return retBox;
	}

	float getSizeX() const {
		return fabs(maxX - minX);
	}
	float getSizeY() const {
		return fabs(maxY - minY);
	}
	float getSizeZ() const {
		return fabs(maxZ - minZ);
	}
	BoundBox getSizeBox() const {
		return BoundBox(0, 0, 0, getSizeX(), getSizeY(), getSizeZ());
	}

	void getCenter(float& x, float& y, float& z) const {
		x = (minX + maxX) / 2;
		y = (minY + maxY) / 2;
		z = (minZ + maxZ) / 2;
	}

	BoundBox getNormalizedBox() const {
		float x, y, z;
		getCenter(x, y, z);
		return BoundBox(minX - x, minY - y, minZ - z, maxX - x, maxY - y, maxZ - z);
	}

	void getVertices(float vertices[8][3]) {
		// Compose 8 vertices for the box
		int idx = 0;
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 2; j++)
				for (int k = 0; k < 2; k++, idx++) {
					vertices[idx][0] = i == 0 ? minX : maxX;
					vertices[idx][1] = j == 0 ? minY : maxY;
					vertices[idx][2] = k == 0 ? minZ : maxZ;
				}
	}

	bool contains(float x, float y, float z) const {
		return x >= minX && x <= maxX && y >= minY && y <= maxY && z >= minZ && z <= maxZ;
	}

#ifdef DIRECT3D_VERSION
	// D3D helpers
	bool contains(const Vector3f& v) const {
		return contains(v.x, v.y, v.z);
	}
	BoundBox merge(const Vector3f& v) {
		return merge(v.x, v.y, v.z);
	}
	void set(const Vector3f& min, const Vector3f& max) {
		set(min.x, min.y, min.z, max.x, max.y, max.z);
	}
#endif

	void scale(const Vector3f& vScale) {
		minX *= vScale.X();
		maxX *= vScale.X();
		minY *= vScale.Y();
		maxY *= vScale.Y();
		minZ *= vScale.Z();
		maxZ *= vScale.Z();
	}

protected:
	bool intersectHelper(const BoundBox& aBox, BoundBox* result) const {
		float Xa = std::max(minX, aBox.minX);
		float Xb = std::min(maxX, aBox.maxX);
		if (Xa <= Xb) {
			float Ya = std::max(minY, aBox.minY);
			float Yb = std::min(maxY, aBox.maxY);
			if (Ya <= Yb) {
				float Za = std::max(minZ, aBox.minZ);
				float Zb = std::min(maxZ, aBox.maxZ);
				if (Za <= Zb) {
					if (result) {
						result->set(Xa, Ya, Za, Xb, Yb, Zb);
						result->validate();
					}
					return true;
				}
			}
		}

		return false;
	}
};

} // namespace KEP