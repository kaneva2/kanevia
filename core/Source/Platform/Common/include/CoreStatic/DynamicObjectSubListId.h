///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

enum DynObjSubListId {
	DynObjSubListId_NeedsUpdate,
	DynObjSubListId_BoundingBoxRender,
	DynObjSubListId_HasMedia,
	DynObjSubListId_HasLight,
	//DynObjSubListId_HasAnimation,
	//DynObjSubListId_HasParticles,

	NumberOfDynObjSubListIds
};

}
