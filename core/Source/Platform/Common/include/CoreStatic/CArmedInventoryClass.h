///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// added by serialize.pl
#include <GetSet.h>
#include "KEPObList.h"
#include "Glids.h"
#include "Core/Util/CArchiveObjectSchema.h"

#include "common\include\CoreStatic\AnimSettings.h"

namespace KEP {

class CSkeletonObject;
class ReSkinMesh;
class RuntimeSkeleton;
class SkeletonConfiguration;
class IMemSizeGadget;

enum eVisualType {
	FirstPerson,
	ThirdPerson
};

class CArmedInventoryObj : public CObject, public GetSet {
public:
	enum EISize {
		EI_Size_Undefined = 0,
		EI_Size_Small,
		EI_Size_Medium,
		EI_Size_Large
	};

	static const float EI_SMALL_SIZE;
	static const float EI_MED_SIZE;

protected:
	CArmedInventoryObj() {
		init();
	}

	DECLARE_SERIAL_SCHEMA(CArmedInventoryObj, VERSIONABLE_SCHEMA | 6)

protected:
	void init();

public:
	CArmedInventoryObj(const std::string& name);
	~CArmedInventoryObj();

	void Clone(CArmedInventoryObj** copy, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Serialize(CArchive& ar);

	const std::string& getName() const { return m_name; }
	const std::string& getAttachedBoneName() const { return m_attachedBoneName; }
	GLID getGlobalId() const { return m_globalId; }
	void setGlobalId(const GLID& glid) { m_globalId = glid; }
	int getAttachedBoneIndex() const { return m_attachedBoneIndex; }
	void setAttachedBoneIndex(int boneIndex) { m_attachedBoneIndex = boneIndex; }
	void setAttachedBone(int boneIndex, const std::string& boneName) {
		m_attachedBoneIndex = boneIndex;
		m_attachedBoneName = boneName;
	}
	int getAbilityTypeUse() const { return m_abilityTypeUse; }
	void setAbilityTypeUse(int val) { m_abilityTypeUse = val; }
	int getLocationOccupancy() const { return m_locationOccupancy; }
	void setLocationOccupancy(int val) { m_locationOccupancy = val; }

	void setPrimaryAnimationSettings(const AnimSettings& animSettings);

	bool getPrimaryAnimationSettings(AnimSettings& animSettings) const;

	BOOL isConfigurable();

	void calculateSize();
	int countReMeshes() const;
	ReSkinMesh** gatherMeshes();

	void setScale(const Vector3f& s) {
		m_scale = s;
		m_bMatrixDirty = true;
	}
	void setTranslation(const Vector3f& t) {
		m_position = t;
		m_bMatrixDirty = true;
	}
	void setRotationZXY(const Vector3f& r) {
		m_rotation = r;
		m_bMatrixDirty = true;
	}
	void setIgnoreParentScale(bool b) {
		m_bIgnoreParentScale = b;
	}

	const Vector3f& getScale() const {
		return m_scale;
	}
	const Vector3f& getPosition() const {
		return m_position;
	}
	const Vector3f& getRotationZXY() const {
		return m_rotation;
	}
	bool getIgnoreParentScale() const {
		return m_bIgnoreParentScale;
	}

	Matrix44f* getRelativeMatrix();

	BOOL isScaled() const {
		return abs(m_scale.x - 1.0f) > 1E-6 || abs(m_scale.y - 1.0f) > 1E-6 || abs(m_scale.z - 1.0f) > 1E-6;
	}

	void realizeDimConfig(eVisualType type, CArmedInventoryObj* pRefObj);

	RuntimeSkeleton* getSkeleton(eVisualType type = eVisualType::ThirdPerson);
	const RuntimeSkeleton* getSkeleton(eVisualType type = eVisualType::ThirdPerson) const;

	SkeletonConfiguration* getSkeletonConfig(eVisualType type = eVisualType::ThirdPerson);
	const SkeletonConfiguration* getSkeletonConfig(eVisualType type = eVisualType::ThirdPerson) const;

	// Called by importer only
	void setThirdPersonSkeleton(CSkeletonObject* pSkeletonObj) { m_thirdPersonSystem = pSkeletonObj; }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	template <typename ClassPtr, typename SkelPtr>
	static SkelPtr GetSkeleton(ClassPtr self, eVisualType type);

	template <typename ClassPtr, typename ConfPtr>
	static ConfPtr GetSkeletonConfig(ClassPtr self, eVisualType type);

private:
	CSkeletonObject* m_firstPersonSystem;
	CSkeletonObject* m_thirdPersonSystem;
	std::string m_name;
	GLID m_globalId;
	int m_attachedBoneIndex;
	std::string m_attachedBoneName;
	int m_locationOccupancy; //the same cannot occupy at the same time

	int m_abilityTypeUse; // used by importer
	EISize m_equippableSize; // used by importer only

	Matrix44f* m_matrixCache;
	bool m_bMatrixDirty;
	Vector3f m_position;
	Vector3f m_scale;
	Vector3f m_rotation;
	bool m_bIgnoreParentScale;

	DECLARE_GETSET
};

class CArmedInventoryList : public CKEPObList {
public:
	CArmedInventoryList() {}

	void Clone(CArmedInventoryList** copy, LPDIRECT3DDEVICE9 g_pd3dDevice) const;
	void Serialize(CArchive& ar);
	void SafeDelete();

	// to increase performance, an std::map with the type as the key
	// and the CArmedInventoryObj as the value could be added.
	// the map would need to be updated whenever the CObList changed.
	CArmedInventoryObj* getByGLID(const GLID& glid);
	const CArmedInventoryObj* getByGLID(const GLID& glid) const;
	void getAllGLIDs(std::vector<GLID>& out) const;
	void getAllItems(std::vector<const CArmedInventoryObj*>& out) const;
	void getAllItems(std::vector<CArmedInventoryObj*>& out);

	CArmedInventoryObj* removeByGLID(const GLID& glid);
	size_t deleteAllWithGLID(const GLID& glid);

	int CountReMeshes();
	ReSkinMesh** GatherMeshes();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	template <typename ListPtrType, typename ObjPtrType>
	static ObjPtrType GetByGLID(ListPtrType list, const GLID& glid);

	template <typename ListPtrType, typename ObjPtrType>
	static void GetAllItems(ListPtrType list, std::vector<ObjPtrType>& out);

	DECLARE_SERIAL_SCHEMA(CArmedInventoryList, VERSIONABLE_SCHEMA | 1)
};

class IEquippableProxy {
public:
	virtual CArmedInventoryObj* GetData(bool bImmediate = true) = 0;
};

class IEquippableManager {
public:
	virtual IEquippableProxy* GetProxy(int glid, bool bCreateIfNotExist) = 0;
};

} // namespace KEP
