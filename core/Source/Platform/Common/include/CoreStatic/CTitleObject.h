///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSetLt.h"
#include "GetSetStrings.h"
#include "KEPObVector.h"

namespace KEP {

class CTitleObject : GETSET_LT_BASE {
public:
	CTitleObject() :
			m_titleID(-1) {}
	int getID() const {
		return m_titleID;
	}

	std::string m_titleName;
	int m_titleID;

	void Write(CArchive& ar) const;
	void Read(CArchive& ar);

#pragma region GetSet(CTitleObject, IDS_TITLEOBJECT_NAME)
	BEGIN_GETSET_LT(std::string)
	GETSET_LT_RO(m_titleName, IDS_TITLEOBJECT_TITLENAME)
	END_GETSET_LT
	BEGIN_GETSET_LT(int)
	GETSET_LT_RO(m_titleID, IDS_TITLEOBJECT_TITLEID)
	END_GETSET_LT
#pragma endregion
};

class CTitleObjectList : public KEPObVector<CTitleObject> {
	// Use a dummy inheritance instead of typedef so that forward declaration works.
};

} // namespace KEP