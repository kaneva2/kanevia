///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class CAITraceObj : public CObject, public GetSet {
	DECLARE_SERIAL(CAITraceObj);

public:
	CAITraceObj();
	CStringA m_NameOfLocation;
	Vector3f m_locationCenter;
	float m_toleranceOfLocation;

	BOOL SetData(CStringA locationName,
		Vector3f position,
		float Tolerance);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CAITraceObjList : public CObList {
public:
	CAITraceObjList(){};

	DECLARE_SERIAL(CAITraceObjList);
};

} // namespace KEP