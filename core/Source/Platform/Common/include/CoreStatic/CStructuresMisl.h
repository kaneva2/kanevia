///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPCommon.h"

#include "Glids.h"
#include "CStructuresMath.h"
#include "Core/Util/HighPrecisionTime.h"
#include "MoveData.h"
#include "event\events\AttribEvents.h"

#pragma pack(push, 1)

namespace KEP {

typedef int PLAYER_HANDLE;

#define MAX_USER_NAME_LEN 64
#define MD5_DIGEST_LEN 16


// Enumerated Message Categories
// Do not change enum order or remove existing values
// Always add new value to the end of the list
#define MSG_TO_EITHER 0x00 // message to either server or client
#define MSG_TO_SERVER 0x10 // message to server
#define MSG_TO_CLIENT 0x20 // message to client
enum class MSG_CAT : BYTE {
	NONE = 0 + MSG_TO_EITHER,
	EVENT = 1 + MSG_TO_EITHER,
	GZIP = 2 + MSG_TO_EITHER,
	// Messages To Server
	LOG_TO_SERVER = 0 + MSG_TO_SERVER, // -> LOG_TO_CLIENT
	ATTRIB_TO_SERVER = 1 + MSG_TO_SERVER,
	SPAWN_TO_SERVER = 2 + MSG_TO_SERVER,
	UPDATE_TO_SERVER = 3 + MSG_TO_SERVER,
	MOVE_TO_SERVER = 4 + MSG_TO_SERVER,
	CREATE_CHAR_TO_SERVER = 5 + MSG_TO_SERVER,
	// Messages To Client
	LOG_TO_CLIENT = 0 + MSG_TO_CLIENT,
	ATTRIB_TO_CLIENT = 1 + MSG_TO_CLIENT,
	SPAWN_TO_CLIENT = 2 + MSG_TO_CLIENT,
	UPDATE_TO_CLIENT = 3 + MSG_TO_CLIENT,
	MOVE_TO_CLIENT = 4 + MSG_TO_CLIENT,
	BLOCK_ITEM_TO_CLIENT = 5 + MSG_TO_CLIENT,
#if (DRF_OLD_VENDORS == 1)
	BLOCK_STORE_TO_CLIENT = 6 + MSG_TO_CLIENT
#endif
};

inline const char* MsgCatStr(const MSG_CAT& msgCat) {
	switch (msgCat) {
		case MSG_CAT::NONE: return "NONE";
		case MSG_CAT::EVENT: return "EVENT";
		case MSG_CAT::GZIP: return "GZIP";
		case MSG_CAT::LOG_TO_SERVER: return "LOG_TO_SERVER";
		case MSG_CAT::ATTRIB_TO_SERVER: return "ATTRIB_TO_SERVER";
		case MSG_CAT::SPAWN_TO_SERVER: return "SPAWN_TO_SERVER";
		case MSG_CAT::UPDATE_TO_SERVER: return "UPDATE_TO_SERVER";
		case MSG_CAT::MOVE_TO_SERVER: return "MOVE_TO_SERVER";
		case MSG_CAT::CREATE_CHAR_TO_SERVER: return "CREATE_CHAR_TO_SERVER";
		case MSG_CAT::LOG_TO_CLIENT: return "LOG_TO_CLIENT";
		case MSG_CAT::ATTRIB_TO_CLIENT: return "ATTRIB_TO_CLIENT";
		case MSG_CAT::SPAWN_TO_CLIENT: return "SPAWN_TO_CLIENT";
		case MSG_CAT::UPDATE_TO_CLIENT: return "UPDATE_TO_CLIENT";
		case MSG_CAT::MOVE_TO_CLIENT: return "MOVE_TO_CLIENT";
		case MSG_CAT::BLOCK_ITEM_TO_CLIENT: return "BLOCK_ITEM_TO_CLIENT";
#if (DRF_OLD_VENDORS == 1)
		case MSG_CAT::BLOCK_STORE_TO_CLIENT: return "BLOCK_STORE_TO_CLIENT";
#endif
	}
	return "UNKNOWN";
}

struct MSG_HEADER {
private:
	MSG_CAT m_msgCat;
	BYTE m_msgVer;

public:
	MSG_HEADER(const MSG_CAT& msgCat = MSG_CAT::NONE, BYTE msgVer = 0) :
			m_msgCat(msgCat), m_msgVer(msgVer) {}
	MSG_CAT GetCategory() const { return m_msgCat; }
	void SetCategory(const MSG_CAT& msgCat) { m_msgCat = msgCat; }
	BYTE GetVersion() const { return m_msgVer; }
	void SetVersion(BYTE msgVer) { m_msgVer = msgVer; }
};

template <typename HT, typename PT>
struct HEADER_PAYLOAD_STRUCT {
	typedef HT HeaderType;
	typedef PT PayloadType;
	HeaderType m_header;
	PayloadType m_data[1];

	static size_t computeSize(size_t numPayloadItems) {
		return sizeof(HeaderType) + sizeof(PayloadType) * numPayloadItems;
	}

	static size_t computePayloadCount(size_t numBytes) {
		if (numBytes < (sizeof(HeaderType) + sizeof(PayloadType)))
			return 0;
		return (numBytes - sizeof(HeaderType)) / sizeof(PayloadType);
	}
};

// MSG_CAT::GZIP Header
struct MSG_GZIP_HEADER {
	MSG_CAT m_msgCat = MSG_CAT::GZIP;
	unsigned long m_uncompressedSize;
	// followed by [compressed gzip data ...]
};

// MSG_CAT::ATTRIB_TO_SERVER/CLIENT Payload Data
// MSG_CAT::UPDATE_TO_SERVER/CLIENT Payload Data
struct ATTRIB_DATA {
	eAttribEventType aeType = eAttribEventType::None;
	short aeShort1 = 0; // usually netTraceId
	short aeShort2 = 0;
	int aeInt1 = 0;
	int aeInt2 = 0;
};

// MSG_CAT::ATTRIB_TO_SERVER Header
// MSG_CAT::UPDATE_TO_SERVER/CLIENT Payload Data
struct MSG_ATTRIB : MSG_HEADER {
	MSG_ATTRIB() :
			MSG_HEADER(MSG_CAT::NONE) {}
	ATTRIB_DATA m_data;
};
typedef HEADER_PAYLOAD_STRUCT<MSG_HEADER, ATTRIB_DATA> MSG_ATTRIB_BLOCK;

// MSG_CAT::UPDATE_TO_SERVER/CLIENT Header
struct MSG_UPDATE : MSG_HEADER {
	MSG_UPDATE() :
			MSG_HEADER(MSG_CAT::NONE) {}
	unsigned short m_attribMsgs;
	unsigned short m_equipMsgs;
	unsigned short m_generalMsgs;
	unsigned short m_eventMsgs;
	// followed by [attrMsgs x ATTRIB_DATA]
	// followed by [equipMsgs x EQUIP_DATA]
	// followed by [generalMsgs x GENERAL_DATA]
	// followed by [eventMsgs x msgOutboxEvent]
};

// MSG_CAT::UPDATE_TO_SERVER/CLIENT Payload Data
struct EQUIP_DATA {
	short netTraceId;
	unsigned char glids;
	// followed by [glids x GLID]
};

// MSG_CAT::UPDATE_TO_SERVER/CLIENT Payload Data
struct GENERAL_DATA {
	unsigned char m_data;
};

// MSG_CAT::LOG_TO_SERVER Header
struct MSG_LOG_TO_SERVER : MSG_HEADER {
	MSG_LOG_TO_SERVER() :
			MSG_HEADER(MSG_CAT::LOG_TO_SERVER) {}
	char logInName[MAX_USER_NAME_LEN + 1];
	char logInPass[MD5_DIGEST_LEN * 2 + 1];
	TimeMs timeLogToServer; // client time at LOG_TO_SERVER
};

// MSG_CAT::SPAWN_TO_SERVER Header
struct MSG_SPAWN_TO_SERVER : MSG_HEADER {
	MSG_SPAWN_TO_SERVER() :
			MSG_HEADER(MSG_CAT::SPAWN_TO_SERVER) {}
	short dbIndex;
	int currentTrace;
	short usePresets;
	short characterInUse;
	short aiCfgIndex;
	long zoneIndex;
	char worldName[1]; // place holder for full name
};

// MSG_CAT::MOVE_TO_SERVER Header (encoded MOVE_DATA)
struct MSG_MOVE_TO_SERVER : MSG_HEADER {
	MSG_MOVE_TO_SERVER() :
			MSG_HEADER(MSG_CAT::MOVE_TO_SERVER) {}
	short netTraceId;
	MOVE_FIELDS fields;
	// followed by MOVE_DATA::Encode() use Decode()
};

// MSG_CAT::CREATE_CHAR_TO_SERVER Header
struct MSG_CREATE_CHAR_TO_SERVER : MSG_HEADER {
	MSG_CREATE_CHAR_TO_SERVER() :
			MSG_HEADER(MSG_CAT::CREATE_CHAR_TO_SERVER) {}
	short m_entityType;
	short m_spawnCfg;
	short m_nameLength;
	short m_numCharItems;
	short m_numCharConfigItems;
	short m_team;
	float m_scaleX;
	float m_scaleY;
	float m_scaleZ;
	// followed by [numCharItems x CHAR_ITEM_DATA]
	// followed by [numCharConfigItems x CHAR_CONFIG_ITEM_DATA]
};

// MSG_CAT::CREATE_CHAR_TO_SERVER Payload Data
struct CHAR_ITEM_DATA {
	GLID m_GLID;
	short m_quanity;
};

// MSG_CAT::CREATE_CHAR_TO_SERVER Payload Data
struct CHAR_CONFIG_ITEM_DATA {
	GLID_OR_BASE m_ccGlidOrBase; // CharConfig GLID_OR_BASE (0=base item (body) otherwise glid of equipped item)
	short m_ccSlot; // CharConfig slot
	short m_ccMeshId;
	short m_ccMatlId;
	float m_ccR;
	float m_ccG;
	float m_ccB;
};

// MSG_CAT::LOG_TO_CLIENT Header
struct MSG_LOG_TO_CLIENT : MSG_HEADER {
	MSG_LOG_TO_CLIENT() :
			MSG_HEADER(MSG_CAT::LOG_TO_CLIENT) {}
	TimeMs timeLogToServer; // client time at LOG_TO_SERVER
	TimeMs timeProcessing; // processing time LOG_TO_SERVER -> LOG_TO_CLIENT
	TimeMs timeLogToClient; // server time at LOG_TO_CLIENT
};

// MSG_CAT::SPAWN_TO_CLIENT Header
struct MSG_SPAWN_TO_CLIENT : MSG_HEADER {
	MSG_SPAWN_TO_CLIENT() :
			MSG_HEADER(MSG_CAT::SPAWN_TO_CLIENT) {}
	short dbIndex;
	int aiConfigDeprecated;
	float x;
	float y;
	float z;
	long zoneIndexInt;
	long instanceId;
	signed char rotation;
	char exgFileName[1]; // place holder for full name, and url
};

// MSG_CAT::MOVE_TO_CLIENT Header
struct MSG_MOVE_TO_CLIENT : MSG_HEADER {
	MSG_MOVE_TO_CLIENT() :
			MSG_HEADER(MSG_CAT::MOVE_TO_CLIENT) {}
	unsigned short m_numMoveToClientData;
	unsigned short m_numStaticEntityData;
	unsigned short m_numSpawnStaticMOData;
	// followed by [numMoveToClientData x MOVE_TO_CLIENT_DATA]
	// followed by [numStaticEntityData x STATIC_ENTITY_DATA]
	// followed by [numSpawnStaticMOData x SPAWN_STATIC_MO_DATA]
};

// MSG_CAT::MOVE_TO_CLIENT Payload Data (encoded MOVE_DATA)
struct MOVE_TO_CLIENT_DATA {
	short netTraceId;
	MOVE_FIELDS fields;
	// followed by MOVE_DATA::Encode() use Decode()
};

// MSG_CAT::MOVE_TO_CLIENT Payload Data
struct STATIC_ENTITY_DATA {
	short netTraceIdSpecial; // <0=static >0=regular movement object
};

// MSG_CAT::MOVE_TO_CLIENT Payload Data
struct SPAWN_STATIC_MO_DATA_v0 {
	BYTE m_siType; // from SERVER_ITEM_TYPE
	BYTE m_nameLength; // from size_t
	BYTE m_charConfigItems; // from size_t
	BYTE m_glidsArmed; // from size_t
	short dbIndex; // from size_t
	short netTraceId;
	float m_rotation;
	float Xpos;
	float Ypos;
	float Zpos;
	GLID glidAnimSpecial; // from int (<0=reverse)
	int m_placementId; // yanfeng - added
	// followed by [nameLength x BYTE]
	// followed by [charConfigItems x CHAR_CONFIG_DATA]
	// followed by [glidsArmed x GLID]
};

// Version 1
struct SPAWN_STATIC_MO_DATA {
	BYTE m_siType; // from SERVER_ITEM_TYPE
	BYTE m_nameLength; // from size_t
	BYTE m_charConfigItems; // from size_t
	BYTE m_glidsArmed; // from size_t
	short dbIndex; // from size_t
	Vector3f scale; // Version 1 - added scale vector
	short netTraceId;
	float m_rotation;
	float Xpos;
	float Ypos;
	float Zpos;
	GLID glidAnimSpecial; // from int (<0=reverse)
	int m_placementId; // yanfeng - added
	// followed by [nameLength x BYTE]
	// followed by [charConfigItems x CHAR_CONFIG_DATA]
	// followed by [glidsArmed x GLID]
};

// MSG_CAT::MOVE_TO_CLIENT Payload Data
struct CHAR_CONFIG_DATA {
	unsigned short m_meshId;
	unsigned short m_matlId;
};

// MSG_CAT::BLOCK_ITEM_TO_CLIENT Header
struct MSG_BLOCK_ITEM_TO_CLIENT_HEADER : MSG_HEADER {
	MSG_BLOCK_ITEM_TO_CLIENT_HEADER() :
			MSG_HEADER(MSG_CAT::BLOCK_ITEM_TO_CLIENT) {}
	short m_miscInt;
	BYTE m_dummy;
	// followed by [MSG_ITEM_DATA]
};
typedef HEADER_PAYLOAD_STRUCT<MSG_BLOCK_ITEM_TO_CLIENT_HEADER, CHAR_ITEM_DATA> MSG_BLOCK_ITEM_TO_CLIENT;

#if (DRF_OLD_VENDORS == 1)

// MSG_CAT::BLOCK_STORE_ITEM_TO_CLIENT Header
struct MSG_BLOCK_STORE_ITEM_TO_CLIENT_HEADER : MSG_HEADER {
	MSG_BLOCK_STORE_ITEM_TO_CLIENT_HEADER() :
			MSG_HEADER(MSG_CAT::BLOCK_STORE_TO_CLIENT) {}
	int m_menuBind;
	int m_playerCash;
	int m_storeItemCount;
	int m_storeCash;
	int m_storeId;
	// followed by [MSG_ITEM_DATA]
};
typedef HEADER_PAYLOAD_STRUCT<MSG_BLOCK_STORE_ITEM_TO_CLIENT_HEADER, CHAR_ITEM_DATA> MSG_BLOCK_STORE_ITEM_TO_CLIENT;

#endif

struct CONTEXT_VALIDATION {
	char username[MAX_USER_NAME_LEN + 1];
	char password[MD5_DIGEST_LEN * 2 + 1]; // digest as string, plus null
	ULONG protocolVersionInfo;
	ULONG contentVersionInfo;
};

struct CONTEXT_VALIDATION_EX : CONTEXT_VALIDATION {
	ULONG minProtocolVersion; // Now supports a range of versions
};

} // namespace KEP

#pragma pack(pop)
