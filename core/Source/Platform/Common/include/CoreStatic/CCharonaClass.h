///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CStructuresMisl.h"

namespace KEP {

class CMaterialObject;
class CAdvancedVertexObj;

class CCharonaObj : public CObject, public GetSet {
public:
	CCharonaObj();
	DECLARE_SERIAL(CCharonaObj);

	float m_size;
	float m_scaleFactor;
	DWORD m_centerPixelSnapShot;
	BOOL m_validThisRound;
	long m_fadeTime;
	CMaterialObject* m_material;
	CStringA m_textureName;
	DWORD m_textureIndex;
	BOOL m_currentlyVisible;
	long m_changeStamp;
	float m_startDistance;
	float m_endDistance;
	float m_visibleDistance;
	float m_currentDistance;
	CAdvancedVertexObj* m_visual;

	WORD vertexCount;
	ABVERTEX vertexArray[4];
	WORD indecieCount;
	UINT indecieArray[6];

	void SafeDelete();
	void Clone(CCharonaObj** clone);
	BOOL InitializeVis();

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

} // namespace KEP