///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#if (DRF_DIRECT_SHOW_MESH == 1)

#include "DirectShowMesh.h"

#include "ReMaterial.h"
#include "ReD3DX9.h"
#include "ExternalEXMesh.h"


namespace KEP {
class IMemSizeGadget;
class CEXMeshObj;
class CStateManagementObj;
class TextureDatabase;

class DirectShowEXMesh : public DirectShowMesh, public IExternalEXMesh {
public:
	static std::shared_ptr<DirectShowEXMesh> Create(
		CStateManagementObj* stateManager,
		TextureDatabase* textureDatabase,
		DownloadManager* dlMgr,
		LPDIRECT3DDEVICE9 dev,
		const MediaParams& mediaParams,
		uint32_t width = 0, uint32_t height = 0,
		bool loop = false,
		int32_t x = 0, int32_t y = 0);

	CStateManagementObj* StateMgr() {
		return m_stateManager;
	}
	TextureDatabase* TextureDB() {
		return m_textureDatabase;
	}
	void SetTexture(ReMaterialPtr mat) {
		if (mat)
			mat->Update(GetTextureShared(), 0);
	}

	ReMaterialPtr GetReMaterial() {
		return m_ReMat;
	}

	void ScrapeTexture(ReD3DX9DeviceState* devState);

	int Render(LPDIRECT3DDEVICE9 g_pd3dDevice, CEXMeshObj* mesh, int vertexType);

private:
	CStateManagementObj* m_stateManager;
	TextureDatabase* m_textureDatabase;

	bool NewReMaterialPtr(ReD3DX9DeviceState* devState);

	DirectShowEXMesh(DownloadManager* dlMgr, CStateManagementObj* stateManager, TextureDatabase* textureDatabase) :
			DirectShowMesh(dlMgr),
			m_stateManager(stateManager),
			m_textureDatabase(textureDatabase) {
	}

public: // DirectShowMesh::IExternalMesh Interface
	std::string ToStr(bool verbose = false) const {
		return DirectShowMesh::StrThis(verbose);
	}

	bool SetMediaParams(const MediaParams& mediaParams, bool allowRewind = true) {
		return DirectShowMesh::SetMediaParams(mediaParams, allowRewind);
	}
	MediaParams& GetMediaParams() {
		return DirectShowMesh::GetMediaParams();
	}

	bool SetVolume(double volPct = -1) {
		return DirectShowMesh::SetVolume(volPct);
	}
	double GetVolume() const {
		return DirectShowMesh::GetVolume();
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	//void Delete() {
	//	DirectShowMesh::Delete();
	//}
};

} // namespace KEP

#endif