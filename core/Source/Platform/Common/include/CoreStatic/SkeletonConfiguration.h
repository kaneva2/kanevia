///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "CharConfigClass.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CDeformableMeshList;
class CMaterialObject;
class TextureDatabase;
class RuntimeSkeleton;
class IMemSizeGadget;

//special alternate cfg class
class CAlterUnitDBObject : public CObject {
	DECLARE_SERIAL_SCHEMA(CAlterUnitDBObject, VERSIONABLE_SCHEMA | 4);

public:
	CAlterUnitDBObject();
	CStringA m_name;
	CStringA m_loadedfoldername;
	int m_meshCount;
	CStringA* m_meshNames;
	void Serialize(CArchive& ar);
	void SafeDelete();
	CDeformableMeshList* m_configurations; //index 0 is always the default

	void GenerateMeshNames();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

// the number of objects in this list represents the amount of deformable meshes
class CAlterUnitDBObjectList : public CObList {
public:
	CAlterUnitDBObjectList(){};

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CAlterUnitDBObjectList);

	void SafeDelete();
};

class SkeletonConfiguration {
public:
	SkeletonConfiguration() :
			m_meshSlots(NULL) {}

	~SkeletonConfiguration() {
		if (m_meshSlots) {
			m_meshSlots->SafeDelete();
			m_meshSlots = nullptr;
		}
	}

	// clone the SkeletonConfiguration and apply it to
	// the reference skeleton.  It's not a very clean interface,
	// because it used to be part of cloning the entire
	// CSkeletonObject and there wasn't a clear differentiation
	// between EDB and runtime skeleton configuration data.
	void Clone(
		SkeletonConfiguration** clone,
		RuntimeSkeleton* pRefRS,
		int* spawnCfgOptional,
		int* matCfgOptional,
		D3DCOLORVALUE* spawnRGBOptional,
		bool fullClone);

	void CloneBonesMeshes(
		SkeletonConfiguration* cloneReturn,
		RuntimeSkeleton* pRefRS,
		int* spawnCfgOptional,
		int* matCfgOptional,
		D3DCOLORVALUE* spawnRGBOptional);

	void CloneMeshes(
		SkeletonConfiguration* cloneReturn,
		RuntimeSkeleton* pRefRS,
		int* spawnCfgOptional,
		int* matCfgOptional,
		D3DCOLORVALUE* spawnRGBOptional) const;

	void InitialDimConfig(const CharConfig&);

	// Applies any character config changes made since the
	// last call to RealizeDimConfig (swaps in meshes and mats)
	// It applies this configuration to the targetSkeleton using the
	// alternateConfiguration list from a reference skeleton configuration. :-(
	//
	// Arguably, this should be a function on RuntimeSkeleton instead.  It
	// would take a SkeletonConfiguration and CAlterUnitDBObjectList.
	//
	void RealizeDimConfig(RuntimeSkeleton* pRS, CAlterUnitDBObjectList* alternateConfiguration);

	// This function handles the physical dim swapping of mesh and material data.
	// If you think you need to call this, you probably don't.
	// Use HotSwapDim() / HotSwapMat() followed by a call to RealizeDimConfig() instead.
	//
	// I've tried to change this to be more logical and consistent.
	// In some cases, it might be sensible to do referenceSkel->m_skeletonConfiguration->RealizeDimConfig(skeleton),
	// but I think that skeleton->m_skeletonConfiguration should always be set correctly for the
	// corresponding skeleton->m_runtimeSkeleton, so skeleton->m_skeletonConfiguration->RealizeDimConfig(skeleton->m_runtimeSkeleton)
	// is the way to go, after making sure that the skeleton has the correct configuration (possibly based on
	// the reference skeleton's configuration).
	bool HotSwapDim(int deformableIndex, int newCfg, int baseMeshes, bool changeBaseCfg);
	BOOL HotSwapMat(int deformableIndex, int newCfg, int newMat, BOOL changeBaseCfg = FALSE);
	BOOL HotSwapMat(int deformableIndex, int newCfg, const CMaterialObject* material, RuntimeSkeleton* pRS, CAlterUnitDBObjectList* alternateConfigurations);

	void RealizeSlotConfig(RuntimeSkeleton* pRS, int deformableIndex, CAlterUnitDBObjectList* alternateConfigurations);

	void SetEquippableCharConfigBlock(const GLID_OR_BASE& glidOrBase, size_t slot, int meshId, int matlId, int baseMeshes);

	BOOL HotSwapRGB(int deformableIndex, float r, float g, float b, RuntimeSkeleton* targetRuntimeSkeleton);

	CharConfig m_charConfig; // base character config
	CharConfig m_newConfig; // most up-to-date character config
	CharConfig m_lastConfig; // last realized character config

	// spawn initializers
	// This is used by the reference configuration (and is the only thing on it),
	// but it's null for the runtime skeleton's configuration.  The reference
	// 'configuration' should probably be a different class.
	CAlterUnitDBObjectList* m_meshSlots;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	void DimSwap(RuntimeSkeleton* pRS, int deformableIndex, int meshIndex, int matIndex) const;
};

} // namespace KEP
