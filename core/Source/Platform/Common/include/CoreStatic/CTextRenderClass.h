///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "kepoblist.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class IMemSizeGadget;
class CAdvancedVertexObj;
class CMaterialObject;
class CStateManagementObj;

class CSymbolMapperObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CSymbolMapperObj, VERSIONABLE_SCHEMA | 2);

public:
	CSymbolMapperObj();

	float m_percentageX;
	CStringA m_uniqueID; //used to assign char reference
	float m_uvIndexLeft;
	float m_uvIndexRight;
	float m_uvIndexTop;
	float m_uvIndexBottom;
	int m_gridDensity;
	float m_origGridIndexHoriz;
	float m_origGridIndexVert;
	CStringA m_group;
	CStringA m_extendedOption;

	void Assign(
		int gridDensity,
		double horizOffset,
		double vertOffset,
		const CStringA& id,
		double xPercentage);

	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CSymbolMapperObjList : public CKEPObList {
	static CSymbolMapperObjList* pInstance;

public:
	DECLARE_SERIAL(CSymbolMapperObjList);

	static CSymbolMapperObjList* Instance() {
		return pInstance;
	}

	CSymbolMapperObjList() {
		pInstance = this;
	}

	void SafeDelete();

	CSymbolMapperObj* GetByID(const CStringA& id) const;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CTempRenderStringObList : public CObList {
public:
	CTempRenderStringObList(){};

	void SafeDelete();

	void AddCharacter(CSymbolMapperObj* newSymbol);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CTextRenderObj : public CObject {
	std::string m_text; // when this changes we must call Setup()

public:
	CTextRenderObj(
		const std::string& text,
		int mode,
		double startPosX,
		double startPosY,
		double fontSizeX,
		double fontSizeY,
		bool tempObj,
		double red,
		double green,
		double blue);

	bool m_initialized;
	bool m_selfDeleteIt;

	size_t m_constChangeTicks; // drf - added

	int m_blendMode;

	POSITION m_nextPosLoc; // Added by Jonny because nothing works

	double m_startPosX;
	double m_startPosY;
	double m_lastStartPosX;
	double m_lastStartPosY;
	double m_fontSizeX;
	double m_fontSizeY;
	double m_unitLength; // Added by Jonny to save calculating it every single frame for every string.
	double m_measuredDistanceRef;

	CAdvancedVertexObj* m_vertexObject;
	CMaterialObject* m_materialObj;
	CSymbolMapperObj* m_nextSymbol; // Added by Jonny as way to predict next symbol to be called from the map.
	CTempRenderStringObList m_textObsList; // Added by Jonny to prevent searching the global symbol map every frame

	Matrix44f m_worldMatrix;

	void UpdateGeometry(LPDIRECT3DDEVICE9 g_pd3dDevice, const std::string& textStr, bool constantlyChanging = true, CSymbolMapperObjList* pSMOL = NULL);

	void Render(LPDIRECT3DDEVICE9 g_pd3dDevice, CStateManagementObj* stateManager);

	void SafeDelete();

	// DRF - Added - Sets text if changed calls required Setup() to recalculate size.
	void SetText(const std::string& text) {
		if (m_text == text)
			return;
		m_text = text;
		Setup();
	}

	const std::string& GetText() const { return m_text; }

	// DRF - Must be called whenever text changes to recalculate size.
	void Setup(CSymbolMapperObjList* pSMOL = NULL);

	CSymbolMapperObj* GetSymbol(const CStringA& id);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CTextRenderObjList : public CObList {
public:
	CTextRenderObjList(){};

	void SafeDelete();

	void DeleteFinishedTextDisplays();

	void AddTextObject(
		const std::string& text,
		int mode,
		double startPosX,
		double startPosY,
		double fontSizeX,
		double fontSizeY,
		bool tempObj);

	void Flush();
};

} // namespace KEP