/******************************************************************************
 CamerasViewportsDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_CAMERASVIEWPORTS_DLG_H_)
#define _CAMERASVIEWPORTS_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CamerasViewportsDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "ViewportsPropertiesCtrl.h"
#include "RuntimeViewportPropertiesCtrl.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"
#include <map>

////////////////////////////////////////////
// CCamerasViewportsDialog control

class CCamerasViewportsDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CCamerasViewportsDialog)

public:
	CCamerasViewportsDialog();
	virtual ~CCamerasViewportsDialog();

	// Dialog Data
	//{{AFX_DATA(CCamerasViewportsDialog)
	enum { IDD = IDD_DIALOG_CAMERAVIEWPORTS };
	//}}AFX_DATA

	// accessors
	CStringA GetViewportName() const {
		return m_propViewports.GetName();
	}
	int GetPercentTop() const {
		return m_propViewports.GetPercentTop();
	}
	int GetPercentBottom() const {
		return m_propViewports.GetPercentBottom();
	}
	int GetPercentLeft() const {
		return m_propViewports.GetPercentLeft();
	}
	int GetPercentRight() const {
		return m_propViewports.GetPercentRight();
	}
	int GetRuntimeViewportRuntimeCameraID() const {
		return m_propRuntimeViewport.GetRuntimeCameraID();
	}

	// modifiers
	void SetViewportName(CStringA name) {
		m_propViewports.SetName(name);
	}
	void SetPercentTop(int percentTop) {
		m_propViewports.SetPercentTop(percentTop);
	}
	void SetPercentBottom(int percentBottom) {
		m_propViewports.SetPercentBottom(percentBottom);
	}
	void SetPercentLeft(int percentLeft) {
		m_propViewports.SetPercentLeft(percentLeft);
	}
	void SetPercentRight(int percentRight) {
		m_propViewports.SetPercentRight(percentRight);
	}
	void SetRuntimeViewportRuntimeCameraID(int runtime_camera_id) {
		m_propRuntimeViewport.SetRuntimeCameraID(runtime_camera_id);
	}

	// methods
	void UpdatePropertyControls();
	void ResetViewportPropertyControl();

	void UpdateViewportPropertiesRuntimeCameras();
	void ResetRuntimeViewportPropertyControl();

	void UpdateCameraBinding(int runtime_viewport_id);
	void SetCameraBinding(int runtime_viewport_id, int runtime_camera_id);
	void RemoveCameraBinding(int runtime_viewport_id);

protected:
	// Generated message map functions
	//{{AFX_MSG(CCamerasViewportsDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	// template cameras
	CCollapsibleGroup m_colCameras;
	CButton m_grpCameras;

	// runtime cameras
	CRuntimeViewportPropertiesCtrl m_propRuntimeViewport;
	CCollapsibleGroup m_colRuntimeCameras;
	CButton m_grpRuntimeCameras;

	// template viewports
	CViewportsPropertiesCtrl m_propViewports;
	CCollapsibleGroup m_colViewports;
	CButton m_grpViewports;

	// template viewports
	CCollapsibleGroup m_colRuntimeViewports;
	CButton m_grpRuntimeViewports;

	CKEPImageButton m_btnAddCamera;
	CKEPImageButton m_btnDeleteCamera;
	CKEPImageButton m_btnCameraProperties;

	CKEPImageButton m_btnAddRuntimeCamera;
	CKEPImageButton m_btnDeleteRuntimeCamera;

	CKEPImageButton m_btnAddViewport;
	CKEPImageButton m_btnDeleteViewport;
	CKEPImageButton m_btnUpdateViewport;

	CKEPImageButton m_btnAddRuntimeViewport;
	CKEPImageButton m_btnDeleteRuntimeViewport;
	CKEPImageButton m_btnUpdateRuntimeViewport;

	typedef std::map<int, int> RuntimeViewportCameraBindingsMap; // key = runtime viewport id, val = runtime camera id
	typedef RuntimeViewportCameraBindingsMap::iterator CameraBindingsIterator;

	// relationship between runtime viewports and cameras bound to them
	RuntimeViewportCameraBindingsMap m_camera_bindings;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_CAMERASVIEWPORTS_DLG_H_)