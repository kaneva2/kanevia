///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_WATERZONEDBDLG_H__71700A3D_4B0C_48D3_93B0_563FE9C064B6__INCLUDED_)
#define AFX_WATERZONEDBDLG_H__71700A3D_4B0C_48D3_93B0_563FE9C064B6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaterZoneDBDLG.h : header file
//
#include "CWaterZoneClass.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "WaterZonePropertiesCtrl.h"
#include "AreaOfEffectPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CWaterZoneDBDLG dialog

class CWaterZoneDBDLG : public CKEPEditDialog {
	// Construction
public:
	CWaterZoneDBDLG(CWaterZoneObjList*& waterZnDB,
		CWnd* pParent = NULL); // standard constructor

	CWaterZoneObjList*& m_waterZnDBREF;
	CWaterZoneObjList** m_waterZnDBREF_memloc;
	LPDIRECT3DDEVICE9 g_pd3dDeviceRef;
	// Dialog Data
	//{{AFX_DATA(CWaterZoneDBDLG)
	enum { IDD = IDD_DIALOG_WATERZONEDB };
	CListBox m_waterZoneDB;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWaterZoneDBDLG)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListWaterZones(int sel);
	// Generated message map functions
	//{{AFX_MSG(CWaterZoneDBDLG)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	afx_msg void OnBUTTONReplace();
	afx_msg void OnBUTTONLoadDB();
	afx_msg void OnBUTTONSaveDB();
	afx_msg void OnSelchangeLISTWaterZoneDB();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceWaterZone(int selection);

public:
	afx_msg void OnBnClickedButtonAddwaterzone();

private:
	CWaterZoneObj* m_selectedWaterZoneObj;

	CCollapsibleGroup m_colWaterZones;
	CWaterZonePropertiesCtrl m_propWaterZone;
	CAreaOfEffectPropertiesCtrl m_propAreaOfEffect;
	CButton m_grpWaterZones;

	CKEPImageButton m_btnAddWaterSector;
	CKEPImageButton m_btnDeleteWaterSector;
	CKEPImageButton m_btnReplaceWaterSector;

	void RedrawAllPropertiesCtrls();
	void LoadWaterZoneProperties();
	CWaterZoneObj* GetSelectedWaterZoneObj();
	CWaterZoneObj* GetWaterZoneObj(int selection);

	//last selected index in the list box
	int m_previousSel;

public:
	afx_msg void OnBnClickedCheckWaterZones();

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WATERZONEDBDLG_H__71700A3D_4B0C_48D3_93B0_563FE9C064B6__INCLUDED_)
