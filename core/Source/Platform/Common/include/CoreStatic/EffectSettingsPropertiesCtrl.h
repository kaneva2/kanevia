/******************************************************************************
 EffectSettingsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_EFFECT_SETTINGSPROPERTIESCTRL_H_)
#define _EFFECT_SETTINGSPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EffectSettingsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CEffectSettingsPropertiesCtrl window

class CEffectSettingsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CEffectSettingsPropertiesCtrl();
	virtual ~CEffectSettingsPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	void Populate();

	// accessors
	int GetForceDirectionY() const {
		return m_forceDirectionX;
	}
	int GetForceDirectionX() const {
		return m_forceDirectionY;
	}

	// modifiers
	void SetForceDirectionX(int forceDirectionX);
	void SetForceDirectionY(int forceDirectionY);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEffectSettingsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CEffectSettingsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;

	int m_forceDirectionX;
	int m_forceDirectionY;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_EFFECT_SETTINGSPROPERTIESCTRL_H_)
