///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>

#include "GetSet.h"

#include "common/include/kepcommon.h"
#include "Core/Util/SimpleSet.h"
#include "Core/Util/monitored.h"
#include "CABFunctionLib.h"
#include "CStructuresMisl.h"
#include "matrixarb.h"
#include "ListItemMap.h"
#include "ChatType.h"

#include "Event\ClientEvents.h"
#include "Event\ServerEvents.h"

#include "LogonInfo.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class IEvent; // for send event
class Dispatcher;
class IGameState;
class IGameStateDispatcher;
class INetworkLogger;
class ISender;
class ServerItemObjMap;
struct ScriptPlayerInfo;
class CAccountObject;
class CAccountObjectList;
class CBountyObjList;
class CClanSystemObjList;
class CHousingZoneDB;
class CPacketObjList;
class CPlayerObjectList;
class CQuestJournalObj;
class CSerializedPacketObjList;
class CSpawnCfgObj;
class CSpawnCfgObjList;
class CWorldAvailableObjList;
class SpawnInfo;

//#define MC_TIMEOUT_DEFAULT 0

const TimeMs msgUpdateMinTimeMs = 50.0; // min time between update messages (ms)
const TimeMs msgUpdateMaxTimeMs = 50.0; // max time between update messages (ms)

const TimeMs msgMoveMinTimeMs = 200.0; // min time between move messages (ms)
const TimeMs msgMoveMaxTimeMs = 200.0; // max time between move messages (ms)

#define LOG_OBLIST_COUNTS(l, x, y) \
	if (x)                         \
		LOG4CPLUS_INFO(l, "Count of " #x " " << x->GetCount() << " dt " #y " " << numToString((ULONG)x));
#define LOG_VECTOR_COUNTS(l, x, y) \
	LOG4CPLUS_INFO(l, "Count of " #x " " << x.size() << " dt " #y " " << numToString((ULONG)&x));

class CMultiplayerObj : public IServerNetworkCallback,
						public IClientNetworkCallback,
						public Monitored,
						public CObject,
						public GetSet {
	DECLARE_SERIAL_SCHEMA(CMultiplayerObj, VERSIONABLE_SCHEMA | 3);

	static CMultiplayerObj* m_pInstance;

public:
	static CMultiplayerObj* GetInstance() {
		return m_pInstance;
	}

	CMultiplayerObj(const char* name = "ServerEngine", ISender* sender = 0);

	~CMultiplayerObj();

	Monitored::DataPtr monitor() const;

	void SetPathBase(const std::string& pathBase) {
		m_pathBase = pathBase;
	}

	void DisconnectClient(NETID netId, eDisconnectReason reason);

	LOGON_RET VerifyAccount(const char* userName);

	virtual bool DataFromClient(NETID from, const BYTE* data, ULONG len) override;

	virtual NETRET ClientConnected(NETID id, const char* notUsedUserName) override;

	virtual NETRET ClientDisconnected(NETID id, eDisconnectReason reason) override;

	virtual LOGON_RET ValidateLogon(NETID id, CONTEXT_VALIDATION* context, const char* ipAddress, const char* userInfo) override;

	virtual bool DataFromServer(NETID from, const BYTE* data, ULONG len) override;

	virtual NETRET Connected(LOGON_RET logonRet, NETRET hr, ULONG sessionProtocolVersion) override;

	virtual NETRET Disconnected(const std::string& reason, HRESULT hr, BOOL report, LOGON_RET logonRet, bool connecting) override;

	CMultiplayerObj& setIPAddress(const std::string& ipAddress);
	const std::string& getIPAddress() const;

	bool isClientEnabled() const {
		return m_isClientEnabled;
	}

	void SafeDelete();

	void Serialize(CArchive& ar);

	BOOL EnableServer(const char* key, eEngineType engineType, const char* instanceName);

	bool EnableClient();

	/** DRF
	* Calls SendServerClientExitEvent to log client off server and tear down network stack.
	* NOTE - Call this before client exit otherwise the client remains logged in on the server.
	*/
	bool DisableClient(bool reconnectingToOtherServer);

	CStringA WINAPI GetIPAddress(NETID id);

	/** DRF
	* Sends event to server to tell it the client is exiting cleanly.
	* Called by DisableClient() to fully log off server before tearing down network stack.
	*/
	bool SendServerClientExitEvent(bool reconnectingToOtherServer);

	void FillInLogonContext(CONTEXT_VALIDATION_EX& cntxValidation, bool legacy, bool hashPassword);

	// Message Network Tx/Rx
	bool MsgTx(const BYTE* pData, DWORD dataSize, NETID netIdTo = SERVER_NETID /*, DWORD timeout = MC_TIMEOUT_DEFAULT*/);
	bool MsgRx(BYTE*& pData, size_t& dataSize, NETID& netIdFrom);

	// Message Senders (to server)
	bool SendMsgLogToServer(const std::string& userName, const std::string& password);
	bool QueueMsgAttribToServer(const ATTRIB_DATA& attribData);
	bool SendMsgSpawnToServer(short dbIndex, const ZoneIndex& zoneIndex, const CStringA& worldFile, int traceNumber, short useNewPresets, short characterInUse, short aiCfgIndex);

	// Message Senders (to client)
	bool SendMsgLogToClient(NETID netIdTo, TimeMs timeLogToServer, TimeMs timeProcessing);
	bool QueueMsgAttribToClient(NETID netIdTo, const ATTRIB_DATA& attribData, CPlayerObject* pPO = nullptr);
	bool SendMsgMoveToClient(NETID netId, CPlayerObject* pPO, bool sendForce);
	bool SetMsgSpawnToClientOnAccountObject(CAccountObject* pAO, short dbIndex, const ZoneIndex& zoneIndex, const std::string& worldFile, const std::string& url, const Vector3f& spawnPt, int rotation, bool initialSpawn);
	bool SendMsgSpawnToClient(CAccountObject* pAO, CPlayerObject* pPO, NETID netIdTo);

	void SendPendingSpawnEventToClient(const SpawnInfo& spawnInfo, NETID netIdTo, const ZoneIndex& zoneIndex, bool initialSpawn);
	void SendPlayerArrivedEvent(NETID netID, ZoneIndex zoneIndex, NETID prevNetID, const ZoneIndex& prevZoneIndex, unsigned playerArriveId, const ScriptPlayerInfo& playerInfo, unsigned parentZoneInstanceId, CAccountObject* pAccount);
	void SendPlayerDepartedEvent(NETID netID, const ZoneIndex& departZoneIndex, unsigned playerArriveId);

	// Message Senders (to client or server)
	bool QueueOrSendMsgUpdate_EventsOnly(IEvent* e);

	BOOL QueueOrSendRenderTextEvent(NETID netIdTo, const char* message, eChatType chatType);

	bool SendTextMessageToGroup(CPlayerObject* pPO, const char* message); // server only
	bool SendTextMessageToClan(CPlayerObject* pPO, const char* message); // server only

	bool SendEventToGroup(CPlayerObject* pPO, IEvent* e); // server only
	bool SendEventToClan(CPlayerObject* pPO, IEvent* rte); // server only

	void BroadcastMessage(
		short radius, // 0=to all
		const char* message,
		const Vector3f& position,
		const ChannelId& channel,
		NETID fromNetId,
		eChatType chatType = eChatType::System,
		bool allInstances = false);

	// Broadcast the message to a specific distribution list
	void BroadcastMessageToDistribution(
		const char* message,
		NETID fromNetId,
		const char* toDistribution,
		const char* fromRecipient,
		std::vector<PLAYER_HANDLE>& serverDictionary,
		eChatType chatType = eChatType::System,
		bool fromRemote = false,
		ULONG filter = 0);

	bool GetNextPrivateChatDistributionPlayer(
		std::string& dist,
		PLAYER_HANDLE* recipientId,
		std::string& recipientType,
		std::string& distributionName);

	bool AddToPrivateChatDistribution(PLAYER_HANDLE player, IEvent* rte, const char* message);

	void ExpandChatGroups(std::string distIn, std::string& distOut);

	void BroadcastEvent(
		short radius, // 0=to all
		IEvent* rte,
		const Vector3f& position,
		const ChannelId& channel,
		NETID fromNetId,
		eChatType chatType,
		bool allInstances = false,
		bool restrictIfBlocked = false);

	void ServerTimeoutCallback();

	bool LogoutCleanup(
		CAccountObject* account,
		ULONG reason,
		CPlayerObject* pPO = nullptr,
		POSITION* runtimeListPlayerPos = 0,
		LONG* minutesConnected = 0,
		IEvent* replyEvent = 0);

	BOOL UpdateAccount(CPlayerObject* pPO, CAccountObject* pAO);

	BOOL UpdateAllAccounts();

	int GetNextFreeNetTraceId();

	ULONG GetVersion();
	ULONG GetContentVersion(const char* dir = NULL);
	std::string GetVersionInfoFullPath(const char* dir);
	void SetContentVersion(ULONG ver);
	bool GetIPDataFromDisk();
	BOOL CleanClanDatabase();
	void WriteDebugFile(CStringA filename, CStringA debugString);

	void BanIPByUsername(const std::string& username);
	void UnBanIPByUsername(const std::string& username);

	void SerializeToXML(const std::string& fname);
	void SerializeFromXML(const std::string& fname);

	//return spawn point cfg for given world index and spawn cfg index
	CSpawnCfgObj* GetSpawnPointCfg(int worldIndex, int spawnIndex);

	void GetConnectionCode(LOGON_RET logonRet) {
		m_connectionErrorCode = LR_GetConnectionCode(logonRet);
	}

	void PacketQueueAllocServer();

	void PacketQueueAllocClient();

	void PacketQueueFree();

	IGetSet* Values() {
		return this;
	}

	void SetDispatcher(Dispatcher* d) {
		m_dispatcher = d;
	}

	void SetGameStateDispatcher(IGameStateDispatcher* d) {
		m_gameStateDispatcher = d;
	}

	ULONG GetClientProtocol() const {
		return m_clientProtocolVersion;
	}

	CPlayerObject* findPlayerByName(const char* name);

	long GetRequestProcessingErrorTimeMS() const {
		return m_requestProcessingErrorTimeMS;
	}

	long GetRequestProcessingInfoTimeMS() const {
		return m_requestProcessingInfoTimeMS;
	}

	int maxPlayers() {
		return std::min(m_maxPlayers, (int)m_maxAllowedPlayers);
	}

	// ED-5754 - DRF - Messaging Metrics
	struct MsgMetric {
		size_t m_msgRxCount = 0;
		uint64_t m_msgRxBytes = 0;
		size_t m_msgTxCount = 0;
		uint64_t m_msgTxBytes = 0;
	};

	// Returns message metrics as JSON encoded string
	std::string GetMsgMetrics(const std::string& runtimeId = "");

	///////////////////////////////////////////////////////////////////////////
	// SERVER & CLIENT
	///////////////////////////////////////////////////////////////////////////

	static bool s_msgGzipLog; // enable gzip message logging
	static size_t s_msgGzipSize; // minimum bytes to enable message gzip compression

	std::vector<MsgMetric> m_msgMetrics;

	CRITICAL_SECTION m_criticalSection;

	ULONG m_protocolVersion;
	ULONG m_contentVersion;

	long m_clientCharacterMaxCount;

	CPacketObjList* m_pMsgOutboxAttrib;
	CPacketObjList* m_pMsgOutboxEquip;
	CPacketObjList* m_pMsgOutboxEvent;

	CSerializedPacketObjList* m_packetQue;

	///////////////////////////////////////////////////////////////////////////
	// SERVER ONLY
	///////////////////////////////////////////////////////////////////////////

	IServerNetwork* m_serverNetwork;

	SimpleSet<std::string> m_ipBlockList;

	std::string m_IPaddress;

	DWORD m_port;

	DWORD m_pingTimeoutSec;

	CAccountObjectList* m_accountDatabase;

	CAccountObjectList* m_runtimeAccountDB;

	LogonInfoVect m_pendingLogins;

	CRITICAL_SECTION m_loginInfoCS;

	CPlayerObjectList* m_runtimePlayersOnServer;

	CAccountObjectList* m_connectedAIservers;

	CWorldAvailableObjList* m_currentWorldsUsed;

	CSpawnCfgObjList* m_spawnPointCfgs;

	ServerItemObjMap* m_pServerItemObjMap; // SIO::MapKey(zoneIndex otherwise channelId) -> ServerItemObjs[]

	CClanSystemObjList* m_clanDatabase;

	IGameState* m_gameStateDb;

	BOOL m_restrictedMode; // Only administrators are allowed to connect if TRUE

	short m_netTraceIdNext;

	const int m_maxPlayers;
	ULONG m_maxAllowedPlayers;

	bool m_alwaysStartInHousingZone;

	BOOL m_enableAutoBackup;

	TimeMs m_maxPlayerBackupTimeMs;

	CStringA m_disabledEdbs;

	int m_currentPlayersOnServer;

	int m_maxItemLimit_onChar;
	int m_maxItemLimit_inBank;

	long m_initialBankCash;
	long m_initialCash;

	int m_tryOnItemDefaultExpirationDuration;

	///////////////////////////////////////////////////////////////////////////
	// CLIENT ONLY
	///////////////////////////////////////////////////////////////////////////

	std::shared_ptr<IClientNetwork> m_clientNetwork;

	bool m_isClientEnabled;

	ULONG m_clientProtocolVersion;

	std::string m_connectionErrorCode;

	bool m_sendMsgLogToServer;

	int m_clientStatusFromServer;

	BOOL m_serverStarted;

	CStringA m_userName;
	CStringA m_passWord;

	ZoneIndex m_tutorialZoneIndex;
	LONG m_tutorialZoneInstanceId;
	int m_tutorialZoneSpawnIndex;
	std::string m_defaultPlaceUrl;
	ZoneIndex m_initialArenaZoneIndex;

	std::string m_currentlyTradingWith;

	int m_currentCharacterInUse; //-1 none

	long m_cashCount;
	long m_bankCashCount;

	BOOL m_chatEnabled;
	BOOL m_groupChatEnabled;
	BOOL m_clanChatEnabled;

	///////////////////////////////////////////////////////////////////////////
	// DEAD CODE
	///////////////////////////////////////////////////////////////////////////

	CHousingZoneDB* m_housingDB;
	CBountyObjList* m_bountySystem;
	//	BOOL                          m_passwordRequired;
	//	DWORD                         m_maxPacketsPerChar;

private:
	void setIsClientEnabled(bool v) {
		m_isClientEnabled = v;
		m_sendMsgLogToServer = v;
	}

	Dispatcher* m_dispatcher;

	IGameStateDispatcher* m_gameStateDispatcher;

	ULONG m_mainThdId; //< id of main thd for correlating log msgs

	std::string m_pathBase; // dir above gamefiles

	long m_requestProcessingErrorTimeMS;
	long m_requestProcessingInfoTimeMS;

public:
	DECLARE_GETSET
};

class CMultiplayerObjList : public CObList {
public:
	CMultiplayerObjList(){};

	DECLARE_SERIAL(CMultiplayerObjList);
};

class OldNetworkSender : public ISendHandler {
public:
	OldNetworkSender(CMultiplayerObj* mp) :
			m_multiplayerObject(mp) {}

	virtual EVENT_PROC_RC SendEventTo(IEvent* pEvent) override {
		// Send Immediately Or Queue For Next MSG_UPDATE ?
		bool sendNow = ((pEvent->Flags() & EF_SENDIMMEDIATE) != 0);
		if (sendNow) {
			std::vector<unsigned char> eventBuffer;
			pEvent->SerializeToBuffer(eventBuffer);
			assert(!eventBuffer.empty());
			for (const auto& netId : pEvent->To())
				m_multiplayerObject->MsgTx(&eventBuffer.front(), eventBuffer.size(), netId);
		} else {
			m_multiplayerObject->QueueOrSendMsgUpdate_EventsOnly(pEvent);
		}

		return EVENT_PROC_RC::OK;
	}
	CMultiplayerObj* m_multiplayerObject;
};

} // namespace KEP
