/******************************************************************************
 KEPEditDialog.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "KEPDialog.h"
#include "EditorState.h"

namespace KEP {

class IKEPDialogProcessor;

// CKEPEditDialog dialog
// class for a dialog to work in the KEP Editor's dialog bar
class CKEPEditDialog : public CKEPDialog {
	DECLARE_DYNAMIC(CKEPEditDialog)

public:
	CKEPEditDialog(UINT id, CWnd* pParent = NULL); // standard constructor
	virtual ~CKEPEditDialog();

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual void OnOK();
	virtual void OnCancel();

	IKEPDialogProcessor* m_processor;
	UINT m_idd; // our dialog id for use in OnOK
public:
	virtual BOOL OnInitDialog();
	void RegisterProcessor(IKEPDialogProcessor* p) {
		m_processor = p;
	}
};

//static EditorState *editorStatePtr = EditorState::GetInstance();

} // namespace KEP