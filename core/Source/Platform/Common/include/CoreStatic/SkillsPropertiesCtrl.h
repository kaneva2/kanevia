/******************************************************************************
 SkillsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_SKILLSPROPERTIESCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
#define AFX_SKILLSPROPERTIESCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SkillsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CSkillsPropertiesCtrl window

class CSkillsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSkillsPropertiesCtrl();
	virtual ~CSkillsPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetSkillName() const {
		return Utf16ToUtf8(m_sSkillName).c_str();
	}
	float GetExperienceStart() const {
		return m_experienceStart;
	}
	float GetGainIncrement() const {
		return m_gainIncrement;
	}
	int GetStartLevel() const {
		return m_startLevel;
	}
	float GetFailureGain() const {
		return m_failureGain;
	}
	float GetMaxMultiplier() const {
		return m_maxMultiplier;
	}
	bool GetFilterLevelEval() const {
		return m_bFilterLevelEval;
	}
	bool GetInternalTimer() const {
		return m_bInternalTimer;
	}
	bool GetBasicAbility() const {
		return m_bBasicAbility;
	}
	float GetBasicAbilityMastered() const {
		return m_basicAbilityMastered;
	}
	int GetBasicAbilityFunction() const {
		return m_basicAbilityFunction;
	}

	// modifiers
	void SetSkillName(CStringA sSkillName) {
		m_sSkillName = Utf8ToUtf16(sSkillName).c_str();
	}
	void SetSkillID(int skillID) {
		m_skillID = skillID;
	}
	void SetExperienceStart(float experienceStart) {
		m_experienceStart = experienceStart;
	}
	void SetGainIncrement(float gainIncrement) {
		m_gainIncrement = gainIncrement;
	}
	void SetStartLevel(int startLevel) {
		m_startLevel = startLevel;
	}
	void SetFailureGain(float failureGain) {
		m_failureGain = failureGain;
	}
	void SetMaxMultiplier(float maxMultiplier) {
		m_maxMultiplier = maxMultiplier;
	}
	void SetFilterLevelEval(bool bFilterLevelEval) {
		m_bFilterLevelEval = bFilterLevelEval;
	}
	void SetInternalTimer(bool bInternalTimer) {
		m_bInternalTimer = bInternalTimer;
	}
	void SetBasicAbility(bool bBasicAbility) {
		m_bBasicAbility = bBasicAbility;
	}
	void SetBasicAbilityMastered(float basicAbilityMastered) {
		m_basicAbilityMastered = basicAbilityMastered;
	}
	void SetBasicAbilityFunction(int basicAbilityFunction) {
		m_basicAbilityFunction = basicAbilityFunction;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkillsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSkillsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;
	CFont m_font;

	CStringW m_sSkillName;
	int m_skillID;
	float m_experienceStart;
	float m_gainIncrement;
	int m_startLevel;
	float m_failureGain;
	float m_maxMultiplier;
	bool m_bFilterLevelEval;
	bool m_bInternalTimer;
	bool m_bBasicAbility;
	float m_basicAbilityMastered;
	int m_basicAbilityFunction;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKILLSPROPERTIESCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
