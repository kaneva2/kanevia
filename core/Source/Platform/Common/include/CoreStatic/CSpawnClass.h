///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "KEPObList.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CSpawnObj : public CObject, public GetSet {
	/*
	Version history
	2 	5/6/05 added manual start
*/
	DECLARE_SERIAL_SCHEMA(CSpawnObj, VERSIONABLE_SCHEMA | 2);

public:
	CSpawnObj();
	CSpawnObj(const CSpawnObj& src) {
		operator=(src);
	}
	CSpawnObj& operator=(const CSpawnObj& src);

	CStringA m_spawnName;
	Matrix44f m_spawnMatrix;
	int m_maxOutPutCapacity;
	int m_currentOutPut;
	int m_possibilityOfSpawn; //% out of 1000
	int m_spawnIndex;
	float m_spawnRadius;
	int m_spawnType;
	int m_aiCfg; //if used

	//functions//

	//tree use
	int m_treeUse; //-1 = none
	int m_treePosition; //-1 = random
	BOOL m_manualStart;

	// added to track channel for this spawn
	long m_channel;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CSpawnObjList : public CKEPObList {
public:
	CSpawnObjList();
	void SafeDelete();

	DECLARE_SERIAL(CSpawnObjList);

	long m_recentTotalOutput;

	long GetTotalAIOutput();
	void UpdateChannel(LONG channel);
};

} // namespace KEP