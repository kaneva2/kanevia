///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SAFEZONEDLG_H__F5715081_591F_11D5_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_SAFEZONEDLG_H__F5715081_591F_11D5_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SafeZoneDlg.h : header file
//
#include "CSafeZones.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "SafeZonePropertiesCtrl.h"
#include "AreaOfEffectPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CSafeZoneDlg dialog

class CSafeZoneDlg : public CKEPEditDialog {
	// Construction
public:
	CSafeZoneDlg(CSafeZoneObjList*& safeZoneDB,
		CWnd* pParent = NULL); // standard constructor

	CSafeZoneObjList*& m_safeZoneDBREF;
	// Dialog Data
	//{{AFX_DATA(CSafeZoneDlg)
	enum { IDD = IDD_DIALOG_SAFEZONEDLG };
	CListBox m_safeZoneDB;

	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSafeZoneDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	BOOL InListSafeZones(int sel);
	// Generated message map functions
	//{{AFX_MSG(CSafeZoneDlg)
	afx_msg void OnBUTTONAddZone();
	afx_msg void OnBUTTONDeleteZone();
	afx_msg void OnButtonLoad();
	afx_msg void OnBUTTONReplaceZone();
	afx_msg void OnButtonSave();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeLISTSafeZoneDB();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceSafeZone(int selection);

private:
	CSafeZoneObj* m_selectedSafeZoneObj;

	CCollapsibleGroup m_colSafeZones;
	CSafeZonePropertiesCtrl m_propSafeZone;
	CAreaOfEffectPropertiesCtrl m_propAreaOfEffect;
	CButton m_grpSafeZones;

	CKEPImageButton m_btnAddSafeSector;
	CKEPImageButton m_btnDeleteSafeSector;
	CKEPImageButton m_btnReplaceSafeSector;

	void RedrawAllPropertiesCtrls();
	void LoadSafeZoneProperties();
	CSafeZoneObj* GetSelectedSafeZoneObj();
	CSafeZoneObj* GetSafeZoneObj(int selection);

	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAFEZONEDLG_H__F5715081_591F_11D5_B1EE_0000B4BD56DD__INCLUDED_)
