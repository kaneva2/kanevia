///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENTITYPROPERTIESDLG_H__F89644E1_F094_11D3_B1E8_0000B4BD56DD__INCLUDED_)
#define AFX_ENTITYPROPERTIESDLG_H__F89644E1_F094_11D3_B1E8_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EntityPropertiesDlg.h : header file
//

#include "EntityPropertiesCtrl.h"
#include "KEPImageButton.h"

/////////////////////////////////////////////////////////////////////////////
// CEntityPropertiesDlg dialog

class CEntityPropertiesDlg : public CDialog {
	// Construction
public:
	CEntityPropertiesDlg(int entityID, CWnd* pParent = NULL); // standard constructor
	// Dialog Data
	//{{AFX_DATA(CEntityPropertiesDlg)
	enum { IDD = IDD_DIALOG_ENTITYPROPERTIESEXT };
	//}}AFX_DATA

	// accessors
	int GetEntityIndex() const {
		return m_propEntity.GetEntityIndex();
	}
	int GetEnergy() const {
		return m_propEntity.GetEnergy();
	}
	float GetPopoutRadius() const {
		return m_propEntity.GetPopoutRadius();
	}
	float GetMeshRadius() const {
		return m_propEntity.GetMeshRadius();
	}
	int GetModelType() const {
		return m_propEntity.GetModelType();
	}
	int GetDeathCamIndex() const {
		return m_death_camera_index;
	}
	int GetHudIndex() const {
		return m_propEntity.GetHudIndex();
	}
	int GetMountBoneIndex() const {
		return m_propEntity.GetMountBoneIndex();
	}
	int GetTeam() const {
		return m_propEntity.GetTeam();
	}
	CStringA GetEntityName() const {
		return m_propEntity.GetEntityName();
	}
	int GetRaceID() const {
		return m_propEntity.GetRaceID();
	}
	int GetSvrPhysics() const {
		return m_propEntity.GetSvrPhysics();
	}
	float GetHardPointOverd() const {
		return m_propEntity.GetHardPointOverd();
	}
	bool GetDisableCollision() const {
		return m_propEntity.GetDisableCollision();
	}

	// modifiers
	void SetEntityIndex(int index) {
		m_propEntity.SetEntityIndex(index);
	}
	void SetEnergy(int energy) {
		m_propEntity.SetEnergy(energy);
	}
	void SetPopoutRadius(float popoutRadius) {
		m_propEntity.SetPopoutRadius(popoutRadius);
	}
	void SetMeshRadius(float meshRadius) {
		m_propEntity.SetMeshRadius(meshRadius);
	}
	void SetModelType(int modelType) {
		m_propEntity.SetModelType(modelType);
	}
	void SetDeathCamIndex(int deathCamIndex) {
		m_death_camera_index = deathCamIndex;
	}
	void SetHudIndex(int hudIndex) {
		m_propEntity.SetHudIndex(hudIndex);
	}
	void SetMountBoneIndex(int mountBoneIndex) {
		m_propEntity.SetMountBoneIndex(mountBoneIndex);
	}
	void SetTeam(int team) {
		m_propEntity.SetTeam(team);
	}
	void SetEntityName(CStringA sEntityName) {
		m_propEntity.SetEntityName(sEntityName);
	}
	void SetRaceID(int raceID) {
		m_propEntity.SetRaceID(raceID);
	}
	void SetSvrPhysics(int svrPhysics) {
		m_propEntity.SetSvrPhysics(svrPhysics);
	}
	void SetHardPointOverd(float hardPointOverd) {
		m_propEntity.SetHardPointOverd(hardPointOverd);
	}
	void SetDisableCollision(bool bDisable) {
		m_propEntity.SetDisableCollision(bDisable);
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEntityPropertiesDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CEntityPropertiesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddCamera();
	afx_msg void OnDeleteCamera();
	afx_msg void OnAddActorGroup();
	afx_msg void OnDelActorGroup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitCameras();
	void InitGroups();
	void SaveCameras();

public:
	afx_msg void OnBnClickedOk();

private:
	CEntityPropertiesCtrl m_propEntity;

	CKEPImageButton m_btnAddCamera;
	CKEPImageButton m_btnDeleteCamera;
	CListBox m_cameras;
	CComboBox m_death_camera;
	CButton m_buttAddGroup;
	CButton m_buttDelGroup;
	CListBox m_actorMembership;

	int m_death_camera_index;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENTITYPROPERTIESDLG_H__F89644E1_F094_11D3_B1E8_0000B4BD56DD__INCLUDED_)
