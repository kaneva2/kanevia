///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "cabfunctionlib.h"
#include "matrixarb.h"
#include "KEPObject.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CEXMeshObjList;
class CLightningObjList;
class CSoundBankList;
class CStateManagementObj;
class CTimeSystemObj;

class CEnvironmentObj : public CKEPObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CEnvironmentObj, VERSIONABLE_SCHEMA | 3)

public:
	CEnvironmentObj();

	BOOL InitAll(
		LPDIRECT3DDEVICE9 device,
		D3DCOLOR* backgroundColorGlobal,
		CStateManagementObj* stateManager);

	BOOL SetWorldCurrentTime(
		long currentSystemDay,
		long currentSystemYear,
		long currentSystemMonth,
		long currentDayTime);

	BOOL CallBack(
		LPDIRECT3DDEVICE9 device,
		D3DCOLOR* backgroundColorGlobal,
		CLightningObjList* lightningEffectSystem,
		Matrix44f soundCenter,
		CSoundBankList* soundManager,
		BOOL& renderEnviorment,
		CStateManagementObj* stateManager,
		CEXMeshObjList* charonaList);

	void SafeDelete();

	void DeleteEnvironmentTimeSystem();

	void Serialize(CArchive& ar);

public:
	CTimeSystemObj* m_environmentTimeSystem;

	BOOL m_enableTimeSystem;

	bool m_fogCanCull; // drf - added

	BOOL m_fogOn;
	float m_fogRange;
	float m_fogStart;
	float m_fogRed;
	float m_fogGreen;
	float m_fogBlue;

	float m_backRed;
	float m_backGreen;
	float m_backBlue;

	BOOL m_sunLightOn;
	float m_sunRed;
	float m_sunGreen;
	float m_sunBlue;
	Vector3f m_sunPosition;

	BOOL m_ambientLightOn;
	float m_ambientLightRedDay;
	float m_ambientLightGreenDay;
	float m_ambientLightBlueDay;

	BOOL m_specularLightOn;
	float m_specularLightRed;
	float m_specularLightGreen;
	float m_specularLightBlue;

	float m_gravity;

	BOOL m_atmosphereDensity;

	float m_seaLevel;
	float m_seaViscusDrag;
	bool m_lockBackColor;

	DECLARE_GETSET
};

} // namespace KEP