///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/Matrix.h"

namespace KEP {

class IMemSizeGadget;
class CTextRenderObj;

// This class represents the data necessary to render overhead labels for a CSkeletonObject
class SkeletonLabels {
	struct LabelStruct {
		CTextRenderObj* pTRO;
		std::string displayText;
		double fontSize;
		Vector3d color;

		LabelStruct(const std::string& textStr, double _fontSize, const Vector3d& _color) :
				pTRO(NULL), displayText(textStr), fontSize(_fontSize), color(_color) {
		}
		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	};

public:
	enum LabelPositions {
		SL_POS_NAME,
		SL_POS_TITLE,
		SL_POS_CLAN,
		SL_POS_END
	};

	SkeletonLabels();
	~SkeletonLabels();

	void reset();

	void render(const Vector3f& displayPos);

	void addLabel(const std::string& textStr, double fontSize, const Vector3d& color, unsigned int position = SL_POS_END);

	void addLabels(const SkeletonLabels& skeletonLabels);

	void setCameraMatrix(const Matrix44f& cameraMatrix);

	bool isEmpty() const {
		return m_labels.empty();
	}

	bool removeByText(const std::string& textStr);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	std::vector<LabelStruct> m_labels;

	// DRF - Making secondary labels slightly smaller
	double FontSize(size_t i) const {
		double fontSize = (i < m_labels.size()) ? m_labels[i].fontSize : 0.0;
		return i ? (0.8 * fontSize) : fontSize;
	}

	Matrix44f m_cameraMatrix;
};

} // namespace KEP
