///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "CArmedInventoryClass.h"
#include "Event/Base/IEvent.h"
#include "ResourceManagers\ResourceManager.h"
#include "IResource.h"
#include "ResourceType.h"


namespace KEP {

class CGlobalInventoryObjList;
class CItemObj;
class EquippableRM;

class CArmedInventoryProxy : public IResource, public IEquippableProxy {
public:
	// Settings for TypedResourceManager
	const static ResourceType _ResType = ResourceType::EQUIPPABLE;

protected:
	CArmedInventoryProxy(ResourceManager* library, UINT64 resKey, bool isLocal); // IResource constructor should only be called by ResourceManager
	bool IsSerializedDataCompressible() const {
		return true; // override
	}

public:
	virtual ~CArmedInventoryProxy();

	virtual std::string ToStr() const override {
		std::string str;
		StrBuild(str, "AIP<" << this << ">");
		return str;
	}

	virtual bool CreateResource() override;

	virtual CArmedInventoryObj* GetData(bool bImmediate = false) override;

	BYTE* GetSerializedData(FileSize& dataLen);

	GLID GetGlid() {
		return (GLID)GetHashKey();
	}

	CArmedInventoryObj* GetInventoryObj() const;

	void ReplaceEquippableObj(std::unique_ptr<CArmedInventoryObj> obj);

private:
	std::unique_ptr<CArmedInventoryObj> m_inventoryObj;

	friend class TypedResourceManager<CArmedInventoryProxy>;
};

class EquippableRM : public TypedResourceManager<CArmedInventoryProxy>, public IEquippableManager {
public:
	static const unsigned int equippable_code = 789;

	EquippableRM(const std::string& id) :
			TypedResourceManager<CArmedInventoryProxy>(id), m_glidMax(1) {}
	~EquippableRM() {}

	static EquippableRM* Instance(); // TODO: remove this

	virtual CArmedInventoryProxy* GetProxy(int glid, bool bCreateIfNotExist) override;

	void LoadFromAPD(CArmedInventoryList* apd);
	bool AddNew(std::unique_ptr<CArmedInventoryObj> obj, int glid);
	CArmedInventoryList* GetLegacyAPD();

	bool WriteEquippableObject(CArmedInventoryObj* prox, int glid, CArchive& ar);
	std::unique_ptr<CArmedInventoryObj> LoadEquippableItemFromLooseArchive(GLID glid, CArchive& ar);
	BYTE* prepareEquippableObjectData(CItemObj* item, bool bEncrypt, FileSize& buffLen, float scaleFactor /*=1.0f*/) const;

	void loadLooseFileInfo();

protected:
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) override;
	virtual std::string GetResourceFileName(UINT64 hash) override;
	virtual std::string GetResourceFilePath(UINT64 hash) override;
	virtual std::string GetResourceURL(UINT64 hash) override;

	virtual bool IsUGCItem(UINT64 hash) const override {
		return IS_UGC_GLID((GLID)hash);
	}

private:
	int m_glidMax;
};

} // namespace KEP
