///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

namespace KEP {

class SceneMultipassOptions {
public:
	enum ShadowType {
		ST_NONE = 0, // default
		ST_STENCILSHADOWVOLUMES = 1, // deprecated
		ST_SPOTSHADOW = 2, // enabled via settings->graphics 'Show Spot Shadows'
		ST_SHADOWMAPS = 3 // deprecated
	};
	ShadowType shadowType;

	SceneMultipassOptions() :
			shadowType(ST_NONE) {}
};

enum RenderListClearFlags {
	RLClear_NONE = 0,
	RLClear_ALL = 0xFFFFFFFF,
};

class ScenePassOptions {
	ScenePassOptions(const ScenePassOptions& rhs);
	void operator=(const ScenePassOptions& rhs);

public:
	bool drawWithSpotShadows;
	ULONG renderTargetClearFlags;
	ULONG renderListsClearFlags;
	bool disableDistanceCulling;
	bool disableObjectRendering;
	bool disableAvatarRendering;
	bool disableEnvironmentRendering;
	bool disableVisualizations;
	bool disableWorldObjectGroupFiltering; // World object group filtering is used by some EXG files to cull an entire group of objects when player is within
		// certain range of a dummy target. It's used to achieve occlusion culling with camera is inside a closed space.
	float minRadiusFilter;

	ScenePassOptions() :
			drawWithSpotShadows(false),
			renderTargetClearFlags(0),
			renderListsClearFlags(RLClear_ALL),
			disableDistanceCulling(false),
			disableObjectRendering(false),
			disableAvatarRendering(false),
			disableEnvironmentRendering(false),
			disableVisualizations(false),
			disableWorldObjectGroupFiltering(false),
			minRadiusFilter(0.0f) {}
};

} // namespace KEP
