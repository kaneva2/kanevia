///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPObList.h"

namespace KEP {

class CGlobalMaterialShader : public CObject, public GetSet {
	DECLARE_SERIAL(CGlobalMaterialShader);

public:
	CGlobalMaterialShader();

	CStringA m_shaderName;
	int m_requiredUvCount;

	int m_chromeUvSet;
	int m_enableChroming;

	DWORD m_textureIndexArray[10]; //-1 = NO Change
	CStringA m_textureStringArray[10]; //pull textures and reassigns indexes
	int m_blendArray[10];

	BOOL m_useOverrideMaterial;
	D3DMATERIAL9 m_overrideMaterial;

	void ScaleForRuntimeCompatibility(BOOL& bumpSupport);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CGlobalMaterialShaderList : public CKEPObList {
public:
	CGlobalMaterialShaderList(){};
	void SafeDelete();

	DECLARE_SERIAL(CGlobalMaterialShaderList);

	void ScaleForRuntimeCompatibility(BOOL& bumpSupport);
	CGlobalMaterialShader* GetByIndex(int index);
};

} // namespace KEP