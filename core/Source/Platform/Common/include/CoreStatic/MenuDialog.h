/******************************************************************************
 MenuDialog.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "resource.h"
#include "KepEditDialog.h"
#include "MenuPropertiesCtrl.h"
#include "MenuControlPropertiesCtrl.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "Common\UIStatic\BasicLayoutMFC.h"

#include <string>

namespace KEP {
class IMenuDialog;
class IMenuControl;
}
// CMenuDialog dialog

class CMenuDialog : public CKEPEditDialog {
	DECLARE_DYNAMIC(CMenuDialog)

public:
	CMenuDialog(CWnd* pParent = NULL); // standard constructor
	virtual ~CMenuDialog();

	// Dialog Data
	enum { IDD = IDD_MENUS };

	// accessors
	string GetBaseDir() const {
		return m_base_dir;
	}

	// modifiers
	void SetBaseDir(string base_dir) {
		m_base_dir = base_dir;
		m_propMenu.SetBaseDir(base_dir);
		m_propMenuControl.SetBaseDir(base_dir);
	}

	void SelectDialog();
	void SelectControl(const char* controlName);
	void UpdateDialogPosition(int x, int y);
	void UpdateDialogSize(int width, int height);
	void UpdateControlPosition(int x, int y);
	void UpdateControlSize(int width, int height);

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	virtual BOOL OnInitDialog();
	virtual void OnDestroy();

	afx_msg void OnSelchangeListMenus();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonSaveAs();
	afx_msg void OnButtonNew();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonTest();

	afx_msg void OnButtonAddStatic();
	afx_msg void OnButtonAddImage();
	afx_msg void OnButtonAddButton();
	afx_msg void OnButtonAddCheckBox();
	afx_msg void OnButtonAddRadioButton();
	afx_msg void OnButtonAddComboBox();
	afx_msg void OnButtonAddSlider();
	afx_msg void OnButtonAddEditBox();
	afx_msg void OnButtonAddListBox();
	afx_msg void OnButtonAddFlash();
	afx_msg void OnButtonAddBrowser();

	afx_msg LRESULT OnPropertyChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnControlPropertyChanged(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	typedef enum {
		Static = 0,
		Image,
		Button,
		CheckBox,
		RadioButton,
		ComboBox,
		Slider,
		EditBox,
		IMEEditBox,
		ListBox,
		Flash,
		Browser
	} ControlType;

	static const int Default_Static_Width = 50;
	static const int Default_Static_Height = 15;

	static const int Default_Image_Width = 100;
	static const int Default_Image_Height = 100;

	static const int Default_Button_Width = 70;
	static const int Default_Button_Height = 25;

	static const int Default_CheckBox_Width = 115;
	static const int Default_CheckBox_Height = 25;

	static const int Default_RadioButton_Width = 115;
	static const int Default_RadioButton_Height = 25;

	static const int Default_ComboBox_Width = 150;
	static const int Default_ComboBox_Height = 24;

	static const int Default_Slider_Width = 100;
	static const int Default_Slider_Height = 24;

	static const int Default_EditBox_Width = 140;
	static const int Default_EditBox_Height = 32;

	static const int Default_IMEEditBox_Width = 140;
	static const int Default_IMEEditBox_Height = 32;

	static const int Default_ListBox_Width = 125;
	static const int Default_ListBox_Height = 75;

	static const int Default_Flash_Width = 500;
	static const int Default_Flash_Height = 400;

	static const int Default_Browser_Width = 500;
	static const int Default_Browser_Height = 400;

	void InitMenus();
	bool SaveMenu(IMenuDialog* pDialog, const char* src_script);
	string GetMenuPath(int nIndex);
	void RefreshMenuPropertyControl();
	void RefreshControlPropertyControl();
	void EnableControls(bool bEnable);
	void EnableControlsForTesting(bool bEnable);
	void ShowIncludedMenus(bool bShow);

	void RequestControlSelection(IMenuControl* pControl);
	void GetNewControlLocation(ControlType type, POINT& pt);

private:
	CListBox m_menus;
	CButton m_btn_save;
	CButton m_btn_save_as;
	CButton m_btn_new;
	CButton m_btn_delete;
	CButton m_btn_test;

	CMenuPropertiesCtrl m_propMenu;
	CMenuControlPropertiesCtrl m_propMenuControl;

	CCollapsibleGroup m_colIncludedMenus;
	CButton m_grpIncludedMenus;
	CKEPImageButton m_btnAddIncludedMenu;
	CKEPImageButton m_btnDeleteIncludedMenu;
	CListBox m_includedMenusList;

	CKEPImageButton m_btn_add_static;
	CKEPImageButton m_btn_add_image;
	CKEPImageButton m_btn_add_button;
	CKEPImageButton m_btn_add_check_box;
	CKEPImageButton m_btn_add_radio_button;
	CKEPImageButton m_btn_add_combo_box;
	CKEPImageButton m_btn_add_slider;
	CKEPImageButton m_btn_add_edit_box;
	CKEPImageButton m_btn_add_list_box;
	CKEPImageButton m_btn_add_flash;
	CKEPImageButton m_btn_add_browser;

	string m_base_dir;

	IMenuDialog* m_cur_dialog;
	bool m_dialog_changed;
	bool m_testing;
	int m_cur_dialog_index;
	string m_cur_dialog_name;

	BasicLayoutMFC m_layout;
};
