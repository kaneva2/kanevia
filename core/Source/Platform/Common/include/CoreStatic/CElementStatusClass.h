///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class IMemSizeGadget;

class CElementStatusObj : public CObject, public GetSet {
	DECLARE_SERIAL(CElementStatusObj);

public:
	CElementStatusObj();

	short m_elementIndex; //corilates to index zero based
	short m_currentValue; //current value

	void SafeDelete();
	void Clone(CElementStatusObj** clone);
	void SerializeAlloc(CArchive& ar);

	void Serialize(CArchive& ar);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CElementStatusList : public CObList {
public:
	CElementStatusList(){};

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CElementStatusList);

	void SafeDelete();
	void Clone(CElementStatusList** clone);
	int GetDefenseForSpecifiedChannel(int damageChannel);
	void SerializeAlloc(CArchive& ar, int loadCount);
};

} // namespace KEP