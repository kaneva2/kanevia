/******************************************************************************
 AITriggeredSpawnPointsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_AI_TRIGGERED_SPAWN_POINTS_PROPERTIESCTRL_H_)
#define _AI_TRIGGERED_SPAWN_POINTS_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AITriggeredSpawnPointsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CAITriggeredSpawnPointsPropertiesCtrl window

class CAITriggeredSpawnPointsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CAITriggeredSpawnPointsPropertiesCtrl();
	virtual ~CAITriggeredSpawnPointsPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetPositionX() const {
		return m_positionX;
	}
	float GetPositionY() const {
		return m_positionY;
	}
	float GetPositionZ() const {
		return m_positionZ;
	}
	int GetChannelID() const {
		return m_channelID;
	}

	// modifiers
	void SetPositionX(float positionX);
	void SetPositionY(float positionY);
	void SetPositionZ(float positionZ);
	void SetChannelID(int channelID) {
		m_channelID = channelID;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAITriggeredSpawnPointsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CAITriggeredSpawnPointsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_POSITION,
		INDEX_X,
		INDEX_Y,
		INDEX_Z,
		INDEX_CHANNEL
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	void InitChannels();

private:
	float m_positionX;
	float m_positionY;
	float m_positionZ;
	int m_channelID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_AI_TRIGGERED_SPAWN_POINTS_PROPERTIESCTRL_H_)
