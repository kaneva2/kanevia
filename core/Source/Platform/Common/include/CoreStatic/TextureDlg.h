/******************************************************************************
 TextureDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_TEXTURE_DLG_H_)
#define _TEXTURE_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextureDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "TextureDatabase.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

////////////////////////////////////////////
// CTextureDialog control

class CTextureDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CTextureDialog)

public:
	CTextureDialog();
	virtual ~CTextureDialog();

	void SaveSurface(CTextureEX* ptrLoc);
	void SaveSurfaceUseHighQuality(CTextureEX* ptrLoc);

	// Dialog Data
	//{{AFX_DATA(CTextureDialog)
	enum { IDD = IDD_DIALOG_TEXTUREDATA };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CTextureDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colTexture;
	CButton m_grpTexture;

	CKEPImageButton m_btnAddTexture;
	CKEPImageButton m_btnDeleteTexture;
	CKEPImageButton m_btnReplaceTexture;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_TEXTURE_DLG_H_)