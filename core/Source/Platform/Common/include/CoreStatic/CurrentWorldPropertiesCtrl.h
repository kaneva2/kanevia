/******************************************************************************
 CurrentWorldPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_CURRENTWORLDPROPERTIESCTRL_H_)
#define _CURRENTWORLDPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CurrentWorldPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CCurrentWorldPropertiesCtrl window

class CCurrentWorldPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CCurrentWorldPropertiesCtrl();
	virtual ~CCurrentWorldPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetDisplayName() const {
		return Utf16ToUtf8(m_sWorldDispName).c_str();
	}
	CStringA GetWorldName() const {
		return Utf16ToUtf8(m_sWorldName).c_str();
	}
	int GetWorldID() const {
		return m_worldID;
	}
	UINT GetMaxOccupancy() const {
		return m_maxOccupancy;
	}
	UINT GetVisibility() const {
		return m_visibility;
	}
	int GetZoneIndex() const {
		return m_zoneIndex;
	}

	// modifiers
	void SetDisplayName(CStringA dispName) {
		m_sWorldDispName = Utf8ToUtf16(dispName).c_str();
	}
	void SetWorldName(CStringA sWorldName) {
		m_sWorldName = Utf8ToUtf16(sWorldName).c_str();
	}
	void SetWorldID(int worldID) {
		m_worldID = worldID;
	}
	void SetMaxOccupancy(UINT i) {
		m_maxOccupancy = i;
	}
	void SetVisibility(UINT i) {
		m_visibility = i;
	}
	void SetZoneIndex(int i) {
		m_zoneIndex = i;
	}
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCurrentWorldPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CCurrentWorldPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_ZONE_NAME,
		INDEX_CHANNEL_ID,
		INDEX_ZONE_DISPNAME,
		INDEX_MAX_OCCUPANCY,
		INDEX_VISIBILITY,
		INDEX_LOAD_WORLD,
		INDEX_ZONE_INDEX
	};

	const static int CHANNEL_ID_MIN = 0;
	const static int CHANNEL_ID_MAX = 1000;

	const static int MAX_OCCUPANCY_MIN = 1;
	const static int MAX_OCCUPANCY_MAX = 5000;

	const static int VIS_VALUE_NONE = 0;
	const static int VIS_VALUE_NO_MENU = 50;
	const static int VIS_VALUE_FULL = 100;

	void LoadWorld();
	void InitVisibility();

private:
	CStringW m_sWorldName;
	CStringW m_sWorldDispName;
	int m_worldID;
	UINT m_maxOccupancy;
	UINT m_visibility;
	int m_zoneIndex;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_CURRENTWORLDPROPERTIESCTRL_H_)
