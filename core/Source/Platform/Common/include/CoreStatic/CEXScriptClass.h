///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "IXmlSerializableObj.h"
#include "Engine\Base\ScriptAttributes.h"
#include "..\KEPUtil\algorithms.h"

namespace KEP {

//scripting class
//main use - for auto loading initial Databases
//Definitions Attributes
//0 = play movie fullscreen
//1 = open scene
//2 = load missile database
//3 = load explosion database
//4 = load movie database
//5 = load controls database
//6 = load default cameras
//7 = load anim textures database
//8 = load mouse database
//9 = load 2D collections database
//10 = load text/font collections database
//11 = load bullethole Database

class CEXScriptObj : public CObject {
protected:
	CEXScriptObj() {}

	DECLARE_SERIAL(CEXScriptObj)

public:
	CEXScriptObj(ACTION_ATTR L_actionAttribute,
		CStringA L_miscString,
		int L_miscInt);

	ACTION_ATTR m_actionAttribute;
	CStringA m_miscString;
	int m_miscInt;

	void Serialize(CArchive& ar);

#ifdef NOT_USED
	DECLARE_GETSET
#endif
};

class CEXScriptObjList : public CObList, public IXmlSerializableObj {
public:
	CEXScriptObjList() {
		m_lastSavedPath = "";
		m_dirty = FALSE;
	};

	void SerializeToXML(const FileName& fileName);
	void SerializeFromXML(const FileName& fileName);

	DECLARE_SERIAL(CEXScriptObjList)

	//implementation of IXmlSerializableObj
public:
	FileName GetLastSavedPath() const {
		return m_lastSavedPath;
	}
	BOOL IsDirty() const {
		return m_dirty;
	}
	void SetDirty(BOOL dirty = TRUE) {
		m_dirty = dirty;
	}

private:
	FileName m_lastSavedPath;
	BOOL m_dirty;
};

} // namespace KEP