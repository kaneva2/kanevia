///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// OpenAL Includes
#include <al.h>
#include <alc.h>

namespace KEP {

class OALSoundBuffer {
public:
	bool SourceOk(ALuint source) const {
		return (source && alIsSource(source));
	}

	virtual bool Playing(ALuint source) const {
		if (!SourceOk(source))
			return false;
		ALenum state;
		alGetSourcei(source, AL_SOURCE_STATE, &state);
		return (state == AL_PLAYING);
	}

	virtual bool Stop(ALuint source, bool force = false) {
		if (!force && !Playing(source))
			return true;
		if (!SourceOk(source))
			return false;
		alSourceStop(source);
		return true;
	}

	virtual bool Rewind(ALuint source) {
		if (!SourceOk(source))
			return false;
		alSourceRewind(source);
		return true;
	}

	virtual bool Play(ALuint source) = 0;

	// Only Needed For Streaming Buffers
	virtual bool Update(ALuint buffer) = 0;

	// Only Needed For Streaming Buffers
	virtual bool EmptyQueue(ALuint source) = 0;
};

} // namespace KEP
