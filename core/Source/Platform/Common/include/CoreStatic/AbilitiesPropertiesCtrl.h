/******************************************************************************
 AbilitiesPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ABILITIESPROPERTIESCTRL_H_)
#define _ABILITIESPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AbilitiesPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CAbilitiesPropertiesCtrl window

class CAbilitiesPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CAbilitiesPropertiesCtrl();
	virtual ~CAbilitiesPropertiesCtrl();
	enum {
		ABILITY_NONE = 0,
		ABILITY_RESERVED,
		ABILITY_MORPH,
		ABILITY_HIDE,
		ABILITY_MOUNT,
		ABILITY_REVEAL,
		ABILITY_RADIUSDMG,
		ABILITY_EMITPROJECTILE,
		ABILITY_HEALSELF
	};

	enum { INDEX_ROOT = 1,
		INDEX_ABILITY_NAME,
		INDEX_MINIMUM_LEVEL,
		INDEX_MASTERED_EXP,
		INDEX_MISC_INT,
		INDEX_MISC_INT2,
		INDEX_MISC_INT3,
		INDEX_EFFECT_BIND,
		INDEX_MISC_AMOUNT,
		INDEX_ABILITY_FUNCTION
	};

	static const int MIN_LEVEL_MIN = 0;
	static const int MIN_LEVEL_MAX = 100;

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetAbilityName() const {
		return Utf16ToUtf8(m_sAbilityName).c_str();
	}
	int GetAbilityMinimumLevel() const {
		return m_abilityMinimumLevel;
	}
	float GetAbilityMasteredExp() const {
		return m_abilityMasteredExp;
	}
	int GetAbilityMiscInt() const {
		return m_abilityMiscInt;
	}
	int GetAbilityMiscInt2() const {
		return m_abilityMiscInt2;
	}
	int GetAbilityMiscInt3() const {
		return m_abilityMiscInt3;
	}
	int GetEffectBind() const {
		return m_effectBind;
	}
	int GetMiscAmount() const {
		return m_miscAmount;
	}
	int GetAbilityFunctionID() const {
		return m_abilityFunctionID;
	}

	// modifiers
	void SetAbilityName(CStringA sAbilityName) {
		m_sAbilityName = Utf8ToUtf16(sAbilityName).c_str();
	}
	void SetAbilityMinimumLevel(int abilityMinimumLevel) {
		m_abilityMinimumLevel = abilityMinimumLevel;
	}
	void SetAbilityMasteredExp(float abilityMasteredExp) {
		m_abilityMasteredExp = abilityMasteredExp;
	}
	void SetAbilityMiscInt(int abilityMiscInt) {
		m_abilityMiscInt = abilityMiscInt;
	}
	void SetAbilityMiscInt2(int abilityMiscInt2) {
		m_abilityMiscInt2 = abilityMiscInt2;
	}
	void SetAbilityMiscInt3(int abilityMiscInt3) {
		m_abilityMiscInt3 = abilityMiscInt3;
	}
	void SetEffectBind(int effectBind) {
		m_effectBind = effectBind;
	}
	void SetMiscAmount(int miscAmount) {
		m_miscAmount = miscAmount;
	}
	void SetAbilityFunctionID(int abilityFunctionID) {
		m_abilityFunctionID = abilityFunctionID;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAbilitiesPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CAbilitiesPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;

	CStringW m_sAbilityName;
	int m_abilityMinimumLevel;
	float m_abilityMasteredExp;
	int m_abilityMiscInt;
	int m_abilityMiscInt2;
	int m_abilityMiscInt3;
	int m_effectBind;
	int m_miscAmount;
	int m_abilityFunctionID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ABILITIESPROPERTIESCTRL_H_)
