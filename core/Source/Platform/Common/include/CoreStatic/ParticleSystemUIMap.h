///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParticleSystemUIMap.h : header file
//
#include "ListItemMap.h"

namespace KEP {

class ParticleRM;

class CParticleSystemUIMap {
public:
	CParticleSystemUIMap() :
			m_particleSystem(0) {}
	CParticleSystemUIMap(ParticleRM* particleSystem) :
			m_particleSystem(particleSystem) {}
	~CParticleSystemUIMap() {}

	ListItemMap GetParticles();

private:
	ParticleRM* m_particleSystem;
};

} // namespace KEP