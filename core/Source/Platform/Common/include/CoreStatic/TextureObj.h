///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

#include "common\KEPUtil\Helpers.h"

#include "IResource.h"
#include "ITextureProxy.h"
#include "ITexture.h"

#define TEX_DL_PRIO_STEP 10
#define TEX_DL_PRIO_DEFAULT (RES_DL_PRIO_DEFAULT - TEX_DL_PRIO_STEP)

class CMapObj;

namespace KEP {

class TextureRM;
class IMemSizeGadget;

enum TextureQuality {
	TEX_NON,
	// From low to high
	TEX_1PX, // one-pixel texture
	TEX_LOW,
	TEX_MED,
	TEX_HIGH, // mapped to 5%, 25%, 50% JPGs
	TEX_DDS, // DDS
};

class CTextureEX : public CObject, public IResource, public ITextureProxy, public ITexture {
public:
	// ED-7732 - Texture Scaling
	static void ClearAllTextureMemoryUsage();
	static ULONGLONG GetAllTextureMemoryUsage();

	CTextureEX(D3DCOLOR colorKey = 0);
	CTextureEX(const std::string& texName, const std::string& subFolder, eTexturePath texPathToUse, const std::string& pathOverride = "", const std::string& urlOverride = "", D3DCOLOR colorKey = 0);
	CTextureEX(const CTextureEX& tex);

	virtual ~CTextureEX();

	std::string ToStr() const;

	// Frees D3D Textures From Memory & Resets State (INITIALIZE_NOT_LOADED).
	// Texture remains in TextureDatabase::texMap and it will be re-loaded on next CreateResource().
	void UnloadTexture();

	void Callback(LPDIRECT3DDEVICE9 pd3dDevice, int stage, int& type, float& parameter);

	IDirect3DTexture9* GetD3DTexture() const { return m_pTexture; }
	IDirect3DTexture9* GetD3DTextureIfLoadedOrQueueToLoad(bool bImmediate = false);
	virtual IDirect3DTexture9* GetOrQueueTexture() override;

	std::string GetTextureCategory() const {
		return StrStripFilePath(StrStripFileNameExt(GetFilePath()), ::PathApp(false));
	}

	D3DFORMAT GetFormat() const {
		return m_texSurfaceDesc.Format;
	}

	int GetWidth() const {
		return m_texSurfaceDesc.Width;
	}

	int GetHeight() const {
		return m_texSurfaceDesc.Height;
	}

	ULONGLONG GetTextureMemoryUsage() const;

	void InitChain();
	void ChainToLowerRes(CTextureEX* pLowerResTex, TextureQuality quality);
	bool IsChainHead() const {
		return m_chainHead;
	}
	void ChainBootstrap();

	// ED-7733 - Load all DO textures before you look at them
	void ChainLoad();

	// Load texture located at <mTexPathToUse>\<texName>
	bool LoadTextureByName(const std::string& texName, int mips, bool managed = false, bool UseTexLod = true, D3DCOLOR colorKey = 0);

	// Load texture located at <texPath>
	bool LoadTextureByPath(const std::string& texPath, int mips, bool UseTexLod, D3DCOLOR colorKey, bool attemptAsyncDownload = true);

	void SetForceFullRes(bool force) {
		m_forceFullRes = force;
	}
	bool GetForceFullRes() const {
		return m_forceFullRes;
	}

	void SetColorKey(const D3DCOLOR& colorKey) {
		m_colorKey = colorKey;
	}
	D3DCOLOR GetColorKey() const {
		return m_colorKey;
	}

	bool IsDDS() const {
		return StrSameFileExt(GetFileName(), "dds");
	}

	void SetEncryptionHashOverride(DWORD hashOverride) {
		m_encryptionHashOverride = hashOverride;
	}

	void SetAltColor(unsigned color) {
		m_altColorSet = true;
		m_altColor = color;
	}

	void SetTranscodeJPG() {
		m_transcodeJPG = true;
	}
	bool GetTranscodeJPG() const {
		return m_transcodeJPG;
	}

	// IResource override
	virtual bool CreateResource() override { return CTextureEX::CreateResourceEx() == eCreateResourceResult::SUCCESS; }
	virtual eCreateResourceResult CreateResourceEx() override;
	virtual unsigned char* GetEncryptionHashOverride(int& hash_len) override;
	virtual unsigned GetPriorityRangeSelector() const override {
		return m_priorityRangeSelector;
	}

	// ITextureProxy override
	virtual IDirect3DTexture9* getTexturePtr(bool chainLoad = false) override;
	virtual bool ready() override {
		return StateIsLoadedAndCreated();
	}

	// Enabled Quality Levels
	static const std::map<TextureQuality, std::string>& GetEnabledQualityLevels() {
		return ms_enabledQualityLevels;
	}
	static void ResetQualityLevels() {
		ms_enabledQualityLevels.clear();
	}
	static bool EnableQualityLevel(TextureQuality quality);

	// Force-One-Pixel flag
	static void SetForceOnePixel(bool val) {
		ms_forceOnePixel = val;
	}
	static bool IsForcingOnePixel() {
		return ms_forceOnePixel;
	}

	// Texture download priority visualization
	static void EnablePriorityVisualization(bool enable) {
		ms_priorityVisualizationEnabled = enable;
	}
	static bool IsPriorityVisualizationEnabled() {
		return ms_priorityVisualizationEnabled;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	static bool IsQualityLevelEnabled(TextureQuality quality) {
		return ms_enabledQualityLevels.find(quality) != ms_enabledQualityLevels.end();
	}

	bool IsSerializedDataCompressible() const override {
		return IsDDS();
	}

	IDirect3DTexture9* getTextureForAverageColor();
	IDirect3DTexture9* getTextureForPriorityVisualization();
	IDirect3DTexture9* createSingleColorTexture(unsigned color);

private:
	D3DSURFACE_DESC m_texSurfaceDesc; // drf - added

	D3DCOLOR m_colorKey;
	bool m_forceFullRes;

	eTexturePath m_texPathToUse;

	unsigned m_altColor; // Alternative color for constructing temporary single pixel texture
	bool m_altColorSet;

	CComPtr<IDirect3DTexture9> m_pTexture;
	CComPtr<IDirect3DTexture9> m_pTextureOnePixel;

	DWORD m_encryptionHashOverride;

	// Texture chaining
	// * Material ==========v
	// * TexChain: null <- Main (DDS) <-> Good <-> Average <-> Poor -> null
	// * Active:            `-----------------------------------^
	// * Priority:             170        180       190        200 (DEFAULT_PRIORITY)
	CTextureEX* m_lowerRes; // Forward link to lower quality/resolution
	CTextureEX* m_higherRes; // Reverse link back to higher quality/resolution
	CTextureEX* m_activeRes; // Shortcut link to currently active resolution (link valid for the main texture only)
	TextureQuality m_quality;
	bool m_chainHead;
	bool m_chainReady;
	bool m_transcodeJPG;
	unsigned m_priorityRangeSelector; // Lowest quality texture gets 0 (standard priority range), incremented by 1 (lower priority range) on each higher quality level

	static TextureRM* ms_pTextureRM;
	static std::map<TextureQuality, std::string> ms_enabledQualityLevels; // Enabled texture qualities
	static std::map<TextureQuality, std::string> ms_availQualityLevels; // All available texture qualities
	static bool ms_forceOnePixel;
	static bool ms_priorityVisualizationEnabled; // Use color code to visualize pending texture downloads (currently only works with dynamic object textures)
	static std::vector<IDirect3DTexture9*> ms_priorityVisTex; // Array of pointers to textures, used for priority visualization

	friend class TextureRM;
};

} // namespace KEP
