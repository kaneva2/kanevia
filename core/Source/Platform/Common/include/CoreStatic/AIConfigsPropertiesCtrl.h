/******************************************************************************
 AIConfigsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_AI_CONFIGS_PROPERTIESCTRL_H_)
#define _AI_CONFIGS_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AIConfigsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CAIConfigsPropertiesCtrl window

class CAIConfigsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CAIConfigsPropertiesCtrl();
	virtual ~CAIConfigsPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	void Populate();

	// accessors
	int GetConfigID() const {
		return m_configID;
	}

	// modifiers
	void SetConfigID(int configID) {
		m_configID = configID;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAIConfigsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CAIConfigsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_CONFIG
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	void InitBots();

private:
	int m_configID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_AI_CONFIGS_PROPERTIESCTRL_H_)
