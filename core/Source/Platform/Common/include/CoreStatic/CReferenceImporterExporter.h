///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "Core/Util/CArchiveObjectSchema.h"

#define OBJECT_REF_TYPE 0
#define LIGHT_REF_TYPE 1

namespace KEP {

class CReferenceImpExpObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CReferenceImpExpObj, VERSIONABLE_SCHEMA | 3);

public:
	CReferenceImpExpObj(){};
	CReferenceImpExpObj(CStringA setName, Matrix44f matrix);
	CReferenceImpExpObj(CStringA test, int type, Matrix44f matrix, float redValue, float greenValue, float blueValue, float startAttenuation, float endAttenuation, float intensity, int lightType, Vector3f direction, float theta, float phi, float falloff);

	CStringA m_idName;
	int m_type;
	Matrix44f m_matrix;
	float m_redValue;
	float m_blueValue;
	float m_greenValue;
	float m_intensity;
	float m_startAttenuation;
	float m_endAttenuation;

	//DPD Spotlight
	int m_lightType;
	Vector3f m_direction;
	float m_theta;
	float m_phi;
	float m_falloff;

	float m_atten0;
	float m_atten1;
	float m_atten2;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CReferenceImpExpList : public CObList {
public:
	CReferenceImpExpList(){};
	void SafeDelete();
	BOOL SaveToFile(CStringA fileName);
	BOOL AddNewReference(CStringA name, Matrix44f matrix);
	BOOL AddNewReference(CStringA name, int type, Matrix44f matrix, float redValue, float greenValue, float blueValue, float startAttenuation, float endAttenuation, float intensity, int lightType, Vector3f direction, float theta, float phi, float falloff);

	BOOL InitializeFromFile(CStringA fileName);

	DECLARE_SERIAL(CReferenceImpExpList);
};

} // namespace KEP