///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

#include "InstanceId.h"
#include <KEPNetwork.h>
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CWorldAvailableObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CWorldAvailableObj, VERSIONABLE_SCHEMA | 2)

public:
	CWorldAvailableObj();

	LONG m_index; // zero-based "id" of this zone, used to be index in array, but couldn't remove any.
	CStringA m_worldName;
	CStringA m_displayName; // added 10/06
	ChannelId m_channel;
	UINT m_maxOccupancy;
	UINT m_visibility;
	void Serialize(CArchive& ar);

	NETID m_aiServer; // if not zero, the AI server server this world

	DECLARE_GETSET
};

class CWorldAvailableObjList : public CObList {
public:
	CWorldAvailableObjList(){};

	DECLARE_SERIAL(CWorldAvailableObjList)

	void SafeDelete();
	void AddNewWorld(LONG index, const char* wldName, const char* dispName, UINT maxOccupancy);
	ChannelId AddWorldIfAvailable(const CStringA& wldName);
	ChannelId AddWorldIfAvailableByIndex(LONG index, const CStringA& wldName);
	ZoneIndex GetZeroBasedIndex(const CStringA& wldName);
	CWorldAvailableObj* GetByZoneIndex(const ZoneIndex& index);
	CWorldAvailableObj* GetByIndex(LONG index);

	void DeleteAndRemove(LONG index);

	void SerializeToXML(const std::string& fname);
	void SerializeFromXML(const std::string& fname);
	LONG FindAvailableId();

	bool UpdateDisplayName(LONG index, const char* newName) {
		CWorldAvailableObj* wa = GetByIndex(index);
		if (wa) {
			wa->m_displayName = newName;
		}
		return wa != 0;
	}

	// must be non-empty,  alpha numeric or space, not dupe
	bool validNewZoneName(const std::string& name) {
		if (name.size() == 0)
			return false;

		for (POSITION posLoc = GetHeadPosition(); posLoc != NULL;) {
			CWorldAvailableObj* wPtr = (CWorldAvailableObj*)GetNext(posLoc);
			if (wPtr->m_displayName.CompareNoCase(name.c_str()) == 0)
				return false;
		}

		std::locale loc = std::locale::classic();
		for (UINT i = 0; i < name.size(); i++)
			if (!isalnum(name[i]) && name[i] != ' ')
				return false;

		return true;
	}
};

class CSpawnCfgObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CSpawnCfgObj, VERSIONABLE_SCHEMA | 2)

public:
	CSpawnCfgObj();

	int m_id; // from database, not serialized
	Vector3f m_position;
	ZoneIndex m_zoneIndex;
	int m_rotation;
	CStringA m_spawnPointName;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CSpawnCfgObjList : public CObList {
public:
	CSpawnCfgObjList(){};

	DECLARE_SERIAL(CSpawnCfgObjList)

	// override CObList to prevent using old method of indexing into list, use FindId instead
	POSITION FindIndex(INT_PTR /*nIndex*/) const {
		ASSERT(FALSE);
		return NULL;
	}

	// override CObList to look up by id instead of index
	CSpawnCfgObj* FindId(int id) const {
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			CSpawnCfgObj* c = (CSpawnCfgObj*)GetNext(pos);
			if (c->m_id == id) {
				return c;
			}
		}
		return NULL;
	}

	void SafeDelete();

	void SerializeToXML(const std::string& fname);
	void SerializeFromXML(const std::string& fname);

	void AddSpawnPoint(int id, float x, float y, float z, int rotation, int zoneIndex, const char* name) {
		CSpawnCfgObj* c = new CSpawnCfgObj();
		c->m_id = id;
		c->m_position.x = x;
		c->m_position.y = y;
		c->m_position.z = z;
		c->m_rotation = rotation;
		c->m_zoneIndex = zoneIndex;
		c->m_spawnPointName = name;

		AddTail(c);
	}

	bool DeleteSpawnPointById(int id) {
		POSITION delPos;
		for (POSITION pos = GetHeadPosition(); (delPos = pos) != NULL;) {
			CSpawnCfgObj* c = (CSpawnCfgObj*)GetNext(pos);
			if (c->m_id == id) {
				RemoveAt(delPos);
				delete c;
				return true;
			}
		}
		return false;
	}

	// delete all spawn pts for a zoneIndex, returning the count
	int DeleteSpawnsPointByZoneIndex(int zoneIndex) { // plain zoneindex
		int count = 0;
		POSITION delPos;
		for (POSITION pos = GetHeadPosition(); (delPos = pos) != NULL;) {
			CSpawnCfgObj* c = (CSpawnCfgObj*)GetNext(pos);
			if (c->m_zoneIndex.zoneIndex() == zoneIndex) {
				RemoveAt(delPos);
				delete c;
				count++;
			}
		}
		return count;
	}

	// count the number of spawn points in the zone
	int SpawnPointsForZone(int zoneIndex) { // plain zoneindex
		int count = 0;
		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			CSpawnCfgObj* c = (CSpawnCfgObj*)GetNext(pos);
			if (c->m_zoneIndex.zoneIndex() == zoneIndex)
				count++;
		}
		return count;
	}

	// must be non-empty,  alpha numeric or space, not dupe
	bool validNewSpawnName(const std::string& name) {
		if (name.size() == 0)
			return false;

		for (POSITION pos = GetHeadPosition(); pos != NULL;) {
			CSpawnCfgObj* c = (CSpawnCfgObj*)GetNext(pos);
			if (c->m_spawnPointName.CompareNoCase(name.c_str()) == 0)
				return false;
		}

		std::locale loc = std::locale::classic();
		for (UINT i = 0; i < name.size(); i++)
			if (!isalnum(name[i]) && name[i] != ' ')
				return false;

		return true;
	}
};

} // namespace KEP