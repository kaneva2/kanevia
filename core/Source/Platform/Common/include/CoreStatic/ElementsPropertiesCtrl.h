/******************************************************************************
 ElementsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ELEMENTSPROPERTIESCTRL_H_)
#define _ELEMENTSPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ElementsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CElementsPropertiesCtrl window

class CElementsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CElementsPropertiesCtrl();
	virtual ~CElementsPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetName() const {
		return Utf16ToUtf8(m_sName).c_str();
	}

	// modifiers
	void SetName(CStringA sName) {
		m_sName = Utf8ToUtf16(sName).c_str();
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CElementsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CElementsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;

	CStringW m_sName;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ELEMENTSPROPERTIESCTRL_H_)
