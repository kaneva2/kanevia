///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CStructuresMisl.h"
#include <KEPNetwork.h>
#include <js.h>
#include <jsSemaphore.h>

namespace KEP {

class CPacketObj : public CObject {
public:
	CPacketObj(size_t sizeData, const BYTE* pData, NETID netId) :
			m_pData(nullptr),
			m_dataSize(sizeData),
			m_netId(netId) {
		if (pData && sizeData) {
			m_pData = new BYTE[sizeData];
			if (m_pData)
				CopyMemory(m_pData, pData, sizeData);
		}
	}

	void SafeDelete() {
		if (m_pData) {
			delete[] m_pData;
			m_pData = nullptr;
		}
	}

	MSG_CAT MsgCat() const {
		return (m_pData && m_dataSize) ? (MSG_CAT)*m_pData : MSG_CAT::NONE;
	}

	BYTE* m_pData;
	size_t m_dataSize;
	NETID m_netId;
};

// not thread safe, use for outgoing lists of packets
class CPacketObjList {
public:
	CObList m_list;
	size_t m_sizeData; // size of packet list (bytes)
	bool m_canSend;

	// Packet Length Header Format Enumeration
	enum LEN_FMT {
		LEN_FMT_NONE = 0,
		LEN_FMT_BYTE = 1,
		LEN_FMT_USHORT = 2,
		LEN_FMT_ULONG = 3
	} m_lenFmt;

	CPacketObjList(LEN_FMT lenFmt = LEN_FMT_NONE) :
			m_sizeData(0),
			m_lenFmt(lenFmt),
			m_canSend(true) {
	}

	virtual void AddPacket(size_t sizeData, const BYTE* pData, NETID netId = 0);

	virtual bool CanSend() const {
		return m_canSend;
	}

	void SetCanSend(bool canSend) {
		m_canSend = canSend;
	}

	void PurgeAll();

	void PurgeAllByID(NETID netId);

	void SafeDelete() { PurgeAll(); }

	size_t PackList(BYTE* ptr);

	size_t PackedSizeBytes() const {
		return m_sizeData;
	}

	CPacketObj* GetNextPacket();

	int GetCount() const {
		return m_list.GetCount();
	}

	POSITION GetHeadPosition() const {
		return m_list.GetHeadPosition();
	}

	CObject* GetNext(POSITION& pos) {
		return m_list.GetNext(pos);
	}

	static void MovePackets(CPacketObjList*& left, CPacketObjList*& right);

	static void EncodeMsgUpdate(
		BYTE*& pData,
		size_t& dataSize,
		MSG_CAT msgCat,
		CPacketObjList* msgOutboxAttrib,
		CPacketObjList* msgOutboxEquip,
		CPacketObjList* msgOutboxGeneral,
		CPacketObjList* msgOutboxEvent,
		bool forceSend);
};

// thread safe version for use for incoming packetObjLists
class CSerializedPacketObjList {
public:
	CSerializedPacketObjList(CPacketObjList::LEN_FMT lenSize = CPacketObjList::LEN_FMT_NONE) :
			m_list(lenSize), m_peakCount(0), m_cummulativePeakQueueDepth(0) {
		InitializeCriticalSection(&m_cs);
	}

	~CSerializedPacketObjList() {
		DeleteCriticalSection(&m_cs);
	}

	void AddPacket(int size, const BYTE* data, NETID netId) {
		AddPacketGetCount(size, data, netId);
	}

	int AddPacketGetCount(int size, const BYTE* data, NETID netId) {
		EnterCriticalSection(&m_cs);
		m_list.AddPacket(size, data, netId);
		int ret = m_list.GetCount();
		if (ret > m_peakCount)
			m_peakCount = ret;

		if (((unsigned long)ret) > m_cummulativePeakQueueDepth)
			m_cummulativePeakQueueDepth = ret;

		LeaveCriticalSection(&m_cs);
		m_semaphore.increment();
		return ret;
	}

	void PurgeAll() {
		EnterCriticalSection(&m_cs);
		m_list.PurgeAll();
		LeaveCriticalSection(&m_cs);
	}

	void PurgeAllByID(NETID netId) {
		EnterCriticalSection(&m_cs);
		m_list.PurgeAllByID(netId);
		LeaveCriticalSection(&m_cs);
	}

	void SafeDelete() { PurgeAll(); }

	jsSemaphore& semaphore() {
		return m_semaphore;
	}

	int PackedSizeBytes() {
		EnterCriticalSection(&m_cs);
		int ret = m_list.PackedSizeBytes();
		LeaveCriticalSection(&m_cs);
		return ret;
	}

	CPacketObj* GetNextPacket() {
		EnterCriticalSection(&m_cs);
		CPacketObj* ret = m_list.GetNextPacket();
		LeaveCriticalSection(&m_cs);
		return ret;
	}

	int GetCount() {
		EnterCriticalSection(&m_cs);
		int ret = m_list.GetCount();
		LeaveCriticalSection(&m_cs);
		return ret;
	}

	int GetAndResetPeak() {
		EnterCriticalSection(&m_cs);
		int ret = m_peakCount;
		m_peakCount = 0;
		LeaveCriticalSection(&m_cs);
		return ret;
	}

	unsigned long cummulativePeakQueueDepth() {
		return m_cummulativePeakQueueDepth;
	}

	CSerializedPacketObjList& resetCummulativePeakQueueDepth() {
		m_cummulativePeakQueueDepth = 0;
		return *this;
	}

private:
	jsSemaphore m_semaphore; // for packet queue
	CRITICAL_SECTION m_cs; // protect m_list
	CPacketObjList m_list;
	int m_peakCount;

	// Secondary peak tracker as the peak tracker above is used for the
	// perfmon counters and hence is reset way too frequently for the
	// purposes of the Monitor interface
	unsigned long m_cummulativePeakQueueDepth;
};

} // namespace KEP