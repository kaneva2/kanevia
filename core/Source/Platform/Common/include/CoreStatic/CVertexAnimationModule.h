///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

struct ABVERTEX0L;
class ReMesh;
class IMemSizeGadget;
class CAdvancedVertexObj;
class CEXMeshObj;

struct VertexPositionsFrame {
	ABVERTEX0L* m_vertexs;
	int m_vertexCount;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CVertexAnimationModule : public CObject, public GetSet {
	DECLARE_SERIAL(CVertexAnimationModule);

protected:
	bool checkNewFrameVertexCount(const std::string& newMeshName, int newVtxCount);

public:
	CVertexAnimationModule();

	int m_frameCount;
	VertexPositionsFrame* m_vertexFrames;
	int m_curFrame;
	int m_targetFrame;
	TimeMs m_timeBetweenFrames;
	BOOL m_looping;
	TimeMs m_lastUpdatedFrameChngTime;
	TimeMs m_currentFrameTime;

	void Serialize(CArchive& ar);
	void Clone(CVertexAnimationModule** clone);
	static bool Compare(CVertexAnimationModule* module1, CVertexAnimationModule* module2);
	bool AddKeyFrame(CEXMeshObj& aMesh);
	bool AddKeyFrame(ABVERTEX0L* vertices, int vtxCount);
	void ApplyAnimation(CEXMeshObj& aMesh, LPDIRECT3DDEVICE9 pd3dDevice);
	void ApplyAnimation(ReMesh* mesh, LPDIRECT3DDEVICE9 pd3dDevice);
	bool AddKeyFrameByMorphing(const CEXMeshObj& base, const CEXMeshObj& target, float weight);
	bool AddKeyFrameByMorphing(const CEXMeshObj& base, int numTargets, const CEXMeshObj* targets, float* weights);
	void SafeDelete();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CVertexAnimationModuleList : public CObList {
public:
	CVertexAnimationModuleList(){};
	void SafeDelete();

	DECLARE_SERIAL(CVertexAnimationModuleList);
};

} // namespace KEP