///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPObList.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Math/Vector.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CEXMeshObj;
class CFrameObj;
class CLaserObj;
class CMovementCaps;
class CRTLightObj;
class CSoundSampleObj;
class RuntimeSkeleton;

//class is basically used for popout
class CMissileObj : public CObject {
	DECLARE_SERIAL(CMissileObj);

public:
	CMissileObj();

	CStringA m_missileName;
	int m_contactAttribute; // not used
	int m_skillType; // not used
	int m_abilityType; // not used
	int m_traceOfTarget; // not used
	float m_heatSeekingPower; // not used
	BOOL m_forceExplodeAtEnd; // triggers explosion
	Vector3f m_contactPointStored; // state: collision world coordinates
	Vector3f m_collidedNormalStore; // state: collision world normal
	Vector3f m_lastPosition; // state: last world position
	TimeMs m_lifeTime; // life time
	int m_ownerTrace; // state: movement object trace number
	int m_damageValue; // damaging power
	BOOL m_orientateToCamera; // rendering attribute
	int m_particleLink; // particle ID
	float m_collisionRadius; // damaging radius (also validated by server)
	TimeMs m_startTimeStamp; // state: timestamp
	int m_explosionLink; // explosion ID
	int m_typeOfProjectile; //0=custom mesh 1=Billboard 2=skeleton 3=lazer segment 4=persistent laser		// Projectile Type/Visual Type
	int m_criteriaForSkillUse; //0=none 1=containment		// For skill improvement or skill check, not critical
	float m_pushPower; // physics against target (character only)
	int m_missileDBRef; // state: link to basis
	int m_damageChannel; // for some kind of server validation
	BOOL m_filterCollision; // collision against world object?
	UINT m_runtimeParticleId;
	CMovementCaps* m_physicDynamics;
	CFrameObj* m_baseFrame;
	CEXMeshObj* m_basicMesh;
	CLaserObj* m_laserObject;
	//BASIC SND
	BOOL m_enableSound;
	float m_distanceFactor;
	BOOL m_needInitPlay;
	int m_sndID;
	CSoundSampleObj* m_soundHandle;
	BOOL m_burstForceType;
	BOOL m_burstAppliedState;
	BOOL m_useFullPhysics;
	BOOL m_useElastics;
	CRTLightObj* m_light;
	int m_teamRef;
	int m_damageSpecific;
	float m_baseKickPower;

	void Clone(CMissileObj** copy, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void SafeDelete();
	void Serialize(CArchive& ar);

	RuntimeSkeleton* getRuntimeSkeleton() { return m_pRuntimeSkeleton.get(); }
	const RuntimeSkeleton* getRuntimeSkeleton() const { return m_pRuntimeSkeleton.get(); }

private:
	std::shared_ptr<RuntimeSkeleton> m_pRuntimeSkeleton;
};

class CMissileObjList : public CKEPObList {
public:
	void SafeDelete();
	CMissileObjList(){};

	DECLARE_SERIAL_SCHEMA(CMissileObjList, VERSIONABLE_SCHEMA | 1);

	void Serialize(CArchive& ar);
};

} // namespace KEP