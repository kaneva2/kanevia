/*
*	This class contains the hash codes that belong to item slot.
*	Each item in the game has a specific set of meshes (and their LOD's)
*	Plus a material
*
*	Most world objects will have only 1 of these, but character meshes
*	are going to have 1-N slots (current max is 25) that will contain
*	one of these.
*/

#pragma once

#define MAX_LODS_PER_SLOT 12
#define MAX_MESH_COMBINATIONS 20000

class LODMeshContainer {
public:
	__int64 MeshHashes[MAX_LODS_PER_SLOT];
	int mNumHashes;

	//	Collection of mesh combinations
	static LODMeshContainer MeshCombinations[MAX_MESH_COMBINATIONS];
	static int mNumMeshCombinations;

	//	Returns the index into the combination array used to hold the hashes
	int AddCombination(__int64* MeshHashes, int numHashes);

	//	Returns the number of hashes found, and fills the passed in array.
	//	If none are found with that index, returns 0
	//	NOTE: CALLER IS RESPONSIBLE FOR FREEING THIS MEMORY
	int GetCombination(__int64** MeshHashes, int CombinationIndex);

	void LoadMeshCombinationsFromFile(const char* path);
	void SaveMeshCombinationsToFile(const char* path);

	LODMeshContainer() {
		;
	}
	~LODMeshContainer() {
		;
	}
};

