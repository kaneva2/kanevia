/******************************************************************************
 ArenaPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ARENAPROPERTIESCTRL_H_)
#define _ARENAPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ArenaPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CArenaPropertiesCtrl window

class CArenaPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CArenaPropertiesCtrl();
	virtual ~CArenaPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetArenaName() const {
		return Utf16ToUtf8(m_sArenaName).c_str();
	}
	int GetArenaTypeID() const {
		return m_arenaTypeID;
	}
	long GetBattleMinimum() const {
		return m_battleMinimum;
	}
	long GetBattleMaximum() const {
		return m_battleMaximum;
	}
	long GetBattleDuration() const {
		return m_battleDuration;
	}
	int GetLevelRangeMax() const {
		return m_levelRangeMax;
	}
	int GetLevelRangeMin() const {
		return m_levelRangeMin;
	}
	int GetVisualWorldIndex() const {
		return m_visualWorldIndex;
	}
	int GetWorldChannel() const {
		return m_worldChannel;
	}
	int GetMaxSignupTimeMs() const {
		return m_maxSignupTimeMs;
	}
	CStringA GetScriptName() const {
		return Utf16ToUtf8(m_scriptName).c_str();
	}
	ULONG GetMaxScore() const {
		return m_maxScore;
	}
	ULONG GetMinTeams() const {
		return m_minTeams;
	}
	ULONG GetMaxTeams() const {
		return m_maxTeams;
	}

	// modifiers
	void SetArenaName(CStringA sArenaName) {
		m_sArenaName = Utf8ToUtf16(sArenaName).c_str();
	}
	void SetArenaTypeID(int arenaTypeID) {
		m_arenaTypeID = arenaTypeID;
	}
	void SetBattleMinimum(long battleMinimum) {
		m_battleMinimum = battleMinimum;
	}
	void SetBattleMaximum(long battleMaximum) {
		m_battleMaximum = battleMaximum;
	}
	void SetBattleDuration(long battleDuration) {
		m_battleDuration = battleDuration;
	}
	void SetLevelRangeMax(int levelRangeMax) {
		m_levelRangeMax = levelRangeMax;
	}
	void SetLevelRangeMin(int levelRangeMin) {
		m_levelRangeMin = levelRangeMin;
	}
	void SetVisualWorldIndex(int visualWorldIndex) {
		m_visualWorldIndex = visualWorldIndex;
	}
	void SetWorldChannel(int worldChannel) {
		m_worldChannel = worldChannel;
	}
	void SetMaxSignupTimeMs(int maxSignupTimeMs) {
		m_maxSignupTimeMs = maxSignupTimeMs;
	}
	void SetScriptName(const CStringA& s) {
		m_scriptName = Utf8ToUtf16(s).c_str();
	}
	void SetMaxScore(ULONG l) {
		m_maxScore = l;
	}
	void SetMinTeams(ULONG l) {
		m_minTeams = l;
	}
	void SetMaxTeams(ULONG l) {
		m_maxTeams = l;
	}

	// methods
	void AddArenaTypeItem(CStringA sItem, int index);
	void ClearArenaTypeItems();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CArenaPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CArenaPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_ARENA_NAME,
		INDEX_SCRIPT_NAME,
		INDEX_ARENA_TYPE,
		INDEX_BATTLE_MINIMUM,
		INDEX_BATTLE_MAXIMUM,
		INDEX_BATTLE_DURATION,
		INDEX_BATTLE_MAX_SCORE,
		INDEX_LEVEL_RANGE_MIN,
		INDEX_LEVEL_RANGE_MAX,
		INDEX_ZONE,
		INDEX_WORLD_CHANNEL,
		INDEX_MAX_SIGNUP_TIME,
		INDEX_MIN_TEAMS,
		INDEX_MAX_TEAMS
	};

	const static int NUM_ITEMS_MIN = 0;
	const static int NUM_ITEMS_MAX = 7;

	const static int PLAYERS_MIN = 1;
	const static int PLAYERS_MAX = 500;

	const static int PLAYER_LEVEL_MIN = 0;
	const static int PLAYER_LEVEL_MAX = 255;
	const static int MAX_SIGNUP_MS = 10000;
	const static ULONG MAX_SCORE = 1000000;
	const static ULONG MAX_TEAMS = 20;

private:
	void InitZones();
	void InitChannels();
	void InitArenaTypes();

private:
	CStringW m_sArenaName;
	int m_arenaTypeID;
	long m_battleMinimum;
	long m_battleMaximum;
	long m_battleDuration;
	int m_levelRangeMin;
	int m_levelRangeMax;
	int m_visualWorldIndex;
	int m_worldChannel;
	int m_maxSignupTimeMs;
	CStringW m_scriptName;
	ULONG m_maxScore;
	ULONG m_minTeams;
	ULONG m_maxTeams;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ARENAPROPERTIESCTRL_H_)
