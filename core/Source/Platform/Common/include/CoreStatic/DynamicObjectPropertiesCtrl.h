/******************************************************************************
 CurrentWorldPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_DYNAMIC_OBJECTSPROPERTIESCTRL_H_)
#define _DYNAMIC_OBJECTSPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CurrentWorldPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CDynamicObjectPropertiesCtrl window

class CDynamicObjectPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CDynamicObjectPropertiesCtrl();
	virtual ~CDynamicObjectPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	//virtual void	OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	CString GetName() const {
		return m_sName;
	}
	ULONG GetID() const {
		return m_id;
	}

	// modifiers
	void SetName(CString sName) {
		m_sName = sName;
	}
	void SetID(ULONG id) {
		m_id = id;
	}

	bool IsAttachableObject() const {
		return m_isAttachableObject;
	}
	bool IsInteractive() const {
		return m_isInteractive;
	}
	void SetAttachableObject(bool b) {
		m_isAttachableObject = b;
	}
	void SetInteractiveObject(bool b) {
		m_isInteractive = b;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynamicObjectPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CDynamicObjectPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_NAME,
		INDEX_ID,
		INDEX_IS_ATTACHABLE_OBJECT,
		INDEX_IS_INTERACTIVE_OBJECT
	};

	//void	LoadDynamicObject();

private:
	CStringW m_sName;
	ULONG m_id;
	bool m_isAttachableObject;
	bool m_isInteractive;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_DYNAMIC_OBJECTSPROPERTIESCTRL_H_)
