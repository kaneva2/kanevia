///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "InstanceId.h"
#include "KEPObList.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CAICfgOption : public CObject, public GetSet {
	DECLARE_SERIAL(CAICfgOption);

public:
	CAICfgOption();

	int m_aiCfg;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CAICfgOptionList : public CObList {
public:
	CAICfgOptionList(){};
	void SafeDelete();

	DECLARE_SERIAL(CAICfgOptionList);
};
//-------------------------------//
//---------ai spawn point--------//
//-------------------------------//
class CAISpawnPoint : public CObject {
	DECLARE_SERIAL(CAISpawnPoint);

public:
	CAISpawnPoint();

	Vector3f m_position;
	ChannelId m_channel;
	int m_spawnCfg;
	CAICfgOptionList* m_cfgList; //will randomize spawn
	BOOL m_commander;

	void SafeDelete();
	void Serialize(CArchive& ar);
};

class CAISpawnPointList : public CObList {
public:
	CAISpawnPointList(){};
	void SafeDelete();

	DECLARE_SERIAL(CAISpawnPointList);
};

class CAIRaidObj : public CObject, public GetSet {
	/*
	Version history
	2 	5/6/05 added manual start
*/
	DECLARE_SERIAL_SCHEMA(CAIRaidObj, VERSIONABLE_SCHEMA | 2);

public:
	CAIRaidObj();

	CStringA m_raidName;
	long m_raidDuration;
	TimeMs m_raidManditoryWait;
	TimeMs m_lastRaidTimeStamp;
	float m_bonusPoints;
	CStringA m_raidAlertMessage;
	float m_raidAlertRadius;
	ChannelId m_raidAlertChannel;
	Vector3f m_raidAlertPosition;
	int m_percentSpawn;
	BOOL m_manualStart;

	CAISpawnPointList* m_spawnPoints;

	void SafeDelete();
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CAIRaidObjList : public CKEPObList {
public:
	CAIRaidObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CAIRaidObjList);

	CAIRaidObj* CallBack(); //returns AI Raid Object to execute

	CAIRaidObj* FindByName(const char* name);
};

} // namespace KEP