///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "include\core\math\Matrix.h"
// added by serialize.pl
#include "GetSet.h"
#include "d3dx9math.h"
#include "common\include\CoreStatic\CBoneAnimationNaming.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class IMemSizeGadget;

#pragma pack(push, 1)
struct AnimKeyFrame {
	Vector3f up;
	Vector3f at;
	Vector3f pos;
	TimeMs m_timeMs;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};
#pragma pack(pop)

struct AnimationContentStreamedV3 {
	SHORT up[3];
	SHORT at[3];
	SHORT pos[3];
	USHORT time;
};

struct AnimationContentStreamedV4 {
	SHORT up[3];
	SHORT at[3];
	FLOAT pos[3];
	USHORT time;
};

struct AnimationContentUStreamed {
	FLOAT up[3];
	FLOAT at[3];
	FLOAT pos[3];
	FLOAT time;
};

//Base skeleton Class
class CBoneAnimationObject : public CObject {
	DECLARE_SERIAL_SCHEMA(CBoneAnimationObject, VERSIONABLE_SCHEMA | 4) // version 4: always use uncompressed pos component

public:
	CBoneAnimationObject();
	~CBoneAnimationObject() {
		SafeDelete();
	}

	void Init();

	void SafeDelete();

	void Serialize(CArchive& ar);

	void Clone(CBoneAnimationObject** clone) const;

	bool GetBoneMatrixByTime(TimeMs animTimeMs, Matrix44f& matrix) const;

	size_t GetKeyFrameIndexAtTime(TimeMs timeMs) const;

	void InterpolateKeyFrames(TimeMs elapsedTimeMs, Matrix44f* bonePtr, bool loopedAnimation) const;

	void RegenerateKeyFrames(FLOAT angTol = .25f, FLOAT posTol = .5f, UINT maxSkipped = 3);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	TimeMs m_animDurationMs;
	TimeMs m_animFrameTimeMs;
	BOOL m_animLooping;
	TimeMs m_animMinTimeMs;
	BOOL m_compressed;

	int m_animKeyFrames;
	AnimKeyFrame* m_pAnimKeyFrames;
	BOOL m_deleteKeyFrames;
};

class CBoneAnimationList : public CObList {
public:
	CBoneAnimationList();
	void SafeDelete();
	void Clone(CBoneAnimationList** clone);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	~CBoneAnimationList() {}
	int m_cloneCnt;

	DECLARE_SERIAL(CBoneAnimationList)
};

} // namespace KEP
