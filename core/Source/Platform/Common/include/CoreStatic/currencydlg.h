///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CURRENCYDLG_H__62032261_514A_11D5_B1EE_0000B4BD56DD__INCLUDED_)
#define AFX_CURRENCYDLG_H__62032261_514A_11D5_B1EE_0000B4BD56DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CurrencyDlg.h : header file
//
#include "CCurrencyClass.h"
#include "KEPImageButton.h"
#include "KEPEditDialog.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "CurrencyPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CCurrencyDlg dialog

class CCurrencyDlg : public CKEPEditDialog {
	// Construction
public:
	CCurrencyDlg(CWnd* pParent = NULL); // standard constructor
	virtual ~CCurrencyDlg();

	// Dialog Data
	//{{AFX_DATA(CCurrencyDlg)
	enum { IDD = IDD_DIALOG_CURRENCYOBJECT };
	CStringA m_currencyName;
	//}}AFX_DATA

	CCollapsibleGroup m_colCurrency;
	CButton m_grpCurrency;
	CCurrencyPropertiesCtrl m_propCurrency;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCurrencyDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	virtual void OnOK();
	//{{AFX_MSG(CCurrencyDlg)
	// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CURRENCYDLG_H__62032261_514A_11D5_B1EE_0000B4BD56DD__INCLUDED_)
