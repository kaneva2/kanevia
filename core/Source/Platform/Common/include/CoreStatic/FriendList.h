///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

#define FriendInfo CCommGroupObj
#define FriendList CCommGroupList

class FriendInfo : public CObject, public GetSet {
	DECLARE_SERIAL(FriendInfo);

public:
	FriendInfo();

	CStringA m_name;
	PLAYER_HANDLE m_handle;

	void Clone(FriendInfo** clone);
	void Serialize(CArchive& ar);
	void SerializeAlloc(CArchive& ar);

	DECLARE_GETSET
};

class FriendList : public CObList {
public:
	FriendList(){};
	DECLARE_SERIAL(FriendList);

	void SafeDelete();
	void Clone(FriendList** clone);
	void SerializeAlloc(CArchive& ar, int loadCount);
};

} // namespace KEP