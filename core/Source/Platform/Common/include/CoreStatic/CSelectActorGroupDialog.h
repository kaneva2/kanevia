/******************************************************************************
 SelectActorGroupDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "resource.h"

// SelectActorGroup dialog

class CSelectActorGroupDialog : public CDialog {
	DECLARE_DYNAMIC(CSelectActorGroupDialog)

public:
	CSelectActorGroupDialog(CWnd* pParent = NULL); // standard constructor
	virtual ~CSelectActorGroupDialog();

	// Dialog Data
	enum { IDD = IDD_DIALOG_SELECT_ACTOR_GROUP };

	// accessors
	//int		GetCameraIndex() const	{ return m_camera_index; }
	//CStringA	GetCameraName() const	{ return m_camera_name; }
	int GetSelectedActorGroupIndex();

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	void OnSelchangeListActorGroups();
	void OnBTNAddActorGroup();

	DECLARE_MESSAGE_MAP()

	virtual void OnOK();
	virtual BOOL OnInitDialog();

	//void		InitCameras();

private:
	CListBox m_actorGroups;
	CEdit m_editNameGroup;
	CButton m_addActorGroup;
	CButton m_replaceActorGroup;
	int m_selGroupIndex;
};