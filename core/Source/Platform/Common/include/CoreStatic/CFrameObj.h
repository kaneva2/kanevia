///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

namespace KEP {

class IMemSizeGadget;

class CFrameObjList : public CObList {
public:
	CFrameObjList(){};
	void SafeDelete();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CFrameObjList)
};

class CFrameObj : public CObject, public GetSet {
	DECLARE_SERIAL(CFrameObj)

	~CFrameObj() {}

public:
	CFrameObj(CFrameObj* pFO_parent = nullptr);

	void SafeDelete();
	void Serialize(CArchive& ar);

	void SetParent(CFrameObj* pFO_parent);

	void UpdateChildren(const Matrix44f& diffMatrix);

	int GetOrientationAngle() const;

	void GetTransform(CFrameObj* pFO, Matrix44f* matrix) const;

	void GetInverseTransform(Matrix44f* matrix);

	void AddTransformBefore(const Matrix44f& matrix);

	void AddTransformBefore2(CFrameObj* pFO, const Matrix44f& matrix);

	void AddTransformAfter(const Matrix44f& matrix);

	void AddTransformReplace(const Matrix44f& matrix);

	void AddTransformReplaceRotationOnly(const Matrix44f& matrix);

	void AddTransformReplaceNoChildAdjust(const Matrix44f& matrix);

	void AddTranslationBefore(float Xloc, float Yloc, float Zloc);

	void AddTranslationAfter(float Xloc, float Yloc, float Zloc);

	void AddTranslationReplace(float Xloc, float Yloc, float Zloc);

	void LookAt(CFrameObj* pFO_target);

	void FaceTowardPoint(const Vector3f& targetVect);

	void SetOrientation(const Vector3f& dir, const Vector3f& up);
	void GetOrientation(Vector3f& dir, Vector3f& up) const;

	void SetPosition(const Vector3f& pos, const CFrameObj* pFO = nullptr);
	void GetPosition(Vector3f& pos, const CFrameObj* pFO = nullptr) const;

	void SetPosDirUp(const Vector3f& pos, const Vector3f& dir, const Vector3f& up) {
		SetPosition(pos);
		SetOrientation(dir, up);
	}
	void GetPosDirUp(Vector3f& pos, Vector3f& dir, Vector3f& up) const {
		GetPosition(pos);
		GetOrientation(dir, up);
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

public:
	Matrix44f m_frameMatrix;
	CFrameObj* m_pFO_parent;
	CFrameObjList* m_pFOL_children;

protected:
	virtual double GetNumber(ULONG index);

	DECLARE_GETSET
};

} // namespace KEP
