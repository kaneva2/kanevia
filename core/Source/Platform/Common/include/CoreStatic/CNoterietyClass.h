///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "InstanceId.h"
#include "matrixarb.h"

namespace KEP {

class CAggressorObj : public CObject {
public:
	CAggressorObj();

	void Clone(CAggressorObj** clone);

	ChannelId m_channel;
	PLAYER_HANDLE m_handle;
	TimeMs m_timeStamp;
};

class CAggressorObjList : public CObList {
public:
	CAggressorObjList(){};
	void SafeDelete();
	void Clone(CAggressorObjList** clone);
	BOOL DoesHandleExist_IfSoRefresh(PLAYER_HANDLE& handle, TimeMs timeout);
	void CacheTimedOutAgressors(TimeMs timeout);
	BOOL DoesHandleExist(PLAYER_HANDLE handle, TimeMs timeout);
	void FilterByChannelID(const ChannelId& id);
};

class CNoterietyObj : public CObject, public GetSet {
	DECLARE_SERIAL(CNoterietyObj);

public:
	CNoterietyObj();

	int m_currentMurders;
	long m_currentMinuteCountSinceLastMurder;
	CAggressorObjList* m_currentAgressors;

	void SafeDelete();
	void Clone(CNoterietyObj** clone);
	void Serialize(CArchive& ar);
	BOOL AddAgressor(PLAYER_HANDLE handle, long agressorTimeout, const ChannelId& channel);
	BOOL DoesHandleExistInAgressorDB(PLAYER_HANDLE handle, long timeout);
	void SerializeAlloc(CArchive& ar);

	DECLARE_GETSET
};

} // namespace KEP