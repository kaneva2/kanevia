/******************************************************************************
 SkillsTitlesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(AFX_SKILLSTITLESCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
#define AFX_SKILLSTITLESCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SkillsTitlesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CSkillsTitlesCtrl window

class CSkillsTitlesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CSkillsTitlesCtrl();
	virtual ~CSkillsTitlesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetTitleName() const {
		return Utf16ToUtf8(m_sTitleName).c_str();
	}
	int GetTitleID() const {
		return m_titleID;
	}

	// modifiers
	void SetTitleName(CStringA sTitleName) {
		m_sTitleName = Utf8ToUtf16(sTitleName).c_str();
	}
	void SetTitleID(int titleID) {
		m_titleID = titleID;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSkillsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CSkillsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_id;
	CFont m_font;

	CStringW m_sTitleName;
	int m_titleID;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SKILLSPROPERTIESCTRL_H__A43F8FE1_E8DD_434B_B4DD_0445302B0317__INCLUDED_)
