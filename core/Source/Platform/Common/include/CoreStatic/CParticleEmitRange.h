///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "common\include\CoreStatic\CBoneAnimationNaming.h"
#include "Core/Util/CArchiveObjectSchema.h"

#define PARTICLE_DB_INDEX_INVALID -1

namespace KEP {

class IMemSizeGadget;

class CParticleEmitRangeObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CParticleEmitRangeObj, VERSIONABLE_SCHEMA | 1);

private:
	int m_animationQue;
	BOOL m_useExactRange;
	int m_startRange;
	int m_endRange;
	int m_particleDBIndex;
	BOOL m_inRange;
	int m_boneLink;
	unsigned m_runtimeParticleId;
	eAnimType m_animationType;
	int m_animationVersion;
	GLID m_animationGLID;
	int m_particleSlotId;
	GLID m_particleGLID;
	Matrix44f m_matrix;
	std::string m_strParticleSystemName;

public:
	CParticleEmitRangeObj(int boneIndex = 0, int particleSlotId = -1, GLID particleGLID = 0, GLID animationGLID = 0);
	void SafeDelete();
	void Clone(CParticleEmitRangeObj** clone);
	void Serialize(CArchive& ar);

	int getBoneIndex() const { return m_boneLink; }
	int getParticleSlotId() const { return m_particleSlotId; }
	GLID getParticleGLID() const { return m_particleGLID; }
	void setParticleDBIndex(int iDBIndex) { m_particleDBIndex = iDBIndex; }
	int getParticleDBIndex() const { return m_particleDBIndex; }
	const std::string& getParticleSystemName() const { return m_strParticleSystemName; }
	eAnimType getAnimationType() const { return m_animationType; }
	GLID getAnimationGLID() const { return m_animationGLID; }
	int getStartRange() const { return m_startRange; }
	int getEndRange() const { return m_endRange; }
	unsigned getRuntimeParticleId() const { return m_runtimeParticleId; }
	const Matrix44f& getMatrix() const { return m_matrix; }

	bool useExactRange() const { return m_useExactRange == TRUE; }
	bool inRange() const { return m_inRange == TRUE; }

	void setMatrix(const Vector3f& offset, const Vector3f& dir, const Vector3f& up) {
		m_matrix.MakeIdentity(); // Clear out any non-standard transforms
		m_matrix.GetTranslation() = offset;
		m_matrix.GetRowZ() = dir;
		m_matrix.GetRowY() = up;
		m_matrix.GetRowX() = up.Cross(dir);
	}

	void setMatrix(const Matrix44f& matrix) { m_matrix = matrix; }
	void setRanges(long start, long end);
	int getLegacyAnimationIndex() const { return m_animationQue; }
	void setAnimationInfo(eAnimType animType, int animVer);
	void setAnimationByGLID(GLID glid);
	void setRuntimeParticleId(unsigned id) { m_runtimeParticleId = id; }
	void setInRange(bool val) { m_inRange = val ? TRUE : FALSE; }
	void setParticleSystemName(const std::string& strName) { m_strParticleSystemName = strName; }

	bool triggerOnAnimation(eAnimType animType, int ver, GLID glid) {
		return m_animationGLID != 0 && glid == m_animationGLID || // Match GLID first if specified
			   m_animationType == animType && m_animationVersion == ver; // Otherwise, compare legacy animation type/ver combination
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET
};

class CParticleEmitRangeObjList : public CObList {
public:
	void SafeDelete();
	void Clone(CParticleEmitRangeObjList** clone);
	CParticleEmitRangeObjList(){};

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_SERIAL(CParticleEmitRangeObjList);
};

} // namespace KEP
