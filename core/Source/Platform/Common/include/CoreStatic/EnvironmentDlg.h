/******************************************************************************
 EnvironmentDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ENVIRONMENT_DLG_H_)
#define _ENVIRONMENT_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EnvironmentDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "EnvironmentLightingPropertiesCtrl.h"
#include "FogPropertiesCtrl.h"
#include "TimeStormPropertiesCtrl.h"
#include "PositionPropertiesCtrl.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "CEnvironmentClass.h"
#include "EditorState.h"
#include "KEPEditDialogBar.h"
#include "Common\UIStatic\BasicLayoutMFC.h"

////////////////////////////////////////////
// CEnvironmentDialog control

class CEnvironmentDialog : public CDialogBar { //CKEPEditDialogBar
	DECLARE_DYNAMIC(CEnvironmentDialog)

public:
	CEnvironmentDialog();
	virtual ~CEnvironmentDialog();

	void InitializeEnvironment();
	void UpdateEnvironment();

	void SetEnvironmentObj(CEnvironmentObj* environmentObj) {
		m_environmentObj = environmentObj;
	}
	CEnvironmentObj* GetEnvironmentObj() {
		return m_environmentObj;
	}

	// Dialog Data
	//{{AFX_DATA(CEnvironmentDialog)
	enum { IDD = IDD_DIALOG_ENVIORMENT };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CEnvironmentDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CEnvironmentObj* m_environmentObj;

	CCollapsibleGroup m_colFog;
	CFogPropertiesCtrl m_propFog;
	CButton m_grpFog;

	CCollapsibleGroup m_colTimeStorm;
	CTimeStormPropertiesCtrl m_propTimeStorm;
	CButton m_grpTimeStorm;

	CCollapsibleGroup m_colEnvironmentLighting;
	CEnvironmentLightingPropertiesCtrl m_propEnvironmentLighting;
	CPositionPropertiesCtrl* m_propSunPosition;
	CButton m_grpEnvironmentLighting;

	CKEPImageButton m_btnTimeSystem;
	BasicLayoutMFC m_layout;

public:
	bool CheckChanges();
};

#endif !defined(_ENVIRONMENT_DLG_H_)