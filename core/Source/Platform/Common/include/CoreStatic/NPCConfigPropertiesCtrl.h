/******************************************************************************
 NPCConfigPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_NPCCONFIGPROPERTIESCTRL_H_)
#define _NPCCONFIGPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NPCConfigPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

class CSpawnGenDlg;

/////////////////////////////////////////////////////////////////////////////
// CNPCConfigPropertiesCtrl window

class CNPCConfigPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CNPCConfigPropertiesCtrl(CSpawnGenDlg* pSpawnGenDlg);
	virtual ~CNPCConfigPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	CStringA GetNPCName() const {
		return Utf16ToUtf8(m_sNPCName).c_str();
	}
	CStringA GetDisplayName() const {
		return Utf16ToUtf8(m_sDisplayName).c_str();
	}
	int GetRotationAdjust();
	long GetRespawnDuration() const {
		return m_respawnDuration;
	}
	int GetCurAnimation() const {
		return m_animGLID;
	}
	int GetSpawnGenID() const {
		return m_spawnGenID;
	}
	int GetKeyID() const {
		return m_keyID;
	}
	float GetSightRangeOverride() const {
		return m_sightRangeOverride;
	}
	int GetActorID() const {
		return m_actorID;
	}
	int GetWorldID() const {
		return m_worldID;
	}
	BOOL GetExistsOnAllPermInstances() const {
		return m_existsInAllPermInstances;
	}
	int GetNPCAITypeID() const {
		return m_NPCAITypeID;
	}
	CStringA GetDeformableMeshID() const {
		return Utf16ToUtf8(m_deformableMeshID).c_str();
	}
	int GetMeshId() const {
		return m_meshId;
	}
	int GetMatlId() const {
		return m_matlId;
	}
	CStringA GetNoResponseMessage() const {
		return Utf16ToUtf8(m_sNoResponseMessage).c_str();
	}
	int GetRandomAnimation() const {
		return m_randomAnimation;
	}
	int GetRandomExpEffect() const {
		return m_randomExpEffect;
	}
	int GetRandomGlobalPerc() const {
		return m_randomGlobalPerc * RANDOM_GLOBAL_PERC_SCALE_FACTOR;
	}
	int GetMountOption() const {
		return m_mountOption;
	}
	int GetOrganizedGroupID() const {
		return m_organizedGroupID;
	}
	float GetPositionX() const {
		return m_positionX;
	}
	float GetPositionY() const {
		return m_positionY;
	}
	float GetPositionZ() const {
		return m_positionZ;
	}

	// modifiers
	void SetNPCName(CStringA sNPCName) {
		m_sNPCName = Utf8ToUtf16(sNPCName).c_str();
	}
	void SetDisplayName(CStringA sDisplayName) {
		m_sDisplayName = Utf8ToUtf16(sDisplayName).c_str();
	}
	void SetRotationAdjust(int rotationAdjust, bool bReorient = false);
	void SetRespawnDuration(long respawnDuration) {
		m_respawnDuration = respawnDuration;
	}
	void SetCurAnimation(int animGLID) {
		m_animGLID = animGLID;
	}
	void SetSpawnGenID(int spawnGenID) {
		m_spawnGenID = spawnGenID;
	}
	void SetKeyID(int keyID) {
		m_keyID = keyID;
	}
	void SetSightRangeOverride(float sightRangeOverride) {
		m_sightRangeOverride = sightRangeOverride;
	}
	void SetActorID(int actorID) {
		m_actorID = actorID;
	}
	void SetWorldID(int worldID) {
		m_worldID = worldID;
	}
	void SetExistsOnAllPermInstances(BOOL b) {
		m_existsInAllPermInstances = b;
	}

	void SetNPCAITypeID(int NPCAITypeID) {
		m_NPCAITypeID = NPCAITypeID;
	}
	void SetDeformableMeshID(CStringA deformableMeshID) {
		m_deformableMeshID = Utf8ToUtf16(deformableMeshID).c_str();
	}
	void SetMeshId(int meshId) {
		m_meshId = meshId;
	}
	void SetMatlId(int matlId) {
		m_matlId = matlId;
	}

	void SetNoResponseMessage(CStringA sNoResponseMessage) {
		m_sNoResponseMessage = Utf8ToUtf16(sNoResponseMessage).c_str();
	}
	void SetRandomAnimation(int randomAnimation) {
		m_randomAnimation = randomAnimation;
	}
	void SetRandomExpEffect(int randomExpEffect) {
		m_randomExpEffect = randomExpEffect;
	}
	void SetRandomGlobalPerc(int randomGlobalPerc) {
		m_randomGlobalPerc = randomGlobalPerc / RANDOM_GLOBAL_PERC_SCALE_FACTOR;
	}
	void SetMountOption(int mountOption) {
		m_mountOption = mountOption;
	}
	void SetOrganizedGroupID(int organizedGroupID) {
		m_organizedGroupID = organizedGroupID;
	}
	void SetPositionX(float positionX);
	void SetPositionY(float positionY);
	void SetPositionZ(float positionZ);

	// methods
	void Reset();
	void AddNPCAIType(CStringW* sNPCAIType);
	void AddDeformableMesh(CStringA sMesh);
	void ClearNPCAITypes();
	void ClearDeformableMeshes();
	void meshMatPopulate();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNPCConfigPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CNPCConfigPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void DeleteMeshNames();
	void InitChannels();
	void InitActors();

	static const int RESPAWN_DURATION_DEFAULT = 1000;
	static const float SIGHT_RANGE_OVERRIDE_DEFAULT; //	= 1.0f;
	static const int RANDOM_ANIMATION_DEFAULT = -1;
	static const int RANDOM_EXP_EFFECT_DEFAULT = -1;
	static const int MOUNT_OPTION_DEFAULT = -1;
	static const int RANDOM_GLOBAL_PERC_MIN = 0;
	static const int RANDOM_GLOBAL_PERC_MAX = 100;
	static const int RANDOM_GLOBAL_PERC_SCALE_FACTOR = 10;
	static const int RESPAWN_DURATION_MIN = 0;
	static const int RESPAWN_DURATION_MAX = 30000;
	static const float SIGHT_RANGE_OVERRIDE_MIN; //		= 1.0;
	static const float SIGHT_RANGE_OVERRIDE_MAX; //		= 3000.0;
	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	enum {
		INDEX_ROOT = 1,
		INDEX_NPC_NAME,
		INDEX_DISPLAY_NAME,
		INDEX_ROTATION,
		INDEX_RESPAWN_DURATION,
		INDEX_CURRENT_ANIMATION,
		//		INDEX_SPAWN_GEN_ID,
		INDEX_KEY_ID,
		INDEX_SIGHT_RANGE_OVERRIDE,
		INDEX_ACTOR,
		INDEX_WORLD_CHANNEL_FROM,
		INDEX_EXISTS_ON_ALL_CHANNELS,
		INDEX_NPC_AI_TYPE,
		INDEX_DEFORMABLE_MESH,
		INDEX_MESH_INDEX,
		INDEX_MAT_INDEX,
		INDEX_NO_RESPONSE_MSG,
		INDEX_POSITION,
		INDEX_X,
		INDEX_Y,
		INDEX_Z,
		INDEX_GET_CURRENT_POSITION,
		INDEX_ADD_APPENDAGE,
		INDEX_EXTENDED_SETTINGS,
		INDEX_RANDOM_ANIMATION,
		INDEX_RANDOM_EXP_EFFECT,
		INDEX_RANDOM_GLOBAL_PERC,
		INDEX_MOUNT_OPTION,
		INDEX_ORGANIZED_GROUP_ID
	};

private:
	CSpawnGenDlg* m_pSpawnGenDlg;

	CStringW m_sNPCName;
	CStringW m_sDisplayName;
	int m_rotationAdjust;
	long m_respawnDuration;
	int m_animGLID;
	int m_spawnGenID;
	int m_keyID;
	float m_sightRangeOverride;
	int m_actorID;
	int m_worldID;
	int m_NPCAITypeID;
	CStringW m_deformableMeshID;
	int m_meshId;
	int m_matlId;
	CStringW m_sNoResponseMessage;

	// extended properties
	int m_randomAnimation;
	int m_randomExpEffect;
	int m_randomGlobalPerc;
	int m_mountOption;
	int m_organizedGroupID;

	float m_positionX;
	float m_positionY;
	float m_positionZ;

	BOOL m_existsInAllPermInstances;

	std::vector<CStringW*> m_meshNames;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_NPCCONFIGPROPERTIESCTRL_H_)
