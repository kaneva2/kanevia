///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "KEPObList.h"

namespace KEP {

class CGFontObj : public CObject, public GetSet {
	DECLARE_SERIAL(CGFontObj);

public:
	CGFontObj();

	CStringA m_fontName;
	DWORD m_fontTableIndex;
	CStringA m_fontTableName;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CGFontObjList : public CKEPObList {
public:
	CGFontObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CGFontObjList)
};

} // namespace KEP