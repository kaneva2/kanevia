/******************************************************************************
 KEPPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Tools\Smart PropertyGrid\GWCPropertiesInc.h"

static UINT WM_KEP_PROPERTY_CTRL_CHANGED = ::RegisterWindowMessageW(L"WM_KEP_PROPERTY_CTRL_CHANGED");

class CKEPPropertiesCtrl : public GWCPropertiesCtrl {
public:
	CKEPPropertiesCtrl();
	virtual ~CKEPPropertiesCtrl();

	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnPropertyBeginEdit(GWCDeepIterator iter);
};
