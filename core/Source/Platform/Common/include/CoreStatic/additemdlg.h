///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CItemClass.h"
#include "CArmedInventoryClass.h"
#include "CMovementObj.h"
#include "CElementsClass.h"

#include "UseTypes.h"

/////////////////////////////////////////////////////////////////////////////
// CAddItemDlg dialog

class CAddItemDlg : public CDialog {
	// Construction
public:
	CAddItemDlg(CWnd* pParent = NULL); // standard constructor

	CItemObj* m_itemRef;
	CMovementObjList* m_enitityDB;
	CElementObjList* m_currentElements;
	CSkillObjectList* m_skillDatabaseRef;
	CStatObjList* m_statDatabaseRef;
	CSkeletonObject* m_skeletonReferencePtr;

	// Dialog Data
	//{{AFX_DATA(CAddItemDlg)
	enum { IDD = IDD_DIALOG_ADDITEM };
	CListBox m_expIconList;
	CButton m_stackable;
	CButton m_destroyWhenUsed;
	CButton m_nonTransferable;
	CButton m_defaultArmed;
	CListBox m_elementDefensiveList;
	CComboBox m_elements;
	CListBox m_statBonusList;
	CButton m_nonDrop;
	CComboBox m_useTypeComboBox;
	CButton m_permanent;
	CComboBox m_skillsAvailable;
	CButton m_checkDisarmable;
	CStringA m_itemName;
	int m_marketCost;
	int m_sellingPrice;
	int m_uniqueLocationID;
	int m_glID;
	int m_animapreference;
	int m_minimumSkillRequired;
	int m_masteredSkillLevel;
	int m_useValue;
	int m_armourRating;
	int m_currentCharges;
	int m_chargeType;
	int m_maxCharges;
	int m_keyID;
	int m_elementDefenseValue;
	int m_requiredSkillLevel;
	int m_iconDBIndex;
	int m_iconMiscInt;
	int m_iconMiscInt2;
	float m_iconPosXonscreen;
	float m_iconPosYonscreen;
	int m_defenseSpecificID;
	CStringA m_passListStr;
	CButton m_useAnywhere;
	// added 8/05 for lists of stats
	CComboBox m_statBonusSel;
	CComboBox m_statReqdSel;
	CListBox m_statReqdList;
	CComboBox m_iconList;
	CComboBox m_animationSetList;
	CStringA m_animationSetVersion;
	CComboBox m_actorGroup;
	CStringA m_itemDescription;

	//	Custom texture, if any
	CStringA mCurrentCustomTexture;

	CComboBox m_itemTypeList;
	//}}AFX_DATA
	int m_reloadDuration;

	CListBox m_deformableCfgList;
	CListBox m_exclusionGroupIDList;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddItemDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListStatBonuses(int sel);
	void InListStatReqd(int sel);
	CStringA ConvToString(int number);
	void InListElementStatus(int sel);
	void InListIconExpandable(int sel);
	void InListIcons(int sel);
	void InListAnimationSet(int sel);
	void InListDeformableConfigs(int sel);
	void InListExclusionGroupIDs(int sel);
	void OnSelchangeActorGroup();
	void OnSelectionChangeItemType();
	// Generated message map functions
	//{{AFX_MSG(CAddItemDlg)
	afx_msg void OnBUTTONAddAttachmentType();
	afx_msg void OnBUTTONCustomTexture();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnCHECKdisarmable();
	afx_msg void OnCHECKpermanent();
	afx_msg void OnBUTTONDeleteStatBonus();
	afx_msg void OnBUTTONAddStatBonus();
	afx_msg void OnSelchangeLISTStatBonusList();
	afx_msg void OnBUTTONAddElementDefense();
	afx_msg void OnBUTTONDeleteElementDefense();
	afx_msg void OnCHECKstackable();
	afx_msg void OnBUTTONAddIconExpObj();
	afx_msg void OnBUTTONDeleteIconExpObj();
	afx_msg void OnSelchangeLISTIconExpObjList();
	afx_msg void OnBUTTONDeleteStatReqd();
	afx_msg void OnBUTTONAddStatReqd();
	void OnSelchangeLISTStatReqdList();
	afx_msg void OnBUTTONAddDeformableCfg();
	afx_msg void OnBUTTONDeleteDeformableCfg();
	afx_msg void OnBUTTONAddExclusionGroupID();
	afx_msg void OnButtonDeleteExclusionGroupID();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	int m_visualRef;
	afx_msg void OnBnClickedOk();

	short m_dimConfigIndex;
	short m_dimConfigChangeTo;
	short m_dimConfigChangeBack;
	short m_dimConfigMaterial;
	BOOL m_dimConfigPreserve;

	CItemObj::DimVector m_dimConfigurations;

	vector<int> m_exclusionIDs;

protected:
	bool AddStatBonus(CComboBox& statBonusSel, CStatBonusList*& statBonuses, UINT editId);
	int DeleteStatBonus(CListBox& statBonusList, CStatBonusList*& statBonuses);
	void SelChangeBonusList(CStatBonusList*& requiredStats, CListBox& statReqdList, CComboBox& statReqdSel, UINT editID);
	void InitBonusList(int sel, CListBox& statBonusList, CStatBonusList*& statBonuses);
	void RefreshActorGroup();
	void InitUseType(const USE_TYPE& useType);
	void InitItemTypeList();
	void InitDeformableCfgList(int sel);
	void AddDeformableConfig(short dimIndex, short dimChangeTo, short dimChangeBack, short dimMaterial, BOOL dimPreserve);
	void InitExclusionGroupIDList(int sel);
	void AddExclusionGroupID(int exclusionID);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
