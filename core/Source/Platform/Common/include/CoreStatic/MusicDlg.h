/******************************************************************************
 MusicDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_MUSIC_DLG_H_)
#define _MUSIC_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MusicDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "KEPEditDialogBar.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

////////////////////////////////////////////
// CMusicDialog control

class CMusicDialog : public CKEPEditDialogBar {
	DECLARE_DYNAMIC(CMusicDialog)

public:
	CMusicDialog();
	virtual ~CMusicDialog();

	// Dialog Data
	//{{AFX_DATA(CMusicDialog)
	enum { IDD = IDD_DIALOG_MUSIC };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CMusicDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colCDAudio;
	CButton m_grpCDAudio;

	CCollapsibleGroup m_colWAVAudio;
	CButton m_grpWAVAudio;

	CCollapsibleGroup m_colSoundtrackList;
	CButton m_grpSoundtrackList;

	CKEPImageButton m_btnLoadWAVAudio;
	CKEPImageButton m_btnAddSoundtrack;
	CKEPImageButton m_btnDeleteSoundtrack;
	CKEPImageButton m_btnPlaySoundtrack;
	CKEPImageButton m_btnStopSoundtrack;

public:
	bool CheckChanges();
	void UpdateScript();
};

#endif !defined(_MUSIC_DLG_H_)