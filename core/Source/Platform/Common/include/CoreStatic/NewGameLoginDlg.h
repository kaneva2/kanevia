#pragma once

#include "KepPropertyPage.h"
#include "resource.h"
#include "StatLink.h"
#include "DistributionGameSelectionDlg.h"

// CNewGameLoginDlg dialog
class CDistributionGameSelectionDlg;
class CNewGameLoginDlg : public CKEPPropertyPage {
	DECLARE_DYNAMIC(CNewGameLoginDlg)

public:
	CNewGameLoginDlg(const char* email, const char* password, const char* gameDir, const char* editorBaseDir, bool skipExtraEditorDlgs, Logger& logger, bool clientRunning);
	virtual ~CNewGameLoginDlg();

	// Dialog Data
	enum { IDD = IDD_NEW_GAME_LOGIN };

	BOOL OnSetActive();
	LRESULT OnWizardNext();
	bool LoadGames();
	BOOL OnInitDialog();

	CGameData* GetGameDataByGameName(CStringA gameName);

	void SetEmail(CStringA email) {
		m_email = email;
	}
	void SetPassword(CStringA password) {
		m_password = password;
	}

	const CObList& GameData() {
		return m_gameData;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCreateNewAccount();

	CStringA m_email;
	CStringA m_password;
	CDistributionGameSelectionDlg* m_gameSelDlg;
	CObList m_gameData;
	Logger m_logger;
	CStringA m_gameDir;
	CStringA m_publishingServerLoginURL;
	CStringA m_publishingServerGetGamesURL;
	bool m_skipNextPage;
	bool m_skipExtraEditorDlgs;
	bool m_clientRunning;

	CStringA m_editorBaseDir;

	CFont m_loginAccountHeaderFont;
	CStaticLink m_lostPasswordLink;
};
