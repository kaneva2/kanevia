///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Util/HighPrecisionTime.h"
#include "Glids.h"
#include "common\include\CoreStatic\CBoneAnimationNaming.h"

namespace KEP {

class CBoneAnimationObject;
class CBoneList;
class IMemSizeGadget;

/*!
* \brief
* Skeleton definition to allow CSkeletonAnimation independent from CSkeletonObject class
*/
class SkeletonDef {
private:
	struct Bone {
		std::string name;
		float length;
		std::string parent;
		Bone(const std::string& n, float l, const std::string& p) :
				name(n), length(l), parent(p) {}

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	};
	std::vector<Bone> m_bonesList;

public:
	void Write(CArchive& ar);
	void Read(CArchive& ar);

	int GetBoneIndexByName(const std::string& name, bool bIgnoreCare, bool bConvertUnderscoreToSpace);

	std::string GetBoneNameByIndex(UINT index, bool bConvertUnderscoreToSpace);

	UINT GetBoneCount() {
		return m_bonesList.size();
	}

	bool IsEmpty() {
		return m_bonesList.empty();
	}

	void AddBone(const std::string& boneName, float length, const std::string& parentName) {
		m_bonesList.push_back(Bone(boneName, length, parentName));
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

/*!
* \brief
* Depricated animation class currently used in the ANM file.
*
* Recently replaced by CSkeletonAnimHeader and AnimationProxy, this
* class is now only used in the ANM file and to initialize those two
* classes if the EDB is an older version.  It can be removed entirely,
* when everyone has the updated EDB version and when the editor can
* read loose animation files, and so doesn't need the ANM.
*/
class CSkeletonAnimation {
protected:
	void init();
	void ReadLegacy(CArchive& ar);

public:
	CSkeletonAnimation();

	virtual ~CSkeletonAnimation();

	void Write(CArchive& ar);
	void Read(CArchive& ar);
	void Hash();

	UINT m_numBones;
	UINT m_animIndex;
	UINT m_animLooping;
	eAnimType m_animType;
	int m_animVer;
	TimeMs m_animMinTimeMs;
	TimeMs m_animDurationMs;
	TimeMs m_animFrameTimeMs;
	UINT m_refCount;
	UINT m_headerSize; // current header size (version0 + version 1)
	UINT64 m_hashCode;
	std::string m_name;
	GLID m_animGlid;

	void setAnimLooping(UINT looping) {
		m_animLooping = looping;
	}

	void setAnimType(eAnimType animType) {
		m_animType = animType;
	}

	void setAnimDurationMs(TimeMs timeMs) {
		m_animDurationMs = timeMs;
	}

	void setAnimFrameTimeMs(TimeMs timeMs) {
		m_animFrameTimeMs = timeMs;
	}

	void setNumBones(UINT cnt) {
		ASSERT(m_numBones == 0 && m_ppBoneAnimationObjs == NULL); // this function should only be called once -- to change bone count, dispose CSkeletonAnimation and allocate a new one
		m_numBones = cnt;
		m_ppBoneAnimationObjs = new CBoneAnimationObject*[m_numBones];
		for (UINT i = 0; i < m_numBones; i++)
			m_ppBoneAnimationObjs[i] = NULL;
	}

	CBoneAnimationObject* GetBoneAnimationObj(UINT boneIndex) {
		return (m_ppBoneAnimationObjs && (boneIndex < m_numBones)) ? m_ppBoneAnimationObjs[boneIndex] : nullptr;
	}

	const CBoneAnimationObject* GetBoneAnimationObj(UINT boneIndex) const {
		return (m_ppBoneAnimationObjs && (boneIndex < m_numBones)) ? m_ppBoneAnimationObjs[boneIndex] : nullptr;
	}

	CBoneAnimationObject** m_ppBoneAnimationObjs;
	SkeletonDef* m_skeletonDef;

	static const UINT CSKELETONANIMATION_KEYFRAMEDATASTRIDE = 13;

	static const UINT m_headerSize1 = sizeof(UINT) * 10; // version 0: m_numBones-m_headerSize
	static const UINT m_headerSize2 = sizeof(UINT64); // version 1: m_hashCode
};

} // namespace KEP
