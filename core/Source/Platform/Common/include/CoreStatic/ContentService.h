///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event\Base\IEvent.h"
#include "Event\Base\IEventHandler.h"
#include "UseTypes.h"
#include "PassGroups.h"
#include "Glids.h"
#include "Core/Util/fast_mutex.h"
#include "Common/Include/KEPHelpers.h"
#include "ItemTextureInfo.h"

#define CACHE_VERSION "5"

class TiXmlElement;
class TiXmlDocument;

namespace KEP {

class ContentMetadata { // Move up to global scope so it can be referred with a forward declaration
public:
	static const int CACHE_DURATION_1_DAY = 1440; // 1-day cache duration (minutes)
	static const int CACHE_DURATION = (10 * CACHE_DURATION_1_DAY); // default cache duration
	static const int EXPIRED_RUNS = 10; // runs unused content remains cached

	ContentMetadata() {
		Reset();
	}

	ContentMetadata(const GLID& glid, bool cacheIfNotFound = false);

	std::string ToStr() const;

	void Reset() {
		CacheDurationReset();
		ExpTimeReset();
		ExpRunsReset();
		HitsReset();
		m_glid = GLID_INVALID;
		m_baseGlid = GLID_INVALID;
		m_useType = USE_TYPE_INVALID;
		m_useValue = 0;
		m_passGroups = PASS_GROUP_INVALID;
		m_v4Texture = true;
		m_texUrl.clear();
		m_texPath.clear();
		m_texUrlJPG.clear();
		m_texPathJPG.clear();
		m_name.clear();
		m_desc.clear();
		m_pathThumbnail_XL.clear();
		m_pathThumbnail_L.clear();
		m_exclusionGroups.clear();
		m_primaryAsset.clear();
		m_localHash.clear();
		clearTextureInfo();
	}

	bool IsValid() const {
		return IS_VALID_GLID(getGlobalId());
	}

	static int TimeNowMin() {
		time_t timeMin = (time(NULL) / 60);
		return (int)timeMin;
	}

	void CacheDurationReset() {
		CacheDurationSet(CACHE_DURATION);
		ExpTimeReset();
	}

	int CacheDurationGet() const {
		return m_cacheDuration;
	}

	void CacheDurationSet(int timeMin) {
		if (timeMin > 0)
			m_cacheDuration = timeMin;
	}

	void ExpTimeReset() {
		m_expTime = TimeNowMin();
	}

	int ExpTimeGet() const {
		return m_expTime;
	}

	void ExpTimeSet(int timeMin) {
		m_expTime = timeMin;
	}

	int ExpTimeLeft() const {
		return (m_cacheDuration - (TimeNowMin() - m_expTime));
	}

	bool ExpiredTime() const {
		return (ExpTimeLeft() <= 0);
	}

	void ExpRunsReset() {
		m_expRuns = 0;
		++m_hits;
	}

	int ExpRunsGet() const {
		return m_expRuns;
	}

	void ExpRunsSet(int runs) {
		m_expRuns = runs;
	}

	int ExpRunsLeft() const {
		return (EXPIRED_RUNS - m_expRuns);
	}

	bool ExpiredRuns() const {
		return (ExpRunsLeft() <= 0);
	}

	void HitsReset() {
		m_hits = 0;
	}

	int HitsGet() const {
		return m_hits;
	}

	void HitsSet(int _hits) {
		m_hits = _hits;
	}

	bool PassGroup(const PASS_GROUP& passGroup) const {
		return ((getPassGroups() & (int)passGroup) != 0);
	}

	bool PassGroupAP() const {
		return PassGroup(PASS_GROUP_AP);
	}

	bool PassGroupVIP() const {
		return PassGroup(PASS_GROUP_VIP);
	}

	bool TypeGameItem() const {
		return (getUseType() == USE_TYPE_GAME_ITEM); // drf - does not include thumbnail
	}

	std::string LocalHashGet() const {
		return m_localHash;
	}

	void LocalHashSet(const std::string& hash) {
		m_localHash = hash;
	}

	GLID getGlobalId() const {
		return m_glid;
	}

	GLID getBaseGlobalId() const {
		return m_baseGlid;
	}

	USE_TYPE getUseType() const {
		return m_useType;
	}

	int getUseValue() const {
		return m_useValue;
	}

	int getPassGroups() const {
		return m_passGroups;
	}

	bool isV4Texture() const {
		return m_v4Texture;
	}

	std::string getPrimaryTextureUrl() const {
		return m_texUrl;
	}

	std::string getPrimaryTexturePath() const {
		return m_texPath;
	}

	std::string getJPEGTextureUrl() const {
		return m_texUrlJPG;
	}

	std::string getJPEGTexturePath() const {
		return m_texPathJPG;
	}

	std::string getItemName() const {
		return m_name;
	}

	std::string getItemDesc() const {
		return m_desc;
	}

	std::string getThumbnailPath() const {
		return getThumbnailPath_L();
	}

	std::string getExclusionGroups() const {
		return m_exclusionGroups;
	}

	void getPrimaryAsset(std::string& url, FileSize& fileSize, std::string& fileHash, unsigned& uniqueId) const {
		m_primaryAsset.get(url, fileSize, fileHash, uniqueId);
	}

	const TextureColorMap& getTextureColors() const {
		return m_textureColors;
	}

	const TextureUniqueIdMap& getTextureUniqueIds() const {
		return m_textureUniqueIds;
	}

	std::string getUniqueTextureFileStore() const {
		return m_uniqueTextureFileStore;
	}

	// XML encoding
	bool loadFromXML(const TiXmlElement* root);
	bool saveToXML(TiXmlElement* root) const;

	static std::string LocalTexturePathFromUrl(const std::string& texUrl);

protected:
	std::string getThumbnailPath_XL() const {
		return m_pathThumbnail_XL;
	}

	std::string getThumbnailPath_L() const {
		return m_pathThumbnail_L;
	}

	void setGlobalId(GLID glid) {
		m_glid = glid;
	}
	void setBaseGlobalId(GLID baseGlid) {
		m_baseGlid = baseGlid;
	}
	void setUseType(USE_TYPE useType) {
		m_useType = useType;
	}

	void setUseValue(int useValue) {
		m_useValue = useValue;
	}

	void setPassGroups(int passGroups) {
		m_passGroups = passGroups;
	}

	void setV4Texture(bool val) {
		m_v4Texture = val;
	}

	void setPrimaryTextureUrl(const std::string& texUrl) {
		m_texUrl = texUrl;
	}

	void setPrimaryTexturePath(const std::string& texPath) {
		m_texPath = texPath;
	}

	void setJPEGTextureUrl(const std::string& texUrlJPG) {
		m_texUrlJPG = texUrlJPG;
	}

	void setJPEGTexturePath(const std::string& texPathJPG) {
		m_texPathJPG = texPathJPG;
	}

	void setItemName(const std::string& itemName) {
		m_name = itemName;
	}

	void setItemDesc(const std::string& itemDesc) {
		m_desc = itemDesc;
	}

	void setExclusionGroups(const std::string& exclusionGroups) {
		m_exclusionGroups = exclusionGroups;
	}

	void setThumbnailPath_XL(const std::string& pathThumbnail) {
		m_pathThumbnail_XL = pathThumbnail;
	}

	void setThumbnailPath_L(const std::string& pathThumbnail) {
		m_pathThumbnail_L = pathThumbnail;
	}

	void setPrimaryAsset(const std::string& url, FileSize fileSize, const std::string& fileHash, unsigned uniqueId) {
		m_primaryAsset.set(url, fileSize, fileHash, uniqueId);
	}

	void clearTextureInfo() {
		m_textureColors.clear();
		m_textureUniqueIds.clear();
	}

	void addTextureColor(ItemTextureOrdinal ordinal, unsigned color) {
		m_textureColors.insert(std::make_pair(ordinal, color));
	}

	void addTextureUniqueId(ItemTextureOrdinal ordinal, unsigned uniqueId) {
		m_textureUniqueIds.insert(std::make_pair(ordinal, uniqueId));
	}

private:
	GLID m_glid;
	GLID m_baseGlid;
	USE_TYPE m_useType;
	int m_useValue;
	int m_passGroups; // bitmapped PASS_GROUP (AP, VIP)
	bool m_v4Texture;
	std::string m_texUrl; // content texture url
	std::string m_texPath; // content texture local path
	std::string m_texUrlJPG; // Texture URL - JPG version
	std::string m_texPathJPG;
	std::string m_name; // content name
	std::string m_desc; // content description
	std::string m_pathThumbnail_XL; // content thumbnail local path (not for USE_TYPE_GAME_ITEM!)
	std::string m_pathThumbnail_L; // content thumbnail local path (not for USE_TYPE_GAME_ITEM!)
	std::string m_exclusionGroups;
	TextureColorMap m_textureColors;
	TextureUniqueIdMap m_textureUniqueIds;
	std::string m_uniqueTextureFileStore;

	struct AssetInfo {
		std::string url; // Download URL
		FileSize fileSize; // File size in bytes
		std::string fileHash; // md5 checksum string (uppercase)
		unsigned uniqueId;

		void clear() {
			url.clear();
			fileSize = 0;
			fileHash.clear();
			uniqueId = 0;
		}

		void set(const std::string& u, FileSize sz, const std::string& hash, unsigned uid) {
			url = u;
			fileSize = sz;
			fileHash = hash;
			uniqueId = uid;
		}

		void get(std::string& u, FileSize& sz, std::string& hash, unsigned& uid) const {
			u = url;
			sz = fileSize;
			hash = fileHash;
			uid = uniqueId;
		}
	};

	AssetInfo m_primaryAsset;

	int m_cacheDuration; // time content is to remain cached (minutes)
	int m_expTime; // expiry timer for content to remain cached (minutes)
	int m_expRuns; // expiry runs for content unused in cache
	int m_hits; // number of cache hits

	std::string m_localHash; // local resource file hash
};

// DRF - TODO - Should This Just Be A Namespace?
class ContentService {
	ContentService();
	~ContentService();

public:
	static ContentService* Instance() {
		return &s_Instance;
	}

	// DRF - Cache Size On Disk Is Approx. 780 Bytes Per Entry
	// NOTE: The problem here is we currently load this into a single string to parse on startup and
	// it has been found to crash 32-bit Windows trying to allocate more then 40mb into a string.
	static const size_t CACHE_SIZE_SAVED_MAX = 50000; // max entries in LocalCache.xml (approx. 40mb)

	// DRF - Url Limitation On Server Is 2k
	static const size_t GLIDS_PER_WEBCALL_MAX = 200; // max glids requested per webcall (approx. 1k)

	// Load/Save Cached Content Metadata (<pathApp>\MapModels\LocalCache.xml)
	std::string LocalCacheFilePath();
	bool LocalCacheFileLoad();
	bool LocalCacheFileSave();

	// Query wokweb for item preload list
	bool UpdateItemPreloadList(int localPreloadListVersion);

	void ContentMetadataCacheLog(bool verbose);
	size_t ContentMetadataCacheSize();
	bool ContentMetadataIsCached(const GLID& glid);

	ContentMetadata* ContentMetadataCacheGetPtr(const GLID& glid);

	// Gets Content Metadata In Cache
	bool ContentMetadataCacheGet(ContentMetadata& md, const GLID& glid, bool cacheIfNotFound = false);

	// Unpack Content Metadata From Xml Into Cache
	size_t ContentMetadataXmlUnpackToCache(const std::string& cmXml, bool overwrite = true);
	size_t ContentMetadataXmlUnpackToCache(const TiXmlElement*, bool expiryReset, bool overwrite = true);

	// Synchronously Cache Content Metadata (blocking webcall and unpack xml into cache)
	void ContentMetadataCacheSync(const std::vector<GLID>& glids, bool forceGet = false);
	void ContentMetadataCacheSync(const GLID& glid, bool forceGet = false) {
		ContentMetadataCacheSync(std::vector<GLID>({ glid }), forceGet);
	}

	// Asynchronously cache content metadata (non-blocking webcall via OnUpdate() and unpack xml into cache and fire event)
	void ContentMetadataCacheAsync(const GLID& glid, class IEvent* e);
	void ContentMetadataCacheAsync(const GLID& glid, std::function<void()> fnCallback, void* pCallbackOwner) {
		ContentMetadataCacheAsync(std::vector<GLID>({ glid }), fnCallback, pCallbackOwner);
	}
	void ContentMetadataCacheAsync(const std::vector<GLID>& glids, std::function<void()> fnCallback, void* pCallbackOwner);

	// Asynchronous processing of glidsQueued[] to perform content metadata webcall (ItemsMetadataResultHandler() callback).
	void OnUpdate(bool bFlushRequests = false);

	// Asynchronous cache content metadata callback (unpack xml into cache and fire event)
	static EVENT_PROC_RC ItemsMetadataResultHandler(ULONG lparam, IDispatcher* dispatcher, IEvent* e);

private:
	EVENT_PROC_RC ItemsMetadataResultHandlerImpl(ULONG lparam, IDispatcher* dispatcher, IEvent* e);

	// Sets Content Metadata In Cache
	bool ContentMetadataCacheSet(ContentMetadata& md, bool expiryReset, bool overwrite = true);

	// Builds a URL to the web service from a list of item IDs
	std::string MakeRequestURL(const std::set<GLID>& glids);
	std::string MakePreloadRequestURL(int localPreloadListVersion);

	void DispatchEvent(IEvent* pEvent);

	std::string ContentMetadataUrl();

	static ContentService s_Instance;

	fast_recursive_mutex m_glidsCachedMutex;
	std::map<GLID, ContentMetadata> m_glidsCached;
	int m_preloadListVersion;

	fast_recursive_mutex m_pendingRequestsSync;
	std::set<GLID> m_glidsQueued;
	std::set<GLID> m_glidsInProcess;

	Timer m_updateTimer;
};

} // namespace KEP