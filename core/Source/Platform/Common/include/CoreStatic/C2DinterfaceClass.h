///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

class C2DvisObj : public CObject, public GetSet {
	C2DvisObj() {}

	DECLARE_SERIAL(C2DvisObj)

public:
	C2DvisObj(LPDIRECTDRAWSURFACE7 L_visSurface,
		LPDIRECTDRAWSURFACE7 L_baseSurface,
		LPDIRECTDRAWSURFACE7 L_upSurface,
		LPDIRECTDRAWSURFACE7 L_downSurface,
		IDXSurface* L_forground,
		IDXSurface* L_background,
		IDXSurface* L_onSurfModify,
		IDXSurface* L_outSurface,
		IDXSurfaceModifier* L_surfaceModifier,
		BOOL L_upState,
		BOOL L_fadeIn,
		BOOL L_initialTranslucentOnly,
		float L_translucency,
		int L_redKeySrc,
		int L_greenKeySrc,
		int L_blueKeySrc,
		int L_attribute,
		RECT L_positionSize,
		CStringA L_fileNameUp,
		CStringA L_fileNameDown,
		RECT L_percentageRect,
		BOOL L_actuated,
		float L_fadeVarial,
		float L_fadeSpeed,
		int L_attribMiscInt,
		CStringA L_attribMiscTxt);

	int m_attribMiscInt;
	CStringA m_attribMiscTxt;
	float m_fadeSpeed;
	float m_fadeVarial;
	BOOL m_actuated;
	RECT m_percentageRect;
	LPDIRECTDRAWSURFACE7 m_visSurface;
	LPDIRECTDRAWSURFACE7 m_baseSurface;
	LPDIRECTDRAWSURFACE7 m_upSurface;
	LPDIRECTDRAWSURFACE7 m_downSurface;
	IDXSurface* m_forground;
	IDXSurface* m_background;
	IDXSurface* m_onSurfModify;
	IDXSurface* m_outSurface;
	IDXSurfaceModifier* m_surfaceModifier;
	BOOL m_upState;
	BOOL m_fadeIn;
	BOOL m_initialTranslucentOnly;
	float m_translucency;
	int m_redKeySrc;
	int m_greenKeySrc;
	int m_blueKeySrc;
	int m_attribute;
	RECT m_positionSize;
	CStringA m_fileNameUp;
	CStringA m_fileNameDown;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class C2DvisObjList : public CObList {
public:
	C2DvisObjList(){};

	DECLARE_SERIAL(C2DvisObjList)
};
//---------------------------------------------//
//------main 2D display collection object------//
//---------------------------------------------//
class C2DinterfaceObj : public CObject {
	C2DinterfaceObj() {}

	DECLARE_SERIAL(C2DinterfaceObj)

public:
	C2DinterfaceObj(CStringA L_displayName,
		BOOL L_inUse,
		C2DvisObjList* L_displayCollection,
		int L_displType,
		int L_inheritMiscInt);

	int m_inheritMiscInt;
	int m_displType;
	CStringA m_displayName;
	BOOL m_inUse;
	C2DvisObjList* m_displayCollection;

	void Serialize(CArchive& ar);
};

class C2DinterfaceObjList : public CObList {
public:
	C2DinterfaceObjList(){};

	DECLARE_SERIAL(C2DinterfaceObjList)
};
#endif