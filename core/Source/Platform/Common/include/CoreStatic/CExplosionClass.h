///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPObList.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Core/Math/Vector.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CEXMeshObj;
class CFrameObj;
class CRTLightObj;
class CSoundSampleObj;
class RuntimeSkeleton;

class CExpKeyFrameObj : public CObject {
	DECLARE_SERIAL(CExpKeyFrameObj);

public:
	CExpKeyFrameObj();
	void Clone(CExpKeyFrameObj** copy);
	float m_scalerX;
	float m_scalerY;
	float m_scalerZ;
	float m_rotationX;
	float m_rotationY;
	float m_rotationZ;
	long m_duration;
	void Serialize(CArchive& ar);
};

class CExpKeyFrameObjList : public CObList {
public:
	CExpKeyFrameObjList(){};

	DECLARE_SERIAL(CExpKeyFrameObjList);

	//Functions
	void Clone(CExpKeyFrameObjList** copy);
	void SafeDelete();
};

class CExplosionObj : public CObject {
	DECLARE_SERIAL(CExplosionObj);

public:
	CExplosionObj();
	void SafeDelete();
	void SetFade(TimeMs timeMs);
	BOOL Clone(CExplosionObj** copy, LPDIRECT3DDEVICE9 g_pd3dDevice);
	CEXMeshObj* m_meshObject;
	long m_duration;
	CFrameObj* m_frame;
	CStringA m_explosionName;
	float m_baseScalerX;
	float m_baseScalerY;
	float m_baseScalerZ;
	float m_rotationX;
	float m_rotationY;
	float m_rotationZ;
	float m_lastScalerX;
	float m_lastScalerY;
	float m_lastScalerZ;
	float m_lastRotationX;
	float m_lastRotationY;
	float m_lastRotationZ;
	float m_damageRadius;
	int m_radiusDamage;
	TimeMs m_beginTimeStamp;
	int m_orientation; //0=none 1=to camera 2=to ground
	long m_startFadeAt; //<0 = none
	BOOL m_splashDamageFinsh;
	CExpKeyFrameObjList* m_keyFrames;
	POSITION m_currentKeyPosition;
	TimeMs m_currentKeyDuration;
	TimeMs m_currentKeyTimeStamp;
	BOOL m_needInitPlay;
	float m_clipRadius;
	//--------------------------------------------//
	BOOL m_enableSound;
	float m_distanceFactor;
	int m_snID;
	int m_ownerTrace;
	float m_pushPower;
	int m_dbIndexRef;
	int m_damageChannel;
	int m_teamRef;
	int m_damageSpecific;
	int m_traceFollow;
	int m_criteriaForSkillUse;
	CSoundSampleObj* m_soundHandle;
	CRTLightObj* m_light;

	BOOL IndexKeyframe();
	void AddKeyFrame(Vector3f scale, Vector3f rotation, int duration);
	BOOL LoadBasicMesh(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Serialize(CArchive& ar);

	RuntimeSkeleton* getRuntimeSkeleton() { return m_pRuntimeSkeleton.get(); }
	const RuntimeSkeleton* getRuntimeSkeleton() const { return m_pRuntimeSkeleton.get(); }

private:
	std::shared_ptr<RuntimeSkeleton> m_pRuntimeSkeleton;
};

class CExplosionObjList : public CKEPObList {
	DECLARE_SERIAL_SCHEMA(CExplosionObjList, VERSIONABLE_SCHEMA | 1);

public:
	CExplosionObjList(){};

	//Functions
	void SafeDelete();
	BOOL CreateExplosion(int dbIndex,
		Vector3f wldPosition,
		CExplosionObjList* runtimeRenderList,
		Vector3f lookatPosition,
		int ownerTrace,
		LPDIRECT3DDEVICE9 g_pd3dDevice,
		int teamRef,
		int traceFollow);
	BOOL AddExplosionToDB(CStringA fileName,
		int duration,
		float damageRadius,
		int orientation,
		int radiusDamage,
		LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Serialize(CArchive& ar);
};

} // namespace KEP