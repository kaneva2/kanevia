///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

enum eKeyCodeType {
	WindowsMessage,
	DirectInput
};

// The parser assumes the binary operators (And, Xor, Or) are listed in order of decreasing precedence.
enum class eLogicalOperator {
	Identity,
	Not,
	And,
	Xor,
	Or,
	Unknown,
};

inline bool IsBinaryOperator(eLogicalOperator op) {
	return op == eLogicalOperator::And || op == eLogicalOperator::Or || op == eLogicalOperator::Xor;
}

enum class eKeyEventType {
	Press,
	Release,
	Click,
	DoubleClick,
	None,

	Count
};

enum class eKeyStateType {
	Up = 1,
	Down = -1,
	Unknown = 0,
};

enum class eActionHandlerEventType {
	Activate,
	Deactivate,
	Sustain,
	Repeat,

	Cancel, // Deactivating due loss of focus or other non-key event.
	Restore, // Activating due to gain of focus or other non-key event.

	None,
	NumberOfTypes
};

struct KeyEvent {
	int m_iKeyId;
	eKeyEventType m_KeyEventType;

	int Compare(const KeyEvent& right) const {
		if (m_iKeyId != right.m_iKeyId)
			return m_iKeyId < right.m_iKeyId ? -1 : +1;
		if (m_KeyEventType != right.m_KeyEventType)
			return m_KeyEventType < right.m_KeyEventType ? -1 : +1;
		return 0;
	}
	bool operator==(const KeyEvent& right) const {
		return Compare(right) == 0;
	}
	bool operator<(const KeyEvent& right) const {
		return Compare(right) < 0;
	}
};

struct KeyState {
	int m_iKeyId;
	eKeyStateType m_keyStateType;

	int Compare(const KeyState& right) const {
		if (m_iKeyId != right.m_iKeyId)
			return m_iKeyId < right.m_iKeyId ? -1 : +1;
		if (m_keyStateType != right.m_keyStateType)
			return m_keyStateType < right.m_keyStateType ? -1 : +1;
		return 0;
	}
	bool operator==(const KeyState& right) const {
		return Compare(right) == 0;
	}
	bool operator<(const KeyState& right) const {
		return Compare(right) < 0;
	}
};

// Logical combination of key states.
struct KeyStateCombination {
	eLogicalOperator m_Operator;
	std::vector<KeyStateCombination> m_aCombinations;
	std::vector<KeyState> m_aKeyStates;

	bool IsEmpty() const {
		return m_aCombinations.empty() && m_aKeyStates.empty();
	}
	int Compare(const KeyStateCombination& right) const {
		if (m_Operator != right.m_Operator)
			return m_Operator < right.m_Operator ? -1 : +1;
		if (m_aCombinations.size() != right.m_aCombinations.size())
			return m_aCombinations.size() < right.m_aCombinations.size() ? -1 : +1;
		if (m_aKeyStates.size() != right.m_aKeyStates.size())
			return m_aKeyStates.size() < right.m_aKeyStates.size() ? -1 : +1;
		for (size_t i = 0; i < m_aKeyStates.size(); ++i) {
			const KeyState& ks1 = m_aKeyStates[i];
			const KeyState& ks2 = right.m_aKeyStates[i];
			int cmp = ks1.Compare(ks2);
			if (cmp != 0)
				return cmp;
		}
		for (size_t i = 0; i < m_aCombinations.size(); ++i) {
			const KeyStateCombination& ksc1 = m_aCombinations[i];
			const KeyStateCombination& ksc2 = right.m_aCombinations[i];
			int cmp = ksc1.Compare(ksc2);
			if (cmp != 0)
				return cmp;
		}
		return 0;
	}
	bool operator==(const KeyStateCombination& right) const {
		return Compare(right) == 0;
	}
	bool operator<(const KeyStateCombination& right) const {
		return Compare(right) < 0;
	}
};

struct BindableKeyEvent {
	// List of key events (press,release,click) tied to this action. When empty, only the key states min m_KeyCombination matter.
	std::vector<KeyEvent> m_aKeyEvents;

	// Key state combination that must be satisfied when one of the above events occurs.
	KeyStateCombination m_KeyCombination;
};

struct KeyEventBindingByName {
	// Name of action that is to be triggered.
	std::string m_strActionName;

	BindableKeyEvent m_BindableKeyEvent;
};

} // namespace KEP