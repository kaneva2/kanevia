///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class IKEPDropTarget;

class DropTargetProxy : public IDropTarget {
private:
	int refcnt;
	IKEPDropTarget& target;
	bool bAllowed;

public:
	DropTargetProxy(IKEPDropTarget& aTarget) :
			refcnt(0), target(aTarget), bAllowed(false) {}

	// IUnknown members
	HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void __RPC_FAR* __RPC_FAR* ppvObject);
	ULONG STDMETHODCALLTYPE AddRef(void);
	ULONG STDMETHODCALLTYPE Release(void);

	//IDropTarget members
	HRESULT STDMETHODCALLTYPE DragEnter(
		IDataObject* pDataObj,
		DWORD grfKeyState,
		POINTL pt,
		DWORD* pdwEffect);

	HRESULT STDMETHODCALLTYPE DragOver(
		DWORD grfKeyState,
		POINTL pt,
		DWORD* pdwEffect);

	HRESULT STDMETHODCALLTYPE DragLeave(void);

	HRESULT STDMETHODCALLTYPE Drop(
		IDataObject* pDataObj,
		DWORD grfKeyState,
		POINTL pt,
		DWORD* pdwEffect);

protected:
	bool ProcessDragDrop(BOOL bDrop, IDataObject* pDataObj, DWORD grfKeyState, const POINTL& pt);
};

} // namespace KEP