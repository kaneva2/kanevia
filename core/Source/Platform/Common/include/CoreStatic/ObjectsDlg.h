/******************************************************************************
 ObjectsDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_OBJECTS_DLG_H_)
#define _OBJECTS_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ObjectsDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "KEPImageToggleButton.h"
#include "CollapsibleGroup.h"
#include "Common\UIStatic\BasicLayoutMFC.h"

////////////////////////////////////////////
// CObjectsDialog control

class CObjectsDialog : public CDialogBar {
	DECLARE_DYNAMIC(CObjectsDialog)

public:
	CObjectsDialog();
	virtual ~CObjectsDialog();

	void EnableSelectButton(bool enable = true);
	void EnableMoveButton(bool enable = true);
	void EnableRotateButton(bool enable = true);
	void EnableScaleButton(bool enable = true);

	void SetSelected();

	// Dialog Data
	//{{AFX_DATA(CObjectsDialog)
	enum { IDD = IDD_DIALOG_OBJECTADD };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CObjectsDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colObjectGroups;
	CButton m_grpObjectGroups;

	CCollapsibleGroup m_colVisualInGroup;
	CButton m_grpVisualInGroup;

	CCollapsibleGroup m_colAttrib;
	CButton m_grpAttrib;

	CCollapsibleGroup m_colLODData;
	CButton m_grpLODData;

	CCollapsibleGroup m_colReference;
	CButton m_grpReference;

	CKEPImageButton m_btnAddGroup;
	CKEPImageButton m_btnDeleteGroup;
	CKEPImageButton m_btnEditGroup;
	CKEPImageButton m_btnGroupModifier;

	CKEPImageButton m_btnUpdateObject;
	CKEPImageButton m_btnLoadObject;
	CKEPImageButton m_btnDeleteObject;
	CKEPImageButton m_btnObjectMaterial;
	CKEPImageButton m_btnEditObject;
	CKEPImageButton m_btnObjectSound;

	CKEPImageToggleButton m_btnObjectTransform;
	CKEPImageToggleButton m_btnObjectSelect;
	CKEPImageToggleButton m_btnObjectMove;
	CKEPImageToggleButton m_btnObjectRotate;
	CKEPImageToggleButton m_btnObjectScale;

	CKEPImageButton m_btnAddLight;
	CKEPImageButton m_btnDeleteLight;
	CKEPImageButton m_btnEditLight;
	CKEPImageButton m_btnAddTrigger;

	CKEPImageButton m_btnAddLOD;
	CKEPImageButton m_btnDeleteLOD;
	CKEPImageButton m_btnEditLOD;

	BasicLayoutMFC m_layout;
};

#endif !defined(_OBJECTS_DLG_H_)