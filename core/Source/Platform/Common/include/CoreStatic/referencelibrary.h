/******************************************************************************
referencelibrary.h

Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once
// ReferenceLibrary.h : header file
//
#include "CReferenceLibrary.h"
#include "TextureDatabase.h"
#include "CABFunctionLib.h"
#include "CWldObject.h"
#include "resource.h"

class CEXScriptObjList;

/////////////////////////////////////////////////////////////////////////////
// CReferenceLibrary dialog

class CReferenceLibrary : public CDialog {
protected:
	// Member functions for Model

	//! \brief Add one or more reference objects from specified URI.
	//! \param URI URI where data source is located.
	//! \param replacePosition If NULL, append new obj to the end of RLB, otherwise, replace specified entry.
	//! \return TRUE if succeeded. FALSE otherwise (invalid URI, bad data, etc).
	//!
	//! Call this function to either add one or more ref objs or to replace one *DELETED* obj.
	//! To replace a "live" obj, call ReplaceReferenceObjects() instead.
	BOOL LoadReferenceObjects(const string& URI, POSITION replacePosition = NULL);

	//! \brief Delete one single reference object from reference library by name.
	//! \param name Name of the reference object to be deleted.
	//! \return TRUE if succeeded. FALSE if name not found.
	BOOL DeleteReferenceObject(const string& name);

	//! \brief Delete one LOD for an existing reference object by object name and LOD.
	//! \param name Name of the reference object from which the LOD is to be deleted.
	//! \param lodLevel LOD level to be deleted.
	//! \return TRUE if succeeded. FALSE if name not found.
	BOOL DeleteReferenceObjectLOD(const string& name, int lodLevel);

	//! \brief Replace mesh data for an existing reference object.
	//! \param URI URI where data source is located.
	//! \param pExistingObj Existing reference object whose meshes are to be replaced.
	//! \return TRUE if succeeded. FALSE otherwise.
	//!
	//! Replace mesh data for an existing reference object. Original activation settings will remain unchanged.
	//! All existing LODs will be deleted before import.
	BOOL ReplaceReferenceObjectMeshes(const string& URI, CReferenceObj* pExistingObj);

	//! \brief Replace mesh for single LOD.
	//! \param pRefObj provide a CReferenceObj for mesh replacement (required)
	//! \param LODNo destination LOD level (-1 means append new LOD) (required)
	//! \return TRUE if succeeded. FALSE otherwise.
	//!
	//! Replace mesh for single LOD. If LOD0, replace the main mesh in CReferenceObj otherwise the mesh in CReference::m_lodVisualList[#]
	BOOL ReplaceReferenceObjectLODMesh(const string& URI, CReferenceObj* pRefObj, int lodNo);

	//! Find existing reference objects by name, return NULL if not found
	CReferenceObj* FindReferenceObjectByName(const string& name);

	//! \brief Launch a dialog to resolve name conflict caused by the newly imported object
	//! \param name Name in conflict.
	//! \param bAutoRename TRUE if automatic rename without UI interference, might be overridden by user input (IN/OUT)
	//! \param pObjReplace The object whose name is being replaced. The original name of this object should be excluded from conflict check.
	//! \return FALSE if name conflict is unresolvable (e.g. cancelled by user)
	BOOL ResolveNameConflict(string& name, BOOL& bAutoRename, CReferenceObj* pObjReplace = NULL);

	// End Model

	// Construction
public:
	CReferenceLibrary(CWnd* pParent = NULL); // standard constructor

	CReferenceObjList* m_referenceDBREF;
	TextureDatabase* GL_textureDataBase;
	LPDIRECT3DDEVICE9 g_pd3dDeviceRef;
	CWldObjectList* m_worldObjListRef;
	CEXScriptObjList* m_sceneScriptExec;
	CStringA* m_textureSearchPathsRef;
	int m_texturePathCountRef;
	CWnd* m_editor;

	CButton m_faceCameraChk;
	CStateManagementObj* m_renderStateObjectRef;
	// Dialog Data
	//{{AFX_DATA(CReferenceLibrary)
	enum { IDD = IDD_DIALOG_REFERENCELIBRARY };
	CListBox m_lodDB;
	CListCtrl m_referenceLibraryDB;
	float m_popoutBoxMaxX;
	float m_popoutBoxMaxY;
	float m_popoutBoxMaxZ;
	float m_popoutBoxMinX;
	float m_popoutBoxMinY;
	float m_popoutBoxMinZ;
	CStringA m_nameEdit;
	BOOL m_sortByName;
	BOOL m_showDeleted;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReferenceLibrary)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// UI helper functions
	void AddLODToList(int lodNo);
	void RemoveLODFromList(int lodNo);
	void InListLods(int sel);
	BOOL InList(int sel);
	int GetSelectedReferenceObj(CReferenceObj** ppRefObj, int* pSelectionIndex = NULL, int* pRLBIndex = NULL);
	int GetSelectedLOD(CReferenceObj* pRefObj, CMeshLODObject** ppLOD = NULL, int* pSelectionIndex = NULL);
	bool SaveReferenceLibrary(CReferenceObjList* referenceLibrary, CStringA& fileName);
	void AppendRefLib(CReferenceObjList* dst, CReferenceObjList* src);
	void SetAllPopoutBoxes();

	// Validate returned reference obj (valid, not found, deleted, not selected) and show warning message if requested by caller
	BOOL CheckRLBRetCode(int nRetCode, BOOL bWarnDeleted = TRUE, BOOL bWarnNotSelected = TRUE, BOOL bWarnNotFound = FALSE);

	BOOL CheckLODRetCode(int nRetCode, BOOL bAllowBaseMesh = TRUE, BOOL bAllowCollision = TRUE, BOOL bWarnBaseMesh = TRUE, BOOL bWarnCollision = TRUE, BOOL bWarnNotSelected = TRUE, BOOL bWarnInvalid = TRUE);

	BOOL UpdateCollisionModel(CReferenceObj* pRefObj);
	BOOL DeleteCollisionModel(CReferenceObj* pRefObj);

	BOOL UpdateBaseMesh(CReferenceObj* pRefObj);
	BOOL UpdateLODSettings(CReferenceObj* pRefObj, int lodNo, CMeshLODObject* pLod);

	// Generated message map functions
	//{{AFX_MSG(CReferenceLibrary)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	afx_msg void OnBUTTONRefMaterial();
	afx_msg void OnBUTTONReplace();
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBUTTONRemoveCollisionModel();
	afx_msg void OnBUTTONAddCollisionModel();
	afx_msg void OnBUTTONAddLod();
	afx_msg void OnBUTTONDeleteLod();
	afx_msg void OnBUTTONEditLod();
	afx_msg void OnItemChangedLISTReferenceLibrary(NMHDR* pNMHdr, LRESULT* pResult);
	afx_msg void OnSelchangeLISTReferenceLibrary();
	afx_msg void OnBUTTONSetPopoutBox();
	afx_msg void OnBUTTONSaveLib();
	afx_msg void OnBUTTONSaveUnit();
	afx_msg void OnBUTTONImportUnit();
	afx_msg void OnBUTTONLoadLib();
	afx_msg void OnBUTTONSetName();
	afx_msg void OnBUTTONReinitClipBoxes();
	afx_msg void OnBUTTONCopyClipBoxToLods();
	afx_msg void OnOK();
	afx_msg void OnBnClickedShowDeleted();
	afx_msg void OnBnClickedSortByName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedButtonAnimationsettings();
	afx_msg void OnBnClickedButtonUnifynormals();
	afx_msg void OnBnClickedButtonRegeneratenormals();
	afx_msg void OnBnClickedCheckFacetocam();
	afx_msg void OnBnClickedButtonloadspt();
	afx_msg void OnBnClickedButtonOptLib();
	afx_msg void OnBnClickedButtoneditlodmaterial();

private:
	int m_referenceLibrarySel;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
