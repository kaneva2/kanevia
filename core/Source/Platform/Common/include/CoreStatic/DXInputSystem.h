///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// DirectInput is a component of DirectX for handling mouse and keyboard input.
// This was written as a light abstraction layer around DirectInput.
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "CABD3DFunctionLib.h"

/// DirectInput Keys
enum {
	DIK_FIRST_DEFINED = DIK_ESCAPE, // first defined key code
	DIK_LAST_DEFINED = DIK_MEDIASELECT, // last defined key code
	DIK_KEYS = 256
};

/// DirectInput Mouse Buttons Enumeration
enum {
	DIM_BUTTON_LEFT,
	DIM_BUTTON_RIGHT,
	DIM_BUTTON_MIDDLE,
	DIM_BUTTON_MIDDLE2,
	DIM_BUTTONS = 4
};

#define DI_DOWN_BIT 0x80 // high order bit flags down state

/// Keyboard State Abstraction
struct KeyboardState {
	CHAR ks[DIK_KEYS]; // DirectInput Keyboard State
	bool enabled; // drf - added - keyboard enabled
	DInputDevice diDevice;

	KeyboardState() {
		Reset();
	}
	KeyboardState(const KeyboardState& _ks) {
		*this = _ks;
	}

	void Reset() {
		ZeroMemory(this, sizeof(*this));
		enabled = true;
	}

	// DRF - Packed into ClientEngine::MouseKeyboardUpdate()->KeyPressedEvent (used only by DDR for triggering dance moves)
	CHAR* Ptr() {
		return ks;
	}

	void Enable(bool enable) {
		enabled = enable;
	}

	void DirectInputDevice(DInputDevice device) {
		diDevice = device;
		Update();
	}

	bool Update() {
		if (!diDevice)
			return false;
		return (diDevice->GetDeviceState(sizeof(ks), (void*)ks) == DI_OK);
	}

	std::string ToStr() const {
		std::string str;
		StrBuild(str, "KS<" << this << "> enabled=" << (enabled ? "YES" : "NO") << " keysDown=" << KeysDown());
		for (size_t i = 0; i < Keys(); ++i)
			if (KeyDown(i)) {
				StrAppend(str, " 0x" << std::hex << i);
			}
		return str;
	}

	size_t Keys() const {
		return DIK_KEYS;
	}
	size_t KeysDown() const {
		if (!enabled)
			return 0;
		size_t keysDown = 0;
		for (size_t i = 0; i < Keys(); ++i)
			if (KeyDown(i))
				++keysDown;
		return keysDown;
	}

	bool KeyDown(size_t dikKey) const {
		if (!enabled)
			return false;
		return (dikKey < Keys()) ? ((ks[dikKey] & DI_DOWN_BIT) != 0) : false;
	}

	bool KeyUp(size_t dikKey) const {
		if (!enabled)
			return true;
		return (dikKey < Keys()) ? !KeyDown(dikKey) : false;
	}

	bool Ctrl() const {
		return KeyDown(DIK_LCONTROL) || KeyDown(DIK_RCONTROL);
	}
	bool Shift() const {
		return KeyDown(DIK_LSHIFT) || KeyDown(DIK_RSHIFT);
	}
	bool Alt() const {
		return KeyDown(DIK_LALT) || KeyDown(DIK_RALT);
	}

	static int GetRepeatDelayMs() {
		int delay = 0;
		::SystemParametersInfo(SPI_GETKEYBOARDDELAY, 0, &delay, 0);
		return 250 * (1 + delay); // MSDN notes 250ms max delay time
	}

	static int GetRepeatRateHz() {
		int frequency = 0;
		::SystemParametersInfo(SPI_GETKEYBOARDSPEED, 0, &frequency, 0);
		return 1000 / (1 + frequency); // MSDN notes 1000hz max rate
	}
};

/// Mouse State Abstraction
struct MouseState {
	DIMOUSESTATE ms; // DirectInput Mouse State
	bool enabled; // drf - added - mouse enabled
	DInputDevice diDevice;

	MouseState() {
		Reset();
	}
	MouseState(const MouseState& _ms) {
		*this = _ms;
	}

	void Reset() {
		ZeroMemory(this, sizeof(*this));
		enabled = true;
	}

	void Enable(bool enable) {
		enabled = enable;
	}

	void DirectInputDevice(DInputDevice device) {
		diDevice = device;
		Update();
	}

	bool Update() {
		if (!diDevice || !enabled)
			return false;
		return (diDevice->GetDeviceState(sizeof(ms), (void*)&ms) == DI_OK);
	}

	std::string ToStr() const {
		std::string str;
		StrBuild(str, "MS<" << this << ">"
							<< " enabled=" << (enabled ? "YES" : "NO")
							<< " buttonsDown=" << ButtonsDown()
							<< (ButtonDownLeft() ? " LEFT" : "")
							<< (ButtonDownRight() ? " RIGHT" : "")
							<< (ButtonDownMiddle() ? " MIDDLE" : "")
							<< " delta=(" << DeltaX() << " " << DeltaY() << " " << DeltaZ() << ")");
		return str;
	}

	size_t Buttons() const {
		return DIM_BUTTONS;
	}
	size_t ButtonsDown() const {
		if (!enabled)
			return 0;
		size_t buttonsDown = 0;
		for (size_t i = 0; i < Buttons(); ++i)
			if (ButtonDown(i))
				++buttonsDown;
		return buttonsDown;
	}

	bool ButtonDown(size_t dimButton) const {
		if (!enabled)
			return false;
		return (dimButton < Buttons()) ? ((ms.rgbButtons[dimButton] & DI_DOWN_BIT) != 0) : false;
	}
	bool ButtonUp(size_t dimButton) const {
		if (!enabled)
			return true;
		return (dimButton < Buttons()) ? !ButtonDown(dimButton) : false;
	}

	bool ButtonUpLeft() const {
		return ButtonUp(DIM_BUTTON_LEFT);
	}
	bool ButtonDownLeft() const {
		return ButtonDown(DIM_BUTTON_LEFT);
	}

	bool ButtonUpRight() const {
		return ButtonUp(DIM_BUTTON_RIGHT);
	}
	bool ButtonDownRight() const {
		return ButtonDown(DIM_BUTTON_RIGHT);
	}

	bool ButtonUpMiddle() const {
		return ButtonUp(DIM_BUTTON_MIDDLE);
	}
	bool ButtonDownMiddle() const {
		return ButtonDown(DIM_BUTTON_MIDDLE);
	}

	// Returns delta position in 'ticks' since the last update, not absolute position!
	int DeltaX() const {
		return enabled ? ms.lX : 0;
	}
	int DeltaY() const {
		return enabled ? ms.lY : 0;
	}
	int DeltaZ() const {
		return enabled ? ms.lZ : 0;
	}
	int DeltaWheel() const {
		return DeltaZ();
	}
};
