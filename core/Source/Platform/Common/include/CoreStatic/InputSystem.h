///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "BooleanLogic.h"
#include "InputSystemTypes.h"
#include <stdint.h>
#include <functional>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <bitset>
#include "Core/Math/Vector.h"

namespace KEP {

// This object is passed to key event handlers registered with the input system.
// It is defined outside of the InputSystem object to allow forward declaration.
struct KeyEventHandlerArgs {
	class EventTypeMask {
	public:
		void Set(eActionHandlerEventType type, bool value = true) {
			m_Mask.set(size_t(type), value);
		}
		bool Test(eActionHandlerEventType type) const {
			return m_Mask.test(size_t(type));
		}

	private:
		std::bitset<size_t(eActionHandlerEventType::NumberOfTypes)> m_Mask;
	};
	// Type of event that triggered the call to the handler. (Begin,End,Sustain,Repeat)
	EventTypeMask m_EventTypes;

	// Duration that the state has been maintained since the last handler call.
	// Zero for eActionHandlerEventType::Begin
	double m_dDuration;

	// Time at which event occurs.
	double m_dEventTime;

	// Number of repeats since last handler call.
	int m_iRepeatCount;

	// Key whose state has changed to trigger the call to the handler.
	int m_iChangedKey;

	// How m_iChangedKey has changed.
	eKeyEventType m_KeyEventType;

	// To allow querying key states.
	std::function<eKeyStateType(int iKeyCode)> m_fnGetKeyState;

	Vector2i m_mouseMovement;
};

class InputSystem {
public:
	typedef std::function<void(const KeyEventHandlerArgs& args)> HandlerFunctionType;

	explicit InputSystem(eKeyCodeType keyCodeType);

	// Called on window system key events.
	void EnqueueKeyStateChange(double dRealTime, int keyCode, bool bDown);
	void EnqueueMouseButtonStateChange(double dRealTime, int keyCode, bool bDown, Vector2i point);
	void ResetKeys(double dTime);

	// Remove all pending key events from queue.
	void ClearInputQueue();

	// Called on initialization or regain of keyboard focus. Time is needed for events that depend on duration.
	// No event handlers are called for key state changes.
	void UpdateKeyStates(const std::vector<int>& aDepressedKeys, double dCurrentTime);

	// Called when losing focus to stop sending events for held-down keys.
	void CancelActiveHandlers(double dRealTime);

	// Called by client application to attach actions to key events.
	void RegisterHandler(const std::string& strActionName, HandlerFunctionType handlerFunction);

	// Defines what actions are performed for a key event.
	bool AddBinding(std::string strActionName, const BindableKeyEvent& keyEvent);

	// Parses stream and adds bindings. Only binds to existing handlers. Returns number of bindings added.
	size_t AddBindingsFromStream(std::istream& strm);

	// Call event handlers based on queued events. Calls handlers that remain active at dRealTime.
	void ProcessQueuedEventsUntil(double dRealTime);

	void SetCallbackForProcessingInput(std::function<void()> func) {
		m_fnOnQueuedEventsProcessed = func;
	}

	// Query current key state.
	eKeyStateType GetKeyState(int iKeyCode);

	// Get key code from name of key, based on configured key map.
	int GetKeyCode(const char* pszKeyName);

private:
	struct KeyStateChangeData;
	struct KeyBinding;
	//class EventHandler;

	// Builds a logic network based on a keystate combination. Connects pKeyBinding to the keys it depends on.
	std::shared_ptr<BooleanLogic::LogicComponent> BuildLogic(const KeyStateCombination& ksc, std::set<int>* pKeysUsed);

	// Gets the logic component that represents a key state.
	std::shared_ptr<BooleanLogic::LogicComponent> GetKeyLogic(int iKeyCode, eKeyStateType keyState);

	// Updates paChangedHandlers with the EventHandlers that have changed state in response to this key state change.
	//eKeyEventType HandleKeyStateChange(const KeyStateChangeData& kscd, double dEventTime, std::vector<EventHandler*>* paChangedHandlers=nullptr);
	eKeyEventType HandleKeyStateChange(const KeyStateChangeData& kscd, double dEventTime, std::vector<KeyBinding*>* papAffectedKeyBindings = nullptr);

	void ProcessActiveHandlers(double dRealTime);

private:
	struct KeyBinding {
		//bool IsEventBased() const { return !m_KeyEvents.empty(); }
		bool IsStateSatisfied() const {
			return m_pKeyStateLogic->GetLastCalculatedState();
		}
		void KeyChanged() {
			m_pKeyStateLogic->CalculateState();
		}

		std::shared_ptr<BooleanLogic::LogicComponent> m_pKeyStateLogic;
		std::set<KeyEvent> m_KeyEvents;
		std::string m_strActionName;
	};

	class EventHandler {
	public:
		EventHandler() :
				m_HandlerFunction(), m_dTimeActivated(0), m_dTimeDeactivated(0), m_dLastProcessTime(-DBL_MAX), m_bLastProcessedActiveState(false) {
		}
		void SetHandlerFunction(HandlerFunctionType handlerFunction) {
			m_HandlerFunction = std::move(handlerFunction);
		}
		void CancelHandlerFunction(double dTime) {
			KeyEventHandlerArgs args;
			args.m_dDuration = 0;
			args.m_dEventTime = dTime;
			args.m_EventTypes.Set(eActionHandlerEventType::Cancel);
			args.m_iChangedKey = -1;
			args.m_iRepeatCount = 0;
			args.m_KeyEventType = eKeyEventType::None;
			args.m_fnGetKeyState = nullptr;
			args.m_mouseMovement = Vector2i(0, 0);

			m_HandlerFunction(args);
			m_dLastProcessTime = dTime;
		}
		void Activate(double dTime) {
			m_dTimeActivated = dTime;
		}
		void Deactivate(double dTime) {
			m_dTimeDeactivated = dTime;
		}

		void UpdateMouseMovement(Vector2i movement) {
			m_vLastRegisteredMouseMovement = movement;
		}

		void CallHandlerFunction(eActionHandlerEventType eventType, double dTime, const KeyEvent& keyEvent, const std::function<eKeyStateType(int iKeyCode)>& fnGetKeyState) {
			KeyEventHandlerArgs args;
			args.m_EventTypes.Set(eventType);
			args.m_iRepeatCount = 0; // Repeat count not yet supported.

			if (eventType == eActionHandlerEventType::Sustain) {
				if (m_bLastProcessedActiveState == false)
					args.m_EventTypes.Set(eActionHandlerEventType::Restore);
			}

			if (eventType == eActionHandlerEventType::Deactivate || eventType == eActionHandlerEventType::Sustain) {
				//args.m_iRepeatCount = CalculateRepeatCount();
				//if( args.m_iRepeatCount > 0 )
				//	args.m_EventTypes.set(size_t(eActionHandlerEventType::Repeat));
			}
			if (eventType == eActionHandlerEventType::Deactivate) {
				args.m_EventTypes.Set(eActionHandlerEventType::Sustain);
			}

			args.m_dDuration = dTime - m_dLastProcessTime;
			args.m_dEventTime = dTime;
			args.m_iChangedKey = keyEvent.m_iKeyId;
			args.m_KeyEventType = keyEvent.m_KeyEventType;
			args.m_fnGetKeyState = fnGetKeyState;
			args.m_mouseMovement = m_vLastRegisteredMouseMovement;
			m_HandlerFunction(args);

			m_vLastRegisteredMouseMovement = Vector2i(0, 0);

			m_dLastProcessTime = dTime;
			if (eventType == eActionHandlerEventType::Deactivate)
				m_bLastProcessedActiveState = false;
			else if (eventType == eActionHandlerEventType::Activate)
				m_bLastProcessedActiveState = true;
		}

		void UpdateLastProcessTime(double dTime) {
			m_dLastProcessTime = dTime;
		}
		// A key binding tied to this handler (possibly) changed state.
		bool UpdateBindingState(KeyBinding* pKeyBinding) {
			bool bActive = pKeyBinding->IsStateSatisfied();
			if (bActive) {
				if (!m_ActiveBindings.insert(pKeyBinding).second)
					return false; // no change to key binding
				if (m_ActiveBindings.size() > 1)
					return false; // no change to state
			} else {
				if (m_ActiveBindings.erase(pKeyBinding) == 0)
					return false; // no change to key binding
				if (!m_ActiveBindings.empty())
					return false; // no change to state
			}

			return true;
		}
		// *this is Active if at least one keybinding is activating it.
		bool IsActive() const {
			return !m_ActiveBindings.empty();
		}
		// If all active key bindings only respond to events then no call is needed while keys are held.
		bool NeedsSustainCall() const {
			for (KeyBinding* pKeyBinding : m_ActiveBindings) {
				if (pKeyBinding->m_KeyEvents.empty())
					return true;
			}
			return false;
		}

	private:
		HandlerFunctionType m_HandlerFunction;

		Vector2i m_vLastRegisteredMouseMovement;
		bool m_bLastProcessedActiveState; // Whether handler was active the last time it was processed. Used to handle state changing when not focused.
		double m_dTimeActivated; // The time that this event was last activated.
		double m_dTimeDeactivated; // The time that this event was last deactivated.
		double m_dLastProcessTime; // The last time this event was processed. Used to calculate the duration for eActionHandlerEventType::Sustain events

		std::set<KeyBinding*> m_ActiveBindings; // The different key bindings activating this event.
	};

	typedef std::bitset<int(eKeyEventType::Count)> KeyEventMask;

	class KeyStateData {
	public:
		// Disallow copying.
		KeyStateData(const KeyStateData&) = delete;
		void operator=(const KeyStateData&) = delete;

		KeyStateData() :
				m_CurrentKeyState(eKeyStateType::Up), m_pKeyDownLogicVariable(std::make_shared<BooleanLogic::Variable>(false)), m_pKeyUpLogicVariable(std::make_shared<BooleanLogic::Variable>(true)) {
		}

		// Calls callback for keybindings activated by the new key state.
		eKeyEventType SetKeyState(const KeyStateChangeData& data, double dEventTime, std::vector<KeyBinding*>* papAffectedKeyBindings) {
			eKeyEventType keyEventType = eKeyEventType::None;
			eKeyStateType newKeyState = data.m_NewKeyState;
			if (newKeyState != m_CurrentKeyState || data.m_bIsMouseInput) {
				m_CurrentKeyState = newKeyState;
				m_dTimeEnteredState = dEventTime;
				m_pKeyDownLogicVariable->SetState(newKeyState == eKeyStateType::Down);
				m_pKeyUpLogicVariable->SetState(newKeyState == eKeyStateType::Up);
				if (papAffectedKeyBindings)
					papAffectedKeyBindings->insert(papAffectedKeyBindings->end(), m_aDependentKeyBindings.begin(), m_aDependentKeyBindings.end());
				if (newKeyState == eKeyStateType::Down)
					keyEventType = eKeyEventType::Press;
				else if (newKeyState == eKeyStateType::Up)
					keyEventType = eKeyEventType::Release;
			}
			return keyEventType;
		}
		eKeyStateType GetKeyState() const {
			return m_CurrentKeyState;
		}
		void AddDependentKeyBinding(KeyBinding* pKeyBinding) {
			m_aDependentKeyBindings.push_back(pKeyBinding);
		}

		const std::shared_ptr<BooleanLogic::Variable>& GetKeyDownLogicVariable() {
			return m_pKeyDownLogicVariable;
		}
		const std::shared_ptr<BooleanLogic::Variable>& GetKeyUpLogicVariable() {
			return m_pKeyUpLogicVariable;
		}

	private:
		eKeyStateType m_CurrentKeyState;
		double m_dTimeEnteredState; // The time that the current keystate was entered.

		// Key state logic can hook into this to determine satisfaction of their logic conditions.
		std::shared_ptr<BooleanLogic::Variable> m_pKeyDownLogicVariable;
		std::shared_ptr<BooleanLogic::Variable> m_pKeyUpLogicVariable;

		std::vector<KeyBinding*> m_aDependentKeyBindings; // Key bindings that are triggered by this key (possibly in combination with other key states).
	};

	struct KeyStateChangeData {
		int m_iKeyCode;
		eKeyStateType m_NewKeyState;
		Vector2i m_mouseMovement;
		bool m_bIsMouseInput;
	};

	// Member variables
private:
	const eKeyCodeType m_KeyCodeType; // Specifies how text strings are translated to integer values. (Windows, DirectInput, or some future defined key code)
	std::function<eKeyStateType(int iKeyCode)> m_fnGetKeyState; // Function object passed to handlers to allow them to determine other key states.
	double m_dPreviousProcessTime;
	std::multimap<double, KeyStateChangeData> m_KeyStateChangeQueue; // Queue of key state changes, ordered by time of key event.

	std::vector<std::unique_ptr<KeyBinding>> m_aKeyBindings;

	std::unordered_map<int, KeyStateData> m_KeyStateDataMap; // Current state of keys, indexed by key ID.
	std::unordered_map<std::string, EventHandler> m_EventHandlerMap; // Handlers associated with action names.
	std::unordered_set<EventHandler*> m_ActiveHandlers; // Handlers that need to be called based on state.

	std::function<void()> m_fnOnQueuedEventsProcessed;
};

} // namespace KEP
