///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CWorldChannelRules.h"
#include "afxwin.h"
// CWorldChannelDlg dialog
#include "KEPEditDialog.h"
#include "KEPImageButton.h"

#include "resource.h"
#include "CollapsibleGroup.h"
#include "WorldChannelPropertiesCtrl.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

class CWorldChannelDlg : public CKEPEditDialog {
	DECLARE_DYNAMIC(CWorldChannelDlg)

public:
	CWorldChannelDlg(CWorldChannelRuleList*& channelDatabase,
		CWnd* pParent = NULL); // standard constructor
	virtual ~CWorldChannelDlg();

	CWorldChannelRuleList*& channelDatabaseRef;
	// Dialog Data
	enum { IDD = IDD_COMBO_RULECHANNEL };

	CKEPImageButton m_btnAdd;
	CKEPImageButton m_btnDelete;
	CKEPImageButton m_btnReplace;

	CCollapsibleGroup m_colWorldChannel;
	CButton m_grpWorldChannel;
	CWorldChannelPropertiesCtrl m_propWorldChannel;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL OnInitDialog();
	//void InListRuleDatabase(int sel);
	void InList(int sel);
	CWordlChannelRuleObj* GetSelectedRule();
	void LoadSelectedRule();
	DECLARE_MESSAGE_MAP()

	CWordlChannelRuleObj* m_currentRuleObj;
	void ReplaceWorldChannel(int selection);

public:
	afx_msg void OnBnClickedButtonAddrule();
	afx_msg void OnBnClickedButtonDeleterule();
	afx_msg void OnBnClickedButtonReplacerule();
	//CComboBox m_channelRulesCombo;
	//int m_worldID;
	CListBox m_currentRulesList;
	afx_msg void OnLbnSelchangeListCurrentrulelist();
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnBnClickedBtnLoad();

private:
	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};
