///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include "CMaterialClass.h"

#define NO_TEXTURE_ASSIGNMENT -1

namespace KEP {

class SkeletonConfiguration;
class CMaterialObject;
class CMovementObjList;
class RuntimeSkeleton;
class IMemSizeGadget;

#pragma pack(push, 1)
enum MaterialColorType {
	FullMaterialColor = 0,
	SpecularMaterialColor,
	EmissiveMaterialColor,
	PlainMaterialColor,
	BareMaterialColor
};

enum TextureAssignmentType {
	FullTextureAssignment = 10,
	BlendedTextureAssignment,
	BareTextureAssignment
};

typedef struct _rgbMatColor {
	float r, g, b, a;
} RGBMatColor;

typedef struct _FULL_MATERIAL_COLOR {
	RGBMatColor diffuse;
	RGBMatColor ambient;
	RGBMatColor specular;
	float specularPower;
	RGBMatColor emissive;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
} FULLMATERIALCOLOR;

typedef struct _SPECULAR_MATERIAL_COLOR {
	RGBMatColor diffuse;
	RGBMatColor ambient;
	RGBMatColor specular;
	float specularPower;
} SPECULARMATERIALCOLOR;

typedef struct _EMISSIVE_MATERIAL_COLOR {
	RGBMatColor diffuse;
	RGBMatColor ambient;
	RGBMatColor emissive;
} EMISSIVEMATERIALCOLOR;

typedef struct _PLAIN_MATERIAL_COLOR {
	RGBMatColor diffuse;
	RGBMatColor ambient;
} PLAINMATERIALCOLOR;

typedef struct _BARE_MATERIAL_COLOR {
	RGBMatColor dambient;
} BAREMATERIALCOLOR;

typedef struct _FULL_TEXTURE_ASSIGNMENT {
	int filenameIndex;
	float textureMovementU;
	float textureMovementV;
	char blendMethod;
	char colorArg1;
	char colorArg2;
	D3DCOLOR textureColorKey;
} FULLTEXTUREASSIGNMENT;

typedef struct _BLENDED_TEXTURE_ASSIGNMENT {
	int filenameIndex;
	char blendMethod;
	char colorArg1;
	char colorArg2;
} BLENDEDTEXTUREASSIGNMENT;

typedef struct _BARE_TEXTURE_ASSIGNMENT {
	int filenameIndex;
} BARETEXTUREASSIGNMENT;

typedef struct _FULL_MATERIAL_FLAGS {
	int blendMode;
	int zBias;
	int sortOrder;
	int transition;
	int cycleTime;
	int diffuseEffect;
	int materialNameIndex;
	int textureTileType;
	long fadeMaxDist;
	long fadeVisDist;
	BOOL fadeByDistance;
	BOOL cullOn;
	BOOL forceWireFrame;
	BOOL mirrorEnabled;
	BOOL fogOverride;
	BOOL fogFilter;
	BOOL alphaDepthTest;
	// shader stuff
	int shaderIndex;
	short pixelShader;
	short vertexShader;
	int fxBindStringIndex;
	//int paramCount; // I don't know what this is let alone how to implement it.
	//*** paramBlock;

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
} FULLMATERIALFLAGS;

typedef struct _MATERIAL_ENTRY {
	int colorTableIndexUse;
	int colorTableIndexMod;
	int flagsIndex;
	int textureAssignments[NUM_CMATERIAL_TEXTURES];
} MATERIALENTRY;

typedef struct _COMPRESSION_HEADER {
	int fullColorCount;
	int specularColorCount;
	int emissiveColorCount;
	int plainColorCount;
	int bareColorCount;
	int fullAssignCount;
	int blendedAssignCount;
	int bareAssignCount;
	int flagsCount;
	int fileNameCount;
	int materialNameCount;
	int bindStringCount;
	int m_compressionTextureCounts[NUM_CMATERIAL_TEXTURES + 1];
} COMPRESSIONHEADER;

#pragma pack(pop)

class MaterialCompressor {
public:
	static const unsigned char material_archive_code = 108;

private:
	/*!
	 * MaterialCompressor objects can only be created (or deleted) through
	 * the static CompressorMap functions.
	 */
	MaterialCompressor();
	~MaterialCompressor();

	// Manage the material compressor map and MT access.
	class CompressorMap {
	public:
		CompressorMap() {
			InitializeCriticalSection(&m_cs);
		}
		~CompressorMap() {
			DeleteCriticalSection(&m_cs);
		}
		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

		typedef std::map<std::string, MaterialCompressor*> MatCompressorMap; // resource file name -> material compressor
		typedef std::pair<std::string, MaterialCompressor*> StringCompressorPair;

		CRITICAL_SECTION m_cs;
		MatCompressorMap m_matCompMap;
	};

	bool m_compressed;
	COMPRESSIONHEADER m_fileHeader;

	std::vector<FULLMATERIALCOLOR> m_colorTable;
	std::vector<CStringA*> m_fileNameTable;
	std::vector<CStringA*> m_materialNameTable;
	std::vector<CStringA*> m_fxBindStringTable;
	std::vector<FULLTEXTUREASSIGNMENT> m_assignmentTable;
	std::vector<FULLMATERIALFLAGS> m_flagsTable;
	std::vector<CMaterialObject*> m_legacyMaterials;
	std::vector<MATERIALENTRY> m_compressedMaterials;
	static CompressorMap m_compressorMap;

	bool m_v1Compatible; // set to true for v1 data compatibility, false otherwise.
	// NOTE: do not change this flag anywhere else than materialCompressionReadV1

	bool m_texFileNameIsConcrete; // See CMaterialObject::m_texFileNameIsConcrete

public:
	/*!
	 * \brief
	 * Create or retrieve a material compressor with the given name.  This allows
	 * multiple threads running various serialization functions to access the correct
	 * compressor, when all they know is the name of the archive they are working on.
	 *
	 * \see
	 * MaterialCompressor CMaterialObject
	 */
	static MaterialCompressor* getMaterialCompressor(const std::string& name);

	/*!
	 * Delete a named material compressor.
	 */
	static void deleteMaterialCompressor(const std::string& name);

	/*!
	 * \brief
	 * Adds a CMaterial for compression.
	 *
	 * \param legacyMat
	 * The legacy CMaterial to be compressed.
	 *
	 * \returns
	 * true on success false on failure.
	 *
	 * \throws <exception class>
	 * Description of criteria for throwing this exception.
	 *
	 * Before material compression all target CMaterials must be added by calling
	 * this function
	 *
	 * \remarks
	 * Write remarks for addMaterial here.
	 *
	 * \see
	 * Separate items with the '|' character.
	 */
	bool addMaterial(CMaterialObject* legacyMat);
	/*!
	 * \brief
	 * Adds all materials for compression from a movement obj list.
	 *
	 * \param movList
	 * the input movement obj list
	 *
	 * \returns
	 * true on success false on failure.
	 *
	 * \throws <exception class>
	 * Description of criteria for throwing this exception.
	 *
	 * Easier way to add a movement obj list rather than adding indivudial materials.
	 *
	 * \remarks
	 * Write remarks for addMaterials here.
	 *
	 * \see
	 * Separate items with the '|' character.
	 */
	bool addMaterials(CMovementObjList* movList);
	/*!
	 * \brief
	 * Add all materials on a skeleton
	 *
	 * \param skeleton
	 * the input skeleton
	 *
	 * \returns
	 * true on success false on failure
	 *
	 * \throws <exception class>
	 * Description of criteria for throwing this exception.
	 *
	 * Easy way to add the materials of a skeleton rather than one at a time
	 *
	 * \remarks
	 * Write remarks for addMaterials here.
	 *
	 * \see
	 * Separate items with the '|' character.
	 */
	bool addMaterials(const RuntimeSkeleton* skeleton, const SkeletonConfiguration* cfg);
	/*!
	 * \brief
	 * compute the necessary optimized tables for saving to archive.
	 *
	 * \throws <exception class>
	 * Description of criteria for throwing this exception.
	 *
	 * This sorts and compresses the data.  The algorithm is n^2 and takes a while for
	 * files with alot of materials
	 *
	 * \remarks
	 * Write remarks for compress here.
	 *
	 * \see
	 * Separate items with the '|' character.
	 */
	void compress();
	/*!
	 * \brief
	 * Saves a compressed material system
	 *
	 * \param ar
	 * The target CArchive.
	 *
	 * \throws <exception class>
	 * CArchiveException
	 *
	 * Write detailed description for writeToArchive here.
	 *
	 * \remarks
	 * Write remarks for writeToArchive here.
	 *
	 * \see
	 * Separate items with the '|' character.
	 */
	void writeToArchive(CArchive& ar);
	/*!
	 * \brief
	 * Reads a set of compressed materials from an archive
	 *
	 * \param ar
	 * The in CArchive
	 *
	 * \throws <exception class>
	 * CArchive Exception
	 *
	 * Reads in compressed materials
	 *
	 * \remarks
	 * Write remarks for readFromArchive here.
	 *
	 * \see
	 * Separate items with the '|' character.
	 */
	void readFromArchive(CArchive& ar);
	/*!
	 * \brief
	 * This function generates a uncompressed Runtime material from an index
	 *
	 * \param materialTableIndex
	 * The index describing the compressed material
	 *
	 * \returns
	 * A CMaterialObject that can be used by the render
	 *
	 * \throws <exception class>
	 * Description of criteria for throwing this exception.
	 *
	 * Write detailed description for getMaterial here.
	 *
	 * \remarks
	 * Write remarks for getMaterial here.
	 *
	 * \see
	 * Separate items with the '|' character.
	 */
	CMaterialObject* getMaterial(int materialTableIndex, BOOL bPreserveDisabledTextureLayers = FALSE);

	void setTexFileNameIsConcrete(bool val) { m_texFileNameIsConcrete = val; }

public:
	template <typename T>
	struct is_pointer {
		static const bool value = false;
	};
	template <typename T>
	struct is_pointer<T*> {
		static const bool value = true;
	};

	template <typename T>
	static int compare(const T& lhs, const T& rhs) {
		// Prevent this template from expanding into pointer comparisons
		static_assert(is_pointer<T>::value == false, "Cannot directly compare two pointers. Specialization is required.");
		if (lhs < rhs)
			return -1;
		if (lhs > rhs)
			return 1;
		return 0;
	}

	// Compare two arrays with range: [min, max)
	template <typename T>
	static int compare(const T& lhs, const T& rhs, size_t rangeMin, size_t rangeMax) {
		int res;
		for (size_t i = rangeMin; i < rangeMax; ++i) {
			res = compare(lhs[i], rhs[i]);
			if (res != 0)
				return res;
		}
		return 0;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	static int compare(const D3DCOLORVALUE& lhs, const D3DCOLORVALUE& rhs);
	static int compare(const D3DMATERIAL9& lhs, const D3DMATERIAL9& rhs);
	static int compare(const TXTUV& lhs, const TXTUV& rhs);
	static int compare(const CMaterialObject& lhs, const CMaterialObject& rhs);

private:
	// private conversion functions
	static void convertColorDown(const FULLMATERIALCOLOR* col1, SPECULARMATERIALCOLOR* col2);
	static void convertColorDown(const FULLMATERIALCOLOR* col1, EMISSIVEMATERIALCOLOR* col2);
	static void convertColorDown(const FULLMATERIALCOLOR* col1, PLAINMATERIALCOLOR* col2);
	static void convertColorDown(const FULLMATERIALCOLOR* col1, BAREMATERIALCOLOR* col2);
	static void convertColorUp(const SPECULARMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2);
	static void convertColorUp(const EMISSIVEMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2);
	static void convertColorUp(const PLAINMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2);
	static void convertColorUp(const BAREMATERIALCOLOR* col1, FULLMATERIALCOLOR* col2);
	static void convertAssignmentDown(const FULLTEXTUREASSIGNMENT* assign1, BLENDEDTEXTUREASSIGNMENT* assign2);
	static void convertAssignmentDown(const FULLTEXTUREASSIGNMENT* assign1, BARETEXTUREASSIGNMENT* assign2);
	static void convertAssignmentUp(const BLENDEDTEXTUREASSIGNMENT* assign1, FULLTEXTUREASSIGNMENT* assign2);
	static void convertAssignmentUp(const BARETEXTUREASSIGNMENT* assign1, FULLTEXTUREASSIGNMENT* assign2);
	// helpers to determine types
	static MaterialColorType getCompressionType(const FULLMATERIALCOLOR* color);
	static TextureAssignmentType getCompressionType(const FULLTEXTUREASSIGNMENT* texAssign);
	// helpers to convert CMaterialObject
	void getBaseColor(const CMaterialObject* legacyMat, FULLMATERIALCOLOR* data);
	void getModColor(const CMaterialObject* legacyMat, FULLMATERIALCOLOR* data);
	void getFlags(const CMaterialObject* legacyMat, FULLMATERIALFLAGS* data);
	void getTextureAssignment(int assignmentIndex, const CMaterialObject* legacyMat, FULLTEXTUREASSIGNMENT* data);
	// private helper functions
	static int alignAndCountCompressedMat(MATERIALENTRY* ent);
	int addColorNoDupes(FULLMATERIALCOLOR* color);
	int addFlagsNoDupes(FULLMATERIALFLAGS* flags);
	int addTexAssignNoDupes(FULLTEXTUREASSIGNMENT* texAssign);
	int addIndexedEntryNoDupes(MATERIALENTRY* matEntry, bool locked);
	void colorSort();
	void flagSort();
	void texAssignSort();
	void matEntrySort();
	void materialCompressionWriteV2(CArchive& ar);
	void materialCompressionReadV2(CArchive& ar);
	void materialCompressionReadV1(CArchive& ar);
	bool verifyCompression(); // unit test
};

} // namespace KEP
