///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

struct LINEEXG {
	float startX, startY, endX, endY, circV;
};

class CEX2DVectObj : public CObject, public GetSet {
	DECLARE_SERIAL(CEX2DVectObj)

public:
	CEX2DVectObj();

	LINEEXG* m_xyPointArray;
	WORD m_xyPointCount;
	float m_centerPointX;
	float m_centerPointY;
	float m_radius;

	void SafeDelete();
	BOOL GetCenterAndRadius();
	BOOL Clone(CEX2DVectObj** copy);
	BOOL DrawIt(HDC hdcLocal, float locX, float locY, float resizeX, float resizeY);
	BOOL LoadDxfFile(CStringA filename);
	BOOL Scale(float size);
	float MagnitudeSquared(float Mx1, float My1,
		float Mz1, float Mx2,
		float My2, float Mz2);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CEX2DVectObjList : public CObList {
public:
	CEX2DVectObjList(){};

	DECLARE_SERIAL(CEX2DVectObjList)
};

} // namespace KEP