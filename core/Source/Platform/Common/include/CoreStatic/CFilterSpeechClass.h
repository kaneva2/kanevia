///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class CFilterSpeechObj : public CObject, public GetSet {
	DECLARE_SERIAL(CFilterSpeechObj);

public:
	CFilterSpeechObj();
	CStringA m_playersName;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CFilterSpeechList : public CObList {
public:
	CFilterSpeechList(){};

	DECLARE_SERIAL(CFilterSpeechList);

	void SafeDelete();
	BOOL ShouldStatmentBeBlocked(CStringA statment);
	void AddName(CStringA name);
	void UnBlockName(CStringA name);
	BOOL IsNameAlreadyInList(CStringA name);
};

} // namespace KEP