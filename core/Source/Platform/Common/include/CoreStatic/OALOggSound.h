///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "OALSoundBuffer.h"
#include "Core/Util/HighPrecisionTime.h"
// Ogg/Vorbis Includes
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

typedef uint64_t FileSize; // from KEPHelpers.h

namespace KEP {

class OALSoundProxy;

class OALOggSound : public OALSoundBuffer {
public:
	OALOggSound();
	~OALOggSound();

	bool OpenOgg(OALSoundProxy* proxy, TimeMs& duration);

	bool OpenOgg(BYTE* data, FileSize dataSize, TimeMs& duration);

	void Release();

	bool Play(ALuint source);

	bool Stream(ALuint buffer);

	bool Update(ALuint source) {
		return true;
	}

	bool EmptyQueue(ALuint source) {
		return true;
	}

private:
	OggVorbis_File m_oggStream;
	vorbis_info* m_vorbisInfo;
	vorbis_comment* m_vorbisComment;

	std::vector<char> m_bufferData;
	ALuint m_bufferID; // The OpenAL sound buffer ID
	ALsizei m_freq;
	ALenum m_format; //internal format
};

} // namespace KEP
