///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/KEPUtil/DecomposeInstanceId.h"
#include "KEP/Util/KEPUtil.h"
#include "Core/Util/fast_mutex.h"
#include <vector>
#include <assert.h>

namespace KEP {

#ifndef REFACTOR_INSTANCEID
KEPUTIL_EXPORT LONG getMappedInstanceId(USHORT shortInstanceId);
KEPUTIL_EXPORT USHORT getShortInstanceId(LONG intInstanceId);
#endif

/*
Types of instances, from the doc:

Type	    Description
-----------+---------------------------------------------------------------------
Group	    An instance where only players in a specific group, identfied by the
				instance id will be able to enter.  This is the non-persistent group
Guild	    Similar to group, only this is the persistent group. (Clan)
Housing     By default only the player may enter this zone.  Others, perhaps in
				their group, guild, or friends list (TBD), may be able to enter.
				The instance id will be a number that can be mapped to a specific
				player.  This may be more like arena in that a list of users is
				associated with it.
Permanent	An instance where all players who go through the portal end up
				in the same instance
Arena	    An instance where only players signed up will automatically be portaled
Channel		An instance tied to a Channel
*/
const ULONG CI_NOT_INSTANCED = 0;
const ULONG CI_GROUP = 1; // maps 32 bit id
const ULONG CI_GUILD = 2; // maps 32 bit id
const ULONG CI_HOUSING = 3; // maps 32 bit id
const ULONG CI_PERMANENT = 4;
const ULONG CI_ARENA = 5;
const ULONG CI_CHANNEL = 6; // maps 32 bit id
// CAN'T BE > 0xF since only 4 bits!

const std::string InstanceIdTypes[] = {
	"NOT_INSTANCED",
	"GROUP",
	"GUILD",
	"HOUSING",
	"PERMANENT",
	"ARENA",
	"CHANNEL"
};

/***********************************************************
CLASS

	InstanceId

	base class to wrap the LONG that holds an id and instance id

DESCRIPTION
	mostly a bunch of helper methods

	NOTE: 6/06 changes to allow ULONG for instanceId.  It
	uses a vector on TLS to map a USHORT embeded in the
	instance id bit field to the ULONG that is the player's
	handle

***********************************************************/
class InstanceId {
public:
	/**
	 * Construct with a type, resource id, and instance id
	 */
	InstanceId(ULONG channelId, ULONG instanceId, ULONG type);

	/**
	 * Construct from a long value.
	 * @deprecated Use InstanceId(ULONG,ULONG,ULONG)
	 */
	InstanceId(LONG l) {
		fromLong(l);
	}

	/**
	 * Default constructor
	 */
	InstanceId() :
			m_channelId(0), m_instanceId(0), m_type(0) {}

	// Union Bit Masks
	static const ULONG MASK_CHANNEL_ID = 0x00000fff;
	static const ULONG MASK_INSTANCE_ID = 0x0ffff000;
	static const ULONG MASK_TYPE = 0xf0000000;

	// max possible values for packing into 32 bits
	static const ULONG MAX_ZONE_INDEX = 0xfff;
	static const ULONG MAX_CHANNEL_ID = 0xfff;
	static const ULONG MAX_TYPE_VALUE = 0xf;
	static const ULONG MAX_INSTANCE_ID = 0xffff; // for non-mapped instanced

	/** DRF
	* InstanceId as 32-bit unsigned long union m_ulong { m_type:4 | m_instanceId:16 | m_channelId:12 }
	*/
	ULONG GetULong() const {
#ifndef REFACTOR_INSTANCEID
		return m_ulong;
#else
		ULONG ul = ((m_type << 28) & MASK_TYPE) | ((m_instanceId << 12) & MASK_INSTANCE_ID) | (m_channelId & MASK_CHANNEL_ID);
		return ul;
#endif
	}

	/** DRF
	* InstanceId.m_type as 4-bit enumerated zone type.
	*/
	ULONG GetType() const {
		return m_type;
	}

	/** DRF
	* InstanceId.m_instanceId as mapped 16-bit integer via IntInstanceIdMap<int>
	*/
	LONG GetInstanceId() const;

	/** DRF
	* InstanceId.m_channelId as 12-bit unsigned long.
	*/
	ULONG GetChannelId() const {
		return m_channelId;
	}
	ULONG channelId() const {
		return m_channelId;
	}

	/**
	 * Determine if the resource component is equal.  This is silly as on can simply use
	 * a.channelId() == b.channelId(), but has been pull up to minimize the footprint of
	 * obsolesce ChannelId refactoring.
	 *
	 * @param		rhs		const reference to InstanceId to be compare this with.
	 * @returns		true if resource ids are equal, else false.
	 * @deprecated	Use innate comparision, e.g. a.channelId() == b.channelId();
	 */
	bool channelIdEquals(const InstanceId& rhs) const {
		return m_channelId == rhs.m_channelId;
	}

	/**
	 * Comparison operator
	 */
	bool operator==(const InstanceId& rhs) const {
		return m_channelId == rhs.m_channelId && m_instanceId == rhs.m_instanceId && m_type == rhs.m_type;
	}

	/**
	 * Negated comparison operator
	 */
	bool operator!=(const InstanceId& rhs) const {
		return !operator==(rhs);
	}

	/**
	 * Assignment operator
	 */
	InstanceId& operator=(const InstanceId& rhs) {
		copy(rhs);
		return *this;
	}

	/** DRF
	* InstanceId.(m_type | m_channelId) as 32-bit unsigned long.
	* This is the format of what is stored as 'zone_index' in wok.dynamic_objects and other
	* database tables.
	*/
	ULONG GetTypeAndChannelId() const {
		return GetULong() & (MASK_TYPE | MASK_CHANNEL_ID);
	}

	// is this of a type that can be instanced
	bool isInstanceType() const {
		return m_type != CI_NOT_INSTANCED;
	}

	// is it a valid instance
	bool isInstanced() const {
		return m_instanceId != 0 && m_type != CI_NOT_INSTANCED;
	}

	bool isPermanent() const {
		return m_type == CI_PERMANENT;
	}
	bool isArena() const {
		return m_type == CI_ARENA;
	}
	bool isGroup() const {
		return m_type == CI_GROUP;
	}
	bool isHousing() const {
		return m_type == CI_HOUSING;
	}
	bool isGuild() const {
		return m_type == CI_GUILD;
	}
	bool isChannel() const {
		return m_type == CI_CHANNEL;
	}

	// mutators
	void clear() {
		m_channelId = 0;
		m_instanceId = 0;
		m_type = 0;
	}

	// TODO: Obsolesce these.  Usage should be
	// iid.setType(<type>).setInstance();
	// Makes for a smaller, cleaner API and doesn't limit us in the future
	// should we add a new type.
	// Also, it should be noted that these methods are only relevant to ZoneIndex.
	// We should either obsolesce ZoneIndex or push these methods down to ZoneIndex.
	//
	// these take int as instance Id, that is mapped to a USHORT
	void setAsArena(USHORT instanceId) {
		m_type = CI_ARENA;
		m_instanceId = instanceId;
	}
	void setAsGroup(int instanceId);
	void setAsHousing(int instanceId);
	void setAsGuild(int instanceId);
	void setAsChannel(int instanceId);
	void setAsPermanent(int instanceId);

	/**
	 * @deprecated. We should no longer be referencing a InstanceId as
	 * an unsigned long.
	 */
	long toLong() const {
		return static_cast<long>(GetULong());
	}
#ifndef REFACTOR_INSTANCEID
	/**
	 * @deprecated. We should no longer be referencing a InstanceId as
	 * an unsigned long.
	 */
	operator long() const {
		return toLong();
	}
#else
	bool operator<(const InstanceId& rhs) const {
		if (m_type < rhs.m_type)
			return true;
		if (m_type > rhs.m_type)
			return false;
		if (m_instanceId < rhs.m_instanceId)
			return true;
		if (m_instanceId > rhs.m_instanceId)
			return false;
		if (m_channelId < rhs.m_channelId)
			return true;
		if (m_channelId > rhs.m_channelId)
			return false;
		return false;

		// Alternatively - though with significantly more overhead
		//
		// return std::tie(_p1,_p2,_p3) < std::tie(rhs._p1,rhs._p2,rhs._p3);
	}

#endif

	InstanceId& fromLong(long i) {
		ULONG ul = static_cast<ULONG>(i);
		m_type = ((ul & MASK_TYPE) >> 28);
		m_instanceId = ((ul & MASK_INSTANCE_ID) >> 12);
		m_channelId = (ul & MASK_CHANNEL_ID);
		return *this;
	}

	void setInstanceAndTypeFrom(const InstanceId& src) {
		m_type = src.m_type;
		m_instanceId = src.m_instanceId;
	}

	void setInstanceAndTypeFrom(LONG currentChannel) {
		setInstanceAndTypeFrom(InstanceId(currentChannel));
	}

private:
	inline bool hasMappedInstanceId() const; // is the instance id a LONG?

public:
	inline long getClearedMappedInstanceId() const; // deprecated?
	inline long getClearedInstanceId() const;

	// use at your own risk
	inline InstanceId& setInstanceId(LONG instanceId);
	void clearInstanceAndType() {
		m_type = 0;
		m_instanceId = 0;
	}
	void clearInstance() {
		m_instanceId = 0;
	}

	/** DRF
	* Returns the enumerated zone type as a string.
	*/
	std::string GetTypeStr() const {
		return InstanceIdTypes[GetType()];
	}

	std::string ToStr(bool verbose = true) const {
		if (verbose) {
			return std::string("ZI<") + std::to_string(GetULong()) + ":" + std::to_string(GetChannelId()) + "." + std::to_string(GetInstanceId()) + "." + GetTypeStr() + ">";
		} else {
			return std::string("ZI<") + std::to_string(GetULong()) + ">";
		}
	}

protected:
#ifndef REFACTOR_INSTANCEID
	union {
		ULONG m_ulong; // 32 bits
		struct {
			ULONG m_channelId : 12; // MSB ---- ---- ---- ---- ---- XXXX XXXX XXXX LSB
			ULONG m_instanceId : 16; // MSB ---- XXXX XXXX XXXX XXXX ---- ---- ---- LSB
			ULONG m_type : 4; // MSB XXXX ---- ---- ---- ---- ---- ---- ---- LSB
		};
	};
#else
	ULONG m_channelId;
	ULONG m_instanceId;
	ULONG m_type;
#endif

	void copy(const InstanceId& src) {
		if (this != &src) {
#ifndef REFACTOR_INSTANCEID
			m_ulong = src.m_ulong;
#else
			m_channelId = src.m_channelId;
			m_instanceId = src.m_instanceId;
			m_type = src.m_type;
#endif
		}
	}

	inline void setIntInstanceId(int instanceId);

private:
#ifndef REFACTOR_INSTANCEID
#else
	operator unsigned long() const {
		return toLong();
	}
	operator long() const {
		return toLong();
	}
#endif
};

/***********************************************************
CLASS

	ChannelId

	class to wrap the LONG that holds channel and instance id

DESCRIPTION
	mostly a bunch of helper methods

***********************************************************/
typedef InstanceId ChannelId;

// methods for serializing the class out and in, we use int since that is what
// was used before for the world/channel ids
#ifdef _AFXDLL
inline CArchive& AFXAPI operator>>(CArchive& ar, ChannelId& w) {
	int i;
	ar >> i;
	w.fromLong(i);
	return ar;
}
inline CArchive& AFXAPI operator<<(CArchive& ar, const ChannelId& w) {
	ar << (int)w.toLong();
	return ar;
}
#endif

static const ChannelId INVALID_CHANNEL_ID(InstanceId::MAX_CHANNEL_ID, 0, 0);

/***********************************************************
CLASS

	ZoneIndex

	class to wrap the LONG that holds zone index and instance id

DESCRIPTION
	mostly a bunch of helper methods

***********************************************************/
class ZoneIndex : public InstanceId {
public:
	ZoneIndex(ULONG ZoneIndex, ULONG instanceId, ULONG type) :
			InstanceId(ZoneIndex, instanceId, type) {}
	ZoneIndex() {}
	ZoneIndex(LONG l) {
		fromLong(l);
	}

	ZoneIndex(const ZoneIndex& src) {
		operator=(src);
	}

	ZoneIndex& operator=(const ZoneIndex& src) {
		copy(src);
		return *this;
	}

	// comparisons
	bool ZoneIndexEquals(const ZoneIndex& w) const {
		return m_channelId == w.m_channelId;
	}
	bool ZoneIndexEquals(int i) const {
		return m_channelId == (ULONG)i;
	}
	bool operator==(const ZoneIndex& w) const {
		return ZoneIndexEquals(w) && m_instanceId == w.m_instanceId && m_type == w.m_type;
	}
	bool operator!=(const ZoneIndex& w) const {
		return !operator==(w);
	}

	// get methods
	LONG zoneIndex() const {
		return m_channelId;
	}

	// set methods
	void zoneIndex(ULONG i) {
		clear();
		m_channelId = i;
	}

	// like from long, but only sets zoneIndex, rest is cleared
	void fromZoneIndex(long i) {
		clear();
		m_channelId = i;
	}

	// Return the ZoneIndex value with dynamically generated instance ID (as oppose to the ones from DB) removed
	ZoneIndex GetBaseZoneIndex() const {
		ZoneIndex retIndex(*this);

		if (retIndex.isPermanent() || retIndex.isArena()) {
			retIndex.setInstanceId(1);
		} else {
			retIndex.setInstanceId(GetInstanceId());
		}

		return retIndex;
	}
};

// methods for serializing the class out and in, we use int since that is what
// was used before for the world/channel ids
#ifdef _AFXDLL
inline CArchive& AFXAPI operator>>(CArchive& ar, ZoneIndex& w) {
	int i;
	ar >> i;
	w.fromLong(i);
	return ar;
}
inline CArchive& AFXAPI operator<<(CArchive& ar, const ZoneIndex& w) {
	ar << (int)w.toLong();
	return ar;
}
#endif

static const ZoneIndex INVALID_ZONE_INDEX(InstanceId::MAX_ZONE_INDEX, 0, 0);

inline bool InstanceId::hasMappedInstanceId() const {
#ifdef _ClientOutput
	return false;
#else
	return m_type == CI_GUILD || m_type == CI_HOUSING || m_type == CI_GROUP || m_type == CI_CHANNEL || m_type == CI_PERMANENT;
#endif
}

inline LONG InstanceId::GetInstanceId() const {
#ifndef REFACTOR_INSTANCEID
	if (hasMappedInstanceId()) {
		if (m_instanceId == 0) {
			return 0; // not instanced, "template" type case
		}

		return getMappedInstanceId((USHORT)m_instanceId);
	} else
#else
#endif
	{
		return m_instanceId;
	}
}

inline InstanceId::InstanceId(ULONG channelId, ULONG instanceId, ULONG type) :
		m_channelId(channelId), m_instanceId(instanceId), m_type(type) {
	// Note that aside from this assertion, channelId is not used, i.e.
	// it is never used instantiate this instance.
	//
	assert(channelId <= MAX_CHANNEL_ID);
	assert(type <= MAX_TYPE_VALUE);

	switch (type) {
		case CI_HOUSING:
			setAsHousing(instanceId);
			break;

		case CI_GUILD:
			setAsGuild(instanceId);

		case CI_GROUP:
			setAsHousing(instanceId);
			break;

		case CI_CHANNEL:
			setAsChannel(instanceId);
			break;

		case CI_PERMANENT:
			setAsPermanent(instanceId);
			break;

		default:
			break;
	}
}

inline void InstanceId::setAsGroup(int instanceId) {
	m_type = CI_GROUP;

	setIntInstanceId(instanceId);
}

inline void InstanceId::setAsHousing(int instanceId) {
	m_type = CI_HOUSING;

	setIntInstanceId(instanceId);
}

inline void InstanceId::setAsGuild(int instanceId) {
	m_type = CI_GUILD;

	setIntInstanceId(instanceId);
}

inline void InstanceId::setAsChannel(int instanceId) {
	m_type = CI_CHANNEL;

	setIntInstanceId(instanceId);
}

inline void InstanceId::setAsPermanent(int instanceId) {
	m_type = CI_PERMANENT;

	setIntInstanceId(instanceId);
}

// private helper for mapped instanceIds
inline void InstanceId::setIntInstanceId(int intInstanceId) {
#ifndef REFACTOR_INSTANCEID
	m_instanceId = getShortInstanceId(intInstanceId);
#else
	m_instanceId = intInstanceId;
#endif
}

// deprecated
inline long InstanceId::getClearedMappedInstanceId() const { // returns copy
#ifndef REFACTOR_INSTANCEID
	InstanceId ret(*this);
	if (ret.hasMappedInstanceId())
		ret.clearInstance();

	return ret.toLong();
#else
	return getClearedInstanceId();
#endif
}

// This is the zone_index in the dynamic_objects table
inline long InstanceId::getClearedInstanceId() const { // returns copy
	InstanceId ret(*this);
	ret.clearInstance();
	return ret.toLong();
}

inline InstanceId& InstanceId::setInstanceId(LONG instanceId) {
	ULONG myType = GetType();
	switch (myType) {
		case CI_PERMANENT:
			setAsPermanent((int)instanceId);
			break;

		case CI_ARENA:
			setAsArena((USHORT)instanceId);
			break;

		case CI_GROUP:
			setAsGroup((int)instanceId);
			break;

		case CI_HOUSING:
			setAsHousing((int)instanceId);
			break;

		case CI_GUILD:
			setAsGuild((int)instanceId);
			break;

		case CI_CHANNEL:
			setAsChannel((int)instanceId);
			break;

		default: // not instanced do nothing
			assert(!isInstanced());
			break;
	}
	return *this;
}

} // namespace KEP
