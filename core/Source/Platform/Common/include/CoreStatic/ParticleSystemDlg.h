/******************************************************************************
 ParticleSystemDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_PARTICLE_SYSTEM_DLG_H_)
#define _PARTICLE_SYSTEM_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParticleSystemDlg.h : header file
//

#include <afxext.h>
#include "resource.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"

////////////////////////////////////////////
// CParticleSystemDialog control

class CParticleSystemDialog : public CDialogBar {
	DECLARE_DYNAMIC(CParticleSystemDialog)

public:
	CParticleSystemDialog();
	virtual ~CParticleSystemDialog();

	// Dialog Data
	//{{AFX_DATA(CParticleSystemDialog)
	enum { IDD = IDD_DIALOG_PARTICLESYSTEM };
	//}}AFX_DATA

protected:
	// Generated message map functions
	//{{AFX_MSG(CParticleSystemDialog)
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
	LRESULT OnInitDialog(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(WPARAM wParam, LPARAM lParam);

private:
	CCollapsibleGroup m_colParticleSystems;
	CButton m_grpParticleSystems;

	CKEPImageButton m_btnAddParticleSystem;
	CKEPImageButton m_btnDeleteParticleSystem;
	CKEPImageButton m_btnParticleSystemProperties;
	CKEPImageButton m_btnSaveParticleSystemUnit;
	CKEPImageButton m_btnLoadParticleSystemUnit;

	CKEPImageButton m_btnTestParticleSystem;
	CKEPImageButton m_btnStopParticleSystem;
};

#endif !defined(_PARTICLE_SYSTEM_DLG_H_)