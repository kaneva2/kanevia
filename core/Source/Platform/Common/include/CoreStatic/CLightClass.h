///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"

namespace KEP {

class CFrameObj;
class CLightTrackerObjList;

class CLightObj : public CObject, public GetSet {
	CLightObj() {}

	DECLARE_SERIAL(CLightObj);

public:
	CLightObj(
		BOOL L_linearTypeAtten,
		BOOL L_ficker,
		int L_flickerOnRange,
		float L_atenuationAdjustVarial,
		float L_attenuationVarial,
		int L_ocilatingVarial,
		float L_colorRed,
		float L_colorGreen,
		float L_colorBlue,
		float L_attenuation,
		float L_atenuationAdjust,
		BOOL L_ocilating,
		int L_ocilatingSteps,
		int L_delay,
		int L_delayVarial,
		BOOL L_on,
		//LPDIRECT3DRMLIGHT L_light,
		CFrameObj* L_lightFrame,
		float L_range);

	D3DLIGHT8 m_imStruct;
	int m_imLight;
	float m_range;
	BOOL m_linearTypeAtten;
	int m_flickerOnRange;
	BOOL m_ficker;
	float m_atenuationAdjustVarial;
	float m_attenuationVarial;
	int m_ocilatingVarial;
	CFrameObj* m_lightFrame;
	//LPDIRECT3DRMLIGHT m_light;
	float m_colorRed;
	float m_colorGreen;
	float m_colorBlue;
	float m_attenuation;
	float m_atenuationAdjust;
	BOOL m_ocilating;
	int m_ocilatingSteps;
	int m_delay;
	int m_delayVarial;
	BOOL m_on;

	void SetLightPositionForRender(LPDIRECT3DDEVICE8 device, Vector3f offset);
	void Serialize(CArchive& ar);
	void SafeDelete(LPDIRECT3DDEVICE8 device,
		CLightTrackerObjList* lightTrackList);

	DECLARE_GETSET
};

class CLightObjList : public CObList {
public:
	CLightObjList(){};

	DECLARE_SERIAL(CLightObjList)
};

} // namespace KEP