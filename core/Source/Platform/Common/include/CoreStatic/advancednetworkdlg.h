///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "afxwin.h"
#if !defined(AFX_ADVANCEDNETWORKDLG_H__BBBAE6C1_8EFB_11D3_B7BC_0000B4B0CE0A__INCLUDED_)
#define AFX_ADVANCEDNETWORKDLG_H__BBBAE6C1_8EFB_11D3_B7BC_0000B4B0CE0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AdvancedNetworkDlg.h : header file
//
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "GetSetPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CAdvancedNetworkDlg dialog

class CAdvancedNetworkDlg : public CKEPEditDialog {
	// Construction
public:
	CAdvancedNetworkDlg(GetSet* networkCfg, CWnd* pParent = NULL); // standard constructor
	// Dialog Data
	//{{AFX_DATA(CAdvancedNetworkDlg)
	enum { IDD = IDD_DIALOG_ADVANCEDNETWORK };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdvancedNetworkDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAdvancedNetworkDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();

private:
	CCollapsibleGroup m_colNetwork;
	CButton m_grpNetwork;
	CGetSetPropertiesCtrl m_propNetwork;
	GetSet* m_network;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADVANCEDNETWORKDLG_H__BBBAE6C1_8EFB_11D3_B7BC_0000B4B0CE0A__INCLUDED_)
