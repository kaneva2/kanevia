/******************************************************************************
 WaterZonePropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_WATER_ZONEPROPERTIESCTRL_H_)
#define _WATER_ZONEPROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaterZonePropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"
#include "CEXMeshClass.h"
#include "direct.h"

/////////////////////////////////////////////////////////////////////////////
// CWaterZonePropertiesCtrl window

class CWaterZonePropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CWaterZonePropertiesCtrl();
	virtual ~CWaterZonePropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	CStringA GetWaterZoneName() const {
		return Utf16ToUtf8(m_waterZoneName).c_str();
	}
	BOOL GetIsExclusionZone() const {
		return m_isExclusionZone;
	}

	// modifiers
	void SetWaterZoneName(CStringA waterZoneName) {
		m_waterZoneName = Utf8ToUtf16(waterZoneName).c_str();
	}
	void SetIsExclusionZone(BOOL isExclusionZone) {
		m_isExclusionZone = isExclusionZone;
	}

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWaterZonePropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CWaterZonePropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	enum { INDEX_ROOT = 1,
		INDEX_WATER_ZONE_NAME,
		INDEX_EXCLUSION_ZONE };

	CStringW m_waterZoneName;
	BOOL m_isExclusionZone;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_WATER_ZONEPROPERTIESCTRL_H_)
