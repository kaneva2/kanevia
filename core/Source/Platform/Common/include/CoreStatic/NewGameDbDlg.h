#pragma once

#include "KepPropertyPage.h"
#include "resource.h"
class CGameData;
// CNewGameDb dialog

class CNewGameDbDlg : public CKEPPropertyPage {
public:
	CNewGameDbDlg(CGameData*& selectedGame, const char* editorBaseDir, bool skipExtraEditorDlgs, Logger& logger); // standard constructor

	virtual ~CNewGameDbDlg();
	// Overrides

	// Dialog Data
	enum { IDD = IDD_NEW_GAME_DB };

	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();
	virtual LRESULT OnWizardBack();
	virtual BOOL OnInitDialog();
	virtual bool TestDbConnection();
	virtual BOOL CheckDB(bool useStatusWindow = false);

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedTestDb();
	CGameData*& m_selectedGame; // game selected in prev wizard so we can tell if GStar or full STAR

	CStringA GetDBServerUtf8() const {
		return Utf16ToUtf8(m_dbServer).c_str();
	}
	CStringA GetDBUserUtf8() const {
		return Utf16ToUtf8(m_dbUser).c_str();
	}
	CStringA GetDBPasswordUtf8() const {
		return Utf16ToUtf8(m_dbPassword).c_str();
	}
	CStringA GetDBNameUtf8() const {
		return Utf16ToUtf8(m_dbName).c_str();
	}
	int GetDBPort() const {
		return m_dbPort;
	}

private:
	CStringW m_dbServer,
		m_dbUser,
		m_dbPassword,
		m_dbName;
	int m_dbPort;

private:
	Logger m_logger;
	CStringA m_editorBaseDir;
	bool m_skipExtraEditorDlgs;
};
