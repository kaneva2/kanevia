///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "MoveData.h"

#include "include\Core\Math\Rotation.h"

namespace KEP {

MOVE_FIELDS MOVE_DATA::FieldsDelta(const MOVE_DATA& rhs) const {
	BYTE fields = MOVE_FIELDS::NONE;
	fields |= (pos == rhs.pos) ? 0 : MOVE_FIELDS::POS;
	Vector3<short> rot, rhsRot;
	ConvertRotationZXY(dir, up, out(rot));
	ConvertRotationZXY(rhs.dir, rhs.up, out(rhsRot));
	fields |= (rot == rhsRot) ? 0 : MOVE_FIELDS::ROT;
	fields |= (animGlid1 == rhs.animGlid1) ? 0 : MOVE_FIELDS::ANIM1;
	fields |= (animGlid2 == rhs.animGlid2) ? 0 : MOVE_FIELDS::ANIM2;
	return (MOVE_FIELDS)fields;
}

template <class T>
void MOVE_DATA::Encode(std::vector<BYTE>& msg, T data) const {
	msg.insert(msg.end(), (BYTE*)&data, (BYTE*)(1 + &data));
}

void MOVE_DATA::Encode(const MOVE_DATA& moveData, std::vector<BYTE>& msg) const {
	MOVE_FIELDS fields = FieldsDelta(moveData);
	Encode(msg, netTraceId);
	Encode(msg, fields);
	if (fields & MOVE_FIELDS::POS)
		Encode(msg, pos);
	if (fields & MOVE_FIELDS::ROT) {
		Vector3<short> rot;
		ConvertRotationZXY(dir, up, out(rot));
		Encode(msg, rot);
	}
	if (fields & MOVE_FIELDS::ANIM1)
		Encode(msg, animGlid1);
	if (fields & MOVE_FIELDS::ANIM2)
		Encode(msg, animGlid2);
}

template <class T>
void MOVE_DATA::Decode(BYTE*& pData, T& data) {
	data = *(T*)pData;
	pData += sizeof(T);
}

void MOVE_DATA::Decode(BYTE*& pData) {
	if (!pData)
		return;
	MOVE_FIELDS fields;
	Decode(pData, netTraceId);
	Decode(pData, fields);
	if (fields & MOVE_FIELDS::POS)
		Decode(pData, pos);
	if (fields & MOVE_FIELDS::ROT) {
		Vector3<short> rot;
		Decode(pData, rot);
		ConvertRotationZXY(rot, out(dir), out(up));
	}
	if (fields & MOVE_FIELDS::ANIM1)
		Decode(pData, animGlid1);
	if (fields & MOVE_FIELDS::ANIM2)
		Decode(pData, animGlid2);
}

} // namespace KEP