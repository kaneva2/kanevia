/******************************************************************************
 SelectCameraDlg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "resource.h"

// SelectCamera dialog

class CSelectCameraDialog : public CDialog {
	DECLARE_DYNAMIC(CSelectCameraDialog)

public:
	CSelectCameraDialog(CWnd* pParent = NULL); // standard constructor
	virtual ~CSelectCameraDialog();

	// Dialog Data
	enum { IDD = IDD_SELECT_CAMERA };

	// accessors
	int GetCameraIndex() const {
		return m_camera_index;
	}
	CStringA GetCameraName() const {
		return m_camera_name;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	virtual void OnOK();
	virtual BOOL OnInitDialog();

	void InitCameras();

private:
	CListBox m_cameras;

	int m_camera_index;
	CStringA m_camera_name;
};
