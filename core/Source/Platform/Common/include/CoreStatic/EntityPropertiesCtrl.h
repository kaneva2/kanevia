#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EntityPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CEntityPropertiesCtrl window

class CEntityPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CEntityPropertiesCtrl(int entityID);
	virtual ~CEntityPropertiesCtrl();

public:
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual BOOL OnPropertyChanged(GWCDeepIterator iter);
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	void Populate();

	// accessors
	int GetEntityIndex() const {
		return m_entityIndex;
	}
	int GetEnergy() const {
		return m_energy;
	}
	float GetPopoutRadius() const {
		return m_popoutRadius;
	}
	float GetMeshRadius() const {
		return m_meshRadius;
	}
	int GetModelType() const {
		return m_modelType;
	}
	int GetHudIndex() const {
		return m_hudIndex;
	}
	int GetMountBoneIndex() const {
		return m_mountBoneIndex;
	}
	int GetTeam() const {
		return m_team;
	}
	CStringA GetEntityName() const {
		return Utf16ToUtf8(m_sEntityName).c_str();
	}
	int GetRaceID() const {
		return m_raceID;
	}
	int GetSvrPhysics() const {
		return m_svrPhysics;
	}
	float GetHardPointOverd() const {
		return m_hardPointOverd;
	}
	bool GetDisableCollision() const {
		return m_bDisableCollision;
	}

	// modifiers
	void SetEntityIndex(int index) {
		m_entityIndex = index;
	}
	void SetEnergy(int energy) {
		m_energy = energy;
	}
	void SetPopoutRadius(float popoutRadius) {
		m_popoutRadius = popoutRadius;
	}
	void SetMeshRadius(float meshRadius) {
		m_meshRadius = meshRadius;
	}
	void SetModelType(int modelType) {
		m_modelType = modelType;
	}
	void SetHudIndex(int hudIndex) {
		m_hudIndex = hudIndex;
	}
	void SetMountBoneIndex(int mountBoneIndex) {
		m_mountBoneIndex = mountBoneIndex;
	}
	void SetTeam(int team) {
		m_team = team;
	}
	void SetEntityName(CStringA sEntityName) {
		m_sEntityName = Utf8ToUtf16(sEntityName).c_str();
	}
	void SetRaceID(int raceID) {
		m_raceID = raceID;
	}
	void SetSvrPhysics(int svrPhysics) {
		m_svrPhysics = svrPhysics;
	}
	void SetHardPointOverd(float hardPointOverd) {
		m_hardPointOverd = hardPointOverd;
	}
	void SetDisableCollision(bool bDisable) {
		m_bDisableCollision = bDisable;
	}

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEntityPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CEntityPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum { INDEX_ROOT = 1,
		INDEX_ENERGY,
		INDEX_POPOUT_RADIUS,
		INDEX_MESH_RADIUS,
		INDEX_MODEL_TYPE,
		INDEX_HUD_INDEX,
		INDEX_MOUNT_BONE_INDEX,
		INDEX_TEAM,
		INDEX_ENTITY_NAME,
		INDEX_RACE_ID,
		INDEX_SVR_PHYSICS,
		INDEX_HARD_POINT_OVERD,
		INDEX_DISABLE_COLLISION
	};

public:
	enum { MODEL_TYPE_NORMAL = 0,
		MODEL_TYPE_MACHINE_MOD_1 = 1,
		MODEL_TYPE_WEAPON = 5,
		MODEL_TYPE_DISABLED = 99
	} ModelType;

protected:
	static const int NUM_ITEMS_MIN = 0;
	static const int NUM_ITEMS_MAX = 7;

	void InitModelTypes();
	void InitHUDs();
	void InitBones();

private:
	int m_entityIndex;
	int m_energy;
	float m_popoutRadius;
	float m_meshRadius;
	int m_modelType;
	int m_hudIndex;
	int m_mountBoneIndex;
	int m_team;
	CStringW m_sEntityName;
	int m_raceID;
	int m_svrPhysics;
	float m_hardPointOverd;
	bool m_bDisableCollision;
};
