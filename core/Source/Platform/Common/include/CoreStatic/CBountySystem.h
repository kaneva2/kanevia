///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "CStructuresMisl.h"

namespace KEP {

class CBountyObj : public CObject, public GetSet {
	DECLARE_SERIAL(CBountyObj);

public:
	CBountyObj();

	CStringA m_playerName;
	int m_bountyAmount;
	PLAYER_HANDLE m_playerHandle;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CBountyObjList : public CObList {
public:
	CBountyObjList(){};
	void SafeDelete();

	int AddBounty(const CStringA& playerName,
		int bountyAmount,
		PLAYER_HANDLE playerHandle,
		int maxBountyCount);
	void RemoveBountyByHandle(PLAYER_HANDLE mHandle);

	DECLARE_SERIAL(CBountyObjList);
};

} // namespace KEP