///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_STATISTICSDBDLG_H__9A3FA9E3_B547_4C71_BBB5_C18A45258F30__INCLUDED_)
#define AFX_STATISTICSDBDLG_H__9A3FA9E3_B547_4C71_BBB5_C18A45258F30__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StatisticsDBDlg.h : header file
//
#include "CStatClass.h"
#include "KEPImageButton.h"
#include "KEPEditDialog.h"
#include "resource.h"
#include "CollapsibleGroup.h"
#include "StatisticPropertiesCtrl.h"
#include "EditorState.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CStatisticsDBDlg dialog

class CStatisticsDBDlg : public CKEPEditDialog {
	// Construction
public:
	CStatisticsDBDlg(CStatObjList*& statisticDB,
		CWnd* pParent = NULL); // standard constructor
	CStatObjList*& statisticDBREF;

	// Dialog Data
	//{{AFX_DATA(CStatisticsDBDlg)
	enum { IDD = IDD_DIALOG_STATISTICSDB };
	CListBox m_statisticList;
	//}}AFX_DATA

	CKEPImageButton m_btnAdd;
	CKEPImageButton m_btnDelete;
	CKEPImageButton m_btnReplace;
	CKEPImageButton m_btnLoad;
	CKEPImageButton m_btnSave;

	CCollapsibleGroup m_colStatistics;
	CButton m_grpStatistics;
	CStatisticPropertiesCtrl m_propStatistic;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStatisticsDBDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	CStatObj* m_selectedStatisticObj;
	//void InListStatisticals(int sel);
	void LoadStatisticProperties();
	CStatObj* GetSelectedStatisticObj();
	CStatObj* GetStatisticObj(int index);
	void InList(int sel);
	void InListBenefitTypeList(int sel);
	// Generated message map functions
	//{{AFX_MSG(CStatisticsDBDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	afx_msg void OnBUTTONReplace();
	virtual void OnOK();
	afx_msg void OnSelchangeLISTStatistics();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonSave();
	afx_msg BOOL DestroyWindow();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void ReplaceStat(int selection);

private:
	//last selected index in the list box
	int m_previousSel;

protected:
	BOOL SaveGameFile(CStringA path);
	BOOL SaveDialog(CStringA path);
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATISTICSDBDLG_H__9A3FA9E3_B547_4C71_BBB5_C18A45258F30__INCLUDED_)
