///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "InstanceId.h"
#include "matrixarb.h"
#include "KEPObList.h"

namespace KEP {

class CBankObj : public CObject, public GetSet {
	DECLARE_SERIAL(CBankObj);

public:
	CBankObj();

	CStringA m_bankName;
	ChannelId m_channel;
	Vector3f m_boundingBoxMax;
	Vector3f m_boundingBoxMin;
	int m_menuBind;
	BOOL m_willDealWithCriminals;

	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CBankObjList : public CKEPObList {
public:
	CBankObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CBankObjList);

	CBankObj* GetBankByPosition(Vector3f position, const ChannelId& channel);
	BOOL BoundBoxPointCheck(Vector3f& point, Vector3f& min, Vector3f& max);
};

} // namespace KEP