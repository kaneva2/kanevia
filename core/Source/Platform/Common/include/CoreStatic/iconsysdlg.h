///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICONSYSDLG_H__C017D2E9_DBB4_4205_9FD6_3ADAFD747E77__INCLUDED_)
#define AFX_ICONSYSDLG_H__C017D2E9_DBB4_4205_9FD6_3ADAFD747E77__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IconSysDlg.h : header file
//
#include "MaterialDlg.h"
#include "CIconSystemClass.h"
#include "TextureDatabase.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "IconPropertiesCtrl.h"
#include "IconTexturePropertiesCtrl.h"
#include "EditorState.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

/////////////////////////////////////////////////////////////////////////////
// CIconSysDlg dialog

class CIconSysDlg : public CKEPEditDialog {
	// Construction
public:
	CIconSysDlg(CIconSysList*& iconDatabase, CWnd* pParent = NULL); // standard constructor

	CIconSysList*& m_iconDatabaseREF;
	TextureDatabase* m_texturedatabaseREF;
	// Dialog Data
	//{{AFX_DATA(CIconSysDlg)
	enum { IDD = IDD_DIALOG_ICONSYS };
	CListBox m_iconList;
	//}}AFX_DATA

	//CComboBox	m_textureDatabaseCombo;
	//float	m_gridDensity;
	//CStringA	m_iconName;
	//float	m_iconSize;
	//float	m_xIndex;
	//float	m_yIndex;

	CCollapsibleGroup m_colIcon;
	CButton m_grpIcon;
	CIconTexturePropertiesCtrl m_propIconTexture;
	CIconPropertiesCtrl m_propIcon;

	CKEPImageButton m_btnAdd;
	CKEPImageButton m_btnDelete;
	CKEPImageButton m_btnReplace;
	CKEPImageButton m_btnMaterial;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIconSysDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void InListIcons(int sel);
	CStringA ConvToString(int number);
	CIconSysObj* GetSelectedIcon();
	CIconSysObj* GetIcon(int index);
	void LoadSelectedIcon();
	// Generated message map functions
	//{{AFX_MSG(CIconSysDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBUTTONAccessMaterial();
	afx_msg void OnBUTTONAddIcon();
	afx_msg void OnBUTTONDeleteIcon();
	afx_msg void OnBUTTONReplaceIcon();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonSave();
	afx_msg void OnSelchangeLISTIconDatabase();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void ReplaceIcon(int selection);

	CIconSysObj* m_currentIconSysObj;

private:
	//last selected index in the list box
	int m_previousSel;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	BOOL SaveDialog(CStringA fileName = "");
	void UpdateScript();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICONSYSDLG_H__C017D2E9_DBB4_4205_9FD6_3ADAFD747E77__INCLUDED_)
