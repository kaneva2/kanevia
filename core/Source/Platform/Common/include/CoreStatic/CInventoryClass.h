///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "GetSet.h"
#include "CItemClass.h"
#include "InventoryType.h"
#include "CStructuresMisl.h"

namespace KEP {

class IMemSizeGadget;
class CStatObjList;

// dirty flags for inventory items
const int INV_NEW = 0x1;
const int INV_QTY_UPDATED = 0x2;
const int INV_ARMED_UPDATED = 0x4;

// Forward declare for collection
class CInventoryObjList;

class CInventoryObj : public CObject, public GetSet {
	DECLARE_SERIAL(CInventoryObj);

	static int MAX_STACK_LIMIT;

public:
	CInventoryObj();

	virtual ~CInventoryObj();
	CInventoryObj(const CInventoryObj& rhs);
	CInventoryObj& operator=(const CInventoryObj& rhs);

	void Clone(CInventoryObj** copy);

	// points to a global inventory item.
	const CItemObj* m_itemData;

	BOOL m_optimizedSave;

	//visual rep needs to be added
	void Serialize(CArchive& ar);
	void SerializeAlloc(CArchive& ar);
	void SafeDelete();

	short inventoryType() const {
		return m_inventoryType;
	}

	void setInventoryType(short invType) {
		m_inventoryType = invType;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

	DECLARE_GETSET

	// getset overrides
	bool getInnerXml(std::string& xml, gsMetaDatum& md, void* value, char* workingString);
	void getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s);

public:
	// accessors/mutators added to also set dirty flags to optimize db saves 7/07
	void setQty(int i) {
		m_quantity = i;
		m_dirty |= INV_QTY_UPDATED;
	}

	// same as setQty but doesn't have the side effect of marking the item dirty.
	CInventoryObj& setQtyRaw(int i) {
		m_quantity = i;
		return *this;
	}

	// Same as setArmed but doesn't have side effect of marking the item dirty.
	CInventoryObj& setArmedRaw(BOOL armed = TRUE) {
		m_armed = armed;
		return *this;
	}

	//Same as setCharge but doesn't have the side effect of marking the item dirty.
	CInventoryObj& setChargeRaw(int i) {
		m_chargeCurrent = i;
		return *this;
	}

	//  Acquire a string representation of this CInventoryObject suitable for reporting.
	std::string toString();

	// Increment the quantity by the supplied amount.  Limits to MAX_STACK_LIMIT  @param i  The amount by which to increment the quantity
	void incQty(int i = 1) {
		m_quantity += i;
		if (m_quantity > MAX_STACK_LIMIT) {
			m_quantity = MAX_STACK_LIMIT;
		}
		m_dirty |= INV_QTY_UPDATED;
	}

	void decQty(int i = 1) {
		m_quantity -= i;
		m_dirty |= INV_QTY_UPDATED;
	}

	int getQty() const {
		return m_quantity;
	}

	bool isArmed() const {
		return (m_armed == TRUE);
	}

	void setArmed(bool armed = true) {
		m_armed = armed;
		m_dirty |= INV_ARMED_UPDATED;
	}

	int dirtyFlags() const {
		return m_dirty;
	}

	void clearDirty() {
		m_dirty = 0;
	}

	int getCharge() const {
		return m_chargeCurrent;
	}
	void setCharge(int i) {
		m_chargeCurrent = i;
		m_dirty |= INV_QTY_UPDATED;
	}
	void incCharge(int i = 1) {
		m_chargeCurrent += i;
		m_dirty |= INV_QTY_UPDATED;
	}
	void decCharge(int i = 1) {
		m_chargeCurrent -= i;
		m_dirty |= INV_QTY_UPDATED;
	}

private:
	int m_dirty;
	int m_quantity;
	BOOL m_armed; //quantity must be 1 for armed
	short m_inventoryType; // see IT_* above, DB only, not serialized
	int m_chargeCurrent; // current charges, m_chargeCurrent on m_itemData is only the default
};

class CInventoryObjList : public CObList {
public:
	CInventoryObjList();

	DECLARE_SERIAL(CInventoryObjList);

	void Serialize(CArchive& ar);

	void stringToFileWithName(const std::string& fname) {
		CStdioFile file;
		file.Open(Utf8ToUtf16(fname).c_str(), CFile::modeWrite | CFile::modeCreate);
		std::string str = toString();
		file.Write(str.c_str(), str.length());
		file.Close();
	}

	int m_wieghtLimit;
	int m_totalWieght;
	int m_itemCountLimit;
	int m_itemTotal;
	int m_lastArmedWeapon;
	int m_commitCount; // number of times this has been committed to db
	bool m_pendingRefresh;

	void Clone(CInventoryObjList** copy);
	void CloneMinimum(CInventoryObjList** copy);

	std::string toString();

	int AddInventoryItem(int quantity, bool armed, const CItemObj* itemData, short inventoryType = IT_NORMAL, bool markDirty = true);

	/**
	* Attempts to find an inventory record for the item supplied and update that record.
	* If it fails to find an inventory record, a new one is added.
	*
	* This method adds a single inventory item to the inventory list.
	* It is intended to be an atomic for adding/updating an item to/in the
	* list that makes no assertions as to whether or not the item
	* can be aggregated into a single inventory record (i.e. stackable).
	* It is incumbent upon callers of this method to enforce item stacking.
	*
	* @param quantity		The quantity of of "items" to be added to the inventory.
	* @param armed			Flag indicating whether or not the item is armed.
	* @param itemData		The item to be added to the inventory.
	* @param inventoryType	Default to IT_NORMAL
	* @param markDirty      In the event that an existing item is updated,
	*                       it's internal state is marked to "dirty".  This
	*                       flag can be set to false in order to update the
	*                       inventory item and not flag the item as "dirty".
	* @return               A pointer to the CInventoryObj updated/added.
	*                       null in the event of a quantity/stackable mismatch.
	*/
	CInventoryObj* AddOneInventoryItem(int quantity, bool armed, const CItemObj* itemData, short inventoryType = IT_NORMAL, bool markDirty = true);

	// Same functionality as above, however, accepts a preconstructed CInventoryObj instance.
	CInventoryObj* AddOneInventoryItem(CInventoryObj&);

	BOOL DeleteByGLID(int glid, int quantity = 1, bool mustBeUnarmed = false, short invTypes = IT_NORMAL, bool markDirty = true);

	// remove from this and add otherList
	BOOL TransferInventoryObjsTo(CInventoryObjList* otherList, int glid, int quantity = 1, bool mustBeUnarmed = false, short invTypes = IT_NORMAL, bool markDirty = true);

	CInventoryObj* VerifyInventory(int glid);

	CInventoryObj* GetByGLID(int glid, short invTypes = IT_GIFT | IT_NORMAL);

	int GetArmourRating(int damageChannel, BOOL forVisualOnly, int attackSpecified);

	BOOL GenerateMsgBlockItemToClient(BOOL includeArmed, MSG_BLOCK_ITEM_TO_CLIENT** ppMsgBlockItem, int& itemCount, int& msgSize, BOOL filterNonTransferable, int useTypeId = -1); //set userTypeId to -1 to return all items

	BOOL IsItemOfValuePresent();

	int GetInvenCountFilterUnseeable();
	BOOL DoesArmedLocationIDExist(int id);
	CInventoryObj* GetArmedInvenItemByLocationID(int id);
	BOOL UseIfKeyInInventory(int keyID);
	BOOL IsAnythingInInventoryValuable();
	void SetOptimizedSave(BOOL optimized);
	BOOL IsExpItemObjectPresent(int glid, int expID);
	int CombineToExpObj(int glid_combineItem, int glid_targetObject);

	bool CanTakeObjects(int maxCount, int itemCountToAdd);

	int GetTotalValue();
	CInventoryObj* VerifyInventory(int glid, int quantity);
	CInventoryObj* CanTradeItem(int glid, int quantity);
	void SerializeAlloc(CArchive& ar, int loadCount);
	CInventoryObj* GetInvenItemByLocationID(int id);
	CInventoryObj* GetInvenItemByLocationIDArmable(int id, CStatObjList& stats);
	BOOL ReorderInventory(int objectToMoveIndex, int targetIndex, int filterType);

	void SafeDelete();

	void DeleteInvItemAndRemoveAt(CInventoryObj* item, POSITION pos) {
		RemoveAt(pos);
		DeleteInvItem(item, item->getQty());
	}

	// clears all dirty flags and empties the deletedItems list
	void ClearDirty();
	bool IsDirty() const;

	const CInventoryObjList* DeletedItems() const {
		return m_deletedItems;
	}

	//Used to remove the item instantly instead of waiting for a lazy update as in DeleteByGLID
	//This will NOT update the database
	BOOL RemoveRecentAddByGlid(int glid, int quantity = 1, bool mustBeUnarmed = false, short invTypes = IT_NORMAL);

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	// for DB, track deleted items
	CInventoryObjList* m_deletedItems;
	void DeleteInvItem(CInventoryObj* item, int qty);
};

} // namespace KEP