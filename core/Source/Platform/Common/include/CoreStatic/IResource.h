#pragma once

#include "common\include\KEPHelpers.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Glids.h"
#include "DownloadPriority.h"
#include <Core/Util/WindowsAPIWrappers.h>

#define LOAD_RESOURCE_BY_DISTANCE			// Enable resource download priority by distance from camera or player

#define RES_DL_PRIO_DEFAULT DL_PRIO_MEDIUM

enum class eTexturePath : int {
	MapsModels = 0, // ...\<star>\MapsModels
	CustomTexture = 1, // ...\<star>\CustomTexture
	NumPaths
};

enum class eAnimPath : int {
	CharacterAnimations = 0, // ...\<star>\GameFiles\CharacterAnimations
	NumPaths
};

enum class eMeshPath : int {
	CharacterMeshes = 0, // ...\<star>\GameFiles\CharacterMeshes
	NumPaths
};

#define NUMBER_OF_DOL_PATHS	1
#define NUMBER_OF_SOUND_PATHS 1
#define NUMBER_OF_EQP_PATHS	1
#define RESOURCE_HASH_LENGTH 32

#define RM_THREAD_JS_PRIORITY (jsThdPriority)PR_NORMAL

#define MD5_DIGEST_LEN 16

namespace KEP {
class IResource;
class ResourceManager;
enum class ResourceType;
class DownloadPriorityDistanceModel;
class IMemSizeGadget;

enum class eCreateResourceResult {
	SUCCESS,
	FAIL,
	RETRY
};

class IResource {
	static std::string mBaseTexturePath[eTexturePath::NumPaths];
	static std::string mBaseAnimPath[eAnimPath::NumPaths];
	static std::string mBaseMeshPath[eMeshPath::NumPaths];

public:

	static std::string mBaseDolPath[NUMBER_OF_DOL_PATHS];
	static std::string mBaseSoundPath[NUMBER_OF_SOUND_PATHS];
	static std::string mBaseEqpPath[NUMBER_OF_EQP_PATHS];

	static void SetBaseTexturePath(eTexturePath texPath, const std::string& filePath) {
		mBaseTexturePath[(int) texPath] = filePath;
	}
	static std::string BaseTexturePath(eTexturePath texPath) {
		return mBaseTexturePath[(int) texPath];
	}

	static void SetBaseAnimPath(eAnimPath animPath, const std::string& filePath) {
		mBaseAnimPath[(int) animPath] = filePath;
	}
	static std::string BaseAnimPath(eAnimPath animPath) {
		return mBaseAnimPath[(int) animPath];
	}

	static void SetBaseMeshPath(eMeshPath meshPath, const std::string& filePath) {
		mBaseMeshPath[(int) meshPath] = filePath;
	}
	static std::string BaseMeshPath(eMeshPath meshPath) {
		return mBaseMeshPath[(int) meshPath];
	}

	static const DownloadPriorityDistanceModel& GetDownloadPriorityModel() {
		return ms_priorityModel;
	}

	/// Resource States
	enum State {
		NOT_INITIALIZED,
		INITIALIZED_NOT_LOADED,
		LOADED_AND_CREATED,
		ERRORED
	};

	/// Resource Errors
	enum Error {
		ERROR_NONE,
		NO_METADATA,
		DOWNLOAD_FAILURE,
		READ_FAILURE,
		CREATE_FAILURE,
		LOAD_ERROR,
	};

	virtual ~IResource() {}

	virtual std::string ToStr() const {
		std::string str;
		StrBuild(str, "pRes<" << this << ":" << GetGlid() << " '" << GetFilePath() << "'>");
		return str;
	}

	virtual bool CreateResource() = 0;
	virtual eCreateResourceResult CreateResourceEx() {
		return CreateResource() ? eCreateResourceResult::SUCCESS : eCreateResourceResult::FAIL;
	}

	virtual BYTE* GetSerializedData(FileSize& dataLen) {
		dataLen = 0;
		return nullptr;
	}

	virtual bool IsSerializedDataCompressible() const = 0;

	ResourceManager* GetResourceManager() const {
		return m_pRM;
	}

	std::string GetFilePath() const {
		return m_filePath;
	}

	std::string GetFileName() const;

	std::string GetURL() const {
		return m_url;
	}

	void SetHashKey(UINT64 newHash) {
		m_hashKey = newHash;
	}
	UINT64 GetHashKey() const {
		return m_hashKey;
	}

	void SetError(Error err) {
		m_status.currError = err;
	}
	Error GetError() const {
		return m_status.currError;
	}

	void SetState(State newState) {
		m_status.currState = newState;
	}
	State GetState() const {
		return m_status.currState;
	}
	bool StateIsNotInitialized() const {
		return GetState() == State::NOT_INITIALIZED;
	}
	bool StateIsInitializedNotLoaded() const {
		return GetState() == State::INITIALIZED_NOT_LOADED;
	}
	bool StateIsLoadedAndCreated() const {
		return GetState() == State::LOADED_AND_CREATED;
	}
	bool StateIsErrored() const {
		return GetState() == State::ERRORED;
	}
	bool NeedsLoad() const {
		return !StateIsLoadedAndCreated() && !StateIsErrored();
	}
	bool LoadStarted() const {
		return m_ResourceManagerData.LoadStarted();
	}

	void SetStateErrored() {
		SetState(State::ERRORED);
	}
	void SetStateErrored(Error error) {
		SetError(error);
		SetState(State::ERRORED);
	}
	bool IsErrored() const {
		return StateIsErrored() || (GetError() != Error::ERROR_NONE);
	}

	// ED-8437 - Clear Errored Resources On Rezone
	bool ClearErrored() {
		if (!IsErrored())
			return false;
		if( !m_ResourceManagerData.TryUnload() )
			return false;
		m_status.ClearErrored();
		m_verificationLastHash = "";
		m_verificationFailures = 0;
		return true;
	}

	void SetNameHash(DWORD nameHash) {
		m_status.nameHash = nameHash;
	}
	DWORD GetNameHash() const {
		return m_status.nameHash;
	}

	void SetDownloadPriority(int prio) {
		m_priority = prio;
	}
	int GetDownloadPriority() const;

	// InitCamDistance is useful for setting a reasonable default value before engine has a chance to update.
	// Because engine updates the distance on interval, if the distance is not probably inited, the resource
	// might get incorrectly pushed to the front of the download queue for a brief moment. There would be a
	// small chance for it to get assigned to a download thread before other (real) high priority ones.
	void InitCamDistance(double dist) {
		if (!m_camDistInited) 
			SetCamDistance(dist);
	}

	void SetCamDistance(double dist) {
		m_camDistInited = true;
		m_camDistance = dist;
	}
	double GetCamDistance() const {
		return m_camDistance;
	}

	virtual unsigned GetPriorityRangeSelector() const {
		return 0; // See DownloadPriorityDistanceModel
	}

	void SetPreload(bool preload) {
		m_preload = preload;
	}
	bool IsPreload() const {
		return m_preload;
	}

	void SetExpectedSize(FileSize size) {
		m_expectedSize = size;
	}
	FileSize GetExpectedSize() const {
		return m_expectedSize;
	}

	void SetExpectedHash(const std::string& hash) {
		m_expectedHash = hash;
	}
	std::string GetExpectedHash() {
		return m_expectedHash;
	}

	void SetGlid(const GLID& glid) {
		m_glid = glid;
	}
	GLID GetGlid() const {
		return m_glid;
	}

	void IncVerificationFailures() {
		++m_verificationFailures;
	}
	int GetVerificationFailures() {
		return m_verificationFailures;
	}

	void SetVerificationLastHash(const std::string& hash) {
		m_verificationLastHash = hash;
	}
	std::string GetVerificationLastHash() {
		return m_verificationLastHash;
	}

	ResourceType GetResourceType() const {
		return m_resourceType;
	}

	bool IsURLAvailable() const {
		return !GetURL().empty();
	}
	bool IsLocalResource() const {
		return m_isLocal;
	}

	bool IsKRXEncrypted() const {
		return m_krxEncrypted;
	}
	void SetKRXEncrypted(bool encrypted) {
		m_krxEncrypted = encrypted;
	}

	virtual unsigned char* GetEncryptionHashOverride(int& hash_len);

	void FreeRawBuffer();

	void SetRawBuffer(std::unique_ptr<BYTE[]> rawBuffer, FileSize rawSize);
	BYTE* GetRawBuffer() const {
		return m_pRawBuffer.get();
	}

	FileSize GetRawSize() const {
		return m_rawBufferSize;
	}

	void SetURL(const std::string& url) {
		m_url = url;
	}

protected:
	IResource(ResourceManager* library, UINT64 hashKey, bool isLocal = false);
	IResource(const IResource& res);

	void SetFilePath(const std::string& filePath) {
		m_filePath = filePath;
	}

private:

	struct Status {
		State currState;
		Error currError;
		DWORD nameHash;

		Status() :
			currState(State::NOT_INITIALIZED),
			currError(Error::ERROR_NONE),
			nameHash(0) {}

		bool ClearErrored() {
			if (currState != State::ERRORED)
				return false;
			currState = State::INITIALIZED_NOT_LOADED;
			currError = Error::ERROR_NONE;
			return true;
		}
	} m_status;

	ResourceManager* m_pRM;

	ResourceType m_resourceType;

	GLID m_glid; // resource glid

	std::string m_filePath; // resource full file path

	std::string m_url; // resource download url

	FileSize m_expectedSize; // the expected file size of a streamed asset
	std::string m_expectedHash; // the expected file hash of a streamed asset

	bool m_isLocal;	// true if local resource (not from patch server or UGC server)

	bool m_preload; // true if requested by preload functions

	int m_priority; // higher number is higher priority
	double m_camDistance;	// Shortest distance between the camera and any 3D entity that uses this resource. Applies to all 3D entities including those outside view frustum.
	bool m_camDistInited;	// Some resource can be shared by multiple owners. Use this flag to ensure InitCamDistance function only gets called once per resource when the distance is propagated from the owner.

	std::unique_ptr<BYTE[]> m_pRawBuffer;
	FileSize m_rawBufferSize;

	unsigned __int64 m_hashKey; // resource unique hash

	int m_verificationFailures; // the number of verification failures (prevent infinite loop)
	std::string m_verificationLastHash; // the last verification failure hash

	bool m_krxEncrypted; // true if KRX encrypted
	bool m_aesEncrypted; // true if AES encrypted (for backward compatibility, can be removed once all asset files are converted)
	unsigned char m_krxHash[MD5_DIGEST_LEN];

	static DownloadPriorityDistanceModel ms_priorityModel;

public:

	// In all states except NOT_QUEUED and COMPLETE, the resource loader has control over the object.
	// The resource loader will only start loading objects in the NOT_QUEUED state and will set their
	// state to COMPLETE when it finishes with them (either successfully or with an error).
	// Users will need to call TryUnload() before trying to load the object again. TryUnload()
	// will fail if the object is still in the process loading.
	enum class eQueuedState : uint32_t {
		NOT_QUEUED,
		//QUEUED_FOR_METADATA, // Async metadata call made immediately. No queue used.
		ADDING, // Entering the queue
		WAITING_METADATA,

		IN_PREPROCESS_QUEUE, // Waiting to determine whether to download or read.
		PREPROCESSING, // Determining whether download or read is necessary.
		IN_DOWNLOAD_QUEUE, // Waiting to start downloading. Prioritized.
		WAITING_DOWNLOAD, // Download in progress.
		PROCESSING_DOWNLOAD, // Received download. Processing it.
		IN_READ_QUEUE, // Waiting to start reading from disk. Prioritized.
		WAITING_READ, // Waiting for read data.
		PROCESSING_READ, // Read completed. Processing it.
		IN_CREATE_QUEUE, // Waiting to be created. Prioritized.
		PROCESSING_CREATE, // Creation in progress
		PROCESSING_ERROR, // Processing an error
		COMPLETE,

		NUMBER_OF_STATES
	};

	class ResourceManagerData {
		friend class ResourceLoader;
		friend class ResourceManager;
		std::atomic<eQueuedState> m_QueuedState = eQueuedState::NOT_QUEUED;
		bool ChangeState(eQueuedState fromState, eQueuedState toState) {
			return m_QueuedState.compare_exchange_strong(fromState, toState, std::memory_order_acq_rel);
		}

		void SetState(eQueuedState state) {
			m_QueuedState.store(state, std::memory_order_release);
		}

		eQueuedState GetState() const {
			return m_QueuedState.load(std::memory_order_acquire);
		}

		bool IsLoading() {
			eQueuedState state = GetState();
			return state != eQueuedState::NOT_QUEUED && state != eQueuedState::COMPLETE;
		}

	public:
		bool TryUnload() {
			return ChangeState(eQueuedState::COMPLETE, eQueuedState::NOT_QUEUED);
		}
		bool LoadStarted() const {
			return GetState() != eQueuedState::NOT_QUEUED;
		}
	} m_ResourceManagerData;
};

template<typename T>
class TResource : public IResource {
public:
	const static ResourceType _ResType = T::_ResType;

	TResource(ResourceManager* library, UINT64 hashKey, bool isLocal = false) : IResource(library, hashKey, isLocal) {}
	virtual T* GetAsset() = 0;
	virtual void SetAsset(T*) = 0;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;
};

} // namespace KEP