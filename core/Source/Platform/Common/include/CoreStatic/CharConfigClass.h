///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Glids.h"
#include <string>
#include <assert.h>

#ifdef _AFX
class CArchive;
#endif

namespace KEP {

class IEvent;
class IMemSizeGadget;

// CharConfig Item Enumeration
enum {
	CC_ITEM_INVALID = -1,
	CC_ITEM_BASE = 0, // character body item
	CC_ITEM_HEAD = 1, // character head item
	CC_ITEM_FIRST_EQUIP = CC_ITEM_HEAD, // First (and also the last, at this moment) equippable item
};

const size_t CC_SLOTS_MAX = 255;

struct CharConfigItemSlot {
	SHORT m_meshId; // The index of the mesh into alternate configurations
	USHORT m_matlId; // The index material into supported materials
	float m_r; // red value for custom colors
	float m_g; // green value for custom colors
	float m_b; // blue value for custom colors

	CharConfigItemSlot(SHORT meshId = 0, USHORT matlId = 0, float r = -1.0f, float g = -1.0f, float b = -1.0f) :
			m_meshId(meshId),
			m_matlId(matlId),
			m_r(r),
			m_g(g),
			m_b(b) {
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

class CharConfigItem {
	typedef std::vector<CharConfigItemSlot> SlotVector;

public:
	CharConfigItem(GLID_OR_BASE glidOrBase = GOB_BASE_ITEM, size_t numSlots = 0) :
			m_glidOrBase(glidOrBase),
			m_glidAnim(0) {
		m_slots.resize(numSlots);
	}

	CharConfigItem(GLID_OR_BASE glidOrBase, std::initializer_list<CharConfigItemSlot> slots) :
			// For Daryl's CharConfigStatic.cpp
			m_glidOrBase(glidOrBase),
			m_glidAnim(0),
			m_slots(slots) {
	}

	CharConfigItem(CharConfigItem&&) = default;
	CharConfigItem(const CharConfigItem&) = default;
	CharConfigItem& operator=(CharConfigItem&&) = default;
	CharConfigItem& operator=(const CharConfigItem&) = default;

	CharConfigItemSlot* getSlot(size_t slotIndex) { return slotIndex < m_slots.size() ? &m_slots[slotIndex] : nullptr; }
	const CharConfigItemSlot* getSlot(size_t slotIndex) const { return slotIndex < m_slots.size() ? &m_slots[slotIndex] : nullptr; }

	size_t getSlotCount() const { return m_slots.size(); }
	void updateSlotCount(size_t newSlotCount) { m_slots.resize(newSlotCount); }

	GLID_OR_BASE getGlidOrBase() const { return m_glidOrBase; }
	void setGlidOrBase(GLID_OR_BASE val) { m_glidOrBase = val; }

	GLID getAnimationGlid() const { return m_glidAnim; }
	void setAnimationGlid(GLID val) { m_glidAnim = val; }

	std::string getTexUrl() const { return m_texUrl; }
	void setTexUrl(std::string val) { m_texUrl = val; }

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	GLID_OR_BASE m_glidOrBase; // base glid (GOB_ACTOR_BASE(0) means base item instead of glid)
	GLID m_glidAnim; // animation glid (always 0)
	std::string m_texUrl; // custom texture (always "")
	std::vector<CharConfigItemSlot> m_slots;
};

class CharConfig {
public:
	CharConfig() {}
	CharConfig(std::initializer_list<CharConfigItem> items) :
			m_items(items) {} // For Daryl's CharConfigStatic.cpp
	~CharConfig() {}
	CharConfig(CharConfig&&) = default;
	CharConfig(const CharConfig&) = default;
	CharConfig& operator=(CharConfig&&) = default;
	CharConfig& operator=(const CharConfig&) = default;

	std::string ToStr(bool verbose = false) const;
	void Log(bool verbose = true) const;
#ifdef _AFX
	CArchive& Serialize(CArchive& ar, int version);
#endif

	bool extractFromEvent(IEvent* e);
	void insertIntoEvent(IEvent* e) const;

	// -----------------------------------------
	// Accessors for config items
	size_t getItemCount() const { return m_items.size(); }

	CharConfigItem* getItemByIndex(size_t itemIndex) { return itemIndex < m_items.size() ? &m_items[itemIndex] : nullptr; }
	const CharConfigItem* getItemByIndex(size_t itemIndex) const { return itemIndex < m_items.size() ? &m_items[itemIndex] : nullptr; }

	void initBaseItem(size_t numBaseSlots) {
		disposeAllItems(); // remove any old data if necessary
		m_items.push_back(CharConfigItem(GOB_BASE_ITEM, numBaseSlots));
	}

	CharConfigItem* addItem(const GLID_OR_BASE& glidOrBase, size_t slotCount) {
		// Already Exists ?
		auto pItem = getItemByGlidOrBase(glidOrBase);
		if (pItem) {
			pItem->updateSlotCount(slotCount);
			return pItem;
		}

		// Append new item
		m_items.push_back(CharConfigItem(glidOrBase, slotCount));
		return getItemByGlidOrBase(glidOrBase);
	}

	template <typename CharConfigItemRef>
	CharConfigItem* addItem(CharConfigItemRef&& item) {
		// Already Exists ?
		auto pItem = getItemByGlidOrBase(item.getGlidOrBase());
		if (pItem) {
			*pItem = std::forward<CharConfigItemRef>(item);
			return pItem;
		}

		// Append new item
		m_items.push_back(std::forward<CharConfigItemRef>(item));
		return getItemByGlidOrBase(item.getGlidOrBase());
	}

	void disposeAllItems() { m_items.clear(); }

private:
	template <typename ClassPtrT, typename ItemPtrT>
	static ItemPtrT GetItemByGlidOrBase(ClassPtrT self, const GLID_OR_BASE& glidOrBase) {
		for (auto& item : self->m_items) {
			if (item.getGlidOrBase() == glidOrBase)
				return &item;
		}
		return nullptr;
	}

public:
	CharConfigItem* getItemByGlidOrBase(const GLID_OR_BASE& glidOrBase) {
		return GetItemByGlidOrBase<CharConfig*, CharConfigItem*>(this, glidOrBase);
	}

	const CharConfigItem* getItemByGlidOrBase(const GLID_OR_BASE& glidOrBase) const {
		return GetItemByGlidOrBase<const CharConfig*, const CharConfigItem*>(this, glidOrBase);
	}

	// -----------------------------------------
	// Accessors for config slots
	size_t getTotalSlotCount() const {
		size_t count = 0;
		for (const CharConfigItem& item : m_items) {
			count += item.getSlotCount();
		}
		return count;
	}

	SHORT getItemSlotMeshId(const GLID_OR_BASE& glidOrBase, size_t slotIndex) const {
		auto pSlot = getItemSlotByGlidOrBase(glidOrBase, slotIndex);
		if (!pSlot)
			return 0;
		return pSlot->m_meshId;
	}

	bool setItemSlotMeshId(const GLID_OR_BASE& glidOrBase, size_t slotIndex, SHORT meshId, bool createIfNotFound = true) {
		// Get Item Slot By GLID
		auto pSlot = getItemSlotByGlidOrBase(glidOrBase, slotIndex);
		if (pSlot == nullptr) {
			// Create new
			return createIfNotFound ? setItemSlot(glidOrBase, slotIndex, CharConfigItemSlot(meshId)) : nullptr;
		}

		// Set Item Slot Mesh Id
		pSlot->m_meshId = meshId;
		return true;
	}

	USHORT getItemSlotMaterialId(const GLID_OR_BASE& glidOrBase, size_t slotIndex) const {
		auto pSlot = getItemSlotByGlidOrBase(glidOrBase, slotIndex);
		if (!pSlot)
			return 0;
		return pSlot->m_matlId;
	}

	bool setItemSlotMaterialId(const GLID_OR_BASE& glidOrBase, size_t slotIndex, USHORT matlId, bool createIfNotFound = true) {
		// Get Item Slot By GLID
		auto pSlot = getItemSlotByGlidOrBase(glidOrBase, slotIndex);
		if (pSlot == nullptr) {
			// Create new
			return createIfNotFound ? setItemSlot(glidOrBase, slotIndex, CharConfigItemSlot(0, matlId)) : false;
		}

		// Set Item Slot Material Id
		pSlot->m_matlId = matlId;
		return true;
	}

	bool getItemSlotColor(const GLID_OR_BASE& glidOrBase, size_t slotIndex, float& r, float& g, float& b) const {
		auto pSlot = getItemSlotByGlidOrBase(glidOrBase, slotIndex);
		if (!pSlot)
			return false;

		r = pSlot->m_r;
		g = pSlot->m_g;
		b = pSlot->m_b;
		return true;
	}

	bool setItemSlotColor(const GLID_OR_BASE& glidOrBase, size_t slotIndex, float r, float g, float b, bool createIfNotFound = true) {
		// Get Item Slot By GLID
		auto pSlot = getItemSlotByGlidOrBase(glidOrBase, slotIndex);
		if (pSlot == nullptr) {
			// Create new
			return createIfNotFound ? setItemSlot(glidOrBase, slotIndex, CharConfigItemSlot(0, 0, r, g, b)) : nullptr;
		}

		// Set Item Slot Color
		pSlot->m_r = r;
		pSlot->m_g = g;
		pSlot->m_b = b;
		return true;
	}

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	// -----------------------------------------
	// Helpers
	template <typename ClassPtrT, typename SlotTypeT>
	static SlotTypeT GetItemSlotByGlidOrBase(ClassPtrT self, const GLID_OR_BASE& glidOrBase, size_t slotIndex) {
		auto pItem = self->getItemByGlidOrBase(glidOrBase);
		if (pItem == nullptr) {
			return nullptr;
		}
		return pItem->getSlot(slotIndex);
	}

	const CharConfigItemSlot* getItemSlotByGlidOrBase(const GLID_OR_BASE& glidOrBase, size_t slotIndex) const {
		return GetItemSlotByGlidOrBase<const CharConfig*, const CharConfigItemSlot*>(this, glidOrBase, slotIndex);
	}

	CharConfigItemSlot* getItemSlotByGlidOrBase(const GLID_OR_BASE& glidOrBase, size_t slotIndex) {
		return GetItemSlotByGlidOrBase<CharConfig*, CharConfigItemSlot*>(this, glidOrBase, slotIndex);
	}

	bool setItemSlot(const GLID_OR_BASE& glidOrBase, size_t slotIndex, const CharConfigItemSlot& slot) {
		CharConfigItem* pItem = getItemByGlidOrBase(glidOrBase);
		if (pItem == nullptr) {
			pItem = addItem(glidOrBase, slotIndex + 1);
			assert(pItem != nullptr);
			if (pItem == nullptr) {
				return false;
			}
		}

		CharConfigItemSlot* pSlot = pItem->getSlot(slotIndex);
		if (pSlot == nullptr) {
			if (slotIndex >= pItem->getSlotCount()) {
				pItem->updateSlotCount(slotIndex + 1);
			}
			pSlot = pItem->getSlot(slotIndex);
		}

		*pSlot = slot;
		return true;
	}

private:
	std::vector<CharConfigItem> m_items;
};

} // namespace KEP
