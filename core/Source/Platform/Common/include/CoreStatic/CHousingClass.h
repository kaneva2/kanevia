///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "KEPObList.h"
#include "CStructuresMisl.h"

namespace KEP {

class CCollisionObj;
class CEXMeshObj;
class CEXMeshObjList;
class CRTLightObj;

class CHousingObj : public CObject, public GetSet {
	DECLARE_SERIAL(CHousingObj);

public:
	CHousingObj();

	CStringA m_houseName;
	CCollisionObj* m_closedState;
	CCollisionObj* m_openState;
	CEXMeshObjList* m_externalVisuals;
	CEXMeshObjList* m_internalVisuals;
	CEXMeshObjList* m_lod2Set;
	CEXMeshObjList* m_lod3Set;
	CEXMeshObj* m_doorVisual;
	ABBOX m_extrnalRangeLod1;
	ABBOX m_extrnalRangeLod2;
	ABBOX m_extrnalRangeLod3;
	ABBOX m_internalArea;
	ABBOX m_clipBox;
	CRTLightObj* m_lightObj;
	Vector3f m_lightPosition;

	void SafeDelete();
	void Serialize(CArchive& ar);
	CEXMeshObjList* GenerateRenderCache(Vector3f& point, BOOL open);
	BOOL FlushCacheObject(CEXMeshObjList* cache);
	BOOL BoundBoxPointCheck(Vector3f& point, ABBOX& box);

	DECLARE_GETSET
};

class CHousingObjList : public CKEPObList {
public:
	CHousingObjList(){};
	void SafeDelete();
	void ReinitAll(LPDIRECT3DDEVICE9 g_pd3dDevice);
	CHousingObj* GetByIndex(int index);

	DECLARE_SERIAL(CHousingObjList);
};

class CHousingPlacementObj : public CObject {
	DECLARE_SERIAL(CHousingPlacementObj);

public:
	CHousingPlacementObj();

	int m_dbIndex;
	int m_worldID;
	Vector3f m_position;
	BOOL m_open;

	void SafeDelete();
	void Serialize(CArchive& ar);
};

class CHousingPlacementList : public CObList {
public:
	CHousingPlacementList(){};
	void SafeDelete();

	DECLARE_SERIAL(CHousingPlacementList);
};

class CHousingCacheObj : public CObject {
public:
	CHousingCacheObj();

	int m_dbIndex;
	Vector3f m_position;
	Matrix44f m_location;
	Matrix44f m_invLocation;
	BOOL m_open;
	BOOL m_validThisRound;
	PLAYER_HANDLE m_playerHandle;

	void SafeDelete();
};

} // namespace KEP