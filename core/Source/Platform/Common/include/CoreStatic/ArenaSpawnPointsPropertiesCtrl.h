/******************************************************************************
 ArenaSpawnPointsPropertiesCtrl.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#if !defined(_ARENA_SPAWN_POINTS_PROPERTIESCTRL_H_)
#define _ARENA_SPAWN_POINTS_PROPERTIESCTRL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ArenaSpawnPointsPropertiesCtrl.h : header file
//
#include "KEPPropertiesCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CArenaSpawnPointsPropertiesCtrl window

class CArenaSpawnPointsPropertiesCtrl : public CKEPPropertiesCtrl {
	// Construction
public:
	CArenaSpawnPointsPropertiesCtrl();
	virtual ~CArenaSpawnPointsPropertiesCtrl();

public:
	virtual void OnEnableItem(GWCDeepIterator iter, bool enable, bool direct);
	virtual void OnShowInPlaceCtrl(CWnd* pWnd, GWCDeepIterator iter);
	virtual void OnHotLinkClicked(GWCDeepIterator iter);
	void Populate();

	// accessors
	float GetPositionX() const {
		return m_positionX;
	}
	float GetPositionY() const {
		return m_positionY;
	}
	float GetPositionZ() const {
		return m_positionZ;
	}

	// modifiers
	void SetPositionX(float positionX);
	void SetPositionY(float positionY);
	void SetPositionZ(float positionZ);

	// methods
	void Reset();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CArenaSpawnPointsPropertiesCtrl)
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CArenaSpawnPointsPropertiesCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum {
		INDEX_ROOT = 1,
		INDEX_POSITION,
		INDEX_X,
		INDEX_Y,
		INDEX_Z,
		INDEX_GET_CURRENT_POSITION
	};

	void GetCurrentPosition();

private:
	float m_positionX;
	float m_positionY;
	float m_positionZ;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_ARENA_SPAWN_POINTS_PROPERTIESCTRL_H_)
