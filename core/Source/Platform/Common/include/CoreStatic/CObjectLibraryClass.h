///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

namespace KEP {

class CWldObject;

class CObjectLibraryObj : public CObject, public GetSet {
	DECLARE_SERIAL(CObjectLibraryObj);

public:
	CObjectLibraryObj();
	void SafeDelete();

	CStringA m_description;
	CWldObject* m_wldObject;

	BOOL InitFromFile(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void Serialize(CArchive& ar);

	DECLARE_GETSET
};

class CObjectLibraryObjList : public CObList {
public:
	DECLARE_SERIAL(CObjectLibraryObjList);

	CObjectLibraryObjList();
	CWldObject* m_altCollisionModel;
	CStringA m_description;

	BOOL AddCollisionObject(CStringA filename, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void RemoveCollisionObject();
	BOOL AddObject(CStringA fileName, LPDIRECT3DDEVICE9 g_pd3dDevice);
	void SafeDelete();
	void Serialize(CArchive& ar);
};

class CObjectCollection : public CObject {
public:
	DECLARE_SERIAL(CObjectCollection);

	CObjectLibraryObjList* m_collection;

	CObjectCollection();
	void SafeDelete();
	void Serialize(CArchive& ar);
};

class CObjectCollectionList : public CObList {
public:
	DECLARE_SERIAL(CObjectCollectionList);

	CObjectCollectionList(){};
	void SafeDelete();
};

} // namespace KEP