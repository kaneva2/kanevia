///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// CArenaManagerDlg dialog
#include "CBattleSchedulerClass.h"
#include "afxwin.h"
#include "KEPEditDialog.h"
#include "KEPImageButton.h"
#include "CollapsibleGroup.h"
#include "ArenaPropertiesCtrl.h"
#include "ArenaSpawnPointsPropertiesCtrl.h"
#include "EditorState.h"
#include "CEXScriptClass.h"
#include "EXScriptHelper.h"
#include "ScriptAttributes.h"

class CArenaManagerDlg : public CKEPEditDialog {
	DECLARE_DYNAMIC(CArenaManagerDlg)

public:
	CArenaManagerDlg(CBattleSchedulerList*& battleSysListRef, CWnd* pParent = NULL); // standard constructor
	virtual ~CArenaManagerDlg();

	CBattleSchedulerList*& m_battleSysListRef;
	// Dialog Data
	enum { IDD = IDD_DIALOG_ARENAMANAGER };

	CCollapsibleGroup m_colArena;
	CButton m_grpArena;
	CArenaPropertiesCtrl m_propArena;

	CCollapsibleGroup m_colArenaSpawn;
	CButton m_grpArenaSpawn;
	CArenaSpawnPointsPropertiesCtrl m_propArenaSpawn;

	CKEPImageButton m_btnAddArena;
	CKEPImageButton m_btnDeleteArena;
	CKEPImageButton m_btnReplaceArena;

	CKEPImageButton m_btnAddSpawnPoint;
	CKEPImageButton m_btnDeleteSpawnPoint;
	CKEPImageButton m_btnReplaceSpawnPoint;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support
	virtual BOOL OnInitDialog();
	void Inlist(int sel);
	void InListSpawnPoints(int sel);
	void SetBattleList();
	void SetBattle(int battleIndex);
	void SetSpawnPointList();
	void SetSpawnPoint(int battleIndex, int spawnPointIndex);
	void ReplaceArena(int arenaSelection);
	void ReplaceSpawnPoint(int arenaSelection, int spawnPointSelection);
	DECLARE_MESSAGE_MAP()

private:
	//last selected index in the list box
	int m_previousSelArena;
	int m_previousSelSpawnPoint;

public:
	afx_msg void OnBnClickedButtonReplace();
	afx_msg void OnBnClickedButtonDelete();
	afx_msg void OnLbnSelchangeListBattlelist();
	afx_msg void OnBnClickedButtonAddspawnpoint();
	afx_msg void OnBnClickedButtonReplacespawnpoint();
	afx_msg void OnBnClickedButtonDeletespawnpoint();
	afx_msg void OnLbnSelchangeListSpawnpoints();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnBnClickedButtonSave();

	CListBox m_spawnPoints;
	CListBox m_battlesList;

protected:
	afx_msg BOOL DestroyWindow();
	BOOL SaveGameFile(CStringA fileName = "");
	void UpdateScript();
	BOOL SaveDialog(CStringA fileName = "");
};
