/******************************************************************************
 CTreeImporter.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "SpeedTreeRT.h"
#include "matrixarb.h"
#include "CReferenceLibrary.h"

#pragma once

class CTreeImporterObj : public CObject {
public:
	CTreeImporterObj();

	Matrix44f MatrixInit2;

	CEXMeshObj* GetLeafBuffersEXMesh(CSpeedTreeRT* m_pSpeedTree,
		Ext_lockpoints** lockPointsReturn,
		int& lockCount,
		float& billboardSize,
		CSpeedTreeRT::SGeometry* m_pGeometryCache,
		int lodOffset,
		float lodLevel);

	CEXMeshObj* GetFrondsEXMeshObjFromSpeedTree(float lodVal, CSpeedTreeRT* m_pSpeedTree, CSpeedTreeRT::SGeometry* m_pGeometryCache);
	CEXMeshObj* GetEXMeshObjFromSpeedTree(float lodVal, CSpeedTreeRT* m_pSpeedTree, CSpeedTreeRT::SGeometry* m_pGeometryCache);
	BOOL GetLibraryObjFromSPTFile(CReferenceObjList* refDB,
		CStringA* textureSearchPaths,
		int& searchPathCount,
		LPDIRECT3DDEVICE9 pd3dDevice);
	CEXMeshObj* GetBillboardBuffersEXMesh(CSpeedTreeRT* m_pSpeedTree,
		Ext_lockpoints** lockPointsReturn,
		int& lockCount,
		float& billboardSize,
		CSpeedTreeRT::SGeometry* m_pGeometryCache,
		int lodOffset,
		CStringA title);
};

