///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Glids.h"

namespace KEP {

class CMovementObjList;
class IMemSizeGadget;

class UGCActor {
public:
	UGCActor();
	~UGCActor();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

protected:
	GLID m_actorGlid;
	GLID m_DOGlid;
	GLID m_walkGlid;
	GLID m_runGlid;
	GLID m_standGlid;
	GLID m_jumpGlid;

	friend class ActorDatabase;
	friend class CMovementObj;
};

class ActorDatabase {
public:
	ActorDatabase(CMovementObjList* actors);
	~ActorDatabase();

	void CreateActorBasedOnDO(const GLID& actorGlid, const GLID& DOGlid);

	UGCActor* getActorData(const GLID& glid);

	CMovementObjList* GetEntObjList() {
		return m_actors;
	}

	void AddSupportedAnimation(const GLID& actorGlid, const GLID& animGlid, const std::string& animName);

private:
	std::vector<UGCActor*> m_ugcActors;
	CMovementObjList* m_actors; // You don't own this memory!!!!
};

} // namespace KEP
