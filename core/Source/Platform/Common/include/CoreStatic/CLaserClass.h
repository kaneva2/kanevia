///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>
#include "Core/Math/Matrix.h"

namespace KEP {
class CEXMeshObj;

class CLaserObj : public CObject, public GetSet {
	DECLARE_SERIAL(CLaserObj);

public:
	CLaserObj();
	void Clone(CLaserObj** clone, LPDIRECT3DDEVICE9 g_pd3dDevice);

	float m_speed;
	float m_currentLength;
	float m_currentWidth;
	BOOL m_lockDown;
	long m_millisecondsOfFade;
	BOOL m_fadeOut;
	BOOL m_terminated; //used for lockdown lasers

	CEXMeshObj* m_meshObject;

	Matrix44f m_orientationAndOrigin;
	Matrix44f m_fireFromLocation;

	void Serialize(CArchive& ar);
	void SafeDelete();
	BOOL UpdateLazerMesh(Matrix44f cameraMatrix);
	void InitializeMesh(LPDIRECT3DDEVICE9 g_pd3dDevice);

	DECLARE_GETSET
};

class CLaserObjList : public CObList {
public:
	CLaserObjList(){};
	void SafeDelete();

	DECLARE_SERIAL(CLaserObjList);
};

} // namespace KEP
