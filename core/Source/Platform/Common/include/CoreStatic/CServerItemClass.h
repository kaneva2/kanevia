///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#include <GetSet.h>
#include "InstanceId.h"
#include "CharConfigClass.h"
#include "Core/Util/CArchiveObjectSchema.h"

namespace KEP {

class CAICollectionQuestList;
class CInventoryObjList;

enum SERVER_ITEM_TYPE {
	SI_TYPE_LOOTABLE_CHEST = 0, // few vendors
	SI_TYPE_AI_QUEST = 3, // most city vendors
	SI_TYPE_ARENA_BOOKIE = 4, // places (employment office, cafe, hangar, theater, ...)
	SI_TYPE_PAPER_DOLL = 5 // drf - new paper doll
};

class CServerItemObj : public CObject, public GetSet {
	DECLARE_SERIAL_SCHEMA(CServerItemObj, VERSIONABLE_SCHEMA | 8);

public:
	CServerItemObj();

	void operator=(const CServerItemObj& rhs);

	ChannelId m_channel; //channel this lives on
	ZoneIndex m_zoneIndex; // drf - added - if not channel match try zone index

	int m_dbRef; //EDB reference
	Vector3f m_scale;
	Vector3f m_position; //cur position in world space
	int m_glidAnimSpecial; //current animation of entity
	int m_glid; //global id for inventory modifications
	int m_spawnID; //spawn Gen index for notification
	int m_netTraceId; // network assigned incrementing id (trace)
	int m_placementId;
	int m_requiredKey;
	int m_rotationAdjust;

	// removed old deformable cfg, switch to m_charConfig
	CharConfig m_charConfig;

	CStringA m_displayedName;

	SERVER_ITEM_TYPE m_type;

	int m_aiIndex; //-1 = none

	CStringA m_noResponseMsg;

	BOOL m_existsInAllPermInstances; // should this appear in all permanent instances of same zone?

	CAICollectionQuestList* m_collectionQuestDB;

	CInventoryObjList* m_contents;

	std::vector<GLID> m_glidsArmed; // drf - added

	std::string ToStr() const {
		std::string str;
		StrBuild(str, "SIO<" << this << ">"
							 << " '" << m_displayedName << "'"
							 << " type=" << (int)m_type
							 << " mapKey=" << MapKey()
							 << " netTraceId=" << m_netTraceId
							 << " spawnId=" << m_spawnID
							 << " " << m_charConfig.ToStr(false));
		return str;
	}

	// drf - added
	long MapKey() const {
		long channelId = m_channel.toLong();
		long zoneIndex = m_zoneIndex.toLong();
		return zoneIndex ? zoneIndex : channelId;
	}

	void SafeDelete();

	void Serialize(CArchive& ar);

	void Clone(CServerItemObj** clone);

	bool parseJSON(const std::string& jsonStr, std::string& err);

	DECLARE_GETSET
};

typedef std::vector<CServerItemObj*> ServerItemVec;

// Channel -> ServerItemObjs[] (static movement objects - city vendors)
class ServerItemObjMap {
public:
	CServerItemObj* GetItemInRadius(Vector3f entPos, float radius, const ChannelId& channel);

	CServerItemObj* GetObjectByNetTraceId(int id);
	CServerItemObj* GetObjectBySpawnID(int id);

	void AddItem(CServerItemObj* pSIO, bool replaceIfFound = false);

	bool DeleteByPlacementId(const ZoneIndex& zoneIndex, int placementId); // drf - added
	bool DeleteByZoneIndex(const ZoneIndex& zoneIndex); // drf - added

	BOOL DeleteByNetTraceId(int netTraceId);
	BOOL DeleteBySpawnIDAndChannel(int spawnId, ChannelId channel);

	void SafeDelete();

	void GetServerItemVec(ServerItemVec*& pSIV, long key1, long key2 = 0);

private:
	bool ErasePlacementId(int placementId);

	std::map<LONG, ServerItemVec> m_keyServerItemVecMap; // mapKey -> vector<ServerItemObj*>
	std::map<int, CServerItemObj*> m_pidServerItemObjMap; // placementId -> ServerItemObj*
};

} // namespace KEP
