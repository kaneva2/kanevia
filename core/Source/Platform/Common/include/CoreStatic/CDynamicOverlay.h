///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/Vector.h"

namespace KEP {

class CDynamicOverlayObj : public CObject {
public:
	CDynamicOverlayObj();

	Vector3f m_screenPos;
	float m_multiplier;
};

class CDynamicOverlayList : public CObList {
public:
	CDynamicOverlayList(){};
	void SafeDelete();
};

} // namespace KEP