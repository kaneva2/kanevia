///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <GetSet.h>

#include "matrixarb.h"
#include "CABFunctionLib.h"

namespace KEP {

class CDayStateObj;
class CDayStateObjList;
class CEXMeshObj;
class CLightningObjList;
class CSoundBankList;
class CStormObj;
class CStormObjList;

class CTimeSystemObj : public CObject, public GetSet {
	DECLARE_SERIAL(CTimeSystemObj);

public:
	CTimeSystemObj();

	void AddStorm(long duration,
		long frequency,
		CStringA stormName,
		long stormTransition,
		int particleSysInt);

	void Serialize(CArchive& ar);
	void SafeDelete();

	int GetNumDayStates();

	void AddDayState(CDayStateObj* _dayState);
	void AddDayState(float minutes,
		float fogStartRange,
		float fogEndRange,
		float fogColorRed,
		float fogColorGreen,
		float fogColorBlue,
		float sunColorRed,
		float sunColorGreen,
		float sunColorBlue,
		float ambientColorRed,
		float ambientColorGreen,
		float ambientColorBlue,
		float sunRotationX,
		float sunRotationY,
		float sunDistance);

	bool InsertDayStateBefore(CDayStateObj* _dayState, int _index);
	bool InsertDayStateBefore(float minutes,
		float fogStartRange,
		float fogEndRange,
		float fogColorRed,
		float fogColorGreen,
		float fogColorBlue,
		float sunColorRed,
		float sunColorGreen,
		float sunColorBlue,
		float ambientColorRed,
		float ambientColorGreen,
		float ambientColorBlue,
		float sunRotationX,
		float sunRotationY,
		float sunDistance,
		int _index);

	bool InsertDayStateAfter(CDayStateObj* _dayState, int _index);
	bool InsertDayStateAfter(float minutes,
		float fogStartRange,
		float fogEndRange,
		float fogColorRed,
		float fogColorGreen,
		float fogColorBlue,
		float sunColorRed,
		float sunColorGreen,
		float sunColorBlue,
		float ambientColorRed,
		float ambientColorGreen,
		float ambientColorBlue,
		float sunRotationX,
		float sunRotationY,
		float sunDistance,
		int _index);

	void UpdateDayState(int _index, const CDayStateObj* _dayState);
	void UpdateDayState(int _index, const float* _minutes = NULL,
		const float* _fogStartRange = NULL,
		const float* _fogEndRange = NULL,
		const float* _fogColorRed = NULL,
		const float* _fogColorGreen = NULL,
		const float* _fogColorBlue = NULL,
		const float* _sunColorRed = NULL,
		const float* _sunColorGreen = NULL,
		const float* _sunColorBlue = NULL,
		const float* _ambientColorRed = NULL,
		const float* _ambientColorGreen = NULL,
		const float* _ambientColorBlue = NULL,
		const float* _sunRotationX = NULL,
		const float* _sunRotationY = NULL,
		const float* _sunDistance = NULL);

	CDayStateObj* GetDayState(int _index);

	bool RemoveDayState(int _index);

	void UpdateSystem(
		CLightningObjList* lightningEffectSystem,
		Matrix44f soundCenter,
		CSoundBankList* soundManager);

	void StartSystem();

	TimeMs m_timeStamp;
	long m_currentSystemDay;
	long m_currentSystemMonth;
	long m_currentSystemYear;
	TimeMs m_currentDayTime;

	//cycle variable
	long m_monthsPerYear;
	long m_daysPerMonth;

	//Generate values
	TimeMs m_millisecondsPerDay;

	CDayStateObj* m_pDSO;

	//sun visual
	CEXMeshObj* m_visual;
	Vector3f m_currentSunPosition;

	//currentStorm if applicable
	BOOL m_stormInUse;
	CStormObj* m_stormPtr;
	long m_frequencyStormCheck;
	long m_stormCheckTimeStamp;
	CDayStateObjList* m_dayStateList;
	CStormObjList* m_stormDB;

private:
	CDayStateObj* MakeDayState(float minutes,
		float fogStartRange,
		float fogEndRange,
		float fogColorRed,
		float fogColorGreen,
		float fogColorBlue,
		float sunColorRed,
		float sunColorGreen,
		float sunColorBlue,
		float ambientColorRed,
		float ambientColorGreen,
		float ambientColorBlue,
		float sunRotationX,
		float sunRotationY,
		float sunDistance);

	DECLARE_GETSET
};

} // namespace KEP