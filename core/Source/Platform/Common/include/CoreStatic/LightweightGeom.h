///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Material.h"
#include "PrimitiveGeomArgs.h"
#include "Tools/wkglib/singleton.h"
#include <vector>

// forwad declaration
enum class ObjectType;

class BoundBox;
namespace KEP {
class ReMesh;

namespace LWG {

enum { // Geometry rendering flag
	F_DRAW_SOLID = 0x0000, // 3D solid
	F_DRAW_WIREFRAME = 0x0001, // wireframe only

	F_ALPHA_BLENDING = 0x0010, // Alpha blending (translucency)
	F_ALPHA_TESTING = 0x0020, // Alpha testing (on/off)
	F_CULL_FRONT = 0x0040,
	F_CULL_BACK = 0x0080,

	F_ALWAY_ON_TOP = 0x1000, // Render on top of everything else
};

enum { // Geom creation type
	//Standard geometries (see enum class PrimitiveType)
	GT_STANDARD_BASE = 0,

	//Geometry defined by list of vertices
	GT_TRIANGLES = 0x100,
	GT_LINES,
	GT_LINESTRIP,
};

typedef TMaterial<float> Material;

class IGeom {
public:
	virtual ~IGeom() {}

	virtual int getUniqueId() const = 0; // Return this geometry's unique ID

	virtual void setReferenceObj(ObjectType refType, int refObjId) = 0;
	virtual void getReferenceObj(ObjectType& refType, int& refObjId) const = 0;

	virtual void setPosition(const Vector3f& pos) = 0;
	virtual Vector3f getPosition() const = 0;
	virtual void setRotationAngles(const Vector3f& rotAngles) = 0;
	virtual void setScale(const Vector3f& scale) = 0;

	virtual bool isVisible() const = 0;
	virtual void setVisible(bool val) = 0;

	virtual void setMaterial(const Material& material) = 0;
	virtual const Material& getMaterial() const = 0;

	virtual void setDrawMode(int mode) = 0;
	virtual int getDrawMode() const = 0;

	virtual bool render(std::vector<ReMesh*>& meshes) = 0;
};

class IGeomFactory : public wkg::Singleton<IGeomFactory> {
public:
	virtual ~IGeomFactory() {}
	virtual IGeom* createStandardGeometry(const PrimitiveGeomArgs<float>& args) = 0;
	virtual IGeom* createCustomGeometry(int geomType, const std::vector<float>& positions, const std::vector<float>& normals) = 0;
};

void InitModule();
void CleanupModule();

} // namespace LWG

} // namespace KEP