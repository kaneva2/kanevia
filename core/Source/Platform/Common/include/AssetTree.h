///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

#include <Tools/tcl/tree.h>
#include "AssetImporterMath.h"

#include "KEPHelpers.h"

namespace KEP {

#define ANIM_URI_PREFIX "%Animation%"

enum KEP_ASSET_TYPE;
class AssetData;
typedef tcl::tree<AssetData> AssetTree;
enum enumUPAXIS;
class CDeformableMesh;
class CEXMeshObj;
class CHiarchyMesh;
typedef CHiarchyMesh CHierarchyMesh;

//! \brief AssetDef class: asset name and semantics.
//!
//! This class holds the definition of an asset (found by PreviewAssets function).
//! When PreviewAssets function traverses an URI, it looks into all asset found at the URI and all its children.
//! PreviewAssets will then return a list of AssetDef which indicates the actual URI the asset is located at and
//! the semantics of the asset (type and LOD level).
class AssetData // inline class
{
public:
	//! Asset Types
	enum Level {
		LVL_UNKNOWN = -1,

		// Newly added types must also be added into corresponding functions in AssetImporterDlg.cpp
		LVL_ALL = 0,
		LVL_GROUP = 1,
		LVL_OBJECT = 2,
		LVL_SECTION = 3,
		LVL_PART = LVL_SECTION,
		LVL_VARIATION = 4,
		LVL_LOD = 5,
		LVL_SUBMESH = 6,

		LVL_FLAT = 0x0F, // Level for various non-hierarchical assets <-- Never insert any non-level flag before this

		// Masks, additional flags goes here
	};

	enum Flag {
		STA_SELECTED = 0x000001, // Marked selected for import
		STA_IMPORTED = 0x000002, // Successfully imported
		STA_DELETED = 0x000004, // Reserved
		WRN_NAMING = 0x000100, // Naming convention mismatch
		ERR_MISSING = 0x010000, // Missing data source
		ERR_BADDATA = 0x020000, // Mal-formed data source
		ERR_INTERNAL = 0x040000, // Internal error during import
		ERR_FILESIZE = 0x080000, // File size over limit (use on root node only)
		ERR_NOTSUPP = 0x100000, // File format is valid but not supported by importer

		STA_MASK = 0x0000FF,
		WRN_MASK = 0x00FF00,
		ERR_MASK = 0xFF0000,
	};

	enum LevelOfDetail {
		LOD0 = 0,
		LOD_MAX = 9,
		LOD_COL = 10,
	}; // ID for base LOD, LOD max and collision mesh

	typedef struct {
		KEP_ASSET_TYPE m_assetType;
		std::string m_name;
		std::string desc;
	} AssetGroupDef;

	static AssetGroupDef assetGroupDefs[];

	//! Return name and description strings for a specific asset type to be used for grouping in asset tree
	static void GetAssetGroupNameFromType(KEP_ASSET_TYPE type, std::string& name, std::string& desc);

	//! Return asset type by asset group name
	static KEP_ASSET_TYPE GetAssetTypeByGroupName(const std::string& name);

	//! Return sub-tree based on asset type
	static AssetTree* GetAssetGroup(AssetTree* pRoot, KEP_ASSET_TYPE type);

	//! Check asset compatibility between two level:type pairs
	//! \return INT_MAX if incompatible. 0 if compatible. +/-1 if partially compatible (+1:level1 higher than level2 -1:level1 lower than level2)
	enum Compatibility {
		AC_LOWER = -1,
		AC_EQUAL = 0,
		AC_HIGHER = 1,
		AC_INCOMPAT = INT_MAX
	};

	static Compatibility CheckAssetCompatibility(Level level1, KEP_ASSET_TYPE type1, Level level2, KEP_ASSET_TYPE type2);

	class AssetMesh {
		CEXMeshObj* m_pExMesh;
		CDeformableMesh* m_pDeMesh;
		CHierarchyMesh* m_pHiMesh;

	public:
		AssetMesh(CEXMeshObj* m) :
				m_pExMesh(m), m_pDeMesh(NULL), m_pHiMesh(NULL) {}

		AssetMesh(CDeformableMesh* m) :
				m_pExMesh(NULL), m_pDeMesh(m), m_pHiMesh(NULL) {}

		AssetMesh(CHierarchyMesh* m) :
				m_pExMesh(NULL), m_pDeMesh(NULL), m_pHiMesh(m) {}

		AssetMesh(const AssetMesh& m) {
			m_pExMesh = m.m_pExMesh;
			m_pDeMesh = m.m_pDeMesh;
			m_pHiMesh = m.m_pHiMesh;
		}

		virtual ~AssetMesh();

		CEXMeshObj* GetExMesh() const {
			return m_pExMesh;
		}

		CDeformableMesh* GetDeMesh() const {
			return m_pDeMesh;
		}

		CHierarchyMesh* GetHiMesh() const {
			return m_pHiMesh;
		}

		CEXMeshObj* ExtractExMesh() {
			auto ret = m_pExMesh;
			m_pExMesh = nullptr;
			return ret;
		}

		CDeformableMesh* ExtractDeMesh() {
			auto ret = m_pDeMesh;
			m_pDeMesh = nullptr;
			return ret;
		}

		CHierarchyMesh* ExtractHiMesh() {
			auto ret = m_pHiMesh;
			m_pHiMesh = nullptr;
			return ret;
		}
	};

	typedef std::shared_ptr<AssetMesh> AssetMeshPtr;

private:
	KEP_ASSET_TYPE m_assetType;
	Level m_level;
	std::string m_key; //! complete name from top, e.g. "W_TABLE"
	std::string m_name; //! name of current node, e.g. "TABLE" ("" if this is a dummy intermediate node)
	std::string m_URI; //! URI for meshes: the URI for <node> element, padded with number of submeshes: Filename.dae#W_TABLE_001_0[3]
	int m_LOD_level; //! LOD level for meshes
	int m_flags; //! Flags: selected for import, imported, etc.

	std::string m_rootURI;

	UINT m_numSubmeshes;
	std::vector<AssetMeshPtr> m_assetMeshes; // multiple static meshes
	ImpFloatMatrix4 m_matrix; // parent matrix (not including transformation at current node)
	enumUPAXIS m_upAxis;

public:
	AssetData(KEP_ASSET_TYPE t = (KEP_ASSET_TYPE)-1, Level lvl = LVL_UNKNOWN, std::string k = "", std::string n = "", std::string u = "", int lod = -1) :
			m_assetType(t),
			m_level(lvl),
			m_key(k),
			m_name(n),
			m_URI(u),
			m_LOD_level(lod),
			m_flags(0),
			m_numSubmeshes(0) {}

	virtual ~AssetData() {}

	const std::string& GetKey() const {
		return m_key;
	}
	const std::string& GetName() const {
		return m_name;
	}

	const std::string& GetURI() const {
		return m_URI;
	}
	void SetURI(const std::string& uri) {
		m_URI = uri;
	}

	KEP_ASSET_TYPE GetType() const {
		return m_assetType;
	}
	void SetType(KEP_ASSET_TYPE t) {
		m_assetType = t;
	}

	Level GetLevel() const {
		return m_level;
	}
	void SetLevel(Level l) {
		m_level = l;
	}
	BOOL IsMeshLevel() const {
		return m_level >= LVL_LOD && m_level <= LVL_SUBMESH;
	}

	int GetLODLevel() const {
		return m_LOD_level;
	}
	void SetLODLevel(int l) {
		m_LOD_level = l;
	}

	int GetFlags() const {
		return m_flags;
	}
	void SetFlags(int f) {
		m_flags = f;
	}

	int CheckFlag(int mask) const {
		return m_flags & mask;
	}

	void SetFlag(int mask) {
		m_flags |= mask;
	}

	void ClearFlag(int mask) {
		m_flags &= ~mask;
	}

	const std::string& GetRootURI() const {
		return m_rootURI;
	}
	void SetRootURI(const std::string& uri) {
		m_rootURI = uri;
	}

	void AddMesh(CEXMeshObj* m) {
		m_assetMeshes.push_back(std::make_shared<AssetMesh>(m));
	}

	void AddMesh(CDeformableMesh* m) {
		m_assetMeshes.push_back(std::make_shared<AssetMesh>(m));
	}

	void AddMesh(CHiarchyMesh* m) {
		m_assetMeshes.push_back(std::make_shared<AssetMesh>(m));
	}

	DWORD GetActualSubmeshCount() const {
		return (DWORD)m_assetMeshes.size();
	}

	BOOL IsValidMeshId(UINT id) const {
		return (id < m_assetMeshes.size());
	}

	CEXMeshObj* GetExMesh(UINT id) const {
		return IsValidMeshId(id) ? m_assetMeshes[id]->GetExMesh() : NULL;
	}

	CDeformableMesh* GetDeMesh(UINT id) const {
		return IsValidMeshId(id) ? m_assetMeshes[id]->GetDeMesh() : NULL;
	}

	CHierarchyMesh* GetHiMesh(UINT id) const {
		return IsValidMeshId(id) ? m_assetMeshes[id]->GetHiMesh() : NULL;
	}

	// Detach and return EXMesh from asset data class
	CEXMeshObj* ExtractExMesh(UINT id) {
		return IsValidMeshId(id) ? m_assetMeshes[id]->ExtractExMesh() : NULL;
	}

	// Detach and return DMesh from asset data class
	CDeformableMesh* ExtractDeMesh(UINT id) {
		return IsValidMeshId(id) ? m_assetMeshes[id]->ExtractDeMesh() : NULL;
	}

	// Detach and return HMesh from asset data class
	CHierarchyMesh* ExtractHiMesh(UINT id) {
		return IsValidMeshId(id) ? m_assetMeshes[id]->ExtractHiMesh() : NULL;
	}

	void SetNumSubmeshes(int n) {
		m_numSubmeshes = n;
	}
	int GetNumSubmeshes() const {
		return m_numSubmeshes;
	}

	operator CEXMeshObj*() {
		return ExtractExMesh(0);
	}
	operator CHierarchyMesh*() {
		return ExtractHiMesh(0);
	}
	operator CDeformableMesh*() {
		return ExtractDeMesh(0);
	}

	const ImpFloatMatrix4& GetMatrix() const {
		return m_matrix;
	}
	void SetMatrix(const ImpFloatMatrix4& m) {
		m_matrix = m;
	}

	enumUPAXIS GetUpAxis() const {
		return m_upAxis;
	}
	void SetUpAxis(enumUPAXIS ua) {
		m_upAxis = ua;
	}

	friend bool operator<(const AssetData& a, const AssetData& b);
	friend bool operator>(const AssetData& a, const AssetData& b);
	friend bool operator==(const AssetData& a, const AssetData& b);
};

class IClassifyAssetNode {
public:
	virtual KEP_ASSET_TYPE operator()(const std::string& nodeName, int typeHint, bool hasGeom) = 0;
	virtual bool isDummy(const std::string& nodeName) = 0;
};

class ClassifyAssetNodeStd : public IClassifyAssetNode {
public:
	virtual KEP_ASSET_TYPE operator()(const std::string& /*nodeName*/, int typeHint, bool /*hasGeom*/) {
		return (KEP_ASSET_TYPE)typeHint;
	}

	virtual bool isDummy(const std::string& nodeName) {
		return nodeName.empty() || STLBeginWithIgnoreCase(nodeName, "nub__") || STLEndWithIgnoreCase(nodeName, "_PIVOT");
	}
};

inline bool operator<(const AssetData& a, const AssetData& b) {
	return a.m_assetType < b.m_assetType || a.m_assetType == b.m_assetType && a.m_key < b.m_key;
}

inline bool operator==(const AssetData& a, const AssetData& b) {
	return a.m_assetType == b.m_assetType && a.m_key == b.m_key;
}

} // namespace KEP
