///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "SDKCommon.h"
#include "KEP/Util/KEPUtil.h"

namespace KEP {

class KEPUTIL_EXPORT IReference {
public:

	IReference();

	virtual unsigned int AddReference();
	virtual unsigned int Release();

protected:

	unsigned int m_count;

	virtual ~IReference() = 0 {}
}; // class..IReference

template <typename ReferenceType>
class ReferenceTemplate {
public:

	ReferenceTemplate();
	ReferenceTemplate(ReferenceType* pReference);

	virtual unsigned int AddReference();
	virtual unsigned int Release();

	ReferenceType& operator*();
	ReferenceType* operator->();
	const ReferenceType& operator*() const;
	const ReferenceType* operator->() const;

protected:
	virtual ~ReferenceTemplate();

	ReferenceType* m_pReference;
	unsigned int m_count;
}; // class..ReferenceTemplate

template <typename ReferenceType>
ReferenceTemplate<ReferenceType>::ReferenceTemplate() :
		m_pReference(0),
		m_count(0) {
} // ReferenceTemplate< ReferenceType >::ReferenceTemplate

template <typename ReferenceType>
ReferenceTemplate<ReferenceType>::ReferenceTemplate(ReferenceType* pReference) :
		m_pReference(pReference),
		m_count(0) {
	delete m_pReference;
} // ReferenceTemplate< ReferenceType >::ReferenceTemplate

template <typename ReferenceType>
ReferenceTemplate<ReferenceType>::~ReferenceTemplate() {
	delete m_pReference;
} // ReferenceTemplate< ReferenceType >::~ReferenceTemplate

template <typename ReferenceType>
unsigned int
ReferenceTemplate<ReferenceType>::AddReference() {
	return ++m_count;
} // ReferenceTemplate< ReferenceType >::AddReference

template <typename ReferenceType>
unsigned int
ReferenceTemplate<ReferenceType>::Release() {
	if (0 == --m_count) {
		delete this;
		return 0;
	} // if..0 == --m_count

	return m_count;
} // ReferenceTemplate< ReferenceType >::Release

template <typename ReferenceType>
ReferenceType&
	ReferenceTemplate<ReferenceType>::operator*() {
	assert(m_pReference);
	return *m_pReference;
} // ReferenceTemplate< ReferenceType >::operator*

template <typename ReferenceType>
ReferenceType*
	ReferenceTemplate<ReferenceType>::operator->() {
	assert(m_pReference);
	return m_pReference;
} // ReferenceTemplate< ReferenceType >::operator->

template <typename ReferenceType>
const ReferenceType&
	ReferenceTemplate<ReferenceType>::operator*() const {
	assert(m_pReference);
	return *m_pReference;
} // ReferenceTemplate< ReferenceType >::operator*

template <typename ReferenceType>
const ReferenceType*
	ReferenceTemplate<ReferenceType>::operator->() const {
	assert(m_pReference);
	return m_pReference;
} // ReferenceTemplate< ReferenceType >::operator->
} // namespace KEP
