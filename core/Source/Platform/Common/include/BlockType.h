#pragma once

namespace KEP {

enum class BlockType {
	Ignore = 1, // blocked from messaging type functionality, e.g. /tell
	PlayerTravel = 2, // blocked from traveling to player or to player's apartment
	ZoneTravel = 3, // blocked from traveling to specific zone, e.g. broadband channel
};

} // namespace KEP