///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define COMPRESS_GZIP 1
#if (COMPRESS_GZIP == 1)
#include "tools\zlib\zlib.h"
#endif

#define COMPRESS_LZMA 1
#if (COMPRESS_LZMA == 1)
#include "tools\lzma1505\LzmaLib.h"
#endif

#include <string>

#include "common\KEPUtil\Helpers.h"

/// File Compression Types
enum COMPRESS_TYPE {
	COMPRESS_TYPE_NONE,
	COMPRESS_TYPE_GZIP,
	COMPRESS_TYPE_LZMA,
	COMPRESS_TYPES
};

/// Default Compression Format
#define COMPRESS_TYPE_DEFAULT COMPRESS_TYPE_LZMA

/// Compression Metrics
struct COMPRESS_METRICS {
	size_t m_num = 0;
	FileSize m_compSize = 0;
	FileSize m_origSize = 0;
	TimeMs m_timeMs = 0.0;

	void Add(FileSize origSize, FileSize compSize, TimeMs timeMs) {
		m_num++;
		m_origSize += origSize;
		m_compSize += compSize;
		m_timeMs += timeMs;
	}

	std::string ToStr() {
		std::string str;
		double pct = 100.0 - 100.0 * ((double)m_compSize / (double)m_origSize);
		StrBuild(str, "CM<num=" << m_num << " pct=" << std::fixed << std::setprecision(1) << pct << " timeMs=" << FMT_TIME << m_timeMs << ">");
		return str;
	}
};

// Returns compression version string.
std::string CompressVersion();

// Returns given file compression type.
COMPRESS_TYPE CompressType(const std::string& filePath);

// Returns default file extension for given compression type.
std::string CompressTypeExt(COMPRESS_TYPE type);

// Adds the given compression type extension to the given file path.
std::string CompressTypeExtAdd(const std::string& filePath, COMPRESS_TYPE type = COMPRESS_TYPE_DEFAULT);

// Removes any compression type extension from the given file path.
std::string CompressTypeExtDel(const std::string& filePath);

// Compresses given input file to output file using given compression type.
bool CompressFile(const std::string& filePathIn, const std::string& filePathOut, COMPRESS_TYPE type = COMPRESS_TYPE_DEFAULT);

// Compresses given input file to extension added output file and optionally cleans up uncompressed file.
bool CompressFile(const std::string& filePathIn, COMPRESS_TYPE type, bool cleanup = false);

// Uncompresses given input file to output file.
bool UncompressFile(const std::string& filePathIn, const std::string& filePathOut);

// Uncompresses given input file to extension stripped output file and optionally cleans up compressed file.
bool UncompressFile(const std::string& filePathIn, bool cleanup = false);

// Compresses given input memory buffer swapping memory in place.
bool CompressMemory(unsigned char*& pData, FileSize& dataSize, COMPRESS_TYPE type = COMPRESS_TYPE_DEFAULT);

// Returns compress metrics as JSON encoded string
std::string GetCompressMetrics(const std::string& runtimeId = "");