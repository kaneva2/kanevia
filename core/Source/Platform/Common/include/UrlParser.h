#pragma once

namespace KEP {

// class for doing some parsing of a URL
class UrlParser {
public:
	UrlParser() :
			m_url(0), m_scheme(0), m_leftPart(0) {}
	~UrlParser() {
		if (m_url)
			free(m_url);
	}

	static std::string unescapeString(const std::string &inStr) {
		std::string ret(inStr);
		char rep[2] = " ";

		std::string::size_type j = 0;
		std::string::size_type found = std::string::npos;
		while ((found = ret.find("%", j)) != std::string::npos) {
			if (found < ret.size() + 3) {
				std::string num = ret.substr(found + 1, 2);
				rep[0] = (char)strtol(num.c_str(), 0, 16);
				ret.replace(found, 3, rep);
				j = found + 1;
			}
		}
		return ret;
	}

	static std::string escapeString(const std::string &inStr) {
		typedef struct
		{
			const char *replacement;
			int replacementLen;
			char charToMap;
		} XmlMapping;

		static XmlMapping xmlMapping[] = {
			{ "%20", 3, ' ' },
			{ "%3d", 3, '=' },
			{ "%3f", 3, '?' },
			{ "%26", 3, '&' }
		};

		static int xmlMappingCnt = sizeof(xmlMapping) / sizeof XmlMapping;

		std::string ret(inStr);
		for (int i = 0; i < xmlMappingCnt; i++) {
			std::string::size_type j = 0;
			std::string::size_type found = std::string::npos;
			while ((found = ret.find(xmlMapping[i].charToMap, j)) != std::string::npos) {
				ret.replace(found, 1, xmlMapping[i].replacement);
				j = found + 1;
			}
		}
		return ret;
	}

	bool parseNoScheme(IN const char *str) { return parse(str, false); }

	bool parse(IN const char *str, bool scheme = true) {
		if (m_url)
			free(m_url);
		m_map.clear();

		m_url = _strdup(str);

		char *next = m_url;
		char *nextToken = 0;

		if (scheme) {
			m_scheme = strtok_s(m_url, ":", &nextToken);
			next = NULL;
		}

		m_leftPart = strtok_s(next, "?", &nextToken);
		char *q = 0;
		std::vector<char *> nameValues;
		while ((q = strtok_s(NULL, "&", &nextToken)) != 0) {
			nameValues.push_back(q);
		}

		for (std::vector<char *>::iterator i = nameValues.begin(); i != nameValues.end(); i++) {
			char *n;
			char *v;
			if ((n = strtok_s(*i, "=", &nextToken)) != 0) {
				if ((v = strtok_s(NULL, "=", &nextToken)) != 0)
					m_map[n] = v;
				else
					m_map[n] = "";

				m_map[n] = unescapeString(m_map[n]);
			}
		}

		return true;
	}

	typedef std::map<std::string, std::string, std::less<std::string>> NameValues;

	const NameValues &map() const { return m_map; }
	const char *leftPart() const { return m_leftPart; }
	const char *scheme() const { return m_scheme; }

	const char *hasValue(IN const char *name) const {
		UrlParser::NameValues::const_iterator j = map().find(name);

		if (j != map().end()) {
			return j->second.c_str();
		}
		return 0;
	}

	void print() {
		printf("Scheme: %s\n", scheme());
		printf("Left part: %s\n", leftPart());
		printf("Count: %d\n", m_map.size());
		for (NameValues::iterator i = m_map.begin(); i != m_map.end(); i++) {
			printf("   %s = %s\n", i->first.c_str(), i->second.c_str());
		}
	}

private:
	NameValues m_map;
	char *m_leftPart;
	char *m_scheme;

	char *m_url;
};

} // namespace KEP

