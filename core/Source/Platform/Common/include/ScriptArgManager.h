///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

class ScriptValue;

// This is the interface used to manage arguments and return values of calls from script.
// A new scripting language would need to implement this interface, as well as the REGISTER_FUNCTION() macro.
class IScriptArgManager {
public:
	enum class eValueType {
		None,
		Double,
		Int,
		Bool,
		String,
		Table,
		Array,
		Pointer
	};

	virtual std::string GetCallInfo() = 0;

	// These functions change the top of the stack by either pushing an element
	// from the stack, a table element, or an array element. The array or table
	// must be on top of the stack.
	virtual bool PushArgAtIndex(size_t idx) = 0;
	virtual bool PushTableElem(const char* pszKey) = 0;
	virtual bool PushArrayElem(size_t idx) = 0;
	virtual bool Pop() = 0;

	virtual bool HasArgAtIndex(size_t idxArg) = 0;

	// GetValue() returns the value on the top of the stack.
	virtual eValueType GetValueType() = 0;
	virtual bool GetValue(ScriptValue* pValue) = 0;
	virtual bool GetValue(bool* pValue) = 0;
	virtual bool GetValue(float* pValue) = 0;
	virtual bool GetValue(double* pValue) = 0;
	virtual bool GetValue(int32_t* pValue) = 0;
	virtual bool GetValue(std::string* pValue) = 0;
	virtual bool GetPointer(void** pValue) = 0;

	// The size of the array on the top of the stack.
	virtual bool GetArraySize(size_t* pSize) = 0;

	// PushValue() puts an item on the stack to be returned.
	virtual bool PushValue(const ScriptValue& value) = 0;
	virtual bool PushValue(nullptr_t) = 0; // nil
	virtual bool PushValue(bool value) = 0;
	virtual bool PushValue(int32_t value) = 0;
	virtual bool PushValue(float value) = 0;
	virtual bool PushValue(double value) = 0;
	virtual bool PushValue(const char* value) = 0;
	virtual bool PushValue(const std::string& value) = 0;
	virtual bool PushPointer(void* pValue) = 0;

	virtual bool PushNewTable(size_t nElements) = 0; // Creates a new table with nElements and pushes it on top of the stack.
	virtual bool PushNewArray(size_t nElements) = 0; // Creates a new array with nElements and pushes it on top of the stack.
	virtual bool PushKey(const char* pszKey) = 0; // Key to be used in combination with AddTableElem() to add an entry to a table.
	bool PushKey(const std::string& strKey) { return this->PushKey(strKey.c_str()); }
	virtual bool AddTableElem() = 0; // Adds top key/value pair to table just below it in the stack.
	virtual bool SetArrayElem(size_t idx) = 0; // Array just below top of stack, value at top. Array[idx] = value.
};
