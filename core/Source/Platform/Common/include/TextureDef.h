///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

enum class ImageOpacity;

struct TextureDef {
	bool valid;

	std::string fileName;
	std::string path;
	unsigned int width;
	unsigned int height;
	FileSize bytes;

	std::string origFileName;
	std::string origPath;
	unsigned int origWidth;
	unsigned int origHeight;
	FileSize origBytes;

	ImageOpacity opacity;
	unsigned int averageColor;

	TextureDef() :
			valid(false),
			width(0),
			height(0),
			bytes(0),
			origWidth(0),
			origHeight(0),
			origBytes(0),
			opacity((ImageOpacity)0),
			averageColor(0) {
	}
};

} // namespace KEP
