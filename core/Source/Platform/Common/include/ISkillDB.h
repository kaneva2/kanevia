#pragma once

namespace KEP {

class IGetSet;
class ISkillDB;

class ISkillDB {
public:
	// version of this class interface
	static BYTE Version() { return 1; }

	virtual IGetSet* SkillByType(IN int type) = 0;
	virtual IGetSet* SkillByName(IN const char* skillName) = 0;
	//	virtual bool SkillCheck( IN int type,  IN ISkillDB * dbList, OUT IGetSet **skPtr, OUT IGetSet **dbSkillPtr, OUT bool &added ) = 0;
};

} // namespace KEP
