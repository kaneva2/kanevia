///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <assert.h>

namespace KEP {

template <typename TReference>
class ReferencePointer {
public:

	ReferencePointer();
	explicit ReferencePointer(TReference* pReference);
	explicit ReferencePointer(const ReferencePointer<TReference>& pointerRHS);
	~ReferencePointer();

	TReference& operator*();
	TReference* operator->();
	const TReference& operator*() const;
	const TReference* operator->() const;

	ReferencePointer<TReference>& operator=(const ReferencePointer<TReference>& pointer);

	bool operator!() const;
	bool operator==(const ReferencePointer<TReference>& pointer) const;
	bool operator!=(const ReferencePointer<TReference>& pointer) const;
	bool operator<(const ReferencePointer<TReference>& pointer) const;
	bool operator>(const ReferencePointer<TReference>& pointer) const;
	bool operator<=(const ReferencePointer<TReference>& pointer) const;
	bool operator>=(const ReferencePointer<TReference>& pointer) const;
	operator bool() const;

	template <typename TOtherType>
	operator TOtherType();

protected:

	void AddReference();
	void Release();

	unsigned int* m_pCount;
	TReference* m_pReference;
}; // class..ReferencePointer

template <typename TReference>
ReferencePointer<TReference>::ReferencePointer() :
		m_pReference(0),
		m_pCount(0) {
} // ReferencePointer< TReference >::ReferencePointer

template <typename TReference>
ReferencePointer<TReference>::ReferencePointer(TReference* pReference) :
		m_pReference(pReference) {
	m_pReference = pReference;
	m_pCount = new unsigned int;
	*m_pCount = 0;

	AddReference();
} // ReferencePointer< TReference >::ReferencePointer

template <typename TReference>
ReferencePointer<TReference>::ReferencePointer(const ReferencePointer<TReference>& pointer) :
		m_pReference(pointer.m_pReference),
		m_pCount(pointer.m_pCount) {
	AddReference();
} // ReferencePointer< TReference >::ReferencePointer

template <typename TReference>
ReferencePointer<TReference>::~ReferencePointer() {
	Release();
} // ReferencePointer< TReference >::~ReferencePointer

template <typename TReference>
void ReferencePointer<TReference>::AddReference() {
	if (m_pReference) {
		(*m_pCount)++;
	} // if..m_pReference
} // ReferencePointer< TReference >::AddReference

template <typename TReference>
void ReferencePointer<TReference>::Release() {
	if (m_pReference) {
		--(*m_pCount);
		if (!(*m_pCount)) {
			delete m_pReference;
			delete m_pCount;
			m_pReference = 0;
			m_pCount = 0;
		} // if..!( *m_pCount )
	} // if..m_pReference
} // ReferencePointer< TReference >::Release

template <typename TReference>
TReference&
	ReferencePointer<TReference>::operator*() {
	assert(m_pReference);
	return *m_pReference;
} // ReferencePointer< TReference >::operator*

template <typename TReference>
TReference*
	ReferencePointer<TReference>::operator->() {
	assert(m_pReference);
	return m_pReference;
} // ReferencePointer< TReference >::operator->

template <typename TReference>
const TReference&
	ReferencePointer<TReference>::operator*() const {
	assert(m_pReference);
	return *m_pReference;
} // ReferencePointer< TReference >::operator*

template <typename TReference>
const TReference*
	ReferencePointer<TReference>::operator->() const {
	assert(m_pReference);
	return m_pReference;
} // ReferencePointer< TReference >::operator->

template <typename TReference>
ReferencePointer<TReference>&
ReferencePointer<TReference>::operator=(const ReferencePointer<TReference>& pointer) {
	// If the given pointer is equivalent to this pointer, then
	// there isn't a need to do anything, therefore just return
	// out of here.
	if (pointer.m_pReference == m_pReference)
		return *this;

	// If the current reference is valid, be sure to decrement it's
	// reference count before assigning the given reference.
	Release();

	m_pReference = pointer.m_pReference;
	m_pCount = pointer.m_pCount;

	AddReference();

	return *this;
} // ReferencePointer< TReference >::operator=

template <typename TReference>
bool ReferencePointer<TReference>::operator!() const {
	return !m_pReference;
} // ReferencePointer< TReference >::operator!

template <typename TReference>
bool ReferencePointer<TReference>::operator==(const ReferencePointer<TReference>& pointer) const {
	return pointer.m_pReference == m_pReference;
} // ReferencePointer< TReference >::operator==

template <typename TReference>
bool ReferencePointer<TReference>::operator!=(const ReferencePointer<TReference>& pointer) const {
	return !(pointer == *this);
} // ReferencePointer< TReference >::operator!=

template <typename TReference>
bool ReferencePointer<TReference>::operator<(const ReferencePointer<TReference>& pointer) const {
	return m_pReference < pointer.m_pReference;
} // ReferencePointer< TReference >::operator<

template <typename TReference>
bool ReferencePointer<TReference>::operator>(const ReferencePointer<TReference>& pointer) const {
	return m_pReference > pointer.m_pReference;
} // ReferencePointer< TReference >::operator>

template <typename TReference>
bool ReferencePointer<TReference>::operator<=(const ReferencePointer<TReference>& pointer) const {
	return m_pReference <= pointer.m_pReference;
} // ReferencePointer< TReference >::operator<=

template <typename TReference>
bool ReferencePointer<TReference>::operator>=(const ReferencePointer<TReference>& pointer) const {
	return m_pReference >= pointer.m_pReference;
} // ReferencePointer< TReference >::operator>=

template <typename TReference>
ReferencePointer<TReference>::operator bool() const {
	return reinterpret_cast<bool>(m_pReference);
} // ReferencePointer< TReference >::operator bool

template <typename TReference>
template <typename TOtherType>
ReferencePointer<TReference>::operator TOtherType() {
	return reinterpret_cast<TOtherType>(m_pReference);
} // ReferencePointer< TReference >::operator TOtherType
} // namespace KEP
