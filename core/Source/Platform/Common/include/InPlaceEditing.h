/******************************************************************************
InPlaceEditing.h

Copyright (c) 2004-2008 Kaneva, Inc. All Rights Reserved Worldwide
Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

//! Callback functor to be called when editing is to be finished
struct InPlaceEditingCallback {
	virtual BOOL operator()(const CStringA& newText) = 0;
	virtual ~InPlaceEditingCallback() {}
};

class InPlaceEditingTarget {
public:
	virtual void GetItemText(int row, int col, CStringA& text) = 0;
	virtual void SetItemText(int row, int col, const CStringA& newText) = 0;
	virtual void GetItemRect(int row, int col, LPRECT rect) = 0;

	virtual BOOL GetSelectedItem(int& row, int& col) = 0; // Return FALSE if no selection, TRUE otherwise

	virtual BOOL IsValid() = 0; // Control validity (for MFC: GetSafeHWnd()!=NULL)

	virtual CWnd& GetWnd() = 0;
};

//! \brief Enabling in-place editing for list controls (currently only ListBox) with a Edit control
class InPlaceEditing {
private:
	InPlaceEditingTarget* m_targetControl;
	int m_row, m_col;
	BOOL m_editing;
	InPlaceEditingCallback* m_editingCallback;
	CEdit& m_editBox;

public:
	InPlaceEditing(CEdit& editBox);

	BOOL startEditing(InPlaceEditingTarget* targetControl, InPlaceEditingCallback* callback);
	BOOL endEditing();
	BOOL cancelEditing();
	BOOL isEditingInProgress() { return m_editing; }
};

class IPE_ListBoxWrapper : public InPlaceEditingTarget {
private:
	CListBox& m_listBox;

public:
	IPE_ListBoxWrapper(CListBox& lb);
	virtual void GetItemText(int row, int col, CStringA& text);
	virtual void SetItemText(int row, int col, const CStringA& newText);
	virtual void GetItemRect(int row, int col, LPRECT rect);

	virtual BOOL GetSelectedItem(int& row, int& col);

	virtual BOOL IsValid();

	virtual CWnd& GetWnd();
};

class IPE_ListCtrlWrapper : public InPlaceEditingTarget {
private:
	CListCtrl& m_listCtrl;

public:
	IPE_ListCtrlWrapper(CListCtrl& lc);
	virtual void GetItemText(int row, int col, CStringA& text);
	virtual void SetItemText(int row, int col, const CStringA& newText);
	virtual void GetItemRect(int row, int col, LPRECT rect);

	virtual BOOL GetSelectedItem(int& row, int& col);

	virtual CWnd& GetWnd();
};

} // namespace KEP