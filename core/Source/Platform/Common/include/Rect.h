///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

inline int RectWidth(const RECT& rc) {
	return (rc.right - rc.left);
}

inline int RectHeight(const RECT& rc) {
	return (rc.bottom - rc.top);
}
