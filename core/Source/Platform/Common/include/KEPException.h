#pragma once

#include "KEPHelpers.h"
#include <memory>
#include "kepmacros.h"

#include <string>

namespace KEP {

class __declspec(dllexport) KEPException {
public:
	std::string m_msg;
	ULONG m_err;
	std::string m_file;
	ULONG m_line;

	KEPException(const std::string& msg, ULONG errNo = 0, const std::string& file = "", ULONG line = 0) :
			m_msg(msg),
			m_err(errNo),
			m_file(file),
			m_line(line) {}

	~KEPException() {}

	std::string ToString() const {
		std::string buffer;
		StrAppend(buffer, "KEPException: '" << m_msg << "'");
		if (m_err)
			StrAppend(buffer, " [" << std::hex << m_err << "]");
		if (!m_file.empty())
			StrAppend(buffer, " at " << m_file << ":" << m_line);
		return buffer;
	}

	/**
	 * @deprecated Destructor is now exposed.  Use delete operator.
	 */
	void Delete() { delete this; }
};

typedef std::shared_ptr<KEPException> KEPExceptionPtr;

// macro to make checking and throwing an error easier
#define THROW_FAILED(hr, msg)                                           \
	{                                                                   \
		if (FAILED(hr))                                                 \
			throw new KEPException(msg, (ULONG)hr, __FILE__, __LINE__); \
	}

} // namespace KEP
