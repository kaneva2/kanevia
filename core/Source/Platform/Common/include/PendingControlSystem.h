///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CoreStatic\DXInputSystem.h"

namespace KEP {

class CPendingControlSystem {
public:
	CPendingControlSystem() :
			m_index(-1) {}
	virtual ~CPendingControlSystem() {}

	int GetIndex() const { return m_index; }
	void SetIndex(int index) { m_index = index; }

	void CheckCaptureControl(const MouseState& mouseState, const KeyboardState& keyboardState);

private:
	int m_index;
};

} // namespace KEP