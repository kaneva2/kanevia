///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// DRF - Added - Pass Group Enumeration (bitmask)
enum PASS_GROUP {
	PASS_GROUP_NONE = 0x00,
	PASS_GROUP_AP = 0x01,
	PASS_GROUP_VIP = 0x02,
	PASS_GROUP_AP_OLD = 0x04, // drf - deprecated - occurs in pairs tied to AP
	PASS_GROUP_GM = 0x08, // drf - added
	PASS_GROUP_OWNER = 0x10, // drf - added
	PASS_GROUP_MODERATOR = 0x20, // drf - added
	PASS_GROUP_INVALID = 0x80
};
