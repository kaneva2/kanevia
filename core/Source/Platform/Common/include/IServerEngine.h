///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPCommon.h"

#include "IGame.h"
#include "IGameStateDispatcher.h"
#include "ChatType.h"
#include "KEPNetwork.h"
#include "KEPConstants.h"
#include "CurrencyType.h"
#include "Core/Util/HighPrecisionTime.h"

namespace KEP {

class ZoneIndex;
class IEvent;
class IPlayer;
class ISkillDB;
class IKEPConfig;
class KGP_Connection;
enum class SlashCommand;
enum class TransactionType;

typedef enum {
	DT_Arena, // 03 / 08 moved down the enum to avoid database checks by stored procedures.  see ServerEngine::initiateSpawn
	DT_ValidatedZone, // don't need to validate player can go there
	DT_ValidatedPlayer, // don't need to validate player can go there
	DT_Zone,
	DT_Player

} EDestinationType;

class IServerEngine : public IGame {
public:
	enum GlidState {
		Loaded, // Your glid is ready and good to go
		NotLoaded, // Your glid needs to be loaded from the DB
		Error // We previously tried and failed to load that glid
	};

	virtual ~IServerEngine();

	// version of this class interface
	static BYTE Version() { return 1; }

	virtual void BroadcastMessage(
		short radius, // 0=to all
		const char* message,
		double x, double y, double z,
		LONG channel,
		NETID fromNetId,
		eChatType chatType = eChatType::System,
		bool allInstances = false) = 0;

	virtual void BroadcastMessageToDistribution(
		const char* message,
		NETID fromNetId,
		const char* toDistribution,
		const char* fromRecipient,
		eChatType chatType = eChatType::System,
		bool fromRemote = false,
		ULONG filter = 0) = 0;

	virtual void BroadcastEvent(
		IEvent* rte,
		LONG channel = -1, // if INVALID_CHANNEL_ID, all channels
		NETID netID = 0, // player NetId for ctClan, ctGroup
		short radius = 0, // 0 radius = to all
		double x = 0, double y = 0, double z = 0, // if non-zero radius, position
		eChatType chatType = eChatType::System,
		bool allInstances = false) = 0;

	virtual void BroadcastEventFromZoneIndex(IEvent* e, int zoneId) = 0;

	virtual bool DisconnectPlayer(NETID playerId, const char* optionalLogMsg = 0) = 0;

	virtual bool SendTextMessage(IPlayer* to, const char* msg, eChatType chatType) = 0;

	// Script Glue Only (GameGlue.cpp)
	virtual IPlayer* GetPlayerByName(const std::string& userName) = 0;
	virtual IPlayer* GetPlayerByNetId(const NETID& netId, bool includeSpawning) = 0;
	virtual IPlayer* GetPlayerByNetTraceId(int netTraceId) = 0;

	// Message Senders
	virtual bool QueueMsgEquipToAllClients(IPlayer* player, const std::vector<GLID>& glidsSpecial) = 0;
	virtual bool QueueMsgEquipToClient(const std::vector<GLID>& glidsSpecial, long netTraceId, IPlayer* player) = 0;
	virtual BOOL SendInventoryUpdateToClient(NETID netIdFrom) = 0;
	virtual BOOL SendHealthUpdateToClient(NETID netIdFrom) = 0;

	// a temporary method to process text commands until event handlers do it all
	virtual void TextHandler(SlashCommand commandId, const char* msg, NETID netIdFrom) = 0;

	virtual void SpawnToPoint(IPlayer* p, double x, double y, double z, LONG zoneIndex, EDestinationType destType = DT_Zone, int rotation = 0, const char* url = 0) = 0;

	virtual IGetSet* GetItemObjectByNetTraceId(int netTraceId) = 0;

	virtual IGetSet* GetItemObjectBySpawnId(int spawnId) = 0;

	virtual IPlayer* GetPlayerOffAccountByNetId(NETID id, int index) = 0;

#if (DRF_OLD_VENDORS == 1)
	virtual void DeSpawnStaticAIByNetTraceId(int netTraceId) = 0;
	virtual void DeSpawnStaticAIBySpawnId(int spawnID, LONG channelInstance) = 0;

	virtual IGetSet* SpawnStaticAIBySpawnId(int spawnID, double x, double y, double z, LONG channelInstance) = 0;
#endif

	virtual bool EquipInventoryItem(IPlayer* player, const GLID& glid, NETID messageFromDPID, bool fromScript = false) = 0;

	virtual BOOL GetInventoryItemQty(IPlayer* player, const GLID& glid, int& itemQuantity) = 0;

	virtual BOOL AddInventoryItemToPlayer(IPlayer* player, int nQty, BOOL bArmed, const GLID& glid) = 0;

	virtual BOOL RemoveInventoryItemFromPlayer(IPlayer* player, int nQty, BOOL bArmed, const GLID& glid, short invTypes) = 0;

	virtual bool TellClientToOpenMenu(LONG menuId, NETID playerNetId, BOOL closeAllOthers, int networkAssignIdOfTradee) = 0;

	virtual IGetSet* GetPlayersBestSkill(IPlayer* player) = 0;
	virtual BOOL UseSkillOnPlayer(IPlayer* player, int skillId, int level, int ability) = 0;
	virtual BOOL UseSkillActionOnPlayer(IPlayer* player, ULONG skillId, ULONG actionId, BOOL bUseAlternate, int& newLevel, std::string& levelMessage, std::string& gainMessage, int& percentToNextLevel, std::string& rewardsAtNextLevel, float& totalExperience) = 0;

	virtual BOOL UpdatePlayersCash(IPlayer* player, long player1AmtChanging, TransactionType transactionType, int glid, long qty, CurrencyType currencyType) = 0;

	virtual void UpdatePlayer(IPlayer* currentPlayer) = 0;

	virtual IGameStateDispatcher* GetGameStateDispatcher() = 0;
	virtual GlidState checkGlid(GLID glid) = 0;
	virtual GlidState checkGlids(std::vector<GLID> const& glids, std::vector<GLID>& unloadedGlids) = 0;
	virtual IBackgroundDBEvent* createLoadGlidEvent(std::vector<GLID> const& glids, IEvent* responseEvent) = 0;
	virtual void setFailedLoadGlid(GLID glid) = 0;

	virtual void updateScriptZoneState(const ZoneIndex& zoneIndex, unsigned spinDownDelayMins, bool spinUp) = 0;

	virtual const IKEPConfig* GetDBKEPConfig() const = 0;
	static IServerEngine* GetInstance();

	virtual unsigned long GetServerId() const = 0;
	virtual void ClearZoneDOMemCache(ZoneIndex const& zi) = 0;
	virtual bool GetParentZoneInfo(KGP_Connection* conn, unsigned zoneInstanceId, unsigned zoneType, unsigned& parentZoneInstanceId, unsigned& parentZoneIndex, unsigned& parentZoneType, unsigned& parentCommunityId) = 0;
	virtual TimeMs GetEffectDurationMs(KGP_Connection* conn, GLID globalId) = 0;

	virtual bool GetYouTubeSWFUrlsFromDB(std::string& lastKnownGoodUrl, std::string& overrideUrl) = 0;

protected:
	IServerEngine();

private:
	static const char* SERVERENGINE;
};

} // namespace KEP
