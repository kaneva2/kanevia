///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// DRF - Use Type Enumeration
enum USE_TYPE {
	USE_TYPE_INVALID = -1, // drf - added
	USE_TYPE_NONE = 0,
	USE_TYPE_HEALING = 1,
	USE_TYPE_AMMO = 2,
	USE_TYPE_SKILL = 3,
	USE_TYPE_MENU = 4,
	USE_TYPE_COMBINE = 5,
	USE_TYPE_PLACE_HOUSE = 6,
	USE_TYPE_INSTALL_HOUSE = 7,
	USE_TYPE_SUMMON_CREATURE = 8,
	USE_TYPE_OLD_SCRIPT = 9,
	USE_TYPE_ADD_DYN_OBJ = 10,
	USE_TYPE_ADD_ATTACH_OBJ = 11,
	USE_TYPE_MOUNT = 12,
	USE_TYPE_EQUIP = 13,

	// constants to be used for getting use values from item_parameters table
	USE_TYPE_FIRST_TYPE = 1,
	USE_TYPE_LAST_TYPE = 13,

	// anything greater than this will be fired as an event
	USE_TYPE_FIRST_EVENT = 200,

	// convenience helpers
	USE_TYPE_ANIMATION = USE_TYPE_FIRST_EVENT + 0,
	USE_TYPE_P2P_ANIMATION = USE_TYPE_FIRST_EVENT + 1,
	USE_TYPE_NPC = USE_TYPE_FIRST_EVENT + 2,
	USE_TYPE_DEED = USE_TYPE_FIRST_EVENT + 3,
	USE_TYPE_QUEST = USE_TYPE_FIRST_EVENT + 4,
	USE_TYPE_PREMIUM = USE_TYPE_FIRST_EVENT + 5,
	USE_TYPE_PARTICLE = USE_TYPE_FIRST_EVENT + 6,
	USE_TYPE_SOUND = USE_TYPE_FIRST_EVENT + 7,
	USE_TYPE_BUNDLE = USE_TYPE_FIRST_EVENT + 8,
	USE_TYPE_CUSTOM_DEED = USE_TYPE_FIRST_EVENT + 9,
	USE_TYPE_ACTION_ITEM = USE_TYPE_FIRST_EVENT + 10,
	USE_TYPE_GAME_ITEM = USE_TYPE_FIRST_EVENT + 11, // 211
};

enum class USE_TYPE_ANIMATION_USE_VALUE {
	DO = 0,
	MALE = 1,
	FEMALE = 2
};