/******************************************************************************
 GetSetMFCSerialize.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace KEP {

class GetSet;

// serialize the meta data, you must pass in the object that is associated
// with the meta meta (usually a default one)
bool SerializeGetSetMetaData(GetSet *obj, CArchive &archive);

// serialize the values out, must have done the metadata first on deserialize
bool SerializeGetSetValues(GetSet *obj, CArchive &archive);

} // namespace KEP

