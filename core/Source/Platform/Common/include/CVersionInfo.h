///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// STL
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <cctype>
#include <vector>
#include <algorithm>
#include <sstream>

inline bool CompareCaseInsensitive(std::string s1, std::string s2) {
	transform(s1.begin(), s1.end(), s1.begin(), toupper);
	transform(s2.begin(), s2.end(), s2.begin(), toupper);
	if (s1 == s2)
		return true;
	else
		return false;
}

// forward declare
class CVIFileEntry;

typedef std::map<std::string, CVIFileEntry> FileEntryMap;
typedef std::map<std::string, CVIFileEntry*> FileEntryMapPtr;

class CVersionInfo {
public: // infastructure
	CVersionInfo();

public: // methods
	void clear();
	bool isValid();

	bool loadFile(const std::string& filePathName);
	bool loadFile(const std::string& basePath, const std::string& fileName);

	bool writeFile(const std::string& fileName);
	bool writeFile(const std::string& basePath, const std::string& fileName);

	bool isEquivalent(CVersionInfo& rhs);

public: // properties
	// read/write
	std::string get_Ugpv() const;
	void set_Ugpv(const std::string& newval);

	std::string get_Version() const;
	void set_Version(const std::string& newval);

	std::string get_VIVersion() const;
	void set_VIVersion(const std::string& newval);

	std::string get_TemplateVersion() const;
	void set_TemplateVersion(const std::string& newval);

	std::string get_TemplateType() const;
	void set_TemplateType(const std::string& newval);

	std::string get_VIFType() const;
	void set_VIFType(const std::string& newval);

	std::string get_GameId() const;
	void set_GameId(const std::string& newval);

	// read only
	long get_TotalFiles();
	long get_TotalSize();
	FileEntryMap& getFileEntries();

private: // data
	// internal
	std::string m_basePath;
	std::string m_configFileName;
	std::vector<std::string> m_errors;

	// fields
	std::map<std::string, std::string> m_headerFields;
	FileEntryMap m_fileEntries;

private: // helpers
	std::string getMapValue(const std::string& key) const;
};

class CVIFileEntry {
public: // infastructure
	CVIFileEntry();

	bool operator==(const CVIFileEntry& rhs) const;
	bool operator!=(const CVIFileEntry& rhs) const;

public: // methods
	void clear();
	bool fromRawLine(const std::string& rawline);
	std::string asRawLine();

	std::string FileNameFull() const;
	std::string ServerFileNameFull() const;

	std::string FileNameOnly() const;
	std::string ServerFileNameOnly() const;

	// convenience
	bool isPackFile(const std::string& filename) const;
	bool isCompressedFile(const std::string& filename) const;

	void debug_dump();

public: // properties
	std::string get_Filename() const;
	void set_Filename(const std::string& newval);

	std::string get_MD5() const;
	void set_MD5(const std::string& newval);

	std::string get_Directory() const;
	void set_Directory(const std::string& newval);

	std::string get_ServerFile() const;
	void set_ServerFile(const std::string& newval);

	bool get_Register() const;
	void set_Register(bool newval);
	void set_Register(const std::string& newval);

	size_t get_ServerFileSize() const;
	void set_ServerFileSize(size_t newval);
	void set_ServerFileSize(const std::string& newval);

	bool get_Required() const;
	void set_Required(bool newval);
	void set_Required(const std::string& newval);

	std::string get_Command() const;
	void set_Command(const std::string& newval);

private: // methods
	std::string fileNameOnly(const std::string& fileName) const;

	bool boolFromYesNo(const std::string& value) const;
	std::string boolToYesNo(bool value) const;

private: // data
	// fields
	std::string m_rawLine;
	std::string m_fldFilename;
	std::string m_fldMD5;
	std::string m_fldDirectory;
	std::string m_fldServerFile;
	std::string m_fldRegister;
	std::string m_fldServerFileSize;
	std::string m_fldRequired;
	std::string m_fldCommand;
};
