///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

// Flash Script Access Mode (https://helpx.adobe.com/flash/kb/control-access-scripts-host-web.html)
enum class FlashScriptAccessMode {
	Auto = -1, // For backward compatibility - FlashControlServer determines it based on YouTubeVideoId param - remove and default to "Never" after 12/31/2016
	Never = 0, // AllowScriptAccess=never
	Domain = 1, // AllowScriptAccess=sameDomain
	Always = 2, // AllowScriptAccess=always
};

} // namespace KEP
