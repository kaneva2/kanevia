///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


/******************************************************************************
Wrapper for version specific Lua support based on LUA_SELECTION:
1 = v5.1.4 - include path: \Tools\Lua\include
2 = v5.1.5 (last 5.1 lua before 5.2) - include path: \Tools\Lua-5.1.5\include
> 3 = v5.1.5-coco (last 5.1 + coco lua before 5.2) - include path: \Tools\Lua-5.1.5-coco\include
4 = v5.2.2 - include path: \Tools\Lua-5.1.5-coco\include
******************************************************************************/
#pragma once

#include "ScriptGlueCommon.h"
#include "ScriptAPITypes.h"
#include <string>

#ifndef LUA_SELECTION
#define LUA_SELECTION 3 // default lua version selection (1=v5.1.4, 2=v5.1.5, 3=v5.1.5-coco)
#endif

#ifndef LUA_CJSON
#define LUA_CJSON 1 // default lua-cjson support selection (0=off 1=on)
#endif

extern "C" {
#if (LUA_SELECTION == 1)
#include "Lua\include\lua.h"
#include "Lua\include\lualib.h"
#include "Lua\include\lauxlib.h"
#if (LUA_CJSON == 1)
#include "lua-cjson\lua_cjson.h" // cjson.dll exports
#endif
#elif (LUA_SELECTION == 2)
#include "Lua-5.1.5\include\lua.h"
#include "Lua-5.1.5\include\lualib.h"
#include "Lua-5.1.5\include\lauxlib.h"
#if (LUA_CJSON == 1)
#include "lua-cjson-5.1.5\lua_cjson.h" // cjson.dll exports
#endif
#elif (LUA_SELECTION == 3)
#include "Lua-5.1.5-coco\include\lua-coco\lua.h"
#include "Lua-5.1.5-coco\include\lua-coco\lualib.h"
#include "Lua-5.1.5-coco\include\lua-coco\lauxlib.h"
#include "Lua-5.1.5-coco\include\lua-coco\lcoco.h"
#if (LUA_CJSON == 1)
#include "lua-cjson-5.1.5-coco\include\lua-cjson\lua_cjson.h" // cjson.dll exports
#endif
#elif (LUA_SELECTION == 4)
#include "Lua-5.2.2\include\lua.h"
#include "Lua-5.2.2\include\lualib.h"
#include "Lua-5.2.2\include\lauxlib.h"
#if (LUA_CJSON == 1)
#include "lua-cjson-5.2.2\lua_cjson.h" // cjson.dll exports
#endif
#endif

#ifndef LUACOCO_VERSION
#define lua_newcthread(L, n) lua_newthread(L) // Added lua_newcthread macro for compatibility with coco
#endif
}

// Lua compatibility information
#define LEGACY_SCRIPT_LUA_COMPAT_LEVEL 500 // Legacy hosted apps: Lua 5.0 compatibility
#define CURRENT_SCRIPT_LUA_COMPAT_LEVEL LUA_VERSION_NUM // All other scripts: Use current LUA language version

// Script index file (storing a list of script hashes, named as .lua so that it will be deployed with the same pak file as real Lua scripts)
#define SCRIPT_INDEX_FILE "scriptinfo.lua"

// Returns installed flash version.
std::string LuaVersion();

// Returns true if user is an authorized script developer (has valid certificate for scripting).
bool LuaIsAuthorizedDeveloper();

// Replace luaL_loadfile to handle signed and/or encrypted script and etc
int LuaLoadFileProtected(lua_State* L, const char* filePath, bool allowDynamicScript);

// Lua script index file
bool LuaLoadScriptInfo(const std::string& directory);

// Lua 5.1 Wrappers
#define LUA_OPEN() luaL_newstate()
#define LUA_GARBAGE_COLLECT(state) lua_gc(state, LUA_GCCOLLECT, 0)
#define LUA_GARBAGE_COLLECT_STOP(state) lua_gc(state, LUA_GCSTOP, 0)
#define LUA_GARBAGE_COLLECT_RESTART(state) lua_gc(state, LUA_GCRESTART, -1)

inline void LUA_OPENLIB(lua_State* L, int (*func)(lua_State*), const char* libName = NULL) {
#if (LUA_VERSION_NUM >= 502)
	luaL_requiref(L, libName ? libName : "__global", func, libName != NULL);
#else
	libName;
	lua_pushcfunction(L, func);
	lua_call(L, 0, 0);
#endif
}

inline void LUA_OPENLIBS(lua_State* L, bool restrict) {
	// Stop Garbage Collection During Initialization
	LUA_GARBAGE_COLLECT_STOP(L);

	// All luaopen_*() Calls Must Be Done From Inside Lua
	LUA_OPENLIB(L, luaopen_base);
#if (LUA_VERSION_NUM >= 502) // In Lua 5.2 coroutine library has to be loaded separately
	LUA_OPENLIB(L, luaopen_coroutine, LUA_COLIBNAME);
#endif
	if (!restrict)
		LUA_OPENLIB(L, luaopen_io, LUA_IOLIBNAME);
	LUA_OPENLIB(L, luaopen_string, LUA_STRLIBNAME);
	LUA_OPENLIB(L, luaopen_math, LUA_MATHLIBNAME);
	LUA_OPENLIB(L, luaopen_table, LUA_TABLIBNAME);
	if (!restrict)
		LUA_OPENLIB(L, luaopen_package, LUA_LOADLIBNAME); // allows 'requires'
	LUA_OPENLIB(L, luaopen_debug, LUA_DBLIBNAME);

#if (LUA_CJSON == 1)
	// Open CJSON 'Safe' Library (cjson.dll)
	luaopen_cjson_safe(L);

	// Register Global "cjson" Table
	// library table is left in the stack by previous luaopen_cjson_safe() call
	lua_setglobal(L, "cjson");
#endif

	// Restart Garbage Collection
	LUA_GARBAGE_COLLECT_RESTART(L);
}

// Simplified version of luaL_openlib/luaL_register (works in Lua 5.0-5.2 and supports upvalues)
// NOTES: no LOADED checks, checking by global name only; no "." delimited library paths, single name only
inline void LUA_REGISTER(lua_State* L, const char* libname, const luaL_Reg* l, int nup) {
#if (LUA_VERSION_NUM == 500 || LUA_VERSION_NUM == 501 && defined(LUA_COMPAT_OPENLIB))
	// Use the standard
	luaL_openlib(L, libname, l, nup);

#else
	// Simplified version of luaL_pushmodule()
	if (libname) {
		// get library table if already exists
		lua_getglobal(L, libname);
		if (!lua_istable(L, -1)) {
			lua_pop(L, 1);
			// creates library table in global
			lua_newtable(L);
			lua_pushvalue(L, -1);
			lua_setglobal(L, libname);
		}
		// push library table below any upvalues
		lua_insert(L, -(nup + 1));
	} else {
		// if libname is NULL, the table is expected in the stack, below any upvalues
	}

#if (LUA_VERSION_NUM == 501)
	// From luaI_openlib() in Lua 5.1
	for (; l && l->name; l++) {
		int i;
		for (i = 0; i < nup; i++) /* copy upvalues to the top */
			lua_pushvalue(L, -nup);
		lua_pushcclosure(L, l->func, nup);
		lua_setfield(L, -(nup + 2), l->name);
	}
	lua_pop(L, nup); /* remove upvalues */

#elif (LUA_VERSION_NUM >= 502)
	// From luaL_openlib() in Lua 5.2
	if (l)
		luaL_setfuncs(L, l, nup);
	else
		lua_pop(L, nup); /* remove upvalues */

#endif

	if (libname) {
		// Remove library table from stack
		lua_pop(L, 1);
	}
#endif
}

class ScriptArgManager : public IScriptArgManager {
public:
	ScriptArgManager(lua_State* pLuaState) :
			m_pLuaState(pLuaState) {}

	lua_State* GetLuaState() {
		return m_pLuaState;
	}

	virtual std::string GetCallInfo() {
		std::ostringstream strm;
		const char* pszCallerFunction = "UNKNOWN";
		const char* pszCalledFunction = "UNKNOWN";
		const char* pszSource = "UNKNOWN";
		int iLineNum = -1;
		lua_Debug ld0, ld1;
		if (lua_getstack(m_pLuaState, 0, &ld0) && lua_getinfo(m_pLuaState, "n", &ld0)) {
			if (ld0.name)
				pszCalledFunction = ld0.name;
		}
		if (lua_getstack(m_pLuaState, 1, &ld1) && lua_getinfo(m_pLuaState, "nSl", &ld1)) {
			if (ld1.source)
				pszSource = ld1.source;
			iLineNum = ld1.currentline;
			if (ld1.name)
				pszCallerFunction = ld1.name;
		}
		const char* pszLineBegin = pszSource;
		const char* pszLineEnd = pszLineBegin;
		for (int i = 0; i < iLineNum; ++i) {
			pszLineBegin = pszLineEnd;
			if (*pszLineEnd == '\n')
				++pszLineEnd;
			while (*pszLineEnd != '\n' && *pszLineEnd != 0)
				++pszLineEnd;
		}
		std::string strSource(pszLineBegin, pszLineEnd);
		if (!strSource.empty())
			pszSource = strSource.c_str();

		strm << pszSource << ":" << iLineNum << " " << pszCallerFunction << " calling C function " << pszCalledFunction;
		return strm.str();
	}

	virtual bool HasArgAtIndex(size_t idxArg) override {
		return idxArg + 1 <= lua_gettop(m_pLuaState);
	}

	virtual bool PushArgAtIndex(size_t idxArg) override {
		if (idxArg + 1 > lua_gettop(m_pLuaState))
			return false;
		lua_pushvalue(m_pLuaState, idxArg + 1);
		return true;
	}

	virtual bool PushTableElem(const char* pszKey) override {
		if (!lua_istable(m_pLuaState, -1))
			return false;
		lua_getfield(m_pLuaState, -1, pszKey);
		if (lua_isnil(m_pLuaState, -1)) {
			// Key does not exist.
			lua_pop(m_pLuaState, 1);
			return false;
		}
		return true;
	}

	virtual bool PushArrayElem(size_t idxElem) override {
		if (!lua_istable(m_pLuaState, -1))
			return false;
		lua_rawgeti(m_pLuaState, -1, idxElem + 1);
		return true;
	}

	virtual bool Pop() {
		lua_pop(m_pLuaState, 1);
		return true;
	}

	virtual eValueType GetValueType() {
		switch (lua_type(m_pLuaState, -1)) {
			default:
			case LUA_TNIL:
				return eValueType::None;
			case LUA_TNUMBER:
				return eValueType::Double;
			case LUA_TBOOLEAN:
				return eValueType::Bool;
			case LUA_TSTRING:
				return eValueType::String;
			case LUA_TTABLE:
				return eValueType::Table;
			case LUA_TLIGHTUSERDATA:
				return eValueType::Pointer;
		}
	}

	virtual bool GetValue(ScriptValue* pValue) override {
		switch (lua_type(m_pLuaState, -1)) {
			default:
				return false;
			case LUA_TNIL:
				pValue->Clear();
				return true;
			case LUA_TNUMBER:
				return GetArgAtIndex(-1, &pValue->GetValueRef<double>());
			case LUA_TBOOLEAN:
				return GetArgAtIndex(-1, &pValue->GetValueRef<bool>());
			case LUA_TSTRING:
				return GetArgAtIndex(-1, &pValue->GetValueRef<std::string>());
			case LUA_TLIGHTUSERDATA:
				return GetArgAtIndex(-1, &pValue->GetValueRef<void*>());
			case LUA_TTABLE: {
				std::vector<ScriptValue> array;
				std::vector<double> aIndexes; // For sparse arrays. Convert to map at the end.
				std::map<std::string, ScriptValue> map;
				bool bSucceeded = true;
				lua_pushnil(m_pLuaState);
				while (lua_next(m_pLuaState, -2)) {
					ScriptValue tableValue;
					bool bGotValue = GetValue(&tableValue);
					if (bGotValue) {
						if (lua_isnumber(m_pLuaState, -2)) {
							double dKey = lua_tonumber(m_pLuaState, -2);
							array.push_back(std::move(tableValue));
							aIndexes.push_back(dKey);
						} else if (lua_isstring(m_pLuaState, -2)) {
							std::string strKey;
							if (GetArgAtTemplate(-2, &strKey))
								map.insert(std::make_pair(std::move(strKey), std::move(tableValue)));
						} else {
							bSucceeded = false; // Bad key
						}
					} else {
						bSucceeded = false;
					}

					lua_pop(m_pLuaState, 1);
				}
				bool bConvertArrayToMap = false;
				if (!map.empty()) {
					bConvertArrayToMap = true;
				} else {
					for (size_t i = 0; i < aIndexes.size(); ++i) {
						if (aIndexes[i] != i + 1) {
							bConvertArrayToMap = true;
							break;
						}
					}
				}
				if (bConvertArrayToMap) {
					for (size_t i = 0; i < array.size(); ++i) {
						lua_pushnumber(m_pLuaState, aIndexes[i]);
						lua_tostring(m_pLuaState, -1);
						std::string strKey;
						GetArgAtTemplate(-1, &strKey);
						lua_pop(m_pLuaState, 1);
						map.insert(std::make_pair(std::move(strKey), std::move(array[i])));
					}
					array.clear();
				}
				if (array.empty())
					pValue->GetValueRef<std::map<std::string, ScriptValue>>() = std::move(map);
				else
					pValue->GetValueRef<std::vector<ScriptValue>>() = std::move(array);
				return bSucceeded;
			}
		}
	}

	virtual bool GetValue(bool* pValue) override {
		return GetArgAtTemplate(-1, pValue);
	}

	virtual bool GetValue(float* pValue) override {
		return GetConvertedArgAtTemplate<double>(-1, pValue);
	}

	virtual bool GetValue(double* pValue) override {
		return GetArgAtTemplate(-1, pValue);
	}

	virtual bool GetValue(int32_t* pValue) override {
		return GetArgAtTemplate(-1, pValue);
	}

	virtual bool GetValue(std::string* pValue) override {
		return GetArgAtTemplate(-1, pValue);
	}

	virtual bool GetPointer(void** pValue) override {
		return GetArgAtTemplate(-1, pValue);
	}

	virtual bool GetArraySize(size_t* pSize) {
		if (!lua_istable(m_pLuaState, -1))
			return false;
		*pSize = lua_objlen(m_pLuaState, -1);
		return true;
	}

	virtual bool PushValue(const ScriptValue& value) override {
		switch (value.GetType()) {
			default:
				return false;
			case eValueType::None:
				lua_pushnil(m_pLuaState);
				return true;
			case eValueType::Int:
				return PushValue(value.GetValueRef<int>());
			case eValueType::Double:
				return PushValue(value.GetValueRef<double>());
			case eValueType::String:
				return PushValue(value.GetValueRef<std::string>());
			case eValueType::Array: {
				const std::vector<ScriptValue>& array = value.GetValueRef<std::vector<ScriptValue>>();
				if (!PushNewArray(array.size()))
					return false;
				for (size_t i = 0; i < array.size(); ++i) {
					PushValue(array[i]);
					SetArrayElem(i);
				}
				return true;
			}
			case eValueType::Table: {
				const std::map<std::string, ScriptValue>& map = value.GetValueRef<std::map<std::string, ScriptValue>>();
				if (!PushNewTable(map.size()))
					return false;
				for (auto& mapEntry : map) {
					PushKey(mapEntry.first.c_str());
					PushValue(mapEntry.second);
					AddTableElem();
				}
				return true;
			}
		}
	}
	virtual bool PushValue(nullptr_t) override {
		lua_pushnil(m_pLuaState);
		return true;
	}
	virtual bool PushValue(bool value) override {
		lua_pushboolean(m_pLuaState, value ? 1 : 0);
		return true;
	}
	virtual bool PushValue(int32_t value) override {
		lua_pushinteger(m_pLuaState, value);
		return true;
	}
	virtual bool PushValue(float value) override {
		lua_pushnumber(m_pLuaState, value);
		return true;
	}
	virtual bool PushValue(double value) override {
		lua_pushnumber(m_pLuaState, value);
		return true;
	}
	virtual bool PushValue(const char* value) override {
		lua_pushstring(m_pLuaState, value);
		return true;
	}
	virtual bool PushValue(const std::string& value) override {
		lua_pushstring(m_pLuaState, value.c_str());
		return true;
	}
	virtual bool PushPointer(void* value) override {
		lua_pushlightuserdata(m_pLuaState, value);
		return true;
	}
	virtual bool PushNewTable(size_t nElements) override {
		lua_createtable(m_pLuaState, 0, nElements);
		return true;
	}
	virtual bool PushKey(const char* pszKey) {
		lua_pushstring(m_pLuaState, pszKey);
		return true;
	}
	virtual bool PushNewArray(size_t nElements) override {
		lua_createtable(m_pLuaState, nElements, 0);
		return true;
	}
	virtual bool AddTableElem() override {
		lua_settable(m_pLuaState, -3);
		return true;
	}
	virtual bool SetArrayElem(size_t idx) override {
		lua_rawseti(m_pLuaState, -2, idx + 1);
		return true;
	}

private:
	bool GetArgAtIndex(int idx, bool* pValue) {
		if (!lua_isboolean(m_pLuaState, idx))
			return false;
		*pValue = lua_toboolean(m_pLuaState, idx) != 0;
		return true;
	}

	bool GetArgAtIndex(int idx, double* pValue) {
		double value = lua_tonumber(m_pLuaState, idx);
		if (value != 0 || lua_isnumber(m_pLuaState, idx)) {
			*pValue = value;
			return true;
		}
		return false;
	}

	bool GetArgAtIndex(int idx, int32_t* pValue) {
		double value = lua_tonumber(m_pLuaState, idx);
		if (value != 0 || lua_isnumber(m_pLuaState, idx)) {
			if (value < 0)
				*pValue = int32_t(value);
			else
				*pValue = uint32_t(value);
			return true;
		}
		return false;
	}

	bool GetArgAtIndex(int idx, std::string* pValue) {
		if (!lua_isstring(m_pLuaState, idx))
			return false;
		const char* p = lua_tostring(m_pLuaState, idx);
		pValue->assign(p, lua_strlen(m_pLuaState, idx));
		return true;
	}

	bool GetArgAtIndex(int idx, void** pValue) {
		if (lua_islightuserdata(m_pLuaState, idx)) {
			*pValue = lua_touserdata(m_pLuaState, idx);
			return true;
		}
		return false;
	}

	template <typename T>
	bool GetTableElemAtTemplate(const char* pszKey, T* pValue) {
		if (!lua_istable(m_pLuaState, -1))
			return false;
		lua_getfield(m_pLuaState, -1, pszKey);
		bool bResult = GetArgAtTemplate(-1, pValue);
		lua_pop(m_pLuaState, 1);
		return bResult;
	}

	template <typename T>
	bool GetArrayElemAtTemplate(size_t idxElem, T* pValue) {
		if (!lua_istable(m_pLuaState, -1))
			return false;
		lua_rawgeti(m_pLuaState, -1, idxElem + 1);
		bool bResult = GetArgAtTemplate(-1, pValue);
		lua_pop(m_pLuaState, 1);
		return bResult;
	}

	template <typename T>
	bool GetArgAtTemplate(int idx, T* pValue) {
		return GetArgAtIndex(idx, pValue);
	}

	template <typename TConvertedFrom, typename TConvertedTo>
	bool GetConvertedArgAtTemplate(int idx, TConvertedTo* pValue) {
		TConvertedFrom from;
		if (!GetArgAtTemplate(idx, &from))
			return false;
		*pValue = from;
		return true;
	}

private:
	lua_State* m_pLuaState;
};

template <typename T>
struct StdFunctionType {};
template <typename ClassType, typename ReturnType, typename... Args>
struct StdFunctionType<ReturnType (ClassType::*)(Args...) const> {
	typedef std::function<ReturnType(Args...)> type;
};

struct LuaCFunction {
	LuaCFunction() :
			m_pszFunctionName(nullptr), m_pFunctionAddress(nullptr), m_iFunctionUpValue(0) {}
	LuaCFunction(const char* pszFunctionName, lua_CFunction pFunctionAddress, lua_Integer iUpValue = 0) :
			m_pszFunctionName(pszFunctionName), m_pFunctionAddress(pFunctionAddress), m_iFunctionUpValue(iUpValue) {}
	template <typename FunctionType>
	LuaCFunction(const char* pszFunctionName, FunctionType&& function);

	const char* m_pszFunctionName;
	lua_CFunction m_pFunctionAddress;
	lua_Integer m_iFunctionUpValue;
};

template <typename FunctionReturnType, typename... FunctionArgs>
inline LuaCFunction CreateLuaCFunctionFromStdFunction(const char* pszFunctionExportName, std::function<FunctionReturnType(FunctionArgs...)>&& f) {
	typedef std::function<FunctionReturnType(FunctionArgs...)> FunctionType;
	static std::map<std::string, lua_Integer> s_mapFunctionToIndex;
	static std::vector<FunctionType> s_aFunctions;
	struct Local {
		static lua_Integer CallFunctionFromScript(lua_State* pLuaState) {
			ScriptArgManager sam(pLuaState);
			lua_Integer idx = lua_tointeger(pLuaState, lua_upvalueindex(1));
			if (idx >= s_aFunctions.size()) {
				// This should not be possible.
				return 0;
			}
			FunctionType pFunc = s_aFunctions[idx];
			int nReturnValues = 0;
			int iOldTop = lua_gettop(pLuaState);
			if (CallScriptFunctionWithArgs<std::function<FunctionReturnType(FunctionArgs...)>>(&sam, pFunc)) {
				if (isScriptAPIRetType<FunctionReturnType, ScriptAPIType::BLOC>::value) {
					// A blocking function always return a BLOC value even if it's not yielding due to validation failure.
					// Check LUA VM status to see if lua_yield is called. (It would be easier if we can access the return value here).
					auto luaStatus = lua_status(pLuaState);
					if (luaStatus != 0) {
						return luaStatus; // yielded or erred
					}
					// If luaState is 0, LUA VM is not yielded. Continue with normal API completion logic.
				}

				int iNewTop = lua_gettop(pLuaState);
				nReturnValues = iNewTop - iOldTop; // Assume everything left on the stack is meant to be returned.
			}
			return nReturnValues;
		}
	};

	// Get function's index in array. Add to array if not already there.
	lua_Integer idx;
	std::string strFunctionExportName(pszFunctionExportName);
	auto itr = s_mapFunctionToIndex.find(strFunctionExportName);
	if (itr != s_mapFunctionToIndex.end()) {
		idx = itr->second;
	} else {
		idx = s_aFunctions.size();
		s_aFunctions.push_back(std::move(f));
		itr = s_mapFunctionToIndex.insert(std::make_pair(strFunctionExportName, idx)).first;
	}

	return LuaCFunction(pszFunctionExportName, &Local::CallFunctionFromScript, idx);
}

template <typename ReturnType, typename... Args>
inline LuaCFunction CreateLuaCFunction(const char* pszFunctionExportName, ReturnType (*pFunction)(Args...)) {
	std::function<ReturnType(Args...)> stdFunction(pFunction);
	return CreateLuaCFunctionFromStdFunction(pszFunctionExportName, std::move(stdFunction));
}

template <typename ClassType, typename FunctionPointerType, typename ReturnType, typename... Args>
inline LuaCFunction CreateLuaCFunctionFromMemberFunction(const char* pszFunctionExportName, FunctionPointerType pMemberFunction) {
	std::function<ReturnType(Args...)> stdFunction = [pMemberFunction](Args... args) {
		if (!GetGlueGlobal<ClassType>()) {
			std::stringstream strm;
			strm << "Script global for function type " << typeid(decltype(pMemberFunction)).name() << " was not initialized.";
			throw std::exception(strm.str().c_str());
		}
		return (GetGlueGlobal<ClassType>()->*pMemberFunction)(args...);
	};
	return CreateLuaCFunctionFromStdFunction(pszFunctionExportName, std::move(stdFunction));
}

template <typename ClassType, typename ReturnType, typename... Args>
inline LuaCFunction CreateLuaCFunction(const char* pszFunctionExportName, ReturnType (ClassType::*pMemberFunction)(Args...)) {
	return CreateLuaCFunctionFromMemberFunction<ClassType, ReturnType (ClassType::*)(Args...), ReturnType, Args...>(pszFunctionExportName, pMemberFunction);
}

template <typename ClassType, typename ReturnType, typename... Args>
inline LuaCFunction CreateLuaCFunction(const char* pszFunctionExportName, ReturnType (ClassType::*pMemberFunction)(Args...) const) {
	return CreateLuaCFunctionFromMemberFunction<ClassType, ReturnType (ClassType::*)(Args...) const, ReturnType, Args...>(pszFunctionExportName, pMemberFunction);
}

template <typename ReturnType, typename... Args>
inline LuaCFunction CreateLuaCFunction(const char* pszFunctionExportName, std::function<ReturnType(Args...)> stdFunction) {
	return CreateLuaCFunctionFromStdFunction(pszFunctionExportName, std::move(stdFunction));
}

template <typename LambdaType>
inline LuaCFunction CreateLuaCFunction(const char* pszFunctionExportName, const LambdaType& lambda) {
	typename StdFunctionType<decltype(&LambdaType::operator())>::type stdFunction(lambda);
	return CreateLuaCFunctionFromStdFunction(pszFunctionExportName, std::move(stdFunction));
}

template <typename FunctionType>
LuaCFunction::LuaCFunction(const char* pszFunctionName, FunctionType&& function) {
	*this = CreateLuaCFunction(pszFunctionName, std::forward<FunctionType>(function));
}

template <>
struct ScriptValueHandler<lua_State*> {
	static bool Get(IScriptArgManager* pScriptArgManager, lua_State** pValue) {
		*pValue = static_cast<ScriptArgManager*>(pScriptArgManager)->GetLuaState();
		return true;
	}
};

// lua_State is an implicit script argument.
template <>
struct ScriptArgTraits<lua_State*> : ScriptArgTraitsImplicit {};
