///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

/// Resource Types (packed into events, do not change)
enum class ResourceType {
	NONE = -1,
	TEXTURE = 0,
	MESH = 1,
	ANIMATION = 2,
	DYNAMIC_OBJECT = 3,
	SOUND = 4,
	EQUIPPABLE = 5,
	SKELETON = 6,
	PARTICLE = 7,

	NUM_RESOURCE_TYPES,

	// Other types that are not currently handled by IResource descendants
	// List them next to existing resource types as they are both used by DownloadManager.
	PATCHFILE = 0x100, // EXG, RLB, texture paks, etc.
	ZONECUST, // ZoneCustomizations.aspx
	CUSTOM_TEXTURE, // User textures hosted by Kaneva (media library)
	OTHER_TEXTURE, // Texture used for non-3D contents, e.g. menu, etc.
	DIRECTSHOW, // Direct show contents (playlist, video, etc)
	SCRIPT, // Action item
};

inline const char* ToString(ResourceType rt) {
	switch (rt) {
		default:
			return "Unknown";
		case ResourceType::NONE:
			return "None";
		case ResourceType::TEXTURE:
			return "Texture";
		case ResourceType::MESH:
			return "Mesh";
		case ResourceType::ANIMATION:
			return "Animation";
		case ResourceType::DYNAMIC_OBJECT:
			return "DynamicObject";
		case ResourceType::SOUND:
			return "Sound";
		case ResourceType::EQUIPPABLE:
			return "Equippable";
		case ResourceType::SKELETON:
			return "Skeleton";
		case ResourceType::PARTICLE:
			return "Particle";
		case ResourceType::PATCHFILE:
			return "Patchfile";
		case ResourceType::ZONECUST:
			return "ZoneCustomization";
		case ResourceType::CUSTOM_TEXTURE:
			return "CustomTexture";
		case ResourceType::OTHER_TEXTURE:
			return "OtherTexture";
		case ResourceType::DIRECTSHOW:
			return "DirectShow";
		case ResourceType::SCRIPT:
			return "Script";
	}
}

} // namespace KEP
