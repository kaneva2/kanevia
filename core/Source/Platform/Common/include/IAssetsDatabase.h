///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <set>
#include <functional>
#include <tcl/tree.h>
#include <wkglib/singleton.h>
#include "AssetImporterMath.h"

// forward declarations of internal engine classes
namespace KEP {

class IAssetsDatabase;
class IClassifyAssetNode;
struct TextureDef;
class CMaterialObject;
class CDeformableMesh;
class CDeformableVisList;
class CDeformableVisObj;
class CEXMeshObj;
class CAlterUnitDBObject;
class CHiarchyMesh;
typedef CHiarchyMesh CHierarchyMesh;
class CHiarcleVisList;
typedef CHiarcleVisList CHierarchyVisList;
class CHiarcleVisObj;
typedef CHiarcleVisObj CHierarchyVisObj;
class CSkeletonObject;
class RuntimeSkeleton;
class CTextureEX;
class CVertexAnimationModule;
typedef std::vector<std::string> kstringVector;

///////////////////////////////////////////////////////////////////////////////
/// KEP_ASSET_TYPE enumerates the different types of assets that
///		can be imported into the Engine
///////////////////////////////////////////////////////////////////////////////
enum KEP_ASSET_TYPE {
	KEP_STATIC_MESH = 0x00, //!< maps to CEXMeshObj	(static meshes)
	KEP_VERTEX_ANIMATION = 0x100,

	KEP_SKA_SKELETAL_OBJECT = 0x10, //!< maps to RuntimeSkeleton (meshes with skeletal animation)
	KEP_SKA_SECTION_MESHES = 0x110, //!< maps to a RuntimeSkeleton full config for all sections available
	KEP_SKA_SKINNED_MESH = 0x20, //!< maps to a RuntimeSkeleton deformable mesh
	KEP_SKA_RIGID_MESH = 0x30, //!< maps to a RuntimeSkeleton hierarchical (rigid) mesh attached to a bone
	KEP_SKA_ANIMATION = 0x40, //!< maps to RuntimeSkeleton animation data

	KEP_MATERIAL = 0x50, //!< maps to CMaterialObject
	KEP_TEXTURE = 0x60, //!< maps to CTextureEX
	KEP_SHADER = 0x70, //!< currently not supported
	KEP_LIGHT = 0x80, //!< currently not supported
	KEP_SCENE = 0xF0, //!< currently not supported: will contain scene graph information and references to other files
	//!<  in the short term we may support Ogre dotScene XML format
	KEP_UNKNOWN_TYPE = -1,
	KEP_ERROR = 0xFFFF,
};

#define BITMASK(x) ((1 << (x)) - 1)
static const int SKIMPORT_SECTIONSHIFT = 0;
static const int SKIMPORT_CONFIGSHIFT = 2;
static const int SKIMPORT_LODSHIFT = 4;
static const int SKIMPORT_ANIMSHIFT = 5;
static const int SKIMPORT_SECTIONMASK = BITMASK(2) << SKIMPORT_SECTIONSHIFT; //3
static const int SKIMPORT_CONFIGMASK = BITMASK(2) << SKIMPORT_CONFIGSHIFT; //12
static const int SKIMPORT_LODMASK = BITMASK(1) << SKIMPORT_LODSHIFT; //16
static const int SKIMPORT_ANIMMASK = BITMASK(1) << SKIMPORT_ANIMSHIFT; //32
static const int SKIMPORT_REPLACE = 0;
static const int SKIMPORT_INSERTAFTER = 1;
static const int SKIMPORT_APPENDTO = 2;

enum IMPORT_FLAGS {
	//! If a section, config or LOD index is found and these flags are set
	//!  data at that position will be replaced. Otherwise it will be inserted.
	SKELETAL_REPLACE_SECTION = SKIMPORT_REPLACE << SKIMPORT_SECTIONSHIFT,
	SKELETAL_INSERTAFTER_SECTION = SKIMPORT_INSERTAFTER << SKIMPORT_SECTIONSHIFT,
	SKELETAL_APPENDTO_SECTION = SKIMPORT_APPENDTO << SKIMPORT_SECTIONSHIFT,

	SKELETAL_REPLACE_CONFIG = SKIMPORT_REPLACE << SKIMPORT_CONFIGSHIFT,
	SKELETAL_INSERTAFTER_CONFIG = SKIMPORT_INSERTAFTER << SKIMPORT_CONFIGSHIFT,
	SKELETAL_APPENDTO_CONFIG = SKIMPORT_APPENDTO << SKIMPORT_CONFIGSHIFT,

	SKELETAL_REPLACE_LOD = SKIMPORT_REPLACE << SKIMPORT_LODSHIFT,
	SKELETAL_INSERTAFTER_LOD = SKIMPORT_INSERTAFTER << SKIMPORT_LODSHIFT,

	SKELETAL_REPLACE_ANIMATION = SKIMPORT_REPLACE << SKIMPORT_ANIMSHIFT,
	SKELETAL_INSERTAFTER_ANIMATION = SKIMPORT_INSERTAFTER << SKIMPORT_ANIMSHIFT,

	//! Additional flags for enhanced asset import -- Yanfeng - September 2008
	//! Bit 16: Is UI available
	IMP_GENERAL_NOUI = 1 << 16, // No UI available, if multiple meshes are contained in a URI, try import everything
	IMP_GENERAL_WITHUI = 0 << 16, // Display a dialog window for user to make selections of assets.
	IMP_GENERAL_UIMASK = 1 << 16,

	//! Bit 17: multi-material import
	IMP_GENERAL_SINGLESUBMESH = 0 << 17, // sub-meshes will be treated as individual meshes
	IMP_GENERAL_MULTISUBMESH = 1 << 17, // allow import and link all sub-meshes automatically
	IMP_GENERAL_SUBMESHMASK = 1 << 17,

	//! Bit 18: multi-part/slot import
	IMP_GENERAL_SINGLEPART = 0 << 18, // displays all but only allow single selection of mesh part/section
	IMP_GENERAL_MULTIPART = 1 << 18, // displays all and allows multi-selection of mesh parts/section
	IMP_GENERAL_MESHPARTMASK = 1 << 18,

	IMP_SKELETAL_SINGLESLOT = IMP_GENERAL_SINGLEPART,
	IMP_SKELETAL_MULTISLOT = IMP_GENERAL_MULTIPART,
	IMP_SKELETAL_MESHSLOTMASK = IMP_GENERAL_MESHPARTMASK,

	//! Bit 19: multi-config import
	IMP_SKELETAL_SINGLECONFIG = 0 << 19,
	IMP_SKELETAL_MULTICONFIG = 1 << 19,
	IMP_SKELETAL_CONFIGMASK = 1 << 19,

	//! Bit 20/21: multi-LOD import
	IMP_GENERAL_SINGLELOD = 0 << 20, // displays all, but only allows single selection of LOD mesh (should not combine with MULTIMESH flag)
	IMP_GENERAL_MULTILOD = 1 << 20, // displays all and allows mixed selection, import and link all selected LODs automatically
	IMP_GENERAL_LOD0ONLY = 2 << 20, // display only mesh of LOD0 but allows multi-selection if combined with MULTIMESH
	IMP_GENERAL_LODMASK = 3 << 20,

	//! Bit 22/23: mesh type selection for skeleton import
	IMP_SKELETAL_NOMESH = 0 << 22, // do not import any mesh (animation only if combined with IF_SKE_ANIM)
	IMP_SKELETAL_SKINNEDMESHES = 1 << 22, // import skinned meshes only
	IMP_SKELETAL_RIGIDMESHES = 2 << 22, // import rigid meshes only
	IMP_SKELETAL_ALLMESHES = 3 << 22, // import both skinned/rigid meshes
	IMP_SKELETAL_MESHTYPEMASK = 3 << 22,

	//! Bit 24: animation import
	IMP_SKELETAL_NOANIM = 0 << 24, // do not import animation
	IMP_SKELETAL_ANIM = 1 << 24, // import animation
	IMP_SKELETAL_ANIMMASK = 1 << 24,

	//! Bit 25: material flag for dmeshes
	IMP_SKELETAL_NODMESHMAT = 0 << 25, // do not import dmesh->m_supportedmaterials and dmesh->m_material
	IMP_SKELETAL_DMESHMAT = 1 << 25, // import dmesh->m_supportedmaterials and dmesh->m_material
	IMP_SKELETAL_DMESHMATMASK = 1 << 25,

	//! Bit 26: import orphan static meshes to skeleton
	IMP_SKELETAL_ORPHANMESHES = 1 << 26,

	//! Bit 27: create dummy bone if no bones found
	IMP_SKELETAL_USEDUMMYBONE = 1 << 27,

	//! Default flags for legacy importers
	// For KZM files: only import in one single-material sub-mesh at a time.
	IMP_STATIC_LEGACY_FLAGS = IMP_GENERAL_SINGLEPART | IMP_GENERAL_LOD0ONLY | IMP_GENERAL_SINGLESUBMESH,
	// For KAO files: import everything including skeleton, both types of meshes and animation.
	IMP_SKELETAL_LEGACY_FLAGS = IMP_SKELETAL_MULTISLOT | IMP_SKELETAL_MULTICONFIG | IMP_GENERAL_MULTILOD | IMP_GENERAL_SINGLESUBMESH | IMP_SKELETAL_ALLMESHES | IMP_SKELETAL_ANIM | IMP_SKELETAL_DMESHMAT,

	//! Import flag for textures

	//! Bit 28: power of 2 rounding
	IMP_TEXTURE_POWEROF2_ROUNDING = 1 << 28,

	//! Bit 29/30: texture size limit
	IMP_TEXTURE_SIZE_LIMIT_NONE = 0 << 29,
	IMP_TEXTURE_SIZE_LIMIT_LARGE = 1 << 29,
	IMP_TEXTURE_SIZE_LIMIT_MEDIUM = 2 << 29,
	IMP_TEXTURE_SIZE_LIMIT_SMALL = 3 << 29,
	IMP_TEXTURE_SIZE_LIMIT_MASK = 3 << 29,

	//! Bit 31: Defy all import options
	IMP_DEFY_IMPORT_OPTIONS = 1 << 31,
};

union ImportFlags {
	int intValue;
	struct
	{
		unsigned int section : 2;
		unsigned int config : 2;
		unsigned int lod : 1;
		unsigned int anim : 1;
	};
};

//class IGameDirProvider
//{
//public:
//	virtual const string PathBase() = 0;
//	virtual const string GetGameBaseDir() = 0;
//};

struct AnimImportOptions {
	std::map<std::string, ImpFloatVector3> rootOffsets;
};

class AssetData;
typedef tcl::tree<AssetData> AssetTree;

///////////////////////////////////////////////////////////////////////////////
/// Base Interface for the Asset Importer classes
///////////////////////////////////////////////////////////////////////////////
class IAssetImporter {
private:
	std::string name, desc;

public:
	IAssetImporter(const std::string& n, const std::string& d) :
			name(n), desc(d) {}
	const std::string& GetName() { return name; }
	const std::string& GetDescription() { return desc; }

	//! Registers the importer with the AssetImporterFactory and does any initialization required
	virtual void Register() = 0;

	//! Unregisters the importer, doing any necessary cleanup
	virtual void Unregister() = 0;

	//! Tries importing the file doing a best guess of its type
	//!   and then adding it to the appropriate database.
	//! \return true if succeeded
	virtual bool Import(const std::string& aFileName) { return false; }

	/** @name Import by object type.
	  * These functions try to import the specified file into the object passed
	  *   returning true upon success and false otherwise.
	  */
	virtual bool Import(
		CEXMeshObj& aStaticMesh,
		const std::string& aFileName,
		AssetData* aDef = NULL,
		int aFlags = IMP_STATIC_LEGACY_FLAGS) { return false; }

	virtual bool Import(
		CSkeletonObject& aSkeletalObject,
		const std::string& aFileName,
		AssetTree* aTree = NULL,
		int aFlags = IMP_SKELETAL_LEGACY_FLAGS) { return false; }

	virtual bool Import(
		CSkeletonObject& aSkeletalObject,
		const kstringVector& aFileNameVector,
		AssetTree* aTree = NULL,
		int aFlags = IMP_SKELETAL_LEGACY_FLAGS) { return false; }

	virtual bool Import(TextureDef& textureInfo, const std::string& fileName, AssetData* assetDef = NULL, int flags = 0) { return false; }
	virtual bool GetTextureImportInfo(TextureDef& textureInfo, const std::string& importedFileName) { return false; }

	/** @name Additional skeletal animation import functions.
	  * These functions try to import skeletal animation data of different types
	  *  to an existing skeletal animation object which is already initialized
	  * See IAssetsDatabase documentation for more details.
	  */
	virtual bool ImportSkinnedMesh(
		CSkeletonObject& aSkeletalObject,
		const std::string& aFileName,
		int aSectionIndex,
		int aConfigIndex,
		int aLodIndex,
		int flags,
		AssetData* aDef = NULL) { return false; }

	virtual bool ImportRigidMesh(
		RuntimeSkeleton& aSkeletalObject,
		const std::string& aFileName,
		int aBoneIndex,
		int aLodIndex,
		int flags,
		AssetData* aDef = NULL) { return false; }

	virtual bool ImportAnimation(
		CSkeletonObject& aSkeletalObject,
		const std::string& aURI,
		int& aAnimGLID,
		int aFlags,
		AssetData* aDef = NULL,
		const AnimImportOptions* apOptions = NULL) { return false; }

	virtual bool ImportVertexAnimation(
		CEXMeshObj& aMesh,
		CVertexAnimationModule& aAnim,
		const std::string& aFileName,
		AssetData* aDef = NULL) { return false; }

	/** @name Static helper functions for IAssetImporter implementations of animation importing functions
	  */
	//@{
	static CAlterUnitDBObject* GetSkeletonSectionPtr(CSkeletonObject& aSkeletalObject, int aSectionIndex, ImportFlags aFlags, const std::string& sectionName);

	static CDeformableMesh* GetSectionConfigPtr(CAlterUnitDBObject& aSectionPtr, int aConfigIndex, ImportFlags aFlags, const std::string& configName);

	static CDeformableMesh* GetSectionConfigPtr(CSkeletonObject& aSkeletalObject, int aSectionIndex, int aConfigIndex, ImportFlags aFlags, const std::string& sectionName, const std::string& configName);

	static void SetConfigLODMeshPtr(CDeformableVisList& aLodList, CDeformableVisObj* aLodMeshPtr, int aLodIndex, ImportFlags aFlags);

	static void SetConfigLODMeshPtr(CHierarchyVisList& aLodList, CHierarchyVisObj* aLodMeshPtr, int aLodIndex, ImportFlags aFlags);

	////////////////////////////////////////////////////////////////////////////////////
	// Start enhancement by Yanfeng September 2008 -- see ColladaImporter design document
public:
	//! \brief Preview assets contained in the provided URI, return all assets found.
	//!
	//! Preview assets contained in the provided URI, return a list of assets found
	//! This function can bring up a UI dialog to allow user to select assets if UI is allowed by flags
	virtual AssetTree* PreviewAssets(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags, IClassifyAssetNode* classifyFunc) { return NULL; }

	//! \brief Check if current importer supports specified asset URI and asset type
	//!
	//! Asset URI comes in following form: [protocol://[host]]/path/to/filename.ext[#assetname["["submeshID"]"]]
	virtual BOOL SupportAssetURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType) { return TRUE; }

	//! \brief Check if current importer is compatible with URI.
	//!
	//! Legacy importer who only supports file path should return false. Compatible importers should be able to
	//! parse the mesh name section from URI. Callers should not pass a URI to non-URI compatible importers.
	virtual BOOL IsURICompatible() { return FALSE; }

	//! \brief Check if current importer always bake vertices (identity matrix returned) for certain asset type
	virtual BOOL VerticesAlwaysBaked(KEP_ASSET_TYPE aAssetType) { return TRUE; }

	virtual bool BeginSession(const std::string& aURI, int aFlags) { return true; }
	virtual bool EndSession(const std::string& aURI) { return true; }

	//////////////////////////////////////////////////////////////////////////////
	// Added May 2009

	//! Set importer options. Return true if options set. false if options not found or value invalid.
	virtual void GetSupportedOptions(std::set<std::string>& optionNames) { optionNames.clear(); }
	virtual bool SetOption(const std::string& optionName, int intValue) { return false; }
	virtual bool GetOption(const std::string& optionName, int& intValue) { return false; }

	// End enhancement by Yanfeng
	///////////////////////////////////////////////////////////////////////////////
};

class IAssetImporterImpl;

// ! \brief IAssetImporterFactory: Registry for all importers.
// !
// ! Separated from original IAssetsDatabase by Yanfeng 09/2008. Importer Factory and Assets Database are two separate traits.
// ! They should evolve independently.
// ! IAssetImporterFactory (and all its decedents) is a singleton (while database might have multiple copies and sources).
// ! Application will decide which implementation class to instantiate. Basic implementation will at least support registration
// ! services and importer instantiation. Advanced implementation might automatically detect the existence of available
// ! importers (e.g. DLL-based blades).
class IAssetImporterFactory : public wkg::Singleton<IAssetImporterFactory> {
public:
	IAssetImporterFactory();
	virtual ~IAssetImporterFactory();

	/** @name AssetImporter Registration */
	//@{
	//! Register the asset importer with the AssetsDatabase, for a particular extension and asset type.
	//! Call this function as many times as needed for each extension and asset type supported
	virtual void RegisterImporter(IAssetImporter* aAssetImporter,
		KEP_ASSET_TYPE aAssetType, const std::string& aExtension) = 0;

	//! Unregister the asset importer with the AssetsDatabase, for a particular extension and asset type.
	//! Call this function as many times as needed to discontinue support of a particular format
	virtual void UnregisterImporter(IAssetImporter* aAssetImporter,
		KEP_ASSET_TYPE aAssetType, const std::string& aExtension) = 0;

	//! Unregister the asset importer with the AssetsDatabase.
	//! Removes all the entries that refer to this importer
	virtual void UnregisterImporter(IAssetImporter* aAssetImporter) = 0;
	//@}

	//! Return, for a particular asset type, a filter to be used in a file open dialog
	virtual void GetOpenDialogFilter(KEP_ASSET_TYPE aAssetType, std::string& aExtensionsList) = 0;
	virtual void GetOpenDialogFilter(std::set<KEP_ASSET_TYPE> aAssetTypes, std::string& aExtensionsList) = 0;

	//! Return an appropriate for the assets specified by the URI, return NULL if no matches.
	virtual IAssetImporter* GetImporter(KEP_ASSET_TYPE aAssetType, const std::string& aURI, int aFlags = 0) = 0;

	virtual void RegisterImporterImpl(IAssetImporterImpl* aImpl, const std::string& aImporterName) = 0;
	virtual void UnregisterImporterImpl(IAssetImporterImpl* aImpl, const std::string& aImporterName) = 0;
	virtual void UnregisterImporterImpl(IAssetImporterImpl* aImpl) = 0;
	virtual std::set<IAssetImporterImpl*> GetImporterImpl(IAssetImporter* aImporter) = 0;
};

class IAssetsDatabase : public wkg::Singleton<IAssetsDatabase> {
public:
	IAssetsDatabase();

	virtual ~IAssetsDatabase();

	/** @name Asset Importing
	 *	These functions attempt the import of an asset from the specified file
	 *	If there is a library to store this particular type of asset,
	 *	  the function will check if an asset with the same file name already exists
	 *	  if found it will only replace it if the "replace" parameter is true
	 *	They return a valid pointer upon success, 0 otherwise
	 */

	virtual AssetData ImportStaticMesh(
		const std::string& aFileName,
		int aFlags = IMP_STATIC_LEGACY_FLAGS,
		IAssetImporter* aImporter = NULL) = 0;

	virtual AssetData ImportStaticMesh(
		const char* const aFileName,
		int aFlags = IMP_STATIC_LEGACY_FLAGS,
		IAssetImporter* aImporter = NULL);

	virtual AssetTree* ImportStaticMeshes(
		const std::string& aFileName,
		int aFlags,
		IClassifyAssetNode* classifyFunc = NULL,
		IAssetImporter* aImporter = NULL) = 0;

	virtual AssetTree* ImportStaticMeshes(
		const char* const aFileName,
		int aFlags,
		IClassifyAssetNode* classifyFunc = NULL,
		IAssetImporter* aImporter = NULL) { return ImportStaticMeshes(std::string(aFileName ? aFileName : ""), aFlags, classifyFunc, aImporter); }

	virtual bool ImportStaticMeshesByAssetTree(
		const std::string& aFileName,
		int aFlags,
		AssetTree* assetTree,
		IAssetImporter* apImporter = NULL) = 0;

	virtual bool ImportStaticMeshByAssetDefinition(
		const std::string& aFileName,
		int aFlags,
		AssetData& aDef,
		int subID = 0,
		IAssetImporter* apImporter = NULL) = 0;

	virtual CSkeletonObject* ImportSkeletalObject(
		const std::string& aFileName,
		int aFlags = IMP_SKELETAL_LEGACY_FLAGS,
		CSkeletonObject* pDstObj = NULL,
		int dstSection = -1,
		int dstConfig = -1,
		int dstLOD = -1,
		IClassifyAssetNode* classifyFunc = NULL,
		IAssetImporter* aImporter = NULL) = 0;

	virtual CSkeletonObject* ImportSkeletalObject(
		const kstringVector& aFileNameVector,
		int aFlags = IMP_SKELETAL_LEGACY_FLAGS,
		CSkeletonObject* pDstObj = NULL,
		int dstSection = -1,
		int dstConfig = -1,
		int dstLOD = -1,
		IClassifyAssetNode* classifyFunc = NULL,
		IAssetImporter* aImporter = NULL) { return ImportSkeletalObject(aFileNameVector[0], aFlags, pDstObj, dstSection, dstConfig, dstLOD, classifyFunc, aImporter); }

	virtual CSkeletonObject* ImportSkeletalObject(
		const char* const aFileName,
		int aFlags = IMP_SKELETAL_LEGACY_FLAGS,
		CSkeletonObject* pDstObj = NULL,
		int dstSection = -1,
		int dstConfig = -1,
		int dstLOD = -1,
		IClassifyAssetNode* classifyFunc = NULL,
		IAssetImporter* aImporter = NULL) { return ImportSkeletalObject(std::string(aFileName ? aFileName : ""), aFlags, pDstObj, dstSection, dstConfig, dstLOD, classifyFunc, aImporter); }

	virtual CSkeletonObject* ImportSkeletalObjectByAssetTree(
		const std::string& aFileName,
		int aFlags,
		AssetTree* assetTree,
		IAssetImporter* apImporter = NULL) = 0;

	virtual TextureDef ImportTexture(const std::string& uri, int flags = 0) = 0;
	virtual bool GetTextureImportInfo(TextureDef& textureInfo, const std::string& importedFileName) = 0;

	/** @name Additional skeletal animation import functions.
	  * These functions try to import skeletal animation data of different types
	  *  to an existing skeletal animation object which is already initialized
	  * The functions return true upon success and false otherwise
	  */
	//! Imports a deformable mesh in the file into the given position
	//! If any *Index is < 0 or not found appending occurs at the appropriate list
	virtual bool ImportSkinnedMesh(CSkeletonObject& aSkeletalObject, const std::string& aFileName, int aSectionIndex, int aConfigIndex, int aLodIndex, int aFlags = (SKELETAL_APPENDTO_SECTION | SKELETAL_APPENDTO_CONFIG | SKELETAL_REPLACE_LOD), IAssetImporter* aImporter = NULL) = 0;

	//! Imports a rigid mesh in the file and attaches it to the bone at the given index as a
	//! rigid mesh at the given LOD. No loading occurs if the bone is not found.
	//! If aLodexIndex < 0 or not found the mesh is appended to the LOD list.
	virtual bool ImportRigidMesh(RuntimeSkeleton& aSkeletalObject, const std::string& aFileName, int aBoneIndex, int aLodIndex, int aFlags = SKELETAL_REPLACE_LOD, IAssetImporter* aImporter = NULL) = 0;

	//! Imports animation data into the given skeletal animated object
	//! Data will only be added if all the bones in the file match (by name) those in the given object
	//! If data is missing for some of the bones it will be assumed to be equal to the default
	//!  transformation for that bone
	//! If aAnimationIndex < 0 or not found the animation data is appended to the list of animations.
	virtual bool ImportAnimation(CSkeletonObject& aSkeletalObject, const std::string& aURI, int& aAnimGLID, int aFlags = SKELETAL_REPLACE_ANIMATION, const AnimImportOptions* apOptions = NULL, IAssetImporter* aImporter = NULL) = 0;

	//! Try importing one single animation using asset definition obtained previously
	virtual bool ImportAnimationByAssetDefinition(CSkeletonObject& aSkeletalObject, const std::string& aURI, int& aAnimGLID, int aFlags, AssetData& aDef, const AnimImportOptions* apOptions = NULL, IAssetImporter* apImporter = NULL) = 0;

	virtual CVertexAnimationModule* ImportVertexAnimation(CEXMeshObj& aMesh, const std::string& aFileName, int aFlags, IClassifyAssetNode* classifyFunc = NULL, IAssetImporter* aImporter = NULL) = 0;

	virtual BOOL ParseAssetName(KEP_ASSET_TYPE assetType, const std::string& name, char& type, std::string& stem, std::string& slotOrPart, std::string& dimVar, int& lod) = 0;
	virtual BOOL ParseAnimationName(const std::string& animID, std::string& animName, int& firstFrame, int& lastFrame, int& fps) = 0;
	virtual std::string MakeAnimationName(const std::string& animName, int firstFrame = 0, int lastFrame = -1, int fps = -1) = 0;

	virtual AssetTree* OpenAndPreviewURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags, IClassifyAssetNode* classifyFunc) = 0;
	virtual void CloseURI(const std::string& aURI, KEP_ASSET_TYPE aAssetType, int aFlags) = 0;

	/**
	 * Utility Functions
	 */

	//! \brief Compare two skeletons: A contains B if both A&B share a same root bone, A contains all bones as in B with same hierarchy, A might contains extra bones at the ends of limbs.
	//! \return 1 if first skeleton contains second, -1 if error found, 0 otherwise.
	virtual int CompareSkeletons(RuntimeSkeleton& first, RuntimeSkeleton& second) = 0;

	//! \brief Merge two skeletons
	virtual BOOL MergeSkeletalObjects(CSkeletonObject& toObject, CSkeletonObject& fromObject, int aSectionIndex, int aConfigIndex, unsigned int aLodIndex, int aFlags) = 0;

	//! \brief Remap bone indices in vertex assignments from old skeleton to new skeleton.
	//! \param dmesh Deformable mesh to be converted
	//! \param sourceSkeleton Source skeleton where the deformable mesh is part of.
	//! \param targetSkeleton Target skeleton where the deformable mesh is converted into.
	//! \param unmatchedBones Returns a list of unmatched bones.
	//! \return FALSE if there is unmatched bones. TRUE otherwise.
	virtual BOOL RemapDeformableMeshBones(CDeformableMesh& dmesh, RuntimeSkeleton& sourceSkeleton, RuntimeSkeleton& targetSkeleton, std::set<std::string>& unmatchedBones) = 0;
};

} // end namespace KEP

//////////////////////////////////////////////////////////////////////////////////////////////
// Predefined import options (mostly hard mechanical limits which should be set to very high values)
// Usually they don't need to be set unless we kept getting unrecoverable crash because of overly complex data.
// These options are global should only be initialized during client startup. For per import options, try using
// import flags instead (unfortunately it's only for YES/NO options but not numerical limits).

// 1. Generic
#define IMP_OPT_MAXFILESIZE "maxFileSize"
#define IMP_OPT_MAXDIMENSIONX "maxDimensionX"
#define IMP_OPT_MAXDIMENSIONY "maxDimensionY"
#define IMP_OPT_MAXDIMENSIONZ "maxDimensionZ"

// 2. 3D model related
#define IMP_OPT_MAXMESHCOUNT "maxMeshCount"
#define IMP_OPT_MAXUVSETSPERMESH "maxUVSetsPerMesh"
#define IMP_OPT_MAXTEXTURESPERMESH "maxTexturesPerMesh"
#define IMP_OPT_MAXTRICOUNT "maxTriCount"
#define IMP_OPT_MAXTRICOUNTPERSTATICMESH "maxTriCountPerStaticMesh"
#define IMP_OPT_MAXTRICOUNTPERSKINNEDMESH "maxTriCountPerSkinnedMesh"
#define IMP_OPT_MAXVERTCOUNT "maxVertCount"
#define IMP_OPT_MAXVERTCOUNTPERSTATICMESH "maxVertCountPerStaticMesh"
#define IMP_OPT_MAXVERTCOUNTPERSKINNEDMESH "maxVertCountPerSkinnedMesh"

#define IMP_OPT_MAXJOINTSPERSKELETON "maxJointsPerSkeleton"
#define IMP_OPT_MAXBONESPERSKINNEDMESH "maxBonesPerSkinnedMesh"

// 3. Animation related
#define IMP_OPT_MAXFRAMESPERANIM "maxFramesPerAnim"
#define IMP_OPT_MAXCHANNELSPERFRAME "maxChannelsPerFrame"

// 4. Image specific
// none so far

// End per-defined import options
////////////////////////////////////////////////////////////////////////////////////////////
