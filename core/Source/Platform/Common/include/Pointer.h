///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <assert.h>

namespace KEP {

template <typename TReference>
class Pointer {
public:
	/////////////////////////////
	// Constructors/Destructor //
	/////////////////////////////

	Pointer();
	Pointer(TReference* pReference);
	Pointer(const Pointer<TReference>& pointerRHS);
	~Pointer();

	//////////////////////////////////
	// Accessor Operators Operators //
	//////////////////////////////////

	TReference& operator*();
	TReference* operator->();
	const TReference& operator*() const;
	const TReference* operator->() const;

	//////////////////////////
	// Assignment Operators //
	//////////////////////////

	Pointer<TReference>& operator=(const Pointer<TReference>& pointer);

	///////////////////////
	// Boolean Operators //
	///////////////////////

	bool operator!() const;
	bool operator==(const Pointer<TReference>& pointer) const;
	bool operator!=(const Pointer<TReference>& pointer) const;
	bool operator<(const Pointer<TReference>& pointer) const;
	bool operator>(const Pointer<TReference>& pointer) const;
	bool operator<=(const Pointer<TReference>& pointer) const;
	bool operator>=(const Pointer<TReference>& pointer) const;
	explicit operator bool() const;

	///////////////////////
	// Casting Operators //
	///////////////////////

	template <typename TOtherType>
	operator TOtherType();

protected:
	//////////////////////
	// Member Variables //
	//////////////////////

	TReference* m_pReference;
}; // class..Pointer

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//
// Class Pointer Definition
//
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
//
// Constructor
// Method initializes the internal reference to 'NULL'.
//
// Parameters:
//  None.
//
// Return:
//  None.
//
template <typename TReference>
Pointer<TReference>::Pointer() :
		m_pReference(0) {
} // Pointer< TReference >::Pointer

////////////////////////////////////////////////////////////////////////////
//
// Constructor
// Method initializes the internal reference to the given pointer. If the
// given pointer is valid then it's reference count will be incremented.
//
// Parameters:
//  pReference - Pointer to the reference to be copied into this pointer.
//
// Return:
//  None.
//
template <typename TReference>
Pointer<TReference>::Pointer(TReference* pReference) :
		m_pReference(pReference) {
	if (m_pReference)
		m_pReference->AddReference();
} // Pointer< TReference >::Pointer

////////////////////////////////////////////////////////////////////////////
//
// Constructor
// Method initializes the internal reference to the given pointer. If the
// given pointer is valid then it's reference count will be incremented.
//
// Parameters:
//  None.
//
// Return:
//  None.
//
template <typename TReference>
Pointer<TReference>::Pointer(const Pointer<TReference>& pointer) :
		m_pReference(pointer.m_pReference) {
	if (m_pReference)
		m_pReference->AddReference();
} // Pointer< TReference >::Pointer

////////////////////////////////////////////////////////////////////////////
//
// Destructor
// If the internal reference pointer is valid, then it will have its
// reference count decremented.
//
// Parameters:
//  None.
//
// Return:
//  None.
//
template <typename TReference>
Pointer<TReference>::~Pointer() {
	if (m_pReference)
		m_pReference->Release();
} // Pointer< TReference >::~Pointer

////////////////////////////////////////////////////////////////////////////
//
// Operator Dereference
// Method dereferences the internal pointer to the reference object and
// returns a reference to it.
//
// Parameters:
//  None.
//
// Return:
//  A reference to the internal pointer to the reference object.
//
template <typename TReference>
TReference&
	Pointer<TReference>::operator*() {
	assert(m_pReference);
	return *m_pReference;
} // Pointer< TReference >::operator*

////////////////////////////////////////////////////////////////////////////
//
// Operator Accessor
// Method dereferences the internal pointer to the reference object and
// returns a reference to it.
//
// Parameters:
//  None.
//
// Return:
//  A reference to the internal pointer to the reference object.
//
template <typename TReference>
TReference*
	Pointer<TReference>::operator->() {
	assert(m_pReference);
	return m_pReference;
} // Pointer< TReference >::operator->

////////////////////////////////////////////////////////////////////////////
//
// Operator Dereference
// Method dereferences the internal pointer to the reference object and
// returns a reference to it.
//
// Parameters:
//  None.
//
// Return:
//  A reference to the internal pointer to the reference object.
//
template <typename TReference>
const TReference&
	Pointer<TReference>::operator*() const {
	assert(m_pReference);
	return *m_pReference;
} // Pointer< TReference >::operator*

////////////////////////////////////////////////////////////////////////////
//
// Operator Accessor
// Method dereferences the internal pointer to the reference object and
// returns a reference to it.
//
// Parameters:
//  None.
//
// Return:
//  A reference to the internal pointer to the reference object.
//
template <typename TReference>
const TReference*
	Pointer<TReference>::operator->() const {
	assert(m_pReference);
	return m_pReference;
} // Pointer< TReference >::operator->

////////////////////////////////////////////////////////////////////////////
//
// Operator Assignment
// Method assigns the given reference to this reference. All existing
// references will be decremented, while the new reference will be
// decremented.
//
// Parameters:
//  pointer - Pointer to the reference to be assigned to this pointer.
//
// Return:
//  A reference to this pointer.
//
template <typename TReference>
Pointer<TReference>&
Pointer<TReference>::operator=(const Pointer<TReference>& pointer) {
	// If the given pointer is equivalent to this pointer, then
	// there isn't a need to do anything, therefore just return
	// out of here.
	if (pointer.m_pReference == m_pReference)
		return *this;

	// If the current reference is valid, be sure to decrement it's
	// reference count before assigning the given reference.
	if (m_pReference)
		m_pReference->Release();
	m_pReference = pointer.m_pReference;
	if (m_pReference)
		m_pReference->AddReference();
	return *this;
} // Pointer< TReference >::operator=

////////////////////////////////////////////////////////////////////////////
//
// Operator Boolean-Negate
// Method returns whether the pointer is not valid.
//
// Parameters:
//  None.
//
// Return:
//  'true' if the pointer is invalid and 'false' if the pointer is valid.
//
template <typename TReference>
bool Pointer<TReference>::operator!() const {
	return !m_pReference;
} // Pointer< TReference >::operator!

////////////////////////////////////////////////////////////////////////////
//
// Operator Equivalency
// Method compares the given pointer to this pointer and returns whether
// they are equivalent.
//
// Parameters:
//  pointer - Pointer to compare with this pointer.
//
// Return:
//  'true' if the given pointer is equivalent to this pointer, otherwise
//  'false' is returned.
//
template <typename TReference>
bool Pointer<TReference>::operator==(const Pointer<TReference>& pointer) const {
	return pointer.m_pReference == m_pReference;
} // Pointer< TReference >::operator==

////////////////////////////////////////////////////////////////////////////
//
// Operator Inequivalency
// Method compares the given pointer to this pointer and returns whether
// they are inequivalent.
//
// Parameters:
//  pointer - Pointer to compare with this pointer.
//
// Return:
//  'true' if the given pointer is not equivalent to this pointer,
//  otherwise 'false' is returned.
//
template <typename TReference>
bool Pointer<TReference>::operator!=(const Pointer<TReference>& pointer) const {
	return !(pointer == *this);
} // Pointer< TReference >::operator!=

////////////////////////////////////////////////////////////////////////////
//
// Operator Less-Than
// Method returns whether this pointer is less-than the given pointer.
//
// Parameters:
//  pointer - Pointer to compare with this pointer.
//
// Return:
//  'true' if this pointer is less-than the given pointer, otherwise 'false'
//  is returned.
//
template <typename TReference>
bool Pointer<TReference>::operator<(const Pointer<TReference>& pointer) const {
	return m_pReference < pointer.m_pReference;
} // Pointer< TReference >::operator<

////////////////////////////////////////////////////////////////////////////
//
// Operator Greater-Than
// Method returns whether this pointer is greater-than the given pointer.
//
// Parameters:
//  pointer - Pointer to compare with this pointer.
//
// Return:
//  'true' if this pointer is greater-than the given pointer, otherwise
//  'false' is returned.
//
template <typename TReference>
bool Pointer<TReference>::operator>(const Pointer<TReference>& pointer) const {
	return m_pReference > pointer.m_pReference;
} // Pointer< TReference >::operator>

////////////////////////////////////////////////////////////////////////////
//
// Operator Less-Than Equal
// Method returns whether this pointer is less-than or equal to the given
// pointer.
//
// Parameters:
//  pointer - Pointer to compare with this pointer.
//
// Return:
//  'true' if this pointer is less-than or equal to the given pointer,
//  otherwise 'false' is returned.
//
template <typename TReference>
bool Pointer<TReference>::operator<=(const Pointer<TReference>& pointer) const {
	return m_pReference <= pointer.m_pReference;
} // Pointer< TReference >::operator<=

////////////////////////////////////////////////////////////////////////////
//
// Operator Greater-Than Equal
// Method returns whether this pointer is greater-than or equal to the given
// pointer.
//
// Parameters:
//  pointer - Pointer to compare with this pointer.
//
// Return:
//  'true' if this pointer is greater-than or equal to the given pointer,
//  otherwise 'false' is returned.
//
template <typename TReference>
bool Pointer<TReference>::operator>=(const Pointer<TReference>& pointer) const {
	return m_pReference >= pointer.m_pReference;
} // Pointer< TReference >::operator>=

////////////////////////////////////////////////////////////////////////////
//
// Operator Cast-Bool
// Method casts the internal reference pointer to a boolean value.
//
// Parameters:
//  None.
//
// Return:
//  'true' if the pointer is something other than '0', otherwise 'false'
//  is returned.
//
template <typename TReference>
Pointer<TReference>::operator bool() const {
	return m_pReference != nullptr;
} // Pointer< TReference >::operator bool

////////////////////////////////////////////////////////////////////////////
//
// Operator Cast
// Method casts the internal reference pointer to 'TOtherType'.
//
// Parameters:
//  None.
//
// Return:
//  The casted value of the internal reference pointer.
//
template <typename TReference>
template <typename TOtherType>
Pointer<TReference>::operator TOtherType() {
	return reinterpret_cast<TOtherType>(m_pReference);
} // Pointer< TReference >::operator TOtherType
} // namespace KEP
