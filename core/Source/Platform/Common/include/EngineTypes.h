#pragma once

// Value Is Serialized!
enum class eEngineType : int {
	None = 0,
	Server = 1,
	Client = 2,
	AI = 3,
	Editor = 4,
	DispatcherServer = 5,
	TellServer = 6,
	ScriptServer = 7,
	DispatcherClient = 8
};
