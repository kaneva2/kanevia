///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string.h>

// Unique asset path formatter: see ItemPathType.cs/getAssetPathUnique()
class UniqueAssetPathFormatter {
public:
	UniqueAssetPathFormatter(const std::string& pathFormat) :
			m_pathFormat(pathFormat) {
		// Pre-parse path format for a list of macros
		size_t ofsStart = 0;
		while (true) {
			size_t ofsOpen = pathFormat.find('{', ofsStart);
			if (ofsOpen == std::string::npos) {
				break;
			}
			size_t ofsClose = pathFormat.find('}', ofsOpen + 1);
			if (ofsClose == std::string::npos) {
				break;
			}

			if (ofsClose > ofsOpen + 1) {
				std::string macro = pathFormat.substr(ofsOpen + 1, ofsClose - ofsOpen - 1);
				if (m_macros.find(macro) == m_macros.end() && macro != "q") { // {q} - quality macro - keep as is
					m_macros.insert(std::make_pair("{" + macro + "}", MacroHelper(macro)));
				}
			}

			ofsStart = ofsClose + 1;
		}
	}

	std::string operator()(unsigned uniqueId) {
		// Format path by unique ID
		std::string path(m_pathFormat);
		for (const auto& pr : m_macros) {
			if (path.find(pr.first) != std::string::npos) {
				STLStringSubstitute(path, pr.first, pr.second(uniqueId));
			}
		}
		return path;
	}

private:
	std::string m_pathFormat;

	class MacroHelper {
	private:
		bool m_plainDecimal;
		bool m_hex;
		bool m_reverse;
		std::vector<std::pair<size_t, size_t>> m_segDefs;

	public:
		MacroHelper(const std::string& macro) :
				m_plainDecimal(true), m_hex(false), m_reverse(false) {
			assert(!macro.empty());
			m_plainDecimal = macro == "d";
			if (!m_plainDecimal) {
				// Not plain decimal - parse macro format
				assert(macro[0] == 'd' || macro[0] == 'x');
				m_hex = macro[0] == 'x'; // Number format
				m_reverse = macro.size() > 1 && macro[1] == 'r'; // Optional reverse flag
				for (size_t i = m_reverse ? 2 : 1, ofs = 0; i < macro.size(); i++) {
					assert(macro[i] >= '0' && macro[i] <= '9');
					size_t len = std::min(macro[i] - '0', 9);
					m_segDefs.push_back(std::make_pair(ofs, len)); // Segments
					ofs += len;
				}
			}
		}

		std::string operator()(unsigned uniqueTextureId) const {
			if (m_plainDecimal) {
				return std::to_string(uniqueTextureId);
			}

			char strUniqueId[20];
			memset(strUniqueId, 0, sizeof(strUniqueId));
			sprintf_s(strUniqueId, m_hex ? "%08x" : "%010d", uniqueTextureId);

			std::string sUniqueId(strUniqueId);
			if (m_reverse) {
				std::reverse(sUniqueId.begin(), sUniqueId.end());
			}

			if (m_segDefs.empty()) {
				return sUniqueId;
			}

			std::string res;
			for (const auto& pr : m_segDefs) {
				res = res + sUniqueId.substr(pr.first, pr.second);
				res = res + "/";
			}

			assert(!res.empty());

			// Trim trailing slash
			res.resize(res.size() - 1);
			return res;
		}
	};

	std::map<std::string, MacroHelper> m_macros;
};
