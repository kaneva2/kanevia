///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WebIDsHelper.h"
#include <fstream>

using namespace std;

//Utility function to generate WebIDs.lua
bool GenerateWebIDsXml(const string& webIDsPath, int gameId, const string& oauthKey, const string& oauthSecret,
	const map<string, int>& achievementIds, const map<string, int>& premiumItemIds) {
	ofstream ofs(webIDsPath, ios::trunc);
	if (ofs.fail()) {
		return false;
	}

	ofs << "GAME_INFO = {" << gameId << ", \"" << oauthKey << "\", \"" << oauthSecret << "\"}" << endl;

	//TODO: register and write badges, premium items
	ofs << "badgeNames = {";
	for (map<string, int>::const_iterator it = achievementIds.begin(); it != achievementIds.end(); ++it) {
		if (it != achievementIds.begin()) {
			ofs << ", ";
		}
		ofs << "\"" << it->first << "\"";
	}
	ofs << "}" << endl;

	ofs << "BADGES = {}" << endl;
	int idx = 1;
	for (map<string, int>::const_iterator it = achievementIds.begin(); it != achievementIds.end(); ++it, ++idx) {
		ofs << "BADGES[badgeNames[" << idx << "]] = " << it->second << endl;
	}

	ofs << "itemNames = {";
	for (map<string, int>::const_iterator it = premiumItemIds.begin(); it != premiumItemIds.end(); ++it) {
		if (it != premiumItemIds.begin()) {
			ofs << ", ";
		}
		ofs << "\"" << it->first << "\"";
	}
	ofs << "}" << endl;

	ofs << "PREMIUM_ITEMS = {}" << endl;
	idx = 1;
	for (map<string, int>::const_iterator it = premiumItemIds.begin(); it != premiumItemIds.end(); ++it, ++idx) {
		ofs << "PREMIUM_ITEMS[itemNames[" << idx << "]] = " << it->second << endl;
	}
	return true;
}
