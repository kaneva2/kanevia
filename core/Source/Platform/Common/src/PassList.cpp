///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "PassList.h"
#include <jsThread.h>

#include "GetSetStrings.h"
#include "common/include/IMemSizeGadget.h"

namespace KEP {

static ULONG bigHackorama;

ULONG& PassList::GetULONG(ULONG index) {
	if (index == 0) {
		bigHackorama = m_ids.size();
	} else {
		jsVerifyReturn(index <= m_ids.size(), bigHackorama);
		bigHackorama = m_ids[index - 1];
	}
	return bigHackorama;
}

static int passListsTlsId = 0;
static int passListCopyTlsId = 0;

/// \brief init lists
///
/// this will create a TLS entry for passlist, optionally
/// setting it to the value copied in.
///
/// \param copy if null will allocate one, otherwise, sets it to this one
void PassLists::Initialize(PassLists* copy) {
	if (passListsTlsId == 0) {
		passListsTlsId = jsThread::createThreadData();
		passListCopyTlsId = jsThread::createThreadData();
	}

	jsThread::setThreadData(passListCopyTlsId, copy == 0 ? 0 : 1);
	if (copy) {
		jsThread::setThreadData(passListsTlsId, (ULONG)copy);
	} else {
		jsThread::setThreadData(passListsTlsId, (ULONG) new PassListsVect());
	}
}

/// \brief free allocated memory
///
/// DANGER this must be called only on one thread, if you call it
/// on a thread that used the copy param on Init, it will delete it twice.
/// If you don't mind a bit of leaking, don't call this.
void PassLists::DeInitialize() {
	if (passListsTlsId != 0 && passListCopyTlsId != 0 &&
		jsThread::getThreadData(passListCopyTlsId) == 0) {
		delete (PassListsVect*)jsThread::getThreadData(passListsTlsId);
	}
}

/// \brief add or update as pass list item
///
/// \param id the pass list id from db
/// \param flags the flags about it.  see fns below
void PassLists::UpdatePassList(PassId id, UINT flags) {
	jsVerifyReturnVoid(passListsTlsId != 0);
	PassListsVect* v = (PassLists::PassListsVect*)jsThread::getThreadData(passListsTlsId);
	jsVerifyReturnVoid(v != 0);

	for (PassListsVect::iterator i = v->begin(); i != v->end(); i++) {
		if (i->first == id) {
			i->second = flags;
			return;
		}
	}
	v->push_back(PassListVectItem(id, flags));
}

/// \brief use this pass id in any zone
///
/// \return true if useAnywhere flag is set for this id
bool PassLists::UseAnywhere(PassId id) {
	jsVerifyReturn(passListsTlsId != 0, false);
	PassListsVect* v = (PassLists::PassListsVect*)jsThread::getThreadData(passListsTlsId);
	jsVerifyReturn(v != 0, false);

	for (PassListsVect::iterator i = v->begin(); i != v->end(); i++) {
		if (i->first == id) {
			return (i->second & UseAnyWhereFlag) != 0;
		}
	}
	return false;
}

void PassList::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		// Get size of base class
		GetSet::GetMemSizeofInternals(pMemSizeGadget);

		// Dynamically allocated data members
		pMemSizeGadget->AddObject(m_ids);
	}
}

BEGIN_GETSET_IMPL(PassList, IDS_PASSLIST_NAME)
END_GETSET_IMPL

} // namespace KEP