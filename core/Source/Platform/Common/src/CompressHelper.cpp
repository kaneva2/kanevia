///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <windows.h>

#include "CompressHelper.h"

#include "common\KEPUTIL\Helpers.h"

#if (COMPRESS_LZMA == 1)
// LZMA1505 - Link As DLL (requires ...\Tools\lzma1505\LzmaLib.dll)
#ifdef _DEBUG
#pragma comment(lib, "LzmaLibd.lib")
#else
#pragma comment(lib, "LzmaLib.lib")
#endif
#endif

static LogInstance("Instance");

COMPRESS_METRICS g_gzipCompress;
COMPRESS_METRICS g_gzipUncompress;

COMPRESS_METRICS g_lzmaCompress;
COMPRESS_METRICS g_lzmaUncompress;

#if (COMPRESS_GZIP == 1)
static bool gzip_EncodeFile(const std::string& filePathIn, const std::string& filePathOut) {
	// Valid File ?
	if (!FileHelper::Exists(filePathIn)) {
		LogError("Invalid File - '" << filePathIn << "'");
		return false;
	}

	// Open Input File
	FileHelper fh(filePathIn, GENERIC_READ, OPEN_EXISTING);
	if (!fh.IsOpen()) {
		LogError("FileHelper::IsOpen() FAILED - '" << filePathIn << "'");
		return false;
	}

	// Open Output File For Encode
	gzFile out;
	out = gzopen(filePathOut.c_str(), "wb6");
	if (!out) {
		LogError("gzopen() FAILED - '" << filePathOut << "'");
		return false;
	}

	// Encode File
	bool ok = true;
	int err;
	unsigned long len;
	char buf[128 * 1024] = "";
	FOREVER {
		len = fh.Rx(buf, sizeof(buf));
		if (len == 0)
			break;

		if (len != (unsigned long)gzwrite(out, buf, (unsigned int)len)) {
			LogError("gzwrite() FAILED - " << gzerror(out, &err));
			ok = false;
			break;
		}
	}

	// Close Encoded File
	if (gzclose(out) != Z_OK) {
		LogError("gzclose() FAILED");
		ok = false;
	}

	return ok;
}

static bool gzip_DecodeFile(const std::string& filePathIn, const std::string& filePathOut) {
	// File Exists?
	if (!FileHelper::Exists(filePathIn)) {
		LogError("FileHelper::Exists() FAILED - '" << filePathIn << "'");
		return false;
	}

	// Valid GZip File ?
	if (::CompressType(filePathIn) != COMPRESS_TYPE_GZIP) {
		LogError("NOT GZIP - '" << filePathIn << "'");
		return false;
	}

	// Valid GZip Header ?
	FileHelper fileGZ(filePathIn, GENERIC_READ, OPEN_EXISTING);
	if (!fileGZ.IsOpen()) {
		LogError("FileHelper::IsOpen() FAILED - '" << filePathIn << "'");
		return false;
	} else {
		unsigned char gzHdr[2] = { 0 };
		if ((fileGZ.Rx(gzHdr, 2) != 2) || (gzHdr[0] != 0x1f) || (gzHdr[1] != 0x8b)) {
			LogError("NOT GZIP HEADER - '" << filePathIn << "'");
			return false;
		}
		fileGZ.Close();
	}

	// Open Input File
	bool ok = true;
	auto pIn = gzopen(filePathIn.c_str(), "rb");
	FileHelper fileOut;
	if (!pIn) {
		LogError("gzopen() FAILED - '" << filePathIn << "'");
		ok = false;
		goto gzip_decode_file_end;
	}

	// Open Output File For Decode
	if (!fileOut.Open(filePathOut, GENERIC_WRITE, CREATE_ALWAYS)) {
		LogError("fileOut.Open() FAILED - '" << filePathOut << "'");
		ok = false;
		goto gzip_decode_file_end;
	}

	// Decode File
	char buf[128 * 1024] = "";
	FOREVER {
		// Read & Decode File
		auto len = gzread(pIn, buf, sizeof(buf));
		if (len < 0) {
			int err;
			LogError("gzread() FAILED - " << gzerror(pIn, &err));
			ok = false;
			break;
		}
		if (len == 0)
			break;

		// Write Decoded File
		if (!fileOut.Tx(buf, len)) {
			LogError("fileOut.Tx() FAILED");
			ok = false;
			break;
		}
	}

gzip_decode_file_end:

	// Close Files (normally deleted by caller)
	if (pIn && (gzclose(pIn) != Z_OK)) {
		LogError("gzclose() FAILED");
		ok = false;
	}

	return ok;
}
#endif

std::string CompressVersion() {
	std::string verStr;
#if (COMPRESS_GZIP == 1)
	StrAppend(verStr, "GZIP ");
#endif
#if (COMPRESS_LZMA == 1)
	StrAppend(verStr, "LZMA ");
#endif
	return verStr;
}

COMPRESS_TYPE CompressType(const std::string& filePath) {
	// Valid File ?
	if (filePath.empty())
		return COMPRESS_TYPE_NONE;

	// Return File Compression Type
	if (StrSameFileExt(filePath, "gz") || StrSameFileExt(filePath, "gzip"))
		return COMPRESS_TYPE_GZIP;
	else if (StrSameFileExt(filePath, "zip") || StrSameFileExt(filePath, "lz") || StrSameFileExt(filePath, "lzma") || StrSameFileExt(filePath, "7z") || StrSameFileExt(filePath, "7zip"))
		return COMPRESS_TYPE_LZMA;
	return COMPRESS_TYPE_NONE;
}

std::string CompressTypeExt(COMPRESS_TYPE compressType) {
	switch (compressType) {
		case COMPRESS_TYPE_GZIP: return ".gz";
		case COMPRESS_TYPE_LZMA: return ".lzma";
	}
	return "";
}

std::string CompressTypeExtAdd(const std::string& filePath, COMPRESS_TYPE compressType) {
	std::string filePathNew = CompressTypeExtDel(filePath);
	StrAppend(filePathNew, ::CompressTypeExt(compressType));
	return filePathNew;
}

std::string CompressTypeExtDel(const std::string& filePath) {
	return (::CompressType(filePath) != COMPRESS_TYPE_NONE) ? StrStripFileExt(filePath) : filePath;
}

bool CompressFile(const std::string& filePathIn, const std::string& filePathOut, COMPRESS_TYPE compressType) {
	// Delete Target File (just incase)
	FileHelper::Delete(filePathOut);

	// Compress To Target File
	bool ok = false;
	switch (compressType) {
#if (COMPRESS_GZIP == 1)
		case COMPRESS_TYPE_GZIP:
			ok = gzip_EncodeFile(filePathIn, filePathOut);
			break;
#endif
#if (COMPRESS_LZMA == 1)
		case COMPRESS_TYPE_LZMA:
			ok = (lzma_EncodeFile(filePathIn.c_str(), filePathOut.c_str()) == SZ_OK);
			break;
#endif
	}

	// Target File Exists ?
	return (ok && FileHelper::Exists(filePathOut));
}

bool CompressFile(const std::string& filePathIn, COMPRESS_TYPE compressType, bool cleanup) {
	// File Exists?
	if (!FileHelper::Exists(filePathIn)) {
		LogError("FileHelper::Exists() FAILED - '" << filePathIn << "'");
		return false;
	}

	// Do Nothing If Already Compressed
	if (::CompressType(filePathIn) == compressType)
		return true;

	// Add File Extension Depending On Type
	std::string filePathOut = filePathIn;
	StrAppend(filePathOut, CompressTypeExt(compressType));

	// Compress File
	Timer timer;
	bool ok = ::CompressFile(filePathIn, filePathOut, compressType);
	if (!ok) {
		LogError("CompressFile() FAILED - '" << filePathIn << "'");
	} else {
		// Update Metrics
		TimeMs timeMs = timer.ElapsedMs();
		auto inSize = FileHelper::Size(filePathIn);
		auto outSize = FileHelper::Size(filePathOut);
		COMPRESS_METRICS& cm = (compressType == COMPRESS_TYPE_GZIP) ? g_gzipCompress : g_lzmaCompress;
		cm.Add(outSize, inSize, timeMs);

#ifdef _DEBUG
		// Log Result
		std::string timeStr;
		if (timeMs)
			StrBuild(timeStr, " " << FMT_TIME << timeMs << "ms");
		LogInfo("OK (" << outSize << " bytes" << timeStr << ")"
					   << " " << cm.ToStr()
					   << " - '" << LogPath(filePathOut) << "'");
#endif
	}

	// Cleanup UnCompressed File ?
	if (cleanup) {
		if (!FileHelper::Delete(filePathIn)) {
			LogError("Delete() FAILED - '" << filePathIn << "'");
			ok = false;
		}
	}

	return ok;
}

bool UncompressFile(const std::string& filePathIn, const std::string& filePathOut) {
	// Delete Target File (just incase)
	FileHelper::Delete(filePathOut);

	// Uncompress To Target File
	bool ok = false;
	COMPRESS_TYPE type = CompressType(filePathIn);
	switch (type) {
#if (COMPRESS_GZIP == 1)
		case COMPRESS_TYPE_GZIP:
			ok = gzip_DecodeFile(filePathIn, filePathOut);
			break;
#endif
#if (COMPRESS_LZMA == 1)
		case COMPRESS_TYPE_LZMA:
			ok = (lzma_DecodeFile(filePathIn.c_str(), filePathOut.c_str()) == SZ_OK);
			break;
#endif
	}

	// Target File Exists ?
	return (ok && FileHelper::Exists(filePathOut));
}

bool UncompressFile(const std::string& filePathIn, bool cleanup) {
	// Do Nothing If Not Compressed
	auto compressType = ::CompressType(filePathIn);
	if (compressType == COMPRESS_TYPE_NONE)
		return true;

	// Uncompress File (strip file extension)
	Timer timer;
	std::string filePathOut = StrStripFileExt(filePathIn);
	bool ok = ::UncompressFile(filePathIn, filePathOut);
	if (!ok) {
		LogError("UncompressFile() FAILED - '" << filePathIn << "'");
	} else {
		// Update Metrics
		TimeMs timeMs = timer.ElapsedMs();
		auto inSize = FileHelper::Size(filePathIn);
		auto outSize = FileHelper::Size(filePathOut);
		COMPRESS_METRICS& cm = (compressType == COMPRESS_TYPE_GZIP) ? g_gzipUncompress : g_lzmaUncompress;
		cm.Add(outSize, inSize, timeMs);

#ifdef _DEBUG
		// Log Result
		std::string timeStr;
		if (timeMs)
			StrBuild(timeStr, " " << FMT_TIME << timeMs << "ms");
		LogInfo("OK (" << outSize << " bytes" << timeStr << ")"
					   << " " << cm.ToStr()
					   << " - '" << LogPath(filePathIn) << "'");
#endif
	}

	// Cleanup Compressed File ?
	if (cleanup) {
		if (!FileHelper::Delete(filePathIn)) {
			LogError("Delete() FAILED - '" << filePathIn << "'");
			ok = false;
		}
	}

	return ok;
}

bool CompressMemory(unsigned char*& pData, FileSize& dataSize, COMPRESS_TYPE compressType) {
	if (!pData || !dataSize)
		return false;

	// Save Memory To File
	static size_t extId = 0;
	std::string filePathIn;
	StrBuild(filePathIn, "CompressMem." << ++extId);
	filePathIn = PathAdd(PathApp(), filePathIn);
	FileHelper fhIn(filePathIn, GENERIC_WRITE, CREATE_ALWAYS);
	if (!fhIn.IsOpen() || (fhIn.Tx(pData, dataSize) != dataSize)) {
		LogError("FileHelper::Tx() FAILED - '" << filePathIn << "'");
		return false;
	}
	fhIn.Close();

	// Compress File (and cleanup)
	std::string filePathOut = filePathIn;
	StrAppend(filePathOut, CompressTypeExt(compressType));
	CompressFile(filePathIn, compressType, true);
	auto dataSizeOut = FileHelper::Size(filePathOut);
	if (!dataSizeOut) {
		LogError("CompressFile() FAILED - '" << filePathIn << "'");
		return false;
	}

	// Reserve Memory For Compressed Data
	auto pDataOut = new unsigned char[dataSizeOut];
	if (!pDataOut) {
		LogError("new FAILED - size=" << dataSizeOut);
		return false;
	}

	// Load Compressed File Into Memory
	FileHelper fhOut(filePathOut, GENERIC_READ, OPEN_EXISTING);
	if (!fhOut.IsOpen() || (fhOut.Rx(pDataOut, dataSizeOut) != dataSizeOut)) {
		LogError("FileHelper::Rx() FAILED - '" << filePathOut << "'");
		delete pDataOut;
		return false;
	}
	fhOut.Close();

	// Clean Up Compressed File
	FileHelper::Delete(filePathOut);

	// Swap Out Memory
	delete[] pData;
	pData = pDataOut;
	dataSize = dataSizeOut;

	return true;
}

std::string GetCompressMetrics(const std::string& runtimeId) {
	std::string str;
	StrAppend(str, "{\"collection\":\"compress_metrics\"");
	if (!runtimeId.empty())
		StrAppend(str, ",\"runtimeId\":\"" << runtimeId << "\"");
	StrAppend(str, ",\"gzipUncompress\":{");
	StrAppend(str, "\"num\":" << g_gzipUncompress.m_num);
	StrAppend(str, ",\"origBytes\":" << g_gzipUncompress.m_origSize);
	StrAppend(str, ",\"compBytes\":" << g_gzipUncompress.m_compSize);
	StrAppend(str, ",\"timeMs\":" << g_gzipUncompress.m_timeMs);
	StrAppend(str, "}");
	StrAppend(str, ",\"gzipCompress\":{");
	StrAppend(str, "\"num\":" << g_gzipCompress.m_num);
	StrAppend(str, ",\"origBytes\":" << g_gzipCompress.m_origSize);
	StrAppend(str, ",\"compBytes\":" << g_gzipCompress.m_compSize);
	StrAppend(str, ",\"timeMs\":" << g_gzipCompress.m_timeMs);
	StrAppend(str, "}");
	StrAppend(str, ",\"lzmaUncompress\":{");
	StrAppend(str, "\"num\":" << g_lzmaUncompress.m_num);
	StrAppend(str, ",\"origBytes\":" << g_lzmaUncompress.m_origSize);
	StrAppend(str, ",\"compBytes\":" << g_lzmaUncompress.m_compSize);
	StrAppend(str, ",\"timeMs\":" << g_lzmaUncompress.m_timeMs);
	StrAppend(str, "}");
	StrAppend(str, ",\"lzmaCompress\":{");
	StrAppend(str, "\"num\":" << g_lzmaCompress.m_num);
	StrAppend(str, ",\"origBytes\":" << g_lzmaCompress.m_origSize);
	StrAppend(str, ",\"compBytes\":" << g_lzmaCompress.m_compSize);
	StrAppend(str, ",\"timeMs\":" << g_lzmaCompress.m_timeMs);
	StrAppend(str, "}");
	StrAppend(str, "}");
	return str;
}
