/******************************************************************************
 KEPEditorUtil.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "stdafx.h"

#include "KEPEditorUtil.h"
#include "afxdlgs.h"

using namespace std;

namespace KEP {

bool GetMultipleFileNamesToOpen(OUT std::vector<string>& aFileNames, OUT string& aFolderPath, IN const string& aFilter, IN CWnd* pParentWnd, IN bool aMakeAbsolute) {
	CFileDialog opendialog(TRUE, 0, 0, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT, Utf8ToUtf16(aFilter).c_str(), pParentWnd);

	wchar_t fileNamesBuffer[128000] = L""; // this should be ok for at least 500 files (assuming ~256 chars per name)
	opendialog.m_ofn.lpstrFile = fileNamesBuffer;
	opendialog.m_ofn.nMaxFile = sizeof(fileNamesBuffer);

	if (opendialog.DoModal() != IDOK)
		return false;

	wchar_t* fnPtr = fileNamesBuffer;
	string path = Utf16ToUtf8(fnPtr);
	fnPtr += (path.size() + 1);
	aFileNames.clear();
	while ((*fnPtr) != 0) {
		aFileNames.push_back(Utf16ToUtf8(fnPtr));
		fnPtr += (aFileNames.back().size() + 1);
	}

	if (aFileNames.size() > 0) {
		aFolderPath = path + "\\";
		;
		if (aMakeAbsolute) {
			vector<string>::iterator ksIt(aFileNames.begin()), ksEnd(aFileNames.end());
			for (; ksIt != ksEnd; ++ksIt)
				*ksIt = aFolderPath + *ksIt;
		}
	} else {
		string::size_type count = path.rfind("\\") + 1;
		aFolderPath = path.substr(0, count);
		if (!aMakeAbsolute)
			aFileNames.push_back(path.substr(count));
		else
			aFileNames.push_back(path);
	}

	return true;
}

} // namespace KEP
