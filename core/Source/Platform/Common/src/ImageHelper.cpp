///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "ImageHelper.h"
#include "KEPHelpers.h"

#if defined(PLATFORM_TEST) || defined(_KEP_EDIT)
#define TOOLS_PATH "..\\Tools"
#elif defined(_KEP_MENU)
#define TOOLS_PATH "..\\..\\..\\Tools"
#else
#define TOOLS_PATH "..\\..\\Tools"
#endif

#if (IMAGE_SELECTION == 1)
// FreeImage-3.17.0 - Link As DLL (requires ...\Tools\FreeImage-3.17.0\x32\FreeImage.dll)
#ifdef _DEBUG
#pragma comment(lib, TOOLS_PATH "\\FreeImage-3.17.0\\x32\\FreeImaged.lib")
#else
#pragma comment(lib, TOOLS_PATH "\\FreeImage-3.17.0\\x32\\FreeImage.lib")
#endif
#endif

std::string ImageVersion() {
	std::string verStr;
#if (IMAGE_SELECTION == 1)
	StrBuild(verStr, "FreeImage " << FreeImage_GetVersion());
#else
	StrBuild(verStr, "Image Disabled");
#endif
	return verStr;
}
