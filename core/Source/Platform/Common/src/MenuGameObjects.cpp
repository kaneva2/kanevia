///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "MenuGameObjects.h"
#include "IClientEngine.h"
#include "CMovementObj.h"
#include "ReD3DX9.h"
#include "RuntimeSkeleton.h"
#include "ReMeshCreate.h"
#include "CArmedInventoryClass.h"
#include "CSkeletonObject.h"
#include "CBoneClass.h"
#include "CFrameObj.h"
#include "StreamableDynamicObj.h"
#include "DynamicObj.h"
#include "EquippableSystem.h"
#include "DynamicObjectManager.h"
#include "BoundBox.h"
#include "SkeletonConfiguration.h"
#include "CDeformableMesh.h"
#include "CHierarchyMesh.h"
#include "CEXMeshClass.h"
#include "Core/Math/Rotation.h"
#include "Core/Math/TransformUtil.h"

namespace KEP {

CMenuGameObjects* CMenuGameObjects::m_pInstance = 0;

CMenuGameObject::CMenuGameObject() {
	Init();
}

CMenuGameObject::CMenuGameObject(CMovementObj* pMO) {
	Init();
	m_pMO = pMO;
}

CMenuGameObject::CMenuGameObject(int game_object_index, GLID animGlid) {
	Init();

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	CMovementObjList* pMORML = pICE->GetMovementObjectRefModelList();
	CMovementObjList* pMOL = pICE->GetMovementObjectList();
	if (pMORML || pMOL) {
		CMovementObj* pMO = NULL;
		if (game_object_index >= 0) {
			pMO = pMORML->getObjByIndex(game_object_index);
		} else if (pMOL) {
			pMO = pMOL->getObjByIndex(0);
			if (pMO->getPosses() != NULL && pMO->getMountType() == MT_STUNT) // Stunt Override
				pMO = pMO->getPosses();
		}
		if (pMO) {
			CMovementObj* pMO_new;
			IDirect3DDevice9* d3d_device = pICE->GetD3DDevice();

			pMO->Clone(&pMO_new, d3d_device, NULL);
			pMO_new->setIdxRefModel(game_object_index);
			// note: this is a hack. Eventually we should either change charactercreation ui to use animGLID or
			// create a separate function for animation by GLID.
			if (animGlid < 0) { // hack: UGC glid is always < 0 so let's assume GLID here
				pMO_new->setOverrideAnimType(eAnimType::None);
				pMO_new->setOverrideAnimSettings(AnimSettings(animGlid));
				pMO_new->setOverrideTimeMs(fTime::TimeMs());
				pMO_new->setOverrideDuration((TimeMs)0.0);
			} else {
				pMO_new->getSkeleton()->setPrimaryAnimationByIndex(animGlid, AnimSettings());
			}

			pMO_new->getSkeleton()->applyFreeLookRotation(0.0f, 0.0f);
			pMO_new->setBaseFrame(new CFrameObj(NULL));

			pMO->getSkeleton()->CloneArmedInventoryList(pMO_new->getSkeleton(), d3d_device);

			m_pMO = pMO_new;
		}
	}
}

CMenuGameObject::CMenuGameObject(std::unique_ptr<CDynamicPlacementObj> pDPO) {
	Init();
	m_pDPO = std::move(pDPO);
}

CMenuGameObject::CMenuGameObject(CArmedInventoryProxy* pAIP) {
	Init();
	if (pAIP->GetData()) {
		CArmedInventoryObj* pAIO = pAIP->GetData();
		pAIO->Clone(&m_pAIO, NULL);
		m_pAIO->getSkeletonConfig()->RealizeDimConfig(m_pAIO->getSkeleton(), pAIO->getSkeletonConfig()->m_meshSlots);
	}
}

CMenuGameObject::~CMenuGameObject() {
	if (m_pMO) {
		m_pMO->SafeDelete();
		m_pMO = nullptr;
	}
	if (m_pDPO) {
		// shared mem
		//delete m_dynamic_obj;
		m_pDPO = nullptr;
	}
	if (m_pAIO) {
		delete m_pAIO;
		m_pAIO = nullptr;
	}
}

void CMenuGameObject::Init() {
	m_pMO = nullptr;
	m_pDPO = nullptr;
	m_pAIO = nullptr;

	m_position.Set(0, 0, 0);
	m_rotation.Set(0, 0, 0);
	m_scaling.x = m_scaling.y = m_scaling.z = 1.0;
	m_vp_position.x = m_vp_position.y = m_vp_position.z = 0;
	m_vp_dimensions.x = m_vp_dimensions.y = 100;
	m_vp_dimensions.z = 0;
	m_vp_bg_color = D3DCOLOR_ARGB(1, 0, 0, 0);
	m_vp_light_color = D3DXCOLOR(.9f, .9f, .9f, 1.0f);
	m_vp_light_ambient = D3DXVECTOR4(.6f, .6f, .6f, 1.0f);
	m_vp_light_dir = Vector3f(.2f, .2f, .7f);
	m_alwaysOnTop = false;
	m_dynObjZoomFactor = 1.0f;
	m_animTimeMs = -1.0; // normal animation (not freeze frame)
}

void CMenuGameObject::setCurrentAnimationByType(eAnimType animType, int animVer) {
	if (GetMovementObject())
		GetMovementObject()->setOverrideAnimationByType(animType, animVer, true);
}

bool CMenuGameObject::setPrimaryAnimationSettings(const AnimSettings& animSettings) {
	m_animSettings = animSettings;
	if (m_pMO)
		m_pMO->setOverrideAnimationSettings(animSettings);
	else if (m_pDPO)
		m_pDPO->setPrimaryAnimationSettings(animSettings);
	else if (m_pAIO)
		m_pAIO->setPrimaryAnimationSettings(animSettings);
	return true;
}

void CMenuGameObject::Render(Matrix44f world_matrix, bool onTop) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	auto pMO = GetMovementObject();
	auto pDPO = GetDynamicObject();

	if (((pMO && pMO->assetsLoaded()) ||
			(pDPO && pDPO->IsResourceReady()) ||
			m_pAIO) &&
		GetOnTop() == onTop) {
		// clear out the mesh caches
		MeshRM::Instance()->ResetMeshCaches(pICE->GetReDeviceState());

		// make a dummy camera world matrix
		Matrix44f camWorld;
		camWorld.MakeIdentity();

		// Save current state
		//------------------------
		Matrix44f prevViewMatrix = reinterpret_cast<Matrix44f&>(pICE->GetRenderStateManager()->m_viewMatrixUTP);
		Matrix44f prevProjMatrix = reinterpret_cast<Matrix44f&>(pICE->GetRenderStateManager()->m_projMatrixUTP);

		D3DVIEWPORT9 prevViewport;
		pICE->GetD3DDevice()->GetViewport(&prevViewport);

		// get object data
		Vector3f vppos(GetViewportPosition());
		Vector3f vpdim(GetViewportDimensions());

		// Setup camera
		//-------------
		// We use the position.xy and scale.xy to define the widget top-left corner
		double aspect = vpdim.x / vpdim.y;

		// we use a predefined fovy, we may want later some customization of this
		double fovy = 40.0 * (3.141592 / 180.0);

		// set the viewmatrix to identity
		Matrix44f newViewMatrix;
		newViewMatrix.MakeIdentity(); // set the view (camera) transform to identity
		// set the projection to a perspective matrix with the given aspect and fovy, near and far are reasonable
		Matrix44f newProjMatrix;
		//
		// 1..5000 matches the extents used by the projection matrix used to draw the background,
		// which is important if the depth buffer clear doesn't work.  This whole thing (rendering
		// with a different projection matrix et al) is only acceptable because the studio is
		// working on a new character creation zone that we need to make sure doesn't go through this
		// render path.
		//
		MakeD3DStylePerspective(fovy, aspect, 0.01f, 5000.0f, out(newProjMatrix)); // change near plane to 0.01 to allow small items rendered

		pICE->GetRenderStateManager()->SetViewMatrix(&newViewMatrix);

		pICE->GetRenderStateManager()->SetProjectionMatrix(&newProjMatrix);

		// Setup viewport to cover exactly the area covered by the widget
		//----------------
		D3DVIEWPORT9 widgetViewport;
		widgetViewport.X = DWORD(vppos.x);
		widgetViewport.Y = DWORD(vppos.y);
		widgetViewport.Width = DWORD(vpdim.x);
		widgetViewport.Height = DWORD(vpdim.y);
		widgetViewport.MinZ = 0.0f;
		widgetViewport.MaxZ = 1.0f;
		pICE->GetD3DDevice()->SetViewport(&widgetViewport);

		pICE->GetD3DDevice()->Clear(0, NULL, D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET, GetViewportBackgroundColor(), 1.0f, 0);

		if (m_pMO)
			LoadReMesh(m_pMO, aspect, fovy, newViewMatrix, newProjMatrix, camWorld);
		else if (GetDynamicObject())
			LoadReMesh(GetDynamicObject(), aspect, fovy, newViewMatrix, newProjMatrix, camWorld);
		else if (m_pAIO)
			LoadReMesh(m_pAIO, aspect, fovy, newViewMatrix, newProjMatrix, camWorld);

		D3DLIGHT9 modelLight;
		ZeroMemory(&modelLight, sizeof(D3DLIGHT9));
		modelLight.Diffuse = GetViewportLightColor();
		reinterpret_cast<Vector3f&>(modelLight.Direction) = GetViewportLightDirection();
		modelLight.Type = D3DLIGHT_DIRECTIONAL;
		Vector3f pos(0.f, 0.f, 0.f);

		pICE->GetReDeviceState()->GetRenderState()->FlushLightCache();
		pICE->GetReDeviceState()->GetRenderState()->LoadLight(&modelLight);
		pICE->GetReDeviceState()->GetRenderState()->SortLights(NULL, 0.f, NULL);
		//Ankit ----- Setting the global ambient to a value to be used by all menu game objects and then resetting them to their actual value
		const D3DXVECTOR4* globalAmbient = pICE->GetReDeviceState()->GetRenderState()->GetGlobalAmbient();
		pICE->GetReDeviceState()->GetRenderState()->SetGlobalAmbient(&m_vp_light_ambient);
		pICE->GetReDeviceState()->GetRenderState()->RenderLights(FALSE);
		pICE->GetReDeviceState()->GetRenderState()->SetGlobalAmbient((D3DXVECTOR4*)globalAmbient);

		// render the mesh caches that the Update() call affect
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_0, true);
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_4, true);
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_1, true);
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_3, true);
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_5, true);
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_6, true);
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_7, true);
		MeshRM::Instance()->RenderMeshCache(MeshRM::RMC_ID_2, true);

		// reset the ReRenderState view & proj matrix
		pICE->GetReDeviceState()->GetRenderState()->SetProjViewMatrix(reinterpret_cast<D3DMATRIX*>(&prevViewMatrix), reinterpret_cast<D3DMATRIX*>(&prevProjMatrix));

		// Set back the previous view and projection matrices
		pICE->GetRenderStateManager()->SetViewMatrix(&prevViewMatrix);

		pICE->GetRenderStateManager()->SetProjectionMatrix(&prevProjMatrix);

		// Set back the previous viewport
		pICE->GetD3DDevice()->SetViewport(&prevViewport);
	}
}

CMenuGameObjects* CMenuGameObjects::GetInstance() {
	if (!m_pInstance)
		m_pInstance = new CMenuGameObjects();
	return m_pInstance;
}

CMenuGameObjects::~CMenuGameObjects() {
	RemoveAll();
	m_pInstance = 0;
}

void CMenuGameObjects::Add(CMenuGameObject* pMGO) {
	m_game_objects.push_back(pMGO);
}

void CMenuGameObjects::Remove(CMenuGameObject* pMGO) {
	for (MenuGameObjIter iter = m_game_objects.begin(); iter != m_game_objects.end(); iter++) {
		CMenuGameObject* pMenuGameObj = *iter;
		if (pMenuGameObj == pMGO) {
			delete pMenuGameObj;
			m_game_objects.erase(iter);
			return;
		}
	}
}

void CMenuGameObjects::RemoveAll() {
	for (const auto& pMGO : m_game_objects)
		if (pMGO)
			delete pMGO;
	m_game_objects.clear();
}

void CMenuGameObject::LoadReMesh(CDynamicPlacementObj* pDPO, double aspect, double fovy, Matrix44f& newViewMatrix, Matrix44f& newProjMatrix, Matrix44f& camWorld) {
	auto pICE = IClientEngine::Instance();
	if (!pDPO || !pICE)
		return;

	Vector3f position = GetPosition();
	StreamableDynamicObject* pSDO = pDPO->GetBaseObj();
	if (!pSDO)
		return;

	BNDBOX box;
	box = pSDO->computeBoundingBox(&camWorld);

	double objWidth = box.max.x - box.min.x;
	double objHeight = box.max.y - box.min.y;
	double objDepth = box.max.z - box.min.z;
	double radius = 0.5 * sqrt(objWidth * objWidth + objHeight * objHeight + objDepth * objDepth);

	// compute exact distance at which the object would be fully visible assuming no rotation or scaling
	double distFromCam(0);
	if ((objWidth / objHeight) > aspect)
		distFromCam = objWidth / (2.0 * tan(fovy * aspect / 2.0));
	else
		distFromCam = objHeight / (2.0 * tan(fovy / 2.0));

	// we add some fraction of the radius because the compute distance does not
	//  take into account object depth range (which may change with rotations and scaling by the way)
	// this is not perfect but it works for tested objects for some range of fovs
	distFromCam += 0.65 * radius;
	distFromCam = distFromCam * GetDynamicObjZoomFactor();

	// center the object at the origin (moves its bbox's center to the origin)
	Matrix44f positional;
	positional.MakeIdentity();
	positional(3, 0) = -float(box.min.x + objWidth * 0.5);
	positional(3, 1) = -float(box.min.y + objHeight * 0.5);
	positional(3, 2) = -float(box.min.z + objDepth * 0.5);

	// apply rotation after this centering translation
	Vector3f rotation(ToRadians(GetRotation()));
	Matrix44f rotationalX, rotationalY, rotationalZ;
	ConvertRotationX(rotation.x, out(rotationalX));
	ConvertRotationY(rotation.y, out(rotationalY));
	ConvertRotationZ(rotation.z, out(rotationalZ));
	positional = positional * rotationalY;
	positional = positional * rotationalZ;
	positional = positional * rotationalX;

	// then we move the object inside away from the camera so it's fully visible
	Matrix44f moveback;
	moveback.MakeTranslation(Vector3f(position.x, position.y, distFromCam));
	m_dynObMatrix = positional * moveback;

	// set the ReRenderState view & proj matrix for vertex shaders
	pICE->GetReDeviceState()->GetRenderState()->SetProjViewMatrix(reinterpret_cast<D3DMATRIX*>(&newViewMatrix), reinterpret_cast<D3DMATRIX*>(&newProjMatrix));

	TimeMs curTimeMs = fTime::TimeMs();
	pDPO->UpdateCallback(curTimeMs);

	auto pRS = pDPO->getSkeleton();

	if (pRS) { // && pDPO->isPrimaryAnimationSettingsValid()) {
		AnimSettings animSettings;
		pDPO->getPrimaryAnimationSettings(animSettings);
		pRS->setPrimaryAnimationSettings(animSettings);

		auto animTimeMs = GetAnimTimeMs();
		pRS->updateAnimations(animTimeMs);
	}

	for (size_t i = 0; i < pSDO->getVisualCount(); i++) {
		StreamableDynamicObjectVisual* visualPtr = (StreamableDynamicObjectVisual*)pSDO->Visual(i);
		if (visualPtr) {
			// use most detailed LOD for menu game objects rendering
			ReMesh* reMesh = pDPO->GetReLODMesh(i, 0);

			if (reMesh) {
				CMaterialObject* pMaterial = pDPO->GetMaterial(i);
				if (!pMaterial)
					pMaterial = visualPtr->getMaterial();
				if (!pMaterial)
					continue;
				pMaterial->Callback();
				pMaterial->GetReMaterial()->PreLight(FALSE);

				reMesh->SetMaterial(pMaterial->GetReMaterial());
				if (pRS && pDPO->isPrimaryAnimationSettingsValid()) {
					reMesh->SetBones(pRS->isAnimUpdated() ? pRS->getBoneList()->getBoneMatrices() : nullptr);
				}

				reMesh->SetWorldMatrix(&(m_dynObMatrix));
				MeshRM::Instance()->LoadMeshCache(reMesh);
			}
		}
	}
}

void CMenuGameObject::LoadReMesh(CMovementObj* pMO, double aspect, double fovy, Matrix44f& newViewMatrix, Matrix44f& newProjMatrix, Matrix44f& camWorld) {
	auto pICE = IClientEngine::Instance();
	if (!pMO || !pICE)
		return;

	auto pRS = pMO->getSkeleton();
	if (!pRS)
		return;

	// Advance animation
	Vector3f position = GetPosition();

	// Set Override Animation Settings As Primary Animation
	auto animSettings = pMO->getOverrideAnimationSettings();
	if (animSettings.isValid())
		pRS->setPrimaryAnimationSettings(animSettings);

	// Update Menu Game Object Animation
	auto animTimeMs = GetAnimTimeMs();
	//	if (pRS->isAnimInUseValid())
	pRS->updateAnimations(animTimeMs);

	// compute object dimensions
	BNDBOX skeletonBox = pRS->getBonesBoundingBox();
	Vector3f boxSize = skeletonBox.GetSize();
	float objWidth = boxSize.X();
	float objHeight = boxSize.Y();
	float radius = 0.5 * boxSize.Length();

	// compute exact distance at which the object would be fully visible assuming no rotation or scaling
	float distFromCam(0);
	if ((objWidth / objHeight) > aspect)
		distFromCam = objWidth / (2.0 * tan(fovy * aspect / 2.0));
	else
		distFromCam = objHeight / (2.0 * tan(fovy / 2.0));

	// we add some fraction of the radius because the compute distance does not
	//  take into account object depth range (which may change with rotations and scaling by the way)
	// this is not perfect but it works for tested objects for some range of fovs
	distFromCam += 0.65 * radius;

	// center the object at the origin (moves its bbox's center to the origin)
	Matrix44f positional;
	positional.MakeTranslation(-(skeletonBox.GetMin() + 0.5f * boxSize));

	// apply scaling after the translation
	Matrix44f scaling;
	Vector3f scale(GetScaling());
	scaling.MakeScale(scale);
	positional = positional * scaling;

	// apply rotation after this centering translation
	Vector3f rotation(ToRadians(GetRotation()));
	Matrix44f rotationalX, rotationalY, rotationalZ;
	ConvertRotationX(rotation.x, out(rotationalX));
	ConvertRotationY(rotation.y, out(rotationalY));
	ConvertRotationZ(rotation.z, out(rotationalZ));
	positional = positional * rotationalY;
	positional = positional * rotationalZ;
	positional = positional * rotationalX;

	// then we move the object inside away from the camera so it's fully visible
	Matrix44f moveback;
	Vector3f positionPlusCam(position.x, position.y, position.z + distFromCam);
	moveback.MakeTranslation(positionPlusCam);
	positional = positional * moveback;

	// set the ReRenderState view & proj matrix for vertex shaders
	pICE->GetReDeviceState()->GetRenderState()->SetProjViewMatrix(reinterpret_cast<D3DMATRIX*>(&newViewMatrix), reinterpret_cast<D3DMATRIX*>(&newProjMatrix));

	// Set Minimum Animation Delay
	pRS->setAnimUpdateDelayMs(ANIM_UPDATE_DELAY_MS_MIN);

	// Update Avatar Animation
	pMO->characterAnimationManagement(pRS, true);

	// render equippable items
	std::vector<CArmedInventoryObj*> pAIOs;
	pRS->getAllEquippableItems(pAIOs);
	for (auto pAIO : pAIOs) {
		if (!pAIO)
			continue;

		auto pRS_aio = pAIO->getSkeleton();
		if (!pRS_aio || !pRS_aio->areAllMeshesLoaded())
			continue;

		// Copy Over Accessory Animation Update Delay
		const double armedDelayFactor = 2.0;
		pRS_aio->setAnimUpdateDelayMs(pRS->getAnimUpdateDelayMs() * armedDelayFactor);

		// Update Accessory Animation
		pMO->characterAnimationManagement(pRS_aio, false);

		pRS_aio->updateAnimations(animTimeMs);

		// copy over the LOD
		pRS_aio->setCurrentLOD(pRS->getCurrentLOD());

		// copy over the scale vector
		pRS_aio->setScaleVector(pRS->getScaleVector());

		Matrix44f mat = pRS->getAnimBoneMatrix(pAIO->getAttachedBoneIndex()) * positional;

		// update the CSkeleton
		if (pRS_aio->getSkinnedMeshCount() > 0)
			pRS_aio->update(mat);
	}

	// use the ReSkinMesh renderer
	pRS->update(positional);
}

void CMenuGameObject::LoadReMesh(CArmedInventoryObj* pAIO, double aspect, double fovy, Matrix44f& newViewMatrix, Matrix44f& newProjMatrix, Matrix44f& camWorld) {
	auto pICE = IClientEngine::Instance();
	if (!pAIO || !pICE)
		return;

	Vector3f position = GetPosition();

	auto pRS_aio = pAIO->getSkeleton();
	if (!pRS_aio)
		return;

	pRS_aio->computeBoundingBoxWithSkinnedMesh(pAIO->getSkeletonConfig());
	BoundBox box = pRS_aio->getLocalBoundingBox();

	double objWidth = box.getSizeX();
	double objHeight = box.getSizeY();
	double objDepth = box.getSizeZ();
	double bboxRadius = 0.5 * sqrt(objWidth * objWidth + objHeight * objHeight + objDepth * objDepth);

	// compute exact distance at which the object would be fully visible assuming no rotation or scaling
	double distFromCam(0);
	if ((objWidth / objHeight) > aspect)
		distFromCam = objWidth / (2.0 * tan(fovy * aspect / 2.0));
	else
		distFromCam = objHeight / (2.0 * tan(fovy / 2.0));

	// we add some fraction of the radius because the compute distance does not
	//  take into account object depth range (which may change with rotations and scaling by the way)
	// this is not perfect but it works for tested objects for some range of fovs
	distFromCam += 0.65 * bboxRadius;
	distFromCam = distFromCam * GetDynamicObjZoomFactor();

	// center the object at the origin (moves its bbox's center to the origin)
	Matrix44f positional;
	positional.MakeIdentity();
	positional(3, 0) = -float(box.minX + objWidth * 0.5);
	positional(3, 1) = -float(box.minY + objHeight * 0.5);
	positional(3, 2) = -float(box.minZ + objDepth * 0.5);

	// apply rotation after this centering translation
	Vector3f rotation(ToRadians(GetRotation()));
	Matrix44f rotationalX, rotationalY, rotationalZ;
	ConvertRotationX(rotation.x, out(rotationalX));
	ConvertRotationY(rotation.y, out(rotationalY));
	ConvertRotationZ(rotation.z, out(rotationalZ));
	positional = positional * rotationalY;
	positional = positional * rotationalZ;
	positional = positional * rotationalX;

	// then we move the object inside away from the camera so it's fully visible
	Matrix44f moveback;
	moveback.MakeTranslation(Vector3f(position.x, position.y, float(distFromCam)));
	m_dynObMatrix = positional * moveback;

	// set the ReRenderState view & proj matrix for vertex shaders
	pICE->GetReDeviceState()->GetRenderState()->SetProjViewMatrix(reinterpret_cast<D3DMATRIX*>(&newViewMatrix), reinterpret_cast<D3DMATRIX*>(&newProjMatrix));

	if (m_animSettings.isValid())
		pAIO->setPrimaryAnimationSettings(m_animSettings);

	pRS_aio->updateAnimations();

	pRS_aio->setCurrentLOD(0);
	pRS_aio->update(m_dynObMatrix);

	// load the static "hiarchy" meshes stored per-bone
	const CBoneList* pBOL = pRS_aio->getBoneList();
	if (pBOL) {
		for (POSITION posLoc = pBOL->GetHeadPosition(); posLoc != NULL;) {
			auto pBO = static_cast<const CBoneObject*>(pBOL->GetNext(posLoc));
			if (!pBO)
				continue;

			for (const auto& pHM : pBO->m_rigidMeshes) {
				if (!pHM)
					continue;

				CHierarchyVisObj* pHVO = pHM->GetByIndex(0);
				if (!pHVO)
					continue;

				pHVO->m_mat = pBO->m_boneFrame->m_frameMatrix = m_dynObMatrix;

				if (pHVO->m_mesh && pHVO->m_mesh->m_materialObject) {
					pHVO->m_mesh->m_materialObject->GetReMaterial()->PreLight(FALSE);

					if (pHVO->GetReMesh()) {
						pHVO->GetReMesh()->SetWorldMatrix(&pHVO->m_mat);
						pHVO->GetReMesh()->SetRadius(pHVO->m_mesh->GetBoundingRadius());
						pHVO->GetReMesh()->SetMaterial(pHVO->m_mesh->m_materialObject->GetReMaterial());
						MeshRM::Instance()->LoadMeshCache(pHVO->GetReMesh());
					}
				}
			}
		}
	}

	if (pRS_aio->getSkinnedMeshCount() == 0)
		return;

	for (int i = 0; i < pRS_aio->getSkinnedMeshCount(); i++) {
		ReMesh* pRM = pRS_aio->getMeshCacheEntry(0, i);
		if (!pRM)
			continue;

		Vector3f boxSize = pRS_aio->getLocalBoundingBox().GetSize();

		float radius = std::max(abs(boxSize.x), std::max(abs(boxSize.y), abs(boxSize.z)));
		pRM->SetBones(pBOL->getBoneMatrices());
		pRM->SetWorldMatrix(&m_dynObMatrix);
		pRM->SetRadius(radius);
		MeshRM::Instance()->LoadMeshCache(pRM);
	}
}

void CMenuGameObjects::Render(Matrix44f world_matrix, bool onTop = false) {
	for (MenuGameObjIter iter = m_game_objects.begin(); iter != m_game_objects.end(); iter++) {
		CMenuGameObject* pMGO = *iter;
		if (pMGO)
			pMGO->Render(world_matrix, onTop);
	}
}

void CMenuGameObjects::GetAllMovementObjects(std::set<CMovementObj*>& movementObjs) {
	for (MenuGameObjIter iter = m_game_objects.begin(); iter != m_game_objects.end(); iter++) {
		CMenuGameObject* pMenuGameObj = *iter;
		if (pMenuGameObj) {
			CMovementObj* pMovementObj = pMenuGameObj->GetMovementObject();
			if (pMovementObj)
				movementObjs.insert(pMovementObj);
		}
	}
}

#include "gsD3dvectorValue.h"

BEGIN_GETSET_IMPL(CMenuGameObject, IDS_MENUGAMEOBJ_NAME)
GETSET_D3DVECTOR(m_position, GS_FLAGS_DEFAULT, 0, 0, MENUGAMEOBJ, POSITION)
GETSET_D3DVECTOR(m_rotation, GS_FLAGS_DEFAULT, 0, 0, MENUGAMEOBJ, ROTATION)
GETSET_D3DVECTOR(m_scaling, GS_FLAGS_DEFAULT, 0, 0, MENUGAMEOBJ, SCALING)
GETSET_D3DVECTOR(m_vp_position, GS_FLAGS_DEFAULT, 0, 0, MENUGAMEOBJ, VP_POSITION)
GETSET_D3DVECTOR(m_vp_dimensions, GS_FLAGS_DEFAULT, 0, 0, MENUGAMEOBJ, VP_DIMENSIONS)
END_GETSET_IMPL

} // namespace KEP