///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "..\Common\include\IMemSizeGadget.h"
#include "..\Common\include\MemSizeSpecializations.h"

namespace KEP {

void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const Matrix44f& param) {
	pMemSizeGadget->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(Matrix44f));
}

void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const Vector3f& param) {
	pMemSizeGadget->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(Vector3f));
}

void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const Vector2f& param) {
	pMemSizeGadget->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(Vector2f));
}

void AddMemSize(IMemSizeGadget* const pMemSizeGadget, const PrimitiveF& param) {
	pMemSizeGadget->AddMemSize(reinterpret_cast<size_t>(&param), sizeof(PrimitiveF));
}
} // namespace KEP
