///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <GetSet.h>

static const ULONG GETSET_SERIALIZE_VERSION = 1;

namespace KEP {

static const char* const SMETADATA_TAG = "GetSetMetaData";
static const char* const ETADATUM_TAG = "MetaDatum";
static const char* const AME_TAG = "Name";
static const char* const YPE_TAG = "Type";
static const char* const LAGS_TAG = "Flags";
static const char* const EFAULT_TAG = "DefaultValue";

bool SerializeGetSetMetaData(GetSet* obj, CArchive& ar) {
	jsVerifyReturn(obj != 0, false);

	gsValues* values = 0;
	gsMetaData* md = 0;

	obj->GetValues(values, md);
	jsVerifyReturn(values != 0 && md != 0, false);
	jsAssert(values->size() == md->size());

	if (ar.IsStoring()) {
		ar << GETSET_SERIALIZE_VERSION;
		ULONG count = 0;
		ULONG i;
		for (i = 0; i < md->size(); i++) {
			if ((md->at(i).GetFlags() & (GS_FLAG_DYNAMIC_ADD | GS_FLAG_TRANSIENT)) == GS_FLAG_DYNAMIC_ADD) {
				count++;
			}
		}
		ar << count;

		for (i = 0; i < md->size(); i++) {
			if ((md->at(i).GetFlags() & (GS_FLAG_DYNAMIC_ADD | GS_FLAG_TRANSIENT)) == GS_FLAG_DYNAMIC_ADD) {
				ar << CStringA(md->at(i).GetDynamicName()) << md->at(i).GetType() << md->at(i).GetFlags();

				switch (md->at(i).GetType()) {
					case gsInt:
						ar << *(int*)md->at(i).GetDynamicDefaultValue();
						break;
					case gsDouble:
						ar << *(double*)md->at(i).GetDynamicDefaultValue();
						break;
					case gsString:
						ar << CStringA(((std::string*)md->at(i).GetDynamicDefaultValue())->c_str());
						break;
					default:
						jsAssert(false);
						ar << (int)0;
						break;
				}
			}
		}
	} else {
		ULONG version = 0;
		ar >> version;
		jsVerifyReturn(version == GETSET_SERIALIZE_VERSION, false);

		ULONG count;
		ar >> count;

		CStringA name;
		EGetSetType type;
		ULONG flags;

		for (ULONG i = 0; i < count; i++) {
			ar >> name >> type >> flags;
			switch (type) {
				case gsInt: {
					int value;
					ar >> value;
					obj->AddNewIntMember(name, value, flags);
				} break;

				case gsDouble: {
					double d;
					ar >> d;
					obj->AddNewDoubleMember(name, d, flags);
				} break;

				case gsString: {
					CStringA s;
					ar >> s;
					obj->AddNewStringMember(name, s, flags);
				} break;
				default: {
					int value;
					ar >> value;
					jsAssert(false);
				} break;
			}
		}
	}
	return true;
}

bool SerializeGetSetValues(GetSet* obj, CArchive& ar) {
	jsVerifyReturn(obj != 0, false);

	gsValues* values = 0;
	gsMetaData* md = 0;

	obj->GetValues(values, md);
	jsVerifyReturn(values != 0 && md != 0, false);
	jsAssert(values->size() == md->size());

	if (ar.IsStoring()) {
		ar << GETSET_SERIALIZE_VERSION;

		ULONG count = 0;
		ULONG i;
		for (i = 0; i < md->size(); i++) {
			if ((md->at(i).GetFlags() & (GS_FLAG_DYNAMIC_ADD | GS_FLAG_TRANSIENT)) == GS_FLAG_DYNAMIC_ADD) {
				count++;
			}
		}
		ar << count;

		for (i = 0; i < md->size(); i++) {
			if ((md->at(i).GetFlags() & (GS_FLAG_DYNAMIC_ADD | GS_FLAG_TRANSIENT)) == GS_FLAG_DYNAMIC_ADD) {
				ar << CStringA(md->at(i).GetDynamicName())
				   << md->at(i).GetType();

				switch (md->at(i).GetType()) {
					case gsInt:
						ar << *(int*)values->at(i);
						break;
					case gsDouble:
						ar << *(double*)values->at(i);
						break;
					case gsString:
						ar << CStringA(((std::string*)values->at(i))->c_str());
						break;
					default:
						jsAssert(false);
						ar << 0;
						break;
				}
			}
		}

	} else {
		ULONG version = 0;
		ar >> version;
		jsVerifyReturn(version == GETSET_SERIALIZE_VERSION, false);

		ULONG count;
		ar >> count;

		CStringA name;
		EGetSetType type;
		ULONG id = FIND_ID_NOT_FOUND;

		for (ULONG i = 0; i < count; i++) {
			ar >> name >> type;
			switch (type) {
				case gsInt: {
					int j;
					ar >> j;
					id = obj->FindIdByName(name, &type);
					if (id != FIND_ID_NOT_FOUND && type == md->at(id).GetType()) {
						// ok, we have it
						obj->SetNumber(id, (LONG)j);
					} else {
						// TODO: not found, or differnet type, log??
					}
				} break;

				case gsDouble: {
					double d;
					ar >> d;
					id = obj->FindIdByName(name, &type);
					if (id != FIND_ID_NOT_FOUND && type == md->at(id).GetType()) {
						// ok, we have it
						obj->SetNumber(id, d);
					} else {
						// TODO: not found, or differnet type, log??
					}
				} break;
				case gsString: {
					CStringA s;
					ar >> s;
					id = obj->FindIdByName(name, &type);
					if (id != FIND_ID_NOT_FOUND && type == md->at(id).GetType()) {
						// ok, we have it
						obj->SetString(id, s);
					} else {
						// TODO: not found, or differnet type, log??
					}
				} break;
				default:
					jsAssert(false);
					ar << 0;
					break;
			}
		}
	}

	return true;
}

} // namespace KEP