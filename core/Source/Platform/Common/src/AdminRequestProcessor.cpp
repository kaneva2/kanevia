/******************************************************************************
 AdminRequestProcessor.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include <js.h>
#include <jsConnection.h>
#include <KEPHelpers.h>
#include <fstream>
#include "../Server/KEPService/ServiceStrings.h"

#include "AdminRequestProcessor.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "resource.h"

using namespace KEP;

UINT AdminRequestProcessor::checkLogin(TiXmlElement* root, string& cookie) {
	// first check cookie
	if (root == 0) {
		LOG4CPLUS_ERROR(logger(), loadStr(IDS_NO_XML));
		return IDS_NO_XML;
	}

	cookie = NULL_CK(root->Attribute("cookie"));
	if (!cookie.empty()) {
		// see if it hasn't expired
		CookieMap::iterator f = m_cookies.find(cookie);
		if (f != m_cookies.end()) {
			if (f->second < time(0)) {
				cookie.clear();
				m_cookies.erase(f);
				LOG4CPLUS_INFO(logger(), loadStr(IDS_AUTH_EXPIRED));
				return IDS_AUTH_EXPIRED;
			} else {
				// bump up their time
				f->second = time(0) + m_adminTimeoutSec;
				return NO_ERROR;
			}
		} else {
			LOG4CPLUS_DEBUG(logger(), "Cookie not found" << cookie);
			cookie.clear(); // not found
		}
		// TODO: sometime clean up other expired ones
	}

	if (cookie.empty()) {
		reloadConfig();

		// better have userid/pw
		string userId = NULL_CK(root->Attribute("userId"));
		string password = NULL_CK(root->Attribute("password"));
		if (userId.empty() || password.empty()) {
			return IDS_NO_AUTH_INFO;
		} else {
			// check the credentials against the userid/pw

			// now compare against the one in the XML
			MD5_DIGEST_STR hash;
			hashString(password.c_str(), hash);

			if (::_stricmp(m_adminUser.c_str(), userId.c_str()) != 0 ||
				m_adminUserHash != hash.s) {
				LOG4CPLUS_INFO(logger(), loadStr(IDS_AUTH_DENIED));
				return IDS_AUTH_DENIED;
			}

			// they made it, or no auth turned on
			newCookie(cookie);
		}
	}

	return NO_ERROR;
}

void AdminRequestProcessor::newCookie(string& cookie) {
	// TODO: something better?
	char s[34];
	do {
		_ltoa_s((LONG)this * rand(), s, _countof(s), 8);
		cookie = s;
	} while (m_cookies.find(cookie) != m_cookies.end());

	m_cookies.insert(CookieMap::value_type(cookie, time(0) + m_adminTimeoutSec));
}

void AdminRequestProcessor::actionResultError(string& resultXml, LONG errNo, const string& errMsg) {
	char s[34];
	::_ltoa_s(errNo, s, _countof(s), 10);

	resultXml = "<ActionResult rc=\"";
	resultXml += s;
	resultXml += "\">";
	resultXml += errMsg;
	resultXml += "</ActionResult>";

	LOG4CPLUS_ERROR(logger(), errMsg);
}

bool AdminRequestProcessor::ProcessRequests() {
	jsConnection conn;

	jsConnection::Status stat;

	if (m_port == 0) { // if zero con't shut it down, usually config error
		LOG4CPLUS_ERROR(logger(), loadStr(IDS_BIND_PORT_ZERO));
		return false;
	}

	stat = conn.bind(m_port);
	LOG4CPLUS_INFO(logger(), loadStr(IDS_BIND_PORT) << m_port);
	if (stat != jsConnection::Ok) {
		jsAssert(false);
		LOG4CPLUS_ERROR(logger(), loadStr(IDS_ERR_BIND) << (int)stat);
		return false;
	}

	stat = conn.listen();
	LOG4CPLUS_DEBUG(logger(), "Listen returned " << (int)stat);
	if (stat != jsConnection::Ok) {
		jsAssert(false);
		LOG4CPLUS_ERROR(logger(), loadStr(IDS_ERR_LISTEN) << (int)stat);
		return false;
	}

	BYTE buffer[65525];
	BYTE* actionXml = buffer + sizeof(ULONG);

	while (!m_shutdown) {
		SOCKFD sock = conn.accept();
		LOG4CPLUS_DEBUG(logger(), "Client socket connected");

		{
			// scope

			jsConnection caller(sock);

			actionXml[0] = '\0';

			BYTE* s = buffer;

			ULONG expectedLen = 0;
			//ULONG remainingLen = sizeof( buffer );
			ULONG totalBytesRead = 0;

			ULONG len = 0;

			while (caller.read(s, sizeof(buffer) - totalBytesRead, len, m_socketTimeoutMs) == jsConnection::Ok) {
				if (len == 0)
					break;

				totalBytesRead += len;

				if (expectedLen == 0 && totalBytesRead >= sizeof(ULONG)) {
					// first four bytes are len, in network order
					expectedLen = jsSocket::netntohl(*(ULONG*)s);
					if (expectedLen > 64000) { // probably bad
						LOG4CPLUS_WARN(logger(), loadStr(IDS_BAD_DATA) << expectedLen);
						totalBytesRead = 0;
						break;
					}
				}
				s += len;

				if (totalBytesRead >= expectedLen + sizeof(ULONG))
					break;
			}

			if (totalBytesRead == 0)
				continue;

			buffer[totalBytesRead] = '\0';

			if (totalBytesRead != expectedLen + sizeof(ULONG)) {
				LOG4CPLUS_WARN(logger(), loadStr(IDS_BAD_DATA) << totalBytesRead << " != " << (expectedLen + sizeof(ULONG)));
				continue;
			}

			LOG4CPLUS_DEBUG(logger(), "Rec'd buffer of len: " << totalBytesRead);
			if (!m_debugAdminFile.empty()) {
				ofstream action((m_debugAdminFile + "_action.xml").c_str());
				action << (char*)actionXml;
				action.close();
			}

			string resultXml;
			processXml((const char*)(actionXml), resultXml);

			if (!m_debugAdminFile.empty()) {
				ofstream result((m_debugAdminFile + "_result.xml").c_str());
				result << resultXml;
				result.close();
			}

			expectedLen = jsSocket::nethtonl(resultXml.length());

			caller.write((BYTE*)&expectedLen, sizeof(expectedLen), len);
			caller.write((BYTE*)resultXml.c_str(), resultXml.length(), len);

			LOG4CPLUS_DEBUG(logger(), "Sent buffer of len: " << resultXml.length() << "/" << len);
		}
	}

	LOG4CPLUS_INFO(logger(), loadStr(IDS_EXIT));
	return true;
}

void AdminRequestProcessor::processXml(const char* actionXml, string& resultXml) {
	TiXmlDocument doc;
	doc.Parse(actionXml);
	if (doc.Error()) {
		actionResultError(resultXml, IDS_XML_ERROR, loadStdStr(IDS_XML_ERROR));
		LOG4CPLUS_INFO(logger(), loadStr(IDS_XML_ERROR) << doc.ErrorDesc());
		return;
	} else {
		string cookie;

		UINT rc = checkLogin(doc.RootElement(), cookie);

		// do login checking
		if (rc == NO_ERROR) {
			if (::strcmp(doc.RootElement()->Value(), "Action") == 0) {
				const char* objectId = doc.RootElement()->Attribute("objectId");
				if (objectId) {
					processRequest(objectId, cookie.c_str(), doc.RootElement(), resultXml);
				} else {
					actionResultError(resultXml, IDS_XML_ERROR, loadStdStr(IDS_XML_ERROR));
					jsAssert(false);
				}
			} else if (::strcmp(doc.RootElement()->Value(), "Login") == 0) {
				resultXml = "<ActionResult rc=\"0\" cookie=\"";
				resultXml += cookie;
				resultXml += "\"/>";
			} else {
				actionResultError(resultXml, IDS_XML_ERROR, loadStdStr(IDS_XML_ERROR));
				jsAssert(false);
			}
		} else {
			actionResultError(resultXml, rc, loadStdStr(rc));
		}
	}
}

void AdminRequestProcessor::StopProcessing() {
	LOG4CPLUS_INFO(logger(), loadStr(IDS_SHUTDOWN));

	m_sync.lock();
	m_shutdown = true;
	m_sync.unlock();

	// wake up the blocked accept now that we set m_shutdown
	jsConnection conn;
	conn.connect("localhost", m_port, 2); // short timeout
	conn.close();
}
