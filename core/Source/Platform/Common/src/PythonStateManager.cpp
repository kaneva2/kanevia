///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Core/Util/fast_mutex.h"
#include <js.h>

#include <jsThread.h>

#include <log4cplus/logger.h>

extern "C" {
// avoid benign warning due to Python/log4cplus conflict
#undef HAVE_FTIME
#ifdef _DEBUG
#undef _DEBUG
#include "../../Tools/Python/include/Python.h"
#define _DEBUG
#else
#include "../../Tools/Python/include/Python.h"
#endif
}

// static data global to Python's usage
static int tlsId = 0;
static int initializedCount;

// protect globals
static KEP::fast_recursive_mutex sync;
KEP::fast_recursive_mutex& getSync() {
	return sync;
}

//----------------------------------------------------------------------------
// an instance of this is kept in TLS for each thread
// to track the state and interpreter
class PSMThdState {
public:
	PSMThdState(PyThreadState* s) :
			m_state(s), m_lockCnt(0), m_useCount(1) {
		jsAssert(s != 0);
	}

	// begin using this state
	void BeginUse() {
		if (m_lockCnt == 0) {
			::PyEval_AcquireThread(m_state);
		}
		m_lockCnt++;
	}

	// end using this state
	void EndUse() {
		jsAssert(m_lockCnt > 0);

		m_lockCnt--;
		if (m_lockCnt == 0) {
			::PyEval_ReleaseThread(m_state);
		}
	}

	// track for Delete
	void IncUsage() {
		m_useCount++;
	}

	// delete is last use
	void Delete() {
		m_useCount--;
		if (m_useCount == 0) {
			::PyEval_AcquireThread(m_state);
			::Py_EndInterpreter(m_state);
			::PyEval_ReleaseLock();

			delete this;
			jsThread::setThreadData(tlsId, 0);
		}
	}

	// called if this state is the state
	// used for finalization
	void Finalizing() {
		jsAssert(m_useCount == 1);

		// finalize needs a thd state
		BeginUse();

		delete this;
		jsThread::setThreadData(tlsId, 0);
	}

	~PSMThdState() {
	}

	PyThreadState* m_state;
	int m_lockCnt;

private:
	int m_useCount;
};

static void PSM_Finalize() {
	// need a thread state
	PSMThdState* thdState = (PSMThdState*)jsThread::getThreadData(tlsId);
	if (thdState != 0) {
		thdState->Finalizing();
	}
	// added GIL State wrapper to help w/potential crashes
	/*PyGILState_STATE gState =*/PyGILState_Ensure();
	::Py_Finalize();
	::PyEval_ReleaseLock();
	// don't do after finalize PyGILState_Release( gState );
}

//----------------------------------------------------------------------------
// call before making Python calls to set the thread state properly
// Initialization is performed once
//
// [in] reInit true if you are reinitializing Python
extern "C" __declspec(dllexport) bool PSM_Begin(bool /*reInit*/) {
	PSMThdState* thdState = (PSMThdState*)jsThread::getThreadData(tlsId);
	if (thdState != 0) {
		thdState->BeginUse();
	}
	return true;
}

//----------------------------------------------------------------------------
// call when finished with making Python calls, This must be
// matched to a PSM_Begin call.  See above
extern "C" __declspec(dllexport) void PSM_End() {
	PSMThdState* thdState = (PSMThdState*)jsThread::getThreadData(tlsId);
	if (thdState != 0) {
		thdState->EndUse();
	}
}

static PSMThdState* globalThdState = 0;

//----------------------------------------------------------------------------
// call before first use of Python for your high-level activity such as
// an engine thread or the main thread
//
// first time called, Python is initalized
// each time this is called a count is incremented to help
// determine when to de-init Python
extern "C" __declspec(dllexport) bool PSM_Initialize(bool useGlobalState) {
	std::lock_guard<KEP::fast_recursive_mutex> lock(getSync());

	if (tlsId == 0)
		tlsId = jsThread::createThreadData();

	initializedCount++;
	if (initializedCount == 1) {
		// one-time initialization
		if (!::Py_IsInitialized()) {
			// found that if init Python twice, must get interp lock
			if (::PyEval_ThreadsInitialized())
				::PyEval_AcquireLock();

			::Py_Initialize(); // void nothing to check

			if (::PyEval_ThreadsInitialized()) {
				// threads init, save current state
				PyThreadState* globalState = PyEval_SaveThread(); // releases lock

				// appears that this thread *must* use this state
				globalThdState = new PSMThdState(globalState);
				jsThread::setThreadData(tlsId, (ULONG)(globalThdState));
				return true;
			}
		}
		if (!::PyEval_ThreadsInitialized()) {
			::PyEval_InitThreads(); // acquires lock
			PyThreadState* globalState = PyEval_SaveThread(); // releases lock

			// appears that this thread *must* use this state
			globalThdState = new PSMThdState(globalState);
			jsThread::setThreadData(tlsId, (ULONG)(globalThdState));
			return true;
		}
	}

	// does this thread already have a state?
	PSMThdState* thdState = (PSMThdState*)jsThread::getThreadData(tlsId);

	if (thdState) {
		// set to the global state
		// should be ok to use this thread state on PSM_Begin
		thdState->IncUsage();
	} else {
		if (!useGlobalState) {
			// init new interp
			::PyEval_AcquireLock();
			// to get a new interp, must now have a current thread state
			PyThreadState* threadState = ::Py_NewInterpreter();
			jsThread::setThreadData(tlsId, (ULONG)(new PSMThdState(threadState)));
			PyThreadState_Swap(0);
			::PyEval_ReleaseLock();
		} else {
			if (globalThdState) {
				globalThdState->IncUsage();
				jsThread::setThreadData(tlsId, (ULONG)(globalThdState));
			}
		}
	}

	return true;
}

//----------------------------------------------------------------------------
// call when finished with your activity, This must be
// matched to a PSM_Initialize call.  See above
extern "C" __declspec(dllexport) void PSM_Uninitialize() {
	std::lock_guard<KEP::fast_recursive_mutex> lock(getSync());

	initializedCount--;
	if (initializedCount == 0) {
		PSM_Finalize();
	} else if (initializedCount > 0) {
		PSMThdState* thdState = (PSMThdState*)jsThread::getThreadData(tlsId);
		if (thdState != 0) {
			thdState->Delete();
		}
	}
}

static std::string getTraceback(PyObject* exception, PyObject* v, PyObject* traceback) {
	std::string result;

	/*
	  import traceback
	  lst = traceback.format_exception(exception, v,
	traceback)
	*/
	PyObject* tbstr = PyString_FromString("traceback"); // new ref
	PyObject* tbmod = PyImport_Import(tbstr); // new ref
	Py_DECREF(tbstr);
	if (!tbmod) {
		return "Unable to import traceback module. Is your Python installed?";
	}

	PyObject* tbdict = PyModule_GetDict(tbmod); // borrowed ref
	PyObject* formatFunc = PyDict_GetItemString(tbdict, "format_exception"); // borrowed ref

	if (!formatFunc) {
		Py_DECREF(tbmod);
		return "Can't find traceback.format_exception";
	}

	if (!traceback) {
		traceback = Py_None;
		Py_INCREF(Py_None);
	}
	PyObject* args = Py_BuildValue("(OOO)", exception, v, traceback); // new ref
	PyObject* lst = PyObject_CallObject(formatFunc, args); // new ref

	if (lst) {
		for (int i = 0; i < PyList_GET_SIZE(lst); i++) {
			result += PyString_AsString(PyList_GetItem(lst, i)); // PyList_GetItem returns borrowed ref
		}
	}

	Py_DECREF(tbmod);
	if (args)
		Py_DECREF(args);
	if (lst)
		Py_DECREF(lst);

	return result;
}

// helper to get info about a Python error and log it
extern "C" __declspec(dllexport) void logPythonErr(log4cplus::Logger& logger, const char* msg, bool warn) {
	// prints to stderr
	PyObject* err = PyErr_Occurred(); // borrowed ref
	if (err) { // should be set
		PyObject *exception, *v, *tb;

		PyErr_Fetch(&exception, &v, &tb); // returns new refrences
		PyErr_NormalizeException(&exception, &v, &tb);
		if (exception != NULL) {
			char* lt = "";
			char* lv = "";

			PyObject* lto = PyObject_Str(exception); // new ref
			PyObject* vo = 0;
			if (v)
				PyObject_Str(v);

			if (lto)
				lt = PyString_AS_STRING(lto);
			if (vo) // n is new ref
				lv = PyString_AS_STRING(vo);

			std::string tbs = getTraceback(exception, v, tb);
			if (warn) {
				LOG4CPLUS_WARN(logger, msg << ": " << lt << " " << lv << " " << tbs);
			} else {
				LOG4CPLUS_ERROR(logger, msg << ": " << lt << " " << lv << " " << tbs);
			}

			if (lto != NULL)
				Py_DECREF(lto);
			if (vo != NULL)
				Py_DECREF(vo);

			Py_DECREF(exception);
			if (v != NULL)
				Py_DECREF(v);
			if (tb != NULL)
				Py_DECREF(tb);

			return;
		}
	}

	if (warn) {
		LOG4CPLUS_WARN(logger, msg);
	} else {
		LOG4CPLUS_ERROR(logger, msg);
	}
}
