///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include <TinyXML/tinyxml.h>

#include "game.h"

#include <js.h>
#include <jsEnumFiles.h>
#include <KEPException.h>
#include "CEXScriptClass.h"
#include "GameAdvancedSettings.h"
#include "KEPFilenames.h"
#include "GetSetStrings.h"
#include "Editor/Resource.h"
#include "Event/Glue/GameIds.h"

using namespace KEP;

static const char* DEFAULT_VERSION = "2.0";

Game::Game(void) :
		m_config(), m_version(DEFAULT_VERSION), m_dirty(false) {
}

Game::~Game(void) {
}

static const char* ROOT_TAG = "Game";
static const char* NAME_TAG = "Name";
static const char* DESC_TAG = "Desc";
static const char* AUTHOR_TAG = "Author";
static const char* DATE_TAG = "Date";
static const char* MANIFEST_TAG = "Manifest";
static const char* SVR_TAG = "SVR";
static const char* CFGGAMEID_TAG = "ID";
static const char* PARENTID_TAG = "ParentID";
static const char* LOADING_TAG = "LoadingImage";
static const char* SHADOW_TAG = "ShadowImage";
static const char* CURSOR_TAG = "CursorImage";
static const char* HTTP_HOST = "HttpHostName";
static const char* EDB_EXPAND = "ExpandedEDB";
static const char* EDB_PROGDL = "DownloadEntities";
static const char* VERSION_TAG = "AssetVersion";
static const char* USER_TAG = "User";
static const char* PASSWORD_TAG = "Password";
static const char* ENCRYPTION_TAG = "EncryptionKey";
static const char* SERVER_TAG = "ServerKey";
static const char* LAST_DISTRO_TAG = "DistributionFolder";
static const char* DEFAULT_SVR = "default.svr";

#include <direct.h>

bool Game::Load(const char* baseDir, const char* fname, bool svrCheck) {
	bool ret = true;

	try {
		m_config.Load(fname, ROOT_TAG);

		m_pathBase = baseDir;
		m_fileName = fname;

		m_name = m_config.Get(NAME_TAG, "").c_str();
		m_desc = m_config.Get(DESC_TAG, "").c_str();
		m_author = m_config.Get(AUTHOR_TAG, "").c_str();
		m_date = m_config.Get(DATE_TAG, "").c_str();
		m_manifest = m_config.Get(MANIFEST_TAG, "").c_str();
		m_svr = m_config.Get(SVR_TAG, "").c_str();
		m_gameId = m_config.Get(CFGGAMEID_TAG, "").c_str();
		m_version = m_config.Get(VERSION_TAG, DEFAULT_VERSION).c_str();
		m_user = m_config.Get(USER_TAG, "").c_str();
		string s;
		m_config.GetValue(PASSWORD_TAG, s, "");
		m_password = s.c_str();
		m_config.GetValue(ENCRYPTION_TAG, s, "");
		m_encryptionKey = s.c_str();
		m_config.GetValue(SERVER_TAG, s, "");
		m_serverKey = s.c_str();
		// this not in XML since XML lives in svn and this is machine-specific
		CStringA ss;
		ss.Format("%s-%s", LAST_DISTRO_TAG, m_gameId);
		m_lastDistroFolder = Utf16ToUtf8(AfxGetApp()->GetProfileString(L"Distribution", Utf8ToUtf16(ss).c_str(), L"")).c_str();
		m_parentId = m_config.Get(PARENTID_TAG, "0").c_str();

		//m_httphostname = m_config.Get( HTTP_HOST, "www.kaneva.com" ).c_str();

		if (svrCheck) {
			CStringA svrFilePreCheck;
			svrFilePreCheck = baseDir;
			svrFilePreCheck += GAMEFILES;
			svrFilePreCheck += "\\";
			svrFilePreCheck += m_svr;

			if (!::jsFileExists(svrFilePreCheck, NULL)) {
				CStringW msg;
				msg.Format(IDS_EDITOR_SVR_LOAD_FAILED, Utf8ToUtf16(svrFilePreCheck).c_str(), ERROR_FILE_NOT_FOUND);
				AfxMessageBox(msg, MB_ICONEXCLAMATION);
				ret = false;
			}
		}
	} catch (KEPException* e) {
		ret = false;
		e->Delete();
	}

	//set dirty to false if game is loaded
	m_dirty = !ret;
	return ret;
}

// [in] fname if not null, do save as
bool Game::Save(const char* pathName /*= 0*/) {
	bool ret = false;
	try {
		m_config.SetValue(NAME_TAG, m_name);
		m_config.SetValue(DESC_TAG, m_desc);
		m_config.SetValue(AUTHOR_TAG, m_author);
		m_config.SetValue(DATE_TAG, m_date);
		m_config.SetValue(MANIFEST_TAG, m_manifest);
		m_config.SetValue(SVR_TAG, m_svr);
		m_config.SetValue(CFGGAMEID_TAG, m_gameId);
		m_config.SetValue(VERSION_TAG, m_version);
		m_config.SetValue(USER_TAG, m_user);
		m_config.SetValue(PASSWORD_TAG, m_password);
		m_config.SetValue(ENCRYPTION_TAG, m_encryptionKey);
		m_config.SetValue(SERVER_TAG, m_serverKey);
		// this not in XML since XML lives in svn and this is machine-specific
		CStringA s;
		s.Format("%s-%s", LAST_DISTRO_TAG, m_gameId);
		AfxGetApp()->WriteProfileString(L"Distribution", Utf8ToUtf16(s).c_str(), Utf8ToUtf16(m_lastDistroFolder).c_str());
		m_config.SetValue(PARENTID_TAG, m_parentId);

		if (pathName != 0) {
			// now save whole path since everyone assumes that anyway
			m_fileName = pathName;
			m_pathBase = pathName;
			int fileNameOffset = m_pathBase.GetLength() - m_pathBase.ReverseFind('\\');
			m_pathBase.GetBufferSetLength(m_pathBase.GetLength() - (fileNameOffset - 1));
		}

		// always call m_config.SaveAs since they may have done saveAs and changed the path
		ret = m_config.SaveAs(m_fileName.str().c_str());

		m_dirty = false;
	} catch (KEPException* e) {
		e->Delete();
	}

	return ret;
}

bool Game::GetGameUserPass(CStringA& user, CStringA& password) {
	user = m_user;
	string pw;
	if (SetupCryptoClient() && DecodeAndDecryptString(m_password, pw, DB_KEY)) {
		password = pw.c_str();
		return true;
	}
	return false;
}

bool Game::SetGameUserPass(CStringA user, CStringA password) {
	m_user = user;
	string pw;
	if (EncryptAndEncodeString(password, pw, DB_KEY)) {
		m_password = pw.c_str();
		return true;
	}
	return false;
}

bool Game::GetEncryptionKey(CStringA& _key) {
	string key;
	if (SetupCryptoClient() && DecodeAndDecryptString(m_encryptionKey, key, DB_KEY)) {
		_key = key.c_str();
		return true;
	}
	return false;
}

bool Game::GetServerKey(CStringA& _key) {
	string key;
	if (SetupCryptoClient() && DecodeAndDecryptString(m_serverKey, key, DB_KEY)) {
		_key = key.c_str();
		return true;
	}
	return false;
}

bool Game::GetLicenseDate(__time64_t& _date) {
	string date;
	if (SetupCryptoClient() && DecodeAndDecryptString(m_licenseDate, date, DB_KEY)) {
		_date = atol(date.c_str());
		return true;
	}
	return false;
}

bool Game::GetLicenseSecondsRemaining(int& _seconds) {
	string seconds;
	if (SetupCryptoClient() && DecodeAndDecryptString(m_licenseSecondsRemaining, seconds, DB_KEY)) {
		_seconds = atol(seconds.c_str());
		return true;
	}
	return false;
}

bool Game::SetEncryptionKey(CStringA _key) {
	string key;
	if (!_key.IsEmpty() && EncryptAndEncodeString(_key, key, DB_KEY)) {
		m_encryptionKey = key.c_str();
		return true;
	}
	return false;
}

bool Game::SetServerKey(CStringA _key) {
	string key;
	if (!_key.IsEmpty() && EncryptAndEncodeString(_key, key, DB_KEY)) {
		m_serverKey = key.c_str();
		return true;
	}
	return false;
}

bool Game::SetLicenseDate(__time64_t date) {
	char buf[20];
	string encrypted;
	_ltoa_s(date, buf, sizeof(buf), 16);
	if (EncryptAndEncodeString(buf, encrypted, DB_KEY)) {
		m_licenseDate = encrypted.c_str();
		return true;
	}
	return false;
}

bool Game::SetLicenseSecondsRemaining(int secondsRemaining) {
	char buf[20];
	string encrypted;
	_itoa_s(secondsRemaining, buf, sizeof(buf), 16);
	if (EncryptAndEncodeString(buf, encrypted, DB_KEY)) {
		m_licenseSecondsRemaining = encrypted.c_str();
		return true;
	}
	return false;
}

void Game::New(const char* pathName, const char* gameName) {
	m_name = gameName;

	m_desc = "";
	m_author = "";
	m_manifest = "";
	m_loadedZone.Empty();
	m_svr = DEFAULT_SVR;

	// set the create time
	time_t t;
	time(&t);
	struct tm now;
	localtime_s(&now, &t);

	const int BUF_SIZE = 100;
	char s[BUF_SIZE];

	strftime(s, BUF_SIZE, "%x %X", &now);
	m_date = s;

	// create the config file
	m_config.New(ROOT_TAG);
	m_config.SaveAs(pathName);

	m_fileName = pathName;
}

void Game::Copy(const char* gameName) {
	m_name = gameName;

	// set the create time
	time_t t;
	time(&t);
	struct tm now;
	localtime_s(&now, &t);

	const int BUF_SIZE = 100;
	char s[BUF_SIZE];

	strftime(s, BUF_SIZE, "%x %X", &now);
	m_date = s;
}

string Game::GetString(ULONG index) {
	string ret;
	CStringA csret;
	switch (index) {
		case GAMEIDS_PASSWORD: {
			CStringA user;
			GetGameUserPass(user, csret);
		} break;

		case GAMEIDS_ENCRYPTIONKEY:
			GetEncryptionKey(csret);
			ret = csret;
			break;

		case GAMEIDS_SERVERKEY:
			GetServerKey(csret);
			ret = csret;
			break;

		default:
			ret = GetSet::GetString(index).c_str();
			break;
	}
	return ret;
}

void Game::SetString(ULONG index, const char* s) {
	switch (index) {
		case GAMEIDS_PASSWORD:
			SetGameUserPass(m_user, s);
			break;

		case GAMEIDS_ENCRYPTIONKEY:
			SetEncryptionKey(s);
			break;

		case GAMEIDS_SERVERKEY:
			SetServerKey(s);
			break;

		default:
			GetSet::SetString(index, s);
			break;
	}
}

BEGIN_GETSET_IMPL(Game, IDS_GAMECLASS_NAME)
GETSET2_MAX(m_name, gsCString, GS_FLAGS_DEFAULT, 0, 0, IDS_GAME_NAME, IDS_GAME_NAME_HELP, STRLEN_MAX)
GETSET2_MAX(m_desc, gsCString, GS_FLAGS_DEFAULT, 0, 0, IDS_GAME_DESC, IDS_GAME_DESC_HELP, STRLEN_MAX)
GETSET2_MAX(m_author, gsCString, GS_FLAGS_DEFAULT, 0, 0, IDS_GAME_AUTHOR, IDS_GAME_AUTHOR_HELP, STRLEN_MAX)
GETSET2_MAX(m_manifest, gsCString, GS_FLAG_READ_ONLY, 0, 0, IDS_GAME_MANIFEST, IDS_GAME_MANIFEST, STRLEN_MAX)
GETSET2_MAX(m_svr, gsCString, GS_FLAGS_DEFAULT, 0, 0, IDS_GAME_SVR, IDS_GAME_SVR_HELP, STRLEN_MAX)
GETSET2(m_version, gsCString, GS_FLAGS_DEFAULT, 0, 0, IDS_GAME_VERSION, IDS_GAME_VERSION_HELP)
GETSET2(m_gameId, gsCString, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_GAME_ID, IDS_GAME_ID_HELP)
GETSET2(m_parentId, gsCString, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_PARENT_ID, IDS_PARENT_ID)
GETSET2_MAX(m_date, gsCString, GS_FLAG_EXPOSE | GS_FLAG_READ_ONLY, 0, 0, IDS_GAME_DATE, IDS_GAME_DATE_HELP, STRLEN_MAX)
GETSET2_MAX(m_user, gsCString, GS_FLAGS_DEFAULT, 0, 0, IDS_GAME_OWNER, IDS_GAME_OWNER_HELP, STRLEN_MAX)
GETSET2_MAX(m_password, gsCString, GS_FLAG_EXPOSE | GS_FLAG_READ_WRITE | GS_FLAG_PASSWORD, 0, 0, IDS_PASSWORD, IDS_PASSWORD, STRLEN_MAX)
GETSET2_MAX(m_encryptionKey, gsCString, GS_FLAG_EXPOSE | GS_FLAG_READ_WRITE | GS_FLAG_PASSWORD, 0, 0, IDS_GAME_ENCR_KEY, IDS_GAME_ENCR_KEY, STRLEN_MAX)
GETSET2_MAX(m_serverKey, gsCString, GS_FLAG_EXPOSE | GS_FLAG_READ_WRITE | GS_FLAG_PASSWORD, 0, 0, IDS_GAME_SERVER_KEY, IDS_GAME_SERVER_KEY, STRLEN_MAX)
END_GETSET_IMPL
