///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GetSet.h"

#include "d3d9.h"

#include <TinyXML/tinyxml.h>
#include "KEPException.h"
#include <jsBERDecoder.h>
#include <jsBEREncoder.h>
#include <jsEnumFiles.h>

#include "gsRgbaValue.h"

#include "LogHelper.h"
static LogInstance("Instance");

#include "common/include/IMemSizeGadget.h"

#define MAX_ID_LEN 512

static const char* const GSMETADATA_TAG = "GetSetMetaData";
static const char* const METADATUM_TAG = "MetaDatum";
static const char* const NAME_TAG = "Name";
static const char* const TYPE_TAG = "Type";
static const char* const FLAGS_TAG = "Flags";
static const char* const DEFAULT_TAG = "DefaultValue";

namespace KEP {

gsMetaDatum& gsMetaDatum::operator=(const gsMetaDatum& src) {
	if (this != &src) {
		if (IsFlagSet(GS_FLAG_DYNAMIC_ADD)) {
			free((void*)dynamicName);
			dynamicName = 0;
			free(dynamicDefaultValue);
			dynamicDefaultValue = 0;
		}

		descId = src.descId;
		type = src.type;
		flags = src.flags;
		indent = src.indent;
		groupTitleId = src.groupTitleId;
		helpId = src.helpId;
		minValue = src.minValue;
		maxValue = src.maxValue;
		getSetFromPtr = src.getSetFromPtr;

		if (src.IsFlagSet(GS_FLAG_DYNAMIC_ADD)) {
			dynamicName = _strdup(src.dynamicName);
			EGetSetType gsType = src.type;
			switch (gsType) {
				case gsInt:
					dynamicDefaultValue = new int;
					*(int*)dynamicDefaultValue = *(int*)src.dynamicDefaultValue;
					break;
				case gsLong:
					dynamicDefaultValue = new long;
					*(long*)dynamicDefaultValue = *(long*)src.dynamicDefaultValue;
					break;
				case gsBool:
					dynamicDefaultValue = new bool;
					*(bool*)dynamicDefaultValue = *(bool*)src.dynamicDefaultValue;
					break;
				case gsBOOL:
					dynamicDefaultValue = new BOOL;
					*(BOOL*)dynamicDefaultValue = *(BOOL*)src.dynamicDefaultValue;
					break;
				case gsDouble:
					dynamicDefaultValue = new double;
					*(double*)dynamicDefaultValue = *(double*)src.dynamicDefaultValue;
					break;
				case gsString:
					dynamicDefaultValue = _strdup((char*)src.dynamicDefaultValue);
					break;
				default:
					LogError("Unexpected gsType=" << gsType);
					jsAssert(false);
					break;
			}
		}
	}
	return *this;
}

GetSet::~GetSet() {
	if (m_gsMetaData) {
		// values could be shorter than md, if checkValues never called
		// (such as deserialize, then delete)
		for (UINT i = 0; i < m_gsMetaData->size() && i < m_gsValues->size(); i++) {
			EGetSetType gsType = m_gsMetaData->at(i).GetType();
			switch (gsType) {
				case gsRgbaColor:
					delete (gsRgbaValue*)m_gsValues->at(i);
					break;

				case gsGetSetPtr:
					if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DELETE))
						delete (GetSet*)m_gsValues->at(i);
					break;

				// added these since they can be dynamically added
				case gsInt:
				case gsLong:
					if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DELETE))
						delete (long*)m_gsValues->at(i);
					break;

				case gsBool:
					if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DELETE))
						delete (bool*)m_gsValues->at(i);
					break;

				case gsBOOL:
					if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DELETE))
						delete (BOOL*)m_gsValues->at(i);
					break;

				case gsDouble:
					if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DELETE))
						delete (double*)m_gsValues->at(i);
					break;

				case gsString:
					if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DELETE))
						delete (std::string*)m_gsValues->at(i);
					break;

				default:
					if ((m_gsMetaData->at(i).GetType() & gsSimpleArray) == gsSimpleArray) {
						if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DELETE))
							delete (GetSetSimpleArray*)m_gsValues->at(i);
					}
					break;
			};
		}
	}
}

static inline bool checkRange(const char* value, double minValue, double maxValue, double* dValue = 0) {
	bool ret = false;
	double d = atof(value);

	if (minValue == maxValue ||
		(d >= minValue && d <= maxValue)) {
		ret = true;
	}

	if (dValue)
		*dValue = d;

	return ret;
}

template <class T>
bool checkAndSetFromLong(const char* strValue, gsMetaDatum& md, void* value) {
	bool ret = checkRange(strValue, md.GetMinValue(), md.GetMaxValue());
	if (ret) {
		*(T*)value = (T)atol(strValue);
	}

	return ret;
}

template <class T>
bool checkAndSetFromDouble(const char* strValue, gsMetaDatum& md, void* value) {
	double d;
	bool ret = checkRange(strValue, md.GetMinValue(), md.GetMaxValue(), &d);
	if (ret) {
		*(T*)value = (T)d;
	}

	return ret;
}

template <class T>
bool checkAndSetFromString(const char* strValue, gsMetaDatum& md, void* value) {
	bool ret = false;
	if (md.GetMinValue() == 0 || ::strlen(strValue) <= md.GetMinValue()) {
		*(T*)value = strValue;
		ret = true;
	}

	return ret;
}

static bool boolFromString(const char* value) {
	// web page sends in empty string or "on", we'll take 0/1 true/false T/F
	// check for false and assume anything else is true
	return !(::strlen(value) == 0 || ::strcmp(value, "0") == 0 || ::_stricmp(value, "false") == 0 || ::_stricmp(value, "f") == 0);
}

bool GetSet::SetValue(ULONG index, const char* value) {
	ASSERT(m_gsValues.get() != 0); // must get the values before setting them!

	bool ret = false;
	if (index >= 0 && index < m_gsMetaData->size()) {
		ASSERT(m_gsMetaData->size() == m_gsValues->size());

		gsMetaDatum& md = (*m_gsMetaData)[index];

		if (!md.IsFlagSet(GS_FLAG_READ_WRITE))
			return true; // pretend we updated it

		if (m_updater && m_updater->SetValue(this, md, value, ret))
			return ret;
		else
			ret = false; // just in case m_updater set it inadvertently

		EGetSetType gsType = md.GetType();
		switch (gsType) {
			case gsBool:
				*(bool*)m_gsValues->at(index) = boolFromString(value);
				ret = true;
				break;

			case gsBOOL:
				// web page sends in "on" or empty string
				*(BOOL*)m_gsValues->at(index) = boolFromString(value) ? TRUE : FALSE;
				ret = true;
				break;

			case gsChar:
				ret = checkAndSetFromLong<char>(value, md, m_gsValues->at(index));
				break;

			case gsByte:
				ret = checkAndSetFromLong<BYTE>(value, md, m_gsValues->at(index));
				break;

			case gsShort:
				ret = checkAndSetFromLong<short>(value, md, m_gsValues->at(index));
				break;

			case gsUshort:
			case gsWord:
				ret = checkAndSetFromLong<USHORT>(value, md, m_gsValues->at(index));
				break;

			case gsInt:
				ret = checkAndSetFromLong<int>(value, md, m_gsValues->at(index));
				break;

			case gsUint:
				ret = checkAndSetFromLong<UINT>(value, md, m_gsValues->at(index));
				break;

			case gsLong:
				ret = checkAndSetFromLong<long>(value, md, m_gsValues->at(index));
				break;

			case gsUlong:
			case gsDword:
				ret = checkAndSetFromLong<ULONG>(value, md, m_gsValues->at(index));
				break;

			case gsFloat:
				ret = checkAndSetFromDouble<float>(value, md, m_gsValues->at(index));
				break;

			case gsDouble:
				ret = checkAndSetFromDouble<double>(value, md, m_gsValues->at(index));
				break;

			case gsString:
				ret = checkAndSetFromString<std::string>(value, md, m_gsValues->at(index));
				break;

			case gsCString:
				ret = checkAndSetFromString<CStringA>(value, md, m_gsValues->at(index));
				break;

			case gsCharArray:
				if (value) {
					if (::strlen(value) > md.GetMaxValue())
						ret = false;
					else
						::strcpy_s((char*)value, (rsize_t)md.GetMaxValue() + 1, value);
				} else {
					*(char*)m_gsValues->at(index) = '\0'; // empty the string, if null passed in
					ret = true;
				}
				break;

			// graphic
			case gsPoint: {
				POINT* p = (POINT*)(*m_gsValues)[index];
				if (p)
					ret = sscanf_s(value, "%d,%d", &p->x, &p->y) == 2; // longs
			} break;

			case gsRect: {
				RECT* r = (RECT*)(*m_gsValues)[index];
				if (r)
					ret = sscanf_s(value, "%d,%d,%d,%d", &r->left, &r->top, &r->right, &r->bottom) == 4; // longs
			} break;

			case gsD3dvector: {
				Vector3f* v = (Vector3f*)(*m_gsValues)[index];

				if (v)
					ret = sscanf_s(value, "%f,%f,%f", &v->x, &v->y, &v->z) == 3; // floats, not doubles in DX9
			} break;

			case gsRgbaColor: {
				// 4 colors and a float, each color 4 floats
				gsRgbaValue* v = (gsRgbaValue*)(*m_gsValues)[index];
				if (v) {
					float r, g, b, a;
					int x = sscanf_s(value, "%f,%f,%f,%f", &r, &g, &b, &a);
					ret = x >= 3;
					if (v->r)
						*v->r = r;
					if (v->g)
						*v->g = g;
					if (v->b)
						*v->b = b;
					if (x == 4) {
						if (v->a)
							*v->a = (float)a;
					} else
						*v->a = 0.0f;
				}
			} break;

			// special items
			case gsDate:
				ASSERT(FALSE); // TODO
				ret = false;
				break;

			case gsDatetime:
				ASSERT(FALSE); // TODO
				ret = false;
				break;

			case gsIpAddress:
				ret = checkAndSetFromString<std::string>(value, md, m_gsValues->at(index));
				break;

			case gsListOfValues:
			case gsObjectPtr:
			case gsObListPtr:
			case gsGetSetVector:
			case gsAction:
			case gsCustom:
				// ignore this, derived class should handle it
				break;

			default:
				LogError("Unexpected gsType=" << gsType);
				ASSERT(FALSE);
				break;
		}
	}

	if (ret)
		m_dirty = true;

	return ret;
}

void GetSet::getItemXml(int i, const char* ourObjectId, int depth, gsFilter* filter, gsMetaData* myMd, std::string& xml, char* s) {
	gsMetaDatum& md = (*m_gsMetaData)[i];

	if (md.GetType() != gsGetSetPtr &&
		md.GetType() != gsObjectPtr &&
		md.GetType() != gsObListPtr &&
		md.GetType() != gsGetSetVector) {
		std::string name = loadStr(md.GetDescId());
		escapeXml(name);

		_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
			"<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\"",
			name.c_str(),
			md.GetType(),
			md.GetFlags(),
			md.GetDescId());
		if (filter == 0 || filter->allAttributes()) {
			std::string help = loadStr(md.GetHelpId());
			escapeXml(help);
			_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
				"%s help=\"%s\" minValue=\"%f\" maxValue=\"%f\" indent=\"%d\" groupId=\"%d\"",
				s,
				help.c_str(),
				md.GetMinValue(), md.GetMaxValue(),
				md.GetIndent(),
				md.GetGroupTitleId());
		}
		strncat_s(s, TEMP_STR_SIZE, ">", _TRUNCATE);
		xml += s;
	}

	if ((*m_gsValues)[i] == 0) {
		// couple of cases
		// 1) dynamic cast failed if not deriving from GetSet on the obj ptr
		// 2) the obj or oblist ptr is actually null
		if (md.GetType() != gsGetSetPtr &&
			md.GetType() != gsObjectPtr &&
			md.GetType() != gsObListPtr &&
			md.GetType() != gsGetSetVector)
			xml += "</GetSetRec>\n";
		return;
	}

	if (!getInnerXml(xml, md, m_gsValues->at(i), s)) { // give derived classes a whack

		EGetSetType gsType = md.GetType();
		switch (gsType) {
			case gsBool:
				xml += (*(bool*)((*m_gsValues)[i]) ? "1" : "0");
				break;

			case gsBOOL:
				xml += (*(BOOL*)((*m_gsValues)[i]) ? "1" : "0");
				break;

			case gsChar:
				_ltoa_s(*(char*)(*m_gsValues)[i], s, TEMP_STR_SIZE, 10);
				xml += s;
				break;

			case gsByte:
				_ltoa_s(*(BYTE*)(*m_gsValues)[i], s, TEMP_STR_SIZE, 10);
				xml += s;
				break;

			case gsShort:
				_ltoa_s(*(short*)(*m_gsValues)[i], s, TEMP_STR_SIZE, 10);
				xml += s;
				break;

			case gsUshort:
			case gsWord:
				_ultoa_s(*(USHORT*)(*m_gsValues)[i], s, TEMP_STR_SIZE, 10);
				xml += s;
				break;

			case gsLong:
			case gsInt:
				_ltoa_s(*(long*)(*m_gsValues)[i], s, TEMP_STR_SIZE, 10);
				xml += s;
				break;

			case gsUlong:
			case gsUint:
			case gsDword:
				_ultoa_s(*(ULONG*)(*m_gsValues)[i], s, TEMP_STR_SIZE, 10);
				xml += s;
				break;

			case gsFloat:
				_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "%f", (double)*(float*)(*m_gsValues)[i]);
				xml += s;
				break;

			case gsDouble:
				_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "%f", *(double*)(*m_gsValues)[i]);
				xml += s;
				break;

			case gsString: {
				std::string temp = *(std::string*)(*m_gsValues)[i];
				escapeXml(temp);
				xml += temp.c_str();
			} break;

			case gsCString: {
				std::string temp = *(CStringA*)(*m_gsValues)[i];
				escapeXml(temp);
				xml += temp.c_str();
			} break;

			case gsCharArray: {
				std::string temp = (const char*)(*m_gsValues)[i];
				escapeXml(temp);
				xml += temp.c_str();
			} break;

			case gsPoint: {
				POINT* p = (POINT*)(*m_gsValues)[i];
				_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "%d,%d", p->x, p->y);
				xml += s;
			} break;

			case gsRect: {
				RECT* r = (RECT*)(*m_gsValues)[i];
				_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "%d,%d,%d,%d", r->left, r->top, r->right, r->bottom);
				xml += s;
			} break;

			case gsD3dvector: {
				Vector3f* v = (Vector3f*)(*m_gsValues)[i];

				if (v) {
					_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "<x>%f</x><y>%f</y><z>%f</z>", (double)v->x, (double)v->y, (double)v->z);
				} else {
					_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "<x>%f</x><y>%f</y><z>%f</z>", 0.0, 0.0, 0.0);
				}
				xml += s;
			} break;

			case gsRgbaColor: {
				// 4 colors and a float, each color 4 floats
				gsRgbaValue* v = (gsRgbaValue*)(*m_gsValues)[i];
				_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "%f,%f,%f,%f", (double)(v->r ? *v->r : 0.0),
					(double)(v->g ? *v->g : 0.0),
					(double)(v->b ? *v->b : 0.0),
					(double)(v->a ? *v->a : 0.0));
				xml += s;
			} break;

			// special items
			case gsDate:
				ASSERT(FALSE); // TODO
				xml += "NOTIMPLEMENTED";
				break;

			case gsDatetime:
				ASSERT(FALSE); // TODO
				xml += "NOTIMPLEMENTED";
				break;

			case gsIpAddress:
				xml += ((std::string*)(*m_gsValues)[i])->c_str();
				break;

			case gsObjectPtr:
				if ((*m_gsValues)[i] && depth > 0) {
					GetSet* obj = ToGetSetPtr(i);
					if (obj) {
						char childObjectId[MAX_ID_LEN]; // used when getting children.  add trailing slash
						_snprintf_s(childObjectId, MAX_ID_LEN, _TRUNCATE, "%s%c%x", ourObjectId, OID_DELIM, obj->m_objectId);

						xml += obj->GetXml(childObjectId, depth - 1, filter);
					}
				} else if (depth > 0) {
					ASSERT(FALSE); // dynamic cast failed?
				}
				break;

			case gsObListPtr:
				if ((*m_gsValues)[i] && depth > 0) {
					CObList* obj = ToObListPtr(i);
					if (obj) {
						char childObjectId[MAX_ID_LEN]; // used when getting children.  add trailing slash
						_snprintf_s(childObjectId, MAX_ID_LEN, _TRUNCATE, "%s%c%Ix", ourObjectId, OID_DELIM, reinterpret_cast<uintptr_t>(obj));
						std::string name = loadStr(md.GetDescId());
						escapeXml(name);
						_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\" objectId=\"%s\" count=\"%d\">\n", name.c_str(), gsObListPtr, md.GetFlags(), md.GetDescId(), childObjectId, obj->GetCount());
						xml += s;

						depth--;
						if (depth > 0) {
							for (POSITION pos = obj->GetHeadPosition(); pos != NULL;) {
								CObject* o = obj->GetNext(pos);
								if (o) {
									GetSet* gs = dynamic_cast<GetSet*>(o);
									//								ASSERT( gs );
									if (gs) {
										_snprintf_s(childObjectId, MAX_ID_LEN, _TRUNCATE, "%s%c%x", ourObjectId, OID_DELIM, gs->m_objectId);

										xml += gs->GetXml(childObjectId, depth - 1, filter);
									}
								}
							}
						}
						xml += "</GetSetRec>\n";
					}
				} else if (depth > 0) {
					ASSERT(FALSE); // dynamic cast failed?
				}
				break;

			case gsGetSetPtr:
				if ((*m_gsValues)[i] && depth > 0) {
					GetSet* obj = (GetSet*)(*m_gsValues)[i];
					if (obj) {
						char childObjectId[MAX_ID_LEN]; // used when getting children.  add trailing slash
						_snprintf_s(childObjectId, MAX_ID_LEN, _TRUNCATE, "%s%c%x", ourObjectId, OID_DELIM, obj->m_objectId);

						xml += obj->GetXml(childObjectId, depth - 1, filter);
					}
				} else if (depth > 0) {
					ASSERT(FALSE); // dynamic cast failed?
				}
				break;

			case gsGetSetVector:
				if ((*m_gsValues)[i] && depth > 0) {
					GetSetVector* obj = ToGetSetVector(i);
					if (obj) {
						char childObjectId[MAX_ID_LEN]; // used when getting children.  add trailing slash
						_snprintf_s(childObjectId, MAX_ID_LEN, _TRUNCATE, "%s%c%Ix", ourObjectId, OID_DELIM, reinterpret_cast<uintptr_t>(obj));
						std::string name = loadStr(md.GetDescId());
						escapeXml(name);
						_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\" objectId=\"%s\" count=\"%d\">\n", name.c_str(), gsGetSetVector, md.GetFlags(), md.GetDescId(), childObjectId, obj->size());
						xml += s;

						depth--;
						if (depth > 0) {
							for (GetSetVector::iterator itr = obj->begin(); itr != obj->end(); itr++) {
								GetSet* gs = *itr;
								ASSERT(gs);
								if (gs) {
									_snprintf_s(childObjectId, MAX_ID_LEN, _TRUNCATE, "%s%c%x", ourObjectId, OID_DELIM, gs->m_objectId);

									xml += gs->GetXml(childObjectId, depth - 1, filter);
								}
							}
						}
						xml += "</GetSetRec>\n";
					}
				} else if (depth > 0) {
					ASSERT(FALSE); // dynamic cast failed?
				}
				break;

			case gsCustom:
			case gsAction:
				// ignore this, derived class should handle it
				break;

			case gsListOfValues:
				if ((*m_gsValues)[i]) {
					gsListItems* list = (gsListItems*)(*m_gsValues)[i];
					_ultoa_s(list->size(), s, TEMP_STR_SIZE, 10); // put count in as text
					xml += s;
					for (gsListItems::iterator j = list->begin(); j != list->end(); j++) {
						std::string value = j->first;
						escapeXml(value);
						_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "<ListItem data=\"%d\">%s</ListItem>\n", j->second, value.c_str());
						xml += s;
					}
				}
				break;

			case gsSimpleArray | gsInt: {
				TGetSetSimpleArray<int>* sa = (TGetSetSimpleArray<int>*)(*m_gsValues)[i];
				for (ULONG j = 0; j < sa->size(); j++) {
					char intString[34];
					_ltoa_s(sa->array()[j], intString, _countof(intString), 10);
					xml += "<Item>";
					xml += intString;
					xml += "</Item>";
				}
			} break;

			default: {
				char intString[34];
				_ltoa_s(md.GetType(), intString, _countof(intString), 16);
				xml += "<UnknownType>";
				xml += intString;
				xml += "</UnknownType>";
			} break;
		};
	}

	if (md.GetType() != gsGetSetPtr &&
		md.GetType() != gsObjectPtr &&
		md.GetType() != gsObListPtr &&
		md.GetType() != gsGetSetVector) {
		xml += "</GetSetRec>\n";
	}
}

#define GetSetTag (MAKE_TAG(0) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetArrayTag (MAKE_TAG(1) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetItemTag (MAKE_TAG(2) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetPointTag (MAKE_TAG(3) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetRectTag (MAKE_TAG(4) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetVectorTag (MAKE_TAG(5) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetRGBATag (MAKE_TAG(6) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetListOfValuesTag (MAKE_TAG(7) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetIntArray (MAKE_TAG(8) | APPLICATION_TAG | SEQUENCE_TAG)
#define GetSetUnknownTypeTag (MAKE_TAG(9) | APPLICATION_TAG | SEQUENCE_TAG)

void GetSet::EncodeToBER(jsBEREncoder& enc, int depth /*= -1*/, gsFilter* filter /*=0*/, gsMetaData* myMd /*= 0*/) {
	initGetSet();

	ASSERT(m_gsMetaData->size() == m_gsValues->size());

	enc.beginConstructed(GetSetTag, TWO_BYTE_LENGTH);
	for (gsMetaData::size_type i = 0; i < m_gsMetaData->size(); i++) {
		gsMetaDatum& md = (*m_gsMetaData)[i];

		if (filter != 0 && !filter->include(md)) {
			continue; // skip this one
		}

		encodeItemToBER(i, depth, filter, myMd, enc);
	}
	enc.endConstructed(GetSetTag);
}

void GetSet::encodeItemToBER(int i, int depth, gsFilter* filter, gsMetaData* myMd, jsBEREncoder& enc) {
	gsMetaDatum& md = (*m_gsMetaData)[i];

	if (md.GetType() != gsGetSetPtr &&
		md.GetType() != gsObjectPtr &&
		md.GetType() != gsObListPtr &&
		md.GetType() != gsGetSetVector) {
		// simpler type
		enc.beginConstructed(GetSetItemTag, ONE_BYTE_LENGTH);
		enc << (LONG)md.GetDescId(); // unique id of item type e.g. PlayerLocation
	}

	if ((*m_gsValues)[i] == 0) {
		// couple of cases
		// 1) dynamic cast failed if not deriving from GetSet on the obj ptr
		// 2) the obj or oblist ptr is actually null
		// ASSERT( false );
		if (md.GetType() != gsGetSetPtr &&
			md.GetType() != gsObjectPtr &&
			md.GetType() != gsObListPtr &&
			md.GetType() != gsGetSetVector)
			enc.endConstructed(GetSetItemTag);
		return;
	}

	// encode the item
	EGetSetType gsType = md.GetType();
	switch (gsType) {
		case gsBool:
			enc.encodeBoolean(*(bool*)((*m_gsValues)[i]));
			break;

		case gsBOOL:
			enc.encodeBoolean(*(BOOL*)((*m_gsValues)[i]) != FALSE);
			break;

		case gsChar:
			enc.encodeInteger(*(char*)(*m_gsValues)[i]);
			break;

		case gsByte:
			enc.encodeInteger(*(BYTE*)(*m_gsValues)[i]);
			break;

		case gsShort:
			enc.encodeInteger(*(short*)(*m_gsValues)[i]);
			break;

		case gsUshort:
		case gsWord:
			enc.encodeInteger(*(USHORT*)(*m_gsValues)[i]);
			break;

		case gsLong:
		case gsInt:
			enc.encodeInteger(*(long*)(*m_gsValues)[i]);
			break;

		case gsUlong:
		case gsUint:
		case gsDword:
			enc.encodeInteger(*(ULONG*)(*m_gsValues)[i]);
			break;

		case gsFloat:
			enc.encodeReal(*(float*)(*m_gsValues)[i]);
			break;

		case gsDouble:
			enc.encodeReal(*(double*)(*m_gsValues)[i]);
			break;

		case gsString: {
			enc.encodeString(((std::string*)(*m_gsValues)[i])->c_str());
		} break;

		case gsCString: {
			enc.encodeString(*(CStringA*)(*m_gsValues)[i]);
		} break;

		case gsCharArray: {
			enc.encodeString((const char*)(*m_gsValues)[i]);
		} break;

		case gsPoint: {
			POINT* p = (POINT*)(*m_gsValues)[i];
			enc.beginConstructed(GetSetPointTag);
			enc.encodeInteger(p->x);
			enc.encodeInteger(p->y);
			enc.endConstructed(GetSetPointTag);
		} break;

		case gsRect: {
			RECT* r = (RECT*)(*m_gsValues)[i];
			enc.beginConstructed(GetSetRectTag);
			enc.encodeInteger(r->left);
			enc.encodeInteger(r->top);
			enc.encodeInteger(r->right);
			enc.encodeInteger(r->bottom);
			enc.endConstructed(GetSetRectTag);
		} break;

		case gsD3dvector: {
			Vector3f* v = (Vector3f*)(*m_gsValues)[i];
			enc.beginConstructed(GetSetVectorTag);
			if (v) {
				enc.encodeReal(v->x);
				enc.encodeReal(v->y);
				enc.encodeReal(v->z);
			} else {
				enc.encodeReal(0.0);
				enc.encodeReal(0.0);
				enc.encodeReal(0.0);
			}
			enc.endConstructed(GetSetVectorTag);
		} break;

		case gsRgbaColor: {
			// 4 colors and a float, each color 4 floats
			gsRgbaValue* v = (gsRgbaValue*)(*m_gsValues)[i];
			enc.beginConstructed(GetSetRGBATag);
			enc.encodeReal(v->r ? *v->r : 0.0);
			enc.encodeReal(v->g ? *v->g : 0.0);
			enc.encodeReal(v->b ? *v->b : 0.0);
			enc.encodeReal(v->a ? *v->a : 0.0);
			enc.endConstructed(GetSetRGBATag);
		} break;

		// special items
		case gsDate:
			ASSERT(FALSE); // TODO
			break;

		case gsDatetime:
			ASSERT(FALSE); // TODO
			break;

		case gsIpAddress:
			enc.encodeString(*(std::string*)(*m_gsValues)[i]);
			break;

		case gsObjectPtr:
			if ((*m_gsValues)[i] && depth > 0) {
				GetSet* obj = ToGetSetPtr(i);
				if (obj) {
					obj->EncodeToBER(enc, depth - 1, filter);
				}
			} else if (depth > 0) {
				ASSERT(FALSE); // dynamic cast failed?
			}
			break;

		case gsObListPtr:

			if ((*m_gsValues)[i]) {
				CObList* obj = ToObListPtr(i);
				if (obj) {
					enc.beginConstructed(GetSetArrayTag, TWO_BYTE_LENGTH);
					enc.encodeInteger(md.GetDescId()); // id
					enc.encodeInteger(obj->GetCount());

					if (depth > 0) {
						depth--;
						if (depth > 0) {
							for (POSITION pos = obj->GetHeadPosition(); pos != NULL;) {
								CObject* o = obj->GetNext(pos);
								if (o) {
									GetSet* gs = dynamic_cast<GetSet*>(o);
									if (gs) {
										gs->EncodeToBER(enc, depth - 1, filter);
									}
								}
							}
						}
					}
					enc.endConstructed(GetSetArrayTag);
				}
			}
			break;

		case gsGetSetPtr:
			if ((*m_gsValues)[i] && depth > 0) {
				GetSet* obj = (GetSet*)(*m_gsValues)[i];
				if (obj) {
					obj->EncodeToBER(enc, depth - 1, filter);
				}
			} else if (depth > 0) {
				ASSERT(FALSE); // dynamic cast failed?
			}
			break;

		case gsGetSetVector:

			if ((*m_gsValues)[i]) {
				GetSetVector* obj = ToGetSetVector(i);
				if (obj) {
					enc.beginConstructed(GetSetArrayTag, TWO_BYTE_LENGTH);
					enc.encodeInteger(md.GetDescId()); // id
					enc.encodeInteger(obj->size());

					if (depth > 0) {
						depth--;
						if (depth > 0) {
							for (GetSetVector::iterator itr = obj->begin(); itr != obj->end(); itr++) {
								GetSet* gs = *itr;
								ASSERT(gs);
								if (gs) {
									gs->EncodeToBER(enc, depth - 1, filter);
								}
							}
						}
					}
					enc.endConstructed(GetSetArrayTag);
				}
			}
			break;

		case gsCustom:
		case gsAction:
			// ignore this, derived class should handle it
			break;

		case gsListOfValues:
			if ((*m_gsValues)[i]) {
				gsListItems* list = (gsListItems*)(*m_gsValues)[i];
				enc.beginConstructed(GetSetListOfValuesTag);

				enc.encodeInteger(list->size()); // put count in

				for (gsListItems::iterator j = list->begin(); j != list->end(); j++) {
					std::string value = j->first;
					enc.encodeInteger(j->second);
					enc.encodeString(value.c_str());
				}
				enc.endConstructed(GetSetListOfValuesTag);
			}
			break;

		case gsSimpleArray | gsInt: {
			TGetSetSimpleArray<int>* sa = (TGetSetSimpleArray<int>*)(*m_gsValues)[i];
			enc.beginConstructed(GetSetIntArray);
			enc.encodeInteger(sa->size());
			for (ULONG j = 0; j < sa->size(); j++) {
				enc.encodeInteger(sa->array()[j]);
			}
			enc.endConstructed(GetSetIntArray);
		} break;

		default: {
			enc.beginConstructed(GetSetUnknownTypeTag);
			enc.encodeInteger(md.GetType());
			enc.endConstructed(GetSetUnknownTypeTag);
		} break;
	};

	if (md.GetType() != gsGetSetPtr &&
		md.GetType() != gsObjectPtr &&
		md.GetType() != gsObListPtr &&
		md.GetType() != gsGetSetVector) {
		enc.endConstructed(GetSetItemTag);
	}
}

std::string GetSet::GetXml(const char* ourObjectId /*= ""*/, int depth /*= -1*/, gsFilter* filter /*=0*/, gsMetaData* myMd /*= 0*/) {
	initGetSet();

	ASSERT(m_gsMetaData->size() == m_gsValues->size());

	// see if it has an instance name
	char s[TEMP_STR_SIZE];
	std::string name, xml;
	xml.reserve(TEMP_STR_SIZE);

	for (gsMetaData::size_type i = 0; i < m_gsMetaData->size(); i++) {
		gsMetaDatum& md = (*m_gsMetaData)[i];

		if (md.IsFlagSet(GS_FLAG_INSTANCE_NAME)) {
			if (md.GetType() == gsString)
				name = ((std::string*)(*m_gsValues)[i])->c_str();
			else if (md.GetType() == gsCString)
				name = (const char*)(*(CStringA*)(*m_gsValues)[i]);
			else {
				jsAssert(false);
			}
		}

		if (filter != 0 && !filter->include(md)) {
			continue; // skip this one
		}

		getItemXml(i, ourObjectId, depth, filter, myMd, xml, s);
	}

	if (name.empty())
		name = GetGsName();

	std::string resultXml;
	resultXml.reserve(xml.size() + 256);
	escapeXml(name);
	_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE, "<GetSetRec name=\"%s\" type=\"%d\" objectId=\"%s\">\n", name.c_str(), gsGetSetPtr, ourObjectId);
	resultXml = std::string(s);
	resultXml += xml;
	resultXml += "</GetSetRec>\n";

	return resultXml;
}

void GetSet::DecodeFromBER(jsBERDecoder& dec) {
	// TODO:
	ASSERT(FALSE);
}

bool GetSet::SetValueFromBER(ULONG index, jsBERDecoder& dec) {
	bool ret = false;
	// TODO:
	ASSERT(FALSE);

	if (ret)
		m_dirty = true;

	return ret;
}

///---------------------------------------------------------
// set values from XML.
//
// [in] xml XML containing one or more values to set
//
void GetSet::SetFromXml(TiXmlElement* getSetDataNode) {
	ASSERT(m_gsValues.get() != 0); // must get the values before setting them!

	// TEMP?
	// since the HTML only sends bools that are SET, and nothing
	// for ones that aren't we clear all of them first
	// this has the CAVEAT that you must pass in all the bools
	// changed or not
	// end TEMP?
	for (size_t i = 0; i < m_gsMetaData->size(); i++) {
		if (m_gsMetaData->at(i).GetType() == gsBool || m_gsMetaData->at(i).GetType() == gsBOOL &&
														   m_gsMetaData->at(i).IsFlagSet(GS_FLAG_READ_WRITE)) { // only do the writable ones!!! otherwise loose read-only values
			SetValue(i, "false");
		}
	}

	// parse the XML to get all the values
	TiXmlNode* n = getSetDataNode->FirstChild("GetSetRec");
	while (n && n->Type() == TiXmlNode::ELEMENT) {
		TiXmlElement* e = n->ToElement();
		jsAssert(e != 0);

		// get the id and value
		int id;
		std::string text;
		if (TIXML_SUCCESS == e->QueryIntAttribute("id", &id)) {
			TiXmlHandle h(e);
			TiXmlText* t = h.FirstChild().Text();
			if (t) {
				text = t->Value();
			} else {
				// no text, ok for empty strings
			}

			// find the index of the id
			for (size_t i = 0; i < m_gsMetaData->size(); i++) {
				if (m_gsMetaData->at(i).GetDescId() == id) {
					SetValue(i, text.c_str());
				}
			}
		} else {
			jsAssert(false);
			// TODO:
		}
		n = n->NextSibling("GetSetRec");
	}

	m_dirty = true;
}

// helper for creating parameter Xml for actions
void GetSet::addActionParamXml(std::string& xml, UINT descId, UINT helpId, EGetSetType type, char* s, ULONG flags, double min, double max) {
	std::string name = loadStr(descId);
	escapeXml(name);
	std::string help = loadStr(helpId);
	escapeXml(help);

	_snprintf_s(s, TEMP_STR_SIZE, _TRUNCATE,
		"<GetSetRec name=\"%s\" type=\"%d\" flags=\"%d\" id=\"%d\" help=\"%s\" minValue=\"%f\" maxValue=\"%f\" indent=\"%d\" groupId=\"%d\" />",
		name.c_str(),
		type,
		flags,
		descId,
		help.c_str(),
		min, max,
		0, 0);
	xml += s;
}

template <class type>
type& getType(gsMetaData* m_gsMetaData, std::unique_ptr<gsValues>& m_gsValues, ULONG index, int gsType, int gsEquivType, const char* errMsg) {
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));
	if (index >= m_gsMetaData->size() ||
		(m_gsMetaData->at(index).GetType() != gsType &&
			m_gsMetaData->at(index).GetType() != gsEquivType) ||
		m_gsValues->at(index) == 0)
		throw new KEPException(errMsg, E_INVALIDARG);
	else
		return *(type*)m_gsValues->at(index);
}

#define IMPLEMENT_GET(type, gsType, gsEquivType)                                                          \
	type& GetSet::Get##type(ULONG index) {                                                                \
		initGetSet();                                                                                     \
		return getType<type>(m_gsMetaData, m_gsValues, index, gsType, gsEquivType, "Error in Get" #type); \
	}

IMPLEMENT_GET(Vector3f, gsD3dvector, 0)
IMPLEMENT_GET(ULONG, gsUlong, gsDword)
IMPLEMENT_GET(INT, gsInt, gsLong)
using std::string;
IMPLEMENT_GET(string, gsString, 0)
IMPLEMENT_GET(BOOL, gsBOOL, 0)
IMPLEMENT_GET(bool, gsBool, 0)

std::string GetSet::GetString(ULONG index) {
	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	std::string ret;
	if (index >= m_gsMetaData->size() ||
		m_gsValues->at(index) == 0)
		throw new KEPException("GetSet::GetString", E_INVALIDARG);
	else {
		EGetSetType gsType = m_gsMetaData->at(index).GetType();
		switch (gsType) {
			case gsString:
				ret = *(std::string*)m_gsValues->at(index);
				break;

			case gsCString:
				ret = *(CStringA*)m_gsValues->at(index);
				break;

			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				break;
		}
	}
	return ret;
}

// return a copy of the number as double
double GetSet::GetNumber(ULONG index) {
	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	double ret;
	if (index >= m_gsMetaData->size() ||
		m_gsValues->at(index) == 0)
		throw new KEPException("GetSet::GetNumber", E_INVALIDARG);
	else {
		EGetSetType gsType = m_gsMetaData->at(index).GetType();
		switch (gsType) {
			case gsByte:
				ret = *(BYTE*)m_gsValues->at(index);
				break;

			case gsDouble:
				ret = *(double*)m_gsValues->at(index);
				break;

			case gsDword:
			case gsUlong:
			case gsUint:
				// convert to long to avoid problems in scripting with sign bit
				ret = (LONG) * (ULONG*)m_gsValues->at(index);
				break;

			case gsInt:
			case gsLong:
				ret = *(LONG*)m_gsValues->at(index);
				break;

			case gsShort:
				ret = *(SHORT*)m_gsValues->at(index);
				break;

			case gsWord:
			case gsUshort:
				ret = *(USHORT*)m_gsValues->at(index);
				break;

			case gsFloat:
				ret = *(FLOAT*)m_gsValues->at(index);
				break;

			case gsBOOL:
				ret = (*(BOOL*)m_gsValues->at(index)) == 0 ? 0.0 : 1.0;
				break;

			case gsBool:
				ret = (*(bool*)m_gsValues->at(index)) ? 1.0 : 0.0;
				break;

			default:
				LogError("Unexpected gsType=" << gsType);
				ret = false;
				jsAssert(false);
				break;
		}
	}
	return ret;
}

IGetSet* GetSet::GetObjectPtr(ULONG index) {
	IGetSet* ret = 0;

	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	if (index >= m_gsMetaData->size()) {
		throw new KEPException("GetSet::GetObjectPtr", E_INVALIDARG);
	} else if (m_gsValues->at(index) == 0) {
		return 0; // ok to have null ptr to an object
	} else {
		EGetSetType gsType = m_gsMetaData->at(index).GetType();
		switch (gsType) {
			case gsObjectPtr: { // pointer to GetSet from CObject
				GetSet* obj = ToGetSetPtr(index);
				if (obj)
					ret = dynamic_cast<IGetSet*>(obj);
			} break;

			case gsGetSetPtr: { // pointer to a GetSet
				GetSet* obj = (GetSet*)(*m_gsValues)[index];
				if (obj)
					ret = dynamic_cast<IGetSet*>(obj);
			} break;

			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				break;
		}
	}

	return ret;
}

void GetSet::GetColor(ULONG index, float* r, float* g, float* b, float* a /*= 0*/) {
	initGetSet();
	jsVerifyThrow(r != 0 && g != 0 && b != 0 && m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	gsRgbaValue& color = getType<gsRgbaValue>(m_gsMetaData, m_gsValues, index, gsRgbaColor, 0, "Error in GetColor");
	*r = (color.r ? *color.r : (float)0.0);
	*g = (color.g ? *color.g : (float)0.0);
	*b = (color.b ? *color.b : (float)0.0);
	if (a != 0)
		(color.a ? *color.a : (float)0.0);
}

void GetSet::SetColor(ULONG index, float r, float g, float b, float a /*= 0*/) {
	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	gsRgbaValue& color = getType<gsRgbaValue>(m_gsMetaData, m_gsValues, index, gsRgbaColor, 0, "Error in GetColor");
	if (color.r)
		*color.r = r;
	if (color.g)
		*color.g = g;
	if (color.b)
		*color.b = b;
	if (color.a)
		*color.a = a;

	m_dirty = true;
}

void GetSet::SetString(ULONG index, const char* s) {
	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	if (index >= m_gsMetaData->size() ||
		m_gsValues->at(index) == 0)
		throw new KEPException("GetSet::SetString", E_INVALIDARG);
	else {
		EGetSetType gsType = m_gsMetaData->at(index).GetType();
		switch (gsType) {
			case gsCString:
				*((CStringA*)m_gsValues->at(index)) = s;
				break;
			case gsString:
				*((std::string*)m_gsValues->at(index)) = s;
				break;
			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				break;
		}
	}

	m_dirty = true;
}

template <class type>
void setNumericType(type& num, gsMetaData* m_gsMetaData, std::unique_ptr<gsValues>& m_gsValues, ULONG index, const char* errMsg) {
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));
	if (index >= m_gsMetaData->size() ||
		m_gsValues->at(index) == 0)
		throw new KEPException(errMsg, E_INVALIDARG);
	else {
		EGetSetType gsType = m_gsMetaData->at(index).GetType();
		switch (gsType) {
			case gsByte:
				*(BYTE*)m_gsValues->at(index) = (BYTE)num;
				break;

			case gsDouble:
				*(double*)m_gsValues->at(index) = (double)num;
				break;

			case gsDword:
			case gsUlong:
			case gsUint:
				*(ULONG*)m_gsValues->at(index) = (ULONG)num;
				break;

			case gsInt:
			case gsLong:
				*(LONG*)m_gsValues->at(index) = (LONG)num;
				break;

			case gsShort:
				*(SHORT*)m_gsValues->at(index) = (SHORT)num;
				break;

			case gsWord:
			case gsUshort:
				*(USHORT*)m_gsValues->at(index) = (USHORT)num;
				break;

			case gsFloat:
				*(FLOAT*)m_gsValues->at(index) = (FLOAT)num;
				break;

			case gsBOOL:
				*(BOOL*)m_gsValues->at(index) = num != 0;
				break;

			case gsBool:
				*(bool*)m_gsValues->at(index) = num != 0;
				break;

			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				break;
		}
	}
}

#define IMPLEMENT_SETNUMBER(type)                                                                 \
	void GetSet::SetNumber(ULONG index, type num) {                                               \
		initGetSet();                                                                             \
		setNumericType(num, m_gsMetaData, m_gsValues, index, "Error in SetNumber type = " #type); \
		m_dirty = true;                                                                           \
	}

IMPLEMENT_SETNUMBER(ULONG)
IMPLEMENT_SETNUMBER(LONG)
IMPLEMENT_SETNUMBER(DOUBLE)
IMPLEMENT_SETNUMBER(BOOL)
IMPLEMENT_SETNUMBER(bool)

UINT GetSet::GetNameId() {
	initGetSet();
	return m_gsNameId;
}

void GetSet::SetVector3f(ULONG index, Vector3f v) {
	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	if (index >= m_gsMetaData->size() ||
		m_gsValues->at(index) == 0)
		throw new KEPException("GetSet::SetD3DVector", E_INVALIDARG);
	else {
		EGetSetType gsType = m_gsMetaData->at(index).GetType();
		switch (gsType) {
			case gsD3dvector:
				*(Vector3f*)m_gsValues->at(index) = v;
				break;
			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				break;
		}
	}

	m_dirty = true;
}

ULONG GetSet::GetArrayCount(ULONG indexOfArray) {
	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	if (indexOfArray >= m_gsMetaData->size() ||
		m_gsValues->at(indexOfArray) == 0)
		throw new KEPException("GetSet::GetObjectInArray", E_INVALIDARG);
	else {
		EGetSetType gsType = m_gsMetaData->at(indexOfArray).GetType();
		switch (gsType) {
			case gsObListPtr: { // CObList that has GetSet-derived classes in it
				CObList* obj = ToObListPtr(indexOfArray);
				if (obj)
					return obj->GetCount();
			} break;

			case gsGetSetVector: { // vector of GetSet pointers
				GetSetVector* obj = ToGetSetVector(indexOfArray);
				if (obj) {
					return obj->size();
				} else
					throw new KEPException("Array not found", E_INVALIDARG);
			} break;

			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				break;
		}
	}

	return 0;
}

IGetSet* GetSet::GetObjectInArray(ULONG indexOfArray, ULONG indexInArray) {
	IGetSet* ret = 0;

	initGetSet();
	jsVerifyThrow(m_gsMetaData != 0, new KEPException("Error initializing GetSet", E_INVALIDARG));

	if (indexOfArray >= m_gsMetaData->size() ||
		m_gsValues->at(indexOfArray) == 0)
		throw new KEPException("GetSet::GetObjectInArray", E_INVALIDARG);
	else {
		EGetSetType gsType = m_gsMetaData->at(indexOfArray).GetType();
		switch (m_gsMetaData->at(indexOfArray).GetType()) {
			case gsObListPtr: { // CObList that has GetSet-derived classes in it
				CObList* obj = ToObListPtr(indexOfArray);
				if (obj) {
					if (indexInArray >= (ULONG)obj->GetCount())
						throw new KEPException("Index out of range", E_INVALIDARG);

					POSITION pos = obj->FindIndex(indexInArray);
					if (pos == 0)
						throw new KEPException("Index out of range", E_INVALIDARG);

					ret = dynamic_cast<IGetSet*>(obj->GetAt(pos));
				} else
					throw new KEPException("Array not found", E_INVALIDARG);

			} break;

			case gsGetSetVector: { // vector of GetSet pointers
				GetSetVector* obj = ToGetSetVector(indexOfArray);
				if (obj) {
					if (indexInArray >= obj->size())
						throw new KEPException("Index out of range", E_INVALIDARG);

					ret = dynamic_cast<IGetSet*>(obj->at(indexInArray));
				} else
					throw new KEPException("Array not found", E_INVALIDARG);
			} break;

			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				break;
		}
	}

	return ret;
}

ULONG GetSet::AddNewIntMember(const char* name, int defaultValue, ULONG flags /*= amfTransient*/) {
	int* i = (int*)malloc(sizeof(int));
	*i = defaultValue;
	return AddNewMember(name, gsInt, flags, i);
}

ULONG GetSet::AddNewDoubleMember(const char* name, double defaultValue, ULONG flags /*= amfTransient*/) {
	double* i = (double*)malloc(sizeof(double));
	*i = defaultValue;
	return AddNewMember(name, gsDouble, flags, i);
}

ULONG GetSet::AddNewStringMember(const char* name, const char* defaultValue, ULONG flags /*= amfTransient*/) {
	return AddNewMember(name, gsString, flags, _strdup(NULL_CK(defaultValue)));
}

ULONG GetSet::AddNewMember(const char* name, EGetSetType type, ULONG flags /* = amfTransient */, void* defaultValue) {
	initGetSet();
	ULONG ret = FindIdByName(name);
	if (ret == FIND_ID_NOT_FOUND) {
		if (type == gsInt ||
			type == gsLong ||
			type == gsBool ||
			type == gsBOOL ||
			type == gsDouble ||
			type == gsString) {
			ret = m_gsMetaData->size();
			m_gsMetaData->push_back(gsMetaDatum(type, (flags & GS_DYNAMIC_FLAG_MASK), name, defaultValue));
			checkValues(); // add to the values of this one
		} else {
			ret = ADD_MEMBER_BAD_TYPE;
		}
	} else {
		if (m_gsMetaData->at(ret).GetType() != type) // trying to add it again with different type
			return ADD_MEMBER_DUP_WRONG_TYPE;
	}
	return ret;
}

ULONG GetSet::FindIdByName(const char* name, EGetSetType* type /*= 0 */) {
	initGetSet();
	for (ULONG i = 0; i < m_gsMetaData->size(); i++) {
		if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DYNAMIC_ADD) && ::strcmp(name, m_gsMetaData->at(i).GetDynamicName()) == 0) {
			if (type != 0)
				*type = m_gsMetaData->at(i).GetType();
			return i;
		}
	}
	return FIND_ID_NOT_FOUND;
}

void GetSet::checkValues() {
	if (m_gsMetaData->size() > m_gsValues->size()) {
		for (ULONG i = m_gsValues->size(); i < m_gsMetaData->size(); i++) {
			EGetSetType gsType = m_gsMetaData->at(i).GetType();
			switch (m_gsMetaData->at(i).GetType()) {
				case gsInt:
				case gsLong:
					m_gsValues->push_back(new int);
					*(int*)m_gsValues->back() = *(int*)m_gsMetaData->at(i).GetDynamicDefaultValue();
					break;
				case gsBool:
					m_gsValues->push_back(new bool);
					*(bool*)m_gsValues->back() = *(bool*)m_gsMetaData->at(i).GetDynamicDefaultValue();
					break;
				case gsBOOL:
					m_gsValues->push_back(new BOOL);
					*(BOOL*)m_gsValues->back() = *(BOOL*)m_gsMetaData->at(i).GetDynamicDefaultValue();
					break;
				case gsDouble:
					m_gsValues->push_back(new double);
					*(double*)m_gsValues->back() = *(double*)m_gsMetaData->at(i).GetDynamicDefaultValue();
					break;
				case gsString:
					m_gsValues->push_back(new std::string((const char*)m_gsMetaData->at(i).GetDynamicDefaultValue()));
					break;
				default:
					LogError("Unexpected gsType=" << gsType);
					jsAssert(false);
					m_gsValues->push_back(0);
					break;
			}
		}
	}
}

void GetSet::CopyDynamicallyAddedMembers(GetSet* from) {
	initGetSet();
	if (from == 0 || from->m_gsValues.get() == 0)
		return; // could be first time

	jsVerifyDo(m_gsValues->size() == from->m_gsValues->size(), return );

	// used when cloning an object to get the dynamic one's, too
	for (ULONG i = 0; i < m_gsMetaData->size(); i++) {
		if (m_gsMetaData->at(i).IsFlagSet(GS_FLAG_DYNAMIC_ADD) &&
			m_gsMetaData->at(i).IsFlagSet(GS_FLAG_SAVE_SPAWN)) {
			EGetSetType gsType = m_gsMetaData->at(i).GetType();
			switch (gsType) {
				case gsInt:
				case gsLong:
					*(int*)m_gsValues->at(i) = *(int*)from->m_gsValues->at(i);
					break;
				case gsBool:
					*(bool*)m_gsValues->at(i) = *(bool*)from->m_gsValues->at(i);
					break;
				case gsBOOL:
					*(BOOL*)m_gsValues->at(i) = *(BOOL*)from->m_gsValues->at(i);
					break;
				case gsDouble:
					*(double*)m_gsValues->at(i) = *(double*)from->m_gsValues->at(i);
					break;
				case gsString:
					if (m_gsValues->at(i))
						*(std::string*)m_gsValues->at(i) = *(std::string*)from->m_gsValues->at(i);
					else
						m_gsValues->at(i) = new std::string(*(std::string*)from->m_gsValues->at(i));
					break;
				default:
					LogError("Unexpected gsType=" << gsType);
					jsAssert(false);
					break;
			}
		}
	}

	m_dirty = true;
}

void GetSet::SerializeMetaDataFromXML(const std::string& fileName) {
	bool xmlExists = FileHelper::Exists(fileName);
	if (xmlExists) {
		TiXmlDocument dom;
		if (dom.LoadFile(fileName.c_str())) {
			TiXmlElement* root = dom.FirstChildElement(GSMETADATA_TAG);
			if (root) {
				TiXmlNode* node;
				TiXmlElement* child = 0;

				while ((node = root->IterateChildren(METADATUM_TAG, child)) != 0 && (child = node->ToElement()) != 0) {
					TiXmlText* nameStr = TiXmlHandle(child->FirstChild(NAME_TAG)).FirstChild().Text();
					TiXmlText* typeStr = TiXmlHandle(child->FirstChild(TYPE_TAG)).FirstChild().Text();
					TiXmlText* flagStr = TiXmlHandle(child->FirstChild(FLAGS_TAG)).FirstChild().Text();
					TiXmlText* defValueStr = TiXmlHandle(child->FirstChild(DEFAULT_TAG)).FirstChild().Text();

					CStringA name;
					EGetSetType type = 0;
					ULONG flags = 0;

					if (nameStr && *nameStr->Value()) {
						name = nameStr->Value();
						name.Trim();
					}

					if (typeStr)
						type = atoi(typeStr->Value());
					if (flagStr)
						flags = strtoul(flagStr->Value(), 0, 10);

					// limit what can be set in flags, and always set some
					flags &= GS_FLAG_SAVE_SPAWN;
					flags |= GS_FLAG_DYNAMIC_ADD | GS_FLAG_READ_ONLY;

					if (name.GetLength() > 0) {
						switch (type) {
							case gsInt: {
								int i = 0;
								if (defValueStr != 0)
									atoi(defValueStr->Value());
								AddNewIntMember(name, i, flags);
							} break;

							case gsDouble: {
								double d = 0;
								if (defValueStr != 0)
									atof(defValueStr->Value());
								AddNewDoubleMember(name, d, flags);
							} break;

							case gsString: {
								AddNewStringMember(name, defValueStr != 0 ? defValueStr->Value() : "", flags);
							} break;

							default: {
								LogError("Unexpected gsType=" << type);
								jsAssert(false);
							} break;
						}
					}
				}
				return;
			}
		}
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}
}

void GetSet::SerializeMetaDataToXML(const std::string& fileName) {
	initGetSet();

	TiXmlDocument dom(fileName);
	TiXmlElement root(GSMETADATA_TAG);
	TiXmlNode* rootNode = dom.InsertEndChild(root);
	if (rootNode == 0) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}

	char s[128];

	for (ULONG i = 0; i < m_gsMetaData->size(); i++) {
		if ((m_gsMetaData->at(i).GetFlags() & (GS_FLAG_DYNAMIC_ADD | GS_FLAG_TRANSIENT)) != GS_FLAG_DYNAMIC_ADD)
			continue;

		TiXmlElement defaultStr(DEFAULT_TAG);
		TiXmlText text("");

		EGetSetType gsType = m_gsMetaData->at(i).GetType();
		switch (gsType) {
			case gsInt:
				_itoa_s(*(int*)m_gsMetaData->at(i).GetDynamicDefaultValue(), s, _countof(s), 10);
				text.SetValue(s);
				break;
			case gsDouble:
				sprintf_s(s, _countof(s), "%f", *(double*)m_gsMetaData->at(i).GetDynamicDefaultValue());
				text.SetValue(s);
				break;
			case gsString:
				text.SetValue((char*)m_gsMetaData->at(i).GetDynamicDefaultValue());
				break;
			default:
				LogError("Unexpected gsType=" << gsType);
				jsAssert(false);
				continue;
				break;
		}

		if (defaultStr.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement child(METADATUM_TAG);

		TiXmlElement nameStr(NAME_TAG);
		text.SetValue(m_gsMetaData->at(i).GetDynamicName());
		if (nameStr.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement type(TYPE_TAG);
		_itoa_s(m_gsMetaData->at(i).GetType(), s, _countof(s), 10);
		text.SetValue(s);
		if (type.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		TiXmlElement flags(FLAGS_TAG);
		_itoa_s(m_gsMetaData->at(i).GetFlags(), s, _countof(s), 10);
		text.SetValue(s);
		if (flags.InsertEndChild(text) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}

		if (child.InsertEndChild(nameStr) == 0 ||
			child.InsertEndChild(type) == 0 ||
			child.InsertEndChild(flags) == 0 ||
			child.InsertEndChild(defaultStr) == 0 ||
			rootNode->InsertEndChild(child) == 0) {
			throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
		}
	}

	if (!dom.SaveFile()) {
		throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);
	}
}

void gsMetaDatum::GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const {
	pMemSizeGadget->AddObject(dynamicName);
	pMemSizeGadget->AddObjectSizeof(dynamicDefaultValue);
}

void gsMetaDatum::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		GetMemSizeofInternals(pMemSizeGadget);
	}
}

void GetSet::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		GetMemSizeofInternals(pMemSizeGadget);
	}
}

void GetSet::GetMemSizeofInternals(IMemSizeGadget* pMemSizeGadget) const {
	pMemSizeGadget->AddObject(m_gsMetaData);
	pMemSizeGadget->AddObject(m_gsValues);
}

} // namespace KEP
