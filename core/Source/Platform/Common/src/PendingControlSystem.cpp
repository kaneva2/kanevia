///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IClientEngine.h"
#include "Event\Base\IEvent.h"
#include "Event/Base/IDispatcher.h"
#include "ControlsClass.h"
#include "PendingControlSystem.h"

namespace KEP {

void CPendingControlSystem::CheckCaptureControl(const MouseState& mouseState, const KeyboardState& keyboardState) {
	if (m_index == -1)
		return;

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	ControlDBList* controls_list = pICE->GetControlsList();
	if (!controls_list)
		return;

	POSITION cntPos = controls_list->FindIndex(0);
	if (!cntPos)
		return;

	ControlDBObj* cObj = (ControlDBObj*)controls_list->GetAt(cntPos);
	if (!cObj || !cObj->m_controlInfoList)
		return;

	// Get String For First Key Down -or-> Mouse Button Down -or-> Mouse Wheel Action
	CStringA keyString = cObj->m_controlInfoList->GetCurrentKeyString(mouseState, keyboardState);
	if (keyString.GetLength() <= 0)
		return;

	CStringA actionString;
	int inventoryGlid = -1;
	int miscInt2 = 0;
	int miscInt3 = 0;
	int miscInt4 = 0;
	actionString = cObj->m_controlInfoList->ReturnActionByIndexAdv(
		m_index,
		inventoryGlid,
		miscInt2,
		miscInt3,
		miscInt4,
		pICE->GetSkillDatabase());

	cObj->m_controlInfoList->AddControlEvent(
		keyString,
		actionString,
		inventoryGlid,
		miscInt2,
		miscInt3,
		miscInt4,
		!cObj->m_allowMultiBind,
		TRUE);

	// send generic event to scripting
	IDispatcher* dispatcher = pICE->GetDispatcher();
	if (!dispatcher)
		return;

	IEvent* e = dispatcher->MakeEvent(pICE->GetControlsUpdatedEventID());
	if (!e)
		return;

	(*e->OutBuffer()) << m_index;
	dispatcher->QueueEvent(e);

	m_index = -1;
}

} // namespace KEP