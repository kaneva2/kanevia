///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <js.h>

#include "ActionEvent.h"

#include "Event/Base/IEncodingFactory.h"

using namespace KEP;

DECLARE_CLASS_INFO(ActionEvent)

ActionEvent::ActionEvent(ControlObjActionState const &act_state, CMovementObj *pMO, ControlInfoObj *control_info_obj, TimeMs cur_time) :
		IEvent(ClassId()) {
	jsVerifyDo(pMO != 0, return );
	jsVerifyDo(control_info_obj != 0, return );

	m_movement_obj = pMO;
	m_control_info_obj = control_info_obj;

	// set filter to command action
	m_header.SetFilter((int)control_info_obj->m_cmdAction);

	// pack the action state and cur_time into the buffer
	(*OutBuffer()) << (int)act_state << cur_time;
}

EVENT_CONSTRUCTOR_PROTOTYPE(ActionEvent) {
}

void ActionEvent::ExtractInfo(ControlObjActionState &act_state, CMovementObj **ppMO_out, ControlInfoObj **control_info_obj, TimeMs &cur_time) {
	*ppMO_out = m_movement_obj;
	*control_info_obj = m_control_info_obj;

	int state = 0;
	(*InBuffer()) >> state >> cur_time;
	act_state = (ControlObjActionState)state;
}
