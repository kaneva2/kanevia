///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CVersionInfo.h"

// Tools
#include "js.h"
#include "jsEnumFiles.h"

// KepUtils
#include "Common/KepUtil/Algorithms.h"
#include "Common/include/KEPHelpers.h"
using namespace KEP;

#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

#include "LogHelper.h"
static LogInstance("Instance");

// TOKEN defintions for VersionInfo files
const char HEADER_DELIM = '=';
const char FILE_DELIM = ',';

// v2-v3
const string TOKEN_HEADER = "[CURRENT]";
const string TOKEN_FILES = "[FILES]";
enum HEADER_SECTIONS {
	SECTION_NOTSET = 0,
	SECTION_HEADER = 1,
	SECTION_FILES = 2
};

const string HEADER_UGPV = "UGPV";
const string HEADER_VERSION = "VERSION";
const string HEADER_TOTALFILES = "TOTALFILES";
const string HEADER_TOTALSIZE = "TOTALSIZE";

// (VI only)
const string HEADER_VI_VERSION = "VI_VERSION";
const string HEADER_TEMPLATEVERSION = "TEMPLATE_VERSION";
const string HEADER_TEMPLATETYPE = "TEMPLATE_TYPE";
const string HEADER_VIFILETYPE = "VIFILE_TYPE";
const string HEADER_GAMEID = "GAMEID";

CVersionInfo::CVersionInfo() {
	clear();

	m_headerFields.clear();
	m_headerFields.insert(make_pair(HEADER_UGPV, ""));
	m_headerFields.insert(make_pair(HEADER_VERSION, ""));
	m_headerFields.insert(make_pair(HEADER_VI_VERSION, ""));
	m_headerFields.insert(make_pair(HEADER_TOTALFILES, ""));
	m_headerFields.insert(make_pair(HEADER_TOTALSIZE, ""));
	m_headerFields.insert(make_pair(HEADER_TEMPLATEVERSION, ""));
	m_headerFields.insert(make_pair(HEADER_TEMPLATETYPE, ""));
	m_headerFields.insert(make_pair(HEADER_VIFILETYPE, ""));
	m_headerFields.insert(make_pair(HEADER_GAMEID, ""));
}

void CVersionInfo::clear() {
	m_basePath.clear();
	m_configFileName.clear();
	for (auto it : m_headerFields)
		it.second.clear();
	m_fileEntries.clear();
}

bool CVersionInfo::isValid() {
	// ALL Vifile's need these headers
	if (m_headerFields[HEADER_VERSION] == "") {
		LogError("FAILED - Missing HEADER_VERSION");
		return false;
	}
	if (get_TotalFiles() < 0) {
		LogError("FAILED - Missing HEADER_TOTALFILES");
		return false;
	}
	if (get_TotalSize() < 0) {
		LogError("FAILED - Missing HEADER_TOTALSIZE");
		return false;
	}

	// VI Version specific
	// older version no addtional check
	if (m_headerFields[HEADER_VI_VERSION] != "") {
		if (m_headerFields[HEADER_VIFILETYPE] == "WOK") {
		} else if (m_headerFields[HEADER_VIFILETYPE] == "TEMPLATE") {
			if (m_headerFields[HEADER_TEMPLATEVERSION] == "") {
				LogError("FAILED - Missing HEADER_TEMPLATEVERSION");
				return false;
			}
			if (m_headerFields[HEADER_TEMPLATETYPE] == "") {
				LogError("FAILED - Missing HEADER_TEMPLATETYPE");
				return false;
			}

		} else if (m_headerFields[HEADER_VIFILETYPE] == "WORLD") {
			if (m_headerFields[HEADER_GAMEID] == "") {
				LogError("FAILED - Missing GAMEID");
				return false;
			}
		} else {
			LogError("FAILED - Unknown VIFILETYPE");
			return false;
		}
	}

	return true;
}

bool CVersionInfo::isEquivalent(CVersionInfo& rhs) {
	const FileEntryMap& rhsEntries = rhs.getFileEntries();

	if (m_fileEntries.size() != rhsEntries.size())
		return false;

	FileEntryMap::const_iterator match;
	for (const auto& it : m_fileEntries) {
		match = rhsEntries.find(it.first);
		if (match == rhsEntries.end()) {
			return false;
		} else {
			if (it.second.get_Filename() != match->second.get_Filename() || it.second.get_MD5() == match->second.get_MD5()) {
				return false;
			}
		}
	}
	return true;
}

bool CVersionInfo::loadFile(const string& filePathName) {
	LogInfo("'" << filePathName << "'");

	// Clear VI Fields
	clear();

	// File Exists?
	if (!FileHelper::Exists(filePathName)) {
		LogError("File Not Found - '" << filePathName << "'");
		return false;
	}

	m_basePath = StrStripFileNameExt(filePathName);
	m_configFileName = filePathName;

	HEADER_SECTIONS VIFSections = SECTION_NOTSET;

	vector<char> unwanted;
	unwanted.push_back('\t');
	unwanted.push_back('\r');
	unwanted.push_back('\n');

	ifstream configStream;
	string line;
	vector<string> lineParts;
	map<string, string>::iterator itr;

	// Read config file
	try {
		configStream.open(Utf8ToUtf16(m_configFileName));
		while (configStream.good()) {
			getline(configStream, line);

			// sanitize input line
			line = StringHelpers::trim(line);
			line = StringHelpers::strip(line, unwanted);
			if (line.empty())
				continue;

			// check if we hit section boundary
			if (line == TOKEN_HEADER) {
				VIFSections = SECTION_HEADER;
				continue;
			} else if (line == TOKEN_FILES) {
				VIFSections = SECTION_FILES;
				continue;
			}

			// process section line
			switch (VIFSections) {
				case SECTION_HEADER:

					// split the line on the delim
					lineParts.clear();
					lineParts = StringHelpers::split(line, HEADER_DELIM);
					if (lineParts.size() != 2) {
						LogWarn("WARNING - Corrupt SECTION_HEADER Delimiter");
						continue;
					}

					// do assigment from parts
					itr = m_headerFields.find(lineParts[0]);
					if (itr != m_headerFields.end()) {
						itr->second = lineParts[1];
					} else {
						//						LogWarn("WARNING - Corrupt SECTION_HEADER Token");
					}
					break;

				case SECTION_FILES:
					CVIFileEntry newEntry;
					newEntry.fromRawLine(line);
					m_fileEntries.insert(make_pair(newEntry.get_Filename(), newEntry));
					break;
			}
		}

	} catch (ifstream::failure e) {
		configStream.close();
		LogError("FAILED - Caught Exception");
		return false;
	}

	// DRF - Added - Close File
	configStream.close();

	// return validation of file
	return isValid();
}

bool CVersionInfo::loadFile(const string& basePath, const string& fileName) {
	bool isDir;
	jsFileExists(basePath.c_str(), &isDir);
	if (!isDir) {
		LogError("FAILED - Invalid Directory '" << basePath << "'");
		return false;
	}

	string fullyQualiedFileName = basePath + "\\" + fileName;
	fullyQualiedFileName = StringHelpers::findandreplace(fullyQualiedFileName, "\\\\", "\\");

	if (jsFileExists(fullyQualiedFileName.c_str(), &isDir) == false) {
		LogError("FAILED - Missing Config File '" << fullyQualiedFileName << "'");
		return false;
	}

	return loadFile(fullyQualiedFileName);
}

bool CVersionInfo::writeFile(const string& filePathName) {
	if (!isValid())
		return false;

	// split up the filePathName into path and file parts
	FileName fileParts = FileName::parse(filePathName);
	string basePath = fileParts.toString(FileName::Drive | FileName::Path);
	string fileName = fileParts.toNameExt();

	bool isDir;
	jsFileExists(basePath.c_str(), &isDir);
	if (!isDir) {
		LogError("FAILED - Invalid Directory '" << basePath << "'");
		return false;
	}

	// argument is valid so read the file
	m_basePath = basePath;
	m_configFileName = basePath + "\\" + fileName;

	// write the file
	ofstream configFile(m_configFileName);
	if (!configFile) {
		LogError("FAILED - Missing Config File '" << m_configFileName << "'");
		return false;
	}

	// fields for all versions
	configFile << TOKEN_HEADER << endl;
	configFile << HEADER_VERSION << HEADER_DELIM << get_Version() << endl;
	configFile << HEADER_TOTALFILES << HEADER_DELIM << get_TotalFiles() << endl;
	configFile << HEADER_TOTALSIZE << HEADER_DELIM << get_TotalSize() << endl;

	// optional fields
	if (get_Ugpv() != "")
		configFile << HEADER_UGPV << HEADER_DELIM << get_Ugpv() << endl;

	// (v3 only)
	if (m_headerFields[HEADER_VI_VERSION] != "") {
		configFile << HEADER_VI_VERSION << HEADER_DELIM << get_VIVersion() << endl;
		configFile << HEADER_VIFILETYPE << HEADER_DELIM << get_VIFType() << endl;

		if (m_headerFields[HEADER_VIFILETYPE] == "WOK") {
		} else if (m_headerFields[HEADER_VIFILETYPE] == "TEMPLATE") {
			configFile << HEADER_TEMPLATEVERSION << HEADER_DELIM << get_TemplateVersion() << endl;
			configFile << HEADER_TEMPLATETYPE << HEADER_DELIM << get_TemplateType() << endl;
		} else if (m_headerFields[HEADER_VIFILETYPE] == "WORLD") {
			configFile << HEADER_GAMEID << HEADER_DELIM << get_GameId() << endl;
		}
	}

	// All versions
	configFile << TOKEN_FILES << endl;
	for (auto itr = m_fileEntries.begin(); itr != m_fileEntries.end(); itr++)
		configFile << itr->second.asRawLine() << endl;
	configFile.close();

	return true;
}

bool CVersionInfo::writeFile(const string& basePath, const string& fileName) {
	bool isDir;
	jsFileExists(basePath.c_str(), &isDir);
	if (!isDir) {
		LogError("FAILED - Invalid Directory '" << basePath << "'");
		return false;
	}

	string fullyQualiedFileName = basePath + "\\" + fileName;

	return writeFile(fullyQualiedFileName);
}

string CVersionInfo::get_Ugpv() const {
	return getMapValue(HEADER_UGPV);
}

void CVersionInfo::set_Ugpv(const string& newval) {
	m_headerFields[HEADER_UGPV] = newval;
}

string CVersionInfo::get_Version() const {
	return getMapValue(HEADER_VERSION);
}

void CVersionInfo::set_Version(const string& newval) {
	m_headerFields[HEADER_VERSION] = newval;
}

string CVersionInfo::get_VIVersion() const {
	return getMapValue(HEADER_VI_VERSION);
}

void CVersionInfo::set_VIVersion(const string& newval) {
	m_headerFields[HEADER_VI_VERSION] = newval;
}

string CVersionInfo::get_TemplateVersion() const {
	return getMapValue(HEADER_TEMPLATEVERSION);
}

void CVersionInfo::set_TemplateVersion(const string& newval) {
	m_headerFields[HEADER_TEMPLATEVERSION] = newval;
}

string CVersionInfo::get_TemplateType() const {
	return getMapValue(HEADER_TEMPLATETYPE);
}

void CVersionInfo::set_TemplateType(const string& newval) {
	m_headerFields[HEADER_TEMPLATETYPE] = newval;
}

string CVersionInfo::get_VIFType() const {
	return getMapValue(HEADER_VIFILETYPE);
}

void CVersionInfo::set_VIFType(const string& newval) {
	m_headerFields[HEADER_VIFILETYPE] = newval;
}

string CVersionInfo::get_GameId() const {
	return getMapValue(HEADER_GAMEID);
}

void CVersionInfo::set_GameId(const string& newval) {
	m_headerFields[HEADER_GAMEID] = newval;
}

long CVersionInfo::get_TotalFiles() {
	long fileEntries = (long)m_fileEntries.size();
	m_headerFields[HEADER_TOTALFILES] = to_string(fileEntries);
	return fileEntries;
}

long CVersionInfo::get_TotalSize() {
	long ttlSize = 0;
	for (const auto& it : m_fileEntries)
		ttlSize += it.second.get_ServerFileSize();
	m_headerFields[HEADER_TOTALSIZE] = to_string(ttlSize);
	return ttlSize;
}

FileEntryMap& CVersionInfo::getFileEntries() {
	return m_fileEntries;
}

std::string CVersionInfo::getMapValue(const std::string& key) const {
	map<string, string>::const_iterator itr = m_headerFields.find(key);
	return (itr != m_headerFields.end() ? itr->second : "");
}

CVIFileEntry::CVIFileEntry() {
	clear();
}

bool CVIFileEntry::operator==(const CVIFileEntry& rhs) const {
	return (get_Filename() == rhs.get_Filename() && get_MD5() == rhs.get_MD5());
}

bool CVIFileEntry::operator!=(const CVIFileEntry& rhs) const {
	return !(*this == rhs);
}

void CVIFileEntry::clear() {
	m_rawLine.clear();
	m_fldFilename.clear();
	m_fldMD5.clear();
	m_fldDirectory.clear();
	m_fldServerFile.clear();
	m_fldRegister.clear();
	m_fldServerFileSize = "0";
	m_fldRequired.clear();
	m_fldCommand.clear();
};

bool CVIFileEntry::fromRawLine(const string& rawline) {
	bool retval(true);

	clear();

	if (rawline.empty()) {
		LogError("FAILED - Parse Empty Line");
		retval = false;
	} else {
		std::vector<string> buf = StringHelpers::split(rawline, '=');
		if (buf.size() == 1) {
			LogError("FAILED - Parse Missing Token");
			retval = false;
		} else {
			std::vector<string> buf2 = StringHelpers::split(buf[1], ',');
			if (buf2.size() < 7) {
				LogError("FAILED - Parse Corrupt Line '" << buf[1] << "'");
				retval = false;
			} else {
				m_fldFilename = buf[0];
				m_fldMD5 = buf2[0];
				m_fldDirectory = buf2[1];
				m_fldServerFile = buf2[2];
				m_fldRegister = buf2[3];
				m_fldServerFileSize = buf2[4];
				m_fldRequired = buf2[5];
				m_fldCommand = buf2[6];
			}
		}
	}
	return retval;
}

string CVIFileEntry::asRawLine() {
	m_rawLine.clear();

	std::stringstream ss;
	ss << m_fldFilename << "="
	   << m_fldMD5 << FILE_DELIM
	   << m_fldDirectory << FILE_DELIM
	   << m_fldServerFile << FILE_DELIM
	   << m_fldRegister << FILE_DELIM
	   << m_fldServerFileSize << FILE_DELIM
	   << m_fldRequired;

	if (m_fldCommand != "") {
		ss << FILE_DELIM << m_fldCommand;
	}

	m_rawLine = ss.str();
	return m_rawLine;
}

string CVIFileEntry::FileNameFull() const {
	ostringstream retval;
	retval << get_Directory() << "\\" << fileNameOnly(get_Filename());
	return retval.str();
}

string CVIFileEntry::ServerFileNameFull() const {
	ostringstream retval;
	retval << get_Directory() << "\\" << fileNameOnly(get_ServerFile());
	return retval.str();
}

string CVIFileEntry::FileNameOnly() const {
	return fileNameOnly(get_Filename());
}

string CVIFileEntry::ServerFileNameOnly() const {
	return fileNameOnly(get_ServerFile());
}

bool CVIFileEntry::isPackFile(const string& filename) const {
	return StrSameFileExt(filename, "pak");
}

bool CVIFileEntry::isCompressedFile(const string& filename) const {
	return (StrSameFileExt(filename, "zip") || StrSameFileExt(filename, "gz") || StrSameFileExt(filename, "gzip") || StrSameFileExt(filename, "lz") || StrSameFileExt(filename, "lzma") || StrSameFileExt(filename, "7z") || StrSameFileExt(filename, "7zip"));
}

string CVIFileEntry::get_Filename() const {
	return m_fldFilename;
}

void CVIFileEntry::set_Filename(const string& newval) {
	m_fldFilename = newval;
}

string CVIFileEntry::get_MD5() const {
	return m_fldMD5;
}

void CVIFileEntry::set_MD5(const string& newval) {
	m_fldMD5 = newval;
}

string CVIFileEntry::get_Directory() const {
	return m_fldDirectory;
}

void CVIFileEntry::set_Directory(const string& newval) {
	m_fldDirectory = newval;
}

string CVIFileEntry::get_ServerFile() const {
	return m_fldServerFile;
}

void CVIFileEntry::set_ServerFile(const string& newval) {
	m_fldServerFile = newval;
}

bool CVIFileEntry::get_Register() const {
	return boolFromYesNo(m_fldRegister);
}

void CVIFileEntry::set_Register(bool newval) {
	m_fldRegister = boolToYesNo(newval);
}

void CVIFileEntry::set_Register(const string& newval) {
	m_fldRegister = newval;
}

size_t CVIFileEntry::get_ServerFileSize() const {
	return stoul(m_fldServerFileSize);
}

void CVIFileEntry::set_ServerFileSize(size_t newval) {
	m_fldServerFileSize = to_string(newval);
}

void CVIFileEntry::set_ServerFileSize(const string& newval) {
	m_fldServerFileSize = newval;
}

bool CVIFileEntry::get_Required() const {
	return boolFromYesNo(m_fldRequired);
}

void CVIFileEntry::set_Required(bool newval) {
	m_fldRequired = boolToYesNo(newval);
}

void CVIFileEntry::set_Required(const string& newval) {
	m_fldRequired = newval;
}

string CVIFileEntry::get_Command() const {
	return m_fldCommand;
}

void CVIFileEntry::set_Command(const string& newval) {
	m_fldCommand = newval;
}

string CVIFileEntry::fileNameOnly(const string& fileName) const {
	string delim("---");

	// Strip fileName of the unwanted prefix
	string retval = fileName;
	unsigned start = fileName.find(delim);
	if (start != string::npos) {
		retval = fileName.substr(start + delim.length(), fileName.length());
	}

	return retval;
}

bool CVIFileEntry::boolFromYesNo(const string& value) const {
	return (value == "Yes") ? true : false;
}

string CVIFileEntry::boolToYesNo(bool value) const {
	return (value == true) ? "Yes" : "No";
}

void CVIFileEntry::debug_dump() {
	std::cout << "\tfilename[" << m_fldFilename << "]" << std::endl
			  << "\tmd5[" << m_fldMD5 << "]" << std::endl
			  << "\tdirectory[" << m_fldDirectory << "]" << std::endl
			  << "\tserverfile[" << m_fldServerFile << "]" << std::endl
			  << "\tregister[" << m_fldRegister << "]" << std::endl
			  << "\tserverFileSize[" << m_fldServerFileSize << "]" << std::endl
			  << "\trequired[" << m_fldRequired << "]" << std::endl
			  << "\tcommand[" << m_fldCommand << "]" << std::endl;
}
