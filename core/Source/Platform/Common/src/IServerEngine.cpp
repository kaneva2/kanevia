///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "IServerEngine.h"
#include "Core/Util\ObjectRegistry.h"
#include "Core/Util/ChainedException.h"

namespace KEP {

const char* IServerEngine::SERVERENGINE = "ServerEngine";

IServerEngine::IServerEngine() :
		IGame(eEngineType::Server) {
	ObjectRegistry& registry = ObjectRegistry::instance();
	if (registry.query(SERVERENGINE) != NULL) {
		// There should only ever be one server engine
		throw ChainedException(SOURCELOCATION, "Attempt to create multiple instances of IServerEngine");
	}
	registry.registerObject("ServerEngine", (void*)this);
}

IServerEngine* IServerEngine::GetInstance() {
	ObjectRegistry& registry = ObjectRegistry::instance();
	if (registry.query(SERVERENGINE) == NULL) {
		throw ChainedException(SOURCELOCATION, "ServerEngine does not exist");
	}
	return (IServerEngine*)registry.query(SERVERENGINE);
}

IServerEngine::~IServerEngine() {
	ObjectRegistry& registry = ObjectRegistry::instance();
	registry.removeObject(SERVERENGINE);
}

} // namespace KEP