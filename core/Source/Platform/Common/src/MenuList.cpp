///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IClientEngine.h"
#include "IMenuBlade.h"
#include "iclientbladefactory.h"
#include <direct.h> // for _getcwd
#include "IClientEngine.h"
#include "MenuList.h"
#include "Engine/ClientBlades/imenugui.h"
#include "Event/Events/ScriptServerEvent.h"
#include "Common/include/UnicodeMFCHelpers.h"
#include "LogHelper.h"
#include "../../Engine/ClientBlades/KepMenu/KEPMenu.h"

namespace KEP {

static LogInstance("ClientEngine");

static KEPMenu* g_pKepMenu = NULL; // DRF - KEPMenu Singleton

CMenuList* CMenuList::m_pInstance = 0;

CMenuList* CMenuList::GetInstance() {
	if (!m_pInstance)
		m_pInstance = new CMenuList();
	return m_pInstance;
}

CMenuList::~CMenuList() {
	RemoveAll();

	m_pInstance = 0;
}

std::string CMenuList::GetFilenameByID(int menuID) {
	for (AssetIterator mi = m_menus.begin(); mi != m_menus.end(); ++mi) {
		if (mi->ID == menuID)
			return mi->name;
	}
	return "";
}

std::string CMenuList::Add(MenuCategory category, std::string filename, int menuID) {
	if (menuID > -1) {
		for (AssetIterator mi = m_menus.begin(); mi != m_menus.end(); ++mi) {
			if ((mi->category == category) && (mi->name == filename))
				return mi->name;
		}
	}

	// Throw-back to old code.  Might be able to remove menuID,
	// if I can remove all use of menuID in WoK, but I do
	// need to check for duplicate filenames.
	m_menus.push_back(AssetDef(category, filename, menuID));

	return std::string();
}

void CMenuList::Load(int gameId, bool isChildGame, bool interactive) {
	LogInfo("START");

	// Remove old entries
	RemoveAll();

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	std::string menuDir, scriptDir, templateMenuDir, templateScriptDir, appMenuDir, appScriptDir, localMenuDir, localScriptDir;

	menuDir = pICE->GetMenuDir();
	scriptDir = pICE->GetMenuScriptDir();

	// Both WOK and 3dapp use localDev
	localMenuDir = pICE->getDevToolAssetPath(ScriptServerEvent::MenuXml);
	localScriptDir = pICE->getDevToolAssetPath(ScriptServerEvent::MenuScript);

	if (isChildGame) {
		templateMenuDir = pICE->GetTemplateMenuDir();
		templateScriptDir = pICE->GetTemplateScriptDir();

		appMenuDir = pICE->GetAppMenuDir();
		appScriptDir = pICE->GetAppScriptDir();
	}

	// Initialize KEP Menu Blade
	auto pMB = pICE->GetBladeFactory()->GetMenuBlade();
	if (pMB) {
		if (!g_pKepMenu)
			g_pKepMenu = dynamic_cast<KEPMenu*>(pMB);

		// Re-Initialize KEP Menu (clear menu paths, clear textures)
		pMB->ReInit(isChildGame, true, true);

		// Add Menu Paths
		if (isChildGame) {
			// Template menus - lowest priority (also add textures)
			if (!templateMenuDir.empty()) // Empty if legacy app
				pMB->AddMenuPaths(templateMenuDir, templateScriptDir, false, true);

			// Custom app menus override template ones (also add textures)
			if (!appMenuDir.empty()) // Empty if not customized
				pMB->AddMenuPaths(appMenuDir, appScriptDir, false, true); // App patch which overrides template

			// Local changes, override both template and app (no textures)
			if (!localMenuDir.empty())
				pMB->AddMenuPaths(localMenuDir, localScriptDir, false, false); // Don't load texture list from LocalDev
		}

		// WOK patch overrides everything else (also add textures)
		pMB->AddMenuPaths(menuDir, scriptDir, true, true);

		if (!isChildGame) {
			// WOK LocalDev overrides WOK patch, if in WOK (no textures)
			if (!localMenuDir.empty())
				pMB->AddMenuPaths(localMenuDir, localScriptDir, true, false); // Don't load texture list from LocalDev
		}
	} else {
		LogWarn("KEPMenu blade not found");
	}

	// Load files from local dev first and then look for more in the
	// 3d apps patch location.  Duplicates are ignored, so that menu_list
	// (used by the menu browser in a 3d app) shows menus in development
	// and patched menus that have not been edited yet.
#define MENU_FILES_WILDCARD "*.xml"
#define SCRIPT_FILES_WILDCARD "*.lua"

	//Load menu/script file names for Menu Editor
	if (isChildGame) {
		if (!localMenuDir.empty())
			LoadFromFolder(MENU_WORLD_LOCALDEV_ONLY, PathAdd(localMenuDir, MENU_FILES_WILDCARD), interactive);
		if (!appMenuDir.empty())
			LoadFromFolder(MENU_WORLD_CUSTOM_ONLY, PathAdd(appMenuDir, MENU_FILES_WILDCARD), interactive);
		if (!templateMenuDir.empty())
			LoadFromFolder(MENU_WORLD_TEMPLATE_ONLY, PathAdd(templateMenuDir, MENU_FILES_WILDCARD), interactive);

		if (!localScriptDir.empty())
			LoadScriptsFromFolder(MENU_WORLD_LOCALDEV_ONLY, PathAdd(localScriptDir, SCRIPT_FILES_WILDCARD));
		if (!appScriptDir.empty())
			LoadScriptsFromFolder(MENU_WORLD_CUSTOM_ONLY, PathAdd(appScriptDir, SCRIPT_FILES_WILDCARD));
		if (!templateScriptDir.empty())
			LoadScriptsFromFolder(MENU_WORLD_TEMPLATE_ONLY, PathAdd(templateScriptDir, SCRIPT_FILES_WILDCARD));
	} else {
		// Unpublished WOK menus
		if (!localMenuDir.empty())
			LoadFromFolder(MENU_WOK_LOCALDEV_ONLY, PathAdd(localMenuDir, MENU_FILES_WILDCARD), interactive);

		// Published WOK Menus
		LoadFromFolder(MENU_WOK_PATCH_ONLY, PathAdd(menuDir, MENU_FILES_WILDCARD), interactive);

		// Unpublished WOK scripts
		if (!localScriptDir.empty())
			LoadScriptsFromFolder(MENU_WOK_LOCALDEV_ONLY, PathAdd(localScriptDir, SCRIPT_FILES_WILDCARD));

		// Published WOK scripts
		LoadScriptsFromFolder(MENU_WOK_PATCH_ONLY, PathAdd(scriptDir, SCRIPT_FILES_WILDCARD));
	}
	LogInfo("END");
}

void CMenuList::LoadFromFolder(MenuCategory category, const std::string& file_mask, bool interactive) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	bool done = false;

	WIN32_FIND_DATA find_data;
	HANDLE handle = ::FindFirstFileW(Utf8ToUtf16(file_mask).c_str(), &find_data);

	if (INVALID_HANDLE_VALUE == handle)
		done = true;

	int loadedCount = 0;
	while (!done) {
		std::string filename = Utf16ToUtf8(find_data.cFileName);

		// don't add the default xml to the list and also don't add any temporary files
		// that were not cleaned up
		if ((filename != "default_elements.xml") &&
			(filename.find("~$") == std::string::npos) &&
			((find_data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0)) {
			auto pMB = pICE->GetBladeFactory()->GetMenuBlade();
			if (pMB) {
				// keep this in for now, but it's not code I want to carry forward, even if some of the other CMenuList_Depricated code is.
				// This code should only be used to build up a list of the old menus that some internal systems still reference by ID.
				//
				// since the dialogs aren't loaded yet, GetDialogID reads the data from disk directly, in order to populate the filename-ID map
				// the ID isn't used by 3d apps, but I still need to add the menus to the vector so that the menu editor works (we have an API
				// that returns the size and content of the m_menus list managed by this object.
				int ID = pMB->GetDialogID(filename);
				const std::string& conflictingMenu = Add(category, filename, ID);
				if (conflictingMenu.size() != 0) { // will only be true for a duplicate menu that has an ID >= 0
					char s[512];
					_snprintf_s(s, _countof(s), _TRUNCATE, "Menu \"%s\" has dupe id with \"%s\"", filename.c_str(), conflictingMenu.c_str());
					if (interactive)
						AfxMessageBoxUtf8(s);
					else {
						LogWarn(s);
					}
				} else
					loadedCount++;
			}
		}

		BOOL res = ::FindNextFile(handle, &find_data);
		if (!res)
			done = true;
	}
	::FindClose(handle);
}

void CMenuList::LoadScriptsFromFolder(MenuCategory category, const std::string& file_mask) {
	WIN32_FIND_DATA find_data;
	HANDLE handle;
	bool done = false;

	handle = ::FindFirstFileW(Utf8ToUtf16(file_mask).c_str(), &find_data);
	if (INVALID_HANDLE_VALUE == handle)
		done = true;

	while (!done) {
		BOOL res = true;
		if ((find_data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0)
			m_scripts.push_back(AssetDef(category, Utf16ToUtf8(find_data.cFileName)));
		res = ::FindNextFile(handle, &find_data);
		if (!res)
			done = true;
	}
	::FindClose(handle);
}

int CMenuList::CountAssets(const std::vector<AssetDef>& assets, int categoryFilter) const {
	int count = 0;
	for (AssetIterator it = assets.begin(); it != assets.end(); ++it)
		if (categoryFilter == MENU_ALL || (it->category & categoryFilter) != 0)
			count++;
	return count;
}

std::string CMenuList::GetAssetName(const std::vector<AssetDef>& assets, int categoryFilter, int index) const {
	int count = 0;
	for (AssetIterator it = assets.begin(); it != assets.end(); ++it) {
		if (categoryFilter == MENU_ALL || (it->category & categoryFilter) != 0) {
			if (index == count)
				return it->name;
			count++;
		}
	}
	return "";
}

} // namespace KEP