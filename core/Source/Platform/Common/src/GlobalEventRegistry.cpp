///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <map>
#include <string>
#include "Core/Util/fast_mutex.h"
#include "Event\Base\EventHeader.h"

namespace KEP {

typedef std::map<std::string, USHORT> EventMap;
typedef std::map<USHORT, std::string> EventMap_Deref;

static fast_recursive_mutex sync;
static USHORT s_nextEventId = 0;
static EventMap s_eventMap;
static EventMap_Deref s_eventMap_deref;

__declspec(dllexport) EVENT_ID GlobalEventRegister(const std::string& name) {
	std::lock_guard<fast_recursive_mutex> lock(sync);
	USHORT ret = USHRT_MAX;
	auto itr = s_eventMap.find(name);
	if (itr == s_eventMap.end()) {
		ret = s_nextEventId++;
		s_eventMap[name] = ret;
		s_eventMap_deref[ret] = name; // drf - added
	} else {
		ret = itr->second;
	}
	return (EVENT_ID)ret;
}

__declspec(dllexport) std::string GlobalEventName(EVENT_ID eventId) {
	std::lock_guard<fast_recursive_mutex> lock(sync);
	auto itr = s_eventMap_deref.find(eventId);
	return (itr == s_eventMap_deref.end()) ? "EVENT_UNKNOWN" : itr->second;
}

} // namespace KEP