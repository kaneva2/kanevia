
#line 2 "shadowmap_post.fx"
//-----------------------------------------------------------------------------
// File: Shadowmap_post.fx
//
// Desc: Effect file for Shadowmap generation and rendering of scene using
//		the generated shadowmap
//
// Copyright (c) Kaneva Inc.
//-----------------------------------------------------------------------------

float4x4 g_mWorld : World;
float4x4 g_mWorldIT : WorldInverseTranspose;
float4x4 g_mViewProj : ViewProjection;

// light parameters
float4x4 g_mLightViewProj;  // Transform from view space to light projection space
float3   g_world_LightPos;  // Light position in view space
float3   g_world_LightDir;  // Light direction in view space
float4   g_vLightAmbient = float4( 0.3f, 0.3f, 0.3f, 1.0f );  // Use an ambient light of 0.3
float    g_fCosTheta = 0.0f;  // Cosine of theta of the spot light (90 degrees on each side)

// depth bias
#define SHADOW_EPSILON 0.003f

// shadowmap texture parameters
#define SMAP_SIZE 512
texture  g_txShadow;
int      g_txShadowWidth = SMAP_SIZE;
int      g_txShadowHeight = SMAP_SIZE;

// shadowmap texture sampler
sampler2D g_samShadow =
sampler_state
{
    Texture = <g_txShadow>;
    MinFilter = Point; // use point sampling because it's a floating point texture
    MagFilter = Point; // do percentage closer filtering in Pixel Shader
    MipFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};


//-----------------------------------------------------------------------------
// Vertex Shader: GenShadowmapVS
// Desc: Process vertex to generate the shadow map
//-----------------------------------------------------------------------------
void GenShadowmapVS( float4 Pos : POSITION,
                 float3 Normal : NORMAL,
                 out float4 oPos : POSITION,
                 out float2 Depth : TEXCOORD0 )
{
    // Compute the projected coordinates
    float4 wPos = mul( Pos, g_mWorld );
    oPos = mul( wPos, g_mViewProj );
    // Store z and w in our spare texcoord
    Depth.xy = oPos.zw;
}

//-----------------------------------------------------------------------------
// Pixel Shader: GenShadowmapPS
// Desc: Process pixel to output perspective projected depth to the shadow map
//-----------------------------------------------------------------------------
void GenShadowmapPS( float2 Depth : TEXCOORD0,
                out float4 Color : COLOR )
{
    // Depth is z / w
    Color = Depth.x / Depth.y;
}



//-----------------------------------------------------------------------------
// Vertex Shader: UseShadowmapPS
// Desc: Process vertex to output its position in camera space and light space
// Generate a color that indicates the amount of light received from this light
//  (between 0.0 and 1.0)
// Use this in a last pass to modulate the scene and producing shadows
//-----------------------------------------------------------------------------
void UseShadowmapVS( float4 iPos : POSITION,
				float3 iNormal : NORMAL,
                out float4 camProj_Pos : POSITION,
                out float3 world_Normal : TEXCOORD0,
                out float4 world_Pos: TEXCOORD1,
                out float4 lightProj_Pos : TEXCOORD2 )
{
	world_Pos = mul( iPos, g_mWorld );
	camProj_Pos = mul( world_Pos, g_mViewProj );
    lightProj_Pos = mul( world_Pos, g_mLightViewProj );

//#define COMP_TC_IN_VS
#ifdef COMP_TC_IN_VS
    //transform from RT space to texture space.
    lightProj_Pos.xy = 0.5 * lightProj_Pos.xy / lightProj_Pos.w + float2( 0.5, 0.5 );
    lightProj_Pos.y = 1.0f - lightProj_Pos.y;
#endif
    
    world_Normal = mul( iNormal, (float3x3)g_mWorldIT );
}

//-----------------------------------------------------------------------------
// Pixel Shader: UseShadowmapPS
// Desc: Process pixel to do post scene render shadow map darkening
//-----------------------------------------------------------------------------
void UseShadowmapPS( float3 world_Normal   : TEXCOORD0, 
					 float4 world_Pos      : TEXCOORD1,
					 float4 lightProj_Pos  : TEXCOORD2,
					 out float4 Color : COLOR )
{
    // world_DirFromLight is the unit vector from the light to this point
    float3 world_DirFromLight = normalize( float3( world_Pos - g_world_LightPos ) );

    float LightAmount = 1.0f;
    if( (dot( -world_DirFromLight, normalize( world_Normal )) > 0) 
		&& dot( world_DirFromLight, g_world_LightDir ) > g_fCosTheta ) 
		// Light must face the pixel (within Theta)
    {
        // If pixel is in lit area. Find out if it's
        // in shadow using 2x2 percentage closest filtering
#ifndef COMP_TC_IN_VS
        //transform from RT space to texture space.
        float2 shadowmapTexC = 0.5 * lightProj_Pos.xy / lightProj_Pos.w + float2( 0.5, 0.5 );
        shadowmapTexC.y = 1.0f - shadowmapTexC.y;
#else
		float2 shadowmapTexC = lightProj_Pos.xy;
#endif
        
        // For Percentage Closer Filtering (PCF)
        // transform to texel space
        float2 shadowmapTexelpos = shadowmapTexC * float2(g_txShadowWidth, g_txShadowHeight);
        // Determine the lerp amounts           
        float2 lerps = frac( shadowmapTexelpos );

        //read in bilerp stamp, doing the shadow checks
        float sourcevals[4];
        sourcevals[0] = (tex2D( g_samShadow, shadowmapTexC ) + SHADOW_EPSILON < lightProj_Pos.z / lightProj_Pos.w)? 0.0f: 1.0f;  
        sourcevals[1] = (tex2D( g_samShadow, shadowmapTexC + float2(1.0/g_txShadowWidth, 0) ) + SHADOW_EPSILON < lightProj_Pos.z / lightProj_Pos.w)? 0.0f: 1.0f;  
        sourcevals[2] = (tex2D( g_samShadow, shadowmapTexC + float2(0, 1.0/g_txShadowHeight) ) + SHADOW_EPSILON < lightProj_Pos.z / lightProj_Pos.w)? 0.0f: 1.0f;  
        sourcevals[3] = (tex2D( g_samShadow, shadowmapTexC + float2(1.0/g_txShadowWidth, 1.0/g_txShadowHeight) ) + SHADOW_EPSILON < lightProj_Pos.z / lightProj_Pos.w)? 0.0f: 1.0f;  
        
        // lerp between the shadow values to calculate our light amount
        LightAmount = lerp( lerp( sourcevals[0], sourcevals[1], lerps.x ),
                                  lerp( sourcevals[2], sourcevals[3], lerps.x ),
                                  lerps.y ) + g_vLightAmbient;
                                  
		LightAmount = clamp( LightAmount, 0, 1);
	}
        
    // output the amount of light, we'll modulate the framebuffer using alpha blending
    // use DSTBLEND = SRCCOLOR ; SRCBLEND = ZERO
	Color = float4(LightAmount, LightAmount, LightAmount, 1.0);		
}


//-----------------------------------------------------------------------------
// Technique: GenShadowmap
// Desc: Renders the shadow map
//-----------------------------------------------------------------------------
technique GenShadowmap
{
    pass p0
    {
		AlphaBlendEnable = False;
		ZWriteEnable = True;
		ZEnable = True;
		ZFunc = LESS;
    
        VertexShader = compile vs_1_1 GenShadowmapVS();
        PixelShader = compile ps_2_0 GenShadowmapPS();
    }
}


//-----------------------------------------------------------------------------
// Technique: UseShadowmap
// Desc: Uses the shadow map to overlay shadows on the scene
//-----------------------------------------------------------------------------
technique UseShadowmap
{
    pass p0
    {
		AlphaBlendEnable = True;
		BlendOp = ADD;
		SrcBlend = ZERO;
		DestBlend = SRCCOLOR;
		
		AlphaTestEnable = False;
    
		ZWriteEnable = False;
		ZEnable = True;
		ZFunc = EQUAL;
		
        VertexShader = compile vs_1_1 UseShadowmapVS();
        PixelShader = compile ps_2_0 UseShadowmapPS();
    }
}
