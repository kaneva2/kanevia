/******************************************************************************
 KEPImageToggleButton.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// KEPImageToggleButton.cpp : implementation file
//

#include "stdafx.h"
#include "KEPImageToggleButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKEPImageToggleButton

CKEPImageToggleButton::CKEPImageToggleButton() :
		CKEPImageButton(),
		m_bSelected(FALSE),
		m_bShowDepress(TRUE) {
}

CKEPImageToggleButton::CKEPImageToggleButton(Gdiplus::Color& fontColor, WCHAR* fontName, UINT fontSize, Gdiplus::FontStyle fontStyle) :
		CKEPImageButton(fontColor, fontName, fontSize, fontStyle), m_bSelected(FALSE) {
}

CKEPImageToggleButton::~CKEPImageToggleButton() {
}

IMPLEMENT_DYNAMIC(CKEPImageToggleButton, CKEPImageButton)

BEGIN_MESSAGE_MAP(CKEPImageToggleButton, CKEPImageButton)
//{{AFX_MSG_MAP(CKEPImageToggleButton)
ON_CONTROL_REFLECT_EX(BN_CLICKED, OnClicked)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////
//	CKEPImageToggleButton message handlers

BOOL CKEPImageToggleButton::OnClicked() {
	m_bSelected = !m_bSelected;

	CRect rect;
	GetClientRect(&rect);
	InvalidateRect(&rect, true);

	return FALSE;
}

void CKEPImageToggleButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) {
	BOOL bImage = FALSE;
	BOOL bDisabledNoImage = FALSE;
	BOOL bHoverNoImage = FALSE;
	BOOL bSelectedNoImage = FALSE;
	CRect rcItem = lpDrawItemStruct->rcItem;
	Gdiplus::Rect gdiRect(rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);
	Gdiplus::Graphics graphics(lpDrawItemStruct->hDC);

	// backbuffering
	Gdiplus::Bitmap bmpBackBuffer(rcItem.right, rcItem.bottom);
	Gdiplus::Graphics* backBuffer = Gdiplus::Graphics::FromImage(&bmpBackBuffer);

	if (lpDrawItemStruct->itemState & ODS_SELECTED) {
		// we use the current image to create the selected state
		// the image we loaded for 'selected' is only displayed when
		// our boolean has been set (m_bSelected)

		bSelectedNoImage = TRUE;

		// use the normal image
		if (!m_bSelected && m_imgNormal) {
			backBuffer->DrawImage(m_imgNormal, gdiRect, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight(), Gdiplus::UnitPixel);
			bImage = TRUE;
		}
		// use the selected image
		else if (m_bSelected && m_imgSelected) {
			backBuffer->DrawImage(m_imgSelected, gdiRect, 0, 0, m_imgSelected->GetWidth(), m_imgSelected->GetHeight(), Gdiplus::UnitPixel);
			bImage = TRUE;
		}
	} else if (lpDrawItemStruct->itemState & ODS_DISABLED) {
		if (m_imgDisabled) {
			backBuffer->DrawImage(m_imgDisabled, gdiRect, 0, 0, m_imgDisabled->GetWidth(), m_imgDisabled->GetHeight(), Gdiplus::UnitPixel);
			bImage = TRUE;
		} else {
			bDisabledNoImage = TRUE;

			// use the normal image -- it will be converted to greyscale
			if (m_imgNormal) {
				backBuffer->DrawImage(m_imgNormal, gdiRect, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight(), Gdiplus::UnitPixel);
				bImage = TRUE;
			}
		}
	} else {
		if (m_bHover) {
			if (m_imgHover) {
				backBuffer->DrawImage(m_imgHover, gdiRect, 0, 0, m_imgHover->GetWidth(), m_imgHover->GetHeight(), Gdiplus::UnitPixel);
				bImage = TRUE;
			} else {
				bHoverNoImage = TRUE;

				// use the normal image -- brightness will be adjusted
				if (!m_bSelected && m_imgNormal) {
					backBuffer->DrawImage(m_imgNormal, gdiRect, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight(), Gdiplus::UnitPixel);
					bImage = TRUE;
				}
				// use the selected image -- brightness will be adjusted
				else if (m_bSelected && m_imgSelected) {
					backBuffer->DrawImage(m_imgSelected, gdiRect, 0, 0, m_imgSelected->GetWidth(), m_imgSelected->GetHeight(), Gdiplus::UnitPixel);
					bImage = TRUE;
				}
			}
		} else {
			if (!m_bSelected && m_imgNormal) {
				Gdiplus::ImageAttributes imAtt;
				backBuffer->DrawImage(m_imgNormal, gdiRect, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight(), Gdiplus::UnitPixel);

				bImage = TRUE;
			} else if (m_bSelected && m_imgSelected) {
				Gdiplus::ImageAttributes imAtt;
				backBuffer->DrawImage(m_imgSelected, gdiRect, 0, 0, m_imgSelected->GetWidth(), m_imgSelected->GetHeight(), Gdiplus::UnitPixel);

				bImage = TRUE;
			}
		}
	}

	if (m_bShowCaption) {
		// get the caption of the button
		CStringW sString;
		GetWindowText(sString);

		if (!sString.IsEmpty() && bImage) {
			// convert caption into format that GDI+ likes
			Gdiplus::RectF rectF(0.0, 0.0, (Gdiplus::REAL)rcItem.Width(), (Gdiplus::REAL)rcItem.Height());

			Gdiplus::StringFormat fontFormat;
			// center-justify each line of text.
			fontFormat.SetAlignment(Gdiplus::StringAlignmentCenter);

			// center the block of text (top to bottom) in the rectangle.
			fontFormat.SetLineAlignment(Gdiplus::StringAlignmentCenter);

			// use antialiasing
			backBuffer->SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);

			backBuffer->DrawString(sString, -1, m_font, rectF, &fontFormat, &m_fontBrush);
		}
	}

	//
	// setup some transformation matrices to be used when certain images are not loaded
	//

	// Greyscale:
	// saturation matrix makes use of weights that are assigned to each RGB channel.
	// weights correspond to sensitivity of our eyes to color channels.
	// the standard NTSC weights are defined as 0.299f (red), 0.587f (green) and 0.144f (blue)
	float rWeight = 0.299f;
	float gWeight = 0.587f;
	float bWeight = 0.114f;

	Gdiplus::ColorMatrix cMatrixGreyscale = {
		rWeight, rWeight, rWeight, 0.0f, 0.0f,
		gWeight, gWeight, gWeight, 0.0f, 0.0f,
		bWeight, bWeight, bWeight, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 1.0f
	};

	// Brightness:
	// brightness matrix simply translates colors in each channel by specified values.
	// -1.0f will result in complete darkness (black), 1.0f will result in pure white colors.
	float adjValueR = 0.05f;
	float adjValueG = 0.05f;
	float adjValueB = 0.05f;

	Gdiplus::ColorMatrix cMatrixHover = {
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		adjValueR, adjValueG, adjValueB, 0.0f, 1.0f
	};

	adjValueR = -0.05f;
	adjValueG = -0.05f;
	adjValueB = -0.05f;

	Gdiplus::ColorMatrix cMatrixSelected = {
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		adjValueR, adjValueG, adjValueB, 0.0f, 1.0f
	};

	if (m_bTransparency) {
		// create an ImageAttributes object, and set its color key.
		Gdiplus::ImageAttributes imAtt;
		imAtt.SetColorKey(m_colorKeyLow, m_colorKeyHigh, Gdiplus::ColorAdjustTypeBitmap);

		// if no image was provided, we will first mask out
		// the transparency to another offscreen buffer
		// and then convert that image using the appropriate translation
		if (bDisabledNoImage || bHoverNoImage || bSelectedNoImage) {
			Gdiplus::ImageAttributes imAttTransp;
			Gdiplus::Bitmap bmpBackBufferTransp(rcItem.right, rcItem.bottom);
			Gdiplus::Graphics* backBufferTransp = Gdiplus::Graphics::FromImage(&bmpBackBufferTransp);

			backBufferTransp->DrawImage(&bmpBackBuffer, gdiRect, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAtt);

			// set the saturation transformation (convert to greyscale)
			if (bDisabledNoImage)
				imAttTransp.SetColorMatrix(&cMatrixGreyscale);
			// set the brightness transformation
			else if (bHoverNoImage)
				imAttTransp.SetColorMatrix(&cMatrixHover);
			else if (bSelectedNoImage)
				imAttTransp.SetColorMatrix(&cMatrixSelected);

			graphics.DrawImage(&bmpBackBufferTransp, gdiRect,
				m_bShowDepress && bSelectedNoImage ? rcItem.left - 1 : rcItem.left,
				m_bShowDepress && bSelectedNoImage ? rcItem.top - 1 : rcItem.top,
				rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAttTransp);

			delete backBufferTransp;
			backBufferTransp = 0;
		} else {
			graphics.DrawImage(&bmpBackBuffer, gdiRect, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAtt);
		}
	} else {
		// need another surface that will be colored like the dialog
		// background and used to display on the front buffer
		Gdiplus::Bitmap bmpBackBufferFill(rcItem.right, rcItem.bottom);
		Gdiplus::Graphics* backBufferFill = Gdiplus::Graphics::FromImage(&bmpBackBufferFill);

		Gdiplus::Color clr;
		clr.SetFromCOLORREF(GetSysColor(COLOR_BTNFACE));
		backBufferFill->Clear(clr);

		// since no image was provided, we will convert the normal
		// image using the appropriate translation
		if (bDisabledNoImage || bHoverNoImage || bSelectedNoImage) {
			Gdiplus::ImageAttributes imAtt;

			// set the saturation transformation (convert to greyscale)
			if (bDisabledNoImage)
				imAtt.SetColorMatrix(&cMatrixGreyscale);
			// set the brightness transformation
			else if (bHoverNoImage)
				imAtt.SetColorMatrix(&cMatrixHover);
			else if (bSelectedNoImage)
				imAtt.SetColorMatrix(&cMatrixSelected);

			backBufferFill->DrawImage(&bmpBackBuffer, gdiRect,
				m_bShowDepress && bSelectedNoImage ? rcItem.left - 1 : rcItem.left,
				m_bShowDepress && bSelectedNoImage ? rcItem.top - 1 : rcItem.top,
				rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAtt);

			graphics.DrawImage(&bmpBackBufferFill, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);
		} else {
			backBufferFill->DrawImage(&bmpBackBuffer, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);

			graphics.DrawImage(&bmpBackBufferFill, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);
		}

		delete backBufferFill;
		backBufferFill = 0;
	}

	delete backBuffer;
	backBuffer = 0;
}

BOOL CKEPImageToggleButton::LoadImages(CStringA pathNormal, CStringA pathSelected, CStringA pathHover, CStringA pathDisabled) {
	return CKEPImageButton::LoadImages(pathNormal, pathHover, pathSelected, pathDisabled);
}

void CKEPImageToggleButton::SetSelected(BOOL bSelected) {
	m_bSelected = bSelected;

	CRect rect;
	GetClientRect(&rect);
	InvalidateRect(&rect, true);
}
