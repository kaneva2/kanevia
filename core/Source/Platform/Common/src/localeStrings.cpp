///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "KEPHelpers.h"
#include "Core/Util/fast_mutex.h"

#include <TinyXML/tinyxml.h>
#include <locale>
#include <map>
#include <vector>
using namespace std;

namespace KEP {

typedef vector<const char*> stringVect;

class LocaleStringMap : map<UINT, string> {
public:
	static bool init(const char* dir, string& fname);
	static void deinit();

	static bool loadString(string& s, UINT id, const stringVect substStrings);

private:
	bool _loadString(string& s, UINT id, const stringVect substStrings);
	bool _load(const char* dir, string& fname);

	static LocaleStringMap* theMap;
	static fast_recursive_mutex m_sync;

	static fast_recursive_mutex& getSync() {
		return m_sync;
	}
};

bool LocaleStringMap::init(const char* dir, string& fname) {
	std::lock_guard<fast_recursive_mutex> lock(getSync());
	deinit(); // allow for reloading
	if (theMap == 0)
		theMap = new LocaleStringMap();

	return theMap->_load(dir, fname);
}

bool LocaleStringMap::loadString(string& s, UINT id, const stringVect substStrings) {
	std::lock_guard<fast_recursive_mutex> lock(getSync());
	if (theMap != 0) {
		return theMap->_loadString(s, id, substStrings);
	} else {
		jsAssert(false);
		s = "Not loaded ";
		s += numToString(id);
		return false;
	}
}

bool LocaleStringMap::_loadString(string& s, UINT id, const stringVect substStrings) {
	LocaleStringMap::iterator i = find(id);
	if (i != end()) {
		string::size_type lastPos = 0;
		string::size_type pctPos = i->second.find_first_of("%", 0);
		if (pctPos == string::npos)
			s = i->second; // no %
		else {
			while (pctPos != string::npos) {
				s += i->second.substr(lastPos, pctPos - lastPos);

				if (pctPos + 1 >= i->second.size()) {
					// nothing after %?
					s += "??%";
					lastPos = i->second.size();
					break;
				} else if (isdigit(i->second.at(pctPos + 1))) {
					ULONG index = (ULONG)(i->second.at(pctPos + 1) - _T('0'));
					if (index < substStrings.size() &&
						substStrings[index] != NULL)
						s += substStrings[index];

					lastPos = pctPos + 2; // skip %x
				} else if (i->second.at(pctPos + 1) == _T('%')) {
					// double %
					s += "%";
					lastPos = pctPos + 2;
				} else { // non number or % after it?
					s += "??%";
					lastPos = pctPos + 1;
				}

				pctPos = i->second.find_first_of("%", lastPos);
			}
			if (lastPos < i->second.size())
				s += i->second.substr(lastPos, i->second.size() - lastPos);
		}

		return true;
	} else {
		// jsAssert( false );
		s = "Id not found: ";
		s += numToString(id);
		return false;
	}
}

bool LocaleStringMap::_load(const char* dir, string& fname) {
	// get the current locale, then try to find a file of that name
	// MS-specific API!
	static const int NAME_LEN = 128;
	wchar_t wname[NAME_LEN];
	::GetLocaleInfoW(LOCALE_USER_DEFAULT, LOCALE_SABBREVLANGNAME, wname, NAME_LEN);
	std::string name = Utf16ToUtf8(wname);
	fname = dir;
	PathAddInPlace(fname, (string("KEPLang_") + name + ".xml").c_str());

	TiXmlDocument doc;
	if (!doc.LoadFile(fname)) {
		// if didn't load, try english us as backup
		fname = dir;
		PathAddInPlace(fname, "KEPLang_ENU.xml");
		if (!doc.LoadFile(fname))
			return false;
	}

	// read all the entries and put them into a file
	TiXmlHandle e = TiXmlHandle(doc.RootElement()).FirstChildElement("String");
	while (e.Element()) {
		int id;
		e.Element()->QueryIntAttribute("id", &id);

		// first child is text
		TiXmlText* text = TiXmlHandle(e.Element()->FirstChild()).Text();
		if (text) {
			insert(LocaleStringMap::value_type(id, text->Value()));
		} else {
			insert(LocaleStringMap::value_type(id, "")); // allow empty strings
		}
		e = e.Element()->NextSiblingElement("String");
	}

	return true;
}

void LocaleStringMap::deinit() {
	std::lock_guard<fast_recursive_mutex> lock(getSync());
	DELETE_AND_ZERO(theMap);
}

LocaleStringMap* LocaleStringMap::theMap = 0;
fast_recursive_mutex LocaleStringMap::m_sync;

// DLL interface
KEPUTIL_EXPORT bool initializeLocaleStrings(const char* dir, string& fname) {
	return LocaleStringMap::init(dir, fname);
}

KEPUTIL_EXPORT void freeLocaleStrings() {
	return LocaleStringMap::deinit();
}

KEPUTIL_EXPORT bool loadLocaleString(string& s, UINT id, const char* s0,
	const char* s1,
	const char* s2,
	const char* s3,
	const char* s4,
	const char* s5,
	const char* s6,
	const char* s7,
	const char* s8,
	const char* s9) {
	stringVect v;
	v.push_back(s0);
	v.push_back(s1);
	v.push_back(s2);
	v.push_back(s3);
	v.push_back(s4);
	v.push_back(s5);
	v.push_back(s6);
	v.push_back(s7);
	v.push_back(s8);
	v.push_back(s9);

	return LocaleStringMap::loadString(s, id, v);
}

} // namespace KEP