///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ScriptEventArgs.h"
#include "Event/Base/IReadableBuffer.h"
#include "Event/Base/IWriteableBuffer.h"

namespace KEP {

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPIDType& val) {
	return inBuffer >> val.value;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPIDType& val) {
	return outBuffer << val.value;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptNETIDType& val) {
	return inBuffer >> val.value;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptNETIDType& val) {
	return outBuffer << val.value;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptTimestamp& val) {
	val.value = fTime::TimeMs();
	return inBuffer;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptTimestamp& val) {
	return outBuffer; // Do nothing
}

template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPos<T>& val) {
	return inBuffer >> val.x >> val.y >> val.z;
}

template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPos<T>& val) {
	return outBuffer << val.x << val.y << val.z;
}

template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPosRot<T>& val) {
	return inBuffer >> val.x >> val.y >> val.z >> val.rx >> val.ry >> val.rz;
}

template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPosRot<T>& val) {
	return outBuffer << val.x << val.y << val.z << val.rx << val.ry << val.rz;
}

template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptLegacyArray<T>& val) {
	size_t count;
	inBuffer >> count;
	val.value.resize(count);
	for (size_t i = 0; i < count; i++) {
		inBuffer >> val.value[i];
	}
	return inBuffer;
}

template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptLegacyArray<T>& val) {
	outBuffer << val.value.size();
	for (const T& item : val.value) {
		outBuffer << item;
	}
	return outBuffer;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, KeyModifierTable& val) {
	return inBuffer >> val.shift >> val.ctrl >> val.alt;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const KeyModifierTable& val) {
	return outBuffer << val.shift << val.ctrl << val.alt;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptZoneInfo& val) {
	return inBuffer >> val.zoneId >> val.appName >> val.instanceIdArray;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptZoneInfo& val) {
	return outBuffer << val.zoneId << val.appName << val.instanceIdArray;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptZoneInstanceInfo& val) {
	inBuffer >> val.instanceId;
	if (val.instanceId != -1) {
		inBuffer >> val.locked >> val.players;
	} else {
		val.locked = 0;
	}
	return inBuffer;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptZoneInstanceInfo& val) {
	outBuffer << val.instanceId;
	if (val.instanceId != -1) {
		outBuffer << val.locked << val.players;
	}
	return outBuffer;
}

IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptItemInfo& val) {
	inBuffer >> val.succ;

	if (val.succ == 1) {
		inBuffer >> val.globalId >> val.useType >> val.actorGroup;
	} else {
		val.globalId = val.useType = val.actorGroup = 0;
	}
	return inBuffer;
}

IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptItemInfo& val) {
	outBuffer << val.succ;
	if (val.succ == 1) {
		outBuffer << val.globalId << val.useType << val.actorGroup;
	}
	return outBuffer;
}

// Instantiation for linkage
template IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPos<float>& val);
template IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPos<float>& val);
template IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptPosRot<float>& val);
template IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptPosRot<float>& val);
template IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptLegacyArray<std::string>& val);
template IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptLegacyArray<std::string>& val);
template IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptLegacyArray<ScriptZoneInfo>& val);
template IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptLegacyArray<ScriptZoneInfo>& val);
template IReadableBuffer& operator>>(IReadableBuffer& inBuffer, ScriptLegacyArray<std::tuple<int, int>>& val);
template IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const ScriptLegacyArray<std::tuple<int, int>>& val);

} // namespace KEP