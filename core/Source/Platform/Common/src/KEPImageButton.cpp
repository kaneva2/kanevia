/******************************************************************************
 KEPImageButton.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
// KEPImageButton.cpp : implementation file
//

#include "stdafx.h"
#include "KEPImageButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKEPImageButton

CKEPImageButton::CKEPImageButton() :
		m_bHover(FALSE), m_bTracking(FALSE), m_bTransparency(FALSE), m_bShowCaption(TRUE), m_bUseHover(TRUE), m_imgNormal(0), m_imgHover(0), m_imgSelected(0), m_imgDisabled(0), m_fontBrush(Gdiplus::Color(255, 0, 0, 0)), m_font(0) {
	m_font = ::new Gdiplus::Font(L"Arial", 10);
}

CKEPImageButton::CKEPImageButton(Gdiplus::Color& fontColor, WCHAR* fontName, UINT fontSize, Gdiplus::FontStyle fontStyle) :
		m_bHover(FALSE), m_bTracking(FALSE), m_bTransparency(FALSE), m_bShowCaption(TRUE), m_bUseHover(TRUE), m_imgNormal(0), m_imgHover(0), m_imgSelected(0), m_imgDisabled(0), m_fontBrush(fontColor), m_font(0) {
	m_font = ::new Gdiplus::Font(fontName, (Gdiplus::REAL)fontSize, fontStyle);
}

CKEPImageButton::~CKEPImageButton() {
	::delete m_font;
}

IMPLEMENT_DYNAMIC(CKEPImageButton, CBitmapButton)

BEGIN_MESSAGE_MAP(CKEPImageButton, CBitmapButton)
//{{AFX_MSG_MAP(CKEPImageButton)
ON_WM_MOUSEMOVE()
ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
ON_MESSAGE(WM_MOUSEHOVER, OnMouseHover)
//}}AFX_MSG_MAP
ON_WM_ERASEBKGND()
ON_WM_DESTROY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////
//	CKEPImageButton message handlers

void CKEPImageButton::OnMouseMove(UINT nFlags, CPoint point) {
	if (!m_bTracking) {
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE | TME_HOVER;
		tme.dwHoverTime = 1;
		m_bTracking = _TrackMouseEvent(&tme);
	}

	CBitmapButton::OnMouseMove(nFlags, point);
}

LRESULT CKEPImageButton::OnMouseHover(WPARAM wparam, LPARAM lparam) {
	if (m_bUseHover) {
		m_bHover = TRUE;

		CRect rect;
		GetClientRect(&rect);
		InvalidateRect(&rect, true);
	}
	return 0;
}

LRESULT CKEPImageButton::OnMouseLeave(WPARAM wparam, LPARAM lparam) {
	m_bTracking = FALSE;
	m_bHover = FALSE;

	CRect rect;
	GetClientRect(&rect);
	InvalidateRect(&rect, true);

	return 0;
}

BOOL CKEPImageButton::OnEraseBkgnd(CDC* pDC) {
	// to prevent flicker do not call the base class
	//return CBitmapButton::OnEraseBkgnd(pDC);
	return 0;
}

void CKEPImageButton::OnDestroy() {
	CBitmapButton::OnDestroy();

	delete m_imgNormal;
	delete m_imgHover;
	delete m_imgSelected;
	delete m_imgDisabled;

	m_imgNormal = 0;
	m_imgHover = 0;
	m_imgSelected = 0;
	m_imgDisabled = 0;
}

BOOL CKEPImageButton::PreTranslateMessage(MSG* pMsg) {
	InitToolTip();
	m_toolTip.RelayEvent(pMsg);
	return CButton::PreTranslateMessage(pMsg);
}

void CKEPImageButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) {
	BOOL bImage = FALSE;
	BOOL bDisabledNoImage = FALSE;
	BOOL bHoverNoImage = FALSE;
	BOOL bSelectedNoImage = FALSE;
	CRect rcItem = lpDrawItemStruct->rcItem;
	Gdiplus::Rect gdiRect(rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);
	Gdiplus::Graphics graphics(lpDrawItemStruct->hDC);

	// backbuffering
	Gdiplus::Bitmap bmpBackBuffer(rcItem.right, rcItem.bottom);
	Gdiplus::Graphics* backBuffer = Gdiplus::Graphics::FromImage(&bmpBackBuffer);

	if (lpDrawItemStruct->itemState & ODS_SELECTED) {
		if (m_imgSelected) {
			backBuffer->DrawImage(m_imgSelected, 0, 0, m_imgSelected->GetWidth(), m_imgSelected->GetHeight());
			bImage = TRUE;
		} else {
			bSelectedNoImage = TRUE;

			// use the normal image
			if (m_imgNormal) {
				backBuffer->DrawImage(m_imgNormal, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight());
				bImage = TRUE;
			}
		}
	} else if (lpDrawItemStruct->itemState & ODS_DISABLED) {
		if (m_imgDisabled) {
			backBuffer->DrawImage(m_imgDisabled, 0, 0, m_imgDisabled->GetWidth(), m_imgDisabled->GetHeight());
			bImage = TRUE;
		} else {
			bDisabledNoImage = TRUE;

			// use the normal image -- it will be converted to greyscale
			if (m_imgNormal) {
				backBuffer->DrawImage(m_imgNormal, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight());
				bImage = TRUE;
			}
		}
	} else {
		if (m_bHover) {
			if (m_imgHover) {
				backBuffer->DrawImage(m_imgHover, 0, 0, m_imgHover->GetWidth(), m_imgHover->GetHeight());
				bImage = TRUE;
			} else {
				bHoverNoImage = TRUE;

				// use the normal image -- brightness will be adjusted
				if (m_imgNormal) {
					backBuffer->DrawImage(m_imgNormal, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight());
					bImage = TRUE;
				}
			}
		} else {
			if (m_imgNormal) {
				Gdiplus::ImageAttributes imAtt;
				backBuffer->DrawImage(m_imgNormal, 0, 0, m_imgNormal->GetWidth(), m_imgNormal->GetHeight());

				bImage = TRUE;
			}
		}
	}

	if (m_bShowCaption) {
		// get the caption of the button
		CStringW sString;
		GetWindowText(sString);

		if (!sString.IsEmpty() && bImage) {
			// convert caption into format that GDI+ likes
			Gdiplus::RectF rectF(0.0, 0.0, (Gdiplus::REAL)rcItem.Width(), (Gdiplus::REAL)rcItem.Height());

			Gdiplus::StringFormat fontFormat;
			// center-justify each line of text.
			fontFormat.SetAlignment(Gdiplus::StringAlignmentCenter);

			// center the block of text (top to bottom) in the rectangle.
			fontFormat.SetLineAlignment(Gdiplus::StringAlignmentCenter);

			// use antialiasing
			backBuffer->SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);

			backBuffer->DrawString(sString, -1, m_font, rectF, &fontFormat, &m_fontBrush);
		}
	}

	//
	// setup some transformation matrices to be used when certain images are not loaded
	//

	// Greyscale:
	// saturation matrix makes use of weights that are assigned to each RGB channel.
	// weights correspond to sensitivity of our eyes to color channels.
	// the standard NTSC weights are defined as 0.299f (red), 0.587f (green) and 0.144f (blue)
	float rWeight = 0.299f;
	float gWeight = 0.587f;
	float bWeight = 0.114f;

	Gdiplus::ColorMatrix cMatrixGreyscale = {
		rWeight, rWeight, rWeight, 0.0f, 0.0f,
		gWeight, gWeight, gWeight, 0.0f, 0.0f,
		bWeight, bWeight, bWeight, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 1.0f
	};

	// Brightness:
	// brightness matrix simply translates colors in each channel by specified values.
	// -1.0f will result in complete darkness (black), 1.0f will result in pure white colors.
	float adjValueR = 0.05f;
	float adjValueG = 0.05f;
	float adjValueB = 0.05f;

	Gdiplus::ColorMatrix cMatrixHover = {
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		adjValueR, adjValueG, adjValueB, 0.0f, 1.0f
	};

	adjValueR = -0.05f;
	adjValueG = -0.05f;
	adjValueB = -0.05f;

	Gdiplus::ColorMatrix cMatrixSelected = {
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		adjValueR, adjValueG, adjValueB, 0.0f, 1.0f
	};

	if (m_bTransparency) {
		// create an ImageAttributes object, and set its color key.
		Gdiplus::ImageAttributes imAtt;
		imAtt.SetColorKey(m_colorKeyLow, m_colorKeyHigh, Gdiplus::ColorAdjustTypeBitmap);

		// if no image was provided, we will first mask out
		// the transparency to another offscreen buffer
		// and then convert that image using the appropriate translation
		if (bDisabledNoImage || bHoverNoImage || bSelectedNoImage) {
			Gdiplus::ImageAttributes imAttTransp;
			Gdiplus::Bitmap bmpBackBufferTransp(rcItem.right, rcItem.bottom);
			Gdiplus::Graphics* backBufferTransp = Gdiplus::Graphics::FromImage(&bmpBackBufferTransp);

			backBufferTransp->DrawImage(&bmpBackBuffer, gdiRect, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAtt);

			// set the saturation transformation (convert to greyscale)
			if (bDisabledNoImage)
				imAttTransp.SetColorMatrix(&cMatrixGreyscale);
			// set the brightness transformation
			else if (bHoverNoImage)
				imAttTransp.SetColorMatrix(&cMatrixHover);
			else if (bSelectedNoImage)
				imAttTransp.SetColorMatrix(&cMatrixSelected);

			graphics.DrawImage(&bmpBackBufferTransp, gdiRect,
				bSelectedNoImage ? rcItem.left - 1 : rcItem.left, bSelectedNoImage ? rcItem.top - 1 : rcItem.top,
				rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAttTransp);

			delete backBufferTransp;
			backBufferTransp = 0;
		} else {
			graphics.DrawImage(&bmpBackBuffer, gdiRect, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAtt);
		}
	} else {
		// need another surface that will be colored like the dialog
		// background and used to display on the front buffer
		Gdiplus::Bitmap bmpBackBufferFill(rcItem.right, rcItem.bottom);
		Gdiplus::Graphics* backBufferFill = Gdiplus::Graphics::FromImage(&bmpBackBufferFill);

		Gdiplus::Color clr;
		clr.SetFromCOLORREF(GetSysColor(COLOR_BTNFACE));
		backBufferFill->Clear(clr);

		// since no image was provided, we will convert the normal
		// image using the appropriate translation
		if (bDisabledNoImage || bHoverNoImage || bSelectedNoImage) {
			Gdiplus::ImageAttributes imAtt;

			// set the saturation transformation (convert to greyscale)
			if (bDisabledNoImage)
				imAtt.SetColorMatrix(&cMatrixGreyscale);
			// set the brightness transformation
			else if (bHoverNoImage)
				imAtt.SetColorMatrix(&cMatrixHover);
			else if (bSelectedNoImage)
				imAtt.SetColorMatrix(&cMatrixSelected);

			backBufferFill->DrawImage(&bmpBackBuffer, gdiRect,
				bSelectedNoImage ? rcItem.left - 1 : rcItem.left, bSelectedNoImage ? rcItem.top - 1 : rcItem.top,
				rcItem.right, rcItem.bottom,
				Gdiplus::UnitPixel, &imAtt);

			graphics.DrawImage(&bmpBackBufferFill, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);
		} else {
			backBufferFill->DrawImage(&bmpBackBuffer, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);

			graphics.DrawImage(&bmpBackBufferFill, rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);
		}

		delete backBufferFill;
		backBufferFill = 0;
	}

	delete backBuffer;
	backBuffer = 0;
}

void CKEPImageButton::SetFontStyle(Gdiplus::FontStyle fontStyle) {
	// get the current font attributes and create a new font
	// with those attribs and set a new style
	Gdiplus::FontFamily fontFamily;
	Gdiplus::REAL fontSize = m_font->GetSize();
	m_font->GetFamily(&fontFamily);

	// remove the old font, make the new
	::delete m_font;
	m_font = ::new Gdiplus::Font(&fontFamily, fontSize, fontStyle);
}

void CKEPImageButton::SetFontSize(Gdiplus::REAL fontSize) {
	// get the current font attributes and create a new font
	// with those attribs and set a new size
	Gdiplus::FontFamily fontFamily;
	INT fontStyle = m_font->GetStyle();
	m_font->GetFamily(&fontFamily);

	// remove the old font, make the new
	::delete m_font;
	m_font = ::new Gdiplus::Font(&fontFamily, fontSize, fontStyle);
}

BOOL CKEPImageButton::LoadImages(CStringA pathNormal, CStringA pathHover, CStringA pathSelected, CStringA pathDisabled) {
	INT cx = -1;
	INT cy = -1;

	if (!pathNormal.IsEmpty()) {
		m_imgNormal = Gdiplus::Image::FromFile(Utf8ToUtf16(pathNormal).c_str());

		cx = m_imgNormal->GetWidth();
		cy = m_imgNormal->GetHeight();
	}

	if (!pathHover.IsEmpty()) {
		m_imgHover = Gdiplus::Image::FromFile(Utf8ToUtf16(pathHover).c_str());
	}

	if (!pathSelected.IsEmpty()) {
		m_imgSelected = Gdiplus::Image::FromFile(Utf8ToUtf16(pathSelected).c_str());
	}

	if (!pathDisabled.IsEmpty()) {
		m_imgDisabled = Gdiplus::Image::FromFile(Utf8ToUtf16(pathDisabled).c_str());
	}

	if (cx > 0 && cy > 0) {
		// make this an owner drawn button since we have the 'normal' image
		ModifyStyle(0, BS_OWNERDRAW);

		// set the window size to match the 'normal' image
		SetWindowPos(NULL, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOOWNERZORDER);
	}

	return TRUE;
}

BOOL CKEPImageButton::LoadImages(const CStringA& pathNormal, const Gdiplus::Color& colorKeyLow, const Gdiplus::Color& colorKeyHigh,
	const CStringA& pathHover, const CStringA& pathSelected, const CStringA& pathDisabled) {
	m_bTransparency = TRUE;
	m_colorKeyLow = colorKeyLow;
	m_colorKeyHigh = colorKeyHigh;

	return LoadImages(pathNormal, pathHover, pathSelected, pathDisabled);
}

//
// Tooltips
//

// Set the tooltip with a string resource
void CKEPImageButton::SetToolTipText(int nId, BOOL bActivate) {
	CStringW sText;

	// load string resource
	sText.LoadString(nId);

	// if string resource is not empty
	if (!sText.IsEmpty())
		SetToolTipText(&sText, bActivate);
}

// Set the tooltip with a CStringW
void CKEPImageButton::SetToolTipText(CStringW* spText, BOOL bActivate) {
	// we cannot accept NULL pointer
	if (!spText)
		return;
	InitToolTip();

	// if there is no tooltip defined then add it
	if (m_toolTip.GetToolCount() == 0) {
		CRect rectBtn;
		GetClientRect(rectBtn);
		m_toolTip.AddTool(this, *spText, rectBtn, 1);
	}

	// set text for tooltip
	m_toolTip.UpdateTipText(*spText, this, 1);
	m_toolTip.Activate(bActivate);
}

void CKEPImageButton::InitToolTip() {
	if (m_toolTip.m_hWnd == NULL) {
		// create ToolTip control
		m_toolTip.Create(this);
		// create inactive
		m_toolTip.Activate(FALSE);
	}
}

// Activate the tooltip
void CKEPImageButton::ActivateTooltip(BOOL bActivate) {
	// if there is no tooltip then do nothing
	if (m_toolTip.GetToolCount() == 0)
		return;

	// Activate tooltip
	m_toolTip.Activate(bActivate);
}
