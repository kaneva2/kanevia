///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <js.h>
#include "TextCommands.h"
#include "KEPHelpers.h"
#include "TinyXML/tinyxml.h"
#include <locale>

using namespace KEP;

#define WORD_DELIM _T(' ')

const char* TextCommands::DEFAULT_COMMAND_STRING = "\tDEFAULT\t";

ULONG TextCommands::mapCommand(const std::string& cmd) {
	std::string cmdLower = cmd;
	STLToLower(cmdLower);

	CommandMap::iterator i = m_map.find(cmdLower);

	if (i != m_map.end())
		return i->second;
	else
		return 0;
}

/** DRF 
* Loads ...\Star\3296\GameFiles\Commands.xml as the list of supported client commands.
*/
bool TextCommands::Init(const std::string& pathCommandsXml) {
	// format is
	/*
	<Config> (or something)
		<Commands commandChar=":" defaultCommand="2000">
			<Command id="12">summon</Command>
			...
		</Commands>
	</Config>
	*/
	TiXmlDocument doc;
	if (!doc.LoadFile(pathCommandsXml.c_str()))
		return false;

	// read all the entries and put them into a file
	TiXmlHandle e = TiXmlHandle(doc.RootElement()).FirstChildElement("Commands");
	if (!e.Element())
		return false;

	const char* cmdChar = e.Element()->Attribute("commandChar");

	if (cmdChar)
		m_commandChar = *cmdChar;

	const char* defaultCommandStr = e.Element()->Attribute("defaultCommand");
	if (defaultCommandStr)
		m_defaultCommand = strtoul(defaultCommandStr, 0, 10);

	e = e.FirstChild("Command");

	while (e.Element()) {
		int id;
		e.Element()->QueryIntAttribute("id", &id);

		// first child is text
		TiXmlText* text = TiXmlHandle(e.Element()->FirstChild()).Text();
		if (text) {
			std::string s(text->Value());
			STLToLower(s);
			m_map.insert(CommandMap::value_type(s, id));
		}
		e = e.Element()->NextSiblingElement("Command");
	}
	return true;
}

// returns false if not plain text or a command
bool TextCommands::GetCommandId(const char* msg, ULONG& commandId, const char*& remainder) {
	jsVerifyReturn(msg != 0, false);
	commandId = 0;
	remainder = msg;

	if (msg[0] == m_commandChar) {
		size_t len = ::strlen(msg);
		size_t commandLen = 0;

		// find first space
		remainder = ::strchr(msg, WORD_DELIM);
		if (remainder) {
			commandLen = remainder - msg;
			while (*remainder == WORD_DELIM)
				remainder++;
		} else {
			commandLen = len;
		}

		if (remainder == 0)
			remainder = "";

		// check the command
		commandId = mapCommand(std::string(msg + 1, commandLen - 1));

		if (commandId == 0)
			remainder = msg;

	} else if (strncmp(msg, DEFAULT_COMMAND_STRING, MIN(strlen(msg), strlen(DEFAULT_COMMAND_STRING))) == 0) {
		if (strlen(msg) > strlen(DEFAULT_COMMAND_STRING)) {
			remainder = msg + strlen(DEFAULT_COMMAND_STRING);
			while (*remainder == WORD_DELIM)
				remainder++;
		} else
			remainder = "";

		if (remainder == 0)
			remainder = "";

		commandId = m_defaultCommand;
	} else
		return true; // if just a text message, ok

	// otherwise looks like a command so should have a command id
	return commandId != 0;
}
