///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "KEPConfig.h"

#include <TinyXML/tinyxml.h>
#include <KEPException.h>

#include <KEPHelpers.h>
#include "Core/Crypto/Base64.h"

#include "..\KEPUtil\RefPtr.h"

#include "common/include/LogHelper.h"
static LogInstance("Instance");

using namespace KEP;

static bool getTextFromNode(TiXmlHandle node, std::string& ret, const char* defaultValue) {
	bool found = false;

	if (node.Node()) {
		found = true;
		TiXmlText* text = node.FirstChild().Text();
		if (text != 0) {
			ret = text->Value();
		} else {
			ret = "";
		}
	} else {
		ret = defaultValue;
	}

	return found;
}

static TiXmlHandle findNode(TiXmlElement* root, const char* valueName) {
	// allow setting multilevels
	std::vector<std::string> tokens;
	STLTokenize(std::string(valueName), tokens, "/");

	TiXmlElement* e = root;
	for (size_t i = 0; i < tokens.size(); i++) {
		if (!e)
			return e;
		TiXmlElement* next = e->FirstChildElement(tokens[i]);

		e = next;
	}
	return e;
}

KEPConfig::KEPConfig() :
		m_loaded(false), m_dirty(false), m_root(0) {
	m_dom = new TiXmlDocument();
}

KEPConfig::~KEPConfig() {
	// DRF - Invalidate Reference Pointer (as KEPConfig*)
	REF_PTR_DEL(this);

	if (m_dom) {
		delete m_dom;
		m_dom = NULL;
	}
}

bool KEPConfig::New(const char* rootNodeName, const char* rootAttribute /*= 0*/, const char* rootAttributeValue /*= 0 */) {
	std::string s = "<";
	s += rootNodeName;
	if (rootAttribute && rootAttributeValue) {
		s += rootAttribute;
		s += "=\"";
		s += rootAttributeValue;
		s += "\"";
	}
	s += "/>";
	return Parse(s.c_str(), rootNodeName, rootAttribute, rootAttributeValue);
}

bool KEPConfig::Parse(const char* buffer, const char* rootNodeName, const char* rootAttribute, const char* rootAttributeValue) {
	m_loaded = false;
	LogInfo("OK");
	if (!m_dom)
		return false;
	(void) m_dom->Parse(buffer);
	if (m_dom->ErrorId() != 0)
		return false;
	if (!ParsePrivate(rootNodeName, rootAttribute, rootAttributeValue))
		return false;
	m_loaded = true;
	return true;
}

bool KEPConfig::Load(const std::string& filePath, const char* rootNodeName, const char* rootAttribute, const char* rootAttributeValue) {
	m_loaded = false;
	if (!m_dom || !m_dom->LoadFile(filePath.c_str()))
		return false;
	return ParsePrivate(rootNodeName, rootAttribute, rootAttributeValue);
}

bool KEPConfig::Save() {
	auto ok = m_loaded && m_dom && m_dom->SaveFile();
	if (ok)
		m_dirty = false;
	return ok;
}

bool KEPConfig::SaveAs(const std::string& fname) {
	auto ok = m_loaded && m_dom && m_dom->SaveFile(fname.c_str());
	if (ok)
		m_dirty = false;
	return ok;
}

bool KEPConfig::ParsePrivate(const char* rootNodeName, const char* rootAttribute, const char* rootAttributeValue) {
	if (!m_dom)
		return false;
	m_root = m_dom->FirstChildElement(rootNodeName);
	if (!m_root) {
		LogError("FAILED dom.FirstChildElement() - rootNodeName=" << rootNodeName);
		return false;
	}
	m_loaded = true;
	if (rootAttribute && rootAttributeValue) {
		const char* xmlClassName = m_root->Attribute(rootAttribute);
		if (xmlClassName == NULL || ::strcmp(xmlClassName, rootAttributeValue) != 0) {
			LogError("FAILED dom.Attribute() - rootAttribute=" << rootAttribute);
			m_loaded = false;
			return false;
		}
	}
	return true;
}

bool KEPConfig::GetValue(const char* valueName, int& ret, int defaultValue) const {
	bool found = false;
	ret = defaultValue;
	if (m_loaded) {
		TiXmlHandle node = findNode(m_root, valueName);
		if (node.Node()) {
			found = true;
			TiXmlText* text = node.FirstChild().Text();
			if (text != 0) {
				ret = atoi(text->Value());
			}
		}
	}
	return found;
}

bool KEPConfig::GetValue(const char* valueName, std::string& val, const char* defaultValue) const {
	val = defaultValue;
	if (!m_loaded)
		return false;
	TiXmlHandle node = findNode(m_root, valueName);
	return getTextFromNode(node, val, defaultValue);
}

bool KEPConfig::GetValue(const char* valueName, bool& val, bool defaultValue /* = false*/) const {
	val = defaultValue;
	int i;
	if (!GetValue(valueName, i, defaultValue ? 1 : 0))
		return false;
	val = (i != 0);
	return true;
}

bool KEPConfig::GetValue(const char* valueName, double& val, double defaultValue /* = 0.0*/) const {
	val = defaultValue;
	std::string s;
	if (!GetValue(valueName, s, "0.0"))
		return false;
	val = atof(s.c_str());
	return true;
}

TiXmlHandle KEPConfig::GetElement(const char* valueName) const {
	if (!m_loaded)
		return TiXmlHandle(0);
	return findNode(m_root, valueName);
}

bool KEPConfig::ValueExists(const char* valueName) const {
	if (!m_loaded)
		return false;
	return TiXmlHandle(m_root->FirstChild(valueName)).Node() != nullptr;
}

bool KEPConfig::SetValue(const char* valueName, int value, bool append) {
	char s[34];
	_itoa_s(value, s, _countof(s), 10);
	return SetValue(valueName, s, append);
}

bool KEPConfig::SetValue(const char* valueName, const char* value, bool append) {
	if (!m_loaded) {
		LogError("NOT LOADED");
		return false;
	}
	m_dirty = true;

	// allow setting multilevels
	std::vector<std::string> tokens;
	STLTokenize(std::string(valueName), tokens, "/");
	if (!tokens.size())
		return false;

	TiXmlElement* pElement = m_root;
	for (size_t i = 0; i < tokens.size(); i++) {
		TiXmlElement* pChild = pElement->FirstChildElement(tokens[i]);
		if (pChild == 0 || (i == tokens.size() - 1 && append))
			pChild = pElement->LinkEndChild(new TiXmlElement(tokens[i]))->ToElement();
		pElement = pChild;
	}
	TiXmlText* pText = TiXmlHandle(pElement).FirstChild().Text();
	if (!pText)
		pText = pElement->LinkEndChild(new TiXmlText(value))->ToText();
	else
		pText->SetValue(value);
	return pText != nullptr;
}

bool KEPConfig::SetValue(const char* valueName, bool value, bool append) {
	return SetValue(valueName, value ? "1" : "0", append);
}

bool KEPConfig::SetValue(const char* valueName, double value, bool append) {
	char s[50];
	_snprintf_s(s, _countof(s), _TRUNCATE, "%f", value);
	return SetValue(valueName, s, append);
}

bool KEPConfig::RemoveValue(const char* valueName) {
	TiXmlHandle h = GetElement(valueName);
	if (!h.Element())
		return false;
	h.Element()->Clear();
	return true;
}

bool KEPConfig::GetFirstValue(TiXmlHandle& node, const char* valueName, std::string& ret, const char* defaultValue) {
	ret = defaultValue;
	if (!m_loaded)
		return false;
	node = TiXmlHandle(0);
	node = GetElement(valueName);
	if (!node.Node())
		return false;
	getTextFromNode(node, ret, defaultValue);
	return true;
}

bool KEPConfig::GetNextValue(TiXmlHandle& h, std::string& ret) {
	if (!m_loaded || !h.Node())
		return false;
	h = h.Node()->NextSibling();
	if (!h.Node())
		return false;
	return getTextFromNode(h, ret, "");
}

bool KEPConfig::Print() {
	if (!m_dom)
		return false;
	m_dom->Print();
	return true;
}
