///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "KEPHelpers.h"

#include "lmerr.h"

// used by loadStr in header
HINSTANCE StringResource = 0;

///---------------------------------------------------------
// convert an error number to a meaningful string for
// OS-type error messages
//
// [in] errNo the error number
// [out] msg the resulting message or "Error number 0x%x"
//
// [Returns]
//		msg.c_str(), for convenience
const wchar_t* errorNumToString(ULONG dwLastError, std::wstring& msg) {
	// copied largely from MSDN to include network error codes

	HMODULE hModule = NULL; // default to system source
	wchar_t* MessageBuffer;
	DWORD dwBufferLength;

	DWORD dwFormatFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER |
						  FORMAT_MESSAGE_IGNORE_INSERTS |
						  FORMAT_MESSAGE_FROM_SYSTEM;

	//
	// If dwLastError is in the network range,
	//  load the message source.
	//

	if (dwLastError >= NERR_BASE && dwLastError <= MAX_NERR) {
		hModule = LoadLibraryExW(
			L"netmsg.dll",
			NULL,
			LOAD_LIBRARY_AS_DATAFILE);

		if (hModule != NULL)
			dwFormatFlags |= FORMAT_MESSAGE_FROM_HMODULE;
	}

	//
	// Call FormatMessage() to allow for message
	//  text to be acquired from the system
	//  or from the supplied module handle.
	//
	dwBufferLength = FormatMessageW(
		dwFormatFlags,
		hModule, // module to get message from (NULL == system)
		dwLastError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // default language
		(wchar_t*)&MessageBuffer,
		0,
		NULL);
	if (dwBufferLength) {
		if (dwBufferLength > 2 &&
			(MessageBuffer[dwBufferLength - 2] == '\r' || MessageBuffer[dwBufferLength - 2] == '\n'))
			dwBufferLength -= 2; // remove \r\n

		msg.assign(MessageBuffer, dwBufferLength);

		//
		// Free the buffer allocated by the system.
		//
		LocalFree(MessageBuffer);
	} else {
		if (HRESULT_FACILITY(dwLastError) == FACILITY_ITF) {
			// TODO: if our custom error message, load a string
		}
		wchar_t s[80];
		_snwprintf_s(s, _countof(s), _TRUNCATE, L"Error number 0x%x", dwLastError);
		msg = s;
	}

	//
	// If we loaded a message source, unload it.
	//
	if (hModule != NULL)
		FreeLibrary(hModule);

	return msg.c_str();
}

const char* errorNumToString(ULONG dwLastError, std::string& msg) {
	std::wstring wmsg;
	errorNumToString(dwLastError, wmsg);
	msg = KEP::Utf16ToUtf8(wmsg);
	return msg.c_str();
}

double GetEpochTime(SYSTEMTIME systemTime) {
	FILETIME fileTime;
	SystemTimeToFileTime(&systemTime, &fileTime);

	ULARGE_INTEGER uli;
	uli.LowPart = fileTime.dwLowDateTime;
	uli.HighPart = fileTime.dwHighDateTime;

	SYSTEMTIME epoch;
	epoch.wYear = 1970;
	epoch.wMonth = 1;
	epoch.wDayOfWeek = 4;
	epoch.wDay = 1;
	epoch.wHour = 0;
	epoch.wMinute = 0;
	epoch.wSecond = 1;
	epoch.wMilliseconds = 0;

	FILETIME fileTimeEpoch;
	SystemTimeToFileTime(&epoch, &fileTimeEpoch);

	ULARGE_INTEGER uliEpoch;
	uliEpoch.LowPart = fileTimeEpoch.dwLowDateTime;
	uliEpoch.HighPart = fileTimeEpoch.dwHighDateTime;

	ULONGLONG secondsEpoch((uli.QuadPart - uliEpoch.QuadPart) / 10000000);
	return (double)secondsEpoch;
}

double GetCurrentEpochTime() {
	SYSTEMTIME systemTime;
	GetSystemTime(&systemTime);

	return GetEpochTime(systemTime);
}

double GetCurrentLocalEpochTime() {
	SYSTEMTIME localEpochTime;
	GetLocalTime(&localEpochTime);

	return GetEpochTime(localEpochTime);
}

std::string GetCurrentTimeStamp() {
	SYSTEMTIME systemTime;
	GetLocalTime(&systemTime);
	std::ostringstream osstream;
	osstream << systemTime.wMonth << "/" << systemTime.wDay << "/" << systemTime.wYear << " " << systemTime.wHour << ":" << systemTime.wMinute << ":" << systemTime.wSecond;
	std::string date = osstream.str();
	return date;
}

std::string EpochTimeToTimeStamp(double epochTime) {
	FILETIME fileTime;
	ULONGLONG sec = (ULONGLONG)epochTime;
	sec = sec * 10000000;

	fileTime.dwHighDateTime = (DWORD)(sec >> 32);
	fileTime.dwLowDateTime = (DWORD)(sec);

	ULARGE_INTEGER uli;
	uli.LowPart = fileTime.dwLowDateTime;
	uli.HighPart = fileTime.dwHighDateTime;

	SYSTEMTIME epoch;
	epoch.wYear = 1970;
	epoch.wMonth = 1;
	epoch.wDayOfWeek = 4;
	epoch.wDay = 1;
	epoch.wHour = 0;
	epoch.wMinute = 0;
	epoch.wSecond = 1;
	epoch.wMilliseconds = 0;

	FILETIME fileTimeEpoch;
	SystemTimeToFileTime(&epoch, &fileTimeEpoch);

	ULARGE_INTEGER uliEpoch;
	uliEpoch.LowPart = fileTimeEpoch.dwLowDateTime;
	uliEpoch.HighPart = fileTimeEpoch.dwHighDateTime;

	ULARGE_INTEGER uliTimeWithEpoch;
	uliTimeWithEpoch.QuadPart = uliEpoch.QuadPart + uli.QuadPart;
	FILETIME fileTimeWithEpoch;
	fileTimeWithEpoch.dwHighDateTime = uliTimeWithEpoch.HighPart;
	fileTimeWithEpoch.dwLowDateTime = uliTimeWithEpoch.LowPart;

	SYSTEMTIME systemTime;
	FileTimeToLocalFileTime(&fileTimeWithEpoch, &fileTimeWithEpoch);
	FileTimeToSystemTime(&fileTimeWithEpoch, &systemTime);

	std::ostringstream osstream;
	osstream << systemTime.wMonth << "/" << systemTime.wDay << "/" << systemTime.wYear << " " << systemTime.wHour << ":" << systemTime.wMinute << ":" << systemTime.wSecond;
	std::string date = osstream.str();
	return date;
}

double DateToEpochTime(WORD year, WORD month, WORD day, WORD hour, WORD minute, WORD second, WORD millisecond) {
	SYSTEMTIME systemTime;
	systemTime.wYear = year;
	systemTime.wMonth = month;
	systemTime.wDay = day;
	systemTime.wHour = hour;
	systemTime.wMinute = minute;
	systemTime.wSecond = second;
	systemTime.wMilliseconds = millisecond;

	return GetEpochTime(systemTime);
}
