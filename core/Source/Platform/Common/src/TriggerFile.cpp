///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TriggerFile.h"
#include "Event/Events/TriggerEvent.h"
#include "TinyXML/tinyxml.h"

#include "Event/Base/IDispatcher.h"
#include "CWldObject.h"
#include "CEXMeshClass.h"
#include "CTriggerClass.h"
#include "CNodeObject.h"
#include <jsEnumFiles.h>
#include <KEPException.h>
#include "Core/Math/Sphere.h"

#include "LogHelper.h"
static LogInstance("Instance");

using namespace KEP;

static const char* TRIGGERDB_TAG = "TriggerDB";
static const char* TRIGGER_OBJ_TAG = "TriggerObject";
static const char* TRIGGERS_TAG = "Triggers";
static const char* TRIGGER_TAG = "Trigger";
static const char* ACTION_TAG = "Action";
static const char* ZONE_ATTR = "zone";
static const char* X_ATTR = "x";
static const char* Y_ATTR = "y";
static const char* Z_ATTR = "z";
static const char* NAME_ATTR = "name";
static const char* POSITION_TAG = "Position";
static const char* TRACE_ATTR = "traceId";
static const char* RADIUS_TAG = "Radius";
static const char* ONETIME_TAG = "Onetime";
static const char* STRING_TAG = "String";

/*
<TriggerDB zone='zone.exg'>
	<TriggerObject traceId='x' name='x' >
		<Position x='1' y='2' z='3'>
		<Triggers>
			<Trigger>
				<Action />
				<Radius />
				<String />
			</Trigger>
		</Triggers>
	</TriggerObject>
</TriggerDB>
*/

#define THROW_DOM_ERR throw new KEPException(dom.ErrorDesc(), dom.ErrorId(), __FILE__, __LINE__);

void TriggerFile::RegisterAllTriggerEvents(const char* gameDir, IDispatcher& dispatcher, CTriggerObjectList* list) {
	std::string path = PathAdd(gameDir, "*.trg");
	jsEnumFiles ef(Utf8ToUtf16(path).c_str());
	while (ef.next())
		RegisterTriggerEvents(Utf16ToUtf8(ef.currentPath()).c_str(), dispatcher, list);
}

bool TriggerFile::RegisterTriggerEvents(const char* fname, IDispatcher& dispatcher, CTriggerObjectList* list) {
	try {
		// go through all the triggers, looking for scripting ones
		TiXmlDocument dom;
		if (!dom.LoadFile(fname))
			THROW_DOM_ERR

		// iterate over db items
		TiXmlHandle docHandle(&dom);
		TiXmlElement* child = docHandle.FirstChild(TRIGGERDB_TAG).FirstChild(TRIGGER_OBJ_TAG).Element();
		while (child) {
			double x = 0.0;
			double y = 0.0;
			double z = 0.0;

			TiXmlElement* pos = TiXmlHandle(child).FirstChild(POSITION_TAG).Element();
			if (pos) {
				pos->QueryDoubleAttribute(X_ATTR, &x);
				pos->QueryDoubleAttribute(Y_ATTR, &y);
				pos->QueryDoubleAttribute(Z_ATTR, &z);
			}

			const char* name = child->Attribute(NAME_ATTR);
			name;

			// iterate over triggers
			TiXmlElement* triggerChild = TiXmlHandle(child).FirstChild(TRIGGER_TAG).Element();
			while (triggerChild) {
				TiXmlNode* n = triggerChild->NextSibling(TRIGGER_TAG);
				if (n)
					triggerChild = n->ToElement();
				else
					triggerChild = 0;
			}

			TiXmlNode* n = child->NextSibling(TRIGGER_OBJ_TAG);
			if (n)
				child = n->ToElement();
			else
				child = 0;
		}

		return true;
	} catch (KEPException* e) {
		LogError("Caught Exception - " << e->ToString());
		e->Delete();

		return false;
	}
}
