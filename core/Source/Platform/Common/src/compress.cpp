#if DEPRECATED_COMPRESS_CPP
/******************************************************************************
 compress.cpp

 Copyright (c) 2004-2008 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include <windows.h>
#include "Core/Util/Unicode.h"
#include <memory>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if defined(_WIN32_WCE)
#if _WIN32_WCE < 211
#error(f|w)printf functions is not support old WindowsCE.
#endif

#undef USE_MMAP
#undef HAVE_UNISTD_H
#include <windows.h>
#else
#include <stdio.h>
#endif
#include "zlib\zlib.h"
#include "stdlib.h"

#ifdef STDC
#include <string.h>
#include <stdlib.h>
#else
extern void exit OF((int));
#endif

#if (defined(MSDOS) || defined(OS2) || defined(WIN32)) && !defined(_WIN32_WCE)
#include <fcntl.h>
#include <io.h>
#define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
//#  define SET_BINARY_MODE(file)
#endif

#if defined(__MWERKS__) && __dest_os != __be_os && __dest_os != __win32_os
#include <unix.h> // for fileno
#endif

#ifndef WIN32 // unlink already in stdio.h for WIN32
extern int unlink OF((const char*));
#endif

#ifndef GZ_SUFFIX
#define GZ_SUFFIX ".gz"
#endif
#define SUFFIX_LEN (sizeof(GZ_SUFFIX) - 1)

#define BUFLEN 16384
#define MAX_NAME_LEN 1024

#define local
#define F_FILE FILE*
#define F_NULL NULL

char* prog;

bool gz_compress OF((F_FILE in, gzFile out));
bool gz_uncompress OF((gzFile in, F_FILE out));
bool file_compress OF((char* file, char* outfile, char* mode, int bDelete));
bool file_uncompress OF((char* file));

// Compress input buffer to output
bool gz_compress_buffer(unsigned char* in_buf, int in_len, unsigned char* out_buf, int& out_len) {
	z_stream strm;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.next_in = in_buf;
	strm.avail_in = in_len;
	strm.next_out = out_buf;
	strm.avail_out = out_len;

	int err = deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, 15 + 16, 8, Z_DEFAULT_STRATEGY); // windowBits +16 = output data with gzip header instead of zlib
	while (err == Z_OK) {
		err = deflate(&strm, Z_FINISH);
	}

	deflateEnd(&strm);

	if (err == Z_STREAM_END) {
		out_len = strm.total_out;
	} else if (err == Z_BUF_ERROR) {
		out_len = -1; // Insufficient output buffer
	}

	return err == Z_STREAM_END;
}

// Uncompress input buffer to output
bool gz_uncompress_buffer(unsigned char* in_buf, int in_len, unsigned char* out_buf, int& out_len) {
	const int priv_len = 16384;
	std::unique_ptr<unsigned char[]> priv_buf;
	if (out_buf == Z_NULL) {
		// Do a measuring pass
		priv_buf = std::make_unique<unsigned char[]>(priv_len);
		out_len = 0;
	}

	z_stream strm;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.next_in = in_buf;
	strm.avail_in = in_len;
	strm.next_out = out_buf == Z_NULL ? priv_buf.get() : out_buf;
	strm.avail_out = out_buf == Z_NULL ? priv_len : out_len;

	int err = inflateInit2(&strm, 15 + 32); // windowBits +32 = handle both gzip and zlib header
	while (err == Z_OK) {
		err = inflate(&strm, Z_FINISH);
		if (priv_buf && err == Z_BUF_ERROR) { // If measuring pass
			// rewind buffer
			strm.next_out = priv_buf.get();
			strm.avail_out = priv_len;
			err = Z_OK;
		}
	}

	inflateEnd(&strm);

	if (err == Z_STREAM_END) {
		out_len = strm.total_out;
	} else if (err == Z_BUF_ERROR) {
		out_len = -1; // Insufficient output buffer
	}

	return err == Z_STREAM_END;
}

// Compress input to output then close both files.
bool gz_compress(F_FILE in, gzFile out) {
	local char buf[BUFLEN];
	size_t len = 0;
	int err;

	for (;;) {
#if defined(_WIN32_WCE)
		bool readOk = false;
		DWORD dwLen = 0;
		readOk = ReadFile(in, buf, sizeof(buf), &dwLen, NULL);
		len = (size_t)dwLen;
		if (!readOk)
#else
		len = fread(buf, 1, sizeof(buf), in);
		if (ferror(in))
#endif
		{
			perror("fread");
			return false;
		}

		if (len == 0)
			break;

		if (gzwrite(out, buf, (unsigned int)len) != (int)len)
			perror((char*)gzerror(out, &err));
	}
#if defined(_WIN32_WCE)
	CloseHandle(in);
#else
	fclose(in);
#endif
	if (gzclose(out) != Z_OK)
		perror("failed gzclose");

	return true;
}

// Uncompress input to output then close both files.
bool gz_uncompress(gzFile in, F_FILE out) {
	local char buf[BUFLEN];
	unsigned long len;
	int err;
#if defined(_WIN32_WCE)
	unsigned long size;
#endif

	for (;;) {
		len = gzread(in, buf, sizeof(buf));
		if (len < 0)
			perror((char*)gzerror(in, &err));
		if (len == 0)
			break;

#if defined(_WIN32_WCE)
		if (!WriteFile(out, buf, (unsigned)len, &size, NULL) || size != len) {
#else
		if ((int)fwrite(buf, 1, (unsigned)len, out) != len) {
#endif
			perror("failed fwrite");
		}
	}
#if defined(_WIN32_WCE)
	if (!CloseHandle(out))
		perror("failed fclose");
#else
	if (fclose(out))
		perror("failed fclose");
#endif

	if (gzclose(in) != Z_OK)
		perror("failed gzclose");

	return true;
}

// Compress the given file: create a corresponding .gz file and remove the original.
bool file_compress(const char* file, char* outfile, char* mode, int bDelete) {
	F_FILE in;
	gzFile out;
#if defined(_WIN32_WCE)
	char path[MAX_PATH];
#endif

#if defined(_WIN32_WCE)
	MultiByteToWideChar(CP_ACP, 0, file, -1, path, MAX_PATH);
	in = CreateFile(path, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
#else
	in = F_NULL;
	fopen_s(&in, file, "rb");
#endif

	if (in == F_NULL) {
		perror(file);
		return false;
	}

	out = gzopen(outfile, mode);
	if (out == F_NULL) {
		//fprintf(stderr, "%s: can't gzopen %s\n", prog, outfile);
		return false;
	}

	gz_compress(in, out);

	if (bDelete)
		_unlink(file);

	return true;
}

// Uncompress the given file and remove the original.
bool file_uncompress(char* file) {
	local char buf[MAX_NAME_LEN];
	char *infile, *outfile;
	F_FILE out;
	gzFile in;
	int len = strlen(file);
#if defined(_WIN32_WCE)
	char path[MAX_PATH];
#endif

	strcpy_s(buf, file);

	if (len > SUFFIX_LEN && strcmp(file + len - SUFFIX_LEN, GZ_SUFFIX) == 0) {
		infile = file;
		outfile = buf;
		outfile[len - 3] = '\0';
	} else {
		outfile = file;
		infile = buf;
		strcat_s(infile, _countof(buf), GZ_SUFFIX);
	}
	in = gzopen(infile, "rb");
	if (in == F_NULL) {
		fprintf(stderr, "%s: can't gzopen %s\n", prog, infile);
		return false;
	}
#if defined(_WIN32_WCE)
	MultiByteToWideChar(CP_ACP, 0, outfile, -1, path, MAX_PATH);
	out = CreateFile(path, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
#else
	out = F_NULL;
	fopen_s(&out, outfile, "wb");
#endif
	if (out == F_NULL) {
		perror(file);
		return false;
	}

	gz_uncompress(in, out);

	_unlink(infile);

	return true;
}

long B1Compress(const char* szFilename, const char* outdir, const char* szOutFile) {
	char outfile[255];
	memset(&outfile[0], 0, 255);

	if (outdir != NULL) {
		char temp[_MAX_PATH]; // put in temp to avoid buffer overrun, and changing param
		strcpy_s(temp, outdir);
		if (outdir[strlen(temp) - 1] != '\\')
			strcat_s(temp, _countof(temp), "\\");

		CreateDirectoryW(KEP::Utf8ToUtf16(temp).c_str(), NULL);
	}

	sprintf_s(outfile, _countof(outfile), "%s\\%s.gz", outdir, szOutFile);

	if (!file_compress(szFilename, outfile, "wb6 ", 0)) {
		return -1;
	}

	long file_size = 0;
	WIN32_FIND_DATA FindData;
	HANDLE fp = FindFirstFileW(KEP::Utf8ToUtf16(outfile).c_str(), &FindData);
	if (fp) {
		file_size = FindData.nFileSizeLow;
	} else {
		file_size = -1;
	}

	FindClose(fp);

	return file_size;
}

#endif