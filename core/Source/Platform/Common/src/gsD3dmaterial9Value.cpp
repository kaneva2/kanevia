///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "gsD3dmaterial9Value.h"

namespace KEP {

BEGIN_GETSET_IMPL(gsD3dmaterial9Value, IDS_GSD3MATERIAL9VALUE_NAME)
GETSET_RGBA(m_material ? &m_material->Diffuse.r : 0, m_material ? &m_material->Diffuse.g : 0, m_material ? &m_material->Diffuse.b : 0, m_material ? &m_material->Diffuse.a : 0, GS_FLAGS_DEFAULT, m_indent, m_groupTitleId, GSD3MATERIAL9VALUE, DIFFUSE)
GETSET_RGBA(m_material ? &m_material->Ambient.r : 0, m_material ? &m_material->Ambient.g : 0, m_material ? &m_material->Ambient.b : 0, m_material ? &m_material->Ambient.a : 0, GS_FLAGS_DEFAULT, m_indent, m_groupTitleId, GSD3MATERIAL9VALUE, AMBIENT)
GETSET_RGBA(m_material ? &m_material->Specular.r : 0, m_material ? &m_material->Specular.g : 0, m_material ? &m_material->Specular.b : 0, m_material ? &m_material->Specular.a : 0, GS_FLAGS_DEFAULT, m_indent, m_groupTitleId, GSD3MATERIAL9VALUE, SPECULAR)
GETSET_RGBA(m_material ? &m_material->Emissive.r : 0, m_material ? &m_material->Emissive.g : 0, m_material ? &m_material->Emissive.b : 0, m_material ? &m_material->Emissive.a : 0, GS_FLAGS_DEFAULT, m_indent, m_groupTitleId, GSD3MATERIAL9VALUE, EMISSIVE)
GETSET_RANGE(m_material ? &m_material->Power : 0, gsFloat, GS_FLAGS_DEFAULT, m_indent, m_groupTitleId, GSD3MATERIAL9VALUE, POWER, FLOAT_MIN, FLOAT_MAX)
END_GETSET_IMPL

} // namespace KEP
