///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "glAdjust.h"

#include <atomic>

#include "LogHelper.h"
static LogInstance("Instance");

using namespace std;

static atomic<uint_fast32_t> s_frameCount = 0;
static atomic<TimeMs> s_frameTimestampMs = fTime::TimeMs();
static atomic<TimeMs> s_frameTimeMs = 0.0;
static atomic<double> s_frameTimeRatio = 1.0; // (frameTimeMs / frameTimeTargetMs)
static atomic<double> s_framesPerSec = 0.0;
static atomic<double> s_framesPerSecFilter = 0.0;

void RenderMetrics::Log() {
	LogInfo("frameCount=" << s_frameCount
						  << " frameTimestampMs=" << FMT_TIME << s_frameTimestampMs
						  << " frameTimeMs=" << FMT_TIME << s_frameTimeMs
						  << " frameTimeRatio=" << s_frameTimeRatio
						  << " framesPerSec=" << s_framesPerSecFilter);
}

void RenderMetrics::FrameUpdate() {
	++s_frameCount;
	auto frameTimestampMs = fTime::TimeMs();
	s_frameTimeMs = frameTimestampMs - s_frameTimestampMs;
	s_frameTimestampMs = frameTimestampMs;
	s_frameTimeRatio = s_frameTimeMs / c_frameTimeTargetMs;
	s_framesPerSec = s_frameTimeMs ? (1000.0 / s_frameTimeMs) : 0.0;
	s_framesPerSecFilter = 0.95 * s_framesPerSecFilter + 0.05 * s_framesPerSec;
}

uint_fast32_t RenderMetrics::GetFrameCount() {
	return s_frameCount;
}

TimeMs RenderMetrics::GetFrameTimestampMs() {
	return s_frameTimestampMs;
}

TimeMs RenderMetrics::GetFrameTimeMs() {
	return s_frameTimeMs;
}

double RenderMetrics::GetFrameTimeRatio() {
	return s_frameTimeRatio;
}

double RenderMetrics::GetFramesPerSec(bool filter) {
	return filter ? s_framesPerSecFilter : s_framesPerSec;
}
