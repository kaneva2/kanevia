///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GetSetUpdateData.h"

// TODO: once we re-integrate this, make sure this class is noop if on server since not needed
#ifdef EDITOR
#include "../KEPEdit/GetSetContextHandler.h" // todo include in here since this is only place used
#endif

namespace KEP {

enum GetSetUpdateData::EContextRet GetSetUpdateData::ContextMenu(GetSet* item, int screenX, int screenY, CWnd* parent) {
#ifdef EDITOR
	GetSetContextHandler ch;
	RECT rect;
	ZeroMemory(&rect, sizeof(rect));
	ch.Create(NULL, "GetSetContextHandler", 0, rect, parent, 1);
	ch.ShowWindow(SW_SHOW); // or else don't get WM_COMMAND ???

	CMenu menu;

	menu.CreatePopupMenu();

	// TESTTESTTEST
	menu.AppendMenu(MF_STRING, 1, "Test1");
	menu.AppendMenu(MF_STRING, 2, "Test2");
	menu.AppendMenu(MF_STRING, 3, "Test3");

	menu.TrackPopupMenu(TPM_LEFTALIGN, screenX, screenY, &ch);
#endif

	return crNoAction;
}

} // namespace KEP