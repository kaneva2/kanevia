///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 Lua 5.0 was historically linked as a static library by the client in both
 KEPMenu & LuaEventHandler projects, meaning there were actually two entire
 copies of Lua in the binary. Lua 5.1 relies on singletons to differentiate
 dummy nodes in tables and therefore must be linked as a DLL otherwise you
 get mismatched references which fail comparisons. LuaJIT is based on Lua 5.1
 which was experimented with however it has some fundamental compatibility
 issues with the client and has been deprecated.  Currently, the client and
 server use Lua v5.1.5, the final version of Lua before a more significant
 code and script migration to v5.2 will be required.

 CJSON is a DLL 'plugin' for lua which enables JSON style table encoding and
 decoding through the globally defined 'cjson' table.  CJSON itself depends on
 Lua and therefore it must also be re-compiled if ever Lua changes.
******************************************************************************/
#include "KEPHelpers.h"
#include "LuaHelper.h"
#include "LogHelper.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "Core/Crypto/Crypto_SHA2.h"
#include "config.h"
#include <fstream>
#include <strstream>

#pragma warning(disable : 4505) // Disable `unreferenced local function has been removed'

static LogInstance("Lua");

#ifdef PLATFORM_TEST
#define TOOLS_PATH "..\\Tools"
#else
#ifdef _KEP_MENU
#define TOOLS_PATH "..\\..\\..\\Tools"
#else
#define TOOLS_PATH "..\\..\\Tools"
#endif
#endif

#if (LUA_SELECTION == 1)

// Lua 5.1.4 - Link As DLL (requires ...\Tools\Lua\luadll_514*.dll)
#if (_DEBUG)
#pragma comment(lib, TOOLS_PATH "\\lua\\lib\\luadll_514d.lib")
#else
#pragma comment(lib, TOOLS_PATH "\\lua\\lib\\luadll_514.lib")
#endif
#if (LUA_CJSON == 1)

// Lua CJSON - Link As DLL (requires ...\Tools\lua-cjson\cjson*.dll)
#if (_DEBUG)
#pragma comment(lib, TOOLS_PATH "\\lua-cjson\\cjsond.lib")
#else
#pragma comment(lib, TOOLS_PATH "\\lua-cjson\\cjson.lib")
#endif
#endif

#elif (LUA_SELECTION == 2)

// Lua 5.1.5 - Link As DLL (requires ...\Tools\Lua-5.1.5\luadll_515*.dll)
#if (_DEBUG)
#pragma comment(lib, TOOLS_PATH "\\lua-5.1.5\\lib\\luadll_515d.lib")
#else
#pragma comment(lib, TOOLS_PATH "\\lua-5.1.5\\lib\\luadll_515.lib")
#endif

// Lua CJSON - Link As DLL (requires ...\Tools\lua-cjson-5.1.5\cjson_515*.dll)
#if (_DEBUG)
#pragma comment(lib, TOOLS_PATH "\\lua-cjson-5.1.5\\cjson_515d.lib")
#else
#pragma comment(lib, TOOLS_PATH "\\lua-cjson-5.1.5\\cjson_515.lib")
#endif

#elif (LUA_SELECTION == 3)

// Lua 5.1.5 CoCo - Link As DLL (requires ...\Tools\Lua-5.1.5-coco\luadll_515co*.dll)
#if (_DEBUG)
#pragma comment(lib, "luadll_515cod.lib")
#else
#pragma comment(lib, "luadll_515co.lib")
#endif

// Lua CJSON - Link As DLL (requires ...\Tools\lua-cjson-5.1.5-coco\cjson_515co*.dll)
#if (_DEBUG)
#pragma comment(lib, "cjson_515cod.lib")
#else
#pragma comment(lib, "cjson_515co.lib")
#endif

#elif (LUA_SELECTION == 4)

// Lua 5.2.2 - Link As DLL (requires ...\Tools\Lua-5.2.2\luadll_522*.dll)
#if (_DEBUG)
#pragma comment(lib, TOOLS_PATH "\\lua-5.2.2\\lib\\luadll_522d.lib")
#else
#pragma comment(lib, TOOLS_PATH "\\lua-5.2.2\\lib\\luadll_522.lib")
#endif

// Lua CJSON - Link As DLL (requires ...\Tools\lua-cjson-5.2.2\cjson_522*.dll)
#if (_DEBUG)
#pragma comment(lib, TOOLS_PATH "\\lua-cjson-5.2.2\\cjson_522d.lib")
#else
#pragma comment(lib, TOOLS_PATH "\\lua-cjson-5.2.2\\cjson_522.lib")
#endif

#endif // LUA_SELECTION

std::string LuaVersion() {
	std::string verStr;
	StrBuild(verStr, LUA_RELEASE);
	return verStr;
}

static bool verifyScript(const char *filePath, const unsigned char *fileContent, FileSize numBytes, bool allowDynamicScript);
static std::map<std::string, std::vector<std::vector<unsigned char>>> s_loadedScriptHashes;
static std::set<std::string> s_dynamicLuaScripts = { "urls.lua", "gainsttrk.lua", "customcolors.lua" }; // Dynamically generated Lua scripts. Authenticity verification will be skipped

// Replace luaL_loadfile to handle signed and/or encrypted script and etc
int LuaLoadFileProtected(lua_State *L, const char *filePath, bool allowDynamicScript) {
	assert(filePath != nullptr);
	if (filePath == nullptr) {
		return LUA_ERRERR;
	}

	// Check for developer authorization
	bool verifyScriptDigest = true, binaryOnly = false; //TODO: enable binaryOnly check on preview and prod
	bool isAuthDev = true;
	if (isAuthDev) {
		verifyScriptDigest = false;
		binaryOnly = false;
	}

	// Determine file size
	auto fileSize = FileHelper::Size(filePath);
	assert(fileSize != 0);
	if (fileSize == 0) {
		LogError("MISSING or EMPTY [" << filePath << "]");
		return LUA_ERRFILE;
	}

	// Read entire file
	std::fstream ifs(filePath, std::ios::in | std::ios::binary);
	assert(ifs.good());
	if (!ifs.good()) {
		LogError("ERROR opening [" << filePath << "]");
		return LUA_ERRFILE;
	}

	auto fileContentPtr = std::make_unique<char[]>(fileSize);
	assert(fileContentPtr);
	if (!fileContentPtr) {
		LogError("BAD ALLOC (" << fileSize << " bytes) [" << filePath << "]");
		return LUA_ERRMEM;
	}

	ifs.read(fileContentPtr.get(), fileSize);
	assert(ifs.good());
	if (!ifs.good()) {
		LogError("ERROR reading [" << filePath << "]");
		return LUA_ERRFILE;
	}

	// Check Lua signature
	struct LUAC_HEADER {
		unsigned signature;
		unsigned char version;
		unsigned char format;
	};

	LUAC_HEADER *header = reinterpret_cast<LUAC_HEADER *>(fileContentPtr.get());
	bool isBinary = memcmp(&header->signature, LUA_SIGNATURE, strlen(LUA_SIGNATURE)) == 0;
	if (!isBinary) {
		if (binaryOnly) {
			LogError("NOT compiled [" << filePath << "]");
			// Lua source not allowed
			return LUA_ERRFILE;
		}

#ifdef SUPPORT_LUAC_INVERTED_VERSION_AND_FORMAT
		if ((header->version & 0x80) != 0 && (header->format & 0x80) != 0) {
			// LUAC header is obfuscated
			header->version = header->version ^ 0xff;
			header->format = header->format ^ 0xff;
		}
#endif
	}

	// Verify checksum if requested
	if (verifyScriptDigest && !verifyScript(filePath, reinterpret_cast<const unsigned char *>(fileContentPtr.get()), fileSize, allowDynamicScript)) {
		assert(false);
		LogError("BAD [" << filePath << "]");
		return LUA_ERRFILE;
	}

	if (isBinary) {
		// Call lua_loadbuffer
		return luaL_loadbuffer(L, fileContentPtr.get(), (size_t)fileSize, filePath);
	} else {
		// Call lua_loadfile to allow proper handling of DOS-style line-endings
		return luaL_loadfile(L, filePath);
	}
}

bool LuaLoadScriptInfo(const std::string &directory) {
	// scriptinfo validation disabled
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Lua tamper checker
#include <set>
#include <map>

bool verifyScript(const char *filePath, const unsigned char *fileContent, FileSize numBytes, bool allowDynamicScript) {
#ifndef SCRIPT_CHECKSUM_METHOD
	return true;
#endif

	assert(filePath != nullptr && filePath[0] != 0);
	if (filePath == nullptr || filePath[0] == 0) {
		return false;
	}

	// Lookup checksum
	std::string sFileName = StrStripFilePath(filePath);
	STLToLower(sFileName);

	if (allowDynamicScript && s_dynamicLuaScripts.find(sFileName) != s_dynamicLuaScripts.end()) {
		// Skip verifications for dynamically generated Lua scripts
		return true;
	}

#ifdef ENABLE_SCRIPTINFO_VALIDATION
	auto itr = s_loadedScriptHashes.find(sFileName);
	if (itr == s_loadedScriptHashes.end()) {
		LogError("Unknown script: " << sFileName);
		return false;
	}

	if (itr->second.empty()) {
		LogError("Bad script info: " << sFileName);
		return false;
	}

	for (const auto &hashBytes : itr->second) {
#if SCRIPT_CHECKSUM_METHOD == SCRIPT_CHECKSUM_MD5
		if (MD5VerifyData(fileContent, numBytes, &hashBytes[0], hashBytes.size())) {
#elif SCRIPT_CHECKSUM_METHOD == SCRIPT_CHECKSUM_SHA256
		if (SHA256VerifyData(fileContent, numBytes, &hashBytes[0], hashBytes.size())) {
#endif
			return true;
		}
	}

	return false;
#else
	// scriptInfo validation disabled
	return true;
#endif
}
