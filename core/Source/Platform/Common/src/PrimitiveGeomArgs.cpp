///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PrimitiveGeomArgs.h"
#include "Core/Math/Sphere.h"
#include "Core/Math/Cuboid.h"
#include "Core/Math/Cylinder.h"
#include "Core/Math/RectangularFrustum.h"
#include "Core/Math/CircularFrustum.h"
#include "Event/Base/IReadableBuffer.h"
#include "Event/Base/IWriteableBuffer.h"

namespace KEP {

template <typename T, template <typename, typename> typename P, size_t A, typename... Args>
struct CreatePrimitiveByAxis {
	static Primitive<T>* Call(size_t axis, Args... args) {
		if (axis == A) {
			return new P<T, Primitive<T>::Axis<A>>(args...);
		}
		return CreatePrimitiveByAxis<T, P, A + 1, Args...>::Call(axis, args...);
	}
};

template <typename T, template <typename, typename> typename P, typename... Args>
struct CreatePrimitiveByAxis<T, P, 3, Args...> {
	static Primitive<T>* Call(size_t, Args...) {
		// Invalid axis
		assert(false);
		return nullptr;
	}
};

// Primitives with axis support (last type argument is specialization helper and not used in definition)
template <typename T, template <typename, typename...> typename P, typename = bool>
struct CreatePrimitive {
	template <typename... Args>
	static Primitive<T>* Call(size_t axis, Args... args) {
		return CreatePrimitiveByAxis<T, P, 0, Args...>::Call(axis, args...);
	}
};

// Primitives with no-axis support
template <typename T, template <typename, typename...> typename P>
struct CreatePrimitive<T, P, std::enable_if_t<std::is_class<P<T>>::value, bool>> {
	template <typename... Args>
	static Primitive<T>* Call(Args... args) {
		return new P<T>(args...);
	}
};

template <typename T>
Primitive<T>* PrimitiveGeomArgs<T>::createPrimitive() const {
	switch (type) {
		case PrimitiveType::Sphere:
			return CreatePrimitive<T, Sphere>::Call(Vector3<T>::Zero(), sphere.radius);
		case PrimitiveType::Cuboid:
			return CreatePrimitive<T, Cuboid>::Call(cuboid.min, cuboid.max);
		case PrimitiveType::Cylinder:
			return CreatePrimitive<T, Cylinder>::Call(axis, cylinder.radius, cylinder.height, cylinder.base);
		case PrimitiveType::RectangularFrustum:
			return CreatePrimitive<T, RectangularFrustum>::Call(axis, rectangularFrustum.fov, rectangularFrustum.aspect, rectangularFrustum.nearPlane, rectangularFrustum.farPlane);
		case PrimitiveType::CircularFrustum:
			return CreatePrimitive<T, CircularFrustum>::Call(axis, circularFrustum.fov, circularFrustum.nearPlane, circularFrustum.farPlane);
	}

	assert(false);
	return nullptr;
}

template <typename T>
IReadableBuffer& operator>>(IReadableBuffer& inBuffer, PrimitiveGeomArgs<T>& val) {
	memset(&val, 0, sizeof(PrimitiveGeomArgs<T>));
	inBuffer >> val.type;
	inBuffer >> val.axis;
	switch (val.type) {
		case PrimitiveType::Sphere:
			inBuffer >> val.sphere.radius;
			break;
		case PrimitiveType::Cuboid:
			inBuffer >> val.cuboid.min >> val.cuboid.max;
			break;
		case PrimitiveType::Cylinder:
			inBuffer >> val.cylinder.radius >> val.cylinder.height >> val.cylinder.base;
			break;
		case PrimitiveType::RectangularFrustum:
			inBuffer >> val.rectangularFrustum.fov >> val.rectangularFrustum.nearPlane >> val.rectangularFrustum.farPlane >> val.rectangularFrustum.aspect;
			break;
		case PrimitiveType::CircularFrustum:
			inBuffer >> val.circularFrustum.fov >> val.circularFrustum.nearPlane >> val.circularFrustum.farPlane;
			break;
		default:
			assert(false);
			break;
	}

	return inBuffer;
}

template <typename T>
IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const PrimitiveGeomArgs<T>& val) {
	outBuffer << val.type;
	outBuffer << val.axis;
	switch (val.type) {
		case PrimitiveType::Sphere:
			outBuffer << val.sphere.radius;
			break;
		case PrimitiveType::Cuboid:
			outBuffer << val.cuboid.min << val.cuboid.max;
			break;
		case PrimitiveType::Cylinder:
			outBuffer << val.cylinder.radius << val.cylinder.height << val.cylinder.base;
			break;
		case PrimitiveType::RectangularFrustum:
			outBuffer << val.rectangularFrustum.fov << val.rectangularFrustum.nearPlane << val.rectangularFrustum.farPlane << val.rectangularFrustum.aspect;
			break;
		case PrimitiveType::CircularFrustum:
			outBuffer << val.circularFrustum.fov << val.circularFrustum.nearPlane << val.circularFrustum.farPlane;
			break;
		default:
			assert(false);
			break;
	}

	return outBuffer;
}

// Explicit instantiation for float
template PrimitiveGeomArgs<float>;
template IReadableBuffer& operator>>(IReadableBuffer& inBuffer, PrimitiveGeomArgs<float>& val);
template IWriteableBuffer& operator<<(IWriteableBuffer& outBuffer, const PrimitiveGeomArgs<float>& val);

} // namespace KEP
