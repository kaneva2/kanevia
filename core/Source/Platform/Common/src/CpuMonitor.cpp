///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CpuMonitor.h"
#include "KEPHelpers.h"

namespace KEP {

CpuMonitor::CpuMonitor(Logger& logger) :
		jsThread("CpuMonitor"),
		m_logger(logger),
		m_interval(60000),
		m_processLastValue(0),
		m_processorCount(1) {
}

/// \brief initialize the CpuMonitor and start the background thread
///
/// \return true if ok, false if failed to get processor count.  Will log
bool CpuMonitor::Initialize(TimeMs interval) {
	m_interval = std::max(interval, 1000.0); // at least 1 sec

	PSYSTEM_LOGICAL_PROCESSOR_INFORMATION pInfo = 0;
	DWORD buffLen = 0;
	// get buffer size
	GetLogicalProcessorInformation(pInfo, &buffLen);
	DWORD e = GetLastError();
	if (e == ERROR_INSUFFICIENT_BUFFER && buffLen > 0) {
		void* buffer = malloc(buffLen);
		if (buffer == 0)
			return false;
		pInfo = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)buffer;

		if (GetLogicalProcessorInformation(pInfo, &buffLen)) {
			m_processorCount = 0;
			ULONG byteOffset = 0;
			while (byteOffset < buffLen) {
				LOG4CPLUS_TRACE(m_logger, "ProcessorMask:    " << pInfo->ProcessorMask);
				LOG4CPLUS_TRACE(m_logger, "    Relationship: " << pInfo->Relationship);

				switch (pInfo->Relationship) {
					case RelationProcessorCore:
						m_processorCount++;
						break;

					default:
						break;
				}
				byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
				pInfo++;
			}
			jsAssert(m_processorCount > 0);
			if (m_processorCount == 0)
				m_processorCount = 1;
		} else {
			e = GetLastError();
			std::string s;
			LOG4CPLUS_WARN(m_logger, "Error getting begin number of processors: " << errorNumToString(e, s));
			free(buffer);
			return false;
		}
		free(buffer);
	} else {
		std::string s;
		LOG4CPLUS_WARN(m_logger, "Error getting begin number of processors: " << errorNumToString(e, s));
		return false;
	}

	LOG4CPLUS_INFO(m_logger, "CpuMonitor initialized with the following values: ");
	LOG4CPLUS_INFO(m_logger, "    Interval ms:             " << FMT_TIME << m_interval);
	LOG4CPLUS_INFO(m_logger, "    Processor count:         " << m_processorCount);
	start();

	return true;
}

/// \brief terminate the CpuMonitor background thread
void CpuMonitor::Terminate() {
	stop();
}

/// \brief call to monitor the calling thread
///
/// \return your thread id, or zero if could not get thread handle
DWORD CpuMonitor::MonitorMe() {
	DWORD thdId = 0;
	HANDLE pseudoH = GetCurrentThread();
	HANDLE h;
	if (DuplicateHandle(GetCurrentProcess(), pseudoH, GetCurrentProcess(), &h, 0, FALSE, DUPLICATE_SAME_ACCESS) &&
		h) {
		LOG4CPLUS_INFO(m_logger, "Monitoring CPU thread of id " << thdId);

		thdId = GetCurrentThreadId();
		std::lock_guard<fast_recursive_mutex> lock(getSync());
		ThreadList::iterator i = m_thds.begin();
		for (; i != m_thds.end(); i++) {
			if (i->id == thdId) {
				i->handle = h;
				break;
			}
		}
		if (i == m_thds.end())
			m_thds.push_back(ThreadInfo(thdId, h));
	}
	return thdId;
}

/// \brief override from jsThread
///
/// loops, monitoring all the threads and the main process
ULONG CpuMonitor::_doWork() {
	HANDLE pHandle = GetCurrentProcess();

	// for the process
	FILETIME ignoreCreate, ignoreExit;
	FILETIME beginKernelTime, beginUserTime;
	FILETIME endKernelTime, endUserTime;

	ULARGE_INTEGER bk, bu, ek, eu;

	while (!isTerminated()) {
		Timer timer;

		// get the start times
		if (!GetProcessTimes(pHandle, &ignoreCreate, &ignoreExit, &beginKernelTime, &beginUserTime)) {
			DWORD e = GetLastError();
			LOG4CPLUS_WARN(m_logger, "Error getting begin process times: " << numToString(e));
		}

		{
			std::lock_guard<fast_recursive_mutex> lock(m_sync);
			for (ThreadList::iterator i = m_thds.begin(); i != m_thds.end(); i++) {
				if (!GetThreadTimes(i->handle, &i->ignoreCreate, &i->ignoreExit, &i->beginKernelTime, &i->beginUserTime)) {
					DWORD e = GetLastError();
					LOG4CPLUS_WARN(m_logger, "Error getting begin thread times: " << i->id << numToString(e, " 0x%x"));
				} else {
					i->hadStart = true;
				}
			}
		}

		// wait for it....
		fTime::SleepMs(m_interval);

		if (!isTerminated()) {
			// get the end times
			if (!GetProcessTimes(pHandle, &ignoreCreate, &ignoreExit, &endKernelTime, &endUserTime)) {
				DWORD e = GetLastError();
				LOG4CPLUS_WARN(m_logger, "Error getting end process times: " << numToString(e));
			}

			{
				std::lock_guard<fast_recursive_mutex> lock(m_sync);
				for (ThreadList::iterator i = m_thds.begin(); i != m_thds.end(); i++) {
					if (i->hadStart) {
						if (!GetThreadTimes(i->handle, &i->ignoreCreate, &i->ignoreExit, &i->endKernelTime, &i->endUserTime)) {
							DWORD e = GetLastError();
							LOG4CPLUS_WARN(m_logger, "Error getting end thread times for thread: " << i->id << numToString(e, " 0x%x"));
						}
					}
				}
			}

			TimeMs realInterval = timer.ElapsedMs();

			// calculate the percentages
			// 		note that the FILETIME values are in 100ns units
			// 		MSDN recommends memcpy instead of member-wise copy to ULARGE_INTEGER
			memcpy(&bk, &beginKernelTime, sizeof(bk));
			memcpy(&ek, &endKernelTime, sizeof(ek));
			memcpy(&bu, &beginUserTime, sizeof(bu));
			memcpy(&eu, &endUserTime, sizeof(eu));
			int process_pct = (((eu.QuadPart - bu.QuadPart) + (ek.QuadPart - bk.QuadPart)) / (realInterval * 100));
			m_processLastValue = process_pct / m_processorCount;

			{
				std::lock_guard<fast_recursive_mutex> lock(m_sync);
				for (ThreadList::iterator i = m_thds.begin(); i != m_thds.end(); i++) {
					if (i->hadStart) {
						memcpy(&bk, &i->beginKernelTime, sizeof(bk));
						memcpy(&ek, &i->endKernelTime, sizeof(ek));
						memcpy(&bu, &i->beginUserTime, sizeof(bu));
						memcpy(&eu, &i->endUserTime, sizeof(eu));
						int thread_pct = (((eu.QuadPart - bu.QuadPart) + (ek.QuadPart - bk.QuadPart)) / (realInterval * 100));

						i->lastValue = thread_pct;
						if (thread_pct > 300 || thread_pct < 0) {
							LOG4CPLUS_TRACE(m_logger, "Bad pct for : " << i->id << " " << thread_pct);
						}
					}
				}
			}
		}
	}

	// close all the duped handles
	for (ThreadList::iterator i = m_thds.begin(); i != m_thds.end(); i++) {
		CloseHandle(i->handle);
	}
	m_thds.clear();

	return 0;
}

} // namespace KEP