///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IClientEngine.h"
#include "RuntimeCameras.h"
#include "CCameraClass.h"

#include "LogHelper.h"

namespace KEP {

static LogInstance("Instance");

// goal is to ensure each runtime camera has a unique name to make identifying them easier
// the format of the new name will be: name_n, where n is an integer
// given the base_name (which will usually be the name of the template camera used to instantiate
// this runtime camera) a search will be made to find any other occurrences of this name as a prefix
// and return a new name with the suffix incremented by 1
std::string RuntimeCameras::GetNewCameraName(const std::string& base_name) {
	CStringA name = "";
	int highest_suffix = 0;
	int index = -1;
	CStringA cBaseName = base_name.c_str();
	cBaseName += "_";
	for (const auto& itr : m_cameras) {
		auto pCO = itr.second;
		if (!pCO)
			continue;
		CStringA cName = pCO->m_cameraName.c_str();
		index = cName.Find(cBaseName);
		if (index > -1) {
			int suffix_index = cName.ReverseFind('_');
			CStringA suffix = cName.Mid(suffix_index + 1);
			int suffix_val = atoi(suffix);
			if (suffix_val > highest_suffix)
				highest_suffix = suffix_val;
		}
	}
	name.Format("%s%d", cBaseName, highest_suffix + 1);
	return name.GetString();
}

// Add New Camera As Default
CCameraObj* RuntimeCameras::AddDefaultCamera() {
	uint32_t cameraId = GetNewCameraId();
	std::string new_camera_name = GetNewCameraName("DefaultCamera");
	auto pCO = new CCameraObj(new_camera_name);
	pCO->m_baseFrame = new CFrameObj();
	m_cameras.insert(CameraMap::value_type(cameraId, pCO));
	LogInfo(pCO->ToStr() << " (" << m_cameras.size() << " cameras)");
	return pCO;
}

// Add New Camera As Copy Of Existing
uint32_t RuntimeCameras::Add(uint32_t db_camera_index) {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return 0;

	CCameraObjList* pCOL = pICE->GetCameraList();
	POSITION db_cam_pos = pCOL->FindIndex(db_camera_index);
	if (!db_cam_pos)
		return 0;

	auto pCO = (CCameraObj*)pCOL->GetAt(db_cam_pos);
	if (!pCO)
		return 0;

	uint32_t cameraId = GetNewCameraId();
	std::string new_camera_name = GetNewCameraName(pCO->m_cameraName);
	auto pCO_new = new CCameraObj(new_camera_name);
	*pCO_new = *pCO;
	pCO_new->m_baseFrame = new CFrameObj();
	m_cameras.insert(CameraMap::value_type(cameraId, pCO_new));

	LogInfo(pCO_new->ToStr() << " (" << m_cameras.size() << " cameras)");

	return cameraId;
}

bool RuntimeCameras::Del(CCameraObj* pCO) {
	if (!pCO)
		return false;
	for (auto itr = m_cameras.begin(); itr != m_cameras.end(); ++itr) {
		auto pCO_find = itr->second;
		if (pCO_find == pCO) {
			LogInfo(pCO->ToStr() << " (" << (m_cameras.size() - 1) << " cameras)");
			delete pCO;
			m_cameras.erase(itr);
			return true;
		}
	}
	return false;
}

// 'DefaultCameras' Deleted By CViewportObj !!!
void RuntimeCameras::DelAllExceptDefault() {
	auto cameras = m_cameras;
	for (const auto& itr : cameras) {
		auto pCO = itr.second;
		if (!pCO || STLContains(pCO->m_cameraName, "DefaultCamera"))
			continue;
		Del(pCO);
	}
}

CCameraObj* RuntimeCameras::GetCamera(uint32_t cameraId) {
	auto itr = m_cameras.find(cameraId);
	return (itr != m_cameras.end()) ? itr->second : nullptr;
}

void RuntimeCameras::Update() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;
	for (const auto& itr : m_cameras) {
		auto cameraId = itr.first;
		auto pCO = itr.second;
		if (!pCO)
			continue;
		pICE->SendCameraEvent(pCO, CameraAction::Update, cameraId);
	}
}

} // namespace KEP