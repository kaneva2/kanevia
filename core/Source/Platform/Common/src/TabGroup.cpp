/******************************************************************************
 TabGroup.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
//
// TabGroup.cpp : implementation file
//

#include "stdafx.h"
#include "TabGroup.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTabGroup

CTabGroup::CTabGroup() :
		m_bEnableResize(TRUE) {
	m_pFrame = NULL;
	m_bPrevState = 0;
}

CTabGroup::~CTabGroup() {
	m_observers.clear();
}

BEGIN_MESSAGE_MAP(CTabGroup, CKEPImageToggleButton)
//{{AFX_MSG_MAP(CTabGroup)
ON_CONTROL_REFLECT_EX(BN_CLICKED, OnClicked)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTabGroup message handlers

BOOL CTabGroup::OnClicked() {
	if (m_pFrame == NULL || m_pParentFrame == NULL)
		return 0;

	if (!GetSelected()) {
		SetSelected(TRUE);
		m_bPrevState = 0;

		Expand();
	}

	return FALSE;
}

//call this function in your dialog's constuctor
//pFrame must point to static in which contols are about to be hidden or shown
void CTabGroup::SetFrame(CWnd* pFrame) {
	ASSERT(pFrame != NULL);
	m_pFrame = pFrame;
}

//call this function in your dialog's constuctor
//pFrame must point to static in which contols are about to be hidden or shown
void CTabGroup::SetParentFrame(CWnd* pParentFrame) {
	ASSERT(pParentFrame != NULL);
	m_pParentFrame = pParentFrame;
}

void CTabGroup::PreSubclassWindow() {
	ASSERT(m_pFrame);
	ASSERT(m_pParentFrame);
	ASSERT(::IsWindow(m_pFrame->m_hWnd));
	ASSERT(::IsWindow(m_pParentFrame->m_hWnd));

	RecalcLayout();

	m_bPrevState = GetSelected();

	if (m_bPrevState == 0)
		Collapse();

	CKEPImageToggleButton::PreSubclassWindow();
}

void CTabGroup::Expand() {
	for (int i = 0; i <= m_haInsideWindows.GetUpperBound(); i++) {
		::ShowWindow(m_haInsideWindows[i], SW_SHOW);
		::RedrawWindow(m_haInsideWindows[i], NULL, NULL, RDW_UPDATENOW);
	}

	if (m_bEnableResize) {
		/*
				CWnd* pParent = GetParent();
				ASSERT(pParent);
				int dx = m_rcMaxSize.bottom - m_rcMinSize.top - 20;
				RECT rcWnd;

				pParent->GetWindowRect(&rcWnd);
				rcWnd.bottom += dx;
				pParent->MoveWindow(&rcWnd);
		*/
	}

	for (ObserverIterator itor = m_observers.begin(); itor < m_observers.end(); itor++) {
		(*itor)->DoUpdate();
	}
}

void CTabGroup::Collapse() {
	for (int i = 0; i <= m_haInsideWindows.GetUpperBound(); i++) {
		::ShowWindow(m_haInsideWindows[i], SW_HIDE);
	}

	m_pFrame->ShowWindow(SW_HIDE);

	if (m_bEnableResize) {
		/*
				CWnd* pParent = GetParent();
				ASSERT(pParent);
				RECT rcWnd;
		//		int dx = m_rcMaxSize.bottom - m_rcMinSize.top - 20;

				pParent->GetWindowRect(&rcWnd);
				rcWnd.bottom -= dx;
				pParent->MoveWindow(&rcWnd);
		*/
	}
}

BOOL CALLBACK CTabGroup::WndEnumProc(HWND hWnd, LPARAM lParam) {
	CTabGroup* p = (CTabGroup*)lParam;
	ASSERT(p);
	HWND hPar = ::GetParent(p->m_hWnd);
	if (::GetParent(hWnd) != hPar || !(::GetWindowLong(hWnd, GWL_STYLE) & WS_VISIBLE))
		return TRUE;

	RECT rcWnd;
	::GetWindowRect(hWnd, &rcWnd);
	CWnd* pWnd = CWnd::FromHandle(hPar);
	pWnd->ScreenToClient(&rcWnd);

	// is it inside?
	if (rcWnd.top > p->m_rcMaxSize.top && rcWnd.bottom < p->m_rcMaxSize.bottom && rcWnd.right < p->m_rcMaxSize.right && rcWnd.left > p->m_rcMaxSize.left) {
		p->m_haInsideWindows.Add(hWnd);
	}

	return TRUE;
}

void CTabGroup::SetCheck(int nCheck) {
	SetSelected(nCheck);

	if (nCheck == m_bPrevState)
		return;

	if (nCheck == 0)
		Collapse();
	else
		Expand();

	m_bPrevState = nCheck;
}

void CTabGroup::RecalcLayout() {
	CWnd* pParent = GetParent();
	ASSERT(pParent);

	GetWindowRect(&m_rcMinSize);

	if (m_pFrame != NULL) {
		if (::IsWindow(m_pFrame->m_hWnd))
			m_pFrame->GetWindowRect(&m_rcMaxSize);
	}

	m_rcMinSize.left = m_rcMaxSize.left;
	m_rcMinSize.right = m_rcMaxSize.right;

	pParent->ScreenToClient(&m_rcMinSize);
	pParent->ScreenToClient(&m_rcMaxSize);

	m_haInsideWindows.RemoveAll();

	::EnumChildWindows(pParent->m_hWnd, WndEnumProc, (long)this);

	PositionChildWindows();
}

void CTabGroup::PositionChildWindows() {
	CWnd* pParent = GetParent();
	ASSERT(pParent);
	RECT rcWndParent;
	pParent->GetWindowRect(&rcWndParent);

	RECT rcWndParentFrame;
	m_pParentFrame->GetWindowRect(&rcWndParentFrame);
	pParent->ScreenToClient(&rcWndParentFrame);

	RECT rcWndFrame;
	m_pFrame->GetWindowRect(&rcWndFrame);
	pParent->ScreenToClient(&rcWndFrame);

	int dy = rcWndParentFrame.top - rcWndFrame.top;
	int dx = rcWndParentFrame.left - rcWndFrame.left;

	for (int i = 0; i <= m_haInsideWindows.GetUpperBound(); i++) {
		RECT rcWnd;
		::GetWindowRect(m_haInsideWindows[i], &rcWnd);
		pParent->ScreenToClient(&rcWnd);

		rcWnd.left += dx;
		rcWnd.right += dx;
		rcWnd.top += dy;
		rcWnd.bottom += dy;

		::MoveWindow(m_haInsideWindows[i], rcWnd.left, rcWnd.top, rcWnd.right - rcWnd.left,
			rcWnd.bottom - rcWnd.top, TRUE);
	}

	// adjust the frame too
	rcWndFrame.left += dx;
	rcWndFrame.right += dx;
	rcWndFrame.top += dy;
	rcWndFrame.bottom += dy;

	m_pFrame->MoveWindow(rcWndFrame.left, rcWndFrame.top, rcWndFrame.right - rcWndFrame.left,
		rcWndFrame.bottom - rcWndFrame.top, TRUE);
}

void CTabGroup::DoUpdate() {
	if (GetSelected()) {
		SetSelected(FALSE);
		m_bPrevState = 1;

		Collapse();
	}
}

void CTabGroup::AddObserver(IObserver* pObserver) {
	m_observers.push_back(pObserver);
}
