///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define NUM_NAMING_TYPES 2
#define NAMING_STATIC 0
#define NAMING_SKELETAL 1

#define NUM_RESULT_STRINGS 5
#define RESULT_TYPE 0
#define RESULT_OBJECT 1
#define RESULT_PART 2
#define RESULT_VARIATION 3
#define RESULT_LOD 4
#define RESULT_UNKNOWN -1

#define NUM_NAMING_TOKENS 30
#define NUM_NAMING_REOUTS NUM_RESULT_STRINGS

namespace KEP {

struct NamingSyntax {
	static NamingSyntax defaultNamingConvention[NUM_NAMING_TYPES];
	static std::string resultVariables[NUM_RESULT_STRINGS];

	enum { BASIC = 0,
		REGEX = 1,
		INVALID = -1 };
	int type;
	struct {
		std::string tokens[NUM_NAMING_TOKENS];
		int firstOptional, lastOptional; // optional tokens
	};
	struct {
		std::string regex;
		std::string regexOutputs[NUM_NAMING_REOUTS];
	};

	NamingSyntax();
	NamingSyntax(const std::string(&t)[NUM_NAMING_TOKENS], int firstOpt = -1, int lastOpt = -1);
	NamingSyntax(const std::string& re, const std::string(&reo)[NUM_NAMING_REOUTS]);
	std::string getSyntaxString();
	BOOL parseString(const std::string& input, std::string(&result)[NUM_RESULT_STRINGS]);

protected:
	BOOL parseStringBasic(const std::string& input, std::string(&result)[NUM_RESULT_STRINGS]);
	BOOL parseStringRegex(const std::string& input, std::string(&result)[NUM_RESULT_STRINGS]);

	static BOOL doParseStringBasic(std::string& unparsedString, std::string(&result)[NUM_RESULT_STRINGS],
		const std::string* tokenArray, int tokenCount,
		const std::string* endingDlmtrs, int endingDlmtrCnt, // optional ending delimiters for last token
		BOOL bBackward, BOOL bPartial, BOOL bOptional);

	static BOOL parseStringBasic_OptionalMatch(std::string& unparsedString, std::string(&result)[NUM_RESULT_STRINGS], const std::string* tokenArray, int tokenCount);
	static BOOL parseSingleVariable(std::string& unparsedString, std::string& result, const std::string* delimiters, int delimiterCount, BOOL bBackward);
	static BOOL parseSingleLiteral(std::string& unparsedString, const std::string& literalToken, BOOL bBackward);

	enum TokenType { TOKEN_LITERAL,
		TOKEN_VARIABLE };
	static TokenType parseToken(const std::string& token, std::string& varName, int& resultID);
	static TokenType parseToken(const std::string& token);
};

} // namespace KEP