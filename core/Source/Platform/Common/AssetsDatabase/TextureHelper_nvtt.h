///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEP/Images/KEPImageFormat.h"

typedef uint64_t FileSize;

namespace KEP {

class TextureHelper {
public:
	static bool IsImageAlreadyConverted(
		const char* inputPath, unsigned int sizeLimit, const char* failSafeOutputPath,
		bool& exists, std::string& outputPath,
		unsigned int& width, unsigned int& height, FileSize& bytes,
		unsigned int& origWidth, unsigned int& origHeight, FileSize& origBytes,
		ImageOpacity& opacity, unsigned int& averageColor);

	static bool ConvertImageToDDS(
		const char* inputPath, const char* outputPath,
		TEXTURE_WRAP_TYPE wrapType, unsigned int sizeLimit, bool requirePowerOf2, bool fast,
		unsigned int& width, unsigned int& height, FileSize& bytes,
		unsigned int& origWidth, unsigned int& origHeight, FileSize& origBytes,
		ImageOpacity& opacity, unsigned int& averageColor);

	// Conversion history query
	static bool GetRawImagePath(const std::string& convertedImagePath, std::string& rawImagePath, bool allowFailSafePath = false);
	static bool GetConvertedImagePath(const std::string& rawImagePath, unsigned int sizeLimit, std::string& convertedImagePath, bool returnFailSafePath = false);

	static bool GetConversionInfo(
		const std::string& convertedImagePath,
		unsigned int& width, unsigned int& height, FileSize& bytes,
		unsigned int& origWidth, unsigned int& origHeight, FileSize& origBytes,
		ImageOpacity& opacity, unsigned int& averageColor);

	// Clear recent conversion history when it's no longer needed. Do not call it until the player leaves the current zone.
	static void ClearConversionHistory();
};

} // namespace KEP
