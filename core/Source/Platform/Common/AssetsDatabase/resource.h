///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDC_CB_IMPORTTYPE               1001
#define IDC_CB_VIEWBY                   1002
#define IDC_TREE_AVAILASSET             1003
#define IDC_BTN_IMPORTALL               1004
#define IDC_BTN_ADVANCED                1005
#define IDC_CK_IMPORT_LODS              1007
#define IDC_LIST_IMPORT                 1008
#define IDC_RB_PREVIEWMESH              1009
#define IDC_RB_PREVIEWTEXTURED          1010
#define IDC_BTN_ADDTOLIST               1011
#define IDC_BTN_REMOVEFROMLIST          1012
#define IDC_CK_SHOW_SUBMESH             1013
#define IDC_BTN_ADDALLTOLIST            1014
#define IDC_BTN_REMOVEALLFROMLIST       1015
#define IDC_CK_IMPORT_ALTCOL            1016
#define IDC_BTN_LINK_TWO_LISTS          1018
#define IDC_IMPORTTYPE                  1020
#define IDC_PREVIEW_WINDOW              1021
#define IDC_CK_ENABLE_LIGHTING          1022
#define IDC_ED_STNAME                   1101
#define IDC_BTN_CHANGE_STNAME           1102
#define IDC_ED_SKNAME                   1103
#define IDC_BTN_CHANGE_SKNAME           1104
#define IDC_CK_ALLOW_PARTIAL            1105
#define IDC_GROUP_NAMING                1200
#define IDC_RB_BASIC                    1201
#define IDC_RB_ADVANCED                 1202
#define IDC_ED_REGEX                    1210
#define IDC_ED_OUTPUT_TYPE              1211
#define IDC_ED_OUTPUT_OBJECT            1212
#define IDC_ED_OUTPUT_PART              1213
#define IDC_ED_OUTPUT_LOD               1214
#define IDC_ED_OUTPUT_VARIATION         1215
#define IDC_LABEL_PARSING               1220
#define IDC_LABEL_OUTPUT                1221
#define IDC_LABEL_TYPE                  1222
#define IDC_LABEL_OBJECT                1223
#define IDC_LABEL_PART                  1224
#define IDC_LABEL_LOD                   1225
#define IDC_LABEL_VARIATION             1226
#define IDC_LABEL_TEST                  1230
#define IDC_ED_TEST_TEXT                1231
#define IDC_BTN_PARSE                   1232
#define IDC_CB_BASIC_P0                 1240
#define IDC_CB_BASIC_P1                 1241
#define IDC_CB_BASIC_P2                 1242
#define IDC_CB_BASIC_P3                 1243
#define IDC_CB_BASIC_P4                 1244
#define IDC_CB_BASIC_P5                 1245
#define IDC_CB_BASIC_P6                 1246
#define IDC_CB_BASIC_P7                 1247
#define IDC_CB_BASIC_P8                 1248
#define IDC_CB_BASIC_P9                 1249
#define IDC_CB_BASIC_P10                1250
#define IDC_CB_BASIC_P11                1251
#define IDC_CB_BASIC_P12                1252
#define IDC_CB_BASIC_P13                1253
#define IDC_CB_BASIC_P14                1254
#define IDC_CB_BASIC_P15                1255
#define IDC_CB_BASIC_P16                1256
#define IDC_CB_BASIC_P17                1257
#define IDC_CB_BASIC_P18                1258
#define IDC_CB_BASIC_P19                1259
#define IDC_CB_BASIC_P20                1260
#define IDC_CB_BASIC_P21                1261
#define IDC_CB_BASIC_P22                1262
#define IDC_CB_BASIC_P23                1263
#define IDC_CB_BASIC_P24                1264
#define IDC_CB_BASIC_P25                1265
#define IDC_CB_BASIC_P26                1266
#define IDC_CB_BASIC_P27                1267
#define IDC_CB_BASIC_P28                1268
#define IDC_CB_BASIC_P29                1269
#define IDC_CK_BASIC_P0                 1270
#define IDC_CK_BASIC_P1                 1271
#define IDC_CK_BASIC_P2                 1272
#define IDC_CK_BASIC_P3                 1273
#define IDC_CK_BASIC_P4                 1274
#define IDC_CK_BASIC_P5                 1275
#define IDC_CK_BASIC_P6                 1276
#define IDC_CK_BASIC_P7                 1277
#define IDC_CK_BASIC_P8                 1278
#define IDC_CK_BASIC_P9                 1279
#define IDC_CK_BASIC_P10                1280
#define IDC_CK_BASIC_P11                1281
#define IDC_CK_BASIC_P12                1282
#define IDC_CK_BASIC_P13                1283
#define IDC_CK_BASIC_P14                1284
#define IDC_CK_BASIC_P15                1285
#define IDC_CK_BASIC_P16                1286
#define IDC_CK_BASIC_P17                1287
#define IDC_CK_BASIC_P18                1288
#define IDC_CK_BASIC_P19                1289
#define IDC_CK_BASIC_P20                1290
#define IDC_CK_BASIC_P21                1291
#define IDC_CK_BASIC_P22                1292
#define IDC_CK_BASIC_P23                1293
#define IDC_CK_BASIC_P24                1294
#define IDC_CK_BASIC_P25                1295
#define IDC_CK_BASIC_P26                1296
#define IDC_CK_BASIC_P27                1297
#define IDC_CK_BASIC_P28                1298
#define IDC_CK_BASIC_P29                1299
#define IDC_BTN_REQUIRE_ALL             1300
#define IDD_DLG_ASSETIMPORTER           11001
#define IDD_DLG_ADVANCED_SETTINGS       11002
#define IDD_DLG_NAMING_CONVENTION       11003
#define IDI_OBJECT                      11101
#define IDI_MESH                        11102
#define IDI_LOD_GRAYED                  11103
#define IDI_VARIATION                   11104
#define IDI_LOD                         11105
#define IDI_MESH_GRAYED                 11106
#define IDI_OBJECT_GRAYED               11107
#define IDI_VARIATION_GRAYED            11108
#define IDI_BONE                        11109
#define IDI_BONE_GRAYED                 11110

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        11111
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1301
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
