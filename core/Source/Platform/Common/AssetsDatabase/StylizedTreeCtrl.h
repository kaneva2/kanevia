///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"

class CStylizedTreeCtrl : public CTreeCtrl {
private:
	std::map<HTREEITEM, DWORD> allItemColors;

public:
	CStylizedTreeCtrl(void);
	~CStylizedTreeCtrl(void);

protected:
	void DrawTreeItem(HWND hWnd, HDC hDC, LPRECT lpClipRect, HTREEITEM hItem);

	DECLARE_MESSAGE_MAP()

public:
	void SetItemColor(HTREEITEM hItem, DWORD color);
	DWORD GetItemColor(HTREEITEM hItem);
	void ResetItemColor(HTREEITEM hItem);

protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};
