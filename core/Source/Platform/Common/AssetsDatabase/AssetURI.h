///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

namespace KEP {

struct AssetURI {
	bool isValid; // Is current URI valid?
	std::string protocol; // Protocol used to access this asset
	std::string host; // Host name where the asset is stored.
	std::string path; // Document Path
	std::string document; // Document name (file name)
	std::string node; // Asset node inside a composite document (e.g. Collada mesh inside a Collada document)
	int subscript; // Subscript for asset arrays (e.g. material-based submesh)

	AssetURI(const std::string& aURIString = "") {
		ParseFromURIString(aURIString);
	}

	// Convert to/from a URI string: prot://host/path/to/document#node[subID]
	void ParseFromURIString(const std::string& aURIString);
	std::string ToURIString();

	std::string GetNodeURI(); // Return URI of the asset node: everything in URI string except subID
	std::string GetDocumentURI(); // Return URI of the document where asset is stored: everything except node/subID
	std::string GetAssetName(); // Return node name or document file name if node name is empty.

	// Frequently-used Static functions for accessing URI segments without fully parsing it
	static std::string GetNodeURIFromURIString(const std::string& aURIString); // Parse and return URI of the asset node (see GetNodeURI)
	static std::string GetDocumentURIFromURIString(const std::string& aURIString); // Parse and return URI of the whole document (see GetDocumentURI)

	static std::string GetNodeNameFromURIString(const std::string& aURIString); // Parse and return name of the node only
	static int GetSubscriptFromURIString(const std::string& aURIString); // Parse and return sub ID only

	static std::string GetAssetNameFromURIString(const std::string& aURIString); // Return node name or document file name if node name is empty.
};

} // namespace KEP
