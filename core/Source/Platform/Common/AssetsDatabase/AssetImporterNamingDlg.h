#pragma once
#include "afxwin.h"

#if DEPRECATED

//#include "Common\UIStatic\BasicLayoutMFC.h"

class AssetImporterNamingDlg : public CDialog {
	DECLARE_DYNAMIC(AssetImporterNamingDlg)

public:
	AssetImporterNamingDlg(int namingType, NamingSyntax& namingSyntax, CWnd* pParent = NULL);
	virtual ~AssetImporterNamingDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_NAMING_CONVENTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	int m_namingType;
	NamingSyntax& m_namingSyntax;

private:
	UINT idBasicTokens[NUM_NAMING_TOKENS], idBasicChecks[NUM_NAMING_TOKENS];
	UINT idRegexOutputs[NUM_NAMING_REOUTS];
	CStatic m_lblVariation;
	CEdit m_edVariation;
	CStringW m_sTestString;
	UINT idLastVisibleToken;
	CComboBox m_cbBasicToken0;
	CButton m_ckBasicToken0;
	UINT ctrlsPerRow, ctrlHeight, ctrlWidth;
	BasicLayoutMFC m_layout;

public:
	afx_msg void OnBnClickedBtnParse();
	afx_msg void OnComboBoxTextChanged(UINT id);
	afx_msg void OnCheckBoxChanged(UINT id);
	afx_msg void OnBnClickedBtnRequireAll();
};
#endif
