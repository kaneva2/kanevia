///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AssetsDatabase.h"
#include "AssetNamingSyntax.h"
#include "StylizedTreeCtrl.h"
#include "AssetImporterDlg.h"
#include "AssetImporterAdvancedDlg.h"
#include "UnicodeMFCHelpers.h"

namespace KEP {

IMPLEMENT_DYNAMIC(CAssetImporterDlg, CDialog)

CAssetImporterDlg::CAssetImporterDlg(AssetTree* assets,
	NamingSyntax* namingSyntaxs,
	UINT numOfSyntaxs,
	KEP_ASSET_TYPE importType /*=KEP_UNKNOWN_TYPE*/,
	AssetData::Level importLevel /*=AssetData::LVL_UNKNOWN*/,
	BOOL allowMultiSel /*=TRUE*/, BOOL defaultImportLODs /*=TRUE*/, BOOL defaultImportCollision /*=FALSE*/,
	CWnd* pParent /*=NULL*/) :
		m_allAssets(assets),
		m_nViewBy(AssetData::LVL_UNKNOWN), m_nImportType(importType), m_nImportLevel(importLevel), m_bAllowMultiSel(allowMultiSel), m_bImportLODs(defaultImportLODs), m_bImportAltCol(defaultImportCollision), m_namingSyntaxs(namingSyntaxs), m_numOfSyntaxs(numOfSyntaxs), m_bInitialDisplay(TRUE), m_bRebuildAssetTree(FALSE), CDialog(CAssetImporterDlg::IDD, pParent) {
	ASSERT(numOfSyntaxs <= NUM_NAMING_TYPES);
}

CAssetImporterDlg::~CAssetImporterDlg() {
}

void CAssetImporterDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_AVAILASSET, m_assetTree);
	DDX_Control(pDX, IDC_CB_IMPORTTYPE, m_cbImportType);
	DDX_Control(pDX, IDC_CB_VIEWBY, m_cbViewBy);
	DDX_Control(pDX, IDC_CK_SHOW_SUBMESH, m_ckShowSubMeshes);
	DDX_Control(pDX, IDC_LIST_IMPORT, m_importList);
	DDX_Control(pDX, IDC_BTN_ADDTOLIST, m_btnAddToList);
	DDX_Control(pDX, IDC_BTN_REMOVEFROMLIST, m_btnRemoveFromList);
	DDX_Control(pDX, IDC_BTN_ADDALLTOLIST, m_btnAddAllToList);
	DDX_Control(pDX, IDC_BTN_REMOVEALLFROMLIST, m_btnRemoveAllFromList);
	DDX_Control(pDX, IDOK, m_btnImport);
	DDX_Control(pDX, IDC_BTN_IMPORTALL, m_btnImportAll);
	DDX_Control(pDX, IDC_IMPORTTYPE, m_lblImportType);
	DDX_Control(pDX, IDC_CK_IMPORT_LODS, m_ckImportLODs);
	DDX_Control(pDX, IDC_CK_IMPORT_ALTCOL, m_ckImportAltCol);
}

BEGIN_MESSAGE_MAP(CAssetImporterDlg, CDialog)
ON_BN_CLICKED(IDC_BTN_ADDTOLIST, &CAssetImporterDlg::OnBnClickedBtnAddtolist)
ON_BN_CLICKED(IDC_BTN_REMOVEFROMLIST, &CAssetImporterDlg::OnBnClickedBtnRemovefromlist)
ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_AVAILASSET, &CAssetImporterDlg::OnTvnSelchangedTreeAvailasset)
ON_CBN_SELCHANGE(IDC_CB_VIEWBY, &CAssetImporterDlg::OnCbnSelchangeCbViewby)
ON_BN_CLICKED(IDC_CK_SHOW_SUBMESH, &CAssetImporterDlg::OnBnClickedCkShowSubmesh)
ON_BN_CLICKED(IDOK, &CAssetImporterDlg::OnBnClickedBtnImport)
ON_BN_CLICKED(IDC_BTN_IMPORTALL, &CAssetImporterDlg::OnBnClickedBtnImportall)
ON_WM_CLOSE()
ON_LBN_SELCHANGE(IDC_LIST_IMPORT, &CAssetImporterDlg::OnLbnSelchangeListImport)
ON_CBN_SELCHANGE(IDC_CB_IMPORTTYPE, &CAssetImporterDlg::OnCbnSelchangeCbImporttype)
ON_BN_CLICKED(IDC_BTN_ADDALLTOLIST, &CAssetImporterDlg::OnBnClickedBtnAddAllToList)
ON_BN_CLICKED(IDC_BTN_REMOVEALLFROMLIST, &CAssetImporterDlg::OnBnClickedBtnRemoveAllFromList)
ON_NOTIFY(NM_KILLFOCUS, IDC_TREE_AVAILASSET, &CAssetImporterDlg::OnNMKillfocusTreeAvailasset)
ON_NOTIFY(NM_SETFOCUS, IDC_TREE_AVAILASSET, &CAssetImporterDlg::OnNMSetfocusTreeAvailasset)
ON_NOTIFY(NM_CLICK, IDC_LIST_IMPORT, &CAssetImporterDlg::OnNMClickListImport)
ON_BN_CLICKED(IDC_BTN_ADVANCED, &CAssetImporterDlg::OnBnClickedBtnAdvanced)
ON_MESSAGE(WM_USER + 1, &CAssetImporterDlg::OnInitialDisplay)
ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

HICON LoadIconFromResource(int resourceID) {
	HINSTANCE hInst = GetModuleHandle(NULL);
	HRSRC hRsrc = FindResource(hInst, MAKEINTRESOURCE(resourceID), L"IMAGE");
	if (hRsrc == NULL)
		return NULL;

	HGLOBAL hGlobal = LoadResource(hInst, hRsrc);
	if (hGlobal == NULL)
		return NULL;

	LPVOID pIcon = LockResource(hGlobal);
	if (pIcon == NULL)
		return NULL;

	DWORD size = SizeofResource(hInst, hRsrc);
	if (size == 0)
		return NULL;

	HICON hIcon = CreateIconFromResource((PBYTE)pIcon + 0x16, size, TRUE, 0x030000);
#ifdef _DEBUG
	DWORD err = GetLastError();
	if (hIcon == NULL) {
		char msg[100];
		sprintf_s(msg, 100, "Load icon failed: %d\n", err);
		TRACE(msg);
	}
#endif

	return hIcon;
}

BOOL CAssetImporterDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	m_bRebuildAssetTree = FALSE; // Reset this flag everytime doModel is called

	int itemID;
	if (m_nImportType == KEP_STATIC_MESH) {
		itemID = m_cbImportType.AddString(L"Static Sub-mesh (single mat)");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_SUBMESH);
		itemID = m_cbImportType.AddString(L"Static LOD");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_LOD);
		itemID = m_cbImportType.AddString(L"Static Object Part");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_PART);
		itemID = m_cbImportType.AddString(L"Static Object");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_OBJECT);
		itemID = m_cbImportType.AddString(L"Mesh Locations Only");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_LOD);
		itemID = m_cbImportType.AddString(L"AI Tree");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_LOD);
	} else if (m_nImportType == KEP_SKA_SKELETAL_OBJECT || m_nImportType == KEP_SKA_SKINNED_MESH || m_nImportType == KEP_SKA_RIGID_MESH) {
		itemID = m_cbImportType.AddString(L"Deformable LOD");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_LOD);
		itemID = m_cbImportType.AddString(L"Deformable Configuration");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_VARIATION);
		itemID = m_cbImportType.AddString(L"Deformable Section");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_SECTION);
		itemID = m_cbImportType.AddString(L"Skeletal Object");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_OBJECT);
		itemID = m_cbImportType.AddString(L"Bone");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_FLAT);
	} else if (m_nImportType == KEP_VERTEX_ANIMATION) {
		itemID = m_cbImportType.AddString(L"Static Object with Morph Targets");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_FLAT);

		m_ckImportLODs.EnableWindow(FALSE);
		m_ckImportAltCol.EnableWindow(FALSE);
	} else if (m_nImportType == KEP_SKA_ANIMATION) {
		itemID = m_cbImportType.AddString(L"Skeletal Animation");
		m_cbImportType.SetItemData(itemID, AssetData::LVL_FLAT);

		m_ckImportLODs.EnableWindow(FALSE);
		m_ckImportAltCol.EnableWindow(FALSE);
	}

	itemID = m_cbViewBy.AddString(L"View Assets By Object Groups");
	m_cbViewBy.SetItemData(itemID, AssetData::LVL_GROUP);
	itemID = m_cbViewBy.AddString(L"View Assets By Objects");
	m_cbViewBy.SetItemData(itemID, AssetData::LVL_OBJECT);
	itemID = m_cbViewBy.AddString(L"View Assets By Meshes (grouped LODs)");
	m_cbViewBy.SetItemData(itemID, AssetData::LVL_SECTION);
	itemID = m_cbViewBy.AddString(L"View Individual Meshes (flat list)");
	m_cbViewBy.SetItemData(itemID, AssetData::LVL_LOD);

	m_hImageList = ImageList_Create(16, 16, ILC_COLOR4, 4, 2);
	idGroup = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_OBJECT));
	idGroup_Gray = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_OBJECT_GRAYED));
	idObject = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_OBJECT));
	idObject_Gray = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_OBJECT_GRAYED));
	idMesh = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_MESH));
	idMesh_Gray = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_MESH_GRAYED));
	idVariation = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_VARIATION));
	idVariation_Gray = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_VARIATION_GRAYED));
	idLOD = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_LOD));
	idLOD_Gray = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_LOD_GRAYED));
	idBone = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_BONE));
	idBone_Gray = ImageList_AddIcon(m_hImageList, LoadIconFromResource(IDI_BONE_GRAYED));

	TreeView_SetImageList(m_assetTree.GetSafeHwnd(), m_hImageList, TVSIL_NORMAL);

	m_importList.InsertColumn(0, L"Import Item", 0, 200);

	// DRF - Decided that this UNICODE warning is not relevant
	m_importList.SetView(LV_VIEW_DETAILS);

	SetViewLevel(AssetData::LVL_GROUP); // Set view type before import type to ensure selected import type is displayed in tree
	SetImportLevel(m_nImportLevel);
	SetAllowMultiSel(m_bAllowMultiSel);

	// Load asset definition into tree view
	UpdateAssetTreeView();

	ExpandTreeToLevel(AssetData::LVL_SECTION);

	m_ckImportLODs.SetCheck(m_bImportLODs);
	m_ckImportAltCol.SetCheck(m_bImportAltCol);

	return TRUE; // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAssetImporterDlg::OnBnClickedBtnAddtolist() {
	HTREEITEM hItem = m_assetTree.GetSelectedItem();
	if (hItem == NULL) {
		//Should not reach this point as it's not allowed by UI
		//@@Todo: notify user to select an item in case something strange happened
		return;
	}

	AddToImportList(hItem);

	// Send focus to import list
	m_importList.SetFocus();
}

void CAssetImporterDlg::OnBnClickedBtnRemovefromlist() {
	int count = m_importList.GetSelectedCount();
	if (count == 0) {
		AfxMessageBox(L"Please select an item from import list");
		return;
	}

	int selCount = m_importList.GetSelectedCount();
	int* allSels = new int[selCount];

	POSITION pos = m_importList.GetFirstSelectedItemPosition();
	for (int i = 0; i < selCount && pos != NULL; i++)
		allSels[i] = m_importList.GetNextSelectedItem(pos);

	for (int i = selCount - 1; i >= 0; i--)
		RemoveFromImportList(allSels[i]);

	delete allSels;
}

AssetData::Level CAssetImporterDlg::CalcViewLevel() {
	if (m_cbViewBy.GetCurSel() == CB_ERR)
		return AssetData::LVL_SUBMESH;
	return (AssetData::Level)m_cbViewBy.GetItemData(m_cbViewBy.GetCurSel());
}

AssetData::Level CAssetImporterDlg::CalcImportLevel() {
	if (m_cbImportType.GetCurSel() == CB_ERR)
		return AssetData::LVL_UNKNOWN;
	return (AssetData::Level)m_cbImportType.GetItemData(m_cbImportType.GetCurSel());
}

int CAssetImporterDlg::CalcViewLevelSel() {
	for (int i = 0; i < m_cbViewBy.GetCount(); i++) {
		AssetData::Level level = (AssetData::Level)m_cbViewBy.GetItemData(i);
		if (level == m_nViewBy)
			return i;
	}

	return CB_ERR;
}

int CAssetImporterDlg::CalcImportLevelSel() {
	for (int i = 0; i < m_cbImportType.GetCount(); i++) {
		AssetData::Level level = (AssetData::Level)m_cbImportType.GetItemData(i);
		if (level == m_nImportLevel)
			return i;
	}

	return CB_ERR;
}

BOOL CAssetImporterDlg::SetImportLevel(AssetData::Level level, BOOL bFromUI /* = FALSE*/) {
	if (bFromUI && level == m_nImportLevel)
		return FALSE;

	m_nImportLevel = level;
	if (!bFromUI) {
		m_cbImportType.SetCurSel(CalcImportLevelSel());
		CStringW sImportType;
		m_cbImportType.GetWindowText(sImportType);
		m_lblImportType.SetWindowText(sImportType);
	}

	// If current import type is not hidden in tree, switch to appropriate view type
	if (level < m_nViewBy)
		SetViewLevel(level);

	// Default show/hide submaterial
	SetShowSubmeshes(CalcImportLevel() == AssetData::LVL_SUBMESH);
	return TRUE;
}

BOOL CAssetImporterDlg::SetViewLevel(AssetData::Level level, BOOL bFromUI /* = FALSE*/) {
	if (bFromUI && level == m_nViewBy)
		return FALSE;

	m_nViewBy = level;
	if (!bFromUI)
		m_cbViewBy.SetCurSel(CalcViewLevelSel());
	return TRUE;
}

BOOL CAssetImporterDlg::SetShowSubmeshes(BOOL bShow, BOOL bFromUI /* = FALSE*/) {
	if (bShow == m_bShowSubmeshes)
		return FALSE;

	m_bShowSubmeshes = bShow;
	if (!bFromUI)
		m_ckShowSubMeshes.SetCheck(m_bShowSubmeshes);
	return TRUE;
}

BOOL CAssetImporterDlg::SetAllowMultiSel(BOOL bAllow, BOOL bFromUI /*=FALSE*/) {
	if (bFromUI && bAllow == m_bAllowMultiSel)
		return FALSE;

	m_bAllowMultiSel = bAllow;

	CStringW sImportType;
	m_lblImportType.GetWindowText(sImportType);
	if (sImportType.Right(14) == L", Multi-Select" && !m_bAllowMultiSel)
		sImportType = sImportType.Left(sImportType.GetLength() - 14);
	else if (sImportType.Right(14) != L", Multi-Select" && m_bAllowMultiSel)
		sImportType = sImportType + L", Multi-Select";
	m_lblImportType.SetWindowText(sImportType);

	return TRUE;
}

//
BOOL CAssetImporterDlg::SetImportLODs(BOOL bImport, BOOL bFromUI /*=FALSE*/) {
	if (bFromUI && bImport == m_bImportLODs)
		return FALSE;

	m_bImportLODs = bImport;
	return TRUE;
}

//
BOOL CAssetImporterDlg::SetImportCollision(BOOL bImport, BOOL bFromUI /*=FALSE*/) {
	if (bFromUI && bImport == m_bImportAltCol)
		return FALSE;

	m_bImportAltCol = bImport;
	return TRUE;
}

CStringW FormatTreeItemKey(const AssetData* pAsset) {
	jsAssert(pAsset != NULL);

	const CStringA& key = pAsset->GetKey().c_str();

	CStringA prefix;
	switch (pAsset->GetLevel()) {
		case AssetData::LVL_OBJECT:
			prefix = "O";
			break;
		case AssetData::LVL_SECTION:
			prefix = "S";
			break;
		case AssetData::LVL_VARIATION:
			prefix = "V";
			break;
		case AssetData::LVL_LOD:
			prefix = "M";
			break;
		case AssetData::LVL_SUBMESH:
			prefix = "_";
			break;
		case AssetData::LVL_FLAT:
			switch (pAsset->GetType()) {
				case KEP_SKA_SKELETAL_OBJECT:
					prefix = "B"; // Bone
					break;
				case KEP_SKA_ANIMATION:
					prefix = "A"; // Animation
					break;
				case KEP_VERTEX_ANIMATION:
					prefix = "X"; // Vertex Animation
					break;
				default:
					prefix = "?";
					break;
			}
			break;
		default:
			prefix = "?";
			break;
	}

	return Utf8ToUtf16(prefix + ":" + key).c_str();
}

CStringW FormatTreeItemText(const AssetData* pAsset, int numChildren, BOOL bShowSubMeshes) {
	jsAssert(pAsset != NULL);

	CStringA text = pAsset->GetName().c_str(); //FormatTreeItemKey(node);

	switch (pAsset->GetLevel()) {
		case AssetData::LVL_OBJECT: // Object
			text += " (object)";
			break;
		case AssetData::LVL_SECTION: // Section
			if (numChildren > 1)
				text.AppendFormat(" (%d variations)", numChildren);
			break;
		case AssetData::LVL_VARIATION: // Variation
			if (text == "")
				text = "{STD}";
			if (numChildren > 1)
				text.AppendFormat(" (%d LODs)", numChildren);
			else
				text.AppendFormat(" (%d LOD)", numChildren);
			break;
		case AssetData::LVL_LOD:
			if (!bShowSubMeshes) {
				int submeshCount = pAsset->GetNumSubmeshes();
				if (submeshCount > 1)
					//text.Format("#%d: [%s] (%d materials)", pAsset->GetLODLevel(), URINode.c_str(), submeshCount);
					text.AppendFormat(" (%d materials)", submeshCount);
				else
					//text.Format("#%d: [%s] (%d material)", pAsset->GetLODLevel(), URINode.c_str(), submeshCount);
					text.AppendFormat(" (%d material)", submeshCount);
			}
			break;
	}

	return Utf8ToUtf16(text).c_str();
}

AssetTree* GetParent(AssetTree* curr, AssetData::Level topLevel) {
	AssetTree* parent = curr->parent();
	return parent;
}

BOOL SameParent(AssetTree* node1, AssetTree* node2, AssetData::Level topLevel) {
	return GetParent(node1, topLevel) == GetParent(node2, topLevel);
}

BOOL CAssetImporterDlg::GetImageIndexByAssetType(KEP_ASSET_TYPE type, AssetData::Level level, int& imageID, int& imageGrayID) {
	switch (level) {
		case AssetData::LVL_GROUP:
			imageID = idGroup;
			imageGrayID = idGroup_Gray;
			break;
		case AssetData::LVL_OBJECT:
			imageID = idObject;
			imageGrayID = idObject_Gray;
			break;
		case AssetData::LVL_SECTION:
			imageID = idMesh;
			imageGrayID = idMesh_Gray;
			break;
		case AssetData::LVL_VARIATION:
			imageID = idVariation;
			imageGrayID = idVariation_Gray;
			break;
		case AssetData::LVL_LOD:
			imageID = idLOD;
			imageGrayID = idLOD_Gray;
			break;
		case AssetData::LVL_FLAT:
			switch (type) {
				case KEP_SKA_SKELETAL_OBJECT:
					imageID = idBone;
					imageGrayID = idBone_Gray;
					break;
				case KEP_SKA_ANIMATION:
					imageID = idObject;
					imageGrayID = idObject_Gray; //@@Todo: Create a new icon
			}
			break;
		default:
			imageID = imageGrayID = 99;
			return FALSE;
	}

	return TRUE;
}

HTREEITEM CAssetImporterDlg::InsertTreeItem(HTREEITEM hParent, const wchar_t* itemText, DWORD itemData, int imageID, BOOL bGrayed) {
	HTREEITEM hItem = m_assetTree.InsertItem(itemText, hParent);
	if (hItem) {
		m_assetTree.SetItemData(hItem, itemData);
		m_assetTree.SetItemImage(hItem, imageID, imageID);
		if (bGrayed)
			m_assetTree.SetItemColor(hItem, 0xA0A0A0);
		else
			m_assetTree.ResetItemColor(hItem);
	}

	return hItem;
}

//! Fill in tree control with available assets
void CAssetImporterDlg::UpdateAssetTreeView() {
	m_ckShowSubMeshes.EnableWindow(m_nImportLevel < AssetData::LVL_SUBMESH);

	SaveTreeStates();

	m_treeItemKeyMap.clear();
	m_assetTree.DeleteAllItems();

	// Return if nothing found
	if (!m_allAssets)
		return;

	BOOL bMeshFound = FALSE;
	for (AssetTree::pre_order_node_iterator_type it = m_allAssets->pre_order_node_begin(); it != m_allAssets->pre_order_node_end(); it++) {
		HTREEITEM hParent = NULL;

		// Current node (AssetData in curr->get())
		AssetTree *curr = &(*it), *parent;
		int numOfChildren = curr->size();
		AssetData* currAsset = curr->get();
		CStringW currKey = FormatTreeItemKey(currAsset);
		BOOL bShowSubMeshes = m_ckShowSubMeshes.GetCheck();
		CStringW currItemText = FormatTreeItemText(currAsset, numOfChildren, bShowSubMeshes);

		if (CalcViewLevel() > currAsset->GetLevel()) {
			// Skip if current node is above the highest-level requested
			continue;
		}

		parent = ::KEP::GetParent(curr, CalcViewLevel());

		if (parent) // Get HTREEITEM for parent node
			hParent = m_treeItemKeyMap[FormatTreeItemKey(parent->get())];

		int imageID, imageGrayID;
		GetImageIndexByAssetType(currAsset->GetType(), currAsset->GetLevel(), imageID, imageGrayID);

		// Check if current item should be grayed (beyond lowest allowed level)
		BOOL bGrayed = FALSE;
		AssetData::Compatibility compatFlag = AssetData::CheckAssetCompatibility(currAsset->GetLevel(), currAsset->GetType(), CalcImportLevel(), m_nImportType);
		if (compatFlag == AssetData::AC_INCOMPAT && compatFlag == AssetData::AC_LOWER) {
			bGrayed = TRUE;
			imageID = imageGrayID;
		}

		if (currAsset->GetLevel() == AssetData::LVL_LOD)
			bMeshFound = TRUE; // At least one mesh found

		if (bShowSubMeshes && currAsset->GetLevel() == AssetData::LVL_LOD) {
			CStringW format = L"%s : %d of %d";
			//if (currAsset->GetNumSubmeshes()==1) format = "%s (one-piece)";

			for (int i = 0; i < currAsset->GetNumSubmeshes(); i++) {
				CStringW subItemText;
				subItemText.Format(format, currItemText, i + 1, currAsset->GetNumSubmeshes());
				HTREEITEM hCurr = InsertTreeItem(hParent, subItemText, (DWORD)currAsset, imageID, bGrayed);

				// Update key to HTREEITEM mapping for future retrieval of parent tree items
				if (hCurr)
					m_treeItemKeyMap.insert(std::pair<CStringW, HTREEITEM>(currKey, hCurr));
			}
		} else {
			// Insert tree item
			HTREEITEM hCurr = InsertTreeItem(hParent, currItemText, (DWORD)currAsset, imageID, bGrayed);

			// Update key to HTREEITEM mapping for future retrieval of parent tree items
			if (hCurr)
				m_treeItemKeyMap.insert(std::pair<CStringW, HTREEITEM>(currKey, hCurr));
		}
	}

	// Expand tree item for convenience
	if (m_treeItemStates.empty()) {
		// Default Expansion
		HTREEITEM firstLevelNode = m_assetTree.GetRootItem();
		while (firstLevelNode) {
			m_assetTree.Expand(firstLevelNode, TVE_EXPAND);
			firstLevelNode = m_assetTree.GetNextSiblingItem(firstLevelNode);
		}
	} else
		RestoreTreeStates(); // Previously saved

	RestoreAssetSelections();

	m_btnImportAll.EnableWindow(m_bAllowMultiSel && bMeshFound);
}

void CAssetImporterDlg::ClearTreeKeys() {
	m_treeItemKeyMap.clear();
}

void CAssetImporterDlg::OnTvnSelchangedTreeAvailasset(NMHDR* pNMHDR, LRESULT* pResult) {
	//LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	// Enable button if an non-grayed item is selected
	m_btnAddToList.EnableWindow(
		m_assetTree.GetSelectedItem() != NULL && m_assetTree.GetItemColor(m_assetTree.GetSelectedItem()) == -1);

	*pResult = 0;
}

void CAssetImporterDlg::OnLbnSelchangeListImport() {
	m_btnRemoveFromList.EnableWindow(m_importList.GetSelectedCount() > 0);
}

void CAssetImporterDlg::OnCbnSelchangeCbViewby() {
	if (SetViewLevel(CalcViewLevel(), TRUE))
		UpdateAssetTreeView();
}

void CAssetImporterDlg::OnBnClickedCkShowSubmesh() {
	if (SetShowSubmeshes(m_ckShowSubMeshes.GetCheck()), TRUE)
		UpdateAssetTreeView();
}

//! Save expansion and selection state for all tree items for future restoration
void CAssetImporterDlg::SaveTreeStates(HTREEITEM item /*= NULL*/) {
	std::map<CStringW, UINT>& states = m_treeItemStates;
	const CTreeCtrl& tree = m_assetTree;

	// Handle current node
	if (item == NULL)
		item = tree.GetRootItem();
	else {
		AssetData* pAsset = reinterpret_cast<AssetData*>(tree.GetItemData(item));
		if (pAsset) {
			UINT stateMask = TVIS_EXPANDED;
			UINT itemState = tree.GetItemState(item, stateMask);

			// Insert or update state
			std::map<CStringW, UINT>::iterator it = states.find(FormatTreeItemKey(pAsset));
			if (it != states.end()) {
				// To update: merge with existing state, remove old item before insertion
				UINT oldState = it->second;
				itemState |= oldState & ~stateMask;
				states.erase(it);
			}
			states.insert(std::pair<CStringW, UINT>(FormatTreeItemKey(pAsset), itemState));
		}
		item = tree.GetChildItem(item);
	}

	// Handle all children
	while (item) {
		SaveTreeStates(item);
		item = tree.GetNextSiblingItem(item);
	}
}

//! Restore expansion and selection state for all tree items
void CAssetImporterDlg::RestoreTreeStates(HTREEITEM item /*= NULL*/) {
	const std::map<CStringW, UINT>& states = m_treeItemStates;
	CTreeCtrl& tree = m_assetTree;

	// Handle current node
	if (item == NULL)
		item = tree.GetRootItem();
	else {
		AssetData* pAsset = reinterpret_cast<AssetData*>(tree.GetItemData(item));
		if (pAsset) {
			std::map<CStringW, UINT>::const_iterator it = states.find(FormatTreeItemKey(pAsset));
			if (it != states.end()) {
				if (it->second & TVIS_EXPANDED)
					tree.Expand(item, TVE_EXPAND);
				//if (it->second & TVIS_SELECTED) tree.Select(item, TVGN_CARET | TVGN_FIRSTVISIBLE);
				if (it->second & TVIS_BOLD)
					tree.SetItemState(item, TVIS_BOLD, TVIS_BOLD);
			}
		}
		item = tree.GetChildItem(item);
	}

	// Handle all children
	while (item) {
		RestoreTreeStates(item);
		item = tree.GetNextSiblingItem(item);
	}
}

void CAssetImporterDlg::ClearTreeStates() {
	m_treeItemStates.clear();
}

void CAssetImporterDlg::AddToImportList(HTREEITEM hItem, BOOL bIsDescendent /*= FALSE*/) {
	ASSERT(hItem != NULL);

	AssetData* pAsset = reinterpret_cast<AssetData*>(m_assetTree.GetItemData(hItem));
	ASSERT(pAsset != NULL);

	if (pAsset->GetLevel() == AssetData::LVL_LOD && bIsDescendent) {
		// Skip LODs or Alt.Col if not requested by UI
		// NOTE: Does not apply to explicitly selected item (bIsDescendent==TRUE for explicitly selected items, FALSE for descendences)
		int lodNo = pAsset->GetLODLevel();
		// Skip LOD meshes if "auto import LOD meshes" not checked
		if (!m_ckImportLODs.GetCheck() && lodNo != AssetData::LOD0 && lodNo != AssetData::LOD_COL)
			return;
		// Skip collision meshes if "auto import collision meshes" not checked
		if (!m_ckImportAltCol.GetCheck() && lodNo == AssetData::LOD_COL)
			return;
	}

	AssetData::Compatibility compatFlag = AssetData::CheckAssetCompatibility(pAsset->GetLevel(), pAsset->GetType(), CalcImportLevel(), m_nImportType);

	if (compatFlag == AssetData::AC_EQUAL) { // At import level
		// Check if current item need to be added to import list
		if (!IsAssetInImportList(pAsset)) {
			if (!m_bAllowMultiSel && m_importList.GetItemCount() > 0) {
				ShowMessage(ERROR_MSG_TOO_MANY_SELECTION, L"You may only select one item to import under single-selection mode.");
				return;
			}

			// Add to list only if it's not already added and the item type matches import type (do not add lower level items)
			// Add item into import list
			int itemID = m_importList.InsertItem(m_importList.GetItemCount(), Utf8ToUtf16(pAsset->GetKey()).c_str());
			m_importList.SetItemData(itemID, (DWORD)pAsset);

			m_btnImport.EnableWindow(m_importList.GetItemCount() > 0);
		}
	}

	if (compatFlag == AssetData::AC_EQUAL || compatFlag == AssetData::AC_LOWER) { // At or below import level
		// Flag asset as selected
		pAsset->SetFlag(AssetData::STA_SELECTED);

		// Set tree item to bold font
		CStringW treeItemKey = FormatTreeItemKey(pAsset);
		SetTreeItemState(treeItemKey, TVIS_BOLD);

		// Update UI
		m_assetTree.SetItemState(hItem, TVIS_BOLD, TVIS_BOLD);
	}

	// Process child nodes
	HTREEITEM hChild = m_assetTree.GetChildItem(hItem);
	while (hChild) {
		AddToImportList(hChild, TRUE);
		hChild = m_assetTree.GetNextSiblingItem(hChild);
	}
}

void CAssetImporterDlg::RemoveFromImportList(HTREEITEM hItem) {
	AssetData* pAsset = reinterpret_cast<AssetData*>(m_assetTree.GetItemData(hItem));
	ASSERT(pAsset != NULL);

	// Flag asset as unselected
	pAsset->ClearFlag(AssetData::STA_SELECTED);

	// Update tree item states map
	CStringW treeItemKey = FormatTreeItemKey(pAsset);
	UnsetTreeItemState(treeItemKey, TVIS_BOLD);

	// Un-bold tree item in UI
	m_assetTree.SetItemState(hItem, 0, TVIS_BOLD);

	// Handle child nodes
	for (HTREEITEM hChild = m_assetTree.GetChildItem(hItem); hChild != NULL; hChild = m_assetTree.GetNextSiblingItem(hChild))
		RemoveFromImportList(hChild);
}

void CAssetImporterDlg::RemoveFromImportList(int itemID) {
	// Get key from list
	AssetData* pAsset = reinterpret_cast<AssetData*>(m_importList.GetItemData(itemID));
	ASSERT(pAsset != NULL && pAsset != (AssetData*)LB_ERR);

	// Remove item from import list
	m_importList.DeleteItem(itemID);

	// Notify asset tree about this removal
	CStringW treeItemKey = FormatTreeItemKey(pAsset);
	if (m_treeItemKeyMap.find(treeItemKey) != m_treeItemKeyMap.end()) {
		HTREEITEM hItem = m_treeItemKeyMap[treeItemKey];
		ASSERT(hItem != NULL);
		RemoveFromImportList(hItem);

		// Update button state
		m_btnImport.EnableWindow(m_importList.GetItemCount() > 0);
	}
}

//! Add all asset to import list
void CAssetImporterDlg::AddAllToImportList() {
	// All all root items to importer list (will be automatically parsed)
	HTREEITEM firstLevelNode = m_assetTree.GetRootItem();
	while (firstLevelNode) {
		AddToImportList(firstLevelNode);
		firstLevelNode = m_assetTree.GetNextSiblingItem(firstLevelNode);
	}
}

//! Remove all asset from import list
void CAssetImporterDlg::RemoveAllFromImportList() {
	int importCount = m_importList.GetItemCount();
	for (int i = importCount - 1; i >= 0; i--)
		RemoveFromImportList(i);
}

//! Check if an asset is already in import list
BOOL CAssetImporterDlg::IsAssetInImportList(const AssetData* pAsset) {
	int itemID = -1;
	while ((itemID = ListView_GetNextItem(m_importList.GetSafeHwnd(), itemID, LVNI_ALL)) != -1) {
		AssetData* tmp = reinterpret_cast<AssetData*>(m_importList.GetItemData(itemID));
		if (!tmp) {
			//@@Todo: log internal error
			continue; // skip
		}

		if (*tmp == *pAsset)
			return TRUE;
	}

	return FALSE;
}

void CAssetImporterDlg::OnBnClickedBtnImport() {
	// Traverse asset tree and mark nodes for import if any of their children are marked for import
	for (AssetTree::post_order_iterator it = m_allAssets->post_order_begin(); it != m_allAssets->post_order_end(); ++it) {
		AssetData& asset = *it;
		AssetTree* node = it.node();
		AssetTree* parentNode = node->parent();

		if (parentNode != NULL && asset.CheckFlag(AssetData::STA_SELECTED))
			node->parent()->get()->SetFlag(AssetData::STA_SELECTED);
	}

	// Caller will do the import
	OnOK();
}

void CAssetImporterDlg::OnBnClickedBtnImportall() {
	if (::MessageBoxW(GetSafeHwnd(), L"This will import all assets from current file, continue?", L"Confirm", MB_YESNO) == IDYES) {
		AddAllToImportList();
		OnBnClickedBtnImport();
	}
}

void CAssetImporterDlg::OnClose() {
	ClearTreeStates();
	ClearTreeKeys();

	if (m_hImageList)
		ImageList_Destroy(m_hImageList);
	m_hImageList = NULL;

	CDialog::OnClose();
}

void ExpandNode(HWND hTreeCtrl, UINT expandFlag, HTREEITEM node, BOOL bThorough = FALSE, BOOL bMakeVisible = TRUE) {
	// Expand/collapse current node
	TreeView_Expand(hTreeCtrl, node, expandFlag);

	if (bMakeVisible) {
		// Expand parents all the way up to root
		HTREEITEM parent = TreeView_GetParent(hTreeCtrl, node);
		if (parent)
			ExpandNode(hTreeCtrl, TVE_EXPAND, parent);
	}

	if (bThorough) {
		// Expand/collapse children recursively
		HTREEITEM child = TreeView_GetChild(hTreeCtrl, node);
		while (child) {
			ExpandNode(hTreeCtrl, expandFlag, child, TRUE, FALSE);
			child = TreeView_GetNextSibling(hTreeCtrl, child);
		}
	}
}

void ExpandTreeByLevel(HWND hTreeCtrl, AssetData::Level level, UINT expandFlag, HTREEITEM rootNode, BOOL bThorough = FALSE) {
	if (rootNode == NULL) {
		HTREEITEM hItem = TreeView_GetRoot(hTreeCtrl);
		while (hItem) {
			ExpandTreeByLevel(hTreeCtrl, level, expandFlag, hItem, bThorough);
			hItem = TreeView_GetNextSibling(hTreeCtrl, hItem);
		}

		return;
	}

	// Get Asset Node
	TVITEM item;
	ZeroMemory(&item, sizeof(item));
	item.mask = TVIF_PARAM;
	item.hItem = rootNode;
	TreeView_GetItem(hTreeCtrl, &item);
	if (item.lParam == NULL)
		return;
	AssetData* pAsset = reinterpret_cast<AssetData*>(item.lParam);

	// Skip everything at levels deeper then requested
	if (pAsset->GetLevel() > level)
		return;

	// If expanding, expand everything before current level
	if (expandFlag == TVE_EXPAND && pAsset->GetLevel() < level)
		ExpandNode(hTreeCtrl, TVE_EXPAND, rootNode, FALSE, FALSE);

	// If collapsing, collapse everything at current level (recursion determined by bThorough flag)
	if (expandFlag == TVE_COLLAPSE && pAsset->GetLevel() == level)
		ExpandNode(hTreeCtrl, TVE_COLLAPSE, rootNode, bThorough, FALSE);

	// Handle all children
	HTREEITEM hChild = TreeView_GetChild(hTreeCtrl, rootNode);
	while (hChild) {
		ExpandTreeByLevel(hTreeCtrl, level, expandFlag, hChild, bThorough);
		hChild = TreeView_GetNextSibling(hTreeCtrl, hChild);
	}
}

void CAssetImporterDlg::CollapseTreeToLevel(AssetData::Level level) {
	ExpandTreeByLevel(m_assetTree.GetSafeHwnd(), level, TVE_COLLAPSE, NULL);
}

void CAssetImporterDlg::ExpandTreeToLevel(AssetData::Level level) {
	ExpandTreeByLevel(m_assetTree.GetSafeHwnd(), level, TVE_EXPAND, NULL);
}

void CAssetImporterDlg::CollapseAndExpandToLevel(AssetData::Level level) {
	CollapseTreeToLevel(level);
	ExpandTreeToLevel(level);
}

void CAssetImporterDlg::CollapseNode(HTREEITEM hItem, BOOL bCollapseEverythingBeyond) {
	::KEP::ExpandNode(GetSafeHwnd(), TVE_COLLAPSE, hItem, bCollapseEverythingBeyond);
}

void CAssetImporterDlg::ExpandNode(HTREEITEM hItem, BOOL bExpandEverythingBeyond) {
	::KEP::ExpandNode(GetSafeHwnd(), TVE_EXPAND, hItem, bExpandEverythingBeyond);
}

void CAssetImporterDlg::CollapseAll() {
	// Collapse all root nodes
	HTREEITEM hItem = m_assetTree.GetRootItem();
	while (hItem) {
		CollapseNode(hItem, TRUE);
		hItem = m_assetTree.GetNextSiblingItem(hItem);
	}
}

void CAssetImporterDlg::ExpandAll() {
	// Expand all root nodes
	HTREEITEM hItem = m_assetTree.GetRootItem();
	while (hItem) {
		ExpandNode(hItem, TRUE);
		hItem = m_assetTree.GetNextSiblingItem(hItem);
	}
}

void CAssetImporterDlg::OnCbnSelchangeCbImporttype() {
	AssetData::Level type = CalcImportLevel();
	if (SetImportLevel(type, TRUE)) {
		UpdateAssetTreeView();
		CollapseTreeToLevel(type);
	}
}

void CAssetImporterDlg::OnBnClickedBtnAddAllToList() {
	AddAllToImportList();
}

void CAssetImporterDlg::OnBnClickedBtnRemoveAllFromList() {
	RemoveAllFromImportList();
}

void CAssetImporterDlg::OnNMKillfocusTreeAvailasset(NMHDR* pNMHDR, LRESULT* pResult) {
	// Disable "Add to list" button
	if (GetFocus()->GetSafeHwnd() != m_btnAddToList.GetSafeHwnd())
		m_btnAddToList.EnableWindow(FALSE);

	*pResult = 0;
}

void CAssetImporterDlg::OnNMSetfocusTreeAvailasset(NMHDR* pNMHDR, LRESULT* pResult) {
	// Enable button if an non-grayed item is selected
	m_btnAddToList.EnableWindow(
		m_assetTree.GetSelectedItem() != NULL && m_assetTree.GetItemColor(m_assetTree.GetSelectedItem()) == -1);

	*pResult = 0;
}

void CAssetImporterDlg::OnNMClickListImport(NMHDR* pNMHDR, LRESULT* pResult) {
	// When selection changed:
	m_btnRemoveFromList.EnableWindow(m_importList.GetSelectedCount() > 0);
	*pResult = 0;
}

void CAssetImporterDlg::OnBnClickedBtnAdvanced() {
	AssetImporterAdvancedDlg dlg(m_namingSyntaxs, NUM_NAMING_TYPES);

	if (dlg.DoModal() == IDOK) {
		if (dlg.m_bNamingSyntaxChanged) {
			UINT numSyntaxReturned = 0;
			dlg.GetNamingSyntaxs(m_namingSyntaxs, NUM_NAMING_TYPES, numSyntaxReturned);

			if (AfxMessageBox(L"Asset naming conventions has been changed.\n"
							  L"Do you want to apply them and rebuild asset tree?\n",
					MB_YESNO | MB_ICONEXCLAMATION) == IDYES) {
				m_bRebuildAssetTree = TRUE;
				OnCancel();
				return;
			}
		}

		UpdateAssetTreeView();
	}
}

//! Traverse asset tree to collect all warnings and display them
void CAssetImporterDlg::CheckAndDisplayPreviewWarnings() {
	std::string msgNamingProblem;

	// Only deal with naming (the only warning defined) for now
	for (AssetTree::pre_order_iterator it = m_allAssets->pre_order_begin(); it != m_allAssets->pre_order_end(); ++it) {
		AssetData& asset = *it;
		int warning = asset.CheckFlag(AssetData::WRN_MASK);

		if (warning & AssetData::WRN_NAMING)
			msgNamingProblem += asset.GetName() + "\n";
	}

	if (msgNamingProblem.empty())
		return;

	msgNamingProblem = "Importer has found following assets with improper naming: \n\n" + msgNamingProblem +
					   "\n\n"
					   "Importer will continue to import these assets but asset hierarchy will be incorrect.\n"
					   "Please make sure all assets follow proper naming convention.";

	AfxMessageBoxUtf8(msgNamingProblem.c_str());
}

LRESULT CAssetImporterDlg::OnInitialDisplay(WPARAM wParam, LPARAM lParam) {
	CheckAndDisplayPreviewWarnings();
	return S_OK;
}

void CAssetImporterDlg::OnShowWindow(BOOL bShow, UINT nStatus) {
	CDialog::OnShowWindow(bShow, nStatus);

	if (bShow && m_bInitialDisplay) {
		PostMessage(WM_USER + 1);
		m_bInitialDisplay = FALSE;
	}
}

void CAssetImporterDlg::RestoreAssetSelections() {
	m_importList.DeleteAllItems();

	for (AssetTree::pre_order_iterator it = m_allAssets->pre_order_begin(); it != m_allAssets->pre_order_end(); ++it) {
		AssetData& asset = *it;
		BOOL bSelected = asset.CheckFlag(AssetData::STA_SELECTED) != 0;

		CStringW treeItemKey = FormatTreeItemKey(&asset);
		if (m_treeItemKeyMap.find(treeItemKey) != m_treeItemKeyMap.end()) {
			HTREEITEM hItem = m_treeItemKeyMap[treeItemKey];

			// Set item states
			if (bSelected)
				SetTreeItemState(treeItemKey, TVIS_BOLD);
			else
				UnsetTreeItemState(treeItemKey, TVIS_BOLD);

			// Bold/Un-bold tree item
			m_assetTree.SetItemState(hItem, bSelected ? TVIS_BOLD : 0, TVIS_BOLD);

			// Add item to import list if selected
			if (bSelected) {
				int itemID = m_importList.InsertItem(m_importList.GetItemCount(), Utf8ToUtf16(asset.GetKey()).c_str());
				m_importList.SetItemData(itemID, (DWORD)&asset);
			}
		}
	}

	// Update button state
	m_btnImport.EnableWindow(m_importList.GetItemCount() > 0);
}

void CAssetImporterDlg::SetTreeItemState(const CStringW& key, int flag) {
	std::map<CStringW, UINT>::iterator itStates = m_treeItemStates.find(key);

	UINT newState = flag;
	if (itStates != m_treeItemStates.end()) {
		UINT oldState = itStates->second;
		newState |= oldState;
	}

	m_treeItemStates[key] = newState;
}

void CAssetImporterDlg::UnsetTreeItemState(const CStringW& key, int flag) {
	std::map<CStringW, UINT>::iterator itStates = m_treeItemStates.find(key);

	if (itStates != m_treeItemStates.end()) {
		UINT oldState = itStates->second;
		m_treeItemStates[key] = oldState & ~flag;
	}
}

void CAssetImporterDlg::OnCancel() {
	// Remove all asset selections
	for (AssetTree::pre_order_iterator it = m_allAssets->pre_order_begin(); it != m_allAssets->pre_order_end(); ++it) {
		AssetData& asset = *it;
		asset.ClearFlag(AssetData::STA_SELECTED);
	}

	CDialog::OnCancel();
}

void CAssetImporterDlg::ClearSuppressedMessage(int msgID) {
	std::map<int, UINT>::iterator findIt = m_suppressedMessages.find(msgID);
	if (findIt != m_suppressedMessages.end())
		m_suppressedMessages.erase(findIt);

	findIt = m_suppressedMsgCounter.find(msgID);
	if (findIt != m_suppressedMsgCounter.end())
		m_suppressedMsgCounter.erase(findIt);
}

void CAssetImporterDlg::ClearSuppressedMessageList() {
	m_suppressedMessages.clear();
	m_suppressedMsgCounter.clear();
}

UINT CAssetImporterDlg::ShowMessage(int msgID, CStringW message, UINT nType) {
	int res = 0;

	if (m_suppressedMessages.find(msgID) != m_suppressedMessages.end()) {
		m_suppressedMsgCounter[msgID]++;
		res = m_suppressedMessages[msgID];
	} else {
		res = AfxMessageBox(message, nType);

		if (m_bAutoSuppressDuplicatedMessage) {
			m_suppressedMsgCounter[msgID] = 1;
			m_suppressedMessages[msgID] = res;
		}
	}

	return res;
}

} // namespace KEP