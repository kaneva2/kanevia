#include "stdafx.h"
#include "AssetsDatabase.h"
#include "AssetNamingSyntax.h"
#include "AssetImporterNamingDlg.h"
#include "Core/Util/Unicode.h"
#include "UnicodeMFCHelpers.h"
#undef SubclassWindow

char* allDelimiters[] = { "_", "-", ".", ":", ";", "/", "\\", "(", ")", "[", "]", "{", "}" };

void AFXAPI DDX_Text(CDataExchange* pDX, int nIDC, string& value) {
	CStringA cstr;
	if (!pDX->m_bSaveAndValidate)
		cstr = value.c_str();
	DDX_Text(pDX, nIDC, cstr);
	if (pDX->m_bSaveAndValidate)
		value = (const char*)cstr;
}

void AFXAPI DDX_CBString(CDataExchange* pDX, int nIDC, string& value) {
	CStringW cstr;
	if (!pDX->m_bSaveAndValidate)
		cstr = Utf8ToUtf16(value).c_str();
	DDX_CBString(pDX, nIDC, cstr);
	if (pDX->m_bSaveAndValidate)
		value = Utf16ToUtf8(cstr).c_str();
}

IMPLEMENT_DYNAMIC(AssetImporterNamingDlg, CDialog)

AssetImporterNamingDlg::AssetImporterNamingDlg(int namingType, NamingSyntax& namingSyntax, CWnd* pParent /*=NULL*/) :
		CDialog(IDD, pParent), m_layout(*this), m_namingType(namingType), m_namingSyntax(namingSyntax), idLastVisibleToken(IDC_CB_BASIC_P0), ctrlsPerRow(0), ctrlWidth(0), ctrlHeight(0) {
	memset(idBasicTokens, 0, sizeof(idBasicTokens));
	idBasicTokens[0] = IDC_CB_BASIC_P0;

	memset(idBasicChecks, 0, sizeof(idBasicChecks));
	idBasicChecks[0] = IDC_CK_BASIC_P0;

	memset(idRegexOutputs, 0, sizeof(idRegexOutputs));
	idRegexOutputs[RESULT_TYPE] = IDC_ED_OUTPUT_TYPE;
	idRegexOutputs[RESULT_OBJECT] = IDC_ED_OUTPUT_OBJECT;
	idRegexOutputs[RESULT_PART] = IDC_ED_OUTPUT_PART;
	idRegexOutputs[RESULT_VARIATION] = IDC_ED_OUTPUT_VARIATION;
	idRegexOutputs[RESULT_LOD] = IDC_ED_OUTPUT_LOD;
}

AssetImporterNamingDlg::~AssetImporterNamingDlg() {
}

void AssetImporterNamingDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);

	// Syntax type radio buttons
	BOOL bBasicSyntax = TRUE, bAdvancedSyntax = FALSE;
	if (!pDX->m_bSaveAndValidate) {
		bBasicSyntax = m_namingSyntax.type == NamingSyntax::BASIC;
		bAdvancedSyntax = !bBasicSyntax;
	}

	// Use checkbox-style query to avoid M$'s "skipping non-radio button" warning
	DDX_Check(pDX, IDC_RB_BASIC, bBasicSyntax);
	DDX_Check(pDX, IDC_RB_ADVANCED, bAdvancedSyntax);

	if (pDX->m_bSaveAndValidate)
		m_namingSyntax.type = bBasicSyntax ? NamingSyntax::BASIC : NamingSyntax::REGEX;

	// Standard controls
	for (int i = 0; i < NUM_NAMING_TOKENS; i++)
		if (idBasicTokens[i])
			DDX_CBString(pDX, idBasicTokens[i], m_namingSyntax.tokens[i]);
	DDX_Text(pDX, IDC_ED_REGEX, m_namingSyntax.regex);
	for (int i = 0; i < NUM_NAMING_REOUTS; i++) DDX_Text(pDX, idRegexOutputs[i], m_namingSyntax.regexOutputs[i]);
	DDX_Control(pDX, IDC_LABEL_VARIATION, m_lblVariation);
	DDX_Control(pDX, IDC_ED_OUTPUT_VARIATION, m_edVariation);
	DDX_Text(pDX, IDC_ED_TEST_TEXT, m_sTestString);

	// Optional flags checkboxes
	if (!pDX->m_bSaveAndValidate) {
		for (int checkLoop = 0; checkLoop <= NUM_NAMING_TOKENS; checkLoop++) {
			if (idBasicChecks[checkLoop] == 0)
				break;

			BOOL bOptional = m_namingSyntax.firstOptional != -1 && m_namingSyntax.lastOptional != -1 && checkLoop >= m_namingSyntax.firstOptional && checkLoop <= m_namingSyntax.lastOptional;
			BOOL bChecked = !bOptional;

			DDX_Check(pDX, idBasicChecks[checkLoop], bChecked);
		}
	}
}

BEGIN_MESSAGE_MAP(AssetImporterNamingDlg, CDialog)
ON_BN_CLICKED(IDC_BTN_PARSE, &AssetImporterNamingDlg::OnBnClickedBtnParse)
ON_CONTROL_RANGE(CBN_EDITCHANGE, IDC_CB_BASIC_P0, IDC_CB_BASIC_P0 + NUM_NAMING_TOKENS - 1, &AssetImporterNamingDlg::OnComboBoxTextChanged)
ON_CONTROL_RANGE(CBN_SELCHANGE, IDC_CB_BASIC_P0, IDC_CB_BASIC_P0 + NUM_NAMING_TOKENS - 1, &AssetImporterNamingDlg::OnComboBoxTextChanged)
ON_CONTROL_RANGE(BN_CLICKED, IDC_CK_BASIC_P0, IDC_CK_BASIC_P0 + NUM_NAMING_TOKENS - 1, &AssetImporterNamingDlg::OnCheckBoxChanged)
ON_BN_CLICKED(IDC_BTN_REQUIRE_ALL, &AssetImporterNamingDlg::OnBnClickedBtnRequireAll)
END_MESSAGE_MAP()

CWnd* duplicateControl(const CWnd* pOldCtrl, int newID, const wchar_t* wndClass, CWnd* pNewCtrl = NULL, const wchar_t* newText = NULL, const CRect* pNewRect = NULL, BOOL bVisible = TRUE, BOOL bEnabled = TRUE) {
	ASSERT(pOldCtrl != NULL && pOldCtrl->GetSafeHwnd() != NULL);

	CWnd* pParent = pOldCtrl->GetParent();
	ASSERT(pParent != NULL && pParent->GetSafeHwnd() != NULL && pParent->GetDlgItem(newID) == NULL); // ID not used

	DWORD style = GetWindowStyle(pOldCtrl->GetSafeHwnd());
	DWORD styleEx = GetWindowExStyle(pOldCtrl->GetSafeHwnd());
	const CWnd* pPrev = pOldCtrl->GetNextWindow(GW_HWNDPREV);
	if (pPrev == NULL)
		pPrev = &pPrev->wndBottom;

	if (bVisible)
		style |= WS_VISIBLE;
	else
		style &= ~WS_VISIBLE;

	CStringW text;
	if (newText != NULL)
		text = newText;
	else
		pOldCtrl->GetWindowText(text);

	CRect rect;
	if (pNewRect)
		rect = *pNewRect;
	else {
		pOldCtrl->GetWindowRect(rect);
		pParent->ScreenToClient(rect);
	}

	CFont* pFont = pOldCtrl->GetFont();

	if (pNewCtrl == NULL)
		pNewCtrl = new CWnd(); // Can only create a CWnd object if not supplied
	pNewCtrl->CreateEx(styleEx, wndClass, text, style, rect, pParent, newID);
	ASSERT(pNewCtrl->GetSafeHwnd() != NULL);

	pNewCtrl->SetFont(pFont);
	pNewCtrl->EnableWindow(bEnabled);
	if (bVisible)
		pNewCtrl->SetWindowPos(pOldCtrl, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

	return pNewCtrl;
}

CComboBox* duplicateComboBox(const CComboBox* pOldCB, int newID, BOOL bVisible = TRUE, BOOL bEnabled = TRUE) {
	ASSERT(pOldCB != NULL && pOldCB->GetSafeHwnd() != NULL);
	ASSERT(pOldCB->GetParent() != NULL && pOldCB->GetParent()->GetSafeHwnd() != NULL);

	vector<CStringW> allStrings;
	map<CStringW, DWORD_PTR> allData;
	for (int i = 0; i < pOldCB->GetCount(); i++) {
		CStringW itemText;
		pOldCB->GetLBText(i, itemText);
		allStrings.push_back(itemText);
		DWORD_PTR data = pOldCB->GetItemData(i);
		allData.insert(pair<CStringW, DWORD_PTR>(itemText, data));
	}

	CStringW text;
	if (pOldCB->GetCurSel() != CB_ERR)
		pOldCB->GetLBText(pOldCB->GetCurSel(), text);
	else
		pOldCB->GetWindowText(text);

	CRect rect;
	pOldCB->GetDroppedControlRect(rect);
	pOldCB->GetParent()->ScreenToClient(rect);

	CComboBox* pNewCB = dynamic_cast<CComboBox*>(duplicateControl(pOldCB, newID, L"COMBOBOX", new CComboBox(), text, &rect, bVisible, bEnabled));
	if (pNewCB == NULL)
		return NULL;

	for (vector<CStringW>::iterator it = allStrings.begin(); it != allStrings.end(); ++it) {
		int idx = pNewCB->AddString(*it);
		if (idx != CB_ERR) {
			map<CStringW, DWORD_PTR>::iterator findData = allData.find(*it);
			if (findData != allData.end())
				pNewCB->SetItemData(idx, findData->second);
		}
	}

	return pNewCB;
}

CButton* duplicateButton(const CButton* pOldBtn, int newID, BOOL bVisible = TRUE, BOOL bEnabled = TRUE) {
	BOOL bChecked = pOldBtn->GetCheck();

	CButton* pNewBtn = dynamic_cast<CButton*>(duplicateControl(pOldBtn, newID, L"BUTTON", new CButton, NULL, NULL, bVisible, bEnabled));
	if (pNewBtn == NULL)
		return NULL;

	pNewBtn->SetCheck(bChecked);
	return pNewBtn;
}

// AssetImporterNamingDlg message handlers

#define CONTROL_SPACING_X 10
#define CONTROL_SPACING_Y 30

BOOL AssetImporterNamingDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	m_layout.AddLayout(IDC_RB_ADVANCED, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_PARSING, ST_BOTTOM);
	m_layout.AddLayout(IDC_ED_REGEX, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_OUTPUT, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_TYPE, ST_BOTTOM);
	m_layout.AddLayout(IDC_ED_OUTPUT_TYPE, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_OBJECT, ST_BOTTOM);
	m_layout.AddLayout(IDC_ED_OUTPUT_OBJECT, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_PART, ST_BOTTOM);
	m_layout.AddLayout(IDC_ED_OUTPUT_PART, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_VARIATION, ST_BOTTOM);
	m_layout.AddLayout(IDC_ED_OUTPUT_VARIATION, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_LOD, ST_BOTTOM);
	m_layout.AddLayout(IDC_ED_OUTPUT_LOD, ST_BOTTOM);
	m_layout.AddLayout(IDC_LABEL_TEST, ST_BOTTOM);
	m_layout.AddLayout(IDC_ED_TEST_TEXT, ST_BOTTOM);
	m_layout.AddLayout(IDC_BTN_PARSE, ST_BOTTOM);
	m_layout.AddLayout(IDC_GROUP_NAMING, ST_LEFT | ST_BOTTOM | ST_RIGHT | ST_TOP);
	m_layout.AddLayout(IDOK, ST_BOTTOM);
	m_layout.AddLayout(IDCANCEL, ST_BOTTOM);
	m_layout.EnableAutoLayout();

	// Prepare for future dynamic_cast's
	m_cbBasicToken0.SubclassWindow(GetDlgItem(IDC_CB_BASIC_P0)->GetSafeHwnd());
	m_ckBasicToken0.SubclassWindow(GetDlgItem(IDC_CK_BASIC_P0)->GetSafeHwnd());

	m_lblVariation.EnableWindow(m_namingType == NAMING_SKELETAL);
	m_lblVariation.ShowWindow(m_namingType == NAMING_SKELETAL ? SW_SHOW : SW_HIDE);
	m_edVariation.EnableWindow(m_namingType == NAMING_SKELETAL);
	m_edVariation.ShowWindow(m_namingType == NAMING_SKELETAL ? SW_SHOW : SW_HIDE);

	CComboBox* pFirstCB = static_cast<CComboBox*>(GetDlgItem(IDC_CB_BASIC_P0));
	CButton* pFirstCK = static_cast<CButton*>(GetDlgItem(IDC_CK_BASIC_P0));

	CRect rectCB, rectCK, rectDlg;
	pFirstCB->GetWindowRect(rectCB);
	ScreenToClient(rectCB);
	pFirstCK->GetWindowRect(rectCK);
	ScreenToClient(rectCK);
	GetClientRect(rectDlg);

	SetWindowLong(pFirstCB->GetSafeHwnd(), GWL_STYLE, pFirstCB->GetStyle() | WS_VSCROLL);

	ctrlWidth = rectCB.Width();
	ctrlHeight = rectCB.Height();
	ctrlsPerRow = (rectDlg.Width() - rectCB.left * 2 + CONTROL_SPACING_X) / (rectCB.Width() + CONTROL_SPACING_X);

	int idLastControlWithContent = 0;
	CComboBox* pPrevCB = pFirstCB;
	for (int i = 0; i < NUM_NAMING_TOKENS; i++) {
		int ctrlCol = i % ctrlsPerRow;
		int ctrlRow = i / ctrlsPerRow;

		int newX = rectCB.left + ctrlCol * (ctrlWidth + CONTROL_SPACING_X);
		int newY = rectCB.top + ctrlRow * (ctrlHeight + CONTROL_SPACING_Y);

		if (i > 0) {
			CComboBox* pNewCB = duplicateComboBox(pFirstCB, IDC_CB_BASIC_P0 + i, FALSE, FALSE);
			ASSERT(pNewCB != NULL);
			pNewCB->SetWindowPos(pPrevCB, newX, newY, 0, 0, SWP_NOSIZE);
			idBasicTokens[i] = IDC_CB_BASIC_P0 + i;
			pPrevCB = pNewCB;

			CButton* pNewCK = duplicateButton(pFirstCK, IDC_CK_BASIC_P0 + i, FALSE, FALSE);
			ASSERT(pNewCK != NULL);
			pNewCK->SetWindowPos(pNewCB, newX + rectCK.left - rectCB.left, newY + rectCK.top - rectCB.top, 0, 0, SWP_NOSIZE);
			idBasicChecks[i] = IDC_CK_BASIC_P0 + i;
		}

		if (idBasicTokens[i] == 0)
			continue;

		for (int varLoop = 0; varLoop < NUM_RESULT_STRINGS; varLoop++) {
			string variable = "$(" + NamingSyntax::resultVariables[varLoop] + ")";
			SendDlgItemMessage(idBasicTokens[i], CB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(variable).c_str());
		}

		for (int dlmLoop = 0; dlmLoop < sizeof(allDelimiters) / sizeof(char*); dlmLoop++)
			SendDlgItemMessage(idBasicTokens[i], CB_ADDSTRING, 0, (LPARAM)Utf8ToUtf16(allDelimiters[dlmLoop]).c_str());

		if (!m_namingSyntax.tokens[i].empty())
			idLastControlWithContent = idBasicTokens[i];
	}

	// Update data again because we have new controls added
	UpdateData(FALSE);

	if (idLastControlWithContent != 0)
		OnComboBoxTextChanged(idLastControlWithContent);

	return TRUE; // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void AssetImporterNamingDlg::OnBnClickedBtnParse() {
	UpdateData(TRUE);
	if (m_sTestString.IsEmpty()) {
		MessageBoxW(L"Please enter a test string for parsing");
		return;
	}

	string results[NUM_RESULT_STRINGS];
	if (!m_namingSyntax.parseString(Utf16ToUtf8(m_sTestString), results)) {
		MessageBoxW(L"Parsing failed");
		return;
	}

	string msg;
	for (int i = 0; i < NUM_RESULT_STRINGS; i++) {
		msg += "$(" + NamingSyntax::resultVariables[i] + ") = [" + results[i] + "]\n";
	}

	MessageBox(Utf8ToUtf16(msg).c_str());
}

void AssetImporterNamingDlg::OnComboBoxTextChanged(UINT id) {
	if (id < IDC_CB_BASIC_P0 || id >= IDC_CB_BASIC_P0 + NUM_NAMING_TOKENS - 1)
		return;

	ASSERT(ctrlsPerRow != 0);
	int oldNumOfRows = (idLastVisibleToken - IDC_CB_BASIC_P0 + ctrlsPerRow) / ctrlsPerRow;

	if (id >= idLastVisibleToken) {
		CComboBox* pComboBox = dynamic_cast<CComboBox*>(GetDlgItem(id));
		ASSERT(pComboBox != NULL);

		if (pComboBox->GetWindowTextLength() > 0 || pComboBox->GetCurSel() != -1) {
			// Bring items visible
			for (UINT i = idLastVisibleToken; i <= id + 1; i++) {
				if (i > idLastVisibleToken) {
					pComboBox = dynamic_cast<CComboBox*>(GetDlgItem(i));
					ASSERT(pComboBox != NULL);
					pComboBox->ShowWindow(SW_SHOW);
					pComboBox->EnableWindow(TRUE);
				}

				CButton* pCheck = dynamic_cast<CButton*>(GetDlgItem(IDC_CK_BASIC_P0 + i - IDC_CB_BASIC_P0));
				ASSERT(pCheck != NULL);
				pCheck->ShowWindow(SW_SHOW);
				pCheck->EnableWindow(i != id + 1);
			}

			idLastVisibleToken = id + 1;
		}
	} else {
		for (UINT i = id; i <= idLastVisibleToken; i++) {
			CComboBox* pComboBox = dynamic_cast<CComboBox*>(GetDlgItem(i));
			ASSERT(pComboBox != NULL);

			if (pComboBox->GetWindowTextLength() > 0)
				return;
		}

		// Hide next item and beyond
		for (UINT i = id; i <= idLastVisibleToken; i++) {
			if (i > id) {
				CComboBox* pComboBox = dynamic_cast<CComboBox*>(GetDlgItem(i));
				pComboBox->ShowWindow(SW_HIDE);
				pComboBox->EnableWindow(FALSE);
			}

			CButton* pCheck = dynamic_cast<CButton*>(GetDlgItem(IDC_CK_BASIC_P0 + i - IDC_CB_BASIC_P0));
			ASSERT(pCheck != NULL);
			pCheck->ShowWindow(i == id ? SW_SHOW : SW_HIDE);
			pCheck->SetCheck(TRUE);
			pCheck->EnableWindow(FALSE);
		}

		idLastVisibleToken = id;
	}

	int numOfRows = (idLastVisibleToken - IDC_CB_BASIC_P0 + ctrlsPerRow) / ctrlsPerRow;
	if (numOfRows != oldNumOfRows) {
		// Resize dialog
		CRect rect;
		GetWindowRect(rect);

		int newHeight = rect.Height() + (numOfRows - oldNumOfRows) * (ctrlHeight + CONTROL_SPACING_Y);
		rect.bottom = rect.top + newHeight;

		SetWindowPos(NULL, 0, 0, rect.Width(), rect.Height(), SWP_NOMOVE | SWP_NOZORDER);
	}
}

void AssetImporterNamingDlg::OnCheckBoxChanged(UINT id) {
	CButton* pCheckBox = dynamic_cast<CButton*>(GetDlgItem(id));
	ASSERT(pCheckBox != NULL && pCheckBox->GetSafeHwnd() != NULL);

	int index = id - IDC_CK_BASIC_P0;

	BOOL bOptional = !pCheckBox->GetCheck();
	if (bOptional) {
		if (m_namingSyntax.firstOptional == -1 || m_namingSyntax.firstOptional > index)
			m_namingSyntax.firstOptional = index;
		if (m_namingSyntax.lastOptional == -1 || m_namingSyntax.firstOptional < index)
			m_namingSyntax.lastOptional = index;
	} else {
		ASSERT(m_namingSyntax.firstOptional != -1 && m_namingSyntax.lastOptional != -1 && m_namingSyntax.firstOptional <= m_namingSyntax.lastOptional);

		if (m_namingSyntax.lastOptional == index) {
			m_namingSyntax.firstOptional = m_namingSyntax.lastOptional = -1;
		} else {
			m_namingSyntax.firstOptional = index + 1;
		}
	}

	UpdateData(FALSE);
}

void AssetImporterNamingDlg::OnBnClickedBtnRequireAll() {
	m_namingSyntax.firstOptional = m_namingSyntax.lastOptional = -1;
	UpdateData(FALSE);
}
